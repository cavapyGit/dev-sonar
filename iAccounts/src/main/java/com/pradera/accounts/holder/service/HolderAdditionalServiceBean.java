package com.pradera.accounts.holder.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.accounts.holder.to.HolderRegisterTO;
import com.pradera.accounts.holderaccount.service.HolderAccountServiceBean;
import com.pradera.accounts.investorType.facade.InvestorTypeFacade;
import com.pradera.accounts.util.view.PropertiesConstants;
import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.RegistryType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericPropertiesConstants;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.GeographicLocationServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.utils.DocumentTO;
import com.pradera.core.component.utils.PersonTO;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.label.InterfaceLabel;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.component.accounts.to.BankAccountObjectTO;
import com.pradera.integration.component.accounts.to.HolderAccountObjectTO;
import com.pradera.integration.component.accounts.to.JuridicHolderObjectTO;
import com.pradera.integration.component.accounts.to.LegalRepresentativeObjectTO;
import com.pradera.integration.component.accounts.to.NaturalHolderObjectTO;
import com.pradera.integration.component.accounts.to.PersonObjectTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.HolderHistory;
import com.pradera.model.accounts.HolderRequest;
import com.pradera.model.accounts.LegalRepresentativeHistory;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountBankRequest;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetRequest;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.HolderAccountRequest;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountGroupType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestHysStatusType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestHysType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountType;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.EconomicActivityType;
import com.pradera.model.accounts.type.HolderRequestType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.JuridicClassType;
import com.pradera.model.accounts.type.LegalRepresentativeStateType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.accounts.type.RepresentativeClassType;
import com.pradera.model.accounts.type.SexType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.GeographicLocationType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * Service Bean del titular Adicional.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/08/2015
 */
@Stateless
public class HolderAdditionalServiceBean extends CrudDaoServiceBean{	
	
	/** The country residence. */
	@Inject
	@Configurable("countryResidence")
	private Integer countryResidence;
	
	
	/** The holder service bean. */
	@EJB
	HolderServiceBean holderServiceBean;
	
	/** The holder account service bean. */
	@EJB
	HolderAccountServiceBean holderAccountServiceBean;
	
	/** The parameter service bean. */
	@EJB
	ParameterServiceBean parameterServiceBean;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	@EJB
	private ParticipantServiceBean participantServiceBean;
	
	/** The geographic location service bean. */
	@EJB
	GeographicLocationServiceBean geographicLocationServiceBean; 
	
	/** The holder control bean. */
	@EJB
	HolderControlBean holderControlBean;
	
	@EJB
	NotificationServiceFacade notificationServiceFacade;
	
	/** The transaction registry. */
	@Resource
	TransactionSynchronizationRegistry transactionRegistry;
	
	/** The lst holder register t os. */
	private List<HolderRegisterTO> lstHolderRegisterTOs;
	
	/** The holder account request. */
	private HolderAccountRequest holderAccountRequest;
	
	/** The map document type. */
	private Map<String,Integer> mapDocumentType;

/** The map document source. */
//	private Map<String,Integer> mapNationality;
	private Map<String,Integer> mapDocumentSource;
    
    /** The logger user. */
    private LoggerUser loggerUser;
    
    /** The Constant logger. */
	@Inject  
	PraderaLogger logger;
   
    /**
     * Inits the.
     */
    @PostConstruct
    public void init(){
    	initMaps();
    }
	
	/**
	 * Inits the maps.
	 */
	
	public void initMaps(){
		
		try{
			ParameterTableTO  parameterTableTO;
			mapDocumentType = new HashMap<String,Integer>();
			mapDocumentSource = new HashMap<String,Integer>();
			parameterTableTO = new ParameterTableTO();
			parameterTableTO.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
			parameterTableTO.setState(BooleanType.YES.getCode());
		
			for(ParameterTable param : generalParametersFacade.getListParameterTableServiceBean(parameterTableTO)) {
				mapDocumentType.put(param.getIndicator1(),param.getParameterTablePk());
			}
			
			parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.DOCUMENT_SOURCE.getCode());
			
			for(ParameterTable param : generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO)){
				mapDocumentSource.put(param.getText1(), param.getParameterTablePk());
			}
		}
		catch(Exception e){
			logger.error(e.getMessage());
		}
	}		
	
	/**
	 * Metodo para Crear Titular y cuenta.
	 *
	 * @param objHolderAccountObjectTO the obj holder account object to
	 * @return the object
	 * @throws ServiceException the service exception
	 */
	public Object createHolderAndAccount(HolderAccountObjectTO objHolderAccountObjectTO) throws ServiceException {
		loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		lstHolderRegisterTOs = new ArrayList<HolderRegisterTO>();
		
		HolderAccount objHolderAcc = new HolderAccount();
		if(objHolderAccountObjectTO.isValidateAccount() && 
				Validations.validateIsNotNullAndNotEmpty(objHolderAccountObjectTO.getAccountNumber())){									
			objHolderAcc = holderAccountServiceBean.getIdHolderAccount(
					objHolderAccountObjectTO.getIdParticipant(), 
					objHolderAccountObjectTO.getAccountNumber(), 
					objHolderAccountObjectTO.getAccountType());
			// Si No encuentra cuenta o esta en estado Invvalido con el numero de cuenta enviado	
			if(objHolderAcc == null){
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDERACCOUNT_NOTEXITS_ACCOUNT_NUMBER, null);
			} else {							
				if(!HolderAccountStatusType.ACTIVE.getCode().equals(objHolderAcc.getStateAccount())){
					return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDERACCOUNT_STATUS_NOT_REGISTERED, null);
				}			
				logger.info(":::: SE ENCONTRO NUMERO DE CUENTA ::::");
				logger.info("ID CUENTA : " + objHolderAcc.getIdHolderAccountPk());
				logger.info("PARTICIPANTE : " + objHolderAccountObjectTO.getIdParticipant());
				logger.info("NUMERO DE CUENTA : " + objHolderAccountObjectTO.getAccountNumber());
				logger.info("TIPO DE CUENTA : " + objHolderAccountObjectTO.getAccountType());
				return objHolderAcc.getIdHolderAccountPk();
			}
		}
		//Cuenta es tipo Natural
		if(Validations.validateIsNotNull(objHolderAccountObjectTO.getAccountType()) && 
				objHolderAccountObjectTO.getAccountType().equals(HolderAccountType.NATURAL.getCode())){
			
			if(Validations.validateListIsNotNullAndNotEmpty(objHolderAccountObjectTO.getLstNaturalHolderObjectTOs())){				
				if(objHolderAccountObjectTO.getLstNaturalHolderObjectTOs().size() != 1){
			   		return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDERACCOUNT_SIZE_NATURAL, null);				   
			   	}				
				RecordValidationType recordValidationType = null;
				if(objHolderAccountObjectTO.isCreateObject()){					
					recordValidationType = validateAndLoadEntities(objHolderAccountObjectTO, objHolderAccountObjectTO.getLstNaturalHolderObjectTOs().get(0));
					if(recordValidationType != null){
						return recordValidationType;
					}					
				} else {
					if(objHolderAccountObjectTO.isEarlyPaymentNoDpf()){
						objHolderAccountObjectTO.getLstNaturalHolderObjectTOs().get(0).setEarlyPaymentNoDpf(true);
					}
					validateInformationDocumentType(objHolderAccountObjectTO.getLstNaturalHolderObjectTOs().get(0));
					loadHolderHistory(objHolderAccountObjectTO, objHolderAccountObjectTO.getLstNaturalHolderObjectTOs().get(0));
				}				
				
				// VALIDACION DE TITULARES				
				Boolean blCreateAccount = false;
				Object objCreateAccount = getObjectNotCreateAccount(objHolderAccountObjectTO);				
				if(objCreateAccount instanceof RecordValidationType){
					RecordValidationType validation = (RecordValidationType) objCreateAccount;
					recordValidationType = validation;
				} else if (objCreateAccount instanceof Boolean){
					blCreateAccount = (Boolean) objCreateAccount;
				}				
				if(recordValidationType != null){
					return recordValidationType;
				}				
				if(blCreateAccount){
					RecordValidationType recordValidationTypeNew = validateHoldersInHolderAccount(objHolderAccountObjectTO, loggerUser);
					if(recordValidationTypeNew != null){
						return recordValidationTypeNew;
					}					
				} else {
					if(objHolderAccountObjectTO.isValidateAccount()){
						return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDERACCOUNT_HOLDERS_NOTEXITS, null);
					}
				}
				
				logger.info(":::: TIPO DE CUENTA : NATURAL ::::");
				logger.info("Tipo de cuenta: " + objHolderAccountObjectTO.getAccountType());
				logger.info("Tipo de cuenta aux: " + objHolderAccountObjectTO.getAccountTypeCode());
				logger.info("Participante: " +  objHolderAccountObjectTO.getIdParticipant());
				logger.info("Numero de cuenta: " +  objHolderAccountObjectTO.getAccountNumber());
				logger.info("pk de cuenta: " +  objHolderAccountObjectTO.getIdHolderAccount());
				
			} else {
				return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_seccTitularNatural,null);
			}
		//Cuenta Tipo Juridica
		} else if(Validations.validateIsNotNull(objHolderAccountObjectTO.getAccountType()) && 
				objHolderAccountObjectTO.getAccountType().equals(HolderAccountType.JURIDIC.getCode())){
			// take juridic holder if account type is juridic
			if (Validations.validateIsNotNull(objHolderAccountObjectTO.getJuridicHolderObjectTO())){
				
				RecordValidationType recordValidationType = null;
				if(objHolderAccountObjectTO.isCreateObject()){	
					recordValidationType = validateAndLoadEntities(objHolderAccountObjectTO, objHolderAccountObjectTO.getJuridicHolderObjectTO());
					if(recordValidationType != null){
						return recordValidationType;
					}
				} else {
					validateInformationDocumentType(objHolderAccountObjectTO.getJuridicHolderObjectTO());
					loadHolderHistory(objHolderAccountObjectTO, objHolderAccountObjectTO.getJuridicHolderObjectTO());
				}																							
				
				// 	VALIDACION DE TITULARES
				
				Boolean blCreateAccount = false;
				Object objCreateAccount = getObjectNotCreateAccount(objHolderAccountObjectTO);
				
				if(objCreateAccount instanceof RecordValidationType){
					RecordValidationType validation = (RecordValidationType) objCreateAccount;
					recordValidationType = validation;
				} else if (objCreateAccount instanceof Boolean){
					blCreateAccount = (Boolean) objCreateAccount;
				}
				
				if(recordValidationType != null){
					return recordValidationType;
				}
				
				if(blCreateAccount){
					RecordValidationType recordValidationTypeNew = validateHoldersInHolderAccount(objHolderAccountObjectTO, loggerUser);
					if(recordValidationTypeNew != null){
						return recordValidationTypeNew;
					}
				} else{
					if(objHolderAccountObjectTO.isValidateAccount()){
						return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDERACCOUNT_HOLDERS_NOTEXITS, null);
					}
				}				

				logger.info(":::: TIPO DE CUENTA : JURIDICO ::::");
				logger.info("Tipo de cuenta: " + objHolderAccountObjectTO.getAccountType());
				logger.info("Tipo de cuenta aux: " + objHolderAccountObjectTO.getAccountTypeCode());
				logger.info("Participante: " +  objHolderAccountObjectTO.getIdParticipant());
				logger.info("Numero de cuenta: " +  objHolderAccountObjectTO.getAccountNumber());
				logger.info("pk de cuenta: " +  objHolderAccountObjectTO.getIdHolderAccount());
				
			} else {
				return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_seccTitularJuridico,null);
			}
		//Si es cuenta Mancomunada	
		} else if (Validations.validateIsNotNull(objHolderAccountObjectTO.getAccountType()) && 
				objHolderAccountObjectTO.getAccountType().equals(HolderAccountType.OWNERSHIP.getCode())){
			// only consider natural holders for ownership accounts
		   if(Validations.validateListIsNotNullAndNotEmpty(objHolderAccountObjectTO.getLstNaturalHolderObjectTOs())){
			   
			   	if(objHolderAccountObjectTO.getLstNaturalHolderObjectTOs().size() < 2){
			   		return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDERACCOUNT_SIZE_OWNERSHIP, null);				   
			   	}
			   
			   	if(objHolderAccountObjectTO.isCreateObject()){
			   		for(NaturalHolderObjectTO naturalHolderObjectTO : objHolderAccountObjectTO.getLstNaturalHolderObjectTOs()){
				   		RecordValidationType recordValidationType  = validateAndLoadEntities(objHolderAccountObjectTO, naturalHolderObjectTO);
						if(recordValidationType != null){
							return recordValidationType;
						}
				   	}
			   	} else {			   		
			   		for(NaturalHolderObjectTO naturalHolderObjectTO : objHolderAccountObjectTO.getLstNaturalHolderObjectTOs()){
			   			validateInformationDocumentType(naturalHolderObjectTO);
			   			loadHolderHistory(objHolderAccountObjectTO, naturalHolderObjectTO);
			   		}			   		
			   	}			   				   			   				   												
				
			   	// VALIDACION DE TITULARES
				
				Boolean blCreateAccount = false;
				Object objCreateAccount = getObjectNotCreateAccount(objHolderAccountObjectTO);
				RecordValidationType recordValidationType = null;
				
				if(objCreateAccount instanceof RecordValidationType){
					RecordValidationType validation = (RecordValidationType) objCreateAccount;
					recordValidationType = validation;
				} else if (objCreateAccount instanceof Boolean){
					blCreateAccount = (Boolean) objCreateAccount;
				}
				
				if(recordValidationType != null){
					return recordValidationType;
				}
			   	
				if(blCreateAccount){
					RecordValidationType recordValidationTypeNew = validateHoldersInHolderAccount(objHolderAccountObjectTO, loggerUser);
					if(recordValidationTypeNew != null){
						return recordValidationTypeNew;
					}
				} else{
					if(objHolderAccountObjectTO.isValidateAccount()){
						return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDERACCOUNT_HOLDERS_NOTEXITS, null);
					}
				}
				
				logger.info(":::: TIPO DE CUENTA : MANCOMUNADO ::::");
				logger.info("Tipo de cuenta: " + objHolderAccountObjectTO.getAccountType());
				logger.info("Tipo de cuenta aux: " + objHolderAccountObjectTO.getAccountTypeCode());
				logger.info("Participante: " +  objHolderAccountObjectTO.getIdParticipant());
				logger.info("Numero de cuenta: " +  objHolderAccountObjectTO.getAccountNumber());
				logger.info("pk de cuenta: " +  objHolderAccountObjectTO.getIdHolderAccount());
				
		   } else {
				return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_seccTitularNatural,null);
			}
		}				
		
		if(objHolderAccountObjectTO.getIdHolderAccount() != null){
			return objHolderAccountObjectTO.getIdHolderAccount();
		} else {
			if(!objHolderAccountObjectTO.isCreateObject()){
				return CommonsUtilities.populateRecordCodeValidation(null, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_HOLDERACCOUNT_NULL);			
			}
		}				
		
		// we iterate holder register list for save
		for (HolderRegisterTO holderAccountRegisterTO : lstHolderRegisterTOs) {
			
			Holder holder = new Holder();			
			HolderRequest objHolderRequestAux = new HolderRequest();
			
			logger.info(":::: VERIFICAR EXISTENCIA DEL TITULAR ::::");			
			if(holderAccountRegisterTO.isExistHolder() && holderAccountRegisterTO.getHolderRequest().getIdHolderRequestPk() != null){
				objHolderRequestAux = holderAccountRegisterTO.getHolderRequest();
				logger.info(" TITULAR EXISTE  : " + objHolderRequestAux.getIdHolderRequestPk());
			} else {
				objHolderRequestAux = null;
				logger.info(" TITULAR NO EXISTE ");
			}
			
			if(objHolderRequestAux == null){
				
				if( Validations.validateIsNotNull(holderAccountRegisterTO.getHolderHistory())
					&& Validations.validateIsNotNull(holderAccountRegisterTO.getHolderHistory().getEconomicActivity())
					){
					// issue 1394: setear el tipo de inversionista por defecto
					holderAccountRegisterTO.getHolderHistory().setInvestorType(getInvestorTypeByDefault(holderAccountRegisterTO.getHolderHistory().getEconomicActivity()));
					if(Validations.validateIsNotNullAndPositive(holderAccountRegisterTO.getHolderHistory().getJuridicClass())){
						if(holderAccountRegisterTO.getHolderHistory().getJuridicClass().equals(JuridicClassType.TRUST.getCode()) ){
							// forzamos a que el tipo de inv sea FIDEICOMISO 
							holderAccountRegisterTO.getHolderHistory().setInvestorType(2584);
						}
					}
				}
				
				HolderRequest holderRequest = holderServiceBean.registerHolderRequestServiceBean(
						holderAccountRegisterTO.getHolderRequest(), 
						holderAccountRegisterTO.getParticipant(), 
						holderAccountRegisterTO.getHolderHistory(), 
						holderAccountRegisterTO.getLstLegalRepresentativeHistories(), null, loggerUser);				
				holderAccountRegisterTO.setHolderRequest(holderRequest);				
				holder = holderServiceBean.confirmHolderRequestServiceBean(holderRequest, holderAccountRegisterTO.getHolderHistory(), loggerUser);
				logger.info(" TITULAR CREADO : " + holder.getIdHolderPk());
				
				// issue 1329: Notificar a los usuarios EDV cuando se registre un titular con las siguientes actividades economicas
				try {
					if(Validations.validateIsNotNullAndPositive(holder.getEconomicActivity())){
						// issue 1329: Si la actividad economica es cualquiera de estas se debe notificar al usuario EDV
						if(	holder.getEconomicActivity().equals(EconomicActivityType.ADMINISTRADORAS_FONDOS_PENSIONES.getCode())
							|| holder.getEconomicActivity().equals(EconomicActivityType.SECTOR_PUBLICO_NO_FINANCIERO.getCode())
							|| holder.getEconomicActivity().equals(EconomicActivityType.AGENCIAS_BOLSA.getCode())
							|| holder.getEconomicActivity().equals(EconomicActivityType.BANCOS.getCode())
							|| holder.getEconomicActivity().equals(EconomicActivityType.BANCOS_AHORRO_CREDITOS_FOMENTO.getCode())
							|| holder.getEconomicActivity().equals(EconomicActivityType.COMPANIAS_SEGURO.getCode())
							|| holder.getEconomicActivity().equals(EconomicActivityType.COOPERATIVAS.getCode())
							|| holder.getEconomicActivity().equals(EconomicActivityType.AFP.getCode())
							|| holder.getEconomicActivity().equals(EconomicActivityType.FONDOS_INVERSION_ABIERTOS.getCode())
							|| holder.getEconomicActivity().equals(EconomicActivityType.FONDOS_INVERSION_CERRADOS.getCode())
							|| holder.getEconomicActivity().equals(EconomicActivityType.FONDOS_INVERSION_MIXTOS.getCode())
							|| holder.getEconomicActivity().equals(EconomicActivityType.OTROS_SERVICIOS_FINANCIEROS.getCode())
							|| holder.getEconomicActivity().equals(EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode())
							|| holder.getEconomicActivity().equals(EconomicActivityType.SOCIEDADES_TITULARIZACION.getCode())
							|| holder.getEconomicActivity().equals(Integer.parseInt(String.valueOf(2362))) // ParameterTable
							|| holder.getEconomicActivity().equals(Integer.parseInt(String.valueOf(2370))) // ParameterTable
							|| holder.getEconomicActivity().equals(Integer.parseInt(String.valueOf(2468))) // ParameterTable
								){
							
							ParameterTable ptEconomicAtivity = em.find(ParameterTable.class, holder.getEconomicActivity());
							if( Validations.validateIsNotNull(ptEconomicAtivity)
									&& Validations.validateIsNotNullAndNotEmpty(ptEconomicAtivity.getParameterName())
									&& Validations.validateIsNotNullAndPositive(holder.getIdHolderPk())){
								Object[] parameters = { holder.getIdHolderPk(), ptEconomicAtivity.getParameterName()};
								// envio de notificacion que se realizo un registro de solicitud de apertura de canal
								BusinessProcess businessProcessNotification = new BusinessProcess();
								businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_WHEN_CONFIRM_HOLDER_WITH_EC_ACTIVITY.getCode());
								notificationServiceFacade.sendNotification("WEBSERVICE",
								businessProcessNotification,
								null, parameters);
								System.out.println(":::  Usuario notificado");
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			} else {				
				holderAccountRegisterTO.setHolderRequest(objHolderRequestAux);
				holder = find(Holder.class, objHolderRequestAux.getHolder().getIdHolderPk());				
				if(Validations.validateIsNullOrEmpty(holder.getDocumentSource())){
					holderServiceBean.updateHolderDocumentSource(holder.getIdHolderPk(), 
							holderAccountRegisterTO.getHolderHistory().getDocumentSource(), loggerUser);
					logger.info(" EXPEDIDO NUEVO : " + holderAccountRegisterTO.getHolderHistory().getDocumentSource());
				}				
				logger.info(" TITULAR EXISTENTE : " + holder.getIdHolderPk());
			}
						
			if(holderAccountRegisterTO.getPersonObjectTO() instanceof NaturalHolderObjectTO){
				((NaturalHolderObjectTO)holderAccountRegisterTO.getPersonObjectTO()).setIdHolder(holder.getIdHolderPk());
			}else if(holderAccountRegisterTO.getPersonObjectTO() instanceof JuridicHolderObjectTO){
				((JuridicHolderObjectTO)holderAccountRegisterTO.getPersonObjectTO()).setIdHolder(holder.getIdHolderPk());
			}
			
			holderAccountRegisterTO.setHolder(holder);
		}
		
		// we iterate registered holder list for creating holder account
		populateHolderAccount(objHolderAccountObjectTO);
		
		// register holderAccountRequest but with confirmed state
		holderAccountServiceBean.registryHolderAccountRequest(holderAccountRequest);
		HolderAccount holderAccount = holderAccountServiceBean.createHolderAccountFromRequest(holderAccountRequest, objHolderAccountObjectTO.getAccountNumber(), null);		
		objHolderAccountObjectTO.setIdHolderAccount(holderAccount.getIdHolderAccountPk());
		
		logger.info("holder account created by interface "+objHolderAccountObjectTO.getIdHolderAccount());
		
		return null;
	}
		
	@EJB
	InvestorTypeFacade investorTypeFacade;
	private Integer getInvestorTypeByDefault(Integer economicActivity){
		try {
			if(Validations.validateIsNotNullAndPositive(economicActivity)){
				List<ParameterTable> lstEconomicActivityWithInvetorType = investorTypeFacade.getLstEconomicActivityToRelationInvestortype();
				for(ParameterTable pt:lstEconomicActivityWithInvetorType){
					if( Validations.validateIsNotNull(pt) 
						&& Validations.validateIsNotNullAndPositive(pt.getParameterTablePk())
						&& pt.getParameterTablePk().equals(economicActivity) 
						&& Validations.validateIsNotNullAndPositive(pt.getShortInteger())){
						return pt.getShortInteger();
					}
				}
			}
		} catch (Exception e) {
			System.out.println("::: Error al sugerir el tipo de inversionista: (error irrelevante) "+e.getMessage());
		}
		
		// si pasa por aqui quiere decir de que no ha encontrado un tipo de inv relacionado, entonces se coloca por defecto OTRAS personas juridicas
		// este es el id de OTRAS PERSONAS JURIDICAS en tipo de Inversionista
		return 2577;
	}
	
	/**
	 * Metodo que Obtiene Objeto no Crea Cuenta.
	 *
	 * @param objHolderAccountObjectTO the obj holder account object to
	 * @return the object not create account
	 */
	private Object getObjectNotCreateAccount(HolderAccountObjectTO objHolderAccountObjectTO){
		Boolean blNotValidateAccount = true;		
		for (HolderRegisterTO holderAccountRegisterTO : lstHolderRegisterTOs) {
			List<HolderHistory> lstHolderHistory = new ArrayList<HolderHistory>();			
			lstHolderHistory.add(holderAccountRegisterTO.getHolderHistory());			
			HolderRequest objHolderRequestAux = new HolderRequest();
			objHolderRequestAux.setHolderHistories(lstHolderHistory);			
			List<HolderRequest> lstholderRequest = holderServiceBean.getListHolderRequestByDocumentTypeAndDocumentNumber(objHolderRequestAux);			
			// OBTENER EL TITULAR 			
			if(Validations.validateListIsNotNullAndNotEmpty(lstholderRequest)){				
				// EXCLUIR LOS TITULARES NO REGISTRADOS				
				List<HolderRequest> lstholderRequestAux = new ArrayList<HolderRequest>();
				int countRegistered = 0;
				for(HolderRequest objHolderRequest : lstholderRequest){					
					Holder objHolder = objHolderRequest.getHolder();
					if(HolderStateType.REGISTERED.getCode().equals(objHolder.getStateHolder())){
						lstholderRequestAux.add(objHolderRequest);
						countRegistered ++;
					}
				}				
				if(countRegistered == GeneralConstants.ONE_VALUE_INTEGER.intValue()){
					lstholderRequest = lstholderRequestAux;
				}				
				// ES FIDEICOMISO
				if(lstholderRequest.size() > 1){
					int count = 0;	
					// VERIFICAR QUE EL TITULAR SEA JURIDICO
					if(Validations.validateIsNotNullAndNotEmpty(holderAccountRegisterTO.getHolderHistory().getHolderType()) &&
							PersonType.JURIDIC.getCode().equals(holderAccountRegisterTO.getHolderHistory().getHolderType())){
						if(Validations.validateIsNullOrEmpty(objHolderAccountObjectTO.getAccountNumber())){
							return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_DIDEICOMISO_ISNECESARIO_CUI, null);						
						}						
					} else {
						
						//Ini Validando con la fecha de nacimiento issue 1284
						if (Validations.validateIsNotNullAndNotEmpty(holderAccountRegisterTO.getHolderHistory().getHolderType()) &&
								!PersonType.JURIDIC.getCode().equals(holderAccountRegisterTO.getHolderHistory().getHolderType())) {
							HolderHistory holderHistoryAux = objHolderRequestAux.getHolderHistories().get(0);
							if (Validations.validateIsNotNullAndNotEmpty(holderHistoryAux.getBirthDate())) {
								boolean swFound = false;
								for (HolderRequest objHolderRequest : lstholderRequest) {
									Holder objHolderAux = objHolderRequest.getHolder();
									if (HolderStateType.REGISTERED.getCode().equals(objHolderAux.getStateHolder())) {
										if (Validations.validateIsNotNullAndNotEmpty(objHolderAux.getBirthDate())) {
											Date birthDateHolderNew = CommonsUtilities.truncateDateTime(holderHistoryAux.getBirthDate());
											Date birthDateHolderOld = CommonsUtilities.truncateDateTime(objHolderAux.getBirthDate());
											if (birthDateHolderNew.compareTo(birthDateHolderOld) == 0) {
												swFound = true;
												lstholderRequest = new ArrayList<HolderRequest>();
												lstholderRequest.add(objHolderRequest);
												break;
											}
										}
									}
								}
								if (!swFound) {
									return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_BIRTH_DATE_INCORRECT, null, new Object[] { holderHistoryAux.getDocumentNumber() });
								}
							}
						}
						//Fin
						
						for(HolderRequest objHolderRequest : lstholderRequest){
							Holder objHolder = objHolderRequest.getHolder();
							if(!HolderStateType.REGISTERED.getCode().equals(objHolder.getStateHolder())){
								count ++;
							} else {
								holderAccountRegisterTO.setHolderRequest(objHolderRequest);
								holderAccountRegisterTO.setExistHolder(true);								
								break;
							}
						}
					}					
					// VALIDAR QUE EL TITULAR ESTE EN ESTADO RESGISTRADO					
					if(count > 0 && Validations.validateIsNullOrEmpty(holderAccountRegisterTO.getHolderRequest().getIdHolderRequestPk())){
						return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_STATUS_NOT_REGISTERED, null);
					}					
				} else {					
					// ASIGNAR EL TITULAR EXISTENTE					
					Holder objHolder = lstholderRequest.get(0).getHolder();
					if(HolderStateType.REGISTERED.getCode().equals(objHolder.getStateHolder())){
						
						//Ini Validando con la fecha de nacimiento issue 1284
						if(Validations.validateIsNotNullAndNotEmpty(holderAccountRegisterTO.getHolderHistory().getHolderType()) &&
								!PersonType.JURIDIC.getCode().equals(holderAccountRegisterTO.getHolderHistory().getHolderType())){
							HolderHistory holderHistoryAux = objHolderRequestAux.getHolderHistories().get(0);
							if (Validations.validateIsNotNullAndNotEmpty(holderHistoryAux.getBirthDate())) {
								if (Validations.validateIsNotNullAndNotEmpty(objHolder.getBirthDate())) {
									Date birthDateHolderNew = CommonsUtilities.truncateDateTime(holderHistoryAux.getBirthDate());
									Date birthDateHolderOld = CommonsUtilities.truncateDateTime(objHolder.getBirthDate());
									if (birthDateHolderNew.compareTo(birthDateHolderOld) != 0) {
										return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_BIRTH_DATE_INCORRECT, null, new Object[] { objHolder.getDocumentNumber() });
									}
								}
							}
						}
						//Fin
						
						holderAccountRegisterTO.setHolderRequest(lstholderRequest.get(0));
						holderAccountRegisterTO.setExistHolder(true);
					} else {
						return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_STATUS_NOT_REGISTERED, null);
					}					
				}				
			} else {
		
				blNotValidateAccount = Boolean.FALSE;						
				holderAccountRegisterTO.setExistHolder(Boolean.FALSE);
			
			}						
		}
		return blNotValidateAccount;
	}
	
	/**
	 * Metodo para llenar los Datos de Cuenta Titular.
	 *
	 * @param objHolderAccountObjectTO the obj holder account object to
	 */
	private void populateHolderAccount(HolderAccountObjectTO objHolderAccountObjectTO) {
		holderAccountRequest = new HolderAccountRequest();
		holderAccountRequest.setHolderAccountDetRequest(new ArrayList<HolderAccountDetRequest>());
		holderAccountRequest.setParticipant(new Participant(objHolderAccountObjectTO.getIdParticipant()));
		holderAccountRequest.setHolderAccountType(objHolderAccountObjectTO.getAccountType());
		holderAccountRequest.setHolderAccountGroup(HolderAccountGroupType.INVERSTOR.getCode());
		holderAccountRequest.setRequestType(HolderAccountRequestHysType.CREATE.getCode());
		holderAccountRequest.setRequestNumber(0l);
		holderAccountRequest.setRegistryDate(CommonsUtilities.currentDate());
		holderAccountRequest.setRegistryUser(loggerUser.getUserName());
		holderAccountRequest.setApproveDate(CommonsUtilities.currentDate());
		holderAccountRequest.setApproveUser(loggerUser.getUserName());
		holderAccountRequest.setConfirmDate(CommonsUtilities.currentDate());
		holderAccountRequest.setConfirmUser(loggerUser.getUserName());
		holderAccountRequest.setRequestState(HolderAccountRequestHysStatusType.CONFIRM.getCode());		
		holderAccountRequest.setRelatedName(objHolderAccountObjectTO.getRelatedName());
		holderAccountRequest.setIndPdd(objHolderAccountObjectTO.getIndPdd());
		holderAccountRequest.setBlPdd(objHolderAccountObjectTO.getIndPdd().equals(BooleanType.YES.getCode())? true : false);
		
		int index = 0;
		for (HolderRegisterTO holderAccountRegisterTO : lstHolderRegisterTOs) {
			HolderAccountDetRequest holderAccountReqDetHy = new HolderAccountDetRequest();
			holderAccountReqDetHy.setHolder(holderAccountRegisterTO.getHolder());
			holderAccountReqDetHy.setHolderAccountRequest(holderAccountRequest);		
			if(objHolderAccountObjectTO.getAccountType().equals(HolderAccountType.OWNERSHIP.getCode())){				
				if(objHolderAccountObjectTO.isDpfInterface()){
					if(index == 0){
						holderAccountReqDetHy.setIndRepresentative(BooleanType.YES.getCode());
					} else {
						holderAccountReqDetHy.setIndRepresentative(BooleanType.NO.getCode());
					}					
				} else {
					holderAccountReqDetHy.setIndRepresentative(((NaturalHolderObjectTO)holderAccountRegisterTO.getPersonObjectTO()).getIndRepresentative());
				}				
			}else{
				holderAccountReqDetHy.setIndRepresentative(ComponentConstant.ONE);
			}
			
			holderAccountReqDetHy.setRegistryDate(CommonsUtilities.currentDate());
			holderAccountReqDetHy.setRegistryUser(loggerUser.getUserName());
			holderAccountReqDetHy.setIndOldNew(BooleanType.YES.getCode());			
			holderAccountRequest.getHolderAccountDetRequest().add(holderAccountReqDetHy);
			index = index + 1;
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(objHolderAccountObjectTO.getLstBankAccountObjectTOs())) {
			holderAccountRequest.setHolderAccountBankRequest(new ArrayList<HolderAccountBankRequest>());
			for(BankAccountObjectTO bankObjTO : objHolderAccountObjectTO.getLstBankAccountObjectTOs()) {
				HolderAccountBankRequest bankReq = new HolderAccountBankRequest();
				bankReq.setAccountNumber(bankObjTO.getAccountNumber());
				
				Bank bank = new Bank();
				bank.setIdBankPk(bankObjTO.getIdBankPk());
				bankReq.setBank(bank);
				
				bankReq.setBankAccountType(bankObjTO.getBankAccountType());
				bankReq.setCurrency(bankObjTO.getCurrency());
				bankReq.setDescriptionBank(bankObjTO.getBankDescription());
				bankReq.setFlagForeignAccount(Boolean.FALSE);
				bankReq.setHaveBic(BooleanType.NO.getCode());
				bankReq.setHolderAccountRequest(holderAccountRequest);
				bankReq.setIndOldNew(BooleanType.NO.getCode());
				bankReq.setAudit(loggerUser);
				bankReq.setRegistryDate(new Date());
				bankReq.setRegistryUser(loggerUser.getUserName());
				
				holderAccountRequest.getHolderAccountBankRequest().add(bankReq);
			}
		}
	}
	
	/**
	 * Valida y Carga Entidades.
	 *
	 * @param holderAccountObjectTO the holder account object to
	 * @param personObjectTO the person object to
	 * @return the record validation type
	 */
	private RecordValidationType validateAndLoadEntities(HolderAccountObjectTO holderAccountObjectTO, PersonObjectTO personObjectTO) {
		RecordValidationType error = validateInformationPerson(personObjectTO);
		if(Validations.validateIsNotNull(error)){
			return error;
		}
		
		error = validateFlowPersonType(holderAccountObjectTO,personObjectTO);
		if(Validations.validateIsNotNull(error)){
			return error;
		}
		
		loadHolderHistory(holderAccountObjectTO, personObjectTO);
		
		return error;
		
	}
		
	/**
	 * Valida Tipo de Persona.
	 *
	 * @param holderAccountObjectTO the holder account object to
	 * @param personObjectTO the person object to
	 * @return the record validation type
	 */
	private RecordValidationType validateFlowPersonType(HolderAccountObjectTO holderAccountObjectTO, PersonObjectTO personObjectTO){
		PersonTO personTO = new PersonTO();
		//Si es titular Tipo Persona Natural
		if(personObjectTO instanceof NaturalHolderObjectTO){			
			NaturalHolderObjectTO naturalHolderObjectTO  = (NaturalHolderObjectTO) personObjectTO;		
			if(naturalHolderObjectTO.getIndMinor() != null && naturalHolderObjectTO.getIndMinor().equals(BooleanType.YES.getCode())){
			   personTO.setFlagMinorIndicator(true);
			}
			if(naturalHolderObjectTO.getIndDisabled() != null && naturalHolderObjectTO.getIndDisabled().equals(BooleanType.YES.getCode())){
				if(holderAccountObjectTO.isDpfInterface()){
					if(GeneralConstants.TWO_VALUE_INTEGER.equals(naturalHolderObjectTO.getIndDisabled())){
						naturalHolderObjectTO.setIndDisabled(BooleanType.NO.getCode());
					}
				}
				// issue 1233: se ha quitado la validacion del tag incapacidad porque no aplica en nuestro pais
				//naturalHolderObjectTO.setIndDisabled(BooleanType.NO.getCode());
			   personTO.setFlagInabilityIndicator(true);	
			}			
			if(naturalHolderObjectTO.getNationality() != null && naturalHolderObjectTO.getNationality().equals(countryResidence) &&
			   naturalHolderObjectTO.getIndResidence() != null && naturalHolderObjectTO.getIndResidence().equals(BooleanType.YES.getCode())){
			   personTO.setFlagNationalResident(true);
			}
			if(naturalHolderObjectTO.getNationality() != null && !naturalHolderObjectTO.getNationality().equals(countryResidence) &&
			   naturalHolderObjectTO.getIndResidence()!=null && naturalHolderObjectTO.getIndResidence().equals(BooleanType.YES.getCode())){
			   personTO.setFlagForeignResident(true);
			}
			if(naturalHolderObjectTO.getNationality()!=null && naturalHolderObjectTO.getNationality().equals(countryResidence) &&
			   naturalHolderObjectTO.getIndResidence()!=null && naturalHolderObjectTO.getIndResidence().equals(BooleanType.NO.getCode())){
			   personTO.setFlagNationalNotResident(true);
			}
			if(naturalHolderObjectTO.getNationality()!=null && !naturalHolderObjectTO.getNationality().equals(countryResidence) &&
			   naturalHolderObjectTO.getIndResidence()!=null && naturalHolderObjectTO.getIndResidence().equals(BooleanType.NO.getCode())){
			   personTO.setFlagForeignNotResident(true);
			}			
			Integer documentType = null;
			for(DocumentType document : DocumentType.list){
				if(document.name().equals(naturalHolderObjectTO.getDocumentTypeCode())){
				    	 documentType = document.getCode();
				    	 break;
				 }					
			}			
			RecordValidationType validation = validateFlowDocumenTypeByPersonType(personTO, documentType);
			if(validation != null){
				return validation;
			}		   
			
			// validando si es menor de edad
			if(Validations.validateIsNotNull(personObjectTO.getBirthDate())){
				if(CommonsUtilities.getYearsBetween(personObjectTO.getBirthDate(), new Date()) < 18){
					personTO.setFlagMinorIndicator(true);
					((NaturalHolderObjectTO) personObjectTO).setIndMinor(BooleanType.YES.getCode());
				}else{
					personTO.setFlagMinorIndicator(false);
				}
			}
			// issue 1233: se ha quitado la validacion del tag incapacidad porque no aplica en nuestro pais
		  /*if(personTO.isFlagMinorIndicator() || personTO.isFlagInabilityIndicator() ) {*/
			
			// Issue 1291: se ha quitado la validacion de representante legal para menor de edad
//			if(personTO.isFlagMinorIndicator()){				
//				if(Validations.validateListIsNullOrEmpty(holderAccountObjectTO.getLstLegalRepresentativeObjectTOs())){
//					return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_NECCESARY_LEGAL_REPRESENTATIVE, null);
//				}				
//				for(LegalRepresentativeObjectTO legalRepresentativeObjectTO : holderAccountObjectTO.getLstLegalRepresentativeObjectTOs()){
//					validation = validateInformationPerson(legalRepresentativeObjectTO);
//					if(validation != null){
//						return validation;
//					}
//				}
//			}
			
		//Si es titular Tipo Persona Juridica
		} else if (personObjectTO instanceof JuridicHolderObjectTO){			
			JuridicHolderObjectTO juridicHolderObjectTO = (JuridicHolderObjectTO) personObjectTO;			
			if(juridicHolderObjectTO.getNationality().equals(countryResidence) &&
				juridicHolderObjectTO.getIndResidence()!=null && juridicHolderObjectTO.getIndResidence().equals(BooleanType.YES.getCode())){
			   personTO.setFlagNationalResident(true);
			}
			if(juridicHolderObjectTO.getNationality()!=null && !juridicHolderObjectTO.getNationality().equals(countryResidence) &&
			  	juridicHolderObjectTO.getIndResidence()!=null && juridicHolderObjectTO.getIndResidence().equals(BooleanType.YES.getCode())){
			   personTO.setFlagForeignResident(true);
			}
			if(juridicHolderObjectTO.getNationality()!=null && juridicHolderObjectTO.getNationality().equals(countryResidence) &&
				juridicHolderObjectTO.getIndResidence()!=null && juridicHolderObjectTO.getIndResidence().equals(BooleanType.NO.getCode())){
			   personTO.setFlagNationalNotResident(true);
			}
			if(juridicHolderObjectTO.getNationality()!=null && !juridicHolderObjectTO.getNationality().equals(countryResidence) &&
			  	juridicHolderObjectTO.getIndResidence()!=null && juridicHolderObjectTO.getIndResidence().equals(BooleanType.NO.getCode())){
			   personTO.setFlagForeignNotResident(true);
			}
			
			RecordValidationType validation = validateFlowDocumenTypeByPersonType(personTO,juridicHolderObjectTO.getDocumentType());
			if(validation != null){
				return validation;
			}
			
			for(LegalRepresentativeObjectTO legalRepresentativeObjectTO : holderAccountObjectTO.getLstLegalRepresentativeObjectTOs()){
				validation = validateInformationPerson(legalRepresentativeObjectTO);
				if(validation!=null){
					return validation;
				}
			}			
		}
		return null;		
	}
		
	/**
	 * Carga Titular Historico.
	 *
	 * @param holderAccountObjectTO the holder account object to
	 * @param personObjectTO the person object to
	 */
	public void loadHolderHistory(HolderAccountObjectTO holderAccountObjectTO, PersonObjectTO personObjectTO){
		HolderRegisterTO holderRegisterTO = new HolderRegisterTO();
		holderRegisterTO.setParticipant(new Participant(holderAccountObjectTO.getIdParticipant()));
		holderRegisterTO.setHolderRequest(new HolderRequest());
		HolderHistory holderHistory = new HolderHistory();
		boolean populateRepresentatives = false;
		
		List<LegalRepresentativeObjectTO> lstLegalRepresentativeObjects = null;
		
		// issue 1239: si el tipo de carnet es CONSULAR se debe concatenar para obterner el nro de documento original
//		if(Validations.validateIsNotNull(holderAccountObjectTO.getSecurityObjectTO())
//				&& (Validations.validateIsNotNull(holderAccountObjectTO.getSecurityObjectTO().getSecurityClass().equals(SecurityClassType.BBX.getCode()))
//				    || Validations.validateIsNotNull(holderAccountObjectTO.getSecurityObjectTO().getSecurityClass().equals(SecurityClassType.BTX.getCode()))) ){
//			if(Validations.validateIsNotNull(personObjectTO.getDocumentType())
//					&& personObjectTO.getDocumentType().equals(DocumentType.DCC.getCode())){
//				String numberDocuemntOriginal="DCC"+personObjectTO.getDocumentNumber()+personObjectTO.getDocumentSource();
//				personObjectTO.setDocumentNumber(numberDocuemntOriginal);
//			}
//			if(Validations.validateIsNotNull(personObjectTO.getDocumentType())
//					&& personObjectTO.getDocumentType().equals(DocumentType.DCD.getCode())){
//				String numberDocuemntOriginal="DCD"+personObjectTO.getDocumentNumber()+personObjectTO.getDocumentSource();
//				personObjectTO.setDocumentNumber(numberDocuemntOriginal);
//			}
//			if(Validations.validateIsNotNull(personObjectTO.getDocumentType())
//					&& personObjectTO.getDocumentType().equals(DocumentType.DCR.getCode())){
//				String numberDocuemntOriginal="DCR"+personObjectTO.getDocumentNumber()+personObjectTO.getDocumentSource();
//				personObjectTO.setDocumentNumber(numberDocuemntOriginal);
//			}
//			
//			if(Validations.validateIsNotNull(personObjectTO.getDocumentType())
//					&& personObjectTO.getDocumentType().equals(DocumentType.CIE.getCode())){
//				
//				String numberDocuemntOriginal="E-"+personObjectTO.getDocumentNumber();
//				personObjectTO.setDocumentNumber(numberDocuemntOriginal);
//			}
//		}
		
		//Titular Tipo Natural
		if(personObjectTO instanceof NaturalHolderObjectTO){
			NaturalHolderObjectTO naturalHolderObjectTO = (NaturalHolderObjectTO) personObjectTO;
			holderHistory.setLegalResidenceCountry(naturalHolderObjectTO.getLegalResidenceCountry());		
			holderHistory.setLegalProvince(naturalHolderObjectTO.getLegalProvince());
			holderHistory.setPostalResidenceCountry(naturalHolderObjectTO.getLegalResidenceCountry());
			holderHistory.setPostalProvince(naturalHolderObjectTO.getLegalProvince());
			holderHistory.setBirthDate(naturalHolderObjectTO.getBirthDate());
			holderHistory.setDocumentNumber(naturalHolderObjectTO.getDocumentNumber());						
			holderHistory.setDocumentType(naturalHolderObjectTO.getDocumentType());						
			holderHistory.setEmail(naturalHolderObjectTO.getEmail());
			holderHistory.setFaxNumber(naturalHolderObjectTO.getFaxNumber());
			holderHistory.setFirstLastName(naturalHolderObjectTO.getFirstLastName());
			holderHistory.setHolderType(PersonType.NATURAL.getCode());
			holderHistory.setHomePhoneNumber(naturalHolderObjectTO.getHomePhoneNumber());
			holderHistory.setIndResidence(naturalHolderObjectTO.getIndResidence());						
			holderHistory.setLegalAddress(naturalHolderObjectTO.getLegalAddress());						
			holderHistory.setLegalDepartment(naturalHolderObjectTO.getLegalDepartment());		
			holderHistory.setLegalDistrict(naturalHolderObjectTO.getLegalDistrict());
			holderHistory.setLegalProvince(naturalHolderObjectTO.getLegalProvince());	
			holderHistory.setName(naturalHolderObjectTO.getName());
			holderHistory.setNationality(naturalHolderObjectTO.getNationality());						
			holderHistory.setOfficePhoneNumber(naturalHolderObjectTO.getOfficePhoneNumber());
			holderHistory.setPostalAddress(naturalHolderObjectTO.getLegalAddress());						
			holderHistory.setPostalDepartment(naturalHolderObjectTO.getLegalDepartment());
			holderHistory.setPostalDistrict(naturalHolderObjectTO.getLegalDistrict());
			holderHistory.setPostalProvince(naturalHolderObjectTO.getLegalProvince());	
			holderHistory.setPostalResidenceCountry(naturalHolderObjectTO.getLegalResidenceCountry());	
			holderHistory.setSecondDocumentNumber(naturalHolderObjectTO.getSecondDocumentNumber());						
			holderHistory.setSecondDocumentType(naturalHolderObjectTO.getSecondDocumentType());						
			holderHistory.setSecondLastName(naturalHolderObjectTO.getSecondLastName());
			holderHistory.setSecondNationality(naturalHolderObjectTO.getSecondNationality());
			holderHistory.setDocumentIssuanceDate(naturalHolderObjectTO.getDocumentIssuanceDate());
			if(Validations.validateIsNotNull(naturalHolderObjectTO.getSex())){
				if (naturalHolderObjectTO.getSex().equals(SexType.MALE.getCode())) {
					holderHistory.setSex(222);
				} else if (naturalHolderObjectTO.getSex().equals(
						SexType.FEMALE.getCode())) {
					holderHistory.setSex(395);
				} else {
					holderHistory.setSex(2306);
				}	
			}else{
				holderHistory.setSex(2306);
			}
			
			if(naturalHolderObjectTO.getIndDisabled()!=null){
				holderHistory.setIndDisabled(naturalHolderObjectTO.getIndDisabled());
			}else{
				holderHistory.setIndDisabled(BooleanType.NO.getCode());
			}
			
			if(naturalHolderObjectTO.getIndMinor()!=null){
				holderHistory.setIndMinor(naturalHolderObjectTO.getIndMinor());
			}else{
				holderHistory.setIndMinor(BooleanType.NO.getCode());
			}
			holderHistory.setIndPEP(BooleanType.NO.getCode());
			holderHistory.setDocumentSource(mapDocumentSource.get(naturalHolderObjectTO.getDocumentSource()));
			holderHistory.setCivilStatus(naturalHolderObjectTO.getCivilStatus());
			holderHistory.setMarriedLastName(naturalHolderObjectTO.getMarriedLastName());
			holderHistory.setIndCanNegotiate(naturalHolderObjectTO.getIndCanNegotiate());
			// issue 1233 quitando la validacion del indicador IndIncapacitado porque no aplica a nuetsro pais
//			if(holderHistory.getIndDisabled().equals(BooleanType.YES.getCode())
//			    	|| holderHistory.getIndMinor().equals(BooleanType.YES.getCode())){
			//Issue 1291 Modificacion Representante Legal para menor de Edad
//			if(holderHistory.getIndMinor().equals(BooleanType.YES.getCode())){
//				populateRepresentatives = true;
//			}
		}
		//Titular Tipo Juridico
		if(personObjectTO instanceof JuridicHolderObjectTO){
			JuridicHolderObjectTO juridicHolderObjectTO = (JuridicHolderObjectTO) personObjectTO;
			holderHistory.setLegalResidenceCountry(juridicHolderObjectTO.getLegalResidenceCountry());		
			holderHistory.setLegalProvince(juridicHolderObjectTO.getLegalProvince());
			holderHistory.setPostalResidenceCountry(juridicHolderObjectTO.getLegalResidenceCountry());
			holderHistory.setPostalProvince(juridicHolderObjectTO.getLegalProvince());
			holderHistory.setDocumentNumber(juridicHolderObjectTO.getDocumentNumber());						
			holderHistory.setDocumentType(juridicHolderObjectTO.getDocumentType());						
			holderHistory.setEconomicActivity(juridicHolderObjectTO.getEconomicActivity());		
			holderHistory.setEconomicSector(juridicHolderObjectTO.getEconomicSector());
			holderHistory.setEmail(juridicHolderObjectTO.getEmail());
			holderHistory.setFaxNumber(juridicHolderObjectTO.getFaxNumber());
			holderHistory.setFullName(juridicHolderObjectTO.getFullName());
			holderHistory.setHolderType(PersonType.JURIDIC.getCode());
			holderHistory.setIndResidence(juridicHolderObjectTO.getIndResidence());						
			holderHistory.setIndPEP(BooleanType.NO.getCode());
			holderHistory.setJuridicClass(juridicHolderObjectTO.getJuridicClass());
			holderHistory.setLegalAddress(juridicHolderObjectTO.getLegalAddress());						
			holderHistory.setLegalDepartment(juridicHolderObjectTO.getLegalDepartment());		
			holderHistory.setLegalDistrict(juridicHolderObjectTO.getLegalDistrict());
			holderHistory.setLegalProvince(juridicHolderObjectTO.getLegalProvince());	
			holderHistory.setLegalResidenceCountry(juridicHolderObjectTO.getLegalResidenceCountry());						
			holderHistory.setMobileNumber(juridicHolderObjectTO.getMobileNumber());
			holderHistory.setNationality(juridicHolderObjectTO.getNationality());						
			holderHistory.setOfficePhoneNumber(juridicHolderObjectTO.getOfficePhoneNumber());
			holderHistory.setPostalAddress(juridicHolderObjectTO.getLegalAddress());						
			holderHistory.setPostalDepartment(juridicHolderObjectTO.getLegalDepartment());
			holderHistory.setPostalDistrict(juridicHolderObjectTO.getLegalDistrict());
			holderHistory.setPostalProvince(juridicHolderObjectTO.getLegalProvince());	
			holderHistory.setPostalResidenceCountry(juridicHolderObjectTO.getLegalResidenceCountry());
			//Capturando el indicador desde el servicio web
			holderHistory.setIndCanNegotiate(personObjectTO.getIndCanNegotiate());
			
			populateRepresentatives = true;
		}
		
		if(populateRepresentatives){
			
			List<LegalRepresentativeHistory> lstLegalRepresentativeHistories = new ArrayList<LegalRepresentativeHistory>();
			LegalRepresentativeHistory legalRepresentativeHistory;
			if(Validations.validateListIsNotNullAndNotEmpty(holderAccountObjectTO.getLstLegalRepresentativeObjectTOs())){
				for (LegalRepresentativeObjectTO representativeTO : holderAccountObjectTO.getLstLegalRepresentativeObjectTOs()){
					legalRepresentativeHistory = new LegalRepresentativeHistory();
					
					legalRepresentativeHistory.setBirthDate(representativeTO.getBirthDate());
					legalRepresentativeHistory.setDocumentNumber(representativeTO.getDocumentNumber());
					legalRepresentativeHistory.setDocumentType(representativeTO.getDocumentType());
					legalRepresentativeHistory.setEmail(representativeTO.getEmail());
					legalRepresentativeHistory.setFaxNumber(representativeTO.getFaxNumber());
					legalRepresentativeHistory.setFirstLastName(representativeTO.getFirstLastName());
					legalRepresentativeHistory.setHomePhoneNumber(representativeTO.getHomePhoneNumber());
					legalRepresentativeHistory.setIndResidence(representativeTO.getIndResidence());
					legalRepresentativeHistory.setLegalAddress(representativeTO.getLegalAddress());
					legalRepresentativeHistory.setLegalDepartment(representativeTO.getLegalDepartment());
					legalRepresentativeHistory.setLegalDistrict(representativeTO.getLegalDistrict());
					legalRepresentativeHistory.setLegalProvince(representativeTO.getLegalProvince());
					legalRepresentativeHistory.setLegalResidenceCountry(representativeTO.getLegalResidenceCountry());
					legalRepresentativeHistory.setMobileNumber(representativeTO.getMobileNumber());
					legalRepresentativeHistory.setName(representativeTO.getName());
					legalRepresentativeHistory.setNationality(representativeTO.getNationality());
					legalRepresentativeHistory.setOfficePhoneNumber(representativeTO.getOfficePhoneNumber());
					legalRepresentativeHistory.setPersonType(PersonType.NATURAL.getCode());
					legalRepresentativeHistory.setPostalAddress(representativeTO.getLegalAddress());		
					legalRepresentativeHistory.setPostalDepartment(representativeTO.getLegalDepartment());
					legalRepresentativeHistory.setPostalDistrict(representativeTO.getLegalDistrict());
					legalRepresentativeHistory.setPostalProvince(representativeTO.getLegalProvince());
					legalRepresentativeHistory.setPostalResidenceCountry(representativeTO.getLegalResidenceCountry());
					legalRepresentativeHistory.setSecondDocumentNumber(representativeTO.getSecondDocumentNumber());
					legalRepresentativeHistory.setSecondDocumentType(representativeTO.getSecondDocumentType());
					legalRepresentativeHistory.setSecondLastName(representativeTO.getSecondLastName());
					legalRepresentativeHistory.setSecondNationality(representativeTO.getSecondNationality());
					legalRepresentativeHistory.setSex(representativeTO.getSex());
					legalRepresentativeHistory.setStateRepreHistory(LegalRepresentativeStateType.REGISTERED.getCode());
					legalRepresentativeHistory.setRegistryUser(loggerUser.getUserName());
					legalRepresentativeHistory.setDocumentSource(mapDocumentSource.get(representativeTO.getDocumentSource()));
					legalRepresentativeHistory.setIndPEP(BooleanType.NO.getCode());
					legalRepresentativeHistory.setRepreHistoryType(HolderRequestType.CREATION.getCode());
					legalRepresentativeHistory.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
					legalRepresentativeHistory.setRegistryUser(loggerUser.getUserName());
					legalRepresentativeHistory.setRepresentativeClass(RepresentativeClassType.LEGAL.getCode());
					legalRepresentativeHistory.setHolderRequest(holderRegisterTO.getHolderRequest());
					legalRepresentativeHistory.setDocumentIssuanceDate(representativeTO.getDocumentIssuanceDate());
					lstLegalRepresentativeHistories.add(legalRepresentativeHistory);
				}
				holderRegisterTO.setLstLegalRepresentativeHistories(lstLegalRepresentativeHistories);
			}
		}
		
		holderRegisterTO.setHolderHistory(holderHistory);
		holderRegisterTO.setPersonObjectTO(personObjectTO);
		lstHolderRegisterTOs.add(holderRegisterTO);
			
	}
		
	/**
	 * Valida Flujo tipo de documento por Tipo de person.
	 *
	 * @param person the person
	 * @param documentType the document type
	 * @return the record validation type
	 */
	public RecordValidationType validateFlowDocumenTypeByPersonType(PersonTO person, Integer documentType){		
		if(person.getPersonType() != null){			
			switch (PersonType.get(person.getPersonType())) {
		 		case NATURAL:
		    	if (!person.isFlagMinorIndicator() && !person.isFlagInabilityIndicator()) {
		    		if (person.isFlagNationalResident()){					
						if (!documentType.equals(DocumentType.CI.getCode()) || !documentType.equals(DocumentType.CID.getCode())){								
							return CommonsUtilities.populateRecordCodeValidation(null,
									InterfaceLabel.TIPO_DOCUMENTO.toString(),
									PropertiesUtilities.getMessage(PropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_NOT_MATCH));
						}
					} else if(person.isFlagNationalNotResident()){
						if (!documentType.equals(DocumentType.CI.getCode())
							|| !documentType.equals(DocumentType.CID.getCode())
							|| !documentType.equals(DocumentType.PAS.getCode())
							|| !documentType.equals(DocumentType.CIEE.getCode())){						
							return CommonsUtilities.populateRecordCodeValidation(null,
									InterfaceLabel.TIPO_DOCUMENTO.toString(),
									PropertiesUtilities.getMessage(PropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_NOT_MATCH));
						}								
					} else if (person.isFlagForeignResident()) {
						if (!documentType.equals(DocumentType.CIE.getCode())
								|| !documentType.equals(DocumentType.DCDL.getCode())
								|| !documentType.equals(DocumentType.DCD.getCode())	
								|| !documentType.equals(DocumentType.DCRA.getCode())
								|| !documentType.equals(DocumentType.DCRB.getCode())	
								|| !documentType.equals(DocumentType.DLC.getCode())		
								|| !documentType.equals(DocumentType.DCC.getCode()) 
								|| !documentType.equals(DocumentType.DCR.getCode())){
							return CommonsUtilities.populateRecordCodeValidation(null,
									InterfaceLabel.TIPO_DOCUMENTO.toString(),
									PropertiesUtilities.getMessage(PropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_NOT_MATCH));
						}
					} else if (person.isFlagForeignNotResident()) {
						if (!documentType.equals(DocumentType.DIO.getCode())) {
							return CommonsUtilities.populateRecordCodeValidation(null,
									InterfaceLabel.TIPO_DOCUMENTO.toString(),
									PropertiesUtilities
									.getMessage(PropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_NOT_MATCH));
						}
					}
			    } else if (person.isFlagMinorIndicator()) {
					if (person.isFlagNationalResident()) {
						if (!documentType.equals(DocumentType.CI.getCode())
							  || !documentType.equals(DocumentType.CID.getCode()) 
							  || !documentType.equals(DocumentType.CDN.getCode())){
							return CommonsUtilities.populateRecordCodeValidation(null,
									InterfaceLabel.TIPO_DOCUMENTO.toString(),
									PropertiesUtilities
									.getMessage(PropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_NOT_MATCH));
						}					
					} else if (person.isFlagNationalNotResident()) {				
						if (!documentType.equals(DocumentType.CI.getCode())
										|| !documentType.equals(DocumentType.CID.getCode())
										|| !documentType.equals(DocumentType.PAS.getCode())
										|| !documentType.equals(DocumentType.CDN.getCode())) {
						
							return CommonsUtilities.populateRecordCodeValidation(null,
									InterfaceLabel.TIPO_DOCUMENTO.toString(),
									PropertiesUtilities
									.getMessage(PropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_NOT_MATCH));
						}				
					} else if (person.isFlagForeignResident()) {							
						if (!documentType.equals(DocumentType.CIE.getCode())) {
							return CommonsUtilities.populateRecordCodeValidation(null,
									InterfaceLabel.TIPO_DOCUMENTO.toString(),
									PropertiesUtilities
									.getMessage(PropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_NOT_MATCH));
						}
					}
					else if(person.isFlagForeignNotResident()){
						if (!documentType.equals(DocumentType.DIO.getCode())) {
							return CommonsUtilities.populateRecordCodeValidation(null,
									InterfaceLabel.TIPO_DOCUMENTO.toString(),
									PropertiesUtilities
									.getMessage(PropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_NOT_MATCH));
						}
					}
			    }
			    else if (person.isFlagInabilityIndicator()) {				
					if (person.isFlagNationalResident()) {
							if (!documentType.equals(DocumentType.CI.getCode())
										|| !documentType.equals(DocumentType.CID.getCode())){									
								return CommonsUtilities.populateRecordCodeValidation(null,
											InterfaceLabel.TIPO_DOCUMENTO.toString(),
											PropertiesUtilities
											.getMessage(PropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_NOT_MATCH));
							}							
					} else if (person.isFlagNationalNotResident()) {						
							if (!documentType.equals(DocumentType.CI.getCode())
								    || !documentType.equals(DocumentType.CID.getCode())											
									|| !documentType.equals(DocumentType.PAS.getCode())								
									|| !documentType.equals(DocumentType.CIEE.getCode())) {
	
								return CommonsUtilities.populateRecordCodeValidation(null,
										InterfaceLabel.TIPO_DOCUMENTO.toString(),
										PropertiesUtilities
										.getMessage(PropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_NOT_MATCH));
								}
				   }				
					else if (person.isFlagForeignResident()) {				
						if (!documentType.equals(DocumentType.CIE.getCode())
									|| !documentType.equals(DocumentType.DCDL.getCode())
									|| !documentType.equals(DocumentType.DCD.getCode())
									|| !documentType.equals(DocumentType.DCRA.getCode())
									|| !documentType.equals(DocumentType.DCRB.getCode())		
									|| !documentType.equals(DocumentType.DLC.getCode())
									|| !documentType.equals(DocumentType.DCC.getCode())		
									|| !documentType.equals(DocumentType.DCR.getCode())) {
		
							return CommonsUtilities.populateRecordCodeValidation(null,
									InterfaceLabel.TIPO_DOCUMENTO.toString(),
									PropertiesUtilities
									.getMessage(PropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_NOT_MATCH));
							}

					} else if(person.isFlagForeignNotResident()){
					   if (!documentType.equals(DocumentType.DIO.getCode())) {
							return CommonsUtilities.populateRecordCodeValidation(null,
									InterfaceLabel.TIPO_DOCUMENTO.toString(),
									PropertiesUtilities
									.getMessage(PropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_NOT_MATCH));
					   }
					}													
			    }		   
		    	break;
			case JURIDIC:				
				if (person.isFlagNationalResident()) {
					
					if (!documentType.equals(DocumentType.RUC.getCode())){
						return CommonsUtilities.populateRecordCodeValidation(null,
								InterfaceLabel.TIPO_DOCUMENTO.toString(),
								PropertiesUtilities
								.getMessage(PropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_NOT_MATCH));
					}
				}				
				else if (person.isFlagNationalNotResident()) {
					//NOTHING
				}
				else if (person.isFlagForeignResident()) {
					if (!documentType.equals(DocumentType.RUC.getCode())){
						return CommonsUtilities.populateRecordCodeValidation(null,
								InterfaceLabel.TIPO_DOCUMENTO.toString(),
								PropertiesUtilities
								.getMessage(PropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_NOT_MATCH));
					}
				} 
				else if(person.isFlagForeignNotResident()){
					if (!documentType.equals(DocumentType.EE.getCode())){
						return CommonsUtilities.populateRecordCodeValidation(null,
								InterfaceLabel.TIPO_DOCUMENTO.toString(),
								PropertiesUtilities
								.getMessage(PropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_NOT_MATCH));
					}					
				}
				
			default:				
				break;
					}
			}
		return null;							
	}
	
	/**
	 * Metodo que valida Formato de Numero de documento.
	 *
	 * @param documentType the document type
	 * @param documentNumber the document number
	 * @return the record validation type
	 */
	private RecordValidationType validateFormatDocumentNumber(Integer documentType, String documentNumber){
		
		String propertiesConstantsDocNumLenght15 = PropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_15_DIGITS;
		String propertiesConstantsDocNumLenght17 = PropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_17_DIGITS;
		String propertiesConstantsDocNumLenght6_20 = PropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_6_20_DIGITS;
		String propertiesConstantsDocNumLenght6_15 = PropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_6_15_DIGITS;
		String propertiesConstantsDocCountThreeZeroValid = PropertiesConstants.ERROR_VALIDATOR_DOCUMENT_COUNT_THREE_ZERO_VALID;
		String propertiesConstantsDocNumLenght12 = PropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_12_DIGITS;	
		String propertiesConstantsDocNumLenght10 = PropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_10_DIGITS;
		
		if(Validations.validateIsNotNullAndNotEmpty(documentNumber)){			
			Object [] bodyData;
			bodyData = new Object[1];
			bodyData[0] = DocumentType.get(documentType).getValue();
			
			if(DocumentType.CI.getCode().equals(documentType) || DocumentType.CIEE.getCode().equals(documentType) || DocumentType.CIE.getCode().equals(documentType)){				
				
				if(documentNumber.length() > loadMaxLenghtType(documentType).getMaxLenghtDocumentNumber()) {
					return CommonsUtilities.populateRecordCodeValidation(null,
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_NUMBER,
							null);
				} else {
					
					RecordValidationType recordValidationType = validateHyphenDocumentNumber(documentType, documentNumber);	
					if(recordValidationType != null){
						return recordValidationType;
					}																	
					if(isNumeric(documentNumber)){
						
						RecordValidationType validation = validateAllZero(documentNumber);
						if(validation != null){
							return validation;
						}						
					}
				}
			
			} else if(DocumentType.CID.getCode().equals(documentType)
					|| DocumentType.DCDL.getCode().equals(documentType)
					|| DocumentType.DCD.getCode().equals(documentType)
					|| DocumentType.DCRA.getCode().equals(documentType)
					|| DocumentType.DCRB.getCode().equals(documentType)
					|| DocumentType.DLC.getCode().equals(documentType)
					|| DocumentType.DCC.getCode().equals(documentType)
					|| DocumentType.DCR.getCode().equals(documentType)){
					
				if(documentNumber.length() > loadMaxLenghtType(documentType).getMaxLenghtDocumentNumber()) {
					return CommonsUtilities.populateRecordCodeValidation(null,
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_NUMBER,
							propertiesConstantsDocNumLenght12);
				} else if(isNumeric(documentNumber)){
					
					RecordValidationType validation = validateAllZero(documentNumber);
					if(validation != null){
						return validation;
					}
										
				}
			} else if(DocumentType.PAS.getCode().equals(documentType)){
				if(documentNumber.length() < 6 || documentNumber.length() > 20) {
					return CommonsUtilities.populateRecordCodeValidation(null,
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_NUMBER,
							propertiesConstantsDocNumLenght6_20);				
				}
				else if(!validateCountZeroDocumentNumber(documentType,documentNumber)){
					return CommonsUtilities.populateRecordCodeValidation(null,
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_NUMBER,
							propertiesConstantsDocCountThreeZeroValid);
				
				}
				else if(isNumeric(documentNumber)){
					RecordValidationType validation = validateAllZero(documentNumber);
					if(validation != null){
						return validation;
					}										
				}

			} else if(DocumentType.RUC.getCode().equals(documentType)){
				if(documentNumber.length() != 10){
					return CommonsUtilities.populateRecordCodeValidation(null,
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_NUMBER,
							propertiesConstantsDocNumLenght10);
				}
				else if(isNumeric(documentNumber)){
					RecordValidationType validation = validateAllZero(documentNumber);
					if(validation != null){
						return validation;
					}		
					
					validation = validateHyphenDocumentNumber(documentType, documentNumber);	
					if(validation != null){
						return validation;
					}		
				}
			} else if(DocumentType.DIO.getCode().equals(documentType) || DocumentType.CDN.getCode().equals(documentType)){
				if(documentNumber.length() < 6 || documentNumber.length() > 15) {
					return CommonsUtilities.populateRecordCodeValidation(null,
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_NUMBER,
							propertiesConstantsDocNumLenght6_15);
				} else if(!validateCountZeroDocumentNumber(documentType, documentNumber)){
					return CommonsUtilities.populateRecordCodeValidation(null,
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_NUMBER,
							propertiesConstantsDocCountThreeZeroValid);
				} else if(isNumeric(documentNumber)){
					RecordValidationType validation = validateAllZero(documentNumber);
					if(validation != null){
						return validation;
					}										
				}
				
			} else if(DocumentType.EE.getCode().equals(documentType)){
				if(documentNumber.length() > loadMaxLenghtType(documentType).getMaxLenghtDocumentNumber() ) {
					return CommonsUtilities.populateRecordCodeValidation(null,
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_NUMBER,
							propertiesConstantsDocNumLenght15);
				} else if(documentNumber.length() <= loadMaxLenghtType(documentType).getMaxLenghtDocumentNumber()){
					String propertiesConstantsDocCountZeroValid = PropertiesConstants.ERROR_VALIDATOR_DOCUMENT_COUNT_ONE_ZERO_VALID;
					String charDocumentNumber = String.valueOf(documentNumber.charAt(0));						
					if(charDocumentNumber.equals(String.valueOf(0))){
							return CommonsUtilities.populateRecordCodeValidation(null,
									GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_NUMBER,
									propertiesConstantsDocCountZeroValid);
					 }
				}
			}
		}
		return null;		
	}
	
	/**
	 * Metodo que Valida la cantidad de ceros en el numero de Documento.
	 *
	 * @param documentType the document type
	 * @param documentNumber the document number
	 * @return true, if successful
	 */
	public boolean validateCountZeroDocumentNumber(Integer documentType, String documentNumber){
		int count = 0;
		boolean flag = true;
				
		if(DocumentType.DIO.getCode().equals(documentType) || DocumentType.PAS.getCode().equals(documentType)){
			
			for(int i=0;i<documentNumber.length();i++){
				
				String charDocumentNumber = String.valueOf(documentNumber.charAt(i));
				if(charDocumentNumber.equals(String.valueOf(0))){
					count++;
					
					if(count<=3 && i<=2){
						flag = true;
					}
					else if (count>3 && i==3){
						flag = false;
						i=documentNumber.length();
					}
				}
				
			}		
			
		}
		return flag;
	}
	
	/**
	 * Validate all zero.
	 *
	 * @param documentNumber the document number
	 * @return the record validation type
	 */
	public RecordValidationType validateAllZero(String documentNumber){
		String propertiesConstantsIsNotAllZero = PropertiesConstants.ERROR_VALIDATOR_IS_NOT_ALL_ZERO;		
	   	Object [] bodyData;
	   	bodyData = new Object[0];		
		if(Long.parseLong(documentNumber) == 0L){
			return CommonsUtilities.populateRecordCodeValidation(null,
					InterfaceLabel.TIPO_DOCUMENTO.toString(), 
					PropertiesUtilities.getMessage(propertiesConstantsIsNotAllZero,bodyData));
		}
		return null;		
	}
	
	/**
	 * Metodo que valida Guion en el numero de documento.
	 *
	 * @param documentType the document type
	 * @param documentNumber the document number
	 * @return the record validation type
	 */
	public RecordValidationType validateHyphenDocumentNumber(Integer documentType, String documentNumber){
		
		int countHyphen = 0;
		boolean flagOk = true;
		int position = 0;
		Object[] bodyData;
		String charDocumentNumber="";
		int extensionMax=1;
		int extensionMin=1;
		
		Integer numericMin9=9;
		
		String propertiesConstantsDocNumHyphen=PropertiesConstants.ERROR_VALIDATOR_NUMBER_CHARACTER_HYPHEN;
		String propertiesConstantsDocNumMaxLength=PropertiesConstants.ERROR_VALIDATOR_NUMBER_MAX_LENGTH;
		String propertiesConstantsDocExtensionMaxLength=PropertiesConstants.ERROR_VALIDATOR_EXTENSION_MAX_LENGTH;
		String propertiesConstantsDocEstensionMinLength=PropertiesConstants.ERROR_VALIDATOR_EXTENSION_MIN_LENGTH;
		String propertiesConstantsIsNumeric=PropertiesConstants.ERROR_VALIDATOR_IS_NUMERIC;
		
		bodyData = new Object[1];
		bodyData[0] = DocumentType.get(documentType).getValue();
		
		
		if(DocumentType.CI.getCode().equals(documentType)
				|| DocumentType.CIEE.getCode().equals(documentType)
				|| DocumentType.RUC.getCode().equals(documentType)){
			
			for(int i=0;i<documentNumber.length();i++){
				//si tiene mas de cero Guiones muestra mensaje de error
				charDocumentNumber = String.valueOf(documentNumber.charAt(i));
				if(charDocumentNumber.equals(GeneralConstants.HYPHEN)){
					countHyphen++;	
					if(countHyphen > 1 && DocumentType.RUC.getCode().equals(documentType)) {
						flagOk = false;
						return CommonsUtilities.populateRecordCodeValidation(null,
								GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_NUMBER,
								propertiesConstantsDocNumHyphen);
					}else if(countHyphen > 0){
						flagOk = false;
						return CommonsUtilities.populateRecordCodeValidation(null,
								GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_NUMBER,
								propertiesConstantsDocNumHyphen);
					}else if (countHyphen == 0 && DocumentType.RUC.getCode().equals(documentType)) {
						flagOk = false;
						return CommonsUtilities.populateRecordCodeValidation(null,
								GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_NUMBER,
								propertiesConstantsDocNumHyphen);
					} else{
						position = i;
					}
				}
				
			}
			
			if(countHyphen==1 && flagOk){
				if(position>numericMin9){
					return CommonsUtilities.populateRecordCodeValidation(null,
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_NUMBER,
							propertiesConstantsDocNumMaxLength);
				}			
				else if(documentNumber.substring(position+1, documentNumber.length()).length()>extensionMax){
					return CommonsUtilities.populateRecordCodeValidation(null,
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_NUMBER,
							PropertiesUtilities
							.getMessage(propertiesConstantsDocExtensionMaxLength,bodyData));
				}
				else if(documentNumber.substring(position+1, documentNumber.length()).length()<extensionMin){
					return CommonsUtilities.populateRecordCodeValidation(null,
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_NUMBER,
							propertiesConstantsDocEstensionMinLength);
				}				
				else if(isNumeric(documentNumber.substring(0,documentNumber.indexOf("-")))){
					RecordValidationType validation = validateAllZero(documentNumber.substring(0,documentNumber.indexOf("-")));
					if(validation!=null){
						return validation;
					}
				} else {				
					return CommonsUtilities.populateRecordCodeValidation(null,
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_NUMBER,
							propertiesConstantsIsNumeric);
					
				}		

			}
			
			if(countHyphen == 0){
				if(DocumentType.RUC.getCode().equals(documentType)) {
					return CommonsUtilities.populateRecordCodeValidation(null,
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_NUMBER,
							propertiesConstantsDocNumHyphen);
				}
				if(documentNumber.length() > numericMin9){
					return CommonsUtilities.populateRecordCodeValidation(null,
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_NUMBER,
						null);
				} else if(isNumeric(documentNumber)){
					RecordValidationType validation = validateAllZero(documentNumber);
					if(validation != null){
						return validation;
					}
				} else {
					return CommonsUtilities.populateRecordCodeValidation(null,
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_NUMBER,
							propertiesConstantsIsNumeric);
				}		
			}		
		}
		return null;		
	
	}
	
	/**
	 * Checks if is numeric.
	 *
	 * @param s the s
	 * @return true, if is numeric
	 */
	public boolean isNumeric(String s) {
	    try { 
	        Long.parseLong(s); 
	    } catch(NumberFormatException e) { 
	        return false; 
	    }
	    return true;
	}

	/**
	 * Metodo para cargar maximo tamaioo por tipo de documento.
	 *
	 * @param documentType the document type
	 * @return the document to
	 */
	public DocumentTO loadMaxLenghtType(Integer documentType){
		
		DocumentTO documentTO = new DocumentTO();
		
		if(documentType!=null && documentType.equals(DocumentType.CI.getCode())){
			documentTO.setNumericTypeDocument(false);
			documentTO.setMaxLenghtDocumentNumber(7);
		}else if (documentType!=null && documentType.equals(DocumentType.CID.getCode())) {
			documentTO.setNumericTypeDocument(true);
			documentTO.setMaxLenghtDocumentNumber(12);
		}else if (documentType!=null && documentType.equals(DocumentType.PAS.getCode())) {
			documentTO.setNumericTypeDocument(false);
			documentTO.setMaxLenghtDocumentNumber(20);
		}else if (documentType!=null && documentType.equals(DocumentType.CIEE.getCode())) {
			documentTO.setNumericTypeDocument(false);
			documentTO.setMaxLenghtDocumentNumber(17);	
		} else if (documentType!=null && documentType.equals(DocumentType.DIO.getCode())) {
			documentTO.setNumericTypeDocument(false);
			documentTO.setMaxLenghtDocumentNumber(15);
		}else if (documentType!=null && documentType.equals(DocumentType.CIE.getCode())) {
			documentTO.setNumericTypeDocument(false);
			documentTO.setMaxLenghtDocumentNumber(17);				
		}else if(documentType!=null && documentType.equals(DocumentType.DCDL.getCode())) {
			documentTO.setNumericTypeDocument(true);
			documentTO.setMaxLenghtDocumentNumber(12);		
		}else if(documentType!=null && documentType.equals(DocumentType.DCD.getCode())) {
			documentTO.setNumericTypeDocument(true);
			documentTO.setMaxLenghtDocumentNumber(12);		
		}else if(documentType!=null && documentType.equals(DocumentType.DCRA.getCode())) {
			documentTO.setNumericTypeDocument(true);
			documentTO.setMaxLenghtDocumentNumber(12);
		}else if(documentType!=null && documentType.equals(DocumentType.DCRB.getCode())) {
			documentTO.setNumericTypeDocument(true);
			documentTO.setMaxLenghtDocumentNumber(12);
		}else if(documentType!=null && documentType.equals(DocumentType.DLC.getCode())) {
			documentTO.setNumericTypeDocument(true);
			documentTO.setMaxLenghtDocumentNumber(12);	
		}else if(documentType!=null && documentType.equals(DocumentType.DCC.getCode())) {
			documentTO.setNumericTypeDocument(true);
			documentTO.setMaxLenghtDocumentNumber(12);
		}else if(documentType!=null && documentType.equals(DocumentType.DCR.getCode())) {
			documentTO.setNumericTypeDocument(true);
			documentTO.setMaxLenghtDocumentNumber(12);	
		} else if(documentType!=null && documentType.equals(DocumentType.CDN.getCode())) {
			documentTO.setNumericTypeDocument(false);
			documentTO.setMaxLenghtDocumentNumber(15);
		} else if(documentType!=null && documentType.equals(DocumentType.RUC.getCode())){
			documentTO.setNumericTypeDocument(true);
			documentTO.setMaxLenghtDocumentNumber(10);		
		}else if(documentType!=null && documentType.equals(DocumentType.EE.getCode())){
			documentTO.setNumericTypeDocument(false);
			documentTO.setMaxLenghtDocumentNumber(15);
		}
		

		return documentTO;
	}
	
	/**
	 * Metodo para validar informacion de la persona.
	 *
	 * @param personObjectTO the person object to
	 * @return the record validation type
	 */
	private RecordValidationType validateInformationPerson(PersonObjectTO personObjectTO){
		
		ParameterTableTO objParameterTableTO = new ParameterTableTO();
		
		// validate document type (DPF uses short integer parameter)
		objParameterTableTO.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
		if(personObjectTO.isDpfInterface()){
			objParameterTableTO.setShortInteger(personObjectTO.getDocumentType());
		} else {
			objParameterTableTO.setIndicator1(personObjectTO.getDocumentTypeCode());
		}
		
		List<ParameterTable> lstDocumentTypes = parameterServiceBean.getListParameterTableServiceBean(objParameterTableTO);			
		if(Validations.validateListIsNotNullAndNotEmpty(lstDocumentTypes)){	
			ParameterTable objParameterTable = lstDocumentTypes.get(0);				
			personObjectTO.setDocumentType(objParameterTable.getParameterTablePk());
			personObjectTO.setDocumentTypeCode(objParameterTable.getIndicator1());
		} else {							
			return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_TYPE, null);
		}
		
		// Quitando validacion de requerido issue 1284
		// Validar el expedido, si el tipo de documento es: cedula de identidad, verificar el document source sea diferente de nulo
		/*if(DocumentType.CI.getCode().equals(personObjectTO.getDocumentType())){
			if(Validations.validateIsNullOrEmpty(personObjectTO.getDocumentSource())){
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_SOURCE, null);
			}
		}*/
		
		// validate document number format
		if(personObjectTO.getDocumentNumber() != null){
			RecordValidationType recordValidationType = validateFormatDocumentNumber(personObjectTO.getDocumentType(), personObjectTO.getDocumentNumber());
			if(recordValidationType != null){
				return recordValidationType;
			}
		}
		
		// Validando el expedido, si el tipo de documento es:DCD,DCC,DCR o DCO
		if (DocumentType.DCD.getCode().equals(personObjectTO.getDocumentType())
				|| DocumentType.DCC.getCode().equals(
						personObjectTO.getDocumentType())
				|| DocumentType.DCR.getCode().equals(
						personObjectTO.getDocumentType())) {
			//Validando que el numero de documento sea solo numerico
			if(!isNumeric(personObjectTO.getDocumentNumber())){
				return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_NUMBER,null);
			}
			// Verificar el document source sea diferente de nulo
			if (Validations.validateIsNullOrEmpty(personObjectTO.getDocumentSource())) {
				return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_SOURCE,null);
			}
			// Verificando que el document source tenga 4 digitos
			if (personObjectTO.getDocumentSource().length() != 4) {
				return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_SOURCE,null);
			}
			// Verificando que document source sea numerico
			try {
				Integer.parseInt(personObjectTO.getDocumentSource());
			} catch (NumberFormatException nfe) {
				return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_SOURCE,null);
			}
			personObjectTO.setDocumentIssuanceDate(Integer.parseInt(personObjectTO.getDocumentSource()));

			// Verificando que document source sea menor he igual al anio actual
			if (personObjectTO.getDocumentIssuanceDate() > CommonsUtilities.currentYear()) {
				return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_SOURCE,null);
			}
			personObjectTO.setDocumentSource(null);

			// Validando que el cod duplicado este vacio
			if (!personObjectTO.getDuplicateCod().equals(GeneralConstants.HYPHEN)) {
				return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_DOCUMENT_DUPLICATE_CODE,null);
			}
			String newDocumentNumber = personObjectTO.getDocumentNumber();

			if (personObjectTO.getDocumentType().equals(DocumentType.DCD.getCode())) {
				newDocumentNumber = "DCD" + personObjectTO.getDocumentNumber();
			}
			if (personObjectTO.getDocumentType().equals(DocumentType.DCC.getCode())) {
				newDocumentNumber = "DCC" + personObjectTO.getDocumentNumber();
			}
			if (personObjectTO.getDocumentType().equals(DocumentType.DCR.getCode())) {
				newDocumentNumber = "DCR" + personObjectTO.getDocumentNumber();
			}

			if (personObjectTO.getDocumentIssuanceDate() != null) {

				if (personObjectTO.getDocumentType().equals(DocumentType.DCD.getCode())
						|| personObjectTO.getDocumentType().equals(
								DocumentType.DCC.getCode())
						|| personObjectTO.getDocumentType().equals(
								DocumentType.DCR.getCode())) {

					newDocumentNumber = newDocumentNumber
							+ personObjectTO.getDocumentIssuanceDate() + "";
					personObjectTO.setDocumentNumber(newDocumentNumber);
				}
			}
		}
		
		//Solo para el tipo de documento CIE
		if(personObjectTO.getDocumentType().equals(DocumentType.CIE.getCode())){
			// Validando que la extension este vacio
			if (Validations.validateIsNotNullAndNotEmpty(personObjectTO.getDocumentSource())) {
				return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_SOURCE,null);
			}
			personObjectTO.setDocumentNumber("E-" + personObjectTO.getDocumentNumber());
		}	

		// Solo para el tipo de documento CIEE
		if (personObjectTO.getDocumentType().equals(DocumentType.CIEE.getCode())) {
			// Validando que la extension este vacio
			if (Validations.validateIsNotNullAndNotEmpty(personObjectTO.getDocumentSource())) {
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_SOURCE, null);
			}
		}
		
		// validate duplicate code
		if(personObjectTO.getDuplicateCod() != null){
			if(!personObjectTO.getDuplicateCod().equals(GeneralConstants.HYPHEN)){
				RecordValidationType recordValidationType = validateFormatDuplicateCode(personObjectTO.getDuplicateCod());
				if(recordValidationType != null){
					return recordValidationType;
				}
				personObjectTO.setDocumentNumber(personObjectTO.getDocumentNumber() + personObjectTO.getDuplicateCod());
			}			
		}
		
		if(Validations.validateIsNullOrEmpty(personObjectTO.getNationalityCode())){
			personObjectTO.setNationalityCode("BO");
		}
		
		// validate nationality, it is always 2-string code
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("codeGeographicLocPrm", personObjectTO.getNationalityCode());
		params.put("typeGeographicLocPrm", GeographicLocationType.COUNTRY.getCode());
		GeographicLocation geographicLocation = null;
		try {
			geographicLocation = findObjectByNamedQuery(GeographicLocation.FIND_BY_CODE_GEO_LOC, params);		
			if(geographicLocation == null){
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_NATIONALITY, null);
			}
		} catch(NoResultException ex){
			return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_NATIONALITY, null);
		}				
		personObjectTO.setNationality(geographicLocation.getIdGeographicLocationPk());
		
		// validate legal residence country, it is always 2-string code
		if(!personObjectTO.getNationalityCode().equals(personObjectTO.getLegalResidenceCountryCode())){			
			params=new HashMap<String, Object>();
			params.put("codeGeographicLocPrm", personObjectTO.getLegalResidenceCountryCode());
			params.put("typeGeographicLocPrm", GeographicLocationType.COUNTRY.getCode());			
			try {
				geographicLocation = findObjectByNamedQuery(GeographicLocation.FIND_BY_CODE_GEO_LOC, params);			
				if(geographicLocation == null){
					return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_RESIDENCE_COUNTRY, null);
				}
			} catch(NoResultException ex){
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_RESIDENCE_COUNTRY, null);
			}			
			
		}
		personObjectTO.setLegalResidenceCountry(geographicLocation.getIdGeographicLocationPk());		
		
		if(personObjectTO instanceof JuridicHolderObjectTO){
			
			// we validate juridic class			
			JuridicHolderObjectTO juridicHolderObjectTO = (JuridicHolderObjectTO) personObjectTO;			
			objParameterTableTO = new ParameterTableTO();
			objParameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			objParameterTableTO.setMasterTableFk(MasterTableType.T_CLASE_JURIDICO_TITULAR.getCode());
			if(personObjectTO.isDpfInterface()){
				objParameterTableTO.setIndicator6(juridicHolderObjectTO.getJuridicClass());
			} else {
				objParameterTableTO.setParameterTablePk(juridicHolderObjectTO.getJuridicClass());
			}
			
			List<ParameterTable> lstJuridicClasses = parameterServiceBean.getListParameterTableServiceBean(objParameterTableTO);			
			if(Validations.validateListIsNotNullAndNotEmpty(lstJuridicClasses)){	
				ParameterTable objParameterTable = lstJuridicClasses.get(0);				
				juridicHolderObjectTO.setJuridicClass(objParameterTable.getParameterTablePk());
			} else {
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_JURIDIC_CLASS, null);
			}
		
			// validate economic sector
			if(juridicHolderObjectTO.getEconomicSector() != null){
				objParameterTableTO = new ParameterTableTO();
				objParameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
				objParameterTableTO.setMasterTableFk(MasterTableType.ECONOMIC_SECTOR.getCode());
				if(personObjectTO.isDpfInterface()){
					objParameterTableTO.setIndicator6(juridicHolderObjectTO.getEconomicSector());
				} else {
					objParameterTableTO.setParameterTablePk(juridicHolderObjectTO.getEconomicSector());
				}				
				List<ParameterTable> lstEconomicSector = parameterServiceBean.getListParameterTableServiceBean(objParameterTableTO);	
				if(Validations.validateListIsNotNullAndNotEmpty(lstEconomicSector)){	
					ParameterTable objParameterTable = lstEconomicSector.get(0);				
					juridicHolderObjectTO.setEconomicSector(objParameterTable.getParameterTablePk());
				} else {
					return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_ECONOMIC_SECTOR, null);
				}
			}
			ParameterTable economicActivity = null;
			// validate economic activity based on economic sector
			if(juridicHolderObjectTO.getEconomicSector() != null && juridicHolderObjectTO.getEconomicActivity() != null){
				Integer ecActvy = null;
				Integer ecActvyNum = null;
				if(personObjectTO.isDpfInterface()){
					ecActvyNum = juridicHolderObjectTO.getEconomicActivity();
				} else {
					ecActvy = juridicHolderObjectTO.getEconomicActivity();
				}				
				List<ParameterTable> lstEconomicActivities = parameterServiceBean.getListEconomicActivityBySector(
						juridicHolderObjectTO.getEconomicSector(), ecActvyNum, ecActvy);				
				if(Validations.validateListIsNotNullAndNotEmpty(lstEconomicActivities)){	
					economicActivity = lstEconomicActivities.get(0);				
					juridicHolderObjectTO.setEconomicActivity(economicActivity.getParameterTablePk());
				} else {
					return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_ECONOMIC_SECTOR, null);
				}
			}	
			
			// validando sector economico, para poder flagear al Titular con la opcion de poder Negociar indicador: indCanNegotiate
            if(economicActivity != null){
                  if(economicActivity.getIndicator6() != null){
                        switch (economicActivity.getIndicator6()) {
                        //case  2: // INTERMEDIACION FINANCIERA - ADMINISTRADORAS DE FONDOS DE PENSIONES
                        case  3: // INTERMEDIACION FINANCIERA - AGENCIAS DE BOLSA
                        case  8: // INTERMEDIACION FINANCIERA - COMPANIAS DE SEGUROS
                        case 15: // INTERMEDIACION FINANICERA - FONDOS DE INVERSION ABIERTO
                        case 16: // INTERMEDIACION FINANICERA - FONDOS DE INVERSION CERRADO
                        case 17: // INTERMEDIACION FINANICERA - FONDOS DE INVERSION MIXTOS
                        case 29: // INTERMEDIACION FINANICERA - SOCIEDADES ADMINISTRADORAS DE FONDOS DE INVERSION

                             // Colocar el flag NO puede Negociar Cartera Propia
                              personObjectTO.setIndCanNegotiate(0);
                             
                             break;

                        default:
                             // Colocar el flag SI puede Negociar Cartera Propia
                              personObjectTO.setIndCanNegotiate(1);
                             break;
                        }
                  }
            }

		}
		
		if(personObjectTO instanceof NaturalHolderObjectTO || personObjectTO instanceof LegalRepresentativeObjectTO){
			
			//validate second document type only when holder is natural and number is different from first document number 
			if(personObjectTO.getSecondDocumentType() != null){
				objParameterTableTO = null;
				if(personObjectTO.isDpfInterface()){
					if(!personObjectTO.getSecondDocumentType().equals(personObjectTO.getDocumentType())){
						objParameterTableTO = new ParameterTableTO();
						objParameterTableTO.setParameterTablePk(personObjectTO.getDocumentType());
					} else {
						personObjectTO.setSecondDocumentType(personObjectTO.getDocumentType());
						personObjectTO.setSecondDocumentTypeCode(personObjectTO.getDocumentTypeCode());	
					}
				} else {
					if(!personObjectTO.getSecondDocumentType().equals(personObjectTO.getDocumentType())){
						objParameterTableTO = new ParameterTableTO();
						objParameterTableTO.setIndicator1(personObjectTO.getDocumentTypeCode());
					} else {
						personObjectTO.setSecondDocumentType(personObjectTO.getDocumentType());
						personObjectTO.setSecondDocumentTypeCode(personObjectTO.getDocumentTypeCode());
					}
				}
				
				if(objParameterTableTO != null){
					List<ParameterTable> lstSecDocumentTypes = parameterServiceBean.getListParameterTableServiceBean(objParameterTableTO);			
					if(Validations.validateListIsNotNullAndNotEmpty(lstSecDocumentTypes)){	
						ParameterTable objParameterTable = lstSecDocumentTypes.get(0);				
						personObjectTO.setSecondDocumentType(objParameterTable.getParameterTablePk());				
					} else {
						return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECOND_DOCUMENT_TYPE, null);
					}
				}
			}
			 
			// validate second nationality , always code 2-string
			if(personObjectTO.getSecondNationalityCode() != null){
				objParameterTableTO = null;
				if(personObjectTO.getNationality().equals(personObjectTO.getSecondNationality())){
					params=new HashMap<String, Object>();
					params.put("codeGeographicLocPrm", personObjectTO.getSecondNationalityCode());
					params.put("typeGeographicLocPrm", GeographicLocationType.COUNTRY.getCode());
					GeographicLocation geographicSecNationality = null;
					try {
						geographicSecNationality = findObjectByNamedQuery(GeographicLocation.FIND_BY_CODE_GEO_LOC, params);					
						if(geographicSecNationality == null){
							return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECOND_NATIONALITY, null);
						}
					} catch(NoResultException ex){
						return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECOND_NATIONALITY, null);
					}					
					personObjectTO.setSecondNationality(geographicSecNationality.getIdGeographicLocationPk());
				} else {
					personObjectTO.setSecondNationality(personObjectTO.getNationality());
				}				
			}
			
			// validate document expended , only for naturals
			if (Validations.validateIsNotNullAndNotEmpty(personObjectTO.getDocumentSource())){				
				GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
				geographicLocationTO.setGeographicLocationType(GeographicLocationType.DEPARTMENT.getCode());
				geographicLocationTO.setIdLocationReferenceFk(countryResidence);
				geographicLocationTO.setGeographicLocationCode(personObjectTO.getDocumentSource());
				GeographicLocation geographicDocSource = geographicLocationServiceBean.findGeographicLocationByIdServiceBean(geographicLocationTO);				
				if(geographicDocSource == null){
					return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_SOURCE,null);
				}				
			}
			//puede realizar transferencia extrabursatil
			personObjectTO.setIndCanNegotiate(1);
		}
		
		// validate department (dpf is 2-string code, bcb/tgn is numeric)	
		GeographicLocation geographicLocation2 = null;
		GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
		geographicLocationTO.setGeographicLocationType(GeographicLocationType.DEPARTMENT.getCode());		
		if(personObjectTO.getLegalDepartment() != null){
			geographicLocationTO.setIdLocationReferenceFk(personObjectTO.getLegalResidenceCountry());
			geographicLocationTO.setIdGeographicLocationPk(personObjectTO.getLegalDepartment());
			geographicLocation2 = geographicLocationServiceBean.findGeographicLocationByIdServiceBean(geographicLocationTO);
		} else if(personObjectTO.getLegalDepartmentCode() != null){
			if(personObjectTO.getLegalDepartmentCode().compareTo("OT") != 0){
				geographicLocationTO.setIdLocationReferenceFk(personObjectTO.getLegalResidenceCountry());
			}
			geographicLocationTO.setGeographicLocationCode(personObjectTO.getLegalDepartmentCode());
			geographicLocation2 = geographicLocationServiceBean.findGeographicLocationByIdServiceBean(geographicLocationTO);
		}
				
		if(Validations.validateIsNull(geographicLocation2) ){
			if(personObjectTO.isDpfInterface()){
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_RESIDENCE_CITY, null);
			}			
		} else {
			personObjectTO.setLegalDepartment(geographicLocation2.getIdGeographicLocationPk());
		}		
		// validate province  (dpf is not required, bcb/tgn is numeric)		
		// validate district  (dpf is not required, bcb/tgn is numeric)
		if(!personObjectTO.isDpfInterface()){
			if(personObjectTO.getLegalDepartment() != null && personObjectTO.getLegalProvince() != null){
				geographicLocationTO = new GeographicLocationTO();
				geographicLocationTO.setGeographicLocationType(GeographicLocationType.PROVINCE.getCode());
				geographicLocationTO.setIdLocationReferenceFk(personObjectTO.getLegalDepartment());
				geographicLocationTO.setIdGeographicLocationPk(personObjectTO.getLegalProvince());
				GeographicLocation geographicLocation3 = geographicLocationServiceBean.findGeographicLocationByIdServiceBean(geographicLocationTO);
				
				if(Validations.validateIsNull(geographicLocation3)){
					return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_RESIDENCE_PROVINCE,null);
				}
			}
			
			if(personObjectTO.getLegalProvince() != null && personObjectTO.getLegalDistrict() != null){
				geographicLocationTO = new GeographicLocationTO();
				geographicLocationTO.setGeographicLocationType(GeographicLocationType.DISTRICT.getCode());
				geographicLocationTO.setIdLocationReferenceFk(personObjectTO.getLegalProvince());
				geographicLocationTO.setIdGeographicLocationPk(personObjectTO.getLegalDistrict());
				GeographicLocation geographicLocation4 = geographicLocationServiceBean.findGeographicLocationByIdServiceBean(geographicLocationTO);
				
				if(Validations.validateIsNull(geographicLocation4)){
					return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_RESIDENCE_DISTRICT,null);
				}
			}					
			
		}
		return null;
	}
		
	/**
	 * Metodo para validar Titular en la cuenta Titular.
	 *
	 * @param holderAccountObjectTO the holder account object to
	 * @param loggerUser the logger user
	 * @return the record validation type
	 */
	private RecordValidationType validateHoldersInHolderAccount(HolderAccountObjectTO holderAccountObjectTO, LoggerUser loggerUser){
		
		List<Long> lstIdHolders = new ArrayList<Long>();
		for (HolderRegisterTO holderAccountRegisterTO : lstHolderRegisterTOs) {
			lstIdHolders.add(holderAccountRegisterTO.getHolderRequest().getHolder().getIdHolderPk());			
		}
		
		if(Validations.validateIsNullOrEmpty(holderAccountObjectTO.getIdParticipant())){
			return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_ENTITY_CODE, null);
		}
		if(Validations.validateListIsNullOrEmpty(lstIdHolders)){
			return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDERACCOUNT_SIZE_HOLDERS, null);
		}
		
		HolderAccountTO holderAccountVal = new HolderAccountTO();
		holderAccountVal.setParticipantTO(holderAccountObjectTO.getIdParticipant());
		holderAccountVal.setHolderAccountType(holderAccountObjectTO.getAccountType());
		if(holderAccountObjectTO.isValidateAccount() && Validations.validateIsNotNullAndNotEmpty(holderAccountObjectTO.getAccountNumber())){
			holderAccountVal.setHolderAccountNumber(holderAccountObjectTO.getAccountNumber());
		}		
		List<Object[]> objectListHolderAccount = holderAccountServiceBean.findHolderAccountsForValidationsAux(holderAccountVal, lstIdHolders);
		//Valida que lista no este Vacia
		if(Validations.validateListIsNullOrEmpty(objectListHolderAccount)){
			return null;
		}
		
		for(Object[] objectHolderAccount: objectListHolderAccount){
			HolderAccount objHolderAccount = (HolderAccount) objectHolderAccount[0];
			Long sizeHolders = (Long) objectHolderAccount[1];
			int count = 0;
			for(HolderAccountDetail objHolderAccountDetail : objHolderAccount.getHolderAccountDetails()){				
				Holder objHolder = objHolderAccountDetail.getHolder();									
				if(Validations.validateIsNotNull(holderAccountObjectTO.getAccountType()) && 
						holderAccountObjectTO.getAccountType().equals(HolderAccountType.NATURAL.getCode())){					
					if(objHolderAccount.getParticipant().getIdParticipantPk().equals(holderAccountObjectTO.getIdParticipant())){
						if(ComponentConstant.ONE != sizeHolders.intValue()){
							return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDERACCOUNT_SIZE_HOLDERS, null);
						}						
						if(holderAccountObjectTO.getLstNaturalHolderObjectTOs().size() != sizeHolders){
							if(holderAccountObjectTO.isValidateAccount()){
								return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDERACCOUNT_SIZE_HOLDERS, null); 
							}
						} else {
							for(NaturalHolderObjectTO objNatHolTO : holderAccountObjectTO.getLstNaturalHolderObjectTOs()){
								if(objHolder.getDocumentType().equals(objNatHolTO.getDocumentType()) &&
										objHolder.getDocumentNumber().compareTo(objNatHolTO.getDocumentNumber()) == 0){								
									//Quitando validacion con el expedido issue 1284
									/*if(Validations.validateIsNotNullAndNotEmpty(objHolder.getDocumentSource())){
										if(!objHolder.getDocumentSource().equals(mapDocumentSource.get(objNatHolTO.getDocumentSource()))){												
											if(holderAccountObjectTO.isValidateAccount()){
												return CommonsUtilities.populateRecordCodeValidation(null, 
														GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_DOCUMENT_SOURCE, null);
											}																							
										} else {
											count ++;
											break;
										}
									} else {											
										holderServiceBean.updateHolderDocumentSource(objHolder.getIdHolderPk(), mapDocumentSource.get(objNatHolTO.getDocumentSource()), loggerUser);
										count ++;
										break;
									}*/	
									count ++;
									break;
								}
							}
						}
					}																				
				}
				//tipo juridico
				else if(Validations.validateIsNotNull(holderAccountObjectTO.getAccountType()) && 
						holderAccountObjectTO.getAccountType().equals(HolderAccountType.JURIDIC.getCode())){
					if(objHolderAccount.getParticipant().getIdParticipantPk().equals(holderAccountObjectTO.getIdParticipant())){
						if(ComponentConstant.ONE != sizeHolders.intValue()){
							if(holderAccountObjectTO.isValidateAccount()){
								return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDERACCOUNT_SIZE_HOLDERS, null); 
							}
						} else {
							JuridicHolderObjectTO objJurHolObjectTO = holderAccountObjectTO.getJuridicHolderObjectTO();															
							if(Validations.validateIsNotNullAndNotEmpty(objJurHolObjectTO.getDocumentType()) &&
									Validations.validateIsNotNullAndNotEmpty(objJurHolObjectTO.getDocumentNumber()) ){
								if(objHolder.getDocumentType().equals(objJurHolObjectTO.getDocumentType()) &&
										objHolder.getDocumentNumber().compareTo(objJurHolObjectTO.getDocumentNumber()) == 0){
									count ++;
								}
							}
						}
					}										
				}
				// Tipo Mancomunada
				else if (Validations.validateIsNotNull(holderAccountObjectTO.getAccountType()) && 
						holderAccountObjectTO.getAccountType().equals(HolderAccountType.OWNERSHIP.getCode())){
					
					if(objHolderAccount.getParticipant().getIdParticipantPk().equals(holderAccountObjectTO.getIdParticipant())){
						if(holderAccountObjectTO.getLstNaturalHolderObjectTOs().size() != sizeHolders){
							if(holderAccountObjectTO.isValidateAccount()){
								return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDERACCOUNT_SIZE_HOLDERS, null); 
							}
						} else {
							for(NaturalHolderObjectTO objNatHolTO : holderAccountObjectTO.getLstNaturalHolderObjectTOs()){																								
								
								if(objHolder.getDocumentType().equals(objNatHolTO.getDocumentType()) && 
										objHolder.getDocumentNumber().compareTo(objNatHolTO.getDocumentNumber()) == 0){										
									//Quitando validacion con el expedido issue 1284
									/*if(Validations.validateIsNotNullAndNotEmpty(objHolder.getDocumentSource())){
										if(!objHolder.getDocumentSource().equals(mapDocumentSource.get(objNatHolTO.getDocumentSource()))){												
											if(holderAccountObjectTO.isValidateAccount()){
												return CommonsUtilities.populateRecordCodeValidation(null, 
														GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_DOCUMENT_SOURCE, null);	
											}																							
										} else {
											count ++;
											break;
										}
									} else {	
										holderServiceBean.updateHolderDocumentSource(objHolder.getIdHolderPk(), 
												mapDocumentSource.get(objNatHolTO.getDocumentSource()), loggerUser);
										count ++;
										break;										
									}*/
									count ++;
									break;		
								}																						
							}
						}
					}																								
				}																											
			}	
			
			if(count != sizeHolders.intValue()){
				if(holderAccountObjectTO.isValidateAccount()){
					return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDERACCOUNT_HOLDERS_NOTEXITS, null);
				}						
			} else {
				if(!HolderAccountStatusType.ACTIVE.getCode().equals(objHolderAccount.getStateAccount())){
					return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDERACCOUNT_STATUS_NOT_REGISTERED, null);
				}
				holderAccountObjectTO.setAccountNumber(objHolderAccount.getAccountNumber());
				holderAccountObjectTO.setIdHolderAccount(objHolderAccount.getIdHolderAccountPk());
				
				// issue 1239: antes de romper el for verificamos si es la cuenta correcta para realizar el RCA(en Venta Directa)
				if(	Validations.validateIsNotNullAndPositive(holderAccountObjectTO.getIdParticipant())
						&& Validations.validateIsNotNullAndPositive(holderAccountObjectTO.getIdHolderAccount())
						&& Validations.validateIsNotNull(holderAccountObjectTO.getSecurityObjectTO())
						&& Validations.validateIsNotNullAndNotEmpty(holderAccountObjectTO.getSecurityObjectTO().getIdSecurityCode())
						){
					
					HolderAccountBalance objHolderAccountBalance = participantServiceBean.getHolderAccountBalanceByRetirement(holderAccountObjectTO.getSecurityObjectTO().getIdSecurityCode(), 
							holderAccountObjectTO.getIdParticipant(), holderAccountObjectTO.getIdHolderAccount());
					if(objHolderAccountBalance != null){
						break;
					}
				}
			}			
		}				
		return null;
	}
	
	/**
	 * Validate format duplicate code.
	 *
	 * @param strDuplicateCode the str duplicate code
	 * @return the record validation type
	 */
	private RecordValidationType validateFormatDuplicateCode(String strDuplicateCode){
		String charDuplicateCode = "";
		int countHyphen = 0;
		for(int i = 0; i < strDuplicateCode.length(); i++){
			charDuplicateCode = String.valueOf(strDuplicateCode.charAt(i));
			if(i == 0 && !charDuplicateCode.equals(GeneralConstants.HYPHEN)){
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_DOCUMENT_DUPLICATE_CODE, null);
			}			
			if(charDuplicateCode.equals(GeneralConstants.HYPHEN)){
				countHyphen++;					
				if(countHyphen > 1){
					return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_DOCUMENT_DUPLICATE_CODE, null);
				}				
			}
		}		
		return null;
	}
	
	/**
	 * Metodo que Obtiene el parametro de Tipo de Documento .
	 *
	 * @param intDocumentType the int document type
	 * @param strExpended the str expended
	 * @param isExpended the is expended
	 * @return the parameter document type
	 */
	public ParameterTable getParameterDocumentType(Integer intDocumentType, String strExpended, boolean isExpended) {
		ParameterTable objParameterTable = null;
		ParameterTableTO objParameterTableTO = new ParameterTableTO();		
		//validate document type (dpf uses short integer param)
		objParameterTableTO.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());	
		if(isExpended){
			objParameterTableTO.setShortInteger(intDocumentType);	
		} else {
			objParameterTableTO.setText1(strExpended);
		}		
		List<ParameterTable> lstDocumentTypes = parameterServiceBean.getListParameterTableServiceBean(objParameterTableTO);			
		if(Validations.validateListIsNotNullAndNotEmpty(lstDocumentTypes)){	
			objParameterTable = lstDocumentTypes.get(0);			
		} 
		return objParameterTable;
	}
	
	/**
	 * Validate information document type.
	 *
	 * @param personObjectTO the person object to
	 * @return the record validation type
	 */
	private RecordValidationType validateInformationDocumentType(PersonObjectTO personObjectTO){
		
		ParameterTableTO objParameterTableTO = new ParameterTableTO();
		
		// validate document type (DPF uses short integer parameter)
		objParameterTableTO.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
		if(personObjectTO.isDpfInterface()){
			objParameterTableTO.setShortInteger(personObjectTO.getDocumentType());
		} else {
			objParameterTableTO.setIndicator1(personObjectTO.getDocumentTypeCode());
			// issue 1239: a requerimiento del issue se envia el tipo de documento (ParameterTablePk)
			if(Validations.validateIsNotNull(personObjectTO)
					&& Validations.validateIsNotNull(personObjectTO.getDocumentType())
					&& Validations.validateIsNotNull(personObjectTO.isEarlyPaymentNoDpf())){
				objParameterTableTO.setParameterTablePk(personObjectTO.getDocumentType());
			}
		}
		
		List<ParameterTable> lstDocumentTypes = parameterServiceBean.getListParameterTableServiceBean(objParameterTableTO);			
		if(Validations.validateListIsNotNullAndNotEmpty(lstDocumentTypes)){	
			ParameterTable objParameterTable = lstDocumentTypes.get(0);				
			personObjectTO.setDocumentType(objParameterTable.getParameterTablePk());
			personObjectTO.setDocumentTypeCode(objParameterTable.getIndicator1());
		} else {							
			return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_TYPE, null);
		}
		
		// Quitando validacion de requerido issue 1284
		// Validar el expedido, si el tipo de documento es: cedula de identidad, verificar el document source sea diferente de nulo
		/*if(DocumentType.CI.getCode().equals(personObjectTO.getDocumentType())){
			if(Validations.validateIsNullOrEmpty(personObjectTO.getDocumentSource())){
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_SOURCE, null);
			}
		}*/
		
		// validate document number format
		if(personObjectTO.getDocumentNumber() != null){
			RecordValidationType recordValidationType = validateFormatDocumentNumber(personObjectTO.getDocumentType(), personObjectTO.getDocumentNumber());
			if(recordValidationType != null){
				return recordValidationType;
			}
		}
		
		// validate duplicate code
		if(personObjectTO.getDuplicateCod() != null){
			if(!personObjectTO.getDuplicateCod().equals(GeneralConstants.HYPHEN)){
				RecordValidationType recordValidationType = validateFormatDuplicateCode(personObjectTO.getDuplicateCod());
				if(recordValidationType != null){
					return recordValidationType;
				}
				personObjectTO.setDocumentNumber(personObjectTO.getDocumentNumber() + personObjectTO.getDuplicateCod());
			}			
		}
		return null;
	}
	
	
	/**
	 * Metodo para Crear Titular y cuenta.
	 *
	 * @param objHolderAccountObjectTO the obj holder account object to
	 * @return the object
	 * @throws ServiceException the service exception
	 */
	public Object createAndValidateHolderAndAccount(HolderAccountObjectTO objHolderAccountObjectTO) throws ServiceException {
		loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());			
		
//		HolderAccount objHolderAccount = holderServiceBean.searchHolderAccountServiceBean(holder, objHolderAccountObjectTO.getAccountType(), 
//				accountNumber, objHolderAccountObjectTO.getIdParticipant(), objHolderAccountObjectTO.getLstIdHolderPk());
		
		Long idHolderAccountPk = validateHoldersInHolderAccountRenew(objHolderAccountObjectTO);
					
		if(idHolderAccountPk != null){
			return idHolderAccountPk;
		} else {			
			HolderAccountRequest objHolderAccountRequest = populateHolderAccountByHolder(objHolderAccountObjectTO);					
			// register holderAccountRequest but with confirmed state
			holderAccountServiceBean.registryHolderAccountRequest(objHolderAccountRequest);
			HolderAccount holderAccount = holderAccountServiceBean.createHolderAccountFromRequest(objHolderAccountRequest, null, null);		
			objHolderAccountObjectTO.setIdHolderAccount(holderAccount.getIdHolderAccountPk());			
			logger.info("holder account created by interface "+objHolderAccountObjectTO.getIdHolderAccount());
			return objHolderAccountObjectTO.getIdHolderAccount();
		}		
	}
	
	
	/**
	 * Populate holder account by holder.
	 *
	 * @param objHolderAccountObjectTO the obj holder account object to
	 * @return the holder account request
	 */
	private HolderAccountRequest populateHolderAccountByHolder(HolderAccountObjectTO objHolderAccountObjectTO) {		
		HolderAccountRequest objHolderAccountRequest = new HolderAccountRequest();
		// we iterate registered holder list for creating holder account			
		objHolderAccountRequest.setHolderAccountDetRequest(new ArrayList<HolderAccountDetRequest>());
		objHolderAccountRequest.setParticipant(new Participant(objHolderAccountObjectTO.getIdParticipant()));
		objHolderAccountRequest.setHolderAccountType(objHolderAccountObjectTO.getAccountType());
		objHolderAccountRequest.setHolderAccountGroup(HolderAccountGroupType.INVERSTOR.getCode());
		objHolderAccountRequest.setRequestType(HolderAccountRequestHysType.CREATE.getCode());
		objHolderAccountRequest.setRequestNumber(0l);
		objHolderAccountRequest.setRegistryDate(CommonsUtilities.currentDate());
		objHolderAccountRequest.setRegistryUser(loggerUser.getUserName());
		objHolderAccountRequest.setApproveDate(CommonsUtilities.currentDate());
		objHolderAccountRequest.setApproveUser(loggerUser.getUserName());
		objHolderAccountRequest.setConfirmDate(CommonsUtilities.currentDate());
		objHolderAccountRequest.setConfirmUser(loggerUser.getUserName());
		objHolderAccountRequest.setRequestState(HolderAccountRequestHysStatusType.CONFIRM.getCode());		
		int index = 0;
		for (Long idHolderPk : objHolderAccountObjectTO.getLstIdHolderPk()) {
			HolderAccountDetRequest holderAccountReqDetHy = new HolderAccountDetRequest();
			Holder objHolder = find(Holder.class, idHolderPk);
			holderAccountReqDetHy.setHolder(objHolder);
			holderAccountReqDetHy.setHolderAccountRequest(objHolderAccountRequest);		
			if(objHolderAccountObjectTO.getAccountType().equals(HolderAccountType.OWNERSHIP.getCode())){				
				if(index == 0){
					holderAccountReqDetHy.setIndRepresentative(BooleanType.YES.getCode());
				} else {
					holderAccountReqDetHy.setIndRepresentative(BooleanType.NO.getCode());
				}				
			}else{
				holderAccountReqDetHy.setIndRepresentative(ComponentConstant.ONE);
			}				
			holderAccountReqDetHy.setRegistryDate(CommonsUtilities.currentDate());
			holderAccountReqDetHy.setRegistryUser(loggerUser.getUserName());
			holderAccountReqDetHy.setIndOldNew(BooleanType.YES.getCode());			
			objHolderAccountRequest.getHolderAccountDetRequest().add(holderAccountReqDetHy);
			index = index + 1;
		}
		return objHolderAccountRequest;
	}
	
	
	/**
	 * Metodo para validar Titular en la cuenta Titular.
	 * 
	 * @param holderAccountObjectTO the holder account object to
	 * @param loggerUser the logger user
	 * @return the record validation type
	 */
	private Long validateHoldersInHolderAccountRenew(HolderAccountObjectTO holderAccountObjectTO){						
		HolderAccountTO holderAccountVal = new HolderAccountTO();
		holderAccountVal.setParticipantTO(holderAccountObjectTO.getIdParticipant());
		holderAccountVal.setHolderAccountType(holderAccountObjectTO.getAccountType());
		List<Long> lstHolderPk = holderAccountObjectTO.getLstIdHolderPk();		
		List<Object[]> objectListHolderAccount = holderAccountServiceBean.findHolderAccountsForValidationsAux(holderAccountVal, lstHolderPk);
		//Valida que lista no este Vacia
		if(Validations.validateListIsNullOrEmpty(objectListHolderAccount)){			
			return null;
		}		
		for(Object[] objectHolderAccount: objectListHolderAccount){
			HolderAccount objHolderAccount = (HolderAccount) objectHolderAccount[0];
			Long sizeHolders = (Long) objectHolderAccount[1];
			int count = 0;
			for(HolderAccountDetail objHolderAccountDetail : objHolderAccount.getHolderAccountDetails()){				
				Holder objHolder = objHolderAccountDetail.getHolder();									
				if(Validations.validateIsNotNull(holderAccountObjectTO.getAccountType()) && 
						holderAccountObjectTO.getAccountType().equals(HolderAccountType.NATURAL.getCode())){					
					if(objHolderAccount.getParticipant().getIdParticipantPk().equals(holderAccountObjectTO.getIdParticipant())){												
						if(lstHolderPk.size() == sizeHolders){							
							for(Long lngHolderPk : lstHolderPk){								
								if(lngHolderPk.equals(objHolder.getIdHolderPk())){
									count ++;
									break;
								}								
							}
						}
					}																				
				} else if(Validations.validateIsNotNull(holderAccountObjectTO.getAccountType()) && 
						holderAccountObjectTO.getAccountType().equals(HolderAccountType.JURIDIC.getCode())){
					if(objHolderAccount.getParticipant().getIdParticipantPk().equals(holderAccountObjectTO.getIdParticipant())){
						if(ComponentConstant.ONE == sizeHolders.intValue()) {
							for(Long lngHolderPk : lstHolderPk){
								if(lngHolderPk.equals(objHolder.getIdHolderPk())){
									count ++;
									break;
								}
							}							
						}
					}										
				}
				else if (Validations.validateIsNotNull(holderAccountObjectTO.getAccountType()) && 
						holderAccountObjectTO.getAccountType().equals(HolderAccountType.OWNERSHIP.getCode())){					
					if(objHolderAccount.getParticipant().getIdParticipantPk().equals(holderAccountObjectTO.getIdParticipant())){
						if(lstHolderPk.size() == sizeHolders) {
							for(Long lngHolderPk : lstHolderPk){																								
								if(lngHolderPk.equals(objHolder.getIdHolderPk())){
									count ++;
									break;
								}																													
							}
						}
					}																								
				}																											
			}			
			if(count == sizeHolders.intValue()) {
				if(HolderAccountStatusType.ACTIVE.getCode().equals(objHolderAccount.getStateAccount())){
					return objHolderAccount.getIdHolderAccountPk();
				}				
			}			
		}				
		return null;
	}
	
	/**
	 * Metodo para Crear Titular y cuenta con una nueva estructura.
	 * Issue 1239 Desarrollo de Servicios Web BCB Venta Directa RCO
	 * @param objHolderAccountObjectTO the obj holder account object to
	 * @return the object
	 * @throws ServiceException the service exception
	 */
	public Object createHolderAndAccountNew(HolderAccountObjectTO objHolderAccountObjectTO) throws ServiceException {
		logger.info(":::: createHolderAndAccountNew ::::");
		loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		lstHolderRegisterTOs = new ArrayList<HolderRegisterTO>();
		
		HolderAccount objHolderAcc = new HolderAccount();
		if(objHolderAccountObjectTO.isValidateAccount() && 
				Validations.validateIsNotNullAndNotEmpty(objHolderAccountObjectTO.getAccountNumber())){									
			objHolderAcc = holderAccountServiceBean.getIdHolderAccount(
					objHolderAccountObjectTO.getIdParticipant(), 
					objHolderAccountObjectTO.getAccountNumber(), 
					null);
			// Si No encuentra cuenta o esta en estado Invvalido con el numero de cuenta enviado	
			if(Validations.validateIsNotNull(objHolderAcc)){					
				logger.info(":::: SE ENCONTRO NUMERO DE CUENTA ::::");
				logger.info("ID CUENTA : " + objHolderAcc.getIdHolderAccountPk());
				logger.info("PARTICIPANTE : " + objHolderAccountObjectTO.getIdParticipant());
				logger.info("NUMERO DE CUENTA : " + objHolderAccountObjectTO.getAccountNumber());
				logger.info("TIPO DE CUENTA : " + objHolderAccountObjectTO.getAccountType());
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDERACCOUNT_EXITS_ACCOUNT_NUMBER, null);
			}
		}
		//Cuenta es tipo Natural
		if(Validations.validateIsNotNull(objHolderAccountObjectTO.getAccountType()) && 
				objHolderAccountObjectTO.getAccountType().equals(HolderAccountType.NATURAL.getCode())){
			
			if(Validations.validateListIsNotNullAndNotEmpty(objHolderAccountObjectTO.getLstNaturalHolderObjectTOs())){				
				if(objHolderAccountObjectTO.getLstNaturalHolderObjectTOs().size() != 1){
			   		return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDERACCOUNT_SIZE_NATURAL, null);				   
			   	}				
				RecordValidationType recordValidationType = null;
				if(objHolderAccountObjectTO.isCreateObject()){					
					recordValidationType = validateAndLoadEntitiesNew(objHolderAccountObjectTO, objHolderAccountObjectTO.getLstNaturalHolderObjectTOs().get(0));
					if(recordValidationType != null){
						return recordValidationType;
					}					
				} else {
					validateInformationDocumentType(objHolderAccountObjectTO.getLstNaturalHolderObjectTOs().get(0));
					loadHolderHistoryNew(objHolderAccountObjectTO, objHolderAccountObjectTO.getLstNaturalHolderObjectTOs().get(0));
				}				
				
				// VALIDACION DE TITULARES				
				Boolean blCreateAccount = false;
				Object objCreateAccount = getObjectNotCreateAccount(objHolderAccountObjectTO);				
				if(objCreateAccount instanceof RecordValidationType){
					RecordValidationType validation = (RecordValidationType) objCreateAccount;
					recordValidationType = validation;
				} else if (objCreateAccount instanceof Boolean){
					blCreateAccount = (Boolean) objCreateAccount;
				}				
				if(recordValidationType != null){
					return recordValidationType;
				}				
				if(blCreateAccount){
					RecordValidationType recordValidationTypeNew = validateHoldersInHolderAccount(objHolderAccountObjectTO, loggerUser);
					if(recordValidationTypeNew != null){
						return recordValidationTypeNew;
					}					
				}
				
				logger.info(":::: TIPO DE CUENTA : NATURAL ::::");
				logger.info("Tipo de cuenta: " + objHolderAccountObjectTO.getAccountType());
				logger.info("Tipo de cuenta aux: " + objHolderAccountObjectTO.getAccountTypeCode());
				logger.info("Participante: " +  objHolderAccountObjectTO.getIdParticipant());
				logger.info("Numero de cuenta: " +  objHolderAccountObjectTO.getAccountNumber());
				logger.info("pk de cuenta: " +  objHolderAccountObjectTO.getIdHolderAccount());
				
			} else {
				return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_seccTitularNatural,null);
			}
		//Cuenta Tipo Juridica
		} else if(Validations.validateIsNotNull(objHolderAccountObjectTO.getAccountType()) && 
				objHolderAccountObjectTO.getAccountType().equals(HolderAccountType.JURIDIC.getCode())){
			// take juridic holder if account type is juridic
			if (Validations.validateIsNotNull(objHolderAccountObjectTO.getJuridicHolderObjectTO())){
				
				RecordValidationType recordValidationType = null;
				if(objHolderAccountObjectTO.isCreateObject()){	
					recordValidationType = validateAndLoadEntitiesNew(objHolderAccountObjectTO, objHolderAccountObjectTO.getJuridicHolderObjectTO());
					if(recordValidationType != null){
						return recordValidationType;
					}
				} else {
					validateInformationDocumentType(objHolderAccountObjectTO.getJuridicHolderObjectTO());
					loadHolderHistoryNew(objHolderAccountObjectTO, objHolderAccountObjectTO.getJuridicHolderObjectTO());
				}																							
				
				// 	VALIDACION DE TITULARES
				
				Boolean blCreateAccount = false;
				Object objCreateAccount = getObjectNotCreateAccount(objHolderAccountObjectTO);
				
				if(objCreateAccount instanceof RecordValidationType){
					RecordValidationType validation = (RecordValidationType) objCreateAccount;
					recordValidationType = validation;
				} else if (objCreateAccount instanceof Boolean){
					blCreateAccount = (Boolean) objCreateAccount;
				}
				
				if(recordValidationType != null){
					return recordValidationType;
				}
				
				if(blCreateAccount){
					RecordValidationType recordValidationTypeNew = validateHoldersInHolderAccount(objHolderAccountObjectTO, loggerUser);
					if(recordValidationTypeNew != null){
						return recordValidationTypeNew;
					}
				} 			

				logger.info(":::: TIPO DE CUENTA : JURIDICO ::::");
				logger.info("Tipo de cuenta: " + objHolderAccountObjectTO.getAccountType());
				logger.info("Tipo de cuenta aux: " + objHolderAccountObjectTO.getAccountTypeCode());
				logger.info("Participante: " +  objHolderAccountObjectTO.getIdParticipant());
				logger.info("Numero de cuenta: " +  objHolderAccountObjectTO.getAccountNumber());
				logger.info("pk de cuenta: " +  objHolderAccountObjectTO.getIdHolderAccount());
				
			} else {
				return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_seccTitularJuridico,null);
			}
		//Si es cuenta Mancomunada	
		} else if (Validations.validateIsNotNull(objHolderAccountObjectTO.getAccountType()) && 
				objHolderAccountObjectTO.getAccountType().equals(HolderAccountType.OWNERSHIP.getCode())){
			// only consider natural holders for ownership accounts
		   if(Validations.validateListIsNotNullAndNotEmpty(objHolderAccountObjectTO.getLstNaturalHolderObjectTOs())){
			   
			   	if(objHolderAccountObjectTO.getLstNaturalHolderObjectTOs().size() < 2){
			   		return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDERACCOUNT_SIZE_OWNERSHIP, null);				   
			   	}
			   
			   	if(objHolderAccountObjectTO.isCreateObject()){
			   		for(NaturalHolderObjectTO naturalHolderObjectTO : objHolderAccountObjectTO.getLstNaturalHolderObjectTOs()){
				   		RecordValidationType recordValidationType  = validateAndLoadEntitiesNew(objHolderAccountObjectTO, naturalHolderObjectTO);
						if(recordValidationType != null){
							return recordValidationType;
						}
				   	}
			   	} else {			   		
			   		for(NaturalHolderObjectTO naturalHolderObjectTO : objHolderAccountObjectTO.getLstNaturalHolderObjectTOs()){
			   			validateInformationDocumentType(naturalHolderObjectTO);
			   			loadHolderHistoryNew(objHolderAccountObjectTO, naturalHolderObjectTO);
			   		}			   		
			   	}			   				   			   				   												
				
			   	// VALIDACION DE TITULARES
				
				Boolean blCreateAccount = false;
				Object objCreateAccount = getObjectNotCreateAccount(objHolderAccountObjectTO);
				RecordValidationType recordValidationType = null;
				
				if(objCreateAccount instanceof RecordValidationType){
					RecordValidationType validation = (RecordValidationType) objCreateAccount;
					recordValidationType = validation;
				} else if (objCreateAccount instanceof Boolean){
					blCreateAccount = (Boolean) objCreateAccount;
				}
				
				if(recordValidationType != null){
					return recordValidationType;
				}
			   	
				if(blCreateAccount){
					RecordValidationType recordValidationTypeNew = validateHoldersInHolderAccount(objHolderAccountObjectTO, loggerUser);
					if(recordValidationTypeNew != null){
						return recordValidationTypeNew;
					}
				}
				
				logger.info(":::: TIPO DE CUENTA : MANCOMUNADO ::::");
				logger.info("Tipo de cuenta: " + objHolderAccountObjectTO.getAccountType());
				logger.info("Tipo de cuenta aux: " + objHolderAccountObjectTO.getAccountTypeCode());
				logger.info("Participante: " +  objHolderAccountObjectTO.getIdParticipant());
				logger.info("Numero de cuenta: " +  objHolderAccountObjectTO.getAccountNumber());
				logger.info("pk de cuenta: " +  objHolderAccountObjectTO.getIdHolderAccount());
				
		   } else {
				return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_seccTitularNatural,null);
			}
		}				
		
		if(objHolderAccountObjectTO.getIdHolderAccount() != null){
			return objHolderAccountObjectTO.getIdHolderAccount();
		} else {
			if(!objHolderAccountObjectTO.isCreateObject()){
				return CommonsUtilities.populateRecordCodeValidation(null, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_HOLDERACCOUNT_NULL);			
			}
		}				
		
		// we iterate holder register list for save
		for (HolderRegisterTO holderAccountRegisterTO : lstHolderRegisterTOs) {
			
			Holder holder = new Holder();			
			HolderRequest objHolderRequestAux = new HolderRequest();
			
			logger.info(":::: VERIFICAR EXISTENCIA DEL TITULAR ::::");			
			if(holderAccountRegisterTO.isExistHolder() && holderAccountRegisterTO.getHolderRequest().getIdHolderRequestPk() != null){
				objHolderRequestAux = holderAccountRegisterTO.getHolderRequest();
				logger.info(" TITULAR EXISTE  : " + objHolderRequestAux.getIdHolderRequestPk());
			} else {
				objHolderRequestAux = null;
				logger.info(" TITULAR NO EXISTE ");
			}
			
			if(objHolderRequestAux == null){				
				HolderRequest holderRequest = holderServiceBean.registerHolderRequestServiceBean(
						holderAccountRegisterTO.getHolderRequest(), 
						holderAccountRegisterTO.getParticipant(), 
						holderAccountRegisterTO.getHolderHistory(), 
						holderAccountRegisterTO.getLstLegalRepresentativeHistories(), null, loggerUser);				
				holderAccountRegisterTO.setHolderRequest(holderRequest);				
				holder = holderServiceBean.confirmHolderRequestServiceBean(holderRequest, holderAccountRegisterTO.getHolderHistory(), loggerUser);
				logger.info(" TITULAR CREADO : " + holder.getIdHolderPk());
			} else {				
				holderAccountRegisterTO.setHolderRequest(objHolderRequestAux);
				holder = find(Holder.class, objHolderRequestAux.getHolder().getIdHolderPk());				
				if(Validations.validateIsNullOrEmpty(holder.getDocumentSource())){
					holderServiceBean.updateHolderDocumentSource(holder.getIdHolderPk(), 
							holderAccountRegisterTO.getHolderHistory().getDocumentSource(), loggerUser);
					logger.info(" EXPEDIDO NUEVO : " + holderAccountRegisterTO.getHolderHistory().getDocumentSource());
				}				
				logger.info(" TITULAR EXISTENTE : " + holder.getIdHolderPk());
			}
						
			if(holderAccountRegisterTO.getPersonObjectTO() instanceof NaturalHolderObjectTO){
				((NaturalHolderObjectTO)holderAccountRegisterTO.getPersonObjectTO()).setIdHolder(holder.getIdHolderPk());
			}else if(holderAccountRegisterTO.getPersonObjectTO() instanceof JuridicHolderObjectTO){
				((JuridicHolderObjectTO)holderAccountRegisterTO.getPersonObjectTO()).setIdHolder(holder.getIdHolderPk());
			}
			
			holderAccountRegisterTO.setHolder(holder);
		}
		
		// we iterate registered holder list for creating holder account
		populateHolderAccount(objHolderAccountObjectTO);
		
		// register holderAccountRequest but with confirmed state
		holderAccountServiceBean.registryHolderAccountRequest(holderAccountRequest);
		HolderAccount holderAccount = holderAccountServiceBean.createHolderAccountFromRequest(holderAccountRequest, objHolderAccountObjectTO.getAccountNumber(), objHolderAccountObjectTO.getStockCode());		
		objHolderAccountObjectTO.setIdHolderAccount(holderAccount.getIdHolderAccountPk());
		holderAccountServiceBean.updateRequestByState(holderAccountRequest,HolderAccountRequestHysStatusType.CONFIRM.getCode(),holderAccount.getIdHolderAccountPk());
		
		logger.info("holder account created by interface "+objHolderAccountObjectTO.getIdHolderAccount());
		
		return null;
	}
	
	public Object createHolderAndAccountMassive(HolderAccountObjectTO objHolderAccountObjectTO) throws ServiceException {
		logger.info(":::: createHolderAndAccountNew ::::");
		loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		lstHolderRegisterTOs = new ArrayList<HolderRegisterTO>();
		
 		HolderAccount objHolderAcc = new HolderAccount();
		if(objHolderAccountObjectTO.isValidateAccount() && 
				Validations.validateIsNotNullAndNotEmpty(objHolderAccountObjectTO.getAccountNumber())){									
			objHolderAcc = holderAccountServiceBean.getIdHolderAccount(
					objHolderAccountObjectTO.getIdParticipant(), 
					objHolderAccountObjectTO.getAccountNumber(), 
					null);
			// Si No encuentra cuenta o esta en estado Invvalido con el numero de cuenta enviado	
			if(Validations.validateIsNotNull(objHolderAcc)){					
				logger.info(":::: SE ENCONTRO NUMERO DE CUENTA ::::");
				logger.info("ID CUENTA : " + objHolderAcc.getIdHolderAccountPk());
				logger.info("PARTICIPANTE : " + objHolderAccountObjectTO.getIdParticipant());
				logger.info("NUMERO DE CUENTA : " + objHolderAccountObjectTO.getAccountNumber());
				logger.info("TIPO DE CUENTA : " + objHolderAccountObjectTO.getAccountType());
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDERACCOUNT_EXITS_ACCOUNT_NUMBER, null);
			}
		}
		//Cuenta es tipo Natural
		if(Validations.validateIsNotNull(objHolderAccountObjectTO.getAccountType()) && 
				objHolderAccountObjectTO.getAccountType().equals(HolderAccountType.NATURAL.getCode())){
			
			if(Validations.validateListIsNotNullAndNotEmpty(objHolderAccountObjectTO.getLstNaturalHolderObjectTOs())){				
				if(objHolderAccountObjectTO.getLstNaturalHolderObjectTOs().size() != 1){
			   		return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDERACCOUNT_SIZE_NATURAL, null);				   
			   	}				
				RecordValidationType recordValidationType = null;
				if(objHolderAccountObjectTO.isCreateObject()){					
					recordValidationType = validateAndLoadEntitiesNew(objHolderAccountObjectTO, objHolderAccountObjectTO.getLstNaturalHolderObjectTOs().get(0));
					if(recordValidationType != null){
						return recordValidationType;
					}					
				} else {
					validateInformationDocumentType(objHolderAccountObjectTO.getLstNaturalHolderObjectTOs().get(0));
					loadHolderHistoryNew(objHolderAccountObjectTO, objHolderAccountObjectTO.getLstNaturalHolderObjectTOs().get(0));
				}				
				
				// VALIDACION DE TITULARES				
				Boolean blCreateAccount = false;
				Object objCreateAccount = getObjectNotCreateAccount(objHolderAccountObjectTO);				
				if(objCreateAccount instanceof RecordValidationType){
					RecordValidationType validation = (RecordValidationType) objCreateAccount;
					recordValidationType = validation;
				} else if (objCreateAccount instanceof Boolean){
					blCreateAccount = (Boolean) objCreateAccount;
				}				
				if(recordValidationType != null){
					return recordValidationType;
				}				
				if(blCreateAccount){
					RecordValidationType recordValidationTypeNew = validateHoldersInHolderAccount(objHolderAccountObjectTO, loggerUser);
					if(recordValidationTypeNew != null){
						return recordValidationTypeNew;
					}					
				}
				
				logger.info(":::: TIPO DE CUENTA : NATURAL ::::");
				logger.info("Tipo de cuenta: " + objHolderAccountObjectTO.getAccountType());
				logger.info("Tipo de cuenta aux: " + objHolderAccountObjectTO.getAccountTypeCode());
				logger.info("Participante: " +  objHolderAccountObjectTO.getIdParticipant());
				logger.info("Numero de cuenta: " +  objHolderAccountObjectTO.getAccountNumber());
				logger.info("pk de cuenta: " +  objHolderAccountObjectTO.getIdHolderAccount());
				
			} else {
				return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_seccTitularNatural,null);
			}
		//Cuenta Tipo Juridica
		} else if(Validations.validateIsNotNull(objHolderAccountObjectTO.getAccountType()) && 
				objHolderAccountObjectTO.getAccountType().equals(HolderAccountType.JURIDIC.getCode())){
			// take juridic holder if account type is juridic
			if (Validations.validateIsNotNull(objHolderAccountObjectTO.getJuridicHolderObjectTO())){
				
				RecordValidationType recordValidationType = null;
				if(objHolderAccountObjectTO.isCreateObject()){	
					recordValidationType = validateAndLoadEntitiesNew(objHolderAccountObjectTO, objHolderAccountObjectTO.getJuridicHolderObjectTO());
					if(recordValidationType != null){
						return recordValidationType;
					}
				} else {
					validateInformationDocumentType(objHolderAccountObjectTO.getJuridicHolderObjectTO());
					loadHolderHistoryNew(objHolderAccountObjectTO, objHolderAccountObjectTO.getJuridicHolderObjectTO());
				}																							
				
				// 	VALIDACION DE TITULARES
				
				Boolean blCreateAccount = false;
				Object objCreateAccount = getObjectNotCreateAccount(objHolderAccountObjectTO);
				
				if(objCreateAccount instanceof RecordValidationType){
					RecordValidationType validation = (RecordValidationType) objCreateAccount;
					recordValidationType = validation;
				} else if (objCreateAccount instanceof Boolean){
					blCreateAccount = (Boolean) objCreateAccount;
				}
				
				if(recordValidationType != null){
					return recordValidationType;
				}
				
				if(blCreateAccount){
					RecordValidationType recordValidationTypeNew = validateHoldersInHolderAccount(objHolderAccountObjectTO, loggerUser);
					if(recordValidationTypeNew != null){
						return recordValidationTypeNew;
					}
				} 			

				logger.info(":::: TIPO DE CUENTA : JURIDICO ::::");
				logger.info("Tipo de cuenta: " + objHolderAccountObjectTO.getAccountType());
				logger.info("Tipo de cuenta aux: " + objHolderAccountObjectTO.getAccountTypeCode());
				logger.info("Participante: " +  objHolderAccountObjectTO.getIdParticipant());
				logger.info("Numero de cuenta: " +  objHolderAccountObjectTO.getAccountNumber());
				logger.info("pk de cuenta: " +  objHolderAccountObjectTO.getIdHolderAccount());
				
			} else {
				return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_seccTitularJuridico,null);
			}
		//Si es cuenta Mancomunada	
		} else if (Validations.validateIsNotNull(objHolderAccountObjectTO.getAccountType()) && 
				objHolderAccountObjectTO.getAccountType().equals(HolderAccountType.OWNERSHIP.getCode())){
			// only consider natural holders for ownership accounts
		   if(Validations.validateListIsNotNullAndNotEmpty(objHolderAccountObjectTO.getLstNaturalHolderObjectTOs())){
			   
			   	if(objHolderAccountObjectTO.getLstNaturalHolderObjectTOs().size() < 2){
			   		return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDERACCOUNT_SIZE_OWNERSHIP, null);				   
			   	}
			   
			   	if(objHolderAccountObjectTO.isCreateObject()){
			   		for(NaturalHolderObjectTO naturalHolderObjectTO : objHolderAccountObjectTO.getLstNaturalHolderObjectTOs()){
				   		RecordValidationType recordValidationType  = validateAndLoadEntitiesNew(objHolderAccountObjectTO, naturalHolderObjectTO);
						if(recordValidationType != null){
							recordValidationType.setLineNumber(naturalHolderObjectTO.getRowNumber());
							return recordValidationType;
						}
				   	}
			   	} else {			   		
			   		for(NaturalHolderObjectTO naturalHolderObjectTO : objHolderAccountObjectTO.getLstNaturalHolderObjectTOs()){
			   			validateInformationDocumentType(naturalHolderObjectTO);
			   			loadHolderHistoryNew(objHolderAccountObjectTO, naturalHolderObjectTO);
			   		}			   		
			   	}			   				   			   				   												
				
			   	// VALIDACION DE TITULARES
				
				Boolean blCreateAccount = false;
				Object objCreateAccount = getObjectNotCreateAccount(objHolderAccountObjectTO);
				RecordValidationType recordValidationType = null;
				
				if(objCreateAccount instanceof RecordValidationType){
					RecordValidationType validation = (RecordValidationType) objCreateAccount;
					recordValidationType = validation;
				} else if (objCreateAccount instanceof Boolean){
					blCreateAccount = (Boolean) objCreateAccount;
				}
				
				if(recordValidationType != null){
					return recordValidationType;
				}
			   	
				if(blCreateAccount){
					RecordValidationType recordValidationTypeNew = validateHoldersInHolderAccount(objHolderAccountObjectTO, loggerUser);
					if(recordValidationTypeNew != null){
						return recordValidationTypeNew;
					}
				}
				
				logger.info(":::: TIPO DE CUENTA : MANCOMUNADO ::::");
				logger.info("Tipo de cuenta: " + objHolderAccountObjectTO.getAccountType());
				logger.info("Tipo de cuenta aux: " + objHolderAccountObjectTO.getAccountTypeCode());
				logger.info("Participante: " +  objHolderAccountObjectTO.getIdParticipant());
				logger.info("Numero de cuenta: " +  objHolderAccountObjectTO.getAccountNumber());
				logger.info("pk de cuenta: " +  objHolderAccountObjectTO.getIdHolderAccount());
				
		   } else {
				return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_seccTitularNatural,null);
			}
		}				
				
		if(!objHolderAccountObjectTO.isCreateObject()){
			return CommonsUtilities.populateRecordCodeValidation(null, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_HOLDERACCOUNT_NULL);			
		}			
		
		// we iterate holder register list for save
		for (HolderRegisterTO holderAccountRegisterTO : lstHolderRegisterTOs) {
			
			Holder holder = new Holder();			
			HolderRequest objHolderRequestAux = new HolderRequest();
			
			logger.info(":::: VERIFICAR EXISTENCIA DEL TITULAR ::::");			
			if(holderAccountRegisterTO.isExistHolder() && holderAccountRegisterTO.getHolderRequest().getIdHolderRequestPk() != null){
				objHolderRequestAux = holderAccountRegisterTO.getHolderRequest();
				logger.info(" TITULAR EXISTE  : " + objHolderRequestAux.getIdHolderRequestPk());
			} else {
				objHolderRequestAux = null;
				logger.info(" TITULAR NO EXISTE ");
			}
			
			if(objHolderRequestAux == null){				
				HolderRequest holderRequest = holderServiceBean.registerHolderRequestServiceBean(
						holderAccountRegisterTO.getHolderRequest(), 
						holderAccountRegisterTO.getParticipant(), 
						holderAccountRegisterTO.getHolderHistory(), 
						holderAccountRegisterTO.getLstLegalRepresentativeHistories(), null, loggerUser);				
				holderAccountRegisterTO.setHolderRequest(holderRequest);				
				holder = holderServiceBean.confirmHolderRequestServiceBean(holderRequest, holderAccountRegisterTO.getHolderHistory(), loggerUser);
				logger.info(" TITULAR CREADO : " + holder.getIdHolderPk());
			} else {				
				holderAccountRegisterTO.setHolderRequest(objHolderRequestAux);
				holder = find(Holder.class, objHolderRequestAux.getHolder().getIdHolderPk());				
				if(Validations.validateIsNullOrEmpty(holder.getDocumentSource())){
					holderServiceBean.updateHolderDocumentSource(holder.getIdHolderPk(), 
							holderAccountRegisterTO.getHolderHistory().getDocumentSource(), loggerUser);
					logger.info(" EXPEDIDO NUEVO : " + holderAccountRegisterTO.getHolderHistory().getDocumentSource());
				}				
				logger.info(" TITULAR EXISTENTE : " + holder.getIdHolderPk());
			}
						
			if(holderAccountRegisterTO.getPersonObjectTO() instanceof NaturalHolderObjectTO){
				((NaturalHolderObjectTO)holderAccountRegisterTO.getPersonObjectTO()).setIdHolder(holder.getIdHolderPk());
			}else if(holderAccountRegisterTO.getPersonObjectTO() instanceof JuridicHolderObjectTO){
				((JuridicHolderObjectTO)holderAccountRegisterTO.getPersonObjectTO()).setIdHolder(holder.getIdHolderPk());
			}
			
			holderAccountRegisterTO.setHolder(holder);
		}
		
		// we iterate registered holder list for creating holder account
		populateHolderAccount(objHolderAccountObjectTO);
		
		// register holderAccountRequest but with confirmed state
		holderAccountServiceBean.registryHolderAccountRequest(holderAccountRequest);
		HolderAccount holderAccount = holderAccountServiceBean.createHolderAccountFromRequest(holderAccountRequest, objHolderAccountObjectTO.getAccountNumber(), objHolderAccountObjectTO.getStockCode());		
		objHolderAccountObjectTO.setIdHolderAccount(holderAccount.getIdHolderAccountPk());
		holderAccountServiceBean.updateRequestByState(holderAccountRequest,HolderAccountRequestHysStatusType.CONFIRM.getCode(),holderAccount.getIdHolderAccountPk());
		
		logger.info("holder account created by interface "+objHolderAccountObjectTO.getIdHolderAccount());
		
		return null;
	}
	
	/**
	 * Valida y Carga Entidades with new struct.
	 * Issue 1239 Desarrollo de Servicios Web BCB Venta Directa RCO
	 * @param holderAccountObjectTO the holder account object to
	 * @param personObjectTO the person object to
	 * @return the record validation type
	 */
	private RecordValidationType validateAndLoadEntitiesNew(HolderAccountObjectTO holderAccountObjectTO, PersonObjectTO personObjectTO) {
		RecordValidationType error = validateInformationPersonNew(personObjectTO);
		if(Validations.validateIsNotNull(error)){
			return error;
		}
		
		error = validateFlowPersonTypeNew(holderAccountObjectTO,personObjectTO);
		if(Validations.validateIsNotNull(error)){
			return error;
		}
		
		loadHolderHistoryNew(holderAccountObjectTO, personObjectTO);
		
		return error;
		
	}
	
	/**
	 * Metodo para validar informacion de la persona nueva estructura.
	 * Issue 1239 Desarrollo de Servicios Web BCB Venta Directa RCO
	 * @param personObjectTO the person object to
	 * @return the record validation type
	 */
	private RecordValidationType validateInformationPersonNew(PersonObjectTO personObjectTO){
		
		ParameterTableTO objParameterTableTO = new ParameterTableTO();
		
		// validate document type (DPF uses short integer parameter)
		objParameterTableTO.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
		if(personObjectTO.isDpfInterface()){
			objParameterTableTO.setShortInteger(personObjectTO.getDocumentType());
		} else {
			//objParameterTableTO.setIndicator1(personObjectTO.getDocumentTypeCode());
			objParameterTableTO.setParameterTablePk(personObjectTO.getDocumentType());
		}
		
		List<ParameterTable> lstDocumentTypes = parameterServiceBean.getListParameterTableServiceBean(objParameterTableTO);			
		if(Validations.validateListIsNotNullAndNotEmpty(lstDocumentTypes)){	
			ParameterTable objParameterTable = lstDocumentTypes.get(0);				
			personObjectTO.setDocumentType(objParameterTable.getParameterTablePk());
			personObjectTO.setDocumentTypeCode(objParameterTable.getIndicator1());
		} else {							
			return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_TYPE, null);
		}
		
		// Validar el expedido, si el tipo de documento es: cedula de identidad, verificar el document source sea diferente de nulo
		/*if(DocumentType.CI.getCode().equals(personObjectTO.getDocumentType())
				||DocumentType.CID.getCode().equals(personObjectTO.getDocumentType())){
			if(Validations.validateIsNullOrEmpty(personObjectTO.getDocumentSource())){
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_SOURCE, null);
			}
			//Verificando la longitud de document source
			if(personObjectTO.getDocumentSource().length()!=2){
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_SOURCE, null);
			}
		}*/
		
		// validate document number format
		if(personObjectTO.getDocumentNumber() != null){
			RecordValidationType recordValidationType = validateFormatDocumentNumber(personObjectTO.getDocumentType(), personObjectTO.getDocumentNumber());
			if(recordValidationType != null){
				return recordValidationType;
			}
		}
		
		// Validando el expedido, si el tipo de documento es:DCD,DCC,DCR o DCO
		if (DocumentType.DCD.getCode().equals(personObjectTO.getDocumentType())
				|| DocumentType.DCC.getCode().equals(
						personObjectTO.getDocumentType())
				|| DocumentType.DCR.getCode().equals(
						personObjectTO.getDocumentType())) {
			// Verificar el document source sea diferente de nulo
			if (Validations.validateIsNullOrEmpty(personObjectTO.getDocumentSource())) {
				return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_SOURCE,null);
			}
			// Verificando que el document source tenga 4 digitos
			if (personObjectTO.getDocumentSource().length() != 4) {
				return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_SOURCE_SIZE,null);
			}
			// Verificando que document source sea numerico
			try {
				Integer.parseInt(personObjectTO.getDocumentSource());
			} catch (NumberFormatException nfe) {
				return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_SOURCE_TYPE_FACT,null);
			}
			personObjectTO.setDocumentIssuanceDate(Integer.parseInt(personObjectTO.getDocumentSource()));

			// Verificando que document source sea menor he igual al anio actual
			if (personObjectTO.getDocumentIssuanceDate() > CommonsUtilities.currentYear()) {
				return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_SOURCE_MANAGEMENT,null);
			}
			personObjectTO.setDocumentSource(null);

			// Validando que el cod duplicado este vacio
			if (!personObjectTO.getDuplicateCod().equals(GeneralConstants.HYPHEN)) {
				return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_DOCUMENT_DUPLICATE_CODE,null);
			}
			String newDocumentNumber = personObjectTO.getDocumentNumber();

			if (personObjectTO.getDocumentType().equals(DocumentType.DCD.getCode())) {
				newDocumentNumber = "DCD" + personObjectTO.getDocumentNumber();
			}
			if (personObjectTO.getDocumentType().equals(DocumentType.DCC.getCode())) {
				newDocumentNumber = "DCC" + personObjectTO.getDocumentNumber();
			}
			if (personObjectTO.getDocumentType().equals(DocumentType.DCR.getCode())) {
				newDocumentNumber = "DCR" + personObjectTO.getDocumentNumber();
			}

			if (personObjectTO.getDocumentIssuanceDate() != null) {

				if (personObjectTO.getDocumentType().equals(DocumentType.DCD.getCode())
						|| personObjectTO.getDocumentType().equals(
								DocumentType.DCC.getCode())
						|| personObjectTO.getDocumentType().equals(
								DocumentType.DCR.getCode())) {

					newDocumentNumber = newDocumentNumber
							+ personObjectTO.getDocumentIssuanceDate() + "";
					personObjectTO.setDocumentNumber(newDocumentNumber);
				}
			}
		}
		
		//Solo para el tipo de documento CIE
		if(personObjectTO.getDocumentType().equals(DocumentType.CIE.getCode())){
			// Validando que el cod duplicado este vacio
			if (Validations.validateIsNotNullAndNotEmpty(personObjectTO.getDocumentSource())) {
				return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_SOURCE,null);
			}
			personObjectTO.setDocumentNumber("E-" + personObjectTO.getDocumentNumber());
		}	
		
		// Solo para el tipo de documento CIEE
		if (personObjectTO.getDocumentType().equals(DocumentType.CIEE.getCode())) {
			// Validando que la extension este vacio
			if (Validations.validateIsNotNullAndNotEmpty(personObjectTO.getDocumentSource())) {
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_SOURCE, null);
			}
		}
		
		// validate duplicate code
		if(personObjectTO.getDuplicateCod() != null){
			if(!personObjectTO.getDuplicateCod().equals(GeneralConstants.HYPHEN)){
				RecordValidationType recordValidationType = validateFormatDuplicateCode(personObjectTO.getDuplicateCod());
				if(recordValidationType != null){
					return recordValidationType;
				}
				personObjectTO.setDocumentNumber(personObjectTO.getDocumentNumber() + personObjectTO.getDuplicateCod());
			}			
		}
		
		if(Validations.validateIsNullOrEmpty(personObjectTO.getNationalityCode())){
			personObjectTO.setNationalityCode("PY");
		}
		
		// validate nationality, it is always 2-string code
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("codeGeographicLocPrm", personObjectTO.getNationalityCode());
		params.put("typeGeographicLocPrm", GeographicLocationType.COUNTRY.getCode());
		GeographicLocation geographicLocation = null;
		try {
			geographicLocation = findObjectByNamedQuery(GeographicLocation.FIND_BY_CODE_GEO_LOC, params);		
			if(geographicLocation == null){
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_NATIONALITY, null);
			}
		} catch(NoResultException ex){
			return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_NATIONALITY, null);
		}				
		personObjectTO.setNationality(geographicLocation.getIdGeographicLocationPk());
		
		// validate legal residence country, it is always 2-string code
		if(!personObjectTO.getNationalityCode().equals(personObjectTO.getLegalResidenceCountryCode())){			
			params=new HashMap<String, Object>();
			params.put("codeGeographicLocPrm", personObjectTO.getLegalResidenceCountryCode());
			params.put("typeGeographicLocPrm", GeographicLocationType.COUNTRY.getCode());			
			try {
				geographicLocation = findObjectByNamedQuery(GeographicLocation.FIND_BY_CODE_GEO_LOC, params);			
				if(geographicLocation == null){
					return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_RESIDENCE_COUNTRY, null);
				}
			} catch(NoResultException ex){
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_RESIDENCE_COUNTRY, null);
			}			
			
		}
		personObjectTO.setLegalResidenceCountry(geographicLocation.getIdGeographicLocationPk());				
		
		if(personObjectTO instanceof JuridicHolderObjectTO){
			
			// we validate juridic class			
			JuridicHolderObjectTO juridicHolderObjectTO = (JuridicHolderObjectTO) personObjectTO;			
			objParameterTableTO = new ParameterTableTO();
			objParameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			objParameterTableTO.setMasterTableFk(MasterTableType.T_CLASE_JURIDICO_TITULAR.getCode());
			if(personObjectTO.isDpfInterface()){
				objParameterTableTO.setIndicator6(juridicHolderObjectTO.getJuridicClass());
			} else {
				objParameterTableTO.setParameterTablePk(juridicHolderObjectTO.getJuridicClass());
			}
			
			List<ParameterTable> lstJuridicClasses = parameterServiceBean.getListParameterTableServiceBean(objParameterTableTO);			
			if(Validations.validateListIsNotNullAndNotEmpty(lstJuridicClasses)){	
				ParameterTable objParameterTable = lstJuridicClasses.get(0);				
				juridicHolderObjectTO.setJuridicClass(objParameterTable.getParameterTablePk());
			} else {
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_JURIDIC_CLASS, null);
			}
		
			// validate economic sector
			if(juridicHolderObjectTO.getEconomicSector() != null){
				objParameterTableTO = new ParameterTableTO();
				objParameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
				objParameterTableTO.setMasterTableFk(MasterTableType.ECONOMIC_SECTOR.getCode());
				if(personObjectTO.isDpfInterface()){
					objParameterTableTO.setIndicator6(juridicHolderObjectTO.getEconomicSector());
				} else {
					objParameterTableTO.setParameterTablePk(juridicHolderObjectTO.getEconomicSector());
				}				
				List<ParameterTable> lstEconomicSector = parameterServiceBean.getListParameterTableServiceBean(objParameterTableTO);	
				if(Validations.validateListIsNotNullAndNotEmpty(lstEconomicSector)){	
					ParameterTable objParameterTable = lstEconomicSector.get(0);				
					juridicHolderObjectTO.setEconomicSector(objParameterTable.getParameterTablePk());
				} else {
					return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_ECONOMIC_SECTOR, null);
				}
			}
			ParameterTable economicActivity = null;
			// validate economic activity based on economic sector
			if(juridicHolderObjectTO.getEconomicSector() != null && juridicHolderObjectTO.getEconomicActivity() != null){
				Integer ecActvy = null;
				Integer ecActvyNum = null;
				if(personObjectTO.isDpfInterface()){
					ecActvyNum = juridicHolderObjectTO.getEconomicActivity();
				} else {
					ecActvy = juridicHolderObjectTO.getEconomicActivity();
				}				
				List<ParameterTable> lstEconomicActivities = parameterServiceBean.getListEconomicActivityBySector(
						juridicHolderObjectTO.getEconomicSector(), ecActvyNum, ecActvy);				
				if(Validations.validateListIsNotNullAndNotEmpty(lstEconomicActivities)){	
					economicActivity = lstEconomicActivities.get(0);				
					juridicHolderObjectTO.setEconomicActivity(economicActivity.getParameterTablePk());
				} else {
					return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_ECONOMIC_SECTOR, null);
				}
			}	
			
			// validando sector economico, para poder flagear al Titular con la opcion de poder Negociar indicador: indCanNegotiate
            if(economicActivity != null){
                  if(economicActivity.getIndicator6() != null){
                        switch (economicActivity.getIndicator6()) {
                        //case  2: // INTERMEDIACION FINANCIERA - ADMINISTRADORAS DE FONDOS DE PENSIONES
                        case  3: // INTERMEDIACION FINANCIERA - AGENCIAS DE BOLSA
                        case  8: // INTERMEDIACION FINANCIERA - COMPANIAS DE SEGUROS
                        case 15: // INTERMEDIACION FINANICERA - FONDOS DE INVERSION ABIERTO
                        case 16: // INTERMEDIACION FINANICERA - FONDOS DE INVERSION CERRADO
                        case 17: // INTERMEDIACION FINANICERA - FONDOS DE INVERSION MIXTOS
                        case 29: // INTERMEDIACION FINANICERA - SOCIEDADES ADMINISTRADORAS DE FONDOS DE INVERSION

                             // Colocar el flag NO puede Negociar Cartera Propia
                              personObjectTO.setIndCanNegotiate(0);
                             
                             break;

                        default:
                             // Colocar el flag SI puede Negociar Cartera Propia
                              personObjectTO.setIndCanNegotiate(1);
                             break;
                        }
                  }
            }
            
         // validate department (dpf is 2-string code, bcb/tgn is numeric)	
    		GeographicLocation geographicLocation2 = null;
    		GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
    		geographicLocationTO.setGeographicLocationType(GeographicLocationType.DEPARTMENT.getCode());		
    		if(personObjectTO.getLegalDepartment() != null){
    			geographicLocationTO.setIdLocationReferenceFk(personObjectTO.getLegalResidenceCountry());
    			geographicLocationTO.setIdGeographicLocationPk(personObjectTO.getLegalDepartment());
    			geographicLocation2 = geographicLocationServiceBean.findGeographicLocationByIdServiceBean(geographicLocationTO);
    		} else if(personObjectTO.getLegalDepartmentCode() != null){
    			if(personObjectTO.getLegalDepartmentCode().compareTo("OT") != 0){
    				geographicLocationTO.setIdLocationReferenceFk(personObjectTO.getLegalResidenceCountry());
    			}
    			geographicLocationTO.setGeographicLocationCode(personObjectTO.getLegalDepartmentCode());
    			geographicLocation2 = geographicLocationServiceBean.findGeographicLocationByIdServiceBean(geographicLocationTO);
    		}
    				
    		if(Validations.validateIsNull(geographicLocation2) ){
    			if(personObjectTO.isDpfInterface()){
    				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_RESIDENCE_CITY, null);
    			}else{
    				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_RESIDENCE_CITY_JURIDIC, null);
    			}
    		} else {
    			personObjectTO.setLegalDepartment(geographicLocation2.getIdGeographicLocationPk());
    		}

		}
		
		if(personObjectTO instanceof NaturalHolderObjectTO || personObjectTO instanceof LegalRepresentativeObjectTO){
			
			//validate second document type only when holder is natural and number is different from first document number 
			if(personObjectTO.getSecondDocumentType() != null){
				objParameterTableTO = null;
				if(personObjectTO.isDpfInterface()){
					if(!personObjectTO.getSecondDocumentType().equals(personObjectTO.getDocumentType())){
						objParameterTableTO = new ParameterTableTO();
						objParameterTableTO.setParameterTablePk(personObjectTO.getDocumentType());
					} else {
						personObjectTO.setSecondDocumentType(personObjectTO.getDocumentType());
						personObjectTO.setSecondDocumentTypeCode(personObjectTO.getDocumentTypeCode());	
					}
				} else {
					if(!personObjectTO.getSecondDocumentType().equals(personObjectTO.getDocumentType())){
						objParameterTableTO = new ParameterTableTO();
						objParameterTableTO.setIndicator1(personObjectTO.getDocumentTypeCode());
					} else {
						personObjectTO.setSecondDocumentType(personObjectTO.getDocumentType());
						personObjectTO.setSecondDocumentTypeCode(personObjectTO.getDocumentTypeCode());
					}
				}
				
				if(objParameterTableTO != null){
					List<ParameterTable> lstSecDocumentTypes = parameterServiceBean.getListParameterTableServiceBean(objParameterTableTO);			
					if(Validations.validateListIsNotNullAndNotEmpty(lstSecDocumentTypes)){	
						ParameterTable objParameterTable = lstSecDocumentTypes.get(0);				
						personObjectTO.setSecondDocumentType(objParameterTable.getParameterTablePk());				
					} else {
						return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECOND_DOCUMENT_TYPE, null);
					}
				}
			}
			 
			// validate second nationality , always code 2-string
			if(personObjectTO.getSecondNationalityCode() != null){
				objParameterTableTO = null;
				//if(personObjectTO.getNationality().equals(personObjectTO.getSecondNationality())){
				if(!personObjectTO.getNationality().equals(personObjectTO.getSecondNationality()) && personObjectTO.getSecondNationality()!=null){
					params=new HashMap<String, Object>();
					params.put("codeGeographicLocPrm", personObjectTO.getSecondNationalityCode());
					params.put("typeGeographicLocPrm", GeographicLocationType.COUNTRY.getCode());
					GeographicLocation geographicSecNationality = null;
					try {
						geographicSecNationality = findObjectByNamedQuery(GeographicLocation.FIND_BY_CODE_GEO_LOC, params);					
						if(geographicSecNationality == null){
							return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECOND_NATIONALITY, null);
						}
					} catch(NoResultException ex){
						return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECOND_NATIONALITY, null);
					}					
					personObjectTO.setSecondNationality(geographicSecNationality.getIdGeographicLocationPk());
				} else {
					personObjectTO.setSecondNationality(personObjectTO.getNationality());
				}				
			}
			
			// validate document expended , only for naturals
			if (Validations.validateIsNotNullAndNotEmpty(personObjectTO.getDocumentSource())){				
				GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
				geographicLocationTO.setGeographicLocationType(GeographicLocationType.DEPARTMENT.getCode());
				geographicLocationTO.setIdLocationReferenceFk(countryResidence);
				geographicLocationTO.setGeographicLocationCode(personObjectTO.getDocumentSource());
				GeographicLocation geographicDocSource = geographicLocationServiceBean.findGeographicLocationByIdServiceBean(geographicLocationTO);				
				if(geographicDocSource == null){
					return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_DOCUMENT_SOURCE,null);
				}				
			}
			//puede realizar transferencia extrabursatil
			personObjectTO.setIndCanNegotiate(1);
		}
		
		// validate department (dpf is 2-string code, bcb/tgn is numeric)	
		GeographicLocation geographicLocation2 = null;
		GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
		geographicLocationTO.setGeographicLocationType(GeographicLocationType.DEPARTMENT.getCode());		
		if(personObjectTO.getLegalDepartment() != null){
			geographicLocationTO.setIdLocationReferenceFk(personObjectTO.getLegalResidenceCountry());
			geographicLocationTO.setIdGeographicLocationPk(personObjectTO.getLegalDepartment());
			geographicLocation2 = geographicLocationServiceBean.findGeographicLocationByIdServiceBean(geographicLocationTO);
		} else if(personObjectTO.getLegalDepartmentCode() != null){
			if(personObjectTO.getLegalDepartmentCode().compareTo("OT") != 0){
				geographicLocationTO.setIdLocationReferenceFk(personObjectTO.getLegalResidenceCountry());
			}
			geographicLocationTO.setGeographicLocationCode(personObjectTO.getLegalDepartmentCode());
			geographicLocation2 = geographicLocationServiceBean.findGeographicLocationByIdServiceBean(geographicLocationTO);
		}
				
		if(Validations.validateIsNull(geographicLocation2) ){
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_RESIDENCE_CITY, null);
		} else {
			personObjectTO.setLegalDepartment(geographicLocation2.getIdGeographicLocationPk());
		}		
		// validate province  (dpf is not required, bcb/tgn is numeric)		
		// validate district  (dpf is not required, bcb/tgn is numeric)
		if(!personObjectTO.isDpfInterface()){
			if(personObjectTO.getLegalDepartment() != null && personObjectTO.getLegalProvince() != null){
				geographicLocationTO = new GeographicLocationTO();
				geographicLocationTO.setGeographicLocationType(GeographicLocationType.PROVINCE.getCode());
				geographicLocationTO.setIdLocationReferenceFk(personObjectTO.getLegalDepartment());
				geographicLocationTO.setIdGeographicLocationPk(personObjectTO.getLegalProvince());
				GeographicLocation geographicLocation3 = geographicLocationServiceBean.findGeographicLocationByIdServiceBean(geographicLocationTO);
				
				if(Validations.validateIsNull(geographicLocation3)){
					return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_RESIDENCE_PROVINCE,null);
				}
			}
			
			if(personObjectTO.getLegalProvince() != null && personObjectTO.getLegalDistrict() != null){
				geographicLocationTO = new GeographicLocationTO();
				geographicLocationTO.setGeographicLocationType(GeographicLocationType.DISTRICT.getCode());
				geographicLocationTO.setIdLocationReferenceFk(personObjectTO.getLegalProvince());
				geographicLocationTO.setIdGeographicLocationPk(personObjectTO.getLegalDistrict());
				GeographicLocation geographicLocation4 = geographicLocationServiceBean.findGeographicLocationByIdServiceBean(geographicLocationTO);
				
				if(Validations.validateIsNull(geographicLocation4)){
					return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_RESIDENCE_DISTRICT,null);
				}
			}					
			
		}
		return null;
	}
	
	/**
	 * Valida Tipo de Persona nueva estructura.
	 * Issue 1239 Desarrollo de Servicios Web BCB Venta Directa RCO
	 * @param holderAccountObjectTO the holder account object to
	 * @param personObjectTO the person object to
	 * @return the record validation type
	 */
	private RecordValidationType validateFlowPersonTypeNew(HolderAccountObjectTO holderAccountObjectTO, PersonObjectTO personObjectTO){
		PersonTO personTO = new PersonTO();
		//Si es titular Tipo Persona Natural
		if(personObjectTO instanceof NaturalHolderObjectTO){			
			NaturalHolderObjectTO naturalHolderObjectTO  = (NaturalHolderObjectTO) personObjectTO;		
			if(naturalHolderObjectTO.getIndMinor() != null && naturalHolderObjectTO.getIndMinor().equals(BooleanType.YES.getCode())){
			   personTO.setFlagMinorIndicator(true);
			}
			if(naturalHolderObjectTO.getIndDisabled() != null && naturalHolderObjectTO.getIndDisabled().equals(BooleanType.YES.getCode())){
				if(holderAccountObjectTO.isDpfInterface()){
					if(GeneralConstants.TWO_VALUE_INTEGER.equals(naturalHolderObjectTO.getIndDisabled())){
						naturalHolderObjectTO.setIndDisabled(BooleanType.NO.getCode());
					}
				}
				// issue 1233: se ha quitado la validacion del tag incapacidad porque no aplica en nuestro pais
				//naturalHolderObjectTO.setIndDisabled(BooleanType.NO.getCode());
			   personTO.setFlagInabilityIndicator(true);	
			}			
			if(naturalHolderObjectTO.getNationality() != null && naturalHolderObjectTO.getNationality().equals(countryResidence) &&
			   naturalHolderObjectTO.getIndResidence() != null && naturalHolderObjectTO.getIndResidence().equals(BooleanType.YES.getCode())){
			   personTO.setFlagNationalResident(true);
			}
			if(naturalHolderObjectTO.getNationality() != null && !naturalHolderObjectTO.getNationality().equals(countryResidence) &&
			   naturalHolderObjectTO.getIndResidence()!=null && naturalHolderObjectTO.getIndResidence().equals(BooleanType.YES.getCode())){
			   personTO.setFlagForeignResident(true);
			}
			if(naturalHolderObjectTO.getNationality()!=null && naturalHolderObjectTO.getNationality().equals(countryResidence) &&
			   naturalHolderObjectTO.getIndResidence()!=null && naturalHolderObjectTO.getIndResidence().equals(BooleanType.NO.getCode())){
			   personTO.setFlagNationalNotResident(true);
			}
			if(naturalHolderObjectTO.getNationality()!=null && !naturalHolderObjectTO.getNationality().equals(countryResidence) &&
			   naturalHolderObjectTO.getIndResidence()!=null && naturalHolderObjectTO.getIndResidence().equals(BooleanType.NO.getCode())){
			   personTO.setFlagForeignNotResident(true);
			}			
			Integer documentType = null;
			for(DocumentType document : DocumentType.list){
				if(document.name().equals(naturalHolderObjectTO.getDocumentTypeCode())){
				    	 documentType = document.getCode();
				    	 break;
				 }					
			}			
			RecordValidationType validation = validateFlowDocumenTypeByPersonType(personTO, documentType);
			if(validation != null){
				return validation;
			}		   
			
			// validando si es menor de edad
			if(Validations.validateIsNotNull(personObjectTO.getBirthDate())){
				if(CommonsUtilities.getYearsBetween(personObjectTO.getBirthDate(), new Date()) < 18){
					personTO.setFlagMinorIndicator(true);
					((NaturalHolderObjectTO) personObjectTO).setIndMinor(BooleanType.YES.getCode());
				}else{
					personTO.setFlagMinorIndicator(false);
				}
			}

			if (personTO.isFlagMinorIndicator()) {
				if (Validations.validateListIsNullOrEmpty(naturalHolderObjectTO.getLstLegalRepresentativeObjects())) {
					return CommonsUtilities.populateRecordCodeValidation(null,
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_NECCESARY_LEGAL_REPRESENTATIVE,
							null);
				}
				for (LegalRepresentativeObjectTO legalRepresentativeObjectTO : naturalHolderObjectTO
						.getLstLegalRepresentativeObjects()) {
					validation = validateInformationPersonNew(legalRepresentativeObjectTO);
					if (validation != null) {
						return validation;
					}
				}
				
				if (Validations.validateListIsNotNullAndNotEmpty(naturalHolderObjectTO.getLstLegalRepresentativeObjects())) {
					for(LegalRepresentativeObjectTO legalRepTO : naturalHolderObjectTO.getLstLegalRepresentativeObjects()) {
						if(legalRepTO.getPersonType().equals(PersonType.NATURAL.getCode()) 
								&&  CommonsUtilities.getYearsBetween(legalRepTO.getBirthDate(), new Date()) < 18){
							return CommonsUtilities.populateRecordCodeValidation(null,
									GenericPropertiesConstants.MSG_INTERFACE_REPRESENTATIVE_MINOR_ERROR,
									null);
						}
					}
				}
			} else {
				if (Validations.validateListIsNotNullAndNotEmpty(naturalHolderObjectTO.getLstLegalRepresentativeObjects())) {
					return CommonsUtilities.populateRecordCodeValidation(null,
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_NOT_NECCESARY_LEGAL_REPRESENTATIVE,
							null);
				}
			}
		//Si es titular Tipo Persona Juridica
		} else if (personObjectTO instanceof JuridicHolderObjectTO){			
			JuridicHolderObjectTO juridicHolderObjectTO = (JuridicHolderObjectTO) personObjectTO;			
			if(juridicHolderObjectTO.getNationality().equals(countryResidence) &&
				juridicHolderObjectTO.getIndResidence()!=null && juridicHolderObjectTO.getIndResidence().equals(BooleanType.YES.getCode())){
			   personTO.setFlagNationalResident(true);
			}
			if(juridicHolderObjectTO.getNationality()!=null && !juridicHolderObjectTO.getNationality().equals(countryResidence) &&
			  	juridicHolderObjectTO.getIndResidence()!=null && juridicHolderObjectTO.getIndResidence().equals(BooleanType.YES.getCode())){
			   personTO.setFlagForeignResident(true);
			}
			if(juridicHolderObjectTO.getNationality()!=null && juridicHolderObjectTO.getNationality().equals(countryResidence) &&
				juridicHolderObjectTO.getIndResidence()!=null && juridicHolderObjectTO.getIndResidence().equals(BooleanType.NO.getCode())){
			   personTO.setFlagNationalNotResident(true);
			}
			if(juridicHolderObjectTO.getNationality()!=null && !juridicHolderObjectTO.getNationality().equals(countryResidence) &&
			  	juridicHolderObjectTO.getIndResidence()!=null && juridicHolderObjectTO.getIndResidence().equals(BooleanType.NO.getCode())){
			   personTO.setFlagForeignNotResident(true);
			}
			
			RecordValidationType validation = validateFlowDocumenTypeByPersonType(personTO,juridicHolderObjectTO.getDocumentType());
			if(validation != null){
				return validation;
			}
			
			for(LegalRepresentativeObjectTO legalRepresentativeObjectTO : juridicHolderObjectTO.getLstLegalRepresentativeObjects()){
				validation = validateInformationPersonNew(legalRepresentativeObjectTO);
				if(validation!=null){
					return validation;
				}
			}
			
			if (Validations.validateListIsNotNullAndNotEmpty(juridicHolderObjectTO.getLstLegalRepresentativeObjects())) {
				for(LegalRepresentativeObjectTO legalRepTO : juridicHolderObjectTO.getLstLegalRepresentativeObjects()) {
					if(legalRepTO.getPersonType().equals(PersonType.NATURAL.getCode()) 
							&&  CommonsUtilities.getYearsBetween(legalRepTO.getBirthDate(), new Date()) < 18){
						return CommonsUtilities.populateRecordCodeValidation(null,
								GenericPropertiesConstants.MSG_INTERFACE_REPRESENTATIVE_MINOR_ERROR,
								null);
					}
				}
			}
		}
		
		return null;		
	}
	
	/**
	 * Carga Titular Historico nueva estructura.
	 * Issue 1239 Desarrollo de Servicios Web BCB Venta Directa RCO
	 * @param holderAccountObjectTO the holder account object to
	 * @param personObjectTO the person object to
	 */
	public void loadHolderHistoryNew(HolderAccountObjectTO holderAccountObjectTO, PersonObjectTO personObjectTO){
		HolderRegisterTO holderRegisterTO = new HolderRegisterTO();
		holderRegisterTO.setParticipant(new Participant(holderAccountObjectTO.getIdParticipant()));
		holderRegisterTO.setHolderRequest(new HolderRequest());
		HolderHistory holderHistory = new HolderHistory();
		boolean populateRepresentatives = false;
		//Titular Tipo Natural
		List<LegalRepresentativeObjectTO> lstLegalRepresentativeObjects = null;
		if(personObjectTO instanceof NaturalHolderObjectTO){
			NaturalHolderObjectTO naturalHolderObjectTO = (NaturalHolderObjectTO) personObjectTO;
			holderHistory.setLegalResidenceCountry(naturalHolderObjectTO.getLegalResidenceCountry());		
			holderHistory.setLegalProvince(naturalHolderObjectTO.getLegalProvince());
			holderHistory.setPostalResidenceCountry(naturalHolderObjectTO.getLegalResidenceCountry());
			holderHistory.setPostalProvince(naturalHolderObjectTO.getLegalProvince());
			holderHistory.setBirthDate(naturalHolderObjectTO.getBirthDate());						
			holderHistory.setDocumentNumber(naturalHolderObjectTO.getDocumentNumber());						
			holderHistory.setDocumentType(naturalHolderObjectTO.getDocumentType());						
			holderHistory.setEmail(naturalHolderObjectTO.getEmail());
			holderHistory.setFaxNumber(naturalHolderObjectTO.getFaxNumber());
			if(naturalHolderObjectTO.getFirstLastName()!=null)
			holderHistory.setFirstLastName(naturalHolderObjectTO.getFirstLastName().trim().toUpperCase());
			holderHistory.setHolderType(PersonType.NATURAL.getCode());
			holderHistory.setHomePhoneNumber(naturalHolderObjectTO.getHomePhoneNumber());
			holderHistory.setMobileNumber(naturalHolderObjectTO.getMobileNumber());
			holderHistory.setIndResidence(naturalHolderObjectTO.getIndResidence());	
			if(naturalHolderObjectTO.getLegalAddress()!=null)
			holderHistory.setLegalAddress(naturalHolderObjectTO.getLegalAddress().trim().toUpperCase());						
			holderHistory.setLegalDepartment(naturalHolderObjectTO.getLegalDepartment());		
			holderHistory.setLegalDistrict(naturalHolderObjectTO.getLegalDistrict());
			holderHistory.setLegalProvince(naturalHolderObjectTO.getLegalProvince());
			if(naturalHolderObjectTO.getName()!=null)
			holderHistory.setName(naturalHolderObjectTO.getName().trim().toUpperCase());
			holderHistory.setNationality(naturalHolderObjectTO.getNationality());						
			holderHistory.setOfficePhoneNumber(naturalHolderObjectTO.getOfficePhoneNumber());
			holderHistory.setPostalAddress(naturalHolderObjectTO.getLegalAddress());						
			holderHistory.setPostalDepartment(naturalHolderObjectTO.getLegalDepartment());
			holderHistory.setPostalDistrict(naturalHolderObjectTO.getLegalDistrict());
			holderHistory.setPostalProvince(naturalHolderObjectTO.getLegalProvince());	
			holderHistory.setPostalResidenceCountry(naturalHolderObjectTO.getLegalResidenceCountry());	
			holderHistory.setSecondDocumentNumber(naturalHolderObjectTO.getSecondDocumentNumber());						
			holderHistory.setSecondDocumentType(naturalHolderObjectTO.getSecondDocumentType());
			if(Validations.validateIsNullOrEmpty(naturalHolderObjectTO.getSecondLastName())){
				naturalHolderObjectTO.setSecondLastName(null);
			}
			if(naturalHolderObjectTO.getSecondLastName()!=null)
			holderHistory.setSecondLastName(naturalHolderObjectTO.getSecondLastName().trim().toUpperCase());
			holderHistory.setSecondNationality(naturalHolderObjectTO.getSecondNationality());
			if(Validations.validateIsNotNull(naturalHolderObjectTO.getSex())){
				holderHistory.setSex(naturalHolderObjectTO.getSex());
			}else{
				holderHistory.setSex(2306);
			}
			
			if(naturalHolderObjectTO.getIndDisabled()!=null){
				holderHistory.setIndDisabled(naturalHolderObjectTO.getIndDisabled());
			}else{
				holderHistory.setIndDisabled(BooleanType.NO.getCode());
			}
			
			if(naturalHolderObjectTO.getIndMinor()!=null){
				holderHistory.setIndMinor(naturalHolderObjectTO.getIndMinor());
			}else{
				holderHistory.setIndMinor(BooleanType.NO.getCode());
			}
			holderHistory.setIndPEP(naturalHolderObjectTO.getIndPep());
			if(holderHistory.getIndPEP().equals(BooleanType.YES.getCode())) {
				holderHistory.setCategory(naturalHolderObjectTO.getPepMotive());
				holderHistory.setRole(naturalHolderObjectTO.getPepRole());
				//holderHistory.setBeginningPeriod(new Date());
				//holderHistory.setEndingPeriod(new Date());
				holderHistory.setGrade(naturalHolderObjectTO.getPepGrade());
				holderHistory.setPepRelatedName(naturalHolderObjectTO.getPepName());
				holderHistory.setBeginningPeriod(naturalHolderObjectTO.getPepBeginningPeriod());
				holderHistory.setEndingPeriod(naturalHolderObjectTO.getPepEndingPeriod());
			}
			holderHistory.setDocumentSource(mapDocumentSource.get(naturalHolderObjectTO.getDocumentSource()));
			holderHistory.setCivilStatus(naturalHolderObjectTO.getCivilStatus());
			if(naturalHolderObjectTO.getMarriedLastName()!=null)
			holderHistory.setMarriedLastName(naturalHolderObjectTO.getMarriedLastName().trim().toUpperCase());
			holderHistory.setIndCanNegotiate(naturalHolderObjectTO.getIndCanNegotiate());
			holderHistory.setDocumentIssuanceDate(naturalHolderObjectTO.getDocumentIssuanceDate());
			holderHistory.setIndFatca(naturalHolderObjectTO.getIndFatca());
			// issue 1233 quitando la validacion del indicador IndIncapacitado porque no aplica a nuetsro pais
			
			//|| holderHistory.getIndMinor().equals(BooleanType.YES.getCode())){
			//if(holderHistory.getIndMinor().equals(BooleanType.YES.getCode())){
			if(holderHistory.getIndMinor().equals(BooleanType.YES.getCode())){
				populateRepresentatives = true;
			}
			
			lstLegalRepresentativeObjects = naturalHolderObjectTO.getLstLegalRepresentativeObjects();
		}
		//Titular Tipo Juridico
		if(personObjectTO instanceof JuridicHolderObjectTO){
			JuridicHolderObjectTO juridicHolderObjectTO = (JuridicHolderObjectTO) personObjectTO;
			holderHistory.setLegalResidenceCountry(juridicHolderObjectTO.getLegalResidenceCountry());		
			holderHistory.setLegalProvince(juridicHolderObjectTO.getLegalProvince());
			holderHistory.setPostalResidenceCountry(juridicHolderObjectTO.getLegalResidenceCountry());
			holderHistory.setPostalProvince(juridicHolderObjectTO.getLegalProvince());
			holderHistory.setDocumentNumber(juridicHolderObjectTO.getDocumentNumber());						
			holderHistory.setDocumentType(juridicHolderObjectTO.getDocumentType());						
			holderHistory.setEconomicActivity(juridicHolderObjectTO.getEconomicActivity());		
			holderHistory.setEconomicSector(juridicHolderObjectTO.getEconomicSector());
			holderHistory.setEmail(juridicHolderObjectTO.getEmail());
			holderHistory.setFaxNumber(juridicHolderObjectTO.getFaxNumber());
			if(juridicHolderObjectTO.getFullName()!=null)
			holderHistory.setFullName(juridicHolderObjectTO.getFullName().trim().toUpperCase());
			holderHistory.setHolderType(PersonType.JURIDIC.getCode());
			holderHistory.setIndResidence(juridicHolderObjectTO.getIndResidence());						
			holderHistory.setIndPEP(juridicHolderObjectTO.getIndPep());
			if(holderHistory.getIndPEP().equals(BooleanType.YES.getCode())) {
				holderHistory.setCategory(juridicHolderObjectTO.getPepMotive());
				holderHistory.setRole(juridicHolderObjectTO.getPepRole());
				holderHistory.setBeginningPeriod(juridicHolderObjectTO.getPepBeginningPeriod());
				holderHistory.setEndingPeriod(juridicHolderObjectTO.getPepEndingPeriod());
				holderHistory.setGrade(juridicHolderObjectTO.getPepGrade());
				holderHistory.setPepRelatedName(juridicHolderObjectTO.getPepName());
			}
			holderHistory.setJuridicClass(juridicHolderObjectTO.getJuridicClass());
			if(juridicHolderObjectTO.getLegalAddress()!=null)
			holderHistory.setLegalAddress(juridicHolderObjectTO.getLegalAddress().trim().toUpperCase());						
			holderHistory.setLegalDepartment(juridicHolderObjectTO.getLegalDepartment());		
			holderHistory.setLegalDistrict(juridicHolderObjectTO.getLegalDistrict());
			holderHistory.setLegalProvince(juridicHolderObjectTO.getLegalProvince());	
			holderHistory.setLegalResidenceCountry(juridicHolderObjectTO.getLegalResidenceCountry());						
			holderHistory.setMobileNumber(juridicHolderObjectTO.getMobileNumber());
			holderHistory.setNationality(juridicHolderObjectTO.getNationality());						
			holderHistory.setOfficePhoneNumber(juridicHolderObjectTO.getOfficePhoneNumber());
			holderHistory.setPostalAddress(juridicHolderObjectTO.getLegalAddress());						
			holderHistory.setPostalDepartment(juridicHolderObjectTO.getLegalDepartment());
			holderHistory.setPostalDistrict(juridicHolderObjectTO.getLegalDistrict());
			holderHistory.setPostalProvince(juridicHolderObjectTO.getLegalProvince());	
			holderHistory.setPostalResidenceCountry(juridicHolderObjectTO.getLegalResidenceCountry());
			//Capturando el indicador desde el servicio web
			holderHistory.setIndCanNegotiate(personObjectTO.getIndCanNegotiate());
			holderHistory.setIndFatca(juridicHolderObjectTO.getIndFatca());
			
			populateRepresentatives = true;
			
			lstLegalRepresentativeObjects = juridicHolderObjectTO.getLstLegalRepresentativeObjects();
		}
		
		if(populateRepresentatives){
			
			List<LegalRepresentativeHistory> lstLegalRepresentativeHistories = new ArrayList<LegalRepresentativeHistory>();
			LegalRepresentativeHistory legalRepresentativeHistory;
			
			for (LegalRepresentativeObjectTO representativeTO : lstLegalRepresentativeObjects){
				legalRepresentativeHistory = new LegalRepresentativeHistory();
				
				legalRepresentativeHistory.setBirthDate(representativeTO.getBirthDate());
				legalRepresentativeHistory.setDocumentNumber(representativeTO.getDocumentNumber());
				legalRepresentativeHistory.setDocumentType(representativeTO.getDocumentType());
				legalRepresentativeHistory.setEmail(representativeTO.getEmail());
				legalRepresentativeHistory.setFaxNumber(representativeTO.getFaxNumber());
				legalRepresentativeHistory.setFirstLastName(representativeTO.getFirstLastName());
				legalRepresentativeHistory.setHomePhoneNumber(representativeTO.getHomePhoneNumber());
				legalRepresentativeHistory.setIndResidence(representativeTO.getIndResidence());
				legalRepresentativeHistory.setLegalAddress(representativeTO.getLegalAddress());
				legalRepresentativeHistory.setLegalDepartment(representativeTO.getLegalDepartment());
				legalRepresentativeHistory.setLegalDistrict(representativeTO.getLegalDistrict());
				legalRepresentativeHistory.setLegalProvince(representativeTO.getLegalProvince());
				legalRepresentativeHistory.setLegalResidenceCountry(representativeTO.getLegalResidenceCountry());
				legalRepresentativeHistory.setMobileNumber(representativeTO.getMobileNumber());
				legalRepresentativeHistory.setName(representativeTO.getName());
				legalRepresentativeHistory.setNationality(representativeTO.getNationality());
				legalRepresentativeHistory.setOfficePhoneNumber(representativeTO.getOfficePhoneNumber());
				legalRepresentativeHistory.setPersonType(representativeTO.getPersonType());
				legalRepresentativeHistory.setPostalAddress(representativeTO.getLegalAddress());		
				legalRepresentativeHistory.setPostalDepartment(representativeTO.getLegalDepartment());
				legalRepresentativeHistory.setPostalDistrict(representativeTO.getLegalDistrict());
				legalRepresentativeHistory.setPostalProvince(representativeTO.getLegalProvince());
				legalRepresentativeHistory.setPostalResidenceCountry(representativeTO.getLegalResidenceCountry());
				legalRepresentativeHistory.setSecondDocumentNumber(representativeTO.getSecondDocumentNumber());
				legalRepresentativeHistory.setSecondDocumentType(representativeTO.getSecondDocumentType());
				legalRepresentativeHistory.setSecondLastName(representativeTO.getSecondLastName());
				legalRepresentativeHistory.setSecondNationality(representativeTO.getSecondNationality());
				legalRepresentativeHistory.setSex(representativeTO.getSex());
				legalRepresentativeHistory.setStateRepreHistory(LegalRepresentativeStateType.REGISTERED.getCode());
				legalRepresentativeHistory.setRegistryUser(loggerUser.getUserName());
				legalRepresentativeHistory.setDocumentSource(mapDocumentSource.get(representativeTO.getDocumentSource()));
				legalRepresentativeHistory.setIndPEP(BooleanType.NO.getCode());
				legalRepresentativeHistory.setRepreHistoryType(HolderRequestType.CREATION.getCode());
				legalRepresentativeHistory.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
				legalRepresentativeHistory.setRegistryUser(loggerUser.getUserName());
				legalRepresentativeHistory.setRepresentativeClass(RepresentativeClassType.LEGAL.getCode());
				legalRepresentativeHistory.setHolderRequest(holderRegisterTO.getHolderRequest());
				legalRepresentativeHistory.setDocumentIssuanceDate(representativeTO.getDocumentIssuanceDate());
				String descriptionRepresentative=null;
				if(representativeTO.getSecondLastName()!=null){
					descriptionRepresentative=representativeTO.getName()+GeneralConstants.BLANK_SPACE+representativeTO.getFirstLastName()+GeneralConstants.BLANK_SPACE+representativeTO.getSecondLastName();
				}
				else{
					descriptionRepresentative=representativeTO.getName()+GeneralConstants.BLANK_SPACE+representativeTO.getFirstLastName();
				}
				if(descriptionRepresentative!=null)
				legalRepresentativeHistory.setFullName(descriptionRepresentative.trim().toUpperCase());
				legalRepresentativeHistory.setRepresentativeClass(representativeTO.getRepresentativeClass());
				lstLegalRepresentativeHistories.add(legalRepresentativeHistory);				
			}
			
			holderRegisterTO.setLstLegalRepresentativeHistories(lstLegalRepresentativeHistories);
		}
		
		holderRegisterTO.setHolderHistory(holderHistory);
		holderRegisterTO.setPersonObjectTO(personObjectTO);
		lstHolderRegisterTOs.add(holderRegisterTO);
			
	}

	public boolean updateStateApproveRegisterHolderRequestServiceFacade(HolderRequest holderRequest, Holder holder)
			throws ServiceException {
		boolean flagTransaction = false;
		LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);

		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());

		if (holderRequest != null) {
			update(holderRequest);
		}
		if (holder != null) {
			update(holder);
		}
		flagTransaction = true;

		return flagTransaction;
	}
	
}
