package com.pradera.accounts.holder.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.accounts.HolderHistoryState;
import com.pradera.model.accounts.HolderRequest;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class HolderHistoryStateDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
public class HolderHistoryStateDataModel extends ListDataModel<HolderHistoryState> implements SelectableDataModel<HolderHistoryState>, Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new holder history state data model.
     */
    public HolderHistoryStateDataModel(){
		
	}
	
	/**
	 * Instantiates a new holder history state data model.
	 *
	 * @param data the data
	 */
	public HolderHistoryStateDataModel(List<HolderHistoryState> data){
		super(data);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public HolderHistoryState getRowData(String rowKey) {
		List<HolderHistoryState> holderHistoryStateList=(List<HolderHistoryState>)getWrappedData();
        for(HolderHistoryState holderHistorySt : holderHistoryStateList) {  
        	
            if(String.valueOf(holderHistorySt.getIdHolHistoryStatePk()).equals(rowKey))
                return holderHistorySt;  
        }  
		return null;
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(HolderHistoryState holderHistoryState) {
		return holderHistoryState.getIdHolHistoryStatePk();
	}		

}
