package com.pradera.accounts.holder.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.pradera.accounts.holder.to.ShareHolderTO;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.model.accounts.Holder;
import com.pradera.model.issuancesecuritie.Issuer;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ShareHoldersService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ShareHoldersService extends CrudDaoServiceBean {
	
	/** The holder control bean. */
	@EJB
	HolderControlBean holderControlBean;
	
	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
	/**
	 * Metodo que obtiene Titular.
	 *
	 * @param accountNumber the account number
	 * @param securityClass the security class
	 * @param securityCode the security code
	 * @param issuerCode the issuer code
	 * @return the list share
	 */
	@SuppressWarnings("unchecked")
	public List<ShareHolderTO> getListShare(Integer accountNumber, Integer  securityClass, String securityCode, String issuerCode){
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("	Select distinct ha.accountNumber,ha.accountType,	"); //0, 1
		stringBuilder.append("	sec.idSecurityCodePk, "); //2
		stringBuilder.append("	hab.availableBalance, "); //3
		stringBuilder.append("	hab.pawnBalance,hab.banBalance,hab.accreditationBalance,	"); //4,5,6
		stringBuilder.append("	hab.totalBalance, "); //7
		stringBuilder.append("	par.mnemonic, "); //8
		stringBuilder.append("	ha.idHolderAccountPk, "); //9
		stringBuilder.append("	par.description, "); //10
		stringBuilder.append("	par.idParticipantPk "); //11
		
		stringBuilder.append("	from HolderAccountBalance hab	"); 
		stringBuilder.append("	inner join hab.participant par 	"); 
		stringBuilder.append("	inner join hab.security sec 	"); 
		stringBuilder.append("	inner join hab.holderAccount ha 	");
		stringBuilder.append("	where 1 = 1 ");
		stringBuilder.append("	and hab.totalBalance > 0  ");
		
		if (securityClass!=null){
			stringBuilder.append(" and sec.securityClass = :securityClass ");
			parameters.put("securityClass", securityClass);
		}else{
			stringBuilder.append(" and sec.securityClass in (126,406,1962,1945,2246,2855,2856) ");
		}
		
		if (issuerCode !=null){
			stringBuilder.append("	and sec.issuer.idIssuerPk = :issuer ");
			parameters.put("issuer", issuerCode);
		}
		
		if (accountNumber!=null){
			stringBuilder.append("	and ha.accountNumber = :accountNumber ");
			parameters.put("accountNumber", accountNumber);
		}
		if (securityCode!=null){
			stringBuilder.append("	and sec.idSecurityCodePk like :idSecurityCode ");
			parameters.put("idSecurityCode", '%' + securityCode + '%');
			
		}
		
		stringBuilder.append("	order by sec.idSecurityCodePk,ha.accountNumber ");
		
		
		List<Object[]> accountShareRequestList =  findListByQueryString(stringBuilder.toString(), parameters);
		List<ShareHolderTO> shareHolderTO = new ArrayList<>();
		for(Object[] object: accountShareRequestList){
			ShareHolderTO shareHolder = new ShareHolderTO();
			
			shareHolder.setAccountNumber(new Integer(object[0].toString()));
			shareHolder.setAccountType(object[1].toString());
			shareHolder.setSecurityCode(object[2].toString());
			shareHolder.setAvailableBalance(new BigDecimal(object[3].toString()));
			BigDecimal blocket = (new BigDecimal(object[4].toString())).add(new BigDecimal(object[5].toString())).add(new BigDecimal(object[6].toString()));
			shareHolder.setBlocketBalance(blocket);
			shareHolder.setTotalBalance(new BigDecimal(object[7].toString()));
			shareHolder.setParticipante(object[8].toString());
			shareHolder.setHolderAccountPk(new Long(object[9].toString()));
			shareHolder.setParticipantDescription(object[10].toString());
			shareHolder.setParticipantCode(object[11].toString());
			shareHolderTO.add(shareHolder);
		}
		return shareHolderTO;
		
	}
	
	
	/**
	 * Metodo que obtiene Lista de Accionistas.
	 *
	 * @param holderAccountPk the holder account pk
	 * @return the list share holder
	 */
	@SuppressWarnings("unchecked")
	public List<Holder> getListShareHolder(Long holderAccountPk){
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("	Select h.fullName, h.documentNumber, h.documentType	"); //0, 1
		stringBuilder.append("	from HolderAccountDetail had	"); 
		stringBuilder.append("	inner join had.holder h "); 
		stringBuilder.append("	where had.holderAccount.idHolderAccountPk = :idHolderAccountPk ");
		
		parameters.put("idHolderAccountPk", holderAccountPk);
		
		stringBuilder.append("	order by h.fullName  ");
		
		List<Object[]> holderAccount =  findListByQueryString(stringBuilder.toString(), parameters);
		List<Holder> holderList = new ArrayList<>();
		for(Object[] object: holderAccount){
			Holder holder = new Holder();
			holder.setFullName(object[0].toString());
			holder.setDocumentNumber(object[1].toString());
			holder.setDocumentType((Integer)object[2]);
			holderList.add(holder);
		}
		return holderList;
		
	}
	
	
	@SuppressWarnings("unchecked")
	public boolean getVerifyHolderAccount(Long idParticipant, Long idHolderAccount,String idIssuerPk){
		
		boolean verifySecurity = false;
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("	Select s.idSecurityCodePk	"); //0
		stringBuilder.append("	from HolderAccountBalance hab	"); 
		stringBuilder.append("	inner join hab.security s "); 
		stringBuilder.append("	where hab.holderAccount.idHolderAccountPk = :idHolderAccountPk ");
		stringBuilder.append("	and hab.participant.idParticipantPk = :idParticipantPk ");
		stringBuilder.append("	and s.issuer.idIssuerPk = :idIssuerPk ");
		stringBuilder.append("	and s.securityClass in (126,406,1962,1945,2246,2855,2856) ");
		
		parameters.put("idHolderAccountPk", idHolderAccount);
		parameters.put("idParticipantPk", idParticipant);
		parameters.put("idIssuerPk", idIssuerPk);
		
		
		List<String[]> object =  (List<String[]>) findListByQueryString(stringBuilder.toString(), parameters);

		if (object!=null && !object.isEmpty()){
			verifySecurity=true;
		}
		return verifySecurity;
	}
	
	@SuppressWarnings("unchecked")
	public List<Issuer> getActionsIssuer(){
		
		boolean verifySecurity = false;
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("	Select distinct i "); //0
		stringBuilder.append("	from Issuance iss "); 
		stringBuilder.append("	inner join iss.issuer i "); 
		stringBuilder.append("	where iss.securityClass in (126,406,1962,1945,2246,2855,2856)");
		stringBuilder.append("	order by i.mnemonic,i.businessName ");
		
		Query query = em.createQuery(stringBuilder.toString());
		return (List<Issuer>)query.getResultList();
	}
	
}
