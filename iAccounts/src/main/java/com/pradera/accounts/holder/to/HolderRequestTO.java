package com.pradera.accounts.holder.to;

import java.io.Serializable;
import java.util.Date;

import com.pradera.core.component.helperui.to.HolderHelperOutputTO;
import com.pradera.model.accounts.Holder;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class HolderRequestTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
public class HolderRequestTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The participant selected. */
	private Long participantSelected;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The end date. */
	private Date endDate;
	
    /** The number request. */
    private Long numberRequest;
	
	/** The state request. */
	private Integer stateRequest;
	
	/** The request type. */
	private Integer requestType;
	
	/** The request type aux. */
	private Integer requestTypeAux;
	
	/** The person type. */
	private Integer personType;
	
	/** The document number. */
	private String documentNumber;
	
	/** The document type. */
	private Integer documentType;
	
	/** The names. */
	private String names;
	
	/** The first last name. */
	private String firstLastName;
	
	/** The second last name. */
	private String secondLastName;
	
	/** The business name. */
	private String businessName;
	
	/** The requester type. */
	private Integer requesterType;
	
	/** The state holder. */
	private Integer stateHolder;
	
	/** The holder. */
	private Holder holder = new Holder();
	
	private String holderFullName;
	
	
	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	

	/**
	 * Gets the participant selected.
	 *
	 * @return the participant selected
	 */
	public Long getParticipantSelected() {
		return participantSelected;
	}

	/**
	 * Sets the participant selected.
	 *
	 * @param participantSelected the new participant selected
	 */
	public void setParticipantSelected(Long participantSelected) {
		this.participantSelected = participantSelected;
	}

	/**
	 * Gets the number request.
	 *
	 * @return the number request
	 */
	public Long getNumberRequest() {
		return numberRequest;
	}

	/**
	 * Sets the number request.
	 *
	 * @param numberRequest the new number request
	 */
	public void setNumberRequest(Long numberRequest) {
		this.numberRequest = numberRequest;
	}

	/**
	 * Gets the state request.
	 *
	 * @return the state request
	 */
	public Integer getStateRequest() {
		return stateRequest;
	}

	/**
	 * Sets the state request.
	 *
	 * @param stateRequest the new state request
	 */
	public void setStateRequest(Integer stateRequest) {
		this.stateRequest = stateRequest;
	}

	/**
	 * Gets the request type.
	 *
	 * @return the request type
	 */
	public Integer getRequestType() {
		return requestType;
	}

	/**
	 * Sets the request type.
	 *
	 * @param requestType the new request type
	 */
	public void setRequestType(Integer requestType) {
		this.requestType = requestType;
	}

	/**
	 * Gets the request type aux.
	 *
	 * @return the request type aux
	 */
	public Integer getRequestTypeAux() {
		return requestTypeAux;
	}

	/**
	 * Sets the request type aux.
	 *
	 * @param requestTypeAux the new request type aux
	 */
	public void setRequestTypeAux(Integer requestTypeAux) {
		this.requestTypeAux = requestTypeAux;
	}

	/**
	 * Gets the person type.
	 *
	 * @return the person type
	 */
	public Integer getPersonType() {
		return personType;
	}

	/**
	 * Sets the person type.
	 *
	 * @param personType the new person type
	 */
	public void setPersonType(Integer personType) {
		this.personType = personType;
	}

	

	/**
	 * Gets the document number.
	 *
	 * @return the document number
	 */
	public String getDocumentNumber() {
		return documentNumber;
	}

	/**
	 * Sets the document number.
	 *
	 * @param documentNumber the new document number
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	/**
	 * Gets the document type.
	 *
	 * @return the document type
	 */
	public Integer getDocumentType() {
		return documentType;
	}

	/**
	 * Sets the document type.
	 *
	 * @param documentType the new document type
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	/**
	 * Gets the names.
	 *
	 * @return the names
	 */
	public String getNames() {
		return names;
	}

	/**
	 * Sets the names.
	 *
	 * @param names the new names
	 */
	public void setNames(String names) {
		this.names = names;
	}

	/**
	 * Gets the business name.
	 *
	 * @return the business name
	 */
	public String getBusinessName() {
		return businessName;
	}

	/**
	 * Sets the business name.
	 *
	 * @param businessName the new business name
	 */
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	/**
	 * Gets the first last name.
	 *
	 * @return the first last name
	 */
	public String getFirstLastName() {
		return firstLastName;
	}

	/**
	 * Sets the first last name.
	 *
	 * @param firstLastName the new first last name
	 */
	public void setFirstLastName(String firstLastName) {
		this.firstLastName = firstLastName;
	}

	/**
	 * Gets the second last name.
	 *
	 * @return the second last name
	 */
	public String getSecondLastName() {
		return secondLastName;
	}

	/**
	 * Sets the second last name.
	 *
	 * @param secondLastName the new second last name
	 */
	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}

	/**
	 * Gets the requester type.
	 *
	 * @return the requester type
	 */
	public Integer getRequesterType() {
		return requesterType;
	}

	/**
	 * Sets the requester type.
	 *
	 * @param requesterType the new requester type
	 */
	public void setRequesterType(Integer requesterType) {
		this.requesterType = requesterType;
	}

	/**
	 * Gets the state holder.
	 *
	 * @return the state holder
	 */
	public Integer getStateHolder() {
		return stateHolder;
	}

	/**
	 * Sets the state holder.
	 *
	 * @param stateHolder the new state holder
	 */
	public void setStateHolder(Integer stateHolder) {
		this.stateHolder = stateHolder;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	public String getHolderFullName() {
		return holderFullName;
	}

	public void setHolderFullName(String holderFullName) {
		this.holderFullName = holderFullName;
	}

}
