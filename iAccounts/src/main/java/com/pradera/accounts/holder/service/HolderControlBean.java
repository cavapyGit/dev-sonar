package com.pradera.accounts.holder.service;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.ejb.AccessTimeout;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import com.pradera.accounts.holder.facade.HolderServiceFacade;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.HolderHistory;
import com.pradera.model.accounts.HolderRequest;
import com.pradera.model.accounts.LegalRepresentative;
import com.pradera.model.accounts.type.EconomicActivityType;
import com.pradera.model.accounts.type.JuridicClassType;
import com.pradera.model.operations.RequestCorrelativeType;

// TODO: Auto-generated Javadoc
/**
 * Session Bean implementation class HolderControlBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
@Stateless
public class HolderControlBean extends CrudDaoServiceBean {

	/** The holder service facade. */
	@EJB
	HolderServiceFacade holderServiceFacade;
	
	/** The holder service bean. */
	@EJB
	HolderServiceBean holderServiceBean;
	
	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
    /**
     * Default constructor. 
     */
	
	public HolderControlBean() {
        // TODO Auto-generated constructor stub
    }
    
    /**
     * Valida exist holder.
     *
     * @param holderRequest the holder request
     * @param lstLegalRepresentative the lst legal representative
     * @param holder the holder
     * @return true, if successful
     * @throws Exception the exception
     */
    //@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    //@Interceptors(ContextHolderInterceptor.class)
    @Performance
    @AccessTimeout(unit=TimeUnit.MINUTES,value=5)
    public boolean validaExistHolder(HolderRequest holderRequest,List<LegalRepresentative> lstLegalRepresentative, Holder holder) throws Exception{
     
    	boolean indExist=false;
      
    	if(!validateExistHolder(holderRequest)){
			for(LegalRepresentative legal : lstLegalRepresentative){			
				create(legal);	
			}
			Long nextVal=assignIdentityHolder(holder.getEconomicActivity());
			holder.setIdHolderPk(nextVal);
			create(holder);					
				
		}
    	else{
    		indExist=true;
    	}
    	em.flush();
    	return indExist;
    }
    
    
    
    /**
     * Valida exist holder request.
     *
     * @param holderRequest the holder request
     * @return the holder request
     */
    //@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Interceptors(ContextHolderInterceptor.class)
    @Performance
    @AccessTimeout(unit=TimeUnit.MINUTES,value=5)
    public HolderRequest validaExistHolderRequest(HolderRequest holderRequest){
       
    	if(!validateExistHolderRequest(holderRequest)){			
    		holderRequest = create(holderRequest);    		
		}
    	
    	return holderRequest;
    }
    
    
    /**
     * Metodo para Validar si Existe holder.
     *
     * @param holderRequest the holder request
     * @return true, if successful
     */
    public boolean validateExistHolder(HolderRequest holderRequest){
		
		 boolean flagExist=false;
		 Holder objHolder;
	     HolderHistory holderHistory = new HolderHistory(); 
	     if(holderRequest.getHolderHistories() != null){
	    	 holderHistory = holderRequest.getHolderHistories().get(0);
		      	//Initial if valid holder - First Nationality
		     
		     if(holderHistory.getJuridicClass() != null && 
					 holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())){
				 return false;
			 }
		     if(holderHistory.getEconomicActivity()!= null &&
					 holderHistory.getEconomicActivity().equals(EconomicActivityType.FONDOS_INVERSION_ABIERTOS.getCode())){
				 return false;
			 }
		     if(holderHistory.getEconomicActivity()!= null &&
					 holderHistory.getEconomicActivity().equals(EconomicActivityType.ADMINISTRADORAS_FONDOS_PENSIONES.getCode())){
				 return false;
			 }
	     }	    	     
	     	objHolder = new Holder();
			objHolder.setDocumentType(holderHistory.getDocumentType());
			objHolder.setDocumentNumber(holderHistory.getDocumentNumber());	
			objHolder.setNationality(holderHistory.getNationality());
			objHolder.setDocumentSource(holderHistory.getDocumentSource());
			objHolder.setRelatedCui(holderHistory.getRelatedCui());
			
			
			objHolder = holderServiceBean.getHolderByDocumentTypeAndDocumentNumberServiceBean(objHolder);
			//End if valid holder - First Nationality
			
			if(objHolder==null){
				//Initial if valid holder - Second Nationality
				objHolder = new Holder();
				objHolder.setSecondDocumentType(holderHistory.getSecondDocumentType());
				objHolder.setSecondDocumentNumber(holderHistory.getSecondDocumentNumber());
				objHolder.setSecondNationality(holderHistory.getSecondNationality());
				
				
				if(objHolder.getSecondDocumentType()!=null && objHolder.getSecondDocumentNumber()!=null && objHolder.getSecondNationality()!=null){
				
				   objHolder = holderServiceBean.getHolderBySecondDocumentTypeAndSecondDocumentNumberServiceBean(objHolder);
				
				   if(objHolder!=null){
					   flagExist = true;
				   }
				   
				}
				else{
					objHolder = null;
				}
				//End if valid holder - Second Nationality			
				
			}
			else{
				flagExist=true;
			}
		 	
	     return flagExist;
	 }
    
    /**
     * Validate exist holder request.
     *
     * @param holderRequest the holder request
     * @return true, if successful
     */
    public boolean validateExistHolderRequest(HolderRequest holderRequest){
    	boolean flagExist=false;    	
		 HolderHistory holderHistory = new HolderHistory();	
		 if(holderRequest.getHolderHistories()!=null){
			 holderHistory = holderRequest.getHolderHistories().get(0);
			 if(holderHistory.getJuridicClass() !=null && 
					 holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())){
				 return false;
			 }
			 if(holderHistory.getEconomicActivity()!= null &&
					 holderHistory.getEconomicActivity().equals(EconomicActivityType.FONDOS_INVERSION_ABIERTOS.getCode())){
				 return false;
			 }
			 if(holderHistory.getEconomicActivity()!= null &&
					 holderHistory.getEconomicActivity().equals(EconomicActivityType.ADMINISTRADORAS_FONDOS_PENSIONES.getCode())){
				 return false;
			 }
		 }    	
		HolderRequest auxholderRequest = holderServiceBean.getHolderRequestByDocumentTypeAndDocumentNumberServiceBean(holderRequest);						
		if(auxholderRequest!=null){
			flagExist = true;			   
		}
		return flagExist;
	 }
    
    /**
     * Assign identity holder.
     *
     * @param economicActivity the economic activity
     * @return the long
     */
    public Long assignIdentityHolder(Integer economicActivity){
		Long nextVal=null;
    	if(economicActivity!=null && economicActivity.equals(EconomicActivityType.AGENCIAS_BOLSA.getCode())){
			nextVal=holderServiceFacade.getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK_AB);
		}
		else if(economicActivity!=null && economicActivity.equals(EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode())){
			nextVal=holderServiceFacade.getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK_SAFI);
		}
		else if(economicActivity!=null && economicActivity.equals(EconomicActivityType.FONDOS_INVERSION_CERRADOS.getCode())){
			//nextVal=holderServiceFacade.getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK_FIC);
			nextVal=holderServiceFacade.getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK_FI);
		}else if(economicActivity!=null && economicActivity.equals(EconomicActivityType.FONDOS_INVERSION_MIXTOS.getCode()))
			nextVal=holderServiceFacade.getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK_FI);
		else if(economicActivity!=null && economicActivity.equals(EconomicActivityType.FONDOS_INVERSION_ABIERTOS.getCode())){
			nextVal=holderServiceFacade.getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK_FI);
		}
		else if(economicActivity!=null && economicActivity.equals(EconomicActivityType.ADMINISTRADORAS_FONDOS_PENSIONES.getCode())){
			nextVal=holderServiceFacade.getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK_AFP);
	    }
		else if(economicActivity!=null && economicActivity.equals(EconomicActivityType.BANCOS.getCode())){
			nextVal=holderServiceFacade.getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK_B);
		}
		else if(economicActivity!=null && (economicActivity.equals(EconomicActivityType.MUTUALES.getCode())
			|| economicActivity.equals(EconomicActivityType.FONDOS_FINANCIEROS_PRIVADOS.getCode())
			|| economicActivity.equals(EconomicActivityType.COOPERATIVAS.getCode()))){
			nextVal=holderServiceFacade.getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK_MFC);
		}
		else if(economicActivity!=null && economicActivity.equals(EconomicActivityType.COMPANIAS_SEGURO.getCode())){
			nextVal=holderServiceFacade.getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK_CS);
		}
		else{
			nextVal=holderServiceFacade.getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK);
		}
	    
	    if(nextVal!=null && nextVal.equals(new Long(-1))){
		    nextVal=holderServiceFacade.getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK);
		}
	    
	    if(nextVal!=null && !nextVal.equals(new Long(-1))){
			if(accountsFacade.validateExistHolderServiceFacade(nextVal)){
				assignIdentityHolder(economicActivity);					
			}
		}
	    return nextVal;
	}
}
