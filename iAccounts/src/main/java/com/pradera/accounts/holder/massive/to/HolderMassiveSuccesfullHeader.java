package com.pradera.accounts.holder.massive.to;

public class HolderMassiveSuccesfullHeader {
	
	private String holderAccountType = "TIPO DE CUENTA TITULAR";
	private String holderDocumentNumber = "NUMERO DOCUMENTO TITULAR";
	
	public String getHolderAccountType() {
		return holderAccountType;
	}
	public void setHolderAccountType(String holderAccountType) {
		this.holderAccountType = holderAccountType;
	}
	public String getHolderDocumentNumber() {
		return holderDocumentNumber;
	}
	public void setHolderDocumentNumber(String holderDocumentNumber) {
		this.holderDocumentNumber = holderDocumentNumber;
	}

}
