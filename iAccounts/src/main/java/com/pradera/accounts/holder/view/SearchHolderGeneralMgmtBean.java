package com.pradera.accounts.holder.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.accounts.holder.facade.HolderServiceFacade;
import com.pradera.accounts.holder.to.HolderRequestTO;
import com.pradera.accounts.util.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.accounts.Holder;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SearchHolderGeneralMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/08/2015
 */
@DepositaryWebBean
public class SearchHolderGeneralMgmtBean extends GenericBaseBean implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The general pameters facade. */
	@EJB
	GeneralParametersFacade generalPametersFacade;

	/** The holder service facade. */
	@EJB
	HolderServiceFacade holderServiceFacade;

	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
	/** The holder filter. */
	private HolderRequestTO holderFilter;
	
	/** The holder. */
	private Holder holder;
	
	/** The expedition. */
	private Integer expedition;
	
	private GenericDataModel<Holder> holderDataModel;
		
	/** The list document type. */
	private List<ParameterTable> listDocumentType;

	/** The list department location. */
	private List<ParameterTable> listDepartmentLocation;

	/** The document type description. */
    private Map<Integer,String> documentTypeDescription = null;
	
    /** The request state description. */
    private Map<Integer,String> holderStateDescription = null;
    
    /** The request state description. */
    private Map<Integer,String> documentSourceDescription = null;

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		holderFilter = new HolderRequestTO();
		holder = new Holder();
		documentTypeDescription = new HashMap<Integer,String>();
		holderStateDescription = new HashMap<Integer,String>();	
		documentSourceDescription = new HashMap<Integer,String>();	
		try {			
			getLstDocumentType();
			getLstDepartmentLocation();
			ParameterTableTO  parameterTableTO = new ParameterTableTO();									
			parameterTableTO.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
			for(ParameterTable param : generalPametersFacade.getListParameterTableServiceBean(parameterTableTO)) {
				documentTypeDescription.put(param.getParameterTablePk(), param.getParameterName());
			}			
			parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_STATE_TYPE.getCode());
			for(ParameterTable param : generalPametersFacade.getListParameterTableServiceBean(parameterTableTO)){
				holderStateDescription.put(param.getParameterTablePk(), param.getParameterName());
			}	
			parameterTableTO.setMasterTableFk(MasterTableType.DOCUMENT_SOURCE.getCode());
			for(ParameterTable param : generalPametersFacade.getListParameterTableServiceBean(parameterTableTO)){
				documentSourceDescription.put(param.getParameterTablePk(), param.getDescription());
			}	
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Gets the lst document type.
	 *
	 * @return the lst document type
	 */
	public void getLstDocumentType() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
			listDocumentType = generalPametersFacade.getListParameterTableServiceBean(parameterTableTO);
			List<ParameterTable> listDocumentTemp = new ArrayList<ParameterTable>();
			for (ParameterTable listdocument : listDocumentType) {
				if (listdocument.getIndicator2() != null && listdocument.getIndicator2().equals(ParameterTableStateType.REGISTERED.getCode().toString())) {
					listDocumentTemp.add(listdocument);
				}
			}
			listDocumentType = listDocumentTemp;
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	public void getLstDepartmentLocation() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.DOCUMENT_SOURCE.getCode());
			listDepartmentLocation = generalPametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Search request holders.
	 */
	@LoggerAuditWeb
	public void searchHolders() {		
		holderDataModel = null;
		holder = new Holder();	
		if (getHolderFilter().getDocumentNumber().length()<3){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
				    PropertiesUtilities.getMessage(PropertiesConstants.ERROR_LENGTH_SIZE));
				  JSFUtilities.showSimpleValidationDialog();
		}else{
				try {
						holder = new Holder();			   	   
						List<Holder> listHolder = new ArrayList<Holder>();
						listHolder = holderServiceFacade.getListSearchHolderGeneral(
								getHolderFilter().getDocumentType(),getHolderFilter().getDocumentNumber(),expedition);
					   
						if(listHolder != null && listHolder.size() > 0){
							for(int i=0;i<listHolder.size();i++){
								if(listHolder.get(i).getDocumentType()!=null){
									listHolder.get(i).setDescriptionTypeDocument(documentTypeDescription.get(listHolder.get(i).getDocumentType()).toString());
								} else {
									listHolder.get(i).setDescriptionTypeDocument(GeneralConstants.EMPTY_STRING);
								}							
								if(listHolder.get(i).getStateHolder()!=null){
									listHolder.get(i).setStateDescription(holderStateDescription.get(listHolder.get(i).getStateHolder()).toString());
							    } else {
									listHolder.get(i).setStateDescription(GeneralConstants.EMPTY_STRING);
							    }
								if(listHolder.get(i).getDocumentSource()!=null){
									listHolder.get(i).setDescriptionDocumentSource(documentSourceDescription.get(listHolder.get(i).getDocumentSource()).toString());
							    } else {
									listHolder.get(i).setDescriptionDocumentSource(GeneralConstants.EMPTY_STRING);
							    }
							}					
					   }			   
						
						holderDataModel = new GenericDataModel<Holder>(listHolder);
				} catch (Exception ex) {
					excepcion.fire(new ExceptionToCatchEvent(ex));
				}
		}
	}
	
	/**
	 * Clean all.
	 */
	public void cleanAll() {		
		JSFUtilities.resetViewRoot();		
		holderFilter = new HolderRequestTO();
		holderDataModel = null;
		holder = new Holder();	
		expedition=null;
			
	}
	
	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Gets the holder filter.
	 *
	 * @return the holder filter
	 */
	public HolderRequestTO getHolderFilter() {
		return holderFilter;
	}

	/**
	 * Sets the holder filter.
	 *
	 * @param holderFilter the new holder filter
	 */
	public void setHolderFilter(HolderRequestTO holderFilter) {
		this.holderFilter = holderFilter;
	}

//	public HolderDataModel getHolderDataModel() {
//		return holderDataModel;
//	}
//
//	public void setHolderDataModel(HolderDataModel holderDataModel) {
//		this.holderDataModel = holderDataModel;
//	}


	/**
 * Gets the list document type.
 *
 * @return the list document type
 */
public List<ParameterTable> getListDocumentType() {
		return listDocumentType;
	}

	/**
	 * Gets the holder data model.
	 *
	 * @return the holder data model
	 */
	public GenericDataModel<Holder> getHolderDataModel() {
		return holderDataModel;
	}

	/**
	 * Sets the holder data model.
	 *
	 * @param holderDataModel the new holder data model
	 */
	public void setHolderDataModel(GenericDataModel<Holder> holderDataModel) {
		this.holderDataModel = holderDataModel;
	}

	/**
	 * Sets the list document type.
	 *
	 * @param listDocumentType the new list document type
	 */
	public void setListDocumentType(List<ParameterTable> listDocumentType) {
		this.listDocumentType = listDocumentType;
	}

	/**
	 * Gets the list department location.
	 *
	 * @return the list department location
	 */
	public List<ParameterTable> getListDepartmentLocation() {
		return listDepartmentLocation;
	}

	/**
	 * Sets the list department location.
	 *
	 * @param listDepartmentLocation the new list department location
	 */
	public void setListDepartmentLocation(
			List<ParameterTable> listDepartmentLocation) {
		this.listDepartmentLocation = listDepartmentLocation;
	}

	/**
	 * Gets the expedition.
	 *
	 * @return the expedition
	 */
	public Integer getExpedition() {
		return expedition;
	}

	/**
	 * Sets the expedition.
	 *
	 * @param expedition the new expedition
	 */
	public void setExpedition(Integer expedition) {
		this.expedition = expedition;
	}

	/**
	 * Gets the document source description.
	 *
	 * @return the document source description
	 */
	public Map<Integer, String> getDocumentSourceDescription() {
		return documentSourceDescription;
	}

	/**
	 * Sets the document source description.
	 *
	 * @param documentSourceDescription the document source description
	 */
	public void setDocumentSourceDescription(
			Map<Integer, String> documentSourceDescription) {
		this.documentSourceDescription = documentSourceDescription;
	}
	
	
	
}
