 /*
 * 
 */
package com.pradera.accounts.holder.view;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.apache.deltaspike.core.api.resourceloader.InjectableResource;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import com.pradera.accounts.holder.facade.HolderServiceFacade;
import com.pradera.accounts.holder.to.HolderRequestTO;
import com.pradera.accounts.holder.to.MassiveHolderTO;
import com.pradera.accounts.investorType.facade.InvestorTypeFacade;
import com.pradera.accounts.investorType.to.InvestorTypeModelTO;
import com.pradera.accounts.util.view.PropertiesConstants;
import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.OperationUserTO;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.type.RegistryType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.accounts.to.LegalRepresentativeTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.extension.view.LegalRepresentativeHelperBean;
import com.pradera.core.component.utils.ClientInformationTO;
import com.pradera.core.component.utils.ClientInformationValidator;
import com.pradera.core.component.utils.DocumentTO;
import com.pradera.core.component.utils.DocumentValidator;
import com.pradera.core.component.utils.PersonTO;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.HolderFile;
import com.pradera.model.accounts.HolderHistory;
import com.pradera.model.accounts.HolderReqFileHistory;
import com.pradera.model.accounts.HolderRequest;
import com.pradera.model.accounts.InstitutionInformation;
import com.pradera.model.accounts.LegalRepresentativeFile;
import com.pradera.model.accounts.LegalRepresentativeHistory;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.PepPerson;
import com.pradera.model.accounts.RepresentativeFileHistory;
import com.pradera.model.accounts.type.AgeLessNationalOrForeignResidentDocHolderNatType;
import com.pradera.model.accounts.type.AgeMajorDomOrForeignResidentDocHolderNatType;
import com.pradera.model.accounts.type.AnnularMotivesHolderRequestType;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.EconomicActivityType;
import com.pradera.model.accounts.type.EconomicSectorType;
import com.pradera.model.accounts.type.HolderFileStateType;
import com.pradera.model.accounts.type.HolderReqFileHisType;
import com.pradera.model.accounts.type.HolderRequestStateType;
import com.pradera.model.accounts.type.HolderRequestType;
import com.pradera.model.accounts.type.JuridicClassType;
import com.pradera.model.accounts.type.LegalRepresentativeStateType;
import com.pradera.model.accounts.type.ParticipantClassType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.accounts.type.PepMotiveType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.accounts.type.RejectMotivesHolderRequestType;
import com.pradera.model.accounts.type.RepresentativeEntityStateType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.GeographicLocationStateType;
import com.pradera.model.generalparameter.type.GeographicLocationType;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class HolderMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class HolderMgmtBean extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The holder request. */
	private HolderRequest holderRequest;

	/** The holder request detail. */
	private HolderRequest holderRequestDetail;

	/** The holder request selected. */
	private HolderRequest holderRequestSelected;

	/** The holder history. */
	private HolderHistory holderHistory;

	/** The holder history detail. */
	private HolderHistory holderHistoryDetail;

	/** The holder filter. */
	private HolderRequestTO holderFilter;

	/** The participant. */
	private Participant participant;

	/** The participant detail. */
	private Participant participantDetail;

	/** The holder. */
	private Holder holder;
	
	/** The show panel options component. */
	private boolean showPanelOptionsComponent;

	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	private UserPrivilege userPrivilege;
	
	/** The country residence. */
	@Inject @Configurable Integer countryResidence;
	
	/** The department issuance. */
	@Inject @Configurable Integer departmentIssuance;

	/** The province default. */
	@Inject @Configurable Integer provinceDefault;
	
	/** The department default. */
	@Inject @Configurable Integer departmentDefault;
	/** The holder request data model. */
	private HolderRequestDataModel holderRequestDataModel;

		/** The flag copy legal holder. */
	private boolean flagCopyLegalHolder;

	/** The flag copy legal representative. */
	private boolean flagCopyLegalRepresentative;

	/** The flag holder natural person. */
	private boolean flagHolderNaturalPerson;

	/** The flag holder juridic person. */
	private boolean flagHolderJuridicPerson;

	/** The flag representative natural person. */
	private boolean flagRepresentativeNaturalPerson;

	/** The flag representative juridic person. */
	private boolean flagRepresentativeJuridicPerson;

	/** The disabled country resident holder. */
	private boolean disabledCountryResidentHolder;

	/** The disabled country resident representative. */
	private boolean disabledCountryResidentRepresentative;

	/** The disabled representative button. */
	private boolean disabledRepresentativeButton;

	/** The flag is pep. */
	private boolean flagIsPEP;

	/** The flag representative is pep. */
	private boolean flagRepresentativeIsPEP;

	/** The list participant. */
	private List<Participant> listParticipant;

	/** The list juridic class. */
	private List<ParameterTable> listJuridicClass;

	/** The list person type. */
	private List<ParameterTable> listPersonType;

	/** The list boolean. */
	private List<BooleanType> listBoolean;

	/** The list motives. */
	private List<ParameterTable> listMotives;

	/** The list state holder request. */
	private List<ParameterTable> listStateHolderRequest;

	/** The list economic sector. */
	private List<ParameterTable> listEconomicSector;

	/** The list economic activity. */
	private List<ParameterTable> listEconomicActivity;
	
	private List<InvestorTypeModelTO> listInvestorType;
	
	/** The list economic activity legal representative. */
	private List<ParameterTable> listEconomicActivityLegalRepresentative;

	/** The list document type. */
	private List<ParameterTable> listDocumentType;
	
	/** The list second document type. */
	private List<ParameterTable> listSecondDocumentType;

	/** The list document type result. */
	private List<ParameterTable> listDocumentTypeResult;

	/** The list document type legal. */
	private List<ParameterTable> listDocumentTypeLegal;

	/** The list document type legal result. */
	private List<ParameterTable> listDocumentTypeLegalResult;
	
	/** The list second document type legal. */
	private List<ParameterTable> listSecondDocumentTypeLegal;

	/** The list category pep. */
	private List<ParameterTable> listCategoryPep;

	/** The list role pep. */
	private List<ParameterTable> listRolePep;
	
	/** The list grade pep. */
	private List<ParameterTable> listPepGrade;
	
	private List<ParameterTable> listPepGradeWithPersonType;

	/** The list sex. */
	private List<ParameterTable> listSex;

	/** The list representative class. */
	private List<ParameterTable> listRepresentativeClass;

	/** The list geographic location. */
	private List<GeographicLocation> listGeographicLocation;
	
	/** The list geographic location without national country. */
	private List<GeographicLocation> listGeographicLocationWithoutNationalCountry;

	/** The list province location. */
	private List<GeographicLocation> listProvinceLocation;

	/** The list department location. */
	private List<GeographicLocation> listDepartmentLocation;

	/** The list sector location. */
	private List<GeographicLocation> listMunicipalityLocation;

	/** The list department postal location. */
	private List<GeographicLocation> listDepartmentPostalLocation;
	
	/** The list province postal location. */
	private List<GeographicLocation> listProvincePostalLocation;

	/** The list sector postal location. */
	private List<GeographicLocation> listMunicipalityPostalLocation;

	/** The disabled cmb postal country. */
	private boolean disabledCmbPostalCountry;

	/** The disabled cmb postal province. */
	private boolean disabledCmbPostalProvince;

	/** The disabled cmb postal sector. */
	private boolean disabledCmbPostalMunicipality;

	/** The disabled input postal address. */
	private boolean disabledInputPostalAddress;

	/** The disabled flagSendASFI */
	private boolean flagflagSendASFI;
	
	/** The disabled Mnemonic ASFI and FONDO. */
	private boolean flagSendASFI;
	
	private List<ParameterTable> listEconomicActivityNaturalPerson;
	
	/** The general pameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;

	/** The holder service facade. */
	@EJB
	HolderServiceFacade holderServiceFacade;

	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
	@EJB
	InvestorTypeFacade investorTypeFacade;
	
	/** The participant service bean. */
	@EJB ParticipantServiceBean participantServiceBean;
	
	/** The lst parameter archive header. */
	private List<ParameterTable> lstParameterArchiveHeader;

	/** The lst holder req file history. */
	private List<HolderReqFileHistory> lstHolderReqFileHistory;

	/** The lst legal representative file. */
	private List<LegalRepresentativeFile> lstLegalRepresentativeFile;

	/** The lst holder file. */
	private List<HolderFile> lstHolderFile;

	/** The flag cmb representative class. */
	private boolean flagCmbRepresentativeClass;

	/** The file. */
	private UploadedFile file;

	/** The flag is participant. */
	private boolean flagIsParticipant;

	/** The flag action confirm. */
	private boolean flagActionConfirm;

	/** The flag action reject. */
	private boolean flagActionReject;

	/** The flag action annular. */
	private boolean flagActionAnnular;

	/** The flag action aprove. */
	private boolean flagActionAprove;

	/** The flag action modify. */
	private boolean flagActionModify;

	/** The disabled cmb participant. */
	private boolean disabledCmbParticipant;
	
	/** The participant af p_ fi c_ pat. */
	private boolean participantAFP_FIC_PAT;
	
	/** The show motive text. */
	private boolean showMotiveText;

	/** The request motive. */
	private Integer requestMotive;

	/** The request other motive. */
	private String requestOtherMotive;

	/** The numeric type document holder. */
	private boolean numericTypeDocumentHolder;

	/** The numeric second type document holder. */
	private boolean numericSecondTypeDocumentHolder;

	/** The numeric type document legal. */
	private boolean numericTypeDocumentLegal;

	/** The numeric second type document legal. */
	private boolean numericSecondTypeDocumentLegal;

	/** The max lenght document number holder. */
	private Integer maxLenghtDocumentNumberHolder;

	/** The max lenght second document number holder. */
	private Integer maxLenghtSecondDocumentNumberHolder;

	/** The max lenght document number legal. */
	private Integer maxLenghtDocumentNumberLegal;

	/** The max lenght second document number legal. */
	private Integer maxLenghtSecondDocumentNumberLegal;

	/** The flag holder indicator inability. */
	private boolean flagHolderIndicatorInability;

	/** The flag indicator minor holder. */
	private boolean flagIndicatorMinorHolder;

    /** The flag national resident. */
    private boolean flagNationalResident;
	
	/** The flag dominican not resident. */
	private boolean flagNationalNotResident;
	
	/** The flag foreign not resident. */
	private boolean flagForeignNotResident;

	/** The flag foreign resident. */
	private boolean flagForeignResident;
	/** The indicator pep. */
	private boolean indicatorPep;

	/** The legal representative history detail. */
	private LegalRepresentativeHistory legalRepresentativeHistoryDetail;

	/** The btn modify view. */
	private boolean btnModifyView;
	
	/** The btn confirm view. */
	private boolean btnConfirmView;
	
	/** The btn reject view. */
	private boolean btnRejectView;
	
	/** The btn approve view. */
	private boolean btnApproveView;
	
	/** The btn annular view. */
	private boolean btnAnnularView;
	
	/** The attach file holder selected. */
	private Integer attachFileHolderSelected;
	
	/** The list holder document type. */
	private List<ParameterTable> listHolderDocumentType;
	
	/** The holder file name display. */
	private String holderFileNameDisplay;
	
	/** The disabled ind resident. */
	private boolean disabledIndResident = false;
	
	/** The disabled nationality. */
	private boolean disabledNationality = false;
	
	/** The disabled representative nationality. */
	private boolean disabledRepresentativeNationality = false;
	
	/** The disabled representative resident. */
	private boolean disabledRepresentativeResident = false;
	
	/** The rnc result find. */
	private InstitutionInformation nitResultFind;
	
	/** The flag add representative modify. */
	private boolean flagAddRepresentativeModify;
	
	/** The flag modify representative modify. */
	private boolean flagModifyRepresentativeModify;
	
	/** The flag export data holder. */
	private boolean flagExportDataHolder;
	
	/** The flag export data representative. */
	private boolean flagExportDataRepresentative;
	
	/** The notification service facade. */
	@EJB NotificationServiceFacade notificationServiceFacade;
	
	/** The flag minor age. */
	private boolean flagMinorAge;
	
	/** The message configuration. */
	@Inject
	@InjectableResource(location = "Messages_es.properties")
	private Properties messageConfiguration;
	
    /** The business process. */
    BusinessProcess businessProcess = new BusinessProcess();
	
    /** The document validator. */
    @Inject
    DocumentValidator documentValidator;
    
    /** The massive holder to. */
    private MassiveHolderTO massiveHolderTO;
    
    /** The disabled cmb postal departament. */
    private boolean disabledCmbPostalDepartament;

    /** The legal representative helper bean. */
    @Inject
    private LegalRepresentativeHelperBean legalRepresentativeHelperBean;
	
    /** The streamed content file. */
    
    private transient StreamedContent streamedContentFile;
    
    /** The request state description. */
    private Map<Integer,String> requestStateDescription = null;
	
    /** The document type description. */
    private Map<Integer,String> documentTypeDescription = null;
	
    /** The list emission source. */
    private List<ParameterTable> listDocumentSource;
    
    /** The list document issuance date. */
    private LinkedHashMap<String,Integer>listDocumentIssuanceDate;
    
    /** The document to. */
    private DocumentTO documentTO;
    
    /** The client information validator. */
    @Inject
    ClientInformationValidator clientInformationValidator;
    
    /** The client information to. */
    private ClientInformationTO clientInformationTO;
    
    /** The list civil status. */
    private List<ParameterTable> listCivilStatus;
    
    /** The list total income. */
    private List<ParameterTable> listTotalIncome;
    
    /** The parameter service bean. */
    @EJB ParameterServiceBean parameterServiceBean;
    
    /** The file photo holder. */
    private transient StreamedContent filePhotoHolder, fileSigning;
    
    /** The photo holder name. */
	private String photoHolderName, signingHolderName;
	
	/** The file photo temporal. */
    private byte[] filePhotoTemp, fileSigningTemp;
    
    /** The aux fupl photo. */
    private boolean auxFuplPhoto, auxFuplSigning;
    
    /** The form state. */
    private boolean formState;
    
    /** The flag economic activity. */
    private boolean flagEconomicActivity;
	
	/** The disabled fund administrator. */
	private boolean disabledFundAdministrator;
	
	/** The disabled fund type. */
	private boolean disabledFundType;
	
	/** The disabled transfer number. */
	private boolean disabledTransferNumber;
	
	/** The ind ficpat economic activity. */
	private boolean indFICPATEconomicActivity;
	
	/** The Constant STYLE_REQUIRED. */
	private static final String STYLE_REQUIRED = "required-field";

	/** The str style required cui. */
	private String strStyleRequiredCUI;
	
	/** The str style required fund type. */
	private String strStyleRequiredFundType;
	
	/** The str style required fund adm. */
	private String strStyleRequiredFundAdm;
	
	/** The str style required transfer nro. */
	private String strStyleRequiredTransferNro;
	
	/** The holder result. */
	private Holder holderResult;
	
	/** The flag is issuer dpf institution. */
	private boolean flagIsIssuerDpfInstitution;
	
	/** The new document number. */
	private String newDocumentNumber;
	
	/** The new document number. */
	private String newDocumentNumberSecondNationality;
	
	private boolean disabledFideicomiso = false;
	
	private List<ParameterTable> lstCategoryPepWithPersonType;
	
	private final Integer linkForFamily = PepMotiveType.LINK_FOR_FAMILY.getCode();
	
    /**
	 * Instantiates a new holder mgmt bean.
	 */
	public HolderMgmtBean() {
		massiveHolderTO = new MassiveHolderTO();
		holderFilter = new HolderRequestTO();
		requestStateDescription = new HashMap<Integer,String>();
		documentTypeDescription = new HashMap<Integer,String>();	
		listDocumentIssuanceDate = new LinkedHashMap<String,Integer>();
		documentTO = new DocumentTO();
		clientInformationTO = new ClientInformationTO();
		participant = new Participant();
		holderResult = new Holder();
		newDocumentNumber = null;
		newDocumentNumberSecondNationality = null;
		
		// deshabilitando el Flag para envio a la ASFI issue 620
		flagflagSendASFI = false;
	}
	
	/**
	 * Valiables en False.
	 */
	public void inFalseFlagNationalForeign() {		
		flagNationalResident = false;
		flagNationalNotResident = false;		
		flagForeignResident = false;
		flagForeignNotResident = false;
	}
	
	/**
	 * Metodo para Cargar Pagina de Registro Masivo.
	 */
	public void loadRegistrationMassivePage(){
		  massiveHolderTO = new MassiveHolderTO();
		  getLstParticipant();
		  massiveHolderTO.setProcessDate(CommonsUtilities.currentDate());
		  
		  newDocumentNumber = null;
		  newDocumentNumberSecondNationality = null;
			
		  if (validateIsParticipant()) {
		   disabledCmbParticipant = true;
		   massiveHolderTO.setParticipantSelected(userInfo.getUserAccountSession().getParticipantCode());
		   
		  } else {
		   disabledCmbParticipant = false;
		   massiveHolderTO.setParticipantSelected(null);
		  } 
	}

		
	
	/**
	 * Metodo que Valida misma Nacionalidad
	 *
	 * @param idComponent the id component
	 * @return true, if successful
	 */
	public boolean validateSameNationality(String idComponent) {

		boolean flag = false;
		if (holderHistory.getNationality() != null
				&& holderHistory.getSecondNationality() != null
				&& (holderHistory.getNationality().equals(holderHistory
						.getSecondNationality()))) {
			JSFUtilities
					.addContextMessage(
							idComponent,
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities
									.getMessage(PropertiesConstants.WARNING_SAME_NATIONALITY),
							PropertiesUtilities
									.getMessage(PropertiesConstants.WARNING_SAME_NATIONALITY));

			holderHistory.setSecondNationality(null);
			flag = true;
		}else{
			loadHolderSecondDocumentType();
		}
		validateSecondNationalityByIndResident();
		
		if(holderHistory.getSecondNationality()==null){
			holderHistory.setSecondDocumentType(null);
			holderHistory.setSecondDocumentNumber(null);
			SelectOneMenu cmbSecondDocumentType = (SelectOneMenu)  FacesContext.getCurrentInstance().getViewRoot().findComponent("frmHolderRegister:tabView:cmbSecondDocumentType");
			if(cmbSecondDocumentType!=null){cmbSecondDocumentType.setValid(true);}
			InputText inputSecondDocumentNumber = (InputText)  FacesContext.getCurrentInstance().getViewRoot().findComponent("frmHolderRegister:tabView:inputSecondDocumentNumber");
			if(inputSecondDocumentNumber!=null){inputSecondDocumentNumber.setValid(true);}
		}
		return flag;
	}
	
	/**
	 * Metodo Para Cargar Segundo tipo de Documento.
	 */
	public void loadHolderSecondDocumentType() {
		
		PersonTO personTo = new PersonTO();
		personTo.setFlagHolderIndicator(true);
		if(holderHistory.getHolderType()!=null){
			personTo.setPersonType(holderHistory.getHolderType());
		}
		if (holderHistory.getSecondNationality() != null
				&& (holderHistory.getSecondNationality().equals(countryResidence))) {
			if (BooleanType.YES.getCode().equals(
					holderHistory.getIndResidence())) {
				personTo.setFlagNationalResident(true);
			} else if (BooleanType.NO.getCode().equals(
					holderHistory.getIndResidence())) {
				personTo.setFlagNationalNotResident(true);
			}
		} else if (holderHistory.getSecondNationality() != null
				&& !(holderHistory.getSecondNationality().equals(countryResidence))) {
			personTo.setFlagForeignResident(false);
			personTo.setFlagForeignNotResident(true);
		}
		if(flagHolderIndicatorInability){
			personTo.setFlagInabilityIndicator(true);
		}
		if (holderHistory.getHolderType().equals(PersonType.NATURAL.getCode())
				&& holderHistory.getIndMinor()!=null && holderHistory.getIndMinor().equals(BooleanType.YES.getCode())) {
			personTo.setFlagMinorIndicator(true);
		}
		if(holderHistory.getHolderType()!=null && holderHistory.getHolderType().equals(PersonType.JURIDIC.getCode())){
			if(holderHistory.getJuridicClass()!=null && holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())){
				personTo.setFlagJuridicClassTrust(true);
			}
			else{
				personTo.setFlagJuridicClassTrust(false);
			}
		}
		else{
			personTo.setFlagJuridicClassTrust(false);
		}
		listSecondDocumentType = documentValidator.getDocumentTypeByPerson(personTo);
		newDocumentNumberSecondNationality = null;
	}
	

	/**
	 * Metodo que Valida Nacionalidad Extranjera.
	 *
	 * @param idComponent the id component
	 */
	public void validateNationalForeign(String idComponent) {

		if (!validateSameNationality(idComponent)) {

			inFalseFlagNationalForeign();

			if (holderHistory.getNationality() != null
					&& (holderHistory.getNationality()
							.equals(countryResidence))) {
				if (BooleanType.YES.getCode().equals(
						holderHistory.getIndResidence())) {
					flagNationalResident = true;
				} else if (BooleanType.NO.getCode().equals(
						holderHistory.getIndResidence())) {
					flagNationalNotResident = true;
				}
			} else if (holderHistory.getNationality() != null
					&& !(holderHistory.getNationality()
							.equals(countryResidence))) {
				if (holderHistory.getIndResidence()!=null && BooleanType.YES.getCode().equals(
						holderHistory.getIndResidence())) {
					flagForeignResident = true;
				} else if (holderHistory.getIndResidence()!=null && BooleanType.NO.getCode().equals(
						holderHistory.getIndResidence())) {
					flagForeignNotResident = true;
				}
			}
		}
		loadingDocumentAttachByHolder();
	}

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {

		try {
			if (!FacesContext.getCurrentInstance().isPostback()) {
				listHolidays();	
				holderFilter.setInitialDate(CommonsUtilities.currentDate());
				holderFilter.setEndDate(CommonsUtilities.currentDate());
				getLstParticipant();
				getLstStateHolderRequestType();
				holderFilter.setParticipantSelected(Long.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER));
				nitResultFind = null;
				if (validateIsParticipant()) {
					disabledCmbParticipant = true;
					holderFilter.setParticipantSelected(userInfo.getUserAccountSession().getParticipantCode());
					Participant filter = new Participant();
					filter.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
					listParticipant = accountsFacade.getLisParticipantServiceBean(filter);				
				} else {
					
					// Add validations participant DPF
					flagIsIssuerDpfInstitution = false;															
					Long idParticipantCode = getIssuerDpfInstitution(userInfo);			
					if(Validations.validateIsNotNullAndNotEmpty(idParticipantCode)){
						disabledCmbParticipant = true;
						flagIsIssuerDpfInstitution = true;	
						holderFilter.setParticipantSelected(idParticipantCode);			
					} else {
						if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
							disabledCmbParticipant = true;
						} else {
							disabledCmbParticipant = false;
						}				
						flagIsIssuerDpfInstitution = false;	
						holderFilter.setParticipantSelected(null);
					}										
				}
				
				ParameterTableTO  parameterTableTO = new ParameterTableTO();
				
				parameterTableTO.setMasterTableFk(MasterTableType.STATE_CREATION_HOLDER_REQUEST.getCode());
				for(ParameterTable param : generalParametersFacade.getListParameterTableServiceBean(parameterTableTO)){
					requestStateDescription.put(param.getParameterTablePk(), param.getParameterName());
				}
				
				parameterTableTO.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
				for(ParameterTable param : generalParametersFacade.getListParameterTableServiceBean(parameterTableTO)) {
					documentTypeDescription.put(param.getParameterTablePk(), param.getParameterName());
				}
				Date today = new Date();
				DateFormat df = new SimpleDateFormat("yyyy");
				int year = Integer.parseInt(df.format(today));
				
				for(int i=1900;i<=year;i++){
						listDocumentIssuanceDate.put(String.valueOf(i),i);
				}
				
				getLstCivilStatus();
				getLstTotalIncome();
				//limpiando filtros ocultos
				cleanStylesEconomicActivity();
				indFICPATEconomicActivity = false;
				flagEconomicActivity = false;
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Flag actions in false.
	 */
	public void flagActionsInFalse() {
		flagActionAnnular = false;
		flagActionConfirm = false;
		flagActionReject = false;
		flagActionAprove = false;
		flagActionModify = false;
	}

	/**
	 * Validate is participant.
	 * 
	 * @return true, if successful
	 */
	public boolean validateIsParticipant() {

		flagIsParticipant = false;
		if(userInfo.getUserAccountSession().isParticipantInstitucion() || userInfo.getUserAccountSession().isInstitution(InstitutionType.AFP)
				|| userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
			flagIsParticipant = true;	
		}		
		return flagIsParticipant;
	}
	
	/**
	 * Seteamos Los BotonesSets the buttons listener.
	 */
	public void setButtonsListener(){
		
		if (!FacesContext.getCurrentInstance().isPostback()) {
				PrivilegeComponent privilegeComponent = new PrivilegeComponent();
			if(!getViewOperationType().equals(ViewOperationsType.DETAIL.getCode())){
			if(holderRequestDetail.getStateHolderRequest().equals(HolderRequestStateType.REGISTERED.getCode())){		
				if(getViewOperationType().equals(ViewOperationsType.APPROVE.getCode())){
					privilegeComponent.setBtnApproveView(true);		
				}
				if(getViewOperationType().equals(ViewOperationsType.ANULATE.getCode())){
					privilegeComponent.setBtnAnnularView(true);
				}
			}
			else if(holderRequestDetail.getStateHolderRequest().equals(HolderRequestStateType.MODIFIED.getCode())){
				if(getViewOperationType().equals(ViewOperationsType.APPROVE.getCode())){
					privilegeComponent.setBtnApproveView(true);		
				}
				if(getViewOperationType().equals(ViewOperationsType.ANULATE.getCode())){
					privilegeComponent.setBtnAnnularView(true);
				}
			}
			else if(holderRequestDetail.getStateHolderRequest().equals(HolderRequestStateType.APPROVED.getCode())){
				
				if(getViewOperationType().equals(ViewOperationsType.CONFIRM.getCode())){
					
				    privilegeComponent.setBtnConfirmView(true);
				}
				
				if(getViewOperationType().equals(ViewOperationsType.REJECT.getCode())){
					privilegeComponent.setBtnRejectView(true);
				}
			}
		}
		userPrivilege.setPrivilegeComponent(privilegeComponent);
		}
		
	}

	/**
	 * Metodo que Confirma Solicitud de Titular.
	 *
	 * @throws Exception the exception
	 */
	@LoggerAuditWeb
	public void confirmHolderRequest() throws Exception{

		try {
			
			Holder holder =holderServiceFacade.confirmRequestHolderServiceFacade(holderRequestDetail,holderHistoryDetail);
		    if(holder.getIdHolderPk()!=null){
		    	Object[] bodyData = new Object[2];
		    	bodyData[0] = holderRequestSelected.getIdHolderRequestPk().toString();
				bodyData[1] = holder.getIdHolderPk().toString();
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						PropertiesUtilities.getMessage(PropertiesConstants.LBL_SUCCESS_CONFIRM_HOLDER_REQUEST,
								bodyData));
				JSFUtilities.executeJavascriptFunction("PF('widgTransactionDialog').show()");
				searchRequestHolders();								
				
				businessProcess = new BusinessProcess();
			    businessProcess.setIdBusinessProcessPk(BusinessProcessType.NEW_HOLDER_CREATION_REQUEST_CONFIRM.getCode());
				Object[] parameters = new Object[2];
				parameters[0]=holderRequestDetail.getIdHolderRequestPk().toString();				
				parameters[1]=holderRequestDetail.getHolder().getIdHolderPk().toString();
				Long idParticipantPk = holderRequestDetail.getParticipant().getIdParticipantPk();
				String idIssuerFk = participantServiceBean.getIssuerIsParticipant(holderRequestDetail.getParticipant().getIdParticipantPk());
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,idParticipantPk,idIssuerFk,parameters);//, holder.getIdHolderPk()
		    }
		} catch (ServiceException ex) {
			
			if(ex.getErrorService().equals(ErrorServiceType.HOLDER_EXIST)){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getExceptionMessage(ex.getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('widgTransactionDialog').show()");
						searchRequestHolders();
			}
		}
			catch(Exception ex){
				excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Approve holder request.
	 */
	@LoggerAuditWeb
	public void approveHolderRequest(){
		try {
			
			HolderRequest holderRequest =holderRequestDetail;// new HolderRequest();
			HolderHistory holderHistory = holderHistoryDetail;//new HolderHistory();
			List<LegalRepresentativeHistory> lstLegalRepresentativeHistory = new ArrayList<LegalRepresentativeHistory>();
			holderRequest.setStateHolderRequest(HolderRequestStateType.APPROVED.getCode());
			holderRequest.setApproveDate(CommonsUtilities.currentDate());
			holderRequest.setApproveUser(userInfo.getUserAccountSession().getUserName());
			holderHistory.setStateHolderHistory(HolderRequestStateType.APPROVED.getCode());
			lstLegalRepresentativeHistory = holderRequest.getLegalRepresentativeHistories();
			if(lstLegalRepresentativeHistory!=null && lstLegalRepresentativeHistory.size()>0){
				for (int i = 0; i < lstLegalRepresentativeHistory.size(); i++) {
					lstLegalRepresentativeHistory.get(i).setStateRepreHistory(HolderRequestStateType.APPROVED.getCode());
				}
			}
			List<HolderHistory> lstHolderHistory = new ArrayList<HolderHistory>();
			lstHolderHistory.add(holderHistory);
			holderRequest.setHolderHistories(lstHolderHistory);
			if(lstLegalRepresentativeHistory!=null && lstLegalRepresentativeHistory.size()>0){
			holderRequest.setLegalRepresentativeHistories(lstLegalRepresentativeHistory);
			}

			boolean flagTransaction = holderServiceFacade
					.updateStateApproveRegisterHolderRequestServiceFacade(holderRequest, null,ViewOperationsType.APPROVE.getCode());
			if (flagTransaction) {
				Object[] bodyData = new Object[1];
				bodyData[0] = holderRequestSelected.getIdHolderRequestPk().toString();
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						PropertiesUtilities.getMessage(PropertiesConstants.LBL_SUCCESS_APPROVED_HOLDER_REQUEST,
								bodyData));
				JSFUtilities.executeJavascriptFunction("PF('widgTransactionDialog').show()");
				searchRequestHolders();
				businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.NEW_HOLDER_CREATION_REQUEST_APPROVE.getCode());
				Object[] parameters = new Object[2];
				parameters[1]=holderRequest.getParticipantDescription();
				parameters[0]=holderRequest.getIdHolderRequestPk().toString();
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,null,null,parameters);
			}

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Annular holder request.
	 */
	@LoggerAuditWeb
	public void annularHolderRequest() {

		try {
			
			JSFUtilities.executeJavascriptFunction("PF('dlgMotive').hide()");
			HolderRequest holderRequest = holderRequestDetail;
			HolderHistory holderHistory = holderHistoryDetail;
			List<LegalRepresentativeHistory> lstLegalRepresentativeHistory = new ArrayList<LegalRepresentativeHistory>();
			holderRequest.setStateHolderRequest(HolderRequestStateType.ANNULED.getCode());
			holderRequest.setAnnulDate(CommonsUtilities.currentDate());
			holderRequest.setAnnulUser(userInfo.getUserAccountSession().getUserName());
			holderRequest.setRequestOtherMotive(requestOtherMotive);
			holderRequest.setRequestMotive(requestMotive);
			holderHistory.setStateHolderHistory(LegalRepresentativeStateType.ANNULLED.getCode());
			lstLegalRepresentativeHistory = holderRequest.getLegalRepresentativeHistories();
			if(lstLegalRepresentativeHistory!=null && lstLegalRepresentativeHistory.size()>0){
				for (int i = 0; i < lstLegalRepresentativeHistory.size(); i++) {
					lstLegalRepresentativeHistory.get(i).setStateRepreHistory(
							HolderRequestStateType.ANNULED.getCode());
				}
			}
			List<HolderHistory> lstHolderHistory = new ArrayList<HolderHistory>();
			lstHolderHistory.add(holderHistory);
			holderRequest.setHolderHistories(lstHolderHistory);
			if(lstLegalRepresentativeHistory!=null && lstLegalRepresentativeHistory.size()>0){
			holderRequest.setLegalRepresentativeHistories(lstLegalRepresentativeHistory);
			}
			boolean flagTransaction = holderServiceFacade.updateStateAnulateRegisterHolderRequestServiceFacade(holderRequest, null,ViewOperationsType.ANULATE.getCode());
			if (flagTransaction) {
				Object[] bodyData = new Object[1];
				bodyData[0] = holderRequestSelected.getIdHolderRequestPk().toString();
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						PropertiesUtilities.getMessage(PropertiesConstants.LBL_SUCCESS_ANNUL_HOLDER_REQUEST,
								bodyData));
				JSFUtilities.executeJavascriptFunction("PF('widgTransactionDialog').show()");
				searchRequestHolders();
				businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.NEW_HOLDER_CREATION_REQUEST_CANCEL.getCode());
				Object[] parameters = new Object[1];
				parameters[0]=holderRequest.getIdHolderRequestPk().toString();				
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,holderRequest.getParticipant().getIdParticipantPk(),parameters);
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Modify holder request.
	 */
	@LoggerAuditWeb
	public void modifyHolderRequest() {

		try {
			
			HolderRequest auxHolderRequest = new HolderRequest();
			Participant auxParticipant = new Participant();
			HolderHistory auxHolderHistory = new HolderHistory();
			auxHolderHistory = holderHistory;
			auxHolderRequest = holderRequest;
			auxParticipant = participant;
			auxHolderRequest.setParticipant(auxParticipant);
			auxHolderRequest.setStateHolderRequest(HolderRequestStateType.MODIFIED
					.getCode());
			if(auxHolderHistory.getSecondNationality()==null){
				auxHolderHistory.setSecondDocumentType(null);
				auxHolderHistory.setSecondDocumentNumber(null);
			}
			if (auxHolderHistory.getSecondDocumentType() == null){
				auxHolderHistory.setSecondDocumentNumber(null);
			}
			if(auxHolderHistory.getSecondDocumentNumber() == null){
				auxHolderHistory.setSecondDocumentType(null);
			}
			if (auxHolderHistory.isIndicatorMinor()) {
				auxHolderHistory.setIndMinor(BooleanType.YES.getCode());
			} else {
				auxHolderHistory.setIndMinor(BooleanType.NO.getCode());
			}
			if (auxHolderHistory.getHolderType().equals(
					PersonType.JURIDIC.getCode())) {
				if(auxHolderHistory.getJuridicClass()!=null && auxHolderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())){
					auxHolderHistory.setDocumentNumber(auxHolderHistory.getDocumentNumber());
				}
				auxHolderHistory.setIndMinor(BooleanType.NO.getCode());
				auxHolderHistory.setIndDisabled(BooleanType.NO.getCode());
			}
			if (Validations.validateIsNull(auxHolderHistory.getEmail())
					|| Validations.validateIsEmpty(auxHolderHistory.getEmail())) {
				InputText objInputText = (InputText) FacesContext
						.getCurrentInstance().getViewRoot()
						.findComponent("frmModifyHolderRequest:tabView:inputEmail");
				objInputText.setDisabled(true);
			}else{
				auxHolderHistory.setEmail(auxHolderHistory.getEmail().toLowerCase());
			}
			auxHolderHistory.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
			auxHolderHistory.setRegistryDate(CommonsUtilities.currentDate());
			auxHolderHistory.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			auxHolderHistory.setStateHolderHistory(HolderRequestStateType.MODIFIED.getCode());
			auxHolderHistory.setHolderHistoryType(HolderRequestType.CREATION.getCode());
			List<HolderHistory> lstHolderHistory = new ArrayList<HolderHistory>();
			lstHolderHistory.add(auxHolderHistory);
			auxHolderRequest.setHolderHistories(lstHolderHistory);
			if(lstHolderReqFileHistory!=null && lstHolderReqFileHistory.size()>0){
				auxHolderRequest.setHolderReqFileHistories(lstHolderReqFileHistory);
			}
			
			auxHolderRequest.setLegalRepresentativeHistories(legalRepresentativeHelperBean.getLstLegalRepresentativeHistory());
			boolean flagTransaction = holderServiceFacade
					.modifyHolderRequestServiceFacade(auxHolderRequest);
			if (flagTransaction) {
				Object[] bodyData = new Object[1];
				bodyData[0] = holderRequestSelected.getIdHolderRequestPk().toString();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						PropertiesUtilities.getMessage(PropertiesConstants.LBL_SUCCESS_MODIFICATION_HOLDER_REQUEST,
								bodyData));
				JSFUtilities.executeJavascriptFunction("PF('endTrasaction').show()");
				searchRequestHolders();
				businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.NEW_HOLDER_CREATION_REQUEST_MODIFICATION.getCode());
				Object[] parameters = new Object[2];
				parameters[0]=holderRequest.getParticipantDescription();
				parameters[1]=holderRequest.getIdHolderRequestPk().toString();				
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,holderRequest.getParticipant().getIdParticipantPk(),parameters);
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Metodo Rechaza Solicitud de Titular.
	 */
	@LoggerAuditWeb
	public void rejectHolderRequest() {

		try {
			JSFUtilities.executeJavascriptFunction("PF('dlgMotive').hide()");
			HolderRequest holderRequest = holderRequestDetail;
			HolderHistory holderHistory = holderHistoryDetail;
			List<LegalRepresentativeHistory> lstLegalRepresentativeHistory = new ArrayList<LegalRepresentativeHistory>();
			holderRequest.setStateHolderRequest(HolderRequestStateType.REJECTED
					.getCode());
			holderRequest.setRejectDate(CommonsUtilities.currentDate());
			holderRequest.setRejectUser(userInfo.getUserAccountSession().getUserName());
			holderRequest.setRequestOtherMotive(requestOtherMotive);
			holderRequest.setRequestMotive(requestMotive);
			holderHistory.setStateHolderHistory(HolderRequestStateType.REJECTED.getCode());
			lstLegalRepresentativeHistory = holderRequest.getLegalRepresentativeHistories();
			if(lstLegalRepresentativeHistory!=null && lstLegalRepresentativeHistory.size()>0){
				for (int i = 0; i < lstLegalRepresentativeHistory.size(); i++) {
					lstLegalRepresentativeHistory.get(i).setStateRepreHistory(
							LegalRepresentativeStateType.REJECTED.getCode());
				}
			}
			List<HolderHistory> lstHolderHistory = new ArrayList<HolderHistory>();
			lstHolderHistory.add(holderHistory);
			holderRequest.setHolderHistories(lstHolderHistory);
			if(lstLegalRepresentativeHistory!=null && lstLegalRepresentativeHistory.size()>0){
			holderRequest.setLegalRepresentativeHistories(lstLegalRepresentativeHistory);
			}
			boolean flagTransaction = holderServiceFacade
					.updateStateRejectRegisterHolderRequestServiceFacade(holderRequest, null,ViewOperationsType.REJECT.getCode());
			if (flagTransaction) {
				Object[] bodyData = new Object[1];
				bodyData[0] = holderRequestSelected.getIdHolderRequestPk().toString();
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						PropertiesUtilities.getMessage(PropertiesConstants.LBL_SUCCESS_REJECT_HOLDER_REQUEST,
								bodyData));
				JSFUtilities.executeJavascriptFunction("PF('widgTransactionDialog').show()");
				searchRequestHolders();
				businessProcess = new BusinessProcess();
			    businessProcess.setIdBusinessProcessPk(BusinessProcessType.NEW_HOLDER_CREATION_REQUEST_REJECT.getCode());
				Object[] parameters = new Object[1];
				parameters[0]=holderRequest.getIdHolderRequestPk().toString();				
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),businessProcess,holderRequest.getParticipant().getIdParticipantPk(),parameters);
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Action do.
	 */
	@LoggerAuditWeb
	public void actionDo() {

		try {
			if (flagActionConfirm) {
				confirmHolderRequest();
			} else if (flagActionReject) {
				rejectHolderRequest();
			} else if (flagActionAnnular) {
				annularHolderRequest();
			} else if (flagActionModify) {
			     modifyHolderRequest();
			}
			else if(flagActionAprove){
				approveHolderRequest();
			}
		}
		catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Evaluate motive.
	 */
	public void evaluateMotive() {

		if (flagActionReject) {
			if (requestMotive!=null && requestMotive
					.equals(RejectMotivesHolderRequestType.OTHER_MOTIVES
							.getCode())) {
				requestOtherMotive = "";
				showMotiveText = true;

			} else {
				showMotiveText = false;
			}
		} else if (flagActionAnnular) {
			if (requestMotive!=null && requestMotive
					.equals(AnnularMotivesHolderRequestType.OTHER_MOTIVES
							.getCode())) {
				requestOtherMotive = "";
				showMotiveText = true;

			} else {
				showMotiveText = false;
			}
		}

	}
	
	
	/**
	 * Cargar Pagina de modificaciones.
	 */
	@LoggerAuditWeb
	public void loadModificationPage() {

		try {
			cleanFileImage();
			setViewOperationType(ViewOperationsType.MODIFY.getCode());
			List<LegalRepresentativeHistory> lstLegalRepresentativeHistory = new ArrayList<LegalRepresentativeHistory>();
			holderFileNameDisplay=GeneralConstants.EMPTY_STRING;
			participant = new Participant();
			holderRequest = new HolderRequest();
			holderHistory = new HolderHistory();
			holderRequest = holderServiceFacade
					.getHolderRequestByIdRequestServiceFacade(holderRequestSelected
							.getIdHolderRequestPk());
			holderHistory = holderRequest.getHolderHistories().get(0);
			lstHolderReqFileHistory = holderRequest.getHolderReqFileHistories();
			if(holderHistory.getFilePhoto() != null){
				setPhotoHolderName("photoHolder");
				JSFUtilities.putSessionMap("sessStreamedPhoto", holderHistory.getFilePhoto());
			}
			if(holderHistory.getFileSigning() != null){
				setSigningHolderName("signingHolder");			
				JSFUtilities.putSessionMap("sessStreamedSigning", holderHistory.getFileSigning());
			}						
			if(lstHolderReqFileHistory == null){
			   lstHolderReqFileHistory = new ArrayList<HolderReqFileHistory>();
			}
			disabledCountryResidentHolder=false;
			disabledIndResident=false;
			if ((holderRequest.getParticipant().getIdParticipantPk() != null)) {
				participant.setIdParticipantPk(holderRequest.getParticipant()
						.getIdParticipantPk());
			}
			getLstBooleanType();
			getLstDocumentType();
			getLstParticipant();
			getLstPersonType();
			getLstSexType();
			getLstJuridicClass();
			getLstGeographicLocation();
			getLstGeographicLocationWithoutNationalCountry();
			getLstRolePep();
			getGradePep();
			getListEconomicActivityWithoutSector();
			getLstCategoryPep();
			getLstEconomicSector();
			changeSelectedEconomicSector(Boolean.TRUE);
			getLstDocumentSource();
			listDepartmentLocation = getLstDepartmentLocation(holderHistory.getLegalResidenceCountry());
			listProvinceLocation = getLstProvinceLocation(holderHistory.getLegalDepartment());
			listMunicipalityLocation = getLstMunicipalityLocation(holderHistory.getLegalProvince());
			listDepartmentPostalLocation = getLstDepartmentLocation(holderHistory.getPostalResidenceCountry());
			listProvincePostalLocation = getLstProvinceLocation(holderHistory.getPostalDepartment());
			listMunicipalityPostalLocation = getLstMunicipalityLocation(holderHistory.getPostalProvince());
			documentTO = new DocumentTO();
			documentTO = documentValidator.loadMaxLenghtType(holderHistory.getDocumentType());
			numericTypeDocumentHolder  = documentTO.isNumericTypeDocument();
			maxLenghtDocumentNumberHolder = documentTO.getMaxLenghtDocumentNumber();
			documentTO = documentValidator.loadMaxLenghtType(holderHistory.getSecondDocumentType());
			maxLenghtSecondDocumentNumberHolder = documentTO.getMaxLenghtDocumentNumber();
			documentTO = documentValidator.validateEmissionRules(holderHistory.getDocumentType());
			numericSecondTypeDocumentHolder = documentTO.isNumericTypeDocument();
			if(holderHistory.getDocumentSource()!=null){
				documentTO.setIndicatorDocumentSource(true);
			}
			if (holderHistory.getHolderType().equals(
					PersonType.NATURAL.getCode())) {
				flagHolderJuridicPerson = false;
				flagHolderNaturalPerson = true;
				if(holderHistory.getIndResidence().equals(BooleanType.YES.getCode())){
					disabledCountryResidentHolder=true;
				}
			} else if (holderHistory.getHolderType().equals(
					PersonType.JURIDIC.getCode())) {

				flagHolderJuridicPerson = true;
				flagHolderNaturalPerson = false;
				
				if(holderHistory.getIndResidence().equals(BooleanType.YES.getCode())){
					disabledCountryResidentHolder=true;
				}
				if(holderHistory.getNationality().equals(countryResidence)){
					disabledCountryResidentHolder=true;
					disabledIndResident=true;
				}else{
					disabledCountryResidentHolder=true;
					disabledIndResident=true;
				}
				disabledRepresentativeButton = false;
			}
			if (holderHistory.getIndPEP()
					.equals(BooleanType.YES.getCode())) {
				holderHistory.setIndicatorPep(true);
				
			} else if (holderHistory.getIndPEP()
					.equals(BooleanType.NO.getCode())) {
				holderHistory.setIndicatorPep(false);
			}

			if (holderHistory.getHolderType().equals(
					PersonType.NATURAL.getCode())
					&& holderHistory.getIndDisabled() != null
					&& (holderHistory.getIndDisabled()
							.equals(BooleanType.YES.getCode()))) {
				holderHistory.setIndicatorDisabled(true);
				flagHolderIndicatorInability = false;
				disabledRepresentativeButton = true;
			} else if (holderHistory.getHolderType().equals(
					PersonType.NATURAL.getCode())
					&& holderHistory.getIndDisabled() != null
					&& (holderHistory.getIndDisabled()
							.equals(BooleanType.NO.getCode()))) {
				holderHistory.setIndicatorDisabled(false);
				flagHolderIndicatorInability = false;
			}

			if (holderHistory.getHolderType().equals(
					PersonType.NATURAL.getCode())
					&& holderHistory.getIndMinor()
							.equals(BooleanType.YES.getCode())) {
				holderHistory.setIndicatorMinor(true);
				flagIndicatorMinorHolder = true;
			} else if (holderHistory.getHolderType().equals(
					PersonType.NATURAL.getCode())
					&& holderHistory.getIndMinor()
							.equals(BooleanType.NO.getCode())) {
				holderHistory.setIndicatorMinor(false);
				flagIndicatorMinorHolder = false;
			}
			
			if(flagIndicatorMinorHolder || flagHolderIndicatorInability){
				disabledRepresentativeButton = false;
			}
			
			if (holderHistory.getNationality() != null
					&& (holderHistory.getNationality()
							.equals(countryResidence))) {
				if (BooleanType.YES.getCode().equals(
						holderHistory.getIndResidence())) {
					flagNationalResident = true;
				} else if (BooleanType.NO.getCode().equals(
						holderHistory.getIndResidence())) {
					flagNationalNotResident = true;
				}

			} else if (holderHistory.getNationality() != null
					&& !(holderHistory.getNationality()
							.equals(countryResidence))) {
				
				if (BooleanType.YES.getCode().equals(
						holderHistory.getIndResidence())) {
					flagForeignResident = true;
				} else if (BooleanType.NO.getCode().equals(
						holderHistory.getIndResidence())) {
					flagForeignNotResident = true;
				}

			}
			//Seteamos los datos
			if (holderRequest.getLegalRepresentativeHistories() != null) {
				for (int i = 0; i < holderRequest
						.getLegalRepresentativeHistories().size(); i++) {
					LegalRepresentativeHistory legalRepresentativeHistoryAux = new LegalRepresentativeHistory();
					if (holderRequest.getLegalRepresentativeHistories().get(i).getIndPEP()!=null && 
							holderRequest.getLegalRepresentativeHistories().get(i).getIndPEP().equals(BooleanType.YES.getCode())) {
						flagRepresentativeIsPEP = true;
						legalRepresentativeHistoryAux.setIndPEP(BooleanType.YES.getCode());
						legalRepresentativeHistoryAux.setCategory(holderRequest
								.getLegalRepresentativeHistories().get(i)
								.getCategory());
						legalRepresentativeHistoryAux.setRole(holderRequest
								.getLegalRepresentativeHistories().get(i)
								.getRole());
						legalRepresentativeHistoryAux
								.setBeginningPeriod(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getBeginningPeriod());
						legalRepresentativeHistoryAux
								.setEndingPeriod(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getEndingPeriod());
					} else if (holderRequest.getLegalRepresentativeHistories().get(i).getIndPEP()!=null && 
							holderRequest.getLegalRepresentativeHistories()
							.get(i).getIndPEP()
							.equals(BooleanType.NO.getCode())) {
						flagRepresentativeIsPEP = false;
						legalRepresentativeHistoryAux.setIndPEP(BooleanType.NO.getCode());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getIdRepresentativeHistoryPk() != null) {
						legalRepresentativeHistoryAux
								.setIdRepresentativeHistoryPk(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getIdRepresentativeHistoryPk());
					}
					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getBirthDate() != null) {
						legalRepresentativeHistoryAux
								.setBirthDate(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getBirthDate());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getDocumentNumber() != null) {
						legalRepresentativeHistoryAux
								.setDocumentNumber(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getDocumentNumber());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getDocumentType() != null) {
						legalRepresentativeHistoryAux
								.setDocumentType(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getDocumentType());
					}
					
					if (holderRequest.getLegalRepresentativeHistories().get(i).getDocumentIssuanceDate() != null) {
						legalRepresentativeHistoryAux.setDocumentIssuanceDate(holderRequest.getLegalRepresentativeHistories().get(i).getDocumentIssuanceDate());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getEconomicActivity() != null) {
						legalRepresentativeHistoryAux
								.setEconomicActivity(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getEconomicActivity());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getEconomicSector() != null) {
						legalRepresentativeHistoryAux
								.setEconomicSector(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getEconomicSector());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getEmail() != null) {
						legalRepresentativeHistoryAux.setEmail(holderRequest
								.getLegalRepresentativeHistories().get(i)
								.getEmail());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getFaxNumber() != null) {
						legalRepresentativeHistoryAux
								.setFaxNumber(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getFaxNumber());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getFirstLastName() != null) {
						legalRepresentativeHistoryAux
								.setFirstLastName(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getFirstLastName());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getFullName() != null) {
						legalRepresentativeHistoryAux.setFullName(holderRequest
								.getLegalRepresentativeHistories().get(i)
								.getFullName());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getHomePhoneNumber() != null) {
						legalRepresentativeHistoryAux
								.setHomePhoneNumber(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getHomePhoneNumber());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getIndResidence() != null) {

						legalRepresentativeHistoryAux
								.setIndResidence(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getIndResidence());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getLastModifyApp() != null) {
						legalRepresentativeHistoryAux
								.setLastModifyApp(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getLastModifyApp());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getLastModifyDate() != null) {
						legalRepresentativeHistoryAux
								.setLastModifyDate(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getLastModifyDate());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getLastModifyIp() != null) {
						legalRepresentativeHistoryAux
								.setLastModifyIp(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getLastModifyIp());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getLastModifyUser() != null) {
						legalRepresentativeHistoryAux
								.setLastModifyUser(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getLastModifyUser());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getLegalAddress() != null) {
						legalRepresentativeHistoryAux
								.setLegalAddress(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getLegalAddress());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getLegalDepartment() != null) {
						legalRepresentativeHistoryAux
								.setLegalDepartment(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getLegalDepartment());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getLegalDistrict() != null) {
						legalRepresentativeHistoryAux
								.setLegalDistrict(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getLegalDistrict());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getLegalProvince() != null) {
						legalRepresentativeHistoryAux
								.setLegalProvince(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getLegalProvince());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getLegalResidenceCountry() != null) {
						legalRepresentativeHistoryAux
								.setLegalResidenceCountry(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getLegalResidenceCountry());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getMobileNumber() != null) {
						legalRepresentativeHistoryAux
								.setMobileNumber(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getMobileNumber());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getName() != null) {
						legalRepresentativeHistoryAux.setName(holderRequest
								.getLegalRepresentativeHistories().get(i)
								.getName());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getNationality() != null) {
						legalRepresentativeHistoryAux
								.setNationality(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getNationality());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getOfficePhoneNumber() != null) {
						legalRepresentativeHistoryAux
								.setOfficePhoneNumber(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getOfficePhoneNumber());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getPostalDepartment() != null) {
						legalRepresentativeHistoryAux
								.setPostalDepartment(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getPostalDepartment());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getPostalDistrict() != null) {
						legalRepresentativeHistoryAux
								.setPostalDistrict(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getPostalDistrict());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getPostalProvince() != null) {
						legalRepresentativeHistoryAux
								.setPostalProvince(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getPostalProvince());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getPostalResidenceCountry() != null) {
						legalRepresentativeHistoryAux
								.setPostalResidenceCountry(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getPostalResidenceCountry());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getPostalAddress() != null) {
						legalRepresentativeHistoryAux
								.setPostalAddress(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getPostalAddress());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getRegistryUser() != null) {
						legalRepresentativeHistoryAux
								.setRegistryUser(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getRegistryUser());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getSecondDocumentNumber() != null) {
						legalRepresentativeHistoryAux
								.setSecondDocumentNumber(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getSecondDocumentNumber());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getSecondLastName() != null) {
						legalRepresentativeHistoryAux
								.setSecondLastName(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getSecondLastName());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getSecondNationality() != null) {
						legalRepresentativeHistoryAux
								.setSecondNationality(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getSecondNationality());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getSex() != null) {
						legalRepresentativeHistoryAux.setSex(holderRequest
								.getLegalRepresentativeHistories().get(i)
								.getSex());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getStateRepreHistory() != null) {
						legalRepresentativeHistoryAux
								.setStateRepreHistory(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getStateRepreHistory());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getPersonType() != null) {
						legalRepresentativeHistoryAux
								.setPersonType(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getPersonType());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getRepresentativeClass() != null) {
						legalRepresentativeHistoryAux
								.setRepresentativeClass(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getRepresentativeClass());
					}

					if (holderRequest.getLegalRepresentativeHistories().get(i)
							.getSecondDocumentType() != null) {
						legalRepresentativeHistoryAux
								.setSecondDocumentType(holderRequest
										.getLegalRepresentativeHistories()
										.get(i).getSecondDocumentType());
					}
					
					if(holderRequest.getLegalRepresentativeHistories().get(i)
						.getDocumentSource()!=null){
							legalRepresentativeHistoryAux.setDocumentSource(holderRequest.getLegalRepresentativeHistories().get(i)
							.getDocumentSource());
						}

					legalRepresentativeHistoryAux
							.setRepresentativeTypeDescription(PersonType.get(
									holderRequest
											.getLegalRepresentativeHistories()
											.get(i).getPersonType()).getValue());

					legalRepresentativeHistoryAux
							.setDocumentTypeDescription(DocumentType.get(
									holderRequest
											.getLegalRepresentativeHistories()
											.get(i).getDocumentType())
									.getValue());
					

					legalRepresentativeHistoryAux
							.setRepresenteFileHistory(holderRequest
									.getLegalRepresentativeHistories().get(i).getRepresenteFileHistory());
					
					legalRepresentativeHistoryAux.setFlagActiveFilesByHolderImport(true);
					lstLegalRepresentativeHistory.add(legalRepresentativeHistoryAux);
					
				}
				
				legalRepresentativeHelperBean.setLstLegalRepresentativeHistory(lstLegalRepresentativeHistory);
			}
			
			if (validateIsParticipant()) {
				disabledCmbParticipant = true;
				participant.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());				
			} else {				
				// Add validations participant DPF			
				Long idParticipantCode = getIssuerDpfInstitution(userInfo);							
				if(Validations.validateIsNotNullAndNotEmpty(idParticipantCode)){
					disabledCmbParticipant = true;				
					participant.setIdParticipantPk(idParticipantCode);
					flagIsIssuerDpfInstitution = true;
				} else {
					if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
						disabledCmbParticipant = true;
					} else {
						disabledCmbParticipant = false;
					}
					flagIsIssuerDpfInstitution = false;
					
					//participant.setIdParticipantPk(null);
				}										
			}
			
			loadHolderDocumentType();
			loadHolderSecondDocumentType();
			loadDocumentTypeHolderParameter();
			legalRepresentativeHelperBean.setFlagModifyLegalRepresentantive(true);
			legalRepresentativeHelperBean.getLegalRepresentativeTO().setViewOperationType(ViewOperationsType.MODIFY.getCode());

			Participant filter = new Participant();
			filter.setIdParticipantPk(participant.getIdParticipantPk());
			participant = accountsFacade.findParticipantByFilters(filter);
			
			if(participant!=null && participant.getAccountClass()!=null &&
					holderHistory.getHolderType()!=null){
				clientInformationTO=clientInformationValidator.validateShowClientInformation(participant.getAccountClass(),holderHistory.getHolderType());
			} 
			
			//cargando el numero de documento 1050
			holderHistory.setDocumentNumber(getDocumentNumber(holderHistory.getDocumentNumber(), holderHistory.getDocumentType()));
			if(holderHistory.getSecondDocumentType() != null && holderHistory.getSecondDocumentNumber() != null){
				holderHistory.setSecondDocumentNumber(getDocumentNumber(holderHistory.getSecondDocumentNumber(), holderHistory.getSecondDocumentType()));
				
				if(holderHistory.getSecondDocumentType().equals(DocumentType.CIE.getCode())){
					newDocumentNumberSecondNationality = "E-" + holderHistory.getSecondDocumentNumber();
				}
			}
			
			loadNewDocumentNumber();
			
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Issue 1050
	 * Cargando el numero de documento
	 */
	public String getDocumentNumber(String documentNumber, Integer documentType){
		
		if(documentType.equals(DocumentType.CIE.getCode())){
			if(documentNumber.length() > 2){
				return documentNumber.substring(2, documentNumber.length());
			}
		}
		if(documentType.equals(DocumentType.DCD.getCode()) ||
		   documentType.equals(DocumentType.DCC.getCode()) ||
		   documentType.equals(DocumentType.DCR.getCode()) ){
			
			if(documentNumber.length() > 6){
				return documentNumber.substring(3, documentNumber.length()-4);
			}
		}
		return documentNumber;
	}

	/**
	 * Metodo que Validia si es Copia.
	 * 
	 * @return true, if successful
	 */
	public boolean validateIsCopy() {

		boolean flag = true;

		if (!(holderHistory.getLegalResidenceCountry().equals(holderHistory
				.getPostalResidenceCountry()))) {
			flag = false;
		}
		if (!(holderHistory.getLegalProvince().equals(holderHistory
				.getPostalProvince()))) {
			flag = false;
		}
		if (!(holderHistory.getLegalDistrict().equals(holderHistory
				.getLegalDistrict()))) {
			flag = false;
		}
		if (!(holderHistory.getLegalAddress().equals(holderHistory
				.getLegalAddress()))) {
			flag = false;
		}

		return flag;

	}

	/**
	 * Validate view operation register.
	 *
	 * @return true, if successful
	 */
	public boolean validateViewOperationRegister(){
		boolean flag=false;
		
		if(isViewOperationRegister()){
		   flag=true;
		}
		
		return flag;
	}
	
	/**
	 * Validate view operation modify.
	 *
	 * @return true, if successful
	 */
	public boolean validateViewOperationModify(){
		boolean flag=false;
		if(isViewOperationModify()){
		   flag=true;
		}
		return flag;
	}
	
	/**
	 * Metodo que cambia Indicador Menor.
	 */
	public void onchangeIndicatorMinor() {

		try {
			if (holderHistory.getBirthDate() != null) {
				Calendar c = Calendar.getInstance();
				DateFormat df = new SimpleDateFormat("yyyy");
				c.setTime(df.parse("1900"));

				if (holderHistory.getBirthDate().getTime() > (new Date()
						.getTime())) {
					JSFUtilities
							.addContextMessage(
									"frmHolderRegister:tabView:trCal",
									FacesMessage.SEVERITY_ERROR,
									PropertiesUtilities
											.getMessage(PropertiesConstants.ERROR_ADM_HOLDER_REQUEST_DATE_BIRTHDAY_DATE_TODAY),
									PropertiesUtilities
											.getMessage(PropertiesConstants.ERROR_ADM_HOLDER_REQUEST_DATE_BIRTHDAY_DATE_TODAY));

					JSFUtilities.showMessageOnDialog(
							PropertiesConstants.LBL_WARNING_ACTION,
							null,
							PropertiesConstants.ERROR_ADM_HOLDER_REQUEST_DATE_BIRTHDAY_DATE_TODAY,
							null);

					JSFUtilities.showSimpleValidationDialog();
					return;
				}
				if (Integer.parseInt(df.format(holderHistory.getBirthDate())) < c
						.get(Calendar.YEAR)) {
					JSFUtilities
							.addContextMessage(
									"frmHolderRegister:tabView:trCal",
									FacesMessage.SEVERITY_ERROR,
									PropertiesUtilities
											.getMessage(PropertiesConstants.ERROR_ADM_HOLDER_REQUEST_DATE_BIRTHDAY_DATE_ONENINEZEROZEO),
									PropertiesUtilities
											.getMessage(PropertiesConstants.ERROR_ADM_HOLDER_REQUEST_DATE_BIRTHDAY_DATE_ONENINEZEROZEO));

					JSFUtilities.showMessageOnDialog(
							PropertiesConstants.LBL_WARNING_ACTION,
							null,
							PropertiesConstants.ERROR_ADM_HOLDER_REQUEST_DATE_BIRTHDAY_DATE_ONENINEZEROZEO,
							null);

					JSFUtilities.showSimpleValidationDialog();
					return;
				}
				SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
				String birthday = formato.format(holderHistory.getBirthDate());
				int age = calculateAge(birthday);
				
			 if (age >= 18) {
					
					flagIndicatorMinorHolder = false;
					holderHistory.setIndicatorMinor(false);
					holderHistory.setIndMinor(BooleanType.NO.getCode());
					if (holderHistory.getHolderType().equals(
							PersonType.NATURAL.getCode())
							&& !flagHolderIndicatorInability) {
						disabledRepresentativeButton = true;
					} else if (flagHolderIndicatorInability) {
						disabledRepresentativeButton = false;
					}
						loadHolderDocumentType();
						loadHolderSecondDocumentType();
						loadingDocumentAttachByHolder();
				} else {
					flagIndicatorMinorHolder = true;
					holderHistory.setIndicatorMinor(true);
					holderHistory.setIndMinor(BooleanType.YES.getCode());
					disabledRepresentativeButton = false;
						loadHolderDocumentType();
						loadHolderSecondDocumentType();
						loadingDocumentAttachByHolder();
				}
			}
			if(flagIndicatorMinorHolder){
				flagMinorAge = true; 
			}
			else{
				flagMinorAge = false;
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Calculate age.
	 *
	 * @param dateBirth the date birth
	 * @return the int
	 */
	public int calculateAge(String dateBirth) {

		Date dateToday = new Date();
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		String today = formato.format(dateToday);
		String[] dat1 = dateBirth.split("/");
		String[] dat2 = today.split("/");
		int years = Integer.parseInt(dat2[2]) - Integer.parseInt(dat1[2]) - 1;
		int month = Integer.parseInt(dat2[1]) - Integer.parseInt(dat1[1]);

		if (month < 0) {
			years = years + 0;
		} else if (month > 0) {
			years = years + 1;
		}

		if (month == 0) {
			int day = Integer.parseInt(dat2[0]) - Integer.parseInt(dat1[0]);
			if (day >= 0) {
				years = years + 1;
			}
		}
		return years;
	}

	/**
	 * Onchange indicator disabled.
	 */
	public void onchangeIndicatorDisabled() {
		if (holderHistory.isIndicatorDisabled()){
			flagHolderIndicatorInability = false;
			holderHistory.setIndDisabled(BooleanType.YES.getCode());
			disabledRepresentativeButton = true;

		} else {
			flagHolderIndicatorInability = false;
			holderHistory.setIndDisabled(BooleanType.NO.getCode());
			
			if(!holderHistory.isIndicatorMinor()){
				disabledRepresentativeButton = true;
			}
			else{
				disabledRepresentativeButton = false;
			}
		}
		loadHolderDocumentType();
		loadHolderSecondDocumentType();
		if(lstHolderReqFileHistory == null || lstHolderReqFileHistory.isEmpty() || lstHolderReqFileHistory.size() == 0 ){
			loadingDocumentAttachByHolder();
		}
	}

	/**
	 * Onchange is pep.
	 */
	public void onchangeIsPEP() {
		JSFUtilities.resetViewRoot();
		if (holderHistory.isIndicatorPep()) {

			holderHistory.setIndPEP(BooleanType.YES.getCode());
			holderHistory.setBeginningPeriod(CommonsUtilities.currentDate());
			holderHistory.setEndingPeriod(CommonsUtilities.currentDate());
		} else {
			holderHistory.setIndPEP(BooleanType.NO.getCode());
			holderHistory.setCategory(null);
			holderHistory.setRole(null);
			holderHistory.setBeginningPeriod(null);
			holderHistory.setEndingPeriod(null);

		}
	}
	
	/**
	 * Metodo que Obtiene Lista de Localizacion Geografica.
	 * 
	 * @return the lst geographic location
	 */
	public void getLstGeographicLocation() {

		try {

			GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
			geographicLocationTO
					.setGeographicLocationType(GeographicLocationType.COUNTRY
							.getCode());
			geographicLocationTO
					.setState(GeographicLocationStateType.REGISTERED.getCode());
			listGeographicLocation = generalParametersFacade
					.getListGeographicLocationServiceFacade(geographicLocationTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	
	/**
	 * Metodo que Obtiene Lista de Localizacion Geografica con Ciudad.
	 *
	 * @return the lst geographic location without national country
	 */
	public void getLstGeographicLocationWithoutNationalCountry(){

		try {

			listGeographicLocationWithoutNationalCountry = new ArrayList<GeographicLocation>();
			GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
			geographicLocationTO
					.setGeographicLocationType(GeographicLocationType.COUNTRY
							.getCode());
			geographicLocationTO
					.setState(GeographicLocationStateType.REGISTERED.getCode());

			listGeographicLocation = generalParametersFacade
					.getListGeographicLocationServiceFacade(geographicLocationTO);
			
			for(GeographicLocation geo : listGeographicLocation){
				if(!geo.getIdGeographicLocationPk().equals(countryResidence)){
					listGeographicLocationWithoutNationalCountry.add(geo);
				}
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Gets the lst province location.
	 * 
	 * @param referenceFk
	 *            the reference fk
	 * @return the lst province location
	 */
	public List<GeographicLocation> getLstProvinceLocation(Integer referenceFk) {

		List<GeographicLocation> lst = null;

		try {

			GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
			geographicLocationTO
					.setGeographicLocationType(GeographicLocationType.PROVINCE
							.getCode());
			geographicLocationTO.setIdLocationReferenceFk(referenceFk);
			lst = generalParametersFacade
					.getListGeographicLocationServiceFacade(geographicLocationTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

		return lst;

	}
	
	/**
	 * Metodo que Obtiene Lista de Departamentos.
	 *
	 * @param referenceFk the reference fk
	 * @return the lst department location
	 */
	public List<GeographicLocation> getLstDepartmentLocation(Integer referenceFk) {

		List<GeographicLocation> lst = null;

		try {
			GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
			geographicLocationTO
					.setGeographicLocationType(GeographicLocationType.DEPARTMENT
							.getCode());
			geographicLocationTO.setIdLocationReferenceFk(referenceFk);
			lst = generalParametersFacade
					.getListGeographicLocationServiceFacade(geographicLocationTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

		return lst;

	}

	/**
	 * Gets the lst sector location.
	 * 
	 * @param referenceFk
	 *            the reference fk
	 * @return the lst sector location
	 */
	public List<GeographicLocation> getLstMunicipalityLocation(Integer referenceFk) {

		List<GeographicLocation> lst = null;

		try {

			GeographicLocationTO geographicLocationTO = new GeographicLocationTO();

			geographicLocationTO
					.setGeographicLocationType(GeographicLocationType.DISTRICT
							.getCode());
			geographicLocationTO.setIdLocationReferenceFk(referenceFk);

			lst = generalParametersFacade
					.getListGeographicLocationServiceFacade(geographicLocationTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

		return lst;

	}

	/**
	 * Metodo que Obtiene lista de Participante.
	 * 
	 * @return the lst participant
	 */
	public void getLstParticipant() {

		try {

			Participant filter = new Participant();

			filter.setState(ParticipantStateType.REGISTERED.getCode());
			listParticipant = accountsFacade.getLisParticipantServiceBean(filter);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}

	/**
	 * Lista de Clases Juridicas.
	 * 
	 * @return the lst juridic class
	 */
	public void getLstJuridicClass() {

		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(MasterTableType.T_CLASE_JURIDICO_TITULAR
							.getCode());

			listJuridicClass = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}

	/**
	 * Metodo que Obtiene Lista de Tipos de Personas.
	 * 
	 * @return the lst person type
	 */
	public void getLstPersonType() {

		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.PERSON_TYPE
					.getCode());
			listPersonType = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}

	/**
	 * Metodo que Obtiene Lista de Sectores Economicos.
	 *
	 * @return the lst economic sector
	 */
	public void getLstEconomicSector() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.ECONOMIC_SECTOR
					.getCode());
			listEconomicSector = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}
	
	public void changeSelectedEconomicSector() {
		this.changeSelectedEconomicSector(Boolean.FALSE);
	}

	/**
	 * Cambia Seleccion de Sector Economico.
	 */
	public void changeSelectedEconomicSector(Boolean isFromModification) {
		try {
			Integer economicSector=null;
			if(isViewOperationRegister()|| isViewOperationModify()){
				economicSector = holderHistory.getEconomicSector();
			}
			else{
				economicSector = holderHistoryDetail.getEconomicSector();
			}
			listEconomicActivity = generalParametersFacade.getListEconomicActivityBySector(economicSector);
			listInvestorType = investorTypeFacade.getLstInvestorTypeModelTO(new InvestorTypeModelTO());
			if(!indFICPATEconomicActivity){
				showFieldsEconomicActivity(isFromModification);
			}
			
			// Validar la transferencia Extrabursatil
			validateIndCanTransfer(holderHistory);
			
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Gets the lst boolean type.
	 * 
	 * @return the lst boolean type
	 */
	public void getLstBooleanType() {
		listBoolean = BooleanType.list;
	}

	/**
	 * Obtiene Lista de Tipos de Motivos.
	 * 
	 * @return the lst motives type
	 */
	public void getLstRejectMotivesType() {

		try {

			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(MasterTableType.HOLDER_REQUEST_REJECT_MOTIVE
							.getCode());
			listMotives = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Metodo que obtiene Lista de Tipo de Motivos de Anulacion.
	 *
	 * @return the lst annular motives type
	 */
	public void getLstAnnularMotivesType() {

		try {

			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(MasterTableType.HOLDER_REQUEST_ANNULAR_MOTIVE
							.getCode());
			listMotives = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Obtiene Lista de Categorias.
	 *
	 * @return the lst category pep
	 */
	public void getLstCategoryPep() {

		try {

			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(MasterTableType.ASSET_LAUNDRY_MOTIVE_PEP
							.getCode());
			listCategoryPep = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		}
		catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}

	/**
	 * Lista de Roles.
	 *
	 * @return the lst role pep
	 */
	public void getLstRolePep() {

		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.PEP_CHARGE
					.getCode());
			listRolePep = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		}
		catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	public void getGradePep() {
		
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.PEP_GRADE
					.getCode());
			listPepGrade = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	public void getListEconomicActivityWithoutSector() {
		try {
			/*Integer economicSector=null;
			if(isViewOperationRegister()|| isViewOperationModify()){
				economicSector = holderHistory.getEconomicSector();
			}
			else{
				economicSector = holderHistoryDetail.getEconomicSector();
			}*/
			listEconomicActivityNaturalPerson = generalParametersFacade.getListEconomicActivityBySector(null);
			
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Obtiene Lista de Tipo de documento Legal.
	 *
	 * @return the lst document type legal
	 */
	public void getLstDocumentTypeLegal() {

		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON
							.getCode());
			listDocumentTypeLegal = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Metodo que Obtiene Lista de Tipo de Documentos.
	 *
	 * @return the lst document type
	 */
	public void getLstDocumentType() {

		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON
							.getCode());
			listDocumentType = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}
	
	/**
	 * Metodo que Obtiene Lista de Emision.
	 *
	 * @return the lst document emission source
	 */
	public void getLstDocumentSource() {

		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(MasterTableType.DOCUMENT_SOURCE.getCode());
			listDocumentSource = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	

	/**
	 * Metodo que Obtiene Lista estados de las solicitudes de Titular.
	 *
	 * @return the lst state holder request type
	 */
	public void getLstStateHolderRequestType() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(MasterTableType.STATE_CREATION_HOLDER_REQUEST
							.getCode());

			listStateHolderRequest = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			
			//Issue 1268 quitando el estado Modificado
			List<ParameterTable> listStateHolderRequestAux = new ArrayList<>();
			for (ParameterTable pt: listStateHolderRequest){
				if(!pt.getParameterTablePk().equals(HolderRequestStateType.MODIFIED.getCode())){
					listStateHolderRequestAux.add(pt);
				}
			}
			listStateHolderRequest = listStateHolderRequestAux;
			
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Metodo que obtiene Lista de estado Civil.
	 *
	 * @return the lst civil status
	 */
	public void getLstCivilStatus(){
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(MasterTableType.CIVIL_STATUS
							.getCode());
			listCivilStatus = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}		
	}

	/**
	 * Metodo que Obtiene Lista de Ingreso Total.
	 *
	 * @return the lst total income
	 */
	public void getLstTotalIncome(){
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(MasterTableType.TOTAL_INCOME
							.getCode());

			listTotalIncome = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}		
	}
	/**
	 * Metodo que Obtiene Lista de tipos de Sexo.
	 * 
	 * @return the lst sex type
	 */
	public void getLstSexType() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.SEX.getCode());

			listSex = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Metodo que Obtiene Lista de Tipos de Clases Representativas.
	 * 
	 * @return the lst representative class type
	 */
	public void getLstRepresentativeClassType() {

		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(MasterTableType.CLASS_REPRENTATIVE
							.getCode());

			listRepresentativeClass = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}

	/**
	 * Busqueda mediante Filtros.
	 *
	 * @return true, if successful
	 */
	public boolean validateSearchFilter() {

		boolean flag = true;

		if (holderFilter.getInitialDate().getTime() > holderFilter.getEndDate()
				.getTime()) {
			JSFUtilities.addContextMessage(
							"frmHolderRequestSearch:calInitialDate",
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities
									.getMessage(PropertiesConstants.ERROR_ADM_HOLDER_REQUEST_FILTER_DATE_INITIAL),
							PropertiesUtilities
									.getMessage(PropertiesConstants.ERROR_ADM_HOLDER_REQUEST_FILTER_DATE_INITIAL));
			holderRequestDataModel = null;
			flag = false;
		}
		if (holderFilter.getEndDate().getTime() > (new Date().getTime())) {
			JSFUtilities
					.addContextMessage(
							"frmHolderRequestSearch:calEndDate",
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities
									.getMessage(PropertiesConstants.ERROR_ADM_HOLDER_REQUEST_FILTER_DATE_FINAL_DATE_TODAY),
							PropertiesUtilities
									.getMessage(PropertiesConstants.ERROR_ADM_HOLDER_REQUEST_FILTER_DATE_FINAL_DATE_TODAY));
			holderRequestDataModel = null;
			flag = false;
		}
		if (holderFilter.getInitialDate().getTime() > (new Date().getTime())) {
			JSFUtilities
					.addContextMessage(
							"frmHolderRequestSearch:calInitialDate",
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities
									.getMessage(PropertiesConstants.ERROR_ADM_HOLDER_REQUEST_FILTER_DATE_INITIAL_DATE_TODAY),
							PropertiesUtilities
									.getMessage(PropertiesConstants.ERROR_ADM_HOLDER_REQUEST_FILTER_DATE_INITIAL_DATE_TODAY));
			holderRequestDataModel = null;
			flag = false;
		}
		return flag;

	}
	
	/**
	 * Sets the true valid buttons.
	 */
	public void setTrueValidButtons(){
		
		
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		
		if(userPrivilege.getUserAcctions().isModify()){
			privilegeComponent.setBtnModifyView(true);	
		}
		
		if(userPrivilege.getUserAcctions().isConfirm()){
			privilegeComponent.setBtnConfirmView(true);
		}
		if(userPrivilege.getUserAcctions().isReject()){
			privilegeComponent.setBtnRejectView(true);
		}
		if(userPrivilege.getUserAcctions().isAnnular()){
			privilegeComponent.setBtnAnnularView(true);
		}
		if(userPrivilege.getUserAcctions().isApprove()){
			privilegeComponent.setBtnApproveView(true);
		}
		userPrivilege.setPrivilegeComponent(privilegeComponent);
		
	}

	/**
	 * Metodo que busca Solicitudes de titular.
	 */
	@LoggerAuditWeb
	public void searchRequestHolders() {
		
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		//SI NO TIENE PARTICIPANTE Y ES EMISOR DPF NO PUEDE REGISTRAR
		if(userInfo.getUserAccountSession().isIssuerDpfInstitucion() && !flagIsIssuerDpfInstitution){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_PARTICIPANT));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		try {
			if (validateSearchFilter()) {
				holderFilter.setRequestType(HolderRequestType.CREATION.getCode());
				holderFilter.setRequestTypeAux(null);
				List<HolderRequest> lstResult = new ArrayList<HolderRequest>();
				lstResult = holderServiceFacade.getListCreationHolderRequestServiceFacade(holderFilter);
				if(lstResult!=null && lstResult.size()>0){
					for(int i=0;i<lstResult.size();i++){
						    if(lstResult.get(i).getStateHolderRequest()!=null){
						    	lstResult.get(i).setStateDescription(requestStateDescription.get(lstResult.get(i).getStateHolderRequest()).toString());
						    }
						    else{
						    	lstResult.get(i).setStateDescription(GeneralConstants.EMPTY_STRING);
						    }
						    if(lstResult.get(i).getHolderHistory()!=null && lstResult.get(i).getHolderHistory().getDocumentType()!=null){
						    	lstResult.get(i).getHolderHistory().setTypeDocumentDescription(documentTypeDescription.get(lstResult.get(i).getHolderHistory().getDocumentType()).toString());
						    }
						    else if(lstResult.get(i).getHolderHistory()!=null){
						    	lstResult.get(i).getHolderHistory().setTypeDocumentDescription(GeneralConstants.EMPTY_STRING);
						    }
					}			
				}
				holderRequestDataModel = new HolderRequestDataModel(lstResult);
				if(holderRequestDataModel.getRowCount()>0){
					setTrueValidButtons();
				}
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Metodo que Valida la residencia del Titular.
	 *
	 * @param idComponent the id component
	 */
	public void validateResidentHolder(String idComponent) {

		disabledIndResident = false;
		disabledCountryResidentHolder=false;
		flagCopyLegalHolder=false;		
		disabledCmbPostalCountry = false;
		disabledCmbPostalDepartament = false;
		disabledCmbPostalProvince = false;
		disabledCmbPostalMunicipality = false;
		disabledInputPostalAddress = false;
		documentTO = new DocumentTO();
		if (holderHistory.getHolderType()!=null && holderHistory.getHolderType().equals(PersonType.NATURAL.getCode())) {
			validateNationalForeign(idComponent);
			loadHolderDocumentType();
			loadingDocumentAttachByHolder();
		} else if (holderHistory.getHolderType()!=null && holderHistory.getHolderType().equals(
				PersonType.JURIDIC.getCode())) {
			if(holderHistory.getNationality()!=null && holderHistory.getNationality().equals(countryResidence)){
				holderHistory.setIndResidence(BooleanType.YES.getCode());
				holderHistory.setLegalResidenceCountry(countryResidence);
				listDepartmentLocation = getLstDepartmentLocation(holderHistory.getLegalResidenceCountry());
				disabledIndResident = true;
				disabledCountryResidentHolder=true;
			}
			else if(holderHistory.getNationality()!=null && !holderHistory.getNationality().equals(countryResidence) && holderHistory.getIndResidence().equals(BooleanType.NO.getCode())){
				if(holderHistory.getJuridicClass()!=null && holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())){
					disabledIndResident = true;
					disabledCountryResidentHolder=true;
				}else{
				holderHistory.setIndResidence(BooleanType.NO.getCode());
				listDepartmentLocation = getLstDepartmentLocation(holderHistory.getLegalResidenceCountry());
				}
			}
			else if(holderHistory.getNationality()!=null && !holderHistory.getNationality().equals(countryResidence) && holderHistory.getIndResidence().equals(BooleanType.YES.getCode())) {
				holderHistory.setIndResidence(BooleanType.YES.getCode());
				holderHistory.setLegalResidenceCountry(countryResidence);
				listDepartmentLocation = getLstDepartmentLocation(holderHistory.getLegalResidenceCountry());
			}
			validateNationalForeign(idComponent);
			loadHolderDocumentType();
			loadingDocumentAttachByHolder();
		}
		if (holderHistory.getIndResidence()!=null && BooleanType.YES.getCode().equals(holderHistory.getIndResidence())) {
			holderHistory.setLegalResidenceCountry(countryResidence);
			disabledCountryResidentHolder=true;
			if(holderHistory.getNationality()!=null && holderHistory.getNationality().equals(countryResidence)){
				holderHistory.setDocumentType(DocumentType.CI.getCode());
				documentTO = documentValidator.loadMaxLenghtType(holderHistory.getDocumentType());			
				numericTypeDocumentHolder = documentTO.isNumericTypeDocument();
				maxLenghtDocumentNumberHolder = documentTO.getMaxLenghtDocumentNumber();
				documentTO.setIndicatorDocumentSource(true);
				getLstDocumentSource();
				holderHistory.setDocumentSource(departmentIssuance);
			}
			else{	
				holderHistory.setDocumentType(null);
				holderHistory.setDocumentSource(null);
			}
			if(isViewOperationRegister()){
			SelectOneMenu cmbContryResident = (SelectOneMenu)FacesContext.getCurrentInstance().getViewRoot().findComponent("frmHolderRegister:tabView:cmbCountryResident");
			cmbContryResident.setValid(true);
			if(holderHistory.getHolderType().equals(PersonType.JURIDIC.getCode())){
			SelectOneMenu cmbContryResident2 = (SelectOneMenu)FacesContext.getCurrentInstance().getViewRoot().findComponent("frmHolderRegister:tabView:cmbCountryResident2");
			cmbContryResident2.setValid(true);
			}
			SelectOneMenu cmbLegalCountry = (SelectOneMenu)FacesContext.getCurrentInstance().getViewRoot().findComponent("frmHolderRegister:tabView:cmbLegalCountry");
			cmbLegalCountry.setValid(true);
			}
			if(isViewOperationModify()){
				SelectOneMenu cmbContryResident = (SelectOneMenu)FacesContext.getCurrentInstance().getViewRoot().findComponent("frmModifyHolderRequest:tabView:cmbCountryResident");
				cmbContryResident.setValid(true);
				if(holderHistory.getHolderType().equals(PersonType.JURIDIC.getCode())){
				SelectOneMenu cmbContryResident2 = (SelectOneMenu)FacesContext.getCurrentInstance().getViewRoot().findComponent("frmModifyHolderRequest:tabView:cmbCountryResident2");
				cmbContryResident2.setValid(true);
				}
				SelectOneMenu cmbLegalCountry = (SelectOneMenu)FacesContext.getCurrentInstance().getViewRoot().findComponent("frmModifyHolderRequest:tabView:cmbLegalCountry");
				cmbLegalCountry.setValid(true);	
			}
			listDepartmentLocation = getLstDepartmentLocation(holderHistory.getLegalResidenceCountry());
			holderHistory.setLegalDepartment(departmentDefault);			
//			listProvinceLocation = getLstProvinceLocation(holderHistory.getLegalDepartment());
//			holderHistory.setLegalProvince(provinceDefault);
//			listMunicipalityLocation=getLstMunicipalityLocation(holderHistory.getLegalProvince());
			listProvinceLocation=null;
			listMunicipalityLocation=null;
			holderHistory.setLegalDepartment(null);
			holderHistory.setLegalProvince(null);
			holderHistory.setLegalDistrict(null);
			holderHistory.setLegalAddress(null);
			holderHistory.setPostalResidenceCountry(null);	
			holderHistory.setPostalDepartment(null);
			holderHistory.setPostalProvince(null);
			holderHistory.setPostalDistrict(null);
			holderHistory.setPostalAddress(null);
			listMunicipalityPostalLocation=null;	
		} else if (holderHistory.getIndResidence()!=null && BooleanType.NO.getCode().equals(
				holderHistory.getIndResidence())) {
			holderHistory.setLegalResidenceCountry(null);
			disabledCountryResidentHolder=false;
			holderHistory.setDocumentType(null);
			holderHistory.setDocumentSource(null);
			holderHistory.setLegalDepartment(null);
			holderHistory.setLegalProvince(null);
			holderHistory.setLegalDistrict(null);
			holderHistory.setLegalAddress(null);
			listProvinceLocation=null;
			listMunicipalityLocation=null;
			holderHistory.setPostalResidenceCountry(null);		
			holderHistory.setPostalDepartment(null);
			holderHistory.setPostalProvince(null);
			holderHistory.setPostalDistrict(null);
			holderHistory.setPostalAddress(null);
			listProvincePostalLocation=null;
			listMunicipalityPostalLocation=null;
		} 
	}

	/**
	 * Metodo de Copia Legal de Titular.
	 */
	public void copyLegalHolder() {
		if (flagCopyLegalHolder) {
			
			listDepartmentPostalLocation = listDepartmentLocation;
			listProvincePostalLocation = listProvinceLocation;
			listMunicipalityPostalLocation = listMunicipalityLocation;
			
			holderHistory.setPostalResidenceCountry(holderHistory.getLegalResidenceCountry());
			holderHistory.setPostalDepartment(holderHistory.getLegalDepartment());
			holderHistory.setPostalProvince(holderHistory.getLegalProvince());
			holderHistory.setPostalDistrict(holderHistory.getLegalDistrict());
			holderHistory.setPostalAddress(holderHistory.getLegalAddress());

			disabledCmbPostalCountry = true;
			disabledCmbPostalDepartament = true;
			disabledCmbPostalProvince = true;
			disabledCmbPostalMunicipality = true;
			disabledInputPostalAddress = false;
		
		if(isViewOperationRegister()){	
		SelectOneMenu cmbPostalCountry = (SelectOneMenu) FacesContext.getCurrentInstance().getViewRoot().findComponent("frmHolderRegister:tabView:cmbPostalCountry");
		SelectOneMenu cmbPostalDepartament = (SelectOneMenu) FacesContext.getCurrentInstance().getViewRoot().findComponent("frmHolderRegister:tabView:cmbPostalDepartament");
		SelectOneMenu cmbPostalProvince = (SelectOneMenu) FacesContext.getCurrentInstance().getViewRoot().findComponent("frmHolderRegister:tabView:cmbPostalProvince");
		SelectOneMenu cmbPostalSector = (SelectOneMenu) FacesContext.getCurrentInstance().getViewRoot().findComponent("frmHolderRegister:tabView:cmbPostalMunicipality");
		InputText inputPostalAddress = (InputText)	FacesContext.getCurrentInstance().getViewRoot().findComponent("frmHolderRegister:tabView:inputPostalAddress");
			
		cmbPostalCountry.setValid(true);
		cmbPostalDepartament.setValid(true);
		cmbPostalProvince.setValid(true);
		cmbPostalSector.setValid(true);
		inputPostalAddress.setValid(true);	
		
		}
		if(isViewOperationModify()){
			SelectOneMenu cmbPostalCountry = (SelectOneMenu) FacesContext.getCurrentInstance().getViewRoot().findComponent("frmModifyHolderRequest:tabView:cmbPostalCountry");
			SelectOneMenu cmbPostalDepartament = (SelectOneMenu) FacesContext.getCurrentInstance().getViewRoot().findComponent("frmModifyHolderRequest:tabView:cmbPostalDepartament");
			SelectOneMenu cmbPostalProvince = (SelectOneMenu) FacesContext.getCurrentInstance().getViewRoot().findComponent("frmModifyHolderRequest:tabView:cmbPostalProvince");
			SelectOneMenu cmbPostalSector = (SelectOneMenu) FacesContext.getCurrentInstance().getViewRoot().findComponent("frmModifyHolderRequest:tabView:cmbPostalMunicipality");
			InputText inputPostalAddress = (InputText)	FacesContext.getCurrentInstance().getViewRoot().findComponent("frmModifyHolderRequest:tabView:inputPostalAddress");
				
			cmbPostalCountry.setValid(true);
			cmbPostalDepartament.setValid(true);
			cmbPostalProvince.setValid(true);
			cmbPostalSector.setValid(true);
			inputPostalAddress.setValid(true);		
		}
			
		} else {
			disabledCmbPostalCountry = false;
			disabledCmbPostalDepartament = false;
			disabledCmbPostalProvince = false;
			disabledCmbPostalMunicipality = false;
			disabledInputPostalAddress = false;
		}
	}

	
	/**
	 * Validate request confirm state.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String validateRequestConfirmState(){
		
		setViewOperationType(ViewOperationsType.CONFIRM.getCode());

		
		String action="";
		boolean flag = false;
		
		if (holderRequestSelected == null) {
			Object[] bodyData = new Object[0];
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED,bodyData));
			JSFUtilities.showSimpleValidationDialog();
			
			flag = true;
		} else if(holderRequestSelected != null){
			
			// Validate in subsidiary			
			List<String> lstOperationUser = validateOperationInSubsidiary(holderRequestSelected);
			if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_INVALID_USER_CONFIRM,
								new Object[] { lstOperationUser.get(0).toString() }));
				JSFUtilities.showSimpleValidationDialog();
				flag = true;
				return action;
			}
			
			if (!holderRequestSelected.getStateHolderRequest().equals(HolderRequestStateType.APPROVED.getCode())) {
				Object[] bodyData = new Object[0];
				flag = true;
				
				JSFUtilities.showMessageOnDialog(
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),						
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_OPERATION_CONFIRM_INVALID,bodyData));
			}
			if(flag){
				JSFUtilities.showSimpleValidationDialog();
				
			}else{
				loadingHolderInformation(null);
				action="holderInformation";	
			}
		}
		
		return action;
	}
	
	/**
	 * Metodo que Valida la Estado de Rechazo de solicitud.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String validateRequestRejectState(){
		
		setViewOperationType(ViewOperationsType.REJECT.getCode());

		String action="";
		boolean flag = false;
		
		if (holderRequestSelected == null) {	
			Object[] bodyData = new Object[0];
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED,bodyData));
			JSFUtilities.showSimpleValidationDialog();
						
			flag = true;
		} else if(holderRequestSelected != null){
			// Validate in subsidiary			
			List<String> lstOperationUser = validateOperationInSubsidiary(holderRequestSelected);
			if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_INVALID_USER_REJECT,
								new Object[] { lstOperationUser.get(0).toString() }));
				JSFUtilities.showSimpleValidationDialog();
				flag = true;
				return action;
			}
			if (!holderRequestSelected.getStateHolderRequest().equals(HolderRequestStateType.APPROVED.getCode())) {
				
				flag = true;
				Object[] bodyData = new Object[0];
				
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_OPERATION_REJECT_INVALID,bodyData));
						
			}
			if(flag){
				JSFUtilities.showSimpleValidationDialog();
			}else{
				loadingHolderInformation(null);
				action="holderInformation";	
			}
		}
		return action;
	}
	
	/**
	 * Validate request approve state.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String validateRequestApproveState(){
		
		setViewOperationType(ViewOperationsType.APPROVE.getCode());
		
		String action="";
		boolean flag = false;
		
		if (holderRequestSelected == null) {
			Object[] bodyData = new Object[0];
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED,bodyData));
			JSFUtilities.showSimpleValidationDialog();
			flag = true;			
		}
		else if(holderRequestSelected != null){
			// Validate in subsidiary			
			List<String> lstOperationUser = validateOperationInSubsidiary(holderRequestSelected);
			if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_INVALID_USER_APPROVE,
								new Object[] { lstOperationUser.get(0).toString() }));
				JSFUtilities.showSimpleValidationDialog();
				flag = true;
				return action;
			}
			
			if (!holderRequestSelected.getStateHolderRequest().equals(HolderRequestStateType.REGISTERED.getCode())
					&& !holderRequestSelected.getStateHolderRequest().equals(HolderRequestStateType.MODIFIED.getCode())) {
				Object[] bodyData = new Object[0];
				flag = true;
				
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_OPERATION_APPROVE_INVALID,bodyData));
			}
			if(flag){
				JSFUtilities.showSimpleValidationDialog();			
			}else{
				loadingHolderInformation(null);
				action="holderInformation";	
			}
		}
		
		return action;
	}
	
	/**
	 * Metodo que Valida el estado de Anulado de la Solicitud.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String validateRequestAnnularState(){
		
		setViewOperationType(ViewOperationsType.ANULATE.getCode());
		String action="";
		boolean flag = false;
		if (holderRequestSelected == null) {	
			Object[] bodyData = new Object[0];
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED,bodyData));
			JSFUtilities.showSimpleValidationDialog();
		    flag = true;
		}
		else if(holderRequestSelected != null){
			
			// Validate in subsidiary			
			List<String> lstOperationUser = validateOperationInSubsidiary(holderRequestSelected);
			if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_INVALID_USER_ANNUL,
								new Object[] { lstOperationUser.get(0).toString() }));
				JSFUtilities.showSimpleValidationDialog();
				flag = true;
				return action;
			}
			
			if (!holderRequestSelected.getStateHolderRequest().equals(HolderRequestStateType.REGISTERED.getCode())
					&& !holderRequestSelected.getStateHolderRequest().equals(HolderRequestStateType.MODIFIED.getCode())) {
				Object[] bodyData = new Object[0];		
				flag = true;
				
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_OPERATION_ANNULAR_INVALID,bodyData));
			}
			if(flag){
				JSFUtilities.showSimpleValidationDialog();
			}else{
				loadingHolderInformation(null);
				action="holderInformation";	
			}
		}
		return action;
	}

	/**
	 * 	Metodo que Valida si usuario esde la misma sucursal que de la operacion.
	 *
	 * @param objHolderRequest the obj holder request
	 * @return the string
	 */
	private List<String> validateOperationInSubsidiary(HolderRequest objHolderRequest){
		List<OperationUserTO> lstOperationUserParam = new ArrayList<OperationUserTO>();
		List<String> lstOperationUserResult;
		OperationUserTO operationUserTO = new OperationUserTO();
		operationUserTO.setOperNumber(objHolderRequest.getIdHolderRequestPk().toString());
		operationUserTO.setUserName(objHolderRequest.getRegistryUser());
		lstOperationUserParam.add(operationUserTO);
		lstOperationUserResult = getIsSubsidiary(userInfo, lstOperationUserParam);
		return lstOperationUserResult;
	}
	
	/**
	 * Medoto que valida antes de modificar.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String beforeOnModify() {		
		String action = "";
		flagActionsInFalse();
		flagActionModify = true;		 		
		boolean flag=false;
		
		if (holderRequestSelected == null) {
			Object[] bodyData = new Object[0];
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED,bodyData));
			JSFUtilities.showSimpleValidationDialog();
			
			flag = true;
		} else if(holderRequestSelected != null){
			
			// Validate in subsidiary			
			List<String> lstOperationUser = validateOperationInSubsidiary(holderRequestSelected);
			if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_INVALID_USER_MODIFY,
								new Object[] { lstOperationUser.get(0).toString() }));
				JSFUtilities.showSimpleValidationDialog();
				flag = true;	
				return action;
			}
			
			if (!holderRequestSelected.getStateHolderRequest().equals(HolderRequestStateType.REGISTERED.getCode())
					&& !holderRequestSelected.getStateHolderRequest().equals(HolderRequestStateType.MODIFIED.getCode())) {
				Object[] bodyData = new Object[0];
				flag = true;				
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_OPERATION_MODIFICATION_INVALID,bodyData));				
			}
			
			if(flag){
				JSFUtilities.showSimpleValidationDialog();
			}else{
				loadModificationPage();
				if(holderHistory.getFilePhotoName()==null) {
					setFilePhotoTemp(holderHistory.getFilePhoto());
				}else {
					setFilePhotoTemp(holderHistory.getFilePhoto());
					setPhotoHolderName(holderHistory.getFilePhotoName());
				}
				action = "goToRegistryHolderModify";
			}
		}

		// verificando si se trata de un titular juridico
		if (holderRequestSelected.getHolderHistory().getHolderType().toString().equals(PersonType.JURIDIC.getCode().toString())) {
			
			flagflagSendASFI = true;
			
			//si ya hay datos, que se muestren los campos
			if(holderRequestSelected.getHolderHistory().getMnemonic() !=null || holderRequestSelected.getHolderHistory().getFundAdministrator() != null ||
					holderRequestSelected.getHolderHistory().getTransferNumber()!= null){
				
				flagSendASFI=true;
				
			}
			
		}else{
			
			flagflagSendASFI = false;
			
		}

		return action;
	}

	/**
	 * Metodo que valida antes de condirmar.
	 *
	 * @param event the event
	 */
	
	@LoggerAuditWeb
	public void beforeOnConfirm(ActionEvent event) {
		
		
		flagActionsInFalse();
		flagActionConfirm = true;

				Object[] bodyData = new Object[1];
				bodyData[0] = holderRequestSelected.getIdHolderRequestPk().toString();
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
						PropertiesUtilities.getMessage(PropertiesConstants.LBL_CONFIRM_HOLDER_REQUEST,
								bodyData));
				JSFUtilities.executeJavascriptFunction("PF('widgConfirmActions').show()");
	
	}

	/**
	 * Metodo que valida antes de Rechazar.
	 *
	 * @param event the event
	 */
	@LoggerAuditWeb
	public void beforeOnReject(ActionEvent event) {
		
		JSFUtilities.resetViewRoot();
		getLstRejectMotivesType();
		flagActionsInFalse();

		flagActionReject = true;

		requestMotive=null;	
		requestOtherMotive="";
		showMotiveText=false;

		JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_MOTIVE_REJECT,
						null, null, null);
		
		JSFUtilities.executeJavascriptFunction("PF('dlgMotive').show();");

		
	}

	/**
	 * Metodo que Valida antes de Aprobar.
	 *
	 * @param event the event
	 */
	@LoggerAuditWeb
	public void beforeOnApprove(ActionEvent event) {
		
		flagActionsInFalse();
		flagActionAprove = true;
				Object[] bodyData = new Object[1];
				bodyData[0] = holderRequestSelected.getIdHolderRequestPk().toString();
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_APPROVE),
						PropertiesUtilities.getMessage(PropertiesConstants.LBL_APPROVE_HOLDER_REQUEST,
								bodyData));
				JSFUtilities.executeJavascriptFunction("PF('widgConfirmActions').show()");
	}
	
	/**
	 * Metodo que Valida antes de Anular.
	 *
	 * @param event the event
	 */
	@LoggerAuditWeb
	public void beforeOnAnnular(ActionEvent event) {
		
		JSFUtilities.resetViewRoot();
		getLstAnnularMotivesType();
		flagActionsInFalse();
		flagActionAnnular = true;
		requestMotive=null;	
		requestOtherMotive="";
		showMotiveText=false;
				JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_MOTIVE_ANNULAR,
						null, null, null);
				JSFUtilities.executeJavascriptFunction("PF('dlgMotive').show()");
	}

	/**
	 * Acciones de Confirmar.
	 */
	public void confirmActions() {

		if (flagActionReject) {

			Object[] bodyData = new Object[1];
			bodyData[0] = holderRequestSelected.getIdHolderRequestPk().toString();
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REJECT),
					PropertiesUtilities.getMessage(PropertiesConstants.LBL_REJECT_HOLDER_REQUEST,
							bodyData));
			JSFUtilities.executeJavascriptFunction("PF('widgConfirmActions').show()");
		} else if (flagActionAnnular) {
			Object[] bodyData = new Object[1];
			bodyData[0] = holderRequestSelected.getIdHolderRequestPk().toString();
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ANNUL),
					PropertiesUtilities.getMessage(PropertiesConstants.LBL_ANNULAR_HOLDER_REQUEST,
							bodyData));
			JSFUtilities.executeJavascriptFunction("PF('widgConfirmActions').show()");
						
		}
	}
	
	/**
	 * Metodo que setea los Flog de Tipo de persona.
	 */
	public void settingFlagTypePerson(){
		disabledIndResident = false;
		disabledCountryResidentHolder=false;
		disabledNationality = false;
		nitResultFind = null;
		flagCopyLegalHolder=false;
		disabledCmbPostalCountry=false;
		disabledCmbPostalDepartament=false;
		disabledCmbPostalProvince=false;
		disabledCmbPostalMunicipality=false;
		disabledInputPostalAddress=false;
	}
	
	/**
	 * Setting juridic class trust.
	 */
	public void settingJuridicClassTrust(){
		flagHolderJuridicPerson = true;
		flagHolderNaturalPerson = false;
		disabledRepresentativeButton = false;
		getLstEconomicSector();

		holderHistory = new HolderHistory();
		holderHistory.setJuridicClass(JuridicClassType.TRUST.getCode());
		holderHistory.setHolderType(PersonType.JURIDIC.getCode());
		holderHistory.setBeginningPeriod(null);
		holderHistory.setEndingPeriod(null);
		holderHistory.setIndSirtexNeg(BooleanType.YES.getCode());
		disabledCountryResidentHolder = false;
		listenerJuridicClass();
	}

	/**
	 * Load by holder type person.
	 */
	public void loadByHolderTypePerson() {
		
		try{
		settingFlagTypePerson();
		documentTO = new DocumentTO();
		
		if(Validations.validateIsNotNullAndNotEmpty(participant) &&
				Validations.validateIsNotNullAndNotEmpty(participant.getIdParticipantPk())){
			Participant filter = new Participant();
			filter.setIdParticipantPk(participant.getIdParticipantPk());
			participant = accountsFacade.findParticipantByFilters(filter);
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(participant) &&
				Validations.validateIsNotNullAndNotEmpty(participant.getIdParticipantPk()) && 
				participant.getAccountClass()!=null &&
				holderHistory.getHolderType()!=null){
			clientInformationTO=clientInformationValidator.validateShowClientInformation(participant.getAccountClass(),holderHistory.getHolderType());
		}
		
		Integer indSirtexNegAux =  holderHistory.getIndSirtexNeg();

		if (holderHistory.getHolderType()!=null && holderHistory.getHolderType().equals(PersonType.NATURAL.getCode())) {
			flagHolderNaturalPerson = true;
			flagHolderJuridicPerson = false;

			holderHistory = new HolderHistory();
			holderHistory.setHolderType(PersonType.NATURAL.getCode());
			holderHistory.setBeginningPeriod(null);
			holderHistory.setEndingPeriod(null);
			holderHistory.setIndicatorDisabled(false);
			holderHistory.setIndSirtexNeg(indSirtexNegAux);
			holderHistory.setNationality(countryResidence);
			flagHolderIndicatorInability = false;
			disabledCountryResidentHolder = false;
			disabledRepresentativeButton = false;
			listPepGradeWithPersonType = listPepGrade.stream().filter(param -> param.getShortInteger().equals(PersonType.NATURAL.getCode())).collect(Collectors.toList());
			lstCategoryPepWithPersonType = listCategoryPep;
			
			// deshabilitando el flag para el envio ASFI
			flagflagSendASFI = false;
				
		} else if (holderHistory.getHolderType()!=null && holderHistory.getHolderType().equals(
				PersonType.JURIDIC.getCode())) {
			flagHolderJuridicPerson = true;
			flagHolderNaturalPerson = false;
			disabledRepresentativeButton = false;
			getLstEconomicSector();

			holderHistory = new HolderHistory();
			holderHistory.setJuridicClass(JuridicClassType.NORMAL.getCode());
			holderHistory.setHolderType(PersonType.JURIDIC.getCode());
			holderHistory.setBeginningPeriod(null);
			holderHistory.setEndingPeriod(null);
			holderHistory.setIndSirtexNeg(indSirtexNegAux);
			disabledCountryResidentHolder = false;
			holderHistory.setNationality(countryResidence);
			listenerJuridicClass();
			holderHistory.setDocumentType(null);
			listPepGradeWithPersonType = listPepGrade.stream().filter(param -> param.getShortInteger().equals(PersonType.JURIDIC.getCode())).collect(Collectors.toList());
			lstCategoryPepWithPersonType = listCategoryPep.stream().filter(param -> param.getParameterTablePk().equals(PepMotiveType.LINK_FOR_FAMILY.getCode())).collect(Collectors.toList());
			
			// habilitando el flag para envio ASFI, issue 620
			flagflagSendASFI = true;
		
		} else {
			flagHolderNaturalPerson = false;
			flagHolderJuridicPerson = false;
			disabledRepresentativeButton = true;
			
		}
		if(!indFICPATEconomicActivity){
			showFieldsEconomicActivity();
		}
		
		// Issue 770
		// Validar la transferencia Extrabursatil
		validateIndCanTransfer(holderHistory);
		
		//620
		flagSendASFI = false;
					
	}
	catch(Exception e){
		e.getMessage();
	}
	
	}
	
	
	/**
	 * Llenar Lista de Clases juridicas.
	 *
	 */
	public void listenerJuridicClass(){
		
		disabledNationality = false;
		disabledIndResident = false;
		disabledCountryResidentHolder = false;
			if(holderHistory.getJuridicClass()!=null && holderHistory.getJuridicClass().equals(JuridicClassType.ENTITY_STATE.getCode())){
				
				holderHistory.setNationality(countryResidence);
				holderHistory.setIndResidence(BooleanType.YES.getCode());
				holderHistory.setLegalResidenceCountry(countryResidence);
				disabledIndResident = true;
				disabledCountryResidentHolder = true;
				disabledNationality = true;
			}
			
			else if(holderHistory.getJuridicClass()!=null && holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())){
				
				holderHistory.setIndResidence(BooleanType.YES.getCode());
				disabledIndResident = true;
				disabledCountryResidentHolder = true;
				holderHistory.setLegalResidenceCountry(countryResidence);
				holderHistory.setNationality(countryResidence);
				disabledNationality = false;
				// issue 1394: a requerimiento de usuario, si la clase juridico es FIDEICOMISO el tipo de inv tambien debe ser FIDEICOMISO
				holderHistory.setInvestorType(2584);
			}
			
			else if(holderHistory.getJuridicClass()!=null && holderHistory.getJuridicClass().equals(JuridicClassType.NORMAL.getCode())){
					disabledNationality = false;
					holderHistory.setNationality(countryResidence);
			}
			
			else if(holderHistory.getJuridicClass()==null){
				disabledNationality = false;
				disabledIndResident = false;
				disabledCountryResidentHolder = false;
				//disabledDocNumberBankCorrelative = false;
				holderHistory.setIndResidence(null);
				holderHistory.setNationality(null);
				holderHistory.setLegalResidenceCountry(null);
				listDepartmentLocation=null;
				listProvinceLocation=null;
				listMunicipalityLocation=null;
				listDepartmentPostalLocation=null;
				listProvincePostalLocation=null;
				listMunicipalityPostalLocation=null;
				
			}
		validateResidentHolder("");
		validateCountryResidentHolder();
		loadingDocumentAttachByHolder();
		
	}

			
	
	/**
	 * Cargando documento adjuntar por titular.
	 */
	public void loadingDocumentAttachByHolder() {	
        
        lstHolderReqFileHistory = new ArrayList<HolderReqFileHistory>();
        holderFileNameDisplay=GeneralConstants.EMPTY_STRING;
		loadDocumentTypeHolderParameter();
		
	}
	
	/**
	 * Cargar parametros de documentos de tipo de Titular.
	 */
	public void loadDocumentTypeHolderParameter(){

		if (flagHolderNaturalPerson) {
			if (!flagIndicatorMinorHolder && !flagHolderIndicatorInability) {
               
				if (flagNationalResident || flagForeignResident) {
					
					listHolderDocumentType = new ArrayList<ParameterTable>();
					
					listHolderDocumentType = getLstDocumentTypeAttachParameter(MasterTableType.AGEMAJOR_BOLIV_OR_FOREIGN_RESIDENT_DOC_HOLDER_NAT.getCode());
					
				}
				if (flagNationalNotResident) {
					
					listHolderDocumentType = new ArrayList<ParameterTable>();
					
					listHolderDocumentType = getLstDocumentTypeAttachParameter(MasterTableType.AGEMAJOR_BOLIV_NO_RESIDENT_DOC_HOLDER_NAT.getCode());
				}
				if (flagForeignNotResident) {

					listHolderDocumentType = new ArrayList<ParameterTable>();
					
					listHolderDocumentType = getLstDocumentTypeAttachParameter(MasterTableType.AGEMAJOR_FOREIGN_NO_RESIDENT_DOC_HOLDER_NAT.getCode());
					
				}

			} else if (flagIndicatorMinorHolder && !flagHolderIndicatorInability) {

				if (flagNationalResident || flagForeignResident) {
					listHolderDocumentType = new ArrayList<ParameterTable>();
					
					listHolderDocumentType = getLstDocumentTypeAttachParameter(MasterTableType.AGELESS_BOLIV_OR_FOREIGN_RESIDENT_DOC_HOLDER_NAT.getCode());
				}
				
				if (flagNationalNotResident) {
					listHolderDocumentType = new ArrayList<ParameterTable>();
					
					listHolderDocumentType = getLstDocumentTypeAttachParameter(MasterTableType.AGELESS_BOLIV_NO_RESIDENT_DOC_HOLDER_NAT.getCode());			

				}
				if (flagForeignNotResident) {
					
					listHolderDocumentType = new ArrayList<ParameterTable>();

					listHolderDocumentType = getLstDocumentTypeAttachParameter(MasterTableType.AGELESS_FOREIGN_NO_RESIDENT_DOC_HOLDER_NAT.getCode());			

				}
			} else if (flagHolderIndicatorInability) {

				if (flagNationalResident || flagForeignResident) {

					listHolderDocumentType = new ArrayList<ParameterTable>();

					listHolderDocumentType = getLstDocumentTypeAttachParameter(MasterTableType.UNABLE_BOLIV_OR_FOREIGN_RESIDENT_DOC_HOLDER_NAT.getCode());					

				}
				if (flagNationalNotResident) {
					
					listHolderDocumentType = new ArrayList<ParameterTable>();
					
					listHolderDocumentType = getLstDocumentTypeAttachParameter(MasterTableType.UNABLE_BOLIV_NO_RESIDENT_DOC_HOLDER_NAT.getCode());
					
					
				}
				if (flagForeignNotResident) {
					
					listHolderDocumentType = new ArrayList<ParameterTable>();
					
					listHolderDocumentType = getLstDocumentTypeAttachParameter(MasterTableType.UNABLE_FOREIGN_NO_RESIDENT_DOC_HOLDER_NAT.getCode());
					
				}
			}
		}
		if (flagHolderJuridicPerson) {

			if ((flagNationalResident || flagForeignResident) 
					&&
					holderHistory.getJuridicClass()!=null && 
					!holderHistory.getJuridicClass().equals(JuridicClassType.ENTITY_STATE.getCode()) &&
					!holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())
					) {
				
				listHolderDocumentType = new ArrayList<ParameterTable>();
				listHolderDocumentType = getLstDocumentTypeAttachParameter(MasterTableType.BOLIV_OR_FOREIGN_RESIDENT_DOC_HOLDER_JUR.getCode());
			}

			if (flagForeignNotResident 
					&&
					holderHistory.getJuridicClass()!=null && 
					!holderHistory.getJuridicClass().equals(JuridicClassType.ENTITY_STATE.getCode()) &&
					!holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())
 				) {
				
				listHolderDocumentType = new ArrayList<ParameterTable>();
			
				listHolderDocumentType = getLstDocumentTypeAttachParameter(MasterTableType.FOREIGN_NO_RESIDENT_DOC_HOLDER_JUR.getCode());

			}

			if (holderHistory.getJuridicClass()!=null && holderHistory.getJuridicClass().equals(JuridicClassType.ENTITY_STATE.getCode())) {
				
				listHolderDocumentType = new ArrayList<ParameterTable>();
				
				listHolderDocumentType = getLstDocumentTypeAttachParameter(MasterTableType.STATAL_ENTITY_DOC_HOLDER_JUR.getCode());
			}

			if (holderHistory.getJuridicClass()!=null && holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode()) && holderHistory.getIndResidence()!=null && holderHistory.getIndResidence().equals(BooleanType.NO.getCode())) {
				
				listHolderDocumentType = new ArrayList<ParameterTable>();			
				listHolderDocumentType = getLstDocumentTypeAttachParameter(MasterTableType.TRUST_NO_RESIDENT_DOC_HOLDER_JUR.getCode());

			}

			else if (holderHistory.getJuridicClass()!=null && holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())) {
				
				listHolderDocumentType = new ArrayList<ParameterTable>();						
				listHolderDocumentType = getLstDocumentTypeAttachParameter(MasterTableType.TRUST_DOC_HOLDER_JUR.getCode());
				
			}

		}

		
	}
	
	/**
	 * Gets the lst document type attach parameter.
	 *
	 * @param masterTableTypeCode the master table type code
	 * @return the lst document type attach parameter
	 */
	public List<ParameterTable> getLstDocumentTypeAttachParameter(Integer masterTableTypeCode){
		List<ParameterTable> lstDocumentTypeAttach = new ArrayList<ParameterTable>();
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(masterTableTypeCode);
			
			
			lstDocumentTypeAttach  =  generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		
		return lstDocumentTypeAttach;
		
	}

		
		
     /**
      * Load registration page.
      *
      * @return the string
      */
	public String loadRegistrationPage() {
		try{
			setViewOperationType(ViewOperationsType.REGISTER.getCode());
			
			//SI NO TIENE PARTICIPANTE Y ES EMISOR DPF NO PUEDE REGISTRAR
			if(userInfo.getUserAccountSession().isIssuerDpfInstitucion() && !flagIsIssuerDpfInstitution){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_PARTICIPANT));
				JSFUtilities.showSimpleValidationDialog();
				return "";
			}
			
			getLstBooleanType();
			getLstDocumentType();
			getLstParticipant();
			getLstPersonType();
			getLstSexType();
			getLstJuridicClass();
			getLstGeographicLocation();
			getLstGeographicLocationWithoutNationalCountry();
			getLstRolePep();
			getLstCategoryPep();
			getGradePep();
			getListEconomicActivityWithoutSector();
			
			holderRequest = new HolderRequest();
			holderRequestDetail = new HolderRequest();
			holderHistory = new HolderHistory();
			lstHolderReqFileHistory = new ArrayList<HolderReqFileHistory>();
			
			holderHistory.setIdHolderHistoryPk(GeneralConstants.NEGATIVE_ONE_LONG);
			holderHistory.setHolderType(null);
			holderHistory.setJuridicClass(null);
			holderHistory.setNationality(null);
			holderHistory.setIndResidence(null);
			holderHistory.setLegalResidenceCountry(null);
			holderHistory.setDocumentType(null);
			holderHistory.setSecondNationality(null);
			holderHistory.setSecondDocumentType(null);
			holderHistory.setBirthDate(CommonsUtilities.currentDate());
			holderHistory.setSex(null);
			holderHistory.setCategory(null);
			holderHistory.setIndPEP(BooleanType.NO.getCode());
			holderHistory.setIndSirtexNeg(BooleanType.YES.getCode());
			legalRepresentativeHelperBean.setDisabledHolderHistory(false);
			legalRepresentativeHelperBean.setDisabledHolderRequest(false);
			legalRepresentativeHelperBean.initializeLstLegalRepresentativeHistory();
			
			//limpiando filtros ocultos
			cleanFieldsEconomicActivity();
			cleanStylesEconomicActivity();
			indFICPATEconomicActivity = false;
			flagEconomicActivity = false;
			
			participant = new Participant();
			
			flagHolderNaturalPerson = false;
			flagHolderJuridicPerson = false;
			flagSendASFI=false;
			disabledRepresentativeButton = true;
			flagCopyLegalHolder = false;
			maxLenghtDocumentNumberHolder = 20;
			maxLenghtDocumentNumberLegal = 20;
			holderFileNameDisplay=GeneralConstants.EMPTY_STRING;
			listHolderDocumentType=null;
			attachFileHolderSelected = null;
			documentTO = new DocumentTO();
			participantAFP_FIC_PAT = false;
			holder = new Holder();
			
			newDocumentNumber = null;
			newDocumentNumberSecondNationality = null;
			
			if(validateIsParticipant()){
				participant.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
				disabledCmbParticipant = true;
				Participant filter = new Participant();
				filter.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
				listParticipant = accountsFacade.getLisParticipantServiceBean(filter);
				if(userInfo.getUserAccountSession().isAfpInstitution()){
					participantAFP_FIC_PAT=true;
				}			
			} else {			
				// Add validations participant DPF			
				Long idParticipantCode = getIssuerDpfInstitution(userInfo);		
				if(Validations.validateIsNotNullAndNotEmpty(idParticipantCode)){
					disabledCmbParticipant = true;				
					participant.setIdParticipantPk(idParticipantCode);
					flagIsIssuerDpfInstitution = true;
				} else {
					if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
						disabledCmbParticipant = true;
					} else {
						disabledCmbParticipant = false;
					}
					flagIsIssuerDpfInstitution = false;
					participant.setIdParticipantPk(null);
				}			
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		cleanFileImage();	
		return "goToRegisterHolder";
	}

	/**
	 * Load province legal.
	 */
	public void loadProvinceLegal() {
		
		if (holderHistory.getLegalDepartment()!=null) {
			listProvinceLocation = getLstProvinceLocation(holderHistory.getLegalDepartment());
		} else {
			holderHistory.setLegalProvince(null);
			listProvinceLocation = null;
			holderHistory.setLegalDistrict(null);
			listMunicipalityLocation = null;
		}
	}
	
	/**
	 * Load departament legal.
	 */
	public void loadDepartamentLegal() {
		
		if (holderHistory.getLegalResidenceCountry()!=null) {

			if (holderHistory.getLegalResidenceCountry().equals(
					countryResidence)) {
				disabledCountryResidentHolder = true;
				holderHistory.setIndResidence(BooleanType.YES.getCode());
			} else {
				disabledCountryResidentHolder = false;
				holderHistory.setIndResidence(BooleanType.NO.getCode());
			}

			listDepartmentLocation = getLstDepartmentLocation(holderHistory
					.getLegalResidenceCountry());
		} else {
			holderHistory.setLegalDepartment(null);
			listDepartmentLocation = null;
			holderHistory.setLegalProvince(null);
			listProvinceLocation = null;
			holderHistory.setLegalDistrict(null);
			listMunicipalityLocation = null;
		}
	}


	/**
	 * Validate country resident holder.
	 */
	public void validateCountryResidentHolder() {
		
		if (holderHistory.getLegalResidenceCountry()!=null && holderHistory.getLegalResidenceCountry().equals(
				countryResidence)) {
			disabledCountryResidentHolder = true;
			flagCopyLegalHolder = false;
			disabledCmbPostalCountry = false;
			disabledCmbPostalDepartament = false;
			disabledCmbPostalProvince = false;
			disabledCmbPostalMunicipality = false;
			disabledInputPostalAddress = false;
			
			listDepartmentLocation = getLstDepartmentLocation(holderHistory.getLegalResidenceCountry());
			
			holderHistory.setLegalDepartment(departmentDefault);			
			listProvinceLocation = getLstProvinceLocation(holderHistory.getLegalDepartment());
//			holderHistory.setLegalProvince(null);
//			listMunicipalityLocation=getLstMunicipalityLocation(holderHistory.getLegalProvince());			
			holderHistory.setLegalDistrict(null);
			holderHistory.setLegalAddress(null);
			holderHistory.setLegalDepartment(null);
			holderHistory.setLegalProvince(null);
			
			holderHistory.setPostalResidenceCountry(null);	
			holderHistory.setPostalDepartment(null);
			holderHistory.setPostalProvince(null);
			holderHistory.setPostalDistrict(null);
			holderHistory.setPostalAddress(null);
			
			listMunicipalityPostalLocation=null;
			
			
			holderHistory.setIndResidence(BooleanType.YES.getCode());
			documentTO = new DocumentTO();
			
			if(holderHistory.getNationality()!=null && holderHistory.getNationality().equals(countryResidence)){
				holderHistory.setDocumentType(DocumentType.CI.getCode());
				documentTO = documentValidator.loadMaxLenghtType(holderHistory.getDocumentType());			
				numericTypeDocumentHolder = documentTO.isNumericTypeDocument();
				maxLenghtDocumentNumberHolder = documentTO.getMaxLenghtDocumentNumber();
				documentTO = documentValidator.validateEmissionRules(holderHistory.getDocumentType());
				getLstDocumentSource();
				holderHistory.setDocumentSource(departmentIssuance);
			}
			else{
				holderHistory.setDocumentType(null);
				holderHistory.setDocumentSource(null);
			}
			
		} else if (holderHistory.getLegalResidenceCountry()!=null && !holderHistory.getLegalResidenceCountry().equals(
					countryResidence)) {
			
			flagCopyLegalHolder = false;
			disabledCountryResidentHolder = false;
			disabledCmbPostalCountry = false;
			disabledCmbPostalProvince = false;
			disabledCmbPostalMunicipality = false;
			disabledInputPostalAddress = false;
			holderHistory.setIndResidence(BooleanType.NO.getCode());
			listDepartmentLocation = getLstDepartmentLocation(holderHistory.getLegalResidenceCountry());
			
			holderHistory.setLegalDepartment(null);
			holderHistory.setLegalProvince(null);
			holderHistory.setLegalDistrict(null);
			holderHistory.setLegalAddress(null);
			
			listMunicipalityLocation=null;
			
			flagCopyLegalHolder=false;				
			
			holderHistory.setPostalResidenceCountry(null);
			holderHistory.setPostalDepartment(null);
			holderHistory.setPostalProvince(null);
			holderHistory.setPostalDistrict(null);
			holderHistory.setPostalAddress(null);
			
			listMunicipalityPostalLocation=null;			
			holderHistory.setDocumentType(null);
			holderHistory.setDocumentSource(null);
		}
		
		validateNationalForeign("");
		loadHolderDocumentType();
		loadHolderSecondDocumentType();
		loadingDocumentAttachByHolder();
	}

	
	
	/**
	 * Load district legal.
	 */
	public void loadMunicipalityLegal() {
		
		if (holderHistory.getLegalProvince()!=null) {
			listMunicipalityLocation = getLstMunicipalityLocation(holderHistory
					.getLegalProvince());
		} else {
			holderHistory.setLegalDistrict(null);
			listMunicipalityLocation = null;
		}
	}

	
	/**
	 * Load province postal.
	 */
	public void loadDepartamentPostal() {
		if (holderHistory.getPostalResidenceCountry()!=null) {
			listDepartmentPostalLocation = getLstDepartmentLocation(holderHistory
					.getPostalResidenceCountry());
		} else {
			holderHistory.setPostalDepartment(null);
			listDepartmentPostalLocation = null;
			holderHistory.setPostalProvince(null);
			listProvincePostalLocation = null;
			holderHistory.setPostalDistrict(null);
			listMunicipalityPostalLocation = null;
		}
	}
	
	/**
	 * Load province postal.
	 */
	public void loadProvincePostal() {
		if (holderHistory.getPostalDepartment()!=null) {
			listProvincePostalLocation = getLstProvinceLocation(holderHistory.getPostalDepartment());
		} else {
			holderHistory.setPostalProvince(null);
			listProvincePostalLocation = null;
			holderHistory.setPostalDistrict(null);
			listMunicipalityPostalLocation = null;
		}
	}

	
	/**
	 * Load district postal.
	 */
	public void loadMunicipalityPostal() {
		if (holderHistory.getPostalProvince()!=null) {
			listMunicipalityPostalLocation = getLstMunicipalityLocation(holderHistory
					.getPostalProvince());
		} else {
			holderHistory.setLegalDistrict(null);
			listMunicipalityPostalLocation = null;
		}
	}

	

	
	/**
	 * Gets the holder request data model.
	 * 
	 * @return the holder request data model
	 */
	public HolderRequestDataModel getHolderRequestDataModel() {
		return holderRequestDataModel;
	}

	/**
	 * Sets the holder request data model.
	 * 
	 * @param holderRequestDataModel
	 *            the new holder request data model
	 */
	public void setHolderRequestDataModel(
			HolderRequestDataModel holderRequestDataModel) {
		this.holderRequestDataModel = holderRequestDataModel;
	}

		
	/**
	 * Before modify all.
	 */
	@LoggerAuditWeb
	public void beforeModifyAll() {

		/* Issue 1231-
		 * Los procesos de adjuntar documentos que no serían obligatorios y que 
		 * están a cargo de los Participantes, sería el siguiente:
		 * - Registro de Titular Jurídico
		 * - Registro de Titular Natural
		 * - Registro Cuenta Titular
    	 * 
    	 */
		if(!validateAttachHolderFile()){
			return;
		}

		if (holderHistory.getHolderType().equals(PersonType.NATURAL.getCode())) {
			
			// seteando los nuevos valores de los documentos de identidad  1050
			holderHistory.setDocumentNumber(newDocumentNumber);
			holderHistory.setSecondDocumentNumber(newDocumentNumberSecondNationality);
			// issue 1233: se quita esta validacion (IndicatorDisabled) porque no aplica a nuestro pais
			// if (holderHistory.isIndicatorDisabled() || holderHistory.isIndicatorMinor()) {
			// Issue 1291 Modificacion Representante Legal para menor de Edad
//			if (holderHistory.isIndicatorMinor()) {
//				if (legalRepresentativeHelperBean.getLstLegalRepresentativeHistory() == null
//						|| legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size() == 0) {
//
//					Object[] bodyData = new Object[0];
//					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
//							PropertiesUtilities.getMessage(PropertiesConstants.WARNING_NECESSARY_LEGAL_REPRESENTATIVE,bodyData));
//
//					JSFUtilities.showSimpleValidationDialog();
//
//					return;
//				}
//			}
		} else if (holderHistory.getHolderType().equals(
				PersonType.JURIDIC.getCode())) {
			if (legalRepresentativeHelperBean.getLstLegalRepresentativeHistory() == null
					|| legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size() == 0) {

				Object[] bodyData = new Object[0];
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_NECESSARY_LEGAL_REPRESENTATIVE,bodyData));


				JSFUtilities.showSimpleValidationDialog();

				return;
			}
			if(holderHistory.getHolderType().equals(
					PersonType.JURIDIC.getCode()) && holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())){
//issue 2613 no debe solicitar representante fiduciario
//				int count=0;
				
				 if(legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size()<2){
					 Object[] bodyData = new Object[0];
					 JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							 PropertiesUtilities.getMessage(PropertiesConstants.WARNING_NECESSARY_TWO_LEGAL_REPRESENTATIVE,bodyData));

					 JSFUtilities.showSimpleValidationDialog();
						return;
				 }
//				 if(legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size()>=2){
//					 for(int i=0;i<legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size();i++){
//						 if(legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getRepresentativeClass().equals(RepresentativeClassType.FIDUCIARY.getCode())){
//							 count++;
//						 }
//					 }
//					 
//					 if(count==0){
//						 Object[] bodyData = new Object[0];
//						 JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
//								 PropertiesUtilities.getMessage(PropertiesConstants.WARNING_NECESSARY_TWO_LEGAL_REPRESENTATIVE,	bodyData));							
//
//						 JSFUtilities.showSimpleValidationDialog();
//							return;
//					 }
//				 }
			}
		}

		
		Object[] bodyData = new Object[1];
		
		bodyData[0] = holderRequestSelected.getIdHolderRequestPk().toString();

		
		JSFUtilities.showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MODIFY),
					PropertiesUtilities.getMessage(PropertiesConstants.ADM_REQUEST_HOLDER_CONFIRM_MODIFICATION,
							bodyData));

			JSFUtilities.executeJavascriptFunction("PF('cnfwAdmRequestHolder').show()");



	}
	
	/**
	 * Validate attach holder file.
	 *
	 * @return true, if successful
	 */
	public boolean validateAttachHolderFile(){
		boolean findCI=false;
		boolean findDIO=false;
		
		if (flagHolderNaturalPerson) {
			if (!flagIndicatorMinorHolder && !flagHolderIndicatorInability) {
               
				if (flagNationalResident || flagForeignResident) {
					
					if(lstHolderReqFileHistory.size()!=listHolderDocumentType.size()){						
						/*if (!validateDocument(lstHolderReqFileHistory)){
							JSFUtilities.showMessageOnDialog(
									PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
									PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
							JSFUtilities.showSimpleValidationDialog();
							return false;
						}*/
					}
					
				}
				if (flagNationalNotResident) {
					/*
					if(lstHolderReqFileHistory.size()!=listHolderDocumentType.size()){						
						JSFUtilities.showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
						JSFUtilities.showSimpleValidationDialog();
						return false;
					}*/

				}
				if (flagForeignNotResident) {
					/*
					if(lstHolderReqFileHistory.size()!=listHolderDocumentType.size()){						
						JSFUtilities.showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
						JSFUtilities.showSimpleValidationDialog();
						return false;
					}*/
				}

			} else if (flagIndicatorMinorHolder && !flagHolderIndicatorInability) {

				if (flagNationalResident || flagForeignResident) {
					
					for(int i=0;i<lstHolderReqFileHistory.size();i++){
						if(lstHolderReqFileHistory.get(i).getDocumentType().equals(AgeLessNationalOrForeignResidentDocHolderNatType.DIO.getCode())){
						   findDIO=true;	
						}
						
						if(lstHolderReqFileHistory.get(i).getDocumentType().equals(AgeLessNationalOrForeignResidentDocHolderNatType.CI.getCode())){
							findCI=true;
						}
					}
					/*
					if(!(findDIO || findCI)){
						JSFUtilities.showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
						JSFUtilities.showSimpleValidationDialog();
						return false;
					}*/
					
				}
				
				if (flagNationalNotResident) {
					
					if(lstHolderReqFileHistory.size()!=listHolderDocumentType.size()){						
						JSFUtilities.showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
						JSFUtilities.showSimpleValidationDialog();
						return false;
					}
					
				}
				if (flagForeignNotResident) {
					
					if(lstHolderReqFileHistory.size()!=listHolderDocumentType.size()){						
						JSFUtilities.showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
						JSFUtilities.showSimpleValidationDialog();
						return false;
					}
				}
			} else if (flagHolderIndicatorInability) {

				if (flagNationalResident || flagForeignResident) {

					if(lstHolderReqFileHistory.size()!=listHolderDocumentType.size()){						
						JSFUtilities.showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
						JSFUtilities.showSimpleValidationDialog();
						return false;
					}
				}
				if (flagNationalNotResident) {
					
					if(lstHolderReqFileHistory.size()!=listHolderDocumentType.size()){						
						JSFUtilities.showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
						JSFUtilities.showSimpleValidationDialog();
						return false;
					}
					
				}
				if (flagForeignNotResident) {
					
					if(lstHolderReqFileHistory.size()!=listHolderDocumentType.size()){						
						JSFUtilities.showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
						JSFUtilities.showSimpleValidationDialog();
						return false;
					}
					
				}
			}
		}
		if (flagHolderJuridicPerson) {

			if ((flagNationalResident || flagForeignResident) 
					&&
					holderHistory.getJuridicClass()!=null && 
					!holderHistory.getJuridicClass().equals(JuridicClassType.ENTITY_STATE.getCode()) &&
					!holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())
					) {
				
				// aqui si el participante es banco
//				List<Participant> primeraLista= listParticipant;
//				Participant participant2 = participant;
				if (!participant.getEconomicActivity().equals(EconomicActivityType.BANCOS.getCode())){
					if(lstHolderReqFileHistory.size()!=listHolderDocumentType.size()){						
						JSFUtilities.showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
						JSFUtilities.showSimpleValidationDialog();
						return false;
					}
				}
					
			
			}

			if (flagForeignNotResident 
					&&
					holderHistory.getJuridicClass()!=null && 
					!holderHistory.getJuridicClass().equals(JuridicClassType.ENTITY_STATE.getCode()) &&
					!holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())
 				) {
				
				if(lstHolderReqFileHistory.size()!=listHolderDocumentType.size()){						
					JSFUtilities.showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
							PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
					JSFUtilities.showSimpleValidationDialog();
					return false;
				}
				
			}

			if (holderHistory.getJuridicClass()!=null && holderHistory.getJuridicClass().equals(JuridicClassType.ENTITY_STATE.getCode())) {
			
				if(lstHolderReqFileHistory.size()!=listHolderDocumentType.size()){						
					JSFUtilities.showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
							PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
					JSFUtilities.showSimpleValidationDialog();
					return false;
				}
			
			}

			
			if (holderHistory.getJuridicClass()!=null && holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode()) && holderHistory.getIndResidence()!=null && holderHistory.getIndResidence().equals(BooleanType.NO.getCode())) {
				
				if(lstHolderReqFileHistory.size()!=listHolderDocumentType.size()){						
					JSFUtilities.showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
							PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
					JSFUtilities.showSimpleValidationDialog();
					return false;
				}
				
			}

			else if (holderHistory.getJuridicClass()!=null && holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())) {
				
				if(lstHolderReqFileHistory.size()!=listHolderDocumentType.size()){						
					JSFUtilities.showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
							PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
					JSFUtilities.showSimpleValidationDialog();
					return false;
				}
				
			}
			
			//1268 validando que adjunte al menos el nit o el de empresa extranjera
			// validacion de CONSTANCIA DE RUC PARA normal/estatal/fideicomiso
			
			boolean validador = true;
			for(HolderReqFileHistory fileHist: lstHolderReqFileHistory){
				if(fileHist.getDocumentType() == 1033 
					//|| fileHist.getDocumentType() == 1019
					//|| fileHist.getDocumentType() == 1051
					//|| fileHist.getDocumentType() == 1067
					|| fileHist.getDocumentType() == 1783
					|| fileHist.getDescription().equalsIgnoreCase("CONSTANCIA DE RUC") 
					|| fileHist.getDocumentType() == 2737){
					validador = false;
					
				}
			}
			
			if(validador){
				JSFUtilities.showMessageOnDialog(
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
				JSFUtilities.showSimpleValidationDialog();
				return false;
			}
		}
		
		return true;

	}
	
	private boolean validateDocument(List<HolderReqFileHistory> listHolderReqFileHistorie){
		boolean validate=false;
		
		for (HolderReqFileHistory lista : listHolderReqFileHistorie){
			if (lista.getDocumentType().equals(AgeMajorDomOrForeignResidentDocHolderNatType.CE.getCode())){
				validate=true;
			}
		}
		
		return validate;
	}

	/**
	 * Before save all.
	 */
	@LoggerAuditWeb
	public void beforeSaveAll() {

		/* Issue 1231-
		 * Los procesos de adjuntar documentos que no serían obligatorios y que 
		 * están a cargo de los Participantes, sería el siguiente:
		 * - Registro de Titular Jurídico
		 * - Registro de Titular Natural
		 * - Registro Cuenta Titular
    	 * 
    	 */
		if(!validateAttachHolderFile()){
			return;
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(participant.getState()) && participant.getState().equals(ParticipantStateType.BLOCKED.getCode())) {
			Object[] bodyData = new Object[0];
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					 PropertiesUtilities.getMessage(PropertiesConstants.LBL_REJECT_HOLDER_REQUEST_PARTICIPANT_BLOCKED,bodyData));
					JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		if (holderHistory.getHolderType().equals(PersonType.NATURAL.getCode())) {
			// seteando los nuevos valores de los documentos de identidad  1050
			if(Validations.validateIsNullOrEmpty(holderHistory.getFilePhoto())) {
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						 PropertiesUtilities.getMessage(PropertiesConstants.HOLDER_FILE_PHOTO_NO_COMPLETE));

				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			if(Validations.validateIsNullOrEmpty(holderHistory.getIndFatca())) {
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						 PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_FATCA));

				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			holderHistory.setDocumentNumber(newDocumentNumber);
			holderHistory.setSecondDocumentNumber(newDocumentNumberSecondNationality);
			// issue 1233: se quita esta validacion (IndicatorDisabled) porque no aplica a nuestro pais
			//if (holderHistory.isIndicatorDisabled() || holderHistory.isIndicatorMinor()) {
						//Issue 1291 Modificacion Representante Legal para menor de Edad
//			if ( holderHistory.isIndicatorMinor()) {
//				if (legalRepresentativeHelperBean.getLstLegalRepresentativeHistory() == null
//						|| legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size() == 0) {
//					Object[] bodyData = new Object[0];
//					
//					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
//							PropertiesUtilities.getMessage(PropertiesConstants.WARNING_NECESSARY_LEGAL_REPRESENTATIVE,bodyData));
//		
//					JSFUtilities.showSimpleValidationDialog();
//					
//					return;
//				}
//				
//			}
		} else if (holderHistory.getHolderType().equals(PersonType.JURIDIC.getCode())) {
			if (legalRepresentativeHelperBean.getLstLegalRepresentativeHistory() == null
				|| legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size() == 0) {
				Object[] bodyData = new Object[0];
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
												 PropertiesUtilities.getMessage(PropertiesConstants.WARNING_NECESSARY_LEGAL_REPRESENTATIVE,bodyData));

				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			if(Validations.validateIsNullOrEmpty(holderHistory.getIndFatca())) {
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						 PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_FATCA));

				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			if(holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())){
				//issue 2613 no debe solicitar representante fiduciario				
				//				int count=0;
				 if(legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size()<2){
					 Object[] bodyData = new Object[0];
					 JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							 						  PropertiesUtilities.getMessage(PropertiesConstants.WARNING_NECESSARY_TWO_LEGAL_REPRESENTATIVE,bodyData));

					 JSFUtilities.showSimpleValidationDialog();
					 return;
				 }
//				 if(legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size()>=2){
//					 for(int i=0;i<legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size();i++){
//						 if(legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getRepresentativeClass().equals(RepresentativeClassType.FIDUCIARY.getCode())){
//							 count++;
//						 }
//					 }
//					 
//					 if(count==0){
//						 Object[] bodyData = new Object[0];
//						 JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
//								 PropertiesUtilities.getMessage(PropertiesConstants.WARNING_NECESSARY_TWO_LEGAL_REPRESENTATIVE,	bodyData));							
//
//						 JSFUtilities.showSimpleValidationDialog();
//							return;
//					 }
//				 }
			}
		}
		
		Object[] bodyData = new Object[0];

		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
										 PropertiesUtilities.getMessage(PropertiesConstants.ADM_REQUEST_HOLDER_CONFIRM_REGISTER, bodyData));

		JSFUtilities.executeJavascriptFunction("PF('cnfwAdmRequestHolder').show()");
		
	}
	
		
	/**
	 * Document attach holder file.
	 *
	 * @param event the event
	 */
	public void documentAttachHolderFile(FileUploadEvent event){
		
		if(attachFileHolderSelected!=null){
			
			if(event.getFile().getFileName().length() > 100){
				Object[] bodyData = new Object[0];
				JSFUtilities.showMessageOnDialog(GeneralConstants.LBL_HEADER_ALERT, PropertiesUtilities.getMessage(
						PropertiesConstants.ERROR_SIZE_FILENAME_DOCUMENT,bodyData));
				JSFUtilities.showSimpleValidationDialog();
				attachFileHolderSelected = null;
			}
			
			String fDisplayName=fUploadValidateFile(event.getFile(),null, null,getfUploadFileDocumentsTypes());
			 
			 if(fDisplayName!=null){
				 holderFileNameDisplay=fDisplayName;
			 }

			 for(int i=0;i<listHolderDocumentType.size();i++){
				if(listHolderDocumentType.get(i).getParameterTablePk().equals(attachFileHolderSelected)){
					
					HolderReqFileHistory holderReqFileHistory = new HolderReqFileHistory();

					holderReqFileHistory
							.setHolderReqFileHisType(HolderReqFileHisType.CREATION
									.getCode());
					holderReqFileHistory
							.setFilename(event.getFile().getFileName());
					holderReqFileHistory
							.setFileHolderReq(event.getFile().getContents());
					holderReqFileHistory.setRegistryUser(userInfo.getUserAccountSession().getUserName());
					holderReqFileHistory
							.setRegistryDate(CommonsUtilities
									.currentDate());
					holderReqFileHistory
							.setHolderRequest(holderRequest);
					holderReqFileHistory
							.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST
									.getCode());
					holderReqFileHistory
							.setStateFile(HolderFileStateType.REGISTERED
									.getCode());
					holderReqFileHistory
							.setDocumentType(listHolderDocumentType.get(i).getParameterTablePk());
					holderReqFileHistory
							.setDescription(listHolderDocumentType.get(i).getParameterName());
					
					if(isViewOperationModify()){
						holderReqFileHistory.setWasModified(true);
					}
					
					insertHolderFileItem(holderReqFileHistory);
				}
			}
		}
		
	}
	
	/**
	 * Reset components.
	 */
	public void validateBack(){
		
		if(requestMotive!=null){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(PropertiesConstants.MESSAGES_ALERT),PropertiesUtilities.getGenericMessage(PropertiesConstants.MESSAGE_BACK));
			JSFUtilities.executeJavascriptFunction("PF('cnfwBackMotive').show()");
		}else{
			resetComponents();
			JSFUtilities.executeJavascriptFunction("PF('dlgMotive').hide()");
		}
		
		
	}
	
	/**
	 * Reset components.
	 */
	public void resetComponents(){
		JSFUtilities.resetViewRoot();
		
	}

	/**
	 * Clean all.
	 */
	public void cleanAll() {
	
		JSFUtilities.resetViewRoot();
		holderRequestSelected=null;
		holderRequest = new HolderRequest();
		holderFilter = new HolderRequestTO();
		holderHistory = new HolderHistory();
		holderRequestDataModel = null;
		participant = new Participant();
		holderFilter.setInitialDate(CommonsUtilities.currentDate());
		holderFilter.setEndDate(CommonsUtilities.currentDate());
		holderHistory.setBeginningPeriod(CommonsUtilities.currentDate());
		holderHistory.setEndingPeriod(CommonsUtilities.currentDate());
		holderHistory.setIndSirtexNeg(BooleanType.YES.getCode());
		flagCopyLegalHolder = false;
		flagHolderJuridicPerson = false;
		flagHolderNaturalPerson = false;
		flagSendASFI=false;
		lstHolderReqFileHistory=null;
		holderFileNameDisplay = "";
		legalRepresentativeHelperBean.setLstLegalRepresentativeHistory(null);
		legalRepresentativeHelperBean.setLstLegalRepresentativeFile(null);
		legalRepresentativeHelperBean.setLstRepresentativeFileHistory(null);
		legalRepresentativeHelperBean.setDisabledHolderHistory(false);
		legalRepresentativeHelperBean.setDisabledHolderRequest(false);
		inFalseFlagNationalForeign();		
		if(validateIsParticipant()){
			participant.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
			holderFilter.setParticipantSelected(userInfo.getUserAccountSession().getParticipantCode());
			disabledCmbParticipant = true;
		} else {			
			// Add validations participant DPF
			flagIsIssuerDpfInstitution = false;
			Long idParticipantCode = getIssuerDpfInstitution(userInfo);			
			if(Validations.validateIsNotNullAndNotEmpty(idParticipantCode)){
				disabledCmbParticipant = true;
				flagIsIssuerDpfInstitution = true;	
				holderFilter.setParticipantSelected(idParticipantCode);
				participant.setIdParticipantPk(idParticipantCode);
			} else {
				if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
					disabledCmbParticipant = true;
				} else {
					disabledCmbParticipant = false;
				}				
				flagIsIssuerDpfInstitution = false;	
				holderFilter.setParticipantSelected(null);
				participant.setIdParticipantPk(null);
			}			
		}
		cleanFileImage();
		//limpiando filtros ocultos
		cleanFieldsEconomicActivity();
		cleanStylesEconomicActivity();
		indFICPATEconomicActivity = false;
		flagEconomicActivity = false;
		participantAFP_FIC_PAT = false;
	}	
		
	/**
	 * Load holder document type.
	 */
	public void loadHolderDocumentType() {

		 try{

		listDocumentTypeResult = new ArrayList<ParameterTable>();

		PersonTO personTo = new PersonTO();
		personTo.setFlagInabilityIndicator(flagHolderIndicatorInability);
		personTo.setFlagMinorIndicator(flagIndicatorMinorHolder);
		personTo.setFlagHolderIndicator(true);
		personTo.setFlagNationalResident(flagNationalResident);
		personTo.setFlagForeignResident(flagForeignResident);
		personTo.setFlagNationalNotResident(flagNationalNotResident);
		personTo.setFlagForeignNotResident(flagForeignNotResident);
		personTo.setPersonType(holderHistory.getHolderType());
		
			
		if(holderHistory.getHolderType()!=null && holderHistory.getHolderType().equals(PersonType.JURIDIC.getCode())){
			if(holderHistory.getJuridicClass()!=null && holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())){
				personTo.setFlagJuridicClassTrust(true);
			}
			else{
				personTo.setFlagJuridicClassTrust(false);
			}
		}
		else{
			personTo.setFlagJuridicClassTrust(false);
		}
		
		listDocumentTypeResult = documentValidator.getDocumentTypeByPerson(personTo);
		 }
		 catch(Exception e){
			 e.getMessage();
		 }
	}

	
	/**
	 * Save all.
	 */
	@LoggerAuditWeb
	public void saveAll() {
		
		try {
			holderRequest = holderServiceFacade.registerHolderRequestServiceFacade(holderRequest,
							participant,
							holderHistory,
							legalRepresentativeHelperBean.getLstLegalRepresentativeHistory(),
							lstHolderReqFileHistory);

			flagflagSendASFI = false;

			if (Validations.validateIsNotNull(holderRequest.getIdHolderRequestPk())){
				
				Object[] bodyData = new Object[1];

				bodyData[0] = holderRequest.getIdHolderRequestPk().toString();

				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						PropertiesUtilities.getMessage(PropertiesConstants.LBL_REGISTRY_HOLDER_REQUEST,
						bodyData));
				JSFUtilities.executeJavascriptFunction("PF('endTrasaction').show()");
				
				businessProcess = new BusinessProcess();
				
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.NEW_HOLDER_CREATION_REQUEST_REGISTRATION.getCode());
				Object[] parameters = new Object[2];
				parameters[0]=holderRequest.getParticipantDescription();
				parameters[1]=holderRequest.getIdHolderRequestPk().toString();				
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,holderRequest.getParticipant().getIdParticipantPk(),parameters);
			}
		}
		catch (ServiceException ex) {
			try {
			Object[] bodyData = new Object[1];

			HolderRequest auxholderRequest = holderServiceFacade.getHolderRequestByDocumentTypeAndDocumentNumberServiceFacade(holderRequest);
			
			if(auxholderRequest!=null){			
						
			bodyData[0] = auxholderRequest.getIdHolderRequestPk().toString();
			
				if(ex.getErrorService().equals(ErrorServiceType.HOLDER_REQUEST_EXIST)){
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getExceptionMessage(ErrorServiceType.HOLDER_REQUEST_EXIST.getMessage(),bodyData));
					JSFUtilities.executeJavascriptFunction("PF('endTrasaction').show()");
									
				}
			}
			}
			catch (ServiceException e) {
				
				e.printStackTrace();
			}

		}
		catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}

	
	/**
	 * Value change cmb document holder.
	 */
	public void valueChangeCmbDocumentHolder(){
		holderFileNameDisplay = null;
	}
	
	/**
	 * Insert or replace holder file item.
	 *
	 * @param holderReqFileHistory the holder req file history
	 */
	public void insertHolderFileItem(HolderReqFileHistory holderReqFileHistory){
		
		
			for(int i=0;i<lstHolderReqFileHistory.size();i++)
			{
				if(lstHolderReqFileHistory.get(i).getDocumentType().equals(holderReqFileHistory.getDocumentType())){
				
					Object[] bodyData = new Object[0];
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_ALERT_DOCUMENT_ALREADY_LOADED,
							bodyData));
					JSFUtilities.showSimpleValidationDialog();
					attachFileHolderSelected = null;
					
					return;
				}	
			}
			
			lstHolderReqFileHistory.add(holderReqFileHistory);
			attachFileHolderSelected = null;
			
		
		
		
	}
	
	/**
	 * Action back.
	 */
	public void actionBack() {
		 
		JSFUtilities.executeJavascriptFunction("PF('dlgTab').hide()");
	}

	

	/**
	 * Validate type document holder.
	 *
	 * @param idComponent the id component
	 */
	public void validateTypeDocumentHolder(String idComponent) {

		try{
			
		if (validateTypesDocumentsHolder(idComponent) && holderHistory.getDocumentType()!=null) {

			holderHistory.setDocumentNumber(GeneralConstants.EMPTY_STRING);
			newDocumentNumber = GeneralConstants.EMPTY_STRING;
			
			documentTO = documentValidator.loadMaxLenghtType(holderHistory.getDocumentType());			
			numericTypeDocumentHolder = documentTO.isNumericTypeDocument();
			maxLenghtDocumentNumberHolder = documentTO.getMaxLenghtDocumentNumber();
			holderHistory.setDocumentIssuanceDate(null);
			
			documentTO = documentValidator.validateEmissionRules(holderHistory.getDocumentType());
			
			if(documentTO.isIndicatorDocumentSource()){
				
				SelectOneMenu cmbDocumentSource;
				
				if(getViewOperationType().equals(ViewOperationsType.MODIFY.getCode())){
					cmbDocumentSource= (SelectOneMenu)FacesContext.getCurrentInstance().getViewRoot().findComponent("frmModifyHolderRequest:tabView:cmbDocumentSource");
				}else{
					cmbDocumentSource= (SelectOneMenu)FacesContext.getCurrentInstance().getViewRoot().findComponent("frmHolderRegister:tabView:cmbDocumentSource");
				}
				
				cmbDocumentSource.setValid(true);
				getLstDocumentSource();
				if(holderHistory.getNationality()!=null 
						&& holderHistory.getNationality().equals(countryResidence)
						&& holderHistory.getIndResidence()!=null 
						&& holderHistory.getIndResidence().equals(BooleanType.YES.getCode())){
					holderHistory.setDocumentSource(departmentIssuance);
				}
				else{
					holderHistory.setDocumentSource(null);
				}
			}
			else{
				holderHistory.setDocumentSource(null);
				listDocumentSource = new ArrayList<ParameterTable>();
			}
		}
		else{
			holderHistory.setDocumentType(null);
		}

		}
		catch(Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
			
		}
	}

	/**
	 * Validate exist second type document and second number document.
	 *
	 * @param idComponent the id component
	 */
	public void validateExistSecondTypeDocumentAndSecondNumberDocument(String idComponent) {

		try {
			
			if(Validations.validateIsNullOrEmpty(holderHistory.getSecondDocumentNumber())
					|| Validations.validateIsNullOrEmpty(holderHistory.getSecondDocumentType())){
				return;
			}
			
			if(holderHistory.getSecondDocumentType() != null && holderHistory.getSecondDocumentNumber() != null){
				
				if(holderHistory.getSecondDocumentType().equals(DocumentType.CIE.getCode())){
					newDocumentNumberSecondNationality = "E-" + holderHistory.getSecondDocumentNumber();
				}else{
					newDocumentNumberSecondNationality = holderHistory.getSecondDocumentNumber();
				}
			}
			
			if(holderHistory.getSecondDocumentType()!=null &&
					!holderHistory.getSecondDocumentType().equals(DocumentType.DIO.getCode()) &&
					   holderHistory.getSecondDocumentNumber()!=null && 
					   legalRepresentativeHelperBean.getLstLegalRepresentativeHistory()!=null && 
					   legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size()>0){
						
					   for(int i=0;i<legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size();i++){
						   
						   if((legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentType().equals(holderHistory.getSecondDocumentType()) &&
						      legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentNumber().equals(holderHistory.getSecondDocumentNumber()))
						      || (legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getSecondDocumentType()!=null && legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getSecondDocumentType().equals(holderHistory.getSecondDocumentType()) &&
						    		  legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getSecondDocumentNumber()!=null && legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getSecondDocumentNumber().equals(holderHistory.getSecondDocumentNumber()))
								      ){
						
							   	Object[] bodyData = new Object[0];
							   	
							   	JSFUtilities.showMessageOnDialog(GeneralConstants.LBL_HEADER_ALERT, PropertiesUtilities.getMessage(
														PropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE,bodyData));
								JSFUtilities.showSimpleValidationDialog();
								
							   
							   JSFUtilities
								.addContextMessage(
										idComponent,
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities
												.getMessage(
														PropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE,
														bodyData),
										PropertiesUtilities
												.getMessage(
														PropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE,
														bodyData));

								holderHistory.setSecondDocumentNumber(null); 
								return;
						   }
					   }
						
					}

			
			
			if(holderHistory.getSecondDocumentType()!=null && holderHistory.getDocumentType()!=null
					&& holderHistory.getDocumentNumber()!=null && holderHistory.getSecondDocumentNumber()!=null){
			
			if(holderHistory.getSecondDocumentType().equals(holderHistory.getDocumentType())
					&& holderHistory.getDocumentNumber().equals(holderHistory.getSecondDocumentNumber())){
				 
				Object[] bodyData = new Object[0];
				
				JSFUtilities.showMessageOnDialog(GeneralConstants.EMPTY_STRING, PropertiesUtilities.getMessage(
										PropertiesConstants.ERROR_DOCUMENTS_TYPE_AND_NUMBER_EQUALS,bodyData));
				JSFUtilities.showSimpleValidationDialog();
				
				JSFUtilities
				.addContextMessage(
						idComponent,
						FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities
								.getMessage(
										PropertiesConstants.ERROR_DOCUMENTS_TYPE_AND_NUMBER_EQUALS,
										bodyData),
						PropertiesUtilities
								.getMessage(
										PropertiesConstants.ERROR_DOCUMENTS_TYPE_AND_NUMBER_EQUALS,
										bodyData));

				holderHistory.setSecondDocumentNumber(null);	
				return;
			}
			
			}
			
			if(documentValidator.validateFormatDocumentNumber(holderHistory.getSecondDocumentType(),holderHistory.getSecondDocumentNumber(),idComponent))
			{
				
				Holder objHolder = new Holder();

				objHolder.setSecondDocumentType(holderHistory.getSecondDocumentType());
				objHolder.setSecondDocumentNumber(newDocumentNumberSecondNationality);
				if(holderHistory.getSecondNationality()!=null){
				objHolder.setSecondNationality(holderHistory.getSecondNationality());
				}
				
				objHolder = accountsFacade
				.getHolderBySecondDocumentTypeAndSecondDocumentNumberServiceFacade(objHolder);
		
				if(objHolder!=null){
					Object[] bodyData = new Object[2];

					bodyData[0] = objHolder.getDescriptionHolder();
					bodyData[1] = objHolder.getIdHolderPk().toString();						
					
					JSFUtilities.showMessageOnDialog(GeneralConstants.EMPTY_STRING, PropertiesUtilities.getMessage(
											PropertiesConstants.WARNING_DOCUMENTTYPE_DOCUMENTNUMBER_OTHER_HOLDER,bodyData));
					JSFUtilities.showSimpleValidationDialog();
					holderHistory.setSecondDocumentNumber(null);	
					
					
					JSFUtilities
							.addContextMessage(
									"frmHolderRegister:tabView:inputSecondDocumentNumber",
									FacesMessage.SEVERITY_ERROR,
									PropertiesUtilities
											.getMessage(
													PropertiesConstants.WARNING_DOCUMENTTYPE_DOCUMENTNUMBER_OTHER_HOLDER,
													bodyData),
									PropertiesUtilities
											.getMessage(
													PropertiesConstants.WARNING_DOCUMENTTYPE_DOCUMENTNUMBER_OTHER_HOLDER,
													bodyData));
					
					holderHistory.setSecondDocumentNumber(null);		

				}
				
				else{

				
				HolderRequest auxHolderRequest = new HolderRequest();
				
				List<HolderHistory> lstHolderHistory = new ArrayList<HolderHistory>();
				lstHolderHistory.add(holderHistory);
				auxHolderRequest.setHolderHistories(lstHolderHistory);			
				
				if(holderRequest.getIdHolderRequestPk()!=null){
					auxHolderRequest.setIdHolderRequestPk(holderRequest.getIdHolderRequestPk());
				}
				
				auxHolderRequest = holderServiceFacade.getHolderRequestBySecondDocumentTypeAndSecondDocumentNumberServiceFacade(auxHolderRequest);
				
				if(auxHolderRequest!=null && !auxHolderRequest.getStateHolderRequest().equals(HolderRequestStateType.CONFIRMED.getCode())){
					Object[] bodyData = new Object[1];
					bodyData[0] = auxHolderRequest.getIdHolderRequestPk().toString();
					JSFUtilities.showMessageOnDialog(GeneralConstants.EMPTY_STRING, PropertiesUtilities.getMessage(
											PropertiesConstants.WARNING_DOCUMENTTYPE_DOCUMENTNUMBER_OTHER_HOLDER_REQUEST,bodyData));
					JSFUtilities.showSimpleValidationDialog();
					holderHistory.setSecondDocumentNumber(null);		
					
					JSFUtilities
					.addContextMessage(
							"frmHolderRegister:tabView:inputSecondDocumentNumber",
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities
									.getMessage(
											PropertiesConstants.WARNING_DOCUMENTTYPE_DOCUMENTNUMBER_OTHER_HOLDER_REQUEST,
											bodyData),
							PropertiesUtilities
									.getMessage(
											PropertiesConstants.WARNING_DOCUMENTTYPE_DOCUMENTNUMBER_OTHER_HOLDER_REQUEST,
											bodyData));
			
				  	
				}
				
									
				}
			
		}
		else{
			holderHistory.setSecondDocumentNumber(null);			
		}

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	
	/**
	 * List holidays.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listHolidays() throws ServiceException{
		  Holiday holidayFilter = new Holiday();
		  holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		  GeographicLocation countryFilter = new GeographicLocation();
		  countryFilter.setIdGeographicLocationPk(countryResidence);
		  holidayFilter.setGeographicLocation(countryFilter);
		    
		  allHolidays = CommonsUtilities.convertListDateToString(generalParametersFacade.getListHolidayDateServiceFacade(holidayFilter));
		 }
	
	
	/**
	 * Validate exist type document and number document.
	 *
	 * @param idComponent the id component
	 * @return true, if successful
	 */
	@LoggerAuditWeb
	public void validateExistTypeDocumentAndNumberDocument(String idComponent) {
		try {
			
			if(Validations.validateIsNullOrEmpty(holderHistory.getDocumentNumber())
					|| Validations.validateIsNullOrEmpty(holderHistory.getDocumentType())){
				return;
			}
			//quitando caracteres especiales
			holderHistory.setDocumentNumber(holderHistory.getDocumentNumber().replaceAll("[^\\dA-Za-z-]", ""));
			
			if(holderHistory.getDocumentType()!=null && 
			   !holderHistory.getDocumentType().equals(DocumentType.DIO.getCode()) &&		
			   holderHistory.getDocumentNumber()!=null && 
			   legalRepresentativeHelperBean.getLstLegalRepresentativeHistory()!=null && 
			   legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size()>0){
				
			   for(int i=0;i<legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size();i++){
				   
				   if((legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentType().equals(holderHistory.getDocumentType()) &&
				      legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentNumber().equals(holderHistory.getDocumentNumber()) &&
				      (!legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentType().equals(DocumentType.CI.getCode())
				      || !legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentType().equals(DocumentType.CID.getCode())
				      || !legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentType().equals(DocumentType.PAS.getCode())
				      || !legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentType().equals(DocumentType.CDN.getCode())))				      
				      || (legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getSecondDocumentType()!=null && legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getSecondDocumentType().equals(holderHistory.getDocumentType()) &&
				    		  legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getSecondDocumentNumber()!=null && legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getSecondDocumentNumber().equals(holderHistory.getDocumentNumber()))
						      ){
					   Object[] bodyData = new Object[0];
					   JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesUtilities.getMessage(
							   PropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE,bodyData));
					   JSFUtilities.showSimpleValidationDialog();
					   JSFUtilities.addContextMessage(idComponent,FacesMessage.SEVERITY_ERROR,PropertiesUtilities.getMessage(
												PropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE,bodyData),
												PropertiesUtilities.getMessage(PropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE,bodyData));
						holderHistory.setDocumentNumber(null); 
						return;
				   }
				   else{
					   if((legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentType().equals(holderHistory.getDocumentType()) &&
							   legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentNumber().equals(holderHistory.getDocumentNumber()) &&
							   (legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentType().equals(DocumentType.CI.getCode())
									   || legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentType().equals(DocumentType.PAS.getCode())
									   || legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentType().equals(DocumentType.CID.getCode())
									   || legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentType().equals(DocumentType.CDN.getCode())) 
									   //&& legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentSource().equals(holderHistory.getDocumentSource())
									   )
									   || (legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getSecondDocumentType()!=null && legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getSecondDocumentType().equals(holderHistory.getDocumentType()) &&
									   legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getSecondDocumentNumber()!=null && legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getSecondDocumentNumber().equals(holderHistory.getDocumentNumber()))
									   ){
						   Object[] bodyData = new Object[0];
						   JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesUtilities.getMessage(
								   PropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE,bodyData));
						   JSFUtilities.showSimpleValidationDialog();
						   JSFUtilities.addContextMessage(idComponent,FacesMessage.SEVERITY_ERROR,PropertiesUtilities.getMessage(
															PropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE,bodyData),
															PropertiesUtilities.getMessage(PropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE,bodyData));
						   holderHistory.setDocumentNumber(null);
						   return;
					   }
				   }
			   }
			}
			
			//preguntamos si el numero de documento pertenece a una safi.
			if (validateDocumentHolder()){
				Object[] bodyData;
				bodyData = new Object[0];
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.HOLDER_INVESTMENT_FUNDS_OPENED,bodyData));
				JSFUtilities.executeJavascriptFunction("PF('cnfwHolderInvestmentFundsOpened').show()");
				return;
			}
						
			if(documentValidator.validateFormatDocumentNumber(holderHistory.getDocumentType(),holderHistory.getDocumentNumber(),idComponent)){
				if(holderHistory.getHolderType()!=null && holderHistory.getHolderType().equals(PersonType.JURIDIC.getCode())){
					if(holderHistory.getJuridicClass()!=null && holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())){
						if(holderHistory.getDocumentType()!=null && holderHistory.getDocumentType().equals(DocumentType.RUC.getCode())){
							Holder objHolder = new Holder();
							objHolder.setDocumentType(holderHistory.getDocumentType());
							objHolder.setDocumentNumber(holderHistory.getDocumentNumber());	
							objHolder.setListSectorActivity(new ArrayList<Integer>());
							objHolder.getListSectorActivity().add(EconomicActivityType.BANCOS.getCode());							
							//objHolder.setEconomicActivity(EconomicActivityType.BANCOS.getCode());
							holderResult = accountsFacade.getHolderByDocumentTypeAndDocumentNumberServiceFacade(objHolder);
							if(Validations.validateIsNullOrEmpty(holderResult)){
								JSFUtilities.addContextMessage(idComponent,FacesMessage.SEVERITY_ERROR,PropertiesUtilities.getMessage(PropertiesConstants.ERROR_PERSON_JURIDIC_CLASS_TRUST_ECONOMIC_ACTIVITY_BANK,new Object[0]),
										PropertiesUtilities.getMessage(PropertiesConstants.ERROR_PERSON_JURIDIC_CLASS_TRUST_ECONOMIC_ACTIVITY_BANK,new Object[0]));
								JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesUtilities.getMessage(
										PropertiesConstants.ERROR_PERSON_JURIDIC_CLASS_TRUST_ECONOMIC_ACTIVITY_BANK,new Object[0]));
								JSFUtilities.showSimpleValidationDialog();
								holderHistory.setDocumentNumber(null);
								return;
							} else {
								holderResult.setEconomicActivity(EconomicActivityType.OTROS_SERVICIOS_FINANCIEROS.getCode());
								Object[] bodyData;
								bodyData = new Object[0];
								JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
										PropertiesUtilities.getMessage(PropertiesConstants.HOLDER_REGISTER_NEW_TRUST,bodyData));
								JSFUtilities.executeJavascriptFunction("PF('cnfwHolderTrust').show()");
								return;
							}
						}
					}
				}
				Holder objHolder = new Holder();
				objHolder.setDocumentType(holderHistory.getDocumentType());
				objHolder.setDocumentNumber(holderHistory.getDocumentNumber());	
				objHolder.setNationality(holderHistory.getNationality());
				objHolder.setRelatedCui(holderHistory.getRelatedCui());
				objHolder.setDocumentSource(holderHistory.getDocumentSource());
				objHolder.setFirstLastName(holderHistory.getFirstLastName());
				
				List<Holder>listHolder = accountsFacade.getHoldersServiceFacade(objHolder);
				if(listHolder!=null && listHolder.size()>0){
					String documentName = null;
					String documentNumber = null;
					String message=GeneralConstants.EMPTY_STRING;
					String slave = GeneralConstants.BLANK_SPACE;
					for(Holder holder : listHolder){
						documentName = parameterServiceBean.findParameterTableDescription(holder.getDocumentType());
						documentNumber = holder.getDocumentNumber();
						slave = slave + PropertiesUtilities.getExceptionMessage(ErrorServiceType.EXIST_HOLDER.getMessage(),new Object[]{holder.getFullName().toUpperCase()});
					}
					Object[] bodyData = new Object[3];
					bodyData[0]= documentName;
					bodyData[1]= documentNumber;
					message = message.concat(GeneralConstants.BLANK_SPACE).concat(slave).trim();
					bodyData[2]= message;
					message = PropertiesUtilities.getExceptionMessage(ErrorServiceType.EXIST_HOLDER_HEADLER.getMessage(),bodyData);
					String header = PropertiesUtilities.getMessage("header.message.alert");
					JSFUtilities.showMessageOnDialog(header,message);
					JSFUtilities.showSimpleValidationDialog();
					holderHistory.setDocumentNumber(null);
					holderHistory.setFullName(null);
					return;
				}else{
					HolderRequest auxHolderRequest = new HolderRequest();
					List<HolderHistory> lstHolderHistory = new ArrayList<HolderHistory>();
					lstHolderHistory.add(holderHistory);
					auxHolderRequest.setHolderHistories(lstHolderHistory);
					if(holderRequest.getIdHolderRequestPk()!=null){
						auxHolderRequest.setIdHolderRequestPk(holderRequest.getIdHolderRequestPk());
					}
					auxHolderRequest = holderServiceFacade.getHolderRequestByDocumentTypeAndDocumentNumberServiceFacade(auxHolderRequest);
					if(auxHolderRequest!=null && !auxHolderRequest.getStateHolderRequest().equals(HolderRequestStateType.CONFIRMED.getCode())){
						Object[] bodyData = new Object[1];
						bodyData[0] = auxHolderRequest.getIdHolderRequestPk().toString();
						JSFUtilities.showMessageOnDialog(GeneralConstants.EMPTY_STRING, PropertiesUtilities.getMessage(
												PropertiesConstants.WARNING_DOCUMENTTYPE_DOCUMENTNUMBER_OTHER_HOLDER_REQUEST,bodyData));
						JSFUtilities.showSimpleValidationDialog();
						holderHistory.setDocumentNumber(null);	
						holderHistory.setDocumentSource(null);
						
						JSFUtilities.addContextMessage(idComponent,FacesMessage.SEVERITY_ERROR,PropertiesUtilities.getMessage(
								PropertiesConstants.WARNING_DOCUMENTTYPE_DOCUMENTNUMBER_OTHER_HOLDER_REQUEST,bodyData),
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_DOCUMENTTYPE_DOCUMENTNUMBER_OTHER_HOLDER_REQUEST,bodyData));
						return;
					}
				}
				loadNewDocumentNumber();
			}else{
				holderHistory.setDocumentNumber(null);
				return;
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	//Segun Issue 1050
	/**
	 * Cargando el numero de documento
	 */
	public void loadNewDocumentNumber(){
		
		if(holderHistory.getDocumentType()!=null && holderHistory.getDocumentNumber() != null){
			
			newDocumentNumber = holderHistory.getDocumentNumber();
			
			if(holderHistory.getDocumentType().equals(DocumentType.CIE.getCode())){
				newDocumentNumber = "E-" + holderHistory.getDocumentNumber();
			}
			if(holderHistory.getDocumentType().equals(DocumentType.DCD.getCode())){
				newDocumentNumber = "DCD" + holderHistory.getDocumentNumber();
			}
			if(holderHistory.getDocumentType().equals(DocumentType.DCC.getCode())){
				newDocumentNumber = "DCC" + holderHistory.getDocumentNumber();
			}
			if(holderHistory.getDocumentType().equals(DocumentType.DCR.getCode())){
				newDocumentNumber = "DCR" + holderHistory.getDocumentNumber();
			}
			
			if(holderHistory.getDocumentIssuanceDate() != null){
				
				if(holderHistory.getDocumentType().equals(DocumentType.DCD.getCode()) ||
					holderHistory.getDocumentType().equals(DocumentType.DCC.getCode()) ||
					holderHistory.getDocumentType().equals(DocumentType.DCR.getCode()) ||
					holderHistory.getDocumentType().equals(DocumentType.DCD.getCode()) ){
					
					newDocumentNumber = newDocumentNumber + holderHistory.getDocumentIssuanceDate()+"";
				}
			}
		}
		
	}

	/**
	 * Validate second type document holder.
	 *
	 * @param idComponent the id component
	 */
	public void validateSecondTypeDocumentHolder(String idComponent) {

		if (validateTypesDocumentsHolder(idComponent)) {

			documentTO = documentValidator.loadMaxLenghtType(holderHistory.getSecondDocumentType());
			
			numericSecondTypeDocumentHolder = documentTO.isNumericTypeDocument();
			maxLenghtSecondDocumentNumberHolder = documentTO.getMaxLenghtDocumentNumber();
			
			if(holderHistory.getSecondDocumentType() != null && holderHistory.getSecondDocumentNumber() != null){
				if(holderHistory.getSecondDocumentType().equals(DocumentType.CIE.getCode()) ){
					newDocumentNumberSecondNationality = "E-" + holderHistory.getSecondDocumentNumber();
				}
				else{
					newDocumentNumberSecondNationality = holderHistory.getSecondDocumentNumber();
				}
			}
		}
		else{
			holderHistory.setSecondDocumentType(null);
			newDocumentNumberSecondNationality = null;
		}
	}
	
	
	
	
	/**
	 * Validate second nationality by ind resident.
	 */
	private void validateSecondNationalityByIndResident(){
		if(holderHistory.getNationality()!=null && !holderHistory.getNationality().equals(countryResidence) && BooleanType.YES.getCode().equals(holderHistory.getIndResidence()) &&
				countryResidence.equals(holderHistory.getSecondNationality())){
			
			holderHistory.setSecondDocumentType(null);
			holderHistory.setSecondDocumentNumber(null);
			newDocumentNumberSecondNationality = GeneralConstants.EMPTY_STRING;
			
		   if(isViewOperationRegister()){
			 SelectOneMenu cmbSecondDocumentType = (SelectOneMenu)  FacesContext.getCurrentInstance().getViewRoot().findComponent("frmHolderRegister:tabView:cmbSecondDocumentType");
			 if(cmbSecondDocumentType!=null){cmbSecondDocumentType.setValid(true);}
			 InputText inputSecondDocumentNumber = (InputText)  FacesContext.getCurrentInstance().getViewRoot().findComponent("frmHolderRegister:tabView:inputSecondDocumentNumber");
			 if(inputSecondDocumentNumber!=null){inputSecondDocumentNumber.setValid(true);}
		   }
		   if(isViewOperationModify()){
			   SelectOneMenu cmbSecondDocumentType = (SelectOneMenu)  FacesContext.getCurrentInstance().getViewRoot().findComponent("frmModifyHolderRequest:tabView:cmbSecondDocumentType");
				 if(cmbSecondDocumentType!=null){cmbSecondDocumentType.setValid(true);}
				 InputText inputSecondDocumentNumber = (InputText)  FacesContext.getCurrentInstance().getViewRoot().findComponent("frmModifyHolderRequest:tabView:inputSecondDocumentNumber");
				 if(inputSecondDocumentNumber!=null){inputSecondDocumentNumber.setValid(true);}
		   }
		}
		
	}

	/**
	 * Validate types documents holder.
	 *
	 * @param idComponent the id component
	 * @return true, if successful
	 */
	public boolean validateTypesDocumentsHolder(String idComponent) {

		boolean flag = true;

		if (holderHistory.getSecondDocumentType()!=null && holderHistory.getDocumentType()!=null && holderHistory.getDocumentType().equals(
				holderHistory.getSecondDocumentType())) {
			
			if(!holderHistory.getSecondDocumentType().equals(DocumentType.DIO.getCode())){
			
			flag = false;

			JSFUtilities
					.addContextMessage(
							idComponent,
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities
									.getMessage(PropertiesConstants.ERROR_SAME_TYPE_DOCUMENT),
							PropertiesUtilities
									.getMessage(PropertiesConstants.ERROR_SAME_TYPE_DOCUMENT));
			JSFUtilities.showMessageOnDialog(null, null,
					PropertiesConstants.ERROR_SAME_TYPE_DOCUMENT, null);
			JSFUtilities.showSimpleValidationDialog();			
			return false;
			
			}
		}

		return flag;
	}

	/**
	 * Gets the holder request.
	 * 
	 * @return the holder request
	 */
	public HolderRequest getHolderRequest() {
		return holderRequest;
	}

	/**
	 * Sets the holder request.
	 * 
	 * @param holderRequest
	 *            the new holder request
	 */
	public void setHolderRequest(HolderRequest holderRequest) {
		this.holderRequest = holderRequest;
	}
	
	/**
	 * Gets the streamed content temporal file holder history.
	 *
	 * @param holderReqFileHistory the holder req file history
	 * @return the streamed content temporal file holder history
	 */
	public StreamedContent getStreamedContentTemporalFileHolderHistory(
			HolderReqFileHistory holderReqFileHistory) {
			
		
		if(holderReqFileHistory.isWasModified() || getViewOperationType().equals(ViewOperationsType.REGISTER.getCode())){

		try {
			InputStream inputStream;

			inputStream = new ByteArrayInputStream(holderReqFileHistory.getFileHolderReq());
			streamedContentFile = new DefaultStreamedContent(inputStream, null,
					holderReqFileHistory.getFilename());
			inputStream.close();

		 } catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		 }
		
		}
		else{
			
			streamedContentFile = getStreamedContentFileHolderHistory(holderRequestSelected.getIdHolderRequestPk(),holderReqFileHistory.getDocumentType());
			}
		
		return streamedContentFile;
	}

	/**
	 * Gets the streamed content file holder history.
	 *
	 * @param id the id
	 * @param documentType the document type
	 * @return the streamed content file holder history
	 */
	public StreamedContent getStreamedContentFileHolderHistory(Long id,
			Integer documentType) {

		
		try {
			HolderReqFileHistory auxHolderReqFileHistory = holderServiceFacade
					.getContentFileHolderHistoryServiceFacade(id, documentType,false);
			InputStream inputStream;

			inputStream = new ByteArrayInputStream(auxHolderReqFileHistory.getFileHolderReq());
			streamedContentFile = new DefaultStreamedContent(inputStream, null,auxHolderReqFileHistory.getFilename());
			inputStream.close();

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

		return streamedContentFile;
	}

	/**
	 * Loading holder information.
	 * 
	 * @param event
	 *            the event
	 */
	public void loadingHolderInformation(ActionEvent event) {		
		try {
			cleanFileImage();
			participantDetail = new Participant();
			holderRequestDetail = new HolderRequest();
			holderHistoryDetail = new HolderHistory();
			holderHistory = new HolderHistory();
			
			List<LegalRepresentativeHistory> lstLegalRepresentativeHistoryDetail = new ArrayList<LegalRepresentativeHistory>();
			
			if(event!=null){
			
			setViewOperationType(ViewOperationsType.DETAIL.getCode());
			
			LegalRepresentativeTO legalRepresentativeTO = new LegalRepresentativeTO();
			legalRepresentativeTO.setViewOperationType(ViewOperationsType.DETAIL.getCode());
			legalRepresentativeHelperBean.setLegalRepresentativeTO(legalRepresentativeTO);
		
			holderRequestDetail = (HolderRequest) event.getComponent()
					.getAttributes().get("holderInformation");
			}
			else{
			
			LegalRepresentativeTO legalRepresentativeTO = new LegalRepresentativeTO();
			legalRepresentativeTO.setViewOperationType(ViewOperationsType.DETAIL.getCode());
			legalRepresentativeHelperBean.setLegalRepresentativeTO(legalRepresentativeTO);
				
			 holderRequestDetail = holderRequestSelected;
			}

			holderRequestDetail = holderServiceFacade
					.getHolderRequestByIdRequestServiceFacade(holderRequestDetail
							.getIdHolderRequestPk());
			
			
			holderHistoryDetail = holderRequestDetail.getHolderHistories().get(0);
						
			JSFUtilities.putSessionMap("sessStreamedPhoto", holderHistoryDetail.getFilePhoto());
			
			JSFUtilities.putSessionMap("sessStreamedSigning", holderHistoryDetail.getFileSigning());

			lstHolderReqFileHistory = holderRequestDetail.getHolderReqFileHistories();

			if ((holderRequestDetail.getParticipant().getIdParticipantPk() != null)) {
				participantDetail.setIdParticipantPk(holderRequestDetail
						.getParticipant().getIdParticipantPk());
			}

			getLstBooleanType();
			getLstDocumentType();
			getLstParticipant();
			getLstPersonType();
			getLstSexType();
			getLstJuridicClass();
			getLstGeographicLocation();
			getLstGeographicLocationWithoutNationalCountry();			
			getLstRolePep();
			getGradePep();
			getLstCategoryPep();
			getLstEconomicSector();
			changeSelectedEconomicSector();
			getLstDocumentSource();
			getListEconomicActivityWithoutSector();
			
			listDepartmentLocation = getLstDepartmentLocation(holderHistoryDetail.getLegalResidenceCountry());
			
			listProvinceLocation = getLstProvinceLocation(holderHistoryDetail.getLegalDepartment());

			listMunicipalityLocation = getLstMunicipalityLocation(holderHistoryDetail.getLegalProvince());
			
			listDepartmentPostalLocation = getLstDepartmentLocation(holderHistoryDetail.getPostalResidenceCountry());
			
			listProvincePostalLocation = getLstProvinceLocation(holderHistoryDetail.getPostalDepartment());

			listMunicipalityPostalLocation = getLstMunicipalityLocation(holderHistoryDetail.getPostalProvince());
			
			if (holderHistoryDetail.getHolderType().equals(
					PersonType.NATURAL.getCode())) {

				flagHolderJuridicPerson = false;
				flagHolderNaturalPerson = true;

			} else if (holderHistoryDetail.getHolderType().equals(
					PersonType.JURIDIC.getCode())) {

				flagHolderJuridicPerson = true;
				flagHolderNaturalPerson = false;
				
				disabledIndResident = true;
				disabledCountryResidentHolder=true;
				
				if(holderHistoryDetail.getNationality().equals(countryResidence)){
					holderHistoryDetail.setIndResidence(BooleanType.YES.getCode());
					holderHistoryDetail.setLegalResidenceCountry(countryResidence);
				}

			}

			if (holderHistoryDetail.getIndPEP()
					.equals(BooleanType.YES.getCode())) {
				holderHistoryDetail.setIndicatorPep(true);
				
			} else if (holderHistoryDetail.getIndPEP()
					.equals(BooleanType.NO.getCode())) {
				holderHistoryDetail.setIndicatorPep(false);
			}

			if (holderHistoryDetail.getIndDisabled() != null
					&& (holderHistoryDetail.getIndDisabled()
							.equals(BooleanType.YES.getCode()))) {
				holderHistoryDetail.setIndicatorDisabled(
						true);
			} else if (holderHistoryDetail.getIndDisabled() != null
					&& (holderHistoryDetail.getIndDisabled()
							.equals(BooleanType.NO.getCode()))) {
				holderHistoryDetail.setIndicatorDisabled(
						false);
			}

			if (holderHistoryDetail.getIndMinor()
					.equals(BooleanType.YES.getCode())) {
				holderHistoryDetail.setIndicatorMinor(true);
			} else if (holderHistoryDetail.getIndMinor()
					.equals(BooleanType.NO.getCode())) {
				holderHistoryDetail.setIndicatorMinor(false);
			}
			
			if (holderRequestDetail.getLegalRepresentativeHistories() != null) {
				for (int i = 0; i < holderRequestDetail
						.getLegalRepresentativeHistories().size(); i++) {

					LegalRepresentativeHistory legalRepresentativeHistoryAux = new LegalRepresentativeHistory();

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getIndPEP() != null
							&& holderRequestDetail
									.getLegalRepresentativeHistories().get(i)
									.getIndPEP()
									.equals(BooleanType.YES.getCode())) {
						flagRepresentativeIsPEP = true;						
						
						legalRepresentativeHistoryAux
								.setCategory(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getCategory());
						legalRepresentativeHistoryAux
								.setRole(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getRole());
						legalRepresentativeHistoryAux
								.setBeginningPeriod(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getBeginningPeriod());
						legalRepresentativeHistoryAux
								.setEndingPeriod(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getEndingPeriod());
					} else if (holderRequestDetail
							.getLegalRepresentativeHistories().get(i)
							.getIndPEP() != null
							&& holderRequestDetail
									.getLegalRepresentativeHistories().get(i)
									.getIndPEP()
									.equals(BooleanType.NO.getCode())) {
						flagRepresentativeIsPEP = false;
					}
					
					if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getEconomicSector()!=null){
					legalRepresentativeHistoryAux.setEconomicSector(holderRequestDetail.getLegalRepresentativeHistories().get(i).getEconomicSector());
					}
					
					if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getEconomicActivity()!=null){
					legalRepresentativeHistoryAux.setEconomicActivity(holderRequestDetail.getLegalRepresentativeHistories().get(i).getEconomicActivity());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getIdRepresentativeHistoryPk() != null) {
						legalRepresentativeHistoryAux
								.setIdRepresentativeHistoryPk(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getIdRepresentativeHistoryPk());
					}
					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getBirthDate() != null) {
						legalRepresentativeHistoryAux
								.setBirthDate(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getBirthDate());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getDocumentNumber() != null) {
						legalRepresentativeHistoryAux
								.setDocumentNumber(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getDocumentNumber());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getDocumentType() != null) {
						legalRepresentativeHistoryAux
								.setDocumentType(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getDocumentType());
					}
					
					if(holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getDocumentSource() != null){
						legalRepresentativeHistoryAux
						.setDocumentSource(holderRequestDetail
								.getLegalRepresentativeHistories().get(i).getDocumentSource());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getEconomicActivity() != null) {
						legalRepresentativeHistoryAux
								.setEconomicActivity(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getEconomicActivity());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getEconomicSector() != null) {
						legalRepresentativeHistoryAux
								.setEconomicSector(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getEconomicSector());
					}
					
					if (holderRequestDetail.getLegalRepresentativeHistories().get(i).getDocumentIssuanceDate() != null) {
						legalRepresentativeHistoryAux.setDocumentIssuanceDate(holderRequestDetail.getLegalRepresentativeHistories().get(i).getDocumentIssuanceDate());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getEmail() != null) {
						legalRepresentativeHistoryAux
								.setEmail(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getEmail());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getFaxNumber() != null) {
						legalRepresentativeHistoryAux
								.setFaxNumber(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getFaxNumber());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getFirstLastName() != null) {
						legalRepresentativeHistoryAux
								.setFirstLastName(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getFirstLastName());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getFullName() != null) {
						legalRepresentativeHistoryAux
								.setFullName(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getFullName());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getHomePhoneNumber() != null) {
						legalRepresentativeHistoryAux
								.setHomePhoneNumber(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getHomePhoneNumber());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getIndResidence() != null) {

						legalRepresentativeHistoryAux
								.setIndResidence(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getIndResidence());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getLastModifyApp() != null) {
						legalRepresentativeHistoryAux
								.setLastModifyApp(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getLastModifyApp());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getLastModifyDate() != null) {
						legalRepresentativeHistoryAux
								.setLastModifyDate(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getLastModifyDate());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getLastModifyIp() != null) {
						legalRepresentativeHistoryAux
								.setLastModifyIp(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getLastModifyIp());
					}
					
					if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getLegalDepartment()!=null){
						legalRepresentativeHistoryAux.setLegalDepartment(holderRequestDetail.getLegalRepresentativeHistories().get(i).getLegalDepartment());
					}
					
					if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getPostalDepartment()!=null){
						legalRepresentativeHistoryAux.setPostalDepartment(holderRequestDetail.getLegalRepresentativeHistories().get(i).getPostalDepartment());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getLastModifyUser() != null) {
						legalRepresentativeHistoryAux
								.setLastModifyUser(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getLastModifyUser());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getLegalAddress() != null) {
						legalRepresentativeHistoryAux
								.setLegalAddress(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getLegalAddress());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getLegalDepartment() != null) {
						legalRepresentativeHistoryAux
								.setLegalDepartment(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getLegalDepartment());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getLegalDistrict() != null) {
						legalRepresentativeHistoryAux
								.setLegalDistrict(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getLegalDistrict());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getLegalProvince() != null) {
						legalRepresentativeHistoryAux
								.setLegalProvince(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getLegalProvince());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getLegalResidenceCountry() != null) {
						legalRepresentativeHistoryAux
								.setLegalResidenceCountry(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getLegalResidenceCountry());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getMobileNumber() != null) {
						legalRepresentativeHistoryAux
								.setMobileNumber(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getMobileNumber());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getName() != null) {
						legalRepresentativeHistoryAux
								.setName(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getName());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getNationality() != null) {
						legalRepresentativeHistoryAux
								.setNationality(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getNationality());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getOfficePhoneNumber() != null) {
						legalRepresentativeHistoryAux
								.setOfficePhoneNumber(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getOfficePhoneNumber());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getPostalDepartment() != null) {
						legalRepresentativeHistoryAux
								.setPostalDepartment(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getPostalDepartment());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getPostalDistrict() != null) {
						legalRepresentativeHistoryAux
								.setPostalDistrict(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getPostalDistrict());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getPostalProvince() != null) {
						legalRepresentativeHistoryAux
								.setPostalProvince(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getPostalProvince());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getPostalResidenceCountry() != null) {
						legalRepresentativeHistoryAux
								.setPostalResidenceCountry(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getPostalResidenceCountry());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getPostalAddress() != null) {
						legalRepresentativeHistoryAux
								.setPostalAddress(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getPostalAddress());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getRegistryUser() != null) {
						legalRepresentativeHistoryAux
								.setRegistryUser(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getRegistryUser());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getSecondDocumentNumber() != null) {
						legalRepresentativeHistoryAux
								.setSecondDocumentNumber(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getSecondDocumentNumber());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getSecondLastName() != null) {
						legalRepresentativeHistoryAux
								.setSecondLastName(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getSecondLastName());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getSecondNationality() != null) {
						legalRepresentativeHistoryAux
								.setSecondNationality(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getSecondNationality());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getSex() != null) {
						legalRepresentativeHistoryAux
								.setSex(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getSex());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getStateRepreHistory() != null) {
						legalRepresentativeHistoryAux
								.setStateRepreHistory(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getStateRepreHistory());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getPersonType() != null) {
						legalRepresentativeHistoryAux
								.setPersonType(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getPersonType());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getRepresentativeClass() != null) {
						legalRepresentativeHistoryAux
								.setRepresentativeClass(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getRepresentativeClass());
					}

					if (holderRequestDetail.getLegalRepresentativeHistories()
							.get(i).getSecondDocumentType() != null) {
						legalRepresentativeHistoryAux
								.setSecondDocumentType(holderRequestDetail
										.getLegalRepresentativeHistories()
										.get(i).getSecondDocumentType());
					}

					legalRepresentativeHistoryAux
							.setRepresentativeTypeDescription(PersonType.get(
									holderRequestDetail
											.getLegalRepresentativeHistories()
											.get(i).getPersonType()).getValue());

					legalRepresentativeHistoryAux
							.setDocumentTypeDescription(DocumentType.get(
									holderRequestDetail
											.getLegalRepresentativeHistories()
											.get(i).getDocumentType())
									.getValue());

					legalRepresentativeHistoryAux
							.setRepresenteFileHistory(holderRequestDetail
									.getLegalRepresentativeHistories().get(i)
									.getRepresenteFileHistory());

					lstLegalRepresentativeHistoryDetail
							.add(legalRepresentativeHistoryAux);
					
										
				}
				
				legalRepresentativeHelperBean.setLstLegalRepresentativeHistory(lstLegalRepresentativeHistoryDetail);
				
			}
			
			

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
		/**
	 * Removes the holder request file.
	 *
	 * @param holderRequestFile the holder request file
	 */
	public void removeHolderRequestFile(HolderReqFileHistory holderRequestFile) {
		lstHolderReqFileHistory.remove(holderRequestFile);
		holderFileNameDisplay = null;
		attachFileHolderSelected = null;
	}
	
	/**
	 * Validate by participant.
	 */
	public void validateByParticipant(){
		try
		{   
			if(participant.getIdParticipantPk()!=null){
			
				Participant filter = new Participant();
				filter.setIdParticipantPk(participant.getIdParticipantPk());
				participant = accountsFacade.findParticipantByFilters(filter);
				
				if(participant!=null && participant.getAccountClass()!=null &&
						holderHistory.getHolderType()!=null){
					clientInformationTO=clientInformationValidator.validateShowClientInformation(participant.getAccountClass(),holderHistory.getHolderType());
				}	
				
				participantAFP_FIC_PAT=false;
				cleanStylesEconomicActivity();
				if(participant.getAccountClass().equals(ParticipantClassType.AFP.getCode())){
					participantAFP_FIC_PAT = true;
					setDisabledEconomicActivity(Boolean.FALSE,Boolean.FALSE,Boolean.FALSE);
					strStyleRequiredCUI = STYLE_REQUIRED;
					strStyleRequiredTransferNro = STYLE_REQUIRED;
				}else{
					showFieldsEconomicActivity();
					return;
				}
			
				
				holderHistory.setHolderType(PersonType.JURIDIC.getCode());
				
				loadByHolderTypePerson();
				holderHistory.setRelatedCui(participant.getHolder().getIdHolderPk());
				holderHistory.setFundAdministrator(null);
				holderHistory.setFundType(null);
				holderHistory.setTransferNumber(null);
				holderHistory.setMnemonic(null);
				List<LegalRepresentativeHistory> lstLegalRepresentativeHistory = new ArrayList<LegalRepresentativeHistory>();
				
				PepPerson pepPerson;
				if(holderHistory.getRelatedCui()!=null){
					pepPerson = holderServiceFacade.getPepPersonByIdHolderServiceFacade(holderHistory.getRelatedCui());
					if(pepPerson!=null){
						holderHistory.setBeginningPeriod(pepPerson.getBeginingPeriod());
						holderHistory.setEndingPeriod(pepPerson.getEndingPeriod());
						holderHistory.setCategory(pepPerson.getCategory());
						holderHistory.setRole(pepPerson.getRole());
						holderHistory.setIndPEP(BooleanType.YES.getCode());
						holderHistory.setIndicatorPep(true);
					}
					if(participant.getResidenceCountry()!=null){
						holderHistory.setNationality(participant.getResidenceCountry());
						holderHistory.setLegalResidenceCountry(participant.getResidenceCountry());
						validateResidentHolder("frmHolderRegister:tabView:cmbNationality2");						
						holderHistory.setPostalResidenceCountry(participant.getResidenceCountry());
					}
					if(participant.getDocumentType()!=null){
						holderHistory.setDocumentType(participant.getDocumentType());
						validateTypeDocumentHolder("frmHolderRegister:tabView:cmbDocumentType2");
					}
					if(participant.getDocumentNumber()!=null){
						holderHistory.setDocumentNumber(participant.getDocumentNumber());
					}
					if(participant.getEconomicSector()!=null){
						holderHistory.setEconomicSector(participant.getEconomicSector());
					}
					if(participant.getEconomicActivity()!=null){
						holderHistory.setEconomicActivity(participant.getEconomicActivity());
						changeSelectedEconomicSector();
					}
					if(participant.getDepartment()!=null){
						holderHistory.setLegalDepartment(participant.getDepartment());
						holderHistory.setPostalDepartment(participant.getDepartment());
					}
					if(participant.getProvince()!=null){
						holderHistory.setLegalProvince(participant.getProvince());
						holderHistory.setPostalProvince(participant.getProvince());
					}
					if(participant.getDistrict()!=null){
						holderHistory.setLegalDistrict(participant.getDistrict());
						holderHistory.setPostalDistrict(participant.getDistrict());
					}
					if(participant.getAddress()!=null){
						holderHistory.setLegalAddress(participant.getAddress());
						holderHistory.setPostalAddress(participant.getAddress());
					}
					
					listDepartmentLocation = getLstDepartmentLocation(holderHistory.getLegalResidenceCountry());
					
					listProvinceLocation = getLstProvinceLocation(holderHistory.getLegalDepartment());

					listMunicipalityLocation = getLstMunicipalityLocation(holderHistory.getLegalProvince());			
					
					listDepartmentPostalLocation = getLstDepartmentLocation(holderHistory.getPostalResidenceCountry());
					
					listProvincePostalLocation = getLstProvinceLocation(holderHistory.getPostalDepartment());

					listMunicipalityPostalLocation = getLstMunicipalityLocation(holderHistory.getPostalProvince());
					
					legalRepresentativeHelperBean.initializeLstLegalRepresentativeHistory();
					legalRepresentativeHelperBean.getLegalRepresentativeTO().setViewOperationType(ViewOperationsType.REGISTER.getCode());
					
					if(participant.getRepresentedEntities()!=null){
						for(int i=0;i<participant.getRepresentedEntities().size();i++){
							if(participant.getRepresentedEntities().get(i).getStateRepresented().equals(RepresentativeEntityStateType.REGISTERED.getCode())){
							
							LegalRepresentativeHistory legalRepresentativeHistory = new LegalRepresentativeHistory();
							
							legalRepresentativeHistory.setHolderRequest(holderRequest);
							
							legalRepresentativeHistory.setRepresentativeClass(participant.getRepresentedEntities().get(i).getRepresentativeClass());
							
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getBirthDate()!=null){
								legalRepresentativeHistory.setBirthDate(participant.getRepresentedEntities().get(i).getLegalRepresentative().getBirthDate());
							}
						    
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getDocumentNumber()!=null){
								legalRepresentativeHistory.setDocumentNumber(participant.getRepresentedEntities().get(i).getLegalRepresentative().getDocumentNumber());
							}
						    
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getDocumentType()!=null){
								legalRepresentativeHistory.setDocumentType(participant.getRepresentedEntities().get(i).getLegalRepresentative().getDocumentType());
							}
						    
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getEconomicActivity()!=null){
								legalRepresentativeHistory.setEconomicActivity(participant.getRepresentedEntities().get(i).getLegalRepresentative().getEconomicActivity());
							}
						    
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getEconomicSector()!=null){
								legalRepresentativeHistory.setEconomicSector(participant.getRepresentedEntities().get(i).getLegalRepresentative().getEconomicSector());
							}
						    
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getEmail()!=null){
								legalRepresentativeHistory.setEmail(participant.getRepresentedEntities().get(i).getLegalRepresentative().getEmail());
							}
						    
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getFaxNumber()!=null){
								legalRepresentativeHistory.setFaxNumber(participant.getRepresentedEntities().get(i).getLegalRepresentative().getFaxNumber());
							}
						    
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getFirstLastName()!=null){
								legalRepresentativeHistory.setFirstLastName(participant.getRepresentedEntities().get(i).getLegalRepresentative().getFirstLastName());
							}
						    
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getFullName()!=null){
								legalRepresentativeHistory.setFullName(participant.getRepresentedEntities().get(i).getLegalRepresentative().getFullName());
							}
							
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getHomePhoneNumber()!=null){
								legalRepresentativeHistory.setHomePhoneNumber(participant.getRepresentedEntities().get(i).getLegalRepresentative().getHomePhoneNumber());
							}
						    
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getIndResidence()!=null){
								
								legalRepresentativeHistory.setIndResidence(participant.getRepresentedEntities().get(i).getLegalRepresentative().getIndResidence()); 
							}
							
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getLastModifyUser()!=null){
								legalRepresentativeHistory.setLastModifyUser(userInfo.getUserAccountSession().getFullName());
							}
						    
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getLegalAddress()!=null){
								legalRepresentativeHistory.setLegalAddress(participant.getRepresentedEntities().get(i).getLegalRepresentative().getLegalAddress());
							}
						    
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getLegalDepartment()!=null){
								legalRepresentativeHistory.setLegalDepartment(participant.getRepresentedEntities().get(i).getLegalRepresentative().getLegalDepartment());
							}
						    
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getLegalDistrict()!=null){
								legalRepresentativeHistory.setLegalDistrict(participant.getRepresentedEntities().get(i).getLegalRepresentative().getLegalDistrict());
							}
						    
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getLegalProvince()!=null){
								legalRepresentativeHistory.setLegalProvince(participant.getRepresentedEntities().get(i).getLegalRepresentative().getLegalProvince());
							}
						    
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getLegalResidenceCountry()!=null){
								legalRepresentativeHistory.setLegalResidenceCountry(participant.getRepresentedEntities().get(i).getLegalRepresentative().getLegalResidenceCountry());
							}
						    
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getMobileNumber()!=null){
								legalRepresentativeHistory.setMobileNumber(participant.getRepresentedEntities().get(i).getLegalRepresentative().getMobileNumber());
							}
						    
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getName()!=null){
								legalRepresentativeHistory.setName(participant.getRepresentedEntities().get(i).getLegalRepresentative().getName()); 
							}
							
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getNationality()!=null){
								legalRepresentativeHistory.setNationality(participant.getRepresentedEntities().get(i).getLegalRepresentative().getNationality());
							}
						    
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getOfficePhoneNumber()!=null){
								legalRepresentativeHistory.setOfficePhoneNumber(participant.getRepresentedEntities().get(i).getLegalRepresentative().getOfficePhoneNumber());
							}
						    
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getPostalDepartment()!=null){
								legalRepresentativeHistory.setPostalDepartment(participant.getRepresentedEntities().get(i).getLegalRepresentative().getPostalDepartment());
							}
						    
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getPostalDistrict()!=null){
								legalRepresentativeHistory.setPostalDistrict(participant.getRepresentedEntities().get(i).getLegalRepresentative().getPostalDistrict());
							}
						    
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getPostalProvince()!=null){
								legalRepresentativeHistory.setPostalProvince(participant.getRepresentedEntities().get(i).getLegalRepresentative().getPostalProvince());
							}
						    
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getPostalResidenceCountry()!=null){
								legalRepresentativeHistory.setPostalResidenceCountry(participant.getRepresentedEntities().get(i).getLegalRepresentative().getPostalResidenceCountry());
							}
							
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getPostalAddress()!=null){
								legalRepresentativeHistory.setPostalAddress(participant.getRepresentedEntities().get(i).getLegalRepresentative().getPostalAddress());
							}
						    
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getSecondDocumentNumber()!=null){
								legalRepresentativeHistory.setSecondDocumentNumber(participant.getRepresentedEntities().get(i).getLegalRepresentative().getSecondDocumentNumber());
							}
						    
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getSecondLastName()!=null){
								legalRepresentativeHistory.setSecondLastName(participant.getRepresentedEntities().get(i).getLegalRepresentative().getSecondLastName());
							}
						    
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getSecondNationality()!=null){
								legalRepresentativeHistory.setSecondNationality(participant.getRepresentedEntities().get(i).getLegalRepresentative().getSecondNationality());	
							}
							
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getSecondDocumentType()!=null){
								legalRepresentativeHistory.setSecondDocumentType(participant.getRepresentedEntities().get(i).getLegalRepresentative().getSecondDocumentType());
							}
						    
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getSex()!=null){
								legalRepresentativeHistory.setSex(participant.getRepresentedEntities().get(i).getLegalRepresentative().getSex());
							}
						    
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getState()!=null){
								legalRepresentativeHistory.setStateRepreHistory(participant.getRepresentedEntities().get(i).getLegalRepresentative().getState());
							}
							
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getPersonType()!=null){
								legalRepresentativeHistory.setPersonType(participant.getRepresentedEntities().get(i).getLegalRepresentative().getPersonType());
							}
							
							if(participant.getRepresentedEntities().get(i).getLegalRepresentative().getDocumentSource()!=null){
								legalRepresentativeHistory.setDocumentSource(participant.getRepresentedEntities().get(i).getLegalRepresentative().getDocumentSource());
							}
							
							legalRepresentativeHelperBean.loadLegalRepresentativePepExists(legalRepresentativeHistory.getIdRepresentativeHistoryPk());
							
							legalRepresentativeHistory.setRegistryUser(userInfo.getUserAccountSession().getUserName());
							
							legalRepresentativeHistory.setStateRepreHistory(HolderRequestStateType.REGISTERED.getCode());
							
							legalRepresentativeHistory.setRepreHistoryType(HolderRequestType.CREATION.getCode());
							
							legalRepresentativeHistory.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
							
							legalRepresentativeHistory.setRepresentativeTypeDescription(PersonType.get(participant.getRepresentedEntities().get(i).getLegalRepresentative().getPersonType()).getValue());
						
							legalRepresentativeHistory.setDocumentTypeDescription(DocumentType.get(participant.getRepresentedEntities().get(i).getLegalRepresentative().getDocumentType()).getValue());
						
							RepresentativeFileHistory representantiveFileHistory;
							
							List<RepresentativeFileHistory> lstRepresentativeFileHistory = new ArrayList<RepresentativeFileHistory>();
							
							for(int j=0;j<participant.getRepresentedEntities().get(i).getLegalRepresentative().getLegalRepresentativeFile().size();j++){
								
								representantiveFileHistory = new RepresentativeFileHistory();
								representantiveFileHistory.setLegalRepresentativeHistory(legalRepresentativeHistory);
								representantiveFileHistory.setFlagByLegalRepresentative(true);
								representantiveFileHistory.setDescription(
										participant.getRepresentedEntities().get(i).getLegalRepresentative().getLegalRepresentativeFile().get(j).getDescription());
								
								representantiveFileHistory.setDocumentType(
										participant.getRepresentedEntities().get(i).getLegalRepresentative().getLegalRepresentativeFile().get(j).getDocumentType());
								
								representantiveFileHistory.setFilename(
										participant.getRepresentedEntities().get(i).getLegalRepresentative().getLegalRepresentativeFile().get(j).getFilename());
								
								representantiveFileHistory.setRepresentFileHistType(HolderReqFileHisType.CREATION.getCode());
								
								representantiveFileHistory.setStateFile(HolderFileStateType.REGISTERED
										.getCode());
								
								representantiveFileHistory.setRegistryDate(CommonsUtilities.currentDate());
								
								representantiveFileHistory.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST
										.getCode());
								
								representantiveFileHistory.setRegistryUser(userInfo.getUserAccountSession().getUserName());
						
								representantiveFileHistory.setFileRepresentative(participant.getRepresentedEntities().get(i).getLegalRepresentative().getLegalRepresentativeFile().get(j).getFileLegalRepre());
								
								lstRepresentativeFileHistory.add(representantiveFileHistory);
								
							}
							
							legalRepresentativeHistory.setRepresenteFileHistory(lstRepresentativeFileHistory);
							
							lstLegalRepresentativeHistory.add(legalRepresentativeHistory);	
							
							legalRepresentativeHelperBean.setLstLegalRepresentativeHistory(lstLegalRepresentativeHistory);
						}
						
						if(legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size()>0){
							legalRepresentativeHelperBean.setFlagModifyLegalRepresentantive(true);
						}
					}

					}
				}
				
			  }
			showFieldsEconomicActivity();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Validate web page format.
	 *
	 * @param component the component
	 */
	public void validateWebPageFormat(String component){
		
		JSFUtilities.setValidViewComponent(component);
    	if(Validations.validateIsNotNullAndNotEmpty(holderHistory.getWebsite())){
    		if(!CommonsUtilities.matchWebPage(holderHistory.getWebsite())){
    			JSFUtilities.addContextMessage(component,
        				FacesMessage.SEVERITY_ERROR,
        				PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDER_VALIDATION_WEB_PAGE_FORMAT), 
        				PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDER_VALIDATION_WEB_PAGE_FORMAT));
    			
    			JSFUtilities.showMessageOnDialog(
    					PropertiesConstants.MESSAGES_ALERT, null,
						PropertiesConstants.ADM_HOLDER_VALIDATION_WEB_PAGE_FORMAT,
						null);
    			//Clean the web site
    			holderHistory.setWebsite(null);
				JSFUtilities.showSimpleValidationDialog();
    		}
    	}
	}
	
	/**
	 * Validate document holder.
	 *
	 * @return true, if successful
	 */
	public boolean validateDocumentHolder(){			
		try{
			if(Validations.validateIsNullOrEmpty(holderHistory.getDocumentNumber())){
				newDocumentNumber = null;
				return false;
			}
			/* Se comenta para no hacer doble validacion
			if(!documentValidator.validateFormatDocumentNumber(holderHistory.getDocumentType(),holderHistory.getDocumentNumber(),"frmHolderRegister:tabView:inputDocumentNumber")){
				holderHistory.setDocumentNumber(null);
				newDocumentNumber = null;
				return false;
			}*/
			if(!validateHolderInvestmentFundsOpened(holderHistory.getDocumentNumber(),holderHistory.getDocumentSource(),holderHistory.getDocumentType(),holderHistory.getNationality())){
				newDocumentNumber = null;
				return false;
			}
			
			//Validar si el nit o titular esta bloqueado o no, pendiente.
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return true;
	}
	
	/**
	 * Validate holder investment funds opened.
	 *
	 * @param documentNumber the document number
	 * @param documentSource the document source
	 * @param documentType the document type
	 * @param nationality the nationality
	 * @return true, if successful
	 */
	public boolean validateHolderInvestmentFundsOpened(String documentNumber,Integer documentSource,Integer documentType,Integer nationality){
		
		boolean flagExists=false;
//		indFICPATEconomicActivity=false;
		
		try {
			if(nationality!=null && nationality.equals(countryResidence)
					&& documentType!=null && documentType.equals(DocumentType.RUC.getCode())){
				Holder filter = new Holder();
				filter.setDocumentType(documentType);
				filter.setDocumentNumber(documentNumber);
				filter.setDocumentSource(documentSource);
				List<Integer> listEAI = new ArrayList<Integer>();
				listEAI.add(EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode());
				filter.setListSectorActivity(listEAI);
				holderResult = accountsFacade.getHolderByDocumentTypeAndDocumentNumberServiceFacade(filter);
		    	if(Validations.validateIsNotNullAndNotEmpty(holderResult)){
		    		if(Validations.validateIsNotNull(holderResult.getEconomicActivity()) && 
							(holderResult.getEconomicActivity().equals(EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode()))){
						flagExists = true;
				    }		    		
		    	}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return flagExists;
	}
	
	/**
	 * Sets the legal representative.
	 *
	 * @param relatedCui the related cui
	 * @return the list
	 */
	@LoggerAuditWeb
	public List<LegalRepresentativeHistory> setLegalRepresentative(Long relatedCui){
		Holder holderAux;
		List<LegalRepresentativeHistory> lstLegalRepresentativeHistory = new ArrayList<LegalRepresentativeHistory>();
		try {
			holderAux = holderServiceFacade.getHolderServiceFacade(relatedCui);
			if(holderAux.getRepresentedEntity()!=null){
				for(int i=0;i<holderAux.getRepresentedEntity().size();i++){
					if(holderAux.getRepresentedEntity().get(i).getStateRepresented().equals(RepresentativeEntityStateType.REGISTERED.getCode())){
						LegalRepresentativeHistory legalRepresentativeHistory = new LegalRepresentativeHistory();
						legalRepresentativeHistory.setHolderRequest(holderRequest);
						legalRepresentativeHistory.setRepresentativeClass(holderAux.getRepresentedEntity().get(i).getRepresentativeClass());
						
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getBirthDate()!=null){
							legalRepresentativeHistory.setBirthDate(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getBirthDate());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentNumber()!=null){
							legalRepresentativeHistory.setDocumentNumber(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentNumber());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentType()!=null){
							legalRepresentativeHistory.setDocumentType(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentType());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getEconomicActivity()!=null){
							legalRepresentativeHistory.setEconomicActivity(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getEconomicActivity());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getEconomicSector()!=null){
							legalRepresentativeHistory.setEconomicSector(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getEconomicSector());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getEmail()!=null){
							legalRepresentativeHistory.setEmail(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getEmail());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getFaxNumber()!=null){
							legalRepresentativeHistory.setFaxNumber(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getFaxNumber());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getFirstLastName()!=null){
							legalRepresentativeHistory.setFirstLastName(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getFirstLastName());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getFullName()!=null){
							legalRepresentativeHistory.setFullName(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getFullName());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getHomePhoneNumber()!=null){
							legalRepresentativeHistory.setHomePhoneNumber(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getHomePhoneNumber());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getIndResidence()!=null){
							legalRepresentativeHistory.setIndResidence(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getIndResidence()); 
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyApp()!=null){
							legalRepresentativeHistory.setLastModifyApp(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyApp());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyDate()!=null){
							legalRepresentativeHistory.setLastModifyDate(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyDate());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyIp()!=null){
							legalRepresentativeHistory.setLastModifyIp(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyIp());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyUser()!=null){
							legalRepresentativeHistory.setLastModifyUser(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyUser());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getLegalAddress()!=null){
							legalRepresentativeHistory.setLegalAddress(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getLegalAddress());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getLegalDepartment()!=null){
							legalRepresentativeHistory.setLegalDepartment(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getLegalDepartment());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getLegalDistrict()!=null){
							legalRepresentativeHistory.setLegalDistrict(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getLegalDistrict());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getLegalProvince()!=null){
							legalRepresentativeHistory.setLegalProvince(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getLegalProvince());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getLegalResidenceCountry()!=null){
							legalRepresentativeHistory.setLegalResidenceCountry(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getLegalResidenceCountry());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getMobileNumber()!=null){
							legalRepresentativeHistory.setMobileNumber(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getMobileNumber());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getName()!=null){
							legalRepresentativeHistory.setName(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getName()); 
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getNationality()!=null){
							legalRepresentativeHistory.setNationality(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getNationality());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getOfficePhoneNumber()!=null){
							legalRepresentativeHistory.setOfficePhoneNumber(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getOfficePhoneNumber());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getPostalDepartment()!=null){
							legalRepresentativeHistory.setPostalDepartment(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getPostalDepartment());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getPostalDistrict()!=null){
							legalRepresentativeHistory.setPostalDistrict(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getPostalDistrict());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getPostalProvince()!=null){
							legalRepresentativeHistory.setPostalProvince(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getPostalProvince());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getPostalResidenceCountry()!=null){
							legalRepresentativeHistory.setPostalResidenceCountry(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getPostalResidenceCountry());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getPostalAddress()!=null){
							legalRepresentativeHistory.setPostalAddress(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getPostalAddress());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getSecondDocumentNumber()!=null){
							legalRepresentativeHistory.setSecondDocumentNumber(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getSecondDocumentNumber());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getSecondLastName()!=null){
							legalRepresentativeHistory.setSecondLastName(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getSecondLastName());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getSecondNationality()!=null){
							legalRepresentativeHistory.setSecondNationality(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getSecondNationality());	
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getSecondDocumentType()!=null){
							legalRepresentativeHistory.setSecondDocumentType(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getSecondDocumentType());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getSex()!=null){
							legalRepresentativeHistory.setSex(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getSex());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getState()!=null){
							legalRepresentativeHistory.setStateRepreHistory(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getState());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getPersonType()!=null){
							legalRepresentativeHistory.setPersonType(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getPersonType());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentSource()!=null){
							legalRepresentativeHistory.setDocumentSource(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentSource());
						}
						if(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentIssuanceDate()!=null){
							legalRepresentativeHistory.setDocumentIssuanceDate(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentIssuanceDate());
						}
						
						legalRepresentativeHelperBean.loadLegalRepresentativePepExists(legalRepresentativeHistory.getIdRepresentativeHistoryPk());
						legalRepresentativeHistory.setRegistryUser(userInfo.getUserAccountSession().getUserName());
						legalRepresentativeHistory.setStateRepreHistory(HolderRequestStateType.REGISTERED.getCode());
						legalRepresentativeHistory.setRepreHistoryType(HolderRequestType.CREATION.getCode());
						legalRepresentativeHistory.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
						legalRepresentativeHistory.setRepresentativeTypeDescription(PersonType.get(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getPersonType()).getValue());
						legalRepresentativeHistory.setDocumentTypeDescription(DocumentType.get(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentType()).getValue());
						RepresentativeFileHistory representantiveFileHistory;
						List<RepresentativeFileHistory> lstRepresentativeFileHistory = new ArrayList<RepresentativeFileHistory>();
						
						for(int j=0;j<holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getLegalRepresentativeFile().size();j++){
							representantiveFileHistory = new RepresentativeFileHistory();
							representantiveFileHistory.setLegalRepresentativeHistory(legalRepresentativeHistory);
							representantiveFileHistory.setFlagByLegalRepresentative(true);
							representantiveFileHistory.setDescription(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getLegalRepresentativeFile().get(j).getDescription());
							representantiveFileHistory.setDocumentType(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getLegalRepresentativeFile().get(j).getDocumentType());
							representantiveFileHistory.setFilename(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getLegalRepresentativeFile().get(j).getFilename());
							representantiveFileHistory.setRepresentFileHistType(HolderReqFileHisType.CREATION.getCode());
							representantiveFileHistory.setStateFile(HolderFileStateType.REGISTERED.getCode());
							representantiveFileHistory.setRegistryDate(CommonsUtilities.currentDate());
							representantiveFileHistory.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
							representantiveFileHistory.setRegistryUser(userInfo.getUserAccountSession().getUserName());
							representantiveFileHistory.setFileRepresentative(holderAux.getRepresentedEntity().get(i).getLegalRepresentative().getLegalRepresentativeFile().get(j).getFileLegalRepre());
							lstRepresentativeFileHistory.add(representantiveFileHistory);
						}
						legalRepresentativeHistory.setRepresenteFileHistory(lstRepresentativeFileHistory);
						lstLegalRepresentativeHistory.add(legalRepresentativeHistory);
					}
				}
			}
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lstLegalRepresentativeHistory;
	}
	
	/**
	 * Action do holder trust.
	 *
	 * @param option the option
	 */
	@LoggerAuditWeb
	public void actionDoHolderTrust(boolean option){
		disabledFideicomiso=false;
		try{
			if(option){
				
				indFICPATEconomicActivity = true;
				
				holderHistory.setHolderType(PersonType.JURIDIC.getCode());
				cleanFieldsEconomicActivity();
				cleanStylesEconomicActivity();
				setDisabledEconomicActivity(Boolean.TRUE,Boolean.TRUE,Boolean.TRUE);
				flagEconomicActivity = Boolean.TRUE;
				
				settingFlagTypePerson();
				settingJuridicClassTrust();
				
				holderHistory.setRelatedCui(holderResult.getIdHolderPk());
				holderHistory.setFundAdministrator(holderResult.getMnemonic());
//				holderHistory.setFundType(holderResult.getFundType());
				holderHistory.setTransferNumber(holderResult.getTransferNumber());
				holderHistory.setMnemonic(null);
				
				PepPerson pepPerson;
				if(holderHistory.getRelatedCui()!=null){
					pepPerson = holderServiceFacade.getPepPersonByIdHolderServiceFacade(holderHistory.getRelatedCui());
					if(pepPerson!=null){
						holderHistory.setBeginningPeriod(pepPerson.getBeginingPeriod());
						holderHistory.setEndingPeriod(pepPerson.getEndingPeriod());
						holderHistory.setCategory(pepPerson.getCategory());
						holderHistory.setRole(pepPerson.getRole());
						holderHistory.setIndPEP(BooleanType.YES.getCode());
						holderHistory.setIndicatorPep(true);
					}
					if(holderResult.getNationality()!=null){
						holderHistory.setNationality(holderResult.getNationality());
						holderHistory.setLegalResidenceCountry(holderResult.getLegalResidenceCountry());
						validateResidentHolder("frmHolderRegister:tabView:cmbNationality2");						
						holderHistory.setPostalResidenceCountry(holderResult.getPostalResidenceCountry());
					}
					if(holderResult.getDocumentType()!=null){
						holderHistory.setDocumentType(holderResult.getDocumentType());
						validateTypeDocumentHolder("frmHolderRegister:tabView:cmbDocumentType2");
					}
					if(holderResult.getDocumentNumber()!=null){
						holderHistory.setDocumentNumber(holderResult.getDocumentNumber());
					}
					
					holderHistory.setFullName(null);
					holderHistory.setEconomicSector(EconomicSectorType.FINANCIAL_PUBLIC_SECTOR.getCode());
					changeSelectedEconomicSector();
					holderHistory.setEconomicActivity(EconomicActivityType.OTROS_SERVICIOS_FINANCIEROS.getCode());
					buildInvestorTypeByEconAcSelected(EconomicActivityType.OTROS_SERVICIOS_FINANCIEROS.getCode());
					
					if(holderResult.getLegalDepartment()!=null){
						holderHistory.setLegalDepartment(holderResult.getLegalDepartment());
						holderHistory.setPostalDepartment(holderResult.getPostalDepartment());
					}
					if(holderResult.getLegalProvince()!=null){
						holderHistory.setLegalProvince(holderResult.getLegalProvince());
						holderHistory.setPostalProvince(holderResult.getPostalProvince());
					}
					if(holderResult.getLegalDistrict()!=null){
						holderHistory.setLegalDistrict(holderResult.getLegalDistrict());
						holderHistory.setPostalDistrict(holderResult.getPostalDistrict());
					}
					if(holderResult.getLegalAddress()!=null){
						holderHistory.setLegalAddress(holderResult.getLegalAddress());
						holderHistory.setPostalAddress(holderResult.getPostalAddress());
					}
					
					listDepartmentLocation = getLstDepartmentLocation(holderHistory.getLegalResidenceCountry());
					listProvinceLocation = getLstProvinceLocation(holderHistory.getLegalDepartment());
					listMunicipalityLocation = getLstMunicipalityLocation(holderHistory.getLegalProvince());			
					listDepartmentPostalLocation = getLstDepartmentLocation(holderHistory.getPostalResidenceCountry());
					listProvincePostalLocation = getLstProvinceLocation(holderHistory.getPostalDepartment());
					listMunicipalityPostalLocation = getLstMunicipalityLocation(holderHistory.getPostalProvince());
					
					legalRepresentativeHelperBean.initializeLstLegalRepresentativeHistory();
					legalRepresentativeHelperBean.getLegalRepresentativeTO().setViewOperationType(ViewOperationsType.REGISTER.getCode());
					
					List<LegalRepresentativeHistory> lstLegalRepresentativeHistory = setLegalRepresentative(holderHistory.getRelatedCui());
					
					legalRepresentativeHelperBean.setLstLegalRepresentativeHistory(lstLegalRepresentativeHistory);
					if(legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size()>0){
						legalRepresentativeHelperBean.setFlagModifyLegalRepresentantive(true);
					}
				}
				//se coloca en false por tratarse de un fideicomiso
				indFICPATEconomicActivity = false;
				disabledFideicomiso=true;
				JSFUtilities.resetViewRoot();
				JSFUtilities.executeJavascriptFunction("PF('cnfwHolderTrust').hide()");
			}
			else{
				cleanAll();
				indFICPATEconomicActivity = false;
				JSFUtilities.resetViewRoot();
				JSFUtilities.executeJavascriptFunction("PF('cnfwHolderTrust').hide()");
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Action do investment funds opened.
	 *
	 * @param option the option
	 */
	public void actionDoInvestmentFundsOpened(boolean option){
		try{
			if(option){
				indFICPATEconomicActivity = true;
				
				holderHistory.setHolderType(PersonType.JURIDIC.getCode());
				//probando para q se muestren los datos
				cleanFieldsEconomicActivity();
				cleanStylesEconomicActivity();
				setDisabledEconomicActivity(Boolean.TRUE,Boolean.TRUE,Boolean.FALSE);
				flagEconomicActivity = Boolean.TRUE;
				strStyleRequiredFundAdm = STYLE_REQUIRED;
				
				loadByHolderTypePerson();
				holderHistory.setRelatedCui(holderResult.getIdHolderPk());
				holderHistory.setFundAdministrator(holderResult.getMnemonic());
//				holderHistory.setFundType(holderResult.getFundType());
				holderHistory.setTransferNumber(holderResult.getTransferNumber());
				holderHistory.setMnemonic(null);
				
				PepPerson pepPerson;
				if(holderHistory.getRelatedCui()!=null){
					pepPerson = holderServiceFacade.getPepPersonByIdHolderServiceFacade(holderHistory.getRelatedCui());
					if(pepPerson!=null){
						holderHistory.setBeginningPeriod(pepPerson.getBeginingPeriod());
						holderHistory.setEndingPeriod(pepPerson.getEndingPeriod());
						holderHistory.setCategory(pepPerson.getCategory());
						holderHistory.setRole(pepPerson.getRole());
						holderHistory.setIndPEP(BooleanType.YES.getCode());
						holderHistory.setIndicatorPep(true);
					}
					if(holderResult.getNationality()!=null){
						holderHistory.setNationality(holderResult.getNationality());
						holderHistory.setLegalResidenceCountry(holderResult.getLegalResidenceCountry());
						validateResidentHolder("frmHolderRegister:tabView:cmbNationality2");						
						holderHistory.setPostalResidenceCountry(holderResult.getPostalResidenceCountry());
					}
					if(holderResult.getDocumentType()!=null){
						holderHistory.setDocumentType(holderResult.getDocumentType());
						validateTypeDocumentHolder("frmHolderRegister:tabView:cmbDocumentType2");
					}
					if(holderResult.getDocumentNumber()!=null){
						holderHistory.setDocumentNumber(holderResult.getDocumentNumber());
					}
					
					holderHistory.setFullName(null);
					holderHistory.setEconomicSector(holderResult.getEconomicSector());
					changeSelectedEconomicSector();
					holderHistory.setEconomicActivity(EconomicActivityType.FONDOS_INVERSION_ABIERTOS.getCode());
					
					if(holderResult.getLegalDepartment()!=null){
						holderHistory.setLegalDepartment(holderResult.getLegalDepartment());
						holderHistory.setPostalDepartment(holderResult.getPostalDepartment());
					}
					if(holderResult.getLegalProvince()!=null){
						holderHistory.setLegalProvince(holderResult.getLegalProvince());
						holderHistory.setPostalProvince(holderResult.getPostalProvince());
					}
					if(holderResult.getLegalDistrict()!=null){
						holderHistory.setLegalDistrict(holderResult.getLegalDistrict());
						holderHistory.setPostalDistrict(holderResult.getPostalDistrict());
					}
					if(holderResult.getLegalAddress()!=null){
						holderHistory.setLegalAddress(holderResult.getLegalAddress());
						holderHistory.setPostalAddress(holderResult.getPostalAddress());
					}
					
					listDepartmentLocation = getLstDepartmentLocation(holderHistory.getLegalResidenceCountry());
					listProvinceLocation = getLstProvinceLocation(holderHistory.getLegalDepartment());
					listMunicipalityLocation = getLstMunicipalityLocation(holderHistory.getLegalProvince());			
					listDepartmentPostalLocation = getLstDepartmentLocation(holderHistory.getPostalResidenceCountry());
					listProvincePostalLocation = getLstProvinceLocation(holderHistory.getPostalDepartment());
					listMunicipalityPostalLocation = getLstMunicipalityLocation(holderHistory.getPostalProvince());
					
					legalRepresentativeHelperBean.initializeLstLegalRepresentativeHistory();
					legalRepresentativeHelperBean.getLegalRepresentativeTO().setViewOperationType(ViewOperationsType.REGISTER.getCode());
					
					List<LegalRepresentativeHistory> lstLegalRepresentativeHistory = setLegalRepresentative(holderHistory.getRelatedCui());
					
					legalRepresentativeHelperBean.setLstLegalRepresentativeHistory(lstLegalRepresentativeHistory);
					if(legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size()>0){
						legalRepresentativeHelperBean.setFlagModifyLegalRepresentantive(true);
					}
				}
				JSFUtilities.resetViewRoot();
				JSFUtilities.executeJavascriptFunction("PF('cnfwHolderInvestmentFundsOpened').hide()");
			}
			else{
				cleanAll();
				indFICPATEconomicActivity = false;
				JSFUtilities.resetViewRoot();
				JSFUtilities.executeJavascriptFunction("PF('cnfwHolderInvestmentFundsOpened').hide()");
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void changeEconomicActivityInModify() {
		buildInvestorTypeByEconAcSelected(holderHistory.getEconomicActivity());
	}
	
	public void buildInvestorTypeByEconAcSelected(Integer economicActivity){
		buildInvestorTypeByEconAcSelected(economicActivity, Boolean.FALSE);
	}
	
	public void buildInvestorTypeByEconAcSelected(Integer economicActivity, Boolean isFromModification){
		// construyendo la lista de tipo de inversionista
		InvestorTypeModelTO it = new InvestorTypeModelTO();
		it.setEconomicActivity(economicActivity);
		try {
			listInvestorType = investorTypeFacade.getLstInvestorTypeModelTO(it);
			
			// sugerir el tipo de inversionista por defecto
			if(Validations.validateIsNotNull(holderHistory) && !isFromModification){
				holderHistory.setInvestorType(getInvestorTypeByDefault(economicActivity));
				if(Validations.validateIsNotNullAndPositive(holderHistory.getJuridicClass())){
					if(holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode()) ){
						// forzamos a que el tipo de inv sea FIDEICOMISO 
						holderHistory.setInvestorType(2584);
					}
				}
			}
		} catch (ServiceException e) {
			e.printStackTrace();
			listInvestorType = new ArrayList<>();
		}
	}
	private Integer getInvestorTypeByDefault(Integer economicActivity){
		try {
			if(Validations.validateIsNotNullAndPositive(economicActivity)){
				List<ParameterTable> lstEconomicActivityWithInvetorType = investorTypeFacade.getLstEconomicActivityToRelationInvestortype();
				for(ParameterTable pt:lstEconomicActivityWithInvetorType){
					if( Validations.validateIsNotNull(pt) 
						&& Validations.validateIsNotNullAndPositive(pt.getParameterTablePk())
						&& pt.getParameterTablePk().equals(economicActivity) ){
						return pt.getShortInteger();
					}
				}
			}
		} catch (Exception e) {
			System.out.println("::: Error al sugerir el tipo de inversionista: (error irrelevante) "+e.getMessage());
		}
		return null;
	}

	@LoggerAuditWeb
	public void showFieldsEconomicActivity() {
		this.showFieldsEconomicActivity(Boolean.FALSE);
	}
	
	/**
	 * Show fields economic activity.
	 */
	public void showFieldsEconomicActivity(Boolean isFromModification){

		// Validar la transferencia Extrabursatil
		validateIndCanTransfer(holderHistory);
				
		if(Validations.validateIsNotNullAndNotEmpty(participant) &&
				Validations.validateIsNotNullAndNotEmpty(participant.getIdParticipantPk()) &&
				Validations.validateIsNotNullAndNotEmpty(holderHistory.getHolderType()) &&
				Validations.validateIsNotNullAndNotEmpty(holderHistory.getJuridicClass()) &&
				Validations.validateIsNotNullAndNotEmpty(holderHistory.getEconomicSector()) &&
				Validations.validateIsNotNullAndNotEmpty(holderHistory.getEconomicActivity())){
			
			// construyendo la lista de tipo de inversionista
			buildInvestorTypeByEconAcSelected(holderHistory.getEconomicActivity(), isFromModification);
			
			if(holderHistory.getEconomicActivity().equals(EconomicActivityType.FONDOS_INVERSION_ABIERTOS.getCode())){
				if(!validateHolderInvestmentFundsOpened(holderHistory.getDocumentNumber(),holderHistory.getDocumentSource(),holderHistory.getDocumentType(),holderHistory.getNationality())){
					Object[] bodyData = new Object[0];
					holderHistory.setEconomicActivity(null);
					JSFUtilities.showMessageOnDialog(GeneralConstants.LBL_HEADER_ALERT, PropertiesUtilities.getMessage(
							PropertiesConstants.ERROR_ECONOMIC_ACTIVITY_FIA,bodyData));
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
			}
			if(holderHistory.getHolderType().equals(PersonType.JURIDIC.getCode())
					&& holderHistory.getJuridicClass().equals(JuridicClassType.NORMAL.getCode())){
//				cleanFieldsEconomicActivity();
				cleanStylesEconomicActivity();
				if(holderHistory.getEconomicActivity().equals(EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode())){
					setDisabledEconomicActivity(Boolean.TRUE,Boolean.FALSE,Boolean.TRUE);
					flagEconomicActivity = Boolean.TRUE;
					strStyleRequiredFundType = STYLE_REQUIRED;
				}else if(holderHistory.getEconomicActivity().equals(EconomicActivityType.COMPANIAS_SEGURO.getCode())){
					setDisabledEconomicActivity(Boolean.FALSE,Boolean.TRUE,Boolean.TRUE);
					flagEconomicActivity = Boolean.TRUE;
					strStyleRequiredTransferNro = STYLE_REQUIRED;	
				}else if(holderHistory.getEconomicActivity().equals(EconomicActivityType.AGENCIAS_BOLSA.getCode())){
					setDisabledEconomicActivity(Boolean.TRUE,Boolean.FALSE,Boolean.TRUE);
					flagEconomicActivity = Boolean.TRUE;
					strStyleRequiredFundType = STYLE_REQUIRED;
				}else{
					flagEconomicActivity = Boolean.FALSE;
//					cleanFieldsEconomicActivity();
					cleanStylesEconomicActivity();
				}
			}else{
				flagEconomicActivity = Boolean.FALSE;
				participantAFP_FIC_PAT = Boolean.TRUE;
				//cleanFieldsEconomicActivity();
			}
		}else{
			flagEconomicActivity = Boolean.FALSE;
//			cleanFieldsEconomicActivity();
		}
	}
	
	/**
	 * Sets the disabled economic activity.
	 *
	 * @param flagTransfer the flag transfer
	 * @param flagFundType the flag fund type
	 * @param flagFundAdm the flag fund adm
	 */
	public void setDisabledEconomicActivity(Boolean flagTransfer,Boolean flagFundType,Boolean flagFundAdm){
		disabledTransferNumber = flagTransfer;
		disabledFundType = flagFundType;
		disabledFundAdministrator = flagFundAdm;
	}
	
	/**
	 * Clean fields economic activity.
	 */
	public void cleanFieldsEconomicActivity(){
		holderHistory.setFundAdministrator(null);
//		holderHistory.setFundType(null);
		holderHistory.setMnemonic(null);
		holderHistory.setTransferNumber(null);
	}
	
	/**
	 * Clean styles economic activity.
	 */
	public void cleanStylesEconomicActivity(){
		strStyleRequiredCUI = GeneralConstants.EMPTY_STRING;
		strStyleRequiredFundAdm = GeneralConstants.EMPTY_STRING;
		strStyleRequiredFundType = GeneralConstants.EMPTY_STRING;
		strStyleRequiredTransferNro = GeneralConstants.EMPTY_STRING;
	}
	
	/**
	 * Validate Ind Cant Transfer
	 * 
	 */
	public void validateIndCanTransfer (HolderHistory holderHistory){
		
		//ISSUE 770
		
		if(holderHistory.getStateHolderHistory()== null){
		
			//en cualquier caso la decision es SI 1
			holderHistory.setIndCanNegotiate(1);
			
			//validando que la actividad economica no sea nula y que sea un nuevo registro de un cui (estado null)
			if (holderHistory.getEconomicActivity() != null){
				//exceptuando las actividades economicas:
				
				//AGENCIAS DE BOLSA
				if(holderHistory.getEconomicActivity().intValue() == EconomicActivityType.AGENCIAS_BOLSA.getCode()){
					holderHistory.setIndCanNegotiate(0);
				}
				
				//COMPANIAS DE SEGUROS
				if(holderHistory.getEconomicActivity().intValue() == EconomicActivityType.COMPANIAS_SEGURO.getCode()){
					holderHistory.setIndCanNegotiate(0);
				}
				
				//FONDOS DE INVERSION ABIERTOS
				if(holderHistory.getEconomicActivity().intValue() == EconomicActivityType.FONDOS_INVERSION_ABIERTOS.getCode()){
					holderHistory.setIndCanNegotiate(0);
				}
				
				//FONDOS DE INVERSION CERRADOS
				if(holderHistory.getEconomicActivity().intValue() == EconomicActivityType.FONDOS_INVERSION_CERRADOS.getCode()){
					holderHistory.setIndCanNegotiate(0);
				}
				
				//FONDOS DE INVERSION MIXTOS
				if(holderHistory.getEconomicActivity().intValue() == EconomicActivityType.FONDOS_INVERSION_MIXTOS.getCode()){
					holderHistory.setIndCanNegotiate(0);
				}
				
				//SOCIEDADES ADMINISTRADORAS DE FONDOS DE INVERSION
				if(holderHistory.getEconomicActivity().intValue() == EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode()){
					holderHistory.setIndCanNegotiate(0);
				}
			}
		}
	}
	
	/**
	 * Event document source.
	 */
	public void eventDocumentSource(){
		holderHistory.setDocumentNumber(null);
	}
	
	/**
	 * Gets the holder creation history.
	 * 
	 * @return the holder creation history
	 */
	public HolderHistory getHolderCreationHistory() {
		return holderHistory;
	}

	/**
	 * Sets the holder creation history.
	 * 
	 * @param holderCreationHistory
	 *            the new holder creation history
	 */
	public void setHolderCreationHistory(HolderHistory holderCreationHistory) {
		this.holderHistory = holderCreationHistory;
	}

	/**
	 * Checks if is flag is pep.
	 * 
	 * @return true, if is flag is pep
	 */
	public boolean isFlagIsPEP() {
		return flagIsPEP;
	}

	/**
	 * Sets the flag is pep.
	 * 
	 * @param flagIsPEP
	 *            the new flag is pep
	 */
	public void setFlagIsPEP(boolean flagIsPEP) {
		this.flagIsPEP = flagIsPEP;
	}

	/**
	 * Checks if is flag copy legal holder.
	 * 
	 * @return true, if is flag copy legal holder
	 */
	public boolean isFlagCopyLegalHolder() {
		return flagCopyLegalHolder;
	}

	/**
	 * Sets the flag copy legal holder.
	 * 
	 * @param flagCopyLegalHolder
	 *            the new flag copy legal holder
	 */
	public void setFlagCopyLegalHolder(boolean flagCopyLegalHolder) {
		this.flagCopyLegalHolder = flagCopyLegalHolder;
	}

	/**
	 * Checks if is flag holder natural person.
	 * 
	 * @return true, if is flag holder natural person
	 */
	public boolean isFlagHolderNaturalPerson() {
		return flagHolderNaturalPerson;
	}

	/**
	 * Sets the flag holder natural person.
	 * 
	 * @param flagHolderNaturalPerson
	 *            the new flag holder natural person
	 */
	public void setFlagHolderNaturalPerson(boolean flagHolderNaturalPerson) {
		this.flagHolderNaturalPerson = flagHolderNaturalPerson;
	}

	/**
	 * Checks if is flag holder juridic person.
	 * 
	 * @return true, if is flag holder juridic person
	 */
	public boolean isFlagHolderJuridicPerson() {
		return flagHolderJuridicPerson;
	}

	/**
	 * Sets the flag holder juridic person.
	 * 
	 * @param flagHolderJuridicPerson
	 *            the new flag holder juridic person
	 */
	public void setFlagHolderJuridicPerson(boolean flagHolderJuridicPerson) {
		this.flagHolderJuridicPerson = flagHolderJuridicPerson;
	}

	/**
	 * Checks if is flag representative natural person.
	 * 
	 * @return true, if is flag representative natural person
	 */
	public boolean isFlagRepresentativeNaturalPerson() {
		return flagRepresentativeNaturalPerson;
	}

	/**
	 * Sets the flag representative natural person.
	 * 
	 * @param flagRepresentativeNaturalPerson
	 *            the new flag representative natural person
	 */
	public void setFlagRepresentativeNaturalPerson(
			boolean flagRepresentativeNaturalPerson) {
		this.flagRepresentativeNaturalPerson = flagRepresentativeNaturalPerson;
	}

	/**
	 * Checks if is flag representative juridic person.
	 * 
	 * @return true, if is flag representative juridic person
	 */
	public boolean isFlagRepresentativeJuridicPerson() {
		return flagRepresentativeJuridicPerson;
	}

	/**
	 * Sets the flag representative juridic person.
	 * 
	 * @param flagRepresentativeJuridicPerson
	 *            the new flag representative juridic person
	 */
	public void setFlagRepresentativeJuridicPerson(
			boolean flagRepresentativeJuridicPerson) {
		this.flagRepresentativeJuridicPerson = flagRepresentativeJuridicPerson;
	}

	/**
	 * Checks if is disabled country resident holder.
	 * 
	 * @return true, if is disabled country resident holder
	 */
	public boolean isDisabledCountryResidentHolder() {
		return disabledCountryResidentHolder;
	}

	/**
	 * Sets the disabled country resident holder.
	 * 
	 * @param disabledCountryResidentHolder
	 *            the new disabled country resident holder
	 */
	public void setDisabledCountryResidentHolder(
			boolean disabledCountryResidentHolder) {
		this.disabledCountryResidentHolder = disabledCountryResidentHolder;
	}

	/**
	 * Checks if is disabled country resident representative.
	 * 
	 * @return true, if is disabled country resident representative
	 */
	public boolean isDisabledCountryResidentRepresentative() {
		return disabledCountryResidentRepresentative;
	}

	/**
	 * Sets the disabled country resident representative.
	 * 
	 * @param disabledCountryResidentRepresentative
	 *            the new disabled country resident representative
	 */
	public void setDisabledCountryResidentRepresentative(
			boolean disabledCountryResidentRepresentative) {
		this.disabledCountryResidentRepresentative = disabledCountryResidentRepresentative;
	}

	/**
	 * Checks if is flag copy legal representative.
	 * 
	 * @return true, if is flag copy legal representative
	 */
	public boolean isFlagCopyLegalRepresentative() {
		return flagCopyLegalRepresentative;
	}

	/**
	 * Sets the flag copy legal representative.
	 * 
	 * @param flagCopyLegalRepresentative
	 *            the new flag copy legal representative
	 */
	public void setFlagCopyLegalRepresentative(
			boolean flagCopyLegalRepresentative) {
		this.flagCopyLegalRepresentative = flagCopyLegalRepresentative;
	}

	/**
	 * Gets the list juridic class.
	 * 
	 * @return the list juridic class
	 */
	public List<ParameterTable> getListJuridicClass() {
		return listJuridicClass;
	}

	/**
	 * Sets the list juridic class.
	 * 
	 * @param listJuridicClass
	 *            the new list juridic class
	 */
	public void setListJuridicClass(List<ParameterTable> listJuridicClass) {
		this.listJuridicClass = listJuridicClass;
	}

	/**
	 * Gets the list person type.
	 * 
	 * @return the list person type
	 */
	public List<ParameterTable> getListPersonType() {
		return listPersonType;
	}

	/**
	 * Sets the list person type.
	 * 
	 * @param listPersonType
	 *            the new list person type
	 */
	public void setListPersonType(List<ParameterTable> listPersonType) {
		this.listPersonType = listPersonType;
	}

	/**
	 * Gets the list boolean.
	 * 
	 * @return the list boolean
	 */
	public List<BooleanType> getListBoolean() {
		return listBoolean;
	}

	/**
	 * Sets the list boolean.
	 * 
	 * @param listBoolean
	 *            the new list boolean
	 */
	public void setListBoolean(List<BooleanType> listBoolean) {
		this.listBoolean = listBoolean;
	}

	/**
	 * Gets the list document type.
	 * 
	 * @return the list document type
	 */
	public List<ParameterTable> getListDocumentType() {
		return listDocumentType;
	}

	/**
	 * Sets the list document type.
	 * 
	 * @param listDocumentType
	 *            the new list document type
	 */
	public void setListDocumentType(List<ParameterTable> listDocumentType) {
		this.listDocumentType = listDocumentType;
	}

	/**
	 * Gets the list sex.
	 * 
	 * @return the list sex
	 */
	public List<ParameterTable> getListSex() {
		return listSex;
	}

	/**
	 * Sets the list sex.
	 * 
	 * @param listSex
	 *            the new list sex
	 */
	public void setListSex(List<ParameterTable> listSex) {
		this.listSex = listSex;
	}

	
	/**
	 * Gets the list geographic location.
	 * 
	 * @return the list geographic location
	 */
	public List<GeographicLocation> getListGeographicLocation() {
		return listGeographicLocation;
	}

	/**
	 * Sets the list geographic location.
	 * 
	 * @param listGeographicLocation
	 *            the new list geographic location
	 */
	public void setListGeographicLocation(
			List<GeographicLocation> listGeographicLocation) {
		this.listGeographicLocation = listGeographicLocation;
	}

	/**
	 * Checks if is disabled representative button.
	 * 
	 * @return true, if is disabled representative button
	 */
	public boolean isDisabledRepresentativeButton() {
		return disabledRepresentativeButton;
	}

	/**
	 * Sets the disabled representative button.
	 * 
	 * @param disabledRepresentativeButton
	 *            the new disabled representative button
	 */
	public void setDisabledRepresentativeButton(
			boolean disabledRepresentativeButton) {
		this.disabledRepresentativeButton = disabledRepresentativeButton;
	}

	/**
	 * Gets the list province location.
	 * 
	 * @return the list province location
	 */
	public List<GeographicLocation> getListProvinceLocation() {
		return listProvinceLocation;
	}

	/**
	 * Sets the list province location.
	 * 
	 * @param listProvinceLocation
	 *            the new list province location
	 */
	public void setListProvinceLocation(
			List<GeographicLocation> listProvinceLocation) {
		this.listProvinceLocation = listProvinceLocation;
	}

	/**
	 * Gets the list sector location.
	 * 
	 * @return the list sector location
	 */
	public List<GeographicLocation> getListMunicipalityLocation() {
		return listMunicipalityLocation;
	}

	/**
	 * Sets the list sector location.
	 *
	 * @param listMunicipalityLocation the new list municipality location
	 */
	public void setListMunicipalityLocation(
			List<GeographicLocation> listMunicipalityLocation) {
		this.listMunicipalityLocation = listMunicipalityLocation;
	}

	/**
	 * Gets the list province postal location.
	 * 
	 * @return the list province postal location
	 */
	public List<GeographicLocation> getListProvincePostalLocation() {
		return listProvincePostalLocation;
	}

	/**
	 * Sets the list province postal location.
	 * 
	 * @param listProvincePostalLocation
	 *            the new list province postal location
	 */
	public void setListProvincePostalLocation(
			List<GeographicLocation> listProvincePostalLocation) {
		this.listProvincePostalLocation = listProvincePostalLocation;
	}

	/**
	 * Gets the list sector postal location.
	 * 
	 * @return the list sector postal location
	 */
	public List<GeographicLocation> getListMunicipalityPostalLocation() {
		return listMunicipalityPostalLocation;
	}

	/**
	 * Sets the list sector postal location.
	 *
	 * @param listMunicipalityPostalLocation the new list municipality postal location
	 */
	public void setListMunicipalityPostalLocation(
			List<GeographicLocation> listMunicipalityPostalLocation) {
		this.listMunicipalityPostalLocation = listMunicipalityPostalLocation;
	}

	/**
	 * Checks if is disabled cmb postal country.
	 * 
	 * @return true, if is disabled cmb postal country
	 */
	public boolean isDisabledCmbPostalCountry() {
		return disabledCmbPostalCountry;
	}

	/**
	 * Sets the disabled cmb postal country.
	 * 
	 * @param disabledCmbPostalCountry
	 *            the new disabled cmb postal country
	 */
	public void setDisabledCmbPostalCountry(boolean disabledCmbPostalCountry) {
		this.disabledCmbPostalCountry = disabledCmbPostalCountry;
	}

	/**
	 * Checks if is disabled cmb postal province.
	 * 
	 * @return true, if is disabled cmb postal province
	 */
	public boolean isDisabledCmbPostalProvince() {
		return disabledCmbPostalProvince;
	}

	/**
	 * Sets the disabled cmb postal province.
	 * 
	 * @param disabledCmbPostalProvince
	 *            the new disabled cmb postal province
	 */
	public void setDisabledCmbPostalProvince(boolean disabledCmbPostalProvince) {
		this.disabledCmbPostalProvince = disabledCmbPostalProvince;
	}

	/**
	 * Checks if is disabled cmb postal sector.
	 * 
	 * @return true, if is disabled cmb postal sector
	 */
	public boolean isDisabledCmbPostalMunicipality() {
		return disabledCmbPostalMunicipality;
	}

	/**
	 * Sets the disabled cmb postal sector.
	 *
	 * @param disabledCmbPostalMunicipality the new disabled cmb postal municipality
	 */
	public void setDisabledCmbPostalMunicipality(boolean disabledCmbPostalMunicipality) {
		this.disabledCmbPostalMunicipality = disabledCmbPostalMunicipality;
	}

	/**
	 * Checks if is disabled input postal address.
	 * 
	 * @return true, if is disabled input postal address
	 */
	public boolean isDisabledInputPostalAddress() {
		return disabledInputPostalAddress;
	}

	/**
	 * Sets the disabled input postal address.
	 * 
	 * @param disabledInputPostalAddress
	 *            the new disabled input postal address
	 */
	public void setDisabledInputPostalAddress(boolean disabledInputPostalAddress) {
		this.disabledInputPostalAddress = disabledInputPostalAddress;
	}

	/**
	 * Gets the list representative class.
	 * 
	 * @return the list representative class
	 */
	public List<ParameterTable> getListRepresentativeClass() {
		return listRepresentativeClass;
	}

	/**
	 * Sets the list representative class.
	 * 
	 * @param listRepresentativeClass
	 *            the new list representative class
	 */
	public void setListRepresentativeClass(
			List<ParameterTable> listRepresentativeClass) {
		this.listRepresentativeClass = listRepresentativeClass;
	}

	
	/**
	 * Gets the holder history.
	 * 
	 * @return the holder history
	 */
	public HolderHistory getHolderHistory() {
		return holderHistory;
	}

	/**
	 * Sets the holder history.
	 * 
	 * @param holderHistory
	 *            the new holder history
	 */
	public void setHolderHistory(HolderHistory holderHistory) {
		this.holderHistory = holderHistory;
	}

	/**
	 * Checks if is flag representative is pep.
	 * 
	 * @return true, if is flag representative is pep
	 */
	public boolean isFlagRepresentativeIsPEP() {
		return flagRepresentativeIsPEP;
	}

	/**
	 * Sets the flag representative is pep.
	 * 
	 * @param flagRepresentativeIsPEP
	 *            the new flag representative is pep
	 */
	public void setFlagRepresentativeIsPEP(boolean flagRepresentativeIsPEP) {
		this.flagRepresentativeIsPEP = flagRepresentativeIsPEP;
	}

	/**
	 * Gets the lst parameter archive header.
	 * 
	 * @return the lst parameter archive header
	 */
	public List<ParameterTable> getLstParameterArchiveHeader() {
		return lstParameterArchiveHeader;
	}

	/**
	 * Sets the lst parameter archive header.
	 * 
	 * @param lstParameterArchiveHeader
	 *            the new lst parameter archive header
	 */
	public void setLstParameterArchiveHeader(
			List<ParameterTable> lstParameterArchiveHeader) {
		this.lstParameterArchiveHeader = lstParameterArchiveHeader;
	}

	/**
	 * Gets the lst holder req file history.
	 * 
	 * @return the lst holder req file history
	 */
	public List<HolderReqFileHistory> getLstHolderReqFileHistory() {
		return lstHolderReqFileHistory;
	}

	/**
	 * Sets the lst holder req file history.
	 * 
	 * @param lstHolderReqFileHistory
	 *            the new lst holder req file history
	 */
	public void setLstHolderReqFileHistory(
			List<HolderReqFileHistory> lstHolderReqFileHistory) {
		this.lstHolderReqFileHistory = lstHolderReqFileHistory;
	}

	/**
	 * Checks if is flag cmb representative class.
	 * 
	 * @return true, if is flag cmb representative class
	 */
	public boolean isFlagCmbRepresentativeClass() {
		return flagCmbRepresentativeClass;
	}

	/**
	 * Sets the flag cmb representative class.
	 * 
	 * @param flagCmbRepresentativeClass
	 *            the new flag cmb representative class
	 */
	public void setFlagCmbRepresentativeClass(boolean flagCmbRepresentativeClass) {
		this.flagCmbRepresentativeClass = flagCmbRepresentativeClass;
	}

	/**
	 * Gets the file.
	 * 
	 * @return the file
	 */
	public UploadedFile getFile() {
		return file;
	}

	/**
	 * Sets the file.
	 * 
	 * @param file
	 *            the new file
	 */
	public void setFile(UploadedFile file) {
		this.file = file;
	}

	
	/**
	 * Gets the participant.
	 * 
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 * 
	 * @param participant
	 *            the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * Gets the list participant.
	 * 
	 * @return the list participant
	 */
	public List<Participant> getListParticipant() {
		return listParticipant;
	}

	/**
	 * Sets the list participant.
	 * 
	 * @param listParticipant
	 *            the new list participant
	 */
	public void setListParticipant(List<Participant> listParticipant) {
		this.listParticipant = listParticipant;
	}

	/**
	 * Gets the holder request selected.
	 * 
	 * @return the holder request selected
	 */
	public HolderRequest getHolderRequestSelected() {
		return holderRequestSelected;
	}

	/**
	 * Sets the holder request selected.
	 * 
	 * @param holderRequestSelected
	 *            the new holder request selected
	 */
	public void setHolderRequestSelected(HolderRequest holderRequestSelected) {
		this.holderRequestSelected = holderRequestSelected;
	}

	/**
	 * Checks if is flag is participant.
	 * 
	 * @return true, if is flag is participant
	 */
	public boolean isFlagIsParticipant() {
		return flagIsParticipant;
	}

	/**
	 * Sets the flag is participant.
	 * 
	 * @param flagIsParticipant
	 *            the new flag is participant
	 */
	public void setFlagIsParticipant(boolean flagIsParticipant) {
		this.flagIsParticipant = flagIsParticipant;
	}

	/**
	 * Checks if is flag action confirm.
	 * 
	 * @return true, if is flag action confirm
	 */
	public boolean isFlagActionConfirm() {
		return flagActionConfirm;
	}

	/**
	 * Sets the flag action confirm.
	 * 
	 * @param flagActionConfirm
	 *            the new flag action confirm
	 */
	public void setFlagActionConfirm(boolean flagActionConfirm) {
		this.flagActionConfirm = flagActionConfirm;
	}

	/**
	 * Checks if is flag action reject.
	 * 
	 * @return true, if is flag action reject
	 */
	public boolean isFlagActionReject() {
		return flagActionReject;
	}

	/**
	 * Sets the flag action reject.
	 * 
	 * @param flagActionReject
	 *            the new flag action reject
	 */
	public void setFlagActionReject(boolean flagActionReject) {
		this.flagActionReject = flagActionReject;
	}

	/**
	 * Checks if is flag action annular.
	 * 
	 * @return true, if is flag action annular
	 */
	public boolean isFlagActionAnnular() {
		return flagActionAnnular;
	}

	/**
	 * Sets the flag action annular.
	 * 
	 * @param flagActionAnnular
	 *            the new flag action annular
	 */
	public void setFlagActionAnnular(boolean flagActionAnnular) {
		this.flagActionAnnular = flagActionAnnular;
	}

	/**
	 * Checks if is disabled cmb participant.
	 * 
	 * @return true, if is disabled cmb participant
	 */
	public boolean isDisabledCmbParticipant() {
		return disabledCmbParticipant;
	}

	/**
	 * Sets the disabled cmb participant.
	 * 
	 * @param disabledCmbParticipant
	 *            the new disabled cmb participant
	 */
	public void setDisabledCmbParticipant(boolean disabledCmbParticipant) {
		this.disabledCmbParticipant = disabledCmbParticipant;
	}

	
	/**
	 * Checks if is show motive text.
	 * 
	 * @return true, if is show motive text
	 */
	public boolean isShowMotiveText() {
		return showMotiveText;
	}

	/**
	 * Sets the show motive text.
	 * 
	 * @param showMotiveText
	 *            the new show motive text
	 */
	public void setShowMotiveText(boolean showMotiveText) {
		this.showMotiveText = showMotiveText;
	}

	/**
	 * Gets the request motive.
	 * 
	 * @return the request motive
	 */
	public Integer getRequestMotive() {
		return requestMotive;
	}

	/**
	 * Sets the request motive.
	 * 
	 * @param requestMotive
	 *            the new request motive
	 */
	public void setRequestMotive(Integer requestMotive) {
		this.requestMotive = requestMotive;
	}

	/**
	 * Gets the request other motive.
	 * 
	 * @return the request other motive
	 */
	public String getRequestOtherMotive() {
		return requestOtherMotive;
	}

	/**
	 * Sets the request other motive.
	 * 
	 * @param requestOtherMotive
	 *            the new request other motive
	 */
	public void setRequestOtherMotive(String requestOtherMotive) {
		this.requestOtherMotive = requestOtherMotive;
	}

	/**
	 * Gets the list motives.
	 *
	 * @return the list motives
	 */
	public List<ParameterTable> getListMotives() {
		return listMotives;
	}

	/**
	 * Sets the list motives.
	 *
	 * @param listMotives the new list motives
	 */
	public void setListMotives(List<ParameterTable> listMotives) {
		this.listMotives = listMotives;
	}

	/**
	 * Gets the list state holder request.
	 *
	 * @return the list state holder request
	 */
	public List<ParameterTable> getListStateHolderRequest() {
		return listStateHolderRequest;
	}

	/**
	 * Sets the list state holder request.
	 *
	 * @param listStateHolderRequest the new list state holder request
	 */
	public void setListStateHolderRequest(
			List<ParameterTable> listStateHolderRequest) {
		this.listStateHolderRequest = listStateHolderRequest;
	}

	/**
	 * Checks if is numeric type document holder.
	 *
	 * @return true, if is numeric type document holder
	 */
	public boolean isNumericTypeDocumentHolder() {
		return numericTypeDocumentHolder;
	}

	/**
	 * Sets the numeric type document holder.
	 *
	 * @param numericTypeDocumentHolder the new numeric type document holder
	 */
	public void setNumericTypeDocumentHolder(boolean numericTypeDocumentHolder) {
		this.numericTypeDocumentHolder = numericTypeDocumentHolder;
	}

	/**
	 * Checks if is numeric second type document holder.
	 *
	 * @return true, if is numeric second type document holder
	 */
	public boolean isNumericSecondTypeDocumentHolder() {
		return numericSecondTypeDocumentHolder;
	}

	/**
	 * Sets the numeric second type document holder.
	 *
	 * @param numericSecondTypeDocumentHolder the new numeric second type document holder
	 */
	public void setNumericSecondTypeDocumentHolder(
			boolean numericSecondTypeDocumentHolder) {
		this.numericSecondTypeDocumentHolder = numericSecondTypeDocumentHolder;
	}

	/**
	 * Checks if is numeric type document legal.
	 *
	 * @return true, if is numeric type document legal
	 */
	public boolean isNumericTypeDocumentLegal() {
		return numericTypeDocumentLegal;
	}

	/**
	 * Sets the numeric type document legal.
	 *
	 * @param numericTypeDocumentLegal the new numeric type document legal
	 */
	public void setNumericTypeDocumentLegal(boolean numericTypeDocumentLegal) {
		this.numericTypeDocumentLegal = numericTypeDocumentLegal;
	}

	/**
	 * Checks if is numeric second type document legal.
	 *
	 * @return true, if is numeric second type document legal
	 */
	public boolean isNumericSecondTypeDocumentLegal() {
		return numericSecondTypeDocumentLegal;
	}

	/**
	 * Sets the numeric second type document legal.
	 *
	 * @param numericSecondTypeDocumentLegal the new numeric second type document legal
	 */
	public void setNumericSecondTypeDocumentLegal(
			boolean numericSecondTypeDocumentLegal) {
		this.numericSecondTypeDocumentLegal = numericSecondTypeDocumentLegal;
	}

	/**
	 * Gets the max lenght document number holder.
	 *
	 * @return the max lenght document number holder
	 */
	public Integer getMaxLenghtDocumentNumberHolder() {
		return maxLenghtDocumentNumberHolder;
	}

	/**
	 * Sets the max lenght document number holder.
	 *
	 * @param maxLenghtDocumentNumberHolder the new max lenght document number holder
	 */
	public void setMaxLenghtDocumentNumberHolder(
			Integer maxLenghtDocumentNumberHolder) {
		this.maxLenghtDocumentNumberHolder = maxLenghtDocumentNumberHolder;
	}

	/**
	 * Gets the max lenght document number legal.
	 *
	 * @return the max lenght document number legal
	 */
	public Integer getMaxLenghtDocumentNumberLegal() {
		return maxLenghtDocumentNumberLegal;
	}

	/**
	 * Sets the max lenght document number legal.
	 *
	 * @param maxLenghtDocumentNumberLegal the new max lenght document number legal
	 */
	public void setMaxLenghtDocumentNumberLegal(
			Integer maxLenghtDocumentNumberLegal) {
		this.maxLenghtDocumentNumberLegal = maxLenghtDocumentNumberLegal;
	}

	/**
	 * Gets the max lenght second document number holder.
	 *
	 * @return the max lenght second document number holder
	 */
	public Integer getMaxLenghtSecondDocumentNumberHolder() {
		return maxLenghtSecondDocumentNumberHolder;
	}

	/**
	 * Sets the max lenght second document number holder.
	 *
	 * @param maxLenghtSecondDocumentNumberHolder the new max lenght second document number holder
	 */
	public void setMaxLenghtSecondDocumentNumberHolder(
			Integer maxLenghtSecondDocumentNumberHolder) {
		this.maxLenghtSecondDocumentNumberHolder = maxLenghtSecondDocumentNumberHolder;
	}

	/**
	 * Gets the max lenght second document number legal.
	 *
	 * @return the max lenght second document number legal
	 */
	public Integer getMaxLenghtSecondDocumentNumberLegal() {
		return maxLenghtSecondDocumentNumberLegal;
	}

	/**
	 * Sets the max lenght second document number legal.
	 *
	 * @param maxLenghtSecondDocumentNumberLegal the new max lenght second document number legal
	 */
	public void setMaxLenghtSecondDocumentNumberLegal(
			Integer maxLenghtSecondDocumentNumberLegal) {
		this.maxLenghtSecondDocumentNumberLegal = maxLenghtSecondDocumentNumberLegal;
	}

	/**
	 * Checks if is flag holder indicator inability.
	 *
	 * @return true, if is flag holder indicator inability
	 */
	public boolean isFlagHolderIndicatorInability() {
		return flagHolderIndicatorInability;
	}

	/**
	 * Sets the flag holder indicator inability.
	 *
	 * @param flagHolderIndicatorInability the new flag holder indicator inability
	 */
	public void setFlagHolderIndicatorInability(
			boolean flagHolderIndicatorInability) {
		this.flagHolderIndicatorInability = flagHolderIndicatorInability;
	}

	/**
	 * Checks if is flag indicator minor holder.
	 *
	 * @return true, if is flag indicator minor holder
	 */
	public boolean isFlagIndicatorMinorHolder() {
		return flagIndicatorMinorHolder;
	}

	/**
	 * Sets the flag indicator minor holder.
	 *
	 * @param flagIndicatorMinorHolder the new flag indicator minor holder
	 */
	public void setFlagIndicatorMinorHolder(boolean flagIndicatorMinorHolder) {
		this.flagIndicatorMinorHolder = flagIndicatorMinorHolder;
	}

	
	/**
	 * Checks if is flag national not resident.
	 *
	 * @return true, if is flag national not resident
	 */
	public boolean isFlagNationalNotResident() {
		return flagNationalNotResident;
	}

	/**
	 * Sets the flag national not resident.
	 *
	 * @param flagNationalNotResident the new flag national not resident
	 */
	public void setFlagNationalNotResident(boolean flagNationalNotResident) {
		this.flagNationalNotResident = flagNationalNotResident;
	}

	/**
	 * Checks if is flag foreign not resident.
	 *
	 * @return true, if is flag foreign not resident
	 */
	public boolean isFlagForeignNotResident() {
		return flagForeignNotResident;
	}

	/**
	 * Sets the flag foreign not resident.
	 *
	 * @param flagForeignNotResident the new flag foreign not resident
	 */
	public void setFlagForeignNotResident(boolean flagForeignNotResident) {
		this.flagForeignNotResident = flagForeignNotResident;
	}

	/**
	 * Checks if is indicator pep.
	 *
	 * @return true, if is indicator pep
	 */
	public boolean isIndicatorPep() {
		return indicatorPep;
	}

	/**
	 * Sets the indicator pep.
	 *
	 * @param indicatorPep the new indicator pep
	 */
	public void setIndicatorPep(boolean indicatorPep) {
		this.indicatorPep = indicatorPep;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Gets the holder request detail.
	 *
	 * @return the holder request detail
	 */
	public HolderRequest getHolderRequestDetail() {
		return holderRequestDetail;
	}

	/**
	 * Sets the holder request detail.
	 *
	 * @param holderRequestDetail the new holder request detail
	 */
	public void setHolderRequestDetail(HolderRequest holderRequestDetail) {
		this.holderRequestDetail = holderRequestDetail;
	}

	/**
	 * Gets the holder history detail.
	 *
	 * @return the holder history detail
	 */
	public HolderHistory getHolderHistoryDetail() {
		return holderHistoryDetail;
	}

	/**
	 * Sets the holder history detail.
	 *
	 * @param holderHistoryDetail the new holder history detail
	 */
	public void setHolderHistoryDetail(HolderHistory holderHistoryDetail) {
		this.holderHistoryDetail = holderHistoryDetail;
	}

	/**
	 * Gets the participant detail.
	 *
	 * @return the participant detail
	 */
	public Participant getParticipantDetail() {
		return participantDetail;
	}

	/**
	 * Sets the participant detail.
	 *
	 * @param participantDetail the new participant detail
	 */
	public void setParticipantDetail(Participant participantDetail) {
		this.participantDetail = participantDetail;
	}

	/**
	 * Gets the legal representative history detail.
	 *
	 * @return the legal representative history detail
	 */
	public LegalRepresentativeHistory getLegalRepresentativeHistoryDetail() {
		return legalRepresentativeHistoryDetail;
	}

	/**
	 * Sets the legal representative history detail.
	 *
	 * @param legalRepresentativeHistoryDetail the new legal representative history detail
	 */
	public void setLegalRepresentativeHistoryDetail(
			LegalRepresentativeHistory legalRepresentativeHistoryDetail) {
		this.legalRepresentativeHistoryDetail = legalRepresentativeHistoryDetail;
	}

	/**
	 * Sets the holder filter.
	 * 
	 * @param holderFilter
	 *            the new holder filter
	 */
	public void setHolderFilter(HolderRequestTO holderFilter) {
		this.holderFilter = holderFilter;
	}

	/**
	 * Gets the holder filter.
	 * 
	 * @return the holder filter
	 */
	public HolderRequestTO getHolderFilter() {
		return holderFilter;
	}

	/**
	 * Checks if is show panel options component.
	 * 
	 * @return true, if is show panel options component
	 */
	public boolean isShowPanelOptionsComponent() {
		return showPanelOptionsComponent;
	}

	/**
	 * Sets the show panel options component.
	 * 
	 * @param showPanelOptionsComponent
	 *            the new show panel options component
	 */
	public void setShowPanelOptionsComponent(boolean showPanelOptionsComponent) {
		this.showPanelOptionsComponent = showPanelOptionsComponent;
	}

	/**
	 * Gets the user info.
	 * 
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 * 
	 * @param userInfo
	 *            the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * Gets the list document type legal.
	 *
	 * @return the list document type legal
	 */
	public List<ParameterTable> getListDocumentTypeLegal() {
		return listDocumentTypeLegal;
	}

	/**
	 * Sets the list document type legal.
	 *
	 * @param listDocumentTypeLegal the new list document type legal
	 */
	public void setListDocumentTypeLegal(
			List<ParameterTable> listDocumentTypeLegal) {
		this.listDocumentTypeLegal = listDocumentTypeLegal;
	}

	/**
	 * Gets the list document type result.
	 *
	 * @return the list document type result
	 */
	public List<ParameterTable> getListDocumentTypeResult() {
		return listDocumentTypeResult;
	}

	/**
	 * Sets the list document type result.
	 *
	 * @param listDocumentTypeResult the new list document type result
	 */
	public void setListDocumentTypeResult(
			List<ParameterTable> listDocumentTypeResult) {
		this.listDocumentTypeResult = listDocumentTypeResult;
	}

	/**
	 * Gets the list document type legal result.
	 *
	 * @return the list document type legal result
	 */
	public List<ParameterTable> getListDocumentTypeLegalResult() {
		return listDocumentTypeLegalResult;
	}

	/**
	 * Sets the list document type legal result.
	 *
	 * @param listDocumentTypeLegalResult the new list document type legal result
	 */
	public void setListDocumentTypeLegalResult(
			List<ParameterTable> listDocumentTypeLegalResult) {
		this.listDocumentTypeLegalResult = listDocumentTypeLegalResult;
	}

	/**
	 * Gets the list category pep.
	 *
	 * @return the list category pep
	 */
	public List<ParameterTable> getListCategoryPep() {
		return listCategoryPep;
	}

	/**
	 * Sets the list category pep.
	 *
	 * @param listCategoryPep the new list category pep
	 */
	public void setListCategoryPep(List<ParameterTable> listCategoryPep) {
		this.listCategoryPep = listCategoryPep;
	}

	/**
	 * Gets the list role pep.
	 *
	 * @return the list role pep
	 */
	public List<ParameterTable> getListRolePep() {
		return listRolePep;
	}

	/**
	 * Sets the list role pep.
	 *
	 * @param listRolePep the new list role pep
	 */
	public void setListRolePep(List<ParameterTable> listRolePep) {
		this.listRolePep = listRolePep;
	}

		
	/**
	 * Gets the list economic sector.
	 *
	 * @return the list economic sector
	 */
	public List<ParameterTable> getListEconomicSector() {
		return listEconomicSector;
	}

	/**
	 * Sets the list economic sector.
	 *
	 * @param listEconomicSector the new list economic sector
	 */
	public void setListEconomicSector(List<ParameterTable> listEconomicSector) {
		this.listEconomicSector = listEconomicSector;
	}

	/**
	 * Gets the list economic activity.
	 *
	 * @return the list economic activity
	 */
	public List<ParameterTable> getListEconomicActivity() {
		return listEconomicActivity;
	}

	/**
	 * Sets the list economic activity.
	 *
	 * @param listEconomicActivity the new list economic activity
	 */
	public void setListEconomicActivity(
			List<ParameterTable> listEconomicActivity) {
		this.listEconomicActivity = listEconomicActivity;
	}

	/**
	 * Checks if is flag action aprove.
	 *
	 * @return true, if is flag action aprove
	 */
	public boolean isFlagActionAprove() {
		return flagActionAprove;
	}

	/**
	 * Sets the flag action aprove.
	 *
	 * @param flagActionAprove the new flag action aprove
	 */
	public void setFlagActionAprove(boolean flagActionAprove) {
		this.flagActionAprove = flagActionAprove;
	}

	/**
	 * Checks if is flag action modify.
	 *
	 * @return true, if is flag action modify
	 */
	public boolean isFlagActionModify() {
		return flagActionModify;
	}

	/**
	 * Sets the flag action modify.
	 *
	 * @param flagActionModify the new flag action modify
	 */
	public void setFlagActionModify(boolean flagActionModify) {
		this.flagActionModify = flagActionModify;
	}

	/**
	 * Gets the lst legal representative file.
	 *
	 * @return the lst legal representative file
	 */
	public List<LegalRepresentativeFile> getLstLegalRepresentativeFile() {
		return lstLegalRepresentativeFile;
	}

	/**
	 * Sets the lst legal representative file.
	 *
	 * @param lstLegalRepresentativeFile the new lst legal representative file
	 */
	public void setLstLegalRepresentativeFile(
			List<LegalRepresentativeFile> lstLegalRepresentativeFile) {
		this.lstLegalRepresentativeFile = lstLegalRepresentativeFile;
	}

	/**
	 * Gets the lst holder file.
	 *
	 * @return the lst holder file
	 */
	public List<HolderFile> getLstHolderFile() {
		return lstHolderFile;
	}

	/**
	 * Sets the lst holder file.
	 *
	 * @param lstHolderFile the new lst holder file
	 */
	public void setLstHolderFile(List<HolderFile> lstHolderFile) {
		this.lstHolderFile = lstHolderFile;
	}

	/**
	 * Gets the list economic activity legal representative.
	 *
	 * @return the list economic activity legal representative
	 */
	public List<ParameterTable> getListEconomicActivityLegalRepresentative() {
		return listEconomicActivityLegalRepresentative;
	}

	/**
	 * Sets the list economic activity legal representative.
	 *
	 * @param listEconomicActivityLegalRepresentative the new list economic activity legal representative
	 */
	public void setListEconomicActivityLegalRepresentative(
			List<ParameterTable> listEconomicActivityLegalRepresentative) {
		this.listEconomicActivityLegalRepresentative = listEconomicActivityLegalRepresentative;
	}

	/**
	 * Checks if is btn modify view.
	 *
	 * @return true, if is btn modify view
	 */
	public boolean isBtnModifyView() {
		return btnModifyView;
	}

	/**
	 * Sets the btn modify view.
	 *
	 * @param btnModifyView the new btn modify view
	 */
	public void setBtnModifyView(boolean btnModifyView) {
		this.btnModifyView = btnModifyView;
	}

	/**
	 * Checks if is btn confirm view.
	 *
	 * @return true, if is btn confirm view
	 */
	public boolean isBtnConfirmView() {
		return btnConfirmView;
	}

	/**
	 * Sets the btn confirm view.
	 *
	 * @param btnConfirmView the new btn confirm view
	 */
	public void setBtnConfirmView(boolean btnConfirmView) {
		this.btnConfirmView = btnConfirmView;
	}

	/**
	 * Checks if is btn reject view.
	 *
	 * @return true, if is btn reject view
	 */
	public boolean isBtnRejectView() {
		return btnRejectView;
	}

	/**
	 * Sets the btn reject view.
	 *
	 * @param btnRejectView the new btn reject view
	 */
	public void setBtnRejectView(boolean btnRejectView) {
		this.btnRejectView = btnRejectView;
	}

	/**
	 * Checks if is btn approve view.
	 *
	 * @return true, if is btn approve view
	 */
	public boolean isBtnApproveView() {
		return btnApproveView;
	}

	/**
	 * Sets the btn approve view.
	 *
	 * @param btnApproveView the new btn approve view
	 */
	public void setBtnApproveView(boolean btnApproveView) {
		this.btnApproveView = btnApproveView;
	}

	/**
	 * Checks if is btn annular view.
	 *
	 * @return true, if is btn annular view
	 */
	public boolean isBtnAnnularView() {
		return btnAnnularView;
	}

	/**
	 * Sets the btn annular view.
	 *
	 * @param btnAnnularView the new btn annular view
	 */
	public void setBtnAnnularView(boolean btnAnnularView) {
		this.btnAnnularView = btnAnnularView;
	}

	public boolean isDisabledFideicomiso() {
		return disabledFideicomiso;
	}

	public void setDisabledFideicomiso(boolean disabledFideicomiso) {
		this.disabledFideicomiso = disabledFideicomiso;
	}

	/**
	 * Gets the attach file holder selected.
	 *
	 * @return the attach file holder selected
	 */
	public Integer getAttachFileHolderSelected() {
		return attachFileHolderSelected;
	}

	/**
	 * Sets the attach file holder selected.
	 *
	 * @param attachFileHolderSelected the new attach file holder selected
	 */
	public void setAttachFileHolderSelected(Integer attachFileHolderSelected) {
		this.attachFileHolderSelected = attachFileHolderSelected;
	}

	/**
	 * Gets the list holder document type.
	 *
	 * @return the list holder document type
	 */
	public List<ParameterTable> getListHolderDocumentType() {
		return listHolderDocumentType;
	}

	/**
	 * Sets the list holder document type.
	 *
	 * @param listHolderDocumentType the new list holder document type
	 */
	public void setListHolderDocumentType(List<ParameterTable> listHolderDocumentType) {
		this.listHolderDocumentType = listHolderDocumentType;
	}

	/**
	 * Gets the holder file name display.
	 *
	 * @return the holder file name display
	 */
	public String getHolderFileNameDisplay() {
		return holderFileNameDisplay;
	}

	/**
	 * Sets the holder file name display.
	 *
	 * @param holderFileNameDisplay the new holder file name display
	 */
	public void setHolderFileNameDisplay(String holderFileNameDisplay) {
		this.holderFileNameDisplay = holderFileNameDisplay;
	}

	/**
	 * Gets the list second document type.
	 *
	 * @return the list second document type
	 */
	public List<ParameterTable> getListSecondDocumentType() {
		return listSecondDocumentType;
	}

	/**
	 * Sets the list second document type.
	 *
	 * @param listSecondDocumentType the new list second document type
	 */
	public void setListSecondDocumentType(
			List<ParameterTable> listSecondDocumentType) {
		this.listSecondDocumentType = listSecondDocumentType;
	}

	/**
	 * Checks if is disabled ind resident.
	 *
	 * @return true, if is disabled ind resident
	 */
	public boolean isDisabledIndResident() {
		return disabledIndResident;
	}

	/**
	 * Sets the disabled ind resident.
	 *
	 * @param disabledIndResident the new disabled ind resident
	 */
	public void setDisabledIndResident(boolean disabledIndResident) {
		this.disabledIndResident = disabledIndResident;
	}

	/**
	 * Checks if is disabled nationality.
	 *
	 * @return true, if is disabled nationality
	 */
	public boolean isDisabledNationality() {
		return disabledNationality;
	}

	/**
	 * Sets the disabled nationality.
	 *
	 * @param disabledNationality the new disabled nationality
	 */
	public void setDisabledNationality(boolean disabledNationality) {
		this.disabledNationality = disabledNationality;
	}

	/**
	 * Checks if is disabled representative nationality.
	 *
	 * @return true, if is disabled representative nationality
	 */
	public boolean isDisabledRepresentativeNationality() {
		return disabledRepresentativeNationality;
	}

	/**
	 * Sets the disabled representative nationality.
	 *
	 * @param disabledRepresentativeNationality the new disabled representative nationality
	 */
	public void setDisabledRepresentativeNationality(
			boolean disabledRepresentativeNationality) {
		this.disabledRepresentativeNationality = disabledRepresentativeNationality;
	}

	/**
	 * Checks if is disabled representative resident.
	 *
	 * @return true, if is disabled representative resident
	 */
	public boolean isDisabledRepresentativeResident() {
		return disabledRepresentativeResident;
	}

	/**
	 * Sets the disabled representative resident.
	 *
	 * @param disabledRepresentativeResident the new disabled representative resident
	 */
	public void setDisabledRepresentativeResident(
			boolean disabledRepresentativeResident) {
		this.disabledRepresentativeResident = disabledRepresentativeResident;
	}

	/**
	 * Gets the rnc result find.
	 *
	 * @return the rnc result find
	 */
	public InstitutionInformation getNitResultFind() {
		return nitResultFind;
	}

	/**
	 * Sets the rnc result find.
	 *
	 * @param nitResultFind the new nit result find
	 */
	public void setNitResultFind(InstitutionInformation nitResultFind) {
		this.nitResultFind = nitResultFind;
	}

	/**
	 * Checks if is flag add representative modify.
	 *
	 * @return true, if is flag add representative modify
	 */
	public boolean isFlagAddRepresentativeModify() {
		return flagAddRepresentativeModify;
	}

	/**
	 * Sets the flag add representative modify.
	 *
	 * @param flagAddRepresentativeModify the new flag add representative modify
	 */
	public void setFlagAddRepresentativeModify(boolean flagAddRepresentativeModify) {
		this.flagAddRepresentativeModify = flagAddRepresentativeModify;
	}

	/**
	 * Checks if is flag modify representative modify.
	 *
	 * @return true, if is flag modify representative modify
	 */
	public boolean isFlagModifyRepresentativeModify() {
		return flagModifyRepresentativeModify;
	}

	/**
	 * Sets the flag modify representative modify.
	 *
	 * @param flagModifyRepresentativeModify the new flag modify representative modify
	 */
	public void setFlagModifyRepresentativeModify(
			boolean flagModifyRepresentativeModify) {
		this.flagModifyRepresentativeModify = flagModifyRepresentativeModify;
	}

	/**
	 * Checks if is flag export data holder.
	 *
	 * @return true, if is flag export data holder
	 */
	public boolean isFlagExportDataHolder() {
		return flagExportDataHolder;
	}

	/**
	 * Sets the flag export data holder.
	 *
	 * @param flagExportDataHolder the new flag export data holder
	 */
	public void setFlagExportDataHolder(boolean flagExportDataHolder) {
		this.flagExportDataHolder = flagExportDataHolder;
	}

	/**
	 * Checks if is flag export data representative.
	 *
	 * @return true, if is flag export data representative
	 */
	public boolean isFlagExportDataRepresentative() {
		return flagExportDataRepresentative;
	}

	/**
	 * Sets the flag export data representative.
	 *
	 * @param flagExportDataRepresentative the new flag export data representative
	 */
	public void setFlagExportDataRepresentative(boolean flagExportDataRepresentative) {
		this.flagExportDataRepresentative = flagExportDataRepresentative;
	}

	/**
	 * Gets the list second document type legal.
	 *
	 * @return the list second document type legal
	 */
	public List<ParameterTable> getListSecondDocumentTypeLegal() {
		return listSecondDocumentTypeLegal;
	}

	/**
	 * Sets the list second document type legal.
	 *
	 * @param listSecondDocumentTypeLegal the new list second document type legal
	 */
	public void setListSecondDocumentTypeLegal(
			List<ParameterTable> listSecondDocumentTypeLegal) {
		this.listSecondDocumentTypeLegal = listSecondDocumentTypeLegal;
	}

	/**
	 * Checks if is flag minor age.
	 *
	 * @return true, if is flag minor age
	 */
	public boolean isFlagMinorAge() {
		return flagMinorAge;
	}

	/**
	 * Sets the flag minor age.
	 *
	 * @param flagMinorAge the new flag minor age
	 */
	public void setFlagMinorAge(boolean flagMinorAge) {
		this.flagMinorAge = flagMinorAge;
	}

	/**
	 * Gets the list department location.
	 *
	 * @return the list department location
	 */
	public List<GeographicLocation> getListDepartmentLocation() {
		return listDepartmentLocation;
	}

	/**
	 * Sets the list department location.
	 *
	 * @param listDepartmentLocation the new list department location
	 */
	public void setListDepartmentLocation(
			List<GeographicLocation> listDepartmentLocation) {
		this.listDepartmentLocation = listDepartmentLocation;
	}

	/**
	 * Checks if is disabled cmb postal departament.
	 *
	 * @return true, if is disabled cmb postal departament
	 */
	public boolean isDisabledCmbPostalDepartament() {
		return disabledCmbPostalDepartament;
	}

	/**
	 * Sets the disabled cmb postal departament.
	 *
	 * @param disabledCmbPostalDepartament the new disabled cmb postal departament
	 */
	public void setDisabledCmbPostalDepartament(boolean disabledCmbPostalDepartament) {
		this.disabledCmbPostalDepartament = disabledCmbPostalDepartament;
	}

	/**
	 * Gets the list department postal location.
	 *
	 * @return the list department postal location
	 */
	public List<GeographicLocation> getListDepartmentPostalLocation() {
		return listDepartmentPostalLocation;
	}

	/**
	 * Sets the list department postal location.
	 *
	 * @param listDepartmentPostalLocation the new list department postal location
	 */
	public void setListDepartmentPostalLocation(
			List<GeographicLocation> listDepartmentPostalLocation) {
		this.listDepartmentPostalLocation = listDepartmentPostalLocation;
	}

	/**
	 * Gets the legal representative helper bean.
	 *
	 * @return the legal representative helper bean
	 */
	public LegalRepresentativeHelperBean getLegalRepresentativeHelperBean() {
		return legalRepresentativeHelperBean;
	}

	/**
	 * Sets the legal representative helper bean.
	 *
	 * @param legalRepresentativeHelperBean the new legal representative helper bean
	 */
	public void setLegalRepresentativeHelperBean(
			LegalRepresentativeHelperBean legalRepresentativeHelperBean) {
		this.legalRepresentativeHelperBean = legalRepresentativeHelperBean;
	}

	/**
	 * Gets the streamed content file.
	 *
	 * @return the streamed content file
	 */
	public StreamedContent getStreamedContentFile() {
		return streamedContentFile;
	}

	/**
	 * Sets the streamed content file.
	 *
	 * @param streamedContentFile the new streamed content file
	 */
	public void setStreamedContentFile(StreamedContent streamedContentFile) {
		this.streamedContentFile = streamedContentFile;
	}


	/**
	 * Gets the list geographic location without national country.
	 *
	 * @return the list geographic location without national country
	 */
	public List<GeographicLocation> getListGeographicLocationWithoutNationalCountry() {
		return listGeographicLocationWithoutNationalCountry;
	}

	/**
	 * Sets the list geographic location without national country.
	 *
	 * @param listGeographicLocationWithoutNationalCountry the new list geographic location without national country
	 */
	public void setListGeographicLocationWithoutNationalCountry(
			List<GeographicLocation> listGeographicLocationWithoutNationalCountry) {
		this.listGeographicLocationWithoutNationalCountry = listGeographicLocationWithoutNationalCountry;
	}

	/**
	 * Gets the list emission source.
	 *
	 * @return the list emission source
	 */
	public List<ParameterTable> getListDocumentSource() {
		return listDocumentSource;
	}

	/**
	 * Sets the list emission source.
	 *
	 * @param listDocumentSource the new list document source
	 */
	public void setListDocumentSource(List<ParameterTable> listDocumentSource) {
		this.listDocumentSource = listDocumentSource;
	}

	/**
	 * Checks if is flag national resident.
	 *
	 * @return true, if is flag national resident
	 */
	public boolean isFlagNationalResident() {
		return flagNationalResident;
	}

	/**
	 * Sets the flag national resident.
	 *
	 * @param flagNationalResident the new flag national resident
	 */
	public void setFlagNationalResident(boolean flagNationalResident) {
		this.flagNationalResident = flagNationalResident;
	}

	/**
	 * Checks if is flag foreign resident.
	 *
	 * @return true, if is flag foreign resident
	 */
	public boolean isFlagForeignResident() {
		return flagForeignResident;
	}

	/**
	 * Sets the flag foreign resident.
	 *
	 * @param flagForeignResident the new flag foreign resident
	 */
	public void setFlagForeignResident(boolean flagForeignResident) {
		this.flagForeignResident = flagForeignResident;
	}

	/**
	 * Gets the list document issuance date.
	 *
	 * @return the list document issuance date
	 */
	public LinkedHashMap<String, Integer> getListDocumentIssuanceDate() {
		return listDocumentIssuanceDate;
	}

	/**
	 * Sets the list document issuance date.
	 *
	 * @param listDocumentIssuanceDate the list document issuance date
	 */
	public void setListDocumentIssuanceDate(LinkedHashMap<String, Integer> listDocumentIssuanceDate) {
		this.listDocumentIssuanceDate = listDocumentIssuanceDate;
	}

	/**
	 * Gets the document to.
	 *
	 * @return the document to
	 */
	public DocumentTO getDocumentTO() {
		return documentTO;
	}

	/**
	 * Sets the document to.
	 *
	 * @param documentTO the new document to
	 */
	public void setDocumentTO(DocumentTO documentTO) {
		this.documentTO = documentTO;
	}

	/**
	 * Checks if is participant af p_ fi c_ pat.
	 *
	 * @return true, if is participant af p_ fi c_ pat
	 */
	public boolean isParticipantAFP_FIC_PAT() {
		return participantAFP_FIC_PAT;
	}

	/**
	 * Sets the participant af p_ fi c_ pat.
	 *
	 * @param participantAFP_FIC_PAT the new participant af p_ fi c_ pat
	 */
	public void setParticipantAFP_FIC_PAT(boolean participantAFP_FIC_PAT) {
		this.participantAFP_FIC_PAT = participantAFP_FIC_PAT;
	}

	/**
	 * Gets the user privilege.
	 *
	 * @return the user privilege
	 */
	public UserPrivilege getUserPrivilege() {
		return userPrivilege;
	}

	/**
	 * Sets the user privilege.
	 *
	 * @param userPrivilege the new user privilege
	 */
	public void setUserPrivilege(UserPrivilege userPrivilege) {
		this.userPrivilege = userPrivilege;
	}

	/**
	 * Gets the client information to.
	 *
	 * @return the client information to
	 */
	public ClientInformationTO getClientInformationTO() {
		return clientInformationTO;
	}

	/**
	 * Sets the client information to.
	 *
	 * @param clientInformationTO the new client information to
	 */
	public void setClientInformationTO(ClientInformationTO clientInformationTO) {
		this.clientInformationTO = clientInformationTO;
	}

	/**
	 * Gets the list civil status.
	 *
	 * @return the list civil status
	 */
	public List<ParameterTable> getListCivilStatus() {
		return listCivilStatus;
	}

	/**
	 * Sets the list civil status.
	 *
	 * @param listCivilStatus the new list civil status
	 */
	public void setListCivilStatus(List<ParameterTable> listCivilStatus) {
		this.listCivilStatus = listCivilStatus;
	}

	/**
	 * Gets the list total income.
	 *
	 * @return the list total income
	 */
	public List<ParameterTable> getListTotalIncome() {
		return listTotalIncome;
	}

	/**
	 * Sets the list total income.
	 *
	 * @param listTotalIncome the new list total income
	 */
	public void setListTotalIncome(List<ParameterTable> listTotalIncome) {
		this.listTotalIncome = listTotalIncome;
	}
	
	/**
	 * File upload handler.
	 *
	 * @param event the event
	 */
	public void fileUploadHandler(FileUploadEvent event){
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);				
		Object [] dataOnMessage = { event.getFile().getFileName(), event.getFile().getSize() / 1000 };
		showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, "opt.adm.holder.fupl.ok", dataOnMessage);
		String fDisplayName = fUploadValidateFile(event.getFile(), null, null);
		if(fDisplayName != null){			 
			try {				
				InputStream inputImage = new ByteArrayInputStream(event.getFile().getContents());
				filePhotoHolder = new DefaultStreamedContent(inputImage, event.getFile().getContentType(), event.getFile().getFileName());  
				photoHolderName = event.getFile().getFileName();				
				holderHistory.setFilePhoto(event.getFile().getContents());
				holderHistory.setFilePhotoName(event.getFile().getFileName());
				filePhotoTemp = event.getFile().getContents();				
				JSFUtilities.putSessionMap("sessStreamedPhoto", filePhotoTemp);
				auxFuplPhoto = Boolean.TRUE;
				formState = Boolean.TRUE;
			} catch (Exception e) {
				e.printStackTrace();
			}
			 JSFUtilities.showValidationDialog();
		 }
	}
	
	/**
	 * Gets the streamed content file photo.
	 *
	 * @return the streamed content file photo
	 */
	public StreamedContent getStreamedContentFilePhoto(){
		StreamedContent streamedContentFile = null;
		try {
			InputStream inputStream = null;
			inputStream = new ByteArrayInputStream(filePhotoTemp);
			streamedContentFile = new DefaultStreamedContent(inputStream, null,	photoHolderName);
			inputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return streamedContentFile;
	}
	
	/**
	 * Gets the streamed content file photo.
	 *
	 * @return the streamed content file photo
	 */
	public StreamedContent getStreamedContentFilePhotoHistory(){
		StreamedContent streamedContentFile = null;
		try {
			InputStream inputStream = null;
			inputStream = new ByteArrayInputStream(holderHistoryDetail.getFilePhoto());
			streamedContentFile = new DefaultStreamedContent(inputStream, null,	holderHistoryDetail.getFilePhotoName());
			inputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return streamedContentFile;
	}
	
	/**
	 * Delete system image temp.
	 */
	public void deleteFilePhotoTemp(){
		filePhotoTemp = null;
		photoHolderName = "";
		holderHistory.setFilePhoto(null);
		holderHistory.setFilePhotoName(null);
		filePhotoHolder = null;
		JSFUtilities.putSessionMap("sessStreamedPhoto",null);
	}

	/**
	 * File upload handler.
	 *
	 * @param event the event
	 */
	public void fileUploadHandlerSigning(FileUploadEvent event){
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);				
		Object [] dataOnMessage = { event.getFile().getFileName(), event.getFile().getSize() / 1000 };
		showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, "opt.adm.holder.fupl.ok", dataOnMessage);
		String fDisplayName = fUploadValidateFile(event.getFile(), null, null);
		if(fDisplayName != null){			 
			try {				
				InputStream inputImage = new ByteArrayInputStream(event.getFile().getContents());
				fileSigning = new DefaultStreamedContent(inputImage, event.getFile().getContentType(), event.getFile().getFileName());  
				signingHolderName = event.getFile().getFileName();				
				holderHistory.setFileSigning(event.getFile().getContents());				
				fileSigningTemp = event.getFile().getContents();				
				JSFUtilities.putSessionMap("sessStreamedSigning", fileSigningTemp);
				auxFuplSigning = Boolean.TRUE;
				formState = Boolean.TRUE;
			} catch (Exception e) {
				e.printStackTrace();
			}
			 JSFUtilities.showValidationDialog();
		 }
	}
	
	/**
	 * Delete system image temp.
	 */
	public void deleteFileSigningTemp(){
		fileSigningTemp = null;
		signingHolderName = "";
		holderHistory.setFileSigning(null);
		fileSigning = null;
		JSFUtilities.putSessionMap("sessStreamedSigning", null);
	}
	
	/**
	 * Clean file image.
	 */
	public void cleanFileImage(){
		JSFUtilities.removeSessionMap("sessStreamedPhoto");
		JSFUtilities.removeSessionMap("sessStreamedSigning");
	}
	
	/**
	 * Gets the file photo holder.
	 *
	 * @return the filePhotoHolder
	 */
	public StreamedContent getFilePhotoHolder() {
		return filePhotoHolder;
	}

	/**
	 * Sets the file photo holder.
	 *
	 * @param filePhotoHolder the filePhotoHolder to set
	 */
	public void setFilePhotoHolder(StreamedContent filePhotoHolder) {
		this.filePhotoHolder = filePhotoHolder;
	}

	/**
	 * Gets the photo holder name.
	 *
	 * @return the photoHolderName
	 */
	public String getPhotoHolderName() {
		return photoHolderName;
	}

	/**
	 * Sets the photo holder name.
	 *
	 * @param photoHolderName the photoHolderName to set
	 */
	public void setPhotoHolderName(String photoHolderName) {
		this.photoHolderName = photoHolderName;
	}

	/**
	 * Gets the file photo temp.
	 *
	 * @return the filePhotoTemp
	 */
	public byte[] getFilePhotoTemp() {
		return filePhotoTemp;
	}

	/**
	 * Sets the file photo temp.
	 *
	 * @param filePhotoTemp the filePhotoTemp to set
	 */
	public void setFilePhotoTemp(byte[] filePhotoTemp) {
		this.filePhotoTemp = filePhotoTemp;
	}

	/**
	 * Checks if is aux fupl photo.
	 *
	 * @return the auxFuplPhoto
	 */
	public boolean isAuxFuplPhoto() {
		return auxFuplPhoto;
	}

	/**
	 * Sets the aux fupl photo.
	 *
	 * @param auxFuplPhoto the auxFuplPhoto to set
	 */
	public void setAuxFuplPhoto(boolean auxFuplPhoto) {
		this.auxFuplPhoto = auxFuplPhoto;
	}

	/**
	 * Checks if is form state.
	 *
	 * @return the formState
	 */
	public boolean isFormState() {
		return formState;
	}

	/**
	 * Sets the form state.
	 *
	 * @param formState the formState to set
	 */
	public void setFormState(boolean formState) {
		this.formState = formState;
	}

	/**
	 * Gets the file signing.
	 *
	 * @return the fileSigning
	 */
	public StreamedContent getFileSigning() {
		return fileSigning;
	}

	/**
	 * Sets the file signing.
	 *
	 * @param fileSigning the fileSigning to set
	 */
	public void setFileSigning(StreamedContent fileSigning) {
		this.fileSigning = fileSigning;
	}

	/**
	 * Gets the signing holder name.
	 *
	 * @return the signingHolderName
	 */
	public String getSigningHolderName() {
		return signingHolderName;
	}

	/**
	 * Sets the signing holder name.
	 *
	 * @param signingHolderName the signingHolderName to set
	 */
	public void setSigningHolderName(String signingHolderName) {
		this.signingHolderName = signingHolderName;
	}

	/**
	 * Gets the file signing temp.
	 *
	 * @return the fileSigningTemp
	 */
	public byte[] getFileSigningTemp() {
		return fileSigningTemp;
	}

	/**
	 * Sets the file signing temp.
	 *
	 * @param fileSigningTemp the fileSigningTemp to set
	 */
	public void setFileSigningTemp(byte[] fileSigningTemp) {
		this.fileSigningTemp = fileSigningTemp;
	}

	/**
	 * Checks if is aux fupl signing.
	 *
	 * @return the auxFuplSigning
	 */
	public boolean isAuxFuplSigning() {
		return auxFuplSigning;
	}

	/**
	 * Sets the aux fupl signing.
	 *
	 * @param auxFuplSigning the auxFuplSigning to set
	 */
	public void setAuxFuplSigning(boolean auxFuplSigning) {
		this.auxFuplSigning = auxFuplSigning;
	}

	/**
	 * Checks if is flag economic activity.
	 *
	 * @return the flagEconomicActivity
	 */
	public boolean isFlagEconomicActivity() {
		return flagEconomicActivity;
	}

	/**
	 * Sets the flag economic activity.
	 *
	 * @param flagEconomicActivity the flagEconomicActivity to set
	 */
	public void setFlagEconomicActivity(boolean flagEconomicActivity) {
		this.flagEconomicActivity = flagEconomicActivity;
	}

	/**
	 * Checks if is disabled fund administrator.
	 *
	 * @return the disabledFundAdministrator
	 */
	public boolean isDisabledFundAdministrator() {
		return disabledFundAdministrator;
	}

	/**
	 * Sets the disabled fund administrator.
	 *
	 * @param disabledFundAdministrator the disabledFundAdministrator to set
	 */
	public void setDisabledFundAdministrator(boolean disabledFundAdministrator) {
		this.disabledFundAdministrator = disabledFundAdministrator;
	}

	/**
	 * Checks if is disabled fund type.
	 *
	 * @return the disabledFundType
	 */
	public boolean isDisabledFundType() {
		return disabledFundType;
	}

	/**
	 * Sets the disabled fund type.
	 *
	 * @param disabledFundType the disabledFundType to set
	 */
	public void setDisabledFundType(boolean disabledFundType) {
		this.disabledFundType = disabledFundType;
	}

	/**
	 * Checks if is disabled transfer number.
	 *
	 * @return the disabledTransferNumber
	 */
	public boolean isDisabledTransferNumber() {
		return disabledTransferNumber;
	}

	/**
	 * Sets the disabled transfer number.
	 *
	 * @param disabledTransferNumber the disabledTransferNumber to set
	 */
	public void setDisabledTransferNumber(boolean disabledTransferNumber) {
		this.disabledTransferNumber = disabledTransferNumber;
	}

	/**
	 * Gets the str style required cui.
	 *
	 * @return the strStyleRequiredCUI
	 */
	public String getStrStyleRequiredCUI() {
		return strStyleRequiredCUI;
	}

	/**
	 * Sets the str style required cui.
	 *
	 * @param strStyleRequiredCUI the strStyleRequiredCUI to set
	 */
	public void setStrStyleRequiredCUI(String strStyleRequiredCUI) {
		this.strStyleRequiredCUI = strStyleRequiredCUI;
	}

	/**
	 * Gets the str style required fund type.
	 *
	 * @return the strStyleRequiredFundType
	 */
	public String getStrStyleRequiredFundType() {
		return strStyleRequiredFundType;
	}

	/**
	 * Sets the str style required fund type.
	 *
	 * @param strStyleRequiredFundType the strStyleRequiredFundType to set
	 */
	public void setStrStyleRequiredFundType(String strStyleRequiredFundType) {
		this.strStyleRequiredFundType = strStyleRequiredFundType;
	}

	/**
	 * Gets the str style required fund adm.
	 *
	 * @return the strStyleRequiredFundAdm
	 */
	public String getStrStyleRequiredFundAdm() {
		return strStyleRequiredFundAdm;
	}

	/**
	 * Sets the str style required fund adm.
	 *
	 * @param strStyleRequiredFundAdm the strStyleRequiredFundAdm to set
	 */
	public void setStrStyleRequiredFundAdm(String strStyleRequiredFundAdm) {
		this.strStyleRequiredFundAdm = strStyleRequiredFundAdm;
	}

	/**
	 * Gets the str style required transfer nro.
	 *
	 * @return the strStyleRequiredTransferNro
	 */
	public String getStrStyleRequiredTransferNro() {
		return strStyleRequiredTransferNro;
	}

	/**
	 * Sets the str style required transfer nro.
	 *
	 * @param strStyleRequiredTransferNro the strStyleRequiredTransferNro to set
	 */
	public void setStrStyleRequiredTransferNro(String strStyleRequiredTransferNro) {
		this.strStyleRequiredTransferNro = strStyleRequiredTransferNro;
	}

	/**
	 * Checks if is ind ficpat economic activity.
	 *
	 * @return the indFICPATEconomicActivity
	 */
	public boolean isIndFICPATEconomicActivity() {
		return indFICPATEconomicActivity;
	}

	/**
	 * Sets the ind ficpat economic activity.
	 *
	 * @param indFICPATEconomicActivity the indFICPATEconomicActivity to set
	 */
	public void setIndFICPATEconomicActivity(boolean indFICPATEconomicActivity) {
		this.indFICPATEconomicActivity = indFICPATEconomicActivity;
	}

	/**
	 * Checks if is flag is issuer dpf institution.
	 *
	 * @return the flagIsIssuerDpfInstitution
	 */
	public boolean isFlagIsIssuerDpfInstitution() {
		return flagIsIssuerDpfInstitution;
	}

	/**
	 * Sets the flag is issuer dpf institution.
	 *
	 * @param flagIsIssuerDpfInstitution the flagIsIssuerDpfInstitution to set
	 */
	public void setFlagIsIssuerDpfInstitution(boolean flagIsIssuerDpfInstitution) {
		this.flagIsIssuerDpfInstitution = flagIsIssuerDpfInstitution;
	}

	public boolean isFlagSendASFI() {
		return flagSendASFI;
	}

	public void setFlagSendASFI(boolean flagSendASFI) {
		this.flagSendASFI = flagSendASFI;
	}
	
	public boolean isFlagflagSendASFI() {
		return flagflagSendASFI;
	}

	public void setFlagflagSendASFI(boolean flagflagSendASFI) {
		this.flagflagSendASFI = flagflagSendASFI;
	}
	
	public String getNewDocumentNumber() {
		return newDocumentNumber;
	}

	public void setNewDocumentNumber(String newDocumentNumber) {
		this.newDocumentNumber = newDocumentNumber;
	}

	public String getNewDocumentNumberSecondNationality() {
		return newDocumentNumberSecondNationality;
	}

	public void setNewDocumentNumberSecondNationality(
			String newDocumentNumberSecondNationality) {
		this.newDocumentNumberSecondNationality = newDocumentNumberSecondNationality;
	}

	public List<InvestorTypeModelTO> getListInvestorType() {
		return listInvestorType;
	}

	public void setListInvestorType(List<InvestorTypeModelTO> listInvestorType) {
		this.listInvestorType = listInvestorType;
	}

	public MassiveHolderTO getMassiveHolderTO() {
		return massiveHolderTO;
	}

	public void setMassiveHolderTO(MassiveHolderTO massiveHolderTO) {
		this.massiveHolderTO = massiveHolderTO;
	}

	public List<ParameterTable> getListPepGrade() {
		return listPepGrade;
	}

	public void setListPepGrade(List<ParameterTable> listPepGrade) {
		this.listPepGrade = listPepGrade;
	}

	public List<ParameterTable> getListPepGradeWithPersonType() {
		return listPepGradeWithPersonType;
	}

	public void setListPepGradeWithPersonType(List<ParameterTable> listPepGradeWithPersonType) {
		this.listPepGradeWithPersonType = listPepGradeWithPersonType;
	}

	public List<ParameterTable> getListEconomicActivityNaturalPerson() {
		return listEconomicActivityNaturalPerson;
	}

	public void setListEconomicActivityNaturalPerson(List<ParameterTable> listEconomicActivityNaturalPerson) {
		this.listEconomicActivityNaturalPerson = listEconomicActivityNaturalPerson;
	}

	public List<ParameterTable> getLstCategoryPepWithPersonType() {
		return lstCategoryPepWithPersonType;
	}

	public void setLstCategoryPepWithPersonType(List<ParameterTable> lstCategoryPepWithPersonType) {
		this.lstCategoryPepWithPersonType = lstCategoryPepWithPersonType;
	}

	public Integer getLinkForFamily() {
		return linkForFamily;
	}
	
}