package com.pradera.accounts.holder.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.accounts.holder.to.HolderAnnulationModelTO;

/**
 * DaModel de la clase HolderAnnulationModelTO
 * @author rlarico
 *
 */
public class HolderAnulationDataModel extends ListDataModel<HolderAnnulationModelTO> implements SelectableDataModel<HolderAnnulationModelTO>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// primer constructor
	public HolderAnulationDataModel(){}
	
	// segundo constructor
	public HolderAnulationDataModel(List<HolderAnnulationModelTO> lstHolderAnnulation){
		super(lstHolderAnnulation);
	}

	@Override
	public HolderAnnulationModelTO getRowData(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getRowKey(HolderAnnulationModelTO arg0) {
		// TODO Auto-generated method stub
		return null;
	}

}
