package com.pradera.accounts.holder.facade;

 
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.accounts.holder.service.HolderControlBean;
import com.pradera.accounts.holder.service.HolderServiceBean;
import com.pradera.accounts.holder.service.ShareHoldersService;
import com.pradera.accounts.holder.to.HolderRequestTO;
import com.pradera.accounts.holder.to.ShareHolderTO;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.services.remote.reports.ReportGenerationService;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.RegistryType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.core.framework.entity.service.LoaderEntityServiceBean;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.HolderFile;
import com.pradera.model.accounts.HolderHistory;
import com.pradera.model.accounts.HolderHistoryState;
import com.pradera.model.accounts.HolderReqFileHistory;
import com.pradera.model.accounts.HolderRequest;
import com.pradera.model.accounts.LegalRepresentative;
import com.pradera.model.accounts.LegalRepresentativeFile;
import com.pradera.model.accounts.LegalRepresentativeHistory;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.PepPerson;
import com.pradera.model.accounts.RepresentativeFileHistory;
import com.pradera.model.accounts.RepresentedEntity;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.HolderFileStateType;
import com.pradera.model.accounts.type.HolderReqFileHisType;
import com.pradera.model.accounts.type.HolderRequestStateType;
import com.pradera.model.accounts.type.HolderRequestType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.LegalRepresentativeStateType;
import com.pradera.model.accounts.type.PepPersonClassificationType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.accounts.type.RepresentativeEntityStateType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.operations.RequestCorrelativeType;
import com.pradera.model.report.ReportLoggerFile;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ShareHoldersFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/08/2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ShareHoldersFacade extends CrudDaoServiceBean {
	
	 /** The user info. */
 	@Inject
	 private UserInfo userInfo;
	 
	 /** The share holders service. */
 	@EJB
	 ShareHoldersService shareHoldersService;
		
	 
	 /**
 	 * Gets the list share holder.
 	 *
 	 * @param accountNumber the account number
 	 * @param securityClass the security class
 	 * @param securityCode the security code
 	 * @param issuerCode the issuer code
 	 * @return the list share holder
 	 * @throws ServiceException the service exception
 	 */
 	@ProcessAuditLogger(process=BusinessProcessType.NEW_HOLDER_CREATION_REQUEST_QUERY)
	 public List<ShareHolderTO> getListShareHolder(Integer accountNumber, Integer  securityClass, String securityCode,String issuerCode) throws ServiceException{
		 
		 List<ShareHolderTO> list = shareHoldersService.getListShare(accountNumber,securityClass,securityCode,issuerCode);
		 return list;
	 }
	 
	 /**
 	 * Gets the list holder.
 	 *
 	 * @param holderAccountPk the holder account pk
 	 * @return the list holder
 	 * @throws ServiceException the service exception
 	 */
 	public List<Holder> getListHolder(Long holderAccountPk) throws ServiceException{
		 
		 List<Holder> list = shareHoldersService.getListShareHolder(holderAccountPk);
		 return list;
	 }
	 
	 /**
 	 * Gets the issuer.
 	 *
 	 * @param issuerCode the issuer code
 	 * @return the issuer
 	 * @throws ServiceException the service exception
 	 */
 	public Issuer getIssuer(String issuerCode) throws ServiceException{
			return shareHoldersService.find(Issuer.class, issuerCode);
	 }
	 
	 public boolean verifySecurity(Long idParticipant, Long idHolderAccount,String idIssuerPk) throws ServiceException{
			return shareHoldersService.getVerifyHolderAccount(idParticipant, idHolderAccount,idIssuerPk);
	 }
	 
	 public List<Issuer> getActionsIssuer (){
		 return shareHoldersService.getActionsIssuer();
	 }
	 
	
     
}
