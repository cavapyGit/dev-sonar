package com.pradera.accounts.holder.massive.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap; 
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.Query;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.accounts.holder.massive.to.HolderMassiveRow;
import com.pradera.accounts.holder.service.HolderAdditionalServiceBean;
import com.pradera.accounts.holder.service.HolderServiceBean;
import com.pradera.accounts.util.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GenericPropertiesConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.component.accounts.to.HolderAccountObjectTO;
import com.pradera.integration.component.accounts.to.LegalRepresentativeObjectTO;
import com.pradera.integration.component.accounts.to.NaturalHolderObjectTO;
import com.pradera.integration.component.accounts.to.PersonObjectTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.HolderMassiveFile;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionStateType;

@Singleton
public class HolderMassiveAdditionalService extends CrudDaoServiceBean{

	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	@EJB
	HolderServiceBean holderServiceBean;
	
	@EJB
	HolderAdditionalServiceBean holderAdditionalServiceBean;
	
	@Inject  
	PraderaLogger logger;

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public HolderMassiveFile saveHolderProcessFileTx(ProcessFileTO mcnProcessFileTO, LoggerUser loggerUser) {        
		HolderMassiveFile file = new HolderMassiveFile();
		file.setRegisterUser(loggerUser.getUserName());
		file.setTotalOperations(new BigDecimal(mcnProcessFileTO.getTotalOperations()));
		file.setFileName(mcnProcessFileTO.getFileName());
		file.setIndAutomacticProcess(mcnProcessFileTO.getIndAutomaticProcess());
		//only for OTC.
		file.setParticipant(new Participant(mcnProcessFileTO.getIdParticipantPk()));
		
		file.setProcessDate(CommonsUtilities.currentDateTime());
		file.setProcessedFile(mcnProcessFileTO.getProcessFile());
		file.setProcessType(mcnProcessFileTO.getProcessType());
		file.setProcessState(ExternalInterfaceReceptionStateType.PENDING.getCode());
		Long sequenceNumber = holderServiceBean.nextHolderMassiveFileSequenceNumber(file.getProcessDate(),mcnProcessFileTO.getIdParticipantPk(),file.getProcessType());
		file.setSequenceNumber(sequenceNumber);
		file.setAudit(loggerUser);
		holderServiceBean.create(file);
		return file;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	public synchronized RecordValidationType saveSingleHolderRequest(HolderMassiveRow holderRow, HolderMassiveFile holderMassiveFile) {
		
		LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

		HolderAccountObjectTO holderAccountObjectTO = holderRow.getHolderAccountObjectTO();
		RecordValidationType recordValidationType = null;
		Long idHolderAccount = null;
		if (!holderAccountObjectTO.isDpfInterface()) {
			if (holderAccountObjectTO.getAccountType().equals(HolderAccountType.JURIDIC.getCode())) {
				/*holderAccountObjectTO.setLstLegalRepresentativeObjectTOs(new ArrayList<LegalRepresentativeObjectTO>());
				for (NaturalHolderObjectTO naturalHolderObjectTO : holderAccountObjectTO
						.getLstNaturalHolderObjectTOs()) {
					LegalRepresentativeObjectTO legalRepresentativeObjectTO = new LegalRepresentativeObjectTO(
							(PersonObjectTO) naturalHolderObjectTO);
					holderAccountObjectTO.getLstLegalRepresentativeObjectTOs().add(legalRepresentativeObjectTO);
				}
				holderAccountObjectTO.setLstNaturalHolderObjectTOs(null);
				*/
				// Verificando que al menos exista un representante legal
				if (Validations.validateListIsNullOrEmpty(holderAccountObjectTO.getLstLegalRepresentativeObjectTOs()) ||
						holderAccountObjectTO.getLstLegalRepresentativeObjectTOs().size() < 1) {
					return CommonsUtilities.populateRecordCodeValidation(null,
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSUFFICIENT_LEGAL_REPRESANTIVE, null);
				}
			}
			if (holderAccountObjectTO.getAccountType() == null) {
				return CommonsUtilities.populateRecordCodeValidation(null,
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_ACCOUNT_TYPE, null);
			}
		}

		holderAccountObjectTO.setChildrenIndicators();

		// CAMBIAR LLAMADA AL METODO REMOTO DE CREACION DE CUENTAS
		
		Object objResponse;
		try {
			objResponse = holderAdditionalServiceBean.createHolderAndAccountMassive(holderAccountObjectTO);
			if (objResponse instanceof RecordValidationType) {
				RecordValidationType validation = (RecordValidationType) objResponse;
				recordValidationType = validation;
			} else {
				recordValidationType = null;
			}
		} catch (ServiceException e) {
			e.printStackTrace();
			RecordValidationType validation = (RecordValidationType) CommonsUtilities.populateRecordCodeValidation(null, PropertiesConstants.HOLDER_MASSIVE_UNEXPECTED_ERROR, null);
			validation.setLineNumber(holderRow.getRowNumber());
			recordValidationType = validation;
		}
		
		
		return recordValidationType;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateMcnProcessFileTx(HolderMassiveFile holderMassiveFile, LoggerUser loggerUser) {
		StringBuilder stringSQL = new StringBuilder();
		stringSQL.append("UPDATE HolderMassiveFile ");
		stringSQL.append(" SET rejectedFile = :rejectedFileParam ");
		stringSQL.append(" , acceptedFile = :acceptedFileParam ");
		stringSQL.append(" , rejectedOperations = :rejectedOperationsParam ");
		stringSQL.append(" , acceptedOperations = :acceptedOperationsParam ");
		stringSQL.append(" , totalOperations = :totalOperationsParam ");
		stringSQL.append(" , processState = :processStateParam ");
		stringSQL.append(" , lastModifyApp = :lastModifyApp ");
		stringSQL.append(" , lastModifyDate = :lastModifyDate ");
		stringSQL.append(" , lastModifyIp = :lastModifyIp ");
		stringSQL.append(" , lastModifyUser = :lastModifyUser ");
		stringSQL.append(" WHERE idHolderMassiveFilePk = :idHolderMassiveFilePkParam ");
		Query queryJpql = em.createQuery(stringSQL.toString());
		queryJpql.setParameter("idHolderMassiveFilePkParam", holderMassiveFile.getIdHolderMassiveFilePk());
		queryJpql.setParameter("rejectedFileParam", holderMassiveFile.getRejectedFile());
		queryJpql.setParameter("acceptedFileParam", holderMassiveFile.getAcceptedFile());
		queryJpql.setParameter("rejectedOperationsParam", holderMassiveFile.getRejectedOperations());
		queryJpql.setParameter("acceptedOperationsParam", holderMassiveFile.getAcceptedOperations());
		queryJpql.setParameter("totalOperationsParam", holderMassiveFile.getTotalOperations());
		queryJpql.setParameter("processStateParam", holderMassiveFile.getProcessState());
		queryJpql.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		queryJpql.setParameter("lastModifyDate", loggerUser.getAuditTime());
		queryJpql.setParameter("lastModifyIp", loggerUser.getIpAddress());
		queryJpql.setParameter("lastModifyUser", loggerUser.getUserName());
		queryJpql.executeUpdate();
	}
	
	public byte[] getAcceptedMassiveFileContent(Long idHolderMassiveFilePk) {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" select mpf.acceptedFile ");
		sbQuery.append(" from HolderMassiveFile mpf where mpf.idHolderMassiveFilePk = :idHolderMassiveFilePk ");
		parameters.put("idHolderMassiveFilePk", idHolderMassiveFilePk);
		return (byte[])findObjectByQueryString(sbQuery.toString(), parameters);
	}

	public byte[] getRejectedMassiveFileContent(Long idHolderMassiveFilePk) {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" select mpf.rejectedFile ");
		sbQuery.append(" from HolderMassiveFile mpf where mpf.idHolderMassiveFilePk = :idHolderMassiveFilePk ");
		parameters.put("idHolderMassiveFilePk", idHolderMassiveFilePk);
		return (byte[])findObjectByQueryString(sbQuery.toString(), parameters);
	}
}
