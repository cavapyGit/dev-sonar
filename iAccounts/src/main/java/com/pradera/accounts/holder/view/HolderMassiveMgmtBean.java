package com.pradera.accounts.holder.view;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.beanio.BeanIOConfigurationException;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.accounts.holder.facade.HolderServiceFacade;
import com.pradera.accounts.holder.massive.facade.HolderMassiveServiceFacade;
import com.pradera.accounts.holder.to.MassiveHolderTO;
import com.pradera.accounts.util.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.massive.type.HolderMassiveFileProcessType;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.common.validation.to.ValidationProcessTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.negotiation.constant.NegotiationConstant;
import com.pradera.model.process.BusinessProcess;

@DepositaryWebBean
@LoggerCreateBean
public class HolderMassiveMgmtBean extends GenericBaseBean{
	
	@EJB
	AccountsFacade accountsFacade;
	
	@EJB
	HolderServiceFacade holderServiceFacade;
	
	@EJB
	HolderMassiveServiceFacade holderMassiveServiceFacade;
	
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	@EJB
    private BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;

	private Boolean disabledCmbParticipant, flagIsParticipant;
	
	/** The massive holder to. */
    private MassiveHolderTO massiveHolderTO;
    
    /** The list participant. */
	private List<Participant> listParticipant;
	
	private GenericDataModel<ProcessFileTO> holderProcessFilesDataModel;
	
	private GenericDataModel<RecordValidationType> errorsDataModel;
	
	private ProcessFileTO tmpHolderProcessFile; // temporal bean on upload
	
	private ProcessFileTO holderProcessFile; // bean sent to Registration Batch 
	
	private String fUploadFileTypes = "XLSX|xlsx|xls|XLS|xml|dat";
	
	private List<ParameterTable> lstHolderType = new ArrayList<ParameterTable>();
	
	@PostConstruct
	private void init() {
		massiveHolderTO = new MassiveHolderTO();
		getLstParticipant();
		massiveHolderTO.setProcessDate(CommonsUtilities.currentDate());

		tmpHolderProcessFile = new ProcessFileTO();
		tmpHolderProcessFile.setProcessDate(CommonsUtilities.currentDate());
		
		if (validateIsParticipant()) {
			disabledCmbParticipant = Boolean.TRUE;
			massiveHolderTO.setParticipantSelected(userInfo.getUserAccountSession().getParticipantCode());
			tmpHolderProcessFile.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());			
		} else {
			disabledCmbParticipant = Boolean.FALSE;
			massiveHolderTO.setParticipantSelected(null);
		}
		
		lstHolderType = new ArrayList<ParameterTable>();			
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.PERSON_TYPE.getCode());
		
		try {
			lstHolderType = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void getLstParticipant() {
		try {
			Participant filter = new Participant();
			filter.setState(ParticipantStateType.REGISTERED.getCode());
			listParticipant = accountsFacade.getLisParticipantServiceBean(filter);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	public Boolean validateIsParticipant() {
		flagIsParticipant = Boolean.FALSE;
		if(userInfo.getUserAccountSession().isParticipantInstitucion() || userInfo.getUserAccountSession().isInstitution(InstitutionType.AFP)
				|| userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
			flagIsParticipant = Boolean.TRUE;	
		}		
		return flagIsParticipant;
	}
	
	public void onChangeProcessDate() {
		holderProcessFilesDataModel = null;
		tmpHolderProcessFile = new ProcessFileTO();	
		holderProcessFile = null;
		if (validateIsParticipant()) {
			disabledCmbParticipant = Boolean.TRUE;
			tmpHolderProcessFile.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());			
		} 
	}
	
	/**
	 * Search processed files.
	 */
	public void searchProcessedFiles() {
		if(Validations.validateIsNotNullAndPositive(massiveHolderTO.getParticipantSelected())){
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("processDate", massiveHolderTO.getProcessDate());
			parameters.put("processType", HolderMassiveFileProcessType.HOLDER_MASSIVE_UPLOAD.getCode());
			parameters.put("idParticipantPk", massiveHolderTO.getParticipantSelected());
			List<ProcessFileTO> mcnProcessFiles = holderServiceFacade.getHolderMassiveProcessedFilesInformation(parameters);
			holderProcessFilesDataModel = new GenericDataModel<ProcessFileTO>(
					mcnProcessFiles);
		} else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
					PropertiesUtilities.getMessage(PropertiesConstants.HOLDER_MASSIVE_PARTICIPANT_NULL));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	public void cleanAll() {
		JSFUtilities.hideGeneralDialogues();
		JSFUtilities.setValidViewComponentAndChildrens(":frmMassiveHolders");
		massiveHolderTO = new MassiveHolderTO();
		massiveHolderTO.setProcessDate(CommonsUtilities.currentDate());
		
		tmpHolderProcessFile = new ProcessFileTO();
		tmpHolderProcessFile.setProcessDate(CommonsUtilities.currentDate());
		if (validateIsParticipant()) {
			disabledCmbParticipant = Boolean.TRUE;
			massiveHolderTO.setParticipantSelected(userInfo.getUserAccountSession().getParticipantCode());
			tmpHolderProcessFile.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());			
		}
		holderProcessFile = null;
		holderProcessFilesDataModel = null;
	}
	
	public void fileUploadHandler(FileUploadEvent event) {
		try {
			String fDisplayName = fUploadValidateFile(event.getFile(), null, null,fUploadFileTypes);
			if (fDisplayName != null) {
				tmpHolderProcessFile.setFileName(fDisplayName);
				tmpHolderProcessFile.setProcessFile(event.getFile().getContents());
				tmpHolderProcessFile.setTempProcessFile(File.createTempFile("tempMcnfile", ".tmp"));
				FileUtils.writeByteArrayToFile(tmpHolderProcessFile.getTempProcessFile(), tmpHolderProcessFile.getProcessFile());
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	public void removefileHandler(ActionEvent actionEvent) {
		tmpHolderProcessFile.setProcessFile(null);
		tmpHolderProcessFile.setFileName(null);
		if(tmpHolderProcessFile.getTempProcessFile()!=null){
			tmpHolderProcessFile.getTempProcessFile().delete();
			tmpHolderProcessFile.setTempProcessFile(null);
		}
	}
	
	public StreamedContent getMcnStreamContent(ProcessFileTO holderFile) throws IOException{
		if(holderFile!=null){
			
			InputStream inputStream = new ByteArrayInputStream(holderFile.getProcessFile());
			StreamedContent streamedContentFile = new DefaultStreamedContent(inputStream,null, holderFile.getFileName());
			inputStream.close();

			return streamedContentFile;
		}
		return null;
	}
	
	public void validateHolderFile() 
			throws BeanIOConfigurationException, IllegalArgumentException, IOException, ServiceException, ParserConfigurationException {
		JSFUtilities.setValidViewComponentAndChildrens(":frmMassiveHolders");
		int countValidationErrors = 0;
		
		if(Validations.validateIsNullOrNotPositive(tmpHolderProcessFile.getIdParticipantPk())){
    		JSFUtilities.addContextMessage("frmMassiveHolders:fuplMcnFile",
    				FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getMessage(PropertiesConstants.HOLDER_MASSIVE_PARTICIPANT_NULL), 
    				PropertiesUtilities.getMessage(PropertiesConstants.HOLDER_MASSIVE_PARTICIPANT_NULL));
			countValidationErrors++;
    	}
		
		if(Validations.validateIsNull(tmpHolderProcessFile.getTempProcessFile())){
    		JSFUtilities.addContextMessage("frmMassiveHolders:fuplMcnFile",
    				FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getMessage(PropertiesConstants.HOLDER_MASSIVE_FILE_NULL), 
    				PropertiesUtilities.getMessage(PropertiesConstants.HOLDER_MASSIVE_FILE_NULL));
			countValidationErrors++;
    	}
		
		if(countValidationErrors > 0) {
    		showMessageOnDialog(null,null,PropertiesConstants.MESSAGE_DATA_REQUIRED,null);
    		JSFUtilities.showComponent("cnfMsgRequiredValidation");
    		return;
    	}
		
		tmpHolderProcessFile.setRootTag(NegotiationConstant.MASSIVE_XML_BBV_ROOT_TAG);
		tmpHolderProcessFile.setStreamFileDir(NegotiationConstant.HOLDER_MASSIVE_FILE_STREAM);
		
		tmpHolderProcessFile.setInterfaceName(ComponentConstant.INTERFACE_HOLDER_MASSIVE_FOLDER);
		tmpHolderProcessFile.setValidationProcessTO(new ValidationProcessTO());
		if(CommonsUtilities.validateFileOperationName(tmpHolderProcessFile, ComponentConstant.MASSIVE_HOLDER_MASSIVE_PREFIX, ComponentConstant.MASSIVE_HOLDER_MASSIVE_SUFFIX)) {
			Boolean excelValid = this.holderMassiveServiceFacade.holderMassiveConvertXLStoXML(tmpHolderProcessFile, lstHolderType);
			if(excelValid)
				CommonsUtilities.validateFileOperation(tmpHolderProcessFile, ComponentConstant.MASSIVE_HOLDER_MASSIVE_PREFIX, ComponentConstant.MASSIVE_HOLDER_MASSIVE_SUFFIX);
		}
		
		ValidationProcessTO mcnValidationTO = tmpHolderProcessFile.getValidationProcessTO();
		
		errorsDataModel = new GenericDataModel<RecordValidationType>(mcnValidationTO==null?new ArrayList<RecordValidationType>():mcnValidationTO.getLstValidations());
		holderProcessFile = tmpHolderProcessFile;
		tmpHolderProcessFile = new ProcessFileTO();
		if (validateIsParticipant()) {
			disabledCmbParticipant = Boolean.TRUE;
			tmpHolderProcessFile.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());			
		}
	}
	
	public void beforeProcessMcnFile(){			
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
				PropertiesUtilities.getMessage(PropertiesConstants.HOLDER_MASSIVE_REGISTER_CONFIRM));
		JSFUtilities.executeJavascriptFunction("PF('cnfwProcessMassiveHoldersFile').show();");
	}
	
	@LoggerAuditWeb
	public void processMassiveFile(){	
		
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.NEW_HOLDER_MASSIVE_CREATION.getCode());
		
		try {
			Map<String, Object> details = new HashMap<String, Object>();
			details.put(GeneralConstants.PARAMETER_FILE, holderProcessFile.getTempProcessFile().getAbsolutePath());
			details.put(GeneralConstants.PARAMETER_STREAM, holderProcessFile.getStreamFileDir());
			details.put(GeneralConstants.PARAMETER_LOCALE, "es"); //JSFUtilities.getCurrentLocale().toString());
			details.put(GeneralConstants.PARAMETER_FILE_NAME, holderProcessFile.getFileName());
			details.put(GeneralConstants.PARTICIPANT_CODE, holderProcessFile.getIdParticipantPk());
			
			batchProcessServiceFacade.registerBatch(
					userInfo.getUserAccountSession().getUserName(),businessProcess,details);
			
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
				PropertiesUtilities.getMessage(PropertiesConstants.HOLDER_MASSIVE_REGISTER_SUCCESS));
		JSFUtilities.showSimpleValidationDialog();
		cleanAll();
	}
	
	public StreamedContent getAcceptedFileContent(ProcessFileTO holderMassiveFile) {
		try {
			
			byte[] file = holderMassiveServiceFacade.getAcceptedMassiveFileContent(holderMassiveFile.getIdProcessFilePk());
			
			if(Validations.validateIsNull(file)) {
				throw new Exception("No existe archivo de aceptados");
			}
			
			return getStreamedContentFromFile(file, null,
					"accepted_files_"+holderMassiveFile.getFileName().substring(0, holderMassiveFile.getFileName().indexOf("."))+".txt");
		} catch (Exception e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getMessage(PropertiesConstants.HOLDER_MASSIVE_ERROR_DOWNLOADING_FILE));
			JSFUtilities.showSimpleValidationDialog();
		}
		return null;
	}
	
	public StreamedContent getRejectedFileContent(ProcessFileTO holderMassiveFile) {
		try {
			
			byte[] file = holderMassiveServiceFacade.getRejectedMassiveFileContent(holderMassiveFile.getIdProcessFilePk());
			
			if(Validations.validateIsNull(file)) {
				throw new Exception("No existe archivo de error");
			}
			return getStreamedContentFromFile(file, null,
					"rejected_files_"+holderMassiveFile.getFileName().substring(0, holderMassiveFile.getFileName().indexOf("."))+".txt");
		} catch (Exception e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getMessage(PropertiesConstants.HOLDER_MASSIVE_ERROR_DOWNLOADING_FILE));
			JSFUtilities.showSimpleValidationDialog();
		}
		return null;
	}
	/**
	 * Getter and Setters
	 */

	public Boolean getDisabledCmbParticipant() {
		return disabledCmbParticipant;
	}

	public void setDisabledCmbParticipant(Boolean disabledCmbParticipant) {
		this.disabledCmbParticipant = disabledCmbParticipant;
	}

	public MassiveHolderTO getMassiveHolderTO() {
		return massiveHolderTO;
	}

	public void setMassiveHolderTO(MassiveHolderTO massiveHolderTO) {
		this.massiveHolderTO = massiveHolderTO;
	}

	public List<Participant> getListParticipant() {
		return listParticipant;
	}

	public void setListParticipant(List<Participant> listParticipant) {
		this.listParticipant = listParticipant;
	}

	public Boolean getFlagIsParticipant() {
		return flagIsParticipant;
	}

	public void setFlagIsParticipant(Boolean flagIsParticipant) {
		this.flagIsParticipant = flagIsParticipant;
	}

	public GenericDataModel<ProcessFileTO> getHolderProcessFilesDataModel() {
		return holderProcessFilesDataModel;
	}

	public void setHolderProcessFilesDataModel(GenericDataModel<ProcessFileTO> holderProcessFilesDataModel) {
		this.holderProcessFilesDataModel = holderProcessFilesDataModel;
	}

	public ProcessFileTO getTmpHolderProcessFile() {
		return tmpHolderProcessFile;
	}

	public void setTmpHolderProcessFile(ProcessFileTO tmpHolderProcessFile) {
		this.tmpHolderProcessFile = tmpHolderProcessFile;
	}

	public ProcessFileTO getHolderProcessFile() {
		return holderProcessFile;
	}

	public void setHolderProcessFile(ProcessFileTO holderProcessFile) {
		this.holderProcessFile = holderProcessFile;
	}

	public String getfUploadFileTypes() {
		return fUploadFileTypes;
	}

	public void setfUploadFileTypes(String fUploadFileTypes) {
		this.fUploadFileTypes = fUploadFileTypes;
	}

	public GenericDataModel<RecordValidationType> getErrorsDataModel() {
		return errorsDataModel;
	}

	public void setErrorsDataModel(GenericDataModel<RecordValidationType> errorsDataModel) {
		this.errorsDataModel = errorsDataModel;
	}
	
}
