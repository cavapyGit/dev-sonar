package com.pradera.accounts.holder.view;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.apache.deltaspike.core.api.resourceloader.InjectableResource;
import org.omnifaces.util.Faces;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import com.pradera.accounts.holder.facade.HolderServiceFacade;
import com.pradera.accounts.holder.to.HolderRequestTO;
import com.pradera.accounts.holder.to.PreviousModificationTO;
import com.pradera.accounts.investorType.facade.InvestorTypeFacade;
import com.pradera.accounts.investorType.to.InvestorTypeModelTO;
import com.pradera.accounts.util.view.PropertiesConstants;
import com.pradera.accounts.webservices.ServiceAPIClients;
import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.OperationUserTO;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.RegistryType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.extension.view.LegalRepresentativeHelperBean;
import com.pradera.core.component.utils.ClientInformationTO;
import com.pradera.core.component.utils.ClientInformationValidator;
import com.pradera.core.component.utils.DocumentTO;
import com.pradera.core.component.utils.DocumentValidator;
import com.pradera.core.component.utils.PersonTO;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.HolderFile;
import com.pradera.model.accounts.HolderHistory;
import com.pradera.model.accounts.HolderReqFileHistory;
import com.pradera.model.accounts.HolderRequest;
import com.pradera.model.accounts.InstitutionInformation;
import com.pradera.model.accounts.LegalRepresentative;
import com.pradera.model.accounts.LegalRepresentativeFile;
import com.pradera.model.accounts.LegalRepresentativeHistory;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.PepPerson;
import com.pradera.model.accounts.RepresentativeFileHistory;
import com.pradera.model.accounts.type.AgeLessNationalOrForeignResidentDocHolderNatType;
import com.pradera.model.accounts.type.AgeMajorDomOrForeignResidentDocHolderNatType;
import com.pradera.model.accounts.type.AnnularMotivesHolderRequestType;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.EconomicActivityType;
import com.pradera.model.accounts.type.HolderFileStateType;
import com.pradera.model.accounts.type.HolderReqFileHisType;
import com.pradera.model.accounts.type.HolderRequestStateType;
import com.pradera.model.accounts.type.HolderRequestType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.JuridicClassType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.accounts.type.PepMotiveType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.accounts.type.RejectMotivesHolderRequestType;
import com.pradera.model.accounts.type.RepresentativeClassType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.GeographicLocationStateType;
import com.pradera.model.generalparameter.type.GeographicLocationType;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.report.ReportLoggerFile;




// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ModifyHolderMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/08/2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class ModifyHolderMgmtBean extends GenericBaseBean implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The log. */
	Logger log = Logger.getLogger(ModifyHolderMgmtBean.class.getName());

	/** The holder request. */
	private HolderRequest holderRequest;
	
	/** The holder request selected. */
	private HolderRequest holderRequestSelected;
	
	/** The holder request detail. */
	private HolderRequest holderRequestDetail;
	
	/** The holder request detail before. */
	private HolderRequest holderRequestDetailBefore;
	
	/** The pep person. */
	private PepPerson pepPerson;
	
	/** The holder history. */
	private HolderHistory holderHistory;
	
	/** The holder history before. */
	private HolderHistory holderHistoryBefore;
	
	/** The holder history detail. */
	private HolderHistory holderHistoryDetail;
	
	/** The holder history detail before. */
	private HolderHistory holderHistoryDetailBefore;
	
	/** The holder filter. */
	private HolderRequestTO holderFilter;
	
	/** The participant. */
	private Participant participant;
	
	/** The participant detail. */
	private Participant participantDetail;
	
	/** The participant requester detail. */
	private Participant participantRequesterDetail;
	
	/** The participant detail before. */
	private Participant participantDetailBefore;
	
	/** The participant requester. */
	private Participant participantRequester = new Participant();
	/** The holder. */
	private Holder holder;
	
	/** The show panel options component. */
	private boolean showPanelOptionsComponent;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	private UserPrivilege userPrivilege;
	
	/** The country residence. */
	@Inject @Configurable Integer countryResidence;
	
	/** The department issuance. */
	@Inject @Configurable Integer departmentIssuance;
	
	/** The province default. */
	@Inject @Configurable Integer provinceDefault;
	
	/** The department default. */
	@Inject @Configurable Integer departmentDefault;
	
	/** The holder request data model. */
	private HolderRequestDataModel holderRequestDataModel;
	
	/** The legal representative history array. */
	private LegalRepresentativeHistory[] legalRepresentativeHistoryArray;

	/** The legal representative array. */
	private LegalRepresentative[] legalRepresentativeArray;

	/** The flag copy legal holder. */
	private boolean flagCopyLegalHolder;
	
	/** The flag holder natural person. */
	private boolean flagHolderNaturalPerson;
	
	/** The flag holder juridic person. */
	private boolean flagHolderJuridicPerson;
	
	/** The flag minor. */
	private boolean flagMinor;
	
	/** The disabled country resident holder. */
	private boolean disabledCountryResidentHolder;
	
	/** The disabled country resident representative. */
	private boolean disabledCountryResidentRepresentative;
	
	/** The disabled representative button. */
	private boolean disabledRepresentativeButton;
	
	/** The flag is pep. */
	private boolean flagIsPEP;
	
	/** The list participant. */
	private List<Participant> listParticipant;

	/** The list juridic class. */
	private List<ParameterTable> listJuridicClass;

	/** The list person type. */
	private List<ParameterTable> listPersonType;

	/** The list boolean. */
	private List<BooleanType> listBoolean;
	
	
	/** The list motives. */
	private List<ParameterTable> listMotives;
	
	/** The list state holder request. */
	private List<ParameterTable> listStateHolderRequest;

		
	/** The list document type. */
	private List<ParameterTable> listDocumentType;
	
	/** The list document type result. */
	private List<ParameterTable> listDocumentTypeResult;
	
	/** The list document type legal. */
	private List<ParameterTable> listDocumentTypeLegal;
	
	/** The list document type legal result. */
	private List<ParameterTable> listDocumentTypeLegalResult;
	
	/** The list second document type. */
	private List<ParameterTable> listSecondDocumentType;

    /** The list category pep. */
    private List<ParameterTable> listCategoryPep;
	
	/** The list role pep. */
	private List<ParameterTable> listRolePep;
	
	/** The list sex. */
	private List<ParameterTable> listSex;

	/** The list representative class. */
	private List<ParameterTable> listRepresentativeClass;

	/** The list geographic location. */
	private List<GeographicLocation> listGeographicLocation;
	
	/** The list geographic location without national country. */
	private List<GeographicLocation> listGeographicLocationWithoutNationalCountry;
	
	/** The list geographic location exception. */
	private List<GeographicLocation> listGeographicLocationException;

	/** The list province location. */
	private List<GeographicLocation> listProvinceLocation;
	
	/** The list province location before. */
	private List<GeographicLocation> listProvinceLocationBefore;

	/** The list department location before. */
	private List<GeographicLocation> listDepartmentLocationBefore;
	
	/** The list municipality location before. */
	private List<GeographicLocation> listMunicipalityLocationBefore;

	/** The list province postal location. */
	private List<GeographicLocation> listProvincePostalLocation;
	
	/** The list province postal location before. */
	private List<GeographicLocation> listProvincePostalLocationBefore;

		/** The list province location representative. */
	private List<GeographicLocation> listProvinceLocationRepresentative;

	/** The list province postal location representative. */
	private List<GeographicLocation> listProvincePostalLocationRepresentative;

	/** The list department postal location before. */
	private List<GeographicLocation> listDepartmentPostalLocationBefore;

	/** The disabled cmb postal country. */
	private boolean disabledCmbPostalCountry;

	/** The disabled cmb postal province. */
	private boolean disabledCmbPostalProvince;

	/** The disabled cmb postal sector. */
	private boolean disabledCmbPostalSector;

	/** The disabled input postal address. */
	private boolean disabledInputPostalAddress;

	/** The disabled cmb postal country representative. */
	private boolean disabledCmbPostalCountryRepresentative;

	/** The disabled cmb postal province representative. */
	private boolean disabledCmbPostalProvinceRepresentative;

	/** The disabled cmb postal sector representative. */
	private boolean disabledCmbPostalSectorRepresentative;

	/** The disabled input postal address representative. */
	private boolean disabledInputPostalAddressRepresentative;

	/** The disabled Mnemonic ASFI and FONDO. */
	private boolean flagSendASFI;
	
	/** The general pameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;

	/** The holder service facade. */
	@EJB
	HolderServiceFacade holderServiceFacade;

	@EJB
	ServiceAPIClients serviceAPIClients;
	
	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
	/** The participant service bean. */
	@EJB ParticipantServiceBean participantServiceBean;
	
	@EJB
	InvestorTypeFacade investorTypeFacade;

	/** The legal representative history detail. */
	private LegalRepresentativeHistory legalRepresentativeHistoryDetail;
	
	/** The lst parameter archive header. */
	private List<ParameterTable> lstParameterArchiveHeader;

	/** The lst holder req file history. */
	private List<HolderReqFileHistory> lstHolderReqFileHistory;
	
	/** The lst holder file. */
	private List<HolderFile> lstHolderFile;
	
	/** The lst legal representative file. */
	private List<LegalRepresentativeFile> lstLegalRepresentativeFile;

	/** The lst representative file history. */
	private List<RepresentativeFileHistory> lstRepresentativeFileHistory;

	/** The flag cmb representative class. */
	private boolean flagCmbRepresentativeClass;

	/** The file. */
	private UploadedFile file;

	
	/** The flag is participant. */
	private boolean flagIsParticipant;

	/** The flag action confirm. */
	private boolean flagActionConfirm;

	/** The flag action reject. */
	private boolean flagActionReject;

	/** The flag action annular. */
	private boolean flagActionAnnular;
	
	/** The flag action approve. */
	private boolean flagActionApprove;

	/** The disabled cmb participant. */
	private boolean disabledCmbParticipant;
	
	
	/** The disabled cmb participant requester. */
	private boolean disabledCmbParticipantRequester;
	
	/** The show motive text. */
	private boolean showMotiveText;

	/** The request motive. */
	private Integer requestMotive;
	
	/** The request other motive. */
	private String requestOtherMotive;
	
	/** The numeric type document holder. */
    private boolean numericTypeDocumentHolder;
	
	/** The numeric second type document holder. */
	private boolean numericSecondTypeDocumentHolder;
	
	/** The numeric type document legal. */
	private boolean numericTypeDocumentLegal;
	
	/** The numeric second type document legal. */
	private boolean numericSecondTypeDocumentLegal;
	
	/** The max lenght document number holder. */
	private Integer maxLenghtDocumentNumberHolder;
	
	/** The max lenght second document number holder. */
	private Integer maxLenghtSecondDocumentNumberHolder;
	
	/** The max lenght document number legal. */
	private Integer maxLenghtDocumentNumberLegal;
	
	/** The max lenght second document number legal. */
	private Integer maxLenghtSecondDocumentNumberLegal;
	
    /** The flag holder indicator inability. */
    private boolean flagHolderIndicatorInability;
	
	/** The flag indicator minor holder. */
	private boolean flagIndicatorMinorHolder;
	
	/** The flag national resident. */
	private boolean flagNationalResident;
	
	/** The flag national not resident. */
	private boolean flagNationalNotResident;
	
	/** The flag foreign resident. */
	private boolean flagForeignResident;
	
	/** The flag foreign not resident. */
	private boolean flagForeignNotResident;
	
	/** The list economic sector. */
	private List<ParameterTable> listEconomicSector;

	/** The list economic activity. */
	private List<ParameterTable> listEconomicActivity;
	
	private List<InvestorTypeModelTO> listInvestorType;
		
	/** The previous modification to. */
	private PreviousModificationTO previousModificationTO;
	
	/** The lst holder req file history detail. */
	private List<HolderReqFileHistory> lstHolderReqFileHistoryDetail;
	
	/** The lst holder req file history detail before. */
	private List<HolderReqFileHistory> lstHolderReqFileHistoryDetailBefore;
	
	/** The btn modify view. */
	private boolean btnModifyView;
	
	/** The btn confirm view. */
	private boolean btnConfirmView;
	
	/** The btn reject view. */
	private boolean btnRejectView;		
	
	/** The btn approve view. */
	private boolean btnApproveView;		
	
	/** The btn annular view. */
	private boolean btnAnnularView;
	
    /** The nit result find. */
    private InstitutionInformation nitResultFind;
    
    /** The disabled ind resident. */
    private boolean disabledIndResident = false;
	
	/** The disabled nationality. */
	private boolean disabledNationality = false;
	
	/** The list holder document type. */
	private List<ParameterTable> listHolderDocumentType;
	
	/** The list representative document type. */
	private List<ParameterTable> listRepresentativeDocumentType;
	
	/** The attach file holder selected. */
	private Integer attachFileHolderSelected;
	
	/** The attach file representative selected. */
	private Integer attachFileRepresentativeSelected;
	
	/** The holder file name display. */
	private String holderFileNameDisplay;
	
    /** The flag export data holder. */
	private boolean flagExportDataHolder;
	
	/** The flag export data representative. */
	private boolean flagExportDataRepresentative;
	
	/** The list second document type legal. */
	private List<ParameterTable> listSecondDocumentTypeLegal;
	
	/** The flag active country resident. */
	private boolean flagActiveCountryResident;
	
	/** The flag active document number. */
	private boolean flagActiveDocumentNumber;
	
	/** The flag active second document number. */
	private boolean flagActiveSecondDocumentNumber;
	
	/** The notification service facade. */
	@EJB NotificationServiceFacade notificationServiceFacade;	
	
	/** The message configuration. */
	@Inject
	@InjectableResource(location = "Messages_es.properties")
	private Properties messageConfiguration;
	
    /** The business process. */
    BusinessProcess businessProcess = new BusinessProcess();
	
    /** The legal representative helper bean. */
    @Inject
    private LegalRepresentativeHelperBean legalRepresentativeHelperBean;
	
    /** The streamed content file. */
    private transient StreamedContent streamedContentFile;
    
    /** The document validator. */
    @Inject
    DocumentValidator documentValidator;
   
    /** The disabled cmb postal departament. */
    private boolean disabledCmbPostalDepartament;
    
    /** The disabled cmb postal municipality. */
    private boolean disabledCmbPostalMunicipality;
    
    /** The list department postal location. */
    private List<GeographicLocation> listDepartmentPostalLocation;
    
    /** The list municipality postal location. */
    private List<GeographicLocation> listMunicipalityPostalLocation;

    /** The list department location. */
    private List<GeographicLocation> listDepartmentLocation;

    /** The list municipality location. */
    private List<GeographicLocation> listMunicipalityLocation;

    /** The list municipality postal location before. */
    private List<GeographicLocation> listMunicipalityPostalLocationBefore;
    
    /** The request state description. */
    private Map<Integer,String> requestStateDescription = null;
	
    /** The document type description. */
    private Map<Integer,String> documentTypeDescription = null;
    
    /** The holder state description. */
    private Map<Integer,String> holderStateDescription = null;
	
//    /** The disabled doc number bank correlative. */
//    private boolean disabledDocNumberBankCorrelative;
//    
    /** The list emission source. */
    private List<ParameterTable> listDocumentSource;
	
    /** The list document issuance date. */
    private LinkedHashMap<String,Integer>listDocumentIssuanceDate;
    
    /** The document to. */
    private DocumentTO documentTO;
    
	/** The file download. */
	private transient StreamedContent fileDownload; 
    
	/** The client information validator. */
	@Inject
    ClientInformationValidator clientInformationValidator;
	
	/** The client information to. */
	private ClientInformationTO clientInformationTO;
	    
	/** The list civil status. */
	private List<ParameterTable> listCivilStatus;
	    
	/** The list total income. */
	private List<ParameterTable> listTotalIncome;
	
	/** The file photo holder. */
    private transient StreamedContent filePhotoHolder, fileSigning;
    
    /** The photo holder name. */
	private String photoHolderName, signingHolderName;
	
	/** The file photo temporal. */
    private byte[] filePhotoTemp, fileSigningTemp;
    
    /** The aux fupl photo. */
    private boolean auxFuplPhoto, auxFuplSigning;
    
    /** The form state. */
    private boolean formState;
    
    /** The flag cui related. */
    private boolean flagCUIRelated;
    
    /** The flag nemonic fund. */
    private boolean flagNemonicFund;
    
    /** The flag nemonic asfi. */
    private boolean flagNemonicAsfi;
	
    /** The disabled nemonic asfi. */
    private boolean disabledNemonic;
    
    /** The flag transfer number. */
    private boolean flagTransferNumber;
    
    /** The disabled transfer number. */
    private boolean disabledTransferNumber;
    
    /** The flag is issuer dpf institution. */
    private boolean flagIsIssuerDpfInstitution;
    
    /** The new document number. */
	private String newDocumentNumber;
	
	/** The new document number. */
	private String newDocumentNumberSecondNationality;
	    
    private boolean indChangesNeedsConfirm;
    
    private List<ParameterTable> listHolderChangesMotivesDocument;
    
    private Integer attachFileHolderChangesMotivesSelected;
    
    private List<HolderReqFileHistory> lstHolderReqChangesFileHistory;
    
    private String holderFileChangesNameDisplay;
        
    /** The lst holder req file history detail. */
    private List<HolderReqFileHistory> lstHolderReqChangesMotiveFileHistoryDetail;
    
    /** The lst holder req file history detail before. */
    private List<HolderReqFileHistory> lstHolderReqChangesMotiveFileHistoryDetailBefore;
    
    private List<ParameterTable> listPepGradeWithPersonType;
    
    private List<ParameterTable> listPepGrade;
    
    private List<ParameterTable> listEconomicActivityNaturalPerson;
    
    private final Integer linkForFamily = PepMotiveType.LINK_FOR_FAMILY.getCode();
                    
	/**
	 * Instantiates a new modify holder mgmt bean.
	 */
	public ModifyHolderMgmtBean() {
		documentTO = new DocumentTO();
		holderFilter = new HolderRequestTO();
		holderRequest = new HolderRequest();
		holder = new Holder();
		requestStateDescription = new HashMap<Integer,String>();
		documentTypeDescription = new HashMap<Integer,String>();
		holderStateDescription = new HashMap<Integer,String>();
		listDocumentIssuanceDate = new LinkedHashMap<String,Integer>();
		clientInformationTO = new ClientInformationTO();
		participantRequester = new Participant();
		newDocumentNumber = null;
		newDocumentNumberSecondNationality = null;
	}

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {

		try {
			flagSendASFI = false;
			
		if (!FacesContext.getCurrentInstance().isPostback()) {
			holderFilter.setInitialDate(CommonsUtilities.currentDate());
			holderFilter.setEndDate(CommonsUtilities.currentDate());
			getLstParticipant();
			getLstStateHolderRequestType();
			holderFilter.setParticipantSelected(Long.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER));
			nitResultFind = null;
			
			if (validateIsParticipant()) {
				disabledCmbParticipant = true;
				holderFilter.setParticipantSelected(userInfo.getUserAccountSession().getParticipantCode());
				Participant filter = new Participant();
				filter.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
				listParticipant = accountsFacade.getLisParticipantServiceBean(filter);	
			} else {
				// Add validations participant DPF
				flagIsIssuerDpfInstitution = false;
				Long idParticipantCode = getIssuerDpfInstitution(userInfo);			
				if(Validations.validateIsNotNullAndNotEmpty(idParticipantCode)){
					disabledCmbParticipant = true;
					flagIsIssuerDpfInstitution = true;	
					holderFilter.setParticipantSelected(idParticipantCode);			
				} else {
					if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
						disabledCmbParticipant = true;
					} else {
						disabledCmbParticipant = false;
					}				
					flagIsIssuerDpfInstitution = false;	
					holderFilter.setParticipantSelected(null);
				}				
			}
			
			ParameterTableTO  parameterTableTO = new ParameterTableTO();
			
			parameterTableTO.setMasterTableFk(MasterTableType.STATE_CREATION_HOLDER_REQUEST.getCode());
			for(ParameterTable param : generalParametersFacade.getListParameterTableServiceBean(parameterTableTO)){
				requestStateDescription.put(param.getParameterTablePk(), param.getParameterName());
			}
			
			parameterTableTO.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
			for(ParameterTable param : generalParametersFacade.getListParameterTableServiceBean(parameterTableTO)) {
				documentTypeDescription.put(param.getParameterTablePk(), param.getParameterName());
			}
			
			parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_STATE_TYPE.getCode());
			for(ParameterTable param : generalParametersFacade.getListParameterTableServiceBean(parameterTableTO)){
				holderStateDescription.put(param.getParameterTablePk(), param.getParameterName());
			}
			
			Date today = new Date();
			DateFormat df = new SimpleDateFormat("yyyy");
			int year = Integer.parseInt(df.format(today));
			
			for(int i=1900;i<=year;i++){
					listDocumentIssuanceDate.put(String.valueOf(i),i);
			}
			
			getLstCivilStatus();
			getLstTotalIncome();
			
		}
		}
		catch(Exception e){
			e.printStackTrace();
		}

	}
	
	
	/**
	 * Gets the lst civil status.
	 *
	 * @return the lst civil status
	 */
	public void getLstCivilStatus(){
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(MasterTableType.CIVIL_STATUS
							.getCode());

			listCivilStatus = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}		
	}

	/**
	 * Gets the lst total income.
	 *
	 * @return the lst total income
	 */
	public void getLstTotalIncome(){
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(MasterTableType.TOTAL_INCOME
							.getCode());

			listTotalIncome = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}		
	}

	/**
	 * Flag actions in false.
	 */
	public void flagActionsInFalse() {
		flagActionAnnular = false;
		flagActionApprove = false;
		flagActionConfirm = false;
		flagActionReject = false;
	}
	
	/**
	 * Listener participant requester.
	 */
	public void listenerParticipantRequester(){
		if(participantRequester!=null && participantRequester.getIdParticipantPk()==null){
			cleanAll();
		}
	}
	
	/**
	 * Validate is participant.
	 *
	 * @return true, if successful
	 */
	public boolean validateIsParticipant() {
		
		flagIsParticipant = false;
		
		if(userInfo.getUserAccountSession().isParticipantInstitucion() 
				||  userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
			flagIsParticipant = true;	
		}		
		
		return flagIsParticipant;

	}	
	
	/**
	 * Sets the buttons listener.
	 */
	public void setButtonsListener(){
		
		if (!FacesContext.getCurrentInstance().isPostback()) {
		
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		
		if(!getViewOperationType().equals(ViewOperationsType.DETAIL.getCode())){
				
		if(holderRequestDetail.getStateHolderRequest().equals(HolderRequestStateType.REGISTERED.getCode())){		
		
			if(getViewOperationType().equals(ViewOperationsType.APPROVE.getCode())){
				privilegeComponent.setBtnApproveView(true);		
			}
			
			if(getViewOperationType().equals(ViewOperationsType.ANULATE.getCode())){
				privilegeComponent.setBtnAnnularView(true);
			}
				
			
		}
		
		else if(holderRequestDetail.getStateHolderRequest().equals(HolderRequestStateType.MODIFIED.getCode())){
			
			if(getViewOperationType().equals(ViewOperationsType.APPROVE.getCode())){
				privilegeComponent.setBtnApproveView(true);		
			}
			
			if(getViewOperationType().equals(ViewOperationsType.ANULATE.getCode())){
				privilegeComponent.setBtnAnnularView(true);
			}
		}
		
		else if(holderRequestDetail.getStateHolderRequest().equals(HolderRequestStateType.APPROVED.getCode())){
			
			if(getViewOperationType().equals(ViewOperationsType.CONFIRM.getCode())){
				
			    privilegeComponent.setBtnConfirmView(true);
			}
			
			if(getViewOperationType().equals(ViewOperationsType.REJECT.getCode())){
				privilegeComponent.setBtnRejectView(true);
			}
		
		}
		
		}
		
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	 }
		
	}
	
	/**
	 * Gets the lst economic sector.
	 *
	 * @return the lst economic sector
	 */
	public void getLstEconomicSector() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.ECONOMIC_SECTOR
					.getCode());

			listEconomicSector = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}
	
	/**
	 * Change selected economic sector.
	 */
	public void changeSelectedEconomicSector() {
		try {
			
			Integer economicSector=null;
			
			if(isViewOperationRegister()){
				economicSector = holderHistory.getEconomicSector();
			}
			else{
				economicSector = holderHistoryDetail.getEconomicSector();
			}
			
			listEconomicActivity = generalParametersFacade.getListEconomicActivityBySector(economicSector);
			
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}	
	
	/**
	 * In false flag dominican foreign.
	 */
	public void inFalseFlagNationalForeign(){		
		flagNationalResident=false;
		flagNationalNotResident=false;
		flagForeignResident=false;
		flagForeignNotResident=false;
	}
	
	
	/**
	 * Document attach holder file.
	 *
	 * @param event the event
	 */
	public void documentAttachHolderFile(FileUploadEvent event){
		
		if(attachFileHolderSelected!=null){
			
			String fDisplayName=fUploadValidateFile(event.getFile(),null, null,getfUploadFileDocumentsTypes());
			 
			 if(fDisplayName!=null){
				 holderFileNameDisplay=fDisplayName;
			 }

			 for(int i=0;i<listHolderDocumentType.size();i++){
				if(listHolderDocumentType.get(i).getParameterTablePk().equals(attachFileHolderSelected)){
					
					HolderReqFileHistory holderReqFileHistory = new HolderReqFileHistory();

					holderReqFileHistory
							.setHolderReqFileHisType(HolderReqFileHisType.MODIFICATION
									.getCode());
					holderReqFileHistory
							.setFilename(event.getFile().getFileName());
					holderReqFileHistory
							.setFileHolderReq(event.getFile().getContents());
					holderReqFileHistory.setRegistryUser(userInfo.getUserAccountSession().getUserName());
					holderReqFileHistory
							.setRegistryDate(CommonsUtilities
									.currentDate());
					holderReqFileHistory
							.setHolderRequest(holderRequest);
					holderReqFileHistory
							.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST
									.getCode());
					holderReqFileHistory
							.setStateFile(HolderFileStateType.REGISTERED
									.getCode());
					holderReqFileHistory
							.setDocumentType(listHolderDocumentType.get(i).getParameterTablePk());
					holderReqFileHistory
							.setDescription(listHolderDocumentType.get(i).getParameterName());
					
					
					holderReqFileHistory.setWasModified(true);
					
					
					insertHolderFileItem(holderReqFileHistory);
				}
			}
		}
		
	}
	
    public void documentAttachHolderChangesMotiveFile(FileUploadEvent event) {
        
        if(attachFileHolderChangesMotivesSelected != null) {
                
                String fDisplayName2=fUploadValidateFile(event.getFile(),null, null,getfUploadFileDocumentsTypes());
                 if(fDisplayName2!=null){
                         holderFileChangesNameDisplay=fDisplayName2;
                 }
                
                 for(int i=0;i<listHolderChangesMotivesDocument.size();i++){
                        if(listHolderChangesMotivesDocument.get(i).getParameterTablePk().equals(attachFileHolderChangesMotivesSelected)){
                                
                                HolderReqFileHistory holderReqFileHistory2 = new HolderReqFileHistory();
                                holderReqFileHistory2
                                                .setHolderReqFileHisType(HolderReqFileHisType.MODIFICATION
                                                                .getCode());
                                holderReqFileHistory2
                                                .setFilename(event.getFile().getFileName());
                                holderReqFileHistory2
                                                .setFileHolderReq(event.getFile().getContents());
                                holderReqFileHistory2.setRegistryUser(userInfo.getUserAccountSession().getUserName());
                                holderReqFileHistory2
                                                .setRegistryDate(CommonsUtilities
                                                                .currentDate());
                                holderReqFileHistory2
                                                .setHolderRequest(holderRequest);
                                holderReqFileHistory2
                                                .setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST
                                                                .getCode());
                                holderReqFileHistory2
                                                .setStateFile(HolderFileStateType.REGISTERED
                                                                .getCode());
                                holderReqFileHistory2
                                                .setDocumentType(listHolderChangesMotivesDocument.get(i).getParameterTablePk());
                                holderReqFileHistory2
                                                .setDescription(listHolderChangesMotivesDocument.get(i).getParameterName());
                                
                                
                                holderReqFileHistory2.setWasModified(true);
                                
                                
                                insertHolderChangesFileItem(holderReqFileHistory2);
                        }
                }
        }
        
}	

	/**
	 * Insert or replace holder file item.
	 *
	 * @param holderReqFileHistory the holder req file history
	 */
	public void insertHolderFileItem(HolderReqFileHistory holderReqFileHistory){
	
		
			for(int i=0;i<lstHolderReqFileHistory.size();i++)
			{
		
				if(lstHolderReqFileHistory.get(i).getDocumentType().equals(holderReqFileHistory.getDocumentType())){
					
					Object[] bodyData = new Object[0];
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_ALERT_DOCUMENT_ALREADY_LOADED,
							bodyData));
					JSFUtilities.showSimpleValidationDialog();
					attachFileHolderSelected = null;
					
					return;
				}	
			}
			
			lstHolderReqFileHistory.add(holderReqFileHistory);
			attachFileHolderSelected = null;
    }
    
    /**
     * Insert or replace holder changes file item.
     *
     * @param holderReqFileHistory the holder req file history
     */
    public void insertHolderChangesFileItem(HolderReqFileHistory holderReqFileHistory){
    
            
                    for(int i=0;i<lstHolderReqChangesFileHistory.size();i++)
                    {
            
                            /*if(lstHolderReqChangesFileHistory.get(i).getDocumentType().equals(holderReqFileHistory.getDocumentType())){
                                    
                                    Object[] bodyData = new Object[0];
                                    JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
                                                    PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_ALERT_DOCUMENT_ALREADY_LOADED,
                                                    bodyData));
                                    JSFUtilities.showSimpleValidationDialog();
                                    attachFileHolderSelected = null;
                                    
                                    return;
                            } */       
                    }
                    
                    lstHolderReqChangesFileHistory.add(holderReqFileHistory);
                    attachFileHolderChangesMotivesSelected = null;
	}
	
	
	
	/**
	 * Loading document attach by holder.
	 */
	public void loadingDocumentAttachByHolder() {	
        
        lstHolderReqFileHistory = new ArrayList<HolderReqFileHistory>();
		loadDocumentTypeHolderParameter();
		
	}
	
	/**
	 * Gets the lst document type attach parameter.
	 *
	 * @param masterTableTypeCode the master table type code
	 * @return the lst document type attach parameter
	 */
	public List<ParameterTable> getLstDocumentTypeAttachParameter(Integer masterTableTypeCode){
		List<ParameterTable> lstDocumentTypeAttach = new ArrayList<ParameterTable>();
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(masterTableTypeCode);
			
			
			lstDocumentTypeAttach  =  generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		
		return lstDocumentTypeAttach;
		
	}
	
	/**
	 * Load document type holder parameter.
	 */
	public void loadDocumentTypeHolderParameter(){

		if (flagHolderNaturalPerson) {
			if (!flagIndicatorMinorHolder && !flagHolderIndicatorInability) {
               
				if (flagNationalResident || flagForeignResident) {
					
					listHolderDocumentType = new ArrayList<ParameterTable>();
					
					listHolderDocumentType = getLstDocumentTypeAttachParameter(MasterTableType.AGEMAJOR_BOLIV_OR_FOREIGN_RESIDENT_DOC_HOLDER_NAT.getCode());
					
				}
				if (flagNationalNotResident) {
					
					listHolderDocumentType = new ArrayList<ParameterTable>();
					
					listHolderDocumentType = getLstDocumentTypeAttachParameter(MasterTableType.AGEMAJOR_BOLIV_NO_RESIDENT_DOC_HOLDER_NAT.getCode());
					

				}
				if (flagForeignNotResident) {

					listHolderDocumentType = new ArrayList<ParameterTable>();
					
					listHolderDocumentType = getLstDocumentTypeAttachParameter(MasterTableType.AGEMAJOR_FOREIGN_NO_RESIDENT_DOC_HOLDER_NAT.getCode());
					
				}

			} else if (flagIndicatorMinorHolder && !flagHolderIndicatorInability) {

				if (flagNationalResident || flagForeignResident) {
					listHolderDocumentType = new ArrayList<ParameterTable>();
					
					listHolderDocumentType = getLstDocumentTypeAttachParameter(MasterTableType.AGELESS_BOLIV_OR_FOREIGN_RESIDENT_DOC_HOLDER_NAT.getCode());
				}
				
				if (flagNationalNotResident) {
					listHolderDocumentType = new ArrayList<ParameterTable>();
					
					listHolderDocumentType = getLstDocumentTypeAttachParameter(MasterTableType.AGELESS_BOLIV_NO_RESIDENT_DOC_HOLDER_NAT.getCode());			

				}
				if (flagForeignNotResident) {
					
					listHolderDocumentType = new ArrayList<ParameterTable>();

					listHolderDocumentType = getLstDocumentTypeAttachParameter(MasterTableType.AGELESS_FOREIGN_NO_RESIDENT_DOC_HOLDER_NAT.getCode());			

				}
			} else if (flagHolderIndicatorInability) {

				if (flagNationalResident || flagForeignResident) {

					listHolderDocumentType = new ArrayList<ParameterTable>();

					listHolderDocumentType = getLstDocumentTypeAttachParameter(MasterTableType.UNABLE_BOLIV_OR_FOREIGN_RESIDENT_DOC_HOLDER_NAT.getCode());					

				}
				if (flagNationalNotResident) {
					
					listHolderDocumentType = new ArrayList<ParameterTable>();
					
					listHolderDocumentType = getLstDocumentTypeAttachParameter(MasterTableType.UNABLE_BOLIV_NO_RESIDENT_DOC_HOLDER_NAT.getCode());
					
					
				}
				if (flagForeignNotResident) {
					
					listHolderDocumentType = new ArrayList<ParameterTable>();
					
					listHolderDocumentType = getLstDocumentTypeAttachParameter(MasterTableType.UNABLE_FOREIGN_NO_RESIDENT_DOC_HOLDER_NAT.getCode());
					
				}
			}
		}
		if (flagHolderJuridicPerson) {

			if ((flagNationalResident || flagForeignResident) 
					&&
					holderHistory.getJuridicClass()!=null && 
					!holderHistory.getJuridicClass().equals(JuridicClassType.ENTITY_STATE.getCode()) &&
					!holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())
					) {
				
				listHolderDocumentType = new ArrayList<ParameterTable>();
				
				
				listHolderDocumentType = getLstDocumentTypeAttachParameter(MasterTableType.BOLIV_OR_FOREIGN_RESIDENT_DOC_HOLDER_JUR.getCode());
					
			}

			if (flagForeignNotResident 
					&&
					holderHistory.getJuridicClass()!=null && 
					!holderHistory.getJuridicClass().equals(JuridicClassType.ENTITY_STATE.getCode()) &&
					!holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())
				) {
				
				listHolderDocumentType = new ArrayList<ParameterTable>();
			
				listHolderDocumentType = getLstDocumentTypeAttachParameter(MasterTableType.FOREIGN_NO_RESIDENT_DOC_HOLDER_JUR.getCode());

			}

			if (holderHistory.getJuridicClass()!=null && holderHistory.getJuridicClass().equals(JuridicClassType.ENTITY_STATE.getCode())) {
				
				listHolderDocumentType = new ArrayList<ParameterTable>();
				
				listHolderDocumentType = getLstDocumentTypeAttachParameter(MasterTableType.STATAL_ENTITY_DOC_HOLDER_JUR.getCode());
			}

			if (holderHistory.getJuridicClass()!=null && holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode()) && holderHistory.getIndResidence()!=null && holderHistory.getIndResidence().equals(BooleanType.NO.getCode())) {
				
				listHolderDocumentType = new ArrayList<ParameterTable>();			
				listHolderDocumentType = getLstDocumentTypeAttachParameter(MasterTableType.TRUST_NO_RESIDENT_DOC_HOLDER_JUR.getCode());

			}

			else if (holderHistory.getJuridicClass()!=null && holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())) {
				
				listHolderDocumentType = new ArrayList<ParameterTable>();						
				listHolderDocumentType = getLstDocumentTypeAttachParameter(MasterTableType.TRUST_DOC_HOLDER_JUR.getCode());
				
			}

		}

		listHolderChangesMotivesDocument = getLstDocumentTypeAttachParameter(MasterTableType.MOTIVES_FILES_HOLDER_MODIFICATION.getCode());
	}
	
	/**
	 * Gets the lst document type parameter.
	 *
	 * @param masterTableTypeCode the master table type code
	 * @return the lst document type parameter
	 */
	public void getLstDocumentTypeParameter(Integer masterTableTypeCode){
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(masterTableTypeCode);
			
			listHolderDocumentType = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
			
			

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Value change cmb document holder.
	 */
	public void valueChangeCmbDocumentHolder(){
		holderFileNameDisplay = null;
	}
	
	
		
	/**
	 * Gets the streamed content file holder history.
	 *
	 * @param id the id
	 * @param documentType the document type
	 * @param registryHistorical the registry historical
	 * @return the streamed content file holder history
	 */
	public StreamedContent getStreamedContentFileHolderHistory(Long id,
			Integer documentType,boolean registryHistorical) {

		try {
			HolderReqFileHistory auxHolderReqFileHistory = holderServiceFacade
					.getContentFileHolderHistoryServiceFacade(id, documentType,registryHistorical);
			InputStream inputStream;

			inputStream = new ByteArrayInputStream(auxHolderReqFileHistory.getFileHolderReq());
			streamedContentFile = new DefaultStreamedContent(inputStream, null,auxHolderReqFileHistory.getFilename());
			inputStream.close();

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

		return streamedContentFile;
	}
	
		
	/**
	 * Validate dominican foreign.
	 *
	 * @param idComponent the id component
	 */
	public void validateNationalForeign(String idComponent){
	
		if (!validateSameNationality(idComponent)) {

			inFalseFlagNationalForeign();

			if (holderHistory.getNationality() != null
					&& (holderHistory.getNationality()
							.equals(countryResidence))) {
				
				if (BooleanType.YES.getCode().equals(
						holderHistory.getIndResidence())) {
					flagNationalResident = true;
				} else if (BooleanType.NO.getCode().equals(
						holderHistory.getIndResidence())) {
					flagNationalNotResident = true;
				}

			} else if (holderHistory.getNationality() != null
					&& !(holderHistory.getNationality()
							.equals(countryResidence))) {
				
				if (BooleanType.YES.getCode().equals(
						holderHistory.getIndResidence())) {
					flagForeignResident = true;
				} else if (BooleanType.NO.getCode().equals(
						holderHistory.getIndResidence())) {
					flagForeignNotResident = true;
				}

			}

		}

		loadingDocumentAttachByHolder();
		
	}
   
  /**
    * Validate second nationality by ind resident.
    */
   private void validateSecondNationalityByIndResident(){
		if(holderHistory.getNationality()!=null && !holderHistory.getNationality().equals(countryResidence) && BooleanType.YES.getCode().equals(holderHistory.getIndResidence()) &&
				countryResidence.equals(holderHistory.getSecondNationality())){
			
			holderHistory.setSecondDocumentType(null);
			holderHistory.setSecondDocumentNumber(null);
			newDocumentNumberSecondNationality = GeneralConstants.EMPTY_STRING;
			
		   if(isViewOperationRegister()){
			 SelectOneMenu cmbSecondDocumentType = (SelectOneMenu)  FacesContext.getCurrentInstance().getViewRoot().findComponent("frmHolderModify:tabView:cmbSecondDocumentType");
			 if(cmbSecondDocumentType!=null){cmbSecondDocumentType.setValid(true);}
			 InputText inputSecondDocumentNumber = (InputText)  FacesContext.getCurrentInstance().getViewRoot().findComponent("frmHolderModify:tabView:inputSecondDocumentNumber");
			 if(inputSecondDocumentNumber!=null){inputSecondDocumentNumber.setValid(true);}
		   }
		   
		}
		
	}
	
	/**
	 * Validate same nationality.
	 *
	 * @param idComponent the id component
	 * @return true, if successful
	 */
	public boolean validateSameNationality(String idComponent){
		
		boolean flag=false;
		
		if(holderHistory.getNationality()!=null && holderHistory.getSecondNationality()!=null && (holderHistory.getNationality().equals(holderHistory.getSecondNationality()))){
			
			JSFUtilities.addContextMessage(idComponent,  
					FacesMessage.SEVERITY_ERROR,
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SAME_NATIONALITY),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SAME_NATIONALITY));	
			holderHistory.setSecondNationality(null);
			flag=true;
		}
		else{
			loadHolderSecondDocumentType();
		}
		validateSecondNationalityByIndResident();
		
		if(holderHistory.getSecondNationality()==null){
			holderHistory.setSecondDocumentType(null);
			holderHistory.setSecondDocumentNumber(null);
		}
		
		return flag;
		
	}
	
	/**
	 * Validation for modification.
	 *
	 * @return true, if successful
	 */
	public boolean validationForModification(){

		boolean flagCorrect=true;

		Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		Object[] data = new Object[1];
		
		
		if(holderHistory.getHolderType().equals(PersonType.NATURAL.getCode())){
			
			if(!(holderHistory.isIndicatorMinor())){ // NATURAL MAYOR
				
				if(!(previousModificationTO.getPreviousNationality().equals(holderHistory.getNationality()))){
					
					if(previousModificationTO.getPreviousNationality().equals(countryResidence)){
						if(!(holderHistory.getNationality().equals(countryResidence))){						
							
							if(previousModificationTO.getPreviousDocumentType().equals(holderHistory.getDocumentType())){
								
								data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.type");
								
								JSFUtilities.addContextMessage("frmHolderModify:tabView:cmbDocumentType",
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
								
								flagCorrect = false;
								
								
								
							         	
							}							
							else if(previousModificationTO.getPreviousDocumentNumber().equals(holderHistory.getDocumentNumber())){								
							
							    data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.number");
								
								JSFUtilities.addContextMessage("frmHolderModify:tabView:inputDocumentNumber",
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
								
								flagCorrect = false;
								
							  
							}
							
						}
					}
					else if(!(previousModificationTO.getPreviousNationality().equals(countryResidence))){
						if(holderHistory.getNationality().equals(countryResidence)){
							
							if(previousModificationTO.getPreviousDocumentType().equals(holderHistory.getDocumentType())){
								
								data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.type");
								
								JSFUtilities.addContextMessage("frmHolderModify:tabView:cmbDocumentType",
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
								
								flagCorrect = false;
								
								   	
							}							
								
							else if(previousModificationTO.getPreviousDocumentNumber().equals(holderHistory.getDocumentNumber())){
								
								 data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.number");
									
								 JSFUtilities.addContextMessage("frmHolderModify:tabView:inputDocumentNumber",
											FacesMessage.SEVERITY_ERROR,
											PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
											PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
								 
								 flagCorrect = false;
								
								  
							}
							
						}
					}
					
				}
				else if(!(previousModificationTO.getPreviousResidence().equals(holderHistory.getIndResidence()))){
					
					if(!(previousModificationTO.getPreviousNationality().equals(countryResidence))){
						
						if(previousModificationTO.getPreviousCountryResident().equals(holderHistory.getLegalResidenceCountry())){
							
							data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.country.resident");
							
							 JSFUtilities.addContextMessage("frmHolderModify:tabView:cmbCountryResident",
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
							 
							 flagCorrect = false;
							
							
						}
						else if(previousModificationTO.getPreviousDocumentType().equals(holderHistory.getDocumentType())){
							
							data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.type");
							
							 JSFUtilities.addContextMessage("frmHolderModify:tabView:cmbDocumentType",
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));	
							 
							 flagCorrect = false;
							
							
						}
						else if(previousModificationTO.getPreviousDocumentNumber().equals(holderHistory.getDocumentNumber())){
							
							data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.number");
							
							 JSFUtilities.addContextMessage("frmHolderModify:tabView:inputDocumentNumber",
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
							 
							 
							 flagCorrect = false;
							
							
						}
						
					}
					
					else if(previousModificationTO.getPreviousLegalAddress().equals(holderHistory.getLegalAddress())){
						
						data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.legal.address");
						
						 JSFUtilities.addContextMessage("frmHolderModify:tabView:inputLegalAddress",
									FacesMessage.SEVERITY_ERROR,
									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
						 
						 flagCorrect = false;
						
						
					}
					else if(Validations.validateIsNotNull(previousModificationTO.getPreviousPostalAddress()) 
							&& CommonsUtilities.compareString(previousModificationTO.getPreviousPostalAddress(), holderHistory.getPostalAddress())){
						
						data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.postal.address");
						
						 JSFUtilities.addContextMessage("frmHolderModify:tabView:inputPostalAddress",
									FacesMessage.SEVERITY_ERROR,
									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));						 
						 
						 flagCorrect = false;
						
						
					}
					
					
					
				}
				int compareDate = 1;
				if(Validations.validateIsNotNullAndNotEmpty(holderHistory.getBirthDate()) && 
						Validations.validateIsNotNullAndNotEmpty(previousModificationTO.getPreviousBirthDate())){
					compareDate = previousModificationTO.getPreviousBirthDate().compareTo(holderHistory.getBirthDate());
				}
				
				if(compareDate!=0){
					SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
					if(calculateAge(formato.format(holderHistory.getBirthDate()))<18){
						
						if(!(previousModificationTO.getPreviousDocumentType().equals(holderHistory.getDocumentType()))){				
							
							if(!(previousModificationTO.getPreviousMaxLenghtDocumentNumberHolder().equals(maxLenghtDocumentNumberHolder))){
								
								data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.number");
								
								 JSFUtilities.addContextMessage("frmHolderModify:tabView:inputDocumentNumber",
											FacesMessage.SEVERITY_ERROR,
											PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
											PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
								 
								 flagCorrect = false;
								
								
							}
						}
			
					}
					
				}
				
				
			}
			else if(holderHistory.isIndicatorMinor()){ // NATURAL MENOR
				
				if(!(previousModificationTO.getPreviousNationality().equals(holderHistory.getNationality()))){
					
					if(previousModificationTO.getPreviousNationality().equals(countryResidence)){
						if(!(holderHistory.getNationality().equals(countryResidence))){						
							
							if(previousModificationTO.getPreviousDocumentType().equals(holderHistory.getDocumentType())){
								
								data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.type");
								
								JSFUtilities.addContextMessage("frmHolderModify:tabView:cmbDocumentType",
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));	
								
								
								flagCorrect = false;
								
							   	
							}							
							if(previousModificationTO.getPreviousDocumentNumber().equals(holderHistory.getDocumentNumber())){
								
								 data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.number");
									
									JSFUtilities.addContextMessage("frmHolderModify:tabView:inputDocumentNumber",
											FacesMessage.SEVERITY_ERROR,
											PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
											PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
									
									flagCorrect = false;
								
								
							  
							}
							
						}
					}
					if(!(previousModificationTO.getPreviousNationality().equals(countryResidence))){
						if(holderHistory.getNationality().equals(countryResidence)){
							
							if(previousModificationTO.getPreviousDocumentType().equals(holderHistory.getDocumentType())){
								
								data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.type");
								
								JSFUtilities.addContextMessage("frmHolderModify:tabView:cmbDocumentType",
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));	
								
								
								flagCorrect = false;
								
								  	
							}							
								
							if(previousModificationTO.getPreviousDocumentNumber().equals(holderHistory.getDocumentNumber())){
								
								data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.number");
								
								JSFUtilities.addContextMessage("frmHolderModify:tabView:inputDocumentNumber",
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
								
								
								flagCorrect = false;
								
								  
							}
							
						}
					}
					
					
				}
				if(!(previousModificationTO.getPreviousResidence().equals(holderHistory.getIndResidence()))){
					
				    if(!(previousModificationTO.getPreviousNationality().equals(countryResidence))){
						
						if(previousModificationTO.getPreviousCountryResident().equals(holderHistory.getLegalResidenceCountry())){
							
							data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.country.resident");
							
							 JSFUtilities.addContextMessage("frmHolderModify:tabView:cmbCountryResident",
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
							 
							 flagCorrect = false;
							
							
						}
						else if(previousModificationTO.getPreviousDocumentType().equals(holderHistory.getDocumentType())){
							
							
							data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.type");
							
							JSFUtilities.addContextMessage("frmHolderModify:tabView:cmbDocumentType",
									FacesMessage.SEVERITY_ERROR,
									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
							
							flagCorrect = false;
							
							
							
						}
						else if(previousModificationTO.getPreviousDocumentNumber().equals(holderHistory.getDocumentNumber())){
							
							 data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.number");
								
								JSFUtilities.addContextMessage("frmHolderModify:tabView:inputDocumentNumber",
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
							
								flagCorrect = false;
							
							
						}
						
					}
					
					if(previousModificationTO.getPreviousLegalAddress().equals(holderHistory.getLegalAddress())){
						
						data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.legal.address");
						
						 JSFUtilities.addContextMessage("frmHolderModify:tabView:inputLegalAddress",
									FacesMessage.SEVERITY_ERROR,
									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
						 
						 flagCorrect = false;
						
						
					}
					else if(previousModificationTO.getPreviousPostalAddress().equals(holderHistory.getPostalAddress())){
						
						data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.postal.address");
						
						 JSFUtilities.addContextMessage("frmHolderModify:tabView:inputPostalAddress",
									FacesMessage.SEVERITY_ERROR,
									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
						 
						 flagCorrect = false;
						
						
						
					}
					
					
					
				}
				//Issue 1050
//				else if(!(previousModificationTO.getPreviousDocumentType().equals(holderHistory.getDocumentType()))){				
//				
//					if(!(previousModificationTO.getPreviousMaxLenghtDocumentNumberHolder().equals(maxLenghtDocumentNumberHolder))){
//						
//						data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.number");
//						
//						 JSFUtilities.addContextMessage("frmHolderModify:tabView:inputDocumentNumber",
//									FacesMessage.SEVERITY_ERROR,
//									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
//									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
//						
//						 flagCorrect = false;
//						
//						
//					}
//					
//				}
//				int compareDate = previousModificationTO.getPreviousBirthDate().compareTo(holderHistory.getBirthDate());
//				
//				if(compareDate!=0){
//					
//				   if(calculateAge(holderHistory.getBirthDate().toString())>=18){
//						
//						if(!(previousModificationTO.getPreviousDocumentType().equals(holderHistory.getDocumentType()))){				
//							
//							if(!(previousModificationTO.getPreviousMaxLenghtDocumentNumberHolder().equals(maxLenghtDocumentNumberHolder))){
//								
//								data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.number");
//								
//								 JSFUtilities.addContextMessage("frmHolderModify:tabView:inputDocumentNumber",
//											FacesMessage.SEVERITY_ERROR,
//											PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
//											PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
//								 
//								 flagCorrect = false;
//								
//								
//							}
//						}
//						
//					}
//					
//				}				
				
				
				
			}
			if(holderHistory.isIndicatorDisabled()){ //NATURAL INCAPACIDAD
				
				if(!(previousModificationTO.getPreviousNationality().equals(holderHistory.getNationality()))){
					
					if(previousModificationTO.getPreviousNationality().equals(countryResidence)){
						if(!(holderHistory.getNationality().equals(countryResidence))){						
							
							if(previousModificationTO.getPreviousDocumentType().equals(holderHistory.getDocumentType())){
								
								data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.type");
								
								JSFUtilities.addContextMessage("frmHolderModify:tabView:cmbDocumentType",
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
								
								flagCorrect = false;
								
							  	
							}							
							if(previousModificationTO.getPreviousDocumentNumber().equals(holderHistory.getDocumentNumber())){
								
							data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.number");
								
								JSFUtilities.addContextMessage("frmHolderModify:tabView:inputDocumentNumber",
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
								
								flagCorrect = false;
								
							  
							}
							
						}
					}
					if(!(previousModificationTO.getPreviousNationality().equals(countryResidence))){
						if(holderHistory.getNationality().equals(countryResidence)){
							
							if(previousModificationTO.getPreviousDocumentType().equals(holderHistory.getDocumentType())){
								
								data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.type");
								
								JSFUtilities.addContextMessage("frmHolderModify:tabView:cmbDocumentType",
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
								
								flagCorrect = false;
								
								   	
							}							
								
							if(previousModificationTO.getPreviousDocumentNumber().equals(holderHistory.getDocumentNumber())){
								
							data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.number");
								
								JSFUtilities.addContextMessage("frmHolderModify:tabView:inputDocumentNumber",
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
								
								flagCorrect = false;
								
								  
							}
							
						}
					}
					
				}
				if(!(previousModificationTO.getPreviousResidence().equals(holderHistory.getIndResidence()))){
					
					if(!(previousModificationTO.getPreviousNationality().equals(countryResidence))){
						
						if(previousModificationTO.getPreviousCountryResident().equals(holderHistory.getLegalResidenceCountry())){
							
							data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.country.resident");
							
							 JSFUtilities.addContextMessage("frmHolderModify:tabView:cmbCountryResident",
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
							
							 flagCorrect = false;
							 
							
						}
						if(previousModificationTO.getPreviousDocumentType().equals(holderHistory.getDocumentType())){
							data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.type");
							
							JSFUtilities.addContextMessage("frmHolderModify:tabView:cmbDocumentType",
									FacesMessage.SEVERITY_ERROR,
									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
							
							flagCorrect = false;
							
							
						}
						if(previousModificationTO.getPreviousDocumentNumber().equals(holderHistory.getDocumentNumber())){
							
							 data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.number");
								
								JSFUtilities.addContextMessage("frmHolderModify:tabView:inputDocumentNumber",
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
										PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
							
								flagCorrect = false;
								
							
						}
						
					}
					
					if(previousModificationTO.getPreviousLegalAddress().equals(holderHistory.getLegalAddress())){
						
						data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.legal.address");
						
						 JSFUtilities.addContextMessage("frmHolderModify:tabView:inputLegalAddress",
									FacesMessage.SEVERITY_ERROR,
									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
						 
						 flagCorrect = false;
						
						
					}
					if(previousModificationTO.getPreviousPostalAddress().equals(holderHistory.getPostalAddress())){
						
						data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.postal.address");
						
						 JSFUtilities.addContextMessage("frmHolderModify:tabView:inputPostalAddress",
									FacesMessage.SEVERITY_ERROR,
									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
					
						 flagCorrect = false;
					 
						
					}
					
					
				}
				//Issue 1050
				//
//				if(!(previousModificationTO.getPreviousDocumentType().equals(holderHistory.getDocumentType()))){				
//				
//					if(!(previousModificationTO.getPreviousMaxLenghtDocumentNumberHolder().equals(maxLenghtDocumentNumberHolder))){
//						
//						data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.number");
//						
//						 JSFUtilities.addContextMessage("frmHolderModify:tabView:inputDocumentNumber",
//									FacesMessage.SEVERITY_ERROR,
//									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
//									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
//						 
//						 flagCorrect = false;
//						
//						
//					}
//					
//				}
				
				int compareDate = previousModificationTO.getPreviousBirthDate().compareTo(holderHistory.getBirthDate());
				
				if(compareDate!=0){
					
					SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
					String birthday = formato.format(holderHistory.getBirthDate());
					if(calculateAge(birthday)<18){
						
						if(!(previousModificationTO.getPreviousDocumentType().equals(holderHistory.getDocumentType()))){				
							
							if(!(previousModificationTO.getPreviousMaxLenghtDocumentNumberHolder().equals(maxLenghtDocumentNumberHolder))){
								
								data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.number");
								
								 JSFUtilities.addContextMessage("frmHolderModify:tabView:inputDocumentNumber",
											FacesMessage.SEVERITY_ERROR,
											PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
											PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
								 
								 flagCorrect = false;
								
								
							}
						}
					
    				}					
					else if(calculateAge(formato.format(holderHistory.getBirthDate()))>=18){
						
						if(!(previousModificationTO.getPreviousDocumentType().equals(holderHistory.getDocumentType()))){				
							
							if(!(previousModificationTO.getPreviousMaxLenghtDocumentNumberHolder().equals(maxLenghtDocumentNumberHolder))){
								
								data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.number");
								
								 JSFUtilities.addContextMessage("frmHolderModify:tabView:inputDocumentNumber",
											FacesMessage.SEVERITY_ERROR,
											PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
											PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
								 
								 flagCorrect = false;
								
								
							}
						}
			
					}	
					
				}
				

			}
		}
		else if(holderHistory.getHolderType().equals(PersonType.JURIDIC.getCode())){  //JURIDICO
			
			if(previousModificationTO.getPreviousNationality().equals(holderHistory.getNationality())){
				
				if(previousModificationTO.getPreviousNationality().equals(countryResidence)){
					if(!(holderHistory.getNationality().equals(countryResidence))){						
						
						if(previousModificationTO.getPreviousDocumentType().equals(holderHistory.getDocumentType())){
							
							data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.type");
							
							JSFUtilities.addContextMessage("frmHolderModify:tabView:cmbDocumentType2",
									FacesMessage.SEVERITY_ERROR,
									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
							
							flagCorrect = false;
							
						  
						}							
						if(previousModificationTO.getPreviousDocumentNumber().equals(holderHistory.getDocumentNumber())){
							
							data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.number");
							
							JSFUtilities.addContextMessage("frmHolderModify:tabView:inputDocumentNumber2",
									FacesMessage.SEVERITY_ERROR,
									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
							
							flagCorrect = false;
							
						  
						}
						
					}
				}
				if(!(previousModificationTO.getPreviousNationality().equals(countryResidence))){
					if(holderHistory.getNationality().equals(countryResidence)){
						
						if(previousModificationTO.getPreviousDocumentType().equals(holderHistory.getDocumentType())){
							
							data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.type");
							
							JSFUtilities.addContextMessage("frmHolderModify:tabView:cmbDocumentType2",
									FacesMessage.SEVERITY_ERROR,
									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
							
							flagCorrect = false;
							
							  	
						}							
							
						if(previousModificationTO.getPreviousDocumentNumber().equals(holderHistory.getDocumentNumber())){
							
							data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.number");
							
							JSFUtilities.addContextMessage("frmHolderModify:tabView:inputDocumentNumber2",
									FacesMessage.SEVERITY_ERROR,
									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
							
							flagCorrect = false;
							
							  
						}
						
					}
				}
				
			}
			
			if(!previousModificationTO.getPreviousResidence().equals(holderHistory.getIndResidence())){
				
				if(!(previousModificationTO.getPreviousNationality().equals(countryResidence))){
					
					if(previousModificationTO.getPreviousCountryResident().equals(holderHistory.getLegalResidenceCountry())){
						
						data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.country.resident");
						
						 JSFUtilities.addContextMessage("frmHolderModify:tabView:cmbCountryResident2",
									FacesMessage.SEVERITY_ERROR,
									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
									PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));	
						 
						 flagCorrect = false;
						 
						 
						
						
					}
					else if(previousModificationTO.getPreviousDocumentType().equals(holderHistory.getDocumentType())){
						
						data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.type");
						
						JSFUtilities.addContextMessage("frmHolderModify:tabView:cmbDocumentType2",
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
								PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
						
						
						flagCorrect = false;
						
						
						
					}
					else if(previousModificationTO.getPreviousDocumentNumber().equals(holderHistory.getDocumentNumber())){
						
						data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.number");
						
						JSFUtilities.addContextMessage("frmHolderModify:tabView:inputDocumentNumber2",
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
								PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
						
						flagCorrect = false;
						
						
					}
					
				}
			
				if(!previousModificationTO.getPreviousCountryResident().equals(holderHistory.getLegalResidenceCountry())){
				if(previousModificationTO.getPreviousLegalAddress().equals(holderHistory.getLegalAddress())){
					
					data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.legal.address");
					
					 JSFUtilities.addContextMessage("frmHolderModify:tabView:inputLegalAddress",
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
								PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
					 
					 flagCorrect = false;
					
					
				}
				else if(previousModificationTO.getPreviousPostalAddress().equals(holderHistory.getPostalAddress())){
					
					data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.postal.address");
					
					 JSFUtilities.addContextMessage("frmHolderModify:tabView:inputPostalAddress",
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
								PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
					 
					 flagCorrect = false;
					 
					
				}
				}
				
			}
			
			else if(previousModificationTO.getPreviousDocumentType().equals(holderHistory.getDocumentType())){				
			
				if(!(previousModificationTO.getPreviousMaxLenghtDocumentNumberHolder().equals(maxLenghtDocumentNumberHolder))){
					
					data[0] = PropertiesUtilities.getMessage(locale, "adm.holders.register.holder.lbl.document.number");
					
					 JSFUtilities.addContextMessage("frmHolderModify:tabView:inputDocumentNumber2",
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data),
								PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
					 
					 flagCorrect = false;
					 
					
				}
				
			}			
			
		}
		
		if(!flagCorrect){
			
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.NECESSARY_TO_MODIFY,data));
		
			JSFUtilities.showSimpleValidationDialog();
		}
		
		return flagCorrect;
	}
	
	/**
	 * Validate type document holder.
	 *
	 * @param idComponent the id component
	 */
	public void validateTypeDocumentHolder(String idComponent){
		
		try{
			
		if (validateTypesDocumentsHolder(idComponent)) {

			holderHistory.setDocumentNumber(GeneralConstants.EMPTY_STRING);
			newDocumentNumber = GeneralConstants.EMPTY_STRING;
			documentTO = documentValidator.loadMaxLenghtType(holderHistory.getDocumentType());			
			numericTypeDocumentHolder = documentTO.isNumericTypeDocument();
			maxLenghtDocumentNumberHolder = documentTO.getMaxLenghtDocumentNumber();
			holderHistory.setDocumentIssuanceDate(null);
			
			documentTO = documentValidator.validateEmissionRules(holderHistory.getDocumentType());
			if(documentTO.isIndicatorDocumentSource()){
				
				getLstDocumentSource();
				SelectOneMenu cmbDocumentSource = (SelectOneMenu)FacesContext.getCurrentInstance().getViewRoot().findComponent("frmHolderModify:tabView:cmbDocumentSource");
				cmbDocumentSource.setValid(true);
				getLstDocumentSource();
				if(holderHistory.getNationality()!=null 
						&& holderHistory.getNationality().equals(countryResidence)
						&& holderHistory.getIndResidence()!=null 
						&& holderHistory.getIndResidence().equals(BooleanType.YES.getCode())){
					holderHistory.setDocumentSource(departmentIssuance);
				}
				else{
					holderHistory.setDocumentSource(null);
				}
			}
			else{
				holderHistory.setDocumentSource(null);
				listDocumentSource = new ArrayList<ParameterTable>();
			}
		}
		else{
			holderHistory.setDocumentType(null);
		}
		
		}
		catch(Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
			
		}
		
	}
	
	
/**
 * Approve holder request.
 */
@LoggerAuditWeb
public void approveHolderRequest(){
	try {
		
		HolderRequest holderRequest = holderRequestDetail;
		HolderHistory holderHistory = holderHistoryDetail;
		List<LegalRepresentativeHistory> lstLegalRepresentativeHistory = new ArrayList<LegalRepresentativeHistory>();
	    List<HolderHistory> lstHolderHistory = new ArrayList<HolderHistory>();
		
		holderRequest.setStateHolderRequest(HolderRequestStateType.APPROVED.getCode());
		holderRequest.setApproveDate(CommonsUtilities.currentDate());
		holderRequest.setApproveUser(userInfo.getUserAccountSession().getUserName());
		holderHistory.setStateHolderHistory(HolderRequestStateType.APPROVED.getCode());
		
		lstLegalRepresentativeHistory = holderRequest.getLegalRepresentativeHistories();

		lstHolderHistory.add(holderHistory);
		
		holderRequest.setHolderHistories(lstHolderHistory);
		
		if(lstLegalRepresentativeHistory!=null && lstLegalRepresentativeHistory.size()>0){
			holderRequest.setLegalRepresentativeHistories(lstLegalRepresentativeHistory);	
		}

		boolean flagTransaction = holderServiceFacade
				.updateStateApproveModificationHolderRequestServiceFacade(holderRequest, null,ViewOperationsType.APPROVE.getCode());

		if (flagTransaction) {
			Object[] bodyData = new Object[1];

			bodyData[0] = holderRequestSelected.getHolder().getIdHolderPk().toString();

			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.LBL_SUCCESS_APPROVE_HOLDER_MODIFICATION_REQUEST,bodyData));
			JSFUtilities.executeJavascriptFunction("PF('widgTransactionDialog').show()");
			
			searchRequestHolders();
			
			businessProcess = new BusinessProcess();
			
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_MODIFICATION_REQUEST_APPROVE.getCode());
			Object[] parameters = new Object[2];
			parameters[0]=holderRequest.getIdHolderRequestPk().toString();				
			parameters[1]=holderRequest.getHolder().getIdHolderPk().toString();
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,null,null,parameters);
	
		    
		}

	} catch (Exception ex) {
		excepcion.fire(new ExceptionToCatchEvent(ex));
	}
}

/**
 * Confirm holder request.
 */
@LoggerAuditWeb 
public void confirmHolderRequest() {

	try {
	 
		boolean flagTransaction = false;
		
		flagTransaction = holderServiceFacade.confirmModificationRequestHolderServiceFacade(holderRequestDetail,holderHistoryDetail);

		if(flagTransaction){
			Object[] bodyData = new Object[1];

			bodyData[0] = holderRequestDetail.getHolder().getIdHolderPk().toString();

			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.LBL_SUCCESS_CONFIRM_HOLDER_MODIFICATION_REQUEST,bodyData));
			JSFUtilities
					.executeJavascriptFunction("PF('widgTransactionDialog').show()");
			
			searchRequestHolders();
			
			businessProcess = new BusinessProcess();
			
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_MODIFICATION_REQUEST_CONFIRM.getCode());
			Object[] parameters = new Object[2];
			parameters[0]=holderRequestDetail.getIdHolderRequestPk().toString();				
			parameters[1]=holderRequestDetail.getHolder().getIdHolderPk().toString();
			Long idParticipantPk = holderRequestDetail.getParticipant().getIdParticipantPk();
			String idIssuerFk = participantServiceBean.getIssuerIsParticipant(holderRequestDetail.getParticipant().getIdParticipantPk());
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,idParticipantPk,idIssuerFk,parameters);	
		}
	} catch (Exception ex) {
		excepcion.fire(new ExceptionToCatchEvent(ex));
	}
}

	/**
	 * Annular holder request.
	 */
    @LoggerAuditWeb
	public void annularHolderRequest() {

		try {
			
			JSFUtilities.executeJavascriptFunction("PF('dlgMotive').hide()");
			
			HolderRequest holderRequest = holderRequestDetail;
			HolderHistory holderHistory = holderHistoryDetail;
			List<LegalRepresentativeHistory> lstLegalRepresentativeHistory = new ArrayList<LegalRepresentativeHistory>();
		    List<HolderHistory> lstHolderHistory = new ArrayList<HolderHistory>();

			holderRequest.setStateHolderRequest(HolderRequestStateType.ANNULED
					.getCode());
			holderRequest.setAnnulDate(CommonsUtilities.currentDate());
			holderRequest.setAnnulUser(userInfo.getUserAccountSession().getUserName());
			

			holderRequest.setRequestOtherMotive(requestOtherMotive);
			holderRequest.setRequestMotive(requestMotive);

			holderHistory.setStateHolderHistory(HolderRequestStateType.ANNULED
					.getCode());
			
			lstLegalRepresentativeHistory = holderRequest.getLegalRepresentativeHistories();

            if(lstLegalRepresentativeHistory!=null && lstLegalRepresentativeHistory.size()>0){
            	for (int i = 0; i < lstLegalRepresentativeHistory.size(); i++) {
            		lstLegalRepresentativeHistory.get(i).setStateRepreHistory(
            				HolderRequestStateType.ANNULED.getCode());
            	}
            }
			lstHolderHistory.add(holderHistory);

			holderRequest.setHolderHistories(lstHolderHistory);
			if(lstLegalRepresentativeHistory!=null && lstLegalRepresentativeHistory.size()>0){
				holderRequest.setLegalRepresentativeHistories(lstLegalRepresentativeHistory);
			}

			boolean flagTransaction = holderServiceFacade
					.updateStateAnulateModificationHolderRequestServiceFacade(holderRequest,null,ViewOperationsType.ANULATE.getCode());

			if (flagTransaction) {
				Object[] bodyData = new Object[1];

				bodyData[0] = holderRequestSelected.getHolder().getIdHolderPk().toString();

				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						PropertiesUtilities.getMessage(PropertiesConstants.LBL_SUCCESS_ANNUL_HOLDER_MODIFICATION_REQUEST,bodyData));
				JSFUtilities.executeJavascriptFunction("PF('widgTransactionDialog').show()");				
				searchRequestHolders();
				
				try {
					
					businessProcess = new BusinessProcess();
					
				    businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_MODIFICATION_REQUEST_CANCEL.getCode());
					Object[] parameters = new Object[1];
					parameters[0]=holderRequest.getIdHolderRequestPk().toString();				
					//parameters[1]=holderRequest.getHolder().getIdHolderPk().toString();
					//notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,holderRequest.getParticipant().getIdParticipantPk(),parameters);

					notificationServiceFacade.sendNotificationEmailParticipantNoConfig(userInfo.getUserAccountSession().getUserName(), 
							   businessProcess, 
							   "ANULACION DE REGISTRO DE TITULAR",
							   "Se realizo la anulación de la solicitud %s ",
							   holderRequest.getParticipant().getIdParticipantPk(), 
							   parameters);

				}catch (Exception e) {
					System.out.println("*** no se pudo enviar la notificacion:: "+e.getMessage());
				}
				
			}

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Reject holder request.
	 */
    @LoggerAuditWeb
	public void rejectHolderRequest() {

		try {
			
			JSFUtilities.executeJavascriptFunction("PF('dlgMotive').hide()");
			    
			HolderRequest holderRequest = holderRequestDetail;
			HolderHistory holderHistory = holderHistoryDetail;
			List<LegalRepresentativeHistory> lstLegalRepresentativeHistory = new ArrayList<LegalRepresentativeHistory>();
		    List<HolderHistory> lstHolderHistory = new ArrayList<HolderHistory>();

			holderRequest.setStateHolderRequest(HolderRequestStateType.REJECTED
					.getCode());
			holderRequest.setRejectDate(CommonsUtilities.currentDate());
			holderRequest.setRejectUser(userInfo.getUserAccountSession().getUserName());
			
			holderRequest.setRequestOtherMotive(requestOtherMotive);
			holderRequest.setRequestMotive(requestMotive);

			holderHistory.setStateHolderHistory(HolderRequestStateType.REJECTED.getCode());
			
			lstLegalRepresentativeHistory = holderRequest.getLegalRepresentativeHistories();

			if(lstLegalRepresentativeHistory!=null && lstLegalRepresentativeHistory.size()>0){
				for (int i = 0; i < lstLegalRepresentativeHistory.size(); i++) {
					lstLegalRepresentativeHistory.get(i).setStateRepreHistory(
							HolderRequestStateType.REJECTED.getCode());
				
				}
			}
			
			lstHolderHistory.add(holderHistory);

			holderRequest.setHolderHistories(lstHolderHistory);
			
			if(lstLegalRepresentativeHistory!=null && lstLegalRepresentativeHistory.size()>0){
				holderRequest.setLegalRepresentativeHistories(lstLegalRepresentativeHistory);
			}
			
			boolean flagTransaction = holderServiceFacade
					.updateStateRejectModificationHolderRequestServiceFacade(holderRequest,null,ViewOperationsType.REJECT.getCode());
			
			if (flagTransaction) {
				Object[] bodyData = new Object[1];

				bodyData[0] = holderRequestSelected.getHolder().getIdHolderPk().toString();

				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						PropertiesUtilities.getMessage(PropertiesConstants.LBL_SUCESS_REJECT_HOLDER_MODIFICATION_REQUEST, bodyData));
				JSFUtilities
						.executeJavascriptFunction("PF('widgTransactionDialog').show()");
				
				searchRequestHolders();
				
				businessProcess = new BusinessProcess();
				
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_MODIFICATION_REQUEST_REJECT.getCode());
				Object[] parameters = new Object[2];
				parameters[0]=holderRequest.getIdHolderRequestPk().toString();				
				parameters[1]=holderRequest.getHolder().getIdHolderPk().toString();
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,holderRequest.getParticipant().getIdParticipantPk(),parameters);
		

			}

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Action do.
	 */
    
    @LoggerAuditWeb
	public void actionDo() {
		
		try{

		 
		
			if (flagActionConfirm) {
				confirmHolderRequest();
			} else if (flagActionReject) {
				rejectHolderRequest();
			} 
			else if(flagActionApprove){
				approveHolderRequest();
			}
			else if (flagActionAnnular) {
				annularHolderRequest();
			}	
	
	    } 
		
		catch (Exception ex) {
		excepcion.fire(new ExceptionToCatchEvent(ex));
	    }
		
	}
	
	/**
	 * Evaluate motive.
	 */
	public void evaluateMotive(){
		
		if(flagActionReject){
			if(requestMotive!=null && requestMotive.equals(RejectMotivesHolderRequestType.OTHER_MOTIVES.getCode())){
				requestOtherMotive="";
				showMotiveText=true;
				
			}
			else{
				showMotiveText=false;
			}
		}
		else if(flagActionAnnular){
			if(requestMotive!=null && requestMotive.equals(AnnularMotivesHolderRequestType.OTHER_MOTIVES.getCode())){
				requestOtherMotive="";
				showMotiveText=true;
				
			}
			else{
				showMotiveText=false;
			}
		}
		
		
	}
	

	/**
	 * Validate second type document holder.
	 *
	 * @param idComponent the id component
	 */
	public void validateSecondTypeDocumentHolder(String idComponent){
		
		if(validateTypesDocumentsHolder(idComponent)){
		
			DocumentTO documentTo = documentValidator.loadMaxLenghtType(holderHistory.getSecondDocumentType());
			
			numericSecondTypeDocumentHolder = documentTo.isNumericTypeDocument();
			maxLenghtSecondDocumentNumberHolder = documentTo.getMaxLenghtDocumentNumber();
			
			if(holderHistory.getSecondDocumentType() != null && holderHistory.getSecondDocumentNumber() != null){
				if(holderHistory.getSecondDocumentType().equals(DocumentType.CIE.getCode()) ){
					newDocumentNumberSecondNationality = "E-" + holderHistory.getSecondDocumentNumber();
				}
				else{
					newDocumentNumberSecondNationality = holderHistory.getSecondDocumentNumber();
				}
			}
		}
		else{
			holderHistory.setSecondDocumentType(null);
			newDocumentNumberSecondNationality = null;
		}
	}
	
	/**
	 * Validate types documents holder.
	 *
	 * @param idComponent the id component
	 * @return true, if successful
	 */
	public boolean validateTypesDocumentsHolder(String idComponent){
		
		boolean flag = true;

		if (holderHistory.getDocumentType()!=null && holderHistory.getDocumentType().equals(
				holderHistory.getSecondDocumentType())) {
			
			if(!holderHistory.getSecondDocumentType().equals(DocumentType.DIO.getCode())){
				
			
			flag = false;

			JSFUtilities
					.addContextMessage(
							idComponent,
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities
									.getMessage(PropertiesConstants.ERROR_SAME_TYPE_DOCUMENT),
							PropertiesUtilities
									.getMessage(PropertiesConstants.ERROR_SAME_TYPE_DOCUMENT));
			
			JSFUtilities.showMessageOnDialog(null, null,
					PropertiesConstants.ERROR_SAME_TYPE_DOCUMENT, null);
			JSFUtilities.showSimpleValidationDialog();
			return false;
			
			}
		}

		return flag;
	}
	
	/**
	 * Listener juridic class.
	 *
	 */
	public void listenerJuridicClass(){
		
		disabledNationality = false;
		disabledIndResident = false;
		disabledCountryResidentHolder = false;
		
		
			if(holderHistory.getJuridicClass()!=null && holderHistory.getJuridicClass().equals(JuridicClassType.ENTITY_STATE.getCode())){
				
				holderHistory.setNationality(countryResidence);
				holderHistory.setIndResidence(BooleanType.YES.getCode());
				holderHistory.setLegalResidenceCountry(countryResidence);
				
				disabledNationality = true;
				disabledIndResident = true;
				disabledCountryResidentHolder = true;
				
				
			}
			
			else if(holderHistory.getJuridicClass()!=null && holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())){
				
				holderHistory.setIndResidence(BooleanType.YES.getCode());
				disabledIndResident = true;
				disabledCountryResidentHolder = true;
				
				holderHistory.setLegalResidenceCountry(countryResidence);
				holderHistory.setNationality(countryResidence);
				
				disabledNationality = false;
								
			}
			
			else if(holderHistory.getJuridicClass()!=null && holderHistory.getJuridicClass().equals(JuridicClassType.NORMAL.getCode())){
					disabledNationality = false;					
			}
			
			else if(holderHistory.getJuridicClass()==null){
				disabledNationality = false;
				disabledIndResident = false;
				disabledCountryResidentHolder = false;
				holderHistory.setIndResidence(null);
				holderHistory.setNationality(null);
				holderHistory.setLegalResidenceCountry(null);
				listDepartmentLocation=null;
				listProvinceLocation=null;
				listMunicipalityLocation=null;
				listDepartmentPostalLocation=null;
				listProvincePostalLocation=null;
				listMunicipalityPostalLocation=null;
				
			}
		
			
		validateResidentHolder("");
		validateCountryResidentHolder();
		loadingDocumentAttachByHolder();
	}
	
	/**
	 * Validate country resident holder.
	 */
	public void validateCountryResidentHolder() {
		if (holderHistory.getLegalResidenceCountry()!=null && holderHistory.getLegalResidenceCountry().equals(
				countryResidence)) {
			disabledCountryResidentHolder = true;
			
			flagCopyLegalHolder = false;
			disabledCmbPostalCountry = false;
			disabledCmbPostalDepartament = false;
			disabledCmbPostalProvince = false;
			disabledCmbPostalMunicipality = false;
			disabledInputPostalAddress = false;
			
			listDepartmentLocation = getLstDepartmentLocation(holderHistory.getLegalResidenceCountry());
			
			holderHistory.setLegalDepartment(departmentDefault);			
			listProvinceLocation = getLstProvinceLocation(holderHistory.getLegalDepartment());
			holderHistory.setLegalProvince(provinceDefault);
			listMunicipalityLocation = getLstMunicipalityLocation(holderHistory.getLegalProvince());
			
			holderHistory.setLegalDistrict(null);
			holderHistory.setLegalAddress(null);
			
			holderHistory.setPostalResidenceCountry(null);	
			holderHistory.setPostalDepartment(null);
			holderHistory.setPostalProvince(null);
			holderHistory.setPostalDistrict(null);
			holderHistory.setPostalAddress(null);
			
			listMunicipalityPostalLocation=null;
			
			holderHistory.setIndResidence(BooleanType.YES.getCode());
			
			documentTO = new DocumentTO();
			
			if(holderHistory.getNationality()!=null && holderHistory.getNationality().equals(countryResidence)){
				holderHistory.setDocumentType(DocumentType.CI.getCode());
				documentTO = documentValidator.loadMaxLenghtType(holderHistory.getDocumentType());			
				numericTypeDocumentHolder = documentTO.isNumericTypeDocument();
				maxLenghtDocumentNumberHolder = documentTO.getMaxLenghtDocumentNumber();
				documentTO = documentValidator.validateEmissionRules(holderHistory.getDocumentType());
				getLstDocumentSource();
				holderHistory.setDocumentSource(departmentIssuance);
			}
			else{
				holderHistory.setDocumentType(null);
				holderHistory.setDocumentSource(null);
			}
			
		} else {
			flagCopyLegalHolder = false;
			disabledCountryResidentHolder = false;
			disabledCmbPostalCountry = false;
			disabledCmbPostalProvince = false;
			disabledCmbPostalMunicipality = false;
			disabledInputPostalAddress = false;
			holderHistory.setIndResidence(BooleanType.NO.getCode());
			listDepartmentLocation = getLstDepartmentLocation(holderHistory.getLegalResidenceCountry());
			
			holderHistory.setLegalDepartment(null);
			holderHistory.setLegalProvince(null);
			holderHistory.setLegalDistrict(null);
			holderHistory.setLegalAddress(null);
			
			listMunicipalityLocation=null;
			
			flagCopyLegalHolder=false;				
			
			holderHistory.setPostalResidenceCountry(null);
			holderHistory.setPostalDepartment(null);
			holderHistory.setPostalProvince(null);
			holderHistory.setPostalDistrict(null);
			holderHistory.setPostalAddress(null);
			
			listMunicipalityPostalLocation=null;
			holderHistory.setDocumentType(null);
			holderHistory.setDocumentSource(null);
		}
		
		validateNationalForeign("");
		loadHolderDocumentType();
		loadHolderSecondDocumentType();
		loadingDocumentAttachByHolder();
	}
	
	/**
	 * Gets the lst department location.
	 *
	 * @param referenceFk the reference fk
	 * @return the lst department location
	 */
	public List<GeographicLocation> getLstDepartmentLocation(Integer referenceFk) {

		List<GeographicLocation> lst = null;

		try {

			GeographicLocationTO geographicLocationTO = new GeographicLocationTO();

			geographicLocationTO
					.setGeographicLocationType(GeographicLocationType.DEPARTMENT
							.getCode());
			geographicLocationTO.setIdLocationReferenceFk(referenceFk);

			lst = generalParametersFacade
					.getListGeographicLocationServiceFacade(geographicLocationTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

		return lst;

	}
	
	/**
	 * Validate exist second type document and second number document.
	 *
	 * @param idComponent the id component
	 */
	@LoggerAuditWeb
	public void validateExistSecondTypeDocumentAndSecondNumberDocument(String idComponent) {

		try {
		
			if(Validations.validateIsNullOrEmpty(holderHistory.getSecondDocumentNumber())
					|| Validations.validateIsNullOrEmpty(holderHistory.getSecondDocumentType())){
				return;
			}
			
			if(holderHistory.getSecondDocumentType() != null && holderHistory.getSecondDocumentNumber() != null){
				newDocumentNumberSecondNationality = holderHistory.getSecondDocumentNumber();
				if(holderHistory.getSecondDocumentType().equals(DocumentType.CIE.getCode())){
					newDocumentNumberSecondNationality = "E-" + holderHistory.getSecondDocumentNumber();
				}
			}
			
			if(holderHistory.getSecondDocumentType()!=null &&
					!holderHistory.getSecondDocumentType().equals(DocumentType.DIO.getCode()) &&
					   holderHistory.getSecondDocumentNumber()!=null && 
					   legalRepresentativeHelperBean.getLstLegalRepresentativeHistory()!=null && 
					   legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size()>0){
						
					   for(int i=0;i<legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size();i++){
						   
						   if((legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentType().equals(holderHistory.getSecondDocumentType()) &&
						      legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentNumber().equals(holderHistory.getSecondDocumentNumber()))
						      || (legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getSecondDocumentType()!=null && legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getSecondDocumentType().equals(holderHistory.getSecondDocumentType()) &&
						    		  legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getSecondDocumentNumber()!=null && legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getSecondDocumentNumber().equals(holderHistory.getSecondDocumentNumber()))
								      ){
						
							   	Object[] bodyData = new Object[0];
							   	
							   	JSFUtilities.showMessageOnDialog(GeneralConstants.LBL_HEADER_ALERT, PropertiesUtilities.getMessage(
														PropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE,bodyData));
								JSFUtilities.showSimpleValidationDialog();
								
							   
							   JSFUtilities
								.addContextMessage(
										idComponent,
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities
												.getMessage(
														PropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE,
														bodyData),
										PropertiesUtilities
												.getMessage(
														PropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE,
														bodyData));

								holderHistory.setSecondDocumentNumber(null); 
								return;
						   }
					   }
						
					}

			
			if(holderHistory.getSecondDocumentType()!=null && holderHistory.getDocumentType()!=null
					&& holderHistory.getDocumentNumber()!=null && holderHistory.getSecondDocumentNumber()!=null){
			
			if(holderHistory.getSecondDocumentType().equals(holderHistory.getDocumentType())
					&& holderHistory.getDocumentNumber().equals(holderHistory.getSecondDocumentNumber())){
				 
				Object[] bodyData = new Object[0];
				
				JSFUtilities.showMessageOnDialog(GeneralConstants.EMPTY_STRING, PropertiesUtilities.getMessage(
										PropertiesConstants.ERROR_DOCUMENTS_TYPE_AND_NUMBER_EQUALS,bodyData));
				
				JSFUtilities.showSimpleValidationDialog();
				
				JSFUtilities
				.addContextMessage(
						idComponent,
						FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities
								.getMessage(
										PropertiesConstants.ERROR_DOCUMENTS_TYPE_AND_NUMBER_EQUALS,
										bodyData),
						PropertiesUtilities
								.getMessage(
										PropertiesConstants.ERROR_DOCUMENTS_TYPE_AND_NUMBER_EQUALS,
										bodyData));

				holderHistory.setSecondDocumentNumber(null);	
				return;
			}
			
			}
			
			if(documentValidator.validateFormatDocumentNumber(holderHistory.getSecondDocumentType(),holderHistory.getSecondDocumentNumber(),idComponent))
			{
			
				Holder objHolder = new Holder();

				objHolder.setSecondDocumentType(holderHistory
						.getSecondDocumentType());
				objHolder.setSecondDocumentNumber(holderHistory
						.getSecondDocumentNumber());
				objHolder.setSecondNationality(holderHistory.getSecondNationality());

				objHolder.setIdHolderPk(holder.getIdHolderPk());
				
					objHolder = accountsFacade
							.getHolderBySecondDocumentTypeAndSecondDocumentNumberServiceFacade(objHolder);
					
					if(objHolder!=null){
					
					Object[] bodyData = new Object[2];

					bodyData[0] = objHolder.getDescriptionHolder();
					bodyData[1] = objHolder.getIdHolderPk();

					showMessageOnDialog(GeneralConstants.EMPTY_STRING, PropertiesUtilities.getMessage(
							PropertiesConstants.WARNING_DOCUMENTTYPE_DOCUMENTNUMBER_OTHER_HOLDER,
							bodyData));
					
					JSFUtilities.showSimpleValidationDialog();
					
					holderHistory.setSecondDocumentNumber(null);
					
					JSFUtilities
							.addContextMessage(
									"frmHolderModify:tabView:inputSecondDocumentNumber",
									FacesMessage.SEVERITY_ERROR,
									PropertiesUtilities
											.getMessage(
													PropertiesConstants.WARNING_DOCUMENTTYPE_DOCUMENTNUMBER_OTHER_HOLDER,
													bodyData),
									PropertiesUtilities
											.getMessage(
													PropertiesConstants.WARNING_DOCUMENTTYPE_DOCUMENTNUMBER_OTHER_HOLDER,
													bodyData));

				      }
					else{
						
					
						List<HolderHistory> lstHolderHistory = new ArrayList<HolderHistory>();
						lstHolderHistory.add(holderHistory);
						
						HolderRequest auxHolderRequest = new HolderRequest();
						auxHolderRequest.setHolderHistories(lstHolderHistory);			
						if(holderRequest.getIdHolderRequestPk()!=null){
							auxHolderRequest.setIdHolderRequestPk(holderRequest.getIdHolderRequestPk());
						}
								
						auxHolderRequest = holderServiceFacade.getHolderRequestByDocumentTypeAndDocumentNumberServiceFacade(auxHolderRequest);
						
						if(auxHolderRequest!= null && !auxHolderRequest.getStateHolderRequest().equals(HolderRequestStateType.CONFIRMED.getCode())){
							
							Object[] bodyData = new Object[1];
							bodyData[0] = auxHolderRequest.getIdHolderRequestPk().toString();
							JSFUtilities.showMessageOnDialog(GeneralConstants.EMPTY_STRING, PropertiesUtilities.getMessage(
												PropertiesConstants.WARNING_DOCUMENTTYPE_DOCUMENTNUMBER_OTHER_HOLDER_REQUEST,
												bodyData));
							JSFUtilities.showSimpleValidationDialog();
							holderHistory.setSecondDocumentNumber(null);
							
							JSFUtilities
							.addContextMessage(
									"frmHolderModify:tabView:inputSecondDocumentNumber",
									FacesMessage.SEVERITY_ERROR,
									PropertiesUtilities
											.getMessage(
													PropertiesConstants.WARNING_DOCUMENTTYPE_DOCUMENTNUMBER_OTHER_HOLDER_REQUEST,
													bodyData),
									PropertiesUtilities
											.getMessage(
													PropertiesConstants.WARNING_DOCUMENTTYPE_DOCUMENTNUMBER_OTHER_HOLDER_REQUEST,
													bodyData));					
							
						}		
		
					}
						 
		      
	      }
		

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Validate exist type document and number document.
	 *
	 * @param idComponent the id component
	 * @return true, if successful
	 */
	@LoggerAuditWeb
	public void validateExistTypeDocumentAndNumberDocument(String idComponent){		
		
		 
		try{
			if(Validations.validateIsNullOrEmpty(holderHistory.getDocumentNumber())
					|| Validations.validateIsNullOrEmpty(holderHistory.getDocumentType())){
				return;
			}
			
			if(holderHistory.getDocumentType()!=null && 
					   !holderHistory.getDocumentType().equals(DocumentType.DIO.getCode()) &&		
					   holderHistory.getDocumentNumber()!=null && 
					   legalRepresentativeHelperBean.getLstLegalRepresentativeHistory()!=null && 
					   legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size()>0){
						
					   for(int i=0;i<legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size();i++){
						   
						   if((legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentType().equals(holderHistory.getDocumentType()) &&
						      legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentNumber().equals(holderHistory.getDocumentNumber()) &&
						      (!legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentType().equals(DocumentType.CI.getCode())						      
						      || !legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentType().equals(DocumentType.CID.getCode())
						      || !legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentType().equals(DocumentType.PAS.getCode())
						      || !legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentType().equals(DocumentType.CDN.getCode())))					      
						      
						      || (legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getSecondDocumentType()!=null && legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getSecondDocumentType().equals(holderHistory.getDocumentType()) &&
						    		  legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getSecondDocumentNumber()!=null && legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getSecondDocumentNumber().equals(holderHistory.getDocumentNumber()))
								      ){
						
							   	Object[] bodyData = new Object[0];
							   	
							   	JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							   			, PropertiesUtilities.getMessage(
														PropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE,bodyData));
								JSFUtilities.showSimpleValidationDialog();
								
							   
							   JSFUtilities
								.addContextMessage(
										idComponent,
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities
												.getMessage(
														PropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE,
														bodyData),
										PropertiesUtilities
												.getMessage(
														PropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE,
														bodyData));

								holderHistory.setDocumentNumber(null);
								newDocumentNumber = null;
								return;
						   }
						   else{
							   if((legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentType().equals(holderHistory.getDocumentType()) &&
									      legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentNumber().equals(holderHistory.getDocumentNumber()) &&
									      (legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentType().equals(DocumentType.CI.getCode())
											    	|| legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentType().equals(DocumentType.PAS.getCode())
											    	|| legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentType().equals(DocumentType.CID.getCode())
											    	|| legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentType().equals(DocumentType.CDN.getCode()))
										  //&& legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getDocumentSource().equals(holderHistory.getDocumentSource())
									      )
									      || (legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getSecondDocumentType()!=null && legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getSecondDocumentType().equals(holderHistory.getDocumentType()) &&
									    		  legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getSecondDocumentNumber()!=null && legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getSecondDocumentNumber().equals(holderHistory.getDocumentNumber()))
											      ){
									
										   	Object[] bodyData = new Object[0];
										   	
										   	JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesUtilities.getMessage(
																	PropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE,bodyData));
											JSFUtilities.showSimpleValidationDialog();
											
										   
										   JSFUtilities
											.addContextMessage(
													idComponent,
													FacesMessage.SEVERITY_ERROR,
													PropertiesUtilities
															.getMessage(
																	PropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE,
																	bodyData),
													PropertiesUtilities
															.getMessage(
																	PropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE,
																	bodyData));

											holderHistory.setDocumentNumber(null); 
											newDocumentNumber = null;
											return;
									   }
						   }
					   }
						
					}
			
			
						
			if(documentValidator.validateFormatDocumentNumber(holderHistory.getDocumentType(),holderHistory.getDocumentNumber(),idComponent))
			{
				
				if(holderHistory.getHolderType()!=null && holderHistory.getHolderType().equals(PersonType.JURIDIC.getCode())){
					   if(holderHistory.getJuridicClass()!=null && holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())){
					      if(holderHistory.getDocumentType()!=null && holderHistory.getDocumentType().equals(DocumentType.RUC.getCode())){
					    	  
					    	    Holder objHolder = new Holder();
								objHolder.setDocumentType(holderHistory.getDocumentType());
								objHolder.setDocumentNumber(holderHistory.getDocumentNumber());	
								objHolder.setListSectorActivity(new ArrayList<Integer>());
								objHolder.getListSectorActivity().add(EconomicActivityType.BANCOS.getCode());
								//objHolder.setEconomicActivity(EconomicActivityType.BANCOS.getCode());
								
								List<Holder>listHolder = accountsFacade.getHoldersServiceFacade(objHolder);
								if(Validations.validateListIsNullOrEmpty(listHolder)){
									
									JSFUtilities
									.addContextMessage(
											idComponent,
											FacesMessage.SEVERITY_ERROR,
											PropertiesUtilities
													.getMessage(
															PropertiesConstants.ERROR_PERSON_JURIDIC_CLASS_TRUST_ECONOMIC_ACTIVITY_BANK,
															new Object[0]),
											PropertiesUtilities
													.getMessage(
															PropertiesConstants.ERROR_PERSON_JURIDIC_CLASS_TRUST_ECONOMIC_ACTIVITY_BANK,
															new Object[0]));
									
								  
									JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesUtilities.getMessage(
											PropertiesConstants.ERROR_PERSON_JURIDIC_CLASS_TRUST_ECONOMIC_ACTIVITY_BANK,new Object[0]));
									
									JSFUtilities.showSimpleValidationDialog();
								    return;
								}
						     
						  }
					   }
				   }
				
				
					//Si no existe en una solicitud verificamos si ya existe un RNT 
					Holder objHolder = new Holder();
					objHolder.setIdHolderPk(holder.getIdHolderPk());
					objHolder.setDocumentType(holderHistory.getDocumentType());
					objHolder.setDocumentNumber(holderHistory.getDocumentNumber());	
					objHolder.setNationality(holderHistory.getNationality());
					
					if(holderHistory.getDocumentSource()!=null){
						objHolder.setDocumentSource(holderHistory.getDocumentSource());
					}
					if(holderHistory.getFirstLastName()!=null){
						objHolder.setFirstLastName(holderHistory.getFirstLastName());
					}
					
						objHolder =	accountsFacade.getHolderByDocumentTypeAndDocumentNumberServiceFacade(objHolder);							
						if(objHolder!=null){
						Object[] bodyData = new Object[2];
						bodyData[0] = objHolder.getDescriptionHolder();
						bodyData[1] = objHolder.getIdHolderPk().toString();					
						JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), PropertiesUtilities.getMessage(
								PropertiesConstants.WARNING_DOCUMENTTYPE_DOCUMENTNUMBER_OTHER_HOLDER,
								bodyData));
						JSFUtilities.showSimpleValidationDialog();
						holderHistory.setDocumentNumber(null);
						holderHistory.setDocumentSource(null);
						newDocumentNumber = null;
						
					
						JSFUtilities
						.addContextMessage(
								idComponent,
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities
										.getMessage(
												PropertiesConstants.WARNING_DOCUMENTTYPE_DOCUMENTNUMBER_OTHER_HOLDER,
												bodyData),
								PropertiesUtilities
										.getMessage(
												PropertiesConstants.WARNING_DOCUMENTTYPE_DOCUMENTNUMBER_OTHER_HOLDER,
												bodyData));
						
					return;	
					}
					 else{
							//Verificamos si existe una solicitud con el tipo y numero de documento ingresado
							
							List<HolderHistory> lstHolderHistory = new ArrayList<HolderHistory>();
							lstHolderHistory.add(holderHistory);
							HolderRequest auxHolderRequest = new HolderRequest();
							auxHolderRequest.setHolderHistories(lstHolderHistory);			
							if(holderRequest.getIdHolderRequestPk()!=null){
								auxHolderRequest.setIdHolderRequestPk(holderRequest.getIdHolderRequestPk());
							}
					
							
							auxHolderRequest = holderServiceFacade.getHolderRequestByDocumentTypeAndDocumentNumberServiceFacade(auxHolderRequest);
					
							if(auxHolderRequest!= null && !auxHolderRequest.getStateHolderRequest().equals(HolderRequestStateType.CONFIRMED.getCode())){
						
								Object[] bodyData = new Object[1];
								bodyData[0] = auxHolderRequest.getIdHolderRequestPk().toString();
								JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), PropertiesUtilities.getMessage(
													PropertiesConstants.WARNING_DOCUMENTTYPE_DOCUMENTNUMBER_OTHER_HOLDER_REQUEST,
													bodyData));
								
								JSFUtilities.showSimpleValidationDialog();
								holderHistory.setDocumentNumber(null);
								holderHistory.setDocumentSource(null);
								newDocumentNumber = null;
								
								JSFUtilities
								.addContextMessage(
										idComponent,
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities
												.getMessage(
														PropertiesConstants.WARNING_DOCUMENTTYPE_DOCUMENTNUMBER_OTHER_HOLDER_REQUEST,
														bodyData),
										PropertiesUtilities
												.getMessage(
														PropertiesConstants.WARNING_DOCUMENTTYPE_DOCUMENTNUMBER_OTHER_HOLDER_REQUEST,
														bodyData));
								return;
							}
						}
			    	 
							
				loadNewDocumentNumber();
			}			
			else{
				holderHistory.setDocumentNumber(null);
				newDocumentNumber = null;
			    return;
			}	
		   
		}
		catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}		
		
	}
	
	//Segun Issue 1050
	/**
	 * Cargando el numero de documento
	 */
	public void loadNewDocumentNumber(){
		
		if(holderHistory.getDocumentType()!=null && holderHistory.getDocumentNumber() != null){
			
			newDocumentNumber = holderHistory.getDocumentNumber();
			
			if(holderHistory.getDocumentType().equals(DocumentType.CIE.getCode())){
				newDocumentNumber = "E-" + holderHistory.getDocumentNumber();
			}
			if(holderHistory.getDocumentType().equals(DocumentType.DCD.getCode())){
				newDocumentNumber = "DCD" + holderHistory.getDocumentNumber();
			}
			if(holderHistory.getDocumentType().equals(DocumentType.DCC.getCode())){
				newDocumentNumber = "DCC" + holderHistory.getDocumentNumber();
			}
			if(holderHistory.getDocumentType().equals(DocumentType.DCR.getCode())){
				newDocumentNumber = "DCR" + holderHistory.getDocumentNumber();
			}
			
			if(holderHistory.getDocumentIssuanceDate() != null){
				
				if(holderHistory.getDocumentType().equals(DocumentType.DCD.getCode()) ||
					holderHistory.getDocumentType().equals(DocumentType.DCC.getCode()) ||
					holderHistory.getDocumentType().equals(DocumentType.DCR.getCode()) ||
					holderHistory.getDocumentType().equals(DocumentType.DCD.getCode()) ){
					
					newDocumentNumber = newDocumentNumber + holderHistory.getDocumentIssuanceDate()+"";
				}
			}
		}
		
	}
	
	/**
	 * Gets the streamed content temporal file holder.
	 *
	 * @param holderReqFileHistory the holder req file history
	 * @return the streamed content temporal file holder
	 */
	public StreamedContent getStreamedContentTemporalFileHolder(
			HolderReqFileHistory holderReqFileHistory) {
			
		if(holderReqFileHistory.isWasModified()){

		try {
			InputStream inputStream;

			inputStream = new ByteArrayInputStream(holderReqFileHistory.getFileHolderReq());
			streamedContentFile = new DefaultStreamedContent(inputStream, null,
					holderReqFileHistory.getFilename());
			inputStream.close();

		 } catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		 }
		
		}
		else{
			streamedContentFile = getStreamedContentFileHolderFile(holder.getIdHolderPk(),holderReqFileHistory.getDocumentType());
		}
		

		return streamedContentFile;
	}
	
	/**
	 * Gets the streamed content file holder file.
	 *
	 * @param id the id
	 * @param documentType the document type
	 * @return the streamed content file holder file
	 */
	public StreamedContent getStreamedContentFileHolderFile(Long id,
			Integer documentType) {

			try {
			HolderFile auxHolderFile = holderServiceFacade
					.getContentFileHolderServiceFacade(id, documentType);
			InputStream inputStream;

			inputStream = new ByteArrayInputStream(auxHolderFile.getFileHolder());
			streamedContentFile = new DefaultStreamedContent(inputStream, null,auxHolderFile.getFilename());
			inputStream.close();

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

		return streamedContentFile;
	}
	
	/**
	 * Validate web page format.
	 *
	 * @param component the component
	 */
	public void validateWebPageFormat(String component){
		
		JSFUtilities.setValidViewComponent(component);
    	if(Validations.validateIsNotNullAndNotEmpty(holderHistory.getWebsite())){
    		if(!CommonsUtilities.matchWebPage(holderHistory.getWebsite())){
    			JSFUtilities.addContextMessage(component,
        				FacesMessage.SEVERITY_ERROR,
        				PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDER_VALIDATION_WEB_PAGE_FORMAT), 
        				PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDER_VALIDATION_WEB_PAGE_FORMAT));
    			
    			JSFUtilities.showMessageOnDialog(
    					PropertiesConstants.MESSAGES_ALERT, null,
						PropertiesConstants.ADM_HOLDER_VALIDATION_WEB_PAGE_FORMAT,
						null);
    			//Clean the web site
    			holderHistory.setWebsite(null);
				JSFUtilities.showSimpleValidationDialog();
    		}
    	}
	}
	
	
	/**
	 * Validate participant with holder.
	 *
	 * @param idParticipant the id participant
	 * @param holder the holder
	 * @return true, if successful
	 */
	public boolean validateParticipantWithHolder(Long idParticipant,Holder holder){
	      boolean flag=false;
	      
	      if(!validateIsParticipant()){
	    	  if((idParticipant==null) && holder.getIdHolderPk()!=null){
	    		  flag = true;
	    		  return true;
	    	  }
	      }
	     
	      try{
	      
	      if(idParticipant!=null && holder.getIdHolderPk()!=null){
	      int count = holderServiceFacade.getCountParticipantWithAccountHolderServiceFacade(idParticipant,holder.getIdHolderPk());
	      	  
	         if(count>=1 || holder.getParticipant().getIdParticipantPk().equals(idParticipant)){
	        	flag=true;
	         }	         
	         
	         if(!flag){
	        	    
	        	    Object[] bodyData = new Object[1];
		        	bodyData[0] = holder.getIdHolderPk().toString();
		 			
		        	if(!validateIsParticipant()){
		        	JSFUtilities.showMessageOnDialog(
		 					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
		 					PropertiesUtilities.getMessage(PropertiesConstants.NO_RELATIONSHIP_BETWEEN_THE_HOLDER_AND_PARTICIPANT,bodyData));
		        	
		        	JSFUtilities.addContextMessage("frmHolderModify:tabView:inputTextCui",
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities.getMessage(PropertiesConstants.NO_RELATIONSHIP_BETWEEN_THE_HOLDER_AND_PARTICIPANT,bodyData),
							PropertiesUtilities.getMessage(PropertiesConstants.NO_RELATIONSHIP_BETWEEN_THE_HOLDER_AND_PARTICIPANT,bodyData));
			
		        	
		 			}
		        	else{
		        		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),	
		    					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_HOLDER_REQUEST_NO_RESULT,bodyData));
		        		
		        		JSFUtilities.addContextMessage("frmHolderModify:tabView:inputTextCui",
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_HOLDER_REQUEST_NO_RESULT,bodyData),
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_HOLDER_REQUEST_NO_RESULT,bodyData));
				
		    	    }
		        	
		        	JSFUtilities.showSimpleValidationDialog();
		        		
		        	
	         }
	         
	        }
	      }
	      catch(Exception e){
	    	  excepcion.fire(new ExceptionToCatchEvent(e));
	      }
	      return flag;
		}
	

	/**
	 * Load modification page.
	 */
	public void loadHolderInformation() {

	try
		{
		cleanFileImage();
		setViewOperationType(ViewOperationsType.REGISTER.getCode());
		
		//Deshabilitando el envio asfi
		flagSendASFI = false;
		
		if(participantRequester.getIdParticipantPk()==null){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
			PropertiesUtilities.getMessage(PropertiesConstants.MSG_PARTICIPANT_REQUIERED));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		if(holder!=null && holder.getIdHolderPk()!=null){
			
			pepPerson = new PepPerson();
			participant = new Participant();
			holderHistory = new HolderHistory();
			previousModificationTO = new PreviousModificationTO();
			lstHolderReqFileHistory = new ArrayList<HolderReqFileHistory>();
			lstHolderReqChangesFileHistory = new ArrayList<HolderReqFileHistory>();
			List<LegalRepresentativeHistory> lstLegalRepresentativeHistory = new ArrayList<LegalRepresentativeHistory>();
			indChangesNeedsConfirm = false;
			
			int count =	holderServiceFacade.getExistHolderServiceFacade(holder.getIdHolderPk()).intValue();
	    
			if(count>0){
				
				Long idParticipant = null;
				if(participantRequester!=null){
					if(participantRequester.getIdParticipantPk()!=null){
						idParticipant = participantRequester.getIdParticipantPk();
					}
				}
				
				holder = holderServiceFacade.getHolderServiceFacade(holder);
				
				if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
					Object[] bodyData = new Object[1];
					bodyData[0] = holder.getIdHolderPk().toString();
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
							PropertiesUtilities.getMessage(PropertiesConstants.WARNING_DISTINCT_REGISTERED_CUI,	bodyData));
					JSFUtilities.showSimpleValidationDialog();
					holder = new Holder();
					return;
				}
				
				if(!validateParticipantWithHolder(idParticipant,holder)){
					holder = new Holder();
					return;
				}
				
				HolderRequest holderRequest = holderServiceFacade.getHolderRequestByRntCodeServiceFacade(holder.getIdHolderPk());
				
				if(holderRequest!=null){
				
					if(!holderRequest.getStateHolderRequest().equals(HolderRequestStateType.CONFIRMED.getCode())){
						
						Object[] bodyData = new Object[2];
						bodyData[0] = holderRequest.getIdHolderRequestPk().toString();
						String stateDescription = requestStateDescription.get(holderRequest.getStateHolderRequest()).toString();
						bodyData[1] = stateDescription;
						JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), PropertiesUtilities.getMessage(
											PropertiesConstants.ERROR_ADM_MODIFICATION_HOLDER_REQUEST_EXIST,
											bodyData));
						JSFUtilities.showSimpleValidationDialog();

						holder = new Holder();
						
						JSFUtilities
						.addContextMessage(
								"frmHolderModify:tabView:inputTextCui",
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities
										.getMessage(
												PropertiesConstants.ERROR_ADM_MODIFICATION_HOLDER_REQUEST_EXIST,
												bodyData),
								PropertiesUtilities
										.getMessage(
												PropertiesConstants.ERROR_ADM_MODIFICATION_HOLDER_REQUEST_EXIST,
												bodyData));
						
						return;
					}
				}
		
				holder = holderServiceFacade.getHolderServiceFacade(holder.getIdHolderPk());
				
				HolderReqFileHistory holderReqFileHistory;
				for(int f=0;f<holder.getHolderFile().size();f++){
					holderReqFileHistory = new HolderReqFileHistory();
					holderReqFileHistory.setDescription(holder.getHolderFile().get(f).getDescription());
					holderReqFileHistory.setDocumentType(holder.getHolderFile().get(f).getDocumentType());
					holderReqFileHistory.setFilename(holder.getHolderFile().get(f).getFilename());
					holderReqFileHistory.setHolderReqFileHisType(HolderReqFileHisType.MODIFICATION.getCode());
					holderReqFileHistory.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
					holderReqFileHistory.setStateFile(HolderFileStateType.REGISTERED.getCode());
					holderReqFileHistory.setRegistryDate(CommonsUtilities.currentDate());
					holderReqFileHistory.setRegistryUser(userInfo.getUserAccountSession().getUserName());
										
					
					if(holder.getHolderFile().get(f).getDocumentType() != 2847) {
						lstHolderReqFileHistory.add(holderReqFileHistory);
					} else {
						//indChangesNeedsConfirm = true;
						lstHolderReqChangesFileHistory.add(holderReqFileHistory);
					}
				}
		
				
				
				if((holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())) || (holder.getStateHolder().equals(HolderStateType.BLOCKED.getCode()))){
				
		
					if(participantRequester!=null){
						Participant filter = new Participant();
						filter.setIdParticipantPk(participantRequester.getIdParticipantPk());
						participantRequester = accountsFacade.findParticipantByFilters(filter);
					}
					
					participant=holder.getParticipant();
					
					getLstBooleanType();
					
					getLstParticipant();
					getLstPersonType();
					getLstSexType();
					getLstJuridicClass();
					getLstGeographicLocation();
					getLstGeographicLocationWithoutNationalCountry();
					getLstRolePep();
					getLstCategoryPep();
					getLstEconomicSector();
					getLstDocumentSource();
					getGradePep();
					getListEconomicActivityWithoutSector();
					
					holderHistory.setLegalResidenceCountry(holder.getLegalResidenceCountry());
					listDepartmentLocation = getLstDepartmentLocation(holder.getLegalResidenceCountry());
					
					holderHistory.setLegalDepartment(holder.getLegalDepartment());
					listProvinceLocation = getLstProvinceLocation(holder.getLegalDepartment());

					holderHistory.setLegalProvince(holder.getLegalProvince());
					
					listMunicipalityLocation = getLstMunicipalityLocation(holder.getLegalProvince());
					
					holderHistory.setLegalDistrict(holder.getLegalDistrict());
					
					
					holderHistory.setPostalResidenceCountry(holder.getPostalResidenceCountry());
					listDepartmentPostalLocation = getLstDepartmentLocation(holder.getPostalResidenceCountry());
					
					holderHistory.setPostalDepartment(holder.getPostalDepartment());			
					listProvincePostalLocation = getLstProvinceLocation(holder.getPostalDepartment());

					
					holderHistory.setPostalProvince(holder.getPostalProvince());
					
					listMunicipalityPostalLocation = getLstMunicipalityLocation(holder.getPostalProvince());
					
					holderHistory.setPostalDistrict(holder.getPostalDistrict());
					

					if(holder.getBirthDate()!=null){
						holderHistory.setBirthDate(holder.getBirthDate());
						previousModificationTO.setPreviousBirthDate(holder.getBirthDate());
					}
					
					if(holder.getDocumentType()!=null){
						holderHistory.setDocumentType(holder.getDocumentType());
						previousModificationTO.setPreviousDocumentType(holder.getDocumentType());
					}
					
					if(holder.getDocumentNumber()!=null){
						holderHistory.setDocumentNumber(getDocumentNumber(holder.getDocumentNumber(), holderHistory.getDocumentType()));
						previousModificationTO.setPreviousDocumentNumber(holder.getDocumentNumber());
					}
					
					if(holder.getDocumentIssuanceDate()!=null){
						holderHistory.setDocumentIssuanceDate(holder.getDocumentIssuanceDate());
					}
					//jgh
					if(Validations.validateIsNotNullAndNotEmpty(holder.getFundAdministrator())){
						holderHistory.setFundAdministrator(holder.getFundAdministrator());
						flagNemonicAsfi = true;
					}
					
					if(Validations.validateIsNotNullAndNotEmpty(holder.getIndCanNegotiate())){
						holderHistory.setIndCanNegotiate(holder.getIndCanNegotiate());
					}
					
//					if(Validations.validateIsNotNullAndNotEmpty(holder.getFundType())){
//						holderHistory.setFundType(holder.getFundType());
//						flagNemonicFund = true;
//					}
					if(Validations.validateIsNotNullAndNotEmpty(holder.getMnemonic())){
						holderHistory.setMnemonic(holder.getMnemonic());
						disabledNemonic = false;
						flagNemonicFund = true;
					}
					if(Validations.validateIsNotNullAndNotEmpty(holder.getTransferNumber())){
						holderHistory.setTransferNumber(holder.getTransferNumber());
						flagTransferNumber = true;
						disabledTransferNumber = false;
					}
					if(Validations.validateIsNotNullAndNotEmpty(holder.getRelatedCui())){
						holderHistory.setRelatedCui(holder.getRelatedCui());
						flagCUIRelated = true;
					}
					
					if(Validations.validateIsNotNull(holder.getInvestorType())){
						listInvestorType = investorTypeFacade.getLstInvestorTypeModelTO(new InvestorTypeModelTO());
						holderHistory.setInvestorType(holder.getInvestorType());
					}
					
					holderHistory.setEconomicActivity(holder.getEconomicActivity());
					
					holderHistory.setEconomicSector(holder.getEconomicSector());
					
					holderHistory.setIndSirtexNeg(holder.getIndSirtexNeg());
					
					holderHistory.setIndFatca(holder.getIndFatca());
					
					holderHistory.setFilePhotoName(holder.getFilePhotoName());
					
					if(Validations.validateIsNotNullAndNotEmpty(holder.getFundType()))
						holderHistory.setFundType(holder.getFundType());
					
					changeSelectedEconomicSector();
					
					holderHistory.setEmail(holder.getEmail());
					
					holderHistory.setFaxNumber(holder.getFaxNumber());
					
					holderHistory.setFirstLastName(holder.getFirstLastName());
					
					holderHistory.setFullName(holder.getFullName());
					
					holderHistory.setHolderType(holder.getHolderType());
					
					holderHistory.setHomePhoneNumber(holder.getHomePhoneNumber());
					
					if(holder.getIndResidence()!=null){
						holderHistory.setIndResidence(holder.getIndResidence());
						previousModificationTO.setPreviousResidence(holder.getIndResidence());
					}
					
					holderHistory.setJuridicClass(holder.getJuridicClass());
					
		
					if(holder.getLegalAddress()!=null){
						holderHistory.setLegalAddress(holder.getLegalAddress());
						previousModificationTO.setPreviousLegalAddress(holder.getLegalAddress());
					}
		
					holderHistory.setLegalDepartment(holder.getLegalDepartment());		
					holderHistory.setLegalDistrict(holder.getLegalDistrict());
					holderHistory.setLegalProvince(holder.getLegalProvince());	
					
		
					if(holder.getLegalResidenceCountry()!=null){
						holderHistory.setLegalResidenceCountry(holder.getLegalResidenceCountry());
						previousModificationTO.setPreviousCountryResident(holder.getLegalResidenceCountry());
					}
		
					holderHistory.setMobileNumber(holder.getMobileNumber());
					holderHistory.setName(holder.getName());
					
		
					if(holder.getNationality()!=null){
						holderHistory.setNationality(holder.getNationality());
						previousModificationTO.setPreviousNationality(holder.getNationality());
					}
		
					holderHistory.setOfficePhoneNumber(holder.getOfficePhoneNumber());
					
		
					if(holder.getPostalAddress()!=null){
						holderHistory.setPostalAddress(holder.getPostalAddress());
						previousModificationTO.setPreviousPostalAddress(holder.getPostalAddress());
					}
		
					holderHistory.setPostalDepartment(holder.getPostalDepartment());
					holderHistory.setPostalDistrict(holder.getPostalDistrict());
					holderHistory.setPostalProvince(holder.getPostalProvince());	
					holderHistory.setPostalResidenceCountry(holder.getPostalResidenceCountry());	
					
					if(holder.getSecondDocumentType()!=null){
						holderHistory.setSecondDocumentType(holder.getSecondDocumentType());
						previousModificationTO.setPreviousSecondDocumentType(holder.getSecondDocumentType());
					}
					
					if(holder.getSecondDocumentNumber()!=null){
						holderHistory.setSecondDocumentNumber(getDocumentNumber(holder.getSecondDocumentNumber(), holder.getSecondDocumentType()));
						previousModificationTO.setPreviousSecondDocumentNumber(holder.getSecondDocumentNumber());
					}
					
					holderHistory.setSecondLastName(holder.getSecondLastName());
					
					if(holder.getSecondNationality()!=null){
						holderHistory.setSecondNationality(holder.getSecondNationality());
						previousModificationTO.setPreviousSecondNationality(holder.getSecondNationality());
					}
					
					holderHistory.setSex(holder.getSex());
					holderHistory.setStateHolderHistory(holder.getStateHolder());
					
				
					if(holder.getIndDisabled()!=null){
						holderHistory.setIndDisabled(holder.getIndDisabled());
						previousModificationTO.setPreviousIncapacity(holder.getIndDisabled());
					}
					
					holderHistory.setIndMinor(holder.getIndMinor());		
						
					if(holder.getDocumentSource()!=null){
						documentTO.setIndicatorDocumentSource(true);
						holderHistory.setDocumentSource(holder.getDocumentSource());
					}
					
					holderHistory.setMarriedLastName(holder.getMarriedLastName());
					holderHistory.setCivilStatus(holder.getCivilStatus());
					holderHistory.setMainActivity(holder.getMainActivity());
					holderHistory.setJobSourceBusinessName(holder.getJobSourceBusinessName());
					holderHistory.setJobDateAdmission(holder.getJobDateAdmission());
					holderHistory.setAppointment(holder.getAppointment());
					holderHistory.setJobAddress(holder.getJobAddress());
					holderHistory.setTotalIncome(holder.getTotalIncome());
					holderHistory.setPerRefFullName(holder.getPerRefFullName());
					holderHistory.setPerRefLandLine(holder.getPerRefLandLine());
					holderHistory.setPerRefCellPhone(holder.getPerRefCellPhone());
					holderHistory.setComerRefBusinessName(holder.getComerRefBusinessName());
					holderHistory.setComerRefLandLine(holder.getComerRefLandLine());
					holderHistory.setEntityPublicAppointment(holder.getEntityPublicAppointment());
					holderHistory.setNitNaturalPerson(holder.getNitNaturalPerson());
					holderHistory.setWebsite(holder.getWebsite());
					holderHistory.setFundCompanyEnrollment(holder.getFundCompanyEnrollment());
					holderHistory.setConstitutionDate(holder.getConstitutionDate());
					
					if(holder.getFilePhoto() != null){
						if (holder.getFilePhotoName()==null) {
							setPhotoHolderName("photoHolder");
						}else {
							setPhotoHolderName(holder.getFilePhotoName());
						}
						JSFUtilities.putSessionMap("sessStreamedPhoto", holder.getFilePhoto());
						holderHistory.setFilePhoto(holder.getFilePhoto());
					}
					
					if(holder.getFileSigning() != null){
						setSigningHolderName("signingHolder");			
						JSFUtilities.putSessionMap("sessStreamedSigning", holder.getFileSigning());
						holderHistory.setFileSigning(holder.getFileSigning());
					}
					
					// verificando que tipo de documento es CIE
					if(DocumentType.CIE.getCode().equals(holder.getDocumentType())){
						this.documentTO.setIndicatorAlphanumericWithDash(true);
					}
		
					if(holderHistory.getIndMinor()!=null && (holderHistory.getIndMinor().equals(BooleanType.YES.getCode()))){
						holderHistory.setIndicatorMinor(true);	
						flagIndicatorMinorHolder = true;
					}
					else if(holderHistory.getIndMinor()!=null && (holderHistory.getIndMinor().equals(BooleanType.NO.getCode()))){
						holderHistory.setIndicatorMinor(false);
						flagIndicatorMinorHolder = false;
					}
					
					if(holderHistory.getIndDisabled()!=null && (holderHistory.getIndDisabled().equals(BooleanType.YES.getCode()))){
						holderHistory.setIndicatorDisabled(true);
						flagHolderIndicatorInability = true;
						
					}
					else if(holderHistory.getIndDisabled()!=null && (holderHistory.getIndDisabled().equals(BooleanType.NO.getCode()))){
						holderHistory.setIndicatorDisabled(false);	
						flagHolderIndicatorInability = false;
						
					}
					
					if(holderHistory.getIndDisabled()!=null && holderHistory.getIndDisabled().equals(BooleanType.YES.getCode()) &&
					   holderHistory.getIndMinor()!=null && holderHistory.getIndMinor().equals(BooleanType.NO.getCode())){
						disabledRepresentativeButton = true;
					}
					else if(holderHistory.getIndDisabled()!=null && holderHistory.getIndDisabled().equals(BooleanType.YES.getCode()) && 
							holderHistory.getIndMinor()!= null && holderHistory.getIndMinor().equals(BooleanType.YES.getCode())) {
						disabledRepresentativeButton = false;
					}
					else if(holderHistory.getIndDisabled()!=null && holderHistory.getIndDisabled().equals(BooleanType.NO.getCode()) &&
							holderHistory.getIndMinor()!=null && holderHistory.getIndMinor().equals(BooleanType.YES.getCode())){
						disabledRepresentativeButton = false;
					}
					else if(holderHistory.getIndDisabled()!=null && holderHistory.getIndDisabled().equals(BooleanType.NO.getCode()) &&
							holderHistory.getIndMinor()!=null && holderHistory.getIndMinor().equals(BooleanType.NO.getCode())){
						disabledRepresentativeButton = true;
					}
										
					if(holderHistory.getIndResidence()!=null && (holderHistory.getIndResidence().equals(BooleanType.YES.getCode()))){
						disabledCountryResidentHolder = true;
					}
					else if(holderHistory.getIndResidence()!=null && (holderHistory.getIndResidence().equals(BooleanType.NO.getCode()))){
						disabledCountryResidentHolder = false;
					}
					
					DocumentTO documentTo = documentValidator.loadMaxLenghtType(holderHistory.getDocumentType());
					maxLenghtDocumentNumberHolder = documentTo.getMaxLenghtDocumentNumber();
					documentTO = documentValidator.validateEmissionRules(holderHistory.getDocumentType());
					numericTypeDocumentHolder = documentTo.isNumericTypeDocument();
					
					previousModificationTO.setPreviousMaxLenghtDocumentNumberHolder(maxLenghtDocumentNumberHolder);
					
					if(holderHistory.getSecondDocumentType()!=null){
						documentTo = documentValidator.loadMaxLenghtType(holderHistory.getSecondDocumentType());			
						numericTypeDocumentHolder = documentTo.isNumericTypeDocument();
						maxLenghtSecondDocumentNumberHolder = documentTo.getMaxLenghtDocumentNumber();
					
					}
					
					inFalseFlagNationalForeign();
					
					if (holderHistory.getNationality() != null
							&& (holderHistory.getNationality()
									.equals(countryResidence))) {
						
						if (BooleanType.YES.getCode().equals(
								holderHistory.getIndResidence())) {
							flagNationalResident = true;
						} else if (BooleanType.NO.getCode().equals(
								holderHistory.getIndResidence())) {
							flagNationalNotResident = true;
						}

					} else if (holderHistory.getNationality() != null
							&& !(holderHistory.getNationality()
									.equals(countryResidence))) {
						
						if (BooleanType.YES.getCode().equals(
								holderHistory.getIndResidence())) {
							flagForeignResident = true;
						} else if (BooleanType.NO.getCode().equals(
								holderHistory.getIndResidence())) {
							flagForeignNotResident = true;
						}

					}
																				
					if(holder.getHolderType().equals(PersonType.NATURAL.getCode())){			
						flagHolderJuridicPerson = false;
						flagHolderNaturalPerson = true;
						
						listPepGradeWithPersonType = listPepGrade.stream().filter(param -> param.getShortInteger().equals(PersonType.NATURAL.getCode())).collect(Collectors.toList());
					
									
					}else if(holder.getHolderType().equals(PersonType.JURIDIC.getCode())){			
						flagHolderJuridicPerson = true;
						flagHolderNaturalPerson = false;
						disabledRepresentativeButton = false;
						disabledIndResident = true;
						disabledCountryResidentHolder=true;
						
						listPepGradeWithPersonType = listPepGrade.stream().filter(param -> param.getShortInteger().equals(PersonType.JURIDIC.getCode())).collect(Collectors.toList());
						
						if(holderHistory.getNationality().equals(countryResidence)){
							holderHistory.setIndResidence(BooleanType.YES.getCode());
							holderHistory.setLegalResidenceCountry(countryResidence);
						}
						
//						if(holderHistory.getJuridicClass()!=null 
//								&& holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode()) ){
//						   String numberDocument[] = holderHistory.getDocumentNumber().trim().split(GeneralConstants.HYPHEN);
//						   holderHistory.setDocumentNumber(numberDocument[0].toString());
//						   disabledDocNumberBankCorrelative = true;
//						}
//						else{
//							disabledDocNumberBankCorrelative=false;
//						}
					}
					
					loadHolderDocumentType();
					loadHolderSecondDocumentType();					
					loadDocumentTypeHolderParameter();
					loadNewDocumentNumber();
					
					if(holderHistory.getSecondDocumentType() != null && holderHistory.getSecondDocumentNumber() != null){
						newDocumentNumberSecondNationality = holderHistory.getSecondDocumentNumber();
						if(holderHistory.getSecondDocumentType().equals(DocumentType.CIE.getCode())){
							newDocumentNumberSecondNationality = "E-" + holderHistory.getSecondDocumentNumber();
						}
					}
					
			loadHolderHistoryPepExists(holder.getIdHolderPk());
						
			legalRepresentativeHelperBean.initializeLstLegalRepresentativeHistory();
			legalRepresentativeHelperBean.getLegalRepresentativeTO().setViewOperationType(ViewOperationsType.REGISTER.getCode());
			
			if(holder.getRepresentedEntity()!=null){
				for(int i=0;i<holder.getRepresentedEntity().size();i++){
					
					
					LegalRepresentativeHistory legalRepresentativeHistory = new LegalRepresentativeHistory();
					
					legalRepresentativeHistory.setRepresentativeClass(holder.getRepresentedEntity().get(i).getRepresentativeClass());
					
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getIdLegalRepresentativePk()!=null){
						legalRepresentativeHistory.setIdRepresentativeHistoryPk(holder.getRepresentedEntity().get(i).getLegalRepresentative().getIdLegalRepresentativePk());
					}
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getBirthDate()!=null){
						legalRepresentativeHistory.setBirthDate(holder.getRepresentedEntity().get(i).getLegalRepresentative().getBirthDate());
					}
					
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentIssuanceDate()!=null){
						legalRepresentativeHistory.setDocumentIssuanceDate(holder.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentIssuanceDate());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentNumber()!=null){
						legalRepresentativeHistory.setDocumentNumber(holder.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentNumber());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentType()!=null){
						legalRepresentativeHistory.setDocumentType(holder.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentType());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getEconomicActivity()!=null){
						legalRepresentativeHistory.setEconomicActivity(holder.getRepresentedEntity().get(i).getLegalRepresentative().getEconomicActivity());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getEconomicSector()!=null){
						legalRepresentativeHistory.setEconomicSector(holder.getRepresentedEntity().get(i).getLegalRepresentative().getEconomicSector());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getEmail()!=null){
						legalRepresentativeHistory.setEmail(holder.getRepresentedEntity().get(i).getLegalRepresentative().getEmail());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getFaxNumber()!=null){
						legalRepresentativeHistory.setFaxNumber(holder.getRepresentedEntity().get(i).getLegalRepresentative().getFaxNumber());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getFirstLastName()!=null){
						legalRepresentativeHistory.setFirstLastName(holder.getRepresentedEntity().get(i).getLegalRepresentative().getFirstLastName());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getFullName()!=null){
						legalRepresentativeHistory.setFullName(holder.getRepresentedEntity().get(i).getLegalRepresentative().getFullName());
					}
					
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getHomePhoneNumber()!=null){
						legalRepresentativeHistory.setHomePhoneNumber(holder.getRepresentedEntity().get(i).getLegalRepresentative().getHomePhoneNumber());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getIndResidence()!=null){
						
						legalRepresentativeHistory.setIndResidence(holder.getRepresentedEntity().get(i).getLegalRepresentative().getIndResidence()); 
					}
					
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyApp()!=null){
						legalRepresentativeHistory.setLastModifyApp(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyApp());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyDate()!=null){
						legalRepresentativeHistory.setLastModifyDate(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyDate());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyIp()!=null){
						legalRepresentativeHistory.setLastModifyIp(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyIp());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyUser()!=null){
						legalRepresentativeHistory.setLastModifyUser(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyUser());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalAddress()!=null){
						legalRepresentativeHistory.setLegalAddress(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalAddress());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalDepartment()!=null){
						legalRepresentativeHistory.setLegalDepartment(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalDepartment());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalDistrict()!=null){
						legalRepresentativeHistory.setLegalDistrict(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalDistrict());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalProvince()!=null){
						legalRepresentativeHistory.setLegalProvince(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalProvince());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalResidenceCountry()!=null){
						legalRepresentativeHistory.setLegalResidenceCountry(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalResidenceCountry());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getMobileNumber()!=null){
						legalRepresentativeHistory.setMobileNumber(holder.getRepresentedEntity().get(i).getLegalRepresentative().getMobileNumber());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getName()!=null){
						legalRepresentativeHistory.setName(holder.getRepresentedEntity().get(i).getLegalRepresentative().getName()); 
					}
					
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getNationality()!=null){
						legalRepresentativeHistory.setNationality(holder.getRepresentedEntity().get(i).getLegalRepresentative().getNationality());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getOfficePhoneNumber()!=null){
						legalRepresentativeHistory.setOfficePhoneNumber(holder.getRepresentedEntity().get(i).getLegalRepresentative().getOfficePhoneNumber());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalDepartment()!=null){
						legalRepresentativeHistory.setPostalDepartment(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalDepartment());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalDistrict()!=null){
						legalRepresentativeHistory.setPostalDistrict(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalDistrict());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalProvince()!=null){
						legalRepresentativeHistory.setPostalProvince(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalProvince());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalResidenceCountry()!=null){
						legalRepresentativeHistory.setPostalResidenceCountry(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalResidenceCountry());
					}
					
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalAddress()!=null){
						legalRepresentativeHistory.setPostalAddress(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalAddress());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getSecondDocumentNumber()!=null){
						legalRepresentativeHistory.setSecondDocumentNumber(holder.getRepresentedEntity().get(i).getLegalRepresentative().getSecondDocumentNumber());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getSecondLastName()!=null){
						legalRepresentativeHistory.setSecondLastName(holder.getRepresentedEntity().get(i).getLegalRepresentative().getSecondLastName());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getSecondNationality()!=null){
						legalRepresentativeHistory.setSecondNationality(holder.getRepresentedEntity().get(i).getLegalRepresentative().getSecondNationality());	
					}
					
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getSecondDocumentType()!=null){
						legalRepresentativeHistory.setSecondDocumentType(holder.getRepresentedEntity().get(i).getLegalRepresentative().getSecondDocumentType());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getSex()!=null){
						legalRepresentativeHistory.setSex(holder.getRepresentedEntity().get(i).getLegalRepresentative().getSex());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getState()!=null){
						legalRepresentativeHistory.setStateRepreHistory(holder.getRepresentedEntity().get(i).getLegalRepresentative().getState());
					}
					
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPersonType()!=null){
						legalRepresentativeHistory.setPersonType(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPersonType());
					}
					
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentSource()!=null){
						legalRepresentativeHistory.setDocumentSource(holder.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentSource());
					}
					
					legalRepresentativeHelperBean.loadLegalRepresentativePepExists(legalRepresentativeHistory.getIdRepresentativeHistoryPk());
					
					legalRepresentativeHistory.setRegistryUser(userInfo.getUserAccountSession().getUserName());
					
					legalRepresentativeHistory.setStateRepreHistory(HolderRequestStateType.REGISTERED.getCode());
					
					legalRepresentativeHistory.setRepreHistoryType(HolderRequestType.MODIFICATION.getCode());
					
					legalRepresentativeHistory.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
					
					legalRepresentativeHistory.setRepresentativeTypeDescription(PersonType.get(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPersonType()).getValue());
				
					legalRepresentativeHistory.setDocumentTypeDescription(DocumentType.get(holder.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentType()).getValue());
				
					RepresentativeFileHistory representantiveFileHistory;
					
					List<RepresentativeFileHistory> lstRepresentativeFileHistory = new ArrayList<RepresentativeFileHistory>();
					
					for(int j=0;j<holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalRepresentativeFile().size();j++){
						
						representantiveFileHistory = new RepresentativeFileHistory();					
						representantiveFileHistory.setIdRepresentFileHisPk(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalRepresentativeFile().get(j).getIdLegalRepreFilePk());
						representantiveFileHistory.setFlagByLegalRepresentative(true);
						representantiveFileHistory.setDescription(
								holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalRepresentativeFile().get(j).getDescription());
						
						representantiveFileHistory.setDocumentType(
								holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalRepresentativeFile().get(j).getDocumentType());
						
						representantiveFileHistory.setFilename(
								holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalRepresentativeFile().get(j).getFilename());
						
						representantiveFileHistory.setRepresentFileHistType(HolderReqFileHisType.MODIFICATION.getCode());
						
						representantiveFileHistory.setStateFile(HolderFileStateType.REGISTERED
								.getCode());
						
						representantiveFileHistory.setRegistryDate(CommonsUtilities.currentDate());
						
						representantiveFileHistory.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST
								.getCode());
						
						representantiveFileHistory.setRegistryUser(userInfo.getUserAccountSession().getUserName());
						
						LegalRepresentativeHistory auxLegalRepresentativeHistory = new LegalRepresentativeHistory();
						
						auxLegalRepresentativeHistory.setIdRepresentativeHistoryPk(
								holder.getRepresentedEntity().get(i).getLegalRepresentative().getIdLegalRepresentativePk());
						
						representantiveFileHistory.setLegalRepresentativeHistory(auxLegalRepresentativeHistory);
						
						lstRepresentativeFileHistory.add(representantiveFileHistory);
						
					}
					
					legalRepresentativeHistory.setRepresenteFileHistory(lstRepresentativeFileHistory);
					
					lstLegalRepresentativeHistory.add(legalRepresentativeHistory);						
					
					legalRepresentativeHelperBean.setLstLegalRepresentativeHistory(lstLegalRepresentativeHistory);
				}
				
				if(legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size()>0){
					legalRepresentativeHelperBean.setFlagModifyLegalRepresentantive(true);
				}
			}
		 }
		 else{

					Object[] bodyData = new Object[2];

					bodyData[0] = holder.getIdHolderPk().toString();
					String stateDescription = holderStateDescription.get(holder.getStateHolder()).toString();
					bodyData[1] = stateDescription;
					
					 JSFUtilities.addContextMessage("frmHolderModify:tabView:inputTextCui",
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_CUI_NO_MODIFICATION,bodyData),
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_CUI_NO_MODIFICATION,bodyData));
					
					holder = new Holder();
					return;
		 }
		}		
			
		}
		else if(holder!=null && holder.getIdHolderPk()==null){
			cleanAll();
		}
		
		if(participantRequester!=null && participantRequester.getAccountClass()!=null &&
				holderHistory.getHolderType()!=null){
			//clientInformationTO=clientInformationValidator.validateShowClientInformation(participant.getAccountClass(),holderHistory.getHolderType());
			
			//issue 1027 segun lo conversado con Cinthi ya no se muestran los secciones:
			//Ocupación Principal o Actividad Principal		
			//Referencias Personales/Comerciales/Familiares	
			//Relación anteriores con cargos públicos
			//en la modificacion de CUI
			clientInformationTO = new ClientInformationTO();
		}
		
		}
		catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}	

	}
	
	/**
	 * Issue 1050
	 * Cargando el numero de documento
	 */
	public String getDocumentNumber(String documentNumber, Integer documentType){
		
		if(documentType.equals(DocumentType.CIE.getCode())){
			if(documentNumber.length() > 2){
				return documentNumber.substring(2, documentNumber.length());
			}
		}
		if(documentType.equals(DocumentType.DCD.getCode()) ||
		   documentType.equals(DocumentType.DCC.getCode()) ||
		   documentType.equals(DocumentType.DCR.getCode()) ){
			
			if(documentNumber.length() > 6){
				return documentNumber.substring(3, documentNumber.length()-4);
			}
		}
		return documentNumber;
	}
	
	/**
	 * Load departament legal.
	 */
	public void loadDepartamentLegal() {
		
		if (holderHistory.getLegalResidenceCountry()!=null) {

			if (holderHistory.getLegalResidenceCountry().equals(
					countryResidence)) {
				disabledCountryResidentHolder = true;
				holderHistory.setIndResidence(BooleanType.YES.getCode());
			} else {
				disabledCountryResidentHolder = false;
				holderHistory.setIndResidence(BooleanType.NO.getCode());
			}

			listDepartmentLocation = getLstDepartmentLocation(holderHistory.getLegalResidenceCountry());
		} else {
			holderHistory.setLegalDepartment(null);
			listDepartmentLocation = null;
			holderHistory.setLegalProvince(null);
			listProvinceLocation = null;
			holderHistory.setLegalDistrict(null);
			listMunicipalityLocation = null;
		}
	}
	
	
	
	/**
	 * Gets the before modification.
	 *
	 * @return the before modification
	 */
	public void getBeforeModification(){
		try{
		
		holder = new Holder();
		
		holderRequestDetailBefore = new HolderRequest();
		holderHistoryBefore = new HolderHistory();
		flagSendASFI = false;
		
		legalRepresentativeHelperBean.initializeLstLegalRepresentativeHistoryDetailBefore();
		
		List<LegalRepresentativeHistory>lstLegalRepresentativeHistoryDetailBefore = new ArrayList<LegalRepresentativeHistory>();
		
		lstHolderReqFileHistoryDetailBefore = new ArrayList<HolderReqFileHistory>();
		lstHolderReqChangesMotiveFileHistoryDetailBefore = new ArrayList<HolderReqFileHistory>();
		
		HolderRequest auxHolderRequest = new HolderRequest();
        auxHolderRequest.setIdHolderRequestPk(holderRequestDetail.getIdHolderRequestPk());
       
        holderRequestDetailBefore = holderServiceFacade.getHolderRequestCopyByIdRequestServiceFacade(auxHolderRequest.getIdHolderRequestPk());
		
        if(holderRequestDetailBefore.getHolderReqFileHistories()!=null && holderRequestDetailBefore.getHolderReqFileHistories().size()>0){
        	
            for(int i = 0; i < holderRequestDetailBefore.getHolderReqFileHistories().size(); i++) {
                    if(holderRequestDetailBefore.getHolderReqFileHistories().get(i).getDocumentType() != 2847) {
                            lstHolderReqFileHistoryDetailBefore.add(holderRequestDetailBefore.getHolderReqFileHistories().get(i));
                    } else {
                            lstHolderReqChangesMotiveFileHistoryDetailBefore.add(holderRequestDetailBefore.getHolderReqFileHistories().get(i));
                    }
            }        	
        	//lstHolderReqFileHistoryDetailBefore = holderRequestDetailBefore.getHolderReqFileHistories();
        }
        
        holderHistoryBefore = holderRequestDetailBefore.getHolderHistories().get(0);
		
        
		participant = new Participant();

			if((holderRequestDetailBefore.getParticipant().getIdParticipantPk()!=null)){
				participant.setIdParticipantPk(holderRequestDetailBefore.getParticipant().getIdParticipantPk());				
			}		

				
			holderHistory.setLegalResidenceCountry(holderHistoryBefore.getLegalResidenceCountry());
			listDepartmentLocationBefore = getLstDepartmentLocation(holderHistoryBefore.getLegalResidenceCountry());
			
			holderHistory.setLegalDepartment(holderHistoryBefore.getLegalDepartment());
			listProvinceLocationBefore = getLstProvinceLocation(holderHistoryBefore.getLegalDepartment());

			holderHistory.setLegalProvince(holderHistoryBefore.getLegalProvince());
			
			listMunicipalityLocationBefore = getLstMunicipalityLocation(holderHistoryBefore.getLegalProvince());
			
			if(holderHistoryBefore.getLegalDistrict()!=null){
				holderHistory.setLegalDistrict(holderHistoryBefore.getLegalDistrict());
			}
			
			holderHistory.setPostalResidenceCountry(holderHistoryBefore.getPostalResidenceCountry());
			listDepartmentPostalLocationBefore = getLstDepartmentLocation(holderHistoryBefore.getPostalResidenceCountry());
			
			holderHistory.setPostalDepartment(holderHistoryBefore.getPostalDepartment());			
			listProvincePostalLocationBefore = getLstProvinceLocation(holderHistoryBefore.getPostalDepartment());

			
			holderHistory.setPostalProvince(holderHistoryBefore.getPostalProvince());
			
			listMunicipalityPostalLocationBefore = getLstMunicipalityLocation(holderHistoryBefore.getPostalProvince());
			
			if(holderHistoryBefore.getPostalDistrict()!=null){	
				holderHistory.setPostalDistrict(holderHistoryBefore.getPostalDistrict());
			}

			//jgh
			if(holderHistoryBefore.getTransferNumber()!=null){	
				holderHistory.setTransferNumber(holderHistoryBefore.getTransferNumber());
			}
			if(holderHistoryBefore.getFundAdministrator()!=null){	
				holderHistory.setFundAdministrator(holderHistoryBefore.getFundAdministrator());
			}
			
//			if(holderHistoryBefore.getFundType()!=null){	
//				holderHistory.setFundType(holderHistoryBefore.getFundType());
//			}
			if(holderHistoryBefore.getMnemonic()!=null){
				holderHistory.setMnemonic(holderHistoryBefore.getMnemonic());
			}
			
			if(holderHistoryBefore.getRelatedCui()!=null){	
				holderHistory.setRelatedCui(holderHistoryBefore.getRelatedCui());
			}
			
			if(holderHistoryBefore.getBirthDate()!=null){
				holderHistory.setBirthDate(holderHistoryBefore.getBirthDate());
				
			}
			if(holderHistoryBefore.getDocumentNumber()!=null){
				holderHistory.setDocumentNumber(holderHistoryBefore.getDocumentNumber());
				
			}
			if(holderHistoryBefore.getDocumentType()!=null){
				holderHistory.setDocumentType(holderHistoryBefore.getDocumentType());
				
			}
			if(holderHistoryBefore.getEconomicActivity()!=null){
				holderHistory.setEconomicActivity(holderHistoryBefore.getEconomicActivity());
			}
			if(holderHistoryBefore.getInvestorType()!=null){
				holderHistory.setInvestorType(holderHistoryBefore.getInvestorType());		
			}
			if(holderHistoryBefore.getEconomicSector()!=null){
				holderHistory.setEconomicSector(holderHistoryBefore.getEconomicSector());
			}
			if(holderHistoryBefore.getEmail()!=null){
				holderHistory.setEmail(holderHistoryBefore.getEmail());
			}
			if(holderHistoryBefore.getFaxNumber()!=null){
				holderHistory.setFaxNumber(holderHistoryBefore.getFaxNumber());
			}
			if(holderHistoryBefore.getFirstLastName()!=null){
				holderHistory.setFirstLastName(holderHistoryBefore.getFirstLastName());
			}
			if(holderHistoryBefore.getFullName()!=null){
				holderHistory.setFullName(holderHistoryBefore.getFullName());
			}
			if(holderHistoryBefore.getHolderType()!=null){
				holderHistory.setHolderType(holderHistoryBefore.getHolderType());
			}
			if(holderHistoryBefore.getHomePhoneNumber()!=null){
				holderHistory.setHomePhoneNumber(holderHistoryBefore.getHomePhoneNumber());
			}
			if(holderHistoryBefore.getIndResidence()!=null){
				holderHistory.setIndResidence(holderHistoryBefore.getIndResidence());
			
			}
			if(holderHistoryBefore.getJuridicClass()!=null){
				holderHistory.setJuridicClass(holderHistoryBefore.getJuridicClass());
			}

			if(holderHistoryBefore.getLegalAddress()!=null){
				holderHistory.setLegalAddress(holderHistoryBefore.getLegalAddress());
				
			}

			if(holderHistoryBefore.getLegalDepartment()!=null){
				holderHistory.setLegalDepartment(holderHistoryBefore.getLegalDepartment());		
			}

			if(holderHistoryBefore.getLegalDistrict()!=null){
				holderHistory.setLegalDistrict(holderHistoryBefore.getLegalDistrict());
			}

			if(holderHistoryBefore.getLegalProvince()!=null){
				holderHistory.setLegalProvince(holderHistoryBefore.getLegalProvince());	
			}

			if(holderHistoryBefore.getLegalResidenceCountry()!=null){
				holderHistory.setLegalResidenceCountry(holderHistoryBefore.getLegalResidenceCountry());		
			}

			if(holderHistoryBefore.getMobileNumber()!=null){
				holderHistory.setMobileNumber(holderHistoryBefore.getMobileNumber());
			}

			if(holderHistoryBefore.getName()!=null){
				holderHistory.setName(holderHistoryBefore.getName());
			}

			if(holderHistoryBefore.getNationality()!=null){
				holderHistory.setNationality(holderHistoryBefore.getNationality());
				
			}

			if(holderHistoryBefore.getOfficePhoneNumber()!=null){		
				holderHistory.setOfficePhoneNumber(holderHistoryBefore.getOfficePhoneNumber());
			}

			if(holderHistoryBefore.getPostalAddress()!=null){
				holderHistory.setPostalAddress(holderHistoryBefore.getPostalAddress());
				
			}

			if(holderHistoryBefore.getPostalDepartment()!=null){
				holderHistory.setPostalDepartment(holderHistoryBefore.getPostalDepartment());
			}

			if(holderHistoryBefore.getPostalDistrict()!=null){
				holderHistory.setPostalDistrict(holderHistoryBefore.getPostalDistrict());
			}

			if(holderHistoryBefore.getPostalProvince()!=null){
				holderHistory.setPostalProvince(holderHistoryBefore.getPostalProvince());	
			}

			if(holderHistoryBefore.getPostalResidenceCountry()!=null){
				holderHistory.setPostalResidenceCountry(holderHistoryBefore.getPostalResidenceCountry());	
			}

			if(holderHistoryBefore.getSecondDocumentNumber()!=null){
				holderHistory.setSecondDocumentNumber(holderHistoryBefore.getSecondDocumentNumber());
				
			}
			if(holderHistoryBefore.getSecondDocumentType()!=null){
				holderHistory.setSecondDocumentType(holderHistoryBefore.getSecondDocumentType());
				
			}
			if(holderHistoryBefore.getSecondLastName()!=null){
				holderHistory.setSecondLastName(holderHistoryBefore.getSecondLastName());
			}
			if(holderHistoryBefore.getSecondNationality()!=null){
				holderHistory.setSecondNationality(holderHistoryBefore.getSecondNationality());
				
			}
			if(holderHistoryBefore.getSex()!=null){
				holderHistory.setSex(holderHistoryBefore.getSex());
			}
			if(holderHistoryBefore.getStateHolderHistory()!=null){
				holderHistory.setStateHolderHistory(holderHistoryBefore.getStateHolderHistory());
			}
		
			if(holderHistoryBefore.getIndDisabled()!=null){
				holderHistory.setIndDisabled(holderHistoryBefore.getIndDisabled());
				
			}
			if(holderHistoryBefore.getIndMinor()!=null){
				holderHistory.setIndMinor(holderHistoryBefore.getIndMinor());		
			}
			
			if(holderHistoryBefore.getIndFatca() != null) {
				holderHistory.setIndFatca(holderHistoryBefore.getIndFatca());
			}
			
			if(holderHistoryBefore.getFilePhotoName() != null) {
				holderHistory.setFilePhotoName(holderHistoryBefore.getFilePhotoName());
			}
			
			if(holderHistoryBefore.getFundType()!=null){
				holderHistory.setFundType(holderHistoryBefore.getFundType());
			}
				

			if(holderHistory.getIndDisabled()!=null && (holderHistory.getIndDisabled().equals(BooleanType.YES.getCode()))){
				holderHistory.setIndicatorDisabled(true);
				disabledRepresentativeButton = false;
			}
			else if(holderHistory.getIndDisabled()!=null && (holderHistory.getIndDisabled().equals(BooleanType.NO.getCode()))){
				holderHistory.setIndicatorDisabled(false);	
				disabledRepresentativeButton = true;
			}

			if(holderHistory.getIndMinor()!=null && (holderHistory.getIndMinor().equals(BooleanType.YES.getCode()))){
				holderHistory.setIndicatorMinor(true);
				disabledRepresentativeButton = false;
			}
			else if(holderHistory.getIndMinor()!=null && (holderHistory.getIndMinor().equals(BooleanType.NO.getCode()))){
				holderHistory.setIndicatorMinor(false);
				disabledRepresentativeButton = true;
			}
			
			if(holderHistory.getIndResidence()!=null && (holderHistory.getIndResidence().equals(BooleanType.YES.getCode()))){
				disabledCountryResidentHolder = true;
			}
			else if(holderHistory.getIndResidence()!=null && (holderHistory.getIndResidence().equals(BooleanType.NO.getCode()))){
				disabledCountryResidentHolder = false;
			}
			
			if(holderHistoryBefore.getDocumentSource()!=null){
				holderHistory.setDocumentSource(holderHistoryBefore.getDocumentSource());
			}
			
			if(holderHistoryBefore.getDocumentIssuanceDate()!=null){
				holderHistory.setDocumentIssuanceDate(holderHistoryBefore.getDocumentIssuanceDate());
			}
			
			if(holderHistoryBefore.getFilePhoto() != null){
				setPhotoHolderName("photoHolder");
				JSFUtilities.putSessionMap("sessStreamedPhoto", holderHistoryBefore.getFilePhoto());
				holderHistory.setFilePhoto(holderHistoryBefore.getFilePhoto());
			}
			
			if(holderHistoryBefore.getFileSigning() != null){
				setSigningHolderName("signingHolder");			
				JSFUtilities.putSessionMap("sessStreamedSigning", holderHistoryBefore.getFileSigning());
				holderHistory.setFileSigning(holderHistoryBefore.getFileSigning());
			}
			
			holderHistory.setMarriedLastName(holderHistoryBefore.getMarriedLastName());
			holderHistory.setCivilStatus(holderHistoryBefore.getCivilStatus());
			holderHistory.setMainActivity(holderHistoryBefore.getMainActivity());
			holderHistory.setJobSourceBusinessName(holderHistoryBefore.getJobSourceBusinessName());
			holderHistory.setJobDateAdmission(holderHistoryBefore.getJobDateAdmission());
			holderHistory.setAppointment(holderHistoryBefore.getAppointment());
			holderHistory.setJobAddress(holderHistoryBefore.getJobAddress());
			holderHistory.setTotalIncome(holderHistoryBefore.getTotalIncome());
			holderHistory.setPerRefFullName(holderHistoryBefore.getPerRefFullName());
			holderHistory.setPerRefLandLine(holderHistoryBefore.getPerRefLandLine());
			holderHistory.setPerRefCellPhone(holderHistoryBefore.getPerRefCellPhone());
			holderHistory.setComerRefBusinessName(holderHistoryBefore.getComerRefBusinessName());
			holderHistory.setComerRefLandLine(holderHistoryBefore.getComerRefLandLine());
			holderHistory.setEntityPublicAppointment(holderHistoryBefore.getEntityPublicAppointment());
			holderHistory.setNitNaturalPerson(holderHistoryBefore.getNitNaturalPerson());
			holderHistory.setWebsite(holderHistoryBefore.getWebsite());
			holderHistory.setFundCompanyEnrollment(holderHistoryBefore.getFundCompanyEnrollment());
			holderHistory.setConstitutionDate(holderHistoryBefore.getConstitutionDate());
			holderHistory.setIndCanNegotiate(holderHistoryBefore.getIndCanNegotiate());
			holderHistory.setIndSirtexNeg(holderHistoryBefore.getIndSirtexNeg());
			
			if(holderHistoryBefore.getHolderType().equals(PersonType.NATURAL.getCode())){
				flagHolderJuridicPerson = false;
				flagHolderNaturalPerson = true;			
							
			}else if(holderHistoryBefore.getHolderType().equals(PersonType.JURIDIC.getCode())){			
				flagHolderJuridicPerson = true;
				flagHolderNaturalPerson = false;
				disabledRepresentativeButton = false;
			}

		
			if(holderHistoryBefore.getIndPEP()!=null && holderHistoryBefore.getIndPEP().equals(BooleanType.YES.getCode())){
				holderHistory.setIndicatorPep(true);
				holderHistory.setCategory(holderHistoryBefore.getCategory());
				holderHistory.setRole(holderHistoryBefore.getRole());
				holderHistory.setBeginningPeriod(holderHistoryBefore.getBeginningPeriod());
				holderHistory.setEndingPeriod(holderHistoryBefore.getEndingPeriod());
				holderHistory.setGrade(holderHistoryBefore.getGrade());
				holderHistory.setPepRelatedName(holderHistoryBefore.getPepRelatedName());
			}
			else if(holderHistoryBefore.getIndPEP()!=null && holderHistoryBefore.getIndPEP().equals(BooleanType.NO.getCode())){
				holderHistory.setIndicatorPep(false);
			}
			
			if(holderRequestDetailBefore.getLegalRepresentativeHistories()!=null){
				for(int i=0;i<holderRequestDetailBefore.getLegalRepresentativeHistories().size();i++){				
					
					LegalRepresentativeHistory legalRepresentativeHistoryAux = new LegalRepresentativeHistory();
				
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getIndPEP()!=null && holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getIndPEP().equals(BooleanType.YES.getCode())){
						legalRepresentativeHistoryAux.setCategory(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getCategory());
						legalRepresentativeHistoryAux.setRole(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getRole());
						legalRepresentativeHistoryAux.setBeginningPeriod(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getBeginningPeriod());
						legalRepresentativeHistoryAux.setEndingPeriod(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getEndingPeriod());
					}
					else if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getIndPEP()!=null && holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getIndPEP().equals(BooleanType.NO.getCode())){
						legalRepresentativeHistoryAux.setCategory(null);
						legalRepresentativeHistoryAux.setRole(null);
						legalRepresentativeHistoryAux.setBeginningPeriod(null);
						legalRepresentativeHistoryAux.setEndingPeriod(null);
										
					}
					
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getIdRepresentativeHistoryPk()!=null){
						legalRepresentativeHistoryAux.setIdRepresentativeHistoryPk(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getIdRepresentativeHistoryPk());
					}
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getBirthDate()!=null){
						legalRepresentativeHistoryAux.setBirthDate(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getBirthDate());
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getDocumentNumber()!=null){
						legalRepresentativeHistoryAux.setDocumentNumber(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getDocumentNumber());
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getDocumentType()!=null){
						legalRepresentativeHistoryAux.setDocumentType(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getDocumentType());
					}
					
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getDocumentSource()!=null){
						legalRepresentativeHistoryAux.setDocumentSource(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getDocumentSource());
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getEconomicActivity()!=null){
						legalRepresentativeHistoryAux.setEconomicActivity(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getEconomicActivity());
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getEconomicSector()!=null){
						legalRepresentativeHistoryAux.setEconomicSector(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getEconomicSector());
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getEmail()!=null){
						legalRepresentativeHistoryAux.setEmail(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getEmail());
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getFaxNumber()!=null){
						legalRepresentativeHistoryAux.setFaxNumber(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getFaxNumber());
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getFirstLastName()!=null){
						legalRepresentativeHistoryAux.setFirstLastName(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getFirstLastName());
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getFullName()!=null){
						legalRepresentativeHistoryAux.setFullName(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getFullName());
					}
					
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getHomePhoneNumber()!=null){
						legalRepresentativeHistoryAux.setHomePhoneNumber(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getHomePhoneNumber());
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getIndResidence()!=null){
						
						legalRepresentativeHistoryAux.setIndResidence(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getIndResidence()); 
					}
					
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getLastModifyApp()!=null){
						legalRepresentativeHistoryAux.setLastModifyApp(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getLastModifyApp());
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getLastModifyDate()!=null){
						legalRepresentativeHistoryAux.setLastModifyDate(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getLastModifyDate());
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getLastModifyIp()!=null){
						legalRepresentativeHistoryAux.setLastModifyIp(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getLastModifyIp());
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getLastModifyUser()!=null){
						legalRepresentativeHistoryAux.setLastModifyUser(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getLastModifyUser());
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getLegalAddress()!=null){
						legalRepresentativeHistoryAux.setLegalAddress(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getLegalAddress());
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getLegalDepartment()!=null){
						legalRepresentativeHistoryAux.setLegalDepartment(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getLegalDepartment());
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getLegalDistrict()!=null){
						legalRepresentativeHistoryAux.setLegalDistrict(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getLegalDistrict());
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getLegalProvince()!=null){
						legalRepresentativeHistoryAux.setLegalProvince(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getLegalProvince());
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getLegalResidenceCountry()!=null){
						legalRepresentativeHistoryAux.setLegalResidenceCountry(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getLegalResidenceCountry());
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getMobileNumber()!=null){
						legalRepresentativeHistoryAux.setMobileNumber(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getMobileNumber());
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getName()!=null){
						legalRepresentativeHistoryAux.setName(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getName()); 
					}
					
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getNationality()!=null){
						legalRepresentativeHistoryAux.setNationality(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getNationality());
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getOfficePhoneNumber()!=null){
						legalRepresentativeHistoryAux.setOfficePhoneNumber(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getOfficePhoneNumber());
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getPostalDepartment()!=null){
						legalRepresentativeHistoryAux.setPostalDepartment(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getPostalDepartment());
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getPostalDistrict()!=null){
						legalRepresentativeHistoryAux.setPostalDistrict(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getPostalDistrict());
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getPostalProvince()!=null){
						legalRepresentativeHistoryAux.setPostalProvince(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getPostalProvince());
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getPostalResidenceCountry()!=null){
						legalRepresentativeHistoryAux.setPostalResidenceCountry(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getPostalResidenceCountry());
					}
					
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getPostalAddress()!=null){
						legalRepresentativeHistoryAux.setPostalAddress(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getPostalAddress());
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getRegistryUser()!=null){
						legalRepresentativeHistoryAux.setRegistryUser(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getRegistryUser());
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getSecondDocumentNumber()!=null){
						legalRepresentativeHistoryAux.setSecondDocumentNumber(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getSecondDocumentNumber());
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getSecondLastName()!=null){
						legalRepresentativeHistoryAux.setSecondLastName(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getSecondLastName());
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getSecondNationality()!=null){
						legalRepresentativeHistoryAux.setSecondNationality(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getSecondNationality());	
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getSex()!=null){
						legalRepresentativeHistoryAux.setSex(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getSex());
					}
				    
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getStateRepreHistory()!=null){
						legalRepresentativeHistoryAux.setStateRepreHistory(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getStateRepreHistory());
					}
					
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getPersonType()!=null){
						legalRepresentativeHistoryAux.setPersonType(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getPersonType());
					}
					
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getRepresentativeClass()!=null){
						legalRepresentativeHistoryAux.setRepresentativeClass(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getRepresentativeClass());
					}
					
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getSecondDocumentType()!=null){
						legalRepresentativeHistoryAux.setSecondDocumentType(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getSecondDocumentType());
					}
					
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getDocumentSource()!=null){
						legalRepresentativeHistoryAux.setDocumentSource(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getDocumentSource());
					}
					
					if(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getDocumentIssuanceDate()!=null){
						legalRepresentativeHistoryAux.setDocumentIssuanceDate(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getDocumentIssuanceDate());
					}
					
					legalRepresentativeHistoryAux.setRepresentativeTypeDescription(PersonType.get(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getPersonType()).getValue());
				
					legalRepresentativeHistoryAux.setDocumentTypeDescription(DocumentType.get(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getDocumentType()).getValue());
				
					legalRepresentativeHistoryAux.setRepresenteFileHistory(holderRequestDetailBefore.getLegalRepresentativeHistories().get(i).getRepresenteFileHistory());
					
					
					lstLegalRepresentativeHistoryDetailBefore.add(legalRepresentativeHistoryAux);		
					
					legalRepresentativeHelperBean.setLstLegalRepresentativeHistoryDetailBefore(lstLegalRepresentativeHistoryDetailBefore);
				}
			}

     }
		catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Pep exists.
	 *
	 * @param identificatorHolder the identificator holder
	 */
	public void loadHolderHistoryPepExists(Long identificatorHolder){		
		try{
			
			if(identificatorHolder!=null){
				
				Integer count = holderServiceFacade.validateExistPepPersonByIdHolderServiceFacade(identificatorHolder);
				
				if(count>0){
				
				PepPerson objPepPerson  = holderServiceFacade.getPepPersonByIdHolderServiceFacade(identificatorHolder);
			
				if(objPepPerson!=null){
					holderHistory.setBeginningPeriod(objPepPerson.getBeginingPeriod());
					holderHistory.setEndingPeriod(objPepPerson.getEndingPeriod());
					holderHistory.setCategory(objPepPerson.getCategory());
					holderHistory.setRole(objPepPerson.getRole());
					holderHistory.setIndicatorPep(true);			
					holderHistory.setGrade(objPepPerson.getGrade());
					holderHistory.setPepRelatedName(objPepPerson.getPepRelatedName());
				}
				else{					
					holderHistory.setIndicatorPep(false);
					holderHistory.setBeginningPeriod(null);
					holderHistory.setEndingPeriod(null);
					holderHistory.setCategory(null);
					holderHistory.setRole(null);
					holderHistory.setGrade(null);
					holderHistory.setPepRelatedName(null);
					}
				}
			 }
			 
			
			}
			catch (Exception ex) {
				excepcion.fire(new ExceptionToCatchEvent(ex));
			}
	}

	/**
	 * Validate is copy.
	 *
	 * @return true, if successful
	 */
	public boolean validateIsCopy() {

		boolean flag = true;

		if (!(holderHistory.getLegalResidenceCountry().equals(holderHistory
				.getPostalResidenceCountry()))) {
			flag = false;
		}
		if (!(holderHistory.getLegalProvince().equals(holderHistory
				.getPostalProvince()))) {
			flag = false;
		}
		if (!(holderHistory.getLegalDistrict().equals(holderHistory
				.getLegalDistrict()))) {
			flag = false;
		}
		if (!(holderHistory.getLegalAddress().equals(holderHistory
				.getLegalAddress()))) {
			flag = false;
		}

		return flag;

	}

	
	/**
	 * Onchange indicator minor.
	 */
	public void onchangeIndicatorMinor() {		
		
		try {
			
			 

			if (holderHistory.getBirthDate() != null) {

				Calendar c = Calendar.getInstance();
				DateFormat df = new SimpleDateFormat("yyyy");
				c.setTime(df.parse("1900"));

				if (holderHistory.getBirthDate().getTime() > (new Date()
						.getTime())) {

					JSFUtilities
							.addContextMessage(
									"frmHolderModify:tabView:trCal",
									FacesMessage.SEVERITY_ERROR,
									PropertiesUtilities
											.getMessage(PropertiesConstants.ERROR_ADM_HOLDER_REQUEST_DATE_BIRTHDAY_DATE_TODAY),
									PropertiesUtilities
											.getMessage(PropertiesConstants.ERROR_ADM_HOLDER_REQUEST_DATE_BIRTHDAY_DATE_TODAY));

					showMessageOnDialog(
							PropertiesConstants.LBL_WARNING_ACTION,
							null,
							PropertiesConstants.ERROR_ADM_HOLDER_REQUEST_DATE_BIRTHDAY_DATE_TODAY,
							null);

					JSFUtilities.showSimpleValidationDialog();
					
					return;
				}

				if (Integer.parseInt(df.format(holderHistory.getBirthDate())) < c
						.get(Calendar.YEAR)) {
					JSFUtilities
							.addContextMessage(
									"frmHolderModify:tabView:trCal",
									FacesMessage.SEVERITY_ERROR,
									PropertiesUtilities
											.getMessage(PropertiesConstants.ERROR_ADM_HOLDER_REQUEST_DATE_BIRTHDAY_DATE_ONENINEZEROZEO),
									PropertiesUtilities
											.getMessage(PropertiesConstants.ERROR_ADM_HOLDER_REQUEST_DATE_BIRTHDAY_DATE_ONENINEZEROZEO));

					showMessageOnDialog(
							PropertiesConstants.LBL_WARNING_ACTION,
							null,
							PropertiesConstants.ERROR_ADM_HOLDER_REQUEST_DATE_BIRTHDAY_DATE_ONENINEZEROZEO,
							null);

					JSFUtilities.showSimpleValidationDialog();
					
					return;
				}

				SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
				String birthday = formato.format(holderHistory.getBirthDate());

				int age = calculateAge(birthday);

				if (age >= 18) {
					flagIndicatorMinorHolder = false;
					holderHistory.setIndicatorMinor(false);

					if (holderHistory.getHolderType().equals(
							PersonType.NATURAL.getCode())
							&& !flagHolderIndicatorInability) {
						disabledRepresentativeButton = true;
					} else if (flagHolderIndicatorInability) {
						disabledRepresentativeButton = false;
					}

				} else {
					flagIndicatorMinorHolder = true;
					holderHistory.setIndicatorMinor(true);
					disabledRepresentativeButton = false;

				}

				loadHolderDocumentType();
				loadHolderSecondDocumentType();
				loadingDocumentAttachByHolder();

			}

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
			
	}
	
	/**
	 * Calculate age.
	 *
	 * @param dateBirth the date birth
	 * @return the int
	 */
	public int calculateAge(String dateBirth) {     
		Date dateToday = new Date();
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		String today = formato.format(dateToday);
		String[] dat1 = dateBirth.split("/");
		String[] dat2 = today.split("/");
		int years = Integer.parseInt(dat2[2]) - Integer.parseInt(dat1[2]) - 1;
		int month = Integer.parseInt(dat2[1]) - Integer.parseInt(dat1[1]);

		if (month < 0) {
			years = years + 0;
		} else if (month > 0) {
			years = years + 1;
		}

		if (month == 0) {
			int day = Integer.parseInt(dat2[0]) - Integer.parseInt(dat1[0]);
			if (day >= 0) {
				years = years + 1;
			}
		}
		return years;
	  }

	/**
	 * Onchange indicator disabled.
	 */
	public void onchangeIndicatorDisabled() {
		if (holderHistory.isIndicatorDisabled()){
			flagHolderIndicatorInability = false;
			disabledRepresentativeButton = true;

		} else {
			flagHolderIndicatorInability = false;
			if(!holderHistory.isIndicatorMinor()){
				disabledRepresentativeButton = true;
			}
			else{
				disabledRepresentativeButton = false;
			}

		}
		loadHolderDocumentType();
		loadHolderSecondDocumentType();
		if(lstHolderReqFileHistory == null || lstHolderReqFileHistory.isEmpty() || lstHolderReqFileHistory.size() == 0 ){
			loadingDocumentAttachByHolder();
		}
	}

	/**
	 * Onchange is pep.
	 */
	public void onchangeIsPEP() {
		JSFUtilities.resetViewRoot();
		if (holderHistory.isIndicatorPep()) {

			holderHistory.setIndPEP(BooleanType.YES.getCode());
			holderHistory.setBeginningPeriod(CommonsUtilities.currentDate());
			holderHistory.setEndingPeriod(CommonsUtilities.currentDate());
		} else {

			holderHistory.setIndPEP(BooleanType.NO.getCode());
			holderHistory.setCategory(null);
			holderHistory.setRole(null);
			holderHistory.setBeginningPeriod(null);
			holderHistory.setEndingPeriod(null);
			holderHistory.setGrade(null);
			holderHistory.setPepRelatedName(null);
		}
	}

	/**
	 * Gets the lst document type legal.
	 *
	 * @return the lst document type legal
	 */
	public void getLstDocumentTypeLegal() {

		try {
	
			ParameterTable auxParameterTable;
			listDocumentTypeLegalResult = new ArrayList<ParameterTable>();
			
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON
							.getCode());

			listDocumentTypeLegal = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			
			if(listDocumentTypeLegal!=null && listDocumentTypeLegal.size()>0){
				for(int i=0;i<listDocumentTypeLegal.size();i++){
					
					auxParameterTable = new ParameterTable();
					if((listDocumentTypeLegal.get(i).getParameterTablePk().equals(DocumentType.CI.getCode())) || (listDocumentTypeLegal.get(i).getParameterTablePk().equals(DocumentType.PAS.getCode()))){
						auxParameterTable.setParameterTablePk(listDocumentTypeLegal.get(i).getParameterTablePk());
						auxParameterTable.setParameterName(listDocumentTypeLegal.get(i).getParameterName());						
						listDocumentTypeLegalResult.add(auxParameterTable);
					}
					
				}
			}
			 
			

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}

	/**
	 * Gets the lst geographic location.
	 *
	 * @return the lst geographic location
	 */
	public void getLstGeographicLocation() {

		try {

			GeographicLocationTO geographicLocationTO = new GeographicLocationTO();

			geographicLocationTO
					.setGeographicLocationType(GeographicLocationType.COUNTRY
							.getCode());
			geographicLocationTO
					.setState(GeographicLocationStateType.REGISTERED.getCode());

			listGeographicLocation = generalParametersFacade
					.getListGeographicLocationServiceFacade(geographicLocationTO);
			
			

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Gets the lst geographic location without national country.
	 *
	 * @return the lst geographic location without national country
	 */
	public void getLstGeographicLocationWithoutNationalCountry(){

		try {

			listGeographicLocationWithoutNationalCountry = new ArrayList<GeographicLocation>();
			
			GeographicLocationTO geographicLocationTO = new GeographicLocationTO();

			geographicLocationTO
					.setGeographicLocationType(GeographicLocationType.COUNTRY
							.getCode());
			geographicLocationTO
					.setState(GeographicLocationStateType.REGISTERED.getCode());

			listGeographicLocation = generalParametersFacade
					.getListGeographicLocationServiceFacade(geographicLocationTO);
			
			for(GeographicLocation geo : listGeographicLocation){
				if(!geo.getIdGeographicLocationPk().equals(countryResidence)){
					listGeographicLocationWithoutNationalCountry.add(geo);
				}
			}
			

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	

	/**
	 * Gets the lst province location.
	 *
	 * @param referenceFk the reference fk
	 * @return the lst province location
	 */
	public List<GeographicLocation> getLstProvinceLocation(Integer referenceFk) {

		List<GeographicLocation> lst = null;

		try {

			GeographicLocationTO geographicLocationTO = new GeographicLocationTO();

			geographicLocationTO
					.setGeographicLocationType(GeographicLocationType.PROVINCE
							.getCode());
			geographicLocationTO.setIdLocationReferenceFk(referenceFk);

			lst = generalParametersFacade
					.getListGeographicLocationServiceFacade(geographicLocationTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

		return lst;

	}
	
	/**
	 * Load departament postal.
	 */
	public void loadDepartamentPostal() {
		if (holderHistory.getPostalResidenceCountry()!=null) {
			listDepartmentPostalLocation = getLstDepartmentLocation(holderHistory.getPostalResidenceCountry());
		} else {
			holderHistory.setPostalDepartment(null);
			listDepartmentPostalLocation = null;
			holderHistory.setPostalProvince(null);
			listProvincePostalLocation = null;
			holderHistory.setPostalDistrict(null);
			listMunicipalityPostalLocation = null;
		}
	}

	
	/**
	 * Load municipality postal.
	 */
	public void loadMunicipalityPostal() {
		if (holderHistory.getPostalProvince()!=null) {
			listMunicipalityPostalLocation = getLstMunicipalityLocation(holderHistory.getPostalProvince());
		} else {
			holderHistory.setPostalDistrict(null);
			listMunicipalityPostalLocation = null;
		}
	}
	
	/**
	 * Gets the lst participant.
	 *
	 * @return the lst participant
	 */
	public void getLstParticipant() {

		try {

			Participant filter = new Participant();

			filter.setState(ParticipantStateType.REGISTERED.getCode());
			listParticipant = accountsFacade
					.getLisParticipantServiceBean(filter);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}

	/**
	 * Gets the lst juridic class.
	 *
	 * @return the lst juridic class
	 */
	public void getLstJuridicClass() {

		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(MasterTableType.T_CLASE_JURIDICO_TITULAR
							.getCode());

			listJuridicClass = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}

	/**
	 * Gets the lst person type.
	 *
	 * @return the lst person type
	 */
	public void getLstPersonType() {

		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.PERSON_TYPE
					.getCode());

			listPersonType = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}

	/**
	 * Gets the lst boolean type.
	 *
	 * @return the lst boolean type
	 */
	public void getLstBooleanType() {
		listBoolean = BooleanType.list;
	}
	
	/**
	 * Gets the lst category pep.
	 *
	 * @return the lst category pep
	 */
	public void getLstCategoryPep(){
		
		try {
		
		ParameterTableTO parameterTableTO = new ParameterTableTO();

		parameterTableTO.setState(ParameterTableStateType.REGISTERED
				.getCode());
		parameterTableTO
				.setMasterTableFk(MasterTableType.ASSET_LAUNDRY_MOTIVE_PEP
						.getCode());

		listCategoryPep = generalParametersFacade
				.getListParameterTableServiceBean(parameterTableTO);

		}
		
		catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

		
	}
	
	/**
	 * Gets the lst role pep.
	 *
	 * @return the lst role pep
	 */
	public void getLstRolePep(){
		
		try{
		ParameterTableTO parameterTableTO = new ParameterTableTO();

		parameterTableTO.setState(ParameterTableStateType.REGISTERED
				.getCode());
		parameterTableTO
				.setMasterTableFk(MasterTableType.PEP_CHARGE
						.getCode());

		listRolePep = generalParametersFacade
				.getListParameterTableServiceBean(parameterTableTO);

		}
		
		catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

		
	}
	
	public void getGradePep() {
		
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.PEP_GRADE
					.getCode());
			listPepGrade = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	public void getListEconomicActivityWithoutSector() {
		try {
			/*Integer economicSector=null;
			if(isViewOperationRegister()|| isViewOperationModify()){
				economicSector = holderHistory.getEconomicSector();
			}
			else{
				economicSector = holderHistoryDetail.getEconomicSector();
			}*/
			listEconomicActivityNaturalPerson = generalParametersFacade.getListEconomicActivityBySector(null);
			
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Gets the lst motives type.
	 *
	 * @return the lst motives type
	 */
	public void getLstRejectMotivesType(){
		
		try{
			
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_REQUEST_REJECT_MOTIVE.getCode());
			
		
	    listMotives = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		
	    } catch (Exception ex) {
		excepcion.fire(new ExceptionToCatchEvent(ex));
	    }
	}
	
	/**
	 * Gets the lst annular motives type.
	 *
	 * @return the lst annular motives type
	 */
	public void getLstAnnularMotivesType(){
		
		try{
		
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_REQUEST_ANNULAR_MOTIVE.getCode());
			
		    listMotives = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			
		}
		catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		
	}

	/**
	 * Gets the lst document type.
	 *
	 * @return the lst document type
	 */
	public void getLstDocumentType() {

		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON
							.getCode());

			listDocumentType = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
			
			

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}
	
	/**
	 * Gets the lst document emission source.
	 *
	 * @return the lst document emission source
	 */
	public void getLstDocumentSource() {

		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(MasterTableType.DOCUMENT_SOURCE.getCode());
			
			
			listDocumentSource = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}
	
	/**
	 * Gets the lst state holder request type.
	 *
	 * @return the lst state holder request type
	 */
	public void getLstStateHolderRequestType(){
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.STATE_CREATION_HOLDER_REQUEST.getCode());

			listStateHolderRequest = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Gets the lst sex type.
	 *
	 * @return the lst sex type
	 */
	public void getLstSexType() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.SEX.getCode());

			listSex = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Gets the lst representative class type.
	 *
	 * @return the lst representative class type
	 */
	public void getLstRepresentativeClassType() {

		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(MasterTableType.CLASS_REPRENTATIVE
							.getCode());

			listRepresentativeClass = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}
	
	/**
	 * Validate operation in subsidiary.
	 *
	 * @param objHolderRequest the obj holder request
	 * @return the string
	 */
	private List<String> validateOperationInSubsidiary(HolderRequest objHolderRequest){
		List<OperationUserTO> lstOperationUserParam = new ArrayList<OperationUserTO>();
		List<String> lstOperationUserResult;
		OperationUserTO operationUserTO = new OperationUserTO();
		operationUserTO.setOperNumber(objHolderRequest.getIdHolderRequestPk().toString());
		operationUserTO.setUserName(objHolderRequest.getRegistryUser());
		lstOperationUserParam.add(operationUserTO);
		lstOperationUserResult = getIsSubsidiary(userInfo, lstOperationUserParam);
		return lstOperationUserResult;
	}
	
	/**
	 * Validate request confirm state.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String validateRequestConfirmState(){
		
		setViewOperationType(ViewOperationsType.CONFIRM.getCode());

		String action="";
		boolean flag = false;
		
		if (holderRequestSelected == null) {
			Object[] bodyData = new Object[0];
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED,bodyData));
			JSFUtilities.showSimpleValidationDialog();
			flag = true;
		}
		else if(holderRequestSelected != null){
			
			// Validate in subsidiary			
			List<String> lstOperationUser = validateOperationInSubsidiary(holderRequestSelected);
			if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_INVALID_USER_CONFIRM,
								new Object[] { lstOperationUser.get(0).toString() }));
				JSFUtilities.showSimpleValidationDialog();
				flag = true;
				return action;
			}
			
			if (!holderRequestSelected.getStateHolderRequest().equals(HolderRequestStateType.APPROVED.getCode())) {
				Object[] bodyData = new Object[0];
				flag = true;
				
				JSFUtilities.showMessageOnDialog(
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),						
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_OPERATION_CONFIRM_INVALID,bodyData));
			}		
						
			if(flag){
				JSFUtilities.showSimpleValidationDialog();
			}else{
				loadingHolderInformation(null);
				action="holderInformation";	
			}			
		}
		
		return action;
	}
	
	/**
	 * Validate request reject state.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String validateRequestRejectState(){
		setViewOperationType(ViewOperationsType.REJECT.getCode());

		String action="";
		boolean flag = false;
		
		if (holderRequestSelected == null) {	
			Object[] bodyData = new Object[0];
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED,bodyData));
			JSFUtilities.showSimpleValidationDialog();			
			flag = true;
		}
		else if(holderRequestSelected != null){
			
			// Validate in subsidiary			
			List<String> lstOperationUser = validateOperationInSubsidiary(holderRequestSelected);
			if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_INVALID_USER_REJECT,
								new Object[] { lstOperationUser.get(0).toString() }));
				JSFUtilities.showSimpleValidationDialog();
				flag = true;
				return action;
			}
			
			if (!holderRequestSelected.getStateHolderRequest().equals(HolderRequestStateType.APPROVED.getCode())) {
				
				flag = true;
				Object[] bodyData = new Object[0];
				
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_OPERATION_REJECT_INVALID,bodyData));
						
			}			
			
			if(flag){
				JSFUtilities.showSimpleValidationDialog();
			}else{
				loadingHolderInformation(null);
				action="holderInformation";	
			}
			
			
		}
		
		return action;

	}
	
	/**
	 * Validate request approve state.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String validateRequestApproveState(){
		
		setViewOperationType(ViewOperationsType.APPROVE.getCode());
		
		String action="";
		boolean flag = false;
		
		if (holderRequestSelected == null) {
			Object[] bodyData = new Object[0];
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED,bodyData));
			JSFUtilities.showSimpleValidationDialog();
						
			flag = true;			
		}
		else if(holderRequestSelected != null){
			
			// Validate in subsidiary			
			List<String> lstOperationUser = validateOperationInSubsidiary(holderRequestSelected);
			if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_INVALID_USER_APPROVE,
								new Object[] { lstOperationUser.get(0).toString() }));
				JSFUtilities.showSimpleValidationDialog();
				flag = true;
				return action;
			}
			
			if (!holderRequestSelected.getStateHolderRequest().equals(HolderRequestStateType.REGISTERED.getCode())
					&& !holderRequestSelected.getStateHolderRequest().equals(HolderRequestStateType.MODIFIED.getCode())) {
				Object[] bodyData = new Object[0];
				flag = true;
				
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_OPERATION_APPROVE_INVALID,bodyData));
						
			}		
			
			if(flag){
				JSFUtilities.showSimpleValidationDialog();
			}else{
				loadingHolderInformation(null);
				action="holderInformation";	
			}
			
			
		}
		
		return action;

		
	}
	
	/**
	 * Validate request annular state.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String validateRequestAnnularState(){
		
		setViewOperationType(ViewOperationsType.ANULATE.getCode());

		String action="";
		boolean flag = false;
		
		if (holderRequestSelected == null) {	
			Object[] bodyData = new Object[0];
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED,bodyData));
			JSFUtilities.showSimpleValidationDialog();
						
		    flag = true;
		}
		else if(holderRequestSelected != null){
			
			// Validate in subsidiary			
			List<String> lstOperationUser = validateOperationInSubsidiary(holderRequestSelected);
			if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_INVALID_USER_ANNUL,
								new Object[] { lstOperationUser.get(0).toString() }));
				JSFUtilities.showSimpleValidationDialog();
				flag = true;
				return action;
			}
			
			if (!holderRequestSelected.getStateHolderRequest().equals(HolderRequestStateType.REGISTERED.getCode())
					&& !holderRequestSelected.getStateHolderRequest().equals(HolderRequestStateType.MODIFIED.getCode())) {
				Object[] bodyData = new Object[0];		
				flag = true;
				
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_OPERATION_ANNULAR_INVALID,bodyData));
			}		
			
			if(flag){
				JSFUtilities.showSimpleValidationDialog();
			}else{
				loadingHolderInformation(null);
				action="holderInformation";	
			}
			
			
		}
		
		return action;

		
	}
	
	/**
	 * Validate search filter.
	 *
	 * @return true, if successful
	 */
	public boolean validateSearchFilter(){
		
		boolean flag=true;
		
		if(holderFilter.getInitialDate().getTime()>holderFilter.getEndDate().getTime()){				
			
	        JSFUtilities.addContextMessage("frmHolderModifySearch:calInitialDate",FacesMessage.SEVERITY_ERROR,
			PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_HOLDER_MODIFICATION_FILTER_DATE_INITIAL),
			PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_HOLDER_MODIFICATION_FILTER_DATE_INITIAL));
	        
	        holderRequestDataModel = null;
	        flag=false;
		}
       if(holderFilter.getEndDate().getTime()>(new Date().getTime())){
    	   JSFUtilities.addContextMessage("frmHolderModifySearch:calEndDate",FacesMessage.SEVERITY_ERROR,
   				PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_HOLDER_MODIFICATION_FILTER_DATE_FINAL_DATE_TODAY),
   				PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_HOLDER_MODIFICATION_FILTER_DATE_FINAL_DATE_TODAY));
   		        
   		        holderRequestDataModel = null;
   		     flag=false;
       }
       if(holderFilter.getInitialDate().getTime()>(new Date().getTime())){
    	   JSFUtilities.addContextMessage("frmHolderModifySearch:calInitialDate",FacesMessage.SEVERITY_ERROR,
   				PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_HOLDER_MODIFICATION_FILTER_DATE_INITIAL_DATE_TODAY),
   				PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_HOLDER_MODIFICATION_FILTER_DATE_INITIAL_DATE_TODAY));
   		        
   		        holderRequestDataModel = null;
   		     flag=false;
       }
       
       return flag;
		
	}

	/**
	 * Search request holders.
	 */
	@LoggerAuditWeb
	public void searchRequestHolders() {
		
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		
		//SI NO TIENE PARTICIPANTE Y ES EMISOR DPF NO PUEDE REGISTRAR
		if(userInfo.getUserAccountSession().isIssuerDpfInstitucion() && !flagIsIssuerDpfInstitution){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_PARTICIPANT));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
	
		try {
			
			if(validateSearchFilter()){

				holderFilter.setRequestType(HolderRequestType.MODIFICATION.getCode());
				holderFilter.setRequestTypeAux(null);

				List<HolderRequest> lstResult = new ArrayList<HolderRequest>();
				
				
				lstResult = holderServiceFacade.getListModificationHolderRequestServiceFacade(holderFilter);
				
				if(lstResult!=null && lstResult.size()>0){
					for(int i=0;i<lstResult.size();i++){
						    if(lstResult.get(i).getStateHolderRequest()!=null){
						    	lstResult.get(i).setStateDescription(requestStateDescription.get(lstResult.get(i).getStateHolderRequest()).toString());
						    }
						    else{
						    	lstResult.get(i).setStateDescription(GeneralConstants.EMPTY_STRING);
						    }
						    
						    if(lstResult.get(i).getHolderHistory()!=null && lstResult.get(i).getHolderHistory().getDocumentType()!=null){
						    	lstResult.get(i).getHolderHistory().setTypeDocumentDescription(documentTypeDescription.get(lstResult.get(i).getHolderHistory().getDocumentType()).toString());
						    }
						    else{
						    	lstResult.get(i).getHolderHistory().setTypeDocumentDescription(GeneralConstants.EMPTY_STRING);
						    }
					}			
				}
				
				holderRequestDataModel = new HolderRequestDataModel(lstResult);
			
				if(holderRequestDataModel.getRowCount()>0){
					setTrueValidButtons();
				}
			}

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}
	
	/**
	 * Sets the true valid buttons.
	 */
	public void setTrueValidButtons(){
		
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		
		
		if(userPrivilege.getUserAcctions().isConfirm()){
			privilegeComponent.setBtnConfirmView(true);
		}
		if(userPrivilege.getUserAcctions().isReject()){
			privilegeComponent.setBtnRejectView(true);
		}
		if(userPrivilege.getUserAcctions().isAnnular()){
			privilegeComponent.setBtnAnnularView(true);
		}
		if(userPrivilege.getUserAcctions().isApprove()){
			privilegeComponent.setBtnApproveView(true);
		}
		
		userPrivilege.setPrivilegeComponent(privilegeComponent);
		
	}

	public void validateResidentHolder(String idComponent) {
		validateResidentHolderAndNationality(idComponent, Boolean.FALSE);
	}
	/**
	 * Validate resident holder.
	 *
	 * @param idComponent the id component
	 */
	public void validateResidentHolderAndNationality(String idComponent, Boolean nationalityChange) {

		disabledIndResident = false;
		disabledCountryResidentHolder=false;
		
		flagCopyLegalHolder=false;		
		disabledCmbPostalCountry = false;
		disabledCmbPostalDepartament =false;
		disabledCmbPostalProvince = false;
		disabledCmbPostalMunicipality = false;
		disabledInputPostalAddress = false;
		documentTO = new DocumentTO();		
		
		holderHistory.setDocumentNumber(GeneralConstants.EMPTY_STRING);
		newDocumentNumber = GeneralConstants.EMPTY_STRING;
		
		if (holderHistory.getHolderType().equals(PersonType.NATURAL.getCode())) {
			validateNationalForeign(idComponent);
			loadHolderDocumentType();
			loadingDocumentAttachByHolder();
		} else if (holderHistory.getHolderType().equals(
				PersonType.JURIDIC.getCode())) {
			
			if(holderHistory.getNationality()!=null && holderHistory.getNationality().equals(countryResidence)){
				holderHistory.setIndResidence(BooleanType.YES.getCode());
				holderHistory.setLegalResidenceCountry(countryResidence);
				listDepartmentLocation = getLstDepartmentLocation(holderHistory.getLegalResidenceCountry());
				
				disabledIndResident = true;
				disabledCountryResidentHolder=true;
			}
			else if(holderHistory.getNationality()!=null && !holderHistory.getNationality().equals(countryResidence)){
			
				if(holderHistory.getJuridicClass()!=null && holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())){
					disabledIndResident = true;
					disabledCountryResidentHolder=true;
				}else{
				holderHistory.setIndResidence(BooleanType.NO.getCode());
				listDepartmentLocation = getLstDepartmentLocation(holderHistory.getLegalResidenceCountry());
				}
				
			}
				
			validateNationalForeign(idComponent);
			loadHolderDocumentType();
			loadingDocumentAttachByHolder();

		}
		
		if (holderHistory.getIndResidence()!=null && BooleanType.YES.getCode().equals(holderHistory.getIndResidence())) {
			holderHistory.setLegalResidenceCountry(countryResidence);			
			disabledCountryResidentHolder=true;
			
			if(holderHistory.getNationality()!=null && holderHistory.getNationality().equals(countryResidence)){
				holderHistory.setDocumentType(DocumentType.CI.getCode());
				documentTO = documentValidator.loadMaxLenghtType(holderHistory.getDocumentType());			
				numericTypeDocumentHolder = documentTO.isNumericTypeDocument();
				maxLenghtDocumentNumberHolder = documentTO.getMaxLenghtDocumentNumber();
				documentTO.setIndicatorDocumentSource(true);
				getLstDocumentSource();
				holderHistory.setDocumentSource(departmentIssuance);
			}
			else{	
				holderHistory.setDocumentType(null);
				holderHistory.setDocumentSource(null);
			}
			
			if(isViewOperationRegister()){
				SelectOneMenu cmbContryResident = (SelectOneMenu)FacesContext.getCurrentInstance().getViewRoot().findComponent("frmHolderModify:tabView:cmbCountryResident");
				cmbContryResident.setValid(true);
				
				if(holderHistory.getHolderType().equals(PersonType.JURIDIC.getCode())){
					SelectOneMenu cmbContryResident2 = (SelectOneMenu)FacesContext.getCurrentInstance().getViewRoot().findComponent("frmHolderModify:tabView:cmbCountryResident2");
					cmbContryResident2.setValid(true);
				}
				
				SelectOneMenu cmbLegalCountry = (SelectOneMenu)FacesContext.getCurrentInstance().getViewRoot().findComponent("frmHolderModify:tabView:cmbLegalCountry");
				cmbLegalCountry.setValid(true);
			}
			
			if(!nationalityChange) {
				listDepartmentLocation = getLstDepartmentLocation(holderHistory.getLegalResidenceCountry());
				
				holderHistory.setLegalDepartment(departmentDefault);			
				listProvinceLocation = getLstProvinceLocation(holderHistory.getLegalDepartment());
				//holderHistory.setLegalProvince(provinceDefault);
				//listMunicipalityLocation = getLstMunicipalityLocation(holderHistory.getLegalProvince());
				listMunicipalityLocation=null;
				holderHistory.setLegalProvince(null);
				holderHistory.setLegalDistrict(null);
				holderHistory.setLegalAddress(null);
				
				
				
				holderHistory.setPostalResidenceCountry(null);	
				holderHistory.setPostalDepartment(null);
				holderHistory.setPostalProvince(null);
				holderHistory.setPostalDistrict(null);
				holderHistory.setPostalAddress(null);
				
				listMunicipalityPostalLocation=null;	
			}		

		} else if (holderHistory.getIndResidence()!=null && BooleanType.NO.getCode().equals(
				holderHistory.getIndResidence())) {
			
			disabledCountryResidentHolder=false;
			
			if(nationalityChange) {
				listDepartmentLocation = getLstDepartmentLocation(holderHistory.getLegalResidenceCountry());
				
				
				holderHistory.setLegalDepartment(null);
				holderHistory.setLegalProvince(null);
				holderHistory.setLegalDistrict(null);
				holderHistory.setLegalAddress(null);
				
				listProvinceLocation=null;
				listMunicipalityLocation=null;
				
				holderHistory.setPostalResidenceCountry(null);		
				holderHistory.setPostalDepartment(null);
				holderHistory.setPostalProvince(null);
				holderHistory.setPostalDistrict(null);
				holderHistory.setPostalAddress(null);
				listProvincePostalLocation=null;
				listMunicipalityPostalLocation=null;
			}
		} 
	}

	/**
	 * Copy legal holder.
	 */
	public void copyLegalHolder() {
		if (flagCopyLegalHolder) {
			
			listDepartmentPostalLocation = listDepartmentLocation;
			listProvincePostalLocation = listProvinceLocation;
			listMunicipalityPostalLocation = listMunicipalityLocation;
			
			holderHistory.setPostalResidenceCountry(holderHistory.getLegalResidenceCountry());
			holderHistory.setPostalDepartment(holderHistory.getLegalDepartment());
			holderHistory.setPostalProvince(holderHistory.getLegalProvince());
			holderHistory.setPostalDistrict(holderHistory.getLegalDistrict());
			holderHistory.setPostalAddress(holderHistory.getPostalAddress());

			disabledCmbPostalCountry = true;
			disabledCmbPostalDepartament = true;
			disabledCmbPostalProvince = true;
			disabledCmbPostalMunicipality = true;
			disabledInputPostalAddress = false;
		
			
		if(isViewOperationRegister()){
			SelectOneMenu cmbPostalCountry = (SelectOneMenu) FacesContext.getCurrentInstance().getViewRoot().findComponent("frmHolderModify:tabView:cmbPostalCountry");
			SelectOneMenu cmbPostalDepartament = (SelectOneMenu) FacesContext.getCurrentInstance().getViewRoot().findComponent("frmHolderModify:tabView:cmbPostalDepartament");
			SelectOneMenu cmbPostalProvince = (SelectOneMenu) FacesContext.getCurrentInstance().getViewRoot().findComponent("frmHolderModify:tabView:cmbPostalProvince");
			SelectOneMenu cmbPostalSector = (SelectOneMenu) FacesContext.getCurrentInstance().getViewRoot().findComponent("frmHolderModify:tabView:cmbPostalMunicipality");
			InputText inputPostalAddress = (InputText)	FacesContext.getCurrentInstance().getViewRoot().findComponent("frmHolderModify:tabView:inputPostalAddress");
				
			cmbPostalCountry.setValid(true);
			cmbPostalDepartament.setValid(true);
			cmbPostalProvince.setValid(true);
			cmbPostalSector.setValid(true);
			inputPostalAddress.setValid(true);		
		}
			
		} else {
			disabledCmbPostalCountry = false;
			disabledCmbPostalDepartament = false;
			disabledCmbPostalProvince = false;
			disabledCmbPostalMunicipality = false;
			disabledInputPostalAddress = false;
		}
	}

	/**
	 * Sets the holder filter.
	 *
	 * @param holderFilter the new holder filter
	 */
	public void setHolderFilter(HolderRequestTO holderFilter) {
		this.holderFilter = holderFilter;
	}

	/**
	 * Gets the holder filter.
	 *
	 * @return the holder filter
	 */
	public HolderRequestTO getHolderFilter() {
		return holderFilter;
	}

	/**
	 * Checks if is show panel options component.
	 *
	 * @return true, if is show panel options component
	 */
	public boolean isShowPanelOptionsComponent() {
		return showPanelOptionsComponent;
	}

	/**
	 * Sets the show panel options component.
	 *
	 * @param showPanelOptionsComponent the new show panel options component
	 */
	public void setShowPanelOptionsComponent(boolean showPanelOptionsComponent) {
		this.showPanelOptionsComponent = showPanelOptionsComponent;
	}

	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * Before on confirm.
	 *
	 * @param event the event
	 */
	public void beforeOnConfirm(ActionEvent event) {
		
		flagActionsInFalse();

		flagActionConfirm = true;
		
		if(indChangesNeedsConfirm) {
			if(this.userInfo.getUserAccountSession().isParticipantInstitucion()) {
				Object[] bodyData = new Object[1];

				bodyData[0] = holderRequestSelected.getHolder().getIdHolderPk().toString();

				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_APPROVE),
						PropertiesUtilities.getMessage(PropertiesConstants.HOLDER_CHANGES_MSG_NEEDS_CAVAPY_CONFIRM));
				JSFUtilities.showSimpleValidationDialog();
				return;
				
			}
		}
		
		Object[] bodyData = new Object[1];
		bodyData[0] = holderRequestSelected.getHolder().getIdHolderPk().toString();

		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
				PropertiesUtilities.getMessage(PropertiesConstants.LBL_CONFIRM_HOLDER_REQUEST_MODIFY,bodyData));
		JSFUtilities.executeJavascriptFunction("PF('widgConfirmActions').show()");

		
	}

	/**
	 * Before on reject.
	 *
	 * @param event the event
	 */
	public void beforeOnReject(ActionEvent event) {
		
		JSFUtilities.resetViewRoot();
		getLstRejectMotivesType();
		flagActionsInFalse();

		flagActionReject = true;
		
		requestMotive=null;
		requestOtherMotive="";
		showMotiveText=false;

		 

			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MOTIVE_REJECT),null);
			JSFUtilities.executeJavascriptFunction("PF('dlgMotive').show()");
		
	}
	
	/**
	 * Before on approve.
	 *
	 * @param event the event
	 */
	public void beforeOnApprove(ActionEvent event){
		
		flagActionsInFalse();

		flagActionApprove = true;
		
		if(indChangesNeedsConfirm) {
			
			if(this.userInfo.getUserAccountSession().isParticipantInstitucion() &&
					this.userInfo.getUserAccountSession().getIsOperator() != null &&
					this.userInfo.getUserAccountSession().getIsOperator() == 1) {
				Object[] bodyData = new Object[1];
	
				bodyData[0] = holderRequestSelected.getHolder().getIdHolderPk().toString();
	
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_APPROVE),
						PropertiesUtilities.getMessage(PropertiesConstants.HOLDER_CHANGES_MSG_NEEDS_SUPERVISOR_CONFIRM));
				JSFUtilities.showSimpleValidationDialog();
				return;
				
			}
		}
		
		Object[] bodyData = new Object[1];

		bodyData[0] = holderRequestSelected.getHolder().getIdHolderPk().toString();

		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_APPROVE),
				PropertiesUtilities.getMessage(PropertiesConstants.LBL_APPROVE_REQUEST_MODIFY,bodyData));
		JSFUtilities.executeJavascriptFunction("PF('widgConfirmActions').show()");

		
		
	}

	/**
	 * Before on annular.
	 *
	 * @param event the event
	 */
	public void beforeOnAnnular(ActionEvent event) {
		
		JSFUtilities.resetViewRoot();
		getLstAnnularMotivesType();
		flagActionsInFalse();

		flagActionAnnular = true;
		
		requestMotive=null;	
		requestOtherMotive="";
		showMotiveText=false;

		 
		
		   JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MOTIVE_ANNUL),null);
		   JSFUtilities.executeJavascriptFunction("PF('dlgMotive').show()");
	}
	
	/**
	 * Confirm actions.
	 */
	public void confirmActions(){
		
		if (flagActionReject) {
			
			Object[] bodyData = new Object[1];

			bodyData[0] = holderRequestSelected.getHolder().getIdHolderPk().toString();

			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REJECT),
					PropertiesUtilities.getMessage(PropertiesConstants.LBL_REJECT_HOLDER_REQUEST_MODIFY,bodyData));
			JSFUtilities.executeJavascriptFunction("PF('widgConfirmActions').show()");
			
			
			
		} else if (flagActionAnnular) {
			Object[] bodyData = new Object[1];
			bodyData[0] = holderRequestSelected.getHolder().getIdHolderPk().toString();
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ANNUL),
					PropertiesUtilities.getMessage(PropertiesConstants.LBL_ANNULAR_HOLDER_REQUEST_MODIFY,bodyData));
			JSFUtilities.executeJavascriptFunction("PF('widgConfirmActions').show()");
			

		}	
	}

	/**
	 * Load by holder type person.
	 */
	public void loadByHolderTypePerson() {
		try{
		
		loadHolderDocumentType();
		//disabledDocNumberBankCorrelative = false;
		documentTO = new DocumentTO();
		
		Participant filter = new Participant();
		filter.setIdParticipantPk(participant.getIdParticipantPk());
		participant = accountsFacade.findParticipantByFilters(filter);
		
		if(participant!=null && participant.getMnemonic()!=null &&
				holderHistory.getHolderType()!=null){
			clientInformationTO=clientInformationValidator.validateShowClientInformation(participant.getAccountClass(),holderHistory.getHolderType());
		}

		if (holderHistory.getHolderType().equals(PersonType.NATURAL.getCode())) {
			flagHolderNaturalPerson = true;
			flagHolderJuridicPerson = false;
			
					
			holderHistory.setHolderType(PersonType.NATURAL.getCode());
			holderHistory.setBeginningPeriod(CommonsUtilities.currentDate());
			holderHistory.setEndingPeriod(CommonsUtilities.currentDate());
			disabledCountryResidentHolder = false;
			
			if(holderHistory.isIndicatorDisabled()){
				disabledRepresentativeButton = false;
			}
			else{
				disabledRepresentativeButton = true;
			}
			
			
		} else if (holderHistory.getHolderType().equals(
				PersonType.JURIDIC.getCode())) {
			flagHolderJuridicPerson = true;
			flagHolderNaturalPerson = false;
			disabledCountryResidentHolder = false;
			
			holderHistory.setJuridicClass(JuridicClassType.NORMAL.getCode());
			holderHistory.setHolderType(PersonType.JURIDIC.getCode());
			holderHistory.setBeginningPeriod(CommonsUtilities.currentDate());
			holderHistory.setEndingPeriod(CommonsUtilities.currentDate());
			disabledCountryResidentHolder = false;
			
		} else {
			flagHolderNaturalPerson = false;
			flagHolderJuridicPerson = false;
			disabledRepresentativeButton = true;
		}
		
		}
		catch(Exception e){
			e.getMessage();
		}
		
	}

		/**
		 * Load registration page.
		 *
		 * @return the string
		 */
	public String loadRegistrationPage() {
		try{
			setViewOperationType(ViewOperationsType.REGISTER.getCode());
	
			//SI NO TIENE PARTICIPANTE Y ES EMISOR DPF NO PUEDE REGISTRAR
			if(userInfo.getUserAccountSession().isIssuerDpfInstitucion() && !flagIsIssuerDpfInstitution){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_PARTICIPANT));
				JSFUtilities.showSimpleValidationDialog();
				return "";
			}
			
			holderRequest = new HolderRequest();		
			holderHistory = new HolderHistory();
			holderHistory.setBirthDate(CommonsUtilities.currentDate());
			
			holder = new Holder();
			participant = new Participant();
			participantRequester = new Participant();
			flagHolderNaturalPerson = false;
			flagHolderJuridicPerson = false;
			flagSendASFI = false;
			disabledRepresentativeButton = true;
			attachFileHolderSelected = null;
			holderFileNameDisplay=GeneralConstants.EMPTY_STRING;
			holderFileChangesNameDisplay=GeneralConstants.EMPTY_STRING;
			getLstParticipant();
			documentTO = new DocumentTO();		
			legalRepresentativeHelperBean.initializeLstLegalRepresentativeHistory();		
			lstRepresentativeFileHistory = new ArrayList<RepresentativeFileHistory>();		
			listHolderDocumentType = new  ArrayList<ParameterTable>();
			lstHolderReqFileHistory = new ArrayList<HolderReqFileHistory>();
			
			if(validateIsParticipant()){
				participantRequester.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
				disabledCmbParticipantRequester = true;
				Participant filter = new Participant();
				filter.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
				listParticipant = accountsFacade.getLisParticipantServiceBean(filter);		
			} else {				
				// Add validations participant DPF
				Long idParticipantCode = getIssuerDpfInstitution(userInfo);			
				if(Validations.validateIsNotNullAndNotEmpty(idParticipantCode)){
					disabledCmbParticipantRequester = true;
					flagIsIssuerDpfInstitution = true;	
					participantRequester.setIdParticipantPk(idParticipantCode);						
				} else {
					if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
						disabledCmbParticipantRequester = true;
					} else {
						disabledCmbParticipantRequester = false;
					}				
					flagIsIssuerDpfInstitution = false;	
					participantRequester.setIdParticipantPk(null);
					participantRequester = new Participant();
				}							
			}
		} catch(Exception e){
			e.printStackTrace();
		}
		return "goToModifyHolder";
	}

	
	/**
	 * Load province legal.
	 */
	public void loadProvinceLegal() {
		
		if (holderHistory.getLegalDepartment()!=null) {
			listProvinceLocation = getLstProvinceLocation(holderHistory.getLegalDepartment());
		} else {
			holderHistory.setLegalProvince(null);
			listProvinceLocation = null;
			holderHistory.setLegalDistrict(null);
			listMunicipalityLocation = null;
		}
	}
	
	
	

		
	/**
	 * Gets the lst municipality location.
	 *
	 * @param referenceFk the reference fk
	 * @return the lst municipality location
	 */
	public List<GeographicLocation> getLstMunicipalityLocation(Integer referenceFk) {

		List<GeographicLocation> lst = null;

		try {

			GeographicLocationTO geographicLocationTO = new GeographicLocationTO();

			geographicLocationTO
					.setGeographicLocationType(GeographicLocationType.DISTRICT
							.getCode());
			geographicLocationTO.setIdLocationReferenceFk(referenceFk);

			lst = generalParametersFacade
					.getListGeographicLocationServiceFacade(geographicLocationTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

		return lst;

	}

	/**
	 * Load municipality legal.
	 */
	public void loadMunicipalityLegal() {
		
		if (holderHistory.getLegalProvince()!=null) {
			listMunicipalityLocation = getLstMunicipalityLocation(holderHistory
					.getLegalProvince());
		} else {
			holderHistory.setLegalDistrict(null);
			listMunicipalityLocation = null;
		}
	}
	
	/**
	 * Load province postal.
	 */
	public void loadProvincePostal() {
		if (holderHistory.getPostalDepartment()!=null) {
			listProvincePostalLocation = getLstProvinceLocation(holderHistory.getPostalDepartment());
		} else {
			holderHistory.setPostalProvince(null);
			listProvincePostalLocation = null;
			holderHistory.setPostalDistrict(null);
			listMunicipalityPostalLocation = null;
		}
	}

	/**
	 * Gets the holder request data model.
	 *
	 * @return the holder request data model
	 */
	public HolderRequestDataModel getHolderRequestDataModel() {
		return holderRequestDataModel;
	}

	/**
	 * Sets the holder request data model.
	 *
	 * @param holderRequestDataModel the new holder request data model
	 */
	public void setHolderRequestDataModel(
			HolderRequestDataModel holderRequestDataModel) {
		this.holderRequestDataModel = holderRequestDataModel;
	}
	
	
	
	/**
	 * Validate restrictions all.
	 *
	 * @return true, if successful
	 */
	public boolean validateRestrictionsAll(){
		boolean flag=false;
		
		
		if(!validationForModification()){
			flag = true;
		}
		
		return flag;
	}
	
	/**
	 * List holidays.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listHolidays() throws ServiceException{
		  Holiday holidayFilter = new Holiday();
		  holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		  GeographicLocation countryFilter = new GeographicLocation();
		  countryFilter.setIdGeographicLocationPk(countryResidence);
		  holidayFilter.setGeographicLocation(countryFilter);
		    
		  allHolidays = CommonsUtilities.convertListDateToString(generalParametersFacade.getListHolidayDateServiceFacade(holidayFilter));
		 }
	
	
	/**
	 * Validate attach holder file.
	 *
	 * @return true, if successful
	 */
	public boolean validateAttachHolderFile(){
		boolean findCI=false;
		boolean findDIO=false;
		
		if (flagHolderNaturalPerson) {
			if (!flagIndicatorMinorHolder && !flagHolderIndicatorInability) {
               
				if (flagNationalResident || flagForeignResident) {
					
					if(lstHolderReqFileHistory.size()!=listHolderDocumentType.size()){	
						/*
						if (!validateDocument(lstHolderReqFileHistory) && lstHolderReqFileHistory.size()!=0){
							JSFUtilities.showMessageOnDialog(
									PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
									PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
							JSFUtilities.showSimpleValidationDialog();
							return false;
						}*/
						
					}
					
				}
				if (flagNationalNotResident) {
					/*
					if(lstHolderReqFileHistory.size()!=listHolderDocumentType.size()){						
						JSFUtilities.showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
						JSFUtilities.showSimpleValidationDialog();
						return false;
					}*/

				}
				if (flagForeignNotResident) {
					/*
					if(lstHolderReqFileHistory.size()!=listHolderDocumentType.size()){						
						JSFUtilities.showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
						JSFUtilities.showSimpleValidationDialog();
						return false;
					}*/
				}

			} else if (flagIndicatorMinorHolder && !flagHolderIndicatorInability) {

				if (flagNationalResident || flagForeignResident) {
					
					for(int i=0;i<lstHolderReqFileHistory.size();i++){
						if(lstHolderReqFileHistory.get(i).getDocumentType().equals(AgeLessNationalOrForeignResidentDocHolderNatType.DIO.getCode())){
						   findDIO=true;	
						}
						
						if(lstHolderReqFileHistory.get(i).getDocumentType().equals(AgeLessNationalOrForeignResidentDocHolderNatType.CI.getCode())){
							findCI=true;
						}
					}
					/*
					if(lstHolderReqFileHistory.size()==0) {
						findDIO=true;
						findCI=true;
					}
					
					if(!(findDIO || findCI)){
						JSFUtilities.showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
						JSFUtilities.showSimpleValidationDialog();
						return false;
					}
					*/
					
				}
				
				if (flagNationalNotResident) {
					
					if(lstHolderReqFileHistory.size()!=listHolderDocumentType.size()){						
						JSFUtilities.showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
						JSFUtilities.showSimpleValidationDialog();
						return false;
					}
					
				}
				if (flagForeignNotResident) {
					
					if(lstHolderReqFileHistory.size()!=listHolderDocumentType.size()){						
						JSFUtilities.showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
						JSFUtilities.showSimpleValidationDialog();
						return false;
					}
				}
			} else if (flagHolderIndicatorInability) {

				if (flagNationalResident || flagForeignResident) {

					if(lstHolderReqFileHistory.size()!=listHolderDocumentType.size()){						
						JSFUtilities.showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
						JSFUtilities.showSimpleValidationDialog();
						return false;
					}
				}
				if (flagNationalNotResident) {
					
					if(lstHolderReqFileHistory.size()!=listHolderDocumentType.size()){						
						JSFUtilities.showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
						JSFUtilities.showSimpleValidationDialog();
						return false;
					}
					
				}
				if (flagForeignNotResident) {
					
					if(lstHolderReqFileHistory.size()!=listHolderDocumentType.size()){						
						JSFUtilities.showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
						JSFUtilities.showSimpleValidationDialog();
						return false;
					}
					
				}
			}
		}
		
		if (flagHolderJuridicPerson) {

			if ((flagNationalResident || flagForeignResident) 
					&&
					holderHistory.getJuridicClass()!=null && 
					!holderHistory.getJuridicClass().equals(JuridicClassType.ENTITY_STATE.getCode()) &&
					!holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())
					) {
				
				if(lstHolderReqFileHistory.size()!=listHolderDocumentType.size()){
//					Issue 512
					if(lstHolderReqFileHistory.size()>0){
						boolean verifDocument =true;

						for(HolderReqFileHistory holderFile :lstHolderReqFileHistory){
														
						/*	if(holderFile.getDescription().equalsIgnoreCase("FOTOCOPIA DE N\u00daMERO DE IDENTIFICACI\u00d3N TRIBUTARIA")){
								verifDocument=false;
							} */
							
							if(holderFile.getDocumentType() !=  null && holderFile.getDescription().equalsIgnoreCase("CONSTANCIA DE RUC") ){ //CONSTANCIA DE RUC PARA normal/estatal/fideicomiso
								verifDocument=false;
							}
							
						}
						if(verifDocument){
							JSFUtilities.showMessageOnDialog(
									PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
									PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
							JSFUtilities.showSimpleValidationDialog();
							return false;
						}
						
					}else{
						JSFUtilities.showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
						JSFUtilities.showSimpleValidationDialog();
						return false;
					}
					
				}
				// Issue 512
			
			}

			if (flagForeignNotResident 
					&&
					holderHistory.getJuridicClass()!=null && 
					!holderHistory.getJuridicClass().equals(JuridicClassType.ENTITY_STATE.getCode()) &&
					!holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())
 				) {
				
				if(lstHolderReqFileHistory.size()!=listHolderDocumentType.size()){						
					JSFUtilities.showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
							PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
					JSFUtilities.showSimpleValidationDialog();
					return false;
				}
				
			}

			if (holderHistory.getJuridicClass()!=null && holderHistory.getJuridicClass().equals(JuridicClassType.ENTITY_STATE.getCode())) {
			
				if(lstHolderReqFileHistory.size()!=listHolderDocumentType.size()){						
					JSFUtilities.showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
							PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
					JSFUtilities.showSimpleValidationDialog();
					return false;
				}
			
			}

			if (holderHistory.getJuridicClass()!=null && holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode()) && holderHistory.getIndResidence()!=null && holderHistory.getIndResidence().equals(BooleanType.NO.getCode())) {
				
				if(lstHolderReqFileHistory.size()!=listHolderDocumentType.size()){						
					JSFUtilities.showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
							PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
					JSFUtilities.showSimpleValidationDialog();
					return false;
				}
				
			}

			else if (holderHistory.getJuridicClass()!=null && holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())) {
				
				if(lstHolderReqFileHistory.size()!=listHolderDocumentType.size()){						
					JSFUtilities.showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
							PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE));
					JSFUtilities.showSimpleValidationDialog();
					return false;
				}
				
			}

		}
		
		if (lstHolderReqChangesFileHistory == null || lstHolderReqChangesFileHistory.size() < 1) {
			
			JSFUtilities.showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage(PropertiesConstants.HOLDER_CHANGES_MSG_REQUIRE_SUPPORTING_FILE));
			JSFUtilities.showSimpleValidationDialog();
			indChangesNeedsConfirm = true;
			return false;
		}
            
        
		
		return true;

	}
	
	private boolean validateDocument(List<HolderReqFileHistory> listHolderReqFileHistorie){
		boolean validate=false;
		
		for (HolderReqFileHistory lista : listHolderReqFileHistorie){
			if (lista.getDocumentType().equals(AgeMajorDomOrForeignResidentDocHolderNatType.CE.getCode())){
				validate=true;
			}
		}
		
		return validate;
	}

	

	/**
	 * Before save all.
	 */
	public void beforeSaveAll() {
	
      try{
    	  
    	  if(Validations.validateIsNull(newDocumentNumber) || Validations.validateIsNull(holderHistory.getDocumentNumber())) {
        	  newDocumentNumber = null;
        	  holderHistory.setDocumentNumber(null);
    		  JSFUtilities.showMessageOnDialog(
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_DOCUMENT_NUMBER_EMPTY));
				JSFUtilities.showSimpleValidationDialog();
				return;
    	  }
    	  /* Issue 1231-
  		 * Los procesos de adjuntar documentos que no serAn obligatorios y que 
  		 * estAn a cargo de los Participantes, serA el siguiente:
  		 * - Registro de Titular JurIdico
  		 * - Registro de Titular Natural
  		 * - Registro Cuenta Titular
      	 * 
      	 */    	
    	  
    	if(!validateAttachHolderFile()){  		  
  			return;
  		} 
		
		Long idParticipant = null;
		if(participantRequester!=null){
			if(participantRequester.getIdParticipantPk()!=null){
				idParticipant = participantRequester.getIdParticipantPk();
			}
		}
		
		if(!validateParticipantWithHolder(idParticipant,holder)){
			return;
		}
	
		if (holderHistory.getHolderType().equals(PersonType.NATURAL.getCode())) {
			// seteando los nuevos valores de los documentos de identidad  1050
			holderHistory.setDocumentNumber(newDocumentNumber);
			holderHistory.setSecondDocumentNumber(newDocumentNumberSecondNationality);
			
			if(Validations.validateIsNullOrEmpty(filePhotoTemp) && Validations.validateIsNullOrEmpty(holder.getFilePhoto())){
	    		JSFUtilities.addContextMessage("frmHolderModify:fuplFormPhotoHolder",
	    				FacesMessage.SEVERITY_ERROR,
	    				PropertiesUtilities.getMessage(PropertiesConstants.MSG_ERROR_ANNULATECUI_REQUIRED_FILEUPLOAD), 
	    				PropertiesUtilities.getMessage(PropertiesConstants.MSG_ERROR_ANNULATECUI_REQUIRED_FILEUPLOAD));
	    		showMessageOnDialog(
	    				PropertiesConstants.MESSAGES_ALERT,
	    				null,
	    				PropertiesConstants.MESSAGE_DATA_REQUIRED,							
	    				null);
	    		JSFUtilities.showSimpleValidationDialog();
	    		return;
	    	}
			// issue 1233: se quita esta validacion (IndicatorDisabled) porque no aplica a nuestro pais
			//Issue 1291 Modificacion Representante Legal para menor de Edad
//				if (holderHistory.isIndicatorMinor()) {
//					if (legalRepresentativeHelperBean.getLstLegalRepresentativeHistory() == null
//							|| legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size() == 0) {
//						Object[] bodyData = new Object[0];
//						
//						JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
//								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_NECESSARY_LEGAL_REPRESENTATIVE,bodyData));
//			
//						JSFUtilities.showSimpleValidationDialog();
//
//						return;
//					}
//				}
		} else if (holderHistory.getHolderType().equals(
				PersonType.JURIDIC.getCode())) {
			if (legalRepresentativeHelperBean.getLstLegalRepresentativeHistory() == null
					|| legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size() == 0) {

				Object[] bodyData = new Object[0];
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_NECESSARY_LEGAL_REPRESENTATIVE,bodyData));

				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			if(holderHistory.getHolderType().equals(
					PersonType.JURIDIC.getCode()) && holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())){
				
				int count=0;
				
				 if(legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size()<2){
					 Object[] bodyData = new Object[0];
					 JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							 PropertiesUtilities.getMessage(PropertiesConstants.WARNING_NECESSARY_TWO_LEGAL_REPRESENTATIVE,bodyData));

					 JSFUtilities.showSimpleValidationDialog();
					 return;
				 }
				 if(legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size()>=2){
					 for(int i=0;i<legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size();i++){
						 if(legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().get(i).getRepresentativeClass().equals(RepresentativeClassType.FIDUCIARY.getCode())){
							 count++;
						 }
					 }
					 
					 if(count==0){
						 Object[] bodyData = new Object[0];
						 JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
								 PropertiesUtilities.getMessage(PropertiesConstants.WARNING_NECESSARY_TWO_LEGAL_REPRESENTATIVE,	bodyData));							

						 JSFUtilities.showSimpleValidationDialog();
		                    return;
					 }
				 }
			}
		}
		
		Object[] bodyData = new Object[1];
		bodyData[0]=holder.getIdHolderPk().toString();

		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
				PropertiesUtilities.getMessage(PropertiesConstants.ADM_REQUEST_HOLDER_CONFIRM_MODIFY,bodyData));

		JSFUtilities.executeJavascriptFunction("PF('cnfwAdmRequestHolder').show()");

	   }
		catch(Exception e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
		
	/**
	 * Validate is not null holder files.
	 *
	 * @return true, if successful
	 */
	public boolean validateIsNotNullHolderFiles() {

			 

			boolean flag = true;
			
			if(listHolderDocumentType==null || (listHolderDocumentType!=null && listHolderDocumentType.size()<1)){
				flag = true;
			}
			else{
				
				if(listHolderDocumentType.size()!=lstHolderReqFileHistory.size()){
					flag = true;
				}
				else{
					flag = false;
				}
				
			}
			
			if(flag){
				JSFUtilities.showMessageOnDialog(null, null,
						PropertiesConstants.WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE,
						null);
				JSFUtilities.showSimpleValidationDialog();
			}
			
			
		return flag;
		
		}

	/**
	 * Clean all.
	 */
	public void cleanAll() {
		
		JSFUtilities.resetViewRoot();
		
		holderRequest = new HolderRequest();
		holderRequestSelected=null;
		holderFilter = new HolderRequestTO();
		holderHistory = new HolderHistory();
		holderRequestDataModel = null;
		participant = new Participant();
		holderFilter.setInitialDate(CommonsUtilities.currentDate());
		holderFilter.setEndDate(CommonsUtilities.currentDate());
		holderHistory.setBeginningPeriod(CommonsUtilities.currentDate());
		holderHistory.setEndingPeriod(CommonsUtilities.currentDate());
		flagCopyLegalHolder = false;
		flagHolderJuridicPerson = false;
		flagHolderNaturalPerson = false;
		flagCUIRelated = false;
		flagNemonicAsfi = false;
		flagNemonicFund = false;
		flagTransferNumber = false;
		disabledTransferNumber = true;
		disabledNemonic = true;
		holder = new Holder(); 
		legalRepresentativeHelperBean.setLstLegalRepresentativeHistory(null);
		legalRepresentativeHelperBean.setLstLegalRepresentativeFile(null);
		legalRepresentativeHelperBean.setLstRepresentativeFileHistory(null);
		legalRepresentativeHelperBean.setDisabledHolderHistory(false);
		legalRepresentativeHelperBean.setDisabledHolderRequest(false);
		lstHolderFile = null;
		inFalseFlagNationalForeign();
		participantRequester = new Participant();
		filePhotoTemp = null;
		photoHolderName = "";
		indChangesNeedsConfirm=false;
		
		
		if (validateIsParticipant()) {
			disabledCmbParticipant = true;
			holderFilter.setParticipantSelected(userInfo.getUserAccountSession().getParticipantCode());
			participantRequester.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
			disabledCmbParticipantRequester = true;
		} else {			
			// Add validations participant DPF
			Long idParticipantCode = getIssuerDpfInstitution(userInfo);			
			if(Validations.validateIsNotNullAndNotEmpty(idParticipantCode)){
				disabledCmbParticipantRequester = true;
				disabledCmbParticipant = true;
				flagIsIssuerDpfInstitution = true;	
				participantRequester.setIdParticipantPk(idParticipantCode);
				holderFilter.setParticipantSelected(idParticipantCode);
			} else {
				if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
					disabledCmbParticipantRequester = true;
					disabledCmbParticipant = true;
				} else {
					disabledCmbParticipantRequester = false;
					disabledCmbParticipant = false;
				}				
				flagIsIssuerDpfInstitution = false;	
				holderFilter.setParticipantSelected(null);
				participantRequester.setIdParticipantPk(null);
				participantRequester = new Participant();
			}																						
		}		
	}

	/**
	 * Load holder document type.
	 */
	public void loadHolderDocumentType() {

		 

		listDocumentTypeResult = new ArrayList<ParameterTable>();

		PersonTO personTo = new PersonTO();
		personTo.setFlagForeignNotResident(flagForeignNotResident);
		personTo.setFlagInabilityIndicator(flagHolderIndicatorInability);
		personTo.setFlagMinorIndicator(flagIndicatorMinorHolder);
		personTo.setFlagHolderIndicator(true);
		personTo.setFlagNationalNotResident(flagNationalNotResident);
		personTo.setFlagNationalResident(flagNationalResident);
		personTo.setFlagForeignResident(flagForeignResident);
		personTo.setPersonType(holderHistory.getHolderType());
		
		
		if(holderHistory.getHolderType()!=null && holderHistory.getHolderType().equals(PersonType.JURIDIC.getCode())){
			if(holderHistory.getJuridicClass()!=null && holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())){
				personTo.setFlagJuridicClassTrust(true);
			}
			else{
				personTo.setFlagJuridicClassTrust(false);
			}
		}
		else{
			personTo.setFlagJuridicClassTrust(false);
		}
		
		listDocumentTypeResult = documentValidator.getDocumentTypeByPerson(personTo);
	}
	
	/**
	 * Load holder second document type.
	 */
	public void loadHolderSecondDocumentType() {
		
		PersonTO personTo = new PersonTO();
		
		personTo.setFlagHolderIndicator(true);
		
		if(holderHistory.getHolderType()!=null){
			personTo.setPersonType(holderHistory.getHolderType());
		}


		if (holderHistory.getSecondNationality() != null
				&& (holderHistory.getSecondNationality().equals(countryResidence))) {
			
			if (BooleanType.YES.getCode().equals(
					holderHistory.getIndResidence())) {
				personTo.setFlagNationalResident(true);
			
			} else if (BooleanType.NO.getCode().equals(
					holderHistory.getIndResidence())) {
				personTo.setFlagNationalNotResident(true);
			}

		} else if (holderHistory.getSecondNationality() != null
				&& !(holderHistory.getSecondNationality().equals(countryResidence))) {
			
			personTo.setFlagForeignResident(false);
			personTo.setFlagForeignNotResident(true);

		}
		
		if(flagHolderIndicatorInability){
			personTo.setFlagInabilityIndicator(true);
		}
		
		if (holderHistory.getHolderType().equals(PersonType.NATURAL.getCode())
				&& holderHistory.getIndMinor()!=null && holderHistory.getIndMinor().equals(BooleanType.YES.getCode())) {
			personTo.setFlagMinorIndicator(true);
		}
		
		if(holderHistory.getHolderType()!=null && holderHistory.getHolderType().equals(PersonType.JURIDIC.getCode())){
			if(holderHistory.getJuridicClass()!=null && holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())){
				personTo.setFlagJuridicClassTrust(true);
			}
			else{
				personTo.setFlagJuridicClassTrust(false);
			}
		}
		else{
			personTo.setFlagJuridicClassTrust(false);
		}

		listSecondDocumentType = documentValidator.getDocumentTypeByPerson(personTo);
		newDocumentNumberSecondNationality = null;
		
		
	}

	/**
	 * Save all.
	 */
	@LoggerAuditWeb
	public void saveAll() {

		try {
			
			 
			if(legalRepresentativeHelperBean.getLstLegalRepresentativeHistory()!=null && legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size()>0){			
								
				holderRequest.setLegalRepresentativeHistories(legalRepresentativeHelperBean.getLstLegalRepresentativeHistory());
			}
			
			holderRequest.setRegistryDate(CommonsUtilities.currentDate());
			holderRequest.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			holderRequest.setStateHolderRequest(HolderRequestStateType.REGISTERED.getCode());
			holderRequest.setParticipant(participantRequester);
			holderRequest.setRequestType(HolderRequestType.MODIFICATION.getCode());
						
			holderHistory.setRegistryDate(CommonsUtilities.currentDate());
			holderHistory.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			holderHistory.setStateHolderHistory(HolderRequestStateType.REGISTERED.getCode());
			holderHistory.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
			holderHistory.setIdHolderHistoryPk(null);
			holderHistory.setHolderRequest(holderRequest);
			holderHistory.setHolderHistoryType(HolderRequestType.MODIFICATION.getCode());
			
			if(filePhotoTemp!=null) {
				holderHistory.setFilePhoto(filePhotoTemp);
				holderHistory.setFilePhotoName(photoHolderName);
			}
			
			if(holderHistory.getSecondNationality()==null){
				holderHistory.setSecondDocumentType(null);
				holderHistory.setSecondDocumentNumber(null);
			}
			
			if(holderHistory.getSecondDocumentType()==null){
				holderHistory.setSecondDocumentNumber(null);
			}
			if(holderHistory.getSecondDocumentNumber()==null){
				holderHistory.setSecondDocumentType(null);
			}
			
			if(holderHistory.isIndicatorMinor()){
				holderHistory.setIndMinor(BooleanType.YES.getCode());
			}else{
				holderHistory.setIndMinor(BooleanType.NO.getCode());
			}
			
			if(holderHistory.getHolderType().equals(PersonType.JURIDIC.getCode())){
				holderHistory.setIndMinor(BooleanType.NO.getCode());
				holderHistory.setIndDisabled(BooleanType.NO.getCode());
				
				if(holderHistory.getJuridicClass()!=null && holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())){
					//holderHistory.setDocumentNumber(holderHistory.getDocumentNumber()+GeneralConstants.HYPHEN+participant.getIdParticipantPk());
					holderHistory.setDocumentNumber(holderHistory.getDocumentNumber());
				}
			}
			
			if (holderHistory.isIndicatorPep()) {
				holderHistory.setIndPEP(BooleanType.YES.getCode());
			} else {
				holderHistory.setIndPEP(BooleanType.NO.getCode());
				holderHistory.setCategory(null);
				holderHistory.setRole(null);
				holderHistory.setBeginningPeriod(null);
				holderHistory.setEndingPeriod(null);
				holderHistory.setGrade(null);
				holderHistory.setPepRelatedName(null);
			}
			
			if (Validations.validateIsNull(holderHistory.getEmail())
					|| Validations.validateIsEmpty(holderHistory.getEmail())) {
				InputText objInputText = (InputText) FacesContext
						.getCurrentInstance().getViewRoot()
						.findComponent("frmHolderModify:tabView:inputEmail");
				objInputText.setDisabled(true);
			}
			else{
				holderHistory.setEmail(holderHistory.getEmail().toLowerCase());
			}

			List<HolderHistory> lstHolderHistory = new ArrayList<HolderHistory>();
			lstHolderHistory.add(holderHistory);
			
			holderRequest.setHolderHistories(lstHolderHistory);
			
			if (Validations.validateIsNotNullAndNotEmpty(lstHolderReqFileHistory)){
				for(int i=0;i<lstHolderReqFileHistory.size();i++){
					lstHolderReqFileHistory.get(i).setHolderRequest(holderRequest);
				}
			}
            if(Validations.validateIsNotNullAndNotEmpty(lstHolderReqChangesFileHistory)) {
                for(int i = 0; i < lstHolderReqChangesFileHistory.size(); i++) {
                        lstHolderReqChangesFileHistory.get(i).setHolderRequest(holderRequest);
                        
                        if(Validations.validateIsNotNullAndNotEmpty(lstHolderReqFileHistory)) {
                                lstHolderReqFileHistory.add(lstHolderReqChangesFileHistory.get(i));
                        }
                }
            }
			if(lstHolderReqFileHistory!=null && lstHolderReqFileHistory.size()>0){
				holderRequest.setHolderReqFileHistories(lstHolderReqFileHistory);
			} else if(Validations.validateIsNotNullAndNotEmpty(lstHolderReqChangesFileHistory)) {
                holderRequest.setHolderReqFileHistories(lstHolderReqChangesFileHistory);
			}    
			else{
				holderRequest.setHolderReqFileHistories(null);
			}
			
			holderRequest.setHolder(holder);

			boolean flagTransaction = holderServiceFacade.registerModificationHolderRequestServiceFacade(holderRequest);
			
			if(flagTransaction)
			{
									
				Object[] bodyData = new Object[1];

				bodyData[0] = holder.getIdHolderPk().toString();

				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						PropertiesUtilities.getMessage(PropertiesConstants.LBL_MODIFY_HOLDER_REQUEST,bodyData));
				JSFUtilities.executeJavascriptFunction("PF('widgTransactionDialog').show()");
				
				businessProcess = new BusinessProcess();
				
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_MODIFICATION_REQUEST_REGISTER.getCode());
				Object[] parameters = new Object[2];
				parameters[0]=holderRequest.getHolder().getIdHolderPk().toString();
				parameters[1]=holderRequest.getIdHolderRequestPk().toString();				
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,holderRequest.getParticipant().getIdParticipantPk(),parameters);
				
				newDocumentNumberSecondNationality = null;
				newDocumentNumber = null;
			}

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}
	
	/**
	 * Reset components.
	 */
	public void validateBack(){
		
		if(requestMotive!=null){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(PropertiesConstants.MESSAGES_ALERT),PropertiesUtilities.getGenericMessage(PropertiesConstants.MESSAGE_BACK));
			JSFUtilities.executeJavascriptFunction("PF('cnfwBackMotive').show()");
		}else{
			resetComponents();
			JSFUtilities.executeJavascriptFunction("PF('dlgMotive').hide()");
		}
		
		
	}

	
	/**
	 * Reset components.
	 */
	public void resetComponents(){
		JSFUtilities.setValidViewComponentAndChildrens("frmHolderDetails");
	}

	/**
	 * Action back.
	 */
	public void actionBack() {

		 
		JSFUtilities.executeJavascriptFunction("PF('dlgTab').hide()");

	}

	/**
	 * Gets the holder request.
	 *
	 * @return the holder request
	 */
	public HolderRequest getHolderRequest() {
		return holderRequest;
	}

	/**
	 * Sets the holder request.
	 *
	 * @param holderRequest the new holder request
	 */
	public void setHolderRequest(HolderRequest holderRequest) {
		this.holderRequest = holderRequest;
	}

	/**
	 * Loading holder information.
	 *
	 * @param event the event
	 */
	public void loadingHolderInformation(ActionEvent event) {
		
		try
		{
		cleanFileImage();
		legalRepresentativeHelperBean.initializeLstLegalRepresentativeHistoryDetail();
		legalRepresentativeHelperBean.getLegalRepresentativeTO().setViewOperationType(ViewOperationsType.DETAIL.getCode());
		
		lstHolderReqFileHistory = new ArrayList<HolderReqFileHistory>();
		
		participantDetail = new Participant();
		participantRequesterDetail = new Participant();
		
		holderRequestDetail = new HolderRequest();
		holderHistoryDetail = new HolderHistory();
		
		participantDetailBefore = new Participant();
		holderRequestDetailBefore = new HolderRequest();
		holderHistoryDetailBefore = new HolderHistory();
		
		lstHolderReqFileHistoryDetail = new ArrayList<HolderReqFileHistory>();
		lstHolderReqChangesMotiveFileHistoryDetail = new ArrayList<HolderReqFileHistory>();
		
		holderHistory = new HolderHistory();
		holderHistoryBefore = new HolderHistory();
		
		if(event!=null){
		setViewOperationType(ViewOperationsType.DETAIL.getCode());
			
		holderRequestDetail = (HolderRequest)event.getComponent().getAttributes().get("holderInformation");
		
		}
		else{
			 holderRequestDetail = holderRequestSelected;
		}
		
		holderRequestDetail = holderServiceFacade.getHolderRequestByIdRequestServiceFacade(holderRequestDetail.getIdHolderRequestPk());
		
		if(holderRequestDetail.getStateHolderRequest()!=null){
			holderRequestDetail.setStateDescription(requestStateDescription.get(holderRequestDetail.getStateHolderRequest()).toString());
	    }
	    
		holderHistoryDetail = holderRequestDetail.getHolderHistories().get(0);	
		listInvestorType = investorTypeFacade.getLstInvestorTypeModelTO(new InvestorTypeModelTO());
		
		JSFUtilities.putSessionMap("sessStreamedPhotoModify", holderHistoryDetail.getFilePhoto());
		
		JSFUtilities.putSessionMap("sessStreamedSigningModify", holderHistoryDetail.getFileSigning());
		
		if(holderRequestDetail.getHolderReqFileHistories()!=null && holderRequestDetail.getHolderReqFileHistories().size()>0){
            for(int i = 0; i < holderRequestDetail.getHolderReqFileHistories().size(); i++) {
                
                if(holderRequestDetail.getHolderReqFileHistories().get(i).getDocumentType() != 2847) {
                        lstHolderReqFileHistoryDetail.add(holderRequestDetail.getHolderReqFileHistories().get(i));
                } else {
                		indChangesNeedsConfirm = true;
                        lstHolderReqChangesMotiveFileHistoryDetail.add(holderRequestDetail.getHolderReqFileHistories().get(i));
                }
                
            }
			//lstHolderReqFileHistoryDetail = holderRequestDetail.getHolderReqFileHistories();	
		}
		
		if((holderRequestDetail.getParticipant().getIdParticipantPk()!=null)){
			participantRequesterDetail.setIdParticipantPk(holderRequestDetail.getParticipant().getIdParticipantPk());				
		}
		
		participantDetail.setIdParticipantPk(holderRequestDetail.getHolder().getParticipant().getIdParticipantPk());

		getLstBooleanType();
		getLstDocumentType();
		getLstParticipant();
		getLstPersonType();
		getLstSexType();
		getLstJuridicClass();
		getLstGeographicLocation();
		getLstGeographicLocationWithoutNationalCountry();
		getLstRolePep();
		getLstCategoryPep();
		getLstEconomicSector();
		changeSelectedEconomicSector();
		getLstDocumentSource();
		getGradePep();
		getListEconomicActivityWithoutSector();
		
		listDocumentTypeResult = listDocumentType;
		
		listDepartmentLocation = getLstDepartmentLocation(holderHistoryDetail.getLegalResidenceCountry());
		
		listProvinceLocation = getLstProvinceLocation(holderHistoryDetail.getLegalDepartment());
		
		listMunicipalityLocation = getLstMunicipalityLocation(holderHistoryDetail.getLegalProvince());
		
		listDepartmentPostalLocation = getLstDepartmentLocation(holderHistoryDetail.getPostalResidenceCountry());
		
		listProvincePostalLocation = getLstProvinceLocation(holderHistoryDetail.getPostalDepartment());

		listMunicipalityPostalLocation = getLstMunicipalityLocation(holderHistoryDetail.getPostalProvince());
		
		if(holderHistoryDetail.getHolderType().equals(PersonType.NATURAL.getCode())){
			
			flagHolderJuridicPerson = false;
			flagHolderNaturalPerson = true;
			
		}else if(holderHistoryDetail.getHolderType().equals(PersonType.JURIDIC.getCode())){
			
			flagHolderJuridicPerson = true;
			flagHolderNaturalPerson = false;
			
		}
				
		if(holderRequestDetail.getHolderHistories().get(0).getIndPEP()!=null && holderRequestDetail.getHolderHistories().get(0).getIndPEP().equals(BooleanType.YES.getCode())){
			holderRequestDetail.getHolderHistories().get(0).setIndicatorPep(true);
			holderHistoryDetail.setCategory(holderRequestDetail.getHolderHistories().get(0).getCategory());
			holderHistoryDetail.setRole(holderRequestDetail.getHolderHistories().get(0).getRole());
			holderHistoryDetail.setBeginningPeriod(holderRequestDetail.getHolderHistories().get(0).getBeginningPeriod());
			holderHistoryDetail.setEndingPeriod(holderRequestDetail.getHolderHistories().get(0).getEndingPeriod());	
			holderHistoryDetail.setGrade(holderRequestDetail.getHolderHistories().get(0).getGrade());
			holderHistoryDetail.setPepRelatedName(holderRequestDetail.getHolderHistories().get(0).getPepRelatedName());
		}
		else if(holderRequestDetail.getHolderHistories().get(0).getIndPEP().equals(BooleanType.NO.getCode())){
			holderRequestDetail.getHolderHistories().get(0).setIndicatorPep(false);
			holderHistoryDetail.setCategory(null);
			holderHistoryDetail.setRole(null);
			holderHistoryDetail.setBeginningPeriod(null);
			holderHistoryDetail.setEndingPeriod(null);			
		
		}
		
		
		if(holderRequestDetail.getHolderHistories().get(0).getIndDisabled()!=null && (holderRequestDetail.getHolderHistories().get(0).getIndDisabled().equals(BooleanType.YES.getCode()))){
			holderRequestDetail.getHolderHistories().get(0).setIndicatorDisabled(true);
	    }
		else if(holderRequestDetail.getHolderHistories().get(0).getIndDisabled()!=null && (holderRequestDetail.getHolderHistories().get(0).getIndDisabled().equals(BooleanType.NO.getCode()))){
			holderRequestDetail.getHolderHistories().get(0).setIndicatorDisabled(false);
		}	
		
		if(holderRequestDetail.getHolderHistories().get(0).getIndMinor().equals(BooleanType.YES.getCode())){
			holderRequestDetail.getHolderHistories().get(0).setIndicatorMinor(true);
		}
		else if(holderRequestDetail.getHolderHistories().get(0).getIndMinor().equals(BooleanType.NO.getCode())){
			holderRequestDetail.getHolderHistories().get(0).setIndicatorMinor(false);
		}
		
		
		if(holderRequestDetail.getLegalRepresentativeHistories()!=null){
			List<LegalRepresentativeHistory> lstLegalRepresentativeHistoryDetail = new ArrayList<LegalRepresentativeHistory>();
			
			for(int i=0;i<holderRequestDetail.getLegalRepresentativeHistories().size();i++){				
				
				LegalRepresentativeHistory legalRepresentativeHistoryAux = new LegalRepresentativeHistory();
			
				
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getIndPEP()!=null && holderRequestDetail.getLegalRepresentativeHistories().get(i).getIndPEP().equals(BooleanType.YES.getCode())){
					legalRepresentativeHistoryAux.setCategory(holderRequestDetail.getLegalRepresentativeHistories().get(i).getCategory());
					legalRepresentativeHistoryAux.setRole(holderRequestDetail.getLegalRepresentativeHistories().get(i).getRole());
					legalRepresentativeHistoryAux.setBeginningPeriod(holderRequestDetail.getLegalRepresentativeHistories().get(i).getBeginningPeriod());
					legalRepresentativeHistoryAux.setEndingPeriod(holderRequestDetail.getLegalRepresentativeHistories().get(i).getEndingPeriod());
				}
				else if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getIndPEP()!=null && holderRequestDetail.getLegalRepresentativeHistories().get(i).getIndPEP().equals(BooleanType.NO.getCode())){
					legalRepresentativeHistoryAux.setCategory(null);
					legalRepresentativeHistoryAux.setRole(null);
					legalRepresentativeHistoryAux.setBeginningPeriod(null);
					legalRepresentativeHistoryAux.setEndingPeriod(null);
									
				}
				
				
				
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getIdRepresentativeHistoryPk()!=null){
					legalRepresentativeHistoryAux.setIdRepresentativeHistoryPk(holderRequestDetail.getLegalRepresentativeHistories().get(i).getIdRepresentativeHistoryPk());
				}
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getBirthDate()!=null){
					legalRepresentativeHistoryAux.setBirthDate(holderRequestDetail.getLegalRepresentativeHistories().get(i).getBirthDate());
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getDocumentNumber()!=null){
					legalRepresentativeHistoryAux.setDocumentNumber(holderRequestDetail.getLegalRepresentativeHistories().get(i).getDocumentNumber());
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getDocumentType()!=null){
					legalRepresentativeHistoryAux.setDocumentType(holderRequestDetail.getLegalRepresentativeHistories().get(i).getDocumentType());
				}
				
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getDocumentSource()!=null){
					legalRepresentativeHistoryAux.setDocumentSource(holderRequestDetail.getLegalRepresentativeHistories().get(i).getDocumentSource());
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getEconomicActivity()!=null){
					legalRepresentativeHistoryAux.setEconomicActivity(holderRequestDetail.getLegalRepresentativeHistories().get(i).getEconomicActivity());
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getEconomicSector()!=null){
					legalRepresentativeHistoryAux.setEconomicSector(holderRequestDetail.getLegalRepresentativeHistories().get(i).getEconomicSector());
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getEmail()!=null){
					legalRepresentativeHistoryAux.setEmail(holderRequestDetail.getLegalRepresentativeHistories().get(i).getEmail());
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getFaxNumber()!=null){
					legalRepresentativeHistoryAux.setFaxNumber(holderRequestDetail.getLegalRepresentativeHistories().get(i).getFaxNumber());
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getFirstLastName()!=null){
					legalRepresentativeHistoryAux.setFirstLastName(holderRequestDetail.getLegalRepresentativeHistories().get(i).getFirstLastName());
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getFullName()!=null){
					legalRepresentativeHistoryAux.setFullName(holderRequestDetail.getLegalRepresentativeHistories().get(i).getFullName());
				}
				
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getHomePhoneNumber()!=null){
					legalRepresentativeHistoryAux.setHomePhoneNumber(holderRequestDetail.getLegalRepresentativeHistories().get(i).getHomePhoneNumber());
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getIndResidence()!=null){
					
					legalRepresentativeHistoryAux.setIndResidence(holderRequestDetail.getLegalRepresentativeHistories().get(i).getIndResidence()); 
				}
				
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getLastModifyApp()!=null){
					legalRepresentativeHistoryAux.setLastModifyApp(holderRequestDetail.getLegalRepresentativeHistories().get(i).getLastModifyApp());
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getLastModifyDate()!=null){
					legalRepresentativeHistoryAux.setLastModifyDate(holderRequestDetail.getLegalRepresentativeHistories().get(i).getLastModifyDate());
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getLastModifyIp()!=null){
					legalRepresentativeHistoryAux.setLastModifyIp(holderRequestDetail.getLegalRepresentativeHistories().get(i).getLastModifyIp());
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getLastModifyUser()!=null){
					legalRepresentativeHistoryAux.setLastModifyUser(holderRequestDetail.getLegalRepresentativeHistories().get(i).getLastModifyUser());
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getLegalAddress()!=null){
					legalRepresentativeHistoryAux.setLegalAddress(holderRequestDetail.getLegalRepresentativeHistories().get(i).getLegalAddress());
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getLegalDepartment()!=null){
					legalRepresentativeHistoryAux.setLegalDepartment(holderRequestDetail.getLegalRepresentativeHistories().get(i).getLegalDepartment());
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getLegalDistrict()!=null){
					legalRepresentativeHistoryAux.setLegalDistrict(holderRequestDetail.getLegalRepresentativeHistories().get(i).getLegalDistrict());
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getLegalProvince()!=null){
					legalRepresentativeHistoryAux.setLegalProvince(holderRequestDetail.getLegalRepresentativeHistories().get(i).getLegalProvince());
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getLegalResidenceCountry()!=null){
					legalRepresentativeHistoryAux.setLegalResidenceCountry(holderRequestDetail.getLegalRepresentativeHistories().get(i).getLegalResidenceCountry());
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getMobileNumber()!=null){
					legalRepresentativeHistoryAux.setMobileNumber(holderRequestDetail.getLegalRepresentativeHistories().get(i).getMobileNumber());
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getName()!=null){
					legalRepresentativeHistoryAux.setName(holderRequestDetail.getLegalRepresentativeHistories().get(i).getName()); 
				}
				
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getNationality()!=null){
					legalRepresentativeHistoryAux.setNationality(holderRequestDetail.getLegalRepresentativeHistories().get(i).getNationality());
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getOfficePhoneNumber()!=null){
					legalRepresentativeHistoryAux.setOfficePhoneNumber(holderRequestDetail.getLegalRepresentativeHistories().get(i).getOfficePhoneNumber());
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getPostalDepartment()!=null){
					legalRepresentativeHistoryAux.setPostalDepartment(holderRequestDetail.getLegalRepresentativeHistories().get(i).getPostalDepartment());
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getPostalDistrict()!=null){
					legalRepresentativeHistoryAux.setPostalDistrict(holderRequestDetail.getLegalRepresentativeHistories().get(i).getPostalDistrict());
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getPostalProvince()!=null){
					legalRepresentativeHistoryAux.setPostalProvince(holderRequestDetail.getLegalRepresentativeHistories().get(i).getPostalProvince());
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getPostalResidenceCountry()!=null){
					legalRepresentativeHistoryAux.setPostalResidenceCountry(holderRequestDetail.getLegalRepresentativeHistories().get(i).getPostalResidenceCountry());
				}
				
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getPostalAddress()!=null){
					legalRepresentativeHistoryAux.setPostalAddress(holderRequestDetail.getLegalRepresentativeHistories().get(i).getPostalAddress());
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getRegistryUser()!=null){
					legalRepresentativeHistoryAux.setRegistryUser(holderRequestDetail.getLegalRepresentativeHistories().get(i).getRegistryUser());
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getSecondDocumentNumber()!=null){
					legalRepresentativeHistoryAux.setSecondDocumentNumber(holderRequestDetail.getLegalRepresentativeHistories().get(i).getSecondDocumentNumber());
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getSecondLastName()!=null){
					legalRepresentativeHistoryAux.setSecondLastName(holderRequestDetail.getLegalRepresentativeHistories().get(i).getSecondLastName());
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getSecondNationality()!=null){
					legalRepresentativeHistoryAux.setSecondNationality(holderRequestDetail.getLegalRepresentativeHistories().get(i).getSecondNationality());	
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getSex()!=null){
					legalRepresentativeHistoryAux.setSex(holderRequestDetail.getLegalRepresentativeHistories().get(i).getSex());
				}
			    
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getStateRepreHistory()!=null){
					legalRepresentativeHistoryAux.setStateRepreHistory(holderRequestDetail.getLegalRepresentativeHistories().get(i).getStateRepreHistory());
				}
				
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getPersonType()!=null){
					legalRepresentativeHistoryAux.setPersonType(holderRequestDetail.getLegalRepresentativeHistories().get(i).getPersonType());
				}
				
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getRepresentativeClass()!=null){
					legalRepresentativeHistoryAux.setRepresentativeClass(holderRequestDetail.getLegalRepresentativeHistories().get(i).getRepresentativeClass());
				}
				
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getSecondDocumentType()!=null){
					legalRepresentativeHistoryAux.setSecondDocumentType(holderRequestDetail.getLegalRepresentativeHistories().get(i).getSecondDocumentType());
				}
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getDocumentSource()!=null){
					legalRepresentativeHistoryAux.setDocumentSource(holderRequestDetail.getLegalRepresentativeHistories().get(i).getDocumentSource());
				}
				if(holderRequestDetail.getLegalRepresentativeHistories().get(i).getDocumentIssuanceDate()!=null){
					legalRepresentativeHistoryAux.setDocumentIssuanceDate(holderRequestDetail.getLegalRepresentativeHistories().get(i).getDocumentIssuanceDate());
				}
				
				if(Validations.validateIsNotNullAndNotEmpty(holder))
					holderHistory.setFundType(holder.getFundType());
				
				legalRepresentativeHistoryAux.setRepresentativeTypeDescription(PersonType.get(holderRequestDetail.getLegalRepresentativeHistories().get(i).getPersonType()).getValue());
			
				legalRepresentativeHistoryAux.setDocumentTypeDescription(DocumentType.get(holderRequestDetail.getLegalRepresentativeHistories().get(i).getDocumentType()).getValue());
			
				legalRepresentativeHistoryAux.setRepresenteFileHistory(holderRequestDetail.getLegalRepresentativeHistories().get(i).getRepresenteFileHistory());

				legalRepresentativeHistoryAux.setRepreHistoryType(holderRequestDetail.getLegalRepresentativeHistories().get(i).getRepreHistoryType());
				
				lstLegalRepresentativeHistoryDetail.add(legalRepresentativeHistoryAux);		
				
			}
			
			legalRepresentativeHelperBean.setLstLegalRepresentativeHistoryDetail(lstLegalRepresentativeHistoryDetail);
			
		}
		
		getBeforeModification();
		
//		if (!holderHistory.getSex().equals(holderHistoryDetail.getSex())){
//			holderHistoryDetail.setColorModification(true);
//		}
		
		
		
		}
		catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		
	}
	
	/**
	 * Checks if is not state confirm.
	 *
	 * @param state the state
	 * @return the boolean
	 */
	public Boolean isNotStateConfirm(String state){
		if(state.equalsIgnoreCase(HolderRequestStateType.CONFIRMED.getValue())){
			return false;
		}
		return true;
	}	
	
	/**
	 * Gets the generate file holder modify report.
	 *
	 * @param holderRequest the holder request
	 * @param nameParam the name param
	 * @return the generate file holder modify report
	 * @throws ServiceException the service exception
	 */
	public void getGenerateFileHolderModifyReport(HolderRequest holderRequest, String nameParam) throws ServiceException{
		Long holderRequestId = holderRequest.getIdHolderRequestPk();	
		this.serviceAPIClients.generateReportFileAccountModification(holderRequestId, nameParam);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		this.searchRequestHolders(); 
	} 
	
	/**
	 * Gets the visible file holder modify report.
	 *
	 * @param holderRequest the holder request
	 * @return the visible file holder modify report
	 */
	public Boolean getVisibleFileHolderModifyReport(HolderRequest holderRequest){
		String nameReport = "CVPRPT119";
		Long holderRequestId = holderRequest.getIdHolderRequestPk();	
		ReportLoggerFile reportLoggerFile = holderServiceFacade.findModifyReport(holderRequestId,nameReport);
		Boolean bol = false;
			if(Validations.validateIsNotNull(reportLoggerFile)){
				bol = true;
			}
		return bol;
	}
	
	/**
	 * Gets the streamed content temporal file holder modify report.
	 *
	 * @param holderRequest the holder request
	 * @return the streamed content temporal file holder modify report
	 * @throws ServiceException the service exception
	 */
	public StreamedContent getStreamedContentTemporalFileHolderModifyReport(HolderRequest holderRequest)throws ServiceException{
		String nameReport = "CVPRPT119";
		Long holderRequestId = holderRequest.getIdHolderRequestPk();	
		String path = holderServiceFacade.findFileModifyReportPath(nameReport+holderRequestId+".pdf");
		if(path != null) {
			Path pathFile = Paths.get(path,nameReport+holderRequestId+".pdf");
			if(Files.exists(pathFile)) {
				try {
					Faces.sendFile(Files.readAllBytes(pathFile), nameReport+holderRequestId+".pdf", true);					
				} catch(IOException iox) {
//					log.error(iox.getMessage());
				}
			}
		}
		return null;
	}

	
	/**
	 * Event document source.
	 */
	public void eventDocumentSource(){
		holderHistory.setDocumentNumber(null);
		newDocumentNumber = null;
	}
	/**
	 * Removes the holder request file.
	 *
	 * @param holderRequestFile the holder request file
	 */
	public void removeHolderRequestFile(HolderReqFileHistory holderRequestFile) {
		lstHolderReqFileHistory.remove(holderRequestFile);
		holderFileNameDisplay = null;
		attachFileHolderSelected = null;		
	}
	
    public void removeHolderChangesMotiveRequestFile(HolderReqFileHistory holderRequestFile) {
        lstHolderReqChangesFileHistory.remove(holderRequestFile);
        holderFileChangesNameDisplay = null;
        attachFileHolderChangesMotivesSelected = null;                
    }	
	
	public boolean changedData(Object oldValue, Object newValue){
		if(Validations.validateIsNull(oldValue) && 
				Validations.validateIsNull(newValue)){
			return false;
		}
		if(Validations.validateIsNotNull(oldValue)){
			if(oldValue.equals(newValue)){
				return false;
			}
		}
		if(Validations.validateIsNotNull(newValue)){
			if(newValue.equals(oldValue)){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Gets the holder creation history.
	 *
	 * @return the holder creation history
	 */
	public HolderHistory getHolderCreationHistory() {
		return holderHistory;
	}

	/**
	 * Sets the holder creation history.
	 *
	 * @param holderCreationHistory the new holder creation history
	 */
	public void setHolderCreationHistory(HolderHistory holderCreationHistory) {
		this.holderHistory = holderCreationHistory;
	}

	/**
	 * Checks if is flag minor.
	 *
	 * @return true, if is flag minor
	 */
	public boolean isFlagMinor() {
		return flagMinor;
	}

	/**
	 * Sets the flag minor.
	 *
	 * @param flagMinor the new flag minor
	 */
	public void setFlagMinor(boolean flagMinor) {
		this.flagMinor = flagMinor;
	}

	/**
	 * Checks if is flag is pep.
	 *
	 * @return true, if is flag is pep
	 */
	public boolean isFlagIsPEP() {
		return flagIsPEP;
	}

	/**
	 * Sets the flag is pep.
	 *
	 * @param flagIsPEP the new flag is pep
	 */
	public void setFlagIsPEP(boolean flagIsPEP) {
		this.flagIsPEP = flagIsPEP;
	}

	/**
	 * Checks if is flag copy legal holder.
	 *
	 * @return true, if is flag copy legal holder
	 */
	public boolean isFlagCopyLegalHolder() {
		return flagCopyLegalHolder;
	}

	/**
	 * Sets the flag copy legal holder.
	 *
	 * @param flagCopyLegalHolder the new flag copy legal holder
	 */
	public void setFlagCopyLegalHolder(boolean flagCopyLegalHolder) {
		this.flagCopyLegalHolder = flagCopyLegalHolder;
	}

	/**
	 * Checks if is flag holder natural person.
	 *
	 * @return true, if is flag holder natural person
	 */
	public boolean isFlagHolderNaturalPerson() {
		return flagHolderNaturalPerson;
	}

	/**
	 * Sets the flag holder natural person.
	 *
	 * @param flagHolderNaturalPerson the new flag holder natural person
	 */
	public void setFlagHolderNaturalPerson(boolean flagHolderNaturalPerson) {
		this.flagHolderNaturalPerson = flagHolderNaturalPerson;
	}

	/**
	 * Checks if is flag holder juridic person.
	 *
	 * @return true, if is flag holder juridic person
	 */
	public boolean isFlagHolderJuridicPerson() {
		return flagHolderJuridicPerson;
	}

	/**
	 * Sets the flag holder juridic person.
	 *
	 * @param flagHolderJuridicPerson the new flag holder juridic person
	 */
	public void setFlagHolderJuridicPerson(boolean flagHolderJuridicPerson) {
		this.flagHolderJuridicPerson = flagHolderJuridicPerson;
	}

	
	/**
	 * Checks if is disabled country resident holder.
	 *
	 * @return true, if is disabled country resident holder
	 */
	public boolean isDisabledCountryResidentHolder() {
		return disabledCountryResidentHolder;
	}

	/**
	 * Sets the disabled country resident holder.
	 *
	 * @param disabledCountryResidentHolder the new disabled country resident holder
	 */
	public void setDisabledCountryResidentHolder(
			boolean disabledCountryResidentHolder) {
		this.disabledCountryResidentHolder = disabledCountryResidentHolder;
	}

	/**
	 * Checks if is disabled country resident representative.
	 *
	 * @return true, if is disabled country resident representative
	 */
	public boolean isDisabledCountryResidentRepresentative() {
		return disabledCountryResidentRepresentative;
	}

	/**
	 * Sets the disabled country resident representative.
	 *
	 * @param disabledCountryResidentRepresentative the new disabled country resident representative
	 */
	public void setDisabledCountryResidentRepresentative(
			boolean disabledCountryResidentRepresentative) {
		this.disabledCountryResidentRepresentative = disabledCountryResidentRepresentative;
	}

	/**
	 * Gets the legal representative array.
	 *
	 * @return the legal representative array
	 */
	public LegalRepresentative[] getLegalRepresentativeArray() {
		return legalRepresentativeArray;
	}

	/**
	 * Sets the legal representative array.
	 *
	 * @param legalRepresentativeArray the new legal representative array
	 */
	public void setLegalRepresentativeArray(
			LegalRepresentative[] legalRepresentativeArray) {
		this.legalRepresentativeArray = legalRepresentativeArray;
	}

	/**
	 * Gets the list juridic class.
	 *
	 * @return the list juridic class
	 */
	public List<ParameterTable> getListJuridicClass() {
		return listJuridicClass;
	}

	/**
	 * Sets the list juridic class.
	 *
	 * @param listJuridicClass the new list juridic class
	 */
	public void setListJuridicClass(List<ParameterTable> listJuridicClass) {
		this.listJuridicClass = listJuridicClass;
	}

	/**
	 * Gets the list person type.
	 *
	 * @return the list person type
	 */
	public List<ParameterTable> getListPersonType() {
		return listPersonType;
	}

	/**
	 * Sets the list person type.
	 *
	 * @param listPersonType the new list person type
	 */
	public void setListPersonType(List<ParameterTable> listPersonType) {
		this.listPersonType = listPersonType;
	}

	/**
	 * Gets the list boolean.
	 *
	 * @return the list boolean
	 */
	public List<BooleanType> getListBoolean() {
		return listBoolean;
	}

	/**
	 * Sets the list boolean.
	 *
	 * @param listBoolean the new list boolean
	 */
	public void setListBoolean(List<BooleanType> listBoolean) {
		this.listBoolean = listBoolean;
	}

	/**
	 * Gets the list document type.
	 *
	 * @return the list document type
	 */
	public List<ParameterTable> getListDocumentType() {
		return listDocumentType;
	}

	/**
	 * Sets the list document type.
	 *
	 * @param listDocumentType the new list document type
	 */
	public void setListDocumentType(List<ParameterTable> listDocumentType) {
		this.listDocumentType = listDocumentType;
	}

	/**
	 * Gets the list sex.
	 *
	 * @return the list sex
	 */
	public List<ParameterTable> getListSex() {
		return listSex;
	}

	/**
	 * Sets the list sex.
	 *
	 * @param listSex the new list sex
	 */
	public void setListSex(List<ParameterTable> listSex) {
		this.listSex = listSex;
	}

	/**
	 * Gets the pep person.
	 *
	 * @return the pep person
	 */
	public PepPerson getPepPerson() {
		return pepPerson;
	}

	/**
	 * Sets the pep person.
	 *
	 * @param pepPerson the new pep person
	 */
	public void setPepPerson(PepPerson pepPerson) {
		this.pepPerson = pepPerson;
	}

	/**
	 * Gets the list geographic location.
	 *
	 * @return the list geographic location
	 */
	public List<GeographicLocation> getListGeographicLocation() {
		return listGeographicLocation;
	}

	/**
	 * Sets the list geographic location.
	 *
	 * @param listGeographicLocation the new list geographic location
	 */
	public void setListGeographicLocation(
			List<GeographicLocation> listGeographicLocation) {
		this.listGeographicLocation = listGeographicLocation;
	}

	/**
	 * Checks if is disabled representative button.
	 *
	 * @return true, if is disabled representative button
	 */
	public boolean isDisabledRepresentativeButton() {
		return disabledRepresentativeButton;
	}

	/**
	 * Sets the disabled representative button.
	 *
	 * @param disabledRepresentativeButton the new disabled representative button
	 */
	public void setDisabledRepresentativeButton(
			boolean disabledRepresentativeButton) {
		this.disabledRepresentativeButton = disabledRepresentativeButton;
	}

	/**
	 * Gets the list province location.
	 *
	 * @return the list province location
	 */
	public List<GeographicLocation> getListProvinceLocation() {
		return listProvinceLocation;
	}

	/**
	 * Sets the list province location.
	 *
	 * @param listProvinceLocation the new list province location
	 */
	public void setListProvinceLocation(
			List<GeographicLocation> listProvinceLocation) {
		this.listProvinceLocation = listProvinceLocation;
	}

	/**
	 * Gets the list province postal location.
	 *
	 * @return the list province postal location
	 */
	public List<GeographicLocation> getListProvincePostalLocation() {
		return listProvincePostalLocation;
	}

	/**
	 * Sets the list province postal location.
	 *
	 * @param listProvincePostalLocation the new list province postal location
	 */
	public void setListProvincePostalLocation(
			List<GeographicLocation> listProvincePostalLocation) {
		this.listProvincePostalLocation = listProvincePostalLocation;
	}

	/**
	 * Checks if is disabled cmb postal country.
	 *
	 * @return true, if is disabled cmb postal country
	 */
	public boolean isDisabledCmbPostalCountry() {
		return disabledCmbPostalCountry;
	}

	/**
	 * Sets the disabled cmb postal country.
	 *
	 * @param disabledCmbPostalCountry the new disabled cmb postal country
	 */
	public void setDisabledCmbPostalCountry(boolean disabledCmbPostalCountry) {
		this.disabledCmbPostalCountry = disabledCmbPostalCountry;
	}

	/**
	 * Checks if is disabled cmb postal province.
	 *
	 * @return true, if is disabled cmb postal province
	 */
	public boolean isDisabledCmbPostalProvince() {
		return disabledCmbPostalProvince;
	}

	/**
	 * Sets the disabled cmb postal province.
	 *
	 * @param disabledCmbPostalProvince the new disabled cmb postal province
	 */
	public void setDisabledCmbPostalProvince(boolean disabledCmbPostalProvince) {
		this.disabledCmbPostalProvince = disabledCmbPostalProvince;
	}

	/**
	 * Checks if is disabled cmb postal sector.
	 *
	 * @return true, if is disabled cmb postal sector
	 */
	public boolean isDisabledCmbPostalSector() {
		return disabledCmbPostalSector;
	}

	/**
	 * Sets the disabled cmb postal sector.
	 *
	 * @param disabledCmbPostalSector the new disabled cmb postal sector
	 */
	public void setDisabledCmbPostalSector(boolean disabledCmbPostalSector) {
		this.disabledCmbPostalSector = disabledCmbPostalSector;
	}

	/**
	 * Checks if is disabled input postal address.
	 *
	 * @return true, if is disabled input postal address
	 */
	public boolean isDisabledInputPostalAddress() {
		return disabledInputPostalAddress;
	}

	/**
	 * Sets the disabled input postal address.
	 *
	 * @param disabledInputPostalAddress the new disabled input postal address
	 */
	public void setDisabledInputPostalAddress(boolean disabledInputPostalAddress) {
		this.disabledInputPostalAddress = disabledInputPostalAddress;
	}

	/**
	 * Gets the list representative class.
	 *
	 * @return the list representative class
	 */
	public List<ParameterTable> getListRepresentativeClass() {
		return listRepresentativeClass;
	}

	/**
	 * Sets the list representative class.
	 *
	 * @param listRepresentativeClass the new list representative class
	 */
	public void setListRepresentativeClass(
			List<ParameterTable> listRepresentativeClass) {
		this.listRepresentativeClass = listRepresentativeClass;
	}

	/**
	 * Gets the list province location representative.
	 *
	 * @return the list province location representative
	 */
	public List<GeographicLocation> getListProvinceLocationRepresentative() {
		return listProvinceLocationRepresentative;
	}

	/**
	 * Sets the list province location representative.
	 *
	 * @param listProvinceLocationRepresentative the new list province location representative
	 */
	public void setListProvinceLocationRepresentative(
			List<GeographicLocation> listProvinceLocationRepresentative) {
		this.listProvinceLocationRepresentative = listProvinceLocationRepresentative;
	}

	/**
	 * Gets the list province postal location representative.
	 *
	 * @return the list province postal location representative
	 */
	public List<GeographicLocation> getListProvincePostalLocationRepresentative() {
		return listProvincePostalLocationRepresentative;
	}

	/**
	 * Sets the list province postal location representative.
	 *
	 * @param listProvincePostalLocationRepresentative the new list province postal location representative
	 */
	public void setListProvincePostalLocationRepresentative(
			List<GeographicLocation> listProvincePostalLocationRepresentative) {
		this.listProvincePostalLocationRepresentative = listProvincePostalLocationRepresentative;
	}

	
	/**
	 * Checks if is disabled cmb postal country representative.
	 *
	 * @return true, if is disabled cmb postal country representative
	 */
	public boolean isDisabledCmbPostalCountryRepresentative() {
		return disabledCmbPostalCountryRepresentative;
	}

	/**
	 * Sets the disabled cmb postal country representative.
	 *
	 * @param disabledCmbPostalCountryRepresentative the new disabled cmb postal country representative
	 */
	public void setDisabledCmbPostalCountryRepresentative(
			boolean disabledCmbPostalCountryRepresentative) {
		this.disabledCmbPostalCountryRepresentative = disabledCmbPostalCountryRepresentative;
	}

	/**
	 * Checks if is disabled cmb postal province representative.
	 *
	 * @return true, if is disabled cmb postal province representative
	 */
	public boolean isDisabledCmbPostalProvinceRepresentative() {
		return disabledCmbPostalProvinceRepresentative;
	}

	/**
	 * Sets the disabled cmb postal province representative.
	 *
	 * @param disabledCmbPostalProvinceRepresentative the new disabled cmb postal province representative
	 */
	public void setDisabledCmbPostalProvinceRepresentative(
			boolean disabledCmbPostalProvinceRepresentative) {
		this.disabledCmbPostalProvinceRepresentative = disabledCmbPostalProvinceRepresentative;
	}

	/**
	 * Checks if is disabled cmb postal sector representative.
	 *
	 * @return true, if is disabled cmb postal sector representative
	 */
	public boolean isDisabledCmbPostalSectorRepresentative() {
		return disabledCmbPostalSectorRepresentative;
	}

	/**
	 * Sets the disabled cmb postal sector representative.
	 *
	 * @param disabledCmbPostalSectorRepresentative the new disabled cmb postal sector representative
	 */
	public void setDisabledCmbPostalSectorRepresentative(
			boolean disabledCmbPostalSectorRepresentative) {
		this.disabledCmbPostalSectorRepresentative = disabledCmbPostalSectorRepresentative;
	}

	/**
	 * Checks if is disabled input postal address representative.
	 *
	 * @return true, if is disabled input postal address representative
	 */
	public boolean isDisabledInputPostalAddressRepresentative() {
		return disabledInputPostalAddressRepresentative;
	}

	/**
	 * Sets the disabled input postal address representative.
	 *
	 * @param disabledInputPostalAddressRepresentative the new disabled input postal address representative
	 */
	public void setDisabledInputPostalAddressRepresentative(
			boolean disabledInputPostalAddressRepresentative) {
		this.disabledInputPostalAddressRepresentative = disabledInputPostalAddressRepresentative;
	}

	/**
	 * Gets the holder history.
	 *
	 * @return the holder history
	 */
	public HolderHistory getHolderHistory() {
		return holderHistory;
	}

	/**
	 * Sets the holder history.
	 *
	 * @param holderHistory the new holder history
	 */
	public void setHolderHistory(HolderHistory holderHistory) {
		this.holderHistory = holderHistory;
	}

	/**
	 * Gets the lst parameter archive header.
	 *
	 * @return the lst parameter archive header
	 */
	public List<ParameterTable> getLstParameterArchiveHeader() {
		return lstParameterArchiveHeader;
	}

	/**
	 * Sets the lst parameter archive header.
	 *
	 * @param lstParameterArchiveHeader the new lst parameter archive header
	 */
	public void setLstParameterArchiveHeader(
			List<ParameterTable> lstParameterArchiveHeader) {
		this.lstParameterArchiveHeader = lstParameterArchiveHeader;
	}

	/**
	 * Gets the lst holder req file history.
	 *
	 * @return the lst holder req file history
	 */
	public List<HolderReqFileHistory> getLstHolderReqFileHistory() {
		return lstHolderReqFileHistory;
	}

	/**
	 * Sets the lst holder req file history.
	 *
	 * @param lstHolderReqFileHistory the new lst holder req file history
	 */
	public void setLstHolderReqFileHistory(
			List<HolderReqFileHistory> lstHolderReqFileHistory) {
		this.lstHolderReqFileHistory = lstHolderReqFileHistory;
	}

	/**
	 * Gets the lst representative file history.
	 *
	 * @return the lst representative file history
	 */
	public List<RepresentativeFileHistory> getLstRepresentativeFileHistory() {
		return lstRepresentativeFileHistory;
	}

	/**
	 * Sets the lst representative file history.
	 *
	 * @param lstRepresentativeFileHistory the new lst representative file history
	 */
	public void setLstRepresentativeFileHistory(
			List<RepresentativeFileHistory> lstRepresentativeFileHistory) {
		this.lstRepresentativeFileHistory = lstRepresentativeFileHistory;
	}

	/**
	 * Checks if is flag cmb representative class.
	 *
	 * @return true, if is flag cmb representative class
	 */
	public boolean isFlagCmbRepresentativeClass() {
		return flagCmbRepresentativeClass;
	}

	/**
	 * Sets the flag cmb representative class.
	 *
	 * @param flagCmbRepresentativeClass the new flag cmb representative class
	 */
	public void setFlagCmbRepresentativeClass(boolean flagCmbRepresentativeClass) {
		this.flagCmbRepresentativeClass = flagCmbRepresentativeClass;
	}

	/**
	 * Gets the file.
	 *
	 * @return the file
	 */
	public UploadedFile getFile() {
		return file;
	}

	/**
	 * Sets the file.
	 *
	 * @param file the new file
	 */
	public void setFile(UploadedFile file) {
		this.file = file;
	}


	/**
	 * Gets the legal representative history array.
	 *
	 * @return the legal representative history array
	 */
	public LegalRepresentativeHistory[] getLegalRepresentativeHistoryArray() {
		return legalRepresentativeHistoryArray;
	}

	/**
	 * Sets the legal representative history array.
	 *
	 * @param legalRepresentativeHistoryArray the new legal representative history array
	 */
	public void setLegalRepresentativeHistoryArray(
			LegalRepresentativeHistory[] legalRepresentativeHistoryArray) {
		this.legalRepresentativeHistoryArray = legalRepresentativeHistoryArray;
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * Gets the list participant.
	 *
	 * @return the list participant
	 */
	public List<Participant> getListParticipant() {
		return listParticipant;
	}

	/**
	 * Sets the list participant.
	 *
	 * @param listParticipant the new list participant
	 */
	public void setListParticipant(List<Participant> listParticipant) {
		this.listParticipant = listParticipant;
	}

	/**
	 * Gets the holder request selected.
	 *
	 * @return the holder request selected
	 */
	public HolderRequest getHolderRequestSelected() {
		return holderRequestSelected;
	}

	/**
	 * Sets the holder request selected.
	 *
	 * @param holderRequestSelected the new holder request selected
	 */
	public void setHolderRequestSelected(HolderRequest holderRequestSelected) {
		this.holderRequestSelected = holderRequestSelected;
	}

	/**
	 * Checks if is flag is participant.
	 *
	 * @return true, if is flag is participant
	 */
	public boolean isFlagIsParticipant() {
		return flagIsParticipant;
	}

	/**
	 * Sets the flag is participant.
	 *
	 * @param flagIsParticipant the new flag is participant
	 */
	public void setFlagIsParticipant(boolean flagIsParticipant) {
		this.flagIsParticipant = flagIsParticipant;
	}

	/**
	 * Checks if is flag action confirm.
	 *
	 * @return true, if is flag action confirm
	 */
	public boolean isFlagActionConfirm() {
		return flagActionConfirm;
	}

	/**
	 * Sets the flag action confirm.
	 *
	 * @param flagActionConfirm the new flag action confirm
	 */
	public void setFlagActionConfirm(boolean flagActionConfirm) {
		this.flagActionConfirm = flagActionConfirm;
	}

	/**
	 * Checks if is flag action reject.
	 *
	 * @return true, if is flag action reject
	 */
	public boolean isFlagActionReject() {
		return flagActionReject;
	}

	/**
	 * Sets the flag action reject.
	 *
	 * @param flagActionReject the new flag action reject
	 */
	public void setFlagActionReject(boolean flagActionReject) {
		this.flagActionReject = flagActionReject;
	}

	/**
	 * Checks if is flag action annular.
	 *
	 * @return true, if is flag action annular
	 */
	public boolean isFlagActionAnnular() {
		return flagActionAnnular;
	}

	/**
	 * Sets the flag action annular.
	 *
	 * @param flagActionAnnular the new flag action annular
	 */
	public void setFlagActionAnnular(boolean flagActionAnnular) {
		this.flagActionAnnular = flagActionAnnular;
	}

	/**
	 * Checks if is disabled cmb participant.
	 *
	 * @return true, if is disabled cmb participant
	 */
	public boolean isDisabledCmbParticipant() {
		return disabledCmbParticipant;
	}

	/**
	 * Sets the disabled cmb participant.
	 *
	 * @param disabledCmbParticipant the new disabled cmb participant
	 */
	public void setDisabledCmbParticipant(boolean disabledCmbParticipant) {
		this.disabledCmbParticipant = disabledCmbParticipant;
	}

	/**
	 * Checks if is show motive text.
	 *
	 * @return true, if is show motive text
	 */
	public boolean isShowMotiveText() {
		return showMotiveText;
	}

	/**
	 * Sets the show motive text.
	 *
	 * @param showMotiveText the new show motive text
	 */
	public void setShowMotiveText(boolean showMotiveText) {
		this.showMotiveText = showMotiveText;
	}

	

	/**
	 * Gets the request motive.
	 *
	 * @return the request motive
	 */
	public Integer getRequestMotive() {
		return requestMotive;
	}

	/**
	 * Sets the request motive.
	 *
	 * @param requestMotive the new request motive
	 */
	public void setRequestMotive(Integer requestMotive) {
		this.requestMotive = requestMotive;
	}

	/**
	 * Gets the request other motive.
	 *
	 * @return the request other motive
	 */
	public String getRequestOtherMotive() {
		return requestOtherMotive;
	}

	/**
	 * Sets the request other motive.
	 *
	 * @param requestOtherMotive the new request other motive
	 */
	public void setRequestOtherMotive(String requestOtherMotive) {
		this.requestOtherMotive = requestOtherMotive;
	}

	/**
	 * Gets the list motives.
	 *
	 * @return the list motives
	 */
	public List<ParameterTable> getListMotives() {
		return listMotives;
	}

	/**
	 * Sets the list motives.
	 *
	 * @param listMotives the new list motives
	 */
	public void setListMotives(List<ParameterTable> listMotives) {
		this.listMotives = listMotives;
	}

	/**
	 * Gets the list state holder request.
	 *
	 * @return the list state holder request
	 */
	public List<ParameterTable> getListStateHolderRequest() {
		return listStateHolderRequest;
	}

	/**
	 * Sets the list state holder request.
	 *
	 * @param listStateHolderRequest the new list state holder request
	 */
	public void setListStateHolderRequest(
			List<ParameterTable> listStateHolderRequest) {
		this.listStateHolderRequest = listStateHolderRequest;
	}

	
	
     /**
	 * Checks if is numeric type document holder.
	 *
	 * @return true, if is numeric type document holder
	 */
	public boolean isNumericTypeDocumentHolder() {
		return numericTypeDocumentHolder;
	}

	/**
	 * Sets the numeric type document holder.
	 *
	 * @param numericTypeDocumentHolder the new numeric type document holder
	 */
	public void setNumericTypeDocumentHolder(boolean numericTypeDocumentHolder) {
		this.numericTypeDocumentHolder = numericTypeDocumentHolder;
	}

	/**
	 * Checks if is numeric second type document holder.
	 *
	 * @return true, if is numeric second type document holder
	 */
	public boolean isNumericSecondTypeDocumentHolder() {
		return numericSecondTypeDocumentHolder;
	}

	/**
	 * Sets the numeric second type document holder.
	 *
	 * @param numericSecondTypeDocumentHolder the new numeric second type document holder
	 */
	public void setNumericSecondTypeDocumentHolder(
			boolean numericSecondTypeDocumentHolder) {
		this.numericSecondTypeDocumentHolder = numericSecondTypeDocumentHolder;
	}

	/**
	 * Checks if is numeric type document legal.
	 *
	 * @return true, if is numeric type document legal
	 */
	public boolean isNumericTypeDocumentLegal() {
		return numericTypeDocumentLegal;
	}

	/**
	 * Sets the numeric type document legal.
	 *
	 * @param numericTypeDocumentLegal the new numeric type document legal
	 */
	public void setNumericTypeDocumentLegal(boolean numericTypeDocumentLegal) {
		this.numericTypeDocumentLegal = numericTypeDocumentLegal;
	}

	/**
	 * Checks if is numeric second type document legal.
	 *
	 * @return true, if is numeric second type document legal
	 */
	public boolean isNumericSecondTypeDocumentLegal() {
		return numericSecondTypeDocumentLegal;
	}

	/**
	 * Sets the numeric second type document legal.
	 *
	 * @param numericSecondTypeDocumentLegal the new numeric second type document legal
	 */
	public void setNumericSecondTypeDocumentLegal(
			boolean numericSecondTypeDocumentLegal) {
		this.numericSecondTypeDocumentLegal = numericSecondTypeDocumentLegal;
	}

	/**
	 * Gets the max lenght document number holder.
	 *
	 * @return the max lenght document number holder
	 */
	public Integer getMaxLenghtDocumentNumberHolder() {
		return maxLenghtDocumentNumberHolder;
	}

	/**
	 * Sets the max lenght document number holder.
	 *
	 * @param maxLenghtDocumentNumberHolder the new max lenght document number holder
	 */
	public void setMaxLenghtDocumentNumberHolder(
			Integer maxLenghtDocumentNumberHolder) {
		this.maxLenghtDocumentNumberHolder = maxLenghtDocumentNumberHolder;
	}

	/**
	 * Gets the max lenght second document number holder.
	 *
	 * @return the max lenght second document number holder
	 */
	public Integer getMaxLenghtSecondDocumentNumberHolder() {
		return maxLenghtSecondDocumentNumberHolder;
	}

	/**
	 * Sets the max lenght second document number holder.
	 *
	 * @param maxLenghtSecondDocumentNumberHolder the new max lenght second document number holder
	 */
	public void setMaxLenghtSecondDocumentNumberHolder(
			Integer maxLenghtSecondDocumentNumberHolder) {
		this.maxLenghtSecondDocumentNumberHolder = maxLenghtSecondDocumentNumberHolder;
	}

	/**
	 * Gets the max lenght document number legal.
	 *
	 * @return the max lenght document number legal
	 */
	public Integer getMaxLenghtDocumentNumberLegal() {
		return maxLenghtDocumentNumberLegal;
	}

	/**
	 * Sets the max lenght document number legal.
	 *
	 * @param maxLenghtDocumentNumberLegal the new max lenght document number legal
	 */
	public void setMaxLenghtDocumentNumberLegal(Integer maxLenghtDocumentNumberLegal) {
		this.maxLenghtDocumentNumberLegal = maxLenghtDocumentNumberLegal;
	}

	/**
	 * Gets the max lenght second document number legal.
	 *
	 * @return the max lenght second document number legal
	 */
	public Integer getMaxLenghtSecondDocumentNumberLegal() {
		return maxLenghtSecondDocumentNumberLegal;
	}

	/**
	 * Sets the max lenght second document number legal.
	 *
	 * @param maxLenghtSecondDocumentNumberLegal the new max lenght second document number legal
	 */
	public void setMaxLenghtSecondDocumentNumberLegal(
			Integer maxLenghtSecondDocumentNumberLegal) {
		this.maxLenghtSecondDocumentNumberLegal = maxLenghtSecondDocumentNumberLegal;
	}

	/**
	 * Checks if is flag holder indicator inability.
	 *
	 * @return true, if is flag holder indicator inability
	 */
	public boolean isFlagHolderIndicatorInability() {
		return flagHolderIndicatorInability;
	}

	/**
	 * Sets the flag holder indicator inability.
	 *
	 * @param flagHolderIndicatorInability the new flag holder indicator inability
	 */
	public void setFlagHolderIndicatorInability(boolean flagHolderIndicatorInability) {
		this.flagHolderIndicatorInability = flagHolderIndicatorInability;
	}

	/**
	 * Checks if is flag indicator minor holder.
	 *
	 * @return true, if is flag indicator minor holder
	 */
	public boolean isFlagIndicatorMinorHolder() {
		return flagIndicatorMinorHolder;
	}

	/**
	 * Sets the flag indicator minor holder.
	 *
	 * @param flagIndicatorMinorHolder the new flag indicator minor holder
	 */
	public void setFlagIndicatorMinorHolder(boolean flagIndicatorMinorHolder) {
		this.flagIndicatorMinorHolder = flagIndicatorMinorHolder;
	}

		/**
	 * Checks if is flag foreign not resident.
	 *
	 * @return true, if is flag foreign not resident
	 */
	public boolean isFlagForeignNotResident() {
		return flagForeignNotResident;
	}

	/**
	 * Sets the flag foreign not resident.
	 *
	 * @param flagForeignNotResident the new flag foreign not resident
	 */
	public void setFlagForeignNotResident(boolean flagForeignNotResident) {
		this.flagForeignNotResident = flagForeignNotResident;
	}

	/**
	 * Gets the previous modification to.
	 *
	 * @return the previous modification to
	 */
	public PreviousModificationTO getPreviousModificationTO() {
		return previousModificationTO;
	}

	/**
	 * Sets the previous modification to.
	 *
	 * @param previousModificationTO the new previous modification to
	 */
	public void setPreviousModificationTO(
			PreviousModificationTO previousModificationTO) {
		this.previousModificationTO = previousModificationTO;
	}

	/**
	 * Gets the list document type legal.
	 *
	 * @return the list document type legal
	 */
	public List<ParameterTable> getListDocumentTypeLegal() {
		return listDocumentTypeLegal;
	}

	/**
	 * Sets the list document type legal.
	 *
	 * @param listDocumentTypeLegal the new list document type legal
	 */
	public void setListDocumentTypeLegal(List<ParameterTable> listDocumentTypeLegal) {
		this.listDocumentTypeLegal = listDocumentTypeLegal;
	}

	/**
	 * Gets the list document type result.
	 *
	 * @return the list document type result
	 */
	public List<ParameterTable> getListDocumentTypeResult() {
		return listDocumentTypeResult;
	}

	/**
	 * Sets the list document type result.
	 *
	 * @param listDocumentTypeResult the new list document type result
	 */
	public void setListDocumentTypeResult(
			List<ParameterTable> listDocumentTypeResult) {
		this.listDocumentTypeResult = listDocumentTypeResult;
	}

	/**
	 * Gets the list document type legal result.
	 *
	 * @return the list document type legal result
	 */
	public List<ParameterTable> getListDocumentTypeLegalResult() {
		return listDocumentTypeLegalResult;
	}

	/**
	 * Sets the list document type legal result.
	 *
	 * @param listDocumentTypeLegalResult the new list document type legal result
	 */
	public void setListDocumentTypeLegalResult(
			List<ParameterTable> listDocumentTypeLegalResult) {
		this.listDocumentTypeLegalResult = listDocumentTypeLegalResult;
	}

	/**
	 * Gets the holder request detail.
	 *
	 * @return the holder request detail
	 */
	public HolderRequest getHolderRequestDetail() {
		return holderRequestDetail;
	}

	/**
	 * Sets the holder request detail.
	 *
	 * @param holderRequestDetail the new holder request detail
	 */
	public void setHolderRequestDetail(HolderRequest holderRequestDetail) {
		this.holderRequestDetail = holderRequestDetail;
	}

	/**
	 * Gets the holder history detail.
	 *
	 * @return the holder history detail
	 */
	public HolderHistory getHolderHistoryDetail() {
		return holderHistoryDetail;
	}

	/**
	 * Sets the holder history detail.
	 *
	 * @param holderHistoryDetail the new holder history detail
	 */
	public void setHolderHistoryDetail(HolderHistory holderHistoryDetail) {
		this.holderHistoryDetail = holderHistoryDetail;
	}

	/**
	 * Gets the participant detail.
	 *
	 * @return the participant detail
	 */
	public Participant getParticipantDetail() {
		return participantDetail;
	}

	/**
	 * Sets the participant detail.
	 *
	 * @param participantDetail the new participant detail
	 */
	public void setParticipantDetail(Participant participantDetail) {
		this.participantDetail = participantDetail;
	}

	/**
	 * Gets the holder request detail before.
	 *
	 * @return the holder request detail before
	 */
	public HolderRequest getHolderRequestDetailBefore() {
		return holderRequestDetailBefore;
	}

	/**
	 * Sets the holder request detail before.
	 *
	 * @param holderRequestDetailBefore the new holder request detail before
	 */
	public void setHolderRequestDetailBefore(HolderRequest holderRequestDetailBefore) {
		this.holderRequestDetailBefore = holderRequestDetailBefore;
	}

	/**
	 * Gets the holder history detail before.
	 *
	 * @return the holder history detail before
	 */
	public HolderHistory getHolderHistoryDetailBefore() {
		return holderHistoryDetailBefore;
	}

	/**
	 * Sets the holder history detail before.
	 *
	 * @param holderHistoryDetailBefore the new holder history detail before
	 */
	public void setHolderHistoryDetailBefore(HolderHistory holderHistoryDetailBefore) {
		this.holderHistoryDetailBefore = holderHistoryDetailBefore;
	}

	/**
	 * Gets the participant detail before.
	 *
	 * @return the participant detail before
	 */
	public Participant getParticipantDetailBefore() {
		return participantDetailBefore;
	}

	/**
	 * Sets the participant detail before.
	 *
	 * @param participantDetailBefore the new participant detail before
	 */
	public void setParticipantDetailBefore(Participant participantDetailBefore) {
		this.participantDetailBefore = participantDetailBefore;
	}

	/**
	 * Gets the holder history before.
	 *
	 * @return the holder history before
	 */
	public HolderHistory getHolderHistoryBefore() {
		return holderHistoryBefore;
	}

	/**
	 * Sets the holder history before.
	 *
	 * @param holderHistoryBefore the new holder history before
	 */
	public void setHolderHistoryBefore(HolderHistory holderHistoryBefore) {
		this.holderHistoryBefore = holderHistoryBefore;
	}

	/**
	 * Gets the list province location before.
	 *
	 * @return the list province location before
	 */
	public List<GeographicLocation> getListProvinceLocationBefore() {
		return listProvinceLocationBefore;
	}

	/**
	 * Sets the list province location before.
	 *
	 * @param listProvinceLocationBefore the new list province location before
	 */
	public void setListProvinceLocationBefore(
			List<GeographicLocation> listProvinceLocationBefore) {
		this.listProvinceLocationBefore = listProvinceLocationBefore;
	}

	
	/**
	 * Gets the list province postal location before.
	 *
	 * @return the list province postal location before
	 */
	public List<GeographicLocation> getListProvincePostalLocationBefore() {
		return listProvincePostalLocationBefore;
	}

	/**
	 * Sets the list province postal location before.
	 *
	 * @param listProvincePostalLocationBefore the new list province postal location before
	 */
	public void setListProvincePostalLocationBefore(
			List<GeographicLocation> listProvincePostalLocationBefore) {
		this.listProvincePostalLocationBefore = listProvincePostalLocationBefore;
	}

	/**
	 * Gets the list category pep.
	 *
	 * @return the list category pep
	 */
	public List<ParameterTable> getListCategoryPep() {
		return listCategoryPep;
	}

	/**
	 * Sets the list category pep.
	 *
	 * @param listCategoryPep the new list category pep
	 */
	public void setListCategoryPep(List<ParameterTable> listCategoryPep) {
		this.listCategoryPep = listCategoryPep;
	}

	/**
	 * Gets the list role pep.
	 *
	 * @return the list role pep
	 */
	public List<ParameterTable> getListRolePep() {
		return listRolePep;
	}

	/**
	 * Sets the list role pep.
	 *
	 * @param listRolePep the new list role pep
	 */
	public void setListRolePep(List<ParameterTable> listRolePep) {
		this.listRolePep = listRolePep;
	}

	/**
	 * Checks if is btn modify view.
	 *
	 * @return true, if is btn modify view
	 */
	public boolean isBtnModifyView() {
		return btnModifyView;
	}

	/**
	 * Sets the btn modify view.
	 *
	 * @param btnModifyView the new btn modify view
	 */
	public void setBtnModifyView(boolean btnModifyView) {
		this.btnModifyView = btnModifyView;
	}

	/**
	 * Checks if is btn confirm view.
	 *
	 * @return true, if is btn confirm view
	 */
	public boolean isBtnConfirmView() {
		return btnConfirmView;
	}

	/**
	 * Sets the btn confirm view.
	 *
	 * @param btnConfirmView the new btn confirm view
	 */
	public void setBtnConfirmView(boolean btnConfirmView) {
		this.btnConfirmView = btnConfirmView;
	}

	/**
	 * Checks if is btn reject view.
	 *
	 * @return true, if is btn reject view
	 */
	public boolean isBtnRejectView() {
		return btnRejectView;
	}

	/**
	 * Sets the btn reject view.
	 *
	 * @param btnRejectView the new btn reject view
	 */
	public void setBtnRejectView(boolean btnRejectView) {
		this.btnRejectView = btnRejectView;
	}

	/**
	 * Checks if is btn approve view.
	 *
	 * @return true, if is btn approve view
	 */
	public boolean isBtnApproveView() {
		return btnApproveView;
	}

	/**
	 * Sets the btn approve view.
	 *
	 * @param btnApproveView the new btn approve view
	 */
	public void setBtnApproveView(boolean btnApproveView) {
		this.btnApproveView = btnApproveView;
	}

	/**
	 * Checks if is btn annular view.
	 *
	 * @return true, if is btn annular view
	 */
	public boolean isBtnAnnularView() {
		return btnAnnularView;
	}

	/**
	 * Sets the btn annular view.
	 *
	 * @param btnAnnularView the new btn annular view
	 */
	public void setBtnAnnularView(boolean btnAnnularView) {
		this.btnAnnularView = btnAnnularView;
	}

	/**
	 * Gets the lst holder file.
	 *
	 * @return the lst holder file
	 */
	public List<HolderFile> getLstHolderFile() {
		return lstHolderFile;
	}

	/**
	 * Sets the lst holder file.
	 *
	 * @param lstHolderFile the new lst holder file
	 */
	public void setLstHolderFile(List<HolderFile> lstHolderFile) {
		this.lstHolderFile = lstHolderFile;
	}

	/**
	 * Checks if is flag action approve.
	 *
	 * @return true, if is flag action approve
	 */
	public boolean isFlagActionApprove() {
		return flagActionApprove;
	}

	/**
	 * Sets the flag action approve.
	 *
	 * @param flagActionApprove the new flag action approve
	 */
	public void setFlagActionApprove(boolean flagActionApprove) {
		this.flagActionApprove = flagActionApprove;
	}

	/**
	 * Gets the list economic sector.
	 *
	 * @return the list economic sector
	 */
	public List<ParameterTable> getListEconomicSector() {
		return listEconomicSector;
	}

	/**
	 * Sets the list economic sector.
	 *
	 * @param listEconomicSector the new list economic sector
	 */
	public void setListEconomicSector(List<ParameterTable> listEconomicSector) {
		this.listEconomicSector = listEconomicSector;
	}

	/**
	 * Gets the list economic activity.
	 *
	 * @return the list economic activity
	 */
	public List<ParameterTable> getListEconomicActivity() {
		return listEconomicActivity;
	}

	/**
	 * Sets the list economic activity.
	 *
	 * @param listEconomicActivity the new list economic activity
	 */
	public void setListEconomicActivity(List<ParameterTable> listEconomicActivity) {
		this.listEconomicActivity = listEconomicActivity;
	}

	/**
	 * Gets the list second document type.
	 *
	 * @return the list second document type
	 */
	public List<ParameterTable> getListSecondDocumentType() {
		return listSecondDocumentType;
	}

	/**
	 * Sets the list second document type.
	 *
	 * @param listSecondDocumentType the new list second document type
	 */
	public void setListSecondDocumentType(
			List<ParameterTable> listSecondDocumentType) {
		this.listSecondDocumentType = listSecondDocumentType;
	}

	/**
	 * Gets the rnc result find.
	 *
	 * @return the rnc result find
	 */
	public InstitutionInformation getNitResultFind() {
		return nitResultFind;
	}

	/**
	 * Sets the rnc result find.
	 *
	 * @param nitResultFind the new rnc result find
	 */
	public void setNitResultFind(InstitutionInformation nitResultFind) {
		this.nitResultFind = nitResultFind;
	}

	/**
	 * Checks if is disabled ind resident.
	 *
	 * @return true, if is disabled ind resident
	 */
	public boolean isDisabledIndResident() {
		return disabledIndResident;
	}

	/**
	 * Sets the disabled ind resident.
	 *
	 * @param disabledIndResident the new disabled ind resident
	 */
	public void setDisabledIndResident(boolean disabledIndResident) {
		this.disabledIndResident = disabledIndResident;
	}

	/**
	 * Checks if is disabled nationality.
	 *
	 * @return true, if is disabled nationality
	 */
	public boolean isDisabledNationality() {
		return disabledNationality;
	}

	/**
	 * Sets the disabled nationality.
	 *
	 * @param disabledNationality the new disabled nationality
	 */
	public void setDisabledNationality(boolean disabledNationality) {
		this.disabledNationality = disabledNationality;
	}

	/**
	 * Gets the list holder document type.
	 *
	 * @return the list holder document type
	 */
	public List<ParameterTable> getListHolderDocumentType() {
		return listHolderDocumentType;
	}

	/**
	 * Sets the list holder document type.
	 *
	 * @param listHolderDocumentType the new list holder document type
	 */
	public void setListHolderDocumentType(
			List<ParameterTable> listHolderDocumentType) {
		this.listHolderDocumentType = listHolderDocumentType;
	}

	/**
	 * Gets the list representative document type.
	 *
	 * @return the list representative document type
	 */
	public List<ParameterTable> getListRepresentativeDocumentType() {
		return listRepresentativeDocumentType;
	}

	/**
	 * Sets the list representative document type.
	 *
	 * @param listRepresentativeDocumentType the new list representative document type
	 */
	public void setListRepresentativeDocumentType(
			List<ParameterTable> listRepresentativeDocumentType) {
		this.listRepresentativeDocumentType = listRepresentativeDocumentType;
	}

	/**
	 * Gets the attach file holder selected.
	 *
	 * @return the attach file holder selected
	 */
	public Integer getAttachFileHolderSelected() {
		return attachFileHolderSelected;
	}

	/**
	 * Sets the attach file holder selected.
	 *
	 * @param attachFileHolderSelected the new attach file holder selected
	 */
	public void setAttachFileHolderSelected(Integer attachFileHolderSelected) {
		this.attachFileHolderSelected = attachFileHolderSelected;
	}

	/**
	 * Gets the attach file representative selected.
	 *
	 * @return the attach file representative selected
	 */
	public Integer getAttachFileRepresentativeSelected() {
		return attachFileRepresentativeSelected;
	}

	/**
	 * Sets the attach file representative selected.
	 *
	 * @param attachFileRepresentativeSelected the new attach file representative selected
	 */
	public void setAttachFileRepresentativeSelected(
			Integer attachFileRepresentativeSelected) {
		this.attachFileRepresentativeSelected = attachFileRepresentativeSelected;
	}

	/**
	 * Gets the holder file name display.
	 *
	 * @return the holder file name display
	 */
	public String getHolderFileNameDisplay() {
		return holderFileNameDisplay;
	}

	/**
	 * Sets the holder file name display.
	 *
	 * @param holderFileNameDisplay the new holder file name display
	 */
	public void setHolderFileNameDisplay(String holderFileNameDisplay) {
		this.holderFileNameDisplay = holderFileNameDisplay;
	}

		
	/**
	 * Gets the legal representative history detail.
	 *
	 * @return the legal representative history detail
	 */
	public LegalRepresentativeHistory getLegalRepresentativeHistoryDetail() {
		return legalRepresentativeHistoryDetail;
	}

	/**
	 * Sets the legal representative history detail.
	 *
	 * @param legalRepresentativeHistoryDetail the new legal representative history detail
	 */
	public void setLegalRepresentativeHistoryDetail(
			LegalRepresentativeHistory legalRepresentativeHistoryDetail) {
		this.legalRepresentativeHistoryDetail = legalRepresentativeHistoryDetail;
	}

	
	/**
	 * Checks if is flag export data holder.
	 *
	 * @return true, if is flag export data holder
	 */
	public boolean isFlagExportDataHolder() {
		return flagExportDataHolder;
	}

	/**
	 * Sets the flag export data holder.
	 *
	 * @param flagExportDataHolder the new flag export data holder
	 */
	public void setFlagExportDataHolder(boolean flagExportDataHolder) {
		this.flagExportDataHolder = flagExportDataHolder;
	}

	/**
	 * Checks if is flag export data representative.
	 *
	 * @return true, if is flag export data representative
	 */
	public boolean isFlagExportDataRepresentative() {
		return flagExportDataRepresentative;
	}

	/**
	 * Sets the flag export data representative.
	 *
	 * @param flagExportDataRepresentative the new flag export data representative
	 */
	public void setFlagExportDataRepresentative(boolean flagExportDataRepresentative) {
		this.flagExportDataRepresentative = flagExportDataRepresentative;
	}

	/**
	 * Gets the list second document type legal.
	 *
	 * @return the list second document type legal
	 */
	public List<ParameterTable> getListSecondDocumentTypeLegal() {
		return listSecondDocumentTypeLegal;
	}

	/**
	 * Sets the list second document type legal.
	 *
	 * @param listSecondDocumentTypeLegal the new list second document type legal
	 */
	public void setListSecondDocumentTypeLegal(
			List<ParameterTable> listSecondDocumentTypeLegal) {
		this.listSecondDocumentTypeLegal = listSecondDocumentTypeLegal;
	}

	/**
	 * Gets the lst legal representative file.
	 *
	 * @return the lst legal representative file
	 */
	public List<LegalRepresentativeFile> getLstLegalRepresentativeFile() {
		return lstLegalRepresentativeFile;
	}

	/**
	 * Sets the lst legal representative file.
	 *
	 * @param lstLegalRepresentativeFile the new lst legal representative file
	 */
	public void setLstLegalRepresentativeFile(
			List<LegalRepresentativeFile> lstLegalRepresentativeFile) {
		this.lstLegalRepresentativeFile = lstLegalRepresentativeFile;
	}

	/**
	 * Gets the country residence.
	 *
	 * @return the country residence
	 */
	public Integer getCountryResidence() {
		return countryResidence;
	}

	/**
	 * Sets the country residence.
	 *
	 * @param countryResidence the new country residence
	 */
	public void setCountryResidence(Integer countryResidence) {
		this.countryResidence = countryResidence;
	}

	/**
	 * Checks if is flag active country resident.
	 *
	 * @return true, if is flag active country resident
	 */
	public boolean isFlagActiveCountryResident() {
		return flagActiveCountryResident;
	}

	/**
	 * Sets the flag active country resident.
	 *
	 * @param flagActiveCountryResident the new flag active country resident
	 */
	public void setFlagActiveCountryResident(boolean flagActiveCountryResident) {
		this.flagActiveCountryResident = flagActiveCountryResident;
	}

	/**
	 * Checks if is flag active document number.
	 *
	 * @return true, if is flag active document number
	 */
	public boolean isFlagActiveDocumentNumber() {
		return flagActiveDocumentNumber;
	}

	/**
	 * Sets the flag active document number.
	 *
	 * @param flagActiveDocumentNumber the new flag active document number
	 */
	public void setFlagActiveDocumentNumber(boolean flagActiveDocumentNumber) {
		this.flagActiveDocumentNumber = flagActiveDocumentNumber;
	}

	/**
	 * Checks if is flag active second document number.
	 *
	 * @return true, if is flag active second document number
	 */
	public boolean isFlagActiveSecondDocumentNumber() {
		return flagActiveSecondDocumentNumber;
	}

	/**
	 * Sets the flag active second document number.
	 *
	 * @param flagActiveSecondDocumentNumber the new flag active second document number
	 */
	public void setFlagActiveSecondDocumentNumber(
			boolean flagActiveSecondDocumentNumber) {
		this.flagActiveSecondDocumentNumber = flagActiveSecondDocumentNumber;
	}

	/**
	 * Gets the participant requester.
	 *
	 * @return the participant requester
	 */
	public Participant getParticipantRequester() {
		return participantRequester;
	}

	/**
	 * Sets the participant requester.
	 *
	 * @param participantRequester the new participant requester
	 */
	public void setParticipantRequester(Participant participantRequester) {
		this.participantRequester = participantRequester;
	}

	/**
	 * Checks if is disabled cmb participant requester.
	 *
	 * @return true, if is disabled cmb participant requester
	 */
	public boolean isDisabledCmbParticipantRequester() {
		return disabledCmbParticipantRequester;
	}

	/**
	 * Sets the disabled cmb participant requester.
	 *
	 * @param disabledCmbParticipantRequester the new disabled cmb participant requester
	 */
	public void setDisabledCmbParticipantRequester(
			boolean disabledCmbParticipantRequester) {
		this.disabledCmbParticipantRequester = disabledCmbParticipantRequester;
	}

	/**
	 * Gets the participant requester detail.
	 *
	 * @return the participant requester detail
	 */
	public Participant getParticipantRequesterDetail() {
		return participantRequesterDetail;
	}

	/**
	 * Sets the participant requester detail.
	 *
	 * @param participantRequesterDetail the new participant requester detail
	 */
	public void setParticipantRequesterDetail(Participant participantRequesterDetail) {
		this.participantRequesterDetail = participantRequesterDetail;
	}

	/**
	 * Gets the list geographic location exception.
	 *
	 * @return the list geographic location exception
	 */
	public List<GeographicLocation> getListGeographicLocationException() {
		return listGeographicLocationException;
	}

	/**
	 * Sets the list geographic location exception.
	 *
	 * @param listGeographicLocationException the new list geographic location exception
	 */
	public void setListGeographicLocationException(
			List<GeographicLocation> listGeographicLocationException) {
		this.listGeographicLocationException = listGeographicLocationException;
	}

	/**
	 * Gets the lst holder req file history detail.
	 *
	 * @return the lst holder req file history detail
	 */
	public List<HolderReqFileHistory> getLstHolderReqFileHistoryDetail() {
		return lstHolderReqFileHistoryDetail;
	}

	/**
	 * Sets the lst holder req file history detail.
	 *
	 * @param lstHolderReqFileHistoryDetail the new lst holder req file history detail
	 */
	public void setLstHolderReqFileHistoryDetail(
			List<HolderReqFileHistory> lstHolderReqFileHistoryDetail) {
		this.lstHolderReqFileHistoryDetail = lstHolderReqFileHistoryDetail;
	}

	/**
	 * Gets the lst holder req file history detail before.
	 *
	 * @return the lst holder req file history detail before
	 */
	public List<HolderReqFileHistory> getLstHolderReqFileHistoryDetailBefore() {
		return lstHolderReqFileHistoryDetailBefore;
	}

	/**
	 * Sets the lst holder req file history detail before.
	 *
	 * @param lstHolderReqFileHistoryDetailBefore the new lst holder req file history detail before
	 */
	public void setLstHolderReqFileHistoryDetailBefore(
			List<HolderReqFileHistory> lstHolderReqFileHistoryDetailBefore) {
		this.lstHolderReqFileHistoryDetailBefore = lstHolderReqFileHistoryDetailBefore;
	}

	/**
	 * Checks if is disabled cmb postal departament.
	 *
	 * @return true, if is disabled cmb postal departament
	 */
	public boolean isDisabledCmbPostalDepartament() {
		return disabledCmbPostalDepartament;
	}

	/**
	 * Sets the disabled cmb postal departament.
	 *
	 * @param disabledCmbPostalDepartament the new disabled cmb postal departament
	 */
	public void setDisabledCmbPostalDepartament(boolean disabledCmbPostalDepartament) {
		this.disabledCmbPostalDepartament = disabledCmbPostalDepartament;
	}

	/**
	 * Checks if is disabled cmb postal municipality.
	 *
	 * @return true, if is disabled cmb postal municipality
	 */
	public boolean isDisabledCmbPostalMunicipality() {
		return disabledCmbPostalMunicipality;
	}

	/**
	 * Sets the disabled cmb postal municipality.
	 *
	 * @param disabledCmbPostalMunicipality the new disabled cmb postal municipality
	 */
	public void setDisabledCmbPostalMunicipality(
			boolean disabledCmbPostalMunicipality) {
		this.disabledCmbPostalMunicipality = disabledCmbPostalMunicipality;
	}

	/**
	 * Gets the legal representative helper bean.
	 *
	 * @return the legal representative helper bean
	 */
	public LegalRepresentativeHelperBean getLegalRepresentativeHelperBean() {
		return legalRepresentativeHelperBean;
	}

	/**
	 * Sets the legal representative helper bean.
	 *
	 * @param legalRepresentativeHelperBean the new legal representative helper bean
	 */
	public void setLegalRepresentativeHelperBean(
			LegalRepresentativeHelperBean legalRepresentativeHelperBean) {
		this.legalRepresentativeHelperBean = legalRepresentativeHelperBean;
	}

	/**
	 * Gets the streamed content file.
	 *
	 * @return the streamed content file
	 */
	public StreamedContent getStreamedContentFile() {
		return streamedContentFile;
	}

	/**
	 * Sets the streamed content file.
	 *
	 * @param streamedContentFile the new streamed content file
	 */
	public void setStreamedContentFile(StreamedContent streamedContentFile) {
		this.streamedContentFile = streamedContentFile;
	}

	/**
	 * Gets the list department postal location.
	 *
	 * @return the list department postal location
	 */
	public List<GeographicLocation> getListDepartmentPostalLocation() {
		return listDepartmentPostalLocation;
	}

	/**
	 * Sets the list department postal location.
	 *
	 * @param listDepartmentPostalLocation the new list department postal location
	 */
	public void setListDepartmentPostalLocation(
			List<GeographicLocation> listDepartmentPostalLocation) {
		this.listDepartmentPostalLocation = listDepartmentPostalLocation;
	}

	/**
	 * Gets the list municipality postal location.
	 *
	 * @return the list municipality postal location
	 */
	public List<GeographicLocation> getListMunicipalityPostalLocation() {
		return listMunicipalityPostalLocation;
	}

	/**
	 * Sets the list municipality postal location.
	 *
	 * @param listMunicipalityPostalLocation the new list municipality postal location
	 */
	public void setListMunicipalityPostalLocation(
			List<GeographicLocation> listMunicipalityPostalLocation) {
		this.listMunicipalityPostalLocation = listMunicipalityPostalLocation;
	}

	/**
	 * Gets the list department location.
	 *
	 * @return the list department location
	 */
	public List<GeographicLocation> getListDepartmentLocation() {
		return listDepartmentLocation;
	}

	/**
	 * Sets the list department location.
	 *
	 * @param listDepartmentLocation the new list department location
	 */
	public void setListDepartmentLocation(
			List<GeographicLocation> listDepartmentLocation) {
		this.listDepartmentLocation = listDepartmentLocation;
	}

	/**
	 * Gets the list municipality location.
	 *
	 * @return the list municipality location
	 */
	public List<GeographicLocation> getListMunicipalityLocation() {
		return listMunicipalityLocation;
	}

	/**
	 * Sets the list municipality location.
	 *
	 * @param listMunicipalityLocation the new list municipality location
	 */
	public void setListMunicipalityLocation(
			List<GeographicLocation> listMunicipalityLocation) {
		this.listMunicipalityLocation = listMunicipalityLocation;
	}

	/**
	 * Gets the list department location before.
	 *
	 * @return the list department location before
	 */
	public List<GeographicLocation> getListDepartmentLocationBefore() {
		return listDepartmentLocationBefore;
	}

	/**
	 * Sets the list department location before.
	 *
	 * @param listDepartmentLocationBefore the new list department location before
	 */
	public void setListDepartmentLocationBefore(
			List<GeographicLocation> listDepartmentLocationBefore) {
		this.listDepartmentLocationBefore = listDepartmentLocationBefore;
	}

	/**
	 * Gets the list municipality location before.
	 *
	 * @return the list municipality location before
	 */
	public List<GeographicLocation> getListMunicipalityLocationBefore() {
		return listMunicipalityLocationBefore;
	}

	/**
	 * Sets the list municipality location before.
	 *
	 * @param listMunicipalityLocationBefore the new list municipality location before
	 */
	public void setListMunicipalityLocationBefore(
			List<GeographicLocation> listMunicipalityLocationBefore) {
		this.listMunicipalityLocationBefore = listMunicipalityLocationBefore;
	}

	/**
	 * Gets the list department postal location before.
	 *
	 * @return the list department postal location before
	 */
	public List<GeographicLocation> getListDepartmentPostalLocationBefore() {
		return listDepartmentPostalLocationBefore;
	}

	/**
	 * Sets the list department postal location before.
	 *
	 * @param listDepartmentPostalLocationBefore the new list department postal location before
	 */
	public void setListDepartmentPostalLocationBefore(
			List<GeographicLocation> listDepartmentPostalLocationBefore) {
		this.listDepartmentPostalLocationBefore = listDepartmentPostalLocationBefore;
	}

	/**
	 * Gets the list municipality postal location before.
	 *
	 * @return the list municipality postal location before
	 */
	public List<GeographicLocation> getListMunicipalityPostalLocationBefore() {
		return listMunicipalityPostalLocationBefore;
	}

	/**
	 * Sets the list municipality postal location before.
	 *
	 * @param listMunicipalityPostalLocationBefore the new list municipality postal location before
	 */
	public void setListMunicipalityPostalLocationBefore(
			List<GeographicLocation> listMunicipalityPostalLocationBefore) {
		this.listMunicipalityPostalLocationBefore = listMunicipalityPostalLocationBefore;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Gets the list geographic location without national country.
	 *
	 * @return the list geographic location without national country
	 */
	public List<GeographicLocation> getListGeographicLocationWithoutNationalCountry() {
		return listGeographicLocationWithoutNationalCountry;
	}

	/**
	 * Sets the list geographic location without national country.
	 *
	 * @param listGeographicLocationWithoutNationalCountry the new list geographic location without national country
	 */
	public void setListGeographicLocationWithoutNationalCountry(
			List<GeographicLocation> listGeographicLocationWithoutNationalCountry) {
		this.listGeographicLocationWithoutNationalCountry = listGeographicLocationWithoutNationalCountry;
	}


	/**
	 * Gets the list emission source.
	 *
	 * @return the list emission source
	 */
	public List<ParameterTable> getListDocumentSource() {
		return listDocumentSource;
	}

	/**
	 * Sets the list emission source.
	 *
	 * @param listDocumentSource the new list document source
	 */
	public void setListDocumentSource(List<ParameterTable> listDocumentSource) {
		this.listDocumentSource = listDocumentSource;
	}

	/**
	 * Checks if is flag national resident.
	 *
	 * @return true, if is flag national resident
	 */
	public boolean isFlagNationalResident() {
		return flagNationalResident;
	}

	/**
	 * Sets the flag national resident.
	 *
	 * @param flagNationalResident the new flag national resident
	 */
	public void setFlagNationalResident(boolean flagNationalResident) {
		this.flagNationalResident = flagNationalResident;
	}

	/**
	 * Checks if is flag national not resident.
	 *
	 * @return true, if is flag national not resident
	 */
	public boolean isFlagNationalNotResident() {
		return flagNationalNotResident;
	}

	/**
	 * Sets the flag national not resident.
	 *
	 * @param flagNationalNotResident the new flag national not resident
	 */
	public void setFlagNationalNotResident(boolean flagNationalNotResident) {
		this.flagNationalNotResident = flagNationalNotResident;
	}

	/**
	 * Gets the document to.
	 *
	 * @return the document to
	 */
	public DocumentTO getDocumentTO() {
		return documentTO;
	}

	/**
	 * Sets the document to.
	 *
	 * @param documentTO the new document to
	 */
	public void setDocumentTO(DocumentTO documentTO) {
		this.documentTO = documentTO;
	}

	/**
	 * Gets the list document issuance date.
	 *
	 * @return the list document issuance date
	 */
	public LinkedHashMap<String, Integer> getListDocumentIssuanceDate() {
		return listDocumentIssuanceDate;
	}

	/**
	 * Sets the list document issuance date.
	 *
	 * @param listDocumentIssuanceDate the list document issuance date
	 */
	public void setListDocumentIssuanceDate(LinkedHashMap<String, Integer> listDocumentIssuanceDate) {
		this.listDocumentIssuanceDate = listDocumentIssuanceDate;
	}

	/**
	 * Gets the file download.
	 *
	 * @return the file download
	 */
	public StreamedContent getFileDownload() {
		return fileDownload;
	}

	/**
	 * Sets the file download.
	 *
	 * @param fileDownload the new file download
	 */
	public void setFileDownload(StreamedContent fileDownload) {
		this.fileDownload = fileDownload;
	}

	/**
	 * Gets the user privilege.
	 *
	 * @return the user privilege
	 */
	public UserPrivilege getUserPrivilege() {
		return userPrivilege;
	}

	/**
	 * Sets the user privilege.
	 *
	 * @param userPrivilege the new user privilege
	 */
	public void setUserPrivilege(UserPrivilege userPrivilege) {
		this.userPrivilege = userPrivilege;
	}

	/**
	 * Gets the list civil status.
	 *
	 * @return the list civil status
	 */
	public List<ParameterTable> getListCivilStatus() {
		return listCivilStatus;
	}

	/**
	 * Sets the list civil status.
	 *
	 * @param listCivilStatus the new list civil status
	 */
	public void setListCivilStatus(List<ParameterTable> listCivilStatus) {
		this.listCivilStatus = listCivilStatus;
	}

	/**
	 * Gets the list total income.
	 *
	 * @return the list total income
	 */
	public List<ParameterTable> getListTotalIncome() {
		return listTotalIncome;
	}

	/**
	 * Sets the list total income.
	 *
	 * @param listTotalIncome the new list total income
	 */
	public void setListTotalIncome(List<ParameterTable> listTotalIncome) {
		this.listTotalIncome = listTotalIncome;
	}

	/**
	 * Gets the client information to.
	 *
	 * @return the client information to
	 */
	public ClientInformationTO getClientInformationTO() {
		return clientInformationTO;
	}

	/**
	 * Sets the client information to.
	 *
	 * @param clientInformationTO the new client information to
	 */
	public void setClientInformationTO(ClientInformationTO clientInformationTO) {
		this.clientInformationTO = clientInformationTO;
	}
	
	
	/**
	 * File upload handler.
	 *
	 * @param event the event
	 */
	public void fileUploadHandler(FileUploadEvent event){
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);				
		Object [] dataOnMessage = { event.getFile().getFileName(), event.getFile().getSize() / 1000 };
		showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, "opt.adm.holder.fupl.ok", dataOnMessage);
		String fDisplayName = fUploadValidateFile(event.getFile(), null, null);
		if(fDisplayName != null){			 
			try {				
				InputStream inputImage = new ByteArrayInputStream(event.getFile().getContents());
				filePhotoHolder = new DefaultStreamedContent(inputImage, event.getFile().getContentType(), event.getFile().getFileName());  
				photoHolderName = event.getFile().getFileName();				
				holderHistory.setFilePhoto(event.getFile().getContents());				
				filePhotoTemp = event.getFile().getContents();				
				JSFUtilities.putSessionMap("sessStreamedPhoto", filePhotoTemp);
				auxFuplPhoto = Boolean.TRUE;
				formState = Boolean.TRUE;
			} catch (Exception e) {
				e.printStackTrace();
			}
			 JSFUtilities.showValidationDialog();
		 }
	}
	
	/**
	 * Delete system image temp.
	 */
	public void deleteFilePhotoTemp(){
		filePhotoTemp = null;
		photoHolderName = "";
		holderHistory.setFilePhoto(null);
		filePhotoHolder = null;
		JSFUtilities.putSessionMap("sessStreamedPhoto",null);
	}
	
	/**
	 * Gets the streamed content file photo.
	 *
	 * @return the streamed content file photo
	 */
	public StreamedContent getStreamedContentFilePhoto(){
		StreamedContent streamedContentFile = null;
		try {
			InputStream inputStream = null;
			if (filePhotoTemp==null) {
				inputStream = new ByteArrayInputStream(holder.getFilePhoto());
				streamedContentFile = new DefaultStreamedContent(inputStream, null,	holder.getFilePhotoName());
				inputStream.close();
			}else {
				inputStream = new ByteArrayInputStream(filePhotoTemp);
				streamedContentFile = new DefaultStreamedContent(inputStream, null,	photoHolderName);
				inputStream.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return streamedContentFile;
	}
	
	/**
	 * Gets the streamed content file photo history detail.
	 *
	 * @return the streamed content file photo history detail
	 */
	public StreamedContent getStreamedContentFilePhotoHistoryDetail(){
		StreamedContent streamedContentFile = null;
		try {
			InputStream inputStream = null;
			inputStream = new ByteArrayInputStream(holderHistoryDetail.getFilePhoto());
			streamedContentFile = new DefaultStreamedContent(inputStream, null,	holderHistoryDetail.getFilePhotoName());
			inputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return streamedContentFile;
	}
	
	/**
	 * Gets the streamed content file photo history.
	 *
	 * @return the streamed content file photo history
	 */
	public StreamedContent getStreamedContentFilePhotoHistory(){
		StreamedContent streamedContentFile = null;
		try {
			InputStream inputStream = null;
			inputStream = new ByteArrayInputStream(holderHistory.getFilePhoto());
			streamedContentFile = new DefaultStreamedContent(inputStream, null,	holderHistory.getFilePhotoName());
			inputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return streamedContentFile;
	}
	
	/**
	 * File upload handler.
	 *
	 * @param event the event
	 */
	public void fileUploadHandlerSigning(FileUploadEvent event){
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);				
		Object [] dataOnMessage = { event.getFile().getFileName(), event.getFile().getSize() / 1000 };
		showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, "opt.adm.holder.fupl.ok", dataOnMessage);
		String fDisplayName = fUploadValidateFile(event.getFile(), null, null);
		if(fDisplayName != null){			 
			try {				
				InputStream inputImage = new ByteArrayInputStream(event.getFile().getContents());
				fileSigning = new DefaultStreamedContent(inputImage, event.getFile().getContentType(), event.getFile().getFileName());  
				signingHolderName = event.getFile().getFileName();				
				holderHistory.setFileSigning(event.getFile().getContents());				
				fileSigningTemp = event.getFile().getContents();				
				JSFUtilities.putSessionMap("sessStreamedSigning", fileSigningTemp);
				auxFuplSigning = Boolean.TRUE;
				formState = Boolean.TRUE;
			} catch (Exception e) {
				e.printStackTrace();
			}
			 JSFUtilities.showValidationDialog();
		 }
	}
	
	/**
	 * Delete system image temp.
	 */
	public void deleteFileSigningTemp(){
		fileSigningTemp = null;
		signingHolderName = "";
		holderHistory.setFileSigning(null);
		fileSigning = null;
		JSFUtilities.putSessionMap("sessStreamedSigning", null);
	}
	
	/**
	 * Clean file image.
	 */
	public void cleanFileImage(){
		JSFUtilities.removeSessionMap("sessStreamedPhoto");
		JSFUtilities.removeSessionMap("sessStreamedSigning");
		JSFUtilities.removeSessionMap("sessStreamedPhotoModify");
		JSFUtilities.removeSessionMap("sessStreamedSigningModify");
	}
	
	public void changeEconomicActivityInModify() {
		buildInvestorTypeByEconAcSelected(holderHistory.getEconomicActivity());
	}
	
	public void buildInvestorTypeByEconAcSelected(Integer economicActivity){
		// construyendo la lista de tipo de inversionista
		InvestorTypeModelTO it = new InvestorTypeModelTO();
		it.setEconomicActivity(economicActivity);
		try {
			listInvestorType = investorTypeFacade.getLstInvestorTypeModelTO(it);
			
			// sugerir el tipo de inversionista por defecto
			if(Validations.validateIsNotNull(holderHistory)){
				holderHistory.setInvestorType(getInvestorTypeByDefault(economicActivity));
				if(Validations.validateIsNotNullAndPositive(holderHistory.getJuridicClass())){
					if(holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode()) ){
						// forzamos a que el tipo de inv sea FIDEICOMISO 
						holderHistory.setInvestorType(2584);
					}
				}
			}
		} catch (ServiceException e) {
			e.printStackTrace();
			listInvestorType = new ArrayList<>();
		}
	}
	private Integer getInvestorTypeByDefault(Integer economicActivity){
		try {
			if(Validations.validateIsNotNullAndPositive(economicActivity)){
				List<ParameterTable> lstEconomicActivityWithInvetorType = investorTypeFacade.getLstEconomicActivityToRelationInvestortype();
				for(ParameterTable pt:lstEconomicActivityWithInvetorType){
					if( Validations.validateIsNotNull(pt) 
						&& Validations.validateIsNotNullAndPositive(pt.getParameterTablePk())
						&& pt.getParameterTablePk().equals(economicActivity) ){
						return pt.getShortInteger();
					}
				}
			}
		} catch (Exception e) {
			System.out.println("::: Error al sugerir el tipo de inversionista: (error irrelevante) "+e.getMessage());
		}
		return null;
	}

	/**
	 * Gets the file photo holder.
	 *
	 * @return the file photo holder
	 */
	public StreamedContent getFilePhotoHolder() {
		return filePhotoHolder;
	}

	/**
	 * Sets the file photo holder.
	 *
	 * @param filePhotoHolder the new file photo holder
	 */
	public void setFilePhotoHolder(StreamedContent filePhotoHolder) {
		this.filePhotoHolder = filePhotoHolder;
	}

	/**
	 * Gets the file signing.
	 *
	 * @return the file signing
	 */
	public StreamedContent getFileSigning() {
		return fileSigning;
	}

	/**
	 * Sets the file signing.
	 *
	 * @param fileSigning the new file signing
	 */
	public void setFileSigning(StreamedContent fileSigning) {
		this.fileSigning = fileSigning;
	}

	/**
	 * Gets the photo holder name.
	 *
	 * @return the photo holder name
	 */
	public String getPhotoHolderName() {
		return photoHolderName;
	}

	/**
	 * Sets the photo holder name.
	 *
	 * @param photoHolderName the new photo holder name
	 */
	public void setPhotoHolderName(String photoHolderName) {
		this.photoHolderName = photoHolderName;
	}

	/**
	 * Gets the signing holder name.
	 *
	 * @return the signing holder name
	 */
	public String getSigningHolderName() {
		return signingHolderName;
	}

	/**
	 * Sets the signing holder name.
	 *
	 * @param signingHolderName the new signing holder name
	 */
	public void setSigningHolderName(String signingHolderName) {
		this.signingHolderName = signingHolderName;
	}

	/**
	 * Gets the file photo temp.
	 *
	 * @return the file photo temp
	 */
	public byte[] getFilePhotoTemp() {
		return filePhotoTemp;
	}

	/**
	 * Sets the file photo temp.
	 *
	 * @param filePhotoTemp the new file photo temp
	 */
	public void setFilePhotoTemp(byte[] filePhotoTemp) {
		this.filePhotoTemp = filePhotoTemp;
	}

	/**
	 * Gets the file signing temp.
	 *
	 * @return the file signing temp
	 */
	public byte[] getFileSigningTemp() {
		return fileSigningTemp;
	}

	/**
	 * Sets the file signing temp.
	 *
	 * @param fileSigningTemp the new file signing temp
	 */
	public void setFileSigningTemp(byte[] fileSigningTemp) {
		this.fileSigningTemp = fileSigningTemp;
	}

	/**
	 * Checks if is aux fupl photo.
	 *
	 * @return true, if is aux fupl photo
	 */
	public boolean isAuxFuplPhoto() {
		return auxFuplPhoto;
	}

	/**
	 * Sets the aux fupl photo.
	 *
	 * @param auxFuplPhoto the new aux fupl photo
	 */
	public void setAuxFuplPhoto(boolean auxFuplPhoto) {
		this.auxFuplPhoto = auxFuplPhoto;
	}

	/**
	 * Checks if is aux fupl signing.
	 *
	 * @return true, if is aux fupl signing
	 */
	public boolean isAuxFuplSigning() {
		return auxFuplSigning;
	}

	/**
	 * Sets the aux fupl signing.
	 *
	 * @param auxFuplSigning the new aux fupl signing
	 */
	public void setAuxFuplSigning(boolean auxFuplSigning) {
		this.auxFuplSigning = auxFuplSigning;
	}

	/**
	 * Checks if is form state.
	 *
	 * @return true, if is form state
	 */
	public boolean isFormState() {
		return formState;
	}

	/**
	 * Sets the form state.
	 *
	 * @param formState the new form state
	 */
	public void setFormState(boolean formState) {
		this.formState = formState;
	}

	/**
	 * Checks if is flag cui related.
	 *
	 * @return the flagCUIRelated
	 */
	public boolean isFlagCUIRelated() {
		return flagCUIRelated;
	}

	/**
	 * Sets the flag cui related.
	 *
	 * @param flagCUIRelated the flagCUIRelated to set
	 */
	public void setFlagCUIRelated(boolean flagCUIRelated) {
		this.flagCUIRelated = flagCUIRelated;
	}

	/**
	 * Checks if is flag nemonic fund.
	 *
	 * @return the flagNemonicFund
	 */
	public boolean isFlagNemonicFund() {
		return flagNemonicFund;
	}

	/**
	 * Sets the flag nemonic fund.
	 *
	 * @param flagNemonicFund the flagNemonicFund to set
	 */
	public void setFlagNemonicFund(boolean flagNemonicFund) {
		this.flagNemonicFund = flagNemonicFund;
	}

	/**
	 * Checks if is flag nemonic asfi.
	 *
	 * @return the flagNemonicAsfi
	 */
	public boolean isFlagNemonicAsfi() {
		return flagNemonicAsfi;
	}

	/**
	 * Sets the flag nemonic asfi.
	 *
	 * @param flagNemonicAsfi the flagNemonicAsfi to set
	 */
	public void setFlagNemonicAsfi(boolean flagNemonicAsfi) {
		this.flagNemonicAsfi = flagNemonicAsfi;
	}

	/**
	 * Checks if is disabled nemonic asfi.
	 *
	 * @return the disabledNemonicAsfi
	 */
	public boolean isDisabledNemonic() {
		return disabledNemonic;
	}

	/**
	 * Sets the disabled nemonic asfi.
	 *
	 * @param disabledNemonicAsfi the disabledNemonicAsfi to set
	 */
	public void setDisabledNemonic(boolean disabledNemonic) {
		this.disabledNemonic = disabledNemonic;
	}

	/**
	 * Checks if is flag transfer number.
	 *
	 * @return the flagTransferNumber
	 */
	public boolean isFlagTransferNumber() {
		return flagTransferNumber;
	}

	/**
	 * Sets the flag transfer number.
	 *
	 * @param flagTransferNumber the flagTransferNumber to set
	 */
	public void setFlagTransferNumber(boolean flagTransferNumber) {
		this.flagTransferNumber = flagTransferNumber;
	}

	/**
	 * Checks if is disabled transfer number.
	 *
	 * @return the disabledTransferNumber
	 */
	public boolean isDisabledTransferNumber() {
		return disabledTransferNumber;
	}

	/**
	 * Sets the disabled transfer number.
	 *
	 * @param disabledTransferNumber the disabledTransferNumber to set
	 */
	public void setDisabledTransferNumber(boolean disabledTransferNumber) {
		this.disabledTransferNumber = disabledTransferNumber;
	}

	/**
	 * Checks if is flag is issuer dpf institution.
	 *
	 * @return the flagIsIssuerDpfInstitution
	 */
	public boolean isFlagIsIssuerDpfInstitution() {
		return flagIsIssuerDpfInstitution;
	}

	/**
	 * Sets the flag is issuer dpf institution.
	 *
	 * @param flagIsIssuerDpfInstitution the flagIsIssuerDpfInstitution to set
	 */
	public void setFlagIsIssuerDpfInstitution(boolean flagIsIssuerDpfInstitution) {
		this.flagIsIssuerDpfInstitution = flagIsIssuerDpfInstitution;
	}
	
	public boolean isFlagSendASFI() {
		return flagSendASFI;
	}

	public void setFlagSendASFI(boolean flagSendASFI) {
		this.flagSendASFI = flagSendASFI;
	}

	/**
	 * false if not exists filename and documentType in holderReqFileHistory to render (*)
	 * @param documentType
	 * @param filename
	 * @param use
	 * @return
	 */
	public boolean getModifiedHolderReqFileHis(Integer documentType, String filename, String use){
		Iterator<HolderReqFileHistory> it;
		//HolderReqFileHistory holderReqFileHis = (HolderReqFileHistory)holderReqFileHistory;
		if(use.equals("new")){
			it = lstHolderReqFileHistoryDetailBefore.iterator();
		}else{
			it = lstHolderReqFileHistoryDetail.iterator();
		}
		HolderReqFileHistory holderReqFileHistory;
		while(it.hasNext()) {
			holderReqFileHistory = it.next();
		    if(holderReqFileHistory.getFilename().equals(filename)){
		    	if(holderReqFileHistory.getDocumentType().equals(documentType)){
		    		return false;
		    	}
		    }
		}
	    return true;
	}
	
	public boolean getModifiedHolderReqChangesFileHis(Integer documentType, String filename, String use){
		Iterator<HolderReqFileHistory> it;
		//HolderReqFileHistory holderReqFileHis = (HolderReqFileHistory)holderReqFileHistory;
		if(use.equals("new")){
			it = lstHolderReqChangesMotiveFileHistoryDetailBefore.iterator();
		}else{
			it = lstHolderReqChangesMotiveFileHistoryDetail.iterator();
		}
		HolderReqFileHistory holderReqFileModificationsHistory;
		while(it.hasNext()) {
			holderReqFileModificationsHistory = it.next();
		    if(holderReqFileModificationsHistory.getFilename().equals(filename)){
		    	if(holderReqFileModificationsHistory.getDocumentType().equals(documentType)){
		    		return false;
		    	}
		    }
		}
	    return true;
	}
	
	public String getNewDocumentNumber() {
		return newDocumentNumber;
	}

	public void setNewDocumentNumber(String newDocumentNumber) {
		this.newDocumentNumber = newDocumentNumber;
	}

	public String getNewDocumentNumberSecondNationality() {
		return newDocumentNumberSecondNationality;
	}

	public void setNewDocumentNumberSecondNationality(
			String newDocumentNumberSecondNationality) {
		this.newDocumentNumberSecondNationality = newDocumentNumberSecondNationality;
	}

	public List<InvestorTypeModelTO> getListInvestorType() {
		return listInvestorType;
	}

	public void setListInvestorType(List<InvestorTypeModelTO> listInvestorType) {
		this.listInvestorType = listInvestorType;
	}
	
    public boolean isindChangesNeedsConfirm() {
        return indChangesNeedsConfirm;
    }
    
	public void setindChangesNeedsConfirm(boolean indChangesNeedsConfirm) {
	        this.indChangesNeedsConfirm = indChangesNeedsConfirm;
	}
	
	public Integer getAttachFileHolderChangesMotivesSelected() {
	        return attachFileHolderChangesMotivesSelected;
	}
	
	public void setAttachFileHolderChangesMotivesSelected(Integer attachFileHolderChangesMotivesSelected) {
	        this.attachFileHolderChangesMotivesSelected = attachFileHolderChangesMotivesSelected;
	}
	
	public List<HolderReqFileHistory> getLstHolderReqChangesFileHistory() {
	        return lstHolderReqChangesFileHistory;
	}
	
	public void setLstHolderReqChangesFileHistory(List<HolderReqFileHistory> lstHolderReqChangesFileHistory) {
	        this.lstHolderReqChangesFileHistory = lstHolderReqChangesFileHistory;
	}
	
	public String getHolderFileChangesNameDisplay() {
	        return holderFileChangesNameDisplay;
	}
	
	public void setHolderFileChangesNameDisplay(String holderFileChangesNameDisplay) {
	        this.holderFileChangesNameDisplay = holderFileChangesNameDisplay;
	}
	
	public List<ParameterTable> getListHolderChangesMotivesDocument() {
	        return listHolderChangesMotivesDocument;
	}
	
	public void setListHolderChangesMotivesDocument(List<ParameterTable> listHolderChangesMotivesDocument) {
	        this.listHolderChangesMotivesDocument = listHolderChangesMotivesDocument;
	}
	
	public List<HolderReqFileHistory> getLstHolderReqChangesMotiveFileHistoryDetail() {
	        return lstHolderReqChangesMotiveFileHistoryDetail;
	}
	
	public void setLstHolderReqChangesMotiveFileHistoryDetail(
	                List<HolderReqFileHistory> lstHolderReqChangesMotiveFileHistoryDetail) {
	        this.lstHolderReqChangesMotiveFileHistoryDetail = lstHolderReqChangesMotiveFileHistoryDetail;
	}
	
	public List<HolderReqFileHistory> getLstHolderReqChangesMotiveFileHistoryDetailBefore() {
	        return lstHolderReqChangesMotiveFileHistoryDetailBefore;
	}
	
	public void setLstHolderReqChangesMotiveFileHistoryDetailBefore(
	                List<HolderReqFileHistory> lstHolderReqChangesMotiveFileHistoryDetailBefore) {
	        this.lstHolderReqChangesMotiveFileHistoryDetailBefore = lstHolderReqChangesMotiveFileHistoryDetailBefore;
	}

	public List<ParameterTable> getListPepGradeWithPersonType() {
		return listPepGradeWithPersonType;
	}

	public void setListPepGradeWithPersonType(List<ParameterTable> listPepGradeWithPersonType) {
		this.listPepGradeWithPersonType = listPepGradeWithPersonType;
	}

	public List<ParameterTable> getListEconomicActivityNaturalPerson() {
		return listEconomicActivityNaturalPerson;
	}

	public void setListEconomicActivityNaturalPerson(List<ParameterTable> listEconomicActivityNaturalPerson) {
		this.listEconomicActivityNaturalPerson = listEconomicActivityNaturalPerson;
	}

	public List<ParameterTable> getListPepGrade() {
		return listPepGrade;
	}

	public void setListPepGrade(List<ParameterTable> listPepGrade) {
		this.listPepGrade = listPepGrade;
	}

	public Integer getLinkForFamily() {
		return linkForFamily;
	}

}
