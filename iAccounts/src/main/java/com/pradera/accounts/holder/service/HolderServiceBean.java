package com.pradera.accounts.holder.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pradera.accounts.holder.to.HolderAnnulationModelTO;
import com.pradera.accounts.holder.to.HolderRequestTO;
import com.pradera.accounts.investorType.facade.InvestorTypeServiceBean;
import com.pradera.accounts.util.Permutations;
import com.pradera.commons.type.RegistryType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.accounts.to.CuiQueryResponseTO;
import com.pradera.integration.component.accounts.to.CuiQueryTO;
import com.pradera.integration.component.accounts.to.HolderAccountObjectTO;
import com.pradera.integration.component.accounts.to.JuridicHolderObjectTO;
import com.pradera.integration.component.accounts.to.NaturalHolderObjectTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.HolderAnnulation;
import com.pradera.model.accounts.HolderFile;
import com.pradera.model.accounts.HolderHistory;
import com.pradera.model.accounts.HolderHistoryState;
import com.pradera.model.accounts.HolderReqFileHistory;
import com.pradera.model.accounts.HolderRequest;
import com.pradera.model.accounts.InstitutionInformation;
import com.pradera.model.accounts.LegalRepresentative;
import com.pradera.model.accounts.LegalRepresentativeFile;
import com.pradera.model.accounts.LegalRepresentativeHistory;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.PepPerson;
import com.pradera.model.accounts.RepresentativeFileHistory;
import com.pradera.model.accounts.RepresentedEntity;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.EconomicActivityType;
import com.pradera.model.accounts.type.HolderAnnulationStateType;
import com.pradera.model.accounts.type.HolderFileStateType;
import com.pradera.model.accounts.type.HolderReqFileHisType;
import com.pradera.model.accounts.type.HolderRequestStateType;
import com.pradera.model.accounts.type.HolderRequestType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.JuridicClassType;
import com.pradera.model.accounts.type.LegalRepresentativeStateType;
import com.pradera.model.accounts.type.PepPersonClassificationType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.accounts.type.RepresentativeEntityStateType;
import com.pradera.model.accounts.type.StateHistoryStateHolderType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.operations.RequestCorrelativeType;
import com.pradera.model.report.ReportLoggerFile;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class HolderServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class HolderServiceBean extends CrudDaoServiceBean {
	
	/** The holder control bean. */
	@EJB
	HolderControlBean holderControlBean;
	
	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
	@EJB
	InvestorTypeServiceBean investorTypeServiceBean;
	
	/** issue 605: registro de solictud de anulacion de cui
	 * 
	 * @param entity
	 * @return */
	public HolderAnnulation regiterHolderAnnulation(HolderAnnulation entity) {
		return createWithFlush(entity);
	}
	
	/**
	 * issue 605: retorna los HolderAccountDetail de un determinado Holder
	 * @param idHolderPk
	 * @return
	 */
	public List<HolderAccountDetail> getHolderAccountDetailOfHolder(Long idHolderPk){
		List<HolderAccountDetail> lst=new ArrayList<>();
		StringBuilder sd=new StringBuilder();
		sd.append(" select had");
		sd.append(" from HolderAccountDetail had ");
		sd.append(" inner join fetch had.holderAccount ha");
		sd.append(" where 0=0");
		sd.append(" and had.holder.idHolderPk = :idHolderPk");
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idHolderPk", idHolderPk);
		lst = findListByQueryString(sd.toString(), parameters);
		return lst;
	}

	/** issue 605: modificacion de solictud de anulacion de cui
	 * 
	 * @param entity
	 * @return
	 * @throws ServiceException */
	public HolderAnnulation updateHolderAnnulation(HolderAnnulation entity) throws ServiceException {
		return update(entity);
	}
	
	/**
	 * issue 605: retorna la cantidad de saldos que un CUI tiene en cartera
	 * @param idHolderPk
	 * @return si retorna 0, significa que NO tiene saldo en cartera
	 */
	public int countBalanceOfOneHolder(Long idHolderPk){
		StringBuilder sd = new StringBuilder();
		sd.append(" select COUNT(*) from HOLDER_ACCOUNT_BALANCE HAB                       ");
		sd.append(" where HAB.ID_HOLDER_ACCOUNT_PK in ( select HAD.ID_HOLDER_ACCOUNT_FK   ");
		sd.append("                                     from HOLDER_ACCOUNT_DETAIL HAD    ");
		sd.append("                                     where HAD.ID_HOLDER_FK = :idHolderPk ) ");
		sd.append(" and (HAB.AVAILABLE_BALANCE > 0                                        ");
		sd.append("     or HAB.TOTAL_BALANCE > 0                                          ");
		sd.append("     or HAB.REPORTED_BALANCE > 0                                       ");
		sd.append("     or HAB.TRANSIT_BALANCE > 0                                        ");
		sd.append("     or HAB.PAWN_BALANCE > 0                                           ");
		sd.append("     or HAB.BAN_BALANCE > 0                                            ");
		sd.append("     or HAB.OTHER_BLOCK_BALANCE > 0                                    ");
		sd.append("     or HAB.SALE_BALANCE > 0                                           ");
		sd.append("     or HAB.PURCHASE_BALANCE > 0                                       ");
		sd.append("     or HAB.MARGIN_BALANCE > 0                                         ");
		sd.append("     or HAB.ACCREDITATION_BALANCE > 0                                  ");
		sd.append("     or HAB.LOANABLE_BALANCE > 0                                       ");
		sd.append("     or HAB.RESERVE_BALANCE > 0                                        ");
		sd.append("     or HAB.OPPOSITION_BALANCE > 0                                     ");
		sd.append("     or HAB.BORROWER_BALANCE > 0                                       ");
		sd.append("     or HAB.REPORTING_BALANCE > 0)									  ");

		Query q = em.createNativeQuery(sd.toString());
		q.setParameter("idHolderPk", idHolderPk);

		return Integer.parseInt(q.getSingleResult().toString());
	}
	
	/** issue 605: retorna la cantidad de solicitudes de bloqueo pendientes de confirmacion de CUI en la que se encuentra un titular
	 * 
	 * @param idHolderPk
	 * @return */
	public int countRequestBlockOfHolder(Long idHolderPk) {

		StringBuilder sd = new StringBuilder();
		sd.append(" select count(*) ");
		sd.append(" from HolderRequest hr ");
		sd.append(" where hr.holder.idHolderPk = :idHolderPk ");
		sd.append(" and hr.requestType = ").append(HolderRequestType.BLOCK.getCode());
		sd.append(" and hr.stateHolderRequest in (");
		sd.append(HolderRequestStateType.REGISTERED.getCode()).append(",");
		sd.append(HolderRequestStateType.MODIFIED.getCode()).append(",");
		sd.append(HolderRequestStateType.APPROVED.getCode()).append(")");

		Query q = em.createQuery(sd.toString());
		q.setParameter("idHolderPk", idHolderPk);

		return Integer.parseInt(q.getSingleResult().toString());
	}
	
	/** issue 605: retorna la cantidad de solicitudes de desbloqueo pendientes de confirmacion de CUI en la que se encuentra un titular
	 * 
	 * @param idHolderPk
	 * @return */
	public int countRequestUnblockOfHolder(Long idHolderPk) {

		StringBuilder sd = new StringBuilder();
		sd.append(" select count(*) ");
		sd.append(" from HolderRequest hr ");
		sd.append(" where hr.holder.idHolderPk = :idHolderPk ");
		sd.append(" and hr.requestType = ").append(HolderRequestType.UNBLOCK.getCode());
		sd.append(" and hr.stateHolderRequest in (");
		sd.append(HolderRequestStateType.REGISTERED.getCode()).append(",");
		sd.append(HolderRequestStateType.MODIFIED.getCode()).append(",");
		sd.append(HolderRequestStateType.APPROVED.getCode()).append(")");

		Query q = em.createQuery(sd.toString());
		q.setParameter("idHolderPk", idHolderPk);

		return Integer.parseInt(q.getSingleResult().toString());
	}
	
	/**
	 * issue 605: retorna la cantidad de solicitudes de anulacion pendientes de confirmacion de CUI en la que se encuentra un titular
	 * @param idHolderPk
	 * @return
	 */
	public int countRequestAnnulationOfHolder(Long idHolderPk) {

		StringBuilder sd = new StringBuilder();
		sd.append(" select count(*) ");
		sd.append(" from HolderAnnulation han ");
		sd.append(" where han.idHolderFk.idHolderPk = :idHolderPk ");
		sd.append(" and han.stateAnnulation in (");
		sd.append(HolderAnnulationStateType.REGISTERED.getCode()).append(",");
		sd.append(HolderAnnulationStateType.APPROVED.getCode()).append(",");
		sd.append(HolderAnnulationStateType.REVIEWED.getCode()).append(")");

		Query q = em.createQuery(sd.toString());
		q.setParameter("idHolderPk", idHolderPk);

		return Integer.parseInt(q.getSingleResult().toString());
	}
	
	/**
	 * issue 605: obtiene los mnemocios de los participates relacionados a un CUI
	 * @param idHolderPk
	 * @return
	 */
	public List<String> getParticipantsRelationshipOfHolder(Long idHolderPk){
		StringBuilder sd=new StringBuilder();
		sd.append(" select distinct p.mnemonic");
		sd.append(" from HolderAccountDetail had");
		sd.append(" inner join had.holderAccount ha ");
		sd.append(" inner join ha.participant p");
		sd.append(" where had.holder.idHolderPk = :idHolderPk");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idHolderPk", idHolderPk);
		List<String> lst = findListByQueryString(sd.toString(), parameters);
		return lst;
	}

	/** issue 605: metodo para obtener un HolderAnnulation incluido los datos del CUI y su participante creador
	 * 
	 * @param idHolderAnnulationPk
	 * @return
	 * @throws ServiceException */
	public HolderAnnulation getOneHolderAnnulationForView(Long idHolderAnnulationPk) throws ServiceException {
		List<HolderAnnulation> lst = new ArrayList<>();
		StringBuilder sd = new StringBuilder();
		sd.append(" select han");
		sd.append(" from HolderAnnulation han ");
		sd.append(" inner join fetch han.idHolderFk h");
		// este join no es de HolderAnnulation sino es el fk de Holder para saber su participante creador
		sd.append(" inner join fetch h.participant h");
		sd.append(" where 0=0 ");
		sd.append(" and han.idAnnulationHolderPk = :idHolderAnnulationPk ");

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idHolderAnnulationPk", idHolderAnnulationPk);
		lst = findListByQueryString(sd.toString(), parameters);
		if (lst.isEmpty()) {
			return null;
		} else {
			return lst.get(0);
		}
	}

	/** issue 605: obtiene las solictudes de anulacion de cui segun los filtros enviados
	 * 
	 * @param filter
	 * @return
	 * @throws ServiceException */
	public List<HolderAnnulationModelTO> getLstHolderAnnulationTO(HolderAnnulationModelTO filter) throws ServiceException {
		List<HolderAnnulationModelTO> lst = new ArrayList<>();
		StringBuilder sd = new StringBuilder();
		sd.append("select");
		sd.append(" new com.pradera.accounts.holder.to.HolderAnnulationModelTO(");
		sd.append(" han.idAnnulationHolderPk,");
		sd.append(" han.motiveAnnulation,");
		sd.append(" han.observationAnnulation,");
		sd.append(" han.fileTypeAnnulation,");
		sd.append(" han.stateAnnulation,");
		sd.append(" han.registryDate,");
		sd.append(" p.idParticipantPk,");
		sd.append(" p.mnemonic,");
		sd.append(" h.idHolderPk,");
		sd.append(" h.fullName");
		sd.append(" ) ");
		sd.append(" from HolderAnnulation han ");
		sd.append(" inner join han.idParticipantFk p");
		sd.append(" inner join han.idHolderFk h");
		sd.append(" where 0=0");
		if (Validations.validateIsNotNull(filter.getInitialDate()) && Validations.validateIsNotNull(filter.getFinalDate())) {
			sd.append(" and trunc(han.registryDate) between :initialDate and :finalDate");
		}
		if (Validations.validateIsNotNull(filter.getIdParticipantSelected())) {
			sd.append(" and p.idParticipantPk = :idParticipant");
		}
		if (Validations.validateIsNotNull(filter.getIdAnnulationHolderPk())) {
			sd.append(" and han.idAnnulationHolderPk = :numberRequest");
		}
		if (Validations.validateIsNotNull(filter.getHolderSelected()) && Validations.validateIsNotNull(filter.getHolderSelected().getIdHolderPk())) {
			sd.append(" and h.idHolderPk = :idHolder");
		}
		if (Validations.validateIsNotNull(filter.getStateAnnulation())) {
			sd.append(" and han.stateAnnulation = :stateHolderAnnulation");
		}
		sd.append(" order by han.idAnnulationHolderPk asc");

		Map<String, Object> parameters = new HashMap<String, Object>();
		if (Validations.validateIsNotNull(filter.getInitialDate()) && Validations.validateIsNotNull(filter.getFinalDate())) {
			parameters.put("initialDate", filter.getInitialDate());
			parameters.put("finalDate", filter.getFinalDate());
		}
		if (Validations.validateIsNotNull(filter.getIdParticipantSelected())) {
			parameters.put("idParticipant", filter.getIdParticipantSelected());
		}
		if (Validations.validateIsNotNull(filter.getIdAnnulationHolderPk())) {
			parameters.put("numberRequest", filter.getIdAnnulationHolderPk());
		}
		if (Validations.validateIsNotNull(filter.getHolderSelected()) && Validations.validateIsNotNull(filter.getHolderSelected().getIdHolderPk())) {
			parameters.put("idHolder", filter.getHolderSelected().getIdHolderPk());
		}
		if (Validations.validateIsNotNull(filter.getStateAnnulation())) {
			parameters.put("stateHolderAnnulation", filter.getStateAnnulation());
		}

		lst = findListByQueryString(sd.toString(), parameters);
		return lst;
	}
	
	/**
	 * Metodo para obtener lista de Titulares
	 *
	 * @param holderFilter the holder filter
	 * @return the list holder request service bean
	 */
	@SuppressWarnings("unchecked")
	public List<HolderRequest> getListHolderRequestServiceBean(HolderRequestTO holderFilter){
		
		StringBuilder stringBuilderSql = new StringBuilder();
		
		if(holderFilter.getRequestTypeAux() == null){
		
			stringBuilderSql.append("SELECT h FROM HolderRequest h");
			stringBuilderSql.append(" LEFT JOIN h.participant ");
			stringBuilderSql.append(" WHERE h.requestType = :requestType");				
		}
		else if(holderFilter.getRequestTypeAux()!=null){
	      stringBuilderSql.append("SELECT h FROM HolderRequest h ");
	      stringBuilderSql.append(" LEFT JOIN h.participant ");
	      stringBuilderSql.append(" INNER JOIN h.holder ");
	     
	      stringBuilderSql.append(" WHERE ( h.requestType = :requestType OR h.requestType = :requestTypeAux )");
		}
		
		if(holderFilter.getDocumentType()!=null){
			stringBuilderSql.append(" and ( (h.holder.documentType = :documentType and h.holder.documentNumber = :documentNumber)");
			stringBuilderSql.append(" or  (h.holder.secondDocumentType = :documentType and h.holder.secondDocumentNumber = :documentNumber) )");;
		}
		
		if(holderFilter.getNames()!=null){
			stringBuilderSql.append(" and h.holder.name like :name ");
		}		
		if(holderFilter.getFirstLastName()!=null){
			   stringBuilderSql.append("and h.holder.firstLastName like :firstLastName ");
		}
		if(holderFilter.getSecondLastName()!=null){
			   stringBuilderSql.append("and h.holder.secondLastName like :secondLastName ");
		}
		if(holderFilter.getBusinessName()!=null){
			stringBuilderSql.append("and h.holder.fullName like :fullName ");
	    }
		
		if(holderFilter.getNumberRequest()!=null){
		stringBuilderSql.append(" and h.idHolderRequestPk = :requestNumber ");
		}
		
		if(holderFilter.getStateRequest()!=null){
		stringBuilderSql.append(" and h.stateHolderRequest = :stateHolderRequest ");
		}
		
		if(holderFilter.getRequesterType()!=null){
			stringBuilderSql.append(" and h.requesterType = :requesterType ");
		}
		
		if(holderFilter.getParticipantSelected()!=null){
		    stringBuilderSql.append(" and h.participant = :participant ");
		}
		
		if(holderFilter.getInitialDate()!=null && holderFilter.getEndDate()!=null){
			stringBuilderSql.append(" and trunc(h.registryDate) between :dateIni and :dateEnd ");
			
	    }
		
		if(holderFilter.getHolder().getIdHolderPk()!=null){
			stringBuilderSql.append(" and h.holder = :holder ");
		}
		
		
		stringBuilderSql.append(" Order by h.idHolderRequestPk DESC");
		
		
		Query query = em.createQuery(stringBuilderSql.toString());
		
		if(holderFilter.getRequestTypeAux() == null){			
			query.setParameter("requestType", holderFilter.getRequestType());	  
		}		
		else if(holderFilter.getRequestTypeAux()!=null){
			query.setParameter("requestType", holderFilter.getRequestType());
			query.setParameter("requestTypeAux", holderFilter.getRequestTypeAux());
		}
		
		if(holderFilter.getDocumentType()!=null){
			query.setParameter("documentType",holderFilter.getDocumentType());
			query.setParameter("documentNumber",holderFilter.getDocumentNumber());
		}
		
		if(holderFilter.getNames()!=null){
			query.setParameter("name",holderFilter.getNames());
		}		
		if(holderFilter.getFirstLastName()!=null){
			query.setParameter("firstLastName",holderFilter.getFirstLastName());
		}
		if(holderFilter.getSecondLastName()!=null){
			query.setParameter("secondLastName",holderFilter.getSecondLastName());
		}
		if(holderFilter.getBusinessName()!=null){
			query.setParameter("fullName",holderFilter.getBusinessName());
	    }
		
		if(holderFilter.getNumberRequest()!=null){
			query.setParameter("requestNumber",holderFilter.getNumberRequest());	
		}
		
		if(holderFilter.getStateRequest()!=null){
			query.setParameter("stateHolderRequest",holderFilter.getStateRequest());
		}
		
		if(holderFilter.getRequesterType()!=null){
		
			query.setParameter("requesterType",holderFilter.getRequesterType());
		}
		
		if(holderFilter.getParticipantSelected()!=null){
			Participant participant = new Participant();
			participant.setIdParticipantPk(holderFilter.getParticipantSelected());
			query.setParameter("participant", participant);
		}
		
		
		if(holderFilter.getInitialDate()!=null && holderFilter.getEndDate()!=null){
			query.setParameter("dateIni",holderFilter.getInitialDate());
			query.setParameter("dateEnd",holderFilter.getEndDate());
		}
		
		if(holderFilter.getHolder()!=null && holderFilter.getHolder().getIdHolderPk()!=null){
			Holder holder = new Holder();
			holder.setIdHolderPk(holderFilter.getHolder().getIdHolderPk());
			query.setParameter("holder",holder);
		}
		
		return (List<HolderRequest>)query.getResultList();
	}
	
	/**
	 * Confirma titular del Service Bean
	 *
	 * @param holderRequest the holder request
	 * @param holderHistory the holder history
	 * @param loggerUser the logger user
	 * @return the holder
	 * @throws ServiceException the service exception
	 */
	public Holder confirmHolderRequestServiceBean(
			HolderRequest holderRequest,HolderHistory holderHistory,
			LoggerUser loggerUser) throws ServiceException{ 
			
		Holder holder = new Holder();
		
		 try{
	     boolean flagTransaction = false;
		 HolderRequest auxHolderRequest = new HolderRequest();
		 HolderHistory auxHolderHistory = new HolderHistory();
		 List<LegalRepresentativeHistory> lstLegalRepresentativeHistory = new ArrayList<LegalRepresentativeHistory>();
		 
		 auxHolderRequest =  getHolderRequesByIdRequestServiceBean(holderRequest.getIdHolderRequestPk());
		 auxHolderHistory = auxHolderRequest.getHolderHistories().get(0);
		 lstLegalRepresentativeHistory=auxHolderRequest.getLegalRepresentativeHistories();
		 
		 LegalRepresentative legalRepresentative = new LegalRepresentative();
		 
		 List<LegalRepresentative> lstLegalRepresentative = new ArrayList<LegalRepresentative>();
		 
		 List<LegalRepresentativeFile> lstLegalRepresentativeFile = new ArrayList<LegalRepresentativeFile>();
		 List<HolderFile>	lstHolderFile = new ArrayList<HolderFile>();
		 Participant participant = new Participant();
		 participant = auxHolderRequest.getParticipant();
		 List<PepPerson> lstPepPerson = new ArrayList<PepPerson>();
		 List<RepresentedEntity> lstRepresentedEntity = new ArrayList<RepresentedEntity>();
		 
		 RepresentedEntity representedEntity = new RepresentedEntity();
		 
		 boolean exist = false;
		 //lista de Representastes Legales
		 if(lstLegalRepresentativeHistory!=null && lstLegalRepresentativeHistory.size()>0){
		 
			for (int i = 0; i < lstLegalRepresentativeHistory.size(); i++) {
				
				legalRepresentative = new LegalRepresentative();
				
				lstLegalRepresentativeFile  = new ArrayList<LegalRepresentativeFile>();
				
				legalRepresentative.setDocumentNumber(lstLegalRepresentativeHistory.get(i).getDocumentNumber());
				legalRepresentative.setDocumentType(lstLegalRepresentativeHistory.get(i).getDocumentType());
				legalRepresentative.setNationality(lstLegalRepresentativeHistory.get(i).getNationality());
				if(lstLegalRepresentativeHistory.get(i).getDocumentSource()!=null){
					legalRepresentative.setDocumentSource(lstLegalRepresentativeHistory.get(i).getDocumentSource());
				}
				if(lstLegalRepresentativeHistory.get(i).getFirstLastName()!=null){
					legalRepresentative.setFirstLastName(lstLegalRepresentativeHistory.get(i).getFirstLastName());
				}
				
				LegalRepresentative auxLR = accountsFacade.getLegalRepresentativeByDocumentTypeAndDocumentNumberServiceFacade(legalRepresentative);
				
				if(auxLR!=null){
					exist = true;
				}else{
					exist = false;
				}
				//Setemos los datos
				legalRepresentative.setBirthDate(lstLegalRepresentativeHistory.get(i).getBirthDate());
				legalRepresentative.setEconomicActivity(lstLegalRepresentativeHistory.get(i).getEconomicActivity());
				legalRepresentative.setEconomicSector(lstLegalRepresentativeHistory.get(i).getEconomicSector());
				legalRepresentative.setEmail(lstLegalRepresentativeHistory.get(i).getEmail());
				legalRepresentative.setFaxNumber(lstLegalRepresentativeHistory.get(i).getFaxNumber());
				legalRepresentative.setFirstLastName(lstLegalRepresentativeHistory.get(i).getFirstLastName());
				legalRepresentative.setFullName(lstLegalRepresentativeHistory.get(i).getFullName());
				legalRepresentative.setHomePhoneNumber(lstLegalRepresentativeHistory.get(i).getHomePhoneNumber());
				legalRepresentative.setIndResidence(lstLegalRepresentativeHistory.get(i).getIndResidence());
				legalRepresentative.setLegalAddress(lstLegalRepresentativeHistory.get(i).getLegalAddress());
				legalRepresentative.setLegalDepartment(lstLegalRepresentativeHistory.get(i).getLegalDepartment());
				legalRepresentative.setLegalDistrict(lstLegalRepresentativeHistory.get(i).getLegalDistrict());
				legalRepresentative.setLegalProvince(lstLegalRepresentativeHistory.get(i).getLegalProvince());
				legalRepresentative.setLegalResidenceCountry(lstLegalRepresentativeHistory.get(i).getLegalResidenceCountry());
				legalRepresentative.setMobileNumber(lstLegalRepresentativeHistory.get(i).getMobileNumber());
				legalRepresentative.setName(lstLegalRepresentativeHistory.get(i).getName());
				legalRepresentative.setNationality(lstLegalRepresentativeHistory.get(i).getNationality());
				legalRepresentative.setOfficePhoneNumber(lstLegalRepresentativeHistory.get(i).getOfficePhoneNumber());
				legalRepresentative.setPersonType(lstLegalRepresentativeHistory.get(i).getPersonType());
				legalRepresentative.setPostalAddress(lstLegalRepresentativeHistory.get(i).getPostalAddress());		
				legalRepresentative.setPostalDepartment(lstLegalRepresentativeHistory.get(i).getPostalDepartment());
				legalRepresentative.setPostalDistrict(lstLegalRepresentativeHistory.get(i).getPostalDistrict());
				legalRepresentative.setPostalProvince(lstLegalRepresentativeHistory.get(i).getPostalProvince());
				legalRepresentative.setPostalResidenceCountry(lstLegalRepresentativeHistory.get(i).getPostalResidenceCountry());
				legalRepresentative.setSecondDocumentNumber(lstLegalRepresentativeHistory.get(i).getSecondDocumentNumber());
				legalRepresentative.setSecondDocumentType(lstLegalRepresentativeHistory.get(i).getSecondDocumentType());
				legalRepresentative.setSecondLastName(lstLegalRepresentativeHistory.get(i).getSecondLastName());
				legalRepresentative.setSecondNationality(lstLegalRepresentativeHistory.get(i).getSecondNationality());
				legalRepresentative.setSex(lstLegalRepresentativeHistory.get(i).getSex());
				legalRepresentative.setState(LegalRepresentativeStateType.REGISTERED.getCode());
				legalRepresentative.setRegistryDate(CommonsUtilities.currentDate());
				legalRepresentative.setRegistryUser(loggerUser.getUserName());
				legalRepresentative.setDocumentSource(lstLegalRepresentativeHistory.get(i).getDocumentSource());
				legalRepresentative.setDocumentIssuanceDate(lstLegalRepresentativeHistory.get(i).getDocumentIssuanceDate());
				
				if (Validations.validateListIsNotNullAndNotEmpty(lstLegalRepresentativeHistory.get(i).getRepresenteFileHistory())) {

					for (int k = 0; k < lstLegalRepresentativeHistory.get(i).getRepresenteFileHistory().size(); k++) {
						
						LegalRepresentativeFile auxLegalRepresentativeFile = new LegalRepresentativeFile();
						
						auxLegalRepresentativeFile.setDescription(lstLegalRepresentativeHistory.get(i).getRepresenteFileHistory()
										.get(k).getDescription());
						auxLegalRepresentativeFile.setDocumentType(lstLegalRepresentativeHistory.get(i).getRepresenteFileHistory()
										.get(k).getDocumentType());
						auxLegalRepresentativeFile.setFilename(lstLegalRepresentativeHistory.get(i).getRepresenteFileHistory()
										.get(k).getFilename());						
						
						RepresentativeFileHistory representativeFileHistory = accountsFacade.getContentFileRepresentativeHistoryServiceFacade(
								lstLegalRepresentativeHistory.get(i).getRepresenteFileHistory().get(k).getIdRepresentFileHisPk());
						
						representativeFileHistory.getFileRepresentative();
						
						auxLegalRepresentativeFile.setFileLegalRepre(representativeFileHistory.getFileRepresentative());					
												
						auxLegalRepresentativeFile.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST
										.getCode());
						auxLegalRepresentativeFile.setStateFile(HolderFileStateType.REGISTERED
										.getCode());
						auxLegalRepresentativeFile.setLegalRepreFileType(HolderReqFileHisType.CREATION
										.getCode());
						auxLegalRepresentativeFile.setLegalRepresentative(legalRepresentative);		
						auxLegalRepresentativeFile.setRegistryDate(CommonsUtilities.currentDateTime());
						auxLegalRepresentativeFile.setRegistryUser(loggerUser.getUserName());
						lstLegalRepresentativeFile.add(auxLegalRepresentativeFile);
						
					}

				}

				if(!exist && lstLegalRepresentativeFile!=null && lstLegalRepresentativeFile.size()>0){
					legalRepresentative.setLegalRepresentativeFile(lstLegalRepresentativeFile);
				}
				if(!exist){
					lstLegalRepresentative.add(legalRepresentative);	
				}
				

				representedEntity = new RepresentedEntity();
				representedEntity.setHolder(holder);
				
				if(!exist){
					representedEntity.setLegalRepresentative(legalRepresentative);	
					
				}
				else{
					representedEntity.setLegalRepresentative(auxLR);
				}
				
				representedEntity.setRepresentativeClass(lstLegalRepresentativeHistory.get(i).getRepresentativeClass());
				representedEntity.setStateRepresented(RepresentativeEntityStateType.REGISTERED.getCode());

				representedEntity.setRegistryDate(CommonsUtilities.currentDate());
				representedEntity.setRegistryUser(loggerUser.getUserName());

				lstRepresentedEntity.add(representedEntity);
				// si Ind PEP es SI
				if ((lstLegalRepresentativeHistory.get(i).getIndPEP() != null)
						&& (lstLegalRepresentativeHistory.get(i)
								.getIndPEP().equals(BooleanType.YES
								.getCode()))) {
					
					
					LegalRepresentative auxlgR = new LegalRepresentative();
					auxlgR.setDocumentType(lstLegalRepresentativeHistory.get(i).getDocumentType());
					auxlgR.setDocumentNumber(lstLegalRepresentativeHistory.get(i).getDocumentNumber());
					if(lstLegalRepresentativeHistory.get(i).getDocumentSource()!=null){
						auxlgR.setDocumentSource(lstLegalRepresentativeHistory.get(i).getDocumentSource());
					}
					if(lstLegalRepresentativeHistory.get(i).getFirstLastName()!=null){
						auxlgR.setFirstLastName(lstLegalRepresentativeHistory.get(i).getFirstLastName());
					}
					
					auxlgR = accountsFacade.getLegalRepresentativeByDocumentTypeAndDocumentNumberServiceFacade(auxlgR);
					
					PepPerson auxPepPerson = new PepPerson();
					
					if(auxlgR!=null){
						auxPepPerson = getPepPersonByIdLegalRepresentativeServiceBean(auxlgR.getIdLegalRepresentativePk());
					}
					//Creamos la persona si no existe
					if(auxPepPerson.getIdPepPersonPk()==null){
					
					PepPerson pepPerson = new PepPerson();
					pepPerson.setNotificationDate(CommonsUtilities.currentDate());
					pepPerson.setBeginingPeriod(lstLegalRepresentativeHistory.get(i).getBeginningPeriod());
					pepPerson.setEndingPeriod(lstLegalRepresentativeHistory.get(i).getEndingPeriod());
					pepPerson.setCategory(lstLegalRepresentativeHistory.get(i).getCategory());
					pepPerson.setRole(lstLegalRepresentativeHistory.get(i).getRole());
					GeographicLocation geo = new GeographicLocation();
					geo.setIdGeographicLocationPk(auxHolderHistory.getLegalResidenceCountry());
					pepPerson.setGeographicLocation(geo);
					pepPerson.setRegistryDate(CommonsUtilities.currentDate());
					pepPerson.setRegistryUser(loggerUser.getUserName());
					pepPerson.setLegalRepresentative(legalRepresentative);
					pepPerson.setDocumentNumber(legalRepresentative.getDocumentNumber());
					pepPerson.setDocumentType(legalRepresentative.getDocumentType());
					pepPerson.setClassification(PepPersonClassificationType.HIGH.getCode());
					pepPerson.setExpeditionPlace(geo);
					
					if(lstLegalRepresentativeHistory.get(i).getPersonType().equals(PersonType.NATURAL.getCode())){
						String lastName="";
						pepPerson.setFirstName(lstLegalRepresentativeHistory.get(i).getName());
						if(lstLegalRepresentativeHistory.get(i).getSecondLastName()==null){
							lastName = lstLegalRepresentativeHistory.get(i).getFirstLastName();
						}
						else{
							lastName = lstLegalRepresentativeHistory.get(i).getFirstLastName()+GeneralConstants.BLANK_SPACE+lstLegalRepresentativeHistory.get(i).getSecondLastName();
						}
						pepPerson.setLastName(lastName);
					}
					else if(lstLegalRepresentativeHistory.get(i).getPersonType().equals(PersonType.JURIDIC.getCode())){
						pepPerson.setFirstName(lstLegalRepresentativeHistory.get(i).getFullName());
					}
					

					lstPepPerson.add(pepPerson);
					
					}
					
				}

			}
		 }
		 	//Si el Holder
			if ((auxHolderHistory.getIndPEP() != null)
					&& auxHolderHistory.getIndPEP().equals(
							BooleanType.YES.getCode())) {
				
				PepPerson pepPerson = new PepPerson();
				pepPerson.setNotificationDate(CommonsUtilities.currentDate());
				pepPerson.setBeginingPeriod(auxHolderHistory.getBeginningPeriod());
				pepPerson.setEndingPeriod(auxHolderHistory.getEndingPeriod());
				pepPerson.setCategory(auxHolderHistory.getCategory());
				pepPerson.setRole(auxHolderHistory.getRole());
				pepPerson.setGrade(auxHolderHistory.getGrade());
				pepPerson.setPepRelatedName(auxHolderHistory.getPepRelatedName());
				GeographicLocation geo = new GeographicLocation();
				geo.setIdGeographicLocationPk(auxHolderHistory.getLegalResidenceCountry());
				pepPerson.setGeographicLocation(geo);
				pepPerson.setRegistryDate(CommonsUtilities.currentDate());
				pepPerson.setRegistryUser(loggerUser.getUserName());
				pepPerson.setHolder(holder);
				pepPerson.setDocumentNumber(auxHolderHistory.getDocumentNumber());
				pepPerson.setDocumentType(auxHolderHistory.getDocumentType());
				pepPerson.setClassification(PepPersonClassificationType.HIGH.getCode());
				pepPerson.setExpeditionPlace(geo);
				
				if(auxHolderHistory.getHolderType().equals(PersonType.NATURAL.getCode())){
					String lastName="";
					pepPerson.setFirstName(auxHolderHistory.getName());
					
					if(auxHolderHistory.getSecondLastName()==null){
						lastName = auxHolderHistory.getFirstLastName();
					}
					else{
						lastName = auxHolderHistory.getFirstLastName()+GeneralConstants.BLANK_SPACE+auxHolderHistory.getSecondLastName();
					}
					
					pepPerson.setLastName(lastName);
				}
				if(auxHolderHistory.getHolderType().equals(PersonType.JURIDIC.getCode())){
					pepPerson.setFirstName(auxHolderHistory.getFullName());
				}

				lstPepPerson.add(pepPerson);
			}
			

			holder.setBirthDate(auxHolderHistory.getBirthDate());
			holder.setDocumentNumber(auxHolderHistory.getDocumentNumber());
			holder.setDocumentType(auxHolderHistory.getDocumentType());
			holder.setEconomicActivity(auxHolderHistory.getEconomicActivity());
			holder.setInvestorType(auxHolderHistory.getInvestorType());
			holder.setEconomicSector(auxHolderHistory.getEconomicSector());
			holder.setEmail(auxHolderHistory.getEmail());
			holder.setFaxNumber(auxHolderHistory.getFaxNumber());
			holder.setFirstLastName(auxHolderHistory.getFirstLastName());
			
			String descriptionHolder="";
			if(auxHolderHistory.getHolderType().equals(PersonType.NATURAL.getCode())){
				if(auxHolderHistory.getSecondLastName()!=null){
				    descriptionHolder=auxHolderHistory.getFirstLastName()+GeneralConstants.BLANK_SPACE+auxHolderHistory.getSecondLastName()+GeneralConstants.STR_COMMA+auxHolderHistory.getName();
				}
				else{
					descriptionHolder=auxHolderHistory.getFirstLastName()+GeneralConstants.STR_COMMA+auxHolderHistory.getName();
				}
				
				holder.setFullName(descriptionHolder.trim());
			}else{			
			holder.setFullName(auxHolderHistory.getFullName());
			}
			holder.setHolderType(auxHolderHistory.getHolderType());
			holder.setHomePhoneNumber(auxHolderHistory.getHomePhoneNumber());
			holder.setIndDisabled(auxHolderHistory.getIndDisabled());
			holder.setIndMinor(auxHolderHistory.getIndMinor());
			holder.setIndResidence(auxHolderHistory.getIndResidence());
			holder.setJuridicClass(auxHolderHistory.getJuridicClass());
			holder.setLegalAddress(auxHolderHistory.getLegalAddress());
			holder.setLegalDepartment(auxHolderHistory.getLegalDepartment());
			holder.setLegalDistrict(auxHolderHistory.getLegalDistrict());
			holder.setLegalProvince(auxHolderHistory.getLegalProvince());
			holder.setLegalResidenceCountry(auxHolderHistory.getLegalResidenceCountry());
			holder.setMobileNumber(auxHolderHistory.getMobileNumber());
			holder.setName(auxHolderHistory.getName());
			holder.setNationality(auxHolderHistory.getNationality());
			holder.setOfficePhoneNumber(auxHolderHistory.getOfficePhoneNumber());
			holder.setPostalAddress(auxHolderHistory.getPostalAddress());
			holder.setPostalDepartment(auxHolderHistory.getPostalDepartment());
			holder.setPostalDistrict(auxHolderHistory.getPostalDistrict());
			holder.setPostalProvince(auxHolderHistory.getPostalProvince());
			holder.setPostalResidenceCountry(auxHolderHistory.getPostalResidenceCountry());
			holder.setSecondDocumentNumber(auxHolderHistory.getSecondDocumentNumber());
			holder.setSecondDocumentType(auxHolderHistory.getSecondDocumentType());
			holder.setSecondLastName(auxHolderHistory.getSecondLastName());
			holder.setSecondNationality(auxHolderHistory.getSecondNationality());
			holder.setSex(auxHolderHistory.getSex());
			holder.setStateHolder(HolderStateType.REGISTERED.getCode());

			holder.setRegistryDate(CommonsUtilities.currentDate());
			holder.setRegistryUser(loggerUser.getUserName());
			holder.setParticipant(participant);
			holder.setDocumentSource(auxHolderHistory.getDocumentSource());
			holder.setDocumentIssuanceDate(auxHolderHistory.getDocumentIssuanceDate());	
			holder.setRelatedCui(auxHolderHistory.getRelatedCui());
			holder.setFundAdministrator(auxHolderHistory.getFundAdministrator());
			holder.setFundType(auxHolderHistory.getFundType());
			holder.setTransferNumber(auxHolderHistory.getTransferNumber());
			holder.setMnemonic(auxHolderHistory.getMnemonic());
			holder.setMarriedLastName(auxHolderHistory.getMarriedLastName());
		    holder.setCivilStatus(auxHolderHistory.getCivilStatus());
			holder.setMainActivity(auxHolderHistory.getMainActivity());
		    holder.setJobSourceBusinessName(auxHolderHistory.getJobSourceBusinessName());
		    holder.setJobDateAdmission(auxHolderHistory.getJobDateAdmission());
		    holder.setAppointment(auxHolderHistory.getAppointment());
		    holder.setJobAddress(auxHolderHistory.getJobAddress());
		    holder.setTotalIncome(auxHolderHistory.getTotalIncome());
		    holder.setPerRefFullName(auxHolderHistory.getPerRefFullName());
		    holder.setPerRefLandLine(auxHolderHistory.getPerRefLandLine());
		    holder.setPerRefCellPhone(auxHolderHistory.getPerRefCellPhone());
		    holder.setComerRefBusinessName(auxHolderHistory.getComerRefBusinessName());
		    holder.setComerRefLandLine(auxHolderHistory.getComerRefLandLine());
		    holder.setEntityPublicAppointment(auxHolderHistory.getEntityPublicAppointment());
		    holder.setNitNaturalPerson(auxHolderHistory.getNitNaturalPerson());
		    holder.setWebsite(auxHolderHistory.getWebsite());
		    holder.setFundCompanyEnrollment(auxHolderHistory.getFundCompanyEnrollment());
		    holder.setConstitutionDate(auxHolderHistory.getConstitutionDate());
		    holder.setIndCanNegotiate(auxHolderHistory.getIndCanNegotiate());
		    
		    holder.setFilePhoto(auxHolderHistory.getFilePhoto());
		    holder.setFileSigning(auxHolderHistory.getFileSigning());
		    holder.setIndSirtexNeg(auxHolderHistory.getIndSirtexNeg());
			holder.setIndFatca(auxHolderHistory.getIndFatca());
			holder.setFilePhotoName(auxHolderHistory.getFilePhotoName());
		    
		    if (auxHolderRequest.getHolderReqFileHistories()!=null && auxHolderRequest.getHolderReqFileHistories().size() > 0) {

				for (int i = 0; i < auxHolderRequest
						.getHolderReqFileHistories().size(); i++) {
					HolderFile auxHolderFile = new HolderFile();
					auxHolderFile.setDescription(auxHolderRequest.getHolderReqFileHistories().get(i).getDescription());
					auxHolderFile.setDocumentType(auxHolderRequest.getHolderReqFileHistories().get(i).getDocumentType());
					
					HolderReqFileHistory holderReqFileHistory = getContentFileHolderHistoryServiceBean(auxHolderRequest.getIdHolderRequestPk(),
							auxHolderRequest.getHolderReqFileHistories().get(i).getDocumentType(),false);
					
					holderReqFileHistory.getFileHolderReq();					
					auxHolderFile.setFileHolder(holderReqFileHistory.getFileHolderReq());					
					
					auxHolderFile.setFilename(auxHolderRequest.getHolderReqFileHistories().get(i).getFilename());
					auxHolderFile.setHolder(holder);
					auxHolderFile.setHolderFileType(HolderReqFileHisType.CREATION.getCode());
					auxHolderFile.setRegistryDate(CommonsUtilities.currentDate());
					auxHolderFile.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
					auxHolderFile.setStateFile(HolderFileStateType.REGISTERED.getCode());
					auxHolderFile.setRegistryUser(loggerUser.getUserName());
					lstHolderFile.add(auxHolderFile);
					
				}
				
			}
			
			if(lstHolderFile!=null && lstHolderFile.size()>0){
				holder.setHolderFile(lstHolderFile);
			}

			if (lstPepPerson.size() > 0) {
				holder.setPepPerson(lstPepPerson);
			}
			holder.setRepresentedEntity(lstRepresentedEntity);		
			
			if(holderControlBean.validaExistHolder(holderRequest,lstLegalRepresentative,holder)){
				holder=null;
				throw new ServiceException(ErrorServiceType.HOLDER_EXIST);    	
			}else{
				 
			  if (holder!=null && holder.getIdHolderPk()!=null) {
			    	
			    HolderRequest finalHolderRequest = new HolderRequest();
				HolderHistory finalHolderHistory = new HolderHistory();
				List<LegalRepresentativeHistory> FinalLstLegalRepresentativeHistory = new ArrayList<LegalRepresentativeHistory>();
			   
				finalHolderRequest = holderRequest;
				finalHolderHistory = holderHistory;
				
				finalHolderRequest.setHolder(holder);
				finalHolderRequest.setStateHolderRequest(HolderRequestStateType.CONFIRMED.getCode());
				finalHolderRequest.setConfirmDate(CommonsUtilities.currentDate());
				finalHolderRequest.setConfirmUser(loggerUser.getUserName());
				finalHolderHistory.setStateHolderHistory(HolderRequestStateType.CONFIRMED.getCode());
				
				FinalLstLegalRepresentativeHistory = finalHolderRequest.getLegalRepresentativeHistories();

				if(FinalLstLegalRepresentativeHistory!=null && FinalLstLegalRepresentativeHistory.size()>0){
				
					for (int i = 0; i < FinalLstLegalRepresentativeHistory.size(); i++) {
						FinalLstLegalRepresentativeHistory.get(i).setStateRepreHistory(LegalRepresentativeStateType.CONFIRMED.getCode());
					}
				}
				List<HolderHistory> lstHolderHistory = new ArrayList<HolderHistory>();
				lstHolderHistory.add(finalHolderHistory);

				finalHolderRequest.setHolderHistories(lstHolderHistory);
				if(FinalLstLegalRepresentativeHistory!=null && FinalLstLegalRepresentativeHistory.size()>0){
					finalHolderRequest.setLegalRepresentativeHistories(FinalLstLegalRepresentativeHistory);
				}
				
				List<HolderHistoryState> lstHolderHistoryState = new ArrayList<HolderHistoryState>();
				HolderHistoryState holderHistoryState = new HolderHistoryState();
				
				holderHistoryState.setHolder(holder);
				holderHistoryState.setHolderRequest(finalHolderRequest);
				holderHistoryState.setRegistryUser(loggerUser.getUserName());
				holderHistoryState.setOldState(StateHistoryStateHolderType.REGISTERED.getCode());
				holderHistoryState.setNewState(StateHistoryStateHolderType.REGISTERED.getCode());
				holderHistoryState.setUpdateStateDate(CommonsUtilities.currentDate());
				lstHolderHistoryState.add(holderHistoryState);				
				
				finalHolderRequest.setHolderHistoryStates(lstHolderHistoryState);
				
				update(finalHolderRequest);
				flagTransaction = true;

			  }
		 }
		 if(!flagTransaction){			  
		    holder=null;
		 }
		 }
		 catch(Exception ex){
			 ex.printStackTrace();
			   if (ex instanceof com.pradera.integration.exception.ServiceException) {
			    ServiceException se= (ServiceException) ex;
			    throw se;
			   }
			   throw new com.pradera.integration.exception.ServiceException(ErrorServiceType.WEB_SERVICE_ERROR_PROCESS);
		 }
		 return holder;
	}
	
	/**
	 * Metodo que Registra Titular request service bean.
	 *
	 * @param holderRequest the holder request
	 * @param participant the participant
	 * @param holderHistory the holder history
	 * @param lstLegalRepresentativeHistories the lst legal representative histories
	 * @param lstHolderReqFileHistory the lst holder req file history
	 * @param loggerUser the logger user
	 * @return the holder request
	 * @throws ServiceException the service exception
	 */
	public HolderRequest registerHolderRequestServiceBean(
			HolderRequest holderRequest,
			Participant participant,
			HolderHistory holderHistory,
			List<LegalRepresentativeHistory> lstLegalRepresentativeHistories,
			List<HolderReqFileHistory> lstHolderReqFileHistory,
			LoggerUser loggerUser) throws ServiceException{
		
		if (Validations.validateListIsNotNullAndNotEmpty(lstLegalRepresentativeHistories)){
			holderRequest.setLegalRepresentativeHistories(lstLegalRepresentativeHistories);
		}

		holderRequest.setRegistryDate(CommonsUtilities.currentDate());
		holderRequest.setRegistryUser(loggerUser.getUserName());
		holderRequest.setStateHolderRequest(HolderRequestStateType.REGISTERED.getCode());
		holderRequest.setParticipant(participant);
		holderRequest.setRequestType(HolderRequestType.CREATION.getCode());
		holderHistory.setRegistryDate(CommonsUtilities.currentDate());
		
		// issue 1063 verificando si se trat de de una persona Natural
		// si es NATURAL, entones se setea la actividaEconomica PARTICULARES (PERSONAS NATURALES)
		if( Validations.validateIsNotNull(holderHistory.getHolderType()) && holderHistory.getHolderType().equals(PersonType.NATURAL.getCode())){
			// ACTIVIDAD ECONOMICA:  1877 PARTICULARES (PERSONAS NATURALES)
			//holderHistory.setEconomicActivity(EconomicActivityType.PERSONAS_NATURALES.getCode());
			holderHistory.setInvestorType(investorTypeServiceBean.getFirtsIdInvestorTypeByEconomicActivity(EconomicActivityType.PERSONAS_NATURALES.getCode()));
		}
		
		holderHistory.setRegistryUser(loggerUser.getUserName());
		holderHistory.setStateHolderHistory(HolderRequestStateType.REGISTERED.getCode());
		holderHistory.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
		holderHistory.setIdHolderHistoryPk(null);
		holderHistory.setHolderRequest(holderRequest);
		holderHistory.setHolderHistoryType(HolderRequestType.CREATION.getCode());
		
		if(Validations.validateIsNull(holderHistory.getIndSirtexNeg())){
			holderHistory.setIndSirtexNeg(BooleanType.YES.getCode());
		}
		
		if(Validations.validateIsNullOrEmpty(holderHistory.getIndPEP())){
			holderHistory.setIndPEP(GeneralConstants.ZERO_VALUE_INTEGER);
		}
		if(holderHistory.getSecondNationality()==null){
			holderHistory.setSecondDocumentType(null);
			holderHistory.setSecondDocumentNumber(null);
		}
		if(holderHistory.getSecondDocumentType()==null){
			holderHistory.setSecondDocumentNumber(null);
		}
		if(holderHistory.getSecondDocumentNumber()==null){
			holderHistory.setSecondDocumentType(null);
		}
		if (holderHistory.getHolderType().equals(PersonType.JURIDIC.getCode())) {
			if(holderHistory.getJuridicClass()!=null && holderHistory.getJuridicClass().equals(JuridicClassType.TRUST.getCode())){
				holderHistory.setDocumentNumber(holderHistory.getDocumentNumber());
			}
			holderHistory.setIndMinor(BooleanType.NO.getCode());
			holderHistory.setIndDisabled(BooleanType.NO.getCode());
		}
		if(Validations.validateIsNotNull(holderHistory.getEmail())){
			holderHistory.setEmail(holderHistory.getEmail().toLowerCase());
		}
		List<HolderHistory> lstHolderHistory = new ArrayList<HolderHistory>();
		lstHolderHistory.add(holderHistory);
		holderRequest.setHolderHistories(lstHolderHistory);
		if(Validations.validateListIsNotNullAndNotEmpty(lstHolderReqFileHistory)){
			holderRequest.setHolderReqFileHistories(lstHolderReqFileHistory);
		}
		if(holderRequest.getRequestType().equals(HolderRequestType.BLOCK.getCode()) || 
			 holderRequest.getRequestType().equals(HolderRequestType.UNBLOCK.getCode())){
			holderRequest.setRequestNumber(getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_BLOCK_UNBLOCK_RNT));
		}
		else{			 
			holderRequest.setRequestNumber(getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_REGISTER_RNT));		 
		}
		holderRequest = holderControlBean.validaExistHolderRequest(holderRequest);
	 
		if(Validations.validateIsNull(holderRequest.getIdHolderRequestPk())){
			throw new ServiceException(ErrorServiceType.HOLDER_REQUEST_EXIST);    	
		}
		return holderRequest;
	}
	
	/**
	 * Obtener el Titular de service bean.
	 *
	 * @param idHolderRequest the id holder request
	 * @return the holder request service bean
	 */
	@SuppressWarnings("unchecked")
	public HolderRequest getHolderRequestServiceBean(Long idHolderRequest){
		Map<String,Object> parameters = new HashMap<String, Object>();
    	parameters.put("idHolderRequestPrm", idHolderRequest);
    	List<HolderRequest> holderRequestList = findWithNamedQuery(HolderRequest.HOLDERREQUEST_SEARCH_BY_ID, parameters);
    	if(holderRequestList!= null && holderRequestList.size()>0){
 	    	return holderRequestList.get(0);
 	    }
 	    else{
 	    	return new HolderRequest();
 	    }
	}
	
	/**
	 * Gets the request information service bean.
	 *
	 * @param holderRequest the holder request
	 * @return the request information service bean
	 */
	public HolderRequest getRequestInformationServiceBean(HolderRequest holderRequest){
		StringBuilder stringBuilderSql = new StringBuilder();
		
		stringBuilderSql.append("SELECT h FROM HolderRequest h WHERE h.holder.idHolderPk = :idHolderPk ");
		stringBuilderSql.append(" and ( h.holder.stateHolder = :stateRegistered or h.holder.stateHolder =:stateBlocked ) ");
	    
		Query query = em.createQuery(stringBuilderSql.toString());
		query.setParameter("idHolderPk", holderRequest.getHolder().getIdHolderPk());
		query.setParameter("stateRegistered",holderRequest.getHolder().getStateHolder());
 		query.setParameter("stateBlocked",holderRequest.getHolder().getStateHolderAux());
		
		
		return (HolderRequest)query.getSingleResult();	
	}
	
	public HolderRequest getHolderInformationServiceBean(Long idHolderRequest){
		StringBuilder stringBuilderSql = new StringBuilder();
		
		stringBuilderSql.append("SELECT h FROM HolderRequest h WHERE h.idHolderRequestPk = :idHolderRequest ");
	    
		Query query = em.createQuery(stringBuilderSql.toString());
		query.setParameter("idHolderRequest", idHolderRequest);
		
		HolderRequest holdertRequest = (HolderRequest) query.getSingleResult();
		try {
			holdertRequest.setHolderReqFileHistories(listHolderFileByRequest(idHolderRequest));
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return (HolderRequest)query.getSingleResult();	
	}
	
	/**
	 * Gets the holder service bean.
	 *
	 * @param idHolderPk the id holder pk
	 * @return the holder service bean
	 */
	public Holder getHolderServiceBean(Long idHolderPk){
		  StringBuilder stringBuilderSql = new StringBuilder();
		  stringBuilderSql.append("SELECT h FROM Holder h WHERE h.idHolderPk = :idHolderPk");
	      
		  Query query = em.createQuery(stringBuilderSql.toString());
		  query.setParameter("idHolderPk", idHolderPk);	  
	
		 
	      return (Holder) query.getSingleResult();
		  
    }
	
	/**
	 * Gets the exist holder service bean.
	 *
	 * @param idHolderPk the id holder pk
	 * @return the exist holder service bean
	 */
	public Integer getExistHolderServiceBean(Long idHolderPk){
		StringBuilder stringBuilderSql = new StringBuilder();
		  stringBuilderSql.append("SELECT count(*) FROM Holder h WHERE h.idHolderPk = :idHolderPk");
	      
		  Query query = em.createQuery(stringBuilderSql.toString());
		  query.setParameter("idHolderPk", idHolderPk);	  
	
		 
	      return Integer.valueOf(query.getSingleResult().toString());
	}
	
	
	/**
	 * Metodo que obtiene la solicitud del titular por Id service bean.
	 *
	 * @param idHolderRequest the id holder request
	 * @return the holder reques by id request service bean
	 * @throws ServiceException the service exception
	 */
	public HolderRequest getHolderRequesByIdRequestServiceBean(Long idHolderRequest) throws ServiceException{
		
		HolderRequest request =   find(HolderRequest.class, idHolderRequest);
        
		request.getParticipant().getIdParticipantPk();
		
    	 if(request.getHolder()!=null){
    		 request.getHolder().getIdHolderPk();
    		 request.getHolder().getParticipant().getIdParticipantPk();
    	 }
    	 //
    	 if(request.getHolder()!=null){    		 
    	
    		 if(request.getHolder().getHolderFile()!=null){
    		 
    			 if(request.getHolder().getHolderFile().size()>0){
    				 List<HolderFile> lstHolderFile;    			 
    				 lstHolderFile = request.getHolder().getHolderFile();
    				 request.getHolder().setHolderFile(null);
    				 List<HolderFile> lstHolderFileResult  = new ArrayList<HolderFile>();
    			 
    				 for(HolderFile hF:lstHolderFile){
    					 if(!hF.getStateFile().equals(HolderFileStateType.DELETED.getCode())){
    						 lstHolderFileResult.add(hF);
    					 }
    				 }
    			  
    				 request.getHolder().setHolderFile(lstHolderFileResult);
    				 request.getHolder().getHolderFile().size();
    			 }
    		 }
         }
        
         if(Validations.validateListIsNotNullAndNotEmpty(request.getHolderReqFileHistories())){
        	 request.getHolderReqFileHistories().size();
             
             List<HolderReqFileHistory> lstHolderReqFileHistory = request.getHolderReqFileHistories();
             List<HolderReqFileHistory> lstHolderReqFileHistoryResult = new ArrayList<HolderReqFileHistory>();
             
             for(HolderReqFileHistory hRFH : lstHolderReqFileHistory){
            	 if(!hRFH.getRegistryType().equals(RegistryType.HISTORICAL_COPY.getCode())){
            		 lstHolderReqFileHistoryResult.add(hRFH);
            	 }
             }
             
             if(Validations.validateListIsNotNullAndNotEmpty(lstHolderReqFileHistoryResult)){
            	 request.setHolderReqFileHistories(lstHolderReqFileHistoryResult);
             } else {
            	 request.setHolderReqFileHistories(null);
             }
         }
         
         if(Validations.validateListIsNotNullAndNotEmpty(request.getLegalRepresentativeHistories())){
        	 List<LegalRepresentativeHistory> lstLegalRepresentativeHistory;
             List<LegalRepresentativeHistory> lstLegalRepresentativeHistoryResult = new ArrayList<LegalRepresentativeHistory>();
             lstLegalRepresentativeHistory = request.getLegalRepresentativeHistories();
             
             
             for(LegalRepresentativeHistory lRH : lstLegalRepresentativeHistory){
            	 
            	 if(lRH.getRepresenteFileHistory()!=null){
            		 lRH.getRepresenteFileHistory().size();
        		 }
            	 
            	 if(!lRH.getRegistryType().equals(RegistryType.HISTORICAL_COPY.getCode())){
            		 lstLegalRepresentativeHistoryResult.add(lRH);
            	 }
             }
             
             if(Validations.validateListIsNotNullAndNotEmpty(lstLegalRepresentativeHistoryResult)){
            	 request.setLegalRepresentativeHistories(lstLegalRepresentativeHistoryResult);
             } else {
            	 request.setLegalRepresentativeHistories(null);
             }
         }
        
         List<HolderHistory> lstHolderHistory;
         List<HolderHistory> lstHolderHistoryResult = new ArrayList<HolderHistory>();
         lstHolderHistory = request.getHolderHistories();
    	 
         for(HolderHistory hH : lstHolderHistory){
        	 if(hH.getRegistryType()!=null && !hH.getRegistryType().equals(RegistryType.HISTORICAL_COPY.getCode())){        		
        		 lstHolderHistoryResult.add(hH);
        	 }
         }
         if(lstHolderHistoryResult!=null && lstHolderHistoryResult.size()>0){
        	 request.setHolderHistories(lstHolderHistoryResult);
         }
         else{
        	 request.setHolderHistories(null);
         }
         return request;
	      
	}
	
	/**
	 * Gets the holder reques by id request service bean aux.
	 *
	 * @param idHolderRequest the id holder request
	 * @return the holder reques by id request service bean aux
	 */
	public HolderRequest getHolderRequesByIdRequestServiceBeanAux(Long idHolderRequest){
		StringBuilder stringBuilderSql = new StringBuilder();
		  stringBuilderSql.append("SELECT h FROM HolderRequest h JOIN FETCH h.holderHistories JOIN FETCH h.holder WHERE h.idHolderRequestPk = :idHolderRequest");
		  
		  Query query = em.createQuery(stringBuilderSql.toString());
		  query.setParameter("idHolderRequest", idHolderRequest);
		 
	      return (HolderRequest) query.getSingleResult();		
	}
	
	/**
	 * Gets the list holder block un block request service bean.
	 *
	 * @param holderRequest the holder request
	 * @return the list holder block un block request service bean
	 */
	@SuppressWarnings("unchecked")
	public List<HolderRequest> getListHolderBlockUnBlockRequestServiceBean(HolderRequest holderRequest){
		StringBuilder stringBuilderSql = new StringBuilder();
		stringBuilderSql.append("SELECT HR FROM HolderRequest HR WHERE HR.holder.idHolderPk = :idHolderPk ");
		stringBuilderSql.append("and (HR.requestType = :requestTypeBlock or HR.requestType = :requestTypeUnBlock) ");		
		
		if(holderRequest.getStateHolderRequest()!=null){
		stringBuilderSql.append(" and HR.stateHolderRequest = :stateHolderRequest ");
		}
		
		Query query = em.createQuery(stringBuilderSql.toString());
		query.setParameter("idHolderPk", holderRequest.getHolder().getIdHolderPk());
		query.setParameter("requestTypeBlock", HolderRequestType.BLOCK.getCode());
		query.setParameter("requestTypeUnBlock", HolderRequestType.UNBLOCK.getCode());
		
		if(holderRequest.getStateHolderRequest()!=null){
		query.setParameter("stateHolderRequest", holderRequest.getStateHolderRequest());
		}
		
		return (List<HolderRequest>)query.getResultList();		
	}
	
	
	/**
	 * Gets the historial state list.
	 *
	 * @param holderRequest the holder request
	 * @return the historial state list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderRequest> getHistorialStateList(HolderRequest holderRequest) throws ServiceException{
		StringBuilder stringBuilderSql = new StringBuilder();
		stringBuilderSql.append(" SELECT HR");
		stringBuilderSql.append(" FROM HolderRequest HR");
		stringBuilderSql.append(" WHERE HR.holder.idHolderPk = :idHolderPk");
		stringBuilderSql.append(" AND (HR.requestType = :requestTypeBlock");
		stringBuilderSql.append(" OR HR.requestType = :requestTypeUnBlock)");
		stringBuilderSql.append(" AND HR.stateHolderRequest = :stateHolderRequest");		
		
		Query query = em.createQuery(stringBuilderSql.toString());
		query.setParameter("idHolderPk", holderRequest.getHolder().getIdHolderPk());
		query.setParameter("stateHolderRequest", HolderRequestStateType.CONFIRMED.getCode());
		query.setParameter("requestTypeBlock", HolderRequestType.BLOCK.getCode());
		query.setParameter("requestTypeUnBlock", HolderRequestType.UNBLOCK.getCode());	
		return (List<HolderRequest>)query.getResultList();		
	}
	
	/**
	 * Gets the holder request service bean.
	 *
	 * @param holderRequest the holder request
	 * @return the holder request service bean
	 */
	public Integer getHolderRequestServiceBean(HolderRequest holderRequest){
		StringBuilder stringBuilderSql = new StringBuilder();
		stringBuilderSql.append("SELECT count(*) FROM HolderRequest HR WHERE HR.holder.idHolderPk = :idHolderPk ");
		stringBuilderSql.append("and (HR.requestType = :requestTypeBlock or HR.requestType = :requestTypeUnBlock) and HR.stateHolderRequest = :stateHolderRequest ");
		
		Query query = em.createQuery(stringBuilderSql.toString());
		query.setParameter("idHolderPk", holderRequest.getHolder().getIdHolderPk());
		query.setParameter("requestTypeBlock", HolderRequestType.BLOCK.getCode());
		query.setParameter("requestTypeUnBlock", HolderRequestType.UNBLOCK.getCode());
		query.setParameter("stateHolderRequest", holderRequest.getStateHolderRequest());
		
		return Integer.parseInt(query.getSingleResult().toString());	
	}
	
	
	/**
	 * Gets the holder request list by state service bean.
	 *
	 * @param filter the filter
	 * @return the holder request list by state service bean
	 */
	@SuppressWarnings("unchecked")
	public List<Holder> getHolderRequestListByStateServiceBean(HolderRequestTO filter){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT h FROM Holder h ");
		if(filter.getStateRequest()!=0){
			sbQuery.append(" Where h.stateHolder = :state");
			
		}
		
		Query query = em.createQuery(sbQuery.toString());
		
		if(filter.getStateRequest()!=0){
			query.setParameter("state", filter.getStateRequest());
		}
		
		return (List<Holder>) query.getResultList();
		
	}
	
	/**
	 * Update holder request service bean.
	 *
	 * @param holderRequest the holder request
	 * @return the int
	 */
	public int updateHolderRequestServiceBean(HolderRequest holderRequest){		
		
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append("Update HolderRequest h set h.holder.idHolderPk = :idHolderPk Where h.idHolderRequestPk = :idHolderRequestPk");
		 Query query = em.createQuery(sbQuery.toString());
		
		 query.setParameter("idHolderPk", holderRequest.getHolder().getIdHolderPk());
		 query.setParameter("idHolderRequestPk", holderRequest.getIdHolderRequestPk());
		 return query.executeUpdate();	
		
	}
	
	/**
	 * Gets the list search holder general.
	 *
	 * @param docType the doc type
	 * @param docNumber the doc number
	 * @param expedition the expedition
	 * @return the list search holder general
	 */
	@SuppressWarnings("unchecked")
	public List<Holder> getListSearchHolderGeneral(Integer docType,String docNumber,Integer expedition){ 
		List<Holder> list = null;		
//		try {		
			StringBuilder stringBuilderSql = new StringBuilder();
			stringBuilderSql.append("SELECT h FROM Holder h ");
			stringBuilderSql.append("WHERE 1 = 1 ");			
				stringBuilderSql.append("and h.stateHolder = :stateHolder ");		
			if(Validations.validateIsNotNullAndNotEmpty(docType)){		
				stringBuilderSql.append("and h.documentType = :docType ");		
			}			
			if(Validations.validateIsNotNullAndNotEmpty(docNumber)){
				
				stringBuilderSql.append("and h.documentNumber like :docNumber ");
			}			
			if(Validations.validateIsNotNullAndNotEmpty(expedition)){
				stringBuilderSql.append("and h.documentSource = :expedition ");
			}			
			stringBuilderSql.append("order by h.fullName  ");									
			Query query = em.createQuery(stringBuilderSql.toString()).setFirstResult(0).setMaxResults(100);			
			query.setParameter("stateHolder",HolderStateType.REGISTERED.getCode());	
			if(Validations.validateIsNotNullAndNotEmpty(docType)){		
			   query.setParameter("docType",docType);
			}			
			if(Validations.validateIsNotNullAndNotEmpty(docNumber)){
				query.setParameter("docNumber",'%'+docNumber+ '%');
			}			
			if(Validations.validateIsNotNullAndNotEmpty(expedition)){
				query.setParameter("expedition",expedition);	
			}			

//		    List<Holder> listHolderFilter = new ArrayList<Holder>();		       
		    list =  (List<Holder>)query.getResultList();		    
//		    Long identificator = new Long(-1);		    
//		    if(Validations.validateListIsNotNullAndNotEmpty(list)){   
//		       for(Holder h : list){
//		    	   if(!identificator.equals(h.getIdHolderPk())){
//		    		   listHolderFilter.add(h);
//		    		   identificator = h.getIdHolderPk();
//		    	   }
//		       }
//		    }		       
//		    if(Validations.validateListIsNotNullAndNotEmpty(listHolderFilter)){
//		    	list = listHolderFilter;
//		    }	    
		    
//		} catch(Exception e){
//			list = null;		
//		}		
		return list;
	}
	
	/**
	 * Gets the list search holder service bean.
	 *
	 * @param holder the holder
	 * @return the list search holder service bean
	 */
	@SuppressWarnings("unchecked")
	public List<Holder> getListSearchHolderServiceBean(Holder holder){ 
		List<Holder> list = null;		
		try {		
			StringBuilder stringBuilderSql = new StringBuilder();
			stringBuilderSql.append("SELECT h FROM Holder h ");
			stringBuilderSql.append("LEFT JOIN h.holderAccountDetail had ");
			stringBuilderSql.append("LEFT JOIN had.holderAccount ha ");
			stringBuilderSql.append("WHERE 1 = 1 ");		
			if(Validations.validateIsNotNullAndNotEmpty(holder.getIdHolderPk())){		
				stringBuilderSql.append("and h.idHolderPk = :idHolderPk ");		
			}			
			if(Validations.validateIsNotNullAndNotEmpty(holder.getStateHolder())){
				stringBuilderSql.append("and h.stateHolder = :stateHolder ");
			}			
			if(Validations.validateIsNotNullAndNotEmpty(holder.getParticipant())){
				stringBuilderSql.append("and (h.participant.idParticipantPk = :idParticipantPk or ha.participant.idParticipantPk = :idParticipantPk)");	
			}			
			if(Validations.validateIsNotNullAndNotEmpty(holder.getDocumentNumber())){
				stringBuilderSql.append("and h.documentType = :documentType ");
				stringBuilderSql.append("and h.documentNumber = :documentNumber ");
			}			
			if(Validations.validateIsNotNullAndNotEmpty(holder.getHolderType())){
				stringBuilderSql.append("and h.holderType = :holderType ");
			}		
			if(Validations.validateIsNotNullAndNotEmpty(holder.getName())){
				stringBuilderSql.append("and h.name like :name ");
			}
			if(Validations.validateIsNotNullAndNotEmpty(holder.getFirstLastName())){
				stringBuilderSql.append("and h.firstLastName like :firstLastName ");
			}
			if(Validations.validateIsNotNullAndNotEmpty(holder.getSecondLastName())){
				stringBuilderSql.append("and h.secondLastName like :secondLastName ");
			}
			if(Validations.validateIsNotNullAndNotEmpty(holder.getFullName())){
				
				String temp = holder.getFullName();
				if(temp.contains(" ")){
					stringBuilderSql.append("and h.fullName in ( :fullName ) ");
				} else {
					stringBuilderSql.append("and h.fullName like :fullName ");
				}				
				
	        }			
			if(Validations.validateIsNotNullAndNotEmpty(holder.getInitialDate()) && 
					Validations.validateIsNotNullAndNotEmpty(holder.getFinalDate())){
				stringBuilderSql.append("and trunc(h.registryDate) between :initialDate and :finalDate");
			} else {
				if(Validations.validateIsNotNullAndNotEmpty(holder.getInitialDate())){
					stringBuilderSql.append("and trunc(h.registryDate) >= :initialDate ");
				} else if(Validations.validateIsNotNullAndNotEmpty(holder.getFinalDate())){
					stringBuilderSql.append("and trunc(h.registryDate) <= :finalDate");
				}
			}	
					
			Query query = em.createQuery(stringBuilderSql.toString());			
			if(Validations.validateIsNotNullAndNotEmpty(holder.getIdHolderPk())){		
			   query.setParameter("idHolderPk", holder.getIdHolderPk());
			}			
			if(Validations.validateIsNotNullAndNotEmpty(holder.getStateHolder())){
				query.setParameter("stateHolder",holder.getStateHolder());
			}			
			if(Validations.validateIsNotNullAndNotEmpty(holder.getParticipant())){
				query.setParameter("idParticipantPk",holder.getParticipant().getIdParticipantPk());	
			}			
			if(Validations.validateIsNotNullAndNotEmpty(holder.getDocumentNumber())){
			   query.setParameter("documentType", holder.getDocumentType());
			   query.setParameter("documentNumber", holder.getDocumentNumber());
			}			
			if(Validations.validateIsNotNullAndNotEmpty(holder.getHolderType())){
				query.setParameter("holderType",holder.getHolderType());
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(holder.getName())){
					query.setParameter("name","%"+holder.getName()+"%");
			}
			if(Validations.validateIsNotNullAndNotEmpty(holder.getFirstLastName())){
					query.setParameter("firstLastName","%"+holder.getFirstLastName()+"%");
			}
			if(Validations.validateIsNotNullAndNotEmpty(holder.getSecondLastName())){
					query.setParameter("secondLastName","%"+holder.getSecondLastName()+"%");
			}
						
		    if(Validations.validateIsNotNullAndNotEmpty(holder.getFullName())){
		    	
		    	String temp = holder.getFullName();
				if(temp != null && temp.contains(" ")){
//					temp = Permutations.getPermutations(temp);
					List<String> lista = Permutations.getPermutations(temp);
					query.setParameter("fullName", lista);
				} else {
					query.setParameter("fullName", "%" + temp + "%");
				}
		    	
//				query.setParameter("fullName","%"+holder.getFullName()+"%");
				// limite maximo de busqueda es de 100
				query.setMaxResults(100);				
			}
		    
		    if(Validations.validateIsNotNullAndNotEmpty(holder.getInitialDate()) && 
					Validations.validateIsNotNullAndNotEmpty(holder.getFinalDate())){
		    	query.setParameter("initialDate",holder.getInitialDate());
		    	query.setParameter("finalDate",holder.getFinalDate());
			} else {
				if(Validations.validateIsNotNullAndNotEmpty(holder.getInitialDate())){
					query.setParameter("initialDate",holder.getInitialDate());
				} else if(Validations.validateIsNotNullAndNotEmpty(holder.getFinalDate())){
					query.setParameter("finalDate",holder.getFinalDate());
				}
			}		    		   
			
		    List<Holder> listHolderFilter = new ArrayList<Holder>();		       
		    list =  (List<Holder>)query.getResultList();		    
		    Long identificator = new Long(-1);		    
		    if(Validations.validateListIsNotNullAndNotEmpty(list)){   
		       for(Holder h : list){
		    	   if(!identificator.equals(h.getIdHolderPk())){
		    		   listHolderFilter.add(h);
		    		   identificator = h.getIdHolderPk();
		    	   }
		       }
		    }		       
		    if(Validations.validateListIsNotNullAndNotEmpty(listHolderFilter)){
		    	list = listHolderFilter;
		    }	    
		    
		} catch(Exception e){
			list = null;		
		}		
		return list;
	}
	
	
	
	
	/**
	 * Search holder account service bean.
	 *
	 * @param holder the holder
	 * @param idAccountType the id account type
	 * @param idAccountNumber the id account number
	 * @return the holder account
	 */
	@SuppressWarnings("unchecked")
	public HolderAccount searchHolderAccountServiceBean(Holder holder, Integer idAccountType, 
			Integer idAccountNumber, Long idParticipantPk, List<Long> lstHolderPk ){ 
		HolderAccount holderAccount = null;		
		try{		
			StringBuilder stringBuilderSql = new StringBuilder();
			stringBuilderSql.append(" SELECT ha FROM Holder h ");
			stringBuilderSql.append(" inner JOIN h.holderAccountDetail had ");
			stringBuilderSql.append(" inner JOIN had.holderAccount ha ");
			stringBuilderSql.append(" WHERE 1 = 1 ");	
			stringBuilderSql.append(" and ha.accountGroup = 462 ");
			if(Validations.validateIsNotNullAndNotEmpty(holder.getIdHolderPk())){		
				stringBuilderSql.append("and h.idHolderPk = :idHolderPk ");		
			}			
			if(Validations.validateListIsNotNullAndNotEmpty(lstHolderPk)){
				stringBuilderSql.append("and h.idHolderPk in (:lstIdHolderPk) ");
			}
			if(Validations.validateIsNotNullAndNotEmpty(holder.getStateHolder())){
				stringBuilderSql.append("and h.stateHolder = :stateHolder ");
			}			
			if(Validations.validateIsNotNullAndNotEmpty(holder.getParticipant())){
				stringBuilderSql.append("and ha.participant.idParticipantPk = :idParticipantPk ");	
			}			
			if(Validations.validateIsNotNullAndNotEmpty(idParticipantPk)){
				stringBuilderSql.append("and ha.participant.idParticipantPk = :idPartAccountPk ");	
			}			
			if(Validations.validateIsNotNullAndNotEmpty(idAccountType)){
				stringBuilderSql.append("and ha.accountType = :idAccountType ");	
			}			
			if(Validations.validateIsNotNullAndNotEmpty(idAccountNumber)){
				stringBuilderSql.append("and ha.accountNumber = :accountNumber ");	
			}			
			if(Validations.validateIsNotNullAndNotEmpty(holder.getDocumentNumber())){
				stringBuilderSql.append("and h.documentType = :documentType ");
				stringBuilderSql.append("and h.documentNumber = :documentNumber ");
			}			
			if(Validations.validateIsNotNullAndNotEmpty(holder.getHolderType())){
				stringBuilderSql.append("and h.holderType = :holderType ");
			}			
			if(Validations.validateIsNotNullAndNotEmpty(holder.getName())){
			   stringBuilderSql.append("and h.name like :name ");
			}
			if(Validations.validateIsNotNullAndNotEmpty(holder.getFirstLastName())){
			   stringBuilderSql.append("and h.firstLastName like :firstLastName ");
			}
			if(Validations.validateIsNotNullAndNotEmpty(holder.getSecondLastName())){
			   stringBuilderSql.append("and h.secondLastName like :secondLastName ");
			}
			if(Validations.validateIsNotNullAndNotEmpty(holder.getFullName())){
				stringBuilderSql.append("and h.fullName like :fullName ");
	        }			
			if(holder.getInitialDate()!=null && holder.getFinalDate()!=null){
				stringBuilderSql.append("and trunc(h.registryDate) between :initialDate and :finalDate");
			}				
			Query query = em.createQuery(stringBuilderSql.toString());			
			if(Validations.validateIsNotNullAndNotEmpty(holder.getIdHolderPk())){		
			   query.setParameter("idHolderPk", holder.getIdHolderPk());
			}	
			if(Validations.validateListIsNotNullAndNotEmpty(lstHolderPk)){
				query.setParameter("lstIdHolderPk", lstHolderPk);
			}
			if(Validations.validateIsNotNullAndNotEmpty(holder.getStateHolder())){
				query.setParameter("stateHolder",holder.getStateHolder());
			}			
			if(Validations.validateIsNotNullAndNotEmpty(holder.getParticipant())){
				query.setParameter("idParticipantPk",holder.getParticipant().getIdParticipantPk());	
			}
			if(Validations.validateIsNotNullAndNotEmpty(idParticipantPk)){
				query.setParameter("idPartAccountPk", idParticipantPk);	
			}
			if(Validations.validateIsNotNullAndNotEmpty(idAccountType)){
				query.setParameter("idAccountType", idAccountType);
			}			
			if(Validations.validateIsNotNullAndNotEmpty(idAccountNumber)){
				query.setParameter("accountNumber", idAccountNumber);
			}			
			if(Validations.validateIsNotNullAndNotEmpty(holder.getDocumentNumber())){
			   query.setParameter("documentType", holder.getDocumentType());
			   query.setParameter("documentNumber", holder.getDocumentNumber());
			}			
			if(Validations.validateIsNotNullAndNotEmpty(holder.getHolderType())){
				query.setParameter("holderType",holder.getHolderType());
			}			
			if(Validations.validateIsNotNullAndNotEmpty(holder.getName())){
					query.setParameter("name","%"+holder.getName()+"%");
			}
			if(Validations.validateIsNotNullAndNotEmpty(holder.getFirstLastName())){
					query.setParameter("firstLastName","%"+holder.getFirstLastName()+"%");
			}
			if(Validations.validateIsNotNullAndNotEmpty(holder.getSecondLastName())){
					query.setParameter("secondLastName","%"+holder.getSecondLastName()+"%");
			}					
		    if(Validations.validateIsNotNullAndNotEmpty(holder.getFullName())){
				query.setParameter("fullName","%"+holder.getFullName()+"%");
			}
		    if(holder.getInitialDate()!=null && holder.getFinalDate()!=null){
		    	query.setParameter("initialDate",holder.getInitialDate());
		    	query.setParameter("finalDate",holder.getFinalDate());
		    }			
		    List<HolderAccount> listHolderAccount;	       
		    listHolderAccount =  (List<HolderAccount>)query.getResultList();	    
	     	if(Validations.validateListIsNotNullAndNotEmpty(listHolderAccount)){
	     		holderAccount = listHolderAccount.get(0);
	     	}	    
		} catch(NoResultException e){
			holderAccount = null;		
		}		
		return holderAccount;
	}
	
	/**
	 * Gets the pep person by id holder service bean.
	 *
	 * @param idHolderPk the id holder pk
	 * @return the pep person by id holder service bean
	 */
	public PepPerson getPepPersonByIdHolderServiceBean(Long idHolderPk){
		PepPerson pepPerson;
		try{
		  StringBuilder stringBuilderSql = new StringBuilder();
		  stringBuilderSql.append("SELECT p FROM PepPerson p WHERE p.holder.idHolderPk = :idHolderPk");	      
		  Query query = em.createQuery(stringBuilderSql.toString());
		  query.setParameter("idHolderPk", idHolderPk);	 
		  pepPerson = (PepPerson) query.getSingleResult();
		}
		catch(Exception e){
			pepPerson = null;
		}
	
		return pepPerson;
		
	}
	
	/**
	 * Validate exist pep person by id holder service bean.
	 *
	 * @param idHolderPk the id holder pk
	 * @return the integer
	 */
	public Integer validateExistPepPersonByIdHolderServiceBean(Long idHolderPk){
		StringBuilder stringBuilderSql = new StringBuilder();
		stringBuilderSql.append("Select count(*) FROM PepPerson p Where p.holder.idHolderPk = :idHolderPk");
		Query query = em.createQuery(stringBuilderSql.toString());
		query.setParameter("idHolderPk", idHolderPk);	 
			   	    
	    return Integer.parseInt(query.getSingleResult().toString());
			
	}
	
	/**
	 * Validate exist pep person by id legal representative service bean.
	 *
	 * @param idLegalRepresentativePk the id legal representative pk
	 * @return the integer
	 */
	public Integer validateExistPepPersonByIdLegalRepresentativeServiceBean(Long idLegalRepresentativePk){
		StringBuilder stringBuilderSql = new StringBuilder();
		stringBuilderSql.append("Select count(*) FROM PepPerson p Where p.legalRepresentative.idLegalRepresentativePk = :idLegalRepresentativePk");
		Query query = em.createQuery(stringBuilderSql.toString());
		query.setParameter("idLegalRepresentativePk", idLegalRepresentativePk);	 
	   	    
	    return Integer.parseInt(query.getSingleResult().toString());
	}
	
	
	/**
	 * Gets the pep person by id legal representative service bean.
	 *
	 * @param idLegalRepresentativePk the id legal representative pk
	 * @return the pep person by id legal representative service bean
	 */
	public PepPerson getPepPersonByIdLegalRepresentativeServiceBean(Long idLegalRepresentativePk){
		PepPerson pepPerson;
		try{
		  StringBuilder stringBuilderSql = new StringBuilder();
		  stringBuilderSql.append("SELECT p FROM PepPerson p WHERE p.legalRepresentative.idLegalRepresentativePk = :idLegalRepresentativePk");	      
		  Query query = em.createQuery(stringBuilderSql.toString());
		  query.setParameter("idLegalRepresentativePk", idLegalRepresentativePk);	 
	      pepPerson = (PepPerson) query.getSingleResult();
		}
		catch(Exception e){
			pepPerson = null;
		}
		return pepPerson;
	}
	
	
	/**
	 * Search represented entity service bean.
	 *
	 * @param representedEntity the represented entity
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<RepresentedEntity> searchRepresentedEntityServiceBean(RepresentedEntity representedEntity){
		StringBuilder stringBuilderSql = new StringBuilder();
		  stringBuilderSql.append("SELECT r FROM RepresentedEntity r WHERE r.stateRepresented = :status ");	      
		  stringBuilderSql.append(" and r.holder.idHolderPk = :idHolder ");
		  Query query = em.createQuery(stringBuilderSql.toString());
		  query.setParameter("idHolder", representedEntity.getHolder().getIdHolderPk());	
		  query.setParameter("status", representedEntity.getStateRepresented());	
		 
	      return (List<RepresentedEntity>) query.getResultList();
	}
	
	/**
	 * Gets the content file holder history service bean.
	 *
	 * @param id the id
	 * @param documentType the document type
	 * @param registryHistorical the registry historical
	 * @return the content file holder history service bean
	 */
	public HolderReqFileHistory getContentFileHolderHistoryServiceBean(Long id,Integer documentType, boolean registryHistorical){
		StringBuilder stringBuilderSql = new StringBuilder();
		  stringBuilderSql.append("SELECT r FROM HolderReqFileHistory r WHERE r.holderRequest.idHolderRequestPk = :idHolderRequest and r.documentType = :documentType ");
		  stringBuilderSql.append(" and r.registryType = :registryType ");
		  Query query = em.createQuery(stringBuilderSql.toString());
		  query.setParameter("idHolderRequest", id);
		  query.setParameter("documentType", documentType);
		  
		  if(!registryHistorical){
			  query.setParameter("registryType", RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
		  }
		  else{
			  query.setParameter("registryType", RegistryType.HISTORICAL_COPY.getCode());
		  }
		  return (HolderReqFileHistory) query.getSingleResult();
	}
	
	/**
	 * Gets the content file holder service bean.
	 *
	 * @param id the id
	 * @param documentType the document type
	 * @return the content file holder service bean
	 */
	public HolderFile getContentFileHolderServiceBean(Long id,Integer documentType){
		StringBuilder stringBuilderSql = new StringBuilder();
		  stringBuilderSql.append("SELECT r FROM HolderFile r WHERE r.holder.idHolderPk = :idHolderPk and r.documentType = :documentType and r.stateFile = :state");	
		  Query query = em.createQuery(stringBuilderSql.toString());
		  query.setParameter("idHolderPk", id);
		  query.setParameter("documentType", documentType);
		  query.setParameter("state", HolderFileStateType.REGISTERED.getCode());
		  return (HolderFile) query.getSingleResult();
	}
	
	/**
	 * Removes the holder request file service bean.
	 *
	 * @param id the id
	 */
	public void removeHolderRequestFileServiceBean(Long id){
		StringBuilder stringBuilderSql = new StringBuilder();
		  stringBuilderSql.append("Delete FROM HolderReqFileHistory r WHERE r.holderRequest.idHolderRequestPk = :idHolderRequestPk ");	
		  stringBuilderSql.append(" and r.registryType = :registryType");
		  Query query = em.createQuery(stringBuilderSql.toString());
		  query.setParameter("idHolderRequestPk", id);		 
		  query.setParameter("registryType", RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
		  query.executeUpdate();
		  
	}
	
	/**
	 * Gets the holder request by document type and document number service bean.
	 *
	 * @param holderRequest the holder request
	 * @return the holder request by document type and document number service bean
	 */
	public HolderRequest getHolderRequestByDocumentTypeAndDocumentNumberServiceBean(HolderRequest holderRequest){		
		
		HolderRequest resultHolderRequest = new HolderRequest();
		
		try{
		
		  HolderHistory holderHistory = new HolderHistory();	
		  holderHistory = 	holderRequest.getHolderHistories().get(0);
		  
		  StringBuilder stringBuilderSql = new StringBuilder();
		  stringBuilderSql.append("SELECT h FROM HolderRequest h join h.holderHistories hHis WHERE ((hHis.documentType = :documentType and hHis.documentNumber = :documentNumber) ");	      
		  stringBuilderSql.append(" or (hHis.secondDocumentType = :documentType and hHis.secondDocumentNumber = :documentNumber)) ");
		  stringBuilderSql.append(" and (h.stateHolderRequest IN (:lstStateRequest))");
		  stringBuilderSql.append(" and hHis.registryType = :registryType");
		  
		  if(holderRequest.getIdHolderRequestPk()!=null){
			  stringBuilderSql.append(" and (h.idHolderRequestPk != :idHolderRequestPk)");
		  }
		  
		  if(holderHistory.getDocumentType()!=null && holderHistory.getDocumentType().equals(DocumentType.DIO.getCode())){
			  if(holderHistory.getNationality()!=null){
				  stringBuilderSql.append(" and (hHis.nationality = :nationality or hHis.secondNationality = :nationality)");
			  }
		  }
		  
		  if(holderHistory.getDocumentSource()!=null){
			  //stringBuilderSql.append(" and hHis.documentSource = :documentSource");
		  }
		  
		  if(holderHistory.getRelatedCui()!=null){
			  stringBuilderSql.append(" and hHis.relatedCui = :relatedCui");
		  }
		  
		  stringBuilderSql.append(" order by h.idHolderRequestPk desc");
		  
		  
		  Query query = em.createQuery(stringBuilderSql.toString());
		  query.setParameter("documentType", holderHistory.getDocumentType());	
		  query.setParameter("documentNumber", holderHistory.getDocumentNumber());
		  if(holderRequest.getIdHolderRequestPk()!=null){
			  query.setParameter("idHolderRequestPk", holderRequest.getIdHolderRequestPk());
		  }
		  List<Integer> lstStateRequest = new ArrayList<Integer>();
		  lstStateRequest.add(HolderRequestStateType.REGISTERED.getCode());
		  lstStateRequest.add(HolderRequestStateType.APPROVED.getCode());
		  lstStateRequest.add(HolderRequestStateType.MODIFIED.getCode());
		  lstStateRequest.add(HolderRequestStateType.CONFIRMED.getCode());
		  query.setParameter("lstStateRequest", lstStateRequest);
		  
		  if(holderHistory.getDocumentType()!=null && holderHistory.getDocumentType().equals(DocumentType.DIO.getCode())){
			  if(holderHistory.getNationality()!=null){
				  query.setParameter("nationality", holderHistory.getNationality());	
			  }
		  }
		  if(holderHistory.getDocumentSource()!=null){
			  //query.setParameter("documentSource", holderHistory.getDocumentSource());
		  }
		  
		  if(holderHistory.getRelatedCui()!=null){
			  query.setParameter("relatedCui", holderHistory.getRelatedCui());			 
		  }
			
		  query.setParameter("registryType",RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
		  
		  List<HolderRequest> listResult = query.getResultList(); 
	      if(listResult.size()<1){
	    	  resultHolderRequest = null;
	      }
	      else if(listResult.size()>0){
	    	  resultHolderRequest = listResult.get(0);
	      }
	       
	       
		}
		catch(Exception e){
			resultHolderRequest = null;
		}
		
		return resultHolderRequest;
	}
	
	/**
	 * Gets the holder request by second document type and second document number service bean.
	 *
	 * @param holderRequest the holder request
	 * @return the holder request by second document type and second document number service bean
	 */
	public HolderRequest getHolderRequestBySecondDocumentTypeAndSecondDocumentNumberServiceBean(HolderRequest holderRequest){		
		
		HolderRequest resultHolderRequest = new HolderRequest();
		
		try{
		
		  HolderHistory holderHistory = new HolderHistory();	
		  holderHistory = holderRequest.getHolderHistories().get(0);
		  
		  
		  StringBuilder stringBuilderSql = new StringBuilder();
		  stringBuilderSql.append("SELECT h FROM HolderRequest h join h.holderHistories hHis WHERE 1=1 ");
		  
		  if(holderHistory.getSecondDocumentType()!=null && holderHistory.getSecondDocumentNumber()!=null){
			  stringBuilderSql.append(" and ((hHis.secondDocumentType = :secondDocumentType and hHis.secondDocumentNumber = :secondDocumentNumber) or ");	      
		  	  stringBuilderSql.append(" (hHis.documentType = :secondDocumentType and hHis.documentNumber = :secondDocumentNumber)) ");
		  }
		  
		  stringBuilderSql.append(" and h.stateHolderRequest IN (:lstStateRequest)");
		  
		  if(holderRequest.getIdHolderRequestPk()!=null){
			  stringBuilderSql.append(" and h.idHolderRequestPk != :idHolderRequestPk ");
		  }
		  
		  if(holderHistory.getSecondDocumentType()!=null && holderHistory.getSecondDocumentType().equals(DocumentType.DIO.getCode())){
			  if(holderHistory.getSecondNationality()!=null){
				  stringBuilderSql.append(" and (hHis.nationality = :secondNationality or hHis.secondNationality = :secondNationality)");
			  }
		  }
		  stringBuilderSql.append(" and hHis.registryType = :registryType");
		  
		  stringBuilderSql.append(" order by h.idHolderRequestPk desc");
		  
		  
		  
		  Query query = em.createQuery(stringBuilderSql.toString());
		  if(holderHistory.getSecondDocumentType()!=null && holderHistory.getSecondDocumentNumber()!=null){
		  query.setParameter("secondDocumentType", holderHistory.getSecondDocumentType());	
		  query.setParameter("secondDocumentNumber", holderHistory.getSecondDocumentNumber());
		  }
		  
		  if(holderRequest.getIdHolderRequestPk()!=null){
			  query.setParameter("idHolderRequestPk", holderRequest.getIdHolderRequestPk());
		  }
		  List<Integer> lstStateRequest = new ArrayList<Integer>();
		  lstStateRequest.add(HolderRequestStateType.REGISTERED.getCode());
		  lstStateRequest.add(HolderRequestStateType.APPROVED.getCode());
		  lstStateRequest.add(HolderRequestStateType.MODIFIED.getCode());
		  lstStateRequest.add(HolderRequestStateType.CONFIRMED.getCode());
		  query.setParameter("lstStateRequest", lstStateRequest);
		  
		  if(holderHistory.getSecondDocumentType()!=null && holderHistory.getSecondDocumentType().equals(DocumentType.DIO.getCode())){
			  if(holderHistory.getSecondNationality()!=null){
				  query.setParameter("secondNationality", holderHistory.getSecondNationality());	
			  }
		  }
		  query.setParameter("registryType",RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
		  
		  
	      if(query.getResultList().size()<1){
	    	  resultHolderRequest = null;
	      }
	      else if(query.getResultList().size()>0){
	    	  resultHolderRequest = (HolderRequest)query.getResultList().get(0);
	      }     
	      
		}
		catch(Exception e){
			resultHolderRequest = null;
		}
		
		return resultHolderRequest;
	}
	
	/**
	 * Find institution information by id service bean.
	 *
	 * @param rncFilter the rnc filter
	 * @return the institution information
	 * @throws ServiceException the service exception
	 */
	public InstitutionInformation findInstitutionInformationByIdServiceBean(InstitutionInformation rncFilter) throws ServiceException {
		try{
			Query query = em.createNamedQuery(InstitutionInformation.INSTITUTION_INFORMATION_BY_ID);
			
			query.setParameter("idInstitutionInformationPrm", rncFilter.getIdInstitutionInformationPk());
			
			return (InstitutionInformation) query.getSingleResult();
		} catch(NoResultException ex){
		   return null;
		} catch(NonUniqueResultException ex){
		   return null;
		}
	}
	
	
	/**
	 * Update request number holder request service bean.
	 *
	 * @param holderRequest the holder request
	 */
	public void updateRequestNumberHolderRequestServiceBean(HolderRequest holderRequest){
		
			  StringBuilder stringBuilderSql = new StringBuilder();			
			  stringBuilderSql.append("UPDATE HolderRequest h set h.requestNumber = :requestNumber  WHERE h.idHolderRequestPk = :idHolderRequest ");	      
			  Query query = em.createQuery(stringBuilderSql.toString());
			  query.setParameter("requestNumber", holderRequest.getRequestNumber());
			  query.setParameter("idHolderRequest",holderRequest.getIdHolderRequestPk());
			  
			  query.executeUpdate();
	}
	
	/**
	 * Disable represented entities state service bean.
	 *
	 * @param holder the holder
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int disableRepresentedEntitiesStateServiceBean(Holder holder, LoggerUser loggerUser)throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update RepresentedEntity re set re.lastModifyUser = :lastModifyUser, "); 
		sbQuery.append(" re.lastModifyDate = :lastModifyDate, re.lastModifyApp = :lastModifyApp, ");
		sbQuery.append(" re.lastModifyIp = :lastModifyIp, re.stateRepresented = :stateRepresented "); 
		sbQuery.append(" Where re.holder.idHolderPk = :idHolder ");
		
		Query query = em.createQuery(sbQuery.toString());
					
		query.setParameter("stateRepresented",RepresentativeEntityStateType.DELETED.getCode());
		query.setParameter("lastModifyUser",loggerUser.getUserName());
		query.setParameter("lastModifyDate",loggerUser.getAuditTime());
		query.setParameter("lastModifyIp",loggerUser.getIpAddress());
		query.setParameter("lastModifyApp",loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("idHolder", holder.getIdHolderPk());

		return query.executeUpdate();
	}
	
	
	
	/**
	 * Delete holder file service bean.
	 *
	 * @param holder the holder
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int deleteHolderFileServiceBean(Holder holder, LoggerUser loggerUser)throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Delete FROM HolderFile hf "); 
		sbQuery.append(" Where hf.holder.idHolderPk = :idHolder ");
		
		Query query = em.createQuery(sbQuery.toString());
					
		query.setParameter("idHolder", holder.getIdHolderPk());

		return query.executeUpdate();
	}
	
	/**
	 * Gets the represented entity by id legal representative service bean.
	 *
	 * @param id the id
	 * @return the represented entity by id legal representative service bean
	 * @throws ServiceException the service exception
	 */
	public RepresentedEntity getRepresentedEntityByIdLegalRepresentativeServiceBean(Long id)throws ServiceException {
	
		  StringBuilder stringBuilderSql = new StringBuilder();			
		  stringBuilderSql.append("SELECT e FROM RepresentedEntity e WHERE e.legalRepresentative.idLegalRepresentativePk = :idLegalRepresentative ");	      
		 
		  TypedQuery<RepresentedEntity> query = em.createQuery(stringBuilderSql.toString(),RepresentedEntity.class);
		  query.setParameter("idLegalRepresentative",id);	
		 	
		  List<RepresentedEntity> resultListRepresentedEntity = (List<RepresentedEntity>) query.getResultList(); 
		 
			 if(Validations.validateListIsNotNullAndNotEmpty(resultListRepresentedEntity)){
				 return resultListRepresentedEntity.get(0);
			 } else {
				 return null;
			 }
		
	}
	
	/**
	 * Cancel rnt request.
	 *
	 * @param indicatorMaxDays the indicator max days
	 * @param loggerUser the logger user
	 */
	@SuppressWarnings("unchecked")
	public void CancelRntRequest(Integer indicatorMaxDays,LoggerUser loggerUser){
		
		List<HolderRequest> lstHolderRequest;
		StringBuilder stringBuilderSql = new StringBuilder();
		
		
		List<Long> lstIdsRequestStateRegistered = new ArrayList<Long>();
		List<Long> lstIdsRequestStateApproved = new ArrayList<Long>();
		
		
		stringBuilderSql.append(" SELECT h.idHolderRequestPk,h.stateHolderRequest From HolderRequest h");
		stringBuilderSql.append(" Where (sysdate - h.registryDate)>= :indicatorMaxDays");	    
	    stringBuilderSql.append(" And (h.stateHolderRequest = :registeredState or h.stateHolderRequest = :approveState)");    
	    stringBuilderSql.append(" And h.requestType = :requestType");
	    

	    
	    Query query = em.createQuery(stringBuilderSql.toString());
	    query.setParameter("indicatorMaxDays",indicatorMaxDays);
	    query.setParameter("registeredState",HolderRequestStateType.REGISTERED.getCode());
		query.setParameter("approveState",HolderRequestStateType.APPROVED.getCode());
		query.setParameter("requestType",HolderRequestType.CREATION.getCode());
		
		lstHolderRequest =  (List<HolderRequest>) query.getResultList();
		
		
		for(HolderRequest holderRequest : lstHolderRequest){
			if(holderRequest.getStateHolderRequest().equals(HolderRequestStateType.REGISTERED.getCode())){
				lstIdsRequestStateRegistered.add(holderRequest.getIdHolderRequestPk());
			}
			else if(holderRequest.getStateHolderRequest().equals(HolderRequestStateType.APPROVED.getCode())){
				lstIdsRequestStateApproved.add(holderRequest.getIdHolderRequestPk());
			}
		}
		
		if(lstIdsRequestStateRegistered.size()>0){
			StringBuilder stringBuilderSql2 = new StringBuilder();
			
			stringBuilderSql2.append(" UPDATE holderRequest h set h.stateHolderRequest = :action,");
			stringBuilderSql2.append(" h.lastModifyUser = :lastModifyUser,"); 
			stringBuilderSql2.append(" h.lastModifyDate = :lastModifyDate,");
			stringBuilderSql2.append(" h.lastModifyApp = :lastModifyApp,");
			stringBuilderSql2.append(" h.lastModifyIp = :lastModifyIp");
			
			stringBuilderSql2.append(" Where .h.idHolderRequestPk in (:lstIdsStateRegistered)");
		    Query query2 = em.createQuery(stringBuilderSql2.toString());
			query2.setParameter("action", HolderRequestStateType.ANNULED.getCode());
			query2.setParameter("lstIdsStateRegistered",lstIdsRequestStateRegistered);
			query2.setParameter("lastModifyUser",loggerUser.getUserName());
			query2.setParameter("lastModifyDate",loggerUser.getAuditTime());
			query2.setParameter("lastModifyIp",loggerUser.getIpAddress());
			query2.setParameter("lastModifyApp",loggerUser.getIdPrivilegeOfSystem());
		
			query2.executeUpdate();
		}
		if(lstIdsRequestStateApproved.size()>0){
			StringBuilder stringBuilderSql3 = new StringBuilder();
			stringBuilderSql3.append(" UPDATE h holderRequest set h.stateHolderRequest = :action,");
			stringBuilderSql3.append(" h.lastModifyUser = :lastModifyUser,"); 
			stringBuilderSql3.append(" h.lastModifyDate = :lastModifyDate,");
			stringBuilderSql3.append(" h.lastModifyApp = :lastModifyApp,");
			stringBuilderSql3.append(" h.lastModifyIp = :lastModifyIp");			
			stringBuilderSql3.append(" Where h.idHolderRequestPk in (:lstIdsRequestStateApproved)");
			
		    Query query3 = em.createQuery(stringBuilderSql3.toString());
			query3.setParameter("action", HolderRequestStateType.REJECTED.getCode());
			query3.setParameter("lstIdsRequestStateApproved", lstIdsRequestStateApproved);			
			query3.setParameter("lastModifyUser",loggerUser.getUserName());
			query3.setParameter("lastModifyDate",loggerUser.getAuditTime());
			query3.setParameter("lastModifyIp",loggerUser.getIpAddress());
			query3.setParameter("lastModifyApp",loggerUser.getIdPrivilegeOfSystem());
			
			query3.executeUpdate();
		}
	    
		
	}
	
	/**
	 * Gets the count participant with account holder service bean.
	 *
	 * @param idParticipantPk the id participant pk
	 * @param idHolderFk the id holder fk
	 * @return the count participant with account holder service bean
	 */
	public int getCountParticipantWithAccountHolderServiceBean(Long idParticipantPk, Long idHolderFk){
				
		StringBuilder stringBuilderSql = new StringBuilder();
		stringBuilderSql.append("select count(*)");		
		stringBuilderSql.append(" from");
		stringBuilderSql.append(" HolderAccountDetail had join had.holderAccount ha");
		stringBuilderSql.append(" join ha.participant p");
		stringBuilderSql.append(" where ha.idHolderAccountPk = had.holderAccount.idHolderAccountPk");
		stringBuilderSql.append(" and p.idParticipantPk = ha.participant.idParticipantPk");
		stringBuilderSql.append(" and p.idParticipantPk = :idParticipantPk and had.holder.idHolderPk = :idHolderFk");
		
		Query query = em.createQuery(stringBuilderSql.toString());
		query.setParameter("idParticipantPk", idParticipantPk);
		query.setParameter("idHolderFk", idHolderFk);
	
		 
	    return Integer.valueOf(query.getSingleResult().toString()).intValue();
	}
	
	/**
	 * Gets the history rnt holder service bean.
	 *
	 * @param rnt_code the rnt_code
	 * @return the history rnt holder service bean
	 */
	@SuppressWarnings("unchecked")
	public List<HolderHistoryState> getHistoryRNTHolderServiceBean(Long rnt_code){
		 
		   StringBuilder sbQuery = new StringBuilder();
		   sbQuery.append(" Select"); 
		   sbQuery.append(" HS");
		   sbQuery.append(" From ");
		   sbQuery.append(" HolderHistoryState HS ");
		   sbQuery.append(" join  HS.holder H ");		   
		   sbQuery.append(" Where HS.holder.idHolderPk = H.idHolderPk and HS.holder.idHolderPk = :rntCode");
		   sbQuery.append(" Order By HS.idHolHistoryStatePk desc");
			  
		   Query query = em.createQuery(sbQuery.toString());
		   query.setParameter("rntCode",rnt_code);	  
			 
		   return (List<HolderHistoryState>) query.getResultList();

	       
	   }
	
	
	/**
	 * metodo para obtener los Cui No disponibles, en un determinado rango
	 * @param initialRange
	 * @param finalRange
	 * @return
	 */
	public List<Long> getListCuiNotAvailableOfOneRange(Long initialRange, Long finalRange) {
		List<Object> lstObj = new ArrayList<>();
		List<Long> lst = new ArrayList<>();
		StringBuilder sd = new StringBuilder();
		sd.append(" select h.idHolderPk");
		sd.append(" from Holder h");
		sd.append(" where 0=0");
		sd.append(" and h.idHolderPk between :initialRange and :finalRange");
		sd.append(" order by h.idHolderPk asc");
		try {
			Query q = em.createQuery(sd.toString());
			q.setParameter("initialRange", initialRange);
			q.setParameter("finalRange", finalRange);
			lstObj = q.getResultList();
			for (Object obj : lstObj) {
				lst.add(Long.parseLong(obj.toString()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lst;
	}
	   
   	/**
   	 * Gets the holder request by rnt code service bean.
   	 *
   	 * @param rntCode the rnt code
   	 * @return the holder request by rnt code service bean
   	 */
   	public HolderRequest getHolderRequestByRntCodeServiceBean(Long rntCode){		
		 
		HolderRequest	holderRequest = new HolderRequest();
			
		try{		
		  
		  StringBuilder stringBuilderSql = new StringBuilder();
		  stringBuilderSql.append("SELECT h FROM HolderRequest h join h.holder ho WHERE ho.idHolderPk = :idHolderPk");
		  stringBuilderSql.append(" and (h.stateHolderRequest IN (:lstStateRequest))");
		  stringBuilderSql.append(" and h.requestType = :requestType order by h.idHolderRequestPk desc");
		  
		  Query query = em.createQuery(stringBuilderSql.toString());
		  query.setParameter("idHolderPk", rntCode);	
		  
		  List<Integer> lstStateRequest = new ArrayList<Integer>();
		  lstStateRequest.add(HolderRequestStateType.REGISTERED.getCode());
		  lstStateRequest.add(HolderRequestStateType.APPROVED.getCode());
		  lstStateRequest.add(HolderRequestStateType.CONFIRMED.getCode());
		  query.setParameter("lstStateRequest", lstStateRequest);
		  
		  query.setParameter("requestType",HolderRequestType.MODIFICATION.getCode());
		  
	      if(query.getResultList().size()<1){
	    	  holderRequest = null;
	      }
	      else if(query.getResultList().size()>0){
	    	  holderRequest = (HolderRequest) query.getResultList().get(0);   
	      }
	       
	       
		}
		catch(Exception e){
			holderRequest = null;
		}
		
		return holderRequest;
	}
	   
	   /**
   	 * Gets the holder by document type and document number service bean.
   	 *
   	 * @param holder the holder
   	 * @return the holder by document type and document number service bean
   	 */
   	public Holder getHolderByDocumentTypeAndDocumentNumberServiceBean(Holder holder){
			
			Holder resultHolder = new Holder();
			
			try{
			  StringBuilder stringBuilderSql = new StringBuilder();			
			  stringBuilderSql.append("SELECT h FROM Holder h WHERE  ( (h.documentType = :documentType and h.documentNumber = :documentNumber) or ");	      
			  stringBuilderSql.append(" (h.secondDocumentType = :documentType and h.secondDocumentNumber = :documentNumber) )");
			  
			  if(holder.getIdHolderPk()!=null){
				  stringBuilderSql.append(" and h.idHolderPk != :idHolderPk ");
			  }
			  
			  if(holder.getDocumentType()!=null && holder.getDocumentType().equals(DocumentType.DIO.getCode())){
				  if(holder.getNationality()!=null){
					  stringBuilderSql.append(" and (h.nationality = :nationality or h.secondNationality = :nationality)");
				  }
			  }
			  
			  if(holder.getDocumentSource()!=null){
				  //stringBuilderSql.append(" and h.documentSource = :documentSource");
			  }
			  
			  if(holder.getRelatedCui()!=null){
				  stringBuilderSql.append(" and h.relatedCui = :relatedCui");
			  }
			  
			  Query query = em.createQuery(stringBuilderSql.toString());
			  
			  query.setParameter("documentType", holder.getDocumentType());	
			  query.setParameter("documentNumber", holder.getDocumentNumber());	
			  
			  if(holder.getIdHolderPk()!=null){
				  query.setParameter("idHolderPk", holder.getIdHolderPk());
			  }
			  if(holder.getDocumentType()!=null && holder.getDocumentType().equals(DocumentType.DIO.getCode())){
				  if(holder.getNationality()!=null){
					  query.setParameter("nationality", holder.getNationality());
				  }
			  }
			  
			  if(holder.getDocumentSource()!=null){
				  //query.setParameter("documentSource", holder.getDocumentSource());			  
			  }
			
			  if(holder.getRelatedCui()!=null){
				  query.setParameter("relatedCui", holder.getRelatedCui());
			  }
			  
			  if(query.getResultList().size()<1){
				  resultHolder = null;
			  }
			  else if(query.getResultList().size()>0){
				  resultHolder = (Holder) query.getResultList().get(0);
			  }
		
			  
			}
			catch(Exception e){
				resultHolder = null;
			}
			
			return resultHolder;
		} 
	   
	   /**
   	 *Metodo que obtiene el titular por su segundo tipo de Documento.
   	 *
   	 * @param holder the holder
   	 * @return the holder by second document type and second document number service bean
   	 */
   	public Holder getHolderBySecondDocumentTypeAndSecondDocumentNumberServiceBean(Holder holder){
			
			Holder resultHolder = new Holder();
			
			try{
				StringBuilder stringBuilderSql = new StringBuilder();
				stringBuilderSql.append("SELECT h FROM Holder h WHERE 1=1");
				
				if(holder.getSecondDocumentType()!=null && holder.getSecondDocumentNumber()!=null){
					stringBuilderSql.append(" and ( (h.documentType = :secondDocumentType and h.documentNumber = :secondDocumentNumber)"); 
			    
					stringBuilderSql.append(" or (h.secondDocumentType = :secondDocumentType and h.secondDocumentNumber = :secondDocumentNumber) )");
				}
				
				
			    if(holder.getIdHolderPk()!=null){
					  stringBuilderSql.append(" and h.idHolderPk != :idHolderPk ");
				  }
				  
				if(holder.getSecondDocumentType()!=null && holder.getSecondDocumentType().equals(DocumentType.DIO.getCode())){
					  if(holder.getSecondNationality()!=null){
						  stringBuilderSql.append(" and (h.nationality = :secondNationality or h.secondNationality = :secondNationality)");
					  }
				}
			    
			    Query query = em.createQuery(stringBuilderSql.toString());
			    
			    if(holder.getSecondDocumentType()!=null && holder.getSecondDocumentNumber()!=null){
			    	query.setParameter("secondDocumentType", holder.getSecondDocumentType());	
			    	query.setParameter("secondDocumentNumber", holder.getSecondDocumentNumber());	
			    }
			 
				if(holder.getIdHolderPk()!=null){
					  query.setParameter("idHolderPk", holder.getIdHolderPk());
				  }
				  if(holder.getSecondDocumentType()!=null && holder.getSecondDocumentType().equals(DocumentType.DIO.getCode())){
					  if(holder.getSecondNationality()!=null){
						  query.setParameter("secondNationality", holder.getSecondNationality());
					  }
				  }
				
				
				if(query.getResultList().size()<1){
					resultHolder = null;
				}
				else if(query.getResultList().size()>0){
					resultHolder =  (Holder)query.getResultList().get(0);
					
				}			
				
			}
			catch(Exception e){
				resultHolder = null;
			}
		
			return resultHolder;
		}
	
		/**
		 * Find modify report.
		 *
		 * @param requestPk the request pk
		 * @param nameReport the name report
		 * @return the report logger file
		 */
		@SuppressWarnings("unchecked")
		public ReportLoggerFile findModifyReport(Long requestPk, String nameReport){
			 StringBuilder stringBuilderSql = new StringBuilder();
			 stringBuilderSql.append("SELECT r FROM ReportLoggerFile r WHERE r.nameFile = :holderRequestPk");
		      
			 Query query = em.createQuery(stringBuilderSql.toString());
			 query.setParameter("holderRequestPk", nameReport+""+String.valueOf(requestPk)+".pdf");	  
			 ReportLoggerFile reportLoggerFile = new ReportLoggerFile();
			 try {
				 reportLoggerFile = (ReportLoggerFile) query.getSingleResult();
			 } catch (NoResultException e) {
			 	reportLoggerFile = null;
			 } catch (NonUniqueResultException e) {
				List<ReportLoggerFile> reportLoggerFileList = (List<ReportLoggerFile>) query.getResultList();
				reportLoggerFile = reportLoggerFileList.get(0);
			}
		     return reportLoggerFile;
	    } 
	   

		/**
		 * Find file holder modify report.
		 *
		 * @param requestPk the request pk
		 * @param nameReport the name report
		 * @return the byte[]
		 */
		public byte[] findFileHolderModifyReport(Long requestPk, String nameReport){
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" SELECT RLF.dataReport");
			sbQuery.append("   FROM ReportLoggerFile RLF");
			sbQuery.append("  WHERE RLF.nameFile =:requestPk");		
			
			Query query = em.createQuery(sbQuery.toString()); 
			query.setParameter("requestPk", nameReport+""+String.valueOf(requestPk)+".pdf"); 
			byte[] dataReport = (byte[]) query.getResultList().get(0);
			return dataReport;
	    }
		
		public String findFileHolderModifyReportPath(String nameReport){
			StringBuilder sbQuery = new StringBuilder();
			
			Map<String,Object> mapParam = new HashMap<String,Object>();		
			
			sbQuery.append(" SELECT RLF.nameTrace");
			sbQuery.append("   FROM ReportLoggerFile RLF");
			sbQuery.append("  WHERE RLF.nameFile =:requestPk");		
			
			mapParam.put("requestPk", nameReport); 
			
			String dataReport = (String) findObjectByQueryString(sbQuery.toString(),mapParam);
			
			return dataReport; 
	    }
		/**
		 * Gets the holder history photo.
		 *
		 * @param idHolderHistory the id holder history
		 * @return the filePhoto
		 * @throws ServiceException the service exception
		 */
		public byte[] getHolderHistoryPhoto(Long idHolderHistory) throws ServiceException{
			String query = "Select hh.filePhoto FROM HolderHistory hh WHERE hh.idHolderHistoryPk = :idHolderHistory";
			Query queryJpql = em.createQuery(query);
			queryJpql.setParameter("idHolderHistory", idHolderHistory);
			try{
				return (byte[]) queryJpql.getSingleResult();
			}catch(NoResultException ex){
				return null;
			}
		}
		
		/**
		 * Gets the holder history signing.
		 *
		 * @param idHolderHistory the id holder history
		 * @return the fileSigning
		 * @throws ServiceException the service exception
		 */
		public byte[] getHolderHistorySigning(Long idHolderHistory) throws ServiceException{
			String query = "Select hh.fileSigning FROM HolderHistory hh WHERE hh.idHolderHistoryPk = :idHolderHistory";
			Query queryJpql = em.createQuery(query);
			queryJpql.setParameter("idHolderHistory", idHolderHistory);
			try{
				return (byte[]) queryJpql.getSingleResult();
			}catch(NoResultException ex){
				return null;
			}
		}
		
		/**
		 * Gets the holder by name and first last name.
		 *
		 * @param strName the str name
		 * @param strFristLastName the str frist last name
		 * @param lngHolderPk the lng holder pk
		 * @return the holder by name and first last name
		 */
		public Holder getHolderByNameAndFirstLastName(String strName, String strFristLastName, Long lngHolderPk){			
			Holder resultHolder = new Holder();			
			try {			  
				StringBuilder stringBuilderSql = new StringBuilder();
				stringBuilderSql.append("SELECT h FROM Holder h WHERE h.name = :parName and h.firstLastName = :parFirstLastName ");
				stringBuilderSql.append(" and h.idHolderPk = :lngHolderPk ");	
				Query query = em.createQuery(stringBuilderSql.toString());
				query.setParameter("lngHolderPk", lngHolderPk);	
				query.setParameter("parName", strName);	
				query.setParameter("parFirstLastName", strFristLastName);
				if(query.getResultList().size()<1){
					resultHolder = null;
				} else if(query.getResultList().size()>0){
					resultHolder = (Holder) query.getResultList().get(0);
				}		       
			} catch(Exception e){
				resultHolder = null;
			}			
			return resultHolder;
		}
		
		/**
		 * The method is used to update holder in document source.
		 *
		 * @param lngHolderPk Long
		 * @param intDocumentSource Integer
		 * @param loggerUser the logger user
		 */
		public void updateHolderDocumentSource(Long lngHolderPk, Integer intDocumentSource, LoggerUser loggerUser) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			StringBuffer query = new StringBuffer(" Update Holder holder set  "+				
					" holder.documentSource = :parDocumentSource , " +								
					" holder.lastModifyUser = :parLastModifyUser , holder.lastModifyDate = :parLastModifyDate , " +
					" holder.lastModifyIp = :parLastModifyIp , holder.lastModifyApp = :parLastModifyApp " +
					" where holder.idHolderPk = :lngHolderPk ");
			parameters.put("lngHolderPk", lngHolderPk);	
			parameters.put("parDocumentSource", intDocumentSource);					
			parameters.put("parLastModifyUser", loggerUser.getUserName());
			parameters.put("parLastModifyDate", loggerUser.getAuditTime());
			parameters.put("parLastModifyApp", loggerUser.getIdPrivilegeOfSystem());
			parameters.put("parLastModifyIp", loggerUser.getIpAddress());
			updateByQuery(query.toString(), parameters);
		}		
		
		/**
		 * Gets the list holder request by document type and document number.
		 *
		 * @param holderRequest the holder request
		 * @return the list holder request by document type and document number
		 */
		@SuppressWarnings("unchecked")
		public List<HolderRequest> getListHolderRequestByDocumentTypeAndDocumentNumber(HolderRequest holderRequest){			
			List<HolderRequest> resultHolderRequest = new ArrayList<HolderRequest>();			
			try {			
			  HolderHistory holderHistory = new HolderHistory();	
			  holderHistory = 	holderRequest.getHolderHistories().get(0);			  
			  StringBuilder stringBuilderSql = new StringBuilder();
			  
			  stringBuilderSql.append(" SELECT h FROM HolderRequest h join fetch h.holder hHis ");
			  stringBuilderSql.append(" WHERE (((hHis.documentType = :documentType and hHis.documentNumber = :documentNumber) ");
			  stringBuilderSql.append(" or (hHis.secondDocumentType = :documentType and hHis.secondDocumentNumber = :documentNumber)) ");
			  stringBuilderSql.append(" and (h.stateHolderRequest IN (:lstStateRequest))) ");
			  // Issue 994
			  stringBuilderSql.append(" and h.requestType = :requestType ");
			  
			  if(holderRequest.getIdHolderRequestPk() != null){
				  stringBuilderSql.append(" and hHis.idHolderPk = :idHolderPk ");
			  }
			  if(holderHistory.getDocumentType() != null && holderHistory.getDocumentType().equals(DocumentType.DIO.getCode())){
				  if(holderHistory.getNationality() != null){
					  stringBuilderSql.append(" and (hHis.nationality = :nationality or hHis.secondNationality = :nationality) ");
				  }
			  }			  
			  stringBuilderSql.append(" order by h.idHolderRequestPk desc");			  
			  Query query = em.createQuery(stringBuilderSql.toString());
			  query.setParameter("documentType", holderHistory.getDocumentType());
			  query.setParameter("documentNumber", holderHistory.getDocumentNumber());
			  if(holderRequest.getIdHolderRequestPk() != null){
				  query.setParameter("idHolderPk", holderRequest.getIdHolderRequestPk());
			  }
			  List<Integer> lstStateRequest = new ArrayList<Integer>();
			  lstStateRequest.add(HolderRequestStateType.REGISTERED.getCode());
			  lstStateRequest.add(HolderRequestStateType.APPROVED.getCode());
			  lstStateRequest.add(HolderRequestStateType.MODIFIED.getCode());
			  lstStateRequest.add(HolderRequestStateType.CONFIRMED.getCode());
			  query.setParameter("lstStateRequest", lstStateRequest);
			  // Issue 994
			  query.setParameter("requestType", HolderRequestType.CREATION.getCode());
			  
			  if(holderHistory.getDocumentType() != null && holderHistory.getDocumentType().equals(DocumentType.DIO.getCode())){
				  if(holderHistory.getNationality() != null){
					  query.setParameter("nationality", holderHistory.getNationality());	
				  }
			  }	  
			  resultHolderRequest = (List<HolderRequest>) query.getResultList();	       
			} catch(Exception e){
				resultHolderRequest = null;
			}			
			return resultHolderRequest;
		}
		
		/**
		 * List holder file by request.
		 *
		 * @param idHolderRequestPk the id holder request pk
		 * @return the list
		 * @throws ServiceException the service exception
		 */
		public List<HolderReqFileHistory> listHolderFileByRequest(Long idHolderRequest) throws ServiceException{
			  StringBuilder stringBuilderSql = new StringBuilder();			
			  stringBuilderSql.append("SELECT H FROM HolderReqFileHistory H ");		  
			  stringBuilderSql.append(" Where H.holderRequest.idHolderRequestPk = :idHolderRequestPrm ");
			  stringBuilderSql.append(" And H.stateFile = :stateFilePrm ");
			 
			  TypedQuery<HolderReqFileHistory> query = em.createQuery(stringBuilderSql.toString(),HolderReqFileHistory.class);
			  query.setParameter("idHolderRequestPrm", idHolderRequest);
			  query.setParameter("stateFilePrm", HolderFileStateType.REGISTERED.getCode());
			  			 
			  return query.getResultList();
		}
		
		/**
		 * QUERY PARA EL WEBSERVICE DE CONSULTA DE CUI SISTEMAS INTERNOS
		 * 
		 * @param otcOperationTO the otc operation to
		 * @return the sirtex operations service bean
		 * @throws ServiceException the service exception
		 */
//		@SuppressWarnings("unchecked")
		public CuiQueryResponseTO consultCui(CuiQueryTO cuiQueryTO) throws ServiceException {
			
			StringBuilder querySql = new StringBuilder();
			
			querySql.append("	select																												");
			querySql.append("	h.ID_HOLDER_PK CUI,                                                                                                 ");
			querySql.append("	h.DOCUMENT_NUMBER NUM_DOCUMENTO,                                                   									");
			querySql.append("	(select PT.TEXT1 from PARAMETER_TABLE pt where pt.PARAMETER_TABLE_PK = h.DOCUMENT_SOURCE) EXPEDIDO,                 ");
			querySql.append("	(select PT.INDICATOR1 from PARAMETER_TABLE pt where pt.PARAMETER_TABLE_PK = h.DOCUMENT_TYPE) TIPO_DOCUMENTO,        ");
			querySql.append("	h.FIRST_LAST_NAME PRIMER_APELLIDO,                                                                                  ");
			querySql.append("	h.SECOND_LAST_NAME SEGUNDO_APELLIDO,                                                                                ");
			querySql.append("	h.NAME NOMBRES,                                                                                                     ");
			querySql.append("	h.FULL_NAME NOM_COMPLETO,                                                                                           ");
			querySql.append("	h.LEGAL_ADDRESS DIRECCION,                                                                                          ");
			querySql.append("	TO_CHAR(h.REGISTRY_DATE, 'DD/MM/YYYY') FECHA_REGISTRO,                                                              ");
			querySql.append("	(select PT.DESCRIPTION from PARAMETER_TABLE pt where pt.PARAMETER_TABLE_PK = h.STATE_HOLDER) ESTADO_CUI,            ");
			querySql.append("	ha.ALTERNATE_CODE,                                                                                                  ");
			querySql.append("	p.MNEMONIC CUENTA_MATRIZ_MNEMONICO,                                                                                 ");
			querySql.append("	p.ID_PARTICIPANT_PK CUENTA_MATRIZ,                                                                                  ");
			querySql.append("	ha.ACCOUNT_NUMBER CUENTA_TITULAR,                                                                                   ");
			querySql.append("	(select PT.TEXT1 from PARAMETER_TABLE pt where pt.PARAMETER_TABLE_PK = ha.ACCOUNT_TYPE) TIPO_CUENTA,                ");
			querySql.append("	(select PT.DESCRIPTION from PARAMETER_TABLE pt where pt.PARAMETER_TABLE_PK = ha.ACCOUNT_TYPE) TIPO_TITULAR,         ");
			querySql.append("	TO_CHAR(ha.REGISTRY_DATE, 'DD/MM/YYYY') FECHA_REGISTRO_CUENTA,                                                      ");
			querySql.append("	(select PT.PARAMETER_NAME from PARAMETER_TABLE pt where pt.PARAMETER_TABLE_PK = ha.STATE_ACCOUNT) ESTADO_CUENTA,    ");
			querySql.append("	ha.ID_HOLDER_ACCOUNT_PK,																							");
			querySql.append("	h.HOLDER_TYPE																							    		");
			
			querySql.append("	from HOLDER_ACCOUNT_DETAIL had                                                                                      ");
			querySql.append("	inner join HOLDER_ACCOUNT ha on ha.ID_HOLDER_ACCOUNT_PK = had.ID_HOLDER_ACCOUNT_FK                                  ");
			querySql.append("	inner join PARTICIPANT p on p.ID_PARTICIPANT_PK = ha.ID_PARTICIPANT_FK                                              ");
			querySql.append("	right join HOLDER h on h.ID_HOLDER_PK = had.ID_HOLDER_FK                                                            ");
			querySql.append("	where 1=1                                                                                                           ");
			
			if((cuiQueryTO.isNaturalHolder() && Validations.validateIsNotNullAndNotEmpty(cuiQueryTO.getNaturalDocumentNumber())) ||
			   (cuiQueryTO.isJuridicHolder() && Validations.validateIsNotNullAndNotEmpty(cuiQueryTO.getJuridicDocumentNumber()))){
				querySql.append("	AND h.DOCUMENT_NUMBER like :documentNumber 																		"); //ci
			}
			
			if((cuiQueryTO.isNaturalHolder() && !Validations.validateIsNotNullAndNotEmpty(cuiQueryTO.getNaturalDocumentNumber())) ||
			   (cuiQueryTO.isJuridicHolder() && !Validations.validateIsNotNullAndNotEmpty(cuiQueryTO.getJuridicDocumentNumber()))){
				querySql.append(" AND ");
			}else{
				if((cuiQueryTO.isNaturalHolder() && Validations.validateIsNotNullAndNotEmpty(cuiQueryTO.getFullName())) ||
				   (cuiQueryTO.isJuridicHolder() && Validations.validateIsNotNullAndNotEmpty(cuiQueryTO.getRazonSocial()))){
					querySql.append(" OR ");
				}
			}
			
			if(cuiQueryTO.isNaturalHolder() && cuiQueryTO.getFullName()!=null && !cuiQueryTO.getFullName().equalsIgnoreCase("")){
				querySql.append("	(TRANSLATE(REPLACE(h.NAME,' ','')||REPLACE(h.FIRST_LAST_NAME,' ','')||REPLACE(h.SECOND_LAST_NAME,' ',''),'\u00c1\u00c9\u00cd\u00d3\u00da\u00d1\u00e1\u00e9\u00ed\u00f3\u00fa\u00f1','AEIOUNaeioun')) IN (:fullName)	"); //nombre completo
			}
			if(cuiQueryTO.isJuridicHolder() && cuiQueryTO.getRazonSocial()!=null && !cuiQueryTO.getRazonSocial().equalsIgnoreCase("")){
				querySql.append("	TRANSLATE(REPLACE(REPLACE(H.FULL_NAME,'.',''),' ',''),'\u00c1\u00c9\u00cd\u00d3\u00da\u00d1\u00e1\u00e9\u00ed\u00f3\u00fa\u00f1','AEIOUNaeioun') LIKE REPLACE(:fullName,' ','')								"); //razon social
			}
			querySql.append("	order by h.ID_HOLDER_PK asc                                                                                         ");
			
			Query query = em.createNativeQuery(querySql.toString());
			
			if(cuiQueryTO.getDuplicateCode() != null && !cuiQueryTO.getDuplicateCode().equalsIgnoreCase("")){
				cuiQueryTO.setNaturalDocumentNumber(cuiQueryTO.getNaturalDocumentNumber()+"-" + cuiQueryTO.getDuplicateCode());
			}
			
			if (cuiQueryTO.isJuridicHolder() && cuiQueryTO.getJuridicDocumentNumber() != null && !cuiQueryTO.getJuridicDocumentNumber().equalsIgnoreCase("")) {
				query.setParameter("documentNumber",cuiQueryTO.getJuridicDocumentNumber().toUpperCase());
			}
			if (cuiQueryTO.isNaturalHolder() && cuiQueryTO.getNaturalDocumentNumber() != null && !cuiQueryTO.getNaturalDocumentNumber().equalsIgnoreCase("")) {
				query.setParameter("documentNumber",cuiQueryTO.getNaturalDocumentNumber().toUpperCase());
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(cuiQueryTO.getFullName()) && cuiQueryTO.isNaturalHolder()){
		    	
		    	String temp = cuiQueryTO.getFullName().toUpperCase();
				if(temp != null && temp.contains(" ")){
					List<String> lista = Permutations.getPermutationsWordsNoSpace(temp);
					query.setParameter("fullName", lista);
				} else {
					query.setParameter("fullName", "%" + temp + "%");
				}
				query.setMaxResults(100);				
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(cuiQueryTO.getRazonSocial()) && cuiQueryTO.isJuridicHolder()){
		    	
				query.setParameter("fullName", "%" + cuiQueryTO.getRazonSocial().toUpperCase() + "%");
		    	
			}
			
			List<Object[]> result = query.getResultList();
			
			//Objeto de Rspuesta
			CuiQueryResponseTO cuiQueryResponseTO = new CuiQueryResponseTO();
			//Mapas de titulares segun el tipo
			Map<Long,JuridicHolderObjectTO> mapJuridicHolders = new HashMap<Long,JuridicHolderObjectTO>();
			Map<Long,NaturalHolderObjectTO> mapNaturalHolders = new HashMap<Long,NaturalHolderObjectTO>();
			List<HolderAccountObjectTO> lstHolderAccountObjectTOs;
			NaturalHolderObjectTO naturalHolderObjectTO;
			JuridicHolderObjectTO juridicHolderObjectTO;
			HolderAccountObjectTO holderAccountObjectTO;
			
			try {
				
				//recorriendo la lista de  respuesta
				for(Object[] objectData: result){
					
					//verificando si es juridico
					if(Long.parseLong(objectData[20].toString()) == PersonType.JURIDIC.getCode()){
						
						//verificando si se encuentra en el mapa de titulares juridicos
						if(mapJuridicHolders.containsKey(Long.parseLong(objectData[0].toString()))){
							//parseando la cuenta
							if(objectData[19] != null){
								holderAccountObjectTO = new HolderAccountObjectTO();
								holderAccountObjectTO.setParticipantMnemonic((String) objectData[12]);
								holderAccountObjectTO.setIdParticipantPk(Long.parseLong(objectData[13].toString()));
								holderAccountObjectTO.setAccountNumber(Integer.parseInt(objectData[14].toString()));
								holderAccountObjectTO.setAccountTypeDescription((String) objectData[15]);
								holderAccountObjectTO.setHolderTypeDescription((String) objectData[16]);
								holderAccountObjectTO.setHolderAccountRegistryDate(CommonsUtilities.convertStringtoDate((String) objectData[17], "dd/MM/yyyy"));
								holderAccountObjectTO.setHolderAccountStateDescription((String) objectData[18]);
								//adicionando la cuenta en la lista de cuentas del titular
								mapJuridicHolders.get(Long.parseLong(objectData[0].toString())).getLstHolderAccountTOs().add(holderAccountObjectTO);
							}
						}else{
							//si no existe se registra el cui en el mapa
							juridicHolderObjectTO = new JuridicHolderObjectTO();
							juridicHolderObjectTO.setIdHolder(Long.parseLong(objectData[0].toString()));
							juridicHolderObjectTO.setDocumentNumber((String) objectData[1]);
							juridicHolderObjectTO.setDocumentTypeDescription((String) objectData[3]);
							juridicHolderObjectTO.setFullName((String) objectData[7]);
							juridicHolderObjectTO.setLegalAddress((String) objectData[8]);
							juridicHolderObjectTO.setRegistryDate(CommonsUtilities.convertStringtoDate((String) objectData[9], "dd/MM/yyyy"));
							juridicHolderObjectTO.setStateDescription((String) objectData[10]);
							
							//parseando la cuenta
							lstHolderAccountObjectTOs = new ArrayList<HolderAccountObjectTO>();
							if(objectData[19] != null){
								holderAccountObjectTO = new HolderAccountObjectTO();
								holderAccountObjectTO.setParticipantMnemonic((String) objectData[12]);
								holderAccountObjectTO.setIdParticipantPk(Long.parseLong(objectData[13].toString()));
								holderAccountObjectTO.setAccountNumber(Integer.parseInt(objectData[14].toString()));
								holderAccountObjectTO.setAccountTypeDescription((String) objectData[15]);
								holderAccountObjectTO.setHolderTypeDescription((String) objectData[16]);
								holderAccountObjectTO.setHolderAccountRegistryDate(CommonsUtilities.convertStringtoDate((String) objectData[17], "dd/MM/yyyy"));
								holderAccountObjectTO.setHolderAccountStateDescription((String) objectData[18]);
								lstHolderAccountObjectTOs.add(holderAccountObjectTO);
							}
							
							juridicHolderObjectTO.setLstHolderAccountTOs(lstHolderAccountObjectTOs);
							
							//adicionando al mapa
							mapJuridicHolders.put(juridicHolderObjectTO.getIdHolder(), juridicHolderObjectTO);
						}
					}
					
					//verificando si es natural
					if(Long.parseLong(objectData[20].toString()) == PersonType.NATURAL.getCode()){
						
						//verificando si se encuentra en el mapa de titulares juridicos
						if(mapNaturalHolders.containsKey(Long.parseLong(objectData[0].toString()))){
							//parseando la cuenta
							if(objectData[19] != null){
								holderAccountObjectTO = new HolderAccountObjectTO();
								holderAccountObjectTO.setParticipantMnemonic((String) objectData[12]);
								holderAccountObjectTO.setIdParticipantPk(Long.parseLong(objectData[13].toString()));
								holderAccountObjectTO.setAccountNumber(Integer.parseInt(objectData[14].toString()));
								holderAccountObjectTO.setAccountTypeDescription((String) objectData[15]);
								holderAccountObjectTO.setHolderTypeDescription((String) objectData[16]);
								holderAccountObjectTO.setHolderAccountRegistryDate(CommonsUtilities.convertStringtoDate((String) objectData[17], "dd/MM/yyyy"));
								holderAccountObjectTO.setHolderAccountStateDescription((String) objectData[18]);
								//adicionando la cuenta en la lista de cuentas del titular
								mapNaturalHolders.get(Long.parseLong(objectData[0].toString())).getLstHolderAccountTOs().add(holderAccountObjectTO);
							}
						}else{
							//si no existe se registra el cui en el mapa
							naturalHolderObjectTO = new NaturalHolderObjectTO();
							naturalHolderObjectTO.setIdHolder(Long.parseLong(objectData[0].toString()));
							naturalHolderObjectTO.setDocumentNumber((String) objectData[1]);
							naturalHolderObjectTO.setExpended((String) objectData[2]);
							naturalHolderObjectTO.setDocumentTypeDescription((String) objectData[3]);
							naturalHolderObjectTO.setFirstLastName((String) objectData[4]);
							naturalHolderObjectTO.setSecondLastName((String) objectData[5]);
							naturalHolderObjectTO.setName((String) objectData[6]);
							naturalHolderObjectTO.setLegalAddress((String) objectData[8]);
							naturalHolderObjectTO.setRegistryDate(CommonsUtilities.convertStringtoDate((String) objectData[9], "dd/MM/yyyy"));
							naturalHolderObjectTO.setStateDescription((String) objectData[10]);
							//dividiendo el numero de documento para obtener el codigo de duplicado
							String[] documentNumber = objectData[1].toString().split("-");
							if(documentNumber.length == 2){
								if(documentNumber[0].equalsIgnoreCase("E")){
									naturalHolderObjectTO.setDocumentNumber(documentNumber[1]);
								}
								else{
									naturalHolderObjectTO.setDocumentNumber(documentNumber[0]);
									naturalHolderObjectTO.setDuplicateCod(documentNumber[1]);
								}
							}
							if(documentNumber.length == 3){
								if(documentNumber[0].equalsIgnoreCase("E")){
									naturalHolderObjectTO.setDocumentNumber(documentNumber[1]);
									naturalHolderObjectTO.setDuplicateCod(documentNumber[2]);
								}
								else{
									naturalHolderObjectTO.setDocumentNumber(documentNumber[0]);
									naturalHolderObjectTO.setDuplicateCod(documentNumber[2]);
								}
							}
							
							//parseando la cuenta
							lstHolderAccountObjectTOs = new ArrayList<HolderAccountObjectTO>();
							if(objectData[19] != null){
								holderAccountObjectTO = new HolderAccountObjectTO();
								holderAccountObjectTO.setParticipantMnemonic((String) objectData[12]);
								holderAccountObjectTO.setIdParticipantPk(Long.parseLong(objectData[13].toString()));
								holderAccountObjectTO.setAccountNumber(Integer.parseInt(objectData[14].toString()));
								holderAccountObjectTO.setAccountTypeDescription((String) objectData[15]);
								holderAccountObjectTO.setHolderTypeDescription((String) objectData[16]);
								holderAccountObjectTO.setHolderAccountRegistryDate(CommonsUtilities.convertStringtoDate((String) objectData[17], "dd/MM/yyyy"));
								holderAccountObjectTO.setHolderAccountStateDescription((String) objectData[18]);
								lstHolderAccountObjectTOs.add(holderAccountObjectTO);
							}
							naturalHolderObjectTO.setLstHolderAccountTOs(lstHolderAccountObjectTOs);
							
							//adicionando al mapa
							mapNaturalHolders.put(naturalHolderObjectTO.getIdHolder(), naturalHolderObjectTO);
						}
					}
					
					cuiQueryResponseTO.setLstJuridicHolderObjectTOs(new ArrayList<JuridicHolderObjectTO>(mapJuridicHolders.values()));
					cuiQueryResponseTO.setLstNaturalHolderObjectTOs(new ArrayList<NaturalHolderObjectTO>(mapNaturalHolders.values()));
					
				}
				return cuiQueryResponseTO;
			} catch (NoResultException ex) {
				return null;
			}
		}
		
		@SuppressWarnings("unchecked")
		public List<Object[]> getHolderMassiveFilesInformation(Map<String, Object> parameters){
			List<Object[]> objects = new ArrayList<Object[]>();
			StringBuffer sbQuery = new StringBuffer();
			
			sbQuery.append("select file.idHolderMassiveFilePk, ");//0
			sbQuery.append("       file.fileName, ");			//1
			sbQuery.append("       file.indAutomacticProcess, ");//2
			sbQuery.append("       file.processDate, ");//3
			sbQuery.append("       file.processType, ");//4
			sbQuery.append("       file.acceptedOperations, ");//5
			sbQuery.append("       file.rejectedOperations, ");//6
			sbQuery.append("       file.totalOperations, ");//7
			sbQuery.append("       file.sequenceNumber, ");//8
			sbQuery.append("       (select pt.parameterName from ParameterTable pt where pt.parameterTablePk = file.processState ), ");//9
			sbQuery.append("       file.participant.description ");//10
			sbQuery.append(" from HolderMassiveFile file");
			sbQuery.append(" where 1 = 1");
			if(Validations.validateIsNotNull((Integer)parameters.get("processType"))){
				sbQuery.append(" and file.processType = :processType ");
			} else{
				parameters.remove("processType");
			}
			if(Validations.validateIsNotNull((Long)parameters.get("idParticipantPk"))){
				sbQuery.append(" and file.participant.idParticipantPk = :idParticipantPk ");
			} else{
				parameters.remove("idParticipantPk");
			}
			sbQuery.append(" and trunc(file.processDate) = :processDate ");
			sbQuery.append(" order by file.processDate desc ");
			
			objects = findListByQueryString(sbQuery.toString(), parameters);
			
			return objects;
		}
		
		public Long nextHolderMassiveFileSequenceNumber(Date processDate,Long idParticipantPk, Integer processType) {
			StringBuffer sbQuery = new StringBuffer();
			Map<String, Object> parameters = new HashMap<String, Object>();
			sbQuery.append("select nvl(max(f.sequenceNumber),0) from HolderMassiveFile f ");
			sbQuery.append(" where trunc(f.processDate) = trunc(:processDate)  ");
			sbQuery.append(" and f.processType = :processType  ");
			if(idParticipantPk!=null){
				sbQuery.append(" and f.participant.idParticipantPk = :idParticipantPk  ");
				parameters.put("idParticipantPk",idParticipantPk);
			}
			parameters.put("processDate",processDate);
			parameters.put("processType",processType);
			Long sq = (Long)findObjectByQueryString(sbQuery.toString(), parameters);
			sq++;
			return sq;
		}
}
