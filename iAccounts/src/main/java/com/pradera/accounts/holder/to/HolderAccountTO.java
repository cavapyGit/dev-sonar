package com.pradera.accounts.holder.to;

import java.io.Serializable;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class HolderAccountTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/04/2013
 */
public class HolderAccountTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
//	//HORDER ACCOUNT
	private String alternateCode;					//ALTERNATE_CODE
	private String participantMnemonic; 			//CTA_MATRIZ
	private Long idParticipantPk; 					//CUENTA_MATRIZ
	private Integer accountNumber; 					//CUENTA_TITULAR
	private String accountTypeDescription; 			//TIPO_CUENTA
	private String holderAccountStateDescription; 	//ESTADO_CUENTA
	private Date holderAccountRegistryDate;			//FECHA_REGISTRO_CUENTA
	
	public HolderAccountTO() {
		super();
	}


	public String getAlternateCode() {
		return alternateCode;
	}
	public void setAlternateCode(String alternateCode) {
		this.alternateCode = alternateCode;
	}
	public String getParticipantMnemonic() {
		return participantMnemonic;
	}
	public void setParticipantMnemonic(String participantMnemonic) {
		this.participantMnemonic = participantMnemonic;
	}
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	public Integer getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getAccountTypeDescription() {
		return accountTypeDescription;
	}
	public void setAccountTypeDescription(String accountTypeDescription) {
		this.accountTypeDescription = accountTypeDescription;
	}
	public String getHolderAccountStateDescription() {
		return holderAccountStateDescription;
	}
	public void setHolderAccountStateDescription(
			String holderAccountStateDescription) {
		this.holderAccountStateDescription = holderAccountStateDescription;
	}
	public Date getHolderAccountRegistryDate() {
		return holderAccountRegistryDate;
	}
	public void setHolderAccountRegistryDate(Date holderAccountRegistryDate) {
		this.holderAccountRegistryDate = holderAccountRegistryDate;
	}
	
}
