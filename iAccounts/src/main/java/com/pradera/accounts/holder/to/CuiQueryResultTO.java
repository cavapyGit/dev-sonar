package com.pradera.accounts.holder.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class CuiQueryResultTO.
 *
 * @author edv
 */
public class CuiQueryResultTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//HOLDER
	private Long idHolderPk;						//CUI
	private String holderTypeDescription; 			//TIPO_TITULAR
	private String holderDocumentNumber;			//NUM_DOCUMENTO
	private String holderDocumentSourceDescription; //EXPEDIDO
	private String holderDocumentTypeDescription; 	//TIPO_DOC
	private String holderFirstLastName;				//PRIMER_APELLIDO
	private String holderSecondLastName;			//SEGUNDO_APELLIDO
	private String holderName;						//NOMBRES
	private String holderFullName;					//NOM_COMPLETO
	private String holderLegalAddress;				//DIRECCION
	private Date holderRegistryDate;				//FECHA_REGISTRO
	private String holderState;						//ESTADO_CUI
	
	//Lista de cuentas del titular
	private List<HolderAccountTO> lstHolderAccountTOs;
	

	public CuiQueryResultTO() {
		super();
	}
	
	
	public Long getIdHolderPk() {
		return idHolderPk;
	}
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}
	public String getHolderTypeDescription() {
		return holderTypeDescription;
	}
	public void setHolderTypeDescription(String holderTypeDescription) {
		this.holderTypeDescription = holderTypeDescription;
	}
	public String getHolderDocumentNumber() {
		return holderDocumentNumber;
	}
	public void setHolderDocumentNumber(String holderDocumentNumber) {
		this.holderDocumentNumber = holderDocumentNumber;
	}
	public String getHolderDocumentSourceDescription() {
		return holderDocumentSourceDescription;
	}
	public void setHolderDocumentSourceDescription(
			String holderDocumentSourceDescription) {
		this.holderDocumentSourceDescription = holderDocumentSourceDescription;
	}
	public String getHolderDocumentTypeDescription() {
		return holderDocumentTypeDescription;
	}
	public void setHolderDocumentTypeDescription(
			String holderDocumentTypeDescription) {
		this.holderDocumentTypeDescription = holderDocumentTypeDescription;
	}
	public String getHolderFirstLastName() {
		return holderFirstLastName;
	}
	public void setHolderFirstLastName(String holderFirstLastName) {
		this.holderFirstLastName = holderFirstLastName;
	}
	public String getHolderSecondLastName() {
		return holderSecondLastName;
	}
	public void setHolderSecondLastName(String holderSecondLastName) {
		this.holderSecondLastName = holderSecondLastName;
	}
	public String getHolderName() {
		return holderName;
	}
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}
	public String getHolderFullName() {
		return holderFullName;
	}
	public void setHolderFullName(String holderFullName) {
		this.holderFullName = holderFullName;
	}
	public String getHolderLegalAddress() {
		return holderLegalAddress;
	}
	public void setHolderLegalAddress(String holderLegalAddress) {
		this.holderLegalAddress = holderLegalAddress;
	}
	public Date getHolderRegistryDate() {
		return holderRegistryDate;
	}
	public void setHolderRegistryDate(Date holderRegistryDate) {
		this.holderRegistryDate = holderRegistryDate;
	}
	public String getHolderState() {
		return holderState;
	}
	public void setHolderState(String holderState) {
		this.holderState = holderState;
	}
	public List<HolderAccountTO> getLstHolderAccountTOs() {
		return lstHolderAccountTOs;
	}
	public void setLstHolderAccountTOs(List<HolderAccountTO> lstHolderAccountTOs) {
		this.lstHolderAccountTOs = lstHolderAccountTOs;
	}
	
}
