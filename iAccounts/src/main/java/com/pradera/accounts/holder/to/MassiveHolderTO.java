package com.pradera.accounts.holder.to;

import java.io.Serializable;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MassiveHolderTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
public class MassiveHolderTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The massive file selected. */
	private Integer massiveFileSelected;
	
	/** The participant selected. */
	private Long participantSelected;
	
	/** The process date. */
	private Date processDate;
	
	
	/**
	 * Gets the massive file selected.
	 *
	 * @return the massive file selected
	 */
	public Integer getMassiveFileSelected() {
		return massiveFileSelected;
	}
	
	/**
	 * Sets the massive file selected.
	 *
	 * @param massiveFileSelected the new massive file selected
	 */
	public void setMassiveFileSelected(Integer massiveFileSelected) {
		this.massiveFileSelected = massiveFileSelected;
	}
	
	/**
	 * Gets the participant selected.
	 *
	 * @return the participant selected
	 */
	public Long getParticipantSelected() {
		return participantSelected;
	}
	
	/**
	 * Sets the participant selected.
	 *
	 * @param participantSelected the new participant selected
	 */
	public void setParticipantSelected(Long participantSelected) {
		this.participantSelected = participantSelected;
	}
	
	/**
	 * Gets the process date.
	 *
	 * @return the process date
	 */
	public Date getProcessDate() {
		return processDate;
	}
	
	/**
	 * Sets the process date.
	 *
	 * @param processDate the new process date
	 */
	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}
	
	
	
}
