package com.pradera.accounts.holder.view;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.accounts.holder.facade.HolderServiceFacade;
import com.pradera.accounts.holder.to.HolderRequestTO;
import com.pradera.accounts.investorType.facade.InvestorTypeFacade;
import com.pradera.accounts.investorType.to.InvestorTypeModelTO;
import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.extension.view.LegalRepresentativeHelperBean;
import com.pradera.core.component.utils.DocumentTO;
import com.pradera.core.component.utils.DocumentValidator;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.HolderFile;
import com.pradera.model.accounts.HolderHistory;
import com.pradera.model.accounts.HolderHistoryState;
import com.pradera.model.accounts.HolderRequest;
import com.pradera.model.accounts.LegalRepresentative;
import com.pradera.model.accounts.LegalRepresentativeFile;
import com.pradera.model.accounts.LegalRepresentativeHistory;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.PepPerson;
import com.pradera.model.accounts.RepresentedEntity;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.accounts.type.PepMotiveType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.GeographicLocationStateType;
import com.pradera.model.generalparameter.type.GeographicLocationType;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SearchHolderMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/08/2015
 */
@DepositaryWebBean
public class SearchHolderMgmtBean extends GenericBaseBean implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The holder request. */
	private HolderRequest holderRequest;
	
	/** The holder request selected. */
	private HolderRequest holderRequestSelected;
	
	/** The pep person. */
	private PepPerson pepPerson;
	
	/** The pep person legal. */
	private PepPerson pepPersonLegal;
	
	/** The holder history. */
	private HolderHistory holderHistory;
	
	/** The holder filter. */
	private HolderRequestTO holderFilter;
	
	/** The participant. */
	private Participant participant;
	
	/** The holder. */
	private Holder holder;
	
	/** The legal representative. */
	private LegalRepresentative legalRepresentative;

	/** The legal representative history. */
	private LegalRepresentativeHistory legalRepresentativeHistory;
	
	/** The represented entity. */
	private RepresentedEntity representedEntity;
	
	/** The show panel options component. */
	private boolean showPanelOptionsComponent;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The holder data model. */
	private HolderDataModel holderDataModel;
	
	/** The legal representative history array. */
	private LegalRepresentativeHistory[] legalRepresentativeHistoryArray;

	/** The legal representative array. */
	private LegalRepresentative[] legalRepresentativeArray;

	/** The flag copy legal holder. */
	private boolean flagCopyLegalHolder;
	
	/** The flag copy legal representative. */
	private boolean flagCopyLegalRepresentative;
	
	/** The flag holder natural person. */
	private boolean flagHolderNaturalPerson;
	
	/** The flag holder juridic person. */
	private boolean flagHolderJuridicPerson;
	
	/** The flag representative natural person. */
	private boolean flagRepresentativeNaturalPerson;
	
	/** The flag representative juridic person. */
	private boolean flagRepresentativeJuridicPerson;
	
	/** The flag minor. */
	private boolean flagMinor;
	
	/** The disabled country resident holder. */
	private boolean disabledCountryResidentHolder;
	
	/** The disabled country resident representative. */
	private boolean disabledCountryResidentRepresentative;
	
	/** The disabled representative button. */
	private boolean disabledRepresentativeButton;
	
	/** The flag is pep. */
	private boolean flagIsPEP;
	
	/** The flag representative is pep. */
	private boolean flagRepresentativeIsPEP;

	/** The list participant. */
	private List<Participant> listParticipant;

	/** The list juridic class. */
	private List<ParameterTable> listJuridicClass;

	/** The list person type. */
	private List<ParameterTable> listPersonType;

	/** The list boolean. */
	private List<BooleanType> listBoolean;
	
	/** The list state holder request. */
	private List<ParameterTable> listStateHolderRequest;

		
	/** The list document type. */
	private List<ParameterTable> listDocumentType;

	/** The list sex. */
	private List<ParameterTable> listSex;

	/** The list category pep. */
	private List<ParameterTable> listCategoryPep;
	
	/** The list role pep. */
	private List<ParameterTable> listRolePep;
	
	/** The list grade pep. */
	private List<ParameterTable> listPepGrade;
	
	/** The list representative class. */
	private List<ParameterTable> listRepresentativeClass;

	/** The list geographic location. */
	private List<GeographicLocation> listGeographicLocation;

	/** The list province location. */
	private List<GeographicLocation> listProvinceLocation;

	/** The list sector location. */
	private List<GeographicLocation> listSectorLocation;

	/** The list province postal location. */
	private List<GeographicLocation> listProvincePostalLocation;

	/** The list sector postal location. */
	private List<GeographicLocation> listSectorPostalLocation;

	/** The list province location representative. */
	private List<GeographicLocation> listProvinceLocationRepresentative;

	/** The list sector location representative. */
	private List<GeographicLocation> listSectorLocationRepresentative;

	/** The list province postal location representative. */
	private List<GeographicLocation> listProvincePostalLocationRepresentative;

	/** The list sector postal location representative. */
	private List<GeographicLocation> listSectorPostalLocationRepresentative;

	/** The disabled cmb postal country. */
	private boolean disabledCmbPostalCountry;

	/** The disabled cmb postal province. */
	private boolean disabledCmbPostalProvince;

	/** The disabled cmb postal sector. */
	private boolean disabledCmbPostalSector;

	/** The disabled input postal address. */
	private boolean disabledInputPostalAddress;

	/** The disabled cmb postal country representative. */
	private boolean disabledCmbPostalCountryRepresentative;

	/** The disabled cmb postal province representative. */
	private boolean disabledCmbPostalProvinceRepresentative;

	/** The disabled cmb postal sector representative. */
	private boolean disabledCmbPostalSectorRepresentative;

	/** The disabled input postal address representative. */
	private boolean disabledInputPostalAddressRepresentative;

	/** The general pameters facade. */
	@EJB
	GeneralParametersFacade generalPametersFacade;

	/** The holder service facade. */
	@EJB
	HolderServiceFacade holderServiceFacade;

	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
	@EJB
	InvestorTypeFacade investorTypeFacade;

	/** The lst parameter archive header. */
	private List<ParameterTable> lstParameterArchiveHeader;

	/** The lst holder req file history. */
	private List<HolderFile> lstHolderFile;

	/** The lst representative file history. */
	private List<LegalRepresentativeFile> lstLegalRepresentativeFile;
	
	/** The flag cmb representative class. */
	private boolean flagCmbRepresentativeClass;

	
	/** The flag is participant. */
	private boolean flagIsParticipant;

	/** The flag action confirm. */
	private boolean flagActionConfirm;

	/** The flag action reject. */
	private boolean flagActionReject;

	/** The flag action annular. */
	private boolean flagActionAnnular;

	/** The disabled cmb participant. */
	private boolean disabledCmbParticipant;
	
	/** The numeric type document holder. */
	private boolean numericTypeDocumentHolder;
	
	/** The numeric second type document holder. */
	private boolean numericSecondTypeDocumentHolder;
	
	/** The max lenght document number holder. */
	private Integer maxLenghtDocumentNumberHolder;
	
	/** The max lenght second document number holder. */
	private Integer maxLenghtSecondDocumentNumberHolder;
	
	/** The option selected one radio. */
	private Integer optionSelectedOneRadio;
	
	/** The disabled document number. */
	private boolean disabledDocumentNumber;
	
	/** The disabled rnte. */
	private boolean disabledCuiCode;
	
	/** The disabled natural type person. */
	private boolean disabledNaturalTypePerson;
	
	/** The disabled juridic type person. */
	private boolean disabledJuridicTypePerson;
	
	/** The disabled cmb type document. */
	private boolean disabledCmbTypeDocument;
	
	/** The disabled cmb type person. */
	private boolean disabledCmbTypePerson;
	
	/** The disabled range dates. */
	private boolean disabledRangeDates;
	
	/** The indicator pep. */
	private boolean indicatorPep;
	
	/** The list economic sector. */
	private List<ParameterTable> listEconomicSector;

	/** The list economic activity. */
	private List<ParameterTable> listEconomicActivity;
	
	private List<InvestorTypeModelTO> listInvestorType;
	
	/** The country residence. */
	@Inject @Configurable Integer countryResidence;
	
	/** The list history cui. */
	private List<HolderHistoryState> listHistoryCui;
	
	/** The list state holder. */
	private List<ParameterTable> listStateHolder;
	
	/** The document validator. */
	@Inject
    DocumentValidator documentValidator;

	/** The legal representative helper bean. */
	@Inject
    private LegalRepresentativeHelperBean legalRepresentativeHelperBean;
	
	/** The list department location. */
	private List<GeographicLocation> listDepartmentLocation;

	/** The list municipality location. */
	private List<GeographicLocation> listMunicipalityLocation;
	
	/** The list department postal location. */
	private List<GeographicLocation> listDepartmentPostalLocation;
	
	/** The list municipality postal location. */
	private List<GeographicLocation> listMunicipalityPostalLocation;

	/** The document type description. */
    private Map<Integer,String> documentTypeDescription = null;
	
    /** The request state description. */
    private Map<Integer,String> holderStateDescription = null;
	
    /** The list emission source. */
    private List<ParameterTable> listDocumentSource;

    /** The list document issuance date. */
    private LinkedHashMap<String,Integer>listDocumentIssuanceDate;
    
    /** The list civil status. */
    private List<ParameterTable> listCivilStatus;
    
    /** The list total income. */
    private List<ParameterTable> listTotalIncome;
    
    /** The flag is participant. */
	private boolean flagIsIssuerDpfInstitution;
	
	private List<ParameterTable> listEconomicActivityNaturalPerson;
	
	private final Integer linkForFamily = PepMotiveType.LINK_FOR_FAMILY.getCode();

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		holderFilter = new HolderRequestTO();
		holderRequest = new HolderRequest();
		participant = new Participant();
		holder = new Holder();
		documentTypeDescription = new HashMap<Integer,String>();
		holderStateDescription = new HashMap<Integer,String>();
		listDocumentIssuanceDate = new LinkedHashMap<String,Integer>();		
		try {
			optionSelectedOneRadio = GeneralConstants.THREE_VALUE_INTEGER;				
			getLstStateHolder();
			getLstParticipant();
			getLstPersonType();
			getLstDocumentType();
			disabledCuiCode = true;
			disabledDocumentNumber = true;
			disabledNaturalTypePerson = true;
			disabledJuridicTypePerson = true;
			disabledCmbTypePerson = false;
			disabledCmbTypeDocument = true;
			disabledRangeDates = false;
			listHolidays();			
			if (validateIsParticipant()) {
				disabledCmbParticipant = true;
				holderFilter.setParticipantSelected(userInfo.getUserAccountSession().getParticipantCode());
				Participant filter = new Participant();
				filter.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
				listParticipant = accountsFacade.getLisParticipantServiceBean(filter);
			} else {				
				// Add validations participant DPF
				flagIsIssuerDpfInstitution = false;
				Long idParticipantCode = getIssuerDpfInstitution(userInfo);			
				if(Validations.validateIsNotNullAndNotEmpty(idParticipantCode)){
					disabledCmbParticipant = true;
					flagIsIssuerDpfInstitution = true;	
					holderFilter.setParticipantSelected(idParticipantCode);
					Participant filter = new Participant();
					filter.setIdParticipantPk(idParticipantCode);
					listParticipant = accountsFacade.getLisParticipantServiceBean(filter);
				} else {
					if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
						disabledCmbParticipant = true;
					} else {
						disabledCmbParticipant = false;
					}				
					flagIsIssuerDpfInstitution = false;	
					holderFilter.setParticipantSelected(null);
				}
			}						
			
			ParameterTableTO  parameterTableTO = new ParameterTableTO();									
			parameterTableTO.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
			for(ParameterTable param : generalPametersFacade.getListParameterTableServiceBean(parameterTableTO)) {
				documentTypeDescription.put(param.getParameterTablePk(), param.getParameterName());
			}			
			parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_STATE_TYPE.getCode());
			for(ParameterTable param : generalPametersFacade.getListParameterTableServiceBean(parameterTableTO)){
				holderStateDescription.put(param.getParameterTablePk(), param.getParameterName());
			}			
			Date today = new Date();
			DateFormat df = new SimpleDateFormat("yyyy");
			int year = Integer.parseInt(df.format(today));			
			for(int i=1900;i<=year;i++){
					listDocumentIssuanceDate.put(String.valueOf(i),i);
			}			
			getLstCivilStatus();
			getLstTotalIncome();
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Gets the lst civil status.
	 *
	 * @return the lst civil status
	 */
	public void getLstCivilStatus(){
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(MasterTableType.CIVIL_STATUS
							.getCode());

			listCivilStatus = generalPametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}		
	}

	/**
	 * Gets the lst total income.
	 *
	 * @return the lst total income
	 */
	public void getLstTotalIncome(){
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(MasterTableType.TOTAL_INCOME
							.getCode());

			listTotalIncome = generalPametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}		
	}

	
	/**
	 * Gets the lst state holder.
	 *
	 * @return the lst state holder
	 */
	public void getLstStateHolder(){
		
		
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_STATE_TYPE.getCode());

			listStateHolder = generalPametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Validate is participant.
	 *
	 * @return true, if successful
	 */
	public boolean validateIsParticipant() {
		flagIsParticipant = false;		
		if(userInfo.getUserAccountSession().isParticipantInstitucion() || userInfo.getUserAccountSession().isInstitution(InstitutionType.AFP) 
				|| userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
			flagIsParticipant = true;	
		}		
		return flagIsParticipant;
	}

	/**
	 * Validate option selected.
	 */
	public void validateOptionSelected(){
		JSFUtilities.resetViewRoot();
		disabledCuiCode = true;
		disabledDocumentNumber = true;
		disabledNaturalTypePerson = true;
		disabledJuridicTypePerson = true;
		disabledCmbTypeDocument = true;
		disabledCmbTypePerson = true;
		disabledRangeDates = true;
		
		Long participantSelected = null;
		Integer stateHolder = null;
		
		if(holderFilter.getStateHolder()!=null){
			stateHolder = holderFilter.getStateHolder();
		}
		
		if(holderFilter.getParticipantSelected()!=null){
			participantSelected = holderFilter.getParticipantSelected();
		}
		
		holderFilter = new HolderRequestTO();
		holderFilter.setParticipantSelected(participantSelected);
		holderFilter.setStateHolder(stateHolder);
		
		if(optionSelectedOneRadio.equals(GeneralConstants.ONE_VALUE_INTEGER)){		   
		   disabledCuiCode = false;
		   holder = new Holder();
		   holderFilter.setHolder(new Holder());
		   holderFilter.setDocumentType(null);		  
		   holderFilter.setPersonType(null);
		   holderFilter.setInitialDate(null);
		   holderFilter.setEndDate(null);
		}
		else if(optionSelectedOneRadio.equals(GeneralConstants.TWO_VALUE_INTEGER)){				
			holderFilter.setPersonType(null);
			disabledCmbTypeDocument = false;
			holder = new Holder();
			holderFilter.setHolder(new Holder());
			holderFilter.setInitialDate(null);
			holderFilter.setEndDate(null);
		}
		else if(optionSelectedOneRadio.equals(GeneralConstants.THREE_VALUE_INTEGER)){			 
			 holderFilter.setDocumentType(null);
			 holder = new Holder();
			 holderFilter.setHolder(new Holder());
			 disabledCmbTypePerson = false;
			 disabledRangeDates = false;
		}		
		
	}
	
	/**
	 * Onchange is pep.
	 */
	public void onchangeIsPEP() {
		if (holderHistory.isIndicatorPep()) {
			holderHistory.setIndPEP(BooleanType.YES.getCode());
		} else {
			holderHistory.setIndPEP(BooleanType.NO.getCode());
		}
	}

	/**
	 * Onchange is representative pep.
	 */
	public void onchangeIsRepresentativePEP() {
		if (flagRepresentativeIsPEP) {			
			legalRepresentativeHistory.setIndPEP(BooleanType.YES.getCode());
		} else {
			legalRepresentativeHistory.setIndPEP(BooleanType.NO.getCode());
		}
	}

	/**
	 * Gets the lst geographic location.
	 *
	 * @return the lst geographic location
	 */
	public void getLstGeographicLocation() {

		try {

			GeographicLocationTO geographicLocationTO = new GeographicLocationTO();

			geographicLocationTO
					.setGeographicLocationType(GeographicLocationType.COUNTRY
							.getCode());
			geographicLocationTO
					.setState(GeographicLocationStateType.REGISTERED.getCode());

			listGeographicLocation = generalPametersFacade
					.getListGeographicLocationServiceFacade(geographicLocationTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Gets the lst department location.
	 *
	 * @param referenceFk the reference fk
	 * @return the lst department location
	 */
	public List<GeographicLocation> getLstDepartmentLocation(Integer referenceFk) {

		List<GeographicLocation> lst = null;

		try {

			GeographicLocationTO geographicLocationTO = new GeographicLocationTO();

			geographicLocationTO
					.setGeographicLocationType(GeographicLocationType.DEPARTMENT
							.getCode());
			geographicLocationTO.setIdLocationReferenceFk(referenceFk);

			lst = generalPametersFacade
					.getListGeographicLocationServiceFacade(geographicLocationTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

		return lst;

	}
	
	/**
	 * Gets the lst municipality location.
	 *
	 * @param referenceFk the reference fk
	 * @return the lst municipality location
	 */
	public List<GeographicLocation> getLstMunicipalityLocation(Integer referenceFk) {

		List<GeographicLocation> lst = null;

		try {

			GeographicLocationTO geographicLocationTO = new GeographicLocationTO();

			geographicLocationTO
					.setGeographicLocationType(GeographicLocationType.DISTRICT
							.getCode());
			geographicLocationTO.setIdLocationReferenceFk(referenceFk);

			lst = generalPametersFacade
					.getListGeographicLocationServiceFacade(geographicLocationTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

		return lst;

	}

	/**
	 * Gets the lst province location.
	 *
	 * @param referenceFk the reference fk
	 * @return the lst province location
	 */
	public List<GeographicLocation> getLstProvinceLocation(Integer referenceFk) {

		List<GeographicLocation> lst = null;

		try {

			GeographicLocationTO geographicLocationTO = new GeographicLocationTO();

			geographicLocationTO
					.setGeographicLocationType(GeographicLocationType.PROVINCE
							.getCode());
			geographicLocationTO.setIdLocationReferenceFk(referenceFk);

			lst = generalPametersFacade
					.getListGeographicLocationServiceFacade(geographicLocationTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

		return lst;

	}

	/**
	 * Gets the lst sector location.
	 *
	 * @param referenceFk the reference fk
	 * @return the lst sector location
	 */
	public List<GeographicLocation> getLstSectorLocation(Integer referenceFk) {
		List<GeographicLocation> lst = null;
		try {
			GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
			geographicLocationTO.setGeographicLocationType(GeographicLocationType.DISTRICT.getCode());
			geographicLocationTO.setState(GeographicLocationStateType.REGISTERED.getCode());
			geographicLocationTO.setIdLocationReferenceFk(referenceFk);
			lst = generalPametersFacade.getListGeographicLocationServiceFacade(geographicLocationTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		return lst;

	}

	/**
	 * Gets the lst participant.
	 *
	 * @return the lst participant
	 */
	public void getLstParticipant() {
		try {
			Participant filter = new Participant();
			filter.setState(ParticipantStateType.REGISTERED.getCode());
			listParticipant = accountsFacade.getLisParticipantServiceBean(filter);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}
	
	/**
	 * Gets the lst economic sector.
	 *
	 * @return the lst economic sector
	 */
	public void getLstEconomicSector() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.ECONOMIC_SECTOR
					.getCode());

			listEconomicSector = generalPametersFacade
					.getListParameterTableServiceBean(parameterTableTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}
	
	/**
	 * List holidays.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listHolidays() throws ServiceException{
		  Holiday holidayFilter = new Holiday();
		  holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		  GeographicLocation countryFilter = new GeographicLocation();
		  countryFilter.setIdGeographicLocationPk(countryResidence);
		  holidayFilter.setGeographicLocation(countryFilter);		    
		  allHolidays = CommonsUtilities.convertListDateToString(generalPametersFacade.getListHolidayDateServiceFacade(holidayFilter));
	}
	
	/**
	 * Change selected economic sector.
	 */
	public void changeSelectedEconomicSector() {
		try {
			ParameterTableTO filterParameterTable = new ParameterTableTO();
			filterParameterTable.setState(ParameterTableStateType.REGISTERED
					.getCode());
			filterParameterTable
					.setMasterTableFk(MasterTableType.ECONOMIC_ACTIVITY
							.getCode());
			
			listEconomicActivity = generalPametersFacade
					.getListParameterTableServiceBean(filterParameterTable);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Gets the lst category pep.
	 *
	 * @return the lst category pep
	 */
	public void getLstCategoryPep(){
		
		try {
		
		ParameterTableTO parameterTableTO = new ParameterTableTO();

		parameterTableTO.setState(ParameterTableStateType.REGISTERED
				.getCode());
		parameterTableTO
				.setMasterTableFk(MasterTableType.ASSET_LAUNDRY_MOTIVE_PEP
						.getCode());

		listCategoryPep = generalPametersFacade
				.getListParameterTableServiceBean(parameterTableTO);

		}
		
		catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

		
	}
	
	/**
	 * Gets the lst role pep.
	 *
	 * @return the lst role pep
	 */
	public void getLstRolePep(){		
		try{
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.PEP_CHARGE.getCode());
			listRolePep = generalPametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}
		catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

		
	}
	
	public void getGradePep() {
		
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.PEP_GRADE
					.getCode());
			listPepGrade = generalPametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Gets the lst juridic class.
	 *
	 * @return the lst juridic class
	 */
	public void getLstJuridicClass() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.T_CLASE_JURIDICO_TITULAR.getCode());
			listJuridicClass = generalPametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}

	/**
	 * Gets the lst person type.
	 *
	 * @return the lst person type
	 */
	public void getLstPersonType() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.PERSON_TYPE.getCode());
			listPersonType = generalPametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}

	/**
	 * Gets the lst boolean type.
	 *
	 * @return the lst boolean type
	 */
	public void getLstBooleanType() {
		listBoolean = BooleanType.list;
	}
	
		
	/**
	 * Gets the lst document type.
	 *
	 * @return the lst document type
	 */
	public void getLstDocumentType() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
			listDocumentType = generalPametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}
	
	/**
	 * Gets the lst state holder request type.
	 *
	 * @return the lst state holder request type
	 */
	public void getLstStateHolderRequestType(){
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.STATE_CREATION_HOLDER_REQUEST.getCode());
			listStateHolderRequest = generalPametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Gets the lst sex type.
	 *
	 * @return the lst sex type
	 */
	public void getLstSexType() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.SEX.getCode());
			listSex = generalPametersFacade	.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Gets the lst representative class type.
	 *
	 * @return the lst representative class type
	 */
	public void getLstRepresentativeClassType() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.CLASS_REPRENTATIVE.getCode());
			listRepresentativeClass = generalPametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}
	
	public void getListEconomicActivityWithoutSector() {
		try {
			/*Integer economicSector=null;
			if(isViewOperationRegister()|| isViewOperationModify()){
				economicSector = holderHistory.getEconomicSector();
			}
			else{
				economicSector = holderHistoryDetail.getEconomicSector();
			}*/
			listEconomicActivityNaturalPerson = generalPametersFacade.getListEconomicActivityBySector(null);
			
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Validate fields.
	 *
	 * @return true, if successful
	 */
	public boolean validateFields(){
		
		String idComponent = "frmHolderSearch:";
		JSFUtilities.resetViewRoot();
		
		boolean flag = false;
		boolean flagRnt = false;
		boolean flagDocumentType = false;
		boolean flagPersonTypeNat = false;		 		  
		  
		if(!disabledCmbTypeDocument && holderFilter.getDocumentType()==null){		 
			flagDocumentType = true;		  
			SelectOneMenu txt = (SelectOneMenu) FacesContext.getCurrentInstance().getViewRoot().findComponent(idComponent+"cmbDocumentType");
			Object[] parametro = {txt.getLabel()};		   
			Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
			JSFUtilities.addContextMessage(idComponent+"cmbDocumentType",
					FacesMessage.SEVERITY_ERROR,
					PropertiesUtilities.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ERROR_COMBO_SELECT,parametro),
					PropertiesUtilities.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ERROR_COMBO_SELECT,parametro));
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
				  PropertiesUtilities.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ERROR_COMBO_SELECT,parametro));		   
			JSFUtilities.showSimpleValidationDialog();
		}		  
		if(!disabledCmbTypePerson && holderFilter.getPersonType()==null){			
			  flagPersonTypeNat = true;			   
			  SelectOneMenu txt = (SelectOneMenu) FacesContext.getCurrentInstance().getViewRoot().findComponent(idComponent+"cmbPersonType");
			   Object[] parametro = {txt.getLabel()};			   
			   Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
			   JSFUtilities.addContextMessage(idComponent+"cmbPersonType",
						FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ERROR_COMBO_SELECT,parametro),
						PropertiesUtilities.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ERROR_COMBO_SELECT,parametro));
		   
			   JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
					   PropertiesUtilities.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ERROR_COMBO_SELECT,parametro));
			   JSFUtilities.showSimpleValidationDialog();
		   }
		
		   if(!disabledCuiCode){			   
			   if(holderFilter.getHolder() == null){
				   flagRnt = true;   
			   }			   
			   if(flagRnt){
				   InputText txt = (InputText) FacesContext.getCurrentInstance().getViewRoot().findComponent(idComponent+"inputCuiNumber");
				   Object[] parametro = {txt.getLabel()};				   
				   Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
				   JSFUtilities.addContextMessage(idComponent+"inputCuiNumber",
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ERROR_INPUT_TEXT,parametro),
							PropertiesUtilities.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ERROR_INPUT_TEXT,parametro));			 		   
				   JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						   PropertiesUtilities.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ERROR_INPUT_TEXT,parametro));	   
				   JSFUtilities.showSimpleValidationDialog();				   				   
			   }			   
			   return flagRnt;
		   }
		   
		   if(!disabledDocumentNumber){			   
			   flagDocumentType = false;			   		   
			   if(holderFilter.getDocumentNumber() == null){
				   flagDocumentType = true;
			   }
			   if(holderFilter.getDocumentNumber()!=null && holderFilter.getDocumentNumber().toString().trim().equals(GeneralConstants.BLANK_SPACE)){
				   flagDocumentType = true;
			   }			   
			   if(flagDocumentType){
				   InputText txt = (InputText) FacesContext.getCurrentInstance().getViewRoot().findComponent(idComponent+"inputDocumentNumber");
				   Object[] parametro = {txt.getLabel()};
				   
				   Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
				   JSFUtilities.addContextMessage(idComponent+"inputDocumentNumber",
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ERROR_INPUT_TEXT,parametro),
							PropertiesUtilities.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ERROR_INPUT_TEXT,parametro));		   
				   JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
							PropertiesUtilities.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ERROR_INPUT_TEXT,parametro));
				   JSFUtilities.showSimpleValidationDialog();				   
			   }			   
			   return flagDocumentType;
		   }
		   // validacion de persona natural
		   if(!disabledNaturalTypePerson){			   			  
			   flagPersonTypeNat = false;			   			   
			   if(holderFilter.getNames() == null && holderFilter.getFirstLastName() == null && holderFilter.getSecondLastName() == null
					   && holderFilter.getHolderFullName() == null){
				   flagPersonTypeNat = true;				   
				   Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();					  
				   Object[] bodyData = new Object[0];
				   
				   JSFUtilities
					.addContextMessage(idComponent+"inputNames",
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities
									.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ENTER_LEAST_ONE,bodyData),
							PropertiesUtilities
									.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ENTER_LEAST_ONE,bodyData));
			       
				   JSFUtilities
					.addContextMessage(idComponent+"inputFirstLastName",
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities
									.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ENTER_LEAST_ONE,bodyData),
							PropertiesUtilities
									.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ENTER_LEAST_ONE,bodyData));
			      
				   				   
				   JSFUtilities
					.addContextMessage(idComponent+"inputSecondLastName",
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities
									.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ENTER_LEAST_ONE,bodyData),
							PropertiesUtilities
									.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ENTER_LEAST_ONE,bodyData));
			   
				   JSFUtilities
					.addContextMessage(idComponent+"inputHolderFullName",
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities
									.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ENTER_LEAST_ONE,bodyData),
							PropertiesUtilities
									.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ENTER_LEAST_ONE,bodyData));
				   
				   
				   JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
							PropertiesUtilities
							.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ENTER_LEAST_ONE,bodyData));
	   
				   //JSFUtilities.showSimpleValidationDialog();
	
				   
			   }
			   else if(holderFilter.getNames()!=null && holderFilter.getNames().trim().equals(GeneralConstants.BLANK_SPACE)){
				   flagPersonTypeNat = true;
				   
				   InputText txt = (InputText) FacesContext.getCurrentInstance().getViewRoot().findComponent(idComponent+"inputNames");
				   Object[] parametro = {txt.getLabel()};
				   
				   Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
				   JSFUtilities
					.addContextMessage(idComponent+"inputNames",
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities
									.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ERROR_INPUT_TEXT,parametro),
							PropertiesUtilities
									.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ERROR_INPUT_TEXT,parametro));
			   
				   JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
							PropertiesUtilities
							.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ERROR_INPUT_TEXT,parametro));

				   JSFUtilities.showSimpleValidationDialog();
	
		
			   
			   }
			   else if(holderFilter.getFirstLastName()!=null && holderFilter.getFirstLastName().trim().equals(GeneralConstants.BLANK_SPACE)){
				   flagPersonTypeNat = true; 
				   
				   InputText txt = (InputText) FacesContext.getCurrentInstance().getViewRoot().findComponent(idComponent+"inputFirstLastName");
				   Object[] parametro = {txt.getLabel()};
				   
				   Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
				   JSFUtilities
					.addContextMessage(idComponent+"inputFirstLastName",
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities
									.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ERROR_INPUT_TEXT,parametro),
							PropertiesUtilities
									.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ERROR_INPUT_TEXT,parametro));
		
				   JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
							PropertiesUtilities
							.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ERROR_INPUT_TEXT,parametro));

				   JSFUtilities.showSimpleValidationDialog();
	
				   
			   }
			   else if(holderFilter.getSecondLastName()!=null && holderFilter.getSecondLastName().trim().equals(GeneralConstants.BLANK_SPACE)){
				   flagPersonTypeNat = true;
				   
				   InputText txt = (InputText) FacesContext.getCurrentInstance().getViewRoot().findComponent(idComponent+"inputSecondLastName");
				   if(txt!=null){
					   Object[] parametro = {txt.getLabel()};
				   
				   Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
				   JSFUtilities
					.addContextMessage(idComponent+"inputSecondLastName",
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities
									.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ERROR_INPUT_TEXT,parametro),
							PropertiesUtilities
									.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ERROR_INPUT_TEXT,parametro));
			
				   
				   JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
							PropertiesUtilities
							.getMessage(com.pradera.commons.utils.view.GeneralPropertiesConstants.PROPERTYFILE,locale,com.pradera.commons.utils.view.GeneralPropertiesConstants.ERROR_INPUT_TEXT,parametro));

				   JSFUtilities.showSimpleValidationDialog();

				   }
			   }			   			  			   
			   return flagPersonTypeNat;
		   }		   
		   if(flagRnt || flagDocumentType || flagPersonTypeNat){
			   flag = true;
		   }
		   else{
			   flag = false;
		   }
		
		return flag;
	}
	
	/**
	 * Search request holders.
	 */
	@LoggerAuditWeb
	public void searchHolders() {		
		try {
			setViewOperationType(ViewOperationsType.CONSULT.getCode());			
			//SI NO TIENE PARTICIPANTE Y ES EMISOR DPF NO PUEDE REGISTRAR
			if(userInfo.getUserAccountSession().isIssuerDpfInstitucion() && !flagIsIssuerDpfInstitution){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_PARTICIPANT));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			if(!validateFields()){			
				Long idHolderPk = null;				
				if(holderFilter.getHolder().getIdHolderPk() != null){
					idHolderPk = holderFilter.getHolder().getIdHolderPk();
				}	
				
				holder = new Holder();			   
				if(idHolderPk != null){
					holder.setIdHolderPk(idHolderPk);
				}	
				
				if(Validations.validateIsNotNullAndNotEmpty(holderFilter.getParticipantSelected())){
					participant.setIdParticipantPk(holderFilter.getParticipantSelected());
					holder.setParticipant(participant);	
				}
			   
				if(Validations.validateIsNotNullAndNotEmpty(holderFilter.getStateHolder())){
					holder.setStateHolder(holderFilter.getStateHolder());
				}
			   			   		   
				if(!disabledDocumentNumber){
					holder.setDocumentType(holderFilter.getDocumentType());
					holder.setDocumentNumber(holderFilter.getDocumentNumber());
				}
				if(!disabledNaturalTypePerson){					
					holder.setHolderType(holderFilter.getPersonType());
					if(Validations.validateIsNotNullAndNotEmpty(holderFilter.getNames())){					   
						holder.setName(holderFilter.getNames());
					} 
					if(Validations.validateIsNotNullAndNotEmpty(holderFilter.getFirstLastName())){
						holder.setFirstLastName(holderFilter.getFirstLastName());					   	   
					} 
					if(Validations.validateIsNotNullAndNotEmpty(holderFilter.getSecondLastName())){
						holder.setSecondLastName(holderFilter.getSecondLastName());
					}
					if(Validations.validateIsNotNullAndNotEmpty(holderFilter.getHolderFullName())){
						holder.setFullName(holderFilter.getHolderFullName());
					}
				}
				if(!disabledJuridicTypePerson){
					holder.setHolderType(holderFilter.getPersonType());
					if(Validations.validateIsNotNullAndNotEmpty( holderFilter.getBusinessName())){
						holder.setFullName(holderFilter.getBusinessName());
					}
				}
				if(Validations.validateIsNotNullAndNotEmpty(holderFilter.getInitialDate())){
					holder.setInitialDate(holderFilter.getInitialDate());
				}				
				if(Validations.validateIsNotNullAndNotEmpty(holderFilter.getEndDate())){					
					holder.setFinalDate(holderFilter.getEndDate());
				}

				List<Holder> listHolder = new ArrayList<Holder>();
				
				listHolder = holderServiceFacade.getListSearchHolderServiceFacade(holder);
			   
				if(listHolder != null && listHolder.size() > 0){
					for(int i=0;i<listHolder.size();i++){
						if(listHolder.get(i).getDocumentType()!=null){
							listHolder.get(i).setDescriptionTypeDocument(documentTypeDescription.get(listHolder.get(i).getDocumentType()).toString());
						} else {
							listHolder.get(i).setDescriptionTypeDocument(GeneralConstants.EMPTY_STRING);
						}							
						if(listHolder.get(i).getStateHolder()!=null){
							listHolder.get(i).setStateDescription(holderStateDescription.get(listHolder.get(i).getStateHolder()).toString());
					    } else {
							listHolder.get(i).setStateDescription(GeneralConstants.EMPTY_STRING);
					    }
					}					
			   }			   
			   holderDataModel = new HolderDataModel(listHolder);		   
		   }			
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}

	/**
	 * Validate resident holder.
	 */
	public void validateResidentHolder() {
		if (BooleanType.YES.getCode().equals(holderHistory.getIndResidence())) {
			holderHistory.setLegalResidenceCountry(countryResidence);
			disabledCountryResidentHolder = true;
		} else if (BooleanType.NO.getCode().equals(holderHistory.getIndResidence())) {
			disabledCountryResidentHolder = false;
		} else {
			disabledCountryResidentHolder = false;
		}
	}

	/**
	 * Copy legal holder.
	 */
	public void copyLegalHolder() {
		if (flagCopyLegalHolder) {

			listProvincePostalLocation = listProvinceLocation;
			listSectorPostalLocation = listSectorLocation;
			holderHistory.setPostalResidenceCountry(holderHistory
					.getLegalResidenceCountry());
			holderHistory.setPostalProvince(holderHistory.getLegalProvince());
			holderHistory.setPostalDistrict(holderHistory.getLegalDistrict());
			holderHistory.setPostalAddress(holderHistory.getLegalAddress());

			disabledCmbPostalCountry = true;
			disabledCmbPostalProvince = true;
			disabledCmbPostalSector = true;
			disabledInputPostalAddress = true;
		} else {
			disabledCmbPostalCountry = false;
			disabledCmbPostalProvince = false;
			disabledCmbPostalSector = false;
			disabledInputPostalAddress = false;
		}
	}

	/**
	 * Copy legal representative.
	 */
	public void copyLegalRepresentative() {
		if (flagCopyLegalRepresentative) {
			listProvincePostalLocationRepresentative = listProvinceLocationRepresentative;
			listSectorPostalLocationRepresentative = listSectorLocationRepresentative;
			legalRepresentativeHistory.setPostalResidenceCountry(legalRepresentativeHistory.getLegalResidenceCountry());
			legalRepresentativeHistory.setPostalProvince(legalRepresentativeHistory.getLegalProvince());
			legalRepresentativeHistory.setPostalDistrict(legalRepresentativeHistory.getLegalDistrict());
			legalRepresentativeHistory.setPostalAddress(legalRepresentativeHistory.getLegalAddress());
			disabledCmbPostalCountryRepresentative = true;
			disabledCmbPostalProvinceRepresentative = true;
			disabledCmbPostalSectorRepresentative = true;
			disabledInputPostalAddressRepresentative = true;
		} else {
			disabledCmbPostalCountryRepresentative = false;
			disabledCmbPostalProvinceRepresentative = false;
			disabledCmbPostalSectorRepresentative = false;
			disabledInputPostalAddressRepresentative = false;
		}
	}

	/**
	 * Sets the holder filter.
	 *
	 * @param holderFilter the new holder filter
	 */
	public void setHolderFilter(HolderRequestTO holderFilter) {
		this.holderFilter = holderFilter;
	}

	/**
	 * Gets the holder filter.
	 *
	 * @return the holder filter
	 */
	public HolderRequestTO getHolderFilter() {
		return holderFilter;
	}

	/**
	 * Checks if is show panel options component.
	 *
	 * @return true, if is show panel options component
	 */
	public boolean isShowPanelOptionsComponent() {
		return showPanelOptionsComponent;
	}

	/**
	 * Sets the show panel options component.
	 *
	 * @param showPanelOptionsComponent the new show panel options component
	 */
	public void setShowPanelOptionsComponent(boolean showPanelOptionsComponent) {
		this.showPanelOptionsComponent = showPanelOptionsComponent;
	}

	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

		/**
	 * Load by holder type person.
	 */
	public void loadByHolderTypePerson() {
		
		if (holderHistory.getHolderType().equals(PersonType.NATURAL.getCode())) {
			flagHolderNaturalPerson = true;
			flagHolderJuridicPerson = false;
			disabledRepresentativeButton = false;					
			holderHistory = new HolderHistory();			
			holderHistory.setHolderType(PersonType.NATURAL.getCode());
			disabledCountryResidentHolder = false;						
		} else if (holderHistory.getHolderType().equals(PersonType.JURIDIC.getCode())) {
			flagHolderJuridicPerson = true;
			flagHolderNaturalPerson = false;
			disabledRepresentativeButton = false;						
			holderHistory = new HolderHistory();			
			holderHistory.setHolderType(PersonType.JURIDIC.getCode());
			disabledCountryResidentHolder = false;			
		} else {
			flagHolderNaturalPerson = false;
			flagHolderJuridicPerson = false;
			disabledRepresentativeButton = true;
		}
	}

	
	/**
	 * Load province legal.
	 */
	public void loadProvinceLegal() {
		if (!(holder.getLegalResidenceCountry().equals(Integer
				.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER)))) {
			listProvinceLocation = getLstProvinceLocation(holder.getLegalResidenceCountry());
		} else {
			holder.setLegalProvince(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER));
			listProvinceLocation = null;
			holder.setLegalDistrict(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER));
			listSectorLocation = null;
		}
	}

	/**
	 * Load province legal representative.
	 */
	public void loadProvinceLegalRepresentative() {
		if (!(legalRepresentative.getLegalResidenceCountry()
				.equals(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER)))) {
			listProvinceLocationRepresentative = getLstProvinceLocation(legalRepresentative.getLegalResidenceCountry());
		} else {
			legalRepresentative.setLegalProvince(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER));
			listProvinceLocationRepresentative = null;
			legalRepresentative.setLegalDistrict(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER));
			listSectorLocationRepresentative = null;
		}
	}

	/**
	 * Load district legal.
	 */
	public void loadDistrictLegal() {
		if (!(holder.getLegalProvince().equals(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER)))) {
			listSectorLocation = getLstSectorLocation(holder.getLegalProvince());
		} else {
			holder.setLegalDistrict(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER));
			listSectorLocation = null;
		}
	}

	/**
	 * Load district legal representative.
	 */
	public void loadDistrictLegalRepresentative() {
		if (!(legalRepresentative.getLegalProvince().equals(Integer
				.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER)))) {
			listSectorLocationRepresentative = getLstSectorLocation(legalRepresentative
					.getLegalProvince());
		} else {
			legalRepresentative.setLegalDistrict(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER));
			listSectorLocationRepresentative = null;
		}
	}

	/**
	 * Load province postal.
	 */
	public void loadProvincePostal() {
		if (!(holder.getPostalResidenceCountry().equals(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER)))) {
			listProvincePostalLocation = getLstProvinceLocation(holder.getPostalResidenceCountry());
		} else {
			holder.setPostalProvince(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER));
			listProvincePostalLocation = null;
			holder.setPostalDistrict(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER));
			listSectorPostalLocation = null;
		}
	}

	/**
	 * Load province postal representative.
	 */
	public void loadProvincePostalRepresentative() {
		if (!(legalRepresentative.getPostalResidenceCountry()
				.equals(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER)))) {
			listProvincePostalLocationRepresentative = getLstProvinceLocation(legalRepresentative
					.getPostalResidenceCountry());
		} else {
			legalRepresentative.setPostalProvince(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER));
			listProvincePostalLocationRepresentative = null;
			legalRepresentative.setPostalDistrict(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER));
			listSectorPostalLocationRepresentative = null;
		}
	}

	/**
	 * Load district postal.
	 */
	public void loadDistrictPostal() {
		if (!(holder.getPostalProvince().equals(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER)))) {
			listSectorPostalLocation = getLstSectorLocation(holder.getPostalProvince());
		} else {
			holder.setLegalDistrict(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER));
			listSectorPostalLocation = null;
		}
	}

	/**
	 * Load district postal representative.
	 */
	public void loadDistrictPostalRepresentative() {
		if(legalRepresentative.getPostalProvince()!=null && !(legalRepresentative.getPostalProvince().equals(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER)))) {
			listSectorPostalLocationRepresentative = getLstSectorLocation(legalRepresentative.getPostalProvince());
		} else {
			legalRepresentative.setLegalDistrict(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER));
			listSectorPostalLocationRepresentative = null;
		}
	}

	/**
	 * Gets the holder data model.
	 *
	 * @return the holder data model
	 */
	public HolderDataModel getHolderDataModel() {
		return holderDataModel;
	}

	/**
	 * Sets the holder data model.
	 *
	 * @param holderDataModel the new holder data model
	 */
	public void setHolderDataModel(HolderDataModel holderDataModel) {
		this.holderDataModel = holderDataModel;
	}

		/**
	 * Clean all.
	 */
	public void cleanAll() {		
		JSFUtilities.resetViewRoot();		
		holderRequest = new HolderRequest();
		holderFilter = new HolderRequestTO();
		holderDataModel = null;
		optionSelectedOneRadio = GeneralConstants.ONE_VALUE_INTEGER;	
		disabledCuiCode = false;
		disabledDocumentNumber = true;
		disabledNaturalTypePerson = true;
		disabledJuridicTypePerson = true;
		disabledCmbTypePerson = true;
		disabledCmbTypeDocument = true;
		holder = new Holder();		
		if (validateIsParticipant()) {
			disabledCmbParticipant = true;
			holderFilter.setParticipantSelected(userInfo.getUserAccountSession().getParticipantCode());
		} else {
			// Add validations participant DPF
			flagIsIssuerDpfInstitution = false;
			Long idParticipantCode = getIssuerDpfInstitution(userInfo);			
			if(Validations.validateIsNotNullAndNotEmpty(idParticipantCode)){
				disabledCmbParticipant = true;
				flagIsIssuerDpfInstitution = true;	
				holderFilter.setParticipantSelected(idParticipantCode);			
			} else {
				if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
					disabledCmbParticipant = true;
				} else {
					disabledCmbParticipant = false;
				}				
				flagIsIssuerDpfInstitution = false;	
				holderFilter.setParticipantSelected(null);
			}
		}		
	}

	/**
	 * Hide dialogues.
	 */
	public void hideDialogues() {
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		JSFUtilities.hideGeneralDialogues();
	}

	/**
	 * Action back.
	 */
	public void actionBack() {
		hideDialogues();
	}
	
	/**
	 * Validate blanks.
	 *
	 * @param field the field
	 * @param idComponent the id component
	 */
	public void validateBlanks(String field,String idComponent){
		String fieldNoSpaces=GeneralConstants.EMPTY_STRING;
		String aux=field.trim();
		if(aux!=null && !(aux.trim().equals(GeneralConstants.EMPTY_STRING))){	
			for(int i=0;i<aux.length();i++){
				if(aux.charAt(i)==' '){
					if(aux.charAt(i+1)!=' '){
						fieldNoSpaces+=aux.charAt(i);
					}
				} else {
					fieldNoSpaces+=aux.charAt(i);
				}
			}
			InputText input = (InputText)  FacesContext.getCurrentInstance().getViewRoot().findComponent(idComponent);		  
			input.setValue(fieldNoSpaces);	  
		}	
	}
	
	
	/**
	 * Validate type document holder.
	 */
	public void validateTypeDocumentHolder(){		
		if(holderFilter.getDocumentType()==null){
			Long idParticipant = null;			
			if(holderFilter.getParticipantSelected()!=null){
				idParticipant = holderFilter.getParticipantSelected();
			}			
			holderFilter = new HolderRequestTO();			
			if(idParticipant!=null){
				holderFilter.setParticipantSelected(idParticipant);
			}
			numericTypeDocumentHolder = true;
			disabledDocumentNumber = true;			
			return;
		}		
		DocumentTO documentTo = documentValidator.loadMaxLenghtType(holderFilter.getDocumentType());			
		numericTypeDocumentHolder = documentTo.isNumericTypeDocument();
		maxLenghtDocumentNumberHolder = documentTo.getMaxLenghtDocumentNumber();
		disabledDocumentNumber = false;
	}
	
	/**
	 * Validate type person holder.
	 */
	public void validateTypePersonHolder(){		
		if(holderFilter.getPersonType() == null){
			Date initialDate = holderFilter.getInitialDate();
			Date endDate = holderFilter.getEndDate();	
			Long idParticipant = null;			
			if(holderFilter.getParticipantSelected()!=null){
				idParticipant = holderFilter.getParticipantSelected();
			}
			holderFilter = new HolderRequestTO();
			if(idParticipant != null){
				holderFilter.setParticipantSelected(idParticipant);
			}
			holderFilter.setInitialDate(initialDate);
			holderFilter.setEndDate(endDate);
			disabledNaturalTypePerson = true;
			disabledJuridicTypePerson = true;			
			return;
		}						
		if(holderFilter.getPersonType().equals(PersonType.NATURAL.getCode())){			
			Long idParticipant = null;			
			if(holderFilter.getParticipantSelected()!=null){
				idParticipant = holderFilter.getParticipantSelected();
			}			
			Date initialDate = holderFilter.getInitialDate();
			Date endDate = holderFilter.getEndDate();			
			holderFilter = new HolderRequestTO();			
			if(idParticipant!=null){
				holderFilter.setParticipantSelected(idParticipant);
			}			
			holderFilter.setPersonType(PersonType.NATURAL.getCode());			
			holderFilter.setInitialDate(initialDate);
			holderFilter.setEndDate(endDate);			
			disabledNaturalTypePerson = false;
			disabledJuridicTypePerson = true;			
		} else if(holderFilter.getPersonType().equals(PersonType.JURIDIC.getCode())){			
			Long idParticipant = null;			
			if(holderFilter.getParticipantSelected()!=null){
				idParticipant = holderFilter.getParticipantSelected();
			}			
			Date initialDate = holderFilter.getInitialDate();
			Date endDate = holderFilter.getEndDate();
			holderFilter = new HolderRequestTO();			
			if(idParticipant!=null){
				holderFilter.setParticipantSelected(idParticipant);
			}			
			holderFilter.setPersonType(PersonType.JURIDIC.getCode());
			holderFilter.setInitialDate(initialDate);
			holderFilter.setEndDate(endDate);			
			disabledJuridicTypePerson = false;
			disabledNaturalTypePerson = true;			
		}
	}
	
	/**
	 * Gets the holder request.
	 *
	 * @return the holder request
	 */
	public HolderRequest getHolderRequest() {
		return holderRequest;
	}

	/**
	 * Sets the holder request.
	 *
	 * @param holderRequest the new holder request
	 */
	public void setHolderRequest(HolderRequest holderRequest) {
		this.holderRequest = holderRequest;
	}
	
	/**
	 * Gets the streamed content file holder.
	 *
	 * @param id the id
	 * @param documentType the document type
	 * @return the streamed content file holder
	 */
	public StreamedContent getStreamedContentFileHolder(Long id, Integer documentType){
		StreamedContent streamedContentFile = null;
		try {
			HolderFile auxHoldeFile = holderServiceFacade.getContentFileHolderServiceFacade(id, documentType);
			InputStream inputStream;
			inputStream = new ByteArrayInputStream(auxHoldeFile.getFileHolder());
			streamedContentFile = new DefaultStreamedContent(inputStream, null,	auxHoldeFile.getFilename());
			inputStream.close();
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

		return streamedContentFile;
	}
	
	/**
	 * Gets the streamed content file photo.
	 *
	 * @return the streamed content file photo
	 */
	public StreamedContent getStreamedContentFilePhoto(){
		StreamedContent streamedContentFile = null;
		try {
			InputStream inputStream = null;
			inputStream = new ByteArrayInputStream(holder.getFilePhoto());
			streamedContentFile = new DefaultStreamedContent(inputStream, null,	holder.getFilePhotoName());
			inputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return streamedContentFile;
	}
	
	/**
	 * Gets the lst document emission source.
	 *
	 * @return the lst document emission source
	 */
	public void getLstDocumentSource() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.DOCUMENT_SOURCE.getCode());					
			listDocumentSource = generalPametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	public void buildInvestorTypeByEconAcSelected(Integer economicActivity){
		// construyendo la lista de tipo de inversionista
		InvestorTypeModelTO it = new InvestorTypeModelTO();
		it.setEconomicActivity(economicActivity);
		try {
			listInvestorType = investorTypeFacade.getLstInvestorTypeModelTO(it);
		} catch (ServiceException e) {
			e.printStackTrace();
			listInvestorType = new ArrayList<>();
		}
	}
	
	/**
	 * Loading holder information.
	 *
	 * @param event the event
	 */
	public void loadingHolderInformation(ActionEvent event) {
		
		try
		{
			holder = new Holder();
			pepPerson = new PepPerson();
			pepPersonLegal = new PepPerson();
			participant = new Participant();
			lstHolderFile = new ArrayList<HolderFile>();
			legalRepresentativeHelperBean.initializeLstLegalRepresentative();				
			List<LegalRepresentative> lstLegalRepresentative = new ArrayList<LegalRepresentative>();				
			holder = (Holder)event.getComponent().getAttributes().get("holderInformation");		
			holder = holderServiceFacade.getHolderServiceFacade(holder.getIdHolderPk());		
			if(holder.getFilePhoto() != null){
				JSFUtilities.putSessionMap("sessStreamedPhoto", holder.getFilePhoto());
			}				
			if(holder.getFileSigning() != null){		
				JSFUtilities.putSessionMap("sessStreamedSigning", holder.getFileSigning());
			}			
			legalRepresentativeHelperBean.setHolder(holder);		
			lstHolderFile = holder.getHolderFile();		
			if((holder.getParticipant().getIdParticipantPk()!=null)){
				participant.setIdParticipantPk(holder.getParticipant().getIdParticipantPk());				
			}
			if(Validations.validateIsNotNull(holder.getEconomicActivity())){
				buildInvestorTypeByEconAcSelected(holder.getEconomicActivity());
			}
			
			getLstBooleanType();
			getLstDocumentType();
			getLstParticipant();
			getLstPersonType();
			getLstSexType();
			getLstJuridicClass();
			getLstGeographicLocation();
			getLstRolePep();
			getLstCategoryPep();
			getLstEconomicSector();
			changeSelectedEconomicSector();
			getLstDocumentSource();
			getGradePep();
			getListEconomicActivityWithoutSector();
			
			listDepartmentLocation = getLstDepartmentLocation(holder.getLegalResidenceCountry());
			
			listProvinceLocation = getLstProvinceLocation(holder.getLegalDepartment());
	
			listMunicipalityLocation = getLstMunicipalityLocation(holder.getLegalProvince());			
			
			listDepartmentPostalLocation = getLstDepartmentLocation(holder.getPostalResidenceCountry());
			
			listProvincePostalLocation = getLstProvinceLocation(holder.getPostalDepartment());
	
			listMunicipalityPostalLocation = getLstMunicipalityLocation(holder.getPostalProvince());
						
			if(holder.getHolderType().equals(PersonType.NATURAL.getCode())){
				
				flagHolderJuridicPerson = false;
				flagHolderNaturalPerson = true;
				
			}else if(holder.getHolderType().equals(PersonType.JURIDIC.getCode())){
				
				flagHolderJuridicPerson = true;
				flagHolderNaturalPerson = false;
				
			}
			
			if(holder.getIndDisabled()!=null && holder.getIndDisabled().equals(BooleanType.YES.getCode())){
				holder.setIndicatorDisabled(true);
			}
			else if(holder.getIndDisabled()!=null && holder.getIndDisabled().equals(BooleanType.NO.getCode())){
				holder.setIndicatorDisabled(false);
			}
			
			if(holder.getIndMinor()!=null && holder.getIndMinor().equals(BooleanType.YES.getCode())){
				holder.setIndicatorMinor(true);
			}
			else if(holder.getIndMinor()!=null && holder.getIndMinor().equals(BooleanType.NO.getCode())){
				holder.setIndicatorMinor(false);
			}			
			pepExists(holder.getIdHolderPk());					
			if(holder.getRepresentedEntity()!=null){
				for(int i=0;i<holder.getRepresentedEntity().size();i++){
					holder.getRepresentedEntity().get(i).getLegalRepresentative().getIdLegalRepresentativePk();					
					representedEntity = new RepresentedEntity();				
					representedEntity.setRepresentativeClass(holder.getRepresentedEntity().get(i).getRepresentativeClass());;										 
					LegalRepresentative legalRepresentative = new LegalRepresentative();
					
					if(holder.getRepresentedEntity().get(i).getRepresentativeClass()!=null){
						legalRepresentative.setRepresentativeClassDescription(representedEntity.getRepresentativeClassDescription());
					}
					
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getIdLegalRepresentativePk()!=null){
						legalRepresentative.setIdLegalRepresentativePk(holder.getRepresentedEntity().get(i).getLegalRepresentative().getIdLegalRepresentativePk());
					}
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getBirthDate()!=null){
						legalRepresentative.setBirthDate(holder.getRepresentedEntity().get(i).getLegalRepresentative().getBirthDate());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentNumber()!=null){
						legalRepresentative.setDocumentNumber(holder.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentNumber());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentType()!=null){
						legalRepresentative.setDocumentType(holder.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentType());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getEconomicActivity()!=null){
						legalRepresentative.setEconomicActivity(holder.getRepresentedEntity().get(i).getLegalRepresentative().getEconomicActivity());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getEconomicSector()!=null){
						legalRepresentative.setEconomicSector(holder.getRepresentedEntity().get(i).getLegalRepresentative().getEconomicSector());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getEmail()!=null){
						legalRepresentative.setEmail(holder.getRepresentedEntity().get(i).getLegalRepresentative().getEmail());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getFaxNumber()!=null){
						legalRepresentative.setFaxNumber(holder.getRepresentedEntity().get(i).getLegalRepresentative().getFaxNumber());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getFirstLastName()!=null){
						legalRepresentative.setFirstLastName(holder.getRepresentedEntity().get(i).getLegalRepresentative().getFirstLastName());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getFullName()!=null){
				        legalRepresentative.setFullName(holder.getRepresentedEntity().get(i).getLegalRepresentative().getFullName());
					}
					
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getHomePhoneNumber()!=null){
						legalRepresentative.setHomePhoneNumber(holder.getRepresentedEntity().get(i).getLegalRepresentative().getHomePhoneNumber());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getIndResidence()!=null){
						
				        legalRepresentative.setIndResidence(holder.getRepresentedEntity().get(i).getLegalRepresentative().getIndResidence()); 
					}
					
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyApp()!=null){
						legalRepresentative.setLastModifyApp(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyApp());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyDate()!=null){
						legalRepresentative.setLastModifyDate(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyDate());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyIp()!=null){
						legalRepresentative.setLastModifyIp(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyIp());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyUser()!=null){
						legalRepresentative.setLastModifyUser(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyUser());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalAddress()!=null){
						legalRepresentative.setLegalAddress(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalAddress());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalDepartment()!=null){
						legalRepresentative.setLegalDepartment(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalDepartment());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalDistrict()!=null){
						legalRepresentative.setLegalDistrict(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalDistrict());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalProvince()!=null){
						legalRepresentative.setLegalProvince(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalProvince());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalResidenceCountry()!=null){
						legalRepresentative.setLegalResidenceCountry(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalResidenceCountry());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getMobileNumber()!=null){
						legalRepresentative.setMobileNumber(holder.getRepresentedEntity().get(i).getLegalRepresentative().getMobileNumber());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getName()!=null){
				        legalRepresentative.setName(holder.getRepresentedEntity().get(i).getLegalRepresentative().getName()); 
					}
					
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getNationality()!=null){
						legalRepresentative.setNationality(holder.getRepresentedEntity().get(i).getLegalRepresentative().getNationality());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getOfficePhoneNumber()!=null){
						legalRepresentative.setOfficePhoneNumber(holder.getRepresentedEntity().get(i).getLegalRepresentative().getOfficePhoneNumber());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPersonType()!=null){
						legalRepresentative.setPersonType(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPersonType());
					}
					
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalDepartment()!=null){
						legalRepresentative.setPostalDepartment(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalDepartment());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalDistrict()!=null){
						legalRepresentative.setPostalDistrict(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalDistrict());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalProvince()!=null){
						legalRepresentative.setPostalProvince(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalProvince());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalResidenceCountry()!=null){
						legalRepresentative.setPostalResidenceCountry(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalResidenceCountry());
					}
					
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalAddress()!=null){
						legalRepresentative.setPostalAddress(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalAddress());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getRegistryDate()!=null){
						legalRepresentative.setRegistryDate(holder.getRepresentedEntity().get(i).getLegalRepresentative().getRegistryDate());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getRegistryUser()!=null){
						legalRepresentative.setRegistryUser(holder.getRepresentedEntity().get(i).getLegalRepresentative().getRegistryUser());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getSecondDocumentNumber()!=null){
						legalRepresentative.setSecondDocumentNumber(holder.getRepresentedEntity().get(i).getLegalRepresentative().getSecondDocumentNumber());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getSecondLastName()!=null){
						legalRepresentative.setSecondLastName(holder.getRepresentedEntity().get(i).getLegalRepresentative().getSecondLastName());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getSecondNationality()!=null){
					    legalRepresentative.setSecondNationality(holder.getRepresentedEntity().get(i).getLegalRepresentative().getSecondNationality());	
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getSex()!=null){
						legalRepresentative.setSex(holder.getRepresentedEntity().get(i).getLegalRepresentative().getSex());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getState()!=null){
						legalRepresentative.setState(holder.getRepresentedEntity().get(i).getLegalRepresentative().getState());
					}
					
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPersonType()!=null){
						legalRepresentative.setPersonType(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPersonType());
					}
					
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentSource()!=null){
						legalRepresentative.setDocumentSource(holder.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentSource());
					}
					
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentIssuanceDate()!=null){
						legalRepresentative.setDocumentSource(holder.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentIssuanceDate());
					}					
					legalRepresentative.setRepresentativeTypeDescription(PersonType.get(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPersonType()).getValue());				
					legalRepresentative.setDocumentTypeDescription(DocumentType.get(holder.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentType()).getValue());				
					legalRepresentative.setLegalRepresentativeFile(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalRepresentativeFile());					
					lstLegalRepresentative.add(legalRepresentative);					
					legalRepresentativeHelperBean.setLstLegalRepresentative(lstLegalRepresentative);
				}
			}		
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Pep exists.
	 *
	 * @param identificatorHolder the identificator holder
	 */
	public void pepExists(Long identificatorHolder){		
		try{			
			if(identificatorHolder!=null){				
				PepPerson objPepPerson  = holderServiceFacade.getPepPersonByIdHolderServiceFacade(identificatorHolder);			
				if(objPepPerson!=null){
					pepPerson = objPepPerson;
					indicatorPep = true;			
				} else {
					indicatorPep = false;
				}
			 }			
			}
			catch (Exception ex) {
				excepcion.fire(new ExceptionToCatchEvent(ex));
			}
	}
	
	
	/**
	 * Gets the holder history state.
	 *
	 * @param event the event
	 * @return the holder history state
	 */
	public void getHolderHistoryState(ActionEvent event){
		try{
		listHistoryCui = new ArrayList<HolderHistoryState>();
		Holder holder = (Holder) (event.getComponent().getAttributes().get("attributeCui"));
		listHistoryCui =  holderServiceFacade.getHistoryRNTHolderServiceFacade(holder.getIdHolderPk());
		JSFUtilities.executeJavascriptFunction("PF('widgHistoryCuiDialog').show()");
		}
		catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Gets the holder creation history.
	 *
	 * @return the holder creation history
	 */
	public HolderHistory getHolderCreationHistory() {
		return holderHistory;
	}

	/**
	 * Sets the holder creation history.
	 *
	 * @param holderCreationHistory the new holder creation history
	 */
	public void setHolderCreationHistory(HolderHistory holderCreationHistory) {
		this.holderHistory = holderCreationHistory;
	}

	/**
	 * Checks if is flag minor.
	 *
	 * @return true, if is flag minor
	 */
	public boolean isFlagMinor() {
		return flagMinor;
	}

	/**
	 * Sets the flag minor.
	 *
	 * @param flagMinor the new flag minor
	 */
	public void setFlagMinor(boolean flagMinor) {
		this.flagMinor = flagMinor;
	}

	/**
	 * Checks if is flag is pep.
	 *
	 * @return true, if is flag is pep
	 */
	public boolean isFlagIsPEP() {
		return flagIsPEP;
	}

	/**
	 * Sets the flag is pep.
	 *
	 * @param flagIsPEP the new flag is pep
	 */
	public void setFlagIsPEP(boolean flagIsPEP) {
		this.flagIsPEP = flagIsPEP;
	}

	/**
	 * Checks if is flag copy legal holder.
	 *
	 * @return true, if is flag copy legal holder
	 */
	public boolean isFlagCopyLegalHolder() {
		return flagCopyLegalHolder;
	}

	/**
	 * Sets the flag copy legal holder.
	 *
	 * @param flagCopyLegalHolder the new flag copy legal holder
	 */
	public void setFlagCopyLegalHolder(boolean flagCopyLegalHolder) {
		this.flagCopyLegalHolder = flagCopyLegalHolder;
	}

	/**
	 * Checks if is flag holder natural person.
	 *
	 * @return true, if is flag holder natural person
	 */
	public boolean isFlagHolderNaturalPerson() {
		return flagHolderNaturalPerson;
	}

	/**
	 * Sets the flag holder natural person.
	 *
	 * @param flagHolderNaturalPerson the new flag holder natural person
	 */
	public void setFlagHolderNaturalPerson(boolean flagHolderNaturalPerson) {
		this.flagHolderNaturalPerson = flagHolderNaturalPerson;
	}

	/**
	 * Checks if is flag holder juridic person.
	 *
	 * @return true, if is flag holder juridic person
	 */
	public boolean isFlagHolderJuridicPerson() {
		return flagHolderJuridicPerson;
	}

	/**
	 * Sets the flag holder juridic person.
	 *
	 * @param flagHolderJuridicPerson the new flag holder juridic person
	 */
	public void setFlagHolderJuridicPerson(boolean flagHolderJuridicPerson) {
		this.flagHolderJuridicPerson = flagHolderJuridicPerson;
	}

	/**
	 * Checks if is flag representative natural person.
	 *
	 * @return true, if is flag representative natural person
	 */
	public boolean isFlagRepresentativeNaturalPerson() {
		return flagRepresentativeNaturalPerson;
	}

	/**
	 * Sets the flag representative natural person.
	 *
	 * @param flagRepresentativeNaturalPerson the new flag representative natural person
	 */
	public void setFlagRepresentativeNaturalPerson(
			boolean flagRepresentativeNaturalPerson) {
		this.flagRepresentativeNaturalPerson = flagRepresentativeNaturalPerson;
	}

	/**
	 * Checks if is flag representative juridic person.
	 *
	 * @return true, if is flag representative juridic person
	 */
	public boolean isFlagRepresentativeJuridicPerson() {
		return flagRepresentativeJuridicPerson;
	}

	/**
	 * Sets the flag representative juridic person.
	 *
	 * @param flagRepresentativeJuridicPerson the new flag representative juridic person
	 */
	public void setFlagRepresentativeJuridicPerson(
			boolean flagRepresentativeJuridicPerson) {
		this.flagRepresentativeJuridicPerson = flagRepresentativeJuridicPerson;
	}

	/**
	 * Gets the represented entity.
	 *
	 * @return the represented entity
	 */
	public RepresentedEntity getRepresentedEntity() {
		return representedEntity;
	}

	/**
	 * Sets the represented entity.
	 *
	 * @param representedEntity the new represented entity
	 */
	public void setRepresentedEntity(RepresentedEntity representedEntity) {
		this.representedEntity = representedEntity;
	}

	/**
	 * Checks if is disabled country resident holder.
	 *
	 * @return true, if is disabled country resident holder
	 */
	public boolean isDisabledCountryResidentHolder() {
		return disabledCountryResidentHolder;
	}

	/**
	 * Sets the disabled country resident holder.
	 *
	 * @param disabledCountryResidentHolder the new disabled country resident holder
	 */
	public void setDisabledCountryResidentHolder(
			boolean disabledCountryResidentHolder) {
		this.disabledCountryResidentHolder = disabledCountryResidentHolder;
	}

	/**
	 * Checks if is disabled country resident representative.
	 *
	 * @return true, if is disabled country resident representative
	 */
	public boolean isDisabledCountryResidentRepresentative() {
		return disabledCountryResidentRepresentative;
	}

	/**
	 * Sets the disabled country resident representative.
	 *
	 * @param disabledCountryResidentRepresentative the new disabled country resident representative
	 */
	public void setDisabledCountryResidentRepresentative(
			boolean disabledCountryResidentRepresentative) {
		this.disabledCountryResidentRepresentative = disabledCountryResidentRepresentative;
	}

	/**
	 * Checks if is flag copy legal representative.
	 *
	 * @return true, if is flag copy legal representative
	 */
	public boolean isFlagCopyLegalRepresentative() {
		return flagCopyLegalRepresentative;
	}

	/**
	 * Sets the flag copy legal representative.
	 *
	 * @param flagCopyLegalRepresentative the new flag copy legal representative
	 */
	public void setFlagCopyLegalRepresentative(
			boolean flagCopyLegalRepresentative) {
		this.flagCopyLegalRepresentative = flagCopyLegalRepresentative;
	}

	/**
	 * Gets the legal representative array.
	 *
	 * @return the legal representative array
	 */
	public LegalRepresentative[] getLegalRepresentativeArray() {
		return legalRepresentativeArray;
	}

	/**
	 * Sets the legal representative array.
	 *
	 * @param legalRepresentativeArray the new legal representative array
	 */
	public void setLegalRepresentativeArray(
			LegalRepresentative[] legalRepresentativeArray) {
		this.legalRepresentativeArray = legalRepresentativeArray;
	}

	/**
	 * Gets the list juridic class.
	 *
	 * @return the list juridic class
	 */
	public List<ParameterTable> getListJuridicClass() {
		return listJuridicClass;
	}

	/**
	 * Sets the list juridic class.
	 *
	 * @param listJuridicClass the new list juridic class
	 */
	public void setListJuridicClass(List<ParameterTable> listJuridicClass) {
		this.listJuridicClass = listJuridicClass;
	}

	/**
	 * Gets the list person type.
	 *
	 * @return the list person type
	 */
	public List<ParameterTable> getListPersonType() {
		return listPersonType;
	}

	/**
	 * Sets the list person type.
	 *
	 * @param listPersonType the new list person type
	 */
	public void setListPersonType(List<ParameterTable> listPersonType) {
		this.listPersonType = listPersonType;
	}

	/**
	 * Gets the list boolean.
	 *
	 * @return the list boolean
	 */
	public List<BooleanType> getListBoolean() {
		return listBoolean;
	}

	/**
	 * Sets the list boolean.
	 *
	 * @param listBoolean the new list boolean
	 */
	public void setListBoolean(List<BooleanType> listBoolean) {
		this.listBoolean = listBoolean;
	}

	/**
	 * Gets the list document type.
	 *
	 * @return the list document type
	 */
	public List<ParameterTable> getListDocumentType() {
		return listDocumentType;
	}

	/**
	 * Sets the list document type.
	 *
	 * @param listDocumentType the new list document type
	 */
	public void setListDocumentType(List<ParameterTable> listDocumentType) {
		this.listDocumentType = listDocumentType;
	}

	/**
	 * Gets the list sex.
	 *
	 * @return the list sex
	 */
	public List<ParameterTable> getListSex() {
		return listSex;
	}

	/**
	 * Sets the list sex.
	 *
	 * @param listSex the new list sex
	 */
	public void setListSex(List<ParameterTable> listSex) {
		this.listSex = listSex;
	}

	/**
	 * Gets the pep person.
	 *
	 * @return the pep person
	 */
	public PepPerson getPepPerson() {
		return pepPerson;
	}

	/**
	 * Sets the pep person.
	 *
	 * @param pepPerson the new pep person
	 */
	public void setPepPerson(PepPerson pepPerson) {
		this.pepPerson = pepPerson;
	}

	/**
	 * Gets the list geographic location.
	 *
	 * @return the list geographic location
	 */
	public List<GeographicLocation> getListGeographicLocation() {
		return listGeographicLocation;
	}

	/**
	 * Sets the list geographic location.
	 *
	 * @param listGeographicLocation the new list geographic location
	 */
	public void setListGeographicLocation(
			List<GeographicLocation> listGeographicLocation) {
		this.listGeographicLocation = listGeographicLocation;
	}

	/**
	 * Checks if is disabled representative button.
	 *
	 * @return true, if is disabled representative button
	 */
	public boolean isDisabledRepresentativeButton() {
		return disabledRepresentativeButton;
	}

	/**
	 * Sets the disabled representative button.
	 *
	 * @param disabledRepresentativeButton the new disabled representative button
	 */
	public void setDisabledRepresentativeButton(
			boolean disabledRepresentativeButton) {
		this.disabledRepresentativeButton = disabledRepresentativeButton;
	}

	/**
	 * Gets the list province location.
	 *
	 * @return the list province location
	 */
	public List<GeographicLocation> getListProvinceLocation() {
		return listProvinceLocation;
	}

	/**
	 * Sets the list province location.
	 *
	 * @param listProvinceLocation the new list province location
	 */
	public void setListProvinceLocation(
			List<GeographicLocation> listProvinceLocation) {
		this.listProvinceLocation = listProvinceLocation;
	}

	/**
	 * Gets the list sector location.
	 *
	 * @return the list sector location
	 */
	public List<GeographicLocation> getListSectorLocation() {
		return listSectorLocation;
	}

	/**
	 * Sets the list sector location.
	 *
	 * @param listSectorLocation the new list sector location
	 */
	public void setListSectorLocation(
			List<GeographicLocation> listSectorLocation) {
		this.listSectorLocation = listSectorLocation;
	}

	/**
	 * Gets the list province postal location.
	 *
	 * @return the list province postal location
	 */
	public List<GeographicLocation> getListProvincePostalLocation() {
		return listProvincePostalLocation;
	}

	/**
	 * Sets the list province postal location.
	 *
	 * @param listProvincePostalLocation the new list province postal location
	 */
	public void setListProvincePostalLocation(
			List<GeographicLocation> listProvincePostalLocation) {
		this.listProvincePostalLocation = listProvincePostalLocation;
	}

	/**
	 * Gets the list sector postal location.
	 *
	 * @return the list sector postal location
	 */
	public List<GeographicLocation> getListSectorPostalLocation() {
		return listSectorPostalLocation;
	}

	/**
	 * Sets the list sector postal location.
	 *
	 * @param listSectorPostalLocation the new list sector postal location
	 */
	public void setListSectorPostalLocation(
			List<GeographicLocation> listSectorPostalLocation) {
		this.listSectorPostalLocation = listSectorPostalLocation;
	}

	/**
	 * Checks if is disabled cmb postal country.
	 *
	 * @return true, if is disabled cmb postal country
	 */
	public boolean isDisabledCmbPostalCountry() {
		return disabledCmbPostalCountry;
	}

	/**
	 * Sets the disabled cmb postal country.
	 *
	 * @param disabledCmbPostalCountry the new disabled cmb postal country
	 */
	public void setDisabledCmbPostalCountry(boolean disabledCmbPostalCountry) {
		this.disabledCmbPostalCountry = disabledCmbPostalCountry;
	}

	/**
	 * Checks if is disabled cmb postal province.
	 *
	 * @return true, if is disabled cmb postal province
	 */
	public boolean isDisabledCmbPostalProvince() {
		return disabledCmbPostalProvince;
	}

	/**
	 * Sets the disabled cmb postal province.
	 *
	 * @param disabledCmbPostalProvince the new disabled cmb postal province
	 */
	public void setDisabledCmbPostalProvince(boolean disabledCmbPostalProvince) {
		this.disabledCmbPostalProvince = disabledCmbPostalProvince;
	}

	/**
	 * Checks if is disabled cmb postal sector.
	 *
	 * @return true, if is disabled cmb postal sector
	 */
	public boolean isDisabledCmbPostalSector() {
		return disabledCmbPostalSector;
	}

	/**
	 * Sets the disabled cmb postal sector.
	 *
	 * @param disabledCmbPostalSector the new disabled cmb postal sector
	 */
	public void setDisabledCmbPostalSector(boolean disabledCmbPostalSector) {
		this.disabledCmbPostalSector = disabledCmbPostalSector;
	}

	/**
	 * Checks if is disabled input postal address.
	 *
	 * @return true, if is disabled input postal address
	 */
	public boolean isDisabledInputPostalAddress() {
		return disabledInputPostalAddress;
	}

	/**
	 * Sets the disabled input postal address.
	 *
	 * @param disabledInputPostalAddress the new disabled input postal address
	 */
	public void setDisabledInputPostalAddress(boolean disabledInputPostalAddress) {
		this.disabledInputPostalAddress = disabledInputPostalAddress;
	}

	/**
	 * Gets the list representative class.
	 *
	 * @return the list representative class
	 */
	public List<ParameterTable> getListRepresentativeClass() {
		return listRepresentativeClass;
	}

	/**
	 * Sets the list representative class.
	 *
	 * @param listRepresentativeClass the new list representative class
	 */
	public void setListRepresentativeClass(
			List<ParameterTable> listRepresentativeClass) {
		this.listRepresentativeClass = listRepresentativeClass;
	}

	/**
	 * Gets the list province location representative.
	 *
	 * @return the list province location representative
	 */
	public List<GeographicLocation> getListProvinceLocationRepresentative() {
		return listProvinceLocationRepresentative;
	}

	/**
	 * Sets the list province location representative.
	 *
	 * @param listProvinceLocationRepresentative the new list province location representative
	 */
	public void setListProvinceLocationRepresentative(
			List<GeographicLocation> listProvinceLocationRepresentative) {
		this.listProvinceLocationRepresentative = listProvinceLocationRepresentative;
	}

	/**
	 * Gets the list sector location representative.
	 *
	 * @return the list sector location representative
	 */
	public List<GeographicLocation> getListSectorLocationRepresentative() {
		return listSectorLocationRepresentative;
	}

	/**
	 * Sets the list sector location representative.
	 *
	 * @param listSectorLocationRepresentative the new list sector location representative
	 */
	public void setListSectorLocationRepresentative(
			List<GeographicLocation> listSectorLocationRepresentative) {
		this.listSectorLocationRepresentative = listSectorLocationRepresentative;
	}

	/**
	 * Gets the list province postal location representative.
	 *
	 * @return the list province postal location representative
	 */
	public List<GeographicLocation> getListProvincePostalLocationRepresentative() {
		return listProvincePostalLocationRepresentative;
	}

	/**
	 * Sets the list province postal location representative.
	 *
	 * @param listProvincePostalLocationRepresentative the new list province postal location representative
	 */
	public void setListProvincePostalLocationRepresentative(
			List<GeographicLocation> listProvincePostalLocationRepresentative) {
		this.listProvincePostalLocationRepresentative = listProvincePostalLocationRepresentative;
	}

	/**
	 * Gets the list sector postal location representative.
	 *
	 * @return the list sector postal location representative
	 */
	public List<GeographicLocation> getListSectorPostalLocationRepresentative() {
		return listSectorPostalLocationRepresentative;
	}

	/**
	 * Sets the list sector postal location representative.
	 *
	 * @param listSectorPostalLocationRepresentative the new list sector postal location representative
	 */
	public void setListSectorPostalLocationRepresentative(
			List<GeographicLocation> listSectorPostalLocationRepresentative) {
		this.listSectorPostalLocationRepresentative = listSectorPostalLocationRepresentative;
	}

	/**
	 * Checks if is disabled cmb postal country representative.
	 *
	 * @return true, if is disabled cmb postal country representative
	 */
	public boolean isDisabledCmbPostalCountryRepresentative() {
		return disabledCmbPostalCountryRepresentative;
	}

	/**
	 * Sets the disabled cmb postal country representative.
	 *
	 * @param disabledCmbPostalCountryRepresentative the new disabled cmb postal country representative
	 */
	public void setDisabledCmbPostalCountryRepresentative(
			boolean disabledCmbPostalCountryRepresentative) {
		this.disabledCmbPostalCountryRepresentative = disabledCmbPostalCountryRepresentative;
	}

	/**
	 * Checks if is disabled cmb postal province representative.
	 *
	 * @return true, if is disabled cmb postal province representative
	 */
	public boolean isDisabledCmbPostalProvinceRepresentative() {
		return disabledCmbPostalProvinceRepresentative;
	}

	/**
	 * Sets the disabled cmb postal province representative.
	 *
	 * @param disabledCmbPostalProvinceRepresentative the new disabled cmb postal province representative
	 */
	public void setDisabledCmbPostalProvinceRepresentative(
			boolean disabledCmbPostalProvinceRepresentative) {
		this.disabledCmbPostalProvinceRepresentative = disabledCmbPostalProvinceRepresentative;
	}

	/**
	 * Checks if is disabled cmb postal sector representative.
	 *
	 * @return true, if is disabled cmb postal sector representative
	 */
	public boolean isDisabledCmbPostalSectorRepresentative() {
		return disabledCmbPostalSectorRepresentative;
	}

	/**
	 * Sets the disabled cmb postal sector representative.
	 *
	 * @param disabledCmbPostalSectorRepresentative the new disabled cmb postal sector representative
	 */
	public void setDisabledCmbPostalSectorRepresentative(
			boolean disabledCmbPostalSectorRepresentative) {
		this.disabledCmbPostalSectorRepresentative = disabledCmbPostalSectorRepresentative;
	}

	/**
	 * Checks if is disabled input postal address representative.
	 *
	 * @return true, if is disabled input postal address representative
	 */
	public boolean isDisabledInputPostalAddressRepresentative() {
		return disabledInputPostalAddressRepresentative;
	}

	/**
	 * Sets the disabled input postal address representative.
	 *
	 * @param disabledInputPostalAddressRepresentative the new disabled input postal address representative
	 */
	public void setDisabledInputPostalAddressRepresentative(
			boolean disabledInputPostalAddressRepresentative) {
		this.disabledInputPostalAddressRepresentative = disabledInputPostalAddressRepresentative;
	}

	/**
	 * Gets the holder history.
	 *
	 * @return the holder history
	 */
	public HolderHistory getHolderHistory() {
		return holderHistory;
	}

	/**
	 * Sets the holder history.
	 *
	 * @param holderHistory the new holder history
	 */
	public void setHolderHistory(HolderHistory holderHistory) {
		this.holderHistory = holderHistory;
	}

	/**
	 * Gets the legal representative history.
	 *
	 * @return the legal representative history
	 */
	public LegalRepresentativeHistory getLegalRepresentativeHistory() {
		return legalRepresentativeHistory;
	}

	/**
	 * Sets the legal representative history.
	 *
	 * @param legalRepresentativeHistory the new legal representative history
	 */
	public void setLegalRepresentativeHistory(
			LegalRepresentativeHistory legalRepresentativeHistory) {
		this.legalRepresentativeHistory = legalRepresentativeHistory;
	}

	/**
	 * Checks if is flag representative is pep.
	 *
	 * @return true, if is flag representative is pep
	 */
	public boolean isFlagRepresentativeIsPEP() {
		return flagRepresentativeIsPEP;
	}

	/**
	 * Sets the flag representative is pep.
	 *
	 * @param flagRepresentativeIsPEP the new flag representative is pep
	 */
	public void setFlagRepresentativeIsPEP(boolean flagRepresentativeIsPEP) {
		this.flagRepresentativeIsPEP = flagRepresentativeIsPEP;
	}

	/**
	 * Gets the lst parameter archive header.
	 *
	 * @return the lst parameter archive header
	 */
	public List<ParameterTable> getLstParameterArchiveHeader() {
		return lstParameterArchiveHeader;
	}

	/**
	 * Sets the lst parameter archive header.
	 *
	 * @param lstParameterArchiveHeader the new lst parameter archive header
	 */
	public void setLstParameterArchiveHeader(
			List<ParameterTable> lstParameterArchiveHeader) {
		this.lstParameterArchiveHeader = lstParameterArchiveHeader;
	}

	/**
	 * Checks if is flag cmb representative class.
	 *
	 * @return true, if is flag cmb representative class
	 */
	public boolean isFlagCmbRepresentativeClass() {
		return flagCmbRepresentativeClass;
	}

	/**
	 * Sets the flag cmb representative class.
	 *
	 * @param flagCmbRepresentativeClass the new flag cmb representative class
	 */
	public void setFlagCmbRepresentativeClass(boolean flagCmbRepresentativeClass) {
		this.flagCmbRepresentativeClass = flagCmbRepresentativeClass;
	}

	/**
	 * Gets the legal representative history array.
	 *
	 * @return the legal representative history array
	 */
	public LegalRepresentativeHistory[] getLegalRepresentativeHistoryArray() {
		return legalRepresentativeHistoryArray;
	}

	/**
	 * Sets the legal representative history array.
	 *
	 * @param legalRepresentativeHistoryArray the new legal representative history array
	 */
	public void setLegalRepresentativeHistoryArray(
			LegalRepresentativeHistory[] legalRepresentativeHistoryArray) {
		this.legalRepresentativeHistoryArray = legalRepresentativeHistoryArray;
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * Gets the list participant.
	 *
	 * @return the list participant
	 */
	public List<Participant> getListParticipant() {
		return listParticipant;
	}

	/**
	 * Sets the list participant.
	 *
	 * @param listParticipant the new list participant
	 */
	public void setListParticipant(List<Participant> listParticipant) {
		this.listParticipant = listParticipant;
	}

	/**
	 * Gets the holder request selected.
	 *
	 * @return the holder request selected
	 */
	public HolderRequest getHolderRequestSelected() {
		return holderRequestSelected;
	}

	/**
	 * Sets the holder request selected.
	 *
	 * @param holderRequestSelected the new holder request selected
	 */
	public void setHolderRequestSelected(HolderRequest holderRequestSelected) {
		this.holderRequestSelected = holderRequestSelected;
	}

	/**
	 * Checks if is flag is participant.
	 *
	 * @return true, if is flag is participant
	 */
	public boolean isFlagIsParticipant() {
		return flagIsParticipant;
	}

	/**
	 * Sets the flag is participant.
	 *
	 * @param flagIsParticipant the new flag is participant
	 */
	public void setFlagIsParticipant(boolean flagIsParticipant) {
		this.flagIsParticipant = flagIsParticipant;
	}

	/**
	 * Checks if is flag action confirm.
	 *
	 * @return true, if is flag action confirm
	 */
	public boolean isFlagActionConfirm() {
		return flagActionConfirm;
	}

	/**
	 * Sets the flag action confirm.
	 *
	 * @param flagActionConfirm the new flag action confirm
	 */
	public void setFlagActionConfirm(boolean flagActionConfirm) {
		this.flagActionConfirm = flagActionConfirm;
	}

	/**
	 * Checks if is flag action reject.
	 *
	 * @return true, if is flag action reject
	 */
	public boolean isFlagActionReject() {
		return flagActionReject;
	}

	/**
	 * Sets the flag action reject.
	 *
	 * @param flagActionReject the new flag action reject
	 */
	public void setFlagActionReject(boolean flagActionReject) {
		this.flagActionReject = flagActionReject;
	}

	/**
	 * Checks if is flag action annular.
	 *
	 * @return true, if is flag action annular
	 */
	public boolean isFlagActionAnnular() {
		return flagActionAnnular;
	}

	/**
	 * Sets the flag action annular.
	 *
	 * @param flagActionAnnular the new flag action annular
	 */
	public void setFlagActionAnnular(boolean flagActionAnnular) {
		this.flagActionAnnular = flagActionAnnular;
	}

	/**
	 * Checks if is disabled cmb participant.
	 *
	 * @return true, if is disabled cmb participant
	 */
	public boolean isDisabledCmbParticipant() {
		return disabledCmbParticipant;
	}

	/**
	 * Sets the disabled cmb participant.
	 *
	 * @param disabledCmbParticipant the new disabled cmb participant
	 */
	public void setDisabledCmbParticipant(boolean disabledCmbParticipant) {
		this.disabledCmbParticipant = disabledCmbParticipant;
	}

	/**
	 * Gets the legal representative.
	 *
	 * @return the legal representative
	 */
	public LegalRepresentative getLegalRepresentative() {
		return legalRepresentative;
	}

	/**
	 * Sets the legal representative.
	 *
	 * @param legalRepresentative the new legal representative
	 */
	public void setLegalRepresentative(LegalRepresentative legalRepresentative) {
		this.legalRepresentative = legalRepresentative;
	}

	/**
	 * Gets the list state holder request.
	 *
	 * @return the list state holder request
	 */
	public List<ParameterTable> getListStateHolderRequest() {
		return listStateHolderRequest;
	}

	/**
	 * Sets the list state holder request.
	 *
	 * @param listStateHolderRequest the new list state holder request
	 */
	public void setListStateHolderRequest(
			List<ParameterTable> listStateHolderRequest) {
		this.listStateHolderRequest = listStateHolderRequest;
	}

	/**
	 * Checks if is numeric type document holder.
	 *
	 * @return true, if is numeric type document holder
	 */
	public boolean isNumericTypeDocumentHolder() {
		return numericTypeDocumentHolder;
	}

	/**
	 * Sets the numeric type document holder.
	 *
	 * @param numericTypeDocumentHolder the new numeric type document holder
	 */
	public void setNumericTypeDocumentHolder(boolean numericTypeDocumentHolder) {
		this.numericTypeDocumentHolder = numericTypeDocumentHolder;
	}

	/**
	 * Checks if is numeric second type document holder.
	 *
	 * @return true, if is numeric second type document holder
	 */
	public boolean isNumericSecondTypeDocumentHolder() {
		return numericSecondTypeDocumentHolder;
	}

	/**
	 * Sets the numeric second type document holder.
	 *
	 * @param numericSecondTypeDocumentHolder the new numeric second type document holder
	 */
	public void setNumericSecondTypeDocumentHolder(
			boolean numericSecondTypeDocumentHolder) {
		this.numericSecondTypeDocumentHolder = numericSecondTypeDocumentHolder;
	}

	/**
	 * Gets the max lenght document number holder.
	 *
	 * @return the max lenght document number holder
	 */
	public Integer getMaxLenghtDocumentNumberHolder() {
		return maxLenghtDocumentNumberHolder;
	}

	/**
	 * Sets the max lenght document number holder.
	 *
	 * @param maxLenghtDocumentNumberHolder the new max lenght document number holder
	 */
	public void setMaxLenghtDocumentNumberHolder(
			Integer maxLenghtDocumentNumberHolder) {
		this.maxLenghtDocumentNumberHolder = maxLenghtDocumentNumberHolder;
	}

	/**
	 * Gets the max lenght second document number holder.
	 *
	 * @return the max lenght second document number holder
	 */
	public Integer getMaxLenghtSecondDocumentNumberHolder() {
		return maxLenghtSecondDocumentNumberHolder;
	}

	/**
	 * Sets the max lenght second document number holder.
	 *
	 * @param maxLenghtSecondDocumentNumberHolder the new max lenght second document number holder
	 */
	public void setMaxLenghtSecondDocumentNumberHolder(
			Integer maxLenghtSecondDocumentNumberHolder) {
		this.maxLenghtSecondDocumentNumberHolder = maxLenghtSecondDocumentNumberHolder;
	}

	
	/**
	 * Gets the option selected one radio.
	 *
	 * @return the option selected one radio
	 */
	public Integer getOptionSelectedOneRadio() {
		return optionSelectedOneRadio;
	}

	/**
	 * Sets the option selected one radio.
	 *
	 * @param optionSelectedOneRadio the new option selected one radio
	 */
	public void setOptionSelectedOneRadio(Integer optionSelectedOneRadio) {
		this.optionSelectedOneRadio = optionSelectedOneRadio;
	}

	/**
	 * Checks if is disabled document number.
	 *
	 * @return true, if is disabled document number
	 */
	public boolean isDisabledDocumentNumber() {
		return disabledDocumentNumber;
	}

	/**
	 * Sets the disabled document number.
	 *
	 * @param disabledDocumentNumber the new disabled document number
	 */
	public void setDisabledDocumentNumber(boolean disabledDocumentNumber) {
		this.disabledDocumentNumber = disabledDocumentNumber;
	}

	/**
	 * Checks if is disabled rnt code.
	 *
	 * @return true, if is disabled rnt code
	 */
	public boolean isDisabledCuiCode() {
		return disabledCuiCode;
	}

	/**
	 * Sets the disabled rnt code.
	 *
	 * @param disabledCuiCode the new disabled cui code
	 */
	public void setDisabledCuiCode(boolean disabledCuiCode) {
		this.disabledCuiCode = disabledCuiCode;
	}

	/**
	 * Checks if is disabled natural type person.
	 *
	 * @return true, if is disabled natural type person
	 */
	public boolean isDisabledNaturalTypePerson() {
		return disabledNaturalTypePerson;
	}

	/**
	 * Sets the disabled natural type person.
	 *
	 * @param disabledNaturalTypePerson the new disabled natural type person
	 */
	public void setDisabledNaturalTypePerson(boolean disabledNaturalTypePerson) {
		this.disabledNaturalTypePerson = disabledNaturalTypePerson;
	}

	/**
	 * Checks if is disabled juridic type person.
	 *
	 * @return true, if is disabled juridic type person
	 */
	public boolean isDisabledJuridicTypePerson() {
		return disabledJuridicTypePerson;
	}

	/**
	 * Sets the disabled juridic type person.
	 *
	 * @param disabledJuridicTypePerson the new disabled juridic type person
	 */
	public void setDisabledJuridicTypePerson(boolean disabledJuridicTypePerson) {
		this.disabledJuridicTypePerson = disabledJuridicTypePerson;
	}

	/**
	 * Checks if is disabled cmb type document.
	 *
	 * @return true, if is disabled cmb type document
	 */
	public boolean isDisabledCmbTypeDocument() {
		return disabledCmbTypeDocument;
	}

	/**
	 * Sets the disabled cmb type document.
	 *
	 * @param disabledCmbTypeDocument the new disabled cmb type document
	 */
	public void setDisabledCmbTypeDocument(boolean disabledCmbTypeDocument) {
		this.disabledCmbTypeDocument = disabledCmbTypeDocument;
	}

	/**
	 * Checks if is disabled cmb type person.
	 *
	 * @return true, if is disabled cmb type person
	 */
	public boolean isDisabledCmbTypePerson() {
		return disabledCmbTypePerson;
	}

	/**
	 * Sets the disabled cmb type person.
	 *
	 * @param disabledCmbTypePerson the new disabled cmb type person
	 */
	public void setDisabledCmbTypePerson(boolean disabledCmbTypePerson) {
		this.disabledCmbTypePerson = disabledCmbTypePerson;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Checks if is indicator pep.
	 *
	 * @return true, if is indicator pep
	 */
	public boolean isIndicatorPep() {
		return indicatorPep;
	}

	/**
	 * Sets the indicator pep.
	 *
	 * @param indicatorPep the new indicator pep
	 */
	public void setIndicatorPep(boolean indicatorPep) {
		this.indicatorPep = indicatorPep;
	}

	/**
	 * Gets the pep person legal.
	 *
	 * @return the pep person legal
	 */
	public PepPerson getPepPersonLegal() {
		return pepPersonLegal;
	}

	/**
	 * Sets the pep person legal.
	 *
	 * @param pepPersonLegal the new pep person legal
	 */
	public void setPepPersonLegal(PepPerson pepPersonLegal) {
		this.pepPersonLegal = pepPersonLegal;
	}

	/**
	 * Gets the list category pep.
	 *
	 * @return the list category pep
	 */
	public List<ParameterTable> getListCategoryPep() {
		return listCategoryPep;
	}

	/**
	 * Sets the list category pep.
	 *
	 * @param listCategoryPep the new list category pep
	 */
	public void setListCategoryPep(List<ParameterTable> listCategoryPep) {
		this.listCategoryPep = listCategoryPep;
	}

	/**
	 * Gets the list role pep.
	 *
	 * @return the list role pep
	 */
	public List<ParameterTable> getListRolePep() {
		return listRolePep;
	}

	/**
	 * Sets the list role pep.
	 *
	 * @param listRolePep the new list role pep
	 */
	public void setListRolePep(List<ParameterTable> listRolePep) {
		this.listRolePep = listRolePep;
	}

	/**
	 * Gets the list economic sector.
	 *
	 * @return the list economic sector
	 */
	public List<ParameterTable> getListEconomicSector() {
		return listEconomicSector;
	}

	/**
	 * Sets the list economic sector.
	 *
	 * @param listEconomicSector the new list economic sector
	 */
	public void setListEconomicSector(List<ParameterTable> listEconomicSector) {
		this.listEconomicSector = listEconomicSector;
	}

	/**
	 * Gets the list economic activity.
	 *
	 * @return the list economic activity
	 */
	public List<ParameterTable> getListEconomicActivity() {
		return listEconomicActivity;
	}

	/**
	 * Sets the list economic activity.
	 *
	 * @param listEconomicActivity the new list economic activity
	 */
	public void setListEconomicActivity(List<ParameterTable> listEconomicActivity) {
		this.listEconomicActivity = listEconomicActivity;
	}

	/**
	 * Gets the lst holder file.
	 *
	 * @return the lst holder file
	 */
	public List<HolderFile> getLstHolderFile() {
		return lstHolderFile;
	}

	/**
	 * Sets the lst holder file.
	 *
	 * @param lstHolderFile the new lst holder file
	 */
	public void setLstHolderFile(List<HolderFile> lstHolderFile) {
		this.lstHolderFile = lstHolderFile;
	}

	/**
	 * Gets the lst legal representative file.
	 *
	 * @return the lst legal representative file
	 */
	public List<LegalRepresentativeFile> getLstLegalRepresentativeFile() {
		return lstLegalRepresentativeFile;
	}

	/**
	 * Sets the lst legal representative file.
	 *
	 * @param lstLegalRepresentativeFile the new lst legal representative file
	 */
	public void setLstLegalRepresentativeFile(
			List<LegalRepresentativeFile> lstLegalRepresentativeFile) {
		this.lstLegalRepresentativeFile = lstLegalRepresentativeFile;
	}

	/**
	 * Gets the list history cui.
	 *
	 * @return the list history cui
	 */
	public List<HolderHistoryState> getListHistoryCui() {
		return listHistoryCui;
	}

	/**
	 * Sets the list history cui.
	 *
	 * @param listHistoryCui the new list history cui
	 */
	public void setListHistoryCui(List<HolderHistoryState> listHistoryCui) {
		this.listHistoryCui = listHistoryCui;
	}

	/**
	 * Gets the list state holder.
	 *
	 * @return the list state holder
	 */
	public List<ParameterTable> getListStateHolder() {
		return listStateHolder;
	}

	/**
	 * Sets the list state holder.
	 *
	 * @param listStateHolder the new list state holder
	 */
	public void setListStateHolder(List<ParameterTable> listStateHolder) {
		this.listStateHolder = listStateHolder;
	}

	/**
	 * Gets the legal representative helper bean.
	 *
	 * @return the legal representative helper bean
	 */
	public LegalRepresentativeHelperBean getLegalRepresentativeHelperBean() {
		return legalRepresentativeHelperBean;
	}

	/**
	 * Sets the legal representative helper bean.
	 *
	 * @param legalRepresentativeHelperBean the new legal representative helper bean
	 */
	public void setLegalRepresentativeHelperBean(
			LegalRepresentativeHelperBean legalRepresentativeHelperBean) {
		this.legalRepresentativeHelperBean = legalRepresentativeHelperBean;
	}

	/**
	 * Gets the list department location.
	 *
	 * @return the list department location
	 */
	public List<GeographicLocation> getListDepartmentLocation() {
		return listDepartmentLocation;
	}

	/**
	 * Sets the list department location.
	 *
	 * @param listDepartmentLocation the new list department location
	 */
	public void setListDepartmentLocation(
			List<GeographicLocation> listDepartmentLocation) {
		this.listDepartmentLocation = listDepartmentLocation;
	}

	/**
	 * Gets the list municipality location.
	 *
	 * @return the list municipality location
	 */
	public List<GeographicLocation> getListMunicipalityLocation() {
		return listMunicipalityLocation;
	}

	/**
	 * Sets the list municipality location.
	 *
	 * @param listMunicipalityLocation the new list municipality location
	 */
	public void setListMunicipalityLocation(
			List<GeographicLocation> listMunicipalityLocation) {
		this.listMunicipalityLocation = listMunicipalityLocation;
	}

	/**
	 * Gets the list department postal location.
	 *
	 * @return the list department postal location
	 */
	public List<GeographicLocation> getListDepartmentPostalLocation() {
		return listDepartmentPostalLocation;
	}

	/**
	 * Sets the list department postal location.
	 *
	 * @param listDepartmentPostalLocation the new list department postal location
	 */
	public void setListDepartmentPostalLocation(
			List<GeographicLocation> listDepartmentPostalLocation) {
		this.listDepartmentPostalLocation = listDepartmentPostalLocation;
	}

	/**
	 * Gets the list municipality postal location.
	 *
	 * @return the list municipality postal location
	 */
	public List<GeographicLocation> getListMunicipalityPostalLocation() {
		return listMunicipalityPostalLocation;
	}

	/**
	 * Sets the list municipality postal location.
	 *
	 * @param listMunicipalityPostalLocation the new list municipality postal location
	 */
	public void setListMunicipalityPostalLocation(
			List<GeographicLocation> listMunicipalityPostalLocation) {
		this.listMunicipalityPostalLocation = listMunicipalityPostalLocation;
	}
	
	/**
	 * Gets the list document source.
	 *
	 * @return the list document source
	 */
	public List<ParameterTable> getListDocumentSource() {
		return listDocumentSource;
	}

	/**
	 * Sets the list document source.
	 *
	 * @param listDocumentSource the new list document source
	 */
	public void setListDocumentSource(List<ParameterTable> listDocumentSource) {
		this.listDocumentSource = listDocumentSource;
	}

	/**
	 * Gets the list document issuace date.
	 *
	 * @return the list document issuace date
	 */
	public LinkedHashMap<String, Integer> getListDocumentIssuaceDate() {
		return listDocumentIssuanceDate;
	}

	/**
	 * Sets the list document issuance date.
	 *
	 * @param listDocumentIssuanceDate the list document issuance date
	 */
	public void setListDocumentIssuanceDate(LinkedHashMap<String, Integer> listDocumentIssuanceDate) {
		this.listDocumentIssuanceDate = listDocumentIssuanceDate;
	}

	/**
	 * Checks if is disabled range dates.
	 *
	 * @return true, if is disabled range dates
	 */
	public boolean isDisabledRangeDates() {
		return disabledRangeDates;
	}

	/**
	 * Sets the disabled range dates.
	 *
	 * @param disabledRangeDates the new disabled range dates
	 */
	public void setDisabledRangeDates(boolean disabledRangeDates) {
		this.disabledRangeDates = disabledRangeDates;
	}

	/**
	 * Gets the list civil status.
	 *
	 * @return the list civil status
	 */
	public List<ParameterTable> getListCivilStatus() {
		return listCivilStatus;
	}

	/**
	 * Sets the list civil status.
	 *
	 * @param listCivilStatus the new list civil status
	 */
	public void setListCivilStatus(List<ParameterTable> listCivilStatus) {
		this.listCivilStatus = listCivilStatus;
	}

	/**
	 * Gets the list total income.
	 *
	 * @return the list total income
	 */
	public List<ParameterTable> getListTotalIncome() {
		return listTotalIncome;
	}

	/**
	 * Sets the list total income.
	 *
	 * @param listTotalIncome the new list total income
	 */
	public void setListTotalIncome(List<ParameterTable> listTotalIncome) {
		this.listTotalIncome = listTotalIncome;
	}

	/**
	 * Checks if is flag is issuer dpf institution.
	 *
	 * @return the flagIsIssuerDpfInstitution
	 */
	public boolean isFlagIsIssuerDpfInstitution() {
		return flagIsIssuerDpfInstitution;
	}

	/**
	 * Sets the flag is issuer dpf institution.
	 *
	 * @param flagIsIssuerDpfInstitution the flagIsIssuerDpfInstitution to set
	 */
	public void setFlagIsIssuerDpfInstitution(boolean flagIsIssuerDpfInstitution) {
		this.flagIsIssuerDpfInstitution = flagIsIssuerDpfInstitution;
	}

	public LinkedHashMap<String, Integer> getListDocumentIssuanceDate() {
		return listDocumentIssuanceDate;
	}

	public List<InvestorTypeModelTO> getListInvestorType() {
		return listInvestorType;
	}

	public void setListInvestorType(List<InvestorTypeModelTO> listInvestorType) {
		this.listInvestorType = listInvestorType;
	}

	public List<ParameterTable> getListPepGrade() {
		return listPepGrade;
	}

	public void setListPepGrade(List<ParameterTable> listPepGrade) {
		this.listPepGrade = listPepGrade;
	}

	public List<ParameterTable> getListEconomicActivityNaturalPerson() {
		return listEconomicActivityNaturalPerson;
	}

	public void setListEconomicActivityNaturalPerson(List<ParameterTable> listEconomicActivityNaturalPerson) {
		this.listEconomicActivityNaturalPerson = listEconomicActivityNaturalPerson;
	}

	public Integer getLinkForFamily() {
		return linkForFamily;
	}
	
}
