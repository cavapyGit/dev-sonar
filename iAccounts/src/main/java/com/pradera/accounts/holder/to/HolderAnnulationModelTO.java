package com.pradera.accounts.holder.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;

public class HolderAnnulationModelTO implements Serializable, Cloneable {
	
	private static final long serialVersionUID = 1L;
	
    private Long idAnnulationHolderPk;
    
    private Integer motiveAnnulationSelected;
    private String motiveAnnulationText;
    
    private String observationAnnulation;
    
    private Integer fileTypeAnnulationSelected;
    private String fileTypeAnnulationText;
    
    private byte[] fileAttached;
    
    private String fileName;
    
    private Integer stateAnnulation;
    private String stateAnnulationText;
    
    private Integer requestCancelTypeSelected;
    private String requestCancelDescription;
    
    private Date registryDate;
    
    private Long idParticipantSelected;
    
    private Holder holderSelected;
    
    private List<Participant> lstParticipant=new ArrayList<>();
    
    private List<ParameterTable> lstMoviveAnnulation=new ArrayList<>();
    
    private List<ParameterTable> lstFileTypeAnnulation=new ArrayList<>();
    
    private List<ParameterTable> lstState=new ArrayList<>();
    
    private List<ParameterTable> lstMotiveCancelRequest=new ArrayList<>();
    
    private String personTypeText;
    
    private Date initialDate;
    private Date finalDate;
    
    // para la session de admin o participantes
    private Long participantLogin;
    private boolean userIsParticipant;
    
    // estos atributos son para mostrarlos en la descripcion de la lista
    // de la pantalla search
    private String mnemonicPart;
    private String fullNameHolder;
    private Long idHolder;
    
    // para la pantalla de view
    private String participantCreator;
    
    // para las pistas de auditoria manuales
    private String auditUserName;
    private String auditIpAddress;
    private Long auditIdPrivilege;
    
    // first constructor
    public HolderAnnulationModelTO(){
    	holderSelected = new  Holder();
    }
    
    // constructor para la lista jpql del metodo
    public HolderAnnulationModelTO(Long idAnnulationHolderPk, Integer motiveAnnulation, String observationAnnulation,
    		Integer fileTypeAnnulation, Integer stateAnnulation,Date registryDate,
    		Long idParticipantPk, String mnemonic, Long idHolderPk, String fullName){
    	this.idAnnulationHolderPk = idAnnulationHolderPk;
    	this.motiveAnnulationSelected = motiveAnnulation;
    	this.observationAnnulation = observationAnnulation;
    	this.fileTypeAnnulationSelected = fileTypeAnnulation;
    	this.stateAnnulation = stateAnnulation;
    	this.registryDate = registryDate;
    	this.idParticipantSelected = idParticipantPk;
    	this.mnemonicPart = mnemonic;
    	this.idHolder = idHolderPk;
    	this.fullNameHolder = fullName;
    }
    
    /**
     * para la clonacion de objetos, asi evitamos la referencia
     */
    @Override
	public Object clone() {
		Object obj = null;
		try {
			obj = super.clone();
		} catch (CloneNotSupportedException ex) {
			System.out.println("No se puede clonar Objeto");
		}
		return obj;
	}

	public Long getIdAnnulationHolderPk() {
		return idAnnulationHolderPk;
	}

	public void setIdAnnulationHolderPk(Long idAnnulationHolderPk) {
		this.idAnnulationHolderPk = idAnnulationHolderPk;
	}

	public Integer getMotiveAnnulationSelected() {
		return motiveAnnulationSelected;
	}

	public void setMotiveAnnulationSelected(Integer motiveAnnulationSelected) {
		this.motiveAnnulationSelected = motiveAnnulationSelected;
	}

	public String getMotiveAnnulationText() {
		return motiveAnnulationText;
	}

	public void setMotiveAnnulationText(String motiveAnnulationText) {
		this.motiveAnnulationText = motiveAnnulationText;
	}

	public String getObservationAnnulation() {
		return observationAnnulation;
	}

	public void setObservationAnnulation(String observationAnnulation) {
		this.observationAnnulation = observationAnnulation;
	}

	public Integer getFileTypeAnnulationSelected() {
		return fileTypeAnnulationSelected;
	}

	public void setFileTypeAnnulationSelected(Integer fileTypeAnnulationSelected) {
		this.fileTypeAnnulationSelected = fileTypeAnnulationSelected;
	}

	public String getFileTypeAnnulationText() {
		return fileTypeAnnulationText;
	}

	public void setFileTypeAnnulationText(String fileTypeAnnulationText) {
		this.fileTypeAnnulationText = fileTypeAnnulationText;
	}


	public byte[] getFileAttached() {
		return fileAttached;
	}

	public void setFileAttached(byte[] fileAttached) {
		this.fileAttached = fileAttached;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Integer getStateAnnulation() {
		return stateAnnulation;
	}

	public void setStateAnnulation(Integer stateAnnulation) {
		this.stateAnnulation = stateAnnulation;
	}

	public String getStateAnnulationText() {
		return stateAnnulationText;
	}

	public void setStateAnnulationText(String stateAnnulationText) {
		this.stateAnnulationText = stateAnnulationText;
	}

	public Integer getRequestCancelTypeSelected() {
		return requestCancelTypeSelected;
	}

	public void setRequestCancelTypeSelected(Integer requestCancelTypeSelected) {
		this.requestCancelTypeSelected = requestCancelTypeSelected;
	}

	public String getRequestCancelDescription() {
		return requestCancelDescription;
	}

	public void setRequestCancelDescription(String requestCancelDescription) {
		this.requestCancelDescription = requestCancelDescription;
	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public Long getIdParticipantSelected() {
		return idParticipantSelected;
	}

	public void setIdParticipantSelected(Long idParticipantSelected) {
		this.idParticipantSelected = idParticipantSelected;
	}

	public Holder getHolderSelected() {
		return holderSelected;
	}

	public void setHolderSelected(Holder holderSelected) {
		this.holderSelected = holderSelected;
	}

	public List<Participant> getLstParticipant() {
		return lstParticipant;
	}

	public void setLstParticipant(List<Participant> lstParticipant) {
		this.lstParticipant = lstParticipant;
	}

	public List<ParameterTable> getLstMoviveAnnulation() {
		return lstMoviveAnnulation;
	}

	public void setLstMoviveAnnulation(List<ParameterTable> lstMoviveAnnulation) {
		this.lstMoviveAnnulation = lstMoviveAnnulation;
	}

	public List<ParameterTable> getLstFileTypeAnnulation() {
		return lstFileTypeAnnulation;
	}

	public void setLstFileTypeAnnulation(List<ParameterTable> lstFileTypeAnnulation) {
		this.lstFileTypeAnnulation = lstFileTypeAnnulation;
	}

	public List<ParameterTable> getLstState() {
		return lstState;
	}

	public void setLstState(List<ParameterTable> lstState) {
		this.lstState = lstState;
	}

	public List<ParameterTable> getLstMotiveCancelRequest() {
		return lstMotiveCancelRequest;
	}

	public void setLstMotiveCancelRequest(List<ParameterTable> lstMotiveCancelRequest) {
		this.lstMotiveCancelRequest = lstMotiveCancelRequest;
	}

	public String getPersonTypeText() {
		return personTypeText;
	}

	public void setPersonTypeText(String personTypeText) {
		this.personTypeText = personTypeText;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	public Long getParticipantLogin() {
		return participantLogin;
	}

	public void setParticipantLogin(Long participantLogin) {
		this.participantLogin = participantLogin;
	}

	public boolean isUserIsParticipant() {
		return userIsParticipant;
	}

	public void setUserIsParticipant(boolean userIsParticipant) {
		this.userIsParticipant = userIsParticipant;
	}

	public String getMnemonicPart() {
		return mnemonicPart;
	}

	public void setMnemonicPart(String mnemonicPart) {
		this.mnemonicPart = mnemonicPart;
	}

	public String getFullNameHolder() {
		return fullNameHolder;
	}

	public void setFullNameHolder(String fullNameHolder) {
		this.fullNameHolder = fullNameHolder;
	}

	public Long getIdHolder() {
		return idHolder;
	}

	public void setIdHolder(Long idHolder) {
		this.idHolder = idHolder;
	}

	public String getParticipantCreator() {
		return participantCreator;
	}

	public void setParticipantCreator(String participantCreator) {
		this.participantCreator = participantCreator;
	}

	public String getAuditUserName() {
		return auditUserName;
	}

	public void setAuditUserName(String auditUserName) {
		this.auditUserName = auditUserName;
	}

	public String getAuditIpAddress() {
		return auditIpAddress;
	}

	public void setAuditIpAddress(String auditIpAddress) {
		this.auditIpAddress = auditIpAddress;
	}

	public Long getAuditIdPrivilege() {
		return auditIdPrivilege;
	}

	public void setAuditIdPrivilege(Long auditIdPrivilege) {
		this.auditIdPrivilege = auditIdPrivilege;
	}
}
