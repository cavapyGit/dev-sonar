package com.pradera.accounts.holder.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.event.Event;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import com.pradera.accounts.holder.facade.HolderServiceFacade;
import com.pradera.accounts.holder.to.HolderAnnulationModelTO;
import com.pradera.accounts.util.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.HolderAnnulation;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.HolderAnnulationFileType;
import com.pradera.model.accounts.type.HolderAnnulationMotiveType;
import com.pradera.model.accounts.type.HolderAnnulationStateType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.process.BusinessProcess;

/** issue 605: Anulacion de CUI
 * 
 * @author rlarico */
@LoggerCreateBean
@DepositaryWebBean
public class HolderAnnulationMgmtBean extends GenericBaseBean implements Serializable {

	private static final long serialVersionUID = 1L;


	@Inject
	private UserInfo userInfo;

	@Inject
	private UserPrivilege userPrivilege;

	@Inject
	private transient PraderaLogger log;

	@EJB
	NotificationServiceFacade notificationServiceFacade;

	@EJB
	private AccountsFacade accountsFacade;

	@EJB
	private GeneralParametersFacade generalParametersFacade;

	@Inject
	protected Event<ExceptionToCatchEvent> excepcion;

	@EJB
	private HolderServiceFacade ejbHolderAnnulation;

	private HolderAnnulationModelTO modelTOReg;

	private HolderAnnulationModelTO modelTOSearch;

	private HolderAnnulationModelTO modelTOView;

	private boolean privilegeRegister;
	private boolean privilegeSearch;

	private boolean operationApprove;
	private boolean operationAnulate;
	private boolean operationReview;
	private boolean operationConfirm;
	private boolean operationReject;

	private HolderAnulationDataModel searchHolderAnnulationDataModel = new HolderAnulationDataModel();
	private List<HolderAnnulationModelTO> lstHolderAnnulationTO;

	private String fUploadFileTypesToCUIAnnul = "jpg|jpeg|pdf|doc|docx|png";
	private String fUploadsDisplayToCUIAnnul = "*.jpg, *.jpeg, *.png, *.doc, *.docx, *.pdf";
	private String docFileNameDisplay;

	@PostConstruct
	public void init() {
		loadSearch();
	}

	/** redirige a la pagina de registro
	 * 
	 * @return */
	public String goPageRegister() {
		loadRegiter();
		return PropertiesConstants.PAGE_REGISTER;
	}

	/** redireige a la pnatalla de administracion
	 * 
	 * @return */
	public String goPageSearch() {
		loadSearch();
		return PropertiesConstants.PAGE_SEARCH;
	}

	/** cargando la pantalla de registro */
	public void loadRegiter() {
		modelTOReg = new HolderAnnulationModelTO();
		modelTOReg.setLstParticipant(getLstParticipant());
		modelTOReg.setLstMoviveAnnulation(getLstMotiveHolderAnnulation());
		modelTOReg.setLstFileTypeAnnulation(getLstFileTyeHolderAnnulation());

		if(Validations.validateIsNotNull(userInfo.getUserAccountSession().getPartIssuerCode())){
			modelTOReg.setParticipantLogin(userInfo.getUserAccountSession().getPartIssuerCode());
		}else if(Validations.validateIsNotNull(userInfo.getUserAccountSession().getParticipantCode())){
			modelTOReg.setParticipantLogin(userInfo.getUserAccountSession().getParticipantCode());
		}else{
			modelTOReg.setParticipantLogin(null);
		}
		
		modelTOReg.setUserIsParticipant(false);
		modelTOReg.setIdParticipantSelected(null);
		docFileNameDisplay = "";
		
		// verificando el perfil de usuario para la atenuacion de participante
		if (!userInfo.getUserAccountSession().getInstitutionType().equals(InstitutionType.DEPOSITARY.getCode())
				&& Validations.validateIsNotNull(modelTOReg.getParticipantLogin())) {
			Participant participant = accountsFacade.getParticipantByPk(modelTOReg.getParticipantLogin());
			modelTOReg.setUserIsParticipant(true);
			modelTOReg.setIdParticipantSelected(modelTOReg.getParticipantLogin());

			if (participant.getState().equals(ParticipantStateType.BLOCKED.getCode())) {
				privilegeRegister = false;
			}
			RequestContext.getCurrentInstance().update("cboparticipant");
		}
	}

	/** lismpiando la pantalla de registro */
	public void btnClearRegister() {
		loadRegiter();
		JSFUtilities.resetViewRoot();
	}

	/** limpinado el helper de CUI cada q se selecciona el participante o cada que salta una validacion */
	public void cleanHelperCUIRegister() {
		log.info(" valor del participante seleccionado: " + modelTOReg.getIdParticipantSelected());
		modelTOReg.setHolderSelected(new Holder());
	}

	/** limpinado el helper de CUI cada q se selecciona el participante */
	public void cleanHelperCUISearch() {
		log.info(" valor del participante seleccionado: " + modelTOSearch.getIdParticipantSelected());
		modelTOSearch.setHolderSelected(new Holder());
	}

	/**
	 * se carga la informacion de holder, pero previo a ello se realizan validaciones
	 */
	public void loadHolderInformation() {
		
		// validamos si el estado del titular es REGISTRADO, caso contrario lanzamos un mensaje
		if (Validations.validateIsNotNull(modelTOReg.getHolderSelected())) {
			// si el estado es distinto de registrado indicamos que el CUI ingresao no esta en estado registrado
			if(Validations.validateIsNotNull(modelTOReg.getHolderSelected().getStateHolder())
					&& !modelTOReg.getHolderSelected().getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MSG_ERROR_ANNULATECUI_REQUIRED_CUISTATEREGISTERED, null);
				//JSFUtilities.showComponent("validationHolderAnnulation");
				JSFUtilities.executeJavascriptFunction("PF('cnfValidationHolderAnnulation').show();");
				cleanHelperCUIRegister();
				return;
			}
			
			// validando si el CUI ingresado existe en otra solicitud de Bloqueo/Desbloqueo/Anulacion de CUI pendiente por confirmar
			// tambien se valida que NO tenga saldos en cartera
			if (Validations.validateIsNotNull(modelTOReg.getHolderSelected().getIdHolderPk())) {
				
				// validacion para saber si esta en una solicitud de bloqueo pendiente por modificar
				if(ejbHolderAnnulation.thisHolderExistInRequestBlock(modelTOReg.getHolderSelected().getIdHolderPk())){
					showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MSG_ERROR_ANNULATECUI_CUI_IS_REQUESTBLOCK, null);
					//JSFUtilities.showComponent("validationHolderAnnulation");
					JSFUtilities.executeJavascriptFunction("PF('cnfValidationHolderAnnulation').show();");
					cleanHelperCUIRegister();
					return;
				}
				
				// validacion para saber si esta en una solicitud de desbloqueo pendiente por modificar
				if(ejbHolderAnnulation.thisHolderExistInRequestUnBlock(modelTOReg.getHolderSelected().getIdHolderPk())){
					showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MSG_ERROR_ANNULATECUI_CUI_IS_REQUESTUNBLOCK, null);
					JSFUtilities.executeJavascriptFunction("PF('cnfValidationHolderAnnulation').show();");
					cleanHelperCUIRegister();
					return;
				}
				
				// validacion para saber si esta en una solicitud de anulacion pendiente por modificar
				if(ejbHolderAnnulation.thisHolderExistInRequestAnnulation(modelTOReg.getHolderSelected().getIdHolderPk())){
					showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MSG_ERROR_ANNULATECUI_CUI_IS_REQUESTANNULATION, null);
					JSFUtilities.executeJavascriptFunction("PF('cnfValidationHolderAnnulation').show();");
					cleanHelperCUIRegister();
					return;
				}
				
				//validando que el CUI ingresado NO tenga saldos en cartera
				if(ejbHolderAnnulation.thisHolderHaveBalance(modelTOReg.getHolderSelected().getIdHolderPk())){
					showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MSG_ERROR_ANNULATECUI_CUI_HAVE_BALANCE, null);
					JSFUtilities.executeJavascriptFunction("PF('cnfValidationHolderAnnulation').show();");
					cleanHelperCUIRegister();
					return;
				}
			}
		}

		// recuperando el tipo de persona
		if (Validations.validateIsNotNull(modelTOReg.getHolderSelected()) 
				&& Validations.validateIsNotNull(modelTOReg.getHolderSelected().getHolderType())) {
			if (modelTOReg.getHolderSelected().getHolderType().equals(PersonType.JURIDIC.getCode())) {
				modelTOReg.setPersonTypeText(PersonType.JURIDIC.getValue());
			} else {
				modelTOReg.setPersonTypeText(PersonType.NATURAL.getValue());
			}
		}
		
		JSFUtilities.executeJavascriptFunction("PF('cnfValidationHolderAnnulation').hide();");
		
		JSFUtilities.executeJavascriptFunction("PF('dlgwMsgRequiredValidation').hide();");
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		//JSFUtilities.hideComponent("validationHolderAnnulation");
		//JSFUtilities.updateComponent("opnlDialogs");

	}

	public void loadFileTypeAccordingMotiveSelected() {
		if (Validations.validateIsNotNull(modelTOReg.getMotiveAnnulationSelected())) {

			Integer fileTypeAvailable = null;
			if (modelTOReg.getMotiveAnnulationSelected().equals(HolderAnnulationMotiveType.REGULATORY_ENTE_REQUEST.getCode())) {
				fileTypeAvailable = HolderAnnulationFileType.RESOLUTION_REGULATORY_ENTE.getCode();
			} else if (modelTOReg.getMotiveAnnulationSelected().equals(HolderAnnulationMotiveType.EDV_REQUEST.getCode())) {
				fileTypeAvailable = HolderAnnulationFileType.RESOLUTION_EDV.getCode();
			} else if (modelTOReg.getMotiveAnnulationSelected().equals(HolderAnnulationMotiveType.PARTICIPANT_REQUEST.getCode())) {
				fileTypeAvailable = HolderAnnulationFileType.ORDER_EXPRESS_PARTICIPANT.getCode();
			} else if (modelTOReg.getMotiveAnnulationSelected().equals(HolderAnnulationMotiveType.DEATH_HOLDER.getCode())) {
				fileTypeAvailable = HolderAnnulationFileType.DEATH_CERTIFICATE.getCode();
			} else if (modelTOReg.getMotiveAnnulationSelected().equals(HolderAnnulationMotiveType.JUDICAL_ORDER.getCode())) {
				fileTypeAvailable = HolderAnnulationFileType.COMPETENT_ENTITY_ORDER.getCode();
			} else if (modelTOReg.getMotiveAnnulationSelected().equals(HolderAnnulationMotiveType.OTHER_MOTIVES.getCode())) {
				fileTypeAvailable = HolderAnnulationFileType.OTHER_MOTIVES.getCode();
			} else if (modelTOReg.getMotiveAnnulationSelected().equals(HolderAnnulationMotiveType.HOLDER_REQUEST.getCode())) {
				fileTypeAvailable = HolderAnnulationFileType.ORDER_EXPRESS_HOLDER.getCode();
			}

			modelTOReg.setLstFileTypeAnnulation(getLstFileTyeHolderAnnulation());
			List<ParameterTable> lstFileTypeAvailable = new ArrayList<>();
			if (Validations.validateIsNotNull(fileTypeAvailable)) {
				for (ParameterTable fileType : modelTOReg.getLstFileTypeAnnulation()) {
					if (fileType.getParameterTablePk().equals(fileTypeAvailable)) {
						lstFileTypeAvailable.add(fileType);
						break;
					}
				}
			}
			modelTOReg.setLstFileTypeAnnulation(lstFileTypeAvailable);
		}
	}

	private String getDisplayName(UploadedFile fileUploaded) {
		String fDisplayName = null;
		if (fileUploaded.getFileName().length() > 30) {
			StringBuilder sbFileName = new StringBuilder();
			sbFileName.append("...");
			sbFileName.append(fileUploaded.getFileName().substring(fileUploaded.getFileName().length() - 27));
			fDisplayName = sbFileName.toString();
		} else {
			fDisplayName = fileUploaded.getFileName();
		}
		return fDisplayName;
	}

	public void documentAttachChannelOpFile(FileUploadEvent event) {
		String fDisplayName = getDisplayName(event.getFile());
		if (fDisplayName != null) {
			docFileNameDisplay = fDisplayName;
			modelTOReg.setFileName(fUploadValidateFileNameLength(event.getFile().getFileName()));
			modelTOReg.setFileAttached(event.getFile().getContents());
			
			JSFUtilities.executeJavascriptFunction("PF('cnfValidationHolderAnnulation').hide();");
			//JSFUtilities.hideComponent("validationHolderAnnulation");
			//JSFUtilities.updateComponent("opnlDialogs");
		}
	}

	/** antes de guardar la solicitud de anulacion CUI */
	public void beforeSaveRegister() {
		// validando si se subio algun archivo
		if(Validations.validateIsNull(modelTOReg.getFileAttached())){
			// lanzando mensaje de que tiene q estar en estado registrado
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MSG_ERROR_ANNULATECUI_REQUIRED_FILEUPLOAD, null);
			//JSFUtilities.showComponent("validationHolderAnnulation");
			JSFUtilities.executeJavascriptFunction("PF('cnfValidationHolderAnnulation').show();");
			return;
		}
		
		// enviando el cui al mensaje de confirmacion de registro
		Object[] arrBodyData = { modelTOReg.getHolderSelected().getIdHolderPk() };
		showMessageOnDialog(PropertiesConstants.MESSAGES_CONFIRM, null, PropertiesConstants.LBL_MSG_CONFIRM_REGITER_ANNULATECUI, arrBodyData);

		JSFUtilities.executeJavascriptFunction("PF('cnfHolderAnnulationRegiter').show();");
	}

	/** guardando en BBDD la solicutd de Anulacion de CUI */
	public void saveRegisterHolderAnnulation() {

		try {
			//hideDialogsListener(null);	
			// opteniendo las pistas de auditoria manualmente por si el processLogger esta en null en el facade
			modelTOReg.setAuditUserName(userInfo.getUserAccountSession().getUserName());
			modelTOReg.setAuditIpAddress(userInfo.getUserAccountSession().getIpAddress());
			modelTOReg.setAuditIdPrivilege(userPrivilege.getUserAcctions().getIdPrivilegeRegister().longValue());

			HolderAnnulation holderAnnulation = ejbHolderAnnulation.regiterHolderAnnulation(modelTOReg);

			// notificando al usuario correspondiente
			String descriptionParticipat="";
			for(Participant p:modelTOReg.getLstParticipant()){
				if(p.getIdParticipantPk().equals(modelTOReg.getIdParticipantSelected())){
					descriptionParticipat = p.getDescription();
					break;
				}
			}
			Object[] parameters = { holderAnnulation.getIdAnnulationHolderPk().toString(), descriptionParticipat};
			BusinessProcess businessProcessNotification = new BusinessProcess();
			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_ANNULATION_CUI_REGISTER.getCode());
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
			businessProcessNotification,
			modelTOReg.getIdParticipantSelected(), parameters);
						
			btnSearch();

			// mensaje de exito
			Object[] arrBodyData = { String.valueOf(holderAnnulation.getIdAnnulationHolderPk()) };
			showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, PropertiesConstants.MSG_SUCCESFUL_REGISTER_ANNULATECUI, arrBodyData);
			JSFUtilities.executeJavascriptFunction("PF('cnfEndTransactionHolderAnnulation').show();");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** metodo para cargar la pantalla de busqueda(administracion) */
	public void loadSearch() {

		modelTOSearch = new HolderAnnulationModelTO();
		if (userPrivilege.getUserAcctions().isRegister()) {
			log.info("::: tiene privilegios de registro");
			privilegeRegister = true;
		}
		if (userPrivilege.getUserAcctions().isSearch()) {
			log.info("::: tiene privilegios de busqueda");
			privilegeSearch = true;
		}

		modelTOSearch.setLstParticipant(getLstParticipant());
		modelTOSearch.setLstState(getLstStateHolderAnnulation());
		modelTOSearch.setLstMoviveAnnulation(getLstMotiveHolderAnnulation());
		modelTOSearch.setLstFileTypeAnnulation(getLstFileTyeHolderAnnulation());

		modelTOSearch.setInitialDate(new Date());
		modelTOSearch.setFinalDate(new Date());
		
		// verificando el perfil de usuario para la atenuacion de participante
		if(Validations.validateIsNotNull(userInfo.getUserAccountSession().getPartIssuerCode())){
			modelTOSearch.setParticipantLogin(userInfo.getUserAccountSession().getPartIssuerCode());
		}else if(Validations.validateIsNotNull(userInfo.getUserAccountSession().getParticipantCode())){
			modelTOSearch.setParticipantLogin(userInfo.getUserAccountSession().getParticipantCode());
		}else{
			modelTOSearch.setParticipantLogin(null);
		}
		
		modelTOSearch.setUserIsParticipant(false);
		if (!userInfo.getUserAccountSession().getInstitutionType().equals(InstitutionType.DEPOSITARY.getCode())
				&& Validations.validateIsNotNull(modelTOSearch.getParticipantLogin())) {
			Participant participant = accountsFacade.getParticipantByPk(modelTOSearch.getParticipantLogin());
			modelTOSearch.setUserIsParticipant(true);
			modelTOSearch.setIdParticipantSelected(modelTOSearch.getParticipantLogin());

			if (participant.getState().equals(ParticipantStateType.BLOCKED.getCode())) {
				privilegeRegister = false;
			}
			RequestContext.getCurrentInstance().update("cboparticipant");
		}
		lstHolderAnnulationTO = new ArrayList<>();
		searchHolderAnnulationDataModel = new HolderAnulationDataModel();
	}

	/** realizando la busqueda segun los criterios seleccionados */
	public void btnSearch() {
		try {
			lstHolderAnnulationTO = ejbHolderAnnulation.getLstHolderAnnulationModelTO(modelTOSearch);
			searchHolderAnnulationDataModel = new HolderAnulationDataModel(lstHolderAnnulationTO);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/** lleva al detalle (pantalla de vista) un registro mediante su link
	 * 
	 * @param channelOp
	 * @return */
	public String linkView(HolderAnnulationModelTO holderAnnulate) {
		modelTOView = (HolderAnnulationModelTO) holderAnnulate.clone();
		modelTOView.setLstParticipant(getLstParticipant());
		modelTOView.setLstMoviveAnnulation(getLstMotiveHolderAnnulation());
		modelTOView.setLstState(getLstStateHolderAnnulation());
		modelTOView.setLstFileTypeAnnulation(getLstFileTyeHolderAnnulation());

		// oteniendo el participante creador y el archivo adjunto
		try {
			HolderAnnulation han = ejbHolderAnnulation.getOneHolderAnnulationForView(modelTOView.getIdAnnulationHolderPk());
			modelTOView.setParticipantCreator(han.getIdHolderFk().getParticipant().getDisplayDescriptionCode());
			modelTOView.setFileAttached(han.getFileAttached());
			modelTOView.setFileName(han.getFileName());
		} catch (ServiceException e) {
			e.printStackTrace();
			log.info("::: se ha producido un error en el metodo HolderServiceFacade.getOneHolderAnnulationForView " + e.getMessage());
			modelTOView.setParticipantCreator("---");
			modelTOView.setFileAttached(null);
			modelTOView.setFileName(null);
		}

		renderFalseAllOperation();
		return PropertiesConstants.PAGE_VIEW;
	}

	/** regresando a la pantalla de search desde la pantalla view
	 * 
	 * @return */
	public String backToSearchFromView() {
		btnSearch();
		return PropertiesConstants.PAGE_SEARCH;
	}

	/** limpiando la pantalla de search */
	public void btnClearSearch() {
		loadSearch();
		JSFUtilities.resetViewRoot();
	}

	public void hideDialogsListener(ActionEvent actionEvent) {
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		JSFUtilities.hideGeneralDialogues();

		JSFUtilities.executeJavascriptFunction("PF('cnfHolderAnnulationRegiterer').hide();");
		JSFUtilities.executeJavascriptFunction("PF('cnfValidationHolderAnnulation').hide();");
		//JSFUtilities.hideComponent("validationHolderAnnulation");
		// JSFUtilities.hideComponent("detailRepresentativeDialog");

	}

	/** cargando datos para la pantalla de aprobacion
	 * 
	 * @return */
	public String loadApproved() {

		if (Validations.validateIsNotNull(modelTOView) && Validations.validateIsNotNull(modelTOView.getIdAnnulationHolderPk())) {
			// validando que el estado sea registrado
			if (modelTOView.getStateAnnulation().equals(HolderAnnulationStateType.REGISTERED.getCode())) {
				modelTOView.setLstParticipant(getLstParticipant());
				modelTOView.setLstMoviveAnnulation(getLstMotiveHolderAnnulation());
				modelTOView.setLstState(getLstStateHolderAnnulation());
				modelTOView.setLstFileTypeAnnulation(getLstFileTyeHolderAnnulation());

				// oteniendo el participante creador y el archivo adjunto
				try {
					HolderAnnulation han = ejbHolderAnnulation.getOneHolderAnnulationForView(modelTOView.getIdAnnulationHolderPk());
					modelTOView.setParticipantCreator(han.getIdHolderFk().getParticipant().getDisplayDescriptionCode());
					modelTOView.setFileAttached(han.getFileAttached());
					modelTOView.setFileName(han.getFileName());
				} catch (ServiceException e) {
					e.printStackTrace();
					log.info("::: se ha producido un error en el metodo HolderServiceFacade.getOneHolderAnnulationForView " + e.getMessage());
					modelTOView.setParticipantCreator("---");
					modelTOView.setFileAttached(null);
					modelTOView.setFileName(null);
				}

				renderBtnApprove();
				return PropertiesConstants.PAGE_VIEW;
			} else {
				// lanzando mensaje de que tiene q estar en estado registrado
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_ANNULATECUI_REQUIRED_STATEREGISTERED, null);
				JSFUtilities.updateComponent("opnlDialogs");
				JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
				return null;
			}
		} else {
			// lanzar mensaje de requerimiento de seleccion
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_ANNULATECUI_REQUIRED_SELECTED, null);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
			return null;
		}
	}

	/** antes de aprobar */
	public void beforeApprove() {
		// obteniendo la fecha de apertura, hora inicio y fin en formato string
		String numberRequest = modelTOView.getIdAnnulationHolderPk().toString();

		Object[] arrBodyData = { numberRequest };
		showMessageOnDialog(PropertiesConstants.MESSAGES_CONFIRM, null, PropertiesConstants.LBL_MSG_CONFIRM_APPROVED_ANNULATECUI, arrBodyData);		
		JSFUtilities.executeJavascriptFunction("PF('cnfHolderAnnulationRegiterer').show();");
	}

	/** cambiando a estado APROBADO la solictud de anulacion CUI
	 * 
	 * @throws Exception */
	public void saveApprovedHolderAnnulation() throws Exception {

		try {
//			hideDialogsListener(null);
			// opteniendo las pistas de auditoria manualmente por si el processLogger esta en null en el facade
			modelTOView.setAuditUserName(userInfo.getUserAccountSession().getUserName());
			modelTOView.setAuditIpAddress(userInfo.getUserAccountSession().getIpAddress());
			modelTOView.setAuditIdPrivilege(userPrivilege.getUserAcctions().getIdPrivilegeApprove().longValue());

			// validando nuevamente que es estado tiene que estar en REGISTRADO para evitar concurrecia de actualizacions de
			// datos
			HolderAnnulation holderAnnulationEvaluated = ejbHolderAnnulation.find(HolderAnnulation.class, modelTOView.getIdAnnulationHolderPk());
			if (holderAnnulationEvaluated.getStateAnnulation().equals(HolderAnnulationStateType.REGISTERED.getCode())) {
				modelTOView.setStateAnnulation(HolderAnnulationStateType.APPROVED.getCode());
				HolderAnnulation holderAnnulation = ejbHolderAnnulation.changeStateHolderAnnulation(modelTOView);
				// mensaje de exito
				Object[] arrBodyData = { String.valueOf(holderAnnulation.getIdAnnulationHolderPk()) };
				showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, PropertiesConstants.MSG_SUCCESFUL_APPROVED_ANNULATECUI, arrBodyData);
				
				// notificando al usuario correspondiente
				String descriptionParticipat="";
				for(Participant p:modelTOView.getLstParticipant()){
					if(p.getIdParticipantPk().equals(modelTOView.getIdParticipantSelected())){
						descriptionParticipat = p.getDescription();
						break;
					}
				}
				Object[] parameters = { modelTOView.getIdAnnulationHolderPk().toString(), descriptionParticipat};
				BusinessProcess businessProcessNotification = new BusinessProcess();
				businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_ANNULATION_CUI_APPROVAL.getCode());
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
				businessProcessNotification,
				modelTOView.getIdParticipantSelected(), parameters);
			} else {
				// mensaje de concurrencia de actualizacion de datos
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_OPERATION_NO_PROCESS_FOR_CONCURRENCE, null);
			}
			btnSearch();

			JSFUtilities.executeJavascriptFunction("PF('cnfEndTransactionHolderAnnulation').show();");
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			// throw new Exception();
		}
	}

	/** cargando datos para la pantalla de view(anulacion)
	 * 
	 * @return */
	public String loadAnulate() {

		if (Validations.validateIsNotNull(modelTOView) && Validations.validateIsNotNull(modelTOView.getIdAnnulationHolderPk())) {
			// validando que el estado sea registrado
			if (modelTOView.getStateAnnulation().equals(HolderAnnulationStateType.REGISTERED.getCode())) {
				modelTOView.setLstParticipant(getLstParticipant());
				modelTOView.setLstMoviveAnnulation(getLstMotiveHolderAnnulation());
				modelTOView.setLstState(getLstStateHolderAnnulation());
				modelTOView.setLstFileTypeAnnulation(getLstFileTyeHolderAnnulation());
				modelTOView.setLstMotiveCancelRequest(getLstMotiveCancelRequyest());

				// oteniendo el participante creador y el archivo adjunto
				try {
					HolderAnnulation han = ejbHolderAnnulation.getOneHolderAnnulationForView(modelTOView.getIdAnnulationHolderPk());
					modelTOView.setParticipantCreator(han.getIdHolderFk().getParticipant().getDisplayDescriptionCode());
					modelTOView.setFileAttached(han.getFileAttached());
					modelTOView.setFileName(han.getFileName());
				} catch (ServiceException e) {
					e.printStackTrace();
					log.info("::: se ha producido un error en el metodo HolderServiceFacade.getOneHolderAnnulationForView " + e.getMessage());
					modelTOView.setParticipantCreator("---");
					modelTOView.setFileAttached(null);
					modelTOView.setFileName(null);
				}

				renderBtnAnulate();
				return PropertiesConstants.PAGE_VIEW;
			} else {
				// lanzando mensaje de que tiene q estar en estado registrado
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_ANNULATECUI_REQUIRED_STATEREGISTERED, null);
				JSFUtilities.updateComponent("opnlDialogs");
				JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
				return null;
			}
		} else {
			// lanzar mensaje de requerimiento de seleccion
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_ANNULATECUI_REQUIRED_SELECTED, null);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
			return null;
		}
	}
	
	public void preBeforeAnulateOrReject(){
		JSFUtilities.updateComponent("opnlDialogs");
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MOTIVE_ANNUL),null);
		JSFUtilities.executeJavascriptFunction("PF('alterRejectWidget').show();");
	}

	/** antes de anular se lanza el mensaje de confirmacion */
	public void beforeAnulate() {
		String numberRequest = modelTOView.getIdAnnulationHolderPk().toString();

		Object[] arrBodyData = { numberRequest };
		JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_CONFIRM, null, PropertiesConstants.LBL_MSG_CONFIRM_ANNULATE_ANNULATECUI, arrBodyData);
		JSFUtilities.executeJavascriptFunction("PF('cnfHolderAnnulationRegiterer').show();");
	}

	/** cambiando a estado ANULADO la solictud de anulacion CUI
	 * 
	 * @throws Exception */
	public void saveAnulateHolderAnnulation() throws Exception {

		try {
			//hideDialogsListener(null);
			// opteniendo las pistas de auditoria manualmente por si el processLogger esta en null en el facade
			modelTOView.setAuditUserName(userInfo.getUserAccountSession().getUserName());
			modelTOView.setAuditIpAddress(userInfo.getUserAccountSession().getIpAddress());
			modelTOView.setAuditIdPrivilege(userPrivilege.getUserAcctions().getIdPrivilegeAnnular().longValue());

			// validando nuevamente que es estado tiene que estar en REGISTRADO para evitar concurrecia de actualizacions de
			// datos
			HolderAnnulation holderAnnulationEvaluated = ejbHolderAnnulation.find(HolderAnnulation.class, modelTOView.getIdAnnulationHolderPk());
			if (holderAnnulationEvaluated.getStateAnnulation().equals(HolderAnnulationStateType.REGISTERED.getCode())) {
				modelTOView.setStateAnnulation(HolderAnnulationStateType.ANNULATED.getCode());
				HolderAnnulation holderAnnulation = ejbHolderAnnulation.changeStateHolderAnnulation(modelTOView);
				// mensaje de exito
				Object[] arrBodyData = { String.valueOf(holderAnnulation.getIdAnnulationHolderPk()) };
				showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, PropertiesConstants.MSG_SUCCESFUL_ANULATE_ANNULATECUI, arrBodyData);
			} else {
				// mensaje de concurrencia de actualizacion de datos
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_OPERATION_NO_PROCESS_FOR_CONCURRENCE, null);
			}

			btnSearch();
			JSFUtilities.executeJavascriptFunction("PF('cnfEndTransactionHolderAnnulation').show();");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			// throw new Exception();
		}
	}

	/** cargando datos para la pantalla de aprobacion
	 * 
	 * @return */
	public String loadReview() {

		if (Validations.validateIsNotNull(modelTOView) && Validations.validateIsNotNull(modelTOView.getIdAnnulationHolderPk())) {
			// validando que el estado sea registrado
			if (modelTOView.getStateAnnulation().equals(HolderAnnulationStateType.APPROVED.getCode())) {
				modelTOView.setLstParticipant(getLstParticipant());
				modelTOView.setLstMoviveAnnulation(getLstMotiveHolderAnnulation());
				modelTOView.setLstState(getLstStateHolderAnnulation());
				modelTOView.setLstFileTypeAnnulation(getLstFileTyeHolderAnnulation());

				// oteniendo el participante creador y el archivo adjunto
				try {
					HolderAnnulation han = ejbHolderAnnulation.getOneHolderAnnulationForView(modelTOView.getIdAnnulationHolderPk());
					modelTOView.setParticipantCreator(han.getIdHolderFk().getParticipant().getDisplayDescriptionCode());
					modelTOView.setFileAttached(han.getFileAttached());
					modelTOView.setFileName(han.getFileName());
				} catch (ServiceException e) {
					e.printStackTrace();
					log.info("::: se ha producido un error en el metodo HolderServiceFacade.getOneHolderAnnulationForView " + e.getMessage());
					modelTOView.setParticipantCreator("---");
					modelTOView.setFileAttached(null);
					modelTOView.setFileName(null);
				}

				renderBtnReview();
				return PropertiesConstants.PAGE_VIEW;
			} else {
				// lanzando mensaje de que tiene q estar en estado aprobado
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_ANNULATECUI_REQUIRED_STATEAPPROVED, null);
				JSFUtilities.updateComponent("opnlDialogs");
				JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
				return null;
			}
		} else {
			// lanzar mensaje de requerimiento de seleccion
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_ANNULATECUI_REQUIRED_SELECTED, null);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
			return null;
		}
	}
	
	/** antes de revisar se lanza el mensaje de confirmacion */
	public void beforeReview() {
		String numberRequest = modelTOView.getIdAnnulationHolderPk().toString();

		Object[] arrBodyData = { numberRequest };
		showMessageOnDialog(PropertiesConstants.MESSAGES_CONFIRM, null, PropertiesConstants.LBL_MSG_CONFIRM_REVIEW_ANNULATECUI, arrBodyData);
		JSFUtilities.executeJavascriptFunction("PF('cnfHolderAnnulationRegiterer').show();");
	}
	
	/** cambiando a estado ANULADO la solictud de anulacion CUI
	 * 
	 * @throws Exception */
	public void saveReviewHolderAnnulation() throws Exception {

		try {
//			hideDialogsListener(null);
			// opteniendo las pistas de auditoria manualmente por si el processLogger esta en null en el facade
			modelTOView.setAuditUserName(userInfo.getUserAccountSession().getUserName());
			modelTOView.setAuditIpAddress(userInfo.getUserAccountSession().getIpAddress());
			modelTOView.setAuditIdPrivilege(userPrivilege.getUserAcctions().getIdPrivilegeReview().longValue());

			// validando nuevamente que es estado tiene que estar en APROBADO para evitar concurrecia de actualizacions de
			// datos
			HolderAnnulation holderAnnulationEvaluated = ejbHolderAnnulation.find(HolderAnnulation.class, modelTOView.getIdAnnulationHolderPk());
			if (holderAnnulationEvaluated.getStateAnnulation().equals(HolderAnnulationStateType.APPROVED.getCode())) {
				modelTOView.setStateAnnulation(HolderAnnulationStateType.REVIEWED.getCode());
				HolderAnnulation holderAnnulation = ejbHolderAnnulation.changeStateHolderAnnulation(modelTOView);
				// mensaje de exito
				Object[] arrBodyData = { String.valueOf(holderAnnulation.getIdAnnulationHolderPk()) };
				showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, PropertiesConstants.MSG_SUCCESFUL_REVIEWED_ANNULATECUI, arrBodyData);
				
				// notificando al usuario correspondiente
				String descriptionParticipat="";
				for(Participant p:modelTOView.getLstParticipant()){
					if(p.getIdParticipantPk().equals(modelTOView.getIdParticipantSelected())){
						descriptionParticipat = p.getDescription();
						break;
					}
				}
				Object[] parameters = { modelTOView.getIdAnnulationHolderPk().toString(), descriptionParticipat};
				BusinessProcess businessProcessNotification = new BusinessProcess();
				businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_ANNULATION_CUI_REVIEWED.getCode());
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
				businessProcessNotification,
				modelTOView.getIdParticipantSelected(), parameters);
				
			} else {
				// mensaje de concurrencia de actualizacion de datos
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_OPERATION_NO_PROCESS_FOR_CONCURRENCE, null);
			}

			btnSearch();
			JSFUtilities.executeJavascriptFunction("PF('cnfEndTransactionHolderAnnulation').show();");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			// throw new Exception();
		}
	}
	
	/** cargando datos para la pantalla de confirmacion
	 * 
	 * @return */
	public String loadConfirm() {

		if (Validations.validateIsNotNull(modelTOView) && Validations.validateIsNotNull(modelTOView.getIdAnnulationHolderPk())) {
			
			//volviendo a validar que NO tenga saldos en cartera
			//validando que el CUI seleccionado NO tenga saldos en cartera
			if(ejbHolderAnnulation.thisHolderHaveBalance(modelTOView.getIdHolder())){
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_ANNULATECUI_CUI_HAVE_BALANCE, null);
				JSFUtilities.updateComponent("opnlDialogs");
				JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
				return null;
			}
			
			// validando que el estado de la solictud sea aprobado o revisado
			if (modelTOView.getStateAnnulation().equals(HolderAnnulationStateType.REVIEWED.getCode())) {
				modelTOView.setLstParticipant(getLstParticipant());
				modelTOView.setLstMoviveAnnulation(getLstMotiveHolderAnnulation());
				modelTOView.setLstState(getLstStateHolderAnnulation());
				modelTOView.setLstFileTypeAnnulation(getLstFileTyeHolderAnnulation());

				// oteniendo el participante creador y el archivo adjunto
				try {
					HolderAnnulation han = ejbHolderAnnulation.getOneHolderAnnulationForView(modelTOView.getIdAnnulationHolderPk());
					
					// volviendo a validar que el estado del CUI sea REGISTRADO
					// si el estado es distinto de registrado indicamos que el CUI ingresao no esta en estado registrado
					if( Validations.validateIsNotNull(han.getIdHolderFk()) 
							&&Validations.validateIsNotNull(han.getIdHolderFk().getStateHolder())
							&& !han.getIdHolderFk().getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
						showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_ANNULATECUI_REQUIRED_CUISTATEREGISTERED, null);
						JSFUtilities.updateComponent("opnlDialogs");
						JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
						return null;
					}
					
					modelTOView.setParticipantCreator(han.getIdHolderFk().getParticipant().getDisplayDescriptionCode());
					modelTOView.setFileAttached(han.getFileAttached());
					modelTOView.setFileName(han.getFileName());
				} catch (ServiceException e) {
					e.printStackTrace();
					log.info("::: se ha producido un error en el metodo HolderServiceFacade.getOneHolderAnnulationForView " + e.getMessage());
					modelTOView.setParticipantCreator("---");
					modelTOView.setFileAttached(null);
					modelTOView.setFileName(null);
				}

				renderBtnConfirm();
				return PropertiesConstants.PAGE_VIEW;
			} else {
				// lanzando mensaje de que tiene q estar en estado revisado
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_ANNULATECUI_REQUIRED_STATEREVIEWED, null);
				JSFUtilities.updateComponent("opnlDialogs");
				JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
				return null;
			}
		} else {
			// lanzar mensaje de requerimiento de seleccion
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_ANNULATECUI_REQUIRED_SELECTED, null);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
			return null;
		}
	}
	
	/** antes de revisar se lanza el mensaje de confirmacion */
	public void beforeConfirm() {
		String numberRequest = modelTOView.getIdAnnulationHolderPk().toString();

		Object[] arrBodyData = { numberRequest };
		showMessageOnDialog(PropertiesConstants.MESSAGES_CONFIRM, null, PropertiesConstants.LBL_MSG_CONFIRM_CONFIRMED_ANNULATECUI, arrBodyData);
		JSFUtilities.executeJavascriptFunction("PF('cnfHolderAnnulationRegiterer').show();");
	}
	
	/** cambiando a estado CONFIRMADO la solictud de anulacion CUI
	 * 
	 * @throws Exception */
	public void saveConfirmHolderAnnulation() throws Exception {

		try {
//			hideDialogsListener(null);
			// opteniendo las pistas de auditoria manualmente por si el processLogger esta en null en el facade
			modelTOView.setAuditUserName(userInfo.getUserAccountSession().getUserName());
			modelTOView.setAuditIpAddress(userInfo.getUserAccountSession().getIpAddress());
			modelTOView.setAuditIdPrivilege(userPrivilege.getUserAcctions().getIdPrivilegeConfirm().longValue());

			// validando nuevamente que es estado tiene que estar en APROBADO para evitar concurrecia de actualizacions de
			// datos
			HolderAnnulation holderAnnulationEvaluated = ejbHolderAnnulation.find(HolderAnnulation.class, modelTOView.getIdAnnulationHolderPk());
			if (holderAnnulationEvaluated.getStateAnnulation().equals(HolderAnnulationStateType.REVIEWED.getCode())) {
				modelTOView.setStateAnnulation(HolderAnnulationStateType.CONFIRMED.getCode());
				HolderAnnulation holderAnnulation = ejbHolderAnnulation.changeStateHolderAnnulation(modelTOView);
				String participantRelationship = ejbHolderAnnulation.getParticipantsRelationshipOfHolder(modelTOView.getIdHolder());
				// mensaje de exito
				Object[] arrBodyData = { String.valueOf(holderAnnulation.getIdAnnulationHolderPk()), modelTOView.getIdHolder().toString(), participantRelationship };
				showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, PropertiesConstants.MSG_SUCCESFUL_CONFIRMED_ANNULATECUI, arrBodyData);
				
				// notificando al usuario correspondiente
				Object[] parameters = { modelTOView.getIdAnnulationHolderPk().toString(), modelTOView.getIdHolder() };
				BusinessProcess businessProcessNotification = new BusinessProcess();
				businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_ANNULATION_CUI_CONFIRM.getCode());
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
				businessProcessNotification,
				modelTOView.getIdParticipantSelected(), parameters);
				
			} else {
				// mensaje de concurrencia de actualizacion de datos
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_OPERATION_NO_PROCESS_FOR_CONCURRENCE, null);
			}

			btnSearch();
			JSFUtilities.executeJavascriptFunction("PF('cnfEndTransactionHolderAnnulation').show();");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			// throw new Exception();
		}
	}
	
	/** cargando datos para la pantalla de aprobacion
	 * 
	 * @return */
	public String loadReject() {

		if (Validations.validateIsNotNull(modelTOView) && Validations.validateIsNotNull(modelTOView.getIdAnnulationHolderPk())) {
			// validando que el estado sea aprobado o revisado
			if (modelTOView.getStateAnnulation().equals(HolderAnnulationStateType.REVIEWED.getCode())
					|| modelTOView.getStateAnnulation().equals(HolderAnnulationStateType.APPROVED.getCode())) {
				modelTOView.setLstParticipant(getLstParticipant());
				modelTOView.setLstMoviveAnnulation(getLstMotiveHolderAnnulation());
				modelTOView.setLstState(getLstStateHolderAnnulation());
				modelTOView.setLstFileTypeAnnulation(getLstFileTyeHolderAnnulation());
				modelTOView.setLstMotiveCancelRequest(getLstMotiveCancelRequyest());
				
				// oteniendo el participante creador y el archivo adjunto
				try {
					HolderAnnulation han = ejbHolderAnnulation.getOneHolderAnnulationForView(modelTOView.getIdAnnulationHolderPk());
					modelTOView.setParticipantCreator(han.getIdHolderFk().getParticipant().getDisplayDescriptionCode());
					modelTOView.setFileAttached(han.getFileAttached());
					modelTOView.setFileName(han.getFileName());
				} catch (ServiceException e) {
					e.printStackTrace();
					log.info("::: se ha producido un error en el metodo HolderServiceFacade.getOneHolderAnnulationForView " + e.getMessage());
					modelTOView.setParticipantCreator("---");
					modelTOView.setFileAttached(null);
					modelTOView.setFileName(null);
				}

				renderBtnReject();
				return PropertiesConstants.PAGE_VIEW;
			} else {
				// lanzando mensaje de que tiene q estar en estado aprobado o revisado
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_ANNULATECUI_REQUIRED_STATEREVIEWEDORAPPROVED, null);
				JSFUtilities.updateComponent("opnlDialogs");
				JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
				return null;
			}
		} else {
			// lanzar mensaje de requerimiento de seleccion
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_ANNULATECUI_REQUIRED_SELECTED, null);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
			return null;
		}
	}
	
	/** antes de revisar se lanza el mensaje de confirmacion */
	public void beforeReject() {
		String numberRequest = modelTOView.getIdAnnulationHolderPk().toString();

		Object[] arrBodyData = { numberRequest };
		showMessageOnDialog(PropertiesConstants.MESSAGES_CONFIRM, null, PropertiesConstants.LBL_MSG_CONFIRM_REJECT_ANNULATECUI, arrBodyData);
		JSFUtilities.executeJavascriptFunction("PF('cnfHolderAnnulationRegiterer').show();");
	}
	
	/** cambiando a estado RECHAZADO la solictud de anulacion CUI
	 * 
	 * @throws Exception */
	public void saveRejectHolderAnnulation() throws Exception {

		try {
//			hideDialogsListener(null);
			// opteniendo las pistas de auditoria manualmente por si el processLogger esta en null en el facade
			modelTOView.setAuditUserName(userInfo.getUserAccountSession().getUserName());
			modelTOView.setAuditIpAddress(userInfo.getUserAccountSession().getIpAddress());
			modelTOView.setAuditIdPrivilege(userPrivilege.getUserAcctions().getIdPrivilegeReject().longValue());

			// validando nuevamente que es estado tiene que estar en APROBADO o REVISADO para evitar concurrecia de actualizacions de
			// datos
			HolderAnnulation holderAnnulationEvaluated = ejbHolderAnnulation.find(HolderAnnulation.class, modelTOView.getIdAnnulationHolderPk());
			if (holderAnnulationEvaluated.getStateAnnulation().equals(HolderAnnulationStateType.APPROVED.getCode())
					|| holderAnnulationEvaluated.getStateAnnulation().equals(HolderAnnulationStateType.REVIEWED.getCode())) {
				modelTOView.setStateAnnulation(HolderAnnulationStateType.REJECTED.getCode());
				HolderAnnulation holderAnnulation = ejbHolderAnnulation.changeStateHolderAnnulation(modelTOView);
				// mensaje de exito
				Object[] arrBodyData = { String.valueOf(holderAnnulation.getIdAnnulationHolderPk()) };
				showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, PropertiesConstants.MSG_SUCCESFUL_REJECTED_ANNULATECUI, arrBodyData);
				
				// notificando al usuario correspondiente
				Object[] parameters = { modelTOView.getIdAnnulationHolderPk().toString()};
				BusinessProcess businessProcessNotification = new BusinessProcess();
				businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_ANNULATION_CUI_REJECT.getCode());
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
				businessProcessNotification,
				modelTOView.getIdParticipantSelected(), parameters);
				
			} else {
				// mensaje de concurrencia de actualizacion de datos
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_OPERATION_NO_PROCESS_FOR_CONCURRENCE, null);
			}

			btnSearch();
			JSFUtilities.executeJavascriptFunction("PF('cnfEndTransactionHolderAnnulation').show();");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			// throw new Exception();
		}
	}

	public void saveOperationChangeState() throws Exception {
		if (operationApprove) {
			saveApprovedHolderAnnulation();
			return;
		}
		if (operationAnulate) {
			saveAnulateHolderAnnulation();
			return;
		}
		if (operationReview) {
			saveReviewHolderAnnulation();
			return;
		}
		if (operationConfirm) {
			saveConfirmHolderAnnulation();
			return;
		}
		if (operationReject) {
			saveRejectHolderAnnulation();
			return;
		}
	}

	// TODO metodos genericos para cargar las listas(combobox) necesarios para cualquier pantalla (registro, vista, search)
	public List<Participant> getLstParticipant() {
		try {
			Participant filter = new Participant();
			filter.setState(ParticipantStateType.REGISTERED.getCode());
			return accountsFacade.getLisParticipantServiceBean(filter);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
			return new ArrayList<>();
		}
	}

	public List<ParameterTable> getLstStateHolderAnnulation() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.STATES_HOLDER_ANNULATION.getCode());
			return generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
			return new ArrayList<>();
		}
	}

	public List<ParameterTable> getLstMotiveHolderAnnulation() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.MOTIVES_HOLDER_ANNULATION.getCode());
			return generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
			return new ArrayList<>();
		}
	}

	public List<ParameterTable> getLstFileTyeHolderAnnulation() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.FILE_TYPE_HOLDER_ANNULATION.getCode());
			return generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
			return new ArrayList<>();
		}
	}
	
	public List<ParameterTable> getLstMotiveCancelRequyest() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.CANCEL_REQUEST_HOLDER_ANNULATION.getCode());
			return generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
			return new ArrayList<>();
		}
	}

	// TODO metodos para el renderizados de los botones de las operaciones (aprobado, anulado, revisado, confirmado, rechzado)
	public void renderBtnApprove() {
		renderFalseAllOperation();
		operationApprove = true;
	}

	public void renderBtnAnulate() {
		renderFalseAllOperation();
		operationAnulate = true;
	}

	public void renderBtnConfirm() {
		renderFalseAllOperation();
		operationConfirm = true;
	}

	public void renderBtnReject() {
		renderFalseAllOperation();
		operationReject = true;
	}

	public void renderBtnReview() {
		renderFalseAllOperation();
		operationReview = true;
	}

	private void renderFalseAllOperation() {
		operationApprove = false;
		operationAnulate = false;
		operationConfirm = false;
		operationReject = false;
		operationReview = false;
	}

	public boolean isPrivilegeRegister() {
		return privilegeRegister;
	}

	public void setPrivilegeRegister(boolean privilegeRegister) {
		this.privilegeRegister = privilegeRegister;
	}

	public boolean isPrivilegeSearch() {
		return privilegeSearch;
	}

	public void setPrivilegeSearch(boolean privilegeSearch) {
		this.privilegeSearch = privilegeSearch;
	}

	public HolderAnulationDataModel getSearchHolderAnnulationDataModel() {
		return searchHolderAnnulationDataModel;
	}

	public void setSearchHolderAnnulationDataModel(HolderAnulationDataModel searchHolderAnnulationDataModel) {
		this.searchHolderAnnulationDataModel = searchHolderAnnulationDataModel;
	}

	public List<HolderAnnulationModelTO> getLstHolderAnnulationTO() {
		return lstHolderAnnulationTO;
	}

	public void setLstHolderAnnulationTO(List<HolderAnnulationModelTO> lstHolderAnnulationTO) {
		this.lstHolderAnnulationTO = lstHolderAnnulationTO;
	}

	public String getfUploadFileTypesToCUIAnnul() {
		return fUploadFileTypesToCUIAnnul;
	}

	public void setfUploadFileTypesToCUIAnnul(String fUploadFileTypesToCUIAnnul) {
		this.fUploadFileTypesToCUIAnnul = fUploadFileTypesToCUIAnnul;
	}

	public String getfUploadsDisplayToCUIAnnul() {
		return fUploadsDisplayToCUIAnnul;
	}

	public void setfUploadsDisplayToCUIAnnul(String fUploadsDisplayToCUIAnnul) {
		this.fUploadsDisplayToCUIAnnul = fUploadsDisplayToCUIAnnul;
	}

	public String getDocFileNameDisplay() {
		return docFileNameDisplay;
	}

	public void setDocFileNameDisplay(String docFileNameDisplay) {
		this.docFileNameDisplay = docFileNameDisplay;
	}

	public HolderAnnulationModelTO getModelTOReg() {
		return modelTOReg;
	}

	public void setModelTOReg(HolderAnnulationModelTO modelTOReg) {
		this.modelTOReg = modelTOReg;
	}

	public HolderAnnulationModelTO getModelTOSearch() {
		return modelTOSearch;
	}

	public void setModelTOSearch(HolderAnnulationModelTO modelTOSearch) {
		this.modelTOSearch = modelTOSearch;
	}

	public HolderAnnulationModelTO getModelTOView() {
		return modelTOView;
	}

	public void setModelTOView(HolderAnnulationModelTO modelTOView) {
		this.modelTOView = modelTOView;
	}

	public boolean isOperationApprove() {
		return operationApprove;
	}

	public void setOperationApprove(boolean operationApprove) {
		this.operationApprove = operationApprove;
	}

	public boolean isOperationAnulate() {
		return operationAnulate;
	}

	public void setOperationAnulate(boolean operationAnulate) {
		this.operationAnulate = operationAnulate;
	}

	public boolean isOperationReview() {
		return operationReview;
	}

	public void setOperationReview(boolean operationReview) {
		this.operationReview = operationReview;
	}

	public boolean isOperationConfirm() {
		return operationConfirm;
	}

	public void setOperationConfirm(boolean operationConfirm) {
		this.operationConfirm = operationConfirm;
	}

	public boolean isOperationReject() {
		return operationReject;
	}

	public void setOperationReject(boolean operationReject) {
		this.operationReject = operationReject;
	}
}
