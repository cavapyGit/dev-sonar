package com.pradera.accounts.holder.massive.facade;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.beanio.BeanWriter;
import org.jboss.ejb3.annotation.TransactionTimeout;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import com.pradera.accounts.holder.facade.HolderServiceFacade;
import com.pradera.accounts.holder.massive.to.HolderMassiveRow;
import com.pradera.accounts.holder.massive.type.HolderMassiveLabelType;
import com.pradera.accounts.holderaccount.facade.HolderAccountServiceFacade;
import com.pradera.accounts.util.view.PropertiesConstants;
import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.massive.type.OperationCorrectHeaderLabel;
import com.pradera.commons.massive.type.OperationIncorrectHeaderLabel;
import com.pradera.commons.report.ReportFile;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.utils.BeanIOUtils;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.common.validation.to.ValidationOperationType;
import com.pradera.integration.common.validation.to.ValidationProcessTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.HolderMassiveFile;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountType;
import com.pradera.model.accounts.type.BlockedFieldsHolderAccountType;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.PepMotiveType;
import com.pradera.model.accounts.type.PepPersonClassificationType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionStateType;
import com.pradera.model.generalparameter.type.GeographicLocationType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.negotiation.constant.NegotiationConstant;
import com.pradera.model.report.type.ReportFormatType;

@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class HolderMassiveServiceFacade extends CrudDaoServiceBean {
	
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	@EJB
	HolderMassiveAdditionalService holderMassiveAdditionalService;
	
	@EJB
	HolderServiceFacade holderServiceFacade;
	
	@EJB
	HolderAccountServiceFacade holderAccountServiceFacade;
	
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;

	/** The country residence. */
	@Inject @Configurable Integer countryResidence;
	/** The department issuance. */
	@Inject @Configurable Integer departmentIssuance;	
	
	private static String regexOnlyNumber = "^([0-9])*$";
	
	private static String regexNumericInteger = "^([\\d])*$";
	
	private static String regexRegularAlphaNotSpace = "([\\d]|[-])*$";
	
	private static String regexLocalDocumentNumber = "^\\d+(-\\d)?$";
	
	private static String lastNameRegex = "^[a-zA-ZñÑáéíóúÁÉÍÓÚüÜ ]+$";
	
	private static String nameRegex = "^[a-zA-ZñÑáéíóúÁÉÍÓÚüÜ ]+$";
	
	private static String passportRegex = "^[a-zA-ZñÑáéíóúÁÉÍÓÚüÜ1-9-]+$";
	
	private static String emailRegex = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$";
	
	private List<ParameterTable> lstIndPddType = new ArrayList<ParameterTable>();
	
	public Boolean holderMassiveConvertXLStoXML(ProcessFileTO processFileTO, List<ParameterTable> lstHolderType){

		Boolean isOk = Boolean.TRUE;
		
 		try {
 			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
 	        DocumentBuilder builder = factory.newDocumentBuilder();
 	        DOMImplementation implementation = builder.getDOMImplementation();
 	 		File fileTemp = null;
 	 		
 			FileInputStream fileInputStream = new FileInputStream(processFileTO.getTempProcessFile().getPath());
 			XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
 			XSSFSheet datatypeSheet = workbook.getSheetAt(0);
 			Iterator<Row> iterator = datatypeSheet.iterator();

			Document documento = implementation.createDocument(null, "ROWSET", null);
	        documento.setXmlVersion("1.0");
			

	        if(processFileTO.getValidationProcessTO() == null){
	        	processFileTO.setValidationProcessTO(new ValidationProcessTO());
	        }	
	        
	        Boolean cellValid = true;
	        
	        String message = GeneralConstants.EMPTY_STRING;
	        
	        Text initLine = documento.createTextNode("\n");
			
			documento.getDocumentElement().appendChild(initLine);
	        
	        while (iterator.hasNext()) {
 				Row row = iterator.next();
 				Element operation = documento.createElement("ROW");
 				
 				if (row.getRowNum() == 0) {
 					continue;
 				}
 				
 				String holderAccountTypeDescription = CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("C"), row);
 				
 				if(Validations.validateIsNull(holderAccountTypeDescription)) {
 					continue;
 				} else {
 	 				CommonsUtilities.createTagXML(operation, documento, String.valueOf(row.getRowNum() + 1), 
 							"NUMERO_FILA");
		
 	 				CommonsUtilities.createTagXML(operation, documento, holderAccountTypeDescription, 
 	 						"TIPO_DE_CUENTA_TITULAR");
 				}
 				
				String holderTypeDescription = CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("M"), row);
				
				CommonsUtilities.createTagXML(operation, documento, 
						Validations.validateIsNotNull(holderTypeDescription)? holderTypeDescription : GeneralConstants.EMPTY_STRING, 
						"TIPO_DE_TITULAR");
				
				if(Validations.validateIsNotNull(holderTypeDescription)) {
					Optional<ParameterTable> pHolderType = validateParamType(holderTypeDescription, lstHolderType, 
							ParameterTableStateType.REGISTERED.getCode(), MasterTableType.PERSON_TYPE.getCode());
					
					if(pHolderType.isPresent()) {
						boolean isJuridic = Boolean.FALSE;
						processFileTO.setStreamFileDir(NegotiationConstant.HOLDER_MASSIVE_FILE_STREAM_NATURAL);
						if(pHolderType.get().getParameterTablePk().equals(PersonType.JURIDIC.getCode())) {
							isJuridic = Boolean.TRUE;
						} 
						convertGeneralFile(operation, documento, row, isJuridic);
					}
				}

				convertHolderAccountInfo(operation, documento, row);
				
				convertRepresentativeInfo(operation, documento, row);
				
				documento.getDocumentElement().appendChild(operation); 
				
				Text newLine = documento.createTextNode("\n");
				   
				documento.getDocumentElement().appendChild(newLine);
				
				// Asocio el source con el Document
				Source source = new DOMSource(documento);
				// Creo el Result, indicado que fichero se va a crear
	 			fileTemp = File.createTempFile("tempHolderMassiveFile", ".tmp");			
				Result result = new StreamResult(fileTemp);
				Transformer transformer = TransformerFactory.newInstance().newTransformer();
		        transformer.transform(source, result);
		        
				processFileTO.setOriginalTempProcessFile(processFileTO.getTempProcessFile());
				processFileTO.setTempProcessFile(fileTemp);
	        }
 		} catch(ParserConfigurationException e) {
			processFileTO.getValidationProcessTO().setLstValidations(new ArrayList<RecordValidationType>());
			RecordValidationType fe = new RecordValidationType();
			fe.setErrorSimpleDetail(e.getMessage());
			processFileTO.getValidationProcessTO().getLstValidations().add(fe);
			return Boolean.FALSE;
 		} catch (FileNotFoundException e) {
 			processFileTO.getValidationProcessTO().setLstValidations(new ArrayList<RecordValidationType>());
			RecordValidationType fe = new RecordValidationType();
			fe.setErrorSimpleDetail(e.getMessage());
			processFileTO.getValidationProcessTO().getLstValidations().add(fe);
			return Boolean.FALSE;
		} catch (IOException e) {
			processFileTO.getValidationProcessTO().setLstValidations(new ArrayList<RecordValidationType>());
			RecordValidationType fe = new RecordValidationType();
			fe.setErrorSimpleDetail(e.getMessage());
			processFileTO.getValidationProcessTO().getLstValidations().add(fe);
			return Boolean.FALSE;
		} catch (TransformerConfigurationException e) {
			processFileTO.getValidationProcessTO().setLstValidations(new ArrayList<RecordValidationType>());
			RecordValidationType fe = new RecordValidationType();
			fe.setErrorSimpleDetail(e.getMessage());
			processFileTO.getValidationProcessTO().getLstValidations().add(fe);
			return Boolean.FALSE;
		} catch (TransformerFactoryConfigurationError e) {
			processFileTO.getValidationProcessTO().setLstValidations(new ArrayList<RecordValidationType>());
			RecordValidationType fe = new RecordValidationType();
			fe.setErrorSimpleDetail(e.getMessage());
			processFileTO.getValidationProcessTO().getLstValidations().add(fe);
			return Boolean.FALSE;
		} catch (TransformerException e) {
			processFileTO.getValidationProcessTO().setLstValidations(new ArrayList<RecordValidationType>());
			RecordValidationType fe = new RecordValidationType();
			fe.setErrorSimpleDetail(e.getMessage());
			processFileTO.getValidationProcessTO().getLstValidations().add(fe);
			return Boolean.FALSE;
		}
 		return isOk;
	}
		
	private void convertGeneralFile(Element operation, Document documento, Row row, boolean isJuridic) {
		
		String juridicClassDescription =		CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("N"), row);
		String firstLastName =					CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("O"), row);	
		String secondLastName =					CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("P"), row);
		String names =							CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("Q"), row);	
		String fullName =						CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("R"), row);
		String email =							CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("S"), row);		
		String economicSectorDescription =		CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("T"), row);
		String economicActivityDescription =	CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("U"), row);
		String nacionalityCountryDescription =	CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("V"), row);
		String indResidentDescription =			CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("W"), row);	
		String documentType =					CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("X"), row);
		String documentNumber =					CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("Y"), row);
		String sexDescription =					CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("Z"), row);
		String birthDateString =				CommonsUtilities.getCellForceStringValue(CellReference.convertColStringToIndex("AA"), row);
		String legalCountryDescription =		CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("AB"), row);
		String legalDepartmentDescryption =		CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("AC"), row);	
		String legalCityDescription =			CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("AD"), row);	
		String legalAddress =					CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("AE"), row);
		String mobileNumber = 					CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("AF"), row);
		String indFatcaDescription =			CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("AG"), row); // AF -> AG
		String indPepDescription =				CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("AH"), row); // AG -> AH
		String pepMotiveDescription =			CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("AI"), row); // AH -> AI
		String pepRoleDescription =				CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("AJ"), row); // AI -> AJ
		String pepGradeDescription = 			CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("AK"), row);
		String pepName = 						CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("AL"), row);
		String pepBeginningPeriodString =		CommonsUtilities.getCellForceStringValue(CellReference.convertColStringToIndex("AM"), row);
		String pepEndingPeriodString =			CommonsUtilities.getCellForceStringValue(CellReference.convertColStringToIndex("AN"), row);

		
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(nacionalityCountryDescription)? nacionalityCountryDescription : GeneralConstants.EMPTY_STRING, 
				"NACIONALIDAD");
				
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(indResidentDescription)? indResidentDescription : GeneralConstants.EMPTY_STRING, 
				"ES_RESIDENTE");
				
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(legalCountryDescription)? legalCountryDescription : GeneralConstants.EMPTY_STRING, 
				"PAIS");
		
		if(Validations.validateIsNotNull(birthDateString)) {
			Date tempBithDate = CommonsUtilities.validateDate(birthDateString, "dd-MMM-yyyy");
			if(Validations.validateIsNotNull(tempBithDate)) {
				birthDateString = CommonsUtilities.convertDateToString(tempBithDate, CommonsUtilities.DATE_PATTERN);
			}
		}
			
		CommonsUtilities.createTagXML(operation, documento,
				Validations.validateIsNotNull(birthDateString) ? birthDateString : GeneralConstants.EMPTY_STRING,
				"FECHA_DE_NACIMIENTO");		
		
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(email)? email : GeneralConstants.EMPTY_STRING, 
				"CORREO");		
				
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(documentType)? documentType : GeneralConstants.EMPTY_STRING, 
				"TIPO_DE_DOCUMENTO");
			
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(documentNumber)? documentNumber : GeneralConstants.EMPTY_STRING, 
				"NUMERO_DE_DOCUMENTO");				
			
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(legalAddress)? legalAddress : GeneralConstants.EMPTY_STRING, 
				"DIRECCION_LEGAL");
			
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(legalDepartmentDescryption)? legalDepartmentDescryption : GeneralConstants.EMPTY_STRING, 
				"DEPARTAMENTO");		
			
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(legalCityDescription)? legalCityDescription : GeneralConstants.EMPTY_STRING, 
				"CIUDAD");
		
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(mobileNumber)? mobileNumber : GeneralConstants.EMPTY_STRING, 
				"NUMERO_CELULAR");
				
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(indFatcaDescription)? indFatcaDescription : GeneralConstants.EMPTY_STRING, 
				"FATCA");
		
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(indPepDescription)? indPepDescription : GeneralConstants.EMPTY_STRING, 
				"PEP");
		
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(pepMotiveDescription)? pepMotiveDescription : GeneralConstants.EMPTY_STRING, 
				"MOTIVO_PEP");
		
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(pepRoleDescription)? pepRoleDescription : GeneralConstants.EMPTY_STRING, 
				"CARGO_PEP");
		
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(pepGradeDescription)? pepGradeDescription : GeneralConstants.EMPTY_STRING, 
				"GRADO_PEP");
		
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(pepName)? pepName : GeneralConstants.EMPTY_STRING, 
				"NOMBRE_PEP");
		
		if(Validations.validateIsNotNull(pepBeginningPeriodString)) {
			Date tempBeginningPeriodDate = CommonsUtilities.validateDate(pepBeginningPeriodString, "dd-MMM-yyyy");
			if(Validations.validateIsNotNull(tempBeginningPeriodDate)) {
				pepBeginningPeriodString = CommonsUtilities.convertDateToString(tempBeginningPeriodDate, CommonsUtilities.DATE_PATTERN);
			}
		}
		
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(pepBeginningPeriodString)? pepBeginningPeriodString : GeneralConstants.EMPTY_STRING, 
				"PERIODO_DESDE_PEP");
		
		if(Validations.validateIsNotNull(pepEndingPeriodString)) {
			Date tempEndingPeriodDate = CommonsUtilities.validateDate(pepEndingPeriodString, "dd-MMM-yyyy");
			if(Validations.validateIsNotNull(tempEndingPeriodDate)) {
				pepEndingPeriodString = CommonsUtilities.convertDateToString(tempEndingPeriodDate, CommonsUtilities.DATE_PATTERN);
			}
		}
		
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(pepEndingPeriodString)? pepEndingPeriodString : GeneralConstants.EMPTY_STRING, 
				"PERIODO_HASTA_PEP");
				
		if(isJuridic) {
			CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(juridicClassDescription)? juridicClassDescription : GeneralConstants.EMPTY_STRING, 
				"CLASE_JURIDICO");
		} else {
			CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(documentNumber)? documentNumber : GeneralConstants.EMPTY_STRING, 
				"CLASE_JURIDICO");
		}	
			
		if(!isJuridic) {
			CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(firstLastName)? firstLastName : GeneralConstants.EMPTY_STRING, 
				"PRIMER_APELLIDO");
		} else {
			CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(documentNumber)? documentNumber : GeneralConstants.EMPTY_STRING, 
				"PRIMER_APELLIDO");
		}		
				
		if(!isJuridic) {
			CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(secondLastName)? secondLastName : GeneralConstants.EMPTY_STRING, 
				"SEGUNDO_APELLIDO");
		} else {
			CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(documentNumber)? documentNumber : GeneralConstants.EMPTY_STRING, 
				"SEGUNDO_APELLIDO");
		}	
			
		if(!isJuridic) {
			CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(names)? names : GeneralConstants.EMPTY_STRING, 
				"NOMBRES");
		} else {
			CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(documentNumber)? documentNumber : GeneralConstants.EMPTY_STRING, 
				"NOMBRES");
		}				
		
		if(!isJuridic) {
			CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(sexDescription)? sexDescription : GeneralConstants.EMPTY_STRING, 
				"SEXO");
		} else {
			CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(documentNumber)? documentNumber : GeneralConstants.EMPTY_STRING, 
				"SEXO");
		}				
		
		if(isJuridic) {
			CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(fullName)? fullName : GeneralConstants.EMPTY_STRING, 
				"RAZON_SOCIAL_NOMBRE_COMPLETO");
		} else {
			CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(documentNumber)? documentNumber : GeneralConstants.EMPTY_STRING, 
				"RAZON_SOCIAL_NOMBRE_COMPLETO");
		}		
		
		if(isJuridic) {
			CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(economicSectorDescription)? economicSectorDescription : GeneralConstants.EMPTY_STRING, 
				"SECTOR_ECONOMICO");
		} else {
			CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(documentNumber)? documentNumber : GeneralConstants.EMPTY_STRING, 
				"SECTOR_ECONOMICO");
		}
				
		if(isJuridic) {
			CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(economicActivityDescription)? economicActivityDescription : GeneralConstants.EMPTY_STRING, 
				"ACTIVIDAD_ECONOMICA");
		} else {
			CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(documentNumber)? documentNumber : GeneralConstants.EMPTY_STRING, 
				"ACTIVIDAD_ECONOMICA");
		}

	}	
	
	private void convertHolderAccountInfo(Element operation, Document documento, Row row) {

		String holderAccountNumber = CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("A"), row);
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(holderAccountNumber)? holderAccountNumber : GeneralConstants.EMPTY_STRING,  
				"CUENTA_INVERSIONISTA_EN_DEPOSITANTE");
			
		String stockCodeDescription = CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("B"), row);		
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(stockCodeDescription)? stockCodeDescription : GeneralConstants.EMPTY_STRING, 
				"CODIGO_BURSATIL");
		
		String relatedName = CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("E"), row);		
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(relatedName)? relatedName : GeneralConstants.EMPTY_STRING, 
				"NOMBRE_RELACIONADO");
		
		String localCurrencyBankDescription = CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("F"), row);		
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(localCurrencyBankDescription)? localCurrencyBankDescription : GeneralConstants.EMPTY_STRING, 
				"BANCO_GUARANIES");
		
		String localCurrencyBankTypeDescription = CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("G"), row);		
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(localCurrencyBankTypeDescription)? localCurrencyBankTypeDescription : GeneralConstants.EMPTY_STRING, 
				"TIPO_DE_CUENTA_BANCARIA_GUARANIES");
		
		String localCurrencyBankAccountNumberDescription = CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("H"), row);		
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(localCurrencyBankAccountNumberDescription)? localCurrencyBankAccountNumberDescription : GeneralConstants.EMPTY_STRING, 
				"NUMERO_CUENTA_BANCARIA_GUARANIES");
		
		String usdCurrencyBankDescription = CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("I"), row);		
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(usdCurrencyBankDescription)? usdCurrencyBankDescription : GeneralConstants.EMPTY_STRING, 
				"BANCO_DOLARES");
		
		String usdCurrencyBankTypeDescription = CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("J"), row);		
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(usdCurrencyBankTypeDescription)? usdCurrencyBankTypeDescription : GeneralConstants.EMPTY_STRING, 
				"TIPO_DE_CUENTA_BANCARIA_DOLARES");
		
		String usdCurrencyBankAccountNumberDescription = CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("K"), row);		
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(usdCurrencyBankAccountNumberDescription)? usdCurrencyBankAccountNumberDescription : GeneralConstants.EMPTY_STRING, 
				"NUMERO_CUENTA_BANCARIA_DOLARES");		

		String indPddDescription = CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("L"), row);		
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(indPddDescription)? indPddDescription : GeneralConstants.EMPTY_STRING, 
				"PDD");
	}
	
	private void convertRepresentativeInfo(Element operation, Document documento, Row row) {
		
		String names = 							CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("AP"), row); // AK -> AP	
		String firstLastName = 					CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("AQ"), row); // AL -> AQ
		String secondLastName = 				CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("AR"), row); // AM -> AR
		String fullName = 						CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("AS"), row); // AN -> AS
		String email = 							CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("AT"), row); // AO -> AT
		String nacionalityCountryDescription = 	CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("AU"), row); // AP -> AU
		String indResidentDescription = 		CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("AV"), row); // AQ -> AV
		String legalCountryDescription = 		CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("AW"), row); // AR -> AW	
		String legalDepartmentDescryption = 	CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("AX"), row); // AS -> AX
		String legalCityDescription = 			CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("AY"), row); // AT -> AY	
		String legalAddress = 					CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("AZ"), row); // AU -> AZ
		String holderTypeDescription = 			CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("BA"), row); // AV -> BA
		String documentType = 					CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("BB"), row); // AW -> BB
		String documentNumber = 				CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("BC"), row); // AX -> BC
		String sexDescription = 				CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("BD"), row); // AY -> BD
		String birthDateString = 				CommonsUtilities.getCellForceStringValue(CellReference.convertColStringToIndex("BE"), row); // AZ -> BE
		String representativeClassDescription = CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("BF"), row); // BA -> BF
		
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(holderTypeDescription)? holderTypeDescription : GeneralConstants.EMPTY_STRING, 
				"TIPO_DE_REPRESENTANTE");
				
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(nacionalityCountryDescription)? nacionalityCountryDescription : GeneralConstants.EMPTY_STRING, 
				"NACIONALIDAD_REPRESENTANTE");		
				
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(indResidentDescription)? indResidentDescription : GeneralConstants.EMPTY_STRING, 
				"REPRESENTATE_ES_RESIDENTE");		
		
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(legalCountryDescription)? legalCountryDescription : GeneralConstants.EMPTY_STRING, 
				"PAIS_REPRESENTANTE");
		
		if(Validations.validateIsNotNullAndNotEmpty(birthDateString)) {
			Date tempBithDate = CommonsUtilities.validateDate(birthDateString, "dd-MMM-yyyy");
			if(Validations.validateIsNotNull(tempBithDate)) {
				birthDateString = CommonsUtilities.convertDateToString(tempBithDate, CommonsUtilities.DATE_PATTERN);
			}
			CommonsUtilities.createTagXML(operation, documento,
					Validations.validateIsNotNull(birthDateString) ? birthDateString : GeneralConstants.EMPTY_STRING,
					"FECHA_DE_NACIMIENTO_REPRESENTANTE");
		}
		
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(names)? names : GeneralConstants.EMPTY_STRING, 
				"NOMBRE_DEL_REPRESENTANTE");
		
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(firstLastName)? firstLastName : GeneralConstants.EMPTY_STRING, 
				"PRIMER_APELLIDO_REPRESENTANTE");
				
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(secondLastName)? secondLastName : GeneralConstants.EMPTY_STRING, 
				"SEGUNDO_APELLIDO_REPRESENTANTE");
			
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(fullName)? fullName : GeneralConstants.EMPTY_STRING, 
				"RAZON_SOCIAL_NOMBRE_COMPLETO_REPRESENTATE");
	
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(email)? email : GeneralConstants.EMPTY_STRING, 
				"CORREO_REPRESENTANTE");
				
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(documentType)? documentType : GeneralConstants.EMPTY_STRING, 
				"TIPO_DE_DOCUMENTO_REPRESENTANTE");
				
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(documentNumber)? documentNumber : GeneralConstants.EMPTY_STRING, 
				"NUMERO_DE_DOCUMENTO_REPRESENTANTE");		
			
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(legalAddress)? legalAddress : GeneralConstants.EMPTY_STRING, 
				"DIRECCION_LEGAL_REPRESENTANTE");
				
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(legalDepartmentDescryption)? legalDepartmentDescryption : GeneralConstants.EMPTY_STRING, 
				"DEPARTAMENTO_REPRESENTANTE");
				
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(legalCityDescription)? legalCityDescription : GeneralConstants.EMPTY_STRING, 
				"CIUDAD_REPRESENTANTE");
				
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(sexDescription)? sexDescription : GeneralConstants.EMPTY_STRING, 
				"SEXO_DESCRIPCION");
				
		CommonsUtilities.createTagXML(operation, documento, 
				Validations.validateIsNotNull(representativeClassDescription)? representativeClassDescription : GeneralConstants.EMPTY_STRING, 
				"CLASE_REPRESENTANTE");
		
	}
	
	@TransactionTimeout(unit=TimeUnit.MINUTES, value=30)
	public Object[] saveHolderRequestMassively(ProcessFileTO holderProcessFileTO, List<ParameterTable> lstParameter, Map<String, GeographicLocation> nationality) {
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
        ReportUser reportUser = new ReportUser();
        reportUser.setUserName(loggerUser.getUserName());
        
        List<Bank> lstBanks = holderAccountServiceFacade.getBankListServiceFacade();
        
		List<ReportFile> files = new ArrayList<ReportFile>(0);
		Integer numberAcceptedOps=0;
		// #1 separate transaction: save file
		ValidationProcessTO validationTO = holderProcessFileTO.getValidationProcessTO();
		HolderMassiveFile holderMassiveFile = holderMassiveAdditionalService.saveHolderProcessFileTx(holderProcessFileTO,loggerUser);
		
		BigDecimal quantityAccepted = BigDecimal.ZERO;
		BigDecimal quantityRejected = BigDecimal.ZERO;
		String messageSecurity = null;
		AtomicInteger correlative = new AtomicInteger(Integer.MAX_VALUE);
		try {
			
			// Mapeamos y agrupamos los registros segun el numero de cuenta			
			Map<Integer, List<HolderMassiveRow>> mapHolderAccounts = validationTO.getLstOperations()
			        .stream()
			        .map(operation -> (HolderMassiveRow) operation)
			        .collect(Collectors.groupingBy(
			        		row -> {
			                    return Validations.validateIsNull(row.getHolderAccountNumber())?
			                    		correlative.getAndDecrement(): row.getHolderAccountNumber();
					        }));
			
			for(List<HolderMassiveRow> lstHolderRow : mapHolderAccounts.values()) {
				
				String firstHolderAccountTypeDescription = lstHolderRow.get(0).getHolderAccountTypeDescription().trim().toUpperCase();
				String firstHolderAccountStockCodeDescription = lstHolderRow.get(0).getStockCodeDescription();
				String firstHolderAccountRelatedName = lstHolderRow.get(0).getRelatedName();
				
				boolean sameHolderAccountTypeDescription = true;
				boolean sameHolderAccountStockCodeDescription = true;
				boolean sameHolderAccountRelatedName = Boolean.TRUE;
				
				for (int i = 1; i < lstHolderRow.size(); i++) {
				    String currentHolderAccountTypeDescription = lstHolderRow.get(i).getHolderAccountTypeDescription().trim().toUpperCase();
				    String currentHolderAccountStockCodeDescription = Validations.validateIsNotNull(lstHolderRow.get(i).getStockCodeDescription()) ? lstHolderRow.get(i).getStockCodeDescription().trim().toUpperCase() : GeneralConstants.EMPTY_STRING;
				    String currentHolderAccountRelatedName = Validations.validateIsNotNull(lstHolderRow.get(i).getRelatedName()) ? lstHolderRow.get(i).getRelatedName().trim().toUpperCase() : GeneralConstants.EMPTY_STRING;
				    
				    if (!currentHolderAccountTypeDescription.equals(firstHolderAccountTypeDescription)) {
				        sameHolderAccountTypeDescription = false;
				        break;
				    }
				    
				    if(Validations.validateIsNotNull(currentHolderAccountStockCodeDescription) && !currentHolderAccountStockCodeDescription.equals(firstHolderAccountStockCodeDescription)) {
				    	sameHolderAccountStockCodeDescription = false;
				    	break;
				    }
				    
				    if(!currentHolderAccountRelatedName.equals(firstHolderAccountRelatedName)) {
				    	sameHolderAccountRelatedName = false;
				    	break;
				    }
				}
				
				HolderMassiveRow finalRow = new HolderMassiveRow();
				boolean firstIteration = true;
				boolean noErrors = true;
				Integer errorLineNumber = null;
				for(Iterator<HolderMassiveRow> it = lstHolderRow.iterator(); it.hasNext(); ){
					HolderMassiveRow holderRow = it.next();
					List<RecordValidationType> errs = new ArrayList<RecordValidationType>(0);
					if(sameHolderAccountTypeDescription && sameHolderAccountStockCodeDescription && sameHolderAccountRelatedName) {
						 errs = validateAndBuildOperations(holderRow, lstParameter, holderMassiveFile, nationality, lstBanks, lstHolderRow.size());
					} else {
						if(!sameHolderAccountTypeDescription){
							errs.add(CommonsUtilities.populateRecordValidation(holderRow,
									HolderMassiveLabelType.TIPO_DE_CUENTA_INVERSIONISTA.toString(),
									PropertiesConstants.HOLDER_MASSIVE_MULTIPLE_HOLDER_TYPE_ERROR));
						} else if(!sameHolderAccountStockCodeDescription) {
							errs.add(CommonsUtilities.populateRecordValidation(holderRow,
									HolderMassiveLabelType.CODIGO_BURSATIL.toString(),
									PropertiesConstants.HOLDER_MASSIVE_MULTIPLE_STOCK_CODE_ERROR));
						} else if(!sameHolderAccountRelatedName) {
							errs.add(CommonsUtilities.populateRecordValidation(holderRow,
									HolderMassiveLabelType.NOMBRE_RELACIONADO.toString(),
									PropertiesConstants.HOLDER_MASSIVE_MULTIPLE_RELATED_NAME_ERROR));
						}
					}				
					
					if(errs.size() == 0){
						if(firstIteration) {
							finalRow = holderRow;
							firstIteration = false;
						} else {
							if(holderRow.getHolderAccountType().equals(HolderAccountType.JURIDIC.getCode())) {
								if(holderRow.getHolderType().equals(PersonType.JURIDIC.getCode())) {
									finalRow.getHolderAccountObjectTO().setJuridicHolderObjectTO(holderRow.getHolderAccountObjectTO().getJuridicHolderObjectTO());
								} else {
									finalRow.getHolderAccountObjectTO().getLstNaturalHolderObjectTOs()
									.addAll(holderRow.getHolderAccountObjectTO().getLstNaturalHolderObjectTOs());
								}
							} else {
								finalRow.getHolderAccountObjectTO().getLstNaturalHolderObjectTOs()
								.addAll(holderRow.getHolderAccountObjectTO().getLstNaturalHolderObjectTOs());
							}
							
						}
						
						if(!it.hasNext() && noErrors) {
							RecordValidationType e = holderMassiveAdditionalService.saveSingleHolderRequest(finalRow,holderMassiveFile);
							
							if(e != null){	
								if(Validations.validateIsNotNull(e.getLineNumber())){
									errorLineNumber = e.getLineNumber();
									Optional<HolderMassiveRow> errorRow = lstHolderRow.stream()
											.filter(row -> row.getRowNumber().equals(e.getLineNumber()))
											.findFirst();
									errorRow.get().setHasError(Boolean.TRUE);
								} else {
									e.setLineNumber(holderRow.getRowNumber());
									errorLineNumber = holderRow.getRowNumber();
									holderRow.setHasError(true);
								}
								noErrors = false;
								validationTO.getLstValidations().add(e);
							}else{
								numberAcceptedOps = numberAcceptedOps + lstHolderRow.size();
							}
						} 
					}else{
						errs.get(0).setLineNumber(holderRow.getRowNumber());
						validationTO.getLstValidations().addAll(errs);						
						holderRow.setHasError(true);
						noErrors = false;
						errorLineNumber = holderRow.getRowNumber();
					}
				}
				
				if(!noErrors && sameHolderAccountTypeDescription && sameHolderAccountStockCodeDescription) {
					for(HolderMassiveRow holderRow : lstHolderRow) {
						if(!holderRow.isHasError()) {
							holderRow.setHasError(Boolean.TRUE);		
							RecordValidationType otherRowError = CommonsUtilities.populateRecordValidation(holderRow,
									GeneralConstants.EMPTY_STRING,
									PropertiesConstants.HOLDER_MASSIVE_OTHER_ROW_ERROR);
							
							otherRowError.setErrorDescription(MessageFormat.format(otherRowError.getErrorDescription(), errorLineNumber));
							otherRowError.setLineNumber(holderRow.getRowNumber());
							
							validationTO.getLstValidations().add(otherRowError);
						}
					}
				}
			}
			validationTO.countOperations();     
			
			// save accepted operations file
			if(validationTO.getAcceptedOps() > 0){
				InputStream mappingFile = Thread.currentThread().getContextClassLoader().
						getResourceAsStream(NegotiationConstant.HOLDER_MASSIVE_FILE_STREAM_RESPONSE);
				StringWriter sw = new StringWriter(); 
				
				//BeanIOUtils.write(val.getAcceptedOperations(),mappingFile, "file", sw);
				BeanWriter out = BeanIOUtils.createWriter(mappingFile, "file", sw);
				
				out.write("header",new OperationCorrectHeaderLabel());
				for (ValidationOperationType object : validationTO.getAcceptedOperations()) {
					out.write("operation",object);
				}
			    out.flush();
			    out.close();
			    
			    holderMassiveFile.setAcceptedFile(sw.toString().getBytes("UTF-8"));
				sw.flush();
				sw.close();
				mappingFile.close();
				
				ReportFile file = new ReportFile();
				file.setFile(holderMassiveFile.getAcceptedFile());
				file.setPhysicalName("ACCEPTED_FILES"+CommonsUtilities.convertDateToString(new Date(),CommonsUtilities.DATETIME_PATTERN));
				file.setReportType(ReportFormatType.TXT.getCode());
				files.add(file);
			}
			
			// save rejected operations file
			if(validationTO.getRejectedOps() > 0 && Validations.validateListIsNotNullAndNotEmpty(validationTO.getLstValidations())){
				InputStream mappingFile = Thread.currentThread().getContextClassLoader().
						getResourceAsStream(NegotiationConstant.HOLDER_MASSIVE_FILE_STREAM_RESPONSE_ERROR);
				StringWriter sw = new StringWriter();
				
				//BeanIOUtils.write(val.getLstErrors(),inputFile, "file", sw);
				BeanWriter out = BeanIOUtils.createWriter(mappingFile, "file", sw);
				out.write("header",new OperationIncorrectHeaderLabel());
				for (RecordValidationType object : validationTO.getLstValidations()) {
					out.write("error",object);
				}
			    out.flush();
			    out.close();
			    
			    holderMassiveFile.setRejectedFile(sw.toString().getBytes("UTF-8"));
				sw.flush();
				sw.close();
				mappingFile.close();
				
				ReportFile file = new ReportFile();
				file.setFile(holderMassiveFile.getRejectedFile());
				file.setPhysicalName("REJECTED_FILES"+CommonsUtilities.convertDateToString(new Date(),CommonsUtilities.DATETIME_PATTERN));
				file.setReportType(ReportFormatType.TXT.getCode());
				files.add(file);
			}
			
			BigDecimal acc = new BigDecimal(validationTO.getAcceptedOps());
			BigDecimal rej = new BigDecimal(validationTO.getRejectedOps());
			holderMassiveFile.setAcceptedOperations(acc);
			holderMassiveFile.setRejectedOperations(rej);
			holderMassiveFile.setTotalOperations(acc.add(rej));
			holderMassiveFile.setProcessState(ExternalInterfaceReceptionStateType.CORRECT.getCode());
			
			quantityAccepted = holderMassiveFile.getAcceptedOperations();
			quantityRejected = holderMassiveFile.getRejectedOperations();
			
			//#3 separate transaction: update file independently
			holderMassiveAdditionalService.updateMcnProcessFileTx(holderMassiveFile, loggerUser);
			
			//mcnOperationAdditionalService.sendReportFile(reportId , reportUser , null, null, loggerUser, files );						
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			if(validationTO.getAcceptedOps()==0){//No llego a terminar el Bucle(For) con las operaciones
				BigDecimal acc = new BigDecimal(numberAcceptedOps);
				BigDecimal total = new BigDecimal(validationTO.getLstOperations().size());
				BigDecimal rej = CommonsUtilities.subtractTwoDecimal(total, acc, 2);
				holderMassiveFile.setAcceptedOperations(acc);
				holderMassiveFile.setRejectedOperations(rej);
				holderMassiveFile.setTotalOperations(total);
			} else if(Validations.validateIsNotNullAndPositive(validationTO.getAcceptedOps())
					&& Validations.validateIsNotNullAndPositive(validationTO.getRejectedOps())){
				BigDecimal acc = new BigDecimal(validationTO.getAcceptedOps());
				BigDecimal rej = new BigDecimal(validationTO.getRejectedOps());
				holderMassiveFile.setAcceptedOperations(acc);
				holderMassiveFile.setRejectedOperations(rej);
				holderMassiveFile.setTotalOperations(acc.add(rej));
			}			
			
			quantityAccepted = holderMassiveFile.getAcceptedOperations();
			quantityRejected = holderMassiveFile.getRejectedOperations();
			
			holderMassiveFile.setProcessState(ExternalInterfaceReceptionStateType.FAILED.getCode());
			
			e.printStackTrace();
			
		} finally {
			holderMassiveAdditionalService.updateMcnProcessFileTx(holderMassiveFile, loggerUser);
		}
		
		return new Object[]{quantityAccepted.add(quantityRejected), quantityAccepted, quantityRejected,messageSecurity};
		
	}
	
	private List<RecordValidationType> validateAndBuildOperations(HolderMassiveRow holderRow, List<ParameterTable> lstParameter, HolderMassiveFile holderMassiveFile, Map<String, 
			GeographicLocation> nationality, List<Bank> lstBanks, Integer holdersSize) throws ServiceException {		
		List<RecordValidationType> lstErrors = new ArrayList<RecordValidationType>(0);

		// General Validation
		generalValidation(lstErrors,holderRow, lstParameter);

		if(holderRow.getHolderType().equals(PersonType.NATURAL.getCode())) {
			generalNaturalValidation(lstErrors, holderRow, lstParameter, nationality);
		} else {
			generalJuridicValidation(lstErrors, holderRow, lstParameter, nationality);
		}
		
		holderAccountValidation(lstErrors, holderRow, lstParameter, lstBanks, holdersSize);
		
		generalRepresentativeValidation(lstErrors, holderRow, lstParameter, nationality);
		
		generalHolderValidations(lstErrors, holderRow, lstParameter, nationality);
		
		if(lstErrors.size() < 1 ) {
			if(holderRow.getHolderType().equals(PersonType.NATURAL.getCode())) {
				holderRow.buildNaturalHolderAccountObject();
			} else {
				holderRow.buildJuridicHolderAccountObject();
			}
		}
		
		return lstErrors;
	}
	
	private void generalValidation(List<RecordValidationType> lstErrors, HolderMassiveRow holderRow, List<ParameterTable> lstParameter) {
		//Revisar holderType
		Optional<ParameterTable> opHolderType = validateParamType(holderRow.getHolderTypeDescription(), lstParameter, 
				ParameterTableStateType.REGISTERED.getCode(), MasterTableType.PERSON_TYPE.getCode());
		
		if(opHolderType.isPresent()) {
			holderRow.setHolderType(opHolderType.get().getParameterTablePk());
		} else {
			lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
					HolderMassiveLabelType.TIPO_DE_TITULAR.toString(),
					PropertiesConstants.HOLDER_MASSIVE_HOLDER_TYPE_FIELD_ERROR_INVALID));
			return;
		}
	}
	
	private void generalNaturalValidation(List<RecordValidationType> lstErrors, HolderMassiveRow holderRow, List<ParameterTable> lstParameter, Map<String, GeographicLocation> nationalityMap) {		
		if(holderRow.getFirstLastName().isEmpty()) {
			lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
					HolderMassiveLabelType.PRIMER_APELLIDO.toString(),
					PropertiesConstants.HOLDER_MASSIVE_MSG_HOLDER_REQUIRED_FIELD));
			return;
		} else if(!holderRow.getFirstLastName().matches(lastNameRegex)) {
			lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
					HolderMassiveLabelType.PRIMER_APELLIDO.toString(),
					PropertiesConstants.HOLDER_MASSIVE_MGS_NAME_FIELD_REGEX_VALIDATION_FAILED));
			return;
		} else {
			holderRow.setFirstLastName(holderRow.getFirstLastName().toUpperCase());
		}
		
		if(Validations.validateIsNotNull(holderRow.getSecondLastName()) 
				&& holderRow.getSecondLastName().length() > 0) {			
			if(!holderRow.getSecondLastName().matches(lastNameRegex)) {
				lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
						HolderMassiveLabelType.SEGUNDO_APELLIDO.toString(),
						PropertiesConstants.HOLDER_MASSIVE_MGS_NAME_FIELD_REGEX_VALIDATION_FAILED));
				return;
			} else {
				holderRow.setSecondLastName(holderRow.getSecondLastName().toUpperCase());
			}
		}
		
		if(!holderRow.getName().matches(nameRegex)) {
			lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
					HolderMassiveLabelType.NOMBRES.toString(),
					PropertiesConstants.HOLDER_MASSIVE_MGS_NAME_FIELD_REGEX_VALIDATION_FAILED));
			return;
		} else {
			holderRow.setName(holderRow.getName().toUpperCase());
		}
		
		holderRow.setFullName(holderRow.getName().concat(GeneralConstants.BLANK_SPACE)
							.concat(holderRow.getFirstLastName()));
		
		if(Validations.validateIsNotNull(holderRow.getSecondLastName()) 
				&& holderRow.getSecondLastName().length() > 0 ) {
			holderRow.setFullName(holderRow.getFullName().concat(GeneralConstants.BLANK_SPACE).concat(holderRow.getSecondLastName()));
		}
		
		Optional<ParameterTable> sexParam = this.validateParamType(holderRow.getSexDescription(), lstParameter, 
				ParameterTableStateType.REGISTERED.getCode(), MasterTableType.SEX.getCode() );
		if(sexParam.isPresent()) {
			holderRow.setSex(sexParam.get().getParameterTablePk());
		} else {
			lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
					HolderMassiveLabelType.SEXO.toString(),
					PropertiesConstants.HOLDER_MASSIVE_MSG_FIELD_NOT_IN_LIST));
			return;
		}
		
	}
	
	private void generalJuridicValidation(List<RecordValidationType> lstErrors, HolderMassiveRow holderRow, List<ParameterTable> lstParameter, Map<String, GeographicLocation> nationalityMap) {				
		/*if(!holderRow.getFullName().matches(nameRegex)) {
			lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
					HolderMassiveLabelType.RAZON_SOCIAL_NOMBRE_COMPLETO.toString(),
					PropertiesConstants.HOLDER_MASSIVE_MGS_NAME_FIELD_REGEX_VALIDATION_FAILED));
			return;
		} else {
			holderRow.setFullName(holderRow.getFullName().toUpperCase());
		}*/
		holderRow.setFullName(holderRow.getFullName().toUpperCase());
		
		Optional<ParameterTable> economicSectorParam = this.validateParamType(holderRow.getEconomicSectorDescription(), lstParameter, 
				ParameterTableStateType.REGISTERED.getCode(), MasterTableType.ECONOMIC_SECTOR.getCode() );
		if(economicSectorParam.isPresent()) {
			holderRow.setEconomicSector(economicSectorParam.get().getParameterTablePk());
		} else {
			lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
					HolderMassiveLabelType.SECTOR_ECONOMICO.toString(),
					PropertiesConstants.HOLDER_MASSIVE_MSG_FIELD_NOT_IN_LIST));
			return;
		}		
		
		Optional<ParameterTable> economicActivityParam = this.validateParamType(holderRow.getEconomicActivityDescription(), lstParameter, 
				ParameterTableStateType.REGISTERED.getCode(), MasterTableType.ECONOMIC_ACTIVITY.getCode() );
		if(economicActivityParam.isPresent()) {
			holderRow.setEconomicActivity(economicActivityParam.get().getParameterTablePk());
		} else {
			lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
					HolderMassiveLabelType.ACTIVIDAD_ECONOMICA.toString(),
					PropertiesConstants.HOLDER_MASSIVE_MSG_FIELD_NOT_IN_LIST));
			return;
		}	
		
		Optional<ParameterTable> juridicClassParam = this.validateParamType(holderRow.getJuridicClassDescription(), lstParameter, 
				ParameterTableStateType.REGISTERED.getCode(), MasterTableType.T_CLASE_JURIDICO_TITULAR.getCode() );
		if(juridicClassParam.isPresent()) {
			holderRow.setJuridicClass(juridicClassParam.get().getParameterTablePk());
		} else {
			lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
					HolderMassiveLabelType.CLASE_JURIDICO.toString(),
					PropertiesConstants.HOLDER_MASSIVE_MSG_FIELD_NOT_IN_LIST));
			return;
		}
		
	}
	
	private void generalHolderValidations(List<RecordValidationType> lstErrors, HolderMassiveRow holderRow, List<ParameterTable> lstParameter, Map<String, GeographicLocation> nationalityMap) {

		GeographicLocation nationalityCountry = nationalityMap.get(holderRow.getNationalityCountryDescription().toUpperCase().trim());
		if(Validations.validateIsNull(nationalityCountry)) {
			lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
					HolderMassiveLabelType.NACIONALIDAD.toString(),
					PropertiesConstants.HOLDER_MASSIVE_MSG_COUNTRY_INVALID));
			return;
		} else {
			holderRow.setNationalityCountry(nationalityCountry);
			holderRow.setNationality(nationalityCountry.getIdGeographicLocationPk());
		}
		
		if(holderRow.getIndResidentDescryption().toUpperCase().trim().equals(BooleanType.YES.getValue())) {
			holderRow.setIndResidence(BooleanType.YES.getCode());
		} else if(holderRow.getIndResidentDescryption().toUpperCase().trim().equals(BooleanType.NO.getValue())) {
			holderRow.setIndResidence(BooleanType.NO.getCode());
		} else {
			lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
					HolderMassiveLabelType.ES_RESIDENTE.toString(),
					PropertiesConstants.HOLDER_MASSIVE_MSG_BOOLEAN_INVALID));
			return;
		}
				
		GeographicLocation residenceCountry = nationalityMap.get(holderRow.getLegalCountryDescription().toUpperCase().trim());
		if(Validations.validateIsNull(residenceCountry)) {
			lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
					HolderMassiveLabelType.PAIS.toString(),
					PropertiesConstants.HOLDER_MASSIVE_MSG_COUNTRY_INVALID));
			return;
		} else {
			holderRow.setLegalCountryResidence(residenceCountry.getIdGeographicLocationPk());
			holderRow.setLegalResidenceCountryCode(residenceCountry.getCodeGeographicLocation());
		}
		
		GeographicLocationTO geoFilter = new GeographicLocationTO();
		geoFilter.setGeographicLocationType(GeographicLocationType.DEPARTMENT.getCode());
		geoFilter.setIdLocationReferenceFk(holderRow.getLegalCountryResidence());
		geoFilter.setCountryName(holderRow.getLegalDepartmentDescription().toUpperCase().trim());
		
		try {
			GeographicLocation department = generalParametersFacade.findGeographicLocationByIdServiceFacade(geoFilter);
			if(Validations.validateIsNotNull(department)) {
				holderRow.setLegalDepartment(department.getIdGeographicLocationPk());
			} else {
				lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
						HolderMassiveLabelType.DEPARTAMENTO.toString(),
						PropertiesConstants.HOLDER_MASSIVE_MSG_DEPARTMENT_INVALID));
				return;
			}
		} catch (ServiceException e) {
			e.printStackTrace();
			lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
					HolderMassiveLabelType.DEPARTAMENTO.toString(),
					PropertiesConstants.HOLDER_MASSIVE_MSG_DEPARTMENT_INVALID));
			return;
		}
		
		geoFilter.setGeographicLocationType(GeographicLocationType.PROVINCE.getCode());
		geoFilter.setIdLocationReferenceFk(holderRow.getLegalDepartment());
		geoFilter.setCountryName(holderRow.getLegalProvinceDescription().toUpperCase().trim());
		
		try {
			GeographicLocation city = generalParametersFacade.findGeographicLocationByIdServiceFacade(geoFilter);
			if(Validations.validateIsNotNull(city)) {
				holderRow.setLegalProvince(city.getIdGeographicLocationPk());
			} else {
				lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
						HolderMassiveLabelType.CIUDAD.toString(),
						PropertiesConstants.HOLDER_MASSIVE_MSG_CITY_INVALID));
				return;
			}
		} catch (ServiceException e) {
			e.printStackTrace();
			lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
					HolderMassiveLabelType.CIUDAD.toString(),
					PropertiesConstants.HOLDER_MASSIVE_MSG_CITY_INVALID));
			return;
		}
		
		Optional<ParameterTable> documentTypeParam = this.validateParamType(holderRow.getDocumentTypeDescription(), lstParameter, 
				ParameterTableStateType.REGISTERED.getCode(), MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode() );
		if(documentTypeParam.isPresent()) {
			holderRow.setDocumentType(documentTypeParam.get().getParameterTablePk());
			
			if((holderRow.getHolderType().equals(PersonType.JURIDIC.getCode()) && !holderRow.getDocumentType().equals(DocumentType.RUC.getCode()))
				|| holderRow.getHolderType().equals(PersonType.NATURAL.getCode()) && holderRow.getDocumentType().equals(DocumentType.RUC.getCode())) {
				lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
						HolderMassiveLabelType.TIPO_DE_DOCUMENTO.toString(),
						PropertiesConstants.HOLDER_MASSIVE_MSG_HOLDER_TYPE_MISSMATCH));
				return;
			}
		} else {
			lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
					HolderMassiveLabelType.TIPO_DE_DOCUMENTO.toString(),
					PropertiesConstants.HOLDER_MASSIVE_DOCUMENT_TYPE_FIELD_ERROR_INVALID));
			return;
		}
		
		if((Validations.validateIsNotNull(holderRow.getDocumentType()) 
				&& !(holderRow.getDocumentType().equals(DocumentType.PAS.getCode()) || holderRow.getDocumentType().equals(DocumentType.CIE.getCode()) )
				&& !holderRow.getDocumentNumber().matches(regexLocalDocumentNumber))
			|| (Validations.validateIsNotNull(holderRow.getDocumentType()) 
				&& (holderRow.getDocumentType().equals(DocumentType.PAS.getCode()) || holderRow.getDocumentType().equals(DocumentType.CIE.getCode()) ) 
				&& !holderRow.getDocumentNumber().matches(passportRegex))) {
			lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
					HolderMassiveLabelType.NUMERO_DE_DOCUMENTO.toString(),
					PropertiesConstants.HOLDER_MASSIVE_MGS_DOCUMENT_NUMBER_FIELD_REGEX_VALIDATION_FAILED));
			return;
		}	
		
		if(!holderRow.getEmail().matches(emailRegex)) {
			lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
					HolderMassiveLabelType.CORREO.toString(),
					PropertiesConstants.HOLDER_MASSIVE_MGS_EMAIL_FIELD_REGEX_VALIDATION_FAILED));
			return;
		}
		
		if(holderRow.getIndFatcaDescription().trim().equals(BooleanType.YES.getValue())) {
			holderRow.setIndFatca(BooleanType.YES.getCode());
		} else if(holderRow.getIndFatcaDescription().trim().equals(BooleanType.NO.getValue())) {
			holderRow.setIndFatca(BooleanType.NO.getCode());
		} else {
			lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
					HolderMassiveLabelType.FATCA.toString(),
					PropertiesConstants.HOLDER_MASSIVE_MSG_BOOLEAN_INVALID));
			return;
		}
		
		if(holderRow.getIndPepDescription().trim().equals(BooleanType.YES.getValue())) {
			holderRow.setIndPep(BooleanType.YES.getCode());
		} else if(holderRow.getIndPepDescription().trim().equals(BooleanType.NO.getValue())) {
			holderRow.setIndPep(BooleanType.NO.getCode());
		} else {
			lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
					HolderMassiveLabelType.PEP.toString(),
					PropertiesConstants.HOLDER_MASSIVE_MSG_BOOLEAN_INVALID));
			return;
		}
		
		if(holderRow.getIndPep().equals(BooleanType.YES.getCode())) {
			Optional<ParameterTable> pepMotiveParam = this.validateParamTypeWithParamName(holderRow.getPepMotiveDescription(), lstParameter, 
					ParameterTableStateType.REGISTERED.getCode(), MasterTableType.ASSET_LAUNDRY_MOTIVE_PEP.getCode() );
			if(pepMotiveParam.isPresent()) {
				holderRow.setPepMotive(pepMotiveParam.get().getParameterTablePk());
			} else {
				lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
						HolderMassiveLabelType.MOTIVO_PEP.toString(),
						PropertiesConstants.HOLDER_MASSIVE_MSG_FIELD_NOT_IN_LIST));
				return;
			}
			
			Optional<ParameterTable> pepRoleParam = this.validateParamType(holderRow.getPepRoleDescription(), lstParameter, 
					ParameterTableStateType.REGISTERED.getCode(), MasterTableType.PEP_CHARGE.getCode() );
			if(pepRoleParam.isPresent()) {
				holderRow.setPepRole(pepRoleParam.get().getParameterTablePk());
			} else {
				lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
						HolderMassiveLabelType.CARGO_PEP.toString(),
						PropertiesConstants.HOLDER_MASSIVE_MSG_FIELD_NOT_IN_LIST));
				return;
			}
			
			if(Validations.validateIsNullOrEmpty(holderRow.getPepBeginningPeriod())) {
				lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
						HolderMassiveLabelType.PERIODO_DESDE.toString(),
						PropertiesConstants.HOLDER_MASSIVE_MSG_HOLDER_REQUIRED_FIELD));
				return;
			}
			
			if(Validations.validateIsNullOrEmpty(holderRow.getPepEndingPeriod())) {
				lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
						HolderMassiveLabelType.PERIODO_HASTA.toString(),
						PropertiesConstants.HOLDER_MASSIVE_MSG_HOLDER_REQUIRED_FIELD));
				return;
			}
			
			if(!pepMotiveParam.get().getParameterTablePk().equals(PepMotiveType.LINK_FOR_FAMILY.getCode())
					&& holderRow.getHolderType().equals(PersonType.JURIDIC.getCode()) ) {
				lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
						HolderMassiveLabelType.MOTIVO_PEP.toString(),
						PropertiesConstants.HOLDER_MASSIVE_MSG_FIELD_NOT_IN_LIST));
				return;
			}
			
			if(pepMotiveParam.get().getParameterTablePk().equals(PepMotiveType.LINK_FOR_FAMILY.getCode())) {
				
				if(Validations.validateIsNullOrEmpty(holderRow.getPepGradeDescription())) {
					lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
							HolderMassiveLabelType.GRADO_PEP.toString(),
							PropertiesConstants.HOLDER_MASSIVE_MSG_HOLDER_REQUIRED_FIELD));
					return;
					
				} 
					
				Optional<ParameterTable> pepGradeParam = this.validateParamTypeWithParamName(
						holderRow.getPepGradeDescription(), lstParameter, ParameterTableStateType.REGISTERED.getCode(),
						MasterTableType.PEP_GRADE.getCode());

				if (pepGradeParam.isPresent()) {

					if (pepGradeParam.get().getShortInteger().equals(holderRow.getHolderType())) {
						holderRow.setPepGrade(pepGradeParam.get().getParameterTablePk());
					} else {
						lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
								HolderMassiveLabelType.GRADO_PEP.toString(),
								PropertiesConstants.HOLDER_MASSIVE_MSG_FIELD_NOT_IN_LIST));
						return;
					}

				} else {
					lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
							HolderMassiveLabelType.GRADO_PEP.toString(),
							PropertiesConstants.HOLDER_MASSIVE_MSG_FIELD_NOT_IN_LIST));
					return;
				}
				
				if(Validations.validateIsNullOrEmpty(holderRow.getPepName())) {
					lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
							HolderMassiveLabelType.NOMBRE_PEP.toString(),
							PropertiesConstants.HOLDER_MASSIVE_MSG_HOLDER_REQUIRED_FIELD));
					return;
					
				}
				
			}
			
		}
	}
	
	private void holderAccountValidation(List<RecordValidationType> lstErrors, HolderMassiveRow holderRow, List<ParameterTable> lstParameter,
			List<Bank> lstBanks, Integer holdersSize){
		Optional<ParameterTable> accountTypeParam = this.validateParamType(holderRow.getHolderAccountTypeDescription(), lstParameter, 
				ParameterTableStateType.REGISTERED.getCode(), MasterTableType.ACCOUNTS_TYPE.getCode() );
		if(accountTypeParam.isPresent()) {
			holderRow.setHolderAccountType(accountTypeParam.get().getParameterTablePk());
			if((holderRow.getHolderAccountType().equals(HolderAccountType.JURIDIC.getCode()) && !holderRow.getHolderType().equals(PersonType.JURIDIC.getCode()))
					|| (holderRow.getHolderAccountType().equals(HolderAccountType.NATURAL.getCode()) && !holderRow.getHolderType().equals(PersonType.NATURAL.getCode()))){
				lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
						HolderMassiveLabelType.TIPO_DE_CUENTA_INVERSIONISTA.toString(),
						PropertiesConstants.HOLDER_MASSIVE_MSG_ACCOUNT_TYPE_MISSMATCH));
				return;
			}
		} else {
			lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
					HolderMassiveLabelType.TIPO_DE_CUENTA_INVERSIONISTA.toString(),
					PropertiesConstants.HOLDER_MASSIVE_MSG_FIELD_NOT_IN_LIST));
			return;
		}
		
		if((holderRow.getHolderAccountType().equals(HolderAccountType.NATURAL.getCode()) || holderRow.getHolderAccountType().equals(HolderAccountType.JURIDIC.getCode())) 
				&& !holdersSize.equals(GeneralConstants.ONE_VALUE_INTEGER)) {
			lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
					HolderMassiveLabelType.CUENTA_INVERSIONISTA_EN_DEPOSITANTE.toString(),
					PropertiesConstants.HOLDER_MASSIVE_MSG_NATURAL_HOLDER_ACCOUNT_SIZE_HOLDER_ERROR));
			return;
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(holderRow.getStockCodeDescription())){
			if(!holderRow.getStockCodeDescription().matches(regexOnlyNumber)) {
				lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
						HolderMassiveLabelType.CODIGO_BURSATIL.toString(),
						PropertiesConstants.HOLDER_MASSIVE_MSG_REGEX_ONLY_NUMBERS_FAILED));
				return;
			} else {
				holderRow.setStockCode(Integer.valueOf(holderRow.getStockCodeDescription()));
			}
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(holderRow.getRelatedName())){
			holderRow.setRelatedName(holderRow.getRelatedName().toUpperCase());
		}
		
		lstIndPddType = new ArrayList<ParameterTable>();			
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.STATE_INDPDD.getCode());
		Boolean blIndPdd = BooleanType.NO.getBooleanValue();
		try {
			lstIndPddType = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (ParameterTable lsIndPdd : lstIndPddType) {
			if (lsIndPdd.getShortInteger() == BlockedFieldsHolderAccountType.BLOCKED.getCode()) {
				blIndPdd = BooleanType.YES.getBooleanValue();
				}
		}
		if(blIndPdd && holderRow.getIndPddDescription().toUpperCase().trim().equals(BooleanType.NO.getValue())) {
			lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
					HolderMassiveLabelType.PDD.toString(),
					PropertiesConstants.HOLDER_MASSIVE_MSG_BOOLEAN_INVALID_INDPDD));
			return;
		}else {
			if(holderRow.getIndPddDescription().toUpperCase().trim().equals(BooleanType.YES.getValue())) {
				holderRow.setIndPdd(BooleanType.YES.getCode());
			} else if(holderRow.getIndPddDescription().toUpperCase().trim().equals(BooleanType.NO.getValue())) {
				holderRow.setIndPdd(BooleanType.NO.getCode());
			} else {
				lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
						HolderMassiveLabelType.PDD.toString(),
						PropertiesConstants.HOLDER_MASSIVE_MSG_BOOLEAN_INVALID));
				return;
			}
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(holderRow.getLocalCurrencyBankDescription())
				|| Validations.validateIsNotNullAndNotEmpty(holderRow.getUsdCurrencyBankDescription())) {
			
			if(Validations.validateIsNotNullAndNotEmpty(holderRow.getLocalCurrencyBankDescription())) {
				Optional<Bank> localCurrencyBank = this.validateBank(lstBanks, holderRow.getLocalCurrencyBankDescription());
				if(localCurrencyBank.isPresent()) {
					holderRow.setLocalBank(localCurrencyBank.get());
				} else {
					lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
							HolderMassiveLabelType.BANCO_GUARANIES.toString(),
							PropertiesConstants.HOLDER_MASSIVE_MSG_BANK_NOT_FOUND));
					return;
				}
				
				Optional<ParameterTable> localCurrencyBankAccountTypeParam = this.validateParamType(holderRow.getLocalCurrencyBankAccountTypeDescription(), lstParameter, 
						ParameterTableStateType.REGISTERED.getCode(), MasterTableType.MASTER_TABLE_BANK_ACCOUNT_TYPE.getCode() );
				if(localCurrencyBankAccountTypeParam.isPresent()) {
					holderRow.setLocalCurrencyBankAccountType(localCurrencyBankAccountTypeParam.get().getParameterTablePk());
				} else {
					lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
							HolderMassiveLabelType.TIPO_DE_CUENTA_BANCARIA_GUARANIES.toString(),
							PropertiesConstants.HOLDER_MASSIVE_MSG_FIELD_NOT_IN_LIST));
					return;
				}
				
				if(Validations.validateIsNotNullAndNotEmpty(holderRow.getLocalCurrencyBankAccountNumber())) {
					if(!holderRow.getLocalCurrencyBankAccountNumber().matches(regexOnlyNumber)) {
						lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
								HolderMassiveLabelType.NUMERO_CUENTA_BANCARIA_GUARANIES.toString(),
								PropertiesConstants.HOLDER_MASSIVE_MSG_REGEX_ONLY_NUMBERS_FAILED));
						return;
					}
				} else {
					lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
							HolderMassiveLabelType.NUMERO_CUENTA_BANCARIA_GUARANIES.toString(),
							PropertiesConstants.HOLDER_MASSIVE_MSG_BANK_ACCOUNT_NUMBER_NOT_FOUND));
					return;
				}
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(holderRow.getUsdCurrencyBankDescription())) {
				Optional<Bank> usdCurrencyBank = this.validateBank(lstBanks, holderRow.getUsdCurrencyBankDescription());
				if(usdCurrencyBank.isPresent()) {
					holderRow.setUsdBank(usdCurrencyBank.get());
				} else {
					lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
							HolderMassiveLabelType.BANCO_DOLARES.toString(),
							PropertiesConstants.HOLDER_MASSIVE_MSG_BANK_NOT_FOUND));
					return;
				}
				
				Optional<ParameterTable> usdCurrencyBankAccountTypeParam = this.validateParamType(holderRow.getUsdCurrencyBankAccountTypeDescription(), lstParameter, 
						ParameterTableStateType.REGISTERED.getCode(), MasterTableType.MASTER_TABLE_BANK_ACCOUNT_TYPE.getCode() );
				if(usdCurrencyBankAccountTypeParam.isPresent()) {
					holderRow.setUsdCurrencyBankAccountType(usdCurrencyBankAccountTypeParam.get().getParameterTablePk());
				} else {
					lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
							HolderMassiveLabelType.TIPO_DE_CUENTA_BANCARIA_DOLARES.toString(),
							PropertiesConstants.HOLDER_MASSIVE_MSG_FIELD_NOT_IN_LIST));
					return;
				}
				
				if(Validations.validateIsNotNullAndNotEmpty(holderRow.getUsdCurrencyBankAccountNumber())) {
					if(!holderRow.getUsdCurrencyBankAccountNumber().matches(regexOnlyNumber)) {
						lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
								HolderMassiveLabelType.NUMERO_CUENTA_BANCARIA_DOLARES.toString(),
								PropertiesConstants.HOLDER_MASSIVE_MSG_REGEX_ONLY_NUMBERS_FAILED));
						return;
					}
				} else {
					lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
							HolderMassiveLabelType.NUMERO_CUENTA_BANCARIA_DOLARES.toString(),
							PropertiesConstants.HOLDER_MASSIVE_MSG_BANK_ACCOUNT_NUMBER_NOT_FOUND));
					return;
				}
			}
		} else {
			lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
					HolderMassiveLabelType.BANCO_GUARANIES.toString(),
					PropertiesConstants.HOLDER_MASSIVE_MSG_BANK_ACCOUNT_NOT_FOUND));
			return;
		}
	}
	

	private void generalRepresentativeValidation(List<RecordValidationType> lstErrors, HolderMassiveRow holderRow, List<ParameterTable> lstParameter, Map<String, GeographicLocation> nationalityMap) {
		
		if(Validations.validateIsNotNullAndNotEmpty(holderRow.getRepresentativeDocumentTypeDescription())) {
			//Revisar holderType
			Optional<ParameterTable> opHolderType = validateParamType(holderRow.getRepresentativeTypeDescription(), lstParameter, 
					ParameterTableStateType.REGISTERED.getCode(), MasterTableType.PERSON_TYPE.getCode());
			
			if(opHolderType.isPresent()) {
				holderRow.setRepresentativeType(opHolderType.get().getParameterTablePk());
			} else {
				lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
						HolderMassiveLabelType.TIPO_DE_REPRESENTANTE.toString(),
						PropertiesConstants.HOLDER_MASSIVE_REPRESENTATIVE_TYPE_FIELD_ERROR_INVALID));
				return;
			}
			
			GeographicLocation nationalityCountry = nationalityMap.get(holderRow.getRepresentativeNationalityCountryDescription().toUpperCase().trim());
			if(Validations.validateIsNull(nationalityCountry)) {
				lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
						HolderMassiveLabelType.NACIONALIDAD_REPRESENTANTE.toString(),
						PropertiesConstants.HOLDER_MASSIVE_MSG_COUNTRY_INVALID));
				return;
			} else {
				holderRow.setRepresentativeNationality(nationalityCountry.getIdGeographicLocationPk());
				holderRow.setRepresentativeNationalityCode(nationalityCountry.getCodeGeographicLocation());
			}
			
			if(holderRow.getRepresentativeIndResidentDescription().toUpperCase().trim().equals(BooleanType.YES.getValue())) {
				holderRow.setRepresentativeIndResident(BooleanType.YES.getCode());
			} else if(holderRow.getRepresentativeIndResidentDescription().toUpperCase().trim().equals(BooleanType.NO.getValue())) {
				holderRow.setRepresentativeIndResident(BooleanType.NO.getCode());
			} else {
				lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
						HolderMassiveLabelType.REPRESENTANTE_ES_RESIDENTE.toString(),
						PropertiesConstants.HOLDER_MASSIVE_MSG_BOOLEAN_INVALID));
				return;
			}
					
			GeographicLocation residenceCountry = nationalityMap.get(holderRow.getRepresentativeLegalCountryDescription().toUpperCase().trim());
			if(Validations.validateIsNull(residenceCountry)) {
				lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
						HolderMassiveLabelType.PAIS_REPRESENTANTE.toString(),
						PropertiesConstants.HOLDER_MASSIVE_MSG_COUNTRY_INVALID));
				return;
			} else {
				holderRow.setRepresentativeLegalCountry(residenceCountry.getIdGeographicLocationPk());
				holderRow.setRepresentativeLegalCountryCode(residenceCountry.getCodeGeographicLocation());
			}
			
			GeographicLocationTO geoFilter = new GeographicLocationTO();
			geoFilter.setGeographicLocationType(GeographicLocationType.DEPARTMENT.getCode());
			geoFilter.setIdLocationReferenceFk(holderRow.getRepresentativeLegalCountry());
			geoFilter.setCountryName(holderRow.getRepresentativeLegalDepartmentDescription().toUpperCase().trim());
			
			try {
				GeographicLocation department = generalParametersFacade.findGeographicLocationByIdServiceFacade(geoFilter);
				if(Validations.validateIsNotNull(department)) {
					holderRow.setRepresentativeLegalDepartment(department.getIdGeographicLocationPk());
				} else {
					lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
							HolderMassiveLabelType.DEPARTAMENTO_REPRESENTANTE.toString(),
							PropertiesConstants.HOLDER_MASSIVE_MSG_DEPARTMENT_INVALID));
					return;
				}
			} catch (ServiceException e) {
				e.printStackTrace();
				lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
						HolderMassiveLabelType.DEPARTAMENTO_REPRESENTANTE.toString(),
						PropertiesConstants.HOLDER_MASSIVE_MSG_DEPARTMENT_INVALID));
				return;
			}
			
			geoFilter.setGeographicLocationType(GeographicLocationType.PROVINCE.getCode());
			geoFilter.setIdLocationReferenceFk(holderRow.getRepresentativeLegalDepartment());
			geoFilter.setCountryName(holderRow.getRepresentativeLegalProvinceDescription().toUpperCase().trim());
			
			try {
				GeographicLocation city = generalParametersFacade.findGeographicLocationByIdServiceFacade(geoFilter);
				if(Validations.validateIsNotNull(city)) {
					holderRow.setRepresentativeLegalProvince(city.getIdGeographicLocationPk());
				} else {
					lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
							HolderMassiveLabelType.CIUDAD_REPRESENTANTE.toString(),
							PropertiesConstants.HOLDER_MASSIVE_MSG_CITY_INVALID));
					return;
				}
			} catch (ServiceException e) {
				e.printStackTrace();
				lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
						HolderMassiveLabelType.CIUDAD_REPRESENTANTE.toString(),
						PropertiesConstants.HOLDER_MASSIVE_MSG_CITY_INVALID));
				return;
			}
			
			Optional<ParameterTable> documentTypeParam = this.validateParamType(holderRow.getRepresentativeDocumentTypeDescription(), lstParameter, 
					ParameterTableStateType.REGISTERED.getCode(), MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode() );
			if(documentTypeParam.isPresent()) {
				holderRow.setRepresentativeTypeDocument(documentTypeParam.get().getParameterTablePk());
			} else {
				lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
						HolderMassiveLabelType.TIPO_DE_DOCUMENTO_REPRESENTANTE.toString(),
						PropertiesConstants.HOLDER_MASSIVE_DOCUMENT_TYPE_FIELD_ERROR_INVALID));
				return;
			}
			
			if(!holderRow.getRepresentativeDocumentNumber().matches(regexLocalDocumentNumber)) {
				lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
						HolderMassiveLabelType.NUMERO_DE_DOCUMENTO_REPRESENTANTE.toString(),
						PropertiesConstants.HOLDER_MASSIVE_MGS_DOCUMENT_NUMBER_FIELD_REGEX_VALIDATION_FAILED));
				return;
			}
			
			if(!holderRow.getRepresentativeFirstLastName().matches(lastNameRegex)) {
				lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
						HolderMassiveLabelType.PRIMER_APELLIDO_REPRESENTANTE.toString(),
						PropertiesConstants.HOLDER_MASSIVE_MGS_NAME_FIELD_REGEX_VALIDATION_FAILED));
				return;
			} else {
				holderRow.setRepresentativeFirstLastName(holderRow.getRepresentativeFirstLastName().toUpperCase());
			}
			
			if(Validations.validateIsNotNull(holderRow.getRepresentativeSecondLastName()) 
					&& holderRow.getRepresentativeSecondLastName().length() > 0) {			
				if(!holderRow.getRepresentativeSecondLastName().matches(lastNameRegex)) {
					lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
							HolderMassiveLabelType.SEGUNDO_APELLIDO_REPRESENTANTE.toString(),
							PropertiesConstants.HOLDER_MASSIVE_MGS_NAME_FIELD_REGEX_VALIDATION_FAILED));
					return;
				} else {
					holderRow.setRepresentativeSecondLastName(holderRow.getRepresentativeSecondLastName().toUpperCase());
				}
			}
			
			if(!holderRow.getRepresentativeName().matches(nameRegex)) {
				lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
						HolderMassiveLabelType.NOMBRE_DEL_REPRESENTANTE.toString(),
						PropertiesConstants.HOLDER_MASSIVE_MGS_NAME_FIELD_REGEX_VALIDATION_FAILED));
				return;
			} else {
				holderRow.setRepresentativeName(holderRow.getRepresentativeName().toUpperCase());
			}
			
			holderRow.setRepresentativeFullName(holderRow.getRepresentativeName().concat(GeneralConstants.BLANK_SPACE)
								.concat(holderRow.getRepresentativeFirstLastName()));
			
			if(Validations.validateIsNotNull(holderRow.getRepresentativeSecondLastName()) 
					&& holderRow.getRepresentativeSecondLastName().length() > 0 ) {
				holderRow.setRepresentativeFullName(holderRow.getRepresentativeFullName().concat(GeneralConstants.BLANK_SPACE).concat(holderRow.getSecondLastName()));
			}
			
			if(!holderRow.getRepresentativeEmail().matches(emailRegex)) {
				lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
						HolderMassiveLabelType.CORREO_REPRESENTANTE.toString(),
						PropertiesConstants.HOLDER_MASSIVE_MGS_EMAIL_FIELD_REGEX_VALIDATION_FAILED));
				return;
			}
			
			Optional<ParameterTable> sexParam = this.validateParamType(holderRow.getRepresentativeSexDescription(), lstParameter, 
					ParameterTableStateType.REGISTERED.getCode(), MasterTableType.SEX.getCode() );
			if(sexParam.isPresent()) {
				holderRow.setRepresentativeSex(sexParam.get().getParameterTablePk());
			} else {
				lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
						HolderMassiveLabelType.SEXO_REPRESENTANTE.toString(),
						PropertiesConstants.HOLDER_MASSIVE_MSG_FIELD_NOT_IN_LIST));
				return;
			}
			
			Optional<ParameterTable> representativeClassParam = this.validateParamType(holderRow.getRepresentativeClassDescription(), lstParameter, 
					ParameterTableStateType.REGISTERED.getCode(), MasterTableType.CLASS_REPRENTATIVE.getCode() );
			if(representativeClassParam.isPresent()) {
				holderRow.setRepresentativeClass(representativeClassParam.get().getParameterTablePk());
			} else {
				lstErrors.add(CommonsUtilities.populateRecordValidation(holderRow,
						HolderMassiveLabelType.CLASE_REPRESENTANTE.toString(),
						PropertiesConstants.HOLDER_MASSIVE_MSG_FIELD_NOT_IN_LIST));
				return;
			}
		}		
	}
	
	
	public Optional<ParameterTable> validateParamType(String parameterDescription, List<ParameterTable> lstParameter,
			Integer paramStateTypeCode, Integer masterTablePk){
		return lstParameter.stream()
				.filter(param -> param.getDescription().equals(parameterDescription.trim().toUpperCase()))
				.filter(param -> param.getParameterState().equals(paramStateTypeCode))
				.filter(param -> param.getMasterTable().getMasterTablePk().equals(masterTablePk))
				.findFirst();
	}
	
	public Optional<ParameterTable> validateParamTypeWithParamName(String parameterName, List<ParameterTable> lstParameter,
			Integer paramStateTypeCode, Integer masterTablePk){
		return lstParameter.stream()
				.filter(param -> param.getParameterName().equals(parameterName.trim().toUpperCase()))
				.filter(param -> param.getParameterState().equals(paramStateTypeCode))
				.filter(param -> param.getMasterTable().getMasterTablePk().equals(masterTablePk))
				.findFirst();
	}
	
	public Optional<Bank> validateBank(List<Bank> lstBanks, String bankDescription){
		return lstBanks.stream()
				.filter(bank -> bank.getDescription().equals(bankDescription.trim().toUpperCase()))
				.findFirst();
	}
	
	public byte[] getAcceptedMassiveFileContent(Long idProcessFilePk) {
		return holderMassiveAdditionalService.getAcceptedMassiveFileContent(idProcessFilePk);
	}
	
	public byte[] getRejectedMassiveFileContent(Long idProcessFilePk) {
		return holderMassiveAdditionalService.getRejectedMassiveFileContent(idProcessFilePk);
	}

}
