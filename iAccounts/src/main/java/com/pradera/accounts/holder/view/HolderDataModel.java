package com.pradera.accounts.holder.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.accounts.Holder;





// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class HolderDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
public class HolderDataModel extends ListDataModel<Holder> implements SelectableDataModel<Holder>, Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new holder data model.
     */
    public HolderDataModel(){
		
	}
	
	/**
	 * Instantiates a new holder data model.
	 *
	 * @param data the data
	 */
	public HolderDataModel(List<Holder> data){
		super(data);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public Holder getRowData(String rowKey) {
		List<Holder> holderList=(List<Holder>)getWrappedData();
        for(Holder objHolder : holderList) {  
        	
            if(String.valueOf(objHolder.getIdHolderPk()).equals(rowKey))
                return objHolder;  
        }  
		return null;
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(Holder objHolder) {
		return objHolder.getIdHolderPk();
	}		

}
