package com.pradera.accounts.holder.massive.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.utils.DocumentTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ValidationOperationType;
import com.pradera.integration.component.accounts.to.BankAccountObjectTO;
import com.pradera.integration.component.accounts.to.HolderAccountObjectTO;
import com.pradera.integration.component.accounts.to.JuridicHolderObjectTO;
import com.pradera.integration.component.accounts.to.LegalRepresentativeObjectTO;
import com.pradera.integration.component.accounts.to.NaturalHolderObjectTO;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.type.CurrencyType;

public class HolderMassiveRow extends ValidationOperationType implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8125036243029338889L;
	
	private Boolean multipleHolder = Boolean.FALSE;
	
	private DocumentTO documentTO;
	
	private String nationalityCountryDescription;
	private GeographicLocation nationalityCountry;
	private Integer nationality;
	private String legalResidenceCountryCode;
	
	private Integer legalCountryResidence;
	private String legalCountryDescription;	
	private Integer legalDepartment;
	private String legalDepartmentDescription;
	private Integer legalProvince;
	private String legalProvinceDescription;
	private Integer legalDistric;
	private String legalDistricDescription;	
	
	private String sexDescription;
	private Integer sex;
	
	private String indResidentDescryption; 
	private Integer indResidence;
	
	private Date birthDate;
	private String birthDateString;
	
	private String holderTypeDescription;
	private Integer holderType;
	
	private String firstLastName;
	private String secondLastName;
	private String name;
	private String fullName;
	private String email;
	
	private String documentTypeDescription;
	private Integer documentType;
	private String documentNumber;
	
	private String address;
	
	private String indFatcaDescription;
	private Integer indFatca;
	
	private String juridicClassDescription;
	private Integer juridicClass;
	
	private String indPepDescription;
	private Integer indPep;
	
	private String pepMotiveDescription;
	private Integer pepMotive;
	private String pepRoleDescription;
	private Integer pepRole;
	//HolderAccount
	private String holderAccountTypeDescription;
	private Integer holderAccountType;
	
	private Integer holderAccountNumber;
	
	private String stockCodeDescription;
	private Integer stockCode;
			
	private String localCurrencyBankDescription;
	private Bank localBank;
	private String localCurrencyBankAccountTypeDescription;
	private Integer localCurrencyBankAccountType;
	private String localCurrencyBankAccountNumber;
	
	private String usdCurrencyBankDescription;
	private Bank usdBank;
	private String usdCurrencyBankAccountTypeDescription;
	private Integer usdCurrencyBankAccountType;
	private String usdCurrencyBankAccountNumber;
	
	private HolderAccountObjectTO holderAccountObjectTO;
	
	private Integer rowNumber;
	
	private String economicSectorDescription;
	private Integer economicSector;
	private String economicActivityDescription;
	private Integer economicActivity;
	
	private String relatedName;
	private String indPddDescription;
	private Integer indPdd;
	
	//Representative
	private String representativeName;
	private String representativeFirstLastName;
	private String representativeSecondLastName;
	private String representativeFullName;
	private String representativeEmail;
	
	private String representativeNationalityCountryDescription;
	private Integer representativeNationality;
	private String representativeNationalityCode;
	private String representativeIndResidentDescription;
	private Integer representativeIndResident;
	private String representativeLegalCountryDescription;
	private Integer representativeLegalCountry;
	private String representativeLegalCountryCode;
	private String representativeLegalDepartmentDescription;
	private Integer representativeLegalDepartment;
	private String representativeLegalProvinceDescription;
	private Integer representativeLegalProvince;
	
	private String representativeAddress;
	
	private String representativeTypeDescription;
	private Integer representativeType;
	private String representativeDocumentTypeDescription;	
	private Integer representativeTypeDocument;
	private String representativeDocumentNumber;
	private String representativeSexDescription;
	private Integer representativeSex;
	private String representativeClassDescription;
	private Integer representativeClass;
	private String representativeBirthDateString;
	private Date representativeBirthDate;
	
	private Integer mobileNumber;
	
	private String pepGradeDescription;
	private Integer pepGrade;
	private String pepName;
	private Date pepBeginningPeriod;
	private Date pepEndingPeriod;
	
	public void buildGeneralHolderAccountObject() {
		this.holderAccountObjectTO = new HolderAccountObjectTO();
		
		holderAccountObjectTO.setDpfInterface(Boolean.FALSE);
		holderAccountObjectTO.setAccountType(holderAccountType);
		holderAccountObjectTO.setValidateAccount(Boolean.FALSE);
		holderAccountObjectTO.setAccountNumber(holderAccountNumber);
		holderAccountObjectTO.setStockCode(stockCode);	
		holderAccountObjectTO.setRelatedName(relatedName);
		holderAccountObjectTO.setIndPdd(indPdd);
		holderAccountObjectTO.setRowNumber(rowNumber);
		
		if(Validations.validateIsNotNull(localBank) || Validations.validateIsNotNull(usdBank)) {
			holderAccountObjectTO.setLstBankAccountObjectTOs(new ArrayList<BankAccountObjectTO>());
			
			if(Validations.validateIsNotNull(localBank)) {
				BankAccountObjectTO localBankObj = new BankAccountObjectTO();
				localBankObj.setAccountNumber(localCurrencyBankAccountNumber);
				localBankObj.setCurrency(CurrencyType.PYG.getCode());
				localBankObj.setIdBankPk(localBank.getIdBankPk());
				localBankObj.setIdParticipantPk(getIdFileParticipantPk());
				localBankObj.setBankAccountType(localCurrencyBankAccountType);
				localBankObj.setBankDescription(localBank.getDescription());
				holderAccountObjectTO.getLstBankAccountObjectTOs().add(localBankObj);
			}
			
			if(Validations.validateIsNotNull(usdBank)) {
				BankAccountObjectTO usdBankObj = new BankAccountObjectTO();
				usdBankObj.setAccountNumber(usdCurrencyBankAccountNumber);
				usdBankObj.setCurrency(CurrencyType.USD.getCode());
				usdBankObj.setIdBankPk(usdBank.getIdBankPk());
				usdBankObj.setIdParticipantPk(getIdFileParticipantPk());
				usdBankObj.setBankAccountType(usdCurrencyBankAccountType);
				usdBankObj.setBankDescription(usdBank.getDescription());
				holderAccountObjectTO.getLstBankAccountObjectTOs().add(usdBankObj);
			}
		}	
		
		holderAccountObjectTO.setCreateObject(Boolean.TRUE);

		holderAccountObjectTO.setIdParticipant(getIdFileParticipantPk());
		holderAccountObjectTO.setIdParticipantPk(getIdFileParticipantPk());
	}
	
	public void buildJuridicHolderAccountObject() {
		buildGeneralHolderAccountObject();

		JuridicHolderObjectTO to = new JuridicHolderObjectTO();
		to.setBirthDate(birthDate);
		to.setFullName(fullName);
		to.setDocumentType(documentType);
		to.setDocumentNumber(documentNumber);
		to.setNationality(nationality);
		to.setNationalityCode(nationalityCountry.getCodeGeographicLocation());
		to.setLegalResidenceCountry(legalCountryResidence);
		to.setLegalResidenceCountryCode(legalResidenceCountryCode);
		to.setEmail(email);
		to.setIndResidence(indResidence);		
		to.setLegalAddress(address);
		to.setLegalDepartment(legalDepartment);
		to.setLegalDistrict(legalDistric);
		to.setLegalProvince(legalProvince);
		to.setSex(sex);
		to.setMobileNumber(mobileNumber.toString());
		to.setIndFatca(indFatca);
		to.setEconomicActivity(economicActivity);
		to.setEconomicSector(economicSector);
		to.setJuridicClass(juridicClass);
		to.setDpfInterface(Boolean.FALSE);
		to.setIndPep(indPep);

		if(to.getIndPep().equals(BooleanType.YES.getCode())) {
			to.setPepMotive(pepMotive);
			to.setPepRole(pepRole);
			to.setPepGrade(pepGrade);
			to.setPepName(pepName);
			to.setPepBeginningPeriod(pepBeginningPeriod);
			to.setPepEndingPeriod(pepEndingPeriod);
		}
		
		to.setRowNumber(rowNumber);
		
		if(Validations.validateIsNotNullAndNotEmpty(representativeTypeDocument)) {
			to.setLstLegalRepresentativeObjects(new ArrayList<LegalRepresentativeObjectTO>());
			to.getLstLegalRepresentativeObjects().add(buildLegalRepresentativeObject());
			
			holderAccountObjectTO.setLstLegalRepresentativeObjectTOs(new ArrayList<LegalRepresentativeObjectTO>());
			holderAccountObjectTO.getLstLegalRepresentativeObjectTOs().add(buildLegalRepresentativeObject());
		}
		
		this.holderAccountObjectTO.setJuridicHolderObjectTO(to);	
		
		holderAccountObjectTO.setLstNaturalHolderObjectTOs(new ArrayList<NaturalHolderObjectTO>());
		
	}
	
	public void buildNaturalHolderAccountObject() {
		buildGeneralHolderAccountObject();
		
		holderAccountObjectTO.setLstNaturalHolderObjectTOs(new ArrayList<NaturalHolderObjectTO>());		
		NaturalHolderObjectTO naturalHolderObjectTO = new NaturalHolderObjectTO();
		naturalHolderObjectTO.setBirthDate(birthDate);
		naturalHolderObjectTO.setFirstLastName(firstLastName);
		naturalHolderObjectTO.setSecondLastName(secondLastName);
		naturalHolderObjectTO.setName(name);
		naturalHolderObjectTO.setDocumentType(documentType);
		naturalHolderObjectTO.setDocumentNumber(documentNumber);
		naturalHolderObjectTO.setNationality(nationality);
		naturalHolderObjectTO.setNationalityCode(nationalityCountry.getCodeGeographicLocation());
		naturalHolderObjectTO.setLegalResidenceCountry(legalCountryResidence);
		naturalHolderObjectTO.setLegalResidenceCountryCode(legalResidenceCountryCode);
		naturalHolderObjectTO.setEmail(email);
		naturalHolderObjectTO.setIndResidence(indResidence);		
		naturalHolderObjectTO.setLegalAddress(address);
		naturalHolderObjectTO.setLegalDepartment(legalDepartment);
		naturalHolderObjectTO.setLegalDistrict(legalDistric);
		naturalHolderObjectTO.setLegalProvince(legalProvince);
		naturalHolderObjectTO.setSex(sex);	
		naturalHolderObjectTO.setMobileNumber(mobileNumber.toString());
		naturalHolderObjectTO.setIndFatca(indFatca);
		naturalHolderObjectTO.setIndPep(indPep);
		if(indPep.equals(BooleanType.YES.getCode())) {
			naturalHolderObjectTO.setPepMotive(pepMotive);
			naturalHolderObjectTO.setPepRole(pepRole);
			naturalHolderObjectTO.setPepGrade(pepGrade);
			naturalHolderObjectTO.setPepName(pepName);
			naturalHolderObjectTO.setPepBeginningPeriod(pepBeginningPeriod);
			naturalHolderObjectTO.setPepEndingPeriod(pepEndingPeriod);
		}
		naturalHolderObjectTO.setFullName(name.concat(GeneralConstants.BLANK_SPACE).concat(firstLastName));
		if(Validations.validateIsNotNull(secondLastName))
			naturalHolderObjectTO.setFullName(naturalHolderObjectTO.getFullName().concat(GeneralConstants.BLANK_SPACE).concat(secondLastName));
		
		int age = CommonsUtilities.calculateAge(birthDate);
		if(age<=18) {
			naturalHolderObjectTO.setIndMinor(BooleanType.YES.getCode());
		} else {
			naturalHolderObjectTO.setIndMinor(BooleanType.NO.getCode());
		}
		naturalHolderObjectTO.setIndRepresentative(BooleanType.YES.getCode());
		
		naturalHolderObjectTO.setRowNumber(rowNumber);
			
		holderAccountObjectTO.getLstNaturalHolderObjectTOs().add(naturalHolderObjectTO);
		
		if(Validations.validateIsNotNullAndNotEmpty(representativeTypeDocument)) {
			naturalHolderObjectTO.setLstLegalRepresentativeObjects(new ArrayList<LegalRepresentativeObjectTO>());
			naturalHolderObjectTO.getLstLegalRepresentativeObjects().add(buildLegalRepresentativeObject());
		}
	}
	
	private LegalRepresentativeObjectTO buildLegalRepresentativeObject() {
		LegalRepresentativeObjectTO to = new LegalRepresentativeObjectTO();
		to.setBirthDate(representativeBirthDate);
		to.setDocumentNumber(representativeDocumentNumber);
		to.setDocumentType(representativeTypeDocument);
		to.setEmail(representativeEmail);
		to.setFirstLastName(representativeFirstLastName);
		to.setFullName(representativeFullName);
		to.setSecondLastName(representativeSecondLastName);
		to.setNationality(representativeNationality);
		to.setNationalityCode(representativeNationalityCode);
		to.setIndResidence(representativeIndResident);
		to.setLegalResidenceCountry(representativeLegalCountry);
		to.setLegalResidenceCountryCode(representativeLegalCountryCode);
		to.setLegalDepartment(representativeLegalDepartment);
		to.setLegalProvince(representativeLegalProvince);
		to.setLegalAddress(representativeAddress);
		to.setName(representativeName);
		to.setPersonType(representativeType);
		to.setSex(representativeSex);
		to.setRepresentativeClass(representativeClass);
		
		return to;
	}
	

	public String getHolderTypeDescription() {
		return holderTypeDescription;
	}

	public void setHolderTypeDescription(String holderTypeDescription) {
		this.holderTypeDescription = holderTypeDescription;
	}

	public Integer getHolderType() {
		return holderType;
	}

	public void setHolderType(Integer holderType) {
		this.holderType = holderType;
	}

	public String getFirstLastName() {
		return firstLastName;
	}

	public void setFirstLastName(String firstLastName) {
		this.firstLastName = firstLastName;
	}

	public String getSecondLastName() {
		return secondLastName;
	}

	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDocumentTypeDescription() {
		return documentTypeDescription;
	}

	public void setDocumentTypeDescription(String documentTypeDescription) {
		this.documentTypeDescription = documentTypeDescription;
	}

	public Integer getDocumentType() {
		return documentType;
	}

	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getNationalityCountryDescription() {
		return nationalityCountryDescription;
	}

	public void setNationalityCountryDescription(String nationalityCountryDescription) {
		this.nationalityCountryDescription = nationalityCountryDescription;
	}

	public GeographicLocation getNationalityCountry() {
		return nationalityCountry;
	}

	public void setNationalityCountry(GeographicLocation nationalityCountry) {
		this.nationalityCountry = nationalityCountry;
	}

	public Integer getNationality() {
		return nationality;
	}

	public void setNationality(Integer nationality) {
		this.nationality = nationality;
	}

	public Integer getIndResidence() {
		return indResidence;
	}

	public void setIndResidence(Integer indResidence) {
		this.indResidence = indResidence;
	}

	public DocumentTO getDocumentTO() {
		return documentTO;
	}

	public void setDocumentTO(DocumentTO documentTO) {
		this.documentTO = documentTO;
	}

	public String getIndResidentDescryption() {
		return indResidentDescryption;
	}

	public void setIndResidentDescryption(String indResidentDescryption) {
		this.indResidentDescryption = indResidentDescryption;
	}

	public Integer getLegalCountryResidence() {
		return legalCountryResidence;
	}

	public void setLegalCountryResidence(Integer legalCountryResidence) {
		this.legalCountryResidence = legalCountryResidence;
	}

	public String getLegalCountryDescription() {
		return legalCountryDescription;
	}

	public void setLegalCountryDescription(String legalCountryDescription) {
		this.legalCountryDescription = legalCountryDescription;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getBirthDateString() {
		return birthDateString;
	}

	public void setBirthDateString(String birthDateString) {
		this.birthDateString = birthDateString;
	}


	public HolderAccountObjectTO getHolderAccountObjectTO() {
		return holderAccountObjectTO;
	}


	public void setHolderAccountObjectTO(HolderAccountObjectTO holderAccountObjectTO) {
		this.holderAccountObjectTO = holderAccountObjectTO;
	}


	public String getHolderAccountTypeDescription() {
		return holderAccountTypeDescription;
	}


	public void setHolderAccountTypeDescription(String holderAccountTypeDescription) {
		this.holderAccountTypeDescription = holderAccountTypeDescription;
	}


	public Integer getHolderAccountType() {
		return holderAccountType;
	}


	public void setHolderAccountType(Integer holderAccountType) {
		this.holderAccountType = holderAccountType;
	}


	public Integer getLegalDepartment() {
		return legalDepartment;
	}


	public void setLegalDepartment(Integer legalDepartment) {
		this.legalDepartment = legalDepartment;
	}


	public String getLegalDepartmentDescription() {
		return legalDepartmentDescription;
	}


	public void setLegalDepartmentDescription(String legalDepartmentDescription) {
		this.legalDepartmentDescription = legalDepartmentDescription;
	}


	public Integer getLegalProvince() {
		return legalProvince;
	}


	public void setLegalProvince(Integer legalProvince) {
		this.legalProvince = legalProvince;
	}


	public String getLegalProvinceDescription() {
		return legalProvinceDescription;
	}


	public void setLegalProvinceDescription(String legalProvinceDescription) {
		this.legalProvinceDescription = legalProvinceDescription;
	}


	public Integer getLegalDistric() {
		return legalDistric;
	}


	public void setLegalDistric(Integer legalDistric) {
		this.legalDistric = legalDistric;
	}


	public String getLegalDistricDescription() {
		return legalDistricDescription;
	}


	public void setLegalDistricDescription(String legalDistricDescription) {
		this.legalDistricDescription = legalDistricDescription;
	}


	public String getSexDescription() {
		return sexDescription;
	}


	public void setSexDescription(String sexDescription) {
		this.sexDescription = sexDescription;
	}


	public Integer getSex() {
		return sex;
	}


	public void setSex(Integer sex) {
		this.sex = sex;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public Integer getHolderAccountNumber() {
		return holderAccountNumber;
	}


	public void setHolderAccountNumber(Integer holderAccountNumber) {
		this.holderAccountNumber = holderAccountNumber;
	}


	public String getLegalResidenceCountryCode() {
		return legalResidenceCountryCode;
	}


	public void setLegalResidenceCountryCode(String legalResidenceCountryCode) {
		this.legalResidenceCountryCode = legalResidenceCountryCode;
	}


	public String getIndFatcaDescription() {
		return indFatcaDescription;
	}


	public void setIndFatcaDescription(String indFatcaDescription) {
		this.indFatcaDescription = indFatcaDescription;
	}


	public Integer getIndFatca() {
		return indFatca;
	}


	public void setIndFatca(Integer indFatca) {
		this.indFatca = indFatca;
	}


	public String getLocalCurrencyBankDescription() {
		return localCurrencyBankDescription;
	}


	public void setLocalCurrencyBankDescription(String localCurrencyBankDescription) {
		this.localCurrencyBankDescription = localCurrencyBankDescription;
	}


	public Bank getLocalBank() {
		return localBank;
	}


	public void setLocalBank(Bank localBank) {
		this.localBank = localBank;
	}


	public String getUsdCurrencyBankDescription() {
		return usdCurrencyBankDescription;
	}


	public void setUsdCurrencyBankDescription(String usdCurrencyBankDescription) {
		this.usdCurrencyBankDescription = usdCurrencyBankDescription;
	}


	public Bank getUsdBank() {
		return usdBank;
	}


	public void setUsdBank(Bank usdBank) {
		this.usdBank = usdBank;
	}


	public String getLocalCurrencyBankAccountTypeDescription() {
		return localCurrencyBankAccountTypeDescription;
	}


	public void setLocalCurrencyBankAccountTypeDescription(String localCurrencyBankAccountTypeDescription) {
		this.localCurrencyBankAccountTypeDescription = localCurrencyBankAccountTypeDescription;
	}


	public Integer getLocalCurrencyBankAccountType() {
		return localCurrencyBankAccountType;
	}


	public void setLocalCurrencyBankAccountType(Integer localCurrencyBankAccountType) {
		this.localCurrencyBankAccountType = localCurrencyBankAccountType;
	}


	public String getUsdCurrencyBankAccountTypeDescription() {
		return usdCurrencyBankAccountTypeDescription;
	}


	public void setUsdCurrencyBankAccountTypeDescription(String usdCurrencyBankAccountTypeDescription) {
		this.usdCurrencyBankAccountTypeDescription = usdCurrencyBankAccountTypeDescription;
	}


	public Integer getUsdCurrencyBankAccountType() {
		return usdCurrencyBankAccountType;
	}


	public void setUsdCurrencyBankAccountType(Integer usdCurrencyBankAccountType) {
		this.usdCurrencyBankAccountType = usdCurrencyBankAccountType;
	}


	public String getLocalCurrencyBankAccountNumber() {
		return localCurrencyBankAccountNumber;
	}


	public void setLocalCurrencyBankAccountNumber(String localCurrencyBankAccountNumber) {
		this.localCurrencyBankAccountNumber = localCurrencyBankAccountNumber;
	}


	public String getUsdCurrencyBankAccountNumber() {
		return usdCurrencyBankAccountNumber;
	}


	public void setUsdCurrencyBankAccountNumber(String usdCurrencyBankAccountNumber) {
		this.usdCurrencyBankAccountNumber = usdCurrencyBankAccountNumber;
	}


	public String getStockCodeDescription() {
		return stockCodeDescription;
	}


	public void setStockCodeDescription(String stockCodeDescription) {
		this.stockCodeDescription = stockCodeDescription;
	}


	public Integer getStockCode() {
		return stockCode;
	}


	public void setStockCode(Integer stockCode) {
		this.stockCode = stockCode;
	}


	public Integer getRowNumber() {
		return rowNumber;
	}


	public void setRowNumber(Integer rowNumber) {
		this.rowNumber = rowNumber;
	}


	public String getRepresentativeName() {
		return representativeName;
	}


	public void setRepresentativeName(String representativeName) {
		this.representativeName = representativeName;
	}


	public String getRepresentativeFirstLastName() {
		return representativeFirstLastName;
	}


	public void setRepresentativeFirstLastName(String representativeFirstLastName) {
		this.representativeFirstLastName = representativeFirstLastName;
	}


	public String getRepresentativeSecondLastName() {
		return representativeSecondLastName;
	}


	public void setRepresentativeSecondLastName(String representativeSecondLastName) {
		this.representativeSecondLastName = representativeSecondLastName;
	}


	public String getRepresentativeFullName() {
		return representativeFullName;
	}


	public void setRepresentativeFullName(String representativeFullName) {
		this.representativeFullName = representativeFullName;
	}


	public String getRepresentativeEmail() {
		return representativeEmail;
	}


	public void setRepresentativeEmail(String representativeEmail) {
		this.representativeEmail = representativeEmail;
	}


	public String getRepresentativeNationalityCountryDescription() {
		return representativeNationalityCountryDescription;
	}


	public void setRepresentativeNationalityCountryDescription(String representativeNationalityCountryDescription) {
		this.representativeNationalityCountryDescription = representativeNationalityCountryDescription;
	}


	public Integer getRepresentativeNationality() {
		return representativeNationality;
	}


	public void setRepresentativeNationality(Integer representativeNationality) {
		this.representativeNationality = representativeNationality;
	}


	public String getRepresentativeIndResidentDescription() {
		return representativeIndResidentDescription;
	}


	public void setRepresentativeIndResidentDescription(String representativeIndResidentDescription) {
		this.representativeIndResidentDescription = representativeIndResidentDescription;
	}


	public Integer getRepresentativeIndResident() {
		return representativeIndResident;
	}


	public void setRepresentativeIndResident(Integer representativeIndResident) {
		this.representativeIndResident = representativeIndResident;
	}


	public String getRepresentativeLegalCountryDescription() {
		return representativeLegalCountryDescription;
	}


	public void setRepresentativeLegalCountryDescription(String representativeLegalCountryDescription) {
		this.representativeLegalCountryDescription = representativeLegalCountryDescription;
	}


	public Integer getRepresentativeLegalCountry() {
		return representativeLegalCountry;
	}


	public void setRepresentativeLegalCountry(Integer representativeLegalCountry) {
		this.representativeLegalCountry = representativeLegalCountry;
	}


	public String getRepresentativeLegalDepartmentDescription() {
		return representativeLegalDepartmentDescription;
	}


	public void setRepresentativeLegalDepartmentDescription(String representativeLegalDepartmentDescription) {
		this.representativeLegalDepartmentDescription = representativeLegalDepartmentDescription;
	}


	public Integer getRepresentativeLegalDepartment() {
		return representativeLegalDepartment;
	}


	public void setRepresentativeLegalDepartment(Integer representativeLegalDepartment) {
		this.representativeLegalDepartment = representativeLegalDepartment;
	}


	public String getRepresentativeLegalProvinceDescription() {
		return representativeLegalProvinceDescription;
	}


	public void setRepresentativeLegalProvinceDescription(String representativeLegalProvinceDescription) {
		this.representativeLegalProvinceDescription = representativeLegalProvinceDescription;
	}


	public Integer getRepresentativeLegalProvince() {
		return representativeLegalProvince;
	}


	public void setRepresentativeLegalProvince(Integer representativeLegalProvince) {
		this.representativeLegalProvince = representativeLegalProvince;
	}


	public String getRepresentativeAddress() {
		return representativeAddress;
	}


	public void setRepresentativeAddress(String representativeAddress) {
		this.representativeAddress = representativeAddress;
	}


	public String getRepresentativeTypeDescription() {
		return representativeTypeDescription;
	}


	public void setRepresentativeTypeDescription(String representativeTypeDescription) {
		this.representativeTypeDescription = representativeTypeDescription;
	}


	public Integer getRepresentativeType() {
		return representativeType;
	}


	public void setRepresentativeType(Integer representativeType) {
		this.representativeType = representativeType;
	}



	public String getRepresentativeDocumentTypeDescription() {
		return representativeDocumentTypeDescription;
	}


	public void setRepresentativeDocumentTypeDescription(String representativeDocumentTypeDescription) {
		this.representativeDocumentTypeDescription = representativeDocumentTypeDescription;
	}


	public Integer getRepresentativeTypeDocument() {
		return representativeTypeDocument;
	}


	public void setRepresentativeTypeDocument(Integer representativeTypeDocument) {
		this.representativeTypeDocument = representativeTypeDocument;
	}


	public String getRepresentativeDocumentNumber() {
		return representativeDocumentNumber;
	}


	public void setRepresentativeDocumentNumber(String representativeDocumentNumber) {
		this.representativeDocumentNumber = representativeDocumentNumber;
	}


	public String getRepresentativeSexDescription() {
		return representativeSexDescription;
	}


	public void setRepresentativeSexDescription(String representativeSexDescription) {
		this.representativeSexDescription = representativeSexDescription;
	}


	public Integer getRepresentativeSex() {
		return representativeSex;
	}


	public void setRepresentativeSex(Integer representativeSex) {
		this.representativeSex = representativeSex;
	}


	public String getRepresentativeClassDescription() {
		return representativeClassDescription;
	}


	public void setRepresentativeClassDescription(String representativeClassDescription) {
		this.representativeClassDescription = representativeClassDescription;
	}


	public Integer getRepresentativeClass() {
		return representativeClass;
	}


	public void setRepresentativeClass(Integer representativeClass) {
		this.representativeClass = representativeClass;
	}


	public String getRepresentativeBirthDateString() {
		return representativeBirthDateString;
	}


	public void setRepresentativeBirthDateString(String representativeBirthDateString) {
		this.representativeBirthDateString = representativeBirthDateString;
	}


	public Date getRepresentativeBirthDate() {
		return representativeBirthDate;
	}


	public void setRepresentativeBirthDate(Date representativeBirthDate) {
		this.representativeBirthDate = representativeBirthDate;
	}


	public String getRepresentativeLegalCountryCode() {
		return representativeLegalCountryCode;
	}


	public void setRepresentativeLegalCountryCode(String representativeLegalCountryCode) {
		this.representativeLegalCountryCode = representativeLegalCountryCode;
	}


	public String getRepresentativeNationalityCode() {
		return representativeNationalityCode;
	}


	public void setRepresentativeNationalityCode(String representativeNationalityCode) {
		this.representativeNationalityCode = representativeNationalityCode;
	}


	public Boolean getMultipleHolder() {
		return multipleHolder;
	}


	public void setMultipleHolder(Boolean multipleHolder) {
		this.multipleHolder = multipleHolder;
	}


	public String getEconomicSectorDescription() {
		return economicSectorDescription;
	}


	public void setEconomicSectorDescription(String economicSectorDescription) {
		this.economicSectorDescription = economicSectorDescription;
	}


	public Integer getEconomicSector() {
		return economicSector;
	}


	public void setEconomicSector(Integer economicSector) {
		this.economicSector = economicSector;
	}


	public String getEconomicActivityDescription() {
		return economicActivityDescription;
	}


	public void setEconomicActivityDescription(String economicActivityDescription) {
		this.economicActivityDescription = economicActivityDescription;
	}


	public Integer getEconomicActivity() {
		return economicActivity;
	}


	public void setEconomicActivity(Integer economicActivity) {
		this.economicActivity = economicActivity;
	}

	public String getRelatedName() {
		return relatedName;
	}

	public void setRelatedName(String relatedName) {
		this.relatedName = relatedName;
	}

	public String getIndPddDescription() {
		return indPddDescription;
	}

	public void setIndPddDescription(String indPddDescription) {
		this.indPddDescription = indPddDescription;
	}

	public Integer getIndPdd() {
		return indPdd;
	}

	public void setIndPdd(Integer indPdd) {
		this.indPdd = indPdd;
	}

	public String getJuridicClassDescription() {
		return juridicClassDescription;
	}

	public void setJuridicClassDescription(String juridicClassDescription) {
		this.juridicClassDescription = juridicClassDescription;
	}

	public Integer getJuridicClass() {
		return juridicClass;
	}

	public void setJuridicClass(Integer juridicClass) {
		this.juridicClass = juridicClass;
	}

	public String getIndPepDescription() {
		return indPepDescription;
	}

	public void setIndPepDescription(String indPepDescription) {
		this.indPepDescription = indPepDescription;
	}

	public Integer getIndPep() {
		return indPep;
	}

	public void setIndPep(Integer indPep) {
		this.indPep = indPep;
	}

	public String getPepMotiveDescription() {
		return pepMotiveDescription;
	}

	public void setPepMotiveDescription(String pepMotiveDescription) {
		this.pepMotiveDescription = pepMotiveDescription;
	}

	public Integer getPepMotive() {
		return pepMotive;
	}

	public void setPepMotive(Integer pepMotive) {
		this.pepMotive = pepMotive;
	}

	public String getPepRoleDescription() {
		return pepRoleDescription;
	}

	public void setPepRoleDescription(String pepRoleDescription) {
		this.pepRoleDescription = pepRoleDescription;
	}

	public Integer getPepRole() {
		return pepRole;
	}

	public void setPepRole(Integer pepRole) {
		this.pepRole = pepRole;
	}

	public Integer getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(Integer mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPepGradeDescription() {
		return pepGradeDescription;
	}

	public void setPepGradeDescription(String pepGradeDescription) {
		this.pepGradeDescription = pepGradeDescription;
	}

	public Integer getPepGrade() {
		return pepGrade;
	}

	public void setPepGrade(Integer pepGrade) {
		this.pepGrade = pepGrade;
	}

	public String getPepName() {
		return pepName;
	}

	public void setPepName(String pepName) {
		this.pepName = pepName;
	}

	public Date getPepBeginningPeriod() {
		return pepBeginningPeriod;
	}

	public void setPepBeginningPeriod(Date pepBeginningPeriod) {
		this.pepBeginningPeriod = pepBeginningPeriod;
	}

	public Date getPepEndingPeriod() {
		return pepEndingPeriod;
	}

	public void setPepEndingPeriod(Date pepEndingPeriod) {
		this.pepEndingPeriod = pepEndingPeriod;
	}

}
