package com.pradera.accounts.holder.massive.service;

import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;

import org.apache.commons.io.FileUtils;

import com.pradera.accounts.holder.massive.facade.HolderMassiveServiceFacade;
import com.pradera.commons.massive.type.HolderMassiveFileProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.GeographicLocationType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.negotiation.constant.NegotiationConstant;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class McnMassiveRegistrationBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@BatchProcess(name="HolderMassiveRegistrationBatch")
@RequestScoped
public class HolderMassiveRegistrationBatch implements Serializable,JobExecution {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The notification service facade. */
	@EJB 
	NotificationServiceFacade notificationServiceFacade;
	
	@EJB 
	HolderMassiveServiceFacade holderMassiveServiceFacade;
	
	@EJB
	GeneralParametersFacade generalParametersFacade;

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		
		System.out.println("************************************* BATCH SERVICE WAS CALLED ******************************************************");
		
		String mappingFile=null;
		String filePath=null;
		Long participantId = null;
		String locale=null;
		String fileName=null;
						
		for(ProcessLoggerDetail detail : processLogger.getProcessLoggerDetails()){
			if(detail.getParameterName().equals(GeneralConstants.PARAMETER_FILE)){
				filePath = detail.getParameterValue();
			}else if(detail.getParameterName().equals(GeneralConstants.PARAMETER_STREAM)){
				mappingFile = detail.getParameterValue();
			}else if(detail.getParameterName().equals(GeneralConstants.PARAMETER_LOCALE)){
				locale = detail.getParameterValue();
			}else if(detail.getParameterName().equals(GeneralConstants.PARAMETER_FILE_NAME)){
				fileName = detail.getParameterValue();
			}else if(detail.getParameterName().equals(GeneralConstants.PARTICIPANT_CODE)){
				participantId = Long.parseLong(detail.getParameterValue());
			}
		}
		
		try {
			
			List<ParameterTable> lstParameter = new ArrayList<ParameterTable>();
			
			List<ParameterTable> newParamLst = new ArrayList<ParameterTable>();			
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.PERSON_TYPE.getCode());
			
			newParamLst = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			lstParameter.addAll(newParamLst);

			parameterTableTO.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
			newParamLst = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			lstParameter.addAll(newParamLst);
			
			parameterTableTO.setMasterTableFk(MasterTableType.ACCOUNTS_TYPE.getCode());
			newParamLst = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			lstParameter.addAll(newParamLst);
			
			parameterTableTO.setMasterTableFk(MasterTableType.SEX.getCode());
			newParamLst = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			lstParameter.addAll(newParamLst);
			
			parameterTableTO.setMasterTableFk(MasterTableType.MASTER_TABLE_BANK_ACCOUNT_TYPE.getCode());
			newParamLst = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			lstParameter.addAll(newParamLst);
			
			parameterTableTO.setMasterTableFk(MasterTableType.ECONOMIC_ACTIVITY.getCode());
			newParamLst = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			lstParameter.addAll(newParamLst);
			
			parameterTableTO.setMasterTableFk(MasterTableType.ECONOMIC_SECTOR.getCode());
			newParamLst = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			lstParameter.addAll(newParamLst);
			
			parameterTableTO.setMasterTableFk(MasterTableType.T_CLASE_JURIDICO_TITULAR.getCode());
			newParamLst = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			lstParameter.addAll(newParamLst);
			
			parameterTableTO.setMasterTableFk(MasterTableType.ASSET_LAUNDRY_MOTIVE_PEP.getCode());
			newParamLst = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			lstParameter.addAll(newParamLst);	
			
			parameterTableTO.setMasterTableFk(MasterTableType.PEP_CHARGE.getCode());
			newParamLst = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			lstParameter.addAll(newParamLst);
			
			parameterTableTO.setMasterTableFk(MasterTableType.CLASS_REPRENTATIVE.getCode());
			newParamLst = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			lstParameter.addAll(newParamLst);
			
			parameterTableTO.setMasterTableFk(MasterTableType.PEP_GRADE.getCode());
			newParamLst = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			lstParameter.addAll(newParamLst);
			
			parameterTableTO.setMasterTableFk(MasterTableType.ASSET_LAUNDRY_MOTIVE_PEP.getCode());
			newParamLst = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			lstParameter.addAll(newParamLst);
			
			GeographicLocationTO filterG = new GeographicLocationTO();
			
			Map<String, GeographicLocation> nationality = new HashMap<String, GeographicLocation>();
			
			filterG.setGeographicLocationType(GeographicLocationType.COUNTRY.getCode());
			for(GeographicLocation param : generalParametersFacade.getListGeographicLocationServiceFacade(filterG)) {
				nationality.put(param.getName(), param);
			}			
			
			ProcessFileTO processFileTO = new ProcessFileTO();
			processFileTO.setStreamFileDir(mappingFile);
			processFileTO.setTempProcessFile(new File(filePath));
			processFileTO.setProcessFile(FileUtils.readFileToByteArray(processFileTO.getTempProcessFile()));
			processFileTO.setLocale(new Locale(locale));
			processFileTO.setFileName(fileName);
			processFileTO.setIndAutomaticProcess(GeneralConstants.TWO_VALUE_INTEGER);
			processFileTO.setIdParticipantPk(participantId);
			processFileTO.setInterfaceName(ComponentConstant.INTERFACE_BBV_OPERATIONS);
			processFileTO.setProcessType(HolderMassiveFileProcessType.HOLDER_MASSIVE_UPLOAD.getCode());
			processFileTO.setRootTag(NegotiationConstant.MASSIVE_XML_BBV_ROOT_TAG);
			processFileTO.setIdParticipantPk(participantId);
			processFileTO.setDpfInterface(Boolean.TRUE);
			
			CommonsUtilities.validateFileOperationStruct(processFileTO);
				
			Object[] operationsProcess = holderMassiveServiceFacade.saveHolderRequestMassively(processFileTO, lstParameter, nationality);

			Integer count = new Integer(0);
			String messageDuplicated= null;
			
			BigDecimal operationTotal = new BigDecimal(0);
			BigDecimal operationAccepted = new BigDecimal(0);
			BigDecimal operationRejected = new BigDecimal(0);
			
			for (Object operationsProcessValue :operationsProcess) {
				count=count+1;
				if(count==1){
					operationTotal=(BigDecimal) operationsProcessValue;
				}
				if(count==2){
					operationAccepted=(BigDecimal) operationsProcessValue;
				}
				if(count==3){
					operationRejected=(BigDecimal) operationsProcessValue;
				}
				if(count==4){
					messageDuplicated=(String) operationsProcessValue;
				}
			}
			
			Object[] operationsProcessA = {operationTotal,operationAccepted,operationRejected};
			
			/* Notifications, Issue 2829 PERU MANTIS*/
			//mcnOperationServiceFacade.notificationConcatenation(processLogger);
					
			sendNotificationProcess(processLogger, operationsProcessA);
				
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * Send notification process.
	 *
	 * @param processLogger the process logger
	 * @param operationsProcess the operations process
	 */
	public void sendNotificationProcess(ProcessLogger processLogger, Object[] operationsProcess) {		
		notificationServiceFacade.sendNotification(processLogger.getIdUserAccount(), processLogger.getBusinessProcess(), null, operationsProcess);				
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}

}
