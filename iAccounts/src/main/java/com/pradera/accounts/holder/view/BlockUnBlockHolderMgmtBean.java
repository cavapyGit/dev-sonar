package com.pradera.accounts.holder.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.apache.deltaspike.core.api.resourceloader.InjectableResource;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.event.FileUploadEvent;

import com.pradera.accounts.holder.facade.HolderServiceFacade;
import com.pradera.accounts.holder.to.HolderRequestTO;
import com.pradera.accounts.util.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.utils.DocumentTO;
import com.pradera.core.component.utils.DocumentValidator;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.HolderFile;
import com.pradera.model.accounts.HolderHistoryState;
import com.pradera.model.accounts.HolderReqFileHistory;
import com.pradera.model.accounts.HolderRequest;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequesterType;
import com.pradera.model.accounts.type.AnnularMotivesHolderRequestType;
import com.pradera.model.accounts.type.HolderBlockMotiveType;
import com.pradera.model.accounts.type.HolderBlockUnBlockActionsType;
import com.pradera.model.accounts.type.HolderReqFileHisType;
import com.pradera.model.accounts.type.HolderRequestStateType;
import com.pradera.model.accounts.type.HolderRequestType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.HolderUnBlockMotiveType;
import com.pradera.model.accounts.type.ParticipantFileType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.accounts.type.RejectMotivesHolderRequestType;
import com.pradera.model.accounts.type.StateHistoryStateHolderType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.process.BusinessProcess;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BlockUnBlockHolderMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class BlockUnBlockHolderMgmtBean extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The log. */
	Logger log = Logger.getLogger(BlockUnBlockHolderMgmtBean.class.getName());

	/** The general pameters facade. */
	@EJB
	GeneralParametersFacade generalPametersFacade;

	/** The holder service facade. */
	@EJB
	HolderServiceFacade holderServiceFacade;

	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
	/** The holder file name display. */
	private String holderFileNameDisplay;
	
	/** The lst holderreq file sustent. */
	private List<HolderReqFileHistory> lstHolderFileSustent;
	
	/** The holder request. */
	private HolderRequest holderRequest;
	
	/** The holder request selected. */
	private HolderRequest holderRequestSelected;
	
	/** The holder filter. */
	private HolderRequestTO holderFilter;	

	/** The show panel options component. */
	private boolean showPanelOptionsComponent;
	
	/** The list participant. */
	private List<Participant> listParticipant;
	
	/** The list person type. */
	private List<ParameterTable> listPersonType;
	
    /** The list actions type. */
    private List<ParameterTable> listActionsType;
    
    /** The list motives. */
    private List<ParameterTable> listMotives;
	
	/** The holder. */
	private Holder holder;
	
	/** The holder history state. */
	private HolderHistoryState holderHistoryState;
	
	
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	private UserPrivilege userPrivilege;
	
	/** The disabled cmb participant. */
	private boolean disabledCmbParticipant;
	
	/** The flag is participant. */
	private boolean flagIsParticipant;
	
	/** The list state block un block holder request. */
	private List<ParameterTable> listStateBlockUnBlockHolderRequest;
	
	/** The holder request data model. */
	private HolderRequestDataModel holderRequestDataModel;
	
	/** The participant. */
	private Participant participant;
	
	/** The flag holder natural person. */
	private boolean flagHolderNaturalPerson;
	
	/** The lst block un block request history. */
	private HolderRequestDataModel lstBlockUnBlockRequestHistory;
	
	/** The action type. */
	private Integer actionType;
	
	/** The motive type. */
	private Integer motiveType;
	
	/** The flag exits information. */
	private boolean flagExitsInformation;
	
	/** The request other motive. */
	private String requestOtherMotive;

	/** The show motive text. */
	private boolean showMotiveText;
	
	/** The flag action annular. */
	private boolean flagActionAnnular;
	
	/** The flag action confirm. */
	private boolean flagActionConfirm;
	
	/** The flag action reject. */
	private boolean flagActionReject;
	
	/** The flag action approve. */
	private boolean flagActionApprove;
	
    /** The request motive. */
    private Integer requestMotive;
    
    /** The requester participant. */
    private boolean requesterParticipant;
	
    /** The requester type. */
    private Integer requesterType;
	
    /** The list requester type. */
    private List<ParameterTable> listRequesterType;
    
    /** The person natural. */
    private boolean personNatural;
    
    /** The person juridic. */
    private boolean personJuridic;
    
    /** The notification service facade. */
    @EJB NotificationServiceFacade notificationServiceFacade;	
	
	/** The message configuration. */
	@Inject
	@InjectableResource(location = "Messages_es.properties")
	private Properties messageConfiguration;
    
    /** The business process. */
    BusinessProcess businessProcess = new BusinessProcess();
    
    /** The numeric type document holder. */
    private boolean numericTypeDocumentHolder;
	
	/** The max lenght document number holder. */
	private Integer maxLenghtDocumentNumberHolder;
	
	/** The disabled document number. */
	private boolean disabledDocumentNumber;
	
	/** The document validator. */
	@Inject
    DocumentValidator documentValidator;
	
	/** The list document type. */
	private List<ParameterTable> listDocumentType;
	/** The request state description. */
    private Map<Integer,String> requestStateDescription = null;
	
    /** The document type description. */
    private Map<Integer,String> documentTypeDescription = null;
	


	
	/**
	    * Instantiates a new block un block holder mgmt bean.
	    */
	   public BlockUnBlockHolderMgmtBean() {
		holderFilter = new HolderRequestTO();
		holderRequest = new HolderRequest();
		holderHistoryState = new HolderHistoryState();
		holder = new Holder();
		requestStateDescription = new HashMap<Integer,String>();
		documentTypeDescription = new HashMap<Integer,String>();
	
	}

	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {

		try{
			
		
		if (!FacesContext.getCurrentInstance().isPostback()) {
			holderFilter.setInitialDate(CommonsUtilities.currentDate());
			holderFilter.setEndDate(CommonsUtilities.currentDate());
			getLstParticipant();
			getLstStateBLockUnBlockHolderRequestType();
			getLstRequesterType();
			getLstDocumentType();
			getLstActionsType();
			
			if (validateIsParticipant()) {
				disabledCmbParticipant = true;
				requesterParticipant=true;
				holderFilter.setRequesterType(HolderAccountRequesterType.PARTICIPANT.getCode());
				holderFilter.setParticipantSelected(userInfo.getUserAccountSession().getParticipantCode());
				Participant filter = new Participant();
				filter.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
				listParticipant = accountsFacade.getLisParticipantServiceBean(filter);
			
			} else {
				disabledCmbParticipant = false;
				requesterParticipant=false;
				holderFilter.setRequesterType(null);
				holderFilter.setParticipantSelected(null);
			}
			
			ParameterTableTO  parameterTableTO = new ParameterTableTO();
			
			parameterTableTO.setMasterTableFk(MasterTableType.STATE_CREATION_HOLDER_REQUEST.getCode());
			for(ParameterTable param : generalPametersFacade.getListParameterTableServiceBean(parameterTableTO)){
				requestStateDescription.put(param.getParameterTablePk(), param.getParameterName());
			}
			
			parameterTableTO.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
			for(ParameterTable param : generalPametersFacade.getListParameterTableServiceBean(parameterTableTO)) {
				documentTypeDescription.put(param.getParameterTablePk(), param.getParameterName());
			}
		}
		
		}
		catch(Exception e){
			e.printStackTrace();
		}

	}
	
	/**
	 * Metodo que valida Tipo de docuemento del Titular.
	 */
	public void validateTypeDocumentHolder(){
		
		if(holderFilter.getDocumentType()==null){
			holderFilter.setDocumentNumber(null);
			numericTypeDocumentHolder = true;
			disabledDocumentNumber = true;
			return;
		}
		DocumentTO documentTo = documentValidator.loadMaxLenghtType(holderFilter.getDocumentType());			
		numericTypeDocumentHolder = documentTo.isNumericTypeDocument();
		maxLenghtDocumentNumberHolder = documentTo.getMaxLenghtDocumentNumber();
		disabledDocumentNumber = false;
	}
	
	/**
	 * Metodo para Obtener Lista de tipos de Documentos.
	 *
	 * @return the lst document type
	 */
	public void getLstDocumentType() {

		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON
							.getCode());

			listDocumentType = generalPametersFacade
					.getListParameterTableServiceBean(parameterTableTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}
	
	/**
	 * Requester listener.
	 *
	 * @param requesterType the requester type
	 */
	public void requesterListener(Integer requesterType){
		
		if(requesterType.equals(HolderAccountRequesterType.PARTICIPANT.getCode())){
			participant = new Participant();
			requesterParticipant = true;
			lstHolderFileSustent = new ArrayList<HolderReqFileHistory>();
		}
		else{
			requesterParticipant = false;
			participant = new Participant();
			holderFilter.setParticipantSelected(null);
			lstHolderFileSustent = new ArrayList<HolderReqFileHistory>();
			JSFUtilities.setValidViewComponentAndChildrens("frmBlockUnBlockRequestRegister");
		}
	}
	
	/**
	 * Sets the buttons listener.
	 */
	public void setButtonsListener(){
		
		if (!FacesContext.getCurrentInstance().isPostback()) {
			
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		
		
		if(!getViewOperationType().equals(ViewOperationsType.DETAIL.getCode())){
			
		
		if(holderRequest.getStateHolderRequest().equals(HolderRequestStateType.REGISTERED.getCode())){		
			
			if(getViewOperationType().equals(ViewOperationsType.APPROVE.getCode())){
			   privilegeComponent.setBtnApproveView(true);
			}
			if(getViewOperationType().equals(ViewOperationsType.ANULATE.getCode())){
			   privilegeComponent.setBtnAnnularView(true);
			}
		}
		
		else if(holderRequest.getStateHolderRequest().equals(HolderRequestStateType.APPROVED.getCode())){
			if(getViewOperationType().equals(ViewOperationsType.CONFIRM.getCode())){
			   privilegeComponent.setBtnConfirmView(true);
			}
			if(getViewOperationType().equals(ViewOperationsType.REJECT.getCode())){				
			   privilegeComponent.setBtnRejectView(true);
			}
			
		}
		
		}
		
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	 }
		
	}
	
	/**
	 * Metodo que Carga Descripcion de los Participantes
	 */
	public void loadParticipantDescription(){
		
		if(participant.getIdParticipantPk().equals(Long.valueOf(-1))){
			participant.setCompleteDescription(null);
		}
		else{
			for(int i=0;i<listParticipant.size();i++){
				if(listParticipant.get(i).getIdParticipantPk().equals(participant.getIdParticipantPk())){
				participant.setCompleteDescription(listParticipant.get(i).getIdParticipantPk() + GeneralConstants.DASH + listParticipant.get(i).getDescription());
				}
			}
		}
	}
	
	/**
	 * Clean all.
	 */
	public void cleanAll(){
		JSFUtilities.resetViewRoot();
		setItForInitial();
		holderRequestDataModel = null;
		holderFilter = new HolderRequestTO();
		holderFilter.setInitialDate(CommonsUtilities.currentDate());
		holderFilter.setEndDate(CommonsUtilities.currentDate());
        holder = new Holder();
        motiveType=null;
        actionType=null;
        participant = new Participant();
        requesterType = null;
        requesterParticipant = false; 
        holderRequestSelected = null;
        requestOtherMotive=null;
        showMotiveText = false;
        disabledDocumentNumber=true;
        lstHolderFileSustent = new ArrayList<HolderReqFileHistory>();
        holderFileNameDisplay = null;
        if (validateIsParticipant()) {
			disabledCmbParticipant = true;
			requesterParticipant=true;			
			holderFilter.setRequesterType(HolderAccountRequesterType.PARTICIPANT.getCode());
			holderFilter.setParticipantSelected(userInfo.getUserAccountSession().getParticipantCode());
		} else {
			disabledCmbParticipant = false;
			requesterParticipant = false;
			holderFilter.setRequesterType(null);
			holderFilter.setParticipantSelected(null);
		}
		if(validateIsParticipant()){
			requesterType = HolderAccountRequesterType.PARTICIPANT.getCode();
			participant.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
			disabledCmbParticipant = true;
			requesterParticipant=true;	
		}
		else{
			participant.setIdParticipantPk(null);
			requesterType=null;
			disabledCmbParticipant = false;
			requesterParticipant=false;	
		}
        
	}
	
	/**
	 * Evaluate motive annular or reject.
	 */
	public void evaluateMotiveAnnularOrReject(){
		
		if(flagActionReject){
			if(requestMotive!=null && requestMotive.equals(RejectMotivesHolderRequestType.OTHER_MOTIVES.getCode())){
				requestOtherMotive="";
				showMotiveText=true;
			}
			else{
				showMotiveText=false;
			}
		}
		else if(flagActionAnnular){
			if(requestMotive!=null && requestMotive.equals(AnnularMotivesHolderRequestType.OTHER_MOTIVES.getCode())){
				requestOtherMotive="";
				showMotiveText=true;
			}
			else{
				showMotiveText=false;
			}
		}
	}
	
	/**
	 * Evaluate motive.
	 */
	public void evaluateMotive(){
		
		if(actionType!=null && actionType.equals(HolderBlockUnBlockActionsType.BLOCK.getCode())){
			if(motiveType!=null && motiveType.equals(HolderBlockMotiveType.OTHERS_MOTIVES.getCode())){
				requestOtherMotive="";
				showMotiveText=true;
			}
			else{
				showMotiveText=false;
			}
		}		
		else if(actionType!=null && actionType.equals(HolderBlockUnBlockActionsType.UNBLOCK.getCode())){
			
			if(motiveType!=null && motiveType.equals(HolderUnBlockMotiveType.OTHERS_MOTIVES.getCode())){
				requestOtherMotive="";
				showMotiveText=true;
			}
			else{
				showMotiveText=false;
			}
		}
	}
	
	/**
	 * Metodo que Valida Relacion Entre Participante y Titular
	 *
	 * @param idParticipant the id participant
	 * @param holder the holder
	 * @return true, if successful
	 */
	public boolean validateParticipantWithHolder(Long idParticipant,Holder holder){
      boolean flag=false;
      if(!validateIsParticipant()){
    	  if(idParticipant==null && holder.getIdHolderPk()!=null){
    		  flag = true;
    		  return true;
    	  }
      }
      try{
      int count = holderServiceFacade.getCountParticipantWithAccountHolderServiceFacade(idParticipant,holder.getIdHolderPk());
         if(count>=1 || holder.getParticipant().getIdParticipantPk().equals(idParticipant)){
        	flag=true;
         }
         if(!flag){
        	Object[] bodyData = new Object[1];			
         	bodyData[0] = holder.getIdHolderPk().toString();
         	
         	if(!validateIsParticipant()){        		
         	JSFUtilities.showMessageOnDialog(
  					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
  					PropertiesUtilities.getMessage(PropertiesConstants.NO_RELATIONSHIP_BETWEEN_THE_HOLDER_AND_PARTICIPANT,bodyData));
  			}
         	else{
         		           	
         		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),	
     					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_HOLDER_REQUEST_NO_RESULT,bodyData));
     	    }
         	JSFUtilities.showSimpleValidationDialog();
         }
      }
      catch(Exception e){
    	  excepcion.fire(new ExceptionToCatchEvent(e));
      }
      return flag;
	}
	
	/**
	 * Metodo para Obtener la Informacion del Titular.
	 */
	public void holderInformation(){
		
	try{	
		if(holder.getIdHolderPk()!=null){
		holderHistoryState = new HolderHistoryState();
		motiveType=null;
		showMotiveText=false;
		requestOtherMotive="";
		actionType=null;
		lstBlockUnBlockRequestHistory=null;
		Long idHolderPk = holder.getIdHolderPk();
		holder = holderServiceFacade.getBlockUnBlockHolderRequestServiceFacade(holder);
		//Si no existe Titular
		if(holder==null){
			Object[] bodyData = new Object[1];			
			bodyData[0]=idHolderPk.toString();			
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),	
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_HOLDER_REQUEST_NO_RESULT,bodyData));
			JSFUtilities.showSimpleValidationDialog();
			setItForInitial();
			holder = new Holder();
		}
		else{
					Long idParticipant = null;
					if(participant!=null){
						if(participant.getIdParticipantPk()!=null){
							idParticipant = participant.getIdParticipantPk();
						}
					}
					if(requesterType!=null && requesterType.equals(HolderAccountRequesterType.PARTICIPANT.getCode())){
						if(!validateParticipantWithHolder(idParticipant,holder)){
							holderHistoryState = new HolderHistoryState();
							motiveType=null;
							showMotiveText=false;
							requestOtherMotive="";
							actionType=null;
							lstBlockUnBlockRequestHistory=null;
							holder = new Holder();
							return;
						}
					}
					HolderRequest objHolderRequest = new HolderRequest();
					objHolderRequest.setHolder(holder);
					List<HolderRequest> lstHolderRequest = holderServiceFacade.getHistorialStateList(objHolderRequest);
					if(Validations.validateListIsNotNullAndNotEmpty(lstHolderRequest)){
						lstBlockUnBlockRequestHistory = new HolderRequestDataModel(lstHolderRequest);
					}
				//Titular en estado Registrado
				if(holder.getStateHolder()!=null && holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){		
					if(!validateRegistrationRequest()){
						getLstPersonType();
						getLstActionsType();
						if(holder.getHolderType().equals(PersonType.NATURAL.getCode())){
							personNatural = true;
							personJuridic = false;
						}
						else if(PersonType.JURIDIC.getCode().equals(PersonType.JURIDIC.getCode())){
							personNatural = false;
							personJuridic = true;
						}
						getLstStateBLockUnBlockHolderRequestType();
						actionType=HolderBlockUnBlockActionsType.BLOCK.getCode();
						getLstMotives(MasterTableType.HOLDER_BLOCK_MOTIVE);
					}					
					else{
						return;
					}
				}
				//Si Titular esta Bloqueado
				else if(holder.getStateHolder()!=null && holder.getStateHolder().equals(HolderStateType.BLOCKED.getCode())){
				if(!validateRegistrationRequest()){
					getLstPersonType();
					getLstActionsType();
					if(holder.getHolderType().equals(PersonType.NATURAL.getCode())){
						  personNatural = true;
						  personJuridic = false;
						}
					else if(PersonType.JURIDIC.getCode().equals(PersonType.JURIDIC.getCode())){						
						  personNatural = false;
						  personJuridic = true;
					}
					getLstStateBLockUnBlockHolderRequestType();
					actionType=HolderBlockUnBlockActionsType.UNBLOCK.getCode();
					getLstMotives(MasterTableType.HOLDER_UNBLOCK_MOTIVE);
				}
				else{
					return;
				}
			}		
			else{
			
		    Object[] bodyData = new Object[1];			
			bodyData[0]=holder.getIdHolderPk().toString();
			
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),	
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_DISTINCT_REGISTERED_BLOCKED,bodyData));
			JSFUtilities.showSimpleValidationDialog();
			setItForInitial();	
			holder = new Holder();
			}
		  }
		
		 }
		
		
	   }
	   catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
	   }
		
	}
	
	/**
	 * Sets the it for initial.
	 */
	public void setItForInitial(){
		participant = new Participant();
		actionType=null;
		motiveType=null;
		lstBlockUnBlockRequestHistory=null;
		
	}
	
	/**
	 * Metodo que valida el Registro.
	 *
	 * @return true, if successful
	 */
	public boolean validateRegistrationRequest(){
		
		boolean flag=false;
		List<HolderRequest> lstTempHolderRequest = null;
		try{
		
		HolderRequest holderRequestObj = new HolderRequest();
		holderRequestObj.setHolder(holder);		
		holderRequestObj.setStateHolderRequest(HolderRequestStateType.REGISTERED.getCode());		
		int indicator=0;
		
		if((holderServiceFacade.getHolderRequestServiceFacade(holderRequestObj))>0){
			setItForInitial();
			lstTempHolderRequest = holderServiceFacade.getListHolderBlockUnBlockRequestServiceFacade(holderRequestObj);
		    lstBlockUnBlockRequestHistory	= new HolderRequestDataModel(lstTempHolderRequest);
		    if(lstBlockUnBlockRequestHistory.getRowCount()>0){
		    indicator = 0;
		    for(int i=0;i<lstBlockUnBlockRequestHistory.getRowCount();i++){
		    	if(lstBlockUnBlockRequestHistory.getRowData().getRequestType().equals(HolderRequestType.BLOCK.getCode())){
		    		indicator=1;
		    	}
		    	if(lstBlockUnBlockRequestHistory.getRowData().getRequestType().equals(HolderRequestType.UNBLOCK.getCode())){
		    		indicator=2;
		    	}
		    }
		    }    
		 }
		holderRequestObj.setStateHolderRequest(HolderRequestStateType.APPROVED.getCode());
		if((holderServiceFacade.getHolderRequestServiceFacade(holderRequestObj))>0){
		setItForInitial();
				
		lstTempHolderRequest = holderServiceFacade.getListHolderBlockUnBlockRequestServiceFacade(holderRequestObj);
		lstBlockUnBlockRequestHistory	= new HolderRequestDataModel(lstTempHolderRequest);
		 if(lstBlockUnBlockRequestHistory.getRowCount()>0){
		    indicator = 0;
		    for(int i=0;i<lstBlockUnBlockRequestHistory.getRowCount();i++){
		    	if(lstBlockUnBlockRequestHistory.getRowData().getRequestType().equals(HolderRequestType.BLOCK.getCode())){
		    		indicator=1;
		    	}
		    	if(lstBlockUnBlockRequestHistory.getRowData().getRequestType().equals(HolderRequestType.UNBLOCK.getCode())){
		    		indicator=2;
		    	}
		    }
		 }
		}
	    CommandButton btnSave = (CommandButton) FacesContext.getCurrentInstance().getViewRoot().findComponent("frmBlockUnBlockRequestRegister:buttonSave");
		    if(indicator>0){
		    	flag=true;
		    	Object[] bodyData = new Object[1];			
		    	if(holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
			    	actionType=HolderBlockUnBlockActionsType.BLOCK.getCode();
			    }
			    else if(holder.getStateHolder().equals(HolderStateType.BLOCKED.getCode())){
			    	actionType=HolderBlockUnBlockActionsType.UNBLOCK.getCode();
			    }
		    	bodyData[0] = HolderBlockUnBlockActionsType.get(actionType).getValue().toLowerCase().toString();
		    	
				if(indicator==1){
				JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,	PropertiesConstants.WARNING_THERE_IS_ALREADY_BLOCK_REQUEST,bodyData);
				holder = new Holder();
				actionType=null;
				}
				if(indicator==2){
				JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,	PropertiesConstants.WARNING_THERE_IS_ALREADY_UNBLOCK_REQUEST,bodyData);
				holder = new Holder();
				actionType=null;
				}
				JSFUtilities.showSimpleValidationDialog();		  	
			  
				btnSave.setDisabled(true);
		    }else{
		    	btnSave.setDisabled(false);
		    	flag=false;
		    }
		    
		    //issue 605: validando que tampoco exista una solicutd de anulacion de CUI, caso contrario lanzar mensaje
		    if(Validations.validateIsNotNull(holder)
		    		&& Validations.validateIsNotNull(holder.getIdHolderPk()) ){
		    	if(holderServiceFacade.thisHolderExistInRequestAnnulation(holder.getIdHolderPk())){
		    		JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,	PropertiesConstants.MSG_ERROR_ANNULATECUI_CUI_IS_REQUESTANNULATION,null);
					holder = new Holder();
					actionType=null;
					JSFUtilities.showSimpleValidationDialog();
		    		btnSave.setDisabled(true);
		    		flag=true;	
		    	}
		    }
		}
		catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		if(Validations.validateListIsNotNullAndNotEmpty(lstTempHolderRequest)){
			lstBlockUnBlockRequestHistory = null;			
		}		
		return flag;
	}
	/**
	 * Load registration page.
	 */
	public void loadRegistrationPage() {
		try{
		setViewOperationType(ViewOperationsType.REGISTER.getCode()); 
		participant = new Participant();
		holder = new Holder();
		holderRequest = new HolderRequest();
		holderHistoryState = new HolderHistoryState();
		lstBlockUnBlockRequestHistory = null;
		getLstRequesterType();
		holder = new Holder();		
		motiveType=null;
		showMotiveText=false;
		requestOtherMotive="";
		actionType=null;
		requesterParticipant = false;
		requesterType = null;
		lstHolderFileSustent = new ArrayList<HolderReqFileHistory>();
        holderFileNameDisplay = null;
		
		if(validateIsParticipant()){
			requesterType = HolderAccountRequesterType.PARTICIPANT.getCode();
			participant.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
			disabledCmbParticipant = true;
			requesterParticipant=true;
			Participant filter = new Participant();
			filter.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
			listParticipant = accountsFacade.getLisParticipantServiceBean(filter);
		}
		else{
			requesterType = null;
			participant.setIdParticipantPk(null);
			disabledCmbParticipant = false;
			requesterParticipant=false;
		}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Loading request information.
	 *
	 * @param event the event
	 */
	public void loadingRequestInformation(ActionEvent event){
		
	participant = new Participant();
	holderRequest = new HolderRequest();
	holder = new Holder();
	getLstParticipant();
	getLstRequesterType();
	HolderRequest objHolderRequest;
	if(event!=null){
	setViewOperationType(ViewOperationsType.DETAIL.getCode());
	objHolderRequest = 	(HolderRequest)event.getComponent().getAttributes().get("blockUnBlockInformation");
	Long idHolderPkAux = objHolderRequest.getHolder().getIdHolderPk();
    
    try {
        holder = holderServiceFacade.getHolderDataServiceFacade(idHolderPkAux);
    } catch (ServiceException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }
    
    try {
		holderRequest= holderServiceFacade.getHolderInformationServiceFacade(objHolderRequest.getIdHolderRequestPk());
	} catch (ServiceException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    holderRequest.setStateDescription(objHolderRequest.getStateDescription());
	}
	else{
		try {
			holderRequest= holderServiceFacade.getHolderInformationServiceFacade(holderRequestSelected.getIdHolderRequestPk());
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
	        holder = holderServiceFacade.getHolderDataServiceFacade(holderRequest.getHolder().getIdHolderPk());
	    } catch (ServiceException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    }
		holderRequest.setStateDescription(holderRequestSelected.getStateDescription());
	}	
	if(holderRequest.getRequesterType()!=null){
	requesterType = holderRequest.getRequesterType();
	}
	if(holderRequest.getParticipant()!=null){
	requesterParticipant = true;	
	participant.setIdParticipantPk(holderRequest.getParticipant().getIdParticipantPk());
	participant.setCompleteDescription(holderRequest.getParticipant().getIdParticipantPk()+GeneralConstants.BLANK_SPACE+holderRequest.getParticipant().getDescription());
	}
	else{
	 requesterParticipant = false;	
	}
	holderRequest.setHolder(holder);
	getLstPersonType();
	holder.setHolderType(holderRequest.getHolder().getHolderType());
	if(holderRequest.getHolder().getHolderType().equals(PersonType.NATURAL.getCode())){
		holder.setFullName(null);
	}
	else if(holderRequest.getHolder().getHolderType().equals(PersonType.JURIDIC.getCode())){
		holder.setName(null);
		holder.setFirstLastName(null);
		holder.setSecondLastName(null);
	}
	getLstActionsType();
	if(holderRequest.getRequestType().equals(HolderRequestType.BLOCK.getCode())){
		actionType = HolderBlockUnBlockActionsType.BLOCK.getCode();	
		getLstMotives(MasterTableType.HOLDER_BLOCK_MOTIVE);	
	}
	else if(holderRequest.getRequestType().equals(HolderRequestType.UNBLOCK.getCode())){
		actionType = HolderBlockUnBlockActionsType.UNBLOCK.getCode();	
		getLstMotives(MasterTableType.HOLDER_UNBLOCK_MOTIVE);	
	}
	motiveType = holderRequest.getRequestMotive();
	if(actionType.equals(HolderBlockUnBlockActionsType.BLOCK.getCode())){
		showMotiveText=true;
		requestOtherMotive = holderRequest.getRequestOtherMotive();
	}
	else if(actionType.equals(HolderBlockUnBlockActionsType.UNBLOCK.getCode())){
			showMotiveText=true;
			requestOtherMotive = holderRequest.getRequestOtherMotive();
	}
	}

	/**
	 * Gets the lst participant.
	 *
	 * @return the lst participant
	 */
	public void getLstParticipant() {

		try {
			Participant filter = new Participant();
			filter.setState(ParticipantStateType.REGISTERED.getCode());
			listParticipant = accountsFacade.getLisParticipantServiceBean(filter);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Validate is participant.
	 *
	 * @return true, if successful
	 */
	public boolean validateIsParticipant() {

		flagIsParticipant = false;
		
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			flagIsParticipant = true;	
		}		
		
		return flagIsParticipant;

	}
	
	/**
	 * Metodo que Obtiene lista de estados Bloqueado o desbloqueado de titular.
	 *
	 * @return the lst state b lock un block holder request type
	 */
	public void getLstStateBLockUnBlockHolderRequestType(){
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.STATE_CREATION_HOLDER_REQUEST.getCode());

			listStateBlockUnBlockHolderRequest = generalPametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
			
			for(int i=0;i<listStateBlockUnBlockHolderRequest.size();i++){
				if(listStateBlockUnBlockHolderRequest.get(i).getParameterTablePk().equals(HolderRequestStateType.MODIFIED.getCode())){
					listStateBlockUnBlockHolderRequest.remove(i);
				}
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Gets the lst requester type.
	 *
	 * @return the lst requester type
	 */
	public void getLstRequesterType(){
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.PARAMETER_REQUESTER_BLOCK_UNBLOC_HOLDERACCOUNT.getCode());

			listRequesterType=generalPametersFacade.getListParameterTableServiceBean(parameterTableTO);			
			
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Gets the lst motives.
	 *
	 * @param master the master
	 * @return the lst motives
	 */
	public void getLstMotives(MasterTableType master){
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO.setMasterTableFk(master.getCode());

			listMotives = generalPametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
			
			List<ParameterTable> listMotivesAux = new ArrayList<>();
			
			for(int i = 0; i<listMotives.size(); i++) {
				ParameterTable iteracion = listMotives.get(i);
				if(Validations.validateIsNotNull(this.requesterType) && this.requesterType == 1219) {
					if(Validations.validateIsNotNull(iteracion.getShortInteger()) && (iteracion.getShortInteger() == 1 || iteracion.getShortInteger() == 2)) {
						listMotivesAux.add(listMotives.get(i));
					}
				} else {
					if(Validations.validateIsNull(iteracion.getShortInteger())) {
						listMotivesAux.add(listMotives.get(i));
					} else if(iteracion.getShortInteger() == 1) {
						listMotivesAux.add(listMotives.get(i));
					}
				}
			}
			
			listMotives = listMotivesAux;

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Handle file upload.
	 *
	 * @param eventFile the event file
	 */
	@LoggerAuditWeb
	public void uploadSustentFiles(FileUploadEvent eventFile) {
		try{
			hideDialogsListener(null);
			JSFUtilities.setValidViewComponentAndChildrens("frmBlockUnBlockRequestRegister");
			
			String fDisplayName=fUploadValidateFile(eventFile.getFile(), null, null);
			 
			 if(fDisplayName!=null){
				 holderFileNameDisplay=fDisplayName;
			 }
			
			if(Validations.validateIsNull(lstHolderFileSustent)){
				lstHolderFileSustent = new ArrayList<HolderReqFileHistory>();
			}
			
			HolderReqFileHistory fileSustent = new HolderReqFileHistory();
			fileSustent = new HolderReqFileHistory();
			fileSustent.setHolderReqFileHisType(HolderReqFileHisType.CREATION.getCode());
			fileSustent.setFilename(fUploadValidateFileNameLength(eventFile.getFile().getFileName()));
			fileSustent.setFileHolderReq(eventFile.getFile().getContents());
			fileSustent.setDocumentType(ParticipantFileType.DEPOSITARY_REGISTRATION_REQUEST.getCode());
			
			lstHolderFileSustent.add(fileSustent);
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		
    }
	
	/**
	 * Hide dialogs listener.
	 *
	 * @param actionEvent the action event
	 */
	public void hideDialogsListener(ActionEvent actionEvent){
		executeAction();
		JSFUtilities.hideGeneralDialogues();
		
		JSFUtilities.hideComponent("cnfAdmRequestHolder");
		JSFUtilities.hideComponent("confirmActionsDialog");
	}
	
	/**
	 * Removes the file sustent.
	 *
	 * @param holderFileRemove the holder file remove
	 */
	public void removeFileSustent(HolderReqFileHistory holderFileRemove){
		lstHolderFileSustent.remove(holderFileRemove);
		holderFileNameDisplay = null;
	}

	/**
	 * Metodo que Obtiene Lista de Tipos de Persona.
	 *
	 * @return the lst person type
	 */
	public void getLstPersonType() {

		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.PERSON_TYPE
					.getCode());

			listPersonType = generalPametersFacade
					.getListParameterTableServiceBean(parameterTableTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}
	
	/**
	 * Metodo que Obtiene Lista de Tipo de Acciones.
	 *
	 * @return the lst actions type
	 */
	public void getLstActionsType() {

		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_BLOCK_UNBLOCK_ACTIONS_TYPE
					.getCode());

			listActionsType = generalPametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Validate holder request filter.
	 *
	 * @return true, if successful
	 */
	public boolean validateHolderFilter() {

		boolean flag = false;
		if (!holderFilter.getParticipantSelected().equals(Long.valueOf(-1))) {
			flag = true;
		} else if (!holderFilter.getStateRequest().equals(Integer.valueOf(-1))) {
			flag = true;
		} else if (holderFilter.getNumberRequest() != null) {
			flag = true;
		} else if (holderFilter.getInitialDate() != null
				&& holderFilter.getEndDate() != null) {
			flag = true;
		}
		return flag;
	}

	/**
	 * Metodo que busca solicitudes de Bloqueo y Desbloqueo  de Titulares
	 */
	@LoggerAuditWeb
	public void searchRequest(){
		
		setViewOperationType(ViewOperationsType.CONSULT.getCode()); 
		try{
			if(holderFilter.getInitialDate().getTime()>holderFilter.getEndDate().getTime()){				
		        JSFUtilities.addContextMessage("frmBlockUnBlockHolderSearch:calInitialDate",FacesMessage.SEVERITY_ERROR,
				PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_BLOCK_UNBLOCK_FILTER_DATE_INITIAL),
				PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_BLOCK_UNBLOCK_FILTER_DATE_INITIAL));
		        holderRequestDataModel = null;
		        return;
			}
			else if(holderFilter.getEndDate().getTime()<holderFilter.getInitialDate().getTime()){	
		
		        JSFUtilities.addContextMessage("frmBlockUnBlockHolderSearch:calEndDate",FacesMessage.SEVERITY_ERROR,
				PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_BLOCK_UNBLOCK_FILTER_DATE_FINAL),
				PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_BLOCK_UNBLOCK_FILTER_DATE_FINAL));
		        holderRequestDataModel = null;
		        return;
			}
			if(holderFilter.getRequestType()==null){
				holderFilter.setRequestType(HolderRequestType.BLOCK.getCode());
				holderFilter.setRequestTypeAux(HolderRequestType.UNBLOCK.getCode());
			}
			else{
				if(holderFilter.getRequestType().equals(HolderBlockUnBlockActionsType.BLOCK.getCode())){
					holderFilter.setRequestTypeAux(HolderRequestType.BLOCK.getCode());
				}
				else if(holderFilter.getRequestType().equals(HolderBlockUnBlockActionsType.UNBLOCK.getCode())){
					holderFilter.setRequestTypeAux(HolderRequestType.UNBLOCK.getCode());
				}
			}
			List<HolderRequest> lstResult = new ArrayList<HolderRequest>();
			lstResult = holderServiceFacade.getListHolderBlockUnBlockRequestServiceFacade(holderFilter);
			if(lstResult!=null && lstResult.size()>0){
				for(int i=0;i<lstResult.size();i++){
					    if(lstResult.get(i).getStateHolderRequest()!=null){
					    	lstResult.get(i).setStateDescription(requestStateDescription.get(lstResult.get(i).getStateHolderRequest()).toString());
					    }
					    else{
					    	lstResult.get(i).setStateDescription(GeneralConstants.EMPTY_STRING);
					    }
					    if(lstResult.get(i).getHolder().getDocumentType()!=null){
					    	lstResult.get(i).getHolder().setDescriptionTypeDocument(documentTypeDescription.get(lstResult.get(i).getHolder().getDocumentType()).toString());
					    }
					    else{
					     	lstResult.get(i).getHolder().setDescriptionTypeDocument(GeneralConstants.EMPTY_STRING);
					    }
				}			
			}
			holderRequestDataModel = new HolderRequestDataModel(lstResult);
			if(holderRequestDataModel.getRowCount()>0){
				setTrueValidButtons();
			}
		}
		catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Sets the true valid buttons.
	 */
	public void setTrueValidButtons(){
		
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		if(userPrivilege.getUserAcctions().isModify()){
		    privilegeComponent.setBtnModifyView(true);	
		}
		if(userPrivilege.getUserAcctions().isConfirm()){
			privilegeComponent.setBtnConfirmView(true);
		}
		if(userPrivilege.getUserAcctions().isReject()){
			privilegeComponent.setBtnRejectView(true);
		}
		if(userPrivilege.getUserAcctions().isAnnular()){
			privilegeComponent.setBtnAnnularView(true);
		}
		if(userPrivilege.getUserAcctions().isApprove()){
			privilegeComponent.setBtnApproveView(true);
		}
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}

	/**
	 * Validate holder request filter.
	 *
	 * @return true, if successful
	 */
	public boolean validateHolderRequestFilter() {

		boolean flag = false;

		if (!holderFilter.getParticipantSelected().equals(Long.valueOf(-1))) {
			flag = true;
		} else if (!holderFilter.getStateRequest().equals(Integer.valueOf(-1))) {
			flag = true;
		} else if (holderFilter.getNumberRequest() != null && holderFilter.getNumberRequest()>0) {
			flag = true;
		} else if (holderFilter.getInitialDate() != null
				&& holderFilter.getEndDate() != null) {
			flag = true;
		}
		return flag;
	}
	
	/**
	 * Flag actions in false.
	 */
	public void flagActionsInFalse() {
		flagActionAnnular = false;
		flagActionConfirm = false;
		flagActionReject = false;
		flagActionApprove = false;
	}
	
	/**
	 * Metodo que Valida Estado de la Solicitud de Confirmacion.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String validateRequestConfirmState(){
		
		setViewOperationType(ViewOperationsType.CONFIRM.getCode());
		String action="";
		boolean flag = false;
		if (holderRequestSelected == null) {
			Object[] bodyData = new Object[0];
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED,bodyData));
						JSFUtilities.showSimpleValidationDialog();
			flag = true;
		}
		else if(holderRequestSelected != null){
			if (!holderRequestSelected.getStateHolderRequest().equals(HolderRequestStateType.APPROVED.getCode())) {
				Object[] bodyData = new Object[0];
				flag = true;
				JSFUtilities.showMessageOnDialog(
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),						
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_OPERATION_CONFIRM_INVALID,bodyData));
			}
			
			// issue 605: verificamos nuevamente que el estado del cui sea REGISTRADO o BLOQUEADO
			if(Validations.validateIsNotNull(holderRequest.getHolder())
					&& Validations.validateIsNotNull(holderRequest.getHolder().getIdHolderPk())){
				Holder holderEvaluated=holderServiceFacade.find(Holder.class, holderRequest.getHolder().getIdHolderPk());
				if(Validations.validateIsNotNull(holderEvaluated)
						&& Validations.validateIsNotNull(holderEvaluated.getStateHolder())
						&& !holderEvaluated.getStateHolder().equals(HolderStateType.BLOCKED.getCode())
						&& !holderEvaluated.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
					Object[] bodyData = new Object[1];			
					bodyData[0]=holderEvaluated.getIdHolderPk().toString();
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),	
							PropertiesUtilities.getMessage(PropertiesConstants.WARNING_DISTINCT_REGISTERED_BLOCKED,bodyData));
					flag=true;
				}
			}
			
			if(flag){
				JSFUtilities.showSimpleValidationDialog();
			}else{
				loadingRequestInformation(null);
				action="goToBlockUnBlockDetail";	
			}
		}
		
		return action;
	}
	
	/**
	 * Metodo para validar Estado de Rechazo de Solicitud.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String validateRequestRejectState(){
		
		setViewOperationType(ViewOperationsType.REJECT.getCode());
		String action="";
		boolean flag = false;
		if (holderRequestSelected == null) {	
			Object[] bodyData = new Object[0];
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED,bodyData));
					JSFUtilities.showSimpleValidationDialog();
			flag = true;
		}
		else if(holderRequestSelected != null){
			if (!holderRequestSelected.getStateHolderRequest().equals(HolderRequestStateType.APPROVED.getCode())) {
				flag = true;
				Object[] bodyData = new Object[0];
				
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_OPERATION_REJECT_INVALID,bodyData));
			}
			if(flag){
				JSFUtilities.showSimpleValidationDialog();
			}else{
				loadingRequestInformation(null);
				action="goToBlockUnBlockDetail";	
			}
		}
		return action;
	}
	
	/**
	 * Metodo que Valida Estado de Aprovado  de una solicitud.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String validateRequestApproveState(){
		
		setViewOperationType(ViewOperationsType.APPROVE.getCode());
		String action="";
		boolean flag = false;
		if (holderRequestSelected == null) {
			Object[] bodyData = new Object[0];
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED,bodyData));
			JSFUtilities.showSimpleValidationDialog();
			flag = true;			
		}
		else if(holderRequestSelected != null){
			
			if (!holderRequestSelected.getStateHolderRequest().equals(HolderRequestStateType.REGISTERED.getCode())
					&& !holderRequestSelected.getStateHolderRequest().equals(HolderRequestStateType.MODIFIED.getCode())) {
				Object[] bodyData = new Object[0];
				flag = true;
				
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_OPERATION_APPROVE_INVALID,bodyData));
			}			
			if(flag){
				JSFUtilities.showSimpleValidationDialog();
			}else{
				loadingRequestInformation(null);
				action="goToBlockUnBlockDetail";	
			}
		}
		return action;
	}
	
	/**
	 * Metodo que Valida Estado de Anulado de la Solicitud.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String validateRequestAnnularState(){
		
		setViewOperationType(ViewOperationsType.ANULATE.getCode());
		String action="";
		boolean flag = false;
		if (holderRequestSelected == null) {	
			Object[] bodyData = new Object[0];
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED,bodyData));
			JSFUtilities.showSimpleValidationDialog();
		    flag = true;
		}
		else if(holderRequestSelected != null){
			if (!holderRequestSelected.getStateHolderRequest().equals(HolderRequestStateType.REGISTERED.getCode())
					&& !holderRequestSelected.getStateHolderRequest().equals(HolderRequestStateType.MODIFIED.getCode())) {
				Object[] bodyData = new Object[0];		
				flag = true;
				
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_OPERATION_ANNULAR_INVALID,bodyData));
			}			
			if(flag){
				JSFUtilities.showSimpleValidationDialog();
			}else{
				loadingRequestInformation(null);
				action="goToBlockUnBlockDetail";	
			}
		}
		return action;
	}
	
	/**
	 * Before on confirm.
	 *
	 * @param event the event
	 */
	@LoggerAuditWeb
	public void beforeOnConfirm(ActionEvent event){
		
		flagActionsInFalse();
		flagActionConfirm = true;
		Object[] bodyData = new Object[1];
		bodyData[0] = holderRequestSelected.getHolder().getIdHolderPk().toString();
		if(holderRequestSelected.getRequestType().equals(HolderRequestType.BLOCK.getCode())){
			 JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),PropertiesUtilities.getMessage(PropertiesConstants.ADM_BLOCK_REQUEST_HOLDER_CONFIRM_REGISTER,
					bodyData));
		}
		else if(holderRequestSelected.getRequestType().equals(HolderRequestType.UNBLOCK.getCode())){
			 JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					 PropertiesUtilities.getMessage(PropertiesConstants.ADM_UNBLOCK_REQUEST_HOLDER_CONFIRM_REGISTER,
						bodyData));
		}
		JSFUtilities.executeJavascriptFunction("PF('widgConfirmActions').show()");
	}
	
	/**
	 * Before on reject.
	 *
	 * @param event the event
	 */
	@LoggerAuditWeb
	public void beforeOnReject(ActionEvent event){
		JSFUtilities.resetViewRoot();
		getLstRejectMotivesType();
		flagActionsInFalse();
		flagActionReject = true;
		requestMotive=null;
		requestOtherMotive="";
		showMotiveText=false;
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MOTIVE_REJECT),null);
		JSFUtilities.executeJavascriptFunction("PF('dlgMotive').show()");
	}
	
	/**
	 * Before on aprove.
	 *
	 * @param event the event
	 */
	@LoggerAuditWeb	
	public void beforeOnAprove(ActionEvent event){
		flagActionsInFalse();
		flagActionApprove = true;
		Object[] bodyData = new Object[2];
		bodyData[0] = holderRequestSelected.getIdHolderRequestPk().toString();
		bodyData[1] = holderRequestSelected.getHolder().getIdHolderPk().toString();
		if(holderRequestSelected.getRequestType().equals(HolderRequestType.BLOCK.getCode())){			
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_APPROVE),				
					PropertiesUtilities.getMessage(PropertiesConstants.ADM_BLOCK_REQUEST_HOLDER_APPROVE_REGISTER,
				bodyData));		
		}
		if(holderRequestSelected.getRequestType().equals(HolderRequestType.UNBLOCK.getCode())){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_APPROVE),				
					PropertiesUtilities.getMessage(PropertiesConstants.ADM_UNBLOCK_REQUEST_HOLDER_APPROVE_REGISTER,
					bodyData));		
		}
		JSFUtilities.executeJavascriptFunction("PF('widgConfirmActions').show()");
	}
	
	/**
	 * Before on annular.
	 *
	 * @param event the event
	 */
	@LoggerAuditWeb
	public void beforeOnAnnular(ActionEvent event){
		JSFUtilities.resetViewRoot();
		getLstAnnularMotivesType();
		flagActionsInFalse();
		flagActionAnnular = true;
		requestMotive=null;	
		requestOtherMotive="";
		showMotiveText=false;
		   JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MOTIVE_ANNUL),null);
		   JSFUtilities.executeJavascriptFunction("PF('dlgMotive').show()");
	}
	
	/**
	 * Confirm actions.
	 */
	public void confirmActions(){
		
		if (flagActionReject) {
			Object[] bodyData = new Object[2];
			bodyData[0] = holderRequestSelected.getIdHolderRequestPk().toString();
			bodyData[1] = holderRequestSelected.getHolder().getIdHolderPk().toString();
			if(holderRequestSelected.getRequestType().equals(HolderRequestType.BLOCK.getCode())){
			 JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REJECT), 
					 PropertiesUtilities.getMessage(PropertiesConstants. ADM_BLOCK_REQUEST_HOLDER_REJECT_REGISTER,
					bodyData));
			}
			else if(holderRequestSelected.getRequestType().equals(HolderRequestType.UNBLOCK.getCode())){
			 JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REJECT),
					 PropertiesUtilities.getMessage(PropertiesConstants.ADM_UNBLOCK_REQUEST_HOLDER_REJECT_REGISTER,
						bodyData));
			}
			JSFUtilities.executeJavascriptFunction("PF('widgConfirmActions').show()");
		}
		else if (flagActionAnnular) {
			Object[] bodyData = new Object[2];
			bodyData[0] = holderRequestSelected.getIdHolderRequestPk().toString();
			bodyData[1] = holderRequestSelected.getHolder().getIdHolderPk().toString();
			if(holderRequestSelected.getRequestType().equals(HolderRequestType.BLOCK.getCode())){
			 JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ANNUL),
					 PropertiesUtilities.getMessage(PropertiesConstants.ADM_BLOCK_REQUEST_HOLDER_ANNULAR_REGISTER,
					bodyData));
			}
			else if(holderRequestSelected.getRequestType().equals(HolderRequestType.UNBLOCK.getCode())){
			 JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ANNUL),
					 PropertiesUtilities.getMessage(PropertiesConstants.ADM_UNBLOCK_REQUEST_HOLDER_ANNULAR_REGISTER,
						bodyData));
			}
			JSFUtilities.executeJavascriptFunction("PF('widgConfirmActions').show()");
		}
	}
		
    /**
     * Gets the lst reject motives type.
     *
     * @return the lst reject motives type
     */
    public void getLstRejectMotivesType(){
		
		try{
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_REQUEST_REJECT_MOTIVE.getCode());
			listMotives = generalPametersFacade.getListParameterTableServiceBean(parameterTableTO);
	    } catch (Exception ex) {
		excepcion.fire(new ExceptionToCatchEvent(ex));
	    }
	}
    
    /**
     * Gets the lst annular motives type.
     *
     * @return the lst annular motives type
     */
    public void getLstAnnularMotivesType(){
		
		try{
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_REQUEST_ANNULAR_MOTIVE.getCode());
		    listMotives = generalPametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}
		catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
     }
    
    /**
	 * Reset components.
	 */
	public void validateBack(){
		
		if(requestMotive!=null){
			if (flagActionReject) {
				if (requestMotive.equals(RejectMotivesHolderRequestType.OTHER_MOTIVES.getCode())) {
					if(requestOtherMotive!=null && !requestOtherMotive.trim().equals(GeneralConstants.EMPTY_STRING)){
						JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(PropertiesConstants.MESSAGES_ALERT),PropertiesUtilities.getGenericMessage(PropertiesConstants.MESSAGE_BACK));
						JSFUtilities.executeJavascriptFunction("PF('cnfwBackMotive').show()");
					}
					else{
						JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(PropertiesConstants.MESSAGES_ALERT),PropertiesUtilities.getGenericMessage(PropertiesConstants.MESSAGE_BACK));
						JSFUtilities.executeJavascriptFunction("PF('cnfwBackMotive').show()");
					}
				}
				else{
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(PropertiesConstants.MESSAGES_ALERT),PropertiesUtilities.getGenericMessage(PropertiesConstants.MESSAGE_BACK));
					JSFUtilities.executeJavascriptFunction("PF('cnfwBackMotive').show()");
				}
			}
			else if (flagActionAnnular) {
				if (requestMotive.equals(AnnularMotivesHolderRequestType.OTHER_MOTIVES.getCode())) {
					if(requestOtherMotive!=null && !requestOtherMotive.trim().equals(GeneralConstants.EMPTY_STRING)){
						JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(PropertiesConstants.MESSAGES_ALERT),PropertiesUtilities.getGenericMessage(PropertiesConstants.MESSAGE_BACK));
						JSFUtilities.executeJavascriptFunction("PF('cnfwBackMotive').show()");
					}
					else{
						JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(PropertiesConstants.MESSAGES_ALERT),PropertiesUtilities.getGenericMessage(PropertiesConstants.MESSAGE_BACK));
						JSFUtilities.executeJavascriptFunction("PF('cnfwBackMotive').show()");
					}
				}
				else{
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(PropertiesConstants.MESSAGES_ALERT),PropertiesUtilities.getGenericMessage(PropertiesConstants.MESSAGE_BACK));
					JSFUtilities.executeJavascriptFunction("PF('cnfwBackMotive').show()");
				}
			}
		}else{
			resetComponents();
			JSFUtilities.executeJavascriptFunction("PF('dlgMotive').hide()");
		}
	}
	
    /**
     * Reset components.
     */
    public void resetComponents(){
		JSFUtilities.setValidViewComponentAndChildrens("frmBlockUnBlockDetails");
	}
	
	/**
	 * Gets the holder request.
	 *
	 * @return the holder request
	 */
	public HolderRequest getHolderRequest() {
		return holderRequest;
	}

	/**
	 * Sets the holder request.
	 *
	 * @param holderRequest the new holder request
	 */
	public void setHolderRequest(HolderRequest holderRequest) {
		this.holderRequest = holderRequest;
	}

	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * Gets the holder filter.
	 *
	 * @return the holder filter
	 */
	public HolderRequestTO getHolderFilter() {
		return holderFilter;
	}

	/**
	 * Sets the holder filter.
	 *
	 * @param holderFilter the new holder filter
	 */
	public void setHolderFilter(HolderRequestTO holderFilter) {
		this.holderFilter = holderFilter;
	}

	/**
	 * Checks if is show panel options component.
	 *
	 * @return true, if is show panel options component
	 */
	public boolean isShowPanelOptionsComponent() {
		return showPanelOptionsComponent;
	}


	/**
	 * Sets the show panel options component.
	 *
	 * @param showPanelOptionsComponent the new show panel options component
	 */
	public void setShowPanelOptionsComponent(boolean showPanelOptionsComponent) {
		this.showPanelOptionsComponent = showPanelOptionsComponent;
	}


	/**
	 * Gets the list participant.
	 *
	 * @return the list participant
	 */
	public List<Participant> getListParticipant() {
		return listParticipant;
	}


	/**
	 * Sets the list participant.
	 *
	 * @param listParticipant the new list participant
	 */
	public void setListParticipant(List<Participant> listParticipant) {
		this.listParticipant = listParticipant;
	}


	/**
	 * Gets the list person type.
	 *
	 * @return the list person type
	 */
	public List<ParameterTable> getListPersonType() {
		return listPersonType;
	}


	/**
	 * Sets the list person type.
	 *
	 * @param listPersonType the new list person type
	 */
	public void setListPersonType(List<ParameterTable> listPersonType) {
		this.listPersonType = listPersonType;
	}


	/**
	 * Checks if is disabled cmb participant.
	 *
	 * @return true, if is disabled cmb participant
	 */
	public boolean isDisabledCmbParticipant() {
		return disabledCmbParticipant;
	}


	/**
	 * Sets the disabled cmb participant.
	 *
	 * @param disabledCmbParticipant the new disabled cmb participant
	 */
	public void setDisabledCmbParticipant(boolean disabledCmbParticipant) {
		this.disabledCmbParticipant = disabledCmbParticipant;
	}


	/**
	 * Checks if is flag is participant.
	 *
	 * @return true, if is flag is participant
	 */
	public boolean isFlagIsParticipant() {
		return flagIsParticipant;
	}


	/**
	 * Sets the flag is participant.
	 *
	 * @param flagIsParticipant the new flag is participant
	 */
	public void setFlagIsParticipant(boolean flagIsParticipant) {
		this.flagIsParticipant = flagIsParticipant;
	}


	/**
	 * Gets the list state block un block holder request.
	 *
	 * @return the list state block un block holder request
	 */
	public List<ParameterTable> getListStateBlockUnBlockHolderRequest() {
		return listStateBlockUnBlockHolderRequest;
	}


	/**
	 * Sets the list state block un block holder request.
	 *
	 * @param listStateBlockUnBlockHolderRequest the new list state block un block holder request
	 */
	public void setListStateBlockUnBlockHolderRequest(
			List<ParameterTable> listStateBlockUnBlockHolderRequest) {
		this.listStateBlockUnBlockHolderRequest = listStateBlockUnBlockHolderRequest;
	}


	/**
	 * Gets the holder request data model.
	 *
	 * @return the holder request data model
	 */
	public HolderRequestDataModel getHolderRequestDataModel() {
		return holderRequestDataModel;
	}


	/**
	 * Sets the holder request data model.
	 *
	 * @param holderRequestDataModel the new holder request data model
	 */
	public void setHolderRequestDataModel(
			HolderRequestDataModel holderRequestDataModel) {
		this.holderRequestDataModel = holderRequestDataModel;
	}


	/**
	 * Gets the holder request selected.
	 *
	 * @return the holder request selected
	 */
	public HolderRequest getHolderRequestSelected() {
		return holderRequestSelected;
	}


	/**
	 * Sets the holder request selected.
	 *
	 * @param holderRequestSelected the new holder request selected
	 */
	public void setHolderRequestSelected(HolderRequest holderRequestSelected) {
		this.holderRequestSelected = holderRequestSelected;
	}


	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}


	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}


	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}


	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}


	/**
	 * Checks if is flag holder natural person.
	 *
	 * @return true, if is flag holder natural person
	 */
	public boolean isFlagHolderNaturalPerson() {
		return flagHolderNaturalPerson;
	}


	/**
	 * Sets the flag holder natural person.
	 *
	 * @param flagHolderNaturalPerson the new flag holder natural person
	 */
	public void setFlagHolderNaturalPerson(boolean flagHolderNaturalPerson) {
		this.flagHolderNaturalPerson = flagHolderNaturalPerson;
	}


	/**
	 * Gets the list actions type.
	 *
	 * @return the list actions type
	 */
	public List<ParameterTable> getListActionsType() {
		return listActionsType;
	}


	/**
	 * Sets the list actions type.
	 *
	 * @param listActionsType the new list actions type
	 */
	public void setListActionsType(List<ParameterTable> listActionsType) {
		this.listActionsType = listActionsType;
	}


	


	/**
	 * Gets the lst block un block request history.
	 *
	 * @return the lst block un block request history
	 */
	public HolderRequestDataModel getLstBlockUnBlockRequestHistory() {
		return lstBlockUnBlockRequestHistory;
	}


	/**
	 * Sets the lst block un block request history.
	 *
	 * @param lstBlockUnBlockRequestHistory the new lst block un block request history
	 */
	public void setLstBlockUnBlockRequestHistory(
			HolderRequestDataModel lstBlockUnBlockRequestHistory) {
		this.lstBlockUnBlockRequestHistory = lstBlockUnBlockRequestHistory;
	}


	/**
	 * Gets the holder history state.
	 *
	 * @return the holder history state
	 */
	public HolderHistoryState getHolderHistoryState() {
		return holderHistoryState;
	}


	/**
	 * Sets the holder history state.
	 *
	 * @param holderHistoryState the new holder history state
	 */
	public void setHolderHistoryState(HolderHistoryState holderHistoryState) {
		this.holderHistoryState = holderHistoryState;
	}
	
	/**
	 * Metodo que Guarda.
	 */
	@LoggerAuditWeb
	public void saveAll(){
		try{
			if(Validations.validateListIsNotNullAndNotEmpty(lstHolderFileSustent)){
				holderRequest.setHolderReqFileHistories(lstHolderFileSustent);
			} else {
				holderRequest.setHolderReqFileHistories(null);
			}
			holderRequest.setRequestOtherMotive(requestOtherMotive);			
			holderRequest.setRequestMotive(motiveType);
			
			if(actionType.equals(HolderBlockUnBlockActionsType.BLOCK.getCode())){						
				holderRequest.setRequestType(HolderRequestType.BLOCK.getCode());				
			}
		    else if(actionType.equals(HolderBlockUnBlockActionsType.UNBLOCK.getCode())){		    	
		    	holderRequest.setRequestType(HolderRequestType.UNBLOCK.getCode());			
		    }
			holderRequest.setRequesterType(requesterType);			
			holderRequest.setActionDate(CommonsUtilities.currentDate());
			holderRequest.setStateHolderRequest(HolderRequestStateType.REGISTERED.getCode());
			holderRequest.setRegistryDate(CommonsUtilities.currentDate());
			holderRequest.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			holderRequest.setHolder(holder);
			if(requesterParticipant){
			holderRequest.setParticipant(participant);
			}
			holderRequest = holderServiceFacade.registerHolderRequestServiceFacade(holderRequest);
			if(Validations.validateIsNotNull(holderRequest.getIdHolderRequestPk())){
		    holderRequest.setStateHolderRequest(null);	
			lstBlockUnBlockRequestHistory = new HolderRequestDataModel(holderServiceFacade.getListHolderBlockUnBlockRequestServiceFacade(holderRequest));
			Object[] bodyData = new Object[2];
			bodyData[0] = holderRequest.getIdHolderRequestPk().toString();
			bodyData[1] = holder.getIdHolderPk().toString();
			if(actionType.equals(HolderBlockUnBlockActionsType.BLOCK.getCode())){					
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
						PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_REGISTRY_BLOCK_HOLDER_REQUEST,
						bodyData));
				businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_BLOCK_REGISTER.getCode());
				Object[] parameters = new Object[3];
				parameters[0]=holderRequest.getHolder().getIdHolderPk().toString();
				parameters[1]=holderRequest.getHolder().getDescriptionHolder();	
				parameters[2]=holderRequest.getIdHolderRequestPk().toString();
				Participant auxParticipant  = (holderServiceFacade.getBlockUnBlockHolderRequestServiceFacade(holderRequest.getHolder())).getParticipant();
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,auxParticipant.getIdParticipantPk(),parameters);
				holder = new Holder();
				participant = new Participant();
				requesterType = null;
				holder = new Holder();
				actionType = null;
				motiveType = null;
				showMotiveText = false;
				lstBlockUnBlockRequestHistory = null;
				requesterParticipant = false;
				holderRequest = new HolderRequest();
				holderHistoryState = new HolderHistoryState();
				}
				else if(actionType.equals(HolderBlockUnBlockActionsType.UNBLOCK.getCode())){					
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
						PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_REGISTRY_UNBLOCK_HOLDER_REQUEST,
							bodyData));
				
				businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_UNBLOCK_REGISTER.getCode());
				Object[] parameters = new Object[3];
				parameters[0]=holderRequest.getHolder().getIdHolderPk().toString();
				parameters[1]=holderRequest.getHolder().getDescriptionHolder();
				parameters[2]=holderRequest.getIdHolderRequestPk().toString();
				Participant auxParticipant  = (holderServiceFacade.getBlockUnBlockHolderRequestServiceFacade(holderRequest.getHolder())).getParticipant();
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,auxParticipant.getIdParticipantPk(),parameters);
     			holder = new Holder();
				participant = new Participant();
				requesterType = null;
				holder = new Holder();
				actionType = null;
				motiveType = null;
				showMotiveText = false;
				lstBlockUnBlockRequestHistory = null;
				requesterParticipant = false;
				holderRequest = new HolderRequest();
				holderHistoryState = new HolderHistoryState();
				}
				JSFUtilities.executeJavascriptFunction("PF('endTrasaction').show()");				
			  } 
		}
		catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Before save all.
	 */
	@LoggerAuditWeb
	public void beforeSaveAll(){
		if(requesterType.equals(HolderAccountRequesterType.PARTICIPANT.getCode())){
			if(!validateParticipantWithHolder(participant.getIdParticipantPk(),holder)){
				return;
			}
		}
		Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		Object[] headData = new Object[1];
		Object[] bodyData = new Object[1];
		headData[0] = PropertiesUtilities.getMessage(locale, "btn.register");
		bodyData[0] =holder.getIdHolderPk().toString();
		if(actionType!=null && actionType.equals(HolderBlockUnBlockActionsType.BLOCK.getCode())){		
		JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_CONFIRM_HEADER_RECORD,
				headData,
				PropertiesConstants.ADM_BLOCK_REQUEST_HOLDER_REGISTER, bodyData);
		}
		else if(actionType!=null && actionType.equals(HolderBlockUnBlockActionsType.UNBLOCK.getCode())){
		JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_CONFIRM_HEADER_RECORD,
					headData,
					PropertiesConstants.ADM_UNBLOCK_REQUEST_HOLDER_REGISTER,bodyData);
		}
		if(Validations.validateListIsNullOrEmpty(lstHolderFileSustent)){
    		JSFUtilities.addContextMessage("frmBlockUnBlockRequestRegister:fuplHolderFile",
    				FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_FILES_SUSTENT), 
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_FILES_SUSTENT));
    		showMessageOnDialog(
    				PropertiesConstants.MESSAGES_ALERT,
    				null,
    				PropertiesConstants.MESSAGE_DATA_REQUIRED,							
    				null);
    		JSFUtilities.showSimpleValidationDialog();
    		return;
    	}
		JSFUtilities.executeJavascriptFunction("PF('cnfwAdmRequestHolder').show()");
	}
	
/**
 * Action do.
 */
@LoggerAuditWeb
public void actionDo() {
		
		try{
		holderRequest = holderServiceFacade.getHolderInformationServiceFacade(holderRequestSelected.getIdHolderRequestPk());
		holderRequest.setStateDescription(holderRequestSelected.getStateDescription());
		Holder objHolder = new Holder();
		objHolder.setIdHolderPk(holderRequestSelected.getHolder().getIdHolderPk());
		holder = holderServiceFacade.getHolderDataServiceFacade(objHolder.getIdHolderPk());
		holderRequest.setHolder(holder);
			if (flagActionConfirm) {
				confirmHolderRequest();
			} else if (flagActionReject) {
				rejectHolderRequest();
			} else if(flagActionApprove) {
				approveHolderRequest();
			} else if (flagActionAnnular) {
				annularHolderRequest();
			}	
	    } 
		catch (Exception ex) {
		excepcion.fire(new ExceptionToCatchEvent(ex));
	    }
	}

	/**
	 * Validate data.
	 *
	 * @param field the field
	 * @param idComponent the id component
	 */
	public void validateData(String field, String idComponent) {
	log.info("validateData " + field);
	log.info("validateData " + idComponent);
	}

    /**
     * Metodo que confirma solicitud de Titular.
     */
    @LoggerAuditWeb
    public void confirmHolderRequest(){
    	
    	try{
    	holderRequest.setStateHolderRequest(HolderRequestStateType.CONFIRMED.getCode());
    	holderRequest.setConfirmDate(CommonsUtilities.currentDate());
    	holderRequest.setConfirmUser(userInfo.getUserAccountSession().getUserName());
    	List<HolderHistoryState> lstHolderHistoryState = new ArrayList<HolderHistoryState>();
		holderHistoryState.setHolder(holder);
		holderHistoryState.setHolderRequest(holderRequest);
		holderHistoryState.setRegistryUser(userInfo.getUserAccountSession().getUserName());
		if(holderRequestSelected.getRequestOtherMotive()!=null){
			holderHistoryState.setOtherMotive(holderRequestSelected.getRequestOtherMotive());
		}
		if(holderRequestSelected.getRequestMotive()!=null){
		   holderHistoryState.setMotive(holderRequestSelected.getRequestMotive());
		}
		holderHistoryState.setHolderRequest(holderRequestSelected);
		if(holderRequestSelected.getRequestType().equals(HolderRequestType.BLOCK.getCode())){
			holder.setStateHolder(HolderStateType.BLOCKED.getCode());
			holderHistoryState.setOldState(StateHistoryStateHolderType.REGISTERED.getCode());
			holderHistoryState.setNewState(StateHistoryStateHolderType.BLOCKED.getCode());
		}
		else if(holderRequestSelected.getRequestType().equals(HolderRequestType.UNBLOCK.getCode())){
			holder.setStateHolder(HolderStateType.REGISTERED.getCode());
			holderHistoryState.setOldState(StateHistoryStateHolderType.BLOCKED.getCode());
			holderHistoryState.setNewState(StateHistoryStateHolderType.REGISTERED.getCode());
		}
		holderHistoryState.setUpdateStateDate(getCurrentSystemDate());
		lstHolderHistoryState.add(holderHistoryState);
		holderRequest.setHolder(holder);
		holderRequest.setHolderHistoryStates(lstHolderHistoryState);
		boolean flagTransaction = false;
		if(holderRequestSelected.getRequestType().equals(HolderRequestType.BLOCK.getCode())){
			
			flagTransaction = holderServiceFacade.updateStateConfirmBlockHolderRequestServiceFacade(holderRequest,holder,ViewOperationsType.CONFIRM.getCode());
		}
		else if(holderRequestSelected.getRequestType().equals(HolderRequestType.UNBLOCK.getCode())){
			flagTransaction = holderServiceFacade.updateStateConfirmUnBlockHolderRequestServiceFacade(holderRequest,holder,ViewOperationsType.CONFIRM.getCode());
		}
		
		if (flagTransaction) {
			Object[] bodyData = new Object[1];
			bodyData[0] = holderRequestSelected.getHolder().getIdHolderPk().toString();
			if(holderRequestSelected.getRequestType().equals(HolderRequestType.BLOCK.getCode())){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_CONFIRM_HOLDER_BLOCK_REQUEST,	bodyData));
				businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_BLOCK_CONFIRM.getCode());
				Object[] parameters = new Object[3];
				parameters[0]=holderRequest.getIdHolderRequestPk().toString();
				parameters[1]=holderRequest.getHolder().getIdHolderPk().toString();
				parameters[2]=holderRequest.getHolder().getDescriptionHolder();
				Participant auxParticipant  = (holderServiceFacade.getBlockUnBlockHolderRequestServiceFacade(holderRequest.getHolder())).getParticipant();
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,auxParticipant.getIdParticipantPk(),parameters);
			}
			else if(holderRequestSelected.getRequestType().equals(HolderRequestType.UNBLOCK.getCode())){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_CONFIRM_HOLDER_UNBLOCK_REQUEST,bodyData));
				businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_UNBLOCK_CONFIRM.getCode());
				Object[] parameters = new Object[3];
				parameters[0]=holderRequest.getIdHolderRequestPk().toString();
				parameters[1]=holderRequest.getHolder().getIdHolderPk().toString();
				parameters[2]=holderRequest.getHolder().getDescriptionHolder();				
				Participant auxParticipant  = (holderServiceFacade.getBlockUnBlockHolderRequestServiceFacade(holderRequest.getHolder())).getParticipant();
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,auxParticipant.getIdParticipantPk(),parameters);
			}
			JSFUtilities.executeJavascriptFunction("PF('widgTransactionDialog').show()");
			searchRequest();
		 }
    	}
    	catch (Exception ex) {
    		excepcion.fire(new ExceptionToCatchEvent(ex));
    	}
    }
    
    /**
     * Reject holder request.
     */
    @LoggerAuditWeb
    public void rejectHolderRequest(){
    	
    	try{
    	JSFUtilities.executeJavascriptFunction("PF('dlgMotive').hide()");
    	holderRequest.setStateHolderRequest(HolderRequestStateType.REJECTED.getCode());
    	holderRequest.setRejectDate(CommonsUtilities.currentDate());
    	holderRequest.setRejectUser(userInfo.getUserAccountSession().getUserName());
    	boolean flagTransaction = false;
    	if(holderRequestSelected.getRequestType().equals(HolderRequestType.BLOCK.getCode())){
			flagTransaction = holderServiceFacade.updateStateRejectBlockHolderRequestServiceFacade(holderRequest,null,ViewOperationsType.REJECT.getCode());
    	}
    	else if(holderRequestSelected.getRequestType().equals(HolderRequestType.UNBLOCK.getCode())){
    		flagTransaction = holderServiceFacade.updateStateRejectUnBlockHolderRequestServiceFacade(holderRequest,null,ViewOperationsType.REJECT.getCode());	
    	}
		if (flagTransaction) {
			Object[] bodyData = new Object[1];
			bodyData[0] = holderRequestSelected.getHolder().getIdHolderPk().toString();
			if(holderRequestSelected.getRequestType().equals(HolderRequestType.BLOCK.getCode())){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),PropertiesUtilities.getMessage(PropertiesConstants.LBL_SUCCESS_REJECTED_HOLDER_BLOCK_REQUEST,	bodyData));
				businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_BLOCK_REJECT.getCode());
				Object[] parameters = new Object[3];
				parameters[0]=holderRequest.getIdHolderRequestPk().toString();
				parameters[1]=holderRequest.getHolder().getIdHolderPk().toString();
				parameters[2]=holderRequest.getHolder().getDescriptionHolder();
				Participant auxParticipant  = (holderServiceFacade.getBlockUnBlockHolderRequestServiceFacade(holderRequest.getHolder())).getParticipant();
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,auxParticipant.getIdParticipantPk(),parameters);
				
			}
			else if(holderRequestSelected.getRequestType().equals(HolderRequestType.UNBLOCK.getCode())){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),PropertiesUtilities.getMessage(PropertiesConstants.LBL_SUCCESS_REJECTED_HOLDER_UNBLOCK_REQUEST, bodyData));
				businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_UNBLOCK_REJECT.getCode());
				Object[] parameters = new Object[3];
				parameters[0]=holderRequest.getIdHolderRequestPk().toString();
				parameters[1]=holderRequest.getHolder().getIdHolderPk().toString();
				parameters[2]=holderRequest.getHolder().getDescriptionHolder();				
				Participant auxParticipant  = (holderServiceFacade.getBlockUnBlockHolderRequestServiceFacade(holderRequest.getHolder())).getParticipant();
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,auxParticipant.getIdParticipantPk(),parameters);
			}
			JSFUtilities.executeJavascriptFunction("PF('widgTransactionDialog').show()");
			searchRequest();
		 }
    	}
    	catch (Exception ex) {
    		excepcion.fire(new ExceptionToCatchEvent(ex));
    	}
    }
    
    /**
     * Metodo para Aprovar solicitud del holder.
     */
    @LoggerAuditWeb
    public void approveHolderRequest(){
    	
    	try{
    		holderRequest.setStateHolderRequest(HolderRequestStateType.APPROVED.getCode());
        	holderRequest.setApproveDate(CommonsUtilities.currentDate());
        	holderRequest.setApproveUser(userInfo.getUserAccountSession().getUserName());
        	boolean flagTransaction = false;
        	if(holderRequestSelected.getRequestType().equals(HolderRequestType.BLOCK.getCode())){
    		   flagTransaction = holderServiceFacade.updateStateApproveBlockHolderRequestServiceFacade(holderRequest,null,ViewOperationsType.APPROVE.getCode());
        	}
        	else if(holderRequestSelected.getRequestType().equals(HolderRequestType.UNBLOCK.getCode())){
        	   flagTransaction = holderServiceFacade.updateStateApproveUnBlockHolderRequestServiceFacade(holderRequest,null,ViewOperationsType.APPROVE.getCode());
        	}
    		if (flagTransaction) {
    			Object[] bodyData = new Object[1];
    			bodyData[0] = holderRequestSelected.getHolder().getIdHolderPk().toString();
    			if(holderRequestSelected.getRequestType().equals(HolderRequestType.BLOCK.getCode())){
    				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),PropertiesUtilities.getMessage(PropertiesConstants.LBL_SUCCESS_APPROVE_HOLDER_BLOCK_REQUEST,	bodyData));
    				businessProcess = new BusinessProcess();
     				businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_BLOCK_APPROVE.getCode());
    				Object[] parameters = new Object[3];
    				parameters[0]=holderRequest.getIdHolderRequestPk().toString();
    				parameters[1]=holderRequest.getHolder().getIdHolderPk().toString();
    				parameters[2]=holderRequest.getHolder().getDescriptionHolder();				
    				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,null,parameters);
    			}
    			else if(holderRequestSelected.getRequestType().equals(HolderRequestType.UNBLOCK.getCode())){
    				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),PropertiesUtilities.getMessage(PropertiesConstants.LBL_SUCCESS_APPROVE_HOLDER_UNBLOCK_REQUEST,bodyData));
    				businessProcess = new BusinessProcess();
    				businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_UNBLOCK_APPROVE.getCode());
    				Object[] parameters = new Object[3];
    				parameters[0]=holderRequest.getIdHolderRequestPk().toString();
    				parameters[1]=holderRequest.getHolder().getIdHolderPk().toString();
    				parameters[2]=holderRequest.getHolder().getDescriptionHolder();				
    				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,null,parameters);
    			}
    			JSFUtilities.executeJavascriptFunction("PF('widgTransactionDialog').show()");
    			searchRequest();
    		 }
    	}
    	catch (Exception ex) {
    		excepcion.fire(new ExceptionToCatchEvent(ex));
    	}
    }
    
    /**
     * Metodo para Anular Solicitud de holder.
     */
    @LoggerAuditWeb
    public void annularHolderRequest(){

    	try{
    	JSFUtilities.executeJavascriptFunction("PF('dlgMotive').hide()");
    	holderRequest.setStateHolderRequest(HolderRequestStateType.ANNULED.getCode());
    	holderRequest.setAnnulDate(CommonsUtilities.currentDate());
    	holderRequest.setAnnulUser(userInfo.getUserAccountSession().getUserName());
    	boolean flagTransaction = false;
    	if(holderRequestSelected.getRequestType().equals(HolderRequestType.BLOCK.getCode())){
    		flagTransaction = holderServiceFacade.updateStateAnulateBlockHolderRequestServiceFacade(holderRequest,null,ViewOperationsType.ANULATE.getCode());
    	}
    	else if(holderRequestSelected.getRequestType().equals(HolderRequestType.UNBLOCK.getCode())){
    		flagTransaction = holderServiceFacade.updateStateAnulateUnBlockHolderRequestServiceFacade(holderRequest,null,ViewOperationsType.ANULATE.getCode());
    	}
    	
		if (flagTransaction) {
			Object[] bodyData = new Object[1];

			bodyData[0] = holderRequestSelected.getHolder().getIdHolderPk().toString();

			if(holderRequestSelected.getRequestType().equals(HolderRequestType.BLOCK.getCode())){
				
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), PropertiesUtilities.getMessage(PropertiesConstants.LBL_SUCCESS_ANNULED_HOLDER_BLOCK_REQUEST,	bodyData));
				businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_BLOCK_CANCEL.getCode());
				Object[] parameters = new Object[3];
				parameters[0]=holderRequest.getIdHolderRequestPk().toString();
				parameters[1]=holderRequest.getHolder().getIdHolderPk().toString();
				parameters[2]=holderRequest.getHolder().getDescriptionHolder();				
				Participant auxParticipant  = (holderServiceFacade.getBlockUnBlockHolderRequestServiceFacade(holderRequest.getHolder())).getParticipant();
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,auxParticipant.getIdParticipantPk(),parameters);
			}
			else if(holderRequestSelected.getRequestType().equals(HolderRequestType.UNBLOCK.getCode())){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), PropertiesUtilities.getMessage(PropertiesConstants.LBL_SUCCESS_ANNULED_HOLDER_UNBLOCK_REQUEST,	bodyData));
				businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_UNBLOCK_CANCEL.getCode());
				Object[] parameters = new Object[3];
				parameters[0]=holderRequest.getIdHolderRequestPk().toString();
				parameters[1]=holderRequest.getHolder().getIdHolderPk().toString();
				parameters[2]=holderRequest.getHolder().getDescriptionHolder();
				Participant auxParticipant  = (holderServiceFacade.getBlockUnBlockHolderRequestServiceFacade(holderRequest.getHolder())).getParticipant();
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,auxParticipant.getIdParticipantPk(),parameters);
			}
			JSFUtilities.executeJavascriptFunction("PF('widgTransactionDialog').show()");
			searchRequest();
		 }
    	}
    	catch (Exception ex) {
    		excepcion.fire(new ExceptionToCatchEvent(ex));
    	}
    }
    
    /**
	 * Gets the holder file name display.
	 *
	 * @return the holder file name display
	 */
	public String getHolderFileNameDisplay() {
		return holderFileNameDisplay;
	}

	/**
	 * Sets the holder file name display.
	 *
	 * @param holderFileNameDisplay the new holder file name display
	 */
	public void setHolderFileNameDisplay(String holderFileNameDisplay) {
		this.holderFileNameDisplay = holderFileNameDisplay;
	}
	
	/**
	 * Gets the lst holder file sustent.
	 *
	 * @return the lst holder file sustent
	 */
	public List<HolderReqFileHistory> getLstHolderFileSustent() {
		return lstHolderFileSustent;
	}

	/**
	 * Sets the lst holder file sustent.
	 *
	 * @param lstHolderFileSustent the new lst holder file sustent
	 */
	public void setLstParticipantFileSustent(
			List<HolderReqFileHistory> lstHolderFileSustent) {
		this.lstHolderFileSustent = lstHolderFileSustent;
	}
	
   	/**
	    * Gets the action type.
	    *
	    * @return the action type
	    */
	   public Integer getActionType() {
		return actionType;
	}


	/**
	 * Sets the action type.
	 *
	 * @param actionType the new action type
	 */
	public void setActionType(Integer actionType) {
		this.actionType = actionType;
	}


	/**
	 * Checks if is flag exits information.
	 *
	 * @return true, if is flag exits information
	 */
	public boolean isFlagExitsInformation() {
		return flagExitsInformation;
	}


	/**
	 * Sets the flag exits information.
	 *
	 * @param flagExitsInformation the new flag exits information
	 */
	public void setFlagExitsInformation(boolean flagExitsInformation) {
		this.flagExitsInformation = flagExitsInformation;
	}

	/**
	 * Gets the motive type.
	 *
	 * @return the motive type
	 */
	public Integer getMotiveType() {
		return motiveType;
	}


	/**
	 * Sets the motive type.
	 *
	 * @param motiveType the new motive type
	 */
	public void setMotiveType(Integer motiveType) {
		this.motiveType = motiveType;
	}


	/**
	 * Gets the list motives.
	 *
	 * @return the list motives
	 */
	public List<ParameterTable> getListMotives() {
		return listMotives;
	}


	/**
	 * Sets the list motives.
	 *
	 * @param listMotives the new list motives
	 */
	public void setListMotives(List<ParameterTable> listMotives) {
		this.listMotives = listMotives;
	}


	/**
	 * Gets the request other motive.
	 *
	 * @return the request other motive
	 */
	public String getRequestOtherMotive() {
		return requestOtherMotive;
	}


	/**
	 * Sets the request other motive.
	 *
	 * @param requestOtherMotive the new request other motive
	 */
	public void setRequestOtherMotive(String requestOtherMotive) {
		this.requestOtherMotive = requestOtherMotive;
	}


	/**
	 * Checks if is show motive text.
	 *
	 * @return true, if is show motive text
	 */
	public boolean isShowMotiveText() {
		return showMotiveText;
	}


	/**
	 * Sets the show motive text.
	 *
	 * @param showMotiveText the new show motive text
	 */
	public void setShowMotiveText(boolean showMotiveText) {
		this.showMotiveText = showMotiveText;
	}


	/**
	 * Checks if is flag action annular.
	 *
	 * @return true, if is flag action annular
	 */
	public boolean isFlagActionAnnular() {
		return flagActionAnnular;
	}


	/**
	 * Sets the flag action annular.
	 *
	 * @param flagActionAnnular the new flag action annular
	 */
	public void setFlagActionAnnular(boolean flagActionAnnular) {
		this.flagActionAnnular = flagActionAnnular;
	}


	/**
	 * Checks if is flag action confirm.
	 *
	 * @return true, if is flag action confirm
	 */
	public boolean isFlagActionConfirm() {
		return flagActionConfirm;
	}


	/**
	 * Sets the flag action confirm.
	 *
	 * @param flagActionConfirm the new flag action confirm
	 */
	public void setFlagActionConfirm(boolean flagActionConfirm) {
		this.flagActionConfirm = flagActionConfirm;
	}


	/**
	 * Checks if is flag action reject.
	 *
	 * @return true, if is flag action reject
	 */
	public boolean isFlagActionReject() {
		return flagActionReject;
	}


	/**
	 * Sets the flag action reject.
	 *
	 * @param flagActionReject the new flag action reject
	 */
	public void setFlagActionReject(boolean flagActionReject) {
		this.flagActionReject = flagActionReject;
	}


	/**
	 * Gets the request motive.
	 *
	 * @return the request motive
	 */
	public Integer getRequestMotive() {
		return requestMotive;
	}


	/**
	 * Sets the request motive.
	 *
	 * @param requestMotive the new request motive
	 */
	public void setRequestMotive(Integer requestMotive) {
		this.requestMotive = requestMotive;
	}


	/**
	 * Checks if is flag action approve.
	 *
	 * @return true, if is flag action approve
	 */
	public boolean isFlagActionApprove() {
		return flagActionApprove;
	}


	/**
	 * Sets the flag action approve.
	 *
	 * @param flagActionApprove the new flag action approve
	 */
	public void setFlagActionApprove(boolean flagActionApprove) {
		this.flagActionApprove = flagActionApprove;
	}


	/**
	 * Checks if is requester participant.
	 *
	 * @return true, if is requester participant
	 */
	public boolean isRequesterParticipant() {
		return requesterParticipant;
	}


	/**
	 * Sets the requester participant.
	 *
	 * @param requesterParticipant the new requester participant
	 */
	public void setRequesterParticipant(boolean requesterParticipant) {
		this.requesterParticipant = requesterParticipant;
	}


	/**
	 * Gets the requester type.
	 *
	 * @return the requester type
	 */
	public Integer getRequesterType() {
		return requesterType;
	}


	/**
	 * Sets the requester type.
	 *
	 * @param requesterType the new requester type
	 */
	public void setRequesterType(Integer requesterType) {
		this.requesterType = requesterType;
	}


	/**
	 * Gets the list requester type.
	 *
	 * @return the list requester type
	 */
	public List<ParameterTable> getListRequesterType() {
		return listRequesterType;
	}


	/**
	 * Sets the list requester type.
	 *
	 * @param listRequesterType the new list requester type
	 */
	public void setListRequesterType(List<ParameterTable> listRequesterType) {
		this.listRequesterType = listRequesterType;
	}


	/**
	 * Checks if is person natural.
	 *
	 * @return true, if is person natural
	 */
	public boolean isPersonNatural() {
		return personNatural;
	}


	/**
	 * Sets the person natural.
	 *
	 * @param personNatural the new person natural
	 */
	public void setPersonNatural(boolean personNatural) {
		this.personNatural = personNatural;
	}


	/**
	 * Checks if is person juridic.
	 *
	 * @return true, if is person juridic
	 */
	public boolean isPersonJuridic() {
		return personJuridic;
	}


	/**
	 * Sets the person juridic.
	 *
	 * @param personJuridic the new person juridic
	 */
	public void setPersonJuridic(boolean personJuridic) {
		this.personJuridic = personJuridic;
	}


     /**
	 * Checks if is numeric type document holder.
	 *
	 * @return true, if is numeric type document holder
	 */
	public boolean isNumericTypeDocumentHolder() {
		return numericTypeDocumentHolder;
	}


	/**
	 * Sets the numeric type document holder.
	 *
	 * @param numericTypeDocumentHolder the new numeric type document holder
	 */
	public void setNumericTypeDocumentHolder(boolean numericTypeDocumentHolder) {
		this.numericTypeDocumentHolder = numericTypeDocumentHolder;
	}


	/**
	 * Gets the max lenght document number holder.
	 *
	 * @return the max lenght document number holder
	 */
	public Integer getMaxLenghtDocumentNumberHolder() {
		return maxLenghtDocumentNumberHolder;
	}


	/**
	 * Sets the max lenght document number holder.
	 *
	 * @param maxLenghtDocumentNumberHolder the new max lenght document number holder
	 */
	public void setMaxLenghtDocumentNumberHolder(
			Integer maxLenghtDocumentNumberHolder) {
		this.maxLenghtDocumentNumberHolder = maxLenghtDocumentNumberHolder;
	}


	/**
	 * Checks if is disabled document number.
	 *
	 * @return true, if is disabled document number
	 */
	public boolean isDisabledDocumentNumber() {
		return disabledDocumentNumber;
	}


	/**
	 * Sets the disabled document number.
	 *
	 * @param disabledDocumentNumber the new disabled document number
	 */
	public void setDisabledDocumentNumber(boolean disabledDocumentNumber) {
		this.disabledDocumentNumber = disabledDocumentNumber;
	}


	/**
	 * Gets the list document type.
	 *
	 * @return the list document type
	 */
	public List<ParameterTable> getListDocumentType() {
		return listDocumentType;
	}


	/**
	 * Sets the list document type.
	 *
	 * @param listDocumentType the new list document type
	 */
	public void setListDocumentType(List<ParameterTable> listDocumentType) {
		this.listDocumentType = listDocumentType;
	}


	/**
	 * Gets the user privilege.
	 *
	 * @return the user privilege
	 */
	public UserPrivilege getUserPrivilege() {
		return userPrivilege;
	}


	/**
	 * Sets the user privilege.
	 *
	 * @param userPrivilege the new user privilege
	 */
	public void setUserPrivilege(UserPrivilege userPrivilege) {
		this.userPrivilege = userPrivilege;
	}
}
