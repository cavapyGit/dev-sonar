package com.pradera.accounts.holder.facade;

 
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.apache.log4j.Logger;

import com.pradera.accounts.holder.service.HolderControlBean;
import com.pradera.accounts.holder.service.HolderServiceBean;
import com.pradera.accounts.holder.to.HolderAnnulationModelTO;
import com.pradera.accounts.holder.to.HolderRequestTO;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.report.remote.ReportRegisterTO;
import com.pradera.commons.services.remote.reports.ReportGenerationService;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ModuleWarType;
import com.pradera.commons.type.RegistryType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.core.framework.entity.service.LoaderEntityServiceBean;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.HolderAnnulation;
import com.pradera.model.accounts.HolderAnnulationHistory;
import com.pradera.model.accounts.HolderFile;
import com.pradera.model.accounts.HolderHistory;
import com.pradera.model.accounts.HolderHistoryState;
import com.pradera.model.accounts.HolderReqFileHistory;
import com.pradera.model.accounts.HolderRequest;
import com.pradera.model.accounts.LegalRepresentative;
import com.pradera.model.accounts.LegalRepresentativeFile;
import com.pradera.model.accounts.LegalRepresentativeHistory;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.PepPerson;
import com.pradera.model.accounts.RepresentativeFileHistory;
import com.pradera.model.accounts.RepresentedEntity;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStateDetailType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.HolderAnnulationStateType;
import com.pradera.model.accounts.type.HolderFileStateType;
import com.pradera.model.accounts.type.HolderReqFileHisType;
import com.pradera.model.accounts.type.HolderRequestStateType;
import com.pradera.model.accounts.type.HolderRequestType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.LegalRepresentativeStateType;
import com.pradera.model.accounts.type.PepPersonClassificationType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.accounts.type.RepresentativeEntityStateType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.operations.RequestCorrelativeType;
import com.pradera.model.report.ReportLoggerFile;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class HolderServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/08/2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class HolderServiceFacade extends CrudDaoServiceBean {
	
	Logger log = Logger.getLogger(HolderServiceFacade.class);
	 /** The holder service bean. */
 	@EJB
	 HolderServiceBean holderServiceBean;
	 
 	/** The crud dao service bean. */
 	@EJB
	 CrudDaoServiceBean crudDaoServiceBean;
	 
 	/** The general pameters facade. */
 	@EJB
	 GeneralParametersFacade generalPametersFacade;
	 
 	/** The load entity service. */
 	@EJB
	 LoaderEntityServiceBean loadEntityService;
	 
 	/** The holder control bean. */
 	@EJB
	 HolderControlBean holderControlBean;
	 
	 /** The transaction registry. */
 	@Resource
	    TransactionSynchronizationRegistry transactionRegistry;
	 
	 /** The user info. */
 	@Inject
		private UserInfo userInfo;
	 
	 /** The report service. */
 	@Inject
	 	Instance<ReportGenerationService> reportService;
		
		
	 /** The notification service facade. */
		@EJB
		NotificationServiceFacade notificationServiceFacade;
		
		/** The accounts facade. */
		@EJB
		AccountsFacade accountsFacade;
 	
	/** verifica si el CUI ingresado esta en una solicitud de bloqueo pendiente por confirmar
	 * 
	 * @param idHolderPk
	 * @return */
	public boolean thisHolderExistInRequestBlock(Long idHolderPk) {
		if (holderServiceBean.countRequestBlockOfHolder(idHolderPk) > 0)
			return true;
		else
			return false;
	}
	
	/** verifica si el CUI ingresado esta en una solicitud de Desbloqueo pendiente por confirmar
	 * 
	 * @param idHolderPk
	 * @return */
	public boolean thisHolderExistInRequestUnBlock(Long idHolderPk) {
		if (holderServiceBean.countRequestUnblockOfHolder(idHolderPk) > 0)
			return true;
		else
			return false;
	}
	
	/** verifica si el CUI ingresado esta en una solicitud de Anulacion pendiente por confirmar
	 * 
	 * @param idHolderPk
	 * @return */
	public boolean thisHolderExistInRequestAnnulation(Long idHolderPk) {
		if (holderServiceBean.countRequestAnnulationOfHolder(idHolderPk) > 0)
			return true;
		else
			return false;
	}
	
	/**
	 * verifica si el CUI ingresado tiene saldos en cartera
	 * @param idHoldePk
	 * @return
	 */
	public boolean thisHolderHaveBalance(Long idHoldePk){
		if(holderServiceBean.countBalanceOfOneHolder(idHoldePk) > 0)
			return true;
		else return false;
		
	}
 	
 	/**
 	 * issue 605: obtiene los mnemocios de los participates relacionados a un CUI en una cadena
 	 * @param idHolder
 	 * @return
 	 */
 	public String getParticipantsRelationshipOfHolder(Long idHolder){
 		List<String> lst=holderServiceBean.getParticipantsRelationshipOfHolder(idHolder);
 		StringBuilder sd=new StringBuilder();
 		sd.append("");
 		boolean firstLoop=true;
 		for(String mnemonicParticipat:lst){
			if (firstLoop)
				firstLoop = false;
			else
				sd.append(",");
			sd.append(mnemonicParticipat);
 		}
 		
 		return sd.toString();
 	}
 	
 	/**
 	 * issue 605: metodo para el cambio de estado de una Anulacion de CUI, tambien se guarda su historico
 	 * @param modelTO
 	 * @return
 	 * @throws ServiceException
 	 */
 	public HolderAnnulation changeStateHolderAnnulation(HolderAnnulationModelTO modelTO)throws ServiceException{
 		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
 		try {
			if(modelTO.getStateAnnulation().equals(HolderAnnulationStateType.APPROVED.getCode())){
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
			} else if(modelTO.getStateAnnulation().equals(HolderAnnulationStateType.ANNULATED.getCode())){
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
			} else if(modelTO.getStateAnnulation().equals(HolderAnnulationStateType.CONFIRMED.getCode())){
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
			} else if(modelTO.getStateAnnulation().equals(HolderAnnulationStateType.REJECTED.getCode())){
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
			} else if(modelTO.getStateAnnulation().equals(HolderAnnulationStateType.REVIEWED.getCode())){
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReview());
			}			
		} catch (Exception e) {
			System.out.println("::: no se pudo obtener el privilegio en el metodo HolderServiceFacade.changeStateHolderAnnulation. Se procedera a setear las pistas de auditoria manualemnte");
		}
 		
 		HolderAnnulation holderAnnulation=holderServiceBean.find(HolderAnnulation.class, modelTO.getIdAnnulationHolderPk());
 		holderAnnulation.setStateAnnulation(modelTO.getStateAnnulation());
 		//si el usuario anula o rechaza se guardan los motivos
 		holderAnnulation.setRequestCancelType(modelTO.getRequestCancelTypeSelected());
 		holderAnnulation.setRequestCancelDescription(modelTO.getRequestCancelDescription());
 		
 		//pistas de auditoria
 		holderAnnulation.setLastModifyUser(modelTO.getAuditUserName());
 		holderAnnulation.setLastModifyDate(new Date());
 		holderAnnulation.setLastModifyIp(modelTO.getAuditIpAddress());
 		holderAnnulation.setLastModifyApp(modelTO.getAuditIdPrivilege());
 		
	 	// preparando el historico
		HolderAnnulationHistory holderAnnulationHis=new HolderAnnulationHistory();
		holderAnnulationHis.setMotiveAnnulation(modelTO.getMotiveAnnulationSelected());
		holderAnnulationHis.setObservationAnnulation(modelTO.getObservationAnnulation());
		holderAnnulationHis.setFileTypeAnnulation(modelTO.getFileTypeAnnulationSelected());
		// el adjunto ya no se guarda en el historico(EL ARCHIVO NO CAMBIA) para evitar data innecesaria en la BBDD
//		holderAnnulationHis.setFileAttached(modelTO.getFileAttached());
//		holderAnnulationHis.setFileName(modelTO.getFileName());
		//si el usuario anula o rechaza se guardan los motivos
		holderAnnulationHis.setRequestCancelType(modelTO.getRequestCancelTypeSelected());
		holderAnnulationHis.setRequestCancelDescription(modelTO.getRequestCancelDescription());
		holderAnnulationHis.setStateAnnulation(modelTO.getStateAnnulation());
		holderAnnulationHis.setRegistryDate(holderAnnulation.getRegistryDate());
		holderAnnulationHis.setIdParticipantFk(modelTO.getIdParticipantSelected());
		holderAnnulationHis.setIdHolderFk(modelTO.getIdHolder());
		holderAnnulationHis.setLastModifyUser(holderAnnulation.getLastModifyUser());
		holderAnnulationHis.setLastModifyDate(holderAnnulation.getLastModifyDate());
		holderAnnulationHis.setLastModifyIp(holderAnnulation.getLastModifyIp());
		holderAnnulationHis.setLastModifyApp(holderAnnulation.getLastModifyApp());
		holderAnnulationHis.setIdAnnulationHolderFk(holderAnnulation);
		// para el registro en cascada
		List<HolderAnnulationHistory> lstHolderAnnulationHis=new ArrayList<>();
		lstHolderAnnulationHis.add(holderAnnulationHis);
		holderAnnulation.setHolderAnnulationHistoryList(lstHolderAnnulationHis);
		
		//si trata de estado CONFIRMADO entonces realizar las siguientes acciones:
		// cambiar a estado TRANSFERIDO el Holder
		// cambiar a estado ELIMINADO sus HolderAccountDetail
		// cambiar a estado CERRADO sus HolderAccount
		if(modelTO.getStateAnnulation().equals(HolderAnnulationStateType.CONFIRMED.getCode())){
			// cambiar a estado TRANSFERIDO el Holder
			Holder holderToAnnulate=holderServiceBean.find(Holder.class, modelTO.getIdHolder());
			holderToAnnulate.setStateHolder(HolderStateType.TRANSFERED.getCode());
			//seteando las pistas de auditoria
			holderToAnnulate.setLastModifyUser(holderAnnulation.getLastModifyUser());
			holderToAnnulate.setLastModifyDate(holderAnnulation.getLastModifyDate());
			holderToAnnulate.setLastModifyIp(holderAnnulation.getLastModifyIp());
			holderToAnnulate.setLastModifyApp(holderAnnulation.getLastModifyApp().intValue());
			holderServiceBean.update(holderToAnnulate);
			
			// cambiar a estado ELIMINADO sus HolderAccountDetail
			List<HolderAccountDetail> lstHadToStateDelete=holderServiceBean.getHolderAccountDetailOfHolder(modelTO.getIdHolder());
			for(HolderAccountDetail had:lstHadToStateDelete){
				had.setStateAccountDetail(HolderAccountStateDetailType.DELETED.getCode());
				//seteando las pistas de auditoria
				had.setLastModifyUser(holderAnnulation.getLastModifyUser());
				had.setLastModifyDate(holderAnnulation.getLastModifyDate());
				had.setLastModifyIp(holderAnnulation.getLastModifyIp());
				had.setLastModifyApp(holderAnnulation.getLastModifyApp().intValue());
				holderServiceBean.update(had);
			}
			
			// cambiar a estado CERRADO sus HolderAccount
			for(HolderAccountDetail had:lstHadToStateDelete){
				had.getHolderAccount().setStateAccount(HolderAccountStatusType.CLOSED.getCode());
				//seteando las pistas de auditoria
				had.getHolderAccount().setLastModifyUser(holderAnnulation.getLastModifyUser());
				had.getHolderAccount().setLastModifyDate(holderAnnulation.getLastModifyDate());
				had.getHolderAccount().setLastModifyIp(holderAnnulation.getLastModifyIp());
				had.getHolderAccount().setLastModifyApp(holderAnnulation.getLastModifyApp().intValue());
				holderServiceBean.update(had.getHolderAccount());
			}
		}
 		
 		return holderServiceBean.updateHolderAnnulation(holderAnnulation);
 		
 	}
 	
 	/**
 	 * issue 605: metodo para obtener un HolderAnnulation incluido los datos del CUI y su participante creador
 	 * @param idHolderAnnulationPk
 	 * @return
 	 * @throws ServiceException
 	 */
 	public HolderAnnulation getOneHolderAnnulationForView(Long idHolderAnnulationPk) throws ServiceException{
 		return holderServiceBean.getOneHolderAnnulationForView(idHolderAnnulationPk);
 	}
 	
 	/**
 	 * issue 605: obtiene las solictudes de anulacion de cui segun los filtros enviados
 	 * @param filter
 	 * @return
 	 * @throws ServiceException
 	 */
 	public List<HolderAnnulationModelTO> getLstHolderAnnulationModelTO(HolderAnnulationModelTO filter)throws ServiceException{
 		List<HolderAnnulationModelTO> lst=holderServiceBean.getLstHolderAnnulationTO(filter);
 		// aca vamos hacer las capturas de los estados
 		
		for (HolderAnnulationModelTO holderAnnulate : lst) {
			// capturando el texto de estado
			for (ParameterTable state : filter.getLstState())
				if (state.getParameterTablePk().equals(holderAnnulate.getStateAnnulation())) {
					holderAnnulate.setStateAnnulationText(state.getParameterName());
					break;
				}
			
			// capturando el texto tipo de archivo
			for (ParameterTable fileType : filter.getLstFileTypeAnnulation())
				if (fileType.getParameterTablePk().equals(holderAnnulate.getFileTypeAnnulationSelected())) {
					holderAnnulate.setFileTypeAnnulationText(fileType.getParameterName());
					break;
				}
			
			// capturando el texto tipo de archivo
			for (ParameterTable motive : filter.getLstMoviveAnnulation())
				if (motive.getParameterTablePk().equals(holderAnnulate.getMotiveAnnulationSelected())) {
					holderAnnulate.setMotiveAnnulationText(motive.getParameterName());
					break;
				}
		}
 		
 		return lst;
 	}
 	
 	//issue 605: registro solictud de anulacion de cui
 	public HolderAnnulation regiterHolderAnnulation(HolderAnnulationModelTO modelTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		try {
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());	
		} catch (Exception e) {
			System.out.println("::: no se pudo obtener el privilegio en el metodo HolderServiceFacade.regiterHolderAnnulation. Se procedera a setear las pistas de auditoria manualemnte");
		}
		
		HolderAnnulation holderAnnulation=new HolderAnnulation();
		holderAnnulation.setMotiveAnnulation(modelTO.getMotiveAnnulationSelected());
		holderAnnulation.setObservationAnnulation(modelTO.getObservationAnnulation());
		holderAnnulation.setFileTypeAnnulation(modelTO.getFileTypeAnnulationSelected());
		holderAnnulation.setFileAttached(modelTO.getFileAttached());
		holderAnnulation.setFileName(modelTO.getFileName());
		holderAnnulation.setStateAnnulation(HolderAnnulationStateType.REGISTERED.getCode());
		holderAnnulation.setRegistryDate(new Date());
		holderAnnulation.setIdParticipantFk(new Participant(modelTO.getIdParticipantSelected()));
		holderAnnulation.setIdHolderFk(modelTO.getHolderSelected());
		
		//pistas de auditoria
		holderAnnulation.setLastModifyUser(modelTO.getAuditUserName());
		holderAnnulation.setLastModifyDate(new Date());
		holderAnnulation.setLastModifyIp(modelTO.getAuditIpAddress());
		holderAnnulation.setLastModifyApp(modelTO.getAuditIdPrivilege());
		
		// preparando el historico
		HolderAnnulationHistory holderAnnulationHis=new HolderAnnulationHistory();
		holderAnnulationHis.setMotiveAnnulation(modelTO.getMotiveAnnulationSelected());
		holderAnnulationHis.setObservationAnnulation(modelTO.getObservationAnnulation());
		holderAnnulationHis.setFileTypeAnnulation(modelTO.getFileTypeAnnulationSelected());
		holderAnnulationHis.setFileAttached(modelTO.getFileAttached());
		holderAnnulationHis.setFileName(modelTO.getFileName());
		holderAnnulationHis.setStateAnnulation(HolderAnnulationStateType.REGISTERED.getCode());
		holderAnnulationHis.setRegistryDate(holderAnnulation.getRegistryDate());
		holderAnnulationHis.setIdParticipantFk(modelTO.getIdParticipantSelected());
		holderAnnulationHis.setIdHolderFk(modelTO.getHolderSelected().getIdHolderPk());
		holderAnnulationHis.setLastModifyUser(holderAnnulation.getLastModifyUser());
		holderAnnulationHis.setLastModifyDate(holderAnnulation.getLastModifyDate());
		holderAnnulationHis.setLastModifyIp(holderAnnulation.getLastModifyIp());
		holderAnnulationHis.setLastModifyApp(holderAnnulation.getLastModifyApp());
		holderAnnulationHis.setIdAnnulationHolderFk(holderAnnulation);
		// para el registro en cascada
		List<HolderAnnulationHistory> lstHolderAnnulationHis=new ArrayList<>();
		lstHolderAnnulationHis.add(holderAnnulationHis);
		holderAnnulation.setHolderAnnulationHistoryList(lstHolderAnnulationHis);
		
		return holderServiceBean.regiterHolderAnnulation(holderAnnulation);
 	}
 	
		
	 
	 /**
 	 * Register holder request service facade.
 	 *
 	 * @param holderRequest the holder request
 	 * @param participant the participant
 	 * @param holderHistory the holder history
 	 * @param lstLegalRepresentativeHistories the lst legal representative histories
 	 * @param lstHolderReqFileHistory the lst holder req file history
 	 * @return the holder request
 	 * @throws ServiceException the service exception
 	 */
 	@ProcessAuditLogger(process=BusinessProcessType.NEW_HOLDER_CREATION_REQUEST_REGISTRATION)
     public HolderRequest registerHolderRequestServiceFacade(HolderRequest holderRequest, 
    		 Participant participant,
    		 HolderHistory holderHistory,
    		 List<LegalRepresentativeHistory> lstLegalRepresentativeHistories,
    		 List<HolderReqFileHistory> lstHolderReqFileHistory) throws ServiceException {
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	     loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
	      
		 return holderServiceBean.registerHolderRequestServiceBean(holderRequest,
				 participant,
				 holderHistory,
				 lstLegalRepresentativeHistories,
				 lstHolderReqFileHistory,loggerUser);
	 }
	 
	 
	 /**
 	 * Register holder request service facade.
 	 *
 	 * @param holderRequest the holder request
 	 * @return the holder request
 	 * @throws ServiceException the service exception
 	 */
 	@ProcessAuditLogger(process=BusinessProcessType.NEW_HOLDER_CREATION_REQUEST_REGISTRATION)
     public HolderRequest registerHolderRequestServiceFacade(HolderRequest holderRequest) throws ServiceException {
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	     loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
	     
		 if(holderRequest.getRequestType().equals(HolderRequestType.BLOCK.getCode()) || 
				 holderRequest.getRequestType().equals(HolderRequestType.UNBLOCK.getCode())){
			 holderRequest.setRequestNumber(crudDaoServiceBean.getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_BLOCK_UNBLOCK_RNT));
		 }
		 else{			 
		 holderRequest.setRequestNumber(crudDaoServiceBean.getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_REGISTER_RNT));		 
		 }
		 
		 //loadEntityService.detachmentAll();
		 if(Validations.validateListIsNotNullAndNotEmpty(holderRequest.getHolderReqFileHistories())){
				for (HolderReqFileHistory holderFileSustent : holderRequest.getHolderReqFileHistories()) {
					holderFileSustent.setRegistryUser(loggerUser.getUserName());
					holderFileSustent.setRegistryDate(CommonsUtilities.currentDateTime());
					holderFileSustent.setStateFile(HolderFileStateType.REGISTERED.getCode());
					holderFileSustent.setDescription("DOCUMENTO SUSTENTARIO");
					holderFileSustent.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
					holderFileSustent.setHolderRequest(holderRequest);
				}
			}
		  holderRequest = holderControlBean.validaExistHolderRequest(holderRequest);
		  if(Validations.validateIsNull(holderRequest)){
			  throw new ServiceException(ErrorServiceType.HOLDER_REQUEST_EXIST);    	
		  }
		  
		  return holderRequest;
	 }
	 
	 
	 /**
 	 * Creates the holder request service facade.
 	 *
 	 * @param holderRequest the holder request
 	 * @return the holder request
 	 */
 	public HolderRequest createHolderRequestServiceFacade(HolderRequest holderRequest) {
		 HolderRequest auxHolderRequest;	    
	     auxHolderRequest = crudDaoServiceBean.create(holderRequest);
	     return auxHolderRequest;  
	     
	 }
	 
	 /**
 	 * Register modification holder request service facade.
 	 *
 	 * @param holderRequest the holder request
 	 * @return true, if successful
 	 * @throws ServiceException the service exception
 	 */
 	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_MODIFICATION_REQUEST_REGISTER)
	 public boolean registerModificationHolderRequestServiceFacade(HolderRequest holderRequest) throws ServiceException {
		 
		 boolean flagTransaction=false;	
		
		 HolderRequest finalHolderRequest  = new HolderRequest();
			
		 
		 Holder holder;
		 Participant participant = new Participant();
		 
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	     loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
	     
	     HolderHistory holderHistoryCopy = new HolderHistory();
		 List<HolderReqFileHistory> lstHolderReqFileHistoryCopy = new ArrayList<HolderReqFileHistory>();
		 List<LegalRepresentativeHistory> lstLegalRepresentativeHistoryCopy = new ArrayList<LegalRepresentativeHistory>();
	     
		 holder = getHolderServiceFacade(holderRequest.getHolder().getIdHolderPk());
			
			HolderReqFileHistory holderReqFileHistory;
			for(int f=0;f<holder.getHolderFile().size();f++){
				holderReqFileHistory = new HolderReqFileHistory();
				holderReqFileHistory.setDescription(holder.getHolderFile().get(f).getDescription());
				holderReqFileHistory.setDocumentType(holder.getHolderFile().get(f).getDocumentType());
				holderReqFileHistory.setFilename(holder.getHolderFile().get(f).getFilename());
				holderReqFileHistory.setHolderReqFileHisType(HolderReqFileHisType.CREATION.getCode());
				holderReqFileHistory.setRegistryType(RegistryType.HISTORICAL_COPY.getCode());
				holderReqFileHistory.setStateFile(HolderFileStateType.REGISTERED.getCode());
				holderReqFileHistory.setRegistryDate(CommonsUtilities.currentDate());
				holderReqFileHistory.setRegistryUser(userInfo.getUserAccountSession().getUserName());
				holderReqFileHistory.setFileHolderReq(holder.getHolderFile().get(f).getFileHolder());
				holderReqFileHistory.setHolderRequest(finalHolderRequest);
				lstHolderReqFileHistoryCopy.add(holderReqFileHistory);
			}
	
			if((holder.getParticipant().getIdParticipantPk()!=null)){
					participant.setIdParticipantPk(holder.getParticipant().getIdParticipantPk());				
			}		
			
			
			
			holderHistoryCopy.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			holderHistoryCopy.setStateHolderHistory(HolderRequestStateType.REGISTERED.getCode());
			holderHistoryCopy.setHolderHistoryType(HolderRequestType.CREATION.getCode());
			holderHistoryCopy.setRegistryType(RegistryType.HISTORICAL_COPY.getCode());
			holderHistoryCopy.setRegistryDate(CommonsUtilities.currentDate());
	
			holderHistoryCopy.setLegalResidenceCountry(holder.getLegalResidenceCountry());		
			holderHistoryCopy.setLegalProvince(holder.getLegalProvince());
			holderHistoryCopy.setPostalResidenceCountry(holder.getPostalResidenceCountry());
			holderHistoryCopy.setPostalProvince(holder.getPostalProvince());
			
			holderHistoryCopy.setFundAdministrator(holder.getFundAdministrator());
			holderHistoryCopy.setFundType(holder.getFundType());
			holderHistoryCopy.setTransferNumber(holder.getTransferNumber());
			holderHistoryCopy.setMnemonic(holder.getMnemonic());
			holderHistoryCopy.setRelatedCui(holder.getRelatedCui());
				
			holderHistoryCopy.setBirthDate(holder.getBirthDate());						
			holderHistoryCopy.setDocumentNumber(holder.getDocumentNumber());	
			holderHistoryCopy.setDocumentIssuanceDate(holder.getDocumentIssuanceDate());
			holderHistoryCopy.setDocumentType(holder.getDocumentType());						
			holderHistoryCopy.setEconomicActivity(holder.getEconomicActivity());	
			holderHistoryCopy.setInvestorType(holder.getInvestorType());
			holderHistoryCopy.setEconomicSector(holder.getEconomicSector());
			holderHistoryCopy.setEmail(holder.getEmail());
			holderHistoryCopy.setFaxNumber(holder.getFaxNumber());
			holderHistoryCopy.setFirstLastName(holder.getFirstLastName());
			holderHistoryCopy.setFullName(holder.getFullName());
			holderHistoryCopy.setHolderType(holder.getHolderType());
			holderHistoryCopy.setHomePhoneNumber(holder.getHomePhoneNumber());
			holderHistoryCopy.setIndResidence(holder.getIndResidence());						
			holderHistoryCopy.setJuridicClass(holder.getJuridicClass());
			holderHistoryCopy.setLegalAddress(holder.getLegalAddress());						
			holderHistoryCopy.setLegalDepartment(holder.getLegalDepartment());		
			holderHistoryCopy.setLegalDistrict(holder.getLegalDistrict());
			holderHistoryCopy.setLegalProvince(holder.getLegalProvince());	
			holderHistoryCopy.setLegalResidenceCountry(holder.getLegalResidenceCountry());						
			holderHistoryCopy.setMobileNumber(holder.getMobileNumber());
			holderHistoryCopy.setName(holder.getName());
			holderHistoryCopy.setNationality(holder.getNationality());						
			holderHistoryCopy.setOfficePhoneNumber(holder.getOfficePhoneNumber());
			holderHistoryCopy.setPostalAddress(holder.getPostalAddress());						
			holderHistoryCopy.setPostalDepartment(holder.getPostalDepartment());
			holderHistoryCopy.setPostalDistrict(holder.getPostalDistrict());
			holderHistoryCopy.setPostalProvince(holder.getPostalProvince());	
			holderHistoryCopy.setPostalResidenceCountry(holder.getPostalResidenceCountry());	
			holderHistoryCopy.setSecondDocumentNumber(holder.getSecondDocumentNumber());						
			holderHistoryCopy.setSecondDocumentType(holder.getSecondDocumentType());						
			holderHistoryCopy.setSecondLastName(holder.getSecondLastName());
			holderHistoryCopy.setSecondNationality(holder.getSecondNationality());						
			holderHistoryCopy.setSex(holder.getSex());
			holderHistoryCopy.setStateHolderHistory(holder.getStateHolder());
			holderHistoryCopy.setIndDisabled(holder.getIndDisabled());						
			holderHistoryCopy.setIndMinor(holder.getIndMinor());		
			holderHistoryCopy.setDocumentSource(holder.getDocumentSource());
			holderHistoryCopy.setDocumentIssuanceDate(holder.getDocumentIssuanceDate());
			holderHistoryCopy.setMarriedLastName(holder.getMarriedLastName());
			holderHistoryCopy.setCivilStatus(holder.getCivilStatus());
			holderHistoryCopy.setMainActivity(holder.getMainActivity());
			holderHistoryCopy.setJobSourceBusinessName(holder.getJobSourceBusinessName());
			holderHistoryCopy.setJobDateAdmission(holder.getJobDateAdmission());
			holderHistoryCopy.setAppointment(holder.getAppointment());
			holderHistoryCopy.setJobAddress(holder.getJobAddress());
			holderHistoryCopy.setTotalIncome(holder.getTotalIncome());
			holderHistoryCopy.setPerRefFullName(holder.getPerRefFullName());
			holderHistoryCopy.setPerRefLandLine(holder.getPerRefLandLine());
			holderHistoryCopy.setPerRefCellPhone(holder.getPerRefCellPhone());
			holderHistoryCopy.setComerRefBusinessName(holder.getComerRefBusinessName());
			holderHistoryCopy.setComerRefLandLine(holder.getComerRefLandLine());
			holderHistoryCopy.setEntityPublicAppointment(holder.getEntityPublicAppointment());
			holderHistoryCopy.setNitNaturalPerson(holder.getNitNaturalPerson());
			holderHistoryCopy.setWebsite(holder.getWebsite());
			holderHistoryCopy.setFundCompanyEnrollment(holder.getFundCompanyEnrollment());
			holderHistoryCopy.setConstitutionDate(holder.getConstitutionDate());
			holderHistoryCopy.setIndCanNegotiate(holder.getIndCanNegotiate());
			holderHistoryCopy.setIndSirtexNeg(holder.getIndSirtexNeg());
			holderHistoryCopy.setIndFatca(holder.getIndFatca());
			holderHistoryCopy.setFilePhotoName(holder.getFilePhotoName());
		    
			holderHistoryCopy.setFilePhoto(holder.getFilePhoto());
			holderHistoryCopy.setFileSigning(holder.getFileSigning());
			
			PepPerson auxPepPerson = holderServiceBean.getPepPersonByIdHolderServiceBean(holder.getIdHolderPk());
			
			if(auxPepPerson!=null){							
				holderHistoryCopy.setIndPEP(BooleanType.YES.getCode());
					holderHistoryCopy.setBeginningPeriod(auxPepPerson.getBeginingPeriod());
					holderHistoryCopy.setEndingPeriod(auxPepPerson.getEndingPeriod());
					holderHistoryCopy.setRole(auxPepPerson.getRole());
					holderHistoryCopy.setCategory(auxPepPerson.getCategory());
					holderHistoryCopy.setGrade(auxPepPerson.getGrade());
					holderHistoryCopy.setPepRelatedName(auxPepPerson.getPepRelatedName());
				
			}
			else{
				holderHistoryCopy.setIndPEP(BooleanType.NO.getCode());				
			}
			
			
			holderHistoryCopy.setHolderRequest(finalHolderRequest);
			
			if(holder.getRepresentedEntity()!=null){
				for(int i=0;i<holder.getRepresentedEntity().size();i++){
					
					LegalRepresentativeHistory legalRepresentativeHistory = new LegalRepresentativeHistory();
					
					legalRepresentativeHistory.setRepresentativeClass(holder.getRepresentedEntity().get(i).getRepresentativeClass());
					
					legalRepresentativeHistory.setHolderRequest(finalHolderRequest);
					
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getBirthDate()!=null){
						legalRepresentativeHistory.setBirthDate(holder.getRepresentedEntity().get(i).getLegalRepresentative().getBirthDate());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentNumber()!=null){
						legalRepresentativeHistory.setDocumentNumber(holder.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentNumber());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentType()!=null){
						legalRepresentativeHistory.setDocumentType(holder.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentType());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getEconomicActivity()!=null){
						legalRepresentativeHistory.setEconomicActivity(holder.getRepresentedEntity().get(i).getLegalRepresentative().getEconomicActivity());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getEconomicSector()!=null){
						legalRepresentativeHistory.setEconomicSector(holder.getRepresentedEntity().get(i).getLegalRepresentative().getEconomicSector());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getEmail()!=null){
						legalRepresentativeHistory.setEmail(holder.getRepresentedEntity().get(i).getLegalRepresentative().getEmail());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getFaxNumber()!=null){
						legalRepresentativeHistory.setFaxNumber(holder.getRepresentedEntity().get(i).getLegalRepresentative().getFaxNumber());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getFirstLastName()!=null){
						legalRepresentativeHistory.setFirstLastName(holder.getRepresentedEntity().get(i).getLegalRepresentative().getFirstLastName());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getFullName()!=null){
						legalRepresentativeHistory.setFullName(holder.getRepresentedEntity().get(i).getLegalRepresentative().getFullName());
					}
					
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getHomePhoneNumber()!=null){
						legalRepresentativeHistory.setHomePhoneNumber(holder.getRepresentedEntity().get(i).getLegalRepresentative().getHomePhoneNumber());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getIndResidence()!=null){
						
						legalRepresentativeHistory.setIndResidence(holder.getRepresentedEntity().get(i).getLegalRepresentative().getIndResidence()); 
					}
					
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyApp()!=null){
						legalRepresentativeHistory.setLastModifyApp(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyApp());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyDate()!=null){
						legalRepresentativeHistory.setLastModifyDate(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyDate());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyIp()!=null){
						legalRepresentativeHistory.setLastModifyIp(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyIp());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyUser()!=null){
						legalRepresentativeHistory.setLastModifyUser(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLastModifyUser());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalAddress()!=null){
						legalRepresentativeHistory.setLegalAddress(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalAddress());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalDepartment()!=null){
						legalRepresentativeHistory.setLegalDepartment(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalDepartment());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalDistrict()!=null){
						legalRepresentativeHistory.setLegalDistrict(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalDistrict());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalProvince()!=null){
						legalRepresentativeHistory.setLegalProvince(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalProvince());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalResidenceCountry()!=null){
						legalRepresentativeHistory.setLegalResidenceCountry(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalResidenceCountry());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getMobileNumber()!=null){
						legalRepresentativeHistory.setMobileNumber(holder.getRepresentedEntity().get(i).getLegalRepresentative().getMobileNumber());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getName()!=null){
						legalRepresentativeHistory.setName(holder.getRepresentedEntity().get(i).getLegalRepresentative().getName()); 
					}
					
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getNationality()!=null){
						legalRepresentativeHistory.setNationality(holder.getRepresentedEntity().get(i).getLegalRepresentative().getNationality());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getOfficePhoneNumber()!=null){
						legalRepresentativeHistory.setOfficePhoneNumber(holder.getRepresentedEntity().get(i).getLegalRepresentative().getOfficePhoneNumber());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalDepartment()!=null){
						legalRepresentativeHistory.setPostalDepartment(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalDepartment());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalDistrict()!=null){
						legalRepresentativeHistory.setPostalDistrict(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalDistrict());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalProvince()!=null){
						legalRepresentativeHistory.setPostalProvince(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalProvince());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalResidenceCountry()!=null){
						legalRepresentativeHistory.setPostalResidenceCountry(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalResidenceCountry());
					}
					
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalAddress()!=null){
						legalRepresentativeHistory.setPostalAddress(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPostalAddress());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getSecondDocumentNumber()!=null){
						legalRepresentativeHistory.setSecondDocumentNumber(holder.getRepresentedEntity().get(i).getLegalRepresentative().getSecondDocumentNumber());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getSecondLastName()!=null){
						legalRepresentativeHistory.setSecondLastName(holder.getRepresentedEntity().get(i).getLegalRepresentative().getSecondLastName());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getSecondNationality()!=null){
						legalRepresentativeHistory.setSecondNationality(holder.getRepresentedEntity().get(i).getLegalRepresentative().getSecondNationality());	
					}
					
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getSecondDocumentType()!=null){
						legalRepresentativeHistory.setSecondDocumentType(holder.getRepresentedEntity().get(i).getLegalRepresentative().getSecondDocumentType());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getSex()!=null){
						legalRepresentativeHistory.setSex(holder.getRepresentedEntity().get(i).getLegalRepresentative().getSex());
					}
				    
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getState()!=null){
						legalRepresentativeHistory.setStateRepreHistory(holder.getRepresentedEntity().get(i).getLegalRepresentative().getState());
					}
					
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPersonType()!=null){
						legalRepresentativeHistory.setPersonType(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPersonType());
					}
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentSource()!=null){
						legalRepresentativeHistory.setDocumentSource(holder.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentSource());
					}
					if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentIssuanceDate()!=null){
						legalRepresentativeHistory.setDocumentIssuanceDate(holder.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentIssuanceDate());
					}
					
					PepPerson auxPepPersonLegal = holderServiceBean.getPepPersonByIdLegalRepresentativeServiceBean(legalRepresentativeHistory.getIdRepresentativeHistoryPk());
					
					
					if(auxPepPersonLegal!=null){							
						legalRepresentativeHistory.setIndPEP(BooleanType.YES.getCode());
						legalRepresentativeHistory.setBeginningPeriod(auxPepPersonLegal.getBeginingPeriod());
						legalRepresentativeHistory.setEndingPeriod(auxPepPersonLegal.getEndingPeriod());
						legalRepresentativeHistory.setRole(auxPepPersonLegal.getRole());
						legalRepresentativeHistory.setCategory(auxPepPersonLegal.getCategory());
					}
					else{
						legalRepresentativeHistory.setIndPEP(BooleanType.NO.getCode());				
					}
					
					legalRepresentativeHistory.setRegistryUser(userInfo.getUserAccountSession().getUserName());
					
					legalRepresentativeHistory.setStateRepreHistory(HolderRequestStateType.REGISTERED.getCode());
					
					legalRepresentativeHistory.setRepreHistoryType(HolderRequestType.CREATION.getCode());
					
					legalRepresentativeHistory.setRegistryType(RegistryType.HISTORICAL_COPY.getCode());
					
					legalRepresentativeHistory.setRepresentativeTypeDescription(PersonType.get(holder.getRepresentedEntity().get(i).getLegalRepresentative().getPersonType()).getValue());
				
					legalRepresentativeHistory.setDocumentTypeDescription(DocumentType.get(holder.getRepresentedEntity().get(i).getLegalRepresentative().getDocumentType()).getValue());
				
					RepresentativeFileHistory representativeFileHistory;
					
					List<RepresentativeFileHistory> lstRepresentativeFileHistory = new ArrayList<RepresentativeFileHistory>();
					
					for(int j=0;j<holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalRepresentativeFile().size();j++){
						
						representativeFileHistory = new RepresentativeFileHistory();
						
						representativeFileHistory.setDescription(
								holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalRepresentativeFile().get(j).getDescription());
						
						representativeFileHistory.setDocumentType(
								holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalRepresentativeFile().get(j).getDocumentType());
						
						representativeFileHistory.setFilename(
								holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalRepresentativeFile().get(j).getFilename());
						
						
						representativeFileHistory.setFileRepresentative(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalRepresentativeFile().get(j).getFileLegalRepre());
						
						representativeFileHistory.setRepresentFileHistType(HolderReqFileHisType.CREATION.getCode());
						
						representativeFileHistory.setStateFile(HolderFileStateType.REGISTERED.getCode());
						
						representativeFileHistory.setRegistryDate(CommonsUtilities.currentDate());
						
						representativeFileHistory.setRegistryType(RegistryType.HISTORICAL_COPY.getCode());
						
						representativeFileHistory.setRegistryUser(userInfo.getUserAccountSession().getUserName());						
						
						representativeFileHistory.setLegalRepresentativeHistory(legalRepresentativeHistory);
						
						lstRepresentativeFileHistory.add(representativeFileHistory);
						
					}
					
					if(lstRepresentativeFileHistory!=null && lstRepresentativeFileHistory.size()>0){
						legalRepresentativeHistory.setRepresenteFileHistory(lstRepresentativeFileHistory);
					}
					
					lstLegalRepresentativeHistoryCopy.add(legalRepresentativeHistory);			    
				}
			  }
		
			
	     finalHolderRequest = getFileHistories(holderRequest);
		 
	     finalHolderRequest.setRequestNumber(crudDaoServiceBean.getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_MODIFICATION_RNT));
		 crudDaoServiceBean.create(finalHolderRequest);
		 
		  
		 if(registerCopyRequest(finalHolderRequest,
				 holderHistoryCopy,
				 lstHolderReqFileHistoryCopy,
				 lstLegalRepresentativeHistoryCopy)){
			 flagTransaction=true;	
		 }		 	 
		 
		 return flagTransaction;
	 }
	 
	 /**
 	 * Register copy request.
 	 *
 	 * @param holderRequest the holder request
 	 * @param holderHistoryCopy the holder history copy
 	 * @param lstHolderReqFileHistoryCopy the lst holder req file history copy
 	 * @param lstLegalRepresentativeHistoryCopy the lst legal representative history copy
 	 * @return true, if successful
 	 * @throws ServiceException the service exception
 	 */
 	public boolean registerCopyRequest(HolderRequest holderRequest,
			 HolderHistory holderHistoryCopy,
			 List<HolderReqFileHistory> lstHolderReqFileHistoryCopy,
			 List<LegalRepresentativeHistory> lstLegalRepresentativeHistoryCopy) throws ServiceException {
		 
		 boolean flagTransaction=false;
		 holderHistoryCopy.setHolderRequest(holderRequest);
    	 
		 crudDaoServiceBean.create(holderHistoryCopy);
		 
		 for(LegalRepresentativeHistory legalRepresentativeHistory : lstLegalRepresentativeHistoryCopy){
			 legalRepresentativeHistory.setHolderRequest(holderRequest);			 
		 }
		 
		 for(HolderReqFileHistory holderReqFileHistory : lstHolderReqFileHistoryCopy){
			 holderReqFileHistory.setHolderRequest(holderRequest);
		 }
		 
		 crudDaoServiceBean.saveAll(lstLegalRepresentativeHistoryCopy);
		 crudDaoServiceBean.saveAll(lstHolderReqFileHistoryCopy);

		 flagTransaction = true;
		 
		 return flagTransaction;
	 }
	 
	 /**
 	 * Register holder request history service facade.
 	 *
 	 * @param holderRequest the holder request
 	 * @return true, if successful
 	 * @throws ServiceException the service exception
 	 */
 	public boolean registerHolderRequestHistoryServiceFacade(HolderRequest holderRequest) throws ServiceException {
		 boolean flagTransaction = false;
		   
		 return flagTransaction;
	 }
	 
	 /**
 	 * Gets the list creation holder request service facade.
 	 *
 	 * @param holderFilter the holder filter
 	 * @return the list creation holder request service facade
 	 * @throws ServiceException the service exception
 	 */
 	@ProcessAuditLogger(process=BusinessProcessType.NEW_HOLDER_CREATION_REQUEST_QUERY)
	 public List<HolderRequest> getListCreationHolderRequestServiceFacade(HolderRequestTO holderFilter) throws ServiceException{
		 List<HolderRequest> list = holderServiceBean.getListHolderRequestServiceBean(holderFilter);
		 
		 
		
		 List<HolderRequest> lstHolderRequest = new ArrayList<HolderRequest>();
		 
		 for(int i=0;i<list.size();i++){
			 
			 
				 lstHolderRequest.add(list.get(i));				 
	               	
				 for(HolderHistory hH : list.get(i).getHolderHistories()){
				
					 if(hH.getRegistryType()!=null && !hH.getRegistryType().equals(RegistryType.HISTORICAL_COPY.getCode())){
						
						 lstHolderRequest.get(i).setHolderHistory(hH);
					
					 }		
				
			 	}
			
			 	if(list.get(i).getParticipant()!=null && list.get(i).getParticipant().getDescription()!=null){
			 		lstHolderRequest.get(i).getParticipant().getDescription();			 	
			 	}			
			
			 
		 }
		 
		 return lstHolderRequest;
	 }
	 
	 /**
 	 * Gets the list modification holder request service facade.
 	 *
 	 * @param holderFilter the holder filter
 	 * @return the list modification holder request service facade
 	 * @throws ServiceException the service exception
 	 */
 	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_MODIFICATION_REQUEST_QUERY)
	 public List<HolderRequest> getListModificationHolderRequestServiceFacade(HolderRequestTO holderFilter) throws ServiceException{
		 List<HolderRequest> list = holderServiceBean.getListHolderRequestServiceBean(holderFilter);
		 
		 
		
		 List<HolderRequest> lstHolderRequest = new ArrayList<HolderRequest>();
		 
		 for(int i=0;i<list.size();i++){
			 
			 
				 lstHolderRequest.add(list.get(i));				 
	               	
				 for(HolderHistory hH : list.get(i).getHolderHistories()){
				
					 if(hH.getRegistryType()!=null && !hH.getRegistryType().equals(RegistryType.HISTORICAL_COPY.getCode())){
						
						 lstHolderRequest.get(i).setHolderHistory(hH);
						 if(lstHolderRequest.get(i).getHolder()!=null){
							 lstHolderRequest.get(i).getHolder().getIdHolderPk();
						 }
					
					 }		
				
			 	}
			
			 	if(list.get(i).getParticipant()!=null && list.get(i).getParticipant().getDescription()!=null){
			 		lstHolderRequest.get(i).getParticipant().getDescription();			 	
			 	}			
			
			 
		 }
		 
		 return lstHolderRequest;
	 }
	 
	 /**
 	 * Gets the holder request service facade.
 	 *
 	 * @param idHolderRequest the id holder request
 	 * @return the holder request service facade
 	 * @throws ServiceException the service exception
 	 */
 	public HolderRequest getHolderRequestServiceFacade(Long idHolderRequest) throws ServiceException{
		HolderRequest objHolderRequest = holderServiceBean.getHolderRequestServiceBean(idHolderRequest);
		
		objHolderRequest.getHolderHistories().size();
		objHolderRequest.getLegalRepresentativeHistories().size();
		objHolderRequest.getParticipant().getIdParticipantPk();
		
		if(objHolderRequest.getHolderReqFileHistories()!=null){
			objHolderRequest.getHolderReqFileHistories().size();
		}
		
		for(int i=0;i<objHolderRequest.getLegalRepresentativeHistories().size();i++){
			if(objHolderRequest.getLegalRepresentativeHistories().get(i).getRepresenteFileHistory()!=null){
				objHolderRequest.getLegalRepresentativeHistories().get(i).getRepresenteFileHistory().size();
			}
		}
		
	    
	    return objHolderRequest;
	 }
	 
	 /**
 	 * Gets the request information service facade.
 	 *
 	 * @param holderRequest the holder request
 	 * @return the request information service facade
 	 * @throws ServiceException the service exception
 	 */
 	public HolderRequest getRequestInformationServiceFacade(HolderRequest holderRequest) throws ServiceException{
		 HolderRequest objHolderRequest = holderServiceBean.getRequestInformationServiceBean(holderRequest); 
		 
		 objHolderRequest.getHolderHistories().size();
		 objHolderRequest.getLegalRepresentativeHistories().size();
		 objHolderRequest.getParticipant().getIdParticipantPk();
		    
		 return objHolderRequest;
	 }
	 
 	public HolderRequest getHolderInformationServiceFacade(Long idHolderRequest) throws ServiceException{
		 HolderRequest holderRequest = holderServiceBean.getHolderInformationServiceBean(idHolderRequest); 
	 	 if(holderRequest.getParticipant()!=null) {
	 		holderRequest.getParticipant().getDescription();
	 	 }else {
	 		 holderRequest.setParticipant(null);
	 	 }
		 
		 return holderRequest;
	 }
 	
 	public Holder getHolderDataServiceFacade(Long idHolderPk) throws ServiceException{
		 Holder holder = holderServiceBean.getHolderServiceBean(idHolderPk); 
	    
		 return holder;
	 }
	 /**
 	 * Gets the holder block un block service facade.
 	 *
 	 * @param idHolderRequest the id holder request
 	 * @return the holder block un block service facade
 	 * @throws ServiceException the service exception
 	 */
 	public HolderRequest getHolderBlockUnBlockServiceFacade(Long idHolderRequest) throws ServiceException{
		 HolderRequest objHolderRequest = holderServiceBean.getHolderRequestServiceBean(idHolderRequest);
		 if(objHolderRequest.getHolder()!=null){
			objHolderRequest.getHolder().getIdHolderPk();
		 }
		 if(objHolderRequest.getParticipant()!=null){
			 objHolderRequest.getParticipant().getIdParticipantPk(); 
		 }
		 
		 
		 return objHolderRequest;	 
	 }
	 
	 /**
 	 * Confirm modification request holder service facade.
 	 *
 	 * @param holderRequest the holder request
 	 * @param holderHistory the holder history
 	 * @return true, if successful
 	 * @throws ServiceException the service exception
 	 */
 	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_MODIFICATION_REQUEST_CONFIRM)
	 public boolean confirmModificationRequestHolderServiceFacade(HolderRequest holderRequest, HolderHistory holderHistory)throws ServiceException{
		 boolean flagTransaction;
		 
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
	        
		 HolderRequest auxHolderRequest = new HolderRequest();
		 HolderHistory auxHolderHistory = new HolderHistory();
		 List<LegalRepresentativeHistory> lstLegalRepresentativeHistory = new ArrayList<LegalRepresentativeHistory>();
		 
		 auxHolderRequest =  getHolderRequestByIdRequestServiceFacade(holderRequest.getIdHolderRequestPk());
		 auxHolderHistory = auxHolderRequest.getHolderHistories().get(0);
		 lstLegalRepresentativeHistory=auxHolderRequest.getLegalRepresentativeHistories();
		    		 
		 LegalRepresentative legalRepresentative = new LegalRepresentative();
		 
		 
		 List<LegalRepresentativeFile> lstLegalRepresentativeFile;
		 List<HolderFile>	lstHolderFile = new ArrayList<HolderFile>();
		 Holder holder = new Holder();
		 
		 List<PepPerson> lstPepPerson = new ArrayList<PepPerson>();
		 
		 boolean exist = false;
		 
		 RepresentedEntity representedEntity = new RepresentedEntity();
		 
		 	//Disabled Represented Entity
			disableRepresentedEntitiesStateServiceFacade(holderRequest.getHolder(),loggerUser);
		 
			if(lstLegalRepresentativeHistory!=null && lstLegalRepresentativeHistory.size()>0){
			for (int i = 0; i < lstLegalRepresentativeHistory.size(); i++) {

				legalRepresentative = new LegalRepresentative();
				lstLegalRepresentativeFile =  new ArrayList<LegalRepresentativeFile>();
				
				legalRepresentative.setDocumentNumber(lstLegalRepresentativeHistory.get(i).getDocumentNumber());
				legalRepresentative.setDocumentType(lstLegalRepresentativeHistory.get(i).getDocumentType());
				legalRepresentative.setNationality(lstLegalRepresentativeHistory.get(i).getNationality());
				if(lstLegalRepresentativeHistory.get(i).getDocumentSource()!=null){
					legalRepresentative.setDocumentSource(lstLegalRepresentativeHistory.get(i).getDocumentSource());
				}
				legalRepresentative.setFirstLastName(lstLegalRepresentativeHistory.get(i).getFirstLastName());
				
				LegalRepresentative auxLR = accountsFacade.getLegalRepresentativeByDocumentTypeAndDocumentNumberServiceFacade(legalRepresentative);
				
				if(auxLR!=null){
					exist = true;
				}else{
					exist = false;
				}
				
				if(exist){
					legalRepresentative.setIdLegalRepresentativePk(auxLR.getIdLegalRepresentativePk());
					
					//Delete Legal Representative File
					accountsFacade.deleteLegalRepresentativeFileServiceFacade(legalRepresentative,loggerUser);
				}
				
				legalRepresentative.setBirthDate(lstLegalRepresentativeHistory.get(i).getBirthDate());
				legalRepresentative.setDocumentNumber(lstLegalRepresentativeHistory.get(i).getDocumentNumber());
				legalRepresentative.setDocumentType(lstLegalRepresentativeHistory.get(i).getDocumentType());
				legalRepresentative.setEconomicActivity(lstLegalRepresentativeHistory.get(i).getEconomicActivity());
				legalRepresentative.setEconomicSector(lstLegalRepresentativeHistory.get(i).getEconomicSector());
				legalRepresentative.setEmail(lstLegalRepresentativeHistory.get(i).getEmail());
				legalRepresentative.setFaxNumber(lstLegalRepresentativeHistory.get(i).getFaxNumber());
				legalRepresentative.setFirstLastName(lstLegalRepresentativeHistory.get(i).getFirstLastName());
				legalRepresentative.setFullName(lstLegalRepresentativeHistory.get(i).getFullName());
				legalRepresentative.setHomePhoneNumber(lstLegalRepresentativeHistory.get(i).getHomePhoneNumber());
				legalRepresentative.setIndResidence(lstLegalRepresentativeHistory.get(i).getIndResidence());
				legalRepresentative.setLegalAddress(lstLegalRepresentativeHistory.get(i).getLegalAddress());
				legalRepresentative.setLegalDepartment(lstLegalRepresentativeHistory.get(i).getLegalDepartment());
				legalRepresentative.setLegalDistrict(lstLegalRepresentativeHistory.get(i).getLegalDistrict());
				legalRepresentative.setLegalProvince(lstLegalRepresentativeHistory.get(i).getLegalProvince());
				legalRepresentative.setLegalResidenceCountry(lstLegalRepresentativeHistory.get(i).getLegalResidenceCountry());
				legalRepresentative.setMobileNumber(lstLegalRepresentativeHistory.get(i).getMobileNumber());
				legalRepresentative.setName(lstLegalRepresentativeHistory.get(i).getName());
				legalRepresentative.setNationality(lstLegalRepresentativeHistory.get(i).getNationality());
				legalRepresentative.setOfficePhoneNumber(lstLegalRepresentativeHistory.get(i).getOfficePhoneNumber());
				legalRepresentative.setPersonType(lstLegalRepresentativeHistory.get(i).getPersonType());
				legalRepresentative.setPostalAddress(lstLegalRepresentativeHistory.get(i).getPostalAddress());		
				legalRepresentative.setPostalDepartment(lstLegalRepresentativeHistory.get(i).getPostalDepartment());
				legalRepresentative.setPostalDistrict(lstLegalRepresentativeHistory.get(i).getPostalDistrict());
				legalRepresentative.setPostalProvince(lstLegalRepresentativeHistory.get(i).getPostalProvince());
				legalRepresentative.setPostalResidenceCountry(lstLegalRepresentativeHistory.get(i).getPostalResidenceCountry());
				legalRepresentative.setSecondDocumentNumber(lstLegalRepresentativeHistory.get(i).getSecondDocumentNumber());
				legalRepresentative.setSecondDocumentType(lstLegalRepresentativeHistory.get(i).getSecondDocumentType());
				legalRepresentative.setSecondLastName(lstLegalRepresentativeHistory.get(i).getSecondLastName());
				legalRepresentative.setSecondNationality(lstLegalRepresentativeHistory.get(i).getSecondNationality());
				legalRepresentative.setSex(lstLegalRepresentativeHistory.get(i).getSex());
				legalRepresentative.setState(LegalRepresentativeStateType.REGISTERED.getCode());
				legalRepresentative.setRegistryDate(CommonsUtilities.currentDate());
				legalRepresentative.setRegistryUser(userInfo.getUserAccountSession().getUserName());
				legalRepresentative.setRepresentativeClassAux(lstLegalRepresentativeHistory.get(i).getRepresentativeClass());
				legalRepresentative.setDocumentSource(lstLegalRepresentativeHistory.get(i).getDocumentSource());
				legalRepresentative.setDocumentIssuanceDate(lstLegalRepresentativeHistory.get(i).getDocumentIssuanceDate());
				
				if (lstLegalRepresentativeHistory.get(i).getRepresenteFileHistory().size() > 0) {
				
					for (int k = 0; k < lstLegalRepresentativeHistory.get(i).getRepresenteFileHistory().size(); k++) {
						
						LegalRepresentativeFile auxLegalRepresentativeFile = new LegalRepresentativeFile();
						
						auxLegalRepresentativeFile.setDescription(lstLegalRepresentativeHistory.get(i).getRepresenteFileHistory()
										.get(k).getDescription());
						auxLegalRepresentativeFile.setDocumentType(lstLegalRepresentativeHistory.get(i).getRepresenteFileHistory()
										.get(k).getDocumentType());
						auxLegalRepresentativeFile.setFilename(lstLegalRepresentativeHistory.get(i).getRepresenteFileHistory()
										.get(k).getFilename());						
						
						RepresentativeFileHistory representativeFileHistory = accountsFacade.getContentFileRepresentativeHistoryServiceFacade(
								lstLegalRepresentativeHistory.get(i).getRepresenteFileHistory().get(k).getIdRepresentFileHisPk());
						
						if(representativeFileHistory!=null){
							representativeFileHistory.getFileRepresentative();
						
							auxLegalRepresentativeFile.setFileLegalRepre(representativeFileHistory.getFileRepresentative());
						//	auxLegalRepresentativeFile.setIdLegalRepreFilePk(representativeFileHistory.getIdRepresentFileHisPk());
						}
						
						auxLegalRepresentativeFile.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST
										.getCode());
						auxLegalRepresentativeFile.setStateFile(HolderFileStateType.REGISTERED
										.getCode());
						auxLegalRepresentativeFile.setLegalRepreFileType(HolderReqFileHisType.CREATION
										.getCode());
						auxLegalRepresentativeFile.setLegalRepresentative(legalRepresentative);
						
						auxLegalRepresentativeFile.setRegistryUser(userInfo.getUserAccountSession().getUserName());
						auxLegalRepresentativeFile.setRegistryDate(CommonsUtilities.currentDate());
						
											
						lstLegalRepresentativeFile.add(auxLegalRepresentativeFile);
					}

				}

				legalRepresentative.setLegalRepresentativeFile(lstLegalRepresentativeFile);
				if(exist){
				crudDaoServiceBean.update(legalRepresentative);
				}
				else{
					crudDaoServiceBean.create(legalRepresentative);
				}
				
				representedEntity = new RepresentedEntity();
				representedEntity.setHolder(holderRequest.getHolder());
//				representedEntity.setIssuer(auxHolderRequest.getHolder().getParticipant().getIssuer());
//				representedEntity.setParticipant(auxHolderRequest.getHolder().getParticipant());
				representedEntity.setLegalRepresentative(legalRepresentative);	
				representedEntity.setRepresentativeClass(lstLegalRepresentativeHistory.get(i).getRepresentativeClass());
				representedEntity.setStateRepresented(RepresentativeEntityStateType.REGISTERED.getCode());

				representedEntity.setRegistryDate(CommonsUtilities.currentDate());
				representedEntity.setRegistryUser(userInfo.getUserAccountSession().getUserName());

				crudDaoServiceBean.create(representedEntity);
				
				
				
				if ((lstLegalRepresentativeHistory.get(i).getIndPEP() != null)
						&& (lstLegalRepresentativeHistory.get(i)
								.getIndPEP().equals(BooleanType.YES
								.getCode()))) {
					
					
					LegalRepresentative auxlgR = new LegalRepresentative();
					auxlgR.setDocumentType(lstLegalRepresentativeHistory.get(i).getDocumentType());
					auxlgR.setDocumentNumber(lstLegalRepresentativeHistory.get(i).getDocumentNumber());
					auxlgR.setNationality(lstLegalRepresentativeHistory.get(i).getNationality());
					
					auxlgR = accountsFacade.getLegalRepresentativeByDocumentTypeAndDocumentNumberServiceFacade(auxlgR);
					
					PepPerson auxPepPerson = new PepPerson();
					
					if(auxlgR!=null){
					auxPepPerson = getPepPersonByIdLegalRepresentativeServiceFacade(auxlgR.getIdLegalRepresentativePk());
					}
					
					PepPerson pepPerson = new PepPerson();
					if(auxPepPerson!=null && auxPepPerson.getIdPepPersonPk()!=null){
						pepPerson.setIdPepPersonPk(auxPepPerson.getIdPepPersonPk());
					}
					pepPerson.setNotificationDate(CommonsUtilities.currentDate());
					pepPerson.setBeginingPeriod(lstLegalRepresentativeHistory.get(i).getBeginningPeriod());
					pepPerson.setEndingPeriod(lstLegalRepresentativeHistory.get(i).getEndingPeriod());
					pepPerson.setCategory(lstLegalRepresentativeHistory.get(i).getCategory());
					pepPerson.setRole(lstLegalRepresentativeHistory.get(i).getRole());
					pepPerson.setComments(lstLegalRepresentativeHistory.get(i).getComments());
					pepPerson.setRegistryDate(CommonsUtilities.currentDate());
					pepPerson.setRegistryUser(userInfo.getUserAccountSession().getUserName());
					pepPerson.setLegalRepresentative(legalRepresentative);
					pepPerson.setDocumentNumber(lstLegalRepresentativeHistory.get(i).getDocumentNumber());
					pepPerson.setDocumentType(lstLegalRepresentativeHistory.get(i).getDocumentType());
					GeographicLocation geo = new GeographicLocation();
					geo.setIdGeographicLocationPk(lstLegalRepresentativeHistory.get(i).getLegalResidenceCountry());
					pepPerson.setGeographicLocation(geo);
					pepPerson.setClassification(PepPersonClassificationType.HIGH.getCode());
					pepPerson.setExpeditionPlace(geo);
					
					
					if(lstLegalRepresentativeHistory.get(i).getPersonType().equals(PersonType.NATURAL.getCode())){
						String lastName="";
						pepPerson.setFirstName(lstLegalRepresentativeHistory.get(i).getName());
						if(lstLegalRepresentativeHistory.get(i).getSecondLastName()==null){
							lastName = lstLegalRepresentativeHistory.get(i).getFirstLastName();
						}
						else{
							lastName = lstLegalRepresentativeHistory.get(i).getFirstLastName()+GeneralConstants.BLANK_SPACE+lstLegalRepresentativeHistory.get(i).getSecondLastName();
						}
						pepPerson.setLastName(lastName);
					}
					else if(lstLegalRepresentativeHistory.get(i).getPersonType().equals(PersonType.JURIDIC.getCode())){
						pepPerson.setFirstName(lstLegalRepresentativeHistory.get(i).getFullName());
					}
					

					lstPepPerson.add(pepPerson);
				}
			  }
			}

			

			if ((auxHolderHistory.getIndPEP() != null)
					&& auxHolderHistory.getIndPEP().equals(
							BooleanType.YES.getCode())) {
				PepPerson pepPerson = new PepPerson();
				
				PepPerson auxPepPerson = new PepPerson();
				auxPepPerson = getPepPersonByIdHolderServiceFacade(holderRequest.getHolder().getIdHolderPk());
				
				if(auxPepPerson!=null){
					pepPerson.setIdPepPersonPk(auxPepPerson.getIdPepPersonPk());
				}
				
				pepPerson.setNotificationDate(CommonsUtilities.currentDate());
				pepPerson.setBeginingPeriod(auxHolderHistory.getBeginningPeriod());
				pepPerson.setEndingPeriod(auxHolderHistory.getEndingPeriod());
				pepPerson.setCategory(auxHolderHistory.getCategory());
				pepPerson.setRole(auxHolderHistory.getRole());
				pepPerson.setComments(auxHolderHistory.getComments());
				pepPerson.setRegistryDate(CommonsUtilities.currentDate());
				pepPerson.setRegistryUser(userInfo.getUserAccountSession().getUserName());
				pepPerson.setHolder(holderRequest.getHolder());
				pepPerson.setDocumentNumber(auxHolderHistory.getDocumentNumber());
				pepPerson.setDocumentType(auxHolderHistory.getDocumentType());
				pepPerson.setGrade(auxHolderHistory.getGrade());
				pepPerson.setPepRelatedName(auxHolderHistory.getPepRelatedName());
				GeographicLocation geo = new GeographicLocation();
				geo.setIdGeographicLocationPk(auxHolderHistory.getLegalResidenceCountry());
				pepPerson.setGeographicLocation(geo);
				pepPerson.setClassification(PepPersonClassificationType.HIGH.getCode());
				pepPerson.setExpeditionPlace(geo);
				
				if(auxHolderHistory.getHolderType().equals(PersonType.NATURAL.getCode())){
					String lastName="";
					pepPerson.setFirstName(auxHolderHistory.getName());
					
					if(auxHolderHistory.getSecondLastName()==null){
						lastName = auxHolderHistory.getFirstLastName();
					}
					else{
						lastName = auxHolderHistory.getFirstLastName()+GeneralConstants.BLANK_SPACE+auxHolderHistory.getSecondLastName();
					}
					
					pepPerson.setLastName(lastName);
				}
				if(auxHolderHistory.getHolderType().equals(PersonType.JURIDIC.getCode())){
					pepPerson.setFirstName(auxHolderHistory.getFullName());
				}

				lstPepPerson.add(pepPerson);
			}

			holder.setIdHolderPk(holderRequest.getHolder().getIdHolderPk());
			Holder originalHolder = holderServiceBean.getHolderServiceBean(holder.getIdHolderPk());
			holder.setBirthDate(auxHolderHistory.getBirthDate());
			holder.setDocumentNumber(auxHolderHistory.getDocumentNumber());
			holder.setDocumentType(auxHolderHistory.getDocumentType());
			holder.setEconomicActivity(auxHolderHistory.getEconomicActivity());
			holder.setInvestorType(auxHolderHistory.getInvestorType());
			holder.setEconomicSector(auxHolderHistory.getEconomicSector());
			holder.setEmail(auxHolderHistory.getEmail());
			holder.setFaxNumber(auxHolderHistory.getFaxNumber());
			holder.setFirstLastName(auxHolderHistory.getFirstLastName());
			
			holder.setFundAdministrator(auxHolderHistory.getFundAdministrator());
			holder.setFundType(auxHolderHistory.getFundType());
			holder.setTransferNumber(auxHolderHistory.getTransferNumber());
			holder.setRelatedCui(auxHolderHistory.getRelatedCui());
			holder.setMnemonic(auxHolderHistory.getMnemonic());
			
			String descriptionHolder="";
			if(auxHolderHistory.getHolderType().equals(PersonType.NATURAL.getCode())){
				if(auxHolderHistory.getSecondLastName()!=null){
				    descriptionHolder=auxHolderHistory.getFirstLastName()+GeneralConstants.BLANK_SPACE+auxHolderHistory.getSecondLastName()+GeneralConstants.STR_COMMA+auxHolderHistory.getName();
				}
				else{
					descriptionHolder=auxHolderHistory.getFirstLastName()+GeneralConstants.STR_COMMA+auxHolderHistory.getName();
				}
				
				holder.setFullName(descriptionHolder.trim());
			}else{			
			holder.setFullName(auxHolderHistory.getFullName());
			}
			
			holder.setHolderType(auxHolderHistory.getHolderType());
			holder.setHomePhoneNumber(auxHolderHistory.getHomePhoneNumber());
			holder.setIndDisabled(auxHolderHistory.getIndDisabled());
			holder.setIndMinor(auxHolderHistory.getIndMinor());
			holder.setIndResidence(auxHolderHistory.getIndResidence());
			holder.setJuridicClass(auxHolderHistory.getJuridicClass());
			holder.setLegalAddress(auxHolderHistory.getLegalAddress());
			holder.setLegalDepartment(auxHolderHistory.getLegalDepartment());
			holder.setLegalDistrict(auxHolderHistory.getLegalDistrict());
			holder.setLegalProvince(auxHolderHistory.getLegalProvince());
			holder.setLegalResidenceCountry(auxHolderHistory.getLegalResidenceCountry());
			holder.setMobileNumber(auxHolderHistory.getMobileNumber());
			holder.setName(auxHolderHistory.getName());
			holder.setNationality(auxHolderHistory.getNationality());
			holder.setOfficePhoneNumber(auxHolderHistory.getOfficePhoneNumber());
			holder.setPostalAddress(auxHolderHistory.getPostalAddress());
			holder.setPostalDepartment(auxHolderHistory.getPostalDepartment());
			holder.setPostalDistrict(auxHolderHistory.getPostalDistrict());
			holder.setPostalProvince(auxHolderHistory.getPostalProvince());
			holder.setPostalResidenceCountry(auxHolderHistory.getPostalResidenceCountry());
			holder.setSecondDocumentNumber(auxHolderHistory.getSecondDocumentNumber());
			holder.setSecondDocumentType(auxHolderHistory.getSecondDocumentType());
			holder.setSecondLastName(auxHolderHistory.getSecondLastName());
			holder.setSecondNationality(auxHolderHistory.getSecondNationality());
			holder.setSex(auxHolderHistory.getSex());
			holder.setStateHolder(HolderStateType.REGISTERED.getCode());
			holder.setIndFatca(auxHolderHistory.getIndFatca());

//			holder.setRegistryDate(CommonsUtilities.currentDate());
//			holder.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			holder.setRegistryDate(originalHolder.getRegistryDate());
			holder.setRegistryUser(originalHolder.getRegistryUser());
			holder.setParticipant(auxHolderRequest.getHolder().getParticipant());
			holder.setDocumentSource(auxHolderHistory.getDocumentSource());
			holder.setDocumentIssuanceDate(auxHolderHistory.getDocumentIssuanceDate());	
			
			holder.setMarriedLastName(auxHolderHistory.getMarriedLastName());
		    holder.setCivilStatus(auxHolderHistory.getCivilStatus());
			holder.setMainActivity(auxHolderHistory.getMainActivity());
		    holder.setJobSourceBusinessName(auxHolderHistory.getJobSourceBusinessName());
		    holder.setJobDateAdmission(auxHolderHistory.getJobDateAdmission());
		    holder.setAppointment(auxHolderHistory.getAppointment());
		    holder.setJobAddress(auxHolderHistory.getJobAddress());
		    holder.setTotalIncome(auxHolderHistory.getTotalIncome());
		    holder.setPerRefFullName(auxHolderHistory.getPerRefFullName());
		    holder.setPerRefLandLine(auxHolderHistory.getPerRefLandLine());
		    holder.setPerRefCellPhone(auxHolderHistory.getPerRefCellPhone());
		    holder.setComerRefBusinessName(auxHolderHistory.getComerRefBusinessName());
		    holder.setComerRefLandLine(auxHolderHistory.getComerRefLandLine());
		    holder.setEntityPublicAppointment(auxHolderHistory.getEntityPublicAppointment());
		    holder.setNitNaturalPerson(auxHolderHistory.getNitNaturalPerson());
		    holder.setWebsite(auxHolderHistory.getWebsite());
		    holder.setFundCompanyEnrollment(auxHolderHistory.getFundCompanyEnrollment());
		    holder.setConstitutionDate(auxHolderHistory.getConstitutionDate());
		    
		    holder.setFilePhoto(auxHolderHistory.getFilePhoto());
		    holder.setFilePhotoName(auxHolderHistory.getFilePhotoName());
		    holder.setFileSigning(auxHolderHistory.getFileSigning());
		    holder.setIndCanNegotiate(auxHolderHistory.getIndCanNegotiate());
		    holder.setIndSirtexNeg(auxHolderHistory.getIndSirtexNeg());
		    
		    //delete Holder File
			deleteHolderFileServiceFacade(holder,loggerUser);
			
			if (auxHolderRequest.getHolderReqFileHistories()!=null && auxHolderRequest.getHolderReqFileHistories().size() > 0) {

				for (int i = 0; i < auxHolderRequest
						.getHolderReqFileHistories().size(); i++) {
					HolderFile auxHolderFile = new HolderFile();
					auxHolderFile.setDescription(auxHolderRequest.getHolderReqFileHistories().get(i).getDescription());
					auxHolderFile.setDocumentType(auxHolderRequest.getHolderReqFileHistories().get(i).getDocumentType());
					
					HolderReqFileHistory holderReqFileHistory = holderServiceBean.getContentFileHolderHistoryServiceBean(auxHolderRequest.getIdHolderRequestPk(),
							auxHolderRequest.getHolderReqFileHistories().get(i).getDocumentType(),false);
					
					holderReqFileHistory.getFileHolderReq();					
					auxHolderFile.setFileHolder(holderReqFileHistory.getFileHolderReq());					
					
					auxHolderFile.setFilename(auxHolderRequest.getHolderReqFileHistories().get(i).getFilename());
					auxHolderFile.setHolder(holderRequest.getHolder());
					auxHolderFile.setHolderFileType(HolderReqFileHisType.CREATION.getCode());
					auxHolderFile.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
					auxHolderFile.setStateFile(HolderFileStateType.REGISTERED.getCode());
					auxHolderFile.setRegistryUser(userInfo.getUserAccountSession().getUserName());
					auxHolderFile.setRegistryDate(CommonsUtilities.currentDate());
					
					if(auxHolderFile.getDocumentType() != 2847) {
						lstHolderFile.add(auxHolderFile);
					}
				}
			}

			if(lstHolderFile!=null && lstHolderFile.size()>0){
				holder.setHolderFile(lstHolderFile);
			}

			if (lstPepPerson.size() > 0) {
				holder.setPepPerson(lstPepPerson);
			}
			
			crudDaoServiceBean.update(holder);
			
			HolderRequest FinalholderRequest = new HolderRequest();
			HolderHistory FinalholderHistory = new HolderHistory();
			List<LegalRepresentativeHistory> FinalLstLegalRepresentativeHistory = new ArrayList<LegalRepresentativeHistory>();
		    List<HolderHistory> lstHolderHistory = new ArrayList<HolderHistory>();
			
		    FinalholderRequest = holderRequest;
		    FinalholderHistory = holderHistory;
			
		    FinalholderRequest.setStateHolderRequest(HolderRequestStateType.CONFIRMED
					.getCode());
		    FinalholderRequest.setConfirmDate(CommonsUtilities.currentDate());
		    FinalholderRequest.setConfirmUser(userInfo.getUserAccountSession().getUserName());
		    FinalholderHistory.setStateHolderHistory(HolderRequestStateType.CONFIRMED.getCode());
			
		    FinalLstLegalRepresentativeHistory = FinalholderRequest.getLegalRepresentativeHistories();

			if(FinalLstLegalRepresentativeHistory!=null && FinalLstLegalRepresentativeHistory.size()>0){
				for (int i = 0; i < FinalLstLegalRepresentativeHistory.size(); i++) {
					FinalLstLegalRepresentativeHistory.get(i).setStateRepreHistory(HolderRequestStateType.CONFIRMED.getCode());
				}
			}
			
			lstHolderHistory.add(FinalholderHistory);

			FinalholderRequest.setHolderHistories(lstHolderHistory);
			
			if(FinalLstLegalRepresentativeHistory!=null && FinalLstLegalRepresentativeHistory.size()>0){
				FinalholderRequest.setLegalRepresentativeHistories(FinalLstLegalRepresentativeHistory);
			}

			flagTransaction  = updateStateConfirmModificationHolderRequestServiceFacade(FinalholderRequest, null,ViewOperationsType.CONFIRM.getCode());
		
	 
		 
		
		 return flagTransaction;
	 }
	 
	 /**
 	 * Disable represented entities state service facade.
 	 *
 	 * @param holder the holder
 	 * @param loggerUser the logger user
 	 * @return the int
 	 * @throws ServiceException the service exception
 	 */
 	public int disableRepresentedEntitiesStateServiceFacade(Holder holder, LoggerUser loggerUser)throws ServiceException{
		return holderServiceBean.disableRepresentedEntitiesStateServiceBean(holder,loggerUser);
	 }
	 
	 /**
 	 * Delete holder file service facade.
 	 *
 	 * @param holder the holder
 	 * @param loggerUser the logger user
 	 * @return the int
 	 * @throws ServiceException the service exception
 	 */
 	public int deleteHolderFileServiceFacade(Holder holder, LoggerUser loggerUser)throws ServiceException{
			return holderServiceBean.deleteHolderFileServiceBean(holder,loggerUser);
     }
	 
	 /**
 	 * Update state confirm register holder request service facade.
 	 *
 	 * @param holderRequest the holder request
 	 * @param holder the holder
 	 * @param viewOperationsType the view operations type
 	 * @return true, if successful
 	 * @throws ServiceException the service exception
 	 */
 	public boolean updateStateConfirmRegisterHolderRequestServiceFacade(HolderRequest holderRequest,Holder holder,Integer viewOperationsType) throws ServiceException{
		 boolean flagTransaction=false;
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		 if(viewOperationsType.equals(ViewOperationsType.CONFIRM.getCode())){
			 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		 }
		 
		 	 if(holderRequest!=null){
				 crudDaoServiceBean.update(holderRequest);
			 }
			 if(holder!=null){
				 crudDaoServiceBean.update(holder);
			 }
			 flagTransaction=true;
		
		return flagTransaction;
     }
	 
	 /**
 	 * Update state confirm modification holder request service facade.
 	 *
 	 * @param holderRequest the holder request
 	 * @param holder the holder
 	 * @param viewOperationsType the view operations type
 	 * @return true, if successful
 	 * @throws ServiceException the service exception
 	 */
 	public boolean updateStateConfirmModificationHolderRequestServiceFacade(HolderRequest holderRequest,Holder holder,Integer viewOperationsType) throws ServiceException{
		 boolean flagTransaction=false;
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		 if(viewOperationsType.equals(ViewOperationsType.CONFIRM.getCode())){
			 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		 }
		 
		 	 if(holderRequest!=null){
				 crudDaoServiceBean.update(holderRequest);
			 }
			 if(holder!=null){
				 crudDaoServiceBean.update(holder);
			 }
			 flagTransaction=true;
		
		return flagTransaction;
     }
	 
	 /**
 	 * Update state confirm block holder request service facade.
 	 *
 	 * @param holderRequest the holder request
 	 * @param holder the holder
 	 * @param viewOperationsType the view operations type
 	 * @return true, if successful
 	 * @throws ServiceException the service exception
 	 */
 	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_BLOCK_CONFIRM)
	 public boolean updateStateConfirmBlockHolderRequestServiceFacade(HolderRequest holderRequest,Holder holder,Integer viewOperationsType) throws ServiceException{
		 boolean flagTransaction=false;
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		 if(viewOperationsType.equals(ViewOperationsType.CONFIRM.getCode())){
			 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		 }
		 List<HolderFile>	lstHolderFile = new ArrayList<HolderFile>();
		 if (holderRequest.getHolderReqFileHistories()!=null && holderRequest.getHolderReqFileHistories().size() > 0) {

				for (int i = 0; i < holderRequest
						.getHolderReqFileHistories().size(); i++) {
					HolderFile auxHolderFile = new HolderFile();
					auxHolderFile.setDescription(holderRequest.getHolderReqFileHistories().get(i).getDescription());
					auxHolderFile.setDocumentType(holderRequest.getHolderReqFileHistories().get(i).getDocumentType());
					
					HolderReqFileHistory holderReqFileHistory = holderServiceBean.getContentFileHolderHistoryServiceBean(holderRequest.getIdHolderRequestPk(),
							holderRequest.getHolderReqFileHistories().get(i).getDocumentType(),false);
					
					holderReqFileHistory.getFileHolderReq();					
					auxHolderFile.setFileHolder(holderReqFileHistory.getFileHolderReq());					
					
					auxHolderFile.setFilename(holderRequest.getHolderReqFileHistories().get(i).getFilename());
					auxHolderFile.setHolder(holder);
					auxHolderFile.setHolderFileType(HolderReqFileHisType.CREATION.getCode());
					auxHolderFile.setRegistryDate(CommonsUtilities.currentDate());
					auxHolderFile.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
					auxHolderFile.setStateFile(HolderFileStateType.REGISTERED.getCode());
					auxHolderFile.setRegistryUser(loggerUser.getUserName());
					lstHolderFile.add(auxHolderFile);
					
				}
				
		 }
		 
		 if(lstHolderFile!=null && lstHolderFile.size()>0){
				holder.setHolderFile(lstHolderFile);
		 }
		 
		 holderRequest.setHolder(holder);
		 
		 	 if(holderRequest!=null){
				 crudDaoServiceBean.update(holderRequest);
			 }
			 if(holder!=null){
				 crudDaoServiceBean.update(holder);
			 }
			 flagTransaction=true;
		
		return flagTransaction;
     }
	 
	 private HolderReqFileHistory getContentFileHolderHistoryServiceBean(Long idHolderRequestPk, Integer documentType,
			boolean b) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
 	 * Update state confirm un block holder request service facade.
 	 *
 	 * @param holderRequest the holder request
 	 * @param holder the holder
 	 * @param viewOperationsType the view operations type
 	 * @return true, if successful
 	 * @throws ServiceException the service exception
 	 */
 	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_UNBLOCK_CONFIRM)
	 public boolean updateStateConfirmUnBlockHolderRequestServiceFacade(HolderRequest holderRequest,Holder holder,Integer viewOperationsType) throws ServiceException{
		 boolean flagTransaction=false;
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		 if(viewOperationsType.equals(ViewOperationsType.CONFIRM.getCode())){
			 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		 }
		 
		 	 if(holderRequest!=null){
				 crudDaoServiceBean.update(holderRequest);
			 }
			 if(holder!=null){
				 crudDaoServiceBean.update(holder);
			 }
			 flagTransaction=true;
		
		return flagTransaction;
     }
	 
	 /**
 	 * Update state approve register holder request service facade.
 	 *
 	 * @param holderRequest the holder request
 	 * @param holder the holder
 	 * @param viewOperationsType the view operations type
 	 * @return true, if successful
 	 * @throws ServiceException the service exception
 	 */
 	@ProcessAuditLogger(process=BusinessProcessType.NEW_HOLDER_CREATION_REQUEST_APPROVE)
	 public boolean updateStateApproveRegisterHolderRequestServiceFacade(HolderRequest holderRequest,Holder holder,Integer viewOperationsType) throws ServiceException{
		 boolean flagTransaction=false;
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		 if(viewOperationsType.equals(ViewOperationsType.APPROVE.getCode())){
			 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
		 }
		 
	   		 if(holderRequest!=null){
				 crudDaoServiceBean.update(holderRequest);
			 }
			 if(holder!=null){
				 crudDaoServiceBean.update(holder);
			 }
			 flagTransaction=true;
		
		return flagTransaction;
     }
	 
	 /**
 	 * Update state approve modification holder request service facade.
 	 *
 	 * @param holderRequest the holder request
 	 * @param holder the holder
 	 * @param viewOperationsType the view operations type
 	 * @return true, if successful
 	 * @throws ServiceException the service exception
 	 */
 	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_MODIFICATION_REQUEST_APPROVE)
	 public boolean updateStateApproveModificationHolderRequestServiceFacade(HolderRequest holderRequest,Holder holder,Integer viewOperationsType) throws ServiceException{
		 boolean flagTransaction=false;
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		 if(viewOperationsType.equals(ViewOperationsType.APPROVE.getCode())){
			 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
		 }
		 
	   		 if(holderRequest!=null){
				 crudDaoServiceBean.update(holderRequest);
			 }
			 if(holder!=null){
				 crudDaoServiceBean.update(holder);
			 }
			 flagTransaction=true;
		
		return flagTransaction;
     }
	 
	 /**
 	 * Update state approve block holder request service facade.
 	 *
 	 * @param holderRequest the holder request
 	 * @param holder the holder
 	 * @param viewOperationsType the view operations type
 	 * @return true, if successful
 	 * @throws ServiceException the service exception
 	 */
 	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_BLOCK_APPROVE)
	 public boolean updateStateApproveBlockHolderRequestServiceFacade(HolderRequest holderRequest,Holder holder,Integer viewOperationsType) throws ServiceException{
		 boolean flagTransaction=false;
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		 if(viewOperationsType.equals(ViewOperationsType.APPROVE.getCode())){
			 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
		 }
		 
	   		 if(holderRequest!=null){
				 crudDaoServiceBean.update(holderRequest);
			 }
			 if(holder!=null){
				 crudDaoServiceBean.update(holder);
			 }
			 flagTransaction=true;
		
		return flagTransaction;
     }
	 
	 /**
 	 * Update state approve un block holder request service facade.
 	 *
 	 * @param holderRequest the holder request
 	 * @param holder the holder
 	 * @param viewOperationsType the view operations type
 	 * @return true, if successful
 	 * @throws ServiceException the service exception
 	 */
 	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_UNBLOCK_APPROVE)
	 public boolean updateStateApproveUnBlockHolderRequestServiceFacade(HolderRequest holderRequest,Holder holder,Integer viewOperationsType) throws ServiceException{
		 boolean flagTransaction=false;
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		 if(viewOperationsType.equals(ViewOperationsType.APPROVE.getCode())){
			 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
		 }
		 
	   		 if(holderRequest!=null){
				 crudDaoServiceBean.update(holderRequest);
			 }
			 if(holder!=null){
				 crudDaoServiceBean.update(holder);
			 }
			 flagTransaction=true;
		
		return flagTransaction;
     }
	 
	 /**
 	 * Update state anulate register holder request service facade.
 	 *
 	 * @param holderRequest the holder request
 	 * @param holder the holder
 	 * @param viewOperationsType the view operations type
 	 * @return true, if successful
 	 * @throws ServiceException the service exception
 	 */
 	@ProcessAuditLogger(process=BusinessProcessType.NEW_HOLDER_CREATION_REQUEST_CANCEL)
	 public boolean updateStateAnulateRegisterHolderRequestServiceFacade(HolderRequest holderRequest,Holder holder,Integer viewOperationsType) throws ServiceException{
		 boolean flagTransaction=false;
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		 if(viewOperationsType.equals(ViewOperationsType.ANULATE.getCode())){
			 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
		 }
		 
		     if(holderRequest!=null){
				 crudDaoServiceBean.update(holderRequest);
			 }
			 if(holder!=null){
				 crudDaoServiceBean.update(holder);
			 }
			 flagTransaction=true;
		
		return flagTransaction;
     }
	 
	 /**
 	 * Update state anulate modification holder request service facade.
 	 *
 	 * @param holderRequest the holder request
 	 * @param holder the holder
 	 * @param viewOperationsType the view operations type
 	 * @return true, if successful
 	 * @throws ServiceException the service exception
 	 */
 	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_MODIFICATION_REQUEST_CANCEL)
	 public boolean updateStateAnulateModificationHolderRequestServiceFacade(HolderRequest holderRequest,Holder holder,Integer viewOperationsType) throws ServiceException{
		 boolean flagTransaction=false;
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		 if(viewOperationsType.equals(ViewOperationsType.ANULATE.getCode())){
			 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
		 }
		 
		     if(holderRequest!=null){
				 crudDaoServiceBean.update(holderRequest);
			 }
			 if(holder!=null){
				 crudDaoServiceBean.update(holder);
			 }
			 flagTransaction=true;
		
		return flagTransaction;
     }
	 
	 /**
 	 * Update state anulate block holder request service facade.
 	 *
 	 * @param holderRequest the holder request
 	 * @param holder the holder
 	 * @param viewOperationsType the view operations type
 	 * @return true, if successful
 	 * @throws ServiceException the service exception
 	 */
 	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_BLOCK_CANCEL)
	 public boolean updateStateAnulateBlockHolderRequestServiceFacade(HolderRequest holderRequest,Holder holder,Integer viewOperationsType) throws ServiceException{
		 boolean flagTransaction=false;
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		 if(viewOperationsType.equals(ViewOperationsType.ANULATE.getCode())){
			 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
		 }
		 
		     if(holderRequest!=null){
				 crudDaoServiceBean.update(holderRequest);
			 }
			 if(holder!=null){
				 crudDaoServiceBean.update(holder);
			 }
			 flagTransaction=true;
		
		return flagTransaction;
     }
	 
	 /**
 	 * Update state anulate un block holder request service facade.
 	 *
 	 * @param holderRequest the holder request
 	 * @param holder the holder
 	 * @param viewOperationsType the view operations type
 	 * @return true, if successful
 	 * @throws ServiceException the service exception
 	 */
 	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_UNBLOCK_CANCEL)
	 public boolean updateStateAnulateUnBlockHolderRequestServiceFacade(HolderRequest holderRequest,Holder holder,Integer viewOperationsType) throws ServiceException{
		 boolean flagTransaction=false;
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		 if(viewOperationsType.equals(ViewOperationsType.ANULATE.getCode())){
			 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
		 }
		 
		     if(holderRequest!=null){
				 crudDaoServiceBean.update(holderRequest);
			 }
			 if(holder!=null){
				 crudDaoServiceBean.update(holder);
			 }
			 flagTransaction=true;
		
		return flagTransaction;
     }
	 
	 /**
 	 * Update state reject register holder request service facade.
 	 *
 	 * @param holderRequest the holder request
 	 * @param holder the holder
 	 * @param viewOperationsType the view operations type
 	 * @return true, if successful
 	 * @throws ServiceException the service exception
 	 */
 	@ProcessAuditLogger(process=BusinessProcessType.NEW_HOLDER_CREATION_REQUEST_REJECT)
	 public boolean updateStateRejectRegisterHolderRequestServiceFacade(HolderRequest holderRequest,Holder holder,Integer viewOperationsType) throws ServiceException{
		 boolean flagTransaction=false;
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		 
		 if(viewOperationsType.equals(ViewOperationsType.REJECT.getCode())){
			 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		 }
	     
		
		     if(holderRequest!=null){
				 crudDaoServiceBean.update(holderRequest);
			 }
			 if(holder!=null){
				 crudDaoServiceBean.update(holder);
			 }
			 flagTransaction=true;
		
		return flagTransaction;
     }
	 
	 /**
 	 * Update state reject modification holder request service facade.
 	 *
 	 * @param holderRequest the holder request
 	 * @param holder the holder
 	 * @param viewOperationsType the view operations type
 	 * @return true, if successful
 	 * @throws ServiceException the service exception
 	 */
 	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_MODIFICATION_REQUEST_REJECT)
	 public boolean updateStateRejectModificationHolderRequestServiceFacade(HolderRequest holderRequest,Holder holder,Integer viewOperationsType) throws ServiceException{
		 boolean flagTransaction=false;
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		 
		 if(viewOperationsType.equals(ViewOperationsType.REJECT.getCode())){
			 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		 }
	     
		
		     if(holderRequest!=null){
				 crudDaoServiceBean.update(holderRequest);
			 }
			 if(holder!=null){
				 crudDaoServiceBean.update(holder);
			 }
			 flagTransaction=true;
		
		return flagTransaction;
     }
	 
	 /**
 	 * Update state reject block holder request service facade.
 	 *
 	 * @param holderRequest the holder request
 	 * @param holder the holder
 	 * @param viewOperationsType the view operations type
 	 * @return true, if successful
 	 * @throws ServiceException the service exception
 	 */
 	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_BLOCK_REJECT)
	 public boolean updateStateRejectBlockHolderRequestServiceFacade(HolderRequest holderRequest,Holder holder,Integer viewOperationsType) throws ServiceException{
		 boolean flagTransaction=false;
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		 
		 if(viewOperationsType.equals(ViewOperationsType.REJECT.getCode())){
			 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		 }
	     
		
		     if(holderRequest!=null){
				 crudDaoServiceBean.update(holderRequest);
			 }
			 if(holder!=null){
				 crudDaoServiceBean.update(holder);
			 }
			 flagTransaction=true;
		
		return flagTransaction;
     }
	 
	 /**
 	 * Update state reject un block holder request service facade.
 	 *
 	 * @param holderRequest the holder request
 	 * @param holder the holder
 	 * @param viewOperationsType the view operations type
 	 * @return true, if successful
 	 * @throws ServiceException the service exception
 	 */
 	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_UNBLOCK_REJECT)
	 public boolean updateStateRejectUnBlockHolderRequestServiceFacade(HolderRequest holderRequest,Holder holder,Integer viewOperationsType) throws ServiceException{
		 boolean flagTransaction=false;
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		 
		 if(viewOperationsType.equals(ViewOperationsType.REJECT.getCode())){
			 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		 }
	     
		
		     if(holderRequest!=null){
				 crudDaoServiceBean.update(holderRequest);
			 }
			 if(holder!=null){
				 crudDaoServiceBean.update(holder);
			 }
			 flagTransaction=true;
		
		return flagTransaction;
     }
	 
	 /**
 	 * Confirm request holder service facade.
 	 *
 	 * @param holderRequest the holder request
 	 * @param holderHistory the holder history
 	 * @return the holder
 	 * @throws ServiceException the service exception
 	 */
 	@ProcessAuditLogger(process=BusinessProcessType.NEW_HOLDER_CREATION_REQUEST_CONFIRM)
	 public Holder confirmRequestHolderServiceFacade(HolderRequest holderRequest,HolderHistory holderHistory)throws ServiceException{
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		 return holderServiceBean.confirmHolderRequestServiceBean(holderRequest, holderHistory, loggerUser);
	 }	
	 
	 
	
	 
     /**
      * Register holder service facade.
      *
      * @param holder the holder
      * @return true, if successful
      * @throws ServiceException the service exception
      */
     public boolean registerHolderServiceFacade(Holder holder) throws ServiceException {
		 
		 boolean flagTransaction;	
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		 
    	 crudDaoServiceBean.create(holder);
		 flagTransaction=true;
		 
		 return flagTransaction;
	 }
     
     /**
      * Gets the holder service facade.
      *
      * @param idHolderPk the id holder pk
      * @return the holder service facade
      * @throws ServiceException the service exception
      */
     public Holder getHolderServiceFacade(Long idHolderPk) throws ServiceException{
       Holder holder =   holderServiceBean.getHolderServiceBean(idHolderPk);
       
       List<RepresentedEntity> lstRepresentedEntity;
       lstRepresentedEntity = holder.getRepresentedEntity();
	   holder.setRepresentedEntity(null);
	   List<RepresentedEntity> lstRepresentedEntityResult = new ArrayList<RepresentedEntity>();
	   
	   
	   for(RepresentedEntity rE : lstRepresentedEntity){
		   if(!rE.getStateRepresented().equals(RepresentativeEntityStateType.DELETED.getCode())){
			   lstRepresentedEntityResult.add(rE);
		   }
	   }
	   
	   holder.setRepresentedEntity(lstRepresentedEntityResult);
	   
	          
       for(int i=0;i<holder.getRepresentedEntity().size();i++){
    	   holder.getRepresentedEntity().get(i).getLegalRepresentative().getIdLegalRepresentativePk();
    	   
    	   if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalRepresentativeFile()!=null){
    		   holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalRepresentativeFile().size();
    	   }
       }
       if(holder.getHolderFile()!=null){    	   
    	     
    	     List<HolderFile> lstHolderFile;    			 
			 lstHolderFile = holder.getHolderFile();
			 holder.setHolderFile(null);
		     List<HolderFile> lstHolderFileResult  = new ArrayList<HolderFile>();
			 
			 for(HolderFile hF:lstHolderFile){
				 if(!hF.getStateFile().equals(HolderFileStateType.DELETED.getCode())){
					 lstHolderFileResult.add(hF);
				 }
			 }
			 
			 holder.setHolderFile(lstHolderFileResult);
			 holder.getHolderFile().size();
       }
       
       if(holder.getParticipant()!=null){
    	   holder.getParticipant().getIdParticipantPk();
       }
       
       return holder;
         
     }
     
     /**
      * Gets the exist holder service facade.
      *
      * @param idHolderPk the id holder pk
      * @return the exist holder service facade
      * @throws ServiceException the service exception
      */
     public Integer getExistHolderServiceFacade(Long idHolderPk) throws ServiceException{
    	return holderServiceBean.getExistHolderServiceBean(idHolderPk);
     }
     
     /**
      * Gets the holder request copy by id request service facade.
      *
      * @param idHolderRequest the id holder request
      * @return the holder request copy by id request service facade
      * @throws ServiceException the service exception
      */
     public HolderRequest getHolderRequestCopyByIdRequestServiceFacade(Long idHolderRequest)throws ServiceException{
    	 HolderRequest request =   holderServiceBean.getHolderRequesByIdRequestServiceBeanAux(idHolderRequest);
    	 
    	 if(request.getHolder()!=null){
    		 request.getHolder().getIdHolderPk();
    		 request.getHolder().getParticipant().getIdParticipantPk();
    	 }
    	 
    	 if(request.getHolder()!=null){    		 
    	
    		 if(request.getHolder().getHolderFile()!=null){
    		 
    			 List<HolderFile> lstHolderFile;    			 
    			 lstHolderFile = request.getHolder().getHolderFile();
    		     request.getHolder().setHolderFile(null);
    		     List<HolderFile> lstHolderFileResult  = new ArrayList<HolderFile>();
    			 
    			 for(HolderFile hF:lstHolderFile){
    				 if(!hF.getStateFile().equals(HolderFileStateType.DELETED.getCode())){
    					 lstHolderFileResult.add(hF);
    				 }
    			 }
    			  
    			 request.getHolder().setHolderFile(lstHolderFileResult);
    			 request.getHolder().getHolderFile().size();
    		 }
         }
    	 
    	 request.getParticipant().getIdParticipantPk();
         request.getLegalRepresentativeHistories().size();
        
         request.getHolderReqFileHistories().size();
         
         List<HolderReqFileHistory> lstHolderReqFileHistory;
         List<HolderReqFileHistory> lstHolderReqFileHistoryResult = new ArrayList<HolderReqFileHistory>();
         lstHolderReqFileHistory = request.getHolderReqFileHistories();
         
         for(HolderReqFileHistory hRFH : lstHolderReqFileHistory){
        	 if(hRFH.getRegistryType().equals(RegistryType.HISTORICAL_COPY.getCode())){
        		 lstHolderReqFileHistoryResult.add(hRFH);
        	 }
         }
         
         if(lstHolderReqFileHistoryResult!=null && lstHolderReqFileHistoryResult.size()>0){
        	 request.setHolderReqFileHistories(lstHolderReqFileHistoryResult);
         }
         else{
        	 request.setHolderReqFileHistories(null);
         }
         
         List<LegalRepresentativeHistory> lstLegalRepresentativeHistory;
         List<LegalRepresentativeHistory> lstLegalRepresentativeHistoryResult = new ArrayList<LegalRepresentativeHistory>();
         lstLegalRepresentativeHistory = request.getLegalRepresentativeHistories();
         
         
         for(LegalRepresentativeHistory lRH : lstLegalRepresentativeHistory){
        	 if(lRH.getRegistryType().equals(RegistryType.HISTORICAL_COPY.getCode())){
        		 lstLegalRepresentativeHistoryResult.add(lRH);
        	 }
         }
         
         if(lstLegalRepresentativeHistoryResult!=null && lstLegalRepresentativeHistoryResult.size()>0){
        	 request.setLegalRepresentativeHistories(lstLegalRepresentativeHistoryResult);
         }
         else{
        	 request.setLegalRepresentativeHistories(null);
         }
         
         if(request.getLegalRepresentativeHistories()!=null && request.getLegalRepresentativeHistories().size()>0){
        	 
        	 for(int i=0;i<request.getLegalRepresentativeHistories().size();i++){
        		 request.getLegalRepresentativeHistories().get(i).getRepresenteFileHistory().size();
             }
         }
         
        
         List<HolderHistory> lstHolderHistory;
         List<HolderHistory> lstHolderHistoryResult = new ArrayList<HolderHistory>();
         lstHolderHistory = request.getHolderHistories();
    	 
         for(HolderHistory hH : lstHolderHistory){
        	 if(hH.getRegistryType()!=null && hH.getRegistryType().equals(RegistryType.HISTORICAL_COPY.getCode())){
        		 lstHolderHistoryResult.add(hH);
        	 }
         }
         
         if(lstHolderHistoryResult!=null && lstHolderHistoryResult.size()>0){
        	 request.setHolderHistories(lstHolderHistoryResult);
         }
         else{
        	 request.setHolderHistories(null);
         }
         
         return request;
     }
     
     /**
      * Gets the holder request by id request service facade.
      *
      * @param idHolderRequest the id holder request
      * @return the holder request by id request service facade
      * @throws ServiceException the service exception
      */
     public HolderRequest getHolderRequestByIdRequestServiceFacade(Long idHolderRequest) throws ServiceException{
    	 return holderServiceBean.getHolderRequesByIdRequestServiceBean(idHolderRequest);
     }
     
     /**
      * Gets the pep person by id holder service facade.
      *
      * @param idHolderPk the id holder pk
      * @return the pep person by id holder service facade
      * @throws ServiceException the service exception
      */
     public PepPerson getPepPersonByIdHolderServiceFacade(Long idHolderPk) throws ServiceException{
    	 PepPerson pepPerson = holderServiceBean.getPepPersonByIdHolderServiceBean(idHolderPk);
    	 return pepPerson;
     }
     
     /**
      * Validate exist pep person by id holder service facade.
      *
      * @param idHolderPk the id holder pk
      * @return the integer
      * @throws ServiceException the service exception
      */
     public Integer validateExistPepPersonByIdHolderServiceFacade(Long idHolderPk)throws ServiceException{
    	 return holderServiceBean.validateExistPepPersonByIdHolderServiceBean(idHolderPk);
     }
     
     /**
      * Validate exist pep person by id legal representative service facade.
      *
      * @param idLegalRepresentative the id legal representative
      * @return the integer
      * @throws ServiceException the service exception
      */
     public Integer validateExistPepPersonByIdLegalRepresentativeServiceFacade(Long idLegalRepresentative) throws ServiceException{
    	 return holderServiceBean.validateExistPepPersonByIdLegalRepresentativeServiceBean(idLegalRepresentative);
     }
     
     /**
      * Gets the pep person by id legal representative service facade.
      *
      * @param idLegalRepresentative the id legal representative
      * @return the pep person by id legal representative service facade
      * @throws ServiceException the service exception
      */
     public PepPerson getPepPersonByIdLegalRepresentativeServiceFacade(Long idLegalRepresentative) throws ServiceException{
    	 PepPerson pepPerson = holderServiceBean.getPepPersonByIdLegalRepresentativeServiceBean(idLegalRepresentative);
    	 
    	 return pepPerson;
     }
     
     /**
      * Register legal representative service facade.
      *
      * @param legalRepresentative the legal representative
      * @return true, if successful
      * @throws ServiceException the service exception
      */
     public boolean registerLegalRepresentativeServiceFacade(LegalRepresentative legalRepresentative) throws ServiceException{
    	 
    	 boolean flagTransaction;
    	 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
 	 
    	 crudDaoServiceBean.create(legalRepresentative);
		 flagTransaction=true;
		 
		 return flagTransaction;
    	 
     }
    
    /**
     * Update legal representative service facade.
     *
     * @param legalRepresentative the legal representative
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean updateLegalRepresentativeServiceFacade(LegalRepresentative legalRepresentative) throws ServiceException{
    	 
    	 boolean flagTransaction;
    	 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
    	  
   	 
    	 crudDaoServiceBean.update(legalRepresentative);
		 flagTransaction=true;
		 
		 return flagTransaction;
    	 
     }
     
     /**
      * Gets the block un block holder request service facade.
      *
      * @param holder the holder
      * @return the block un block holder request service facade
      * @throws ServiceException the service exception
      */
     public Holder getBlockUnBlockHolderRequestServiceFacade(Holder holder) throws ServiceException{
 		Holder objHolder = crudDaoServiceBean.find(Holder.class,holder.getIdHolderPk()); 	
 		
 		if(objHolder!=null){
 			if(objHolder.getParticipant()!=null){
 				objHolder.getParticipant().getIdParticipantPk();
 			}
 		}
 		
 		return objHolder;
 	    
 	 }
     
     /**
      * Gets the holder service facade.
      *
      * @param holder the holder
      * @return the holder service facade
      * @throws ServiceException the service exception
      */
     public Holder getHolderServiceFacade(Holder holder) throws ServiceException{
  		Holder objHolder = crudDaoServiceBean.find(Holder.class,holder.getIdHolderPk()); 	
  		
  		if(objHolder!=null){
  			if(objHolder.getParticipant()!=null){
  				objHolder.getParticipant().getIdParticipantPk();
  			}
  		}
  		
  		return objHolder;
  	    
  	 }
     
     /**
      * Gets the list holder block un block request service facade.
      *
      * @param holderRequest the holder request
      * @return the list holder block un block request service facade
      * @throws ServiceException the service exception
      */
     public List<HolderRequest> getListHolderBlockUnBlockRequestServiceFacade(HolderRequest holderRequest) throws ServiceException{
		 List<HolderRequest> list = holderServiceBean.getListHolderBlockUnBlockRequestServiceBean(holderRequest);
		 
		 for(int i=0;i<list.size();i++){
			list.get(i).getHolderHistoryStates().size();
			list.get(i).getHolder().getIdHolderPk();
			
			if(list.get(i).getParticipant()!=null){
				list.get(i).getParticipant().getIdParticipantPk();
			}
			
			}
		 
		 return list;
	 }
     
     /**
      * Gets the historial state list.
      *
      * @param holderRequest the holder request
      * @return the historial state list
      */
     public List<HolderRequest> getHistorialStateList(HolderRequest holderRequest){
    	 List<HolderRequest> lstResult = null;
    	 try{
	    	 lstResult = holderServiceBean.getHistorialStateList(holderRequest);
			 //For para inicializar los otros atributos
			 for(HolderRequest objHolderRequest:lstResult){
				 objHolderRequest.getHolderHistoryStates().size();
				 objHolderRequest.getHolder().getIdHolderPk();
				 if(Validations.validateIsNotNullAndNotEmpty(objHolderRequest.getParticipant()))
					 objHolderRequest.getParticipant().getIdParticipantPk();
		 	}
    	 }catch (ServiceException e) {
			e.printStackTrace();
		}
		return lstResult;
	 }
     
     
     
     /**
      * Gets the holder request service facade.
      *
      * @param holderRequest the holder request
      * @return the holder request service facade
      * @throws ServiceException the service exception
      */
     public Integer getHolderRequestServiceFacade(HolderRequest holderRequest) throws ServiceException{
    	 return holderServiceBean.getHolderRequestServiceBean(holderRequest);
     }
     
     /**
      * Gets the list holder block un block request service facade.
      *
      * @param holderFilter the holder filter
      * @return the list holder block un block request service facade
      * @throws ServiceException the service exception
      */
     @ProcessAuditLogger(process=BusinessProcessType.HOLDER_BLOCK_UNBLOCK_QUERY)
     public List<HolderRequest> getListHolderBlockUnBlockRequestServiceFacade(HolderRequestTO holderFilter) throws ServiceException{
		 List<HolderRequest> list = new ArrayList<HolderRequest>();
		 list = holderServiceBean.getListHolderRequestServiceBean(holderFilter);
		 
		 
		for(int i=0;i<list.size();i++){
			list.get(i).getHolder();
			list.get(i).getHolder().getParticipant();			
			if(Validations.validateIsNotNullAndNotEmpty(list.get(i).getParticipant())){
				list.get(i).getParticipant();		
			}
		 }		 
		 return list;
	 }
     
     /**
      * Update holder request service facade.
      *
      * @param holderRequest the holder request
      * @return true, if successful
      * @throws ServiceException the service exception
      */
     public boolean updateHolderRequestServiceFacade(HolderRequest holderRequest)throws ServiceException{
    	 boolean flagTransaction;		
    	 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
    	 
   	 
		 holderServiceBean.updateHolderRequestServiceBean(holderRequest);
         flagTransaction=true;
		 return flagTransaction;    	 
     }
     
     /**
      * Gets the holder list by state.
      *
      * @param filter the filter
      * @return the holder list by state
      * @throws ServiceException the service exception
      */
     public List<Holder> getHolderListByState(HolderRequestTO filter) throws ServiceException{
    	 return holderServiceBean.getHolderRequestListByStateServiceBean(filter);
    	 
     }
     
     /**
      * Gets the list search holder service facade.
      *
      * @param holder the holder
      * @return the list search holder service facade
      * @throws ServiceException the service exception
      */
     @ProcessAuditLogger(process=BusinessProcessType.HOLDER_QUERY)
	  public List<Holder> getListSearchHolderServiceFacade(Holder holder) throws ServiceException{
    	 List<Holder> list = holderServiceBean.getListSearchHolderServiceBean(holder);
		 return list;
     }
     
     /**
      * Gets the list search holder general.
      *
      * @param docType the doc type
      * @param docNumber the doc number
      * @param expedition the expedition
      * @return the list search holder general
      * @throws ServiceException the service exception
      */
     @ProcessAuditLogger(process=BusinessProcessType.HOLDER_QUERY)
	  public List<Holder> getListSearchHolderGeneral(Integer docType,String docNumber,Integer expedition) throws ServiceException{
   	 List<Holder> list = holderServiceBean.getListSearchHolderGeneral(docType,docNumber,expedition);
		 return list;
    }
     
     /**
      * Search represented entity service facade.
      *
      * @param representedEntity the represented entity
      * @return the list
      * @throws ServiceException the service exception
      */
     public List<RepresentedEntity> searchRepresentedEntityServiceFacade(RepresentedEntity representedEntity)throws ServiceException{
    	 List<RepresentedEntity> list =  holderServiceBean.searchRepresentedEntityServiceBean(representedEntity);
    	 for(int i=0;i<list.size();i++){
    		 if(list.get(i).getHolder()!=null){
    			 list.get(i).getHolder().getIdHolderPk();
    		 }
    		 if(list.get(i).getLegalRepresentative()!=null){
                 list.get(i).getLegalRepresentative().getIdLegalRepresentativePk();    			 
    		 }    		 
    	 }
    	 
    	 return list;
     }
     
     /**
      * Gets the content file holder history service facade.
      *
      * @param id the id
      * @param documentType the document type
      * @param registryHistorical the registry historical
      * @return the content file holder history service facade
      * @throws ServiceException the service exception
      */
     public HolderReqFileHistory getContentFileHolderHistoryServiceFacade(Long id, Integer documentType,boolean registryHistorical)throws ServiceException{
    	 HolderReqFileHistory holderReqFileHistory = holderServiceBean.getContentFileHolderHistoryServiceBean(id,documentType,registryHistorical);
    	 
    	 if(holderReqFileHistory!=null){
    		 holderReqFileHistory.getFileHolderReq();
    	 }
    	 return holderReqFileHistory;
     }
     
     /**
      * Gets the content file holder service facade.
      *
      * @param id the id
      * @param documentType the document type
      * @return the content file holder service facade
      * @throws ServiceException the service exception
      */
     public HolderFile getContentFileHolderServiceFacade(Long id, Integer documentType)throws ServiceException{
    	 HolderFile holderFile = holderServiceBean.getContentFileHolderServiceBean(id,documentType);
    	 
    	 if(holderFile!=null){
    		 holderFile.getFileHolder();
    	 }
    	 return holderFile;
     }
     

     /**
      * Gets the content file holder format holder history service facade.
      *
      * @param id the id
      * @param documentType the document type
      * @return the content file holder format holder history service facade
      * @throws ServiceException the service exception
      */
     public HolderReqFileHistory getContentFileHolderFormatHolderHistoryServiceFacade(Long id, Integer documentType)throws ServiceException{
    	 
    	 HolderFile holderFile = holderServiceBean.getContentFileHolderServiceBean(id,documentType);
    	 
    	 HolderReqFileHistory holderReqFileHistory = new HolderReqFileHistory();
    	 
    	 if(holderFile!=null){
    		 holderReqFileHistory.setFileHolderReq(holderFile.getFileHolder());
    	 }
    	 return holderReqFileHistory;   	 
     }

     /**
      * Modify holder request service facade.
      *
      * @param holderRequest the holder request
      * @return true, if successful
      * @throws ServiceException the service exception
      */
     @ProcessAuditLogger(process=BusinessProcessType.NEW_HOLDER_CREATION_REQUEST_MODIFICATION)
     public boolean modifyHolderRequestServiceFacade(HolderRequest holderRequest)throws ServiceException {
    	 
    	 boolean flagTransaction;	
    	 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
    	 
    	 
    	 List<HolderReqFileHistory> listHolderHF = new ArrayList<HolderReqFileHistory>();    	 
    	 List<RepresentativeFileHistory> listRepreFH;
    	 List<LegalRepresentativeHistory> listLRH = new ArrayList<LegalRepresentativeHistory>();
    	 if(Validations.validateIsNotNull(holderRequest)){
    	 listHolderHF = holderRequest.getHolderReqFileHistories();
    	 }
    	 RepresentativeFileHistory representativeFileHistory;
    	 
    	 int countRepresentativeFileH = 0;
    	 
    	 if( Validations.validateIsNotNull(holderRequest) && holderRequest.getLegalRepresentativeHistories()!=null && holderRequest.getLegalRepresentativeHistories().size()>0){
    	
    		 for(int i=0;i<holderRequest.getLegalRepresentativeHistories().size();i++){
    			 listRepreFH = new ArrayList<RepresentativeFileHistory>();
    			 listRepreFH = holderRequest.getLegalRepresentativeHistories().get(i).getRepresenteFileHistory();
    			 
    			 countRepresentativeFileH =0;
    			    
    			 for(RepresentativeFileHistory rfh :listRepreFH){
    			    	
    			       if(!loadEntityService.isLoadedEntity(rfh,"fileRepresentative")){
    			    	   representativeFileHistory =  accountsFacade.getContentFileRepresentativeHistoryServiceFacade(rfh.getIdRepresentFileHisPk());
    			    	   rfh = representativeFileHistory;
    			    	      			    	   
    			    	   listRepreFH.set(countRepresentativeFileH, rfh);
    			       }
    			       
    			       countRepresentativeFileH++;
    			       
    			    }
    			    
    			    holderRequest.getLegalRepresentativeHistories().get(i).setRepresenteFileHistory(listRepreFH);  	    			  			    
    		 }
    	 
    		 
    		 listLRH  = holderRequest.getLegalRepresentativeHistories();
    	    }
    	 	if(Validations.validateIsNotNullAndNotEmpty(holderRequest)){
	    		 if(Validations.validateIsNotNullAndNotEmpty(holderRequest.getHolderReqFileHistories()) && holderRequest.getHolderReqFileHistories().size()>0){
	    			 listHolderHF = holderRequest.getHolderReqFileHistories();
	    		 }
    	 	}
    		 HolderReqFileHistory  holderReqFileHistory;
    		
    		 int countHolderFileH = 0;
        	 
    		 if(listHolderHF!=null && listHolderHF.size()>0){

    			 for(HolderReqFileHistory hrfh : listHolderHF){
    				 
    				 if(!loadEntityService.isLoadedEntity(hrfh,"fileHolderReq")){
    					 holderReqFileHistory  = new HolderReqFileHistory();       
    					 holderReqFileHistory = getContentFileHolderHistoryServiceFacade(holderRequest.getIdHolderRequestPk(),hrfh.getDocumentType(),false);
    					 hrfh = holderReqFileHistory;
    				 
    					 listHolderHF.set(countHolderFileH,hrfh);	
    				 }
    				 countHolderFileH++;
    			 }
    		 }
    		 
    	
    	 if(listLRH!=null && listLRH.size()>0){
    	
    		 for(int i=0;i<listLRH.size();i++){
    			 accountsFacade.removeLegalRepresentativeHistoryFileServiceFacade(listLRH.get(i).getIdRepresentativeHistoryPk());        	 	 
    		 }
    	 }
    	 if(Validations.validateIsNotNullAndNotEmpty(holderRequest)){
    		 Long holderRequestPk = holderRequest.getIdHolderRequestPk();
    	 accountsFacade.removeLegalRepresentativeServiceFacade(holderRequestPk);
    	 holderServiceBean.removeHolderRequestFileServiceBean(holderRequestPk);
    	 }
    	 List<LegalRepresentativeHistory> auxLstLegalRepresetantiveHistory = new  ArrayList<LegalRepresentativeHistory>();
    	 
    	 List<HolderReqFileHistory> auxHolderReqFileHistory = new ArrayList<HolderReqFileHistory>();
    	 
    	 List<RepresentativeFileHistory> auxRepresentativeFileHistory = new ArrayList<RepresentativeFileHistory>();
    	 
    	 HolderRequest auxHolderRequest = new HolderRequest();   
    	 auxHolderRequest = holderRequest;
    	 
    	
    	 
    	 LegalRepresentativeHistory auxLRH;
    	 RepresentativeFileHistory auxRFH;
    	 
    	if(listLRH!=null && listLRH.size()>0){
    		 
    	 
    	 for(LegalRepresentativeHistory lrh : listLRH){
    		 auxLRH = new LegalRepresentativeHistory();
    		 
    		 if(lrh.getBeginningPeriod()!=null){auxLRH.setBeginningPeriod(lrh.getBeginningPeriod());}
    		 if(lrh.getBirthDate()!=null){auxLRH.setBirthDate(lrh.getBirthDate());}
    		 if(lrh.getCategory()!=null){auxLRH.setCategory(lrh.getCategory());}
    		 if(lrh.getComments()!=null){auxLRH.setComments(lrh.getComments());}
    		 if(lrh.getDocumentNumber()!=null){auxLRH.setDocumentNumber(lrh.getDocumentNumber());}
    		 if(lrh.getDocumentType()!=null){auxLRH.setDocumentType(lrh.getDocumentType());}
    		 if(lrh.getDocumentIssuanceDate() !=null){auxLRH.setDocumentIssuanceDate(lrh.getDocumentIssuanceDate());}
    		 if(lrh.getDocumentTypeDescription()!=null){auxLRH.setDocumentTypeDescription(lrh.getDocumentTypeDescription());}
    		 if(lrh.getEconomicActivity()!=null){auxLRH.setEconomicActivity(lrh.getEconomicActivity());}
    		 if(lrh.getEconomicSector()!=null){auxLRH.setEconomicSector(lrh.getEconomicSector());}
    		 if(lrh.getEmail()!=null){auxLRH.setEmail(lrh.getEmail());}
    		 if(lrh.getEndingPeriod()!=null){auxLRH.setEndingPeriod(lrh.getEndingPeriod());}
    		 if(lrh.getFaxNumber()!=null){auxLRH.setFaxNumber(lrh.getFaxNumber());}
    		 if(lrh.getFirstLastName()!=null){auxLRH.setFirstLastName(lrh.getFirstLastName());}
    		 if(lrh.getFullName()!=null){auxLRH.setFullName(lrh.getFullName());}
    		 							auxLRH.setHolderRequest(auxHolderRequest);
    		 if(lrh.getHomePhoneNumber()!=null){auxLRH.setHomePhoneNumber(lrh.getHomePhoneNumber());}
    		 if(lrh.getIndPEP()!=null){auxLRH.setIndPEP(lrh.getIndPEP());}
    		 if(lrh.getIndResidence()!=null){auxLRH.setIndResidence(lrh.getIndResidence());}
    		 if(lrh.getIssuerRequest()!=null){auxLRH.setIssuerRequest(lrh.getIssuerRequest());}
    		 if(lrh.getLegalAddress()!=null){auxLRH.setLegalAddress(lrh.getLegalAddress());}
    		 if(lrh.getLegalDepartment()!=null){auxLRH.setLegalDepartment(lrh.getLegalDepartment());}
    		 if(lrh.getLegalDistrict()!=null){auxLRH.setLegalDistrict(lrh.getLegalDistrict());}
    		 if(lrh.getLegalProvince()!=null){auxLRH.setLegalProvince(lrh.getLegalProvince());}
    		 if(lrh.getLegalResidenceCountry()!=null){auxLRH.setLegalResidenceCountry(lrh.getLegalResidenceCountry());}
    		 if(lrh.getMobileNumber()!=null){auxLRH.setMobileNumber(lrh.getMobileNumber());}
    		 if(lrh.getName()!=null){auxLRH.setName(lrh.getName());}
    		 if(lrh.getNationality()!=null){auxLRH.setNationality(lrh.getNationality());}
    		 if(lrh.getOfficePhoneNumber()!=null){auxLRH.setOfficePhoneNumber(lrh.getOfficePhoneNumber());}
    		 if(lrh.getParticipantRequest()!=null){auxLRH.setParticipantRequest(lrh.getParticipantRequest());}
    		 if(lrh.getPersonType()!=null){auxLRH.setPersonType(lrh.getPersonType());}
    		 if(lrh.getPostalAddress()!=null){auxLRH.setPostalAddress(lrh.getPostalAddress());}
    		 if(lrh.getPostalDepartment()!=null){auxLRH.setPostalDepartment(lrh.getPostalDepartment());}
    		 if(lrh.getPostalDistrict()!=null){auxLRH.setPostalDistrict(lrh.getPostalDistrict());}
    		 if(lrh.getPostalProvince()!=null){auxLRH.setPostalProvince(lrh.getPostalProvince());}
    		 if(lrh.getPostalResidenceCountry()!=null){auxLRH.setPostalResidenceCountry(lrh.getPostalResidenceCountry());}
    		 auxLRH.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
    		 auxLRH.setRegistryUser(userInfo.getUserAccountSession().getUserName());
    		 auxLRH.setRepreHistoryType(HolderRequestType.CREATION.getCode());
    		 if(lrh.getRepresentativeClass()!=null){auxLRH.setRepresentativeClass(lrh.getRepresentativeClass());}
    		 if(lrh.getRepresentativeClassDescription()!=null){auxLRH.setRepresentativeClassDescription(lrh.getRepresentativeClassDescription());}
    		 if(lrh.getRepresentativeDescription()!=null){auxLRH.setRepresentativeDescription(lrh.getRepresentativeDescription());}
    		 if(lrh.getRepresentativeDescriptionAux()!=null){auxLRH.setRepresentativeDescriptionAux(lrh.getRepresentativeDescriptionAux());}
    		 if(lrh.getRepresentativeTypeDescription()!=null){auxLRH.setRepresentativeTypeDescription(lrh.getRepresentativeTypeDescription());}
    		 if(lrh.getRole()!=null){auxLRH.setRole(lrh.getRole());}    		 
    		 if(lrh.getSecondDocumentNumber()!=null){auxLRH.setSecondDocumentNumber(lrh.getSecondDocumentNumber());}
    		 if(lrh.getSecondDocumentType()!=null){auxLRH.setSecondDocumentType(lrh.getSecondDocumentType());}
    		 if(lrh.getSecondLastName()!=null){auxLRH.setSecondLastName(lrh.getSecondLastName());}
    		 if(lrh.getSecondNationality()!=null){auxLRH.setSecondNationality(lrh.getSecondNationality());}
    		 if(lrh.getSex()!=null){auxLRH.setSex(lrh.getSex());}
    		 if(lrh.getDocumentSource()!=null){auxLRH.setDocumentSource(lrh.getDocumentSource());}
    		 auxLRH.setStateRepreHistory(HolderRequestStateType.MODIFIED.getCode());
    	 
    		 if(lrh.getRepresenteFileHistory().size()>0){
    		 
    		 for(RepresentativeFileHistory rfh : lrh.getRepresenteFileHistory()){
    			 auxRFH = new RepresentativeFileHistory();
    			
    			 if(rfh.getDescription()!=null){auxRFH.setDescription(rfh.getDescription());}
    			 if(rfh.getDocumentType()!=null){auxRFH.setDocumentType(rfh.getDocumentType());}
    			 if(rfh.getFilename()!=null){auxRFH.setFilename(rfh.getFilename());}
    			 if(rfh.getFileRepresentative()!=null){auxRFH.setFileRepresentative(rfh.getFileRepresentative());}
    			 									   auxRFH.setLegalRepresentativeHistory(auxLRH);
    		     auxRFH.setRegistryDate(CommonsUtilities.currentDate());
    			 auxRFH.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
    			 auxRFH.setRegistryUser(userInfo.getUserAccountSession().getUserName());
    			 auxRFH.setRepresentFileHistType(HolderReqFileHisType.CREATION.getCode());
    			 auxRFH.setStateFile(HolderFileStateType.REGISTERED.getCode());
    			     			 
    			 auxRepresentativeFileHistory.add(auxRFH);
    		 }
    		 
    		 }
    	 
    		 if(auxRepresentativeFileHistory.size()>0){
    		 auxLRH.setRepresenteFileHistory(auxRepresentativeFileHistory);
    		 }
    		 
    		 auxLstLegalRepresetantiveHistory.add(auxLRH);
    		 
    	     }
    	}
    		 
    	 HolderReqFileHistory auxHRFH;
    	 
    	if(listHolderHF!=null && listHolderHF.size()>0){
    		for(HolderReqFileHistory hrfh :listHolderHF){
    		
    		 auxHRFH = new HolderReqFileHistory();
    		 
    		 if(hrfh.getDescription()!=null){auxHRFH.setDescription(hrfh.getDescription());}
			 if(hrfh.getDocumentType()!=null){auxHRFH.setDocumentType(hrfh.getDocumentType());}
			 if(hrfh.getFilename()!=null){auxHRFH.setFilename(hrfh.getFilename());}
			 if(hrfh.getFileHolderReq()!=null){auxHRFH.setFileHolderReq(hrfh.getFileHolderReq());}
			 								   auxHRFH.setHolderRequest(auxHolderRequest);
			 auxHRFH.setRegistryDate(CommonsUtilities.currentDate());
			 auxHRFH.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
			 auxHRFH.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			 auxHRFH.setHolderReqFileHisType(HolderReqFileHisType.CREATION.getCode());
			 auxHRFH.setStateFile(HolderFileStateType.REGISTERED.getCode());
			 
			 auxHolderReqFileHistory.add(auxHRFH);
         }
    	 
    	 }
    	 
    	 if(auxLstLegalRepresetantiveHistory!=null && auxLstLegalRepresetantiveHistory.size()>0){ 
    		 auxHolderRequest.setLegalRepresentativeHistories(auxLstLegalRepresetantiveHistory);
    	 }
    	
    	 if(auxHolderReqFileHistory!=null && auxHolderReqFileHistory.size()>0){
    		 auxHolderRequest.setHolderReqFileHistories(auxHolderReqFileHistory);
    	 }
    	 
    	 if(auxHolderRequest!=null){    		    		 
    		 crudDaoServiceBean.update(auxHolderRequest);
    		 
    	 }
    		     	 
    	 flagTransaction = true;
    	 
    	 return flagTransaction;
    		  
     }
     
     
     /**
      * Removes the holder request file service facade.
      *
      * @param id the id
      * @throws ServiceException the service exception
      */
     public void removeHolderRequestFileServiceFacade(Long id) throws ServiceException {
    	 
    	 holderServiceBean.removeHolderRequestFileServiceBean(id);
     }
     
     /**
      * Gets the holder request by document type and document number service facade.
      *
      * @param holderRequest the holder request
      * @return the holder request by document type and document number service facade
      * @throws ServiceException the service exception
      */
     public HolderRequest getHolderRequestByDocumentTypeAndDocumentNumberServiceFacade(HolderRequest holderRequest) throws ServiceException {
    	HolderRequest  objHolderRequest = holderServiceBean.getHolderRequestByDocumentTypeAndDocumentNumberServiceBean(holderRequest);
        if(objHolderRequest==null){
        	HolderRequest auxHolderRequest = new HolderRequest();
        	HolderHistory auxHolderHistory = new HolderHistory();
        	auxHolderHistory.setDocumentNumber(holderRequest.getHolderHistories().get(0).getDocumentNumber());
        	auxHolderHistory.setDocumentType(holderRequest.getHolderHistories().get(0).getDocumentType());
        	auxHolderHistory.setNationality(holderRequest.getHolderHistories().get(0).getNationality());
        	auxHolderHistory.setRelatedCui(holderRequest.getHolderHistories().get(0).getRelatedCui());
        	List<HolderHistory> listHolderHistory = new ArrayList<HolderHistory>();
        	listHolderHistory.add(auxHolderHistory);
        	auxHolderRequest.setHolderHistories(listHolderHistory);
        	auxHolderRequest.setIdHolderRequestPk(holderRequest.getIdHolderRequestPk());
        	
        	objHolderRequest = holderServiceBean.getHolderRequestByDocumentTypeAndDocumentNumberServiceBean(auxHolderRequest);
        }
        return objHolderRequest;
     }
     
     /**
      * Gets the holder request by second document type and second document number service facade.
      *
      * @param holderRequest the holder request
      * @return the holder request by second document type and second document number service facade
      * @throws ServiceException the service exception
      */
     public HolderRequest getHolderRequestBySecondDocumentTypeAndSecondDocumentNumberServiceFacade(HolderRequest holderRequest) throws ServiceException {
     	return holderServiceBean.getHolderRequestBySecondDocumentTypeAndSecondDocumentNumberServiceBean(holderRequest);
     }
     
     /**
      * Update request number holder request.
      *
      * @param holderRequest the holder request
      * @throws ServiceException the service exception
      */
     public void updateRequestNumberHolderRequest(HolderRequest holderRequest) throws ServiceException {
    	 holderServiceBean.updateRequestNumberHolderRequestServiceBean(holderRequest);
     }
     
     
     /**
      * Gets the file histories.
      *
      * @param holderRequest the holder request
      * @return the file histories
      * @throws ServiceException the service exception
      */
     public HolderRequest getFileHistories(HolderRequest holderRequest)throws ServiceException {
    	 List<HolderReqFileHistory> listHolderHF = new ArrayList<HolderReqFileHistory>();    	 
    	 List<RepresentativeFileHistory> listRepreFH;
    	 List<LegalRepresentativeHistory> listLRH = new ArrayList<LegalRepresentativeHistory>();
    	 
    	 
    	 listHolderHF = holderRequest.getHolderReqFileHistories();
    	 
    	 
    	RepresentativeFileHistory representativeFileHistory;
    	 
    	 
    	 int countRepresentativeFileH = 0;
    	 
    	 if(holderRequest.getLegalRepresentativeHistories()!=null && holderRequest.getLegalRepresentativeHistories().size()>0){
    	 
    		 for(int i=0;i<holderRequest.getLegalRepresentativeHistories().size();i++){
    			 listRepreFH = new ArrayList<RepresentativeFileHistory>();
    			 
    			 if(holderRequest.getLegalRepresentativeHistories().get(i).getRepresenteFileHistory()!=null &&
    					 holderRequest.getLegalRepresentativeHistories().get(i).getRepresenteFileHistory().size()>0){
    				 listRepreFH = holderRequest.getLegalRepresentativeHistories().get(i).getRepresenteFileHistory();
    			 }
    			 countRepresentativeFileH = 0;
    			 
    			 RepresentativeFileHistory auxRF;
    			 
    			 if(listRepreFH!=null && listRepreFH.size()>0){
    			    
    			 for(RepresentativeFileHistory rfh :listRepreFH){
    			    	  
    			       if(!rfh.isWasModified() && rfh.getIdRepresentFileHisPk()!=null){
    			    	   
    			    	   auxRF = new RepresentativeFileHistory();
    	    				 
    			    	   LegalRepresentativeFile legalRF =    accountsFacade.getContentFileLegalRepresentativeServiceFacade(rfh.getIdRepresentFileHisPk());
    			    	   
    			    	   representativeFileHistory = new RepresentativeFileHistory(); 
    			    	   representativeFileHistory.setDescription(legalRF.getDescription());
    			    	   representativeFileHistory.setDocumentType(legalRF.getDocumentType());
    			    	   representativeFileHistory.setFilename(legalRF.getFilename());
    			    	   representativeFileHistory.setRegistryType(legalRF.getRegistryType());
    			    	   representativeFileHistory.setRepresentFileHistType(legalRF.getLegalRepreFileType());
    			    	   representativeFileHistory.setStateFile(legalRF.getStateFile());
    			    	   representativeFileHistory.setFileRepresentative(legalRF.getFileLegalRepre());
    			    	   
    			    	   auxRF.setDescription(representativeFileHistory.getDescription());
    			    	   auxRF.setDocumentType(representativeFileHistory.getDocumentType());
    			    	   auxRF.setFilename(representativeFileHistory.getFilename());
    			    	   auxRF.setRegistryDate(CommonsUtilities.currentDate());
    			    	   auxRF.setRegistryType(representativeFileHistory.getRegistryType());
    			    	   auxRF.setRegistryUser(userInfo.getUserAccountSession().getUserName());
    			    	   auxRF.setRepresentFileHistType(representativeFileHistory.getRepresentFileHistType());
    			    	   auxRF.setStateFile(representativeFileHistory.getStateFile());
    			    	   auxRF.setFileRepresentative(representativeFileHistory.getFileRepresentative());
    			    	   
    			    	   listRepreFH.set(countRepresentativeFileH, auxRF);
    			       }
    			       
    			       countRepresentativeFileH++;
    			       
    			    }
    			 }
    			    if(listRepreFH!=null && listRepreFH.size()>0){
    			    	holderRequest.getLegalRepresentativeHistories().get(i).setRepresenteFileHistory(listRepreFH);
    			    }
    		 }
    	 
    		 
    		 listLRH  = holderRequest.getLegalRepresentativeHistories();
    	 }	 
    		 listHolderHF = holderRequest.getHolderReqFileHistories();
    		 
    		 HolderReqFileHistory  holderReqFileHistory;
    		
    		 int countHolderFileH = 0;
        	 
    		 HolderReqFileHistory auxHF = new HolderReqFileHistory();
    		 
    		if(listHolderHF!=null && listHolderHF.size()>0){
    			 
    		 for(HolderReqFileHistory hrfh : listHolderHF){
    			
    			 if(!hrfh.isWasModified()){
    				 
    				 auxHF = new HolderReqFileHistory();
    				 
    				 holderReqFileHistory  = new HolderReqFileHistory();       
    				 holderReqFileHistory = getContentFileHolderFormatHolderHistoryServiceFacade(holderRequest.getHolder().getIdHolderPk(),hrfh.getDocumentType());
    				
    				 auxHF.setDescription(hrfh.getDescription());
    				 auxHF.setDocumentType(hrfh.getDocumentType());
    				 auxHF.setFilename(hrfh.getFilename());
    				 auxHF.setRegistryDate(hrfh.getRegistryDate());
    				 auxHF.setRegistryType(hrfh.getRegistryType());
    				 auxHF.setRegistryUser(userInfo.getUserAccountSession().getUserName());
    				 auxHF.setHolderReqFileHisType(hrfh.getHolderReqFileHisType());
    				 auxHF.setStateFile(hrfh.getStateFile());
    				 auxHF.setFileHolderReq(holderReqFileHistory.getFileHolderReq());
    				 
    				 listHolderHF.set(countHolderFileH,auxHF);
    			 }
    			 countHolderFileH++;
    		  }
    		}
    		 
    		 List<LegalRepresentativeHistory> auxLstLegalRepresetantiveHistory = new  ArrayList<LegalRepresentativeHistory>();
        	 
        	 List<HolderReqFileHistory> auxHolderReqFileHistory = new ArrayList<HolderReqFileHistory>();
        	 
        	 List<RepresentativeFileHistory> auxRepresentativeFileHistory = new ArrayList<RepresentativeFileHistory>();
        	 
        	 HolderRequest auxHolderRequest = new HolderRequest();   
        	 auxHolderRequest = holderRequest;
        	 
        	
        	 
        	 LegalRepresentativeHistory auxLRH;
        	 RepresentativeFileHistory auxRFH;
        	 
        	 if(listLRH!=null && listLRH.size()>0){
        	 for(LegalRepresentativeHistory lrh : listLRH){
        		 auxLRH = new LegalRepresentativeHistory();
        		 
        		 if(lrh.getBeginningPeriod()!=null){auxLRH.setBeginningPeriod(lrh.getBeginningPeriod());}
        		 if(lrh.getBirthDate()!=null){auxLRH.setBirthDate(lrh.getBirthDate());}
        		 if(lrh.getCategory()!=null){auxLRH.setCategory(lrh.getCategory());}
        		 if(lrh.getComments()!=null){auxLRH.setComments(lrh.getComments());}
        		 if(lrh.getDocumentNumber()!=null){auxLRH.setDocumentNumber(lrh.getDocumentNumber());}
        		 if(lrh.getDocumentIssuanceDate()!=null){auxLRH.setDocumentIssuanceDate(lrh.getDocumentIssuanceDate());}
        		 if(lrh.getDocumentType()!=null){auxLRH.setDocumentType(lrh.getDocumentType());}
        		 if(lrh.getDocumentTypeDescription()!=null){auxLRH.setDocumentTypeDescription(lrh.getDocumentTypeDescription());}
        		 if(lrh.getEconomicActivity()!=null){auxLRH.setEconomicActivity(lrh.getEconomicActivity());}
        		 if(lrh.getEconomicSector()!=null){auxLRH.setEconomicSector(lrh.getEconomicSector());}
        		 if(lrh.getEmail()!=null){auxLRH.setEmail(lrh.getEmail());}
        		 if(lrh.getEndingPeriod()!=null){auxLRH.setEndingPeriod(lrh.getEndingPeriod());}
        		 if(lrh.getFaxNumber()!=null){auxLRH.setFaxNumber(lrh.getFaxNumber());}
        		 if(lrh.getFirstLastName()!=null){auxLRH.setFirstLastName(lrh.getFirstLastName());}
        		 if(lrh.getFullName()!=null){auxLRH.setFullName(lrh.getFullName());}
        		 							auxLRH.setHolderRequest(auxHolderRequest);
        		 if(lrh.getHomePhoneNumber()!=null){auxLRH.setHomePhoneNumber(lrh.getHomePhoneNumber());}
        		 if(lrh.getIndPEP()!=null){auxLRH.setIndPEP(lrh.getIndPEP());}
        		 if(lrh.getIndResidence()!=null){auxLRH.setIndResidence(lrh.getIndResidence());}
        		 if(lrh.getIssuerRequest()!=null){auxLRH.setIssuerRequest(lrh.getIssuerRequest());}
        		 if(lrh.getLegalAddress()!=null){auxLRH.setLegalAddress(lrh.getLegalAddress());}
        		 if(lrh.getLegalDepartment()!=null){auxLRH.setLegalDepartment(lrh.getLegalDepartment());}
        		 if(lrh.getLegalDistrict()!=null){auxLRH.setLegalDistrict(lrh.getLegalDistrict());}
        		 if(lrh.getLegalProvince()!=null){auxLRH.setLegalProvince(lrh.getLegalProvince());}
        		 if(lrh.getLegalResidenceCountry()!=null){auxLRH.setLegalResidenceCountry(lrh.getLegalResidenceCountry());}
        		 if(lrh.getMobileNumber()!=null){auxLRH.setMobileNumber(lrh.getMobileNumber());}
        		 if(lrh.getName()!=null){auxLRH.setName(lrh.getName());}
        		 if(lrh.getNationality()!=null){auxLRH.setNationality(lrh.getNationality());}
        		 if(lrh.getOfficePhoneNumber()!=null){auxLRH.setOfficePhoneNumber(lrh.getOfficePhoneNumber());}
        		 if(lrh.getParticipantRequest()!=null){auxLRH.setParticipantRequest(lrh.getParticipantRequest());}
        		 if(lrh.getPersonType()!=null){auxLRH.setPersonType(lrh.getPersonType());}
        		 if(lrh.getPostalAddress()!=null){auxLRH.setPostalAddress(lrh.getPostalAddress());}
        		 if(lrh.getPostalDepartment()!=null){auxLRH.setPostalDepartment(lrh.getPostalDepartment());}
        		 if(lrh.getPostalDistrict()!=null){auxLRH.setPostalDistrict(lrh.getPostalDistrict());}
        		 if(lrh.getPostalProvince()!=null){auxLRH.setPostalProvince(lrh.getPostalProvince());}
        		 if(lrh.getPostalResidenceCountry()!=null){auxLRH.setPostalResidenceCountry(lrh.getPostalResidenceCountry());}
        		 if(lrh.getRegistryType()!=null){auxLRH.setRegistryType(lrh.getRegistryType());}
        		 if(lrh.getRegistryUser()!=null){auxLRH.setRegistryUser(lrh.getRegistryUser());}
        		 if(lrh.getRepreHistoryType()!=null){auxLRH.setRepreHistoryType(lrh.getRepreHistoryType());}
        		 if(lrh.getRepresentativeClass()!=null){auxLRH.setRepresentativeClass(lrh.getRepresentativeClass());}
        		 if(lrh.getRepresentativeClassDescription()!=null){auxLRH.setRepresentativeClassDescription(lrh.getRepresentativeClassDescription());}
        		 if(lrh.getRepresentativeDescription()!=null){auxLRH.setRepresentativeDescription(lrh.getRepresentativeDescription());}
        		 if(lrh.getRepresentativeDescriptionAux()!=null){auxLRH.setRepresentativeDescriptionAux(lrh.getRepresentativeDescriptionAux());}
        		 if(lrh.getRepresentativeTypeDescription()!=null){auxLRH.setRepresentativeTypeDescription(lrh.getRepresentativeTypeDescription());}
        		 if(lrh.getRole()!=null){auxLRH.setRole(lrh.getRole());}    		 
        		 if(lrh.getSecondDocumentNumber()!=null){auxLRH.setSecondDocumentNumber(lrh.getSecondDocumentNumber());}
        		 if(lrh.getSecondDocumentType()!=null){auxLRH.setSecondDocumentType(lrh.getSecondDocumentType());}
        		 if(lrh.getSecondLastName()!=null){auxLRH.setSecondLastName(lrh.getSecondLastName());}
        		 if(lrh.getSecondNationality()!=null){auxLRH.setSecondNationality(lrh.getSecondNationality());}
        		 if(lrh.getSex()!=null){auxLRH.setSex(lrh.getSex());}
        		 if(lrh.getStateRepreHistory()!=null){auxLRH.setStateRepreHistory(lrh.getStateRepreHistory());}
        	     if(lrh.getDocumentSource()!=null){auxLRH.setDocumentSource(lrh.getDocumentSource());}
        		 if(lrh.getRepresenteFileHistory().size()>0){
        		 
        		 for(RepresentativeFileHistory rfh : lrh.getRepresenteFileHistory()){
        			 auxRFH = new RepresentativeFileHistory();
        			
        			 if(rfh.getDescription()!=null){auxRFH.setDescription(rfh.getDescription());}
        			 if(rfh.getDocumentType()!=null){auxRFH.setDocumentType(rfh.getDocumentType());}
        			 if(rfh.getFilename()!=null){auxRFH.setFilename(rfh.getFilename());}
        			 if(rfh.getFileRepresentative()!=null){auxRFH.setFileRepresentative(rfh.getFileRepresentative());}
        			 									   auxRFH.setLegalRepresentativeHistory(auxLRH);
        			 if(rfh.getRegistryDate()!=null){auxRFH.setRegistryDate(rfh.getRegistryDate());}
        			 if(rfh.getRegistryType()!=null){auxRFH.setRegistryType(rfh.getRegistryType());}
        			 if(rfh.getRegistryUser()!=null){auxRFH.setRegistryUser(rfh.getRegistryUser());}
        			 if(rfh.getRepresentFileHistType()!=null){auxRFH.setRepresentFileHistType(rfh.getRepresentFileHistType());}
        			 if(rfh.getStateFile()!=null){auxRFH.setStateFile(rfh.getStateFile());}
        			 
        			     			 
        			 auxRepresentativeFileHistory.add(auxRFH);
        		 }
        		 
        		 }
        		 
        		 if(auxRepresentativeFileHistory.size()>0){
        		 auxLRH.setRepresenteFileHistory(auxRepresentativeFileHistory);
        		 }
        		 
        		 auxLstLegalRepresetantiveHistory.add(auxLRH);
        		 
        	 }
        	 }
        	
        		 
        	 HolderReqFileHistory auxHRFH;
        	 
        	if(listHolderHF!=null && listHolderHF.size()>0){
        	 for(HolderReqFileHistory hrfh :listHolderHF){
        		
        		 auxHRFH = new HolderReqFileHistory();
        		 
        		 if(hrfh.getDescription()!=null){auxHRFH.setDescription(hrfh.getDescription());}
    			 if(hrfh.getDocumentType()!=null){auxHRFH.setDocumentType(hrfh.getDocumentType());}
    			 if(hrfh.getFilename()!=null){auxHRFH.setFilename(hrfh.getFilename());}
    			 if(hrfh.getFileHolderReq()!=null){auxHRFH.setFileHolderReq(hrfh.getFileHolderReq());}
    			 								   auxHRFH.setHolderRequest(auxHolderRequest);
    			 if(hrfh.getRegistryDate()!=null){auxHRFH.setRegistryDate(hrfh.getRegistryDate());}
    			 if(hrfh.getRegistryType()!=null){auxHRFH.setRegistryType(hrfh.getRegistryType());}
    			 if(hrfh.getRegistryUser()!=null){auxHRFH.setRegistryUser(hrfh.getRegistryUser());}
    			 if(hrfh.getHolderReqFileHisType()!=null){auxHRFH.setHolderReqFileHisType(hrfh.getHolderReqFileHisType());}
    			 if(hrfh.getStateFile()!=null){auxHRFH.setStateFile(hrfh.getStateFile());} 
    			 
    			 auxHolderReqFileHistory.add(auxHRFH);
             }
        	}
        	 
        	 if(auxLstLegalRepresetantiveHistory.size()>0){
        		 auxHolderRequest.setLegalRepresentativeHistories(auxLstLegalRepresetantiveHistory);
        	 }
        	 if(auxHolderReqFileHistory!=null && auxHolderReqFileHistory.size()>0){
        		 auxHolderRequest.setHolderReqFileHistories(auxHolderReqFileHistory);
        	 }
        	 
        	 return auxHolderRequest;
     }
        
	/**
	 * Find modify report.
	 *
	 * @param idRequest the id request
	 * @param nameReport the name report
	 * @return the report logger file
	 */
	public ReportLoggerFile findModifyReport(Long idRequest, String nameReport){
		ReportLoggerFile reportLoggerFile = new ReportLoggerFile();
		try {
			reportLoggerFile = holderServiceBean.findModifyReport(idRequest, nameReport);
		} catch (NullPointerException e) {
			reportLoggerFile = null;
		}
		return reportLoggerFile;
	} 
	
	/**
	 * Find file modify report.
	 *
	 * @param idRequest the id request
	 * @param nameReport the name report
	 * @return the byte[]
	 */
	public byte[] findFileModifyReport(Long idRequest, String nameReport){
		byte[] arreglo = holderServiceBean.findFileHolderModifyReport(idRequest, nameReport);
		return arreglo;
	}
	public String findFileModifyReportPath(String nameReport){		
		return holderServiceBean.findFileHolderModifyReportPath(nameReport);		
	} 
	
     /**
      * Gets the represented entity by id legal representative facade bean.
      *
      * @param id the id
      * @return the represented entity by id legal representative facade bean
      * @throws ServiceException the service exception
      */
     public RepresentedEntity getRepresentedEntityByIdLegalRepresentativeFacadeBean(Long id)throws ServiceException{
    	return holderServiceBean.getRepresentedEntityByIdLegalRepresentativeServiceBean(id);
     }
     
     /**
      * Cancel rnt request.
      *
      * @param indicatorMaxDays the indicator max days
      * @throws ServiceException the service exception
      */
     public void CancelRntRequest(Integer indicatorMaxDays)throws ServiceException {
    	 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	     loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
    	 holderServiceBean.CancelRntRequest(indicatorMaxDays,loggerUser);
     }
     
     /**
      * Gets the count participant with account holder service facade.
      *
      * @param idParticipantPk the id participant pk
      * @param idHolderFk the id holder fk
      * @return the count participant with account holder service facade
      * @throws ServiceException the service exception
      */
     public int getCountParticipantWithAccountHolderServiceFacade(Long idParticipantPk, Long idHolderFk)throws ServiceException{
     	return holderServiceBean.getCountParticipantWithAccountHolderServiceBean(idParticipantPk, idHolderFk);
     }
     
     /**
      * Gets the history rnt holder service facade.
      *
      * @param rntCode the rnt code
      * @return the history rnt holder service facade
      * @throws ServiceException the service exception
      */
     public List<HolderHistoryState> getHistoryRNTHolderServiceFacade(Long rntCode)throws ServiceException {
    	 return holderServiceBean.getHistoryRNTHolderServiceBean(rntCode);
     }
     
     /**
      * Gets the holder request by rnt code service facade.
      *
      * @param rntCode the rnt code
      * @return the holder request by rnt code service facade
      * @throws ServiceException the service exception
      */
     public HolderRequest  getHolderRequestByRntCodeServiceFacade(Long rntCode)throws ServiceException {
    	 return holderServiceBean.getHolderRequestByRntCodeServiceBean(rntCode);
     }
     
     /**
      * Gets the next correlative secuence.
      *
      * @param correlativeType the correlative type
      * @return the next correlative secuence
      */
     public Long getNextCorrelativeSecuence(RequestCorrelativeType correlativeType){
 		return  holderServiceBean.getNextCorrelativeOperations(correlativeType);		
 	 }
     
     
     /**
      * metodo para obtener un parameter table
      * @param idParameterTablePk
      * @return
      */
	public Long getRangeCuiAvailableFromParameterTable(Integer idParameterTablePk) {
		try {
			ParameterTable parameterTable = crudDaoServiceBean.find(ParameterTable.class, idParameterTablePk);
			if (parameterTable != null)
				return Long.parseLong(parameterTable.getText1());
			else
				return null;
		} catch (Exception e) {
			return null;
		}
	}
	
     
     /**
      * metodo para retornar los cuis diponibles de un determinado rango
      * @param initialCui
      * @param finalCui
      * @return
      */
	public List<Long> getListCuiAvailable(Long initialCui, Long finalCui) {
		List<Long> listCuiNotAvailable = holderServiceBean.getListCuiNotAvailableOfOneRange(initialCui, finalCui);
		List<Long> listCuiAvailable = new ArrayList<>();
		Long i;
		boolean isEquals = false;
		for (i = initialCui; i <= finalCui; i++) {
			for (Long cuiNotAvailable : listCuiNotAvailable)
				if (i.equals(cuiNotAvailable)) {
					isEquals = true;
					break;
				}

			if (!isEquals)
				listCuiAvailable.add(i);

			isEquals = false;
		}
		return listCuiAvailable;
	}
      
      /* (non-Javadoc)
       * @see com.pradera.core.component.daogeneric.service.CrudDaoServiceBean#getCurrValSequence(com.pradera.model.operations.RequestCorrelativeType)
       */
      public Long getCurrValSequence(RequestCorrelativeType correlativeType) {
  		return  holderServiceBean.getCurrValSequence(correlativeType);		
  	 }
     
    /**
     * Gets the holder history photo.
     *
     * @param idHolderHistory the id holder history
     * @return the holder history photo
     * @throws ServiceException the service exception
     */
    public byte[] getHolderHistoryPhoto(Long idHolderHistory) throws ServiceException{
  		return holderServiceBean.getHolderHistoryPhoto(idHolderHistory);
  	}
    
    /**
     * Gets the holder history signing.
     *
     * @param idHolderHistory the id holder history
     * @return the holder history signing
     * @throws ServiceException the service exception
     */
    public byte[] getHolderHistorySigning(Long idHolderHistory) throws ServiceException{
  		return holderServiceBean.getHolderHistorySigning(idHolderHistory);
  	}
    
    public List<ProcessFileTO> getHolderMassiveProcessedFilesInformation(
			Map<String, Object> parameters) {
		List<ProcessFileTO> holderFiles = new ArrayList<ProcessFileTO>(); 
		List<Object[]> objects = holderServiceBean.getHolderMassiveFilesInformation(parameters);
		for (Object[] object : objects) {
			ProcessFileTO file = new ProcessFileTO();
			file.setIdProcessFilePk((Long)object[0]);
			file.setFileName(object[1]!=null?(String)object[1]:null);
			file.setProcessDate((Date)object[3]);
			file.setAcceptedOperations(object[5] == null ? 0 :((BigDecimal)object[5]).intValue());
			file.setRejectedOperations(object[6] == null ? 0 : ((BigDecimal)object[6]).intValue());
			file.setTotalOperations(((BigDecimal)object[7]).intValue());
			file.setSequenceNumber((Long)object[8]);
			file.setProcessStateDescription((String)object[9]);
			file.setDescription(object[10]!=null?(String)object[10]:null);
			
			holderFiles.add(file);
		}
		return holderFiles;
	}
}
