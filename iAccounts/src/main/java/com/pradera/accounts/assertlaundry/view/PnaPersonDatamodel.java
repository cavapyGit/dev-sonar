package com.pradera.accounts.assertlaundry.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.accounts.PnaPerson;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PnaPersonDatamodel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/08/2015
 */
public class PnaPersonDatamodel extends ListDataModel<PnaPerson> implements SelectableDataModel<PnaPerson>, Serializable {

	/**
	 * Instantiates a new pna person datamodel.
	 */
	public PnaPersonDatamodel(){

	}
	
	/**
	 * Instantiates a new pna person datamodel.
	 *
	 * @param data the data
	 */
	public PnaPersonDatamodel(List<PnaPerson> data){
		super(data);

	}



	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public PnaPerson getRowData(String rowKey) {
		
		List<PnaPerson> person = (List<PnaPerson>) getWrappedData();
		
		for(PnaPerson p: person){
			if(p.getIdPnaPersonPk()==Long.parseLong(rowKey))
				return p;
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(PnaPerson pnaPerson) {
		// TODO Auto-generated method stub
		return pnaPerson.getIdPnaPersonPk();
	}

}
