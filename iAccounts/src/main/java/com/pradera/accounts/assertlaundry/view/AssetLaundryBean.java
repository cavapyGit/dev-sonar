package com.pradera.accounts.assertlaundry.view;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import com.pradera.accounts.assetlaundry.facade.AssetLaundryFacade;
import com.pradera.accounts.assetlaundry.service.AssetLaundryServiceBean;
import com.pradera.accounts.util.OfacPersonTO;
import com.pradera.accounts.util.PepPersonTO;
import com.pradera.accounts.util.PnaPersonTO;
import com.pradera.accounts.util.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.security.model.type.LogoutMotiveType;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.OfacPerson;
import com.pradera.model.accounts.PepPerson;
import com.pradera.model.accounts.PnaPerson;
import com.pradera.model.accounts.type.AssertLaundryType;
import com.pradera.model.accounts.type.AssetLaundryFileType;
import com.pradera.model.accounts.type.PepChargeType;
import com.pradera.model.accounts.type.PepMotiveType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
//import com.pradera.model.accounts.type.SourceInfoAssetLaundryType;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary. Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class AssetLaundryBean.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , Mar 25, 2013
 */
@DepositaryWebBean
@LoggerCreateBean
public class AssetLaundryBean extends GenericBaseBean implements Serializable {


	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The asset laundry facade. */

	@EJB
	private AssetLaundryFacade assetLaundryFacade;

	/** The general parameter facade. */
	@EJB
	private GeneralParametersFacade generalParameterFacade;

	/** The batch process service facade. */
	@EJB
	private BatchProcessServiceFacade batchProcessServiceFacade;

	/** The log. */
	@Inject
	private transient PraderaLogger log;

	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	private UserPrivilege userPrivilege;

	
	/** The selected pna person. */
	private PnaPerson selectedPnaPerson;

	/** The selected pep person. */
	private PepPerson selectedPepPerson;

	/** The selected ofac person. */
	private OfacPerson selectedOfacPerson;

	/** The pna data model. */
	private PnaPersonDatamodel pnaDataModel;

	/** The ofac data model. */
	private OfacPersonDataModel ofacDataModel;

	/** The pep data model. */
	private PepPersonDataModel pepDataModel;

	/** The pna person to. */
	private PnaPersonTO pnaPersonTO = new PnaPersonTO();

	/** The pep person to. */
	private PepPersonTO pepPersonTO = new PepPersonTO();

	/** The ofac person to. */
	private OfacPersonTO ofacPersonTO = new OfacPersonTO();

	/** The pna person. */
	private PnaPerson pnaPerson = new PnaPerson();

	/** The pep person. */
	private PepPerson pepPerson = new PepPerson();

	/** The pep person render. */
	private Boolean pepPersonRender;

	/** The pna person render. */
	private Boolean pnaPersonRender;

	/** The ofac result render. */
	private Boolean ofacResultRender;

	/** The render pep table. */
	private Boolean renderPepTable = Boolean.FALSE;

	/** The render. */
	private Boolean render = Boolean.FALSE;

	/** The render ofactable. */
	private Boolean renderOfactable = Boolean.FALSE;

	/** The render field set. */
	private Boolean renderFieldSet = Boolean.FALSE;

	/** The ofac person render. */
	private Boolean ofacPersonRender;

	/** The register pep person render. */
	private Boolean registerPepPersonRender;

	/** The register pna person render. */
	private Boolean registerPnaPersonRender;

	/** The person type selected. */
	private Integer personTypeSelected;

	/** The file type. */
	private Integer fileType;

	/** The person type. */
	private Integer personType;

	/** The file. */
	private UploadedFile file;

	/** The list country. */
	private List<GeographicLocation> listCountry;

	/** The list source info. */
	private List<ParameterTable> listSourceInfo;

	/** The list pep charge type. */
	private List<ParameterTable> listPepChargeType;

	/** The list pep motive type. */
	private List<ParameterTable> listPepMotiveType;
	
	/** The render names. */
	private Boolean renderNames;
	
	/** The render social. */
	private Boolean renderSocial;

	/** The lst document type. */
	private List<ParameterTable> lstDocumentType;

	/** The lst pep classification. */
	private List<ParameterTable> lstPepClassification;
	
	/**
	 * Gets the render names.
	 *
	 * @return the render names
	 */
	public Boolean getRenderNames() {
		return renderNames;
	}

	/**
	 * Sets the render names.
	 *
	 * @param renderNames the new render names
	 */
	public void setRenderNames(Boolean renderNames) {
		this.renderNames = renderNames;
	}

	/**
	 * Gets the render social.
	 *
	 * @return the render social
	 */
	public Boolean getRenderSocial() {
		return renderSocial;
	}

	/**
	 * Sets the render social.
	 *
	 * @param renderSocial the new render social
	 */
	public void setRenderSocial(Boolean renderSocial) {
		this.renderSocial = renderSocial;
	}


	/** The list pna motive. */
	private List<ParameterTable> listPnaMotive;

	/** The list pep motive. */
	private List<PepMotiveType> listPepMotive;

	/** The list pep charge. */
	private List<PepChargeType> listPepCharge;

	/** The list laundry type. */
	private List<AssertLaundryType> listLaundryType;

	/** The list laundry type register. */
	private List<AssertLaundryType> listLaundryTypeRegister;

	/** The list file type. */
	private List<ParameterTable> listFileType;

	/** The list asset laundry type. */
	private List<ParameterTable> listAssetLaundryType;

	/** The list asset laundry type register. */
	private List<ParameterTable> listAssetLaundryTypeRegister;



	/** The pna person list. */
	private List<PnaPerson> pnaPersonList;

	/** The pep person list. */
	private List<PepPerson> pepPersonList;

	/** The ofac person list. */
	private List<OfacPerson> ofacPersonList;

	/** The file type list. */
	private List<AssetLaundryFileType> fileTypeList;

	/** The data table result. */
	@SuppressWarnings("rawtypes")
	private List<Map> dataTableResult;


	/** The person type description. */
	private String personTypeDescription;
	
	/** The list ofac person type. */
	private List<PersonType> listOfacPersonType;
	
	/** The asset laundry service. */
	@Inject
	private AssetLaundryServiceBean assetLaundryService;

	/**
	 * Registrar Proceso de Informacion Cruzada.
	 */

	@LoggerAuditWeb
	public void registerCrossInformationProcess() {
		BusinessProcess cinBusinessProcess = new BusinessProcess();
		cinBusinessProcess.setIdBusinessProcessPk(BusinessProcessType.ASSET_LAUNDERING_COMPARING_INFORMATION_PROCESS.getCode());

		Map<String, Object> param = new HashMap<String, Object>();
		try {
			batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(), cinBusinessProcess, param);
			
			showMessageOnDialog(null, null,PropertiesConstants.ADM_ASSET_LAUNDRY_CONFIRMED_CORSS_INFORMATION_PROCESS,
								null);
			JSFUtilities.showComponent("frmSearchPersonType:cnfConfirmedCrossInformation");


		} catch (ServiceException se) {
			log.error(se.getMessage());
			excepcion.fire(new ExceptionToCatchEvent(se));
		}
	}

	/**
	 * Confirmar registro de Proceso de Informacion Cruzada
	 */
	public void confirmRegisterCrossInformationProcess() {

		showMessageOnDialog(null, null,
				PropertiesConstants.ADM_ASSET_LAUNDRY_CONFIRM_CROSS_INFORMATION_PROCESS,
				null);
		JSFUtilities.showComponent("frmSearchPersonType:cnfConfirmCrossInformation");

	}



	/**
	 * Load detail information.
	 *
	 * @return the string
	 */
	public String loadDetailInformation(){
		setViewOperationType(ViewOperationsType.DETAIL.getCode());
		personTypeDescription = AssertLaundryType.get(personTypeSelected).getValue();
		return "detailPna";

	}
	
	/**
	 * Render ofac fields listener.
	 */
	public void renderOfacFieldsListener(){
		if(ofacPersonTO.getOfacPersonType()!=null){
			if(ofacPersonTO.getOfacPersonType().equals(PersonType.JURIDIC.getCode())){
				renderSocial=Boolean.TRUE;
				renderNames= Boolean.FALSE;
 			}
			else if(ofacPersonTO.getOfacPersonType().equals(PersonType.NATURAL.getCode())){
				renderNames=Boolean.TRUE;
				renderSocial=Boolean.FALSE;
 			}
		}
		clearGrill();
	}

	/**
	 * Gets the assert laundry type list.
	 * 
	 * @return the assert laundry type list
	 */

	@LoggerAuditWeb
	public void getAssertLaundryTypeList() {
		ParameterTableTO personTypeTO = new ParameterTableTO();
		personTypeTO.setMasterTableFk(MasterTableType.ASSET_LAUNDRY_PERSON_TYPE
				.getCode());


		listLaundryType = AssertLaundryType.list;
		try {
			listAssetLaundryType = generalParameterFacade
					.getListParameterTableServiceBean(personTypeTO);
			if(Validations.validateIsNull(listAssetLaundryType)){
				excepcion.fire(new ExceptionToCatchEvent(new Exception("Error")));
			}
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		listLaundryTypeRegister= AssertLaundryType.list;

	}

	/**
	 * Instantiates a new asset laundry bean.
	 */
	public AssetLaundryBean() {
	}

	/**
	 * Gets the country list.
	 * 
	 * @return the country list
	 */
	@LoggerAuditWeb
	public void getCountryList() {
		try{
			listCountry = assetLaundryFacade.getCountryList();
		}catch(ServiceException se){
			log.info(se.getMessage());
			excepcion.fire(new ExceptionToCatchEvent(se));
		}
	}

	/**
	 * Gets the list of document types.
	 *
	 * @return the list of document types
	 * @throws ServiceException the service exception
	 */
	private void getlistOfDocumentTypes() throws ServiceException{
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		filterParameterTable.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
		lstDocumentType = generalParameterFacade.getListParameterTableServiceBean(filterParameterTable);
	}
	
	/**
	 * Gets the list of pep classification.
	 *
	 * @return the list of pep classification
	 * @throws ServiceException the service exception
	 */
	private void getlistOfPepClassification() throws ServiceException{
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		filterParameterTable.setMasterTableFk(MasterTableType.PEP_CLASSIFICATION.getCode());
		lstPepClassification = generalParameterFacade.getListParameterTableServiceBean(filterParameterTable);
	}
	/**
	 * Hide dialogues.
	 */
	public void hideDialogues() {
		//
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		JSFUtilities.hideGeneralDialogues();
		JSFUtilities.hideComponent("frmRegistrarPNA:cnfGoBack");
		JSFUtilities.hideComponent("frmRegistrarPNA:cnfConfirmProcess");
		JSFUtilities.hideComponent("frmRegisterOfac:cnfConfirmProcess");
		JSFUtilities.hideComponent("frmRegisterOfac:cnfGoBack");
	}

	/**
	 * Gets the source info list.
	 * 
	 * @return the source info list
	 */

	@LoggerAuditWeb
	public void getSourceInfoList() {
		ParameterTableTO parameterFilter = new ParameterTableTO();
		parameterFilter
		.setMasterTableFk(MasterTableType.ASSET_LAUNDRY_SOURCE_INFO
				.getCode());
		parameterFilter.setState(1);

		// List<ParameterTable> parameterList = new ArrayList<ParameterTable>();
		try {
			listSourceInfo = generalParameterFacade
					.getListParameterTableServiceBean(parameterFilter);

		} catch (ServiceException se) {
			log.info(se.getMessage());
			excepcion.fire(new ExceptionToCatchEvent(se));
		}

	}

	/**
	 * Load file type list.
	 */
	@LoggerAuditWeb
	@SuppressWarnings("unused")
	private void loadFileTypeList() {

		ParameterTableTO parameterFilter = new ParameterTableTO();
		parameterFilter
		.setMasterTableFk(MasterTableType.ASSET_LAUNDRY_FILE_TYPE
				.getCode());
		parameterFilter.setState(1);
		try {
			listFileType = generalParameterFacade
					.getListParameterTableServiceBean(parameterFilter);
		} catch (ServiceException se) {
			log.info(se.getMessage());
			excepcion.fire(new ExceptionToCatchEvent(se));

		}

	}

	/**
	 * Register change listener.
	 * Determine which panel to display
	 */
	public void registerChangeListener() {
		JSFUtilities.resetComponent("frmRegistrarPNA:iptextPnaFullname");
		JSFUtilities.resetComponent("frmRegistrarPNA:iptextPnaLastname");
		JSFUtilities.resetComponent("frmRegistrarPNA:cmbPnaCountry");
		JSFUtilities.resetComponent("frmRegistrarPNA:calendarPnaDate");
		JSFUtilities.resetComponent("frmRegistrarPNA:inputTextMotivePna");
		JSFUtilities.resetComponent("frmRegistrarPNA:cmbSourceINfoPna");
		JSFUtilities.resetComponent("frmRegistrarPNA:optExpeditionCountry");
		JSFUtilities.resetComponent("frmRegistrarPNA:optDocType");
		JSFUtilities.resetComponent("frmRegistrarPNA:optDocNumber");
		
		JSFUtilities.resetComponent("frmRegistrarPNA:iptextPepFullname");
		JSFUtilities.resetComponent("frmRegistrarPNA:iptextPepLastname");
		JSFUtilities.resetComponent("frmRegistrarPNA:cmbPepCountry");
		JSFUtilities.resetComponent("frmRegistrarPNA:datePepPerson");
		JSFUtilities.resetComponent("frmRegistrarPNA:cmbPepMotive");
		JSFUtilities.resetComponent("frmRegistrarPNA:cmbPepCharge");
		JSFUtilities.resetComponent("frmRegistrarPNA:optExpCountryPep");
		JSFUtilities.resetComponent("frmRegistrarPNA:optDocTypePep");
		JSFUtilities.resetComponent("frmRegistrarPNA:optDocNumberPep");
		JSFUtilities.resetComponent("frmRegistrarPNA:optClassification");

 		if (personType.equals(new Integer(-1))) {
			this.clear();
			this.clearFieldsPanels();
		} else if (personType.equals(AssertLaundryType.PEP.getCode())) {
			registerPepPersonRender = true;
			registerPnaPersonRender = false;
		}else if (personType.equals(AssertLaundryType.PNA.getCode())) {
			registerPepPersonRender = false;
			registerPnaPersonRender = true;
		}
	}

	/**
	 * Go back ofac register massive.
	 *
	 * @return the string
	 */
	public String goBackOfacRegisterMassive(){
		hideDialogues();
		String navigationRule = "";

		if(Validations.validateIsNull(fileType)){
			navigationRule = "assetLaundrySearch";
		}else{
			if(fileType.equals(new Integer(-1))){
				navigationRule = "assetLaundrySearch";
			}else{
				if(Validations.validateIsNotNull(file) || Validations.validateIsNotNull(fileType)){
					JSFUtilities.showComponent("frmRegisterOfac:cnfGoBack");
				}else{
					navigationRule= "assetLaundrySearch";
				}
			}
		}
		return navigationRule;
	}

	/**
	 * Go back to search.
	 *
	 * @return the string
	 */
	public String goBackToSearch(){
		hideDialogues();
		String navigationRule = "";

		if(Validations.validateIsNull(personType) || personType==-1){
			navigationRule = "assetLaundrySearch";
		}else{
			JSFUtilities.showComponent("frmRegistrarPNA:cnfGoBack");
		}
		return navigationRule;
	}
	
	/** The search navigation rule. */
	private String searchNavigationRule = "";

	/**
	 * Go back listener.
	 */
	public void goBackListener(){
		JSFUtilities.hideComponent("frmRegistrarPNA:cnfConfirmDialog");

		log.info("go Back listener"+pnaPerson.getComment());
		if(personType.equals(new Integer(-1))){
			searchNavigationRule="assetLaundrySearch";
		}else{
			JSFUtilities.showComponent("frmRegistrarPNA:cnfGoBack");

		}
	}

	/**
	 * Go back.
	 *
	 * @return the string
	 */
	public String goBack() {
		log.info("Pna FullName" + pnaPerson.getFirstName());
		if(Validations.validateIsNotNull(pnaPerson.getFirstName())){

			if(pnaPerson.getFirstName().length()>0){
				JSFUtilities.showComponent("frmRegistrarPNA:cnfGoBack");
			}
			else{
				searchNavigationRule="assetLaundrySearch";
			}
		}
		else{
			searchNavigationRule="assetLaundrySearch";
		}
		return searchNavigationRule;
	}

	/**
	 * Change listener.
	 * Determine which panel to display
	 *
	 * @throws ServiceException the service exception
	 */
	public void changeListener() throws ServiceException {
		//clear();
		getCountryList();
		getlistOfDocumentTypes();
		getlistOfPepClassification();
		if (personTypeSelected==null) {
			this.clear();
			this.clearFieldsPanels();
		}else{
			if (personTypeSelected.equals(AssertLaundryType.PEP.getCode())) {
				pepPersonRender = true;
				pnaPersonRender = false;
				ofacPersonRender = false;
				render = false;
				renderOfactable = false;
				registerPepPersonRender = true;
				registerPnaPersonRender = false;
			} else if (personTypeSelected.equals(AssertLaundryType.PNA.getCode())) {
				pnaPersonRender = true;
				pepPersonRender = false;
				ofacPersonRender = false;
				renderPepTable = false;
				renderOfactable = false;
				registerPepPersonRender = false;
				registerPnaPersonRender = true; 
				pnaPersonTO.setStartDate(new Date());
				pnaPersonTO.setEndDate(new Date());
			} else if (personTypeSelected.equals(AssertLaundryType.OFAC.getCode())) {
				ofacPersonRender = true;
				pepPersonRender = false;
				pnaPersonRender = false;
				render = false;
				renderPepTable = false;
				ofacPersonTO.setInitialDate(new Date());
				ofacPersonTO.setEndDate(new Date());
			}
		}
	}

	/**
	 * Clear all the panel.
	 */
	public void clearAll() {
		issuanceCertFileNameDisplay=null;

		//this.clear();
		this.clearFieldsPanels();
		fileType = -1;
		personTypeSelected=-1;
		//		JSFUtilities.resetComponent("frmSearchPersonType:slctPersonType");
	}

	/**
	 * Clear grill.
	 */
	public void clearGrill(){
		if(renderFieldSet==true){
			render = Boolean.FALSE;
			renderPepTable = Boolean.FALSE;
			renderOfactable = Boolean.FALSE;
		}
	}
	
	/**
	 * Confirmar antes de Guardar
	 */
	public void confirmSave() {

		hideDialogues();
		Object[] bodyData= new Object[1];

		if (personType.equals(AssertLaundryType.PNA.getCode())) {
			bodyData[0] = pnaPerson.getFirstName() + " " + pnaPerson.getLastName();
			showMessageOnDialog(null, null,
					PropertiesConstants.ADM_ASSETLAUNDRY_PNA_CONFIRM_REGISTER,
					bodyData);
			JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");//colocar pf(

		} else if (personType.equals(AssertLaundryType.PEP.getCode())) {
			bodyData[0] = pepPerson.getFirstName() + " " + pepPerson.getLastName();

			showMessageOnDialog(null, null,
					PropertiesConstants.ADM_ASSETLAUNDRY_PEP_CONFIRM_REGISTER,
					bodyData);
			JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
		}else{
			JSFUtilities.addContextMessage("frmRegistrarPNA:slctoneType",
					FacesMessage.SEVERITY_ERROR, "Debe Seleccionar el  tipo de Persona", "Debe Seleccionar el  tipo de Persona");

		}
	}

	/**
	 * Mehtod Save
	 * to Persist the Entity in the database.
	 */

	@LoggerAuditWeb
	public void save() {

		hideDialogues();

		Object[] bodyData = new Object[2];
		String nombre="", apellido="";
		String nombreBd="", apellidoBd="";
		Integer idCountryView=null;
		boolean flag=true;
		try{
			//Determine which entity is selected
			if (personType.equals(AssertLaundryType.PNA.getCode())) {
				if(pnaPerson.getFirstName()!=null){
					nombre=pnaPerson.getFirstName();
				}
				if(pnaPerson.getLastName()!=null){
					apellido=pnaPerson.getLastName();
				}

				PnaPersonTO objPna=new PnaPersonTO();
				idCountryView=pnaPerson.getCountry().getIdGeographicLocationPk();
				List<PnaPerson> lstPna=null;
				lstPna = assetLaundryService.getPnaPersonList(objPna);
				
				for (int i = 0; i < lstPna.size(); i++) {
					if(pnaPerson.getFirstName()!=null){
						nombreBd=lstPna.get(i).getFirstName();
					}
					if(pnaPerson.getLastName()!=null){
						apellidoBd=lstPna.get(i).getLastName();
					}
					Integer idCountryBd=null;
					if(lstPna.get(i).getCountry().getIdGeographicLocationPk()!=null){
						idCountryBd=lstPna.get(i).getCountry().getIdGeographicLocationPk();
					}
					
					if(nombre.equals(nombreBd) && apellido.equals(apellidoBd) && idCountryView.equals(idCountryBd)){
						flag=false;//compare names and country from Database
						break;
					}
				}
				if(flag){
					assetLaundryFacade.savePnaPerson(pnaPerson);
					bodyData[0] = nombre + " " + apellido;
					bodyData[1] = pnaPerson.getIdPnaPersonPk().toString();
					showMessageOnDialog(null, null,
							PropertiesConstants.ADM_ASSETLAUNDRY_PNA_REGISTER_OK,
							bodyData);
					JSFUtilities.executeJavascriptFunction("cnfwConfirmedDialog.show();");
				}else{
					JSFUtilities.executeJavascriptFunction("wvClose.show();");
				}
			} else if (personType.equals(AssertLaundryType.PEP.getCode())) {
				if(pepPerson.getFirstName()!=null){
					nombre=pepPerson.getFirstName();
				}
				if(pepPerson.getLastName()!=null){
					apellido=pepPerson.getLastName();
				}

				PepPersonTO objPep=new PepPersonTO();
				idCountryView=pepPerson.getGeographicLocation().getIdGeographicLocationPk();
				List<PepPerson> lstPep=null;
				lstPep = assetLaundryService.getPepPersonList(objPep);
				
				for (int i = 0; i < lstPep.size(); i++) {
					if(pepPerson.getFirstName()!=null){
						nombreBd=lstPep.get(i).getFirstName();
					}
					if(pepPerson.getLastName()!=null){
						apellidoBd=lstPep.get(i).getLastName();
					}
					Integer idCountryBd=null;
					if(lstPep.get(i).getGeographicLocation().getIdGeographicLocationPk()!=null){
						idCountryBd=lstPep.get(i).getGeographicLocation().getIdGeographicLocationPk();
					}
					
					if(nombre.equals(nombreBd) && apellido.equals(apellidoBd) && idCountryView.equals(idCountryBd)){
						flag=false;//compare names and country from Database
						break;
					}
				}
				if(flag){
					assetLaundryFacade.savePepPerson(pepPerson);
					bodyData[0] = nombre + " " + apellido;
					bodyData[1] = pepPerson.getIdPepPersonPk().toString();
					showMessageOnDialog(null, null,
							PropertiesConstants.ADM_ASSETLAUNDRY_PEP_REGISTER_OK,
							bodyData);
					JSFUtilities.executeJavascriptFunction("cnfwConfirmedDialog.show();");
				}else{
					JSFUtilities.executeJavascriptFunction("wvClose.show();");
				}
			}
		}catch(ServiceException se){
			excepcion.fire(new ExceptionToCatchEvent(se));
		}
	}

	/**
	 * Cargar Registro de Lavado de Activods
	 *
	 * @throws ServiceException the service exception
	 */

	@LoggerAuditWeb
	public void loadRegisterAssetLaundry() throws ServiceException {
		setViewOperationType(ViewOperationsType.REGISTER.getCode());

		ParameterTableTO toPepMotive = new ParameterTableTO();
		ParameterTableTO toPepCharge = new ParameterTableTO();

		toPepCharge.setMasterTableFk(MasterTableType.PEP_CHARGE.getCode());
		toPepMotive.setMasterTableFk(MasterTableType.ASSET_LAUNDRY_MOTIVE_PEP.getCode());

		personTypeSelected = -1;
		searchNavigationRule="";
		personType = -1;
		this.clearFieldsPanels();
		this.setPnaPerson(new PnaPerson());
		this.setPepPerson(new PepPerson());
		getPepPerson().setExpeditionPlace(new GeographicLocation());
		getPepPerson().setGeographicLocation(new GeographicLocation());
		getPnaPerson().setExpeditionPlace(new GeographicLocation());
		getPnaPerson().setCountry(new GeographicLocation());
		getCountryList();
		getlistOfDocumentTypes();
		getlistOfPepClassification();
		getSourceInfoList();
		ParameterTableTO personTypeTO = new ParameterTableTO();
		//personTypeTO.setMasterTableFk(MasterTableType.ASSET_LAUNDRY_PERSON_TYPE.getCode());
		personTypeTO.setState(1);
		List<Integer> lstPersonCodes= new ArrayList<Integer>();
		lstPersonCodes.add(AssertLaundryType.PNA.getCode());
		lstPersonCodes.add(AssertLaundryType.PEP.getCode());
		personTypeTO.setLstParameterTablePk(lstPersonCodes); 
		try {
			listAssetLaundryTypeRegister = generalParameterFacade.getListParameterTableServiceBean(personTypeTO);
			listPepMotiveType = generalParameterFacade.getListParameterTableServiceBean(toPepMotive);
			listPepChargeType = generalParameterFacade.getListParameterTableServiceBean(toPepCharge);
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		pepPerson.setRegistryDate(CommonsUtilities.currentDate());
		pnaPerson.setRegistryDate(CommonsUtilities.currentDate());
	}

	/**
	 * Load massive register asset laundry.
	 */
	public void loadMassiveRegisterAssetLaundry() {
		setViewOperationType(ViewOperationsType.REGISTER.getCode());
 		file = null;
		loadFileTypeList();
	}

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		listOfacPersonType = PersonType.list;
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		personType = 0;
		getAssertLaundryTypeList();
		fileTypeList = AssetLaundryFileType.list;

		//Initializate variables
		pnaPerson = new PnaPerson();
		pnaPerson.setCountry(new GeographicLocation());
		pnaPerson.setExpeditionPlace(new GeographicLocation());
		pepPerson = new PepPerson();
		pepPerson.setGeographicLocation(new GeographicLocation());
		pepPerson.setExpeditionPlace(new GeographicLocation());
		renderNames=Boolean.FALSE;
		renderSocial=Boolean.FALSE;

	}

	/**
	 * Upload.
	 * 
	 * @param event
	 * Upload the OFAC File
	 * 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@LoggerAuditWeb
	public void upload(ActionEvent event) {
		hideDialogues();
		Object[] bodyData = new Object[1];
		
		InputStream inputStream = null;
		OutputStream out = null;
		
		if(Validations.validateIsNotNull(file)){
			// Convert FileUpload InputStream to a XML
			try {
				inputStream = file.getInputstream();
				String physicalName=file.getFileName();
				bodyData[0] = file.getFileName();
				if (fileType.equals(AssetLaundryFileType.OFAC.getCode())) {
					
					out = new FileOutputStream(physicalName);
					
					int read = 0;
					byte[] bytes = new byte[1024];
					while ((read = inputStream.read(bytes)) != -1) {
						out.write(bytes, 0, read);
					}
					// Finish conversion
					//Create the business process
					BusinessProcess bp = new BusinessProcess();
					//Asign the busnies process
					bp.setIdBusinessProcessPk(BusinessProcessType.OFAC_FILE_PROCESS.getCode());
					Map<String, Object> param = new HashMap<String, Object>();
					try {
						batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(), bp, param);
						issuanceCertFileNameDisplay="";
						showMessageOnDialog(null, null,
								PropertiesConstants.ADM_ASSETLAUNDRY_PFAC_FILE_OK, bodyData);
					} catch (ServiceException se) {
						log.error(se.getMessage());
						excepcion.fire(new ExceptionToCatchEvent(se));
					}
					
				} else if (fileType.equals(AssetLaundryFileType.PNA_PEP.getCode())) {
					// Convert FileUpload InputStream to a .XLS
					out = new FileOutputStream("pna_pep.xls");
					int read = 0;
					byte[] bytes = new byte[1024];

					while ((read = inputStream.read(bytes)) != -1) {

						out.write(bytes, 0, read);

					} // End conversion
					try{
						assetLaundryFacade.processExcelFile(new File("pna_pep.xls"));
						//Map<String,List>temp = assetLaundryFacade.processExcelFile(new File("pna_pep.xlsx"));
					}catch (ServiceException e) {
						if(e.getErrorService().equals(ErrorServiceType.ASSET_LAUNDERING_PROCESS_MASSIVE_FILE)){
							Object[] Params = e.getParams();
							String line = Params[0].toString();
							showMessageOnDialog("Mensaje de Error", "No se pudo procesar el archivo correctamente, favor verifique linea: "+line);
							JSFUtilities.showComponent("frmRegisterOfac:cnfBackTransaction");
							return;
							
						}
 					}
					showMessageOnDialog(null, null,
							PropertiesConstants.ADM_ASSETLAUNDRY_PFAC_FILE_OK,bodyData);
				}

			} catch (Exception e) {
				e.printStackTrace();
				excepcion.fire(new ExceptionToCatchEvent(e));

			} finally {

				try {
					inputStream.close();
					out.flush();
					out.close();

				} catch (Exception e) {
					excepcion.fire(new ExceptionToCatchEvent(e));

				}

			}
		} else{


			//File is null
		}

		JSFUtilities.showComponent("frmRegisterOfac:cnfProcessConfirmed");
		ofacResultRender = true;

	}

	/**
	 * Confirm process file.
	 * Metodo para confirmar el proceso de los archivos
	 */
	public void confirmProcessFile() {
		//Hide the wrong file modal
		JSFUtilities.hideComponent("frmRegisterOfac:cnfWrongFile");
		hideDialogues();
		Object[] bodyData = new Object[1];
		String mnemonicOfac = "ofac", mnemonicPna_Pep="pna_pep";
		String currentDt=CommonsUtilities.convertDatetoString(getCurrentSystemDate());
		String extension=".xml";
		String validPathOfac=null, validPathPna_Pep=null;
		String physicalName=file.getFileName();
		
		validPathOfac=mnemonicOfac+currentDt.replace("/", "")+extension;
		validPathPna_Pep=mnemonicPna_Pep+currentDt.replace("/", "")+extension;
		
			if(Validations.validateIsNull(fileType)){
				JSFUtilities.addContextMessage("frmRegisterOfac:slconeFileType",
						FacesMessage.SEVERITY_ERROR, "Seleccione tipo de Archivo", "Seleccione tipo de Archivo");
			}else{
				if(physicalName.toLowerCase().equals(validPathOfac) || physicalName.toLowerCase().equals(validPathPna_Pep)){
					if (fileType.equals(AssetLaundryFileType.OFAC.getCode())) {
						//Validate if the file is NULL
						if(Validations.validateIsNull(file)){
							log.info("Confirm Process File method is NUll");
							JSFUtilities.addContextMessage("frmRegisterOfac:fluOfacPerson",
									FacesMessage.SEVERITY_ERROR, "ERROR", null);
						}else{//Else show Confirm Modal
							bodyData[0] = file.getFileName();

							showMessageOnDialog(null, null,
									PropertiesConstants.ADM_ASSETLAUNDRY_OFAC_FILE_TYPE, bodyData);
							JSFUtilities.showComponent("frmRegisterOfac:cnfConfirmProcess");
						}
					}else if (fileType.equals(AssetLaundryFileType.PNA_PEP.getCode())){// Determine if PNA / PEP File Type is selected

						//Validate if the file is NULL
						if(Validations.validateIsNull(file)){
							log.info("Confirm Process File method is NUll");
							JSFUtilities.addContextMessage("frmRegisterOfac:fluOfacPerson",	FacesMessage.SEVERITY_ERROR, "ERROR", null);
						}else{//Else show Confirm Modal
							bodyData[0] = file.getFileName();

							showMessageOnDialog(null, null,
									PropertiesConstants.ADM_ASSETLAUNDRY_PNA_FILE_TYPE, bodyData);

							JSFUtilities.showComponent("frmRegisterOfac:cnfConfirmProcess");
						}
					}
				}else{
					showMessageOnDialog(null, null,
							PropertiesConstants.ADM_ASSETLAUNDRY_OFAC_FILE_REJECT, bodyData);
					JSFUtilities.showComponent("frmRegisterOfac:cnfRejectFile");
				}
			}	
		}

	/**
	 *Search all Asset Laundry Info.
	 */

	@LoggerAuditWeb
	public void search() {
		renderFieldSet = Boolean.TRUE;
		setViewOperationType(ViewOperationsType.CONSULT.getCode());

		if(Validations.validateIsNull(personTypeSelected)){
			JSFUtilities
			.addContextMessage(
					"frmSearchPersonType:slctPersonType",
					FacesMessage.SEVERITY_ERROR,
					PropertiesUtilities
					.getMessage(PropertiesConstants.ADM_ASSETLAUNDRY_SELECT_PERSON),
					PropertiesUtilities
					.getMessage(PropertiesConstants.ADM_ASSETLAUNDRY_SELECT_PERSON));
		}else if (personTypeSelected.equals(AssertLaundryType.PNA.getCode())) {
			personTypeDescription = AssertLaundryType.get(personTypeSelected).getValue();

			try {
				pnaPersonList = assetLaundryFacade.getPnaPerson(pnaPersonTO);
				pnaDataModel = new PnaPersonDatamodel(pnaPersonList);
				render = true;
				renderPepTable = false;
				renderOfactable = false;
			} catch (ServiceException se) {
				excepcion.fire(new ExceptionToCatchEvent(se));
			}

		} else if (personTypeSelected.equals(AssertLaundryType.PEP.getCode())) {
			personTypeDescription = AssertLaundryType.get(personTypeSelected).getValue();

			try{
				pepPersonList = assetLaundryFacade.getPepPerson(pepPersonTO);
				pepDataModel = new PepPersonDataModel(pepPersonList);
				renderPepTable = true;
				render = false;
				renderOfactable = false;
			}catch(ServiceException se){
				log.info(se.getMessage());
				excepcion.fire(new ExceptionToCatchEvent(se));
			}
		} else if (personTypeSelected.equals(AssertLaundryType.OFAC.getCode())) {
			personTypeDescription = AssertLaundryType.get(personTypeSelected).getValue();
			try{
				ofacPersonList = assetLaundryFacade.getOfacPersonList(ofacPersonTO);
				ofacDataModel = new OfacPersonDataModel(ofacPersonList);
				renderOfactable = true;
				render = false;
				renderPepTable = false;

			}catch(ServiceException se){
				log.info(se.getMessage());
				excepcion.fire(new ExceptionToCatchEvent(se));
			}
		}
	}


	/**
	 * Clear register entity.
	 */
	public void clearRegisterEntity(){
		log.error("clearRegisterEntity method");
		this.setPnaPerson(new PnaPerson());
		this.setPepPerson(new PepPerson());
		this.getPnaPerson().setExpeditionPlace(new GeographicLocation());
		this.getPnaPerson().setCountry(new GeographicLocation());
		this.getPepPerson().setExpeditionPlace(new GeographicLocation());
		this.getPepPerson().setGeographicLocation(new GeographicLocation());
		this.setPersonType(null);
		this.setRegisterPnaPersonRender(false);
		this.setRegisterPepPersonRender(false);
		this.getPnaPerson().setRegistryDate(getCurrentDate());
		this.getPepPerson().setRegistryDate(getCurrentDate());
		JSFUtilities.hideComponent("frmRegistrarPNA:cnfConfirmDialog");
		JSFUtilities.resetComponent("frmRegistrarPNA");
	}

	/**
	 * Confirm clear entity listener.
	 */
	public void confirmClearEntityListener(){
		if(personTypeSelected!=null){
		showMessageOnDialog(null,null, PropertiesConstants.ADM_ASSET_LAUNDRY_CONFIRM_CLEAR_MESSAGE,null);
		JSFUtilities.showComponent("frmSearchPersonType:cnfConfirmClear");		
		}
	}

	/**
	 * Clear.
	 */
	public void clear() {
		renderNames=Boolean.TRUE;
		renderSocial=Boolean.FALSE;
		log.error("Clear method doing his thing");
		personType=null;
		personTypeSelected=null;
		renderFieldSet = false;
		setPnaPersonTO(new PnaPersonTO());
		setPepPersonTO(new PepPersonTO());
		setOfacPersonTO(new OfacPersonTO());
		pnaDataModel = new PnaPersonDatamodel();
		pepDataModel = new PepPersonDataModel();
		ofacDataModel = new OfacPersonDataModel();
		render = false;
		renderPepTable = false;
		renderOfactable = false;
		this.clearFieldsPanels();

		if(isViewOperationConsult()){
			JSFUtilities.hideComponent("frmSearchPersonType:cnfConfirmClear");	
			JSFUtilities.resetComponent("frmSearchPersonType:slctPersonType");
			JSFUtilities.resetComponent("frmSearchPersonType:pnaTxtRegisterNumber");


			JSFUtilities.resetComponent("frmSearchPersonType:pnaPersonTxtName");
			JSFUtilities.resetComponent("frmSearchPersonType:pnaPersonTxtLastName");

			JSFUtilities.resetComponent("frmSearchPersonType:slctCountryPna");
			JSFUtilities.resetComponent("frmSearchPersonType:calIniDatePna");
			JSFUtilities.resetComponent("frmSearchPersonType:calEndDatePna");
			JSFUtilities.resetComponent("frmSearchPersonType:pepPersonTxtRegisterNumber");
			JSFUtilities.resetComponent("frmSearchPersonType:pepPersonTxtName");

			JSFUtilities.resetComponent("frmSearchPersonType:slctMotivePep");
			JSFUtilities.resetComponent("frmSearchPersonType:calendarFin");
			JSFUtilities.resetComponent("frmSearchPersonType:calendarInic");
			JSFUtilities.resetComponent("frmSearchPersonType:ofacPersonTxtNumber");

			JSFUtilities.resetComponent("frmSearchPersonType:inpTextOfacName");

			JSFUtilities.resetComponent("frmSearchPersonType:inpTextOfacLastName");
 			JSFUtilities.resetComponent("frmSearchPersonType:calendarInicOfac");
			JSFUtilities.resetComponent("frmSearchPersonType:calendarFinOfac");
		}
		JSFUtilities.resetComponent("frmSearchPersonType");
	}

	/**
	 * Clear fields panels.
	 */
	private void clearFieldsPanels() {
		pepPersonRender = false;
		pnaPersonRender = false;
		ofacPersonRender = false;
		registerPnaPersonRender = false;
		registerPepPersonRender = false;

	}

	/**
	 * Gets the stream filter.
	 *
	 * @return the stream filter
	 */
	public StreamedContent getStreamFilter(){
		String n = file.getFileName();
		System.out.println("stream filter" + n);

		try {
			return getStreamedContentFromFile(file.getContents(), null, file.getFileName());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("will return null");
		return null;
	}

	/**
	 * Upload hanlder.
	 * 
	 * @param event
	 *            the event
	 */
	public void uploadHanlder(FileUploadEvent event) {


		try {
			//Get the file Name
			String fileName = event.getFile().getFileName().toLowerCase();

			// Long fileSize=event.getFile().getSize();
			Object[] argObj = new Object[1];
			String message;

			//Determine which file type is selected
			if (fileType.equals(AssetLaundryFileType.OFAC.getCode())) {

				//determine if the file extension is .xml
				if (fileName.lastIndexOf(".xml") == -1) {

					JSFUtilities
					.addContextMessage("frmRegisterOfac:fluOfacPerson",FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities.getMessage(PropertiesConstants.ADM_ASSETLAUNDRY_INVALID_FILE_TYPE),
							PropertiesUtilities
							.getMessage(PropertiesConstants.ADM_ASSETLAUNDRY_INVALID_FILE_TYPE));
					JSFUtilities.showComponent("frmRegisterOfac:cnfWrongFile");

					return;
				}

				file = event.getFile();
				InputStream inputStream = file.getInputstream();


				//Validate if the inputStream is null
				if(Validations.validateIsNull(inputStream)){
					JSFUtilities
					.addContextMessage("frmRegisterOfac:fluOfacPerson",FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities.getMessage(PropertiesConstants.ADM_ASSETLAUNDRY_INVALID_FILE_TYPE),
							PropertiesUtilities
							.getMessage(PropertiesConstants.ADM_ASSETLAUNDRY_INVALID_FILE_TYPE));

				}
				else{
					argObj[0] = event.getFile().getFileName();

					message = PropertiesUtilities.getMessage(
							PropertiesConstants.ADM_ASSETLAUNDRY_SUCCESS_UPLOAD,
							argObj);
					message = message + " " + event.getFile().getFileName();

					JSFUtilities.addContextMessage("frmRegisterOfac:fluOfacPerson",
							FacesMessage.SEVERITY_INFO, message, message);
				}

			} else if (fileType.equals(AssetLaundryFileType.PNA_PEP.getCode())) {
				//
				if (fileName.lastIndexOf(".xlsx") == -1) {

					JSFUtilities
					.addContextMessage(
							"frmRegisterOfac:fluOfacPerson",
							FacesMessage.SEVERITY_ERROR,

							PropertiesUtilities
							.getMessage(PropertiesConstants.ADM_ASSETLAUNDRY_INVALID_FILE_TYPE),

							PropertiesUtilities
							.getMessage(PropertiesConstants.ADM_ASSETLAUNDRY_INVALID_FILE_TYPE));


					return;
				}

				file = event.getFile();

				argObj[0] = event.getFile().getFileName();

				message = PropertiesUtilities.getMessage(
						PropertiesConstants.ADM_ASSETLAUNDRY_SUCCESS_UPLOAD,
						argObj);

				JSFUtilities.addContextMessage("frmRegisterOfac:fluOfacPerson",
						FacesMessage.SEVERITY_INFO, message, message);

			}

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}

	}

	/** The issuance cert file name display. */
	private String issuanceCertFileNameDisplay;


	/**
	 * Removes the uploaded file handler.
	 *
	 * @param event the event
	 */
	public void removeUploadedFileHandler(ActionEvent event){
		try {
			issuanceCertFileNameDisplay=null;
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}


	/**
	 * Certificate file handler.
	 *
	 * @param event the event
	 */
	public void certificateFileHandler(FileUploadEvent event) {

		if(Validations.validateIsNotNull(fileType)){

			if(fileType.equals(AssetLaundryFileType.OFAC.getCode())){
				try {

					String fDisplayName=fUploadValidateXmlFile(event.getFile(), 
							"cnfwMsgCustomValidationAcc.show();", null, null);
					uploadHanlder(event);


					if(fDisplayName!=null){
						issuanceCertFileNameDisplay=fDisplayName;

					}

				} catch (Exception e) {
					excepcion.fire(new ExceptionToCatchEvent(e));
				}

			}

			else if(fileType.equals(AssetLaundryFileType.PNA_PEP.getCode())){

				try {

					String fDisplayName=fUploadValidateXlsFile(event.getFile(), 
							"cnfwMsgCustomValidationAcc.show();", null, null);
					uploadHanlder(event);


					if(fDisplayName!=null){
						issuanceCertFileNameDisplay=fDisplayName;

					}

				} catch (Exception e) {
					excepcion.fire(new ExceptionToCatchEvent(e));
				}

			}
		}

	}

	/**
	 * Method for logout the user session on idle time of the application.
	 */
	public void sessionLogout(){
		try{
			JSFUtilities.setHttpSessionAttribute(GeneralConstants.LOGOUT_MOTIVE, LogoutMotiveType.EXPIREDSESSION);
			JSFUtilities.killSession(); 
			JSFUtilities.showMessageOnDialog(null, null,PropertiesConstants.IDLE_TIME_EXPIRED, null);
			JSFUtilities.showComponent(":idCnfDialogKillSession");
		} catch (Exception ex) {
				excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Gets the issuance cert file name display.
	 *
	 * @return the issuance cert file name display
	 */
	public String getIssuanceCertFileNameDisplay() {
		return issuanceCertFileNameDisplay;
	}

	/**
	 * Sets the issuance cert file name display.
	 *
	 * @param issuanceCertFileNameDisplay the new issuance cert file name display
	 */
	public void setIssuanceCertFileNameDisplay(String issuanceCertFileNameDisplay) {
		this.issuanceCertFileNameDisplay = issuanceCertFileNameDisplay;
	}

	/**
	 * Hide ofac table.
	 */
	public void hideOfacTable() {

		renderOfactable = false;
		renderPepTable = false;
	}



	/**
	 * Gets the render field set.
	 * 
	 * @return the render field set
	 */
	public Boolean getRenderFieldSet() {
		return renderFieldSet;
	}

	/**
	 * Sets the render field set.
	 * 
	 * @param renderFieldSet
	 *            the new render field set
	 */
	public void setRenderFieldSet(Boolean renderFieldSet) {
		this.renderFieldSet = renderFieldSet;
	}

	/**
	 * Gets the list asset laundry type register.
	 * 
	 * @return the list asset laundry type register
	 */
	public List<ParameterTable> getListAssetLaundryTypeRegister() {
		return listAssetLaundryTypeRegister;
	}

	/**
	 * Sets the list asset laundry type register.
	 * 
	 * @param listAssetLaundryTypeRegister
	 *            the new list asset laundry type register
	 */
	public void setListAssetLaundryTypeRegister(
			List<ParameterTable> listAssetLaundryTypeRegister) {
		this.listAssetLaundryTypeRegister = listAssetLaundryTypeRegister;
	}

	/**
	 * Gets the list asset laundry type.
	 * 
	 * @return the list asset laundry type
	 */
	public List<ParameterTable> getListAssetLaundryType() {
		return listAssetLaundryType;
	}

	/**
	 * Sets the list asset laundry type.
	 * 
	 * @param listAssetLaundryType
	 *            the new list asset laundry type
	 */
	public void setListAssetLaundryType(
			List<ParameterTable> listAssetLaundryType) {
		this.listAssetLaundryType = listAssetLaundryType;
	}

	/**
	 * Gets the list laundry type register.
	 * 
	 * @return the list laundry type register
	 */
	public List<AssertLaundryType> getListLaundryTypeRegister() {
		return listLaundryTypeRegister;
	}

	/**
	 * Sets the list laundry type register.
	 * 
	 * @param listLaundryTypeRegister
	 *            the new list laundry type register
	 */
	public void setListLaundryTypeRegister(
			List<AssertLaundryType> listLaundryTypeRegister) {
		this.listLaundryTypeRegister = listLaundryTypeRegister;
	}

	/**
	 * Gets the file type list.
	 * 
	 * @return the file type list
	 */
	public List<AssetLaundryFileType> getFileTypeList() {
		return fileTypeList;
	}

	/**
	 * Sets the file type list.
	 * 
	 * @param fileTypeList
	 *            the new file type list
	 */
	public void setFileTypeList(List<AssetLaundryFileType> fileTypeList) {
		this.fileTypeList = fileTypeList;
	}

	/**
	 * Gets the selected ofac person.
	 * 
	 * @return the selected ofac person
	 */
	public OfacPerson getSelectedOfacPerson() {
		return selectedOfacPerson;
	}

	/**
	 * Sets the selected ofac person.
	 * 
	 * @param selectedOfacPerson
	 *            the new selected ofac person
	 */
	public void setSelectedOfacPerson(OfacPerson selectedOfacPerson) {
		this.selectedOfacPerson = selectedOfacPerson;
	}

	/**
	 * Gets the ofac person list.
	 * 
	 * @return the ofac person list
	 */
	public List<OfacPerson> getOfacPersonList() {
		return ofacPersonList;
	}

	/**
	 * Sets the ofac person list.
	 * 
	 * @param ofacPersonList
	 *            the new ofac person list
	 */
	public void setOfacPersonList(List<OfacPerson> ofacPersonList) {
		this.ofacPersonList = ofacPersonList;
	}

	/**
	 * Gets the ofac data model.
	 * 
	 * @return the ofac data model
	 */
	public OfacPersonDataModel getOfacDataModel() {
		return ofacDataModel;
	}

	/**
	 * Sets the ofac data model.
	 * 
	 * @param ofacDataModel
	 *            the new ofac data model
	 */
	public void setOfacDataModel(OfacPersonDataModel ofacDataModel) {
		this.ofacDataModel = ofacDataModel;
	}

	/**
	 * Gets the ofac person to.
	 * 
	 * @return the ofac person to
	 */
	public OfacPersonTO getOfacPersonTO() {
		return ofacPersonTO;
	}

	/**
	 * Sets the ofac person to.
	 * 
	 * @param ofacPersonTO
	 *            the new ofac person to
	 */
	public void setOfacPersonTO(OfacPersonTO ofacPersonTO) {
		this.ofacPersonTO = ofacPersonTO;
	}

	/**
	 * Gets the pep person to.
	 * 
	 * @return the pep person to
	 */
	public PepPersonTO getPepPersonTO() {
		return pepPersonTO;
	}

	/**
	 * Sets the pep person to.
	 * 
	 * @param pepPeronTO
	 *            the new pep person to
	 */
	public void setPepPersonTO(PepPersonTO pepPeronTO) {
		this.pepPersonTO = pepPeronTO;
	}

	/**
	 * Gets the pna person to.
	 * 
	 * @return the pna person to
	 */
	public PnaPersonTO getPnaPersonTO() {
		return pnaPersonTO;
	}

	/**
	 * Sets the pna person to.
	 * 
	 * @param pnaPersonTO
	 *            the new pna person to
	 */
	public void setPnaPersonTO(PnaPersonTO pnaPersonTO) {
		this.pnaPersonTO = pnaPersonTO;
	}

	/**
	 * Gets the render ofactable.
	 * 
	 * @return the render ofactable
	 */
	public Boolean getRenderOfactable() {
		return renderOfactable;
	}

	/**
	 * Sets the render ofactable.
	 * 
	 * @param renderOfactable
	 *            the new render ofactable
	 */
	public void setRenderOfactable(Boolean renderOfactable) {
		this.renderOfactable = renderOfactable;
	}

	/**
	 * Gets the ofac person render.
	 * 
	 * @return the ofac person render
	 */
	public Boolean getOfacPersonRender() {
		return ofacPersonRender;
	}

	/**
	 * Sets the ofac person render.
	 * 
	 * @param ofacPersonRender
	 *            the new ofac person render
	 */
	public void setOfacPersonRender(Boolean ofacPersonRender) {
		this.ofacPersonRender = ofacPersonRender;
	}

	/**
	 * Gets the render.
	 * 
	 * @return the render
	 */
	public Boolean getRender() {
		return render;
	}

	/**
	 * Sets the render.
	 * 
	 * @param render
	 *            the new render
	 */
	public void setRender(Boolean render) {
		this.render = render;
	}

	/**
	 * Gets the render pep table.
	 * 
	 * @return the render pep table
	 */
	public Boolean getRenderPepTable() {
		return renderPepTable;
	}

	/**
	 * Sets the render pep table.
	 * 
	 * @param renderPepTable
	 *            the new render pep table
	 */
	public void setRenderPepTable(Boolean renderPepTable) {
		this.renderPepTable = renderPepTable;
	}

	/**
	 * Gets the ofac result render.
	 * 
	 * @return the ofac result render
	 */
	public Boolean getOfacResultRender() {
		return ofacResultRender;
	}

	/**
	 * Sets the ofac result render.
	 * 
	 * @param ofacResultRender
	 *            the new ofac result render
	 */
	public void setOfacResultRender(Boolean ofacResultRender) {
		this.ofacResultRender = ofacResultRender;
	}

	/**
	 * Gets the list file type.
	 * 
	 * @return the list file type
	 */
	public List<ParameterTable> getListFileType() {
		return listFileType;
	}

	/**
	 * Sets the list file type.
	 * 
	 * @param listFileType
	 *            the new list file type
	 */
	public void setListFileType(List<ParameterTable> listFileType) {
		this.listFileType = listFileType;
	}

	/**
	 * Sets the file type.
	 * 
	 * @param fileType
	 *            the new file type
	 */
	public void setFileType(Integer fileType) {
		this.fileType = fileType;
	}

	/**
	 * Gets the file type.
	 * 
	 * @return the file type
	 */
	public Integer getFileType() {
		return fileType;
	}

	/**
	 * Gets the file.
	 * 
	 * @return the file
	 */
	public UploadedFile getFile() {
		return file;
	}

	/**
	 * Sets the file.
	 * 
	 * @param file
	 *            the new file
	 */
	public void setFile(UploadedFile file) {
		this.file = file;
	}

	/**
	 * Gets the pep person render.
	 * 
	 * @return the pep person render
	 */
	public Boolean getPepPersonRender() {
		return pepPersonRender;
	}

	/**
	 * Sets the pep person render.
	 * 
	 * @param pepPersonRender
	 *            the new pep person render
	 */
	public void setPepPersonRender(Boolean pepPersonRender) {
		this.pepPersonRender = pepPersonRender;
	}

	/**
	 * Gets the pna person render.
	 * 
	 * @return the pna person render
	 */
	public Boolean getPnaPersonRender() {
		return pnaPersonRender;
	}

	/**
	 * Sets the pna person render.
	 * 
	 * @param pnaPersonRender
	 *            the new pna person render
	 */
	public void setPnaPersonRender(Boolean pnaPersonRender) {
		this.pnaPersonRender = pnaPersonRender;
	}




	/**
	 * Gets the list country.
	 * 
	 * @return the list country
	 */
	public List<GeographicLocation> getListCountry() {
		return listCountry;
	}

	/**
	 * Sets the list country.
	 * 
	 * @param listCountry
	 *            the new list country
	 */
	public void setListCountry(List<GeographicLocation> listCountry) {
		this.listCountry = listCountry;
	}

	/**
	 * Gets the list source info.
	 * 
	 * @return the list source info
	 */
	public List<ParameterTable> getListSourceInfo() {
		return listSourceInfo;
	}

	/**
	 * Sets the list source info.
	 * 
	 * @param listSourceInfo
	 *            the new list source info
	 */
	public void setListSourceInfo(List<ParameterTable> listSourceInfo) {
		this.listSourceInfo = listSourceInfo;
	}

	/**
	 * Gets the list pna motive.
	 * 
	 * @return the list pna motive
	 */
	public List<ParameterTable> getListPnaMotive() {
		return listPnaMotive;
	}

	/**
	 * Sets the list pna motive.
	 * 
	 * @param listPnaMotive
	 *            the new list pna motive
	 */
	public void setListPnaMotive(List<ParameterTable> listPnaMotive) {
		this.listPnaMotive = listPnaMotive;
	}

	/**
	 * Gets the list pep motive.
	 * 
	 * @return the list pep motive
	 */
	public List<PepMotiveType> getListPepMotive() {
		listPepMotive = PepMotiveType.list;
		return listPepMotive;
	}

	/**
	 * Sets the list pep motive.
	 * 
	 * @param listPepMotive
	 *            the new list pep motive
	 */
	public void setListPepMotive(List<PepMotiveType> listPepMotive) {
		this.listPepMotive = listPepMotive;
	}

	/**
	 * Gets the list pep charge.
	 * 
	 * @return the list pep charge
	 */
	public List<PepChargeType> getListPepCharge() {
		listPepCharge = PepChargeType.list;
		return listPepCharge;
	}

	/**
	 * Sets the list pep charge.
	 * 
	 * @param listPepCharge
	 *            the new list pep charge
	 */
	public void setListPepCharge(List<PepChargeType> listPepCharge) {
		this.listPepCharge = listPepCharge;
	}

	/**
	 * Gets the pep person.
	 * 
	 * @return the pep person
	 */
	public PepPerson getPepPerson() {
		return pepPerson;
	}

	/**
	 * Sets the pep person.
	 * 
	 * @param pepPerson
	 *            the new pep person
	 */
	public void setPepPerson(PepPerson pepPerson) {
		this.pepPerson = pepPerson;
	}

	/**
	 * Gets the current date.
	 * 
	 * @return the current date
	 */
	public Date getCurrentDate() {
		return new Date();
	}

	/**
	 * Gets the person type.
	 * 
	 * @return the person type
	 */
	public Integer getPersonType() {
		return personType;
	}

	/**
	 * Sets the person type.
	 * 
	 * @param personType
	 *            the new person type
	 */
	public void setPersonType(Integer personType) {
		this.personType = personType;
	}

	/**
	 * Gets the pna person.
	 * 
	 * @return the pna person
	 */
	public PnaPerson getPnaPerson() {
		return pnaPerson;
	}

	/**
	 * Sets the pna person.
	 * 
	 * @param pnaPerson
	 *            the new pna person
	 */
	public void setPnaPerson(PnaPerson pnaPerson) {
		this.pnaPerson = pnaPerson;
	}

	/**
	 * Gets the list laundry type.
	 * 
	 * @return the list laundry type
	 */
	public List<AssertLaundryType> getListLaundryType() {
		return listLaundryType;
	}

	/**
	 * Sets the list laundry type.
	 * 
	 * @param listLaundryType
	 *            the new list laundry type
	 */
	public void setListLaundryType(List<AssertLaundryType> listLaundryType) {
		this.listLaundryType = listLaundryType;
	}

	/**
	 * Gets the selected pna person.
	 * 
	 * @return the selected pna person
	 */
	public PnaPerson getSelectedPnaPerson() {
		return selectedPnaPerson;
	}

	/**
	 * Sets the selected pna person.
	 * 
	 * @param selectedPnaPerson
	 *            the new selected pna person
	 */
	public void setSelectedPnaPerson(PnaPerson selectedPnaPerson) {

		this.selectedPnaPerson = selectedPnaPerson;
	}

	/**
	 * Gets the selected pep person.
	 * 
	 * @return the selected pep person
	 */
	public PepPerson getSelectedPepPerson() {
		return selectedPepPerson;
	}

	/**
	 * Sets the selected pep person.
	 * 
	 * @param selectedPepPerson
	 *            the new selected pep person
	 */
	public void setSelectedPepPerson(PepPerson selectedPepPerson) {
		this.selectedPepPerson = selectedPepPerson;
	}

	/**
	 * Gets the pep data model.
	 * 
	 * @return the pep data model
	 */
	public PepPersonDataModel getPepDataModel() {
		return pepDataModel;
	}

	/**
	 * Gets the pna person data model.
	 * 
	 * @return the pna person data model
	 */
	public PnaPersonDatamodel getPnaPersonDataModel() {
		return pnaDataModel;
	}

	/**
	 * Gets the person type selected.
	 * 
	 * @return the person type selected
	 */
	public Integer getPersonTypeSelected() {
		return personTypeSelected;
	}

	/**
	 * Sets the person type selected.
	 * 
	 * @param personTypeSelected
	 *            the new person type selected
	 */
	public void setPersonTypeSelected(Integer personTypeSelected) {
		this.personTypeSelected = personTypeSelected;
	}

	/**
	 * Gets the data table result.
	 * 
	 * @return the data table result
	 */
	@SuppressWarnings("rawtypes")
	public List<Map> getDataTableResult() {
		return dataTableResult;
	}

	/**
	 * Gets the register pep person render.
	 * 
	 * @return the register pep person render
	 */
	public Boolean getRegisterPepPersonRender() {
		return registerPepPersonRender;
	}

	/**
	 * Sets the register pep person render.
	 * 
	 * @param registerPepPersonRender
	 *            the new register pep person render
	 */
	public void setRegisterPepPersonRender(Boolean registerPepPersonRender) {
		this.registerPepPersonRender = registerPepPersonRender;
	}

	/**
	 * Gets the register pna person render.
	 * 
	 * @return the register pna person render
	 */
	public Boolean getRegisterPnaPersonRender() {
		return registerPnaPersonRender;
	}

	/**
	 * Sets the register pna person render.
	 * 
	 * @param registerPnaPersonRender
	 *            the new register pna person render
	 */
	public void setRegisterPnaPersonRender(Boolean registerPnaPersonRender) {
		this.registerPnaPersonRender = registerPnaPersonRender;
	}


	/**
	 * Gets the person type description.
	 *
	 * @return the person type description
	 */
	public String getPersonTypeDescription() {
		return personTypeDescription;
	}

	/**
	 * Sets the person type description.
	 *
	 * @param personTypeDescription the new person type description
	 */
	public void setPersonTypeDescription(String personTypeDescription) {
		this.personTypeDescription = personTypeDescription;
	}

	/**
	 * Gets the list pep charge type.
	 *
	 * @return the list pep charge type
	 */
	public List<ParameterTable> getListPepChargeType() {
		return listPepChargeType;
	}

	/**
	 * Sets the list pep charge type.
	 *
	 * @param listPepChargeType the new list pep charge type
	 */
	public void setListPepChargeType(List<ParameterTable> listPepChargeType) {
		this.listPepChargeType = listPepChargeType;
	}

	/**
	 * Gets the list pep motive type.
	 *
	 * @return the list pep motive type
	 */
	public List<ParameterTable> getListPepMotiveType() {
		return listPepMotiveType;
	}

	/**
	 * Sets the list pep motive type.
	 *
	 * @param listPepMotiveType the new list pep motive type
	 */
	public void setListPepMotiveType(List<ParameterTable> listPepMotiveType) {
		this.listPepMotiveType = listPepMotiveType;
	}	

	/**
	 * Gets the list ofac person type.
	 *
	 * @return the list ofac person type
	 */
	public List<PersonType> getListOfacPersonType() {
		return listOfacPersonType;
	}

	/**
	 * Sets the list ofac person type.
	 *
	 * @param listOfacPersonType the new list ofac person type
	 */
	public void setListOfacPersonType(List<PersonType> listOfacPersonType) {
		this.listOfacPersonType = listOfacPersonType;
	}
	
	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * Gets the lst document type.
	 *
	 * @return the lst document type
	 */
	public List<ParameterTable> getLstDocumentType() {
		return lstDocumentType;
	}

	/**
	 * Sets the lst document type.
	 *
	 * @param lstDocumentType the new lst document type
	 */
	public void setLstDocumentType(List<ParameterTable> lstDocumentType) {
		this.lstDocumentType = lstDocumentType;
	}

	/**
	 * Gets the lst pep classification.
	 *
	 * @return the lst pep classification
	 */
	public List<ParameterTable> getLstPepClassification() {
		return lstPepClassification;
	}

	/**
	 * Sets the lst pep classification.
	 *
	 * @param lstPepClassification the new lst pep classification
	 */
	public void setLstPepClassification(List<ParameterTable> lstPepClassification) {
		this.lstPepClassification = lstPepClassification;
	}

	/**
	 * Gets the user privilege.
	 *
	 * @return the user privilege
	 */
	public UserPrivilege getUserPrivilege() {
		return userPrivilege;
	}

	/**
	 * Sets the user privilege.
	 *
	 * @param userPrivilege the new user privilege
	 */
	public void setUserPrivilege(UserPrivilege userPrivilege) {
		this.userPrivilege = userPrivilege;
	}
	
	

}
