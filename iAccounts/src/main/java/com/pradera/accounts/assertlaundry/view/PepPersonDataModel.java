package com.pradera.accounts.assertlaundry.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.accounts.PepPerson;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PepPersonDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/08/2015
 */
public class PepPersonDataModel extends ListDataModel<PepPerson> implements SelectableDataModel<PepPerson>, Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new pep person data model.
	 */
	public PepPersonDataModel(){

	}
	
	/**
	 * Instantiates a new pep person data model.
	 *
	 * @param data the data
	 */
	public PepPersonDataModel(List<PepPerson> data){
		super(data);

	}



	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public PepPerson getRowData(String rowKey) {
		
		List<PepPerson> person = (List<PepPerson>) getWrappedData();
		
		for(PepPerson p: person){
			if(p.getIdPepPersonPk()==Long.parseLong(rowKey))
				return p;
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(PepPerson pepPerson) {
		// TODO Auto-generated method stub
		return pepPerson.getIdPepPersonPk();
	}

}
