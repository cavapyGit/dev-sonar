package com.pradera.accounts.assertlaundry.view;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BlockEntityTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/08/2015
 */
public class BlockEntityTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	
	/** The person type. */
	private Integer personType;

	/**
	 * Gets the person type.
	 *
	 * @return the person type
	 */
	public Integer getPersonType() {
		return personType;
	}

	/**
	 * Sets the person type.
	 *
	 * @param personType the new person type
	 */
	public void setPersonType(Integer personType) {
		this.personType = personType;
	}

}
