package com.pradera.accounts.assertlaundry.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.accounts.OfacPerson;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class OfacPersonDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/08/2015
 */
public class OfacPersonDataModel extends ListDataModel<OfacPerson> implements SelectableDataModel<OfacPerson>, Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new ofac person data model.
	 */
	public OfacPersonDataModel(){

	}
	
	/**
	 * Instantiates a new ofac person data model.
	 *
	 * @param data the data
	 */
	public OfacPersonDataModel(List<OfacPerson> data){
		super(data);

	}



	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public OfacPerson getRowData(String rowKey) {
		
		List<OfacPerson> person = (List<OfacPerson>) getWrappedData();
		
		for(OfacPerson p: person){
			if(p.getIdOfacPersonPk()==Long.parseLong(rowKey))
				return p;
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(OfacPerson ofacPerson) {
		// TODO Auto-generated method stub
		return ofacPerson.getIdOfacPersonPk();
	}

}
