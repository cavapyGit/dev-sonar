package com.pradera.accounts.securitiesdirectsales.view;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import com.pradera.accounts.securitiesdirectsales.facade.SecuritiesDirectSaleServiceFacade;
import com.pradera.accounts.securitiesdirectsales.to.SecuritiesDirectSaleFilter;
import com.pradera.accounts.securitiesdirectsales.to.SecuritiesDirectSaleTO;
import com.pradera.accounts.util.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Security;

/**
 * 
 * EDV
 * @author equinajo.
 * 23/02/2021
 */
@DepositaryWebBean
@LoggerCreateBean
public class SecuritiesDirectSaleBean extends GenericBaseBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	/** The user privilege*/
	@Inject
	private UserPrivilege userPrivilege;
	/** The user info. */
	@Inject
	private UserInfo userInfo;

    @EJB
	private SecuritiesDirectSaleServiceFacade securitiesDirectSaleServiceFacade;
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	//Definiendo variables
	private String documentNumber;
	private Date initialDate;
	private Date finalDate;
	private Integer state;
	private Date currentDate;
	private boolean privilegeSearch;
	private boolean privilegeGenerate;
	private byte[] fileMassive_temp;
	private String fileMassive_name;
	private UploadedFile archivoExcel;
	private Security	securitySearch;
	private List<ParameterTable> listState;
	private transient StreamedContent fileMassive;
	private SecuritiesDirectSaleFilter secDirectSaleFilter;
	private List<SecuritiesDirectSaleTO> securitiesDirectSalesSearchList;
	
	/**
	 * Post construc.
	 */
	@PostConstruct
	public void init(){
		loadSearchCriteria();
		securityEvents();
		enabledButtons();
		
    }

    /**
     * Busca los valores de venta directa a partir de los criterios de busqueda 
     */
	 public void searchSecuritiesDirectSales(){
		try {
			secDirectSaleFilter = new SecuritiesDirectSaleFilter();
			secDirectSaleFilter.setDocumentNumber(documentNumber);
			secDirectSaleFilter.setInitialDate(initialDate);
			secDirectSaleFilter.setFinalDate(finalDate);
			secDirectSaleFilter.setState(state);
			secDirectSaleFilter.setSecurity(securitySearch);
			List<String> lstDocumentNumber = new ArrayList<>();
			if (Validations.validateIsNotNullAndNotEmpty(secDirectSaleFilter.getDocumentNumber()))
				lstDocumentNumber.add(secDirectSaleFilter.getDocumentNumber());
			if (Validations.validateIsNotNullAndNotEmpty(fileMassive_temp)) {
				Map<String,List<String>> mapResult = securitiesDirectSaleServiceFacade.getLstDocumentNumberFileMassive(archivoExcel);
				if(Validations.validateIsNotNullAndNotEmpty(mapResult.get("OK")))
					lstDocumentNumber = mapResult.get("OK");
				else{
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
							, mapResult.get("ERROR").get(0));
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
			}
			secDirectSaleFilter.setLstDocumentNumber(lstDocumentNumber);
			if (Validations.validateListIsNullOrEmpty(secDirectSaleFilter.getLstDocumentNumber())) {
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.LBL_ALERT_EXCHANGE_DOCUMENT_NUMBER_REQUIRED, null));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			securitiesDirectSalesSearchList = new ArrayList<SecuritiesDirectSaleTO>();
			securitiesDirectSalesSearchList = securitiesDirectSaleServiceFacade.getSecuritiesDirectSaleSearchList(secDirectSaleFilter);
		} catch (ServiceException e) {
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
	
	/**
	 *  Inicializa los criterios de busqueda 
	 */
    public void clearSearchCriteria(){
    	documentNumber = null;
		initialDate = new Date();
		finalDate = new Date();
		state = 0;
		currentDate = new Date();
		fileMassive_temp=null;
		fileMassive_name=null;
		fileMassive=null;
		archivoExcel = null;
		securitySearch = new Security();
		securitiesDirectSalesSearchList=null;
		secDirectSaleFilter = new SecuritiesDirectSaleFilter();
    }
    
	/** Carga los criterios de busqueda */
	public void loadSearchCriteria(){
		try {
			clearSearchCriteria();
			getListStateSearch();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void getListStateSearch(){
		try {
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.SECURITY_STATE.getCode());
		/*List<Integer> listPkState = new ArrayList<Integer>();
		listPkState.add(SecurityStateType.SUSPENDED.getCode());
		listPkState.add(SecurityStateType.EXCLUDED.getCode());
		listPkState.add(SecurityStateType.BLOCKED.getCode());
		listPkState.add(SecurityStateType.REDEEMED.getCode());
		listPkState.add(SecurityStateType.UNIFIED.getCode());
		listPkState.add(SecurityStateType.FUSED.getCode());
		listPkState.add(SecurityStateType.ESCISIONADO.getCode());
		parameterTableTO.setLstParameterTablePkNotIn(listPkState);*/
		listState = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Carga archivo 
	 * */
	public void fileUploadHandler (FileUploadEvent event) {
		String fDisplayName = fUploadValidateFile(event.getFile(),null,getfUploadFileLargeDocumentsSize(),getfUploadFileDocumentsTypes());		
		if(fDisplayName != null){	
			try {				
				InputStream inputFile = new ByteArrayInputStream(event.getFile().getContents());
				fileMassive = new DefaultStreamedContent(inputFile, event.getFile().getContentType(), event.getFile().getFileName());
				fileMassive_name = event.getFile().getFileName();
				fileMassive_temp = event.getFile().getContents();
				archivoExcel = event.getFile();
			} catch (Exception e) {
				e.printStackTrace();
			}
			documentNumber = null;
		}		
	}
	
	/**
	 * ELIMINA ARCHIVO TEMPORAL 
	 * */
	public void deleteFileTemp () {
		fileMassive = null;
		fileMassive_name = null;
		fileMassive_temp = null;
		archivoExcel = null;
	}
	
	/**
	 * Gets the generate file holder modify report.
	 *
	 * @param holderRequest the holder request
	 * @param nameParam the name param
	 * @return the generate file holder modify report
	 * @throws ServiceException the service exception
	 */
	public void getGenerateReport() throws ServiceException{	
		try {
			if (Validations.validateListIsNullOrEmpty(secDirectSaleFilter.getLstDocumentNumber())) {
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getGenericMessage(PropertiesConstants.ALERT_EXCEL_FILE_WITH_ERRORS, null));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			LoggerUser loggerUser = new LoggerUser();
			loggerUser.setUserName(userInfo.getUserAccountSession().getUserName());
			loggerUser.setUserAction(userInfo.getUserAcctions());
			loggerUser.setAuditTime(new Date());
			loggerUser.setIpAddress(userInfo.getUserAccountSession().getIpAddress());
			loggerUser.setIdUserSessionPk(userInfo.getUserAccountSession().getIdUserSessionPk());
			
			securitiesDirectSaleServiceFacade.generateReportFile(secDirectSaleFilter,loggerUser);
			Thread.sleep(5000);
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
					, PropertiesUtilities.getGenericMessage(GeneralConstants.REPORT_GENERATED_SUCCESSFULLY, null));
			JSFUtilities.showSimpleValidationDialog();
		} catch (InterruptedException ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	} 
	
	/** Security events. */
    public void securityEvents(){
		if (userPrivilege.getUserAcctions().isSearch()) 
			privilegeSearch = true;
		if (userPrivilege.getUserAcctions().isGenerate()) 
			privilegeGenerate = true;
    }
	    
    /** Enabled buttons */
    public void enabledButtons(){
    	PrivilegeComponent privilegeComponent = new PrivilegeComponent();
    	if(userPrivilege.getUserAcctions().isSearch())
			privilegeComponent.setBtnSearchView(true);
    	if(userPrivilege.getUserAcctions().isGenerate())
			privilegeComponent.setBtnGenerateView(true);
    }
    
    /** Disabled buttons */
    public void disabledButtons(){
    	PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnSearchView(false);
		privilegeComponent.setBtnGenerateView(false);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
    }
    
}
