package com.pradera.accounts.securitiesdirectsales.facade;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.primefaces.model.UploadedFile;

import com.pradera.accounts.holder.facade.HolderServiceFacade;
import com.pradera.accounts.securitiesdirectsales.service.SecuritiesDirectSaleServiceBean;
import com.pradera.accounts.securitiesdirectsales.to.SecuritiesDirectSaleFilter;
import com.pradera.accounts.securitiesdirectsales.to.SecuritiesDirectSaleTO;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.services.remote.reports.ReportGenerationService;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.report.type.ReportFormatType;

/**
 * EDV
 * @author equinajo.
 * @version 1.0 , 23/02/2021
 */

@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class SecuritiesDirectSaleServiceFacade{
	
	Logger log = Logger.getLogger(HolderServiceFacade.class);
	/** The crud dato service bean. */
	@EJB
	CrudDaoServiceBean crudDatoServiceBean;
	
	/** Holds current logger user details. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;
	
	/** Service bean authorized signature*/
	@EJB
	SecuritiesDirectSaleServiceBean authorizedSignatureServiceBean;
	
	/** The report service. */
 	@Inject
	Instance<ReportGenerationService> reportService;
	
 	/** The securities direct sale report id. */
	private final Long SECURITIES_DIRECT_SALE_REPORT_ID = new Long(338);
			
	/**
	 * Obtiene el listado de valores de venta directa
	 * @param holderFilter
	 * @return authorizedSignatureTOList
	 * @throws ServiceException
	 */
	public List<SecuritiesDirectSaleTO> getSecuritiesDirectSaleSearchList(SecuritiesDirectSaleFilter secDirectSaleFilter)
			throws ServiceException {
		List<SecuritiesDirectSaleTO> authorizedSignatureTOList = authorizedSignatureServiceBean.getSecuritiesDirectSaleSearchList(secDirectSaleFilter);
		return authorizedSignatureTOList;
	}
	
	/**
	 * Obtiene el listado de los numeros de documento a buscar
	 * @param fileMassive
	 * @return lstDocumentNumber
	 * @throws ServiceException
	 * @throws IOException 
	 * @throws InvalidFormatException 
	 */
	public Map<String,List<String>> getLstDocumentNumberFileMassive(UploadedFile fileMassive)
			throws ServiceException, InvalidFormatException, IOException {
		Map<String,List<String>> lstDocumentNumber = authorizedSignatureServiceBean.getLstDocumentNumberFileMassive(fileMassive);
		return lstDocumentNumber;
	}
	
	/**
	 * Generate report file.
	 *
	 * @param idRequest the id request
	 * @param nameParam the name param
	 * @throws ServiceException the service exception
	 */
	public void generateReportFile(SecuritiesDirectSaleFilter secDirectSaleFilter,LoggerUser loggerUserB) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		log.info("loggerUser: " + loggerUser);
		if(loggerUser==null)
			loggerUser = loggerUserB;
		if(loggerUser.getUserAction() != null)
			log.info("loggerUser.getUserAction(): " + loggerUser.getUserAction());
		else
			log.info("loggerUser.getUserAction(): NULL");
		if(loggerUser.getUserAction().getIdPrivilegeExecute() != null)
			log.info("loggerUser.getUserAction().getIdPrivilegeExecute(): " + loggerUser.getUserAction().getIdPrivilegeExecute());
		else 
			log.info("loggerUser.getUserAction().getIdPrivilegeExecute(): NULL");
	    
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		//loggerUser.getUserAction().setIdPrivilegeExecute(3440);
		loggerUser.setIdPrivilegeOfSystem(1);
		
		ReportUser reportUser = new ReportUser();
		reportUser.setUserName(loggerUser.getUserName());
		reportUser.setShowNotification(BooleanType.YES.getCode());
		//Asignando parametros
		Map<String,String> parameterDetails = new HashMap<String, String>();
		Map<String,String> parameters = new HashMap<>();
		parameters.put("reportType", SECURITIES_DIRECT_SALE_REPORT_ID.toString());
		parameters.put("reportFormats", String.valueOf(ReportFormatType.PDF.getCode()));
		parameters.put("lstDocumentNumber", secDirectSaleFilter.getLstDocumentNumber().toString());
		parameters.put("stateSecurity", secDirectSaleFilter.getState()!=null?secDirectSaleFilter.getState().toString():null);
		parameters.put("initialDate", CommonsUtilities.convertDatetoString(secDirectSaleFilter.getInitialDate()));
		parameters.put("finalDate", CommonsUtilities.convertDatetoString(secDirectSaleFilter.getFinalDate()));
		parameters.put("idSecurityCodePk", secDirectSaleFilter.getSecurity().getIdSecurityCodePk());
		
		reportService.get().saveReportExecution(SECURITIES_DIRECT_SALE_REPORT_ID, parameters, parameterDetails, reportUser, loggerUser);
		
	}
	
}