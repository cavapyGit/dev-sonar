package com.pradera.accounts.securitiesdirectsales.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.model.issuancesecuritie.Security;

/**
 * EDV
 * @author equinajo.
 * @version 1.0 , 23/02/2021
 */
public class SecuritiesDirectSaleFilter implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;	
	
	private String documentNumber;
	private Date initialDate;
	private Date finalDate;
	private List<String> lstDocumentNumber;
	private Security security;
	private Integer state;
	public String getDocumentNumber() {
		return documentNumber;
	}
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	public Date getInitialDate() {
		return initialDate;
	}
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	public Date getFinalDate() {
		return finalDate;
	}
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}
	public List<String> getLstDocumentNumber() {
		return lstDocumentNumber;
	}
	public void setLstDocumentNumber(List<String> lstDocumentNumber) {
		this.lstDocumentNumber = lstDocumentNumber;
	}
	public Security getSecurity() {
		return security;
	}
	public void setSecurity(Security security) {
		this.security = security;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	
}
