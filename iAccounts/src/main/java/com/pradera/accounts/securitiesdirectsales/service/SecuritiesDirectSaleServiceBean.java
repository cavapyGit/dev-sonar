package com.pradera.accounts.securitiesdirectsales.service;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.primefaces.model.UploadedFile;

import com.pradera.accounts.securitiesdirectsales.to.SecuritiesDirectSaleFilter;
import com.pradera.accounts.securitiesdirectsales.to.SecuritiesDirectSaleTO;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;

/**
 * EDV
 * @author equinajo.
 * @version 1.0 , 26/02/2021
 */

@Stateless
public class SecuritiesDirectSaleServiceBean extends CrudDaoServiceBean{
	
	/**
	 * Obtiene el listado de valores de venta directa 	
	 * @param holderFilter
	 * @return
	 * @throws ServiceException
	 */
	public List<SecuritiesDirectSaleTO> getSecuritiesDirectSaleSearchList(SecuritiesDirectSaleFilter secDirectSaleFilter)throws ServiceException{
		
		//Creating query
		StringBuffer sbQuery = new StringBuffer("");
		sbQuery.append(" SELECT to_char(co.registryDate,'DD/MM/YYYY'), aao.security.idSecurityCodePk,hol.fullName,hol.idHolderPk");
		sbQuery.append(" ,aao.holderAccount.accountNumber,hol.documentNumber,aao.totalBalance,sec.initialNominalValue");
		sbQuery.append(" ,sec.stateSecurity");
		sbQuery.append(" ,(select description from ParameterTable where parameterTablePk = sec.stateSecurity)");
		sbQuery.append(" FROM AccountAnnotationOperation aao");
		sbQuery.append(" INNER JOIN aao.holderAccount.holderAccountDetails had");
		sbQuery.append(" INNER JOIN had.holder hol");
		sbQuery.append(" INNER JOIN aao.security sec");
		sbQuery.append(" INNER JOIN aao.custodyOperation co");
		sbQuery.append(" WHERE  1=1");
		sbQuery.append(" AND sec.securityClass = 415");
		sbQuery.append(" AND aao.state = 858");
		if(Validations.validateIsNotNull(secDirectSaleFilter.getSecurity())
				&& Validations.validateIsNotNull(secDirectSaleFilter.getSecurity().getIdSecurityCodePk()))
			sbQuery.append(" AND sec.idSecurityCodePk = :idSecurityCodePk");
		if(Validations.validateListIsNotNullAndNotEmpty(secDirectSaleFilter.getLstDocumentNumber()))
			sbQuery.append(" AND hol.documentNumber IN (:lstDocumentNumber)");
		if(Validations.validateIsNotNullAndPositive(secDirectSaleFilter.getState()))
			sbQuery.append(" AND sec.stateSecurity = :stateSecurity");
		if(Validations.validateIsNotNullAndNotEmpty(secDirectSaleFilter.getInitialDate()) && Validations.validateIsNotNull(secDirectSaleFilter.getFinalDate()))
			sbQuery.append(" AND TRUNC(co.registryDate) BETWEEN :initialDate AND :finalDate");
			//sbQuery.append(" AND TRUNC(aao.expeditionDate) BETWEEN :initialDate AND :finalDate");	
		sbQuery.append(" ORDER BY co.registryDate"); 
		// Setting parameter
		Query query = em.createQuery(sbQuery.toString());
		if(Validations.validateIsNotNull(secDirectSaleFilter.getSecurity())
				&& Validations.validateIsNotNull(secDirectSaleFilter.getSecurity().getIdSecurityCodePk()))
			query.setParameter("idSecurityCodePk", secDirectSaleFilter.getSecurity().getIdSecurityCodePk());
		if(Validations.validateListIsNotNullAndNotEmpty(secDirectSaleFilter.getLstDocumentNumber()))
			query.setParameter("lstDocumentNumber", secDirectSaleFilter.getLstDocumentNumber());
		if(Validations.validateIsNotNullAndPositive(secDirectSaleFilter.getState()))
			query.setParameter("stateSecurity", secDirectSaleFilter.getState());
		if(Validations.validateIsNotNullAndNotEmpty(secDirectSaleFilter.getInitialDate()) && Validations.validateIsNotNull(secDirectSaleFilter.getFinalDate())){
			query.setParameter("initialDate", secDirectSaleFilter.getInitialDate());
			query.setParameter("finalDate", secDirectSaleFilter.getFinalDate());
		}
		List<Object[]> listObjects = query.getResultList();
		List<SecuritiesDirectSaleTO> securitiesDirectSaleTOList = new ArrayList<SecuritiesDirectSaleTO>();
		SecuritiesDirectSaleTO seDirectSaleTO;
		for(Object[] temp: listObjects){
			seDirectSaleTO = new SecuritiesDirectSaleTO();
			seDirectSaleTO.setExpeditionDate(temp[0]!=null?CommonsUtilities.convertStringtoDate(temp[0].toString()):null);
			seDirectSaleTO.setIdSecurityCodeFk(temp[1]!=null?temp[1].toString():null);
			seDirectSaleTO.setFullName(temp[2]!=null?temp[2].toString():null);
			seDirectSaleTO.setIdHolderPk(temp[3]!=null?Long.parseLong(temp[3].toString()):null);
			seDirectSaleTO.setIdHolderAccountFk(temp[4]!=null?Long.parseLong(temp[4].toString()):null);
			seDirectSaleTO.setDocumentNumber(temp[5]!=null?temp[5].toString():null);
			seDirectSaleTO.setTotalBalance(temp[6]!=null?new BigDecimal(temp[6].toString()):null);
			seDirectSaleTO.setInitialNominalValue(temp[7]!=null?new BigDecimal(temp[7].toString()):null);
			seDirectSaleTO.setStateSecurity(temp[8]!=null?Integer.parseInt(temp[8].toString()):null);
			seDirectSaleTO.setStateSecurityString(temp[9]!=null?temp[9].toString():null);
			securitiesDirectSaleTOList.add(seDirectSaleTO);
		}
		return securitiesDirectSaleTOList;
	}
	
	/**
	 * Obtiene el listado de numeros de documento a buscar 	
	 * @param fileMassive
	 * @return
	 * @throws ServiceException
	 * @throws IOException 
	 * @throws InvalidFormatException 
	 */
	public Map<String,List<String>> getLstDocumentNumberFileMassive(UploadedFile fileMassive)throws ServiceException, InvalidFormatException, IOException{
		List<String> result = new ArrayList<String>();
		List<String> error = new ArrayList<String>();
		Map<String,List<String>> mapResult = new HashMap<String,List<String>>();
		try {
			InputStream fileMassiveIS= fileMassive.getInputstream();
			//XSSFWorkbook libro = new XSSFWorkbook(fileMassiveIS);
			Workbook libro = WorkbookFactory.create(fileMassiveIS);
			//XSSFSheet hoja;
			//XSSFRow fila;
			Row fila;
			//XSSFCell celda;
			Cell celda;
			//hoja = libro.getSheetAt(0);
			Sheet hoja = libro.getSheetAt(0);
			int primeraColumna = 1;
			int ultimaColumna = hoja.getLastRowNum();
			
			int colDocumentNumber = 0;
			
			for (int i = primeraColumna; i <= ultimaColumna; i++) {
				fila = hoja.getRow(i);
				if (fila != null) {					
					String documentNumber = null;
					celda = fila.getCell(colDocumentNumber);
					if (celda != null) {
						error = new ArrayList<String>();
						documentNumber = verificaTipoColumna(celda);
						//Verificando estructura del numero de documento
						/*if(documentNumber.contains(" ")){
							error.add("Numero de documento no valido '"+documentNumber+"', Contiene espacios en blanco.");
							mapResult.put("ERROR", error);
							return mapResult;
						}*/
						if(documentNumber.length()>16){
							error.add("Numero de documento no valido '"+documentNumber+"', Longitud no permitida.");
							mapResult.put("ERROR", error);
							return mapResult;
						}
						result.add(documentNumber);
					}					
				}
			}
			fileMassiveIS.close();				
		} catch (Exception e) {
			error.add("Archivo Excel no valido, Verifique.");
			mapResult.put("ERROR", error);
			e.printStackTrace();
			return mapResult;
		}		
		mapResult.put("OK", result);
		return mapResult;
	}
	
    public static String verificaTipoColumna(Cell celda) {
        String resultado = "";
        try {
            if (celda != null) {
                int cellType = celda.getCellType();
                switch (cellType) {
                    case XSSFCell.CELL_TYPE_BLANK:
                        break;
                    case XSSFCell.CELL_TYPE_BOOLEAN:
                        break;
                    case XSSFCell.CELL_TYPE_ERROR:
                        break;
                    case XSSFCell.CELL_TYPE_FORMULA:
                        resultado = String.valueOf(celda.getRichStringCellValue());
                        break;
                    case XSSFCell.CELL_TYPE_NUMERIC:
                        resultado = String.valueOf((int) celda.getNumericCellValue());
                        break;
                    case XSSFCell.CELL_TYPE_STRING:
                        resultado = celda.getStringCellValue();
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultado.trim();
    }
		
}
