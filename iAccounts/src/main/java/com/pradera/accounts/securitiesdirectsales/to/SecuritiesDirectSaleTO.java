package com.pradera.accounts.securitiesdirectsales.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.pradera.commons.utils.CommonsUtilities;

/**
 * EDV
 * @author equinajo.
 * @version 1.0 , 23/02/2021
 */

public class SecuritiesDirectSaleTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;	
	
	private Date expeditionDate;
	private String idSecurityCodeFk;
	private String fullName;
	private Long idHolderPk;
	private Long idHolderAccountFk;
	private String documentNumber;
	private BigDecimal totalBalance;
	private BigDecimal initialNominalValue;
	private Integer stateSecurity;
	private String stateSecurityString;
	
	/**
	 * Obtiene la fecha de expedicion
	 * @return
	 */
	public String getExpeditionDateString(){
		return CommonsUtilities.convertDatetoStringForTemplate(this.expeditionDate,CommonsUtilities.DATE_PATTERN);
    }

	public Date getExpeditionDate() {
		return expeditionDate;
	}

	public void setExpeditionDate(Date expeditionDate) {
		this.expeditionDate = expeditionDate;
	}

	public String getIdSecurityCodeFk() {
		return idSecurityCodeFk;
	}

	public void setIdSecurityCodeFk(String idSecurityCodeFk) {
		this.idSecurityCodeFk = idSecurityCodeFk;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Long getIdHolderPk() {
		return idHolderPk;
	}

	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}

	public Long getIdHolderAccountFk() {
		return idHolderAccountFk;
	}

	public void setIdHolderAccountFk(Long idHolderAccountFk) {
		this.idHolderAccountFk = idHolderAccountFk;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public BigDecimal getTotalBalance() {
		return totalBalance;
	}

	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	public BigDecimal getInitialNominalValue() {
		return initialNominalValue;
	}

	public void setInitialNominalValue(BigDecimal initialNominalValue) {
		this.initialNominalValue = initialNominalValue;
	}

	public Integer getStateSecurity() {
		return stateSecurity;
	}

	public void setStateSecurity(Integer stateSecurity) {
		this.stateSecurity = stateSecurity;
	}

	public String getStateSecurityString() {
		return stateSecurityString;
	}

	public void setStateSecurityString(String stateSecurityString) {
		this.stateSecurityString = stateSecurityString;
	}
	
	

}
