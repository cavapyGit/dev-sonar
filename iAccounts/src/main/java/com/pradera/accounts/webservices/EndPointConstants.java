package com.pradera.accounts.webservices;

public class EndPointConstants {
	public static final String GENERATE_REPORT_ACCOUNT_MODIFICATION = "/resources/generate/modify";

	public static final String CREATE_INSTITUTION_SECURITY = "/resources/institution/create";

	public static final String UPDATE_INSTITUTION_SECURITY = "/resources/institution/update";
}
