package com.pradera.accounts.holderaccount.facade;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.accounts.holderaccount.service.HolderAccountServiceBean;
import com.pradera.accounts.holderaccount.to.HolderAccountResultTO;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountRequest;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountMovement;
import com.pradera.model.process.BusinessProcess;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class HolderAccountServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/08/2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class HolderAccountServiceFacade {

	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The holder account service bean. */
	@EJB
	HolderAccountServiceBean holderAccountServiceBean;
	
	/** The notification service facade. */
	@EJB 
	NotificationServiceFacade notificationServiceFacade;
	
	/**
	 * Gets the bank list service facade.
	 *
	 * @return the bank list service facade
	 */
	public List<Bank> getBankListServiceFacade(){
		return holderAccountServiceBean.getBankListServiceBean();
	}
	
	/**
	 * Find holder accounts service facade.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> findHolderAccountsServiceFacade(HolderAccountTO holderAccountTO)throws ServiceException{
		return holderAccountServiceBean.findHolderAccountsServiceBean(holderAccountTO);
	}
	
	/**
	 * Gets the holder account request detail list service facade.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account request detail list service facade
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_QUERY)
	public List<HolderAccountRequest> getHolderAccountRequestList(HolderAccountTO holderAccountTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return holderAccountServiceBean.getHolderAccountRequestsList(holderAccountTO);
	}
	
	/**
	 * Register holder account request service facade.
	 *
	 * @param holderAccountRequest the holder account request
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_CREATE_REQUEST_REGISTER)
	public boolean registerHolderAccountRequest(HolderAccountRequest holderAccountRequest) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());		
		holderAccountServiceBean.registryHolderAccountRequest(holderAccountRequest);
		//Paratemers for notification
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_ACCOUNT_CREATE_REQUEST_REGISTER.getCode());
		Object[] parameters = new Object[]{holderAccountRequest.getIdHolderAccountReqPk().toString()};
		Long participantCode = holderAccountRequest.getParticipant().getIdParticipantPk();		
		//Send notification
		sendNotification(participantCode, businessProcess, parameters);		
		return true; 
	}
	
	/**
	 * Gets the holder account request service facade.
	 *
	 * @param idHolderAccountRequestPk the id holder account request
	 * @return the holder account request service facade
	 * @throws ServiceException the service exception
	 */
	public HolderAccountRequest getHolderAccountRequest(Long idHolderAccountRequestPk) throws ServiceException{
		return holderAccountServiceBean.getHolderAccountRequest(idHolderAccountRequestPk);
	}
	
	/**
	 * Approve holder account request.
	 *
	 * @param holderAccountRequestHy the holder account request
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	//Map this type due to compilation error
	@ProcessAuditLogger(process=BusinessProcessType.NEW_HOLDER_CREATION_REQUEST_APPROVE)
	public HolderAccountRequest approveHolderAccountRequest(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		HolderAccountRequest holderAccountRequest = holderAccountServiceBean.approveHolderAccountRequest(holderAccountRequestHy);		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());		
		//Paratemers for notification
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_ACCOUNT_CREATE_REQUEST_APPROVE.getCode());
		Object[] parameters = new Object[]{holderAccountRequestHy.getIdHolderAccountReqPk().toString()};
		Long participantCode = holderAccountRequest.getParticipant().getIdParticipantPk();		
		//Send notification
		sendNotification(participantCode, businessProcess, parameters);		
		return holderAccountRequest;
	}
	
	/**
	 * Annulate holder account request.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @return the holder account request hy
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_CREATE_REQUEST_CANCEL)
	public HolderAccountRequest annulateHolderAccountRequest(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		HolderAccountRequest holderAccountReq = holderAccountServiceBean.anulateHolderAccountRequest(holderAccountRequestHy);		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());		
		//Paratemers for notification
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_ACCOUNT_CREATE_REQUEST_CANCEL.getCode());
		Object[] parameters = new Object[]{holderAccountReq.getIdHolderAccountReqPk().toString()};
		Long participantCode = holderAccountReq.getParticipant().getIdParticipantPk(); 		
		//Send notification
		sendNotification(participantCode, businessProcess, parameters);
		
		return holderAccountReq ; 
	}
	
	/**
	 * Reject holder account request.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @return the holder account request hy
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_CREATE_REQUEST_REJECT)
	public HolderAccountRequest rejectHolderAccountRequest(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		HolderAccountRequest holderAccountReq = holderAccountServiceBean.rejectHolderAccountRequest(holderAccountRequestHy);		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());		
		//Paratemers for notification
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_ACCOUNT_CREATE_REQUEST_REJECT.getCode());
		Object[] parameters = new Object[]{holderAccountReq.getIdHolderAccountReqPk().toString()};
		Long participantCode = holderAccountReq.getParticipant().getIdParticipantPk(); 
		
		//Send notification
		sendNotification(participantCode, businessProcess, parameters);
		
		return holderAccountReq;
	}
	
	/**
	 * Confirm holder account request.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @return the holder account request hy
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_CREATE_REQUEST_CONFIRM)
	public HolderAccountRequest confirmHolderAccountRequest(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());		
		HolderAccountRequest holderAccountReq = holderAccountServiceBean.confirmHolderAccountRequest(holderAccountRequestHy);		
		//Paratemers for notification
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_ACCOUNT_CREATE_REQUEST_CONFIRM.getCode());
		Object[] parameters = new Object[]{holderAccountReq.getIdHolderAccountReqPk().toString(), holderAccountReq.getHolderAccount().getAccountNumber()};
		Long participantCode = holderAccountReq.getParticipant().getIdParticipantPk(); 
		
		//Send notification
		sendNotification(participantCode, businessProcess, parameters);
		
		return holderAccountReq;
	}
	
	/**
	 * Gets the holder account by native query facade.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account by native query facade
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountResultTO> getHolderAccountList(HolderAccountTO holderAccountTO) throws ServiceException{
		List<HolderAccountResultTO> holderAccountResultList = new ArrayList<HolderAccountResultTO>();
		Map<Long,HolderAccountResultTO> holderAccountResultMap = new HashMap<Long,HolderAccountResultTO>();		
		List<Object[]> objectListTmp = holderAccountServiceBean.getHolderAccountList(holderAccountTO);
		for(Object[] objectData: objectListTmp){
			Long idHolderAccountPk = new Long(objectData[0].toString());// ((BigDecimal) objectData[0]).longValue();
			String holderName = objectData[11].toString() + GeneralConstants.DASH +(String)objectData[12] ;			
			if(holderName==null || holderName.isEmpty()){
				holderName = objectData[11].toString()+" "+(String)objectData[5]+" "+(String)objectData[6]+" "+(String)objectData[7] ;
			}			
			if(holderAccountResultMap.get(idHolderAccountPk)!=null){
				HolderAccountResultTO holderAccountResultTO = holderAccountResultMap.get(idHolderAccountPk);
				holderAccountResultTO.getHolders().add(holderName);
			}else{
				try{
					HolderAccountResultTO holderAccountResultTO = new HolderAccountResultTO();
					holderAccountResultTO.setHolders(new ArrayList<String>());
					holderAccountResultTO.setIdHolderAccountPk(idHolderAccountPk);
					holderAccountResultTO.setAccountNumber( new Integer(objectData[1].toString()));
					holderAccountResultTO.setRegisterDate((Date) objectData[2]);
					holderAccountResultTO.setIdParticipantPk(new Long(objectData[3].toString()));
					holderAccountResultTO.setParticipant((String)objectData[4]);
					holderAccountResultTO.getHolders().add(holderName);
					holderAccountResultTO.setAccountGroup((String)objectData[16]);
					holderAccountResultTO.setAccountType((String)objectData[17]);
					holderAccountResultTO.setAccountState((String)objectData[18]);					
					holderAccountResultList.add(holderAccountResultTO);
					holderAccountResultMap.put(idHolderAccountPk, holderAccountResultTO);
				}catch(NullPointerException ex){
					continue;
				}
			}
		}
		return holderAccountResultList;
	}
	
	/**
	 * Gets the holder account service facade.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account service facade
	 * @throws ServiceException the service exception
	 */
	public HolderAccount getHolderAccount(HolderAccountTO holderAccountTO) throws ServiceException{
		return holderAccountServiceBean.getHolderAccount(holderAccountTO);
	}
	
	/**
	 * Gets the Exist Issuer Account Type service facade.
	 *
	 * @param documentType and documentNumber
	 * @param documentNumber the document number
	 * @return the Exist Issuer Account Type service facade
	 * @throws ServiceException the service exception
	 */
	public Integer getExistIssuerAccountTypeServiceFacade(Integer documentType, String documentNumber) throws ServiceException{
		 return holderAccountServiceBean.getExistIssuerAccountTypeServiceBean(documentType, documentNumber);
	 }
	
	/**
	 * Gets the holder account req file facade.
	 *
	 * @param idFilePk the id file pk
	 * @return the holder account req file facade
	 * @throws ServiceException the service exception
	 */
	public byte[] getHolderAccountReqFileFacade(Long idFilePk) throws ServiceException{
		return holderAccountServiceBean.getHolderAccountReqFile(idFilePk);
	}
	
	/**
	 * Gets the history state facade.
	 *
	 * @param accountNumber and id_participant
	 * @param id_participant the id_participant
	 * @return the history state facade
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountRequest> getHolderAccountRequestHistoryStateServiceFacade(Integer accountNumber,Long id_participant) throws ServiceException{
		return holderAccountServiceBean.getHolderAccountRequestHistoryStateServiceBean(accountNumber,id_participant);
	}
	
	/**
	 * Gets the holder account file facade.
	 *
	 * @param idFilePk the id file pk
	 * @return the holder account file facade
	 * @throws ServiceException the service exception
	 */
	public byte[] getHolderAccountFileFacade(Long idFilePk) throws ServiceException{
		return holderAccountServiceBean.getHolderAccountFile(idFilePk);
	}
	
	 /**
 	 * Send notification.
 	 *
 	 * @param partCode the part code
 	 * @param businessProcess the business process
 	 * @param parameters the parameters
 	 */
 	public void sendNotification(Long partCode , BusinessProcess businessProcess,Object[] parameters){
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		 notificationServiceFacade.sendNotification(loggerUser.getUserName(), businessProcess, partCode, parameters);
	 }
	
	/**
	 * Holder account can will closed service bean.
	 *
	 * @param holderAccount the holder account
	 * @return the boolean
	 * @throws ServiceException the service exception
	 */
	public Boolean holderAccountCanWillClosedServiceFacade(HolderAccount holderAccount) throws ServiceException{
		return holderAccountServiceBean.holderAccountCanWillClosedServiceBean(holderAccount);
	}
	
	/**
	 * Gets the holder account list service facade.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account list service facade
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> getHolderAccountListServiceFacade(HolderAccountTO holderAccountTO) throws ServiceException{
		return holderAccountServiceBean.getHolderAccountListServiceBean(holderAccountTO);
	}
	
	/**
	 * Have request with out confirmation service facade.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account request hy
	 * @throws ServiceException the service exception
	 */
	public HolderAccountRequest haveRequestWithOutConfirmationServiceFacade(HolderAccountTO holderAccountTO) throws ServiceException{
		return holderAccountServiceBean.haveRequestWithOutConfirmationServiceBean(holderAccountTO);
	}
	
	/*
	 *	Created by Martin Zarate Rafael 
	 */
	/**
	 * Gets the balance holder from security.
	 *
	 * @param filter the filter
	 * @return the balance holder from security
	 * @throws ServiceException the service exception
	 */
	//Business Validations for Balances
	public HolderAccountBalance getBalanceHolderFromSecurity(HolderAccountTO filter) throws ServiceException{
	    if(Validations.validateIsNotNullAndPositive(filter.getParticipantTO()) &&
		   Validations.validateIsNotNullAndNotEmpty(filter.getHolderAccountNumber()) &&
		   Validations.validateIsNotNullAndNotEmpty(filter.getIdIsinCodePk())){
		   return holderAccountServiceBean.getBalanceHolderFromSecurity(filter);
		}else{
			return null;
		}
	}
	
	/*
	 *	Created by Martin Zarate Rafael 
	 */
	/**
	 * Gets the holder account movements.
	 *
	 * @param filter the filter
	 * @return the holder account movements
	 * @throws ServiceException the service exception
	 */
	//Business Validations for Movements
	public List<HolderAccountMovement> getHolderAccountMovements(HolderAccountTO filter) throws ServiceException{
		 if(Validations.validateIsNotNullAndPositive(filter.getParticipantTO()) &&
			Validations.validateIsNotNullAndNotEmpty(filter.getHolderAccountNumber()) &&
			Validations.validateIsNotNullAndNotEmpty(filter.getIdIsinCodePk())){
			return holderAccountServiceBean.getHolderAccountMovements(filter);
		}else{
			return null;
		}
	}
	 
}