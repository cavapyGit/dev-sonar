package com.pradera.accounts.holderaccount.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountBank;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.HolderAccountFile;
import com.pradera.model.accounts.holderaccounts.HolderAccountBankRequest;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetRequest;
import com.pradera.model.accounts.holderaccounts.HolderAccountFileRequest;
import com.pradera.model.accounts.holderaccounts.HolderAccountRequest;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestHysStatusType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestHysType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStateBankType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStateDetailType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountMovement;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.OtcOperationStateType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class HolderAccountCloseServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/08/2015
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@Interceptors(ContextHolderInterceptor.class)
public class HolderAccountCloseServiceBean extends CrudDaoServiceBean{
	
	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
	/** The parameter service bean. */
	@EJB
	ParameterServiceBean parameterServiceBean;
	
	/**
	 * Gets the bank list service bean.
	 *
	 * @return the bank list service bean
	 */
	@SuppressWarnings("unchecked")
	public List<Bank> getBankListServiceBean(){
		Query query = em.createQuery("Select b From Bank b WHERE b.state = :stateBankParam");
		query.setParameter("stateBankParam", MasterTableType.PARAMETER_TABLE_BANK_STATE_REGISTERED.getCode());
		return (List<Bank>)query.getResultList();
	}
	
	/**
	 * Gets the holder account request detail list service bean.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account request detail list service bean
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	public List<HolderAccountRequest> getHolderAccountRequestCloseListServiceBean(HolderAccountTO holderAccountTO) throws ServiceException{		
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("	Select har.idHolderAccountReqPk, har.registryDate,	"); //0, 1
		stringBuilder.append("	har.holderAccountType, (Select p.parameterName From ParameterTable p Where p.parameterTablePk = har.holderAccountType),	"); // 2, 3
		stringBuilder.append("	har.requestState, (Select p.parameterName From ParameterTable p Where p.parameterTablePk = har.requestState),	"); // 4, 5
		stringBuilder.append("	har.participant.idParticipantPk, har.participant.mnemonic,	"); //6, 7
		stringBuilder.append("	ho.idHolderPk, ho.fullName,	");	//8, 9
		stringBuilder.append("	ho.documentType, (Select p.indicator1 From ParameterTable p Where p.parameterTablePk = ho.documentType), ");//10, 11
		stringBuilder.append("	ho.documentNumber, ");//12		
		stringBuilder.append("	har.requestType, ha.accountNumber, ");//13, 14	
		stringBuilder.append("	(Select p.parameterName From ParameterTable p Where p.parameterTablePk = har.requestType) ");//15
		stringBuilder.append("	from HolderAccountRequest har	");
		stringBuilder.append("	join har.holderAccount ha	");
		stringBuilder.append("	join ha.holderAccountDetails had	");
		stringBuilder.append("	join had.holder ho	");
		stringBuilder.append("	Where 1 = 1	");				
		if(holderAccountTO.getRequestNumber() !=null ){
			stringBuilder.append("	AND	har.requestNumber = :requestNumber	");
			parameters.put("requestNumber", holderAccountTO.getRequestNumber());			
		}
		if(holderAccountTO.getParticipantTO() != null){
			stringBuilder.append("	AND	har.participant.idParticipantPk = :idParticipantPk	");
			parameters.put("idParticipantPk", holderAccountTO.getParticipantTO());			
		}		
		if(holderAccountTO.getRelatedName() != null){
			stringBuilder.append("	AND	har.relatedName LIKE :relatedName	");
			parameters.put("relatedName", "%" +  holderAccountTO.getRelatedName() + "%");				
		}
		if(Validations.validateIsNotNullAndNotEmpty(holderAccountTO.getInitialDate()) && Validations.validateIsNotNullAndNotEmpty(holderAccountTO.getFinalDate())){
			// issue 1288: adicion de trunc para para ver las solicitudes 
			stringBuilder.append(" and trunc(har.registryDate) between :dateIni and :dateEnd ");
			parameters.put("dateIni", holderAccountTO.getInitialDate());
			parameters.put("dateEnd", holderAccountTO.getFinalDate());			
		}
		if(Validations.validateIsNotNull(holderAccountTO.getStatus()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(holderAccountTO.getStatus())))){
			stringBuilder.append("	AND	har.requestState = :requestState	");
			parameters.put("requestState", holderAccountTO.getStatus());							
		}
		if(Validations.validateIsNotNull(holderAccountTO.getHolderAccountRequestType()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(holderAccountTO.getHolderAccountRequestType())))){
			stringBuilder.append("	AND	har.requestType = :requestType	");
			parameters.put("requestType", holderAccountTO.getHolderAccountRequestType());			
		} 		
		List<Object[]> accountHolderRequestList =  findListByQueryString(stringBuilder.toString(), parameters);
		Map<Long,HolderAccountRequest> accountsHolderRequestPk = new HashMap<Long, HolderAccountRequest>();		
		for(Object[] object: accountHolderRequestList){
			if(accountsHolderRequestPk.get(object[0]) == null){
				HolderAccountRequest holderAccountRequest = new HolderAccountRequest();
				holderAccountRequest.setHolderAccountDetRequest(new ArrayList<HolderAccountDetRequest>());	
				holderAccountRequest.setIdHolderAccountReqPk(new Long(object[0].toString()));				
				holderAccountRequest.setRegistryDate((Date) object[1]);				
				holderAccountRequest.setHolderAccountType(new Integer(object[2].toString()));
				holderAccountRequest.setHolderAccountTypeDescription(object[3].toString());
				holderAccountRequest.setRequestState(new Integer(object[4].toString()));
				holderAccountRequest.setStateAccountName(object[5].toString());		
				holderAccountRequest.setRequestType(new Integer(object[13].toString()));
				holderAccountRequest.setRequestTypeName(object[15].toString());
				Participant participant = new Participant();
				participant.setIdParticipantPk(new Long(object[6].toString()));
				participant.setMnemonic(object[7].toString());
				holderAccountRequest.setParticipant(participant);	
				HolderAccount holderAccount = new HolderAccount();
				holderAccount.setAccountNumber(new Integer(object[14].toString()));
				holderAccountRequest.setHolderAccount(holderAccount);
				accountsHolderRequestPk.put(holderAccountRequest.getIdHolderAccountReqPk(), holderAccountRequest);
			}
			//add data of holder 
			HolderAccountRequest accountRequestMap = accountsHolderRequestPk.get(object[0]);
			HolderAccountDetRequest accountRequestDetail = new HolderAccountDetRequest();
			Holder holder = new Holder();						
			holder.setIdHolderPk(new Long(object[8].toString()));
			holder.setFullName(object[9].toString());
			holder.setDocumentType(new Integer(object[10].toString()));
			holder.setDescriptionTypeDocument(object[11].toString());
			holder.setDocumentNumber(object[12].toString());			
			accountRequestDetail.setHolder(holder);
			accountRequestMap.getHolderAccountDetRequest().add(accountRequestDetail);
		}
		return new ArrayList<HolderAccountRequest>(accountsHolderRequestPk.values());
	}
	
	/**
	 * Have request with out confirmation service bean.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public HolderAccountRequest haveRequestWithOutConfirmation(HolderAccountTO holderAccountTO) throws ServiceException{
		String query = "	SELECT holderAccountReq FROM HolderAccountRequest holderAccountReq " +
						"	WHERE holderAccountReq.holderAccount.accountNumber = :accountNumberParam AND " +
						"  	holderAccountReq.participant.idParticipantPk = :idParticipantParam AND" +
						"  	holderAccountReq.requestState IN (:stateRegisteredParam, :stateApproveParam)";
		Query queryJpa = em.createQuery(query);
		queryJpa.setParameter("stateRegisteredParam", HolderAccountRequestHysStatusType.REGISTERED.getCode());
		queryJpa.setParameter("stateApproveParam", HolderAccountRequestHysStatusType.APPROVE.getCode());
		queryJpa.setParameter("accountNumberParam", holderAccountTO.getHolderAccountNumber());
		queryJpa.setParameter("idParticipantParam", holderAccountTO.getParticipantTO());		
		List<HolderAccountRequest> holderAccountRequestHyList = null;
		try{
			holderAccountRequestHyList = ((List<HolderAccountRequest>)queryJpa.getResultList());
		}catch(NoResultException ex){
			return null;
		}
		if(holderAccountRequestHyList!=null && !holderAccountRequestHyList.isEmpty()){
			return holderAccountRequestHyList.get(0);
		}
		return null;
	}
	
	/**
	 * Gets the holder account service bean.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account service bean
	 * @throws ServiceException the service exception
	 */
	public HolderAccount getHolderAccount(HolderAccountTO holderAccountTO) throws ServiceException{
		return accountsFacade.getHolderAccountComponentServiceFacade(holderAccountTO);
	}
	
	/**
	 * Registry holder account request service bean.
	 *
	 * @param holderAccountRequest the holder account request
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean registryHolderAccountRequest(HolderAccountRequest holderAccountRequest) throws ServiceException{
		validateEntitiesOnRequest(holderAccountRequest);		
		if(!holderAccountRequest.getRequestType().equals(HolderAccountRequestHysType.CREATE.getCode())){
			List<Integer> stateValidate = Arrays.asList(HolderAccountRequestHysStatusType.REGISTERED.getCode(),HolderAccountRequestHysStatusType.APPROVE.getCode());
			HolderAccount holderAccount = holderAccountRequest.getHolderAccount();
			Map<String,Object> holderAccountPkParam = new HashMap<String,Object>();
			holderAccountPkParam.put("idAccountPk", holderAccount.getIdHolderAccountPk());
			holderAccountPkParam.put("statesParam", stateValidate);			
			Long quantityRequest = findObjectByNamedQuery(HolderAccountRequest.ACC_ACCOUNT_USED, holderAccountPkParam);			
			if(quantityRequest>0L){
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			}
		}		
		create(holderAccountRequest);
		Query query = em.createQuery(new StringBuilder("UPDATE HolderAccountRequest r SET r.requestNumber = r.idHolderAccountReqPk ").append(
									 				   " WHERE r.idHolderAccountReqPk = :idHolderAccountRequestParameter").toString());
		query.setParameter("idHolderAccountRequestParameter", holderAccountRequest.getIdHolderAccountReqPk());
		query.executeUpdate();
		return true;
	}
	
	/**
	 * Gets the holder account request service bean.
	 *
	 * @param idRequestPk the id request pk
	 * @return the holder account request service bean
	 * @throws ServiceException the service exception 
	 */
	public HolderAccountRequest getHolderAccountRequestClose(Long idRequestPk) throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.HOLDERACCOUNT_DOCUMENTS_BLOCK.getCode());
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		List<ParameterTable> listParameterTable = null;		
		listParameterTable = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);						
		String query = "Select distinct har From HolderAccountRequest har " +	
				"			Inner Join Fetch har.holderAccount ha " +
				"			Inner Join Fetch har.participant part " +
				"			Inner Join Fetch ha.holderAccountDetails hard " +
				"			Inner Join Fetch hard.holder ho " +								
				"			Where har.idHolderAccountReqPk = :idPk";		
		Query queryJpa = em.createQuery(query); 
		queryJpa.setParameter("idPk", idRequestPk);						
		HolderAccountRequest holderAccountRequestHy = (HolderAccountRequest) queryJpa.getSingleResult();			
		holderAccountRequestHy.getHolderAccountFilesRequest().size();							
		if(holderAccountRequestHy.getHolderAccount() != null){
			HolderAccountTO holderAccountTO = new HolderAccountTO();
			holderAccountTO.setNeedHolder(Boolean.TRUE);
			holderAccountTO.setNeedFiles(Boolean.TRUE);
			holderAccountTO.setNeedParticipant(Boolean.FALSE);
			holderAccountTO.setIdHolderAccountPk(holderAccountRequestHy.getHolderAccount().getIdHolderAccountPk());
			holderAccountRequestHy.setHolderAccount(getHolderAccount(holderAccountTO));
		}
		for(HolderAccountFileRequest file : holderAccountRequestHy.getHolderAccountFilesRequest()){
			for(ParameterTable parameter: listParameterTable){
				if(file.getDocumentType().equals(parameter.getParameterTablePk())){
					file.setDocumentTypeDesc(parameter.getDescription());
				}
			}
		}	
		return holderAccountRequestHy;
	}
	
	/**
	 * Gets the holder account request file doc block.
	 *
	 * @param idFilePk the id file pk
	 * @return the holder account request file doc block
	 * @throws ServiceException the service exception
	 */
	public byte[] getHolderAccountReqDocBlock(Long idFilePk) throws ServiceException{
		String query = "Select har.docBlock FROM HolderAccountRequest har WHERE har.idHolderAccountReqPk = :idFilePkParam";
		Query queryJpql = em.createQuery(query);
		queryJpql.setParameter("idFilePkParam", idFilePk);
		try{
			return (byte[]) queryJpql.getSingleResult();
		}catch(NoResultException ex){
			return null;
		}
	}
	
	/**
	 * Gets the holder account request file doc block acc.
	 *
	 * @param idFilePk the id file pk
	 * @return the holder account request file doc block acc
	 * @throws ServiceException the service exception
	 */
	public byte[] getHolderAccountReqDocBlockAct(Long idFilePk) throws ServiceException{
		String query = "Select har.docBlockAct FROM HolderAccountRequest har WHERE har.idHolderAccountReqPk = :idFilePkParam";
		Query queryJpql = em.createQuery(query);
		queryJpql.setParameter("idFilePkParam", idFilePk);
		try{
			return (byte[]) queryJpql.getSingleResult();
		}catch(NoResultException ex){
			return null;
		}
	}
	
	/**
	 * Approve Holder Account request .
	 *
	 * @param holderAccountRequest the holder account request
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	public HolderAccountRequest approveHolderAccountRequest(HolderAccountRequest holderAccountRequest) throws ServiceException{
		validateStatesRequestHys(holderAccountRequest.getIdHolderAccountReqPk(), Arrays.asList(HolderAccountRequestHysStatusType.REGISTERED.getCode()));	
		HolderAccountRequest holderAccountRequestHy = getHolderAccountRequestClose(holderAccountRequest.getIdHolderAccountReqPk());
		validateEntitiesOnRequest(holderAccountRequestHy);
		updateRequestByState(holderAccountRequest);		
		return holderAccountRequest;
	}
	
	/**
	 * Anulate Holder Account request .
	 *
	 * @param holderAccountRequest the holder account request
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	public HolderAccountRequest anulateHolderAccountRequest(HolderAccountRequest holderAccountRequest) throws ServiceException{
		validateStatesRequestHys(holderAccountRequest.getIdHolderAccountReqPk(), Arrays.asList(HolderAccountRequestHysStatusType.REGISTERED.getCode()));
		HolderAccountRequest holderAccountRequestHy = getHolderAccountRequestClose(holderAccountRequest.getIdHolderAccountReqPk());
		validateEntitiesOnRequest(holderAccountRequestHy);		
		updateRequestByState(holderAccountRequest);		
		return holderAccountRequest;
	}
	
	/**
	 * Reject Holder Account request .
	 *
	 * @param holderAccountRequest the holder account request
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	public HolderAccountRequest rejectHolderAccountRequest(HolderAccountRequest holderAccountRequest) throws ServiceException{
		validateStatesRequestHys(holderAccountRequest.getIdHolderAccountReqPk(), Arrays.asList(HolderAccountRequestHysStatusType.APPROVE.getCode()));	
		HolderAccountRequest holderAccountRequestHy = getHolderAccountRequestClose(holderAccountRequest.getIdHolderAccountReqPk());
		validateEntitiesOnRequest(holderAccountRequestHy);		
		updateRequestByState(holderAccountRequest);		
		return holderAccountRequest;
	}
	
	/**
	 * Confirm Holder Account request .
	 *
	 * @param holderAccountRequest the holder account request
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	public HolderAccountRequest confirmHolderAccountRequest(HolderAccountRequest holderAccountRequest) throws ServiceException{
		HolderAccountRequest holderAccountSend = getHolderAccountRequestClose(holderAccountRequest.getIdHolderAccountReqPk());
		validateEntitiesOnRequest(holderAccountSend);
		validateStatesRequestHys(holderAccountRequest.getIdHolderAccountReqPk(), Arrays.asList(HolderAccountRequestHysStatusType.APPROVE.getCode()));								
		updateRequestByState(holderAccountRequest);
		holderAccountRequest.setHolderAccount(updateHolderAccountFromRequest(holderAccountSend));				
		return holderAccountRequest;
	}
	
	/**
	 * Update hold account referenc.
	 *
	 * @param holdAccountRequestHy the hold account request
	 * @param holderAccount the holder account
	 * @throws ServiceException the service exception
	 */
	public void updateHoldAccReferenc(HolderAccountRequest holdAccountRequestHy, HolderAccount holderAccount) throws ServiceException{
		holdAccountRequestHy = find(HolderAccountRequest.class, holdAccountRequestHy.getIdHolderAccountReqPk());
		holdAccountRequestHy.setHolderAccount(holderAccount);
		update(holdAccountRequestHy);
	}
	
	/**
	 * Registry holder account from holder account request service bean.
	 *
	 * @param holderAcccounRequest the holder account request
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public HolderAccount createHolderAccountFromRequest(HolderAccountRequest holderAcccounRequest) throws ServiceException{
		HolderAccount holderAccount = new HolderAccount();
		HolderAccountRequest holderAccountRequestHy = getHolderAccountRequestClose(holderAcccounRequest.getIdHolderAccountReqPk());
		StringBuilder alternativeCode = new StringBuilder(holderAccountRequestHy.getParticipant().getIdParticipantPk().toString());
		holderAccount.setAccountNumber(getCorrelativeNumberHolderAccountServiceBean(holderAccountRequestHy.getParticipant().getIdParticipantPk()));		
		Holder holderMain = null;
		List<HolderAccountDetail> holderAccountDetailList = new ArrayList<HolderAccountDetail>();
		alternativeCode.append("-(");
		for(HolderAccountDetRequest holderAccountReqDetHy: holderAccountRequestHy.getHolderAccountDetRequest()){
			HolderAccountDetail holderAccountDetail = new HolderAccountDetail();
			holderAccountDetail.setHolderAccount(holderAccount);
			holderAccountDetail.setHolder(holderAccountReqDetHy.getHolder());
			if(holderAccountRequestHy.getHolderAccountType().equals(HolderAccountType.OWNERSHIP.getCode())){
				holderAccountDetail.setIndRepresentative(holderAccountReqDetHy.getIndRepresentative());
				if(holderAccountReqDetHy.getIndRepresentative().equals(BooleanType.YES.getCode())){
					holderMain = holderAccountDetail.getHolder();
				}
			}else if(holderAccountRequestHy.getHolderAccountType().equals(HolderAccountType.SUCCESSION.getCode())){
				holderAccountDetail.setIndRepresentative(holderAccountReqDetHy.getIndRepresentative());
				if(holderAccountReqDetHy.getIndRepresentative().equals(BooleanType.YES.getCode())){
					holderMain = holderAccountDetail.getHolder();
				}
			}else if(holderAccountRequestHy.getHolderAccountType().equals(HolderAccountType.JURIDIC.getCode()) || holderAccountRequestHy.getHolderAccountType().equals(HolderAccountType.NATURAL.getCode())){
				holderAccountDetail.setIndRepresentative(BooleanType.YES.getCode());
				holderMain = holderAccountDetail.getHolder(); 
			}			
			holderAccountDetail.setRegistryDate(holderAccountRequestHy.getRegistryDate());
			holderAccountDetail.setRegistryUser(holderAccountRequestHy.getRegistryUser());
			holderAccountDetail.setStateAccountDetail(HolderAccountStateDetailType.REGISTERED.getCode());
			holderAccountDetailList.add(holderAccountDetail);			
			alternativeCode.append(holderAccountReqDetHy.getHolder().getIdHolderPk());
			if(holderAccountRequestHy.getHolderAccountDetRequest().indexOf(holderAccountReqDetHy)!=holderAccountRequestHy.getHolderAccountDetRequest().size()-1){
				alternativeCode.append(",");
			}
		}		
		List<HolderAccountBank> holderAccountBankList = new ArrayList<HolderAccountBank>();
		for(HolderAccountBankRequest holderAccountReqBankHy: holderAccountRequestHy.getHolderAccountBankRequest()){
			HolderAccountBank holderAccountBank = new HolderAccountBank();
			holderAccountBank.setHolderAccount(holderAccount);
			holderAccountBank.setBank(holderAccountReqBankHy.getBank());
			holderAccountBank.setCurrency(holderAccountReqBankHy.getCurrency());
			holderAccountBank.setBankAccountType(holderAccountReqBankHy.getBankAccountType());
			holderAccountBank.setBankAccountNumber(holderAccountReqBankHy.getAccountNumber());
			holderAccountBank.setDescriptionBank(holderAccountReqBankHy.getDescriptionBank());	
			holderAccountBank.setHaveBic(0);
			holderAccountBank.setRegistryUser(holderAccountRequestHy.getRegistryUser());
			holderAccountBank.setRegistryDate(holderAccountRequestHy.getRegistryDate());
			holderAccountBank.setStateAccountBank(HolderAccountStateBankType.REGISTERED.getCode());
			holderAccountBank.setHolder(holderMain);			
			holderAccountBankList.add(holderAccountBank);
		}		
		alternativeCode.append(")-");
		alternativeCode.append(holderAccount.getAccountNumber());		
		List<HolderAccountFile> holderAccountFileList = new ArrayList<HolderAccountFile>();
		for(HolderAccountFileRequest holderAccountReqFileHy: holderAccountRequestHy.getHolderAccountFilesRequest()){
			HolderAccountFile holderAccountFile = new HolderAccountFile();
			holderAccountFile.setDocumentType(holderAccountReqFileHy.getDocumentType());
			holderAccountFile.setDocumentFile(holderAccountReqFileHy.getDocumentFile());
			holderAccountFile.setFilename(holderAccountReqFileHy.getFilename());
			holderAccountFile.setHolderAccount(holderAccount);
			holderAccountFile.setRegistryDate(new Date());
			holderAccountFile.setRegistryUser(holderAccountRequestHy.getRegistryUser());
			holderAccountFile.setIndMain(holderAccountReqFileHy.getIndMain());
			holderAccountFileList.add(holderAccountFile);
		}		
		holderAccount.setRegistryDate(holderAccountRequestHy.getRegistryDate());
		holderAccount.setRegistryUser(holderAccountRequestHy.getRegistryUser());
		holderAccount.setParticipant(holderAccountRequestHy.getParticipant());
		holderAccount.setAccountType(holderAccountRequestHy.getHolderAccountType());
		holderAccount.setAccountGroup(holderAccountRequestHy.getHolderAccountGroup());
		holderAccount.setStateAccount(HolderAccountStatusType.ACTIVE.getCode());	
		holderAccount.setRelatedName(holderAccountRequestHy.getRelatedName());
		holderAccount.setHolderAccountDetails(holderAccountDetailList);
		holderAccount.setHolderAccountBanks(holderAccountBankList);
		holderAccount.setHolderAccountFiles(holderAccountFileList);
		holderAccount.setAlternateCode(alternativeCode.toString());
		create(holderAccount);
		return holderAccount;
	}
	
	/**
	 * Update selective holder account request service bean.
	 *
	 * @param holderAccountRequestHy the holder account request
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	public HolderAccountRequest updateRequestByState(HolderAccountRequest holderAccountRequestHy) throws ServiceException{		
		StringBuilder jpqlQuery = new StringBuilder("Update HolderAccountRequest holderRequest SET holderRequest.requestState = :stateHolderAccountReqParameter, ");
		Map<String,Object> mapParameters = new HashMap<String,Object>();
		if(holderAccountRequestHy.getRequestState().equals(HolderAccountRequestHysStatusType.REGISTERED.getCode())){
			if(Validations.validateIsNotNullAndNotEmpty(holderAccountRequestHy.getAnullUser())){
				jpqlQuery.append("holderRequest.anullUser = :anullUserParameter, ").
						  append("holderRequest.anullDate = :anullDateParameter, ").
						  append("holderRequest.requestMotive = :motiveRequestParameter, ").
						  append("holderRequest.requestOtherMotive = :otherMotiveRequestParameter");
				mapParameters.put("anullUserParameter", holderAccountRequestHy.getAnullUser());
				mapParameters.put("anullDateParameter", holderAccountRequestHy.getAnullDate());
				mapParameters.put("motiveRequestParameter", holderAccountRequestHy.getRequestMotive());
				mapParameters.put("otherMotiveRequestParameter", holderAccountRequestHy.getRequestOtherMotive());
				mapParameters.put("stateHolderAccountReqParameter", HolderAccountRequestHysStatusType.ANULATE.getCode());
			}else if(Validations.validateIsNotNullAndNotEmpty(holderAccountRequestHy.getApproveUser())){
				jpqlQuery.append("holderRequest.approveUser = :approveUserParameter, ").
						  append("holderRequest.approveDate = :approveDateParameter");
				mapParameters.put("approveUserParameter", holderAccountRequestHy.getApproveUser());
				mapParameters.put("approveDateParameter", holderAccountRequestHy.getApproveDate());
				mapParameters.put("stateHolderAccountReqParameter", HolderAccountRequestHysStatusType.APPROVE.getCode());
			}
		}else if(holderAccountRequestHy.getRequestState().equals(HolderAccountRequestHysStatusType.APPROVE.getCode())){
			if(Validations.validateIsNotNullAndNotEmpty(holderAccountRequestHy.getConfirmUser())){
				jpqlQuery.append("holderRequest.confirmUser = :confirmUserParameter, ").
				  		  append("holderRequest.confirmDate = :confirmDateParameter");
				mapParameters.put("confirmUserParameter", holderAccountRequestHy.getConfirmUser());
				mapParameters.put("confirmDateParameter", holderAccountRequestHy.getConfirmDate());
				mapParameters.put("stateHolderAccountReqParameter", HolderAccountRequestHysStatusType.CONFIRM.getCode());
			}else if(Validations.validateIsNotNullAndNotEmpty(holderAccountRequestHy.getRejectUser())){
				jpqlQuery.append("holderRequest.rejectUser = :rejectUserParameter, ").
						  append("holderRequest.rejectDate = :rejectDateParameter, ").
						  append("holderRequest.requestMotive = :motiveRequestParameter, ").
						  append("holderRequest.requestOtherMotive = :otherMotiveRequestParameter");
				mapParameters.put("rejectUserParameter", holderAccountRequestHy.getRejectUser());
				mapParameters.put("rejectDateParameter", holderAccountRequestHy.getRejectDate());
				mapParameters.put("motiveRequestParameter", holderAccountRequestHy.getRequestMotive());
				mapParameters.put("otherMotiveRequestParameter", holderAccountRequestHy.getRequestOtherMotive());
				mapParameters.put("stateHolderAccountReqParameter", HolderAccountRequestHysStatusType.REJECT.getCode());
			}
		}
		jpqlQuery.append(" WHERE holderRequest.idHolderAccountReqPk = ").append(holderAccountRequestHy.getIdHolderAccountReqPk());
		Query query = em.createQuery(jpqlQuery.toString());		
        @SuppressWarnings("rawtypes")
		Iterator it= mapParameters.entrySet().iterator();
        while(it.hasNext()){
        	@SuppressWarnings("unchecked")
			Entry<String,Object> entry = (Entry<String,Object>)it.next();
        	query.setParameter(entry.getKey(), entry.getValue());
        }
		query.executeUpdate();
		return holderAccountRequestHy;
	}
	
	/**
	 * Validate states request.
	 *
	 * @param accountRequestId the account request id
	 * @param statesValidate the states validate
	 * @throws ServiceException the service exception
	 */
	public void validateStatesRequestHys(Long accountRequestId, List<Integer> statesValidate) throws ServiceException{
		Map<String,Object> holderAccountPkParam = new HashMap<String,Object>();
		holderAccountPkParam.put("idAccountReqPk", accountRequestId);
		HolderAccountRequest accountRequest = findObjectByNamedQuery(HolderAccountRequest.ACC_STATE, holderAccountPkParam);
		if(!statesValidate.contains(accountRequest.getRequestState())){
			throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
		}
	}
	
	/**
	 * Validate holder on request.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @throws ServiceException the service exception
	 */
	public void validateEntitiesOnRequest(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		Map<String,Object> holderPkParam = new HashMap<String,Object>();
		Map<String,Object> participantPkParam = new HashMap<String,Object>();
		Map<String,Object> holderAccountPkParam = new HashMap<String,Object>();
		for(HolderAccountDetRequest holderAccountDetail: holderAccountRequestHy.getHolderAccountDetRequest()){
			holderPkParam.put("idHolderPkParam", holderAccountDetail.getHolder().getIdHolderPk());
			Holder holder = findObjectByNamedQuery(Holder.HOLDER_STATE, holderPkParam);
			if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.HOLDER_BLOCK);
			}
		}
		participantPkParam.put("idParticipantPkParam", holderAccountRequestHy.getParticipant().getIdParticipantPk());
		Participant participant = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, participantPkParam);
		if(!participant.getState().equals(ParticipantStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
		}		
		if(holderAccountRequestHy.getHolderAccount()!=null){
			holderAccountPkParam.put("idHolderAccountPkParam", holderAccountRequestHy.getHolderAccount().getIdHolderAccountPk());
			HolderAccount holderAccount = findObjectByNamedQuery(HolderAccount.HOLDER_ACCOUNT_STATE, holderAccountPkParam);
			if(holderAccountRequestHy.getRequestType().equals(HolderAccountRequestHysType.UNBLOCK.getCode())){
				if(!holderAccount.getStateAccount().equals(HolderAccountStatusType.BLOCK.getCode())){
					throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_BLOCK);
				}
			}else{
				if(!holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())){
					throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_BLOCK);
				}
				for(HolderAccountDetail holderAccountDetail: holderAccountRequestHy.getHolderAccount().getHolderAccountDetails()){
					holderPkParam.put("idHolderPkParam", holderAccountDetail.getHolder().getIdHolderPk());
					Holder holder = findObjectByNamedQuery(Holder.HOLDER_STATE, holderPkParam);
					if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
						throw new ServiceException(ErrorServiceType.HOLDER_BLOCK);
					}
				}
			}
		}
	}
	
	/**
	 * Update holder account from request.
	 *
	 * @param holderAcccounRequest the holder acccoun request
	 * @return the holder account
	 * @throws ServiceException the service exception
	 */
	public HolderAccount updateHolderAccountFromRequest(HolderAccountRequest holderAcccounRequest) throws ServiceException{		
		HolderAccount holderAccount = holderAcccounRequest.getHolderAccount();		
		switch(HolderAccountRequestHysType.lookup.get(holderAcccounRequest.getRequestType())){
				case BLOCK: holderAccount.setStateAccount(HolderAccountStatusType.BLOCK.getCode());break;
				case UNBLOCK: holderAccount.setStateAccount(HolderAccountStatusType.ACTIVE.getCode());break;
				case CLOSE: holderAccount.setStateAccount(HolderAccountStatusType.CLOSED.getCode());break;			
		}
		update(holderAccount);
		return holderAccount;
	}
	
	/**
	 * Gets the correlative number holder account service bean.
	 *
	 * @param idParticipantPK the id participant pk
	 * @return the correlative number holder account service bean
	 * @throws ServiceException the service exception
	 */
	public synchronized Integer getCorrelativeNumberHolderAccountServiceBean(Long idParticipantPK) throws ServiceException{
		Query query = em.createQuery("SELECT MAX(ha.accountNumber) + 1 FROM HolderAccount ha WHERE ha.participant.idParticipantPk = :idParticipantPkParameter");
		query.setParameter("idParticipantPkParameter", idParticipantPK);
		Integer correlative = 0;
		try{
			correlative = (Integer) query.getSingleResult();
		}catch(Exception ex){
			correlative = null;
		}
		if(Validations.validateIsNull(correlative)){
			correlative = 1;
		}
		return correlative;
	}
	
	/**
	 * Holder account can will closed.
	 *
	 * @param holderAccount the holder account
	 * @return the boolean
	 */
	public Boolean holderAccountCanWillClosedServiceBean(HolderAccount holderAccount){
		if(holderAccount==null || holderAccount.getIdHolderAccountPk()==null){
			return Boolean.FALSE;
		}		
		StringBuilder queryValidate = new StringBuilder();
		queryValidate.append("SELECT COUNT(*) "+
							 "	FROM HolderAccountOperation hao "+
							 "	INNER JOIN hao.mechanismOperation mo "+
							 "	INNER JOIN mo.mechanisnModality mmo "+ 							
							 "	INNER JOIN mmo.negotiationModality negmo "+
							 "	WHERE hao.holderAccount.idHolderAccountPk = :idAccountPkParam AND mo.operationState "+ 
							 "	IN( "+
							 "	  :operationRegisteredParam "+
							 "	  ,:otcOperationConfirmedParam "+
							 "	  , (CASE negmo.indTermSettlement " +
							 "					WHEN 1 THEN :operationSettlementTermParam " +
							 "					ELSE :operationSettlementCashParam " +
							 "		END) "+
							 "	  ) ");				
		Query query = em.createQuery(queryValidate.toString());
		query.setParameter("operationRegisteredParam",MechanismOperationStateType.REGISTERED_STATE.getCode());
		query.setParameter("otcOperationConfirmedParam",MechanismOperationStateType.ASSIGNED_STATE.getCode());
		query.setParameter("operationSettlementCashParam",MechanismOperationStateType.CASH_SETTLED.getCode());
		query.setParameter("operationSettlementTermParam",MechanismOperationStateType.TERM_SETTLED.getCode());
		query.setParameter("idAccountPkParam",holderAccount.getIdHolderAccountPk());
		Long rowNumbers = 0L;
		try{
			rowNumbers = ((Long)query.getSingleResult()).longValue();
		}catch(NoResultException ex){
			rowNumbers = 0L;
		}catch(NullPointerException e){
			rowNumbers = 0L;
		}
		if(rowNumbers!=null && rowNumbers>0L){
			return Boolean.FALSE;
		}
		
		StringBuilder validateBalance = new StringBuilder();
		validateBalance.append("SELECT SUM(totalBalance) FROM HolderAccountBalance hab WHERE hab.holderAccount.idHolderAccountPk = :idAccountParam");
		Query queryBalance = em.createQuery(validateBalance.toString());
		queryBalance.setParameter("idAccountParam", holderAccount.getIdHolderAccountPk());
		Long totalBalance = 0L;
		try{
			totalBalance = ((BigDecimal)queryBalance.getSingleResult()).longValue();
		}catch(NoResultException ex){
			totalBalance = 0L;
		}catch(NullPointerException e){
			totalBalance = 0L;
		}
		if(totalBalance!=null && totalBalance>0L){
			return Boolean.FALSE;
		}		
		return Boolean.TRUE;
	}
	
	/**
	 * Gets the holder account list service bean.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account list service bean
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> getHolderAccountListServiceBean(HolderAccountTO holderAccountTO) throws ServiceException{
		return accountsFacade.getHolderAccountListComponentServiceFacade(holderAccountTO);
	}
	
	/**
	 * Gets the holder account req file.
	 *
	 * @param idFilePk the id file pk
	 * @return the holder account req file
	 * @throws ServiceException the service exception
	 */
	public byte[] getHolderAccountReqFile(Long idFilePk) throws ServiceException{
		String query = "Select har.documentFile FROM HolderAccountFileRequest har WHERE har.idHolderAccountReqFilePk = :idFilePkParam";
		Query queryJpql = em.createQuery(query);
		queryJpql.setParameter("idFilePkParam", idFilePk);
		try{
			return (byte[]) queryJpql.getSingleResult();
		}catch(NoResultException ex){
			return null;
		}
	}
	
	/**
	 * Gets the holder account file.
	 *
	 * @param idFilePk the id file pk
	 * @return the holder account file
	 * @throws ServiceException the service exception
	 */
	public byte[] getHolderAccountFile(Long idFilePk) throws ServiceException{
		String query = "Select har.documentFile FROM HolderAccountFile har WHERE har.idHolderAccountFilePk = :idFilePkParam";
		Query queryJpql = em.createQuery(query);
		queryJpql.setParameter("idFilePkParam", idFilePk);
		try{
			return (byte[]) queryJpql.getSingleResult();
		}catch(NoResultException ex){
			return null;
		}
	}
}