package com.pradera.accounts.holderaccount.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountBank;
import com.pradera.model.accounts.holderaccounts.HolderAccountBankRequest;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetRequest;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.HolderAccountFile;
import com.pradera.model.accounts.holderaccounts.HolderAccountFileRequest;
import com.pradera.model.accounts.holderaccounts.HolderAccountRequest;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestHysStatusType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestHysType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStateBankType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStateDetailType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountMovement;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class HolderAccountServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 28/02/2013
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class HolderAccountServiceBean extends CrudDaoServiceBean{
	
	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
	/** The parameter service bean. */
	@EJB
	ParameterServiceBean parameterServiceBean;

	/**
	 * Gets the bank list service bean.
	 *
	 * @return the bank list service bean
	 */
	@SuppressWarnings("unchecked")
	public List<Bank> getBankListServiceBean(){
		Query query = em.createQuery("Select b From Bank b WHERE b.state = :stateBankParam");
		query.setParameter("stateBankParam", MasterTableType.PARAMETER_TABLE_BANK_STATE_REGISTERED.getCode());
		return (List<Bank>)query.getResultList();
	}
	
	/**
	 * Find holder accounts service bean.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccount> findHolderAccountsServiceBean(HolderAccountTO holderAccountTO) {
		List<HolderAccount> result = null;
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT distinct had.holderAccount.idHolderAccountPk");
		sbQuery.append("	FROM HolderAccountDetail had");
		sbQuery.append("	WHERE had.holderAccount.stateAccount = :stateAccount");
		sbQuery.append("	AND had.holderAccount.participant.idParticipantPk = :idParticipantPk");
		if(Validations.validateIsNotNullAndNotEmpty(holderAccountTO.getIdHolderPk())){
		sbQuery.append("	AND had.holder.idHolderPk = :idHolderPk");		
		}
		if(Validations.validateIsNotNullAndNotEmpty(holderAccountTO.getHolderAccountType())){
		sbQuery.append("	AND had.holderAccount.accountType = :accountType");
		}
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("stateAccount", holderAccountTO.getStatus());
		query.setParameter("idParticipantPk", holderAccountTO.getParticipantTO());
		if(Validations.validateIsNotNullAndNotEmpty(holderAccountTO.getIdHolderPk())){
			query.setParameter("idHolderPk", holderAccountTO.getIdHolderPk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(holderAccountTO.getHolderAccountType())){
			query.setParameter("accountType", holderAccountTO.getHolderAccountType());
		}
		List<Long> lstIdHolderAccountPk = (List<Long>)query.getResultList();
		if(Validations.validateListIsNotNullAndNotEmpty(lstIdHolderAccountPk)){
			sbQuery = new StringBuilder();
			sbQuery.append("	SELECT distinct ha");
			sbQuery.append("	FROM HolderAccount ha");
			sbQuery.append("	INNER JOIN FETCH ha.holderAccountDetails had");
			sbQuery.append("	INNER JOIN FETCH had.holder");
			sbQuery.append("	WHERE ha.idHolderAccountPk in (:lstIdHolderAccountPk)");
			sbQuery.append("	ORDER BY ha.idHolderAccountPk DESC");
			query = em.createQuery(sbQuery.toString());
			query.setParameter("lstIdHolderAccountPk", lstIdHolderAccountPk);
			result = (List<HolderAccount>)query.getResultList();
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<HolderAccount> getHolderAccountPkList(HolderAccountTO holderAccountTO) throws ServiceException{
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuilder q = new StringBuilder();
		q.append(" SELECT ha FROM HolderAccount ha");
		q.append(" JOIN ha.holderAccountDetails had");
		q.append(" JOIN had.holder ho");
		q.append(" WHERE 1=1");
		if(holderAccountTO.getIdHolderPk()!=null){
			q.append(" AND ho.idHolderPk = :idHolderPk	");
			parameters.put("idHolderPk", holderAccountTO.getIdHolderPk());
		}
		if(holderAccountTO.getParticipantTO() != null){
			q.append("	AND	ha.participant.idParticipantPk = :idParticipantPk	");
			parameters.put("idParticipantPk", holderAccountTO.getParticipantTO());			
		}		
		if(holderAccountTO.getRelatedName() != null){
			q.append("	AND	ha.relatedName LIKE :relatedName	");
			parameters.put("relatedName", "%" +  holderAccountTO.getRelatedName() + "%");				
		}
		
		return findListByQueryString(q.toString(), parameters);
	}
	
	public Long getMostRecentHolderRequest(HolderAccountTO holderAccountTO){
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuilder q = new StringBuilder();
		q.append(" SELECT max(har.idHolderAccountReqPk) FROM HolderAccountRequest har");
		q.append(" WHERE 1=1");
		if(holderAccountTO.getIdHolderAccountPk() !=null ){
			q.append("	AND	har.holderAccount.idHolderAccountPk = :idHolderAccountPk	");
			parameters.put("idHolderAccountPk", holderAccountTO.getIdHolderAccountPk());			
		}
		if(holderAccountTO.getRequestNumber() !=null ){
			q.append("	AND	har.requestNumber = :requestNumber	");
			parameters.put("requestNumber", holderAccountTO.getRequestNumber());			
		}
		if(Validations.validateIsNotNullAndNotEmpty(holderAccountTO.getInitialDate()) && Validations.validateIsNotNullAndNotEmpty(holderAccountTO.getFinalDate())){
			q.append(" AND har.registryDate between :dateIni and :dateEnd ");
			parameters.put("dateIni", holderAccountTO.getInitialDate());
			parameters.put("dateEnd", holderAccountTO.getFinalDate());			
		}
		if(Validations.validateIsNotNull(holderAccountTO.getStatus()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(holderAccountTO.getStatus())))){
			q.append("	AND	har.requestState = :requestState	");
			parameters.put("requestState", holderAccountTO.getStatus());							
		}
		if(Validations.validateIsNotNull(holderAccountTO.getHolderAccountRequestType()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(holderAccountTO.getHolderAccountRequestType())))){
			q.append("	AND	har.requestType = :requestType	");
			parameters.put("requestType", holderAccountTO.getHolderAccountRequestType());			
		} 		
		
		Object res = findObjectByQueryString(q.toString(), parameters);
		
		if(Validations.validateIsNotNull(res)) {
			return (Long) res;
		} else {
			return null;
		}
	}
	
	public List<HolderAccountRequest> getMostRecentHolderRequestList(HolderAccountTO holderAccountTO) throws ServiceException{
		List<HolderAccount> holderAccountPks = getHolderAccountPkList(holderAccountTO);
		List<HolderAccountRequest> res = new ArrayList<HolderAccountRequest>();
		
		if(Validations.validateListIsNotNullAndNotEmpty(holderAccountPks)) {
			for(HolderAccount holderAccount : holderAccountPks) {
				holderAccountTO.setIdHolderAccountPk(holderAccount.getIdHolderAccountPk());
				Long mostRecentRequest = getMostRecentHolderRequest(holderAccountTO);
				if(Validations.validateIsNotNull(mostRecentRequest)) {
					HolderAccountTO newFilter = new HolderAccountTO();
					newFilter.setIdHolderAccountRequestPk(mostRecentRequest);
					
					List<HolderAccountRequest> requestList = getHolderAccountRequestsList(newFilter);
					if(Validations.validateListIsNotNullAndNotEmpty(requestList)) {
						res.add(requestList.get(0));
					}
				}
			}
		} 

		return res;
	}
	
	@SuppressWarnings("unchecked")
	public List<HolderAccountRequest> getHolderAccountRequestsList(HolderAccountTO holderAccountTO) throws ServiceException{		
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("	Select har.idHolderAccountReqPk, har.registryDate,	"); //0, 1
		stringBuilder.append("	har.holderAccountType, (Select p.parameterName From ParameterTable p Where p.parameterTablePk = har.holderAccountType),	"); // 2, 3
		stringBuilder.append("	har.requestState, (Select p.parameterName From ParameterTable p Where p.parameterTablePk = har.requestState),	"); // 4, 5
		stringBuilder.append("	har.participant.idParticipantPk, har.participant.mnemonic,	"); //6, 7
		stringBuilder.append("	ho.idHolderPk, ho.fullName,	");	//8, 9
		stringBuilder.append("	ho.documentType, (Select p.indicator1 From ParameterTable p Where p.parameterTablePk = ho.documentType), ");//10, 11
		stringBuilder.append("	ho.documentNumber "); //12
		stringBuilder.append("	,har.registryUser "); //13
		stringBuilder.append("	from HolderAccountRequest har	");
		stringBuilder.append("	join har.holderAccountDetRequest hard	");
		stringBuilder.append("	join hard.holder ho	");
		stringBuilder.append("	Where 1 = 1	");		
		
		if(holderAccountTO.getIdHolderPk()!=null){
			stringBuilder.append(" AND ho.idHolderPk = :idHolderPk	");
			parameters.put("idHolderPk", holderAccountTO.getIdHolderPk());
		}
		if(holderAccountTO.getRequestNumber() !=null ){
			stringBuilder.append("	AND	har.requestNumber = :requestNumber	");
			parameters.put("requestNumber", holderAccountTO.getRequestNumber());			
		}
		if(holderAccountTO.getParticipantTO() != null){
			stringBuilder.append("	AND	har.participant.idParticipantPk = :idParticipantPk	");
			parameters.put("idParticipantPk", holderAccountTO.getParticipantTO());			
		}		
		if(holderAccountTO.getRelatedName() != null){
			stringBuilder.append("	AND	har.relatedName LIKE :relatedName	");
			parameters.put("relatedName", "%" +  holderAccountTO.getRelatedName() + "%");				
		}
		if(Validations.validateIsNotNullAndNotEmpty(holderAccountTO.getInitialDate()) && Validations.validateIsNotNullAndNotEmpty(holderAccountTO.getFinalDate())){
			stringBuilder.append(" AND har.registryDate between :dateIni and :dateEnd ");
			parameters.put("dateIni", holderAccountTO.getInitialDate());
			parameters.put("dateEnd", holderAccountTO.getFinalDate());			
		}
		if(Validations.validateIsNotNull(holderAccountTO.getStatus()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(holderAccountTO.getStatus())))){
			stringBuilder.append("	AND	har.requestState = :requestState	");
			parameters.put("requestState", holderAccountTO.getStatus());							
		}
		if(Validations.validateIsNotNull(holderAccountTO.getHolderAccountRequestType()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(holderAccountTO.getHolderAccountRequestType())))){
			stringBuilder.append("	AND	har.requestType = :requestType	");
			parameters.put("requestType", holderAccountTO.getHolderAccountRequestType());			
		} 		
		if(holderAccountTO.getIdHolderAccountRequestPk() != null){
			stringBuilder.append("	AND	har.idHolderAccountReqPk = :idHolderAccountReqPk	");
			parameters.put("idHolderAccountReqPk", holderAccountTO.getIdHolderAccountRequestPk());			
		}
		List<Object[]> accountHolderRequestList =  findListByQueryString(stringBuilder.toString(), parameters);
		Map<Long,HolderAccountRequest> accountsHolderRequestPk = new HashMap<Long, HolderAccountRequest>();		
		for(Object[] object: accountHolderRequestList){
			if(accountsHolderRequestPk.get(object[0]) == null){
				HolderAccountRequest holderAccountRequest = new HolderAccountRequest();
				holderAccountRequest.setHolderAccountDetRequest(new ArrayList<HolderAccountDetRequest>());	
				holderAccountRequest.setIdHolderAccountReqPk(new Long(object[0].toString()));				
				holderAccountRequest.setRegistryDate((Date) object[1]);				
				holderAccountRequest.setHolderAccountType(new Integer(object[2].toString()));
				holderAccountRequest.setHolderAccountTypeDescription(object[3].toString());
				holderAccountRequest.setRequestState(new Integer(object[4].toString()));
				holderAccountRequest.setStateAccountName(object[5].toString());
				holderAccountRequest.setRegistryUser(object[13].toString());
				Participant participant = new Participant();
				participant.setIdParticipantPk(new Long(object[6].toString()));
				participant.setMnemonic(object[7].toString());
				holderAccountRequest.setParticipant(participant);				
				accountsHolderRequestPk.put(holderAccountRequest.getIdHolderAccountReqPk(), holderAccountRequest);
			}
			HolderAccountRequest accountRequestMap = accountsHolderRequestPk.get(object[0]);
			HolderAccountDetRequest accountRequestDetail = new HolderAccountDetRequest();
			Holder holder = new Holder();						
			holder.setIdHolderPk(new Long(object[8].toString()));
			holder.setFullName(object[9].toString());
			holder.setDocumentType(new Integer(object[10].toString()));
			holder.setDescriptionTypeDocument(object[11].toString());
			holder.setDocumentNumber(object[12].toString());
			accountRequestDetail.setHolder(holder);
			accountRequestMap.getHolderAccountDetRequest().add(accountRequestDetail);
		}
		return new ArrayList<HolderAccountRequest>(accountsHolderRequestPk.values());
	}
	
	/**
	 * Registry holder account request service bean.
	 *
	 * @param holderAccountRequest the holder account request hy
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean registryHolderAccountRequest(HolderAccountRequest holderAccountRequest) throws ServiceException{
		holderAccountRequest.setIndPdd(holderAccountRequest.isBlPdd()?BooleanType.YES.getCode():BooleanType.NO.getCode());
		validateEntitiesOnRequest(holderAccountRequest);		
		create(holderAccountRequest);
		Query query = em.createQuery(new StringBuilder("UPDATE HolderAccountRequest r SET r.requestNumber = r.idHolderAccountReqPk ").append(
									 				   " WHERE r.idHolderAccountReqPk = :idHolderAccountRequestParameter").toString());
		query.setParameter("idHolderAccountRequestParameter", holderAccountRequest.getIdHolderAccountReqPk());
		query.executeUpdate();
		return true;
	}	
	
	/**
	 * Validate holder on request.
	 *
	 * @param holderAccountRequestHy the holder account request
	 * @throws ServiceException the service exception
	 */
	public void validateEntitiesOnRequest(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		Map<String,Object> holderPkParam = new HashMap<String,Object>();
		Map<String,Object> participantPkParam = new HashMap<String,Object>();
		Map<String,Object> holderAccountPkParam = new HashMap<String,Object>();
		for(HolderAccountDetRequest holderAccountDetail: holderAccountRequestHy.getHolderAccountDetRequest()){
			holderPkParam.put("idHolderPkParam", holderAccountDetail.getHolder().getIdHolderPk());
			Holder holder = findObjectByNamedQuery(Holder.HOLDER_STATE, holderPkParam);
			if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.HOLDER_BLOCK);
			}
		}
		participantPkParam.put("idParticipantPkParam", holderAccountRequestHy.getParticipant().getIdParticipantPk());
		Participant participant = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, participantPkParam);
		if(!participant.getState().equals(ParticipantStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
		}		
		if(holderAccountRequestHy.getHolderAccount()!=null){
			holderAccountPkParam.put("idHolderAccountPkParam", holderAccountRequestHy.getHolderAccount().getIdHolderAccountPk());
			HolderAccount holderAccount = findObjectByNamedQuery(HolderAccount.HOLDER_ACCOUNT_STATE, holderAccountPkParam);
			if(holderAccountRequestHy.getRequestType().equals(HolderAccountRequestHysType.UNBLOCK.getCode())){
				if(!holderAccount.getStateAccount().equals(HolderAccountStatusType.BLOCK.getCode())){
					throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_BLOCK);
				}
			}else{
				if(!holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())){
					throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_BLOCK);
				}
				for(HolderAccountDetail holderAccountDetail: holderAccountRequestHy.getHolderAccount().getHolderAccountDetails()){
					holderPkParam.put("idHolderPkParam", holderAccountDetail.getHolder().getIdHolderPk());
					Holder holder = findObjectByNamedQuery(Holder.HOLDER_STATE, holderPkParam);
					if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
						throw new ServiceException(ErrorServiceType.HOLDER_BLOCK);
					}
				}
			}
		}
	}
	
	/**
	 * Approve Holder Account request .
	 *
	 * @param holderAccountRequest the holder account request
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	public HolderAccountRequest approveHolderAccountRequest(HolderAccountRequest holderAccountRequest) throws ServiceException{
		validateStatesRequestHys(holderAccountRequest.getIdHolderAccountReqPk(), Arrays.asList(HolderAccountRequestHysStatusType.REGISTERED.getCode()));	
		validateEntitiesOnRequest(holderAccountRequest);
		updateRequestByState(holderAccountRequest,HolderAccountRequestHysStatusType.APPROVE.getCode(),null);		
		return holderAccountRequest;
	}
	
	/**
	 * Anulate Holder Account request .
	 *
	 * @param holderAccountRequest the holder account request
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	public HolderAccountRequest anulateHolderAccountRequest(HolderAccountRequest holderAccountRequest) throws ServiceException{
		validateStatesRequestHys(holderAccountRequest.getIdHolderAccountReqPk(), Arrays.asList(HolderAccountRequestHysStatusType.REGISTERED.getCode()));	
		validateEntitiesOnRequest(holderAccountRequest);		
		updateRequestByState(holderAccountRequest,HolderAccountRequestHysStatusType.ANULATE.getCode(),null);		
		return holderAccountRequest;
	}
	
	/**
	 * Reject Holder Account request .
	 *
	 * @param holderAccountRequest the holder account request
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	public HolderAccountRequest rejectHolderAccountRequest(HolderAccountRequest holderAccountRequest) throws ServiceException{
		validateStatesRequestHys(holderAccountRequest.getIdHolderAccountReqPk(), Arrays.asList(HolderAccountRequestHysStatusType.APPROVE.getCode()));	
		validateEntitiesOnRequest(holderAccountRequest);		
		updateRequestByState(holderAccountRequest,HolderAccountRequestHysStatusType.REJECT.getCode(),null);		
		return holderAccountRequest;
	}
	
	/**
	 * Confirm Holder Account request .
	 *
	 * @param holderAccountRequest the holder account request
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	public HolderAccountRequest confirmHolderAccountRequest(HolderAccountRequest holderAccountRequest) throws ServiceException{
		validateStatesRequestHys(holderAccountRequest.getIdHolderAccountReqPk(), Arrays.asList(HolderAccountRequestHysStatusType.APPROVE.getCode()));
		validateEntitiesOnRequest(holderAccountRequest);
		HolderAccount holderAccount = createHolderAccountFromRequest(holderAccountRequest, null, null);		
		updateRequestByState(holderAccountRequest,HolderAccountRequestHysStatusType.CONFIRM.getCode(),holderAccount.getIdHolderAccountPk());			
		holderAccountRequest.setHolderAccount(holderAccount);
		return holderAccountRequest;
	}
	
	/**
	 * Gets the holder account request service bean.
	 *
	 * @param idRequestPk the id request
	 * @return the holder account request service bean
	 * @throws ServiceException the service exception 
	 */
	public HolderAccountRequest getHolderAccountRequest(Long idRequestPk) throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_FILES.getCode());
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		List<ParameterTable> listParameterTable = null;		
		listParameterTable = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);						
		String query = "Select distinct har From HolderAccountRequest har " +
				"			Inner Join Fetch har.participant part " +
				"			Inner Join Fetch har.holderAccountDetRequest hard " +
				"			Inner Join Fetch hard.holder ho " +								
				"			Where har.idHolderAccountReqPk = :idPk";		
		Query queryJpa = em.createQuery(query); 
		queryJpa.setParameter("idPk", idRequestPk);						
		HolderAccountRequest holderAccountRequestHy = (HolderAccountRequest) queryJpa.getSingleResult();			
		holderAccountRequestHy.getHolderAccountFilesRequest().size();
		holderAccountRequestHy.getHolderAccountBankRequest().size();	
		holderAccountRequestHy.setBlPdd(holderAccountRequestHy.getIndPdd()==BooleanType.YES.getCode()? true:false);
		if(holderAccountRequestHy.getHolderAccount() != null){
			HolderAccountTO holderAccountTO = new HolderAccountTO();
			holderAccountTO.setNeedHolder(Boolean.TRUE);
			holderAccountTO.setNeedFiles(Boolean.TRUE);
			holderAccountTO.setNeedParticipant(Boolean.FALSE);
			holderAccountTO.setIdHolderAccountPk(holderAccountRequestHy.getHolderAccount().getIdHolderAccountPk());
			holderAccountRequestHy.setHolderAccount(getHolderAccount(holderAccountTO));
		}
		for(HolderAccountFileRequest file : holderAccountRequestHy.getHolderAccountFilesRequest()){
			for(ParameterTable parameter: listParameterTable){
				if(file.getDocumentType().equals(parameter.getParameterTablePk())){
					file.setDocumentTypeDesc(parameter.getDescription());
				}
			}
		}	
		return holderAccountRequestHy;
	}
	
	/**
	 * Validate states request hys.
	 *
	 * @param accountRequestId the account request id
	 * @param statesValidate the states validate
	 * @throws ServiceException the service exception
	 */
	public void validateStatesRequestHys(Long accountRequestId, List<Integer> statesValidate) throws ServiceException{
		Map<String,Object> holderAccountPkParam = new HashMap<String,Object>();
		holderAccountPkParam.put("idAccountReqPk", accountRequestId);
		HolderAccountRequest accountRequest = findObjectByNamedQuery(HolderAccountRequest.ACC_STATE, holderAccountPkParam);
		if(!statesValidate.contains(accountRequest.getRequestState())){
			throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
		}
	}

	/**
	 * Update selective holder account request service bean.
	 *
	 * @param holderAccountRequestHy the holder account request
	 * @param newState the new state
	 * @param idHolderAccount the id holder account
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	public HolderAccountRequest updateRequestByState(HolderAccountRequest holderAccountRequestHy, Integer newState, Long idHolderAccount) throws ServiceException{		
		StringBuilder jpqlQuery = new StringBuilder("Update HolderAccountRequest SET requestState = :stateHolderAccountReqParameter ");
		Map<String,Object> mapParameters = new HashMap<String,Object>();
		mapParameters.put("stateHolderAccountReqParameter", newState);
		mapParameters.put("idHolderAccRequest", holderAccountRequestHy.getIdHolderAccountReqPk());
		
		if(newState.equals(HolderAccountRequestHysStatusType.ANULATE.getCode())){
			jpqlQuery.append(", anullUser = :anullUserParameter ").
					  append(", anullDate = :anullDateParameter ").
					  append(", requestMotive = :motiveRequestParameter ").
					  append(", requestOtherMotive = :otherMotiveRequestParameter");
			mapParameters.put("anullUserParameter", holderAccountRequestHy.getAnullUser());
			mapParameters.put("anullDateParameter", holderAccountRequestHy.getAnullDate());
			mapParameters.put("motiveRequestParameter", holderAccountRequestHy.getRequestMotive());
			mapParameters.put("otherMotiveRequestParameter", holderAccountRequestHy.getRequestOtherMotive());
		}else if(newState.equals(HolderAccountRequestHysStatusType.REJECT.getCode())){
			jpqlQuery.append(", rejectUser = :rejectUserParameter ").
					  append(", rejectDate = :rejectDateParameter ").
					  append(", requestMotive = :motiveRequestParameter ").
					  append(", requestOtherMotive = :otherMotiveRequestParameter");
			mapParameters.put("rejectUserParameter", holderAccountRequestHy.getRejectUser());
			mapParameters.put("rejectDateParameter", holderAccountRequestHy.getRejectDate());
			mapParameters.put("motiveRequestParameter", holderAccountRequestHy.getRequestMotive());
			mapParameters.put("otherMotiveRequestParameter", holderAccountRequestHy.getRequestOtherMotive());
		}else if(newState.equals(HolderAccountRequestHysStatusType.APPROVE.getCode())){
			jpqlQuery.append(", approveUser = :approveUserParameter ");
			jpqlQuery.append(", approveDate = :approveDateParameter ");
			mapParameters.put("approveUserParameter", holderAccountRequestHy.getApproveUser());
			mapParameters.put("approveDateParameter", holderAccountRequestHy.getApproveDate());
		}else if(newState.equals(HolderAccountRequestHysStatusType.CONFIRM.getCode())){
			jpqlQuery.append(", confirmUser = :confirmUserParameter ").
			  		  append(", confirmDate = :confirmDateParameter ").
					  append(", holderAccount.idHolderAccountPk = :idHolderAccount ");
			mapParameters.put("confirmUserParameter", holderAccountRequestHy.getConfirmUser());
			mapParameters.put("confirmDateParameter", holderAccountRequestHy.getConfirmDate());
			mapParameters.put("idHolderAccount", idHolderAccount);
		}
		
		jpqlQuery.append(" WHERE idHolderAccountReqPk = :idHolderAccRequest ");
		
		Query query = em.createQuery(jpqlQuery.toString());		
        @SuppressWarnings("rawtypes")
		Iterator it= mapParameters.entrySet().iterator();
        while(it.hasNext()){
        	@SuppressWarnings("unchecked")
			Entry<String,Object> entry = (Entry<String,Object>)it.next();
        	query.setParameter(entry.getKey(), entry.getValue());
        }
		query.executeUpdate();
		return holderAccountRequestHy;
	}
	
	/**
	 * Gets the holder account by native query service bean.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account by native query service bean
	 */
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getHolderAccountList(HolderAccountTO holderAccountTO){
		Map<String,Object> mapParameters = new HashMap<String,Object>();
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append("		ha.idHolderAccountPk, ha.accountNumber, ha.registryDate, ha.participant.idParticipantPk, pa.mnemonic, " );
		sb.append("		ho.name, ho.firstLastName, ho.secondLastName, " );
		sb.append("		ha.accountGroup, ha.accountType, ha.stateAccount, ho.idHolderPk, ho.fullName, " );
		sb.append("		ho.documentType, ho.documentNumber, " );
		sb.append("		(SELECT p.indicator1 FROM ParameterTable p WHERE p.parameterTablePk = ho.documentType),  " );
		sb.append("		(SELECT p.parameterName FROM ParameterTable p WHERE p.parameterTablePk = ha.accountGroup),  " );
		sb.append("		(SELECT p.parameterName FROM ParameterTable p WHERE p.parameterTablePk = ha.accountType),  " );
		sb.append("		(SELECT p.parameterName FROM ParameterTable p WHERE p.parameterTablePk = ha.stateAccount),  " );
		sb.append("		ho.documentSource " );
		sb.append("	FROM 	HolderAccount ha ");
		sb.append(" JOIN	ha.holderAccountDetails had ");
		sb.append("	JOIN	had.holder	ho" );
		sb.append("	JOIN 	ha.participant pa " );
		sb.append("	Where 1 = 1  ");		
		if(holderAccountTO.getParticipantTO() != null){
			sb.append("	AND	ha.participant.idParticipantPk = :participantParam " );
			mapParameters.put("participantParam", holderAccountTO.getParticipantTO());
		}
		if(holderAccountTO.getStatus()!=null){
			sb.append("	AND ha.stateAccount = :stateAccountParam " );
			mapParameters.put("stateAccountParam", holderAccountTO.getStatus());
		}				
		if(holderAccountTO.getIdHolderPk()!=null){
			StringBuilder stringSubQuery = new StringBuilder();
			stringSubQuery.append("	SELECT ha2.idHolderAccountPk FROM HolderAccount ha2 ");
			stringSubQuery.append("	JOIN ha2.holderAccountDetails had2 JOIN had2.holder	ho2  ");			
			stringSubQuery.append("	WHERE 1 = 1 " +
					" AND ho2.idHolderPk = :holderIdParam ");				
			sb.append("	AND ha.idHolderAccountPk IN ( " );
			sb.append(stringSubQuery.toString());			
			sb.append(") ");
			mapParameters.put("holderIdParam", holderAccountTO.getIdHolderPk());			 
		}
		if(holderAccountTO.getHolderAccountNumber()!=null){
			sb.append("	AND ha.accountNumber = :accountNumberParam " );
			mapParameters.put("accountNumberParam", holderAccountTO.getHolderAccountNumber());
		}
		if(holderAccountTO.getHolderAccountType()!=null){
			sb.append("	AND ha.accountType = :accountTypeParam " );
			mapParameters.put("accountTypeParam", holderAccountTO.getHolderAccountType());
		}
		if(holderAccountTO.getHolderaccountGroupType()!=null){
			sb.append("	AND ha.accountGroup = :accountGroupTypeParam " );
			mapParameters.put("accountGroupTypeParam", holderAccountTO.getHolderaccountGroupType());
		}
		if(holderAccountTO.getRelatedName()!=null){
			sb.append("	AND ha.relatedName like :accountRelatedNameParam " );
			mapParameters.put("accountRelatedNameParam", "%" + holderAccountTO.getRelatedName() + "%");
		}
		if(holderAccountTO.getInitialDate()!=null && holderAccountTO.getFinalDate()!=null){
			sb.append(" AND TRUNC(ha.registryDate) between :initialDate and :finalDate");
			mapParameters.put("initialDate", holderAccountTO.getInitialDate());
			mapParameters.put("finalDate", holderAccountTO.getFinalDate());
		}
		if(holderAccountTO.getHolderPrincipal()!=null){
			StringBuilder stringSubQuery1 = new StringBuilder();
			stringSubQuery1.append("	SELECT ha2.idHolderAccountPk FROM HolderAccount ha2 ");
			stringSubQuery1.append("	JOIN ha2.holderAccountDetails had2 JOIN had2.holder	ho2  ");			
			stringSubQuery1.append("	WHERE 1 = 1	 " +
					" AND ho2.fullName like :accountHolderPrincipalParam AND had2.indRepresentative = 1 ");
			sb.append("	AND ha.idHolderAccountPk IN ( " );	
			sb.append(stringSubQuery1.toString());			
			sb.append(") ");
			mapParameters.put("accountHolderPrincipalParam", "%" + holderAccountTO.getHolderPrincipal() + "%");
		}							   						      
        List<Object[]> objectListTemp = null;
        try{
        	objectListTemp =  findListByQueryString(sb.toString(), mapParameters);
        }catch(NoResultException ex){
        	return null;
        }
        
        return objectListTemp;
	}
	
	/**
	 * Gets the holder account service bean.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account service bean
	 * @throws ServiceException the service exception
	 */
	public HolderAccount getHolderAccount(HolderAccountTO holderAccountTO) throws ServiceException{
		return accountsFacade.getHolderAccountComponentServiceFacade(holderAccountTO);
	}

	/**
	 * Registry holder account from holder account request service bean.
	 *
	 * @param holderAcccounRequest the holder account request
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public HolderAccount createHolderAccountFromRequest(HolderAccountRequest holderAcccounRequest, Integer requestedAccountNumber, Integer stockCode) throws ServiceException{
		HolderAccount holderAccount = new HolderAccount();
		HolderAccountRequest holderAccountRequestHy = find(HolderAccountRequest.class, holderAcccounRequest.getIdHolderAccountReqPk());
		StringBuilder alternativeCode = new StringBuilder(holderAccountRequestHy.getParticipant().getIdParticipantPk().toString());	
		Participant objParticipant = find(Participant.class, holderAccountRequestHy.getParticipant().getIdParticipantPk());
		
//		Long accountNumberCorrelative = getNextCorrelativeOperations(GeneralConstants.STR_SQ_PARTICIPANT_PK + objParticipant.getMnemonic());
		
		/* Descomentar esto si se desea que se toma el codigo de cuenta titular que viene en el archivo
		if(Validations.validateIsNull(requestedAccountNumber)) {
			Integer accountNumberCorrelative  = getCorrelativeAccount(objParticipant.getIdParticipantPk());
			if(accountNumberCorrelative!= null)
				holderAccount.setAccountNumber(accountNumberCorrelative);
			else
				holderAccount.setAccountNumber(GeneralConstants.ONE_VALUE_INTEGER);
		} else {
			holderAccount.setAccountNumber(requestedAccountNumber);
		}*/	
		
		Integer accountNumberCorrelative  = getCorrelativeAccount(objParticipant.getIdParticipantPk());
		if(accountNumberCorrelative!= null)
			holderAccount.setAccountNumber(accountNumberCorrelative);
		else
			holderAccount.setAccountNumber(GeneralConstants.ONE_VALUE_INTEGER);
		
		//holderAccount.setAccountNumber(getCorrelativeNumberHolderAccountServiceBean(holderAccountRequestHy.getParticipant().getIdParticipantPk()));		
		Holder holderMain = null;
		List<HolderAccountDetail> holderAccountDetailList = new ArrayList<HolderAccountDetail>();
		alternativeCode.append("-(");
		for(HolderAccountDetRequest holderAccountReqDetHy: holderAccountRequestHy.getHolderAccountDetRequest()){
			HolderAccountDetail holderAccountDetail = new HolderAccountDetail();
			holderAccountDetail.setHolderAccount(holderAccount);
			holderAccountDetail.setHolder(holderAccountReqDetHy.getHolder());
			if(holderAccountRequestHy.getHolderAccountType().equals(HolderAccountType.OWNERSHIP.getCode())){
				holderAccountDetail.setIndRepresentative(holderAccountReqDetHy.getIndRepresentative());
				if(holderAccountReqDetHy.getIndRepresentative().equals(BooleanType.YES.getCode())){
					holderMain = holderAccountDetail.getHolder();
				}
			}else if(holderAccountRequestHy.getHolderAccountType().equals(HolderAccountType.SUCCESSION.getCode())){
				holderAccountDetail.setIndRepresentative(holderAccountReqDetHy.getIndRepresentative());
				if(holderAccountReqDetHy.getIndRepresentative().equals(BooleanType.YES.getCode())){
					holderMain = holderAccountDetail.getHolder();
				}
			}else if(holderAccountRequestHy.getHolderAccountType().equals(HolderAccountType.JURIDIC.getCode()) || holderAccountRequestHy.getHolderAccountType().equals(HolderAccountType.NATURAL.getCode())){
				holderAccountDetail.setIndRepresentative(BooleanType.YES.getCode());
				holderMain = holderAccountDetail.getHolder(); 
			}			
			holderAccountDetail.setRegistryDate(holderAccountRequestHy.getRegistryDate());
			holderAccountDetail.setRegistryUser(holderAccountRequestHy.getRegistryUser());
			holderAccountDetail.setStateAccountDetail(HolderAccountStateDetailType.REGISTERED.getCode());
			holderAccountDetailList.add(holderAccountDetail);			
			alternativeCode.append(holderAccountReqDetHy.getHolder().getIdHolderPk());
			if(holderAccountRequestHy.getHolderAccountDetRequest().indexOf(holderAccountReqDetHy)!=holderAccountRequestHy.getHolderAccountDetRequest().size()-1){
				alternativeCode.append(",");
			}
		}
		alternativeCode.append(")-");
		alternativeCode.append(holderAccount.getAccountNumber());	
		
		List<HolderAccountBank> holderAccountBankList = null;
		if(Validations.validateListIsNotNullAndNotEmpty(holderAccountRequestHy.getHolderAccountBankRequest())){
			holderAccountBankList = new ArrayList<HolderAccountBank>();
			for(HolderAccountBankRequest holderAccountReqBankHy: holderAccountRequestHy.getHolderAccountBankRequest()){
				HolderAccountBank holderAccountBank = new HolderAccountBank();
				holderAccountBank.setHolderAccount(holderAccount);
				holderAccountBank.setBank(holderAccountReqBankHy.getBank());
				holderAccountBank.setCurrency(holderAccountReqBankHy.getCurrency());
				holderAccountBank.setBankAccountType(holderAccountReqBankHy.getBankAccountType());
				holderAccountBank.setBankAccountNumber(holderAccountReqBankHy.getAccountNumber());
				holderAccountBank.setDescriptionBank(holderAccountReqBankHy.getDescriptionBank());	
				holderAccountBank.setHaveBic(0);
				holderAccountBank.setRegistryUser(holderAccountRequestHy.getRegistryUser());
				holderAccountBank.setRegistryDate(holderAccountRequestHy.getRegistryDate());
				holderAccountBank.setStateAccountBank(HolderAccountStateBankType.REGISTERED.getCode());
				holderAccountBank.setHolder(holderMain);
				holderAccountBank.setSwiftCode(holderAccountReqBankHy.getSwiftCode());
				holderAccountBankList.add(holderAccountBank);
			}
		}
			
		List<HolderAccountFile> holderAccountFileList = null;
		if(Validations.validateListIsNotNullAndNotEmpty(holderAccountRequestHy.getHolderAccountFilesRequest())){
			holderAccountFileList = new ArrayList<HolderAccountFile>();
			for(HolderAccountFileRequest holderAccountReqFileHy: holderAccountRequestHy.getHolderAccountFilesRequest()){
				HolderAccountFile holderAccountFile = new HolderAccountFile();
				holderAccountFile.setDocumentType(holderAccountReqFileHy.getDocumentType());
				holderAccountFile.setDocumentFile(holderAccountReqFileHy.getDocumentFile());
				holderAccountFile.setFilename(holderAccountReqFileHy.getFilename());
				holderAccountFile.setHolderAccount(holderAccount);
				holderAccountFile.setRegistryDate(new Date());
				holderAccountFile.setRegistryUser(holderAccountRequestHy.getRegistryUser());
				holderAccountFile.setIndMain(holderAccountReqFileHy.getIndMain());
				holderAccountFileList.add(holderAccountFile);
			}	
		}
			
		holderAccount.setRegistryDate(holderAccountRequestHy.getRegistryDate());
		holderAccount.setRegistryUser(holderAccountRequestHy.getRegistryUser());
		holderAccount.setParticipant(holderAccountRequestHy.getParticipant());
		holderAccount.setAccountType(holderAccountRequestHy.getHolderAccountType());
		holderAccount.setAccountGroup(holderAccountRequestHy.getHolderAccountGroup());
		holderAccount.setStateAccount(HolderAccountStatusType.ACTIVE.getCode());	
		holderAccount.setRelatedName(holderAccountRequestHy.getRelatedName());
		holderAccount.setHolderAccountDetails(holderAccountDetailList);
		holderAccount.setHolderAccountBanks(holderAccountBankList);
		holderAccount.setHolderAccountFiles(holderAccountFileList);
		holderAccount.setAlternateCode(alternativeCode.toString());
		holderAccount.setOpeningDate(CommonsUtilities.currentDate());
		holderAccount.setIndPdd(holderAccountRequestHy.getIndPdd());
		holderAccount.setDocumentPdd(holderAccountRequestHy.getDocumentPdd());
		holderAccount.setNameDocumentPdd(holderAccountRequestHy.getNameDocumentPdd());
				
		if(Validations.validateIsNotNull(stockCode)) {
			holderAccount.getStockCode();
		}
		
		create(holderAccount);
		return holderAccount;
	}
	
	/**
	 * Gets the exist issuer account type service bean.
	 *
	 * @param documentType the document type
	 * @param documentNumber the document number
	 * @return the exist issuer account type service bean
	 * @throws ServiceException the service exception
	 */
	public Integer getExistIssuerAccountTypeServiceBean(Integer documentType, String documentNumber) throws ServiceException {
		StringBuilder stringBuilderSql  = new StringBuilder();
		stringBuilderSql.append("SELECT count(*) FROM Issuer i WHERE i.documentType = :documentType and documentNumber = :documentNumber ");
		Query query = em.createQuery(stringBuilderSql.toString());
		query.setParameter("documentType", documentType);
		query.setParameter("documentNumber", documentNumber);
		return Integer.valueOf(query.getSingleResult().toString());
	}
	
	/**
	 * Gets the holder account req file.
	 *
	 * @param idFilePk the id file pk
	 * @return the holder account req file
	 * @throws ServiceException the service exception
	 */
	public byte[] getHolderAccountReqFile(Long idFilePk) throws ServiceException{
		String query = "Select har.documentFile FROM HolderAccountFileRequest har WHERE har.idHolderAccountReqFilePk = :idFilePkParam";
		Query queryJpql = em.createQuery(query);
		queryJpql.setParameter("idFilePkParam", idFilePk);
		try{
			return (byte[]) queryJpql.getSingleResult();
		}catch(NoResultException ex){
			return null;
		}
	}
	
	/**
	 * Gets the history state service.
	 *
	 * @param accountNumber and id_participant
	 * @param idParticipant the id participant
	 * @return the history state service
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccountRequest> getHolderAccountRequestHistoryStateServiceBean(Integer accountNumber,Long idParticipant) throws ServiceException{
		List<HolderAccountRequest> returnListHolderAccount = new ArrayList<HolderAccountRequest>();
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<Integer> lstRequestType = new ArrayList<Integer>();		
		lstRequestType.add(HolderAccountRequestHysType.CREATE.getCode());		
		lstRequestType.add(HolderAccountRequestHysType.BLOCK.getCode());
		lstRequestType.add(HolderAccountRequestHysType.UNBLOCK.getCode());
		lstRequestType.add(HolderAccountRequestHysType.CLOSE.getCode());		
		Integer holderAccountRequestState = HolderAccountRequestHysStatusType.CONFIRM.getCode();		
		sbQuery.append(" SELECT harh.registryDate, harh.requestType, "); 
		sbQuery.append(" (Select p.parameterName From ParameterTable p Where p.parameterTablePk = harh.requestType) "); // 4, 5
		sbQuery.append(" FROM HolderAccount ha join ha.holderAccountRequest harh");		
		sbQuery.append(" WHERE ha.idHolderAccountPk = harh.holderAccount.idHolderAccountPk");
		sbQuery.append(" AND ha.accountNumber = :accountNumber");
		sbQuery.append(" AND ha.participant.idParticipantPk = :idParticipant");
		sbQuery.append(" AND harh.requestType in (:lstRequestType)");
		sbQuery.append(" AND harh.requestState = :holderAccountRequestState");
		sbQuery.append(" ORDER BY harh.idHolderAccountReqPk asc");		   			  		
		parameters.put("accountNumber",accountNumber);
		parameters.put("idParticipant",idParticipant);
		parameters.put("lstRequestType",lstRequestType);
		parameters.put("holderAccountRequestState",holderAccountRequestState);	   		
		List<Object[]> HolderRequestList =  findListByQueryString(sbQuery.toString(), parameters);		
		for(Object[] object: HolderRequestList){			
			HolderAccountRequest holderAccountRequest = new HolderAccountRequest();									
			holderAccountRequest.setRegistryDate((Date) object[0]);							
			holderAccountRequest.setRequestType(new Integer(object[1].toString()));
			holderAccountRequest.setHolderAccountTypeDescription(object[2].toString());
			returnListHolderAccount.add(holderAccountRequest);
		}
		return returnListHolderAccount;
	}
	
	/**
	 * Gets the holder account file.
	 *
	 * @param idFilePk the id file pk
	 * @return the holder account file
	 * @throws ServiceException the service exception
	 */
	public byte[] getHolderAccountFile(Long idFilePk) throws ServiceException{
		String query = "Select har.documentFile FROM HolderAccountFile har WHERE har.idHolderAccountFilePk = :idFilePkParam";
		Query queryJpql = em.createQuery(query);
		queryJpql.setParameter("idFilePkParam", idFilePk);
		try{
			return (byte[]) queryJpql.getSingleResult();
		}catch(NoResultException ex){
			return null;
		}
	}
	
	/**
	 * Gets the correlative number holder account service bean.
	 *
	 * @param idParticipantPK the id participant pk
	 * @return the correlative number holder account service bean
	 * @throws ServiceException the service exception
	 */
	public synchronized Integer getCorrelativeNumberHolderAccountServiceBean(Long idParticipantPK) throws ServiceException{
		Query query = em.createQuery("SELECT MAX(ha.accountNumber) + 1 FROM HolderAccount ha WHERE ha.participant.idParticipantPk = :idParticipantPkParameter");
		query.setParameter("idParticipantPkParameter", idParticipantPK);
		Integer correlative = 0;
		try{
			correlative = (Integer) query.getSingleResult();
		}catch(Exception ex){
			correlative = null;
		}
		if(Validations.validateIsNull(correlative)){
			correlative = 1;
		}
		return correlative;
	}
	
	/**
	 * Gets the holder account list service bean.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account list service bean
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> getHolderAccountListServiceBean(HolderAccountTO holderAccountTO) throws ServiceException{
		return accountsFacade.getHolderAccountListComponentServiceFacade(holderAccountTO);
	}
	
	/**
	 * Gets the holder req det hies.
	 *
	 * @param holderAccountReqDetHies the holder account req det hies
	 * @return the holder req det hies
	 */
	public List<HolderAccountDetRequest> getHolderReqDetHies(List<HolderAccountDetRequest> holderAccountReqDetHies){
		Collections.sort(holderAccountReqDetHies, new Comparator<HolderAccountDetRequest>(){
			  
  			/**
  			 * Compare.
  			 *
  			 * @param s1 the s1
  			 * @param s2 the s2
  			 * @return the int
  			 */
  			public int compare(HolderAccountDetRequest s1, HolderAccountDetRequest s2) {
			    return s1.getHolder().getIdHolderPk().compareTo(s2.getHolder().getIdHolderPk());
			  }
			});
		return holderAccountReqDetHies;
	}
	 
	/**
	 * Have request with out confirmation service bean.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account request hy
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public HolderAccountRequest haveRequestWithOutConfirmationServiceBean(HolderAccountTO holderAccountTO) throws ServiceException{
		String query = "	SELECT holderAccountReq FROM HolderAccountRequest holderAccountReq " +
						"	WHERE holderAccountReq.holderAccount.accountNumber = :accountNumberParam AND " +
						"  	holderAccountReq.participant.idParticipantPk = :idParticipantParam AND" +
						"  	holderAccountReq.requestState IN (:stateRegisteredParam, :stateApproveParam)";
		Query queryJpa = em.createQuery(query);
		queryJpa.setParameter("stateRegisteredParam", HolderAccountRequestHysStatusType.REGISTERED.getCode());
		queryJpa.setParameter("stateApproveParam", HolderAccountRequestHysStatusType.APPROVE.getCode());
		queryJpa.setParameter("accountNumberParam", holderAccountTO.getHolderAccountNumber());
		queryJpa.setParameter("idParticipantParam", holderAccountTO.getParticipantTO());		
		List<HolderAccountRequest> holderAccountRequestHyList = null;
		try{
			holderAccountRequestHyList = ((List<HolderAccountRequest>)queryJpa.getResultList());
		}catch(NoResultException ex){
			return null;
		}
		if(holderAccountRequestHyList!=null && !holderAccountRequestHyList.isEmpty()){
			return holderAccountRequestHyList.get(0);
		}
		return null;
	}
	
	/**
	 * Holder account can will closed.
	 *
	 * @param holderAccount the holder account
	 * @return the boolean
	 */
	public Boolean holderAccountCanWillClosedServiceBean(HolderAccount holderAccount){
		if(holderAccount==null || holderAccount.getIdHolderAccountPk()==null){
			return Boolean.FALSE;
		}		
		StringBuilder queryValidate = new StringBuilder();
		queryValidate.append("SELECT COUNT(*) "+
							 "	FROM HolderAccountOperation hao "+
							 "	INNER JOIN hao.mechanismOperation mo "+
							 "	INNER JOIN mo.mechanisnModality mmo "+ 							
							 "	INNER JOIN mmo.negotiationModality negmo "+
							 "	WHERE hao.holderAccount.idHolderAccountPk = :idAccountPkParam AND mo.operationState "+ 
							 "	IN( "+
							 "	  :operationRegisteredParam "+
							 "	  ,:otcOperationConfirmedParam "+
							 "	  , (CASE negmo.indTermSettlement " +
							 "					WHEN 1 THEN :operationSettlementTermParam " +
							 "					ELSE :operationSettlementCashParam " +
							 "		END) "+
							 "	  ) ");				
		Query query = em.createQuery(queryValidate.toString());
		query.setParameter("operationRegisteredParam",MechanismOperationStateType.REGISTERED_STATE.getCode());
		query.setParameter("otcOperationConfirmedParam",MechanismOperationStateType.ASSIGNED_STATE.getCode());
		query.setParameter("operationSettlementCashParam",MechanismOperationStateType.CASH_SETTLED.getCode());
		query.setParameter("operationSettlementTermParam",MechanismOperationStateType.TERM_SETTLED.getCode());
		query.setParameter("idAccountPkParam",holderAccount.getIdHolderAccountPk());
		Long rowNumbers = 0L;
		try{
			rowNumbers = ((Long)query.getSingleResult()).longValue();
		}catch(NoResultException ex){
			rowNumbers = 0L;
		}catch(NullPointerException e){
			rowNumbers = 0L;
		}
		if(rowNumbers!=null && rowNumbers>0L){
			return Boolean.FALSE;
		}
		
		StringBuilder validateBalance = new StringBuilder();
		validateBalance.append("SELECT SUM(totalBalance) FROM HolderAccountBalance hab WHERE hab.holderAccount.idHolderAccountPk = :idAccountParam");
		Query queryBalance = em.createQuery(validateBalance.toString());
		queryBalance.setParameter("idAccountParam", holderAccount.getIdHolderAccountPk());
		Long totalBalance = 0L;
		try{
			totalBalance = ((BigDecimal)queryBalance.getSingleResult()).longValue();
		}catch(NoResultException ex){
			totalBalance = 0L;
		}catch(NullPointerException e){
			totalBalance = 0L;
		}
		if(totalBalance!=null && totalBalance>0L){
			return Boolean.FALSE;
		}		
		return Boolean.TRUE;
	}
	
	/**
	 * Find participant.
	 *
	 * @param documentType the document type
	 * @param documentNumber the document number
	 * @return the participant
	 * @throws ServiceException the service exception
	 */
	public Participant findParticipantByDocument(Integer documentType, String documentNumber) throws ServiceException{
		StringBuilder builderSql = new StringBuilder();
		builderSql.append("Select new com.pradera.model.accounts.Participant(p.idParticipantPk, p.description, p.mnemonic) ");
		builderSql.append("From Participant p ");
		builderSql.append("Where p.documentType = :documentType, And p.documentNumber = :documentNumber ");		
		Query query = em.createNamedQuery(builderSql.toString());
		query.setParameter("documentType", documentType);
		query.setParameter("documentNumber", documentNumber);		
		Participant participant = null;
		try{
			participant = (Participant) query.getSingleResult(); 
		}catch(NoResultException ex){
			ex.printStackTrace();
		}
		return participant;
	}
		
	/**
	 * Gets the balance holder from security.
	 *
	 * @param filter the filter
	 * @return the balance holder from security
	 */
	/*
	 *	Created by Martin Zarate Rafael 
	 *  Gets Balance from Holder Account
	 */
	public HolderAccountBalance getBalanceHolderFromSecurity(HolderAccountTO filter){
		StringBuilder jpqlQuery = new StringBuilder(" select * from HolderAccountBalance hab ");
		jpqlQuery.append(" where hab.participant.idParticipantPk="+filter.getParticipantTO())
				 .append(" and hab.holderAccount.accountNumber="+filter.getHolderAccountNumber())
				 .append(" and hab.security.idIsinCodePk="+filter.getIdIsinCodePk());
		Query query = em.createQuery(jpqlQuery.toString());
		return (HolderAccountBalance)query.getSingleResult();
	}
	
	/**
	 * Gets the holder account movements.
	 *
	 * @param filter the filter
	 * @return the holder account movements
	 */
	/*
	 *	Created by Martin Zarate Rafael 
	 *  Gets Movements from Security
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccountMovement> getHolderAccountMovements(HolderAccountTO filter){
		StringBuilder jpqlQuery = new StringBuilder(" select * from HolderAccountMovement ham ");
		jpqlQuery.append(" where hab.participant.idParticipantPk="+filter.getParticipantTO())
				 .append(" and hab.holderAccount.accountNumber="+filter.getHolderAccountNumber())
				 .append(" and hab.security.idIsinCodePk="+filter.getIdIsinCodePk());
		Query query = em.createQuery(jpqlQuery.toString());
		return (List<HolderAccountMovement>)query.getResultList();
	}	
	
	
	/**
	 * Gets the id holder account.
	 *
	 * @param idParticipant the id participant
	 * @param accountNumber the account number
	 * @param accountType the account type
	 * @return the id holder account
	 */
	public HolderAccount getIdHolderAccount(Long idParticipant, Integer accountNumber, Integer accountType){
		StringBuilder jpqlQuery = new StringBuilder();
		jpqlQuery.append(" select ha.idHolderAccountPk from HolderAccount ha ");
		jpqlQuery.append(" where ha.participant.idParticipantPk= :idParticipant ");
		jpqlQuery.append(" and ha.accountNumber = :accountNumber ");
		if(Validations.validateIsNotNull(accountType))
			jpqlQuery.append(" and ha.accountType = :accountType ");
		Query query = em.createQuery(jpqlQuery.toString());
		query.setParameter("idParticipant", idParticipant);
		query.setParameter("accountNumber", accountNumber);
		if(Validations.validateIsNotNull(accountType))
			query.setParameter("accountType", accountType);
		try {
			HolderAccount res = new HolderAccount();
			res.setIdHolderAccountPk((Long) query.getSingleResult());
			
			return res;
		} catch (NoResultException e) {
			return null;
		}
	}
	
	
	/**
	 * Find holder accounts for validations aux.
	 *
	 * @param holderAccountTO the holder account to
	 * @param lstHolders the lst holders
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> findHolderAccountsForValidationsAux(HolderAccountTO holderAccountTO, List<Long> lstHolders){
		Map<String,Object> mapParameters = new HashMap<String,Object>();
		StringBuilder sb = new StringBuilder();
		sb.append("	SELECT ha, ");		
		sb.append("	(SELECT count(hadaux.idHolderAccountDetailPk) FROM HolderAccountDetail hadaux WHERE hadaux.holderAccount.idHolderAccountPk = ha.idHolderAccountPk)  " );
		sb.append("	FROM 	HolderAccount ha ");
		sb.append(" JOIN 	ha.holderAccountDetails had ");
		sb.append("	JOIN 	had.holder	ho " );
		sb.append("	Where 1 = 1  ");
		sb.append("	AND ha.stateAccount = :accountStateParam " );
		mapParameters.put("accountStateParam", HolderAccountStatusType.ACTIVE.getCode());
		if(holderAccountTO.getParticipantTO() != null){
			sb.append("	AND	ha.participant.idParticipantPk = :participantParam " );
			mapParameters.put("participantParam", holderAccountTO.getParticipantTO());
		}
		if(holderAccountTO.getHolderAccountType() != null){
			sb.append("	AND ha.accountType = :accountTypeParam " );
			mapParameters.put("accountTypeParam", holderAccountTO.getHolderAccountType());
		}	
		if(holderAccountTO.getHolderAccountNumber() != null){
			sb.append("	AND ha.accountNumber = :accountNumberParam " );
			mapParameters.put("accountNumberParam", holderAccountTO.getHolderAccountNumber());
		}
		if(lstHolders != null){
			sb.append("	AND had.holder.idHolderPk in (:lstHolders) " );
			mapParameters.put("lstHolders", lstHolders);
		}									   						      
        List<Object[]> objectListTemp = null;
        try {
        	objectListTemp =  findListByQueryString(sb.toString(), mapParameters);
        	
        	 Map<Long, Object[]> resMap = new LinkedHashMap<>();

    	    if (objectListTemp != null && !objectListTemp.isEmpty()) {
    	        for (Object[] obj : objectListTemp) {
    	            HolderAccount ha = (HolderAccount) obj[0];
    	            resMap.computeIfAbsent(ha.getIdHolderAccountPk(), k -> obj);
    	        }

    	        return new ArrayList<>(resMap.values());
    	    }   
        } catch(NoResultException ex){
        	return null;
        }        
        return objectListTemp;
	}

	public  Integer getCorrelativeAccount(Long idParticipantPK) {
		Query query = em.createQuery("SELECT MAX(ha.accountNumber) + 1 FROM HolderAccount ha WHERE ha.participant.idParticipantPk = :idParticipantPK");
		query.setParameter("idParticipantPK", idParticipantPK);
		try{
			return (Integer) query.getSingleResult();
		}catch (NoResultException e) {
			return 1;
		}
		
	}
	
	public  Integer getCorrelativeAccountbyIssuer(Long idParticipantPK) {
		Query query = em.createQuery("SELECT MAX(ha.accountNumber) + 1 FROM HolderAccount ha WHERE ha.participant.idParticipantPk = :idParticipantPK");
		query.setParameter("idParticipantPK", idParticipantPK);
		try{
			return (Integer) query.getSingleResult();
		}catch (NoResultException e) {
			return 1;
		}
		
	}
	
}