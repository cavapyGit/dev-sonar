/*
 * 
 */
package com.pradera.accounts.holderaccount.view;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.omnifaces.util.Faces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.accounts.holder.facade.HolderServiceFacade;
import com.pradera.accounts.holderaccount.facade.HolderAccountModifyFacade;
import com.pradera.accounts.participants.facade.ParticipantServiceFacade;
import com.pradera.accounts.participants.to.MotiveController;
import com.pradera.accounts.util.view.PropertiesConstants;
import com.pradera.accounts.webservices.ServiceAPIClients;
import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.sessionuser.OperationUserTO;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.BackDialogController;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountBank;
import com.pradera.model.accounts.holderaccounts.HolderAccountBankRequest;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetRequest;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.HolderAccountFile;
import com.pradera.model.accounts.holderaccounts.HolderAccountFileRequest;
import com.pradera.model.accounts.holderaccounts.HolderAccountRequest;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestHysStatusType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestHysType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStateBankType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.report.ReportLoggerFile;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class HolderAccountMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/02/2013
 */
@DepositaryWebBean
public class HolderAccountModifyMgmtBean extends GenericBaseBean implements Serializable{
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The country residence. */
	@Inject @Configurable Integer countryResidence;

	/** The holder account session. */
	private HolderAccount holderAccountSession;
	
	/** The holder account request session. */
	private HolderAccountRequest holderAccountRequestHySession;
	
	/** The holder account to. */
	private HolderAccountTO holderAccountTO;
	
	/** The participant list. */
	private List<Participant> participantList;
	
	/** The Constant TAMANIO_BANK_ACCOUNTS. */
	private static Integer TAMANIO_BANK_ACCOUNTS = 2;
	
	/** The holder account request bank request. */
	private HolderAccountBankRequest holderAccountReqBankHyRequest;
	
	/** The holder request. */
	private Holder holderAccountReqDetHyRequest;
	
	/** The bank list. */
	private List<Bank> bankList;
	
	/** The holder account request data model. */
	private GenericDataModel<HolderAccountRequest> holderAccountRequestHyDataModel;
	
	/** The parameter table money type list. */
	private List<ParameterTable> parameterTableMoneyTypeList;
	
	/** The show group account. */
	private boolean showGroupAccount;
	
	/** The motive controller. */
	private MotiveController motiveController;
	
	/** The Constant ERROR_REJECT. */
	private static final long ERROR_REJECT = 482;
	
	/** The Constant ERROR_ANULATE. */
	private static final long ERROR_ANULATE = 486;
	
	/** The holder account request before save. */
	private StringBuilder holderAccountRequestBeforeSave;
	
	/** The parameter table documents list. */
	private List<ParameterTable> parameterTableDocumentsList;
	
	/** The holder account service facade. */
	@EJB
	HolderAccountModifyFacade holderAccountServiceFacade;
	
	/** The accounts facade. */
	@EJB
	ParticipantServiceFacade participantServiceFacade;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	/** The accounts facade. */
	@EJB
	private AccountsFacade accountsFacade;
	
	/** The back dialog controller. */
	private BackDialogController backDialogController;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;
	
	/** The current date. */
	private Date currentDate = new Date();
	
	/** The id bcr pk. */
	private static Long ID_BCR_PK = 23L;

	/** The parameter table accounts type list. */
	private List<ParameterTable> parameterTableAccoutsTypeList;
	
	/** The parameter table account group list. */
	private List<ParameterTable> parameterTableAccountGroupList;
	
	/** The parameter table status list. */
	private List<ParameterTable> parameterTableAccountsTypeBankList;
	
	/** The parameter table status list. */
	private List<ParameterTable> parameterTableStatusList;
	
	/** The parameter table to. */
	private ParameterTableTO parameterTableTO;
	
	/** The attach file holder account selected. */
	private Integer attachFileHolderAccountSelected;
	
	/** The Account holder file name display. */
	private String accountHolderFileNameDisplay;
	
	/** The holder account detail temp. */
	private List<HolderAccountDetRequest> holderAccountDetailTemp;
	
	/** The holder account file request temp. */
	private List<HolderAccountFileRequest > holderAccountFileRequestTemp;
	
	/** The holder account bank request temp. */
	private List<HolderAccountBankRequest > holderAccountBankRequestTemp;
	
	/** The holder account request detail data model old. */
	private GenericDataModel<HolderAccountDetRequest> holderAccountDetRequestOld;
	
	/** The holder account request detail data model new. */
	private GenericDataModel<HolderAccountDetRequest> holderAccountDetRequestNew;
	
	/** The holder account request bank data model old. */
	private GenericDataModel<HolderAccountBankRequest> holderAccountBankRequestOld;
	
	/** The holder account request bank data model new. */
	private GenericDataModel<HolderAccountBankRequest> holderAccountBankRequestNew;
	
	/** The holder account request file data model old. */
	private GenericDataModel<HolderAccountFileRequest> holderAccountFileRequestOld;
	
	/** The holder account request file data model new. */
	private GenericDataModel<HolderAccountFileRequest> holderAccountFileRequestNew;
	
	/** The Constant SEARCH_ACTION_ACCOUNT_MODIFY. */
	private static final String SEARCH_ACTION_ACCOUNT_MODIFY = "searchHolderAccountModify";
	
	/** The holder request. */
	private Holder holderRequest;
	
	/** The holder service facade. */
	@EJB
	HolderServiceFacade holderServiceFacade;
	
	@EJB
	ServiceAPIClients serviceAPIClients;
	
	/** The upload file size mgmt. */
	private String fUploadFileSizeMgmt="3000000";
	
	/** The upload file size display mgmt. */
	private String fUploadFileSizeDisplayMgmt="3000";

	/** The file download. */
	private transient StreamedContent fileDownload; 
	
	/** The holder accounts. */
	private List<HolderAccount> holderAccounts;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	/** The flag exist participant dpf. */
	private boolean flagExistParticipantDPF;
	
	private String pdfFileUploadFiltTypes="doc|docx|xls|xlsx|pdf";
	
	private String documentFileName;
	
	/**
	 * Instantiates a new holder account modify mgmt bean.
	 */
	public HolderAccountModifyMgmtBean() {
		super();
		holderAccountTO = new HolderAccountTO();
		holderAccountTO.setInitialDate(new Date());
		holderAccountTO.setFinalDate(new Date());
		holderAccountTO.setNeedHolder(Boolean.TRUE);
		holderAccountTO.setNeedParticipant(Boolean.FALSE);
	}
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		beginConversation();
		this.setHolderAccountRequestHySession(new HolderAccountRequest());
		holderAccountRequestHySession.setHolderAccount(new HolderAccount());
		motiveController = new MotiveController();
		holderAccountRequestHySession.setHolderAccountDetRequest(new ArrayList<HolderAccountDetRequest>());
		holderAccountRequestHySession.setHolderAccountBankRequest(new ArrayList<HolderAccountBankRequest>());
		holderAccountRequestHySession.setHolderAccountFilesRequest(new ArrayList<HolderAccountFileRequest>());
		Participant participantTO = new Participant();
		parameterTableTO = new ParameterTableTO();
		participantTO.setState(ParticipantStateType.REGISTERED.getCode());
		holderAccountReqBankHyRequest = new HolderAccountBankRequest();
		holderAccountReqBankHyRequest.setBank(new Bank());
		parameterTableTO.setState(1);
		holderRequest = new Holder();
		try {
			this.setParameterTableMoneyTypeList(generalParametersFacade.getListParameterTableServiceBean(parameterTableTO.setParaterTablePkFromMaster(MasterTableType.CURRENCY)));
			TAMANIO_BANK_ACCOUNTS = parameterTableMoneyTypeList.size(); 
			this.setParticipantList(accountsFacade.getLisParticipantServiceBean(participantTO));
			this.setBankList(holderAccountServiceFacade.getBankListServiceFacade());			
			this.getMotiveController().setMotiveList(generalParametersFacade.getListParameterTableServiceBean(parameterTableTO.setParaterTablePkFromMaster(MasterTableType.HOLDER_ACCOUNT_REQUEST_ANNULAR_MOTIVE)),MasterTableType.HOLDER_ACCOUNT_REQUEST_ANNULAR_MOTIVE);
			this.getMotiveController().setMotiveList(generalParametersFacade.getListParameterTableServiceBean(parameterTableTO.setParaterTablePkFromMaster(MasterTableType.HOLDER_ACCOUNT_REQUEST_REJECT_MOTIVE)),MasterTableType.HOLDER_REQUEST_REJECT_MOTIVE);
			loadCboDocumentsList();
			
			parameterTableTO.setMasterTableFk(MasterTableType.ACCOUNTS_TYPE.getCode());			
			this.setParameterTableAccoutsTypeList(generalParametersFacade.getListParameterTableServiceBean(this.getParameterTableTO()));			
			parameterTableTO.setMasterTableFk(MasterTableType.ACCOUNT_GROUP_TYPE.getCode());			
			this.setParameterTableAccountGroupList(generalParametersFacade.getListParameterTableServiceBean(this.getParameterTableTO()));
			parameterTableTO.setMasterTableFk(MasterTableType.MASTER_TABLE_BANK_ACCOUNT_TYPE.getCode());			
			this.setParameterTableAccountsTypeBankList(generalParametersFacade.getListParameterTableServiceBean(this.getParameterTableTO()));
			parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_REQUEST_STATUS.getCode());			
			this.setParameterTableStatusList(generalParametersFacade.getListParameterTableServiceBean(this.getParameterTableTO()));
			
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		loadPrivilegesUser();
		evaluateUserSession();
		evaluateUserInfoActive();
		loadHolidays();
		accountHolderFileNameDisplay=GeneralConstants.EMPTY_STRING;	
		holderAccountDetailTemp = new ArrayList<HolderAccountDetRequest>(); 
		holderAccountFileRequestTemp = new ArrayList<HolderAccountFileRequest>();
		holderAccountBankRequestTemp = new ArrayList<HolderAccountBankRequest>();
	}
	
	/**
	 * Load holidays.
	 */
	private void loadHolidays() {
		// TODO Auto-generated method stub
		 Holiday holidayFilter = new Holiday();
		 holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		 GeographicLocation countryFilter = new GeographicLocation();
		 countryFilter.setIdGeographicLocationPk(countryResidence);
		 holidayFilter.setGeographicLocation(countryFilter);
		 try {
			allHolidays = CommonsUtilities.convertListDateToString(generalParametersFacade.getListHolidayDateServiceFacade(holidayFilter));
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Load privileges user.
	 */
	public void loadPrivilegesUser() {
		// TODO Auto-generated method stub
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnApproveView(Boolean.TRUE);
		privilegeComponent.setBtnAnnularView(Boolean.TRUE);
		privilegeComponent.setBtnConfirmView(Boolean.TRUE);
		privilegeComponent.setBtnRejectView(Boolean.TRUE);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
		userPrivilege.getUserAcctions();
	}
	
	/**
	 * Evaluate user session.
	 */
	private void evaluateUserSession() {
		// TODO Auto-generated method stub
		if(userInfo.getUserAccountSession().isParticipantInstitucion() || 
				userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
			Long participantCode = userInfo.getUserAccountSession().getParticipantCode();
			holderAccountRequestHySession.setParticipant(searchParticipantOnListFromPK(participantCode));
			holderAccountTO.setParticipantTO(participantCode);
		}else if (userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
			Long participantDpf = getIssuerDpfInstitution(userInfo);
			if(Validations.validateIsNotNullAndNotEmpty(participantDpf)){
				flagExistParticipantDPF = true;
				holderAccountRequestHySession.setParticipant(new Participant(participantDpf));
				holderAccountTO.setParticipantTO(participantDpf);
			}else{
				flagExistParticipantDPF = false;
				holderAccountTO.setParticipantTO(null);
			}
		}else if(userInfo.getUserAccountSession().isDepositaryInstitution()){
			cleanHolderAccountTO();
		}
	}

	/**
	 * Load select documents list.
	 */
	public void loadCboDocumentsList(){
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_FILES.getCode());
		try {
			parameterTableDocumentsList = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Execute back page hanlder.
	 */
	public void executeBackPageHanlder(){
		if(holderAccountRequestHyDataModel==null || holderAccountRequestHyDataModel.getSize()==0){
			evaluateUserSession();
		}
	}
	
	/**
	 * Removes the holder on data table handler.
	 *
	 * @param holderAccountReqDetHys the holder account req det hys
	 */
	public void removeHolderOnDataTableHandler(HolderAccountDetRequest holderAccountReqDetHys){
		holderAccountRequestHySession.getHolderAccountDetRequest().remove(holderAccountReqDetHys);
		cleanHolderAccountReqBankHyRequest();
	}
	
	/**
	 * Before clean holder account request.
	 */
	public void beforeCleanHolderAccountRequest(){
		executeAction();
		JSFUtilities.hideGeneralDialogues();
		hideLocalComponents();
		if(this.getBackDialogController().getObjectBefore().toString().equals(this.getHolderAccountRequestHySession().toString())){
			cleanHolderAccountRequest();
		}else{
			showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, "lbl.clean.confirm", null);
			JSFUtilities.showComponent(":frmHolderAccountMgmt:cnfCleanHolderAccountMgmt");
		}
	}
	
	/**
	 * Clean holder account request bank request.
	 */
	public void cleanHolderAccountReqBankHyRequest(){
		hideLocalComponents();
		if(JSFUtilities.findViewComponent("frmHolderAccountMgmt:flsBanksAccounts")!=null){
			JSFUtilities.resetComponent("frmHolderAccountMgmt:flsBanksAccounts");
		}
		this.getHolderAccountReqBankHyRequest().setBank(searchBankOnListFromPK(0L));
		this.getHolderAccountReqBankHyRequest().setBankAccountType(null);
		this.getHolderAccountReqBankHyRequest().setCurrency(null);
		this.getHolderAccountReqBankHyRequest().setAccountNumber(null);
		this.getHolderAccountReqBankHyRequest().setFlagForeignAccount(false);
		this.getHolderAccountReqBankHyRequest().setSwiftCode(null);
	}
	
	/**
	 * Clean holder account request.
	 */
	public void cleanHolderAccountRequest(){
		hideLocalComponents();
		this.getHolderAccountRequestHySession().setHolderAccount(new HolderAccount());
		this.getHolderAccountRequestHySession().getHolderAccount().setParticipant(new Participant());
		this.getHolderAccountRequestHySession().setHolderAccountDetRequest(new ArrayList<HolderAccountDetRequest>());
		this.getHolderAccountRequestHySession().setHolderAccountBankRequest(new ArrayList<HolderAccountBankRequest>());
		this.getHolderAccountRequestHySession().setHolderAccountFilesRequest(new ArrayList<HolderAccountFileRequest>());
		this.holderAccountSession = new HolderAccount();
		evaluateUserSession();
		this.setHolderAccountDetailTemp(new ArrayList<HolderAccountDetRequest>());
		this.setHolderAccountBankRequestTemp(new ArrayList<HolderAccountBankRequest>());
		this.setHolderAccountFileRequestTemp(new ArrayList<HolderAccountFileRequest>());
		this.setHolderAccountDetRequestNew(null);
		this.setHolderAccountDetRequestOld(null);
		this.setHolderAccountBankRequestNew(null);
		this.setHolderAccountBankRequestOld(null);
		this.setHolderAccountFileRequestNew(null);
		this.setHolderAccountFileRequestOld(null);
		holderRequest = new Holder();
	}
	
	/**
	 * Clean holder account entered.
	 */
	public void cleanHolderAccountEntered(){
		JSFUtilities.resetComponent(":frmHolderAccountMgmt:pnlData");
		this.getHolderAccountRequestHySession().setHolderAccount(new HolderAccount());
		this.getHolderAccountRequestHySession().getHolderAccount().setParticipant(new Participant());
		this.getHolderAccountRequestHySession().setHolderAccountBankRequest(new ArrayList<HolderAccountBankRequest>());
		this.getHolderAccountRequestHySession().setHolderAccountFilesRequest(new ArrayList<HolderAccountFileRequest>());			
		this.holderAccountSession = new HolderAccount();
	}
	
	/**
	 * Registry holder account modify request handler.
	 *
	 * @return the string
	 */
	public String registryHolderAccountModifyRequestHandler(){
		if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
			//SI NO TIENE PARTICIPANTE Y ES EMISOR DPF NO PUEDE REGISTRAR
			if(!flagExistParticipantDPF){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_PARTICIPANT));
				JSFUtilities.showSimpleValidationDialog();
				return "";
			}
		}
		holderAccountTO.setParticipantTO(null);
		holderAccountRequestHySession = new HolderAccountRequest();
		holderAccountRequestHySession.setHolderAccountDetRequest(new ArrayList<HolderAccountDetRequest>());
		holderAccountRequestHySession.setHolderAccountBankRequest(new ArrayList<HolderAccountBankRequest>());
		holderAccountRequestHySession.setHolderAccountFilesRequest(new ArrayList<HolderAccountFileRequest>());
		holderAccountRequestHySession.setParticipant(searchParticipantOnListFromPK(holderAccountTO.getParticipantTO()));
		holderAccounts =null;
		
		this.holderAccountSession = new HolderAccount();
		cleanHolderAccountRequest();
		cleanHolderAccountReqBankHyRequest();
		evaluateUserSession();
		backDialogController = new BackDialogController();
		backDialogController.setObjectBefore(new StringBuilder(this.getHolderAccountRequestHySession().toString()));
		return "holderAccountModifyMgmRule";
	}
	
	/**
	 * Asignate holder main.
	 *
	 * @param holderAccountDetailParam the holder account detail param
	 */
	public void asignateHolderMain(HolderAccountDetail holderAccountDetailParam){
		for(HolderAccountDetail holderAccountDetail: holderAccountRequestHySession.getHolderAccount().getHolderAccountDetails()){
			if(holderAccountDetailParam.getHolder().getIdHolderPk().equals(holderAccountDetail.getHolder().getIdHolderPk())){
				holderAccountDetailParam.setIndRepresentative(BooleanType.YES.getCode());
			}
		}
		
	}
	
	/**
	 * Deasignate holder main.
	 *
	 * @param holderAccountDetailParam the holder account detail param
	 */
	public void deasignateHolderMain(HolderAccountDetail holderAccountDetailParam){
		for(HolderAccountDetail holderAccountDetail: holderAccountRequestHySession.getHolderAccount().getHolderAccountDetails()){
			if(holderAccountDetailParam.getHolder().getIdHolderPk().equals(holderAccountDetail.getHolder().getIdHolderPk())){
				holderAccountDetailParam.setIndRepresentative(BooleanType.NO.getCode());
			}
		}
		
	}
	
	/**
	 * Search holder account hanlder.
	 */
	public void searchHolderAccountModifyHanlder(){
		JSFUtilities.hideGeneralDialogues();
		hideLocalComponents();
		executeAction();
		this.setHolderAccountDetailTemp(new ArrayList<HolderAccountDetRequest>());
		this.setHolderAccountBankRequestTemp(new ArrayList<HolderAccountBankRequest>());
		this.setHolderAccountFileRequestTemp(new ArrayList<HolderAccountFileRequest>());
		this.getHolderAccountRequestHySession().setHolderAccountBankRequest(new ArrayList<HolderAccountBankRequest>());
		this.getHolderAccountRequestHySession().setHolderAccountFilesRequest(new ArrayList<HolderAccountFileRequest>());
		if(holderAccountSession.getAccountNumber()!= null && Validations.validateIsPositiveNumber(holderAccountSession.getAccountNumber())){
			if(Validations.validateIsNull(this.getHolderAccountTO().getParticipantTO())){
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_PARTICIPANT,null);
				JSFUtilities.showSimpleValidationDialog();
				cleanHolderAccountEntered();
				return; 
			}
			holderAccountTO.setNeedHolder(Boolean.TRUE);
			holderAccountTO.setNeedFiles(Boolean.TRUE);
			holderAccountTO.setNeedBanks(Boolean.TRUE);
			holderAccountTO.setHolderAccountNumber(holderAccountSession.getAccountNumber());
			
			//Validate if exist some request not Approve And Not Confirm
			HolderAccountRequest holderAccountRequest = null;
			try {
				holderAccountRequest = holderAccountServiceFacade.haveRequestWithOutConfirmationServiceFacade(holderAccountTO);
			} catch (ServiceException e1) {
				e1.printStackTrace();
			}
			if(holderAccountRequest!=null){
				String action=null;
				if(HolderAccountRequestHysType.CREATE.getCode().equals(holderAccountRequest.getRequestType())){
					action=PropertiesUtilities.getMessage("lbl.action.create").toLowerCase();
				}
				if(HolderAccountRequestHysType.MODIFY.getCode().equals(holderAccountRequest.getRequestType())){
					action=PropertiesUtilities.getMessage("lbl.action.modify").toLowerCase();
				}
				if(HolderAccountRequestHysType.BLOCK.getCode().equals(holderAccountRequest.getRequestType())){
					action=PropertiesUtilities.getMessage("lbl.action.block").toLowerCase();
				}
				if(HolderAccountRequestHysType.UNBLOCK.getCode().equals(holderAccountRequest.getRequestType())){
					action=PropertiesUtilities.getMessage("lbl.action.unblock").toLowerCase();
				}
				if(HolderAccountRequestHysType.CLOSE.getCode().equals(holderAccountRequest.getRequestType())){
					action=PropertiesUtilities.getMessage("lbl.action.close").toLowerCase();
				}
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_REQUEST_EXIST,
						new Object[]{action,holderAccountTO.getHolderAccountNumber()});
				
				JSFUtilities.showSimpleValidationDialog();
				cleanHolderAccountEntered();
				holderAccountSession = new HolderAccount();
				return;
			}
			//END -- Validate if exist some request not Approve And Not Confirm
			
			HolderAccount holderAccountFinded = null;
			try {
				holderAccountFinded = holderAccountServiceFacade.getHolderAccountServiceFacade(holderAccountTO);
				
				List<HolderAccountFile> listAccountFile = new ArrayList<>();
				for (HolderAccountFile file : holderAccountFinded.getHolderAccountFiles()){
					file.setStateDocumentFile(HolderAccountStateBankType.REGISTERED.getCode());
					listAccountFile.add(file);
				}
				holderAccountFinded.setHolderAccountFiles(new ArrayList<HolderAccountFile>());
				holderAccountFinded.setHolderAccountFiles(listAccountFile);
				
				holderAccountRequestHySession.setHolderAccount(holderAccountFinded);																						
				if(Validations.validateIsNull(holderAccountFinded)){
					showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_NUMBER,
										new String[]{String.valueOf(holderAccountTO.getHolderAccountNumber()),this.searchParticipantOnListFromPK(holderAccountTO.getParticipantTO()).getMnemonic()});
					JSFUtilities.showSimpleValidationDialog();
					cleanHolderAccountEntered();
					return;
				}
				
				if(Validations.validateIsNull(holderAccountFinded.getIndPdd()) || holderAccountFinded.getIndPdd().equals(BooleanType.NO.getCode())){
					holderAccountRequestHySession.setBlPdd(Boolean.FALSE);
					holderAccountRequestHySession.setIndPddOriginal(BooleanType.NO.getCode());
				} else {
					holderAccountRequestHySession.setBlPdd(Boolean.TRUE);
					holderAccountRequestHySession.setIndPddOriginal(BooleanType.YES.getCode());
					holderAccountRequestHySession.setDocumentPdd(holderAccountFinded.getDocumentPdd());
					holderAccountRequestHySession.setDocumentPddOriginal(holderAccountFinded.getDocumentPdd());
					holderAccountRequestHySession.setNameDocumentPdd(holderAccountFinded.getNameDocumentPdd());
					holderAccountRequestHySession.setNameDocumentPddOriginal(holderAccountFinded.getNameDocumentPdd());
					this.setDocumentFileName(holderAccountFinded.getNameDocumentPdd());
				}
				holderAccountRequestHySession.getHolderAccount().setParticipant(this.searchParticipantOnListFromPK(holderAccountTO.getParticipantTO()));
				holderAccountRequestHySession.setRelatedNameOriginal(holderAccountFinded.getRelatedName());
				
				if(Validations.validateListIsNotNullAndNotEmpty(holderAccountRequestHySession.getHolderAccount().getHolderAccountBanks())) {
					holderAccountRequestHySession.getHolderAccount().getHolderAccountBanks()
						.forEach(hab -> hab.setOriginalStateAccountBank(hab.getStateAccountBank()));
				}
				
				this.setHolderAccountRequestBeforeSave(new StringBuilder(this.getHolderAccountRequestHySession().toString()));
			} catch (ServiceException e) {
				e.printStackTrace();
			}
			if(!(this.getHolderAccountRequestHySession().getHolderAccount().getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode()))){
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_HOLDERACCOUNT,
									new String[]{String.valueOf(holderAccountTO.getHolderAccountNumber()),HolderAccountStatusType.lookup.get(this.getHolderAccountRequestHySession().getHolderAccount().getStateAccount()).getValue()});
				JSFUtilities.showSimpleValidationDialog();
				//alert(PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_HOLDERACCOUNT));
				cleanHolderAccountEntered();
				return;
			}
			if(!holderAccountFinded.validateIfRntIsRegistered()){
				showMessageOnDialogOnlyKey(PropertiesConstants.MESSAGES_ALERT, PropertiesConstants.HOLDERACCOUNT_VALIDATION_HOLDER_BLOCK);
				JSFUtilities.showSimpleValidationDialog();
				cleanHolderAccountEntered();
				return;
			}
			
			JSFUtilities.hideGeneralDialogues();
			if(holderAccountFinded.getAccountType().equals(HolderAccountType.OWNERSHIP.getCode()) ||
					holderAccountFinded.getAccountType().equals(HolderAccountType.SUCCESSION.getCode())){
				for(HolderAccountDetail holderAccountDetail: holderAccountFinded.getHolderAccountDetails()){
					HolderAccountDetRequest holderAccountReqDetHy = new HolderAccountDetRequest();
					holderAccountReqDetHy.setHolder(holderAccountDetail.getHolder());
					holderAccountReqDetHy.setIndRepresentative(holderAccountDetail.getIndRepresentative());
					holderAccountReqDetHy.setRegistryDate(holderAccountDetail.getRegistryDate());
					holderAccountReqDetHy.setRegistryUser(holderAccountDetail.getRegistryUser());			
					holderAccountReqDetHy.setHolderAccountRequest(this.getHolderAccountRequestHySession());
					holderAccountReqDetHy.setIndOldNew(BooleanType.NO.getCode());
					holderAccountDetailTemp.add(holderAccountReqDetHy);
				}
			}
		}else{
			cleanHolderAccountEntered();
		}
	}
	
	/**
	 * Evaluate back.
	 *
	 * @return the string
	 */
	public String evaluateBack(){
		holderAccountRequestHySession.setParticipant(searchParticipantOnListFromPK(holderAccountTO.getParticipantTO()));
		hideLocalComponents();
		this.getBackDialogController().setObjectAfter(new StringBuilder(this.getHolderAccountRequestHySession().toString()));
		if(this.getBackDialogController().isShowModal()){
			JSFUtilities.showComponent(":frmHolderAccountMgmt:cnfBackAdvancedAccount");
			return GeneralConstants.EMPTY_STRING;
		}
		if(holderAccountRequestHyDataModel==null || holderAccountRequestHyDataModel.getSize()==0){
			evaluateUserSession();
		}
		return SEARCH_ACTION_ACCOUNT_MODIFY;
	}
	
	/**
	 * Adds the account on data table handler.
	 */
	public void addAccountOnDataTableHandler(){
		executeAction();
		JSFUtilities.hideGeneralDialogues();			
		HolderAccount holderAccount = holderAccountRequestHySession.getHolderAccount();		
		if(Validations.validateIsNull(holderAccountRequestHySession.getHolderAccount().getIdHolderAccountPk())){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_DATA_EMPTY, null);
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		if(Validations.validateIsNullOrEmpty(this.getHolderAccountReqBankHyRequest().getBank().getIdBankPk()) 
				|| Validations.validateIsNullOrEmpty(this.getHolderAccountReqBankHyRequest().getBankAccountType())
				|| Validations.validateIsNullOrEmpty(this.getHolderAccountReqBankHyRequest().getAccountNumber())
				|| Validations.validateIsNullOrEmpty(this.getHolderAccountReqBankHyRequest().getCurrency())){
			if (Validations.validateIsNullOrNotPositive(this.getHolderAccountReqBankHyRequest().getBank().getIdBankPk())) {
				JSFUtilities.addContextMessage("frmHolderAccountMgmt:cboBanksMod", FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_CBO_BANK),
						PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_CBO_BANK));
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MESSAGE_DATA_REQUIRED, null);
				JSFUtilities.showSimpleValidationDialog();				
			}
			if (Validations.validateIsNullOrNotPositive(this.getHolderAccountReqBankHyRequest().getBankAccountType())) {
				JSFUtilities.addContextMessage("frmHolderAccountMgmt:cboAccountsType", FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_CBO_ACCOUNT_TYPE),
						PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_CBO_ACCOUNT_TYPE));							
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MESSAGE_DATA_REQUIRED, null);
				JSFUtilities.showSimpleValidationDialog();				
			} 
			if (Validations.validateIsNullOrEmpty(this.getHolderAccountReqBankHyRequest().getAccountNumber())) {
				JSFUtilities.addContextMessage("frmHolderAccountMgmt:txtAccountNumber",	FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_NUMBER_ACCOUNT),
								PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_NUMBER_ACCOUNT));							
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MESSAGE_DATA_REQUIRED, null);
				JSFUtilities.showSimpleValidationDialog();
			}
			if (Validations.validateIsNullOrNotPositive(this.getHolderAccountReqBankHyRequest().getCurrency())){
				JSFUtilities.addContextMessage("frmHolderAccountMgmt:cboMoneyType",FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_MONEY_TYPE),
						PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_MONEY_TYPE));							
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MESSAGE_DATA_REQUIRED, null);
				JSFUtilities.showSimpleValidationDialog();
			}
			return;
		}
		
		if(this.getHolderAccountReqBankHyRequest().isFlagForeignAccount()) {
			if(Validations.validateIsNullOrEmpty(this.getHolderAccountReqBankHyRequest().getSwiftCode())) {
				JSFUtilities.addContextMessage("frmHolderAccountMgmt:txtSwiftCode",	FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_NUMBER_ACCOUNT),
								PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_NUMBER_ACCOUNT));							
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MESSAGE_DATA_REQUIRED, null);
				JSFUtilities.showSimpleValidationDialog();			
				return;
			}
		}
		 					
		if(holderAccountRequestHySession.getHolderAccountBankRequest().size()+
				 holderAccountRequestHySession.getHolderAccount().getCountHolderAccountBankAvailables()==TAMANIO_BANK_ACCOUNTS){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_BANKACCOUNTSMODIFY, null);
			JSFUtilities.showSimpleValidationDialog();
			return;
		}				
		
		CurrencyType currency = CurrencyType.get(holderAccountReqBankHyRequest.getCurrency());
		if(holderAccount.validateIfBankAccountCurrencyIsAvalilableOnHolderAccount(currency)){
			showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_CURRENCE, new Object[]{holderAccountReqBankHyRequest.getCurrencyDescription()});
			cleanHolderAccountReqBankHyRequest();
			JSFUtilities.showSimpleValidationDialog();			
			return;
		}
		//Validate if account in DOP were selected OR account in USD were selected, is only one account by money's type
		for(HolderAccountBankRequest holderAccountReqBankHy: this.getHolderAccountRequestHySession().getHolderAccountBankRequest()){			
			if(holderAccountReqBankHy.getCurrency().equals(holderAccountReqBankHyRequest.getCurrency())){
				showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_CURRENCE, new Object[]{holderAccountReqBankHyRequest.getCurrencyDescription()});
				cleanHolderAccountReqBankHyRequest();
				JSFUtilities.showSimpleValidationDialog();				
				return;
			}			
		}
		
		if(validateNumberAccountAndBank(this.getHolderAccountReqBankHyRequest().getBank().getIdBankPk(), this.getHolderAccountReqBankHyRequest().getAccountNumber())){
			showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_NUMBER_ACCOUNT_BANK, null);			
			JSFUtilities.showSimpleValidationDialog();				
			return;
		}
		
		JSFUtilities.hideGeneralDialogues();
		this.getHolderAccountReqBankHyRequest().setBank(searchBankOnListFromPK(this.getHolderAccountReqBankHyRequest().getBank().getIdBankPk()));
		HolderAccountBankRequest holderAccountReqBankHy = new HolderAccountBankRequest();
		holderAccountReqBankHy.setBank(this.getHolderAccountReqBankHyRequest().getBank());
		holderAccountReqBankHy.setCurrency(this.getHolderAccountReqBankHyRequest().getCurrency());
		holderAccountReqBankHy.setAccountNumber(this.getHolderAccountReqBankHyRequest().getAccountNumber());
		holderAccountReqBankHy.setBankAccountType(this.getHolderAccountReqBankHyRequest().getBankAccountType());
		holderAccountReqBankHy.setDescriptionBank(this.getHolderAccountReqBankHyRequest().getBankDescription());		
		holderAccountReqBankHy.setHolderAccountRequest(this.getHolderAccountRequestHySession());		
		holderAccountReqBankHy.setRegistryUser(userInfo.getUserAccountSession().getUserName());
		holderAccountReqBankHy.setRegistryDate(CommonsUtilities.currentDateTime());
		holderAccountReqBankHy.setIndOldNew(BooleanType.YES.getCode());
		holderAccountReqBankHy.setHaveBic(BooleanType.NO.getCode());
		holderAccountReqBankHy.setSwiftCode(this.getHolderAccountReqBankHyRequest().getSwiftCode());
		this.getHolderAccountRequestHySession().getHolderAccountBankRequest().add(holderAccountReqBankHy);
		this.holderAccountBankRequestTemp.add(holderAccountReqBankHy);
		cleanHolderAccountReqBankHyRequest();				
	}
	
	/**
	 * Validate number account and bank.
	 *
	 * @param idBank the id bank
	 * @param number the number
	 * @return true, if successful
	 */
	public boolean validateNumberAccountAndBank(Long idBank, String number ){
		boolean band = false;
		HolderAccount holderAccount = holderAccountRequestHySession.getHolderAccount();
		for (HolderAccountBank holderAccountBank : holderAccount.getHolderAccountBanks()) {
			for(Bank bank : this.getBankList()){
				if(bank.getIdBankPk().equals(idBank)){
					if(bank.getDescription().compareTo(holderAccountBank.getDescriptionBank()) == 0 && holderAccountBank.getBankAccountNumber().compareTo(number) == 0){					
						band = true;
						return band;
					}									
				}				
			}			
		}
		for (HolderAccountBankRequest holderAccountBankRequest : getHolderAccountRequestHySession().getHolderAccountBankRequest()) {
			for(Bank bank : this.getBankList()){
				if(bank.getIdBankPk().equals(idBank)){
					if(bank.getDescription().compareTo(holderAccountBankRequest.getDescriptionBank()) == 0 && holderAccountBankRequest.getAccountNumber().compareTo(number) == 0){					
						band = true;
						return band;
					}									
				}				
			}						
		}
		return band;
	}
	
	/**
	 * Validate if have changes.
	 * 
	 * @param holderAccountReqBankHy
	 *            the holder account request bank
	 * @return true, if successful
	 */
	public boolean validateBanksIfHaveChanges(
			HolderAccountBankRequest holderAccountReqBankHy) {
		if (Validations.validateIsNotNull(holderAccountReqBankHy.getBank().getIdBankPk())
				|| (Validations.validateIsNotNull(holderAccountReqBankHy.getCurrency()) && Validations.validateIsPositiveNumber(holderAccountReqBankHy.getCurrency()))
				|| (Validations.validateIsNotNull(holderAccountReqBankHy.getBankAccountType()) && Validations.validateIsPositiveNumber(holderAccountReqBankHy.getBankAccountType()))) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	public Boolean validateIfRequestHaveChanges(HolderAccountRequest holderAccountRequestHySession) {
		
		String newRelatedName = GeneralConstants.EMPTY_STRING;
		String oldRelatedName = GeneralConstants.EMPTY_STRING;
		
		if(Validations.validateIsNotNull(holderAccountRequestHySession.getHolderAccount().getRelatedName())) {
			newRelatedName = holderAccountRequestHySession.getHolderAccount().getRelatedName();
		}
		
		if(Validations.validateIsNotNull(holderAccountRequestHySession.getRelatedNameOriginal())) {
			oldRelatedName = holderAccountRequestHySession.getRelatedNameOriginal();
		}
		
		if( !(newRelatedName.compareTo(oldRelatedName) == 0 && newRelatedName.equals(oldRelatedName))) {
			return Boolean.TRUE;
		}
		
		Integer newIndPDD = holderAccountRequestHySession.isBlPdd()? GeneralConstants.ONE_VALUE_INTEGER : GeneralConstants.ZERO_VALUE_INT;
		
		if(!holderAccountRequestHySession.getIndPddOriginal().equals(newIndPDD)) {
			return Boolean.TRUE;
		}
		
		Boolean hasOriginalFile = Validations.validateIsNotNull(holderAccountRequestHySession.getDocumentPddOriginal());
		Boolean hasNewFile = Validations.validateIsNotNull(holderAccountRequestHySession.getDocumentPdd());
		
		if(!hasOriginalFile.equals(hasNewFile)) {
			return Boolean.TRUE;
		} else {
			if(hasNewFile && !(holderAccountRequestHySession.getDocumentPddOriginal().equals(holderAccountRequestHySession.getDocumentPdd()))) {
				return Boolean.TRUE;
			}
		}
		
		return Boolean.FALSE;
	}
	
	public Boolean validateIfHasOneBankAccountActive() {
		if (!holderAccountRequestHySession.isBlPdd()) {
			if(Validations.validateListIsNullOrEmpty(holderAccountRequestHySession.getHolderAccountBankRequest())){
				return holderAccountRequestHySession.getHolderAccount().getHolderAccountBanks()
						.stream()
						.anyMatch(bankAcc -> bankAcc.getStateAccountBank().equals(HolderAccountStateBankType.REGISTERED.getCode()));
			} else {
				return Boolean.TRUE;
			}
		} else {
			return Boolean.TRUE;
		}
				
		 
	}
	
	/**
	 * Clean holder account to handler.
	 */
	public void cleanHolderAccountTOHandler(){
		this.setHolderAccountTO(new HolderAccountTO());
		holderAccountTO.setInitialDate(CommonsUtilities.currentDate());
		holderAccountTO.setFinalDate(CommonsUtilities.currentDate());
		this.setHolderAccountRequestHyDataModel(null);
		JSFUtilities.resetViewRoot();
		evaluateUserSession();
	}
	
	/**
	 * Clean holder account to.
	 */
	private void cleanHolderAccountTO(){
		holderAccountTO = new HolderAccountTO();
		holderAccountTO.setInitialDate(CommonsUtilities.currentDate());
		holderAccountTO.setFinalDate(CommonsUtilities.currentDate());
		holderAccountTO.setNeedHolder(Boolean.TRUE);
		holderAccountTO.setNeedParticipant(Boolean.FALSE);
	}
	
	/**
	 * Before save holder account modify request handler.
	 */
	public void beforeSaveHolderAccountModifyRequestHandler(){
		executeAction();
		JSFUtilities.hideGeneralDialogues();
		this.hideLocalComponents();
		boolean indRepresentative = false;
		boolean indStatusBank = false;
		if(this.getHolderAccountRequestHySession().getHolderAccount().getHolderAccountDetails() != null && this.getHolderAccountRequestHySession().getHolderAccount().getHolderAccountBanks() != null){
			indRepresentative = compareDetailList(this.getHolderAccountRequestHySession().getHolderAccount().getHolderAccountDetails(), this.getHolderAccountDetailTemp());
			indStatusBank = compareListsBanks(this.getHolderAccountRequestHySession().getHolderAccount().getHolderAccountBanks());	
		}			
		if(Validations.validateIsNull(this.getHolderAccountTO().getParticipantTO()) ||
		   !Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(this.getHolderAccountTO().getParticipantTO())))){
			JSFUtilities.addContextMessage("frmHolderAccountMgmt:cboParticipantTo",FacesMessage.SEVERITY_ERROR,PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_PARTICIPANT, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_PARTICIPANT);
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MESSAGE_DATA_REQUIRED, null);
			JSFUtilities.showSimpleValidationDialog();
			return;
		}else  //Validate fields if there were changes in bank account
			if (validateBanksIfHaveChanges(this.getHolderAccountReqBankHyRequest())) {
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_BANK_CHANGES,null);
				JSFUtilities.showSimpleValidationDialog();
				return;
			}else if(this.getHolderAccountRequestBeforeSave().toString().equals(this.getHolderAccountRequestHySession().toString()) && indRepresentative && indStatusBank 
					&& !(validateDocumentFile(this.getHolderAccountRequestHySession().getHolderAccount().getHolderAccountFiles())) 
					&& !(validateIfRequestHaveChanges(this.getHolderAccountRequestHySession()))){				
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_DATA_EMPTY, null);
				JSFUtilities.showSimpleValidationDialog();
				return;
		}
			
		if(!validateIfHasOneBankAccountActive()) {
			JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_BANK_EMPTY,null);
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		/*Issue 1231-
		 * Los procesos de adjuntar documentos que no serían obligatorios y que 
		 * están a cargo de los Participantes, sería el siguiente:
		 * - Registro de Titular Jurídico
		 * - Registro de Titular Natural
		 * - Registro Cuenta Titular
    	 * 
    	 */
		if(!userInfo.getUserAccountSession().isParticipantInstitucion()){
			if(holderAccountBankRequestTemp!=null && holderAccountBankRequestTemp.size()>0){
				boolean indicator = false;
				for(int i=0; i<parameterTableDocumentsList.size(); i++){
						for (HolderAccountFileRequest holderAccountFile : holderAccountRequestHySession.getHolderAccountFilesRequest()){						
							if(parameterTableDocumentsList.get(i).getParameterTablePk().equals(holderAccountFile.getDocumentType())){
								if(parameterTableDocumentsList.get(i).getShortInteger().equals(BooleanType.YES.getCode())){
									indicator=true;
								}
							}						
						}		
				}
				if(!indicator){
					for(int i=0; i<parameterTableDocumentsList.size(); i++){					
						for (HolderAccountFile holderAccountFile : holderAccountRequestHySession.getHolderAccount().getHolderAccountFiles()){						
							if(parameterTableDocumentsList.get(i).getParameterTablePk().equals(holderAccountFile.getDocumentType())){
								if(parameterTableDocumentsList.get(i).getShortInteger().equals(BooleanType.YES.getCode())){
								indicator=true;
								}
							}						
						}		
					}
				}
				/*if (!indicator) {
					showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_FILE_REQUIRED,null);
					JSFUtilities.showSimpleValidationDialog();
					return;
				}*/
			
			}
		}
		holderAccountTO.setRelatedName(this.getHolderAccounts().get(0).getRelatedName());
		showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_CONFIRMATION_MODIFY, 
				new Object[]{holderAccountRequestHySession.getHolderAccount().getAccountNumber().toString()});
		//JSFUtilities.showComponent(":frmHolderAccountMgmt:cnfHolderAccountModifyMgmt");
		JSFUtilities.executeJavascriptFunction("PF('cnfwHolderAccountMgmt').show()");
	}
	
	private boolean validateDocumentFile(List<HolderAccountFile> listHolderAccountFile ){
		boolean validate = false;
		
		for (HolderAccountFile list : listHolderAccountFile){
			if (!list.getStateDocumentFile().equals(HolderAccountStateBankType.REGISTERED.getCode())){
				validate=true;
			}
		}
		return validate; 
	}
	
	/**
	 * Save holder account modify handler.
	 * we need to put holderDetail and HolderBanks into HolderAccountRequest. because is Modify
	 */
	@LoggerAuditWeb
	public void saveHolderAccountModifyHandler(){
		this.getHolderAccountRequestHySession().setRequestType(HolderAccountRequestHysType.MODIFY.getCode());
		this.getHolderAccountRequestHySession().setParticipant(this.getHolderAccountRequestHySession().getHolderAccount().getParticipant());
		this.getHolderAccountRequestHySession().setParticipant(searchParticipantOnListFromPK(this.getHolderAccountRequestHySession().getParticipant().getIdParticipantPk()));
		this.getHolderAccountRequestHySession().setRequestState(HolderAccountRequestHysStatusType.REGISTERED.getCode());
		this.getHolderAccountRequestHySession().setRequestNumber(0L);
		this.getHolderAccountRequestHySession().setRegistryUser(userInfo.getUserAccountSession().getUserName());
		this.getHolderAccountRequestHySession().setRegistryDate(new Date());
		this.getHolderAccountRequestHySession().setHolderAccountGroup(this.getHolderAccountRequestHySession().getHolderAccount().getAccountGroup());
		this.getHolderAccountRequestHySession().setHolderAccountType(this.getHolderAccountRequestHySession().getHolderAccount().getAccountType());
		this.getHolderAccountRequestHySession().setRelatedName(this.getHolderAccountRequestHySession().getHolderAccount().getRelatedName());
		this.getHolderAccountRequestHySession().setIndPdd(this.getHolderAccountRequestHySession().isBlPdd()?BooleanType.YES.getCode():BooleanType.NO.getCode());
		
		boolean indStatusBank = false;
		indStatusBank = compareListsBanks(this.getHolderAccountRequestHySession().getHolderAccount().getHolderAccountBanks());
		for(HolderAccountBank holderAccountBank: this.getHolderAccountRequestHySession().getHolderAccount().getHolderAccountBanks()){
			HolderAccountBankRequest holderAccountReqBankHy = new HolderAccountBankRequest();
			holderAccountReqBankHy.setAccountNumber(holderAccountBank.getBankAccountNumber());
			holderAccountReqBankHy.setBank(holderAccountBank.getBank());
			holderAccountReqBankHy.setBankAccountType(holderAccountBank.getBankAccountType());				
			holderAccountReqBankHy.setCurrency(holderAccountBank.getCurrency());
			holderAccountReqBankHy.setDescriptionBank(holderAccountBank.getDescriptionBank());				
			holderAccountReqBankHy.setRegistryDate(holderAccountBank.getRegistryDate());
			holderAccountReqBankHy.setRegistryUser(userInfo.getUserAccountSession().getUserName());				
			holderAccountReqBankHy.setHolderAccountRequest(this.getHolderAccountRequestHySession());
			holderAccountReqBankHy.setHaveBic(BooleanType.NO.getCode());
			holderAccountReqBankHy.setSwiftCode(holderAccountBank.getSwiftCode());
			holderAccountReqBankHy.setStateAccountBank(holderAccountBank.getStateAccountBank());
			holderAccountReqBankHy.setIndOldNew(BooleanType.YES.getCode());	
			holderAccountReqBankHy.setRefIdHolderAccountBankFk(holderAccountBank.getIdHolderAccountBankPk());
			this.getHolderAccountRequestHySession().getHolderAccountBankRequest().add(holderAccountReqBankHy);
			
			HolderAccountBankRequest holderAccountReqBankHyOther = new HolderAccountBankRequest();
			holderAccountReqBankHyOther.setAccountNumber(holderAccountBank.getBankAccountNumber());
			holderAccountReqBankHyOther.setBank(holderAccountBank.getBank());
			holderAccountReqBankHyOther.setBankAccountType(holderAccountBank.getBankAccountType());				
			holderAccountReqBankHyOther.setCurrency(holderAccountBank.getCurrency());
			holderAccountReqBankHyOther.setDescriptionBank(holderAccountBank.getDescriptionBank());				
			holderAccountReqBankHyOther.setRegistryDate(holderAccountBank.getRegistryDate());
			holderAccountReqBankHyOther.setRegistryUser(userInfo.getUserAccountSession().getUserName());				
			holderAccountReqBankHyOther.setHolderAccountRequest(this.getHolderAccountRequestHySession());
			holderAccountReqBankHyOther.setHaveBic(BooleanType.NO.getCode());
			holderAccountReqBankHyOther.setIndOldNew(BooleanType.NO.getCode());	
			holderAccountReqBankHyOther.setSwiftCode(holderAccountBank.getSwiftCode());
			holderAccountReqBankHyOther.setStateAccountBank(holderAccountBank.getOriginalStateAccountBank());
			holderAccountReqBankHyOther.setRefIdHolderAccountBankFk(holderAccountBank.getIdHolderAccountBankPk());
			this.getHolderAccountRequestHySession().getHolderAccountBankRequest().add(holderAccountReqBankHyOther);
			
			if(this.getHolderAccountBankRequestTemp().size() != 0){								
				HolderAccountBankRequest holderAccountReqBankHyNew = new HolderAccountBankRequest();
				holderAccountReqBankHyNew.setAccountNumber(holderAccountBank.getBankAccountNumber());
				holderAccountReqBankHyNew.setBank(holderAccountBank.getBank());
				holderAccountReqBankHyNew.setBankAccountType(holderAccountBank.getBankAccountType());				
				holderAccountReqBankHyNew.setCurrency(holderAccountBank.getCurrency());
				holderAccountReqBankHyNew.setDescriptionBank(holderAccountBank.getDescriptionBank());				
				holderAccountReqBankHyNew.setRegistryDate(holderAccountBank.getRegistryDate());
				holderAccountReqBankHyNew.setRegistryUser(userInfo.getUserAccountSession().getUserName());				
				holderAccountReqBankHyNew.setHolderAccountRequest(this.getHolderAccountRequestHySession());
				holderAccountReqBankHyNew.setHaveBic(BooleanType.NO.getCode());
				holderAccountReqBankHyNew.setIndOldNew(BooleanType.NO.getCode());	
				holderAccountReqBankHyNew.setSwiftCode(holderAccountBank.getSwiftCode());
				holderAccountReqBankHyNew.setStateAccountBank(HolderAccountStateBankType.REGISTERED.getCode());
				holderAccountReqBankHyNew.setRefIdHolderAccountBankFk(null);
				this.getHolderAccountRequestHySession().getHolderAccountBankRequest().add(holderAccountReqBankHyOther);
			}
			
			/*
			if(!indStatusBank){  //if(this.getHolderAccountBankRequestTemp().size() != 0){								
				holderAccountReqBankHy.setIndOldNew(BooleanType.YES.getCode());				
				HolderAccountBankRequest holderAccountReqBankHyOther = new HolderAccountBankRequest();
				holderAccountReqBankHyOther.setAccountNumber(holderAccountBank.getBankAccountNumber());
				holderAccountReqBankHyOther.setBank(holderAccountBank.getBank());
				holderAccountReqBankHyOther.setBankAccountType(holderAccountBank.getBankAccountType());				
				holderAccountReqBankHyOther.setCurrency(holderAccountBank.getCurrency());
				holderAccountReqBankHyOther.setDescriptionBank(holderAccountBank.getDescriptionBank());				
				holderAccountReqBankHyOther.setRegistryDate(holderAccountBank.getRegistryDate());
				holderAccountReqBankHyOther.setRegistryUser(userInfo.getUserAccountSession().getUserName());				
				holderAccountReqBankHyOther.setHolderAccountRequest(this.getHolderAccountRequestHySession());
				holderAccountReqBankHyOther.setHaveBic(BooleanType.NO.getCode());
				holderAccountReqBankHyOther.setIndOldNew(BooleanType.NO.getCode());
				holderAccountReqBankHyOther.setSwiftCode(holderAccountBank.getSwiftCode());
				this.getHolderAccountRequestHySession().getHolderAccountBankRequest().add(holderAccountReqBankHyOther);
			}else{
				if(this.getHolderAccountBankRequestTemp().size() != 0){								
					holderAccountReqBankHy.setIndOldNew(BooleanType.YES.getCode());				
					HolderAccountBankRequest holderAccountReqBankHyOther = new HolderAccountBankRequest();
					holderAccountReqBankHyOther.setAccountNumber(holderAccountBank.getBankAccountNumber());
					holderAccountReqBankHyOther.setBank(holderAccountBank.getBank());
					holderAccountReqBankHyOther.setBankAccountType(holderAccountBank.getBankAccountType());				
					holderAccountReqBankHyOther.setCurrency(holderAccountBank.getCurrency());
					holderAccountReqBankHyOther.setDescriptionBank(holderAccountBank.getDescriptionBank());				
					holderAccountReqBankHyOther.setRegistryDate(holderAccountBank.getRegistryDate());
					holderAccountReqBankHyOther.setRegistryUser(userInfo.getUserAccountSession().getUserName());				
					holderAccountReqBankHyOther.setHolderAccountRequest(this.getHolderAccountRequestHySession());
					holderAccountReqBankHyOther.setHaveBic(BooleanType.NO.getCode());
					holderAccountReqBankHyOther.setIndOldNew(BooleanType.NO.getCode());	
					holderAccountReqBankHyOther.setSwiftCode(holderAccountBank.getSwiftCode());
					this.getHolderAccountRequestHySession().getHolderAccountBankRequest().add(holderAccountReqBankHyOther);
				}else{
					holderAccountReqBankHy.setIndOldNew(BooleanType.NO.getCode());	
				}
			}*/		
		}			
		boolean indRepresentative = false;
		indRepresentative = compareDetailList(this.getHolderAccountRequestHySession().getHolderAccount().getHolderAccountDetails(), this.getHolderAccountDetailTemp());
		for(HolderAccountDetail holderAccountDetail: this.getHolderAccountRequestHySession().getHolderAccount().getHolderAccountDetails()){
			HolderAccountDetRequest holderAccountReqDetHy = new HolderAccountDetRequest();
			holderAccountReqDetHy.setHolder(holderAccountDetail.getHolder());
			holderAccountReqDetHy.setIndRepresentative(holderAccountDetail.getIndRepresentative());			
			holderAccountReqDetHy.setRegistryDate(holderAccountDetail.getRegistryDate());
			holderAccountReqDetHy.setRegistryUser(holderAccountDetail.getRegistryUser());			
			holderAccountReqDetHy.setHolderAccountRequest(this.getHolderAccountRequestHySession());																
			if(this.getHolderAccountRequestHySession().getHolderAccount().getAccountType().equals(HolderAccountType.OWNERSHIP.getCode()) ||
					this.getHolderAccountRequestHySession().getHolderAccount().getAccountType().equals(HolderAccountType.SUCCESSION.getCode())){				
				if(indRepresentative){					
					holderAccountReqDetHy.setIndOldNew(BooleanType.NO.getCode());
				}else{
					holderAccountReqDetHy.setIndOldNew(BooleanType.YES.getCode());
				}				
			}else{
				holderAccountReqDetHy.setIndOldNew(BooleanType.NO.getCode());
			}			
			this.getHolderAccountRequestHySession().getHolderAccountDetRequest().add(holderAccountReqDetHy);
		}		
		if(this.getHolderAccountRequestHySession().getHolderAccount().getAccountType().equals(HolderAccountType.OWNERSHIP.getCode()) ||
				this.getHolderAccountRequestHySession().getHolderAccount().getAccountType().equals(HolderAccountType.SUCCESSION.getCode())){
			if(!indRepresentative){
				this.getHolderAccountRequestHySession().getHolderAccountDetRequest().addAll(this.getHolderAccountDetailTemp());
			}
		}
		
		for(HolderAccountFile holderAccountFile: this.getHolderAccountRequestHySession().getHolderAccount().getHolderAccountFiles()){
			HolderAccountFileRequest holderAccountReqFile = new HolderAccountFileRequest();			
			holderAccountReqFile.setDocumentType(holderAccountFile.getDocumentType());
			holderAccountReqFile.setFilename(holderAccountFile.getFilename());
			holderAccountReqFile.setIndMain(holderAccountFile.getIndMain());			
			holderAccountReqFile.setRegistryDate(holderAccountFile.getRegistryDate());
			holderAccountReqFile.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			byte[] fileAccount = null;
			try {
				fileAccount = holderAccountServiceFacade.getHolderAccountFileFacade(holderAccountFile.getIdHolderAccountFilePk());
			} catch (ServiceException e) {
				e.printStackTrace();
			}
			holderAccountReqFile.setDocumentFile(fileAccount);
			holderAccountReqFile.setHolderAccountRequest(this.getHolderAccountRequestHySession());
			if(this.getHolderAccountFileRequestTemp().size() != 0){
				if(compareListsFiles(holderAccountFile.getDocumentType(), this.getHolderAccountRequestHySession().getHolderAccountFilesRequest())){
					holderAccountReqFile.setIndOldNew(BooleanType.NO.getCode());
					this.getHolderAccountRequestHySession().getHolderAccountFilesRequest().add(holderAccountReqFile);//linea q viene de 1074
				}else{				
					holderAccountReqFile.setIndOldNew(BooleanType.YES.getCode());				
					HolderAccountFileRequest holderAccountReqFileOther = new HolderAccountFileRequest();			
					holderAccountReqFileOther.setDocumentType(holderAccountFile.getDocumentType());
					holderAccountReqFileOther.setFilename(holderAccountFile.getFilename());
					holderAccountReqFileOther.setIndMain(holderAccountFile.getIndMain());			
					holderAccountReqFileOther.setRegistryDate(holderAccountFile.getRegistryDate());
					holderAccountReqFileOther.setRegistryUser(userInfo.getUserAccountSession().getUserName());
					holderAccountReqFileOther.setDocumentFile(fileAccount);
					holderAccountReqFileOther.setHolderAccountRequest(this.getHolderAccountRequestHySession());
					holderAccountReqFileOther.setIndOldNew(BooleanType.NO.getCode());
					this.getHolderAccountRequestHySession().getHolderAccountFilesRequest().add(holderAccountReqFileOther);
				}
				
				if (!holderAccountFile.getStateDocumentFile().equals(HolderAccountStateBankType.DELETED.getCode())){// nueva linea
					this.getHolderAccountRequestHySession().getHolderAccountFilesRequest().add(holderAccountReqFile);//linea q viene de 1074
				}
				
			}else{
				// nueva logica que funciona
				if (holderAccountFile.getStateDocumentFile().equals(HolderAccountStateBankType.REGISTERED.getCode())){
					HolderAccountFileRequest holderAccountReqFileInitial = new HolderAccountFileRequest();
					holderAccountReqFileInitial.setDocumentType(holderAccountFile.getDocumentType());
					holderAccountReqFileInitial.setFilename(holderAccountFile.getFilename());
					holderAccountReqFileInitial.setIndMain(holderAccountFile.getIndMain());			
					holderAccountReqFileInitial.setRegistryDate(holderAccountFile.getRegistryDate());
					holderAccountReqFileInitial.setRegistryUser(userInfo.getUserAccountSession().getUserName());
					holderAccountReqFileInitial.setDocumentFile(fileAccount);
					holderAccountReqFileInitial.setHolderAccountRequest(this.getHolderAccountRequestHySession());
					holderAccountReqFileInitial.setIndOldNew(BooleanType.NO.getCode());
					this.getHolderAccountRequestHySession().getHolderAccountFilesRequest().add(holderAccountReqFileInitial);
					holderAccountReqFile.setIndOldNew(BooleanType.YES.getCode());
				}else{
					holderAccountReqFile.setIndOldNew(BooleanType.NO.getCode());
				}
				
				this.getHolderAccountRequestHySession().getHolderAccountFilesRequest().add(holderAccountReqFile);//linea q viene de 1075
			}
//				this.getHolderAccountRequestHySession().getHolderAccountFilesRequest().add(holderAccountReqFile);
		}
			
		try {
			holderAccountServiceFacade.registerHolderAccountRequestServiceFacade(getHolderAccountRequestHySession());
		} catch (ServiceException e) {
			if(e.getErrorService()!=null){
				if(e.getErrorService().equals(ErrorServiceType.HOLDER_ACCOUNT_WITH_BALANCES)){
					showMessageOnDialogOnlyKey(PropertiesConstants.MESSAGES_ALERT, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_CLOSE_WITH_BALANCES);
					JSFUtilities.showComponent(":frmHolderAccountMgmt:holderAccountMgmtOk");
					loadPrivilegesUser();
					this.setViewOperationType(ViewOperationsType.CONSULT.getCode());
					return;
				}else{
					showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
					JSFUtilities.showComponent(":frmHolderAccountMgmt:holderAccountMgmtOk");
					loadPrivilegesUser();
					return;
				}
			}
			e.printStackTrace();
		}
		showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, PropertiesConstants.ADM_HOLDERACCOUNT_CONFIRMATION_MODIFY_OK,
				new Object[]{holderAccountRequestHySession.getIdHolderAccountReqPk().toString(), holderAccountRequestHySession.getHolderAccount().getAccountNumber().toString()});
		JSFUtilities.showComponent(":frmHolderAccountMgmt:holderAccountMgmtOk");
		this.setHolderAccountRequestHySession(new HolderAccountRequest());
		this.getHolderAccountRequestHySession().setHolderAccount(new HolderAccount());
	}
	
	/**
	 * Anular archivos adjuntos
	 * @param holderAccountBankParam
	 */
	public void annulateHolderAccountFileFromDataTableHandler(HolderAccountFile holderAccountFile){
		executeAction();
//		if(holderAccountBankParam.getStateAccountBank().equals(HolderAccountStateBankType.DELETED.getCode())){
//			for(HolderAccountBankRequest holderAccountReqBankHy: this.getHolderAccountRequestHySession().getHolderAccountBankRequest()){
//				if(holderAccountReqBankHy.getCurrency().equals(holderAccountBankParam.getCurrency())){
//					showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_CURRENCY, new Object[]{CurrencyType.lookup.get(holderAccountBankParam.getCurrency()).getValue()});
//					JSFUtilities.showValidationDialog();
//					return;
//				}
//				if(holderAccountReqBankHy.getDescriptionBank().compareTo(holderAccountBankParam.getDescriptionBank()) == 0 && holderAccountReqBankHy.getAccountNumber().compareTo(holderAccountBankParam.getBankAccountNumber()) == 0){										
//					showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_NUMBER_ACCOUNT_BANK, null);			
//					JSFUtilities.showSimpleValidationDialog();	
//					return;
//				}														
//				if(holderAccountReqBankHy.getCurrency().equals(holderAccountBankParam.getCurrency())){
//					showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_CURRENCY, new Object[]{CurrencyType.lookup.get(holderAccountBankParam.getCurrency()).getValue()});
//					JSFUtilities.showValidationDialog();
//					return;
//				}
//			}
//		}
		JSFUtilities.hideGeneralDialogues();
		if(holderAccountFile.getStateDocumentFile().equals(HolderAccountStateBankType.REGISTERED.getCode())){
			holderAccountFile.setStateDocumentFile(HolderAccountStateBankType.DELETED.getCode());
		}else if(holderAccountFile.getStateDocumentFile().equals(HolderAccountStateBankType.DELETED.getCode())){
			holderAccountFile.setStateDocumentFile(HolderAccountStateBankType.REGISTERED.getCode());
		}
	}
	
	/**
	 * Compare lists files.
	 *
	 * @param documentType the document type
	 * @param modelList the model list
	 * @return true, if successful
	 */
	public boolean compareListsFiles(Integer documentType, List<HolderAccountFileRequest> modelList) {        
		for (HolderAccountFileRequest modelListdata : modelList) {
			if (documentType.equals(modelListdata.getDocumentType())) {
				return  true;
			}
		}               
        return false; 
    }
	
	/**
	 * Compare detail list.
	 *
	 * @param listA the list a
	 * @param listB the list b
	 * @return true, if successful
	 */
	public boolean compareDetailList(List<HolderAccountDetail> listA, List<HolderAccountDetRequest> listB) {	        		
	    for (int i=0; i<listA.size(); i++) {
	    	HolderAccountDetail aClass = (HolderAccountDetail) listA.get(i);	    	
	    	for(int r=0; r<listB.size(); r++){
	    		HolderAccountDetRequest bClass = (HolderAccountDetRequest) listB.get(r);
	    		if ((aClass.getHolder().getIdHolderPk().equals(bClass.getHolder().getIdHolderPk()))
	                    && aClass.getIndRepresentative().equals(bClass.getIndRepresentative())) {
		    		return false;
		    	}	
	    	}	    		    		    		    	    		       
	    }
	    return true;
	}
	
	/**
	 * Compare lists banks.
	 *
	 * @param modelList the model list
	 * @return true, if successful
	 */
	public boolean compareListsBanks(List<HolderAccountBank> modelList) {        
		for (HolderAccountBank modelListdata : modelList) {
			if (modelListdata.getStateAccountBank().equals(HolderAccountStateBankType.DELETED.getCode())) {
				return  false;
			}
		}               
        return true; 
    }
	
	/**
	 * Search holder account modify request hys handler.
	 */
	@LoggerAuditWeb
	public void searchHolderAccountModifyRequestHysHandler(){
		JSFUtilities.hideGeneralDialogues();
		this.hideLocalComponents();
		List<HolderAccountRequest> holderAccountRequestHyList =null;
		holderAccountTO.setHolderAccountRequestType(HolderAccountRequestHysType.MODIFY.getCode());
		holderAccountTO.setNeedHolder(Boolean.TRUE);
		if (holderAccountTO.getParticipantTO() == null || !Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(holderAccountTO.getParticipantTO())))) {
			JSFUtilities.addContextMessage(
							"frmHolderRequestSearch:cboParticipant",FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_PARTICIPANT),
							PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_PARTICIPANT));
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MESSAGE_DATA_REQUIRED, null);
			JSFUtilities.showValidationDialog();
			return;
		}
		if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
			//SI NO TIENE PARTICIPANTE Y ES EMISOR DPF NO PUEDE REGISTRAR
			if(!flagExistParticipantDPF){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_PARTICIPANT));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
		}
		try {
			holderAccountRequestHyList = holderAccountServiceFacade.getHolderAccountRequestHysListServiceFacade(holderAccountTO);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		holderAccountRequestHyDataModel = new GenericDataModel<HolderAccountRequest>(holderAccountRequestHyList);
	}
	
	/**
	 * Selected holder account request detail hanlder.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 */
	public void selectedHolderAccountRequestDetailHanlder(HolderAccountRequest holderAccountRequestHy){
		this.setViewOperationType(ViewOperationsType.CONSULT.getCode());
		readObjectFromBD(holderAccountRequestHy);
		documentFileName = holderAccountRequestHySession.getNameDocumentPdd();
	}
	
	/**
	 * Before anulate holder account request handler.
	 *
	 * @param e the e
	 */
	@SuppressWarnings("unchecked")
	public void beforeAnulateHolderAccountRequestHandler(ActionEvent e){
		JSFUtilities.resetComponent(":frmMotive:dialogMotive");
		executeAction();
		if(Validations.validateIsNull(this.getHolderAccountRequestHySession())){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
			JSFUtilities.showValidationDialog();
			return;
		}
		JSFUtilities.hideGeneralDialogues();
		hideLocalComponents();
		if(this.motiveController!=null){
			this.motiveController.setIdMotivePK(null);
			this.motiveController.setShowMotiveText(Boolean.FALSE);
			this.motiveController.setMotiveText(null);
		}
		if(this.getHolderAccountRequestHySession().getRequestState().equals(HolderAccountRequestHysStatusType.REGISTERED.getCode())){
			this.getMotiveController().setMotiveList((List<ParameterTable>) MotiveController.getMotiveListCache().get(MasterTableType.HOLDER_REQUEST_ANNULAR_MOTIVE.getCode()));
			this.setViewOperationType(ViewOperationsType.ANULATE.getCode());			
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MOTIVE_ANNUL),null);
			JSFUtilities.showComponent(":frmMotive:dialogMotive");
			return;
		}
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_DOACTION, new Object[]{this.getHolderAccountRequestHySession().getStateHolderAccountReqDescription()}));
		JSFUtilities.showValidationDialog();
	}
	
	/**
	 * Before approve holder account request handler.
	 *
	 * @param e the e
	 */
	public void beforeApproveHolderAccountRequestHandler(ActionEvent e){
		executeAction();
		if(Validations.validateIsNull(this.getHolderAccountRequestHySession())){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
			JSFUtilities.showValidationDialog();
			return;
		}
		JSFUtilities.hideGeneralDialogues();	
		hideLocalComponents();
		if(this.getHolderAccountRequestHySession().getRequestState().equals(HolderAccountRequestHysStatusType.REGISTERED.getCode())){
			
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_APPROVE), 
					PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_APPROVE_MODIFY, 
					new Object[]{holderAccountRequestHySession.getIdHolderAccountReqPk().toString(),holderAccountRequestHySession.getRequestTypeDescription(),holderAccountRequestHySession.getDescriptionCui()}));
			this.setViewOperationType(ViewOperationsType.APPROVE.getCode());
			JSFUtilities.showComponent(":frmBlock:cnfblock");
			return;
		}
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
				PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_DOACTION, new Object[]{this.getHolderAccountRequestHySession().getStateHolderAccountReqDescription()}));
		JSFUtilities.showValidationDialog();
	}
	/**
	 * Hide local components.
	 */
	public void hideLocalComponents(){
		this.motiveController.setIdMotivePK(null);
		this.motiveController.setShowMotiveText(Boolean.FALSE);
		this.motiveController.setMotiveText(null);
		JSFUtilities.hideComponent(":frmMotive:dialogMotive");
		JSFUtilities.hideComponent(":frmBlock:cnfblock");
		//JSFUtilities.hideComponent(":frmHolderAccountMgmt:cnfHolderAccountModifyMgmt");
		JSFUtilities.executeJavascriptFunction("PF('cnfwHolderAccountMgmt').hide()");
		JSFUtilities.hideComponent(":frmHolderAccountMgmt:cnfCleanHolderAccountMgmt");
		JSFUtilities.hideComponent(":frmHolderAccountMgmt:cnfBackAdvancedAccount");
		JSFUtilities.hideGeneralDialogues();
	}
	
	/**
	 * Before reject holder account request handler.
	 *
	 * @param e the e
	 */
	@SuppressWarnings("unchecked")
	public void beforeRejectHolderAccountRequestHandler(ActionEvent e){
		JSFUtilities.resetComponent(":frmMotive:dialogMotive");
		executeAction();
		if(Validations.validateIsNull(this.getHolderAccountRequestHySession())){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
			JSFUtilities.showValidationDialog();
			return;
		}
		JSFUtilities.hideGeneralDialogues();
		hideLocalComponents();
		if(this.motiveController!=null){
			this.motiveController.setIdMotivePK(null);
			this.motiveController.setShowMotiveText(Boolean.FALSE);
			this.motiveController.setMotiveText(null);
		}
		if(this.getHolderAccountRequestHySession().getRequestState().equals(HolderAccountRequestHysStatusType.APPROVE.getCode())){
			this.getMotiveController().setMotiveList((List<ParameterTable>) MotiveController.getMotiveListCache().get(MasterTableType.HOLDER_REQUEST_REJECT_MOTIVE.getCode()));
			this.setViewOperationType(ViewOperationsType.REJECT.getCode());
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MOTIVE_REJECT), null);
			JSFUtilities.showComponent(":frmMotive:dialogMotive");
			return;
		}
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
				PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_DOACTION, new Object[]{this.getHolderAccountRequestHySession().getStateHolderAccountReqDescription()}));
		JSFUtilities.showValidationDialog();
	}
	
	/**
	 * Removes the account on data table handler.
	 *
	 * @param holderAccountReqBankHy the holder account req bank hy
	 */
	public void removeAccountOnDataTableHandler(HolderAccountBankRequest holderAccountReqBankHy){
		holderAccountRequestHySession.getHolderAccountBankRequest().remove(holderAccountReqBankHy);
		this.holderAccountBankRequestTemp.remove(holderAccountReqBankHy);
	}
	
	/**
	 * Before confirm holder account request handler.
	 *
	 * @param e the e
	 */
	public void beforeConfirmHolderAccountRequestHandler(ActionEvent e){
		executeAction();
		if(Validations.validateIsNull(this.getHolderAccountRequestHySession())){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
			JSFUtilities.showValidationDialog();
			return;
		}
		JSFUtilities.hideGeneralDialogues();
		hideLocalComponents();
		if(this.getHolderAccountRequestHySession().getRequestState().equals(HolderAccountRequestHysStatusType.APPROVE.getCode())){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_CONFIRM_MODIFY, 
					new Object[]{holderAccountRequestHySession.getIdHolderAccountReqPk().toString(),holderAccountRequestHySession.getRequestTypeDescription(),holderAccountRequestHySession.getDescriptionCui()}));
			this.setViewOperationType(ViewOperationsType.CONFIRM.getCode());
			JSFUtilities.showComponent(":frmBlock:cnfblock");
			return;
		}
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_DOACTION,new Object[]{holderAccountRequestHySession.getStateHolderAccountReqDescription()}));
		JSFUtilities.showValidationDialog();
	}
	/**
	 * Change action holder account request handler.
	 */
	@LoggerAuditWeb
	public void changeActionHolderAccountRequestHandler(){
		HolderAccountRequest holdAccReqTemp = new HolderAccountRequest();
		holdAccReqTemp.setIdHolderAccountReqPk(this.getHolderAccountRequestHySession().getIdHolderAccountReqPk());
		holdAccReqTemp.setRequestState(this.getHolderAccountRequestHySession().getRequestState());
		holdAccReqTemp.setHolderAccountDetRequest(holderAccountRequestHySession.getHolderAccountDetRequest());
		holdAccReqTemp.setRequestType(HolderAccountRequestHysType.MODIFY.getCode());
		holdAccReqTemp.setParticipant(holderAccountRequestHySession.getParticipant());
		holdAccReqTemp.setHolderAccount(holderAccountRequestHySession.getHolderAccount());
		holdAccReqTemp.setRelatedName(holderAccountRequestHySession.getRelatedName());
		try {
			boolean isConfirm = Boolean.FALSE;
			String messageResponseOk = null ;
			switch(ViewOperationsType.lookup.get(this.getViewOperationType())){
				case APPROVE:
					holdAccReqTemp.setApproveDate(new Date());
					holdAccReqTemp.setApproveUser(userInfo.getUserAccountSession().getUserName());
					messageResponseOk = PropertiesConstants.ADM_HOLDERACCOUNT_REQUEST_APPROVE_OK_MODIFY;
					holdAccReqTemp = holderAccountServiceFacade.approveHolderAccountRequest(holdAccReqTemp);break;
				case ANULATE:
					holdAccReqTemp.setAnullDate(new Date());
					holdAccReqTemp.setAnullUser(userInfo.getUserAccountSession().getUserName());
					holdAccReqTemp.setRequestMotive(Integer.parseInt(String.valueOf(this.getMotiveController().getIdMotivePK())));
					holdAccReqTemp.setRequestOtherMotive(this.getMotiveController().getMotiveText());
					messageResponseOk = PropertiesConstants.ADM_HOLDERACCOUNT_REQUEST_ANULATE_OK_MODIFY;
					holdAccReqTemp = holderAccountServiceFacade.annulateHolderAccountRequest(holdAccReqTemp);break;
				case CONFIRM:
					holdAccReqTemp.setConfirmDate(new Date());
					holdAccReqTemp.setConfirmUser(userInfo.getUserAccountSession().getUserName());
					isConfirm = Boolean.TRUE;
					holdAccReqTemp = holderAccountServiceFacade.confirmHolderAccountRequest(holdAccReqTemp);					
					break;
				case REJECT:
					holdAccReqTemp.setRejectDate(new Date());
					holdAccReqTemp.setRejectUser(userInfo.getUserAccountSession().getUserName());
					holdAccReqTemp.setRequestMotive(Integer.parseInt(String.valueOf(this.getMotiveController().getIdMotivePK())));
					holdAccReqTemp.setRequestOtherMotive(this.getMotiveController().getMotiveText());
					messageResponseOk = PropertiesConstants.ADM_HOLDERACCOUNT_REQUEST_REJECT_OK_MODIFY;
					holdAccReqTemp = holderAccountServiceFacade.rejectHolderAccountRequest(holdAccReqTemp);break;
			}
			searchHolderAccountModifyRequestHysHandler();
			if(isConfirm){
				showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, PropertiesConstants.ADM_HOLDERACCOUNT_REQUEST_CONFIRM_MODIFY_OK, 
						new Object[]{holdAccReqTemp.getIdHolderAccountReqPk().toString(),holdAccReqTemp.getHolderAccount().getAccountNumber().toString(),holdAccReqTemp.getDescriptionCui()});
			}else{
				showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, messageResponseOk, 
						new Object[]{holdAccReqTemp.getIdHolderAccountReqPk().toString(), holdAccReqTemp.getRequestTypeDescription(),holdAccReqTemp.getDescriptionCui()});
			}
			JSFUtilities.hideComponent(":frmBlock:cnfblock");
			JSFUtilities.showComponent(":frmBlock:holderAccountMgmtOk");
			loadPrivilegesUser();
			this.setViewOperationType(ViewOperationsType.CONSULT.getCode());
		} catch (ServiceException e) {
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
				JSFUtilities.showComponent(":frmBlock:holderAccountMgmtOk");
				loadPrivilegesUser();
				return;
			}
			e.printStackTrace();
		}
	}
	
	/**
	 * Change motive handler.
	 */
	public void changeMotiveHandler(){
		if (this.getMotiveController().getIdMotivePK() != null) {
			if(this.getMotiveController().getIdMotivePK()==ERROR_ANULATE || this.getMotiveController().getIdMotivePK()==ERROR_REJECT){
				this.getMotiveController().setShowMotiveText(Boolean.TRUE);
			}else{
				this.getMotiveController().setShowMotiveText(Boolean.FALSE);
			}
		}else{
			this.getMotiveController().setShowMotiveText(Boolean.FALSE);
		}
	}
	
	/**
	 * Confirm motive handler.
	 */
	public void confirmMotiveHandler(){	
		JSFUtilities.hideComponent(":frmBlock:cnfblock");
		if (this.getMotiveController().getIdMotivePK() == null || !Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(this.getMotiveController().getIdMotivePK())))) {
			JSFUtilities.addContextMessage(
							"frmMotive:cboMotive",FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_MOTIVE_REJECT_ANULATE),
							PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_MOTIVE_REJECT_ANULATE));
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MESSAGE_DATA_REQUIRED, null);
			JSFUtilities.showValidationDialog();
			return;
		}	
		if (this.getMotiveController().getIdMotivePK().equals(ERROR_ANULATE)|| this.getMotiveController().getIdMotivePK().equals(ERROR_REJECT)) {
			if (this.getMotiveController().getMotiveText() == null || Validations.validateIsNullOrEmpty(this.getMotiveController().getMotiveText())) {			
				JSFUtilities.addContextMessage(
						"frmMotive:txtOtherMotive",FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_OTHER_MOTIVE_REJECT_ANULATE),
						PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_OTHER_MOTIVE_REJECT_ANULATE));
						showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MESSAGE_DATA_REQUIRED, null);
						JSFUtilities.showValidationDialog();
						return;
			} 
		} 
		
		if(this.getMotiveController().getIdMotivePK()==ERROR_ANULATE || this.getMotiveController().getIdMotivePK()==ERROR_REJECT){
			if(Validations.validateIsNullOrEmpty(this.getMotiveController().getMotiveText())){
				JSFUtilities.showComponent(":frmMotive:dialogMotive");
				return;
			}
		}
		//JSFUtilities.hideComponent(":frmMotive:dialogMotive");
		if(this.getViewOperationType().equals(ViewOperationsType.ANULATE.getCode())){
			showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ADM_HOLDERACCOUNT_ANULATE_MODIFY, 
			new Object[]{holderAccountRequestHySession.getIdHolderAccountReqPk().toString(),holderAccountRequestHySession.getRequestTypeDescription(),holderAccountRequestHySession.getDescriptionCui()});
		}else if(this.getViewOperationType().equals(ViewOperationsType.REJECT.getCode())){
			showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ADM_HOLDERACCOUNT_REJECT_MODIFY, 
			new Object[]{holderAccountRequestHySession.getIdHolderAccountReqPk().toString(),holderAccountRequestHySession.getRequestTypeDescription(),holderAccountRequestHySession.getDescriptionCui()});
		}
		JSFUtilities.showComponent(":frmBlock:cnfblock");
	}
	
	/**
	 * Annulate holder account bank from data table handler.
	 *
	 * @param holderAccountBankParam the holder account bank parameter
	 */
	public void annulateHolderAccountBankFromDataTableHandler(HolderAccountBank holderAccountBankParam){
		executeAction();
		if(holderAccountBankParam.getStateAccountBank().equals(HolderAccountStateBankType.DELETED.getCode())){
			for(HolderAccountBankRequest holderAccountReqBankHy: this.getHolderAccountRequestHySession().getHolderAccountBankRequest()){
				if(holderAccountReqBankHy.getCurrency().equals(holderAccountBankParam.getCurrency())){
					showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_CURRENCY, new Object[]{CurrencyType.lookup.get(holderAccountBankParam.getCurrency()).getValue()});
					JSFUtilities.showValidationDialog();
					return;
				}
				if(holderAccountReqBankHy.getDescriptionBank().compareTo(holderAccountBankParam.getDescriptionBank()) == 0 && holderAccountReqBankHy.getAccountNumber().compareTo(holderAccountBankParam.getBankAccountNumber()) == 0){										
					showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_NUMBER_ACCOUNT_BANK, null);			
					JSFUtilities.showSimpleValidationDialog();	
					return;
				}														
				if(holderAccountReqBankHy.getCurrency().equals(holderAccountBankParam.getCurrency())){
					showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_CURRENCY, new Object[]{CurrencyType.lookup.get(holderAccountBankParam.getCurrency()).getValue()});
					JSFUtilities.showValidationDialog();
					return;
				}
			}
		}
		JSFUtilities.hideGeneralDialogues();
		if(holderAccountBankParam.getStateAccountBank().equals(HolderAccountStateBankType.REGISTERED.getCode())){
			holderAccountBankParam.setStateAccountBank(HolderAccountStateBankType.DELETED.getCode());
		}else if(holderAccountBankParam.getStateAccountBank().equals(HolderAccountStateBankType.DELETED.getCode())){
			holderAccountBankParam.setStateAccountBank(HolderAccountStateBankType.REGISTERED.getCode());
		}
	}
	
	/**
	 * Gets the holder account request status type list.
	 *
	 * @return the holder account request  status type list
	 */
	public List<HolderAccountRequestHysStatusType> getHolderAccountRequestHysStatusTypeList(){
		return HolderAccountRequestHysStatusType.list;
	}
	
	/**
	 * Search bank on list from pk.
	 *
	 * @param idBankPk the id bank pk
	 * @return the bank
	 */
	public Bank searchBankOnListFromPK(Long idBankPk){
		for(Bank bank: this.getBankList()){
			if(bank.getIdBankPk().equals(idBankPk)){
				return bank;
			}
		}
		return new Bank();
	}
	
	/**
	 * Search participant on list from pk.
	 *
	 * @param idParticipantPk the id participant pk
	 * @return the participant
	 * this method is useful to get Bank from idBankPk because with this method
	 * it's not necesary to getParticipant from database.
	 */
	public Participant searchParticipantOnListFromPK(Long idParticipantPk){
		for(Participant participant: this.getParticipantList()){
			if(participant.getIdParticipantPk().equals(idParticipantPk)){
				return participant;
			}
		}
		return null;
	}

	/**
	 * Gets the holder account session.
	 *
	 * @return the holder account session
	 */
	public HolderAccount getHolderAccountSession() {
		return holderAccountSession;
	}

	/**
	 * Sets the holder account session.
	 *
	 * @param holderAccountSession the new holder account session
	 */
	public void setHolderAccountSession(HolderAccount holderAccountSession) {
		this.holderAccountSession = holderAccountSession;
	}

	/**
	 * Gets the holder account request  session.
	 *
	 * @return the holder account request  session
	 */
	public HolderAccountRequest getHolderAccountRequestHySession() {
		return holderAccountRequestHySession;
	}

	/**
	 * Sets the holder account request  session.
	 *
	 * @param holderAccountRequestHySession the new holder account request  session
	 */
	public void setHolderAccountRequestHySession(HolderAccountRequest holderAccountRequestHySession) {
		this.holderAccountRequestHySession = holderAccountRequestHySession;
	}

	/**
	 * Gets the holder account to.
	 *
	 * @return the holder account to
	 */
	public HolderAccountTO getHolderAccountTO() {
		return holderAccountTO;
	}
	
	/**
	 * Sets the holder account to.
	 *
	 * @param holderAccountTO the new holder account to
	 */
	public void setHolderAccountTO(HolderAccountTO holderAccountTO) {
		this.holderAccountTO = holderAccountTO;
	}

	/**
	 * Gets the participant list.
	 *
	 * @return the participant list
	 */
	public List<Participant> getParticipantList() {
		return participantList;
	}

	/**
	 * Sets the participant list.
	 *
	 * @param participantList the new participant list
	 */
	public void setParticipantList(List<Participant> participantList) {
		this.participantList = participantList;
	}

	/**
	 * Gets the holder account req bank hy request.
	 *
	 * @return the holder account req bank hy request
	 */
	public HolderAccountBankRequest getHolderAccountReqBankHyRequest() {
		return holderAccountReqBankHyRequest;
	}

	/**
	 * Sets the holder account request bank  request.
	 *
	 * @param holderAccountReqBankHyRequest the new holder account request bank  request
	 */
	public void setHolderAccountReqBankHyRequest(
			HolderAccountBankRequest holderAccountReqBankHyRequest) {
		this.holderAccountReqBankHyRequest = holderAccountReqBankHyRequest;
	}

	/**
	 * Gets the bank list.
	 *
	 * @return the bank list
	 */
	public List<Bank> getBankList() {
		return bankList;
	}

	/**
	 * Sets the bank list.
	 *
	 * @param bankList the new bank list
	 */
	public void setBankList(List<Bank> bankList) {
		this.bankList = bankList;
	}

	/**
	 * Gets the holder account request  data model.
	 *
	 * @return the holder account request  data model
	 */
	public GenericDataModel<HolderAccountRequest> getHolderAccountRequestHyDataModel() {
		return holderAccountRequestHyDataModel;
	}

	/**
	 * Sets the holder account request  data model.
	 *
	 * @param holderAccountRequestHyDataModel the new holder account request  data model
	 */
	public void setHolderAccountRequestHyDataModel(GenericDataModel<HolderAccountRequest> holderAccountRequestHyDataModel) {
		this.holderAccountRequestHyDataModel = holderAccountRequestHyDataModel;
	}

	/**
	 * Gets the holder account request detail request.
	 *
	 * @return the holder account request detail request
	 */
	public Holder getHolderAccountReqDetHyRequest() {
		return holderAccountReqDetHyRequest;
	}

	/**
	 * Sets the holder account request detail request.
	 *
	 * @param holderAccountReqDetHyRequest the new holder account request detail request
	 */
	public void setHolderAccountReqDetHyRequest(Holder holderAccountReqDetHyRequest) {
		this.holderAccountReqDetHyRequest = holderAccountReqDetHyRequest;
	}

	/**
	 * Checks if is show group account.
	 *
	 * @return true, if is show group account
	 */
	public boolean isShowGroupAccount() {
		return showGroupAccount;
	}

	/**
	 * Sets the show group account.
	 *
	 * @param showGroupAccount the new show group account
	 */
	public void setShowGroupAccount(boolean showGroupAccount) {
		this.showGroupAccount = showGroupAccount;
	}

	/**
	 * Gets the parameter table money type list.
	 *
	 * @return the parameter table money type list
	 */
	public List<ParameterTable> getParameterTableMoneyTypeList() {
		return parameterTableMoneyTypeList;
	}

	/**
	 * Sets the parameter table money type list.
	 *
	 * @param parameterTableMoneyTypeList the new parameter table money type list
	 */
	public void setParameterTableMoneyTypeList(List<ParameterTable> parameterTableMoneyTypeList) {
		this.parameterTableMoneyTypeList = parameterTableMoneyTypeList;
	}

	/**
	 * Gets the motive controller.
	 *
	 * @return the motive controller
	 */
	public MotiveController getMotiveController() {
		return motiveController;
	}

	/**
	 * Sets the motive controller.
	 *
	 * @param motiveController the new motive controller
	 */
	public void setMotiveController(MotiveController motiveController) {
		this.motiveController = motiveController;
	}
	
	/**
	 * Gets the size max on file.
	 *
	 * @return the size max on file
	 */
	public String getTamanioMaxOnFile(){
		return PropertiesUtilities.getMessage(PropertiesConstants.TAMANIO_FILE, new Object[]{GeneralConstants.TAMANIO_FILE_KBS});
	}

	/**
	 * Gets the back dialog controller.
	 *
	 * @return the back dialog controller
	 */
	public BackDialogController getBackDialogController() {
		return backDialogController;
	}

	/**
	 * Sets the back dialog controller.
	 *
	 * @param backDialogController the new back dialog controller
	 */
	public void setBackDialogController(BackDialogController backDialogController) {
		this.backDialogController = backDialogController;
	}

	/**
	 * Gets the holder account before save.
	 *
	 * @return the holder account before save
	 */
	public StringBuilder getHolderAccountRequestBeforeSave() {
		return holderAccountRequestBeforeSave;
	}

	/**
	 * Sets the holder account before save.
	 *
	 * @param holderAccountBeforeSave the new holder account before save
	 */
	public void setHolderAccountRequestBeforeSave(StringBuilder holderAccountBeforeSave) {
		this.holderAccountRequestBeforeSave = holderAccountBeforeSave;
	}

	/**
	 * Gets the parameter table documents list.
	 *
	 * @return the parameter table documents list
	 */
	public List<ParameterTable> getParameterTableDocumentsList() {
		return parameterTableDocumentsList;
	}

	/**
	 * Sets the parameter table documents list.
	 *
	 * @param parameterTableDocumentsList the new parameter table documents list
	 */
	public void setParameterTableDocumentsList(List<ParameterTable> parameterTableDocumentsList) {
		this.parameterTableDocumentsList = parameterTableDocumentsList;
	}
	
	/**
	 * Gets the word formated.
	 *
	 * @param word the word
	 * @return the word formated
	 */
	public static String getWordFormated(String word){
		char[] caracteres = word.toLowerCase().toCharArray();
		caracteres[0] = Character.toUpperCase(caracteres[0]);
		for (int i = 0; i < word.length()- 2; i++){
			if (caracteres[i] == ' ' || caracteres[i] == '.' || caracteres[i] == ','){
		    	caracteres[i + 1] = Character.toUpperCase(caracteres[i + 1]);
		    }
		}
		return new String(caracteres);
	}

	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	/**
	 * Evaluate user info active.
	 */
	private void evaluateUserInfoActive(){
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			Participant participantTO = new Participant(userInfo.getUserAccountSession().getParticipantCode());
			try {
				List<Participant> participantList = accountsFacade.getLisParticipantServiceBean(participantTO);
				if(participantList!=null && !participantList.isEmpty()){
					participantTO = participantList.get(0);
					if(this.participantList.contains(participantTO)){
						userInfo.getUserAccountSession().setInstitutionState(ParticipantStateType.REGISTERED.getCode());
					}else{
						this.participantList.addAll(participantList);
						userInfo.getUserAccountSession().setInstitutionState(participantTO.getState());
					}
				}
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Gets the evaluate user info active.
	 * @return the evaluate user info active
	 */
	public boolean getEvaluateUserInfoIsActive(){
		if(userInfo.getUserAccountSession().isDepositaryInstitution()){
			return Boolean.TRUE;
		} else if (userInfo.getUserAccountSession().isIssuerDpfInstitucion()) {
			return Boolean.TRUE;
		} else if (userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
			return Boolean.TRUE;
		}else if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			if(userInfo.getUserAccountSession().getInstitutionState()!=null){
				return userInfo.getUserAccountSession().getInstitutionState().equals(ParticipantStateType.REGISTERED.getCode());
			}
		}else if(userInfo.getUserAccountSession().isIssuerInstitucion()){
			if(userInfo.getUserAccountSession().getInstitutionState()!=null){
				return userInfo.getUserAccountSession().getInstitutionState().equals(IssuerStateType.REGISTERED.getCode());
			}
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Gets the evaluate user info register.
	 * metodo para saber si usuario puede O no Registrar
	 * @return the evaluate user info register
	 */
	public boolean getEvaluateUserInfoCanRegister(){
		if(userInfo.getUserAcctions()!=null){
			Boolean canRegister = userPrivilege.getUserAcctions().isRegister();
			if(canRegister!=null && canRegister){
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Read object from bd.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 */
	private void readObjectFromBD(HolderAccountRequest holderAccountRequestHy){
		HolderAccountRequest holderAccountRequestHyFinded = new HolderAccountRequest();
		try {
			holderAccountRequestHyFinded = holderAccountServiceFacade.getHolderAccountRequestHyServiceFacade(holderAccountRequestHy.getIdHolderAccountReqPk());
		} catch (ServiceException e) {
			e.printStackTrace();
		}		
		
		holderAccountRequestHyFinded.setBlPdd(holderAccountRequestHyFinded.getIndPdd()==BooleanType.YES.getCode()?true:false);
		List<HolderAccountDetRequest> hadold = new ArrayList<HolderAccountDetRequest>();
		List<HolderAccountDetRequest> hadnew = new ArrayList<HolderAccountDetRequest>();
		for(HolderAccountDetRequest holderAccountDetGen : holderAccountRequestHyFinded.getHolderAccountDetRequest()){			
			if(holderAccountDetGen.getIndOldNew() == (BooleanType.NO.getCode())){
				hadold.add(holderAccountDetGen);				
			}else{
				hadnew.add(holderAccountDetGen);
			}
		}
		holderAccountDetRequestOld = new GenericDataModel<HolderAccountDetRequest>(hadold);
		holderAccountDetRequestNew = new GenericDataModel<HolderAccountDetRequest>(hadnew);	
		
		List<HolderAccountBankRequest> habrold = new ArrayList<HolderAccountBankRequest>();
		List<HolderAccountBankRequest> habrnew = new ArrayList<HolderAccountBankRequest>();
		for(HolderAccountBankRequest holderAccountBankGen : holderAccountRequestHyFinded.getHolderAccountBankRequest()){			
			if(holderAccountBankGen.getIndOldNew() == (BooleanType.NO.getCode())){
				habrold.add(holderAccountBankGen);				
			}else{
				habrnew.add(holderAccountBankGen);
			}
		}
		holderAccountBankRequestOld = new GenericDataModel<HolderAccountBankRequest>(habrold);
		holderAccountBankRequestNew = new GenericDataModel<HolderAccountBankRequest>(habrnew);	
		
		List<HolderAccountFileRequest> hafrold = new ArrayList<HolderAccountFileRequest>();
		List<HolderAccountFileRequest> hafrnew = new ArrayList<HolderAccountFileRequest>();
		for(HolderAccountFileRequest holderAccountFileGen : holderAccountRequestHyFinded.getHolderAccountFilesRequest()){			
			if(holderAccountFileGen.getIndOldNew() == (BooleanType.NO.getCode())){
				hafrold.add(holderAccountFileGen);				
			}else{
				hafrnew.add(holderAccountFileGen);
			}
		}
		holderAccountFileRequestOld = new GenericDataModel<HolderAccountFileRequest>(hafrold);
		holderAccountFileRequestNew = new GenericDataModel<HolderAccountFileRequest>(hafrnew);	
		
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		if(holderAccountRequestHyFinded!=null && this.getEvaluateUserInfoIsActive()){
			if(this.getViewOperationType().equals(ViewOperationsType.CONSULT.getCode())){
			}else{
				switch(ViewOperationsType.get(this.getViewOperationType())){
					case CONFIRM:privilegeComponent.setBtnConfirmView(Boolean.TRUE);break;
					case APPROVE:privilegeComponent.setBtnApproveView(Boolean.TRUE);break;
					case ANULATE:privilegeComponent.setBtnAnnularView(Boolean.TRUE);break;
					case REJECT:privilegeComponent.setBtnRejectView(Boolean.TRUE);break;
				}
			}
		}
		userPrivilege.setPrivilegeComponent(privilegeComponent);
		this.setHolderAccountRequestHySession(holderAccountRequestHyFinded);
	}
	
	/**
	 * Before anulate action.
	 *
	 * @return the string
	 */
	public String beforeAnulateAction(){
		executeAction();
		hideLocalComponents();
		if(this.getHolderAccountRequestHySession()==null){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
			JSFUtilities.showValidationDialog();
			return ""; 
		}
		List<String> lstOperationUser = validateIsValidUser(holderAccountRequestHySession);
		if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_INVALID_USER_ANNUL,
							new Object[] { lstOperationUser.get(0).toString() }));
			JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		if(!this.getHolderAccountRequestHySession().getRequestState().equals(HolderAccountRequestHysStatusType.REGISTERED.getCode())){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_DOACTION_APROVATE_ANULATE, new Object[]{this.getHolderAccountRequestHySession().getStateHolderAccountReqDescription()});
			JSFUtilities.showValidationDialog();
			return "";
		}
		this.setViewOperationType(ViewOperationsType.ANULATE.getCode());
		readObjectFromBD(holderAccountRequestHySession);
		return "holderAccountModifyDetailRule";
	}
	
	/**
	 * Before approve action.
	 *
	 * @return the string
	 */
	public String beforeApproveAction(){
		executeAction();
		hideLocalComponents();
		if(this.getHolderAccountRequestHySession()==null){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
			JSFUtilities.showValidationDialog();
			return ""; 
		}
		List<String> lstOperationUser = validateIsValidUser(holderAccountRequestHySession);
		if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_INVALID_USER_APPROVE,
							new Object[] { lstOperationUser.get(0).toString() }));
			JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		if(!this.getHolderAccountRequestHySession().getRequestState().equals(HolderAccountRequestHysStatusType.REGISTERED.getCode())){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_DOACTION_APROVATE_ANULATE, new Object[]{this.getHolderAccountRequestHySession().getStateHolderAccountReqDescription()});
			JSFUtilities.showValidationDialog();
			return "";
		}
		this.setViewOperationType(ViewOperationsType.APPROVE.getCode());
		readObjectFromBD(holderAccountRequestHySession);
		return "holderAccountModifyDetailRule";
	}
	
	/**
	 * Before confirm action.
	 *
	 * @return the string
	 */
	public String beforeConfirmAction(){
		executeAction();
		hideLocalComponents();
		if(this.getHolderAccountRequestHySession()==null){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
			JSFUtilities.showValidationDialog();
			return ""; 
		}
		List<String> lstOperationUser = validateIsValidUser(holderAccountRequestHySession);
		if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_INVALID_USER_CONFIRM,
							new Object[] { lstOperationUser.get(0).toString() }));
			JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		if(!this.getHolderAccountRequestHySession().getRequestState().equals(HolderAccountRequestHysStatusType.APPROVE.getCode())){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_DOACTION_CONFIRM_REJECT, new Object[]{this.getHolderAccountRequestHySession().getStateHolderAccountReqDescription()});
			JSFUtilities.showValidationDialog();
			return "";
		}
		this.setViewOperationType(ViewOperationsType.CONFIRM.getCode());
		readObjectFromBD(holderAccountRequestHySession);
		return "holderAccountModifyDetailRule";
	}
	
	/**
	 * Before reject action.
	 *
	 * @return the string
	 */
	public String beforeRejectAction(){
		executeAction();
		hideLocalComponents();
		if(this.getHolderAccountRequestHySession()==null){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
			JSFUtilities.showValidationDialog();
			return ""; 
		}
		List<String> lstOperationUser = validateIsValidUser(holderAccountRequestHySession);
		if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_INVALID_USER_REJECT,
							new Object[] { lstOperationUser.get(0).toString() }));
			JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		if(!this.getHolderAccountRequestHySession().getRequestState().equals(HolderAccountRequestHysStatusType.APPROVE.getCode())){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_DOACTION_CONFIRM_REJECT, new Object[]{this.getHolderAccountRequestHySession().getStateHolderAccountReqDescription()});
			JSFUtilities.showValidationDialog();
			return "";
		}
		this.setViewOperationType(ViewOperationsType.REJECT.getCode());
		readObjectFromBD(holderAccountRequestHySession);
		return "holderAccountModifyDetailRule";
	}

	/**
	 * Validate is valid user.
	 *
	 * @param objHolderAccountRequest the obj holder account request
	 * @return the list
	 */
	private List<String> validateIsValidUser(
			HolderAccountRequest objHolderAccountRequest) {
		// TODO Auto-generated method stub
		List<OperationUserTO> lstOperationUserParam = new ArrayList<OperationUserTO>();
		List<String> lstOperationUserResult;
		OperationUserTO operationUserTO = new OperationUserTO();
		operationUserTO.setOperNumber(holderAccountRequestHySession.getIdHolderAccountReqPk().toString());
		operationUserTO.setUserName(holderAccountRequestHySession.getRegistryUser());
		lstOperationUserParam.add(operationUserTO);
		lstOperationUserResult = getIsSubsidiary(userInfo,lstOperationUserParam);
		return lstOperationUserResult;
	}
	
	/**
	 * Delete document.
	 */
	public void deleteDocumentPdd(){
		documentFileName = "";
		holderAccountRequestHySession.setDocumentPdd(null);
		holderAccountRequestHySession.setNameDocumentPdd(null);
//		JSFUtilities.putSessionMap("sessStreamedPhoto",null);
	}
	
	/**
	 * File upload handler.
	 *
	 * @param event the event
	 */
	public void fileUploadHandler(FileUploadEvent event){
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);				
		Object [] dataOnMessage = { event.getFile().getFileName(), event.getFile().getSize() / 1000 };
		showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, "adm.holderaccount.fupload.pdd", dataOnMessage);
		String fDisplayName = fUploadValidateFile(event.getFile(), null, null);
		if(fDisplayName != null){			 
			try {				
				documentFileName = event.getFile().getFileName();				
				holderAccountRequestHySession.setDocumentPdd(event.getFile().getContents());
				holderAccountRequestHySession.setNameDocumentPdd(documentFileName);
			} catch (Exception e) {
				e.printStackTrace();
			}
//			 JSFUtilities.showValidationDialog();
		 }
	}
	
	
	public StreamedContent getStreamedContentFile(){
		StreamedContent streamedContentFile = null;
		try {
			InputStream inputStream = null;
			inputStream = new ByteArrayInputStream(holderAccountRequestHySession.getDocumentPdd());
			streamedContentFile = new DefaultStreamedContent(inputStream, null,	documentFileName);
			inputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return streamedContentFile;
	}
	
	public StreamedContent getStreamedContentFilePDD(){
		StreamedContent streamedContentFile = null;
		try {
			InputStream inputStream = null;
			inputStream = new ByteArrayInputStream(holderAccountRequestHySession.getDocumentPdd());
			streamedContentFile = new DefaultStreamedContent(inputStream, null,	holderAccountRequestHySession.getNameDocumentPdd());
			inputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return streamedContentFile;
	}
	
	public StreamedContent getStreamedContentFileOriginal(){
		StreamedContent streamedContentFile = null;
		try {
			InputStream inputStream = null;
			inputStream = new ByteArrayInputStream(holderAccountRequestHySession.getDocumentPddOriginal());
			streamedContentFile = new DefaultStreamedContent(inputStream, null,	holderAccountRequestHySession.getNameDocumentPddOriginal());
			inputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return streamedContentFile;
	}
	
	public void changeIndPdd() {
		if(!holderAccountRequestHySession.isBlPdd()) {
			this.deleteDocumentPdd();
		}
	}
	/**
	 * Gets the current date.
	 *
	 * @return the current date
	 */
	public Date getCurrentDate() {
		return currentDate;
	}

	/**
	 * Sets the current date.
	 *
	 * @param currentDate the new current date
	 */
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}
	
	/**
	 * Close dialog reject motive.
	 */
	public void closeDialogRejectMotive(){
		JSFUtilities.resetComponent(":frmMotive:dialogMotive");
		hideLocalComponents();
	}
	
	/**
	 * Clean data model accoun modify.
	 */
	public void cleanDataModelAccounModify(){
		this.setHolderAccountRequestHyDataModel(null);
	}

	/**
	 * Gets the parameter table accouts type list.
	 *
	 * @return the parameter table accouts type list
	 */
	public List<ParameterTable> getParameterTableAccoutsTypeList() {
		return parameterTableAccoutsTypeList;
	}

	/**
	 * Sets the parameter table accouts type list.
	 *
	 * @param parameterTableAccoutsTypeList the new parameter table accouts type list
	 */
	public void setParameterTableAccoutsTypeList(
			List<ParameterTable> parameterTableAccoutsTypeList) {
		this.parameterTableAccoutsTypeList = parameterTableAccoutsTypeList;
	}

	/**
	 * Gets the parameter table to.
	 *
	 * @return the parameter table to
	 */
	public ParameterTableTO getParameterTableTO() {
		return parameterTableTO;
	}

	/**
	 * Sets the parameter table to.
	 *
	 * @param parameterTableTO the new parameter table to
	 */
	public void setParameterTableTO(ParameterTableTO parameterTableTO) {
		this.parameterTableTO = parameterTableTO;
	}

	/**
	 * Gets the parameter table account group list.
	 *
	 * @return the parameter table account group list
	 */
	public List<ParameterTable> getParameterTableAccountGroupList() {
		return parameterTableAccountGroupList;
	}

	/**
	 * Sets the parameter table account group list.
	 *
	 * @param parameterTableAccountGroupList the new parameter table account group list
	 */
	public void setParameterTableAccountGroupList(
			List<ParameterTable> parameterTableAccountGroupList) {
		this.parameterTableAccountGroupList = parameterTableAccountGroupList;
	}

	/**
	 * Gets the parameter table accounts type bank list.
	 *
	 * @return the parameter table accounts type bank list
	 */
	public List<ParameterTable> getParameterTableAccountsTypeBankList() {
		return parameterTableAccountsTypeBankList;
	}

	/**
	 * Sets the parameter table accounts type bank list.
	 *
	 * @param parameterTableAccountsTypeBankList the new parameter table accounts type bank list
	 */
	public void setParameterTableAccountsTypeBankList(
			List<ParameterTable> parameterTableAccountsTypeBankList) {
		this.parameterTableAccountsTypeBankList = parameterTableAccountsTypeBankList;
	}
	
	
	/**
	 * Gets the parameter table status list.
	 *
	 * @return the parameter table status list
	 */
	public List<ParameterTable> getParameterTableStatusList() {
		return parameterTableStatusList;
	}

	/**
	 * Sets the parameter table status list.
	 *
	 * @param parameterTableStatusList the new parameter table status list
	 */
	public void setParameterTableStatusList(
			List<ParameterTable> parameterTableStatusList) {
		this.parameterTableStatusList = parameterTableStatusList;
	}

	/**
	 * Gets the streamed content file holder Account history.
	 *
	 * @param holderAccountFileHy the holder account file hy
	 * @return the streamed content temporal file holder Account history
	 */
	public StreamedContent getStreamedContentFileHolderAccountModify(HolderAccountFile holderAccountFileHy) {				
		InputStream is = null;				
		byte[] fileAccount = new byte[]{};;
		try {
				fileAccount = holderAccountServiceFacade.getHolderAccountFileFacade(holderAccountFileHy.getIdHolderAccountFilePk());
		} catch (ServiceException e) {
				e.printStackTrace();
		}
		is = new ByteArrayInputStream(fileAccount);												
		return new DefaultStreamedContent(is,"application/pdf",holderAccountFileHy.getFilename());
	}

	/**
	 * Gets the attach file holder account selected.
	 *
	 * @return the attach file holder account selected
	 */
	public Integer getAttachFileHolderAccountSelected() {
		return attachFileHolderAccountSelected;
	}

	/**
	 * Sets the attach file holder account selected.
	 *
	 * @param attachFileHolderAccountSelected the new attach file holder account selected
	 */
	public void setAttachFileHolderAccountSelected(
			Integer attachFileHolderAccountSelected) {
		this.attachFileHolderAccountSelected = attachFileHolderAccountSelected;
	}

	/**
	 * Gets the account holder file name display.
	 *
	 * @return the account holder file name display
	 */
	public String getAccountHolderFileNameDisplay() {
		return accountHolderFileNameDisplay;
	}

	/**
	 * Sets the account holder file name display.
	 *
	 * @param accountHolderFileNameDisplay the new account holder file name display
	 */
	public void setAccountHolderFileNameDisplay(String accountHolderFileNameDisplay) {
		this.accountHolderFileNameDisplay = accountHolderFileNameDisplay;
	}	
	
	/**
	 * Value change cmb document holder.
	 */
	public void valueChangeCmbDocumentHolderAccount(){
		accountHolderFileNameDisplay = null;
	}
	
	/**
	 * Document attach holder account request file.
	 *
	 * @param event the event
	 */
	public void documentAttachHolderAccountRequestFile(FileUploadEvent event){
		if(attachFileHolderAccountSelected!=null && !attachFileHolderAccountSelected.equals(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER))){						
			 for(int i=0;i<parameterTableDocumentsList.size();i++){
				if(parameterTableDocumentsList.get(i).getParameterTablePk().equals(attachFileHolderAccountSelected)){					
					HolderAccountFileRequest holderAccountReqFileHySend =  new HolderAccountFileRequest();
					holderAccountReqFileHySend.setDocumentType(parameterTableDocumentsList.get(i).getParameterTablePk());
					holderAccountReqFileHySend.setDocumentTypeDesc(parameterTableDocumentsList.get(i).getDescription());
					holderAccountReqFileHySend.setIndMain(parameterTableDocumentsList.get(i).getShortInteger());
					holderAccountReqFileHySend.setFilename(event.getFile().getFileName());
					holderAccountReqFileHySend.setDocumentFile(event.getFile().getContents());
					holderAccountReqFileHySend.setIndOldNew(BooleanType.YES.getCode());
					holderAccountReqFileHySend.setRegistryUser(userInfo.getUserAccountSession().getUserName());
					holderAccountReqFileHySend.setRegistryDate(CommonsUtilities.currentDate());
					holderAccountReqFileHySend.setHolderAccountRequest(holderAccountRequestHySession);										
					holderAccountReqFileHySend.setWasModified(true);
					insertOrReplaceHolderAccountRequestFileItem(holderAccountReqFileHySend);
				}
			}
		}
	}
	
	/**
	 * Insert or replace holder account request file item.
	 *
	 * @param holderAccountReqFileHySend the holder account req file hy send
	 */
	public void insertOrReplaceHolderAccountRequestFileItem(HolderAccountFileRequest holderAccountReqFileHySend){		
		boolean flag = false;		
		int listSize = holderAccountRequestHySession.getHolderAccountFilesRequest().size();
		if(listSize>0){
			for(int i=0;i<listSize;i++)
			{			
				if(holderAccountRequestHySession.getHolderAccountFilesRequest().get(i).getDocumentType().equals(holderAccountReqFileHySend.getDocumentType())){
					holderAccountRequestHySession.getHolderAccountFilesRequest().remove(i);
					holderAccountRequestHySession.getHolderAccountFilesRequest().add(i,holderAccountReqFileHySend);
					this.setAttachFileHolderAccountSelected(null);							
					holderAccountFileRequestTemp.remove(i);
					holderAccountFileRequestTemp.add(i,holderAccountReqFileHySend);					
					flag = true;
				}	
			}
			if(!flag){
				holderAccountRequestHySession.getHolderAccountFilesRequest().add(holderAccountReqFileHySend);				
				this.setAttachFileHolderAccountSelected(null);
				this.holderAccountFileRequestTemp.add(holderAccountReqFileHySend);
			}
		}
		else{
			holderAccountRequestHySession.getHolderAccountFilesRequest().add(holderAccountReqFileHySend);
			this.setAttachFileHolderAccountSelected(null);
			this.holderAccountFileRequestTemp.add(holderAccountReqFileHySend);
		}		
	}
	
	/**
	 * Gets the generate file holder account modify report.
	 *
	 * @param holderAccountRequest the holder account request
	 * @param nameParam the name param
	 * @return the generate file holder account modify report
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void getGenerateFileHolderAccountModifyReport(HolderAccountRequest holderAccountRequest,String nameParam) throws ServiceException{
		Long holderAccountRequestId = holderAccountRequest.getIdHolderAccountReqPk();	
		this.serviceAPIClients.generateReportFileAccountModification(holderAccountRequestId, nameParam);
			
		try {
			Thread.sleep(5000);
		} catch (InterruptedException ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		this.searchHolderAccountModifyRequestHysHandler();
	}
	
	/**
	 * Checks if is not state confirm.
	 *
	 * @param state the state
	 * @return the boolean
	 */
	public Boolean isNotStateConfirm(Integer state){
		if(state.equals(HolderAccountRequestHysStatusType.CONFIRM.getCode())){
			return false;
		}
		return true;
	}
	
	/**
	 * Gets the visible file holder account modify report.
	 *
	 * @param holderAccountRequest the holder account request
	 * @return the visible file holder account modify report
	 */
	public Boolean getVisibleFileHolderAccountModifyReport(HolderAccountRequest holderAccountRequest){
		String nameReport = GeneralConstants.HOLDER_ACCOUNT_MODIFY_REPORT;
		Long holderAccountRequestId = holderAccountRequest.getIdHolderAccountReqPk();	
		ReportLoggerFile reportLoggerFile = holderServiceFacade.findModifyReport(holderAccountRequestId, nameReport);
		Boolean bol = false;
			if(Validations.validateIsNotNull(reportLoggerFile)){ 
				bol = true;
			}
		return bol;
	}
	
	/**
	 * Gets the streamed content temporal file holder account modify report.
	 *
	 * @param holderAccountRequest the holder account request
	 * @return the streamed content temporal file holder account modify report
	 * @throws ServiceException the service exception
	 */
	public StreamedContent getStreamedContentTemporalFileHolderAccountModifyReport(HolderAccountRequest holderAccountRequest)throws ServiceException{
		String nameReport = GeneralConstants.HOLDER_ACCOUNT_MODIFY_REPORT;
		Long holderAccountRequestId = holderAccountRequest.getIdHolderAccountReqPk();	
		ReportLoggerFile reportLoggerFile = holderServiceFacade.findModifyReport(holderAccountRequestId,nameReport);
		if(reportLoggerFile!= null){
			Path pathFile = Paths.get(reportLoggerFile.getNameTrace(),reportLoggerFile.getNameFile());
			if(Files.exists(pathFile)){
				try{
				Faces.sendFile(Files.readAllBytes(pathFile), reportLoggerFile.getNameFile(), true);
				} catch(IOException iox){
//					log.error(iox.getMessage());
				}
			}
		}
		return null;
	}	
	
	/**
	 * Gets the streamed content temporal file holder Account history.
	 *
	 * @param holderAccountReqFileHy the holder account req file hy
	 * @return the streamed content temporal file holder Account history
	 */
	public StreamedContent getStreamedContentTemporalFileHolderReqAccountHys(HolderAccountFileRequest holderAccountReqFileHy) {				
		InputStream is = null;					
		is = new ByteArrayInputStream(holderAccountReqFileHy.getDocumentFile());										
		return new DefaultStreamedContent(is,"application/pdf",holderAccountReqFileHy.getFilename());
	}
	
	/**
	 * Removes the holder Account request file.
	 *
	 * @param holderAccountReqFileHy the holder account req file hy
	 */
	public void removeHolderAccountRequestFileAdd(HolderAccountFileRequest holderAccountReqFileHy) {
		holderAccountRequestHySession.getHolderAccountFilesRequest().remove(holderAccountReqFileHy);
		accountHolderFileNameDisplay = null;
		this.setAttachFileHolderAccountSelected(null);		
		holderAccountFileRequestTemp.remove(holderAccountReqFileHy);
	}

	/**
	 * Gets the holder account detail temp.
	 *
	 * @return the holder account detail temp
	 */
	public List<HolderAccountDetRequest> getHolderAccountDetailTemp() {
		return holderAccountDetailTemp;
	}

	/**
	 * Sets the holder account detail temp.
	 *
	 * @param holderAccountDetailTemp the new holder account detail temp
	 */
	public void setHolderAccountDetailTemp(List<HolderAccountDetRequest> holderAccountDetailTemp) {
		this.holderAccountDetailTemp = holderAccountDetailTemp;
	}

	/**
	 * Gets the holder account file request temp.
	 *
	 * @return the holder account file request temp
	 */
	public List<HolderAccountFileRequest> getHolderAccountFileRequestTemp() {
		return holderAccountFileRequestTemp;
	}

	/**
	 * Sets the holder account file request temp.
	 *
	 * @param holderAccountFileRequestTemp the new holder account file request temp
	 */
	public void setHolderAccountFileRequestTemp(
			List<HolderAccountFileRequest> holderAccountFileRequestTemp) {
		this.holderAccountFileRequestTemp = holderAccountFileRequestTemp;
	}
	
	/**
	 * Gets the holder account bank request temp.
	 *
	 * @return the holder account bank request temp
	 */
	public List<HolderAccountBankRequest> getHolderAccountBankRequestTemp() {
		return holderAccountBankRequestTemp;
	}

	/**
	 * Sets the holder account bank request temp.
	 *
	 * @param holderAccountBankRequestTemp the new holder account bank request temp
	 */
	public void setHolderAccountBankRequestTemp(
			List<HolderAccountBankRequest> holderAccountBankRequestTemp) {
		this.holderAccountBankRequestTemp = holderAccountBankRequestTemp;
	}

	/**
	 * Gets the holder account det request old.
	 *
	 * @return the holder account det request old
	 */
	public GenericDataModel<HolderAccountDetRequest> getHolderAccountDetRequestOld() {
		return holderAccountDetRequestOld;
	}

	/**
	 * Sets the holder account det request old.
	 *
	 * @param holderAccountDetRequestOld the new holder account det request old
	 */
	public void setHolderAccountDetRequestOld(
			GenericDataModel<HolderAccountDetRequest> holderAccountDetRequestOld) {
		this.holderAccountDetRequestOld = holderAccountDetRequestOld;
	}

	/**
	 * Gets the holder account det request new.
	 *
	 * @return the holder account det request new
	 */
	public GenericDataModel<HolderAccountDetRequest> getHolderAccountDetRequestNew() {
		return holderAccountDetRequestNew;
	}

	/**
	 * Sets the holder account det request new.
	 *
	 * @param holderAccountDetRequestNew the new holder account det request new
	 */
	public void setHolderAccountDetRequestNew(
			GenericDataModel<HolderAccountDetRequest> holderAccountDetRequestNew) {
		this.holderAccountDetRequestNew = holderAccountDetRequestNew;
	}

	/**
	 * Gets the holder account bank request old.
	 *
	 * @return the holder account bank request old
	 */
	public GenericDataModel<HolderAccountBankRequest> getHolderAccountBankRequestOld() {
		return holderAccountBankRequestOld;
	}

	/**
	 * Sets the holder account bank request old.
	 *
	 * @param holderAccountBankRequestOld the new holder account bank request old
	 */
	public void setHolderAccountBankRequestOld(
			GenericDataModel<HolderAccountBankRequest> holderAccountBankRequestOld) {
		this.holderAccountBankRequestOld = holderAccountBankRequestOld;
	}

	/**
	 * Gets the holder account bank request new.
	 *
	 * @return the holder account bank request new
	 */
	public GenericDataModel<HolderAccountBankRequest> getHolderAccountBankRequestNew() {
		return holderAccountBankRequestNew;
	}

	/**
	 * Sets the holder account bank request new.
	 *
	 * @param holderAccountBankRequestNew the new holder account bank request new
	 */
	public void setHolderAccountBankRequestNew(
			GenericDataModel<HolderAccountBankRequest> holderAccountBankRequestNew) {
		this.holderAccountBankRequestNew = holderAccountBankRequestNew;
	}

	/**
	 * Gets the holder account file request old.
	 *
	 * @return the holder account file request old
	 */
	public GenericDataModel<HolderAccountFileRequest> getHolderAccountFileRequestOld() {
		return holderAccountFileRequestOld;
	}

	/**
	 * Sets the holder account file request old.
	 *
	 * @param holderAccountFileRequestOld the new holder account file request old
	 */
	public void setHolderAccountFileRequestOld(
			GenericDataModel<HolderAccountFileRequest> holderAccountFileRequestOld) {
		this.holderAccountFileRequestOld = holderAccountFileRequestOld;
	}

	/**
	 * Gets the holder account file request new.
	 *
	 * @return the holder account file request new
	 */
	public GenericDataModel<HolderAccountFileRequest> getHolderAccountFileRequestNew() {
		return holderAccountFileRequestNew;
	}

	/**
	 * Sets the holder account file request new.
	 *
	 * @param holderAccountFileRequestNew the new holder account file request new
	 */
	public void setHolderAccountFileRequestNew(
			GenericDataModel<HolderAccountFileRequest> holderAccountFileRequestNew) {
		this.holderAccountFileRequestNew = holderAccountFileRequestNew;
	}		
	
	/**
	 * Gets the streamed content temporal file holder Account history.
	 *
	 * @param holderAccountReqFileHy the holder account req file hy
	 * @return the streamed content temporal file holder Account history
	 */
	public StreamedContent getStreamedContentFileRequest(HolderAccountFileRequest holderAccountReqFileHy) {				
		InputStream is = null;				
		byte[] fileAccount = null;
		try {
			fileAccount = holderAccountServiceFacade.getHolderAccountReqFileFacade(holderAccountReqFileHy.getIdHolderAccountReqFilePk());
			is = new ByteArrayInputStream(fileAccount);
		} catch (ServiceException e) {
				e.printStackTrace();
		}														
		return new DefaultStreamedContent(is,"application/pdf",holderAccountReqFileHy.getFilename());
	}
	
	/**
	 * Gets the holder request.
	 * 
	 * @return the holder request
	 */
	public Holder getHolderRequest() {
		return holderRequest;
	}

	/**
	 * Sets the holder request.
	 * 
	 * @param holderRequest
	 *            the new holder request
	 */
	public void setHolderRequest(Holder holderRequest) {
		this.holderRequest = holderRequest;
	}
	
	public void alert(String message) {
		String header = PropertiesUtilities.getGenericMessage(PropertiesConstants.MESSAGES_ALERT);
		showMessageOnDialog(header, message);
		JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
		JSFUtilities.updateComponent("opnlDialogs");
	}
	
	/**
	 * Search holder handler.
	 */
	public void searchHolderHandler(){
		try{
		executeAction();
		JSFUtilities.hideGeneralDialogues(); 
		cleanHolderAccountEntered();
		holderAccounts = null;
		
		if(holderRequest.getIdHolderPk() != null && Validations.validateIsPositiveNumber(holderRequest.getIdHolderPk().intValue())){
			
			// Get holder from database *.*
			Holder holder = null;
			try {
				holder = holderServiceFacade.getHolderServiceFacade(this.getHolderRequest().getIdHolderPk());
			holder.setFullName(holder.getDescriptionHolder());
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (Validations.validateIsNull(holder)) {
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_HOLDERNULL,
					new Object[] { String.valueOf(this.getHolderRequest().getIdHolderPk()) });
				this.setHolderRequest(new Holder());
				JSFUtilities.showSimpleValidationDialog();
				return;
				}
			if (!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())) {
				alert(PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_HOLDER_BLOCK));		
//						showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,	null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_HOLDER_BLOCK,
//								new Object[] { String.valueOf(this.getHolderRequest().getIdHolderPk()) });
//						this.setHolderRequest(new Holder());
//						JSFUtilities.showSimpleValidationDialog();
				this.setHolderRequest(new Holder());
				return;
			}
			
			//Validating if holder is not equal null and Holder State is registered
			if (Validations.validateIsNotNull(holderRequest.getIdHolderPk())) {
				// Validating if participant is not null
				if (holderAccountTO.getParticipantTO()!=null) {					
					// Searching holder accounts by participant
					HolderTO holderTO = new HolderTO();
					if(holder.getIdHolderPk()!=null){
						holderTO.setHolderId(holder.getIdHolderPk());
					}
						holderTO.setFlagAllHolders(true);
						holderTO.setParticipantFk(holderAccountTO.getParticipantTO());
					List<HolderAccountHelperResultTO> lstAccountTo = new ArrayList<HolderAccountHelperResultTO>();
					lstAccountTo = helperComponentFacade.searchAccountByFilter(holderTO);					
					
					if(lstAccountTo!=null && lstAccountTo.size()>0){
						holderAccounts = new ArrayList<HolderAccount>();
						HolderAccount ha;
						for(HolderAccountHelperResultTO hahrTO : lstAccountTo){
							ha = new HolderAccount();							
							ha.setAccountNumber(hahrTO.getAccountNumber());
							ha.setAlternateCode(hahrTO.getAlternateCode());
							ha.setAccountGroup(hahrTO.getAccountGroup());
							ha.setIdHolderAccountPk(hahrTO.getAccountPk());
							ha.setHolderDescriptionList(hahrTO.getHolderDescriptionList());
							ha.setStateAccountDescription(hahrTO.getAccountStatusDescription());
							ha.setRelatedName(hahrTO.getRelatedName());
							holderAccounts.add(ha);
						}
						
					}
					if(holderAccounts != null){
						if (holderAccounts.size() > 0) {
							if(holderAccounts.size()==1){
								holderAccountSession=holderAccounts.get(0);
								searchHolderAccountModifyHanlder();
							}
						} else {
							//Calling Modal
							JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.ERROR_ON_ACCOUNT_AND_PARTICIPANT, null);
							JSFUtilities.showSimpleValidationDialog();
							holderRequest = new Holder();
						}
					}else{
						holderRequest = new Holder();
						alert(PropertiesUtilities.getMessage(PropertiesConstants.ERROR_HOLDER_WITH_NOT_ACCOUNT));	
						//JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.ERROR_HOLDER_WITH_NOT_ACCOUNT, null);
						//JSFUtilities.showSimpleValidationDialog();
					}									
				} 
			 
			} else if (!Validations.validateIsNotNull(holder)) {
				holderRequest = new Holder();			
			}
		}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Go to.
	 *
	 * @param viewToGo the view to go
	 * @return the string
	 */
	public String goTo(String viewToGo) {
		cleanHolderAccountTO();
		return viewToGo;
	}

	/**
	 * Gets the f upload file size mgmt.
	 *
	 * @return the f upload file size mgmt
	 */
	public String getfUploadFileSizeMgmt() {
		return fUploadFileSizeMgmt;
	}

	/**
	 * Sets the f upload file size mgmt.
	 *
	 * @param fUploadFileSizeMgmt the new f upload file size mgmt
	 */
	public void setfUploadFileSizeMgmt(String fUploadFileSizeMgmt) {
		this.fUploadFileSizeMgmt = fUploadFileSizeMgmt;
	}

	/**
	 * Gets the f upload file size display mgmt.
	 *
	 * @return the f upload file size display mgmt
	 */
	public String getfUploadFileSizeDisplayMgmt() {
		return fUploadFileSizeDisplayMgmt;
	}

	/**
	 * Sets the f upload file size display mgmt.
	 *
	 * @param fUploadFileSizeDisplayMgmt the new f upload file size display mgmt
	 */
	public void setfUploadFileSizeDisplayMgmt(String fUploadFileSizeDisplayMgmt) {
		this.fUploadFileSizeDisplayMgmt = fUploadFileSizeDisplayMgmt;
	}

	/**
	 * Gets the file download.
	 *
	 * @return the file download
	 */
	public StreamedContent getFileDownload() {
		return fileDownload;
	}

	/**
	 * Sets the file download.
	 *
	 * @param fileDownload the new file download
	 */
	public void setFileDownload(StreamedContent fileDownload) {
		this.fileDownload = fileDownload;
	}

	/**
	 * Gets the user privilege.
	 *
	 * @return the user privilege
	 */
	public UserPrivilege getUserPrivilege() {
		return userPrivilege;
	}

	/**
	 * Sets the user privilege.
	 *
	 * @param userPrivilege the new user privilege
	 */
	public void setUserPrivilege(UserPrivilege userPrivilege) {
		this.userPrivilege = userPrivilege;
	}

	/**
	 * Gets the holder accounts.
	 *
	 * @return the holder accounts
	 */
	public List<HolderAccount> getHolderAccounts() {
		return holderAccounts;
	}

	/**
	 * Sets the holder accounts.
	 *
	 * @param holderAccounts the new holder accounts
	 */
	public void setHolderAccounts(List<HolderAccount> holderAccounts) {
		this.holderAccounts = holderAccounts;
	}

	/**
	 * Checks if is flag exist participant dpf.
	 *
	 * @return the flagExistParticipantDPF
	 */
	public boolean isFlagExistParticipantDPF() {
		return flagExistParticipantDPF;
	}

	/**
	 * Sets the flag exist participant dpf.
	 *
	 * @param flagExistParticipantDPF the flagExistParticipantDPF to set
	 */
	public void setFlagExistParticipantDPF(boolean flagExistParticipantDPF) {
		this.flagExistParticipantDPF = flagExistParticipantDPF;
	}

	public String getPdfFileUploadFiltTypes() {
		return pdfFileUploadFiltTypes;
	}

	public void setPdfFileUploadFiltTypes(String pdfFileUploadFiltTypes) {
		this.pdfFileUploadFiltTypes = pdfFileUploadFiltTypes;
	}
	
	public String getDocumentFileName() {
		return documentFileName;
	}

	public void setDocumentFileName(String documentFileName) {
		this.documentFileName = documentFileName;
	}

	
	
}