/*
 * 
 */
package com.pradera.accounts.holderaccount.view;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.accounts.holder.facade.HolderServiceFacade;
import com.pradera.accounts.holderaccount.facade.HolderAccountServiceFacade;
import com.pradera.accounts.participants.facade.ParticipantServiceFacade;
import com.pradera.accounts.participants.to.MotiveController;
import com.pradera.accounts.util.view.PropertiesConstants;
import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.sessionuser.OperationUserTO;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.BackDialogController;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountBankRequest;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetRequest;
import com.pradera.model.accounts.holderaccounts.HolderAccountFileRequest;
import com.pradera.model.accounts.holderaccounts.HolderAccountRequest;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountGroupType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestHysStatusType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestHysType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountType;
import com.pradera.model.accounts.type.BlockedFieldsHolderAccountType;
import com.pradera.model.accounts.type.EconomicSectorType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera. Copyright 2013.</li>
 * </ul>
 * 
 * The Class HolderAccountMgmtBean.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 18/02/2013
 */
@DepositaryWebBean
public class HolderAccountMgmtBean extends GenericBaseBean  implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The country residence. */
	@Inject
	@Configurable
	Integer countryResidence;

	/** The participant list. */
	private List<Participant> participantList;
	/** The participant list. */

	/** The Constant TAMANIO_BANK_ACCOUNTS. */
	private  Integer TAMANIO_BANK_ACCOUNTS = 2;	

	/** The Constant ERROR_REJECT. */
	private static final long ERROR_REJECT = 482;

	/** The Constant ERROR_ANULATE. */
	private static final long ERROR_ANULATE = 486;

	/** The bank list. */
	private List<Bank> bankList;

	/** The holder account request session. */
	private HolderAccountRequest holderAccountRequestHySession;

	/** The holder account bank request. */
	private HolderAccountBankRequest holderAccountReqBankHyRequest;

	/** The holder request. */
	private Holder holderRequest;

	/** The parameter table to. */
	private ParameterTableTO parameterTableTO;

	/** The parameter table money type list. */
	private List<ParameterTable> parameterTableMoneyTypeList;

	/** The parameter table accounts type list. */
	private List<ParameterTable> parameterTableAccoutsTypeList;

	/** The parameter table documents list. */
	private List<ParameterTable> parameterTableDocumentsList;

	/** The parameter table account group list. */
	private List<ParameterTable> parameterTableAccountGroupList;
	
	/** The parameter table status list. */
	private List<ParameterTable> parameterTableStatusList;
	
	/** The parameter table status list. */
	private List<ParameterTable> parameterTableAccountsTypeBankList;

	/** The holder account to. */
	private HolderAccountTO holderAccountTO;

	/** The holder account request data model. */
	private GenericDataModel<HolderAccountRequest> holderAccountRequestHyDataModel;

	/** The motive controller. */
	private MotiveController motiveController;

	/** The back dialog controller. */
	private BackDialogController backDialogController;

	/** The holder account service facade. */
	@EJB
	private HolderAccountServiceFacade holderAccountServiceFacade;

	/** The holder service facade. */
	@EJB
	private HolderServiceFacade holderServiceFacade;

	/** The general parameters facade. */
	@EJB
	private GeneralParametersFacade generalParametersFacade;

	/** The participant service facade. */
	@EJB
	private ParticipantServiceFacade participantServiceFacade;

	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;

	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	private UserPrivilege userPrivilege;
	
	/** The current date. */
	private Date currentDate = new Date();
	
	/** The attach file holder account selected. */
	private Integer attachFileHolderAccountSelected;
	
	/** The Account holder file name display. */
	private String accountHolderFileNameDisplay;		
	
	/** The Constant SEARCH_ACTION_ACCOUNT_MGMT. */
	private static final String SEARCH_ACTION_ACCOUNT_MGMT = "searchHolderAccountMgmt";
	
	/** The upload file size mgmt. */
	private String fUploadFileSizeMgmt="3000000";
	
	/** The upload file size display mgmt. */
	private String fUploadFileSizeDisplayMgmt="3000";
	
	/** The flag exist participant dpf. */
	private boolean flagExistParticipantDPF;
	
	private String pdfFileUploadFiltTypes="doc|docx|xls|xlsx|pdf";
	
	private String documentFileName;
	
	private Boolean blockPdd;


	/**
	 * Init the.
	 */
	@PostConstruct
	public void init() {
		beginConversation();
		Participant participantTO = new Participant();
		motiveController = new MotiveController();
		this.setViewOperationType(ViewOperationsType.CONSULT.getCode());
		participantTO.setState(ParticipantStateType.REGISTERED.getCode());
		holderAccountRequestHySession = new HolderAccountRequest();
		holderAccountRequestHySession.setHolderAccountDetRequest(new ArrayList<HolderAccountDetRequest>());
		holderAccountRequestHySession.setHolderAccountBankRequest(new ArrayList<HolderAccountBankRequest>());
		holderAccountRequestHySession.setHolderAccountFilesRequest(new ArrayList<HolderAccountFileRequest>());		
		holderRequest = new Holder();
		holderAccountReqBankHyRequest = new HolderAccountBankRequest();
		holderAccountReqBankHyRequest.setBank(new Bank());
		parameterTableTO = new ParameterTableTO();				
		holderAccountTO = new HolderAccountTO();
		holderAccountTO.setInitialDate(CommonsUtilities.currentDate());
		holderAccountTO.setFinalDate(CommonsUtilities.currentDate());
		parameterTableTO.setState(BooleanType.YES.getCode());
		try {
			this.setBankList(holderAccountServiceFacade.getBankListServiceFacade());	
			parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			this.setParameterTableMoneyTypeList(generalParametersFacade.getListParameterTableServiceBean(this.getParameterTableTO()));
			TAMANIO_BANK_ACCOUNTS = new Integer(parameterTableMoneyTypeList.size());
			parameterTableTO.setMasterTableFk(MasterTableType.ACCOUNTS_TYPE.getCode());			
			this.setParameterTableAccoutsTypeList(generalParametersFacade.getListParameterTableServiceBean(this.getParameterTableTO()));
			parameterTableTO.setMasterTableFk(MasterTableType.ACCOUNT_GROUP_TYPE.getCode());			
			this.setParameterTableAccountGroupList(generalParametersFacade.getListParameterTableServiceBean(this.getParameterTableTO()));			
			parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_REQUEST_STATUS.getCode());			
			this.setParameterTableStatusList(generalParametersFacade.getListParameterTableServiceBean(this.getParameterTableTO()));			
			parameterTableTO.setMasterTableFk(MasterTableType.MASTER_TABLE_BANK_ACCOUNT_TYPE.getCode());			
			this.setParameterTableAccountsTypeBankList(generalParametersFacade.getListParameterTableServiceBean(this.getParameterTableTO()));			
			this.setParticipantList(accountsFacade.getLisParticipantServiceBean(participantTO));
			this.getMotiveController().setMotiveList(generalParametersFacade.getListParameterTableServiceBean(parameterTableTO.setParaterTablePkFromMaster(MasterTableType.HOLDER_ACCOUNT_REQUEST_ANNULAR_MOTIVE)),MasterTableType.HOLDER_ACCOUNT_REQUEST_ANNULAR_MOTIVE);
			this.getMotiveController().setMotiveList(generalParametersFacade.getListParameterTableServiceBean(parameterTableTO.setParaterTablePkFromMaster(MasterTableType.HOLDER_ACCOUNT_REQUEST_REJECT_MOTIVE)),MasterTableType.HOLDER_REQUEST_REJECT_MOTIVE);
			loadIndPdd();
			loadCboDocumentsList();			
			loadingDocumentAttachByAccountHolder();
		} catch (Exception e) {
			e.printStackTrace();
		}
		loadPrivilegesUser();
		evaluateUserSession();
		evaluateUserInfoActive();
		loadHolidays();
		accountHolderFileNameDisplay=GeneralConstants.EMPTY_STRING;	
		attachFileHolderAccountSelected = GeneralConstants.NEGATIVE_ONE_INTEGER;
	}
	
	
	public void loadIndPdd() {
		parameterTableTO.setMasterTableFk(MasterTableType.STATE_INDPDD.getCode());
		try {
			for (ParameterTable parameter : generalParametersFacade.getListParameterTableServiceBean(this.parameterTableTO)) {
				if (parameter.getShortInteger() == BlockedFieldsHolderAccountType.BLOCKED.getCode()) {
					holderAccountRequestHySession.setBlPdd(BooleanType.YES.getBooleanValue());
					blockPdd = BooleanType.YES.getBooleanValue();
					}else {
						holderAccountRequestHySession.setBlPdd(BooleanType.NO.getBooleanValue());
						blockPdd = BooleanType.NO.getBooleanValue();
					}
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Load holidays.
	 */
	private void loadHolidays() {
		Holiday holidayFilter = new Holiday();
		holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		GeographicLocation countryFilter = new GeographicLocation();
		countryFilter.setIdGeographicLocationPk(countryResidence);
		holidayFilter.setGeographicLocation(countryFilter);
		try {
			allHolidays = CommonsUtilities.convertListDateToString(generalParametersFacade.getListHolidayDateServiceFacade(holidayFilter));
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Load Privileges User.
	 */
	public void loadPrivilegesUser() {
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnApproveView(Boolean.TRUE);
		privilegeComponent.setBtnAnnularView(Boolean.TRUE);
		privilegeComponent.setBtnConfirmView(Boolean.TRUE);
		privilegeComponent.setBtnRejectView(Boolean.TRUE);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
		userPrivilege.getPrivilegeComponent();
	}

	/**
	 * Evaluate user session. method to know if user is Participant Or EDV
	 */
	private void evaluateUserSession() {
		if (userInfo.getUserAccountSession().isParticipantInstitucion() ||
				userInfo.getUserAccountSession().isParticipantInvestorInstitucion()) {
			Long participantCode = userInfo.getUserAccountSession().getParticipantCode();
			holderAccountRequestHySession.setParticipant(new Participant(participantCode));
			holderAccountTO.setParticipantTO(participantCode);
		}else if (userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
			Long participantDpf = getIssuerDpfInstitution(userInfo);
			if(Validations.validateIsNotNullAndNotEmpty(participantDpf)){
				flagExistParticipantDPF = true;
				holderAccountRequestHySession.setParticipant(new Participant(participantDpf));
				holderAccountTO.setParticipantTO(participantDpf);
			}else{
				flagExistParticipantDPF = false;
				holderAccountTO.setParticipantTO(null);
			}
		}
	}
	
	/**
	 * Evaluate user info active.
	 */
	private void evaluateUserInfoActive() {
		if (userInfo.getUserAccountSession().isParticipantInstitucion()) {
			Participant participantTO = new Participant(userInfo.getUserAccountSession().getParticipantCode());
			try {
				List<Participant> participantList = accountsFacade.getLisParticipantServiceBean(participantTO);
				if (participantList != null && !participantList.isEmpty()) {
					participantTO = participantList.get(0);
					if (this.participantList.contains(participantTO)) {
						userInfo.getUserAccountSession().setInstitutionState(ParticipantStateType.REGISTERED.getCode());
					} else {
						this.participantList.addAll(participantList);
						userInfo.getUserAccountSession().setInstitutionState(participantTO.getState());
					}
				}
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Load BlPdd.
	 */
	public Boolean BlPdd() {
		Boolean blPdd = BooleanType.NO.getBooleanValue();
		parameterTableTO.setMasterTableFk(MasterTableType.STATE_INDPDD.getCode());
		try {
			for (ParameterTable parameter : generalParametersFacade.getListParameterTableServiceBean(this.parameterTableTO)) {
			if (parameter.getShortInteger() == BlockedFieldsHolderAccountType.BLOCKED.getCode()) {
				holderAccountRequestHySession.setBlPdd(BooleanType.YES.getBooleanValue());
				blPdd = BooleanType.YES.getBooleanValue();
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return blPdd;
	}

	/**
	 * Load combo documents list.
	 */
	public void loadCboDocumentsList() {
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_FILES.getCode());
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		try {
			parameterTableDocumentsList = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Load document attach.
	 */
	public void loadingDocumentAttachByAccountHolder() {	  		
		accountHolderFileNameDisplay=GeneralConstants.EMPTY_STRING;		
	}

	/**
	 * Instantiates a new holder account bean.
	 */
	public HolderAccountMgmtBean() {
		super();
	}

	/**
	 * Gets the motive controller.
	 * 
	 * @return the motive controller
	 */
	public MotiveController getMotiveController() {
		return motiveController;
	}

	/**
	 * Sets the motive controller.
	 * 
	 * @param motiveController
	 *            the new motive controller
	 */
	public void setMotiveController(MotiveController motiveController) {
		this.motiveController = motiveController;
	}

	/**
	 * Gets the holder account bank request.
	 * 
	 * @return the holder account bank request
	 */
	public HolderAccountBankRequest getHolderAccountReqBankHyRequest() {
		return holderAccountReqBankHyRequest;
	}

	/**
	 * Sets the holder account bank request.
	 * 
	 * @param holderAccountReqBankHyRequest
	 *            the new holder account bank request
	 */
	public void setHolderAccountReqBankHyRequest(HolderAccountBankRequest holderAccountReqBankHyRequest) {
		this.holderAccountReqBankHyRequest = holderAccountReqBankHyRequest;
	}

	/**
	 * Gets the parameter table to.
	 * 
	 * @return the parameter table to
	 */
	public ParameterTableTO getParameterTableTO() {
		return parameterTableTO;
	}

	/**
	 * Sets the parameter table to.
	 * 
	 * @param parameterTableTO
	 *            the new parameter table to
	 */
	public void setParameterTableTO(ParameterTableTO parameterTableTO) {
		this.parameterTableTO = parameterTableTO;
	}

	/**
	 * Gets the bank list.
	 * 
	 * @return the bank list
	 */
	public List<Bank> getBankList() {
		return bankList;
	}

	/**
	 * Sets the bank list.
	 * 
	 * @param bankList
	 *            the new bank list
	 */
	public void setBankList(List<Bank> bankList) {
		this.bankList = bankList;
	}
	
	/**
	 * Gets the parameter table accounts type list.
	 * 
	 * @return the parameter table accounts type list
	 */
	public List<ParameterTable> getParameterTableAccoutsTypeList() {
		return parameterTableAccoutsTypeList;
	}

	/**
	 * Sets the parameter table accounts type list.
	 * 
	 * @param parameterTableAccoutsTypeList
	 *            the new parameter table accounts type list
	 */
	public void setParameterTableAccoutsTypeList(List<ParameterTable> parameterTableAccoutsTypeList) {
		this.parameterTableAccoutsTypeList = parameterTableAccoutsTypeList;
	}	

	/**
	 * Gets the parameter table money type list.
	 * 
	 * @return the parameter table money type list
	 */
	public List<ParameterTable> getParameterTableMoneyTypeList() {
		return parameterTableMoneyTypeList;
	}

	/**
	 * Sets the parameter table money type list.
	 * 
	 * @param parameterTableMoneyTypeList
	 *            the new parameter table money type list
	 */
	public void setParameterTableMoneyTypeList(List<ParameterTable> parameterTableMoneyTypeList) {
		this.parameterTableMoneyTypeList = parameterTableMoneyTypeList;
	}

	/**
	 * Gets the holder account request session.
	 * 
	 * @return the holder account request session
	 */
	public HolderAccountRequest getHolderAccountRequestHySession() {
		return holderAccountRequestHySession;
	}

	/**
	 * Sets the holder account request session.
	 * 
	 * @param holderAccountRequestHySession
	 *            the new holder account request session
	 */
	public void setHolderAccountRequestHySession(HolderAccountRequest holderAccountRequestHySession) {
		this.holderAccountRequestHySession = holderAccountRequestHySession;
	}

	/**
	 * Gets the participant list.
	 * 
	 * @return the participant list
	 */
	public List<Participant> getParticipantList() {
		return participantList;
	}

	/**
	 * Sets the participant list.
	 * 
	 * @param participantList
	 *            the new participant list
	 */
	public void setParticipantList(List<Participant> participantList) {
		this.participantList = participantList;
	}

	/**
	 * Gets the holder request.
	 * 
	 * @return the holder request
	 */
	public Holder getHolderRequest() {
		return holderRequest;
	}

	/**
	 * Sets the holder request.
	 * 
	 * @param holderRequest
	 *            the new holder request
	 */
	public void setHolderRequest(Holder holderRequest) {
		this.holderRequest = holderRequest;
	}

	/**
	 * Holder request new handler.
	 * 
	 * @param evt
	 *            the evt
	 */
//	public void holderRequestNewHandler(ValueChangeEvent evt) {
	public void holderRequestNewHandler() {
		executeAction();		
		hideLocalComponents();
		
//		if (evt.getPhaseId() != PhaseId.INVOKE_APPLICATION) {
//			evt.setPhaseId(PhaseId.INVOKE_APPLICATION);
//			evt.queue();
//			return;
//		}
		this.setHolderRequest(new Holder());
		this.getHolderAccountRequestHySession().getHolderAccountDetRequest().removeAll(this.getHolderAccountRequestHySession().getHolderAccountDetRequest());			
		this.getHolderAccountRequestHySession().getHolderAccountBankRequest().removeAll(this.getHolderAccountRequestHySession().getHolderAccountBankRequest());
		cleanHolderAccountReqBankHyRequest();
		this.getHolderAccountRequestHySession().getHolderAccountFilesRequest().removeAll(this.getHolderAccountRequestHySession().getHolderAccountFilesRequest());
	}	

	/**
	 * Register holder account handler.
	 *
	 * @return the string
	 */
	public String registerHolderAccountHandler() {
		if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
			//SI NO TIENE PARTICIPANTE Y ES EMISOR DPF NO PUEDE REGISTRAR
			if(!flagExistParticipantDPF){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_PARTICIPANT));
				JSFUtilities.showSimpleValidationDialog();
				return "";
			}
		}
		cleanHolderAccountRequest();
		return "holderAccountMgmRule";
	}

	/**
	 * Search holder.
	 * 
	 * this method is useful to get Holder from database
	 */
	@LoggerAuditWeb
	public void searchHolderHandler() {
		try{
		//executeAction();
		//JSFUtilities.hideGeneralDialogues();
		if(holderRequest.getIdHolderPk() != null && Validations.validateIsPositiveNumber(holderRequest.getIdHolderPk().intValue())){
			// If the Holder's IdHolderPk were ingresed the message is not neceseray
			// go to the DataBase
			if (Validations.validateIsNotNullAndNotEmpty(holderAccountRequestHySession.getHolderAccountDetRequest())
					&& Validations.validateIsNotNull(holderRequest.getIdHolderPk())) {
				for (HolderAccountDetRequest holderAccountReqDetHy : holderAccountRequestHySession.getHolderAccountDetRequest()) {
					if (holderAccountReqDetHy.getHolder().getIdHolderPk().equals(holderRequest.getIdHolderPk())) {
						JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,	null,PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_HOLDER_REPETED,
								new Object[] {String.valueOf(holderAccountReqDetHy.getHolder().getIdHolderPk()),
										holderAccountReqDetHy.getHolder().getFullName() });
						
						JSFUtilities.showSimpleValidationDialog();
						return;
					}
				}
			}
			// Get holder from database.
			Holder holder = null;
			try {
				holder = holderServiceFacade.getHolderServiceFacade(this.getHolderRequest().getIdHolderPk());
				holder.setFullName(holder.getDescriptionHolder());
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (Validations.validateIsNull(holder)) {
				JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_HOLDERNULL,
						new Object[] { String.valueOf(this.getHolderRequest().getIdHolderPk()) });
				this.setHolderRequest(new Holder());
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			if (!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())) {
				JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,	null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_HOLDER_BLOCK,
						new Object[] { String.valueOf(this.getHolderRequest().getIdHolderPk()) });
				this.setHolderRequest(new Holder());
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			// Validate if holder's type from database need to be equals than the
			// HolderAccount's type from SelectOneMenu . |
			if (this.getHolderAccountRequestHySession().getHolderAccountType().equals(HolderAccountType.JURIDIC.getCode())
					&& holder.getHolderType().equals(PersonType.NATURAL.getCode())
					|| this.getHolderAccountRequestHySession().getHolderAccountType().equals(HolderAccountType.NATURAL.getCode())
					&& holder.getHolderType().equals(PersonType.JURIDIC.getCode())) {
				JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_HOLDER_TYPE,
						new Object[] { String.valueOf(holder.getIdHolderPk()), holder.getFullName(), holder.getHolderTypeDescription(), HolderAccountType.lookup.get(this.getHolderAccountRequestHySession().getHolderAccountType()).getValue() });
				this.setHolderRequest(new Holder());
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			if ((this.getHolderAccountRequestHySession().getHolderAccountType().equals(HolderAccountType.OWNERSHIP.getCode())
					&& holder.getHolderType().equals(PersonType.JURIDIC.getCode()))) {
				JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_HOLDER_TYPE,
						new Object[] { String.valueOf(holder.getIdHolderPk()), holder.getFullName(), holder.getHolderTypeDescription(), "FÍSICA" });
				this.setHolderRequest(new Holder());
				JSFUtilities.showSimpleValidationDialog();
				return;
				
			}
			if (this.getHolderAccountRequestHySession().getHolderAccountType().equals(HolderAccountType.SUCCESSION.getCode())
					&& holder.getHolderType().equals(PersonType.JURIDIC.getCode())) {
				JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_HOLDER_TYPE_SUCCESION,
						new Object[] { String.valueOf(holder.getIdHolderPk()), holder.getFullName(), holder.getHolderTypeDescription() });
				this.setHolderRequest(new Holder());
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			// We need to validate if HolderAccount type is Juridic And cui code is
			// the same that the participant's idCode selected. .
			if (this.getHolderAccountRequestHySession().getHolderAccountType().equals(HolderAccountType.JURIDIC.getCode())) {
				cleanHolderAccountReqBankHyRequest();
				if (holder.getEconomicSector() != null && holder.getEconomicSector().equals(EconomicSectorType.FINANCIAL_PUBLIC_SECTOR.getCode())) {
				}
			}
			JSFUtilities.hideGeneralDialogues();
			this.setHolderRequest(holder);
			
			HolderAccountTO holderAccountTO = new HolderAccountTO();
			holderAccountTO.setIdHolderPk(holderRequest.getIdHolderPk());
			holderAccountTO.setParticipantTO(holderAccountRequestHySession.getParticipant().getIdParticipantPk());
			List<HolderAccountRequest> listHolderAccountRequest = holderAccountServiceFacade.getHolderAccountRequestList(holderAccountTO);
			if(listHolderAccountRequest!=null && listHolderAccountRequest.size()>0){
				Participant participantTO = new Participant();
				for(HolderAccountRequest har : listHolderAccountRequest){
					if(har.getRequestState().equals(HolderAccountRequestHysStatusType.REGISTERED.getCode())
							|| har.getRequestState().equals(HolderAccountRequestHysStatusType.APPROVE.getCode())){
					participantTO.setIdParticipantPk(holderAccountRequestHySession.getParticipant().getIdParticipantPk());
					List<Participant> participantList = accountsFacade.getLisParticipantServiceBean(participantTO);
					Participant auxParticipant = participantList.get(0);
					JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,	null,PropertiesConstants.ADM_VALIDATION_HOLDER_ACCOUNT_REQUEST_EXIST,
						new Object[] {holderRequest.getIdHolderPk().toString(),
						auxParticipant.getDisplayMnemonicCode() });
				
					JSFUtilities.showSimpleValidationDialog();
					this.setHolderRequest(new Holder());
					return;
					}
				}
			}
			
			// If HolderAccounts' type is Natural Or Juridic, quantity of holders is
			// One
			if (!(this.getHolderAccountRequestHySession().getHolderAccountType().equals(HolderAccountType.OWNERSHIP.getCode()))
					&& !(this.getHolderAccountRequestHySession().getHolderAccountType().equals(HolderAccountType.SUCCESSION.getCode()))	) {
				holderAccountRequestHySession.getHolderAccountDetRequest().removeAll(holderAccountRequestHySession.getHolderAccountDetRequest());
				addHolderOnDataTableHandler();
			}
			
			holderAccountTO.setStatus(HolderAccountStatusType.ACTIVE.getCode());
			List<HolderAccount> listHolderAccount = holderAccountServiceFacade.findHolderAccountsServiceFacade(holderAccountTO);
			if(listHolderAccount!=null && listHolderAccount.size()>0){
				Participant participantTO = new Participant();
				String  AccountNumber=GeneralConstants.BLANK_SPACE;
				
				for(HolderAccount ha : listHolderAccount){
					
					participantTO.setIdParticipantPk(holderAccountRequestHySession.getParticipant().getIdParticipantPk());
					List<Participant> participantList = accountsFacade.getLisParticipantServiceBean(participantTO);
					Participant auxParticipant = participantList.get(0);
					AccountNumber +=GeneralConstants.DASH.concat(ha.getAccountNumber().toString().concat(GeneralConstants.STR_BR));
					
					JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,	null,PropertiesConstants.ADM_VALIDATION_HOLDER_ACCOUNT_EXIST_ALL,
						new Object[] {holderRequest.getIdHolderPk().toString(),
						auxParticipant.getDisplayMnemonicCode(),AccountNumber });
				
					JSFUtilities.executeJavascriptFunction("PF('dlgHolderAccountExistW').show()");
					
					}
				
			}
			
		}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
	}

	/**
	 * Before save holder account handler.
	 * @throws ServiceException 
	 */
	@LoggerAuditWeb
	public void beforeSaveHolderAccountRequestHandler() throws ServiceException {
		executeAction();
		JSFUtilities.hideGeneralDialogues();		
		hideLocalComponents();
		if (isViewOperationRegister()) {
			if (holderAccountRequestHySession.getParticipant().getIdParticipantPk() == null
					|| !Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(this.getHolderAccountRequestHySession().getParticipant().getIdParticipantPk())))) {
				JSFUtilities.addContextMessage(
								"frmHolderAccountMgmt:cboParticipant",FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_PARTICIPANT),
								PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_PARTICIPANT));
				JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MESSAGE_DATA_REQUIRED, null);
				JSFUtilities.showSimpleValidationDialog();;
				return;
			} else if (Validations.validateIsNullOrEmpty(this.getHolderAccountRequestHySession().getHolderAccountType())
					|| !Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(this.getHolderAccountRequestHySession().getHolderAccountType())))) {
				JSFUtilities.addContextMessage(
								"frmHolderAccountMgmt:cboAccountType",FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_CLASSTYPE),
								PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_CLASSTYPE));
				JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MESSAGE_DATA_REQUIRED, null);
				JSFUtilities.showSimpleValidationDialog();;
				JSFUtilities.showSimpleValidationDialog();
				
				return;
			}
			//Validate that there holders
			if (Validations.validateListIsNullOrEmpty(this.getHolderAccountRequestHySession().getHolderAccountDetRequest())) {
				JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_HOLDER_EMPTY,null);
				JSFUtilities.showSimpleValidationDialog();
				return;
			} else if (holderAccountRequestHySession.getHolderAccountType().equals(HolderAccountType.OWNERSHIP.getCode()) ||
					holderAccountRequestHySession.getHolderAccountType().equals(HolderAccountType.SUCCESSION.getCode())) {
				boolean haveMainHolder = BooleanType.NO.getBooleanValue();
				for (HolderAccountDetRequest holderDet : holderAccountRequestHySession.getHolderAccountDetRequest()) {
					if (holderDet.isRepresentative() == BooleanType.YES.getBooleanValue()) {
						haveMainHolder = BooleanType.YES.getBooleanValue();
						break;
					}
				}
				if (!haveMainHolder) {
					JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,	null,PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_HOLDER_MAIN,null);
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
			}
			if (holderAccountRequestHySession.getHolderAccountType().equals(HolderAccountType.OWNERSHIP.getCode())
					&& this.getHolderAccountRequestHySession().getHolderAccountDetRequest().size() == 1) {
				JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_HOLDER_ONE,null);
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			if(!Validations.validateListIsNotNullAndNotEmpty(holderAccountRequestHySession.getHolderAccountBankRequest()) && !holderAccountRequestHySession.isBlPdd()) {
				JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_BANK_EMPTY,null);
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
		
			Integer contCombo = BooleanType.NO.getCode();
			Integer contTable = BooleanType.NO.getCode();			
			for(int i=0;i<parameterTableDocumentsList.size();i++){				
				if(parameterTableDocumentsList.get(i).getShortInteger().equals(BooleanType.YES.getCode())){	
					contCombo ++;
					for ( HolderAccountFileRequest holderAccountFile : holderAccountRequestHySession.getHolderAccountFilesRequest()){						
						if(parameterTableDocumentsList.get(i).getParameterTablePk().equals(holderAccountFile.getDocumentType())){
							contTable ++;							
						}						
					}
				}
			}
			/*Issue 1231-
			 * Los procesos de adjuntar documentos que no seran obligatorios y que 
			 * estan a cargo de los Participantes, sera el siguiente:
			 * - Registro de Titular Jurídico
			 * - Registro de Titular Natural
			 * - Registro Cuenta Titular
	    	 * 
	    	 */
			
			Participant participantAux = new Participant();     //getHolderAccountRequestHySession
			participantAux = accountsFacade.findParticipantByFilters(this.getHolderAccountRequestHySession().getParticipant());
			/*
			if (!participantAux.getEconomicActivity().equals(EconomicActivityType.BANCOS.getCode())){
				if(!userInfo.getUserAccountSession().isParticipantInstitucion()){
					
					if (!contCombo.equals(contTable)) {
						JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_FILE_REQUIRED,null);
						JSFUtilities.showSimpleValidationDialog();
						JSFUtilities.addContextMessage("frmHolderAccountMgmt:cmbAttachFileDocType",
								FacesMessage.SEVERITY_INFO, " ", " ");
						return;
					
				}	
			}
			}*/		
			
			holderAccountRequestHySession.setRequestType(HolderAccountRequestHysType.CREATE.getCode());
			holderAccountRequestHySession.setRequestNumber(this.getHolderAccountRequestHySession().getIdHolderAccountReqPk());
//			holderAccountRequestHySession.setParticipant(searchParticipantOnListFromPK(this.getHolderAccountRequestHySession().getParticipant().getIdParticipantPk()));
		}
		JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_REGISTER, null,PropertiesConstants.ADM_HOLDERACCOUNT_CONFIRMATION_REGISTER,
				new Object[] { holderAccountRequestHySession.getHolderAccountReqDetRnts() });
		JSFUtilities.showComponent(":frmHolderAccountMgmt:cnfHolderAccountMgmt");
	}

	/**
	 * Save holder account handler.
	 */
	@LoggerAuditWeb
	public void saveHolderAccountHandler() {
		holderAccountRequestHySession.setRegistryDate(CommonsUtilities.currentDate());
		holderAccountRequestHySession.setRegistryUser(userInfo.getUserAccountSession().getUserName());
		holderAccountRequestHySession.setRequestNumber(0L);
		holderAccountRequestHySession.setRequestState(HolderAccountRequestHysStatusType.REGISTERED.getCode());
		try {
			holderAccountServiceFacade.registerHolderAccountRequest(this.getHolderAccountRequestHySession());
		} catch (ServiceException e) {
			if (e.getErrorService() != null) {
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
				JSFUtilities.showComponent(":frmHolderAccountMgmt:holderAccountMgmtOk");
				return;
			}
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL,	null, PropertiesConstants.ADM_HOLDERACCOUNT_CONFIRMATION_REGISTER_OK,
				new Object[] {holderAccountRequestHySession.getIdHolderAccountReqPk().toString(),holderAccountRequestHySession.getHolderAccountReqDetRnts() });
		JSFUtilities.showComponent(":frmHolderAccountMgmt:holderAccountMgmtOk");
		loadPrivilegesUser();
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
	}

	/**
	 * Before annulate action.
	 * 
	 * @return the string
	 */
	public String beforeAnulateAction() {
		executeAction();
		hideLocalComponents();
		if (holderAccountRequestHySession == null) {
			JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
			JSFUtilities.showSimpleValidationDialog();;
			return "";
		}
		List<String> lstOperationUser = validateIsValidUser(holderAccountRequestHySession);
		if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_INVALID_USER_ANNUL,
							new Object[] { lstOperationUser.get(0).toString() }));
			JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		if (!holderAccountRequestHySession.getRequestState().equals(HolderAccountRequestHysStatusType.REGISTERED.getCode())) {
			JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,
					PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_DOACTION_APROVATE_ANULATE,
					new Object[] { this.getHolderAccountRequestHySession().getStateHolderAccountReqDescription() 
			});
			JSFUtilities.showSimpleValidationDialog();;
			return "";
		}
		this.setViewOperationType(ViewOperationsType.ANULATE.getCode());
		readObjectFromBD(holderAccountRequestHySession);
		return "holderAccountDetailRule";
	}
	
	/**
	 * Before approve action.
	 * 
	 * @return the string
	 */
	public String beforeApproveAction() {
		executeAction();
		hideLocalComponents();
		if (this.getHolderAccountRequestHySession() == null) {
			JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
			JSFUtilities.showSimpleValidationDialog();;
			return "";
		}
		List<String> lstOperationUser = validateIsValidUser(holderAccountRequestHySession);
		if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_INVALID_USER_APPROVE,
							new Object[] { lstOperationUser.get(0).toString() }));
			JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		if (!this.getHolderAccountRequestHySession().getRequestState().equals(HolderAccountRequestHysStatusType.REGISTERED.getCode())) {
			JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,
					PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_DOACTION_APROVATE_ANULATE,
					new Object[] { this.getHolderAccountRequestHySession().getStateHolderAccountReqDescription() });
			JSFUtilities.showSimpleValidationDialog();;
			return "";
		}
		this.setViewOperationType(ViewOperationsType.APPROVE.getCode());
		readObjectFromBD(holderAccountRequestHySession);
		return "holderAccountDetailRule";
	}

	/**
	 * Validate is valid user.
	 *
	 * @param objHolderAccountRequest the obj holder account request
	 * @return the list
	 */
	private List<String> validateIsValidUser(
			HolderAccountRequest objHolderAccountRequest) {
		// TODO Auto-generated method stub
		List<OperationUserTO> lstOperationUserParam = new ArrayList<OperationUserTO>();
		List<String> lstOperationUserResult;
		OperationUserTO operationUserTO = new OperationUserTO();
		operationUserTO.setOperNumber(holderAccountRequestHySession.getIdHolderAccountReqPk().toString());
		operationUserTO.setUserName(holderAccountRequestHySession.getRegistryUser());
		lstOperationUserParam.add(operationUserTO);
		lstOperationUserResult = getIsSubsidiary(userInfo,lstOperationUserParam);
		return lstOperationUserResult;
	}

	/**
	 * Before confirm action.
	 * 
	 * @return the string
	 */
	public String beforeConfirmAction() {
		executeAction();
		hideLocalComponents();
		if (this.getHolderAccountRequestHySession() == null) {
			JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,
					PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
			JSFUtilities.showSimpleValidationDialog();;
			return "";
		}
		List<String> lstOperationUser = validateIsValidUser(holderAccountRequestHySession);
		if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_INVALID_USER_CONFIRM,
							new Object[] { lstOperationUser.get(0).toString() }));
			JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		if (!this.getHolderAccountRequestHySession().getRequestState().equals(HolderAccountRequestHysStatusType.APPROVE.getCode())) {
			JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,
					PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_DOACTION_CONFIRM_REJECT,
					new Object[] { this.getHolderAccountRequestHySession()
							.getStateHolderAccountReqDescription() });
			JSFUtilities.showSimpleValidationDialog();;
			return "";
		}
		this.setViewOperationType(ViewOperationsType.CONFIRM.getCode());
		readObjectFromBD(holderAccountRequestHySession);
		return "holderAccountDetailRule";
	}

	/**
	 * Before reject action.
	 * 
	 * @return the string
	 */
	public String beforeRejectAction() {
		executeAction();
		hideLocalComponents();
		if (this.getHolderAccountRequestHySession() == null) {
			JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,
					PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
			JSFUtilities.showSimpleValidationDialog();;
			return "";
		} 
		List<String> lstOperationUser = validateIsValidUser(holderAccountRequestHySession);
		if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_INVALID_USER_REJECT,
							new Object[] { lstOperationUser.get(0).toString() }));
			JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		if (!this.getHolderAccountRequestHySession().getRequestState().equals(HolderAccountRequestHysStatusType.APPROVE.getCode())) {
			JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,
					PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_DOACTION_CONFIRM_REJECT,
					new Object[] { this.getHolderAccountRequestHySession()
							.getStateHolderAccountReqDescription() });
			JSFUtilities.showSimpleValidationDialog();;
			return "";
		}
		this.setViewOperationType(ViewOperationsType.REJECT.getCode());
		readObjectFromBD(holderAccountRequestHySession);
		return "holderAccountDetailRule";
	}

	/**
	 * Before anulate holder account request handler.
	 *
	 * @param e the e
	 */
	@SuppressWarnings("unchecked")
	public void beforeAnulateHolderAccountRequestHandler(ActionEvent e) {
		JSFUtilities.resetComponent(":frmMotive:dialogMotive");
		executeAction();
		if (Validations.validateIsNull(this.getHolderAccountRequestHySession())) {
			JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
			JSFUtilities.showSimpleValidationDialog();;
			return;
		}
		JSFUtilities.hideGeneralDialogues();
		hideLocalComponents();
		if (this.motiveController != null) {
			this.motiveController.setIdMotivePK(null);
			this.motiveController.setShowMotiveText(Boolean.FALSE);
			this.motiveController.setMotiveText(null);
		}
		if (this.getHolderAccountRequestHySession().getRequestState().equals(HolderAccountRequestHysStatusType.REGISTERED.getCode())) {								
			this.getMotiveController().setMotiveList((List<ParameterTable>) MotiveController.getMotiveListCache().get(MasterTableType.HOLDER_REQUEST_ANNULAR_MOTIVE.getCode()));
			this.setViewOperationType(ViewOperationsType.ANULATE.getCode());
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MOTIVE_ANNUL), null);
			JSFUtilities.showComponent(":frmMotive:dialogMotive");
			return;
		}
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
				PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_DOACTION,
				new Object[] { this.getHolderAccountRequestHySession().getStateHolderAccountReqDescription() }));
		JSFUtilities.showSimpleValidationDialog();;
	}

	/**
	 * Before approve holder account request handler.
	 *
	 * @param e the e
	 */
	public void beforeApproveHolderAccountRequestHandler(ActionEvent e) {
		executeAction();
		if (Validations.validateIsNull(this.getHolderAccountRequestHySession())) {
			JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_WARNING_ACTION, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
			JSFUtilities.showSimpleValidationDialog();;
			return;
		}
		JSFUtilities.hideGeneralDialogues();
		hideLocalComponents();
		if (this.getHolderAccountRequestHySession().getRequestState().equals(HolderAccountRequestHysStatusType.REGISTERED.getCode())) {
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_APPROVE),
					PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_APPROVE,
					new Object[] {
							holderAccountRequestHySession.getIdHolderAccountReqPk().toString(),
							holderAccountRequestHySession.getRequestTypeDescription(),
							holderAccountRequestHySession.getDescriptionCui() }));
			this.setViewOperationType(ViewOperationsType.APPROVE.getCode());
			JSFUtilities.showComponent(":frmBlock:cnfblock");
			return;
		}
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_DOACTION,
				new Object[] { this.getHolderAccountRequestHySession().getStateHolderAccountReqDescription() }));
		JSFUtilities.showSimpleValidationDialog();;
		return;
	}

	/**
	 * Hide local components.
	 */
	public void hideLocalComponents() {
		this.motiveController.setIdMotivePK(null);
		this.motiveController.setShowMotiveText(Boolean.FALSE);
		this.motiveController.setMotiveText(null);
		JSFUtilities.hideComponent(":frmMotive:dialogMotive");
		JSFUtilities.hideComponent(":frmBlock:cnfblock");
		JSFUtilities.hideComponent(":frmHolderAccountMgmt:cnfHolderAccountMgmt");
		JSFUtilities.hideComponent(":frmHolderAccountMgmt:cnfCleanHolderAccountMgmt");
		JSFUtilities.hideGeneralDialogues();
	}

	/**
	 * Close dialog reject motive.
	 */
	public void closeDialogRejectMotive(){
		JSFUtilities.resetComponent(":frmMotive:dialogMotive");
		hideLocalComponents();
	}
	
	/**
	 * Before clean holder account request.
	 */
	public void beforeCleanHolderAccountRequest() {
		executeAction();		
		hideLocalComponents();
		if (this.getBackDialogController().getObjectBefore().toString().equals(this.getHolderAccountRequestHySession().toString())) {
			cleanHolderAccountRequest();
		} else {
			JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null,"lbl.clean.confirm", null);
			JSFUtilities.showComponent(":frmHolderAccountMgmt:cnfCleanHolderAccountMgmt");
		}
	}

	/**
	 * Clean holder account request.
	 */
	public void cleanHolderAccountRequest() {
		executeAction();
		JSFUtilities.hideGeneralDialogues();
		JSFUtilities.resetViewRoot();
		hideLocalComponents();
		this.setViewOperationType(ViewOperationsType.REGISTER.getCode());
		holderAccountRequestHySession = new HolderAccountRequest();
		holderAccountRequestHySession.setHolderAccountDetRequest(new ArrayList<HolderAccountDetRequest>());
		holderAccountRequestHySession.setHolderAccountBankRequest(new ArrayList<HolderAccountBankRequest>());
		holderAccountRequestHySession.setHolderAccountGroup(HolderAccountGroupType.INVERSTOR.getCode());		
		holderAccountRequestHySession.setHolderAccountFilesRequest(new ArrayList<HolderAccountFileRequest>());
		holderAccountRequestHySession.setHolderAccountType(null);
		backDialogController = new BackDialogController();
		cleanHolderAccountReqBankHyRequest();
		holderRequest = new Holder();
		this.getBackDialogController().setObjectBefore(new StringBuilder(this.getHolderAccountRequestHySession().toString()));
		evaluateUserSession();
		accountHolderFileNameDisplay = null;		
		if (blockPdd) {
			holderAccountRequestHySession.setBlPdd(BooleanType.YES.getBooleanValue());
		}
	}
	
	/**
	 * Clean holder account request.
	 */
	public void cleanNoHolderAccountRequest() {
		executeAction();
		JSFUtilities.hideGeneralDialogues();
		hideLocalComponents();			
	}

	/**
	 * Before reject holder account request handler.
	 *
	 * @param e the e
	 */
	@SuppressWarnings("unchecked")
	public void beforeRejectHolderAccountRequestHandler(ActionEvent e) {
		JSFUtilities.resetComponent(":frmMotive:dialogMotive");
		executeAction();
		if (Validations.validateIsNull(this.getHolderAccountRequestHySession())) {
			JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_WARNING_ACTION, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
			JSFUtilities.showSimpleValidationDialog();;
			return;
		}
		JSFUtilities.hideGeneralDialogues();
		hideLocalComponents();
		if (this.motiveController != null) {											
			this.motiveController.setIdMotivePK(null);
			this.motiveController.setShowMotiveText(Boolean.FALSE);
			this.motiveController.setMotiveText(null);
		}
		if (holderAccountRequestHySession.getRequestState().equals(HolderAccountRequestHysStatusType.APPROVE.getCode())) {
			motiveController.setMotiveList((List<ParameterTable>) MotiveController.getMotiveListCache().get(MasterTableType.HOLDER_REQUEST_REJECT_MOTIVE.getCode()));
			this.setViewOperationType(ViewOperationsType.REJECT.getCode());
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MOTIVE_REJECT), null);
			JSFUtilities.showComponent(":frmMotive:dialogMotive");
			return;
		}
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_DOACTION,new Object[] { 
				this.getHolderAccountRequestHySession().getStateHolderAccountReqDescription() }));
		JSFUtilities.showSimpleValidationDialog();;
	}

	/**
	 * Before confirm holder account request handler.
	 *
	 * @param e the e
	 */
	public void beforeConfirmHolderAccountRequestHandler(ActionEvent e) {
		executeAction();
		if (Validations.validateIsNull(this.getHolderAccountRequestHySession())) {
			JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_WARNING_ACTION, null,
					PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
			JSFUtilities.showSimpleValidationDialog();;
			return;
		}
		JSFUtilities.hideGeneralDialogues();
		hideLocalComponents();
		if (holderAccountRequestHySession.getRequestState().equals(HolderAccountRequestHysStatusType.APPROVE.getCode())) {
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(
					PropertiesConstants.ADM_HOLDERACCOUNT_CONFIRM,
					new Object[] {
							holderAccountRequestHySession.getIdHolderAccountReqPk().toString(),
							holderAccountRequestHySession.getRequestTypeDescription(),
							holderAccountRequestHySession.getDescriptionCui() }));
			this.setViewOperationType(ViewOperationsType.CONFIRM.getCode());
			JSFUtilities.showComponent(":frmBlock:cnfblock");
			return;
		}
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_DOACTION,
				new Object[] { holderAccountRequestHySession
						.getStateHolderAccountReqDescription() }));
		JSFUtilities.showSimpleValidationDialog();;
	}

	/**
	 * Change action holder account request handler.
	 */
	@LoggerAuditWeb
	public void changeActionHolderAccountRequestHandler() {
		HolderAccountRequest holdAccReqTemp = new HolderAccountRequest();
		holdAccReqTemp.setIdHolderAccountReqPk(holderAccountRequestHySession.getIdHolderAccountReqPk());
		holdAccReqTemp.setRequestState(holderAccountRequestHySession.getRequestState());
		holdAccReqTemp.setHolderAccountDetRequest(holderAccountRequestHySession.getHolderAccountDetRequest());
		holdAccReqTemp.setRequestType(HolderAccountRequestHysType.CREATE.getCode());
		holdAccReqTemp.setParticipant(holderAccountRequestHySession.getParticipant());
		boolean isConfirm = Boolean.FALSE;
		String messageResponseOk = null;
		try {
			switch (ViewOperationsType.lookup.get(this.getViewOperationType())) {
			case APPROVE:
				holdAccReqTemp.setApproveDate(CommonsUtilities.currentDate());
				holdAccReqTemp.setApproveUser(userInfo.getUserAccountSession().getUserName());
				messageResponseOk = PropertiesConstants.ADM_HOLDERACCOUNT_REQUEST_APPROVE_OK;
				holdAccReqTemp = holderAccountServiceFacade.approveHolderAccountRequest(holdAccReqTemp);
				break;
			case ANULATE:
				holdAccReqTemp.setAnullDate(CommonsUtilities.currentDate());
				holdAccReqTemp.setAnullUser(userInfo.getUserAccountSession().getUserName());
				holdAccReqTemp.setRequestMotive(Integer.parseInt(motiveController.getIdMotivePK().toString()));
				holdAccReqTemp.setRequestOtherMotive(motiveController.getMotiveText());
				messageResponseOk = PropertiesConstants.ADM_HOLDERACCOUNT_REQUEST_ANULATE_OK;
				holdAccReqTemp = holderAccountServiceFacade.annulateHolderAccountRequest(holdAccReqTemp);
				break;
			case CONFIRM:
				holdAccReqTemp.setConfirmDate(CommonsUtilities.currentDate());
				holdAccReqTemp.setConfirmUser(userInfo.getUserAccountSession().getUserName());
				isConfirm = Boolean.TRUE;
				holdAccReqTemp = holderAccountServiceFacade.confirmHolderAccountRequest(holdAccReqTemp);
				break;
			case REJECT:
				holdAccReqTemp.setRejectDate(CommonsUtilities.currentDate());
				holdAccReqTemp.setRejectUser(userInfo.getUserAccountSession().getUserName());
				holdAccReqTemp.setRequestMotive(Integer.parseInt(motiveController.getIdMotivePK().toString()));
				holdAccReqTemp.setRequestOtherMotive(getMotiveController().getMotiveText());
				messageResponseOk = PropertiesConstants.ADM_HOLDERACCOUNT_REQUEST_REJECT_OK;
				holdAccReqTemp = holderAccountServiceFacade.rejectHolderAccountRequest(holdAccReqTemp);
				break;
			}
			// search holderAccountRequest again
			searchHolderAccountRequestHysHandler();
			if (isConfirm) {
				JSFUtilities.showMessageOnDialog(
						PropertiesConstants.MESSAGES_SUCCESFUL,
						null,
						PropertiesConstants.ADM_HOLDERACCOUNT_REQUEST_CONFIRM_OK,
						new Object[] {
								holdAccReqTemp.getIdHolderAccountReqPk().toString(),
								holdAccReqTemp.getHolderAccount().getAccountNumber().toString(),
								holdAccReqTemp.getDescriptionCui() });
			} else {
				JSFUtilities.showMessageOnDialog(
						PropertiesConstants.MESSAGES_SUCCESFUL,
						null,
						messageResponseOk,
						new Object[] {
								holdAccReqTemp.getIdHolderAccountReqPk().toString(),
								holdAccReqTemp.getRequestTypeDescription(),
								holdAccReqTemp.getDescriptionCui() });
			}
			JSFUtilities.hideComponent(":frmBlock:cnfblock");
			JSFUtilities.showComponent(":frmBlock:holderAccountMgmtOk");
			loadPrivilegesUser();
			setViewOperationType(ViewOperationsType.CONSULT.getCode());																		
		} catch (ServiceException e) {
			if (e.getErrorService() != null) {
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e
						.getErrorService().getMessage());
				JSFUtilities.showComponent(":frmBlock:holderAccountMgmtOk");
				loadPrivilegesUser();
				return;
			}
		}
	}

	/**
	 * Change motive handler.
	 */
	public void changeMotiveHandler() {
		if (this.getMotiveController().getIdMotivePK() != null) {
			if (this.getMotiveController().getIdMotivePK().equals(ERROR_ANULATE)|| this.getMotiveController().getIdMotivePK().equals(ERROR_REJECT)) {
				this.getMotiveController().setShowMotiveText(Boolean.TRUE);
			} else {
				this.getMotiveController().setShowMotiveText(Boolean.FALSE);
			}
		}else{
			this.getMotiveController().setShowMotiveText(Boolean.FALSE);
		}
	}

	/**
	 * Confirm motive handler.
	 */
	public void confirmMotiveHandler() {
		JSFUtilities.hideComponent(":frmBlock:cnfblock");
		if (this.getMotiveController().getIdMotivePK() == null || !Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(this.getMotiveController().getIdMotivePK())))) {
			JSFUtilities.addContextMessage(
							"frmMotive:cboMotive",FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_MOTIVE_REJECT_ANULATE),
							PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_MOTIVE_REJECT_ANULATE));
			JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MESSAGE_DATA_REQUIRED, null);
			JSFUtilities.showSimpleValidationDialog();;
			return;
		}	
		if (this.getMotiveController().getIdMotivePK().equals(ERROR_ANULATE)|| this.getMotiveController().getIdMotivePK().equals(ERROR_REJECT)) {
			if (this.getMotiveController().getMotiveText() == null || Validations.validateIsNullOrEmpty(this.getMotiveController().getMotiveText())) {			
				JSFUtilities.addContextMessage(
						"frmMotive:txtOtherMotive",FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_OTHER_MOTIVE_REJECT_ANULATE),
						PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_OTHER_MOTIVE_REJECT_ANULATE));
						JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MESSAGE_DATA_REQUIRED, null);						
						JSFUtilities.showSimpleValidationDialog();;						
						return;
			} 
		} 
				
		if (this.getMotiveController().getIdMotivePK().equals(ERROR_ANULATE) || this.getMotiveController().getIdMotivePK().equals(ERROR_REJECT)) {
			if (Validations.validateIsNullOrEmpty(this.getMotiveController().getMotiveText())) {
				JSFUtilities.showComponent(":frmMotive:dialogMotive");
				return;
			}
		}
		if (this.getViewOperationType().equals(ViewOperationsType.ANULATE.getCode())) {
			JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_CONFIRM,null,PropertiesConstants.ADM_HOLDERACCOUNT_ANULATE,
					new Object[] {
							holderAccountRequestHySession.getIdHolderAccountReqPk().toString(),
							holderAccountRequestHySession.getRequestTypeDescription(),
							holderAccountRequestHySession.getDescriptionCui() });
		} else if (this.getViewOperationType().equals(ViewOperationsType.REJECT.getCode())) {
			JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_CONFIRM,null, PropertiesConstants.ADM_HOLDERACCOUNT_REJECT,
					new Object[] {
							holderAccountRequestHySession.getIdHolderAccountReqPk().toString(),
							holderAccountRequestHySession.getRequestTypeDescription(),
							holderAccountRequestHySession.getDescriptionCui() });
		}
		JSFUtilities.showComponent(":frmBlock:cnfblock");
	}

	/**
	 * Search holder account request detail list.
	 */
	@LoggerAuditWeb
	public void searchHolderAccountRequestHysHandler() {
		JSFUtilities.hideGeneralDialogues();
		this.hideLocalComponents();
		List<HolderAccountRequest> holderAccountRequestHyList = null;
		holderAccountTO.setHolderAccountRequestType(HolderAccountRequestHysType.CREATE.getCode());
		holderAccountTO.setNeedHolder(Boolean.TRUE);		
		if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
			//SI NO TIENE PARTICIPANTE Y ES EMISOR DPF NO PUEDE REGISTRAR
			if(!flagExistParticipantDPF){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_PARTICIPANT));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
		}
		try {
			holderAccountRequestHyList = holderAccountServiceFacade.getHolderAccountRequestList(getHolderAccountTO());
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		holderAccountRequestHyDataModel = new GenericDataModel<HolderAccountRequest>(holderAccountRequestHyList);
	}

	/**
	 * Clean holder account to handler.
	 */
	public void cleanHolderAccountTOHandler() {
		this.setHolderAccountTO(new HolderAccountTO());
		holderAccountTO.setInitialDate(CommonsUtilities.currentDate());
		holderAccountTO.setFinalDate(CommonsUtilities.currentDate());
		this.setHolderAccountRequestHyDataModel(null);
		JSFUtilities.resetViewRoot();
		evaluateUserSession();
	}

	/**
	 * Adds the holder on data table handler.
	 */
	public void addHolderOnDataTableHandler() {
		if (Validations.validateIsNotNullAndNotEmpty(holderAccountRequestHySession.getHolderAccountDetRequest())) {
			for (HolderAccountDetRequest holderAccountReqDetHy : holderAccountRequestHySession.getHolderAccountDetRequest()) {
				if (holderAccountReqDetHy.getHolder().getIdHolderPk().equals(holderRequest.getIdHolderPk())) {
					JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,	null,PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_HOLDER_REPETED,
							new Object[] {String.valueOf(holderAccountReqDetHy.getHolder().getIdHolderPk()),
									holderAccountReqDetHy.getHolder().getFullName() });
					
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
			}
		}
		HolderAccountDetRequest holderAccountReqDetHy = new HolderAccountDetRequest();
		Holder holder = new Holder();
		holder.setIdHolderPk(holderRequest.getIdHolderPk());		
		holder.setBirthDate(holderRequest.getBirthDate());
		holder.setDocumentNumber(holderRequest.getDocumentNumber());
		holder.setDocumentType(holderRequest.getDocumentType());
		holder.setEconomicActivity(holderRequest.getEconomicActivity());
		holder.setEconomicSector(holderRequest.getEconomicSector());
		holder.setEmail(holderRequest.getEmail());
		holder.setFaxNumber(holderRequest.getFaxNumber());
		holder.setFirstLastName(holderRequest.getFirstLastName());
		holder.setFullName(holderRequest.getFullName());
		holder.setHolderType(holderRequest.getHolderType());
		holder.setHomePhoneNumber(holderRequest.getHomePhoneNumber());
		holder.setIndDisabled(holderRequest.getIndDisabled());
		holder.setIndMinor(holderRequest.getIndMinor());
		holder.setIndResidence(holderRequest.getIndResidence());
		holder.setJuridicClass(holderRequest.getJuridicClass());
		holder.setLegalAddress(holderRequest.getLegalAddress());
		holder.setLegalDepartment(holderRequest.getLegalDepartment());
		holder.setLegalDistrict(holderRequest.getLegalDistrict());
		holder.setLegalProvince(holderRequest.getLegalProvince());
		holder.setLegalResidenceCountry(holderRequest.getLegalResidenceCountry());
		holder.setMobileNumber(holderRequest.getMobileNumber());
		holder.setName(holderRequest.getName());
		holder.setNationality(holderRequest.getNationality());
		holder.setOfficePhoneNumber(holderRequest.getOfficePhoneNumber());
		holder.setPostalAddress(holderRequest.getPostalAddress());
		holder.setPostalDepartment(holderRequest.getPostalDepartment());
		holder.setPostalDistrict(holderRequest.getPostalDistrict());
		holder.setPostalProvince(holderRequest.getPostalProvince());
		holder.setPostalResidenceCountry(holderRequest.getPostalResidenceCountry());
		holder.setSecondDocumentNumber(holderRequest.getSecondDocumentNumber());
		holder.setSecondDocumentType(holderRequest.getSecondDocumentType());
		holder.setSecondLastName(holderRequest.getSecondLastName());
		holder.setSecondNationality(holderRequest.getSecondNationality());
		holder.setSex(holderRequest.getSex());
		holder.setStateHolder(holderRequest.getStateHolder());
		holder.setRegistryDate(CommonsUtilities.currentDate());
		holder.setRegistryUser(userInfo.getUserAccountSession().getUserName());
		holder.setParticipant(holderRequest.getParticipant());
		if(holderRequest.getDocumentSource()!=null){
		holder.setDocumentSource(holderRequest.getDocumentSource());
		}
		if(holderRequest.getDocumentIssuanceDate()!=null){
		holder.setDocumentIssuanceDate(holderRequest.getDocumentIssuanceDate());	
		}
		
		holderAccountReqDetHy.setHolder(holder);
		holderAccountReqDetHy.setHolderAccountRequest(holderAccountRequestHySession);		
		holderAccountReqDetHy.setIndRepresentative(BooleanType.NO.getCode());
		holderAccountReqDetHy.setRegistryDate(CommonsUtilities.currentDate());
		holderAccountReqDetHy.setRegistryUser(userInfo.getUserAccountSession().getUserName());
		holderAccountReqDetHy.setIndOldNew(BooleanType.YES.getCode());
		holderAccountRequestHySession.getHolderAccountDetRequest().add(holderAccountReqDetHy);
		
	}

	/**
	 * Selected holder account request detail hanlder.
	 * 
	 * @param holderAccountRequestHy
	 *            the holder account request
	 */
	public void selectedHolderAccountRequestDetailHanlder(HolderAccountRequest holderAccountRequestHy) {
		this.setViewOperationType(ViewOperationsType.CONSULT.getCode());
		readObjectFromBD(holderAccountRequestHy);
	}

	/**
	 * Read object from data base.
	 * 
	 * @param holderAccountRequestHy
	 *            the holder account request
	 */
	private void readObjectFromBD(HolderAccountRequest holderAccountRequestHy) {
		HolderAccountRequest holderAccountRequestHyFinded = null;
		try {
			holderAccountRequestHyFinded = holderAccountServiceFacade.getHolderAccountRequest(holderAccountRequestHy.getIdHolderAccountReqPk());
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		// Necessary privileges to set starting component buttons
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();																			
		if (holderAccountRequestHyFinded != null && this.getEvaluateUserInfoIsActive()) {
			// If the object exists if the user has been active to confirm, approve, disapprove, reject
			if (this.getViewOperationType().equals(ViewOperationsType.CONSULT.getCode())) {
			// If you only query can only return
			} else {
				// Depending on the action to be performed, the respective privileges will be shown.
				switch (ViewOperationsType.get(this.getViewOperationType())) {
				case CONFIRM:
					privilegeComponent.setBtnConfirmView(Boolean.TRUE);
					break;
				case APPROVE:
					privilegeComponent.setBtnApproveView(Boolean.TRUE);
					break;
				case ANULATE:
					privilegeComponent.setBtnAnnularView(Boolean.TRUE);
					break;
				case REJECT:
					privilegeComponent.setBtnRejectView(Boolean.TRUE);
					break;
				}
			}
		}
		userPrivilege.setPrivilegeComponent(privilegeComponent);
		setHolderAccountRequestHySession(holderAccountRequestHyFinded);
	}


	/**
	 * This method is useful to add Account on DataTable.
	 */
	public void addAccountOnDataTableHandler() {
		executeAction();
		hideLocalComponents();
		JSFUtilities.hideGeneralDialogues();
		if(Validations.validateIsNullOrNotPositive(holderAccountReqBankHyRequest.getBank().getIdBankPk()) 
				|| Validations.validateIsNullOrNotPositive(holderAccountReqBankHyRequest.getBankAccountType())
				|| Validations.validateIsNullOrEmpty(holderAccountReqBankHyRequest.getAccountNumber()) 
				|| Validations.validateIsNullOrNotPositive(holderAccountReqBankHyRequest.getCurrency())){
			JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING,null,PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_BANK_CHANGES,null);
			JSFUtilities.showSimpleValidationDialog();
			if(Validations.validateIsNullOrNotPositive(holderAccountReqBankHyRequest.getBank().getIdBankPk()) ){
				JSFUtilities.addContextMessage("frmHolderAccountMgmt:cboBanks", FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_CBO_BANK),
						PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_CBO_BANK));
			}
			if(Validations.validateIsNullOrNotPositive(holderAccountReqBankHyRequest.getBankAccountType()) ){
				JSFUtilities.addContextMessage("frmHolderAccountMgmt:cboAccountsType", FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_CBO_ACCOUNT_TYPE),
						PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_CBO_ACCOUNT_TYPE));					
			}
			if(Validations.validateIsNullOrEmpty(holderAccountReqBankHyRequest.getAccountNumber()) ){
				JSFUtilities.addContextMessage("frmHolderAccountMgmt:txtAccountNumber",	FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_NUMBER_ACCOUNT),
						PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_NUMBER_ACCOUNT));		
			}
			if(Validations.validateIsNullOrNotPositive(holderAccountReqBankHyRequest.getCurrency()) ){
				JSFUtilities.addContextMessage("frmHolderAccountMgmt:cboMoneyType",FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_MONEY_TYPE),
						PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_MONEY_TYPE));	
			}
			return;			
		}
		if (this.getHolderAccountRequestHySession().getHolderAccountBankRequest().size() == TAMANIO_BANK_ACCOUNTS) {
			cleanHolderAccountReqBankHyRequest();
			JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING,null,PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_BANKACCOUNTS,null);
			JSFUtilities.showSimpleValidationDialog();;
			return;
		}
		// Validate if account in DOP were selected OR account in USD were
		// selected, is only one account by money's type
		// boolean haveDOP = false,haveUSD = false;
		for (HolderAccountBankRequest holderAccountReqBankHy : this.getHolderAccountRequestHySession().getHolderAccountBankRequest()) {
			if (holderAccountReqBankHy.getCurrency().equals(holderAccountReqBankHyRequest.getCurrency())) {
				JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING,null,PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_CURRENCE,
						new Object[] { holderAccountReqBankHyRequest.getCurrencyDescription() });
				JSFUtilities.showSimpleValidationDialog();;
				cleanHolderAccountReqBankHyRequest();
				return;
			}			
		}
		
		if(validateNumberAccountAndBank(holderAccountReqBankHyRequest.getBank().getIdBankPk(), holderAccountReqBankHyRequest.getAccountNumber())){
			JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_NUMBER_ACCOUNT_BANK, null);			
			JSFUtilities.showSimpleValidationDialog();			
			return;
		}
		
		JSFUtilities.hideGeneralDialogues();
		holderAccountReqBankHyRequest.setBank(searchBankOnListFromPK(this.getHolderAccountReqBankHyRequest().getBank().getIdBankPk()));
		HolderAccountBankRequest holderAccountReqBankHy = new HolderAccountBankRequest();
		holderAccountReqBankHy.setBank(holderAccountReqBankHyRequest.getBank());
		holderAccountReqBankHy.setCurrency(holderAccountReqBankHyRequest.getCurrency());		
		holderAccountReqBankHy.setAccountNumber(holderAccountReqBankHyRequest.getAccountNumber());
		holderAccountReqBankHy.setBankAccountType(holderAccountReqBankHyRequest.getBankAccountType());
		holderAccountReqBankHy.setDescriptionBank(holderAccountReqBankHyRequest.getBankDescription());		
		holderAccountReqBankHy.setHolderAccountRequest(holderAccountRequestHySession);
		holderAccountReqBankHy.setIndOldNew(BooleanType.YES.getCode());
		holderAccountReqBankHy.setRegistryUser(userInfo.getUserAccountSession().getUserName());
		holderAccountReqBankHy.setRegistryDate(CommonsUtilities.currentDate());	
		holderAccountReqBankHy.setHaveBic(BooleanType.NO.getCode());
		holderAccountReqBankHy.setSwiftCode(holderAccountReqBankHyRequest.getSwiftCode());
		holderAccountRequestHySession.getHolderAccountBankRequest().add(holderAccountReqBankHy);
		cleanHolderAccountReqBankHyRequest();
	}

	/**
	 * Validate number account and bank.
	 *
	 * @param idBank the id bank
	 * @param number the number
	 * @return true, if successful
	 */
	public boolean validateNumberAccountAndBank(Long idBank, String number ){
		boolean band = false;		
		for (HolderAccountBankRequest holderAccountBankRequest : holderAccountRequestHySession.getHolderAccountBankRequest()) {
			for(Bank bank : this.getBankList()){
				if(bank.getIdBankPk().equals(idBank)){
					if(bank.getDescription().compareTo(holderAccountBankRequest.getDescriptionBank()) == 0 && holderAccountBankRequest.getAccountNumber().compareTo(number) == 0){					
						band = true;
						return band;
					}									
				}				
			}						
		}
		return band;
	}
	
	/**
	 * Validate if have changes.
	 * 
	 * @param holderAccountReqBankHy
	 *            the holder account request bank
	 * @return true, if successful
	 */
	public boolean validateIfHaveChanges(
			HolderAccountBankRequest holderAccountReqBankHy) {
		if (Validations.validateIsNotNull(holderAccountReqBankHy.getBank().getIdBankPk())
				|| (Validations.validateIsNotNull(holderAccountReqBankHy.getCurrency()) && Validations.validateIsPositiveNumber(holderAccountReqBankHy.getCurrency()))
				|| (Validations.validateIsNotNull(holderAccountReqBankHy.getBankAccountType()) && Validations.validateIsPositiveNumber(holderAccountReqBankHy.getBankAccountType()))) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	/**
	 * Clean holder account request bank request.
	 */
	public void cleanHolderAccountReqBankHyRequest() {
		if (JSFUtilities.findViewComponent("frmHolderAccountMgmt:flsHoldBan") != null) {
			JSFUtilities.resetComponent("frmHolderAccountMgmt:flsHoldBan");
		}
		holderAccountReqBankHyRequest.setBank(searchBankOnListFromPK(0L));
		holderAccountReqBankHyRequest.setBankAccountType(null);
		holderAccountReqBankHyRequest.setCurrency(null);
		holderAccountReqBankHyRequest.setAccountNumber("");
		holderAccountReqBankHyRequest.setSwiftCode(null);
		holderAccountReqBankHyRequest.setFlagForeignAccount(false);
		this.setAttachFileHolderAccountSelected(null);
	}
	

	/**
	 * Delete system image temp.
	 */
	public void deleteDocumentPdd(){
		documentFileName = "";
		holderAccountRequestHySession.setDocumentPdd(null);
//		JSFUtilities.putSessionMap("sessStreamedPhoto",null);
	}
	
	/**
	 * File upload handler.
	 *
	 * @param event the event
	 */
	public void fileUploadHandler(FileUploadEvent event){
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);				
		Object [] dataOnMessage = { event.getFile().getFileName(), event.getFile().getSize() / 1000 };
		showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, "adm.holderaccount.fupload.pdd", dataOnMessage);
		String fDisplayName = fUploadValidateFile(event.getFile(), null, null);
		if(fDisplayName != null){			 
			try {				
				documentFileName = event.getFile().getFileName();				
				holderAccountRequestHySession.setDocumentPdd(event.getFile().getContents());
				holderAccountRequestHySession.setNameDocumentPdd(documentFileName);
			} catch (Exception e) {
				e.printStackTrace();
			}
//			 JSFUtilities.showValidationDialog();
		 }
	}
	
	public StreamedContent getStreamedContentFile(){
		StreamedContent streamedContentFile = null;
		try {
			InputStream inputStream = null;
			inputStream = new ByteArrayInputStream(holderAccountRequestHySession.getDocumentPdd());
			if(Validations.validateIsNullOrEmpty(documentFileName)) {
				documentFileName=holderAccountRequestHySession.getNameDocumentPdd();
			}
			streamedContentFile = new DefaultStreamedContent(inputStream, null,	documentFileName);
			inputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return streamedContentFile;
	}

	/**
	 * Clean holder request.
	 */
	public void cleanHolderRequest() {
		this.getHolderRequest().setIdHolderPk(null);
		this.getHolderRequest().setFullName(null);
	}

	/**
	 * Evaluate back.
	 * 
	 * @return the string
	 */
	public String evaluateBack() {
		executeAction();		
		hideLocalComponents();
		JSFUtilities.hideGeneralDialogues();		
		this.getBackDialogController().setObjectAfter(new StringBuilder(this.getHolderAccountRequestHySession().toString()));
		if (this.getBackDialogController().isShowModal()) {			
			JSFUtilities.showComponent(":frmHolderAccountMgmt:cnfBackAdvancedAccount");
			return GeneralConstants.EMPTY_STRING;
		}
		return SEARCH_ACTION_ACCOUNT_MGMT;
	}

	/**
	 * Removes the holder on data table handler.
	 * 
	 * @param holderAccountReqDetHys
	 *            the holder account request detail
	 */
	public void removeHolderOnDataTableHandler(HolderAccountDetRequest holderAccountReqDetHys) {
		holderAccountRequestHySession.getHolderAccountDetRequest().remove(holderAccountReqDetHys);
		cleanHolderAccountReqBankHyRequest();
	}

	/**
	 * Removes the account on data table handler.
	 * 
	 * @param holderAccountReqBankHy
	 *            the holder account request bank
	 */
	public void removeAccountOnDataTableHandler(HolderAccountBankRequest holderAccountReqBankHy) {
		holderAccountRequestHySession.getHolderAccountBankRequest().remove(holderAccountReqBankHy);
	}

	/**
	 * Gets the holder account to.
	 * 
	 * @return the holder account to
	 */
	public HolderAccountTO getHolderAccountTO() {
		return holderAccountTO;
	}

	/**
	 * Sets the holder account to.
	 * 
	 * @param holderAccountTO
	 *            the new holder account to
	 */
	public void setHolderAccountTO(HolderAccountTO holderAccountTO) {
		this.holderAccountTO = holderAccountTO;
	}

	/**
	 * Gets the holder account request hy data model.
	 * 
	 * @return the holder account request hy data model
	 */
	public GenericDataModel<HolderAccountRequest> getHolderAccountRequestHyDataModel() {
		return holderAccountRequestHyDataModel;
	}

	/**
	 * Sets the holder account request data model.
	 * 
	 * @param holderAccountRequestHyDataModel
	 *            the new holder account request data model
	 */
	public void setHolderAccountRequestHyDataModel(GenericDataModel<HolderAccountRequest> holderAccountRequestHyDataModel) {
		this.holderAccountRequestHyDataModel = holderAccountRequestHyDataModel;
	}

	/**
	 * Asignate holder main.
	 * 
	 * @param holderAccountReqDetHys
	 *            the holder account request detail
	 */
	public void asignateHolderMain(HolderAccountDetRequest holderAccountReqDetHys) {
		for (HolderAccountDetRequest holderDet : holderAccountRequestHySession.getHolderAccountDetRequest()) {
			if(holderDet.getHolder().getIdHolderPk().equals(holderAccountReqDetHys.getHolder().getIdHolderPk())){
				holderDet.setIndRepresentative(BooleanType.YES.getCode());
			}
		}
	}
	
	/**
	 * Deasignate holder main.
	 *
	 * @param holderAccountReqDetHys the holder account req det hys
	 */
	public void deasignateHolderMain(HolderAccountDetRequest holderAccountReqDetHys) {
		for (HolderAccountDetRequest holderDet : holderAccountRequestHySession.getHolderAccountDetRequest()) {
			if(holderDet.getHolder().getIdHolderPk().equals(holderAccountReqDetHys.getHolder().getIdHolderPk())){
				holderDet.setIndRepresentative(BooleanType.NO.getCode());
			}
		}
		
	}

	/**
	 * Search bank on list from pk.
	 * 
	 * @param idBankPk
	 *            the id bank pk
	 * @return the bank
	 * this method is useful to get Bank from idBankPk because
	 * this method it's not necesary to getBank from database.
	 */
	public Bank searchBankOnListFromPK(Long idBankPk) {
		for (Bank bank : this.getBankList()) {
			if (bank.getIdBankPk().equals(idBankPk)) {
				return bank;
			}
		}
		return new Bank();
	}

	/**
	 * Search participant on list from pk.
	 * 
	 * @param idParticipantPk
	 *            the id participant pk
	 * @return the participant
	 * this method is useful to get Bank from idBankPk because
	 *         with this method it's not necesary to getParticipant from
	 *         database.
	 */
	public Participant searchParticipantOnListFromPK(Long idParticipantPk) {
		for (Participant participant : this.getParticipantList()) {
			if (participant.getIdParticipantPk().equals(idParticipantPk)) {
				return participant;
			}
		}
		return null;
	}

	/**
	 * Gets the size max on file.
	 * 
	 * @return the size max on file
	 */
	public String getTamanioMaxOnFile() {
		return PropertiesUtilities.getMessage(PropertiesConstants.TAMANIO_FILE,	new Object[] { GeneralConstants.TAMANIO_FILE_KBS });
	}

	/**
	 * Gets the back dialog controller.
	 * 
	 * @return the back dialog controller
	 */
	public BackDialogController getBackDialogController() {
		return backDialogController;
	}

	/**
	 * Sets the back dialog controller.
	 * 
	 * @param backDialogController
	 *            the new back dialog controller
	 */
	public void setBackDialogController(BackDialogController backDialogController) {
		this.backDialogController = backDialogController;
	}

	/**
	 * Gets the parameter table documents list.
	 * 
	 * @return the parameter table documents list
	 */
	public List<ParameterTable> getParameterTableDocumentsList() {
		return parameterTableDocumentsList;
	}

	/**
	 * Sets the parameter table documents list.
	 * 
	 * @param parameterTableDocumentsList
	 *            the new parameter table documents list
	 */
	public void setParameterTableDocumentsList(
			List<ParameterTable> parameterTableDocumentsList) {
		this.parameterTableDocumentsList = parameterTableDocumentsList;
	}

	/**
	 * Gets the word formated.
	 * 
	 * @param word
	 *            the word
	 * @return the word formated
	 */
	public static String getWordFormated(String word) {
		char[] caracteres = word.toLowerCase().toCharArray();
		caracteres[0] = Character.toUpperCase(caracteres[0]);
		for (int i = 0; i < word.length() - 2; i++) {
			if (caracteres[i] == ' ' || caracteres[i] == '.'
					|| caracteres[i] == ',') {
				caracteres[i + 1] = Character.toUpperCase(caracteres[i + 1]);
			}
		}
		return new String(caracteres);
	}

	/**
	 * Gets the user info.
	 * 
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 * 
	 * @param userInfo
	 *            the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	

	/**
	 * Gets the evaluate user info active. 
	 * 
	 * @return the evaluate user info active
	 */
	public boolean getEvaluateUserInfoIsActive() {
		if (userInfo.getUserAccountSession().isDepositaryInstitution()) {
			return Boolean.TRUE;
		} else if (userInfo.getUserAccountSession().isIssuerDpfInstitucion()) {
			return Boolean.TRUE;
		} else if (userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
			return Boolean.TRUE;
		} else if (userInfo.getUserAccountSession().isParticipantInstitucion()) {
			if (userInfo.getUserAccountSession().getInstitutionState() != null) {
				return userInfo.getUserAccountSession().getInstitutionState().equals(ParticipantStateType.REGISTERED.getCode());
			}
		} else if (userInfo.getUserAccountSession().isIssuerInstitucion()) {
			if (userInfo.getUserAccountSession().getInstitutionState() != null) {
				return userInfo.getUserAccountSession().getInstitutionState().equals(IssuerStateType.REGISTERED.getCode());
			}
		}
		return Boolean.FALSE;
	}

	/**
	 * Gets the evaluate user info register. 
	 * 
	 * @return the evaluate user info register
	 */
	public boolean getEvaluateUserInfoCanRegister() {
		if (userInfo.getUserAcctions() != null) {
			Boolean canRegister = userPrivilege.getUserAcctions().isRegister();
			if (canRegister != null && canRegister) {
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}

	/**
	 * Gets the current date.
	 * 
	 * @return the current date
	 */
	public Date getCurrentDate() {
		return currentDate;
	}

	/**
	 * Sets the current date.
	 * 
	 * @param currentDate
	 *            the new current date
	 */
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}			

	/**
	 * Gets the account group list.
	 * 
	 * @return the account group list
	 */
	public List<ParameterTable> getParameterTableAccountGroupList() {
		return parameterTableAccountGroupList;
	}

	/**
	 * Sets the account group list.
	 *
	 * @param parameterTableAccountGroupList the new parameter table account group list
	 */
	public void setParameterTableAccountGroupList(List<ParameterTable> parameterTableAccountGroupList) {
		this.parameterTableAccountGroupList = parameterTableAccountGroupList;
	}

	/**
	 * Gets the attach file selected.
	 * 
	 * @return the attach file selected
	 */
	public Integer getAttachFileHolderAccountSelected() {
		return attachFileHolderAccountSelected;
	}

	/**
	 * Sets the attach file selected.
	 *
	 * @param attachFileHolderAccountSelected the new attach file holder account selected
	 */
	public void setAttachFileHolderAccountSelected(Integer attachFileHolderAccountSelected) {
		this.attachFileHolderAccountSelected = attachFileHolderAccountSelected;
	}
	
	/**
	 * Gets the name file.
	 * 
	 * @return the name file
	 */
	public String getAccountHolderFileNameDisplay() {
		return accountHolderFileNameDisplay;
	}

	/**
	 * Sets the name file.
	 *
	 * @param accountHolderFileNameDisplay the new account holder file name display
	 */
	public void setAccountHolderFileNameDisplay(String accountHolderFileNameDisplay) {
		this.accountHolderFileNameDisplay = accountHolderFileNameDisplay;
	}					

	/**
	 * Value change select document holder.
	 */
	public void valueChangeCmbDocumentHolderAccount(){
		accountHolderFileNameDisplay = null;
	}
	
	/**
	 * Attach document Holder Account Request file.
	 *
	 * @param event the event
	 */
	public void documentAttachHolderAccountRequestFile(FileUploadEvent event){
		if(attachFileHolderAccountSelected!=null && !attachFileHolderAccountSelected.equals(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER))){						
			 for(int i=0;i<parameterTableDocumentsList.size();i++){
				if(parameterTableDocumentsList.get(i).getParameterTablePk().equals(attachFileHolderAccountSelected)){					
					HolderAccountFileRequest holderAccountReqFileHySend =  new HolderAccountFileRequest();
					holderAccountReqFileHySend.setDocumentType(parameterTableDocumentsList.get(i).getParameterTablePk());
					holderAccountReqFileHySend.setDocumentTypeDesc(parameterTableDocumentsList.get(i).getDescription());
					holderAccountReqFileHySend.setIndMain(parameterTableDocumentsList.get(i).getShortInteger());
					holderAccountReqFileHySend.setFilename(event.getFile().getFileName());
					holderAccountReqFileHySend.setDocumentFile(event.getFile().getContents());
					holderAccountReqFileHySend.setRegistryUser(userInfo.getUserAccountSession().getUserName());
					holderAccountReqFileHySend.setRegistryDate(CommonsUtilities.currentDate());
					holderAccountReqFileHySend.setIndOldNew(BooleanType.YES.getCode());
					holderAccountReqFileHySend.setHolderAccountRequest(holderAccountRequestHySession);										
					insertOrReplaceHolderAccountRequestFileItem(holderAccountReqFileHySend);
				}
			}
		}
	}
	
	/**
	 * Insert document Holder Account Request file.
	 *
	 * @param holderAccountReqFileHySend the holder account req file hy send
	 */
	public void insertOrReplaceHolderAccountRequestFileItem(HolderAccountFileRequest holderAccountReqFileHySend){		
		boolean flag = false;		
		int listSize = holderAccountRequestHySession.getHolderAccountFilesRequest().size();
		if(listSize>0){
			for(int i=0;i<listSize;i++)
			{			
				if(holderAccountRequestHySession.getHolderAccountFilesRequest().get(i).getDocumentType().equals(holderAccountReqFileHySend.getDocumentType())){
					holderAccountRequestHySession.getHolderAccountFilesRequest().remove(i);
					holderAccountRequestHySession.getHolderAccountFilesRequest().add(i,holderAccountReqFileHySend);
					this.setAttachFileHolderAccountSelected(null);				
					flag = true;
				}	
			}
			if(!flag){
				holderAccountRequestHySession.getHolderAccountFilesRequest().add(holderAccountReqFileHySend);
				this.setAttachFileHolderAccountSelected(null);
			}
		}
		else{
			holderAccountRequestHySession.getHolderAccountFilesRequest().add(holderAccountReqFileHySend);
			this.setAttachFileHolderAccountSelected(null);
		}		
	}
	
	/**
	 * Gets the streamed content temporal file holder Account history.
	 *
	 * @param holderAccountReqFileHy the holder account req file hy
	 * @return the streamed content temporal file holder Account history
	 */
	public StreamedContent getStreamedContentTemporalFileHolderAccountHys(HolderAccountFileRequest holderAccountReqFileHy) {				
		InputStream is = null;		
		if(!this.getViewOperationType().equals(BooleanType.YES.getCode())){
			byte[] fileAccount = new byte[]{};
			try {
				fileAccount = holderAccountServiceFacade.getHolderAccountReqFileFacade(holderAccountReqFileHy.getIdHolderAccountReqFilePk());
			} catch (ServiceException e) {
				e.printStackTrace();
			}
			is = new ByteArrayInputStream(fileAccount);	
									
		}else{
			is = new ByteArrayInputStream(holderAccountReqFileHy.getDocumentFile());
		}
		return new DefaultStreamedContent(is,"application/pdf",holderAccountReqFileHy.getFilename());
	}
	
	/**
	 * Removes the holder Account request file.
	 *
	 * @param holderAccountReqFileHy the holder account req file hy
	 */
	public void removeHolderAccountRequestFile(HolderAccountFileRequest holderAccountReqFileHy) {
		holderAccountRequestHySession.getHolderAccountFilesRequest().remove(holderAccountReqFileHy);
		accountHolderFileNameDisplay = null;
		this.setAttachFileHolderAccountSelected(null);		
	}

	/**
	 * Gets the status list.
	 * 
	 * @return the status list
	 */
	public List<ParameterTable> getParameterTableStatusList() {
		return parameterTableStatusList;
	}

	/**
	 * Sets the status list.
	 *
	 * @param parameterTableStatusList the new parameter table status list
	 */
	public void setParameterTableStatusList(List<ParameterTable> parameterTableStatusList) {
		this.parameterTableStatusList = parameterTableStatusList;
	}
	
	/**
	 * clean model request account.
	 */
	public void cleanDataModelAccountRequest(){
		this.setHolderAccountRequestHyDataModel(null);
	}

	/**
	 * Gets the type banks list.
	 * 
	 * @return the type banks list
	 */
	public List<ParameterTable> getParameterTableAccountsTypeBankList() {
		return parameterTableAccountsTypeBankList;
	}

	/**
	 * Sets the  type banks list.
	 *
	 * @param parameterTableAccountsTypeBankList the new parameter table accounts type bank list
	 */
	public void setParameterTableAccountsTypeBankList(List<ParameterTable> parameterTableAccountsTypeBankList) {
		this.parameterTableAccountsTypeBankList = parameterTableAccountsTypeBankList;
	}
	
	/**
	 * Execute back page hanlder.
	 */
	public void executeBackPageHanlderHolderAccountRequest(){
		if(holderAccountRequestHyDataModel==null || holderAccountRequestHyDataModel.getSize()==0){
			evaluateUserSession();
		}
	}

	/**
	 * Gets the f upload file size mgmt.
	 *
	 * @return the f upload file size mgmt
	 */
	public String getfUploadFileSizeMgmt() {
		return fUploadFileSizeMgmt;
	}

	/**
	 * Sets the f upload file size mgmt.
	 *
	 * @param fUploadFileSizeMgmt the new f upload file size mgmt
	 */
	public void setfUploadFileSizeMgmt(String fUploadFileSizeMgmt) {
		this.fUploadFileSizeMgmt = fUploadFileSizeMgmt;
	}

	/**
	 * Gets the f upload file size display mgmt.
	 *
	 * @return the f upload file size display mgmt
	 */
	public String getfUploadFileSizeDisplayMgmt() {
		return fUploadFileSizeDisplayMgmt;
	}

	/**
	 * Sets the f upload file size display mgmt.
	 *
	 * @param fUploadFileSizeDisplayMgmt the new f upload file size display mgmt
	 */
	public void setfUploadFileSizeDisplayMgmt(String fUploadFileSizeDisplayMgmt) {
		this.fUploadFileSizeDisplayMgmt = fUploadFileSizeDisplayMgmt;
	}

	/**
	 * Gets the user privilege.
	 *
	 * @return the user privilege
	 */
	public UserPrivilege getUserPrivilege() {
		return userPrivilege;
	}

	/**
	 * Sets the user privilege.
	 *
	 * @param userPrivilege the new user privilege
	 */
	public void setUserPrivilege(UserPrivilege userPrivilege) {
		this.userPrivilege = userPrivilege;
	}

	/**
	 * Checks if is flag exist issuer dpf.
	 *
	 * @return the flagExistIssuerDPF
	 */
	public boolean isFlagExistIssuerDPF() {
		return flagExistParticipantDPF;
	}

	/**
	 * Sets the flag exist issuer dpf.
	 *
	 * @param flagExistIssuerDPF the flagExistIssuerDPF to set
	 */
	public void setFlagExistIssuerDPF(boolean flagExistIssuerDPF) {
		this.flagExistParticipantDPF = flagExistIssuerDPF;
	}

	public String getPdfFileUploadFiltTypes() {
		return pdfFileUploadFiltTypes;
	}

	public void setPdfFileUploadFiltTypes(String pdfFileUploadFiltTypes) {
		this.pdfFileUploadFiltTypes = pdfFileUploadFiltTypes;
	}

	public String getDocumentFileName() {
		return documentFileName;
	}

	public void setDocumentFileName(String documentFileName) {
		this.documentFileName = documentFileName;
	}


	/**
	 * @return the blockPdd
	 */
	public Boolean getBlockPdd() {
		return blockPdd;
	}


	/**
	 * @param blockPdd the blockPdd to set
	 */
	public void setBlockPdd(Boolean blockPdd) {
		this.blockPdd = blockPdd;
	}

}