package com.pradera.accounts.holderaccount.facade;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.accounts.holderaccount.service.HolderAccountModifyServiceBean;
import com.pradera.accounts.holderaccount.service.HolderAccountServiceBean;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountRequest;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestHysType;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class HolderAccountModifyFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/08/2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class HolderAccountModifyFacade {

	/** The holder account service bean. */
	@EJB
	HolderAccountModifyServiceBean holderAccountServiceBean;

	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The notification service facade. */
	@EJB 
	NotificationServiceFacade	notificationServiceFacade;
	
	/**
	 * Gets the bank list service facade.
	 *
	 * @return the bank list service facade
	 */
	public List<Bank> getBankListServiceFacade(){
		return holderAccountServiceBean.getBankListServiceBean();
	}
	
	/**
	 * Register holder account request service facade.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_MODIFICATION_REGISTER)
	public boolean registerHolderAccountRequestServiceFacade(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		//Paratemers for notification
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_ACCOUNT_MODIFICATION_REGISTER.getCode());
		Object[] parameters = new Object[]{holderAccountRequestHy.getHolderAccount().getAccountNumber().toString()};
		Long participantCode = holderAccountRequestHy.getParticipant().getIdParticipantPk();
		
		//Send notification
		sendNotification(participantCode, businessProcess, parameters);
		
		return holderAccountServiceBean.registryHolderAccountRequest(holderAccountRequestHy);
	}
	
	/**
	 * Gets the holder account req det hy list service facade.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account req det hy list service facade
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_MODIFICATION_QUERY)
	public List<HolderAccountRequest> getHolderAccountRequestHysListServiceFacade(HolderAccountTO holderAccountTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return holderAccountServiceBean.getHolderAccountRequestsList(holderAccountTO);
	}
	
	/**
	 * Approve holder account request.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @return the holder account request hy
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_MODIFICATION_APPROVE)
	public HolderAccountRequest approveHolderAccountRequest(HolderAccountRequest holderAccountRequestHy) throws ServiceException{			
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
		
		HolderAccountRequest holderAccountRequest = updateSelectiveHolderAccountRequestServiceFacade(holderAccountRequestHy);
		
		//Paratemers for notification
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_ACCOUNT_MODIFICATION_APPROVE.getCode());
		Object[] parameters = new Object[]{holderAccountRequestHy.getHolderAccount().getAccountNumber().toString()};
		Long participantCode = holderAccountRequest.getParticipant().getIdParticipantPk();
		
		//Send notification
		sendNotification(participantCode, businessProcess, parameters);
		
		return holderAccountRequest;
	}
	
	/**
	 * Annulate holder account request.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @return the holder account request hy
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_MODIFICATION_ANNULATE)
	public HolderAccountRequest annulateHolderAccountRequest(HolderAccountRequest holderAccountRequestHy) throws ServiceException{		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
		
		HolderAccountRequest holderAccountReq = updateSelectiveHolderAccountRequestServiceFacade(holderAccountRequestHy);
		
		//Paratemers for notification
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_ACCOUNT_MODIFICATION_APPROVE.getCode());
		Object[] parameters = new Object[]{holderAccountRequestHy.getHolderAccount().getAccountNumber().toString()};
		Long participantCode = holderAccountReq.getParticipant().getIdParticipantPk();
		
		//Send notification
		sendNotification(participantCode, businessProcess, parameters);
		
		return holderAccountReq ; 
	}
	
	/**
	 * Reject holder account request.
	 *
	 * @param holderAccountRequestHy the holder account request
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_MODIFICATION_REJECT)
	public HolderAccountRequest rejectHolderAccountRequest(HolderAccountRequest holderAccountRequestHy) throws ServiceException{		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		
		HolderAccountRequest holderAccountReq = updateSelectiveHolderAccountRequestServiceFacade(holderAccountRequestHy);
		
		//Paratemers for notification
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_ACCOUNT_MODIFICATION_REJECT.getCode());
		Object[] parameters = new Object[]{holderAccountRequestHy.getHolderAccount().getAccountNumber().toString()};
		Long participantCode = holderAccountReq.getParticipant().getIdParticipantPk(); 
		
		//Send notification
		sendNotification(participantCode, businessProcess, parameters);
		
		return holderAccountReq;
	}
	
	/**
	 * Confirm holder account request.
	 *
	 * @param holderAccountRequestHy the holder account request
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_MODIFICATION_CONFIRM)
	public HolderAccountRequest confirmHolderAccountRequest(HolderAccountRequest holderAccountRequestHy) throws ServiceException{			
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());	
		
		HolderAccountRequest holderAccountReq = updateSelectiveHolderAccountRequestServiceFacade(holderAccountRequestHy);
		
		//Paratemers for notification
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_ACCOUNT_MODIFICATION_CONFIRM.getCode());
		Object[] parameters = new Object[]{holderAccountReq.getHolderAccount().getAccountNumber().toString()};
		Long participantCode = holderAccountReq.getParticipant().getIdParticipantPk(); 
		
		//Send notification
		sendNotification(participantCode, businessProcess, parameters);
		
		return holderAccountReq;
	}
	
	
	/**
	 * Update selective holder account request service facade.
	 *
	 * @param holderAccountRequestHy the holder account request
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	public HolderAccountRequest updateSelectiveHolderAccountRequestServiceFacade(HolderAccountRequest holderAccountRequestHy) throws ServiceException{		
		if(holderAccountRequestHy.getRequestType().equals(HolderAccountRequestHysType.CLOSE.getCode())){
			if(!holderAccountServiceBean.holderAccountCanWillClosedServiceBean(holderAccountRequestHy.getHolderAccount())){
				throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_WITH_BALANCES);
			}
		}
		return holderAccountServiceBean.updateRequestByState(holderAccountRequestHy);
	}
	
	/**
	 * Holder account can will closed service bean.
	 *
	 * @param holderAccount the holder account
	 * @return the boolean
	 * @throws ServiceException the service exception
	 */
	public Boolean holderAccountCanWillClosedServiceFacade(HolderAccount holderAccount) throws ServiceException{
		return holderAccountServiceBean.holderAccountCanWillClosedServiceBean(holderAccount);
	}
	
	/**
	 * Gets the holder account request hy service facade.
	 *
	 * @param idHolderAccountRequestPk the id holder account request pk
	 * @return the holder account request hy service facade
	 * @throws ServiceException the service exception
	 */
	public HolderAccountRequest getHolderAccountRequestHyServiceFacade(Long idHolderAccountRequestPk) throws ServiceException{
		return holderAccountServiceBean.getHolderAccountRequest(idHolderAccountRequestPk);
	}
	
	/**
	 * Gets the holder account service facade.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account service facade
	 * @throws ServiceException the service exception
	 */
	public HolderAccount getHolderAccountServiceFacade(HolderAccountTO holderAccountTO) throws ServiceException{
		return holderAccountServiceBean.getHolderAccount(holderAccountTO);
	}
	
	/**
	 * Gets the holder account list service facade.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account list service facade
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> getHolderAccountListServiceFacade(HolderAccountTO holderAccountTO) throws ServiceException{
		return holderAccountServiceBean.getHolderAccountListServiceBean(holderAccountTO);
	}
	
	/**
	 * Have request with out confirmation service facade.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account request hy
	 * @throws ServiceException the service exception
	 */
	public HolderAccountRequest haveRequestWithOutConfirmationServiceFacade(HolderAccountTO holderAccountTO) throws ServiceException{
		return holderAccountServiceBean.haveRequestWithOutConfirmationServiceBean(holderAccountTO);
	}
	
	/**
	 * Gets the holder account req file facade.
	 *
	 * @param idFilePk the id file pk
	 * @return the holder account req file facade
	 * @throws ServiceException the service exception
	 */
	public byte[] getHolderAccountReqFileFacade(Long idFilePk) throws ServiceException{
		return holderAccountServiceBean.getHolderAccountReqFile(idFilePk);
	}
	
	/**
	 * Gets the holder account file facade.
	 *
	 * @param idFilePk the id file pk
	 * @return the holder account file facade
	 * @throws ServiceException the service exception
	 */
	public byte[] getHolderAccountFileFacade(Long idFilePk) throws ServiceException{
		return holderAccountServiceBean.getHolderAccountFile(idFilePk);
	}
	
	/**
	 * Send notification.
	 *
	 * @param partCode the part code
	 * @param businessProcess the business process
	 * @param parameters the parameters
	 */
	public void sendNotification(Long partCode , BusinessProcess businessProcess,Object[] parameters){
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		 notificationServiceFacade.sendNotification(loggerUser.getUserName(), businessProcess, partCode, parameters);
	 }
}