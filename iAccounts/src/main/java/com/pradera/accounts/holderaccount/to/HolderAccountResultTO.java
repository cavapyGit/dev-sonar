package com.pradera.accounts.holderaccount.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class HolderAccountResultTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 07/06/2013
 */
public class HolderAccountResultTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id holder account pk. */
	private Long idHolderAccountPk;
	
	/** The account number. */
	private Integer accountNumber;
	
	/** The register date. */
	private Date registerDate;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The participant. */
	private String participant;
	
	/** The holders. */
	private List<String> holders;
	
	/** The account type. */
	private String accountType;
	
	/** The account group. */
	private String accountGroup;
	
	/** The account state. */
	private String accountState;
	
	/**
	 * Gets the id holder account pk.
	 *
	 * @return the id holder account pk
	 */
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}
	
	/**
	 * Sets the id holder account pk.
	 *
	 * @param idHolderAccountPk the new id holder account pk
	 */
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}
	
	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public Integer getAccountNumber() {
		return accountNumber;
	}
	
	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	/**
	 * Gets the register date.
	 *
	 * @return the register date
	 */
	public Date getRegisterDate() {
		return registerDate;
	}
	
	/**
	 * Sets the register date.
	 *
	 * @param registerDate the new register date
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}
	
	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public String getParticipant() {
		return participant;
	}
	
	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(String participant) {
		this.participant = participant;
	}
	
	/**
	 * Gets the holders.
	 *
	 * @return the holders
	 */
	public List<String> getHolders() {
		return holders;
	}
	
	/**
	 * Sets the holders.
	 *
	 * @param holders the new holders
	 */
	public void setHolders(List<String> holders) {
		this.holders = holders;
	}
	
	/**
	 * Gets the account type.
	 *
	 * @return the account type
	 */
	public String getAccountType() {
		return accountType;
	}
	
	/**
	 * Sets the account type.
	 *
	 * @param accountType the new account type
	 */
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	
	/**
	 * Gets the account group.
	 *
	 * @return the account group
	 */
	public String getAccountGroup() {
		return accountGroup;
	}
	
	/**
	 * Sets the account group.
	 *
	 * @param accountGroup the new account group
	 */
	public void setAccountGroup(String accountGroup) {
		this.accountGroup = accountGroup;
	}
	
	/**
	 * Gets the account state.
	 *
	 * @return the account state
	 */
	public String getAccountState() {
		return accountState;
	}
	
	/**
	 * Sets the account state.
	 *
	 * @param accountState the new account state
	 */
	public void setAccountState(String accountState) {
		this.accountState = accountState;
	}

	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
}