package com.pradera.accounts.holderaccount.view; 

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.persistence.Transient;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.accounts.holder.facade.HolderServiceFacade;
import com.pradera.accounts.holderaccount.facade.HolderAccountServiceFacade;
import com.pradera.accounts.holderaccount.to.HolderAccountResultTO;
import com.pradera.accounts.participants.facade.ParticipantServiceFacade;
import com.pradera.accounts.util.view.PropertiesConstants;
import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountFile;
import com.pradera.model.accounts.holderaccounts.HolderAccountRequest;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.component.HolderAccountMovement;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class HolderAccountMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/02/2013
 */
@DepositaryWebBean
public class HolderAccountQueryMgmtBean extends GenericBaseBean implements Serializable{
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The holder account request hy session. */
	private HolderAccount holderAccountSession;
	
	/** The holder account result session. */
	private HolderAccountResultTO holderAccountResultSession;
	
	/**  The holder account movement. */
	private HolderAccountMovement holderAccountMovement;
	
	/** The holder account to. */
	private HolderAccountTO holderAccountTO;
	
	/** The participant list. */
	private List<Participant> participantList;
	
	/** The holder request. */
	private Holder holderRequest;
	
	/** The bank list. */
	private List<Bank> bankList;
	
	/** The holder account request data model. */
	private GenericDataModel<HolderAccountResultTO> holderAccountResultDataModel;
	
	/** The parameter table money type list. */
	private List<ParameterTable> parameterTableMoneyTypeList;
	
	/** The parameter table documents list. */
	private List<ParameterTable> parameterTableDocumentsList;
	
	/** The show group account. */
	private boolean showGroupAccount;
	
	/** The holder account service facade. */
	@EJB
	HolderAccountServiceFacade holderAccountServiceFacade;
	
	/** The accounts facade. */
	@EJB
	ParticipantServiceFacade participantServiceFacade;
	
	/** The holder service facade. */
	@EJB
	HolderServiceFacade holderServiceFacade;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The country residence. */
	@Inject @Configurable Integer countryResidence;
	
	/** The accounts facade. */
	@EJB
	private AccountsFacade accountsFacade;
	
	/** The holder account history list. */
	private List<HolderAccountRequest>listHistoryHolderAccount;	
	
	/** The parameter table accouts type list. */
	private List<ParameterTable> parameterTableAccoutsTypeList;
	
	/** The parameter table to. */
	private ParameterTableTO parameterTableTO;
	
	/** The parameter table group list. */
	private List<ParameterTable> parameterTableAccountGroupList;
	
	/** The parameter table status list. */
	private List<ParameterTable> parameterTableStatusList;
	
	/** The flag exist participant dpf. */
	private boolean flagExistParticipantDPF;
	
	/** The upload file size display mgmt. */
	private String fUploadFileSizeDisplayMgmt="3000";
	
	private boolean blPdd;
	
	/**
	 * Instantiates a new holder account query mgmt bean.
	 */
	public HolderAccountQueryMgmtBean() {
		super();
		holderAccountTO = new HolderAccountTO();
		holderAccountSession	=	new HolderAccount();
		holderAccountTO.setInitialDate(null);
		holderAccountTO.setFinalDate(CommonsUtilities.currentDate());
		holderAccountTO.setNeedHolder(Boolean.TRUE);
		holderAccountTO.setNeedParticipant(Boolean.FALSE);
	}
	
	/**
	 * Init.
	 */
	@PostConstruct
	public void init(){
		beginConversation();
		holderRequest = new Holder();
		holderAccountSession = new HolderAccount();
		Participant participantTO = new Participant();
		loadCboDocumentsList();
		parameterTableTO = new ParameterTableTO();				
		try {
			this.setParticipantList(accountsFacade.getLisParticipantServiceBean(participantTO));
			this.setBankList(holderAccountServiceFacade.getBankListServiceFacade());
			//load the account type
			parameterTableTO.setMasterTableFk(MasterTableType.ACCOUNTS_TYPE.getCode());
			parameterTableTO.setState(1);
			this.setParameterTableAccoutsTypeList(generalParametersFacade.getListParameterTableServiceBean(this.getParameterTableTO()));
			//load the group accounts
			parameterTableTO.setMasterTableFk(MasterTableType.ACCOUNT_GROUP_TYPE.getCode());
			parameterTableTO.setState(1);
			this.setParameterTableAccountGroupList(generalParametersFacade.getListParameterTableServiceBean(this.getParameterTableTO()));
			//load the status accounts
			parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
			parameterTableTO.setState(1);
			this.setParameterTableStatusList(generalParametersFacade.getListParameterTableServiceBean(this.getParameterTableTO()));
			listHolidays();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		evaluateUserSession();
		
	}
	
	/**
	 * List holidays.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listHolidays() throws ServiceException{
		  Holiday holidayFilter = new Holiday();
		  holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		  GeographicLocation countryFilter = new GeographicLocation();
		  countryFilter.setIdGeographicLocationPk(countryResidence);
		  holidayFilter.setGeographicLocation(countryFilter);
		    
		  allHolidays = CommonsUtilities.convertListDateToString(generalParametersFacade.getListHolidayDateServiceFacade(holidayFilter));
	}
	
	/**
	 * Evaluate user session.
	 */
	public void evaluateUserSession(){
		if(userInfo.getUserAccountSession().isParticipantInstitucion() ||
				userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
			this.holderAccountTO.setParticipantTO(userInfo.getUserAccountSession().getParticipantCode());
		}else if (userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
			Long participantDpf = getIssuerDpfInstitution(userInfo);
			if(Validations.validateIsNotNullAndNotEmpty(participantDpf)){
				flagExistParticipantDPF = true;
				holderAccountTO.setParticipantTO(participantDpf);
			}else{
				flagExistParticipantDPF = false;
				holderAccountTO.setParticipantTO(null);
			}
		}
	}
	
	/**
	 * Search holder handler.
	 */
	public void searchHolderHandler(){
		cleanDataModelAccounQuery();
		executeAction();
		JSFUtilities.hideGeneralDialogues(); 
		if(holderRequest.getIdHolderPk() != null && Validations.validateIsPositiveNumber(holderRequest.getIdHolderPk().intValue())){
			// Get holder from database *.*
			Holder holder = null;
			try {
				holder = holderServiceFacade.getHolderServiceFacade(this.getHolderRequest().getIdHolderPk());
			holder.setFullName(holder.getDescriptionHolder());
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (Validations.validateIsNull(holder)) {
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_HOLDERNULL,
					new Object[] { String.valueOf(this.getHolderRequest().getIdHolderPk()) });
				this.setHolderRequest(new Holder());
				JSFUtilities.showSimpleValidationDialog();
						return;
				}
			if (!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())) {
						showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,	null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_HOLDER_BLOCK,
								new Object[] { String.valueOf(this.getHolderRequest().getIdHolderPk()) });
						this.setHolderRequest(new Holder());
						JSFUtilities.showSimpleValidationDialog();
						return;
			}
				this.holderAccountTO.setHolderaccountGroupType(null);
		}
		
	}
	
	/**
	 * Load select documents list.
	 */
	public void loadCboDocumentsList(){
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_FILES.getCode());
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		try {
			parameterTableDocumentsList = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Select state history.
	 *
	 * @param event the event
	 * @return the account history state
	 */
	public void getAccountHistoryState(ActionEvent event){		
	    try{
			listHistoryHolderAccount = new ArrayList<HolderAccountRequest>();
			HolderAccountResultTO holderAccountResultTO = (HolderAccountResultTO) (event.getComponent().getAttributes().get("attributeAccount"));
			listHistoryHolderAccount =  holderAccountServiceFacade.getHolderAccountRequestHistoryStateServiceFacade(holderAccountResultTO.getAccountNumber(),holderAccountResultTO.getIdParticipantPk());			
			//JSFUtilities.executeJavascriptFunction("widgHistoryHolderAccountDialog.show()");
			JSFUtilities.showComponent(":frmHistoryHolderAccount:historyAccountDialog");
	    }
	    catch(Exception ex){
	    excepcion.fire(new ExceptionToCatchEvent(ex));
	    }
	}
	
	/**
	 * Clean holder account to handler.
	 */
	public void cleanHolderAccountTOHandler(){
		this.setHolderAccountTO(new HolderAccountTO());
		this.setHolderAccountSession(new HolderAccount());
		holderRequest = new Holder();
		holderAccountTO.setInitialDate(null);
		holderAccountTO.setFinalDate(null);
		this.setHolderAccountDataModel(null);
		evaluateUserSession();
	}
	
	/**
	 * Change participant handler.
	 */
	public void changeParticipantHandler(){
		holderRequest = new Holder();
		this.setHolderAccountDataModel(null);
		this.holderAccountTO.setHolderaccountGroupType(null);
	}
	
	/**
	 * clean model handler.
	 */
	public void cleanDataModelAccounQuery(){
		this.setHolderAccountDataModel(null);
	}
	
	/**
	 * Search holder account modify request handler.
	 */
	public void searchHolderAccountListHandler(){	
		executeAction();
		JSFUtilities.hideGeneralDialogues();		
		hideLocalComponents();
		HolderAccountTO holdAccTO = new HolderAccountTO();
		holdAccTO.setParticipantTO(this.holderAccountTO.getParticipantTO());
		holdAccTO.setIdHolderPk(this.holderRequest.getIdHolderPk());
		holdAccTO.setHolderAccountType(this.holderAccountTO.getHolderAccountType());
		holdAccTO.setHolderaccountGroupType(this.holderAccountTO.getHolderaccountGroupType());
		holdAccTO.setStatus(this.holderAccountTO.getStatus());
		holdAccTO.setHolderAccountNumber(this.holderAccountTO.getHolderAccountNumber());		
		holdAccTO.setRelatedName(this.holderAccountTO.getRelatedName());
		holdAccTO.setHolderPrincipal(this.holderAccountTO.getHolderPrincipal());		
		holdAccTO.setInitialDate(this.holderAccountTO.getInitialDate());
		holdAccTO.setFinalDate(this.holderAccountTO.getFinalDate());
		List<HolderAccountResultTO> holderAccountResultTmp = null;
		if(this.holderAccountTO.getParticipantTO() == null && this.holderRequest.getIdHolderPk() == null
				&& this.holderAccountTO.getHolderAccountType() == null && this.holderAccountTO.getHolderaccountGroupType() == null
				&& this.holderAccountTO.getStatus() == null && this.holderAccountTO.getHolderAccountNumber() == null 
				&& this.holderAccountTO.getRelatedName() == null && this.holderAccountTO.getHolderPrincipal() == null	
				&& this.holderAccountTO.getInitialDate() == null && this.holderAccountTO.getFinalDate() == null){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_MESSAGE_DATA_REQUIRED, null);
			JSFUtilities.showValidationDialog();
			return;
		}
		if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
			//SI NO TIENE PARTICIPANTE Y ES EMISOR DPF NO PUEDE REGISTRAR
			if(!flagExistParticipantDPF){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_PARTICIPANT));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
		}
		try {
			holderAccountResultTmp = holderAccountServiceFacade.getHolderAccountList(holdAccTO);			
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		holderAccountResultDataModel = new GenericDataModel<HolderAccountResultTO>(holderAccountResultTmp);
	}
	
	/**
	 * Selected holder account request detail hanlder.
	 *
	 * @param holderAccountResult the holder account result
	 */
	public void selectedHolderAccountDetailHandler(HolderAccountResultTO holderAccountResult){
		HolderAccount holderAccountFinded = null;
		HolderAccountTO holderAccountTO = new HolderAccountTO();
		holderAccountTO.setNeedHolder(Boolean.TRUE);
		holderAccountTO.setNeedParticipant(Boolean.TRUE);
		holderAccountTO.setNeedFiles(Boolean.TRUE);
		holderAccountTO.setIdHolderAccountPk(holderAccountResult.getIdHolderAccountPk());
		try {
			holderAccountFinded = holderAccountServiceFacade.getHolderAccount(holderAccountTO);			
			holderAccountFinded.setParticipant(searchParticipantOnListFromPK(holderAccountFinded.getParticipant().getIdParticipantPk()));
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		this.setHolderAccountSession(holderAccountFinded);
		if(Validations.validateIsNotNull(holderAccountSession.getIndPdd()) && holderAccountSession.getIndPdd().equals(BooleanType.YES.getCode())) {
			blPdd=true;
		}else {
			blPdd=false;
		}
	}
	
	/**
	 * Hide local components.
	 */
	public void hideLocalComponents(){
		JSFUtilities.hideComponent(":frmMotive:dialogMotive");
		JSFUtilities.hideComponent(":frmBlock:cnfblock");
		JSFUtilities.hideComponent(":frmHolderAccountMgmt:cnfHolderAccountModifyMgmt");
		JSFUtilities.hideComponent(":frmHistoryHolderAccount:historyAccountDialog");
	}
	
	public StreamedContent getStreamedContentFile(){
		StreamedContent streamedContentFile = null;
		String documentFileName = "";
		try {
			InputStream inputStream = null;
			inputStream = new ByteArrayInputStream(holderAccountSession.getDocumentPdd());
			documentFileName=holderAccountSession.getNameDocumentPdd();
			streamedContentFile = new DefaultStreamedContent(inputStream, null,	documentFileName);
			inputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return streamedContentFile;
	}
	
	/**
	 * Gets the holder account status type list.
	 *
	 * @return the holder account status type list
	 */
	public List<HolderAccountStatusType> getHolderAccountStatusTypeList(){
		return HolderAccountStatusType.list;
	}
	
	/**
	 * Search bank on list from pk.
	 *
	 * @param idBankPk the id bank pk
	 * @return the bank
	 */
	public Bank searchBankOnListFromPK(Long idBankPk){
		for(Bank bank: this.getBankList()){
			if(bank.getIdBankPk().equals(idBankPk)){
				return bank;
			}
		}
		return new Bank();
	}
	
	/**
	 * Search participant on list from pk.
	 *
	 * @param idParticipantPk the id participant pk
	 * @return the participant
	 * this method is useful to get Bank from idBankPk because with this method
	 * it's not necesary to getParticipant from database.
	 */
	public Participant searchParticipantOnListFromPK(Long idParticipantPk){
		for(Participant participant: this.getParticipantList()){
			if(participant.getIdParticipantPk().equals(idParticipantPk)){
				return participant;
			}
		}
		return null;
	}

	
	/**
	 * Gets the holder account request hy session.
	 *
	 * @return the holder account request hy session
	 */
	public HolderAccount getHolderAccountSession() {
		return holderAccountSession;
	}

	/**
	 * Sets the holder account request hy session.
	 *
	 * @param holderAccountRequestHySession the new holder account request hy session
	 */
	public void setHolderAccountSession(HolderAccount holderAccountRequestHySession) {
		this.holderAccountSession = holderAccountRequestHySession;
	}

	/**
	 * Gets the holder account to.
	 *
	 * @return the holder account to
	 */
	public HolderAccountTO getHolderAccountTO() {
		return holderAccountTO;
	}
	
	/**
	 * Sets the holder account to.
	 *
	 * @param holderAccountTO the new holder account to
	 */
	public void setHolderAccountTO(HolderAccountTO holderAccountTO) {
		this.holderAccountTO = holderAccountTO;
	}

	/**
	 * Gets the holder account movement.
	 *
	 * @return the holder account movement
	 */
	public HolderAccountMovement getHolderAccountMovement() {
		return holderAccountMovement;
	}

	/**
	 * Sets the holder account movement.
	 *
	 * @param holderAccountMovement the new holder account movement
	 */
	public void setHolderAccountMovement(HolderAccountMovement holderAccountMovement) {
		this.holderAccountMovement = holderAccountMovement;
	}
	/**
	 * Gets the participant list.
	 *
	 * @return the participant list
	 */
	public List<Participant> getParticipantList() {
		return participantList;
	}

	/**
	 * Sets the participant list.
	 *
	 * @param participantList the new participant list
	 */
	public void setParticipantList(List<Participant> participantList) {
		this.participantList = participantList;
	}

	/**
	 * Gets the bank list.
	 *
	 * @return the bank list
	 */
	public List<Bank> getBankList() {
		return bankList;
	}

	/**
	 * Sets the bank list.
	 *
	 * @param bankList the new bank list
	 */
	public void setBankList(List<Bank> bankList) {
		this.bankList = bankList;
	}

	/**
	 * Gets the holder account request data model.
	 *
	 * @return the holder account request data model
	 */
	public GenericDataModel<HolderAccountResultTO> getHolderAccountResultDataModel() {
		return holderAccountResultDataModel;
	}

	/**
	 * Sets the holder account request hy data model.
	 *
	 * @param holderAccountRequestHyDataModel the new holder account request hy data model
	 */
	public void setHolderAccountDataModel(GenericDataModel<HolderAccountResultTO> holderAccountRequestHyDataModel) {
		this.holderAccountResultDataModel = holderAccountRequestHyDataModel;
	}

	/**
	 * Gets the holder account request detail request.
	 *
	 * @return the holder account request detail request
	 */
	public Holder getHolderRequest() {
		return holderRequest;
	}

	/**
	 * Sets the holder account request detail.
	 *
	 * @param holderAccountReqDetHyRequest the new holder account request detail
	 */
	public void setHolderRequest(Holder holderAccountReqDetHyRequest) {
		this.holderRequest = holderAccountReqDetHyRequest;
	}

	/**
	 * Checks if is show group account.
	 *
	 * @return true, if is show group account
	 */
	public boolean isShowGroupAccount() {
		return showGroupAccount;
	}

	/**
	 * Sets the show group account.
	 *
	 * @param showGroupAccount the new show group account
	 */
	public void setShowGroupAccount(boolean showGroupAccount) {
		this.showGroupAccount = showGroupAccount;
	}

	/**
	 * Gets the parameter table money type list.
	 *
	 * @return the parameter table money type list
	 */
	public List<ParameterTable> getParameterTableMoneyTypeList() {
		return parameterTableMoneyTypeList;
	}

	/**
	 * Sets the parameter table money type list.
	 *
	 * @param parameterTableMoneyTypeList the new parameter table money type list
	 */
	public void setParameterTableMoneyTypeList(List<ParameterTable> parameterTableMoneyTypeList) {
		this.parameterTableMoneyTypeList = parameterTableMoneyTypeList;
	}

	/**
	 * Gets the parameter table documents list.
	 *
	 * @return the parameter table documents list
	 */
	public List<ParameterTable> getParameterTableDocumentsList() {
		return parameterTableDocumentsList;
	}

	/**
	 * Sets the parameter table documents list.
	 *
	 * @param parameterTableDocumentsList the new parameter table documents list
	 */
	public void setParameterTableDocumentsList(List<ParameterTable> parameterTableDocumentsList) {
		this.parameterTableDocumentsList = parameterTableDocumentsList;
	}

	/**
	 * Gets the holder account result session.
	 *
	 * @return the holder account result session
	 */
	public HolderAccountResultTO getHolderAccountResultSession() {
		return holderAccountResultSession;
	}

	/**
	 * Sets the holder account result session.
	 *
	 * @param holderAccountResultSession the new holder account result session
	 */
	public void setHolderAccountResultSession(HolderAccountResultTO holderAccountResultSession) {
		this.holderAccountResultSession = holderAccountResultSession;
	}

	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * Gets the holder account history list.
	 *
	 * @return the holder account history list
	 */
	public List<HolderAccountRequest> getListHistoryHolderAccount() {
		return listHistoryHolderAccount;
	}

	/**
	 * Sets the holder account history list.
	 *
	 * @param listHistoryHolderAccount the new list history holder account
	 */
	public void setListHistoryHolderAccount(List<HolderAccountRequest> listHistoryHolderAccount) {
		this.listHistoryHolderAccount = listHistoryHolderAccount;
	}

	/**
	 * Gets the account type list.
	 *
	 * @return the account type list
	 */
	public List<ParameterTable> getParameterTableAccoutsTypeList() {
		return parameterTableAccoutsTypeList;
	}

	/**
	 * Sets the account type list.
	 *
	 * @param parameterTableAccoutsTypeList the new parameter table accouts type list
	 */
	public void setParameterTableAccoutsTypeList(List<ParameterTable> parameterTableAccoutsTypeList) {
		this.parameterTableAccoutsTypeList = parameterTableAccoutsTypeList;
	}

	/**
	 * Gets the parameter table to.
	 *
	 * @return the parameter table to
	 */
	public ParameterTableTO getParameterTableTO() {
		return parameterTableTO;
	}

	/**
	 * Sets the parameter table to.
	 *
	 * @param parameterTableTO the new parameter table to
	 */
	public void setParameterTableTO(ParameterTableTO parameterTableTO) {
		this.parameterTableTO = parameterTableTO;
	}

	/**
	 * Gets the account group list.
	 *
	 * @return the account group list
	 */
	public List<ParameterTable> getParameterTableAccountGroupList() {
		return parameterTableAccountGroupList;
	}

	/**
	 * Sets the account group list.
	 *
	 * @param parameterTableAccountGroupList the new parameter table account group list
	 */
	public void setParameterTableAccountGroupList(List<ParameterTable> parameterTableAccountGroupList) {
		this.parameterTableAccountGroupList = parameterTableAccountGroupList;
	}

	/**
	 * Gets the table status list.
	 *
	 * @return the table status list
	 */
	public List<ParameterTable> getParameterTableStatusList() {
		return parameterTableStatusList;
	}

	/**
	 * Sets the table status list.
	 *
	 * @param parameterTableStatusList the new parameter table status list
	 */
	public void setParameterTableStatusList(List<ParameterTable> parameterTableStatusList) {
		this.parameterTableStatusList = parameterTableStatusList;
	}
	
	/**
	 * Gets the streamed content temporal file holder Account history.
	 *
	 * @param holderAccountFileHy the holder account file hy
	 * @return the streamed content temporal file holder Account history
	 */
	public StreamedContent getStreamedContentFileHolderAccount(HolderAccountFile holderAccountFileHy) {				
		InputStream is = null;				
		byte[] fileAccount = new byte[]{};
		try {
				fileAccount = holderAccountServiceFacade.getHolderAccountFileFacade(holderAccountFileHy.getIdHolderAccountFilePk());
		} catch (ServiceException e) {
				e.printStackTrace();
		}
		is = new ByteArrayInputStream(fileAccount);												
		return new DefaultStreamedContent(is,"application/pdf",holderAccountFileHy.getFilename());
	}

	/**
	 * Checks if is flag exist participant dpf.
	 *
	 * @return the flagExistParticipantDPF
	 */
	public boolean isFlagExistParticipantDPF() {
		return flagExistParticipantDPF;
	}

	/**
	 * Sets the flag exist participant dpf.
	 *
	 * @param flagExistParticipantDPF the flagExistParticipantDPF to set
	 */
	public void setFlagExistParticipantDPF(boolean flagExistParticipantDPF) {
		this.flagExistParticipantDPF = flagExistParticipantDPF;
	}
	
	public boolean isBlPdd() {
		return blPdd;
	}

	public void setBlPdd(boolean blPdd) {
		this.blPdd = blPdd;
	}
	
	public String getfUploadFileSizeDisplayMgmt() {
		return fUploadFileSizeDisplayMgmt;
	}

	public void setfUploadFileSizeDisplayMgmt(String fUploadFileSizeDisplayMgmt) {
		this.fUploadFileSizeDisplayMgmt = fUploadFileSizeDisplayMgmt;
	}
	
}