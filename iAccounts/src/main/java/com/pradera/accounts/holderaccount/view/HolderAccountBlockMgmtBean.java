package com.pradera.accounts.holderaccount.view;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.accounts.holder.facade.HolderServiceFacade;
import com.pradera.accounts.holderaccount.facade.HolderAccountCloseFacade;
import com.pradera.accounts.participants.facade.ParticipantServiceFacade;
import com.pradera.accounts.participants.to.MotiveController;
import com.pradera.accounts.util.view.PropertiesConstants;
import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.BackDialogController;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountBank;
import com.pradera.model.accounts.holderaccounts.HolderAccountBankRequest;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetRequest;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.HolderAccountFileRequest;
import com.pradera.model.accounts.holderaccounts.HolderAccountRequest;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestBlockMotiveType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestHysStatusType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestHysType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class HolderAccountMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/02/2013
 */
@DepositaryWebBean
public class HolderAccountBlockMgmtBean extends GenericBaseBean implements Serializable{
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The holder account request hy session. */
	private HolderAccountRequest holderAccountRequestHySession;
	
	/** The country residence. */
	@Inject @Configurable Integer countryResidence;
	
	/** The holder account to. */
	private HolderAccountTO holderAccountTO;
	
	/** The participant list. */
	private List<Participant> participantList;
	
	/** The holder account request data model. */
	private GenericDataModel<HolderAccountRequest> holderAccountRequestHyDataModel;
	
	/** The motive controller. */
	private MotiveController motiveController;
	
	/** The parameter table motive list. */
	private List<ParameterTable> parameterTableMotiveHolderAccountBlock;
	
	/** The parameter table type document list. */
	private List<ParameterTable> documentsHolderAccountBlock;
	
	/** The Constant ERROR_REJECT. */
	private static final long ERROR_REJECT = 482;
	
	/** The Constant ERROR_ANULATE. */
	private static final long ERROR_ANULATE = 486;
	
	/** The holder account service facade. */
	@EJB
	HolderAccountCloseFacade holderAccountServiceFacade;
	
	/** The accounts facade. */
	@EJB
	ParticipantServiceFacade participantServiceFacade;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	/** The accounts facade. */
	@EJB
	private AccountsFacade accountsFacade;
	
	/** The list request block. */
	private List<ParameterTable> lstRequestestBlock;
	
	/** The back dialog controller. */
	private BackDialogController backDialogController;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	private UserPrivilege userPrivilege;
	
	/** The current date. */
	private Date currentDate = new Date();
	
	/** The parameter table type of action list. */
	private List<ParameterTable> parameterTableHolderAccountRequestHysForBlockList;
	
	/** The parameter table status list. */
	private List<ParameterTable> parameterTableStatusList;
	
	/** The parameter table accounts type list. */
	private List<ParameterTable> parameterTableAccoutsTypeList;
	
	/** The parameter table to. */
	private ParameterTableTO parameterTableTO;
	
	/** The attach file holder account selected. */
	private Integer attachFileHolderAccountSelected;
	
	/** The Account holder file name display. */
	private String accountHolderFileNameDisplay;
	
	/** The Constant SEARCH_ACTION_ACCOUNT_BLOCK. */
	private static final String SEARCH_ACTION_ACCOUNT_BLOCK = "searchHolderAccountBlock";
	
	/** The holder request. */
	private Holder holderRequest;
	
	/** The holder service facade. */
	@EJB
	HolderServiceFacade holderServiceFacade;
	
	/** The upload file size mgmt. */
	private String fUploadFileSizeMgmt="3000000";
	
	private String pdfFileUploadFiltTypes="pdf";
	private String pdfFileUploadFileTypesDisplay="*.pdf";
	
	/** The upload file size display mgmt. */
	private String fUploadFileSizeDisplayMgmt="3000";
	
	/** The str file. */
	private String strFile="Archivo ";
	
	/** The str kb. */
	private String strKb=" KB";
	
	/**
	 * Instantiates a new holder account modify bean.
	 */
	public HolderAccountBlockMgmtBean() {
		super();
		cleanHolderAccountTO();
	}
	
	/**
	 * Clean holder account to.
	 */
	private void cleanHolderAccountTO(){
		holderAccountTO = new HolderAccountTO();
		holderAccountTO.setInitialDate(CommonsUtilities.currentDate());
		holderAccountTO.setFinalDate(CommonsUtilities.currentDate());
		holderAccountTO.setNeedHolder(Boolean.TRUE);
		holderAccountTO.setNeedParticipant(Boolean.FALSE);
	}
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){		
		beginConversation();
		this.setHolderAccountRequestHySession(new HolderAccountRequest());
		this.getHolderAccountRequestHySession().setHolderAccount(new HolderAccount());
		motiveController = new MotiveController();		
		holderAccountRequestHySession.setHolderAccountDetRequest(new ArrayList<HolderAccountDetRequest>());
		holderAccountRequestHySession.setHolderAccountBankRequest(new ArrayList<HolderAccountBankRequest>());
		holderAccountRequestHySession.setHolderAccountFilesRequest(new ArrayList<HolderAccountFileRequest>());	
		Participant participantTO = new Participant();
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(BooleanType.YES.getCode());
		participantTO.setState(ParticipantStateType.REGISTERED.getCode());
		holderRequest = new Holder();
		try {
			this.setParticipantList(accountsFacade.getLisParticipantServiceBean(participantTO));
			this.getMotiveController().setMotiveList(generalParametersFacade.getListParameterTableServiceBean(parameterTableTO.setParaterTablePkFromMaster(MasterTableType.HOLDER_ACCOUNT_REQUEST_ANNULAR_MOTIVE)),MasterTableType.HOLDER_ACCOUNT_REQUEST_ANNULAR_MOTIVE);
			this.getMotiveController().setMotiveList(generalParametersFacade.getListParameterTableServiceBean(parameterTableTO.setParaterTablePkFromMaster(MasterTableType.HOLDER_ACCOUNT_REQUEST_REJECT_MOTIVE)),MasterTableType.HOLDER_REQUEST_REJECT_MOTIVE);
			parameterTableTO.setOrderbyParameterTableCd(BooleanType.YES.getCode());
			this.setParameterTableMotiveHolderAccountBlock(generalParametersFacade.getListParameterTableServiceBean(parameterTableTO.setParaterTablePkFromMaster(MasterTableType.MOTIVE_BLOCK_HOLDERACCOUNT)));
			loadCboListRequestestBlock();			
			parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_REQUEST_STATUS.getCode());
			this.setParameterTableStatusList(generalParametersFacade.getListParameterTableServiceBean(this.getParameterTableTO()));
			parameterTableTO.setMasterTableFk(MasterTableType.ACCOUNTS_TYPE.getCode());			
			this.setParameterTableAccoutsTypeList(generalParametersFacade.getListParameterTableServiceBean(this.getParameterTableTO()));
			parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_TYPE_ACTION.getCode());
			parameterTableTO.setIndicator1(BooleanType.YES.getCode().toString());
			this.setParameterTableHolderAccountRequestHysForBlockList(generalParametersFacade.getListParameterTableServiceBean(this.getParameterTableTO()));			
		} catch (ServiceException e) {
			e.printStackTrace();
		}		
		loadPrivilegesUser();
		evaluateUserSession();
		evaluateUserInfoActive();
		loadHolidays();
		loadingDocumentAttachByAccountHolder();	
		attachFileHolderAccountSelected = GeneralConstants.NEGATIVE_ONE_INTEGER;
		accountHolderFileNameDisplay=GeneralConstants.EMPTY_STRING;	
	}
	
	/**
	 * Load holidays.
	 */
	private void loadHolidays() {
		// TODO Auto-generated method stub
		 Holiday holidayFilter = new Holiday();
		 holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		 GeographicLocation countryFilter = new GeographicLocation();
		 countryFilter.setIdGeographicLocationPk(countryResidence);
		 holidayFilter.setGeographicLocation(countryFilter);
		 try {
			allHolidays = CommonsUtilities.convertListDateToString(generalParametersFacade.getListHolidayDateServiceFacade(holidayFilter));
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Load privileges user.
	 */
	public void loadPrivilegesUser() {
		// TODO Auto-generated method stub
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnApproveView(Boolean.TRUE);
		privilegeComponent.setBtnAnnularView(Boolean.TRUE);
		privilegeComponent.setBtnConfirmView(Boolean.TRUE);
		privilegeComponent.setBtnRejectView(Boolean.TRUE);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
		userPrivilege.getUserAcctions();
	}
	
	/**
	 * Evaluate user session.
	 */
	private void evaluateUserSession() {
		// TODO Auto-generated method stub
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			Long participantCode = userInfo.getUserAccountSession().getParticipantCode();
			holderAccountRequestHySession.setParticipant(searchParticipantOnListFromPK(participantCode));
			holderAccountTO.setParticipantTO(participantCode);
		}else if(userInfo.getUserAccountSession().isDepositaryInstitution()){
			cleanHolderAccountTO();
		}
	}
		
	/**
	 * Load type the files.
	 */
	public void loadTypeFiles(){		
		try{
			if (holderAccountRequestHySession.getRequestMotive()!=null && !(holderAccountRequestHySession.getRequestMotive().equals(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER)))) {									
				parameterTableTO.setMasterTableFk(MasterTableType.HOLDERACCOUNT_DOCUMENTS_BLOCK.getCode());	
				parameterTableTO.setIndicator1(holderAccountRequestHySession.getRequestMotive().toString());			
				this.setDocumentsHolderAccountBlock(generalParametersFacade.getListParameterTableServiceBean(this.getParameterTableTO()));			
			} else {
				this.setDocumentsHolderAccountBlock(null);		
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Load select list request block.
	 */
	public void loadCboListRequestestBlock(){
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.PARAMETER_REQUESTER_BLOCK_UNBLOC_HOLDERACCOUNT.getCode());
		try {
			this.lstRequestestBlock = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Clean holder account on holder account request handler.
	 */
	public void cleanHolderAccountOnHolderAccountRequestHandler(){
		holderAccountRequestHySession.getHolderAccount().setIdHolderAccountPk(0L);
		holderAccountRequestHySession.getHolderAccount().setAccountNumber(null);
		holderAccountRequestHySession.getHolderAccount().setAlternateCode(null);
		holderAccountRequestHySession.getHolderAccount().setParticipant(new Participant());
		holderAccountRequestHySession.getHolderAccount().setAccountType(0);
		holderAccountRequestHySession.getHolderAccount().setHolderAccountDetails(null);
		holderAccountRequestHySession.getHolderAccount().setAccountType(null);
		holderAccountRequestHySession.setRequestMotive(null);
		holderAccountRequestHySession.setComments(null);
		this.setDocumentsHolderAccountBlock(null);
		this.setAttachFileHolderAccountSelected(null);
		JSFUtilities.resetComponent("frmHolderAccountMgmt:cboRequestType");
		JSFUtilities.resetComponent("frmHolderAccountMgmt:arkAccuntId");
		JSFUtilities.resetComponent("frmHolderAccountMgmt:cboMotive");
	}
	
	/**
	 * Evaluate change request handler.
	 */
	public void evaluateChangeRequestHandler(){
		cleanHolderAccountOnHolderAccountRequestHandler();
		if(holderAccountRequestHySession.getRequestType()!=null && this.getParameterTableMotiveHolderAccountBlock()!=null){
			for(ParameterTable parameterTable: parameterTableMotiveHolderAccountBlock){
				if(holderAccountRequestHySession.getRequestType().equals(HolderAccountRequestHysType.UNBLOCK.getCode())  &&
				   parameterTable.getParameterTablePk().equals(HolderAccountRequestBlockMotiveType.DIED.getCode())){					
					parameterTable.setIndicator1(BooleanType.YES.getCode().toString());return;
				}else{
					parameterTable.setIndicator1(BooleanType.NO.getCode().toString());
				}
			}
		}
	}
	
	/**
	 * Gets the holder account request type for block.
	 *
	 * @return the holder account request type for block
	 */
	public List<HolderAccountRequestHysType> getHolderAccountRequestHysTypeForBlock(){
		return HolderAccountRequestHysType.listSomeElements(HolderAccountRequestHysType.BLOCK,HolderAccountRequestHysType.UNBLOCK,HolderAccountRequestHysType.CLOSE);
	}
	
	/**
	 * Removes the holder on data table handler.
	 *
	 * @param holderAccountReqDetHys the holder account request detail
	 */
	public void removeHolderOnDataTableHandler(HolderAccountDetRequest holderAccountReqDetHys){
		holderAccountRequestHySession.getHolderAccountDetRequest().remove(holderAccountReqDetHys);
	} 
	
	/**
	 * Clean holder account request.
	 */
	public void cleanHolderAccountRequest(){
		hideLocalComponents();
		JSFUtilities.resetComponent("frmHolderAccountMgmt:flsHolderAccountBlock");
		registryHolderAccountBlockRequestHandler();
	}
	
	/**
	 * Registry holder account modify request handler.
	 */
	public void registryHolderAccountBlockRequestHandler(){
		holderAccountTO.setParticipantTO(null);
		this.setDocumentsHolderAccountBlock(null);	
		this.setHolderAccountRequestHySession(new HolderAccountRequest());
		holderAccountRequestHySession.setHolderAccount(new HolderAccount());
		holderAccountRequestHySession.getHolderAccount().setParticipant(new Participant()); 
		holderAccountRequestHySession.setHolderAccountDetRequest(new ArrayList<HolderAccountDetRequest>());
		holderAccountRequestHySession.setHolderAccountBankRequest(new ArrayList<HolderAccountBankRequest>());
		holderAccountRequestHySession.setHolderAccountFilesRequest(new ArrayList<HolderAccountFileRequest>());
		holderAccountRequestHySession.setParticipant(searchParticipantOnListFromPK(holderAccountTO.getParticipantTO()));		
		evaluateUserSession();
		backDialogController = new BackDialogController();
		holderRequest = new Holder();
		holderAccountRequestHySession.setRequestType(null);
		this.getBackDialogController().setObjectBefore(new StringBuilder(this.getHolderAccountRequestHySession().toString() + this.getHolderRequest().toString() + this.getHolderAccountTO().getParticipantTO()));		
	}
	
	/**
	 * Asignate holder main.
	 *
	 * @param holderAccountDetailParam the holder account detail param
	 */
	public void asignateHolderMain(HolderAccountDetail holderAccountDetailParam){
		for(HolderAccountDetail holderAccountDetail: holderAccountRequestHySession.getHolderAccount().getHolderAccountDetails()){
			holderAccountDetail.setIndRepresentative(BooleanType.NO.getCode());
		}
		holderAccountDetailParam.setIndRepresentative(BooleanType.YES.getCode());
	}
	
	/**
	 * Search holder account hanlder.
	 */
	public void searchHolderAccountHanlder(){
		executeAction();
		if(this.getHolderAccountRequestHySession().getHolderAccount().getAccountNumber() != null && Validations.validateIsPositiveNumber(this.getHolderAccountRequestHySession().getHolderAccount().getAccountNumber())){
			if(Validations.validateIsNull(this.getHolderAccountTO().getParticipantTO())){
				hideLocalComponents();
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_PARTICIPANT,null);			
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			holderAccountTO.setHolderAccountNumber(this.getHolderAccountRequestHySession().getHolderAccount().getAccountNumber());
			holderAccountTO.setNeedBanks(Boolean.TRUE);
			holderAccountTO.setNeedHolder(Boolean.TRUE);
			//Validate if exist some request not Approve And Not Confirm
			HolderAccountRequest holderAccountRequest = null;
			try {
				holderAccountRequest = holderAccountServiceFacade.haveRequestWithOutConfirmationServiceFacade(holderAccountTO);
			} catch (ServiceException e1) {
				e1.printStackTrace();
			}
			if(holderAccountRequest!=null){
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_REQUEST_EXIST,
						new Object[]{getWordFormated(holderAccountRequest.getRequestTypeDescription()),holderAccountTO.getHolderAccountNumber().toString()});
				JSFUtilities.resetComponent("frmHolderAccountMgmt:arkAccuntId");
				JSFUtilities.showSimpleValidationDialog();
				holderAccountRequestHySession.setHolderAccount(new HolderAccount());
				holderAccountRequestHySession.setRequestMotive(null); 
				holderAccountRequestHySession.setComments(null);
				holderAccountRequestHySession.setRequesterBlock(null);			
				return;
			}
			//END -- Validate if exist some request not Approve And Not Confirm
			
			try {
				if(this.getHolderAccountRequestHySession().getHolderAccount()==null){
					holderAccountRequestHySession.setHolderAccount(holderAccountServiceFacade.getHolderAccountServiceFacade(holderAccountTO));
				}
				if(Validations.validateIsNull(this.getHolderAccountRequestHySession().getHolderAccount())){
					showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_NUMBER,
										new String[]{String.valueOf(holderAccountTO.getHolderAccountNumber()),this.searchParticipantOnListFromPK(holderAccountTO.getParticipantTO()).getMnemonic()});
					JSFUtilities.resetComponent("frmHolderAccountMgmt:arkAccuntId");
					JSFUtilities.showSimpleValidationDialog();
					holderAccountRequestHySession.setHolderAccount(new HolderAccount());
					holderAccountRequestHySession.setRequestMotive(null); 
					holderAccountRequestHySession.setComments(null);
					holderAccountRequestHySession.setRequesterBlock(null);				
					return;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			if(this.getHolderAccountRequestHySession().getHolderAccount().getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode()) && 
			   this.getHolderAccountRequestHySession().getRequestType().equals(HolderAccountRequestHysType.UNBLOCK.getCode())){
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_HOLDERACCOUNT_UNBLOCK,
									new String[]{String.valueOf(holderAccountTO.getHolderAccountNumber()),HolderAccountStatusType.lookup.get(this.getHolderAccountRequestHySession().getHolderAccount().getStateAccount()).getValue()});
				holderAccountRequestHySession.setHolderAccount(new HolderAccount());
				holderAccountRequestHySession.setRequestMotive(null); 
				holderAccountRequestHySession.setComments(null);
				holderAccountRequestHySession.setRequesterBlock(null);			
				JSFUtilities.resetComponent("frmHolderAccountMgmt:arkAccuntId");
				JSFUtilities.showSimpleValidationDialog();
				return;
			}else if(this.getHolderAccountRequestHySession().getHolderAccount().getStateAccount().equals(HolderAccountStatusType.BLOCK.getCode()) && 
					   (this.getHolderAccountRequestHySession().getRequestType().equals(HolderAccountRequestHysType.BLOCK.getCode()) || this.getHolderAccountRequestHySession().getRequestType().equals(HolderAccountRequestHysType.CLOSE.getCode()))){
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_HOLDERACCOUNT,
									new String[]{String.valueOf(holderAccountTO.getHolderAccountNumber()),HolderAccountStatusType.lookup.get(this.getHolderAccountRequestHySession().getHolderAccount().getStateAccount()).getValue()});
				holderAccountRequestHySession.setHolderAccount(new HolderAccount());
				holderAccountRequestHySession.setRequestMotive(null); 
				holderAccountRequestHySession.setComments(null);
				holderAccountRequestHySession.setRequesterBlock(null);			
				JSFUtilities.resetComponent("frmHolderAccountMgmt:arkAccuntId");
				JSFUtilities.showSimpleValidationDialog();
				return;
			}else if(this.getHolderAccountRequestHySession().getHolderAccount().getStateAccount().equals(HolderAccountStatusType.CLOSED.getCode())){
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_HOLDERACCOUNT,
									new String[]{String.valueOf(holderAccountTO.getHolderAccountNumber()),HolderAccountStatusType.lookup.get(this.getHolderAccountRequestHySession().getHolderAccount().getStateAccount()).getValue()});
				JSFUtilities.resetComponent("frmHolderAccountMgmt:arkAccuntId");
				JSFUtilities.showSimpleValidationDialog();
				holderAccountRequestHySession.setHolderAccount(new HolderAccount());
				holderAccountRequestHySession.setRequestMotive(null); 
				holderAccountRequestHySession.setComments(null);
				holderAccountRequestHySession.setRequesterBlock(null);			
				return;
			}

			try{
				if(this.holderAccountRequestHySession.getRequestType().equals(HolderAccountRequestHysType.CLOSE.getCode()) && 
				   !holderAccountServiceFacade.holderAccountCanWillClosedServiceFacade(this.getHolderAccountRequestHySession().getHolderAccount())){
					showMessageOnDialogOnlyKey(PropertiesConstants.MESSAGES_ALERT, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_CLOSE_WITH_BALANCES);
					holderAccountRequestHySession.setHolderAccount(new HolderAccount());
					holderAccountRequestHySession.setRequestMotive(null); 
					holderAccountRequestHySession.setComments(null);
					holderAccountRequestHySession.setRequesterBlock(null);				
					JSFUtilities.resetComponent("frmHolderAccountMgmt:arkAccuntId");
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
				if(!this.getHolderAccountRequestHySession().getHolderAccount().validateIfRntIsRegistered()){
					showMessageOnDialogOnlyKey(PropertiesConstants.MESSAGES_ALERT, PropertiesConstants.HOLDERACCOUNT_VALIDATION_HOLDER_BLOCK);
					JSFUtilities.resetComponent("frmHolderAccountMgmt:arkAccuntId");
					JSFUtilities.showSimpleValidationDialog();
					holderAccountRequestHySession.setHolderAccount(new HolderAccount());
					holderAccountRequestHySession.setRequestMotive(null); 
					holderAccountRequestHySession.setComments(null);
					holderAccountRequestHySession.setRequesterBlock(null);				
					return;
				}
			}catch(ServiceException e){
			}			
			this.getHolderAccountRequestHySession().getHolderAccount().setParticipant(this.searchParticipantOnListFromPK(holderAccountTO.getParticipantTO()));
			JSFUtilities.resetComponent("frmHolderAccountMgmt:arkAccuntId");
			JSFUtilities.hideGeneralDialogues();
		}	
	}
	
	/**
	 * Evaluate back.
	 *
	 * @return the string
	 */
	public String evaluateBack(){
		hideLocalComponents();
		holderAccountRequestHySession.setParticipant(searchParticipantOnListFromPK(holderAccountTO.getParticipantTO()));
		backDialogController.setObjectAfter(new StringBuilder(this.getHolderAccountRequestHySession().toString()));
		if(backDialogController.isShowModal()){
			JSFUtilities.showComponent(":frmHolderAccountMgmt:cnfBackAdvancedAccount");
			return GeneralConstants.EMPTY_STRING;
		}
		return SEARCH_ACTION_ACCOUNT_BLOCK;
	}
	
	/**
	 * Execute back page hanlder.
	 */
	public void executeBackPageHanlder(){
		if(holderAccountRequestHyDataModel==null || holderAccountRequestHyDataModel.getSize()==0){
			evaluateUserSession();
		}
	}
	/**
	 * Clean holder account to handler.
	 */
	public void cleanHolderAccountTOHandler(){
		this.setHolderAccountTO(new HolderAccountTO());
		holderAccountTO.setInitialDate(CommonsUtilities.currentDate());
		holderAccountTO.setFinalDate(CommonsUtilities.currentDate());
		this.setHolderAccountRequestHyDataModel(null);
		JSFUtilities.resetViewRoot();
		evaluateUserSession();
	}
	
	/**
	 * Before save holder account modify request handler.
	 */
	public void beforeSaveHolderAccountBlockRequestHandler(){
		hideLocalComponents();		
		if(Validations.validateIsNullOrEmpty(this.getHolderAccountTO().getParticipantTO()) ||
		   !Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(this.getHolderAccountTO().getParticipantTO())))){
			JSFUtilities.addContextMessage("frmHolderAccountMgmt:cboParticipantTo",FacesMessage.SEVERITY_ERROR,PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_PARTICIPANT), PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_PARTICIPANT));
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MESSAGE_DATA_REQUIRED, null);
			JSFUtilities.showValidationDialog();
			return;
		}else if(Validations.validateIsNullOrEmpty(this.getHolderAccountRequestHySession().getRequestType()) ||
				   !Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(this.getHolderAccountRequestHySession().getRequestType())))){
			JSFUtilities.addContextMessage("frmHolderAccountMgmt:cboRequestType",FacesMessage.SEVERITY_ERROR,PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_BLOCKTYPE), PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_BLOCKTYPE));
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_BANK_EMPTY, null);
			JSFUtilities.showValidationDialog();
			return;
		}else if(Validations.validateIsNullOrEmpty(this.getHolderAccountRequestHySession().getRequestMotive()) ||
				!Validations.validateIsPositiveNumber(this.getHolderAccountRequestHySession().getRequestMotive())){
			JSFUtilities.addContextMessage("frmHolderAccountMgmt:cboMotive",FacesMessage.SEVERITY_ERROR,PropertiesUtilities.getMessage(PropertiesConstants.LBL_OTHER_MOTIVE), PropertiesUtilities.getMessage(PropertiesConstants.LBL_OTHER_MOTIVE));
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MESSAGE_DATA_REQUIRED, null);
			JSFUtilities.showValidationDialog();
			return;
		}else if(Validations.validateIsNullOrEmpty(this.getHolderAccountRequestHySession().getHolderAccount().getAccountNumber()) ||
				!Validations.validateIsPositiveNumber(this.getHolderAccountRequestHySession().getHolderAccount().getAccountNumber())){
			JSFUtilities.addContextMessage("frmHolderAccountMgmt:txtAccountNumber",FacesMessage.SEVERITY_ERROR,PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_ACCOUNTNUMBER), PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_ACCOUNTNUMBER));
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MESSAGE_DATA_REQUIRED, null);
			JSFUtilities.showValidationDialog();
			return;
		}		
		/*Validacion para la carga de archivo*/
//		Integer contCombo = BooleanType.NO.getCode();
//		Integer contTable = BooleanType.NO.getCode();			
//		for(int i=0;i<documentsHolderAccountBlock.size();i++){				
//			if(documentsHolderAccountBlock.get(i).getShortInteger().equals(BooleanType.YES.getCode())){	
//				contCombo ++;
//				for ( HolderAccountFileRequest holderAccountFile : holderAccountRequestHySession.getHolderAccountFilesRequest()){						
//					if(documentsHolderAccountBlock.get(i).getParameterTablePk().equals(holderAccountFile.getDocumentType())){
//						contTable ++;							
//					}						
//				}
//			}
//		}
		
//		if (contCombo.equals(contTable)) {
//			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_FILE_REQUIRED,null);
//			JSFUtilities.showSimpleValidationDialog();
//			return;
//		}
		JSFUtilities.hideGeneralDialogues();		
		holderAccountRequestHySession.setRequestNumber(holderAccountRequestHySession.getIdHolderAccountReqPk());
		holderAccountRequestHySession.setParticipant(searchParticipantOnListFromPK(holderAccountRequestHySession.getHolderAccount().getParticipant().getIdParticipantPk()));
		holderAccountRequestHySession.setComments(holderAccountRequestHySession.getComments());		
		Integer accountNumber = holderAccountRequestHySession.getHolderAccount().getAccountNumber();		
		switch(HolderAccountRequestHysType.lookup.get(this.getHolderAccountRequestHySession().getRequestType())){
			case BLOCK:
				showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ADM_HOLDERACCOUNT_CONFIRMATION_BLOCK, new Object[]{accountNumber.toString()});break;
			case UNBLOCK:
				showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ADM_HOLDERACCOUNT_CONFIRMATION_UNBLOCK, new Object[]{accountNumber.toString()});break;
			case CLOSE:
				showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ADM_HOLDERACCOUNT_CONFIRMATION_CLOSE, new Object[]{accountNumber.toString()});break;
		}
		JSFUtilities.showComponent(":frmHolderAccountMgmt:cnfHolderAccountModifyMgmt");
	}
	
	/**
	 * Save holder account modify handler.
	 * we need to put holderDetail and HolderBanks into HolderAccountRequest. because is Modify
	 */
	@LoggerAuditWeb
	public void saveHolderAccountBlockHandler(){
		this.getHolderAccountRequestHySession().setRequestState(HolderAccountRequestHysStatusType.REGISTERED.getCode());
		this.getHolderAccountRequestHySession().setRegistryUser(userInfo.getUserAccountSession().getUserName());
		this.getHolderAccountRequestHySession().setRegistryDate(CommonsUtilities.currentDateTime());
		this.getHolderAccountRequestHySession().setRequestNumber(0L);
		this.getHolderAccountRequestHySession().setHolderAccountGroup(this.getHolderAccountRequestHySession().getHolderAccount().getAccountGroup());
		this.getHolderAccountRequestHySession().setHolderAccountType(this.getHolderAccountRequestHySession().getHolderAccount().getAccountType());						
		try {
			holderAccountServiceFacade.registerHolderAccountRequest(getHolderAccountRequestHySession());
		} catch (ServiceException e) {
			if(e.getErrorService()!=null){
				if(e.getErrorService().equals(ErrorServiceType.HOLDER_ACCOUNT_WITH_BALANCES)){
					showMessageOnDialogOnlyKey(PropertiesConstants.MESSAGES_ALERT, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_CLOSE_WITH_BALANCES);
					JSFUtilities.showComponent(":frmHolderAccountMgmt:holderAccountMgmtOk");
					loadPrivilegesUser();
					this.setViewOperationType(ViewOperationsType.CONSULT.getCode());
					return;
				}else{
					showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
					JSFUtilities.showComponent(":frmHolderAccountMgmt:holderAccountMgmtOk");
					loadPrivilegesUser();
					return;
				}
			}
			e.printStackTrace();
		}
		
		String message = null;
		switch(HolderAccountRequestHysType.lookup.get(holderAccountRequestHySession.getRequestType())){
			case BLOCK: message = PropertiesConstants.ADM_HOLDERACCOUNT_CONFIRMATION_BLOCK_OK;break;
			case UNBLOCK:message = PropertiesConstants.ADM_HOLDERACCOUNT_CONFIRMATION_UNBLOCK_OK;break;
			case CLOSE:message = PropertiesConstants.ADM_HOLDERACCOUNT_CONFIRMATION_CLOSE_OK;break;
		}
		
		showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, message,
				new Object[]{holderAccountRequestHySession.getIdHolderAccountReqPk().toString(),holderAccountRequestHySession.getHolderAccount().getAccountNumber().toString()});
		JSFUtilities.showComponent(":frmHolderAccountMgmt:holderAccountMgmtOk");
	}
	
	/**
	 * Search holder account modify request handler.
	 */
	@LoggerAuditWeb
	public void searchHolderAccountModifyRequestHysHandler(){
		this.hideLocalComponents();
		JSFUtilities.hideGeneralDialogues();
		List<HolderAccountRequest> holderAccountRequestHyList =null;
		HolderAccountTO holderAccountTO = new HolderAccountTO();
		holderAccountTO.setInitialDate(this.holderAccountTO.getInitialDate());
		holderAccountTO.setFinalDate(this.holderAccountTO.getFinalDate());
		holderAccountTO.setRequestNumber(this.holderAccountTO.getRequestNumber());
		holderAccountTO.setStatus(this.holderAccountTO.getStatus());
		holderAccountTO.setParticipantTO(this.holderAccountTO.getParticipantTO());
		holderAccountTO.setHolderAccountRequestType(this.holderAccountTO.getHolderAccountRequestType());
		holderAccountTO.setRequests(this.holderAccountTO.getRequests());		
		holderAccountTO.setNeedHolder(Boolean.TRUE);
		holderAccountTO.setNeedParticipant(Boolean.TRUE);
		if (holderAccountTO.getHolderAccountRequestType() == null || !Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(holderAccountTO.getHolderAccountRequestType())))) {
			JSFUtilities.addContextMessage(
							"frmHolderRequestSearch:cboRequestsType",FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_BLOCKTYPE),
							PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_BLOCKTYPE));
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MESSAGE_DATA_REQUIRED, null);
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		try {
			holderAccountRequestHyList = holderAccountServiceFacade.getHolderAccountRequestHysListServiceFacade(holderAccountTO);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		holderAccountRequestHyDataModel = new GenericDataModel<HolderAccountRequest>(holderAccountRequestHyList);
	}
	
	/**
	 * Selected holder account request detail hanlder.
	 *
	 * @param holderAccountRequestHy the holder account request
	 */
	public void selectedHolderAccountRequestDetailHanlder(HolderAccountRequest holderAccountRequestHy){
		this.setViewOperationType(ViewOperationsType.CONSULT.getCode());
		readObjectFromBD(holderAccountRequestHy);
	}
	
	/**
	 * Before anulate holder account request handler.
	 *
	 * @param e the e
	 */
	@SuppressWarnings("unchecked")
	public void beforeAnulateHolderAccountRequestHandler(ActionEvent e){
		JSFUtilities.resetComponent(":frmMotive:dialogMotive");
		executeAction();
		if(Validations.validateIsNull(this.getHolderAccountRequestHySession())){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
			JSFUtilities.showValidationDialog();
			return;
		}
		JSFUtilities.hideGeneralDialogues();
		hideLocalComponents();
		if(this.motiveController!=null){
			this.motiveController.setIdMotivePK(null);
			this.motiveController.setShowMotiveText(Boolean.FALSE);
			this.motiveController.setMotiveText(null);
		}
		if(this.getHolderAccountRequestHySession().getRequestState().equals(HolderAccountRequestHysStatusType.REGISTERED.getCode())){
			this.getMotiveController().setMotiveList((List<ParameterTable>) MotiveController.getMotiveListCache().get(MasterTableType.HOLDER_REQUEST_ANNULAR_MOTIVE.getCode()));
			this.setViewOperationType(ViewOperationsType.ANULATE.getCode());
			JSFUtilities.showComponent(":frmMotive:dialogMotive");
			return;
		}
		showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_DOACTION, new Object[]{this.getHolderAccountRequestHySession().getStateHolderAccountReqDescription()});
		JSFUtilities.showValidationDialog();
	}
	
	/**
	 * Before approve holder account request handler.
	 *
	 * @param e the e
	 */
	public void beforeApproveHolderAccountRequestHandler(ActionEvent e){
		executeAction();
		if(Validations.validateIsNull(this.getHolderAccountRequestHySession())){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
			JSFUtilities.showValidationDialog();
			return;
		}
		JSFUtilities.hideGeneralDialogues();	
		hideLocalComponents();
		boolean canWillClosed = false;
		if(this.getHolderAccountRequestHySession().getRequestState().equals(HolderAccountRequestHysStatusType.REGISTERED.getCode())){			
			try{
				canWillClosed = holderAccountServiceFacade.holderAccountCanWillClosedServiceFacade(this.getHolderAccountRequestHySession().getHolderAccount());
				if(!canWillClosed){
					if(this.getHolderAccountRequestHySession().getRequestType().equals(HolderAccountRequestHysType.BLOCK.getCode())){
						showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ADM_HOLDERACCOUNT_APPROVE_BLOCK_ACEPTED,null);
						this.setViewOperationType(ViewOperationsType.APPROVE.getCode());
						JSFUtilities.showComponent(":frmAcept:cnfAcept");
						return;
					}else if(this.getHolderAccountRequestHySession().getRequestType().equals(HolderAccountRequestHysType.CLOSE.getCode())){						
						showMessageOnDialogOnlyKey(PropertiesConstants.MESSAGES_ALERT, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_CLOSE_WITH_BALANCES_APPROVE);
						holderAccountRequestHySession.setHolderAccount(new HolderAccount());
						holderAccountRequestHySession.setRequestMotive(null); 
						holderAccountRequestHySession.setComments(null);
						holderAccountRequestHySession.setRequesterBlock(null);							
						JSFUtilities.showValidationDialog();
						return;					
					}
				}											
				showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ADM_HOLDERACCOUNT_APPROVE_BLOCK, 
						new Object[]{holderAccountRequestHySession.getIdHolderAccountReqPk().toString(),holderAccountRequestHySession.getRequestTypeDescription(),
						holderAccountRequestHySession.getHolderAccount().getHolderAccountReqDetCUI()});
				this.setViewOperationType(ViewOperationsType.APPROVE.getCode());
				JSFUtilities.showComponent(":frmBlock:cnfblock");
				return;
			}catch(ServiceException exc){}
		}
		showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_DOACTION, new Object[]{this.getHolderAccountRequestHySession().getStateHolderAccountReqDescription()});
		JSFUtilities.showValidationDialog();
	}
	/**
	 * Hide local components.
	 */
	public void hideLocalComponents(){
		JSFUtilities.hideComponent(":frmMotive:dialogMotive");
		JSFUtilities.hideComponent(":frmBlock:cnfblock");
		JSFUtilities.hideComponent(":frmHolderAccountMgmt:cnfHolderAccountModifyMgmt");
		//JSFUtilities.hideComponent(":frmHolderAccountMgmt:cnfCleanHolderAccountMgmt");
		JSFUtilities.hideComponent(":frmHolderAccountMgmt:cnfBackAdvancedAccount");
		JSFUtilities.hideGeneralDialogues();
	}
	
	/**
	 * Close dialog reject motive.
	 */
	public void closeDialogRejectMotive(){
		JSFUtilities.resetComponent(":frmMotive:dialogMotive");
		hideLocalComponents();
	}
	
	
	/**
	 * Before reject holder account request handler.
	 *
	 * @param e the e
	 */
	@SuppressWarnings("unchecked")
	public void beforeRejectHolderAccountRequestHandler(ActionEvent e){
		JSFUtilities.resetComponent(":frmMotive:dialogMotive");
		executeAction();
		if(Validations.validateIsNull(this.getHolderAccountRequestHySession())){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
			JSFUtilities.showValidationDialog();
			return;
		}
		JSFUtilities.hideGeneralDialogues();
		hideLocalComponents();
		if(this.motiveController!=null){
			this.motiveController.setIdMotivePK(null);
			this.motiveController.setShowMotiveText(Boolean.FALSE);
			this.motiveController.setMotiveText(null);
		}
		if(this.getHolderAccountRequestHySession().getRequestState().equals(HolderAccountRequestHysStatusType.APPROVE.getCode())){
			this.getMotiveController().setMotiveList((List<ParameterTable>) MotiveController.getMotiveListCache().get(MasterTableType.HOLDER_REQUEST_REJECT_MOTIVE.getCode()));
			this.setViewOperationType(ViewOperationsType.REJECT.getCode());
			JSFUtilities.showComponent(":frmMotive:dialogMotive");
			return;
		}
		showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_DOACTION, 
				new Object[]{this.getHolderAccountRequestHySession().getStateHolderAccountReqDescription()});
		JSFUtilities.showValidationDialog();
	}
	

	/**
	 * Remove data holder account request handler.
	 *
	 * @param holderAccountReqBankHy the holder account req bank hy
	 */
	public void removeAccountOnDataTableHandler(HolderAccountBankRequest holderAccountReqBankHy){
		holderAccountRequestHySession.getHolderAccountBankRequest().remove(holderAccountReqBankHy);
	}
	
	/**
	 * Before confirm holder account request handler.
	 *
	 * @param e the e
	 */
	public void beforeConfirmHolderAccountRequestHandler(ActionEvent e){
		executeAction();
		if(Validations.validateIsNull(this.getHolderAccountRequestHySession())){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
			JSFUtilities.showValidationDialog();
			return;
		}
		JSFUtilities.hideGeneralDialogues();
		hideLocalComponents();
		boolean canWillClosed = false;
		if(this.getHolderAccountRequestHySession().getRequestState().equals(HolderAccountRequestHysStatusType.APPROVE.getCode())){
			try{
				canWillClosed = holderAccountServiceFacade.holderAccountCanWillClosedServiceFacade(this.getHolderAccountRequestHySession().getHolderAccount());
				if(!canWillClosed){
					if(this.getHolderAccountRequestHySession().getRequestType().equals(HolderAccountRequestHysType.BLOCK.getCode())){
						showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ADM_HOLDERACCOUNT_CONFIRM_BLOCK_ACEPTED,null);
						this.setViewOperationType(ViewOperationsType.CONFIRM.getCode());
						JSFUtilities.showComponent(":frmAcept:cnfAcept");
						return;
					}else if(this.getHolderAccountRequestHySession().getRequestType().equals(HolderAccountRequestHysType.CLOSE.getCode())){						
						showMessageOnDialogOnlyKey(PropertiesConstants.MESSAGES_ALERT, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_CLOSE_WITH_BALANCES_CONFIRM);
						holderAccountRequestHySession.setHolderAccount(new HolderAccount());
						holderAccountRequestHySession.setRequestMotive(null); 
						holderAccountRequestHySession.setComments(null);
						holderAccountRequestHySession.setRequesterBlock(null);							
						JSFUtilities.showValidationDialog();
						return;					
					}
				}				
				showMessageOnDialog(PropertiesConstants.MESSAGES_REGISTER, null, PropertiesConstants.ADM_HOLDERACCOUNT_CONFIRM_BLOCK, 
						new Object[]{holderAccountRequestHySession.getIdHolderAccountReqPk().toString(),holderAccountRequestHySession.getRequestTypeDescription(),
									 holderAccountRequestHySession.getHolderAccount().getHolderAccountReqDetCUI()});
				this.setViewOperationType(ViewOperationsType.CONFIRM.getCode());
				JSFUtilities.showComponent(":frmBlock:cnfblock");
				return;
			}catch(ServiceException exc){}
		}
		showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_DOACTION,new Object[]{holderAccountRequestHySession.getStateHolderAccountReqDescription()});
		JSFUtilities.showValidationDialog();
	}
	
	/**
	 * Change action holder account request handler.
	 */
	@LoggerAuditWeb
	public void changeActionHolderAccountRequestHandler(){
		hideLocalComponents();
		HolderAccountRequest holdAccReqTemp = new HolderAccountRequest();
		holdAccReqTemp.setIdHolderAccountReqPk(this.getHolderAccountRequestHySession().getIdHolderAccountReqPk());
		holdAccReqTemp.setRequestState(this.getHolderAccountRequestHySession().getRequestState());
		holdAccReqTemp.setRequestType(this.getHolderAccountRequestHySession().getRequestType());
		holdAccReqTemp.setHolderAccountDetRequest(holderAccountRequestHySession.getHolderAccountDetRequest());
		holdAccReqTemp.setHolderAccount(holderAccountRequestHySession.getHolderAccount());
		holdAccReqTemp.setParticipant(holderAccountRequestHySession.getParticipant());		
		boolean isConfirm = Boolean.FALSE;
		String messageResponseOk = null ;
		try {
			switch(ViewOperationsType.lookup.get(this.getViewOperationType())){
				case APPROVE:
					holdAccReqTemp.setApproveDate(new Date());
					holdAccReqTemp.setApproveUser(userInfo.getUserAccountSession().getUserName());
					messageResponseOk = PropertiesConstants.ADM_HOLDERACCOUNT_REQUEST_APPROVE_OK_BLOCK;
					holdAccReqTemp = holderAccountServiceFacade.approveHolderAccountRequest(holdAccReqTemp);break;
				case ANULATE:
					holdAccReqTemp.setAnullDate(new Date());
					holdAccReqTemp.setAnullUser(userInfo.getUserAccountSession().getUserName());
					holdAccReqTemp.setRequestMotive(Integer.parseInt(String.valueOf(this.getMotiveController().getIdMotivePK())));
					holdAccReqTemp.setRequestOtherMotive(this.getMotiveController().getMotiveText());
					messageResponseOk = PropertiesConstants.ADM_HOLDERACCOUNT_REQUEST_ANULATE_OK_BLOCK;
					holdAccReqTemp = holderAccountServiceFacade.annulateHolderAccountRequest(holdAccReqTemp);break;
				case CONFIRM:
					holdAccReqTemp.setConfirmDate(new Date());
					holdAccReqTemp.setConfirmUser(userInfo.getUserAccountSession().getUserName());
					isConfirm = Boolean.TRUE;
					holdAccReqTemp = holderAccountServiceFacade.confirmHolderAccountRequest(holdAccReqTemp);break;
				case REJECT:
					holdAccReqTemp.setRejectDate(new Date());
					holdAccReqTemp.setRejectUser(userInfo.getUserAccountSession().getUserName());
					holdAccReqTemp.setRequestMotive(Integer.parseInt(String.valueOf(this.getMotiveController().getIdMotivePK())));
					holdAccReqTemp.setRequestOtherMotive(this.getMotiveController().getMotiveText());
					messageResponseOk = PropertiesConstants.ADM_HOLDERACCOUNT_REQUEST_REJECT_OK_BLOCK;
					holdAccReqTemp = holderAccountServiceFacade.rejectHolderAccountRequest(holdAccReqTemp);break;
			}
			searchHolderAccountModifyRequestHysHandler();
			if(isConfirm){
				switch(HolderAccountRequestHysType.lookup.get(this.getHolderAccountRequestHySession().getRequestType())){
					case BLOCK: messageResponseOk = PropertiesConstants.ADM_HOLDERACCOUNT_REQUEST_CONFIRM_BLOCK_OK;break;
					case UNBLOCK: messageResponseOk = PropertiesConstants.ADM_HOLDERACCOUNT_REQUEST_CONFIRM_UNBLOCK_OK;break;
					case CLOSE: messageResponseOk = PropertiesConstants.ADM_HOLDERACCOUNT_REQUEST_CONFIRM_CLOSED_OK;break;
				}
				showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, messageResponseOk, 
						new Object[]{holdAccReqTemp.getIdHolderAccountReqPk().toString(),holdAccReqTemp.getHolderAccount().getAccountNumber().toString(),holdAccReqTemp.getHolderAccount().getHolderAccountReqDetCUI(), holdAccReqTemp.getRequestTypeDescription()});
			}else{
				showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, messageResponseOk, 
						new Object[]{holdAccReqTemp.getIdHolderAccountReqPk().toString(), holdAccReqTemp.getRequestTypeDescription(),holdAccReqTemp.getHolderAccount().getHolderAccountReqDetCUI()});
			}
			JSFUtilities.hideComponent(":frmBlock:cnfblock");
			JSFUtilities.showComponent(":frmBlock:holderAccountMgmtOk");
			loadPrivilegesUser();
			this.setViewOperationType(ViewOperationsType.CONSULT.getCode());
		} catch (ServiceException e) {
			if(e.getErrorService()!=null ){
				if(e.getErrorService().equals(ErrorServiceType.HOLDER_ACCOUNT_WITH_BALANCES)){
					if(ViewOperationsType.lookup.get(this.getViewOperationType()).getCode().equals(ViewOperationsType.APPROVE.getCode())){
						showMessageOnDialogOnlyKey(PropertiesConstants.MESSAGES_ALERT, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_CLOSE_WITH_BALANCES_APPROVE);
					}else if(ViewOperationsType.lookup.get(this.getViewOperationType()).getCode().equals(ViewOperationsType.CONFIRM.getCode())){
						showMessageOnDialogOnlyKey(PropertiesConstants.MESSAGES_ALERT, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_CLOSE_WITH_BALANCES_CONFIRM);
					}					
					JSFUtilities.hideComponent(":frmBlock:cnfblock");
					JSFUtilities.showComponent(":frmBlock:holderAccountMgmtOk");
					loadPrivilegesUser();
					this.setViewOperationType(ViewOperationsType.CONSULT.getCode());
					return;
				}else{
					showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
					JSFUtilities.showComponent(":frmBlock:holderAccountMgmtOk");
					loadPrivilegesUser();
					return;
				}
			}
			excepcion.fire(new ExceptionToCatchEvent(e));
			e.printStackTrace();
		}
	}
	
	/**
	 * Change motive handler.
	 */
	public void changeMotiveHandler(){
		if (this.getMotiveController().getIdMotivePK() != null) {
			if(this.getMotiveController().getIdMotivePK().equals(ERROR_ANULATE) || this.getMotiveController().getIdMotivePK().equals(ERROR_REJECT)){
				this.getMotiveController().setShowMotiveText(Boolean.TRUE);
			}else{
				this.getMotiveController().setShowMotiveText(Boolean.FALSE);
			}
		}else{
			this.getMotiveController().setShowMotiveText(Boolean.FALSE);
		}
	}
	
	/**
	 * Confirm motive handler.
	 */
	public void confirmMotiveHandler(){
		JSFUtilities.hideComponent(":frmBlock:cnfblock");
		if (this.getMotiveController().getIdMotivePK() == null || !Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(this.getMotiveController().getIdMotivePK())))) {
			JSFUtilities.addContextMessage(
							"frmMotive:cboMotive",FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_MOTIVE_REJECT_ANULATE),
							PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_MOTIVE_REJECT_ANULATE));
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MESSAGE_DATA_REQUIRED, null);
			JSFUtilities.showValidationDialog();
			return;
		}	
		if (this.getMotiveController().getIdMotivePK().equals(ERROR_ANULATE)|| this.getMotiveController().getIdMotivePK().equals(ERROR_REJECT)) {
			if (this.getMotiveController().getMotiveText() == null || Validations.validateIsNullOrEmpty(this.getMotiveController().getMotiveText())) {			
				JSFUtilities.addContextMessage(
						"frmMotive:txtOtherMotive",FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_OTHER_MOTIVE_REJECT_ANULATE),
						PropertiesUtilities.getMessage(PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_OTHER_MOTIVE_REJECT_ANULATE));
						showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MESSAGE_DATA_REQUIRED, null);
						JSFUtilities.showValidationDialog();
						return;
			} 
		} 
		
		if(this.getMotiveController().getIdMotivePK()==ERROR_ANULATE || this.getMotiveController().getIdMotivePK()==ERROR_REJECT){
			if(Validations.validateIsNullOrEmpty(this.getMotiveController().getMotiveText())){
				JSFUtilities.showComponent(":frmMotive:dialogMotive");
				return;
			}
		}
		if(this.getViewOperationType().equals(ViewOperationsType.ANULATE.getCode())){
			showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ADM_HOLDERACCOUNT_ANULATE_BLOCK, 
			new Object[]{holderAccountRequestHySession.getIdHolderAccountReqPk().toString(),holderAccountRequestHySession.getRequestTypeDescription(),
						 holderAccountRequestHySession.getHolderAccount().getHolderAccountReqDetCUI()});
		}else if(this.getViewOperationType().equals(ViewOperationsType.REJECT.getCode())){
			showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ADM_HOLDERACCOUNT_REJECT_BLOCK, 
			new Object[]{holderAccountRequestHySession.getIdHolderAccountReqPk().toString(),holderAccountRequestHySession.getRequestTypeDescription(),
						 holderAccountRequestHySession.getHolderAccount().getHolderAccountReqDetCUI()});
		}
		JSFUtilities.showComponent(":frmBlock:cnfblock");
	}
	
	/**
	 * Annulate holder account bank from data table handler.
	 *
	 * @param holderAccountBankParam the holder account bank parameter
	 */
	public void annulateHolderAccountBankFromDataTableHandler(HolderAccountBank holderAccountBankParam){
		executeAction();
		if(holderAccountBankParam.getStateAccountBank().equals(BooleanType.NO.getCode())){
			for(HolderAccountBankRequest holderAccountReqBankHy: this.getHolderAccountRequestHySession().getHolderAccountBankRequest()){
				if(holderAccountReqBankHy.getCurrency().equals(holderAccountBankParam.getCurrency())){
					showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_CURRENCY, new Object[]{CurrencyType.lookup.get(holderAccountBankParam.getCurrency()).getValue()});
					JSFUtilities.showValidationDialog();
					return;
				}
			}
		}
		JSFUtilities.hideGeneralDialogues();
		holderAccountBankParam.setStateAccountBank(BooleanType.lookup.get(!BooleanType.list.get(holderAccountBankParam.getStateAccountBank()).getBooleanValue()).getCode());
	}
	
	/**
	 * Gets the holder account request status type list.
	 *
	 * @return the holder account request status type list
	 */
	public List<HolderAccountRequestHysStatusType> getHolderAccountRequestHysStatusTypeList(){
		return HolderAccountRequestHysStatusType.list;
	}
	
	/**
	 * Search participant on list from pk.
	 *
	 * @param idParticipantPk the id participant pk
	 * @return the participant
	 * this method is useful to get Bank from idBankPk because with this method
	 * it's not necesary to getParticipant from database.
	 */
	public Participant searchParticipantOnListFromPK(Long idParticipantPk){
		for(Participant participant: this.getParticipantList()){
			if(participant.getIdParticipantPk().equals(idParticipantPk)){
				return participant;
			}
		}
		return null;
	}
	

	/**
	 * Gets the holder account request session.
	 *
	 * @return the holder account request session
	 */
	public HolderAccountRequest getHolderAccountRequestHySession() {
		return holderAccountRequestHySession;
	}

	/**
	 * Sets the holder account request session.
	 *
	 * @param holderAccountRequestHySession the new holder account request session
	 */
	public void setHolderAccountRequestHySession(HolderAccountRequest holderAccountRequestHySession) {
		this.holderAccountRequestHySession = holderAccountRequestHySession;
	}

	/**
	 * Gets the holder account to.
	 *
	 * @return the holder account to
	 */
	public HolderAccountTO getHolderAccountTO() {
		return holderAccountTO;
	}
	
	/**
	 * Sets the holder account to.
	 *
	 * @param holderAccountTO the new holder account to
	 */
	public void setHolderAccountTO(HolderAccountTO holderAccountTO) {
		this.holderAccountTO = holderAccountTO;
	}

	/**
	 * Gets the participant list.
	 *
	 * @return the participant list
	 */
	public List<Participant> getParticipantList() {
		return participantList;
	}

	/**
	 * Sets the participant list.
	 *
	 * @param participantList the new participant list
	 */
	public void setParticipantList(List<Participant> participantList) {
		this.participantList = participantList;
	}

	/**
	 * Gets the holder account request data model.
	 *
	 * @return the holder account request data model
	 */
	public GenericDataModel<HolderAccountRequest> getHolderAccountRequestHyDataModel() {
		return holderAccountRequestHyDataModel;
	}

	/**
	 * Sets the holder account request data model.
	 *
	 * @param holderAccountRequestHyDataModel the new holder account request data model
	 */
	public void setHolderAccountRequestHyDataModel(GenericDataModel<HolderAccountRequest> holderAccountRequestHyDataModel) {
		this.holderAccountRequestHyDataModel = holderAccountRequestHyDataModel;
	}

	/**
	 * Gets the motive controller.
	 *
	 * @return the motive controller
	 */
	public MotiveController getMotiveController() {
		return motiveController;
	}

	/**
	 * Sets the motive controller.
	 *
	 * @param motiveController the new motive controller
	 */
	public void setMotiveController(MotiveController motiveController) {
		this.motiveController = motiveController;
	}

	/**
	 * Gets the parameter table motive holder account block.
	 *
	 * @return the parameter table motive holder account block
	 */
	public List<ParameterTable> getParameterTableMotiveHolderAccountBlock() {
		return parameterTableMotiveHolderAccountBlock;
	}

	/**
	 * Sets the parameter table motive holder account block.
	 *
	 * @param parameterTableMotiveHolderAccountBlock the new parameter table motive holder account block
	 */
	public void setParameterTableMotiveHolderAccountBlock(List<ParameterTable> parameterTableMotiveHolderAccountBlock) {
		this.parameterTableMotiveHolderAccountBlock = parameterTableMotiveHolderAccountBlock;
	}
	
	/**
	 * Gets the size max on file.
	 *
	 * @return the size max on file
	 */
	public String getTamanioMaxOnFile(){
		return PropertiesUtilities.getMessage(PropertiesConstants.TAMANIO_FILE, new Object[]{GeneralConstants.TAMANIO_FILE_KBS});
	}

	/**
	 * Gets the back dialog controller.
	 *
	 * @return the back dialog controller
	 */
	public BackDialogController getBackDialogController() {
		return backDialogController;
	}

	/**
	 * Sets the back dialog controller.
	 *
	 * @param backDialogController the new back dialog controller
	 */
	public void setBackDialogController(BackDialogController backDialogController) {
		this.backDialogController = backDialogController;
	}

	/**
	 * Gets the documents holder account block.
	 *
	 * @return the documents holder account block
	 */
	public List<ParameterTable> getDocumentsHolderAccountBlock() {
		return documentsHolderAccountBlock;
	}

	/**
	 * Sets the documents holder account block.
	 *
	 * @param documentsHolderAccountBlock the new documents holder account block
	 */
	public void setDocumentsHolderAccountBlock(List<ParameterTable> documentsHolderAccountBlock) {
		this.documentsHolderAccountBlock = documentsHolderAccountBlock;
	}
	
	/**
	 * Gets the list request block.
	 *
	 * @return the list request block
	 */
	public List<ParameterTable> getLstRequestestBlock() {
		return lstRequestestBlock;
	}

	/**
	 * Sets the list request block.
	 *
	 * @param lstRequestestBlock the new list request block
	 */
	public void setLstRequestestBlock(List<ParameterTable> lstRequestestBlock) {
		this.lstRequestestBlock = lstRequestestBlock;
	}

	/**
	 * Gets the word formated.
	 *
	 * @param word the word
	 * @return the word formated
	 */
	public static String getWordFormated(String word){
		char[] caracteres = word.toLowerCase().toCharArray();
		caracteres[0] = Character.toUpperCase(caracteres[0]);
		for (int i = 0; i < word.length()- 2; i++){
			if (caracteres[i] == ' ' || caracteres[i] == '.' || caracteres[i] == ','){
		    	caracteres[i + 1] = Character.toUpperCase(caracteres[i + 1]);
		    }
		}
		return new String(caracteres);
	}
	
	/**
	 * Before anulate action.
	 *
	 * @return the string
	 */
	public String beforeAnulateAction(){
		executeAction();
		hideLocalComponents();
		if(this.getHolderAccountRequestHySession()==null){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
			JSFUtilities.showValidationDialog();
			return ""; 
		}else if(!this.getHolderAccountRequestHySession().getRequestState().equals(HolderAccountRequestHysStatusType.REGISTERED.getCode())){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_DOACTION_APROVATE_ANULATE, new Object[]{this.getHolderAccountRequestHySession().getStateHolderAccountReqDescription()});
			JSFUtilities.showValidationDialog();
			return "";
		}
		this.setViewOperationType(ViewOperationsType.ANULATE.getCode());
		readObjectFromBD(holderAccountRequestHySession);
		return "holderAccountBlockDetailRule";
	}
	
	/**
	 * Before approve action.
	 *
	 * @return the string
	 */
	public String beforeApproveAction(){
		executeAction();
		hideLocalComponents();
		if(this.getHolderAccountRequestHySession()==null){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
			JSFUtilities.showValidationDialog();
			return ""; 
		}else if(!this.getHolderAccountRequestHySession().getRequestState().equals(HolderAccountRequestHysStatusType.REGISTERED.getCode())){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_DOACTION_APROVATE_ANULATE, new Object[]{this.getHolderAccountRequestHySession().getStateHolderAccountReqDescription()});
			JSFUtilities.showValidationDialog();
			return "";
		}
		this.setViewOperationType(ViewOperationsType.APPROVE.getCode());
		readObjectFromBD(holderAccountRequestHySession);
		return "holderAccountBlockDetailRule";
	}
	
	/**
	 * Before confirm action.
	 *
	 * @return the string
	 */
	public String beforeConfirmAction(){
		executeAction();
		hideLocalComponents();
		if(this.getHolderAccountRequestHySession()==null){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
			JSFUtilities.showValidationDialog();
			return ""; 
		}else if(!this.getHolderAccountRequestHySession().getRequestState().equals(HolderAccountRequestHysStatusType.APPROVE.getCode())){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_DOACTION_CONFIRM_REJECT, new Object[]{this.getHolderAccountRequestHySession().getStateHolderAccountReqDescription()});
			JSFUtilities.showValidationDialog();
			return "";
		}
		this.setViewOperationType(ViewOperationsType.CONFIRM.getCode());
		readObjectFromBD(holderAccountRequestHySession);
		return "holderAccountBlockDetailRule";
	}
	
	/**
	 * Before reject action.
	 *
	 * @return the string
	 */
	public String beforeRejectAction(){
		executeAction();
		hideLocalComponents();
		if(this.getHolderAccountRequestHySession()==null){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
			JSFUtilities.showValidationDialog();
			return ""; 
		}else if(!this.getHolderAccountRequestHySession().getRequestState().equals(HolderAccountRequestHysStatusType.APPROVE.getCode())){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_DOACTION_CONFIRM_REJECT, new Object[]{this.getHolderAccountRequestHySession().getStateHolderAccountReqDescription()});
			JSFUtilities.showValidationDialog();
			return "";
		}
		this.setViewOperationType(ViewOperationsType.REJECT.getCode());
		readObjectFromBD(holderAccountRequestHySession);
		return "holderAccountBlockDetailRule";
	}
	
	/**
	 * Read object from data base.
	 *
	 * @param holderAccountRequestHy the holder account request
	 */
	private void readObjectFromBD(HolderAccountRequest holderAccountRequestHy){
		HolderAccountRequest holderAccountRequestHyFinded = new HolderAccountRequest();
		try {
			holderAccountRequestHyFinded = holderAccountServiceFacade.getHolderAccountRequestHyServiceFacade(holderAccountRequestHy.getIdHolderAccountReqPk());
		} catch (ServiceException e) {
			e.printStackTrace();
		}		
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		if(holderAccountRequestHyFinded!=null && this.getEvaluateUserInfoIsActive()){
			if(this.getViewOperationType().equals(ViewOperationsType.CONSULT.getCode())){
			}else{
				switch(ViewOperationsType.get(this.getViewOperationType())){
					case CONFIRM:privilegeComponent.setBtnConfirmView(Boolean.TRUE);break;
					case APPROVE:privilegeComponent.setBtnApproveView(Boolean.TRUE);break;
					case ANULATE:privilegeComponent.setBtnAnnularView(Boolean.TRUE);break;
					case REJECT:privilegeComponent.setBtnRejectView(Boolean.TRUE);break;
				}
			}
		}
		userPrivilege.setPrivilegeComponent(privilegeComponent);
		List<HolderAccountFileRequest> lstHAFileRequest = new ArrayList<HolderAccountFileRequest>();
		boolean listHasChange=false;
		if(Validations.validateIsNotNull( holderAccountRequestHyFinded.getHolderAccountFilesRequest())){
			for(HolderAccountFileRequest hafr: holderAccountRequestHyFinded.getHolderAccountFilesRequest()){
				String strFileName= hafr.getFilename();
				int fileSize= hafr.getFilename().length();
				byte[] docFile = null;
				try {
					docFile = holderAccountServiceFacade.getHolderAccountReqFileFacade(hafr.getIdHolderAccountReqFilePk());
					String extensionFile = "";
				    if(hafr.getFilename()!=null){
				    	extensionFile = strFileName.toLowerCase().substring(strFileName.lastIndexOf(".")+1, fileSize);
				    	hafr.setFileExtension(strFile+extensionFile.toUpperCase());
				    	hafr.setFileSize(String.valueOf(Integer.valueOf(docFile.length).longValue()/1024L).toString()+strKb);
				    	lstHAFileRequest.add(hafr);
				    	listHasChange=true;
				    }
				} catch (ServiceException e) {
					e.printStackTrace();
				}
			}
		}
		if(listHasChange){
			holderAccountRequestHyFinded.setHolderAccountFilesRequest(lstHAFileRequest);
		}
		this.setHolderAccountRequestHySession(holderAccountRequestHyFinded);
	}
	
	/**
	 * Evaluate user info active.
	 */
	private void evaluateUserInfoActive(){
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			Participant participantTO = new Participant(userInfo.getUserAccountSession().getParticipantCode());
			try {
				List<Participant> participantList = accountsFacade.getLisParticipantServiceBean(participantTO);
				if(participantList!=null && !participantList.isEmpty()){
					participantTO = participantList.get(0);
					if(this.participantList.contains(participantTO)){
						userInfo.getUserAccountSession().setInstitutionState(ParticipantStateType.REGISTERED.getCode());
					}else{
						this.participantList.addAll(participantList);
						userInfo.getUserAccountSession().setInstitutionState(participantTO.getState());
					}
				}
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Gets the evaluate user info active.
	 * @return the evaluate user info active
	 */
	public boolean getEvaluateUserInfoIsActive(){
		if(userInfo.getUserAccountSession().isDepositaryInstitution()){
			return Boolean.TRUE;
		}else if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			if(userInfo.getUserAccountSession().getInstitutionState()!=null){
				return userInfo.getUserAccountSession().getInstitutionState().equals(ParticipantStateType.REGISTERED.getCode());
			}
		}else if(userInfo.getUserAccountSession().isIssuerInstitucion()){
			if(userInfo.getUserAccountSession().getInstitutionState()!=null){
				return userInfo.getUserAccountSession().getInstitutionState().equals(IssuerStateType.REGISTERED.getCode());
			}
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Gets the evaluate user info register.
	 * @return the evaluate user info register
	 */
	public boolean getEvaluateUserInfoCanRegister(){
		if(userInfo.getUserAcctions()!=null){
			Boolean canRegister = userPrivilege.getUserAcctions().isRegister();
			if(canRegister!=null && canRegister){
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Before clean holder account request.
	 */
	public void beforeCleanHolderAccountRequest(){
		executeAction();
		hideLocalComponents();
		if(this.getBackDialogController().getObjectBefore().toString().equals(this.getHolderAccountRequestHySession().toString()+this.getHolderRequest().toString() + this.getHolderAccountTO().getParticipantTO())){
			cleanHolderAccountRequest();
		}else{
			showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, "lbl.clean.confirm", null);
			JSFUtilities.executeJavascriptFunction("PF('cnfwCleanHolderAccountMgmt').show()");
		}
	}
	
	/**
	 * Change participant handler.
	 */
	public void changeParticipantHandler(){
		cleanHolderAccountOnHolderAccountRequestHandler();
		holderAccountRequestHySession.setRequestType(null);
		this.setHolderRequest(new Holder());
		JSFUtilities.resetComponent("frmHolderAccountMgmt:cboRequestType");		
	}

	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	
	/**
	 * Hide dialogs handler.
	 */
	public void hideDialogsHandler(){
		this.hideLocalComponents();
	}

	/**
	 * Gets the current date.
	 *
	 * @return the current date
	 */
	public Date getCurrentDate() {
		return currentDate;
	}

	/**
	 * Sets the current date.
	 *
	 * @param currentDate the new current date
	 */
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}

	/**
	 * Gets the type account for block list.
	 * 
	 * @return the type account for block list
	 */
	public List<ParameterTable> getParameterTableHolderAccountRequestHysForBlockList() {
		return parameterTableHolderAccountRequestHysForBlockList;
	}

	/**
	 * Sets the type account for block list.
	 * 
	 * @param parameterTableHolderAccountRequestHysForBlockList
	 *            the new type account for block list
	 */
	public void setParameterTableHolderAccountRequestHysForBlockList(List<ParameterTable> parameterTableHolderAccountRequestHysForBlockList) {
		this.parameterTableHolderAccountRequestHysForBlockList = parameterTableHolderAccountRequestHysForBlockList;
	}

	/**
	 * Gets the status list.
	 * 
	 * @return the status list
	 */
	public List<ParameterTable> getParameterTableStatusList() {
		return parameterTableStatusList;
	}

	/**
	 * Sets the status list.
	 * 
	 * @param parameterTableStatusList
	 *            the new status list
	 */
	public void setParameterTableStatusList(List<ParameterTable> parameterTableStatusList) {
		this.parameterTableStatusList = parameterTableStatusList;
	}

	/**
	 * Gets the parameter to.
	 * 
	 * @return the  parameter to
	 */
	public ParameterTableTO getParameterTableTO() {
		return parameterTableTO;
	}

	/**
	 * Sets the  parameter to.
	 * 
	 * @param parameterTableTO
	 *            the new  parameter to
	 */
	public void setParameterTableTO(ParameterTableTO parameterTableTO) {
		this.parameterTableTO = parameterTableTO;
	}
	
	/**
	 * Clean Data model account block.
	 */
	public void cleanDataModelAccountBlock(){
		this.setHolderAccountRequestHyDataModel(null);
	}

	/**
	 * Gets the type account.
	 * 
	 * @return the type account
	 */
	public List<ParameterTable> getParameterTableAccoutsTypeList() {
		return parameterTableAccoutsTypeList;
	}

	/**
	 * Sets the type account to.
	 * 
	 * @param parameterTableAccoutsTypeList
	 *            the new  type account
	 */
	public void setParameterTableAccoutsTypeList(List<ParameterTable> parameterTableAccoutsTypeList) {
		this.parameterTableAccoutsTypeList = parameterTableAccoutsTypeList;
	}

	/**
	 * Gets the attach file.
	 * 
	 * @return the attach file
	 */
	public Integer getAttachFileHolderAccountSelected() {
		return attachFileHolderAccountSelected;
	}

	/**
	 * Sets the attach file.
	 * 
	 * @param attachFileHolderAccountSelected
	 *            the new attach file
	 */
	public void setAttachFileHolderAccountSelected(	Integer attachFileHolderAccountSelected) {
		this.attachFileHolderAccountSelected = attachFileHolderAccountSelected;
	}
	
	/**
	 * Gets the name file.
	 * 
	 * @return the name file
	 */
	public String getAccountHolderFileNameDisplay() {
		return accountHolderFileNameDisplay;
	}

	/**
	 * Sets the name file.
	 * 
	 * @param accountHolderFileNameDisplay
	 *            the new name file
	 */
	public void setAccountHolderFileNameDisplay(String accountHolderFileNameDisplay) {
		this.accountHolderFileNameDisplay = accountHolderFileNameDisplay;
	}

	/**
	 * Value change select document holder.
	 */
	public void valueChangeCmbDocumentHolderAccount(){
		accountHolderFileNameDisplay = null;
	}
	
	/**
	 * Load document attach.
	 */
	public void loadingDocumentAttachByAccountHolder() {	  		
		accountHolderFileNameDisplay=GeneralConstants.EMPTY_STRING;		
	}

	/**
	 * Attach document Holder Account Request file.
	 *
	 * @param event the event
	 */
	public void documentAttachHolderAccountBlockUnBlockFile(FileUploadEvent event){
		if(attachFileHolderAccountSelected!=null && !attachFileHolderAccountSelected.equals(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER))){						
			 for(int i=0;i<documentsHolderAccountBlock.size();i++){
				if(documentsHolderAccountBlock.get(i).getParameterTablePk().equals(attachFileHolderAccountSelected)){					
					HolderAccountFileRequest holderAccountReqFileHySend =  new HolderAccountFileRequest();
					holderAccountReqFileHySend.setDocumentType(documentsHolderAccountBlock.get(i).getParameterTablePk());
					holderAccountReqFileHySend.setDocumentTypeDesc(documentsHolderAccountBlock.get(i).getDescription());
					holderAccountReqFileHySend.setIndMain(documentsHolderAccountBlock.get(i).getShortInteger());
					holderAccountReqFileHySend.setFilename(event.getFile().getFileName());
					holderAccountReqFileHySend.setDocumentFile(event.getFile().getContents());
					holderAccountReqFileHySend.setRegistryUser(userInfo.getUserAccountSession().getUserName());
					holderAccountReqFileHySend.setRegistryDate(CommonsUtilities.currentDate());
					holderAccountReqFileHySend.setIndOldNew(BooleanType.YES.getCode());
					holderAccountReqFileHySend.setHolderAccountRequest(holderAccountRequestHySession);										
					insertOrReplaceHolderAccountRequestFileItem(holderAccountReqFileHySend);
				}
			}
		}
	}

	/**
	 * Insert document Holder Account Request file.
	 *
	 * @param holderAccountReqFileHySend the holder account req file hy send
	 */
	public void insertOrReplaceHolderAccountRequestFileItem(HolderAccountFileRequest holderAccountReqFileHySend){	
		byte[] docFile = holderAccountReqFileHySend.getDocumentFile();
		boolean flag = false;	
		int listSize = BooleanType.NO.getCode();
		int fileSize= holderAccountReqFileHySend.getFilename().length();
		String strFileName= holderAccountReqFileHySend.getFilename();
		String extensionFile = "";

    	extensionFile = strFileName.toLowerCase().substring(strFileName.lastIndexOf(".")+1, fileSize);
    	holderAccountReqFileHySend.setFileExtension(strFile+extensionFile.toUpperCase());
    	holderAccountReqFileHySend.setFileSize(String.valueOf(Integer.valueOf(docFile.length).longValue()/1024L).toString()+strKb);
    	listSize = this.getHolderAccountRequestHySession().getHolderAccountFilesRequest().size();
		if(listSize>0){
			for(int i=0;i<listSize;i++){			
				if(holderAccountRequestHySession.getHolderAccountFilesRequest().get(i).getDocumentType().equals(holderAccountReqFileHySend.getDocumentType())){
					holderAccountRequestHySession.getHolderAccountFilesRequest().remove(i);
					holderAccountRequestHySession.getHolderAccountFilesRequest().add(i,holderAccountReqFileHySend);
					this.setAttachFileHolderAccountSelected(null);				
					flag = true;
				}	
			}
			if(!flag){
				holderAccountRequestHySession.getHolderAccountFilesRequest().add(holderAccountReqFileHySend);
				this.setAttachFileHolderAccountSelected(null);
			}
		}else{
			holderAccountRequestHySession.getHolderAccountFilesRequest().add(holderAccountReqFileHySend);
			this.setAttachFileHolderAccountSelected(null);
		}		
	}
	
	/**
	 * Holder request new handler.
	 *
	 * @param evt the evt
	 */
	public void holderRequestNewHandler(ValueChangeEvent evt) {
		if (evt.getPhaseId() != PhaseId.INVOKE_APPLICATION) {
			evt.setPhaseId(PhaseId.INVOKE_APPLICATION);
			evt.queue();
			return;
		}					
		this.getHolderAccountRequestHySession().getHolderAccountFilesRequest().removeAll(this.getHolderAccountRequestHySession().getHolderAccountFilesRequest());
	}
	
	/**
	 * Gets the streamed content temporal file holder Account history.
	 *
	 * @param holderAccountReqFileHy the holder account req file hy
	 * @return the streamed content temporal file holder Account history
	 */
	public StreamedContent getStreamedContentTemporalFile(HolderAccountFileRequest holderAccountReqFileHy) {				
		InputStream is = null;	
		is = new ByteArrayInputStream(holderAccountReqFileHy.getDocumentFile());		
		return new DefaultStreamedContent(is,"application/pdf",holderAccountReqFileHy.getFilename());
	}
	
	/**
	 * Gets the streamed content temporal file holder Account history.
	 *
	 * @param holderAccountReqFileHy the holder account req file hy
	 * @return the streamed content temporal file holder Account history
	 */
	public StreamedContent getStreamedContentFile(HolderAccountFileRequest holderAccountReqFileHy) {				
		InputStream is = null;				
		byte[] fileAccount = null;
		try {
			fileAccount = holderAccountServiceFacade.getHolderAccountReqFileFacade(holderAccountReqFileHy.getIdHolderAccountReqFilePk());
			is = new ByteArrayInputStream(fileAccount);
		} catch (ServiceException e) {
				e.printStackTrace();
		}														
		return new DefaultStreamedContent(is,"application/pdf",holderAccountReqFileHy.getFilename());
	}
	
	/**
	 * Removes the holder Account request file.
	 *
	 * @param holderAccountReqFileHy the holder account req file hy
	 */
	public void removeDocumentFileRequest(HolderAccountFileRequest holderAccountReqFileHy) {
		holderAccountRequestHySession.getHolderAccountFilesRequest().remove(holderAccountReqFileHy);
		accountHolderFileNameDisplay = null;
		this.setAttachFileHolderAccountSelected(null);		
	}
	
	/**
	 * Gets the holder request.
	 * 
	 * @return the holder request
	 */
	public Holder getHolderRequest() {
		return holderRequest;
	}

	/**
	 * Sets the holder request.
	 * 
	 * @param holderRequest
	 *            the new holder request
	 */
	public void setHolderRequest(Holder holderRequest) {
		this.holderRequest = holderRequest;
	}
	
	/**
	 * Search holder handler.
	 */
	public void searchHolderHandler(){		
		executeAction();
		JSFUtilities.hideGeneralDialogues(); 
		cleanHolderAccountOnHolderAccountRequestHandler();
		holderAccountRequestHySession.setRequestType(null);	
		JSFUtilities.resetComponent("frmHolderAccountMgmt:cboRequestType");		
		if(holderRequest.getIdHolderPk() != null && Validations.validateIsPositiveNumber(holderRequest.getIdHolderPk().intValue())){			
			// Get holder from database *.*
			Holder holder = null;
			try {
				holder = holderServiceFacade.getHolderServiceFacade(this.getHolderRequest().getIdHolderPk());
				holder.setFullName(holder.getDescriptionHolder());
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (Validations.validateIsNull(holder)) {
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_HOLDERNULL,
					new Object[] { String.valueOf(this.getHolderRequest().getIdHolderPk()) });
				this.setHolderRequest(new Holder());
				JSFUtilities.showSimpleValidationDialog();
						return;
				}
			if (!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())) {
						showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,	null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_HOLDER_BLOCK,
								new Object[] { String.valueOf(this.getHolderRequest().getIdHolderPk()) });
						this.setHolderRequest(new Holder());
						JSFUtilities.showSimpleValidationDialog();
						return;
			}			
		}		
	}
	
	
	public String getPdfFileUploadFiltTypes() {
		return pdfFileUploadFiltTypes;
	}
	
	public String getPdfFileUploadFileTypesDisplay() {
		return pdfFileUploadFileTypesDisplay;
	}

	/**
	 * Gets the f upload file size mgmt.
	 *
	 * @return the f upload file size mgmt
	 */
	public String getfUploadFileSizeMgmt() {
		return fUploadFileSizeMgmt;
	}

	/**
	 * Sets the f upload file size mgmt.
	 *
	 * @param fUploadFileSizeMgmt the new f upload file size mgmt
	 */
	public void setfUploadFileSizeMgmt(String fUploadFileSizeMgmt) {
		this.fUploadFileSizeMgmt = fUploadFileSizeMgmt;
	}

	/**
	 * Gets the f upload file size display mgmt.
	 *
	 * @return the f upload file size display mgmt
	 */
	public String getfUploadFileSizeDisplayMgmt() {
		return fUploadFileSizeDisplayMgmt;
	}

	/**
	 * Sets the f upload file size display mgmt.
	 *
	 * @param fUploadFileSizeDisplayMgmt the new f upload file size display mgmt
	 */
	public void setfUploadFileSizeDisplayMgmt(String fUploadFileSizeDisplayMgmt) {
		this.fUploadFileSizeDisplayMgmt = fUploadFileSizeDisplayMgmt;
	}

	/**
	 * Go to.
	 *
	 * @param viewToGo the view to go
	 * @return the string
	 */
	public String goTo(String viewToGo) {
		cleanHolderAccountTO();
		return viewToGo;
	}

	/**
	 * Gets the user privilege.
	 *
	 * @return the user privilege
	 */
	public UserPrivilege getUserPrivilege() {
		return userPrivilege;
	}

	/**
	 * Sets the user privilege.
	 *
	 * @param userPrivilege the new user privilege
	 */
	public void setUserPrivilege(UserPrivilege userPrivilege) {
		this.userPrivilege = userPrivilege;
	}
	
	
	
}