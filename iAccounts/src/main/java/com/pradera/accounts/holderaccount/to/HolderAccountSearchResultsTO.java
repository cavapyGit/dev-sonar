package com.pradera.accounts.holderaccount.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountMovement;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class HolderAccountSearchResultsTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/08/2015
 */
public class HolderAccountSearchResultsTO implements Serializable{
	
	/** The account balance. */
	private HolderAccountBalance accountBalance;
	
	/** The account movement list. */
	private List<HolderAccountMovement> accountMovementList;
	
	/**
	 * Gets the account balance.
	 *
	 * @return the account balance
	 */
	//Getters and Setters
	public HolderAccountBalance getAccountBalance() {
		return accountBalance;
	}

	/**
	 * Sets the account balance.
	 *
	 * @param accountBalance the new account balance
	 */
	public void setAccountBalance(HolderAccountBalance accountBalance) {
		this.accountBalance = accountBalance;
	}

	/**
	 * Gets the account movement list.
	 *
	 * @return the account movement list
	 */
	public List<HolderAccountMovement> getAccountMovementList() {
		return accountMovementList;
	}

	/**
	 * Sets the account movement list.
	 *
	 * @param accountMovementList the new account movement list
	 */
	public void setAccountMovementList(
			List<HolderAccountMovement> accountMovementList) {
		this.accountMovementList = accountMovementList;
	}
	
	
}
