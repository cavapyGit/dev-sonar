package com.pradera.accounts.holderaccount.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountBank;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.HolderAccountFile;
import com.pradera.model.accounts.holderaccounts.HolderAccountBankRequest;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetRequest;
import com.pradera.model.accounts.holderaccounts.HolderAccountFileRequest;
import com.pradera.model.accounts.holderaccounts.HolderAccountRequest;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestHysStatusType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestHysType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStateBankType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStateDetailType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountMovement;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.OtcOperationStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class HolderAccountModifyServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/08/2015
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class HolderAccountModifyServiceBean extends CrudDaoServiceBean{
	
	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
	/** The parameter service bean. */
	@EJB
	ParameterServiceBean parameterServiceBean;

	/**
	 * Gets the bank list service bean.
	 *
	 * @return the bank list service bean
	 */
	@SuppressWarnings("unchecked")
	public List<Bank> getBankListServiceBean(){
		Query query = em.createQuery("Select b From Bank b WHERE b.state = :stateBankParam");
		query.setParameter("stateBankParam", MasterTableType.PARAMETER_TABLE_BANK_STATE_REGISTERED.getCode());
		return (List<Bank>)query.getResultList();
	}
	
	/**
	 * Registry holder account request service bean.
	 *
	 * @param holderAccountRequest the holder account request hy
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean registryHolderAccountRequest(HolderAccountRequest holderAccountRequest) throws ServiceException{
		validateEntitiesOnRequest(holderAccountRequest);		
		if(!holderAccountRequest.getRequestType().equals(HolderAccountRequestHysType.CREATE.getCode())){
			List<Integer> stateValidate = Arrays.asList(HolderAccountRequestHysStatusType.REGISTERED.getCode(),HolderAccountRequestHysStatusType.APPROVE.getCode());
			HolderAccount holderAccount = holderAccountRequest.getHolderAccount();
			Map<String,Object> holderAccountPkParam = new HashMap<String,Object>();
			holderAccountPkParam.put("idAccountPk", holderAccount.getIdHolderAccountPk());
			holderAccountPkParam.put("statesParam", stateValidate);			
			Long quantityRequest = findObjectByNamedQuery(HolderAccountRequest.ACC_ACCOUNT_USED, holderAccountPkParam);			
			if(quantityRequest>0L){
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			}
		}		
		create(holderAccountRequest);
		Query query = em.createQuery(new StringBuilder("UPDATE HolderAccountRequest r SET r.requestNumber = r.idHolderAccountReqPk ").append(
									 				   " WHERE r.idHolderAccountReqPk = :idHolderAccountRequestParameter").toString());
		query.setParameter("idHolderAccountRequestParameter", holderAccountRequest.getIdHolderAccountReqPk());
		query.executeUpdate();
		return true;
	}	
	
	/**
	 * Gets the holder account request detail list service bean.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account request detail list service bean
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings({"unchecked" })
	public List<HolderAccountRequest> getHolderAccountRequestsList(HolderAccountTO holderAccountTO) throws ServiceException{		
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("	Select har.idHolderAccountReqPk, har.registryDate,	"); //0, 1
		stringBuilder.append("	har.holderAccountType, (Select p.parameterName From ParameterTable p Where p.parameterTablePk = har.holderAccountType),	"); // 2, 3
		stringBuilder.append("	har.requestState, (Select p.parameterName From ParameterTable p Where p.parameterTablePk = har.requestState),	"); // 4, 5
		stringBuilder.append("	har.participant.idParticipantPk, har.participant.mnemonic,	"); //6, 7
		stringBuilder.append("	ho.idHolderPk, ho.fullName,	");	//8, 9
		stringBuilder.append("	ho.documentType, (Select p.indicator1 From ParameterTable p Where p.parameterTablePk = ho.documentType), ");//10, 11
		stringBuilder.append("	ho.documentNumber, ");//12	
		stringBuilder.append("	har.requestType, ha.accountNumber ");//13, 14
		stringBuilder.append("	,har.registryUser, ha.relatedName ");//15, 16
		stringBuilder.append("	from HolderAccountRequest har	");				
		stringBuilder.append("	join har.holderAccount ha	");
		stringBuilder.append("	join ha.holderAccountDetails had	");
		stringBuilder.append("	join had.holder ho	");		
		stringBuilder.append("	Where 1 = 1	");		
		if(holderAccountTO.getRequestNumber() !=null ){
			stringBuilder.append("	AND	har.requestNumber = :requestNumber	");
			parameters.put("requestNumber", holderAccountTO.getRequestNumber());			
		}
		if(holderAccountTO.getParticipantTO() != null){
			stringBuilder.append("	AND	har.participant.idParticipantPk = :idParticipantPk	");
			parameters.put("idParticipantPk", holderAccountTO.getParticipantTO());			
		}				
		if(Validations.validateIsNotNullAndNotEmpty(holderAccountTO.getInitialDate()) && Validations.validateIsNotNullAndNotEmpty(holderAccountTO.getFinalDate())){
			stringBuilder.append(" and TRUNC(har.registryDate) between :dateIni and :dateEnd ");
			parameters.put("dateIni", holderAccountTO.getInitialDate());
			parameters.put("dateEnd", holderAccountTO.getFinalDate());			
		}
		if(Validations.validateIsNotNull(holderAccountTO.getStatus()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(holderAccountTO.getStatus())))){
			stringBuilder.append("	AND	har.requestState = :requestState	");
			parameters.put("requestState", holderAccountTO.getStatus());							
		}			
		if(holderAccountTO.getRelatedName() != null){
			stringBuilder.append("	AND	har.relatedName LIKE :relatedName	");
			parameters.put("relatedName", "%" +  holderAccountTO.getRelatedName() + "%");				
		}
		if(holderAccountTO.getHolderAccountNumber() != null){
			stringBuilder.append("	AND	ha.accountNumber = :accountNumber	");
			parameters.put("accountNumber",holderAccountTO.getHolderAccountNumber());				
		}
		if(Validations.validateIsNotNull(holderAccountTO.getHolderAccountRequestType()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(holderAccountTO.getHolderAccountRequestType())))){
			stringBuilder.append("	AND	har.requestType = :requestType	");
			parameters.put("requestType", holderAccountTO.getHolderAccountRequestType());			
		} 
		List<Object[]> accountHolderRequestList =  findListByQueryString(stringBuilder.toString(), parameters);
		Map<Long,HolderAccountRequest> accountsHolderRequestPk = new HashMap<Long, HolderAccountRequest>();		
		for(Object[] object: accountHolderRequestList){
			if(accountsHolderRequestPk.get(object[0]) == null){
				HolderAccountRequest holderAccountRequest = new HolderAccountRequest();
				holderAccountRequest.setHolderAccountDetRequest(new ArrayList<HolderAccountDetRequest>());	
				holderAccountRequest.setIdHolderAccountReqPk(new Long(object[0].toString()));				
				holderAccountRequest.setRegistryDate((Date) object[1]);				
				holderAccountRequest.setHolderAccountType(new Integer(object[2].toString()));
				holderAccountRequest.setHolderAccountTypeDescription(object[3].toString());
				holderAccountRequest.setRequestState(new Integer(object[4].toString()));
				holderAccountRequest.setStateAccountName(object[5].toString());	
				holderAccountRequest.setRequestType(new Integer(object[13].toString()));
				holderAccountRequest.setRegistryUser(object[15].toString());
				
				if(Validations.validateIsNotNull(object[16])) {
					holderAccountRequest.setRelatedName(object[16].toString());
				}				
		
				Participant participant = new Participant();
				participant.setIdParticipantPk(new Long(object[6].toString()));
				participant.setMnemonic(object[7].toString());
				holderAccountRequest.setParticipant(participant);	
				HolderAccount holderAccount = new HolderAccount();
				holderAccount.setAccountNumber(new Integer(object[14].toString()));
				holderAccountRequest.setHolderAccount(holderAccount);
				accountsHolderRequestPk.put(holderAccountRequest.getIdHolderAccountReqPk(), holderAccountRequest);
			}
			HolderAccountRequest accountRequestMap = accountsHolderRequestPk.get(object[0]);
			HolderAccountDetRequest accountRequestDetail = new HolderAccountDetRequest();
			Holder holder = new Holder();						
			holder.setIdHolderPk(new Long(object[8].toString()));
			holder.setFullName(object[9].toString());
			holder.setDocumentType(new Integer(object[10].toString()));
			holder.setDescriptionTypeDocument(object[11].toString());
			holder.setDocumentNumber(object[12].toString());
			accountRequestDetail.setHolder(holder);
			accountRequestMap.getHolderAccountDetRequest().add(accountRequestDetail);
		}
		return new ArrayList<HolderAccountRequest>(accountsHolderRequestPk.values());
	}
	
	/**
	 * Update selective holder account request service bean.
	 *
	 * @param holderAccountRequestHy the holder account request
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	public HolderAccountRequest updateRequestByState(HolderAccountRequest holderAccountRequestHy) throws ServiceException{		
		StringBuilder jpqlQuery = new StringBuilder("Update HolderAccountRequest holderRequest SET holderRequest.requestState = :stateHolderAccountReqParameter, ");
		Map<String,Object> mapParameters = new HashMap<String,Object>();
		boolean accountRequestIsConfirmation = Boolean.FALSE;
		if(holderAccountRequestHy.getRequestState().equals(HolderAccountRequestHysStatusType.REGISTERED.getCode())){
			if(Validations.validateIsNotNullAndNotEmpty(holderAccountRequestHy.getAnullUser())){
				jpqlQuery.append("holderRequest.anullUser = :anullUserParameter, ").
						  append("holderRequest.anullDate = :anullDateParameter, ").
						  append("holderRequest.requestMotive = :motiveRequestParameter, ").
						  append("holderRequest.requestOtherMotive = :otherMotiveRequestParameter");
				mapParameters.put("anullUserParameter", holderAccountRequestHy.getAnullUser());
				mapParameters.put("anullDateParameter", holderAccountRequestHy.getAnullDate());
				mapParameters.put("motiveRequestParameter", holderAccountRequestHy.getRequestMotive());
				mapParameters.put("otherMotiveRequestParameter", holderAccountRequestHy.getRequestOtherMotive());
				mapParameters.put("stateHolderAccountReqParameter", HolderAccountRequestHysStatusType.ANULATE.getCode());
			}else if(Validations.validateIsNotNullAndNotEmpty(holderAccountRequestHy.getApproveUser())){
				jpqlQuery.append("holderRequest.approveUser = :approveUserParameter, ").
						  append("holderRequest.approveDate = :approveDateParameter");
				mapParameters.put("approveUserParameter", holderAccountRequestHy.getApproveUser());
				mapParameters.put("approveDateParameter", holderAccountRequestHy.getApproveDate());
				mapParameters.put("stateHolderAccountReqParameter", HolderAccountRequestHysStatusType.APPROVE.getCode());
			}
		}else if(holderAccountRequestHy.getRequestState().equals(HolderAccountRequestHysStatusType.APPROVE.getCode())){
			if(Validations.validateIsNotNullAndNotEmpty(holderAccountRequestHy.getConfirmUser())){
				jpqlQuery.append("holderRequest.confirmUser = :confirmUserParameter, ").
				  		  append("holderRequest.confirmDate = :confirmDateParameter");
				mapParameters.put("confirmUserParameter", holderAccountRequestHy.getConfirmUser());
				mapParameters.put("confirmDateParameter", holderAccountRequestHy.getConfirmDate());
				mapParameters.put("stateHolderAccountReqParameter", HolderAccountRequestHysStatusType.CONFIRM.getCode());
				accountRequestIsConfirmation = Boolean.TRUE;
			}else if(Validations.validateIsNotNullAndNotEmpty(holderAccountRequestHy.getRejectUser())){
				jpqlQuery.append("holderRequest.rejectUser = :rejectUserParameter, ").
						  append("holderRequest.rejectDate = :rejectDateParameter, ").
						  append("holderRequest.requestMotive = :motiveRequestParameter, ").
						  append("holderRequest.requestOtherMotive = :otherMotiveRequestParameter");
				mapParameters.put("rejectUserParameter", holderAccountRequestHy.getRejectUser());
				mapParameters.put("rejectDateParameter", holderAccountRequestHy.getRejectDate());
				mapParameters.put("motiveRequestParameter", holderAccountRequestHy.getRequestMotive());
				mapParameters.put("otherMotiveRequestParameter", holderAccountRequestHy.getRequestOtherMotive());
				mapParameters.put("stateHolderAccountReqParameter", HolderAccountRequestHysStatusType.REJECT.getCode());
			}
		}
		jpqlQuery.append(" WHERE holderRequest.idHolderAccountReqPk = ").append(holderAccountRequestHy.getIdHolderAccountReqPk());
		Query query = em.createQuery(jpqlQuery.toString());		
        @SuppressWarnings("rawtypes")
		Iterator it= mapParameters.entrySet().iterator();
        while(it.hasNext()){
        	@SuppressWarnings("unchecked")
			Entry<String,Object> entry = (Entry<String,Object>)it.next();
        	query.setParameter(entry.getKey(), entry.getValue());
        }
		query.executeUpdate();		
		if(accountRequestIsConfirmation){
			 holderAccountRequestHy.setHolderAccount(updateHolderAccount(holderAccountRequestHy));
		}		
		return holderAccountRequestHy;
	}
	
	/**
	 * Gets the holder account request service bean.
	 *
	 * @param idRequestPk the id request
	 * @return the holder account request service bean
	 * @throws ServiceException the service exception 
	 */
	public HolderAccountRequest getHolderAccountRequest(Long idRequestPk) throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_FILES.getCode());
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		List<ParameterTable> listParameterTable = null;		
		listParameterTable = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);						
		String query = "Select distinct har From HolderAccountRequest har " +
				"			Inner Join Fetch har.participant part " +
				"			Inner Join Fetch har.holderAccountDetRequest hard " +
				"			Inner Join Fetch hard.holder ho " +								
				"			Where har.idHolderAccountReqPk = :idPk";		
		Query queryJpa = em.createQuery(query); 
		queryJpa.setParameter("idPk", idRequestPk);						
		HolderAccountRequest holderAccountRequestHy = (HolderAccountRequest) queryJpa.getSingleResult();			
		holderAccountRequestHy.getHolderAccountFilesRequest().size();
		holderAccountRequestHy.getHolderAccountBankRequest().size();						
		if(holderAccountRequestHy.getHolderAccount() != null){
			HolderAccountTO holderAccountTO = new HolderAccountTO();
			holderAccountTO.setNeedHolder(Boolean.TRUE);
			holderAccountTO.setNeedFiles(Boolean.TRUE);
			holderAccountTO.setNeedParticipant(Boolean.FALSE);
			holderAccountTO.setIdHolderAccountPk(holderAccountRequestHy.getHolderAccount().getIdHolderAccountPk());
			holderAccountRequestHy.setHolderAccount(getHolderAccount(holderAccountTO));
		}
		for(HolderAccountFileRequest file : holderAccountRequestHy.getHolderAccountFilesRequest()){
			for(ParameterTable parameter: listParameterTable){
				if(file.getDocumentType().equals(parameter.getParameterTablePk())){
					file.setDocumentTypeDesc(parameter.getDescription());
				}
			}
		}	
		return holderAccountRequestHy;
	}
	
	/**
	 * Gets the holder account service bean.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account service bean
	 * @throws ServiceException the service exception
	 */
	public HolderAccount getHolderAccount(HolderAccountTO holderAccountTO) throws ServiceException{
		return accountsFacade.getHolderAccountComponentServiceFacade(holderAccountTO);
	}
	
	/**
	 * Update holder account from holder account request service bean.
	 *
	 * @param holderAcccounRequest the holder account request
	 * @return the holder account
	 * @throws ServiceException the service exception
	 */
	public HolderAccount updateHolderAccount(HolderAccountRequest holderAcccounRequest) throws ServiceException{
		HolderAccountRequest holderAccountRequestHy = getHolderAccountRequest(holderAcccounRequest.getIdHolderAccountReqPk());
		HolderAccount holderAccount = holderAccountRequestHy.getHolderAccount();		
		if(holderAcccounRequest.getRequestType().equals(HolderAccountRequestHysType.MODIFY.getCode())){
			
			holderAccount.getHolderAccountDetails().removeAll(holderAccount.getHolderAccountDetails());
			//holderAccount.getHolderAccountBanks().removeAll(holderAccount.getHolderAccountBanks());
			holderAccount.getHolderAccountFiles().removeAll(holderAccount.getHolderAccountFiles());
			
			Query queryDetail = em.createQuery("DELETE FROM HolderAccountDetail accountDet WHERE accountDet.holderAccount.idHolderAccountPk = :idHolderAccountPkParam");
			queryDetail.setParameter("idHolderAccountPkParam", holderAccount.getIdHolderAccountPk());
			queryDetail.executeUpdate();	
			
			List<HolderAccountBankRequest> lstBankAccountToPersist = holderAccountRequestHy.getHolderAccountBankRequest()
																		.stream()
																		.filter(bankAcc -> bankAcc.getIndOldNew().equals(BooleanType.YES.getCode()) 
																				&& Validations.validateIsNull(bankAcc.getRefIdHolderAccountBankFk()))
																		.collect(Collectors.toList());
			
			List<HolderAccountBankRequest> lstBankAccountToUpdate = holderAccountRequestHy.getHolderAccountBankRequest()
																		.stream()
																		.filter(bankAcc -> bankAcc.getIndOldNew().equals(BooleanType.YES.getCode()) 
																				&& Validations.validateIsNotNull(bankAcc.getRefIdHolderAccountBankFk()))
																		.collect(Collectors.toList());
			
			for(HolderAccountBankRequest bankToUpdate : lstBankAccountToUpdate) {
				for(HolderAccountBank accBank : holderAccount.getHolderAccountBanks()) {
					if(accBank.getIdHolderAccountBankPk().equals(bankToUpdate.getRefIdHolderAccountBankFk())) {
						accBank.setStateAccountBank(bankToUpdate.getStateAccountBank());
					}
				}
			}
			
			
			/*Query queryBank = em.createQuery("DELETE FROM HolderAccountBank accountBank WHERE accountBank.holderAccount.idHolderAccountPk = :idHolderAccountPkParam");
			queryBank.setParameter("idHolderAccountPkParam", holderAccount.getIdHolderAccountPk());
			queryBank.executeUpdate();	*/
			Query queryFile = em.createQuery("DELETE FROM HolderAccountFile accountFile WHERE accountFile.holderAccount.idHolderAccountPk = :idHolderAccountPkParam");
			queryFile.setParameter("idHolderAccountPkParam", holderAccount.getIdHolderAccountPk());
			queryFile.executeUpdate();
			
			Holder holderMain = null;
			Integer indNew = 1;
			Integer indOld = 0;
			boolean flagExist = existModifyRequestDetail(holderAccountRequestHy.getHolderAccountDetRequest());
			for(HolderAccountDetRequest holderAccountReqDetHy: holderAccountRequestHy.getHolderAccountDetRequest()){				
				HolderAccountDetail holderAccountDetail = new HolderAccountDetail();
				holderAccountDetail.setHolderAccount(holderAccount);
				holderAccountDetail.setHolder(holderAccountReqDetHy.getHolder());
				holderAccountDetail.setIndRepresentative(holderAccountReqDetHy.getIndRepresentative());
				holderAccountDetail.setRegistryDate(holderAccountRequestHy.getRegistryDate());
				holderAccountDetail.setRegistryUser(holderAccountRequestHy.getRegistryUser());
				holderAccountDetail.setHolderAccount(holderAccount);
				holderAccountDetail.setStateAccountDetail(HolderAccountStateDetailType.REGISTERED.getCode());				
				if(!flagExist){
					if(holderAccountReqDetHy.getIndOldNew().equals(indNew)){
						holderAccount.getHolderAccountDetails().add(holderAccountDetail);
					}					
				}else{
					if(holderAccountReqDetHy.getIndOldNew().equals(indOld)){
						holderAccount.getHolderAccountDetails().add(holderAccountDetail);
					}
				}												
				if(holderAccountReqDetHy.getIndRepresentative().equals(BooleanType.YES.getCode())){
					holderMain = holderAccountDetail.getHolder(); 
				}
			}					
			
			for(HolderAccountBankRequest holderAccountReqBankHy: lstBankAccountToPersist){
				HolderAccountBank holderAccountBank = new HolderAccountBank();
				holderAccountBank.setHolderAccount(holderAccount);
				holderAccountBank.setBank(holderAccountReqBankHy.getBank());
				holderAccountBank.setHolder(holderMain);
				holderAccountBank.setCurrency(holderAccountReqBankHy.getCurrency());
				holderAccountBank.setBankAccountType(holderAccountReqBankHy.getBankAccountType());
				holderAccountBank.setBankAccountNumber(holderAccountReqBankHy.getAccountNumber());
				holderAccountBank.setDescriptionBank(holderAccountReqBankHy.getDescriptionBank());						
				holderAccountBank.setRegistryUser(holderAccountRequestHy.getRegistryUser());
				holderAccountBank.setRegistryDate(holderAccountRequestHy.getRegistryDate());
				holderAccountBank.setStateAccountBank(HolderAccountStateBankType.REGISTERED.getCode());
				holderAccountBank.setHaveBic(BooleanType.NO.getCode());
				holderAccountBank.setHolderAccount(holderAccount);
				holderAccountBank.setSwiftCode(holderAccountReqBankHy.getSwiftCode());
				holderAccount.getHolderAccountBanks().add(holderAccountBank);							
			}		
			
			boolean flagExistFile = existModifyRequestFile(holderAccountRequestHy.getHolderAccountFilesRequest());
			for(HolderAccountFileRequest holderAccountReqFileHy: holderAccountRequestHy.getHolderAccountFilesRequest()){
				HolderAccountFile holderAccountFile = new HolderAccountFile();
				holderAccountFile.setDocumentType(holderAccountReqFileHy.getDocumentType());
				holderAccountFile.setDocumentFile(holderAccountReqFileHy.getDocumentFile());
				holderAccountFile.setFilename(holderAccountReqFileHy.getFilename());
				holderAccountFile.setHolderAccount(holderAccount);
				holderAccountFile.setRegistryDate(new Date());
				holderAccountFile.setRegistryUser(holderAccountRequestHy.getRegistryUser());
				holderAccountFile.setIndMain(holderAccountReqFileHy.getIndMain());		
				
				if(!flagExistFile){
					if(holderAccountReqFileHy.getIndOldNew().equals(indNew)){
						holderAccount.getHolderAccountFiles().add(holderAccountFile);
					}					
				}
				//esta linea fue eliminada
//				else{
//					if(holderAccountReqFileHy.getIndOldNew().equals(indOld)){
//						holderAccount.getHolderAccountFiles().add(holderAccountFile);
//					}
//				}																
			}
			holderAccount.setRelatedName(holderAccountRequestHy.getRelatedName());
			holderAccount.setIndPdd(holderAccountRequestHy.getIndPdd());
			
			if(holderAccountRequestHy.getIndPdd().equals(GeneralConstants.ONE_VALUE_INTEGER)
					&& Validations.validateIsNotNull(holderAccountRequestHy.getDocumentPdd())
					&& !holderAccountRequestHy.getDocumentPdd().equals(holderAccount.getDocumentPdd())) {
				holderAccount.setDocumentPdd(holderAccountRequestHy.getDocumentPdd());
				holderAccount.setNameDocumentPdd(holderAccountRequestHy.getNameDocumentPdd());
			} else if (Validations.validateIsNull(holderAccountRequestHy.getDocumentPdd())){
				holderAccount.setDocumentPdd(null);
				holderAccount.setNameDocumentPdd(null);
			}
		}
		update(holderAccount);
		return holderAccount;
	}
	
	
	/**
	 * Exist modify request detail.
	 *
	 * @param holderDetailRequest the holder detail request
	 * @return true, if successful
	 */
	public boolean existModifyRequestDetail(List<HolderAccountDetRequest> holderDetailRequest ){
		Integer flag = 0;
		for(HolderAccountDetRequest detObject : holderDetailRequest){
			if(detObject.getIndOldNew().equals(flag)){
				
			}else{
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Exist modify request bank.
	 *
	 * @param holderBankRequest the holder bank request
	 * @return true, if successful
	 */
	public boolean existModifyRequestBank(List<HolderAccountBankRequest> holderBankRequest ){
		Integer flag = 0;
		for(HolderAccountBankRequest bankObject : holderBankRequest){
			if(bankObject.getIndOldNew().equals(flag)){
				
			}else{
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Exist modify request file.
	 *
	 * @param holderFileRequest the holder file request
	 * @return true, if successful
	 */
	public boolean existModifyRequestFile(List<HolderAccountFileRequest> holderFileRequest ){
		Integer flag = 0;
		for(HolderAccountFileRequest fileObject : holderFileRequest){
			if(fileObject.getIndOldNew().equals(flag)){
				
			}else{
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Validate holder on request.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @throws ServiceException the service exception
	 */
	public void validateEntitiesOnRequest(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		Map<String,Object> holderPkParam = new HashMap<String,Object>();
		Map<String,Object> participantPkParam = new HashMap<String,Object>();
		Map<String,Object> holderAccountPkParam = new HashMap<String,Object>();
		for(HolderAccountDetRequest holderAccountDetail: holderAccountRequestHy.getHolderAccountDetRequest()){
			holderPkParam.put("idHolderPkParam", holderAccountDetail.getHolder().getIdHolderPk());
			Holder holder = findObjectByNamedQuery(Holder.HOLDER_STATE, holderPkParam);
			if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.HOLDER_BLOCK);
			}
		}
		participantPkParam.put("idParticipantPkParam", holderAccountRequestHy.getParticipant().getIdParticipantPk());
		Participant participant = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, participantPkParam);
		if(!participant.getState().equals(ParticipantStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
		}		
		if(holderAccountRequestHy.getHolderAccount()!=null){
			holderAccountPkParam.put("idHolderAccountPkParam", holderAccountRequestHy.getHolderAccount().getIdHolderAccountPk());
			HolderAccount holderAccount = findObjectByNamedQuery(HolderAccount.HOLDER_ACCOUNT_STATE, holderAccountPkParam);
			if(holderAccountRequestHy.getRequestType().equals(HolderAccountRequestHysType.UNBLOCK.getCode())){
				if(!holderAccount.getStateAccount().equals(HolderAccountStatusType.BLOCK.getCode())){
					throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_BLOCK);
				}
			}else{
				if(!holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())){
					throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_BLOCK);
				}
				for(HolderAccountDetail holderAccountDetail: holderAccountRequestHy.getHolderAccount().getHolderAccountDetails()){
					holderPkParam.put("idHolderPkParam", holderAccountDetail.getHolder().getIdHolderPk());
					Holder holder = findObjectByNamedQuery(Holder.HOLDER_STATE, holderPkParam);
					if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
						throw new ServiceException(ErrorServiceType.HOLDER_BLOCK);
					}
				}
			}
		}
	}
	
	/**
	 * Gets the holder req det hies.
	 *
	 * @param holderAccountReqDetHies the holder account req det hies
	 * @return the holder req det hies
	 */
	public List<HolderAccountDetRequest> getHolderReqDetHies(List<HolderAccountDetRequest> holderAccountReqDetHies){
		Collections.sort(holderAccountReqDetHies, new Comparator<HolderAccountDetRequest>(){
  			public int compare(HolderAccountDetRequest s1, HolderAccountDetRequest s2) {
			    return s1.getHolder().getIdHolderPk().compareTo(s2.getHolder().getIdHolderPk());
			  }
			});
		return holderAccountReqDetHies;
	}
	
	/**
	 * Gets the holder account list service bean.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account list service bean
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> getHolderAccountListServiceBean(HolderAccountTO holderAccountTO) throws ServiceException{
		return accountsFacade.getHolderAccountListComponentServiceFacade(holderAccountTO);
	}
	
	/*
	 *	Created by Martin Zarate Rafael 
	 */
	/**
	 * Gets the balance holder from security.
	 *
	 * @param filter the filter
	 * @return the balance holder from security
	 */
	//Gets Balance from Holder Account
	public HolderAccountBalance getBalanceHolderFromSecurity(HolderAccountTO filter){
		StringBuilder jpqlQuery = new StringBuilder(" select * from HolderAccountBalance hab ");
		jpqlQuery.append(" where hab.participant.idParticipantPk="+filter.getParticipantTO())
				 .append(" and hab.holderAccount.accountNumber="+filter.getHolderAccountNumber())
				 .append(" and hab.security.idIsinCodePk="+filter.getIdIsinCodePk());
		Query query = em.createQuery(jpqlQuery.toString());
		return (HolderAccountBalance)query.getSingleResult();
	}
	
	/*
	 *	Created by Martin Zarate Rafael 
	 */
	/**
	 * Gets the holder account movements.
	 *
	 * @param filter the filter
	 * @return the holder account movements
	 */
	//Gets Movements from Security
	@SuppressWarnings("unchecked")
	public List<HolderAccountMovement> getHolderAccountMovements(HolderAccountTO filter){
		StringBuilder jpqlQuery = new StringBuilder(" select * from HolderAccountMovement ham ");
		jpqlQuery.append(" where hab.participant.idParticipantPk="+filter.getParticipantTO())
				 .append(" and hab.holderAccount.accountNumber="+filter.getHolderAccountNumber())
				 .append(" and hab.security.idIsinCodePk="+filter.getIdIsinCodePk());
		Query query = em.createQuery(jpqlQuery.toString());
		return (List<HolderAccountMovement>)query.getResultList();
	}
	
	/**
	 * Have request with out confirmation service bean.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public HolderAccountRequest haveRequestWithOutConfirmationServiceBean(HolderAccountTO holderAccountTO) throws ServiceException{
		String query = "	SELECT holderAccountReq FROM HolderAccountRequest holderAccountReq " +
						"	WHERE holderAccountReq.holderAccount.accountNumber = :accountNumberParam AND " +
						"  	holderAccountReq.participant.idParticipantPk = :idParticipantParam AND" +
						"  	holderAccountReq.requestState IN (:stateRegisteredParam, :stateApproveParam)";
		Query queryJpa = em.createQuery(query);
		queryJpa.setParameter("stateRegisteredParam", HolderAccountRequestHysStatusType.REGISTERED.getCode());
		queryJpa.setParameter("stateApproveParam", HolderAccountRequestHysStatusType.APPROVE.getCode());
		queryJpa.setParameter("accountNumberParam", holderAccountTO.getHolderAccountNumber());
		queryJpa.setParameter("idParticipantParam", holderAccountTO.getParticipantTO());		
		List<HolderAccountRequest> holderAccountRequestHyList = null;
		try{
			holderAccountRequestHyList = ((List<HolderAccountRequest>)queryJpa.getResultList());
		}catch(NoResultException ex){
			return null;
		}
		if(holderAccountRequestHyList!=null && !holderAccountRequestHyList.isEmpty()){
			return holderAccountRequestHyList.get(0);
		}
		return null;
	}
	
	/**
	 * Holder account can will closed.
	 *
	 * @param holderAccount the holder account
	 * @return the boolean
	 */
	public Boolean holderAccountCanWillClosedServiceBean(HolderAccount holderAccount){
		if(holderAccount==null || holderAccount.getIdHolderAccountPk()==null){
			return Boolean.FALSE;
		}		
		StringBuilder queryValidate = new StringBuilder();
		queryValidate.append("SELECT COUNT(*) "+
							 "	FROM HolderAccountOperation hao "+
							 "	INNER JOIN hao.mechanismOperation mo "+
							 "	INNER JOIN mo.mechanisnModality mmo "+ 							
							 "	INNER JOIN mmo.negotiationModality negmo "+
							 "	WHERE hao.holderAccount.idHolderAccountPk = :idAccountPkParam AND mo.operationState "+ 
							 "	IN( "+
							 "	  :operationRegisteredParam "+
							 "	  ,:otcOperationConfirmedParam "+
							 "	  , (CASE negmo.indTermSettlement " +
							 "					WHEN 1 THEN :operationSettlementTermParam " +
							 "					ELSE :operationSettlementCashParam " +
							 "		END) "+
							 "	  ) ");				
		Query query = em.createQuery(queryValidate.toString());
		query.setParameter("operationRegisteredParam",MechanismOperationStateType.REGISTERED_STATE.getCode());
		query.setParameter("otcOperationConfirmedParam",MechanismOperationStateType.ASSIGNED_STATE.getCode());
		query.setParameter("operationSettlementCashParam",MechanismOperationStateType.CASH_SETTLED.getCode());
		query.setParameter("operationSettlementTermParam",MechanismOperationStateType.TERM_SETTLED.getCode());
		query.setParameter("idAccountPkParam",holderAccount.getIdHolderAccountPk());
		Long rowNumbers = 0L;
		try{
			rowNumbers = ((Long)query.getSingleResult()).longValue();
		}catch(NoResultException ex){
			rowNumbers = 0L;
		}catch(NullPointerException e){
			rowNumbers = 0L;
		}
		if(rowNumbers!=null && rowNumbers>0L){
			return Boolean.FALSE;
		}
		
		StringBuilder validateBalance = new StringBuilder();
		validateBalance.append("SELECT SUM(totalBalance) FROM HolderAccountBalance hab WHERE hab.holderAccount.idHolderAccountPk = :idAccountParam");
		Query queryBalance = em.createQuery(validateBalance.toString());
		queryBalance.setParameter("idAccountParam", holderAccount.getIdHolderAccountPk());
		Long totalBalance = 0L;
		try{
			totalBalance = ((BigDecimal)queryBalance.getSingleResult()).longValue();
		}catch(NoResultException ex){
			totalBalance = 0L;
		}catch(NullPointerException e){
			totalBalance = 0L;
		}
		if(totalBalance!=null && totalBalance>0L){
			return Boolean.FALSE;
		}		
		return Boolean.TRUE;
	}
	
	/**
	 * Gets the holder account request file.
	 *
	 * @param idFilePk the id file pk
	 * @return the holder account request file
	 * @throws ServiceException the service exception
	 */
	public byte[] getHolderAccountReqFile(Long idFilePk) throws ServiceException{
		String query = "Select har.documentFile FROM HolderAccountFileRequest har WHERE har.idHolderAccountReqFilePk = :idFilePkParam";
		Query queryJpql = em.createQuery(query);
		queryJpql.setParameter("idFilePkParam", idFilePk);
		try{
			return (byte[]) queryJpql.getSingleResult();
		}catch(NoResultException ex){
			return null;
		}
	}
	
	/**
	 * Gets the holder account file.
	 *
	 * @param idFilePk the id file pk
	 * @return the holder account file
	 * @throws ServiceException the service exception
	 */
	public byte[] getHolderAccountFile(Long idFilePk) throws ServiceException{
		String query = "Select har.documentFile FROM HolderAccountFile har WHERE har.idHolderAccountFilePk = :idFilePkParam";
		Query queryJpql = em.createQuery(query);
		queryJpql.setParameter("idFilePkParam", idFilePk);
		try{
			return (byte[]) queryJpql.getSingleResult();
		}catch(NoResultException ex){
			return null;
		}
	}
	
	/**
	 * Validate states request.
	 *
	 * @param accountRequestId the account request id
	 * @param statesValidate the states validate
	 * @throws ServiceException the service exception
	 */
	public void validateStatesRequestHys(Long accountRequestId, List<Integer> statesValidate) throws ServiceException{
		Map<String,Object> holderAccountPkParam = new HashMap<String,Object>();
		holderAccountPkParam.put("idAccountReqPk", accountRequestId);
		HolderAccountRequest accountRequest = findObjectByNamedQuery(HolderAccountRequest.ACC_STATE, holderAccountPkParam);
		if(!statesValidate.contains(accountRequest.getRequestState())){
			throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
		}
	}
}