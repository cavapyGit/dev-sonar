package com.pradera.accounts.holderaccount.facade;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.accounts.holderaccount.service.HolderAccountCloseServiceBean;
import com.pradera.accounts.holderaccount.service.HolderAccountServiceBean;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountRequest;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestHysType;
import com.pradera.model.process.BusinessProcess;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class HolderAccountCloseFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/08/2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class HolderAccountCloseFacade {	
	
	/** The holder account close service bean. */
	@EJB
	HolderAccountCloseServiceBean holderAccountCloseServiceBean;
	
	
	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The notification service facade. */
	@EJB 
	NotificationServiceFacade	notificationServiceFacade;
	
	/**
	 * Gets the bank list service facade.
	 *
	 * @return the bank list service facade
	 */
	public List<Bank> getBankListServiceFacade(){
		return holderAccountCloseServiceBean.getBankListServiceBean();
	}
	
	/**
	 * Register holder account request service facade.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean registerHolderAccountRequest(HolderAccountRequest holderAccountRequestHy) throws ServiceException{				
		switch(HolderAccountRequestHysType.get(holderAccountRequestHy.getRequestType())){
			case BLOCK: return registerHolderAccountBlockFacade(holderAccountRequestHy);
			case UNBLOCK: return registerHolderAccountUnBlockFacade(holderAccountRequestHy);
			case CLOSE: return registerHolderAccountCloseFacade(holderAccountRequestHy);
		}
		return false;
	}

	/**
	 * Register holder account block facade.
	 *
	 * @param holderAccountRequest the holder account request
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_BLOCK_REGISTER)
	public boolean registerHolderAccountBlockFacade(HolderAccountRequest holderAccountRequest) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());		
		Boolean option = holderAccountCloseServiceBean.registryHolderAccountRequest(holderAccountRequest);		
		//Paratemers for notification
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_ACCOUNT_BLOCK_REGISTER.getCode());
		Object[] parameters = new Object[]{holderAccountRequest.getHolderAccount().getAccountNumber().toString()};		
		//Send notification
		sendNotification(holderAccountRequest.getParticipant().getIdParticipantPk(), businessProcess, parameters);		
		return option;
	}
	
	/**
	 * Register holder account un block facade.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_UNBLOCK_REGISTER)
	public boolean registerHolderAccountUnBlockFacade(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		Boolean option = holderAccountCloseServiceBean.registryHolderAccountRequest(holderAccountRequestHy);
		//Paratemers for notification
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_ACCOUNT_UNBLOCK_REGISTER.getCode());
		Object[] parameters = new Object[]{holderAccountRequestHy.getHolderAccount().getAccountNumber().toString()};		
		//Send notification
		sendNotification(holderAccountRequestHy.getParticipant().getIdParticipantPk(), businessProcess, parameters);		
		return option;
	}
	
	/**
	 * Register holder account close facade.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_CLOSE_REGISTER)
	public boolean registerHolderAccountCloseFacade(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

		Boolean option = holderAccountCloseServiceBean.registryHolderAccountRequest(holderAccountRequestHy);
		
		//Paratemers for notification
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_ACCOUNT_CLOSE_REGISTER.getCode());
		Object[] parameters = new Object[]{holderAccountRequestHy.getHolderAccount().getAccountNumber().toString()};
		
		//Send notification
		sendNotification(holderAccountRequestHy.getParticipant().getIdParticipantPk(), businessProcess, parameters);
		
		return option;
	}
	
	/**
	 * Gets the holder account request detail list service facade.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account request detail list service facade
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountRequest> getHolderAccountRequestHysListServiceFacade(HolderAccountTO holderAccountTO) throws ServiceException{
		switch(HolderAccountRequestHysType.get(holderAccountTO.getHolderAccountRequestType())){
			case BLOCK: return getHolderAccountBlockQueryFacade(holderAccountTO);
			case UNBLOCK: return getHolderAccountUnBlockQueryFacade(holderAccountTO);
			case CLOSE: return getHolderAccountCloseQueryFacade(holderAccountTO);
		}
		return null;
	}
	
	/**
	 * Gets the holder account block query facade.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account block query facade
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_BLOCK_QUERY)
	public List<HolderAccountRequest> getHolderAccountBlockQueryFacade(HolderAccountTO holderAccountTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return holderAccountCloseServiceBean.getHolderAccountRequestCloseListServiceBean(holderAccountTO);
	}
	
	/**
	 * Gets the holder account un block query facade.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account un block query facade
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_UNBLOCK_QUERY)
	public List<HolderAccountRequest> getHolderAccountUnBlockQueryFacade(HolderAccountTO holderAccountTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return holderAccountCloseServiceBean.getHolderAccountRequestCloseListServiceBean(holderAccountTO);
	}
	
	/**
	 * Gets the holder account close query facade.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account close query facade
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_CLOSE_QUERY)
	public List<HolderAccountRequest> getHolderAccountCloseQueryFacade(HolderAccountTO holderAccountTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return holderAccountCloseServiceBean.getHolderAccountRequestCloseListServiceBean(holderAccountTO);
	}
	
	/**
	 * Approve holder account request.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @return the holder account request hy
	 * @throws ServiceException the service exception
	 */
	public HolderAccountRequest approveHolderAccountRequest(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		switch(HolderAccountRequestHysType.get(holderAccountRequestHy.getRequestType())){
			case BLOCK: return approveAccountBlockRequest(holderAccountRequestHy);
			case UNBLOCK: return approveAccountUnBlockRequest(holderAccountRequestHy);
			case CLOSE: return approveAccountCloseRequest(holderAccountRequestHy);
		}
		return null;
	}

	/**
	 * Approve account block request.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_BLOCK_APPROVE)
	public HolderAccountRequest approveAccountBlockRequest(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		HolderAccountRequest holderAccountRequest = holderAccountCloseServiceBean.approveHolderAccountRequest(holderAccountRequestHy);
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
		
		//Paratemers for notification
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_ACCOUNT_BLOCK_APPROVE.getCode());
		Object[] parameters = new Object[]{holderAccountRequestHy.getHolderAccount().getAccountNumber().toString()};
		Long participantCode = holderAccountRequest.getParticipant().getIdParticipantPk();
		
		//Send notification
		sendNotification(participantCode, businessProcess, parameters);
		
		return holderAccountRequest;
	}
	
	/**
	 * Approve account un block request.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_UNBLOCK_APPROVE)
	public HolderAccountRequest approveAccountUnBlockRequest(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		HolderAccountRequest holderAccountRequest = holderAccountCloseServiceBean.approveHolderAccountRequest(holderAccountRequestHy);
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
		
		//Paratemers for notification
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_ACCOUNT_UNBLOCK_APPROVE.getCode());
		Object[] parameters = new Object[]{holderAccountRequestHy.getHolderAccount().getAccountNumber().toString()};
		Long participantCode = holderAccountRequest.getParticipant().getIdParticipantPk();
		
		//Send notification
		sendNotification(participantCode, businessProcess, parameters);
		
		return holderAccountRequest;
	}
	
	/**
	 * Approve account close request.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_CLOSE_APPROVE)
	public HolderAccountRequest approveAccountCloseRequest(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		if(holderAccountRequestHy.getRequestType().equals(HolderAccountRequestHysType.CLOSE.getCode())){
			if(!holderAccountCloseServiceBean.holderAccountCanWillClosedServiceBean(holderAccountRequestHy.getHolderAccount())){
				throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_WITH_BALANCES);
			}
		}
		HolderAccountRequest holderAccountRequest = holderAccountCloseServiceBean.approveHolderAccountRequest(holderAccountRequestHy);
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
		
		//Paratemers for notification
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_ACCOUNT_CLOSE_APPROVE.getCode());
		Object[] parameters = new Object[]{holderAccountRequestHy.getHolderAccount().getAccountNumber().toString()};
		Long participantCode = holderAccountRequest.getParticipant().getIdParticipantPk();
		
		//Send notification
		sendNotification(participantCode, businessProcess, parameters);
		
		return holderAccountRequest;
	}
	
	/**
	 * Annulate holder account request.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @return the holder account request hy
	 * @throws ServiceException the service exception
	 */
	public HolderAccountRequest annulateHolderAccountRequest(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		switch(HolderAccountRequestHysType.get(holderAccountRequestHy.getRequestType())){
			case BLOCK: return annulateAccountBlockRequest(holderAccountRequestHy);
			case UNBLOCK: return annulateAccountUnBlockRequest(holderAccountRequestHy);
			case CLOSE: return annulateAccountCloseRequest(holderAccountRequestHy);
		}
		return null;
	}
	
	/**
	 * Annulate account block request.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_BLOCK_CANCEL)
	public HolderAccountRequest annulateAccountBlockRequest(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		HolderAccountRequest holderAccountRequest = holderAccountCloseServiceBean.anulateHolderAccountRequest(holderAccountRequestHy);
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
		
		//Paratemers for notification
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_ACCOUNT_BLOCK_CANCEL.getCode());
		Object[] parameters = new Object[]{holderAccountRequestHy.getHolderAccount().getAccountNumber().toString()};
		Long participantCode = holderAccountRequest.getParticipant().getIdParticipantPk();
		
		//Send notification
		sendNotification(participantCode, businessProcess, parameters);
		
		return holderAccountRequest;
	}
	
	/**
	 * Annulate account un block request.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_UNBLOCK_CANCEL)
	public HolderAccountRequest annulateAccountUnBlockRequest(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		HolderAccountRequest holderAccountRequest = holderAccountCloseServiceBean.anulateHolderAccountRequest(holderAccountRequestHy);
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
		
		//Paratemers for notification
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_ACCOUNT_UNBLOCK_CANCEL.getCode());
		Object[] parameters = new Object[]{holderAccountRequestHy.getHolderAccount().getAccountNumber().toString()};
		Long participantCode = holderAccountRequest.getParticipant().getIdParticipantPk();
		
		//Send notification
		sendNotification(participantCode, businessProcess, parameters);
		
		return holderAccountRequest;
	}
	
	/**
	 * Annulate account close request.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_CLOSE_CANCEL)
	public HolderAccountRequest annulateAccountCloseRequest(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		HolderAccountRequest holderAccountRequest = holderAccountCloseServiceBean.anulateHolderAccountRequest(holderAccountRequestHy);
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
		
		//Paratemers for notification
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_ACCOUNT_CLOSE_CANCEL.getCode());
		Object[] parameters = new Object[]{holderAccountRequestHy.getHolderAccount().getAccountNumber().toString()};
		Long participantCode = holderAccountRequest.getParticipant().getIdParticipantPk();
		
		//Send notification
		sendNotification(participantCode, businessProcess, parameters);
		
		return holderAccountRequest;
	}
	
	/**
	 * Reject holder account request.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @return the holder account request hy
	 * @throws ServiceException the service exception
	 */
	public HolderAccountRequest rejectHolderAccountRequest(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		switch(HolderAccountRequestHysType.get(holderAccountRequestHy.getRequestType())){
			case BLOCK: return rejectAccountBlockRequest(holderAccountRequestHy);
			case UNBLOCK: return rejectAccountUnBlockRequest(holderAccountRequestHy);
			case CLOSE: return rejectAccountCloseRequest(holderAccountRequestHy);
		}
		return null;
	}
	
	/**
	 * Reject account block request.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_BLOCK_REJECT)
	public HolderAccountRequest rejectAccountBlockRequest(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		HolderAccountRequest holderAccountRequest = holderAccountCloseServiceBean.rejectHolderAccountRequest(holderAccountRequestHy);
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		
		//Paratemers for notification
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_ACCOUNT_BLOCK_REJECT.getCode());
		Object[] parameters = new Object[]{holderAccountRequestHy.getHolderAccount().getAccountNumber().toString()};
		Long participantCode = holderAccountRequest.getParticipant().getIdParticipantPk();
		
		//Send notification
		sendNotification(participantCode, businessProcess, parameters);
		
		return holderAccountRequest;
	}
	
	/**
	 * Reject account un block request.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_UNBLOCK_REJECT)
	public HolderAccountRequest rejectAccountUnBlockRequest(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		HolderAccountRequest holderAccountRequest = holderAccountCloseServiceBean.rejectHolderAccountRequest(holderAccountRequestHy);
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		
		//Paratemers for notification
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_ACCOUNT_UNBLOCK_REJECT.getCode());
		Object[] parameters = new Object[]{holderAccountRequestHy.getHolderAccount().getAccountNumber().toString()};
		Long participantCode = holderAccountRequest.getParticipant().getIdParticipantPk();
		
		//Send notification
		sendNotification(participantCode, businessProcess, parameters);
		
		return holderAccountRequest;
	}
	
	/**
	 * Reject account close request.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_CLOSE_REJECT)
	public HolderAccountRequest rejectAccountCloseRequest(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		HolderAccountRequest holderAccountRequest = holderAccountCloseServiceBean.rejectHolderAccountRequest(holderAccountRequestHy);
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		
		//Paratemers for notification
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_ACCOUNT_CLOSE_REJECT.getCode());
		Object[] parameters = new Object[]{holderAccountRequestHy.getHolderAccount().getAccountNumber().toString()};
		Long participantCode = holderAccountRequest.getParticipant().getIdParticipantPk();
		
		//Send notification
		sendNotification(participantCode, businessProcess, parameters);
		
		return holderAccountRequest;
	}
	
	/**
	 * Confirm holder account request.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @return the holder account request hy
	 * @throws ServiceException the service exception
	 */
	public HolderAccountRequest confirmHolderAccountRequest(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		switch(HolderAccountRequestHysType.get(holderAccountRequestHy.getRequestType())){
			case BLOCK: return confirmAccountBlockRequest(holderAccountRequestHy);
			case UNBLOCK: return confirmAccountUnBlockRequest(holderAccountRequestHy);
			case CLOSE: return confirmAccountCloseRequest(holderAccountRequestHy);
		}
		return null;
	}
	
	/**
	 * Confirm account block request.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_BLOCK_CONFIRM)
	public HolderAccountRequest confirmAccountBlockRequest(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		
		HolderAccountRequest holderAccountRequest = holderAccountCloseServiceBean.confirmHolderAccountRequest(holderAccountRequestHy);				
		
		//Paratemers for notification
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_ACCOUNT_BLOCK_CONFIRM.getCode());
		Object[] parameters = new Object[]{holderAccountRequestHy.getHolderAccount().getAccountNumber().toString()};
		Long participantCode = holderAccountRequest.getParticipant().getIdParticipantPk();
		
		//Send notification
		sendNotification(participantCode, businessProcess, parameters);
		
		return holderAccountRequest;
	}
	
	/**
	 * Confirm account un block request.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_UNBLOCK_CONFIRM)
	public HolderAccountRequest confirmAccountUnBlockRequest(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		
		HolderAccountRequest holderAccountRequest = holderAccountCloseServiceBean.confirmHolderAccountRequest(holderAccountRequestHy);
		
		//Paratemers for notification
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_ACCOUNT_UNBLOCK_CONFIRM.getCode());
		Object[] parameters = new Object[]{holderAccountRequestHy.getHolderAccount().getAccountNumber().toString()};
		Long participantCode = holderAccountRequest.getParticipant().getIdParticipantPk();
		
		//Send notification
		sendNotification(participantCode, businessProcess, parameters);
		
		return holderAccountRequest;
	}
	
	/**
	 * Confirm account close request.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_CLOSE_CONFIRM)
	public HolderAccountRequest confirmAccountCloseRequest(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		if(holderAccountRequestHy.getRequestType().equals(HolderAccountRequestHysType.CLOSE.getCode())){
			if(!holderAccountCloseServiceBean.holderAccountCanWillClosedServiceBean(holderAccountRequestHy.getHolderAccount())){
				throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_WITH_BALANCES);
			}
		}				
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		
		HolderAccountRequest holderAccountRequest = holderAccountCloseServiceBean.confirmHolderAccountRequest(holderAccountRequestHy);					
		//Paratemers for notification
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_ACCOUNT_CLOSE_CONFIRM.getCode());
		Object[] parameters = new Object[]{holderAccountRequestHy.getHolderAccount().getAccountNumber().toString()};
		Long participantCode = holderAccountRequest.getParticipant().getIdParticipantPk();
		
		//Send notification
		sendNotification(participantCode, businessProcess, parameters);
		
		return holderAccountRequest;
	}
	
	
	/**
	 * Update selective holder account request service facade.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @return the holder account request hy
	 * @throws ServiceException the service exception
	 */
	public HolderAccountRequest updateSelectiveHolderAccountRequestServiceFacade(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		if(holderAccountRequestHy.getRequestType().equals(HolderAccountRequestHysType.CLOSE.getCode())){//Validate if request Is closed
			if(!holderAccountCloseServiceBean.holderAccountCanWillClosedServiceBean(holderAccountRequestHy.getHolderAccount())){
				throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_WITH_BALANCES);
			}
		}
		return holderAccountCloseServiceBean.updateRequestByState(holderAccountRequestHy);
	}
	
	/**
	 * Holder account can will closed service bean.
	 *
	 * @param holderAccount the holder account
	 * @return the boolean
	 * @throws ServiceException the service exception
	 */
	public Boolean holderAccountCanWillClosedServiceFacade(HolderAccount holderAccount) throws ServiceException{
		return holderAccountCloseServiceBean.holderAccountCanWillClosedServiceBean(holderAccount);
	}
	
	/**
	 * Gets the holder account request hy service facade.
	 *
	 * @param idHolderAccountRequestPk the id holder account request pk
	 * @return the holder account request hy service facade
	 * @throws ServiceException the service exception
	 */
	public HolderAccountRequest getHolderAccountRequestHyServiceFacade(Long idHolderAccountRequestPk) throws ServiceException{
		return holderAccountCloseServiceBean.getHolderAccountRequestClose(idHolderAccountRequestPk);
	}
	
	/**
	 * Gets the holder account service facade.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account service facade
	 * @throws ServiceException the service exception
	 */
	public HolderAccount getHolderAccountServiceFacade(HolderAccountTO holderAccountTO) throws ServiceException{
		return holderAccountCloseServiceBean.getHolderAccount(holderAccountTO);
	}
	
	/**
	 * Gets the holder account list service facade.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account list service facade
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> getHolderAccountListServiceFacade(HolderAccountTO holderAccountTO) throws ServiceException{
		return holderAccountCloseServiceBean.getHolderAccountListServiceBean(holderAccountTO);
	}
	
	/**
	 * Have request with out confirmation service facade.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account request hy
	 * @throws ServiceException the service exception
	 */
	public HolderAccountRequest haveRequestWithOutConfirmationServiceFacade(HolderAccountTO holderAccountTO) throws ServiceException{
		return holderAccountCloseServiceBean.haveRequestWithOutConfirmation(holderAccountTO);
	}
	
	/**
	 * Gets the holder account req file facade.
	 *
	 * @param idFilePk the id file pk
	 * @return the holder account req file facade
	 * @throws ServiceException the service exception
	 */
	public byte[] getHolderAccountReqFileFacade(Long idFilePk) throws ServiceException{
		return holderAccountCloseServiceBean.getHolderAccountReqFile(idFilePk);
	}
	
	/**
	 * Gets the holder account file facade.
	 *
	 * @param idFilePk the id file pk
	 * @return the holder account file facade
	 * @throws ServiceException the service exception
	 */
	public byte[] getHolderAccountFileFacade(Long idFilePk) throws ServiceException{
		return holderAccountCloseServiceBean.getHolderAccountFile(idFilePk);
	}
	
	/**
	 * Send notification.
	 *
	 * @param partCode the part code
	 * @param businessProcess the business process
	 * @param parameters the parameters
	 */
	public void sendNotification(Long partCode , BusinessProcess businessProcess,Object[] parameters){
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		 notificationServiceFacade.sendNotification(loggerUser.getUserName(), businessProcess, partCode, parameters);
	 }
	
	/**
	 * Gets the holder account request file doc block facade.
	 *
	 * @param idFilePk the id file pk
	 * @return the holder account request file doc block facade
	 * @throws ServiceException the service exception
	 */
	public byte[] getHolderAccountReqDocBlock(Long idFilePk) throws ServiceException{
		return holderAccountCloseServiceBean.getHolderAccountReqDocBlock(idFilePk);
	}
	
	
	/**
	 * Gets the holder account request file doc block acc facade.
	 *
	 * @param idFilePk the id file pk
	 * @return the holder account request file doc block acc facade
	 * @throws ServiceException the service exception
	 */
	public byte[] getHolderAccountReqDocBlockAcct(Long idFilePk) throws ServiceException{
		return holderAccountCloseServiceBean.getHolderAccountReqDocBlockAct(idFilePk);
	}
}