package com.pradera.accounts.applicant.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Date;
import java.util.HashMap;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Applicant;
import com.pradera.model.accounts.ApplicantConsult;
import com.pradera.model.accounts.ApplicantConsultRequest;
import com.pradera.model.accounts.AppRequester;
import com.pradera.model.accounts.Holder;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.report.ReportLoggerFile;
/**
* 
* <ul><li>Copyright EDV 2020.</li></ul> 
* @author RCHIARA.
* 
*
*/
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ApplicantServiceBean extends CrudDaoServiceBean {
	
	@EJB CrudDaoServiceBean crudDaoServiceBean;	
	/**
	 * 
	 * 
	 * 
	 * */
	@SuppressWarnings("unchecked")
	public ReportLoggerFile findReport(Long holderId, String nameReport) {
		
		 StringBuilder stringBuilderSql = new StringBuilder();
		 stringBuilderSql.append("SELECT r FROM ReportLoggerFile r WHERE r.nameFile = :nameReport");
	      
		 Query query = em.createQuery(stringBuilderSql.toString());
		 
		 query.setParameter("nameReport", nameReport+""+String.valueOf(holderId)+".pdf");	  
		 ReportLoggerFile reportLoggerFile = new ReportLoggerFile();
		 try {
			 reportLoggerFile = (ReportLoggerFile) query.getSingleResult();
		 } catch (NoResultException e) {
		 	reportLoggerFile = null;
		 } catch (NonUniqueResultException e) {
			List<ReportLoggerFile> reportLoggerFileList = (List<ReportLoggerFile>) query.getResultList();
			reportLoggerFile = reportLoggerFileList.get(0);
		}
	    return reportLoggerFile;
	    
    } 
	/**
	 * 
	 * 
	 * 
	 * */
	public String findFileReportPath(String nameReport){
		StringBuilder sbQuery = new StringBuilder();		
		Map<String,Object> mapParam = new HashMap<String,Object>();	
		sbQuery.append(" SELECT RLF.nameTrace");
		sbQuery.append("   FROM ReportLoggerFile RLF");
		sbQuery.append("  WHERE RLF.nameFile =:requestPk");		
		
		mapParam.put("requestPk", nameReport);
		String dataReport = (String) findObjectByQueryString(sbQuery.toString(),mapParam);
		
		return dataReport; 
    }	
	/**
	 * 
	 * 
	 * 
	 * */
	@SuppressWarnings("unchecked")
	public List<ParameterTable> getListApplicants() throws ServiceException {
		
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" select PARAMETER_TABLE_PK ");
		stringBuilder.append(" 		, PARAMETER_TABLE_CD ");
		stringBuilder.append(" 		, DESCRIPTION ");
		stringBuilder.append(" 		, PARAMETER_NAME ");
		stringBuilder.append(" from PARAMETER_TABLE ");
		stringBuilder.append(" where MASTER_TABLE_FK = 1209 ");
		stringBuilder.append(" order by PARAMETER_TABLE_PK ");

		Query query = em.createNativeQuery(stringBuilder.toString());
		
		List<Object[]> listObjects = query.getResultList();
		List<ParameterTable> applicants = new ArrayList<ParameterTable>();
		ParameterTable parameter;
		
		for(Object[] temp: listObjects){
			parameter = new ParameterTable();
			parameter.setParameterTablePk(Integer.parseInt(temp[0].toString()));
			parameter.setParameterTableCd(temp[1].toString());
			parameter.setDescription(temp[2].toString());
			parameter.setParameterName(temp[3].toString());
			applicants.add(parameter);
		}
		
		return applicants;
		
	}

	@SuppressWarnings("unchecked")
	public List<AppRequester> getListRequesteres () throws ServiceException {
		List<AppRequester> result = new ArrayList<AppRequester>();
		StringBuilder str = new StringBuilder();
		str.append("	select a from AppRequester a");
		str.append("	order by a.idAppRequesterPk");		
		Query query = em.createQuery(str.toString());
		result = (List<AppRequester>)query.getResultList();	
		return result;
	}
	
	/**
	 * 
	 * 
	 * 
	 * */
	@SuppressWarnings("unchecked")
	public List<ParameterTable> getDocumentTypes () throws ServiceException {
		
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" select PARAMETER_TABLE_PK, DESCRIPTION from PARAMETER_TABLE "
				+ " where PARAMETER_STATE = 1 and MASTER_TABLE_FK = 15 order by PARAMETER_NAME ");
		
		Query query = em.createNativeQuery(stringBuilder.toString());
		
		List<Object[]> listObjects = query.getResultList();
		List<ParameterTable> types = new ArrayList<ParameterTable>();
		ParameterTable parameter;
		for(Object[] temp: listObjects){
			parameter = new ParameterTable();
			parameter.setParameterTablePk(Integer.parseInt(temp[0].toString()));
			parameter.setDescription(temp[1].toString());
			types.add(parameter);
		}
		
		return types;
		
	}	
	/**
	 * 
	 * BUSCA PERSONAS DE SOLICITUD
	 * 
	 * */
	@SuppressWarnings("unchecked")
	public List<Holder> getListHolderServiceBean(ApplicantConsult applicantCons) { 
		
		List<Holder> list = null;		
		try {		
			StringBuilder stringBuilderSql = new StringBuilder();
			stringBuilderSql.append(" SELECT distinct(h) FROM Holder h ");
//			stringBuilderSql.append(" 	LEFT JOIN h.holderAccountDetail had ");
//			stringBuilderSql.append(" 	LEFT JOIN had.holderAccount ha ");
			stringBuilderSql.append(" WHERE 1 = 1 ");
			stringBuilderSql.append(" 	and h.stateHolder = 496 ");//REGISTRADO
			if(Validations.validateIsNotNullAndNotEmpty(applicantCons.getDocType())){
				stringBuilderSql.append(" and h.documentType = :documentType ");
			}	
			if(Validations.validateIsNotNullAndNotEmpty(applicantCons.getDocNum())){
				stringBuilderSql.append(" and trim(h.documentNumber) = :documentNumber ");
			}
			if(Validations.validateIsNotNullAndNotEmpty(applicantCons.getName())){
				stringBuilderSql.append(" and h.name like :name ");
			}
			if(Validations.validateIsNotNullAndNotEmpty(applicantCons.getLastName1())){
				stringBuilderSql.append(" and h.firstLastName like :firstLastName ");
			}
			if(Validations.validateIsNotNullAndNotEmpty(applicantCons.getLastName2())){
				stringBuilderSql.append(" and h.secondLastName like :secondLastName ");
			}
			if(Validations.validateIsNotNullAndNotEmpty(applicantCons.getBusinessName())){
				stringBuilderSql.append(" and h.fullName like :businessName ");
			}
					
			Query query = em.createQuery(stringBuilderSql.toString());			
			
			if(Validations.validateIsNotNullAndNotEmpty(applicantCons.getDocType())){
				query.setParameter("documentType", applicantCons.getDocType());
			}	
			if(Validations.validateIsNotNullAndNotEmpty(applicantCons.getDocNum())){
				query.setParameter("documentNumber", applicantCons.getDocNum());
			}
			if(Validations.validateIsNotNullAndNotEmpty(applicantCons.getName())){
				query.setParameter("name","%"+applicantCons.getName()+"%");
			}
			if(Validations.validateIsNotNullAndNotEmpty(applicantCons.getLastName1())){
				query.setParameter("firstLastName","%"+applicantCons.getLastName1()+"%");
			}
			if(Validations.validateIsNotNullAndNotEmpty(applicantCons.getLastName2())){
				query.setParameter("secondLastName","%"+applicantCons.getLastName2()+"%");
			}
			if(Validations.validateIsNotNullAndNotEmpty(applicantCons.getBusinessName())){
				query.setParameter("businessName","%"+applicantCons.getBusinessName()+"%");
			}
		    
		    List<Holder> listHolderFilter = new ArrayList<Holder>();		       
		    list = (List<Holder>)query.getResultList();		    
		    
		    Long identificator = new Long(-1);		    
		    
		    if(Validations.validateListIsNotNullAndNotEmpty(list)){   
		       for(Holder h : list){
		    	   if(!identificator.equals(h.getIdHolderPk())){
		    		   listHolderFilter.add(h);
		    		   identificator = h.getIdHolderPk();
		    	   }
		       }
		    }		       
		    if(Validations.validateListIsNotNullAndNotEmpty(listHolderFilter)){
		    	list = listHolderFilter;
		    }	    
		    
		} catch(Exception e){
			list = null;		
		}		
		return list;
		
	}
	/**
	 * 
	 * 
	 * 
	 * */
	@SuppressWarnings("unchecked")
	public List<Applicant> searchApplicantsConsultMgmt (Date initial, Date end, String app_num) {
		
		StringBuilder strb = new StringBuilder();
		strb.append(" SELECT a FROM Applicant a  ");
		strb.append(" WHERE 1=1  ");
		strb.append("   AND a.processQuantity is not null  ");
		
		if(initial != null && end != null){
			strb.append(" AND trunc(a.requestDate) between :dateIni and :dateEnd  ");
		}
		if(app_num != null) {
			strb.append(" AND a.applicantNum = :appNum  ");
		}
		
		Query query = em.createQuery(strb.toString());
		
		if(initial!=null && end!=null){
			query.setParameter("dateIni", initial);
			query.setParameter("dateEnd", end);
		}
		if(app_num != null) {
			query.setParameter("appNum", app_num);
		}
		
		return (List<Applicant>)query.getResultList();
		
	}
	
	@SuppressWarnings("unchecked")
	public List<ApplicantConsult> findApplicantConsultSearch (Applicant a) {
		
		StringBuilder str = new StringBuilder();
		str.append(" select ac from ApplicantConsult ac ");
		str.append(" where 1=1 ");
		str.append("   and ac.applicant.idApplicantPk = :idApplicant ");
		
		Query q = em.createQuery(str.toString());
		
		q.setParameter("idApplicant", a.getIdApplicantPk());
		
		return (List<ApplicantConsult>)q.getResultList();
		
	}
	
	@SuppressWarnings("unchecked")
	public List<ApplicantConsultRequest> findApplicantConsultRequestSearch (ApplicantConsult ac) {
		
		StringBuilder str = new StringBuilder();
		str.append(" select acr from ApplicantConsultRequest acr ");
		str.append(" where 1=1 ");
		str.append("    and acr.applicantConsult.idApplicantConsultPk = :idApplicantConsult ");
		
		Query q = em.createQuery(str.toString());
		
		q.setParameter("idApplicantConsult", ac.getIdApplicantConsultPk());
		
		return (List<ApplicantConsultRequest>)q.getResultList();
		
	}
		
	/**
	 * 
	 * ACTUALIZA DATOS ADICIONALES DE LAS BUSQUEDAS REALIZADAS POR SOLICITUD
	 * 
	 * */
	public void updateAdditionalRequestData (Applicant applicant, Integer quantity, Integer observed, Integer verified) {
		
		StringBuilder strup = new StringBuilder();
		
		strup.append(" update Applicant a set a.processQuantity = :quantity, ");
		strup.append(" a.processObserved = :observed, ");
		strup.append(" a.processVerified = :verified ");
		strup.append(" where a.idApplicantPk = :idApplicant ");
		
		Query query = em.createQuery(strup.toString());
		
		query.setParameter("idApplicant", applicant.getIdApplicantPk());
		query.setParameter("quantity", quantity);
		query.setParameter("observed", observed);
		query.setParameter("verified", verified);
		
		query.executeUpdate();
		
	}
	
	/**
	 * VERIFICA TENENCIAS
	 * */
	public Integer verifyHoldings (Long holderId) {		
		StringBuilder str = new StringBuilder();		
		str.append(" select count(*) ");
		str.append(" from HOLDER h  ");
		str.append(" 	join HOLDER_ACCOUNT_DETAIL had on h.ID_HOLDER_PK = had.ID_HOLDER_FK  ");
		str.append(" 	join HOLDER_ACCOUNT_BALANCE hab on hab.ID_HOLDER_ACCOUNT_PK = had.ID_HOLDER_ACCOUNT_FK  ");
		str.append(" where 1=1  ");
		str.append("   	and (hab.TOTAL_BALANCE > 0 or hab.REPORTED_BALANCE > 0)  ");
		str.append(" 	and h.STATE_HOLDER = 496  ");
		str.append("   	and h.ID_HOLDER_PK = :holderPk  ");
		
		Query query = em.createNativeQuery(str.toString());
		query.setParameter("holderPk", holderId);		
		return Integer.parseInt(query.getSingleResult().toString());		
	}	

	public Long getIdHolder (ApplicantConsultRequest acr) throws ServiceException {
		String res = null;		
		try {
			StringBuilder str = new StringBuilder();
			str.append(" SELECT ID_HOLDER_FK ");
			str.append(" FROM APPLICANT_CONSULT_REQUEST ");
			str.append(" WHERE ID_APPLICANT_CONS_REQ_PK = :idApplicantConsultRequest ");
			Query q = em.createNativeQuery(str.toString());		
			q.setParameter("idApplicantConsultRequest", acr.getIdApplicantConsRegPk());
			res = q.getSingleResult().toString();
		} catch(NoResultException no) {
			return null;
		}
		return new Long(res);		
	}
	
	public void dropCurrentApplicantConsult (ApplicantConsult appConsultDel) {
		Query queryDetail = em.createQuery("DELETE FROM ApplicantConsult app WHERE app.idApplicantConsultPk = :appConsultPk");
		queryDetail.setParameter("appConsultPk", appConsultDel.getIdApplicantConsultPk());
		queryDetail.executeUpdate();		
	}
	
	public void dropCurrentRequester (AppRequester requester) {
		Query query = em.createQuery("DELETE FROM AppRequester app WHERE app.idAppRequesterPk = :requesterId");
		query.setParameter("requesterId", requester.getIdAppRequesterPk());
		query.executeUpdate();
	}
	
	@SuppressWarnings("unchecked")
	public List<ParameterTable> getListDescriptionDocSource(Integer parameterId) throws ServiceException {		
		StringBuilder str = new StringBuilder();
		str.append("	select PARAMETER_TABLE_PK");
		str.append("	  ,DESCRIPTION");
		str.append("	from PARAMETER_TABLE");
		str.append("	where MASTER_TABLE_FK = :parameterId");
		
		Query query = em.createNativeQuery(str.toString());
		query.setParameter("parameterId", parameterId);
		
		List<Object[]> listObjects = query.getResultList();
		List<ParameterTable> sources = new ArrayList<ParameterTable>();
		ParameterTable parameter;
		for(Object[] temp: listObjects){
			parameter = new ParameterTable();
			parameter.setParameterTablePk(Integer.parseInt(temp[0].toString()));
			parameter.setDescription(temp[1].toString());
			sources.add(parameter);
		}		
		return sources;		
	}	

}
