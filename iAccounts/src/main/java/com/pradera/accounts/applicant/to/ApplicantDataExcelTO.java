package com.pradera.accounts.applicant.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ApplicantDataExcelTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Integer codeResult;
	private String msgResult;
	
	private String applicantNum;
	private String docType;
	private String docSearch;
	private String docNum;
	private String name;
	private String lastName1;
	private String lastName2;
	private String businessName;
	private List<ApplicantDataExcelTO> lstDataExcelResult;
	private List<String> lstErrorReadExcel;
	
	public ApplicantDataExcelTO() {
		this.lstDataExcelResult = new ArrayList<>();
		this.lstErrorReadExcel = new ArrayList<>();
	}
	public Integer getCodeResult() {
		return codeResult;
	}
	public void setCodeResult(Integer codeResult) {
		this.codeResult = codeResult;
	}
	public String getMsgResult() {
		return msgResult;
	}
	public void setMsgResult(String msgResult) {
		this.msgResult = msgResult;
	}
	public String getApplicantNum() {
		return applicantNum;
	}
	public void setApplicantNum(String applicantNum) {
		this.applicantNum = applicantNum;
	}
	public String getDocType() {
		return docType;
	}
	public void setDocType(String docType) {
		this.docType = docType;
	}
	public String getDocSearch() {
		return docSearch;
	}
	public void setDocSearch(String docSearch) {
		this.docSearch = docSearch;
	}
	public String getDocNum() {
		return docNum;
	}
	public void setDocNum(String docNum) {
		this.docNum = docNum;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName1() {
		return lastName1;
	}
	public void setLastName1(String lastName1) {
		this.lastName1 = lastName1;
	}
	public String getLastName2() {
		return lastName2;
	}
	public void setLastName2(String lastName2) {
		this.lastName2 = lastName2;
	}
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public List<ApplicantDataExcelTO> getLstDataExcelResult() {
		return lstDataExcelResult;
	}
	public void setLstDataExcelResult(List<ApplicantDataExcelTO> lstDataExcelResult) {
		this.lstDataExcelResult = lstDataExcelResult;
	}
	public List<String> getLstErrorReadExcel() {
		return lstErrorReadExcel;
	}
	public void setLstErrorReadExcel(List<String> lstErrorReadExcel) {
		this.lstErrorReadExcel = lstErrorReadExcel;
	}
	
	
}
