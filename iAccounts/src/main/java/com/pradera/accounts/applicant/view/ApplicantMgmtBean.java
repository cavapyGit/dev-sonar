package com.pradera.accounts.applicant.view;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.omnifaces.util.Faces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import com.pradera.accounts.applicant.facade.ApplicantServiceFacade;
import com.pradera.accounts.applicant.to.ApplicantDataExcelTO;
import com.pradera.accounts.applicant.to.ApplicantTO;
import com.pradera.accounts.util.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.AppRequester;
import com.pradera.model.accounts.Applicant;
import com.pradera.model.accounts.ApplicantConsult;
import com.pradera.model.accounts.ApplicantConsultRequest;
import com.pradera.model.accounts.Holder;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLoggerFile;
/**
* 
* <ul><li>Copyright EDV 2020.</li></ul> 
* @author RCHIARA.
* 
*
*/
@DepositaryWebBean
@LoggerCreateBean
public class ApplicantMgmtBean extends GenericBaseBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
//	private List<ParameterTable> listApplicants;
	private List<AppRequester> listApplicants;
	private List<ParameterTable> listDocumentTypes;
	private Applicant applicant;
	private ApplicantConsult applicantConsult;
	private ApplicantConsultRequest applicantConsultReq;
	private ApplicantTO applicantTO;
	private List<ApplicantConsult> listApplicantsConsult;
	private transient StreamedContent respaldo;
	private byte[] respaldo_temp;
	private String respaldo_name;
	private transient StreamedContent excel;
	private byte[] excel_temp;
	private String excel_name;
	private List<ApplicantConsultRequest> listApplicantConsultRequest;
	private String numero_documento=null, nombre=null, paterno=null, materno=null, razon_social=null;
	private Integer tipo_documento=null;
	private String tipo_documento_desc=null;
	private boolean indSaved = false, indSearched = false, indConfirmed = false; 
	private boolean indAggregate = false, indAppSabed = false, indGenerable = true;
	private Integer quantity = 0, observed = 0, verified = 0;	
	private ApplicantConsultDataModel applicantConsultDataModel;
	private ApplicantConsultRequestDataModel applicantConsultRequestDataModel;
	private ApplicantSearchDataModel applicantSearchDataModel;
	private GenericDataModel<ApplicantConsult> applicantConsultRegister;
	private GenericDataModel<ApplicantConsultRequest> applicantConsultRequestForReports;
	private ApplicantConsult applicantConsultSelected = new ApplicantConsult();
	private List<ApplicantConsult> listApplicantConsultSelected = new ArrayList<ApplicantConsult>();
	private List<ApplicantConsultRequest> listApplicantConsultRequestSelected = new ArrayList<ApplicantConsultRequest>();
	private String parameter;
	private GenericDataModel<AppRequester> listApplicantRequester;
	private AppRequester requesterSelected = new AppRequester();
	private AppRequester requesterRegister = new AppRequester();
	private Map<Integer,String> documentSourceDescription = null;
	private List<String> lstErrorReadExcel;
	
	@EJB
	ParameterServiceBean parameterServiceBean;
	
	@EJB
	ApplicantServiceFacade applicantServiceFacade;
	
	@Inject
	private UserInfo userInfo;
	
	public ApplicantMgmtBean() {}
	
	@PostConstruct
	public void init() {		
		applicant = new Applicant();
		applicantConsult = new ApplicantConsult();
		applicantConsultReq = new ApplicantConsultRequest();
		applicantTO = new ApplicantTO();
		listApplicantsConsult = new ArrayList<ApplicantConsult>();
		documentSourceDescription = new HashMap<Integer,String>();
		try {			
//			listApplicants = applicantServiceFacade.getListApplicants();
			listApplicants = applicantServiceFacade.getListRequesteres();
			listDocumentTypes = applicantServiceFacade.getDocumentTypes();			
			if (!FacesContext.getCurrentInstance().isPostback()) {
				applicantTO.setInitialDate(CommonsUtilities.currentDate());
				applicantTO.setEndDate(CommonsUtilities.currentDate());
			}			
			for (ParameterTable parameter : applicantServiceFacade.getListDescriptionDocSource(MasterTableType.DOCUMENT_SOURCE.getCode())) {
				documentSourceDescription.put(parameter.getParameterTablePk(), parameter.getDescription());
			}
		} catch(Exception e){
			e.printStackTrace();
		}		
	}
	
	/**
	 * VIEW BUSQUEDA INDIVIDUAL
	 * */
	public String loadRegistrationPage() {		
		cleanApplicant();
		cleanApplicantConsult();
		cleanApplicantConsultRequest();
		applicant.setRequestDate(CommonsUtilities.currentDate());
		respaldo = null;
		respaldo_name = null;
		respaldo_temp = null;
		applicant.setBackFile(null);
		indConfirmed = false;
		indAggregate = false;
		indAppSabed = false;
		indGenerable = true;
		return "goToRegisterApplicantReq";		
	}	
	
	/**
	 * REGISTRA SOLICITUD
	 * */
	public void saveApplicant() {		
		try {						
			if(Validations.validateIsNullOrEmpty(applicant.getIdApplicantPk())) {				
				applicant.setFilename(respaldo_name);
				applicant.setProcessState(GeneralConstants.ONE_VALUE_INTEGER);
				applicant.setUserReg(userInfo.getUserAccountSession().getUserName());//USER_REG				
				if(Validations.validateIsNotNullAndNotEmpty(applicant.getApplicantType())) {
					AppRequester requester = applicantServiceFacade.getRequesterById(new Long(applicant.getApplicantType()));
					if(Validations.validateIsNotNullAndNotEmpty(requester)) {
						applicant.setApplicantDesc((requester.getNameRequester()));
					}
				} else {
					Object[] bodyData = new Object[0];
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_ALERT_APPLICANT_TYPE_MISSING, bodyData));
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
				if (applicant.getApplicantNum() == null) {
					Object[] bodyData = new Object[0];
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_ALERT_APPLICANT_NUM_MISSING, bodyData));
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
				if (applicant.getFilename() == null) {
					Object[] body = new Object[0];
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							PropertiesUtilities.getMessage(PropertiesConstants.ALERT_BACK_FILE, body));
					JSFUtilities.showSimpleValidationDialog();			
					return;
				}				
				applicantServiceFacade.registerApplicant(applicant);
				indAppSabed = true;
				
				Object[] bodyData = new Object[1];
				bodyData[0] = applicant.getApplicantNum().toString();
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						PropertiesUtilities.getMessage(GeneralConstants.LBL_SUCCESS_APPLICANT_REGISTERED, bodyData));
				JSFUtilities.showSimpleValidationDialog();
				return;			
			} else {
				Object[] bodyData = new Object[0];
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_ALERT_APPLICANT_IS_ALREADY_REGISTERED, bodyData));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}			
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * REGISTRA BUSQUEDA INDIVIDUAL
	 * */
	public void saveApplicantConsult() {		
		applicantConsult = new ApplicantConsult();		
		try {
			if(Validations.validateIsNullOrEmpty(applicant.getIdApplicantPk())) {
				Object[] bodyData = new Object[0];
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_ALERT_NO_APPLICANT_REGISTERED, bodyData));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}			
//			if (tipo_documento != null)	
				applicantConsult.setDocType(tipo_documento);
			if (numero_documento != null)
				applicantConsult.setDocNum(numero_documento);
			if (nombre != null) 
				applicantConsult.setName(nombre);
			if (paterno != null)
				applicantConsult.setLastName1(paterno);
			if (materno != null)
				applicantConsult.setLastName2(materno);
			if (razon_social != null)
				applicantConsult.setBusinessName(razon_social);
			
			applicantConsult.setApplicant(applicant);
			applicantConsult.setApplicantNum(applicant.getApplicantNum());
			applicantConsult.setIndEditable(GeneralConstants.ONE_VALUE_INTEGER);			
			if (numero_documento == null && razon_social == null && (nombre != null || paterno != null || materno != null)) 
				applicantConsult.setDocSearch(SEARCH_TYPE_NAME);
			else if (numero_documento == null && razon_social != null && nombre == null && paterno == null && materno == null)
				applicantConsult.setDocSearch(SEARCH_TYPE_BUSINESS);
			else {
				if (tipo_documento != null) {
					ParameterTable paramDoc = applicantServiceFacade.getParameterName(tipo_documento);
					if(Validations.validateIsNotNullAndNotEmpty(paramDoc)) {
						tipo_documento_desc = paramDoc.getDescription();
						applicantConsult.setDocSearch(tipo_documento_desc);
					}
				}
			}			
			if (numero_documento != null || nombre != null || paterno != null || materno != null || razon_social != null) {
				
				if (tipo_documento != null && numero_documento == null) {
					Object[] bodyData = new Object[0];
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_ALERT_MUST_ENTER_DOCUMENT_NUMBER, bodyData));
					JSFUtilities.showSimpleValidationDialog();
					return;	
				} 
//				else if (tipo_documento == null && numero_documento != null) {
//					Object[] bodyData = new Object[0];
//					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
//							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_ALERT_MUST_ENTER_DOCUMENT_TYPE, bodyData));
//					JSFUtilities.showSimpleValidationDialog();
//					return;	
//				}				
				applicantConsult = applicantServiceFacade.registerApplicantConsult(applicantConsult);//GUARDA PERSONA PARA BUSQUEDA	
				
			} else {				
				Object[] bodyData = new Object[0];
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_ALERT_SPECIFY_DATA_OF_SEARCH, bodyData));
				JSFUtilities.showSimpleValidationDialog();
				return;			
			}			
			indSearched = true;			
			tipo_documento = null; numero_documento = null; nombre = null; paterno = null; materno = null; razon_social = null;			
			listApplicantsConsult.add(applicantConsult);			
			applicantConsultRegister = new GenericDataModel<ApplicantConsult>(listApplicantsConsult);
			applicantConsultDataModel = new ApplicantConsultDataModel(listApplicantsConsult);
			indAggregate = false;
			
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * ELIMINA UNA SOLICITUD DE BUSQUEDA
	 * */
	public void dropCurrentApplicantConsult(ApplicantConsult appConsultDel) throws ServiceException {
		applicantServiceFacade.dropCurrentApplicantConsult(appConsultDel);		
	}
	
	@LoggerAuditWeb
	public void deleteApplicantConsultSelected() throws ServiceException {		
		boolean flag = true;
		if(Validations.validateListIsNullOrEmpty(listApplicantConsultSelected)) {
			Object[] bodyData = new Object[0];
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED,bodyData));
			JSFUtilities.showSimpleValidationDialog();
		    flag = false;			
		} 		
		if (flag) {
			for (ApplicantConsult applicantConsult : listApplicantConsultSelected) {
				dropCurrentApplicantConsult(applicantConsult);			
				listApplicantsConsult.remove(applicantConsult);			
			}
			applicantConsultRegister = new GenericDataModel<ApplicantConsult>(listApplicantsConsult);
			applicantConsultDataModel = new ApplicantConsultDataModel(listApplicantsConsult);

			Object[] bodyData = new Object[0];
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage(PropertiesConstants.ALERT_DROP_APPLICANT_CONSULT_SUCCESS, bodyData));
			JSFUtilities.showSimpleValidationDialog();			
		}		
	}	
	
	/**
	 * BUSCA CUIS
	 * */
	public void beforeSearchHolders() {		
		Object[] bodyData = new Object[0];
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
				PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CONFIRM_SEARCH,bodyData));
		JSFUtilities.updateComponent("opnlDialogs");
		JSFUtilities.executeJavascriptFunction("PF('widgetConfirmSearch').show()");	
		
	}
	@LoggerAuditWeb
	public void searchHolders(){				
		try {
			quantity = 0; observed = 0; verified = 0;
			listApplicantConsultRequest = new ArrayList<ApplicantConsultRequest>();			
			quantity = listApplicantsConsult.size();
			for (ApplicantConsult appConsult : listApplicantsConsult) {				
				List<Holder> listHolder = new ArrayList<Holder>();	
				listHolder = applicantServiceFacade.getListHolderServiceFacade(appConsult);				
				if (listHolder.size() > 0) {					
					for (Holder holder : listHolder) {						
						ApplicantConsultRequest appConsReq = new ApplicantConsultRequest();						
						ParameterTable estado = applicantServiceFacade.getParameterName(holder.getStateHolder());
						String estadoHolder = estado.getDescription();
						holder.setStateDescription(estadoHolder);						
						appConsReq.setHolder(holder);
						appConsReq.setDescription(holder.getFullName());
						if(holder.getDocumentSource() != null) {
							holder.setDescriptionDocumentSource(documentSourceDescription.get(holder.getDocumentSource()).toString());
						}
						appConsReq.setIssued(holder.getDescriptionDocumentSource());
						appConsReq.setRegDateHolder(holder.getRegistryDate());
						appConsReq.setIndStateHolder(holder.getStateHolder());						
						appConsReq.setIndVerifiedConsult(GeneralConstants.ONE_VALUE_INTEGER);//1-ENCONTRADO, 0-NO ENCONTRADO	
						appConsReq.setIndCuiExist(GeneralConstants.ONE_VALUE_INTEGER);//1-TIENE CUI, 0-NO TIENE CUI
						appConsReq.setApplicantConsult(appConsult);
						appConsReq.setApplicantNum(appConsult.getApplicantNum());
						appConsReq.setDocType(appConsult.getDocType());
						appConsReq.setDocNum(appConsult.getDocNum());
						appConsReq.setIndStateConsult(GeneralConstants.ONE_VALUE_INTEGER);//1-REGISTRADO, 0-PROCESADO'
						appConsReq.setIndEditable(GeneralConstants.ONE_VALUE_INTEGER);//1-EDITABLE
						Integer cant = applicantServiceFacade.verifyHoldings(holder.getIdHolderPk());
						if (cant > 0) {
							appConsReq.setIndSecValid(GeneralConstants.ONE_VALUE_INTEGER);//1-TIENE TENENCIA, 0-NO TIENE TENENCIA
						} else {
							appConsReq.setIndSecValid(GeneralConstants.ZERO_VALUE_INTEGER);//1-TIENE TENENCIA, 0-NO TIENE TENENCIA
						}
						listApplicantConsultRequest.add(appConsReq);//TODO: fisico o desma
						verified++;					
					}					
				} else {
					ApplicantConsultRequest appConsReq = new ApplicantConsultRequest();						
					appConsReq.setIndVerifiedConsult(GeneralConstants.ZERO_VALUE_INTEGER);//1-ENCONTRADO, 0-NO ENCONTRADO'
					appConsReq.setIndCuiExist(GeneralConstants.ZERO_VALUE_INTEGER);//1-TIENE CUI, 0-NO TIENE CUI
					appConsReq.setApplicantConsult(appConsult);
					appConsReq.setApplicantNum(appConsult.getApplicantNum());
					appConsReq.setDocType(appConsult.getDocType());
					appConsReq.setDocNum(appConsult.getDocNum());
					appConsReq.setIndStateConsult(GeneralConstants.ONE_VALUE_INTEGER);//1-REGISTRADO, 0-PROCESADO'
					appConsReq.setIndEditable(GeneralConstants.ONE_VALUE_INTEGER);//EDITABLE					
					listApplicantConsultRequest.add(appConsReq);
					observed++;
					
				}
			}	
			//Lista a persistir
			List<ApplicantConsultRequest> listMain = new ArrayList<ApplicantConsultRequest>();
			for (ApplicantConsultRequest appConsultRequest : listApplicantConsultRequest) 
				if (appConsultRequest.getIndVerifiedConsult() == 1) 
					listMain.add(appConsultRequest);
			Map<Long, ApplicantConsultRequest> map = new HashMap<Long, ApplicantConsultRequest>();
			for (ApplicantConsultRequest appConsultRequest : listApplicantConsultRequest) {
				if (appConsultRequest.getIndVerifiedConsult() == 1) {
					map.put(appConsultRequest.getIdHolderPk(), appConsultRequest);
				}
			}
			List<ApplicantConsultRequest> appConsReqForDatamodel =  new ArrayList<ApplicantConsultRequest>(listMain);
			verified = appConsReqForDatamodel.size();
			applicantConsultRequestDataModel = new ApplicantConsultRequestDataModel(appConsReqForDatamodel);
			applicantConsultRequestForReports = new GenericDataModel<ApplicantConsultRequest>(appConsReqForDatamodel);
			indSaved = true;			
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		
	}	
	
	/**
	 * ACTUALIZA RESULTADOS DE LA BUSQUEDA INDIVIDUAL - ENCONTRADOS - NO ENCONTRADOS
	 * */
	public void beforeSaveApplicantConsultRequest () {		
		Object[] bodyData = new Object[0];
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
				PropertiesUtilities.getMessage(PropertiesConstants.ALERT_FINALICE_SEARCH,bodyData));
		JSFUtilities.updateComponent("opnlDialogs");
		JSFUtilities.executeJavascriptFunction("PF('widgetConfirmFinalice').show()");
		
	}
	
	public void saveApplicantConsultRequest(){
		try {			
			ArrayList<ApplicantConsultRequest> list_acr_saved = new ArrayList<ApplicantConsultRequest>();			
			for (ApplicantConsultRequest applicantConsultReq : listApplicantConsultRequest) {				
				if(Validations.validateIsNull(applicantConsultReq.getDescription())) {
					if(nombre != null || paterno != null || materno != null) {
						applicantConsultReq.setDescription(nombre + " " + paterno + " " + materno);
					}					
				}				
				applicantConsultReq.setIndEditable(GeneralConstants.ZERO_VALUE_INTEGER);//NO EDITABLE
				ApplicantConsultRequest acr_saved = applicantServiceFacade.registerApplicantConsultRequest(applicantConsultReq);
												
				if (acr_saved.getIndVerifiedConsult() == 1) 
					list_acr_saved.add(acr_saved);				
			}			
			Timestamp process_date = new Timestamp(System.currentTimeMillis());
			applicant.setProcessDate(process_date);
			applicant.setProcessQuantity(quantity);
			applicant.setProcessObserved(observed);
			applicant.setProcessVerified(verified);
			applicantServiceFacade.updateAdditionalRequestData(applicant);
			indSaved = false;
			indConfirmed = true;
			indSearched = false;
			indAggregate = true;
			indGenerable = false;
			applicantConsultRequestDataModel = new ApplicantConsultRequestDataModel(list_acr_saved);
			applicantConsultRequestForReports = new GenericDataModel<ApplicantConsultRequest>(list_acr_saved);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	@LoggerAuditWeb
	public void generateReportsApplicantConsultRequestSelected () throws ServiceException {
		if(Validations.validateListIsNullOrEmpty(listApplicantConsultRequestSelected)) {
			if(indSaved) {			
				Object[] bodyData = new Object[0];
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_ALERT_YOU_MUST_END_SEARCH, bodyData));
				JSFUtilities.showSimpleValidationDialog();
				return;				
			}
			
			Object[] bodyData = new Object[0];
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED,bodyData));
			JSFUtilities.showSimpleValidationDialog();
			return;
		} 		
		DailyExchangeRates dayExchangeRateUSD = parameterServiceBean.getDayExchangeRate(CommonsUtilities.currentDate(),CurrencyType.USD.getCode());		
		if (Validations.validateIsNullOrEmpty(dayExchangeRateUSD)) {
			Object[] bodyData = new Object[0];
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_ALERT_EXCHANGE_RATE_REQUIRED, bodyData));
			JSFUtilities.showSimpleValidationDialog();
			return;				
		}
		if(indSaved) {			
			Object[] bodyData = new Object[0];
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_ALERT_YOU_MUST_END_SEARCH, bodyData));
			JSFUtilities.showSimpleValidationDialog();
			return;				
		}
		if (listApplicantConsultRequestSelected.size() > 20) {
			Object[] bodyData = new Object[0];
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getGenericMessage(GeneralConstants.THERE_ARE_TOO_MANY_REPORTS_THERE_COULD_BE_A_PROBLEM, bodyData));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		getGenerateMassiveFileReport(listApplicantConsultRequestSelected);
		
		Object[] bodyData = new Object[0];
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
				PropertiesUtilities.getGenericMessage(GeneralConstants.REPORT_WERE_GENERATED_CORRECTLY, bodyData));
		JSFUtilities.showSimpleValidationDialog();
	}
	
	@LoggerAuditWeb
	public void getGenerateMassiveFileReport(List<ApplicantConsultRequest> applicantConsultRequests) throws ServiceException {
		for (ApplicantConsultRequest applicantConsultRequest : applicantConsultRequests) {			
			if (Validations.validateIsNotNullAndNotEmpty(applicantConsultRequest.getIdApplicantConsRegPk())) {
				String nameReport = "EDVRPT16_" + applicantConsultRequest.getIdApplicantConsRegPk().toString();
				if (Validations.validateIsNotNullAndNotEmpty(applicantConsultRequest.getHolder())) {
					Long holderId = applicantConsultRequest.getHolder().getIdHolderPk();
					ReportLoggerFile reportLoggerFile = applicantServiceFacade.findReport(holderId, nameReport);
					if (Validations.validateIsNull(reportLoggerFile)) {
						Long consultId = applicantConsultRequest.getIdApplicantConsRegPk();
						String holderDesc = applicantConsultRequest.getHolder().getFullName();		
						this.applicantServiceFacade.generateReportFile(holderId, consultId, holderDesc);		
						try {
							Thread.sleep(5000);
						} catch (InterruptedException ex) {
							excepcion.fire(new ExceptionToCatchEvent(ex));
						}
					}
				}
			}		
		}
	}
	
	/**GENERA REPORTE TENENCIAS ONE BY ONE*/
	@LoggerAuditWeb
	public void getGenerateFilePortafolioReport(ApplicantConsultRequest applicantConsultRequest) throws ServiceException {		
		if(indSaved) {			
			Object[] bodyData = new Object[0];
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_ALERT_YOU_MUST_END_SEARCH, bodyData));
			JSFUtilities.showSimpleValidationDialog();
			return;				
		}		
		DailyExchangeRates dayExchangeRateUSD = parameterServiceBean.getDayExchangeRate(CommonsUtilities.currentDate(),CurrencyType.USD.getCode());		
		if (Validations.validateIsNullOrEmpty(dayExchangeRateUSD)) {
			Object[] bodyData = new Object[0];
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_ALERT_EXCHANGE_RATE_REQUIRED, bodyData));
			JSFUtilities.showSimpleValidationDialog();
			return;				
		}		
		Long idHolderPk = applicantConsultRequest.getHolder().getIdHolderPk();
		Long idApplicantConsultRequest = applicantConsultRequest.getIdApplicantConsRegPk();
		String holder_desc = applicantConsultRequest.getHolder().getFullName();		
		this.applicantServiceFacade.generateReportFile(idHolderPk, idApplicantConsultRequest, holder_desc);		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}		
	}
		
	/**VERIFICA QUE REPORTE TENENCIAS*/
	public Boolean getVisibleFilePortafolioReport(ApplicantConsultRequest applicantConsultRequest) {		
		Boolean bool = false;
		if(Validations.validateIsNotNullAndNotEmpty(applicantConsultRequest.getIdApplicantConsRegPk())) {
			String nameReport = "EDVRPT16_"+applicantConsultRequest.getIdApplicantConsRegPk().toString();
			if(Validations.validateIsNotNullAndNotEmpty(applicantConsultRequest.getHolder())) {				
				Long holderId = applicantConsultRequest.getHolder().getIdHolderPk();	
				ReportLoggerFile reportLoggerFile = applicantServiceFacade.findReport(holderId, nameReport);
				if(Validations.validateIsNotNull(reportLoggerFile)){ 
					bool = true;
				}				
			}	
		}
		return bool;		
	}
	
	/**DESCARGA REPORTE TENENCIAS*/
	public StreamedContent getStreamedContentTemporalFilePortafolioReport(ApplicantConsultRequest applicantConsultRequest) throws ServiceException {
		String nameReport = "EDVRPT16_"+applicantConsultRequest.getIdApplicantConsRegPk().toString();
		Long holderId = applicantConsultRequest.getHolder().getIdHolderPk();		
		String path = applicantServiceFacade.findFileReportPath(nameReport+holderId+".pdf");
		if(path != null) {
			Path pathFile = Paths.get(path,nameReport+holderId+".pdf");
			if(Files.exists(pathFile)) {
				try {
					Faces.sendFile(Files.readAllBytes(pathFile), nameReport+holderId+".pdf", true);					
				} catch(IOException iox) {
//					log.error(iox.getMessage());
				}
			}
		}
		return null;
	}
	
	/**
	 * SEARCH VIEW OPERACIONES
	 * */
	public void searchApplicantRequest () {
		List<Applicant> listAppCons = new ArrayList<Applicant>();
		listAppCons = applicantServiceFacade.searchApplicantsConsultMgmt(applicantTO);
		List<Applicant> listAppDM = new ArrayList<Applicant>();
		for (Applicant applicant : listAppCons) {
			if(applicant.getProcessState() == 1) {
				applicant.setProcessType("INDIVIDUAL");
			} else {
				applicant.setProcessType("MASIVO");
			}
			listAppDM.add(applicant);
		}		
		applicantSearchDataModel = new ApplicantSearchDataModel(listAppDM);
		
	}
	
	/**
	 * VIEW DETAIL
	 * */
	public void loadApplicantInformation (ActionEvent event) {
		try {			
			indSaved = false;
			indGenerable = false;
			applicant = new Applicant();
			applicantConsultDataModel = new ApplicantConsultDataModel();
			applicantConsultRequestDataModel = new ApplicantConsultRequestDataModel();
			applicantConsultRequestForReports = new GenericDataModel<ApplicantConsultRequest>();
			Holder holder = new Holder();
			
			List<ApplicantConsult> listApplicantConsult = new ArrayList<ApplicantConsult>();
			List<ApplicantConsultRequest> listApplicantConsultRequest = new ArrayList<ApplicantConsultRequest>();
			
			if (event != null) {				
				applicant = (Applicant) event.getComponent().getAttributes().get("applicantInformation");				
				listApplicantConsult = applicantServiceFacade.findApplicantConsultSearch(applicant);				
				for (ApplicantConsult applicantConsult : listApplicantConsult) {					
					List<ApplicantConsultRequest> acrSearchList = new ArrayList<ApplicantConsultRequest>();
					acrSearchList = applicantServiceFacade.findApplicantConsultRequestSearch(applicantConsult);					
					for (ApplicantConsultRequest acr : acrSearchList) {
						Long idHolder = null;
						if (acr.getIndCuiExist() == GeneralConstants.ONE_VALUE_INTEGER) {
							idHolder = applicantServiceFacade.getIdHolder(acr);
						}										
						if (Validations.validateIsNotNull(idHolder)) {
							holder = applicantServiceFacade.findHolderById(idHolder);
						}
						String estadoHolder = null;
						if (Validations.validateIsNotNull(holder.getStateHolder())) {
							ParameterTable estado = applicantServiceFacade.getParameterName(holder.getStateHolder());
							estadoHolder = estado.getDescription();
						}
						holder.setStateDescription(estadoHolder);
						if(holder.getDocumentSource() != null) {
							holder.setDescriptionDocumentSource(documentSourceDescription.get(holder.getDocumentSource()).toString());
						}
						if (acr.getIndVerifiedConsult() == 1) {
							acr.setHolder(holder);
							listApplicantConsultRequest.add(acr);
						}						
					}				
				}				
				applicantConsultDataModel = new ApplicantConsultDataModel(listApplicantConsult);
				
				List<ApplicantConsultRequest> listMain = new ArrayList<ApplicantConsultRequest>();
				for (ApplicantConsultRequest appConsultRequest : listApplicantConsultRequest) 
					if (appConsultRequest.getIndVerifiedConsult() == 1) 
						listMain.add(appConsultRequest);
				listApplicantConsultRequest= new ArrayList<ApplicantConsultRequest>(listMain);
				applicantConsultRequestDataModel = new ApplicantConsultRequestDataModel(listApplicantConsultRequest);				
				applicantConsultRequestForReports = new GenericDataModel<ApplicantConsultRequest>(listApplicantConsultRequest);		
			}			
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	public StreamedContent getGetBackDocumentFile() throws ServiceException{		
		return respaldo;
		
	}
	
	/**
	 * ELIMINA ARCHIVO TEMPORAL DE RESPALDO
	 * */
	public void deleteFileTemp () {
		respaldo = null;
		respaldo_name = null;
		respaldo_temp = null;
		applicant.setBackFile(null);
		JSFUtilities.putSessionMap("sessStreamedRespaldo", null);		
	}
	
	/**
	 * CARGA ARCHIVO DE RESPALDO
	 * */
	public void fileUploadHandler (FileUploadEvent event) {
		String fDisplayName = fUploadValidateFile(event.getFile(),null,getfUploadFileLargeDocumentsSize(),getfUploadFileDocumentsTypes());		
		if(fDisplayName != null){	
			try {				
				InputStream inputFile = new ByteArrayInputStream(event.getFile().getContents());
				respaldo = new DefaultStreamedContent(inputFile, event.getFile().getContentType(), event.getFile().getFileName());
				respaldo_name = event.getFile().getFileName();
				applicant.setBackFile(event.getFile().getContents());
				respaldo_temp = event.getFile().getContents();				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}		
	}	
	
	/**
	 * VIEW BUSQUEDA MASIVA
	 * */
	public String loadRegistrationMassive() {		
		cleanApplicant();
		cleanApplicantConsult();
		cleanApplicantConsultRequest();
		applicant.setRequestDate(CommonsUtilities.currentDate());
		indConfirmed = false;
		respaldo = null;
		respaldo_name = null;
		respaldo_temp = null;
		applicant.setBackFile(null);
		excel = null;
		excel_name = null;
		excel_temp = null;
		indConfirmed = false;
		indAppSabed = false;
		indAggregate = false;
		indGenerable = true;
		return "goToRegisterApplicantExcel";
	}
	
	/**
	 * CARGA ARCHIVO EXCEL
	 * */
	public void readFileExcel (FileUploadEvent event) throws ServiceException, InvalidFormatException {		
		Object[] bodyData = new Object[0];
		if(Validations.validateIsNullOrEmpty(applicant.getIdApplicantPk())) {
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_ALERT_NO_APPLICANT_REGISTERED, bodyData));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		String fDisplayName = fUploadValidateFile(event.getFile(), null, getfUploadFileDocumentsSize(), getfUploadFileDocumentsTypes());
		if(fDisplayName != null){	
			try {					
				InputStream inputFile = new ByteArrayInputStream(event.getFile().getContents());
				excel = new DefaultStreamedContent(inputFile, event.getFile().getContentType(), event.getFile().getFileName());
				excel_name = event.getFile().getFileName();
				excel_temp = event.getFile().getContents();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}		
		
		if (listApplicantsConsult.size() > 0) {
			for (ApplicantConsult applicantConsult : listApplicantsConsult) {
				dropCurrentApplicantConsult(applicantConsult);
			}
		}		
		
		UploadedFile archivoExcel = event.getFile();
		if(!excel_name.contains(".xlsx")) {
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
					                         PropertiesUtilities.getMessage(PropertiesConstants.ALERT_EXCEL_FORMAT,bodyData));
			JSFUtilities.showSimpleValidationDialog();
			return;
		} 
		
		lstErrorReadExcel = new ArrayList<String>();
		applicantConsultRegister = new GenericDataModel<ApplicantConsult>();
		applicantConsultRequestDataModel = new ApplicantConsultRequestDataModel();
		applicantConsultRequestForReports = new GenericDataModel<ApplicantConsultRequest>();
		try {			
			archivoExcel.getInputstream();
			ApplicantDataExcelTO result;			
			result = readDataMassive(archivoExcel.getInputstream());			
			switch (result.getCodeResult()) {
			case READ_EXCEL_SUCCESS:					
				listApplicantsConsult = new ArrayList<ApplicantConsult>();
				for (ApplicantDataExcelTO dataExcel : result.getLstDataExcelResult()) {						
					applicantConsult = new ApplicantConsult();
					if (dataExcel.getDocNum() != null)
						applicantConsult.setDocNum(dataExcel.getDocNum()); 
					if (dataExcel.getName() != null)
						applicantConsult.setName(dataExcel.getName()); 
					if (dataExcel.getLastName1() != null)
						applicantConsult.setLastName1(dataExcel.getLastName1()); 
					if (dataExcel.getLastName2() != null)
						applicantConsult.setLastName2(dataExcel.getLastName2()); 
					if (dataExcel.getBusinessName() != null)
						applicantConsult.setBusinessName(dataExcel.getBusinessName());						
					applicant.setProcessState(GeneralConstants.ZERO_VALUE_INTEGER);
					applicantConsult.setApplicant(applicant);
					applicantConsult.setApplicantNum(applicant.getApplicantNum());
					applicantConsult.setIndEditable(GeneralConstants.ONE_VALUE_INTEGER);						
					if (dataExcel.getDocNum() == null && (dataExcel.getName() != null || dataExcel.getLastName1() != null || dataExcel.getLastName2() != null) && dataExcel.getBusinessName() == null) 
						applicantConsult.setDocSearch(SEARCH_TYPE_NAME);						
					if ((dataExcel.getName() == null && dataExcel.getLastName1() == null && dataExcel.getLastName2() == null)  && dataExcel.getDocNum() != null && dataExcel.getBusinessName() == null)
						applicantConsult.setDocSearch(SEARCH_TYPE_DOCUMENT);						
					if ((dataExcel.getName() == null && dataExcel.getLastName1() == null && dataExcel.getLastName2() == null)  && dataExcel.getDocNum() == null && dataExcel.getBusinessName() != null)
						applicantConsult.setDocSearch(SEARCH_TYPE_BUSINESS);						
					if ((dataExcel.getName() != null || dataExcel.getLastName1() != null || dataExcel.getLastName2() != null) && dataExcel.getDocNum() != null && dataExcel.getBusinessName() == null)
						applicantConsult.setDocSearch(SEARCH_TYPE_DOCUMENT_AND_NAME);						
					if ((dataExcel.getName() != null || dataExcel.getLastName1() != null || dataExcel.getLastName2() != null)  && dataExcel.getDocNum() == null && dataExcel.getBusinessName() != null)
						applicantConsult.setDocSearch(SEARCH_TYPE_BUSINESS_AND_NAME);						
					if ((dataExcel.getName() == null && dataExcel.getLastName1() == null && dataExcel.getLastName2() == null)  && dataExcel.getDocNum() != null && dataExcel.getBusinessName() != null)
						applicantConsult.setDocSearch(SEARCH_TYPE_BUSINESS_AND_DOCUMENT);						
					if ((dataExcel.getName() != null || dataExcel.getLastName1() != null || dataExcel.getLastName2() != null)  && dataExcel.getDocNum() != null && dataExcel.getBusinessName() != null)
						applicantConsult.setDocSearch(SEARCH_TYPE_NAME_BUSINESS_AND_DOCUMENT);											
					
					listApplicantsConsult.add(applicantServiceFacade.registerApplicantConsult(applicantConsult));							
				}										
				if (Validations.validateIsNotNullAndNotEmpty(listApplicantsConsult)) {
					indSearched = true;
				}					
				applicantConsultRegister = new GenericDataModel<ApplicantConsult>(listApplicantsConsult);					
				break;
			case READ_EXCEL_ERROR:					
				if(result.getLstDataExcelResult().isEmpty()) {
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
													 PropertiesUtilities.getMessage(PropertiesConstants.ALERT_EXCEL_FILE_FORMAT,bodyData));
					JSFUtilities.showSimpleValidationDialog();
				} else {
					lstErrorReadExcel = new ArrayList<>(result.getLstErrorReadExcel());
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
													 PropertiesUtilities.getMessage(PropertiesConstants.ALERT_EXCEL_FILE_WITH_ERRORS,bodyData));
					JSFUtilities.showSimpleValidationDialog();
				}
				break;
			}			
		} catch (IOException e) {
			e.printStackTrace();
			showDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					   PropertiesUtilities.getMessage("msg.error.exception", new Object[] { e.getMessage() != null ? e.getMessage().toString() : "?" }));
			JSFUtilities.showSimpleValidationDialog();
		} catch (Exception e) {
			e.printStackTrace();
			showDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					   PropertiesUtilities.getMessage("msg.error.exception", new Object[] { e.getMessage() != null ? e.getMessage().toString() : "?" }));
			JSFUtilities.showSimpleValidationDialog();
		}
	}	
	
	/**variables auxiliares para lectura y procesamiento del archivo excel*/
	private static final int READ_EXCEL_SUCCESS = 1;
	private static final int READ_EXCEL_ERROR = 0;
	private static final String SEARCH_TYPE_NAME = "NOMBRE";
	private static final String SEARCH_TYPE_BUSINESS = "RAZON SOCIAL";
	private static final String SEARCH_TYPE_DOCUMENT = "DOCUMENTO";
	private static final String SEARCH_TYPE_DOCUMENT_AND_NAME = "DOCUMENTO Y NOMBRE";
	private static final String SEARCH_TYPE_BUSINESS_AND_NAME = "NOMBRE Y RAZON SOCIAL";
	private static final String SEARCH_TYPE_BUSINESS_AND_DOCUMENT = "DOCUMENTO Y RAZON SOCIAL";
	private static final String SEARCH_TYPE_NAME_BUSINESS_AND_DOCUMENT = "NOMBRE DOCUMENTO Y RAZON SOCIAL";
	
	/**
	 * LEE Y VALIDA EXCEL
	 * */
	public ApplicantDataExcelTO readDataMassive (InputStream entryFile) throws InvalidFormatException {
		ApplicantDataExcelTO result = new ApplicantDataExcelTO();		
		try {
			XSSFWorkbook libro = new XSSFWorkbook(entryFile);
			XSSFSheet hoja;
			XSSFRow fila;
			XSSFCell celda;			
			hoja = libro.getSheetAt(0);			
			List<ApplicantDataExcelTO> lstDataExcel = new ArrayList<>();
			ApplicantDataExcelTO dataExcel;
			int colDocumento = 0;
			int colNombre = 1;
			int colPaterno = 2;
			int colMaterno = 3;
			int colRazonSocial = 4;			
			int primeraColumna = 1;
			int ultimaColumna = hoja.getLastRowNum();			
			for (int i = primeraColumna; i <= ultimaColumna; i++) {
				fila = hoja.getRow(i);
				if (fila != null) {					
					dataExcel = new ApplicantDataExcelTO();
					String documento = null;
					String nombre = null;
					String paterno = null;
					String materno = null;
					String razonSocial = null;					
					celda = fila.getCell(colDocumento);
					if (celda != null) {
						if (celda.getCellType() == XSSFCell.CELL_TYPE_NUMERIC){ 
							documento = String.valueOf((int)celda.getNumericCellValue());
							documento = documento.trim();
						} else if (celda.getCellType() == XSSFCell.CELL_TYPE_STRING){
							documento = celda.getStringCellValue();						
							documento = documento.trim();
						} else if (celda.getCellType() == XSSFCell.CELL_TYPE_BLANK){							
							if (fila.getCell(colNombre) == null || fila.getCell(colPaterno) == null || fila.getCell(colMaterno) == null || fila.getCell(colRazonSocial) == null) {
								result.setCodeResult(READ_EXCEL_ERROR);
								result.setMsgResult(PropertiesUtilities.getMessage("msg.error.cell.document.required", new Object[] { celda.getReference() }));
								result.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.document.required", new Object[] { celda.getReference() }));
							}
						} else {
							result.setCodeResult(READ_EXCEL_ERROR);
							result.setMsgResult(PropertiesUtilities.getMessage("msg.error.cell.value.document.number", new Object[] { celda.getReference() }));
							result.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.value.document.number", new Object[] { celda.getReference() }));
						}			
					}					
					
					celda = fila.getCell(colNombre);
					if (celda != null) {
						if (celda.getCellType() == XSSFCell.CELL_TYPE_STRING){ 
							nombre = celda.getStringCellValue();
							nombre = nombre.trim();
						}
						else if (celda.getCellType() == XSSFCell.CELL_TYPE_BLANK) 
							nombre = null; 
						else {
							result.setCodeResult(READ_EXCEL_ERROR);
							result.setMsgResult(PropertiesUtilities.getMessage("msg.error.cell.required.string.value", new Object[] { celda.getReference() }));
							result.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.required.string.value", new Object[] { celda.getReference() }));
						}
					}
					
					celda = fila.getCell(colPaterno);
					if (celda != null) {
						if (celda.getCellType() == XSSFCell.CELL_TYPE_STRING){
							paterno = celda.getStringCellValue();
							paterno = paterno.trim();
						}	
						else if (celda.getCellType() == XSSFCell.CELL_TYPE_BLANK)
							paterno = null; 
						else {
							result.setCodeResult(READ_EXCEL_ERROR);
							result.setMsgResult(PropertiesUtilities.getMessage("msg.error.cell.required.string.value", new Object[] { celda.getReference() }));
							result.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.required.string.value", new Object[] { celda.getReference() }));
						}
					}
					
					celda = fila.getCell(colMaterno);
					if (celda != null) {
						if (celda.getCellType() == XSSFCell.CELL_TYPE_STRING){
							materno = celda.getStringCellValue();
							materno = materno.trim();
						}
						else if (celda.getCellType() == XSSFCell.CELL_TYPE_BLANK)
							materno = null; 
						else {
							result.setCodeResult(READ_EXCEL_ERROR);
							result.setMsgResult(PropertiesUtilities.getMessage("msg.error.cell.required.string.value", new Object[] { celda.getReference() }));
							result.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.required.string.value", new Object[] { celda.getReference() }));
						}
					}
					
					celda = fila.getCell(colRazonSocial);
					if (celda != null) {
						if (celda.getCellType() == XSSFCell.CELL_TYPE_STRING){
							razonSocial = celda.getStringCellValue();
							razonSocial = razonSocial.trim();
						}
						else if (celda.getCellType() == XSSFCell.CELL_TYPE_BLANK)
							razonSocial = null;
						else {
							result.setCodeResult(READ_EXCEL_ERROR);
							result.setMsgResult(PropertiesUtilities.getMessage("msg.error.cell.required.string.value", new Object[] { celda.getReference() }));
							result.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.required.string.value", new Object[] { celda.getReference() }));
						}
					}
					dataExcel.setDocNum(documento);
					dataExcel.setName(nombre);
					dataExcel.setLastName1(paterno);
					dataExcel.setLastName2(materno);
					dataExcel.setBusinessName(razonSocial);					
					if(documento != null || nombre != null || paterno != null || materno != null || razonSocial != null){
						lstDataExcel.add(dataExcel);						
					}
				}				
				if (Validations.validateIsNull(result.getCodeResult())) {
					result.setCodeResult(READ_EXCEL_SUCCESS);
					result.setMsgResult(PropertiesUtilities.getMessage("msg.success.read.excel"));
					result.setLstDataExcelResult(lstDataExcel);
				}				
				entryFile.close();				
			}			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("::: se ha producido un error : " + e.getMessage());
			result.setCodeResult(READ_EXCEL_ERROR);
			result.setMsgResult(PropertiesUtilities.getMessage("msg.error.exception", new Object[] { e.getMessage().toString() }));
		}		
		return result;		
	}
	
	/***/
	public static void showDialog(String cabecera, String cuerpo) {
		JSFUtilities.putViewMap("bodyMessageView", cuerpo);
		JSFUtilities.putViewMap("headerMessageView", cabecera);
		
	}
	
	public Boolean isNotStateConfirm(Integer state) {		
		if(state.equals(GeneralConstants.ONE_VALUE_INTEGER)) {
			return false;
		}
		return true;
		
	}
	
	/**CLEANS*/
	public void cleanAll() {
		JSFUtilities.resetViewRoot();
		cleanApplicant();
		cleanApplicantConsult();
		cleanApplicantConsultRequest();
		excel = null;
		excel_name = null;
		excel_temp = null;
		applicant.setBackFile(null);
		applicant.setRequestDate(CommonsUtilities.currentDate());
		
	}
	
	public void cleanApplicantConsultRequest () {
		listApplicantConsultRequest = new ArrayList<ApplicantConsultRequest>();
		applicantConsultRequestDataModel = new ApplicantConsultRequestDataModel();		
		indConfirmed = false;
		indSearched = true;
		indSaved = false;
		indGenerable = true;
	}
	
	public void cleanApplicantConsult () {
		applicantConsult = new ApplicantConsult();
		listApplicantsConsult = new ArrayList<ApplicantConsult>();
		numero_documento=null; 
		nombre=null; 
		paterno=null; 
		materno=null; 
		razon_social=null;
		tipo_documento=null;
		applicantConsultDataModel = new ApplicantConsultDataModel();
		applicantConsultRegister = new GenericDataModel<>();		
	}
	
	public void cleanApplicant () {
		applicant = new Applicant();
		respaldo = null;
		respaldo_name = null;
		respaldo_temp = null;
		indAppSabed = false;
	}
	
	public void cleanApplicantSearch () {
		applicantTO = new ApplicantTO();
		applicantTO.setInitialDate(CommonsUtilities.currentDate());
		applicantTO.setEndDate(CommonsUtilities.currentDate());
		applicantSearchDataModel = new ApplicantSearchDataModel();		
	}
	
	public void cleanApplicantConsultBtn() throws ServiceException {		
		for (ApplicantConsult applicantConsult : listApplicantsConsult) {
			dropCurrentApplicantConsult(applicantConsult);
		}
		cleanApplicantConsult();
		cleanApplicantConsultRequest();		
	}
	
//	############## REQUESTER
	public String loadRequesterRegister () throws ServiceException{
		searchRequesters();		
		return "goToRequesterReg";
	}
	
	public void beforeSaveRequester () {
		if (Validations.validateIsNullOrEmpty(parameter)) {
			Object[] body = new Object[0];
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage(PropertiesConstants.ALERT_PARAMETER_NAME, body));
			JSFUtilities.showSimpleValidationDialog();			
			return;
		}
		Object[] bodyData = new Object[0];
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
				PropertiesUtilities.getMessage(PropertiesConstants.PARAMETER_REGISTRY_CONFIRM,bodyData));
		JSFUtilities.updateComponent("opnlDialogs");
		JSFUtilities.executeJavascriptFunction("PF('widgetSave').show()");
	}
	
	public void saveRequester () throws ServiceException{
		requesterRegister = new AppRequester();
		requesterRegister.setNameRequester(parameter);
		applicantServiceFacade.saveNewRequester(requesterRegister);
		parameter = "";
		searchRequesters();		
		Object[] bodyData = new Object[0];
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
				PropertiesUtilities.getMessage(PropertiesConstants.PARAMETER_REGISTRY_SUCCESS,bodyData));
		JSFUtilities.updateComponent("opnlDialogs");
		JSFUtilities.executeJavascriptFunction("PF('widgetSuccess').show()");
		JSFUtilities.updateComponent("dtableRequesters");
	}
	
	public void searchRequesters () throws ServiceException {
		List<AppRequester> listApplicants = new ArrayList<>();
		listApplicants = applicantServiceFacade.getListRequesteres();
		listApplicantRequester = new GenericDataModel<AppRequester>(listApplicants);
	}
	
	public void beforeDeleteRequester() {
		if (Validations.validateIsNull(requesterSelected)) {
			Object[] bodyData = new Object[0];
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED,bodyData));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		Object[] bodyData = new Object[0];
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
				PropertiesUtilities.getMessage(PropertiesConstants.PARAMETER_DELETE_CONFIRM,bodyData));
		JSFUtilities.updateComponent("opnlDialogs");
		JSFUtilities.executeJavascriptFunction("PF('widgetDelete').show()");
	}
	
	public void deleteRequester() throws ServiceException{
		dropCurrentRequester(requesterSelected);
		searchRequesters();		
		Object[] bodyData = new Object[0];
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
				PropertiesUtilities.getMessage(PropertiesConstants.PARAMETER_DELETE_SUCCESS,bodyData));
		JSFUtilities.updateComponent("opnlDialogs");
		JSFUtilities.executeJavascriptFunction("PF('widgetDeleteSuccess').show()");
		JSFUtilities.updateComponent("dtableRequesters");
	}
	
	public void dropCurrentRequester(AppRequester requester) {
		applicantServiceFacade.dropCurrentRequester(requester);
		JSFUtilities.updateComponent("dtableRequesters");
	}
	
	/*********************************************************************************/
	/**GETTERS - SETTERS*/
	public AppRequester getRequesterRegister() {
		return requesterRegister;
	}

	public void setRequesterRegister(AppRequester requesterRegister) {
		this.requesterRegister = requesterRegister;
	}

	public AppRequester getRequesterSelected() {
		return requesterSelected;
	}

	public void setRequesterSelected(AppRequester requesterSelected) {
		this.requesterSelected = requesterSelected;
	}

	public GenericDataModel<AppRequester> getListApplicantRequester() {
		return listApplicantRequester;
	}

	public void setListApplicantRequester(GenericDataModel<AppRequester> listApplicantRequester) {
		this.listApplicantRequester = listApplicantRequester;
	}
	
	public Applicant getApplicant() {
		return applicant;
	}

	public void setApplicant(Applicant applicant) {
		this.applicant = applicant;
	}

	public ApplicantConsult getApplicantConsult() {
		return applicantConsult;
	}

	public void setApplicantConsult(ApplicantConsult applicantConsult) {
		this.applicantConsult = applicantConsult;
	}

	public ApplicantConsultRequest getApplicantConsultReq() {
		return applicantConsultReq;
	}

	public void setApplicantConsultReq(ApplicantConsultRequest applicantConsultReq) {
		this.applicantConsultReq = applicantConsultReq;
	}
		
	public ApplicantTO getApplicantTO() {
		return applicantTO;
	}

	public void setApplicantTO(ApplicantTO applicantTO) {
		this.applicantTO = applicantTO;
	}

	public List<ApplicantConsult> getListApplicantsConsult() {
		return listApplicantsConsult;
	}

	public void setListApplicantsConsult(List<ApplicantConsult> listApplicantsConsult) {
		this.listApplicantsConsult = listApplicantsConsult;
	}

	public List<AppRequester> getListApplicants() {
		return listApplicants;
	}

	public void setListApplicants(List<AppRequester> listApplicants) {
		this.listApplicants = listApplicants;
	}

	public StreamedContent getBackfileDownload() {
		return respaldo;
	}

	public void setBackfileDownload(StreamedContent backfileDownload) {
		this.respaldo = backfileDownload;
	}
	
	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public String getNumero_documento() {
		return numero_documento;
	}

	public void setNumero_documento(String numero_documento) {
		this.numero_documento = numero_documento;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPaterno() {
		return paterno;
	}

	public void setPaterno(String paterno) {
		this.paterno = paterno;
	}

	public String getMaterno() {
		return materno;
	}

	public void setMaterno(String materno) {
		this.materno = materno;
	}

	public String getRazon_social() {
		return razon_social;
	}

	public void setRazon_social(String razon_social) {
		this.razon_social = razon_social;
	}

	public Integer getTipo_documento() {
		return tipo_documento;
	}

	public void setTipo_documento(Integer tipo_documento) {
		this.tipo_documento = tipo_documento;
	}

	public List<ApplicantConsultRequest> getListApplicantConsultRequest() {
		return listApplicantConsultRequest;
	}

	public void setListApplicantConsultRequest(List<ApplicantConsultRequest> listApplicantConsultRequest) {
		this.listApplicantConsultRequest = listApplicantConsultRequest;
	}

	public boolean isIndSaved() {
		return indSaved;
	}

	public void setIndSaved(boolean indSaved) {
		this.indSaved = indSaved;
	}

	public ApplicantConsultDataModel getApplicantConsultDataModel() {
		return applicantConsultDataModel;
	}

	public void setApplicantConsultDataModel(ApplicantConsultDataModel applicantConsultDataModel) {
		this.applicantConsultDataModel = applicantConsultDataModel;
	}

	public ApplicantConsultRequestDataModel getApplicantConsultRequestDataModel() {
		return applicantConsultRequestDataModel;
	}

	public void setApplicantConsultRequestDataModel(ApplicantConsultRequestDataModel applicantConsultRequestDataModel) {
		this.applicantConsultRequestDataModel = applicantConsultRequestDataModel;
	}

	public String getRespaldo_name() {
		return respaldo_name;
	}

	public void setRespaldo_name(String respaldo_name) {
		this.respaldo_name = respaldo_name;
	}

	public byte[] getRespaldo_temp() {
		return respaldo_temp;
	}

	public void setRespaldo_temp(byte[] respaldo_temp) {
		this.respaldo_temp = respaldo_temp;
	}

	public StreamedContent getRespaldo() {
		return respaldo;
	}

	public void setRespaldo(StreamedContent respaldo) {
		this.respaldo = respaldo;
	}

	public List<ParameterTable> getListDocumentTypes() {
		return listDocumentTypes;
	}

	public void setListDocumentTypes(List<ParameterTable> listDocumentTypes) {
		this.listDocumentTypes = listDocumentTypes;
	}

	public ApplicantSearchDataModel getApplicantSearchDataModel() {
		return applicantSearchDataModel;
	}

	public void setApplicantSearchDataModel(ApplicantSearchDataModel applicantSearchDataModel) {
		this.applicantSearchDataModel = applicantSearchDataModel;
	}

	public GenericDataModel<ApplicantConsult> getApplicantConsultRegister() {
		return applicantConsultRegister;
	}

	public void setApplicantConsultRegister(GenericDataModel<ApplicantConsult> applicantConsultRegister) {
		this.applicantConsultRegister = applicantConsultRegister;
	}

	public boolean isIndSearched() {
		return indSearched;
	}

	public void setIndSearched(boolean indSearched) {
		this.indSearched = indSearched;
	}

	public StreamedContent getExcel() {
		return excel;
	}

	public void setExcel(StreamedContent excel) {
		this.excel = excel;
	}

	public byte[] getExcel_temp() {
		return excel_temp;
	}

	public void setExcel_temp(byte[] excel_temp) {
		this.excel_temp = excel_temp;
	}

	public String getExcel_name() {
		return excel_name;
	}

	public void setExcel_name(String excel_name) {
		this.excel_name = excel_name;
	}

	public boolean isIndConfirmed() {
		return indConfirmed;
	}

	public void setIndConfirmed(boolean indConfirmed) {
		this.indConfirmed = indConfirmed;
	}

	public ApplicantConsult getApplicantConsultSelected() {
		return applicantConsultSelected;
	}

	public void setApplicantConsultSelected(ApplicantConsult applicantConsultSelected) {
		this.applicantConsultSelected = applicantConsultSelected;
	}

	public boolean isIndAggregate() {
		return indAggregate;
	}

	public void setIndAggregate(boolean indAggregate) {
		this.indAggregate = indAggregate;
	}

	public boolean isIndAppSabed() {
		return indAppSabed;
	}

	public void setIndAppSabed(boolean indAppSabed) {
		this.indAppSabed = indAppSabed;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public List<ApplicantConsult> getListApplicantConsultSelected() {
		return listApplicantConsultSelected;
	}

	public void setListApplicantConsultSelected(List<ApplicantConsult> listApplicantConsultSelected) {
		this.listApplicantConsultSelected = listApplicantConsultSelected;
	}

	public GenericDataModel<ApplicantConsultRequest> getApplicantConsultRequestForReports() {
		return applicantConsultRequestForReports;
	}

	public void setApplicantConsultRequestForReports(
			GenericDataModel<ApplicantConsultRequest> applicantConsultRequestForReports) {
		this.applicantConsultRequestForReports = applicantConsultRequestForReports;
	}

	public List<ApplicantConsultRequest> getListApplicantConsultRequestSelected() {
		return listApplicantConsultRequestSelected;
	}

	public void setListApplicantConsultRequestSelected(List<ApplicantConsultRequest> listApplicantConsultRequestSelected) {
		this.listApplicantConsultRequestSelected = listApplicantConsultRequestSelected;
	}

	public boolean isIndGenerable() {
		return indGenerable;
	}

	public void setIndGenerable(boolean indGenerable) {
		this.indGenerable = indGenerable;
	}
	
	public List<String> getLstErrorReadExcel() {
		return lstErrorReadExcel;
	}
	public void setLstErrorReadExcel(List<String> lstErrorReadExcel) {
		this.lstErrorReadExcel = lstErrorReadExcel;
	}
	
}
