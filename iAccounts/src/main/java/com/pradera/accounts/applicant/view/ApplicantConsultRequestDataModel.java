package com.pradera.accounts.applicant.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.accounts.ApplicantConsultRequest;

public class ApplicantConsultRequestDataModel extends ListDataModel<ApplicantConsultRequest> implements SelectableDataModel<ApplicantConsultRequest>, Serializable {
	private static final long serialVersionUID = 1L;
	
	public ApplicantConsultRequestDataModel() {
		
	}
	
	public ApplicantConsultRequestDataModel(List<ApplicantConsultRequest> data) {
		super(data);
	}
	
	@Override
	public ApplicantConsultRequest getRowData(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getRowKey(ApplicantConsultRequest arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
