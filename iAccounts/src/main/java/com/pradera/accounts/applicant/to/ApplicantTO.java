package com.pradera.accounts.applicant.to;

import java.io.Serializable;
import java.util.Date;

public class ApplicantTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Date initialDate;
	private Date endDate;
	private Integer applicatType;
	private String applicantNum;
	
	public Date getInitialDate() {
		return initialDate;
	}
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Integer getApplicatType() {
		return applicatType;
	}
	public void setApplicatType(Integer applicatType) {
		this.applicatType = applicatType;
	}
	public String getApplicantNum() {
		return applicantNum;
	}
	public void setApplicantNum(String applicantNum) {
		this.applicantNum = applicantNum;
	}
	
	
}
