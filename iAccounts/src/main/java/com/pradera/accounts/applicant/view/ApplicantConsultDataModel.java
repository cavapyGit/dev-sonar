package com.pradera.accounts.applicant.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.accounts.ApplicantConsult;
/**
* 
* <ul><li>Copyright EDV 2020.</li></ul> 
* @author RCHIARA.
* 
*
**/
public class ApplicantConsultDataModel extends ListDataModel<ApplicantConsult> implements SelectableDataModel<ApplicantConsult>, Serializable {
	private static final long serialVersionUID = 1L;
	
	public ApplicantConsultDataModel() {}
	
	public ApplicantConsultDataModel(List<ApplicantConsult> data) {
		super(data);
	}

	@Override
	public ApplicantConsult getRowData(String rowKey) {
		
		List<ApplicantConsult> applicantConsultList = (List<ApplicantConsult>)getWrappedData();
		
		for(ApplicantConsult applicantConsult : applicantConsultList) {
			
			if (String.valueOf(applicantConsult.getIdApplicantConsultPk()).equals(rowKey))
				return applicantConsult;
				
		}
		
		return null;
	}

	@Override
	public Object getRowKey(ApplicantConsult applicantConsult) {
		
		return applicantConsult.getIdApplicantConsultPk();
	
	}
}
