package com.pradera.accounts.applicant.view;

import java.io.Serializable;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;
import com.pradera.model.accounts.Applicant;

public class ApplicantSearchDataModel extends ListDataModel<Applicant> implements SelectableDataModel<Applicant>, Serializable {
	private static final long serialVersionUID = 1L;
	
	public ApplicantSearchDataModel() {
		
	}
	
	public ApplicantSearchDataModel(List<Applicant> data) {
		super(data);
	}

	@Override
	public Applicant getRowData(String rowKey) {
		
		List<Applicant> applicantConsultRequestList = (List<Applicant>)getWrappedData();
		
		for (Applicant appConsult : applicantConsultRequestList) {
			
			if(String.valueOf(appConsult.getApplicantNum()).equals(rowKey)) return appConsult; 
			
		}
		
		return null;
		
	}

	@Override
	public Object getRowKey(Applicant arg0) {
		// TODO Auto-generated method stub
		return null;
	}

}
