package com.pradera.accounts.applicant.facade;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.apache.log4j.Logger;

import com.pradera.accounts.applicant.service.ApplicantServiceBean;
import com.pradera.accounts.applicant.to.ApplicantTO;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.services.remote.reports.ReportGenerationService;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Applicant;
import com.pradera.model.accounts.ApplicantConsult;
import com.pradera.model.accounts.ApplicantConsultRequest;
import com.pradera.model.accounts.AppRequester;
import com.pradera.model.accounts.Holder;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.report.ReportLoggerFile;
/**
* 
* <ul><li>Copyright EDV 2020.</li></ul> 
* @author RCHIARA.
* 
*
*/
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ApplicantServiceFacade extends CrudDaoServiceBean {
	
	Logger log = Logger.getLogger(ApplicantServiceFacade.class);
	
	@EJB 
	ApplicantServiceBean applicantServiceBean;
	
	@Resource 
	TransactionSynchronizationRegistry transactionRegistry;	
	
	@Inject 
	Instance<ReportGenerationService> reportService;
	
	@EJB
	CrudDaoServiceBean crudDaoServiceBean;
	
	/** REPORTE DE TENENCIAS */
	private final Long REPORT_ID = new Long(329);
	
	/**NOMBRE REPORTE*/
	public ReportLoggerFile findReport(Long holderId, String nameReport) {
		ReportLoggerFile reportLoggerFile = new ReportLoggerFile();
		try {
			reportLoggerFile = applicantServiceBean.findReport(holderId, nameReport);
		} catch (NullPointerException e) {
			reportLoggerFile = null;
		}
		return reportLoggerFile;
	}
	
	/**ARCHIVO REPORTE*/
	public String findFileReportPath (String nameReport) {
		return applicantServiceBean.findFileReportPath(nameReport);
	}
	
	/**GENERA REPORTE*/
	@LoggerAuditWeb
	public void generateReportFile(Long id_holder_pk, Long idApplicantConsultRequest, String holder_desc) throws ServiceException {		
		LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		log.info("loggerUser: " + loggerUser);
		
		if(loggerUser.getUserAction() != null)
			log.info("loggerUser.getUserAction(): " + loggerUser.getUserAction());
		else
			log.info("loggerUser.getUserAction(): NULL");
		
		if(loggerUser.getUserAction().getIdPrivilegeExecute() != null)
			log.info("loggerUser.getUserAction().getIdPrivilegeExecute(): " + loggerUser.getUserAction().getIdPrivilegeExecute());
		else 
			log.info("loggerUser.getUserAction().getIdPrivilegeExecute(): NULL");
		
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
	    this.reportService.get().sendApplicantConsult(REPORT_ID, id_holder_pk, idApplicantConsultRequest, holder_desc, loggerUser.getUserName(), loggerUser);
		
	}
	
	/**LISTA SOLICITANTES*/
	public List<ParameterTable> getListApplicants() throws ServiceException {		
		return applicantServiceBean.getListApplicants();
	}
	
	/**TIPOS DE DOCUMENTO*/
	public List<ParameterTable> getDocumentTypes () throws ServiceException {
		return applicantServiceBean.getDocumentTypes();
	}
	
	/**BUSQUEDA PERSONAS*/
	public List<Holder> getListHolderServiceFacade(ApplicantConsult applicantConsult) throws ServiceException {		
		return applicantServiceBean.getListHolderServiceBean(applicantConsult);
	}
	
	/**REGISTRA SOLICITANTE*/
	public Applicant registerApplicant(Applicant applicant) throws ServiceException {		
	    return create(applicant);
	}
	
	/**REGISTRA SOLICITUD*/
	public ApplicantConsult registerApplicantConsult(ApplicantConsult applicantCon) throws ServiceException {		
		return create(applicantCon);
	}
	
	/**REGISTRA RESULTADO*/
	public ApplicantConsultRequest registerApplicantConsultRequest(ApplicantConsultRequest applicantConReq) throws ServiceException {		
		return create(applicantConReq);
	}
	
	/**BUSCADOR OPE MGMT*/
	public List<Applicant> searchApplicantsConsultMgmt (ApplicantTO appTO) {		
		return applicantServiceBean.searchApplicantsConsultMgmt(appTO.getInitialDate(), appTO.getEndDate(), appTO.getApplicantNum());
	}
	
	public void updateAdditionalRequestData (Applicant applicant, Integer quantity, Integer observed, Integer verified) throws ServiceException {		
		applicantServiceBean.updateAdditionalRequestData (applicant,quantity,observed,verified);		
	}
	
	public void updateAdditionalRequestData (Applicant app) throws ServiceException {
		update(app);
	}
	
	public ParameterTable getParameterName (Integer parameterTablePk) throws ServiceException {		
		return find(ParameterTable.class, parameterTablePk);
	}
	
	public Integer verifyHoldings (Long holderId) throws ServiceException {		
		return applicantServiceBean.verifyHoldings(holderId);
	}
	
	public List<ApplicantConsult> findApplicantConsultSearch (Applicant a) throws ServiceException {		
		return applicantServiceBean.findApplicantConsultSearch(a);
	}
	
	public List<ApplicantConsultRequest> findApplicantConsultRequestSearch (ApplicantConsult ac) throws ServiceException {		
		return applicantServiceBean.findApplicantConsultRequestSearch(ac);
	}
	
	public Long getIdHolder (ApplicantConsultRequest acr) throws ServiceException {		
		return applicantServiceBean.getIdHolder(acr);
	}
	
	public Holder findHolderById(Long idHolder) throws ServiceException {		
		return find(Holder.class, idHolder);		
	}
	
	public void dropCurrentApplicantConsult (ApplicantConsult appConsultDel) throws ServiceException {		
		applicantServiceBean.dropCurrentApplicantConsult(appConsultDel);
	}
	
	public void dropCurrentRequester (AppRequester requester) {
		applicantServiceBean.dropCurrentRequester(requester);
	}
		
	public AppRequester saveNewRequester (AppRequester requester) {
		return create(requester);
	}
	
	public AppRequester getRequesterById(Long requesterId){
		return find(AppRequester.class,requesterId);
	}
	
	public List<AppRequester> getListRequesteres () throws ServiceException {
		return applicantServiceBean.getListRequesteres();
	}
	
	public List<ParameterTable> getListDescriptionDocSource (Integer documentSource) throws ServiceException {
		return applicantServiceBean.getListDescriptionDocSource(documentSource);
	}
 }
