package com.pradera.accounts.remote;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.accounts.holder.service.HolderAdditionalServiceBean;
import com.pradera.accounts.holder.service.HolderServiceBean;
import com.pradera.accounts.holderaccount.service.HolderAccountServiceBean;
import com.pradera.commons.contextholder.interceptor.AsynchronousLoggerInterceptor;
import com.pradera.commons.contextholder.interceptor.RemoteLoggerUserInterceptor;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GenericPropertiesConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.component.accounts.service.AccountsRemoteService;
import com.pradera.integration.component.accounts.to.CuiQueryResponseTO;
import com.pradera.integration.component.accounts.to.CuiQueryTO;
import com.pradera.integration.component.accounts.to.HolderAccountObjectTO;
import com.pradera.integration.component.accounts.to.LegalRepresentativeObjectTO;
import com.pradera.integration.component.accounts.to.NaturalHolderObjectTO;
import com.pradera.integration.component.accounts.to.PersonObjectTO;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.custody.to.ReverseRegisterTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountType;
import com.pradera.model.accounts.type.PersonType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountsRemoteServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/08/2015
 */
@Stateless
public class AccountsRemoteServiceBean extends CrudDaoServiceBean implements AccountsRemoteService{

	/** The holder additional service bean. */
	@EJB
	HolderAdditionalServiceBean holderAdditionalServiceBean;
	
	/** The holder account service bean. */
	@EJB
	HolderAccountServiceBean holderAccountServiceBean;
	
	/** The parameter service bean. */
	@EJB
	ParameterServiceBean parameterServiceBean;
	
	/** The holder service bean. */
	@EJB
	HolderServiceBean holderServiceBean;
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	/* (non-Javadoc)
	 * @see com.pradera.integration.component.accounts.service.AccountsRemoteService#createHolderRemoteService(com.pradera.integration.component.accounts.to.HolderAccountObjectTO)
	 */
	@Override	
	@Interceptors(AsynchronousLoggerInterceptor.class)
	public Object createHolderRemoteService(LoggerUser loggerUser, HolderAccountObjectTO holderAccountObjectTO) throws ServiceException {			
		RecordValidationType recordValidationType = null;		
		Long idHolderAccount = null;
		//check information for dpf interface for holder account
		if(holderAccountObjectTO.isDpfInterface()){
			if(holderAccountObjectTO.getAccountTypeCode().equals(HolderAccountObjectTO.INDIVIDUAL_ACCOUNT_TYPE)){
				if(holderAccountObjectTO.getHolderTypeAlt().equals(PersonType.NATURAL.getInterfaceCode())){
					holderAccountObjectTO.setAccountType(HolderAccountType.NATURAL.getCode());
				}else if (holderAccountObjectTO.getHolderTypeAlt().equals(PersonType.JURIDIC.getInterfaceCode())){
					holderAccountObjectTO.setAccountType(HolderAccountType.JURIDIC.getCode());
					holderAccountObjectTO.setLstLegalRepresentativeObjectTOs(new ArrayList<LegalRepresentativeObjectTO>());
					for(NaturalHolderObjectTO naturalHolderObjectTO : holderAccountObjectTO.getLstNaturalHolderObjectTOs()){
						LegalRepresentativeObjectTO legalRepresentativeObjectTO = new LegalRepresentativeObjectTO((PersonObjectTO)naturalHolderObjectTO);
						holderAccountObjectTO.getLstLegalRepresentativeObjectTOs().add(legalRepresentativeObjectTO);
					}
					holderAccountObjectTO.setLstNaturalHolderObjectTOs(null);
				}
			}else if(holderAccountObjectTO.getAccountTypeCode().equals(HolderAccountObjectTO.OWNERSHIP_ACCOUNT_TYPE)){
				if(holderAccountObjectTO.getHolderTypeAlt().equals(PersonType.NATURAL.getInterfaceCode())){
					holderAccountObjectTO.setAccountType(HolderAccountType.OWNERSHIP.getCode());
				}
			}
			
			if(holderAccountObjectTO.getAccountType() == null){
				return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_ACCOUNT_TYPE,null); 
			}
		}			
		
		holderAccountObjectTO.setChildrenIndicators();
		
		// CAMBIAR LLAMADA AL METODO REMOTO DE CREACION DE CUENTAS
		
		Object objResponse = holderAdditionalServiceBean.createHolderAndAccount(holderAccountObjectTO);			
		if(objResponse instanceof RecordValidationType){
			RecordValidationType validation = (RecordValidationType) objResponse;
			recordValidationType = validation;
		} else if (objResponse instanceof Long){
			idHolderAccount = (Long) objResponse;
			holderAccountObjectTO.setIdHolderAccount(idHolderAccount);
		} else {
			recordValidationType = null;
		}
		
		if(recordValidationType != null){
			return recordValidationType;
		} else {
			return holderAccountObjectTO;
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.component.accounts.service.AccountsRemoteService#deleteHolderRemoteService(com.pradera.integration.component.custody.to.ReverseRegisterTO)
	 */
	@Override
	public List<RecordValidationType> deleteHolderRemoteService(ReverseRegisterTO reverseRegisterTO) throws ServiceException {
		
		return null;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.integration.component.accounts.service.AccountsRemoteService#createHolderAccountRemoteService(com.pradera.integration.contextholder.LoggerUser, com.pradera.integration.component.accounts.to.HolderAccountObjectTO)
	 */
	@Override	
	@Interceptors(AsynchronousLoggerInterceptor.class)
	public Object createHolderAccountRemoteService(LoggerUser loggerUser, HolderAccountObjectTO holderAccountObjectTO) throws ServiceException {
		RecordValidationType recordValidationType = null;
		Long idHolderAccount = null;		
		Object objResponse = holderAdditionalServiceBean.createAndValidateHolderAndAccount(holderAccountObjectTO);			
		if(objResponse instanceof RecordValidationType){
			RecordValidationType validation = (RecordValidationType) objResponse;
			recordValidationType = validation;
		} else if (objResponse instanceof Long){
			idHolderAccount = (Long) objResponse;
			holderAccountObjectTO.setIdHolderAccount(idHolderAccount);
		}		
		if(recordValidationType != null){
			return recordValidationType;
		} else {
			return holderAccountObjectTO;
		}		
	}
	
	/**
	 * 
	 * @param cuiQueryObjectTO
	 * @return
	 * @throws ServiceException
	 */
	@Override
	@Interceptors(RemoteLoggerUserInterceptor.class)
	public List<RecordValidationType> getListCuiQuery(CuiQueryTO cuiQueryTO) throws ServiceException {	
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER.name());
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        Integer transactionState= BooleanType.NO.getCode();
        cuiQueryTO.setOperationNumber(cuiQueryTO.getQueryNumber());
        CuiQueryResponseTO cuiQueryResponseTO = null;
        
        List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
        try {
        	if (Validations.validateListIsNullOrEmpty(lstRecordValidationTypes)) {
        		
        		//ejecutamos el query con los parametros enviados
        		cuiQueryResponseTO = holderServiceBean.consultCui(cuiQueryTO);
                
                // Este es el objeto con todos los datos de la consulta
        		cuiQueryResponseTO.setQueryNumber(Integer.valueOf(cuiQueryTO.getQueryNumber().toString()));
        		cuiQueryResponseTO.setOperationType(cuiQueryTO.getOperationType());
        		cuiQueryResponseTO.setOperationDate(cuiQueryTO.getOperationDate());
        		
        		RecordValidationType objRecordValidationType = CommonsUtilities.populateRecordCodeValidation(cuiQueryResponseTO,GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
				lstRecordValidationTypes.add(objRecordValidationType);
				transactionState= BooleanType.YES.getCode();
        	}
        	
        } catch (Exception e) {
        	e.printStackTrace();
        	throw e;
        }
		return lstRecordValidationTypes;				
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.integration.component.accounts.service.AccountsRemoteService#createHolderNewRemoteService(com.pradera.integration.component.accounts.to.HolderAccountObjectTO)
	 */
	@Override	
	@Interceptors(AsynchronousLoggerInterceptor.class)
	public Object createHolderNewRemoteService(LoggerUser loggerUser, HolderAccountObjectTO holderAccountObjectTO) throws ServiceException {			
		RecordValidationType recordValidationType = null;		
		Long idHolderAccount = null;
			if(!holderAccountObjectTO.isDpfInterface()){
					if (holderAccountObjectTO.getAccountType().equals(HolderAccountType.JURIDIC.getCode())){
					holderAccountObjectTO.setLstLegalRepresentativeObjectTOs(new ArrayList<LegalRepresentativeObjectTO>());
					for(NaturalHolderObjectTO naturalHolderObjectTO : holderAccountObjectTO.getLstNaturalHolderObjectTOs()){
						LegalRepresentativeObjectTO legalRepresentativeObjectTO = new LegalRepresentativeObjectTO((PersonObjectTO)naturalHolderObjectTO);
						holderAccountObjectTO.getLstLegalRepresentativeObjectTOs().add(legalRepresentativeObjectTO);
					}
					holderAccountObjectTO.setLstNaturalHolderObjectTOs(null);
					//Verificando que al menos exista un representante legal
					if(holderAccountObjectTO.getLstLegalRepresentativeObjectTOs().size()<1){
						return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSUFFICIENT_LEGAL_REPRESANTIVE,null);		
					}
				}			
			if(holderAccountObjectTO.getAccountType() == null){
				return CommonsUtilities.populateRecordCodeValidation(null,GenericPropertiesConstants.MSG_INTERFACE_INVALID_ACCOUNT_TYPE,null); 
			}
		}			
		
		holderAccountObjectTO.setChildrenIndicators();
		
		// CAMBIAR LLAMADA AL METODO REMOTO DE CREACION DE CUENTAS
		
		Object objResponse = holderAdditionalServiceBean.createHolderAndAccountNew(holderAccountObjectTO);			
		if(objResponse instanceof RecordValidationType){
			RecordValidationType validation = (RecordValidationType) objResponse;
			recordValidationType = validation;
		} else if (objResponse instanceof Long){
			idHolderAccount = (Long) objResponse;
			holderAccountObjectTO.setIdHolderAccount(idHolderAccount);
		} else {
			recordValidationType = null;
		}
		
		if(recordValidationType != null){
			return recordValidationType;
		} else {
			return holderAccountObjectTO;
		}
	}
	
	
}
