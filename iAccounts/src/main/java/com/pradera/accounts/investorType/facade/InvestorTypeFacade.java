package com.pradera.accounts.investorType.facade;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.accounts.investorType.to.InvestorTypeModelTO;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.MasterTable;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.ParameterTableStateType;

@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class InvestorTypeFacade  {
	
	@EJB
	InvestorTypeServiceBean investorTypeServiceBean;
	
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	public List<ParameterTable> getLstEconomicActivityToRelationInvestortype() throws ServiceException{
		return investorTypeServiceBean.getLstEconomicActivityToRelationInvestortype();
	}
	
	public void updateRelationEconomicActWithInvestorType(ParameterTable pt) throws ServiceException{
		ParameterTable ptUpdate = investorTypeServiceBean.find(ParameterTable.class, pt.getParameterTablePk());
		ptUpdate.setShortInteger(pt.getShortInteger());
		investorTypeServiceBean.update(pt);
	}
	
	/**
	 * verifica si el nombre de tipo de inversionista ya existe
	 * @param investorType
	 * @return
	 */
	public boolean verifyInvestorTypeIsDuplicated(InvestorTypeModelTO investorType) {
		InvestorTypeModelTO filter = new InvestorTypeModelTO();
		filter.setShortName(investorType.getShortName());
		filter.setEconomicActivity(investorType.getEconomicActivity());
		
		List<InvestorTypeModelTO> lst=null;
		try {
			lst = investorTypeServiceBean.getLstInvestorTypeModelTO(filter);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		if (Validations.validateListIsNullOrEmpty(lst)) {
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * listado de tipo de inversionista
	 * @param filter
	 * @return
	 * @throws ServiceException
	 */
	public List<InvestorTypeModelTO> getLstInvestorTypeModelTO(InvestorTypeModelTO filter) throws ServiceException {
		List<InvestorTypeModelTO> lst = investorTypeServiceBean.getLstInvestorTypeModelTO(filter);
		
		// seteando su estado en formato texto
		for(InvestorTypeModelTO it:lst){
			if(Validations.validateIsNotNull(it.getState())){
				it.setStateText(ParameterTableStateType.get(it.getState()).getValue());
			}
			it.setShortNameAux(it.getShortName());
		}
		
		return lst;
	}
	
	public ParameterTable modifyInvestorType(InvestorTypeModelTO modelTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		try {
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());	
		} catch (Exception e) {
			System.out.println("::: no se pudo obtener el privilegio en el metodo InvestorTypeFacade.modifyInvestorType. Se asignarA el privilegio manualemnte");
		}
		
		ParameterTable investorType = investorTypeServiceBean.find(ParameterTable.class, modelTO.getId());
		investorType.setDescription(modelTO.getDescription());
		investorType.setParameterName(modelTO.getShortName());
		
		// pistas de auditoria
		//investorType.setAudit(loggerUser);
		investorType.setLastModifyUser(modelTO.getLastModifyUser());
		investorType.setLastModifyDate(new Date());
		investorType.setLastModifyIp(modelTO.getLastModifyIp());
		investorType.setLastModifyApp(Integer.parseInt(modelTO.getLastModifyApp().toString()));
		
		return investorTypeServiceBean.modifyInvestorType(investorType);
	}

	public ParameterTable regiterInvestorType(InvestorTypeModelTO modelTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		try {
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());	
		} catch (Exception e) {
			System.out.println("::: no se pudo obtener el privilegio en el metodo InvestorTypeFacade.regiterInvestorType. Se asignarA el privilegio manualemnte");
		}
		
		ParameterTable investorType = new ParameterTable();
		investorType.setDescription(modelTO.getDescription());
		investorType.setParameterName(modelTO.getShortName());
		
		//investorType.setMasterTable(new MasterTable(modelTO.getIdMasterTable()));
		MasterTable mt = investorTypeServiceBean.find(MasterTable.class, modelTO.getIdMasterTable());
		investorType.setMasterTable(mt);
		
		// pistas de auditoria
		//investorType.setAudit(loggerUser);
		investorType.setLastModifyUser(modelTO.getLastModifyUser());
		investorType.setLastModifyDate(new Date());
		investorType.setLastModifyIp(modelTO.getLastModifyIp());
		investorType.setLastModifyApp(Integer.parseInt(modelTO.getLastModifyApp().toString()));
		
		investorType.setParameterState(ParameterTableStateType.REGISTERED.getCode());
		
		// si esto funcionara, entonces se tiene que hallar manualmente el correlativo dentro del rango master table
		investorType.setParameterTablePk(investorTypeServiceBean.getNextParameterTableByMasterTable1219());
		
		return investorTypeServiceBean.registerInvestorType(investorType);
	}
}
