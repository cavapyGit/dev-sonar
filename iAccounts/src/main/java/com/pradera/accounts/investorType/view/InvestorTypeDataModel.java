package com.pradera.accounts.investorType.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.accounts.investorType.to.InvestorTypeModelTO;

public class InvestorTypeDataModel extends ListDataModel<InvestorTypeModelTO> implements SelectableDataModel<InvestorTypeModelTO>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// constructor 1
	public InvestorTypeDataModel(){}
	
	// constructor 2
	public InvestorTypeDataModel(List<InvestorTypeModelTO> lstInvestorType){
		super(lstInvestorType);
	}

	@Override
	public InvestorTypeModelTO getRowData(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getRowKey(InvestorTypeModelTO arg0) {
		// TODO Auto-generated method stub
		return null;
	}

}
