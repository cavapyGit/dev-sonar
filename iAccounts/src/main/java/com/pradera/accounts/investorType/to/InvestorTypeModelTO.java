package com.pradera.accounts.investorType.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.pradera.model.generalparameter.ParameterTable;


public class InvestorTypeModelTO implements Serializable, Cloneable {
	
	private static final long serialVersionUID = 1L;
	
    private Integer id;
    
    private String description;
    private String shortName;
    private String shortNameAux;
    
    private Integer state;
    private String stateText;
    
    // estos campos servirAn para relacionar la 'actividad enconOmica' con los 'Tipos de Inversionista'
    private Integer economicActivity;
    private String economicActivityText;
    
    private List<ParameterTable> lstState=new ArrayList<>();
    private List<ParameterTable> lstEconomicActivity = new ArrayList<>();
    
    private List<ParameterTable> lstInvestorTypeToRelation = new ArrayList<>();
    
    private Integer idMasterTable;
    
    // pistas de auditoria
	private Long lastModifyApp;
	private Date lastModifyDate;
	private String lastModifyIp;
	private String lastModifyUser;
    
	// constructor 1
	public InvestorTypeModelTO(){}
	
    // constructor 2 para la lista jpql del metodo
	public InvestorTypeModelTO(
				Integer id, 
				String shortName,
				String description, 
				Integer state) {
		this.id = id;
		this.shortName = shortName;
		this.description = description;
		this.state = state;
	}
	
	
	
	
    /**
     * para la clonacion de objetos, asi evitamos la referencia
     */
    @Override
	public Object clone() {
		Object obj = null;
		try {
			obj = super.clone();
		} catch (CloneNotSupportedException ex) {
			System.out.println("No se puede clonar Objeto");
		}
		return obj;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InvestorTypeModelTO other = (InvestorTypeModelTO) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (economicActivity == null) {
			if (other.economicActivity != null)
				return false;
		} else if (!economicActivity.equals(other.economicActivity))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (idMasterTable == null) {
			if (other.idMasterTable != null)
				return false;
		} else if (!idMasterTable.equals(other.idMasterTable))
			return false;
		if (shortName == null) {
			if (other.shortName != null)
				return false;
		} else if (!shortName.equals(other.shortName))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime
				* result
				+ ((economicActivity == null) ? 0 : economicActivity.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((idMasterTable == null) ? 0 : idMasterTable.hashCode());
		result = prime * result
				+ ((shortName == null) ? 0 : shortName.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		return result;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getShortNameAux() {
		return shortNameAux;
	}

	public void setShortNameAux(String shortNameAux) {
		this.shortNameAux = shortNameAux;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getStateText() {
		return stateText;
	}

	public void setStateText(String stateText) {
		this.stateText = stateText;
	}

	public Integer getEconomicActivity() {
		return economicActivity;
	}

	public void setEconomicActivity(Integer economicActivity) {
		this.economicActivity = economicActivity;
	}

	public String getEconomicActivityText() {
		return economicActivityText;
	}

	public void setEconomicActivityText(String economicActivityText) {
		this.economicActivityText = economicActivityText;
	}

	public List<ParameterTable> getLstState() {
		return lstState;
	}

	public void setLstState(List<ParameterTable> lstState) {
		this.lstState = lstState;
	}

	public List<ParameterTable> getLstEconomicActivity() {
		return lstEconomicActivity;
	}

	public void setLstEconomicActivity(List<ParameterTable> lstEconomicActivity) {
		this.lstEconomicActivity = lstEconomicActivity;
	}

	public List<ParameterTable> getLstInvestorTypeToRelation() {
		return lstInvestorTypeToRelation;
	}

	public void setLstInvestorTypeToRelation(List<ParameterTable> lstInvestorTypeToRelation) {
		this.lstInvestorTypeToRelation = lstInvestorTypeToRelation;
	}

	public Integer getIdMasterTable() {
		return idMasterTable;
	}

	public void setIdMasterTable(Integer idMasterTable) {
		this.idMasterTable = idMasterTable;
	}

	public Long getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Long lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
}
