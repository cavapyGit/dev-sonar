package com.pradera.accounts.investorType.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.event.Event;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.RowEditEvent;

import com.pradera.accounts.investorType.facade.InvestorTypeFacade;
import com.pradera.accounts.investorType.to.InvestorTypeModelTO;
import com.pradera.accounts.util.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;

@LoggerCreateBean
@DepositaryWebBean
public class InvestorTypeMgmtBean extends GenericBaseBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String PAGE_REGISTER="newInvestorType";
	public static final String PAGE_SEARCH="searchInvestorType";
	public static final String PAGE_VIEW="viewInvestorType";
	public static final String PAGE_RELATION="relationInvestorType";
	
	@Inject
	private UserInfo userInfo;

	@Inject
	private UserPrivilege userPrivilege;

	@Inject
	private transient PraderaLogger log;

	@EJB
	private AccountsFacade accountsFacade;

	@EJB
	private GeneralParametersFacade generalParametersFacade;

	@Inject
	protected Event<ExceptionToCatchEvent> excepcion;

	@EJB
	private InvestorTypeFacade ejbInvestorType;
	
	private InvestorTypeModelTO modelTOReg;
	private InvestorTypeModelTO modelTOSearch;
	private InvestorTypeModelTO modelTOView;
	private InvestorTypeModelTO modelTOModify;
	
	private InvestorTypeDataModel searchInvestorTypeDataModel = new InvestorTypeDataModel();
	private List<InvestorTypeModelTO> lstInvestorTypeModelTO;
	
	private boolean privilegeRegister;
	private boolean privilegeSearch;
	private boolean privilegeModify;
	
	// para renderizar el boton dinamicamnete en el PAGE_VIEW
	private boolean operationModify;
	
	@PostConstruct
	public void init(){
		loadSearch();
	}
	
	/** redirige a la pagina de registro
	 * 
	 * @return */
	public String goPageRegister() {
		loadRegiter();
		//return PAGE_REGISTER;
		return null;
	}
	
	/** redirige a la pagina de relacion Actividad EconOmica - Tipo de Inversionista
	 * 
	 * @return */
	public String goPageRelation() {
		loadRelation();
		return PAGE_RELATION;
	}
	
	/** redireige a la pnatalla de administracion
	 * 
	 * @return */
	public String goPageSearch() {
		loadSearch();
		return PAGE_SEARCH;
	}
	
	/** cargando la pantalla de registro */
	public void loadRegiter() {
		modelTOReg = new InvestorTypeModelTO();
		modelTOReg.setLstEconomicActivity(getLstEconomicActivity());
		modelTOReg.setIdMasterTable(MasterTableType.INVESTOR_TYPE_ECONOMIC_ACTIVITY.getCode());
	}
	
	public void loadRelation() {
		modelTOReg = new InvestorTypeModelTO();
		modelTOReg.setLstEconomicActivity(getLstEconomicActivityToRelationInvestorType());
		modelTOReg.setLstInvestorTypeToRelation(getLstInvestioTypeToRelation());
		modelTOReg.setIdMasterTable(MasterTableType.INVESTOR_TYPE_ECONOMIC_ACTIVITY.getCode());
	}
	
	public void onRowEdit(RowEditEvent event) {
//        FacesMessage msg = new FacesMessage("Car Edited", event.getObject().getId());
//        FacesContext.getCurrentInstance().addMessage(null, msg);
		System.out.println("::: datos del evento "+event);
    	System.out.println("::: "+event.getObject());
    	
    	ParameterTable pt = (ParameterTable) event.getObject();
    	try {
			ejbInvestorType.updateRelationEconomicActWithInvestorType(pt);
			log.info("::: Mostrar lista actualizada");
		} catch (ServiceException e) {
			e.printStackTrace();
			log.info("::: Error al relacionar Act EconOmica con Tipo de Inversionista "+e.getMessage());
		}
    	loadRelation();
    }
     
    public void onRowCancel(RowEditEvent event) {
    	log.info("::: datos del evento "+event);
    	log.info("::: "+event.getObject());
        //FacesMessage msg = new FacesMessage("Edit Cancelled", event.getObject().getId());
        //FacesContext.getCurrentInstance().addMessage(null, msg);
    	loadRelation();
    }
	
	public String getInvestorTypeRelated(Integer idInvestiorTypePk){
		if(Validations.validateIsNotNullAndPositive(idInvestiorTypePk)){
			for(ParameterTable pt:modelTOReg.getLstInvestorTypeToRelation()){
				if(Validations.validateIsNotNullAndPositive(pt.getParameterTablePk()) && pt.getParameterTablePk().equals(idInvestiorTypePk)){
					return pt.getParameterName();
				}
			}
		}
		return "NO RELACIONADO";
	}
	
	/** limpiando la pantalla de registro */
	public void btnClearRegister() {
		loadRegiter();
		JSFUtilities.resetViewRoot();
	}
	
	/** antes de guardar, se valida datos y se hace la pregunta de confirmaciOn */
	public void beforeSaveRegister() {
		// validar si el nombre corto y actividad ya existe
		if(ejbInvestorType.verifyInvestorTypeIsDuplicated(modelTOReg)){
			Object[] arrBodyData = { modelTOReg.getShortName() };
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MSG_VALIDATED_INVESTORTYPE_DUPLICATED, arrBodyData);
			JSFUtilities.showComponent("alertValidationMgmtBean");
			return;
		}
		
		JSFUtilities.hideComponent("alertValidationMgmtBean");
		// enviando el cui al mensaje de confirmacion de registro
		Object[] arrBodyData = { modelTOReg.getShortName() };
		showMessageOnDialog(PropertiesConstants.MESSAGES_CONFIRM, null, PropertiesConstants.LBL_MSG_CONFIRM_REGITER_INVESTORTYPE, arrBodyData);
		JSFUtilities.showComponent("cnfRegisterMgmtBean");
	}
	
	/** guardando en BBDD */
	public void saveRegister() {

		try {
			hideDialogsListener(null);

			// opteniendo las pistas de auditoria manualmente por si el processLogger esta en null en el facade
			modelTOReg.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
			modelTOReg.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
			modelTOReg.setLastModifyApp(userPrivilege.getUserAcctions().getIdPrivilegeRegister().longValue());

			ParameterTable investorType = ejbInvestorType.regiterInvestorType(modelTOReg);

			// notificando al usuario correspondiente
//			String descriptionParticipat="";
//			for(Participant p:modelTOReg.getLstParticipant()){
//				if(p.getIdParticipantPk().equals(modelTOReg.getIdParticipantSelected())){
//					descriptionParticipat = p.getDescription();
//					break;
//				}
//			}
//			Object[] parameters = { holderAnnulation.getIdAnnulationHolderPk().toString(), descriptionParticipat};
//			BusinessProcess businessProcessNotification = new BusinessProcess();
//			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_ANNULATION_CUI_REGISTER.getCode());
//			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
//			businessProcessNotification,
//			modelTOReg.getIdParticipantSelected(), parameters);
						
			btnSearch();

			// mensaje de exito
			Object[] arrBodyData = { String.valueOf(investorType.getParameterName()) };
			showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, PropertiesConstants.MSG_SUCCESFUL_REGISTER_INVESTORTYPE, arrBodyData);
			JSFUtilities.showComponent("cnfEndTrasnactionMgmtBean");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/** metodo para cargar la pantalla de busqueda(administracion) */
	public void loadSearch() {

		modelTOSearch = new InvestorTypeModelTO();
		if (userPrivilege.getUserAcctions().isRegister()) {
			log.info("::: tiene privilegios de registro");
			privilegeRegister = true;
		}
		if (userPrivilege.getUserAcctions().isSearch()) {
			log.info("::: tiene privilegios de busqueda");
			privilegeSearch = true;
		}

		lstInvestorTypeModelTO = new ArrayList<>();
		searchInvestorTypeDataModel = new InvestorTypeDataModel();
		btnSearch();
	}

	/** realizando la busqueda segun los criterios seleccionados */
	public void btnSearch() {
		try {
			lstInvestorTypeModelTO = ejbInvestorType.getLstInvestorTypeModelTO(modelTOSearch);
			searchInvestorTypeDataModel = new InvestorTypeDataModel(lstInvestorTypeModelTO);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/** lleva al detalle (pantalla de vista) un registro mediante su link
	 * 
	 * @param channelOp
	 * @return */
	public String linkView(InvestorTypeModelTO investorTypeTO) {
		modelTOView = (InvestorTypeModelTO) investorTypeTO.clone();
		modelTOView.setLstEconomicActivity(getLstEconomicActivity());
		renderFalseAllOperation();
		return PAGE_VIEW;
	}
	
	/** regresando a la pantalla de search desde la pantalla view
	 * 
	 * @return */
	public String backToSearchFromView() {
		btnSearch();
		return PAGE_SEARCH;
	}
	
	/** limpiando la pantalla de search */
	public void btnClearSearch() {
		loadSearch();
		JSFUtilities.resetViewRoot();
	}
	public void hideDialogsListener(ActionEvent actionEvent) {
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		JSFUtilities.hideGeneralDialogues();

		JSFUtilities.hideComponent("cnfRegisterMgmtBean");
		JSFUtilities.hideComponent("alertValidationMgmtBean");
		// JSFUtilities.hideComponent("detailRepresentativeDialog");

	}
	
	public void onRowEditInvestorType(RowEditEvent event) {
		System.out.println("::: datos del evento " + event);
		System.out.println("::: " + event.getObject());

		modelTOModify = new InvestorTypeModelTO();
		modelTOModify = (InvestorTypeModelTO) event.getObject();
		
		// validando si se ingreso un duplicado
		if(!modelTOModify.getShortName().equals(modelTOModify.getShortNameAux())){
			if(ejbInvestorType.verifyInvestorTypeIsDuplicated(modelTOModify)){
				Object[] arrBodyData = { modelTOModify.getShortName() };
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MSG_VALIDATED_INVESTORTYPE_DUPLICATED, arrBodyData);
				JSFUtilities.showComponent("alertValidationMgmtBean");
				
				// recuperar nombre
				modelTOModify.setShortName(modelTOModify.getShortNameAux());
				return;
			}
		}
		
		// si pasa la validacion procedemos a realizar el cambio
		try {
			hideDialogsListener(null);
			modelTOModify.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
			modelTOModify.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
			modelTOModify.setLastModifyApp(userPrivilege.getUserAcctions().getIdPrivilegeModify().longValue());

			ParameterTable investorType = ejbInvestorType.modifyInvestorType(modelTOModify);
			
			// mensaje de exito
			Object[] arrBodyData = { String.valueOf(investorType.getParameterName()) };
			showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, PropertiesConstants.MSG_SUCCESFUL_MODIFY_INVESTORTYPE, arrBodyData);
			JSFUtilities.showComponent("cnfEndTrasnactionMgmtBean");
		} catch (Exception e) {
			log.info("Se ha producido un error en la modificacion de Tipo de Inversionista "+e.getMessage());
			
		}
		
		btnSearch();
	}
   
	public void onRowCancelInvestorType(RowEditEvent event) {
		log.info("::: datos del evento " + event);
		log.info("::: " + event.getObject());
		// FacesMessage msg = new FacesMessage("Edit Cancelled", event.getObject().getId());
		// FacesContext.getCurrentInstance().addMessage(null, msg);
		btnSearch();
	}
	
	public String loadModify(){
		if( Validations.validateIsNotNull(modelTOView) 
			&& Validations.validateIsNotNullAndPositive(modelTOView.getId())){
			
			// clonando para despues verificar si se hizo cambios en la modificacion
			modelTOModify = (InvestorTypeModelTO) modelTOView.clone();
			
			modelTOView.setLstEconomicActivity(getLstEconomicActivity());
			renderBtnModify();
			return PAGE_VIEW;
		}else{
			// lanzar mensaje de requerimiento de seleccion
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_ANNULATECUI_REQUIRED_SELECTED, null);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('alertValidationMgmtBean').show();");
			return null;
		}
	}
	
	public void beforeModify() throws Exception{
		
		// verificar si se hizo algun cambio
		if(Validations.validateIsNotNull(modelTOModify)){
			
			if(!modelTOView.getShortName().equals(modelTOModify.getShortName())){
				if(ejbInvestorType.verifyInvestorTypeIsDuplicated(modelTOView)){
					for(ParameterTable actEconomica:getLstEconomicActivity()){
						if(actEconomica.getParameterTablePk().equals(modelTOView.getEconomicActivity())){
							modelTOView.setEconomicActivityText(actEconomica.getParameterName());
							break;
						}
					}
					Object[] arrBodyData = { modelTOView.getShortName(), modelTOView.getEconomicActivityText() };
					showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MSG_VALIDATED_INVESTORTYPE_DUPLICATED, arrBodyData);
					JSFUtilities.showComponent("alertValidationMgmtBean");
					return;
				}
			}
			
			if(modelTOModify.equals(modelTOView)){
				// si aun permaneces iguales entonces no hubo cambios, 
				// lanzar error de que no se realizO ninguna modificaciOn
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_VALIDATION_INVESTORTYPE_NOTFOUNDCHANGES, null);
				JSFUtilities.showComponent("alertValidationMgmtBean");
			}else{
				
				JSFUtilities.hideComponent("alertValidationMgmtBean");
				
				// proceder a la confirmacion para la modificacion
				Object[] arrBodyData = { modelTOView.getShortName() };
				showMessageOnDialog(PropertiesConstants.MESSAGES_CONFIRM, null, PropertiesConstants.LBL_MSG_CONFIRM_MODIFY_INVESTORTYPE, arrBodyData);
				JSFUtilities.showComponent("cnfRegisterMgmtBean");
			}
		}else{
			// Se ha producido un error. Teoricamente nunca deberia pasar este error
			throw new Exception("El objeto modelTOModify esta nulo");
		}
		JSFUtilities.updateComponent("opnlDialogs");
	}
	
	public void saveModify() throws Exception {
		try {
			hideDialogsListener(null);
			
			modelTOView.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
			modelTOView.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
			modelTOView.setLastModifyApp(userPrivilege.getUserAcctions().getIdPrivilegeModify().longValue());
			
			ParameterTable investorType = ejbInvestorType.modifyInvestorType(modelTOView);
			
			// mensaje de exito
			Object[] arrBodyData = { String.valueOf(investorType.getParameterName()) };
			showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, PropertiesConstants.MSG_SUCCESFUL_MODIFY_INVESTORTYPE, arrBodyData);
			btnSearch();
			JSFUtilities.showComponent("cnfEndTrasnactionMgmtBean");
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	// TODO metodos para la rederizacion de los botones
	public void renderBtnModify() {
		renderFalseAllOperation();
		operationModify = true;
	}
	
	private void renderFalseAllOperation() {
		operationModify = false;
//		operationAnulate = false;
//		operationConfirm = false;
//		operationReject = false;
	}
	
	public void saveOperation() throws Exception {
		if(operationModify){
			saveModify();
			return;
		}
//		if (operationAnulate) {
//			saveAnulateHolderAnnulation();
//			return;
//		}
//		if (operationReview) {
//			saveReviewHolderAnnulation();
//			return;
//		}
//		if (operationConfirm) {
//			saveConfirmHolderAnnulation();
//			return;
//		}
	}
	
	// TODO metodos genericos para cargar las listas(combobox) necesarios para cualquier pantalla (registro, vista, search)
	public List<ParameterTable> getLstEconomicActivity() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.ECONOMIC_ACTIVITY.getCode());
			return generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
			return new ArrayList<>();
		}
	}
	
	public List<ParameterTable> getLstEconomicActivityToRelationInvestorType() {
		try {
			return ejbInvestorType.getLstEconomicActivityToRelationInvestortype();
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
			return new ArrayList<>();
		}
	}
	
	public List<ParameterTable> getLstInvestioTypeToRelation() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.INVESTOR_TYPE_ECONOMIC_ACTIVITY.getCode());
			return generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
			return new ArrayList<>();
		}
	}
}
