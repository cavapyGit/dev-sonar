package com.pradera.accounts.investorType.facade;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.pradera.accounts.investorType.to.InvestorTypeModelTO;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;

/**
 * @author rlarico
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class InvestorTypeServiceBean extends CrudDaoServiceBean {
	
	public Integer getFirtsIdInvestorTypeByEconomicActivity(Integer economicActivity){
		List<Object> lst = new ArrayList<>();
		StringBuilder sd=new StringBuilder();
		sd.append(" SELECT PT.PARAMETER_TABLE_PK ");
		sd.append(" FROM PARAMETER_TABLE PT");
		sd.append(" WHERE PT.ID_RELATED_PARAMETER_TABLE_FK = :economicActivity");
		sd.append(" AND PT.MASTER_TABLE_FK = :idMasterTablePk");
		sd.append(" AND ROWNUM <= 1");
		sd.append(" ORDER BY PT.PARAMETER_TABLE_PK ASC");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("economicActivity", economicActivity);
		parameters.put("idMasterTablePk", MasterTableType.INVESTOR_TYPE_ECONOMIC_ACTIVITY.getCode());
		lst = findByNativeQuery(sd.toString(), parameters);
		if(Validations.validateListIsNotNullAndNotEmpty(lst) && Validations.validateIsNotNull(lst.get(0))){
			return Integer.parseInt(lst.get(0).toString());
		}
		return null;
	}
	
	public List<ParameterTable> getLstEconomicActivityToRelationInvestortype() throws ServiceException{
		StringBuilder sd=new StringBuilder();
		sd.append(" select p ");
		sd.append(" from ParameterTable p");
		sd.append(" inner join p.masterTable mt");
		sd.append(" where mt.masterTablePk = :idMasterTablePk");
		sd.append(" and p.parameterState = 1");
		sd.append(" order by p.parameterName");
		
		Query q = em.createQuery(sd.toString());
		q.setParameter("idMasterTablePk", MasterTableType.ECONOMIC_ACTIVITY.getCode());
		return q.getResultList();
	}

	/**
	 * listado de tipo de inversionista
	 * @param filter
	 * @return
	 * @throws ServiceException
	 */
	public List<InvestorTypeModelTO> getLstInvestorTypeModelTO(InvestorTypeModelTO filter) throws ServiceException{
		List<InvestorTypeModelTO> lst = new ArrayList<>();
		StringBuilder sd = new StringBuilder();
		sd.append(" select ");
		sd.append(" new com.pradera.accounts.investorType.to.InvestorTypeModelTO(");
		sd.append(" p.parameterTablePk,");
		sd.append(" p.parameterName,");
		sd.append(" p.description,");
		sd.append(" p.parameterState");
		sd.append(" ) ");
		sd.append(" from ParameterTable p");
		sd.append(" inner join p.masterTable mt");
		sd.append(" where mt.masterTablePk = :idMasterTablePk");
		sd.append(" and p.parameterState = :parameterState");
		if (Validations.validateIsNotNullAndNotEmpty(filter.getShortName())){
			sd.append(" and p.parameterName = :shortName");
		}
		if (Validations.validateIsNotNullAndNotEmpty(filter.getDescription())){
			sd.append(" and p.description = :description");
		}
		sd.append(" order by p.parameterTablePk asc");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idMasterTablePk", MasterTableType.INVESTOR_TYPE_ECONOMIC_ACTIVITY.getCode());
		if (Validations.validateIsNotNullAndNotEmpty(filter.getShortName())){
			parameters.put("shortName", filter.getShortName());
		}
		if (Validations.validateIsNotNullAndNotEmpty(filter.getDescription())){
			parameters.put("description", filter.getDescription());
		}		

		parameters.put("parameterState", GeneralConstants.ONE_VALUE_INTEGER);
		lst = findListByQueryString(sd.toString(), parameters);
		return lst;
	}
	
	public Integer getNextParameterTableByMasterTable1219() throws ServiceException{
		List<Object> lst = new ArrayList<>();
		StringBuilder sd = new StringBuilder();
		sd.append(" SELECT NVL ( MAX ( P.PARAMETER_TABLE_PK ) ,0 ) 						");
		sd.append(" FROM PARAMETER_TABLE P 												");
		sd.append(" INNER JOIN MASTER_TABLE M ON M.MASTER_TABLE_PK = P.MASTER_TABLE_FK 	");
		sd.append(" WHERE P.MASTER_TABLE_FK = :idMasterTablePk							");
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idMasterTablePk", MasterTableType.INVESTOR_TYPE_ECONOMIC_ACTIVITY.getCode());
		lst = findByNativeQuery(sd.toString(), parameters);
		if(Validations.validateListIsNotNullAndNotEmpty(lst) && Validations.validateIsNotNull(lst.get(0))){
			Integer parameterPk = Integer.parseInt(lst.get(0).toString());
			
			System.out.println("::: Buscando el correlativo vacante en PARAMETER_TABLE");
			try {
				ParameterTable pt = null;
				do{
					pt = em.find(ParameterTable.class, ++parameterPk);
				}while(Validations.validateIsNotNull(pt) && Validations.validateIsNotNullAndPositive(pt.getParameterTablePk()));
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("::: "+e.getMessage());
			}
			return parameterPk;
			
		}
		return null;
	}
	
	/**
	 * registro de un tipo de inversionista
	 * @param entity
	 * @return
	 */
	public ParameterTable registerInvestorType(ParameterTable entity){
		return createWithFlush(entity);
	}
	
	/**
	 * modificacion de un tipo de inversionista
	 * @param entity
	 * @return
	 * @throws ServiceException 
	 */
	public ParameterTable modifyInvestorType(ParameterTable entity) throws ServiceException{
		return update(entity);
	}
}
