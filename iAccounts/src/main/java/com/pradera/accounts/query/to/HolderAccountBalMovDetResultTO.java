package com.pradera.accounts.query.to; 

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class HolderAccountMgmtBean.
 * Consulta de saldos y movimientos
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/02/2013
 */
public class HolderAccountBalMovDetResultTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id holder account movement pk. */
	private Long idHolderAccountMovementPk;

	/** The movement date. */
	private Date movementDate;
	
	/** The movement name. */
	private String movementName;
	
	/** The operation number. */
	private Long operationNumber;
	
	/** The operation date. */
	private Date operationDate;
	
	/** The id source participant. */
	private Long idSourceParticipant;
	
	/** The id target participant. */
	private Long idTargetParticipant;
	
	/** The movement quantity. */
	private BigDecimal movementQuantity;
	
	/** The operation price. */
	private BigDecimal operationPrice;
	
	/** The id modality. */
	private Long idModality;
	
	/** The Id mechanism. */
	private Long IdMechanism;
	
	/** The movement detail. */
	private List<String> movementDetail;
	
	/** The mechanism name. */
	private String mechanismName;
	
	/**
	 * Gets the movement date.
	 *
	 * @return the movement date
	 */
	public Date getMovementDate() {
		return movementDate;
	}
	
	/**
	 * Sets the movement date.
	 *
	 * @param movementDate the new movement date
	 */
	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}
	
	/**
	 * Gets the movement name.
	 *
	 * @return the movement name
	 */
	public String getMovementName() {
		return movementName;
	}
	
	/**
	 * Sets the movement name.
	 *
	 * @param movementName the new movement name
	 */
	public void setMovementName(String movementName) {
		this.movementName = movementName;
	}
	
	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}
	
	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}
	
	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return operationDate;
	}
	
	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}
	
	/**
	 * Gets the id source participant.
	 *
	 * @return the id source participant
	 */
	public Long getIdSourceParticipant() {
		return idSourceParticipant;
	}
	
	/**
	 * Sets the id source participant.
	 *
	 * @param idSourceParticipant the new id source participant
	 */
	public void setIdSourceParticipant(Long idSourceParticipant) {
		this.idSourceParticipant = idSourceParticipant;
	}
	
	/**
	 * Gets the id target participant.
	 *
	 * @return the id target participant
	 */
	public Long getIdTargetParticipant() {
		return idTargetParticipant;
	}
	
	/**
	 * Sets the id target participant.
	 *
	 * @param idTargetParticipant the new id target participant
	 */
	public void setIdTargetParticipant(Long idTargetParticipant) {
		this.idTargetParticipant = idTargetParticipant;
	}
	
	/**
	 * Gets the movement quantity.
	 *
	 * @return the movement quantity
	 */
	public BigDecimal getMovementQuantity() {
		return movementQuantity;
	}
	
	/**
	 * Sets the movement quantity.
	 *
	 * @param movementQuantity the new movement quantity
	 */
	public void setMovementQuantity(BigDecimal movementQuantity) {
		this.movementQuantity = movementQuantity;
	}
	
	/**
	 * Gets the operation price.
	 *
	 * @return the operation price
	 */
	public BigDecimal getOperationPrice() {
		return operationPrice;
	}
	
	/**
	 * Sets the operation price.
	 *
	 * @param operationPrice the new operation price
	 */
	public void setOperationPrice(BigDecimal operationPrice) {
		this.operationPrice = operationPrice;
	}
	
	/**
	 * Gets the id modality.
	 *
	 * @return the id modality
	 */
	public Long getIdModality() {
		return idModality;
	}
	
	/**
	 * Sets the id modality.
	 *
	 * @param idModality the new id modality
	 */
	public void setIdModality(Long idModality) {
		this.idModality = idModality;
	}

	/**
	 * Gets the id mechanism.
	 *
	 * @return the id mechanism
	 */
	public Long getIdMechanism() {
		return IdMechanism;
	}

	/**
	 * Sets the id mechanism.
	 *
	 * @param idMechanism the new id mechanism
	 */
	public void setIdMechanism(Long idMechanism) {
		IdMechanism = idMechanism;
	}

	/**
	 * Gets the movement detail.
	 *
	 * @return the movement detail
	 */
	public List<String> getMovementDetail() {
		return movementDetail;
	}

	/**
	 * Sets the movement detail.
	 *
	 * @param movementDetail the new movement detail
	 */
	public void setMovementDetail(List<String> movementDetail) {
		this.movementDetail = movementDetail;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((idHolderAccountMovementPk == null) ? 0
						: idHolderAccountMovementPk.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HolderAccountBalMovDetResultTO other = (HolderAccountBalMovDetResultTO) obj;
		if (idHolderAccountMovementPk == null) {
			if (other.idHolderAccountMovementPk != null)
				return false;
		} else if (!idHolderAccountMovementPk
				.equals(other.idHolderAccountMovementPk))
			return false;
		return true;
	}

	/**
	 * Gets the id holder account movement pk.
	 *
	 * @return the id holder account movement pk
	 */
	public Long getIdHolderAccountMovementPk() {
		return idHolderAccountMovementPk;
	}

	/**
	 * Sets the id holder account movement pk.
	 *
	 * @param idHolderAccountMovementPk the new id holder account movement pk
	 */
	public void setIdHolderAccountMovementPk(Long idHolderAccountMovementPk) {
		this.idHolderAccountMovementPk = idHolderAccountMovementPk;
	}

	/**
	 * Gets the mechanism name.
	 *
	 * @return the mechanism name
	 */
	public String getMechanismName() {
		return mechanismName;
	}

	/**
	 * Sets the mechanism name.
	 *
	 * @param mechanismName the new mechanism name
	 */
	public void setMechanismName(String mechanismName) {
		this.mechanismName = mechanismName;
	}
}