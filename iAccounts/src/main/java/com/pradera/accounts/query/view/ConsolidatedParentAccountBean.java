package com.pradera.accounts.query.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.accounts.query.facade.ConsolidateParentAccountServiceFacade;
import com.pradera.accounts.query.facade.HolderAccountBalancMovServiceFacade;
import com.pradera.accounts.query.facade.IssuerHolderQueryServiceFacade;
import com.pradera.accounts.query.to.ConsolidateParentAccountResultTO;
import com.pradera.accounts.query.to.ConsolidateParentAccountTO;
import com.pradera.accounts.util.view.PropertiesConstants;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.core.component.helperui.to.IssuerSearcherTO;
import com.pradera.core.component.helperui.to.IssuerTO;
import com.pradera.core.component.helperui.view.SecuritiesHelperBean;
import com.pradera.core.component.helperui.view.SecurityHelpBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.ValorizationQueryType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class ConsolidatedParentAccountBean.
 * Consolidado de Cuenta Matriz por Valor
 * Martin Zarate Rafael
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 28/07/2013
 */

@DepositaryWebBean
public class ConsolidatedParentAccountBean extends GenericBaseBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The participant list. */
	private List<Participant> participantList;
	
	/** The consolidate parent account to. */
	private ConsolidateParentAccountTO consolidateParentAccountTO;
	
	/** The consolidate parent account result to list. */
	private List<ConsolidateParentAccountResultTO> consolidateParentAccountResultTOList;
	
	/** The lst cbo filter securitie class. */
	private List<ParameterTable> lstCboFilterSecuritieClass;
	
	/** The lst cbo filter currency. */
	private List<ParameterTable> lstCboFilterCurrency;
	
	/** The daily exchange rates. */
	private DailyExchangeRates dailyExchangeRates;
	
	/** The is participant. */
	private boolean isParticipant;
	
	/** The is issuer. */
	private boolean isIssuer;
	
	/** The consolidate parent account result to. */
	private ConsolidateParentAccountResultTO consolidateParentAccountResultTO;
	
	
	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
	/** The consolidate service Facade. */
	@EJB
	ConsolidateParentAccountServiceFacade consolidateParentAccountServiceFacade;
	
	/** The Security Helper Bean. */
	@Inject
	SecuritiesHelperBean securitiesHelperBean;
	
	/** The security help bean. */
	@Inject
	SecurityHelpBean securityHelpBean;
	
	/** The obj issuer holder query service facade. */
	@EJB
	IssuerHolderQueryServiceFacade objIssuerHolderQueryServiceFacade;
	
	/** The general parameter facade. */
	@EJB
	private GeneralParametersFacade generalParameterFacade;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The helper component facade. */
	@EJB
	private HelperComponentFacade 		  helperComponentFacade;
	
	/** The holder account balanc mov service facade. */
	@EJB
	HolderAccountBalancMovServiceFacade holderAccountBalancMovServiceFacade;
	
	/** The holder account balance. */
	private HolderAccountBalance holderAccountBalance;
	
	/** The option selected one radio. */
	private Integer optionSelectedOneRadio;
	
	/** The lst account to. */
	private GenericDataModel<HolderAccount> lstAccountTo;
	
	/** The selected account to. */
	private HolderAccount selectedAccountTO;
	
	/** The last pos. */
	private int lastPos=-1;
	
	/**
	 * Inits the.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException{
		initObjects();
		consolidateParentAccountResultTOList = null;
		consolidateParentAccountResultTO = new ConsolidateParentAccountResultTO();
		holderAccountBalance = new HolderAccountBalance();
	    listParticipants();
	    loadCboFilterSecuritieClass();
	    loadCboFilterCurency();
	    if(validateIsIssuer()){
	    	searchIssuerByCode();
	    }
		
	}
	
	/**
	 * Search issuer by code.
	 *
	 * @throws ServiceException the service exception
	 */
	public void searchIssuerByCode() throws ServiceException{
		if(Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getIssuer())
				&& Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getIssuer().getIdIssuerPk())){
			IssuerSearcherTO issuerSearcher = new IssuerSearcherTO();
			issuerSearcher.setIssuerCode(consolidateParentAccountTO.getIssuer().getIdIssuerPk());
			IssuerTO issuerTO = helperComponentFacade.findIssuerByCode(issuerSearcher);
			Issuer issuer = new Issuer();
			issuer.setIdIssuerPk(issuerTO.getIssuerCode());
			issuer.setMnemonic(issuerTO.getMnemonic());
			consolidateParentAccountTO.setIssuer(issuer);
		}
	}
	
	/**
	 * Validate is issuer.
	 *
	 * @return true, if successful
	 */
	public boolean validateIsIssuer(){
		isIssuer = false;
		Issuer issuerTO = new Issuer();
		if(userInfo.getUserAccountSession().isIssuerInstitucion() && 
				Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getIssuerCode())){
			isIssuer = true;			
			issuerTO.setIdIssuerPk(userInfo.getUserAccountSession().getIssuerCode());	
			consolidateParentAccountTO.setIssuer(issuerTO);
			//AGREGANDO PARAMETROS PARA PODER CUANDO SEA EMISOR 
			optionSelectedOneRadio = GeneralConstants.ZERO_VALUE_INTEGER;
		}		
		return isIssuer;
	}
	
	/**
	 * List participants.
	 *
	 * @throws ServiceException the service exception
	 */
	//LIST PARTICIPANTS
	private void listParticipants() throws ServiceException{
		Participant participantTO = new Participant();
		Long participantCode = null;
		isParticipant = false;
		if(userInfo.getUserAccountSession().isParticipantInstitucion() || userInfo.getUserAccountSession().isInstitution(InstitutionType.AFP)
				|| userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
			if( Validations.validateIsNotNullAndPositive(userInfo.getUserAccountSession().getParticipantCode())){
				isParticipant = true;
				participantCode = userInfo.getUserAccountSession().getParticipantCode();
				participantTO.setIdParticipantPk(participantCode);
				consolidateParentAccountTO.setParticipant(participantTO);
			}else{
				return;
			}
		}	
		participantList = accountsFacade.getLisParticipantServiceBean(participantTO);
	}

	/**
	 * Search consolidate.
	 */
	//SERCH CONSOLIDATE
	public void searchConsolidate(){
		try{	
			if(Validations.validateIsNullOrEmpty(optionSelectedOneRadio)){
				//SI NO SE SELECCIONO FILTRO SE ENVIA MENSAJE DE ALERTA
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getGenericMessage(PropertiesConstants.ERROR_NOT_SELECTED_ISSUER_OR_PARTICIPANT));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			consolidateParentAccountTO.setBlParticipantInv(userInfo.getUserAccountSession().isParticipantInvestorInstitucion());
			consolidateParentAccountResultTOList = consolidateParentAccountServiceFacade.searchConsolidateParentAccount(consolidateParentAccountTO);
		}catch(ServiceException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Clean.
	 *
	 * @throws ServiceException the service exception
	 */
	//CLEAR  FILTERS
	public void clean() throws ServiceException{
		JSFUtilities.resetViewRoot();
		lstAccountTo = null;
		initObjects();
		securitiesHelperBean.setSecurityDescription(null);
		consolidateParentAccountResultTOList = null;
		consolidateParentAccountResultTO = null;
		holderAccountBalance = null;
		optionSelectedOneRadio = null;
		listParticipants();
		if(validateIsIssuer()){
	    	searchIssuerByCode();
	    }
	}
	
	/**
	 * Inits the objects.
	 */
	public void initObjects(){
		consolidateParentAccountTO = new ConsolidateParentAccountTO();
		consolidateParentAccountTO.setSecurity(new Security());
		consolidateParentAccountTO.setParticipant(new Participant());
		consolidateParentAccountTO.setIssuer(new Issuer());
		consolidateParentAccountTO.setHolder(new Holder());
		consolidateParentAccountTO.setHolderAccount(new HolderAccount());
	}
	
	/**
	 * Gets the issuer by security.
	 *
	 * @return the issuer by security
	 */
	public void getIssuerBySecurity(){		
		if(consolidateParentAccountTO.getSecurity().getIdSecurityCodePk() != null && !isIssuer){			
			try {
				SecurityTO securityTO = new SecurityTO();
				securityTO.setIdSecurityCodePk(consolidateParentAccountTO.getSecurity().getIdSecurityCodePk());
				Security objSecurity = helperComponentFacade.findSecurityComponentServiceFacade(securityTO);				
				IssuerSearcherTO issuerTo = new IssuerSearcherTO();
				issuerTo.setIssuerCode(objSecurity.getIssuer().getIdIssuerPk());
				Issuer issuer =	objIssuerHolderQueryServiceFacade.getIssuerDetails(issuerTo);
				consolidateParentAccountTO.setIssuer(issuer);				
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			//consolidateParentAccountTO.setIssuer(new Issuer());
			consolidateParentAccountTO.setSecurity(new Security());
		}
		clearGrill();
	}
	
	/**
	 * Clear grill.
	 */
	public void clearGrill(){		
		consolidateParentAccountResultTOList = null;		
	}
	
	/**
	 * Change participant.
	 */
	public void changeParticipant(){
		consolidateParentAccountTO.setHolder(new Holder());
		lstAccountTo=null;
		clearGrill();
	}
	
	/**
	 * Search holder and holder account.
	 */
	public void searchHolderAndHolderAccount(){	
		try {			
			if (Validations.validateIsNull(consolidateParentAccountTO.getHolder()) || Validations.validateIsNull(consolidateParentAccountTO.getHolder().getIdHolderPk())) {
				lstAccountTo=null;
				return;
			}
			
			HolderTO holderTO = new HolderTO();
			holderTO.setHolderId(consolidateParentAccountTO.getHolder().getIdHolderPk());
			holderTO.setFlagAllHolders(true);
			holderTO.setFlagOnlyInvestors(false);
			holderTO.setParticipantFk(consolidateParentAccountTO.getParticipant().getIdParticipantPk());
			
			List<HolderAccountHelperResultTO> lista = helperComponentFacade.searchAccountByFilter(holderTO);
			List<HolderAccount> listaAux = new ArrayList<HolderAccount>();
			
			if (lista==null || lista.isEmpty()){
				lstAccountTo = new GenericDataModel<>();
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
												PropertiesUtilities.getGenericMessage(PropertiesConstants.COMMONS_LABEL_PARTICIPANT_CUI_INCORRECT));
				JSFUtilities.showSimpleValidationDialog();
			}else if(lista.size()==1){
				listaAux= fillHolderAccountHelper(lista);
				lstAccountTo = new GenericDataModel<>(listaAux);
				consolidateParentAccountTO.setHolderAccount(listaAux.get(0));
//				selectedAccountTO=listaAux.get(0);
			}else{
				listaAux= fillHolderAccountHelper(lista);
				lstAccountTo = new GenericDataModel<>(listaAux);
			}
			clearGrill();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Selected one row.
	 *
	 * @param haView the ha view
	 */
	public void selectedOneRow(HolderAccount haView){

	}
	
	/**
	 * Fill holder account helper.
	 *
	 * @param lista the lista
	 * @return the list
	 */
	public List<HolderAccount> fillHolderAccountHelper(List<HolderAccountHelperResultTO> lista){
		List<HolderAccount> listaAux = new ArrayList<HolderAccount>();
		for(HolderAccountHelperResultTO haHelper: lista){
			HolderAccount ha= new HolderAccount();
			ha.setIdHolderAccountPk(haHelper.getAccountPk());
			ha.setAccountNumber(haHelper.getAccountNumber());
			ha.setAlternateCode(haHelper.getAlternateCode());
			ha.setHolderDescriptionList(haHelper.getHolderDescriptionList());
			ha.setStateAccountDescription(haHelper.getAccountStatusDescription());
			listaAux.add(ha);
		}
		return listaAux;
	}
	
	/**
	 * Search holder account balance.
	 *
	 * @param event the event
	 */
	public void searchHolderAccountBalance(ActionEvent event){
		consolidateParentAccountResultTO = (ConsolidateParentAccountResultTO) event.getComponent().getAttributes().get("consolidateInformation");
	
		HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();		
		holderAccountBalanceTO.setIdHolderAccount(consolidateParentAccountResultTO.getIdHolderAccountPk());
		holderAccountBalanceTO.setIdParticipant(consolidateParentAccountResultTO.getIdParticipantPk());
		holderAccountBalanceTO.setIdSecurityCode(consolidateParentAccountResultTO.getIdSecurityCodePk());
		
		try {
			holderAccountBalance = holderAccountBalancMovServiceFacade.holderAccountBalanceServiceFacade(holderAccountBalanceTO);
			//CONCAT JOINT ACCOUNT
			consolidateParentAccountResultTO.setInversionistFullName(new ArrayList<String>());
			List<Object[]> lstObjects = holderAccountBalancMovServiceFacade.getHolderAccountDetailSF(consolidateParentAccountResultTO.getIdHolderAccountPk());
			for(Object[] objectList: lstObjects){
				if(Integer.valueOf(objectList[0].toString()).equals(PersonType.NATURAL.getCode())){
					if(Validations.validateIsNotNullAndNotEmpty(objectList[4])){
						consolidateParentAccountResultTO.getInversionistFullName().add(objectList[2]+" "+objectList[3]+" "+objectList[4]);
					}else{
						consolidateParentAccountResultTO.getInversionistFullName().add(objectList[2]+" "+objectList[3]);
					}
				}else{
					if(Validations.validateIsNotNullAndNotEmpty(objectList[1].toString())){
						consolidateParentAccountResultTO.getInversionistFullName().add(objectList[5].toString() + " - " + objectList[1].toString());
					}
				}
			}		
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Valid holder account.
	 */
	public void validHolderAccount(){
		if(Validations.validateIsNotNull(consolidateParentAccountTO.getParticipant()) && 
		   Validations.validateIsNotNullAndPositive(consolidateParentAccountTO.getParticipant().getIdParticipantPk()) &&
		   Validations.validateIsNotNull(consolidateParentAccountTO.getHolder()) && 
		   Validations.validateIsNotNullAndPositive(consolidateParentAccountTO.getHolder().getIdHolderPk()) &&
		   Validations.validateIsNotNull(consolidateParentAccountTO.getHolderAccount()) && 
		   Validations.validateIsNotNullAndPositive(consolidateParentAccountTO.getHolderAccount().getIdHolderAccountPk()) ){
		
			HolderAccount holderAccount=null;
			try {
				holderAccount = getHolderAccountAndDetails(consolidateParentAccountTO.getHolderAccount().getAccountNumber(), consolidateParentAccountTO.getParticipant().getIdParticipantPk());
			} catch (ServiceException e) {
				e.printStackTrace();
			}
				
			if(Validations.validateIsNotNull(holderAccount) && Validations.validateIsNotNull(holderAccount.getIdHolderAccountPk())){
				if (!holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())) {
					alert(PropertiesUtilities.getMessage(PropertiesConstants.VALIDATE_HOLDER_ACCOUNT_WRONG_HOLDER_STATE));
					consolidateParentAccountTO.setHolderAccount(new HolderAccount());
					return;
				}
			}else{
				alert(PropertiesUtilities.getMessage(PropertiesConstants.VALIDATE_HOLDER_ACCOUNT_WRONG_NUMBER));
				consolidateParentAccountTO.setHolderAccount(new HolderAccount());
				return;
			}
			consolidateParentAccountTO.setHolderAccount(holderAccount);
		}	
	}
	
	/**
	 * Show security detail.
	 *
	 * @param idSecurityCode the id security code
	 */
	public void showSecurityDetail(String idSecurityCode){
		securityHelpBean.setSecurityCode(idSecurityCode);
		securityHelpBean.setName("helpSecurity");
		securityHelpBean.searchSecurityHandler();
	}
	
	/**
	 * Load cbo filter securitie class.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboFilterSecuritieClass() throws ServiceException{
		ParameterTableTO parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.SECURITIES_CLASS.getCode() );
		lstCboFilterSecuritieClass=generalParameterFacade.getListParameterTableServiceBean( parameterTableTO );
	}
	
	/**
	 * Load cbo filter curency.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboFilterCurency() throws ServiceException{
		ParameterTableTO parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.CURRENCY.getCode() );
		lstCboFilterCurrency=generalParameterFacade.getListParameterTableServiceBean( parameterTableTO );
	}
	
	/**
	 * Gets the holder account and details.
	 *
	 * @param accountNumber the account number
	 * @param idParticipant the id participant
	 * @return the holder account and details
	 * @throws ServiceException the service exception
	 */
	public HolderAccount getHolderAccountAndDetails(Integer accountNumber, Long idParticipant) throws ServiceException{
		return helperComponentFacade.findHolderAccount(accountNumber, idParticipant, null);
	}
	
	/**
	 * Alert.
	 *
	 * @param message the message
	 */
	public void alert(String message){
		String header=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING);
		
		showMessageOnDialog(header,message); 
		JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
		JSFUtilities.updateComponent("opnlDialogs");
	}
	
	/**
	 * Evaluate option selected.
	 */
	public void evaluateOptionSelected(){
		clearGrill();
		if(optionSelectedOneRadio.equals(BooleanType.NO.getCode())){
			
		}else if(optionSelectedOneRadio.equals(BooleanType.YES.getCode())){
			
		}
		if(optionSelectedOneRadio.compareTo(GeneralConstants.ZERO_VALUE_INTEGER)==0){
			consolidateParentAccountTO.setParticipant(new Participant());
			consolidateParentAccountTO.setHolder(new Holder());
			consolidateParentAccountTO.setHolderAccount(new HolderAccount());
			lstAccountTo = null;
		}else if(optionSelectedOneRadio.compareTo( GeneralConstants.ONE_VALUE_INTEGER )==0){
			consolidateParentAccountTO.setIssuer(new Issuer());
			consolidateParentAccountTO.setSecurity(new Security());
		}
	}

	/**
	 * Valorization query type list.
	 *
	 * @return the list
	 */
	public List<ValorizationQueryType> getValorizationQueryTypeList(){
		return ValorizationQueryType.list;
	}
	
	/**
	 * Gets the participant list.
	 *
	 * @return the participant list
	 */
	// GETTERS AND SETTERS
	public List<Participant> getParticipantList() {
		return participantList;
	}

	/**
	 * Sets the participant list.
	 *
	 * @param participantList the new participant list
	 */
	public void setParticipantList(List<Participant> participantList) {
		this.participantList = participantList;
	}

	/**
	 * Gets the consolidate parent account to.
	 *
	 * @return the consolidate parent account to
	 */
	public ConsolidateParentAccountTO getConsolidateParentAccountTO() {
		return consolidateParentAccountTO;
	}

	/**
	 * Sets the consolidate parent account to.
	 *
	 * @param consolidateParentAccountTO the new consolidate parent account to
	 */
	public void setConsolidateParentAccountTO(
			ConsolidateParentAccountTO consolidateParentAccountTO) {
		this.consolidateParentAccountTO = consolidateParentAccountTO;
	}

	/**
	 * Gets the daily exchange rates.
	 *
	 * @return the daily exchange rates
	 */
	public DailyExchangeRates getDailyExchangeRates() {
		return dailyExchangeRates;
	}

	/**
	 * Sets the daily exchange rates.
	 *
	 * @param dailyExchangeRates the new daily exchange rates
	 */
	public void setDailyExchangeRates(DailyExchangeRates dailyExchangeRates) {
		this.dailyExchangeRates = dailyExchangeRates;
	}

	/**
	 * Gets the consolidate parent account result to list.
	 *
	 * @return the consolidate parent account result to list
	 */
	public List<ConsolidateParentAccountResultTO> getConsolidateParentAccountResultTOList() {
		return consolidateParentAccountResultTOList;
	}

	/**
	 * Sets the consolidate parent account result to list.
	 *
	 * @param consolidateParentAccountResultTOList the new consolidate parent account result to list
	 */
	public void setConsolidateParentAccountResultTOList(
			List<ConsolidateParentAccountResultTO> consolidateParentAccountResultTOList) {
		this.consolidateParentAccountResultTOList = consolidateParentAccountResultTOList;
	}

	/**
	 * Checks if is participant.
	 *
	 * @return the isParticipant
	 */
	public boolean isParticipant() {
		return isParticipant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param isParticipant the isParticipant to set
	 */
	public void setParticipant(boolean isParticipant) {
		this.isParticipant = isParticipant;
	}

	/**
	 * Checks if is issuer.
	 *
	 * @return true, if is issuer
	 */
	public boolean isIssuer() {
		return isIssuer;
	}

	/**
	 * Sets the issuer.
	 *
	 * @param isIssuer the new issuer
	 */
	public void setIssuer(boolean isIssuer) {
		this.isIssuer = isIssuer;
	}
	
	/**
	 * Gets the holder account balance.
	 *
	 * @return the holder account balance
	 */
	public HolderAccountBalance getHolderAccountBalance() {
		return holderAccountBalance;
	}
	
	/**
	 * Sets the holder account balance.
	 *
	 * @param holderAccountBalance the new holder account balance
	 */
	public void setHolderAccountBalance(HolderAccountBalance holderAccountBalance) {
		this.holderAccountBalance = holderAccountBalance;
	}
	
	/**
	 * Gets the consolidate parent account result to.
	 *
	 * @return the consolidate parent account result to
	 */
	public ConsolidateParentAccountResultTO getConsolidateParentAccountResultTO() {
		return consolidateParentAccountResultTO;
	}
	
	/**
	 * Sets the consolidate parent account result to.
	 *
	 * @param consolidateParentAccountResultTO the new consolidate parent account result to
	 */
	public void setConsolidateParentAccountResultTO(
			ConsolidateParentAccountResultTO consolidateParentAccountResultTO) {
		this.consolidateParentAccountResultTO = consolidateParentAccountResultTO;
	}
	
	/**
	 * Gets the option selected one radio.
	 *
	 * @return the option selected one radio
	 */
	public Integer getOptionSelectedOneRadio() {
		return optionSelectedOneRadio;
	}
	
	/**
	 * Sets the option selected one radio.
	 *
	 * @param optionSelectedOneRadio the new option selected one radio
	 */
	public void setOptionSelectedOneRadio(Integer optionSelectedOneRadio) {
		this.optionSelectedOneRadio = optionSelectedOneRadio;
	}
	
	/**
	 * Gets the lst cbo filter securitie class.
	 *
	 * @return the lst cbo filter securitie class
	 */
	public List<ParameterTable> getLstCboFilterSecuritieClass() {
		return lstCboFilterSecuritieClass;
	}
	
	/**
	 * Sets the lst cbo filter securitie class.
	 *
	 * @param lstCboFilterSecuritieClass the new lst cbo filter securitie class
	 */
	public void setLstCboFilterSecuritieClass(
			List<ParameterTable> lstCboFilterSecuritieClass) {
		this.lstCboFilterSecuritieClass = lstCboFilterSecuritieClass;
	}
	
	/**
	 * Gets the lst cbo filter currency.
	 *
	 * @return the lst cbo filter currency
	 */
	public List<ParameterTable> getLstCboFilterCurrency() {
		return lstCboFilterCurrency;
	}
	
	/**
	 * Sets the lst cbo filter currency.
	 *
	 * @param lstCboFilterCurrency the new lst cbo filter currency
	 */
	public void setLstCboFilterCurrency(List<ParameterTable> lstCboFilterCurrency) {
		this.lstCboFilterCurrency = lstCboFilterCurrency;
	}
	
	/**
	 * Gets the lst account to.
	 *
	 * @return the lst account to
	 */
	public GenericDataModel<HolderAccount> getLstAccountTo() {
		return lstAccountTo;
	}
	
	/**
	 * Gets the selected account to.
	 *
	 * @return the selected account to
	 */
	public HolderAccount getSelectedAccountTO() {
		return selectedAccountTO;
	}
	
	/**
	 * Sets the lst account to.
	 *
	 * @param lstAccountTo the new lst account to
	 */
	public void setLstAccountTo(GenericDataModel<HolderAccount> lstAccountTo) {
		this.lstAccountTo = lstAccountTo;
	}
	
	/**
	 * Sets the selected account to.
	 *
	 * @param selectedAccountTO the new selected account to
	 */
	public void setSelectedAccountTO(HolderAccount selectedAccountTO) {
		this.selectedAccountTO = selectedAccountTO;
	}
	
}
