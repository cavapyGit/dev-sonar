package com.pradera.accounts.query.to;

import java.io.Serializable;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.model.accounts.Holder;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.issuancesecuritie.Security;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class IssuerHolderDetailTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/08/2015
 */
public class IssuerHolderDetailTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The selected mc. */
	private boolean selectedMC;
	
	/** The security. */
	private Security security;
	
	/** The holder. */
	private Holder holder; 
	
	/** The lst holder to. */
	private GenericDataModel<HolderTO> lstHolderTO;
	
	/** The lst holder account balance. */
	private GenericDataModel<HolderAccountBalance> lstHolderAccountBalance;
	
	/** The holder account balance. */
	private HolderAccountBalance holderAccountBalance;
	
	/**
	 * Instantiates a new issuer holder detail to.
	 */
	public IssuerHolderDetailTO(){
		
	}

	/**
	 * Checks if is selected mc.
	 *
	 * @return true, if is selected mc
	 */
	public boolean isSelectedMC() {
		return selectedMC;
	}

	/**
	 * Sets the selected mc.
	 *
	 * @param selectedMC the new selected mc
	 */
	public void setSelectedMC(boolean selectedMC) {
		this.selectedMC = selectedMC;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Gets the holder account balance.
	 *
	 * @return the holder account balance
	 */
	public HolderAccountBalance getHolderAccountBalance() {
		return holderAccountBalance;
	}

	/**
	 * Sets the holder account balance.
	 *
	 * @param holderAccountBalance the new holder account balance
	 */
	public void setHolderAccountBalance(HolderAccountBalance holderAccountBalance) {
		this.holderAccountBalance = holderAccountBalance;
	}

	/**
	 * Gets the lst holder account balance.
	 *
	 * @return the lst holder account balance
	 */
	public GenericDataModel<HolderAccountBalance> getLstHolderAccountBalance() {
		return lstHolderAccountBalance;
	}

	/**
	 * Sets the lst holder account balance.
	 *
	 * @param lstHolderAccountBalance the new lst holder account balance
	 */
	public void setLstHolderAccountBalance(
			GenericDataModel<HolderAccountBalance> lstHolderAccountBalance) {
		this.lstHolderAccountBalance = lstHolderAccountBalance;
	}

	/**
	 * Gets the lst holder to.
	 *
	 * @return the lstHolderTO
	 */
	public GenericDataModel<HolderTO> getLstHolderTO() {
		return lstHolderTO;
	}

	/**
	 * Sets the lst holder to.
	 *
	 * @param lstHolderTO the lstHolderTO to set
	 */
	public void setLstHolderTO(GenericDataModel<HolderTO> lstHolderTO) {
		this.lstHolderTO = lstHolderTO;
	}

	
}
