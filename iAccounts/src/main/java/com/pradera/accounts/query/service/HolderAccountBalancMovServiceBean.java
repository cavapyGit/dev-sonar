package com.pradera.accounts.query.service; 

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.account.enquiries.HolderAccountMovInfoTO;
import com.pradera.accounts.query.to.HolderAccountBalMovDetResultTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.operation.to.MarketFactBalanceTO;
import com.pradera.core.component.operation.to.MarketFactDetailTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountMovement;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.component.type.ParameterBalanceType;
import com.pradera.model.custody.accreditationcertificates.AccreditationDetail;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class HolderAccountMgmtBean.
 * Consulta de saldos y movimientos
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/02/2013
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class HolderAccountBalancMovServiceBean extends CrudDaoServiceBean{
	
	/**
	 * Holder account balance service bean.
	 *
	 * @param holderAccountBalanceTO the holder account balance to
	 * @return the holder account balance
	 * @throws ServiceException the service exception
	 */
	public HolderAccountBalance holderAccountBalanceServiceBean(HolderAccountBalanceTO holderAccountBalanceTO) throws ServiceException{
		StringBuilder jpqlQuery = new StringBuilder();		
		jpqlQuery.append(" 	SELECT hab FROM HolderAccountBalance hab	");
		jpqlQuery.append("	WHERE hab.participant.idParticipantPk = :idParticipantParam	");
		jpqlQuery.append(" 	AND hab.holderAccount.idHolderAccountPk = :idHolderAccountParam	");
		jpqlQuery.append(" 	AND hab.security.idSecurityCodePk = :idSecurityCodeParam	");
		Query queryJpql = em.createQuery(jpqlQuery.toString());		
		queryJpql.setParameter("idParticipantParam", holderAccountBalanceTO.getIdParticipant());
		queryJpql.setParameter("idHolderAccountParam", holderAccountBalanceTO.getIdHolderAccount());
		queryJpql.setParameter("idSecurityCodeParam", holderAccountBalanceTO.getIdSecurityCode());		
		HolderAccountBalance objectData = null;		
		try{
			objectData = (HolderAccountBalance) queryJpql.getSingleResult();
		}catch(NoResultException ex){
			return null;
		}		
		return objectData;
	}
	
	/**
	 * List holder market fact balance service bean.
	 *
	 * @param holderAccountBalanceTO the holder account balance to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderMarketFactBalance> listHolderMarketFactBalanceServiceBean(HolderAccountBalanceTO holderAccountBalanceTO) throws ServiceException{
		
		List<HolderMarketFactBalance> objectList = new ArrayList<HolderMarketFactBalance>();
		
		StringBuilder jpqlQuery = new StringBuilder();		
		jpqlQuery.append(" 	SELECT hmb FROM HolderMarketFactBalance hmb							");
		jpqlQuery.append("	WHERE hmb.participant.idParticipantPk = :idParticipantParam			");
		jpqlQuery.append(" 	AND hmb.holderAccount.idHolderAccountPk = :idHolderAccountParam		");
		jpqlQuery.append(" 	AND hmb.security.idSecurityCodePk = :idSecurityCodeParam 			");
		jpqlQuery.append(" 	AND (hmb.totalBalance 		>= :oneBalance 	Or 						");
		jpqlQuery.append(" 		 hmb.reportedBalance	>= :oneBalance 	Or						");
		jpqlQuery.append(" 		 hmb.purchaseBalance 	>= :oneBalance 	)						");
		jpqlQuery.append(" 	AND hmb.indActive = :oneParam ");
		Query queryJpql = em.createQuery(jpqlQuery.toString());		
		queryJpql.setParameter("idParticipantParam", holderAccountBalanceTO.getIdParticipant());
		queryJpql.setParameter("idHolderAccountParam", holderAccountBalanceTO.getIdHolderAccount());
		queryJpql.setParameter("idSecurityCodeParam", holderAccountBalanceTO.getIdSecurityCode());
		queryJpql.setParameter("oneBalance", BigDecimal.ONE);
		queryJpql.setParameter("oneParam", BooleanType.YES.getCode());
				
		try{
			objectList = queryJpql.getResultList();			
		}catch(NoResultException ex){
			return null;
		}		
		return objectList;
	}
	
	/**
	 * Gets the holder account mov list service bean.
	 *
	 * @param holderAccountBalanceTO the holder account balance to
	 * @return the holder account mov list service bean
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings({ "unchecked"})
	public List<HolderAccountBalMovDetResultTO> getHolderAccountMovListServiceBean(HolderAccountBalanceTO holderAccountBalanceTO) throws ServiceException{
		StringBuilder jpqlQuery = new StringBuilder();
		
		jpqlQuery.append("SELECT  ham.id_holder_account_movement_pk, ham.OPERATION_NUMBER, ham.OPERATION_DATE, ham.ID_SOURCE_PARTICIPANT, ham.ID_TARGET_PARTICIPANT, ");
		jpqlQuery.append(" 		  ham.MOVEMENT_QUANTITY, ham.OPERATION_PRICE, ham.ID_MODALITY, ham.MOVEMENT_DATE, ham.ID_MECHANISM, ");
		jpqlQuery.append(" 		  mvt.MOVEMENT_NAME, ");
		jpqlQuery.append(" 		  CASE  mvb.id_behavior ");
		jpqlQuery.append(" 		  	WHEN 1 THEN  'SUMA - ' ");
		jpqlQuery.append(" 		  	WHEN 2 THEN 'RESTA - ' ");
		jpqlQuery.append(" 		  	ELSE  '' ");
		jpqlQuery.append(" 		  END || balTyp.balance_name AS track ");
		jpqlQuery.append("FROM HOLDER_ACCOUNT_MOVEMENT ham ");
		jpqlQuery.append("	INNER JOIN MOVEMENT_TYPE mvt ON mvt.ID_MOVEMENT_TYPE_PK = ham.ID_MOVEMENT_TYPE_FK ");
		jpqlQuery.append("	LEFT OUTER JOIN MOVEMENT_BEHAVIOR mvb ON mvb.ID_MOVEMENT_TYPE_FK = mvt.ID_MOVEMENT_TYPE_PK ");
		jpqlQuery.append("	LEFT OUTER JOIN BALANCE_TYPE balTyp ON balTyp.id_balance_type_pk = mvb.id_balance_type_fk ");
		jpqlQuery.append("	WHERE ham.ID_PARTICIPANT_FK = :idParticipantParam AND ");
		jpqlQuery.append(" 		  ham.ID_HOLDER_ACCOUNT_FK = :idHolderAccountParam AND ");
		jpqlQuery.append(" 		  mvt.IND_VISIBILITY = :onePara	AND ");
		jpqlQuery.append(" 		  ham.ID_SECURITY_CODE_FK = :idSecurityCodeParam AND ");
		jpqlQuery.append(" 		  TRUNC(ham.MOVEMENT_DATE) >= TRUNC(:initialDateParam) AND ");
		jpqlQuery.append(" 		  TRUNC(ham.MOVEMENT_DATE) <= TRUNC(:finalDateParam) ");
		jpqlQuery.append(" 		  ORDER BY ham.MOVEMENT_DATE DESC, ham.id_holder_account_movement_pk DESC, ham.OPERATION_NUMBER DESC, ham.OPERATION_DATE DESC,"); 
		jpqlQuery.append(" 		  		   ham.ID_SOURCE_PARTICIPANT DESC, ham.ID_TARGET_PARTICIPANT DESC");
		
		Query queryJpql = em.createNativeQuery(jpqlQuery.toString());
		queryJpql.setParameter("idParticipantParam", holderAccountBalanceTO.getIdParticipant());
		queryJpql.setParameter("idHolderAccountParam", holderAccountBalanceTO.getIdHolderAccount());
		queryJpql.setParameter("idSecurityCodeParam", holderAccountBalanceTO.getIdSecurityCode());
		queryJpql.setParameter("initialDateParam", holderAccountBalanceTO.getInitialDate());
		queryJpql.setParameter("finalDateParam", holderAccountBalanceTO.getFinalDate());
		queryJpql.setParameter("onePara", BooleanType.YES.getCode());
		
		List<Object[]> objectData = null;
		try{
			objectData = (List<Object[]>)queryJpql.getResultList();
		}catch(NoResultException ex){
			return null;
		}
		
		List<HolderAccountBalMovDetResultTO> holderAccountMovementList = new ArrayList<HolderAccountBalMovDetResultTO>();
		
		for(Object[] movementObjet: objectData){
			List<String> movDetail = new ArrayList<String>();
			HolderAccountBalMovDetResultTO holderAccountMovement = new HolderAccountBalMovDetResultTO();
			holderAccountMovement.setIdHolderAccountMovementPk(((BigDecimal)movementObjet[0]).longValue());
			holderAccountMovement.setOperationNumber(((BigDecimal)(movementObjet[1]==null?BigDecimal.ZERO:movementObjet[1])).longValue());
			holderAccountMovement.setOperationDate((Date)movementObjet[2]);
			holderAccountMovement.setIdSourceParticipant(((BigDecimal)movementObjet[3]).longValue());
			holderAccountMovement.setIdTargetParticipant(((BigDecimal)movementObjet[4]).longValue());
			holderAccountMovement.setMovementQuantity((BigDecimal)movementObjet[5]);
			holderAccountMovement.setOperationPrice((BigDecimal)(movementObjet[6]==null?BigDecimal.ZERO:movementObjet[6]));
			holderAccountMovement.setIdModality(((BigDecimal)(movementObjet[7]==null?BigDecimal.ZERO:movementObjet[7])).longValue());
			holderAccountMovement.setMovementDate((Date)movementObjet[8]);
			holderAccountMovement.setIdMechanism(((BigDecimal)(movementObjet[9]==null?BigDecimal.ZERO:movementObjet[9])).longValue());
			
			holderAccountMovement.setMovementName(((String)movementObjet[10]));
			movDetail.add((String)movementObjet[11]);
			holderAccountMovement.setMovementDetail(movDetail);
			
			holderAccountMovementList.add(holderAccountMovement);
		}
		return holderAccountMovementList;
	}
	
	/**
	 * Gets the mechanism modality list service bean.
	 *
	 * @return the mechanism modality list service bean
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<NegotiationModality> getMechanismModalityListServiceBean() throws ServiceException{
		String querySql = "SELECT nm.ID_NEGOTIATION_MODALITY_PK, nm.MODALITY_NAME FROM NEGOTIATION_MODALITY nm";
		Query queryJpql = em.createNativeQuery(querySql);
		List<Object[]> objectData = null;
		try{
			objectData = (List<Object[]>)queryJpql.getResultList();
		}catch(NoResultException ex){
			return null;
		}
		List<NegotiationModality> negotiationModalityList = new ArrayList<NegotiationModality>();
		for(Object[] objectRow: objectData){
			NegotiationModality negotiationModality = new NegotiationModality();
			negotiationModality.setIdNegotiationModalityPk(((BigDecimal)objectRow[0]).longValue());
			negotiationModality.setModalityName((String)objectRow[1]);
			negotiationModalityList.add(negotiationModality);
		}
		return negotiationModalityList;
	}
	
	/**
	 * Gets the negotiation mechanism service.
	 *
	 * @return the negotiation mechanism service
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<NegotiationMechanism> getNegotiationMechanismService() throws ServiceException{
		String querySql = "SELECT negMec FROM NegotiationMechanism negMec";
		Query query = em.createQuery(querySql);
		try{
			return query.getResultList();
		}catch(NoResultException ex){
			return new ArrayList<NegotiationMechanism>();
		}
	}
	
	/**
	 * Find market fact balance help.
	 *
	 * @param marketFacTO the market fac to
	 * @return the market fact balance to
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public MarketFactBalanceTO findMarketFactBalanceHelp(MarketFactBalanceTO marketFacTO) throws ServiceException{
		Map<String,Object> parameters = new HashMap<String,Object>();					
		StringBuilder querySql = new StringBuilder();
		querySql.append("Select hab.security.idSecurityCodePk, hab.participant.idParticipantPk, hab.holderAccount.idHolderAccountPk, hab.security.description,   ");//0,1,2,3
		querySql.append("		hab.security.instrumentType, (Select p.parameterName From ParameterTable p Where p.parameterTablePk = hab.security.instrumentType),  ");//4,5
		querySql.append("		mark.IdMarketfactBalancePk, mark.marketDate, mark.marketPrice, mark.marketRate, mark.totalBalance, mark.availableBalance, ");//6,7,8,9,10,11
		querySql.append("		hab.totalBalance, mark.pawnBalance, mark.banBalance, mark.otherBlockBalance, ");//12,13,14,15
		querySql.append("		mark.accreditationBalance, mark.borrowerBalance, mark.purchaseBalance, mark.lenderBalance,	"); //16,17,18,19
		querySql.append("		mark.loanableBalance, mark.marginBalance, mark.oppositionBalance, mark.reportedBalance,	"); //20,21,22,23
		querySql.append("		mark.reportingBalance, mark.reserveBalance, mark.saleBalance, mark.transitBalance	"); //24,25,26,27
		querySql.append("From 	HolderAccountBalance hab	");
		querySql.append("Left 	Join hab.holderMarketfactBalance mark	");
		querySql.append("Where 	hab.security.idSecurityCodePk = :securityCode And	");
		querySql.append("	   	hab.participant.idParticipantPk = :participantCode And	");
		querySql.append("	   	hab.holderAccount.idHolderAccountPk = :holderAccountCode And	");
		querySql.append("	   	mark.indActive = :oneParam	");		
		if(marketFacTO.getBalanceType().equals(ParameterBalanceType.TOTAL_BALANCE.getCode())){
			querySql.append("	AND mark.totalBalance > 0	");
		}else if(marketFacTO.getBalanceType().equals(ParameterBalanceType.AVAILABLE_BALANCE.getCode())){
			querySql.append("	AND mark.availableBalance > 0	");
		}else if(marketFacTO.getBalanceType().equals(ParameterBalanceType.TRANSIT_BALANCE.getCode())){
			querySql.append("	AND mark.transitBalance > 0	");
		}else if(marketFacTO.getBalanceType().equals(ParameterBalanceType.PAWN_BALANCE.getCode())){
			querySql.append("	AND mark.pawnBalance > 0	");
		}else if(marketFacTO.getBalanceType().equals(ParameterBalanceType.BAN_BALANCE.getCode())){
			querySql.append("	AND mark.banBalance > 0	");
		}else if(marketFacTO.getBalanceType().equals(ParameterBalanceType.OTHERS_BALANCE.getCode())){
			querySql.append("	AND mark.otherBlockBalance > 0	");
		}else if(marketFacTO.getBalanceType().equals(ParameterBalanceType.RESERVE_BALANCE.getCode())){
			querySql.append("	AND mark.reserveBalance > 0	");
		}else if(marketFacTO.getBalanceType().equals(ParameterBalanceType.OPPOSITION_BALANCE.getCode())){
			querySql.append("	AND mark.oppositionBalance > 0	");
		}else if(marketFacTO.getBalanceType().equals(ParameterBalanceType.ACCREDITATION_BALANCE.getCode())){
			querySql.append("	AND mark.accreditationBalance > 0	");
		}else if(marketFacTO.getBalanceType().equals(ParameterBalanceType.REPORTING_BALANCE.getCode())){
			querySql.append("	AND mark.reportingBalance > 0	");
		}else if(marketFacTO.getBalanceType().equals(ParameterBalanceType.REPORTED_BALANCE.getCode())){
			querySql.append("	AND mark.reportedBalance > 0	");
		}else if(marketFacTO.getBalanceType().equals(ParameterBalanceType.MARGIN_BALANCE.getCode())){
			querySql.append("	AND mark.marginBalance > 0	");
		}else if(marketFacTO.getBalanceType().equals(ParameterBalanceType.PURCHASE_BALANCE.getCode())){
			querySql.append("	AND mark.purchaseBalance > 0	");
		}else if(marketFacTO.getBalanceType().equals(ParameterBalanceType.SALE_BALANCE.getCode())){
			querySql.append("	AND mark.saleBalance > 0	");
		}else if(marketFacTO.getBalanceType().equals(ParameterBalanceType.LENDER_BALANCE.getCode())){
			querySql.append("	AND mark.lenderBalance > 0	");
		}else if(marketFacTO.getBalanceType().equals(ParameterBalanceType.LOANABLE_BALANCE.getCode())){
			querySql.append("	AND mark.loanableBalance > 0	");
		}else if(marketFacTO.getBalanceType().equals(ParameterBalanceType.BORROWER_BALANCE.getCode())){
			querySql.append("	AND mark.borrowerBalance > 0	");
		}				
		querySql.append("Order By mark.marketDate, mark.marketPrice, mark.marketRate DESC	");
		parameters.put("securityCode", marketFacTO.getSecurityCodePk());
		parameters.put("participantCode", marketFacTO.getParticipantPk());
		parameters.put("holderAccountCode", marketFacTO.getHolderAccountPk());
		parameters.put("oneParam", BooleanType.YES.getCode());		
		return populateMarketFactBalanceTO(this.findListByQueryString(querySql.toString(), parameters), marketFacTO);
	}
	
	/**
	 * Find market fact by balance.
	 *
	 * @param marketFacTO the market fac to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccountMovInfoTO> findMarketFactByBalance(MarketFactBalanceTO marketFacTO) throws ServiceException{

		StringBuffer sbQuery = new StringBuffer();

		sbQuery.append(" Select ham ");
		sbQuery.append(" From HolderAccountMovement ham ");
		sbQuery.append(" Inner join fetch ham.custodyOperation cop ");
		sbQuery.append(" where ham.holderAccountBalance.participant.idParticipantPk = :participant ");
		sbQuery.append(" and ham.holderAccountBalance.holderAccount.idHolderAccountPk = :holderAccount ");
		sbQuery.append(" and ham.holderAccountBalance.security.idSecurityCodePk = :securityCode ");
		sbQuery.append(" and ham.movementType.idMovementTypePk = :movementType ");
		sbQuery.append(" Order By ham.movementDate asc");
		
		Query query = em.createQuery(sbQuery.toString());
		
		query.setParameter("participant", marketFacTO.getParticipantPk());        //103
		query.setParameter("holderAccount", marketFacTO.getHolderAccountPk());    //17014
		query.setParameter("securityCode", marketFacTO.getSecurityCodePk());      //BTX-BTXN05J3812
		query.setParameter("movementType", marketFacTO.getMovementBalanceType()); //300101		
		
		List<HolderAccountMovement> result = query.getResultList(); 
		List<HolderAccountMovInfoTO> listHolderAccountMovInfoTO = new  ArrayList<HolderAccountMovInfoTO>();
		
		for(HolderAccountMovement lista : result){
			HolderAccountMovInfoTO holderAccountMovInfoTO = new HolderAccountMovInfoTO();
			holderAccountMovInfoTO.setMarketDate(lista.getMovementDate());
			holderAccountMovInfoTO.setBlockOperation(lista.getCustodyOperation().getIdCustodyOperationPk().toString());
			holderAccountMovInfoTO.setMovementQuantity(lista.getMovementQuantity());
			listHolderAccountMovInfoTO.add(holderAccountMovInfoTO);
		}
		return listHolderAccountMovInfoTO;
	
	}
	
	/**
	 * Find market fact by accreditation.
	 *
	 * @param marketFacTO the market fac to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccountMovInfoTO> findMarketFactByAccreditation(MarketFactBalanceTO marketFacTO) throws ServiceException{

		StringBuffer sbQuery = new StringBuffer();

		sbQuery.append(" Select acd ");
		sbQuery.append(" From AccreditationDetail acd ");
		sbQuery.append(" Inner join fetch acd.accreditationOperation aco ");
		sbQuery.append(" Inner join fetch aco.custodyOperation cus ");
		sbQuery.append(" where acd.idParticipantFk.idParticipantPk = :participant ");
		sbQuery.append(" and acd.security.idSecurityCodePk = :securityCode ");
		sbQuery.append(" and acd.holderAccount.idHolderAccountPk = :holderAccount ");
		sbQuery.append(" and aco.accreditationState = :accreditationState ");
		sbQuery.append(" Order By cus.operationDate asc");
		Query query = em.createQuery(sbQuery.toString());
		
		query.setParameter("participant", marketFacTO.getParticipantPk());        //103
		query.setParameter("holderAccount", marketFacTO.getHolderAccountPk());    //17014
		query.setParameter("securityCode", marketFacTO.getSecurityCodePk());      //BTX-BTXN05J3812
		query.setParameter("accreditationState", marketFacTO.getAccreditationOperationState()); //456		
		
		List<AccreditationDetail> result = query.getResultList(); 
		List<HolderAccountMovInfoTO> listHolderAccountMovInfoTO = new  ArrayList<HolderAccountMovInfoTO>();
		
		for(AccreditationDetail lista : result){
			HolderAccountMovInfoTO holderAccountMovInfoTO = new HolderAccountMovInfoTO();
			holderAccountMovInfoTO.setMarketDate(lista.getAccreditationOperation().getCustodyOperation().getOperationDate());
			holderAccountMovInfoTO.setBlockOperation(lista.getAccreditationOperation().getCustodyOperation().getOperationNumber().toString());
			holderAccountMovInfoTO.setMovementQuantity(lista.getAvailableBalance());
			listHolderAccountMovInfoTO.add(holderAccountMovInfoTO);
		}
		return listHolderAccountMovInfoTO;
	
	}
	
	/**
	 * Populate market fact balance to.
	 *
	 * @param objectDataList the object data list
	 * @param marketBalance the market balance
	 * @return the market fact balance to
	 * @throws ServiceException the service exception
	 */
	private MarketFactBalanceTO populateMarketFactBalanceTO(List<Object[]> objectDataList, MarketFactBalanceTO marketBalance) throws ServiceException{
		MarketFactBalanceTO marketBalanceTO = new MarketFactBalanceTO();						
		marketBalanceTO.setMarketFacBalances(new ArrayList<MarketFactDetailTO>());	
		marketBalanceTO.setBalanceType(marketBalance.getBalanceType());
		for(Object[] rowData: objectDataList){			
			//if object it's null need to set general data
			if(marketBalanceTO.getSecurityCodePk()==null ){
				marketBalanceTO.setSecurityCodePk((String) rowData[0]);
				marketBalanceTO.setParticipantPk((Long) rowData[1]);
				marketBalanceTO.setHolderAccountPk((Long) rowData[2]);
				marketBalanceTO.setSecurityDescription((String) rowData[3]);				
				marketBalanceTO.setInstrumentType((Integer) rowData[4]);
				marketBalanceTO.setInstrumentDescription((String) rowData[5]);				
			}			
			//adding the detail of MarketFact
			MarketFactDetailTO marketDetailTO = null;
			Object holderMarketFact = rowData[6];
			if(holderMarketFact!=null){
				marketDetailTO = new MarketFactDetailTO();
				marketDetailTO.setMarketFactBalancePk((Long) holderMarketFact);
				marketDetailTO.setMarketDate((Date) rowData[7]);
				marketDetailTO.setMarketFactBalanceTO(marketBalanceTO);				
				if(marketBalanceTO.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode())){
					marketDetailTO.setMarketRate((BigDecimal) rowData[9]);
				}else if(marketBalanceTO.getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode())){
					marketDetailTO.setMarketPrice((BigDecimal) rowData[8]);
				}				
				marketDetailTO.setTotalBalance((BigDecimal) rowData[10]);
				marketDetailTO.setAvailableBalance((BigDecimal) rowData[11]);
				marketDetailTO.setTransitBalance((BigDecimal) rowData[27]);
				marketDetailTO.setPawnBalance((BigDecimal) rowData[13]);
				marketDetailTO.setBanBalance((BigDecimal) rowData[14]);
				marketDetailTO.setOtherBanBalance((BigDecimal) rowData[15]);
				
				marketDetailTO.setReserveBalance((BigDecimal) rowData[25]);
				marketDetailTO.setOppositionBalance((BigDecimal) rowData[22]);
				marketDetailTO.setAccreditationBalance((BigDecimal) rowData[16]);
				marketDetailTO.setReportingBalance((BigDecimal) rowData[24]);
				marketDetailTO.setReportedBalance((BigDecimal) rowData[23]);
				marketDetailTO.setMarginBalance((BigDecimal) rowData[21]);
				marketDetailTO.setPurchaseBalance((BigDecimal) rowData[18]);
				marketDetailTO.setSaleBalance((BigDecimal) rowData[26]);
				marketDetailTO.setLenderBalance((BigDecimal) rowData[19]);
				marketDetailTO.setLonableBalance((BigDecimal) rowData[20]);
				marketDetailTO.setBorrowerBalance((BigDecimal) rowData[17]);														
			}			
			if(marketDetailTO!=null){
				if(marketBalanceTO.getLastMarketDate()==null){
					marketBalanceTO.setLastMarketDate(marketDetailTO.getMarketDate());
					marketBalanceTO.setLastMarketPrice(marketDetailTO.getMarketPrice());
					marketBalanceTO.setLastMarketRate(marketDetailTO.getMarketRate());
				}
				marketBalanceTO.getMarketFacBalances().add(marketDetailTO);
			}
		}		
		return marketBalanceTO;
	}
	
	/**
	 * Gets the movement marketFact service.
	 *
	 * @param holderAccountBalMovDetResultTO the holder account bal mov det result to
	 * @return the movement marketFact service
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public MarketFactBalanceTO getHolderMarketFactMovementService(HolderAccountBalMovDetResultTO holderAccountBalMovDetResultTO ) throws ServiceException{				
		Map<String,Object> parameters = new HashMap<String,Object>();	
		StringBuilder querySqlMov = new StringBuilder();
		querySqlMov.append("Select 	maMov.securities.idSecurityCodePk, maMov.participant.idParticipantPk, maMov.holderAccount.idHolderAccountPk, maMov.securities.description,   ");//0,1,2,3
		querySqlMov.append("		maMov.securities.instrumentType, (Select p.parameterName From ParameterTable p Where p.parameterTablePk = maMov.securities.instrumentType),  ");//4,5
		querySqlMov.append("		maMov.idMarketFactMovementPk, maMov.marketDate, maMov.marketPrice, maMov.marketRate, maMov.movementQuantity	"); //6,7,8,9,10
		querySqlMov.append("From 	HolderMarketFactMovement maMov	");
		querySqlMov.append("Where 	maMov.holderAccountMovement.idHolderAccountMovementPk = :movPk	");	
		parameters.put("movPk", holderAccountBalMovDetResultTO.getIdHolderAccountMovementPk());
		return populateMarketFactBalanceMovementTO(this.findListByQueryString(querySqlMov.toString(), parameters));
	}
	
	/**
	 * Populate market fact balance movement to.
	 *
	 * @param objectDataList the object data list
	 * @return the market fact balance to
	 * @throws ServiceException the service exception
	 */
	private MarketFactBalanceTO populateMarketFactBalanceMovementTO(List<Object[]> objectDataList) throws ServiceException{
		MarketFactBalanceTO marketBalanceTO = new MarketFactBalanceTO();						
		marketBalanceTO.setMarketFacBalances(new ArrayList<MarketFactDetailTO>());			
		for(Object[] rowData: objectDataList){			
			if(marketBalanceTO.getSecurityCodePk()==null ){
				marketBalanceTO.setSecurityCodePk((String) rowData[0]);
				marketBalanceTO.setParticipantPk((Long) rowData[1]);
				marketBalanceTO.setHolderAccountPk((Long) rowData[2]);
				marketBalanceTO.setSecurityDescription((String) rowData[3]);				
				marketBalanceTO.setInstrumentType((Integer) rowData[4]);
				marketBalanceTO.setInstrumentDescription((String) rowData[5]);				
			}			
			MarketFactDetailTO marketDetailTO = null;
			Object holderMarketFact = rowData[6];
			if(holderMarketFact!=null){
				marketDetailTO = new MarketFactDetailTO();
				marketDetailTO.setMarketFactBalancePk((Long) holderMarketFact);
				marketDetailTO.setMarketDate((Date) rowData[7]);
				marketDetailTO.setMarketFactBalanceTO(marketBalanceTO);				
				if(marketBalanceTO.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode())){
					marketDetailTO.setMarketRate((BigDecimal) rowData[9]);
				}else if(marketBalanceTO.getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode())){
					marketDetailTO.setMarketPrice((BigDecimal) rowData[8]);
				}						
				marketDetailTO.setMovementQuantity((BigDecimal) rowData[10]);																	
			}			
			if(marketDetailTO!=null){
				if(marketBalanceTO.getLastMarketDate()==null){
					marketBalanceTO.setLastMarketDate(marketDetailTO.getMarketDate());
					marketBalanceTO.setLastMarketPrice(marketDetailTO.getMarketPrice());
					marketBalanceTO.setLastMarketRate(marketDetailTO.getMarketRate());
				}
				marketBalanceTO.getMarketFacBalances().add(marketDetailTO);
			}
		}		
		return marketBalanceTO;
	}
	
	/**
	 * Gets the holder account detail sb.
	 *
	 * @param holderAccountPk the holder account pk
	 * @return the holder account detail sb
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getHolderAccountDetailSB(Long holderAccountPk) throws ServiceException{
		StringBuilder querySql = new StringBuilder();
		querySql.append(" SELECT  ");
		querySql.append(" h.holderType, h.fullName, h.name, h.firstLastName, h.secondLastName, h.idHolderPk ");
		querySql.append(" FROM HolderAccountDetail had inner join had.holderAccount ha ");
		querySql.append(" inner join had.holder h where ha.idHolderAccountPk=:idHolderAccount ");

		Query query = em.createQuery(querySql.toString());
		query.setParameter("idHolderAccount", holderAccountPk);
		try{
			return query.getResultList();
		}catch(NoResultException ex){
			return new ArrayList<Object[]>();
		}
	}
}