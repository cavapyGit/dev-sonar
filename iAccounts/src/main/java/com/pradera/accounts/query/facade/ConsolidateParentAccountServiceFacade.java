package com.pradera.accounts.query.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.pradera.accounts.query.service.ConsolidateParentAccountServiceBean;
import com.pradera.accounts.query.to.ConsolidateParentAccountResultTO;
import com.pradera.accounts.query.to.ConsolidateParentAccountTO;
import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ConsolidateParentAccountServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/08/2015
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@Performance
public class ConsolidateParentAccountServiceFacade {

	/** The consolidate parent account service bean. */
	@EJB
	ConsolidateParentAccountServiceBean consolidateParentAccountServiceBean;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;

	
	/**
	 * Search consolidate parent account.
	 *
	 * @param consolidateParentAccountTO the consolidate parent account to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ConsolidateParentAccountResultTO> searchConsolidateParentAccount(ConsolidateParentAccountTO consolidateParentAccountTO) throws ServiceException{
		List<ConsolidateParentAccountResultTO> consolidateParentAccountResultTOList = new ArrayList<ConsolidateParentAccountResultTO>();
//		Map<Integer,ConsolidateParentAccountResultTO> mapHolderDesc = new HashMap<Integer,ConsolidateParentAccountResultTO>();
		
		Map<Integer,String> accountStateDescription = new HashMap<Integer,String>();
		ParameterTableTO  parameterTableTO = new ParameterTableTO();
		
		parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
		for(ParameterTable param : generalParametersFacade.getListParameterTableServiceBean(parameterTableTO)){
			accountStateDescription.put(param.getParameterTablePk(), param.getParameterName());
		}
			
		int i=0;
		int l;
		boolean r;
		for(Object[] objectList : consolidateParentAccountServiceBean.searchConsolidateParentAccount(consolidateParentAccountTO)){
			ConsolidateParentAccountResultTO consolidateParentAccountResultTO = new ConsolidateParentAccountResultTO();
			Integer accountNumber;
			//GETTING ACCOUNT NUMBER
			accountNumber = Integer.valueOf(objectList[1].toString());
			int posChar=0;
			if(Validations.validateIsNotNullAndPositive(accountNumber)){
				//CONDITIONAL TO SETTING TRADE NAME OR FULL NAME
				consolidateParentAccountResultTO = new ConsolidateParentAccountResultTO();
				
				//SETTING ACCOUNT NUMBER
				consolidateParentAccountResultTO.setAccountNumber(accountNumber);
				consolidateParentAccountResultTO.setAlternativeCode(objectList[2].toString());	
				consolidateParentAccountResultTO.setAccountStateDescription(accountStateDescription.get(Integer.valueOf(objectList[3].toString())));
				consolidateParentAccountResultTO.setIdParticipantPk(Long.valueOf(objectList[4].toString()));
				consolidateParentAccountResultTO.setIdHolderAccountPk(Long.valueOf(objectList[5].toString()));
				consolidateParentAccountResultTO.setIdSecurityCodePk(objectList[6].toString());
				consolidateParentAccountResultTO.setAccountingBalance(new BigDecimal(objectList[7].toString()));
				consolidateParentAccountResultTO.setAvailableBalance(new BigDecimal(objectList[8].toString()));
				consolidateParentAccountResultTO.setBlockBalance(new BigDecimal(objectList[9].toString()));
				if(objectList[10]!=null){
					consolidateParentAccountResultTO.setParticipantMnemonic(objectList[10].toString());
				}
				consolidateParentAccountResultTO.setIssuerMnemonic(objectList[11].toString());
				consolidateParentAccountResultTO.setIdHolderPk(Long.valueOf(objectList[12].toString()));
				consolidateParentAccountResultTO.setFullNameChain("* "+objectList[13].toString()+ "\n" + "prueba");
				consolidateParentAccountResultTO.getFullName().addAll(consolidateParentAccountServiceBean.searchHolderAccountDescription(consolidateParentAccountResultTO));
				posChar=objectList[2].toString().indexOf('-')+1; 
				consolidateParentAccountResultTO.setAlternativeCodeIssuer(objectList[2].toString().substring(posChar));
				consolidateParentAccountResultTO.setCurrentNominalValue(new BigDecimal(objectList[14].toString()));
//				consolidateParentAccountResultTO.setFullNameChain(objectList[13].toString());
//				
				consolidateParentAccountResultTOList.add(consolidateParentAccountResultTO);
				
//				if(i==0) {
//					consolidateParentAccountResultTOList.add(consolidateParentAccountResultTO);
//					i++;
//				}else {
//					l=0;
//					r = false;
//					for(ConsolidateParentAccountResultTO sinChequear : consolidateParentAccountResultTOList) {
//						if(consolidateParentAccountResultTO.getIdSecurityCodePk().equals(sinChequear.getIdSecurityCodePk())  && consolidateParentAccountResultTO.getAlternativeCode().equals(sinChequear.getAlternativeCode())) {
//							consolidateParentAccountResultTOList.get(l).setFullNameChain(consolidateParentAccountResultTOList.get(l).getFullNameChain() + "\n" + consolidateParentAccountResultTO.getFullNameChain());
//							consolidateParentAccountResultTOList.get(l).getFullName().add(consolidateParentAccountResultTO.getIdHolderPk()+"-"+objectList[13].toString());
//							r = true;
//							break;
//						}
//						l++;
//					}
//					if(r==false) {
//						consolidateParentAccountResultTOList.add(consolidateParentAccountResultTO);
//					}
//				}
					
			}
		}
		return consolidateParentAccountResultTOList;
	}
}
