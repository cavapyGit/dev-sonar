package com.pradera.accounts.query.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ConsolidateParentAccountResultTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/08/2015
 */
public class ConsolidateParentAccountResultTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The id holder account pk. */
	private Long idHolderAccountPk;
	
	/** The id security code pk. */
	private String idSecurityCodePk;
	
	/** The account number. */
	private Integer accountNumber;
	
	/** The inversionist full name. */
	private List<String> inversionistFullName;
	
	/** The alternative code. */
	private String alternativeCode;
	
	/** The account state description. */
	private String accountStateDescription;
	
	/** The id issuer pk. */
	private String idIssuerPK;
	
	/** The accounting balance. */
	private BigDecimal accountingBalance;
	
	/** The available balance. */
	private BigDecimal availableBalance;
	
	/** The block balance. */
	private BigDecimal blockBalance;
	
	/** The issuer mnemonic. */
	private String issuerMnemonic;
	
	/** The participant mnemonic. */
	private String participantMnemonic;
	
	/** The id holder pk. */
	private Long idHolderPk;
	
	/** The alternative code issuer. */
	private String alternativeCodeIssuer;
	
	private List<String> fullName = new ArrayList<String>();
	
	private String fullNameChain;
	
	private BigDecimal currentNominalValue;
	
	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	//GETTERS AND SETTERS
	public Integer getAccountNumber() {
		return accountNumber;
	}
	
	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	/**
	 * Gets the inversionist full name.
	 *
	 * @return the inversionist full name
	 */
	public List<String> getInversionistFullName() {
		return inversionistFullName;
	}
	
	/**
	 * Sets the inversionist full name.
	 *
	 * @param inversionistFullName the new inversionist full name
	 */
	public void setInversionistFullName(List<String> inversionistFullName) {
		this.inversionistFullName = inversionistFullName;
	}
	
	/**
	 * Gets the alternative code.
	 *
	 * @return the alternative code
	 */
	public String getAlternativeCode() {
		return alternativeCode;
	}
	
	/**
	 * Sets the alternative code.
	 *
	 * @param alternativeCode the new alternative code
	 */
	public void setAlternativeCode(String alternativeCode) {
		this.alternativeCode = alternativeCode;
	}
	
	/**
	 * Gets the account state description.
	 *
	 * @return the account state description
	 */
	public String getAccountStateDescription() {
		return accountStateDescription;
	}
	
	/**
	 * Sets the account state description.
	 *
	 * @param accountStateDescription the new account state description
	 */
	public void setAccountStateDescription(String accountStateDescription) {
		this.accountStateDescription = accountStateDescription;
	}
	
	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	
	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	
	/**
	 * Gets the id holder account pk.
	 *
	 * @return the id holder account pk
	 */
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}
	
	/**
	 * Sets the id holder account pk.
	 *
	 * @param idHolderAccountPk the new id holder account pk
	 */
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}
	
	/**
	 * Gets the id security code pk.
	 *
	 * @return the id security code pk
	 */
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}
	
	/**
	 * Sets the id security code pk.
	 *
	 * @param idSecurityCodePk the new id security code pk
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
	
	/**
	 * Gets the id issuer pk.
	 *
	 * @return the id issuer pk
	 */
	public String getIdIssuerPK() {
		return idIssuerPK;
	}
	
	/**
	 * Gets the accounting balance.
	 *
	 * @return the accounting balance
	 */
	public BigDecimal getAccountingBalance() {
		return accountingBalance;
	}
	
	/**
	 * Gets the block balance.
	 *
	 * @return the block balance
	 */
	public BigDecimal getBlockBalance() {
		return blockBalance;
	}
	
	/**
	 * Sets the id issuer pk.
	 *
	 * @param idIssuerPK the new id issuer pk
	 */
	public void setIdIssuerPK(String idIssuerPK) {
		this.idIssuerPK = idIssuerPK;
	}
	
	/**
	 * Sets the accounting balance.
	 *
	 * @param accountingBalance the new accounting balance
	 */
	public void setAccountingBalance(BigDecimal accountingBalance) {
		this.accountingBalance = accountingBalance;
	}
	
	/**
	 * Sets the block balance.
	 *
	 * @param blockBalance the new block balance
	 */
	public void setBlockBalance(BigDecimal blockBalance) {
		this.blockBalance = blockBalance;
	}
	
	/**
	 * Gets the available balance.
	 *
	 * @return the available balance
	 */
	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}
	
	/**
	 * Sets the available balance.
	 *
	 * @param availableBalance the new available balance
	 */
	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}
	
	/**
	 * Gets the issuer mnemonic.
	 *
	 * @return the issuer mnemonic
	 */
	public String getIssuerMnemonic() {
		return issuerMnemonic;
	}
	
	/**
	 * Gets the participant mnemonic.
	 *
	 * @return the participant mnemonic
	 */
	public String getParticipantMnemonic() {
		return participantMnemonic;
	}
	
	/**
	 * Sets the issuer mnemonic.
	 *
	 * @param issuerMnemonic the new issuer mnemonic
	 */
	public void setIssuerMnemonic(String issuerMnemonic) {
		this.issuerMnemonic = issuerMnemonic;
	}
	
	/**
	 * Sets the participant mnemonic.
	 *
	 * @param participantMnemonic the new participant mnemonic
	 */
	public void setParticipantMnemonic(String participantMnemonic) {
		this.participantMnemonic = participantMnemonic;
	}
	
	/**
	 * Gets the id holder pk.
	 *
	 * @return the id holder pk
	 */
	public Long getIdHolderPk() {
		return idHolderPk;
	}
	
	/**
	 * Sets the id holder pk.
	 *
	 * @param idHolderPk the new id holder pk
	 */
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}
	
	/**
	 * Gets the alternative code issuer.
	 *
	 * @return the alternativeCodeIssuer
	 */
	public String getAlternativeCodeIssuer() {
		return alternativeCodeIssuer;
	}
	
	/**
	 * Sets the alternative code issuer.
	 *
	 * @param alternativeCodeIssuer the alternativeCodeIssuer to set
	 */
	public void setAlternativeCodeIssuer(String alternativeCodeIssuer) {
		this.alternativeCodeIssuer = alternativeCodeIssuer;
	}

	public List<String> getFullName() {
		return fullName;
	}

	public void setFullName(List<String> fullName) {
		this.fullName = fullName;
	}

	public String getFullNameChain() {
		return fullNameChain;
	}

	public void setFullNameChain(String fullNameChain) {
		this.fullNameChain = fullNameChain;
	}

	public BigDecimal getCurrentNominalValue() {
		return currentNominalValue;
	}

	public void setCurrentNominalValue(BigDecimal currentNominalValue) {
		this.currentNominalValue = currentNominalValue;
	}

	/*public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}*/
	
}
