package com.pradera.accounts.query.to;

import java.io.Serializable;
import java.util.Date;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.Issuer;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class IssuerHolderQueryTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/08/2015
 */
public class IssuerHolderQueryTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The issuer helper mgmt. */
	private Issuer issuerHelperMgmt;
	
	/** The holder helper mgmt. */
	private Holder holderHelperMgmt;
	
	/** The id instrument type. */
	private Integer idInstrumentType;
	
	/** The id offer type. */
	private Integer idOfferType;
	
	/** The id security class. */
	private Integer idSecurityClass;
	
	/** The balance no. */
	private boolean balanceYes , balanceNo;
	
	/** The lst issuer holder detail to. */
	private GenericDataModel<IssuerHolderDetailTO> lstIssuerHolderDetailTO;
	
	/** The issuer holder detail to. */
	private IssuerHolderDetailTO issuerHolderDetailTO;
	
	/** The holder account. */
	private HolderAccount holderAccount;
	
	/** The id security code. */
	private String idSecurityCode;
	
	/**
	 * Instantiates a new issuer holder query to.
	 */
	public IssuerHolderQueryTO(){
		
	}
	
	/**
	 * Gets the issuer helper mgmt.
	 *
	 * @return the issuer helper mgmt
	 */
	public Issuer getIssuerHelperMgmt() {
		return issuerHelperMgmt;
	}

	/**
	 * Sets the issuer helper mgmt.
	 *
	 * @param issuerHelperMgmt the new issuer helper mgmt
	 */
	public void setIssuerHelperMgmt(Issuer issuerHelperMgmt) {
		this.issuerHelperMgmt = issuerHelperMgmt;
	}
	
	/**
	 * Gets the holder helper mgmt.
	 *
	 * @return the holder helper mgmt
	 */
	public Holder getHolderHelperMgmt() {
		return holderHelperMgmt;
	}
	
	/**
	 * Sets the holder helper mgmt.
	 *
	 * @param holderHelperMgmt the new holder helper mgmt
	 */
	public void setHolderHelperMgmt(Holder holderHelperMgmt) {
		this.holderHelperMgmt = holderHelperMgmt;
	}
	
	/**
	 * Gets the id instrument type.
	 *
	 * @return the id instrument type
	 */
	public Integer getIdInstrumentType() {
		return idInstrumentType;
	} 
	
	/**
	 * Sets the id instrument type.
	 *
	 * @param idInstrumentType the new id instrument type
	 */
	public void setIdInstrumentType(Integer idInstrumentType) {
		this.idInstrumentType = idInstrumentType;
	}
	
	/**
	 * Gets the id offer type.
	 *
	 * @return the id offer type
	 */
	public Integer getIdOfferType() {
		return idOfferType;
	}
	
	/**
	 * Sets the id offer type.
	 *
	 * @param idOfferType the new id offer type
	 */
	public void setIdOfferType(Integer idOfferType) {
		this.idOfferType = idOfferType;
	}
	
	/**
	 * Gets the id security class.
	 *
	 * @return the id security class
	 */
	public Integer getIdSecurityClass() {
		return idSecurityClass;
	}
	
	/**
	 * Sets the id security class.
	 *
	 * @param idSecurityClass the new id security class
	 */
	public void setIdSecurityClass(Integer idSecurityClass) {
		this.idSecurityClass = idSecurityClass;
	}
	
	/**
	 * Checks if is balance yes.
	 *
	 * @return true, if is balance yes
	 */
	public boolean isBalanceYes() {
		return balanceYes;
	}
	
	/**
	 * Sets the balance yes.
	 *
	 * @param balanceYes the new balance yes
	 */
	public void setBalanceYes(boolean balanceYes) {
		this.balanceYes = balanceYes;
	}
	
	/**
	 * Checks if is balance no.
	 *
	 * @return true, if is balance no
	 */
	public boolean isBalanceNo() {
		return balanceNo;
	}
	
	/**
	 * Sets the balance no.
	 *
	 * @param balanceNo the new balance no
	 */
	public void setBalanceNo(boolean balanceNo) {
		this.balanceNo = balanceNo;
	}

	/**
	 * Gets the issuer holder detail to.
	 *
	 * @return the issuer holder detail to
	 */
	public IssuerHolderDetailTO getIssuerHolderDetailTO() {
		return issuerHolderDetailTO;
	}
	
	/**
	 * Sets the issuer holder detail to.
	 *
	 * @param issuerHolderDetailTO the new issuer holder detail to
	 */
	public void setIssuerHolderDetailTO(IssuerHolderDetailTO issuerHolderDetailTO) {
		this.issuerHolderDetailTO = issuerHolderDetailTO;
	}
	
	/**
	 * Gets the lst issuer holder detail to.
	 *
	 * @return the lst issuer holder detail to
	 */
	public GenericDataModel<IssuerHolderDetailTO> getLstIssuerHolderDetailTO() {
		return lstIssuerHolderDetailTO;
	}
	
	/**
	 * Sets the lst issuer holder detail to.
	 *
	 * @param lstIssuerHolderDetailTO the new lst issuer holder detail to
	 */
	public void setLstIssuerHolderDetailTO(
			GenericDataModel<IssuerHolderDetailTO> lstIssuerHolderDetailTO) {
		this.lstIssuerHolderDetailTO = lstIssuerHolderDetailTO;
	}
	
	/**
	 * Gets the holder account.
	 *
	 * @return the holderAccount
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}
	
	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the holderAccount to set
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}
	
	/**
	 * Gets the id security code.
	 *
	 * @return the idSecurityCode
	 */
	public String getIdSecurityCode() {
		return idSecurityCode;
	}
	
	/**
	 * Sets the id security code.
	 *
	 * @param idSecurityCode the idSecurityCode to set
	 */
	public void setIdSecurityCode(String idSecurityCode) {
		this.idSecurityCode = idSecurityCode;
	}
}
