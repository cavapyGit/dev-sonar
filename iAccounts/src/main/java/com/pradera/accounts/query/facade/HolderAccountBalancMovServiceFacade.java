package com.pradera.accounts.query.facade; 

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.pradera.account.enquiries.HolderAccountMovInfoTO;
import com.pradera.accounts.query.service.HolderAccountBalancMovServiceBean;
import com.pradera.accounts.query.to.HolderAccountBalMovDetResultTO;
import com.pradera.core.component.operation.to.MarketFactBalanceTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class HolderAccountMgmtBean.
 * Consulta de saldos y movimientos
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/02/2013
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class HolderAccountBalancMovServiceFacade {
	
	/** The holder account balanc mov service bean. */
	@EJB
	HolderAccountBalancMovServiceBean holderAccountBalancMovServiceBean;
	/**
	 * Holder account balance service bean.
	 *
	 * @param holderAccountBalanceTO the holder account balance to
	 * @return the holder account balance
	 * @throws ServiceException the service exception
	 */
	public HolderAccountBalance holderAccountBalanceServiceFacade(HolderAccountBalanceTO holderAccountBalanceTO) throws ServiceException{
		return holderAccountBalancMovServiceBean.holderAccountBalanceServiceBean(holderAccountBalanceTO);
	}
	
	/**
	 * Holder market fact balance service facade.
	 *
	 * @param holderAccountBalanceTO the holder account balance to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderMarketFactBalance> holderMarketFactBalanceServiceFacade(HolderAccountBalanceTO holderAccountBalanceTO) throws ServiceException{
		return holderAccountBalancMovServiceBean.listHolderMarketFactBalanceServiceBean(holderAccountBalanceTO);
	}
	
	
	
	/**
	 * Gets the holder account mov list service facade.
	 *
	 * @param holderAccountBalanceTO the holder account balance to
	 * @return the holder account mov list service facade
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBalMovDetResultTO> getHolderAccountMovListServiceFacade(HolderAccountBalanceTO holderAccountBalanceTO) throws ServiceException{
		List<HolderAccountBalMovDetResultTO> holdAccountBalMovList = new ArrayList<HolderAccountBalMovDetResultTO>();
		List<HolderAccountBalMovDetResultTO> holdAccountList = holderAccountBalancMovServiceBean.getHolderAccountMovListServiceBean(holderAccountBalanceTO);
		for(HolderAccountBalMovDetResultTO holderAccountResult: getHolAccBalMovResultOrder(holdAccountList)){
			
			List<String> stringList = null;
			for(HolderAccountBalMovDetResultTO holderAccountResultAll: holdAccountList){
				if(holderAccountResult.equals(holderAccountResultAll) && holderAccountResultAll.getMovementDetail().get(0)!=null){//Verificamos que sea el objeto y exista tracking
					if(stringList==null)//Si aun no a sido inicializada
						stringList = new ArrayList<String>();
					stringList.add("</br>");
					stringList.add(holderAccountResultAll.getMovementDetail().get(0));
				}
			}
			holderAccountResult.setMovementDetail(stringList);
			holdAccountBalMovList.add(holderAccountResult);
		}
		return holdAccountBalMovList;
	}
	
	/**
	 * Gets the hol acc bal mov result order.
	 * metodo para retornar el detalle de cada movimiento.(SUMA,RESTA,etc)
	 * @param holdAccBalMovDetResultTO the hold acc bal mov det result to
	 * @return the hol acc bal mov result order
	 */
	private List<HolderAccountBalMovDetResultTO> getHolAccBalMovResultOrder(List<HolderAccountBalMovDetResultTO> holdAccBalMovDetResultTO){
		List<HolderAccountBalMovDetResultTO> holdAccBalMovReturn = new ArrayList<HolderAccountBalMovDetResultTO>();
		for(HolderAccountBalMovDetResultTO holderAccountResult: holdAccBalMovDetResultTO){
			if(holdAccBalMovReturn.indexOf(holderAccountResult)==-1){
				holdAccBalMovReturn.add(holderAccountResult);
			}
		}
		return holdAccBalMovReturn;
	}
	
	/**
	 * Gets the mechanism modality list service facade.
	 *
	 * @return the mechanism modality list service facade
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationModality> getMechanismModalityListServiceFacade() throws ServiceException{
		return holderAccountBalancMovServiceBean.getMechanismModalityListServiceBean();
	}
	
	/**
	 * Gets the negotiation mechanism facade.
	 *
	 * @return the negotiation mechanism facade
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationMechanism> getNegotiationMechanismFacade() throws ServiceException{
		return holderAccountBalancMovServiceBean.getNegotiationMechanismService();
	}
	
	/**
	 * Market Fact Balance service bean.
	 *
	 * @param marketFacTO the Market Fact Balance to
	 * @return the Market Fact Balance to
	 * @throws ServiceException the service exception
	 */
	public MarketFactBalanceTO findMarketFactBalanceHelp(MarketFactBalanceTO marketFacTO) throws ServiceException{
		return holderAccountBalancMovServiceBean.findMarketFactBalanceHelp(marketFacTO);
	}
	
	/**
	 * List market fact by balance.
	 *
	 * @param marketFacTO the market fac to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountMovInfoTO> listMarketFactByBalance(MarketFactBalanceTO marketFacTO) throws ServiceException{
		return holderAccountBalancMovServiceBean.findMarketFactByBalance(marketFacTO);
	}
	
	/**
	 * List market fact by accreditation.
	 *
	 * @param marketFacTO the market fac to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountMovInfoTO> listMarketFactByAccreditation(MarketFactBalanceTO marketFacTO) throws ServiceException{
		return holderAccountBalancMovServiceBean.findMarketFactByAccreditation(marketFacTO);
	}
	
	/**
	 * Market Fact Balance service bean.
	 *
	 * @param holderAccountBalMovDetResultTO the holder account bal mov det result to
	 * @return the Market Fact Balance to
	 * @throws ServiceException the service exception
	 */
	public MarketFactBalanceTO getHolderMarketFactMovementFacade(HolderAccountBalMovDetResultTO holderAccountBalMovDetResultTO) throws ServiceException{
		return holderAccountBalancMovServiceBean.getHolderMarketFactMovementService(holderAccountBalMovDetResultTO);
	}
	
	/**
	 * Gets the holder account detail sf.
	 *
	 * @param idHolderAccountPk the id holder account pk
	 * @return the holder account detail sf
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getHolderAccountDetailSF(Long idHolderAccountPk) throws ServiceException{
		return holderAccountBalancMovServiceBean.getHolderAccountDetailSB(idHolderAccountPk);
	}
}