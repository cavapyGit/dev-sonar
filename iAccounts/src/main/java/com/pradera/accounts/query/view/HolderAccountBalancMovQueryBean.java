package com.pradera.accounts.query.view; 

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.account.enquiries.HolderAccountMovInfoTO;
import com.pradera.accounts.holder.facade.HolderServiceFacade;
import com.pradera.accounts.query.facade.HolderAccountBalancMovServiceFacade;
import com.pradera.accounts.query.to.HolderAccountBalMovDetResultTO;
import com.pradera.accounts.util.view.PropertiesConstants;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.core.component.helperui.to.SecuritiesSearcherTO;
import com.pradera.core.component.helperui.view.SecuritiesPartHolAccount;
import com.pradera.core.component.operation.to.MarketFactBalanceTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.type.MovementBalanceType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.component.type.ParameterBalanceType;
import com.pradera.model.custody.type.AccreditationOperationStateType;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.type.NegotiationMechanismType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class HolderAccountMgmtBean.
 * Consulta de saldos y movimientos
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/02/2013
 */
@DepositaryWebBean
public class HolderAccountBalancMovQueryBean extends GenericBaseBean implements Serializable{
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The holder account. */
	private HolderAccount holderAccountRequest;
	
	/** The participant list. */
	private List<Participant> participantList;
	
	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
	/** The general parameters facade. */
	@EJB
	private GeneralParametersFacade generalParametersFacade;
	
	/** The holder account balance to. */
	private HolderAccountBalanceTO holderAccountBalanceTO;
	
	/** The holder account balance session. */
	private HolderAccountBalance holderAccountBalanceSession;
	
	/** The holder account movement data model. */
	private GenericDataModel<HolderAccountBalMovDetResultTO> holderAccountMovementDataModel;
	
	 /** The securities part hol account. */
 	@Inject
	SecuritiesPartHolAccount securitiesPartHolAccount;
 	
 	/** The holder account balanc mov service facade. */
	 @EJB
 	HolderAccountBalancMovServiceFacade holderAccountBalancMovServiceFacade;
	 
	/** The negotiation modality list. */
	private List<NegotiationModality> negotiationModalityList;
	
	/** The negotiation mechanism list. */
	private List<NegotiationMechanism> negotiationMechanismList;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The holder request. */
	private Holder holderRequest;
 	
	/** The holder service facade. */
	@EJB
	HolderServiceFacade holderServiceFacade;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The market fact balance. */
	private MarketFactBalanceTO marketFactBalance;
	
	/** The market fact balance movement. */
	private MarketFactBalanceTO marketFactBalanceMovement;
	
	/** The lst account to. */
	private GenericDataModel<HolderAccountHelperResultTO> lstAccountTo;
	
	/** The selected account to. */
	private HolderAccountHelperResultTO selectedAccountTO;
	
	/** The list holder market fact balance. */
	private List<HolderMarketFactBalance> listHolderMarketFactBalance;
	
	/** The holder market fact balance. */
	private HolderMarketFactBalance holderMarketFactBalance;
	
	/** The list holder account mov info to. */
	private List<HolderAccountMovInfoTO> listHolderAccountMovInfoTO;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	/**
	 * Instantiates a new holder account modify mgmt bean.
	 */
	public HolderAccountBalancMovQueryBean() {
		super();
	}
	
	/**
	 * Clean holder account balance.
	 */
	private void initHolderAccountBalanceTO(){
		this.holderAccountBalanceTO = new HolderAccountBalanceTO();
		this.holderAccountBalanceTO.setInitialDate(CommonsUtilities.currentDate());
		this.holderAccountBalanceTO.setFinalDate(CommonsUtilities.currentDate());
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			holderAccountBalanceTO.setIdParticipant(userInfo.getUserAccountSession().getParticipantCode());
		}
		securitiesPartHolAccount.setDescription(null);
	}
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		beginConversation();
		Participant participantTO = new Participant();
		holderRequest = new Holder();
		try {
			setParticipantList(accountsFacade.getLisParticipantServiceBean(participantTO));
			negotiationModalityList = holderAccountBalancMovServiceFacade.getMechanismModalityListServiceFacade();
			negotiationMechanismList = holderAccountBalancMovServiceFacade.getNegotiationMechanismFacade();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		initHolderAccountBalanceTO();
		holderAccountRequest   = new HolderAccount();
		
		listHolderMarketFactBalance= new ArrayList<HolderMarketFactBalance>();
		holderMarketFactBalance = new HolderMarketFactBalance();
		listHolderAccountMovInfoTO= new ArrayList<HolderAccountMovInfoTO>();
	}
	
	/**
	 * Join part hold acc handler.
	 */
	public void joinPartHoldAccHandler(){
		this.holderAccountBalanceTO.setIdSecurityCode(null);
		securitiesPartHolAccount.setDescription(null);
		SecuritiesSearcherTO securitiesSearcherTO = new SecuritiesSearcherTO();
		securitiesSearcherTO.setParticipantCode(this.holderAccountBalanceTO.getIdParticipant());
		securitiesSearcherTO.setHolderAccount(this.holderAccountRequest);
		securitiesPartHolAccount.setSecuritiesSearcher(securitiesSearcherTO);
		
		//Limpiamos informacion de la grilla
		this.holderAccountBalanceSession = null;
		this.holderAccountMovementDataModel = null;
		JSFUtilities.resetComponent("frmHolderAccountBalancMov:pnlSecurityAccHelp");
	}
	
	/**
	 * Search holder account bal mov handler.
	 */
	public void searchHolderAccountBalMovHandler(){
		JSFUtilities.hideGeneralDialogues();
		if(Validations.validateIsNullOrEmpty(holderRequest.getIdHolderPk())) {
			JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MESSAGE_DATA_REQUIRED,null);
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();		
		holderAccountBalanceTO.setIdHolderAccount(this.holderAccountRequest.getIdHolderAccountPk());
		holderAccountBalanceTO.setIdParticipant(this.holderAccountBalanceTO.getIdParticipant());
		holderAccountBalanceTO.setIdSecurityCode(this.holderAccountBalanceTO.getIdSecurityCode());
		holderAccountBalanceTO.setInitialDate(this.holderAccountBalanceTO.getInitialDate());
		holderAccountBalanceTO.setFinalDate(this.holderAccountBalanceTO.getFinalDate());
		
		try {
			holderAccountBalanceSession = holderAccountBalancMovServiceFacade.holderAccountBalanceServiceFacade(holderAccountBalanceTO);
			if(holderAccountBalanceSession==null){
				holderAccountMovementDataModel = new GenericDataModel<HolderAccountBalMovDetResultTO>();
				
				JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_VALIDATION_SEARCH,null);
				JSFUtilities.showSimpleValidationDialog();
				return;
				
			}else{
				List<HolderAccountBalMovDetResultTO> holderAccountMovementList = null;
				holderAccountMovementList = holderAccountBalancMovServiceFacade.getHolderAccountMovListServiceFacade(holderAccountBalanceTO);
				if(holderAccountMovementList==null || holderAccountMovementList.isEmpty()){
					holderAccountMovementDataModel = new GenericDataModel<HolderAccountBalMovDetResultTO>();
				}else{
					holderAccountMovementDataModel = new GenericDataModel<HolderAccountBalMovDetResultTO>(holderAccountMovementList);
				}
			}
			listHolderMarketFactBalance = new ArrayList<HolderMarketFactBalance>();
			listHolderMarketFactBalance = holderAccountBalancMovServiceFacade.holderMarketFactBalanceServiceFacade(holderAccountBalanceTO);
			
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Clean holder account bal handler.
	 */
	public void cleanHolderAccountBalHandler(){
		JSFUtilities.resetComponent("frmHolderAccountBalancMov:fldsFilters");
		this.holderAccountBalanceSession = null;
		this.holderAccountMovementDataModel = null;
		this.holderAccountRequest = new HolderAccount();
		initHolderAccountBalanceTO();
		holderRequest = new Holder();
		lstAccountTo = null;
		selectedAccountTO = null;
	
	}
	
	/**
	 * Clean holder account to handler.
	 */
	public void cleanHolderAccountTOHandler(){
		holderAccountRequest = new HolderAccount();
	}
	
	/**
	 * Change participant handler.
	 */
	public void changeParticipantHandler(){
		JSFUtilities.resetComponent("frmHolderAccountBalancMov:fldsFilters");
		this.holderAccountRequest = new HolderAccount();
		this.holderRequest = new Holder();
		this.holderAccountBalanceTO.setIdSecurityCode(null);
		this.holderAccountBalanceSession = null;
		this.holderAccountMovementDataModel = null;
		this.listHolderMarketFactBalance = new ArrayList<HolderMarketFactBalance>();
		lstAccountTo = null;
		securitiesPartHolAccount.setDescription(null);			
	}
	
	/**
	 * Gets the modality description.
	 *
	 * @param modality the modality
	 * @return the modality description
	 */
	public String getModalityDescription(Long modality){
		for(NegotiationModality negotiationModality: this.negotiationModalityList){
			if(negotiationModality.getIdNegotiationModalityPk().equals(modality)){
				return negotiationModality.getModalityName();
			}
		}
		return null;
	}
	
	/**
	 * Gets the mechanism code.
	 *
	 * @param mechanism the mechanism
	 * @return the mechanism code
	 */
	public String getMechanismCode(Long mechanism){
		for(NegotiationMechanism negotiationMechanism: this.negotiationMechanismList){
			if(negotiationMechanism.getIdNegotiationMechanismPk().equals(mechanism)){
				return negotiationMechanism.getMechanismCode()+"-"+NegotiationMechanismType.get(mechanism);
			}
		}
		return null;
	}
	
	
	/**
	 * Search participant on list from pk.
	 *
	 * @author mmacalupu
	 * this method is useful to get Bank from idBankPk because with this method
	 * it's not necesary to getParticipant from database.
	 * @param idParticipantPk the id participant pk
	 * @return the participant
	 */
	public Participant searchParticipantOnListFromPK(Long idParticipantPk){
		for(Participant participant: this.getParticipantList()){
			if(participant.getIdParticipantPk().equals(idParticipantPk)){
				return participant;
			}
		}
		return null;
	}
	
	/**
	 * Clean data table handler.
	 */
	public void cleanDataTableHandler(){
		//Limpiamos informacion de la grilla
		this.holderAccountBalanceSession = null;
		this.holderAccountMovementDataModel = null;
		JSFUtilities.resetComponent("frmHolderAccountBalancMov:pnlSecurityAccHelp");
	}

	
	/**
	 * Gets the holder account request.
	 *
	 * @return the holder account request
	 */
	public HolderAccount getHolderAccountRequest() {
		return holderAccountRequest;
	}

	/**
	 * Sets the holder account request.
	 *
	 * @param holderAccount the new holder account request
	 */
	public void setHolderAccountRequest(HolderAccount holderAccount) {
		this.holderAccountRequest = holderAccount;
	}

	/**
	 * Gets the participant list.
	 *
	 * @return the participant list
	 */
	public List<Participant> getParticipantList() {
		return participantList;
	}

	/**
	 * Sets the participant list.
	 *
	 * @param participantList the new participant list
	 */
	public void setParticipantList(List<Participant> participantList) {
		this.participantList = participantList;
	}

	/**
	 * Gets the holder account balance to.
	 *
	 * @return the holder account balance to
	 */
	public HolderAccountBalanceTO getHolderAccountBalanceTO() {
		return holderAccountBalanceTO;
	}

	/**
	 * Sets the holder account balance to.
	 *
	 * @param holderAccountBalanceTO the new holder account balance to
	 */
	public void setHolderAccountBalanceTO(HolderAccountBalanceTO holderAccountBalanceTO) {
		this.holderAccountBalanceTO = holderAccountBalanceTO;
	}

	/**
	 * Gets the holder account balance session.
	 *
	 * @return the holder account balance session
	 */
	public HolderAccountBalance getHolderAccountBalanceSession() {
		return holderAccountBalanceSession;
	}

	/**
	 * Sets the holder account balance session.
	 *
	 * @param holderAccountBalanceSession the new holder account balance session
	 */
	public void setHolderAccountBalanceSession(
			HolderAccountBalance holderAccountBalanceSession) {
		this.holderAccountBalanceSession = holderAccountBalanceSession;
	}

	/**
	 * Movement name min handler.
	 *
	 * @param movName the mov name
	 * @return the string
	 */
	public String movementNameMinHandler(String movName){
		if(movName==null || movName.length()==0){
			return null;
		}
		if(movName.length()<30){
			return movName;
		}
		String returnString = "";
		for(Character character: movName.toCharArray()){
			returnString+=character;
			if(returnString.length()==30){
				break;
			}
		}
		return returnString+"...";
	}

	/**
	 * Gets the negotiation modality list.
	 *
	 * @return the negotiation modality list
	 */
	public List<NegotiationModality> getNegotiationModalityList() {
		return negotiationModalityList;
	}

	/**
	 * Sets the negotiation modality list.
	 *
	 * @param negotiationModalityList the new negotiation modality list
	 */
	public void setNegotiationModalityList(
			List<NegotiationModality> negotiationModalityList) {
		this.negotiationModalityList = negotiationModalityList;
	}

	/**
	 * Gets the holder account movement data model.
	 *
	 * @return the holder account movement data model
	 */
	public GenericDataModel<HolderAccountBalMovDetResultTO> getHolderAccountMovementDataModel() {
		return holderAccountMovementDataModel;
	}

	/**
	 * Sets the holder account movement data model.
	 *
	 * @param holderAccountMovementDataModel the new holder account movement data model
	 */
	public void setHolderAccountMovementDataModel(
			GenericDataModel<HolderAccountBalMovDetResultTO> holderAccountMovementDataModel) {
		this.holderAccountMovementDataModel = holderAccountMovementDataModel;
	}

	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * Gets the negotiation mechanism list.
	 *
	 * @return the negotiation mechanism list
	 */
	public List<NegotiationMechanism> getNegotiationMechanismList() {
		return negotiationMechanismList;
	}

	/**
	 * Sets the negotiation mechanism list.
	 *
	 * @param negotiationMechanismList the new negotiation mechanism list
	 */
	public void setNegotiationMechanismList(
			List<NegotiationMechanism> negotiationMechanismList) {
		this.negotiationMechanismList = negotiationMechanismList;
	}
	
	/**
	 * Gets the holder request.
	 * 
	 * @return the holder request
	 */
	public Holder getHolderRequest() {
		return holderRequest;
	}

	/**
	 * Sets the holder request.
	 * 
	 * @param holderRequest
	 *            the new holder request
	 */
	public void setHolderRequest(Holder holderRequest) {
		this.holderRequest = holderRequest;
	}
	
	/**
	 * Search holder handler.
	 */
	public void searchHolderHandler(){	
		try {
			
			changeHolderHandler();
			Holder holder = this.getHolderRequest();			
			if (Validations.validateIsNull(holder) || Validations.validateIsNull(holder.getIdHolderPk())) {
				return;
			} 
			HolderTO holderTO = new HolderTO();
			holderTO.setHolderId(holder.getIdHolderPk());
			holderTO.setFlagAllHolders(true);
			holderTO.setParticipantFk(holderAccountBalanceTO.getIdParticipant());
			
			List<HolderAccountHelperResultTO> lista = helperComponentFacade.searchAccountByFilter(holderTO);
			
			if (lista==null || lista.isEmpty()){
				lstAccountTo = new GenericDataModel<>();
			}else{
				lstAccountTo = new GenericDataModel<>(lista);
			}
			
			if(lstAccountTo.getSize()<0){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getGenericMessage(PropertiesConstants.COMMONS_LABEL_PARTICIPANT_CUI_INCORRECT));
				holderRequest = new Holder();
				JSFUtilities.showSimpleValidationDialog();
			}
			
			if(lista!=null && lista.size()==1){
				selectedAccountTO = lista.get(0);
				changeSelectAccount();
			}else if(lstAccountTo!=null){
				JSFUtilities.executeJavascriptFunction("resultHelperAccountVar.clearFilters(); resultHelperAccountVar.unselectAllRows();");
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change select account.
	 */
	public void changeSelectAccount(){
		if(selectedAccountTO!=null){
			holderAccountRequest = new HolderAccount();
			holderAccountRequest.setIdHolderAccountPk(selectedAccountTO.getAccountPk());
			holderAccountRequest.setAccountNumber(selectedAccountTO.getAccountNumber());
			joinPartHoldAccHandler();
		}
		
	}
	/**
	 * Change participant handler.
	 */
	public void changeHolderHandler(){
		JSFUtilities.resetComponent("frmHolderAccountBalancMov:fldsFilters");
		this.holderAccountRequest = new HolderAccount();
		this.holderAccountBalanceTO.setIdSecurityCode(null);
		this.holderAccountBalanceSession = null;
		this.holderAccountMovementDataModel = null;
		securitiesPartHolAccount.setDescription(null);		
		lstAccountTo = null;
	}
	
	/**
	 * Gets the market fact balance.
	 *
	 * @param actionEvent the action event
	 * @return the market fact balance
	 */
	public void getMarketFactBalance(ActionEvent actionEvent){	
		JSFUtilities.hideGeneralDialogues();				
		MarketFactBalanceTO marketFactBalanceTO = new MarketFactBalanceTO();		
		try{
			marketFactBalanceTO.setSecurityCodePk((String) (actionEvent.getComponent().getAttributes().get("attributeIdSecurityCode")));
			marketFactBalanceTO.setHolderAccountPk((Long) (actionEvent.getComponent().getAttributes().get("attributeHolderAccountRequest")));
			marketFactBalanceTO.setParticipantPk((Long) (actionEvent.getComponent().getAttributes().get("attributeIdParticipant")));											
			marketFactBalanceTO.setBalanceType((Long) (actionEvent.getComponent().getAttributes().get("attributeBalanceType")));
			
			Long balanceType = (Long) (actionEvent.getComponent().getAttributes().get("attributeBalanceType"));
			
			//Embargo,otros,gravamen
			if(ParameterBalanceType.BAN_BALANCE.getCode().equals(balanceType) || ParameterBalanceType.OTHERS_BALANCE.getCode().equals(balanceType) || ParameterBalanceType.PAWN_BALANCE.getCode().equals(balanceType)){
				if (ParameterBalanceType.BAN_BALANCE.getCode().equals(balanceType)){
					marketFactBalanceTO.setMovementBalanceType(MovementBalanceType.BLOCK_BALANCE_BAN.getCode());
				}
				if (ParameterBalanceType.OTHERS_BALANCE.getCode().equals(balanceType)){
					marketFactBalanceTO.setMovementBalanceType(MovementBalanceType.BLOCK_BALANCE_OTHERS.getCode());
				}
				if (ParameterBalanceType.PAWN_BALANCE.getCode().equals(balanceType)){
					marketFactBalanceTO.setMovementBalanceType(MovementBalanceType.BLOCK_BALANCE_PAWN.getCode());
				}
				listHolderAccountMovInfoTO = holderAccountBalancMovServiceFacade.listMarketFactByBalance(marketFactBalanceTO);
			}
			//Acreditacion
			if(ParameterBalanceType.ACCREDITATION_BALANCE.getCode().equals(balanceType)){
				marketFactBalanceTO.setAccreditationOperationState(AccreditationOperationStateType.CONFIRMED.getCode());
				listHolderAccountMovInfoTO= holderAccountBalancMovServiceFacade.listMarketFactByAccreditation(marketFactBalanceTO);
			}
			
			this.marketFactBalance = holderAccountBalancMovServiceFacade.findMarketFactBalanceHelp(marketFactBalanceTO);				
			if(this.listHolderAccountMovInfoTO.size() <= 0 && marketFactBalance !=null){
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.ADM_ACCOUNT_BALANCE_QUERY_MOVEMENTS_NOT_RESULT_MARKETFACT, null);
				JSFUtilities.showValidationDialog();
				return;
			}
			JSFUtilities.executeJavascriptFunction("PF('dialogWgvMarketFact').show()");
			
			
		}catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Gets the movements market fact balance.
	 *
	 * @param actionEvent the action event
	 * @return the movements market fact balance
	 */
	public void getMovementsMarketFactBalance(ActionEvent actionEvent){	
		JSFUtilities.hideGeneralDialogues();				
		HolderAccountBalMovDetResultTO movTO = new HolderAccountBalMovDetResultTO();	
		/*try{			
			movTO = (HolderAccountBalMovDetResultTO)(actionEvent.getComponent().getAttributes().get("attributeMov"));	
			this.marketFactBalanceMovement = holderAccountBalancMovServiceFacade.getHolderMarketFactMovementFacade(movTO);
			if(this.getMarketFactBalanceMovement().getMarketFacBalances().size() <= 0){
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.ADM_ACCOUNT_BALANCE_QUERY_MOVEMENTS_NOT_RESULT_MARKETFACT_MOVEMENT, null);
				JSFUtilities.showValidationDialog();
				return;
			}
			JSFUtilities.executeJavascriptFunction("PF('dialogWgvMarketFactMov').show()");
		}catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}*/
	}
	
	/**
	 * Gets the market fact balance.
	 *
	 * @return the market fact balance
	 */
	public MarketFactBalanceTO getMarketFactBalance() {
		return marketFactBalance;
	}

	/**
	 * Sets the market fact balance.
	 *
	 * @param marketFactBalance the new market fact balance
	 */
	public void setMarketFactBalance(MarketFactBalanceTO marketFactBalance) {
		this.marketFactBalance = marketFactBalance;
	}

	/**
	 * Gets the market fact balance movement.
	 *
	 * @return the market fact balance movement
	 */
	public MarketFactBalanceTO getMarketFactBalanceMovement() {
		return marketFactBalanceMovement;
	}

	/**
	 * Sets the market fact balance movement.
	 *
	 * @param marketFactBalanceMovement the new market fact balance movement
	 */
	public void setMarketFactBalanceMovement(
			MarketFactBalanceTO marketFactBalanceMovement) {
		this.marketFactBalanceMovement = marketFactBalanceMovement;
	}

	

	/**
	 * Gets the lst account to.
	 *
	 * @return the lst account to
	 */
	public GenericDataModel<HolderAccountHelperResultTO> getLstAccountTo() {
		return lstAccountTo;
	}

	/**
	 * Sets the lst account to.
	 *
	 * @param lstAccountTo the new lst account to
	 */
	public void setLstAccountTo(
			GenericDataModel<HolderAccountHelperResultTO> lstAccountTo) {
		this.lstAccountTo = lstAccountTo;
	}

	/**
	 * Gets the selected account to.
	 *
	 * @return the selected account to
	 */
	public HolderAccountHelperResultTO getSelectedAccountTO() {
		return selectedAccountTO;
	}

	/**
	 * Sets the selected account to.
	 *
	 * @param selectedAccountTO the new selected account to
	 */
	public void setSelectedAccountTO(HolderAccountHelperResultTO selectedAccountTO) {
		this.selectedAccountTO = selectedAccountTO;
	}

	/**
	 * Gets the list holder market fact balance.
	 *
	 * @return the list holder market fact balance
	 */
	public List<HolderMarketFactBalance> getListHolderMarketFactBalance() {
		return listHolderMarketFactBalance;
	}

	/**
	 * Sets the list holder market fact balance.
	 *
	 * @param listHolderMarketFactBalance the new list holder market fact balance
	 */
	public void setListHolderMarketFactBalance(
			List<HolderMarketFactBalance> listHolderMarketFactBalance) {
		this.listHolderMarketFactBalance = listHolderMarketFactBalance;
	}

	/**
	 * Gets the holder market fact balance.
	 *
	 * @return the holder market fact balance
	 */
	public HolderMarketFactBalance getHolderMarketFactBalance() {
		return holderMarketFactBalance;
	}

	/**
	 * Sets the holder market fact balance.
	 *
	 * @param holderMarketFactBalance the new holder market fact balance
	 */
	public void setHolderMarketFactBalance(
			HolderMarketFactBalance holderMarketFactBalance) {
		this.holderMarketFactBalance = holderMarketFactBalance;
	}

	/**
	 * Gets the list holder account mov info to.
	 *
	 * @return the list holder account mov info to
	 */
	public List<HolderAccountMovInfoTO> getListHolderAccountMovInfoTO() {
		return listHolderAccountMovInfoTO;
	}

	/**
	 * Sets the list holder account mov info to.
	 *
	 * @param listHolderAccountMovInfoTO the new list holder account mov info to
	 */
	public void setListHolderAccountMovInfoTO(
			List<HolderAccountMovInfoTO> listHolderAccountMovInfoTO) {
		this.listHolderAccountMovInfoTO = listHolderAccountMovInfoTO;
	}
	
	
	
}