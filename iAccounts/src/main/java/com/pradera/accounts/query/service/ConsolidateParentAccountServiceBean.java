package com.pradera.accounts.query.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.pradera.accounts.query.to.ConsolidateParentAccountResultTO;
import com.pradera.accounts.query.to.ConsolidateParentAccountTO;
import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ConsolidateParentAccountServiceBean.
 *
 * @author Martin Zarate Rafael
 */

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@Performance
public class ConsolidateParentAccountServiceBean extends CrudDaoServiceBean{

	/**
	 * Gets the daily exchange rates service bean.
	 *
	 * @param consolidateParentAccountTO the consolidate parent account to
	 * @return Consolidate Parent Account
	 */
	public List<Object[]> searchConsolidateParentAccount(ConsolidateParentAccountTO consolidateParentAccountTO){
		StringBuffer sb = new StringBuffer();
		
		sb.append(" SELECT DISTINCT ha.ACCOUNT_GROUP , 									");//0		h.HOLDER_TYPE
		
		sb.append("  	   ha.ACCOUNT_NUMBER ,												");//5		1
		sb.append("  	   ha.ALTERNATE_CODE ,												");//6		2
		sb.append("  	   ha.STATE_ACCOUNT ,												");//7		3
		sb.append("		   hab.ID_PARTICIPANT_PK ,											");//8		4
		sb.append("		   ha.ID_HOLDER_ACCOUNT_PK ,										");//9		5
		sb.append("		   s.ID_SECURITY_CODE_PK ,											");//10		6
		sb.append("		   hab.TOTAL_BALANCE ,												");//11		7
		sb.append("		   hab.AVAILABLE_BALANCE ,											");//12		8
		sb.append("		   hab.PAWN_BALANCE + hab.BAN_BALANCE + hab.OTHER_BLOCK_BALANCE ,	");//13		9
		sb.append("		   p.MNEMONIC AS part_desc,											");//14		10
		sb.append("	       i.MNEMONIC ,														");//15		11
		sb.append("	       ha.ID_HOLDER_ACCOUNT_PK as HOLDER_ACCOUNT,						");//15		12 h.ID_HOLDER_PK
		sb.append("	       FN_GET_HOLDER_DESC(ha.ID_HOLDER_ACCOUNT_PK ,'-',1) 				");//15		13
		sb.append("        ,s.CURRENT_NOMINAL_VALUE                                 		");//14
		
		sb.append(" FROM HOLDER_ACCOUNT_BALANCE hab  ");
		sb.append(" INNER JOIN HOLDER_ACCOUNT ha ON hab.ID_HOLDER_ACCOUNT_PK = ha.ID_HOLDER_ACCOUNT_PK ");
		sb.append(" INNER JOIN HOLDER_ACCOUNT_DETAIL had ON had.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK ");
		sb.append(" INNER JOIN HOLDER h ON h.ID_HOLDER_PK = had.ID_HOLDER_FK ");
		sb.append(" INNER JOIN SECURITY s ON hab.ID_SECURITY_CODE_PK = s.ID_SECURITY_CODE_PK ");
		sb.append(" INNER JOIN ISSUER i ON s.ID_ISSUER_FK = i.ID_ISSUER_PK 					");
		sb.append(" INNER JOIN PARTICIPANT p ON hab.ID_PARTICIPANT_PK = p.ID_PARTICIPANT_PK ");
		sb.append("	WHERE 1 = 1 														");
		
		sb.append("	AND hab.TOTAL_BALANCE + hab.REPORTED_BALANCE + hab.PURCHASE_BALANCE > 0");
		//sb.append(" AND had.IND_REPRESENTATIVE = 1");
		
		if(Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getParticipant()) &&
				Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getParticipant().getIdParticipantPk())){
			sb.append(" and p.ID_PARTICIPANT_PK = :participantPk");
			if(consolidateParentAccountTO.isBlParticipantInv())
				sb.append(" and p.DOCUMENT_NUMBER = ho.documentNumber");
		}
		if(Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getHolder()) &&
				Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getHolder().getIdHolderPk())){
			sb.append(" and h.ID_HOLDER_PK = :holderPk");
		}
		if(Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getHolderAccount()) &&
				Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getHolderAccount().getIdHolderAccountPk())){
			sb.append(" and ha.ID_HOLDER_ACCOUNT_PK = :holderAccountPk");
		}
		if(Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getSecurity().getIdSecurityCodePk())){
			sb.append(" and s.ID_SECURITY_CODE_PK = :securityCodePk");
		}	
		if(Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getIssuer()) &&
				Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getIssuer().getIdIssuerPk())){
		    sb.append(" and i.ID_ISSUER_PK = :issuerPk");
		}
		if(Validations.validateIsNotNullAndNotEmpty(  consolidateParentAccountTO.getSecurity().getSecurityClass()  )){
		    sb.append(" and s.SECURITY_CLASS = :securityClassPrm");
		}
		if(Validations.validateIsNotNullAndNotEmpty(  consolidateParentAccountTO.getSecurity().getCurrency()  )){
		    sb.append(" and s.CURRENCY = :currencyPrm");
		}
		sb.append(" order by ha.ACCOUNT_NUMBER ");
		
		Query query = em.createNativeQuery(sb.toString());
		
		if(Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getParticipant()) &&
				Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getParticipant().getIdParticipantPk())){
			query.setParameter("participantPk", consolidateParentAccountTO.getParticipant().getIdParticipantPk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getHolder()) &&
				Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getHolder().getIdHolderPk())){
			query.setParameter("holderPk", consolidateParentAccountTO.getHolder().getIdHolderPk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getHolderAccount()) &&
				Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getHolderAccount().getIdHolderAccountPk())){
			query.setParameter("holderAccountPk", consolidateParentAccountTO.getHolderAccount().getIdHolderAccountPk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getSecurity().getIdSecurityCodePk())){
			query.setParameter("securityCodePk", consolidateParentAccountTO.getSecurity().getIdSecurityCodePk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getIssuer()) &&
				Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getIssuer().getIdIssuerPk())){
			query.setParameter("issuerPk",consolidateParentAccountTO.getIssuer().getIdIssuerPk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(  consolidateParentAccountTO.getSecurity().getSecurityClass()  )){
		    query.setParameter("securityClassPrm",consolidateParentAccountTO.getSecurity().getSecurityClass());
		}
		if(Validations.validateIsNotNullAndNotEmpty(  consolidateParentAccountTO.getSecurity().getCurrency()  )){
		    query.setParameter("currencyPrm",consolidateParentAccountTO.getSecurity().getCurrency()  );
		}
		return query.getResultList();
	}
	
	public List<String> searchHolderAccountDescription(ConsolidateParentAccountResultTO consolidateParentAccountResultTO){
		StringBuffer sb = new StringBuffer();
		
		
		sb.append("	(select h.id_holder_pk || ' - ' || h.full_name DESCRIPCION from holder_account_detail had, holder h where had.id_holder_account_fk=:holderAccountPk and h.id_holder_pk=had.id_holder_fk) 	");
		
		Query query = em.createNativeQuery(sb.toString());
		
		if(Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountResultTO.getIdHolderAccountPk())){
			query.setParameter("holderAccountPk", consolidateParentAccountResultTO.getIdHolderAccountPk());
		}
		
		return query.getResultList();
	}
}
