package com.pradera.accounts.query.facade;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.pradera.accounts.query.service.IssuerHolderQueryServiceBean;
import com.pradera.accounts.query.to.HolderAccountBalMovDetResultTO;
import com.pradera.accounts.query.to.IssuerHolderDetailTO;
import com.pradera.accounts.query.to.IssuerHolderQueryTO;
import com.pradera.core.component.helperui.to.IssuerSearcherTO;
import com.pradera.core.component.operation.to.MarketFactBalanceTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountMovement;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.negotiation.HolderAccountOperation;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class IssuerHolderQueryServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/08/2015
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class IssuerHolderQueryServiceFacade {
	
	/** The obj issuer holder query service bean. */
	@EJB
	IssuerHolderQueryServiceBean objIssuerHolderQueryServiceBean;
	
	/**
	 * Search issuer holder query.
	 *
	 * @param issuerHolderQueryTO the issuer holder query to
	 * @param issuerUser the issuer user
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<IssuerHolderDetailTO> searchIssuerHolderQuery(IssuerHolderQueryTO issuerHolderQueryTO, boolean issuerUser)throws ServiceException{
        return objIssuerHolderQueryServiceBean.searchIssuerHolderQuery(issuerHolderQueryTO,issuerUser);
	}
	
	/**
	 * Search issuer security query.
	 *
	 * @param issuerHolderQueryTO the issuer holder query to
	 * @param issuerUser the issuer user
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public String searchIssuerSecurityQuery(IssuerHolderQueryTO issuerHolderQueryTO, boolean issuerUser) throws ServiceException{
			 return objIssuerHolderQueryServiceBean.searchIssuerSecurityQuery(issuerHolderQueryTO,issuerUser);
	    }
	
	public List<Object[]> getQueryListByClass(String query) throws ServiceException{
		 return objIssuerHolderQueryServiceBean.getQueryListByClass(query);
   }
	
	/**
	 * Search holder account balance.
	 *
	 * @param issuerHolderDetailTO the issuer holder detail to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBalance> searchHolderAccountBalance(IssuerHolderDetailTO issuerHolderDetailTO)throws ServiceException{
        return objIssuerHolderQueryServiceBean.searchHolderAccountBalance(issuerHolderDetailTO);
	}
	
	/**
	 * Gets the list securities class setup.
	 *
	 * @param isIssuerDpf the is issuer dpf
	 * @return the list securities class setup
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getListSecuritiesClassSetup(boolean isIssuerDpf) throws ServiceException{
		return objIssuerHolderQueryServiceBean.getListSecuritiesClassSetup(isIssuerDpf);
	} 
	
	/**
	 * Checks for balance.
	 *
	 * @param holderAccountBalance the holder account balance
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean hasBalance(HolderAccountBalance holderAccountBalance) throws ServiceException{
		return objIssuerHolderQueryServiceBean.hasBalance(holderAccountBalance);
	}
	
	/**
	 * Gets the holder account balance.
	 *
	 * @param holderAccountBalance the holder account balance
	 * @return the holder account balance
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBalance> getHolderAccountBalance(HolderAccountBalance holderAccountBalance) throws ServiceException{
		return objIssuerHolderQueryServiceBean.getHolderAccountBalance(holderAccountBalance);
	}
	
	/**
	 * Gets the holder account movement.
	 *
	 * @param holderAccountBalance the holder account balance
	 * @return the holder account movement
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountMovement> getHolderAccountMovement(HolderAccountBalance holderAccountBalance) throws ServiceException{
		return objIssuerHolderQueryServiceBean.getHolderAccountMovement(holderAccountBalance);
	}
	
	/**
	 * Gets the holders detail.
	 *
	 * @param holderAccountPK the holder account pk
	 * @return the holders detail
	 * @throws ServiceException the service exception
	 */
	public List<Object[]>  getHoldersDetail(String holderAccountPK) throws ServiceException{
		return objIssuerHolderQueryServiceBean.getHoldersDetail(holderAccountPK);
	}
	/**
	 * Gets the holder account mov list service facade.
	 *
	 * @param holderAccountBalanceTO the holder account balance to
	 * @return the holder account mov list service facade
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBalMovDetResultTO> getHolderAccountMovListServiceFacade(HolderAccountBalanceTO holderAccountBalanceTO) throws ServiceException{
		List<HolderAccountBalMovDetResultTO> holdAccountBalMovList = new ArrayList<HolderAccountBalMovDetResultTO>();
		List<HolderAccountBalMovDetResultTO> holdAccountList = objIssuerHolderQueryServiceBean.getHolderAccountMovListServiceBean(holderAccountBalanceTO);
		if(holdAccountList!=null){
			for(HolderAccountBalMovDetResultTO holderAccountResult: getHolAccBalMovResultOrder(holdAccountList)){
				
				List<String> stringList = null;
				for(HolderAccountBalMovDetResultTO holderAccountResultAll: holdAccountList){
					if(holderAccountResult.equals(holderAccountResultAll) && holderAccountResultAll.getMovementDetail().get(0)!=null){//Verificamos que sea el objeto y exista tracking
						if(stringList==null)//Si aun no a sido inicializada
							stringList = new ArrayList<String>();
						stringList.add("</br>");
						stringList.add(holderAccountResultAll.getMovementDetail().get(0));
					}
				}
				holderAccountResult.setMovementDetail(stringList);
				holdAccountBalMovList.add(holderAccountResult);
			}
		}		
		return holdAccountBalMovList;
	}
	/**
	 * Gets the hol acc bal mov result order.
	 * metodo para retornar el detalle de cada movimiento.(SUMA,RESTA,etc)
	 * @param holdAccBalMovDetResultTO the hold acc bal mov det result to
	 * @return the hol acc bal mov result order
	 */
	private List<HolderAccountBalMovDetResultTO> getHolAccBalMovResultOrder(List<HolderAccountBalMovDetResultTO> holdAccBalMovDetResultTO){
		List<HolderAccountBalMovDetResultTO> holdAccBalMovReturn = new ArrayList<HolderAccountBalMovDetResultTO>();
		for(HolderAccountBalMovDetResultTO holderAccountResult: holdAccBalMovDetResultTO){
			if(holdAccBalMovReturn.indexOf(holderAccountResult)==-1){
				holdAccBalMovReturn.add(holderAccountResult);
			}
		}
		return holdAccBalMovReturn;
	}
	
	/**
	 * Market Fact Balance service bean.
	 *
	 * @param holderAccountBalMovDetResultTO the holder account bal mov det result to
	 * @return the Market Fact Balance to
	 * @throws ServiceException the service exception
	 */
	public MarketFactBalanceTO getHolderMarketFactMovementFacade(HolderAccountBalMovDetResultTO holderAccountBalMovDetResultTO) throws ServiceException{
		return objIssuerHolderQueryServiceBean.getHolderMarketFactMovementService(holderAccountBalMovDetResultTO);
	}
	
	/**
	 * It gives the Issuer details.
	 *
	 * @param issuerTo the IssuerSearcherTO
	 * @return the Issuer
	 * @throws ServiceException the service exception
	 */
	public Issuer getIssuerDetails(IssuerSearcherTO issuerTo) throws ServiceException{	 
		return  objIssuerHolderQueryServiceBean.getIssuerByCode(issuerTo);
		}
}
