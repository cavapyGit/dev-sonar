package com.pradera.accounts.query.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.accounts.query.facade.HolderAccountBalancMovServiceFacade;
import com.pradera.accounts.query.facade.IssuerHolderQueryServiceFacade;
import com.pradera.accounts.query.to.HolderAccountBalMovDetResultTO;
import com.pradera.accounts.query.to.IssuerHolderDetailTO;
import com.pradera.accounts.query.to.IssuerHolderQueryTO;
import com.pradera.accounts.util.view.PropertiesConstants;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.commons.view.ui.UICompositeType;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.helperui.HelperBean;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.core.component.helperui.to.IssuerSearcherTO;
import com.pradera.core.component.helperui.to.SecurityFinancialDataHelpTO;
import com.pradera.core.component.operation.to.MarketFactBalanceTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityType;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
// TODO: Auto-generated Javadoc
//TODO: Auto-generated Javadoc
/**
* <ul>
* <li>
* Project Pradera.
* Copyright 2015.</li>
* </ul>
* 
* The Class IssuerHolderQueryBean.
* Consulta de inversiones del titular en el emisor
*
* @author PraderaTechnologies.
* @version 1.0 , 16/04/2015
*/
@DepositaryWebBean
@HelperBean
public class IssuerHolderQueryBean extends GenericBaseBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The holder account request. */
	private HolderAccount holderAccountRequest;
	
	/** The helper component facade. */
	@EJB
	private HelperComponentFacade 		  helperComponentFacade;
	
	/** The issuer holder query to. */
	IssuerHolderQueryTO issuerHolderQueryTO;
	
	/** The parameter service. */
	@EJB
	private ParameterServiceBean parameterService;
	
	/** The lst cbo instrument type. */
	private List<ParameterTable> lstCboInstrumentType , lstCboSecurityType,lstCboSecurityClass;
	
	/** The parameter table to. */
	
	private GenericDataModel<HolderAccount> lstAccountTo;
	
	private GenericDataModel<Holder> holderDataModel;
	
	/** The selected account to. */
	private HolderAccount selectedAccountTO;
	
	/** The holder. */
	private Holder holder;
	
	/** The selected issuer holder detail to. */
	IssuerHolderDetailTO selectedIssuerHolderDetailTO;
	
	/** The lst holder account balances. */
	private List<HolderAccountBalance> lstHolderAccountBalances;
	
	/** The holder account movement list. */
	List<HolderAccountBalMovDetResultTO> holderAccountMovementList = null;
	
	/** The market fact balance movement. */
	private MarketFactBalanceTO marketFactBalanceMovement;
	
	/** The issuer holder detail to. */
	private IssuerHolderDetailTO issuerHolderDetailTO;
	
	/** The negotiation mechanism list. */
	private List<NegotiationMechanism> negotiationMechanismList;
	
	/** The negotiation modality list. */
	private List<NegotiationModality> negotiationModalityList;
	
	/** The participant list. */
	private List<Participant> participantList;
	
	/** The issuer user. */
	private boolean issuerUser;
	
	/** The participant user. */
	private boolean participantUser;
	
	/** The security code. */
	private String securityCode;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The id to compare in the screen */
	private Long idHolderScreen;
	
	/** The option selected one radio. */
	private Integer optionSelectedOneRadio;
	
	/** The document type description. */
    private Map<Integer,String> documentTypeDescription = null;
	
    /** The request state description. */
    private Map<Integer,String> holderStateDescription = null;
    
    /** The request state description. */
    private Map<Integer,String> documentSourceDescription = null;
    
    private final String SPACING="                ";
	
	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
	/** The obj issuer holder query service facade. */
	@EJB
	IssuerHolderQueryServiceFacade objIssuerHolderQueryServiceFacade;
	
	/** The holder account balanc mov service facade. */
	 @EJB
	HolderAccountBalancMovServiceFacade holderAccountBalancMovServiceFacade;
	 
	/** The general parameter facade. */
	@EJB
	private GeneralParametersFacade generalParameterFacade;
	
	/** The user privilege. */
	@Inject
	private UserPrivilege userPrivilege;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The participant service bean. */
	@EJB
	ParticipantServiceBean participantServiceBean;
	
	/**
	 * Instantiates a new issuer holder query bean.
	 */
	public IssuerHolderQueryBean(){
		super();
	}
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		try{
		createObject();
		loadCboSecurityClass();
		optionSelectedOneRadio = Integer.valueOf(0);
		setIdHolderScreen(null);
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire( new ExceptionToCatchEvent(e) );
		}
	}
	
	/**
	 * Gets the constancy holder.
	 *
	 * @param actionEvent the action event
	 * @return the constancy holder
	 */
	public void getConstancyHolder(ActionEvent actionEvent){
		
		try{	
		JSFUtilities.hideGeneralDialogues();	
		issuerHolderDetailTO=new IssuerHolderDetailTO();	
		issuerHolderDetailTO = (IssuerHolderDetailTO)(actionEvent.getComponent().getAttributes().get("holderAccount"));
		selectedIssuerHolderDetailTO=issuerHolderDetailTO;
		issuerHolderDetailTO.getSecurity().getIdSecurityCodePk();
		List<Object[]>   lsHolders=objIssuerHolderQueryServiceFacade.getHoldersDetail(selectedIssuerHolderDetailTO.getHolderAccountBalance().
				getHolderAccount().getIdHolderAccountPk().toString());
		if(lsHolders!=null){
			List<HolderTO> lstHolderTO = new ArrayList<HolderTO>();
			lstHolderTO=fillListHolderTO(lsHolders);
			issuerHolderDetailTO.setLstHolderTO(new GenericDataModel<HolderTO>(lstHolderTO));
		}
			JSFUtilities.executeJavascriptFunction("PF('dlg2').show()");  
		}catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Fill list holder to.
	 *
	 * @param lsHolders the ls holders
	 * @return the list
	 */
	public List<HolderTO> fillListHolderTO(List<Object[]> lsHolders)
	{
		List<HolderTO> lstHolderTO = new ArrayList<HolderTO>();
		
		for (Object[]  ojb:lsHolders) {
			HolderTO holderTo=new HolderTO();
			holderTo.setHolderId(new Long(ojb[0].toString()));
			holderTo.setFullName(ojb[1].toString());
			holderTo.setDocumentTypeDesc(ojb[2].toString());
			holderTo.setDocumentNumber(ojb[3].toString());
			if(Validations.validateIsNotNullAndNotEmpty(ojb[4]))
			holderTo.setName(ojb[4].toString());
			lstHolderTO.add(holderTo);
		}
		return lstHolderTO;
	}
	
	/**
	 * Creates the object.
	 */
	public void createObject(){
		issuerHolderQueryTO = new IssuerHolderQueryTO();
		issuerHolderQueryTO.setIssuerHelperMgmt(new Issuer());
		issuerHolderQueryTO.setHolderHelperMgmt(new Holder());
		issuerHolderQueryTO.setBalanceYes(true);
		issuerHolderQueryTO.setIdSecurityCode(null);
		lstCboSecurityClass=new ArrayList<ParameterTable>();
		issuerHolderQueryTO.setIdSecurityCode(null);
		selectedAccountTO =new HolderAccount();
		selectedAccountTO.setParticipant(new Participant());
		Participant participantTO = new Participant();
		issuerUser = false;
		participantUser=false;
		idParticipantPk=null;
		try {
			setParticipantList(accountsFacade.getLisParticipantServiceBean(participantTO));
			negotiationMechanismList = holderAccountBalancMovServiceFacade.getNegotiationMechanismFacade();
			negotiationModalityList = holderAccountBalancMovServiceFacade.getMechanismModalityListServiceFacade();			
			if((userInfo.getUserAccountSession().isIssuerInstitucion() && 
					Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getIssuerCode())) ||
					(userInfo.getUserAccountSession().isIssuerDpfInstitucion() && 
							Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getIssuerCode())) ) {				
				issuerUser = true;
				IssuerSearcherTO issuerTo = new IssuerSearcherTO();
				issuerTo.setIssuerCode(userInfo.getUserAccountSession().getIssuerCode());
				Issuer issuer =	objIssuerHolderQueryServiceFacade.getIssuerDetails(issuerTo);
				issuerHolderQueryTO.setIssuerHelperMgmt(issuer);	
				issuerHolderQueryTO.setIdInstrumentType(InstrumentType.FIXED_INCOME.getCode());
				changeInstrumentType();
				issuerHolderQueryTO.setIdOfferType(SecurityType.PUBLIC.getCode());
				changeSecurityType();
				List<ParameterTable> lstCboSecurityClassAux = new ArrayList<ParameterTable>();
				for(ParameterTable objSecurityClass : lstCboSecurityClass){
					if(objSecurityClass.getParameterTablePk().equals(SecurityClassType.DPA.getCode()) || 
							objSecurityClass.getParameterTablePk().equals(SecurityClassType.DPF.getCode())){
						lstCboSecurityClassAux.add(objSecurityClass);
					}
				}
				lstCboSecurityClass = lstCboSecurityClassAux;
			} else {
				issuerUser=false;
			}						
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * Load cbo instrument type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCboSecurityClass() throws ServiceException{
		lstCboSecurityClass=objIssuerHolderQueryServiceFacade.getListSecuritiesClassSetup(userInfo.getUserAccountSession().isIssuerDpfInstitucion());
	}
	
	/**
	 * Load holder account balance listener.
	 *
	 * @param issuerHolderDetailTO the issuer holder detail to
	 * @throws ServiceException the service exception
	 */
	public void loadHolderAccountBalanceListener(IssuerHolderDetailTO issuerHolderDetailTO)throws ServiceException{
		selectedIssuerHolderDetailTO = issuerHolderDetailTO;
		List<HolderAccountBalance> lstHolderAccountBalance = objIssuerHolderQueryServiceFacade.searchHolderAccountBalance(selectedIssuerHolderDetailTO);
		if(lstHolderAccountBalance!=null){
			selectedIssuerHolderDetailTO.setLstHolderAccountBalance(new GenericDataModel<HolderAccountBalance>(lstHolderAccountBalance));
		}
	}
	
	/**
	 * Load holder account balance and movements listener.
	 *
	 * @param holderAccountBalance the holder account balance
	 */
	public void loadHolderAccountBalanceAndMovementsListener(HolderAccountBalance holderAccountBalance){
		selectedIssuerHolderDetailTO.setHolderAccountBalance(holderAccountBalance);
		lstHolderAccountBalances = null;
		holderAccountMovementList = null;		
		try {
			if(selectedIssuerHolderDetailTO.getHolderAccountBalance()!=null){
				boolean hasBalance = false;
				hasBalance = objIssuerHolderQueryServiceFacade.hasBalance(selectedIssuerHolderDetailTO.getHolderAccountBalance());				
				if(hasBalance){														
					lstHolderAccountBalances = objIssuerHolderQueryServiceFacade.getHolderAccountBalance(selectedIssuerHolderDetailTO.getHolderAccountBalance());
					if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountBalances)){
						HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();		
						holderAccountBalanceTO.setIdHolderAccount(selectedIssuerHolderDetailTO.getHolderAccountBalance().getId().getIdHolderAccountPk());
						holderAccountBalanceTO.setIdParticipant(selectedIssuerHolderDetailTO.getHolderAccountBalance().getId().getIdParticipantPk());
						holderAccountBalanceTO.setIdSecurityCode(selectedIssuerHolderDetailTO.getHolderAccountBalance().getId().getIdSecurityCodePk());
						holderAccountMovementList = objIssuerHolderQueryServiceFacade.getHolderAccountMovListServiceFacade(holderAccountBalanceTO);
					}				
					if(holderAccountMovementList!=null && holderAccountMovementList.size()>0){
						JSFUtilities.executeJavascriptFunction("PF('dlgWBalance').show();");
					}else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage("alt.query.operations.noMovements"));
						JSFUtilities.showSimpleValidationDialog();
						selectedIssuerHolderDetailTO.setHolderAccountBalance(null);
					}					
				}else{
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage("alt.query.operations.noBalance"));
					JSFUtilities.showSimpleValidationDialog();
					selectedIssuerHolderDetailTO.setHolderAccountBalance(null);
				}
			}	
		} catch (ServiceException e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Clean data.
	 */
	public void cleanData(){
		//Limpiamos informacion de la grilla
		issuerHolderQueryTO.setLstIssuerHolderDetailTO(null);
		JSFUtilities.resetComponent("frmIssuerHolderQuery:opnlResult");
		JSFUtilities.resetComponent("frmIssuerHolderQuery:opnlResultSec");
	} 
	
	
	/**
	 * Fill list holder to.
	 *
	 * @param lsHolders the ls holders
	 * @return the list
	 */
	public List<HolderTO> fillListHolders(List<Object[]> lsHolders)
	{
		List<HolderTO> lstHolderTO = new ArrayList<HolderTO>();
		for (Object[]  ojb:lsHolders) {
			HolderTO holderTo=new HolderTO();
			holderTo.setDocumentTypeDesc(ojb[0].toString());
			holderTo.setDocumentNumber(ojb[1].toString());
			holderTo.setFullName(ojb[2].toString());
			holderTo.setName(ojb[3].toString());
			holderTo.setSecondLastName(ojb[4].toString());
			holderTo.setTotalBalance(ojb[5].toString());
			holderTo.setAvailableBalance(ojb[6].toString());
			lstHolderTO.add(holderTo);
		}
		return lstHolderTO;
	}
	
	/**
	 * Search issuer holder query.
	 *
	 * @throws ServiceException the service exception
	 */
	public void searchIssuerHolderQuery() throws ServiceException{
		if(optionSelectedOneRadio.equals(Integer.valueOf(0))){												
			if(issuerHolderQueryTO.getIssuerHelperMgmt().getIdIssuerPk()==null || selectedAccountTO==null || issuerHolderQueryTO.getHolderHelperMgmt().getIdHolderPk()==null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REVIEW), 
					    PropertiesUtilities.getMessage(PropertiesConstants.MESSAGE_DATA_REQUIRED));
					  JSFUtilities.showSimpleValidationDialog();
				return;
			}else{
				List<IssuerHolderDetailTO> lstIssuerHolderDetailTO = objIssuerHolderQueryServiceFacade.searchIssuerHolderQuery(issuerHolderQueryTO,issuerUser);
				if(lstIssuerHolderDetailTO!=null){
					issuerHolderQueryTO.setLstIssuerHolderDetailTO(new GenericDataModel<IssuerHolderDetailTO>(lstIssuerHolderDetailTO));
				}
			}
		}
		
		if(optionSelectedOneRadio.equals(Integer.valueOf(1))){
			if(issuerHolderQueryTO.getIssuerHelperMgmt().getIdIssuerPk()==null || issuerHolderQueryTO.getIdSecurityCode()==null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REVIEW), 
					    PropertiesUtilities.getMessage(PropertiesConstants.MESSAGE_DATA_REQUIRED));
					  JSFUtilities.showSimpleValidationDialog();
				return;
			}else{
				List<Object[]> queryMain=new ArrayList<Object[]>();
				try {
					String aa =objIssuerHolderQueryServiceFacade.searchIssuerSecurityQuery(issuerHolderQueryTO,issuerUser);
					queryMain=objIssuerHolderQueryServiceFacade.getQueryListByClass(aa);
					
					if(queryMain!=null){
						issuerHolderDetailTO=new IssuerHolderDetailTO();
						List<HolderTO> lstHolderTO = new ArrayList<HolderTO>();
						lstHolderTO=fillListHolders(queryMain);
						issuerHolderDetailTO.setLstHolderTO(new GenericDataModel<HolderTO>(lstHolderTO));
					}
				} catch (Exception ex) {
					excepcion.fire(new ExceptionToCatchEvent(ex));
				}
			}
		}
	}
	
	/**
	 * Clean search.
	 */
	public void cleanSearch(){
		optionSelectedOneRadio = Integer.valueOf(0);
		if ((userInfo.getUserAccountSession().isIssuerInstitucion() && 
		Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getIssuerCode()))||
			(userInfo.getUserAccountSession().isIssuerDpfInstitucion() && 
		Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getIssuerCode())))
		{
			issuerHolderQueryTO.setHolderHelperMgmt(new Holder());
			issuerHolderQueryTO.setBalanceNo(false);
			issuerHolderQueryTO.setIdSecurityCode(null);
			issuerHolderQueryTO.setBalanceYes(true);
			issuerHolderQueryTO.setIdSecurityClass(null);
			selectedAccountTO =new HolderAccount();
			selectedAccountTO.setParticipant(new Participant());
			holderAccountRequest=new HolderAccount();
			lstAccountTo=null;
			cleanResults();
		}else{
			holderAccountRequest=new HolderAccount();
			issuerHolderQueryTO.setIssuerHelperMgmt(new Issuer());
			issuerHolderQueryTO.setHolderHelperMgmt(new Holder());
			issuerHolderQueryTO.setIdInstrumentType(null);
			issuerHolderQueryTO.setIdOfferType(null);
			issuerHolderQueryTO.setIdSecurityClass(null);
			issuerHolderQueryTO.setBalanceNo(false);
			issuerHolderQueryTO.setBalanceYes(true);
			selectedAccountTO =new HolderAccount();
			selectedAccountTO.setParticipant(new Participant());
			issuerHolderQueryTO.setIdSecurityCode(null);
			lstAccountTo=null;
			cleanResults();
		}		
	}
	
	/**
	 * Validate select security balance.
	 */
	public void validateSelectSecurityBalance(){
		cleanResults();
		if(!issuerHolderQueryTO.isBalanceNo() && !issuerHolderQueryTO.isBalanceYes()){
			issuerHolderQueryTO.setBalanceYes(true);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.VALIDATE_SELECTED_SECURITY_BALANCE));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Clean results.
	 */
	public void cleanResults(){
		issuerHolderQueryTO.setLstIssuerHolderDetailTO(null);
		issuerHolderDetailTO=new IssuerHolderDetailTO();
		issuerHolderDetailTO.setLstHolderTO(null);
		selectedIssuerHolderDetailTO = null;		
	}
	
	/**
	 * Change security class.
	 */
	public void changeSecurityClass(){
		cleanResults();
	}
	
	/**
	 * Change security type.
	 */
	public void changeSecurityType(){
		cleanResults();
		try {
			issuerHolderQueryTO.setIdSecurityClass(null);			
			if(issuerHolderQueryTO.getIssuerHelperMgmt()==null || issuerHolderQueryTO.getIssuerHelperMgmt().getIdIssuerPk()==null ||
					issuerHolderQueryTO.getIdOfferType()==null || issuerHolderQueryTO.getIdInstrumentType()==null){
				lstCboSecurityClass=new ArrayList<ParameterTable>();
				return;
			}			
			loadCboSecurityClass();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Gets the movements market fact balance.
	 *
	 * @param actionEvent the action event
	 * @return the movements market fact balance
	 */
	public void getMovementsMarketFactBalance(ActionEvent actionEvent){	
		JSFUtilities.hideGeneralDialogues();				
		HolderAccountBalMovDetResultTO movTO = new HolderAccountBalMovDetResultTO();	
		try{			
			movTO = (HolderAccountBalMovDetResultTO)(actionEvent.getComponent().getAttributes().get("attributeMov"));	
			this.marketFactBalanceMovement = objIssuerHolderQueryServiceFacade.getHolderMarketFactMovementFacade(movTO);
			if(this.getMarketFactBalanceMovement().getMarketFacBalances().size() <= 0){
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.ADM_ACCOUNT_BALANCE_QUERY_MOVEMENTS_NOT_RESULT_MARKETFACT_MOVEMENT, null);
				JSFUtilities.showValidationDialog();
				return;
			}
			JSFUtilities.executeJavascriptFunction("PF('dialogWgvMarketFactMov').show()");
		}catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	/**
	 * Gets the mechanism code.
	 *
	 * @param mechanism the mechanism
	 * @return the mechanism code
	 */
	public String getMechanismCode(Long mechanism){
		for(NegotiationMechanism negotiationMechanism: this.negotiationMechanismList){
			if(negotiationMechanism.getIdNegotiationMechanismPk().equals(mechanism)){
				return negotiationMechanism.getMechanismCode()+"-"+NegotiationMechanismType.get(mechanism);
			}
		}
		return null;
	}
	/**
	 * Gets the modality description.
	 *
	 * @param modality the modality
	 * @return the modality description
	 */
	public String getModalityDescription(Long modality){
		for(NegotiationModality negotiationModality: this.negotiationModalityList){
			if(negotiationModality.getIdNegotiationModalityPk().equals(modality)){
				return negotiationModality.getModalityName();
			}
		}
		return null;
	}
	
	/**
	 * Search participant on list from pk.
	 *
	 * @author mmacalupu
	 * this method is useful to get Bank from idBankPk because with this method
	 * it's not necesary to getParticipant from database.
	 * @param idParticipantPk the id participant pk
	 * @return the participant
	 */
	public Participant searchParticipantOnListFromPK(Long idParticipantPk){
		for(Participant participant: this.getParticipantList()){
			if(participant.getIdParticipantPk().equals(idParticipantPk)){
				return participant;
			}
		}
		return null;
	}
	
	/**
	 * Change instrument type.
	 */
	public void changeInstrumentType(){
		issuerHolderQueryTO.setIdOfferType(null);
		issuerHolderQueryTO.setIdSecurityClass(null);	
		lstCboSecurityClass=new ArrayList<ParameterTable>();
		cleanResults();
	}
	
	/**
	 * Validate issuer.
	 */
	public void validateIssuer(){
		issuerHolderQueryTO.setIdSecurityClass(null);
		issuerHolderQueryTO.setIdSecurityCode(null);
		selectedAccountTO =new HolderAccount();
		selectedAccountTO.setParticipant(new Participant());
		issuerHolderQueryTO.setHolderAccount(new HolderAccount());		
		issuerHolderQueryTO.setHolderHelperMgmt(new Holder());
		lstAccountTo= null;
		cleanResults();
	}
	
	/**
	 * View security financial importance event.
	 *
	 * @param securityCode the security code
	 */
	public void viewSecurityFinancialImportanceEvent(String securityCode)
	{
		SecurityFinancialDataHelpTO securityData = new SecurityFinancialDataHelpTO();
		  securityData.setSecurityCodePk(securityCode);
		  securityData.setUiComponentName("securityHelp");
		  showUIComponent(UICompositeType.SECURITY_FINANCIAL_DATA.getCode(),securityData);
	}
	
	/**
	 * Validate holder.
	 */
	public void validateHolder(){
		
		issuerHolderQueryTO.setIdSecurityClass(null);
		issuerHolderQueryTO.setIdSecurityCode(null);
		issuerHolderQueryTO.setHolderAccount(new HolderAccount());
		selectedAccountTO =new HolderAccount();
		selectedAccountTO.setParticipant(new Participant());
		lstAccountTo=null;
		searchHolderAndHolderAccount();
		cleanResults();
		
	}
	
	public void evaluateOptionSelected(){
//		JSFUtilities.resetViewRoot();
		cleanResults();
		if(optionSelectedOneRadio.equals(Integer.valueOf(0))){
			optionSelectedOneRadio = Integer.valueOf(0);
			issuerHolderQueryTO.setIdSecurityCode(null);
		}else if (optionSelectedOneRadio.equals(Integer.valueOf(1))){
			setIdHolderScreen(null);
			optionSelectedOneRadio = Integer.valueOf(1);
			issuerHolderQueryTO.setIdSecurityClass(null);
			issuerHolderQueryTO.setIdSecurityCode(null);
			issuerHolderQueryTO.setHolderAccount(new HolderAccount());
			selectedAccountTO =new HolderAccount();
			selectedAccountTO.setParticipant(new Participant());
			lstAccountTo=null;
			
			issuerHolderQueryTO.setLstIssuerHolderDetailTO(null);
			issuerHolderQueryTO.setHolderHelperMgmt(new Holder());
		}
	}
		
	
	/**
	 * Gets the issuer holder query to.
	 *
	 * @return the issuer holder query to
	 */
	public IssuerHolderQueryTO getIssuerHolderQueryTO() {
		return issuerHolderQueryTO;
	}
	
	/**
	 * Sets the issuer holder query to.
	 *
	 * @param issuerHolderQueryTO the new issuer holder query to
	 */
	public void setIssuerHolderQueryTO(IssuerHolderQueryTO issuerHolderQueryTO) {
		this.issuerHolderQueryTO = issuerHolderQueryTO;
	}
	
	/**
	 * Gets the lst cbo instrument type.
	 *
	 * @return the lst cbo instrument type
	 */
	public List<ParameterTable> getLstCboInstrumentType() {
		return lstCboInstrumentType;
	}
	
	/**
	 * Sets the lst cbo instrument type.
	 *
	 * @param lstCboInstrumentType the new lst cbo instrument type
	 */
	public void setLstCboInstrumentType(List<ParameterTable> lstCboInstrumentType) {
		this.lstCboInstrumentType = lstCboInstrumentType;
	}
	
	/**
	 * Gets the lst cbo security type.
	 *
	 * @return the lst cbo security type
	 */
	public List<ParameterTable> getLstCboSecurityType() {
		return lstCboSecurityType;
	}
	
	/**
	 * Sets the lst cbo security type.
	 *
	 * @param lstCboSecurityType the new lst cbo security type
	 */
	public void setLstCboSecurityType(List<ParameterTable> lstCboSecurityType) {
		this.lstCboSecurityType = lstCboSecurityType;
	}
	
	/**
	 * Gets the lst cbo security class.
	 *
	 * @return the lst cbo security class
	 */
	public List<ParameterTable> getLstCboSecurityClass() {
		return lstCboSecurityClass;
	}	
	
	/**
	 * Sets the lst cbo security class.
	 *
	 * @param lstCboSecurityClass the new lst cbo security class
	 */
	public void setLstCboSecurityClass(List<ParameterTable> lstCboSecurityClass) {
		this.lstCboSecurityClass = lstCboSecurityClass;
	}
	
	/**
	 * Gets the selected issuer holder detail to.
	 *
	 * @return the selected issuer holder detail to
	 */
	public IssuerHolderDetailTO getSelectedIssuerHolderDetailTO() {
		return selectedIssuerHolderDetailTO;
	}
	
	/**
	 * Sets the selected issuer holder detail to.
	 *
	 * @param selectedIssuerHolderDetailTO the new selected issuer holder detail to
	 */
	public void setSelectedIssuerHolderDetailTO(
			IssuerHolderDetailTO selectedIssuerHolderDetailTO) {
		this.selectedIssuerHolderDetailTO = selectedIssuerHolderDetailTO;
	}
	
	/**
	 * Gets the lst holder account balances.
	 *
	 * @return the lst holder account balances
	 */
	public List<HolderAccountBalance> getLstHolderAccountBalances() {
		return lstHolderAccountBalances;
	}
	
	/**
	 * Sets the lst holder account balances.
	 *
	 * @param lstHolderAccountBalances the new lst holder account balances
	 */
	public void setLstHolderAccountBalances(
			List<HolderAccountBalance> lstHolderAccountBalances) {
		this.lstHolderAccountBalances = lstHolderAccountBalances;
	}
	
	/**
	 * Gets the holder account movement list.
	 *
	 * @return the holder account movement list
	 */
	public List<HolderAccountBalMovDetResultTO> getHolderAccountMovementList() {
		return holderAccountMovementList;
	}
	
	/**
	 * Sets the holder account movement list.
	 *
	 * @param holderAccountMovementList the new holder account movement list
	 */
	public void setHolderAccountMovementList(
			List<HolderAccountBalMovDetResultTO> holderAccountMovementList) {
		this.holderAccountMovementList = holderAccountMovementList;
	}
	
	/**
	 * Gets the market fact balance movement.
	 *
	 * @return the market fact balance movement
	 */
	public MarketFactBalanceTO getMarketFactBalanceMovement() {
		return marketFactBalanceMovement;
	}
	
	/**
	 * Sets the market fact balance movement.
	 *
	 * @param marketFactBalanceMovement the new market fact balance movement
	 */
	public void setMarketFactBalanceMovement(
			MarketFactBalanceTO marketFactBalanceMovement) {
		this.marketFactBalanceMovement = marketFactBalanceMovement;
	}
	
	/**
	 * Gets the negotiation mechanism list.
	 *
	 * @return the negotiation mechanism list
	 */
	public List<NegotiationMechanism> getNegotiationMechanismList() {
		return negotiationMechanismList;
	}
	
	/**
	 * Sets the negotiation mechanism list.
	 *
	 * @param negotiationMechanismList the new negotiation mechanism list
	 */
	public void setNegotiationMechanismList(
			List<NegotiationMechanism> negotiationMechanismList) {
		this.negotiationMechanismList = negotiationMechanismList;
	}
	
	/**
	 * Gets the negotiation modality list.
	 *
	 * @return the negotiation modality list
	 */
	public List<NegotiationModality> getNegotiationModalityList() {
		return negotiationModalityList;
	}
	
	/**
	 * Sets the negotiation modality list.
	 *
	 * @param negotiationModalityList the new negotiation modality list
	 */
	public void setNegotiationModalityList(
			List<NegotiationModality> negotiationModalityList) {
		this.negotiationModalityList = negotiationModalityList;
	}
	
	/**
	 * Gets the participant list.
	 *
	 * @return the participant list
	 */
	public List<Participant> getParticipantList() {
		return participantList;
	}
	
	/**
	 * Sets the participant list.
	 *
	 * @param participantList the new participant list
	 */
	public void setParticipantList(List<Participant> participantList) {
		this.participantList = participantList;
	}
	
	/**
	 * Checks if is issuer user.
	 *
	 * @return true, if is issuer user
	 */
	public boolean isIssuerUser() {
		return issuerUser;
	}
	
	/**
	 * Sets the issuer user.
	 *
	 * @param issuerUser the new issuer user
	 */
	public void setIssuerUser(boolean issuerUser) {
		this.issuerUser = issuerUser;
	}
	
	/**
	 * Gets the security code.
	 *
	 * @return the security code
	 */
	public String getSecurityCode() {
		return securityCode;
	}
	
	/**
	 * Sets the security code.
	 *
	 * @param securityCode the new security code
	 */
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
	
	/**
	 * Gets the lst account to.
	 *
	 * @return the lstAccountTo
	 */
	public GenericDataModel<HolderAccount> getLstAccountTo() {
		return lstAccountTo;
	}
	
	/**
	 * Sets the lst account to.
	 *
	 * @param lstAccountTo the lstAccountTo to set
	 */
	public void setLstAccountTo(GenericDataModel<HolderAccount> lstAccountTo) {
		this.lstAccountTo = lstAccountTo;
	}
	
	/**
	 * Gets the selected account to.
	 *
	 * @return the selectedAccountTO
	 */
	public HolderAccount getSelectedAccountTO() {
		return selectedAccountTO;
	}
	
	/**
	 * Sets the selected account to.
	 *
	 * @param selectedAccountTO the selectedAccountTO to set
	 */
	public void setSelectedAccountTO(HolderAccount selectedAccountTO) {
		this.selectedAccountTO = selectedAccountTO;
	}
	
	/**
	 * Search holder and holder account.
	 */
	/*public void clearGrill(){		
		consolidateParentAccountResultTOList = null;		
	}*/
	public void searchHolderAndHolderAccount(){	
		idHolderScreen=null;
		try {			
			Holder holder = issuerHolderQueryTO.getHolderHelperMgmt();			
			if (Validations.validateIsNull(holder) || Validations.validateIsNull(holder.getIdHolderPk())) {
				return;
			} 
			
			HolderTO holderTO = new HolderTO();
			holderTO.setHolderId(issuerHolderQueryTO.getHolderHelperMgmt().getIdHolderPk());
			holderTO.setFlagAllHolders(true);
			setIdHolderScreen(issuerHolderQueryTO.getHolderHelperMgmt().getIdHolderPk());
			List<HolderAccountHelperResultTO> lista = helperComponentFacade.searchAccountByFilter(holderTO);
			List<HolderAccount> listaAux = new ArrayList<HolderAccount>();
			
			if (lista==null || lista.isEmpty()){
				lstAccountTo = new GenericDataModel<>();
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
												PropertiesUtilities.getGenericMessage(PropertiesConstants.COMMONS_LABEL_PARTICIPANT_CUI_INCORRECT));
				JSFUtilities.showSimpleValidationDialog();
			}else if (!userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
				listaAux= fillHolderAccountHelper(lista);
				lstAccountTo = new GenericDataModel<>(listaAux);
				selectedAccountTO=listaAux.get(0);
				issuerHolderQueryTO.setHolderAccount(selectedAccountTO);
			}
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		
	}
	
	/**
	 * Change select account.
	 */
	public void changeSelectAccount (){
		if(selectedAccountTO!=null){
			issuerHolderQueryTO.setIdSecurityCode(null);
			
			issuerHolderQueryTO.setHolderAccount(selectedAccountTO);
			JSFUtilities.resetComponent("frmIssuerHolderQuery:panelSecurityAccHelp");
		}
	}
		
	/**
	 * Fill holder account helper.
	 *
	 * @param lista the lista
	 * @return the list
	 */
	public List<HolderAccount> fillHolderAccountHelper(List<HolderAccountHelperResultTO> lista){
		List<HolderAccount> listaAux = new ArrayList<HolderAccount>();
		for(HolderAccountHelperResultTO haHelper: lista){
			HolderAccount ha= new HolderAccount();
			ha.setIdHolderAccountPk(haHelper.getAccountPk());
			ha.setAccountNumber(haHelper.getAccountNumber());
			ha.setAlternateCode(haHelper.getAlternateCode());
			ha.setHolderDescriptionList(haHelper.getHolderDescriptionList());
			ha.setStateAccountDescription(haHelper.getAccountStatusDescription());
			ha.setRelatedName(haHelper.getAlternativeCodeIssuer());
			Participant participant = participantServiceBean.find(Participant.class, new Long(haHelper.getParticipant().getIdParticipantPk()));
			haHelper.setParticipant(participant);
			ha.setParticipant(haHelper.getParticipant());
			
			listaAux.add(ha);
		}
		return listaAux;
	}
	
	/**
	 * Gets the holder account request.
	 *
	 * @return the holderAccountRequest
	 */
	public HolderAccount getHolderAccountRequest() {
		return holderAccountRequest;
	}
	
	/**
	 * Sets the holder account request.
	 *
	 * @param holderAccountRequest the holderAccountRequest to set
	 */
	public void setHolderAccountRequest(HolderAccount holderAccountRequest) {
		this.holderAccountRequest = holderAccountRequest;
	}
	
	/**
	 * <ul>
	 * <li>
	 * Project iDepositary.
	 * Copyright PraderaTechnologies 2015.</li>
	 * </ul>
	 * 
	 * The Class HeaderFooter.
	 *
	 * @author PraderaTechnologies.
	 * @version 1.0 , 21/08/2015
	 */
	static class HeaderFooter extends PdfPageEventHelper {

		/** The tpl. */
		public PdfTemplate tpl;
		private PdfTemplate pdfTemplateDates;
		  
		/* (non-Javadoc)
		 * @see com.itextpdf.text.pdf.PdfPageEventHelper#onStartPage(com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document)
		 */
		public void onStartPage(PdfWriter writer, Document document) {
			try{
			Image imghead = null;
			//imghead = Image.getInstance(this.getClass().getResource("/img/edv-logo.jpg"));
			
			InputStream is = this.getClass().getResourceAsStream("/img/edv-logo.jpg");
			imghead = Image.getInstance(IOUtils.toByteArray(is));
			
			imghead.setAbsolutePosition(20,0);
			imghead.scaleAbsolute(100,33);
			PdfContentByte cbCabecera = writer.getDirectContent();
			tpl = cbCabecera.createTemplate(300,150);
            tpl.addImage(imghead);
            
            cbCabecera.addTemplate(tpl, 0, 775);
			//ColumnText.showTextAligned(cbCabecera, Element.ALIGN_LEFT, new Phrase(""), 200,830,0);
            
			} catch (BadElementException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        } catch (DocumentException e) {
	            e.printStackTrace();
	        }
		}
    }
	
	/**
	 * Prints the.
	 */
	public void print() 
	{
		try{		
			
		//Generamos el archivo PDF
        String directorioArchivos;
        
        ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        directorioArchivos = ctx.getRealPath("/");
        String name = directorioArchivos + GeneralConstants.STR_DOCUMENT_PDF;
        String dato = "";
        Document document = new Document(PageSize.A4);
        
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(name));
        
        HeaderFooter header = new HeaderFooter();
        writer.setPageEvent(header);
        
        document.open();
        /*datos del reporte fecha, hora, usuario*/
        SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy hh:mm a"); 
        Paragraph p = new Paragraph(dt.format(new Date())+
        		"\n"+userInfo.getUserAccountSession().getUserName()+
        		"\nCPYRPT101",new Font(FontFamily.HELVETICA,5,Font.NORMAL));
        p.setAlignment(Element.ALIGN_RIGHT);
        document.add(p);
        
        Paragraph parrafo= new Paragraph(GeneralConstants.STR_EVIDENCY_OWNERSHIP,new Font(FontFamily.HELVETICA,20,Font.BOLD));
        parrafo.setAlignment(Element.ALIGN_CENTER);
        document.add(parrafo);
        
        dt = new SimpleDateFormat("dd/MM/yyyy"); 
        Paragraph pp = new Paragraph("Al: "+dt.format(new Date()),new Font(FontFamily.HELVETICA,10,Font.NORMAL));
        pp.setAlignment(Element.ALIGN_CENTER);
        document.add(pp);
        
        document.add(new Paragraph(GeneralConstants.STR_NEW_LINEA+GeneralConstants.STR_NEW_LINEA + GeneralConstants.STR_INFO_HOLDER,new Font(FontFamily.HELVETICA,15,Font.BOLD)));
        document.add(new Paragraph(GeneralConstants.STR_NEW_LINEA+SPACING+GeneralConstants.STR_HOLDER_ACCOUNT +SPACING+ issuerHolderDetailTO.getHolderAccountBalance().getHolderAccount().getAccountNumber() ));
        //document.add(new Paragraph(SPACING+GeneralConstants.STR_COMPONENT+SPACING));
        List<HolderTO> lstHolder=issuerHolderDetailTO.getLstHolderTO().getDataList();
        for(HolderTO itHolder:lstHolder){
        	 dato=GeneralConstants.EMPTY_STRING;
        	 if (Validations.validateIsNotNullAndNotEmpty(itHolder.getName()))
        		 dato=" - " + itHolder.getName().toString();
        	document.add(new Paragraph(SPACING+
        	GeneralConstants.STR_CUI+SPACING+SPACING+"\t            "+itHolder.getHolderId()+
            GeneralConstants.STR_NEW_LINEA+SPACING+"Nombre Completo : "+SPACING+"    "+itHolder.getFullName()+ 
            GeneralConstants.STR_NEW_LINEA+SPACING+itHolder.getDocumentTypeDesc()+" :"+SPACING+SPACING+"             "+ itHolder.getDocumentNumber()  + dato));
        }
        document.add(new Paragraph(GeneralConstants.STR_NEW_LINEA+GeneralConstants.STR_INFO_SECURITY,new Font(FontFamily.HELVETICA,15,Font.BOLD)));
        if(Validations.validateIsNotNull(issuerHolderDetailTO.getSecurity().getClassTypeDescription()))
        	dato=issuerHolderDetailTO.getSecurity().getClassTypeDescription();
        document.add(new Paragraph(GeneralConstants.STR_NEW_LINEA+SPACING+GeneralConstants.STR_SECURITY_CLASS +SPACING+dato));
        dato=GeneralConstants.EMPTY_STRING;
        if(Validations.validateIsNotNull(issuerHolderDetailTO.getSecurity().getIdSecurityCodePk()))
        	dato= issuerHolderDetailTO.getSecurity().getIdSecurityCodeOnly();
        document.add(new Paragraph(SPACING+GeneralConstants.STR_SECURITY_CODE +SPACING+dato));
        dato=GeneralConstants.EMPTY_STRING;
        if(Validations.validateIsNotNull(issuerHolderDetailTO.getSecurity().getCurrentNominalValue()))
        	dato=CommonsUtilities.formatDecimal(issuerHolderDetailTO.getSecurity().getCurrentNominalValue(), PropertiesConstants.QUANTITY_FORMAT_DECIMAL);
        document.add(new Paragraph(SPACING+GeneralConstants.STR_NOMINAL_VALUE+SPACING +dato));
        dato=GeneralConstants.EMPTY_STRING;
        if(Validations.validateIsNotNull(issuerHolderDetailTO.getSecurity().getCurrencyName()))
        	dato=issuerHolderDetailTO.getSecurity().getCurrencyName();
        document.add(new Paragraph(SPACING +GeneralConstants.STR_CURRENCY +SPACING+dato));
        dato=GeneralConstants.EMPTY_STRING;
        if(Validations.validateIsNotNull(issuerHolderDetailTO.getSecurity().getAlternativeCode()))
        	dato=issuerHolderDetailTO.getSecurity().getAlternativeCode();
        document.add(new Paragraph(SPACING+GeneralConstants.STR_SERIE_ALTERNATIVE +SPACING+dato));
        dato=GeneralConstants.EMPTY_STRING;
        if(Validations.validateIsNotNull(issuerHolderDetailTO.getSecurity().getIssuanceDate()))
        	dato= CommonsUtilities.convertDatetoString(issuerHolderDetailTO.getSecurity().getIssuanceDate());
        document.add(new Paragraph(SPACING+GeneralConstants.STR_ISSUE_DATE +SPACING+dato));
        dato=GeneralConstants.EMPTY_STRING;
        if(Validations.validateIsNotNull(issuerHolderDetailTO.getSecurity().getSecurityDaysTerm()))
        	dato=CommonsUtilities.formatDecimal( issuerHolderDetailTO.getSecurity().getSecurityDaysTerm(), PropertiesConstants.INTEGER_FORMAT_NUMBER);
        document.add(new Paragraph(SPACING+GeneralConstants.STR_LAST_TERM +SPACING+dato));
        dato=GeneralConstants.EMPTY_STRING;
        if(Validations.validateIsNotNull(issuerHolderDetailTO.getSecurity().getExpirationDate()))
        	dato=CommonsUtilities.convertDatetoString(issuerHolderDetailTO.getSecurity().getExpirationDate());
        document.add(new Paragraph(SPACING+GeneralConstants.STR_EXPIRATE_DATE +SPACING+dato));
        dato=GeneralConstants.EMPTY_STRING;
        if(Validations.validateIsNotNull(issuerHolderDetailTO.getSecurity().getInterestRate()))
        	dato=CommonsUtilities.formatDecimal(issuerHolderDetailTO.getSecurity().getInterestRate(), PropertiesConstants.PERCENT_FORMAT_DECIMAL);
        document.add(new Paragraph(SPACING+GeneralConstants.STR_ISSUE_RATE +SPACING+ dato));
        dato=GeneralConstants.EMPTY_STRING;
        if(Validations.validateIsNotNull(issuerHolderDetailTO.getSecurity().getObservations()))
        	dato=issuerHolderDetailTO.getSecurity().getObservations();
        document.add(new Paragraph(SPACING+GeneralConstants.STR_CONDITION +SPACING+dato));
        dato=GeneralConstants.EMPTY_STRING;
        if(Validations.validateIsNotNull(issuerHolderDetailTO.getSecurity().getDescription())){;
        	dato=issuerHolderDetailTO.getSecurity().getDescription();}
        document.add(new Paragraph(SPACING+GeneralConstants.STR_BALANCE +SPACING+ dato));
        document.close();
  
        //Abrimos el archivo PDF
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.setContentType("application/pdf");
        response.setHeader("Content-disposition",
                "inline=filename=" + name);
        try {
            response.getOutputStream().write(getBytesFromFile(new File(name)));  
            response.getOutputStream().flush();
            response.getOutputStream().close();
            context.responseComplete();
        } catch (IOException e) {
            e.printStackTrace();
        }
    } catch (Exception e) {
        e.printStackTrace();
    }
	}
	
	/**
	 * Gets the bytes from file.
	 *
	 * @param file the file
	 * @return the bytes from file
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static byte[] getBytesFromFile(File file) throws IOException {
        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            throw new IOException("File is too large!");
        }
        byte[] bytes = new byte[(int) length];
        int offset = 0;
        int numRead = 0;
        InputStream is = new FileInputStream(file);
        try {
            while (offset < bytes.length
                    && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
                offset += numRead;
            }
        } finally {
            is.close();
        }
        if (offset < bytes.length) {
            throw new IOException(PropertiesConstants.STRING_ERROR_WHRITEEN + file.getName());
        }
        return bytes;
    }
	
	/**
	 * Gets the issuer holder detail to.
	 *
	 * @return the issuerHolderDetailTO
	 */
	public IssuerHolderDetailTO getIssuerHolderDetailTO() {
		return issuerHolderDetailTO;
	}
	
	/**
	 * Sets the issuer holder detail to.
	 *
	 * @param issuerHolderDetailTO the issuerHolderDetailTO to set
	 */
	public void setIssuerHolderDetailTO(IssuerHolderDetailTO issuerHolderDetailTO) {
		this.issuerHolderDetailTO = issuerHolderDetailTO;
	}
	
	/**
	 * Checks if is participant user.
	 *
	 * @return the participantUser
	 */
	public boolean isParticipantUser() {
		return participantUser;
	}
	
	/**
	 * Sets the participant user.
	 *
	 * @param participantUser the participantUser to set
	 */
	public void setParticipantUser(boolean participantUser) {
		this.participantUser = participantUser;
	}
	/**
	 * Gets the option selected one radio.
	 * @return
	 */
	public Integer getOptionSelectedOneRadio() {
		return optionSelectedOneRadio;
	}
	/**
	 * Sets the option selected one radio.
	 * @param optionSelectedOneRadio
	 */
	public void setOptionSelectedOneRadio(Integer optionSelectedOneRadio) {
		this.optionSelectedOneRadio = optionSelectedOneRadio;
	}
	public Long getIdHolderScreen() {
		return idHolderScreen;
	}
	public void setIdHolderScreen(Long idHolderScreen) {
		this.idHolderScreen = idHolderScreen;
	}
	
	
	
}
