package com.pradera.accounts.query.service;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;

import com.pradera.accounts.query.to.HolderAccountBalMovDetResultTO;
import com.pradera.accounts.query.to.IssuerHolderDetailTO;
import com.pradera.accounts.query.to.IssuerHolderQueryTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.to.IssuerSearcherTO;
import com.pradera.core.component.operation.to.MarketFactBalanceTO;
import com.pradera.core.component.operation.to.MarketFactDetailTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountGroupType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountBalancePK;
import com.pradera.model.component.HolderAccountMovement;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class IssuerHolderQueryServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/08/2015
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class IssuerHolderQueryServiceBean extends CrudDaoServiceBean{


	/** The parameter service bean. */
	@EJB
	ParameterServiceBean parameterServiceBean;
	
	/**
	 * Search issuer holder query.
	 *
	 * @param issuerHolderQueryTO the issuer holder query to
	 * @param issuerUser the issuer user
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings({ "unchecked"})
	public List<IssuerHolderDetailTO> searchIssuerHolderQuery(IssuerHolderQueryTO issuerHolderQueryTO, boolean issuerUser)throws ServiceException{
		try{
			Map<String, Object> parameters = new HashMap<String, Object>();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("	Select DISTINCT se.idSecurityCodePk, se.issuanceDate,	");//0,1
			stringBuilder.append("	se.expirationDate, se.currentNominalValue,	"); //2,3
			stringBuilder.append("	se.interestRate, hab.totalBalance,	"); //4,5
			stringBuilder.append("	se.stateSecurity, (Select p.parameterName From ParameterTable p Where p.parameterTablePk = se.stateSecurity),  "); //6,7
			stringBuilder.append("	ho.idHolderPk, 	");	//8
			stringBuilder.append("	hab.pawnBalance, ");	//9
			stringBuilder.append("	hab.banBalance, ");	//10
			stringBuilder.append("	hab.otherBlockBalance, ");	//11
			stringBuilder.append("	hab.reportingBalance, ");	//12
			stringBuilder.append("	hab.accreditationBalance, ");	//13
			stringBuilder.append("	hab.saleBalance, ");	//14
			stringBuilder.append("	hab.transitBalance, ");	//15
			stringBuilder.append("	(Select p.parameterName From ParameterTable p Where p.parameterTablePk = se.currency), ");	//16
			stringBuilder.append("	(Select p.text1 From ParameterTable p Where p.parameterTablePk = se.securityClass),"); //17
			stringBuilder.append("  se.securityDaysTerm,  ");	//18
			stringBuilder.append("  hab.holderAccount.idHolderAccountPk,  ");	//19
			stringBuilder.append("  se.idSecurityCodeOnly, ");	//20
			stringBuilder.append("  se.alternativeCode, ");	//21
			stringBuilder.append("  hab.holderAccount.accountNumber, ");	//22
			stringBuilder.append("	hab.reportedBalance ");	//23 
			stringBuilder.append("	from Holder ho,HolderAccountDetail had,HolderAccount hol,	");
			stringBuilder.append("	HolderAccountBalance hab,Security se,Issuance iss,Issuer isss	");
			stringBuilder.append("	where ho.idHolderPk = had.holder.idHolderPk	");
			stringBuilder.append("	and had.holderAccount.idHolderAccountPk = hol.idHolderAccountPk	");			
			stringBuilder.append("	and had.holderAccount.idHolderAccountPk = hab.holderAccount.idHolderAccountPk	");
			stringBuilder.append("	and hab.security.idSecurityCodePk = se.idSecurityCodePk	");
			stringBuilder.append("	and hab.participant.idParticipantPk = hol.participant.idParticipantPk	");
			stringBuilder.append("	and se.issuance.idIssuanceCodePk = iss.idIssuanceCodePk	");
			stringBuilder.append("	and iss.issuer.idIssuerPk = isss.idIssuerPk	");	
			stringBuilder.append("	and had.holderAccount.stateAccount = :stateAccount ");
			stringBuilder.append("	and had.holderAccount.accountGroup = :accountGroup ");
			stringBuilder.append("	and (se.desmaterializedBalance<>0 or se.stateSecurity <> 2033) ");
			stringBuilder.append("	and hab.totalBalance>0 ");
			parameters.put("stateAccount", HolderAccountStatusType.ACTIVE.getCode());
			parameters.put("accountGroup", HolderAccountGroupType.INVERSTOR.getCode());
			if(Validations.validateIsNotNull(issuerHolderQueryTO.getIssuerHelperMgmt()) && 
					(Validations.validateIsNotNull(issuerHolderQueryTO.getIssuerHelperMgmt().getIdIssuerPk()))){
				stringBuilder.append("	AND	isss.idIssuerPk = :idIssuerPk	");
				parameters.put("idIssuerPk", issuerHolderQueryTO.getIssuerHelperMgmt().getIdIssuerPk());							
			}
			if(Validations.validateIsNotNull(issuerHolderQueryTO.getHolderAccount()) && 
					(Validations.validateIsNotNull(issuerHolderQueryTO.getHolderAccount().getIdHolderAccountPk()) ) &&  !issuerUser){
				stringBuilder.append("	AND	hol.idHolderAccountPk = :idHolderAccountPk	");
				parameters.put("idHolderAccountPk", issuerHolderQueryTO.getHolderAccount().getIdHolderAccountPk());							
			}
			if(Validations.validateIsNotNull(issuerHolderQueryTO.getHolderHelperMgmt()) && 
					Validations.validateIsPositiveNumber(issuerHolderQueryTO.getHolderHelperMgmt().getIdHolderPk())){
				stringBuilder.append("	AND	ho.idHolderPk = :idHolderPk	");
				parameters.put("idHolderPk", issuerHolderQueryTO.getHolderHelperMgmt().getIdHolderPk());							
			}
			if(Validations.validateIsNotNull(issuerHolderQueryTO.getIdSecurityClass()) 
					&& Validations.validateIsPositiveNumber(new Long(issuerHolderQueryTO.getIdSecurityClass()))){
				stringBuilder.append("	AND	se.securityClass = :securityClass	");
				parameters.put("securityClass", issuerHolderQueryTO.getIdSecurityClass());							
			}
//			else {
//				if(issuerUser){
//					stringBuilder.append("	AND	se.securityClass in (1976,420)	");
//				}
//			}
			if(Validations.validateIsNotNull(issuerHolderQueryTO.getIdSecurityCode())){
				stringBuilder.append("	AND	se.idSecurityCodePk = :idSecurityCodePk	");
				parameters.put("idSecurityCodePk", issuerHolderQueryTO.getIdSecurityCode());							
			}
			
			List<Object[]> lstIssuerHolderDetailTO =  findListByQueryString(stringBuilder.toString(), parameters);
			Map<String,IssuerHolderDetailTO> issuerHolderDetailTOPk = new HashMap<String, IssuerHolderDetailTO>();
			for(Object[] object: lstIssuerHolderDetailTO){
				IssuerHolderDetailTO issuerHolderDetailTO = new IssuerHolderDetailTO();
				Security security = new Security();
				if(object[0]!=null){
					security.setIdSecurityCodePk(object[0].toString());
				}
				if(object[1]!=null){
					security.setIssuanceDate((Date)object[1]);
				}	
				if(object[2]!=null){
					security.setExpirationDate((Date)object[2]);
				}
				if(object[3]!=null){
					security.setCurrentNominalValue((BigDecimal)object[3]);
				}
				if(object[4]!=null){
					security.setInterestRate((BigDecimal)object[4]);
				}
				if(object[5]!=null){
					security.setDesmaterializedBalance((BigDecimal)object[5]);
				}
				if(object[6]!=null){
					security.setStateSecurity((Integer)object[6]);
				}
				if(object[16]!=null){
					security.setCurrencyName((String)object[16]);
				}
				if(object[17]!=null){
					security.setClassTypeDescription((String)object[17]);
				}
				if(object[18]!=null){
					security.setSecurityDaysTerm((Integer)object[18]);
				}
				if(object[20]!=null){
					security.setIdSecurityCodeOnly((String)object[20]);
				}
				if(object[21]!=null){
					security.setAlternativeCode((String)object[21]);
				}
				issuerHolderDetailTO.setSecurity(security);
				Holder holder = new Holder();
				if(object[8]!=null){
					holder.setIdHolderPk((Long)object[8]);
				}
				
				HolderAccountBalance holderAccountBalance=new HolderAccountBalance();
				HolderAccount holderAccount=new HolderAccount((long)object[19]);
				holderAccount.setAccountNumber((Integer)object[22]);
				BigDecimal sumBalance=new BigDecimal(0);
				if(object[9].toString().equals("1"))
					security.setDescription("GRAVAMEN");
				else if (object[10].toString().equals("1"))
					security.setDescription("EMBARGO");
				else if (object[11].toString().equals("1"))
					security.setDescription("OTRO BLOQUEO");
				else if (object[12].toString().equals("1"))
					security.setDescription("REPORTADOR");
				else if (object[13].toString().equals("1"))
					security.setDescription("ACREDITACION");
				else if (object[14].toString().equals("1"))
					security.setDescription("VENTA");
				else if (object[15].toString().equals("1"))
					security.setDescription("TRANSITO");
				else if (object[23].toString().equals("1"))
					security.setDescription("REPORTADOR");
				else security.setDescription("DISPONIBLE");
					
				sumBalance=sumBalance.add(new BigDecimal(object[9].toString())).add(new BigDecimal(object[10].toString())).add(new BigDecimal(object[11].toString()));
				sumBalance=sumBalance.add(new BigDecimal(object[12].toString())).add(new BigDecimal(object[13].toString())).add(new BigDecimal(object[14].toString()));
				sumBalance=sumBalance.add(new BigDecimal(object[15].toString()));
				if (sumBalance.compareTo(new BigDecimal(0))>0){
					security.setObservations("NO DISPONIBLE");
				}else 
					security.setObservations("DISPONIBLE");
				holderAccountBalance.setHolderAccount(holderAccount);
				issuerHolderDetailTO.setHolderAccountBalance(holderAccountBalance);
				issuerHolderDetailTO.setHolder(holder);
				issuerHolderDetailTOPk.put(issuerHolderDetailTO.getSecurity().getIdSecurityCodePk(), issuerHolderDetailTO);
			}
			return new ArrayList<IssuerHolderDetailTO>(issuerHolderDetailTOPk.values());
			}catch(NoResultException ex){
				   return null;
			} catch(NonUniqueResultException ex){
				   return null;
			}
	}
	
	/**
	 * Search holder account balance.
	 *
	 * @param issuerHolderDetailTO the issuer holder detail to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings({ "unchecked"})
	public List<HolderAccountBalance> searchHolderAccountBalance(IssuerHolderDetailTO issuerHolderDetailTO)throws ServiceException{
		try{
			Map<String, Object> parameters = new HashMap<String, Object>();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("	Select hab.security.idSecurityCodePk, hab.participant.idParticipantPk,hab.participant.mnemonic,	");//0,1,2
			stringBuilder.append("	hab.holderAccount.idHolderAccountPk, hab.holderAccount.accountNumber,	"); //3,4
			stringBuilder.append("	hab.totalBalance,hab.participant.description,hab.holderAccount.alternateCode,hab.security.description,hab.security.cfiCode,hol.description	"); //5,6,7,8,9,10
			stringBuilder.append("	from HolderAccountBalance hab, HolderAccountDetail had,HolderAccount hol	");
			stringBuilder.append("	where hab.holderAccount.idHolderAccountPk = hol.idHolderAccountPk	");
			stringBuilder.append("	and hol.idHolderAccountPk = had.holderAccount.idHolderAccountPk 	");		
			stringBuilder.append("	and had.holderAccount.stateAccount = :stateAccount ");
			stringBuilder.append("	and had.holderAccount.accountGroup = :accountGroup ");	
			parameters.put("stateAccount", HolderAccountStatusType.ACTIVE.getCode());
			parameters.put("accountGroup", HolderAccountGroupType.INVERSTOR.getCode());
			if(Validations.validateIsNotNull(issuerHolderDetailTO.getHolder()) && 
					Validations.validateIsPositiveNumber(issuerHolderDetailTO.getHolder().getIdHolderPk())){
				stringBuilder.append("	and had.holder.idHolderPk = :idHolderPk	");	
				parameters.put("idHolderPk", issuerHolderDetailTO.getHolder().getIdHolderPk());					
			}										
			if(Validations.validateIsNotNull(issuerHolderDetailTO.getSecurity()) && 
					Validations.validateIsNotNull(issuerHolderDetailTO.getSecurity().getIdSecurityCodePk())){
				stringBuilder.append("	and hab.security.idSecurityCodePk = :idSecurityCodePk	");		
				parameters.put("idSecurityCodePk", issuerHolderDetailTO.getSecurity().getIdSecurityCodePk());		
			}			
			List<Object[]> lstHolderAccountBalance =  findListByQueryString(stringBuilder.toString(), parameters);
			Map<HolderAccountBalancePK,HolderAccountBalance> holderAccountBalancePk = new HashMap<HolderAccountBalancePK, HolderAccountBalance>();
			for(Object[] object: lstHolderAccountBalance){
				HolderAccountBalancePK holderAccountBalancePK = new HolderAccountBalancePK();
				HolderAccountBalance holderAccountBalance = new HolderAccountBalance();
				holderAccountBalancePK.setIdSecurityCodePk(object[0].toString());	
				holderAccountBalancePK.setIdParticipantPk(new Long(object[1].toString()));
				holderAccountBalancePK.setIdHolderAccountPk(new Long(object[3].toString()));
				holderAccountBalance.setId(holderAccountBalancePK);
				Participant participant = new Participant();
				participant.setIdParticipantPk(new Long(object[1].toString()));
				if(object[2]!=null){
					participant.setMnemonic(object[2].toString());
				}
				if(object[6]!=null){
					participant.setDescription(object[6].toString());
				}				
				holderAccountBalance.setParticipant(participant);
				HolderAccount holderAccount = new HolderAccount();
				holderAccount.setIdHolderAccountPk(new Long(object[3].toString()));
				if(object[4]!=null){
					holderAccount.setAccountNumber(new Integer(object[4].toString()));
				}
				if(object[7]!=null){
					holderAccount.setAlternateCode(object[7].toString());
				}
				Map<String, Object> parametersAux = new HashMap<String, Object>();
				StringBuilder stringBuilderAux = new StringBuilder();
				stringBuilderAux.append("	Select had.holder.idHolderPk, had.holder.holderType ,had.holder.firstLastName,	");//0,1,2,
				stringBuilderAux.append("	had.holder.secondLastName ,had.holder.name ,had.holder.fullName	");//3,4,5
				stringBuilderAux.append("	from HolderAccountDetail had	");
				stringBuilderAux.append("	where had.holderAccount.idHolderAccountPk = :idHolderAccountPk	");
				parametersAux.put("idHolderAccountPk", holderAccount.getIdHolderAccountPk());
				List<Object[]> lstHolderAccountDetailAux = findListByQueryString(stringBuilderAux.toString(), parametersAux);
				List<HolderAccountDetail>lstHolderAccountDetail = new ArrayList<HolderAccountDetail>();
				for(Object[] objectAux: lstHolderAccountDetailAux){
					HolderAccountDetail objHolderAccountDetail = new HolderAccountDetail();
					Holder objHolder = new Holder();
					objHolder.setIdHolderPk((Long)objectAux[0]);
					if(objectAux[1]!=null){
						objHolder.setHolderType((Integer)objectAux[1]);
					}
					if(objectAux[2]!=null){
						objHolder.setFirstLastName(objectAux[2].toString());				
					}
					if(objectAux[3]!=null){
						objHolder.setSecondLastName(objectAux[3].toString());
					}
					if(objectAux[4]!=null){
						objHolder.setName(objectAux[4].toString());
					}					
					if(objectAux[5]!=null){
						objHolder.setFullName(objectAux[5].toString());
					}
					objHolderAccountDetail.setHolder(objHolder);
					lstHolderAccountDetail.add(objHolderAccountDetail);
				}
				holderAccount.setHolderAccountDetails(lstHolderAccountDetail);
				if(object[10]!=null){
					holderAccount.setAccountDescription(object[10].toString());
				}
				holderAccountBalance.setHolderAccount(holderAccount);
				Security objSecurity = new Security();
				objSecurity.setIdSecurityCodePk(object[0].toString());
				if(object[8]!=null){
					objSecurity.setDescription(object[8].toString());
				}
				if(object[9]!=null){
					objSecurity.setCfiCode(object[9].toString());
				}
				holderAccountBalance.setSecurity(objSecurity);
				holderAccountBalance.setTotalBalance(new BigDecimal((object[5].toString())));
				holderAccountBalancePk.put(holderAccountBalancePK, holderAccountBalance);
			}
			return new ArrayList<HolderAccountBalance>(holderAccountBalancePk.values());
			}catch(NoResultException ex){
				   return null;
			} catch(NonUniqueResultException ex){
				   return null;
			}
	}
	
	/**
	 * Gets the list securities class setup.
	 *
	 * @param isIssuerDpf the is issuer dpf
	 * @return the list securities class setup
	 */
	@SuppressWarnings({ "unchecked"})
	public List<ParameterTable> getListSecuritiesClassSetup(boolean isIssuerDpf) {
		List<ParameterTable> lstSecuritiesClassPrm=null;
		List<ParameterTable> lstEconomicActivityPrmAux=new ArrayList<ParameterTable>();		
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		filterParameterTable.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		filterParameterTable.setOrderByText1(BooleanType.YES.getCode());	
		lstSecuritiesClassPrm= parameterServiceBean.getListParameterTableServiceBean(filterParameterTable);
		if(isIssuerDpf){
			for(ParameterTable pTable : lstSecuritiesClassPrm){
				if((SecurityClassType.DPA.getCode().compareTo( pTable.getParameterTablePk())==0)
						||(SecurityClassType.DPF.getCode().compareTo( pTable.getParameterTablePk())==0)){
					lstEconomicActivityPrmAux.add(pTable);
				}
			} 
			return lstEconomicActivityPrmAux;
		} else {
			return lstSecuritiesClassPrm;
			}
	}
	
	/**
	 * Checks for balance.
	 *
	 * @param holderAccountBalance the holder account balance
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings({ "unchecked"})
	public boolean hasBalance(HolderAccountBalance holderAccountBalance) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT nvl(count(hab),0) FROM HolderAccountBalance hab");
		sbQuery.append("	WHERE hab.holderAccount.idHolderAccountPk = :idHolderAccountPk");
		sbQuery.append("	AND	hab.security.idSecurityCodePk = :idSecurityCodePk");
		sbQuery.append("	AND hab.participant.idParticipantPk = :idParticipantPk");			
		Query query = em.createQuery(sbQuery.toString());		
		query.setParameter("idHolderAccountPk", holderAccountBalance.getId().getIdHolderAccountPk());
		query.setParameter("idParticipantPk", holderAccountBalance.getId().getIdParticipantPk());
		query.setParameter("idSecurityCodePk", holderAccountBalance.getId().getIdSecurityCodePk());
		
		Long count  = (Long)query.getSingleResult();	
		if(count >= 1){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Gets the holder account balance.
	 *
	 * @param holderAccountBalance the holder account balance
	 * @return the holder account balance
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings({ "unchecked"})
	public List<HolderAccountBalance> getHolderAccountBalance(HolderAccountBalance holderAccountBalance) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT hab FROM HolderAccountBalance hab");
		sbQuery.append("	WHERE hab.holderAccount.idHolderAccountPk = :idHolderAccountPk");
		sbQuery.append("	AND	hab.security.idSecurityCodePk = :idSecurityCodePk");
		sbQuery.append("	AND hab.participant.idParticipantPk = :idParticipantPk");
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idHolderAccountPk",  holderAccountBalance.getId().getIdHolderAccountPk());
		parameters.put("idParticipantPk", holderAccountBalance.getId().getIdParticipantPk());
		parameters.put("idSecurityCodePk", holderAccountBalance.getId().getIdSecurityCodePk());
		return (List<HolderAccountBalance>)findListByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Gets the holder account movement.
	 *
	 * @param holderAccountBalance the holder account balance
	 * @return the holder account movement
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings({ "unchecked"})
	public List<HolderAccountMovement> getHolderAccountMovement(HolderAccountBalance holderAccountBalance) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ham FROM HolderAccountMovement ham");
		sbQuery.append("	INNER JOIN FETCH ham.movementType");
		sbQuery.append("	WHERE ham.holderAccountBalance.holderAccount.idHolderAccountPk = :idHolderAccountPk");
		sbQuery.append("	AND	ham.holderAccountBalance.security.idSecurityCodePk = :idSecurityCodePk");
		sbQuery.append("	AND ham.holderAccountBalance.participant.idParticipantPk = :idParticipantPk");
		sbQuery.append("	ORDER BY ham.movementDate asc");
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idHolderAccountPk",holderAccountBalance.getId().getIdHolderAccountPk());
		parameters.put("idParticipantPk", holderAccountBalance.getId().getIdParticipantPk());
		parameters.put("idSecurityCodePk",  holderAccountBalance.getId().getIdSecurityCodePk());
		return (List<HolderAccountMovement>)findListByQueryString(sbQuery.toString(), parameters);
	}
	/**
	 * Gets the holder account mov list service bean.
	 *
	 * @param holderAccountBalanceTO the holder account balance to
	 * @return the holder account mov list service bean
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings({ "unchecked"})
	public List<HolderAccountBalMovDetResultTO> getHolderAccountMovListServiceBean(HolderAccountBalanceTO holderAccountBalanceTO) throws ServiceException{
		StringBuilder jpqlQuery = new StringBuilder();
		Object objAux = null;
		jpqlQuery.append("SELECT  ham.id_holder_account_movement_pk, ham.OPERATION_NUMBER, ham.OPERATION_DATE, ham.ID_SOURCE_PARTICIPANT, ham.ID_TARGET_PARTICIPANT, ");
		jpqlQuery.append(" 		  ham.MOVEMENT_QUANTITY, ham.OPERATION_PRICE, ham.ID_MODALITY, ham.MOVEMENT_DATE, ham.ID_MECHANISM, ");
		jpqlQuery.append(" 		  mvt.MOVEMENT_NAME, ");
		jpqlQuery.append(" 		  CASE  mvb.id_behavior ");
		jpqlQuery.append(" 		  	WHEN 1 THEN  'SUMA - ' ");
		jpqlQuery.append(" 		  	WHEN 2 THEN 'RESTA - ' ");
		jpqlQuery.append(" 		  	ELSE  '' ");
		jpqlQuery.append(" 		  END || balTyp.balance_name AS track ");
		jpqlQuery.append("FROM HOLDER_ACCOUNT_MOVEMENT ham ");
		jpqlQuery.append("	INNER JOIN MOVEMENT_TYPE mvt ON mvt.ID_MOVEMENT_TYPE_PK = ham.ID_MOVEMENT_TYPE_FK ");
		jpqlQuery.append("	LEFT OUTER JOIN MOVEMENT_BEHAVIOR mvb ON mvb.ID_MOVEMENT_TYPE_FK = mvt.ID_MOVEMENT_TYPE_PK ");
		jpqlQuery.append("	LEFT OUTER JOIN BALANCE_TYPE balTyp ON balTyp.id_balance_type_pk = mvb.id_balance_type_fk ");
		jpqlQuery.append("	WHERE ham.ID_PARTICIPANT_FK = :idParticipantParam AND ");
		jpqlQuery.append(" 		  ham.ID_HOLDER_ACCOUNT_FK = :idHolderAccountParam AND ");
		jpqlQuery.append(" 		  mvt.IND_VISIBILITY = :onePara	AND ");
		jpqlQuery.append(" 		  ham.ID_SECURITY_CODE_FK = :idSecurityCodeParam ");
		jpqlQuery.append(" 		  ORDER BY ham.MOVEMENT_DATE DESC, ham.id_holder_account_movement_pk DESC, ham.OPERATION_NUMBER DESC, ham.OPERATION_DATE DESC,"); 
		jpqlQuery.append(" 		  		   ham.ID_SOURCE_PARTICIPANT DESC, ham.ID_TARGET_PARTICIPANT DESC");
		
		Query queryJpql = em.createNativeQuery(jpqlQuery.toString());
		queryJpql.setParameter("idParticipantParam", holderAccountBalanceTO.getIdParticipant());
		queryJpql.setParameter("idHolderAccountParam", holderAccountBalanceTO.getIdHolderAccount());
		queryJpql.setParameter("idSecurityCodeParam", holderAccountBalanceTO.getIdSecurityCode());
		queryJpql.setParameter("onePara", BooleanType.YES.getCode());
		
		List<Object[]> objectData = null;
		try{
			objAux = queryJpql.getResultList();
			if(objAux!=null){
				objectData = (List<Object[]>)objAux;
			}else{
				return null;
			}
		}catch(NoResultException ex){
			return null;
		}
		
		List<HolderAccountBalMovDetResultTO> holderAccountMovementList = new ArrayList<HolderAccountBalMovDetResultTO>();
		
		for(Object[] movementObjet: objectData){
			List<String> movDetail = new ArrayList<String>();
			HolderAccountBalMovDetResultTO holderAccountMovement = new HolderAccountBalMovDetResultTO();
			holderAccountMovement.setIdHolderAccountMovementPk(((BigDecimal)movementObjet[0]).longValue());
			holderAccountMovement.setOperationNumber(((BigDecimal)(movementObjet[1]==null?BigDecimal.ZERO:movementObjet[1])).longValue());
			holderAccountMovement.setOperationDate((Date)movementObjet[2]);
			holderAccountMovement.setIdSourceParticipant(((BigDecimal)movementObjet[3]).longValue());
			holderAccountMovement.setIdTargetParticipant(((BigDecimal)movementObjet[4]).longValue());
			holderAccountMovement.setMovementQuantity((BigDecimal)movementObjet[5]);
			holderAccountMovement.setOperationPrice((BigDecimal)(movementObjet[6]==null?BigDecimal.ZERO:movementObjet[6]));
			holderAccountMovement.setIdModality(((BigDecimal)(movementObjet[7]==null?BigDecimal.ZERO:movementObjet[7])).longValue());
			holderAccountMovement.setMovementDate((Date)movementObjet[8]);
			holderAccountMovement.setIdMechanism(((BigDecimal)(movementObjet[9]==null?BigDecimal.ZERO:movementObjet[9])).longValue());
			
			holderAccountMovement.setMovementName(((String)movementObjet[10]));
			movDetail.add((String)movementObjet[11]);
			holderAccountMovement.setMovementDetail(movDetail);
			
			holderAccountMovementList.add(holderAccountMovement);
		}
		return holderAccountMovementList;
	}
	
	/**
	 * Gets the movement marketFact service.
	 *
	 * @param holderAccountBalMovDetResultTO the holder account bal mov det result to
	 * @return the movement marketFact service
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public MarketFactBalanceTO getHolderMarketFactMovementService(HolderAccountBalMovDetResultTO holderAccountBalMovDetResultTO ) throws ServiceException{				
		Map<String,Object> parameters = new HashMap<String,Object>();	
		StringBuilder querySqlMov = new StringBuilder();
		querySqlMov.append("Select 	maMov.securities.idSecurityCodePk, maMov.participant.idParticipantPk, maMov.holderAccount.idHolderAccountPk, maMov.securities.description,   ");//0,1,2,3
		querySqlMov.append("		maMov.securities.instrumentType, (Select p.parameterName From ParameterTable p Where p.parameterTablePk = maMov.securities.instrumentType),  ");//4,5
		querySqlMov.append("		maMov.idMarketFactMovementPk, maMov.marketDate, maMov.marketPrice, maMov.marketRate, maMov.movementQuantity	"); //6,7,8,9,10
		querySqlMov.append("From 	HolderMarketFactMovement maMov	");
		querySqlMov.append("Where 	maMov.holderAccountMovement.idHolderAccountMovementPk = :movPk	");	
		parameters.put("movPk", holderAccountBalMovDetResultTO.getIdHolderAccountMovementPk());
		return populateMarketFactBalanceMovementTO(this.findListByQueryString(querySqlMov.toString(), parameters));
	}
	
	/**
	 * Populate market fact balance movement to.
	 *
	 * @param objectDataList the object data list
	 * @return the market fact balance to
	 * @throws ServiceException the service exception
	 */
	private MarketFactBalanceTO populateMarketFactBalanceMovementTO(List<Object[]> objectDataList) throws ServiceException{
		MarketFactBalanceTO marketBalanceTO = new MarketFactBalanceTO();						
		marketBalanceTO.setMarketFacBalances(new ArrayList<MarketFactDetailTO>());			
		for(Object[] rowData: objectDataList){			
			if(marketBalanceTO.getSecurityCodePk()==null ){
				marketBalanceTO.setSecurityCodePk((String) rowData[0]);
				marketBalanceTO.setParticipantPk((Long) rowData[1]);
				marketBalanceTO.setHolderAccountPk((Long) rowData[2]);
				marketBalanceTO.setSecurityDescription((String) rowData[3]);				
				marketBalanceTO.setInstrumentType((Integer) rowData[4]);
				marketBalanceTO.setInstrumentDescription((String) rowData[5]);				
			}			
			MarketFactDetailTO marketDetailTO = null;
			Object holderMarketFact = rowData[6];
			if(holderMarketFact!=null){
				marketDetailTO = new MarketFactDetailTO();
				marketDetailTO.setMarketFactBalancePk((Long) holderMarketFact);
				marketDetailTO.setMarketDate((Date) rowData[7]);
				marketDetailTO.setMarketFactBalanceTO(marketBalanceTO);				
				if(marketBalanceTO.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode())){
					marketDetailTO.setMarketRate((BigDecimal) rowData[9]);
				}else if(marketBalanceTO.getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode())){
					marketDetailTO.setMarketPrice((BigDecimal) rowData[8]);
				}						
				marketDetailTO.setMovementQuantity((BigDecimal) rowData[10]);																	
			}			
			if(marketDetailTO!=null){
				if(marketBalanceTO.getLastMarketDate()==null){
					marketBalanceTO.setLastMarketDate(marketDetailTO.getMarketDate());
					marketBalanceTO.setLastMarketPrice(marketDetailTO.getMarketPrice());
					marketBalanceTO.setLastMarketRate(marketDetailTO.getMarketRate());
				}
				marketBalanceTO.getMarketFacBalances().add(marketDetailTO);
			}
		}		
		return marketBalanceTO;
	}
	
	/**
	 * Gets the issuer by code.
	 *
	 * @param issuerSearcher the issuer searcher
	 * @return the issuer by code
	 * @throws ServiceException the service exception
	 */
	public Issuer getIssuerByCode(IssuerSearcherTO issuerSearcher) throws ServiceException {
		
		Issuer iss = null;
		try {
			
			StringBuilder sb = new StringBuilder();
			sb.append("select i from Issuer i join fetch i.geographicLocation g  ");
			sb.append(" where 1=1 ");
			//Default state to confirmed		
			if (null != issuerSearcher.getState()) {
				//sb.append(" and i.state = :state ");
			}
			if (null != issuerSearcher.getIssuerCode()) {
				sb.append(" and i.idIssuerPk = :issuerCode ");
			}
			if (null != issuerSearcher.getMnemonic()) {
				sb.append(" and i.mnemonic = :nemonico ");
			}
			
			TypedQuery<Issuer> query = em.createQuery(sb.toString(), Issuer.class);
			
			if (StringUtils.isNotBlank(issuerSearcher.getIssuerCode())) {
				query.setParameter("issuerCode", issuerSearcher.getIssuerCode());
			}
			if (StringUtils.isNotBlank(issuerSearcher.getMnemonic())) {
				query.setParameter("nemonico", issuerSearcher.getMnemonic());
			}
			if (null != issuerSearcher.getState()) {
				//query.setParameter("state", issuerSearcher.getState());
			}
			iss =  query.getSingleResult();
			
		}catch(NoResultException nex) {
			iss = null;
		}
		
		return iss;
	}
	
	/**
	 * Gets the holders detail.
	 *
	 * @param holderAccountPK the holder account pk
	 * @return the holders detail
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings({ "unchecked"})
	public List<Object[]>  getHoldersDetail(String holderAccountPK) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ho.idHolderPk, ho.fullName, ");
		sbQuery.append("	(select indicator1 from ParameterTable where parameterTablePk=ho.documentType ), ");
		sbQuery.append("	ho.documentNumber, ");
		sbQuery.append("	(select text1 from ParameterTable where parameterTablePk=ho.documentSource ) ");
		sbQuery.append("	FROM HolderAccount ha, HolderAccountDetail had, Holder ho ");
		sbQuery.append("	WHERE ha.idHolderAccountPk =had.holderAccount.idHolderAccountPk ");
		sbQuery.append("	AND	had.holder.idHolderPk = ho.idHolderPk");
		sbQuery.append("	AND ha.idHolderAccountPk = :idHolderAccountPk");
		sbQuery.append("	ORDER BY ho.idHolderPk");
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idHolderAccountPk",new Long(holderAccountPK));

		return (List<Object[]>)findListByQueryString(sbQuery.toString(), parameters);
	}
	
	
	
	
	/**
	 * Gets the list search holder general.
	 *
	 * @param docType the doc type
	 * @param docNumber the doc number
	 * @param expedition the expedition
	 * @return the list search holder general
	 */
	public String searchIssuerSecurityQuery(IssuerHolderQueryTO issuerHolderQueryTO, boolean issuerUser){ 
		
		StringBuilder sbQuery= new StringBuilder();
    	
		sbQuery.append(" 	select DISTINCT 	 ");
		sbQuery.append(" 	 (select pt.parameter_name from parameter_table pt where pt.parameter_table_pk=h.HOLDER_TYPE) HolderType ,	 ");
		sbQuery.append(" 	 (SELECT LISTAGG((select h.ID_HOLDER_PK from holder h where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK),'' ||'<br>') 	 ");
		sbQuery.append(" 	  WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) 	 ");
		sbQuery.append(" 	  FROM HOLDER_ACCOUNT_DETAIL HAD 	 ");
		sbQuery.append(" 	  WHERE HAD.ID_HOLDER_ACCOUNT_FK = hab.ID_HOLDER_ACCOUNT_PK) ListCUI,	 ");
		sbQuery.append(" 	  (SELECT LISTAGG((select h.full_name from holder h where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK),'' ||'<br>') 	 ");
		sbQuery.append(" 	  WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) 	 ");
		sbQuery.append(" 	  FROM HOLDER_ACCOUNT_DETAIL HAD 	 ");
		sbQuery.append(" 	  WHERE HAD.ID_HOLDER_ACCOUNT_FK = hab.ID_HOLDER_ACCOUNT_PK) ListaDESC,	 ");
		sbQuery.append(" 	  (SELECT LISTAGG((select h.document_number from holder h where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK),'' ||'<br>') 	");
		sbQuery.append(" 	    WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) 	 ");
		sbQuery.append(" 	  FROM HOLDER_ACCOUNT_DETAIL HAD 	  ");
		sbQuery.append(" 	  WHERE HAD.ID_HOLDER_ACCOUNT_FK = hab.ID_HOLDER_ACCOUNT_PK) DocNumber, ");
		sbQuery.append(" 	  ha.account_number, hab.TOTAL_BALANCE, hab.AVAILABLE_BALANCE ");
		sbQuery.append(" 	from holder_account_balance hab	 ");
		sbQuery.append(" 	INNER JOIN security sec ON sec.ID_SECURITY_CODE_PK = hab.ID_SECURITY_CODE_PK	 ");
		sbQuery.append(" 	INNER JOIN HOLDER_ACCOUNT ha ON ha.ID_HOLDER_ACCOUNT_PK = hab.ID_HOLDER_ACCOUNT_PK	 ");
		sbQuery.append(" 	INNER JOIN HOLDER_ACCOUNT_DETAIL had ON had.ID_HOLDER_ACCOUNT_FK = hab.ID_HOLDER_ACCOUNT_PK	 ");
		sbQuery.append(" 	INNER JOIN HOLDER h ON h.ID_HOLDER_PK = had.ID_HOLDER_FK	 ");
		sbQuery.append(" 	WHERE 1=1 	 ");
		sbQuery.append(" 	AND HAB.TOTAL_BALANCE>0 	 ");
		if(Validations.validateIsNotNull(issuerHolderQueryTO.getIssuerHelperMgmt().getIdIssuerPk())){
			sbQuery.append(" 	and sec.ID_ISSUER_FK=':issuer'	 ");
    	}
		if(Validations.validateIsNotNull(issuerHolderQueryTO.getIdSecurityCode())){
			sbQuery.append(" 	and hab.ID_SECURITY_CODE_PK=':securityCode'	 ");
    	}
		if(Validations.validateIsNotNull(issuerHolderQueryTO.getIdSecurityClass())){
			sbQuery.append(" 	and sec.SECURITY_CLASS=':idSecurityClass'	 ");
    	}
		
		sbQuery.append(" 	order by hab.ID_SECURITY_CODE_PK	 ");

		String strQueryFormatted = sbQuery.toString();
		
		if(issuerHolderQueryTO.getIssuerHelperMgmt()!=null){
    		strQueryFormatted= strQueryFormatted.replace(":issuer", issuerHolderQueryTO.getIssuerHelperMgmt().getIdIssuerPk().toString());
    	}
		if(issuerHolderQueryTO.getIdSecurityCode()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":securityCode", issuerHolderQueryTO.getIdSecurityCode().toString());
    	}
		if(issuerHolderQueryTO.getIdSecurityClass()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":idSecurityClass", issuerHolderQueryTO.getIdSecurityClass().toString());
    	}

		return strQueryFormatted;
	}
	
	public List<Object[]> getQueryListByClass(String strQuery) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(strQuery);
		Query query = em.createNativeQuery(sbQuery.toString());
		return query.getResultList();
	}
	
}
