package com.pradera.accounts.query.to;

import java.io.Serializable;

import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ConsolidateParentAccountTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/08/2015
 */
public class ConsolidateParentAccountTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The issuer. */
	private Issuer	  	  issuer;
	
	/** The security. */
	private Security  	  security;
	
	/** The participant. */
	private Participant   participant;
	
	/** The holder. */
	private Holder		  holder;
	
	/** The holder account. */
	private HolderAccount holderAccount;
	
	/** The valorization type. */
	private Integer 	  valorizationType;
	
	/** The bl participant inv. */
	private boolean 		 blParticipantInv;
	
	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	//GETTERS AND SETTERS
	public Issuer getIssuer() {
		return issuer;
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}
	
	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}
	
	/**
	 * Gets the valorization type.
	 *
	 * @return the valorization type
	 */
	public Integer getValorizationType() {
		return valorizationType;
	}
	
	/**
	 * Sets the valorization type.
	 *
	 * @param valorizationType the new valorization type
	 */
	public void setValorizationType(Integer valorizationType) {
		this.valorizationType = valorizationType;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}
	
	/**
	 * Checks if is bl participant inv.
	 *
	 * @return the blParticipantInv
	 */
	public boolean isBlParticipantInv() {
		return blParticipantInv;
	}

	/**
	 * Sets the bl participant inv.
	 *
	 * @param blParticipantInv the blParticipantInv to set
	 */
	public void setBlParticipantInv(boolean blParticipantInv) {
		this.blParticipantInv = blParticipantInv;
	}

}
