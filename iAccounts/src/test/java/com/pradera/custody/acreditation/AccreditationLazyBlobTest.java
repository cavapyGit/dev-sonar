package com.pradera.custody.acreditation;

import static org.junit.Assert.fail;

import java.io.File;

import javax.ejb.EJB;
import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.formatter.Formatters;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.model.custody.accreditationcertificates.AccreditationOperation;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccreditationLazyBlobTest.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/08/2015
 */
//@RunWith(Arquillian.class)
public class AccreditationLazyBlobTest {
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The dao service. */
	@EJB
	CrudDaoServiceBean daoService;

	 /**
 	 * Creates the test archive.
 	 *
 	 * @return the web archive
 	 */
 	@Deployment
	  	public static WebArchive createTestArchive() {
	  		File[] libs = Maven.resolver()
	                  .loadPomFromFile("pom.xml")
	                  .importRuntimeDependencies()
	                  .asFile();
	  		
	  		WebArchive war =
	  		
	  		ShrinkWrap.create(WebArchive.class, "iAccounts.war")
	  				.addAsLibraries(libs)
	           		.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	  		System.out.println(war.toString((Formatters.VERBOSE)));
	  		return war;
	  	}
	
/**
 * Test.
 */
//	@Test
	public void test() {
		AccreditationOperation acreOper = daoService.find(AccreditationOperation.class, new Long(202));
		Assert.assertNotNull(acreOper);
	}

}
