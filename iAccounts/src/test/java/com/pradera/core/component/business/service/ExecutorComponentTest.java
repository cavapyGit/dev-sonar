package com.pradera.core.component.business.service;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.formatter.Formatters;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.custody.accreditationcertificates.AccreditationOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ExecutorComponentTest.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/08/2015
 */
//	@RunWith(Arquillian.class)
public class ExecutorComponentTest {
	
  /** The log. */
  @Inject
  PraderaLogger log;
	
  /**
 * Creates the test archive.
 *
 * @return the web archive
 */
@Deployment
  	public static WebArchive createTestArchive() {
  		File[] libs = Maven.resolver()
                  .loadPomFromFile("pom.xml")
                  .importRuntimeDependencies()
                  .asFile();
  		
  		WebArchive war =
  		
  		ShrinkWrap.create(WebArchive.class, "PraderaAccounts.war")
  				.addAsLibraries(libs)
           		.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
  		System.out.println(war.toString((Formatters.VERBOSE)));
  		return war;
  	}
  
  	/**
	   * Check componenet acreditation.
	   */
	  //@Test
  	public void checkComponenetAcreditation() {
  		log.info("EJB no es null");
  		//Test llamada a componente
  		//Operacion de acreditacion
  		Long accreditationOperationId = Long.valueOf(565);
  		AccountsComponentTO objAccountComponent = new AccountsComponentTO();
  		List<HolderAccountBalanceTO> accountBalanceTOs = new ArrayList<HolderAccountBalanceTO>();
		
			HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
			holderAccountBalanceTO.setIdHolderAccount(40l);
			holderAccountBalanceTO.setIdParticipant(16l);
			holderAccountBalanceTO.setIdSecurityCode("DO1005216929");
			holderAccountBalanceTO.setStockQuantity(new BigDecimal(300));
			//Adding holderAccountBalanceTO to List
			accountBalanceTOs.add(holderAccountBalanceTO);
		
		objAccountComponent.setIdBusinessProcess(new Long(BusinessProcessType.ACCREDITATION_CERTIFICATE_CONFIRM.getCode()));
		objAccountComponent.setIdRefCustodyOperation(accreditationOperationId);
		objAccountComponent.setLstSourceAccounts(accountBalanceTOs);
		objAccountComponent.setIdOperationType(new Long(ParameterOperationType.SECURITIES_ACCREDITATION.getCode()));
		//call componente
  	}
  	
  	/**
	   * Check new save accreditation.
	   */
	  //@Test
  	public void checkNewSaveAccreditation() {
  		AccreditationOperation acreOpe = new AccreditationOperation();
  		CustodyOperation custodyOperation = new CustodyOperation();
  		custodyOperation.setOperationType(1l);
		custodyOperation.setLastModifyApp(0);
		custodyOperation.setLastModifyDate(CommonsUtilities.currentDateTime());
		custodyOperation.setLastModifyUser("CEVAL");
		custodyOperation.setOperationDate(CommonsUtilities.currentDateTime());
		custodyOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		custodyOperation.setRegistryUser("CEVAL");
		custodyOperation.setOperationType(2l);
		acreOpe.setRegisterDate(CommonsUtilities.currentDateTime());
		acreOpe.setCustodyOperation(custodyOperation);
		//Probar leer 
		Map<String,Object> parma = new HashMap<String, Object>();
		 parma.put("idPk", new Long(565));
  	}
}
