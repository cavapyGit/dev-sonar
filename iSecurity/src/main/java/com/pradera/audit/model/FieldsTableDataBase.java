package com.pradera.audit.model;

import java.io.Serializable;
// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class FieldsTableDataBase
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.1
 */
public class FieldsTableDataBase implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 9212093363150523704L;
	
	/** The id table pk. */
	private Integer idTablePK;
	
	/** The field name. */
	private String fieldName;
	
	/** The insertable. */
	private Boolean insertable;
	
	/** The updatable. */
	private Boolean updatable;
	
	/** The deletable. */
	private Boolean deletable;
	
	/** The constraint. */
	private Boolean constraint;
	
	/** The disable insertable. */
	private Boolean disableInsertable;
	
	/** The disable updatable. */
	private Boolean disableUpdatable;
	
	/** The primarykey. */
	private Boolean primarykey;
	
	/**
	 * Gets the id table pk.
	 *
	 * @return the idTablePK
	 */
	public Integer getIdTablePK() {
		return idTablePK;
	}
	
	/**
	 * Sets the id table pk.
	 *
	 * @param idTablePK the idTablePK to set
	 */
	public void setIdTablePK(Integer idTablePK) {
		this.idTablePK = idTablePK;
	}
	
	/**
	 * Gets the field name.
	 *
	 * @return the fieldName
	 */
	public String getFieldName() {
		return fieldName;
	}
	
	/**
	 * Sets the field name.
	 *
	 * @param fieldName the fieldName to set
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	
	/**
	 * Gets the insertable.
	 *
	 * @return the insertable
	 */
	public Boolean getInsertable() {
		return insertable;
	}
	
	/**
	 * Sets the insertable.
	 *
	 * @param insertable the insertable to set
	 */
	public void setInsertable(Boolean insertable) {
		this.insertable = insertable;
	}
	
	/**
	 * Gets the updatable.
	 *
	 * @return the updatable
	 */
	public Boolean getUpdatable() {
		return updatable;
	}
	
	/**
	 * Sets the updatable.
	 *
	 * @param updatable the updatable to set
	 */
	public void setUpdatable(Boolean updatable) {
		this.updatable = updatable;
	}
	
	/**
	 * Gets the deletable.
	 *
	 * @return the deletable
	 */
	public Boolean getDeletable() {
		return deletable;
	}
	
	/**
	 * Sets the deletable.
	 *
	 * @param deletable the deletable to set
	 */
	public void setDeletable(Boolean deletable) {
		this.deletable = deletable;
	}
	
	/**
	 * Gets the constraint.
	 *
	 * @return the constraint
	 */
	public Boolean getConstraint() {
		return constraint;
	}
	
	/**
	 * Sets the constraint.
	 *
	 * @param constraint the constraint to set
	 */
	public void setConstraint(Boolean constraint) {
		this.constraint = constraint;
	}
	
	/**
	 * Gets the disable insertable.
	 *
	 * @return the disable insertable
	 */
	public Boolean getDisableInsertable() {
		return disableInsertable;
	}
	
	/**
	 * Sets the disable insertable.
	 *
	 * @param disableInsertable the new disable insertable
	 */
	public void setDisableInsertable(Boolean disableInsertable) {
		this.disableInsertable = disableInsertable;
	}
	
	/**
	 * Gets the disable updatable.
	 *
	 * @return the disable updatable
	 */
	public Boolean getDisableUpdatable() {
		return disableUpdatable;
	}
	
	/**
	 * Sets the disable updatable.
	 *
	 * @param disableUpdatable the new disable updatable
	 */
	public void setDisableUpdatable(Boolean disableUpdatable) {
		this.disableUpdatable = disableUpdatable;
	}

	/**
	 * Gets the primarykey.
	 *
	 * @return the primarykey
	 */
	public Boolean getPrimarykey() {
		return primarykey;
	}

	/**
	 * Sets the primarykey.
	 *
	 * @param primarykey the new primarykey
	 */
	public void setPrimarykey(Boolean primarykey) {
		this.primarykey = primarykey;
	}
		
		
}