package com.pradera.audit.model;


import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class TableDataModel
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.1
 */

public class TableDataModel extends ListDataModel<TableDataBase> implements Serializable,SelectableDataModel<TableDataBase>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2469737978576747201L;

	public TableDataModel(){
		
	}
	
	public TableDataModel(List<TableDataBase> data){
		super(data);
	}
	
//	@Override
	public TableDataBase getRowData(String rowKey) {
		// TODO Auto-generated method stub
		List<TableDataBase> perfiles=(List<TableDataBase>)getWrappedData();
        for(TableDataBase perfil : perfiles) {  
            if(String.valueOf(perfil.getIdTablePK()).equals(rowKey))
                return perfil;  
        }  
		return null;
	}

//	@Override
	public Object getRowKey(TableDataBase perfilBean) {
		// TODO Auto-generated method stub
		return perfilBean.getIdTablePK();
	}
	
	

}


