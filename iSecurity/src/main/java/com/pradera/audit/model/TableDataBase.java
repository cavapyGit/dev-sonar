package com.pradera.audit.model;

import java.io.Serializable;
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class TableDataBase
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.1
 */
public class TableDataBase implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3552983990550267531L;
	private long idTablePK;
	private String tableName;
	private String tabledescription;
	private Integer numColumns;
	private Integer tableStatus;
	private Integer[] actions;
	private String system;
	/**
	 * @return the idTablePK
	 */
	public long getIdTablePK() {
		return idTablePK;
	}
	/**
	 * @param idTablePK the idTablePK to set
	 */
	public void setIdTablePK(long idTablePK) {
		this.idTablePK = idTablePK;
	}
	/**
	 * @return the tableName
	 */
	public String getTableName() {
		return tableName;
	}
	/**
	 * @param tableName the tableName to set
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	/**
	 * @return the tabledescription
	 */
	public String getTabledescription() {
		return tabledescription;
	}
	/**
	 * @param tabledescription the tabledescription to set
	 */
	public void setTabledescription(String tabledescription) {
		this.tabledescription = tabledescription;
	}
	/**
	 * @return the numColumns
	 */
	public Integer getNumColumns() {
		return numColumns;
	}
	/**
	 * @param numColumns the numColumns to set
	 */
	public void setNumColumns(Integer numColumns) {
		this.numColumns = numColumns;
	}
	/**
	 * @return the tableStatus
	 */
	public Integer getTableStatus() {
		return tableStatus;
	}
	/**
	 * @param tableStatus the tableStatus to set
	 */
	public void setTableStatus(Integer tableStatus) {
		this.tableStatus = tableStatus;
	}
	/**
	 * @return the actions
	 */
	public Integer[] getActions() {
		return actions;
	}
	/**
	 * @param actions the actions to set
	 */
	public void setActions(Integer[] actions) {
		this.actions = actions;
	}
	/**
	 * @return the system
	 */
	public String getSystem() {
		return system;
	}
	/**
	 * @param system the system to set
	 */
	public void setSystem(String system) {
		this.system = system;
	}
	
}
