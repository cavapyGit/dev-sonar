package com.pradera.audit.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.integration.common.validation.Validations;
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class GenericDataModel
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.1
 */
public class GenericDataModel<T> extends ListDataModel<T> implements Serializable, SelectableDataModel<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3568287745069924396L;
	public GenericDataModel() {
		super();
	}

	public GenericDataModel(List<T> list) {
		super(list);
	}

	@SuppressWarnings("unchecked")
	public List<T> getDataList(){
		List<T> list = (List<T>) this.getWrappedData();
		return list;
	}

	public T getRowData(String arg0) {
		return this.getDataList().get(Integer.parseInt(arg0));
	}

	public Object getRowKey(T arg0) {
		return arg0;
	}
	@SuppressWarnings("unchecked")
	public int getSize(){
		if(Validations.validateIsNotNullAndNotEmpty(((List<T>) this.getWrappedData()))){
			return ((List<T>) this.getWrappedData()).size();
		}
		return 0;
	}
}
