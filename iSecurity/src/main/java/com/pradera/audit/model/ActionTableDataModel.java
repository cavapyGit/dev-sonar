package com.pradera.audit.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.security.model.ActionTable;

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class ActionTableDataModel
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.1
 */
public class ActionTableDataModel extends ListDataModel<ActionTable> implements Serializable,SelectableDataModel<ActionTable>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9003832687717633085L;

	public ActionTableDataModel(){
		
	}
	
	public ActionTableDataModel(List<ActionTable> data){
		super(data);
	}
	
//	@Override
	public ActionTable getRowData(String rowKey) {
		// TODO Auto-generated method stub
		List<ActionTable> perfiles=(List<ActionTable>)getWrappedData();
        for(ActionTable perfil : perfiles) {  
        	
            if(String.valueOf(perfil.getIdActionTablesPk()).equals(rowKey))
                return perfil;  
        }  
		return null;
	}

//	@Override
	public Object getRowKey(ActionTable perfilBean) {
		// TODO Auto-generated method stub
		return perfilBean.getIdActionTablesPk();
	}
	
	

}

