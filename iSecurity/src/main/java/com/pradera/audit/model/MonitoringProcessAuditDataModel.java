package com.pradera.audit.model;
import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;
import com.pradera.security.bean.Process;


/**
 * <ul>
 * <li>
 * Project PraderaCommons Copyright 2012.</li>
 * </ul>
 * 
 * The Class UserAuditingServiceBean.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 10/01/2013
 */
public class MonitoringProcessAuditDataModel extends ListDataModel<Process> implements Serializable,SelectableDataModel<Process>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9079841082525455699L;

	public MonitoringProcessAuditDataModel(){
		
	}
	
	public MonitoringProcessAuditDataModel(List<Process> data){
		super(data);
	}
	
//	@Override
	public Process getRowData(String rowKey) {
		// TODO Auto-generated method stub
		List<Process> perfiles=(List<Process>)getWrappedData();
        for(Process perfil : perfiles) {  
        	
            if(String.valueOf(perfil.getIdProcessPK()).equals(rowKey))
                return perfil;  
        }  
		return null;
	}

//	@Override
	public Object getRowKey(Process perfilBean) {
		// TODO Auto-generated method stub
		return perfilBean.getIdProcessPK();
	}

}


