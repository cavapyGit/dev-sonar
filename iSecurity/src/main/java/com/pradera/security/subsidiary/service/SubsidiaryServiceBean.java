package com.pradera.security.subsidiary.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.model.Subsidiary;
import com.pradera.security.subsidiary.view.filters.SubsidiaryFilter;
import com.pradera.security.systemmgmt.service.CrudSecurityServiceBean;

//TODO: Auto-generated Javadoc
/**
* <ul>
* <li>
* Project Pradera.
* Copyright 2015.</li>
* </ul>
* 
* The Class SubsidiaryServiceBean.
*
* @author PraderaTechnologies.
* @version 1.0 , 01/07/2015
*/
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class SubsidiaryServiceBean extends CrudSecurityServiceBean{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Search Subsidiary by user service bean.
	 *
	 * @param subsidiaryFilter the SubsidiaryFilter
	 * @return the list the Institution Security
	 * @throws ServiceException the service exception
	 */
	
	@SuppressWarnings("unchecked")
	public List<Subsidiary> lstSubsidiaryByInstitutionSecurity(SubsidiaryFilter subsidiaryFilter) throws ServiceException {		
		List<Subsidiary> lstSubsidiary = new ArrayList<Subsidiary>();		
		try {			
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" Select SU from Subsidiary SU ");
			sbQuery.append(" where SU.institutionsSecurity.idInstitutionSecurityPk = :parInstitutionSecurity  ");			
			if(Validations.validateIsNotNullAndNotEmpty(subsidiaryFilter.getInstitutionType())){
				sbQuery.append(" and SU.institutionTypeFk = :institutionType  ");
			}
			if(Validations.validateIsNotNullAndNotEmpty(subsidiaryFilter.getDepartament())){
				sbQuery.append(" and SU.departament = :departament  ");
			}
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("parInstitutionSecurity", subsidiaryFilter.getInstitutionSecurity());
			if(Validations.validateIsNotNullAndNotEmpty(subsidiaryFilter.getInstitutionType())){
				query.setParameter("institutionType", subsidiaryFilter.getInstitutionType());
			}
			if(Validations.validateIsNotNullAndNotEmpty(subsidiaryFilter.getDepartament())){
				query.setParameter("departament", subsidiaryFilter.getDepartament());
			}
			lstSubsidiary = (List<Subsidiary>) query.getResultList();									
		} catch (NoResultException e) {
			return null;
		}				
		return lstSubsidiary;				
	}
	
	/**
	 * Search Subsidiary by management service bean.
	 *
	 * @param subsidiary the Subsidiary Filter
	 * @return the list Subsidiary Filter
	 * @throws ServiceException the service exception
	 */
	
	@SuppressWarnings("unchecked")
	public List<Subsidiary> getLstSubsidiaryByManagement(SubsidiaryFilter subsidiaryFilter) throws ServiceException {		
		List<Subsidiary> lstSubsidiary = new ArrayList<Subsidiary>();		
		try {			
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" Select distinct SU from Subsidiary SU ");
			sbQuery.append(" inner join fetch SU.institutionsSecurity isec ");
			sbQuery.append(" where 1=1 ");			
			if(Validations.validateIsNotNullAndNotEmpty(subsidiaryFilter.getInstitutionSecurity())){
				sbQuery.append(" and isec.idInstitutionSecurityPk=:parInstitutionSecurity  ");
			}
			if(Validations.validateIsNotNullAndNotEmpty(subsidiaryFilter.getDepartament())){
				sbQuery.append(" and SU.departament=:parDepartament  ");
			}
			if(Validations.validateIsNotNullAndNotEmpty(subsidiaryFilter.getDescription())){
				sbQuery.append(" and SU.subsidiaryDescription like :parDescription  ");
			}
			if(Validations.validateIsNotNullAndNotEmpty(subsidiaryFilter.getInstitutionType())){
				sbQuery.append(" and SU.institutionTypeFk=:parInstitutionType  ");
			}
			sbQuery.append(" ORDER BY SU.idSubsidiaryPk desc ");
			Query query = em.createQuery(sbQuery.toString());			
			if(Validations.validateIsNotNullAndNotEmpty(subsidiaryFilter.getInstitutionSecurity())){
				query.setParameter("parInstitutionSecurity", subsidiaryFilter.getInstitutionSecurity());
			}
			if(Validations.validateIsNotNullAndNotEmpty(subsidiaryFilter.getDepartament())){
				query.setParameter("parDepartament", subsidiaryFilter.getDepartament());
			}
			if(Validations.validateIsNotNullAndNotEmpty(subsidiaryFilter.getDescription())){				
				query.setParameter("parDescription", "%" + subsidiaryFilter.getDescription() + "%");
			}		
			if(Validations.validateIsNotNullAndNotEmpty(subsidiaryFilter.getInstitutionType())){
				query.setParameter("parInstitutionType", subsidiaryFilter.getInstitutionType());
			}
			lstSubsidiary = (List<Subsidiary>) query.getResultList();									
		} catch (NoResultException e) {
			return null;
		}				
		return lstSubsidiary;				
	}

}
