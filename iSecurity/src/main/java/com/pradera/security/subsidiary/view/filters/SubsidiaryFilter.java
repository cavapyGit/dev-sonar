package com.pradera.security.subsidiary.view.filters;

import java.io.Serializable;

/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2015.</li>
 * </ul>
 * 
 * The Class SubsidiaryFilter.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/07/2015
 */

public class SubsidiaryFilter implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** The id subsidiary pk. */
	private Long idSudsiadiaryPk;		
	
	/** The description subsidiary. */
	private String description;
	
	/** The id departament. */
	private Integer departament;

	/** The user account type. */
	private Integer userAccountType;
	
	/** The institution type. */
	private Integer institutionType;
	
	/** The id institution security. */
	private Integer institutionSecurity;
	
	/**
	 * @return the idSudsiadiaryPk
	 */
	public Long getIdSudsiadiaryPk() {
		return idSudsiadiaryPk;
	}

	/**
	 * @param idSudsiadiaryPk the idSudsiadiaryPk to set
	 */
	public void setIdSudsiadiaryPk(Long idSudsiadiaryPk) {
		this.idSudsiadiaryPk = idSudsiadiaryPk;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the departament
	 */
	public Integer getDepartament() {
		return departament;
	}

	/**
	 * @param departament the departament to set
	 */
	public void setDepartament(Integer departament) {
		this.departament = departament;
	}

	/**
	 * @return the userAccountType
	 */
	public Integer getUserAccountType() {
		return userAccountType;
	}

	/**
	 * @param userAccountType the userAccountType to set
	 */
	public void setUserAccountType(Integer userAccountType) {
		this.userAccountType = userAccountType;
	}

	/**
	 * @return the institutionType
	 */
	public Integer getInstitutionType() {
		return institutionType;
	}

	/**
	 * @param institutionType the institutionType to set
	 */
	public void setInstitutionType(Integer institutionType) {
		this.institutionType = institutionType;
	}

	/**
	 * @return the institutionSecurity
	 */
	public Integer getInstitutionSecurity() {
		return institutionSecurity;
	}

	/**
	 * @param institutionSecurity the institutionSecurity to set
	 */
	public void setInstitutionSecurity(Integer institutionSecurity) {
		this.institutionSecurity = institutionSecurity;
	}

}
