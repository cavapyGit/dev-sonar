package com.pradera.security.subsidiary.facade;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.model.Subsidiary;
import com.pradera.security.subsidiary.service.SubsidiaryServiceBean;
import com.pradera.security.subsidiary.view.filters.SubsidiaryFilter;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class SubsidiaryServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/07/2015
 */

@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class SubsidiaryServiceFacade {

	/** The user integration service bean. */
	@EJB
	private SubsidiaryServiceBean subsidiaryServiceBean;
	
	/**  The transaction repository bean. */
    @Resource
    private TransactionSynchronizationRegistry transactionRegistry;
	
	/**
	 * Search Subsidiary by user service bean.
	 *
	 * @param intInstitutionSecurity the Institution Security
	 * @return the list the Institution Security
	 * @throws ServiceException the service exception
	 */
	public List<Subsidiary> lstSubsidiaryByInstitutionSecurity(SubsidiaryFilter subsidiaryFilter) throws ServiceException {
		return subsidiaryServiceBean.lstSubsidiaryByInstitutionSecurity(subsidiaryFilter);
	}
	
	/**
	 * Search Subsidiary by management service bean.
	 *
	 * @param subsidiary the Subsidiary Filter
	 * @return the list Subsidiary Filter
	 * @throws ServiceException the service exception
	 */
		
	public List<Subsidiary> getLstSubsidiaryByManagement(SubsidiaryFilter subsidiaryFilter) throws ServiceException {
		return subsidiaryServiceBean.getLstSubsidiaryByManagement(subsidiaryFilter);
	}
	
	/**
 	 * Register Subsidiary service facade.
 	 *
 	 * @param subsidiary the Subsidiary
 	 * @return Subsidiary, if successful
 	 * @throws ServiceException the service exception
 	 */
 	public Subsidiary registerSubsidiaryServiceFacade(Subsidiary objSubsidiary)  throws ServiceException {
 		LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		subsidiaryServiceBean.create(objSubsidiary);
 		return objSubsidiary;
 	}
 	
 	/**
 	 * Update Subsidiary service facade.
 	 *
 	 * @param subsidiary the Subsidiary
 	 * @return Subsidiary, if successful
 	 * @throws ServiceException the service exception
 	 */
 	public Subsidiary updateSubsidiaryServiceFacade(Subsidiary objSubsidiary)  throws ServiceException {
 		LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		subsidiaryServiceBean.update(objSubsidiary);
 		return objSubsidiary;
 	}
	
}
