package com.pradera.security.subsidiary.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.model.InstitutionsSecurity;
import com.pradera.security.model.Parameter;
import com.pradera.security.model.Subsidiary;
import com.pradera.security.model.type.DefinitionType;
import com.pradera.security.model.type.ParameterTableType;
import com.pradera.security.model.type.UserAccountType;
import com.pradera.security.subsidiary.facade.SubsidiaryServiceFacade;
import com.pradera.security.subsidiary.view.filters.SubsidiaryFilter;
import com.pradera.security.systemmgmt.view.filters.InstitutionSecurityFilter;
import com.pradera.security.utils.view.GenericBaseBean;
import com.pradera.security.utils.view.PropertiesConstants;

/**
 * <ul>
 * <li>	
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SubsidiaryBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/07/2015
 */
@DepositaryWebBean
@LoggerCreateBean

public class SubsidiaryBean extends GenericBaseBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;
	
	/** The subsidiary filter. */
	private SubsidiaryFilter subsidiaryFilter;
	
	/** The subsidiary. */
	private Subsidiary subsidiary;
	
	/** The list User Account Type. */
	private List<Parameter> lstUserAccountType;
    
    /** The list institution type for filter. */
    private List<Parameter> lstInstitutionType;
    
    /** The list institutions security for filter. */
	private List<InstitutionsSecurity> lstInstitutionsSecurity;
	
	/** The list institution type for filter. */
    private List<Parameter> lstDepartament;
    
    /** The list . */
	private List<BooleanType> listIsCentral = new ArrayList<BooleanType>();
    
    /** The subsidiary users result. */
	private GenericDataModel<Subsidiary> gdmSubsidiary;
    
	/** The subsidiary service facade. */
	@EJB
	protected SubsidiaryServiceFacade subsidiaryServiceFacade;
	
	/**
	 * Init.
	 */
	@PostConstruct
	public void init(){
		try {
			subsidiaryFilter = new SubsidiaryFilter();
			loadParameterTable();
			showPrivileges();
			
		} catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	public void loadParameterTable() {
		try {
			lstUserAccountType = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.USER_TYPES.getCode());	
			subsidiaryFilter.setUserAccountType(UserAccountType.EXTERNAL.getCode());
			lstInstitutionType = userServiceFacade.listClientTypeByDefinitionServiceFacade(ParameterTableType.INSITUTIONS_STATE.getCode());			
			lstDepartament = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.DEPARTAMENT.getCode());
			listIsCentral = BooleanType.list;
		} catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}		
	}
	
	/**
	 * Search Subsidiary listener.
	 * 
	 * @param event the event
	 */
	public void searchSubsidiaryListener(ActionEvent event){
		List<Subsidiary> lstSubsidiarySearch = null;
		gdmSubsidiary = null;
		try {
			lstSubsidiarySearch = subsidiaryServiceFacade.getLstSubsidiaryByManagement(subsidiaryFilter);
			if(Validations.validateListIsNotNullAndNotEmpty(lstSubsidiarySearch)){
				for(Subsidiary objSubsidiary : lstSubsidiarySearch){
					Parameter objParameter = systemServiceFacade.getParameterByPk(objSubsidiary.getDepartament());
					if(objParameter != null){
						objSubsidiary.setDescDepartament(objParameter.getName());
					}					
				}				
			}
			gdmSubsidiary = new GenericDataModel<Subsidiary>(lstSubsidiarySearch);
		} catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}				
	}
	
	/**
	 * Clean search Subsidiary listener.
	 *
	 * @param event the event
	 */
	public void cleanSearchSubsidiaryListener(ActionEvent event){
		subsidiaryFilter.setInstitutionSecurity(null);
		subsidiaryFilter.setInstitutionType(null);
		subsidiaryFilter.setDepartament(null);
		subsidiaryFilter.setDescription(null);	
		lstInstitutionsSecurity = null;
		gdmSubsidiary = null;
	}
	
	/**
	 * Subsidiary register action.
	 * @return the string
	 */
	@LoggerAuditWeb
	public void registerSubsidiaryListener(ActionEvent event){
		try {
			subsidiaryFilter = new SubsidiaryFilter();
			subsidiary = new Subsidiary();
			subsidiary.setInstitutionsSecurity(new InstitutionsSecurity());
			setOperationType(ViewOperationsType.REGISTER.getCode());			
			loadParameterTable();			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
	}
	
	/**
	 * Before user registration listener.
	 * This method is  called when the button save is pressed
	 * If validation is successful it show the confirmation dialog
	 *
	 * @param event the event
	 */
	public void beforeSubsidiaryRegistrationListener(ActionEvent event){
		try {
			Object[] argObj = new Object[1];
			JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);			
			if(isRegister()){
				argObj[0] = subsidiary.getSubsidiaryDescription();
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_CONFIRM, null, PropertiesConstants.SUBSIDIARY_CONFIRM_REGISTER, argObj);
			}
			if(isModify()){
				argObj[0] = subsidiary.getSubsidiaryDescription();
				showMessageOnDialog(PropertiesConstants.LBL_CONFIRM_HEADER_CONFIRM, null, PropertiesConstants.SUBSIDIARY_CONFIRM_MODIFY, argObj);
			}			
			JSFUtilities.showComponent("frmSubsiadiaryAdministration:cnfSubsidiaryAdm");			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Subsidiary registration listener.
	 * This method is called after confirming, if save or update the user
	 */
	@LoggerAuditWeb
	public void saveSubsidiaryListener(){
		try {
			JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
			if(isRegister()){
				registrySubsidiary();
			}else if(isModify()) {
				modifySubsidiary();
			}
			JSFUtilities.showComponent("cnfEndTransaction");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Registry Subsidiary.
	 *
	 * This method is called if the operation type is "register"
	 */
	@LoggerAuditWeb
	public void registrySubsidiary(){
		try {
			Object[] argObj=new Object[1];			
			Subsidiary objSubsidiary = subsidiaryServiceFacade.registerSubsidiaryServiceFacade(subsidiary);
			argObj[0] = objSubsidiary.getSubsidiaryDescription();
			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS, null, PropertiesConstants.SUBSIDIARY_MSG_REGISTER_SUCCESS, argObj);
			cleanSubsidiary();
		} catch(Exception e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
	}
	
	/**
	 * Clean Subsidiary.
	 *  This method clean the datatable and the entered filters
	 */
	public void cleanSubsidiary(){
		subsidiaryFilter = new SubsidiaryFilter();
		subsidiary = new Subsidiary();
		subsidiary.setInstitutionsSecurity(new InstitutionsSecurity());
		gdmSubsidiary = null;
		loadParameterTable();
	}
	
	/**
	 * Registry Subsidiary.
	 *
	 * This method is called if the operation type is "register"
	 */
	@LoggerAuditWeb
	public void modifySubsidiary(){
		try {
			Object[] argObj=new Object[1];			
			Subsidiary objSubsidiary = subsidiaryServiceFacade.updateSubsidiaryServiceFacade(subsidiary);
			argObj[0] = objSubsidiary.getSubsidiaryDescription();
			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS, null, PropertiesConstants.SUBSIDIARY_MSG_MODIFY_SUCCESS, argObj);
			cleanSubsidiary();
		} catch(Exception e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * The Method is used close the Confirm dialog on Pressing No.
	 */
	public void hideConfirm(){
		JSFUtilities.hideComponent("frmSubsiadiaryAdministration:cnfSubsidiaryAdm");
	}
	
	/**
	 * Subsidiary modification action.
	 * @return the string
	 */
	@LoggerAuditWeb
	public String subsidiaryModificationAction(){
		try {
			JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);			
			if(subsidiary == null){
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT, null, 
						PropertiesConstants.USER_ERROR_REGISTER_NO_SELECTED, null);	
				JSFUtilities.showComponent("cnfMsgCustomValidation");
				return null;
			} 
			setOperationType(ViewOperationsType.MODIFY.getCode());			
			subsidiaryFilter.setInstitutionType(subsidiary.getInstitutionsSecurity().getIdInstitutionTypeFk());
			changeInstitutionType("mgm");			
			return "mgmtSubsidiary";			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}		
	}
	
	/**
	 * Subsidiary Detail.
	 *
	 * This method is called to see the detail information for the Subsidiary
	 * @param event the event
	 */
	@LoggerAuditWeb
	public void subsidiaryDetail(ActionEvent event){
		try {
			setOperationType(ViewOperationsType.DETAIL.getCode());
			subsidiary = (Subsidiary) event.getComponent().getAttributes().get("subSelected");					
			changeInstitutionType("mgm");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change institution type for filter listener.
	 */
	public void changeInstitutionType(String strOpcion){
		Integer institutionType = null;
		try {
			if(strOpcion.compareTo("search") == 0){
				subsidiaryFilter.setInstitutionSecurity(null);
				institutionType = subsidiaryFilter.getInstitutionType();				
			} else {
				institutionType = subsidiary.getInstitutionTypeFk();	
			}
			InstitutionSecurityFilter insSecurityFilter = new InstitutionSecurityFilter();			
			if(Validations.validateIsNotNullAndNotEmpty(institutionType)){
				if(InstitutionType.ISSUER_DPF.getCode().equals(institutionType)){
					insSecurityFilter.setIdInstitutionTypeFk(InstitutionType.ISSUER.getCode());
				} else if(InstitutionType.PARTICIPANT_INVESTOR.getCode().equals(institutionType)){
					insSecurityFilter.setIdInstitutionTypeFk(InstitutionType.PARTICIPANT.getCode());
				} else  {
					insSecurityFilter.setIdInstitutionTypeFk(institutionType);
				}
			} else {
				insSecurityFilter.setIdInstitutionTypeFk(institutionType);
			}	
			if(Validations.validateIsNotNullAndNotEmpty(insSecurityFilter.getIdInstitutionTypeFk())){
				lstInstitutionsSecurity = userServiceFacade.getInstitutionsByFilterServiceFacade(insSecurityFilter);
			}	else {
				lstInstitutionsSecurity = null;
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Show privileges.
	 *
	 * @throws ServiceException the service exception
	 */
	private void showPrivileges() throws ServiceException{
		PrivilegeComponent privilegeComp = new PrivilegeComponent();
		privilegeComp.setBtnModifyView(true);
		privilegeComp.setBtnRegisterView(true);		
		userPrivilege.setPrivilegeComponent(privilegeComp);
	}
	
	/**
	 * @return the subsidiaryFilter
	 */
	public SubsidiaryFilter getSubsidiaryFilter() {
		return subsidiaryFilter;
	}

	/**
	 * @param subsidiaryFilter the subsidiaryFilter to set
	 */
	public void setSubsidiaryFilter(SubsidiaryFilter subsidiaryFilter) {
		this.subsidiaryFilter = subsidiaryFilter;
	}

	/**
	 * @return the lstUserAccountType
	 */
	public List<Parameter> getLstUserAccountType() {
		return lstUserAccountType;
	}

	/**
	 * @param lstUserAccountType the lstUserAccountType to set
	 */
	public void setLstUserAccountType(List<Parameter> lstUserAccountType) {
		this.lstUserAccountType = lstUserAccountType;
	}

	/**
	 * @return the lstInstitutionType
	 */
	public List<Parameter> getLstInstitutionType() {
		return lstInstitutionType;
	}

	/**
	 * @param lstInstitutionType the lstInstitutionType to set
	 */
	public void setLstInstitutionType(List<Parameter> lstInstitutionType) {
		this.lstInstitutionType = lstInstitutionType;
	}

	/**
	 * @return the lstInstitutionsSecurity
	 */
	public List<InstitutionsSecurity> getLstInstitutionsSecurity() {
		return lstInstitutionsSecurity;
	}

	/**
	 * @param lstInstitutionsSecurity the lstInstitutionsSecurity to set
	 */
	public void setLstInstitutionsSecurity(
			List<InstitutionsSecurity> lstInstitutionsSecurity) {
		this.lstInstitutionsSecurity = lstInstitutionsSecurity;
	}

	/**
	 * @return the lstDepartament
	 */
	public List<Parameter> getLstDepartament() {
		return lstDepartament;
	}

	/**
	 * @param lstDepartament the lstDepartament to set
	 */
	public void setLstDepartament(List<Parameter> lstDepartament) {
		this.lstDepartament = lstDepartament;
	}

	/**
	 * @return the gdmSubsidiary
	 */
	public GenericDataModel<Subsidiary> getGdmSubsidiary() {
		return gdmSubsidiary;
	}

	/**
	 * @param gdmSubsidiary the gdmSubsidiary to set
	 */
	public void setGdmSubsidiary(GenericDataModel<Subsidiary> gdmSubsidiary) {
		this.gdmSubsidiary = gdmSubsidiary;
	}

	/**
	 * @return the subsidiary
	 */
	public Subsidiary getSubsidiary() {
		return subsidiary;
	}

	/**
	 * @param subsidiary the subsidiary to set
	 */
	public void setSubsidiary(Subsidiary subsidiary) {
		this.subsidiary = subsidiary;
	}

	/**
	 * @return the listIsCentral
	 */
	public List<BooleanType> getListIsCentral() {
		return listIsCentral;
	}

	/**
	 * @param listIsCentral the listIsCentral to set
	 */
	public void setListIsCentral(List<BooleanType> listIsCentral) {
		this.listIsCentral = listIsCentral;
	}	
	
}
