package com.pradera.security.audit.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.apache.log4j.Logger;
import org.primefaces.event.SelectEvent;

import com.pradera.audit.model.ActionTableDataModel;
import com.pradera.audit.model.AuditTrackFilterBean;
import com.pradera.audit.model.FieldsTableDataBase;
import com.pradera.audit.model.GenericDataModel;
import com.pradera.audit.model.TableDataBase;
import com.pradera.audit.model.TableDataModel;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.audit.facade.AuditsensitiveFieldBeanFacade;
import com.pradera.security.model.ActionTable;
import com.pradera.security.model.DetailAction;
import com.pradera.security.model.Parameter;
import com.pradera.security.model.System;
import com.pradera.security.model.type.AuditTableSituationType;
import com.pradera.security.model.type.AuditTableStateType;
import com.pradera.security.model.type.DefinitionType;
import com.pradera.security.model.type.StateType;
import com.pradera.security.model.type.SystemStateType;
import com.pradera.security.systemmgmt.service.UserExceptionServiceBean;
import com.pradera.security.utils.view.GenericBaseBean;
import com.pradera.security.utils.view.PropertiesConstants;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ConsultSensitiveFieldAuditBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 03-feb-2015
 */
@DepositaryWebBean
public class ConsultSensitiveFieldAuditBean extends GenericBaseBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The auditsensitive field bean facade. */
	@EJB
	AuditsensitiveFieldBeanFacade auditsensitiveFieldBeanFacade;	
	
	@EJB
	UserExceptionServiceBean parameterService;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** the list of tables in respected system(schema). */
	private List<TableDataBase> lstTables;
	
	/** the list of fields in respected table. */
	private List<FieldsTableDataBase> lstFieldsTable;
	
	/** The lst fields table before save. */
	private List<FieldsTableDataBase> lstFieldsTableBeforeSave;
	
	/** the list of fields in search. */
	private List<FieldsTableDataBase> lstFieldsTableSearch;
	
	/** the list of users in the respected system. */
	private List<String> lstUsers;
	
	/** the selected system. */
	private Integer systemSelected = -1;
	
	/** the selected table. */
	private Integer tableSelected = -1;
	
	/** the selected field. */
	private Integer fieldSelected = -1;
	
	/**  the selected user. */
	private String userSelected = "";
	
	/** the selected table in search. */
	private Integer tableSelectedSearch = -1;
	
	/** the selected field in search. */
	private Integer fieldSelectedSearch = -1;
	
	/** the start date in search. */
	private Date startDate;
	
	/** the end date in search. */
	private Date endDate;
	
	/** the list of action tables. */
	private List<ActionTable> actionTables;
	
	/** the action table. */
	private ActionTable actionTable;
	
	/** the action table modified in selection. */
	private ActionTable actionTableModified;
	
	/** the logger. */
	private static final  Logger log = Logger.getLogger(ConsultSensitiveFieldAuditBean.class.getName());
	
	/** the rule selected in row selection. */
	private Integer ruleSelected = -1;
	
	/** the action table model to show the list of action tables. */
	private ActionTableDataModel actionTableDataModel;
	
	/** the table model used to display the list of action table. */
	private TableDataModel tableDataModel;
	
	/** the table data base. */
	private TableDataBase tableDataBase = new TableDataBase();
	
	/** the data model tabla campos to show columns. */
	private GenericDataModel<FieldsTableDataBase> dataModelTablaCampo;
	
	/** the field table data base. */
	private FieldsTableDataBase fieldsTableDataBase;
	
	/** the row selected in selection. */
	private boolean rowSelected = false;
	
	/** The show save. */
	private boolean showSave = true;
	
	/** the errMsg. */
	private boolean errMsg = false;
	
	/** the add rule. */
	private boolean addRule = false;
	
	/** the new selected. */
	private boolean newSelected = false;
	
	/** the modify activated in modificar. */
	private boolean modifyActivated = false;
	
	/** the modify de activated in deactivate. */
	private boolean desactivateSelected = false;
	
	/** the activated in activate. */
	private boolean activateSelected = false;
	
	/** the eliminar activatedin eliminar. */
	private boolean eliminarSelected = false;
	
	/** the rule selected disble. */
	private boolean ruleSelectedDisable  = false;
	
	/** back button disable. */
	private boolean viewButton=false;
	
	/** The table selected disable. */
	private boolean tableSelectedDisable = false;
	
	/** the selection event. */
	private boolean selectionEvent = false;
	
	/** the checked to show dialog when no field selected as insertable or updatable. */
	private boolean checked = false;
	
	/** the list of action table in search. */
	private List<ActionTable> actionTablesSearch;
	
	/** the action table data model search in search. */
	private ActionTableDataModel actionTableDataModelSearch;
	
	/** the system selection in search. */
	private Integer systemSelectedSearch = -1;
	
	/** the search system is used to render back button. */
	private boolean searchSystem;
	
	/** the max date is used to validate start and end date. */
	private Date maxDate;
	
	/** The aggregar. */
	private boolean aggregar;
	
	/** The selected records. */
	private ActionTable[] selectedRecords;
	
	private ActionTable selectedRule;
	
	/** The state selected. */
	private Integer stateSelected;
	
	/** The actiontable states. */
	private List<StateType> actiontableStates;
	
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/** The rules states. */
	private List<Parameter> rulesStates;
	
	private Map<Integer,String> statesDescription;
	
	private Map<Integer,String> situationDescription;
	
	
	/**
	 * Generate parameters export.
	 *
	 * @return the map
	 */
	public Map<String, Object> generateParametersExport(){
		//using LinkedHashMap for using order of insert in map
		LinkedHashMap<String, Object> parametersExport = new LinkedHashMap<>();
		FacesContext context = FacesContext.getCurrentInstance();
	    ResourceBundle msgs = context.getApplication().evaluateExpressionGet(context, "#{msgs}", ResourceBundle.class);
	    //parameter system
	    String systemName=null;
	    for(System system :lstSystems){
	    	if(system.getIdSystemPk().equals(systemSelected)){
	    		systemName = system.getName();
	    		break;
	    	}
	    }
	    parametersExport.put(msgs.getString("audit.lbl.sistema"), systemName);
	    //parameter rule name
	    parametersExport.put(msgs.getString("audit.actiontable.ruleName"),actionTable.getRuleName());
	    //parameter register date
	    parametersExport.put(msgs.getString("audit.lbl.date.register"), CommonsUtilities.convertDateToString(actionTable.getRegistryDate(),msgs.getString("dateTimeSecondsFormat")));
	    //parameter table name 
	    parametersExport.put(msgs.getString("audit.msg.tablesistema"),actionTable.getTableName());
		return parametersExport;
	}
	
	
	/**
	 * Find Table selected.
	 *
	 * @return the system
	 */
	public TableDataBase findTableSelected() {
		if(tableSelected != -1){
			for (TableDataBase tableDataBase : lstTables) {
				if (tableSelected == tableDataBase.getIdTablePK()) {
					return tableDataBase;
				}
			}
		}
		return null;
	}
	
	/**
	 * Find Table selected.
	 *
	 * @return the system
	 */
	public TableDataBase findTableSelectedSearch() {
		if(tableSelectedSearch != -1){
			for (TableDataBase tableDataBase : lstTables) {
				if (tableSelectedSearch == tableDataBase.getIdTablePK()) {
					return tableDataBase;
				}
			}
		}
		return null;
	}
	
	/**
	 * used to find selected column.
	 *
	 * @return fieldsTableDataBase
	 * 							FieldsTableDataBase
	 */
	public FieldsTableDataBase findFieldSelectedSearch(){
		if(fieldSelectedSearch != -1){
			for(FieldsTableDataBase fieldsTableDataBase: lstFieldsTableSearch){
				if(fieldSelectedSearch.equals(fieldsTableDataBase.getIdTablePK())){
					return fieldsTableDataBase;
				}
			}
		}
		return null;
	}
	
	/**
	 * used to find selected column.
	 *
	 * @return fieldsTableDataBase
	 * 							FieldsTableDataBase
	 */
	public FieldsTableDataBase findFieldSelected(){
		for(FieldsTableDataBase fieldsTableDataBase: lstFieldsTable){
			if(fieldSelected.equals(fieldsTableDataBase.getIdTablePK())){
				return fieldsTableDataBase;
			}
		}
		return null;
	}
	
	/**
	 * used to fill the table columns.
	 */
	public void fillTableFields(){
		try{
		TableDataBase tableDataBaseNew = new TableDataBase(); 
		String schemaName="";
		System system = findSystemSelected();
		if(Validations.validateIsNotNullAndNotEmpty(system.getSchemaDb())){
			schemaName=system.getSchemaDb();
		}
		log.info("-------------------------------------------");
		log.info("Selected SchemanName-------"+schemaName);
		
		//List<FieldsTableDataBase> columnList = new ArrayList<FieldsTableDataBase>();
		if(selectionEvent){
			lstFieldsTable = auditsensitiveFieldBeanFacade.getColumnList(tableDataBase.getTableName(),schemaName);
		}else{
			tableDataBaseNew = findTableSelected();
			if(tableDataBaseNew != null)
				lstFieldsTable = auditsensitiveFieldBeanFacade.getColumnList(tableDataBaseNew.getTableName(),schemaName);
		}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}	
		
	}
	
	/**
	 * used to fill the table columns.
	 */
	public void fillTableFieldsSearch(){
		try{
		String schemaName="";
		System system = findSystemSelected();
		if(Validations.validateIsNotNullAndNotEmpty(system.getSchemaDb())){
			schemaName=system.getSchemaDb();
		}
		TableDataBase tableDataBaseSearch = new TableDataBase(); 
		tableDataBaseSearch = findTableSelectedSearch();
			if(tableDataBaseSearch != null)
				lstFieldsTableSearch = auditsensitiveFieldBeanFacade.getColumnList(tableDataBaseSearch.getTableName(),schemaName);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Find system selected.
	 *
	 * @return the system
	 */
	public System findSystemSelected() {
		if(systemSelected != -1){
			for (System sys : lstSystems) {
				if ((systemSelected.equals(sys.getIdSystemPk()))) {
					return sys;
				}
			}
		}
		return null;
	}
	
	/**
	 * Find system selected.
	 *
	 * @return the system
	 */
	public System findSystemSelectedSearch() {
		if(systemSelectedSearch != -1){
			for (System sys : lstSystems) {
				if ((systemSelectedSearch.equals(sys.getIdSystemPk()))) {
					return sys;
				}
			}
		}
		return null;
	}
	
	/**
	 * used to fill users list.
	 */
	public void fillUsers(){
		try{
		if(systemSelectedSearch != -1){
			System system = findSystemSelectedSearch();
			if(lstTables != null)
				lstTables.clear();
			if(system.getSchemaDb() != null && !system.getSchemaDb().equals("null"))
				lstTables =  auditsensitiveFieldBeanFacade.getTableList(system.getSchemaDb());
			if(lstUsers != null)
				lstUsers.clear();
			lstUsers  =  auditsensitiveFieldBeanFacade.getUsersList();
		}else{
			lstUsers = new ArrayList<String>();
			lstTables = new ArrayList<TableDataBase>();		
		}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * used to initialize the list systems.
	 */
	@PostConstruct
	@LoggerAuditWeb
	public void init() {
		listSystemsByState(SystemStateType.CONFIRMED.getCode());
		try {
			if (!FacesContext.getCurrentInstance().isPostback()) {
	    		if(actionTablesSearch != null){
	    			actionTablesSearch.clear();
	    		}
		    	systemSelectedSearch = -1;
		    	tableSelectedSearch = -1;
		    	fieldSelectedSearch = -1;
		    	userSelected = "";
		    	startDate = getMaxDate();
		    	endDate = getMaxDate();
		    	rulesStates=parameterService.listByDefinitionServiceBean(DefinitionType.AUDIT_TABLES_RULE_STATE.getCode());
		    	List<Parameter> situations = parameterService.listByDefinitionServiceBean(DefinitionType.AUDIT_TABLES_RULE_STITUATION.getCode());
		    	listHolidays();
		    	statesDescription = new HashMap<>();
		    	situationDescription = new HashMap<>();
		    	for(Parameter param: rulesStates){
		    		statesDescription.put(param.getIdParameterPk(), param.getName());
		    	}
		    	for(Parameter param: situations){
		    		situationDescription.put(param.getIdParameterPk(), param.getName());
		    	}
			}
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * used to return back to consultar .
	 *
	 * @return the string
	 */
	public String returnConsultar(){
		return "consultSensitiveFieldsView";
	}
	
	/**
	 * Gets the audit sensitive field.
	 *
	 * @return auditSensitiveFieldsView
	 * 									String
	 */
	public String getAuditSensitiveField(){
		this.setOperationType(ViewOperationsType.REGISTER.getCode());
		systemSelected = -1;
		showSave = false;
		tableSelected = -1;
		ruleSelectedDisable = false;
		tableDataBase = new TableDataBase();
		if(actionTables != null)
			actionTables.clear();
		if(lstFieldsTable != null)
			lstFieldsTable.clear();
		if(lstTables != null)
			lstTables.clear();
		addRule = true;
		aggregar = false;
		actionTable = new ActionTable();
		actionTable.setRegistryDate(CommonsUtilities.currentDateTime());
		searchSystem = true;
		viewButton=false;
		selectionEvent = false;
		return "auditSensitiveFieldsView";
	}
	
	/**
	 * used to redirect the page to auditSensitiveFields.xhtml
	 * 
	 * @return	auditSensitiveFieldsView
	 * 				String
	 */
	public String displayAsModify(){
		this.setOperationType(ViewOperationsType.MODIFY.getCode());
		actionTable = actionTableModified;
		searchSystem = true;
		showSave = false;
		tableSelected = tableSelectedSearch;
		fieldSelected = fieldSelectedSearch;
		systemSelected = systemSelectedSearch;
		//setInitialDate(actionTable.getInitialDate());
		//setFinalDate(actionTable.getEndDate());
		//fillTracking();
		modifyRule();
		lstTables.clear();
		TableDataBase tableDataBaseSelect = new TableDataBase();
		tableDataBaseSelect.setTableName(actionTable.getTableName());
		lstTables.add(tableDataBaseSelect);
		tableDataBase = tableDataBaseSelect;
		tableDataModel = new TableDataModel(lstTables);
		//added update in audit sensitive fields screen
		return "auditSensitiveFieldsView";
	}
	
	/**
	 * used to fill the data table of type List<ActionTable>.
	 */
	public void fillLog(){
		if(systemSelectedSearch.equals(Integer.valueOf(-1))){
			/*JSFUtilities.hideGeneralDialogues();
			if(systemSelectedSearch==-1){
			JSFUtilities.addContextMessage("frmSensitiveFieldConsult:cboSystem",FacesMessage.SEVERITY_ERROR,"",PropertiesUtilities.getMessage(com.pradera.security.utils.view.PropertiesConstants.AUDIT_SYSTEM_REQUIRED));
			}
			if(userSelected==-1){
			JSFUtilities.addContextMessage("frmSensitiveFieldConsult:cboUserList",FacesMessage.SEVERITY_ERROR,"",PropertiesUtilities.getMessage(com.pradera.security.utils.view.PropertiesConstants.AUDIT_USER_REQUIRED));
			}
			JSFUtilities.showComponent("cnfMsgRequiredValidation");*/
			return;
		}else{
		if(actionTablesSearch != null)
			actionTablesSearch.clear();
		else
			actionTablesSearch = new ArrayList<ActionTable>();
		AuditTrackFilterBean auditTrackFilterBean = new AuditTrackFilterBean();
		if(systemSelectedSearch != -1) {
			auditTrackFilterBean.setSchemaName(findSystemSelectedSearch().getSchemaDb());
			auditTrackFilterBean.setSystemSelected(systemSelectedSearch);
		}
		if(!tableSelectedSearch.equals(Integer.valueOf(-1)))
			auditTrackFilterBean.setTableName(findTableSelectedSearch().getTableName());
		if(!fieldSelectedSearch.equals(Integer.valueOf(-1)))
			auditTrackFilterBean.setFieldName(findFieldSelectedSearch().getFieldName());
		if(!userSelected.equals("-1") && !userSelected.isEmpty())
			auditTrackFilterBean .setUser(userSelected);
		if(startDate != null){
			//SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyy");
			//auditTrackFilterBean.setInitialDate(sdf.format(startDate));
			auditTrackFilterBean.setInitialDate(startDate);
		}
		if(endDate != null){
			//SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyy");
			//auditTrackFilterBean.setFinalDate(sdf.format(endDate));
			auditTrackFilterBean.setFinalDate(CommonsUtilities.addDate(endDate,1));
		}
		if(stateSelected != null){
			auditTrackFilterBean.setState(stateSelected);
		}
		try{
		//actionTablesSearch = auditsensitiveFieldBeanFacade.getActionTableFromAuditLog(auditTrackFilterBean);
			actionTablesSearch = auditsensitiveFieldBeanFacade.searchAutionTableByAuditFilter(auditTrackFilterBean);
		}
		catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		actionTableDataModelSearch = new ActionTableDataModel(actionTablesSearch);
		}
	}
	
	/**
	 * used to empty the actionTables.
	 */
	public void clearLog(){
		JSFUtilities.resetComponent("frmSensitiveFieldConsult:idstate");
		JSFUtilities.resetComponent("frmSensitiveFieldConsult:cboUserList");
		hideDialogs();
		actionTablesSearch = new ArrayList<ActionTable>();
		userSelected =  "";
		stateSelected = GeneralConstants.NEGATIVE_ONE_INTEGER;
		fieldSelectedSearch = new Integer(-1);
		tableSelectedSearch = new Integer(-1);
		systemSelectedSearch = new Integer(-1);
		actionTable = null;
		actionTableDataModelSearch = null;
		startDate = maxDate;
		endDate = maxDate;
	}
	
	/**
	 * To clear the Table values.
	 */
	public void clearTableData(){
		try {
			if(lstTables!=null ){
				lstTables.clear();
			}
			if(systemSelectedSearch != -1)
			lstTables = auditsensitiveFieldBeanFacade.getTableList(findSystemSelectedSearch().getSchemaDb());
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * To clear registration screen data.
	 */
	public void clearData(){
		hideDialogs();
		systemSelected = GeneralConstants.NEGATIVE_ONE_INTEGER;
		actionTable = new ActionTable();
		actionTable.setRegistryDate(CommonsUtilities.currentDateTime());
		lstTables = null;
		tableDataBase = new TableDataBase();
		selectionEvent = false;
		dataModelTablaCampo=null;
		lstFieldsTable=null;
		showSave = false;
		JSFUtilities.resetComponent("frmSensitiveFieldAudit:txtRuleName");
	}
	
	/**
	 * used to fill the data table of type List<ActionTable>.
	 */
	public void fillTracking(){
		if(systemSelected != -1){
			rowSelected = false;
			if(actionTables != null)
				actionTables.clear();
			else 
				actionTables = new ArrayList<ActionTable>();
			try {
					actionTables = auditsensitiveFieldBeanFacade.searchActionTableBySystem(systemSelected);
					/*for (ActionTable var : actionTables) {
						if(var.getState() == StateType.BLOCKED.getCode())
							var.setStateType(StateType.BLOCKED.getValue());
						else if(var.getState() == StateType.ANNULLED.getCode())
							var.setStateType(StateType.ANNULLED.getValue());
						else if(var.getState() == StateType.CONFIRMED.getCode())
							var.setStateType(StateType.CONFIRMED.getValue());
						else if(var.getState() == StateType.REGISTERED.getCode())
							var.setStateType(StateType.REGISTERED.getValue());
						else if(var.getState() == StateType.REJECTED.getCode())
							var.setStateType(StateType.REJECTED.getValue());
					}*/
				} catch(Exception ex){
					excepcion.fire(new ExceptionToCatchEvent(ex));
				}
			actionTableDataModel = new ActionTableDataModel(actionTables);
			if(searchSystem)
				aggregar = false;
			else{
				aggregar = true;
				actionTable = null;
			}
				
			rowSelected = false;
			
		}else{
			if(actionTables != null)
				actionTables.clear();
			else
				actionTables = new ArrayList<ActionTable>();
			if(lstTables != null)
				lstTables.clear();
			aggregar = false;
			rowSelected = false;
			addRule = false;
		}
	}
	
	/**
	 * used to update the table fields when modification and in new creation.
	 */
	public void permissionsTable(){
		try{
		if(!rowSelected){
			if(lstFieldsTable != null)
				lstFieldsTable.clear();
			fillTableFields();
			dataModelTablaCampo = new GenericDataModel<FieldsTableDataBase>(lstFieldsTable);
		}else if(actionTable.getTableName() != null){
			ruleSelectedDisable = true;
			viewButton=true;
			if(lstTables == null)
				lstTables = new ArrayList<TableDataBase>();
			else
				lstTables.clear();
			if(searchSystem){
				lstTables = auditsensitiveFieldBeanFacade.getTableList(findSystemSelectedSearch().getSchemaDb());
			}
			//findSystemSelectedSearch()
			else
				lstTables = auditsensitiveFieldBeanFacade.getTableList(findSystemSelected().getSchemaDb());
			TableDataBase tableDataBaseSelect = new TableDataBase();
			tableDataBaseSelect.setTableName(actionTable.getTableName());
			for (TableDataBase table : lstTables) {
				if(tableDataBaseSelect.getTableName().equals(table.getTableName())){
					tableDataBaseSelect.setIdTablePK(table.getIdTablePK());
				}
			}
			tableDataBase = tableDataBaseSelect;
			tableDataModel = new TableDataModel(lstTables);
			lstFieldsTable = auditsensitiveFieldBeanFacade.getDetaiActionByActionPk(actionTable.getIdActionTablesPk());
			dataModelTablaCampo = new GenericDataModel<FieldsTableDataBase>(lstFieldsTable);
		}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * used to select the radio selected row of type ACtionTable
	 *  
	 * @param selectEvent actionTable
	 */
	public void assignRule(SelectEvent selectEvent){
		hideDialogs();
		actionTable = (ActionTable)selectEvent.getObject();
		if(lstTables != null)
			lstTables.clear();
		rowSelected = true;
		aggregar = false;
	}
	
	/**
	 * used to get the selected value in the data table on row selection.
	 *
	 * @param event 				SelectEvent
	 */
	public void onRowSelect(SelectEvent event) {
		selectionEvent = true;
        tableDataBase = (TableDataBase) event.getObject();
        
        if(actionTable.getIdActionTablesPk() == 0){
        	if(lstFieldsTable != null)
    			lstFieldsTable.clear();
    		fillTableFields();
    		
    		List<FieldsTableDataBase> lstTmpCompareFields = new ArrayList<>();
    		try {
    			AuditTrackFilterBean auditTrackFilterBean = new AuditTrackFilterBean();
    			//auditTrackFilterBean.setSchemaName(findSystemSelectedSearch().getSchemaDb());
    			auditTrackFilterBean.setTableName(tableDataBase.getTableName());
    			auditTrackFilterBean.setState(AuditTableStateType.CONFIRMED.getCode()); // solo los activos
    			actionTablesSearch = auditsensitiveFieldBeanFacade.searchAutionTableByAuditFilter(auditTrackFilterBean);
    			List<FieldsTableDataBase> lstFieldsTable = new ArrayList<>();
    			for (ActionTable table : actionTablesSearch) {
    				lstFieldsTable.addAll(auditsensitiveFieldBeanFacade.getDetaiActionByActionPk(table.getIdActionTablesPk()));
    			}

    			FieldsTableDataBase fieldsTable = null;
    			for (FieldsTableDataBase fields : lstFieldsTable) {
    				fieldsTable = new FieldsTableDataBase();
    				if(fields.getInsertable()){
    					fieldsTable.setFieldName(fields.getFieldName());
    					fieldsTable.setInsertable(true);
    					fieldsTable.setDisableInsertable(true);
    				}
    				if(fields.getUpdatable()){
    					fieldsTable.setFieldName(fields.getFieldName());
    					fieldsTable.setUpdatable(true);
    					fieldsTable.setDisableUpdatable(true);
    				}
    				lstTmpCompareFields.add(fieldsTable);
    			}
    			
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		lstFieldsTableBeforeSave = new ArrayList<FieldsTableDataBase>();	//listaTemporal
    		for (FieldsTableDataBase fieldsTmp : lstTmpCompareFields) {
    			for(FieldsTableDataBase fields :lstFieldsTable){
    				if( fields.getFieldName().equals(fieldsTmp.getFieldName()) ){
    					fields.setInsertable(fieldsTmp.getInsertable());
    					fields.setUpdatable(fieldsTmp.getUpdatable());
    					fields.setDisableInsertable(fieldsTmp.getDisableInsertable());
    					fields.setDisableUpdatable(fieldsTmp.getDisableUpdatable());
        	        	lstFieldsTableBeforeSave.add(fields);//test
    				}
    			}
    		}
    		
    		dataModelTablaCampo = new GenericDataModel<FieldsTableDataBase>(lstFieldsTable);
    		
        }else{
        	//nothing to do
        }
    }
	
	/**
	 * used to enable the register panel.
	 */
	public void addEnable(){
		try{
			hideDialogs();
			addRule = true;
			modifyActivated=false;
			rowSelected = false;
			newSelected = true;
			searchSystem = false;
			actionTable = new ActionTable();
			tableSelectedDisable = false;
			ruleSelectedDisable = false;
			viewButton=false;
			initialDate = CommonsUtilities.currentDate();
			finalDate = CommonsUtilities.currentDate();
			tableSelected = -1;
			actionTable.setRegistryDate(CommonsUtilities.currentDateTime());
			//actionTable.setInitialDate(CommonsUtilities.currentDate());
			//actionTable.setEndDate(CommonsUtilities.currentDate());
			if(systemSelected != -1){
				System system = findSystemSelected();
				if (lstTables != null) {
					lstTables.clear();
				}
				if(system.getSchemaDb() != null){
					lstTables =  auditsensitiveFieldBeanFacade.getTableList(system.getSchemaDb());
				}
				tableDataModel = new TableDataModel(lstTables);
				if(Validations.validateListIsNullOrEmpty(lstTables)) {
					JSFUtilities.showComponent("cnfEndTransactionSamePage");
					JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, 
							PropertiesConstants.ERROR_MSG_SYSTEM_HAS_NO_TABLE, null);
					showSave = false;
				} else {
					showSave = true;
				}
			}else{
				clearData();
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Hide dialogs.
	 */
	public void hideDialogs(){
		JSFUtilities.hideGeneralDialogues();
		//JSFUtilities.hideComponent("frmSensitiveFieldAudit:activateConfirmation");
		//JSFUtilities.hideComponent("frmSensitiveFieldAudit:deactivateConfirmation");
		//JSFUtilities.hideComponent("frmSensitiveFieldAudit:modifyConfirmation");
		//JSFUtilities.hideComponent("frmSensitiveFieldAudit:eliminarConfirmation");
		//JSFUtilities.hideComponent("frmSensitiveFieldConsult:eliminarConfirmDialog");
		JSFUtilities.hideComponent("frmSensitiveFieldAudit:cnfaddNewrule");
		//JSFUtilities.hideComponent("frmSensitiveFieldAudit:eliminarConfirmDialog");
	}
	

	/**
	 * 	
	 * used to update and create new rule of type ActionTable.
	 */

	@LoggerAuditWeb
	public void addNewRule(){
		try{
		JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
			hideDialogs();
			checked = false;
			int saveOrUpdate = 0;
				List<DetailAction> detailActionCheck;
				if(desactivateSelected){
						for(ActionTable action : selectedRecords){
							auditsensitiveFieldBeanFacade.executeInsertTrigger(action);
							auditsensitiveFieldBeanFacade.executeUpdateTrigger(action);
							action.setState(StateType.DEACTIVATED.getCode());
							auditsensitiveFieldBeanFacade.saveActionTable(action, 1);
						}
						desactivateSelected = false;
						JSFUtilities.showEndTransactionDialog();
						JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS, null, 
								PropertiesConstants.RULE_DESACTIVE_SUCCESSFULLY, null);
						fillLog();
						return;
				}else if(newSelected){
					actionTable.setTableName(tableDataBase.getTableName());
					actionTable.setRuleName(actionTable.getRuleName());
					actionTable.setState(AuditTableStateType.REGISTERED.getCode());
					actionTable.setSituation(AuditTableSituationType.PENDING_EXECUTION.getCode());
					actionTable.setRegistryDate(actionTable.getRegistryDate());
					detailActionCheck = getListDetailAction(actionTable);
					int count = detailActionCheck.size();
					int size = 0;
					for (DetailAction detailAction : detailActionCheck) {
						if(detailAction.getIsInsertable() == 0  && detailAction.getIsUpdatable() == 0)
							size++;
					}
					if(size == count){
						JSFUtilities.showComponent("cnfEndTransactionSamePage");
						JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, 
								PropertiesConstants.ERROR_MSG_SELECT, null);
						newSelected = true;
						checked = true;
						return;
					}else {
						//shoulbe check at least one
						tableDataBase = new TableDataBase();
						actionTable.setDetailActions(detailActionCheck);
						actionTable.setSystem(findSystemSelected());
						checked =false;
					}
				}
				if(!checked){					
					//Get the list of columns of primary key
					List<String> lstColumnsPrimaryKey = new ArrayList<String>();					
					for (FieldsTableDataBase fieldsTableDataBase : lstFieldsTable) {
						if(fieldsTableDataBase.getPrimarykey()){
							lstColumnsPrimaryKey.add(fieldsTableDataBase.getFieldName());
						}
					}					
					//save rules in state register
					System system = findSystemSelected();
					auditsensitiveFieldBeanFacade.registerRulesTrigger(actionTable, system, lstColumnsPrimaryKey);
					fillLog();
					addRule = false;
					if(lstFieldsTable != null)
						lstFieldsTable.clear();
					lstTables = null;
					checked = false;
					rowSelected = false;
					activateSelected = false;
					desactivateSelected = false;
//					if(newSelected){
						newSelected = false;
						errMsg = true;
						clearTableData();
						JSFUtilities.showEndTransactionDialog();
						Object[] obj = new Object[2];
						obj[0]=actionTable.getRuleName();
						obj[1]=String.valueOf(actionTable.getIdActionTablesPk());
						JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS, null,
								PropertiesConstants.RULE_ADDED_SUCCESSFULLY, obj);
						
//					}
				}else if(!errMsg){
					JSFUtilities.showComponent("cnfEndTransactionSamePage");
					JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,
							PropertiesConstants.ERROR_MSG_STATE_DELETE, null);
				}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * used to get the Detail Action List .
	 *
	 * @param actionTable the action table
	 * @return detailActions
	 * 							List<DetailAction>
	 */
	public List<DetailAction> getListDetailAction(ActionTable actionTable){
		List<DetailAction> detailActions = new ArrayList<DetailAction>();
		detailActions.clear();
		DetailAction detailAction;
		if(lstFieldsTableBeforeSave.size()>0)
			lstFieldsTable.removeAll(lstFieldsTableBeforeSave);//los registros marcados con check antes de grabar
		
		for (FieldsTableDataBase fieldsTableDataBase : lstFieldsTable) {
			detailAction = new DetailAction();
			detailAction.setFieldName(fieldsTableDataBase.getFieldName());
			if(fieldsTableDataBase.getInsertable() != null)
			{
				if(fieldsTableDataBase.getInsertable())
					detailAction.setIsInsertable(1);
				else
					detailAction.setIsInsertable(0);
			}
			if(fieldsTableDataBase.getUpdatable() != null){
				if(fieldsTableDataBase.getUpdatable())
					detailAction.setIsUpdatable(1);
				else 
					detailAction.setIsUpdatable(0);
			}
			detailAction.setActionTable(actionTable);
			detailAction.setLastModifyApp(1);
			detailAction.setLastModifyDate(new Date());
			detailAction.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
			detailAction.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
			detailActions.add(detailAction);
			}
		return detailActions;
	}
	
	/**
	 * used to call when modificar selected.
	 */
	public void modifyRule(){
		if(!searchSystem){
			if(actionTable.getStateType().equals(StateType.REGISTERED.getValue())){
				modifyActivated = true;
				rowSelected = true;
				addRule = true;
				lstFieldsTable = null;
				permissionsTable();
				rowSelected = false;
				setInitialDate(actionTable.getInitialDate());
				setFinalDate(actionTable.getEndDate());
			}else{
				JSFUtilities.showComponent("cnfEndTransactionSamePage");
				JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,
						PropertiesConstants.ERROR_MSG_STATE_MODIFY, null);
				errMsg = true;
				checked = true;
				actionTable = null;
			}
		}else{
			modifyActivated = true;
			rowSelected = true;
			addRule = true;
			lstFieldsTable = null;
			permissionsTable();
			rowSelected = false;
			setInitialDate(actionTable.getInitialDate());
			setFinalDate(actionTable.getEndDate());
		}
	}
	
	
	/**
	 * used to call when deactivate selected.
	 */
	@LoggerAuditWeb
	public void desactivate(){
		hideDialogs();
		desactivateSelected = true;
		addNewRule();
		rowSelected = false;
	}
	
	/**
	 * Gets the initial date.
	 *
	 * @return the initialDate
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the initialDate to set
	 */
	public void setInitialDate(Date initialDate) {
		if(initialDate!=null && finalDate!=null){
			if(initialDate.compareTo(finalDate)>=0){
			finalDate = initialDate;
			}
		}
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the finalDate
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the finalDate to set
	 */
	public void setFinalDate(Date finalDate) {
		if(finalDate!=null && initialDate!=null){
			if(finalDate.compareTo(initialDate)>=0){
			this.finalDate=finalDate;
			}else{
				this.finalDate=initialDate;
			}
		}else{
			this.finalDate = finalDate;
		}

	}
	
	/**
	 * Before add new rule.
	 */
	@LoggerAuditWeb
	public void beforeAddNewRule(){
		if(newSelected){
			hideDialogs();
			if(systemSelected.equals(Integer.valueOf(-1))){
				JSFUtilities.showRequiredDialog();
				return;
			}			
			if(!selectionEvent){
				selectionEvent = false; 
				JSFUtilities.showComponent("cnfEndTransactionSamePage");
				JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, 
						PropertiesConstants.ERROR_MSG_SELECTONETABLE, null);
				return;
			}else{
				JSFUtilities.showComponent("frmSensitiveFieldAudit:cnfaddNewrule");
				Object[] obj = new Object[1];
				obj[0] = actionTable.getRuleName();
				JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,
						PropertiesConstants.CNF_ADD_NEWRULE, obj);
			}
		}else{
			addNewRule();
		}
	}
	
	/**
	 * Beforeremoverule.
	 */
	public void beforeremoverule(){
		hideDialogs(); 
		/*if(Validations.validateIsNullOrEmpty(actionTable)){
			JSFUtilities.showComponent("cnfEndTransactionSamePage");
			JSFUtilities.showMessageOnDialog(null, null, PropertiesConstants.ERROR_SELECT_RECORD_TODELETE, null);
			return;
		}*/
		
		//JSFUtilities.showComponent("frmSensitiveFieldConsult:eliminarConfirmDialog");
		JSFUtilities.executeJavascriptFunction("PF('eliminarConfirmation').show();");
		if (selectedRecords != null && selectedRecords.length == 1) {
			JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,
					PropertiesConstants.CNF_DELETE_RULE, null);
		} else if (selectedRecords != null && selectedRecords.length > 1) {
			JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,
					PropertiesConstants.CNF_DELETE_MULTIPLERULE, null);
		}
	}

	public void beforeConfirmRule(){
		hideDialogs();
		if(this.auditsensitiveFieldBeanFacade.checkIfSqlsWereExecuted(selectedRule) 
				|| this.selectedRule.getSituation().equals(AuditTableSituationType.PENDING_EXECUTION.getCode()) ) {
			JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
			JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,
					PropertiesConstants.AUDIT_ERR_MSG_SQL_NO_EXECUTED, null);
			JSFUtilities.showSimpleValidationDialog();
		} else {
			JSFUtilities.executeJavascriptFunction("PF('ConfirmDialogWdvar').show();");
			JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,
				"audit.msg.alert.confirm", null);
		}
	}
	
	public void beforeRejectedRule(){
		hideDialogs();
		JSFUtilities.executeJavascriptFunction("PF('RejectedDialogWdvar').show();");
		JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,
				"audit.msg.alert.rejected", null);
	}
	
	public void beforeDisabledRule(){
		hideDialogs();
		if(selectedRule!=null){
			JSFUtilities.executeJavascriptFunction("PF('DisabledDialogWdvar').show();");
			String message =null;
			if(selectedRule.getSituation().equals(AuditTableSituationType.ACTIVE.getCode())){
				message="audit.msg.alert.register.deactivate";
			} else {
				//confirm desactivate
				message="audit.msg.alert.confirm.deactivate";
			}
			JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,
					message, null);
		}
	}
	
	@LoggerAuditWeb
	public void registerDesactivate() throws ServiceException{
		if(selectedRule!=null){
			try{
				auditsensitiveFieldBeanFacade.registerDesactivateTriggerRule(selectedRule);
				selectedRule = null;
				fillLog();
				} catch(ServiceException sex){
					if(sex.getMessage()!=null){
						JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
						showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION,sex.getErrorService().getMessage());
						JSFUtilities.showSimpleValidationDialog();
						selectedRule = null;
						fillLog();
					}
				}catch (Exception e) {
					excepcion.fire(new ExceptionToCatchEvent(e));
				}
		}
	}
	
	@LoggerAuditWeb
	public void rejectedDesactivate() throws ServiceException{
		if(selectedRule!=null){
			try{
				auditsensitiveFieldBeanFacade.rejectedDesactivateTriggerRule(selectedRule);
				selectedRule = null;
				fillLog();
				} catch(ServiceException sex){
					if(sex.getMessage()!=null){
						JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
						showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION,sex.getErrorService().getMessage());
						JSFUtilities.showSimpleValidationDialog();
						selectedRule = null;
						fillLog();
					}
				}catch (Exception e) {
					excepcion.fire(new ExceptionToCatchEvent(e));
				}
		}
	}
	
	@LoggerAuditWeb
	public void confirmDesactivate() throws ServiceException{
		if(selectedRule!=null){
			try {
				auditsensitiveFieldBeanFacade.confirmdDesactivateTriggerRule(selectedRule);
				selectedRule = null;
				fillLog();
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,
						PropertiesConstants.AUDIT_MSG_DELETE_SUCCESS, null);
				JSFUtilities.showSimpleValidationDialog();
			} catch (ServiceException sex) {
				if (sex.getMessage() != null) {
					JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
					showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION, sex.getErrorService().getMessage());
					JSFUtilities.showSimpleValidationDialog();
					selectedRule = null;
					fillLog();
				}
			} catch (Exception e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
		}
	}
	
	@LoggerAuditWeb
	public void confirmTriggerRule() {
		if (selectedRule != null) {
			try {
				auditsensitiveFieldBeanFacade.confirmRulestrigger(selectedRule);
				selectedRule = null;
				fillLog();

				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,
						PropertiesConstants.AUDIT_MSG_CONFIRM_SUCCESS, null);
				JSFUtilities.showSimpleValidationDialog();
			} catch (ServiceException sex) {
				if (sex.getMessage() != null) {
					JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
					showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION, sex.getErrorService().getMessage());
					JSFUtilities.showSimpleValidationDialog();
					selectedRule = null;
					fillLog();
				}
			} catch (Exception e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
		}
	}
	
	@LoggerAuditWeb
	public void rejectedTriggerRule(){
		if(selectedRule!=null){
			try{
			auditsensitiveFieldBeanFacade.rejectedRulesTrigger(selectedRule);
			selectedRule = null;
			fillLog();
			} catch(ServiceException sex){
				if(sex.getMessage()!=null){
					JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
					showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION,sex.getErrorService().getMessage());
					JSFUtilities.showSimpleValidationDialog();
					selectedRule = null;
					fillLog();
				}
			}catch (Exception e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
		}
	}

	/**
	 * Gets the selected records.
	 *
	 * @return the selectedRecords
	 */
	public ActionTable[] getSelectedRecords() {
		return selectedRecords;
	}

	/**
	 * Sets the selected records.
	 *
	 * @param selectedRecords the selectedRecords to set
	 */
	public void setSelectedRecords(ActionTable[] selectedRecords) {
		this.selectedRecords = selectedRecords;
	}

	/**
	 * Gets the state selected.
	 *
	 * @return the stateSelected
	 */
	public Integer getStateSelected() {
		return stateSelected;
	}

	/**
	 * Sets the state selected.
	 *
	 * @param stateSelected the stateSelected to set
	 */
	public void setStateSelected(Integer stateSelected) {
		this.stateSelected = stateSelected;
	}

	/**
	 * Gets the actiontable states.
	 *
	 * @return the actiontableStates
	 */
	public List<StateType> getActiontableStates() {		
		return actiontableStates;
	}

	/**
	 * Sets the actiontable states.
	 *
	 * @param actiontableStates the actiontableStates to set
	 */
	public void setActiontableStates(List<StateType> actiontableStates) {
		this.actiontableStates = actiontableStates;
	}
	
	/**
	 * Cleanup.
	 */
	@PreDestroy
	void cleanup()
	{
		endConversation();
	}
	
	/**
	 * Checks if is view button.
	 *
	 * @return the viewButton
	 */
	public boolean isViewButton() {
		return viewButton;
	}

	/**
	 * Sets the view button.
	 *
	 * @param viewButton the viewButton to set
	 */
	public void setViewButton(boolean viewButton) {
		this.viewButton = viewButton;
	}

	/**
	 * Checks if is err msg.
	 *
	 * @return the errMsg
	 */
	public boolean isErrMsg() {
		return errMsg;
	}

	
	/**
	 * Checks if is show save.
	 *
	 * @return the showSave
	 */
	public boolean isShowSave() {
		return showSave;
	}

	/**
	 * Sets the show save.
	 *
	 * @param showSave the showSave to set
	 */
	public void setShowSave(boolean showSave) {
		this.showSave = showSave;
	}

	/**
	 * Sets the err msg.
	 *
	 * @param errMsg the errMsg to set
	 */
	public void setErrMsg(boolean errMsg) {
		this.errMsg = errMsg;
	}

	/**
	 * Checks if is aggregar.
	 *
	 * @return the aggregar
	 */
	public boolean isAggregar() {
		return aggregar;
	}

	/**
	 * Sets the aggregar.
	 *
	 * @param aggregar the aggregar to set
	 */
	public void setAggregar(boolean aggregar) {
		this.aggregar = aggregar;
	}

	/**
	 * Gets the lst fields table search.
	 *
	 * @return the lstFieldsTableSearch
	 */
	public List<FieldsTableDataBase> getLstFieldsTableSearch() {
		return lstFieldsTableSearch;
	}

	/**
	 * Sets the lst fields table search.
	 *
	 * @param lstFieldsTableSearch the lstFieldsTableSearch to set
	 */
	public void setLstFieldsTableSearch(
			List<FieldsTableDataBase> lstFieldsTableSearch) {
		this.lstFieldsTableSearch = lstFieldsTableSearch;
	}

	/**
	 * Gets the max date.
	 *
	 * @return the maxDate
	 */
	public Date getMaxDate() {
		maxDate = CommonsUtilities.currentDate();
		return maxDate;
	}

	/**
	 * Sets the max date.
	 *
	 * @param maxDate            the maxDate to set
	 */
	public void setMaxDate(Date maxDate) {
		this.maxDate = maxDate;
	}
	
	/**
	 * Checks if is search system.
	 *
	 * @return the searchSystem
	 */
	public boolean isSearchSystem() {
		return searchSystem;
	}

	/**
	 * Sets the search system.
	 *
	 * @param searchSystem the searchSystem to set
	 */
	public void setSearchSystem(boolean searchSystem) {
		this.searchSystem = searchSystem;
	}

	/**
	 * Gets the system selected search.
	 *
	 * @return the systemSelectedSearch
	 */
	public Integer getSystemSelectedSearch() {
		return systemSelectedSearch;
	}

	/**
	 * Gets the table selected search.
	 *
	 * @return the tableSelectedSearch
	 */
	public Integer getTableSelectedSearch() {
		return tableSelectedSearch;
	}

	/**
	 * Sets the table selected search.
	 *
	 * @param tableSelectedSearch the tableSelectedSearch to set
	 */
	public void setTableSelectedSearch(Integer tableSelectedSearch) {
		this.tableSelectedSearch = tableSelectedSearch;
	}

	/**
	 * Gets the field selected search.
	 *
	 * @return the fieldSelectedSearch
	 */
	public Integer getFieldSelectedSearch() {
		return fieldSelectedSearch;
	}

	/**
	 * Sets the field selected search.
	 *
	 * @param fieldSelectedSearch the fieldSelectedSearch to set
	 */
	public void setFieldSelectedSearch(Integer fieldSelectedSearch) {
		this.fieldSelectedSearch = fieldSelectedSearch;
	}

	/**
	 * Sets the system selected search.
	 *
	 * @param systemSelectedSearch the systemSelectedSearch to set
	 */
	public void setSystemSelectedSearch(Integer systemSelectedSearch) {
		this.systemSelectedSearch = systemSelectedSearch;
	}

	/**
	 * Gets the action table data model search.
	 *
	 * @return the actionTableDataModelSearch
	 */
	public ActionTableDataModel getActionTableDataModelSearch() {
		return actionTableDataModelSearch;
	}

	/**
	 * Sets the action table data model search.
	 *
	 * @param actionTableDataModelSearch the actionTableDataModelSearch to set
	 */
	public void setActionTableDataModelSearch(
			ActionTableDataModel actionTableDataModelSearch) {
		this.actionTableDataModelSearch = actionTableDataModelSearch;
	}

	/**
	 * Gets the action tables search.
	 *
	 * @return the actionTablesSearch
	 */
	public List<ActionTable> getActionTablesSearch() {
		return actionTablesSearch;
	}

	/**
	 * Sets the action tables search.
	 *
	 * @param actionTablesSearch the actionTablesSearch to set
	 */
	public void setActionTablesSearch(List<ActionTable> actionTablesSearch) {
		this.actionTablesSearch = actionTablesSearch;
	}

	/**
	 * Checks if is checked.
	 *
	 * @return the checked
	 */
	public boolean isChecked() {
		return checked;
	}

	/**
	 * Sets the checked.
	 *
	 * @param checked the checked to set
	 */
	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	/**
	 * Checks if is selection event.
	 *
	 * @return the selectionEvent
	 */
	public boolean isSelectionEvent() {
		return selectionEvent;
	}

	/**
	 * Sets the selection event.
	 *
	 * @param selectionEvent the selectionEvent to set
	 */
	public void setSelectionEvent(boolean selectionEvent) {
		this.selectionEvent = selectionEvent;
	}

	/**
	 * Gets the lst tables.
	 *
	 * @return the lstTables
	 */
	public List<TableDataBase> getLstTables() {
		return lstTables;
	}

	/**
	 * Sets the lst tables.
	 *
	 * @param lstTables the lstTables to set
	 */
	public void setLstTables(List<TableDataBase> lstTables) {
		this.lstTables = lstTables;
	}
	
	/**
	 * Gets the lst fields table.
	 *
	 * @return the lstFieldsTable
	 */
	public List<FieldsTableDataBase> getLstFieldsTable() {
		return lstFieldsTable;
	}
	
	/**
	 * Gets the start date.
	 *
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Sets the start date.
	 *
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		 Date today = CommonsUtilities.currentDate();
		  if(startDate != null && today.compareTo(startDate)>=0){
		   this.startDate = startDate;
		  }else{
		   this.startDate = null;
		  }
	}

	/**
	 * Gets the end date.
	 *
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		 	Date today = CommonsUtilities.currentDate();
		  if(endDate != null && today.compareTo(endDate)>=0 && (startDate==null || startDate.compareTo(endDate)<=0)){
		   this.endDate = endDate;
		  }else{
		   this.endDate = null;
		  }
	}

	/**
	 * Sets the lst fields table.
	 *
	 * @param lstFieldsTable the lstFieldsTable to set
	 */
	public void setLstFieldsTable(List<FieldsTableDataBase> lstFieldsTable) {
		this.lstFieldsTable = lstFieldsTable;
	}

	/**
	 * Gets the lst users.
	 *
	 * @return the lstUsers
	 */
	public List<String> getLstUsers() {
		return lstUsers;
	}

	/**
	 * Sets the lst users.
	 *
	 * @param lstUsers the lstUsers to set
	 */
	public void setLstUsers(List<String> lstUsers) {
		this.lstUsers = lstUsers;
	}

	/**
	 * Gets the system selected.
	 *
	 * @return the systemSelected
	 */
	public Integer getSystemSelected() {
		return systemSelected;
	}

	/**
	 * Sets the system selected.
	 *
	 * @param systemSelected the systemSelected to set
	 */
	public void setSystemSelected(Integer systemSelected) {
		this.systemSelected = systemSelected;
	}
	
	/**
	 * Empty.
	 */
	public void empty(){
		
	}

	/**
	 * Gets the table selected.
	 *
	 * @return the tableSelected
	 */
	public Integer getTableSelected() {
		return tableSelected;
	}

	/**
	 * Sets the table selected.
	 *
	 * @param tableSelected the tableSelected to set
	 */
	public void setTableSelected(Integer tableSelected) {
		this.tableSelected = tableSelected;
	}

	/**
	 * Gets the field selected.
	 *
	 * @return the fieldSelected
	 */
	public Integer getFieldSelected() {
		return fieldSelected;
	}

	/**
	 * Sets the field selected.
	 *
	 * @param fieldSelected the fieldSelected to set
	 */
	public void setFieldSelected(Integer fieldSelected) {
		this.fieldSelected = fieldSelected;
	}

	/**
	 * Gets the user selected.
	 *
	 * @return the userSelected
	 */
	public String getUserSelected() {
		return userSelected;
	}

	/**
	 * Sets the user selected.
	 *
	 * @param userSelected the userSelected to set
	 */
	public void setUserSelected(String userSelected) {
		this.userSelected = userSelected;
	}

	/**
	 * Gets the action tables.
	 *
	 * @return the actionTables
	 */
	public List<ActionTable> getActionTables() {
		return actionTables;
	}

	/**
	 * Sets the action tables.
	 *
	 * @param actionTables the actionTables to set
	 */
	public void setActionTables(List<ActionTable> actionTables) {
		this.actionTables = actionTables;
	}

	/**
	 * Gets the action table.
	 *
	 * @return the actionTable
	 */
	public ActionTable getActionTable() {
		return actionTable;
	}

	/**
	 * Sets the action table.
	 *
	 * @param actionTable the actionTable to set
	 */
	public void setActionTable(ActionTable actionTable) {
			this.actionTable = actionTable;
	}

	/**
	 * Gets the action table modified.
	 *
	 * @return the actionTableModified
	 */
	public ActionTable getActionTableModified() {
		return actionTableModified;
	}

	/**
	 * Sets the action table modified.
	 *
	 * @param actionTableModified the actionTableModified to set
	 */
	public void setActionTableModified(ActionTable actionTableModified) {
		this.actionTableModified = actionTableModified;
	}

	/**
	 * Gets the rule selected.
	 *
	 * @return the ruleSelected
	 */
	public Integer getRuleSelected() {
		return ruleSelected;
	}

	/**
	 * Sets the rule selected.
	 *
	 * @param ruleSelected the ruleSelected to set
	 */
	public void setRuleSelected(Integer ruleSelected) {
		this.ruleSelected = ruleSelected;
	}

	/**
	 * Gets the action table data model.
	 *
	 * @return the actionTableDataModel
	 */
	public ActionTableDataModel getActionTableDataModel() {
		return actionTableDataModel;
	}

	/**
	 * Sets the action table data model.
	 *
	 * @param actionTableDataModel the actionTableDataModel to set
	 */
	public void setActionTableDataModel(ActionTableDataModel actionTableDataModel) {
		this.actionTableDataModel = actionTableDataModel;
	}

	/**
	 * Gets the table data model.
	 *
	 * @return the tableDataModel
	 */
	public TableDataModel getTableDataModel() {
		return tableDataModel;
	}

	/**
	 * Sets the table data model.
	 *
	 * @param tableDataModel the tableDataModel to set
	 */
	public void setTableDataModel(TableDataModel tableDataModel) {
		this.tableDataModel = tableDataModel;
	}

	/**
	 * Gets the table data base.
	 *
	 * @return the tableDataBase
	 */
	public TableDataBase getTableDataBase() {
		return tableDataBase;
	}

	/**
	 * Sets the table data base.
	 *
	 * @param tableDataBase the tableDataBase to set
	 */
	public void setTableDataBase(TableDataBase tableDataBase) {
		this.tableDataBase = tableDataBase;
	}

	/**
	 * Gets the data model tabla campo.
	 *
	 * @return the dataModelTablaCampo
	 */
	public GenericDataModel<FieldsTableDataBase> getDataModelTablaCampo() {
		return dataModelTablaCampo;
	}

	/**
	 * Sets the data model tabla campo.
	 *
	 * @param dataModelTablaCampo the dataModelTablaCampo to set
	 */
	public void setDataModelTablaCampo(
			GenericDataModel<FieldsTableDataBase> dataModelTablaCampo) {
		this.dataModelTablaCampo = dataModelTablaCampo;
	}

	/**
	 * Gets the fields table data base.
	 *
	 * @return the fieldsTableDataBase
	 */
	public FieldsTableDataBase getFieldsTableDataBase() {
		return fieldsTableDataBase;
	}

	/**
	 * Sets the fields table data base.
	 *
	 * @param fieldsTableDataBase the fieldsTableDataBase to set
	 */
	public void setFieldsTableDataBase(FieldsTableDataBase fieldsTableDataBase) {
		this.fieldsTableDataBase = fieldsTableDataBase;
	}

	/**
	 * Checks if is row selected.
	 *
	 * @return the rowSelected
	 */
	public boolean isRowSelected() {
		return rowSelected;
	}

	/**
	 * Sets the row selected.
	 *
	 * @param rowSelected the rowSelected to set
	 */
	public void setRowSelected(boolean rowSelected) {
		this.rowSelected = rowSelected;
	}

	/**
	 * Checks if is adds the rule.
	 *
	 * @return the addRule
	 */
	public boolean isAddRule() {
		return addRule;
	}

	/**
	 * Sets the adds the rule.
	 *
	 * @param addRule the addRule to set
	 */
	public void setAddRule(boolean addRule) {
		this.addRule = addRule;
	}

	/**
	 * Checks if is new selected.
	 *
	 * @return the newSelected
	 */
	public boolean isNewSelected() {
		return newSelected;
	}

	/**
	 * Sets the new selected.
	 *
	 * @param newSelected the newSelected to set
	 */
	public void setNewSelected(boolean newSelected) {
		this.newSelected = newSelected;
	}

	/**
	 * Checks if is modify activated.
	 *
	 * @return the modifyActivated
	 */
	public boolean isModifyActivated() {
		return modifyActivated;
	}

	/**
	 * Sets the modify activated.
	 *
	 * @param modifyActivated the modifyActivated to set
	 */
	public void setModifyActivated(boolean modifyActivated) {
		this.modifyActivated = modifyActivated;
	}

	/**
	 * Checks if is desactivate selected.
	 *
	 * @return the desactivateSelected
	 */
	public boolean isDesactivateSelected() {
		return desactivateSelected;
	}

	/**
	 * Sets the desactivate selected.
	 *
	 * @param desactivateSelected the desactivateSelected to set
	 */
	public void setDesactivateSelected(boolean desactivateSelected) {
		this.desactivateSelected = desactivateSelected;
	}

	/**
	 * Checks if is activate selected.
	 *
	 * @return the activateSelected
	 */
	public boolean isActivateSelected() {
		return activateSelected;
	}

	/**
	 * Sets the activate selected.
	 *
	 * @param activateSelected the activateSelected to set
	 */
	public void setActivateSelected(boolean activateSelected) {
		this.activateSelected = activateSelected;
	}

	/**
	 * Checks if is eliminar selected.
	 *
	 * @return the eliminarSelected
	 */
	public boolean isEliminarSelected() {
		return eliminarSelected;
	}

	/**
	 * Sets the eliminar selected.
	 *
	 * @param eliminarSelected the eliminarSelected to set
	 */
	public void setEliminarSelected(boolean eliminarSelected) {
		this.eliminarSelected = eliminarSelected;
	}

	/**
	 * Checks if is rule selected disable.
	 *
	 * @return the ruleSelectedDisable
	 */
	public boolean isRuleSelectedDisable() {
		return ruleSelectedDisable;
	}

	/**
	 * Sets the rule selected disable.
	 *
	 * @param ruleSelectedDisable the ruleSelectedDisable to set
	 */
	public void setRuleSelectedDisable(boolean ruleSelectedDisable) {
		this.ruleSelectedDisable = ruleSelectedDisable;
	}

	/**
	 * Checks if is table selected disable.
	 *
	 * @return the tableSelectedDisable
	 */
	public boolean isTableSelectedDisable() {
		return tableSelectedDisable;
	}

	/**
	 * Sets the table selected disable.
	 *
	 * @param tableSelectedDisable the tableSelectedDisable to set
	 */
	public void setTableSelectedDisable(boolean tableSelectedDisable) {
		this.tableSelectedDisable = tableSelectedDisable;
	}

	/**
	 * Gets the log.
	 *
	 * @return the log
	 */
	public static Logger getLog() {
		return log;
	}

	/**
	 * Instantiates a new consult sensitive field audit bean.
	 */
	public ConsultSensitiveFieldAuditBean(){
		//empty constructor
	}


	/**
	 * Gets the rules states.
	 *
	 * @return the rules states
	 */
	public List<Parameter> getRulesStates() {
		return rulesStates;
	}


	/**
	 * Sets the rules states.
	 *
	 * @param rulesStates the new rules states
	 */
	public void setRulesStates(List<Parameter> rulesStates) {
		this.rulesStates = rulesStates;
	}


	public ActionTable getSelectedRule() {
		return selectedRule;
	}


	public void setSelectedRule(ActionTable selectedRule) {
		this.selectedRule = selectedRule;
	}


	public Map<Integer,String> getStatesDescription() {
		return statesDescription;
	}


	public void setStatesDescription(Map<Integer,String> statesDescription) {
		this.statesDescription = statesDescription;
	}


	public Map<Integer,String> getSituationDescription() {
		return situationDescription;
	}


	public void setSituationDescription(Map<Integer,String> situationDescription) {
		this.situationDescription = situationDescription;
	}
}
