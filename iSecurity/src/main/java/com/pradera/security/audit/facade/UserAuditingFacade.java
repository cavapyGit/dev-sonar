package com.pradera.security.audit.facade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.audit.service.UserAuditingServiceBean;
import com.pradera.security.model.UserAuditTracking;
import com.pradera.security.model.UserAuditTrackingDetail;
import com.pradera.security.model.UserSession;

/**
 * <ul>
 * <li>
 * Project PraderaSecurity.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Class UserAuditingFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 10/01/2012
 */
@Stateless
@Performance
public class UserAuditingFacade  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** The user session service bean. */
	@EJB
	UserAuditingServiceBean userAuditingServiceBean;
	/** The crud security service bean. */

    
	/**
	 * Close session service facade.
	 *
	 * @param userInfo the user session
	 * @throws ServiceException the service exception
	 */
	public void closeSessionServiceFacade(UserInfo userInfo) throws ServiceException{
		userAuditingServiceBean.closeSessionServiceBean(userInfo);	
	}
	/**
     * Register User Audit Tracking Service Facade
     * @param userAuditTracking
     * @return true, if successful
     * @throws ServiceException
     */
    public boolean registerUserAuditTrackingServiceFacade(UserAuditTracking userAuditTracking) throws ServiceException {
    	userAuditingServiceBean.create(userAuditTracking);
        return true;
    }
    
    /**
	 * 
	 * @param url
	 * @param exceptionMsg
	 * @param queryString
	 * @param timeAccess
	 * @param SessionPK
	 * @param parameters
	 * @return
	 * @throws ServiceException
	 */
    @Asynchronous
	public void registerUserAuditTrackingServiceFacade(String url, String exceptionMsg, String queryString, Date timeAccess,
			Long SessionPK, Map<String, String> parameters)	throws ServiceException{
		UserAuditTracking userAuditTracking = new UserAuditTracking();
		if(Validations.validateIsNotNullAndNotEmpty(url)){
			userAuditTracking.setUrl(url);
		}
		if(Validations.validateIsNotNullAndNotEmpty(exceptionMsg)){
			userAuditTracking.setExceptionMessage(exceptionMsg);
		}
		if(Validations.validateIsNotNullAndNotEmpty(queryString)){
			userAuditTracking.setQueryString(queryString);
		}
		if(Validations.validateIsNotNull(timeAccess)){
			userAuditTracking.setTimeAccess(timeAccess);
		}
		if(Validations.validateIsNotNull(SessionPK)){
			UserSession objSession = new UserSession();
			objSession.setIdUserSessionPk(SessionPK);
			userAuditTracking.setUserSession(objSession);
		}
		if(parameters != null && parameters.size() > 0){
			List<UserAuditTrackingDetail> userAuditTrackingDetails = new ArrayList<UserAuditTrackingDetail>();
			UserAuditTrackingDetail userAuditTrackDetail = null;
			for(String name : parameters.keySet()){
				userAuditTrackDetail = new UserAuditTrackingDetail();
				userAuditTrackDetail.setParameterName(name);
				userAuditTrackDetail.setParameterValue(parameters.get(name));
				userAuditTrackDetail.setUserAuditTracking(userAuditTracking);
			}
			userAuditTracking.setUserAuditTrackingDetails(userAuditTrackingDetails);
		}
		userAuditingServiceBean.create(userAuditTracking);
	}
    
    /**
	 * Close all sessions service bean.
	 *
	 * @throws ServiceException the service exception
	 */
	public void closeAllOpenSessions() throws ServiceException{
		userAuditingServiceBean.closeAllOpenSessions();
	}
}
