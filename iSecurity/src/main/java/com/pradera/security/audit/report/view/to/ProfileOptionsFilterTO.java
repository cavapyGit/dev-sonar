package com.pradera.security.audit.report.view.to;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ProfileOptionsFilterTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 08-mar-2015
 */
public class ProfileOptionsFilterTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7913152858165829985L;
	
	/** The mnemonic. */
	private String mnemonic;
	
	/** The profile name. */
	private String profileName;
	
	/** The profile type. */
	private Integer profileType;
	
	/** The profile state. */
	private Integer profileState;
	
	/** The profile situation. */
	private Integer profileSituation;
	
	/** The system id. */
	private Integer systemId;
	
	/** The module name. */
	private String moduleName;
	
	/** The privilege state. */
	private Integer privilegeState;

	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}

	/**
	 * Sets the mnemonic.
	 *
	 * @param mnemonic the new mnemonic
	 */
	public void setMnemonic(String mnemonic) {
		if(StringUtils.isNotBlank(mnemonic)){
			mnemonic = mnemonic.toUpperCase();
		}
		this.mnemonic = mnemonic;
	}

	/**
	 * Gets the profile name.
	 *
	 * @return the profile name
	 */
	public String getProfileName() {
		return profileName;
	}

	/**
	 * Sets the profile name.
	 *
	 * @param profileName the new profile name
	 */
	public void setProfileName(String profileName) {
		if(StringUtils.isNotBlank(profileName)){
			profileName = profileName.toUpperCase();
		}
		this.profileName = profileName;
	}

	/**
	 * Gets the profile type.
	 *
	 * @return the profile type
	 */
	public Integer getProfileType() {
		return profileType;
	}

	/**
	 * Sets the profile type.
	 *
	 * @param profileType the new profile type
	 */
	public void setProfileType(Integer profileType) {
		this.profileType = profileType;
	}

	/**
	 * Gets the profile state.
	 *
	 * @return the profile state
	 */
	public Integer getProfileState() {
		return profileState;
	}

	/**
	 * Sets the profile state.
	 *
	 * @param profileState the new profile state
	 */
	public void setProfileState(Integer profileState) {
		this.profileState = profileState;
	}

	/**
	 * Gets the profile situation.
	 *
	 * @return the profile situation
	 */
	public Integer getProfileSituation() {
		return profileSituation;
	}

	/**
	 * Sets the profile situation.
	 *
	 * @param profileSituation the new profile situation
	 */
	public void setProfileSituation(Integer profileSituation) {
		this.profileSituation = profileSituation;
	}

	/**
	 * Gets the system id.
	 *
	 * @return the system id
	 */
	public Integer getSystemId() {
		return systemId;
	}

	/**
	 * Sets the system id.
	 *
	 * @param systemId the new system id
	 */
	public void setSystemId(Integer systemId) {
		this.systemId = systemId;
	}

	/**
	 * Gets the module name.
	 *
	 * @return the module name
	 */
	public String getModuleName() {
		return moduleName;
	}

	/**
	 * Sets the module name.
	 *
	 * @param moduleName the new module name
	 */
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	/**
	 * Gets the privilege state.
	 *
	 * @return the privilege state
	 */
	public Integer getPrivilegeState() {
		return privilegeState;
	}

	/**
	 * Sets the privilege state.
	 *
	 * @param privilegeState the new privilege state
	 */
	public void setPrivilegeState(Integer privilegeState) {
		this.privilegeState = privilegeState;
	}
	
	

}
