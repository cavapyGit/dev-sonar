package com.pradera.security.audit.view;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.apache.log4j.Logger;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.primefaces.push.PushContext;
import org.primefaces.push.PushContextFactory;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.httpevent.publish.BrowserWindow;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.security.model.type.LanguageType;
import com.pradera.commons.security.model.type.UserAccountStateType;
import com.pradera.commons.security.model.type.UserSessionStateType;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.security.audit.facade.MonitoringUserAuditFacade;
import com.pradera.security.audit.view.filters.MonUserAuditSearchFilterBean;
import com.pradera.security.audit.view.filters.MonitoringUserAuditFilterBean;
import com.pradera.security.model.InstitutionTypeDetail;
import com.pradera.security.model.InstitutionsSecurity;
import com.pradera.security.model.Parameter;
import com.pradera.security.model.System;
import com.pradera.security.model.SystemProfile;
import com.pradera.security.model.UserAccount;
import com.pradera.security.model.UserProfile;
import com.pradera.security.model.UserSession;
import com.pradera.security.model.type.DefinitionType;
import com.pradera.security.model.type.InstitutionTypeDetailStateType;
import com.pradera.security.model.type.ParameterTableType;
import com.pradera.security.model.type.SystemProfileStateType;
import com.pradera.security.model.type.SystemStateType;
import com.pradera.security.model.type.UserAccountType;
import com.pradera.security.model.type.UserProfileStateType;
import com.pradera.security.systemmgmt.view.NodeBean;
import com.pradera.security.systemmgmt.view.filters.InstitutionSecurityFilter;
import com.pradera.security.systemmgmt.view.filters.SystemProfileFilter;
import com.pradera.security.utils.view.GenericBaseBean;
import com.pradera.security.utils.view.LDAPUtilities;
import com.pradera.security.utils.view.LDAPUtility;
import com.pradera.security.utils.view.PropertiesConstants;
import com.pradera.security.utils.view.SecurityUtilities;


/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class MonitoringUserAuditBean
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaSecurity
 * @Creation_Date :
 */

@DepositaryWebBean
public class MonitoringUserAuditBean extends GenericBaseBean implements
		Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The hourstimeformat. */
	private final String HOURSTIMEFORMAT = "h:mm a";
	
	/** The option type. */
	private Integer optionType;
	
	/** The user type selected. */
	private Integer userTypeSelected;
	
	/** The lst institution types. */
	private List<InstitutionType> lstInstitutionTypes;
	
	/** The entity type selected. */
	private Integer entityTypeSelected;
	
	/** The entity selected. */
	private Integer entitySelected;
	/** The lst institutions security for filter. */
	private List<InstitutionsSecurity> lstInstitutionsSecurityForFilter;
	
	/** The lst users. */
	private List<String> lstUsers;

	/** The user. */
	private MonUserAuditSearchFilterBean user;
	
	/** The Constant log. */
	private static final Logger log = Logger
			.getLogger(MonitoringUserAuditBean.class);
	
	/** The restrict mac. */
	private boolean restrictMac;
	
	/** The tree profiles root. */
	private TreeNode treeProfilesRoot;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/** The monitoring user audit facade. */
	@EJB
	private MonitoringUserAuditFacade monitoringUserAuditFacade;
	
	/** The monitoring user audit filter bean. */
	@Inject
	private MonitoringUserAuditFilterBean monitoringUserAuditFilterBean;
	
	/** The user monit audit bean. */
	private UserAccount userMonitAuditBean;
	
	/** The lst users result. */
	private GenericDataModel<MonUserAuditSearchFilterBean> lstUsersResult;
	
	/** The lst user sessions. */
	private GenericDataModel<MonUserAuditSearchFilterBean> lstUserSessions;
	
	/** The user pk. */
	private Integer userPk;

	/** The lst estado. */
	private List<UserAccountStateType> lstEstado;
	
	/** The codigo user. */
	private String codigoUser;
	
	/** The estado selected. */
	private Integer estadoSelected;
	
	/** The in user audit. */
	private boolean inUserAudit;
	
	/** The main institution. */
	private boolean mainInstitution;

	/** The lst profiles assigned to user. */
	private List<UserProfile> lstProfilesAssignedToUser;

	/** The user account bean. */
	private UserAccount userAccountBean;
	/** The list institution type detail assigned to user. */
	private List<InstitutionTypeDetail> listInstitutionTypeDetailAssignedToUser;
	/** The lst institution type. */
	private List<InstitutionType> lstInstitutionType;
	/** The lst show institution type assigned to user. */
	private List<String> lstShowInstitutionTypeAssignedToUser;
	
	/**  valid user. */

	private boolean validUserFalg;

	/** The user session state type. */
	private List userSessionStateType;

	/** The selected state connection. */
	private Integer selectedStateConnection;
	
    /** The lst institution type for filter. */
	private List<Parameter> lstInstitutionTypeForFilter;
	
	/** The lst institution type for filter. */
	private List<Parameter> lstInstitutionTypeForFilterTemp;
	
	/** The lista tipo cliente. */
    private List<Parameter> lstClientTypeValues;

	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The ldap utilities. */
	@Inject
	@LDAPUtility
	LDAPUtilities ldapUtilities;

	/**
	 * Instantiates a new monitoring user audit bean.
	 */
	public MonitoringUserAuditBean() {
		optionType = Integer.valueOf(0);
		userTypeSelected = Integer.valueOf(-1);
		entityTypeSelected = Integer.valueOf(-1);
		entitySelected = Integer.valueOf(-1);
		lstUsers = new ArrayList<String>();
		userMonitAuditBean = new UserAccount();
		selectedStateConnection = -1;

	}

	
	public Map<String, Object> generateParametersExport(){
		//using LinkedHashMap for using order of insert in map
		LinkedHashMap<String, Object> parametersExport = new LinkedHashMap<>();
		FacesContext context = FacesContext.getCurrentInstance();
	    ResourceBundle msgs = context.getApplication().evaluateExpressionGet(context, "#{msgs}", ResourceBundle.class);
	    //parameter userName
	    parametersExport.put(msgs.getString("user.col.code"), user.getPloginUser());
	    //parameter empty only for look and feel
	    parametersExport.put(" "," ");
	    //parameter initial Date
	    parametersExport.put(msgs.getString("schedule.exception.col.initial.date"), CommonsUtilities.convertDatetoString(initialDate));
	    //parameter final Date
	    parametersExport.put(msgs.getString("schedule.exception.col.final.daten"), CommonsUtilities.convertDatetoString(finalDate));
		return parametersExport;
	}

	/**
	 * Change user type for filter listener.
	 */
	public void changeUserTypeForFilterListener(){
			if(userTypeSelected.intValue()>0){
				if(userTypeSelected.intValue()==UserAccountType.INTERNAL.getCode().intValue()){
					mainInstitution = true;
					//Commeneted by Hema
					//lstInstitutionTypes=InstitutionType.listInternalInstitutions;
					lstInstitutionTypeForFilter.addAll(lstInstitutionTypeForFilterTemp);
					for(Parameter param:lstInstitutionTypeForFilterTemp){
						if(!InstitutionType.DEPOSITARY.getCode().equals(param.getIdParameterPk())){
							lstInstitutionTypeForFilter.remove(param);
						}
					}
					setEntityTypeSelected(InstitutionType.DEPOSITARY.getCode().intValue());					
					changeInstitutionTypeForFilterListener();
					
					HtmlSelectOneMenu combo=(HtmlSelectOneMenu)JSFUtilities.findViewComponent("formMonitoringUserAudit:cboEntityList");
					UISelectItems items=(UISelectItems)combo.getChildren().get(1);
					List<InstitutionsSecurity> lstInstituions=(List<InstitutionsSecurity>)items.getValue();
					setEntitySelected(Integer.valueOf(lstInstituions.get(0).getIdInstitutionSecurityPk()));
				}else{
					//Commeneted by Hema
					//lstInstitutionTypes=InstitutionType.listExternalInstitutions;
					lstInstitutionTypeForFilter=new ArrayList<Parameter>();
					lstInstitutionTypeForFilter.addAll(lstInstitutionTypeForFilterTemp);
					for(Parameter param:lstInstitutionTypeForFilterTemp){
						if(InstitutionType.DEPOSITARY.getCode().equals(param.getIdParameterPk())){
							lstInstitutionTypeForFilter.remove(param);
						}
					}
					lstInstitutionsSecurityForFilter=new ArrayList<InstitutionsSecurity>();
					setEntityTypeSelected(Integer.valueOf(-1));
					mainInstitution = false;
				}
			} else{
				mainInstitution = false;
				lstInstitutionTypeForFilter=new ArrayList<Parameter>();
				lstInstitutionsSecurityForFilter=new ArrayList<InstitutionsSecurity>();
				setEntityTypeSelected(Integer.valueOf(-1));
				setEntitySelected(Integer.valueOf(-1));
			}
	}
	/**
	 * Change institution type for filter listener.
	 */
	public void changeInstitutionTypeForFilterListener(){
		try {
			if(entityTypeSelected > 0){
				//used to invisible the Institution when the selected Institution type is other than Issuer and Participant
//				if(entityTypeSelected.equals(InstitutionType.ISSUER.getCode()) 
//					|| entityTypeSelected.equals(InstitutionType.PARTICIPANT.getCode()) 
//					|| entityTypeSelected.equals(InstitutionType.AFP.getCode())){
//					
					InstitutionSecurityFilter insSecurityFilter=new InstitutionSecurityFilter();
					insSecurityFilter.setIdInstitutionTypeFk(entityTypeSelected );
					lstInstitutionsSecurityForFilter=userServiceFacade.getInstitutionsByFilterServiceFacade(insSecurityFilter);
				//}
			}else{
				lstInstitutionsSecurityForFilter=new ArrayList<InstitutionsSecurity>();
				setEntitySelected(Integer.valueOf(-1));
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * search details of user.
	 */
	public void listSearchResult(){
		try{
			lstUsersResult = null;
			if(!inValidUser()){
				JSFUtilities.hideGeneralDialogues();
				List<Object[]> lstSerachData = null;
				monitoringUserAuditFilterBean.setEntitySeletectedPk(entitySelected);
				monitoringUserAuditFilterBean.setUserTypeId(userTypeSelected);
				monitoringUserAuditFilterBean.setUserCode(codigoUser);
				monitoringUserAuditFilterBean.setStateSelected(estadoSelected);
				monitoringUserAuditFilterBean.setEntityTypeSeletected(entityTypeSelected);	
				monitoringUserAuditFilterBean.setInstitution(entitySelected);
				monitoringUserAuditFilterBean.setSelectedStateConnection(selectedStateConnection);
				
				lstSerachData = monitoringUserAuditFacade.searchAuditUsersServiceFacade(monitoringUserAuditFilterBean,false);
				List<MonUserAuditSearchFilterBean> lstAuditSearch = convertToMonUserAuditSearchFilterBean(lstSerachData);
				
				lstUsersResult = new GenericDataModel<MonUserAuditSearchFilterBean>(lstAuditSearch);
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Removes the duplicate users.
	 *
	 * @param lstAuditSearch the lst audit search
	 * @param lstSerachData the lst serach data
	 * @return the list
	 */
	public List<MonUserAuditSearchFilterBean> removeDuplicateUsers(List<MonUserAuditSearchFilterBean> lstAuditSearch, List<Object[]> lstSerachData){
		for (Object[] objarr : lstSerachData) {
			if(objarr[0] != null){
				for(MonUserAuditSearchFilterBean searchFilterBean: lstAuditSearch){
					if(objarr[0].toString().equals(searchFilterBean.getPloginUser())){
						lstAuditSearch.remove(searchFilterBean);
						break;
					}
				}
			
			}
		}
		return lstAuditSearch;
	}
	
	/**
	 * The method is used to convert List of Object Array to List of MonUserAuditSearchFilterBean.
	 *
	 * @param objectArr 					List<objcet[]>
	 * @return 					List<MonUserAuditSearchFilterBean>
	 */
	public List<MonUserAuditSearchFilterBean> convertToMonUserAuditSearchFilterBean(List<Object[]> objectArr){
		List<MonUserAuditSearchFilterBean> lstAuditSearch = new ArrayList<MonUserAuditSearchFilterBean>();
		MonUserAuditSearchFilterBean searchBean = null;
		for (Object[] objarr : objectArr) {
			searchBean = new MonUserAuditSearchFilterBean();
			if(objarr[0] != null)
				searchBean.setPloginUser(objarr[0].toString());
			if(objarr[1] != null)
				searchBean.setPfullName(objarr[1].toString());
			if(objarr[2] != null)
				searchBean.setPfirtsLastName(objarr[2].toString());
			if(objarr[3] != null)
				searchBean.setPsecondLastName(objarr[3].toString());
			if(objarr[4] != null)
				searchBean.setPuserType(getUserAccType(objarr[4]));
			if(objarr[5] != null)
				searchBean.setPinstitutionType(getInstituteType(objarr[5]));
			if(objarr[6] != null)
				searchBean.setPinstitutionName(objarr[6].toString());
			if(objarr[7] != null)
				searchBean.setPlastLoginDate((Date) objarr[7]);
			if(objarr[9] != null)
				searchBean.setPconnectionStatus(Integer.parseInt(objarr[9].toString()));
			if(objarr[11] != null)
				searchBean.setPidUserPk(Integer.parseInt(objarr[11].toString()));
			if(objarr[12] != null)
				searchBean.setPidPhoneNo(objarr[12].toString());
			if(objarr[13] != null)
				searchBean.setPidEmail(objarr[13].toString());
			if(objarr[10] != null){
				searchBean.setPstate(getUserAccState(objarr[10]));
				searchBean.setIstate(Integer.parseInt(objarr[10].toString()));
			}
			if(objarr[14] != null)
				searchBean.setPregisterUser(objarr[14].toString());
			if(objarr[15] != null)
				searchBean.setPregisterDate((Date) objarr[15]);
			if(objarr[16] != null)
				searchBean.setPlastModifyuser(objarr[16].toString());
			if(objarr[17] != null)
				searchBean.setPlastModifyDate((Date) objarr[17]);
			if(objarr[18] != null)
				searchBean.setIdUserSessionPk(Long.parseLong(objarr[18].toString()));
			lstAuditSearch.add(searchBean);
		}
		return lstAuditSearch;
	}

	/**
	 * init.
	 */
	@PostConstruct
	public void init() {
		try{
		if (!FacesContext.getCurrentInstance().isPostback()) {

			beginConversation();
			setInitialDate(CommonsUtilities.currentDate());
			setFinalDate(CommonsUtilities.currentDate());
			selectedStateConnection = -1;
			
			 lstInstitutionTypeForFilter = new ArrayList<Parameter>();
			 lstInstitutionTypeForFilterTemp = userServiceFacade.listClientTypeByDefinitionServiceFacade(ParameterTableType.INSITUTIONS_STATE.getCode());
			 lstClientTypeValues = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.USER_TYPES.getCode());
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * it gives current locale language code.
	 *
	 * @return current language code integer type
	 */
	private Integer getCurrentLangCode() {
		Integer currentLangCode = null;
		Locale locale = FacesContext.getCurrentInstance().getViewRoot()
				.getLocale();
		Set<Entry<Integer, LanguageType>> setvalue = LanguageType.lookup
				.entrySet();
		String strlocale = locale.toString();
		if (strlocale.contains("_")) {
			strlocale = strlocale.substring(0, strlocale.indexOf("_"));
		}
		for (Entry<Integer, LanguageType> entry : setvalue) {
			// current language code
			if (((LanguageType) entry.getValue()).getValue().equals(strlocale)) {
				currentLangCode = ((LanguageType) entry.getValue()).getCode();
			}
		}

		return currentLangCode;
	}

	/**
	 * It gives user activities details.
	 */
	public void listUserActivities() {
		try{
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		monitoringUserAuditFilterBean = new MonitoringUserAuditFilterBean();
		monitoringUserAuditFilterBean.setLanguage(getCurrentLangCode());
		if(userPk != null){
			monitoringUserAuditFilterBean.setIduserpk(userPk);
		}
		monitoringUserAuditFilterBean.setInitialDate(sdf.format(initialDate));
		monitoringUserAuditFilterBean.setFinalDate(sdf.format(finalDate));
		List<MonUserAuditSearchFilterBean> lstActivitiesSearch = monitoringUserAuditFacade
				.serachUserConsultActiviesFacade(monitoringUserAuditFilterBean);
		lstUserSessions = new GenericDataModel<MonUserAuditSearchFilterBean>(
				lstActivitiesSearch);
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * it clears user consult activities details.
	 */
	public void clearLstUserSessions() {
		lstUserSessions = null;
		setInitialDate(CommonsUtilities.currentDate());
		setFinalDate(CommonsUtilities.currentDate());
	}

	/**
	 * It gives user data as user details and as well as user profile details.
	 *
	 * @param userAux            MonUserAuditSearchFilterBean type
	 * @param route            long type
	 * @return return delegate page as String type
	 */
	public String assignUser(MonUserAuditSearchFilterBean userAux, long route) {
		user = userAux;
		userPk = userAux.getPidUserPk();
		String pageDetail;
		log.info("user.getIdUserPk(): " + user.getPidUserPk());
		log.info("userTypeSelected: " + userTypeSelected);
		restrictMac = false;

		if (route == 1L) {
			loadProfileData();
			pageDetail = "userDetailsView";
		} else {
			userActivitiesDetails();
			pageDetail = "userActivitiesView";
		}
		clearLstUserSessions();
		return pageDetail;
	}

	/**
	 * It clears the window data.
	 *
	 * @param event            ActionEvent type
	 */
	public void clear(ActionEvent event) {
		try {
			cleanUsersInfo();
			selectedStateConnection = null;
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * It clears the window data.
	 */
	public void cleanUsersInfo() {
		mainInstitution = false;
		monitoringUserAuditFilterBean = new MonitoringUserAuditFilterBean();
		userTypeSelected = null;
		entitySelected = null;
		estadoSelected = null;
		entityTypeSelected = null;
		lstUsersResult = null;
		codigoUser = null;
		lstInstitutionTypes = null;
	}

	/**
	 * It gives user consult activities page details.
	 */
	public void userActivitiesDetails() {
		try{
		Integer langCode = getCurrentLangCode();
		List<Object[]> data = monitoringUserAuditFacade.userActivitiesFacade(user.getPidUserPk(), langCode, user.getIdUserSessionPk());
		for (Object[] objarr : data) {
			// user = new MonUserAuditSearchFilterBean();
			user.setSessionTime(getTime(objarr[0]));
			user.setSystemName(objarr[1].toString());
			user.setCatalogName(objarr[2].toString());
		}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * It gives h:mm a format time.
	 *
	 * @param time            object type
	 * @return string type
	 */
	private String getTime(Object time) {
		Date userDate = (Date) time;
		SimpleDateFormat sdf = new SimpleDateFormat(HOURSTIMEFORMAT);
		return sdf.format(userDate);
	}


	/**
	 * It gives user type from UserAccountType
	 * 
	 * @param accType
	 *            object type
	 * @return String type
	 */
	private String getUserAccType(Object accType) {
		String userAccType = null;
		Set<Entry<Integer, UserAccountType>> mapUserAcc = UserAccountType.lookup
				.entrySet();

		for (Entry<Integer, UserAccountType> entry : mapUserAcc) {
			if (((UserAccountType) entry.getValue()).getCode() == Integer
					.parseInt(accType.toString())) {
				userAccType = ((UserAccountType) entry.getValue()).getValue();
			}
		}
		return userAccType;

	}

	/**
	 * It gives institute type from InstitutionType.
	 *
	 * @param instType the inst type
	 * @return the institute type
	 */
	private String getInstituteType(Object instType) {
		String instituteType = null;
		Set<Entry<Integer, InstitutionType>> mapInstType = InstitutionType.lookup
				.entrySet();

		for (Entry<Integer, InstitutionType> entry : mapInstType) {
			if (((InstitutionType) entry.getValue()).getCode().intValue() == Integer
					.parseInt(instType.toString())) {
				instituteType = ((InstitutionType) entry.getValue()).getValue();
			}
		}
		return instituteType;

	}

	
	/**
	 * It gives user account state type of the corresponding integer value.
	 *
	 * @param obj the obj
	 * @return the user acc state
	 */
	private String getUserAccState(Object obj) {
		String accStateType = null;
		Set<Entry<Integer, UserAccountStateType>> mapAccStateType = UserAccountStateType.lookup
				.entrySet();

		for (Entry<Integer, UserAccountStateType> entry : mapAccStateType) {
			if (((UserAccountStateType) entry.getValue()).getCode() == Integer
					.parseInt(obj.toString())) {
				accStateType = ((UserAccountStateType) entry.getValue())
						.getValue();
			}
		}
		return accStateType;

	}

	/**
	 * Load tree assigned profiles.
	 * 
	 * This method load only the system profiles assigned to the selected user
	 */
	public void loadTreeAssignedProfiles() {
		try {
			SystemProfileFilter psf = new SystemProfileFilter();
			psf.setProfileStateBaseQuery(SystemProfileStateType.CONFIRMED
					.getCode());

			List<System> lstSystems = systemServiceFacade
					.searchSystemByStateServiceFacade(SystemStateType.CONFIRMED
							.getCode());
			List<SystemProfile> lstSystemProfile = systemServiceFacade
					.searchProfilesByFiltersServiceFacade(psf);

			List<System> lstSystemAux = new ArrayList<System>();
			List<SystemProfile> lstSystemProfileAux = new ArrayList<SystemProfile>();

			// fill the system profile list from user profile list
			for (UserProfile userProfile : lstProfilesAssignedToUser) {
				for (SystemProfile sp : lstSystemProfile) {
					if (userProfile.getSystemProfile().getIdSystemProfilePk()
							.intValue() == sp.getIdSystemProfilePk().intValue()) {
						if (!lstSystemProfileAux.contains(sp)) {
							lstSystemProfileAux.add(sp);
							break;
						}
					}
				}
			}
			// fill the systems list assigned to the selected user
			for (SystemProfile sp : lstSystemProfileAux) {
				for (System sis : lstSystems) {
					if (sp.getSystem().getIdSystemPk().intValue() == sis
							.getIdSystemPk().intValue()) {
						if (!lstSystemAux.contains(sis)) {
							lstSystemAux.add(sis);
							break;
						}
					}
				}
			}
			createTreeProfiles(lstSystemAux, lstSystemProfileAux);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Create tree profiles.
	 * 
	 * This method create the profiles tree First create system nodes and then
	 * create the system profile node assigned to the system
	 * 
	 * @param lstSystems
	 *            system list
	 * @param lstSystemProfiles
	 *            system profile list
	 */
	public void createTreeProfiles(List<System> lstSystems,
			List<SystemProfile> lstSystemProfiles) {
		treeProfilesRoot = new DefaultTreeNode("Root", null);

		TreeNode systemsNode;
		NodeBean node;

		for (System system : lstSystems) {
			node = new NodeBean();
			node.setCode(system.getIdSystemPk());
			node.setName(system.getName());
			node.setLevel(1);
			systemsNode = new DefaultTreeNode(node, treeProfilesRoot);
			createNodeProfileBySystem(system.getIdSystemPk(),
					lstSystemProfiles, systemsNode, 2);
		}

	}

	/**
	 * Create node profile by system. This method create the system profile node
	 * according with a system
	 * 
	 * @param idSystem
	 *            the system id of the system to be loaded the system profiles
	 * @param lstSystemProfiles
	 *            this list contains the system profiles
	 * @param parentNode
	 *            the node of the system to be loaded the system profiles
	 * @param level
	 *            the level of the node
	 */
	public void createNodeProfileBySystem(int idSystem,
			List<SystemProfile> lstSystemProfiles, TreeNode parentNode,
			int level) {

		TreeNode profileNode;
		NodeBean node;

		for (SystemProfile sisProf : lstSystemProfiles) {
			if (sisProf.getSystem().getIdSystemPk().intValue() == idSystem) {
				node = new NodeBean();
				node.setCode(sisProf.getIdSystemProfilePk());
				node.setName(sisProf.getName());
				node.setLevel(level);
				profileNode = new DefaultTreeNode(node, parentNode);

				if (isModify()
						&& assignedProfile(sisProf.getIdSystemProfilePk()
								.intValue())) {
					profileNode.setSelected(true);
					profileNode.getParent().setExpanded(true);
					profileNode.getParent().setSelected(true);

				} else if (isDetail()) {
					profileNode.getParent().setExpanded(true);
				}
			}
		}
	}

	/**
	 * Assigned profile. This method checks if the system profile is assigned to
	 * the selected user
	 * 
	 * @param idProfile
	 *            the id profile
	 * @return true, if successful
	 */
	public boolean assignedProfile(int idProfile) {
		for (UserProfile perAsi : lstProfilesAssignedToUser) {
			if (idProfile == perAsi.getSystemProfile().getIdSystemProfilePk()
					.intValue()) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Closesession.
	 */
	@LoggerAuditWeb
	public void closesession(){
		try{
			//To show alert in client window
			//PushContext pushContext = PushContextFactory.getDefault().getPushContext();
			//integration with notifications
			BrowserWindow browser = new BrowserWindow();
			browser.setNotificationType(NotificationType.KILLSESSION.getCode());
			//TODO change hardcode message
			browser.setSubject("Expire Session");
			browser.setMessage(PropertiesConstants.SESSION_EXPIRED);
			//pushContext.push(PropertiesConstants.PUSH_MSG+"/"+user.getPloginUser().trim(),browser);
			
			//To close the session of selected user
			UserSession userSession = new UserSession();
			userSession.setIdUserSessionPk(user.getIdUserSessionPk());
			userServiceFacade.closeSessionServiceFacade(userSession);
			
			cleanUsersInfo();
		}catch(Exception e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Resetpassword.
	 */
	@LoggerAuditWeb
	public void resetpassword(){
		try{
			String password = SecurityUtilities.generatePassword().substring(0, ldapUtilities.getPwdMinLength());
			UserAccount userAccount=new UserAccount();
			userAccount.setIdUserPk(user.getPidUserPk());
			userAccount.setLoginUser(user.getPloginUser());
			userAccount.setFullName(user.getPfullName());
			userAccount.setIdEmail(user.getPidEmail());
			userAccount.setChangePassword( BooleanType.YES.getCode() );
			userServiceFacade.updateChangePasswordServiceFacade(userAccount,password);
		}catch(Exception e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Get list show institution type detail.
	 * 
	 * This method returns a list of the types of institutions assigned to the
	 * user
	 * 
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	public List<String> getListShowInstitutionTypeDetail() throws Exception {
		List<String> listaResult = new ArrayList<String>();

		for (InstitutionTypeDetail insTypeDet : listInstitutionTypeDetailAssignedToUser) {
			for (InstitutionType detTipoInsType : lstInstitutionType) {

				if (insTypeDet.getIdInstitutionTypeFk().intValue() == detTipoInsType
						.getCode()) {
					listaResult.add(detTipoInsType.getValue());
					break;
				}
			}
		}

		return listaResult;
	}

	/**
	 * Gets the lst show institution type assigned to user.
	 *
	 * @return the lstShowInstitutionTypeAssignedToUser
	 */
	public List<String> getLstShowInstitutionTypeAssignedToUser() {
		return lstShowInstitutionTypeAssignedToUser;
	}

	/**
	 * Sets the lst show institution type assigned to user.
	 *
	 * @param lstShowInstitutionTypeAssignedToUser            the lstShowInstitutionTypeAssignedToUser to set
	 */
	public void setLstShowInstitutionTypeAssignedToUser(
			List<String> lstShowInstitutionTypeAssignedToUser) {
		this.lstShowInstitutionTypeAssignedToUser = lstShowInstitutionTypeAssignedToUser;
	}

	/**
	 * It loads user profile data.
	 */
	private void loadProfileData() {
		try {
			setOperationType(ViewOperationsType.DETAIL.getCode());

			userAccountBean = new UserAccount();
			listInstitutionTypeDetailAssignedToUser = userServiceFacade
					.searchInstitutionTypeByUserServiceFacade(
							user.getPidUserPk(),
							InstitutionTypeDetailStateType.REGISTERED.getCode());

			UserProfile userProfile = new UserProfile();
			userProfile.setUserAccount(userAccountBean);
			userProfile.setState(UserProfileStateType.REGISTERED.getCode());
			lstProfilesAssignedToUser = monitoringUserAuditFacade
					.searchRegisteredProfilesByUserServiceFacade(userProfile,
							user.getPidUserPk());

			loadTreeAssignedProfiles();

			// show the institution types list by user type
			if (userTypeSelected.intValue() == UserAccountType.INTERNAL
					.getCode().intValue()) {
				lstInstitutionType = InstitutionType.listInternalInstitutions;
			} else {
				lstInstitutionType = InstitutionType.listExternalInstitutions;
			}

			lstShowInstitutionTypeAssignedToUser = getListShowInstitutionTypeDetail();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * It checks user exist or not.
	 *
	 * @return false if user exist
	 */
	public boolean inValidUser() {
		try{
		if (Validations.validateIsNotNullAndNotEmpty(codigoUser)) {
			validUserFalg = monitoringUserAuditFacade.inValidUserFacade(codigoUser.toUpperCase());
			if (validUserFalg) {
				JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, 
						PropertiesConstants.CODE_USER_INVALID, null);
				JSFUtilities.showEndTransactionSamePageDialog();
				codigoUser = null;
			}

		}else{
			codigoUser = null;
			validUserFalg = false;
		}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return validUserFalg;
	}
	
	/**
	 * Render variables condition.
	 */
	public void selectedValeus(){
		if(entityTypeSelected.equals(InstitutionType.ISSUER.getCode()) 
			|| entityTypeSelected.equals(InstitutionType.PARTICIPANT.getCode()) 
			|| entityTypeSelected.equals(InstitutionType.AFP.getCode())){
			//showInstitution = true;
		}else{
			//showInstitution = false;
		}
	}
	
	/**
	 * Gets the max date.
	 *
	 * @return the max date
	 */
	public Date getMaxDate() {
		return CommonsUtilities.currentDate();
	}

	/**
	 * Gets the user session state type.
	 *
	 * @return the userSessionStateType
	 */
	public List getUserSessionStateType() {
		return UserSessionStateType.list;
	}

	/**
	 * Sets the user session state type.
	 *
	 * @param userSessionStateType            the userSessionStateType to set
	 */
	public void setUserSessionStateType(List userSessionStateType) {
		this.userSessionStateType = userSessionStateType;
	}

	/**
	 * Gets the selected state connection.
	 *
	 * @return the selectedStateConnection
	 */
	public Integer getSelectedStateConnection() {
		return selectedStateConnection;
	}

	/**
	 * Sets the selected state connection.
	 *
	 * @param selectedStateConnection            the selectedStateConnection to set
	 */
	public void setSelectedStateConnection(Integer selectedStateConnection) {
		this.selectedStateConnection = selectedStateConnection;
	}

	/**
	 * Gets the lst institution types.
	 *
	 * @return the lstInstitutionTypes
	 */
	public List<InstitutionType> getLstInstitutionTypes() {
		return lstInstitutionTypes;
	}

	/**
	 * Sets the lst institution types.
	 *
	 * @param lstInstitutionTypes the lstInstitutionTypes to set
	 */
	public void setLstInstitutionTypes(List<InstitutionType> lstInstitutionTypes) {
		this.lstInstitutionTypes = lstInstitutionTypes;
	}

	/**
	 * Gets the lst institutions security for filter.
	 *
	 * @return the lstInstitutionsSecurityForFilter
	 */
	public List<InstitutionsSecurity> getLstInstitutionsSecurityForFilter() {
		return lstInstitutionsSecurityForFilter;
	}

	/**
	 * Sets the lst institutions security for filter.
	 *
	 * @param lstInstitutionsSecurityForFilter the lstInstitutionsSecurityForFilter to set
	 */
	public void setLstInstitutionsSecurityForFilter(
			List<InstitutionsSecurity> lstInstitutionsSecurityForFilter) {
		this.lstInstitutionsSecurityForFilter = lstInstitutionsSecurityForFilter;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initialDate
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the initialDate to set
	 */
	public void setInitialDate(Date initialDate) {
		Date today = CommonsUtilities.currentDate();
		if (initialDate != null && today.compareTo(initialDate) >= 0) {
			this.initialDate = initialDate;
			if (finalDate != null && initialDate.after(finalDate)) {
				finalDate = null;
			}
		} else {
			this.initialDate = null;
		}
	}

	/**
	 * Gets the final date.
	 *
	 * @return the finalDate
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the finalDate to set
	 */
	public void setFinalDate(Date finalDate) {
		Date today = CommonsUtilities.currentDate();
		if (finalDate != null && today.compareTo(finalDate) >= 0
				&& (initialDate == null || initialDate.compareTo(finalDate) <= 0)) {
			this.finalDate = finalDate;
		} else {
			this.finalDate = null;
		}
	}
	
	/**
	 * Cleanup.
	 */
	@PreDestroy
	void cleanup()
	{
		endConversation();
	}
	
	/**
	 * Gets the lst institution type for filter.
	 *
	 * @return the lstInstitutionTypeForFilter
	 */
	public List<Parameter> getLstInstitutionTypeForFilter() {
		return lstInstitutionTypeForFilter;
	}

	/**
	 * Sets the lst institution type for filter.
	 *
	 * @param lstInstitutionTypeForFilter the lstInstitutionTypeForFilter to set
	 */
	public void setLstInstitutionTypeForFilter(
			List<Parameter> lstInstitutionTypeForFilter) {
		this.lstInstitutionTypeForFilter = lstInstitutionTypeForFilter;
	}

	/**
	 * Gets the lst institution type for filter temp.
	 *
	 * @return the lstInstitutionTypeForFilterTemp
	 */
	public List<Parameter> getLstInstitutionTypeForFilterTemp() {
		return lstInstitutionTypeForFilterTemp;
	}

	/**
	 * Sets the lst institution type for filter temp.
	 *
	 * @param lstInstitutionTypeForFilterTemp the lstInstitutionTypeForFilterTemp to set
	 */
	public void setLstInstitutionTypeForFilterTemp(
			List<Parameter> lstInstitutionTypeForFilterTemp) {
		this.lstInstitutionTypeForFilterTemp = lstInstitutionTypeForFilterTemp;
	}
	
	/**
	 * Gets the lst client type values.
	 *
	 * @return the lstClientTypeValues
	 */
	public List<Parameter> getLstClientTypeValues() {
		return lstClientTypeValues;
	}

	/**
	 * Sets the lst client type values.
	 *
	 * @param lstClientTypeValues the lstClientTypeValues to set
	 */
	public void setLstClientTypeValues(List<Parameter> lstClientTypeValues) {
		this.lstClientTypeValues = lstClientTypeValues;
	}
	
	/**
	 * Checks if is main institution.
	 *
	 * @return true, if is main institution
	 */
	public boolean isMainInstitution() {
		return mainInstitution;
	}


	/**
	 * Sets the main institution.
	 *
	 * @param mainInstitution the new main institution
	 */
	public void setMainInstitution(boolean mainInstitution) {
		this.mainInstitution = mainInstitution;
	}


	/**
	 * Gets the option type.
	 *
	 * @return the optionType
	 */
	public Integer getOptionType() {
		return optionType;
	}

	/**
	 * Sets the option type.
	 *
	 * @param optionType            the optionType to set
	 */
	public void setOptionType(Integer optionType) {
		this.optionType = optionType;
	}

	/**
	 * Gets the user type selected.
	 *
	 * @return the userTypeSelected
	 */
	public Integer getUserTypeSelected() {
		return userTypeSelected;
	}

	/**
	 * Sets the user type selected.
	 *
	 * @param userTypeSelected            the userTypeSelected to set
	 */
	public void setUserTypeSelected(Integer userTypeSelected) {
		this.userTypeSelected = userTypeSelected;
	}

	/**
	 * Gets the entity type selected.
	 *
	 * @return the entityTypeSelected
	 */
	public Integer getEntityTypeSelected() {
		return entityTypeSelected;
	}

	/**
	 * Sets the entity type selected.
	 *
	 * @param entityTypeSelected            the entityTypeSelected to set
	 */
	public void setEntityTypeSelected(Integer entityTypeSelected) {
		this.entityTypeSelected = entityTypeSelected;
	}

	/**
	 * Gets the entity selected.
	 *
	 * @return the entitySelected
	 */
	public Integer getEntitySelected() {
		return entitySelected;
	}

	/**
	 * Sets the entity selected.
	 *
	 * @param entitySelected            the entitySelected to set
	 */
	public void setEntitySelected(Integer entitySelected) {
		this.entitySelected = entitySelected;
	}


	/**
	 * Gets the lst users.
	 *
	 * @return the lstUsers
	 */
	public List<String> getLstUsers() {
		return lstUsers;
	}

	/**
	 * Sets the lst users.
	 *
	 * @param lstUsers            the lstUsers to set
	 */
	public void setLstUsers(List<String> lstUsers) {
		this.lstUsers = lstUsers;
	}

	/**
	 * Gets the user.
	 *
	 * @return the user
	 */
	public MonUserAuditSearchFilterBean getUser() {
		return user;
	}

	/**
	 * Sets the user.
	 *
	 * @param user            the user to set
	 */
	public void setUser(MonUserAuditSearchFilterBean user) {
		this.user = user;
	}

	/**
	 * Checks if is restrict mac.
	 *
	 * @return the restrictMac
	 */
	public boolean isRestrictMac() {
		return restrictMac;
	}

	/**
	 * Sets the restrict mac.
	 *
	 * @param restrictMac            the restrictMac to set
	 */
	public void setRestrictMac(boolean restrictMac) {
		this.restrictMac = restrictMac;
	}

	/**
	 * Gets the tree profiles root.
	 *
	 * @return the treeProfilesRoot
	 */
	public TreeNode getTreeProfilesRoot() {
		return treeProfilesRoot;
	}

	/**
	 * Sets the tree profiles root.
	 *
	 * @param treeProfilesRoot            the treeProfilesRoot to set
	 */
	public void setTreeProfilesRoot(TreeNode treeProfilesRoot) {
		this.treeProfilesRoot = treeProfilesRoot;
	}



	/**
	 * Gets the monitoring user audit filter bean.
	 *
	 * @return the monitoringUserAuditFilterBean
	 */
	public MonitoringUserAuditFilterBean getMonitoringUserAuditFilterBean() {
		return monitoringUserAuditFilterBean;
	}

	/**
	 * Sets the monitoring user audit filter bean.
	 *
	 * @param monitoringUserAuditFilterBean            the monitoringUserAuditFilterBean to set
	 */
	public void setMonitoringUserAuditFilterBean(
			MonitoringUserAuditFilterBean monitoringUserAuditFilterBean) {
		this.monitoringUserAuditFilterBean = monitoringUserAuditFilterBean;
	}

	/**
	 * Gets the user monit audit bean.
	 *
	 * @return the userMonitAuditBean
	 */
	public UserAccount getUserMonitAuditBean() {
		return userMonitAuditBean;
	}

	/**
	 * Sets the user monit audit bean.
	 *
	 * @param userMonitAuditBean            the userMonitAuditBean to set
	 */
	public void setUserMonitAuditBean(UserAccount userMonitAuditBean) {
		this.userMonitAuditBean = userMonitAuditBean;
	}

	/**
	 * Gets the lst users result.
	 *
	 * @return the lstUsersResult
	 */
	public GenericDataModel<MonUserAuditSearchFilterBean> getLstUsersResult() {
		return lstUsersResult;
	}

	/**
	 * Sets the lst users result.
	 *
	 * @param lstUsersResult            the lstUsersResult to set
	 */
	public void setLstUsersResult(
			GenericDataModel<MonUserAuditSearchFilterBean> lstUsersResult) {
		this.lstUsersResult = lstUsersResult;
	}

	/**
	 * Gets the lst user sessions.
	 *
	 * @return the lstUserSessions
	 */
	public GenericDataModel<MonUserAuditSearchFilterBean> getLstUserSessions() {
		return lstUserSessions;
	}

	/**
	 * Sets the lst user sessions.
	 *
	 * @param lstUserSessions            the lstUserSessions to set
	 */
	public void setLstUserSessions(
			GenericDataModel<MonUserAuditSearchFilterBean> lstUserSessions) {
		this.lstUserSessions = lstUserSessions;
	}

	/**
	 * Gets the monitoring user audit facade.
	 *
	 * @return the monitoringUserAuditFacade
	 */
	public MonitoringUserAuditFacade getMonitoringUserAuditFacade() {
		return monitoringUserAuditFacade;
	}

	/**
	 * Sets the monitoring user audit facade.
	 *
	 * @param monitoringUserAuditFacade            the monitoringUserAuditFacade to set
	 */
	public void setMonitoringUserAuditFacade(
			MonitoringUserAuditFacade monitoringUserAuditFacade) {
		this.monitoringUserAuditFacade = monitoringUserAuditFacade;
	}

	/**
	 * Gets the lst estado.
	 *
	 * @return the lstEstado
	 */
	public List<Parameter> getLstEstado() {
		List<Parameter> lstStates = new ArrayList<Parameter>();
		List<Parameter> lstStatesTemp = new ArrayList<Parameter>();
		try{
		lstStates = userServiceFacade.listClientTypeByDefinitionServiceFacade(ParameterTableType.USER_STATES.getCode());
		lstStatesTemp.addAll(lstStates);
		for(Parameter param:lstStatesTemp){
			if(param.getName().equals(UserAccountStateType.REGISTERED.getValue())||param.getName().equals(UserAccountStateType.REJECTED.getValue())){
				lstStates.remove(param);
			}
		}
//		lstStates.add(UserAccountStateType.CONFIRMED);
//		lstStates.add(UserAccountStateType.BLOCKED);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return lstStates;
	}

	/**
	 * Sets the lst estado.
	 *
	 * @param lstEstado            the lstEstado to set
	 */
	public void setLstEstado(List<UserAccountStateType> lstEstado) {
		this.lstEstado = lstEstado;
	}

	/**
	 * Gets the codigo user.
	 *
	 * @return the codigoUser
	 */
	public String getCodigoUser() {
		return codigoUser;
	}

	/**
	 * Sets the codigo user.
	 *
	 * @param codigoUser            the codigoUser to set
	 */
	public void setCodigoUser(String codigoUser) {
		this.codigoUser = codigoUser;
	}

	/**
	 * Gets the estado selected.
	 *
	 * @return the estadoSelected
	 */
	public Integer getEstadoSelected() {
		return estadoSelected;
	}

	/**
	 * Sets the estado selected.
	 *
	 * @param estadoSelected            the estadoSelected to set
	 */
	public void setEstadoSelected(Integer estadoSelected) {
		this.estadoSelected = estadoSelected;
	}

	/**
	 * Checks if is valid user falg.
	 *
	 * @return the validUserFalg
	 */
	public boolean isValidUserFalg() {
		return validUserFalg;
	}

	/**
	 * Sets the valid user falg.
	 *
	 * @param validUserFalg            the validUserFalg to set
	 */
	public void setValidUserFalg(boolean validUserFalg) {
		this.validUserFalg = validUserFalg;
	}
	
	/**
	 * Checks if is in user audit.
	 *
	 * @return the inUserAudit
	 */
	public boolean isInUserAudit() {
		return inUserAudit;
	}

	/**
	 * Sets the in user audit.
	 *
	 * @param inUserAudit            the inUserAudit to set
	 */
	public void setInUserAudit(boolean inUserAudit) {
		this.inUserAudit = true;
	}

	
}
