package com.pradera.security.audit.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.security.model.type.UserAccountStateType;
import com.pradera.commons.security.model.type.UserSessionStateType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.audit.view.filters.MonitoringUserAuditFilterBean;
import com.pradera.security.model.SystemProfile;
import com.pradera.security.model.UserProfile;
import com.pradera.security.model.type.OptionStateType;
import com.pradera.security.model.type.UserAccountType;
import com.pradera.security.systemmgmt.service.CrudSecurityServiceBean;

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class MonitoringUserAuditService
 * 
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date :
 * @version 1.1
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class MonitoringUserAuditService extends CrudSecurityServiceBean {
	/**
	 * added default serail uid
	 */
	private static final long serialVersionUID = 1L;
	/** The Constant logger. */
	private static final Logger log = LoggerFactory.getLogger(MonitoringUserAuditService.class);

	/**
	 * It gives user activities details
	 * 
	 * @param userid
	 *            Integer type
	 * @param langCode
	 *            Integer type
	 * @return List<Object[]> type
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> userActivitiesService(Integer userid,
			Integer langCode, Long idUserSessionPk)throws ServiceException {
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append("select us.INITIAL_DATE, s.NAME, cl.CATALOGUE_NAME ");
		sbQuery.append("from USER_SESSION us, USER_AUDIT_TRACKING uat, SYSTEM s, SYSTEM_OPTION sp, ");
		sbQuery.append("CATALOGUE_LANGUAGE cl where sp.ID_SYSTEM_FK = s.ID_SYSTEM_PK ");
		sbQuery.append("and sp.URL_OPTION like '%'||uat.URL||'%' ");
		sbQuery.append("and sp.ID_SYSTEM_OPTION_PK = cl.ID_SYSTEM_OPTION_FK ");
		sbQuery.append(" and cl.LANGUAGE = " + langCode.intValue());
		sbQuery.append(" and us.ID_USER_FK = " + userid.intValue());
		sbQuery.append(" and uat.ID_USER_AUDIT_PK = (select max(uaa.ID_USER_AUDIT_PK) ");
		sbQuery.append(" from USER_AUDIT_TRACKING uaa , SYSTEM_OPTION spp where ");
		sbQuery.append(" uaa.ID_USER_SESSION_FK = us.ID_USER_SESSION_PK  ");
		sbQuery.append("and spp.URL_OPTION like '%'||uaa.URL||'%' )");
		sbQuery.append(" and us.ID_USER_SESSION_PK =" + idUserSessionPk);
		List<Object[]> userActivitiesData = findByNativeQuery(sbQuery
				.toString());
		return userActivitiesData;
	}

	/**
	 * It gives user consultant details
	 * 
	 * @param monitoringUserAudFilter
	 *            MonitoringUserAuditFilterBean type
	 * @return List<Object[]> type
	 *  @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> serachUserConsultActiviesService(
			MonitoringUserAuditFilterBean monitoringUserAudFilter) throws ServiceException{
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append(" select");
		sbQuery.append(" us.INITIAL_DATE as start_date,");
		sbQuery.append(" us.INITIAL_DATE as start_date_second,");
		sbQuery.append(" us.FINAL_DATE,");
		sbQuery.append(" s.NAME,");
		sbQuery.append(" ( select cl2.CATALOGUE_NAME");
		sbQuery.append(" FROM CATALOGUE_LANGUAGE cl2"); 
		sbQuery.append(" where cl2.LANGUAGE = "+monitoringUserAudFilter.getLanguage().intValue()+" and");
		sbQuery.append(" so.ID_OPTION_FATHER = cl2.ID_SYSTEM_OPTION_FK ) as parentcatalog,");
		sbQuery.append(" cl.CATALOGUE_NAME as childcatalog,");
		sbQuery.append(" us.REGISTRY_IP,");
		sbQuery.append(" uat.TIME_ACCESS");
		sbQuery.append(" from  USER_SESSION us,");
		sbQuery.append(" USER_AUDIT_TRACKING uat,");
		sbQuery.append(" SYSTEM s,");
		sbQuery.append(" SYSTEM_OPTION so,");
		sbQuery.append(" CATALOGUE_LANGUAGE cl");
		sbQuery.append(" where us.ID_USER_SESSION_PK = uat.ID_USER_SESSION_FK and");
		sbQuery.append(" so.ID_SYSTEM_FK = s.ID_SYSTEM_PK and");
		sbQuery.append(" (( uat.URL NOT LIKE '%loadReport%' AND   '/'||uat.URL like '%'||so.URL_OPTION||'%' ) ");
		sbQuery.append(" 	OR ( uat.URL LIKE '%loadReport%' AND   so.URL_OPTION = '/'||uat.URL||'' ))");
		sbQuery.append(" and");
		sbQuery.append(" cl.ID_SYSTEM_OPTION_FK=so.ID_SYSTEM_OPTION_PK and");
		sbQuery.append(" cl.LANGUAGE = "+monitoringUserAudFilter.getLanguage().intValue()+" and");
		sbQuery.append(" us.ID_USER_FK = "+ monitoringUserAudFilter.getIduserpk().intValue()+" and");
		sbQuery.append(" us.INITIAL_DATE >= :stadate and");
		sbQuery.append(" us.INITIAL_DATE < :findate and");
		sbQuery.append(" SO.STATE = " + OptionStateType.CONFIRMED.getCode().toString());
		sbQuery.append(" order by uat.TIME_ACCESS desc ");		
		/*sbQuery.append(" select us.INITIAL_DATE as start_date, ");
		sbQuery.append(" us.INITIAL_DATE as start_date_second, ");
		sbQuery.append(" us.FINAL_DATE, ");
		sbQuery.append(" s.NAME, ");
		sbQuery.append(" cl1.CATALOGUE_NAME as parentcatalog, ");
		sbQuery.append(" cl.CATALOGUE_NAME as childcatalog, ");
		sbQuery.append(" us.LAST_MODIFY_IP, ");
		sbQuery.append(" uat.TIME_ACCESS ");
		sbQuery.append(" from USER_SESSION us, USER_AUDIT_TRACKING uat, SYSTEM s, SYSTEM_OPTION sp, ");
		sbQuery.append(" CATALOGUE_LANGUAGE cl, SYSTEM_OPTION sp1, CATALOGUE_LANGUAGE cl1 ");
		sbQuery.append(" where us.ID_USER_SESSION_PK = uat.ID_USER_SESSION_FK and sp.ID_SYSTEM_FK = s.ID_SYSTEM_PK ");
		sbQuery.append(" and sp.URL_OPTION like '%'||uat.URL||'%' ");
		sbQuery.append(" and sp.ID_SYSTEM_OPTION_PK = cl.ID_SYSTEM_OPTION_FK ");
		sbQuery.append(" and cl.LANGUAGE = " + monitoringUserAudFilter.getLanguage().intValue());
		sbQuery.append(" and sp1.ID_SYSTEM_OPTION_PK = cl1.ID_SYSTEM_OPTION_FK ");
		sbQuery.append(" and cl1.LANGUAGE = " + monitoringUserAudFilter.getLanguage().intValue());
		sbQuery.append(" and sp.ID_OPTION_FATHER = sp1.ID_SYSTEM_OPTION_PK ");
		sbQuery.append(" and us.INITIAL_DATE >= :stadate ");
		sbQuery.append(" AND us.INITIAL_DATE < :findate ");
		sbQuery.append(" and us.ID_USER_FK = "+ monitoringUserAudFilter.getIduserpk().intValue());
		sbQuery.append(" order by uat.TIME_ACCESS desc ");*/
		Query query = em.createNativeQuery(sbQuery.toString());
		
		Date finDate = CommonsUtilities.addDate(CommonsUtilities.convertStringtoDate(monitoringUserAudFilter.getFinalDate()), 1);
		query.setParameter("stadate",CommonsUtilities.convertStringtoDate(monitoringUserAudFilter.getInitialDate()));			
		query.setParameter("findate", finDate);
		return (List<Object[]>)query.getResultList();
	}

	/**
	 * It gives profile details of specific user
	 * 
	 * @param userProfile
	 *            UserProfile type
	 * @param id
	 *            Integer type user id
	 * @return list ofuser profiles List<UserProfile> type
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	public List<UserProfile> searchRegisteredProfilesByUserServiceBean(
			UserProfile userProfile, Integer id) throws ServiceException {
		String strQuery = "Select PU from UserProfile PU where PU.userAccount.idUserPk=:idUserPkPrm and PU.state=:statePrm";
		Query query = em.createQuery(strQuery);
		query.setParameter("idUserPkPrm", id);
		query.setParameter("statePrm", userProfile.getState());
		query.setHint("org.hibernate.cacheable", true);
		List<UserProfile> listUserProfiles=(List<UserProfile>) query.getResultList();
		for (UserProfile usrProfile : listUserProfiles) {
		SystemProfile systemProfile=usrProfile.getSystemProfile();
		systemProfile.getIdSystemProfilePk();
		}
		return listUserProfiles;
	}

	/**
	 * It checks user exist or not
	 * 
	 * @param coduser
	 *            String type
	 * @return count
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	public Long inValidUserService(String coduser) throws ServiceException{
		
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select count(*) from UserAccount ua ");
		sbQuery.append("where ua.loginUser LIKE :loguser");
		parameters.put("loguser", "%"+coduser+"%");
		List<Long> usercount = findByQueryString(sbQuery.toString(), parameters);
		return (Long) usercount.get(0);
		
	}

	/**
	 * To get the user details
	 * @param monitoringUserAudFilter
	 * @return List
	 * @throws ServiceException
	 */
	public List<Object[]> searchAuditUsersServiceBean(
			MonitoringUserAuditFilterBean monitoringUserAudFilter,Boolean connectionState)throws ServiceException {
		
		StringBuffer sbQuery = new StringBuffer();
		
		sbQuery.append("select ");
		sbQuery.append("ua.LOGIN_USER, ");
		sbQuery.append("ua.FULL_NAME, ");
		sbQuery.append("ua.FIRTS_LAST_NAME, ");
		sbQuery.append("ua.SECOND_LAST_NAME, ");
		sbQuery.append("ua.TYPE, ");
		sbQuery.append("ua.INSTITUTION_TYPE_FK, ");
		sbQuery.append("inst.INSTITUTION_NAME, ");
		sbQuery.append("us.INITIAL_DATE last_connection_start, ");
		sbQuery.append("us.FINAL_DATE last_connection_end, ");
		sbQuery.append("us.STATE last_connection_state, ");
		sbQuery.append("ua.STATE, ");
		sbQuery.append("ua.ID_USER_PK, ");
		sbQuery.append("ua.PHONE_NUMBER, ");
		sbQuery.append("ua.ID_EMAIL, ");
		sbQuery.append("ua.REGISTRY_USER, ");
		sbQuery.append("ua.REGISTRY_DATE, ");
		sbQuery.append("ua.LAST_MODIFY_USER, ");
		sbQuery.append("ua.LAST_MODIFY_DATE, ");
		sbQuery.append("us.ID_USER_SESSION_PK ");
		sbQuery.append("from ");
		sbQuery.append("USER_ACCOUNT ua, ");
		sbQuery.append("INSTITUTIONS_SECURITY inst, ");
		sbQuery.append("(select us.ID_USER_SESSION_PK, us.INITIAL_DATE, us.FINAL_DATE, us.STATE, us.ID_USER_FK ");       
				sbQuery.append("from USER_SESSION us where us.ID_USER_SESSION_PK in ");
				sbQuery.append("(select max(us.ID_USER_SESSION_PK) from USER_SESSION us group by us.ID_USER_FK)) us ");
		sbQuery.append("where ");
		sbQuery.append("ua.ID_SECURITY_INSTITUTION_FK = inst.ID_INSTITUTION_SECURITY_PK and ");   
		sbQuery.append("ua.ID_USER_PK = us.ID_USER_FK ");		
		
		if(monitoringUserAudFilter.getStateSelected() != null && !monitoringUserAudFilter.getStateSelected().equals(Integer.valueOf(-1))){
			sbQuery.append("and ua.STATE = :statePrm ");		
		}else{			
			sbQuery.append("and ua.STATE <> :statePrm ");		
		}
		if(monitoringUserAudFilter.getUserTypeId() != null && !monitoringUserAudFilter.getUserTypeId().equals(Integer.valueOf(-1))){
			sbQuery.append("and ua.TYPE = :userTypePrm ");
			
			if(monitoringUserAudFilter.getUserTypeId().equals(UserAccountType.EXTERNAL.getCode())
				&& monitoringUserAudFilter.getEntityTypeSeletected() != null 
				&& !monitoringUserAudFilter.getEntityTypeSeletected().equals(Integer.valueOf(-1))){
				sbQuery.append("and inst.ID_INSTITUTION_TYPE_Fk = :institutionTypePrm ");		
				
				if(monitoringUserAudFilter.getEntitySeletectedPk() != null 
					&& !monitoringUserAudFilter.getEntitySeletectedPk().equals(Integer.valueOf(-1))){
					sbQuery.append("and ua.ID_PARTICIPANT_FK = :participantPrm ");			
				}				
			}						
		}		
		if(monitoringUserAudFilter.getUserCode() != null && !monitoringUserAudFilter.getUserCode().isEmpty()){
			sbQuery.append("and ua.LOGIN_USER like :loginUserPrm ");			
		}
		if(monitoringUserAudFilter.getSelectedStateConnection() != null && !monitoringUserAudFilter.getSelectedStateConnection().equals(Integer.valueOf(-1))){
			sbQuery.append("and us.STATE = :stateConnectionPrm ");			
		}
		sbQuery.append("order by last_connection_state desc,last_connection_start desc ");
		Query query = em.createNativeQuery(sbQuery.toString());
		//Parameters
		if(monitoringUserAudFilter.getUserTypeId() != null && !monitoringUserAudFilter.getUserTypeId().equals(Integer.valueOf(-1))){
			query.setParameter("userTypePrm",monitoringUserAudFilter.getUserTypeId());	
			
			if(monitoringUserAudFilter.getUserTypeId().equals(UserAccountType.EXTERNAL.getCode())
				&& monitoringUserAudFilter.getEntityTypeSeletected() != null 
				&& !monitoringUserAudFilter.getEntityTypeSeletected().equals(Integer.valueOf(-1))){
				query.setParameter("institutionTypePrm",monitoringUserAudFilter.getEntityTypeSeletected());		
				
				if(monitoringUserAudFilter.getEntitySeletectedPk() != null 
					&& !monitoringUserAudFilter.getEntitySeletectedPk().equals(Integer.valueOf(-1))){
					query.setParameter("participantPrm",monitoringUserAudFilter.getEntitySeletectedPk());
				}				
			}	
		}
		if(monitoringUserAudFilter.getUserCode() != null && !monitoringUserAudFilter.getUserCode().isEmpty()){
			query.setParameter("loginUserPrm","%"+monitoringUserAudFilter.getUserCode().toUpperCase()+"%");
		}
		if(monitoringUserAudFilter.getSelectedStateConnection() != null && !monitoringUserAudFilter.getSelectedStateConnection().equals(Integer.valueOf(-1))){
			query.setParameter("stateConnectionPrm",monitoringUserAudFilter.getSelectedStateConnection());
		}
		if(monitoringUserAudFilter.getStateSelected() != null && !monitoringUserAudFilter.getStateSelected().equals(Integer.valueOf(-1))){
			query.setParameter("statePrm",monitoringUserAudFilter.getStateSelected());
		}else{			
			query.setParameter("statePrm",UserAccountStateType.REGISTERED.getCode());
		}
		return (List<Object[]>)query.getResultList();
	}
	/**
	 * Get the last access systems list
	 * @param userID
	 * @param systemID
	 * @return
	 * @throws ServiceException
	 */
	public List<Object[]>  getLastAccessSystemNameServiceBean(Integer userID,Integer systemID) throws ServiceException{
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append(" select us.ID_CURRENT_SYSTEM_FK from USER_AUDIT_TRACKING uat,USER_SESSION us ");
		sbQuery.append(" where uat.ID_USER_SESSION_FK = us.ID_USER_SESSION_PK and ID_CURRENT_SYSTEM_FK=:systemID and uat.ID_USER_AUDIT_PK in ( ");
		sbQuery.append(" select  max(uat.ID_USER_AUDIT_PK) from USER_SESSION us, USER_AUDIT_TRACKING uat ");
		sbQuery.append(" where us.ID_USER_SESSION_PK = uat.ID_USER_SESSION_FK  and us.STATE= :connectedState");
		sbQuery.append("  and us.ID_USER_FK = :userID) ");
		
		Query query = em.createNativeQuery(sbQuery.toString());
		
		query.setParameter("connectedState",UserSessionStateType.CONNECTED.getCode());
		query.setParameter("userID",userID);
		query.setParameter("systemID",systemID);
		return (List<Object[]>)query.getResultList();
	}
}
