package com.pradera.security.audit.report.view.to;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class UserCreationReportFilterTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 09-mar-2015
 */
public class UserCreationReportFilterTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -321100763434509667L;
	
	/** The user name. */
	private String userName;
	
	/** The institution type. */
	private Integer institutionType;
	
	/** The institution. */
	private Integer institution;
	
	/** The user state. */
	private Integer userState;
	
	/** The profile name. */
	private String profileName;
	
	/** The registry user. */
	private String registryUser;
	
	/** The confirmation user. */
	private String confirmationUser;
	
	/** The initial registry. */
	private Date initialRegistry;
	
	/** The final registry. */
	private Date finalRegistry;

	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName the new user name
	 */
	public void setUserName(String userName) {
		if(StringUtils.isNotBlank(userName)){
			userName=userName.toUpperCase();
		}
		this.userName = userName;
	}

	/**
	 * Gets the institution type.
	 *
	 * @return the institution type
	 */
	public Integer getInstitutionType() {
		return institutionType;
	}

	/**
	 * Sets the institution type.
	 *
	 * @param institutionType the new institution type
	 */
	public void setInstitutionType(Integer institutionType) {
		this.institutionType = institutionType;
	}

	/**
	 * Gets the institution.
	 *
	 * @return the institution
	 */
	public Integer getInstitution() {
		return institution;
	}

	/**
	 * Sets the institution.
	 *
	 * @param institution the new institution
	 */
	public void setInstitution(Integer institution) {
		this.institution = institution;
	}

	/**
	 * Gets the user state.
	 *
	 * @return the user state
	 */
	public Integer getUserState() {
		return userState;
	}

	/**
	 * Sets the user state.
	 *
	 * @param userState the new user state
	 */
	public void setUserState(Integer userState) {
		this.userState = userState;
	}

	/**
	 * Gets the profile name.
	 *
	 * @return the profile name
	 */
	public String getProfileName() {
		return profileName;
	}

	/**
	 * Sets the profile name.
	 *
	 * @param profileName the new profile name
	 */
	public void setProfileName(String profileName) {
		if(StringUtils.isNotBlank(profileName)){
			profileName=profileName.toUpperCase();
		}
		this.profileName = profileName;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		if(StringUtils.isNotBlank(registryUser)){
			registryUser=registryUser.toUpperCase();
		}
		this.registryUser = registryUser;
	}

	/**
	 * Gets the confirmation user.
	 *
	 * @return the confirmation user
	 */
	public String getConfirmationUser() {
		return confirmationUser;
	}

	/**
	 * Sets the confirmation user.
	 *
	 * @param confirmationUser the new confirmation user
	 */
	public void setConfirmationUser(String confirmationUser) {
		if(StringUtils.isNotBlank(confirmationUser)){
			confirmationUser=confirmationUser.toUpperCase();
		}
		this.confirmationUser = confirmationUser;
	}

	/**
	 * Gets the initial registry.
	 *
	 * @return the initial registry
	 */
	public Date getInitialRegistry() {
		return initialRegistry;
	}

	/**
	 * Sets the initial registry.
	 *
	 * @param initialRegistry the new initial registry
	 */
	public void setInitialRegistry(Date initialRegistry) {
		this.initialRegistry = initialRegistry;
	}

	/**
	 * Gets the final registry.
	 *
	 * @return the final registry
	 */
	public Date getFinalRegistry() {
		return finalRegistry;
	}

	/**
	 * Sets the final registry.
	 *
	 * @param finalRegistry the new final registry
	 */
	public void setFinalRegistry(Date finalRegistry) {
		this.finalRegistry = finalRegistry;
	}

}
