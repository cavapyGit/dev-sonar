package com.pradera.security.audit.facade;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.apache.log4j.Logger;

import com.pradera.audit.model.AuditTrackFilterBean;
import com.pradera.audit.model.FieldsTableDataBase;
import com.pradera.audit.model.TableDataBase;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.audit.service.AuditsensitiveFieldServiceBean;
import com.pradera.security.model.ActionTable;
import com.pradera.security.model.ActionTableSql;
import com.pradera.security.model.System;
import com.pradera.security.model.type.ActionTableSqlStateType;
import com.pradera.security.model.type.AuditTableSituationType;
import com.pradera.security.model.type.AuditTableStateType;
import com.pradera.security.model.type.StateType;
import com.pradera.security.utils.view.RestServiceNotification;

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class AuditsensitiveFieldBeanFacade
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
@SuppressWarnings({ "unchecked", "unchecked" })
@Stateless(name = "auditsensitiveFieldBeanFacade")
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class AuditsensitiveFieldBeanFacade {
	
	/** the logger. */
	private static final  Logger log = Logger.getLogger(AuditsensitiveFieldBeanFacade.class.getName());
	
	/** The transaction registry. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The auditsensitive field service bean. */
	@EJB
	AuditsensitiveFieldServiceBean auditsensitiveFieldServiceBean;
	
    /** The rest service notification. */
    @Inject
    private RestServiceNotification restServiceNotification;

    private final static Integer max_sql_length = 2000;
    
	/**
	 * used to get the table names by System.
	 *
	 * @param systemName the system name
	 * @return tableList List<TableDataBase>
	 * @throws ServiceException the service exception
	 */
	public List<TableDataBase> getTableList(String systemName)
			throws ServiceException {
		List<TableDataBase> tableList = new ArrayList<TableDataBase>();
		List<String> commonNames = auditsensitiveFieldServiceBean.getTableListServiceBean(systemName);
		Collections.sort(commonNames);
		TableDataBase tableDataBase;
		for (int i = 0; i < commonNames.size(); i++) {
			tableDataBase = new TableDataBase();
			tableDataBase.setIdTablePK(i + 1);
			tableDataBase.setTableName(commonNames.get(i));
			tableList.add(tableDataBase);
		}
		return tableList;
	}

	/**
	 * used to get the user names by System.
	 *
	 * @return userList ArrayList<BeanType>
	 * @throws ServiceException the service exception
	 */
	public List<String> getUsersList() throws ServiceException {
		List<String> commonNames = auditsensitiveFieldServiceBean.getUsersListServiceBean();
		if(commonNames == null){
			commonNames = new ArrayList<String>();
		}
		return commonNames;
	}

	/**
	 * used to get the fields by table.
	 *
	 * @param tableName            String
	 * @param schemaName the schema name
	 * @return columnList ArrayList<FieldsTableDataBase>
	 * @throws ServiceException the service exception
	 */
	public List<FieldsTableDataBase> getColumnList(String tableName,String schemaName)
			throws ServiceException {
		log.info("-------------------------------------------");
		log.info("Selected SchemanName-------"+schemaName);
		List<FieldsTableDataBase> columnList = new ArrayList<FieldsTableDataBase>();
		List<String> commonNames = auditsensitiveFieldServiceBean.getColumnListServiceBean(tableName,schemaName);
		Collections.sort(commonNames);
		FieldsTableDataBase fieldsTableDataBase;
		for (int i = 0; i < commonNames.size(); i++) {
			fieldsTableDataBase = new FieldsTableDataBase();
			fieldsTableDataBase.setIdTablePK(i + 1);
			fieldsTableDataBase.setFieldName(commonNames.get(i));
			if (commonNames.get(i).contains("_FK")
					|| commonNames.get(i).contains("_PK")) {
				fieldsTableDataBase.setConstraint(true);
			} else {
				fieldsTableDataBase.setConstraint(false);
			}
			
			if(commonNames.get(i).contains("_PK")) {
				fieldsTableDataBase.setPrimarykey(true);
			} else {
				fieldsTableDataBase.setPrimarykey(false);
			}
				
			columnList.add(fieldsTableDataBase);
		}
		return columnList;
	}

	/**
	 * used to save and update the ActionTable.
	 *
	 * @param actionTable            ActionTable
	 * @param operation            Integer
	 * @throws ServiceException the service exception
	 */
	public void saveActionTable(ActionTable actionTable, int operation)	throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		
		Long businessProcess = null;
		if (operation == 0){
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
			businessProcess = BusinessProcessType.MASTER_TABLE_REGISTRATION.getCode();
			auditsensitiveFieldServiceBean.saveActionTableServiceBean(actionTable);
		}else if (operation == 1) {
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
			businessProcess = BusinessProcessType.MASTER_TABLE_MODIFICATION.getCode();
			auditsensitiveFieldServiceBean.updateActionTableServiceBean(actionTable);
		}
		
		//Se envia email y notificacion
  		Object[] parameters = {actionTable.getIdActionTablesPk()};
  		restServiceNotification.sendNotification(loggerUser,businessProcess,parameters);   	
	}
	
	/**
	 * Register desactivate trigger rule.
	 *
	 * @param actionTable the action table
	 * @throws ServiceException the service exception
	 */
	public void registerDesactivateTriggerRule(ActionTable actionTable) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeDeactivate());
		actionTable.setSituation(AuditTableSituationType.PENDING_DELETE.getCode());
		auditsensitiveFieldServiceBean.update(actionTable);
	}
	
	/**
	 * Rejected desactivate trigger rule.
	 *
	 * @param actionTable the action table
	 * @throws ServiceException the service exception
	 */
	public void rejectedDesactivateTriggerRule(ActionTable actionTable) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeDeactivate());
		actionTable.setSituation(AuditTableSituationType.ACTIVE.getCode());
		auditsensitiveFieldServiceBean.update(actionTable);
	}
	
	/**
	 * Confirmd desactivate trigger rule.
	 *
	 * @param actionTable the action table
	 * @throws ServiceException the service exception
	 */
	public void confirmdDesactivateTriggerRule(ActionTable actionTable) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeDeactivate());
		actionTable.setState(AuditTableStateType.DISABLED.getCode());
		actionTable.setSituation(AuditTableSituationType.PENDING_EXECUTION.getCode());
		
		//drop triggers
		String sql1 = auditsensitiveFieldServiceBean.executeInsertTriggerServiceBean(actionTable);
		
		Integer order = GeneralConstants.ZERO_VALUE_INTEGER;
		Integer sqlNumber = 2;
		
		for (int i = 0; i < sql1.length(); i += max_sql_length) {
			ActionTableSql sql = new ActionTableSql();
			sql.setActionTable(actionTable);
			sql.setVersion(actionTable.getVersion());
			sql.setState(ActionTableSqlStateType.NOT_EXECUTED.getCode());			
			sql.setOrderSql(order);
			int endIndex = Math.min(i + max_sql_length, sql1.length());
            String substring = sql1.substring(i, endIndex);
            
            sql.setTriggerRule(substring);
            
            order = order + 1;
            sql.setSqlNumber(sqlNumber);
            auditsensitiveFieldServiceBean.create(sql);
		}

		String sql2 = auditsensitiveFieldServiceBean.executeUpdateTriggerServiceBean(actionTable);
		sqlNumber = sqlNumber +1;
		order = GeneralConstants.ZERO_VALUE_INTEGER;
		for (int i = 0; i < sql2.length(); i += max_sql_length) {
			ActionTableSql sql = new ActionTableSql();
			sql.setActionTable(actionTable);
			sql.setVersion(actionTable.getVersion());
			sql.setState(ActionTableSqlStateType.NOT_EXECUTED.getCode());			
			sql.setOrderSql(order);
			int endIndex = Math.min(i + max_sql_length, sql2.length());
            String substring = sql2.substring(i, endIndex);
            
            sql.setTriggerRule(substring);
            
            order = order + 1;
            sql.setSqlNumber(sqlNumber);
            auditsensitiveFieldServiceBean.create(sql);
		}
		
		auditsensitiveFieldServiceBean.update(actionTable);
		
		Long businessProcess = BusinessProcessType.MASTER_TABLE_MODIFICATION.getCode();
  		Object[] parameters = {actionTable.getIdActionTablesPk()};
  		restServiceNotification.sendNotification(loggerUser,businessProcess,parameters);  
	}
	
	/**
	 * Register rules trigger.
	 *
	 * @param actionTable the action table
	 * @param system the system
	 * @param lstColumnsPrimaryKey the lst columns primary key
	 * @throws ServiceException the service exception
	 */
	public void registerRulesTrigger(ActionTable actionTable, System system, List<String> lstColumnsPrimaryKey)
			throws ServiceException {
		LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

		actionTable.setActionTableSqls(new ArrayList<ActionTableSql>());
		
		Integer order = GeneralConstants.ZERO_VALUE_INTEGER;
		Integer numberSQL = GeneralConstants.ZERO_VALUE_INTEGER;
		
		String firstSql = auditsensitiveFieldServiceBean.createAndActivateTriggerInsertAuditServiceBean(system,
				actionTable, lstColumnsPrimaryKey);
		
		for (int i = 0; i < firstSql.length(); i += max_sql_length) {
			ActionTableSql sql = new ActionTableSql();
			sql.setActionTable(actionTable);
			sql.setVersion(actionTable.getVersion());
			sql.setState(ActionTableSqlStateType.NOT_EXECUTED.getCode());			
			sql.setOrderSql(order);
			int endIndex = Math.min(i + max_sql_length, firstSql.length());
            String substring = firstSql.substring(i, endIndex);
            
            sql.setTriggerRule(substring);
            
            order = order + 1;
            sql.setSqlNumber(numberSQL);
            actionTable.getActionTableSqls().add(sql);
		}

		order = GeneralConstants.ZERO_VALUE_INTEGER;
		numberSQL = numberSQL + 1;
		
		String secondSql = auditsensitiveFieldServiceBean.createAndActivateTriggerUpdateAuditServiceBean(system,
				actionTable, lstColumnsPrimaryKey);
		
		for (int i = 0; i < secondSql.length(); i += max_sql_length) {
			ActionTableSql sql = new ActionTableSql();
			sql.setActionTable(actionTable);
			sql.setVersion(actionTable.getVersion());
			sql.setState(ActionTableSqlStateType.NOT_EXECUTED.getCode());
			sql.setOrderSql(order);
			int endIndex = Math.min(i + max_sql_length, secondSql.length());
            String substring = secondSql.substring(i, endIndex);
            
            sql.setTriggerRule(substring);
            
            order = order + 1;
            sql.setSqlNumber(numberSQL);
            actionTable.getActionTableSqls().add(sql);
		}
		
		auditsensitiveFieldServiceBean.saveActionTableServiceBean(actionTable);
		// save triggers disabled
	}
	
	/**
	 * Confirm rulestrigger.
	 *
	 * @param actionTable the action table
	 * @throws ServiceException the service exception
	 */
	public void confirmRulestrigger(ActionTable actionTable) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		ActionTable currentTable = auditsensitiveFieldServiceBean.find(ActionTable.class, actionTable.getIdActionTablesPk());
		currentTable.setState(AuditTableStateType.CONFIRMED.getCode());
		currentTable.setSituation(AuditTableSituationType.PENDING_EXECUTION.getCode());

		//enable trigger
		String enableSql = auditsensitiveFieldServiceBean.enableTriggersRuleInsert(actionTable);
		
		Integer order = GeneralConstants.ZERO_VALUE_INTEGER;
		Integer numberSql = GeneralConstants.ZERO_VALUE_INTEGER;
		
		for (int i = 0; i < enableSql.length(); i += max_sql_length) {
			ActionTableSql sql = new ActionTableSql();
			sql.setActionTable(actionTable);
			sql.setVersion(actionTable.getVersion());
			sql.setState(ActionTableSqlStateType.NOT_EXECUTED.getCode());			
			sql.setOrderSql(order);
			int endIndex = Math.min(i + max_sql_length, enableSql.length());
            String substring = enableSql.substring(i, endIndex);
            
            sql.setTriggerRule(substring);
            
            order = order + 1;
            sql.setSqlNumber(numberSql);
            auditsensitiveFieldServiceBean.create(sql);
		}
		
		String enableSql2 = auditsensitiveFieldServiceBean.enableTriggersRuleUpdate(actionTable);

		order = GeneralConstants.ZERO_VALUE_INTEGER;
		numberSql = numberSql + 1;
		
		for (int i = 0; i < enableSql2.length(); i += max_sql_length) {
			ActionTableSql sql = new ActionTableSql();
			sql.setActionTable(actionTable);
			sql.setVersion(actionTable.getVersion());
			sql.setState(ActionTableSqlStateType.NOT_EXECUTED.getCode());			
			sql.setOrderSql(order);
			int endIndex = Math.min(i + max_sql_length, enableSql2.length());
            String substring = enableSql2.substring(i, endIndex);
            
            sql.setTriggerRule(substring);
            
            order = order + 1;
            sql.setSqlNumber(numberSql);
            auditsensitiveFieldServiceBean.create(sql);
		}
		auditsensitiveFieldServiceBean.update(currentTable);
		//Se envia email y notificacion
		Long businessProcess = BusinessProcessType.MASTER_TABLE_REGISTRATION.getCode();
  		Object[] parameters = {actionTable.getIdActionTablesPk()};
  		restServiceNotification.sendNotification(loggerUser,businessProcess,parameters);   	
	}
	
	/**
	 * Rejected rules trigger.
	 *
	 * @param actionTable the action table
	 * @throws ServiceException the service exception
	 */
	public void rejectedRulesTrigger(ActionTable actionTable) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		ActionTable currentTable = auditsensitiveFieldServiceBean.find(ActionTable.class, actionTable.getIdActionTablesPk());
		currentTable.setState(AuditTableStateType.REJECTED.getCode());
		currentTable.setSituation(AuditTableSituationType.PENDING_EXECUTION.getCode());
				
		//drop triggers
		String sql1 = auditsensitiveFieldServiceBean.executeInsertTriggerServiceBean(actionTable);
		
		Integer order = GeneralConstants.ZERO_VALUE_INTEGER;
		Integer sqlNumber = GeneralConstants.ZERO_VALUE_INTEGER;
		
		for (int i = 0; i < sql1.length(); i += max_sql_length) {
			ActionTableSql sql = new ActionTableSql();
			sql.setActionTable(actionTable);
			sql.setVersion(actionTable.getVersion());
			sql.setState(ActionTableSqlStateType.NOT_EXECUTED.getCode());			
			sql.setOrderSql(order);
			int endIndex = Math.min(i + max_sql_length, sql1.length());
            String substring = sql1.substring(i, endIndex);
            
            sql.setTriggerRule(substring);
            
            order = order + 1;
            sql.setSqlNumber(sqlNumber);
            auditsensitiveFieldServiceBean.create(sql);
		}

		String sql2 = auditsensitiveFieldServiceBean.executeUpdateTriggerServiceBean(actionTable);
		sqlNumber = sqlNumber +1;
		order = GeneralConstants.ZERO_VALUE_INTEGER;
		for (int i = 0; i < sql2.length(); i += max_sql_length) {
			ActionTableSql sql = new ActionTableSql();
			sql.setActionTable(actionTable);
			sql.setVersion(actionTable.getVersion());
			sql.setState(ActionTableSqlStateType.NOT_EXECUTED.getCode());			
			sql.setOrderSql(order);
			int endIndex = Math.min(i + max_sql_length, sql2.length());
            String substring = sql2.substring(i, endIndex);
            
            sql.setTriggerRule(substring);
            
            order = order + 1;
            sql.setSqlNumber(sqlNumber);
            auditsensitiveFieldServiceBean.create(sql);
		}
		
		auditsensitiveFieldServiceBean.update(currentTable);
	}
	
	/**
	 * used to get the ActionTable List by system state.
	 *
	 * @param state            Integer
	 * @return list List<ActionTable>
	 * @throws ServiceException the service exception
	 */
	public List<ActionTable> searchActionTableBySystem(Integer state) throws ServiceException {
		return auditsensitiveFieldServiceBean.searchActionTableBySystemServiceBean(state);
	}

	/**
	 * used to get the DetaiAction Table list.
	 *
	 * @param actionPk            long
	 * @return fieldsTableDataBases List<FieldsTableDataBase>
	 * @throws ServiceException the service exception
	 */
	public List<FieldsTableDataBase> getDetaiActionByActionPk(long actionPk)
			throws ServiceException {
		List<FieldsTableDataBase> fieldsTableDataBases = new ArrayList<FieldsTableDataBase>();
		FieldsTableDataBase fieldsTableDataBase;
		List<Object[]> objectArray = auditsensitiveFieldServiceBean.getDetaiActionByActionPkServiceBean(actionPk);
		for (Object[] object : objectArray) {
			fieldsTableDataBase = new FieldsTableDataBase();
			fieldsTableDataBase.setFieldName(object[0].toString());
			if (fieldsTableDataBase.getFieldName().contains("_PK")
					|| fieldsTableDataBase.getFieldName().contains("_FK"))
				fieldsTableDataBase.setConstraint(true);
			if (Integer.parseInt(object[1].toString()) == 1)
				fieldsTableDataBase.setInsertable(true);
			else
				fieldsTableDataBase.setInsertable(false);
			if (Integer.parseInt(object[2].toString()) == 1)
				fieldsTableDataBase.setUpdatable(true);
			else
				fieldsTableDataBase.setUpdatable(false);
			fieldsTableDataBases.add(fieldsTableDataBase);
		}
		return fieldsTableDataBases;
	}

	/**
	 * used to delete the DetailAction by ActionTablePK.
	 *
	 * @param detailActionFk            long
	 * @return true boolean
	 * @throws ServiceException the service exception
	 */
	public boolean deleteDetaiActionByActionPk(long detailActionFk)
			throws ServiceException {
		return auditsensitiveFieldServiceBean
				.deleteDetaiActionByActionPkServiceBean(detailActionFk);
	}

	/**
	 * Execute insert trigger.
	 *
	 * @param action the action
	 * @throws ServiceException the service exception
	 */
	public void executeInsertTrigger(ActionTable action) throws ServiceException {
		auditsensitiveFieldServiceBean.executeInsertTriggerServiceBean(action);
	}
	
	/**
	 * Execute update trigger.
	 *
	 * @param action the action
	 * @throws ServiceException the service exception
	 */
	public void executeUpdateTrigger(ActionTable action) throws ServiceException {
		auditsensitiveFieldServiceBean.executeUpdateTriggerServiceBean(action);
	}

	/**
	 * used to get the actionTables.
	 *
	 * @param auditTrackFilterBean the audit track filter bean
	 * @return actionTables List<ActionTables
	 * @throws ServiceException the service exception
	 */
	public List<ActionTable> getActionTableFromAuditLog(
			AuditTrackFilterBean auditTrackFilterBean) throws ServiceException {
		List<ActionTable> actionTables = null;
		ActionTable actionTable;
		List<Object[]> objects = auditsensitiveFieldServiceBean
				.getActionTableFromAuditLogServiceBean(auditTrackFilterBean);
		if (objects != null && objects.size() >= 1) {
			actionTables = new ArrayList<ActionTable>();
			for (Object[] var : objects) {
				actionTable = new ActionTable();
				actionTable.setIdActionTablesPk(Integer.parseInt(var[0]
						.toString()));
				if (var[1] != null)
					actionTable.setTableName(var[1].toString());
				if (var[2] != null)
					actionTable.setRuleName(var[2].toString());
				actionTable.setRegistryDate((Date) (var[3]));
				actionTable.setInitialDate((Date) (var[4]));
				actionTable.setEndDate((Date) (var[5]));
				if (Integer.parseInt(var[6].toString()) == StateType.BLOCKED
						.getCode())
					actionTable.setStateType(StateType.BLOCKED.getValue());
				else if (Integer.parseInt(var[6].toString()) == StateType.ANNULLED
						.getCode())
					actionTable.setStateType(StateType.ANNULLED.getValue());
				else if (Integer.parseInt(var[6].toString()) == StateType.CONFIRMED
						.getCode())
					actionTable.setStateType(StateType.CONFIRMED.getValue());
				else if (Integer.parseInt(var[6].toString()) == StateType.REGISTERED
						.getCode())
					actionTable.setStateType(StateType.REGISTERED.getValue());
				else if (Integer.parseInt(var[6].toString()) == StateType.REJECTED
						.getCode())
					actionTable.setStateType(StateType.REJECTED.getValue());
				actionTable.setOldValue(var[7].toString());
				actionTable.setNewValue(var[8].toString());
				actionTable.setLastModifyUser(var[9].toString());
				actionTable.setFieldName(var[10].toString());
				actionTable.setKey(Integer.parseInt(var[11].toString()));
				actionTables.add(actionTable);
			}
			objects.clear();
		}
		return actionTables;
	}

	/**
	 * To get the search Results.
	 *
	 * @param auditTrackFilterBean the audit track filter bean
	 * @return list
	 * @throws ServiceException the service exception
	 */
	public List<ActionTable> searchAutionTableByAuditFilter(AuditTrackFilterBean auditTrackFilterBean) throws ServiceException {
		return auditsensitiveFieldServiceBean.searchAutionTableByAuditFilterServiceBean(auditTrackFilterBean);
	}
	
	/**
	 * Creates the and activate trigger insert audit service facade.
	 *
	 * @param system the system
	 * @param actionTable the action table
	 * @param lstColumnsPrimaryKey the lst columns primary key
	 * @return true if success
	 * @throws ServiceException the service exception
	 */
	public String createAndActivateTriggerInsertAuditServiceFacade(System system,ActionTable actionTable, List<String> lstColumnsPrimaryKey)throws ServiceException{
		return auditsensitiveFieldServiceBean.createAndActivateTriggerInsertAuditServiceBean(system,actionTable,lstColumnsPrimaryKey);
	}
	
	/**
	 * Creates the and activate trigger update audit service facade.
	 *
	 * @param system the system
	 * @param actionTable the action table
	 * @param lstColumnsPrimaryKey the lst columns primary key
	 * @return true if success
	 * @throws ServiceException the service exception
	 */
	public String createAndActivateTriggerUpdateAuditServiceFacade(System system,ActionTable actionTable, List<String> lstColumnsPrimaryKey)throws ServiceException{
		return auditsensitiveFieldServiceBean.createAndActivateTriggerUpdateAuditServiceBean(system,actionTable,lstColumnsPrimaryKey);
	}
	
	public Boolean checkIfSqlsWereExecuted(ActionTable actionTable) {
		return auditsensitiveFieldServiceBean.seachExecutedSqls(actionTable);
	}
	
	public List<ActionTableSql> filterActionTableSql(ActionTable actionTable){
		return auditsensitiveFieldServiceBean.getAllSqlsOfAction(actionTable);
	}
}
