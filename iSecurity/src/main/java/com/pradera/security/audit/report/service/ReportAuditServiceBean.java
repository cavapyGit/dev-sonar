package com.pradera.security.audit.report.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.security.audit.report.view.to.AuditFieldsChangeFilterTO;
import com.pradera.security.audit.report.view.to.ExceptionPrivilegeReportFilterTO;
import com.pradera.security.audit.report.view.to.ProfileOptionsFilterTO;
import com.pradera.security.audit.report.view.to.UserCreationReportFilterTO;
import com.pradera.security.model.type.UserProfileStateType;
import com.pradera.security.systemmgmt.service.CrudSecurityServiceBean;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ReportAuditServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 09-mar-2015
 */
@Stateless
public class ReportAuditServiceBean extends CrudSecurityServiceBean {

    /** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7398871076214885736L;

	/**
     * Default constructor. 
     */
    public ReportAuditServiceBean() {
        // TODO Auto-generated constructor stub
    }
    
    /**
     * Search monitoring fields tables report.
     *
     * @param auditFieldsFilter the audit fields filter
     * @return the list
     */
    public List<Object[]> searchMonitoringFieldsTablesReport(AuditFieldsChangeFilterTO auditFieldsFilter, String schemaDb){
    	StringBuilder sbQuery = new StringBuilder(300);
    	Map<String, Object> parameters = new HashMap<>();
    	sbQuery.append(" select atl.id_system_fk,atl.REGISTRY_DATE,atl.LAST_MODIFY_USER,atl.LAST_MODIFY_IP ");
    	sbQuery.append(" ,atl.NAME_TABLE,atf.NAME_FIELD,atf.OLD_VALUE,atf.NEW_VALUE ");
    	sbQuery.append(" from " + schemaDb + ".AUDIT_TRACK_LOG atl join " + schemaDb + ".AUDIT_TRACK_FIELD atf on(atl.ID_AUDIT_TRACK_PK=atf.ID_AUDIT_TRACK_FK) ");
    	sbQuery.append(" where (atf.OLD_VALUE is null OR atf.OLD_VALUE <> atf.NEW_VALUE) ");
    	sbQuery.append(" and atl.REGISTRY_DATE >= :initial and atl.REGISTRY_DATE < :final ");
    	parameters.put("initial", auditFieldsFilter.getInitialRegistry());
    	parameters.put("final", CommonsUtilities.putTimeToDateToEndOfDay(auditFieldsFilter.getFinalRegistry()));
    	if(auditFieldsFilter.getSystemId()!=null){
    		sbQuery.append(" and atl.id_system_fk = :system ");
    		parameters.put("system", auditFieldsFilter.getSystemId());
    	}
    	if(StringUtils.isNotBlank(auditFieldsFilter.getTableName())){
    		sbQuery.append(" and atl.NAME_TABLE= :table ");
    		parameters.put("table", auditFieldsFilter.getTableName());
    	}
    	sbQuery.append(" order by atl.REGISTRY_DATE,atl.NAME_TABLE,atf.NAME_FIELD");
    	Query query=em.createNativeQuery(sbQuery.toString());
    	Set<Entry<String, Object>> rawParameters = parameters.entrySet();
    	for (Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
    	return query.getResultList();
    }
    
    
    /**
     * Search user creation report.
     *
     * @param userCreationFilter the user creation filter
     * @return the list
     */
    public List<Object[]> searchUserCreationReport(UserCreationReportFilterTO userCreationFilter){
    	StringBuilder sbQuery = new StringBuilder(300);
    	Map<String, Object> parameters = new HashMap<>();
    	sbQuery.append(" select ua.LOGIN_USER,ua.ID_EMAIL,ua.INSTITUTION_TYPE_FK ,"
    			+ " ua.ID_SECURITY_INSTITUTION_FK,ua.STATE,(ua.FULL_NAME || ' ' || ua.FIRTS_LAST_NAME || ' ' || nvl(ua.SECOND_LAST_NAME,''))"
    			+ " , sp.NAME as profile_name,ua.REGISTRY_USER,ua.CONFIRMATION_USER "
    			+ " , ua.REGISTRY_DATE,ua.CONFIRMATION_DATE,ua.LAST_MODIFY_USER"
    			+ " ,ua.LAST_MODIFY_DATE,ua.LAST_MODIFY_IP ,ua.LAST_ACCESS_DATE"
    			+ " ,ua.LAST_PASSWORD_CHANGE "
    			+ " ,(select SUBSIDIARY_DESCRIPTION from SUBSIDIARY s where s.ID_INSTITUTION_SECURITY_FK= ua.ID_SECURITY_INSTITUTION_FK and ua.ID_SUBSIDIARY_FK=s.ID_SUBSIDIARY_PK) as AGENCIA"
    			+ " ,(select DESCRIPTION from  PARAMETER where ID_PARAMETER_PK=ua.RESPONSABILITY) as responsabilidad "
    			+ " ,ua.DOCUMENT_NUMBER"
    			+ " ,(select NAME from  PARAMETER where ID_PARAMETER_PK=(select DEPARTMENT from  SUBSIDIARY s where s.ID_INSTITUTION_SECURITY_FK= ua.ID_SECURITY_INSTITUTION_FK and ua.ID_SUBSIDIARY_FK=s.ID_SUBSIDIARY_PK) )as DEPARTAMENTO"
    			+ " ,ua.OFFICE "
    			+ " from  user_account ua join  user_profile up on (up.ID_USER_FK=ua.ID_USER_PK) "
    			+ " join  system_profile sp on (up.ID_SYSTEM_PROFILE_FK=sp.ID_SYSTEM_PROFILE_PK)");
    	sbQuery.append(" where ua.REGISTRY_DATE >= :initial and ua.REGISTRY_DATE< :final ");
    	sbQuery.append(" and up.state= :active ");
    	parameters.put("initial", userCreationFilter.getInitialRegistry());
    	parameters.put("final", CommonsUtilities.addDate(userCreationFilter.getFinalRegistry(), 1));
    	parameters.put("active", UserProfileStateType.REGISTERED.getCode());
    	if(StringUtils.isNotBlank(userCreationFilter.getUserName())){
    		sbQuery.append(" and ua.LOGIN_USER like :login  ");
    		parameters.put("login", "%"+userCreationFilter.getUserName()+"%");
    	}
    	if(userCreationFilter.getInstitutionType()!=null){
    		sbQuery.append(" and ua.INSTITUTION_TYPE_FK = :institution ");
    		parameters.put("institution",userCreationFilter.getInstitutionType());
    	}
    	if(userCreationFilter.getInstitution()!=null){
    		sbQuery.append(" and ua.ID_SECURITY_INSTITUTION_FK = :institution_security ");
    		parameters.put("institution_security",userCreationFilter.getInstitution());
    	}
    	if(userCreationFilter.getUserState()!=null){
    		sbQuery.append(" and ua.STATE = :state ");
    		parameters.put("state",userCreationFilter.getUserState());
    	}
    	if(StringUtils.isNotBlank(userCreationFilter.getProfileName())){
    		sbQuery.append(" and sp.NAME like :profileName ");
    		parameters.put("profileName", "%"+userCreationFilter.getProfileName()+"%");
    	}
    	if(StringUtils.isNotBlank(userCreationFilter.getRegistryUser())){
    		sbQuery.append(" and ua.REGISTRY_USER like :registUser  ");
    		parameters.put("registUser", "%"+userCreationFilter.getRegistryUser()+"%");
    	}
    	if(StringUtils.isNotBlank(userCreationFilter.getConfirmationUser())){
    		sbQuery.append(" and ua.CONFIRMATION_USER like :confirmUser  ");
    		parameters.put("confirmUser", "%"+userCreationFilter.getConfirmationUser()+"%");
    	}
    	sbQuery.append(" order by ua.LOGIN_USER,profile_name ");
    	Query query=em.createNativeQuery(sbQuery.toString());
    	Set<Entry<String, Object>> rawParameters = parameters.entrySet();
    	for (Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
    	return query.getResultList();
    }
    
    /**
     * Search exceptions privilege user report.
     *
     * @param exceptionFilter the exception filter
     * @return the list
     */
    public List<Object[]> searchExceptionsPrivilegeUserReport(ExceptionPrivilegeReportFilterTO exceptionFilter){
    	StringBuilder sbQuery = new StringBuilder(300);
    	Map<String, Object> parameters = new HashMap<>();
    	sbQuery.append(" select ua.LOGIN_USER,ua.INSTITUTION_TYPE_FK,ua.ID_SECURITY_INSTITUTION_FK,ua.STATE as user_state ");
    	sbQuery.append(" ,eup.NAME as exception_name,eup.STATE as state_exception,eup.SITUATION,s.NAME as system_name, ");
    	sbQuery.append(" (select distinct so2.ORDER_OPTION from system_option so2 where so2.ID_SYSTEM_OPTION_PK = so.id_option_father and so2.option_type=1) as module_oder, ");
    	sbQuery.append(" (select distinct cl2.CATALOGUE_NAME from system_option so2 join catalogue_language cl2 on (cl2.ID_SYSTEM_OPTION_FK=so2.ID_SYSTEM_OPTION_PK) where so2.ID_SYSTEM_OPTION_PK = so.id_option_father  ");
    	sbQuery.append(" and cl2.LANGUAGE=1 and so2.option_type=1) as module, ");
    	sbQuery.append(" cl.CATALOGUE_NAME,op.ID_PRIVILEGE,eup.REGISTRY_DATE,eup.INITIAL_DATE,eup.END_DATE ");
    	sbQuery.append(" ,eup.LAST_MODIFY_IP,eup.LAST_MODIFY_USER ");
    	sbQuery.append(" from EXCEPTION_USER_PRIVILEGE eup join user_account ua on (eup.ID_USER_FK=ua.ID_USER_PK) ");
    	sbQuery.append(" join system s on (eup.ID_SYSTEM_FK=s.ID_SYSTEM_PK) join user_privilege_detail upd ");
    	sbQuery.append(" on (upd.ID_EXCEPTION_USER_PRIVILEGE_FK=eup.ID_EXCEPTION_USER_PRIVILEGE_PK) ");
    	sbQuery.append(" join option_privilege op on (upd.ID_PRIVILEGE_FK=op.ID_OPTION_PRIVILEGE_PK) ");
    	sbQuery.append(" join system_option so on (op.ID_SYSTEM_OPTION_FK=so.ID_SYSTEM_OPTION_PK) ");
    	sbQuery.append(" join catalogue_language cl on(cl.ID_SYSTEM_OPTION_FK=so.ID_SYSTEM_OPTION_PK) ");
    	sbQuery.append(" where cl.LANGUAGE=1 and so.option_type=2 ");
    	if(StringUtils.isNotBlank(exceptionFilter.getUserName())){
    		sbQuery.append(" and ua.LOGIN_USER like :login  ");
    		parameters.put("login", "%"+exceptionFilter.getUserName()+"%");
    	}
    	if(exceptionFilter.getInstitutionType()!=null){
    		sbQuery.append(" and ua.INSTITUTION_TYPE_FK = :institution ");
    		parameters.put("institution",exceptionFilter.getInstitutionType());
    	}
    	if(exceptionFilter.getInstitution()!=null){
    		sbQuery.append(" and ua.ID_SECURITY_INSTITUTION_FK = :institution_security ");
    		parameters.put("institution_security",exceptionFilter.getInstitution());
    	}
    	if(exceptionFilter.getSystemId()!=null){
    		sbQuery.append(" and s.ID_SYSTEM_PK = :system");
    		parameters.put("system",exceptionFilter.getSystemId());
    	}
    	if(exceptionFilter.getInitialRegistry()!=null){
    		sbQuery.append(" and eup.REGISTRY_DATE >= :initial");
    		parameters.put("initial",exceptionFilter.getInitialRegistry());
    	}
    	if(exceptionFilter.getEndRegistry()!=null){
    		sbQuery.append(" and eup.REGISTRY_DATE < :end");
    		parameters.put("end",CommonsUtilities.addDate(exceptionFilter.getEndRegistry(), 1));
    	}
    	sbQuery.append(" order by ua.LOGIN_USER,module_oder,module,so.ORDER_OPTION ");
    	Query query=em.createNativeQuery(sbQuery.toString());
    	Set<Entry<String, Object>> rawParameters = parameters.entrySet();
    	for (Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
    	return query.getResultList();
    }

    /**
     * Search profiles options report.
     *
     * @param profileFilter the profile filter
     * @return the list
     */
    public List<Object[]> searchProfilesOptionsReport(ProfileOptionsFilterTO profileFilter){
    	StringBuilder sbQuery = new StringBuilder(300);
    	Map<String, Object> parameters = new HashMap<>();
    	sbQuery.append("select sp.MNEMONIC,sp.NAME,sp.IND_USER_TYPE,sp.STATE,sp.SITUATION,s.NAME as system_name,");
    	sbQuery.append(" (select distinct so2.ORDER_OPTION from system_option so2 where so2.ID_SYSTEM_OPTION_PK = so.id_option_father and so2.option_type=1) as module_oder, ");
    	sbQuery.append(" (select distinct cl2.CATALOGUE_NAME from system_option so2 join catalogue_language cl2 on (cl2.ID_SYSTEM_OPTION_FK=so2.ID_SYSTEM_OPTION_PK) where so2.ID_SYSTEM_OPTION_PK = so.id_option_father"); 
    	sbQuery.append(" and cl2.LANGUAGE=1 and so2.option_type=1) as module, ");
    	sbQuery.append(" cl.CATALOGUE_NAME, 1 , pp.STATE as profileprivilege_state,sp.REGISTRY_DATE,sp.LAST_MODIFY_DATE,sp.LAST_MODIFY_USER,sp.LAST_MODIFY_IP ");
//    	sbQuery.append(" cl.CATALOGUE_NAME ,op.ID_PRIVILEGE,pp.STATE as profileprivilege_state,sp.REGISTRY_DATE,sp.LAST_MODIFY_DATE,sp.LAST_MODIFY_USER,sp.LAST_MODIFY_IP ");
    	sbQuery.append(" from SYSTEM_PROFILE sp join system s on (sp.ID_SYSTEM_FK=s.ID_SYSTEM_PK) ");
    	sbQuery.append(" join profile_privilege pp on (pp.ID_SYSTEM_PROFILE_FK=sp.ID_SYSTEM_PROFILE_PK) ");
    	sbQuery.append(" join option_privilege op on (pp.ID_PRIVILEGE_FK=op.ID_OPTION_PRIVILEGE_PK)  ");
    	sbQuery.append(" join system_option so on (op.ID_SYSTEM_OPTION_FK=so.ID_SYSTEM_OPTION_PK) ");
    	sbQuery.append(" join catalogue_language cl on(cl.ID_SYSTEM_OPTION_FK=so.ID_SYSTEM_OPTION_PK) ");
    	sbQuery.append(" where cl.LANGUAGE=1 and so.option_type=2 ");
    	if(StringUtils.isNotBlank(profileFilter.getMnemonic())){
    		sbQuery.append(" and sp.MNEMONIC like :mnemonic ");
    		parameters.put("mnemonic", "%"+profileFilter.getMnemonic()+"%");
    	}
    	if(StringUtils.isNotBlank(profileFilter.getProfileName())){
    		sbQuery.append(" and sp.NAME like :profileName ");
    		parameters.put("profileName", "%"+profileFilter.getProfileName()+"%");
    	}
    	if(profileFilter.getProfileType()!=null){
    		sbQuery.append(" and sp.IND_USER_TYPE = :profileType ");
    		parameters.put("profileType",profileFilter.getProfileType());
    	}
    	if(profileFilter.getProfileState()!=null){
    		sbQuery.append(" and sp.STATE = :profileState ");
    		parameters.put("profileState",profileFilter.getProfileState());
    	}
    	if(profileFilter.getProfileSituation()!=null){
    		sbQuery.append(" and sp.SITUATION = :profileSituation ");
    		parameters.put("profileSituation",profileFilter.getProfileSituation());
    	}
    	if(profileFilter.getSystemId()!=null){
    		sbQuery.append(" and s.ID_SYSTEM_PK = :sistema ");
    		parameters.put("sistema",profileFilter.getSystemId());
    	}
//    	if(profileFilter.getPrivilegeState()!=null){
    		sbQuery.append(" and pp.STATE = :privilegeState ");
    		parameters.put("privilegeState",BooleanType.YES.getCode());
//    	}
    	
    	sbQuery.append(" and sp.STATE <> 6 ");
    	sbQuery.append(" group by sp.MNEMONIC,sp.NAME,sp.IND_USER_TYPE,sp.STATE,sp.SITUATION,s.NAME,  so.id_option_father, cl.CATALOGUE_NAME ,  ");
    	sbQuery.append("  pp.STATE ,sp.REGISTRY_DATE,sp.LAST_MODIFY_DATE,sp.LAST_MODIFY_USER,sp.LAST_MODIFY_IP ");
    	
    	sbQuery.append(" order by sp.NAME,sp.MNEMONIC,module_oder,module ");
    	Query query=em.createNativeQuery(sbQuery.toString());
    	Set<Entry<String, Object>> rawParameters = parameters.entrySet();
    	for (Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
    	return query.getResultList();
    }
    
}
