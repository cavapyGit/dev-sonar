package com.pradera.security.audit.process.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;

import com.pradera.integration.exception.ServiceException;
import com.pradera.security.systemmgmt.service.CrudSecurityServiceBean;

/**
 * <ul>
 * <li>
 * Project Pradera. Copyright 2012.</li>
 * </ul>
 * 
 * The Class SystemServiceFacade.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 19/12/2012
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class MonitoringProcessAuditServiceBean extends CrudSecurityServiceBean{
	/**
	 * Default serial version uid
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Method for retrieving the System options and Catalogue names from the
	 * database
	 * 
	 * @param idSystemOptionPK
	 *            Integer
	 * @param lang
	 *            Integer
	 * @param optnFather
	 *            Integer
	 * @return list of object arrays of the id's
	 * @throws ServiceException
	 */
	public List<Object[]> getAuditProcessListService(Integer idSystemOptionPK,
			Integer lang, Integer optnFather)throws ServiceException {
		StringBuilder stringBuilderSql=new StringBuilder();
		stringBuilderSql.append(" Select SO.idSystemOptionPk, CL.catalogueName ");
		stringBuilderSql.append(" From SystemOption SO");
		stringBuilderSql.append(" Join SO.catalogueLanguages CL");		
		stringBuilderSql.append(" Where CL.language = :languagePrm ");
		stringBuilderSql.append(" AND SO.idSystemOptionPk = CL.systemOption.idSystemOptionPk");		
		stringBuilderSql.append(" And SO.system.idSystemPk = :systemOption");
		stringBuilderSql.append(" And SO.parentSystemOption ");
		if (optnFather != null) {
			stringBuilderSql.append(" = "+optnFather);
		} else {
			stringBuilderSql.append(" is null");
		}
		Query queryString = em.createQuery(stringBuilderSql.toString());
		queryString.setParameter("languagePrm",lang.intValue());
		queryString.setParameter("systemOption", idSystemOptionPK);
		queryString.setHint("org.hibernate.cacheable", true);
		List<Object[]> listOptions = (List<Object[]>) queryString.getResultList();
		return listOptions;
	}
	
	
	/**
	 * Method for getting the audit process id's from the data base
	 * @param integers List<Integer> 
	 * @return the Iteger list of ids
	 * @throws ServiceException
	 */
	public List<Integer> getAuditProceccesIdsService(List<Integer> integers) throws ServiceException{
		try{
		StringBuilder stringBuilderSql=new StringBuilder();
		stringBuilderSql.append(" Select OP.idOptionPrivilegePk ");
		stringBuilderSql.append(" From SystemOption SO");
		stringBuilderSql.append(" Join SO.optionPrivileges OP ");		
		stringBuilderSql.append(" Where SO.idSystemOptionPk = OP.systemOption.idSystemOptionPk");		
		stringBuilderSql.append(" And SO.idSystemOptionPk in (:lstOptionFather) ");
		Query queryString = em.createQuery(stringBuilderSql.toString());
		queryString.setParameter("lstOptionFather",integers);
		queryString.setHint("org.hibernate.cacheable", true);
		List<Integer> listOptions = (List<Integer>) queryString.getResultList();
		return listOptions;
		} catch(NoResultException ex){
			return null;
		} catch(NonUniqueResultException ex){
			return null;
		}
		
		
	}
	
	/**
	 * Method for getting the Process id options from the data base
	 * @param integers List<Integer>
	 * @return the Iteger list of ids
	 * @throws ServiceException
	 */
	public List<Integer> getAuditProcIdsOptnsService(List<Integer> integers)throws ServiceException {
		StringBuilder stringBuilderSql=new StringBuilder();
		stringBuilderSql.append(" Select OP.idOptionPrivilegePk ");
		stringBuilderSql.append(" From SystemOption SO");
		stringBuilderSql.append(" Join SO.optionPrivileges OP ");		
		stringBuilderSql.append(" Where SO.idSystemOptionPk = OP.systemOption.idSystemOptionPk");		
		stringBuilderSql.append(" And SO.idSystemOptionPk = :idSystemOption ");
		Query queryString = em.createQuery(stringBuilderSql.toString());
		queryString.setParameter("idSystemOption",integers);
		queryString.setHint("org.hibernate.cacheable", true);
		List<Integer> listOptions = (List<Integer>) queryString.getResultList();
		return listOptions;
		
	}

}
