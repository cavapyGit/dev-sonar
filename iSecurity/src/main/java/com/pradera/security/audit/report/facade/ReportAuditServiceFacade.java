package com.pradera.security.audit.report.facade;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.pradera.security.audit.report.service.ReportAuditServiceBean;
import com.pradera.security.audit.report.view.to.AuditFieldsChangeFilterTO;
import com.pradera.security.audit.report.view.to.BeanReportAuditsResultTO;
import com.pradera.security.audit.report.view.to.ExceptionPrivilegeReportFilterTO;
import com.pradera.security.audit.report.view.to.ProfileOptionsFilterTO;
import com.pradera.security.audit.report.view.to.UserCreationReportFilterTO;
import com.pradera.security.model.System;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ReportAuditServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 09-mar-2015
 */
@Stateless
public class ReportAuditServiceFacade {
	
	/** The report audit service. */
	@EJB
	ReportAuditServiceBean reportAuditService;

    /**
     * Default constructor. 
     */
    public ReportAuditServiceFacade() {
        // TODO Auto-generated constructor stub
    }
    
    /**
     * Search monitoring fields tables report.
     *
     * @param auditFieldsFilter the audit fields filter
     * @return the list
     */
    public List<BeanReportAuditsResultTO> searchMonitoringFieldsTablesReport(
    		AuditFieldsChangeFilterTO auditFieldsFilter, List<System> allSystems){
    	Optional<System> selectedSystem = allSystems.stream()
    									.filter(s -> s.getIdSystemPk().equals(auditFieldsFilter.getSystemId()))
    									.findFirst();
    									
    	String schemaDb = selectedSystem.get().getSchemaDb();
    	List<BeanReportAuditsResultTO> result = new ArrayList<>();
    	List<Object[]> rows = reportAuditService.searchMonitoringFieldsTablesReport(auditFieldsFilter, schemaDb);
    	for(Object[] row : rows){
    		BeanReportAuditsResultTO bean = new BeanReportAuditsResultTO();
    		bean.setSystemId(Integer.valueOf(row[0].toString()));
    		bean.setRegistryDate((Date)row[1]);
    		bean.setLastModificationUser(row[2]!=null?row[2].toString():"");
    		bean.setLastModificationIp(row[3]!=null?row[3].toString():"");
    		bean.setTableName(row[4]!=null?row[4].toString():"");
    		bean.setFieldName(row[5]!=null?row[5].toString():"");
    		bean.setOldValue(row[6]!=null?row[6].toString():"");
    		bean.setNewValue(row[7]!=null?row[7].toString():"");
    		result.add(bean);
    	}
    	return result;
    }
    
    /**
     * Search user creation report.
     *
     * @param userCreationFilter the user creation filter
     * @return the list
     */
    public List<BeanReportAuditsResultTO> searchUserCreationReport(UserCreationReportFilterTO userCreationFilter){
    	List<BeanReportAuditsResultTO> result = new ArrayList<>();
    	List<Object[]> rows = reportAuditService.searchUserCreationReport(userCreationFilter);
    	for(Object[] row : rows){
    		BeanReportAuditsResultTO bean = new BeanReportAuditsResultTO();
    		bean.setLoginUser(row[0]!=null?row[0].toString():"");
    		bean.setUserEmail(row[1]!=null?row[1].toString():"");
    		bean.setInstitutionType(row[2]!=null?Integer.valueOf(row[2].toString()):Integer.valueOf(-1));
    		bean.setInstitution(row[3]!=null?Integer.valueOf(row[3].toString()):Integer.valueOf(-1));
    		bean.setUserState(row[4]!=null?Integer.valueOf(row[4].toString()):Integer.valueOf(-1));
    		bean.setUserFullName(row[5]!=null?row[5].toString():"");
    		bean.setProfileName(row[6]!=null?row[6].toString():"");
    		bean.setRegistryUser(row[7]!=null?row[7].toString():"");
    		bean.setConfirmationUser(row[8]!=null?row[8].toString():"");
    		bean.setRegistryDate((Date)row[9]);
    		bean.setConfirmationDate((Date)row[10]);
    		bean.setLastModificationUser(row[11].toString());
    		bean.setLastModification((Date)row[12]);
    		bean.setLastModificationIp(row[13].toString());
    		bean.setLastAccessToSystem(row[14]!=null?(Date)row[14]:null);
    		bean.setLastChangePasswordDate(row[15]!=null?(Date)row[15]:null);
    		bean.setAgency(row[16]!=null?row[16].toString():"");
    		bean.setResponsibility(row[17]!=null?row[17].toString():"");
    		bean.setDocumentNumber(row[18]!=null?row[18].toString():"");
    		bean.setDepartment(row[19]!=null?row[19].toString():"");
    		bean.setJobTitle(row[20]!=null?row[20].toString():"");
    		result.add(bean);
    	}
    	return result;
    }
    
    /**
     * Search exceptions privilege user report.
     *
     * @param exceptionFilter the exception filter
     * @return the list
     */
    public List<BeanReportAuditsResultTO> searchExceptionsPrivilegeUserReport(ExceptionPrivilegeReportFilterTO exceptionFilter){
    	List<BeanReportAuditsResultTO> result = new ArrayList<>();
    	List<Object[]> rows = reportAuditService.searchExceptionsPrivilegeUserReport(exceptionFilter);
    	for(Object[] row : rows){
    		BeanReportAuditsResultTO bean = new BeanReportAuditsResultTO();
    		bean.setLoginUser(row[0]!=null?row[0].toString():"");
    		bean.setInstitutionType(row[1]!=null?Integer.valueOf(row[1].toString()):Integer.valueOf(-1));
    		bean.setInstitution(row[2]!=null?Integer.valueOf(row[2].toString()):Integer.valueOf(-1));
    		bean.setUserState(row[3]!=null?Integer.valueOf(row[3].toString()):Integer.valueOf(-1));
    		bean.setExceptionName(row[4]!=null?row[4].toString():"");
    		bean.setExceptionState(row[5]!=null?Integer.valueOf(row[5].toString()):Integer.valueOf(-1));
    		bean.setExceptionSituation(row[6]!=null?Integer.valueOf(row[6].toString()):Integer.valueOf(-1));
    		bean.setSystemName(row[7]!=null?row[7].toString():"");
    		bean.setModuleName(row[9]!=null?row[9].toString():"");
    		bean.setOptionName(row[10]!=null?row[10].toString():"");
    		bean.setPrivilege(row[11]!=null?Integer.valueOf(row[11].toString()):Integer.valueOf(-1));
    		bean.setRegistryDate((Date)row[12]);
    		bean.setInitialException((Date)row[13]);
    		bean.setFinalException((Date)row[14]);
    		bean.setLastModificationIp(row[15].toString());
    		bean.setLastModificationUser(row[16].toString());
    		result.add(bean);
    	}
    	return result;
    }
    
    /**
     * Search profiles options report.
     *
     * @param profileFilter the profile filter
     * @return the list
     */
    public List<BeanReportAuditsResultTO> searchProfilesOptionsReport(ProfileOptionsFilterTO profileFilter){
    	List<BeanReportAuditsResultTO> result = new ArrayList<>();
    	List<Object[]> rows = reportAuditService.searchProfilesOptionsReport(profileFilter);
    	for(Object[] row : rows){
    		BeanReportAuditsResultTO bean = new BeanReportAuditsResultTO();
    		bean.setMnemonic(row[0]!=null?row[0].toString():"");
    		bean.setProfileName(row[1]!=null?row[1].toString():"");
    		bean.setProfileType(row[2]!=null?Integer.valueOf(row[2].toString()):Integer.valueOf(-1));
    		bean.setProfileState(row[3]!=null?Integer.valueOf(row[3].toString()):Integer.valueOf(-1));
    		bean.setProfileSituation(row[4]!=null?Integer.valueOf(row[4].toString()):Integer.valueOf(-1));
    		bean.setSystemName(row[5]!=null?row[5].toString():"");
    		bean.setModuleName(row[7]!=null?row[7].toString():"");
    		bean.setOptionName(row[8]!=null?row[8].toString():"");
    		bean.setPrivilege(row[9]!=null?Integer.valueOf(row[9].toString()):Integer.valueOf(-1));
    		bean.setPrivilegeState(row[10]!=null?Integer.valueOf(row[10].toString()):Integer.valueOf(-1));
    		bean.setRegistryDate((Date)row[11]);
    		bean.setLastModification((Date)row[12]);
    		bean.setLastModificationUser(row[13].toString());
    		bean.setLastModificationIp(row[14].toString());
    		result.add(bean);
    	}
    	return result;
    }
    
    /**
     * Gets the all systems.
     *
     * @return the all systems
     */
    public List<System> getAllSystems(){
    	List<System> systems= new ArrayList<>();
    	List<Object[]> rows =reportAuditService.findByQueryString("select s.idSystemPk,s.name, s.schemaDb from System s");
    	for(Object[] row : rows){
    		System s = new System();
    		s.setIdSystemPk((Integer)row[0]);
    		s.setName(row[1].toString());
    		s.setSchemaDb(row[2].toString());
    		systems.add(s);
    	}
    	return systems;
    }

}
