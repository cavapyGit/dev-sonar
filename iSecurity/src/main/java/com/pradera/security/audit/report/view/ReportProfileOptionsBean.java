package com.pradera.security.audit.report.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.audit.report.facade.ReportAuditServiceFacade;
import com.pradera.security.audit.report.view.to.BeanReportAuditsResultTO;
import com.pradera.security.audit.report.view.to.ProfileOptionsFilterTO;
import com.pradera.security.model.Parameter;
import com.pradera.security.model.System;
import com.pradera.security.model.type.DefinitionType;
import com.pradera.security.model.type.ProfilePrivilegeStateType;
import com.pradera.security.model.type.SystemProfileSituationType;
import com.pradera.security.model.type.SystemProfileStateType;
import com.pradera.security.model.type.UserAccountType;
import com.pradera.security.systemmgmt.service.ParameterSecurityServiceBean;

@DepositaryWebBean
public class ReportProfileOptionsBean extends GenericBaseBean implements
		Serializable {
	
	@EJB
	ReportAuditServiceFacade reportAuditFacade;
	
	@EJB
    ParameterSecurityServiceBean parameterSecurityService;
	
	private List<System> systems;
	
	private List<Parameter> profileTypes;
	
	private Map<Integer,String> mapProfilesTypes;
	
	private List<Parameter> profileStates;
	
	private Map<Integer,String> mapProfileStates;
	
	private List<Parameter> profileSituations;
	
	private Map<Integer,String> mapProfileSituations;
	
	private List<Parameter> privilegeStates;
	
	private Map<Integer,String> mapPrivilegeState;
	
	private ProfileOptionsFilterTO profileOptionsFilter;
	
	private Map<Integer,String> mapPrivilege;
	
	private List<BeanReportAuditsResultTO> profilesOptionsListResult;
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7230766603969553860L;

	public ReportProfileOptionsBean() {
		// TODO Auto-generated constructor stub
	}
	
	@PostConstruct
	public void init() throws ServiceException{
		systems = reportAuditFacade.getAllSystems();
		profileTypes = new ArrayList<>();
		Parameter param = new Parameter();
		param.setIdParameterPk(BooleanType.NO.getCode());
		param.setName(UserAccountType.INTERNAL.getValue());
		profileTypes.add(param);
		param = new Parameter();
		param.setIdParameterPk(BooleanType.YES.getCode());
		param.setName(UserAccountType.EXTERNAL.getValue());
		profileTypes.add(param);
		mapProfilesTypes = new HashMap<>();
		for(Parameter par : profileTypes){
			mapProfilesTypes.put(par.getIdParameterPk(), par.getName());
		}
		profileStates = new ArrayList<>();
		for(SystemProfileStateType spState: SystemProfileStateType.list){
			param = new Parameter();
			param.setIdParameterPk(spState.getCode());
			param.setName(spState.getValue());
			profileStates.add(param);
		}
		mapProfileStates = new HashMap<>();
		for(Parameter par : profileStates){
			mapProfileStates.put(par.getIdParameterPk(), par.getName());
		}
		List<Integer> params =  new ArrayList<>();
		for(SystemProfileSituationType profileSitu:  SystemProfileSituationType.list){
			params.add(profileSitu.getCode());
		}
		profileSituations = parameterSecurityService.listParameterByIdentificator(params);
		mapProfileSituations = new HashMap<>();
		for(Parameter situation: profileSituations){
			mapProfileSituations.put(situation.getIdParameterPk(), situation.getName());
		}
		privilegeStates = new ArrayList<>();
		param = new Parameter();
		param.setIdParameterPk(ProfilePrivilegeStateType.REGISTERED.getCode());
		param.setName(BooleanType.YES.getValue());
		privilegeStates.add(param);
		param = new Parameter();
		param.setIdParameterPk(ProfilePrivilegeStateType.DELETED.getCode());
		param.setName(BooleanType.NO.getValue());
		privilegeStates.add(param);
		mapPrivilegeState = new HashMap<>();
		for(Parameter state: privilegeStates){
			mapPrivilegeState.put(state.getIdParameterPk(), state.getName());
		}
		List<Parameter> privileges = parameterSecurityService.listParameterByDefinitionServiceBean(DefinitionType.PRIVILEGES_OPTION.getCode());
		mapPrivilege = new HashMap<>();
		for(Parameter priv: privileges){
			mapPrivilege.put(priv.getIdParameterPk(), priv.getName());
		}
		profileOptionsFilter = new ProfileOptionsFilterTO();
	}
	
	public Map<String, Object> generateParametersExport(){
		//using LinkedHashMap for using order of insert in map
		LinkedHashMap<String, Object> parametersExport = new LinkedHashMap<>();
		FacesContext context = FacesContext.getCurrentInstance();
	    ResourceBundle msgs = context.getApplication().evaluateExpressionGet(context, "#{msgs}", ResourceBundle.class);
	    //parameter userName
	    parametersExport.put(msgs.getString("audit.report.page.mnemonic"), profileOptionsFilter.getMnemonic());
	    //parameter empty only for look and feel
	    parametersExport.put(msgs.getString("profile.lbl.name"),profileOptionsFilter.getProfileName());
	    parametersExport.put("Perfil Interno/Externo",profileOptionsFilter.getProfileType()!=null?mapProfilesTypes.get(profileOptionsFilter.getProfileType()):"");
	    parametersExport.put("Estado", profileOptionsFilter.getProfileState()!=null?mapProfileStates.get(profileOptionsFilter.getProfileState()):"");
	    parametersExport.put("Situacion", profileOptionsFilter.getProfileSituation()!=null?mapProfileSituations.get(profileOptionsFilter.getProfileSituation()):"");
	    String systemName="";
	    if(profileOptionsFilter.getSystemId()!=null){
	    	for(System system: systems){
	    		if(system.getIdSystemPk().equals(profileOptionsFilter.getSystemId())){
	    			systemName=system.getName();
	    		}
	    	}
	    }
	    parametersExport.put("Tipo de Sistema",systemName);
	    parametersExport.put("Sistema",profileOptionsFilter.getModuleName());
	    parametersExport.put("Privilegio Habilitado/Deshabilitado",profileOptionsFilter.getPrivilegeState()!=null?mapPrivilegeState.get(profileOptionsFilter.getPrivilegeState()):"");
		return parametersExport;
	}
	
	@LoggerAuditWeb
	public void searchProfiles(){
		profilesOptionsListResult = reportAuditFacade.searchProfilesOptionsReport(profileOptionsFilter);
	}
	
	@LoggerAuditWeb
	public void clean(){
		profileOptionsFilter = new ProfileOptionsFilterTO();
		if(profilesOptionsListResult!=null){
			profilesOptionsListResult = null;
		}
	}
	
	public List<System> getSystems() {
		return systems;
	}

	public void setSystems(List<System> systems) {
		this.systems = systems;
	}

	public List<Parameter> getProfileTypes() {
		return profileTypes;
	}

	public void setProfileTypes(List<Parameter> profileTypes) {
		this.profileTypes = profileTypes;
	}

	public Map<Integer, String> getMapProfilesTypes() {
		return mapProfilesTypes;
	}

	public void setMapProfilesTypes(Map<Integer, String> mapProfilesTypes) {
		this.mapProfilesTypes = mapProfilesTypes;
	}

	public List<Parameter> getProfileStates() {
		return profileStates;
	}

	public void setProfileStates(List<Parameter> profileStates) {
		this.profileStates = profileStates;
	}

	public Map<Integer, String> getMapProfileStates() {
		return mapProfileStates;
	}

	public void setMapProfileStates(Map<Integer, String> mapProfileStates) {
		this.mapProfileStates = mapProfileStates;
	}

	public List<Parameter> getProfileSituations() {
		return profileSituations;
	}

	public void setProfileSituations(List<Parameter> profileSituations) {
		this.profileSituations = profileSituations;
	}

	public Map<Integer, String> getMapProfileSituations() {
		return mapProfileSituations;
	}

	public void setMapProfileSituations(Map<Integer, String> mapProfileSituations) {
		this.mapProfileSituations = mapProfileSituations;
	}

	public List<Parameter> getPrivilegeStates() {
		return privilegeStates;
	}

	public void setPrivilegeStates(List<Parameter> privilegeStates) {
		this.privilegeStates = privilegeStates;
	}

	public Map<Integer, String> getMapPrivilegeState() {
		return mapPrivilegeState;
	}

	public void setMapPrivilegeState(Map<Integer, String> mapPrivilegeState) {
		this.mapPrivilegeState = mapPrivilegeState;
	}

	public ProfileOptionsFilterTO getProfileOptionsFilter() {
		return profileOptionsFilter;
	}

	public void setProfileOptionsFilter(ProfileOptionsFilterTO profileOptionsFilter) {
		this.profileOptionsFilter = profileOptionsFilter;
	}

	public List<BeanReportAuditsResultTO> getProfilesOptionsListResult() {
		return profilesOptionsListResult;
	}

	public void setProfilesOptionsListResult(
			List<BeanReportAuditsResultTO> profilesOptionsListResult) {
		this.profilesOptionsListResult = profilesOptionsListResult;
	}

	public Map<Integer,String> getMapPrivilege() {
		return mapPrivilege;
	}

	public void setMapPrivilege(Map<Integer,String> mapPrivilege) {
		this.mapPrivilege = mapPrivilege;
	}

}
