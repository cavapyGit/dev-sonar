package com.pradera.security.audit.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.enterprise.inject.Instance;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.extensions.model.dynaform.DynaFormControl;
import org.primefaces.extensions.model.dynaform.DynaFormLabel;
import org.primefaces.extensions.model.dynaform.DynaFormModel;
import org.primefaces.extensions.model.dynaform.DynaFormRow;

import com.pradera.audit.model.MonitoringProcessAuditDataModel;
import com.pradera.commons.security.model.type.LanguageType;
import com.pradera.commons.services.remote.parameters.ProcessesMonitoringService;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.ViewDepositaryBean;
import com.pradera.security.audit.process.facade.MonitoringProcessAuditBeanFacade;
import com.pradera.security.bean.BeanTypeProcess;
import com.pradera.security.bean.Parameters;
import com.pradera.security.bean.Process;
import com.pradera.security.model.System;
import com.pradera.security.model.type.SystemStateType;
import com.pradera.security.utils.view.GenericBaseBean;

/**
 * <ul>
 * <li>
 * Project Pradera. Copyright 2012.</li>
 * </ul>
 * 
 * The Class SystemServiceFacade.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 19/12/2012
 */
@ViewDepositaryBean
public class MonitoringProcessAuditBean extends GenericBaseBean implements
		Serializable {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	
	@EJB
	private MonitoringProcessAuditBeanFacade monitoringProcessAuditBeanFacade;

	@Inject
	Instance<ProcessesMonitoringService> iProcessesMonitoringRemote;
	
	
	private System objSystem;
	/** The system selected. */
	private Integer systemSelected;
	private List<BeanTypeProcess> lstModules = new ArrayList<BeanTypeProcess>();
	private Integer moduleSelected = -1;
	private Integer subModuleSelected = -1;
	private Integer optionSelected = -1;
	private List<Process> lstProcess;
	private Process process = new Process();
	private List<BeanTypeProcess> lstSubModules = new ArrayList<BeanTypeProcess>();
	private List<BeanTypeProcess> lstOptions = new ArrayList<BeanTypeProcess>();
	/** The obj system selected. */
	private System objSystemSelected;
	private BeanTypeProcess beanType;
	private Date startDate;
	private Date endDate;
	private Process selectedProcess;
	private DynaFormModel model;
	private Parameters parameters;
	private Parameters test;
	private MonitoringProcessAuditDataModel auditModel;

	/**
	 * the max date is used to validate start and end date
	 */
	private Date maxDate;

	/**
	 * @return maxdate
	 */
	public Date getMaxDate() {
		maxDate = CommonsUtilities.currentDate();
		return maxDate;
	}

	/**
	 * @param maxDate
	 */
	public void setMaxDate(Date maxDate) {
		this.maxDate = maxDate;
	}

	/**
	 * @return auditModel
	 */
	public MonitoringProcessAuditDataModel getAuditModel() {
		return auditModel;
	}

	/**
	 * @param auditModel
	 */
	public void setAuditModel(MonitoringProcessAuditDataModel auditModel) {
		this.auditModel = auditModel;
	}

	/**
	 * @return test
	 */
	public Parameters getTest() {
		return test;
	}

	/**
	 * @param test
	 */
	public void setTest(Parameters test) {
		this.test = test;
	}

	/**
	 * @return model
	 */
	public DynaFormModel getModel() {
		return model;
	}

	/**
	 * @param model
	 */
	public void setModel(DynaFormModel model) {
		this.model = model;
	}

	/**
	 * @return parameters
	 */
	public Parameters getParameters() {
		return parameters;
	}

	/**
	 * @param parameters
	 */
	public void setParameters(Parameters parameters) {
		this.parameters = parameters;
	}

	/**
	 * @return selectedProcess
	 */
	public Process getSelectedProcess() {
		return selectedProcess;
	}

	/**
	 * @param selectedProcess
	 */
	public void setSelectedProcess(Process selectedProcess) {
		this.selectedProcess = selectedProcess;
	}

	/**
	 * @return startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 */
	public void setStartDate(Date startDate) {
		Date today = CommonsUtilities.currentDate();
		if (startDate != null && today.compareTo(startDate) >= 0) {
			this.startDate = startDate;
			if (endDate != null && startDate.after(endDate)) {
				endDate = null;
			}
		} else {
			this.startDate = null;
		}
	}

	/**
	 * @return endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate
	 */
	public void setEndDate(Date endDate) {
		Date today = CommonsUtilities.currentDate();
		if (endDate != null && today.compareTo(endDate) >= 0
				&& (startDate == null || startDate.compareTo(endDate) <= 0)) {
			this.endDate = endDate;
		} else {
			this.endDate = null;
		}
	}

	/**
	 * @return optionSelected
	 */
	public Integer getOptionSelected() {
		return optionSelected;
	}

	/**
	 * @param optionSelected
	 */
	public void setOptionSelected(Integer optionSelected) {
		this.optionSelected = optionSelected;
	}

	/**
	 * @return beanType
	 */
	public BeanTypeProcess getBeanTypeProcess() {
		return beanType;
	}

	/**
	 * @param beanType
	 */
	public void setBeanTypeProcess(BeanTypeProcess beanType) {
		this.beanType = beanType;
	}



	

	/**
	 * @return objSystemSelected
	 */
	public System getObjSystemSelected() {
		return objSystemSelected;
	}

	/**
	 * @return subModuleSelected
	 */
	public Integer getSubModuleSelected() {
		return subModuleSelected;
	}

	/**
	 * @param subModuleSelected
	 */
	public void setSubModuleSelected(Integer subModuleSelected) {
		this.subModuleSelected = subModuleSelected;
	}

	/**
	 * @param objSystemSelected
	 */
	public void setObjSystemSelected(System objSystemSelected) {
		this.objSystemSelected = objSystemSelected;
	}

	/**
	 * @return lstSubModules
	 */
	public List<BeanTypeProcess> getLstSubModules() {
		return lstSubModules;
	}

	/**
	 * @param lstSubModules
	 */
	public void setLstSubModules(List<BeanTypeProcess> lstSubModules) {
		this.lstSubModules = lstSubModules;
	}

	/**
	 * @return lstOptions
	 */
	public List<BeanTypeProcess> getLstOptions() {
		return lstOptions;
	}

	/**
	 * @param lstOptions
	 */
	public void setLstOptions(List<BeanTypeProcess> lstOptions) {
		this.lstOptions = lstOptions;
	}

	/**
	 * @return process
	 */
	public Process getProcess() {
		return process;
	}

	/**
	 * @param process
	 */
	public void setProcess(Process process) {
		this.process = process;
	}

	/**
	 * @return lstProcess
	 */
	public List<Process> getLstProcess() {
		return lstProcess;
	}

	/**
	 * @param lstProcess
	 */
	public void setLstProcess(List<Process> lstProcess) {
		this.lstProcess = lstProcess;
	}

	/**
	 * @return moduleSelected
	 */
	public Integer getModuleSelected() {
		return moduleSelected;
	}

	/**
	 * @param moduleSelected
	 */
	public void setModuleSelected(Integer moduleSelected) {
		this.moduleSelected = moduleSelected;
	}

	/**
	 * @return lstModules
	 */
	public List<BeanTypeProcess> getLstModules() {
		return lstModules;
	}

	/**
	 * @param lstModules
	 */
	public void setLstModules(List<BeanTypeProcess> lstModules) {
		this.lstModules = lstModules;
	}

	/**
	 * @return systemSelected
	 */
	public Integer getSystemSelected() {
		return systemSelected;
	}

	/**
	 * @param systemSelected
	 */
	public void setSystemSelected(Integer systemSelected) {
		this.systemSelected = systemSelected;
	}

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		if (!FacesContext.getCurrentInstance().isPostback()) {
			beginConversation();
			startDate = CommonsUtilities.currentDate();
			endDate = CommonsUtilities.currentDate();
			listSystemsByState(SystemStateType.CONFIRMED.getCode());
		}
	}

	/**
	 * Method for getting the Remote service object
	 * 
	 * @return AuditProcessesRemote service object
	 */
	// public static AuditProcessesRemote getAuditProcessesRemote(){
	// try{
	// final Hashtable jndiProperties = new Hashtable();
	// jndiProperties.put(Context.URL_PKG_PREFIXES,
	// "org.jboss.ejb.client.naming");
	// final Context context = new InitialContext(jndiProperties);
	// auditProcesses =
	// (AuditProcessesRemote)context.lookup("ejb:/PraderaGeneralParameters//AuditProcessesBean!com.pradera.commons.processes.remote.AuditProcessesRemote");
	// }catch (Exception e) {
	// e.printStackTrace();
	// }
	// return auditProcesses;
	// }
	/**
	 * Method for retrieving the Modules
	 */
	public void auditModuleList() {
		objSystemSelected = findSystemSelected();
		try {
			lstModules.clear();
			lstSubModules.clear();
			lstOptions.clear();
			List<Object[]> lstModulesArry = monitoringProcessAuditBeanFacade
					.getAuditProcessListFacade(
							objSystemSelected.getIdSystemPk(), getLanguageId(),
							null);
			BeanTypeProcess type = null;
			Integer idSystemOptionPk = null;
			// Iterating the lstModulesArry list for getting the modules list
			for (Object[] arrObj : lstModulesArry) {
				type = new BeanTypeProcess();
				idSystemOptionPk = new Integer(arrObj[0].toString());
				type.setIdTypePK(idSystemOptionPk);
				if (arrObj[1] != null) {
					type.setDescription(arrObj[1].toString());
					lstModules.add(type);
				}

			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Method for retrieving the Sub-Modules
	 */
	public void auditSubModuleList() {
		beanType = findModuleSelected();
		try {
			lstSubModules.clear();
			lstOptions.clear();
			List<Object[]> lstModulesArry = monitoringProcessAuditBeanFacade
					.getAuditProcessListFacade(
							objSystemSelected.getIdSystemPk(), getLanguageId(),
							beanType.getIdTypePK());
			BeanTypeProcess type = null;
			Integer idSystemOptionPk = null;
			// Iterating the lstModulesArry list for getting the sub-modules
			// list
			for (Object[] arrObj : lstModulesArry) {
				type = new BeanTypeProcess();
				idSystemOptionPk = new Integer(arrObj[0].toString());
				type.setIdTypePK(idSystemOptionPk);
				if (arrObj[1] != null) {
					type.setDescription(arrObj[1].toString());
					lstSubModules.add(type);
				}

			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}

	}

	/**
	 * Method for retrieving the options
	 */
	public void auditOptionsList() {
		beanType = findSubModuleSelected();
		try {
			lstOptions.clear();
			List<Object[]> lstModulesArry = monitoringProcessAuditBeanFacade
					.getAuditProcessListFacade(
							objSystemSelected.getIdSystemPk(), getLanguageId(),
							beanType.getIdTypePK());
			BeanTypeProcess type = null;
			Integer idSystemOptionPk = null;
			// Iterating the lstModulesArry list for getting the options list
			for (Object[] arrObj : lstModulesArry) {
				type = new BeanTypeProcess();
				idSystemOptionPk = new Integer(arrObj[0].toString());
				type.setIdTypePK(idSystemOptionPk);
				if (arrObj[1] != null) {
					type.setDescription(arrObj[1].toString());
					lstOptions.add(type);
				}

			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}

	}

	/**
	 * Find system selected.
	 * 
	 * @return the system
	 */
	public System findSystemSelected() {

		for (System sys : lstSystems) {
			if ((systemSelected.equals(sys.getIdSystemPk()))) {
				sys.setOptionUrl(sys.getSystemUrl());
				return sys;
			}
		}
		return null;
	}

	/**
	 * Find sub-module selected.
	 * 
	 * @return the sub-module
	 */
	public BeanTypeProcess findSubModuleSelected() {
		for (BeanTypeProcess module : lstSubModules) {
			if ((subModuleSelected.equals(module.getIdTypePK()))) {
				return module;
			}
		}
		return null;
	}

	/**
	 * Find module selected.
	 * 
	 * @return the module
	 */
	public BeanTypeProcess findModuleSelected() {
		for (BeanTypeProcess subModule : lstModules) {
			if ((moduleSelected.equals(subModule.getIdTypePK()))) {
				return subModule;
			}
		}
		return null;
	}

	/**
	 * Constructor
	 */
	public MonitoringProcessAuditBean() {
		objSystem = new System();
	}

	/**
	 * Method for retrieving the audit process details
	 */
	public void listProcess() {
		lstProcess = new ArrayList<Process>();
		List<Integer> integers = new ArrayList<Integer>();
		Integer integer = null;
		lstProcess.clear();
		try {
			if (subModuleSelected == null) {
				for (BeanTypeProcess beanType : lstSubModules) {
					integer = new Integer(beanType.getIdTypePK());
					integers.add(integer);
				}
			} else {
				integers.add(subModuleSelected);
			}

			List<Integer> listIds = monitoringProcessAuditBeanFacade
					.getAuditProceccesIds(integers);
			if(listIds == null){
				auditModel = new MonitoringProcessAuditDataModel();
				return;
			}
			if (optionSelected != null) {
				integers.clear();
				integers.add(optionSelected);
				if (integers.size() > 0) {
					listIds = monitoringProcessAuditBeanFacade
							.getAuditProcIdsOptns(integers);
				}
			}
			if (listIds.size() > 0) {
				List<Object[]> listProcDetails = iProcessesMonitoringRemote.get()
						.getAuditProcessesDetails(listIds, startDate, endDate);
				Process process = null;
				// Iterating the listProcDetails for getting the process details
				for (Object[] proc : listProcDetails) {
					process = new Process();
					process.setProcessType(proc[0].toString());
					process.setIdProcessPK(new Integer(proc[1].toString()));
					process.setProcessModule(proc[2].toString());
					process.setProcessSubmodule(proc[3].toString());
					process.setProcessName(proc[4].toString());
					String date = proc[5].toString().trim();
					process.setProcessDate(date.substring(0, 10));
					process.setProcessTime(date.substring(10, date.length()));
					process.setProcessUser(proc[6].toString());
					process.setIpProcess(proc[7].toString());
					process.setProcessStatus(proc[8].toString());
					lstProcess.add(process);
				}
			}
			auditModel = new MonitoringProcessAuditDataModel(lstProcess);

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Method for displaying the process details of selected row
	 */
	public void getProcessDetails() {
		Long id = new Long(selectedProcess.getIdProcessPK());
		List<Object[]> listProcParams = null;
		DynaFormRow dynaFormRow = null;
		DynaFormControl dynaFormControl = null;
		DynaFormLabel dynaFormLabel = null;
		Parameters param = null;
		try {
			model = new DynaFormModel();
			listProcParams = iProcessesMonitoringRemote.get()
					.getProcessParameters(id);
			// Iterating the listProcParams for getting the key and values of
			// the selected process parameters
			for (Object[] obj : listProcParams) {
				param = new Parameters(obj[1].toString());
				dynaFormRow = model.createRegularRow();
				dynaFormLabel = dynaFormRow.addLabel(obj[0].toString(), 1, 1);
				dynaFormControl = dynaFormRow.addControl(param, "input", 1, 1);
				dynaFormLabel.setForControl(dynaFormControl);
			}
			// Showing the dialogue box parameters
			JSFUtilities
					.showComponent("formMonitoringProcessAudit:cnfAdministrarUsu");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}

	}

	/**
	 * Method for clear the screen components
	 */
	public void clearProcess() {
		if (lstProcess != null) {
			lstProcess.clear();
		}
		if (lstModules != null) {
			lstModules.clear();
		}
		auditModel = null;
		if (lstSubModules != null) {
			lstSubModules.clear();
		}
		if (lstOptions != null) {
			lstOptions.clear();
		}

		systemSelected = 0;
		moduleSelected = 0;
		subModuleSelected = 0;
		optionSelected = 0;
		startDate = CommonsUtilities.currentDate();
		endDate = CommonsUtilities.currentDate();
		JSFUtilities.resetComponent("formMonitoringProcessAudit:systemTypeSelect");
		JSFUtilities.resetComponent("formMonitoringProcessAudit:modeuleSelect");
	}

	/**
	 * @return objSystem
	 */
	public System getObjSystem() {
		return objSystem;
	}

	/**
	 * @param objSystem
	 */
	public void setObjSystem(System objSystem) {
		this.objSystem = objSystem;
	}

	/**
	 * Method for getting the language locale
	 * 
	 * @return the id of language
	 */
	private int getLanguageId() {
		String strLocale = (String) JSFUtilities.getSessionMap("locale");
		int localeId = 0;
		Map<Integer, LanguageType> mapLanguageType = LanguageType.lookup;
		for (Entry<Integer, LanguageType> entry : mapLanguageType.entrySet()) {
			if (((LanguageType) entry.getValue()).getValue().equals(strLocale)) {
				localeId = entry.getKey();
				break;
			}
		}
		return localeId;
	}

	@PreDestroy
	void cleanup()
	{
		endConversation();
	}
}
