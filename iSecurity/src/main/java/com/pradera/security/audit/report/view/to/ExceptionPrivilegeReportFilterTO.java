package com.pradera.security.audit.report.view.to;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ExceptionPrivilegeReportFilterTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 09-mar-2015
 */
public class ExceptionPrivilegeReportFilterTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 885648269201977736L;
	
	/** The user name. */
	private String userName;
	
	/** The institution type. */
	private Integer institutionType;
	
	/** The institution. */
	private Integer institution;
	
	/** The system id. */
	private Integer systemId;
	
	/** The initial registry. */
	private Date initialRegistry;
	
	/** The end registry. */
	private Date endRegistry;

	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName the new user name
	 */
	public void setUserName(String userName) {
		if(StringUtils.isNotBlank(userName)){
			userName = userName.toUpperCase();
		}
		this.userName = userName;
	}

	/**
	 * Gets the institution type.
	 *
	 * @return the institution type
	 */
	public Integer getInstitutionType() {
		return institutionType;
	}

	/**
	 * Sets the institution type.
	 *
	 * @param institutionType the new institution type
	 */
	public void setInstitutionType(Integer institutionType) {
		this.institutionType = institutionType;
	}

	/**
	 * Gets the institution.
	 *
	 * @return the institution
	 */
	public Integer getInstitution() {
		return institution;
	}

	/**
	 * Sets the institution.
	 *
	 * @param institution the new institution
	 */
	public void setInstitution(Integer institution) {
		this.institution = institution;
	}

	/**
	 * Gets the system id.
	 *
	 * @return the system id
	 */
	public Integer getSystemId() {
		return systemId;
	}

	/**
	 * Sets the system id.
	 *
	 * @param systemId the new system id
	 */
	public void setSystemId(Integer systemId) {
		this.systemId = systemId;
	}

	/**
	 * Gets the initial registry.
	 *
	 * @return the initial registry
	 */
	public Date getInitialRegistry() {
		return initialRegistry;
	}

	/**
	 * Sets the initial registry.
	 *
	 * @param initialRegistry the new initial registry
	 */
	public void setInitialRegistry(Date initialRegistry) {
		this.initialRegistry = initialRegistry;
	}

	/**
	 * Gets the end registry.
	 *
	 * @return the end registry
	 */
	public Date getEndRegistry() {
		return endRegistry;
	}

	/**
	 * Sets the end registry.
	 *
	 * @param endRegistry the new end registry
	 */
	public void setEndRegistry(Date endRegistry) {
		this.endRegistry = endRegistry;
	}

}
