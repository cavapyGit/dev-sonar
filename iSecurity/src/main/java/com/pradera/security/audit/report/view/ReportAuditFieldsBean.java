package com.pradera.security.audit.report.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.security.audit.report.facade.ReportAuditServiceFacade;
import com.pradera.security.audit.report.view.to.AuditFieldsChangeFilterTO;
import com.pradera.security.audit.report.view.to.BeanReportAuditsResultTO;
import com.pradera.security.audit.service.AuditsensitiveFieldServiceBean;
import com.pradera.security.model.ActionTable;
import com.pradera.security.model.System;
import com.pradera.security.systemmgmt.service.ParameterSecurityServiceBean;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ReportAuditFieldsBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 10-mar-2015
 */
@DepositaryWebBean
public class ReportAuditFieldsBean extends GenericBaseBean implements
		Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8654833679549024596L;
	
	/** The report audit facade. */
	@EJB
	ReportAuditServiceFacade reportAuditFacade;
	
	/** The parameter security service. */
	@EJB
    ParameterSecurityServiceBean parameterSecurityService;
	
	/** The audit fields service. */
	@EJB
	AuditsensitiveFieldServiceBean auditFieldsService;
	
	/** The systems. */
	private List<System> systems;
	
	/** The map systems. */
	private Map<Integer,String> mapSystems;
	
	/** The tables. */
	private List<String> tables;
	
	/** The audit fields filter. */
	private AuditFieldsChangeFilterTO auditFieldsFilter;
	
	/** The audit fields result. */
	private List<BeanReportAuditsResultTO> auditFieldsResult;
	
	/** The tables system. */
	private Map<Integer,List<String>> tablesSystem;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		systems = reportAuditFacade.getAllSystems();
		mapSystems = new HashMap<>();
		for(System sys : systems){
			mapSystems.put(sys.getIdSystemPk(), sys.getName());
		}
		tablesSystem = new HashMap<>();
		List<ActionTable> rules = auditFieldsService.searchRulesWithTriggers();
		tables = new ArrayList<>();
		for(ActionTable action : rules){
			if(tablesSystem.containsKey(action.getSystem().getIdSystemPk())){
				List<String> tables = tablesSystem.get(action.getSystem().getIdSystemPk());
				tables.add(action.getTableName());
			} else {
				List<String> tables = new ArrayList<>();
				tables.add(action.getTableName());
				tablesSystem.put(action.getSystem().getIdSystemPk(), tables);
			}
			tables.add(action.getTableName());
		}
		
		auditFieldsFilter = new AuditFieldsChangeFilterTO();
		auditFieldsFilter.setInitialRegistry(CommonsUtilities.currentDate());
		auditFieldsFilter.setFinalRegistry(CommonsUtilities.currentDate());
	}
	
	public Map<String, Object> generateParametersExport(){
		//using LinkedHashMap for using order of insert in map
		LinkedHashMap<String, Object> parametersExport = new LinkedHashMap<>();
		FacesContext context = FacesContext.getCurrentInstance();
	    ResourceBundle msgs = context.getApplication().evaluateExpressionGet(context, "#{msgs}", ResourceBundle.class);
	    //parameter userName
	    parametersExport.put("Sistema",auditFieldsFilter.getSystemId()!=null?mapSystems.get(auditFieldsFilter.getSystemId()):"");
	    //parameter empty only for look and feel
	    parametersExport.put("Tabla",auditFieldsFilter.getTableName());
	    parametersExport.put("Feca Inicial",CommonsUtilities.convertDatetoString(auditFieldsFilter.getInitialRegistry()));
	    parametersExport.put("Fecha Final",CommonsUtilities.convertDatetoString(auditFieldsFilter.getFinalRegistry()));
		return parametersExport;
	}
	
	/**
	 * Search exceptions.
	 */
	@LoggerAuditWeb
	public void searchExceptions(){
		auditFieldsResult = reportAuditFacade.searchMonitoringFieldsTablesReport(auditFieldsFilter, systems);
	}
	
	/**
	 * Clean.
	 */
	@LoggerAuditWeb
	public void clean(){
		auditFieldsFilter = new AuditFieldsChangeFilterTO();
		auditFieldsFilter.setInitialRegistry(CommonsUtilities.currentDate());
		auditFieldsFilter.setFinalRegistry(CommonsUtilities.currentDate());
		if(auditFieldsResult!=null){
			auditFieldsResult = null;
		}
	}

	/**
	 * Gets the systems.
	 *
	 * @return the systems
	 */
	public List<System> getSystems() {
		return systems;
	}

	/**
	 * Sets the systems.
	 *
	 * @param systems the new systems
	 */
	public void setSystems(List<System> systems) {
		this.systems = systems;
	}

	/**
	 * Gets the map systems.
	 *
	 * @return the map systems
	 */
	public Map<Integer,String> getMapSystems() {
		return mapSystems;
	}

	/**
	 * Sets the map systems.
	 *
	 * @param mapSystems the map systems
	 */
	public void setMapSystems(Map<Integer,String> mapSystems) {
		this.mapSystems = mapSystems;
	}

	/**
	 * Gets the tables.
	 *
	 * @return the tables
	 */
	public List<String> getTables() {
		return tables;
	}

	/**
	 * Sets the tables.
	 *
	 * @param tables the new tables
	 */
	public void setTables(List<String> tables) {
		this.tables = tables;
	}

	/**
	 * Gets the audit fields filter.
	 *
	 * @return the audit fields filter
	 */
	public AuditFieldsChangeFilterTO getAuditFieldsFilter() {
		return auditFieldsFilter;
	}

	/**
	 * Sets the audit fields filter.
	 *
	 * @param auditFieldsFilter the new audit fields filter
	 */
	public void setAuditFieldsFilter(AuditFieldsChangeFilterTO auditFieldsFilter) {
		this.auditFieldsFilter = auditFieldsFilter;
	}

	/**
	 * Gets the audit fields result.
	 *
	 * @return the audit fields result
	 */
	public List<BeanReportAuditsResultTO> getAuditFieldsResult() {
		return auditFieldsResult;
	}

	/**
	 * Sets the audit fields result.
	 *
	 * @param auditFieldsResult the new audit fields result
	 */
	public void setAuditFieldsResult(List<BeanReportAuditsResultTO> auditFieldsResult) {
		this.auditFieldsResult = auditFieldsResult;
	}

	/**
	 * Gets the tables system.
	 *
	 * @return the tables system
	 */
	public Map<Integer,List<String>> getTablesSystem() {
		return tablesSystem;
	}

	/**
	 * Sets the tables system.
	 *
	 * @param tablesSystem the tables system
	 */
	public void setTablesSystem(Map<Integer,List<String>> tablesSystem) {
		this.tablesSystem = tablesSystem;
	}
	
	

}
