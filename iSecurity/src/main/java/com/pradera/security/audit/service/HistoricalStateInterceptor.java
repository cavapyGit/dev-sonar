package com.pradera.security.audit.service;

import java.io.Serializable;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.security.model.HistoricalState;
import com.pradera.security.systemmgmt.service.CrudSecurityServiceBean;
import com.pradera.security.utils.view.PropertiesConstants;


public class HistoricalStateInterceptor implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3410286256432383182L;
	
	@Resource
	private TransactionSynchronizationRegistry registry;
	
	@EJB
	CrudSecurityServiceBean securityService;

	public HistoricalStateInterceptor() {
		// TODO Auto-generated constructor stub
	}

	@LoggerAuditWeb
	@TransactionAttribute(TransactionAttributeType.SUPPORTS) 
	public Object aroundInvokeTransaction(InvocationContext ic)
			throws Exception {
		try {
		return ic.proceed();
		} finally{
			//in the end register change state
		 HistoricalState history = (HistoricalState)ThreadLocalContextHolder.get(PropertiesConstants.HISTORICAL_STATE_LOGGER);
		 if(history != null){
			 LoggerUser loggerUser = (LoggerUser)registry.getResource(RegistryContextHolderType.LOGGER_USER);
			 if(loggerUser != null)
				 history.setRegistryUser(loggerUser.getUserName());
			 securityService.getEm().persist(history);
		 }
		}
	}

}
