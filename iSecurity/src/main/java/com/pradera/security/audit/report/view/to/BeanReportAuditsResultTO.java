package com.pradera.security.audit.report.view.to;

import java.io.Serializable;
import java.util.Date;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BeanReportAuditsResultTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 09-mar-2015
 */
public class BeanReportAuditsResultTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 245443010642407035L;
	
	

	/** The mnemonic. */
	private String mnemonic;
	
	/** The profile name. */
	private String profileName;
	
	/** The profile type. */
	private Integer profileType;
	
	/** The profile state. */
	private Integer profileState;
	
	/** The profile situation. */
	private Integer profileSituation;
	
	/** The system name. */
	private String systemName;
	
	/** The module name. */
	private String moduleName;
	
	/** The option name. */
	private String optionName;
	
	/** The privilege. */
	private Integer privilege;
	
	/** The privilege state. */
	private Integer privilegeState;
	
	/** The login user. */
	private String loginUser;
	
	/** The institution type. */
	private Integer institutionType;
	
	/** The institution. */
	private Integer institution;
	
	/** The exception name. */
	private String exceptionName;
	
	/** The exception state. */
	private Integer exceptionState;
	
	/** The user state. */
	private Integer userState;
	
	/** The exception situation. */
	private Integer exceptionSituation;
	
	/** The initial exception. */
	private Date initialException;
	
	/** The Final exception. */
	private Date FinalException;
	
	/** The user email. */
	private String userEmail;
	
	/** The user full name. */
	private String userFullName;
	
	/** The registry user. */
	private String registryUser;
	
	/** The confirmation user. */
	private String confirmationUser;
	
	/** The confirmation date. */
	private Date confirmationDate;
	
	/** The last change password date. */
	private Date lastChangePasswordDate;
	
	/** The last access to system. */
	private Date lastAccessToSystem;
	
	/** The old value. */
	private String oldValue;
	
	/** The new value. */
	private String newValue;
	
	/** The table name. */
	private String tableName;
	
	/** The field name. */
	private String fieldName;
	
	private Integer systemId;
	
	/** The registry date. */
	private Date registryDate;
	
	/** The last modification. */
	private Date lastModification;
	
	/** The last modification user. */
	private String lastModificationUser;
	
	/** The last modification ip. */
	private String lastModificationIp;
	
	private String agency;
	
	private String responsibility;
	
	private String documentNumber;
	
	private String department;
	
	private String jobTitle;
	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}

	/**
	 * Sets the mnemonic.
	 *
	 * @param mnemonic the new mnemonic
	 */
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	/**
	 * Gets the profile name.
	 *
	 * @return the profile name
	 */
	public String getProfileName() {
		return profileName;
	}

	/**
	 * Sets the profile name.
	 *
	 * @param profileName the new profile name
	 */
	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

	/**
	 * Gets the profile type.
	 *
	 * @return the profile type
	 */
	public Integer getProfileType() {
		return profileType;
	}

	/**
	 * Sets the profile type.
	 *
	 * @param profileType the new profile type
	 */
	public void setProfileType(Integer profileType) {
		this.profileType = profileType;
	}

	/**
	 * Gets the profile state.
	 *
	 * @return the profile state
	 */
	public Integer getProfileState() {
		return profileState;
	}

	/**
	 * Sets the profile state.
	 *
	 * @param profileState the new profile state
	 */
	public void setProfileState(Integer profileState) {
		this.profileState = profileState;
	}

	/**
	 * Gets the profile situation.
	 *
	 * @return the profile situation
	 */
	public Integer getProfileSituation() {
		return profileSituation;
	}

	/**
	 * Sets the profile situation.
	 *
	 * @param profileSituation the new profile situation
	 */
	public void setProfileSituation(Integer profileSituation) {
		this.profileSituation = profileSituation;
	}

	/**
	 * Gets the system name.
	 *
	 * @return the system name
	 */
	public String getSystemName() {
		return systemName;
	}

	/**
	 * Sets the system name.
	 *
	 * @param systemName the new system name
	 */
	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	/**
	 * Gets the module name.
	 *
	 * @return the module name
	 */
	public String getModuleName() {
		return moduleName;
	}

	/**
	 * Sets the module name.
	 *
	 * @param moduleName the new module name
	 */
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	/**
	 * Gets the option name.
	 *
	 * @return the option name
	 */
	public String getOptionName() {
		return optionName;
	}

	/**
	 * Sets the option name.
	 *
	 * @param optionName the new option name
	 */
	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}

	/**
	 * Gets the privilege.
	 *
	 * @return the privilege
	 */
	public Integer getPrivilege() {
		return privilege;
	}

	/**
	 * Sets the privilege.
	 *
	 * @param privilege the new privilege
	 */
	public void setPrivilege(Integer privilege) {
		this.privilege = privilege;
	}

	/**
	 * Gets the privilege state.
	 *
	 * @return the privilege state
	 */
	public Integer getPrivilegeState() {
		return privilegeState;
	}

	/**
	 * Sets the privilege state.
	 *
	 * @param privilegeState the new privilege state
	 */
	public void setPrivilegeState(Integer privilegeState) {
		this.privilegeState = privilegeState;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the last modification.
	 *
	 * @return the last modification
	 */
	public Date getLastModification() {
		return lastModification;
	}

	/**
	 * Sets the last modification.
	 *
	 * @param lastModification the new last modification
	 */
	public void setLastModification(Date lastModification) {
		this.lastModification = lastModification;
	}

	/**
	 * Gets the last modification user.
	 *
	 * @return the last modification user
	 */
	public String getLastModificationUser() {
		return lastModificationUser;
	}

	/**
	 * Sets the last modification user.
	 *
	 * @param lastModificationUser the new last modification user
	 */
	public void setLastModificationUser(String lastModificationUser) {
		this.lastModificationUser = lastModificationUser;
	}

	/**
	 * Gets the last modification ip.
	 *
	 * @return the last modification ip
	 */
	public String getLastModificationIp() {
		return lastModificationIp;
	}

	/**
	 * Sets the last modification ip.
	 *
	 * @param lastModificationIp the new last modification ip
	 */
	public void setLastModificationIp(String lastModificationIp) {
		this.lastModificationIp = lastModificationIp;
	}

	/**
	 * Gets the login user.
	 *
	 * @return the login user
	 */
	public String getLoginUser() {
		return loginUser;
	}

	/**
	 * Sets the login user.
	 *
	 * @param loginUser the new login user
	 */
	public void setLoginUser(String loginUser) {
		this.loginUser = loginUser;
	}

	/**
	 * Gets the institution type.
	 *
	 * @return the institution type
	 */
	public Integer getInstitutionType() {
		return institutionType;
	}

	/**
	 * Sets the institution type.
	 *
	 * @param institutionType the new institution type
	 */
	public void setInstitutionType(Integer institutionType) {
		this.institutionType = institutionType;
	}

	/**
	 * Gets the institution.
	 *
	 * @return the institution
	 */
	public Integer getInstitution() {
		return institution;
	}

	/**
	 * Sets the institution.
	 *
	 * @param institution the new institution
	 */
	public void setInstitution(Integer institution) {
		this.institution = institution;
	}

	/**
	 * Gets the exception name.
	 *
	 * @return the exception name
	 */
	public String getExceptionName() {
		return exceptionName;
	}

	/**
	 * Sets the exception name.
	 *
	 * @param exceptionName the new exception name
	 */
	public void setExceptionName(String exceptionName) {
		this.exceptionName = exceptionName;
	}

	/**
	 * Gets the exception state.
	 *
	 * @return the exception state
	 */
	public Integer getExceptionState() {
		return exceptionState;
	}

	/**
	 * Sets the exception state.
	 *
	 * @param exceptionState the new exception state
	 */
	public void setExceptionState(Integer exceptionState) {
		this.exceptionState = exceptionState;
	}

	/**
	 * Gets the user state.
	 *
	 * @return the user state
	 */
	public Integer getUserState() {
		return userState;
	}

	/**
	 * Sets the user state.
	 *
	 * @param userState the new user state
	 */
	public void setUserState(Integer userState) {
		this.userState = userState;
	}

	/**
	 * Gets the exception situation.
	 *
	 * @return the exception situation
	 */
	public Integer getExceptionSituation() {
		return exceptionSituation;
	}

	/**
	 * Sets the exception situation.
	 *
	 * @param exceptionSituation the new exception situation
	 */
	public void setExceptionSituation(Integer exceptionSituation) {
		this.exceptionSituation = exceptionSituation;
	}

	/**
	 * Gets the initial exception.
	 *
	 * @return the initial exception
	 */
	public Date getInitialException() {
		return initialException;
	}

	/**
	 * Sets the initial exception.
	 *
	 * @param initialException the new initial exception
	 */
	public void setInitialException(Date initialException) {
		this.initialException = initialException;
	}

	/**
	 * Gets the final exception.
	 *
	 * @return the final exception
	 */
	public Date getFinalException() {
		return FinalException;
	}

	/**
	 * Sets the final exception.
	 *
	 * @param finalException the new final exception
	 */
	public void setFinalException(Date finalException) {
		FinalException = finalException;
	}

	/**
	 * Gets the user email.
	 *
	 * @return the user email
	 */
	public String getUserEmail() {
		return userEmail;
	}

	/**
	 * Sets the user email.
	 *
	 * @param userEmail the new user email
	 */
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	/**
	 * Gets the user full name.
	 *
	 * @return the user full name
	 */
	public String getUserFullName() {
		return userFullName;
	}

	/**
	 * Sets the user full name.
	 *
	 * @param userFullName the new user full name
	 */
	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the confirmation user.
	 *
	 * @return the confirmation user
	 */
	public String getConfirmationUser() {
		return confirmationUser;
	}

	/**
	 * Sets the confirmation user.
	 *
	 * @param confirmationUser the new confirmation user
	 */
	public void setConfirmationUser(String confirmationUser) {
		this.confirmationUser = confirmationUser;
	}

	/**
	 * Gets the confirmation date.
	 *
	 * @return the confirmation date
	 */
	public Date getConfirmationDate() {
		return confirmationDate;
	}

	/**
	 * Sets the confirmation date.
	 *
	 * @param confirmationDate the new confirmation date
	 */
	public void setConfirmationDate(Date confirmationDate) {
		this.confirmationDate = confirmationDate;
	}

	/**
	 * Gets the last change password date.
	 *
	 * @return the last change password date
	 */
	public Date getLastChangePasswordDate() {
		return lastChangePasswordDate;
	}

	/**
	 * Sets the last change password date.
	 *
	 * @param lastChangePasswordDate the new last change password date
	 */
	public void setLastChangePasswordDate(Date lastChangePasswordDate) {
		this.lastChangePasswordDate = lastChangePasswordDate;
	}

	/**
	 * Gets the last access to system.
	 *
	 * @return the last access to system
	 */
	public Date getLastAccessToSystem() {
		return lastAccessToSystem;
	}

	/**
	 * Sets the last access to system.
	 *
	 * @param lastAccessToSystem the new last access to system
	 */
	public void setLastAccessToSystem(Date lastAccessToSystem) {
		this.lastAccessToSystem = lastAccessToSystem;
	}

	/**
	 * Gets the old value.
	 *
	 * @return the old value
	 */
	public String getOldValue() {
		return oldValue;
	}

	/**
	 * Sets the old value.
	 *
	 * @param oldValue the new old value
	 */
	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}

	/**
	 * Gets the new value.
	 *
	 * @return the new value
	 */
	public String getNewValue() {
		return newValue;
	}

	/**
	 * Sets the new value.
	 *
	 * @param newValue the new new value
	 */
	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}

	/**
	 * Gets the table name.
	 *
	 * @return the table name
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * Sets the table name.
	 *
	 * @param tableName the new table name
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	/**
	 * Gets the field name.
	 *
	 * @return the field name
	 */
	public String getFieldName() {
		return fieldName;
	}

	/**
	 * Sets the field name.
	 *
	 * @param fieldName the new field name
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Integer getSystemId() {
		return systemId;
	}

	public void setSystemId(Integer systemId) {
		this.systemId = systemId;
	}

	public String getAgency() {
		return agency;
	}

	public void setAgency(String agency) {
		this.agency = agency;
	}

	public String getResponsibility() {
		return responsibility;
	}

	public void setResponsibility(String responsibility) {
		this.responsibility = responsibility;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

}
