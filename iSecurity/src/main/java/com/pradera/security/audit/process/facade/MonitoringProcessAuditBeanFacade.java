package com.pradera.security.audit.process.facade;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.pradera.integration.exception.ServiceException;
import com.pradera.security.audit.process.service.MonitoringProcessAuditServiceBean;

/**
 * <ul>
 * <li>
 * Project Pradera. Copyright 2012.</li>
 * </ul>
 * 
 * The Class SystemServiceFacade.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 19/12/2012
 */
@Stateless
public class MonitoringProcessAuditBeanFacade {
	@EJB
	private MonitoringProcessAuditServiceBean auditServiceBean;

	/**
	 * Method for retrieving the System options and Catalogue names from service 
	 * 
	 * 
	 * @param idSystemOptionPK
	 *            Integer
	 * @param lang
	 *            Integer
	 * @param optnFather
	 *            Integer
	 * @return list of object arrays of the id's
	 * @throws ServiceException
	 */
	public List<Object[]> getAuditProcessListFacade(Integer idSystemOptionPK,
			Integer lang, Integer optnFather)throws ServiceException {
		return auditServiceBean.getAuditProcessListService(idSystemOptionPK,
				lang, optnFather);
	}

	/**
	 * Method for getting the Process id options from the service
	 * 
	 * @param integers
	 *            List<Integer>
	 * @return the Iteger list of ids
	 * @throws ServiceException
	 */
	public List<Integer> getAuditProceccesIds(List<Integer> integers) throws ServiceException{
		return auditServiceBean.getAuditProceccesIdsService(integers);
	}

	/**
	 * Method for getting the Process id options from the service
	 * 
	 * @param integers
	 *            List<Integer>
	 * @return the Iteger list of ids
	 * @throws ServiceException
	 */
	public List<Integer> getAuditProcIdsOptns(List<Integer> integers)throws ServiceException {
		return auditServiceBean.getAuditProcIdsOptnsService(integers);
	}

}
