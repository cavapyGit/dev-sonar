package com.pradera.security.audit.report.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.audit.report.facade.ReportAuditServiceFacade;
import com.pradera.security.audit.report.view.to.BeanReportAuditsResultTO;
import com.pradera.security.audit.report.view.to.ExceptionPrivilegeReportFilterTO;
import com.pradera.security.model.InstitutionsSecurity;
import com.pradera.security.model.Parameter;
import com.pradera.security.model.System;
import com.pradera.security.model.type.DefinitionType;
import com.pradera.security.model.type.ParameterTableType;
import com.pradera.security.model.type.UserExceptionSituationType;
import com.pradera.security.model.type.UserExceptionStateType;
import com.pradera.security.systemmgmt.service.ParameterSecurityServiceBean;
import com.pradera.security.usermgmt.service.UserIntegrationServiceBean;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ReportExceptionUserPrivilegeBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 10-mar-2015
 */
@DepositaryWebBean
public class ReportExceptionUserPrivilegeBean extends GenericBaseBean implements
		Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 274856635738366771L;
	
	/** The report audit facade. */
	@EJB
	ReportAuditServiceFacade reportAuditFacade;
	
	/** The parameter security service. */
	@EJB
    ParameterSecurityServiceBean parameterSecurityService;
	
	/** The user integration service. */
	@EJB
	UserIntegrationServiceBean userIntegrationService;
	
	/** The systems. */
	private List<System> systems;
	
	/** The list institutions type. */
	private List<Parameter> listInstitutionsType;
	
	/** The map institutions type. */
	private Map<Integer,String> mapInstitutionsType;
	
	/** The list institutions security. */
	private List<InstitutionsSecurity> listInstitutionsSecurity;
	
	/** The map institutions security. */
	private Map<Integer,String> mapInstitutionsSecurity;
	
	/** The map systems. */
	private Map<Integer,String> mapSystems;
	
	/** The map account states. */
	private Map<Integer,String> mapAccountStates;
	
	/** The map exception states. */
	private Map<Integer,String> mapExceptionStates;
	
	/** The map exception situatios. */
	private Map<Integer,String> mapExceptionSituatios;
	
	/** The map privileges. */
	private Map<Integer,String> mapPrivileges;
	
	/** The exception privilege result. */
	private List<BeanReportAuditsResultTO> exceptionPrivilegeResult;
	
	/** The exception privilege filter. */
	private ExceptionPrivilegeReportFilterTO exceptionPrivilegeFilter;

	/**
	 * Instantiates a new report exception user privilege bean.
	 */
	public ReportExceptionUserPrivilegeBean() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Inits the.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException{
		systems = reportAuditFacade.getAllSystems();
		mapSystems = new HashMap<>();
		for(System sys : systems){
			mapSystems.put(sys.getIdSystemPk(), sys.getName());
		}
		listInstitutionsType = parameterSecurityService.listParameterByDefinitionServiceBean(ParameterTableType.INSITUTIONS_STATE.getCode());
		mapInstitutionsType = new HashMap<>();
		for(Parameter param : listInstitutionsType){
			mapInstitutionsType.put(param.getIdParameterPk(), param.getName());
		}
//		InstitutionSecurityFilter filter = new InstitutionSecurityFilter();
		listInstitutionsSecurity = userIntegrationService.getInstitutionsSecurityServiceBean();
		mapInstitutionsSecurity = new HashMap<>();
		for(InstitutionsSecurity instSecurity : listInstitutionsSecurity){
			mapInstitutionsSecurity.put(instSecurity.getIdInstitutionSecurityPk(), instSecurity.getMnemonicInstitution());
		}
		mapAccountStates = new HashMap<>();
		for(Parameter param :parameterSecurityService.listParameterByDefinitionServiceBean(ParameterTableType.USER_STATES.getCode())){
			mapAccountStates.put(param.getIdParameterPk(), param.getName());
		}
		mapExceptionStates = new HashMap<>();
		for(UserExceptionStateType excep:UserExceptionStateType.list){
			mapExceptionStates.put(excep.getCode(), excep.getValue());
		}
		mapExceptionSituatios = new HashMap<>();
		List<Integer> params =  new ArrayList<>();{}
		params.add(UserExceptionSituationType.ACTIVE.getCode());
		params.add(UserExceptionSituationType.PENDING_DELETE.getCode());
		for(Parameter situation:  parameterSecurityService.listParameterByIdentificator(params)){
			mapExceptionSituatios.put(situation.getIdParameterPk(), situation.getName());
		}
		mapPrivileges = new HashMap<>();
		for(Parameter priv: parameterSecurityService.listParameterByDefinitionServiceBean(DefinitionType.PRIVILEGES_OPTION.getCode())){
			mapPrivileges.put(priv.getIdParameterPk(), priv.getName());
		}
		exceptionPrivilegeFilter = new ExceptionPrivilegeReportFilterTO();
	}
	
	/**
	 * Generate parameters export.
	 *
	 * @return the map
	 */
	public Map<String, Object> generateParametersExport(){
		//using LinkedHashMap for using order of insert in map
		LinkedHashMap<String, Object> parametersExport = new LinkedHashMap<>();
		FacesContext context = FacesContext.getCurrentInstance();
	    ResourceBundle msgs = context.getApplication().evaluateExpressionGet(context, "#{msgs}", ResourceBundle.class);
	    //parameter userName
	    parametersExport.put("Codigo de Usuario", exceptionPrivilegeFilter.getUserName());
	    //parameter empty only for look and feel
	    parametersExport.put("Tipo de institucion",exceptionPrivilegeFilter.getInstitutionType()!=null?mapInstitutionsType.get(exceptionPrivilegeFilter.getInstitutionType()):"");
	    parametersExport.put("Institucion",exceptionPrivilegeFilter.getInstitution()!=null?mapInstitutionsSecurity.get(exceptionPrivilegeFilter.getInstitution()):"");
	    parametersExport.put("Sistema", exceptionPrivilegeFilter.getSystemId()!=null?mapSystems.get(exceptionPrivilegeFilter.getSystemId()):"");
	    parametersExport.put("Feca Inicial", exceptionPrivilegeFilter.getInitialRegistry()!=null?CommonsUtilities.convertDatetoString(exceptionPrivilegeFilter.getInitialRegistry()):"");
	    parametersExport.put("Fecha Final",exceptionPrivilegeFilter.getEndRegistry()!=null?CommonsUtilities.convertDatetoString(exceptionPrivilegeFilter.getEndRegistry()):"");
		return parametersExport;
	}
	
	/**
	 * Search exceptions.
	 */
	@LoggerAuditWeb
	public void searchExceptions(){
		exceptionPrivilegeResult = reportAuditFacade.searchExceptionsPrivilegeUserReport(exceptionPrivilegeFilter);
	}
	
	/**
	 * Clean.
	 */
	@LoggerAuditWeb
	public void clean(){
		exceptionPrivilegeFilter = new ExceptionPrivilegeReportFilterTO();
		if(exceptionPrivilegeResult!=null){
			exceptionPrivilegeResult = null;
		}
	}

	/**
	 * Gets the report audit facade.
	 *
	 * @return the report audit facade
	 */
	public ReportAuditServiceFacade getReportAuditFacade() {
		return reportAuditFacade;
	}

	/**
	 * Sets the report audit facade.
	 *
	 * @param reportAuditFacade the new report audit facade
	 */
	public void setReportAuditFacade(ReportAuditServiceFacade reportAuditFacade) {
		this.reportAuditFacade = reportAuditFacade;
	}

	/**
	 * Gets the parameter security service.
	 *
	 * @return the parameter security service
	 */
	public ParameterSecurityServiceBean getParameterSecurityService() {
		return parameterSecurityService;
	}

	/**
	 * Sets the parameter security service.
	 *
	 * @param parameterSecurityService the new parameter security service
	 */
	public void setParameterSecurityService(
			ParameterSecurityServiceBean parameterSecurityService) {
		this.parameterSecurityService = parameterSecurityService;
	}

	/**
	 * Gets the systems.
	 *
	 * @return the systems
	 */
	public List<System> getSystems() {
		return systems;
	}

	/**
	 * Sets the systems.
	 *
	 * @param systems the new systems
	 */
	public void setSystems(List<System> systems) {
		this.systems = systems;
	}

	/**
	 * Gets the list institutions type.
	 *
	 * @return the list institutions type
	 */
	public List<Parameter> getListInstitutionsType() {
		return listInstitutionsType;
	}

	/**
	 * Sets the list institutions type.
	 *
	 * @param listInstitutionsType the new list institutions type
	 */
	public void setListInstitutionsType(List<Parameter> listInstitutionsType) {
		this.listInstitutionsType = listInstitutionsType;
	}

	/**
	 * Gets the map institutions type.
	 *
	 * @return the map institutions type
	 */
	public Map<Integer, String> getMapInstitutionsType() {
		return mapInstitutionsType;
	}

	/**
	 * Sets the map institutions type.
	 *
	 * @param mapInstitutionsType the map institutions type
	 */
	public void setMapInstitutionsType(Map<Integer, String> mapInstitutionsType) {
		this.mapInstitutionsType = mapInstitutionsType;
	}

	/**
	 * Gets the list institutions security.
	 *
	 * @return the list institutions security
	 */
	public List<InstitutionsSecurity> getListInstitutionsSecurity() {
		return listInstitutionsSecurity;
	}

	/**
	 * Sets the list institutions security.
	 *
	 * @param listInstitutionsSecurity the new list institutions security
	 */
	public void setListInstitutionsSecurity(
			List<InstitutionsSecurity> listInstitutionsSecurity) {
		this.listInstitutionsSecurity = listInstitutionsSecurity;
	}

	/**
	 * Gets the map institutions security.
	 *
	 * @return the map institutions security
	 */
	public Map<Integer, String> getMapInstitutionsSecurity() {
		return mapInstitutionsSecurity;
	}

	/**
	 * Sets the map institutions security.
	 *
	 * @param mapInstitutionsSecurity the map institutions security
	 */
	public void setMapInstitutionsSecurity(
			Map<Integer, String> mapInstitutionsSecurity) {
		this.mapInstitutionsSecurity = mapInstitutionsSecurity;
	}

	/**
	 * Gets the map systems.
	 *
	 * @return the map systems
	 */
	public Map<Integer, String> getMapSystems() {
		return mapSystems;
	}

	/**
	 * Sets the map systems.
	 *
	 * @param mapSystems the map systems
	 */
	public void setMapSystems(Map<Integer, String> mapSystems) {
		this.mapSystems = mapSystems;
	}

	/**
	 * Gets the map account states.
	 *
	 * @return the map account states
	 */
	public Map<Integer, String> getMapAccountStates() {
		return mapAccountStates;
	}

	/**
	 * Sets the map account states.
	 *
	 * @param mapAccountStates the map account states
	 */
	public void setMapAccountStates(Map<Integer, String> mapAccountStates) {
		this.mapAccountStates = mapAccountStates;
	}

	/**
	 * Gets the map exception states.
	 *
	 * @return the map exception states
	 */
	public Map<Integer, String> getMapExceptionStates() {
		return mapExceptionStates;
	}

	/**
	 * Sets the map exception states.
	 *
	 * @param mapExceptionStates the map exception states
	 */
	public void setMapExceptionStates(Map<Integer, String> mapExceptionStates) {
		this.mapExceptionStates = mapExceptionStates;
	}

	/**
	 * Gets the map exception situatios.
	 *
	 * @return the map exception situatios
	 */
	public Map<Integer, String> getMapExceptionSituatios() {
		return mapExceptionSituatios;
	}

	/**
	 * Sets the map exception situatios.
	 *
	 * @param mapExceptionSituatios the map exception situatios
	 */
	public void setMapExceptionSituatios(Map<Integer, String> mapExceptionSituatios) {
		this.mapExceptionSituatios = mapExceptionSituatios;
	}

	/**
	 * Gets the map privileges.
	 *
	 * @return the map privileges
	 */
	public Map<Integer, String> getMapPrivileges() {
		return mapPrivileges;
	}

	/**
	 * Sets the map privileges.
	 *
	 * @param mapPrivileges the map privileges
	 */
	public void setMapPrivileges(Map<Integer, String> mapPrivileges) {
		this.mapPrivileges = mapPrivileges;
	}

	

	/**
	 * Gets the serialversionuid.
	 *
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * Gets the exception privilege filter.
	 *
	 * @return the exception privilege filter
	 */
	public ExceptionPrivilegeReportFilterTO getExceptionPrivilegeFilter() {
		return exceptionPrivilegeFilter;
	}

	/**
	 * Sets the exception privilege filter.
	 *
	 * @param exceptionPrivilegeFilter the new exception privilege filter
	 */
	public void setExceptionPrivilegeFilter(ExceptionPrivilegeReportFilterTO exceptionPrivilegeFilter) {
		this.exceptionPrivilegeFilter = exceptionPrivilegeFilter;
	}

	/**
	 * Gets the exception privilege result.
	 *
	 * @return the exception privilege result
	 */
	public List<BeanReportAuditsResultTO> getExceptionPrivilegeResult() {
		return exceptionPrivilegeResult;
	}

	/**
	 * Sets the exception privilege result.
	 *
	 * @param exceptionPrivilegeResult the new exception privilege result
	 */
	public void setExceptionPrivilegeResult(List<BeanReportAuditsResultTO> exceptionPrivilegeResult) {
		this.exceptionPrivilegeResult = exceptionPrivilegeResult;
	}

}
