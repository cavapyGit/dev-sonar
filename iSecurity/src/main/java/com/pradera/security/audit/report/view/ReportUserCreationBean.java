package com.pradera.security.audit.report.view;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.audit.report.facade.ReportAuditServiceFacade;
import com.pradera.security.audit.report.view.to.BeanReportAuditsResultTO;
import com.pradera.security.audit.report.view.to.UserCreationReportFilterTO;
import com.pradera.security.model.InstitutionsSecurity;
import com.pradera.security.model.Parameter;
import com.pradera.security.model.type.ParameterTableType;
import com.pradera.security.systemmgmt.service.ParameterSecurityServiceBean;
import com.pradera.security.usermgmt.service.UserIntegrationServiceBean;


/**
 * <ul>
 * <li>exceptionPrivilegeResult
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ReportUserCreationBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 10-mar-2015
 */
@DepositaryWebBean
public class ReportUserCreationBean extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4076981418754641399L;
	
	/** The report audit facade. */
	@EJB
	ReportAuditServiceFacade reportAuditFacade;
	
	/** The parameter security service. */
	@EJB
    ParameterSecurityServiceBean parameterSecurityService;
	
	/** The user integration service. */
	@EJB
	UserIntegrationServiceBean userIntegrationService;
	
	/** The list institutions type. */
	private List<Parameter> listInstitutionsType;
	
	/** The map institutions type. */
	private Map<Integer,String> mapInstitutionsType;
	
	/** The list institutions security. */
	private List<InstitutionsSecurity> listInstitutionsSecurity;
	
	/** The map institutions security. */
	private Map<Integer,String> mapInstitutionsSecurity;
	
	/** The map account states. */
	private Map<Integer,String> mapAccountStates;
	
	/** The list account states. */
	private List<Parameter> listAccountStates;
	
	/** The user creation filter. */
	private UserCreationReportFilterTO userCreationFilter;
	
	/** The user creations list result. */
	private List<BeanReportAuditsResultTO> userCreationsListResult;
	
	/**
	 * Inits the.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException{
		listInstitutionsType = parameterSecurityService.listParameterByDefinitionServiceBean(ParameterTableType.INSITUTIONS_STATE.getCode());
		mapInstitutionsType = new HashMap<>();
		for(Parameter param : listInstitutionsType){
			mapInstitutionsType.put(param.getIdParameterPk(), param.getName());
		}
//		InstitutionSecurityFilter filter = new InstitutionSecurityFilter();
		listInstitutionsSecurity = userIntegrationService.getInstitutionsSecurityServiceBean();
		mapInstitutionsSecurity = new HashMap<>();
		for(InstitutionsSecurity instSecurity : listInstitutionsSecurity){
			mapInstitutionsSecurity.put(instSecurity.getIdInstitutionSecurityPk(), instSecurity.getMnemonicInstitution());
		}
		listAccountStates = parameterSecurityService.listParameterByDefinitionServiceBean(ParameterTableType.USER_STATES.getCode());
		mapAccountStates = new HashMap<>();
		for(Parameter param :listAccountStates){
			mapAccountStates.put(param.getIdParameterPk(), param.getName());
		}
		userCreationFilter = new UserCreationReportFilterTO();
		userCreationFilter.setInitialRegistry(CommonsUtilities.currentDate());
		userCreationFilter.setFinalRegistry(CommonsUtilities.currentDate());
	}
	
	public Map<String, Object> generateParametersExport(){
		//using LinkedHashMap for using order of insert in map
		LinkedHashMap<String, Object> parametersExport = new LinkedHashMap<>();
		FacesContext context = FacesContext.getCurrentInstance();
	    ResourceBundle msgs = context.getApplication().evaluateExpressionGet(context, "#{msgs}", ResourceBundle.class);
	    //parameter userName
	    parametersExport.put("Codigo de Usuario", userCreationFilter.getUserName());
	    //parameter empty only for look and feel
	    parametersExport.put("","");
	    parametersExport.put("Tipo de institucion",userCreationFilter.getInstitutionType()!=null?mapInstitutionsType.get(userCreationFilter.getInstitutionType()):"");
	    parametersExport.put("Institucion",userCreationFilter.getInstitution()!=null?mapInstitutionsSecurity.get(userCreationFilter.getInstitution()):"");
	    parametersExport.put("Estado", userCreationFilter.getUserState()!=null?mapAccountStates.get(userCreationFilter.getUserState()):"");
	    parametersExport.put("Nombre Perfil", userCreationFilter.getProfileName());
	    parametersExport.put("Usuario Registrador", userCreationFilter.getRegistryUser());
	    parametersExport.put("Usuario Confirmador", userCreationFilter.getConfirmationUser());
	    parametersExport.put("Feca Inicial", userCreationFilter.getInitialRegistry()!=null?CommonsUtilities.convertDatetoString(userCreationFilter.getInitialRegistry()):"");
	    parametersExport.put("Fecha Final",userCreationFilter.getFinalRegistry()!=null?CommonsUtilities.convertDatetoString(userCreationFilter.getFinalRegistry()):"");
		return parametersExport;
	}
	
	/**
	 * Search exceptions.
	 */
	@LoggerAuditWeb
	public void searchExceptions(){
		userCreationsListResult = reportAuditFacade.searchUserCreationReport(userCreationFilter);
	}
	
	/**
	 * Clean.
	 */
	@LoggerAuditWeb
	public void clean(){
		userCreationFilter = new UserCreationReportFilterTO();
		userCreationFilter.setInitialRegistry(CommonsUtilities.currentDate());
		userCreationFilter.setFinalRegistry(CommonsUtilities.currentDate());
		if(userCreationsListResult!=null){
			userCreationsListResult = null;
		}
	}

	/**
	 * Gets the list institutions type.
	 *
	 * @return the list institutions type
	 */
	public List<Parameter> getListInstitutionsType() {
		return listInstitutionsType;
	}

	/**
	 * Sets the list institutions type.
	 *
	 * @param listInstitutionsType the new list institutions type
	 */
	public void setListInstitutionsType(List<Parameter> listInstitutionsType) {
		this.listInstitutionsType = listInstitutionsType;
	}

	/**
	 * Gets the map institutions type.
	 *
	 * @return the map institutions type
	 */
	public Map<Integer, String> getMapInstitutionsType() {
		return mapInstitutionsType;
	}

	/**
	 * Sets the map institutions type.
	 *
	 * @param mapInstitutionsType the map institutions type
	 */
	public void setMapInstitutionsType(Map<Integer, String> mapInstitutionsType) {
		this.mapInstitutionsType = mapInstitutionsType;
	}

	/**
	 * Gets the list institutions security.
	 *
	 * @return the list institutions security
	 */
	public List<InstitutionsSecurity> getListInstitutionsSecurity() {
		return listInstitutionsSecurity;
	}

	/**
	 * Sets the list institutions security.
	 *
	 * @param listInstitutionsSecurity the new list institutions security
	 */
	public void setListInstitutionsSecurity(
			List<InstitutionsSecurity> listInstitutionsSecurity) {
		this.listInstitutionsSecurity = listInstitutionsSecurity;
	}

	/**
	 * Gets the map institutions security.
	 *
	 * @return the map institutions security
	 */
	public Map<Integer, String> getMapInstitutionsSecurity() {
		return mapInstitutionsSecurity;
	}

	/**
	 * Sets the map institutions security.
	 *
	 * @param mapInstitutionsSecurity the map institutions security
	 */
	public void setMapInstitutionsSecurity(
			Map<Integer, String> mapInstitutionsSecurity) {
		this.mapInstitutionsSecurity = mapInstitutionsSecurity;
	}

	/**
	 * Gets the map account states.
	 *
	 * @return the map account states
	 */
	public Map<Integer, String> getMapAccountStates() {
		return mapAccountStates;
	}

	/**
	 * Sets the map account states.
	 *
	 * @param mapAccountStates the map account states
	 */
	public void setMapAccountStates(Map<Integer, String> mapAccountStates) {
		this.mapAccountStates = mapAccountStates;
	}

	/**
	 * Gets the list account states.
	 *
	 * @return the list account states
	 */
	public List<Parameter> getListAccountStates() {
		return listAccountStates;
	}

	/**
	 * Sets the list account states.
	 *
	 * @param listAccountStates the new list account states
	 */
	public void setListAccountStates(List<Parameter> listAccountStates) {
		this.listAccountStates = listAccountStates;
	}

	/**
	 * Gets the user creation filter.
	 *
	 * @return the user creation filter
	 */
	public UserCreationReportFilterTO getUserCreationFilter() {
		return userCreationFilter;
	}

	/**
	 * Sets the user creation filter.
	 *
	 * @param userCreationFilter the new user creation filter
	 */
	public void setUserCreationFilter(UserCreationReportFilterTO userCreationFilter) {
		this.userCreationFilter = userCreationFilter;
	}

	/**
	 * Gets the user creations list result.
	 *
	 * @return the user creations list result
	 */
	public List<BeanReportAuditsResultTO> getUserCreationsListResult() {
		return userCreationsListResult;
	}

	/**
	 * Sets the user creations list result.
	 *
	 * @param userCreationsListResult the new user creations list result
	 */
	public void setUserCreationsListResult(
			List<BeanReportAuditsResultTO> userCreationsListResult) {
		this.userCreationsListResult = userCreationsListResult;
	}

}
