package com.pradera.security.audit.facade;

import com.pradera.commons.interceptores.Performance;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.audit.service.MonitoringUserAuditService;
import com.pradera.security.audit.view.filters.MonUserAuditSearchFilterBean;
import com.pradera.security.audit.view.filters.MonitoringUserAuditFilterBean;
import com.pradera.security.model.UserProfile;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class MonitoringUserAuditFacade
 * 
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date :
 * @version 1.1
 */
@Stateless
@Performance
public class MonitoringUserAuditFacade implements Serializable {

	/**
	 * added default serial uid
	 */
	private static final long serialVersionUID = 1L;
	@EJB
	private MonitoringUserAuditService monitoringUserAuditService;

	/**
	 * It gives user activities details
	 * 
	 * @param userid
	 *            Integer type
	 * @param langCode
	 *            Integer type
	 * @return List<Object[]> type
	 * @throws ServiceException
	 */
	public List<Object[]> userActivitiesFacade(Integer userid, Integer langCode,Long idUserSessionPk)throws ServiceException {
		return monitoringUserAuditService.userActivitiesService(userid,
				langCode, idUserSessionPk);
	}

	/**
	 * It gives user consultant details
	 * 
	 * @param monitoringUserAudFilter
	 *            MonitoringUserAuditFilterBean type
	 * @return List<MonUserAuditSearchFilterBean> type
	 * @throws ServiceException
	 */
	public List<MonUserAuditSearchFilterBean> serachUserConsultActiviesFacade(
			MonitoringUserAuditFilterBean monitoringUserAudFilter)throws ServiceException {
		List<Object[]> searchResult = monitoringUserAuditService.serachUserConsultActiviesService(monitoringUserAudFilter);
		List<MonUserAuditSearchFilterBean> lstActivitiesSearch = new ArrayList<MonUserAuditSearchFilterBean>();
		MonUserAuditSearchFilterBean searchActivities = null;
		
		for (int i = 0 ; i < searchResult.size(); i++) {			
			searchActivities = new MonUserAuditSearchFilterBean();
			searchActivities.setDate((Date) searchResult.get(i)[0]);//FECHA SESSION
			searchActivities.setPlastModifyDate((Date) searchResult.get(i)[1]);//HORA SESSION
			searchActivities.setSystem(searchResult.get(i)[3].toString());//SISTEMA		
			searchActivities.setBusinessProcess(searchResult.get(i)[5].toString());//PROCESO
			searchActivities.setOption(searchResult.get(i)[4].toString());//OPCION
			searchActivities.setInitialTime((Date)searchResult.get(i)[7]);//HORA INICIO
			if(i==0){//HORA FIN
				searchActivities.setFinalTime((Date) searchResult.get(i)[2]);
			}
			else if((Date)searchResult.get(i)[2] != null && (Date)searchResult.get(i-1)[7] != null &&
					((Date)searchResult.get(i)[2]).compareTo((Date)searchResult.get(i-1)[7]) < 0 ){
				searchActivities.setFinalTime((Date) searchResult.get(i)[2]);
			}else{
				searchActivities.setFinalTime((Date)searchResult.get(i-1)[7]);
			}
			searchActivities.setIpuUicacion(searchResult.get(i)[6].toString());
			lstActivitiesSearch.add(searchActivities);
		}

		return lstActivitiesSearch;
	}

	/**
	 * Search registered profiles by user service facade.
	 * 
	 * @param userProfile
	 *            the user profile
	 * @param id
	 *            Integer type user id
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
	public List<UserProfile> searchRegisteredProfilesByUserServiceFacade(
			UserProfile userProfile, Integer id) throws ServiceException {
		return monitoringUserAuditService
				.searchRegisteredProfilesByUserServiceBean(userProfile, id);
	}
	/**
	 * It checks user exist or not
	 * @param coduser String type
	 * @return true if user exist
	 * @throws ServiceException
	 */
	public boolean inValidUserFacade(String codUser)throws ServiceException{
		boolean userExist = false;
		Long count = monitoringUserAuditService.inValidUserService(codUser);
		if (count == 0l) {
			userExist = true;
		}
		return userExist;
	}
	
	/**
	 * @param monitoringUserAudFilter
	 * @return
	 * @throws ServiceException
	 */
	public List<Object[]> searchAuditUsersServiceFacade(MonitoringUserAuditFilterBean monitoringUserAudFilter,Boolean connectionState)throws ServiceException{
		return monitoringUserAuditService.searchAuditUsersServiceBean(monitoringUserAudFilter,connectionState);
	}
	/**
	 * @param monitoringUserAudFilter
	 * @return
	 * @throws ServiceException
	 */
	public List<Object[]> getLastAccessSystemName(Integer userID,Integer systemID)throws ServiceException{
		return monitoringUserAuditService.getLastAccessSystemNameServiceBean(userID,systemID);
	}
}
