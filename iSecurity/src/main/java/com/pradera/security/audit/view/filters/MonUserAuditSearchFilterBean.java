package com.pradera.security.audit.view.filters;

import java.io.Serializable;
import java.util.Date;

import com.pradera.commons.security.model.type.UserAccountStateType;
import com.pradera.commons.security.model.type.UserSessionStateType;

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class MonUserAuditSearchFilterBean
 * 
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date :
 * @version 1.1
 */
public class MonUserAuditSearchFilterBean implements Serializable {
	/**
	 * added default serail uid
	 */
	private static final long serialVersionUID = 1L;
	private String ploginUser;
	private String pfirtsLastName;
	private String psecondLastName;
	private String pfullName;
	private String pinstitutionName;
	private Integer pcurrentSystem;
	private Date plastLoginDate;
	private Integer pconnectionStatus;
	private String puserType;
	private String pinstitutionType;
	private Integer pidUserPk;
	private String pidPhoneNo;
	private String pidEmail;
	private String sessionTime;
	private String catalogName;
	private String systemName;
	private Date date;
	private Date initialTime;
	private Date finalTime;
	private String system;
	private String option;
	private String businessProcess;
	private String ipuUicacion;
	private String pstate;
	private String pregisterUser;
	private Date pregisterDate;
	private String plastModifyuser;
	private Date plastModifyDate;
	private Integer istate;
	private Long idUserSessionPk;

	/**
	 * @return the ploginUser
	 */
	public String getPloginUser() {
		return ploginUser;
	}

	/**
	 * @param ploginUser
	 *            the ploginUser to set
	 */
	public void setPloginUser(String ploginUser) {
		this.ploginUser = ploginUser;
	}

	/**
	 * @return the pfirtsLastName
	 */
	public String getPfirtsLastName() {
		return pfirtsLastName;
	}

	/**
	 * @param pfirtsLastName
	 *            the pfirtsLastName to set
	 */
	public void setPfirtsLastName(String pfirtsLastName) {
		this.pfirtsLastName = pfirtsLastName;
	}

	/**
	 * @return the psecondLastName
	 */
	public String getPsecondLastName() {
		return psecondLastName;
	}

	/**
	 * @param psecondLastName
	 *            the psecondLastName to set
	 */
	public void setPsecondLastName(String psecondLastName) {
		this.psecondLastName = psecondLastName;
	}

	/**
	 * @return the pfullName
	 */
	public String getPfullName() {
		return pfullName;
	}

	/**
	 * @param pfullName
	 *            the pfullName to set
	 */
	public void setPfullName(String pfullName) {
		this.pfullName = pfullName;
	}

	/**
	 * @return the pinstitutionName
	 */
	public String getPinstitutionName() {
		return pinstitutionName;
	}

	/**
	 * @param pinstitutionName
	 *            the pinstitutionName to set
	 */
	public void setPinstitutionName(String pinstitutionName) {
		this.pinstitutionName = pinstitutionName;
	}

	/**
	 * @return the pcurrentSystem
	 */
	public Integer getPcurrentSystem() {
		return pcurrentSystem;
	}

	/**
	 * @param pcurrentSystem
	 *            the pcurrentSystem to set
	 */
	public void setPcurrentSystem(Integer pcurrentSystem) {
		this.pcurrentSystem = pcurrentSystem;
	}

	/**
	 * @return the plastLoginDate
	 */
	public Date getPlastLoginDate() {
		return plastLoginDate;
	}

	/**
	 * @param plastLoginDate
	 *            the plastLoginDate to set
	 */
	public void setPlastLoginDate(Date plastLoginDate) {
		this.plastLoginDate = plastLoginDate;
	}

	/**
	 * @return the pconnectionStatus
	 */
	public Integer getPconnectionStatus() {
		return pconnectionStatus;
	}

	/**
	 * @param pconnectionStatus
	 *            the pconnectionStatus to set
	 */
	public void setPconnectionStatus(Integer pconnectionStatus) {
		this.pconnectionStatus = pconnectionStatus;
	}

	/**
	 * @return the puserType
	 */
	public String getPuserType() {
		return puserType;
	}

	/**
	 * @param puserType
	 *            the puserType to set
	 */
	public void setPuserType(String puserType) {
		this.puserType = puserType;
	}

	/**
	 * @return the pinstitutionType
	 */
	public String getPinstitutionType() {
		return pinstitutionType;
	}

	/**
	 * @param pinstitutionType
	 *            the pinstitutionType to set
	 */
	public void setPinstitutionType(String pinstitutionType) {
		this.pinstitutionType = pinstitutionType;
	}

	/**
	 * @return the pidUserPk
	 */
	public Integer getPidUserPk() {
		return pidUserPk;
	}

	/**
	 * @param pidUserPk
	 *            the pidUserPk to set
	 */
	public void setPidUserPk(Integer pidUserPk) {
		this.pidUserPk = pidUserPk;
	}

	/**
	 * @return the sessionTime
	 */
	public String getSessionTime() {
		return sessionTime;
	}

	/**
	 * @param sessionTime
	 *            the sessionTime to set
	 */
	public void setSessionTime(String sessionTime) {
		this.sessionTime = sessionTime;
	}

	/**
	 * @return the catalogName
	 */
	public String getCatalogName() {
		return catalogName;
	}

	/**
	 * @param catalogName
	 *            the catalogName to set
	 */
	public void setCatalogName(String catalogName) {
		this.catalogName = catalogName;
	}

	/**
	 * @return the systemName
	 */
	public String getSystemName() {
		return systemName;
	}

	/**
	 * @param systemName
	 *            the systemName to set
	 */
	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}
	
	/**
	 * @return the ipuUicacion
	 */
	public String getIpuUicacion() {
		return ipuUicacion;
	}

	/**
	 * @param ipuUicacion
	 *            the ipuUicacion to set
	 */
	public void setIpuUicacion(String ipuUicacion) {
		this.ipuUicacion = ipuUicacion;
	}

	/**
	 * @return the pidPhoneNo
	 */
	public String getPidPhoneNo() {
		return pidPhoneNo;
	}

	/**
	 * @param pidPhoneNo
	 *            the pidPhoneNo to set
	 */
	public void setPidPhoneNo(String pidPhoneNo) {
		this.pidPhoneNo = pidPhoneNo;
	}

	/**
	 * @return the pidEmail
	 */
	public String getPidEmail() {
		return pidEmail;
	}

	/**
	 * @param pidEmail
	 *            the pidEmail to set
	 */
	public void setPidEmail(String pidEmail) {
		this.pidEmail = pidEmail;
	}

	/**
	 * @return the pstate
	 */
	public String getPstate() {
		return pstate;
	}

	/**
	 * @param pstate
	 *            the pstate to set
	 */
	public void setPstate(String pstate) {
		this.pstate = pstate;
	}

	/**
	 * @return the pregisterUser
	 */
	public String getPregisterUser() {
		return pregisterUser;
	}

	/**
	 * @param pregisterUser
	 *            the pregisterUser to set
	 */
	public void setPregisterUser(String pregisterUser) {
		this.pregisterUser = pregisterUser;
	}

	/**
	 * @return the pregisterDate
	 */
	public Date getPregisterDate() {
		return pregisterDate;
	}

	/**
	 * @param pregisterDate
	 *            the pregisterDate to set
	 */
	public void setPregisterDate(Date pregisterDate) {
		this.pregisterDate = pregisterDate;
	}

	/**
	 * @return the plastModifyuser
	 */
	public String getPlastModifyuser() {
		return plastModifyuser;
	}

	/**
	 * @param plastModifyuser
	 *            the plastModifyuser to set
	 */
	public void setPlastModifyuser(String plastModifyuser) {
		this.plastModifyuser = plastModifyuser;
	}

	/**
	 * @return the plastModifyDate
	 */
	public Date getPlastModifyDate() {
		return plastModifyDate;
	}

	/**
	 * @param plastModifyDate
	 *            the plastModifyDate to set
	 */
	public void setPlastModifyDate(Date plastModifyDate) {
		this.plastModifyDate = plastModifyDate;
	}

	/**
	 * @return the istate
	 */
	public Integer getIstate() {
		return istate;
	}

	/**
	 * @param istate
	 *            the istate to set
	 */
	public void setIstate(Integer istate) {
		this.istate = istate;
	}
	
	  /**
  	 * Gets the state icon.
  	 *
  	 * @return the state icon
  	 */
  	public String getStateIcon() {
			String icono = null;
			if (pconnectionStatus != null && UserSessionStateType.get(pconnectionStatus)!=null) {
				icono = UserSessionStateType.get(pconnectionStatus).getIcon();
			}
			return icono;
		}
		  
	    /**
    	 * Gets the state description.
    	 *
    	 * @return the state description
    	 */
    	public String getStateDescription() {
			String description = null;
			if (pconnectionStatus != null && UserSessionStateType.get(pconnectionStatus)!=null) {
				description = UserSessionStateType.get(pconnectionStatus).getValue();
			}
			return description;
		}

		/**
		 * @return the idUserSessionPk
		 */
		public Long getIdUserSessionPk() {
			return idUserSessionPk;
		}

		/**
		 * @param idUserSessionPk the idUserSessionPk to set
		 */
		public void setIdUserSessionPk(Long idUserSessionPk) {
			this.idUserSessionPk = idUserSessionPk;
		}
		/**
	  	 * Gets the state icon.
	  	 *
	  	 * @return the state icon
	  	 */
	  	public String getUserStateIcon() {
				String icono = null;
				if (istate != null) {
					icono = UserAccountStateType.get(istate).getIcon();
				}
				return icono;
			}

		/**
		 * @return the date
		 */
		public Date getDate() {
			return date;
		}

		/**
		 * @param date the date to set
		 */
		public void setDate(Date date) {
			this.date = date;
		}

		/**
		 * @return the initialTime
		 */
		public Date getInitialTime() {
			return initialTime;
		}

		/**
		 * @param initialTime the initialTime to set
		 */
		public void setInitialTime(Date initialTime) {
			this.initialTime = initialTime;
		}

		/**
		 * @return the finalTime
		 */
		public Date getFinalTime() {
			return finalTime;
		}

		/**
		 * @param finalTime the finalTime to set
		 */
		public void setFinalTime(Date finalTime) {
			this.finalTime = finalTime;
		}

		/**
		 * @return the system
		 */
		public String getSystem() {
			return system;
		}

		/**
		 * @param system the system to set
		 */
		public void setSystem(String system) {
			this.system = system;
		}

		/**
		 * @return the option
		 */
		public String getOption() {
			return option;
		}

		/**
		 * @param option the option to set
		 */
		public void setOption(String option) {
			this.option = option;
		}

		/**
		 * @return the businessProcess
		 */
		public String getBusinessProcess() {
			return businessProcess;
		}

		/**
		 * @param businessProcess the businessProcess to set
		 */
		public void setBusinessProcess(String businessProcess) {
			this.businessProcess = businessProcess;
		}

}
