package com.pradera.security.audit.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import com.pradera.audit.model.AuditTrackFilterBean;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.model.ActionTable;
import com.pradera.security.model.ActionTableSql;
import com.pradera.security.model.DetailAction;
import com.pradera.security.model.System;
import com.pradera.security.model.type.ActionTableSqlStateType;
import com.pradera.security.model.type.AuditTableStateType;
import com.pradera.security.systemmgmt.service.CrudSecurityServiceBean;

/**
 * <ul>
 * <li>Copyright 2013 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class AuditsensitiveFieldServiceBean
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class AuditsensitiveFieldServiceBean extends CrudSecurityServiceBean {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	/** the logger. */
	private static final  Logger log = Logger.getLogger(AuditsensitiveFieldServiceBean.class.getName());

	private static final String LAST_MODIFY_APP_COLUMN = "LAST_MODIFY_APP";
	private static final String LAST_MODIFY_PRIV_COLUMN = "LAST_MODIFY_PRIV";
	private static final String LAST_MODIFY_IP_COLUMN = "LAST_MODIFY_IP";
	private static final String LAST_MODIFY_USER_COLUMN = "LAST_MODIFY_USER";
	/**
	 * Search rules with triggers.
	 *
	 * @return the list
	 */
	public List<ActionTable> searchRulesWithTriggers(){
		Query query=em.createNativeQuery("select distinct at.ID_SYSTEM_FK,at.TABLE_NAME from ACTION_TABLE at where at.STATE in (:states)");
		List<Integer> states = new ArrayList<>();
		states.add(AuditTableStateType.CONFIRMED.getCode());
		states.add(AuditTableStateType.DISABLED.getCode());
		query.setParameter("states", states);
		List<Object[]> rows=query.getResultList();
		List<ActionTable> actions = new ArrayList<>();
		for(Object[] row : rows){
			ActionTable table = new ActionTable();
			table.setTableName(row[1].toString());
			System s = new System();
			s.setIdSystemPk(Integer.valueOf(row[0].toString()));
			table.setSystem(s);
			actions.add(table);
		}
		return actions;
	}

	/**
	 * used to get the table names by System.
	 *
	 * @param systemName the system name
	 * @return list of strings List<String>
	 * @throws ServiceException the service exception
	 */
	public List<String> getTableListServiceBean(String systemName) throws ServiceException {
		
		StringBuffer query = new StringBuffer("select distinct table_name from dba_tables where owner="+ "\'" + systemName + "\'");
		List<String> tablesList=findByNativeQuery(query.toString());
		return tablesList;
		
	}

	/**
	 * used to get the user names by System.
	 *
	 * @return list of strings List<String>
	 * @throws ServiceException the service exception
	 */
	public List<String> getUsersListServiceBean() throws ServiceException {
		
		StringBuffer query = new StringBuffer(
				"select distinct loginUser from UserAccount order by loginUser");
		return (List<String>)findByQueryString(query.toString());
		
	}

	/**
	 * used to get the fields by table.
	 *
	 * @param tableName            String
	 * @param schemaName the schema name
	 * @return string list ArrayList<String>
	 * @throws ServiceException the service exception
	 */
	public List<String> getColumnListServiceBean(String tableName,String schemaName)
			throws ServiceException {
		
		/*StringBuffer query = new StringBuffer(
				"select distinct column_name from user_tab_columns where table_name="
						+ "\'" + tableName + "\'");*/
		log.info("-------------------------------------------");
		log.info("Selected SchemanName-------"+schemaName);
		log.info("Selected TableName-------"+tableName);
		StringBuffer query = new StringBuffer(
				"SELECT distinct column_name FROM dba_tab_columns WHERE OWNER=" +"\'"+schemaName+"\'" +
										"AND TABLE_NAME="+ "\'" + tableName + "\'");
		return (List<String>)findByNativeQuery(query.toString());
		
		
	}

	/**
	 * used to save  the ActionTable.
	 *
	 * @param actionTable            ActionTable
	 * @throws ServiceException the service exception
	 */
	public void saveActionTableServiceBean(ActionTable actionTable) throws ServiceException {
	
		createOnCascade(actionTable);
	}
	
	/**
	 * used to  update the ActionTable.
	 *
	 * @param actionTable            ActionTable
	 * @throws ServiceException the service exception
	 */
	public void updateActionTableServiceBean(ActionTable actionTable) throws ServiceException {
		   
		update(actionTable);
    }

	/**
	 * used to get the ActionTable List by system state.
	 *
	 * @param state            Integer
	 * @return list List<ActionTable>
	 * @throws ServiceException the service exception
	 */
	public List<ActionTable> searchActionTableBySystemServiceBean(Integer state)
			throws ServiceException {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idSystemPk", state);
		return (List<ActionTable>) findWithNamedQuery(
				ActionTable.ACTION_TABLE_SEARCH_BY_SYSTEM, parameters);
	}

	/**
	 * used to get the DetaiAction Table list.
	 *
	 * @param actionPk            long
	 * @return object array List<Object[]>
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getDetaiActionByActionPkServiceBean(
			long actionPk) throws ServiceException {
		List<Object[]> objectArray = new ArrayList<Object[]>();
		Map<String, Object> parameter = new HashMap<String, Object>();
		parameter.put("idActionTablesPk", actionPk);
		objectArray = (List<Object[]>) findWithNamedQuery(
				DetailAction.DETAIL_ACTION_TABLE_SEARCH_BY_ACTION_TABLE_PK,
				parameter);
		return objectArray; 
	}

	/**
	 * used to delete the DetailAction by ActionTablePK.
	 *
	 * @param detailActionFk            long
	 * @return true boolean
	 * @throws ServiceException the service exception
	 */
	public boolean deleteDetaiActionByActionPkServiceBean(long detailActionFk)
			throws ServiceException {
		deleteByFk(
				"delete from DetailAction da where da.actionTable.idActionTablesPk = ?",
				detailActionFk);
		return true;
	}

	/**
	 * Execute insert trigger service bean.
	 *
	 * @param action the action
	 * @throws ServiceException the service exception
	 */
	public String executeInsertTriggerServiceBean(ActionTable action) {
		String strRule = action.getRuleName().replace(GeneralConstants.BLANK_SPACE, GeneralConstants.UNDERLINE_SEPARATOR);
		if(strRule.length() > 24) {
			strRule = strRule.substring(0,24);
		}
		String query = "DROP TRIGGER "+action.getSystem().getSchemaDb()+"."+strRule+"_TRG_I";
		log.info(query);
		//executeStatement(query);
		
		return query;
	}
	
	/**
	 * Execute update trigger service bean.
	 *
	 * @param action the action
	 * @throws ServiceException the service exception
	 */
	public String executeUpdateTriggerServiceBean(ActionTable action) {
		String strRule = action.getRuleName().replace(GeneralConstants.BLANK_SPACE, GeneralConstants.UNDERLINE_SEPARATOR);
		if(strRule.length() > 24) {
			strRule = strRule.substring(0,24);
		}
		String query = "DROP TRIGGER "+action.getSystem().getSchemaDb()+"."+strRule+"_TRG_U";
		//executeStatement(query);
		
		return query;
	}
	
	/**
	 * Enable triggers rule.
	 *
	 * @param action the action
	 * @throws ServiceException the service exception
	 */
	public String enableTriggersRuleInsert(ActionTable action) throws ServiceException{
		String strRule = action.getRuleName().replace(GeneralConstants.BLANK_SPACE, GeneralConstants.UNDERLINE_SEPARATOR);
		if(strRule.length() > 24) {
			strRule = strRule.substring(0,24);
		}
		//to insert
		String queryOne = "ALTER TRIGGER "+action.getSystem().getSchemaDb()+"."+strRule+"_TRG_I ENABLE";
		//executeStatement(query);
		return queryOne;
	}
	
	public String enableTriggersRuleUpdate(ActionTable action) throws ServiceException{
		String strRule = action.getRuleName().replace(GeneralConstants.BLANK_SPACE, GeneralConstants.UNDERLINE_SEPARATOR);
		if(strRule.length() > 24) {
			strRule = strRule.substring(0,24);
		}
		//to update
		String queryTwo = "ALTER TRIGGER "+action.getSystem().getSchemaDb()+"."+strRule+"_TRG_U ENABLE";
		//executeStatement(query);
		return queryTwo;
	}
	
	/**
	 * Delete update insert trigger rules.
	 *
	 * @param action the action
	 * @throws ServiceException the service exception
	 */
	public String deleteUpdateInsertTriggerRules(ActionTable action) {
		String sql1 = executeInsertTriggerServiceBean(action);
		String sql2 = executeUpdateTriggerServiceBean(action);
		
		return sql1.concat(sql2);
	}

	/**
	 * used to get the actionTables.
	 *
	 * @param auditTrackFilterBean the audit track filter bean
	 * @return list of object array List<Object[]>
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getActionTableFromAuditLogServiceBean(
			AuditTrackFilterBean auditTrackFilterBean) throws ServiceException {
	
		List<Object[]> objects = new ArrayList<Object[]>();
		StringBuffer nativeQuery = new StringBuffer(
				"select at.ID_ACTION_TABLES_PK,at.TABLE_NAME,at.RULE_NAME,at.REGISTRY_DATE,at.INITIAL_DATE,at.END_DATE,at.STATE, atf.OLD_VALUE,atf.NEW_VALUE,atl.USER_REGISTER,atf.NAME_FIELD,at.ID_SYSTEM_FK"
						+ " from SEGURIDAD.ACTION_TABLE at,"
						+ auditTrackFilterBean.getSchemaName()
						+ "."
						+ "AUDIT_TRACK_FIELD atf,"
						+ auditTrackFilterBean.getSchemaName()
						+ "."
						+ "AUDIT_TRACK_LOG atl where at.ID_SYSTEM_FK = "
						+ auditTrackFilterBean.getSystemSelected()
						+ " and atl.ID_SYSTEM_FK = "
						+ auditTrackFilterBean.getSystemSelected()
						+ " and atl.ID_AUDIT_TRACK_PK = atf.ID_AUDIT_TRACK_FK");
		// " and at.TABLE_NAME = atl.NAME_TABLE ");
		if (auditTrackFilterBean.getTableName() != null)
			nativeQuery.append(" and at.TABLE_NAME = '"
					+ auditTrackFilterBean.getTableName() + "'"
					+ " and atl.NAME_TABLE = '"
					+ auditTrackFilterBean.getTableName() + "'");
		if (auditTrackFilterBean.getFieldName() != null)
			nativeQuery.append(" and atf.NAME_FIELD = '"
					+ auditTrackFilterBean.getFieldName() + "'");
		if (auditTrackFilterBean.getUser() != null)
			nativeQuery.append(" and atl.USER_REGISTER = '"
					+ auditTrackFilterBean.getUser() + "'");
		if (auditTrackFilterBean.getInitialDate() != null)
			nativeQuery.append(" and at.REGISTRY_DATE >= to_date('"
					+ auditTrackFilterBean.getInitialDate() + "')");
		if (auditTrackFilterBean.getFinalDate() != null)
			nativeQuery.append(" and at.REGISTRY_DATE <= to_date('"
					+ auditTrackFilterBean.getFinalDate() + "')");
		objects = (List<Object[]>)findByNativeQuery(nativeQuery.toString());
		return objects;
	}

	/**
	 * To get the search Results.
	 *
	 * @param auditTrackFilterBean the audit track filter bean
	 * @return list
	 * @throws ServiceException the service exception
	 */
	public List<ActionTable> searchAutionTableByAuditFilterServiceBean(
			AuditTrackFilterBean auditTrackFilterBean) throws ServiceException {

		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append("select at from ActionTable at left join fetch at.system ");
		sbQuery.append("where 1=1 ");

		if (Validations.validateIsNotNullAndNotEmpty(auditTrackFilterBean
				.getSystemSelected())) {
			sbQuery.append(" and at.system.idSystemPk=:idSystemPk");
		}
		if (Validations.validateIsNotNullAndNotEmpty(auditTrackFilterBean
				.getTableName())) {
			sbQuery.append(" and at.tableName=:tableName");
		}
		if (Validations.validateIsNotNullAndNotEmpty(auditTrackFilterBean
				.getUser())) {
			sbQuery.append(" and at.lastModifyUser=:lastModifyUser");
		}
		if (Validations.validateIsNotNullAndNotEmpty(auditTrackFilterBean
				.getInitialDate())) {
			sbQuery.append(" and at.registryDate >=:initialDate");
		}
		if (Validations.validateIsNotNullAndNotEmpty(auditTrackFilterBean
				.getFinalDate())) {
			sbQuery.append(" and at.registryDate <=:finalDate");
		}
		if (Validations.validateIsNotNullAndNotEmpty(auditTrackFilterBean
				.getState())) {
			sbQuery.append(" and at.state = :state");
		}

		sbQuery.append(" order by at.idActionTablesPk");
		Query query = getEm().createQuery(sbQuery.toString());

		if (Validations.validateIsNotNullAndNotEmpty(auditTrackFilterBean
				.getSystemSelected())) {
			query.setParameter("idSystemPk",
					auditTrackFilterBean.getSystemSelected());
		}
		if (Validations.validateIsNotNullAndNotEmpty(auditTrackFilterBean
				.getTableName())) {
			query.setParameter("tableName", auditTrackFilterBean.getTableName());
		}
		if (Validations.validateIsNotNullAndNotEmpty(auditTrackFilterBean
				.getUser())) {
			query.setParameter("lastModifyUser", auditTrackFilterBean.getUser());
		}
		if (Validations.validateIsNotNullAndNotEmpty(auditTrackFilterBean
				.getInitialDate())) {
			query.setParameter("initialDate",
					auditTrackFilterBean.getInitialDate());
		}
		if (Validations.validateIsNotNullAndNotEmpty(auditTrackFilterBean
				.getFinalDate())) {
			query.setParameter("finalDate", auditTrackFilterBean.getFinalDate());
		}
		if (Validations.validateIsNotNullAndNotEmpty(auditTrackFilterBean
				.getState())) {
			query.setParameter("state", auditTrackFilterBean.getState());
		}
		query.setHint("org.hibernate.cacheable", true);
		return query.getResultList();
	}

	/**
	 * Creates the and activate trigger insert audit service bean.
	 *
	 * @param system the system
	 * @param actionTable the action table
	 * @param lstColumnsPrimaryKey the lst columns primary key
	 * @return true if success
	 * @throws ServiceException the service exception
	 */
	public String createAndActivateTriggerInsertAuditServiceBean(System system,
			ActionTable actionTable, List<String> lstColumnsPrimaryKey)throws ServiceException {
		String columnLasModApp =  existsColumn(system.getSchemaDb(), actionTable.getTableName(), LAST_MODIFY_APP_COLUMN) ? 
				":NEW." + LAST_MODIFY_APP_COLUMN : existsColumn(system.getSchemaDb(), actionTable.getTableName(), LAST_MODIFY_PRIV_COLUMN) ?
								":NEW." + LAST_MODIFY_PRIV_COLUMN : "NULL";

		String columnLastModIP = existsColumn(system.getSchemaDb(), actionTable.getTableName(), LAST_MODIFY_IP_COLUMN) ?
						":NEW." + LAST_MODIFY_IP_COLUMN : "127.0.0.1";
		
		String columnLastModUser = existsColumn(system.getSchemaDb(), actionTable.getTableName(), LAST_MODIFY_IP_COLUMN) ?
						":NEW." + LAST_MODIFY_USER_COLUMN : "TRIGGER";
		
		String strRule = actionTable.getRuleName().replace(GeneralConstants.BLANK_SPACE, GeneralConstants.UNDERLINE_SEPARATOR);
		if(strRule.length() > 24) {
			strRule = strRule.substring(0,24);
		}
		
		StringBuffer triggerI = new StringBuffer();
		StringBuffer detailTriggerI = new StringBuffer();
		triggerI.append("CREATE OR REPLACE TRIGGER ");
		triggerI.append(system.getSchemaDb()+"."+strRule+"_TRG_I");
		triggerI.append(" AFTER INSERT ON "+system.getSchemaDb()+"."+actionTable.getTableName());
		triggerI.append(" REFERENCING NEW AS NEW OLD AS OLD FOR EACH ROW ");
		triggerI.append(" DISABLE ");
		triggerI.append(" DECLARE audit_log_reference Number;");
		
		triggerI.append(" BEGIN ");
		
		List<String> tempLstCols = new ArrayList<String>();
		String prefix = "to_char(:NEW.";
		String sufix = ")";
		for (String columnPk : lstColumnsPrimaryKey) {
			tempLstCols.add(prefix.concat(columnPk).concat(sufix));
		}
		
		String v_primary_key = String.join(",", tempLstCols);
		
		triggerI.append(" select SQ_ID_AUDIT_TRACK_PK.NEXTVAL into audit_log_reference from dual;");
		triggerI.append(" Insert into AUDIT_TRACK_LOG(ID_AUDIT_TRACK_PK,ID_SYSTEM_FK,NAME_TABLE,REGISTRY_DATE,USER_REGISTER,LAST_MODIFY_APP,LAST_MODIFY_IP,LAST_MODIFY_USER,LAST_MODIFY_DATE,EVENT_TYPE,NAME_TRIGGER,PRIMARY_KEY_ROW) values ( ");
		triggerI.append("  	audit_log_reference"+","+system.getIdSystemPk()+","+"'"+actionTable.getTableName()+"'"+"," );
		triggerI.append("	sysdate," + columnLastModUser +",");
		triggerI.append( columnLasModApp +","+columnLastModIP+"," + columnLastModUser+ ",sysdate,1,'" + system.getSchemaDb()+"."+strRule+"_TRG_I"+  "'," + v_primary_key + ");");
		
		List<DetailAction>detailActions = actionTable.getDetailActions();
		for (DetailAction detailActionobj : detailActions) {
			String field = detailActionobj.getFieldName();
			if(detailActionobj.getIsInsertable() == 1){
				detailTriggerI.append("Insert into AUDIT_TRACK_FIELD(ID_AUDIT_TRACK_FIELD_PK,ID_AUDIT_TRACK_FK,NAME_FIELD,OLD_VALUE,NEW_VALUE,LAST_MODIFY_APP,LAST_MODIFY_IP,LAST_MODIFY_USER,LAST_MODIFY_DATE) values ("+
					"SQ_ID_AUDIT_TRACK_FIELD_PK.NEXTVAL"+","+"audit_log_reference"+","+"'"+detailActionobj.getFieldName()+"'"+","
					+"null, :NEW."+field+","+ columnLasModApp +"," +columnLastModIP+ "," + columnLastModUser +",SYSDATE);");
			}
		}
		triggerI.append(detailTriggerI+" "+"END;");
		log.info(triggerI.toString());
		//executeStatement(triggerI.toString());
		return triggerI.toString();
	}
	
	/**
	 * Creates the and activate trigger update audit service bean.
	 *
	 * @param system the system
	 * @param actionTable the action table
	 * @param lstColumnsPrimaryKey the lst columns primary key
	 * @return true if success
	 * @throws ServiceException the service exception
	 */
	
	public String createAndActivateTriggerUpdateAuditServiceBean(System system,
			ActionTable actionTable, List<String> lstColumnsPrimaryKey)throws ServiceException {	
		String columnLasModApp =  existsColumn(system.getSchemaDb(), actionTable.getTableName(), LAST_MODIFY_APP_COLUMN) ? 
									":NEW." + LAST_MODIFY_APP_COLUMN : existsColumn(system.getSchemaDb(), actionTable.getTableName(), LAST_MODIFY_PRIV_COLUMN) ?
													":NEW." + LAST_MODIFY_PRIV_COLUMN : "NULL";
		
		String columnLastModIP = existsColumn(system.getSchemaDb(), actionTable.getTableName(), LAST_MODIFY_IP_COLUMN) ?
									":NEW." + LAST_MODIFY_IP_COLUMN : "127.0.0.1";
		
		String columnLastModUser = existsColumn(system.getSchemaDb(), actionTable.getTableName(), LAST_MODIFY_IP_COLUMN) ?
				":NEW." + LAST_MODIFY_USER_COLUMN : "TRIGGER";
		
		String strRule = actionTable.getRuleName().replace(GeneralConstants.BLANK_SPACE, GeneralConstants.UNDERLINE_SEPARATOR);
		if(strRule.length() > 24) {
			strRule = strRule.substring(0,24);
		}
		
		StringBuffer triggerU = new StringBuffer();
		StringBuffer detailTriggerU = new StringBuffer();
		triggerU = new StringBuffer("CREATE OR REPLACE TRIGGER "
				+system.getSchemaDb()+"."+strRule+"_TRG_U"+
				" AFTER UPDATE ON "+system.getSchemaDb()+"."+actionTable.getTableName()+
				" REFERENCING NEW AS NEW OLD AS OLD FOR EACH ROW "
				+ " DISABLE "
				+ " DECLARE"
				+ " audit_log_reference Number;");
		
		triggerU.append(" BEGIN");
		
		List<String> tempLstCols = new ArrayList<String>();
		String prefix = "to_char(:NEW.";
		String sufix = ")";
		for (String columnPk : lstColumnsPrimaryKey) {
			tempLstCols.add(prefix.concat(columnPk).concat(sufix));
		}
		
		String v_primary_key = String.join(",", tempLstCols);

		
		triggerU.append(" select SQ_ID_AUDIT_TRACK_PK.NEXTVAL into audit_log_reference from dual;"
				+ " Insert into AUDIT_TRACK_LOG(ID_AUDIT_TRACK_PK,ID_SYSTEM_FK,NAME_TABLE,REGISTRY_DATE,USER_REGISTER,LAST_MODIFY_APP,LAST_MODIFY_IP,LAST_MODIFY_USER,LAST_MODIFY_DATE,EVENT_TYPE,NAME_TRIGGER,PRIMARY_KEY_ROW) values ("
				+ "audit_log_reference"+","+system.getIdSystemPk()+","+"'"+actionTable.getTableName()+"'"+","
				+"sysdate"+"," + columnLastModUser +","
				+ columnLasModApp +"," + columnLastModIP + ","+columnLastModUser+",sysdate, 2,'" + system.getSchemaDb()+"."+strRule+"_TRG_U"+  "'," + v_primary_key + ");");
		List<DetailAction>detailActions = actionTable.getDetailActions();
		for (DetailAction detailActionobj : detailActions) {
			String field = detailActionobj.getFieldName();
			if(detailActionobj.getIsUpdatable() == 1){
				detailTriggerU.append("Insert into AUDIT_TRACK_FIELD(ID_AUDIT_TRACK_FIELD_PK,ID_AUDIT_TRACK_FK,NAME_FIELD,OLD_VALUE,NEW_VALUE,LAST_MODIFY_APP,LAST_MODIFY_IP,LAST_MODIFY_USER,LAST_MODIFY_DATE) values ("+
						"SQ_ID_AUDIT_TRACK_FIELD_PK.NEXTVAL"+","+"audit_log_reference"+","+"'"+detailActionobj.getFieldName()+"'"+
						",:OLD."+field+","+":NEW."+field+","+ columnLasModApp +"," + columnLastModIP + "," +columnLastModUser +",SYSDATE);");
			}
		}
		triggerU.append(detailTriggerU+" "+"END;");
		log.info(triggerU.toString());
		//executeStatement(triggerU.toString());
		return triggerU.toString();
	}
	
	/**
	 * Exists last modify app column.
	 *
	 * @param schemaDb the schema db
	 * @param tableName the table name
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean existsColumn(String schemaDb, String tableName, String columnName) throws ServiceException{
		
		StringBuilder sbQuery = new StringBuilder("Select count(*) From ");
		sbQuery.append(" dba_tab_columns where owner = :ownerPrm and table_name = :tablePrm and column_name = :columnPrm ");
		
		Query query = em.createNativeQuery(sbQuery.toString());
		
		query.setParameter("ownerPrm", schemaDb);
		query.setParameter("tablePrm", tableName);
		query.setParameter("columnPrm", columnName);
		
		Integer countResult = Integer.parseInt(query.getSingleResult().toString());
		
		return countResult > 0;
	}
	
	public Boolean seachExecutedSqls(ActionTable filter) {
		try {
			StringBuilder sb = new StringBuilder();
			sb.append(" SELECT Count(*) FROM ActionTableSql ats");		
			sb.append(" WHERE ats.actionTable.idActionTablesPk = :idActionTablesPk");
			sb.append(" 	AND ats.state = :stateParam");
			
			Query q = em.createQuery(sb.toString());
			
			q.setParameter("idActionTablesPk", filter.getIdActionTablesPk());
			q.setParameter("stateParam", ActionTableSqlStateType.NOT_EXECUTED.getCode());			
	
			Integer countResult = Integer.parseInt(q.getSingleResult().toString());
			
			return countResult > 0;
		} catch (NoResultException e) {
			return Boolean.FALSE;
		}
	}
	
	public List<ActionTableSql> getAllSqlsOfAction(ActionTable filter){
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT ats From ActionTableSql ats ");
		sb.append(" WHERE ats.actionTable.idActionTablesPk = :idActionTablesPk");
		sb.append(" 	AND ats.state = :stateParam");
		
		Query q = em.createQuery(sb.toString());
		q.setParameter("idActionTablesPk", filter.getIdActionTablesPk());
		q.setParameter("stateParam", ActionTableSqlStateType.NOT_EXECUTED.getCode());
		
		return (List<ActionTableSql>) q.getResultList();
	}
}
