package com.pradera.security.audit.view.filters;

import java.io.Serializable;

import javax.ejb.Stateless;

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class MonitoringUserAuditFilterBean
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.1
 */
@Stateless
public class MonitoringUserAuditFilterBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer userTypeId;
	private Integer entityTypeSeletectedParticipantPk;
	private Integer entityTypeSeletectedEmisorPk;
	private Integer entitySeletectedPk;
	private Integer userSeletectedPk;
	private Integer language;
	private String initialDate;
	private String finalDate;
	private Integer iduserpk;
	private Integer entityTypeSeletected;
	private Integer selectedStateConnection;
	private String userCode;
	private Integer stateSelected;
	private Integer institution;
	/**
	 * @return the userTypeId
	 */
	public Integer getUserTypeId() {
		return userTypeId;
	}

	/**
	 * @param userTypeId
	 *            the userTypeId to set
	 */
	public void setUserTypeId(Integer userTypeId) {
		this.userTypeId = userTypeId;
	}

	/**
	 * @return the entityTypeSeletectedParticipantPk
	 */
	public Integer getEntityTypeSeletectedParticipantPk() {
		return entityTypeSeletectedParticipantPk;
	}

	/**
	 * @param entityTypeSeletectedParticipantPk
	 *            the entityTypeSeletectedParticipantPk to set
	 */
	public void setEntityTypeSeletectedParticipantPk(
			Integer entityTypeSeletectedParticipantPk) {
		this.entityTypeSeletectedParticipantPk = entityTypeSeletectedParticipantPk;
	}

	/**
	 * @return the entityTypeSeletectedEmisorPk
	 */
	public Integer getEntityTypeSeletectedEmisorPk() {
		return entityTypeSeletectedEmisorPk;
	}

	/**
	 * @param entityTypeSeletectedEmisorPk
	 *            the entityTypeSeletectedEmisorPk to set
	 */
	public void setEntityTypeSeletectedEmisorPk(
			Integer entityTypeSeletectedEmisorPk) {
		this.entityTypeSeletectedEmisorPk = entityTypeSeletectedEmisorPk;
	}

	/**
	 * @return the entitySeletectedPk
	 */
	public Integer getEntitySeletectedPk() {
		return entitySeletectedPk;
	}

	/**
	 * @param entitySeletectedPk
	 *            the entitySeletectedPk to set
	 */
	public void setEntitySeletectedPk(Integer entitySeletectedPk) {
		this.entitySeletectedPk = entitySeletectedPk;
	}

	/**
	 * @return the userSeletectedPk
	 */
	public Integer getUserSeletectedPk() {
		return userSeletectedPk;
	}

	/**
	 * @param userSeletectedPk
	 *            the userSeletectedPk to set
	 */
	public void setUserSeletectedPk(Integer userSeletectedPk) {
		this.userSeletectedPk = userSeletectedPk;
	}

	/**
	 * @return the language
	 */
	public Integer getLanguage() {
		return language;
	}

	/**
	 * @param language
	 *            the language to set
	 */
	public void setLanguage(Integer language) {
		this.language = language;
	}


	/**
	 * @return the iduserpk
	 */
	public Integer getIduserpk() {
		return iduserpk;
	}

	/**
	 * @param iduserpk
	 *            the iduserpk to set
	 */
	public void setIduserpk(Integer iduserpk) {
		this.iduserpk = iduserpk;
	}

	/**
	 * @return the entityTypeSeletected
	 */
	public Integer getEntityTypeSeletected() {
		return entityTypeSeletected;
	}

	/**
	 * @param entityTypeSeletected the entityTypeSeletected to set
	 */
	public void setEntityTypeSeletected(Integer entityTypeSeletected) {
		this.entityTypeSeletected = entityTypeSeletected;
	}

	/**
	 * @return the selectedStateConnection
	 */
	public Integer getSelectedStateConnection() {
		return selectedStateConnection;
	}

	/**
	 * @param selectedStateConnection the selectedStateConnection to set
	 */
	public void setSelectedStateConnection(Integer selectedStateConnection) {
		this.selectedStateConnection = selectedStateConnection;
	}

	/**
	 * @return the userCode
	 */
	public String getUserCode() {
		return userCode;
	}

	/**
	 * @param userCode the userCode to set
	 */
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	/**
	 * @return the stateSelected
	 */
	public Integer getStateSelected() {
		return stateSelected;
	}

	/**
	 * @param stateSelected the stateSelected to set
	 */
	public void setStateSelected(Integer stateSelected) {
		this.stateSelected = stateSelected;
	}

	/**
	 * @return the institution
	 */
	public Integer getInstitution() {
		return institution;
	}

	/**
	 * @param institution the institution to set
	 */
	public void setInstitution(Integer institution) {
		this.institution = institution;
	}

	/**
	 * @return the initialDate
	 */
	public String getInitialDate() {
		return initialDate;
	}

	/**
	 * @param initialDate the initialDate to set
	 */
	public void setInitialDate(String initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * @return the finalDate
	 */
	public String getFinalDate() {
		return finalDate;
	}

	/**
	 * @param finalDate the finalDate to set
	 */
	public void setFinalDate(String finalDate) {
		this.finalDate = finalDate;
	}

	
}
