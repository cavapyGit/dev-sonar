package com.pradera.security.audit.report.view.to;

import java.io.Serializable;
import java.util.Date;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AuditFieldsChangeFilterTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 10-mar-2015
 */
public class AuditFieldsChangeFilterTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8774293857332329673L;
	
	/** The system id. */
	private Integer systemId;
	
	/** The table name. */
	private String tableName;
	
	/** The initial registry. */
	private Date initialRegistry;
	
	/** The final registry. */
	private Date finalRegistry;

	/**
	 * Gets the system id.
	 *
	 * @return the system id
	 */
	public Integer getSystemId() {
		return systemId;
	}

	/**
	 * Sets the system id.
	 *
	 * @param systemId the new system id
	 */
	public void setSystemId(Integer systemId) {
		this.systemId = systemId;
	}

	/**
	 * Gets the table name.
	 *
	 * @return the table name
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * Sets the table name.
	 *
	 * @param tableName the new table name
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	/**
	 * Gets the initial registry.
	 *
	 * @return the initial registry
	 */
	public Date getInitialRegistry() {
		return initialRegistry;
	}

	/**
	 * Sets the initial registry.
	 *
	 * @param initialRegistry the new initial registry
	 */
	public void setInitialRegistry(Date initialRegistry) {
		this.initialRegistry = initialRegistry;
	}

	/**
	 * Gets the final registry.
	 *
	 * @return the final registry
	 */
	public Date getFinalRegistry() {
		return finalRegistry;
	}

	/**
	 * Sets the final registry.
	 *
	 * @param finalRegistry the new final registry
	 */
	public void setFinalRegistry(Date finalRegistry) {
		this.finalRegistry = finalRegistry;
	}

}
