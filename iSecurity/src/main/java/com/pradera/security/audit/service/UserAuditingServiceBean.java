package com.pradera.security.audit.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.pradera.commons.security.model.type.LogoutMotiveType;
import com.pradera.commons.security.model.type.UserSessionStateType;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.AESKeyType;
import com.pradera.commons.utils.AESEncryptUtils;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.model.type.SystemProfileStateType;
import com.pradera.security.model.type.SystemType;
import com.pradera.security.model.type.UserProfileStateType;
import com.pradera.security.systemmgmt.service.CrudSecurityServiceBean;

/**
 * <ul>
 * <li>
 * Project PraderaSecurity
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Class UserAuditingServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 10/01/2013
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class UserAuditingServiceBean extends CrudSecurityServiceBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 83943518844672344L;
	private Logger Log = Logger.getLogger(UserAuditingServiceBean.class.getName());
	
	/**
	 * Close session service bean.
	 *
	 * @param userInfo the user session
	 * @throws ServiceException the service exception
	 */
	public void closeSessionServiceBean(UserInfo userInfo) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update UserSession US ");	
		sbQuery.append(" Set US.state = :statePrm, ");
		sbQuery.append(" US.finalDate = :finalDatePrm, ");
		sbQuery.append(" US.logoutMotiveFk = :logoutMotivePrm, ");
		sbQuery.append(" US.lastModifyDate = :lastModifyDatePrm ");
		sbQuery.append(" Where US.idUserSessionPk = :idUserSessionPrm");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("statePrm", UserSessionStateType.DISCONNECTED.getCode());
		query.setParameter("finalDatePrm", CommonsUtilities.currentDateTime());
		query.setParameter("logoutMotivePrm", userInfo.getLogoutMotive().getCode());
		query.setParameter("lastModifyDatePrm",CommonsUtilities.currentDateTime());
		query.setParameter("idUserSessionPrm",userInfo.getUserAccountSession().getIdUserSessionPk());
		query.executeUpdate();
	}
	
	/**
	 * Close all sessions service bean.
	 *
	 * @throws ServiceException the service exception
	 */
	public void closeAllOpenSessions() throws ServiceException{
		// Only catch exceptions of business not RuntimeExceptions
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update UserSession US ");	
		sbQuery.append(" Set US.state = :statePrm, ");
		sbQuery.append(" US.finalDate = :finalDatePrm, ");
		sbQuery.append(" US.logoutMotiveFk = :logoutMotivePrm, ");
		sbQuery.append(" US.lastModifyUser = :lastModifyUserPrm, ");
		sbQuery.append(" US.lastModifyDate = :lastModifyDatePrm ");
		//sbQuery.append(" US.lastModifyIp = :lastModifyIpPrm, ");
		//sbQuery.append(" US.lastModifyPriv = :lastModifyPrivPrm ");
		sbQuery.append(" Where US.state = :stateOpen");
		
		Query query = em.createQuery(sbQuery.toString());
		/*String ipCloseSession = "unknown";
		try {
			ipCloseSession = InetAddress.getLocalHost().getHostAddress();
		} catch(UnknownHostException uhex) {
			Log.severe("error in InetAddress.getLocalHost().getHostAddress() is not possible get ip");
		}*/
		query.setParameter("statePrm", UserSessionStateType.DISCONNECTED.getCode());
		query.setParameter("finalDatePrm", CommonsUtilities.currentDateTime());
		query.setParameter("logoutMotivePrm", LogoutMotiveType.SERVERRESTART.getCode());
		query.setParameter("lastModifyUserPrm", "SYSTEM");
		query.setParameter("lastModifyDatePrm",CommonsUtilities.currentDateTime());
		//query.setParameter("lastModifyIpPrm", ipCloseSession);
		//query.setParameter("lastModifyPrivPrm",1);
		query.setParameter("stateOpen",UserSessionStateType.CONNECTED.getCode());
		query.executeUpdate();

	}
	/**
	 * Metodo que consiste en verificar si un usuario tiene
	 * perfil de supervisor o gerente
	 * @param loginUser
	 * */
	public boolean isSupervisorOrManagerProfile(String loginUser){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT SP.NAME from USER_PROFILE UP ");
		sbQuery.append(" INNER JOIN USER_ACCOUNT UA ON UP.ID_USER_FK = UA.ID_USER_PK ");
		sbQuery.append(" INNER JOIN SYSTEM_PROFILE SP ON UP.ID_SYSTEM_PROFILE_FK = SP.ID_SYSTEM_PROFILE_PK ");
		sbQuery.append(" WHERE 1 = 1 ");
		//sbQuery.append(" and ua.ID_SECURITY_INSTITUTION_FK = 1 "); //institucion EDV
		sbQuery.append(" AND UA.LOGIN_USER = UPPER('"+loginUser+"')");
		sbQuery.append(" AND SP.ID_SYSTEM_FK = "+SystemType.CSDCORE.getCode()); //solo Sunqu
		sbQuery.append(" AND SP.STATE = "+SystemProfileStateType.CONFIRMED.getCode());
		sbQuery.append(" AND UP.STATE = "+UserProfileStateType.REGISTERED.getCode());
		sbQuery.append(" AND (SP.NAME LIKE '%SUPERVISOR%' OR SP.NAME LIKE '%GERENTE%' OR SP.NAME LIKE '%GERENCIA%' OR SP.NAME LIKE '%ADMINISTRADOR%') ");

		
		Query query = em.createNativeQuery(sbQuery.toString());
		
		try {
			List<BigDecimal> list = query.getResultList();
			if(list!=null&&list.size()>0)
				return true;
			else
				return false;
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * Metodo con el cual verificamos cual fue la ultima ip que 
	 * se registro
	 * @throws Exception 
	 * */
	public String getRegistrationIp(String registryUser,String ticket) throws Exception{
		String ticketDecrypt = AESEncryptUtils.decrypt(AESKeyType.PRADERAKEY.getValue(),ticket);
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select us.registryIp from UserSession us ");
		sbQuery.append(" where 1 = 1 ");
		//sbQuery.append(" and us.registryUser = :registryUser ");
		sbQuery.append(" and us.ticket = :ticket ");
		//sbQuery.append(" and us.state = "+UserSessionStateType.CONNECTED.getCode());
		sbQuery.append(" order by us.idUserSessionPk desc ");
		
		Query query = em.createQuery(sbQuery.toString()).setFirstResult(0).setMaxResults(1);
		//query.setParameter("registryUser",registryUser);
		query.setParameter("ticket", ticketDecrypt);
		String ip = null;
		try {
			ip = (String) query.getSingleResult();
		} catch (Exception e) {
			ip = "null";
		}
		return ip;
	}
}
