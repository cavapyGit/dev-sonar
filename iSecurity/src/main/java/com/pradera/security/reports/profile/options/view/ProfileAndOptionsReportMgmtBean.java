package com.pradera.security.reports.profile.options.view;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.services.remote.reports.ReportGenerationService;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.security.systemmgmt.view.filters.SystemProfileFilter;
import com.pradera.security.utils.view.GenericBaseBean;
import com.pradera.security.utils.view.PropertiesConstants;

@DepositaryWebBean
public class ProfileAndOptionsReportMgmtBean extends GenericBaseBean{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5971618254709241260L;

	/** The system profile filter. */
	private SystemProfileFilter systemProfileFilter;
	
	@Inject
	Instance<ReportGenerationService> reportService;
	
	/**
	 * @return the systemProfileFilter
	 */
	public SystemProfileFilter getSystemProfileFilter() {
		return systemProfileFilter;
	}

	/**
	 * @param systemProfileFilter the systemProfileFilter to set
	 */
	public void setSystemProfileFilter(SystemProfileFilter systemProfileFilter) {
		this.systemProfileFilter = systemProfileFilter;
	}
	
	public ProfileAndOptionsReportMgmtBean(){
		systemProfileFilter = new SystemProfileFilter();
	}

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		try {
			//List the Systems to display on the combo box of the page
			listAllSystems();
			//List the States of Profile to display on the combo box of the page
			listSystemProfileState();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	@LoggerAuditWeb
	public void sendReport(){
		try{
			hideDialogs();
			LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());
			
			Map<String,String> reportParams = new HashMap<String, String>();
			if(Validations.validateIsNotNullAndPositive(systemProfileFilter.getIdSystemBaseQuery())){
				reportParams.put("system", systemProfileFilter.getIdSystemBaseQuery().toString());
			}
			if(Validations.validateIsNotNullAndNotEmpty(systemProfileFilter.getProfileMnemonicBaseQuery())){
				reportParams.put("mnemonic", systemProfileFilter.getProfileMnemonicBaseQuery());
			}
			if(Validations.validateIsNotNullAndPositive(systemProfileFilter.getProfileStateBaseQuery())){
				reportParams.put("state", systemProfileFilter.getProfileStateBaseQuery().toString());
			}
			Long reportId = ReportIdType.REPOERT_PROFILE_AND_OPTIONS.getCode();
			ReportUser reportUser = new ReportUser();
			reportUser.setUserName(loggerUser.getUserName());
			if(reportId != null){
				reportService.get().saveReportExecution(reportId, reportParams, null, reportUser, loggerUser);
			}
			JSFUtilities.showComponent("frmGeneral:idDialReportConfirm");
			JSFUtilities.showMessageOnDialog(null, null, PropertiesConstants.REPORT_SEND_SUCCESS_MSG, null);
			return;
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void clean(){
		systemProfileFilter = new SystemProfileFilter();
		hideDialogs();
	}
	
	/**
	 * Hide dialogs.
	 */
	public void hideDialogs(){
		JSFUtilities.hideComponent("frmGeneral:idDialReportConfirm");
		JSFUtilities.hideGeneralDialogues();
	}
}
