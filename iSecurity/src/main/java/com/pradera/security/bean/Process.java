package com.pradera.security.bean;

import java.io.Serializable;

import com.pradera.security.model.type.AuditProcessState;
import com.pradera.security.model.type.AuditProcessType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class Process.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05/06/2014
 */
public class Process implements Serializable {
	
	/** Default serial version uid. */
	private static final long serialVersionUID = 1L;
	
	/** The id process pk. */
	private long idProcessPK;
	
	/** The process type. */
	private String processType;
	
	/** The process module. */
	private String processModule;
	
	/** The process name. */
	private String processName;
	
	/** The process date. */
	private String processDate;
	
	/** The process user. */
	private String processUser;
	
	/** The ip process. */
	private String ipProcess;
	
	/** The process status. */
	private String processStatus;
	
	/** The process submodule. */
	private String processSubmodule;
	
	/** The process time. */
	private String processTime;

	/**
	 * Gets the id process pk.
	 *
	 * @return the idProcessPK
	 */
	public long getIdProcessPK() {
		return idProcessPK;
	}

	/**
	 * Sets the id process pk.
	 *
	 * @param idProcessPK            the idProcessPK to set
	 */
	public void setIdProcessPK(long idProcessPK) {
		this.idProcessPK = idProcessPK;
	}

	/**
	 * Gets the process type.
	 *
	 * @return the processType
	 */
	public String getProcessType() {
		return processType;
	}

	/**
	 * Sets the process type.
	 *
	 * @param processType            the processType to set
	 */
	public void setProcessType(String processType) {
		this.processType = processType;
	}

	/**
	 * Gets the process module.
	 *
	 * @return the processModule
	 */
	public String getProcessModule() {
		return processModule;
	}

	/**
	 * Sets the process module.
	 *
	 * @param processModule            the processModule to set
	 */
	public void setProcessModule(String processModule) {
		this.processModule = processModule;
	}

	/**
	 * Gets the process name.
	 *
	 * @return the processName
	 */
	public String getProcessName() {
		return processName;
	}

	/**
	 * Sets the process name.
	 *
	 * @param processName            the processName to set
	 */
	public void setProcessName(String processName) {
		this.processName = processName;
	}

	/**
	 * Gets the process date.
	 *
	 * @return the processDate
	 */
	public String getProcessDate() {
		return processDate;
	}

	/**
	 * Sets the process date.
	 *
	 * @param processDate            the processDate to set
	 */
	public void setProcessDate(String processDate) {
		this.processDate = processDate;
	}

	/**
	 * Gets the process user.
	 *
	 * @return the processUser
	 */
	public String getProcessUser() {
		return processUser;
	}

	/**
	 * Sets the process user.
	 *
	 * @param processUser            the processUser to set
	 */
	public void setProcessUser(String processUser) {
		this.processUser = processUser;
	}

	/**
	 * Gets the ip process.
	 *
	 * @return the ipProcess
	 */
	public String getIpProcess() {
		return ipProcess;
	}

	/**
	 * Sets the ip process.
	 *
	 * @param ipProcess            the ipProcess to set
	 */
	public void setIpProcess(String ipProcess) {
		this.ipProcess = ipProcess;
	}

	/**
	 * Gets the process status.
	 *
	 * @return the processStatus
	 */
	public String getProcessStatus() {
		return processStatus;
	}

	/**
	 * Sets the process status.
	 *
	 * @param processStatus            the processStatus to set
	 */
	public void setProcessStatus(String processStatus) {
		this.processStatus = processStatus;
	}

	/**
	 * Gets the process submodule.
	 *
	 * @return the processSubmodule
	 */
	public String getProcessSubmodule() {
		return processSubmodule;
	}

	/**
	 * Sets the process submodule.
	 *
	 * @param processSubmodule            the processSubmodule to set
	 */
	public void setProcessSubmodule(String processSubmodule) {
		this.processSubmodule = processSubmodule;
	}

	/**
	 * Gets the process time.
	 *
	 * @return the processTime
	 */
	public String getProcessTime() {
		return processTime;
	}

	/**
	 * Sets the process time.
	 *
	 * @param processTime            the processTime to set
	 */
	public void setProcessTime(String processTime) {
		this.processTime = processTime;
	}

	/**
	 * Gets the Audit Process type description.
	 * 
	 * @return the Audit Process type description
	 */
	public String getAuditProcessType() {
		String description = null;
		if (processType != null) {
			description = AuditProcessType.lookup.get(
					Integer.valueOf(processType)).getValue();
		}
		return description;
	}

	/**
	 * Gets the Audit Process State description.
	 * 
	 * @return the Audit Process State description
	 */
	public String getAuditProcessState() {
		String description = null;
		if (processType != null) {
			description = AuditProcessState.lookup.get(
					Integer.valueOf(processType)).getValue();
		}
		return description;
	}

}