package com.pradera.security.bean;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary. Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class Parameters.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 05/06/2014
 */
public class Parameters {

	/** The value. */
	private String value;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 * 
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Instantiates a new parameters.
	 * 
	 * @param text
	 *            the text
	 */
	public Parameters(String text) {
		value = text;
	}

}
