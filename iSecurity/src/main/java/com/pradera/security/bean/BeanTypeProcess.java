package com.pradera.security.bean;

import java.io.Serializable;

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class BeanType
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.1
 */

public class BeanTypeProcess implements Serializable{
	
	private Integer idTypePK;
	private String description;
	
	/**
	 * @param idTypePK
	 * @param description
	 */
	public BeanTypeProcess(Integer idTypePK, String description) {
		super();
		this.idTypePK = idTypePK;
		this.description = description;
	}
	/**
	 * Constructor
	 */
	public BeanTypeProcess() {
		super();
	}
	/**
	 * @return the idTypePK
	 */
	public Integer getIdTypePK() {
		return idTypePK;
	}
	/**
	 * @param idTypePK the idTypePK to set
	 */
	public void setIdTypePK(Integer idTypePK) {
		this.idTypePK = idTypePK;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
