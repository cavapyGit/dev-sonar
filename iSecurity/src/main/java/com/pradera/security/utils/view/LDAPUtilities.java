package com.pradera.security.utils.view;

import java.io.Serializable;

import com.pradera.commons.security.model.type.ChangePwdMessageType;
import com.pradera.commons.security.model.type.LoginMessageType;
import com.pradera.security.model.UserAccount;
import com.pradera.security.ppolicy.to.PasswordPolicyTO;



/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Annotation DepositaryDataBase.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 29/11/2012
 */
public class LDAPUtilities implements Serializable{
	
	public static final String ENABLE_AUTH = "enable.authentication";
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The stage application. */
	private String stageApplication;
		
	private boolean enableAuthentication;
	
	/**
	 * Instantiates a new LDAP utilities.
	 *
	 * @param stageApplication the stage application
	 */
	public LDAPUtilities(String stageApplication, boolean enableAuthentication){
		this.stageApplication = stageApplication;
		this.enableAuthentication = enableAuthentication;
	}
	
	/**
	 * Fill password policy.
	 *
	 * @throws Exception the exception
	 */
	public void fillPasswordPolicy() throws Exception{
		OpenLDAPUtilities.getInstance(stageApplication).fillPasswordPolicy();
	}
	
	/**
	 * Adds the user.
	 *
	 * @param userAccount the user account
	 * @param password the password
	 * @throws Exception the exception
	 */
	public void addUser(UserAccount userAccount,String password) throws Exception{
		OpenLDAPUtilities.getInstance(stageApplication).addUser(userAccount, password);
	}
	
	/**
	 * Update user details.
	 *
	 * @param userAccount the user account
	 * @throws Exception the exception
	 */
	public void updateUserDetails(UserAccount userAccount) throws Exception{
		OpenLDAPUtilities.getInstance(stageApplication).updateUserDetails(userAccount);
	}
	
	/**
	 * Reset password.
	 *
	 * @param userAccount the user account
	 * @param password the password
	 * @throws Exception the exception
	 */
	public void resetPassword(UserAccount userAccount,String password) throws Exception{
		OpenLDAPUtilities.getInstance(stageApplication).resetPassword(userAccount, password);
	}
	
	/**
	 * Authenticate user.
	 *
	 * @param usrName the usr name
	 * @param pwd the pwd
	 * @param objStrBuff the obj str buff
	 * @return the login message type
	 * @throws Exception the exception
	 */
	public LoginMessageType authenticateUser(String usrName, String pwd, StringBuffer objStrBuff) throws Exception{
		return OpenLDAPUtilities.getInstance(stageApplication).authenticateUser(usrName, pwd, objStrBuff);
	}
	
	/**
	 * Change password.
	 *
	 * @param strUser the str user
	 * @param strOldPwd the str old pwd
	 * @param strNewPwd the str new pwd
	 * @param strConfPwd the str conf pwd
	 * @return the change pwd message type
	 * @throws Exception the exception
	 */
	public ChangePwdMessageType changePassword(String strUser, String strOldPwd, String strNewPwd, String strConfPwd) throws Exception{
		return OpenLDAPUtilities.getInstance(stageApplication).changePassword(strUser, strOldPwd, strNewPwd, strConfPwd);
	}
	
	/**
	 * Gets the pwd min length.
	 *
	 * @return the pwd min length
	 * @throws Exception the exception
	 */
	public int getPwdMinLength() throws Exception{
		//return OpenLDAPUtilities.getInstance(stageApplication).getPwdMinLength();
		return 8;
	}
	
	/**
	 * Disable user account.
	 *
	 * @param strUserLoginName the str user login name
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public boolean disableUserAccount(String strUserLoginName) throws Exception{
		return OpenLDAPUtilities.getInstance(stageApplication).disableUserAccount(strUserLoginName);
	}
	
	/**
	 * Enable user account.
	 *
	 * @param strUserLoginName the str user login name
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public boolean enableUserAccount(String strUserLoginName) throws Exception{
		return OpenLDAPUtilities.getInstance(stageApplication).enableUserAccount(strUserLoginName);
	}
	
	/**
	 * Gets the password policy details.
	 *
	 * @return the password policy details
	 */
	public PasswordPolicyTO getPasswordPolicyDetails(){
		return OpenLDAPUtilities.getInstance(stageApplication).getPasswordPolicyDetails();		
	}
	
	/**
	 * Update password policy details.
	 *
	 * @param pPolicy the policy
	 * @throws Exception the exception
	 */
	public void updatePasswordPolicyDetails(PasswordPolicyTO pPolicy) throws Exception{
		OpenLDAPUtilities.getInstance(stageApplication).updatePasswordPolicyDetails(pPolicy);
	}
	
	/**
	 * Authenticate user active directory.
	 *
	 * @param usrName the usr name
	 * @param pwd the pwd
	 * @return the login message type
	 * @throws Exception the exception
	 */
	public LoginMessageType authenticateUserActiveDirectory(String usrName, String pwd) throws Exception{
		return OpenLDAPUtilities.getInstanceeActiveDirectory(stageApplication).authenticateUserActiveDirectory(usrName, pwd);
	}
	
	/**
	 * Gets the password user ldap.
	 *
	 * @param usrName the usr name
	 * @return the password user ldap
	 * @throws Exception the exception
	 */
	public String getPasswordUserLdap(String usrName) throws Exception{
		return OpenLDAPUtilities.getInstance(stageApplication).getPasswordUserLdap(usrName);
	}

	/**
	 * @return the enableAuthentication
	 */
	public boolean isEnableAuthentication() {
		return enableAuthentication;
	}

	/**
	 * @param enableAuthentication the enableAuthentication to set
	 */
	public void setEnableAuthentication(boolean enableAuthentication) {
		this.enableAuthentication = enableAuthentication;
	}
}
