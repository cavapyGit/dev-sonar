package com.pradera.security.utils.view;

import java.util.ArrayList;
import java.util.List;

import com.novell.ldap.LDAPConnection;
import com.novell.ldap.LDAPException;

public class AppPoolManager
{
    /** Contains all of the sharedConns that are in use */
    private List<LDAPConnection> inUseConnections;
    /** Contains all of the available sharedConns */
    private List<LDAPConnection> availableConnections;
    /** Set by finalize. This tells any waiting thread to shutdown.*/
    private boolean shuttingDown;

    /**
     * Initialize the connection pool.
     *
     * @param host - Host name associated with this connection pool 
     * (see {@link com.novell.ldap.LDAPConnection#connect(String, int) LDAPConnection.connect()}).
     * @param port - Port number for the host associated with this connection
     *   pool.
     * (see {@link com.novell.ldap.LDAPConnection#connect(String, int) LDAPConnection.connect()}).
     * @param maxConns - Maximum number of physical connections allowed for
     *             this host.
     * 
     */
    public AppPoolManager(String host,
                          int port,
                          int maxConns)
        throws LDAPException
    {
        // Use the keystore file if it is there.

        inUseConnections = new ArrayList<LDAPConnection>();
        availableConnections = new ArrayList<LDAPConnection>();
        // Set up the max connections and max shared connections
        // ( original + clones) in availableConnection.
        for (int i = 0; i < maxConns; i++)
        {
            LDAPConnection conn = new LDAPConnection();
            // At this point all of the connections anonymous
            conn.connect(host, port);
            availableConnections.add(conn);
        }
        shuttingDown = false;
    }

    /**
     * Get a bound connection.
     * <p>This returns a bound (bind) connection for the desired DN and
     * password.</p>
     * @param DN  Authentication DN used for bind and key.
     * @param PW  Authentication password used for bind and key.
     * @throws LDAPException if an LDAPConnection could not be bound.
     */
    public LDAPConnection getConnection()
            throws LDAPException, InterruptedException
    {

        LDAPConnection        conn        = null;        

        synchronized (availableConnections)
        {
                // If there are no available sharedConns wait for one.
                while(0 == availableConnections.size())
                {
                    // Wait for available Instances
                    availableConnections.wait();
                    // If we are shutting down return null
                    if(shuttingDown) return null;
                }
                // Get connection from first available sharedConns
                conn = availableConnections.remove(0);
        }

        synchronized (inUseConnections)
        {
            // Move into inuse.
        	inUseConnections.add(conn);
        }
        return conn;
    }

    /**
     * Make this connection available.
     * @param conn LDAPConnection to be made available.
     */
    public void makeConnectionAvailable(LDAPConnection conn)
    {
    	boolean connRemoved = false;
        synchronized(inUseConnections)
        {

        	connRemoved = inUseConnections.remove(conn);
        }

        if(connRemoved)
        {
            synchronized(availableConnections)
            {
            	availableConnections.add(conn);
                // Notify anyone that might be waiting on this connection.
            	availableConnections.notify();
            }
        }
        return;
    }

    /**
     * Free connections.
     * <p> Tell all waiting threads that we are shutting down.
     * Clean up the in use and available connections.</p>
     *
     * @throws Throwable when disconnect fails.
     */
    protected void finalize()
            throws Throwable

    {
        synchronized (availableConnections)
        {
            // Notify all waiting threads.
            shuttingDown = true;
            availableConnections.notifyAll();            
        }
        for(LDAPConnection conn : availableConnections){
        	if(conn!= null && conn.isConnected()){
        		conn.disconnect();
        	}
        }
        for(LDAPConnection conn : inUseConnections){
        	if(conn!= null && conn.isConnected()){
        		conn.disconnect();
        	}
        }
        
    }
}

