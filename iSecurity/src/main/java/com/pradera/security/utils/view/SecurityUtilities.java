package com.pradera.security.utils.view;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.usersession.UserAcctions;
import com.pradera.security.model.OptionPrivilege;
import com.pradera.security.model.type.PrivilegeType;

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class SecurityUtilities
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.1
 */
public class SecurityUtilities {
	
	/**
	 * Convert user acctions from privileges options' List
	 * this method is useful to get UserAcctions from list's PrivilegesOptions.
	 * and get id of privileges of systems
	 * @param optionPrivileges the option privileges
	 * @return the user acctions
	 * @throws FileNotFoundException 
	 */
	public static UserAcctions convertUserAcctionsFromPrivileges(List<OptionPrivilege> optionPrivileges) {
		UserAcctions userAcctions = new UserAcctions();
		if(Validations.validateIsNotNull(optionPrivileges)){
			
			for(OptionPrivilege optionPrivilege: optionPrivileges){
			
				switch(PrivilegeType.lookup.get(optionPrivilege.getIdPrivilege())){
					case REGISTER:
						userAcctions.setRegister(Boolean.TRUE);
						userAcctions.setIdPrivilegeRegister(optionPrivilege.getIdOptionPrivilegePk());
						break;
					case MODIFY: 
						userAcctions.setModify(Boolean.TRUE);
						userAcctions.setIdPrivilegeModify(optionPrivilege.getIdOptionPrivilegePk());
						break;
					case CONFIRM:
						userAcctions.setConfirm(Boolean.TRUE);
						userAcctions.setIdPrivilegeConfirm(optionPrivilege.getIdOptionPrivilegePk());
						break;
					case DELETE: 
						userAcctions.setDelete(Boolean.TRUE);
						userAcctions.setIdPrivilegeDelete(optionPrivilege.getIdOptionPrivilegePk());
						break;
					case BLOCK: 
						userAcctions.setBlock(Boolean.TRUE);
						userAcctions.setIdPrivilegeBlock(optionPrivilege.getIdOptionPrivilegePk());
						break;
					case UNBLOCK: 
						userAcctions.setUnblock(Boolean.TRUE);
						userAcctions.setIdPrivilegeUnblock(optionPrivilege.getIdOptionPrivilegePk());
						break;				
					case REJECT: 
						userAcctions.setReject(Boolean.TRUE);
						userAcctions.setIdPrivilegeReject(optionPrivilege.getIdOptionPrivilegePk());
						break;
					case SEARCH: 
						userAcctions.setSearch(Boolean.TRUE);
						userAcctions.setIdPrivilegeSearch(optionPrivilege.getIdOptionPrivilegePk());
						break;
					case ADD: 
						userAcctions.setAdd(Boolean.TRUE);
						userAcctions.setIdPrivilegeAdd(optionPrivilege.getIdOptionPrivilegePk());
						break;		
					case ANNULAR:
						userAcctions.setAnnular(Boolean.TRUE);
						userAcctions.setIdPrivilegeAnnular(optionPrivilege.getIdOptionPrivilegePk());
						break;
					case APPROVE:
						userAcctions.setApprove(Boolean.TRUE);
						userAcctions.setIdPrivilegeApprove(optionPrivilege.getIdOptionPrivilegePk());
						break;
					case REVIEW:
						userAcctions.setReview(Boolean.TRUE);
						userAcctions.setIdPrivilegeReview(optionPrivilege.getIdOptionPrivilegePk());
						break;
					case APPLY:
						userAcctions.setApply(Boolean.TRUE);
						userAcctions.setIdPrivilegeApply(optionPrivilege.getIdOptionPrivilegePk());
						break;
					case AUTHORIZE:
						userAcctions.setAuthorize(Boolean.TRUE);
						userAcctions.setIdPrivilegeAuthorize(optionPrivilege.getIdOptionPrivilegePk());
						break;	
						
					case CANCEL:
						userAcctions.setCancel(Boolean.TRUE);
						userAcctions.setIdPrivilegeCancel(optionPrivilege.getIdOptionPrivilegePk());
						break;
						
					case SETTLEMENT:
						userAcctions.setSettlement(Boolean.TRUE);
						userAcctions.setIdPrivilegeSettlement(optionPrivilege.getIdOptionPrivilegePk());
						break;
						
					case ACTIVATE:
						userAcctions.setActivate(Boolean.TRUE);
						userAcctions.setIdPrivilegeActivate(optionPrivilege.getIdOptionPrivilegePk());
						break;
						
					case DEACTIVATE:
						userAcctions.setDeactivate(Boolean.TRUE);
						userAcctions.setIdPrivilegeDeactivate(optionPrivilege.getIdOptionPrivilegePk());
						break;
						
						//---
					case CONFIRM_DEPOSITARY:
						userAcctions.setConfirmDepositary(Boolean.TRUE);
						userAcctions.setIdPrivilegeConfirmDepositary(optionPrivilege.getIdOptionPrivilegePk());
						break;
					
					case CONFIRM_ISSUER:
						userAcctions.setConfirmIssuer(Boolean.TRUE);
						userAcctions.setIdPrivilegeConfirmIssuer(optionPrivilege.getIdOptionPrivilegePk());
						break;
					
					case PRELIMINARY:
						userAcctions.setPreliminary(Boolean.TRUE);
						userAcctions.setIdPrivilegePreliminary(optionPrivilege.getIdOptionPrivilegePk());
						break;
					
					case DEFINITIVE:
						userAcctions.setDefinitive(Boolean.TRUE);
						userAcctions.setIdPrivilegeDefinitive(optionPrivilege.getIdOptionPrivilegePk());
						break;
					
					case ADJUSTSMENTS:
						userAcctions.setAdjustments(Boolean.TRUE);
						userAcctions.setIdPrivilegeAdjustments(optionPrivilege.getIdOptionPrivilegePk());
						break;
						
					case GENERATE:
						userAcctions.setGenerate(Boolean.TRUE);
						userAcctions.setIdPrivilegeGenerate(optionPrivilege.getIdOptionPrivilegePk());
						break;
						
					case EXECUTE:
						userAcctions.setExecute(Boolean.TRUE);
						userAcctions.setIdPrivilegeExecute(optionPrivilege.getIdOptionPrivilegePk());
						break;
						
					case UPDATE:
						userAcctions.setUpdate(Boolean.TRUE);
						userAcctions.setIdPrivilegeUpdate(optionPrivilege.getIdOptionPrivilegePk());
						break;
						
					case DELIVER:
						userAcctions.setDeliver(Boolean.TRUE);
						userAcctions.setIdPrivilegeDeliver(optionPrivilege.getIdOptionPrivilegePk());
						break;
						
					case APPEAL:
						userAcctions.setAppeal(Boolean.TRUE);
						userAcctions.setIdPrivilegeAppeal(optionPrivilege.getIdOptionPrivilegePk());
						break;
					
					case CERTIFY:
						userAcctions.setCertify(Boolean.TRUE);
						userAcctions.setIdPrivilegeCertify(optionPrivilege.getIdOptionPrivilegePk());
						break;
						
					case CONFIRM_BLOCK:
						userAcctions.setConfirmBlock(Boolean.TRUE);
						userAcctions.setIdPrivilegeConfirmBlock(optionPrivilege.getIdOptionPrivilegePk());
						break;
						
					case REJECT_BLOCK:
						userAcctions.setRejectBlock(Boolean.TRUE);
						userAcctions.setIdPrivilegeRejectBlock(optionPrivilege.getIdOptionPrivilegePk());
						break;
						
					case CONFIRM_UNBLOCK:
						userAcctions.setConfirmUnblock(Boolean.TRUE);
						userAcctions.setIdPrivilegeConfirmUnblock(optionPrivilege.getIdOptionPrivilegePk());
						break;
						
					case REJECT_UNBLOCK:
						userAcctions.setRejectUnblock(Boolean.TRUE);
						userAcctions.setIdPrivilegeRejectUnblock(optionPrivilege.getIdOptionPrivilegePk());
						break;
						
					case CONFIRM_DELETE:
						userAcctions.setConfirmDelete(Boolean.TRUE);
						userAcctions.setIdPrivilegeConfirmDelete(optionPrivilege.getIdOptionPrivilegePk());
						break;
						
					case REJECT_DELETE:
						userAcctions.setRejectDelete(Boolean.TRUE);
						userAcctions.setIdPrivilegeRejectDelete(optionPrivilege.getIdOptionPrivilegePk());
						break;
					
					case CONFIRM_ANNULAR:
						userAcctions.setConfirmAnnular(Boolean.TRUE);
						userAcctions.setIdPrivilegeConfirmAnnular(optionPrivilege.getIdOptionPrivilegePk());
						break;
						
					case REJECT_ANNULAR:
						userAcctions.setRejectAnnular(Boolean.TRUE);
						userAcctions.setIdPrivilegeRejectAnnular(optionPrivilege.getIdOptionPrivilegePk());
						break;
						
					case CONFIRM_MODIFY:
						userAcctions.setConfirmModify(Boolean.TRUE);
						userAcctions.setIdPrivilegeConfirmModify(optionPrivilege.getIdOptionPrivilegePk());
						break;
						
					case REJECT_MODIFY:
						userAcctions.setRejectModify(Boolean.TRUE);
						userAcctions.setIdPrivilegeRejectModify(optionPrivilege.getIdOptionPrivilegePk());
						break;
						
				}
			}
			
			
		}
		
		return userAcctions;
	}
	
	/**
	 * Generate session ticket.
	 *
	 * @return the string
	 */
	public static String generateSessionTicket(){
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
	
	/**
	 * Generate the password for user
	 * 
	 * @return the String type
	 */
	public static String generatePassword()
    {
            Random rd = new Random();
            char lowerChars[] = "abcdefghijklmnopqrstuvwxyz".toCharArray();
            char upperChars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
            char specialChars[] = "~!$%&*-_=+[{]}|;:/?".toCharArray();

            List<Character> pwdLst = new ArrayList<Character>();
            for (int g = 1; g <=3; g++)
            {
                    for (int z = 0; z < 1; z++)
                    {
                  	  	  if (g == 1)
                            {
                                    pwdLst.add(lowerChars[rd.nextInt(26)]);
                            }
                            else if (g == 2)
                            {
                                    pwdLst.add(upperChars[rd.nextInt(26)]);
                            }
                            else if (g == 3)
                            {
                                    pwdLst.add(specialChars[rd.nextInt(19)]);
                            }
                    }
                    if (pwdLst.size() == 8)
                    {
                            break;
                    }
                    if (g + 1 == 4)
                    {
//                          g = (int) Math.random() * 5;
                            g=0;
                    }
            }
            StringBuilder password = new StringBuilder();
            Collections.shuffle(pwdLst);
            for (int c = 0; c < pwdLst.size(); c++)
            {
                    password.append(pwdLst.get(c));
            }
            return password.toString();
    }
}
