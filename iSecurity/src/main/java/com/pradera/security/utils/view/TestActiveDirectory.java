package com.pradera.security.utils.view;

import com.pradera.commons.type.AESKeyType;
import com.pradera.commons.utils.AESEncryptUtils;
import com.novell.ldap.LDAPConnection;
import com.novell.ldap.LDAPResponse;
import com.novell.ldap.LDAPResponseQueue;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class TestActiveDirectory.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 15-jul-2015
 */
public class TestActiveDirectory {
	
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public  static void main(String[] args){
		try{
			TestActiveDirectory test = new TestActiveDirectory();
			test.testEncrypPasswordLdap();
			}catch(Exception ex){
				ex.printStackTrace();
			}
	}
	
	/**
	 * Test encryp password ldap.
	 */
	public void testEncrypPasswordLdap(){
		try{
		String passwordPlain="testpasswordsimple";
		String passwordEncrypted=AESEncryptUtils.encrypt(AESKeyType.PRADERAKEY.getValue(), passwordPlain);
		String passwordDecrypted=AESEncryptUtils.decrypt(AESKeyType.PRADERAKEY.getValue(), passwordEncrypted);
		System.out.println("cifrando string : "+passwordPlain);
		System.out.println("valor cifrado es: "+passwordEncrypted);
		System.out.println("valor descifrado es: "+passwordDecrypted);
		} catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	/**
	 * Login active directory.
	 *
	 * @param rc the rc
	 * @throws Exception the exception
	 */
	public void loginActiveDirectory() throws Exception{
		String hostname="192.168.1.68";
		int port=389;
		String username="oquisbert";
		String domain="edvbolivia";
		String password="Hola12345";
//		String username="jajata";
//		String domain="edvbolivia";
//		String password="Laura25.";
//		LDAPConnection connection = new LDAPConnection( new LDAPJSSEStartTLSFactory() );
		LDAPConnection connection = new LDAPConnection();
		connection.connect(hostname, port);
		LDAPResponseQueue queue= connection.bind(LDAPConnection.LDAP_V3, username+"@"+domain, password.getBytes(),(LDAPResponseQueue)null);
		LDAPResponse rsp = (LDAPResponse)queue.getResponse();
	}

}
