package com.pradera.security.utils.view;

import javax.inject.Inject;

import com.pradera.commons.notifications.remote.NotificationRegisterTO;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.integration.contextholder.LoggerUser;

public class RestServiceNotification {

	@Inject
	protected ClientRestService clientRestService;
	
	public void sendNotification(LoggerUser loggerUser, Long businessProcess,Object[] parameters ){
		NotificationRegisterTO notificationRegisterTO = new NotificationRegisterTO();
		notificationRegisterTO.setLoggerUser(loggerUser);
		notificationRegisterTO.setUserName(loggerUser.getUserName());
		notificationRegisterTO.setBusinessProcess(businessProcess);
		notificationRegisterTO.setParameters(parameters);
		
		clientRestService.sendNotificationConfigurate(notificationRegisterTO);
	}
	
}
