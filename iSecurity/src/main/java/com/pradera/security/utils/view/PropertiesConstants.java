package com.pradera.security.utils.view;

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class PropertiesConstants
 * 
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date :
 * @version 1.1
 */
public class PropertiesConstants {

	private PropertiesConstants() {
		super();
	}
	
	public static final String LBL_ENVIRONMENT_PRODUCTION = "lbl.environment.production";
	public static final String LBL_ENVIRONMENT_SYSTEMTEST = "lbl.environment.systemtest";
	public static final String LBL_ENVIRONMENT_DEVELOPMENT = "lbl.environment.development";
	public static final String LBL_ENVIRONMENT_MIGRATION = "lbl.environment.migration";

	public static final String SUCCESS_ADM_PROFILE_REGISTER_LABEL = "success.adm.profile.register.label";
	public static final String SUCCESS_ADM_PROFILE_REGISTER_MESSAGE = "success.adm.profile.register.message";
	public static final String SUCCESS_ADM_PROFILE_MODIFY_LABEL = "success.adm.profile.modify.label";
	public static final String SUCCESS_ADM_PROFILE_MODIFY_MESSAGE = "success.adm.profile.modify.message";

	public static final String LBL_CONFIRM_HEADER_REGISTER = "lbl.confirm.header.record";
	public static final String LBL_CONFIRM_HEADER_MODIFICATION = "lbl.confirm.header.modification";
	public static final String LBL_CONFIRM_HEADER_BLOCK = "lbl.confirm.header.block";
	public static final String LBL_CONFIRM_HEADER_UNBLOCK = "lbl.confirm.header.unblock";
	public static final String LBL_CONFIRM_HEADER_CONFIRM = "lbl.confirm.header.confirm";
	public static final String LBL_CONFIRM_HEADER_REJECT = "lbl.confirm.header.reject";
	public static final String LBL_CONFIRM_HEADER_DELETE = "lbl.confirm.header.delete";
	public static final String LBL_ERROR_INSERT_OTHERMOTIVE="lbl.insert.other.motive";
	public static final String LBL_ERROR_INSERT_DESCRIPTOIN="holiday.lbl.description.required";
	
	public final static String LBL_HEADER_ALERT_ERROR="lbl.header.alert.error";

	public static final String ERROR_COMBO_SELECCIONE = "error.combo.select";
	public static final String SEARCH_ERROR_COMBO_SELECCIONE = "error.combo.select";

	public static final String ADM_PROFILLE_LBL_CONFIRM_REGISTER = "adm.profile.lbl.confirm.register";
	public static final String ADM_PROFILE_LBL_CONFIRM_MODIFY = "adm.profile.lbl.confirm.modify";

	public static final String ERROR_ADM_PROFILE_MODIFY_NO_SELECTED = "error.adm.profile.modify.no.selected";
	public static final String ERROR_ADM_PROFILE_BLOCK_NO_SELECTED = "error.adm.profile.block.no.selected";
	public static final String ERROR_ADM_PROFILE_UNBLOCK_NO_SELECTED = "error.adm.profile.unblock.no.selected";
	public static final String ERROR_ADM_PROFILE_MODIFY_STATE_BLOCKED = "error.adm.profile.modify.state.blocked";

	public static final String ERROR_ADM_PROFILE_MNEMONIC_REPEATED = "error.adm.profile.mnemonic.repeated";
	public static final String ERROR_ADM_PROFILE_NAME_REPEATED = "error.adm.profile.name.repeated";
	public static final String ERROR_ADM_PROFILE_PRIVILEGES_NO_SELECTED = "error.adm.profile.privileges.no.selected";
	public static final String ERROR_ADM_PROFILE_PRIVILEGES_NO_EXIST = "error.adm.profile.privileges.no.exist";
	public static final String SYSTEM_PROFILE_ERROR_MODIFIED_RECENT = "system.profile.error.modify.recent";
	public static final String ERROR_ADM_ALREADY_BLOCKED = "error.adm.profile.already.blocked";
	public static final String ERROR_ADM_ALREADY_UNBLOCKED = "error.adm.profile.already.unblocked";
	public static final String ERROR_ADM_ALREADY_CONFIRM = "error.adm.profile.already.confirm";
	public static final String ERROR_ADM_ALREADY_REJECT = "error.adm.profile.already.reject";	
	public static final String ERROR_INSERT_PROFILE_MNEMONIC="profile.mnemonic";
	public static final String ERROR_INSERT_PROFILE_NAME="profile.name";
	public static final String ERROR_INSERT_PROFILE_DESCRIPTION="profile.description";
	
	public static final String SYSTEM_REGISTER_SUCCESS = "system.msg.register.success";
	public static final String SYSTEM_MODIFY_SUCCESS = "system.msg.modify.success";
	
	public final static String FUPL_ERROR_INVALID_FILE_TYPE="fupl.error.invalid.file.type";
	/** FILE SIZE VALIDATION */
	public static final String FUPL_ERROR_INVALID_FILE_SIZE="opt.adm.system.fupl.error.max.size";
	/** FILE NAME VALIDATION */
	public static final String FUPL_ERROR_INVALID_FILE_NAME="opt.adm.system.fupl.error.length";	
	public static final String FUPL_NO_SELECTED="fupl.no.selected";
	public static final String FUPL_ATTACHED_DOCUMENT="fupl.attached.document";
	public static final String FUPL_ATTACHED_IMAGE="fupl.attached.image";

	public static final String SYSTEM_ERROR_MODIFY_NO_SELECTED = "system.error.modify.no.selected";
	public static final String SYSTEM_ERROR_BLOCK_NO_SELECTED = "system.error.block.no.selected";
	public static final String SYSTEM_ERROR_UNBLOCK_NO_SELECTED = "system.error.unblock.no.selected";
	public static final String SYSTEM_ERROR_MODIFY_STATE_BLOCK = "system.error.modify.state.block";
	public static final String SYSTEM_ERROR_BLOCK_STATE_BLOCK = "system.error.block.state.block";
	public static final String SYSTEM_ERROR_UNBLOCK_STATE_REGISTER = "system.error.unblock.state.register";
	public static final String SYSTEM_ERROR_IMAGE_REQUIRED = "system.error.image.required";
	public static final String SYSTEM_ERROR_PRODUCTION_FILE_REQUIRED = "system.error.production.file.required";
	public static final String SYSTEM_ERROR_MNEMONIC_REPEATED = "system.error.mnemonic.repeated";
	public static final String SYSTEM_ERROR_MODIFY = "system.error.modify";
	public static final String SYSTEM_ERROR_NAME_REPEATED = "system.error.name.repeated";
    public static final String SYSTEM_ERROR_MODIFIED_RECENT = "system.error.modify.recent";
    public static final String SYSTEM_OPTION_ERROR_MODIFIED_RECENT = "system.option.error.modify.recent";
    public static final String SYSTEM_OPTION_ERROR_ALREDY_BLOCKED = "system.option.error.already.blocked";
    
	public static final String ADM_HOLIDAY_LBL_CONFIRM_REGISTER = "adm.holiday.lbl.confirm.register";
	public static final String ADM_HOLIDAY_LBL_CONFIRM_DELETE = "adm.holiday.lbl.confirm.delete";
	public static final String SUCCESS_ADM_HOLIDAY_REGISTER_LABEL = "success.adm.holiday.register.label";
	public static final String SUCCESS_ADM_HOLIDAY_DELETE_LABEL = "success.adm.holiday.delete.label";
	public static final String ERROR_ADM_HOLIDAY_REGISTER_PAST_DATE = "error.adm.holiday.register.past.date";
	public static final String ERROR_ADM_HOLIDAY_REGISTER_USER="error.adm.holiday.register.user";
	public static final String HOLIDAY_ALREADY_REGISTERED ="error.holiday.already.register";
	
	public static final String DATE_FORMAT = "dateFormat";
	public static final String MONTH_YEAR_FORMAT = "monthYearFormat";
	public static final String LBL_MOTIVE_BLOCK = "lbl.motive.block";
	public static final String LBL_MOTIVE_UNBLOCK = "lbl.motive.unblock";
	public static final String LBL_MOTIVE_REJECT = "lbl.motive.reject";
	public static final String LBL_MOTIVE_UNREGISTER = "lbl.motive.unregister";
	public static final String LBL_MOTIVE_DELETE = "lbl.motive.delete";
	public static final String LBL_MOTIVE_CONFIRM = "lbl.motive.confirm";

	public static final String SCHEDULEEXCEPTION_CONFIRM_REGISTER = "schedule.exception.confirm.register";
	public static final String SCHEDULEEXCEPTION_CONFIRM_MODIFY = "schedule.exception.confirm.modify";
	public static final String SCHEDULEEXCPTION_CONFIRM_BLOCK = "schedule.exception.confirm.block";
	public static final String SCHEDULEEXCEPTION_CONFIRM_UNBLOCK = "schedule.exception.confirm.unblock";
	public static final String SCHEDULEEXCEPTION_REGISTER_SUCCESS = "schedule.exception.msg.register.success";
	public static final String SCHEDULEEXCEPTION_REGISTER_MESSAGE = "schedule.exception.msg.register.message";
	public static final String SCHEDULEEXCEPTION_MODIFY_SUCCESS = "schedule.exception.msg.modify.success";
	public static final String SCHEDULEEXCEPTION_MODIFY_MESSAGE = "schedule.exception.msg.modify.message";


	public static final String SCHEDULEEXCEPTION_CONFIRM_HEADER_REGISTER = "opc.adm.schedule.exception.title.register";
	public static final String SCHEDULEEXCEPTION_CONFIRM_HEADER_MODIFY = "opc.adm.schedule.exception.title.modify";





	public static final String SUCCESS_SCHEDULEEXCEPTION_BLOCK = "success.adm.schedule.block.label";
	public static final String SUCCESS_SCHEDULEEXCEPTION_UNBLOCK = "success.adm.schedule.unblock.label";


	public static final String SCHEDULEEXCEPTION_SAME_DATES = "schedule.exception.msg.samedate.message";
	
	public static final String ERROR_ADM_SCHEDULE_MODIFY_IS_LOCK = "success.adm.schedule.modify.state.blocked";

	public static final String ERROR_ADM_SCHEDULE_EXCEPTION_REVOKE = "success.adm.scheduleException.delete.state.revoke";
	public static final String ERROR_ADM_SCHEDULE_EXCEPTION_DELETE = "error.adm.exceptionschedule.delete.state.registered";



	public static final String SCHEDULE_CONFIRM_REGISTER = "schedule.confirm.register";
	public static final String SCHEDULE_CONFIRM_MODIFY = "schedule.confirm.modify";

	public static final String SCHEDULE_MESSAGE_REGISTER = "schedule.msg.register.success";
	public static final String SCHEDULE_MESSAGE_MODIFY = "schedule.msg.modify.success";

	public static final String SCHEDULE_MESSAGE_REJECT = "schedule.msg.reject.success";

	public static final String SCHEDULE_CONFIRM_HEADER_REGISTER = "lbl.header.alert.confirm";
	public static final String SCHEDULE_CONFIRM_HEADER_MODIFY = "opc.adm.schedule.title.modification";
	public static final String ERROR_SCHEDULE_ALREADY_MODIFIED="error.schedule.already.modified";
	public static final String ERROR_SCHEDULE_ALREADY_BLOCKED="error.schedule.already.blocked";
	public static final String ERROR_SCHEDULE_ALREADY_UNBLOCKED="error.schedule.already.unblocked";
	public static final String HEADER_MSG_SCHEDULE_SUCCESS_REGISTRATION="lbl.header.alert.success";

	public static final String ERROR_ADM_EXCEPCION_USER_PRIVILEDGES_NO_EXIST = "error.adm.exception.user.privileges.no.exists";
	public static final String SCHEDULE_BLOCK_SUCCESS = "success.adm.schedule.block.label";
	public static final String SCHEDULE_UNBLOCK_SUCCESS = "success.adm.schedule.unblock.label";

	public static final String ERROR_SELECT_BLOCK = "message.select.block";
	public static final String ERROR_SELECT_UNBLOCK = "message.select.unblock";
	public static final String ERROR_SELECT_DELETE = "message.select.delete";
	public static final String ERROR_SELECT_MODIFY = "message.select.modify";
	public static final String ERROR_RECORD_NO_SELECTED = "message.record.no.selected";

	public static final String USER_ERROR_NAME_USER_REPEATED = "user.error.name.user.repeated";
	public static final String USER_ERROR_SELECT_PROFILE = "user.error.select.profile";
	public static final String USER_ERROR_SELECT_NOT_FOUND_PROFILE = "user.error.select.no.found.profile";
	public static final String USER_ERROR_SELECT_PROFILE_ALLOWED = "user.error.select.allowed.profile";
	public static final String USER_ERROR_SELECT_NOT_FOUND_PROFILE_ALLOWED = "user.error.select.no.found.allowed.profile";

	public static final String USER_REGISTER_SUCCESS = "user.msg.register.success";
	public static final String USER_MODIFY_SUCCESS = "user.msg.modify.success";
	public static final String USER_RESET_PASSWORD_SUCCESS = "user.msg.reset.password.success";
	public static final String USER_RESET_MFA_SUCCESS = "user.msg.reset.mfa.success";
	
	public static final String SESSION_EXPIRED = "admin.your.session.expired";
	public static final String SESSION_EXPIRED_USER_BLOCKED = "admin.your.session.expired.userBlocked";
	public static final String SESSION_EXPIRED_USER_UNREGISTER = "admin.your.session.expired.userUnregister";
	
	public static final String USER_CONFIRM_SUCCESS = "user.msg.confirm.success";
	public static final String USER_CONFIRM_SUCCESS_MAIL = "user.msg.confirm.success.mail";
	public static final String USER_REJECT_SUCCESS = "user.msg.reject.success";
	public static final String USER_REJECT_SUCCESS_MAIL = "user.msg.reject.success.mail";
	public static final String USER_ERROR_REJECT_RECENT = "user.error.reject.recent";
	public static final String SESSION_CLOSED_BY_REMOTE_USER = "login.remoteuser.close.currentsession";
	public static final String IDLE_TIME_EXPIRED = "idle.session.expired";
	public static final String SESSION_EXPIRED_SYSTEM_PROFILE_BLOCKED="system.profile.blocked";
	
	public static final String PUSH_CHANNEL = "messageChannel";
	public static final String PUSH_MSG = "/message";
	public static final String PUSH_MSG1 = "/message1";
	public static final String PROCESS_TITLE = "login.session.lbl.processes";
	public static final String MESSAGES_TITLE = "login.session.lbl.messages";

	public static final String USER_ERROR_MODIFY_NO_SELECTED = "user.error.modify.no.selected";
	public static final String USER_ERROR_MODIFY_STATE_BLOCKED = "user.error.modify.state.blocked";
	public static final String USER_ERROR_MODIFY_STATE_REJECTED = "user.error.modify.state.rejected";
	public static final String USER_ERROR_MODIFY_STATE_REGISTERED = "user.error.modify.state.registered";
	public static final String USER_ERROR_BLOCK_NO_SELECTED = "user.error.block.no.selected";
	public static final String USER_ERROR_BLOCK_STATE_BLOCKED = "user.error.block.state.blocked";
	public static final String USER_ERROR_BLOCK_STATE_REGISTERED = "user.error.block.state.registered";
	public static final String USER_ERROR_BLOCK_STATE_REJECTED = "user.error.block.state.rejected";
	public static final String USER_ERROR_UNBLOCK_NO_SELECTED = "user.error.unblock.no.selected";
	public static final String USER_ERROR_UNBLOCK_STATE_REGISTERED = "user.error.unblock.state.registered";
	public static final String USER_ERROR_UNBLOCK_STATE_BLOCKED = "user.error.unblock.state.blocked";
	public static final String USER_ERROR_UNBLOCK_STATE_REJECTED = "user.error.unblock.state.rejected";
	public static final String USER_ERROR_REJECT_NO_SELECTED = "user.error.reject.no.selected";
	public static final String USER_ERROR_REJECT_STATE_REJECTED = "user.error.reject.state.rejected";
	public static final String USER_ERROR_REJECT_STATE_CONFIRMED = "user.error.reject.state.confirmed";
	public static final String USER_ERROR_REJECT_STATE_BLOCKED = "user.error.reject.state.blocked";
	public static final String USER_ERROR_REGISTER_NO_SELECTED = "user.error.register.no.selected";
	public static final String USER_ERROR_UNBLOCK_STATE_NO_BLOCK = "user.error.unblock.state.no.block";
	public static final String USER_ERROR_MODIFY = "user.error.modify";
	public static final String USER_ERROR_BLOCK = "user.error.block";
	public static final String USER_ERROR_REJECT = "user.error.reject";
	public static final String USER_ERROR_CONFIRM = "user.error.confirm";
	public static final String USER_ERROR_REGISTER_MOST_ONE_PROFILE = "user.error.register.most.one.pprofile";	

	public static final String USER_ERROR_CONFIRM_NO_SELECTED = "user.error.confirm.no.selected";
	public static final String USER_ERROR_CONFIRM_STATE_CONFIRMED = "user.error.confirm.state.confirmed";
	public static final String USER_ERROR_CONFIRM_STATE_REJECTED = "user.error.confirm.state.rejected";
	public static final String USER_ERROR_CONFIRM_STATE_BLOCKED = "user.error.confirm.state.blocked";

	public static final String ERROR_ADM_EXCEPTIONUSER_REQUIRED = "error.adm.exception.user.required";

	public static final String ERROR_LOGIN_ERROR_TOKEN_NOT_GENERATED = "login.error.token.not.generated";
	public static final String ERROR_LOGIN_ERROR_TOKEN_INCORRECT = "login.error.token.incorrect";
	public static final String ERROR_LOGIN_USER_INVALID_PASSWORD = "login.error.user.password.invalid";
	public static final String ERROR_LOGIN_USER_BLOCKED = "login.user.locked";
	public static final String ERROR_LOGIN_USER_UNREGISTER = "login.user.unregister";
	public static final String ERROR_LOGIN_USER_INACTIVE="login.user.inactive";
	public static final String ERROR_LOGIN_USER_DISABLED="login.user.disabled";
	public static final String ERROR_LOGIN_HOLIDAY = "login.error.holiday";
	public static final String ERROR_LOGIN_INVALID_SCHEDULE = "login.error.schedule.invalid";
	public static final String ERROR_LOGIN_SELECT_SYSTEM_SCHEDULE_INVALID = "login.error.select.system.schedule.invalid";
	public static final String ERROR_LOGIN_ACTIVE_DIRECTORY="login.error.user.password.activedirectory";

	public static final String No_MENUS_AVAILABLE_FOR_SYSTEM ="login.error.select.system.no.menus";
			
	public static final String ERROR_ADM_EXCEPTIONUSER_DATE_INITIAL = "error.adm.exception.user.date.initial";

	public static final String ERROR_ADM_EXCEPTIONUSER_DATE_FINAL = "error.adm.exception.user.date.final";

	public static final String MESSAGE_MODIFY_WITHOUTCHANGES = "message.alert.modify.withoutchanges";

	public static final String ADM_OPTION_TABVIEW_TAB_HEADER_ADD = "adm.opt.tabview.tab.header.add";

	public static final String WARNING_ADM_EXCEPTIONSCHEDULE_SYSTEM_NO_SELECTED = "warning.adm.exceptionschedule.system.no.selected";
	
	public static final String WARNING_ADM_EXCEPTIONSCHEDULE_TYPE_OF_CLIENT_NO_SELECTED = "exception.type.client.required";
			
	public static final String WARNING_ADM_EXCEPTIONSCHEDULE_TYPE_OF_INSTITUTION_NO_SELECTED ="exception.type.institution.required";
	
	public static final String WARNING_ADM_EXCEPTIONSCHEDULE_INSTITUTION_NO_SELECTED ="exception.institution.required"; 

	public static final String ERROR_ADM_OPTION_URLOPTION_REQUIRED = "error.adm.option.urloption.required";
	
	public static final String EEROR_URL_PATTERN = "error.adm.url.pattern";

	public static final String ERROR_ADM_OPTION_PRIVILEGES_REQUIRED = "error.adm.option.privileges.required";

	public static final String LBL_CONFIRM_ACTION = "lbl.confirm.action";

	public static final String LBL_CONFIRM_ADD_OPTION_ACTION = "lbl.confirm.add.option.action";

	public static final String LBL_CONFIRM_ADD_MODULE_ACTION = "lbl.confirm.add.module.action";

	public static final String LBL_CONFIRM_REJECT_OPTION_ACTION = "lbl.confirm.reject.option.action";

	public static final String LBL_CONFIRM_REJECT_MODULE_ACTION = "lbl.confirm.reject.module.action";
	
	public static final String LBL_CONFIRM_REJECT_BLOCK_OPTION_ACTION = "lbl.confirm.reject.block.option.action";

	public static final String LBL_CONFIRM_REJECT_BLOCK_MODULE_ACTION = "lbl.confirm.reject.block.module.action";
	
	public static final String LBL_CONFIRM_REJECT_UNBLOCK_OPTION_ACTION = "lbl.confirm.reject.unblock.option.action";

	public static final String LBL_CONFIRM_REJECT_UNBLOCK_MODULE_ACTION = "lbl.confirm.reject.unblock.module.action";

	public static final String LBL_CONFIRM_BLOCK_OPTION_ACTION = "lbl.confirm.block.option.action";

	public static final String LBL_CONFIRM_BLOCK_MODULE_ACTION = "lbl.confirm.block.module.action";
	
	public static final String LBL_CONFIRM_BLOCK_OPTION_REQUEST_ACTION = "lbl.confirm.block.option.request.action";

	public static final String LBL_CONFIRM_BLOCK_MODULE_REQUEST_ACTION = "lbl.confirm.block.module.request.action";

	public static final String LBL_CONFIRM_UNBLOCK_OPTION_ACTION = "lbl.confirm.unblock.option.action";

	public static final String LBL_CONFIRM_UNBLOCK_MODULE_ACTION = "lbl.confirm.unblock.module.action";
	
	public static final String LBL_CONFIRM_UNBLOCK_OPTION_REQUEST_ACTION = "lbl.confirm.unblock.option.request.action";

	public static final String LBL_CONFIRM_UNBLOCK_MODULE_REQUEST_ACTION = "lbl.confirm.unblock.module.request.action";

	public static final String LBL_CONFIRM_DELETE_OPTION_ACTION = "lbl.confirm.delete.option.action";

	public static final String LBL_CONFIRM_DELETE_MODULE_ACTION = "lbl.confirm.delete.module.action";

	public static final String ERROR_ADM_OPTION_CATALOGUELANGUAGE_REQUIRED = "error.adm.option.cataloglanguage.required";

	public static final String ERROR_ADM_OPTION_CATALOGUELANGUAGE_NAME_REQUIRED = "error.adm.option.cataloglanguage.name.required";

	public static final String LBL_CONFIRM_MODIFY_OPTION_ACTION = "lbl.confirm.modify.option.action";

	public static final String LBL_CONFIRM_MODIFY_MODULE_ACTION = "lbl.confirm.modify.module.action";

	public static final String ADM_OPTION_TABVIEW_TAB_HEADER_MODIFY = "adm.opt.tabview.tab.header.modify";

	public static final String LBL_CONFIRM_OPTION_ACTION = "lbl.confirm.option.action";
	
	public static final String LBL_CONFIRM_MODULE_ACTION = "lbl.confirm.module.action";

	public static final String WARNING_ADM_OPTION_EXCEPTION_LABEL = "warning.adm.option.exception.label";

	public static final String WARNING_ADM_MODULE_EXCEPTION_LABEL = "warning.adm.module.exception.label";

	public static final String ADM_OPT_TABVIEW_TAB_HEADER_CONSULT = "adm.opt.tabview.tab.header.consult";

	public static final String SUCCESS_ADM_OPTION_ADD_LABEL = "success.adm.option.add.label";

	public static final String SUCCESS_ADM_MODULE_ADD_LABEL = "success.adm.module.add.label";

	public static final String SUCCESS_ADM_OPTION_MODIFY_LABEL = "success.adm.option.modify.label";

	public static final String SUCCESS_ADM_MODULE_MODIFY_LABEL = "success.adm.module.modify.label";

	public static final String ERROR_MESSAGE_MOTIVE_REQUIRED = "error.message.motive.required";

	public static final String SUCCESS_ADM_OPTION_CONFIRM_LABEL = "success.adm.option.confirm.label";
	
	public static final String SUCCESS_ADM_MODULE_CONFIRM_LABEL = "success.adm.module.confirm.label";

	public static final String SUCCESS_ADM_OPTION_REJECT_LABEL = "success.adm.option.reject.label";

	public static final String SUCCESS_ADM_MODULE_REJECT_LABEL = "success.adm.module.reject.label";
	
	public static final String SUCCESS_ADM_OPTION_REJECT_BLOCK_LABEL = "success.adm.option.reject.block.label";

	public static final String SUCCESS_ADM_MODULE_REJECT_BLOCK_LABEL = "success.adm.module.reject.block.label";
	
	public static final String SUCCESS_ADM_OPTION_REJECT_UNBLOCK_LABEL = "success.adm.option.reject.unblock.label";

	public static final String SUCCESS_ADM_MODULE_REJECT_UNBLOCK_LABEL = "success.adm.module.reject.unblock.label";

	public static final String SUCCESS_ADM_OPTION_REQUEST_BLOCK_LABEL = "success.adm.option.request.block.label";

	public static final String SUCCESS_ADM_MODULE_REQUEST_BLOCK_LABEL = "success.adm.module.request.block.label";
	
	public static final String SUCCESS_ADM_OPTION_BLOCK_LABEL = "success.adm.option.block.label";

	public static final String SUCCESS_ADM_MODULE_BLOCK_LABEL = "success.adm.module.block.label";

	public static final String SUCESS_ADM_OPTION_UNBLOCK_LABEL = "success.adm.option.unblock.label";

	public static final String SUCCESS_ADM_MODULE_UNBLOCK_LABEL = "success.adm.module.unblock.label";

	public static final String SUCCESS_ADM_OPTION_DELETE_LABEL = "success.adm.option.delete.label";

	public static final String SUCCESS_ADM_MODULE_DELETE_LABEL = "success.adm.module.delete.label";

	public static final String WARNING_ADM_EXCEPTION_USER_PRIVILEGES_LABEL = "warning.adm.exception.user.privileges.label";

	public static final String ERROR_ADM_EXCEPTION_USER_PRIVILEGES_SELECT_SAVE_LABEL = "error.adm.exception.user.privileges.select.save.label";

	public static final String ADM_EXCEPTION_USER_CONFIRM_REGISTER = "adm.exception.user.confirm.register";

	public static final String LBL_CONFIRM_HEADER_RECORD = "lbl.confirm.header.record";

	public static final String SUCCESS_ADM_EXCEPTION_USER_REGISTER_LABEL = "success.adm.exception.user.register.label";

	public static final String ERROR_ADM_EXCEPTION_USER_PRIVILEGES_SELECT_DELETE_LABEL = "error.adm.exception.user.privileges.select.delete.label";

	public static final String SUCCESS_ADM_EXCEPTION_USER_DELETE_LABEL = "success.adm.exception.user.delete.label";

	public static final String LOGIN_SESSION_CLOSE_SYSTEM_BODY = "login.session.close.system.body";
	/** User code validation message */
	public static final String CODE_USER_INVALID = "lbl.invaliduser";
	/** Check time difference message*/
	public static final String TIME_DIFF_MSG = "lbl.difftime";
	
	public static final String NO_EXIST_USER_LOGIN = "no.exist.user.login";
	public static final String AUDIT_USER_REQUIRED="audit.user.required";
	public static final String AUDIT_SYSTEM_REQUIRED="audit.msg.elcampo";
	public static final String AUDIT_ERR_MSG_SQL_NO_EXECUTED="audit.err.msg.sql.no.executed";
	public static final String AUDIT_MSG_CONFIRM_SUCCESS="audit.msg.confirm.success";
	public static final String AUDIT_MSG_DELETE_SUCCESS="audit.msg.delete.success";
	
	/** The Constant ERROR_ADM_SCHEDULE_DAYS_NO_SELECTED. */
	public static final String ERROR_ADM_SCHEDULE_DAYS_NO_SELECTED = "error.adm.schedule.days.no.selected";
	
	public static final String ERROR_ADM_SCHEDULE_DAYS_NO_HOURS = "error.adm.schedule.days.no.hours";
	
	public static final String ERROR_MSG_STATE_ALREADY_CONFIRMED="audit.err.msg.activate.confirm";
	
	public static final String ERROR_MSG_STATE_DEACTIVATED = "audit.err.msg.deactivate";
	
	public static final String ERROR_MSG_STATE_MODIFY = "audit.err.msg.modify";
	
	public static final String ERROR_MSG_STATE_DELETE ="audit.err.msg.eliminar";
	
	public static final String ERROR_MSG_SELECT="audit.err.msg.grabar";
	
	public static final String ERROR_MSG_SYSTEM_HAS_NO_TABLE="audit.err.msg.system.has.no.table";
	public static final String ERROR_MSG_SELECTONETABLE="audit.err.msg.tablenotselect.grabar";
	
	public static final String ERROR_WRONG_RECORD_SELECTED="error.wrongrecord.selected";
	public static final String ERROR_WRONG_RECORD_MODIFY="error.wrongrecord.modify";
	public static final String ERROR_SELECT_ONE_RECORD="error.msg.select.onerecord";
	public static final String ADM_EXCEPTION_USER_CONFIRM_MODIFY="adm.exception.user.confirm.modify";
	public static final String SUCCESS_ADM_EXCEPTION_USER_MODIFY_LABEL="success.adm.exception.user.modify.label";
	
	public static final String ERROR_SYSTEM_OPTIONS_ALREADY_BLOCKED="error.options.already.block";
	public static final String ERROR_SYSTEM_OPTIONS_ALREADY_UNBLOCKED="error.options.already.unblock";
	public static final String ERROR_SYSTEM_OPTIONS_ALREADY_MODIFY="error.options.already.modify";
	public static final String ERROR_SYSTEM_MODULE_ALREADY_MODIFY="error.module.already.modify";
	public static final String ERROR_SYSTEM_OPTIONS_ALREADY_CONFIRMED="error.options.already.confirm";
	public static final String CNF_USER_STATUS="cnf.user.status";
	public static final String ERROR_SELECTION_ALREADY_MODIFIED="error.selection.already.modificar";
	public static final String ERROR_SELECTION_ALREADY_DELETED="error.selection.already.eliminar";
	public static final String SCHEDULE_EXCEPTION_ERROR_MODIFIED_RECENT="schedule.exception.error.modify.recent";
	public static final String CNF_USER_REMOVE="lbl.cnf.delete.user";
	public static final String CNF_USER_ADD="lbl.cnf.add.user";
	public static final String SUCCESS_USER_REMOVE="lbl.success.delete.user";
	public static final String SUCCESS_USER_ADD="lbl.success.add.user";

	public static final String RULE_ADDED_SUCCESSFULLY="lbl.rule.add.success";
	public static final String RULE_DESACTIVE_SUCCESSFULLY="lbl.rule.desative.success";
	public static final String CNF_ADD_NEWRULE="lbl.cnf.new.rule";
	public static final String CNF_DELETE_RULE="audit.msg.reject";
	public static final String CNF_DELETE_MULTIPLERULE="audit.msg.reject.records";
	public static final String ERROR_SELECT_RECORD_TODELETE="audit.msg.select.record";
	
	public static final String 	MOTIVE_REQUIRED="lbl.motive.option.action";
	public static final String USER_CONFIRM_HEADER_REGISTER="lbl.confirm.header.record";
	public static final String USER_CONFIRM_REGISTER="user.confirm.register";
	public static final String USER_CONFIRM_HEADER_MODIFICATION="lbl.confirm.header.modification";
	public static final String USER_CONFIRM_MODIFICATION="user.confirm.modify";
	public static final String USER_CONFIRM_HEARDERNORECSELECT="user.confirm.norecselect";
	public static final String USER_DOC_NUMBER_REPEATED ="user.doc.number.repeat";
	
	public static final String REPORT_SEND_SUCCESSFULLY="report.msg.success";
	public static final String REPORT_SEND_FAILURE="report.msg.failure";
	public static final String REPORT_SEND_ERROR="report.msg.error";
	public static final String REPORT_FILTERS_LOAD_ERROR="report.msg.load.error";
	public static final String REPORT_HEADING_INFO="report.msg.header";
	public static final String REPORT_SEND_SUCCESS_MSG="admin.reportes.msg.report.success";
	
	public static final String USER_EXCEPTION_EXCEPTIONNAME_INVALID="adm.exception.error.exceptionname";
	
	public static final String ORDER_ALREADY_REGISTERED="adm.opt.order.exists";
	
	public static final String ERROR_VALIDATION_USER_EXCEPTION_ALREADY_EXISTS="error.validation.exception.already.exists";
	
	// confirmation dialogs headers
	public static final String DIALOG_HEADER_CONFIRM="lbl.header.alert.confirm";
	
	public static final String DIALOG_HEADER_REJECT="lbl.header.alert.reject";
	public static final String DIALOG_HEADER_APPROVE="lbl.header.alert.approve";
	public static final String DIALOG_HEADER_MODIFY="lbl.header.alert.modify";
	public static final String DIALOG_HEADER_ANNUL="lbl.header.alert.annul";
	public static final String DIALOG_HEADER_REGISTER="lbl.header.alert.register";
	public static final String DIALOG_USER_ERROR="error.adm.schedule.user";
	public static final String DIALOG_HEADER_SUCCESS="lbl.header.alert.success";
	public static final String DIALOG_HEADER_MOTIVE_REJECT="lbl.header.alert.motiveReject";
	public static final String DIALOG_HEADER_MOTIVE_ANNUL="lbl.header.alert.motiveAnnul";
	public static final String DIALOG_HEADER_REVIEW="lbl.header.alert.review";
	public static final String DIALOG_HEADER_ERROR="lbl.header.alert.error";
	public static final String DIALOG_HEADER_WARNING="lbl.header.alert.warning";
	public static final String DIALOG_HEADER_ALERT="lbl.header.alert.alert";
	public static final String DIALOG_HEADER_BLOCKMOTIVE="lbl.header.alert.motiveblock";
	public static final String DIALOG_HEADER_UNBLOCKMOTIVE="lbl.header.alert.motiveunblock";
	public static final String DIALOG_HEADER_DELETEMOTIVE="lbl.header.alert.motiveEliminar";
	
	public static final String DIALOG_FILE_SIZE_ERROR="opt.adm.system.fupl.error.max.size";
	
	public static final String USER_PASSWORD_MINLENGTH="user.confirm.reset.minLength";
	public static final String SYSTEM_OPTION_ORDER_REQUIRED="error.adm.option.order.required";
	
	public static final String USER_PWD_WRONG_PATTERN="pwd.pattern.wrong";
	
	public static final String SCREEN_ALREADY_OPENED="msg.screen.alread.opened";
	
	public static final String PWD_CHANGE_FAIL="pwd.change.fail";
	public static final String PWD_AUTH_FAIL="pwd.auth.fail";
	public static final String PWD_NOTMATCH_CONFIRM="pwd.notmatch.confirm";
	
	public static final String DAY_OF_WEEK="job.schedule.dayOfWeek";
	public static final String HOUR="job.schedule.hour";
	public static final String MINUTE="job.schedule.minute";
	public static final String SECOND="job.schedule.second";
	public static final String YEAR="job.schedule.year";
	
	/**
	 * Holds the Required message of Schedule type
	 */
	public static final String REQUIRED_SCHEDULETYPE="required.scheduletype";
	/**
	 * Holds the Required message of Client type
	 */
	public static final String REQUIRED_CLIENTTYPE="required.clienttype";
	
	/**
	 * Holds the Required message of Client type
	 */
	public static final String REQUIRED_INSTITUTETYPE="required.institutiontype";
	
	/**
	 * Holds the Date with time in seconds with  AM/PM marker
	 */
	public static final String DATE_FORMAT_WITH_SECONDS = "date.time.seconds.marker.format";
	
	public static final String HISTORICAL_STATE_LOGGER ="historicalStateLogger";

	/** The Constant MESSAGES_SUCCESFUL. */
	public static final String MESSAGES_SUCCESFUL="messages.succesful";
	
	/** The Constant MESSAGES_REGISTER. */
	public static final String MESSAGES_REGISTER="messages.register";
	
	/** The Constant MESSAGES_CONFIRM. */
	public static final String MESSAGES_CONFIRM="messages.confirm";
	
	/** The Constant MESSAGES_REJECT. */
	public static final String MESSAGES_REJECT="messages.reject";
	
	/** The Constant MESSAGES_ALERT. */
	public static final String MESSAGES_ALERT="messages.alert";
	
	/** The Constant MESSAGES_WARNING. */
	public static final String MESSAGES_WARNING="messages.warning";
	
	//-------------------------------------------------------------------------------------------------
	
	public static final String SYSTEM_CONFIRM_REGISTER_ERROR = "system.confirm.register.error";
	
	public static final String SYSTEM_CONFIRM_REGISTER_MESSAGE = "system.confirm.register.message";

	public static final String SYSTEM_CONFIRM_REGISTER_SUCCESS = "system.confirm.register.success";
	
	public static final String SYSTEM_CONFIRM_REGISTER_NOTIFICATION = "system.confirm.register.notification";
	
	//-------------------------------------------------------------------------------------------------
	
	public static final String SYSTEM_REJECT_REGISTER_ERROR = "system.reject.register.error";
	
	public static final String SYSTEM_REJECT_REGISTER_MESSAGE = "system.reject.register.message";
	
	public static final String SYSTEM_REJECT_REGISTER_SUCCESS="system.reject.register.success";
	
	public static final String SYSTEM_REJECT_REGISTER_NOTIFICATION="system.reject.register.notification";
	
	//-------------------------------------------------------------------------------------------------
	
	public static final String SYSTEM_BLOCK_ERROR = "system.block.error";
	
	public static final String SYSTEM_REQUEST_BLOCK_MESSAGE = "system.request.block.message";
	
	public static final String SYSTEM_REQUEST_BLOCK_SUCCESS = "system.request.block.success";
	
	public static final String SYSTEM_REQUEST_BLOCK_NOTIFICATION = "system.request.block.notification";
	
	public static final String SYSTEM_BLOCK_MESSAGE="system.block.message";	
	
	public static final String SYSTEM_BLOCK_SUCCESS = "system.block.success";
	
	public static final String SYSTEM_BLOCK_NOTIFICATION = "system.block.notification";
	
	public static final String SYSTEM_REJECT_BLOCK_MESSAGE = "system.reject.block.message";
	
	public static final String SYSTEM_REJECT_BLOCK_SUCCESS = "system.reject.block.success";
	
	public static final String SYSTEM_REJECT_BLOCK_NOTIFICATION = "system.reject.block.notification";
	
	//-------------------------------------------------------------------------------------------------
	
	public static final String SYSTEM_UNBLOCK_ERROR = "system.unblock.error";
	
	public static final String SYSTEM_UNBLOCK_MESSAGE="system.unblock.message";	
	
	public static final String SYSTEM_UNBLOCK_SUCCESS="system.unblock.success";
	
	public static final String SYSTEM_UNBLOCK_NOTIFICATION="system.unblock.notification";
	
	public static final String SYSTEM_REQUEST_UNBLOCK_MESSAGE = "system.request.unblock.message";
	
	public static final String SYSTEM_REQUEST_UNBLOCK_SUCCESS = "system.request.unblock.success";
	
	public static final String SYSTEM_REQUEST_UNBLOCK_NOTIFICATION="system.request.unblocked.notification";
	
	public static final String SYSTEM_REJECT_UNBLOCK_MESSAGE="system.reject.unblock.message";
	
	public static final String SYSTEM_REJECT_UNBLOCK_SUCCESS = "system.reject.unblock.success";

	public static final String SYSTEM_REJECT_UNBLOCK_NOTIFICATION = "system.reject.unblock.notification"; 
	
	//-------------------------------------------------------------------------------------------------
	
	public static final String SYSTEM_PROFILE_CONFIRM_REGISTER_ERROR = "system.profile.confirm.register.error";
	
	public static final String SYSTEM_PROFILE_CONFIRM_MESSAGE = "system.profile.confirm.message";
	
	public static final String SYSTEM_PROFILE_CONFIRM_SUCCESS = "system.profile.confirm.success";
	
	
	//-------------------------------------------------------------------------------------------------
	
	public static final String SYSTEM_PROFILE_REJECT_REGISTER_ERROR = "system.profile.reject.register.error";
	
	public static final String SYSTEM_PROFILE_REJECT_REGISTER_MESSAGE = "system.profile.reject.register.message";
	
	public static final String SYSTEM_PROFILE_REJECT_REGISTER_SUCCESS = "system.profile.reject.register.success";
	
	public static final String SYSTEM_PROFILE_REJECT_REGISTER_NOTIFICATION = "system.profile.reject.register.notification";
	
	//-------------------------------------------------------------------------------------------------
	
	public static final String SYSTEM_PROFILE_BLOCK_ERROR = "system.profile.block.error";
	
	public static final String SYSTEM_PROFILE_REQUEST_BLOCK_MESSAGE = "system.profile.request.block.message";
	
	public static final String SYSTEM_PROFILE_REQUEST_BLOCK_SUCCESS = "system.profile.request.block.success";
	
	public static final String SYSTEM_PROFILE_REQUEST_BLOCK_NOTIFICATION = "system.profile.request.block.notification";
	
	
	public static final String SYSTEM_PROFILE_REJECT_BLOCK_MESSAGE = "system.profile.reject.block.message";
	
	public static final String SYSTEM_PROFILE_REJECT_BLOCK_SUCCESS  = "system.profile.reject.block.success";
	
	public static final String SYSTEM_PROFILE_REJECT_BLOCK_NOTIFICATION  = "system.profile.reject.block.notification";
	
	
	public static final String SYSTEM_PROFILE_BLOCK_MESSAGE = "system.profile.block.message";
	
	public static final String SYSTEM_PROFILE_BLOCK_SUCCESS = "system.profile.block.success";
	
	public static final String SYSTEM_PROFILE_BLOCK_NOTIFICATION = "system.profile.block.notification";
	
	//-------------------------------------------------------------------------------------------------
	
	public static final String SYSTEM_PROFILE_UNBLOCK_ERROR = "system.profile.unblock.error";
	
	public static final String SYSTEM_PROFILE_REQUEST_UNBLOCK_MESSAGE = "system.profile.request.unblock.message";
	
	public static final String SYSTEM_PROFILE_REQUEST_UNBLOCK_SUCCESS = "system.profile.request.unblock.success";
	
	public static final String SYSTEM_PROFILE_REQUEST_UNBLOCK_NOTIFICATION = "system.profile.block.notification";
	
	public static final String SYSTEM_PROFILE_UNBLOCK_MESSAGE = "system.profile.unblock.message";
	
	public static final String SYSTEM_PROFILE_UNBLOCK_SUCCESS = "system.profile.unblock.success";
	
	public static final String SYSTEM_PROFILE_UNBLOCK_NOTIFICATION = "system.profile.unblock.notification";
	
	public static final String SYSTEM_PROFILE_REJECT_UNBLOCK_MESSAGE = "system.profile.reject.unblock.message";
	
	public static final String SYSTEM_PROFILE_REJECT_UNBLOCK_SUCCESS = "system.profile.reject.unblock.success";

	public static final String SYSTEM_PROFILE_REJECT_UNBLOCK_NOTIFICATION = "system.profile.reject.unblock.notification";
	
	//-------------------------------------------------------------------------------------------------
	
	public static final String SYSTEM_SCHEDULE_CONFIRM_ERROR = "system.schedule.confirm.error";
	
	public static final String SYSTEM_SCHEDULE_CONFIRM_MESSAGE = "system.schedule.confirm.message";
	
	public static final String SYSTEM_SCHEDULE_CONFIRM_SUCCESS = "system.schedule.confirm.success";
	
	public static final String SYSTEM_SCHEDULE_CONFIRM_NOTIFICATION = "system.schedule.confirm.notification";
	
	public static final String SYSTEM_SCHEDULE_REJECT_ERROR = "system.schedule.reject.error";
	
	public static final String SYSTEM_SCHEDULE_REJECT_MESSAGE = "system.schedule.reject.message";
	
	public static final String SYSTEM_SCHEDULE_REJECT_SUCCESS = "system.schedule.reject.success";
	
	public static final String SYSTEM_SCHEDULE_REJECT_NOTIFICATION = "system.schedule.reject.notification";
	
	//-------------------------------------------------------------------------------------------------
	
	public static final String SYSTEM_SCHEDULE_REQUEST_BLOCK_MESSAGE = "system.schedule.request.block.message";
	
	public static final String SYSTEM_SCHEDULE_REQUEST_BLOCK_SUCCESS = "system.schedule.request.block.success";
	
	public static final String SYSTEM_SCHEDULE_REQUEST_BLOCK_NOTIFICATION = "system.schedule.request.block.notification";
		
	
	public static final String SYSTEM_SCHEDULE_REJECT_BLOCK_MESSAGE = "system.schedule.reject.block.message";
	
	public static final String SYSTEM_SCHEDULE_REJECT_BLOCK_SUCCESS = "system.schedule.reject.block.success";
	
	public static final String SYSTEM_SCHEDULE_REJECT_BLOCK_NOTIFICATION = "system.schedule.reject.block.notification";	
		
	
	public static final String SYSTEM_SCHEDULE_BLOCK_ERROR = "system.schedule.block.error";
	
	public static final String SYSTEM_SCHEDULE_BLOCK_MESSAGE = "system.schedule.block.message";
	
	public static final String SYSTEM_SCHEDULE_BLOCK_SUCCESS = "system.schedule.block.success";
	
	public static final String SYSTEM_SCHEDULE_BLOCK_NOTIFICATION = "system.schedule.block.notification";
	
	//-------------------------------------------------------------------------------------------------
	
	public static final String SYSTEM_SCHEDULE_REQUEST_UNBLOCK_MESSAGE = "system.schedule.request.unblock.message";
	
	public static final String SYSTEM_SCHEDULE_REQUEST_UNBLOCK_SUCCESS = "system.schedule.request.unblock.success";
	
	public static final String SYSTEM_SCHEDULE_REQUEST_UNBLOCK_NOTIFICATION = "system.schedule.request.unblock.notification";
	
	
	public static final String SYSTEM_SCHEDULE_REJECT_UNBLOCK_MESSAGE = "system.schedule.reject.unblock.message";
	
	public static final String SYSTEM_SCHEDULE_REJECT_UNBLOCK_SUCCESS = "system.schedule.reject.unblock.success";
	
	public static final String SYSTEM_SCHEDULE_REJECT_UNBLOCK_NOTIFICATION = "system.schedule.reject.unblock.notification";
	
		
	public static final String SYSTEM_SCHEDULE_UNBLOCK_ERROR = "system.schedule.unblock.error";
	
	public static final String SYSTEM_SCHEDULE_UNBLOCK_MESSAGE = "system.schedule.unblock.message";
	
	public static final String SYSTEM_SCHEDULE_UNBLOCK_SUCCESS = "system.schedule.unblock.success";
	
	public static final String SYSTEM_SCHEDULE_UNBLOCK_NOTIFICATION = "system.schedule.unblock.notification";
	
	//-------------------------------------------------------------------------------------------------
	
	public static final String SYSTEM_SCHEDULE_EXCEPTION_CONFIRM_ERROR = "system.schedule.exception.confirm.error";
	
	public static final String SYSTEM_SCHEDULE_EXCEPTION_CONFIRM_MESSAGE = "system.schedule.exception.confirm.message";
	
	public static final String SYSTEM_SCHEDULE_EXCEPTION_CONFIRM_SUCCESS = "system.schedule.exception.confirm.success";
	
	public static final String SYSTEM_SCHEDULE_EXCEPTION_CONFIRM_NOTIFICATION = "system.schedule.exception.confirm.notification";
	
	public static final String SYSTEM_SCHEDULE_EXCEPTION_REJECT_ERROR = "system.schedule.exception.reject.error";
	
	public static final String SYSTEM_SCHEDULE_EXCEPTION_REJECT_MESSAGE = "system.schedule.exception.reject.message";
	
	public static final String SYSTEM_SCHEDULE_EXCEPTION_REJECT_SUCCESS = "system.schedule.exception.reject.success";
	
	public static final String SYSTEM_SCHEDULE_EXCEPTION_REJECT_NOTIFICATION = "system.schedule.exception.reject.notification";
		
	public static final String SYSTEM_SCHEDULE_EXCEPTION_DELETE_ERROR = "system.schedule.exception.delete.error";
	
	public static final String SYSTEM_SCHEDULE_EXCEPTION_DELETE_MESSAGE = "system.schedule.exception.delete.message";
	
	public static final String SYSTEM_SCHEDULE_EXCEPTION_DELETE_SUCCESS = "system.schedule.exception.delete.success";
	
	public static final String SYSTEM_SCHEDULE_EXCEPTION_DELETE_NOTIFICATION = "system.schedule.exception.delete.notification";
	
	//-------------------------------------------------------------------------------------------------
	
	public static final String USER_BLOCK_SUCCESS = "user.block.success";
	
	public static final String USER_BLOCK_NOTIFICATION = "user.block.notification";
	
	public static final String USER_REQUEST_BLOCK_SUCCESS = "user.request.block.success";
	
	public static final String USER_REQUEST_BLOCK_NOTIFICATION = "user.request.block.notification";
	
	public static final String USER_REJECT_BLOCK_SUCCESS = "user.reject.block.success";
	
	public static final String USER_REJECT_BLOCK_NOTIFICATION = "user.reject.block.notification";
	
	//-------------------------------------------------------------------------------------------------
	
	public static final String USER_UNBLOCK_SUCCESS = "user.unblock.success";
	
	public static final String USER_UNBLOCK_NOTIFICATION = "user.unblock.notification";
	
	public static final String USER_REQUEST_UNBLOCK_SUCCESS = "user.request.unblock.success";
	
	public static final String USER_REQUEST_UNBLOCK_NOTIFICATION = "user.request.unblock.notification";
	
	public static final String USER_REJECT_UNBLOCK_SUCCESS = "user.reject.unblock.success";
	
	public static final String USER_REJECT_UNBLOCK_NOTIFICATION = "user.reject.unblock.notification";
	
	//-------------------------------------------------------------------------------------------------
	
	public static final String USER_REJECT_UNREGISTER_SUCCESS = "user.reject.unregister.success";
	
	public static final String USER_REJECT_UNREGISTER_NOTIFICATION = "user.reject.unregister.notification";
	
	public static final String USER_REQUEST_UNREGISTER_SUCCESS = "user.request.unregister.success";
	
	public static final String USER_REQUEST_UNREGISTER_NOTIFICATION = "user.request.unregister.notification";
	
	public static final String USER_UNREGISTER_SUCCESS = "user.unregister.success";
	
	public static final String USER_UNREGISTER_NOTIFICATION = "user.unregister.notification";
	
	//-------------------------------------------------------------------------------------------------
	
	public static final String USER_UNREGISTER_ERROR = "user.unregister.error";
	
	public static final String USER_MANUAL_NOT_FILE = "user.manual.not.file.message";
	
	public static final String SUBSIDIARY_CONFIRM_REGISTER = "subsidary.confirm.register";
	
	public static final String SUBSIDIARY_CONFIRM_MODIFY = "subsidary.confirm.modify";
	
	public static final String SUBSIDIARY_MSG_REGISTER_SUCCESS = "subsidary.msg.register.success";
	
	public static final String SUBSIDIARY_MSG_MODIFY_SUCCESS = "subsidary.msg.modify.success";

}
