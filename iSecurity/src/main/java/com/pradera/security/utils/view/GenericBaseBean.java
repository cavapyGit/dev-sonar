package com.pradera.security.utils.view;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;
import java.util.regex.Matcher;

import javax.ejb.EJB;
import javax.enterprise.event.Event;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.apache.myfaces.extensions.cdi.core.api.scope.conversation.Conversation;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.data.PageEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.security.model.type.UserAccountStateType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.model.HistoricalState;
import com.pradera.security.model.InstitutionsSecurity;
import com.pradera.security.model.Parameter;
import com.pradera.security.model.System;
import com.pradera.security.model.type.DefinitionType;
import com.pradera.security.model.type.SystemProfileStateType;
import com.pradera.security.model.type.UserAccountType;
import com.pradera.security.model.type.UserExceptionStateType;
import com.pradera.security.notification.facade.NotificationServiceFacade;
import com.pradera.security.systemmgmt.facade.SystemServiceFacade;
import com.pradera.security.usermgmt.facade.UserServiceFacade;
import com.pradera.security.webservices.UserAccountResource;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class GenericBaseBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 29/01/2014
 */
public class GenericBaseBean implements Serializable{
	
	/** The all holidays. */
	protected String allHolidays;
	
	/** The excepcion. */
	@Inject
	protected Event<ExceptionToCatchEvent> excepcion;
	

	/** The conversation. */
	@Inject
	private Conversation conversation;
	
	/** The Session idle Time. */
	@Inject
	@Configurable
	private Integer sessionIdleTime;
	
	@Inject 
	@Configurable 
	String allowTypesFileUpExp;
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The view operations type. */
	protected ViewOperationsType viewOperationsType;
	
	/** The operation type. */
	private Integer operationType;
	
	/** The lst systems. */
	protected List<System> lstSystems;
	
	/** The lst all systems. */
	protected List<System> lstAllSystems;
	
	/** The lst system profile status. */
	protected List<SystemProfileStateType> lstSystemProfileStatus;
    
    /** The lst motives. */
    protected List<Parameter> lstMotives;	
    
    /** The lst motives. */
    protected List<Parameter> lstBlockMotives;	
    
    /** The lst motives. */
    protected List<Parameter> lstUnBlockMotives;	
    
    /** The lst delete motives. */
    protected List<Parameter> lstDeleteMotives;	
    
	/** The lista tipo cliente. */
    protected List<Parameter> lstClientType;
    
    /** The list institutions. */
    protected List<InstitutionsSecurity> lstInstitutions;
    
    /** The lst user types. */
    protected List<UserAccountType> lstUserTypes;
    
    /** The lista estado excepcion usuario. */
    protected List<UserExceptionStateType> listUserExceptionState;
		
	/** The system service facade. */
	@EJB
    protected SystemServiceFacade systemServiceFacade;
	
	/** The user service facade. */
	@EJB
	protected UserServiceFacade userServiceFacade;
	
	/** The firs register data table. */
	protected int firsRegisterDataTable;
	
	/** The log. */
    Logger log = Logger.getLogger(GenericBaseBean.class.getName());
    
	/** The upload file size display with comma. */
	private String fUploadFileSizeDisplayWithComma="2,000";
	
	/** The upload file size. */
	private String fUploadFileSize="2000000";
	
	/** The upload imagen file size. */
	private String fUploadImagenFileSize="200";
	
	/** The upload file types. */
	private String fUploadFileTypes="jpg|jpeg|png";
	
	/** The upload file size display. */
	private String fUploadFileSizeDisplay="2000";
	
	/** The upload file types display. */
	private String fUploadFileTypesDisplay="*.jpg,*.png";	
	
	/** The xml file upload size. */
	private String xmlFileUploadSize="10000000";
	
	/** The xml file upload filt types. */
	private String xmlFileUploadFiltTypes="xml";
	
	/** The xml file upload file types display. */
	private String xmlFileUploadFileTypesDisplay="*.xml";
	
	/** The txt file upload size. */
	private String txtFileUploadSize="10000000";
	
	/** The txt file upload types. */
	private String txtFileUploadTypes="txt";
	
	/** The txt file upload size display. */
	private String txtFileUploadSizeDisplay="10000";
	
	/** The txt file upload types display. */
	private String txtFileUploadTypesDisplay="*.txt";
	
	/** The xls file upload size. */
	private String xlsFileUploadSize="10000000";
	
	/** The xls file upload filt types. */
	private String xlsFileUploadFiltTypes="xlsx";
	
	/** The xls file upload file types display. */
	private String xlsFileUploadFileTypesDisplay="*.xlsx";
	
	/** The userAccountResource. */
	@Inject
	UserAccountResource userAccountResource;
	
	/** The notification service facade. */
	@EJB
	protected NotificationServiceFacade notificationServiceFacade;
	
	/** The lst reject motives. */
	protected List<Parameter> lstRejectMotives;	
	
	/**
	 * Instantiates a new generic base bean.
	 */
	public GenericBaseBean() {
	}	
	
	/**
	 * Creates the historial state.
	 *
	 * @param tableName the table name
	 * @param beforeSate the before sate
	 * @param idRegister the id register
	 * @param historical the historical
	 * @return the historical state
	 */
	public HistoricalState createHistorialState(String tableName,Integer beforeSate,Long idRegister,HistoricalState historical){
		HistoricalState histChangeState = new HistoricalState();
		histChangeState.setTableName(tableName);
		histChangeState.setBeforeState(beforeSate);
		histChangeState.setCurrentState(historical.getCurrentState());
		histChangeState.setIdRegister(idRegister);
		histChangeState.setRegistryDate(CommonsUtilities.currentDateTime());
		histChangeState.setMotive(historical.getMotive());
		histChangeState.setMotiveDescription(historical.getMotiveDescription());
		return histChangeState;
	}
	
	/**
	 * List holidays.
	 *
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void listHolidays() throws ServiceException{		
		if(Validations.validateIsNullOrEmpty(allHolidays)) {
			allHolidays = CommonsUtilities.convertListDateToString(systemServiceFacade.getListAvailableHolidayDate());
		}
	}
	
	/**
	 * Gets the view operations type.
	 *
	 * @return the view operations type
	 */
	public ViewOperationsType getViewOperationsType() {
		return viewOperationsType;
	}
	
	/**
	 * Sets the view operations type.
	 *
	 * @param viewOperationsType the new view operations type
	 */
	public void setViewOperationsType(ViewOperationsType viewOperationsType) {
		this.viewOperationsType = viewOperationsType;
	}
	
	/**
	 * Gets the operation type.
	 *
	 * @return the operation type
	 */
	public Integer getOperationType() {
		return operationType;
	}
	
	/**
	 * Sets the operation type.
	 *
	 * @param operationType the new operation type
	 */
	public void setOperationType(Integer operationType) {
		this.operationType = operationType;
	}
	
	/**
	 * Checks if is generado.
	 *
	 * @return true, if is generado
	 */
	public boolean isGenerado(){
		return this.getOperationType().equals(ViewOperationsType.REGISTER.getCode());
	}	
	
	/**
	 * Gets the lst systems.
	 *
	 * @return the lst systems
	 */
	public List<System> getLstSystems() {
		return lstSystems;
	}
	
	/**
	 * Sets the lst systems.
	 *
	 * @param lstSystems the new lst systems
	 */
	public void setLstSystems(List<System> lstSystems) {
		this.lstSystems = lstSystems;
	}
	
	/**
	 * Gets the lst system profile status.
	 *
	 * @return the lst system profile status
	 */
	public List<SystemProfileStateType> getLstSystemProfileStatus() {
		return lstSystemProfileStatus;
	}
	
	/**
	 * Sets the lst system profile status.
	 *
	 * @param lstSystemProfileStatus the new lst system profile status
	 */
	public void setLstSystemProfileStatus(
			List<SystemProfileStateType> lstSystemProfileStatus) {
		this.lstSystemProfileStatus = lstSystemProfileStatus;
	}
	
	/**
	 * List systems by state.
	 *
	 * @param estado the estado
	 */
	@LoggerAuditWeb
	public void listSystemsByState(Integer estado){
		try{
			lstSystems = systemServiceFacade.searchSystemByStateServiceFacade(estado);		
		}
		catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * List all systems.
	 */
	public void listAllSystems(){
		try{			
			lstAllSystems = systemServiceFacade.searchAllSystemServiceFacade();			
		}
		catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Show motives.
	 */
	public void showMotives(){
		try{
		lstMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.ACTION_MOTIVES.getCode());
		}
		catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * List system profile state.
	 */
	public void listSystemProfileState(){
		lstSystemProfileStatus = SystemProfileStateType.list;
	}
	
	/**
	 * Checks if is register.
	 *
	 * @return true, if is register
	 */
	public boolean isRegister(){
		return ViewOperationsType.REGISTER.getCode().equals(this.getOperationType());
	}	
	
	/**
	 * Checks if is modify.
	 *
	 * @return true, if is modify
	 */
	public boolean isModify(){
		return ViewOperationsType.MODIFY.getCode().equals(this.getOperationType());
	}
	
	/**
	 * Checks if is detail.
	 *
	 * @return true, if is detail
	 */
	public boolean isDetail(){
		return ViewOperationsType.DETAIL.getCode().equals(this.getOperationType());
	}
	
	/**
	 * Checks if is block.
	 *
	 * @return true, if is block
	 */
	public boolean isBlock(){
		return ViewOperationsType.BLOCK.getCode().equals(this.getOperationType());
	}	
	
	/**
	 * Checks if is un block.
	 *
	 * @return true, if is un block
	 */
	public boolean isUnBlock(){
		return ViewOperationsType.UNBLOCK.getCode().equals(this.getOperationType());
	}
	
	/**
	 * Checks if is consult.
	 *
	 * @return true, if is consult
	 */
	public boolean isConsult(){
		return ViewOperationsType.CONSULT.getCode().equals(this.getOperationType());
	}
	
	/**
	 * Checks if is delete.
	 *
	 * @return true, if is delete
	 */
	public boolean isDelete(){
		return ViewOperationsType.DELETE.getCode().equals(this.getOperationType());
	}
	
	/**
	 * Checks if is confirm.
	 *
	 * @return true, if is confirm
	 */
	public boolean isConfirm(){
		return ViewOperationsType.CONFIRM.getCode().equals(this.getOperationType());
	}
	
	/**
	 * Checks if is reject.
	 *
	 * @return true, if is reject
	 */
	public boolean isReject(){
		return ViewOperationsType.REJECT.getCode().equals(this.getOperationType());
	}
	
	/**
	 * Checks if is annular.
	 *
	 * @return true, if is annular
	 */
	public boolean isAnnular(){
		return ViewOperationsType.ANULATE.getCode().equals(this.getOperationType());
	}
	
	/**
	 * Gets the firs register data table.
	 *
	 * @return the firsRegisterDataTable
	 */
	public int getFirsRegisterDataTable() {
		return firsRegisterDataTable;
	}

	/**
	 * Sets the firs register data table.
	 *
	 * @param firsRegisterDataTable the firsRegisterDataTable to set
	 */
	public void setFirsRegisterDataTable(int firsRegisterDataTable) {
		this.firsRegisterDataTable = firsRegisterDataTable;
	}
	/**
	 * Change page datatable.
	 *
	 * @param pageEvent the page event
	 */
	public void changePageDatatable (PageEvent pageEvent) {
		DataTable dtb = (DataTable) pageEvent.getSource();
		firsRegisterDataTable = pageEvent.getPage() * dtb.getRows(); 	      
	}
	/**
	 * Show message on dialog.
	 *
	 * @param keyHeaderProperty the key header property
	 * @param headerData the header data
	 * @param keyBodyProperty the key body property
	 * @param bodyData the body data
	 */
	public static void showMessageOnDialog(String keyHeaderProperty,Object[] headerData,String keyBodyProperty, Object[] bodyData){
		Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();	
		String headerMessageConfirmation = "";
		if(keyHeaderProperty!=null && !keyHeaderProperty.isEmpty()){
			headerMessageConfirmation = PropertiesUtilities.getMessage(locale, keyHeaderProperty,headerData);
		}
		String bodyMessageConfirmation = "";
		if(keyBodyProperty!=null && !keyBodyProperty.isEmpty()){
			bodyMessageConfirmation = PropertiesUtilities.getMessage(locale, keyBodyProperty,bodyData);
		}		
		JSFUtilities.putViewMap("bodyMessageView", bodyMessageConfirmation);
		JSFUtilities.putViewMap("headerMessageView", headerMessageConfirmation);
	}
	
	/**
	 * Show message on dialog Alternative.
	 *
	 * @param keyHeaderProperty the key header property
	 * @param headerData the header data
	 * @param keyBodyProperty the key body property
	 * @param bodyData the body data
	 */
	public static void showMessageOnDialog2(String keyHeaderProperty,Object[] headerData,String keyBodyProperty, Object[] bodyData){
		Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();	
		String headerMessageConfirmation = "";
		if(keyHeaderProperty!=null && !keyHeaderProperty.isEmpty()){
			headerMessageConfirmation = PropertiesUtilities.getMessage(locale, keyHeaderProperty,headerData);
		}
		String bodyMessageConfirmation = "";
		if(keyBodyProperty!=null && !keyBodyProperty.isEmpty()){
			bodyMessageConfirmation = PropertiesUtilities.getMessage(locale, keyBodyProperty,bodyData);
		}		
		JSFUtilities.putViewMap("bodyMessageView2", bodyMessageConfirmation);
		JSFUtilities.putViewMap("headerMessageView2", headerMessageConfirmation);
	}
	
	/**
	 * Gets the lst motives.
	 *
	 * @return the lst motives
	 */
	public List<Parameter> getLstMotives() {
		return lstMotives;
	}
	
	/**
	 * Sets the lst motives.
	 *
	 * @param lstMotives the new lst motives
	 */
	public void setLstMotives(List<Parameter> lstMotives) {
		this.lstMotives = lstMotives;
	}
	
	/**
	 * Listar tipo usuario.
	 */
	public void listUserType(){
		try{
		lstClientType = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.USER_TYPES.getCode());
		}
		catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Show institutions by user account type.
	 *
	 * @param idUserAccountType the id user account type
	 */
	public void showInstitutionsByUserAccountType(Integer idUserAccountType){	
		try{
			List<InstitutionsSecurity> list =userServiceFacade.searchAllInstitutionsServiceFacade();
			lstInstitutions = new ArrayList<InstitutionsSecurity>();
			if(UserAccountType.INTERNAL.getCode().equals(idUserAccountType)){
				lstInstitutions.add(list.get(0));			
			}else{
				list.remove(0);
				lstInstitutions.addAll(list);
			}
		}catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}	
	
	/**
	 * Sets the lista tipo cliente.
	 *
	 * @return the lst user account state
	 */
		
	
	/**
	 * Gets the lista estado usuario.
	 *
	 * @return the lista estado usuario
	 */
	public List<UserAccountStateType> getLstUserAccountState() {
		return UserAccountStateType.list;
	}
	
	/**
	 * Sets the lst user types.
	 *
	 * @param lstUserTypes the new lst user types
	 */
	public void setLstUserTypes(List<UserAccountType> lstUserTypes) {
		this.lstUserTypes = lstUserTypes;
	}
	
	
	/**
	 * Gets the lst client type.
	 *
	 * @return the lst client type
	 */
	public List<Parameter> getLstClientType() {
		return lstClientType;
	}

	/**
	 * Gets the lst institutions.
	 *
	 * @return the lst institutions
	 */
	public List<InstitutionsSecurity> getLstInstitutions() {
		return lstInstitutions;
	}

	/**
	 * Sets the lst institutions.
	 *
	 * @param lstInstitutions the new lst institutions
	 */
	public void setLstInstitutions(List<InstitutionsSecurity> lstInstitutions) {
		this.lstInstitutions = lstInstitutions;
	}

	/**
	 * Sets the lst client type.
	 *
	 * @param lstClientType the new lst client type
	 */
	public void setLstClientType(List<Parameter> lstClientType) {
		this.lstClientType = lstClientType;
	}

	/**
	 * Listar estado excepcion usuario.
	 */
	public void listUserExceptionState(){
		listUserExceptionState= UserExceptionStateType.list;
	}
	
	/**
	 * Gets the list user exception state.
	 *
	 * @return the list user exception state
	 */
	public List<UserExceptionStateType> getListUserExceptionState() {
		return listUserExceptionState;
	}

	/**
	 * Sets the list user exception state.
	 *
	 * @param listUserExceptionState the new list user exception state
	 */
	public void setListUserExceptionState(
			List<UserExceptionStateType> listUserExceptionState) {
		this.listUserExceptionState = listUserExceptionState;
	}

	/**
	 * Gets the lst user types.
	 *
	 * @return the lst user types
	 */
	public List<UserAccountType> getLstUserTypes() {
		return lstUserTypes;
	}

	/**
	 * Gets the boolean espaniol.
	 *
	 * @param parameter the parametro
	 * @return the boolean espaniol
	 */
	public String getBooleanSpanish(boolean parameter){
		return BooleanType.lookup.get(parameter).getValue();
	}
	
	/**
	 * Begin conversation.
	 */
	public void beginConversation() {
//		if (conversation.isTransient()) {
//		conversation.begin();
//		conversation.setTimeout(Long.valueOf(1000*60*60)); //One Hour
//		}
	}
	/**
	 * End conversation.
	 */
	public void endConversation() {
		conversation.close();
//		if (!conversation.isTransient()) {
//		conversation.end();
//		}
//		conversation.restart();
	}
	
	/**
	 * Gets the conversation id.
	 *
	 * @return the conversation id
	 */
	public String getConversationId() {
		return "1";//conversation.getId();
	}
	
	/**
	 * Gets the conversation timeout.
	 *
	 * @return the conversation timeout
	 */
	public long getConversationTimeout() {
		return  60;//conversation.getTimeout();
	}

	/**
	 * Gets the lst all systems.
	 *
	 * @return the lst all systems
	 */
	public List<System> getLstAllSystems() {
		return lstAllSystems;
	}

	/**
	 * Sets the lst all systems.
	 *
	 * @param lstAllSystems the new lst all systems
	 */
	public void setLstAllSystems(List<System> lstAllSystems) {
		this.lstAllSystems = lstAllSystems;
	}
	
	 /**
 	 * Gets the lst block motives.
 	 *
 	 * @return the lstBlockMotives
 	 */
	public List<Parameter> getLstBlockMotives() {
		return lstBlockMotives;
	}

	/**
	 * Sets the lst block motives.
	 *
	 * @param lstBlockMotives the lstBlockMotives to set
	 */
	public void setLstBlockMotives(List<Parameter> lstBlockMotives) {
		this.lstBlockMotives = lstBlockMotives;
	}

	/**
	 * Gets the lst un block motives.
	 *
	 * @return the lstUnBlockMotives
	 */
	public List<Parameter> getLstUnBlockMotives() {
		return lstUnBlockMotives;
	}

	/**
	 * Sets the lst un block motives.
	 *
	 * @param lstUnBlockMotives the lstUnBlockMotives to set
	 */
	public void setLstUnBlockMotives(List<Parameter> lstUnBlockMotives) {
		this.lstUnBlockMotives = lstUnBlockMotives;
	}
	
    /**
     * Gets the lst delete motives.
     *
     * @return the lstDeleteMotives
     */
	public List<Parameter> getLstDeleteMotives() {
		return lstDeleteMotives;
	}

	/**
	 * Sets the lst delete motives.
	 *
	 * @param lstDeleteMotives the lstDeleteMotives to set
	 */
	public void setLstDeleteMotives(List<Parameter> lstDeleteMotives) {
		this.lstDeleteMotives = lstDeleteMotives;
	}

	/**
	 * Check modify date and status.
	 *
	 * @param strTable the str table
	 * @param strColumn the str column
	 * @param searchId the search id
	 * @param modifyDate the modify date
	 * @param state the state
	 * @return the integer
	 */
	public Integer checkModifyDateAndStatus(String strTable,String strColumn,Integer searchId,Date modifyDate,Integer state){
			//Map<String, Object> parameters){
		Integer result = null;
		/*String strTable = (String)parameters.get("table");
		String strColumn = (String)parameters.get("column");
		Integer searchId = (Integer)parameters.get("searchId");
		Date modifyDate = (Date)parameters.get("modifyDate");
		Integer state = (Integer)parameters.get("state");*/
		if(Validations.validateIsNotNullAndNotEmpty(strTable) 
				&& Validations.validateIsNotNullAndNotEmpty(strColumn)
				&& Validations.validateIsNotNullAndPositive(searchId)
				&& Validations.validateIsNotNull(modifyDate)
				&& Validations.validateIsNotNullAndPositive(state)){			
			Object[] objArr = userServiceFacade.getModifyDateAndState(strTable, strColumn, searchId);
			if(Validations.validateIsNotNull(objArr) && objArr.length>0){
				Date newModifyDate = (Date) objArr[0];
				Integer newState = (Integer) objArr[1];
				if(newModifyDate.after(modifyDate)){
					result = GeneralConstants.ONE_VALUE_INTEGER;
					if(state.equals(newState)){
						result = GeneralConstants.TWO_VALUE_INTEGER;
					}
				}
			}
		}
		return result;
	}

//	/**
//	 * To send Notification Message and Emeil.
//	 *
//	 * @param userName the user name
//	 * @param businessProcess the business process
//	 * @param subject the subject
//	 * @param message the message
//	 * @param isSendMessage the is send message
//	 * @param isSendEmail the is send email
//	 */
//	public void sendNotification(String userName,Long businessProcess,String subject,String message,boolean isSendMessage,boolean isSendEmail){
//		try{			
//			log.info("Start  method sendNotification() ");
//			//send remote notification
//			UserFilterTO userFilter = new UserFilterTO();
//			userFilter.setState(UserAccountStateType.CONFIRMED.getCode());
//			userFilter.setInstitutionType(InstitutionType.DEPOSITARY.getCode());
//			userFilter.setIndExtInterface(ExternalInterfaceType.NO.getCode());
//			List<UserAccountSession> users = userAccountResource.getUsers(userFilter);
//			//use case said send notification message
//			if(isSendMessage){
//				notificationServiceFacade.sendNotificationRemote(userName, businessProcess, users, subject, message,  com.pradera.commons.httpevent.type.NotificationType.MESSAGE);
//			}
//			//use case said send notification email
//			if(isSendEmail){
//				notificationServiceFacade.sendNotificationRemote(userName,businessProcess, users, subject, message,  com.pradera.commons.httpevent.type.NotificationType.EMAIL);
//			}
//			log.info("End   method sendNotification() ");
//			
//			
//		} catch (Exception e) {
//			excepcion.fire(new ExceptionToCatch(e));
//		}
//	}

	/**
	 * Gets the session idle time.
	 *
	 * @return the sessionIdleTime
	 */
	public Integer getSessionIdleTime() {
		return sessionIdleTime;
	}
	
	/**
	 * Show message on dialog.
	 *
	 * @param headerMessageConfirmation the header message confirmation
	 * @param bodyMessageConfirmation the body message confirmation
	 */
	public static void showMessageOnDialog(String headerMessageConfirmation,String bodyMessageConfirmation){
		JSFUtilities.putViewMap("bodyMessageView", bodyMessageConfirmation);
		JSFUtilities.putViewMap("headerMessageView", headerMessageConfirmation);
	}
	
	/**
	 * F upload validate file.
	 *
	 * @param fileUploaded the file uploaded
	 * @param dialog the dialog
	 * @param matcher the matcher
	 * @param maxSize the max size
	 * @return the string
	 */
	public String fUploadValidateFile(UploadedFile fileUploaded,String dialog, Matcher matcher, String maxSize){
		return fUploadValidateFile(fileUploaded, dialog, matcher, maxSize,null);
	}
	
	/**
	 * F upload validate.
	 *
	 * @param fileUploaded the file uploaded
	 * @param dialog the dialog
	 * @param matcher the matcher
	 * @param maxSize the max size
	 * @param fUploadFileTypes the f upload file types
	 * @return the string
	 */
	public String fUploadValidate(UploadedFile fileUploaded,String dialog, Matcher matcher, String maxSize, String fUploadFileTypes){
		return fUploadValidateFile(fileUploaded, dialog, matcher, maxSize,fUploadFileTypes);
	}
	
	/**
	 * F upload validate file.
	 *
	 * @param fileUploaded the file uploaded
	 * @param dialog the dialog
	 * @param matcher the matcher
	 * @param maxSize the max size
	 * @param fUploadFileTypes the f upload file types
	 * @return the string
	 */
	public String fUploadValidateFile(UploadedFile fileUploaded,String dialog, Matcher matcher, String maxSize,String fUploadFileTypes){
		String displayName=null;
		String extensionFile = fileUploaded.getFileName().toLowerCase().substring(fileUploaded.getFileName().lastIndexOf(".")+1,fileUploaded.getFileName().length());
		Matcher matcherToUser= null;
		String maxSizeToUse=getfUploadImagenFileSize();
		
		if(matcher!=null){
			matcherToUser=matcher;
		}
		if(maxSize!=null){
			maxSizeToUse=maxSize;
		}
		
		if (matcherToUser!=null && !matcherToUser.matches()) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getMessage(PropertiesConstants.FUPL_ERROR_INVALID_FILE_TYPE));
			JSFUtilities.executeJavascriptFunction(dialog);
			return null;
		}
		
		if(fUploadFileTypes == null){
			fUploadFileTypes = this.fUploadFileTypes;
		}
		if (!fUploadFileTypes.contains(extensionFile) && matcherToUser==null) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getMessage(PropertiesConstants.FUPL_ERROR_INVALID_FILE_TYPE));
			JSFUtilities.executeJavascriptFunction(dialog);
			return null;
		}
		
		if (fileUploaded.getSize() > (Integer.valueOf(maxSizeToUse) * 1024)) {//compare size in kb
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getMessage(PropertiesConstants.FUPL_ERROR_INVALID_FILE_SIZE));
			JSFUtilities.executeJavascriptFunction(dialog);
			return null;
		}
		if(fileUploaded.getFileName().length()>30){
			StringBuilder sbFileName=new StringBuilder();
			sbFileName.append("...");
			sbFileName.append( fileUploaded.getFileName().substring(fileUploaded.getFileName().length()-27) );
			displayName=sbFileName.toString();
		}else{
			displayName=fileUploaded.getFileName();
		}
		return displayName;
	}
	
	/**
	 * F upload validate file name length.
	 *
	 * @param fileName the file name
	 * @return the string
	 */
	public String fUploadValidateFileNameLength(String fileName){
		return fUploadValidateFileNameLength(fileName, null);
	}
	
	/**
	 * F upload validate file name length.
	 *
	 * @param fileName the file name
	 * @param maxLength the max length
	 * @return the string
	 */
	public String fUploadValidateFileNameLength(String fileName, Integer maxLength){
		String fileNameToUse = null;
		
		int maxLengthToUse = 30;
		
		if(Validations.validateIsNotNull(maxLength)){
			maxLengthToUse = maxLength;
		}
		
		if(fileName.length() > maxLengthToUse) {
			String extensionFile = fileName.toLowerCase().substring(fileName.lastIndexOf(".")+1,fileName.length());
			
			//get the name without extension
			StringBuilder sbName = new StringBuilder(fileName.substring(0,maxLengthToUse-4));
			//concatenate dot and extensionFile
			sbName.append(".").append(extensionFile);
			
			fileNameToUse = sbName.toString();
		} else {
			fileNameToUse = fileName;
		}
		
		return fileNameToUse;
	}
	
	
	/**
	 * F upload validate xml file.
	 *
	 * @param fileUploaded the file uploaded
	 * @param dialog the dialog
	 * @param matcher the matcher
	 * @param maxSize the max size
	 * @return the string
	 */
	public String fUploadValidateXmlFile(UploadedFile fileUploaded,String dialog, Matcher matcher, String maxSize){
		String displayName=null;
		String extensionFile = fileUploaded.getFileName().toLowerCase().substring(fileUploaded.getFileName().indexOf(".")+1,fileUploaded.getFileName().length());
		Matcher matcherToUser= null;
		String maxSizeToUse=getXmlFileUploadSize();
		
		if(matcher!=null){
			matcherToUser=matcher;
		}
		if(maxSize!=null){
			maxSizeToUse=maxSize;
		}
		
		if (matcherToUser!=null && !matcherToUser.matches()) {
			JSFUtilities.showMessageOnDialog(null, null,
					PropertiesConstants.FUPL_ERROR_INVALID_FILE_TYPE, null);
			JSFUtilities
					.executeJavascriptFunction(dialog);
			return null;
		}
		
		if (!xmlFileUploadFiltTypes.contains(extensionFile) && matcherToUser==null) {
			JSFUtilities.showMessageOnDialog(null, null,PropertiesConstants.FUPL_ERROR_INVALID_FILE_TYPE, null);
			JSFUtilities.executeJavascriptFunction(dialog);
			return null;
		}
		
		if (fileUploaded.getSize() > Integer.valueOf(maxSizeToUse)) {
			JSFUtilities.showMessageOnDialog(null, null,
					PropertiesConstants.FUPL_ERROR_INVALID_FILE_SIZE, null);
			JSFUtilities
					.executeJavascriptFunction(dialog);
			return null;
		}
		if(fileUploaded.getFileName().length()>30){
			StringBuilder sbFileName=new StringBuilder();
			sbFileName.append("...");
			sbFileName.append( fileUploaded.getFileName().substring(fileUploaded.getFileName().length()-27) );
			displayName=sbFileName.toString();
		}else{
			displayName=fileUploaded.getFileName();
		}
		return displayName;
	}
	
	/**
	 * F upload validate xls file.
	 *
	 * @param fileUploaded the file uploaded
	 * @param dialog the dialog
	 * @param matcher the matcher
	 * @param maxSize the max size
	 * @return the string
	 */
	public String fUploadValidateXlsFile(UploadedFile fileUploaded,String dialog, Matcher matcher, String maxSize){
		String displayName=null;
		String extensionFile = fileUploaded.getFileName().toLowerCase().substring(fileUploaded.getFileName().indexOf(".")+1,fileUploaded.getFileName().length());
		Matcher matcherToUser= null;
		String maxSizeToUse=getXlsFileUploadSize();
		
		if(matcher!=null){
			matcherToUser=matcher;
		}
		if(maxSize!=null){
			maxSizeToUse=maxSize;
		}
		
		if (matcherToUser!=null && !matcherToUser.matches()) {
			JSFUtilities.showMessageOnDialog(null, null,
					PropertiesConstants.FUPL_ERROR_INVALID_FILE_TYPE, null);
			JSFUtilities
					.executeJavascriptFunction(dialog);
			return null;
		}
		
		if (!xlsFileUploadFiltTypes.contains(extensionFile) && matcherToUser==null) {
			JSFUtilities.showMessageOnDialog(null, null,PropertiesConstants.FUPL_ERROR_INVALID_FILE_TYPE, null);
			JSFUtilities.executeJavascriptFunction(dialog);
			return null;
		}
		
		if (fileUploaded.getSize() > Integer.valueOf(maxSizeToUse)) {
			JSFUtilities.showMessageOnDialog(null, null,
					PropertiesConstants.FUPL_ERROR_INVALID_FILE_SIZE, null);
			JSFUtilities
					.executeJavascriptFunction(dialog);
			return null;
		}
		if(fileUploaded.getFileName().length()>30){
			StringBuilder sbFileName=new StringBuilder();
			sbFileName.append("...");
			sbFileName.append( fileUploaded.getFileName().substring(fileUploaded.getFileName().length()-27) );
			displayName=sbFileName.toString();
		}else{
			displayName=fileUploaded.getFileName();
		}
		return displayName;
	}
	
	/**
	 * F upload validate txt file.
	 *
	 * @param fileUploaded the file uploaded
	 * @param dialog the dialog
	 * @param matcher the matcher
	 * @param maxSize the max size
	 * @return the string
	 */
	public String fUploadValidateTxtFile(UploadedFile fileUploaded,String dialog, Matcher matcher, String maxSize){
		String displayName=null;
		String extensionFile = fileUploaded.getFileName().toLowerCase().substring(fileUploaded.getFileName().indexOf(".")+1,fileUploaded.getFileName().length());
		Matcher matcherToUser= null;
		String maxSizeToUse=getTxtFileUploadSize();
		
		if(matcher!=null){
			matcherToUser=matcher;
		}
		if(maxSize!=null){
			maxSizeToUse=maxSize;
		}
		
		if (matcherToUser!=null && !matcherToUser.matches()) {
			JSFUtilities.showMessageOnDialog(null, null,
					PropertiesConstants.FUPL_ERROR_INVALID_FILE_TYPE, null);
			JSFUtilities
					.executeJavascriptFunction(dialog);
			return null;
		}
		
		if (!txtFileUploadTypes.contains(extensionFile) && matcherToUser==null) {
			JSFUtilities.showMessageOnDialog(null, null,PropertiesConstants.FUPL_ERROR_INVALID_FILE_TYPE, null);
			JSFUtilities.executeJavascriptFunction(dialog);
			return null;
		}
		
		if (fileUploaded.getSize() > Integer.valueOf(maxSizeToUse)) {
			JSFUtilities.showMessageOnDialog(null, null,
					PropertiesConstants.FUPL_ERROR_INVALID_FILE_SIZE, null);
			JSFUtilities
					.executeJavascriptFunction(dialog);
			return null;
		}
		if(fileUploaded.getFileName().length()>30){
			StringBuilder sbFileName=new StringBuilder();
			sbFileName.append("...");
			sbFileName.append( fileUploaded.getFileName().substring(fileUploaded.getFileName().length()-27) );
			displayName=sbFileName.toString();
		}else{
			displayName=fileUploaded.getFileName();
		}
		return displayName;
	}
	
	/**
	 * Show message on dialog only key.
	 *
	 * @param keyHeaderProperty the key header property
	 * @param keyBodyProperty the key body property
	 */
	public static void showMessageOnDialogOnlyKey(String keyHeaderProperty,String keyBodyProperty){
		Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();	
		String headerMessageConfirmation = "";
		if(keyHeaderProperty!=null && !keyHeaderProperty.isEmpty()){
			headerMessageConfirmation = PropertiesUtilities.getMessage(locale, keyHeaderProperty);
		}
		String bodyMessageConfirmation = "";
		if(keyBodyProperty!=null && !keyBodyProperty.isEmpty()){
			bodyMessageConfirmation = PropertiesUtilities.getMessage(locale, keyBodyProperty);
		}		
		JSFUtilities.putViewMap("bodyMessageView", bodyMessageConfirmation);
		JSFUtilities.putViewMap("headerMessageView", headerMessageConfirmation);
	}
	
	/**
	 * Show exception message.
	 *
	 * @param keyHeaderProperty the key header property
	 * @param keyBodyProperty the key body property
	 */
	public static void showExceptionMessage(String keyHeaderProperty, String keyBodyProperty){
		String headerMessageConfirmation = "";
		if(keyHeaderProperty!=null && !keyHeaderProperty.isEmpty()){
			headerMessageConfirmation = PropertiesUtilities.getExceptionMessage(keyHeaderProperty);
		}
		String bodyMessageConfirmation = "";
		if(keyBodyProperty!=null && !keyBodyProperty.isEmpty()){
			bodyMessageConfirmation = PropertiesUtilities.getExceptionMessage(keyBodyProperty);
		}		
		JSFUtilities.putViewMap("bodyMessageView", bodyMessageConfirmation);
		JSFUtilities.putViewMap("headerMessageView", headerMessageConfirmation);
	}

	/**
	 * Sets the session idle time.
	 *
	 * @param sessionIdleTime the sessionIdleTime to set
	 */
	public void setSessionIdleTime(Integer sessionIdleTime) {
		this.sessionIdleTime = sessionIdleTime;
	}

	/**
	 * Gets the f upload file size display with comma.
	 *
	 * @return the f upload file size display with comma
	 */
	public String getfUploadFileSizeDisplayWithComma() {
		return fUploadFileSizeDisplayWithComma;
	}

	/**
	 * Sets the f upload file size display with comma.
	 *
	 * @param fUploadFileSizeDisplayWithComma the new f upload file size display with comma
	 */
	public void setfUploadFileSizeDisplayWithComma(
			String fUploadFileSizeDisplayWithComma) {
		this.fUploadFileSizeDisplayWithComma = fUploadFileSizeDisplayWithComma;
	}

	/**
	 * Gets the f upload file size.
	 *
	 * @return the f upload file size
	 */
	public String getfUploadFileSize() {
		return fUploadFileSize;
	}

	/**
	 * Sets the f upload file size.
	 *
	 * @param fUploadFileSize the new f upload file size
	 */
	public void setfUploadFileSize(String fUploadFileSize) {
		this.fUploadFileSize = fUploadFileSize;
	}

	/**
	 * Gets the f upload file types.
	 *
	 * @return the f upload file types
	 */
	public String getfUploadFileTypes() {
		return fUploadFileTypes;
	}

	/**
	 * Sets the f upload file types.
	 *
	 * @param fUploadFileTypes the new f upload file types
	 */
	public void setfUploadFileTypes(String fUploadFileTypes) {
		this.fUploadFileTypes = fUploadFileTypes;
	}

	/**
	 * Gets the f upload file size display.
	 *
	 * @return the f upload file size display
	 */
	public String getfUploadFileSizeDisplay() {
		return fUploadFileSizeDisplay;
	}

	/**
	 * Sets the f upload file size display.
	 *
	 * @param fUploadFileSizeDisplay the new f upload file size display
	 */
	public void setfUploadFileSizeDisplay(String fUploadFileSizeDisplay) {
		this.fUploadFileSizeDisplay = fUploadFileSizeDisplay;
	}

	/**
	 * Gets the f upload file types display.
	 *
	 * @return the f upload file types display
	 */
	public String getfUploadFileTypesDisplay() {
		return fUploadFileTypesDisplay;
	}

	/**
	 * Sets the f upload file types display.
	 *
	 * @param fUploadFileTypesDisplay the new f upload file types display
	 */
	public void setfUploadFileTypesDisplay(String fUploadFileTypesDisplay) {
		this.fUploadFileTypesDisplay = fUploadFileTypesDisplay;
	}

	/**
	 * Gets the xml file upload size.
	 *
	 * @return the xml file upload size
	 */
	public String getXmlFileUploadSize() {
		return xmlFileUploadSize;
	}

	/**
	 * Sets the xml file upload size.
	 *
	 * @param xmlFileUploadSize the new xml file upload size
	 */
	public void setXmlFileUploadSize(String xmlFileUploadSize) {
		this.xmlFileUploadSize = xmlFileUploadSize;
	}

	/**
	 * Gets the xml file upload filt types.
	 *
	 * @return the xml file upload filt types
	 */
	public String getXmlFileUploadFiltTypes() {
		return xmlFileUploadFiltTypes;
	}

	/**
	 * Sets the xml file upload filt types.
	 *
	 * @param xmlFileUploadFiltTypes the new xml file upload filt types
	 */
	public void setXmlFileUploadFiltTypes(String xmlFileUploadFiltTypes) {
		this.xmlFileUploadFiltTypes = xmlFileUploadFiltTypes;
	}

	/**
	 * Gets the xml file upload file types display.
	 *
	 * @return the xml file upload file types display
	 */
	public String getXmlFileUploadFileTypesDisplay() {
		return xmlFileUploadFileTypesDisplay;
	}

	/**
	 * Sets the xml file upload file types display.
	 *
	 * @param xmlFileUploadFileTypesDisplay the new xml file upload file types display
	 */
	public void setXmlFileUploadFileTypesDisplay(
			String xmlFileUploadFileTypesDisplay) {
		this.xmlFileUploadFileTypesDisplay = xmlFileUploadFileTypesDisplay;
	}

	/**
	 * Gets the txt file upload size.
	 *
	 * @return the txt file upload size
	 */
	public String getTxtFileUploadSize() {
		return txtFileUploadSize;
	}

	/**
	 * Sets the txt file upload size.
	 *
	 * @param txtFileUploadSize the new txt file upload size
	 */
	public void setTxtFileUploadSize(String txtFileUploadSize) {
		this.txtFileUploadSize = txtFileUploadSize;
	}

	/**
	 * Gets the txt file upload types.
	 *
	 * @return the txt file upload types
	 */
	public String getTxtFileUploadTypes() {
		return txtFileUploadTypes;
	}

	/**
	 * Sets the txt file upload types.
	 *
	 * @param txtFileUploadTypes the new txt file upload types
	 */
	public void setTxtFileUploadTypes(String txtFileUploadTypes) {
		this.txtFileUploadTypes = txtFileUploadTypes;
	}

	/**
	 * Gets the txt file upload size display.
	 *
	 * @return the txt file upload size display
	 */
	public String getTxtFileUploadSizeDisplay() {
		return txtFileUploadSizeDisplay;
	}

	/**
	 * Sets the txt file upload size display.
	 *
	 * @param txtFileUploadSizeDisplay the new txt file upload size display
	 */
	public void setTxtFileUploadSizeDisplay(String txtFileUploadSizeDisplay) {
		this.txtFileUploadSizeDisplay = txtFileUploadSizeDisplay;
	}

	/**
	 * Gets the txt file upload types display.
	 *
	 * @return the txt file upload types display
	 */
	public String getTxtFileUploadTypesDisplay() {
		return txtFileUploadTypesDisplay;
	}

	/**
	 * Sets the txt file upload types display.
	 *
	 * @param txtFileUploadTypesDisplay the new txt file upload types display
	 */
	public void setTxtFileUploadTypesDisplay(String txtFileUploadTypesDisplay) {
		this.txtFileUploadTypesDisplay = txtFileUploadTypesDisplay;
	}

	/**
	 * Gets the xls file upload size.
	 *
	 * @return the xls file upload size
	 */
	public String getXlsFileUploadSize() {
		return xlsFileUploadSize;
	}

	/**
	 * Sets the xls file upload size.
	 *
	 * @param xlsFileUploadSize the new xls file upload size
	 */
	public void setXlsFileUploadSize(String xlsFileUploadSize) {
		this.xlsFileUploadSize = xlsFileUploadSize;
	}

	/**
	 * Gets the xls file upload filt types.
	 *
	 * @return the xls file upload filt types
	 */
	public String getXlsFileUploadFiltTypes() {
		return xlsFileUploadFiltTypes;
	}

	/**
	 * Sets the xls file upload filt types.
	 *
	 * @param xlsFileUploadFiltTypes the new xls file upload filt types
	 */
	public void setXlsFileUploadFiltTypes(String xlsFileUploadFiltTypes) {
		this.xlsFileUploadFiltTypes = xlsFileUploadFiltTypes;
	}

	/**
	 * Gets the xls file upload file types display.
	 *
	 * @return the xls file upload file types display
	 */
	public String getXlsFileUploadFileTypesDisplay() {
		return xlsFileUploadFileTypesDisplay;
	}

	/**
	 * Sets the xls file upload file types display.
	 *
	 * @param xlsFileUploadFileTypesDisplay the new xls file upload file types display
	 */
	public void setXlsFileUploadFileTypesDisplay(
			String xlsFileUploadFileTypesDisplay) {
		this.xlsFileUploadFileTypesDisplay = xlsFileUploadFileTypesDisplay;
	}

	/**
	 * Gets the f upload imagen file size.
	 *
	 * @return the f upload imagen file size
	 */
	public String getfUploadImagenFileSize() {
		return fUploadImagenFileSize;
	}

	/**
	 * Sets the f upload imagen file size.
	 *
	 * @param fUploadImagenFileSize the new f upload imagen file size
	 */
	public void setfUploadImagenFileSize(String fUploadImagenFileSize) {
		this.fUploadImagenFileSize = fUploadImagenFileSize;
	}	
		/**
	 * Gets the all holidays.
	 *
	 * @return the all holidays
	 */
	public String getAllHolidays() {
		return allHolidays;
	}

	/**
	 * Sets the all holidays.
	 *
	 * @param allHolidays the new all holidays
	 */
	public void setAllHolidays(String allHolidays) {
		this.allHolidays = allHolidays;
	}

	/**
	 * Gets the lst reject motives.
	 *
	 * @return the lst reject motives
	 */
	public List<Parameter> getLstRejectMotives() {
		return lstRejectMotives;
	}

	/**
	 * Sets the lst reject motives.
	 *
	 * @param lstRejectMotives the new lst reject motives
	 */
	public void setLstRejectMotives(List<Parameter> lstRejectMotives) {
		this.lstRejectMotives = lstRejectMotives;
	}	
	
	public String getAllowTypes(String filter) {
		String expression=allowTypesFileUpExp.replace("{0}", filter);
		//String allowType= "/(\\.|\\/)(" + filter  + ")$/";
		return expression;
	}
	
	public StreamedContent getStreamedContentFromFile(byte[] file,String fileType, String fileName) throws IOException {
		
		if(file!=null){
			InputStream inputStream = new ByteArrayInputStream(file);
			StreamedContent streamedContentFile = new DefaultStreamedContent(inputStream, fileType, fileName);
			inputStream.close();

			return streamedContentFile;
		} else {
			return null;
		}
		
	}
}