package com.pradera.security.utils.view;

import java.io.Serializable;

public class TimerTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long entityId;
	
	private String entityClass;
	
	public TimerTO() {
		// TODO Auto-generated constructor stub
	}
	
	public TimerTO(Long entityId, String entityClass) {
		this.entityId = entityId;
		this.entityClass = entityClass;
	}

	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public String getEntityClass() {
		return entityClass;
	}

	public void setEntityClass(String entityClass) {
		this.entityClass = entityClass;
	}
}
