package com.pradera.security.utils.view;

import java.io.IOException;
import java.io.Serializable;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.context.RequestContext;

import com.pradera.commons.security.model.type.ChangePwdMessageType;
import com.pradera.commons.security.model.type.LogoutMotiveType;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.ViewDepositaryBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.security.usermgmt.facade.UserServiceFacade;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class X
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
@ViewDepositaryBean
public class ChangePasswordBean extends GenericBaseBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The userinfo. */
	@Inject
	private UserInfo userinfo;
	
	@EJB
	UserServiceFacade userServiceFacade;
	
	/**  To store old password. */
	private String oldPassword;
	
	/**  To store new password. */
	private String newPassword;
	
	/** The conf password. */
	private String confPassword;
	
	/** The ldap utilities. */
	@Inject
	@LDAPUtility
	LDAPUtilities ldapUtilities;
	private int charactersToCompare;

	public ChangePasswordBean() {
		Properties tmpProp=new Properties();
		try {
			tmpProp.load(OpenLDAPUtilities.class.getResourceAsStream("/ParametersLdap.properties"));
			charactersToCompare = Integer.parseInt(tmpProp.getProperty("ldap.number.characters.compare"));
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
	/**
	 * This method is called when user click on accept button in change	password
	 * dialog.
	 */
	public void changePwd() {
		try{
			JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
			if(this.ldapUtilities.isEnableAuthentication()) {
				ChangePwdMessageType message = ldapUtilities.changePassword(userinfo.getUserAccountSession().getUserName(), oldPassword, newPassword, confPassword);
				if(!message.equals(ChangePwdMessageType.PWD_CHANGE_SUCCESS)){				
					JSFUtilities.executeJavascriptFunction("PF('cnfwcngpassword').hide();");
					JSFUtilities.showComponent("cnfEndTransactionSamePage");
					showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,message.getValue(), null);
					String message_detail = PropertiesUtilities.getMessage(message.getValue());
					
					switch (message) {
					case PWD_ALPHA_NUMERIC:
					case PWD_FIRST_CHARACTERS_EQUAL:
					case PWD_ILLEGAL_VALUE:
					case PWD_IN_HISTORY:
					case PWD_NULL:
					case PWD_NUMERIC:
					case PWD_SAME_OLD:
						JSFUtilities.addContextMessage("pwd1",FacesMessage.SEVERITY_ERROR,message_detail,message_detail);
						JSFUtilities.addContextMessage("pwd2",FacesMessage.SEVERITY_ERROR,message_detail,message_detail);
						break;
					case PWD_AUTH_FAIL:
					case PWD_CHANGE_FAIL:
					default:
						JSFUtilities.addContextMessage("oldpwd",FacesMessage.SEVERITY_ERROR,message_detail,message_detail);
						break;
					}
					
				}else{
					clean();
					JSFUtilities.executeJavascriptFunction("PF('cnfwcngpassword').hide();");
					RequestContext.getCurrentInstance().closeDialog("sucess");
				}
			}
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * This method is called when user is logged in at first time.
	 */
	public void resetPwd() {
		try{
			JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
			JSFUtilities.hideComponent("cnfMsgCustomValidation");
			Pattern pwdPattern = Pattern.compile(GeneralConstants.PASSWORD_PATTERN);  
			Matcher newpwdmatcher = pwdPattern.matcher(this.newPassword);	
			int	pwdMinLength = ldapUtilities.getPwdMinLength();
			if(newPassword.length()< pwdMinLength){
				Object[] minLength={pwdMinLength};
				String validationMsg=PropertiesUtilities.getMessage(PropertiesConstants.USER_PASSWORD_MINLENGTH);
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.USER_PASSWORD_MINLENGTH, minLength);
				JSFUtilities.addContextMessage("pwd1",FacesMessage.SEVERITY_ERROR,validationMsg,validationMsg);
				JSFUtilities.addContextMessage("pwd2",FacesMessage.SEVERITY_ERROR,validationMsg,validationMsg);
				JSFUtilities.showComponent("cnfEndTransactionSamePage");
				return;
			}
			else if(!newpwdmatcher.find()){
				String validationMsg=PropertiesUtilities.getMessage(PropertiesConstants.USER_PWD_WRONG_PATTERN);
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.USER_PWD_WRONG_PATTERN, null);
				JSFUtilities.addContextMessage("pwd1",FacesMessage.SEVERITY_ERROR,validationMsg,validationMsg);
				JSFUtilities.addContextMessage("pwd2",FacesMessage.SEVERITY_ERROR,validationMsg,validationMsg);
				JSFUtilities.showComponent("cnfEndTransactionSamePage");
				return;
			}else if(newPassword.compareTo(confPassword) != 0){
				String validationMsg=PropertiesUtilities.getMessage(PropertiesConstants.PWD_NOTMATCH_CONFIRM);
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.PWD_NOTMATCH_CONFIRM, null);
				JSFUtilities.addContextMessage("pwd1",FacesMessage.SEVERITY_ERROR,validationMsg,validationMsg);
				JSFUtilities.addContextMessage("pwd2",FacesMessage.SEVERITY_ERROR,validationMsg,validationMsg);
				JSFUtilities.showComponent("cnfEndTransactionSamePage");
				return;
			}
			
			if(this.ldapUtilities.isEnableAuthentication()) {
				ChangePwdMessageType message = userServiceFacade.changePasswordUser(userinfo.getUserAccountSession().getIdUserAccountPk(),
						userinfo.getUserAccountSession().getUserName(),oldPassword, newPassword, confPassword);
	//					ldapUtilities.changePassword(userinfo.getUserAccountSession().getUserName(), oldPassword, newPassword, confPassword);
				
				if(!message.equals(ChangePwdMessageType.PWD_CHANGE_SUCCESS)){				
					String message_detail = PropertiesUtilities.getMessage(message.getValue());
					
					switch (message) {
					case PWD_ALPHA_NUMERIC:
					case PWD_FIRST_CHARACTERS_EQUAL:
					case PWD_ILLEGAL_VALUE:
					case PWD_IN_HISTORY:
					case PWD_NULL:
					case PWD_NUMERIC:
					case PWD_SAME_OLD:
					case PWD_NEW_SECURITY_VIOLATION:	
						JSFUtilities.addContextMessage("pwd1",FacesMessage.SEVERITY_ERROR,message_detail,message_detail);
						JSFUtilities.addContextMessage("pwd2",FacesMessage.SEVERITY_ERROR,message_detail,message_detail);
						break;
					case PWD_AUTH_FAIL:
					case PWD_CHANGE_FAIL:
					default:
						JSFUtilities.addContextMessage("oldpwd",FacesMessage.SEVERITY_ERROR,message_detail,message_detail);
						break;
					}
					
					JSFUtilities.showComponent("cnfMsgCustomValidation");
					showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,message.getValue(), null);
					return;
				}else{
					JSFUtilities.executeJavascriptFunction("PF('showpwdDialog').show();");
					return;
				}
			}
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Cnf change pwd.
	 */
	public void cnfChangePwd(){
		try{
			JSFUtilities.hideComponent("cnfEndTransactionSamePage");
			Pattern pwdPattern = Pattern.compile(GeneralConstants.PASSWORD_PATTERN);  
			Matcher newpwdmatcher = pwdPattern.matcher(this.newPassword);
			int	pwdMinLength = ldapUtilities.getPwdMinLength();
			if(Validations.validateIsNotNull(this.newPassword)){
				JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
				if(newPassword.length()< pwdMinLength ){
					Object[] minLength={pwdMinLength};
					String validationMsg=PropertiesUtilities.getMessage(PropertiesConstants.USER_PASSWORD_MINLENGTH);
					showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.USER_PASSWORD_MINLENGTH, minLength);
					JSFUtilities.addContextMessage("pwd1",FacesMessage.SEVERITY_ERROR,validationMsg,validationMsg);
					JSFUtilities.addContextMessage("pwd2",FacesMessage.SEVERITY_ERROR,validationMsg,validationMsg);
					JSFUtilities.showComponent("cnfEndTransactionSamePage");
					return;
				}
				else if(!newpwdmatcher.find()){
					String validationMsg=PropertiesUtilities.getMessage(PropertiesConstants.USER_PWD_WRONG_PATTERN);
					showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.USER_PWD_WRONG_PATTERN, null);
					JSFUtilities.addContextMessage("pwd1",FacesMessage.SEVERITY_ERROR,validationMsg,validationMsg);
					JSFUtilities.addContextMessage("pwd2",FacesMessage.SEVERITY_ERROR,validationMsg,validationMsg);
					JSFUtilities.showComponent("cnfEndTransactionSamePage");
					return;
				}else if(newPassword.compareTo(confPassword) != 0){
					String validationMsg=PropertiesUtilities.getMessage(PropertiesConstants.PWD_NOTMATCH_CONFIRM);
					showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.PWD_NOTMATCH_CONFIRM, null);
					JSFUtilities.addContextMessage("pwd1",FacesMessage.SEVERITY_ERROR,validationMsg,validationMsg);
					JSFUtilities.addContextMessage("pwd2",FacesMessage.SEVERITY_ERROR,validationMsg,validationMsg);
					JSFUtilities.showComponent("cnfEndTransactionSamePage");
					return;
				}else if(oldPassword.substring(0, charactersToCompare).compareTo(newPassword.substring(0, charactersToCompare)) == 0){
					String validationMsg=PropertiesUtilities.getMessage(ChangePwdMessageType.PWD_FIRST_CHARACTERS_EQUAL.getValue());
					showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, ChangePwdMessageType.PWD_FIRST_CHARACTERS_EQUAL.getValue(), null);
					JSFUtilities.addContextMessage("pwd1",FacesMessage.SEVERITY_ERROR,validationMsg,validationMsg);
					JSFUtilities.addContextMessage("pwd2",FacesMessage.SEVERITY_ERROR,validationMsg,validationMsg);
					JSFUtilities.showComponent("cnfEndTransactionSamePage");
					return;
				}
			}
			JSFUtilities.executeJavascriptFunction("PF('cnfwcngpassword').show();");
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	public void closePopup(){
		RequestContext.getCurrentInstance().closeDialog(null);
	}
	
	/**
	 * Clean.
	 */
	public void clean(){
		newPassword = null;
		oldPassword = null;
	}
	
	/**
	 * This method is called when user click on accept button in changepassword
	 * dialog.
	 */
	public void changePwdSuccess() {
		userinfo.setLogoutMotive(LogoutMotiveType.PASSWORDRESET);
		JSFUtilities.setHttpSessionAttribute(GeneralConstants.LOGOUT_MOTIVE, LogoutMotiveType.PASSWORDRESET);
		JSFUtilities.killSession();
		//return "loginPage";
		
	}

	/**
	 * Gets the old password.
	 *
	 * @return oldPassword
	 */
	public String getOldPassword() {
		return oldPassword;
	}

	/**
	 * Sets the old password.
	 *
	 * @param oldPassword the new old password
	 */
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	/**
	 * Gets the new password.
	 *
	 * @return new password
	 */
	public String getNewPassword() {
		return newPassword;
	}

	/**
	 * Sets the new password.
	 *
	 * @param newPassword the new new password
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	/**
	 * Gets the conf password.
	 *
	 * @return the conf password
	 */
	public String getConfPassword() {
		return confPassword;
	}
	
	/**
	 * Sets the conf password.
	 *
	 * @param confPassword the new conf password
	 */
	public void setConfPassword(String confPassword) {
		this.confPassword = confPassword;
	}
	
}
