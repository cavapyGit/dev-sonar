package com.pradera.security.utils.view;

import java.util.Properties;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Singleton;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

import com.pradera.commons.configuration.type.StageApplication;

@Singleton
public class LDAPUtilitiesProducer {


    @Inject
	StageApplication stageApplication;
    

	@Inject
	@InjectableResource(location = "ApplicationConfiguration.properties")
	Properties applicationConfiguration;
	
    
    private static LDAPUtilities ldapUtilities;

    @Produces
    @LDAPUtility
    public LDAPUtilities getLDAPUtilities(){
    	if(ldapUtilities == null){
    		String enauth = this.applicationConfiguration.getProperty(stageApplication+"."+LDAPUtilities.ENABLE_AUTH, this.applicationConfiguration.getProperty(LDAPUtilities.ENABLE_AUTH));
    		ldapUtilities = new LDAPUtilities(stageApplication.name(), Boolean.valueOf(enauth));
    	}
    	return ldapUtilities;    	
    }
}
