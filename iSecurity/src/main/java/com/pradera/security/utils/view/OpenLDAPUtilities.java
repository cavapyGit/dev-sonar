package com.pradera.security.utils.view;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;

import javax.inject.Inject;

import com.novell.ldap.LDAPAttribute;
import com.novell.ldap.LDAPAttributeSet;
import com.novell.ldap.LDAPConnection;
import com.novell.ldap.LDAPEntry;
import com.novell.ldap.LDAPException;
import com.novell.ldap.LDAPModification;
import com.novell.ldap.LDAPResponse;
import com.novell.ldap.LDAPResponseQueue;
import com.novell.ldap.connectionpool.PoolManager;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.security.model.type.ChangePwdMessageType;
import com.pradera.commons.security.model.type.LoginMessageType;
import com.pradera.commons.type.AESKeyType;
import com.pradera.commons.utils.AESEncryptUtils;
import com.pradera.security.model.UserAccount;
import com.pradera.security.ppolicy.to.PasswordPolicyTO;

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class LDAPUtilities to specify the Ldap configuration
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public class OpenLDAPUtilities {

	/** The ldap host. */
	private String ldapHost;
	
	/** The ldap port. */
	private int ldapPort;
	
	/** The ldap version. */
	private int ldapVersion;
	
	/** The login administrator. */
	private String loginAdministrator;
	
	/** The login user. */
	private String loginUser;
	
	/** The password administrador. */
	private String passwordAdministrador;
	
	/** The add user account. */
	private String addUserAccount;
	
	/** The default pwd policy. */
	private String defaultPwdPolicy;
	
	/** The max root conns. */
	private int maxRootConns;
	
	/** The max app conns. */
	private int maxAppConns;
    
    /** The root pool manager. */
    private PoolManager rootPoolManager;
    
    /** The app pool manager. */
    private AppPoolManager appPoolManager;
    
    /** The password policy. */
    private PasswordPolicyTO passwordPolicy;
    
    /** The characters to compare. */
    private int charactersToCompare;
    
    /** The domain active directory. */
    private String domainActiveDirectory;
    
    /** The log. */
    @Inject
	private static PraderaLogger log;
    
    /** The ldap utilities. */
    private static OpenLDAPUtilities ldapUtilities;
    
    /** The active directory utilities. */
    private static OpenLDAPUtilities activeDirectoryUtilities;
    
    private Properties parametersLdapProperties;
    
    private String stageApplication;
    
    /**
     * Gets the single instance of OpenLDAPUtilities.
     *
     * @param stageApplication the stage application
     * @return single instance of OpenLDAPUtilities
     */
    public static OpenLDAPUtilities getInstance(String stageApplication){
    	if(ldapUtilities == null){
    		ldapUtilities = new OpenLDAPUtilities(stageApplication);
    	}
    	return ldapUtilities;
    }
    
    /**
     * Gets the instancee active directory.
     *
     * @param stageApplication the stage application
     * @return the instancee active directory
     */
    public static OpenLDAPUtilities getInstanceeActiveDirectory(String stageApplication){
    	if(activeDirectoryUtilities == null){
    		activeDirectoryUtilities = new OpenLDAPUtilities(stageApplication,true);
    	}
    	return activeDirectoryUtilities;
    }
    
    /**
     * Instantiates a new open ldap utilities.
     *
     * @param stageApplication the stage application
     * @param activeDirectory the active directory
     */
    public OpenLDAPUtilities(String stageApplication,boolean activeDirectory){
    	try {
	    	Properties tmpProp=new Properties();
			tmpProp.load(OpenLDAPUtilities.class.getResourceAsStream("/ParametersLdap.properties"));
			ldapHost=tmpProp.getProperty("activedirectory.uri");
			ldapPort=Integer.parseInt( tmpProp.getProperty("activedirectory.port"));
			ldapVersion=Integer.parseInt( tmpProp.getProperty("ldap.version")  );
			setDomainActiveDirectory(tmpProp.getProperty("activedirectory.domain"));
    		String connections = tmpProp.getProperty(stageApplication+".maximum.appuser.connections");
    		if(connections != null && connections.length() > 0){
    			maxAppConns=Integer.parseInt(connections);
    		}else{
    			log.error("default connections for application login");
    			maxAppConns = 5;
    		}
    		appPoolManager = new AppPoolManager(ldapHost, ldapPort, maxAppConns);	
    	} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    private String getProperty(String key) {
    	return this.parametersLdapProperties.getProperty(this.stageApplication+"."+key, this.parametersLdapProperties.getProperty(key));
    }
            
    /**
     * Instantiates a new open ldap utilities.
     *
     * @param stageApplication the stage application
     */
    public OpenLDAPUtilities(String stageApplication){
    	try {
    		this.stageApplication = stageApplication;
    		this.parametersLdapProperties  =new Properties();
    		this.parametersLdapProperties.load(OpenLDAPUtilities.class.getResourceAsStream("/ParametersLdap.properties"));
    		ldapHost=this.getProperty("ldap.uri");
    		loginAdministrator=this.getProperty("ldap.connection.user.manager");
    		passwordAdministrador=AESEncryptUtils.decrypt(AESKeyType.PRADERAKEY.getValue(),this.getProperty("ldap.password.manager"));
    		loginUser=this.getProperty("ldap.connection.user.normal");
    		ldapPort=Integer.parseInt( this.getProperty("ldap.port") );
    		ldapVersion=Integer.parseInt( this.getProperty("ldap.version")  );
    		addUserAccount=this.getProperty("ldap.add.user");
    		defaultPwdPolicy=this.getProperty("ldap.default.pwdpolicy");
    		charactersToCompare = Integer.parseInt(this.getProperty("ldap.number.characters.compare"));
    		String connections =  this.getProperty("maximum.rootuser.connections") ;
    		
    		if(connections != null && connections.length() > 0){
    			maxRootConns=Integer.parseInt(connections);
    		}else{
    			log.error("default connections for ldap root login");
    			maxRootConns=5;
    		}
    		connections = this.getProperty("maximum.appuser.connections");
    		if(connections != null && connections.length() > 0){
    			maxAppConns=Integer.parseInt(connections);
    		}else{
    			log.error("default connections for application login");
    			maxAppConns = 5;
    		}
    		
    		rootPoolManager = new PoolManager(ldapHost, ldapPort, 1, maxRootConns, null);
    		appPoolManager = new AppPoolManager(ldapHost, ldapPort, maxAppConns);
    		
    		fillPasswordPolicy();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    /**
     * Gets the root bind connection.
     *
     * @return the root bind connection
     * @throws Exception the exception
     */
    private LDAPConnection getRootBindConnection() throws Exception{
    	LDAPConnection conn = rootPoolManager.getBoundConnection(loginAdministrator, passwordAdministrador.getBytes("UTF8"));    	
    	return conn;    	
    }
    
    private LDAPConnection createManagerConnection()  throws Exception{
    	LDAPConnection conn = new LDAPConnection();
		conn.connect(ldapHost, ldapPort);
		conn.bind(LDAPConnection.LDAP_V3, loginAdministrator, passwordAdministrador.getBytes("UTF8"));			
		
		return conn;
    	
    }
    /**
     * Release root bind connection.
     *
     * @param conn the conn
     * @throws Exception the exception
     */
    private void releaseRootBindConnection(LDAPConnection conn) throws Exception{
    	rootPoolManager.makeConnectionAvailable(conn);
    }
    
    /**
     * Gets the app connection.
     *
     * @return the app connection
     * @throws Exception the exception
     */
    private LDAPConnection getAppConnection() throws Exception{
    	LDAPConnection conn =  appPoolManager.getConnection();

        conn.disconnect();
        conn.connect(conn.getHost(), conn.getPort());
        
        return conn;
    }
    
    /**
     * Release app connection.
     *
     * @param conn the conn
     * @throws Exception the exception
     */
    private void releaseAppConnection(LDAPConnection conn) throws Exception{
    	appPoolManager.makeConnectionAvailable(conn);
    }
    
    /**
     * Authenticate user active directory.
     *
     * @param userName the user name
     * @param pwd the pwd
     * @return the login message type
     * @throws Exception the exception
     */
    public LoginMessageType authenticateUserActiveDirectory(String userName, String pwd) throws Exception{
    	LoginMessageType statusLogin = LoginMessageType.LOGIN_FAIL;
    	String strUserDN =userName+"@"+domainActiveDirectory;
		byte[] password = pwd.getBytes("UTF8");
		LDAPConnection appConn = getAppConnection();
		int rc = -1;
		try{
	        LDAPResponseQueue queue =appConn.bind( ldapVersion, strUserDN,password,(LDAPResponseQueue)null );
	        LDAPResponse rsp = (LDAPResponse)queue.getResponse();
	        rc = rsp.getResultCode();
	        
	        if (rc == LDAPException.CONNECT_ERROR) {
				appConn.disconnect();
				appConn.connect(appConn.getHost(), appConn.getPort());
				
				queue = appConn.bind(ldapVersion, strUserDN, password, (LDAPResponseQueue) null);
				rsp = (LDAPResponse) queue.getResponse();
				rc = rsp.getResultCode();
			}
		}catch(LDAPException le){
			log.error(le.getLDAPErrorMessage());				
			throw le;
		}finally{
			releaseAppConnection(appConn);
		}
		if ( rc == LDAPException.SUCCESS ){
			statusLogin = LoginMessageType.LOGIN_SUCCESS;
		} else if(rc == LDAPException.INVALID_CREDENTIALS){
			statusLogin = LoginMessageType.INVALIED_CREDENTIALS;
		}
    	return statusLogin;
    }
    
    /**
     * Fill password policy.
     *
     * @throws Exception the exception
     */
    public void fillPasswordPolicy() throws Exception{
    	LDAPConnection connection = getRootBindConnection();
    	try{
	    	String pwdRetrunAttr[] = {"pwdAllowUserChange","pwdExpireWarning","pwdFailureCountInterval","pwdGraceAuthNLimit","pwdInHistory",
	    			"pwdLockout","pwdLockoutDuration","pwdMaxAge","pwdMaxFailure","pwdMinAge","pwdMinLength","pwdMustChange"};
			LDAPEntry objEntryPPolicy = connection.read(defaultPwdPolicy, pwdRetrunAttr);
			passwordPolicy = new PasswordPolicyTO();
			LDAPAttribute objAttribute =  objEntryPPolicy.getAttribute(pwdRetrunAttr[0]);		
			if(objAttribute != null && objAttribute.getStringValue() != null){
				passwordPolicy.setPwdAllowUserChange(Boolean.valueOf(objAttribute.getStringValue()));
			}
			objAttribute =  objEntryPPolicy.getAttribute(pwdRetrunAttr[1]);		
			if(objAttribute != null && objAttribute.getStringValue() != null){
				passwordPolicy.setPwdExpireWarning(Long.parseLong(objAttribute.getStringValue()));
			}
			objAttribute =  objEntryPPolicy.getAttribute(pwdRetrunAttr[2]);		
			if(objAttribute != null && objAttribute.getStringValue() != null){
				passwordPolicy.setPwdFailureCountInterval(Long.parseLong(objAttribute.getStringValue()));
			}
			objAttribute =  objEntryPPolicy.getAttribute(pwdRetrunAttr[3]);		
			if(objAttribute != null && objAttribute.getStringValue() != null){
				passwordPolicy.setPwdGraceAuthNLimit(Integer.parseInt(objAttribute.getStringValue()));
			}
			objAttribute =  objEntryPPolicy.getAttribute(pwdRetrunAttr[4]);		
			if(objAttribute != null && objAttribute.getStringValue() != null){
				passwordPolicy.setPwdInHistory(Integer.parseInt(objAttribute.getStringValue()));
			}
			objAttribute =  objEntryPPolicy.getAttribute(pwdRetrunAttr[5]);		
			if(objAttribute != null && objAttribute.getStringValue() != null){
				passwordPolicy.setPwdLockout(Boolean.valueOf(objAttribute.getStringValue()));
			}
			objAttribute =  objEntryPPolicy.getAttribute(pwdRetrunAttr[6]);		
			if(objAttribute != null && objAttribute.getStringValue() != null){
				passwordPolicy.setPwdLockoutDuration(Long.parseLong(objAttribute.getStringValue()));
			}
			objAttribute =  objEntryPPolicy.getAttribute(pwdRetrunAttr[7]);		
			if(objAttribute != null && objAttribute.getStringValue() != null){
				passwordPolicy.setPwdMaxAge(Long.parseLong(objAttribute.getStringValue()));
			}
			objAttribute =  objEntryPPolicy.getAttribute(pwdRetrunAttr[8]);		
			if(objAttribute != null && objAttribute.getStringValue() != null){
				passwordPolicy.setPwdMaxFailure(Integer.parseInt(objAttribute.getStringValue()));
			}
			objAttribute =  objEntryPPolicy.getAttribute(pwdRetrunAttr[9]);		
			if(objAttribute != null && objAttribute.getStringValue() != null){
				passwordPolicy.setPwdMinAge(Long.parseLong(objAttribute.getStringValue()));
			}
			objAttribute =  objEntryPPolicy.getAttribute(pwdRetrunAttr[10]);		
			if(objAttribute != null && objAttribute.getStringValue() != null){
				passwordPolicy.setPwdMinLength(Integer.parseInt(objAttribute.getStringValue()));
			}
			objAttribute =  objEntryPPolicy.getAttribute(pwdRetrunAttr[11]);		
			if(objAttribute != null && objAttribute.getStringValue() != null){
				passwordPolicy.setPwdMustChange(Boolean.valueOf(objAttribute.getStringValue()));
			}
    	}finally{
    		releaseRootBindConnection(connection);
    	}
    }
    
    /**
     * Adds the user.
     *
     * @param userAccount the user account
     * @param password the password
     * @throws Exception the exception
     */
    public void addUser(UserAccount userAccount,String password) throws Exception{
    	LDAPConnection objConnection = createManagerConnection();
		if(objConnection != null){
	    	try {				
	            LDAPAttributeSet setAtr = new LDAPAttributeSet();
	
	            setAtr.add(new LDAPAttribute("cn", userAccount.getFullName()));
	            setAtr.add(new LDAPAttribute("givenname", userAccount.getFullName()));
	            setAtr.add(new LDAPAttribute("sn", userAccount.getFirtsLastName() + " "+userAccount.getSecondLastName()));
	            setAtr.add(new LDAPAttribute("userpassword", password));
	            if(userAccount.getPhoneNumber()!=null && !userAccount.getPhoneNumber().isEmpty()){
	            	setAtr.add(new LDAPAttribute("telephonenumber", userAccount.getPhoneNumber()));
	            }
	            if(passwordPolicy.isPwdMustChange()){
	            	setAtr.add( new LDAPAttribute("pwdReset", new String("TRUE")));
				}
	            setAtr.add(new LDAPAttribute("mail", userAccount.getIdEmail()));
	            setAtr.add(new LDAPAttribute("objectclass", new String("inetOrgPerson")));
	            setAtr.add(new LDAPAttribute("objectclass", new String("posixAccount")));
	       
	            String dn = MessageFormat.format(addUserAccount, userAccount.getLoginUser());
	            LDAPEntry newEntry = new LDAPEntry(dn, setAtr);
	            objConnection.add(newEntry);
	            //log.info("Usuario Ingresado Correctamente...");            
			} catch (Exception e) {
				if(log != null)
					log.error(e.getMessage());
				//e.printStackTrace();
			}finally{ 
				objConnection.disconnect();
			}
		}
    }
    
    /**
     * Update user details.
     *
     * @param userAccount the user account
     * @throws Exception the exception
     */
    public void updateUserDetails(UserAccount userAccount) throws Exception{
    	LDAPConnection objConnection = createManagerConnection();
		if(objConnection != null){
	    	try {
	    		String dnUsuario=MessageFormat.format(loginUser, userAccount.getLoginUser());
				
				LDAPModification[] modifications = null;				
				if(userAccount.getPhoneNumber()!=null && !userAccount.getPhoneNumber().isEmpty()){
					modifications = new LDAPModification[2];
					modifications[1] = new LDAPModification( LDAPModification.REPLACE,new LDAPAttribute("telephonenumber", userAccount.getPhoneNumber()));
				}else{
					modifications = new LDAPModification[1];
				}
				modifications[0] = new LDAPModification( LDAPModification.REPLACE,new LDAPAttribute("mail", userAccount.getIdEmail()));
				objConnection.modify( dnUsuario, modifications);
				           
			} catch (Exception e) {
				if(log != null)
					log.error(e.getMessage());
				//e.printStackTrace();
			}finally{ 
				objConnection.disconnect();
			}
		}
    }
    
    /**
     * Reset password.
     *
     * @param userAccount the user account
     * @param password the password
     * @throws Exception the exception
     */
    public void resetPassword(UserAccount userAccount,String password) throws Exception{
    	LDAPConnection objConnection = createManagerConnection();
		if(objConnection != null){
	    	try {
				String dnUsuario=MessageFormat.format(loginUser, userAccount.getLoginUser());
							
				LDAPModification[] modifications = null;
				if(passwordPolicy.isPwdMustChange()){
					modifications = new LDAPModification[2];
					modifications[1] = new LDAPModification( LDAPModification.REPLACE,new LDAPAttribute("pwdReset", new String("TRUE")));
				}else{
					modifications = new LDAPModification[1];	
				}
				LDAPAttribute updatePassword = new LDAPAttribute( "userPassword", password);
				modifications[0] = new LDAPModification( LDAPModification.REPLACE, updatePassword);	
				
				objConnection.modify( dnUsuario, modifications);
			} catch (Exception e) {
				if(log != null)
					log.error(e.getMessage());
			}finally{
				objConnection.disconnect();
			}
		}
    }
	
    /**
     * Gets the connection.
     *
     * @return the connection
     * @throws LDAPException the LDAP exception
     */
    private LDAPConnection getConnection() throws LDAPException {		
		LDAPConnection objCon = new LDAPConnection();
		objCon.connect(ldapHost, ldapPort);
		return objCon;
	}
    
    /**
     * Gets the binded connection.
     *
     * @return the binded connection
     * @throws Exception the exception
     */
    private LDAPConnection getBindedConnection() throws Exception {		
    	LDAPConnection objCon =getConnection();
		objCon.bind(LDAPConnection.LDAP_V3, loginAdministrator, passwordAdministrador.getBytes("utf-8"));
		return objCon;
	}
    
    /**
     * Checks for the existance of user with the given credentials and
     * authenticate the user. If the given details are valid this method returns
     * true else returns false.
     *
     * @param usrName            <code>String</code>
     * @param pwd            <code>String</code>
     * @param objStrBuff the obj str buff
     * @return <code>bolean</code>
     * @throws Exception the exception
     */
    @Deprecated
	public LoginMessageType isValidUser(String usrName, String pwd, StringBuffer objStrBuff)
			throws Exception {
		LoginMessageType inFlag = LoginMessageType.LOGIN_FAIL;
		LDAPConnection objConnection = getBindedConnection();
		boolean correct = false;
		LDAPAttribute objAttribute = null;
		String strUserDN =MessageFormat.format(loginUser, usrName);
		try{
			objAttribute = new LDAPAttribute("userPassword", pwd);
			correct = objConnection.compare( strUserDN, objAttribute );
		}catch (LDAPException e) {
			if ( e.getResultCode() == LDAPException.NO_SUCH_OBJECT ) {
				return LoginMessageType.NO_SUCH_OBJECT;
            } else if ( e.getResultCode() == LDAPException.NO_SUCH_ATTRIBUTE ) {
            	return LoginMessageType.NO_SUCH_ATTRIBUTE;
            }else {
            	throw e;
            }
		}		
		String userReturnAttrs[] = { "pwdAccountLockedTime", "pwdReset", "pwdPolicySubentry", "pwdChangedTime","pwdGraceUseTime", "pwdFailureTime"};
		LDAPEntry objEntryUser = objConnection.read(strUserDN, userReturnAttrs );
		String strPwdPolicy = defaultPwdPolicy;
		objAttribute =  objEntryUser.getAttribute(userReturnAttrs[2]);
		if(objAttribute != null && objAttribute.getStringValue() != null){
			strPwdPolicy = objAttribute.getStringValue();
		}
		String pwdRetrunAttr[] = {"pwdMaxFailure","pwdMaxAge","pwdExpireWarning","pwdGraceAuthNLimit"};
		LDAPEntry objEntryPPolicy = objConnection.read( strPwdPolicy , pwdRetrunAttr );		
		if(correct){
			objAttribute = objEntryUser.getAttribute(userReturnAttrs[0]);
			if(objAttribute != null && objAttribute.getStringValue() != null){
				inFlag = LoginMessageType.USER_LOCKED;
			}else{
				objAttribute =  objEntryUser.getAttribute(userReturnAttrs[1]);
				if(objAttribute != null && objAttribute.getStringValue() != null && objAttribute.getStringValue().equalsIgnoreCase("true")){
					inFlag = LoginMessageType.PWD_MUST_CHANGE_FIRST_LOGIN;
				}else{
					Date dtPwdChangedTime = null;
					objAttribute = objEntryUser.getAttribute(userReturnAttrs[3]);
					if(objAttribute != null && objAttribute.getStringValue() != null){
						dtPwdChangedTime = getDate(objAttribute.getStringValue());
					}
					int inMaxAge = 0, inExpWarn = 0, inGraceLimit = 0, inGrace = 0;
					objAttribute = objEntryPPolicy.getAttribute(pwdRetrunAttr[1]);
					if(objAttribute != null && objAttribute.getStringValue() != null){
						inMaxAge = Integer.parseInt(objAttribute.getStringValue());
					}
					objAttribute = objEntryPPolicy.getAttribute(pwdRetrunAttr[2]);
					if(objAttribute != null && objAttribute.getStringValue() != null){
						inExpWarn = Integer.parseInt(objAttribute.getStringValue());
					}
					objAttribute = objEntryPPolicy.getAttribute(pwdRetrunAttr[3]);
					if(objAttribute != null && objAttribute.getStringValue() != null){
						inGraceLimit = Integer.parseInt(objAttribute.getStringValue());
					}
					if(dtPwdChangedTime != null && inMaxAge > 0){
						Date dtCurrent = new Date();
						Calendar objCal = Calendar.getInstance();
						objCal.setTime(dtPwdChangedTime);
						objCal.add(Calendar.SECOND, inMaxAge);
						Date dtExpired = objCal.getTime();
						if(dtCurrent.compareTo(dtExpired) > 0){
							objAttribute = objEntryUser.getAttribute(userReturnAttrs[4]);
							if(objAttribute != null && objAttribute.getStringValueArray() != null){
								inGrace = objAttribute.getStringValueArray().length;
							}
							if(inGrace < inGraceLimit){
								inFlag = LoginMessageType.PWD_MUST_CHANGE_GRACE_LOGIN;
								objStrBuff.append((inGraceLimit-inGrace));
							}else{
								inFlag = LoginMessageType.PWD_EXPIRED;
							}
						}else{
							objCal.setTime(dtPwdChangedTime);
							Date dtWarn = dtCurrent;
							if(inExpWarn > 0){
								objCal.add(Calendar.SECOND, inExpWarn);
								dtWarn = objCal.getTime();
							}
							if(dtCurrent.compareTo(dtWarn) > 0){
								if(inGraceLimit > 0){
									inFlag = LoginMessageType.PWD_IN_EXP_WARING;
								}else{
									inFlag = LoginMessageType.PWD_MUST_CHANGE_EXP_WARN;
								}
							}else{
								inFlag = LoginMessageType.LOGIN_SUCCESS;
							}
						}
					}else{
						inFlag = LoginMessageType.LOGIN_SUCCESS;
					}
					// to do for the password max age and expire warning with grace limit
				}				
			}
		}else{
			objAttribute = objEntryPPolicy.getAttribute(pwdRetrunAttr[0]);			
			if(objAttribute != null && objAttribute.getStringValue() != null){
				objStrBuff.append(objAttribute.getStringValue());
			}
			if(objStrBuff != null && objStrBuff.length() > 0){				
				objAttribute =  objEntryUser.getAttribute(userReturnAttrs[5]);
				if(objAttribute != null && objAttribute.getStringValue() != null){
					objStrBuff.append(","+objAttribute.getStringValue());
				}				
			}
			inFlag = LoginMessageType.INVALIED_CREDENTIALS;
		}				
		disconnect(objConnection);
		return inFlag;
	}
    
    /**
     * Gets the password user ldap.
     *
     * @param usrName the usr name
     * @return the password user ldap
     * @throws Exception the exception
     */
    public String getPasswordUserLdap(String usrName) throws Exception{
    	String password =null;
    	LDAPAttribute objAttribute = null;
		LDAPConnection objConnection = getRootBindConnection();
    	try{
    		String strUserDN =MessageFormat.format(loginUser, usrName);
    		String userReturnAttrs[] = {"userPassword"};
    		LDAPEntry objEntryUser = objConnection.read(strUserDN, userReturnAttrs );
    		objAttribute = objEntryUser.getAttribute(userReturnAttrs[0]);
    		if(objAttribute != null){
    			password = objAttribute.getStringValue();
    		}
		}catch(LDAPException le){
			log.error(le.getLDAPErrorMessage());				
			throw le;
		}finally{
			releaseAppConnection(objConnection);
		}
    	return password;
    }
	
	/**
	 * Authenticate user.
	 *
	 * @param usrName the usr name
	 * @param pwd the pwd
	 * @param objStrBuff the obj str buff
	 * @return the login message type
	 * @throws Exception the exception
	 */
	public LoginMessageType authenticateUser(String usrName, String pwd, StringBuffer objStrBuff) throws Exception {
		
		LoginMessageType inFlag = LoginMessageType.LOGIN_FAIL;
		LDAPAttribute objAttribute = null;
		String strUserDN = MessageFormat.format(loginUser, usrName);
		byte[] password = pwd.getBytes("UTF8");
		LDAPConnection appConn = getAppConnection();
		int rc = -1;
		try {			
			LDAPResponseQueue queue = appConn.bind(ldapVersion, strUserDN, password, (LDAPResponseQueue) null);
			LDAPResponse rsp = (LDAPResponse) queue.getResponse();
			rc = rsp.getResultCode();
			
			if (rc == LDAPException.CONNECT_ERROR) {
				appConn.disconnect();
				appConn.connect(appConn.getHost(), appConn.getPort());
				
				queue = appConn.bind(ldapVersion, strUserDN, password, (LDAPResponseQueue) null);
				rsp = (LDAPResponse) queue.getResponse();
				rc = rsp.getResultCode();
			}
		} catch (LDAPException le) {
			log.error(le.getLDAPErrorMessage());
			throw le;
		} finally {
			releaseAppConnection(appConn);
		}

		LDAPConnection objConnection = getRootBindConnection();
		
		LDAPEntry objEntryUser = null;
		
		String userReturnAttrs[] = { "pwdAccountLockedTime", "pwdReset", "pwdChangedTime", "pwdGraceUseTime",
		"pwdFailureTime" };
		try {			
	    	
			objConnection.disconnect();
			objConnection.connect(objConnection.getHost(), objConnection.getPort());
			objEntryUser = objConnection.read(strUserDN, userReturnAttrs);

		} catch (LDAPException e) {
			if (e.getResultCode() == LDAPException.NO_SUCH_OBJECT) {
				inFlag = LoginMessageType.NO_SUCH_OBJECT;
				return inFlag;
			} else if (e.getResultCode() == LDAPException.NO_SUCH_ATTRIBUTE) {
				inFlag = LoginMessageType.NO_SUCH_ATTRIBUTE;
				return inFlag;
			} else if (e.getResultCode() == LDAPException.CONNECT_ERROR){
				objConnection.disconnect();
				objConnection.connect(objConnection.getHost(), objConnection.getPort());
				
				objEntryUser = objConnection.read(strUserDN, userReturnAttrs);
				
				if(objEntryUser == null) {
					inFlag = LoginMessageType.NO_SUCH_OBJECT;
					return inFlag;
				}
			} else {
				throw e;
			}
			
		} finally {
			releaseRootBindConnection(objConnection);
		}
		
		Date dtPwdChangedTime = null;
		objAttribute = objEntryUser.getAttribute(userReturnAttrs[2]);
		if (objAttribute != null && objAttribute.getStringValue() != null) {
			dtPwdChangedTime = getDate(objAttribute.getStringValue());
		}
		objAttribute = objEntryUser.getAttribute(userReturnAttrs[0]);
		if (objAttribute != null && objAttribute.getStringValue() != null) {
			inFlag = LoginMessageType.USER_LOCKED;
		} else {
			if (rc == LDAPException.SUCCESS) {
				objAttribute = objEntryUser.getAttribute(userReturnAttrs[1]);
				if (objAttribute != null && objAttribute.getStringValue() != null
						&& objAttribute.getStringValue().equalsIgnoreCase("true")) {
					inFlag = LoginMessageType.PWD_MUST_CHANGE_FIRST_LOGIN;
				} else {
					int inGrace = 0;
					if (dtPwdChangedTime != null && passwordPolicy.getPwdMaxAge() > 0) {
						Date dtCurrent = new Date();
						Calendar objCal = Calendar.getInstance();
						objCal.setTime(dtPwdChangedTime);
						objCal.add(Calendar.SECOND, (int) passwordPolicy.getPwdMaxAge());
						Date dtExpired = objCal.getTime();
						if (dtCurrent.compareTo(dtExpired) > 0) {
							objAttribute = objEntryUser.getAttribute(userReturnAttrs[3]);
							if (objAttribute != null && objAttribute.getStringValueArray() != null) {
								inGrace = objAttribute.getStringValueArray().length;
							}
							if (inGrace < passwordPolicy.getPwdGraceAuthNLimit()) {
								inFlag = LoginMessageType.PWD_MUST_CHANGE_GRACE_LOGIN;
								objStrBuff.append((passwordPolicy.getPwdGraceAuthNLimit() - inGrace));
							} else {
								inFlag = LoginMessageType.PWD_EXPIRED;
							}
						} else {
							objCal.setTime(dtPwdChangedTime);
							Date dtWarn = dtCurrent;
							if (passwordPolicy.getPwdExpireWarning() > 0) {
								objCal.add(Calendar.SECOND, (int) passwordPolicy.getPwdExpireWarning());
								dtWarn = objCal.getTime();
							}
							if (dtCurrent.compareTo(dtWarn) > 0) {
								if (passwordPolicy.getPwdGraceAuthNLimit() > 0) {
									inFlag = LoginMessageType.PWD_IN_EXP_WARING;
								} else {
									inFlag = LoginMessageType.PWD_MUST_CHANGE_EXP_WARN;
								}
							} else {
								inFlag = LoginMessageType.LOGIN_SUCCESS;
							}
						}
					} else {
						inFlag = LoginMessageType.LOGIN_SUCCESS;
					}
					// to do for the password max age and expire warning with grace limit

				}
			} else {
				if (passwordPolicy.getPwdMaxFailure() > 0) {
					if (objStrBuff.length() > 0) {
						objStrBuff.append(",");
					}
					objAttribute = objEntryUser.getAttribute(userReturnAttrs[4]);
					if (objAttribute != null && objAttribute.getStringValueArray() != null) {
						objStrBuff.append(","
								+ (passwordPolicy.getPwdMaxFailure() - objAttribute.getStringValueArray().length));
					}
				}
				if (dtPwdChangedTime != null && passwordPolicy.getPwdMaxAge() > 0) {
					Date dtCurrent = new Date();
					Calendar objCal = Calendar.getInstance();
					objCal.setTime(dtPwdChangedTime);
					objCal.add(Calendar.SECOND, (int) passwordPolicy.getPwdMaxAge());
					Date dtExpired = objCal.getTime();
					if (dtCurrent.compareTo(dtExpired) > 0) {
						inFlag = LoginMessageType.PWD_EXPIRED;
					} else {
						inFlag = LoginMessageType.INVALIED_CREDENTIALS;
					}
				} else {
					inFlag = LoginMessageType.INVALIED_CREDENTIALS;
				}

			}
		}
		 
		return inFlag;
	}
	
	/**
	 * Disconnect.
	 *
	 * @param objConnection the obj connection
	 * @throws LDAPException the LDAP exception
	 */
	private void disconnect(LDAPConnection objConnection) throws LDAPException{
		objConnection.disconnect();
	}
	
	/**
	 * change the password with the given new password for the user.
	 *
	 * @param strUser <code>String</code>
	 * @param strOldPwd <code>String</code>
	 * @param strNewPwd <code>String</code>
	 * @param strConfPwd <code>String</code>
	 * @return <code>int</code>
	 * @throws Exception the exception
	 */
	public ChangePwdMessageType changePassword(String strUser, String strOldPwd, String strNewPwd, String strConfPwd) throws Exception{
		
		ChangePwdMessageType inFlag = ChangePwdMessageType.PWD_CHANGE_FAIL;
		
		LDAPConnection objConnection = null;
		
		try{
			if(!strConfPwd.equals(strNewPwd)){
				return ChangePwdMessageType.PWD_NOT_MATCH_CONFIRM;
			}
			if(passwordPolicy.getPwdMinLength() > 0 && strNewPwd.length() < passwordPolicy.getPwdMinLength()){
				return ChangePwdMessageType.PWD_MIN_LENGH;
			}
			objConnection = getAppConnection();
			
			String strUserDN = MessageFormat.format(loginUser, strUser);
			
			LDAPResponseQueue queue =objConnection.bind( ldapVersion, strUserDN,strOldPwd.getBytes("UTF8"),(LDAPResponseQueue)null);
	        LDAPResponse rsp = (LDAPResponse)queue.getResponse();
	        
	        int rc = rsp.getResultCode();
	        
	        if (rc == LDAPException.CONNECT_ERROR) {
	        	objConnection.disconnect();
	        	objConnection.connect(objConnection.getHost(), objConnection.getPort());
				
				queue = objConnection.bind(ldapVersion, strUserDN, strOldPwd.getBytes("UTF8"), (LDAPResponseQueue) null);
				rsp = (LDAPResponse) queue.getResponse();
				rc = rsp.getResultCode();
			}
	        
	        if ( rc != LDAPException.SUCCESS ){
	        	return ChangePwdMessageType.PWD_AUTH_FAIL;
	        }
	        
	        if( strOldPwd.substring(0, charactersToCompare).compareTo(strNewPwd.substring(0, charactersToCompare)) == 0){
	        	return ChangePwdMessageType.PWD_FIRST_CHARACTERS_EQUAL;
	        }
	        	
			LDAPModification[] modifications = new LDAPModification[2];
			LDAPAttribute deletePassword = new LDAPAttribute( "userPassword", strOldPwd);
			modifications[0] = new LDAPModification( LDAPModification.DELETE, deletePassword);
			LDAPAttribute addPassword = new LDAPAttribute( "userPassword", strNewPwd);
			modifications[1] = new LDAPModification( LDAPModification.ADD, addPassword);
			try{
				objConnection.modify( strUserDN, modifications);
			}catch(LDAPException le){ 
				if(le.getResultCode() == 19 && le.getLDAPErrorMessage() != null && le.getLDAPErrorMessage().contains("history")){
					return ChangePwdMessageType.PWD_IN_HISTORY;
				}else if(le.getResultCode() == 19 && le.getLDAPErrorMessage() != null && le.getLDAPErrorMessage().contains("Password fails quality")){
					return ChangePwdMessageType.PWD_NEW_SECURITY_VIOLATION;
				} else { 
					throw le;
				}
			}
			inFlag = ChangePwdMessageType.PWD_CHANGE_SUCCESS;
		}catch (LDAPException lException) {
			if(log != null)
			log.error(lException.getMessage());
			//lException.printStackTrace();
		}catch(Exception objException){
			if(log != null)
			log.error(objException.getMessage());	
			//objException.printStackTrace();
		}finally{
			if(objConnection != null){
				releaseAppConnection(objConnection);
			}
		}
		return inFlag;
	}
	
	/**
	 * return the implemented schema password minimum length.
	 *
	 * @return <code>int</code>
	 * @throws Exception the exception
	 */
	public int getPwdMinLength() throws Exception{		
		return passwordPolicy.getPwdMinLength();
	}

	/**
	 * disable the user account in the given schema.
	 *
	 * @param strUserLoginName <code>String</code>
	 * @return <code>boolean</code>
	 * @throws Exception the exception
	 */
	public boolean disableUserAccount(String strUserLoginName) throws Exception{
		LDAPConnection objConnection = null;
		try{
			String strUserDN = MessageFormat.format(loginUser, strUserLoginName);
			LDAPModification[] modifications = new LDAPModification[1];
			LDAPAttribute updateLockTime = new LDAPAttribute("pwdAccountLockedTime", getStringFromDate(new Date()));
			modifications[0] = new LDAPModification( LDAPModification.REPLACE, updateLockTime);		
			objConnection = createManagerConnection();
			objConnection.modify( strUserDN, modifications);
		}catch(Exception e){
			if(log != null)
			log.error(e.getMessage());
		}finally{
			if(objConnection != null){
				objConnection.disconnect();
			}
		}
		return true;
	}
	
	/**
	 * enable the user account in the given schema.
	 *
	 * @param strUserLoginName <code>String</code>
	 * @return <code>boolean</code>
	 * @throws Exception the exception
	 */
	public boolean enableUserAccount(String strUserLoginName) throws Exception{
		LDAPConnection objConnection = null;
		LDAPConnection conn = null;
		String password = "";
		String strUserDN = MessageFormat.format(loginUser, strUserLoginName);
		try{			
			LDAPModification[] modifications = new LDAPModification[1];
			modifications[0] = new LDAPModification( LDAPModification.DELETE, new LDAPAttribute("pwdAccountLockedTime"));		
			objConnection = getRootBindConnection();
			
			conn = createManagerConnection();			
			conn.modify( strUserDN, modifications);
			
			String userReturnAttrs[] = { "userPassword"};
			LDAPEntry objEntryUser = objConnection.read(strUserDN, userReturnAttrs );
			LDAPAttribute objAttribute =  objEntryUser.getAttribute(userReturnAttrs[0]);
			if(objAttribute != null && objAttribute.getStringValue() != null){
				password = objAttribute.getStringValue();
			}				        
		}catch(Exception e){
			if(log != null)
			log.error(e.getMessage());
		}finally{
			if(objConnection != null){
				releaseRootBindConnection(objConnection);
			}
			if(conn != null) {
				conn.disconnect();
			}
		}
		if(password != null && password.length() > 0){
			byte[] pwd = password.getBytes("UTF8");
			LDAPConnection appConn = getAppConnection();
			try{
				appConn.bind( ldapVersion, strUserDN,pwd,
	                (LDAPResponseQueue)null );
			}catch(LDAPException le){
				log.error(le.toString());
			}finally{
				if(appConn != null){
					releaseAppConnection(appConn);
				}
			}
		}
		return true;
	}
	
	/**
	 * return the open ldap date string to java date object.
	 *
	 * @param strOIDDate the str oid date
	 * @return the date
	 */
	private Date getDate(String strOIDDate){
		Date dt = null;
		try{
		DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss'Z'");
       // LDAP time in UTC - must set formatter
        TimeZone tz = TimeZone.getTimeZone("UTC");
        formatter.setTimeZone(tz);	        
        // parse into Date - converted to locale time zone
        dt = formatter.parse(strOIDDate);
		}catch (Exception exc) {
			//log.error(e.getMessage());
			exc.printStackTrace();
		}
		return dt;
	}
	
	/**
	 * return the open ldap date string from java date object.
	 *
	 * @param dtDate <code>Date</code>
	 * @return <code>String</code>
	 */
	private String getStringFromDate(Date dtDate){
		String strDate = null;
		try{
			DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss'Z'");
	       // LDAP time in UTC - must set formatter
	        TimeZone tz = TimeZone.getTimeZone("UTC");
	        formatter.setTimeZone(tz);	        
	        strDate = formatter.format(dtDate);
		}catch (Exception exc) {
			//log.error(e.getMessage());
			exc.printStackTrace();
		}
		return strDate;
	}	
	
	/**
	 * Gets the password policy details.
	 *
	 * @return the password policy details
	 */
	public PasswordPolicyTO getPasswordPolicyDetails(){
		return passwordPolicy.clone();
	}
	
	/**
	 * Update password policy details.
	 *
	 * @param pPolicy the policy
	 * @throws Exception the exception
	 */
	public void updatePasswordPolicyDetails(PasswordPolicyTO pPolicy) throws Exception{
		Map<String,String> modifyDetail = new HashMap<String,String>();
		if(pPolicy.getPwdExpireWarning() != passwordPolicy.getPwdExpireWarning()){
			modifyDetail.put("pwdExpireWarning", String.valueOf(pPolicy.getPwdExpireWarning()));
		}
		if(pPolicy.getPwdFailureCountInterval() != passwordPolicy.getPwdFailureCountInterval()){
			modifyDetail.put("pwdFailureCountInterval", String.valueOf(pPolicy.getPwdFailureCountInterval()));
		}
		if(pPolicy.getPwdGraceAuthNLimit() != passwordPolicy.getPwdGraceAuthNLimit()){
			modifyDetail.put("pwdGraceAuthNLimit", String.valueOf(pPolicy.getPwdGraceAuthNLimit()));
		}
		if(pPolicy.getPwdInHistory() != passwordPolicy.getPwdInHistory()){
			modifyDetail.put("pwdInHistory", String.valueOf(pPolicy.getPwdInHistory()));
		}
		if(pPolicy.getPwdLockoutDuration() != passwordPolicy.getPwdLockoutDuration()){
			modifyDetail.put("pwdLockoutDuration", String.valueOf(pPolicy.getPwdLockoutDuration()));
		}
		if(pPolicy.getPwdMaxAge() != passwordPolicy.getPwdMaxAge()){
			modifyDetail.put("pwdMaxAge", String.valueOf(pPolicy.getPwdMaxAge()));
		}
		if(pPolicy.getPwdMaxFailure() != passwordPolicy.getPwdMaxFailure()){
			modifyDetail.put("pwdMaxFailure", String.valueOf(pPolicy.getPwdMaxFailure()));
		}
		if(pPolicy.getPwdMinAge() != passwordPolicy.getPwdMinAge()){
			modifyDetail.put("pwdMinAge", String.valueOf(pPolicy.getPwdMinAge()));
		}
		if(pPolicy.getPwdMinLength() != passwordPolicy.getPwdMinLength()){
			modifyDetail.put("pwdMinLength", String.valueOf(pPolicy.getPwdMinLength()));
		}
		if(pPolicy.isPwdAllowUserChange() != passwordPolicy.isPwdAllowUserChange()){
			modifyDetail.put("pwdAllowUserChange", Boolean.toString(pPolicy.isPwdAllowUserChange()).toUpperCase());
		}
		if(pPolicy.isPwdLockout() != passwordPolicy.isPwdLockout()){
			modifyDetail.put("pwdLockout", Boolean.toString(pPolicy.isPwdLockout()).toUpperCase());
		}
		if(pPolicy.isPwdMustChange() != passwordPolicy.isPwdMustChange()){
			modifyDetail.put("pwdMustChange", Boolean.toString(pPolicy.isPwdMustChange()).toUpperCase());
		}
		LDAPConnection objConnection = null;
    	try {	
    		objConnection = createManagerConnection();
    		Set<Entry<String, String>> modifyEntries = modifyDetail.entrySet();
			LDAPModification[] modifications = new LDAPModification[modifyEntries.size()];	
			int i=0;
			for(Iterator<Entry<String,String>> it = modifyEntries.iterator();it.hasNext();){
				Entry<String,String> entry = it.next();
				modifications[i++] = new LDAPModification( LDAPModification.REPLACE,new LDAPAttribute(entry.getKey(),entry.getValue()));
			}				
			objConnection.modify( defaultPwdPolicy, modifications);				
		} catch (Exception e) {
			if(log != null)
				log.error(e.getMessage());
			throw e;
		}finally{ 
			if(objConnection != null)
				objConnection.disconnect();
		}
    	fillPasswordPolicy();
	}

	/**
	 * Gets the domain active directory.
	 *
	 * @return the domain active directory
	 */
	public String getDomainActiveDirectory() {
		return domainActiveDirectory;
	}

	/**
	 * Sets the domain active directory.
	 *
	 * @param domainActiveDirectory the new domain active directory
	 */
	public void setDomainActiveDirectory(String domainActiveDirectory) {
		this.domainActiveDirectory = domainActiveDirectory;
	}
	
}
