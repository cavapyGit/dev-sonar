package com.pradera.security.utils.view;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.commons.utils.JSFUtilities;
import com.pradera.security.systemmgmt.facade.SystemServiceFacade;
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class ImageStreamerBean
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.1
 */
@Named
@RequestScoped
public class ImageStreamerBean implements Serializable{  

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	SystemServiceFacade systemServiceFacade;
	
	private StreamedContent fileContent;
	
	private StreamedContent fileSystemImage;
	
	
	/**
	 * @return fileContent
	 */
	public StreamedContent getFileContent() {
		try {
		    FacesContext context = FacesContext.getCurrentInstance();
		    if (context.getRenderResponse()) {
		        // So, we're rendering the view. Return a stub StreamedContent so that it will generate right URL.
		        return new DefaultStreamedContent();
		    }
		    
			Integer rowIndex=Integer.parseInt(JSFUtilities.getRequestParameterMap("rowIndex"));
			if(rowIndex.intValue()>=0){
				byte[] imageFile = systemServiceFacade.getSystemImage(rowIndex);
				InputStream input = new ByteArrayInputStream(imageFile);
				fileContent=new DefaultStreamedContent(input, "image/jpeg"); 
			}
		} catch (Exception e) {
			//fileContent=defaultFileContent;
			e.printStackTrace();
		}
		return fileContent;
	}
	
	
	/**
	 * @param fileContent
	 */
	public void setFileContent(StreamedContent fileContent) {
		this.fileContent = fileContent;
	}


	/**
	 * @return fileSystemImage
	 */
	public StreamedContent getFileSystemImage() {
		if(JSFUtilities.getSessionMap("sessStreamedImage")!=null){
			byte[] systemImage=(byte[])JSFUtilities.getSessionMap("sessStreamedImage");
			InputStream input = new ByteArrayInputStream(systemImage);
			fileSystemImage=new DefaultStreamedContent(input, "image/jpeg"); 
		}
		return fileSystemImage;
	}


	/**
	 * @param fileSystemImage
	 */
	public void setFileSystemImage(StreamedContent fileSystemImage) {
		this.fileSystemImage = fileSystemImage;
	}
	
	
	
	
	
}
