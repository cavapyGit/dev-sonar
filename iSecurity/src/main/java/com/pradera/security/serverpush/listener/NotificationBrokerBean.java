package com.pradera.security.serverpush.listener;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.apache.commons.lang3.StringEscapeUtils;
import org.codehaus.jackson.node.ObjectNode;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuElement;
import org.primefaces.model.menu.MenuItem;
import org.primefaces.model.menu.MenuModel;
import org.primefaces.push.PushContext;
import org.primefaces.push.PushContextFactory;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.configuration.StageDependent;
import com.pradera.commons.httpevent.NotificationEvent;
import com.pradera.commons.httpevent.publish.BrowserWindow;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.notifications.remote.Message;
import com.pradera.commons.notifications.remote.MessageFilter;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.type.AESKeyType;
import com.pradera.commons.utils.AESEncryptUtils;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.security.notification.facade.NotificationServiceFacade;
import com.pradera.security.notification.service.InsolutionNotificationService;
import com.pradera.security.serverpush.view.NotificationBean;
import com.pradera.security.usermgmt.service.AlertsServiceBean;
import com.pradera.security.utils.view.PropertiesConstants;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class NotificationBrokerBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/05/2013
 */
@ApplicationScoped
public class NotificationBrokerBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8819867026711419761L;
	
	/** The log. */
	@Inject
	transient PraderaLogger log;
	
	/** The security path. */
	@Inject @StageDependent
	String serverPath;
	
	
	/** The url inbox notifications. */
	@Inject @Configurable
	String urlInboxNotifications;
	
	@Inject @Configurable
	String urlInboxProcess;
	
	/** The max results notifications. */
	@Inject @Configurable
	Integer maxResultsNotifications;
	
	/** The emailSendingBean. */
	@EJB
	private EmailSendingBean emailSendingBean;
	
	@Inject
	InsolutionNotificationService insolutionNotificationService;
	
	/** The set user info. */
	private List<NotificationBean> setNotificationBean;
	
	/** The push context. */
	//private final transient PushContext pushContext = PushContextFactory.getDefault().getPushContext();
	
	/**The IMonitorProcessRemote. */
	@EJB
	NotificationServiceFacade notificationServiceFacade;
	
	/** The menu all descriptions. */
	private String menuAllDescriptions =null;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		setNotificationBean = new ArrayList<NotificationBean>();
		Properties tmpProp=new Properties();
		try{
		//Url Server Security is read from parameters with stage(Development,Systemtest,Production)
		tmpProp.load(ClientRestService.class.getResourceAsStream("/GenericConstants.properties"));
		menuAllDescriptions = tmpProp.getProperty("notifications.menu.see.all");
		} catch (Exception ex){
			log.error(ex.getMessage());
		}
	}
	
	/** The tmp prop. */
	Properties tmpProp=new Properties();
	
	/**
	 * Instantiates a new notification broker bean.
	 */
	public NotificationBrokerBean() {
		try{
			tmpProp.load(AlertsServiceBean.class.getResourceAsStream("/ParametersEmail.properties"));
	    } catch (IOException e) {
		    e.printStackTrace();
	    }
	}
	/**
	 * Notify process.
	 *
	 * @param browser the browser
	 */
	public void notifyProcess(@Observes @NotificationEvent(NotificationType.PROCESS) BrowserWindow browser) {
		log.info("notification was fire from remote app in type process");
		for(NotificationBean notificationBean : setNotificationBean) {
			if (notificationBean.getUserName().equals(browser.getUserChannel())) {
				//update structure menu
				MenuModel menuNotificationMsgs = notificationBean.getMenuMessages();
				MenuModel menuNotification = new DefaultMenuModel();
				DefaultMenuItem menu = new DefaultMenuItem();
				menu.setValue(CommonsUtilities.truncateString(browser.getMessage(), 200, null));
				menu.setTarget("gameFrame");
				menu.setIcon("ui-icon-info");
				menu.setUrl(getUrlRemote(createUrl(NotificationType.PROCESS.getCode())+"?"+GeneralConstants.PROCESS_ID+"="+browser.getIdNotification(),
						notificationBean.getSessionTicket()) );
				menu.setId("menuProcess"+browser.getIdNotification());
				menu.setOnclick("$(this).attr('target',window.frames[0].name);");
				menuNotification.addElement(menu);
				notificationBean.setTotalMessages(notificationBean.getTotalMessages()+1);
				List<MenuElement> listMenuItems = menuNotificationMsgs.getElements();
				Iterator<MenuElement> iter= listMenuItems.iterator();
				if(listMenuItems.size()<(maxResultsNotifications+1)){
					while(iter.hasNext()){
						MenuItem item = (MenuItem)iter.next();
						menuNotification.addElement(item);
					}
				} else {
					//only show maximun notifications parameter
					int countEnableds =1;
					while(iter.hasNext() && countEnableds < maxResultsNotifications){
						MenuItem item = (MenuItem)iter.next();
						menuNotification.addElement(item);
						countEnableds++;
					}
					//last element see all
					menuNotification.addElement(createMenuseeAll(NotificationType.PROCESS.getCode(),createUrl(NotificationType.PROCESS.getCode()), notificationBean.getSessionTicket()));
				}
				//added 
				notificationBean.setMenuMessageTitle(""+notificationBean.getTotalMessages());
				notificationBean.setMenuMessages(menuNotification);
			}
		}
		//before send to screen convert to base64 for problems encoding json
//		browser.setSubject(CommonsUtilities.getStringBase64(browser.getSubject()));
//		browser.setMessage(CommonsUtilities.getStringBase64(browser.getMessage()));
//		pushContext.push(PropertiesConstants.PUSH_MSG+"/"+browser.getUserChannel(),
//				browser);
	}
	
	/**
	 * Notify message.
	 *
	 * @param browser the browser
	 */
	public void notifyMessage(@Observes @NotificationEvent(NotificationType.MESSAGE) BrowserWindow browser) {
		log.info("notification was fire from remote app in type message");
		for(int i=0;i<setNotificationBean.size();i++) {
			NotificationBean notificationBean = setNotificationBean.get(i);
			if (notificationBean.getUserName().equals(browser.getUserChannel())) {
				//update structure menu
				MenuModel menuNotificationMsgs = notificationBean.getMenuMessages();
				MenuModel menuNotification = new DefaultMenuModel();
				DefaultMenuItem menu = new DefaultMenuItem();
				menu.setValue(CommonsUtilities.truncateString(browser.getMessage(), 200, null));
				menu.setTarget("gameFrame");
				menu.setIcon("ui-icon-info");
				menu.setUrl(getUrlRemote(createUrl(NotificationType.MESSAGE.getCode())+"?"+GeneralConstants.MESSAGE_ID+"="+browser.getIdNotification(),
						notificationBean.getSessionTicket()) );
				menu.setId("menuMessage"+browser.getIdNotification());
				menu.setOnclick("$(this).attr('target',window.frames[0].name);");
				menuNotification.addElement(menu);
				notificationBean.setTotalMessages(notificationBean.getTotalMessages()+1);
				List<MenuElement> listMenuItems = menuNotificationMsgs.getElements();
				Iterator<MenuElement> iter= listMenuItems.iterator();
				if(listMenuItems.size()<(maxResultsNotifications+1)){
					while(iter.hasNext()){
						MenuItem item = (MenuItem)iter.next();
						menuNotification.addElement(item);
					}
				} else {
					//only show maximun notifications parameter
					int countEnableds =1;
					while(iter.hasNext() && countEnableds < maxResultsNotifications){
						MenuItem item = (MenuItem)iter.next();
						menuNotification.addElement(item);
						countEnableds++;
					}
					//last element see all
					menuNotification.addElement(createMenuseeAll(NotificationType.MESSAGE.getCode(),createUrl(NotificationType.MESSAGE.getCode()), notificationBean.getSessionTicket()));
				}
				//added 
				notificationBean.setMenuMessageTitle(""+notificationBean.getTotalMessages());
				notificationBean.setMenuMessages(menuNotification);
			}
		}
		//before send to screen convert to base64 for problems encoding json
//		browser.setSubject(CommonsUtilities.getStringBase64(browser.getSubject()));
//		browser.setMessage(CommonsUtilities.getStringBase64(browser.getMessage()));
//		pushContext.push(PropertiesConstants.PUSH_MSG+"/"+browser.getUserChannel(),
//				browser);
	}
	
	/**
	 * Notify email.
	 *
	 * @param browser the browser
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void notifyEmail(@Observes @NotificationEvent(NotificationType.EMAIL) BrowserWindow browser) throws IOException {
		Map<String, String> mapEmailIds = new HashMap<String, String>();
		String[] emails=browser.getUserChannel().split(";");
		for(String emailID:emails){
			if(!mapEmailIds.containsKey(emailID.toUpperCase())){
				mapEmailIds.put(emailID.toUpperCase(), emailID);
				emailSendingBean.sendMail(emailID, browser);
			}
		}
		
	}
	public void notifySMS(@Observes @NotificationEvent(NotificationType.SMS) BrowserWindow browser) throws IOException {
		String[] phones=browser.getUserChannel().split(";");
		for(String phone:phones){
			//insolutionNotificationService.processInsolutionApiSendSMS(phone, browser.getMessage());
		}
	}

	/**
	 * Notify report.
	 *
	 * @param browser the browser
	 */
	public void notifyReport(@Observes @NotificationEvent(NotificationType.REPORT) BrowserWindow browser){
		log.info("notification was fire from remote app in type report");
		for(NotificationBean notificationBean : setNotificationBean) {
			if (notificationBean.getUserName().equals(browser.getUserChannel())) {
				notificationBean.setTotalReports(notificationBean.getTotalReports()+1);
				notificationBean.setMenuReportTitle(""+notificationBean.getTotalReports());
			}
		}
		//before send to screen convert to base64 for problems encoding json
//		browser.setSubject(CommonsUtilities.getStringBase64(browser.getSubject()));
//		browser.setMessage(CommonsUtilities.getStringBase64(browser.getMessage()));
//		pushContext.push(PropertiesConstants.PUSH_MSG+"/"+browser.getUserChannel(),
//				browser);
	}
	
	/**
	 * Creates the menu notifications.
	 *
	 * @param notification the notification
	 * @param notificationType the notification type
	 * @param status the status
	 * @return the menu model
	 */
	public void createMenuNotifications(NotificationBean notification,Integer notificationType,Integer status){
		MenuModel menuNotification = new DefaultMenuModel();
		MessageFilter messageFilter = new MessageFilter();
		messageFilter.setNotificationType(notificationType);
		messageFilter.setRead(BooleanType.NO.getCode());
		messageFilter.setUserReceptor(notification.getUserName());
		int totalNotifications =0;
		List<Message> messages =null;
		try {
			messages = notificationServiceFacade.searchNotifications(messageFilter);
			if(messages != null && !messages.isEmpty()){
				totalNotifications = messages.size();
				if(totalNotifications > maxResultsNotifications){
					//only display max notifications
					messages = messages.subList(0, maxResultsNotifications);
				}
				for(Message message : messages){
					DefaultMenuItem menu = new DefaultMenuItem();
					menu.setValue(CommonsUtilities.truncateString(StringEscapeUtils.escapeHtml4(message.getMessageBody()), 200, null));
					menu.setIcon("ui-icon-info");
					menu.setTarget("gameFrame");
					if(notificationType.equals(NotificationType.MESSAGE.getCode())){
						menu.setUrl(getUrlRemote(createUrl(notificationType)+"?"+GeneralConstants.MESSAGE_ID+"="+message.getIdMessage(),
								notification.getSessionTicket()) );
						menu.setId("menuMessage"+message.getIdMessage());
					} else {
						menu.setUrl(getUrlRemote(createUrl(notificationType)+"?"+GeneralConstants.PROCESS_ID+"="+message.getIdMessage(),
								notification.getSessionTicket()) );
						menu.setId("menuProcess"+message.getIdMessage());
					}
					menu.setOnclick("$(this).attr('target',window.frames[0].name);");
					menuNotification.addElement(menu);
				}
			}
			//added last option
			menuNotification.addElement(createMenuseeAll(notificationType,createUrl(notificationType), notification.getSessionTicket()));
			if(notificationType.equals(NotificationType.MESSAGE.getCode())){
				notification.setMenuMessages(menuNotification);
				//total notifications message
				notification.setMenuMessageTitle(""+totalNotifications);
				notification.setTotalMessages(totalNotifications);
			} else {
				//process
				notification.setMenuProcess(menuNotification);
				notification.setMenuProcessTitle(""+totalNotifications);
				notification.setTotalProcess(totalNotifications);
			}
		} catch (Exception sexc){
			log.error("error de service "+sexc.getMessage());
		}
	}
	
	
	
	/**
	 * Creates the url.
	 *
	 * @param notificationType the notification type
	 * @return the string
	 */
	private String createUrl(Integer notificationType){
		String url =null;
		if(notificationType.equals(NotificationType.MESSAGE.getCode())){
			url = serverPath+urlInboxNotifications;
		} else {
			//process
			url = serverPath+urlInboxProcess;
		}
		return url;
	}
	
	/**
	 * Creates the menusee all.
	 *
	 * @param notificationType the notification type
	 * @param url the url
	 * @param tickect the tickect
	 * @return the menu item
	 */
	private MenuItem createMenuseeAll(Integer notificationType,String url,String tickect){
		DefaultMenuItem menu = new DefaultMenuItem();
		menu.setValue(menuAllDescriptions);
		menu.setIcon("ui-icon-search");
		menu.setTarget("gameFrame");
		menu.setUrl(getUrlRemote(url, tickect) );
		if(notificationType.equals(NotificationType.MESSAGE.getCode())){
			menu.setId("menuMessage"+0);
		} else {
			menu.setId("menuProcess"+tickect);
		}
		menu.setOnclick("$(this).attr('target',window.frames[0].name);");
		return menu;
	}
	
	/**
	 * Gets the url remote.
	 *
	 * @param url the url
	 * @param ticket the ticket
	 * @return the url remote
	 */
	private String getUrlRemote(String url,String ticket){
		StringBuilder urlBuilder = new StringBuilder(100);
		try{
		urlBuilder.append(url);
		if(url.contains("?")){
			urlBuilder.append("&");
		}else{
			urlBuilder.append("?");
		}
		urlBuilder.append("tikectId="+AESEncryptUtils.encrypt(
				AESKeyType.PRADERAKEY.getValue(),ticket));
		urlBuilder.append("&"+GeneralConstants.RESET_BEAN_MENU).
		append("=").append(GeneralConstants.RESET_BEAN_MENU);
		}catch(Exception ex){
			log.error("error building url remote "+ex.getMessage());
		}
		return urlBuilder.toString();
	}
	
	/**
	 * Gets the sets the notification bean.
	 *
	 * @return the sets the notification bean
	 */
	public List<NotificationBean> getSetNotificationBean() {
		return setNotificationBean;
	}

	/**
	 * Sets the sets the notification bean.
	 *
	 * @param setNotificationBean the new sets the notification bean
	 */
	public void setSetNotificationBean(List<NotificationBean> setNotificationBean) {
		this.setNotificationBean = setNotificationBean;
	}

}