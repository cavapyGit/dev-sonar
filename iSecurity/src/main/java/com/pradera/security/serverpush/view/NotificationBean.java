package com.pradera.security.serverpush.view;

import java.io.Serializable;

import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class NotificationBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/05/2013
 */
public class NotificationBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1430383458674836817L;
	
	/** The user name. */
	private String userName;
	
	/** The id session. */
	private Long idSession;
	
	/** The menu process. */
	private MenuModel menuProcess;
	
	/** The menu messages. */
	private MenuModel menuMessages;
	
	/** The menu title. */
	private String menuMessageTitle;
	
	/** The menu process title. */
	private String menuProcessTitle;
	
	/** The menu report title. */
	private String menuReportTitle;
	
	/** The session ticket. */
	private String sessionTicket;
	
	/** The total messages. */
	private int totalMessages;
	
	/** The total process. */
	private int totalProcess;
	
	/** The total reports. */
	private int totalReports;

	/**
	 * Instantiates a new notification bean.
	 */
	public NotificationBean() {
		menuMessages = new DefaultMenuModel();
		menuMessageTitle = "0";
		menuProcess = new DefaultMenuModel();
		menuProcessTitle = "0";
		menuReportTitle ="0";
	}
	
	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the menu process.
	 *
	 * @return the menu process
	 */
	public MenuModel getMenuProcess() {
		return menuProcess;
	}

	/**
	 * Sets the menu process.
	 *
	 * @param menuProcess the new menu process
	 */
	public void setMenuProcess(MenuModel menuProcess) {
		this.menuProcess = menuProcess;
	}

	/**
	 * Gets the menu messages.
	 *
	 * @return the menu messages
	 */
	public MenuModel getMenuMessages() {
		return menuMessages;
	}

	/**
	 * Sets the menu messages.
	 *
	 * @param menuMessages the new menu messages
	 */
	public void setMenuMessages(MenuModel menuMessages) {
		this.menuMessages = menuMessages;
	}

	/**
	 * Gets the id session.
	 *
	 * @return the id session
	 */
	public Long getIdSession() {
		return idSession;
	}

	/**
	 * Sets the id session.
	 *
	 * @param idSession the new id session
	 */
	public void setIdSession(Long idSession) {
		this.idSession = idSession;
	}

	/**
	 * Gets the menu message title.
	 *
	 * @return the menu message title
	 */
	public String getMenuMessageTitle() {
		return menuMessageTitle;
	}

	/**
	 * Sets the menu message title.
	 *
	 * @param menuMessageTitle the new menu message title
	 */
	public void setMenuMessageTitle(String menuMessageTitle) {
		this.menuMessageTitle = menuMessageTitle;
	}

	/**
	 * Gets the menu process title.
	 *
	 * @return the menu process title
	 */
	public String getMenuProcessTitle() {
		return menuProcessTitle;
	}

	/**
	 * Sets the menu process title.
	 *
	 * @param menuProcessTitle the new menu process title
	 */
	public void setMenuProcessTitle(String menuProcessTitle) {
		this.menuProcessTitle = menuProcessTitle;
	}

	/**
	 * Gets the session ticket.
	 *
	 * @return the session ticket
	 */
	public String getSessionTicket() {
		return sessionTicket;
	}

	/**
	 * Sets the session ticket.
	 *
	 * @param sessionTicket the new session ticket
	 */
	public void setSessionTicket(String sessionTicket) {
		this.sessionTicket = sessionTicket;
	}

	/**
	 * Gets the total messages.
	 *
	 * @return the total messages
	 */
	public int getTotalMessages() {
		return totalMessages;
	}

	/**
	 * Sets the total messages.
	 *
	 * @param totalMessages the new total messages
	 */
	public void setTotalMessages(int totalMessages) {
		this.totalMessages = totalMessages;
	}

	/**
	 * Gets the total process.
	 *
	 * @return the total process
	 */
	public int getTotalProcess() {
		return totalProcess;
	}

	/**
	 * Sets the total process.
	 *
	 * @param totalProcess the new total process
	 */
	public void setTotalProcess(int totalProcess) {
		this.totalProcess = totalProcess;
	}

	/**
	 * Gets the menu report title.
	 *
	 * @return the menu report title
	 */
	public String getMenuReportTitle() {
		return menuReportTitle;
	}

	/**
	 * Sets the menu report title.
	 *
	 * @param menuReportTitle the new menu report title
	 */
	public void setMenuReportTitle(String menuReportTitle) {
		this.menuReportTitle = menuReportTitle;
	}

	/**
	 * Gets the total reports.
	 *
	 * @return the total reports
	 */
	public int getTotalReports() {
		return totalReports;
	}

	/**
	 * Sets the total reports.
	 *
	 * @param totalReports the new total reports
	 */
	public void setTotalReports(int totalReports) {
		this.totalReports = totalReports;
	}

	
}
