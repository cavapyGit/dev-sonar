package com.pradera.security.serverpush.listener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

import com.pradera.commons.configuration.type.StageApplication;
import com.pradera.commons.httpevent.publish.BrowserWindow;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.security.notification.service.InsolutionNotificationService;
import com.pradera.security.usermgmt.service.AlertsServiceBean;
import com.sun.mail.util.MailSSLSocketFactory;

/**
 * @author PraderaTechnologies
 *
 */
@Stateless
public class EmailSendingBean implements Serializable {

	/**default serialVersionUID*/
	private static final long serialVersionUID = 1L;
	/** The log. */
	@Inject
	PraderaLogger log;
	
	@Inject
	InsolutionNotificationService insolutionNotificationService;
	
	@Inject 
	StageApplication stateApplication;
	
	/** The tmpProp. */
	private Properties tmpProp;
	
	/**default onstructor*/
	public EmailSendingBean() {
		try{
			tmpProp=new Properties();
			tmpProp.load(AlertsServiceBean.class.getResourceAsStream("/ParametersEmail.properties"));
	    } catch (IOException e) {
		    e.printStackTrace();
	    }
	}
	
	/**
	 * to send email(s) Asynchronously 
 	 * @param emailID
	 * @param browser
	 * @throws IOException
	 */
	@Asynchronous
	public void sendMail(String emailID,BrowserWindow browser) throws IOException {
		FileOutputStream foStream=null;
		InputStream inputFile =null;

		String to = emailID;

		String from = tmpProp.getProperty("alert.from.mail");
		final String username = tmpProp.getProperty("alert.user.mail");
		final String password = tmpProp.getProperty("alert.password.mail");

		// Assuming you are sending email through relay.jangosmtp.net
		String host = tmpProp.getProperty("alert.server.mail");

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", tmpProp.getProperty("alert.smtpport"));
		props.put("mail.smtp.ssl.protocols", "TLSv1.2");
		
		MailSSLSocketFactory sf;
		try {
			sf = new MailSSLSocketFactory();

			sf.setTrustAllHosts(true); 
			props.put("mail.smtp.ssl.trust", host);
			props.put("mail.smtp.ssl.socketFactory", sf);
		} catch (GeneralSecurityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Get the Session object.
		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {		
			// Create a default MimeMessage object.
			Message message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));

			MimeMultipart content = new MimeMultipart();

			// Set Subject: header field
			message.setSubject(browser.getSubject());

			//Load image from resources
			inputFile =  Thread.currentThread().getContextClassLoader().getResourceAsStream(tmpProp.getProperty("alert.url"));
	        File tmpLogo =File.createTempFile("logo",".jpg");
	        foStream = new FileOutputStream(tmpLogo);
	        foStream.write(IOUtils.toByteArray(inputFile));
	        
	        MimeBodyPart imagePart = new MimeBodyPart();
	        imagePart.attachFile(tmpLogo);
	        imagePart.setHeader("Content-ID", "<logoimg>");
	        content.addBodyPart(imagePart);	     
	        
	        //Create html body	        
	        StringBuilder htmlBody = new StringBuilder();
	        htmlBody.append("<html><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />");
			
			String stringToConvert[] = {"á","Á","é","É","í","Í","ó","Ó","ú","Ú","ñ","Ñ"};
			String stringConverted[] = new String[12];
			
			if(StringUtils.isNotBlank(browser.getMessage())){
	
				for(int i=0;i<stringToConvert.length;i++){		
					stringConverted[i]	= StringEscapeUtils.escapeHtml4(stringToConvert[i]);			
				}
				htmlBody.append(StringUtils.replaceEachRepeatedly(browser.getMessage(), stringToConvert, stringConverted));			
			}
			
			String app = StageApplication.Development.name();
			Boolean showEnvironment = Boolean.TRUE;
			if(Validations.validateIsNotNull(stateApplication)) {
				if(stateApplication.equals(StageApplication.Production)) {
					showEnvironment = Boolean.FALSE;
				} else {
					app = stateApplication.name();
				}
			}
			
			if(showEnvironment)
				htmlBody.append(""+tmpProp.getProperty("alert.message.environment.name")+" " + app + "<br>");
			
			htmlBody.append("<br>"+tmpProp.getProperty("alert.message.dont.response.mail")+"<br><br>");
			htmlBody.append("<img src=\"cid:logoimg\"><br><br>");
			htmlBody.append(tmpProp.getProperty("alert.footer.mail")+"<br>");
			htmlBody.append("</html>");
			
			BodyPart htmlPart = new MimeBodyPart();
			htmlPart.setContent(htmlBody.toString(), "text/html");
			content.addBodyPart(htmlPart);	  
			
			// Send the actual HTML message
			message.setContent(content);

			// Send message
			Transport.send(message);

			System.out.println("Sent message successfully....");

		} catch (MessagingException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		/*
		try{
			HtmlEmail email = new HtmlEmail();
			email.setHostName(tmpProp.getProperty("alert.server.mail"));
			email.setFrom(tmpProp.getProperty("alert.from.mail"), tmpProp.getProperty("alert.pradera"));
			email.setAuthentication(tmpProp.getProperty("alert.user.mail"), tmpProp.getProperty("alert.password.mail"));
			email.setSmtpPort(Integer.parseInt(tmpProp.getProperty("alert.smtpport")));
			//email.setSSL(true);
			email.setTLS(true);
			//Load image from resources
			inputFile =  Thread.currentThread().getContextClassLoader().getResourceAsStream(tmpProp.getProperty("alert.url"));
	        File tmpLogo =File.createTempFile("logo",".jpg");
	        foStream = new FileOutputStream(tmpLogo);
	        foStream.write(IOUtils.toByteArray(inputFile));
	  	  	String cid = email.embed(tmpLogo,tmpProp.getProperty("alert.email.embed"));
	  	  	// set the html message
			StringBuilder message = new StringBuilder();
			message.append("<html><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />");
			
			String stringToConvert[] = {"á","Á","é","É","í","Í","ó","Ó","ú","Ú","ñ","Ñ"};
			String stringConverted[] = new String[12];
			
			if(StringUtils.isNotBlank(browser.getMessage())){
	
				for(int i=0;i<stringToConvert.length;i++){		
					stringConverted[i]	= StringEscapeUtils.escapeHtml4(stringToConvert[i]);			
				}
				message.append(StringUtils.replaceEachRepeatedly(browser.getMessage(), stringToConvert, stringConverted));			
			}
			/*if(browser.getFilesAtach()!=null && !browser.getFilesAtach().isEmpty()) {
				
				for(FileAtach fileAtach:browser.getFilesAtach()) {
					message.append("</br>").append(fileAtach.getFileTitle()).append("</br>");
					byte[] file = Base64.getDecoder().decode(fileAtach.getDataFileB64());
					
			        File tmpFile =File.createTempFile(fileAtach.getFileName(),fileAtach.getExtension());
			        foStream = new FileOutputStream(tmpFile);
			        foStream.write(file);
			  	  	String cidFile = email.embed(tmpFile,fileAtach.getFileName());
					
					message.append("<br>"+"<img src=\"cid:" + cidFile + "\"><br><br>");
				}
			}/
			
			message.append("<br>"+tmpProp.getProperty("alert.message.dont.response.mail")+"<br><br>");
			message.append("<img src=\"cid:" + cid + "\"><br><br>");
			message.append(tmpProp.getProperty("alert.footer.mail")+"<br>");
			message.append("</html>");
			email.setSubject(browser.getSubject());
			email.setHtmlMsg(message.toString());
			email.addTo(emailID);
			String priorityValue = browser.getPriority();
			if (priorityValue != null && priorityValue.trim().length() > 0) {
				Map<String, String> headerMap = new HashMap<String, String>();
				headerMap.put("X-Priority", priorityValue);
				email.setHeaders(headerMap);
	        }
			
			email.send();
			//UserAccountSession userAccountSession=new UserAccountSession();
			//userAccountSession.setEmail(emailID);
			//insolutionNotificationService.processInsolutionApiSendMail(Arrays.asList( userAccountSession ), null, message.toString(), browser.getSubject(),true);
		}
		catch (EmailException emailException) {
			log.error("Unable to send an email to the this User::::"+emailID);
			log.error(emailException.getMessage());
			emailException.printStackTrace();
		} catch (Exception ex) {
			log.error(ex.getMessage());
			ex.printStackTrace();
		}finally{
			if(foStream != null)
				foStream.close();
			if(inputFile != null)
				inputFile.close();
		}*/
	}
}
