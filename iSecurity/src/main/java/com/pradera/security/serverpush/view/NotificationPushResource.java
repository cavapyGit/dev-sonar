package com.pradera.security.serverpush.view;

import org.primefaces.push.EventBus;
import org.primefaces.push.RemoteEndpoint;
import org.primefaces.push.annotation.OnMessage;
import org.primefaces.push.annotation.PathParam;
import org.primefaces.push.annotation.PushEndpoint;
import org.primefaces.push.impl.JSONDecoder;
import org.primefaces.push.impl.JSONEncoder;
import org.primefaces.push.annotation.Singleton;

@PushEndpoint("/message/{userName}")
@Singleton
public class NotificationPushResource {

	@PathParam(value = "userName")
	private String userName;

	@OnMessage(decoders = JSONDecoder.class, encoders = JSONEncoder.class)
	public void onMessage(RemoteEndpoint r, EventBus eventBus) {
		System.out.println("pushing to " + userName);
	}

}
