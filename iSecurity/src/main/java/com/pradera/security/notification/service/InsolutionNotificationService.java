package com.pradera.security.notification.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.codehaus.jettison.json.JSONObject;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.notifications.remote.FileAtach;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

@Stateless
public class InsolutionNotificationService {
	
	@Inject
	PraderaLogger log;
	
	@Inject @Configurable
	String insolutionsWSsourceEmail;
	
	@Inject @Configurable
	String insolutionsWSsourceHost;
	
	@Inject @Configurable
	String insolutionsWSsourceToken;
	
	@Inject @Configurable
	String insolutionsWSuserName;
	
	@Inject @Configurable
	String insolutionsWSpassword;
	
	@Inject @Configurable
	String insolutionsWSsourceSms;
	
	@Inject @Configurable
	String insolutionsWStoken;
	
	public void processInsolutionApiSendMail(List<UserAccountSession> userDestinations, List<FileAtach> filesAtach, String bodyMessage, String subjetMessage, boolean isContentHtml) {
    	log.info(" inicio realizar proceso de email insolution api ");
    	
	    // destinatarios
    	ArrayNode correos = new ArrayNode(JsonNodeFactory.instance);
    	for(UserAccountSession dest : userDestinations) {
    		
    		ObjectNode correo = new ObjectNode(JsonNodeFactory.instance);
    		correo.put("name", dest.getFullName() != null ? dest.getFullName() : null);
    		correo.put("address", dest.getEmail());
    		
    		correos.add(correo);
    	}
		// din destinatarios
		
		// adjuntos
		ArrayNode  attachedFiles = new ArrayNode(JsonNodeFactory.instance);
		if(Validations.validateListIsNotNullAndNotEmpty(filesAtach)) {
			
			ObjectNode attached = new ObjectNode(JsonNodeFactory.instance);
			
			int num = 0;
			for(FileAtach adj : filesAtach) {
				num = num +1;
				
				String b64 = null;
				if(adj.getFile()!=null) {
					byte[] encodedBytes = Base64.getEncoder().encode(adj.getFile());
				    b64 =  new String(encodedBytes);
				}else {
					if(adj.getDataFileB64()!=null) {
						 b64 = adj.getDataFileB64();
					}
				}
			    
				attached = new ObjectNode(JsonNodeFactory.instance);
				attached.put("fileBase64", b64 != null ? b64 : null);
				
				String fileName = null;
				if(adj.getExtension()!= null) {
					if(adj.getExtension().equals(GeneralConstants.STR_PDF)) {
						fileName = adj.getFileName() != null ? (adj.getFileName()+".pdf") : (GeneralConstants.ATTACHED_DEFAULT_NAME +"_"+num+".pdf");
						attached.put("contentType", "application/pdf");
						attached.put("extension", "pdf");
					}else if(adj.getExtension().equals(GeneralConstants.STR_PNG)) {
						fileName = adj.getFileName() != null ? (adj.getFileName()+".png") : (GeneralConstants.ATTACHED_DEFAULT_NAME +"_"+num+".png");
						attached.put("contentType", "application/png");
						attached.put("extension", "png");
					}
				}
				attached.put("fileName", fileName);
				
				attachedFiles.add(attached);
			}
		}
		// fin adjuntos
		
		// body mail
		ObjectNode head = new ObjectNode(JsonNodeFactory.instance);
		head.set("recipientsList",  correos);
		head.put("ccRecipientsList", "");  
		head.put("bccRecipientsList", ""); 
		head.put("subject", subjetMessage != null ? subjetMessage : GeneralConstants.ATTACHED_DEFAULT_SUBJECT); 
		head.put("content", bodyMessage != null ? bodyMessage : GeneralConstants.ATTACHED_DEFAULT_MESSAGE); 
		head.put("isContentHtml", isContentHtml); 
		head.set("attachedFiles",  attachedFiles.size() > 0 ? attachedFiles : null); 
		// end body mail
    	
		this.consumeApiInsolutionDinamicNotification(head, insolutionsWSsourceEmail);
		
		log.info(" fin realizar proceso de email insolution api ");
    }
	
	public void consumeApiInsolutionDinamicNotification(ObjectNode headPrm, String insolutionsWSsourcePath) {
		log.info("::::: start consumeApiInsolutionDinamicNotification  :::::" + insolutionsWSsourcePath);
		try {
			
			String tokenResult = getInsolutionTokenToNotificactions();
			
			HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
	        interceptor.level(HttpLoggingInterceptor.Level.BODY);
	        
			OkHttpClient client = new OkHttpClient().newBuilder()
	                .connectTimeout(30, TimeUnit.SECONDS)
	                .readTimeout(30, TimeUnit.SECONDS)
	                .addInterceptor(interceptor)
	                .build();
	
			String data = headPrm.toString();
            JSONObject jsonObject = new JSONObject(data);
            data = jsonObject.toString();
              
            RequestBody body = null;
	        if (data != null) {
	            body = RequestBody.create(data.getBytes("UTF-8"), MediaType.parse("application/json"));
	        }
	        
	        Request.Builder builder = new Request.Builder()
	                .url(insolutionsWSsourceHost.concat(insolutionsWSsourcePath))
	                .method("POST", body)
	                .addHeader("Authorization","Bearer "+tokenResult);
	        if (data != null) {
	            builder.addHeader("Content-Type", "application/json")
	                    .addHeader("Content-Length", String.valueOf(data.length()));
	        }
	        
	        Response response = client.newCall(builder.build()).execute();
	        log.info("Responso :: " + response.toString());
	        log.info("::::: end consumeApiInsolutionDinamicNotification  :::::" + insolutionsWSsourcePath);
		} catch (Exception e) {
			log.info(e.getMessage());
			log.error(e.getMessage());
			log.info("::::: ERROR WEB SERVICE INSOLUTION serviceInsolutionApiNotification :::::" + insolutionsWSsourcePath);
		}
        
        
	}
	
	@Asynchronous
	public void processInsolutionApiSendSMSpersonalized(LoggerUser loggerUser, List<String> numbers, String notificationMessage) {
    	log.info(" inicio realizar proceso de sms personalized insolution api ");
    	for(String destMobileNumber : numbers) {
    		this.processInsolutionApiSendSMS(destMobileNumber, notificationMessage);
    	}
		log.info(" fin realizar proceso de sms personalized insolution api ");
    }
	
	@Asynchronous
	public void processInsolutionApiSendSMS(String destMobileNumber, String notificationMessage) {
    	log.info(" inicio realizar proceso de sms insolution api ");
		// body mail
		ObjectNode head = new ObjectNode(JsonNodeFactory.instance);
		head.put("phoneNumber", destMobileNumber);
		head.put("message", notificationMessage);  
		// end body mail
		this.consumeApiInsolutionDinamicNotification(head, insolutionsWSsourceSms);
		log.info(" fin realizar proceso de sms insolution api ");
    }
	
	public String getInsolutionTokenToNotificactions() {
		log.info(" :: inicio proceso de getInsolutionTokenToNotificactions :: ");

		String tokenResult = null;
		
		ObjectNode head = new ObjectNode(JsonNodeFactory.instance);
		head.put("userName", insolutionsWSuserName);
		head.put("password", insolutionsWSpassword);  
		String input = head.toString();
		
		try {
			URL url = new URL(insolutionsWSsourceHost.concat(insolutionsWSsourceToken)); 
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			
			OutputStream os = conn.getOutputStream();
			os.write(input.getBytes());
			os.flush();
			
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			String output;
			log.info(" :: Output from Server :: ");
			
			JsonObject jsonObject = null;
			while ((output = br.readLine()) != null) {
				jsonObject = new JsonParser().parse(output.toString()).getAsJsonObject();

			}
			
			if(jsonObject!=null) {
				JsonObject jsonObjectData = (JsonObject) jsonObject.get("data");
				tokenResult= jsonObjectData.get("token").getAsString();
			}
			
			conn.disconnect();
 		} catch (Exception e) {
			e.printStackTrace();
			log.info(e.getMessage());
			log.error(e.getMessage());
			log.info("::::: ERROR WEB SERVICE INSOLUTION getInsolutionTokenToNotificactions :::::");
		}
		log.info(" :: fin proceso de getInsolutionTokenToNotificactions :: ");
		
		return tokenResult;
	}
	

}
