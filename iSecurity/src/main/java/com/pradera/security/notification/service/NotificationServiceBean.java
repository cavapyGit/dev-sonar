package com.pradera.security.notification.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.Query;

import com.pradera.commons.httpevent.NotificationEvent;
import com.pradera.commons.httpevent.publish.BrowserWindow;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.notifications.remote.NotificationMessgaeDetail;
import com.pradera.commons.security.model.type.UserAccountStateType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.model.UserAccount;
import com.pradera.security.model.type.UserAccountType;
import com.pradera.security.systemmgmt.service.CrudSecurityServiceBean;

@Stateless
public class NotificationServiceBean extends CrudSecurityServiceBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2870137289518910492L;

	@Inject
	@NotificationEvent(NotificationType.MESSAGE)
	private Event<BrowserWindow> eventMessage;
	
	@Inject
	@NotificationEvent(NotificationType.EMAIL)
	private Event<BrowserWindow> eventEmail;
	
	/**
	 * System modifed or blocked or unblocked send message notification to cevldom users
	 * system Option modified or rejected ..
	 * new user registered
	 * @param userName
	 * @param message
	 * @param notificationMessgaeDetail
	 * @throws ServiceException
	 */
	public void notificationMessageServiceBean(String userName,String message,NotificationMessgaeDetail notificationMessgaeDetail){
		BrowserWindow browser = new BrowserWindow();
		browser.setSubject(message);
		browser.setMessage(message);
		browser.setNotificationType(NotificationType.MESSAGE.getCode());
		browser.setUserChannel(userName);
		browser.setNotificationMessgaeDetail(notificationMessgaeDetail);
		eventMessage.fire(browser);
	}
	
	/**
	 * getting the cevldom users from the DataBase
	 * @return
	 * @throws ServiceException
	 */
	public List<UserAccount> internalUsersListServiceBean()throws ServiceException{
		StringBuilder sql=new StringBuilder();
		sql.append("select UA from UserAccount UA");
		sql.append(" where UA.state=:statePrm ");
		sql.append(" and UA.type=:typePrm ");
		
		Query query = em.createQuery(sql.toString());
		query.setParameter("typePrm",UserAccountType.INTERNAL.getCode());
		query.setParameter("statePrm",UserAccountStateType.CONFIRMED.getCode());
		query.setHint("org.hibernate.cacheable", true);
		return (List<UserAccount>)query.getResultList();
	}
	
	public void emailMessageServiceBean(StringBuilder userEmails,String subject,String message){
		BrowserWindow browser = new BrowserWindow();
		browser.setSubject(subject);
		browser.setMessage(message);
		browser.setUserChannel(userEmails.toString());
		browser.setNotificationType(NotificationType.EMAIL.getCode());
		eventEmail.fire(browser);
	}
}
