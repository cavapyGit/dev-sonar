package com.pradera.security.notification.facade;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.notifications.remote.Message;
import com.pradera.commons.notifications.remote.MessageFilter;
import com.pradera.commons.notifications.remote.NotificationRegisterTO;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.notification.service.NotificationServiceBean;



/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class NotificationServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/10/2013
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class NotificationServiceFacade {

	/** The service bean. */
	@EJB
	NotificationServiceBean serviceBean;
	
	/** The i process remote. */
	@Inject 
	ClientRestService clientRestService;
	
	/** The transaction repository bean. */
    @Resource
    private TransactionSynchronizationRegistry transactionRegistry;
    
    @Inject
    PraderaLogger log;
    
    /**
     * Send notification remote.
     *
     * @param userName the user name
     * @param businessProcess the business process
     * @param users the users
     * @param subject the subject
     * @param message the message
     * @param notification the notification
     */
    public void sendNotificationRemote(String userName, Long businessProcess, List<UserAccountSession> users, String subject,String message, NotificationType notification){
    	log.info("Start  method sendNotificationRemote() ");
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	if(loggerUser !=null && loggerUser.getIdPrivilegeOfSystem()==null){
    		//default privilege register from is caller
    		loggerUser.setIdPrivilegeOfSystem(GeneralConstants.TWO_VALUE_INTEGER);
    	}
    	if(subject.length() > 100){
    	 subject = subject.substring(0, 100);
    	}
    	NotificationRegisterTO notificationRegister = new NotificationRegisterTO();
    	notificationRegister.setLoggerUser(loggerUser);
    	notificationRegister.setUserName(userName);
    	notificationRegister.setBusinessProcess(businessProcess);
    	notificationRegister.setUsers(users);
    	notificationRegister.setSubject(subject);
    	notificationRegister.setMessage(message);
    	notificationRegister.setNotificationType(notification.getCode());
    	//clientRestService.sendNotificationManual(notificationRegister);;
    	log.info("End  method sendNotificationRemote() ");
    } 
    
    /**
     * Update notification status.
     *
     * @param userName the user name
     * @param notifications the notifications
     * @param status the status
     * @param notificationType the notification type
     * @throws ServiceException the service exception
     */
    public void updateNotificationStatus(String userName,List<Long> notifications,Integer status,Integer notificationType) throws ServiceException{
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	if(loggerUser !=null && loggerUser.getIdPrivilegeOfSystem()==null){
    		//default privilege register from is caller
    		loggerUser.setIdPrivilegeOfSystem(GeneralConstants.TWO_VALUE_INTEGER);
    	}
    	NotificationRegisterTO notificationRegister = new NotificationRegisterTO();
    	notificationRegister.setLoggerUser(loggerUser);
    	notificationRegister.setUserName(userName);
    	notificationRegister.setNotifications(notifications);
    	notificationRegister.setStatus(status);
    	notificationRegister.setNotificationType(notificationType);
    //	clientRestService.updateNotificationStatus(notificationRegister);
    }
    
    /**
     * Search notifications.
     *
     * @param messageFilter the message filter
     * @return the list
     */
    public List<Message> searchNotifications(MessageFilter messageFilter){
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	if(loggerUser !=null && loggerUser.getIdPrivilegeOfSystem()==null){
    		//default privilege register from is caller
    		loggerUser.setIdPrivilegeOfSystem(GeneralConstants.TWO_VALUE_INTEGER);
    	}
    //	return clientRestService.searchNotifications(messageFilter);
    	return null;
    }
}

