package com.pradera.security.usermgmt.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.security.model.type.UserAccountStateType;
import com.pradera.commons.security.model.type.UserSessionStateType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.model.Holiday;
import com.pradera.security.model.InstitutionTypeDetail;
import com.pradera.security.model.InstitutionsSecurity;
import com.pradera.security.model.OptionPrivilege;
import com.pradera.security.model.SystemOption;
import com.pradera.security.model.SystemProfile;
import com.pradera.security.model.UserAccount;
import com.pradera.security.model.UserManualProfile;
import com.pradera.security.model.UserProfile;
import com.pradera.security.model.UserProfileAllowed;
import com.pradera.security.model.UserSession;
import com.pradera.security.model.type.OptionPrivilegeStateType;
import com.pradera.security.model.type.OptionStateType;
import com.pradera.security.model.type.ProfilePrivilegeStateType;
import com.pradera.security.model.type.SystemProfileStateType;
import com.pradera.security.model.type.SystemStateType;
import com.pradera.security.model.type.UserAccountType;
import com.pradera.security.model.type.UserProfileStateType;
import com.pradera.security.systemmgmt.service.CrudSecurityServiceBean;
import com.pradera.security.usermgmt.view.filters.UserAccountFilter;

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class UserAccountServiceBean
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class UserAccountServiceBean extends CrudSecurityServiceBean{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 299076168112051656L;
	
	/**
	 * Validate login user service bean.
	 *
	 * @param userAccount the user account
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int validateLoginUserServiceBean(UserAccount userAccount) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append("Select Count(u) from UserAccount u where u.loginUser=:loginUserPrm");
		sbQuery.append(" and  u.state !=:state");
		if(userAccount.getIdUserPk()!=null){
			sbQuery.append(" and u.idUserPk != :idUserPkPrm");
		}
		Query query=em.createQuery(sbQuery.toString());
		query.setParameter("loginUserPrm", userAccount.getLoginUser());
		query.setParameter("state", UserAccountStateType.REJECTED.getCode());
		if(userAccount.getIdUserPk()!=null){
			query.setParameter("idUserPkPrm", userAccount.getIdUserPk());
		}
		query.setHint("org.hibernate.cacheable", true);
		return Integer.parseInt( query.getSingleResult().toString() );
	}
	
	/**
	 * Search users with schedule exception by filter service bean.
	 *
	 * @author mmacalupu
	 * Search users with schedule exception by filter service bean.
	 * method is useful to get el users with his scheduleexception
	 * @param userAccountFilter the user account filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<UserAccount> searchUsersWithScheduleExceptionByFilterServiceBean(UserAccountFilter userAccountFilter) throws ServiceException{
		List<UserAccount> userAccountList = this.searchUsersByFilterServiceBean(userAccountFilter);
		
		return userAccountList;
	}
	
	
	/**
	 * Search users by filter service bean.
	 *
	 * @param userAccountFilter the user account filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<UserAccount> searchUsersByFilterServiceBean(UserAccountFilter userAccountFilter) throws ServiceException{
		CriteriaBuilder cb=em.getCriteriaBuilder();
		CriteriaQuery<UserAccount> cq=cb.createQuery(UserAccount.class);
		Root<UserAccount> userAccountRoot=cq.from(UserAccount.class);
		userAccountRoot.fetch("institutionsSecurity");
		cq.orderBy(cb.asc(userAccountRoot.get("loginUser")));
		List<Predicate> criteriaParameters=new ArrayList<Predicate>();
		
		cq.select(userAccountRoot);

		/**
		 * @author mmacalupu
		 * This filter is used when we want to show the users and his schedule's exception
		 */
		if(Validations.validateIsNotNull(userAccountFilter.getScheduleExceptionFilter())){
			userAccountRoot.fetch("scheduleExceptionDetails",JoinType.LEFT);
			cq.distinct(true);
		}
		/**End
		 */
		
		if(Validations.validateIsNotNullAndNotEmpty(userAccountFilter.getLoginUserAccount())){
			criteriaParameters.add(cb.like(userAccountRoot.<String>get("loginUser"),userAccountFilter.getLoginUserAccount()+"%"));
		}
		if(Validations.validateIsNotNullAndNotEmpty(userAccountFilter.getUserName())){
			criteriaParameters.add(cb.like(userAccountRoot.<String>get("fullName"),userAccountFilter.getUserName()+"%"));
		}
		if(Validations.validateIsNotNullAndNotEmpty(userAccountFilter.getFirtsLastName())){
			criteriaParameters.add(cb.like(userAccountRoot.<String>get("firtsLastName"),userAccountFilter.getFirtsLastName()+"%"));
		}
		if(Validations.validateIsNotNullAndNotEmpty(userAccountFilter.getSecondLastName())){
			criteriaParameters.add(cb.like(userAccountRoot.<String>get("secondLastName"),userAccountFilter.getSecondLastName()+"%"));
		}
		if(Validations.validateIsNotNullAndNotEmpty(userAccountFilter.getUserAccountType())
				&& userAccountFilter.getUserAccountType().intValue()>0){
			criteriaParameters.add(cb.equal(userAccountRoot.get("type"),userAccountFilter.getUserAccountType()));
		}
		if(Validations.validateIsNotNullAndNotEmpty(userAccountFilter.getInstitutionType())
				&& userAccountFilter.getInstitutionType().intValue()>0){
			criteriaParameters.add(cb.equal(userAccountRoot.get("institutionTypeFk"),userAccountFilter.getInstitutionType()));
		}
		if(Validations.validateIsNotNullAndNotEmpty(userAccountFilter.getIdInstitutionSecurity())
				&& userAccountFilter.getIdInstitutionSecurity().intValue()>0){
			criteriaParameters.add(cb.equal(userAccountRoot.get("institutionsSecurity").get("idInstitutionSecurityPk"),userAccountFilter.getIdInstitutionSecurity()));
		}
		if(Validations.validateIsNotNullAndNotEmpty( userAccountFilter.getUserAccountState() )
				&& userAccountFilter.getUserAccountState().intValue()>0){
			criteriaParameters.add(cb.equal(userAccountRoot.get("state"),userAccountFilter.getUserAccountState()));
		}
		
		if(!criteriaParameters.isEmpty()){
			if (criteriaParameters.size() == 1) {
		        cq.where(criteriaParameters.get(0));
		    } else {
		        cq.where(cb.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()])));
		    }
		}
		
		TypedQuery<UserAccount> userAccountCriteria= em.createQuery(cq);
		userAccountCriteria.setHint("org.hibernate.cacheable", true);
		
		return (List<UserAccount>) userAccountCriteria.getResultList();
	}
	
	
	/**
	 * Search users by filter for schedule exception.
	 *
	 * @param userAccountFilter the user account filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<UserAccount> searchUsersByFilterForScheduleException(UserAccountFilter userAccountFilter) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append("Select U from UserAccount U ");
		sbQuery.append("left join fetch U.institutionsSecurity I ");
		sbQuery.append("left join fetch U.scheduleExceptionDetails SE ");
		sbQuery.append("where ");		
		sbQuery.append("U.state = :statePrm ");	
		
		if(Validations.validateIsNotNullAndNotEmpty(userAccountFilter.getUserAccountType())
				&& !userAccountFilter.getUserAccountType().equals(Integer.valueOf(0))){
			sbQuery.append("and U.type = :typePrm ");
		}	
		if(Validations.validateIsNotNullAndNotEmpty(userAccountFilter.getInstitutionType())
				&& !userAccountFilter.getInstitutionType().equals(Integer.valueOf(0))){
			sbQuery.append("and U.institutionTypeFk = :institutionTypeFkPrm ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(userAccountFilter.getIdInstitutionSecurity())
				&& !userAccountFilter.getIdInstitutionSecurity().equals(Integer.valueOf(0))){
			sbQuery.append("and U.institutionsSecurity.idInstitutionSecurityPk = :idInstitutionSecurityPkPrm ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(userAccountFilter.getLoginUserAccount())){
			sbQuery.append("and U.loginUser like :loginUserPrm ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(userAccountFilter.getUserName())){
			sbQuery.append("and U.fullName like :fullNamePrm ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(userAccountFilter.getFirtsLastName())){
			sbQuery.append("and U.firtsLastName like :firtsLastNamePrm ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(userAccountFilter.getSecondLastName())){
			sbQuery.append("and U.secondLastName like :secondLastNamePrm ");
		}
		sbQuery.append("order by U.loginUser");
		
		Query query=em.createQuery(sbQuery.toString());
		query.setParameter("statePrm", userAccountFilter.getUserAccountState());
		
		if(Validations.validateIsNotNullAndNotEmpty(userAccountFilter.getUserAccountType())
				&& !userAccountFilter.getUserAccountType().equals(Integer.valueOf(0))){
			query.setParameter("typePrm", userAccountFilter.getUserAccountType());
		}	
		if(Validations.validateIsNotNullAndNotEmpty(userAccountFilter.getInstitutionType())
				&& !userAccountFilter.getInstitutionType().equals(Integer.valueOf(0))){
			query.setParameter("institutionTypeFkPrm", userAccountFilter.getInstitutionType());
		}
		if(Validations.validateIsNotNullAndNotEmpty(userAccountFilter.getIdInstitutionSecurity())
				&& !userAccountFilter.getIdInstitutionSecurity().equals(Integer.valueOf(0))){
			query.setParameter("idInstitutionSecurityPkPrm", userAccountFilter.getIdInstitutionSecurity());
		}
		if(Validations.validateIsNotNullAndNotEmpty(userAccountFilter.getLoginUserAccount())){
			query.setParameter("loginUserPrm", userAccountFilter.getLoginUserAccount()+"%");
		}
		if(Validations.validateIsNotNullAndNotEmpty(userAccountFilter.getUserName())){
			query.setParameter("fullNamePrm", userAccountFilter.getUserName());
		}
		if(Validations.validateIsNotNullAndNotEmpty(userAccountFilter.getFirtsLastName())){
			query.setParameter("firtsLastNamePrm", userAccountFilter.getFirtsLastName());
		}
		if(Validations.validateIsNotNullAndNotEmpty(userAccountFilter.getSecondLastName())){
			query.setParameter("secondLastNamePrm", userAccountFilter.getSecondLastName());
		}
		
		query.setHint("org.hibernate.cacheable", true);
		return  (List<UserAccount>)query.getResultList();
	}
	
	/**
	 * Update user account state service bean.
	 *
	 * @param userAccount the user account
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void updateUserAccountStateServiceBean(UserAccount userAccount) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update UserAccount U ");	
		sbQuery.append(" Set U.state = :statePrm,");
		sbQuery.append(" U.situation = :situationPrm,");
		sbQuery.append(" U.changePassword = :changePasswordPrm,");
		sbQuery.append(" U.lastModifyDate = :lastModifyDatePrm,");
		sbQuery.append(" U.lastModifyUser = :lastModifyUserPrm,");
		sbQuery.append(" U.lastModifyIp = :lastModifyIpPrm,");
		sbQuery.append(" U.lastModifyPriv = :lastModifyPrivPrm,");
		sbQuery.append(" U.blockMotive = :blockMotivePrm, ");
		sbQuery.append(" U.blockOtherMotive = :blockOtherMotivePrm, ");
		sbQuery.append(" U.unblockMotive = :unblockMotivePrm, ");
		sbQuery.append(" U.unblockOtherMotive = :unblockOtherMotivePrm, ");
		sbQuery.append(" U.annularMotive = :annularMotivePrm, ");
		sbQuery.append(" U.annularOtherMotive = :annularOtherMotivePrm, ");
		sbQuery.append(" U.rejectMotive = :rejectMotivePrm, ");
		sbQuery.append(" U.rejectOtherMotive = :rejectOtherMotivePrm,");
		sbQuery.append(" U.confirmationUser = :confirmationUserPrm,");
		sbQuery.append(" U.confirmationDate = :confirmationDatePrm, ");
		sbQuery.append(" U.documentFile = :documentFilePrm, ");
		sbQuery.append(" U.unregisterFile = :unregisterFilePrm ");
		sbQuery.append(" Where U.idUserPk = :idUserPkPrm ");
		
		Query query = em.createQuery(sbQuery.toString());
					
		query.setParameter("idUserPkPrm",userAccount.getIdUserPk());
		query.setParameter("statePrm",userAccount.getState());
		query.setParameter("situationPrm",userAccount.getSituation());
		query.setParameter("changePasswordPrm", userAccount.getChangePassword());
		query.setParameter("lastModifyDatePrm", userAccount.getLastModifyDate());
		query.setParameter("lastModifyUserPrm", userAccount.getLastModifyUser());
		query.setParameter("lastModifyIpPrm", userAccount.getLastModifyIp());
		query.setParameter("lastModifyPrivPrm", userAccount.getLastModifyPriv());
		query.setParameter("blockMotivePrm", userAccount.getBlockMotive());
		query.setParameter("blockOtherMotivePrm", userAccount.getBlockOtherMotive());
		query.setParameter("unblockMotivePrm", userAccount.getUnblockMotive());
		query.setParameter("unblockOtherMotivePrm", userAccount.getUnblockOtherMotive());
		query.setParameter("annularMotivePrm", userAccount.getAnnularMotive());
		query.setParameter("annularOtherMotivePrm", userAccount.getAnnularOtherMotive());
		query.setParameter("rejectMotivePrm", userAccount.getRejectMotive());
		query.setParameter("rejectOtherMotivePrm", userAccount.getRejectOtherMotive());
		query.setParameter("confirmationUserPrm", userAccount.getConfirmationUser());
		query.setParameter("confirmationDatePrm", userAccount.getConfirmationDate());
		query.setParameter("documentFilePrm", userAccount.getDocumentFile());
		query.setParameter("unregisterFilePrm", userAccount.getUnregisterFile());
		
		query.executeUpdate();
	}
									   
	/**
	 * Search institution type by user service bean.
	 *
	 * @param idUserPk the id user pk
	 * @param state the state
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<InstitutionTypeDetail> searchInstitutionTypeByUserServiceBean(int idUserPk, int state) throws ServiceException{
		Map<String,Object> parameters=new HashMap<String, Object>();
		parameters.put("idUserPkPrm", idUserPk);
		parameters.put("statePrm", state);					  	
		return (List<InstitutionTypeDetail>)findWithNamedQuery(InstitutionTypeDetail.SEARCH_BY_USER, parameters);
	}
							
	/**
	 * Search registered profiles by user service bean.
	 *
	 * @param userProfile the user profile
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<UserProfile> searchRegisteredProfilesByUserServiceBean(UserProfile userProfile) throws ServiceException{
		String strQuery="Select PU from UserProfile PU where PU.userAccount.idUserPk=:idUserPkPrm and PU.state=:statePrm";
		Query query=em.createQuery(strQuery);
		query.setParameter("idUserPkPrm", userProfile.getUserAccount().getIdUserPk());
		query.setParameter("statePrm", userProfile.getState());
		query.setHint("org.hibernate.cacheable", true);
		List<UserProfile> listUserProfiles=(List<UserProfile>) query.getResultList();
		for (UserProfile usrProfile : listUserProfiles) {
		SystemProfile systemProfile=usrProfile.getSystemProfile();
		systemProfile.getIdSystemProfilePk();
		}
		return listUserProfiles;
	}
	
	/**
	 * Search registerd profiles allowed user.
	 *
	 * @param userProfile the user profile
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<UserProfileAllowed> searchRegisteredProfilesAllowedUser(UserProfile userProfile) throws ServiceException{
		String strQuery="Select PU from UserProfileAllowed PU where PU.userAccount.idUserPk=:idUserPkPrm and PU.profileState=:statePrm";
		TypedQuery<UserProfileAllowed> query=em.createQuery(strQuery,UserProfileAllowed.class);
		query.setParameter("idUserPkPrm", userProfile.getUserAccount().getIdUserPk());
		query.setParameter("statePrm", userProfile.getState());
		query.setHint("org.hibernate.cacheable", true);
		List<UserProfileAllowed> listUserProfiles=query.getResultList();
		return listUserProfiles;
	}
	
	/**
	 * Validate user email.
	 *
	 * @param userName the user name
	 * @param email the email
	 * @return the integer
	 * @throws ServiceException the service exception
	 */
	public UserAccount validateUserEmail(String userName,String email) throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("Select U.idUserPk,U.countRequestPassword,U.lastModifyPriv,U.lastModifyIp From UserAccount U where U.loginUser = :login ");
			sbQuery.append(" and U.idEmail = :email and U.state = :state and U.type = :userType ");
			TypedQuery<Object[]> query = em.createQuery(sbQuery.toString(), Object[].class);
			query.setParameter("login", userName);
			query.setParameter("email", email);
			query.setParameter("state", UserAccountStateType.CONFIRMED.getCode());
			query.setParameter("userType", UserAccountType.EXTERNAL.getCode());
			Object[] row = query.getSingleResult();
			UserAccount user = new UserAccount();
			user.setIdUserPk((Integer)row[0]);
			user.setCountRequestPassword(row[1]!=null?(Integer)row[1]:0);
			user.setLastModifyIp(row[3]!=null?row[3].toString():"");
			user.setLastModifyPriv(row[2]!=null?(Integer)row[2]:0);
			return user;
		}catch(NoResultException ex){
			return null;
		} catch(NonUniqueResultException ex){
			return null;
		}
	}
	
	/**
	 * Validate user service bean.
	 *
	 * @param userAccountFilter the user account filter
	 * @return the user account
	 * @throws ServiceException the service exception
	 */
	public UserAccount validateUserServiceBean(UserAccountFilter userAccountFilter) throws ServiceException{
		
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("Select U From UserAccount U left join fetch U.institutionsSecurity I ");
			sbQuery.append(" Where U.loginUser = :loginUserPrm ");
		//	if(userAccountFilter.getUserAccountState() != null && userAccountFilter.getUserAccountState() > 0){
			sbQuery.append(" and U.state != :userStatePrm ");
		//	}
			Query query=em.createQuery(sbQuery.toString());			
			query.setParameter("loginUserPrm", userAccountFilter.getLoginUserAccount());
		//	if(userAccountFilter.getUserAccountState() != null && userAccountFilter.getUserAccountState() > 0){
			query.setParameter("userStatePrm", UserAccountStateType.REJECTED.getCode());
		//	}
			query.setHint("org.hibernate.cacheable", false);
			UserAccount account = (UserAccount) query.getSingleResult();
			account.getInstitutionsSecurity().toString();
			return account;
		} catch(NoResultException ex){
			return null;
		} catch(NonUniqueResultException ex){
			return null;
		}
	}
	
	/**
	 * Gets the user account.
	 *
	 * @param userAccount the user account
	 * @return the user account
	 * @throws ServiceException the service exception
	 */
	public UserAccount getUserAccount(UserAccount userAccount) throws ServiceException{
		UserAccount account = null;
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("Select U From UserAccount U ");
			sbQuery.append(" Where U.loginUser = :loginUserPrm ");
			sbQuery.append(" and U.state != :state ");
			
			Query query=em.createQuery(sbQuery.toString());			
			query.setParameter("loginUserPrm", userAccount.getLoginUser());
			query.setParameter("state", UserAccountStateType.REJECTED.getCode());
			query.setHint("org.hibernate.cacheable", true);
			account = (UserAccount) query.getSingleResult();	
		} catch(NoResultException ex){
			return null;
		} catch(NonUniqueResultException ex){
			return null;
		}
		
		return account;
	}
	
	/**
	 * Update change password service bean.
	 *
	 * @param usuario the usuario
	 * @throws ServiceException the service exception
	 */
	public void updateChangePasswordServiceBean(UserAccount usuario) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update UserAccount U ");	
		sbQuery.append(" Set U.changePassword = :changePasswordPrm,");
		sbQuery.append(" U.lastModifyDate = :lastModifyDatePrm,");
		sbQuery.append(" U.lastModifyUser = :lastModifyUserPrm,");
		sbQuery.append(" U.lastModifyIp = :lastModifyIpPrm,");
		sbQuery.append(" U.lastModifyPriv = :lastModifyPrivPrm ");
		sbQuery.append(" Where U.idUserPk = :idUserPkPrm ");
		
		Query query = em.createQuery(sbQuery.toString());
					
		query.setParameter("idUserPkPrm",usuario.getIdUserPk());
		query.setParameter("changePasswordPrm",usuario.getChangePassword());
		query.setParameter("lastModifyDatePrm", usuario.getLastModifyDate());
		query.setParameter("lastModifyUserPrm", usuario.getLastModifyUser());
		query.setParameter("lastModifyIpPrm", usuario.getLastModifyIp());
		query.setParameter("lastModifyPrivPrm", usuario.getLastModifyPriv());
		
	}
	
	public void resetMfaServiceBean(UserAccount usuario) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update UserAccount U ");	
		sbQuery.append(" Set U.mfaSecret = null,");
		sbQuery.append(" U.lastModifyDate = :lastModifyDatePrm,");
		sbQuery.append(" U.lastModifyUser = :lastModifyUserPrm,");
		sbQuery.append(" U.lastModifyIp = :lastModifyIpPrm,");
		sbQuery.append(" U.lastModifyPriv = :lastModifyPrivPrm ");
		sbQuery.append(" Where U.idUserPk = :idUserPkPrm ");
		
		Query query = em.createQuery(sbQuery.toString());
					
		query.setParameter("idUserPkPrm",usuario.getIdUserPk());
		query.setParameter("lastModifyDatePrm", usuario.getLastModifyDate());
		query.setParameter("lastModifyUserPrm", usuario.getLastModifyUser());
		query.setParameter("lastModifyIpPrm", usuario.getLastModifyIp());
		query.setParameter("lastModifyPrivPrm", usuario.getLastModifyPriv());		

		query.executeUpdate();
	}
	
	/**
	 * List availiable privileges by user service bean.
	 *
	 * @param userAccountFilter the user account filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@Performance
	public List<OptionPrivilege> listAvailiablePrivilegesByUserServiceBean(UserAccountFilter userAccountFilter) throws ServiceException{
		TypedQuery<Object[]> query = em.createNamedQuery(OptionPrivilege.OPTION_PRIVILEGE_LOGIN,Object[].class);
		query.setParameter("userStatePrm", UserAccountStateType.CONFIRMED.getCode());		
		query.setParameter("idUserPrm", userAccountFilter.getIdUserAccountPK());
		query.setParameter("userProfileStatePrm", UserProfileStateType.REGISTERED.getCode());
		query.setParameter("systemProfilePrm", SystemProfileStateType.CONFIRMED.getCode());
		query.setParameter("profilePrivilegeStatePrm", ProfilePrivilegeStateType.REGISTERED.getCode());
		query.setParameter("optionPrivilegeStatePrm", OptionPrivilegeStateType.REGISTERED.getCode());
		query.setParameter("systemOptionStatePrm", OptionStateType.CONFIRMED.getCode());
		query.setParameter("systemStatePrm", SystemStateType.CONFIRMED.getCode());
		query.setParameter("idSystemPrm", userAccountFilter.getIdSystemPk());
		List<Object[]> rows = query.getResultList();
		List<OptionPrivilege> optionPrivileges = new ArrayList<>();
		for(Object[] row: rows){
			OptionPrivilege privilege = new OptionPrivilege();
			SystemOption option = new SystemOption();
			privilege.setIdOptionPrivilegePk((Integer)row[0]);
			privilege.setIdPrivilege((Integer)row[1]);
			privilege.setState((Integer)row[2]);
			option.setIdSystemOptionPk((Integer)row[3]);
			option.setUrlOption(row[4].toString());
			option.setOptionType((Integer)row[5]);
			option.setOrderOption((Integer)row[6]);
			privilege.setSystemOption(option);
			optionPrivileges.add(privilege);
		}
		return optionPrivileges;
	}
	
	/**
	 * Validate doc number service bean.
	 *
	 * @param userAccount the user account
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int validateDocNumberServiceBean(UserAccount userAccount) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append("Select Count(u) from UserAccount u where ");
		sbQuery.append(" u.documentNumber=:documentNumber ");
		sbQuery.append(" and documentType = :documentType ");
		sbQuery.append(" and state not in :stateType ");
		Query query=em.createQuery(sbQuery.toString());
		query.setParameter("documentNumber", userAccount.getDocumentNumber());
		query.setParameter("documentType", userAccount.getDocumentType());
		query.setParameter("stateType", UserAccountStateType.REJECTED.getCode());
		query.setHint("org.hibernate.cacheable", true);
		return Integer.parseInt( query.getSingleResult().toString() );
	}
	
	/**
	 * Gets the institutions name.
	 *
	 * @param instSecurityPk the inst security pk
	 * @return the institutions name
	 * @throws ServiceException the service exception
	 */
	public String getInstitutionsName(Integer instSecurityPk)throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append("Select i.institutionName from InstitutionsSecurity i where i.idInstitutionSecurityPk=:idInstitutionSecurityPk ");
		Query query=em.createQuery(sbQuery.toString());
		query.setParameter("idInstitutionSecurityPk", instSecurityPk);
		query.setHint("org.hibernate.cacheable", true);
		return query.getSingleResult().toString();
	}

	/**
	 * To Check whether the selected user is in session or not.
	 *
	 * @param selectedUserAccountBean the selected user account bean
	 * @return Integer count value
	 * @throws ServiceException the service exception
	 */
	public Integer  isSelectedUserInSessionServiceFacade(
			UserAccount selectedUserAccountBean) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select count(us) from UserSession us where us.userAccount.idUserPk = :idUserpk and us.finalDate is null ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idUserpk", selectedUserAccountBean.getIdUserPk());
		query.setHint("org.hibernate.cacheable", true);
		return Integer.parseInt( query.getSingleResult().toString() );
		
	}
	
	/**
	 * Gets the holidays.
	 *
	 * @return the holidays
	 */
	public List<Holiday> getHolidays(){
		Map<String, Object> parameters  = new HashMap<String, Object>();
		parameters.put("state", Integer.valueOf(1));
		return findWithNamedQuery(Holiday.HOLIDAY_SEARCH_BY_STATE,parameters);
	}
	
	/**
	 * Gets the users with name service bean.
	 *
	 * @param loginUser the login user
	 * @return userlist
	 * @throws ServiceException the service exception
	 */
	public List<String> getUsersWithNameServiceBean(String loginUser)
			throws ServiceException {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("Select ua.loginUser from UserAccount ua where ua.state != :state");
			sbQuery.append(" and ua.loginUser like :loginUser");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("state", UserAccountStateType.REJECTED.getCode());
			query.setParameter("loginUser", loginUser+"%");
			query.setHint("org.hibernate.cacheable", true);
			return (List<String>) query.getResultList();
		} catch (NoResultException ex) {
			return null;
		}
	}
	
	/**
	 * get UserDetails based on the userID.
	 *
	 * @param loginUser the login user
	 * @return the user account details service bean
	 * @throws ServiceException the service exception
	 */
	public List<UserAccount> getUserAccountDetailsServiceBean(String loginUser) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append("Select u from UserAccount u where u.loginUser=:loginUserPrm");
		
		Query query=em.createQuery(sbQuery.toString());
		query.setParameter("loginUserPrm", loginUser);
		query.setHint("org.hibernate.cacheable", true);
		return  (List<UserAccount>)query.getResultList() ;
	}
	
	/**
	 * Gets the user document unregister.
	 *
	 * @param idUserPk the id user pk
	 * @return the user document unregister
	 */
	public byte[] getUserDocumentUnregister(Integer idUserPk){
		String strQuery = "select t.unregisterFile from UserAccount t where t.idUserPk=" + idUserPk;
		Query query = em.createQuery(strQuery);
		query.setHint("org.hibernate.cacheable", true);
		return (byte[])query.getSingleResult();
	}
	
	/**
	 * Gets the user document register.
	 *
	 * @param idUserPk the id user pk
	 * @return the user document register
	 */
	public byte[] getUserDocumentRegister(Integer idUserPk){
		String strQuery = "select t.documentFile from UserAccount t where t.idUserPk=" + idUserPk;
		Query query = em.createQuery(strQuery);
		query.setHint("org.hibernate.cacheable", true);
		return (byte[])query.getSingleResult();
	}

	/**
	 * Gets the modify date and state.
	 *
	 * @param tableName the table name
	 * @param columnName the column name
	 * @param idPk the id pk
	 * @return Object[]
	 */
	public Object[] getModifyDateAndState(String tableName, String columnName,Integer idPk) {
		String strQuery = "select t.lastModifyDate,t.state from "+tableName+" t where t."+columnName+"="+idPk;
		Query query = em.createQuery(strQuery);
		query.setHint("org.hibernate.cacheable", true);
		return (Object[])query.getSingleResult();
	}
	
	/**
	 * Used to get the User current status.
	 *
	 * @param loginUser the login user
	 * @return UserSession
	 * @throws ServiceException the service exception
	 */
	public UserSession getUserCurrentStatus(String loginUser)throws ServiceException{
		try{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select max(us) from UserSession us,UserAccount ua ");
		sbQuery.append("where ua.loginUser = :loginUser and ua.idUserPk = us.userAccount.idUserPk ");
		sbQuery.append("and us.state = :state");
		Query query=em.createQuery(sbQuery.toString());
		query.setParameter("loginUser", loginUser);
		query.setParameter("state",UserSessionStateType.CONNECTED.getCode());
		return (UserSession)query.getSingleResult();
		} catch (NoResultException ex) {
			return null;
		}
	}
	
	public List<UserSession> allRemoteConnectedByUser(String loginUser){
		try{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select us from UserSession us ");
		sbQuery.append(" join fetch us.userAccount ua ");
		sbQuery.append("where ua.loginUser = :loginUser and ua.idUserPk = us.userAccount.idUserPk ");
		sbQuery.append("and us.state = :state");
		Query query=em.createQuery(sbQuery.toString());
		query.setParameter("loginUser", loginUser);
		query.setParameter("state",UserSessionStateType.CONNECTED.getCode());
		return query.getResultList();
		} catch (Exception e) {
			return new ArrayList<>();
		}
	}
	
	/**
	 * Gets the user profile.
	 *
	 * @param system the system
	 * @param userAccount the user account
	 * @return the user profile
	 */
	public Integer getUserProfile(Integer system,Integer userAccount){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select up.systemProfile.idSystemProfilePk from UserProfile up where up.userAccount.idUserPk = :account ");
		sbQuery.append(" and up.systemProfile.system.idSystemPk= :system ");
		sbQuery.append(" and up.state = :active ");
		TypedQuery<Integer> query = em.createQuery(sbQuery.toString(), Integer.class);
		query.setParameter("account", userAccount);
		query.setParameter("system", system);
		query.setParameter("active", BooleanType.YES.getCode());
		return query.getResultList().get(0);
	}
	
	/**
	 * Gets the user manual.
	 *
	 * @param systemOption the system option
	 * @param systemProfile the system profile
	 * @return the user manual
	 */
	public UserManualProfile getUserManual(Integer systemOption,Integer systemProfile){
		try{
			TypedQuery<UserManualProfile> query = em.createNamedQuery(UserManualProfile.GET_USER_MANUAL, UserManualProfile.class);
			query.setParameter("option", systemOption);
			query.setParameter("profile", systemProfile);
			return query.getSingleResult();
		} catch (NoResultException ex) {
			return null;
		}
	}
	
	/**
	 * Get list User Account by subsidiary.
	 *
	 * @param userAccountFilter the user account filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<UserAccount> getLstUserAccountBySubsidiary(UserAccountFilter userAccountFilter) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();
		List<UserAccount> lstUserAccount = null;
		try {
			sbQuery.append(" Select U.idUserPk, U.loginUser from UserAccount U ");	
			sbQuery.append(" where U.institutionsSecurity.idInstitutionSecurityPk = :parInstitutionSecurityPk ");
			sbQuery.append(" and U.institutionTypeFk = :institutionTypeFk ");	
			if(Validations.validateIsNotNullAndNotEmpty(userAccountFilter.getSubsidiary().getDepartamentCentral())
					&& GeneralConstants.ONE_VALUE_INTEGER.equals(userAccountFilter.getSubsidiary().getDepartamentCentral())){
				sbQuery.append(" and U.subsidiary.departament = :departament ");
			} else {
				sbQuery.append(" and U.subsidiary.idSubsidiaryPk = :parIdSubsidiaryPk ");
			}			
			Query query=em.createQuery(sbQuery.toString());
			query.setParameter("parInstitutionSecurityPk", userAccountFilter.getIdInstitutionSecurity());
			query.setParameter("institutionTypeFk", userAccountFilter.getInstitutionType());
			if(Validations.validateIsNotNullAndNotEmpty(userAccountFilter.getSubsidiary().getDepartamentCentral())
					&& GeneralConstants.ONE_VALUE_INTEGER.equals(userAccountFilter.getSubsidiary().getDepartamentCentral())){
				query.setParameter("departament", userAccountFilter.getSubsidiary().getDepartament());
			} else {
				query.setParameter("parIdSubsidiaryPk", userAccountFilter.getSubsidiary().getIdSubsidiaryPk());
			}				
			query.setHint("org.hibernate.cacheable", true);
			
			List<Object> objectList = query.getResultList();
			lstUserAccount = new ArrayList<UserAccount>();			
			for(int i=0; i<objectList.size(); ++i){
			    Object[] sResults = (Object[]) objectList.get(i);
			    UserAccount user = new UserAccount();
			    user.setIdUserPk(sResults[0]==null?0:Integer.parseInt(sResults[0].toString()));
			    user.setLoginUser(sResults[1]==null?"":sResults[1].toString());			    
			    lstUserAccount.add(user);
			}
			return  lstUserAccount;
		} catch (NoResultException e) {
			return null;
		}				
	}
	
	/**
	 * Metodo que obtiene el perfil de un usuario segun el mnemonico del sistema y el nombre de usuario
	 *
	 * @param userAccount the user account
	 * @return the user account
	 * @throws ServiceException the service exception
	 */
	
	
	public SystemProfile getSystemProfile (String mnemonicoSistema, String loginUsuario) throws ServiceException{
		SystemProfile systemProfile = null;
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" select sp ");
			sbQuery.append(" from UserAccount ua ");
			sbQuery.append(" join ua.userProfiles up ");
			sbQuery.append(" join up.systemProfile sp ");
			sbQuery.append(" join sp.system s ");
			sbQuery.append(" where s.state = :systemState ");
			sbQuery.append(" and trim(upper(ua.loginUser)) like trim(upper(:loginUser)) ");
			sbQuery.append(" and up.state = :userProfileStatePrm ");
			sbQuery.append(" and sp.state = :systemProfilePrm ");
			sbQuery.append(" and trim(upper(s.mnemonic)) like trim(upper(:mnemonic)) ");
			
			Query query=em.createQuery(sbQuery.toString());			
			query.setParameter("systemState",SystemStateType.CONFIRMED.getCode());
			query.setParameter("loginUser",loginUsuario);
			query.setParameter("userProfileStatePrm",UserProfileStateType.REGISTERED.getCode());
			query.setParameter("systemProfilePrm",SystemProfileStateType.CONFIRMED.getCode());
			query.setParameter("mnemonic",mnemonicoSistema);
			
			systemProfile =  (SystemProfile) query.getSingleResult();	
		} catch(NoResultException ex){
			return null;
		} catch(NonUniqueResultException ex){
			return null;
		}
		
		return systemProfile;
	}
	
	/**
	 * Lista de opciones del sistema al que el usuario tiene acceso
	 *
	 * @param userAccountFilter the user account filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<SystemOption> listAvailiablePrivilegesByUserAndSystem(String userLogin, String mnemonicSystem,Integer parentSystemOption) throws ServiceException{
		
			List<SystemOption> listSystemOption;
			StringBuilder sbQuery = new StringBuilder();
			
			sbQuery.append(" select so ");
			sbQuery.append(" from UserAccount ua ");
			sbQuery.append(" join ua.userProfiles up ");
			sbQuery.append(" join up.systemProfile sp ");
			sbQuery.append(" join sp.system s ");
			sbQuery.append(" join s.systemOptions so ");
			sbQuery.append(" join so.parentSystemOption sof ");
			sbQuery.append(" join so.catalogueLanguages ca ");
			
			sbQuery.append(" where s.state = :systemState ");
			sbQuery.append(" and trim(upper(ua.loginUser)) like trim(upper(:loginUser)) ");
			sbQuery.append(" and up.state = :userProfileStatePrm ");
			sbQuery.append(" and sp.state = :systemProfilePrm ");
			sbQuery.append(" and trim(upper(s.mnemonic)) like trim(upper(:mnemonic)) ");
			
			if(parentSystemOption == null)sbQuery.append(" and sof.idSystemOptionPk is null ");
			else sbQuery.append(" and sof.idSystemOptionPk = :parentSystemOption ");
			
			sbQuery.append(" order by so.orderOption asc ");
			
			Query query=em.createQuery(sbQuery.toString());			
			query.setParameter("systemState",SystemStateType.CONFIRMED.getCode());
			query.setParameter("loginUser",userLogin);
			query.setParameter("userProfileStatePrm",UserProfileStateType.REGISTERED.getCode());
			query.setParameter("systemProfilePrm",SystemProfileStateType.CONFIRMED.getCode());
			query.setParameter("mnemonic",mnemonicSystem);
			if(parentSystemOption != null){
				query.setParameter("parentSystemOption",parentSystemOption);
			}
			
		try{	
			listSystemOption = query.getResultList();
			
			return listSystemOption;
			
		} catch(NoResultException ex){
			return null;
		} catch(NonUniqueResultException ex){
			return null;
		}
	}
		
	/**
	 * Metodo que obtiene la lista de usuarios segun el mnemonico del sistema y mnemonico del perfil
	 * 
	 * @param mnemonicoSistema
	 * @param mnemonicoPerfil
	 * @return
	 * @throws ServiceException
	 */
	public List<UserAccount> getUserAccountByProfileAndSystem(String mnemonicoSistema, String mnemonicoPerfil) throws ServiceException{
		SystemProfile systemProfile = null;
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" select ua ");
			sbQuery.append(" from UserProfile up ");
			sbQuery.append(" join up.userAccount ua ");
			sbQuery.append(" join up.systemProfile sp ");
			sbQuery.append(" join sp.system s ");
			sbQuery.append(" where ua.state = :userState ");
			sbQuery.append(" and trim(upper(s.mnemonic)) like trim(upper(:mnemonicoSistema)) ");
			sbQuery.append(" and trim(upper(sp.mnemonic)) like trim(upper(:mnemonicoPerfil)) ");
			sbQuery.append(" and up.state = :userProfileState ");
			sbQuery.append(" and sp.state = :systemProfileState ");
			sbQuery.append(" order by ua.firtsLastName, ua.secondLastName, ua.fullName ");
			
			Query query=em.createQuery(sbQuery.toString());
			query.setParameter("userState", UserAccountStateType.CONFIRMED.getCode());
			query.setParameter("mnemonicoSistema",mnemonicoSistema);
			query.setParameter("mnemonicoPerfil",mnemonicoPerfil);
			query.setParameter("userProfileState",UserProfileStateType.REGISTERED.getCode());
			query.setParameter("systemProfileState",SystemProfileStateType.CONFIRMED.getCode());
			
			List<UserAccount> listUserAccount =  query.getResultList();
			
			return listUserAccount;
			
		} catch(NoResultException ex){
			return null;
		} catch(NonUniqueResultException ex){
			return null;
		}
	}
	

	/**
	 * Update user account state service bean.
	 *
	 * @param userAccount the user account
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void updateUserAccountForMFASecret(UserAccount userAccount) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update UserAccount ");	
		sbQuery.append(" Set mfaSecret = :mfaSecretPrm,");
		sbQuery.append(" lastModifyDate = :lastModifyDatePrm,");
		sbQuery.append(" lastModifyUser = :lastModifyUserPrm,");
		sbQuery.append(" lastModifyIp = :lastModifyIpPrm ");
		sbQuery.append(" Where idUserPk = :idUserPkPrm ");
		
		Query query = em.createQuery(sbQuery.toString());
					
		query.setParameter("idUserPkPrm",userAccount.getIdUserPk());
		query.setParameter("mfaSecretPrm",userAccount.getMfaSecret());
		query.setParameter("lastModifyDatePrm", userAccount.getLastModifyDate());
		query.setParameter("lastModifyUserPrm", userAccount.getLastModifyUser());
		query.setParameter("lastModifyIpPrm", userAccount.getLastModifyIp());		
		query.executeUpdate();
	}
	
}
