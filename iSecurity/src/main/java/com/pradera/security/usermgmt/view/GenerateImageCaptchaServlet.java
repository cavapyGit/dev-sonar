package com.pradera.security.usermgmt.view;

import java.io.IOException;
import java.io.Serializable;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.captcha.Captcha;
import nl.captcha.backgrounds.GradiatedBackgroundProducer;
import nl.captcha.servlet.CaptchaServletUtil;
import nl.captcha.text.producer.DefaultTextProducer;

import org.apache.commons.lang3.StringUtils;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class GenerateImageCaptchaServlet.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 28/10/2014
 */
@WebServlet(name="/GenerateImageCaptchaServlet",urlPatterns={"/captcha.png"})
public class GenerateImageCaptchaServlet extends HttpServlet implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	private static final char[] DEFAULT_CHARS = new char[] { 'a', 'b', 'c', 'd',
		            'e', 'f', 'g', 'h', 'k', 'm', 'n', 'p', 'r', 'w', 'x', 'y',
		            '2', '3', '4', '5', '6', '7', '8', };
       
    /**
     * Instantiates a new generate image captcha servlet.
     *
     * @see HttpServlet#HttpServlet()
     */
    public GenerateImageCaptchaServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * Do get.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String reload = request.getParameter("reload");
		HttpSession session = request.getSession();
		Captcha captcha=(Captcha)session.getAttribute("CAPTCHA_IDEPOSITARY");
		if(captcha== null || StringUtils.isNotBlank(reload)){
			captcha = new Captcha.Builder(190, 40)
			.addBackground(new GradiatedBackgroundProducer())
			.addText(new DefaultTextProducer(6,DEFAULT_CHARS))
//			.gimp()
//			.gimp(new DropShadowGimpyRenderer())
//			Lines in captcha
			.addNoise()
			.addNoise()
			.addBorder()
			.build();
			session.setAttribute("CAPTCHA_IDEPOSITARY", captcha);
			session.setAttribute("RELOAD", "");
			CaptchaServletUtil.writeImage(response, captcha.getImage());
			return;
		}
		CaptchaServletUtil.writeImage(response, captcha.getImage());
	}

	/**
	 * Do post.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
