package com.pradera.security.usermgmt.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.NodeUnselectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.configuration.type.StageApplication;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.httpevent.publish.BrowserWindow;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.security.model.type.UserAccountStateType;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.usersession.UserAcctions;
import com.pradera.security.model.HistoricalState;
import com.pradera.security.model.InstitutionTypeDetail;
import com.pradera.security.model.InstitutionsSecurity;
import com.pradera.security.model.Parameter;
import com.pradera.security.model.Subsidiary;
import com.pradera.security.model.System;
import com.pradera.security.model.SystemProfile;
import com.pradera.security.model.UserAccount;
import com.pradera.security.model.UserProfile;
import com.pradera.security.model.UserProfileAllowed;
import com.pradera.security.model.type.DefinitionType;
import com.pradera.security.model.type.InstitutionTypeDetailStateType;
import com.pradera.security.model.type.OtherMotiveType;
import com.pradera.security.model.type.ParameterTableType;
import com.pradera.security.model.type.SystemProfileStateType;
import com.pradera.security.model.type.SystemStateType;
import com.pradera.security.model.type.UserAccountDocumentType;
import com.pradera.security.model.type.UserAccountSituationType;
import com.pradera.security.model.type.UserAccountType;
import com.pradera.security.model.type.UserProfileStateType;
import com.pradera.security.subsidiary.facade.SubsidiaryServiceFacade;
import com.pradera.security.subsidiary.view.filters.SubsidiaryFilter;
import com.pradera.security.systemmgmt.view.NodeBean;
import com.pradera.security.systemmgmt.view.filters.InstitutionSecurityFilter;
import com.pradera.security.systemmgmt.view.filters.SystemProfileFilter;
import com.pradera.security.usermgmt.view.filters.UserAccountFilter;
import com.pradera.security.utils.view.GenericBaseBean;
import com.pradera.security.utils.view.LDAPUtilities;
import com.pradera.security.utils.view.LDAPUtility;
import com.pradera.security.utils.view.PropertiesConstants;
import com.pradera.security.utils.view.SecurityUtilities;
import com.pradera.security.webservices.UserAccountResource;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class UserBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/02/2013
 */
@DepositaryWebBean
@LoggerCreateBean
public class UserBean extends GenericBaseBean implements Serializable{


	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The log. */
    Logger log = Logger.getLogger(UserBean.class.getName());
    
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;
	
	/** The user account resource. */
	@Inject
	UserAccountResource userAccountResource;
	
	@Inject
	StageApplication stageApplication;
	
	/** The san configuration. */
	@Inject
	@Configurable("app.security.behavior.san.enabled")
	private boolean sanConfiguration;
	
	/** The user account bean. */
	private UserAccount userAccountBean;
	
	/** The selected user account bean. */
	private UserAccount selectedUserAccountBean;

	/** The historical state. */
	private HistoricalState historicalState;
	
	/** The user account filter. */
	private UserAccountFilter userAccountFilter;
	
	/** The user data model. */
	private UserDataModel userDataModel;
	
	/** The lst institution type. */
	private List<InstitutionType> lstInstitutionType;
	
	/** The lst profiles assigned to user. */
	private List<UserProfile> lstProfilesAssignedToUser;
	
	/** The profiles allowed user. */
	private List<UserProfileAllowed> profilesAllowedUser;
	
	/** The list institution type detail assigned to user. */
	private List<InstitutionTypeDetail> listInstitutionTypeDetailAssignedToUser;
	
	/** The lst show institution type assigned to user. */
	private List<String> lstShowInstitutionTypeAssignedToUser;
	
	/** The lst user document type. */
	private List<UserAccountDocumentType> lstUserDocumentType;
	
	/** The lst institutions type selected. */
	private List<String> lstInstitutionsTypeSelected;
	
	/** The lst institutions by institutions type. */
	private List<InstitutionsSecurity> lstInstitutionsByInstitutionType;
	
	/** The lst institution type for filter. */
	private List<Parameter> lstInstitutionTypeForFilter;
	
	/** The lst institution type for filter. */
	private List<Parameter> lstInstitutionTypeForFilterTemp;
	
	/** The lst institutions security for filter. */
	private List<InstitutionsSecurity> lstInstitutionsSecurityForFilter;
	
	/** The tree profiles root. */
	private TreeNode treeProfilesRoot;

	/** The nodes system profiles selected. */
	private TreeNode[] nodesSystemProfilesSelected;
	
	
	/** The tree externs profiles. */
	private TreeNode treeExternsProfiles;
	
	/** The nodes externs profiles selected. */
	private TreeNode[] nodesExternsProfilesSelected;
	
	/** The show enableds profiles. */
	private boolean showProfilesExtern;
	
	/** The show permitted profiles. */
	private boolean showProfilesAllowed;
	
	/** The show other motive. */
	private boolean showOtherMotive;
	
	/** User account state type. */
	private UserAccountStateType userAccountConfirmedStateType=UserAccountStateType.CONFIRMED;
	
	/** The valid user code. */
	private int validUserCode;
	
	/** The numeric type document. */
	private boolean numericTypeDocument;
	
	/** The max lenght document number. */
	private Integer maxLenghtDocumentNumber;
	/** The user document type. */
	private Integer documentType;
	/** The  user document number. */
	private String documentNumber;
	
	/**  The previous UserCode. */
	private String preUserCode;
	
	/**  The Disable institution. */
	private Boolean institution=true;
	
	 /** The user account state. */
 	private List<Parameter> userAccountState; 
	 
	 /** The is reject option. */
 	private boolean isRejectOption;
	 
	 /** The is confirm block or unblock. */
 	private boolean isConfirmBlockOrUnblock;
	 
	 /** The is unregister event. */
 	private boolean isUnregisterEvent;
 	
	/** The ldap utilities. */
	@Inject
	@LDAPUtility
	LDAPUtilities ldapUtilities;

	/** The disable institution selector. */
	private boolean disableInstitutionSelector;
	
	/** The bl is recorder. */
	private boolean blIsRecorder;
	
	/** The bl is committer. */
	private boolean blIsCommitter;
	
	/** The lst system profile. */
	private List<SystemProfile> lstSystemProfile;
	
	/** The bl disabled extern. */
	private boolean blDisabledExtern;//disabled comboBox Institution Type
	
	/** The bl disabled user extern. */
	private boolean blDisabledUserExtern;
	
	/** The ua. */
	private UserAccount ua;
	
	/** The extern disabled. */
	private boolean externDisabled = false;
	
	/** The select subsidiary. */
	private boolean selectSubsidiary;
	
	/** The has user committer privilege. */
	private boolean hasUserCommitterPrivilege;
	
	/** The list subsidiary . */
	private List<Subsidiary> lstSubsidiary;
	
	/** The subsidiary filter. */
	private SubsidiaryFilter subsidiaryFilter;
	
	/** The list institution type for filter. */
    private List<Parameter> lstDepartament;
	
	/**  The description Subsidiary. */
	private String descriptionSubsidiary;
	
	/**  The description Subsidiary. */
	private String descriptionDepartament;
	
	/** The user service facade. */
	@EJB
	protected SubsidiaryServiceFacade subsidiaryServiceFacade;
	
	
	/** The disabled name. */
	private boolean disabledName;
	
	 /**
	 * Instantiates a new user bean.
	 */
	public UserBean(){
	
		 userAccountFilter=new UserAccountFilter();
		 userAccountFilter.setUserAccountState(Integer.valueOf(-1));
		 userAccountFilter.setIdInstitutionSecurity(Integer.valueOf(-1));
		 userAccountFilter.setIdUserAccountType(Integer.valueOf(-1));
		 userAccountFilter.setInstitutionType(Integer.valueOf(-1));
		 historicalState=new HistoricalState();
		 historicalState.setMotive(Integer.valueOf(-1));
	}
	
	/**
	 * Init.
	 */
	@PostConstruct
	@LoggerAuditWeb
	public void init(){
		try {
	    	if (!FacesContext.getCurrentInstance().isPostback()) {
	    		
				 beginConversation();
				 SystemProfileFilter ps=new SystemProfileFilter();
				 ps.setSystemOptionState(SystemProfileStateType.REGISTERED.getCode());
				 
				 lstInstitutions =userServiceFacade.searchAllInstitutionsServiceFacade();
				 
				 listUserType();
				 loadListUserDocumentType();
				 showMotives();		 
				 this.setValidUserCode(0);
				 
				 lstInstitutionTypeForFilter = new ArrayList<Parameter>();
				 lstInstitutionTypeForFilterTemp = userServiceFacade.listClientTypeByDefinitionServiceFacade(ParameterTableType.INSITUTIONS_STATE.getCode());
				 
				 lstDepartament = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.DEPARTAMENT.getCode());
				 
				 userAccountState = userServiceFacade.listClientTypeByDefinitionServiceFacade(ParameterTableType.USER_STATES.getCode());
				 userAccountBean = new UserAccount();
				 userAccountBean.setLoginUser(userInfo.getUserAccountSession().getUserName());
				 ua = userServiceFacade.getUserAccountByLogin(userAccountBean);
				 //Show Only valid privileges. Only can be showed Register, Search Modify, Confirm, Reject, Block and Unblock
				 showOnlyValidPrivileges(ua);
				 if(!userInfo.getUserAccountSession().getInstitutionType().equals(InstitutionType.DEPOSITARY.getCode())){
					 loadInstitutionSecurityByExternal();
					 lstInstitutionTypeForFilter.addAll(lstInstitutionTypeForFilterTemp);
					 //only restring for exter user					 
					 userAccountFilter.setInstitutionType(userInfo.getUserAccountSession().getInstitutionType());					 
					 userAccountFilter.setIdInstitutionSecurity(userInfo.getUserAccountSession().getInstitutionCode());
					 userAccountFilter.setIdUserAccountType(UserAccountType.EXTERNAL.getCode());
					 externDisabled = true;
				 }
				 lstSubsidiary = new ArrayList<Subsidiary>();
				 subsidiaryFilter = new SubsidiaryFilter();
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Load institution security by external.
	 */
	public void loadInstitutionSecurityByExternal(){
		lstInstitutionsSecurityForFilter = new ArrayList<InstitutionsSecurity>();	
		if(userInfo.getUserAccountSession().getInstitutionType() > 0 ){
			InstitutionSecurityFilter insSecurityFilter=new InstitutionSecurityFilter();			
			if(InstitutionType.ISSUER_DPF.getCode().equals(userInfo.getUserAccountSession().getInstitutionType())){
				insSecurityFilter.setIdInstitutionTypeFk(InstitutionType.ISSUER.getCode());
			} else if(InstitutionType.PARTICIPANT_INVESTOR.getCode().equals(userInfo.getUserAccountSession().getInstitutionType())){
				insSecurityFilter.setIdInstitutionTypeFk(InstitutionType.PARTICIPANT.getCode());
			} else {
				insSecurityFilter.setIdInstitutionTypeFk(userInfo.getUserAccountSession().getInstitutionType());
			}																
			try {
				lstInstitutionsSecurityForFilter = userServiceFacade.getInstitutionsByFilterServiceFacade(insSecurityFilter);
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			lstInstitutionsSecurityForFilter=new ArrayList<InstitutionsSecurity>();
		}
	}
	
	/**
	 * Show only valid privileges.
	 *
	 * @param ua the ua
	 * @throws ServiceException the service exception
	 */
	private void showOnlyValidPrivileges(UserAccount ua) throws ServiceException{
		UserAcctions userAcctions = new UserAcctions();
		//only confirm or reject block/unblock/unsubscribe users internal o external with attribute committer
		if(BooleanType.YES.getCode().equals(ua.getIndIsCommitter())){
			hasUserCommitterPrivilege=true;
		}else {
			hasUserCommitterPrivilege=false;
		}
		//Register
		if(UserAccountType.INTERNAL.getCode().equals(ua.getType()) ||
			(BooleanType.YES.getCode().equals(ua.getIndIsRecorder()) && UserAccountType.EXTERNAL.getCode().equals(ua.getType()))){
			userAcctions.setIdPrivilegeRegister(userPrivilege.getUserAcctions().getIdPrivilegeRegister());
			userAcctions.setRegister(userPrivilege.getUserAcctions().isRegister());
		}

		 //Search
		 userAcctions.setSearch(userPrivilege.getUserAcctions().isSearch());
		 userAcctions.setIdPrivilegeSearch(userPrivilege.getUserAcctions().getIdPrivilegeSearch());
		 
		 //Modify
		 userAcctions.setModify(userPrivilege.getUserAcctions().isModify());
		 userAcctions.setIdPrivilegeModify(userPrivilege.getUserAcctions().getIdPrivilegeModify());
		 
		 //Confirm
		 if(UserAccountType.INTERNAL.getCode().equals(ua.getType()) ||
			(BooleanType.YES.getCode().equals(ua.getIndIsCommitter()) && UserAccountType.EXTERNAL.getCode().equals(ua.getType()))){
			 userAcctions.setConfirm(userPrivilege.getUserAcctions().isConfirm());	
			 userAcctions.setIdPrivilegeConfirm(userPrivilege.getUserAcctions().getIdPrivilegeConfirm());
		 } 
		 
		 //Reject
		 if(UserAccountType.INTERNAL.getCode().equals(ua.getType()) ||
			(BooleanType.YES.getCode().equals(ua.getIndIsCommitter()) && UserAccountType.EXTERNAL.getCode().equals(ua.getType()))){
			 userAcctions.setReject(userPrivilege.getUserAcctions().isReject());
			 userAcctions.setIdPrivilegeReject(userPrivilege.getUserAcctions().getIdPrivilegeReject());
		 }
		 //Block
		 userAcctions.setBlock(userPrivilege.getUserAcctions().isBlock());
		 userAcctions.setIdPrivilegeBlock(userPrivilege.getUserAcctions().getIdPrivilegeBlock());
		 
		 //Unblock
		 userAcctions.setUnblock(userPrivilege.getUserAcctions().isUnblock());
		 userAcctions.setIdPrivilegeUnblock(userPrivilege.getUserAcctions().getIdPrivilegeUnblock());
		 
		//Unregister
		 userAcctions.setAnnular(userPrivilege.getUserAcctions().isAnnular());
		 userAcctions.setIdPrivilegeAnnular(userPrivilege.getUserAcctions().getIdPrivilegeAnnular());
		 
		 //confirm
		 userAcctions.setConfirmBlock(userPrivilege.getUserAcctions().isConfirmBlock());
		 userAcctions.setIdPrivilegeConfirmBlock(userPrivilege.getUserAcctions().getIdPrivilegeConfirmBlock());
		 userAcctions.setConfirmUnblock(userPrivilege.getUserAcctions().isConfirmUnblock());
		 userAcctions.setIdPrivilegeConfirmUnblock(userPrivilege.getUserAcctions().getIdPrivilegeConfirmUnblock());
		 userAcctions.setConfirmAnnular(userPrivilege.getUserAcctions().isConfirmAnnular());
		 userAcctions.setIdPrivilegeConfirmAnnular(userPrivilege.getUserAcctions().getIdPrivilegeConfirmAnnular());
		 //reject
		 userAcctions.setRejectBlock(userPrivilege.getUserAcctions().isRejectBlock());
		 userAcctions.setIdPrivilegeRejectBlock(userPrivilege.getUserAcctions().getIdPrivilegeRejectBlock());
		 userAcctions.setRejectUnblock(userPrivilege.getUserAcctions().isRejectUnblock());
		 userAcctions.setIdPrivilegeRejectUnblock(userPrivilege.getUserAcctions().getIdPrivilegeRejectUnblock());
		 userAcctions.setRejectAnnular(userPrivilege.getUserAcctions().isRejectAnnular());
		 userAcctions.setIdPrivilegeRejectAnnular(userPrivilege.getUserAcctions().getIdPrivilegeRejectAnnular());
		 userPrivilege.setUserAcctions(userAcctions);
		//setting privilege
		PrivilegeComponent privilegeComp=new PrivilegeComponent();
		privilegeComp.setBtnModifyView(true);
		privilegeComp.setBtnRegisterView(true);
		privilegeComp.setBtnConfirmView(true);
		privilegeComp.setBtnRejectView(true);
		privilegeComp.setBtnBlockView(true);
		privilegeComp.setBtnUnblockView(true);
		privilegeComp.setBtnAnnularView(true);
		userPrivilege.setPrivilegeComponent(privilegeComp);
	}
	
	/**
	 * Register user listener.
	 *
	 * This method is called for register a new user
	 * Initialize some variables with a default value  and load the profiles tree
	 * @param event the event
	 */
	@LoggerAuditWeb
	public void registerUserListener(ActionEvent event){
		lstSystemProfile = null;
		blIsCommitter=false;
		blIsRecorder=false;
		blDisabledExtern=false;
		disabledName=false;
		try {
			userAccountBean=new UserAccount();
			userAccountBean.setType(Integer.valueOf(-1));
			userAccountBean.setResponsability(Integer.valueOf(-1));
			userAccountBean.setDocumentType(Integer.valueOf(-1));
			//check user logged for loginName
			userAccountBean.setLoginUser(userInfo.getUserAccountSession().getUserName());
			ua = userServiceFacade.getUserAccountByLogin(userAccountBean);
			if(UserAccountType.EXTERNAL.getCode().equals(ua.getType())){
				//load profiles allowed only extern users
				UserProfile userProfile = new UserProfile();
				userProfile.setUserAccount(ua);
				userProfile.setState(UserProfileStateType.REGISTERED.getCode());
				profilesAllowedUser = userServiceFacade.searchRegisteredProfilesAllowedUser(userProfile);
				lstSystemProfile = new ArrayList<>();
				for(UserProfileAllowed profileAllowed :profilesAllowedUser){
					lstSystemProfile.add(profileAllowed.getSystemProfile());
				}
				userAccountBean.setInternalUser(false);
				userAccountBean.setInstitutionTypeFk(ua.getInstitutionTypeFk());
				userAccountBean.setInstitutionsSecurity(ua.getInstitutionsSecurity());
				userAccountBean.setType(ua.getType());
				blDisabledExtern=true;
				blDisabledUserExtern=true;
				changeInstitutionTypeListener();
				createLoginUserListener(true);
				lstInstitutionTypeForFilter=new ArrayList<Parameter>();
				lstInstitutionTypeForFilter.addAll(lstInstitutionTypeForFilterTemp);
				for(Parameter param:lstInstitutionTypeForFilterTemp){
					if(InstitutionType.DEPOSITARY.getCode().equals(param.getIdParameterPk())){
						lstInstitutionTypeForFilter.remove(param);
					}
				}
			}else{
				userAccountBean.setInternalUser(true);
				userAccountBean.setInstitutionTypeFk(Integer.valueOf(-1));
				InstitutionsSecurity institutionSecurity=new InstitutionsSecurity();
				institutionSecurity.setIdInstitutionSecurityPk(-1);
				userAccountBean.setInstitutionsSecurity(institutionSecurity);				
			}			
			setOperationType(ViewOperationsType.REGISTER.getCode());
			lstInstitutionsTypeSelected=new ArrayList<String>();
			lstProfilesAssignedToUser=new ArrayList<UserProfile>();
			profilesAllowedUser = new ArrayList<>();
			listInstitutionTypeDetailAssignedToUser=new ArrayList<InstitutionTypeDetail>();
			this.setValidUserCode(0);
			Subsidiary objSubsidiary = new Subsidiary();
			userAccountBean.setSubsidiary(objSubsidiary);
			subsidiaryFilter = new SubsidiaryFilter();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * User Detail.
	 *
	 * This method is called to see the detail information for the user
	 * @param event the event
	 */
	@LoggerAuditWeb
	public void userDetail(ActionEvent event){
		try {
			selectSubsidiary = false;
			setOperationType( ViewOperationsType.DETAIL.getCode() );
			
			userAccountBean=(UserAccount)event.getComponent().getAttributes().get("userAccountSelected");
			
			if(Validations.validateIsNotNullAndNotEmpty(userAccountBean.getInstitutionTypeFk())){
				if(InstitutionType.ISSUER_DPF.getCode().equals(userAccountBean.getInstitutionTypeFk())){
					selectSubsidiary = true;
				} else if(InstitutionType.PARTICIPANT_INVESTOR.getCode().equals(userAccountBean.getInstitutionTypeFk())){
					selectSubsidiary = true;
				}
			}
			
			if(BooleanType.YES.getCode().equals(userAccountBean.getIndIsRecorder())){
				blIsRecorder=true;
			}else{
				blIsRecorder=false;
			}
			if (BooleanType.YES.getCode().equals(userAccountBean.getIndIsCommitter())) {
				blIsCommitter=true;
			}else{
				blIsCommitter=false;
			}
			listInstitutionTypeDetailAssignedToUser=userServiceFacade.searchInstitutionTypeByUserServiceFacade(
					userAccountBean.getIdUserPk(), InstitutionTypeDetailStateType.REGISTERED.getCode());
			
			UserProfile userProfile=new UserProfile();
			userProfile.setUserAccount(userAccountBean);
			userProfile.setState( UserProfileStateType.REGISTERED.getCode() );
			lstProfilesAssignedToUser=userServiceFacade.searchRegisteredProfilesByUserServiceFacade(userProfile);

			loadTreeAssignedProfiles();
			
			//show the institution types list by user type
			if(UserAccountType.INTERNAL.getCode().equals(userAccountBean.getType())){
				lstInstitutionTypeForFilter=new ArrayList<Parameter>();
				lstInstitutionTypeForFilter.addAll(lstInstitutionTypeForFilterTemp);
				for(Parameter param:lstInstitutionTypeForFilterTemp){
					if(!InstitutionType.DEPOSITARY.getCode().equals(param.getIdParameterPk())){
						lstInstitutionTypeForFilter.remove(param);
					}
				}
			}else{
				lstInstitutionTypeForFilter=new ArrayList<Parameter>();
				lstInstitutionTypeForFilter.addAll(lstInstitutionTypeForFilterTemp);
				for(Parameter param:lstInstitutionTypeForFilterTemp){
					if(InstitutionType.DEPOSITARY.getCode().equals(param.getIdParameterPk())){
						lstInstitutionTypeForFilter.remove(param);
					}
				}
			}
			userAccountBean.setDocumentFile(userServiceFacade.getUserDocumentRegister(userAccountBean.getIdUserPk()));
			
			if(userAccountBean.getState().equals(UserAccountStateType.ANNULLED.getCode()) ||
				(userAccountBean.getSituation() != null && 
				userAccountBean.getSituation().equals(UserAccountSituationType.PENDING_UNREGISTER.getCode()))){
				
				userAccountBean.setUnregisterFile(userServiceFacade.getUserDocumentUnregister(userAccountBean.getIdUserPk()));
			}
			lstShowInstitutionTypeAssignedToUser=getListShowInstitutionTypeDetail();
			
			//Get subsidiary
			if(userAccountBean.getSubsidiary() != null){
				subsidiaryFilter.setDepartament(userAccountBean.getSubsidiary().getDepartament());
				descriptionDepartament = systemServiceFacade.getParameterByPk(userAccountBean.getSubsidiary().getDepartament()).getName();
				descriptionSubsidiary = userAccountBean.getSubsidiary().getSubsidiaryDescription();
			} else {
				descriptionSubsidiary = null;
				descriptionDepartament = null;
			}
			
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	

	/**
	 * User modification action.
	 * This method is called to modify user
	 * Validate that selected user is in confirmed state 
	 * load the institution type detail assigned for the user
	 * load the profiles assigned to the user
	 * @return the string
	 */
	@LoggerAuditWeb
	public String userModificationAction(){
		try {
			selectSubsidiary = false;
			disabledName=false;
			JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
			//hideDialogs();
			if(selectedUserAccountBean==null){
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT, null, 
						PropertiesConstants.USER_ERROR_REGISTER_NO_SELECTED, null);	
				JSFUtilities.showComponent("cnfMsgCustomValidation");
				return null;									
			}else if(!(selectedUserAccountBean.getState().intValue()==UserAccountStateType.REGISTERED.getCode().intValue() 
					|| selectedUserAccountBean.getState().intValue()==UserAccountStateType.CONFIRMED.getCode().intValue())){
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT, null,
						PropertiesConstants.USER_ERROR_MODIFY, null);	
				//JSFUtilities.showComponent("cnfMsgCustomValidation");
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidation').show()");
				return null;
			}
			loadSelectedUser();
			documentType=selectedUserAccountBean.getDocumentType();
			documentNumber=selectedUserAccountBean.getDocumentNumber();
			setOperationType( viewOperationsType.MODIFY.getCode() );
			showInstitutionsByUserAccountType(userAccountBean.getType());
			lstInstitutionsTypeSelected=new ArrayList<String>();
			if(userAccountBean.isRegulatorAgentBool()){
				listInstitutionTypeDetailAssignedToUser=userServiceFacade.searchInstitutionTypeByUserServiceFacade(selectedUserAccountBean.getIdUserPk(), 
																												 InstitutionTypeDetailStateType.REGISTERED.getCode());
				if(!listInstitutionTypeDetailAssignedToUser.isEmpty()){
					for(InstitutionTypeDetail insTypeDet:listInstitutionTypeDetailAssignedToUser){
						lstInstitutionsTypeSelected.add( String.valueOf( insTypeDet.getIdInstitutionTypeFk().intValue() ) );
					}
				}
			}
			UserProfile perfilUsuario=new UserProfile();
			perfilUsuario.setUserAccount(userAccountBean);
			perfilUsuario.setState(UserProfileStateType.REGISTERED.getCode());
			lstProfilesAssignedToUser=userServiceFacade.searchRegisteredProfilesByUserServiceFacade(perfilUsuario);
			showTreePanelExterns();
			if(userInfo.getUserAccountSession().isDepositaryInstitution()){
				if(showProfilesExtern){
					//load profiles allowed only extern users
					UserProfile userProfile = new UserProfile();
					UserAccount userAccountRegister = new UserAccount();
					//in modification load from user saved
					userAccountRegister.setIdUserPk(userAccountBean.getIdUserPk());
					userProfile.setUserAccount(userAccountRegister);
					userProfile.setState(UserProfileStateType.REGISTERED.getCode());
					profilesAllowedUser = userServiceFacade.searchRegisteredProfilesAllowedUser(userProfile);
				} else {
					profilesAllowedUser = null;
				}
			} else {
				UserAccount userLoginRegistered = new UserAccount();
				userLoginRegistered.setLoginUser(userAccountBean.getRegistryUser());
				UserAccount userAccountRegister = userServiceFacade.getUserAccountByLogin(userLoginRegistered);
//				if(userAccountRegister != null){
					//If user register is not confirmed so we using from user session
					userAccountRegister = new UserAccount();
					userAccountRegister.setIdUserPk(userInfo.getUserAccountSession().getIdUserAccountPk());
//				}
					
				UserProfile userProfile = new UserProfile();
				userProfile.setUserAccount(userAccountRegister);
				userProfile.setState(UserProfileStateType.REGISTERED.getCode());
				profilesAllowedUser = userServiceFacade.searchRegisteredProfilesAllowedUser(userProfile);
				lstSystemProfile = new ArrayList<>();
				for(UserProfileAllowed profileAllowed :profilesAllowedUser){
					lstSystemProfile.add(profileAllowed.getSystemProfile());
				}
			}
			//show the institution types list by user type
			if(UserAccountType.INTERNAL.getCode().equals(userAccountBean.getType())){
				lstInstitutionTypeForFilter=new ArrayList<Parameter>();
				lstInstitutionTypeForFilter.addAll(lstInstitutionTypeForFilterTemp);
				for(Parameter param:lstInstitutionTypeForFilterTemp){
					if(!InstitutionType.DEPOSITARY.getCode().equals(param.getIdParameterPk())){
						lstInstitutionTypeForFilter.remove(param);
					}
				}
				//Commented by Hema
				lstInstitutionType=InstitutionType.listInternalInstitutions;
			}else{
				//Commented by Hema
				lstInstitutionType=InstitutionType.listExternalInstitutions;
				lstInstitutionTypeForFilter=new ArrayList<Parameter>();
				lstInstitutionTypeForFilter.addAll(lstInstitutionTypeForFilterTemp);
				for(Parameter param:lstInstitutionTypeForFilterTemp){
					if(InstitutionType.DEPOSITARY.getCode().equals(param.getIdParameterPk())){
						lstInstitutionTypeForFilter.remove(param);
					}
				}
			}
			//Show the institution list by institution type of the user			
			InstitutionSecurityFilter insSecurityFilter=new InstitutionSecurityFilter();			
			if(Validations.validateIsNotNullAndNotEmpty(selectedUserAccountBean.getInstitutionTypeFk())){
				if(InstitutionType.ISSUER_DPF.getCode().equals(selectedUserAccountBean.getInstitutionTypeFk())){
					selectSubsidiary = true;
					insSecurityFilter.setIdInstitutionTypeFk(InstitutionType.ISSUER.getCode());
				} else if(InstitutionType.PARTICIPANT_INVESTOR.getCode().equals(selectedUserAccountBean.getInstitutionTypeFk())){
					selectSubsidiary = true;
					insSecurityFilter.setIdInstitutionTypeFk(InstitutionType.PARTICIPANT.getCode());
				}  else {
					insSecurityFilter.setIdInstitutionTypeFk(userAccountBean.getInstitutionTypeFk() );
				}
			} else {
				insSecurityFilter.setIdInstitutionTypeFk(userAccountBean.getInstitutionTypeFk() );
			}			
			//insSecurityFilter.setIdInstitutionTypeFk(userAccountBean.getInstitutionTypeFk() );
			lstInstitutionsByInstitutionType=userServiceFacade.getInstitutionsByFilterServiceFacade(insSecurityFilter);
			userAccountBean.setInstitutionsSecurity(selectedUserAccountBean.getInstitutionsSecurity());
			validateTypeDocument();
			userAccountBean.setDocumentNumber(documentNumber);
			
			//Load sudsidiary
			getLstSubsidiaryForDetail(selectedUserAccountBean.getInstitutionsSecurity().getIdInstitutionSecurityPk(),
					selectedUserAccountBean.getInstitutionTypeFk());
			
			if(userAccountBean.getSubsidiary() != null){
				subsidiaryFilter.setDepartament(userAccountBean.getSubsidiary().getDepartament());				
			}
			
			loadProfileTree();
			return "userMgmt";
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}
	
	/**
	 * Gets the lst subsidiary for detail.
	 *
	 * @param intInstitutionSecurity the int institution security
	 * @param intitutionType the intitution type
	 * @return the lst subsidiary for detail
	 */
	public void getLstSubsidiaryForDetail(Integer intInstitutionSecurity, Integer intitutionType){		
		lstSubsidiary = new ArrayList<Subsidiary>();				
		try {
			if(Validations.validateIsNotNullAndNotEmpty(intInstitutionSecurity)){
				SubsidiaryFilter subsidiaryFilterSearch = new SubsidiaryFilter();
				subsidiaryFilterSearch.setInstitutionSecurity(intInstitutionSecurity);
				subsidiaryFilterSearch.setInstitutionType(intitutionType);				
				lstSubsidiary = subsidiaryServiceFacade.lstSubsidiaryByInstitutionSecurity(subsidiaryFilterSearch);
			} else {
				lstSubsidiary = null;
			}
		} catch (ServiceException e) {
			
		}
	}
	
	/**
	 * Load selected user.
	 */
	public void loadSelectedUser(){
		userAccountBean = new UserAccount();
		if(BooleanType.YES.getCode().equals(selectedUserAccountBean.getIndIsCommitter())){
			blIsCommitter=true;
		}else{
			blIsCommitter=false;
		}
		if(BooleanType.YES.getCode().equals(selectedUserAccountBean.getIndIsRecorder())){
			blIsRecorder=true;
		}else{
			blIsRecorder=false;
		}
		userAccountBean.setIndIsCommitter(selectedUserAccountBean.getIndIsCommitter());
		userAccountBean.setIndIsRecorder(selectedUserAccountBean.getIndIsRecorder());
		userAccountBean.setBlockMotive(selectedUserAccountBean.getBlockMotive());
		userAccountBean.setBlockOtherMotive(selectedUserAccountBean.getBlockOtherMotive());
		userAccountBean.setChangePassword(selectedUserAccountBean.getChangePassword());
		userAccountBean.setConfirmationDate(selectedUserAccountBean.getConfirmationDate());
		userAccountBean.setConfirmationUser(selectedUserAccountBean.getConfirmationUser());
		userAccountBean.setDocumentNumber(selectedUserAccountBean.getDocumentNumber());
		userAccountBean.setDocumentType(selectedUserAccountBean.getDocumentType());
		userAccountBean.setExceptionUserPrivileges(selectedUserAccountBean.getExceptionUserPrivileges());
		userAccountBean.setFirtsLastName(selectedUserAccountBean.getFirtsLastName());
		userAccountBean.setFullName(selectedUserAccountBean.getFullName());
		userAccountBean.setIdEmail(selectedUserAccountBean.getIdEmail());
		userAccountBean.setIdIssuerFk(selectedUserAccountBean.getIdIssuerFk());
		userAccountBean.setIdParticipantFk(selectedUserAccountBean.getIdParticipantFk());
		userAccountBean.setIdUserPk(selectedUserAccountBean.getIdUserPk());						
		userAccountBean.setInstitutionsSecurity(selectedUserAccountBean.getInstitutionsSecurity());
		userAccountBean.setInstitutionTypeDetails(selectedUserAccountBean.getInstitutionTypeDetails());		
		
		userAccountBean.setInstitutionTypeFk(selectedUserAccountBean.getInstitutionTypeFk());
				
		if(UserAccountType.INTERNAL.getCode().equals(ua.getType())){
			userAccountBean.setInternalUser(true);
		}	
		userAccountBean.setLastModifyDate(selectedUserAccountBean.getLastModifyDate());
		userAccountBean.setLastModifyIp(selectedUserAccountBean.getLastModifyIp());
		userAccountBean.setLastModifyPriv(selectedUserAccountBean.getLastModifyPriv());
		userAccountBean.setLastModifyUser(selectedUserAccountBean.getLastModifyUser());
		userAccountBean.setLoginUser(selectedUserAccountBean.getLoginUser());
		userAccountBean.setMacAddress(selectedUserAccountBean.getMacAddress());
		userAccountBean.setMacRestriction(selectedUserAccountBean.getMacRestriction());
		userAccountBean.setMacRestrictionBool(selectedUserAccountBean.isMacRestrictionBool());
		userAccountBean.setPhoneNumber(selectedUserAccountBean.getPhoneNumber());
		userAccountBean.setRegistryDate(selectedUserAccountBean.getRegistryDate());
		userAccountBean.setRegistryUser(selectedUserAccountBean.getRegistryUser());
		userAccountBean.setRegulatorAgent(selectedUserAccountBean.getRegulatorAgent());
		userAccountBean.setRegulatorAgentBool(selectedUserAccountBean.isRegulatorAgentBool());
		userAccountBean.setRejectDate(selectedUserAccountBean.getRejectDate());
		userAccountBean.setRejectUser(selectedUserAccountBean.getRejectUser());
		userAccountBean.setResponsability(selectedUserAccountBean.getResponsability());
		userAccountBean.setScheduleExceptionDetails(selectedUserAccountBean.getScheduleExceptionDetails());
		userAccountBean.setSecondLastName(selectedUserAccountBean.getSecondLastName());
		userAccountBean.setSelected(selectedUserAccountBean.isSelected());
		userAccountBean.setState(selectedUserAccountBean.getState());
		userAccountBean.setSituation(selectedUserAccountBean.getSituation());
		userAccountBean.setType(selectedUserAccountBean.getType());
		userAccountBean.setUnblockMotive(selectedUserAccountBean.getUnblockMotive());
		userAccountBean.setUnblockOtherMotive(selectedUserAccountBean.getUnblockOtherMotive());
		userAccountBean.setUserProfiles(selectedUserAccountBean.getUserProfiles());
		userAccountBean.setUserSessions(selectedUserAccountBean.getUserSessions());
		userAccountBean.setUserTypeDescription(selectedUserAccountBean.getUserAccountTypeDescription());
		userAccountBean.setIndExtInterface(selectedUserAccountBean.getIndExtInterface());
		userAccountBean.setDocumentFile(selectedUserAccountBean.getDocumentFile());
		userAccountBean.setUnregisterFile(selectedUserAccountBean.getUnregisterFile());
		if(selectedUserAccountBean.getSubsidiary() != null){
			userAccountBean.setSubsidiary(selectedUserAccountBean.getSubsidiary());
		} else {
			userAccountBean.setSubsidiary(new Subsidiary());
		}
		userAccountBean.setOffice(selectedUserAccountBean.getOffice());
	}
	/**
	 * Search user listener.
	 * 
	 * @param event the event
	 */
	public void searchUserListener(ActionEvent event){
		try {
			listUsersByFilter(userAccountFilter);			
//			JSFUtilities.showComponent("frmGeneral:pnlResult");
//			JSFUtilities.showComponent("frmGeneral:pnlPrivileges");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	

	/**
	 * Clean search users listener.
	 *
	 * @param event the event
	 */
	public void cleanSearchUsersListener(ActionEvent event){
		try {
			cleanUsers();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clean users.
	 *  This method clean the datatable and the entered filters
	 */
	public void cleanUsers(){
		userAccountFilter = new UserAccountFilter();
		selectedUserAccountBean =null;
		userDataModel=null;
		if(!userInfo.getUserAccountSession().getInstitutionType().equals(InstitutionType.DEPOSITARY.getCode())){
			 //only restring for exter user
			 userAccountFilter.setIdInstitutionSecurity(userInfo.getUserAccountSession().getInstitutionCode());
			 userAccountFilter.setInstitutionType(userInfo.getUserAccountSession().getInstitutionType());
			 userAccountFilter.setIdUserAccountType(UserAccountType.EXTERNAL.getCode());
			 externDisabled = true;
		 }
	}
	
	/**
	 * List users by filter.
	 *
	 * This method fill 
	 *
	 * @param userAccountFilter the user account filter
	 */
	public void listUsersByFilter(UserAccountFilter userAccountFilter){
		List<UserAccount> lstUserAccount = null;
		try{
			lstUserAccount = userServiceFacade.searchUsersByFilterServiceFacade(userAccountFilter);
			if(Validations.validateListIsNotNullAndNotEmpty(lstUserAccount)){
				for(UserAccount objUserAccount : lstUserAccount){
					Parameter objParameter = systemServiceFacade.getParameterByPk(objUserAccount.getInstitutionTypeFk());
					if(objParameter != null){
						objUserAccount.setInstitutionName(objParameter.getName());
					}				
				}
			}			
			userDataModel=new UserDataModel(lstUserAccount);
		}catch(Exception e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Get list show institution type detail.
	 * 
	 * This method returns a list of the types of institutions assigned to the user
	 *
	 * @return the list
	 */					
	//Added modifications by Hema
	public List<String> getListShowInstitutionTypeDetail(){ 
		List<String> listaResult=new ArrayList<String>();
		
		for(InstitutionTypeDetail insTypeDet : listInstitutionTypeDetailAssignedToUser){
			for(Parameter detTipoInsType : lstInstitutionTypeForFilter){
				   
				if(insTypeDet.getIdInstitutionTypeFk().intValue()==detTipoInsType.getIdParameterPk()){
					listaResult.add( detTipoInsType.getName() );
					break;
				}
			}
		}
		
		return listaResult;
	}
	
	
	/**
	 * Creates the login user edv follow this rules.
	 * first letter of name
	 * first last name full
	 * if the two first parts already exits so add first letter of second last name
	 *  else this login still exist
	 *
	 * @throws ServiceException the service exception
	 */
	public void createLoginUserEDV() throws ServiceException{
		StringBuilder username=new StringBuilder();
		String initialName=null;
		if(userAccountBean.getInstitutionsSecurity() != null && userAccountBean.getInstitutionsSecurity().getIdInstitutionSecurityPk().intValue() > 0){
			if(StringUtils.isNotBlank(userAccountBean.getFullName())){
				userAccountBean.setFullName(userAccountBean.getFullName().trim());
				username.append(StringUtils.substring(userAccountBean.getFullName(), 0, 1));
				if(StringUtils.isNotBlank(userAccountBean.getFirtsLastName())){
					userAccountBean.setFirtsLastName(userAccountBean.getFirtsLastName().trim());
					username.append(userAccountBean.getFirtsLastName());
					//check if exist same login name
					List<String> userList = userServiceFacade.getUsersWithNameServiceFacade(username.toString());
					if(userList.isEmpty()){
						//set login name
						initialName =username.toString();
					} else {
						//if exist duplicate login is required second last name
						if(StringUtils.isNotBlank(userAccountBean.getSecondLastName())){
							userAccountBean.setSecondLastName(userAccountBean.getSecondLastName().trim());
							username.append(StringUtils.substring(userAccountBean.getSecondLastName(), 0, 1));
							userList = userServiceFacade.getUsersWithNameServiceFacade(username.toString());
							if(userList.isEmpty()){
								//set long name
								initialName =username.toString();
							} else {
								//last options is to concatenate login with numbers
								int sufix=0;
								while(userList.contains(username.toString())){
									username.append(++sufix);
								}
								//set long name
								initialName =username.toString();
							}
						}
					}
				}
			}
			
		} 
		userAccountBean.setLoginUser(initialName);
	}
	
	
	/**
	 * Create login user listener.
	 * 
	 * This method create the login user, prepending with the institution mnemonic, 
	 * followed by the first letter of the name, followed by the first letter of first last name,
	 * followed by the first letter of second last name
	 */
	public void createLoginUserListener(boolean loadSubsidiary){
		//only for Internal User
		if(userAccountBean.getType() != null && userAccountBean.getType().equals(UserAccountType.INTERNAL.getCode())){
			try{
				createLoginUserEDV();
			}catch(Exception ex){
				excepcion.fire(new ExceptionToCatchEvent(ex));
			}
		} else {
		
		StringBuilder username=new StringBuilder();
		boolean blInstSelected=false;
		//validate when user type is external we have to put mnemonic name in user code txt
		//it depends what institution we selected
		if(lstInstitutionsByInstitutionType != null && lstInstitutionsByInstitutionType.size() > 0 &&
				userAccountBean.getInstitutionsSecurity().getIdInstitutionSecurityPk().intValue() > 0){
			for(InstitutionsSecurity insSec: lstInstitutionsByInstitutionType){																					
				if(userAccountBean.getInstitutionsSecurity().getIdInstitutionSecurityPk().intValue()==insSec.getIdInstitutionSecurityPk().intValue()){
					username.append(insSec.getMnemonicInstitution());
					setPreUserCode(null);
					break;
				}
			}
		}else{
			userAccountBean.setLoginUser("");
		}

		if(StringUtils.isNotBlank(userAccountBean.getFullName())){
			userAccountBean.setFullName(userAccountBean.getFullName().trim());
			username.append(userAccountBean.getFullName().substring(0,1) );
		}
		if(StringUtils.isNotBlank(userAccountBean.getFirtsLastName())){
			userAccountBean.setFirtsLastName(userAccountBean.getFirtsLastName().trim());
			username.append( userAccountBean.getFirtsLastName().substring(0,1) );
		}	
		if(StringUtils.isNotBlank(userAccountBean.getSecondLastName())){
			userAccountBean.setSecondLastName(userAccountBean.getSecondLastName().trim());
			username.append( userAccountBean.getSecondLastName().substring(0,1) );
		}
		
		if(Validations.validateIsNullOrEmpty(preUserCode) || userAccountBean.getLoginUser()==null){
			userAccountBean.setLoginUser(username.toString());
			setPreUserCode(username.toString());
			blInstSelected=true;
		}
		
			if (Validations.validateIsNotNullAndNotEmpty(preUserCode) && username.toString().equals(preUserCode)) {
				try {
					List<String> userList = null;
					if (Validations.validateIsNotNullAndNotEmpty(username.toString())) {
						userList = userServiceFacade.getUsersWithNameServiceFacade(username.toString());
					}

					if (Validations.validateListIsNotNullAndNotEmpty(userList)) {
						int id = 0;
						userAccountBean.setLoginUser(username.toString());
						while (userList.contains(userAccountBean.getLoginUser())) {
							if(id==0){
								setPreUserCode(userAccountBean.getLoginUser());
							}
							userAccountBean.setLoginUser(username.toString() + (++id));
						}
						if(id==0){
							userAccountBean.setLoginUser(username.toString());
							setPreUserCode(userAccountBean.getLoginUser());
						}
					}else{
						userAccountBean.setLoginUser(username.toString());
						setPreUserCode(userAccountBean.getLoginUser());
					}

				} catch (Exception e) {
					excepcion.fire(new ExceptionToCatchEvent(e));
				}
			}
			if(!blInstSelected){
				userAccountBean.setLoginUser(null);
			}
			
			if(loadSubsidiary){
				// Load subsidiary 	
				subsidiaryFilter.setDepartament(null);
				userAccountBean.setSubsidiary(new Subsidiary());
				lstSubsidiary = new ArrayList<Subsidiary>();				
				try {
					if(Validations.validateIsNotNullAndNotEmpty(userAccountBean.getInstitutionsSecurity().getIdInstitutionSecurityPk())){
						SubsidiaryFilter subsidiaryFilterSearch = new SubsidiaryFilter();
						subsidiaryFilterSearch.setInstitutionSecurity(userAccountBean.getInstitutionsSecurity().getIdInstitutionSecurityPk());
						subsidiaryFilterSearch.setInstitutionType(userAccountBean.getInstitutionTypeFk());
						lstSubsidiary = subsidiaryServiceFacade.lstSubsidiaryByInstitutionSecurity(subsidiaryFilterSearch);
					} else {
						lstSubsidiary = null;
					}
				} catch (ServiceException e) {
					excepcion.fire(new ExceptionToCatchEvent(e));
				}
			}
			
		}						
		
	}
	
	/**
	 * Load subsidiaries by departament.
	 */
	public void loadSubsidiariesByDepartament(){
		// Load subsidiary 		
		userAccountBean.setSubsidiary(new Subsidiary());
		lstSubsidiary = new ArrayList<Subsidiary>();				
		try {
			if(Validations.validateIsNotNullAndNotEmpty(userAccountBean.getInstitutionsSecurity().getIdInstitutionSecurityPk())
					&& Validations.validateIsNotNullAndNotEmpty(subsidiaryFilter.getDepartament())){
				SubsidiaryFilter subsidiaryFilterSearch = new SubsidiaryFilter();
				subsidiaryFilterSearch.setInstitutionSecurity(userAccountBean.getInstitutionsSecurity().getIdInstitutionSecurityPk());
				subsidiaryFilterSearch.setInstitutionType(userAccountBean.getInstitutionTypeFk());
				subsidiaryFilterSearch.setDepartament(subsidiaryFilter.getDepartament());
				lstSubsidiary = subsidiaryServiceFacade.lstSubsidiaryByInstitutionSecurity(subsidiaryFilterSearch);
			} else {
				lstSubsidiary = null;
			}
		} catch (ServiceException e) {
			
		}
	}
	
	/**
	 * Before user registration listener.
	 * This method is  called when the button save is pressed
	 * If validation is successful it show the confirmation dialog
	 *
	 * @param event the event
	 */
	public void beforeUserRegistrationListener(ActionEvent event){
		try {
			hideDialogs();
//			option file in register
//			if(isRegister() && userAccountBean.getDocumentFile() == null){
//				JSFUtilities.showComponent("cnfMsgCustomValidation");					
//				showMessageOnDialog(
//						PropertiesConstants.DIALOG_HEADER_ALERT, null,
//						PropertiesConstants.FUPL_NO_SELECTED,null);
//				String errorMessage=PropertiesUtilities.getMessage(PropertiesConstants.FUPL_NO_SELECTED);
//				JSFUtilities.addContextMessage("ffrmUserAdministration:fuplformUser",FacesMessage.SEVERITY_ERROR, errorMessage,errorMessage);
//				return;
//			}
			
			isConfirmBlockOrUnblock = false;
			isRejectOption = false;
			isUnregisterEvent = false;
			Object[] argObj=new Object[1];
			int error=0;
			//Here validating the in the both register and modification screens
			error = validationMgmtUserAccount();
			if(error==0){
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				if(isRegister()){
					createLoginUserListener(false);
					disabledName=true;
					argObj[0]=userAccountBean.getLoginUser();
					showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_CONFIRM, null, PropertiesConstants.USER_CONFIRM_REGISTER, argObj);
				}
				if(isModify()){
					if(userAccountBean.getState().equals(UserAccountStateType.REGISTERED.getCode())){
//						Problem change login after save
//						createLoginUserListener();
						disabledName=true;	
					}
					argObj[0]=userAccountBean.getLoginUser();
					showMessageOnDialog(PropertiesConstants.LBL_CONFIRM_HEADER_CONFIRM, null, PropertiesConstants.USER_CONFIRM_MODIFICATION, argObj);
				}
				
				JSFUtilities.showComponent("frmUserAdministration:cnfUserAdm");
			}
				
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Validation mgmt user account.
	 * This method validates the user required values
	 * @return 0, if successful
	 */
	public int validationMgmtUserAccount(){
		int error=0;
		try{
		boolean validateLoginUser=userServiceFacade.validateLoginUserServiceFacade(userAccountBean);
		StringBuilder strbMessage=new StringBuilder();
		boolean validateDocNumber =false;
		if(isRegister()){
			if(validateLoginUser){
				strbMessage.append(PropertiesUtilities.getMessage(PropertiesConstants.USER_ERROR_NAME_USER_REPEATED));
				JSFUtilities.addContextMessage("frmUserAdministration:txtLoginUser", 
						FacesMessage.SEVERITY_ERROR, strbMessage.toString(), strbMessage.toString());
				strbMessage.delete(0, strbMessage.length());
				error++;
			}
		}	
		if(!isModify()){
		    validateDocNumber = userServiceFacade.validateDocNumberServiceBean(userAccountBean);
		}
//		if(validateDocNumber){
//			strbMessage.append(PropertiesUtilities.getMessage(PropertiesConstants.USER_DOC_NUMBER_REPEATED));
//			JSFUtilities.addContextMessage("frmUserAdministration:txtDocumentNro", 
//					FacesMessage.SEVERITY_ERROR, strbMessage.toString(), strbMessage.toString());
//			strbMessage.delete(0, strbMessage.length());
//			error++;
//		}
		if(nodesSystemProfilesSelected == null || nodesSystemProfilesSelected.length==0){
			if(lstSystemProfile!=null && lstSystemProfile.size()>0){
				strbMessage.append(PropertiesUtilities.getMessage(PropertiesConstants.USER_ERROR_SELECT_PROFILE));
			}else{
				strbMessage.append(PropertiesUtilities.getMessage(PropertiesConstants.USER_ERROR_SELECT_NOT_FOUND_PROFILE));
			}
			JSFUtilities.addContextMessage("frmUserAdministration:treeProfiles", FacesMessage.SEVERITY_ERROR, strbMessage.toString(), strbMessage.toString());
			strbMessage.delete(0, strbMessage.length());
			error++;
		}
		
		if(nodesSystemProfilesSelected != null && nodesSystemProfilesSelected.length > 1){			
			List<Integer> lstCodes = new ArrayList<Integer>();
			for(TreeNode node : nodesSystemProfilesSelected){	
				if(((NodeBean)node.getData()).getLevel().equals(GeneralConstants.TWO_VALUE_INTEGER)){
				NodeBean nodeBean = (NodeBean) node.getParent().getData();					
				lstCodes.add(nodeBean.getCode());
				}
			}			
			Set<Integer> setToRepeat = new HashSet(); 
			Set<Integer> setCode = new HashSet();		
			for (Integer intCodeSystem : lstCodes){
				if (!setCode.add(intCodeSystem)) {
					setToRepeat.add(intCodeSystem);
				}
			}			
			if(setToRepeat != null && setToRepeat.size() > 0){
				strbMessage.append(PropertiesUtilities.getMessage(PropertiesConstants.USER_ERROR_REGISTER_MOST_ONE_PROFILE));
				JSFUtilities.addContextMessage("frmUserAdministration:treeProfiles", FacesMessage.SEVERITY_ERROR, strbMessage.toString(), strbMessage.toString());
				strbMessage.delete(0, strbMessage.length());
				error++;
			}							
		}
		
		if(showProfilesExtern){
			if(nodesExternsProfilesSelected==null || nodesExternsProfilesSelected.length==0){
				if(lstSystemProfile!=null && lstSystemProfile.size()>0){
					strbMessage.append(PropertiesUtilities.getMessage(PropertiesConstants.USER_ERROR_SELECT_PROFILE_ALLOWED));
				}else{
					strbMessage.append(PropertiesUtilities.getMessage(PropertiesConstants.USER_ERROR_SELECT_NOT_FOUND_PROFILE_ALLOWED));
				}
				JSFUtilities.addContextMessage("frmUserAdministration:treeEnabledProfiles", FacesMessage.SEVERITY_ERROR, strbMessage.toString(), strbMessage.toString());
				strbMessage.delete(0, strbMessage.length());
				error++;
			}
		}
		this.validUserCode = error;
		}catch(Exception e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return error;
		
	}
	
	/**
	 * Validate exist type document and number document.
	 */
	public void validateExistTypeDocumentAndNumberDocument(){
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		hideDialogs();
		
//		boolean validateDocNumber = false;
//		try{
//			hideDialogs();
//			if(!isModify()){
//				validateDocNumber = userServiceFacade.validateDocNumberServiceBean(userAccountBean);
//			}
//		}catch(Exception e){
//			
//		}
//		if(validateDocNumber){
//			userAccountBean.setDocumentNumber("");
//			String str = PropertiesUtilities.getMessage(PropertiesConstants.USER_DOC_NUMBER_REPEATED);
//			JSFUtilities.addContextMessage("frmUserAdministration:txtDocumentNro", 
//					FacesMessage.SEVERITY_ERROR, str, str);
//			JSFUtilities.showEndTransactionSamePageDialog();
//			JSFUtilities.showMessageOnDialog(
//					PropertiesConstants.DIALOG_HEADER_ALERT, null, 
//					PropertiesConstants.USER_DOC_NUMBER_REPEATED, null);
//		}
	}
	
	/**
	 * Institution type detail assigned.
	 *
	 * This method checks if the user is assigned to the institution type
	 *
	 * @param idInstitutionType the id institution type
	 * @return true, if successful
	 */
	public boolean institutionTypeDetailAssigned(int idInstitutionType){
		for(InstitutionTypeDetail insTypeDet:listInstitutionTypeDetailAssignedToUser){
			if(insTypeDet.getIdInstitutionTypeFk().intValue()==idInstitutionType){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Institution type detail checked.
	 * This method checked if the institution type is selected in the view
	 *
	 * @param idTipoInstitucion the id tipo institucion
	 * @return true, if successful
	 */
	public boolean institutionTypeDetailChecked(int idTipoInstitucion){
		for(String idTypeIns : lstInstitutionsTypeSelected){
			if(Integer.parseInt( idTypeIns )==idTipoInstitucion){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Checks if is node profile selected.
	 * This method checks if a profile was selected in the profile tree
	 *
	 * @param treeSelected the tree selected
	 * @param idSystemProfile the id system profile
	 * @return true, if is node profile selected
	 */
	public boolean isNodeProfileSelected(TreeNode[] treeSelected,int idSystemProfile){
		NodeBean nodeSystemProfileBean=null;
		if(treeSelected!=null){
			for(TreeNode node:treeSelected){
				nodeSystemProfileBean=(NodeBean)node.getData();
				if(nodeSystemProfileBean.getLevel()==2){
					if(nodeSystemProfileBean.getCode()==idSystemProfile){
						return true;
					}
				}
			}
		}
		return false;
	}
	
	/**
	 * User registration listener.
	 * This method is called after confirming, if save or update the user
	 */
	@LoggerAuditWeb
	public void saveUserListener(){
		try {
			JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);			
			hideDialogs();
			if(isRegister()){
				registry();
			}else if(isModify()) {
				modify();
			}
			JSFUtilities.showComponent("cnfEndTransaction");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Registry.
	 *
	 * This method is called if the operation type is "register"
	 */
	@LoggerAuditWeb
	public void registry(){
		try{
			Object[] argObj=new Object[1];
			int error=validationMgmtUserAccount();
			if(error>0){
				return;
			}
			userAccountBean.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			userAccountBean.setRegistryDate(new Date());
			userAccountBean.setState(UserAccountStateType.REGISTERED.getCode());;
			userAccountBean.setChangePassword(BooleanType.YES.getCode());
			if(blIsRecorder == true){
				userAccountBean.setIndIsRecorder(BooleanType.YES.getCode());
			}else{
				userAccountBean.setIndIsRecorder(BooleanType.NO.getCode());
			}
			if(blIsCommitter == true){
				userAccountBean.setIndIsCommitter(BooleanType.YES.getCode());
			}else{
				userAccountBean.setIndIsCommitter(BooleanType.NO.getCode());
			}
			
			if (stageApplication != null && (stageApplication.equals(StageApplication.Development)
					|| stageApplication.equals(StageApplication.developmenttest))) {
				userAccountBean.setIndMfaCode(BooleanType.NO.getCode());
			} else {
				userAccountBean.setIndMfaCode(BooleanType.YES.getCode());
			}
			//Get code entity
			int indexEntity = lstInstitutionsByInstitutionType.indexOf(userAccountBean.getInstitutionsSecurity());
			InstitutionsSecurity institutionEntity = lstInstitutionsByInstitutionType.get(indexEntity);
			if(userAccountBean.getInstitutionTypeFk().equals(InstitutionType.PARTICIPANT.getCode()) ||
					userAccountBean.getInstitutionTypeFk().equals(InstitutionType.AFP.getCode()) ||
					userAccountBean.getInstitutionTypeFk().equals(InstitutionType.PARTICIPANT_INVESTOR.getCode()) ){
				userAccountBean.setIdParticipantFk(institutionEntity.getIdParticipante());
			}else if(userAccountBean.getInstitutionTypeFk().equals(InstitutionType.ISSUER.getCode()) || 
					userAccountBean.getInstitutionTypeFk().equals(InstitutionType.ISSUER_DPF.getCode()) ){
				userAccountBean.setIdIssuerFk(institutionEntity.getIdIssuer());
			}
	
			List<UserProfile> listUserProfiles=new ArrayList<UserProfile>();
			UserProfile userProfile;
			SystemProfile systemProfile;
			NodeBean nodeBean;
			if(nodesSystemProfilesSelected!=null){				
				for(TreeNode node:nodesSystemProfilesSelected){
					nodeBean=(NodeBean)node.getData();
					if(nodeBean.getLevel().intValue()==2)	{
						userProfile=new UserProfile();
						systemProfile=new SystemProfile();
						
						systemProfile.setIdSystemProfilePk(nodeBean.getCode());
						
						userProfile.setSystemProfile( systemProfile );
						userProfile.setState(UserProfileStateType.REGISTERED.getCode());
						userProfile.setUserAccount(userAccountBean);
						
						userProfile.setRegistryDate(new Date());
						userProfile.setRegistryUser(userInfo.getUserAccountSession().getUserName());
						listUserProfiles.add(userProfile);
					}
				}
			}
			userAccountBean.setUserProfiles(listUserProfiles);
			if(showProfilesExtern){
				//Save allowed profiles for external user
				List<UserProfileAllowed> profilesAllowed = new ArrayList<>();
				UserProfileAllowed userProfileAllowed;
				if(nodesExternsProfilesSelected!=null){
					for(TreeNode node:nodesExternsProfilesSelected){
						nodeBean=(NodeBean)node.getData();
						if(nodeBean.getLevel().intValue()==2)	{
							userProfileAllowed=new UserProfileAllowed();
							systemProfile=new SystemProfile();
							systemProfile.setIdSystemProfilePk(nodeBean.getCode());
							userProfileAllowed.setSystemProfile( systemProfile );
							userProfileAllowed.setProfileState(UserProfileStateType.REGISTERED.getCode());
							userProfileAllowed.setUserAccount(userAccountBean);
							userProfileAllowed.setRegistryDate(CommonsUtilities.currentDateTime());
							userProfileAllowed.setRegistryUser(userInfo.getUserAccountSession().getUserName());
							profilesAllowed.add(userProfileAllowed);
						}
					}
				}
				userAccountBean.setUserProfilesAllowed(profilesAllowed);
			}
			List<InstitutionTypeDetail> listInstitutionTypeDetail=new ArrayList<InstitutionTypeDetail>();
			InstitutionTypeDetail institutionTypeDetail;
			if(userAccountBean.isRegulatorAgentBool()){	
					for(String institutionType : lstInstitutionsTypeSelected){
					institutionTypeDetail=new InstitutionTypeDetail();
					institutionTypeDetail.setUserAccount(userAccountBean);
					institutionTypeDetail.setIdInstitutionTypeFk(Integer.parseInt( institutionType ));
													
					institutionTypeDetail.setState(InstitutionTypeDetailStateType.REGISTERED.getCode());
					institutionTypeDetail.setRegistryUser(userInfo.getUserAccountSession().getUserName());
					institutionTypeDetail.setRegistryDate(new Date());
					
					listInstitutionTypeDetail.add(institutionTypeDetail);
				}
			}
			userAccountBean.setInstitutionTypeDetails(listInstitutionTypeDetail);
			
			if(userAccountBean.getSubsidiary() == null || 
					Validations.validateIsNullOrEmpty(userAccountBean.getSubsidiary().getIdSubsidiaryPk())){
				userAccountBean.setSubsidiary(null);
			}
			
			userServiceFacade.registerUserServiceFacade(userAccountBean);
			if(userAccountBean.getSubsidiary() == null){
				userAccountBean.setSubsidiary(new Subsidiary());
			}
			argObj[0]=userAccountBean.getLoginUser();
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_SUCCESS,null,
					PropertiesConstants.USER_REGISTER_SUCCESS,argObj);
			cleanUsers();
			
		}catch(Exception e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Modify.
	 *
	 * This method is called if the operation type is "modify"
	 */
	public void modify(){
		try{
			Boolean modifyUser=false;
			Object[] argObj=new Object[1];
			int error=validationMgmtUserAccount();
			if(error>0){
				return;
			}
			//check concurrency update
			UserAccount userTemp=userServiceFacade.getUserAccountByLogin(userAccountBean);
			if(userTemp != null && userTemp.getLastModifyDate().after(userAccountBean.getLastModifyDate())){
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, "user.error.reject.recent", null);
				cleanUsers();
				return;
			}
			
			userAccountBean.setIdParticipantFk( null );
			userAccountBean.setIdIssuerFk( null );
			//Get code entity
			int indexEntity = lstInstitutionsByInstitutionType.indexOf(userAccountBean.getInstitutionsSecurity());
			InstitutionsSecurity institutionEntity = lstInstitutionsByInstitutionType.get(indexEntity);
			if(userAccountBean.getInstitutionTypeFk().equals(InstitutionType.PARTICIPANT.getCode()) ||
					userAccountBean.getInstitutionTypeFk().equals(InstitutionType.PARTICIPANT_INVESTOR.getCode())){
				userAccountBean.setIdParticipantFk(institutionEntity.getIdParticipante());
			}else if(userAccountBean.getInstitutionTypeFk().equals(InstitutionType.ISSUER.getCode()) ||
					userAccountBean.getInstitutionTypeFk().equals(InstitutionType.ISSUER_DPF.getCode())){
				userAccountBean.setIdIssuerFk(institutionEntity.getIdIssuer());
			}
			UserProfile userProfile = null;
			SystemProfile systemProfile = null;
			NodeBean nodeBean =null;
			documentType = null;
			documentNumber = null;
			for(UserProfile userProf : lstProfilesAssignedToUser){
				if(!isNodeProfileSelected(nodesSystemProfilesSelected,userProf.getSystemProfile().getIdSystemProfilePk())){
					userProf.setState(UserProfileStateType.DELETED.getCode());
					userProf.setUserAccount(userAccountBean);
					modifyUser=true;
				}
			}
			if(nodesSystemProfilesSelected!=null){								
				for(TreeNode node:nodesSystemProfilesSelected){
					nodeBean=(NodeBean)node.getData();
					if(nodeBean.getLevel()==2){
						if(!assignedProfile(nodeBean.getCode())){
							userProfile=new UserProfile();
							systemProfile=new SystemProfile();
							
							systemProfile.setIdSystemProfilePk(nodeBean.getCode());
							
							userProfile.setRegistryDate(new Date());
							userProfile.setRegistryUser(userInfo.getUserAccountSession().getUserName());
							userProfile.setSystemProfile( systemProfile );
							userProfile.setState(UserProfileStateType.REGISTERED.getCode());
							
							userProfile.setUserAccount(userAccountBean);
							lstProfilesAssignedToUser.add(userProfile);
						}
					}
				}
			}
			userAccountBean.setUserProfiles(lstProfilesAssignedToUser);
			if(userInfo.getUserAccountSession().isDepositaryInstitution()){
				if(showProfilesExtern){
					if(Validations.validateIsNotNullAndNotEmpty(profilesAllowedUser)){
						for(UserProfileAllowed profileAllowed : profilesAllowedUser){							
							if(!isNodeProfileSelected(nodesExternsProfilesSelected,profileAllowed.getSystemProfile().getIdSystemProfilePk())){
								profileAllowed.setProfileState(UserProfileStateType.DELETED.getCode());
								profileAllowed.setUserAccount(userAccountBean);
								modifyUser=true;
							}														
						}
					}					
					if(nodesExternsProfilesSelected != null){
						if(profilesAllowedUser == null){
							profilesAllowedUser = new ArrayList<>();
						}
						UserProfileAllowed profileAllowed;
						for(TreeNode node:nodesExternsProfilesSelected){
							nodeBean=(NodeBean)node.getData();
							if(nodeBean.getLevel()==2){
								if(!assignedProfileExtern(nodeBean.getCode())){
									profileAllowed=new UserProfileAllowed();
									systemProfile=new SystemProfile();
									systemProfile.setIdSystemProfilePk(nodeBean.getCode());
									profileAllowed.setRegistryDate(new Date());
									profileAllowed.setRegistryUser(userInfo.getUserAccountSession().getUserName());
									profileAllowed.setSystemProfile( systemProfile );
									profileAllowed.setProfileState(UserProfileStateType.REGISTERED.getCode());
									profileAllowed.setUserAccount(userAccountBean);
									profilesAllowedUser.add(profileAllowed);
								}
							}
						}
					}
				} else {		
					if(!blIsRecorder){	
						if(Validations.validateIsNotNullAndNotEmpty(profilesAllowedUser)){
							for(UserProfileAllowed profileAllowed : profilesAllowedUser){							
								if(isNodeProfileSelected(nodesExternsProfilesSelected,profileAllowed.getSystemProfile().getIdSystemProfilePk())){
									profileAllowed.setProfileState(UserProfileStateType.DELETED.getCode());
									profileAllowed.setUserAccount(userAccountBean);
									modifyUser=true;
								}														
							}
						}
					}									
				}
				userAccountBean.setUserProfilesAllowed(profilesAllowedUser);
			}
			
			InstitutionTypeDetail institutionTypeDet;
			List<InstitutionTypeDetail> listInstitutionsTypeDetailAssignedAux=new ArrayList<InstitutionTypeDetail>();
			listInstitutionsTypeDetailAssignedAux=listInstitutionTypeDetailAssignedToUser;
			if(userAccountBean.isRegulatorAgentBool()){
				for(InstitutionTypeDetail insTypeDet : listInstitutionsTypeDetailAssignedAux){
					if(!institutionTypeDetailChecked(insTypeDet.getIdInstitutionTypeFk().intValue())){
						insTypeDet.setState(UserProfileStateType.DELETED.getCode());
					}
				}
				if(Validations.validateIsNotNullAndNotEmpty(lstInstitutionsTypeSelected)){
					for(String tipoIns : lstInstitutionsTypeSelected){
						if(!institutionTypeDetailAssigned(Integer.parseInt( tipoIns ))){
							institutionTypeDet=new InstitutionTypeDetail();
							institutionTypeDet.setUserAccount(userAccountBean);
							institutionTypeDet.setIdInstitutionTypeFk(Integer.parseInt( tipoIns ));							
							institutionTypeDet.setRegistryUser(userInfo.getUserAccountSession().getUserName());
							institutionTypeDet.setRegistryDate(new Date());
							institutionTypeDet.setState(InstitutionTypeDetailStateType.REGISTERED.getCode());							
							listInstitutionsTypeDetailAssignedAux.add(institutionTypeDet);
						}
					}
				}
			}else{
	
				if(listInstitutionTypeDetailAssignedToUser!=null && !listInstitutionTypeDetailAssignedToUser.isEmpty()){
					for(InstitutionTypeDetail detTipoIns : listInstitutionTypeDetailAssignedToUser){
							detTipoIns.setState(UserProfileStateType.DELETED.getCode());
					}
				}
			}
			userAccountBean.setInstitutionTypeDetails(listInstitutionsTypeDetailAssignedAux);
			if(blIsRecorder==true){
				userAccountBean.setIndIsRecorder(BooleanType.YES.getCode());
			}else{
				userAccountBean.setIndIsRecorder(BooleanType.NO.getCode());
			}
			if(blIsCommitter==true){
				userAccountBean.setIndIsCommitter(BooleanType.YES.getCode());
			}else{
				userAccountBean.setIndIsCommitter(BooleanType.NO.getCode());
			}
			
			if(Validations.validateIsNullOrEmpty(userAccountBean.getSubsidiary().getIdSubsidiaryPk())){
				userAccountBean.setSubsidiary(null);
			}
			
			userServiceFacade.modifyUserServiceFacade(userAccountBean);
			
			if(userAccountBean.getSubsidiary() == null){
				userAccountBean.setSubsidiary(new Subsidiary());
			}
			
			//LDAP comments
			if(this.ldapUtilities.isEnableAuthentication()) {
				try{
					ldapUtilities.updateUserDetails(userAccountBean);
				}catch(Exception e){
					log.info(e.getMessage());
				}
			}
			
			if(modifyUser){
				log.info("login user:--"+userAccountBean.getLoginUser());
				//PushContext pushContext = PushContextFactory.getDefault().getPushContext();
				//integration with notifications
				BrowserWindow browser = new BrowserWindow();
				browser.setNotificationType(NotificationType.KILLSESSION.getCode());
				browser.setSubject("Expire Session");
				browser.setMessage("Expire Session user "+userAccountBean.getLoginUser());
				//pushContext.push(PropertiesConstants.PUSH_MSG+"/"+userAccountBean.getLoginUser(),browser);
			}
			
			argObj[0]=userAccountBean.getLoginUser();
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_SUCCESS,null,
					PropertiesConstants.USER_MODIFY_SUCCESS,argObj);
	        cleanUsers();
        }catch(Exception e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Load profile tree.
	 * This method load the profiles tree
	 */
	public void loadProfileTree(){
		try{
			System sis=new System(); 
			sis.setState(SystemStateType.CONFIRMED.getCode());
			List<System> lstSystem=systemServiceFacade.searchSystemByStateServiceFacade(SystemStateType.CONFIRMED.getCode());
			//if is idepositary load all profiles but if is extern only allowed profiles
			if(userInfo.getUserAccountSession().isDepositaryInstitution()){
				SystemProfileFilter sp=new SystemProfileFilter();	
				sp.setProfileStateBaseQuery(SystemProfileStateType.CONFIRMED.getCode());
				if(userAccountBean.getType().equals(UserAccountType.INTERNAL.getCode()) ){
					sp.setIndUserType(BooleanType.NO.getCode());
				} else {
					sp.setIndUserType(BooleanType.YES.getCode());
				}
				lstSystemProfile = systemServiceFacade.searchProfilesByFiltersServiceFacade(sp);
				createTreeProfiles(lstSystem, lstSystemProfile);
				if(showProfilesExtern){
					//load profiles allowed only user externs
					createTreeProfilesExtern(lstSystem, lstSystemProfile);
				} else{
					//clean selected profiles
					treeExternsProfiles = null;
					//nodesExternsProfilesSelected = null;
				}
			} else {
				/* For user externs load profiles allowed in register button click
				 * 
				 * */
				createTreeProfiles(lstSystem, lstSystemProfile);
			}
			
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Load tree assigned profiles.
	 * 
	 * This method load only the system profiles assigned to the selected user
	 */
	public void loadTreeAssignedProfiles(){
		try{
			SystemProfileFilter psf=new SystemProfileFilter();
			psf.setProfileStateBaseQuery(SystemProfileStateType.CONFIRMED.getCode());
		
			List<System> lstSystems=systemServiceFacade.searchSystemByStateServiceFacade(SystemStateType.CONFIRMED.getCode());
			lstSystemProfile=systemServiceFacade.searchProfilesByFiltersServiceFacade(psf);
	
			List<System> lstSystemAux=new ArrayList<System>();
			List<SystemProfile> lstSystemProfileAux=new ArrayList<SystemProfile>();
			
			//fill the system profile list from user profile list (lstProfilesAssignedToUser)
			for(UserProfile userProfile : lstProfilesAssignedToUser){
				for(SystemProfile sp : lstSystemProfile){
					if(userProfile.getSystemProfile().getIdSystemProfilePk().intValue()==sp.getIdSystemProfilePk().intValue()){
						if(!lstSystemProfileAux.contains(sp)){
							lstSystemProfileAux.add(sp);
							break;
						}
					}
				}
			}
			//fill the systems list assigned to the selected user 
			for(SystemProfile sp : lstSystemProfileAux){
				for(System sis : lstSystems){
					if(sp.getSystem().getIdSystemPk().intValue()==sis.getIdSystemPk().intValue()){
						if(!lstSystemAux.contains(sis)){
							lstSystemAux.add(sis);
							break;
						}
					}
				}
			}
			createTreeProfiles(lstSystemAux, lstSystemProfileAux);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Create tree profiles.
	 *
	 * This method create the profiles tree
	 * First create system nodes and then create the system profile node assigned to the system 
	 *  
	 * @param lstSystems system list
	 * @param lstSystemProfiles system profile list
	 */
	public void createTreeProfiles(List<System> lstSystems,List<SystemProfile> lstSystemProfiles){
		treeProfilesRoot=new DefaultTreeNode("Root",null);
		
		TreeNode systemsNode;
		NodeBean node;
		
		for(System system:lstSystems){
			   node=new NodeBean();	
			   node.setCode(system.getIdSystemPk());
			   node.setName(system.getName());
			   node.setLevel(1);
			   systemsNode=new DefaultTreeNode(node,treeProfilesRoot);
			   systemsNode.setSelectable(false);
			   createNodeProfileBySystem(system.getIdSystemPk(), lstSystemProfiles, systemsNode, 2);
		}
		
	}
	
	/**
	 * Creates the tree profiles extern.
	 *
	 * @param lstSystems the lst systems
	 * @param lstSystemProfiles the lst system profiles
	 */
	public void createTreeProfilesExtern(List<System> lstSystems,List<SystemProfile> lstSystemProfiles){
		treeExternsProfiles=new DefaultTreeNode("Root",null);
		TreeNode systemsNode;
		NodeBean node;
		for(System system:lstSystems){
			   node=new NodeBean();	
			   node.setCode(system.getIdSystemPk());
			   node.setName(system.getName());
			   node.setLevel(1);
			   systemsNode=new DefaultTreeNode(node,treeExternsProfiles);
			   systemsNode.setSelectable(false);
			   createNodeProfileBySystemExtern(system.getIdSystemPk(), lstSystemProfiles, systemsNode, 2);
		}
		
	}
	
	/**
	 * Creates the node profile by system extern.
	 *
	 * @param idSystem the id system
	 * @param lstSystemProfiles the lst system profiles
	 * @param parentNode the parent node
	 * @param level the level
	 */
	public void createNodeProfileBySystemExtern(int idSystem,List<SystemProfile> lstSystemProfiles,TreeNode parentNode, int level){
			TreeNode profileNode;
			NodeBean node;
			for(SystemProfile sisProf:lstSystemProfiles){
				if(sisProf.getSystem().getIdSystemPk().intValue()==idSystem){
					node=new NodeBean();
					node.setCode(sisProf.getIdSystemProfilePk());
					node.setName(sisProf.getName());
					node.setLevel(level);
					profileNode=new DefaultTreeNode(node,parentNode);
					profileNode.setSelectable(true);								 
					if(isModify() && assignedProfileExtern(sisProf.getIdSystemProfilePk().intValue())){
						profileNode.setSelected(true);
						profileNode.getParent().setExpanded(true);
						profileNode.getParent().setSelected(true);
					}else if(isDetail()){
						profileNode.getParent().setExpanded(true);
					}
				}	
			}
		}
	
	/**
	 * Create node profile by system.
	 * This method create the system profile node according with a system
	 *
	 * @param idSystem the system id of the system to be loaded the system profiles
	 * @param lstSystemProfiles this list contains the system profiles
	 * @param parentNode the node of the system to be loaded the system profiles
	 * @param level the level of the node
	 */
	public void createNodeProfileBySystem(int idSystem,List<SystemProfile> lstSystemProfiles,TreeNode parentNode, int level){
		
		TreeNode profileNode;
		NodeBean node;
		
		for(SystemProfile sisProf:lstSystemProfiles){
			if(sisProf.getSystem().getIdSystemPk().intValue()==idSystem){
				node=new NodeBean();
				node.setCode(sisProf.getIdSystemProfilePk());
				node.setName(sisProf.getName());
				node.setLevel(level);
				profileNode=new DefaultTreeNode(node,parentNode);
				profileNode.setSelectable(true);								 
				if(isModify() && assignedProfile(sisProf.getIdSystemProfilePk().intValue())){
					profileNode.setSelected(true);
					profileNode.getParent().setExpanded(true);
					profileNode.getParent().setSelected(true);
				
				}else if(isDetail()){
					profileNode.getParent().setExpanded(true);
				}
			}	
		}
	}
	
	/**
	 * Assigned profile.
	 * This method checks if the system profile is assigned to the selected user
	 *
	 * @param idProfile the id profile
	 * @return true, if successful
	 */
	public boolean assignedProfile(int idProfile){
		for(UserProfile perAsi : lstProfilesAssignedToUser){
			if(idProfile == perAsi.getSystemProfile().getIdSystemProfilePk().intValue()){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Assigned profile extern.
	 *
	 * @param idProfile the id profile
	 * @return true, if successful
	 */
	public boolean assignedProfileExtern(int idProfile){
		if(profilesAllowedUser != null && profilesAllowedUser.size() >  0){
			for(UserProfileAllowed perAsi : profilesAllowedUser){
				if(idProfile == perAsi.getSystemProfile().getIdSystemProfilePk().intValue()){
					return true;
				}
			}
		}
		return false;
	}
	

	/**
	 * Before user confirmation listener.
	 * 
	 * This method is called before confirming a user
	 * If validation is successful show the confirm dialog else show validation dialog
	 *
	 * @param event the event
	 */
	public void beforeUserConfirmationListener(ActionEvent event){
		try {
		  	JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
			//hideDialogs();
			if(selectedUserAccountBean==null){
				showMessageOnDialog(
						PropertiesConstants.USER_CONFIRM_HEARDERNORECSELECT,null,
						PropertiesConstants.USER_ERROR_REGISTER_NO_SELECTED, null);
				JSFUtilities.showComponent("cnfMsgCustomValidation");
			} else if(selectedUserAccountBean.getState().intValue()!=UserAccountStateType.REGISTERED.getCode().intValue()){
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT,null,
						PropertiesConstants.USER_ERROR_CONFIRM, null);
				JSFUtilities.showComponent("cnfMsgCustomValidation");
			}else{
				showMessageOnDialog(PropertiesConstants.LBL_CONFIRM_HEADER_CONFIRM, null, null, null);
				setOperationType(ViewOperationsType.CONFIRM.getCode());
				//cleanAndShowDlgMotive();
				JSFUtilities.executeJavascriptFunction("PF('cnfwConfirmUser').show()");
				//JSFUtilities.showComponent("frmConfirm:cnfConfirmUser");
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Before user rejection listener.
	 * 
	 * This method is called before rejecting a user
	 * If validation is successful show the confirm dialog else show validation dialog
	 *
	 * @param event the event
	 */
	public void beforeUserRejectionListener(ActionEvent event){
		try {
			JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
			//hideDialogs();
			isConfirmBlockOrUnblock = false;
			isRejectOption = false;
			isUnregisterEvent = false;
			//showMotives();
			lstMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.USER_REJECT_MOTIVES.getCode());
			if(selectedUserAccountBean==null){
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT,null,
						PropertiesConstants.USER_ERROR_REGISTER_NO_SELECTED, null);
				JSFUtilities.showComponent("cnfMsgCustomValidation");
			}else if(selectedUserAccountBean.getState().intValue()!=UserAccountStateType.REGISTERED.getCode().intValue()){
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT,null,
						PropertiesConstants.USER_ERROR_REJECT, null);
				JSFUtilities.showComponent("cnfMsgCustomValidation");
			}else{
				showMessageOnDialog(PropertiesConstants.LBL_MOTIVE_REJECT, null, null, null);
				setOperationType(ViewOperationsType.REJECT.getCode());
				cleanAndShowDlgMotive();
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Before user unregister listener.
	 *
	 * @param actionEvent the action event
	 */
	public void beforeUserUnregisterListener(ActionEvent actionEvent){
		userAccountBean.setfDisplayName(null);
		try {
			JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
			//hideDialogs();
			isConfirmBlockOrUnblock = false;
			isRejectOption = false;
			isUnregisterEvent = false;
			
			lstMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.USER_UNREGISTER_MOTIVES.getCode());
			if(selectedUserAccountBean==null){
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT,null,
						PropertiesConstants.USER_ERROR_REGISTER_NO_SELECTED, null);
				JSFUtilities.showComponent("cnfMsgCustomValidation");
			}else if(!(selectedUserAccountBean.getState().equals(UserAccountStateType.CONFIRMED.getCode()) ||
					selectedUserAccountBean.getState().equals(UserAccountStateType.BLOCKED.getCode()))){
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT,null,
						PropertiesConstants.USER_UNREGISTER_ERROR, null);
				JSFUtilities.showComponent("cnfMsgCustomValidation");
			}else{
				setOperationType(ViewOperationsType.ANULATE.getCode());
				showMessageOnDialog(PropertiesConstants.LBL_MOTIVE_UNREGISTER, null, null, null);
				isUnregisterEvent = true;
				removeDocumentFile();
				if(selectedUserAccountBean.getSituation() != null &&
					selectedUserAccountBean.getSituation().equals(UserAccountSituationType.PENDING_UNREGISTER.getCode())){
					selectedUserAccountBean.setUnregisterFile(userServiceFacade.getUserDocumentUnregister(selectedUserAccountBean.getIdUserPk()));
					isConfirmBlockOrUnblock = true;
				}
				cleanAndShowDlgMotive();				
			}

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Before user block listener.
	 *
	 * This method is called before blocking a user
	 * If validation is successful show the confirm dialog else show validation dialog
	 * @param event the event
	 */
	public void beforeUserBlockListener(ActionEvent event){
		try {
			JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
			//hideDialogs();
			isConfirmBlockOrUnblock = false;
			isRejectOption = false;
			isUnregisterEvent = false;
			
			lstMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.BLOCK_MOTIVES.getCode());
			if(selectedUserAccountBean==null){
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT,null,
						PropertiesConstants.USER_ERROR_REGISTER_NO_SELECTED, null);
				JSFUtilities.showComponent("cnfMsgCustomValidation");
			}else if(selectedUserAccountBean.getState().intValue()!=UserAccountStateType.CONFIRMED.getCode().intValue()){
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT,null,
						PropertiesConstants.USER_ERROR_BLOCK, null);
				JSFUtilities.showComponent("cnfMsgCustomValidation");
			}else{
				setOperationType(ViewOperationsType.BLOCK.getCode());
				showMessageOnDialog(PropertiesConstants.LBL_MOTIVE_BLOCK, null, null, null);				
				if(selectedUserAccountBean.getState().equals(UserAccountStateType.CONFIRMED.getCode()) && 
					selectedUserAccountBean.getSituation().equals(UserAccountSituationType.PENDING_LOCK.getCode())){
					isConfirmBlockOrUnblock = true;
				}
				cleanAndShowDlgMotive();				
			}

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Before user unblock listener.
	 *
	 * This method is called before unblocking a user
	 * If validation is successful show the confirm dialog else show validation dialog
	 * @param event the event
	 */
	public void beforeUserUnblockListener(ActionEvent event){
		try {
			JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
			//hideDialogs();
			isConfirmBlockOrUnblock = false;
			isRejectOption = false;
			isUnregisterEvent = false;
			
			lstMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.DESBLOCK_MOTIVES.getCode());
			if(selectedUserAccountBean==null){
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT, null,
						PropertiesConstants.USER_ERROR_REGISTER_NO_SELECTED,  null);
				JSFUtilities.showComponent("cnfMsgCustomValidation");
			}else if(selectedUserAccountBean.getState().intValue()!=UserAccountStateType.BLOCKED.getCode()){
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT, null,
						PropertiesConstants.USER_ERROR_UNBLOCK_STATE_NO_BLOCK,  null);
				JSFUtilities.showComponent("cnfMsgCustomValidation");
			}else{
				setOperationType(ViewOperationsType.UNBLOCK.getCode());
				
				if(selectedUserAccountBean.getState().equals(UserAccountStateType.BLOCKED.getCode()) &&
						selectedUserAccountBean.getSituation().equals(UserAccountSituationType.PENDING_UNLOCK.getCode())){
					isConfirmBlockOrUnblock = true;
				}
				cleanAndShowDlgMotive();			
				
				showMessageOnDialog(PropertiesConstants.LBL_MOTIVE_UNBLOCK, null, null, null);
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clean and show dlg motive.
	 * This method clean the motive dialog
	 *
	 */
	public void cleanAndShowDlgMotive(){
		historicalState=new HistoricalState();
		historicalState.setMotive(Integer.valueOf(-1));		
		showOtherMotive=false;
		JSFUtilities.setValidViewComponent("frmMotive:cboMotive");
		JSFUtilities.executeJavascriptFunction("PF('dlgwMotive').show()");
		
		if(isBlock()){
			if(isConfirmBlockOrUnblock){
				historicalState.setMotive(selectedUserAccountBean.getBlockMotive());
				if(OtherMotiveType.USER_BLOCK_OTHER_MOTIVE.getCode().equals(selectedUserAccountBean.getBlockMotive())){
					showOtherMotive = true;
					historicalState.setMotiveDescription(selectedUserAccountBean.getBlockOtherMotive());
				}
			}
			return;
		}
		if(isUnBlock()){
			if(isConfirmBlockOrUnblock){
				historicalState.setMotive(selectedUserAccountBean.getUnblockMotive());
				if(OtherMotiveType.USER_UNBLOCK_OTHER_MOTIVE.getCode().equals(selectedUserAccountBean.getBlockMotive())){
					showOtherMotive = true;
					historicalState.setMotiveDescription(selectedUserAccountBean.getUnblockOtherMotive());
				}
			}
			return;
		}
		if(isAnnular()){
			if(isConfirmBlockOrUnblock){
				historicalState.setMotive(selectedUserAccountBean.getAnnularMotive());
				if(OtherMotiveType.USER_ANNULAR_OTHER_MOTIVE.getCode().equals(selectedUserAccountBean.getAnnularMotive())){
					showOtherMotive = true;
					historicalState.setMotiveDescription(selectedUserAccountBean.getAnnularOtherMotive());
				}
			}
			return;
		}
	}
	
	/**
	 * Before password reboot listener.
	 * 
	 * This method is called before rebooting the password of the selected user
	 *
	 * @param event the event
	 */
	public void beforePasswordRebootListener(ActionEvent event){
		try {
			JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
			hideDialogs();
			JSFUtilities.showComponent("frmUserAdministration:cnfRebootPassword");
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Before password reboot listener.
	 * 
	 * This method is called before rebooting the password of the selected user
	 *
	 * @param event the event
	 */
	public void beforeMfaRebootListener(ActionEvent event){
		try {
			JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
			//hideDialogs();
			//JSFUtilities.showComponent("frmUserAdministration:cnfRebootPassword");
			JSFUtilities.executeJavascriptFunction("PF('cnfwRebootMfa').show()");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	/**
	 * Password reboot listener.
	 * 
	 * This method reboot the password of the selected user
	 */
	@LoggerAuditWeb
	public void passwordRebootListener(){
		try {
		//	String password = UUID.randomUUID().toString().replaceAll("-", "").substring(0, LDAPUtilities.getPwdMinLength()+1);
			String password = SecurityUtilities.generatePassword().substring(0, ldapUtilities.getPwdMinLength());
			JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
			hideDialogs();
			Object[] argObj=new Object[]{userAccountBean.getLoginUser()};
			showMessageOnDialog(
					 PropertiesConstants.DIALOG_HEADER_SUCCESS,null,
					PropertiesConstants.USER_RESET_PASSWORD_SUCCESS,
					argObj);
			UserAccount userAccount=new UserAccount();
			userAccount.setIdUserPk( userAccountBean.getIdUserPk());
			userAccount.setLoginUser( userAccount.getLoginUser() );
			userAccount.setChangePassword( BooleanType.YES.getCode() );
			userServiceFacade.updateChangePasswordServiceFacade(userAccountBean,password);
			JSFUtilities.showComponent("cnfEndTransaction");
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	@LoggerAuditWeb
	public void mfaRebootListener(){
		try {
			Object[] argObj=new Object[]{userAccountBean.getLoginUser()};
			showMessageOnDialog(
					 PropertiesConstants.DIALOG_HEADER_SUCCESS,null,
					PropertiesConstants.USER_RESET_MFA_SUCCESS,
					argObj);
			UserAccount userAccount=new UserAccount();
			userAccount.setIdUserPk( userAccountBean.getIdUserPk());
			userAccount.setLoginUser( userAccount.getLoginUser() );
			userServiceFacade.resetMfaServiceFacade(userAccountBean);
			JSFUtilities.showComponent("cnfEndTransaction");
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Action motive listener.
	 *
	 * This method is called when confirm the motive of the operation
	 * @param event the event
	 */
	public void actionMotiveListener(ActionEvent event){
		try {
			JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
			
			if(showOtherMotive==true){
				if (Validations.validateIsNullOrEmpty(this.getHistoricalState().getMotiveDescription())||
						this.getHistoricalState().getMotiveDescription().trim().length() == 0) {
					this.getHistoricalState().setMotiveDescription("");
					hideConfBlock();
					JSFUtilities.showComponent("cnfMsgCustomValidation");					
					showMessageOnDialog(
							PropertiesConstants.DIALOG_HEADER_ALERT, null,
							PropertiesConstants.ERROR_MESSAGE_MOTIVE_REQUIRED,null);
					String errorMessage=PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_MOTIVE_REQUIRED);
					JSFUtilities.addContextMessage("frmMotive:txtOtherMotive",FacesMessage.SEVERITY_ERROR, errorMessage,errorMessage);
					return;
				}
			}

			//File optional in annular
//			if(isAnnular() && selectedUserAccountBean.getUnregisterFile() == null){
//				hideConfBlock();
//				JSFUtilities.showComponent("cnfMsgCustomValidation");					
//				showMessageOnDialog(
//						PropertiesConstants.DIALOG_HEADER_ALERT, null,
//						PropertiesConstants.FUPL_NO_SELECTED,null);
//				String errorMessage=PropertiesUtilities.getMessage(PropertiesConstants.FUPL_NO_SELECTED);
//				JSFUtilities.addContextMessage("frmMotive:fuplformUser",FacesMessage.SEVERITY_ERROR, errorMessage,errorMessage);
//				return;
//			}
			
			//hideDialogs();
			//JSFUtilities.showComponent("frmMotive:dlgMotive");
			if(isBlock()){
				//JSFUtilities.showComponent("frmBlockUser:cnfBlock");
				JSFUtilities.executeJavascriptFunction("PF('cnfwBlock').show()");
			}else if(isUnBlock()){
				isRejectOption = false;
				//JSFUtilities.showComponent("frmUnblockUser:cnfUnblock");
				JSFUtilities.executeJavascriptFunction("PF('cnfwUnblock').show()");
			}else if(isReject()){
				//JSFUtilities.showComponent("frmRejectUser:cnfRejectUser");
				JSFUtilities.executeJavascriptFunction("PF('cnfwRejectUser').show()");
			}else if(isAnnular()){
				//JSFUtilities.showComponent("frmUnregisterUser:cnfUnregister");
				JSFUtilities.executeJavascriptFunction("PF('cnfWUnregister').show()");
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Back motive listener.
	 *
	 * @param event the event
	 */
	public void backMotiveListener(ActionEvent event){
		hideDialogs();
		removeDocumentFile();
	}
	
	/**
	 * Action reject motive listener.
	 *
	 * @param event the event
	 */
	public void actionRejectMotiveListener(ActionEvent event){
		try {
			JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
			hideDialogs();
			JSFUtilities.showComponent("frmMotive:dlgMotive");
			if(isBlock()){
				isRejectOption = true;
				JSFUtilities.showComponent("frmBlockUser:cnfBlock");
			}else if(isUnBlock()){
				isRejectOption = true;
				JSFUtilities.showComponent("frmUnblockUser:cnfUnblock");
			}else if(isAnnular()){
				isRejectOption = true;
				JSFUtilities.showComponent("frmUnregisterUser:cnfUnregister");
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Gets the lst user types.
	 *
	 * @return the lst user types
	 */
	public List<UserAccountType> getLstUserTypes(){
		return UserAccountType.list;
	}
	
	/**
	 * Load institutions by user type listener.
	 */
	public void loadInstitutionsByUserTypeListener(){
		try {
			showInstitutionsByUserAccountType(userAccountFilter.getUserAccountType().intValue());
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change user type listener.
	 * 
	 * This method is called when change the user type in the view
	 */
	public void changeUserTypeListener(){
		blDisabledExtern=false;
		InstitutionsSecurity institutionSecurity=new InstitutionsSecurity();
		institutionSecurity.setIdInstitutionSecurityPk(-1);
		userAccountBean.setInstitutionTypeFk(-1);
		userAccountBean.setInstitutionsSecurity( institutionSecurity );
		lstInstitutionsByInstitutionType=new ArrayList<InstitutionsSecurity>();		
		//refresh in change combo
		showTreePanelExterns();
		if(userAccountBean.getType().intValue()==-1 || 
				userAccountBean.getType().intValue()==UserAccountType.INTERNAL.getCode().intValue()){
			userAccountBean.setRegulatorAgentBool(false);
			lstInstitutionsTypeSelected=new ArrayList<String>();
		}
		if(userAccountBean.getType().intValue()>0){
			showInstitutionsByUserAccountType(userAccountBean.getType());
			lstInstitutionTypeForFilter=new ArrayList<Parameter>();
			lstInstitutionTypeForFilter.addAll(lstInstitutionTypeForFilterTemp);
			if(UserAccountType.INTERNAL.getCode().equals(userAccountBean.getType())){
				//commented by Hema
				lstInstitutionType=InstitutionType.listInternalInstitutions;		
				for(Parameter param:lstInstitutionTypeForFilterTemp){
					if(!InstitutionType.DEPOSITARY.getCode().equals(param.getIdParameterPk())){
						lstInstitutionTypeForFilter.remove(param);
					}
				}			
				for(int i=0;i<lstInstitutionTypeForFilter.size();i++){
					userAccountBean.setInstitutionTypeFk(lstInstitutionTypeForFilter.get(i).getIdParameterPk());
				}
				//userAccountBean.setMacRestrictionBool(true);
				this.getUserAccountBean().setMacAddress("");
				changeInstitutionTypeListener();
				createLoginUserListener(true);
				blDisabledExtern=true;
			}else{
				for(Parameter param:lstInstitutionTypeForFilterTemp){
					if(InstitutionType.DEPOSITARY.getCode().equals(param.getIdParameterPk())){
						lstInstitutionTypeForFilter.remove(param);
					}
				}
				//Commented by Hema
				lstInstitutionType=InstitutionType.listExternalInstitutions;
				userAccountBean.setMacRestrictionBool(false);
				userAccountBean.setLoginUser(null);
				if(Validations.validateIsNotNullAndPositive(userAccountBean.getInstitutionTypeFk())){
					changeInstitutionTypeListener();
					loadProfileTree();
				}else{
					lstSystemProfile = null;
				}
			}
		}else{
			userAccountBean.setInstitutionTypeFk(-1);
			userAccountBean.setLoginUser(null);
			lstSystemProfile = null;
		}
		userAccountBean.setSubsidiary(new Subsidiary());
		lstSubsidiary = null;
		subsidiaryFilter = new SubsidiaryFilter();
	}
	
	/**
	 * Change institution type listener.
	 */
	public void changeInstitutionTypeListener(){
		selectSubsidiary = false;
		try {
			InstitutionsSecurity institutionSecurity=new InstitutionsSecurity();
			institutionSecurity.setIdInstitutionSecurityPk(-1);							
			
			if(!Validations.validateIsNotNullAndPositive(userAccountBean.getInstitutionTypeFk())){
				userAccountBean.setInstitutionsSecurity(institutionSecurity);
			}
	
			if(userAccountBean.getInstitutionTypeFk()>0){
				InstitutionSecurityFilter insSecurityFilter=new InstitutionSecurityFilter();
				
				if(Validations.validateIsNotNullAndNotEmpty(userAccountBean.getInstitutionTypeFk())){
					if(InstitutionType.ISSUER_DPF.getCode().equals(userAccountBean.getInstitutionTypeFk())){
						selectSubsidiary = true;
						insSecurityFilter.setIdInstitutionTypeFk(InstitutionType.ISSUER.getCode());
					} else if(InstitutionType.PARTICIPANT_INVESTOR.getCode().equals(userAccountBean.getInstitutionTypeFk())){
						selectSubsidiary = true;
						insSecurityFilter.setIdInstitutionTypeFk(InstitutionType.PARTICIPANT.getCode());
					} else {
						insSecurityFilter.setIdInstitutionTypeFk(userAccountBean.getInstitutionTypeFk());
					}
				} else {
					insSecurityFilter.setIdInstitutionTypeFk(userAccountBean.getInstitutionTypeFk());
				}																
				lstInstitutionsByInstitutionType=userServiceFacade.getInstitutionsByFilterServiceFacade(insSecurityFilter);
			}else{
				lstInstitutionsByInstitutionType=new ArrayList<InstitutionsSecurity>();
			}
			if(UserAccountType.INTERNAL.getCode().equals(userAccountBean.getType())){
				for(int i=0;i<lstInstitutionsByInstitutionType.size();i++){
					userAccountBean.getInstitutionsSecurity().setIdInstitutionSecurityPk(lstInstitutionsByInstitutionType.get(i).getIdInstitutionSecurityPk());
				}
			}else{
				institution=false;
			}
			
			if(lstInstitutionsByInstitutionType!=null && lstInstitutionsByInstitutionType.size()==1){
				institution=true;
				userAccountBean.getInstitutionsSecurity().setIdInstitutionSecurityPk(lstInstitutionsByInstitutionType.get(0).getIdInstitutionSecurityPk());
			}else{
				institution=false;
			}
			createLoginUserListener(true);			
			if(userAccountBean.getType()!=null && Validations.validateIsNotNullAndPositive(userAccountBean.getInstitutionTypeFk())){
				loadProfileTree();
			}
			userAccountBean.setSubsidiary(new Subsidiary());
			lstSubsidiary = null;
			subsidiaryFilter = new SubsidiaryFilter();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
			
	/**
	 * Change select motive listener.
	 */
	public void changeSelectMotiveListener(){
		JSFUtilities.setValidViewComponent("frmMotive:txtOtherMotive");
		try {
			if(OtherMotiveType.USER_BLOCK_OTHER_MOTIVE.getCode().equals(historicalState.getMotive()) ||
				OtherMotiveType.USER_UNBLOCK_OTHER_MOTIVE.getCode().equals(historicalState.getMotive()) ||
				OtherMotiveType.USER_REJECT_OTHER_MOTIVE.getCode().equals(historicalState.getMotive()) ||
				OtherMotiveType.USER_ANNULAR_OTHER_MOTIVE.getCode().equals(historicalState.getMotive())){
				showOtherMotive = true;
			} else {
				showOtherMotive = false;
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Block user listener.
	 *
	 * @param event the event
	 */
	@LoggerAuditWeb
	public void blockUserListener(ActionEvent event){
		try {
			log.info("Start method blockUserListener()");
			//hideDialogs();
			UserAccount userTemp=userServiceFacade.getUserAccountByLogin(selectedUserAccountBean);
			if(userTemp != null && userTemp.getLastModifyDate().after(selectedUserAccountBean.getLastModifyDate())){
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, "user.error.reject.recent", null);
				listUsersByFilter(userAccountFilter);
				JSFUtilities.showEndTransactionSamePageDialog();
				return;
			}
			selectedUserAccountBean.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			selectedUserAccountBean.setRegistryDate(new Date());
			
			String messageModal = "";
			String messageNotification = "";
			if(isRejectOption && UserAccountStateType.CONFIRMED.getCode().equals(selectedUserAccountBean.getState()) && 
					UserAccountSituationType.PENDING_LOCK.getCode().equals(selectedUserAccountBean.getSituation())){
				selectedUserAccountBean.setState(UserAccountStateType.CONFIRMED.getCode());
				selectedUserAccountBean.setSituation(UserAccountSituationType.ACTIVE.getCode());
				
				historicalState.setCurrentState(selectedUserAccountBean.getSituation());
				messageModal = PropertiesConstants.USER_REJECT_BLOCK_SUCCESS;
				messageNotification = PropertiesConstants.USER_REJECT_BLOCK_NOTIFICATION;
			}else if(UserAccountStateType.CONFIRMED.getCode().equals(selectedUserAccountBean.getState()) && 
					UserAccountSituationType.ACTIVE.getCode().equals(selectedUserAccountBean.getSituation())){
				selectedUserAccountBean.setState(UserAccountStateType.CONFIRMED.getCode());
				selectedUserAccountBean.setSituation(UserAccountSituationType.PENDING_LOCK.getCode());
				
				historicalState.setCurrentState(selectedUserAccountBean.getSituation());
				messageModal = PropertiesConstants.USER_REQUEST_BLOCK_SUCCESS;
				messageNotification = PropertiesConstants.USER_REQUEST_BLOCK_NOTIFICATION;
			}else if(UserAccountStateType.CONFIRMED.getCode().equals(selectedUserAccountBean.getState()) && 
					UserAccountSituationType.PENDING_LOCK.getCode().equals(selectedUserAccountBean.getSituation())){
				selectedUserAccountBean.setState(UserAccountStateType.BLOCKED.getCode());
				selectedUserAccountBean.setSituation(UserAccountSituationType.ACTIVE.getCode());
				
				historicalState.setCurrentState(selectedUserAccountBean.getState());
				messageModal = PropertiesConstants.USER_BLOCK_SUCCESS;
				messageNotification = PropertiesConstants.USER_BLOCK_NOTIFICATION;
			}
			
			selectedUserAccountBean.setBlockMotive(isRejectOption ? Integer.valueOf(-1) : historicalState.getMotive());//Man
			selectedUserAccountBean.setBlockOtherMotive(isRejectOption ? "" : historicalState.getMotiveDescription());
			
			ThreadLocalContextHolder.put(
					PropertiesConstants.HISTORICAL_STATE_LOGGER, 
					createHistorialState(
							selectedUserAccountBean.getClass().getAnnotation(Table.class).name(), 
							UserAccountStateType.CONFIRMED.getCode(),
					Long.parseLong(selectedUserAccountBean.getIdUserPk().toString()), historicalState));
			//To close the session of blocked user
			boolean resultSessionActive = userServiceFacade.isSelectedUserInSessionServiceBean(selectedUserAccountBean);
			//To send Message to blocked User in the session
			if(resultSessionActive && !isRejectOption && selectedUserAccountBean.getState().equals(UserAccountStateType.BLOCKED.getCode()) && 
					selectedUserAccountBean.getSituation().equals(UserAccountSituationType.ACTIVE.getCode())){

					//PushContext pushContext = PushContextFactory.getDefault().getPushContext();
					//integration with notifications
					BrowserWindow browser = new BrowserWindow();
					browser.setNotificationType(NotificationType.KILLSESSION.getCode());
					//TODO change hardcode message
					browser.setSubject("Expire Session");
					browser.setMessage(PropertiesConstants.SESSION_EXPIRED_USER_BLOCKED);
					log.info("calling push method");
					//pushContext.push(PropertiesConstants.PUSH_MSG+"/"+selectedUserAccountBean.getLoginUser(),browser);			
			}
			
			userServiceFacade.updateUserAccountStateServiceBean(selectedUserAccountBean);
			
			if(this.ldapUtilities.isEnableAuthentication()) {
				if(!isRejectOption && selectedUserAccountBean.getState().equals(UserAccountStateType.BLOCKED.getCode()) && 
						selectedUserAccountBean.getSituation().equals(UserAccountSituationType.ACTIVE.getCode())){
					ldapUtilities.disableUserAccount(selectedUserAccountBean.getLoginUser());
				}
			}
			
			listUsersByFilter(userAccountFilter);			
			
			Object[] arrBodyData = {selectedUserAccountBean.getLoginUser()};			
			showMessageOnDialog(
					PropertiesConstants.HEADER_MSG_SCHEDULE_SUCCESS_REGISTRATION, null,
					messageModal, 
					arrBodyData);
			JSFUtilities.showEndTransactionSamePageDialog();
			
			//JSFUtilities.executeJavascriptFunction("PF('updatedCnfwEndTransactionSamePage').show()"); 
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Unregister user listener.
	 *
	 * @param event the event
	 */
	@LoggerAuditWeb
	public void unregisterUserListener(ActionEvent event){
		try {
			hideDialogs();
			UserAccount userTemp=userServiceFacade.getUserAccountByLogin(selectedUserAccountBean);
			if(userTemp != null && userTemp.getLastModifyDate().after(selectedUserAccountBean.getLastModifyDate())){
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, "user.error.reject.recent", null);
				listUsersByFilter(userAccountFilter);
				removeDocumentFile();
				JSFUtilities.showEndTransactionSamePageDialog();
				return;
			}
			selectedUserAccountBean.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			selectedUserAccountBean.setRegistryDate(new Date());
			
			String messageModal = "";
			String messageNotification = "";
			if(isRejectOption && selectedUserAccountBean.getSituation() !=  null
					&& selectedUserAccountBean.getSituation().equals(UserAccountSituationType.PENDING_UNREGISTER.getCode())){
				
				if(selectedUserAccountBean.getState().equals(UserAccountStateType.REGISTERED.getCode())){
					selectedUserAccountBean.setSituation(null);
				}else{
					selectedUserAccountBean.setSituation(UserAccountSituationType.ACTIVE.getCode());
				}
				
				historicalState.setCurrentState(UserAccountSituationType.PENDING_UNREGISTER.getCode());
				messageModal = PropertiesConstants.USER_REJECT_UNREGISTER_SUCCESS;
				messageNotification = PropertiesConstants.USER_REJECT_UNREGISTER_NOTIFICATION;
			}else if(selectedUserAccountBean.getSituation() != null
					&& selectedUserAccountBean.getSituation().equals(UserAccountSituationType.PENDING_UNREGISTER.getCode())){
				selectedUserAccountBean.setState(UserAccountStateType.ANNULLED.getCode());
				selectedUserAccountBean.setSituation(UserAccountSituationType.ACTIVE.getCode());
				
				historicalState.setCurrentState(selectedUserAccountBean.getState());
				messageModal = PropertiesConstants.USER_UNREGISTER_SUCCESS;
				messageNotification = PropertiesConstants.USER_UNREGISTER_NOTIFICATION;
			}else {
				selectedUserAccountBean.setSituation(UserAccountSituationType.PENDING_UNREGISTER.getCode());
				
				historicalState.setCurrentState(selectedUserAccountBean.getSituation());
				messageModal = PropertiesConstants.USER_REQUEST_UNREGISTER_SUCCESS;
				messageNotification = PropertiesConstants.USER_REQUEST_UNREGISTER_NOTIFICATION;
			}
			
			selectedUserAccountBean.setAnnularMotive(isRejectOption ? Integer.valueOf(-1) : historicalState.getMotive());//Man
			selectedUserAccountBean.setAnnularOtherMotive(isRejectOption ? "" : historicalState.getMotiveDescription());
			
			ThreadLocalContextHolder.put(
					PropertiesConstants.HISTORICAL_STATE_LOGGER, 
					createHistorialState(
							selectedUserAccountBean.getClass().getAnnotation(Table.class).name(), 
							UserAccountStateType.CONFIRMED.getCode(),
							Long.parseLong(selectedUserAccountBean.getIdUserPk().toString()),
							historicalState));
			//To close the session of blocked user
			boolean resultSessionActive = userServiceFacade.isSelectedUserInSessionServiceBean(selectedUserAccountBean);
			//To send Message to blocked User in the session
			if(resultSessionActive && !isRejectOption
					&& selectedUserAccountBean.getState().equals(UserAccountStateType.ANNULLED.getCode())
					&& selectedUserAccountBean.getSituation().equals(UserAccountSituationType.ACTIVE.getCode())){

					//PushContext pushContext = PushContextFactory.getDefault().getPushContext();
					//integration with notifications
					BrowserWindow browser = new BrowserWindow();
					browser.setNotificationType(NotificationType.KILLSESSION.getCode());
					//TODO change hardcode message
					browser.setSubject("Expire Session");
					browser.setMessage(PropertiesConstants.SESSION_EXPIRED_USER_UNREGISTER);
					log.info("calling push method");
					//pushContext.push(PropertiesConstants.PUSH_MSG+"/"+selectedUserAccountBean.getLoginUser(),browser);			
			}
			
			userServiceFacade.updateUserAccountStateServiceBean(selectedUserAccountBean);
			
			if(this.ldapUtilities.isEnableAuthentication()) {
				if(!isRejectOption && selectedUserAccountBean.getState().equals(UserAccountStateType.ANNULLED.getCode()) && 
						selectedUserAccountBean.getSituation().equals(UserAccountSituationType.ACTIVE.getCode())){
					ldapUtilities.disableUserAccount(selectedUserAccountBean.getLoginUser());
				}
			}
			
			listUsersByFilter(userAccountFilter);			
			
			Object[] arrBodyData = {selectedUserAccountBean.getLoginUser()};			
			showMessageOnDialog(
					PropertiesConstants.HEADER_MSG_SCHEDULE_SUCCESS_REGISTRATION, null,
					messageModal, 
					arrBodyData);
			JSFUtilities.showEndTransactionSamePageDialog();
			removeDocumentFile();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Unblock user listener.
	 *
	 * @param event the event
	 */
	@LoggerAuditWeb
	public void unblockUserListener(ActionEvent event){
		try {
			//hideDialogs();
			UserAccount userTemp=userServiceFacade.getUserAccountByLogin(selectedUserAccountBean);
			if(userTemp != null && userTemp.getLastModifyDate().after(selectedUserAccountBean.getLastModifyDate())){
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, "user.error.reject.recent", null);
				listUsersByFilter(userAccountFilter);
				JSFUtilities.showEndTransactionSamePageDialog();
				return;
			}
			selectedUserAccountBean.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			selectedUserAccountBean.setRegistryDate(new Date());
			
			String messageModal = "";
			String messageNotification = "";
			if(isRejectOption && selectedUserAccountBean.getState().equals(UserAccountStateType.BLOCKED.getCode()) && 
					selectedUserAccountBean.getSituation().equals(UserAccountSituationType.PENDING_UNLOCK.getCode())){
				selectedUserAccountBean.setState(UserAccountStateType.BLOCKED.getCode());
				selectedUserAccountBean.setSituation(UserAccountSituationType.ACTIVE.getCode());
				
				historicalState.setCurrentState(selectedUserAccountBean.getSituation());
				messageModal = PropertiesConstants.USER_REJECT_UNBLOCK_SUCCESS;
				messageNotification = PropertiesConstants.USER_REJECT_UNBLOCK_NOTIFICATION;
			}else if(selectedUserAccountBean.getState().equals(UserAccountStateType.BLOCKED.getCode()) && 
					selectedUserAccountBean.getSituation().equals(UserAccountSituationType.ACTIVE.getCode())){
				selectedUserAccountBean.setState(UserAccountStateType.BLOCKED.getCode());
				selectedUserAccountBean.setSituation(UserAccountSituationType.PENDING_UNLOCK.getCode());
				
				historicalState.setCurrentState(selectedUserAccountBean.getSituation());
				messageModal = PropertiesConstants.USER_REQUEST_UNBLOCK_SUCCESS;
				messageNotification = PropertiesConstants.USER_REQUEST_UNBLOCK_NOTIFICATION;
			}else if(selectedUserAccountBean.getState().equals(UserAccountStateType.BLOCKED.getCode()) && 
					selectedUserAccountBean.getSituation().equals(UserAccountSituationType.PENDING_UNLOCK.getCode())){
				selectedUserAccountBean.setState(UserAccountStateType.CONFIRMED.getCode());
				selectedUserAccountBean.setSituation(UserAccountSituationType.ACTIVE.getCode());
				
				historicalState.setCurrentState(selectedUserAccountBean.getState());
				messageModal = PropertiesConstants.USER_UNBLOCK_SUCCESS;
				messageNotification = PropertiesConstants.USER_UNBLOCK_NOTIFICATION;
			}
			
			selectedUserAccountBean.setUnblockMotive(isRejectOption ? Integer.valueOf(-1) :historicalState.getMotive());//man
			selectedUserAccountBean.setUnblockOtherMotive(isRejectOption ? "" : historicalState.getMotiveDescription());
			
			if(this.ldapUtilities.isEnableAuthentication()) {
				if(!isRejectOption && selectedUserAccountBean.getState().equals(UserAccountStateType.CONFIRMED.getCode()) && 
						selectedUserAccountBean.getSituation().equals(UserAccountSituationType.ACTIVE.getCode())){
					ldapUtilities.enableUserAccount(selectedUserAccountBean.getLoginUser());
				}
			}
			
			ThreadLocalContextHolder.put(
					PropertiesConstants.HISTORICAL_STATE_LOGGER, 
					createHistorialState(
							selectedUserAccountBean.getClass().getAnnotation(Table.class).name(),
							UserAccountStateType.BLOCKED.getCode(),
							Long.parseLong(selectedUserAccountBean.getIdUserPk().toString()), 
							historicalState));
			
			userServiceFacade.updateUserAccountStateServiceBean(selectedUserAccountBean);
			
			listUsersByFilter(userAccountFilter);
			Object[] arrBodyData = {selectedUserAccountBean.getLoginUser()};
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_SUCCESS, null,
					messageModal, 
					arrBodyData);
			JSFUtilities.showEndTransactionSamePageDialog();
			//JSFUtilities.executeJavascriptFunction("PF('updatedCnfwEndTransactionSamePage').show()");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Confirm user listener.
	 *
	 * @param event the event
	 */
	@LoggerAuditWeb
	public void confirmUserListener(ActionEvent event){
		try {
			hideDialogs();
			//check concurrency update
			UserAccount userTemp=userServiceFacade.getUserAccountByLogin(selectedUserAccountBean);
			if(userTemp != null && userTemp.getLastModifyDate().after(selectedUserAccountBean.getLastModifyDate())){
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, "user.error.reject.recent", null);
				searchUserListener(null);
				JSFUtilities.showEndTransactionSamePageDialog();
				return;
			}
			String password = SecurityUtilities.generatePassword().substring(0, ldapUtilities.getPwdMinLength());
			selectedUserAccountBean.setState(UserAccountStateType.CONFIRMED.getCode());
			selectedUserAccountBean.setSituation(UserAccountSituationType.ACTIVE.getCode());
			selectedUserAccountBean.setConfirmationUser(userInfo.getUserAccountSession().getUserName());
			selectedUserAccountBean.setConfirmationDate(CommonsUtilities.currentDateTime());
			userServiceFacade.confirmUserSate(selectedUserAccountBean, password);
			Object[] arrBodyData = {selectedUserAccountBean.getLoginUser()};
			searchUserListener(null);
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_SUCCESS, null,
					PropertiesConstants.USER_CONFIRM_SUCCESS, arrBodyData);
			JSFUtilities.showEndTransactionSamePageDialog();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Reject user listener.
	 *
	 * @param event the event
	 */
	@LoggerAuditWeb
	public void rejectUserListener(ActionEvent event){
		try {
			hideDialogs();
			Object[] arrBodyData = {selectedUserAccountBean.getLoginUser()};
			UserAccount latestUserAccInfo=userServiceFacade.getLastestUserAccountDetailsFacade(selectedUserAccountBean.getIdUserPk());
			if(!latestUserAccInfo.getState().equals(selectedUserAccountBean.getState())){
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT,null,PropertiesConstants.USER_ERROR_REJECT_RECENT,arrBodyData);
				listUsersByFilter(userAccountFilter);
			    JSFUtilities.showEndTransactionDialog();
			    return;
			}
			    
			selectedUserAccountBean.setState(UserAccountStateType.REJECTED.getCode());
			selectedUserAccountBean.setSituation(UserAccountSituationType.ACTIVE.getCode());
			selectedUserAccountBean.setRejectUser(userInfo.getUserAccountSession().getUserName());
			selectedUserAccountBean.setRejectDate(new Date());
			selectedUserAccountBean.setRejectMotive(historicalState.getMotive());
			selectedUserAccountBean.setRejectOtherMotive(historicalState.getMotiveDescription());
			
			userServiceFacade.updateUserAccountStateServiceBean(selectedUserAccountBean);
			listUsersByFilter(userAccountFilter);
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_SUCCESS, null,
					PropertiesConstants.USER_REJECT_SUCCESS,arrBodyData);
			JSFUtilities.showEndTransactionSamePageDialog();
		} catch (Exception e) {
			excepcion.fire( new ExceptionToCatchEvent(e) );
		}
	}
	
	/**
	 * File upload handler.
	 *
	 * @param event the event
	 */
	public void fileUploadHandler(FileUploadEvent event){
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);		
		JSFUtilities.hideGeneralDialogues();
		userAccountBean.setfDisplayName(fUploadValidate(event.getFile(),"PF('cnfwMsgCustomValidationAcc').show();", null, getfUploadFileSize(), "pdf"));
		if(userAccountBean.getfDisplayName()!=null){
			try {
				if(isRegister()){
					userAccountBean.setDocumentFile(event.getFile().getContents());
				}else if(isAnnular()){
					selectedUserAccountBean.setUnregisterFile(event.getFile().getContents());
				}
				JSFUtilities.updateComponent("otFileName");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Removes the document file.
	 */
	public void removeDocumentFile(){		
		if(isRegister() && userAccountBean.getDocumentFile() != null){
			userAccountBean.setDocumentFile(null);
		}else if(isAnnular() && selectedUserAccountBean.getUnregisterFile() != null){
			selectedUserAccountBean.setUnregisterFile(null);
		}
	}
	
	/**
	 * Hide dialogs.
	 *
	 */
	public void hideDialogs(){		
		JSFUtilities.hideComponent("frmUserAdministration:cnfUserAdm");
		JSFUtilities.hideComponent("frmUserAdministration:cnfAdmUsuario");
		JSFUtilities.hideComponent("frmUserAdministration:cnfReiniciarContr");
		JSFUtilities.hideComponent("frmBlockUser:cnfBlock");
		JSFUtilities.hideComponent("frmUnblockUser:cnfUnblock");
		JSFUtilities.hideComponent("frmConfirm:cnfConfirmUser");
		JSFUtilities.hideComponent("frmRejectUser:cnfRejectUser");
		JSFUtilities.hideComponent("frmUnregisterUser:cnfUnregister");	
		JSFUtilities.hideComponent("frmMotive:dlgMotive");
		JSFUtilities.hideGeneralDialogues();
		
	}
	
	/**
	 * Hide Confirm block dialog.
	 *
	 */
	public void hideConfBlock(){
		/*JSFUtilities.hideComponent("frmBlockUser:cnfBlock");
		JSFUtilities.hideComponent("frmUnblockUser:cnfUnblock");
		JSFUtilities.hideComponent("frmConfirm:cnfConfirmUser");
		JSFUtilities.hideComponent("frmRejectUser:cnfRejectUser");
		JSFUtilities.hideComponent("frmUnregisterUser:cnfUnregister");*/
	}
	
	/**
	 * The Method is used close the Confirm dialog on Pressing No.
	 */
	public void hideConfirmUser(){
		if(isRegister() || isModify()){
			//revert name
			disabledName=false;
		}
		//JSFUtilities.hideComponent("frmUserAdministration:cnfUserAdm");
		JSFUtilities.executeJavascriptFunction("PF('frmUserAdministration:cnfUserAdm').hide()");
	}

	/**
	 * Change user type for filter listener.
	 */
	public void changeUserTypeForFilterListener(){
		disableInstitutionSelector = false;
//		try {
			if(userAccountFilter.getUserAccountType().intValue()>0){
				if(userAccountFilter.getUserAccountType().intValue()==UserAccountType.INTERNAL.getCode().intValue()){
					lstInstitutionTypeForFilter=new ArrayList<Parameter>();
					lstInstitutionTypeForFilter.addAll(lstInstitutionTypeForFilterTemp);
					for(Parameter param:lstInstitutionTypeForFilterTemp){
						if(!InstitutionType.DEPOSITARY.getCode().equals(param.getIdParameterPk())){
							lstInstitutionTypeForFilter.remove(param);
						}
					}
					
					userAccountFilter.setInstitutionType(Integer.parseInt(InstitutionType.DEPOSITARY.getCode().toString()));
					
					changeInstitutionTypeForFilterListener();
					HtmlSelectOneMenu combo=(HtmlSelectOneMenu)JSFUtilities.findViewComponent("frmGeneral:cboInstitution");
					UISelectItems items=(UISelectItems)combo.getChildren().get(1);
					List<InstitutionsSecurity> lstInstituions=(List<InstitutionsSecurity>)items.getValue();
					userAccountFilter.setIdInstitutionSecurity(Integer.valueOf(lstInstituions.get(0).getIdInstitutionSecurityPk()));
					disableInstitutionSelector = true;
				}else{
					lstInstitutionTypeForFilter=new ArrayList<Parameter>();
					lstInstitutionTypeForFilter.addAll(lstInstitutionTypeForFilterTemp);
					for(Parameter param:lstInstitutionTypeForFilterTemp){
						if(InstitutionType.DEPOSITARY.getCode().equals(param.getIdParameterPk())){
							lstInstitutionTypeForFilter.remove(param);
						}
					}
					
					lstInstitutionsSecurityForFilter=new ArrayList<InstitutionsSecurity>();
					userAccountFilter.setInstitutionType(Integer.valueOf(-1));
					userAccountFilter.setIdInstitutionSecurity(Integer.valueOf(-1));
				}
			} else{
				lstInstitutionTypeForFilter=new ArrayList<Parameter>();
				lstInstitutionsSecurityForFilter=new ArrayList<InstitutionsSecurity>();
				userAccountFilter.setInstitutionType(Integer.valueOf(-1));
				userAccountFilter.setIdInstitutionSecurity(Integer.valueOf(-1));
			}
//		} catch (Exception e) {
//			excepcion.fire( new ExceptionToCatchEvent(e) );
//		}
	}
	
	
	/**
	 * Change institution type for filter listener.
	 */
	public void changeInstitutionTypeForFilterListener(){
		try {
			if(userAccountFilter.getInstitutionType()>0){
				InstitutionSecurityFilter insSecurityFilter=new InstitutionSecurityFilter();
				
				if(Validations.validateIsNotNullAndNotEmpty(userAccountFilter.getInstitutionType())){
					if(InstitutionType.ISSUER_DPF.getCode().equals(userAccountFilter.getInstitutionType())){
						insSecurityFilter.setIdInstitutionTypeFk(InstitutionType.ISSUER.getCode());
					} else if(InstitutionType.PARTICIPANT_INVESTOR.getCode().equals(userAccountFilter.getInstitutionType())){
						insSecurityFilter.setIdInstitutionTypeFk(InstitutionType.PARTICIPANT.getCode());
					} else  {
						insSecurityFilter.setIdInstitutionTypeFk(userAccountFilter.getInstitutionType() );
					}
				} else {
					insSecurityFilter.setIdInstitutionTypeFk(userAccountFilter.getInstitutionType() );
				}				
				//insSecurityFilter.setIdInstitutionTypeFk(userAccountFilter.getInstitutionType() );
				lstInstitutionsSecurityForFilter=userServiceFacade.getInstitutionsByFilterServiceFacade(insSecurityFilter);
			}else{
				lstInstitutionsSecurityForFilter=new ArrayList<InstitutionsSecurity>();
				userAccountFilter.setIdInstitutionSecurity(Integer.valueOf(-1));
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Validate type document.
	 */
	public void validateTypeDocument() {
		userAccountBean.setDocumentNumber(GeneralConstants.EMPTY_STRING);
		JSFUtilities.setValidViewComponent("frmUserAdministration:txtDocumentNro");
		
		if(Validations.validateIsNotNullAndPositive(userAccountBean.getDocumentType())){
			switch (UserAccountDocumentType.get(userAccountBean.getDocumentType())) {			
			case CI:
			case RUC:
				numericTypeDocument = true;
				maxLenghtDocumentNumber = 15;
				break;
			case CIE:
			case DIO:
			case PAS:
				numericTypeDocument = false;
				maxLenghtDocumentNumber = 15;
				break;
			case CDN:
				numericTypeDocument = false;
				maxLenghtDocumentNumber = 21;
				break;
			}
		}
	}
	
 /**
  * Return principal page.
  *
  * @return the string
  */
 @LoggerAuditWeb
 public String returnPrincipalPage(){
	 return "searchUserMgmtRule";
 }

			
	/**
 * Load list user document type.
 *
 */
public void loadListUserDocumentType(){
		lstUserDocumentType=UserAccountDocumentType.list;
	}

	/**
	 * Gets the user account bean.
	 *
	 * @return the user account bean
	 */
	public UserAccount getUserAccountBean() {
		return userAccountBean;
	}
	
	/**
	 * Sets the user account bean.
	 *
	 * @param userAccountBean the new user account bean
	 */
	public void setUserAccountBean(UserAccount userAccountBean) {
		this.userAccountBean = userAccountBean;
	}
	
	/**
	 * Gets the institution.
	 *
	 * @return the institution
	 */
	public Boolean getInstitution() {
		return institution;
	}

	/**
	 * Sets the institution.
	 *
	 * @param institution the institution to set
	 */
	public void setInstitution(Boolean institution) {
		this.institution = institution;
	}

	/**
	 * Gets the lst institution type.
	 *
	 * @return the lst institution type
	 */
	public List<InstitutionType> getLstInstitutionType() {
		return lstInstitutionType;
	}

	/**
	 * Gets the lst user responsability.
	 *
	 * @return the lst user responsability
	 */
	public List<Parameter> getLstUserResponsability(){
		List<Parameter> lstParam = new ArrayList<Parameter>();
		try{
			lstParam = userServiceFacade.listClientTypeByDefinitionServiceFacade(ParameterTableType.USER_RESPONSIBILITIES.getCode());
		}catch (Exception e) {
			e.printStackTrace();
		}
		return lstParam;
	}
	

	/**
	 * Gets the tree profiles root.
	 *
	 * @return the tree profiles root
	 */
	public TreeNode getTreeProfilesRoot() {
		return treeProfilesRoot;
	}

	/**
	 * Sets the tree profiles root.
	 *
	 * @param treeProfilesRoot the new tree profiles root
	 */
	public void setTreeProfilesRoot(TreeNode treeProfilesRoot) {
		this.treeProfilesRoot = treeProfilesRoot;
	}

	/**
	 * Gets the nodes system profiles selected.
	 *
	 * @return the nodes system profiles selected
	 */
	public TreeNode[] getNodesSystemProfilesSelected() {
		return nodesSystemProfilesSelected;
	}

	/**
	 * Sets the nodes system profiles selected.
	 *
	 * @param nodesSystemProfilesSelected the new nodes system profiles selected
	 */
	public void setNodesSystemProfilesSelected(TreeNode[] nodesSystemProfilesSelected) {
		this.nodesSystemProfilesSelected = nodesSystemProfilesSelected;
	}

	/**
	 * Gets the historical state.
	 *
	 * @return the historical state
	 */
	public HistoricalState getHistoricalState() {
		return historicalState;
	}

	/**
	 * Sets the historical state.
	 *
	 * @param historicalState the new historical state
	 */
	public void setHistoricalState(HistoricalState historicalState) {
		this.historicalState = historicalState;
	}

	/**
	 * Gets the lst institutions type selected.
	 *
	 * @return the lst institutions type selected
	 */
	public List<String> getLstInstitutionsTypeSelected() {
		return lstInstitutionsTypeSelected;
	}

	/**
	 * Sets the lst institutions type selected.
	 *
	 * @param lstInstitutionsTypeSelected the new lst institutions type selected
	 */
	public void setLstInstitutionsTypeSelected(List<String> lstInstitutionsTypeSelected) {
		this.lstInstitutionsTypeSelected = lstInstitutionsTypeSelected;
	}

	/**
	 * Gets the user account filter.
	 *
	 * @return the user account filter
	 */
	public UserAccountFilter getUserAccountFilter() {
		return userAccountFilter;
	}

	/**
	 * Sets the user account filter.
	 *
	 * @param userAccountFilter the new user account filter
	 */
	public void setUserAccountFilter(UserAccountFilter userAccountFilter) {
		this.userAccountFilter = userAccountFilter;
	}

	/**
	 * Gets the user data model.
	 *
	 * @return the user data model
	 */
	public UserDataModel getUserDataModel() {
		return userDataModel;
	}

	/**
	 * Sets the user data model.
	 *
	 * @param userDataModel the new user data model
	 */
	public void setUserDataModel(UserDataModel userDataModel) {
		this.userDataModel = userDataModel;
	}

	/**
	 * Gets the selected user account bean.
	 *
	 * @return the selected user account bean
	 */
	public UserAccount getSelectedUserAccountBean() {
		return selectedUserAccountBean;
	}

	/**
	 * Sets the selected user account bean.
	 *
	 * @param selectedUserAccountBean the new selected user account bean
	 */
	public void setSelectedUserAccountBean(UserAccount selectedUserAccountBean) {
		this.selectedUserAccountBean = selectedUserAccountBean;
	}

	/**
	 * Checks if is show other motive.
	 *
	 * @return true, if is show other motive
	 */
	public boolean isShowOtherMotive() {
		return showOtherMotive;
	}

	/**
	 * Sets the show other motive.
	 *
	 * @param showOtherMotive the new show other motive
	 */
	public void setShowOtherMotive(boolean showOtherMotive) {
		this.showOtherMotive = showOtherMotive;
	}

	/**
	 * Gets the lst show institution type assigned to user.
	 *
	 * @return the lst show institution type assigned to user
	 */
	public List<String> getLstShowInstitutionTypeAssignedToUser() {
		return lstShowInstitutionTypeAssignedToUser;
	}

	/**
	 * Sets the lst show institution type assigned to user.
	 *
	 * @param lstShowInstitutionTypeAssignedToUser the new lst show institution type assigned to user
	 */
	public void setLstShowInstitutionTypeAssignedToUser(
			List<String> lstShowInstitutionTypeAssignedToUser) {
		this.lstShowInstitutionTypeAssignedToUser = lstShowInstitutionTypeAssignedToUser;
	}

	/**
	 * Gets the lst user document type.
	 *
	 * @return the lst user document type
	 */
	public List<UserAccountDocumentType> getLstUserDocumentType() {
		return lstUserDocumentType;
	}

	/**
	 * Gets the lst institutions by institution type.
	 *
	 * @return the lst institutions by institution type
	 */
	public List<InstitutionsSecurity> getLstInstitutionsByInstitutionType() {
		return lstInstitutionsByInstitutionType;
	}

	/**
	 * Sets the lst institutions by institution type.
	 *
	 * @param lstInstitutionsByInstitutionType the new lst institutions by institution type
	 */
	public void setLstInstitutionsByInstitutionType(
			List<InstitutionsSecurity> lstInstitutionsByInstitutionType) {
		this.lstInstitutionsByInstitutionType = lstInstitutionsByInstitutionType;
	}

	/**
	 * Gets the lst institution type for filter.
	 *
	 * @return the lst institution type for filter
	 */
	public List<Parameter> getLstInstitutionTypeForFilter() {
		return lstInstitutionTypeForFilter;
	}

	/**
	 * Sets the lst institution type for filter.
	 *
	 * @param lstInstitutionTypeForFilter the new lst institution type for filter
	 */
	public void setLstInstitutionTypeForFilter(
			List<Parameter> lstInstitutionTypeForFilter) {
		this.lstInstitutionTypeForFilter = lstInstitutionTypeForFilter;
	}

	/**
	 * Gets the lst institution type for filter temp.
	 *
	 * @return the lstInstitutionTypeForFilterTemp
	 */
	public List<Parameter> getLstInstitutionTypeForFilterTemp() {
		return lstInstitutionTypeForFilterTemp;
	}

	/**
	 * Sets the lst institution type for filter temp.
	 *
	 * @param lstInstitutionTypeForFilterTemp the lstInstitutionTypeForFilterTemp to set
	 */
	public void setLstInstitutionTypeForFilterTemp(
			List<Parameter> lstInstitutionTypeForFilterTemp) {
		this.lstInstitutionTypeForFilterTemp = lstInstitutionTypeForFilterTemp;
	}

	/**
	 * Gets the lst institutions security for filter.
	 *
	 * @return the lst institutions security for filter
	 */
	public List<InstitutionsSecurity> getLstInstitutionsSecurityForFilter() {
		return lstInstitutionsSecurityForFilter;
	}

	/**
	 * Sets the lst institutions security for filter.
	 *
	 * @param lstInstitutionsSecurityForFilter the new lst institutions security for filter
	 */
	public void setLstInstitutionsSecurityForFilter(
			List<InstitutionsSecurity> lstInstitutionsSecurityForFilter) {
		this.lstInstitutionsSecurityForFilter = lstInstitutionsSecurityForFilter;
	}

	/**
	 * Gets the user account confirmed state type.
	 *
	 * @return the user account confirmed state type
	 */
	public UserAccountStateType getUserAccountConfirmedStateType() {
		return userAccountConfirmedStateType;
	}

	/**
	 * Sets the user account confirmed state type.
	 *
	 * @param userAccountConfirmedStateType the new user account confirmed state type
	 */
	public void setUserAccountConfirmedStateType(
			UserAccountStateType userAccountConfirmedStateType) {
		this.userAccountConfirmedStateType = userAccountConfirmedStateType;
	}
	
	/**
	 * For Block Descripcion.
	 *
	 * @return the motive descripcion
	 */
	public String getMotiveDescripcion(){
	    String desc = "";
	    try{
	    lstMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.BLOCK_MOTIVES.getCode());
	    if(userAccountBean.getBlockMotive() != null){
			if(lstMotives != null && lstMotives.size() > 0 ){
				for(int i=0;i<lstMotives.size();i++){
					if(userAccountBean.getBlockMotive().compareTo(lstMotives.get(i).getIdParameterPk()) == 0){
							desc=lstMotives.get(i).getName();
						}
					}
				}
			}
	    }
	    catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return desc;
	}	
		
		/**
		 * For unBlock Descripcion.
		 *
		 * @return the motiveun block descripcion
		 */
	public String getMotiveunBlockDescripcion(){
	    String desc = "";
	    try{
	   lstMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.DESBLOCK_MOTIVES.getCode());
	    if(userAccountBean.getUnblockMotive() != null){
			if(lstMotives != null && lstMotives.size() > 0 ){
				for(int i=0;i<lstMotives.size();i++){
					if(userAccountBean.getUnblockMotive().compareTo(lstMotives.get(i).getIdParameterPk()) == 0){
							desc=lstMotives.get(i).getName();
						}
					}
				}
			}
	    }
	    catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return desc;
	}	
	
	/**
	 * For user reject motives Descripcion.
	 *
	 * @return the motive reject descripcion
	 */
    public String getMotiveRejectDescripcion(){
        String desc = "";
	    try{
	    lstMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.USER_REJECT_MOTIVES.getCode());
	    if(userAccountBean.getRejectMotive() != null){
			if(lstMotives != null && lstMotives.size() > 0 ){
				for(int i=0;i<lstMotives.size();i++){
					if(userAccountBean.getRejectMotive().compareTo(lstMotives.get(i).getIdParameterPk()) == 0){
							desc=lstMotives.get(i).getName();
						}
					}
				}
			}
	    }
	    catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return desc;
    }
    
    /**
     * Gets the motive annular description.
     *
     * @return the motive annular description
     */
    public String getMotiveAnnularDescription(){
    	String desc = "";
 	    try{
 	    lstMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.USER_UNREGISTER_MOTIVES.getCode());
 	    if(userAccountBean.getAnnularMotive() != null){
 			if(lstMotives != null && lstMotives.size() > 0 ){
 				for(int i=0;i<lstMotives.size();i++){
 					if(userAccountBean.getAnnularMotive().compareTo(lstMotives.get(i).getIdParameterPk()) == 0){
 							desc=lstMotives.get(i).getName();
 						}
 					}
 				}
 			}
 	    }
 	    catch (Exception e) {
 			excepcion.fire(new ExceptionToCatchEvent(e));
 		}
 		return desc;
    }
	
	/**
	 * it clear the mac address when we unselect check.
	 */
	public void clearMacRestriccion(){
		if(!this.getUserAccountBean().isMacRestrictionBool()){
			this.getUserAccountBean().setMacAddress("");
		}
	} 
	
	/**
	 * This method used for In tree node it can only check one profile.
	 *
	 * @param event the event
	 */
	public void onNodeSelect(NodeSelectEvent event) { 
		TreeNode selected = event.getTreeNode();

			int dynamicSize = nodesSystemProfilesSelected.length;
			
			for(int i=0; i<nodesSystemProfilesSelected.length; i++){
				if(!selected.equals(nodesSystemProfilesSelected[i]) && !selected.getParent().toString().equals("Root") &&
						selected.getParent().equals(nodesSystemProfilesSelected[i].getParent())){
					nodesSystemProfilesSelected[i].setSelected(false);
					dynamicSize--;
				}		
			}
			
			if(nodesSystemProfilesSelected.length>GeneralConstants.ONE_VALUE_INTEGER){
				TreeNode[] nodesSystemProfilesSelectedANew = new TreeNode[dynamicSize];
				int count = GeneralConstants.ZERO_VALUE_INTEGER;
				
				for(int k=0;k<nodesSystemProfilesSelected.length;k++){
					if(nodesSystemProfilesSelected[k].isSelected()){
						nodesSystemProfilesSelectedANew[count] = nodesSystemProfilesSelected[k];
						count++;
					}
				}
				
				nodesSystemProfilesSelected = nodesSystemProfilesSelectedANew;
			}
			
				for(int j=0; j<nodesSystemProfilesSelected.length; j++){
					if(!selected.equals(nodesSystemProfilesSelected[j])&& nodesSystemProfilesSelected[j].getParent().isSelected() && 
							selected.getChildCount()>1){
						nodesSystemProfilesSelected[j].setSelected(false);
						nodesSystemProfilesSelected[j].getParent().setSelectable(false);
						nodesSystemProfilesSelected[j].getParent().setExpanded(true);
				}
			}	
			selected.getParent().setExpanded(true);	
								
		}
	
	/**
	 * This method used for In tree node when no one profile select.
	 *
	 * @param event the event
	 */
	public void onNodeUnSelected(NodeUnselectEvent event){
		TreeNode unSelect = event.getTreeNode();
		unSelect.setSelected(false);
		unSelect.getParent().setSelected(false);
		//nodesSystemProfilesSelected = new TreeNode[]{};
		
	}
	
	/**
	 * Refresh external tree.
	 */
	public void refreshExternalTree(){
		showTreePanelExterns();
		loadProfileTree();
	}
	
	/**
	 * Show tree panel externs.
	 */
	public void showTreePanelExterns(){
		if(userInfo.getUserAccountSession().isDepositaryInstitution()){
			if(userAccountBean.getType().equals(UserAccountType.EXTERNAL.getCode()) && blIsRecorder){
				setShowProfilesExtern(true);
			} else {
				setShowProfilesExtern(false);
			}
			setShowProfilesAllowed(false);
		} else {
			//extern user 
			setShowProfilesAllowed(true);
			setShowProfilesExtern(false);
		}
	}
	
	/**
	 * Show confirm button modal.
	 *
	 * @return true, if successful
	 */
	public boolean showConfirmButtonModal(){
		boolean show =false;
		//First register is enabled for Internal user or external register user
		if(!isConfirmBlockOrUnblock){
			if(userInfo.getUserAccountSession().isDepositaryInstitution()){
				//only extern user not confirmer can register request
				show=true;
			} else if(!hasUserCommitterPrivilege && ViewOperationsType.ANULATE.getCode().equals(getOperationType())) {
				show=true;
			} else if(ViewOperationsType.UNBLOCK.getCode().equals(getOperationType()) || ViewOperationsType.BLOCK.getCode().equals(getOperationType())){
				show=true;
			} else if(ViewOperationsType.REJECT.getCode().equals(getOperationType())) {
				show=true;
			}
		} else {
			//final confirmation or rejected only for privileges enables
			if(ViewOperationsType.UNBLOCK.getCode().equals(getOperationType()) && userPrivilege.getUserAcctions().isConfirmUnblock()){
				show=true;
			} else if(ViewOperationsType.BLOCK.getCode().equals(getOperationType()) && userPrivilege.getUserAcctions().isConfirmBlock()){
				show=true;
			} else if(ViewOperationsType.ANULATE.getCode().equals(getOperationType()) && userPrivilege.getUserAcctions().isConfirmAnnular()){
				show=true;
			}
		}
		return show;
	}
	
	/**
	 * Show rejetc button modal.
	 *
	 * @return true, if successful
	 */
	public boolean showRejetcButtonModal(){
		boolean show =false;
		if(isConfirmBlockOrUnblock){
			if(ViewOperationsType.UNBLOCK.getCode().equals(getOperationType()) && userPrivilege.getUserAcctions().isRejectUnblock()){
				show=true;
			} else if(ViewOperationsType.BLOCK.getCode().equals(getOperationType()) && userPrivilege.getUserAcctions().isRejectBlock()){
				show=true;
			} else if(ViewOperationsType.ANULATE.getCode().equals(getOperationType()) && userPrivilege.getUserAcctions().isRejectAnnular()){
				show=true;
			}
		}
		return show;
	}
	
	/**
	 * Disable tree profile.
	 *
	 * @return true, if successful
	 */
	public boolean isDisableTreeProfile(){
		boolean disabled =false;
		if(isModify() && !userInfo.getUserAccountSession().isDepositaryInstitution()){
			if(userInfo.getUserAccountSession().getUserName().equals(userAccountBean.getLoginUser()) 
					|| BooleanType.YES.getCode().equals(userAccountBean.getIndIsCommitter())
					|| BooleanType.YES.getCode().equals(userAccountBean.getIndIsRecorder())){
				disabled = true;
			}
		} 
		return disabled;
	}

	/**
	 * Checks if is numeric type document.
	 *
	 * @return the numericTypeDocument
	 */
	public boolean isNumericTypeDocument() {
		return numericTypeDocument;
	}

	/**
	 * Sets the numeric type document.
	 *
	 * @param numericTypeDocument the numericTypeDocument to set
	 */
	public void setNumericTypeDocument(boolean numericTypeDocument) {
		this.numericTypeDocument = numericTypeDocument;
	}

	/**
	 * Gets the max lenght document number.
	 *
	 * @return the maxLenghtDocumentNumber
	 */
	public Integer getMaxLenghtDocumentNumber() {
		return maxLenghtDocumentNumber;
	}

	/**
	 * Sets the max lenght document number.
	 *
	 * @param maxLenghtDocumentNumber the maxLenghtDocumentNumber to set
	 */
	public void setMaxLenghtDocumentNumber(Integer maxLenghtDocumentNumber) {
		this.maxLenghtDocumentNumber = maxLenghtDocumentNumber;
	}

	/**
	 * Gets the valid user code.
	 *
	 * @return the validUserCode
	 */
	public int getValidUserCode() {
		return validUserCode;
	}

	/**
	 * Sets the valid user code.
	 *
	 * @param validUserCode the validUserCode to set
	 */
	public void setValidUserCode(int validUserCode) {
		this.validUserCode = validUserCode;
	}
	
	/**
	 * Cleanup.
	 */
	@PreDestroy
	void cleanup()
	{
		endConversation();
	}

	/**
	 * Gets the pre user code.
	 *
	 * @return the preUserCode
	 */
	public String getPreUserCode() {
		return preUserCode;
	}

	/**
	 * Sets the pre user code.
	 *
	 * @param preUserCode the preUserCode to set
	 */
	public void setPreUserCode(String preUserCode) {
		this.preUserCode = preUserCode;
	}

	/**
	 * Gets the user account state.
	 *
	 * @return the userAccountState
	 */
	public List<Parameter> getUserAccountState() {
		return userAccountState;
	}

	/**
	 * Sets the user account state.
	 *
	 * @param userAccountState the userAccountState to set
	 */
	public void setUserAccountState(List<Parameter> userAccountState) {
		this.userAccountState = userAccountState;
	}

	/**
	 * Checks if is reject option.
	 *
	 * @return true, if is reject option
	 */
	public boolean isRejectOption() {
		return isRejectOption;
	}

	/**
	 * Sets the reject option.
	 *
	 * @param isRejectOption the new reject option
	 */
	public void setRejectOption(boolean isRejectOption) {
		this.isRejectOption = isRejectOption;
	}

	/**
	 * Checks if is confirm block or unblock.
	 *
	 * @return true, if is confirm block or unblock
	 */
	public boolean isConfirmBlockOrUnblock() {
		return isConfirmBlockOrUnblock;
	}

	/**
	 * Sets the confirm block or unblock.
	 *
	 * @param isConfirmBlockOrUnblock the new confirm block or unblock
	 */
	public void setConfirmBlockOrUnblock(boolean isConfirmBlockOrUnblock) {
		this.isConfirmBlockOrUnblock = isConfirmBlockOrUnblock;
	}

	/**
	 * Checks if is unregister event.
	 *
	 * @return true, if is unregister event
	 */
	public boolean isUnregisterEvent() {
		return isUnregisterEvent;
	}

	/**
	 * Sets the unregister event.
	 *
	 * @param isUnregisterEvent the new unregister event
	 */
	public void setUnregisterEvent(boolean isUnregisterEvent) {
		this.isUnregisterEvent = isUnregisterEvent;
	}

	/**
	 * Checks if is disable institution selector.
	 *
	 * @return true, if is disable institution selector
	 */
	public boolean isDisableInstitutionSelector() {
		return disableInstitutionSelector;
	}

	/**
	 * Sets the disable institution selector.
	 *
	 * @param disableInstitutionSelector the new disable institution selector
	 */
	public void setDisableInstitutionSelector(boolean disableInstitutionSelector) {
		this.disableInstitutionSelector = disableInstitutionSelector;
	}

	/**
	 * Checks if is bl is recorder.
	 *
	 * @return true, if is bl is recorder
	 */
	public boolean isBlIsRecorder() {
		return blIsRecorder;
	}

	/**
	 * Sets the bl is recorder.
	 *
	 * @param blIsRecorder the new bl is recorder
	 */
	public void setBlIsRecorder(boolean blIsRecorder) {
		this.blIsRecorder = blIsRecorder;
	}

	/**
	 * Checks if is bl is committer.
	 *
	 * @return true, if is bl is committer
	 */
	public boolean isBlIsCommitter() {
		return blIsCommitter;
	}

	/**
	 * Sets the bl is committer.
	 *
	 * @param blIsCommitter the new bl is committer
	 */
	public void setBlIsCommitter(boolean blIsCommitter) {
		this.blIsCommitter = blIsCommitter;
	}

	/**
	 * Gets the lst system profile.
	 *
	 * @return the lst system profile
	 */
	public List<SystemProfile> getLstSystemProfile() {
		return lstSystemProfile;
	}

	/**
	 * Sets the lst system profile.
	 *
	 * @param lstSystemProfile the new lst system profile
	 */
	public void setLstSystemProfile(List<SystemProfile> lstSystemProfile) {
		this.lstSystemProfile = lstSystemProfile;
	}

	/**
	 * Checks if is bl disabled extern.
	 *
	 * @return true, if is bl disabled extern
	 */
	public boolean isBlDisabledExtern() {
		return blDisabledExtern;
	}

	/**
	 * Sets the bl disabled extern.
	 *
	 * @param blDisabledExtern the new bl disabled extern
	 */
	public void setBlDisabledExtern(boolean blDisabledExtern) {
		this.blDisabledExtern = blDisabledExtern;
	}

	/**
	 * Checks if is bl disabled user extern.
	 *
	 * @return true, if is bl disabled user extern
	 */
	public boolean isBlDisabledUserExtern() {
		return blDisabledUserExtern;
	}

	/**
	 * Sets the bl disabled user extern.
	 *
	 * @param blDisabledUserExtern the new bl disabled user extern
	 */
	public void setBlDisabledUserExtern(boolean blDisabledUserExtern) {
		this.blDisabledUserExtern = blDisabledUserExtern;
	}

	/**
	 * Checks if is extern disabled.
	 *
	 * @return true, if is extern disabled
	 */
	public boolean isExternDisabled() {
		return externDisabled;
	}

	/**
	 * Sets the extern disabled.
	 *
	 * @param externDisabled the new extern disabled
	 */
	public void setExternDisabled(boolean externDisabled) {
		this.externDisabled = externDisabled;
	}

	/**
	 * Gets the tree externs profiles.
	 *
	 * @return the tree externs profiles
	 */
	public TreeNode getTreeExternsProfiles() {
		return treeExternsProfiles;
	}

	/**
	 * Sets the tree externs profiles.
	 *
	 * @param treeExternsProfiles the new tree externs profiles
	 */
	public void setTreeExternsProfiles(TreeNode treeExternsProfiles) {
		this.treeExternsProfiles = treeExternsProfiles;
	}

	/**
	 * Gets the nodes externs profiles selected.
	 *
	 * @return the nodes externs profiles selected
	 */
	public TreeNode[] getNodesExternsProfilesSelected() {
		return nodesExternsProfilesSelected;
	}

	/**
	 * Sets the nodes externs profiles selected.
	 *
	 * @param nodesExternsProfilesSelected the new nodes externs profiles selected
	 */
	public void setNodesExternsProfilesSelected(
			TreeNode[] nodesExternsProfilesSelected) {
		this.nodesExternsProfilesSelected = nodesExternsProfilesSelected;
	}

	/**
	 * Checks if is show profiles extern.
	 *
	 * @return true, if is show profiles extern
	 */
	public boolean isShowProfilesExtern() {
		return showProfilesExtern;
	}

	/**
	 * Sets the show profiles extern.
	 *
	 * @param showProfilesExtern the new show profiles extern
	 */
	public void setShowProfilesExtern(boolean showProfilesExtern) {
		this.showProfilesExtern = showProfilesExtern;
	}

	/**
	 * Checks if is show profiles allowed.
	 *
	 * @return true, if is show profiles allowed
	 */
	public boolean isShowProfilesAllowed() {
		return showProfilesAllowed;
	}

	/**
	 * Sets the show profiles allowed.
	 *
	 * @param showProfilesAllowed the new show profiles allowed
	 */
	public void setShowProfilesAllowed(boolean showProfilesAllowed) {
		this.showProfilesAllowed = showProfilesAllowed;
	}

	/**
	 * Gets the profiles allowed user.
	 *
	 * @return the profiles allowed user
	 */
	public List<UserProfileAllowed> getProfilesAllowedUser() {
		return profilesAllowedUser;
	}

	/**
	 * Sets the profiles allowed user.
	 *
	 * @param profilesAllowedUser the new profiles allowed user
	 */
	public void setProfilesAllowedUser(List<UserProfileAllowed> profilesAllowedUser) {
		this.profilesAllowedUser = profilesAllowedUser;
	}

	/**
	 * Checks if is checks for user committer privilege.
	 *
	 * @return true, if is checks for user committer privilege
	 */
	public boolean isHasUserCommitterPrivilege() {
		return hasUserCommitterPrivilege;
	}

	/**
	 * Sets the checks for user committer privilege.
	 *
	 * @param hasUserCommitterPrivilege the new checks for user committer privilege
	 */
	public void setHasUserCommitterPrivilege(boolean hasUserCommitterPrivilege) {
		this.hasUserCommitterPrivilege = hasUserCommitterPrivilege;
	}

	/**
	 * Gets the lst subsidiary.
	 *
	 * @return the lstSubsidiary
	 */
	public List<Subsidiary> getLstSubsidiary() {
		return lstSubsidiary;
	}

	/**
	 * Sets the lst subsidiary.
	 *
	 * @param lstSubsidiary the lstSubsidiary to set
	 */
	public void setLstSubsidiary(List<Subsidiary> lstSubsidiary) {
		this.lstSubsidiary = lstSubsidiary;
	}

	/**
	 * Gets the description subsidiary.
	 *
	 * @return the descriptionSubsidiary
	 */
	public String getDescriptionSubsidiary() {
		return descriptionSubsidiary;
	}

	/**
	 * Sets the description subsidiary.
	 *
	 * @param descriptionSubsidiary the descriptionSubsidiary to set
	 */
	public void setDescriptionSubsidiary(String descriptionSubsidiary) {
		this.descriptionSubsidiary = descriptionSubsidiary;
	}

	/**
	 * Gets the subsidiary filter.
	 *
	 * @return the subsidiaryFilter
	 */
	public SubsidiaryFilter getSubsidiaryFilter() {
		return subsidiaryFilter;
	}

	/**
	 * Sets the subsidiary filter.
	 *
	 * @param subsidiaryFilter the subsidiaryFilter to set
	 */
	public void setSubsidiaryFilter(SubsidiaryFilter subsidiaryFilter) {
		this.subsidiaryFilter = subsidiaryFilter;
	}

	/**
	 * Gets the lst departament.
	 *
	 * @return the lstDepartament
	 */
	public List<Parameter> getLstDepartament() {
		return lstDepartament;
	}

	/**
	 * Sets the lst departament.
	 *
	 * @param lstDepartament the lstDepartament to set
	 */
	public void setLstDepartament(List<Parameter> lstDepartament) {
		this.lstDepartament = lstDepartament;
	}

	/**
	 * Gets the description departament.
	 *
	 * @return the descriptionDepartament
	 */
	public String getDescriptionDepartament() {
		return descriptionDepartament;
	}

	/**
	 * Sets the description departament.
	 *
	 * @param descriptionDepartament the descriptionDepartament to set
	 */
	public void setDescriptionDepartament(String descriptionDepartament) {
		this.descriptionDepartament = descriptionDepartament;
	}

	/**
	 * Checks if is select subsidiary.
	 *
	 * @return the selectSubsidiary
	 */
	public boolean isSelectSubsidiary() {
		return selectSubsidiary;
	}

	/**
	 * Sets the select subsidiary.
	 *
	 * @param selectSubsidiary the selectSubsidiary to set
	 */
	public void setSelectSubsidiary(boolean selectSubsidiary) {
		this.selectSubsidiary = selectSubsidiary;
	}

	/**
	 * Checks if is disabled name.
	 *
	 * @return true, if is disabled name
	 */
	public boolean isDisabledName() {
		return disabledName;
	}

	/**
	 * Sets the disabled name.
	 *
	 * @param disabledName the new disabled name
	 */
	public void setDisabledName(boolean disabledName) {
		this.disabledName = disabledName;
	}

}