package com.pradera.security.usermgmt.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;
import org.omnifaces.cdi.Push;
import org.omnifaces.cdi.PushContext;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;

import com.pradera.commons.configuration.StageDependent;
import com.pradera.commons.configuration.UserAdmin;
import com.pradera.commons.configuration.application.ApplicationParameter;
import com.pradera.commons.configuration.type.StageApplication;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.httpevent.publish.BrowserWindow;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.menu.MenuProducer;
import com.pradera.commons.security.model.type.LanguageType;
import com.pradera.commons.security.model.type.LogoutMotiveType;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.audit.facade.UserAuditingFacade;
import com.pradera.security.model.CatalogueLanguage;
import com.pradera.security.model.ExceptionUserPrivilege;
import com.pradera.security.model.Schedule;
import com.pradera.security.model.ScheduleDetail;
import com.pradera.security.model.ScheduleException;
import com.pradera.security.model.System;
import com.pradera.security.model.SystemOption;
import com.pradera.security.model.TicketsDatamart;
import com.pradera.security.model.UserSession;
import com.pradera.security.model.type.ScheduleDetailStateType;
import com.pradera.security.model.type.ScheduleExceptionDetailStateType;
import com.pradera.security.model.type.ScheduleExceptionStateType;
import com.pradera.security.model.type.ScheduleExceptionType;
import com.pradera.security.model.type.ScheduleStateType;
import com.pradera.security.model.type.ScheduleType;
import com.pradera.security.notification.facade.NotificationServiceFacade;
import com.pradera.security.serverpush.listener.NotificationBrokerBean;
import com.pradera.security.serverpush.view.NotificationBean;
import com.pradera.security.systemmgmt.facade.SystemServiceFacade;
import com.pradera.security.systemmgmt.view.filters.ScheduleDetailFilter;
import com.pradera.security.systemmgmt.view.filters.ScheduleExceptionDetailFilter;
import com.pradera.security.systemmgmt.view.filters.ScheduleExceptionFilter;
import com.pradera.security.systemmgmt.view.filters.ScheduleFilter;
import com.pradera.security.usermgmt.facade.UserServiceFacade;
import com.pradera.security.usermgmt.service.GeneratorMenu;
import com.pradera.security.usermgmt.service.UserAccountServiceBean;
import com.pradera.security.usermgmt.view.filters.UserAccountFilter;
import com.pradera.security.utils.view.PropertiesConstants;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ManagerNavigationBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 26-may-2015
 */
@Named
@SessionScoped
public class ManagerNavigationBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7939392173365192549L;
	
	@Inject
	private transient PraderaLogger log;
	
	@Inject @Push(channel=PropertiesConstants.PUSH_CHANNEL)
	private PushContext omniChannel;
	
	/** The message channel. */
	private String messageChannel;
	
	private String messageChannelOmni;

	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	@Inject
	ApplicationParameter applicationParameter;
	
	/** The notification broker. */
	@Inject
	NotificationBrokerBean notificationBroker;
	
	/** The system service facade. */
	@EJB
    SystemServiceFacade systemServiceFacade;
	
	@EJB
	UserServiceFacade userServiceFacade;
	
	/** The user auditing facade. */
	@EJB
	UserAuditingFacade userAuditingFacade;
	
	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade;
	
	/** The menu producer. */
	@Inject
	MenuProducer menuProducer;
	
	/** The generator menu. */
	@Inject
	private GeneratorMenu generatorMenu;
	@EJB
	UserAccountServiceBean userAccountServiceBean;
	
	/** The user admin. */
	@Inject
	@UserAdmin
	String userAdmin;
	
	@Inject 
	private StageApplication stateApplication;
	
	/** The stage application. */
	@Inject
	StageApplication stageApplication;
	
	private String urlSourceLogin;
	
	private Long userSessionRemote;
	
	/** The notification bean. */
	private NotificationBean notificationBean;
	
	private System currentSystem;
	
	private List<System> listSystems;
	
	/** The message. */
	private String message;
	
	/** The show one time. */
	private boolean showOneTime;
	
	/** The sel direct access. */
	private String selDirectAccess;
	
	/** The current option name. */
	private String currentOptionName;

	/** The current option url. */
	private String currentOptionUrl;
	
	/** The current Tab. */
	private String currentTabId;

	/** The tabHistory. */
	private Map<String, String> tabHistory;
	
	/** The lst direct access. */
	private List<CatalogueLanguage> lstDirectAccess;
	
	@Inject
	ClientRestService clientRestService;

	/**
	 * Instantiates a new manager navigation bean.
	 */
	
	
	/**
	 *  ADD BY JALOR , REFERENCE TO DATAMART 29/01/2016
	 */
	@Inject
	@InjectableResource(location = "ParametersDatawarehouse.properties")
	private Properties applicationDTMConfiguration;
	
	@Inject
	@InjectableResource(location = "ServerConfiguration.properties")
	private Properties serverConfiguration;
	
	/** The server path. */
	@Inject @StageDependent
	private String serverPath;
	
	private Integer indRedirectNewTab = GeneralConstants.ZERO_VALUE_INTEGER;
	
	private Boolean isUatOrProd = Boolean.FALSE;
	
	public ManagerNavigationBean() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		this.isUatOrProd = checkUatOrProd();
		// Chanel global to possibles notifications global
		messageChannel = PropertiesConstants.PUSH_MSG + "/" + userInfo.getUserAccountSession().getUserName();
		messageChannelOmni= PropertiesConstants.PUSH_CHANNEL;
		/* register notification menus */
		notificationBean = new NotificationBean();
		notificationBean.setUserName(userInfo.getUserAccountSession().getUserName());
		notificationBean.setIdSession(userInfo.getUserAccountSession().getIdUserSessionPk());
		notificationBean.setSessionTicket(userInfo.getUserAccountSession().getTicketSession());
		/*
		 * change way how build menu for notifications after
		 * login
		 */
		notificationBroker.createMenuNotifications(notificationBean,NotificationType.MESSAGE.getCode(),BooleanType.NO.getCode());
		// menu process
		notificationBroker.createMenuNotifications(notificationBean,NotificationType.PROCESS.getCode(),BooleanType.NO.getCode());
		notificationBroker.getSetNotificationBean().add(notificationBean);
		setShowOneTime(true);
		lstDirectAccess = new ArrayList<CatalogueLanguage>();
	}
	
	/**
	 * Select system after login. in this method Menu is generate
	 * 
	 * @return the string
	 */
	public String selectSystem() throws ServiceException{
			if (currentSystem != null){
				userInfo.setCurrentSystem(currentSystem.getName());
			if ( ("SISTEMA_"+currentSystem.getName()).indexOf("DATAMART") > 0 ){

					TicketsDatamart ticketsDatamart = new TicketsDatamart();
					ticketsDatamart.setIdTicketsDatamartPk(userInfo.getUserAccountSession().getTicketSession());
					ticketsDatamart.setSystemCode(currentSystem.getIdSystemPk());
					ticketsDatamart.setUseraccountCode(userInfo.getUserAccountSession().getIdUserAccountPk());
					Integer profileCode = userAccountServiceBean.getUserProfile(ticketsDatamart.getSystemCode(),ticketsDatamart.getUseraccountCode());
					ticketsDatamart.setProfileCode(profileCode);
					ticketsDatamart.setInstitutionTypeCode(userInfo.getUserAccountSession().getInstitutionType());
					ticketsDatamart.setInstitutionTypeName(userInfo.getUserAccountSession().getDescInstitutionType());
					ticketsDatamart.setInstitutionCode(userInfo.getUserAccountSession().getInstitutionCode());
					systemServiceFacade.saveTicketDatamart(ticketsDatamart);
				}
			
			boolean isScheduleException = false;
			ScheduleException scheduleException = systemServiceFacade
					.getScheduleExceptionBySystem(currentSystem.getIdSystemPk(), userInfo.getUserAccountSession().getIdUserAccountPk());
			if (Validations.validateIsNotNull(scheduleException)) {
				isScheduleException = true;
			}
			if (!isScheduleException) {
				ScheduleFilter scheduleFilter = new ScheduleFilter();
				scheduleFilter.setScheduleType(ScheduleType.SYSTEMS.getCode());
				scheduleFilter.setIdSystem(currentSystem.getIdSystemPk());
				scheduleFilter.setScheduleState(ScheduleStateType.CONFIRMED.getCode());
				ScheduleDetailFilter scheduleDetailFilter = new ScheduleDetailFilter();
				scheduleDetailFilter.setDay(Calendar.getInstance().get(Calendar.DAY_OF_WEEK));
				scheduleDetailFilter.setHour(CommonsUtilities.getDefaultDateWithCurrentTime());
				scheduleDetailFilter.setScheduleDetailState(ScheduleDetailStateType.REGISTERED.getCode());
				scheduleFilter.setScheduleDetailFilter(scheduleDetailFilter);
				// Only check to users normals
				if (!userAdmin.equalsIgnoreCase(userInfo.getUserAccountSession().getUserName())) {
					ScheduleDetail scheduleDetail = systemServiceFacade.getScheduleDetailBySystemServiceFacade(scheduleFilter);
					if (Validations.validateIsNull(scheduleDetail)) {
						JSFUtilities.showMessageOnDialog(null,null,PropertiesConstants.ERROR_LOGIN_SELECT_SYSTEM_SCHEDULE_INVALID,null);
						JSFUtilities.addContextMessage(null,FacesMessage.SEVERITY_ERROR, "", "");
						JSFUtilities.showValidationDialog();
						return null;
					}
					Date endTimeConnection = CommonsUtilities.setTimeToDate(CommonsUtilities.currentDate(),scheduleDetail.getEndDate());
					userInfo.setEndTimeConnection(endTimeConnection);
					if (CommonsUtilities.currentDateTime().after(endTimeConnection)) {
						JSFUtilities.showMessageOnDialog(null,null,PropertiesConstants.ERROR_LOGIN_SELECT_SYSTEM_SCHEDULE_INVALID,null);
						JSFUtilities.addContextMessage(null,FacesMessage.SEVERITY_ERROR, "", "");
						JSFUtilities.showValidationDialog();
						return null;
					}
				}

			}
			UserAccountFilter userAccountFilter = new UserAccountFilter();
			userAccountFilter.setIdUserAccountPK(userInfo.getUserAccountSession().getIdUserAccountPk());
			userAccountFilter.setIdSystemPk(currentSystem.getIdSystemPk());
			// Get options to MENU
			// Select language of session for default spanish
			String languageSelected = LanguageType.SPANISH.getValue();
			if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("locale") != null) {
				languageSelected = FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("locale").toString();
			}
			for (LanguageType language : LanguageType.list) {
				if (language.getValue().equals(languageSelected)) {
					userAccountFilter.setIdLanguage(language.getCode());
					break;
				}
			}
			List<String> lstAvailiableOptionsUrl = new ArrayList<String>();
			List<SystemOption> lstSystemOption = userServiceFacade.getOptionsForMenuGeneration(userAccountFilter,lstAvailiableOptionsUrl);
			if (Validations.validateListIsNullOrEmpty(lstSystemOption)) {
				JSFUtilities.showMessageOnDialog(null, null,PropertiesConstants.No_MENUS_AVAILABLE_FOR_SYSTEM, null);
				// JSFUtilities.addContextMessage(null,FacesMessage.SEVERITY_INFO,"","");
				JSFUtilities.showValidationDialog();
				return null;
			}
			// Change current System
			UserSession sesionRegistered = new UserSession();
			sesionRegistered.setIdUserSessionPk(userInfo.getUserAccountSession().getIdUserSessionPk());
			sesionRegistered.setSystem(currentSystem);
			userServiceFacade.updateCurrentSystemUserSession(sesionRegistered);
			// Generation Menu
			String urlPath=currentSystem.getSystemUrl();
			if(stateApplication==StageApplication.Development) { 
				urlPath=serverPath;
			}
			userInfo.setPraderaMenuAll(generatorMenu.generateMenu(lstSystemOption, urlPath));
			//Generate Privileges
			menuProducer.createPrivilegeUser(userInfo);
			userInfo.setAvailiableOptionsUrl(lstAvailiableOptionsUrl);
			// if the request is remote redirect to origin url
//			if (StringUtils.isNotBlank(urlSourceLogin) && stageApplication != null
//					&& (stageApplication.equals(StageApplication.Development))) {
//				FacesContext fccontext = FacesContext.getCurrentInstance();
//				ExternalContext eContext = fccontext.getExternalContext();
//				try{
//				eContext.redirect(urlSourceLogin + "?tikectId="
//						+ userInfo.getUserAccountSession().getTicketSession());
//				} catch(IOException io){
//					
//				}
//				return null;
//			}
		}
		return "welcomeRule";
	}
	

	
	public void testPushOmni() {
		BrowserWindow browser = new BrowserWindow();
		browser.setNotificationType(NotificationType.KILLSESSION.getCode());
		browser.setSubject("Expire Session");
		browser.setMessage(PropertiesConstants.SESSION_CLOSED_BY_REMOTE_USER);
		
		omniChannel.send(browser,userInfo.getUserAccountSession().getUserName()); 
	}

	public void testPush() {
		EventBus eventBus = EventBusFactory.getDefault().eventBus();
		
		BrowserWindow browser = new BrowserWindow();
		browser.setNotificationType(NotificationType.KILLSESSION.getCode());
		browser.setSubject("Expire Session");
		browser.setMessage(PropertiesConstants.SESSION_CLOSED_BY_REMOTE_USER);
		String canal=PropertiesConstants.PUSH_MSG + "/"+ userInfo.getUserAccountSession().getUserName().trim();
		log.debug("canal "+canal);
		eventBus.publish(canal,browser);
	}
	
	public void testPushResult() {
		log.debug("testPushResult ");
	}
	
	
	public List<UserSession> getAllRemoteSession(){
		List<UserSession> userSessiones=userServiceFacade.allRemoteConnectedByUser( userInfo.getUserAccountSession().getUserName() );
		userSessiones.removeIf(x->x.getIdUserSessionPk().equals(userInfo.getUserAccountSession().getIdUserSessionPk()));
		return userSessiones;
	}
	
	/**
	 * To close remoteSession.
	 * 
	 * @return String
	 */
	public String closeRemoteSession() throws ServiceException {
			if(userSessionRemote!=null){
				UserSession remoteUserSession = new UserSession();
				remoteUserSession.setIdUserSessionPk(userSessionRemote);
				remoteUserSession.setLogoutMotiveFk(LogoutMotiveType.KILLREMOTEUSERSESSION.getCode());
				remoteUserSession.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
				remoteUserSession.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
				userServiceFacade.closeMultipleSessionFacade(remoteUserSession,
						userInfo.getUserAccountSession().getIdUserSessionPk(),
						userInfo.getUserAccountSession().getIdUserAccountPk());
				//audit user
				userAuditingFacade.registerUserAuditTrackingServiceFacade(
						"/iSecurity/pages/selectSystem.xhtml", "", "",
						CommonsUtilities.currentDateTime(), userInfo.getUserAccountSession().getIdUserSessionPk(), null);
				// To remove session variable
				JSFUtilities.removeHttpSessionAttribute("multipleUserSession");
				userSessionRemote = null;
				// To close Reomote user Session
				
//				PushContext pushContext = PushContextFactory.getDefault()
//						.getPushContext();
//				EventBus eventBus = EventBusFactory.getDefault().eventBus();
				
				// integration with notifications
				BrowserWindow browser = new BrowserWindow();
				browser.setNotificationType(NotificationType.KILLSESSION.getCode());
				// TODO change hardcode message
				browser.setSubject("Expire Session");
				browser.setMessage(PropertiesConstants.SESSION_CLOSED_BY_REMOTE_USER);
				
				//pushContext.push(PropertiesConstants.PUSH_MSG + "/"+ userInfo.getUserAccountSession().getUserName().trim(),browser);
//				eventBus.publish(PropertiesConstants.PUSH_MSG + "/"+ userInfo.getUserAccountSession().getUserName().trim(),browser);
				omniChannel.send(browser,userInfo.getUserAccountSession().getUserName());
			}
		return "selectSystem";
	}
	
	/**
	 * To close current session.
	 * 
	 * @return string
	 */
	public String closeCurrentSession() throws ServiceException {
			// To kill Remote User Session.
			userInfo.setLogoutMotive(LogoutMotiveType.KILLCURRENTUSERSESSION);
			UserSession session = new UserSession();
			session.setIdUserSessionPk(userInfo.getUserAccountSession().getIdUserSessionPk());
			session.setLogoutMotiveFk(LogoutMotiveType.KILLCURRENTUSERSESSION.getCode());
			session.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
			session.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
			userServiceFacade.closeMultipleSessionFacade(session);
			JSFUtilities.killSession();
		return "loginPage";
	}
	
	/**
	 * Close session User from Menu Logout action.
	 * 
	 * @return the string
	 */
	public String closeSessionAction() throws ServiceException {
			userInfo.setLogoutMotive(LogoutMotiveType.LOGOUTSESSION);
			JSFUtilities.setHttpSessionAttribute(GeneralConstants.LOGOUT_MOTIVE,LogoutMotiveType.LOGOUTSESSION);
			//remove menu notification
			notificationBroker.getSetNotificationBean().remove(notificationBean);
			UserSession session = new UserSession();
			session.setIdUserSessionPk(userInfo.getUserAccountSession().getIdUserSessionPk());
			session.setLogoutMotiveFk(LogoutMotiveType.LOGOUTSESSION.getCode());
			session.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
			session.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
			String userName = userInfo.getUserAccountSession().getUserName();
			//kill http session
			JSFUtilities.killSession();
			//logout in database
			userServiceFacade.closeMultipleSessionFacade(session);
			//close session in remote wars
			userServiceFacade.closeSessionModules(userName);
		return "loginPage";
	}
	
	/**
	 * Browser closed.
	 */
	public void browserClosed() {
			userInfo.setLogoutMotive(LogoutMotiveType.LOGOUTSESSION);
			JSFUtilities.setHttpSessionAttribute(GeneralConstants.LOGOUT_MOTIVE,LogoutMotiveType.LOGOUTSESSION);
			//remove menu notification
			notificationBroker.getSetNotificationBean().remove(notificationBean);
			JSFUtilities.killSession();
	}
	
	/**
	 * Idle time logout user session.
	 */
	public void sessionLogout() throws ServiceException {
			userInfo.setLogoutMotive(LogoutMotiveType.EXPIREDSESSION);
			JSFUtilities.setHttpSessionAttribute(GeneralConstants.LOGOUT_MOTIVE,LogoutMotiveType.EXPIREDSESSION);
			//remove menu notification
			notificationBroker.getSetNotificationBean().remove(notificationBean);
			UserSession session = new UserSession();
			session.setIdUserSessionPk(userInfo.getUserAccountSession().getIdUserSessionPk());
			session.setLogoutMotiveFk(LogoutMotiveType.LOGOUTSESSION.getCode());
			session.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
			session.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
			String userName = userInfo.getUserAccountSession().getUserName();
			JSFUtilities.killSession();
			JSFUtilities.showMessageOnDialog(null, null,PropertiesConstants.IDLE_TIME_EXPIRED, null);
			JSFUtilities.showComponent(":idCnfDialogKillSession");
			//kill in DB
			//logout in database
			userServiceFacade.closeMultipleSessionFacade(session);
			//close session in remote wars
			userServiceFacade.closeSessionModules(userName);
	}
	
	/**
	 * Close system action.
	 * 
	 * @return the string
	 */
	public String closeSystemAction() throws ServiceException{
			JSFUtilities.hideGeneralDialogues();
			userInfo.setCurrentSystem(null);
			//audit user
			userAuditingFacade.registerUserAuditTrackingServiceFacade(
					"/iSecurity/pages/selectSystem.xhtml", "", "",
					CommonsUtilities.currentDateTime(), userInfo.getUserAccountSession().getIdUserSessionPk(), null);
			listSystems.clear();
			listSystems = getSystemsBySchedule();
			// to check if there is any access schedule exception for the system
			listSystems.addAll(getExceptionalSystemList(ScheduleExceptionType.ACCESS_ACEPT.getCode()));
			// To get the userExceptionprivileges for the current date if
			// available
			List<ExceptionUserPrivilege> userExceptionPrivileges = systemServiceFacade.listUserExceptionPrivilegeServiceFacade(userInfo
							.getUserAccountSession().getIdUserAccountPk());
			if (Validations.validateIsNotNull(userExceptionPrivileges)
					&& !userExceptionPrivileges.isEmpty()) {
				// For each system on the list of exceptions, add the system to
				// lstSystemsByUser
				for (ExceptionUserPrivilege userExceptionPrivile : userExceptionPrivileges) {
					listSystems.add(systemServiceFacade.getSystemWithImage(userExceptionPrivile.getSystem().getIdSystemPk()));
				}
			}
			List<System> tmpList = new ArrayList<System>();
			tmpList.addAll(listSystems);
			listSystems.clear();
			// to check if there is any deny schedule exception for the system
			List<System> deniedSystems = getExceptionalSystemList(ScheduleExceptionType.ACCESS_REFUSE.getCode());
			List<System> lstSystemsAvailable = new ArrayList<System>();
			lstSystemsAvailable.addAll(tmpList);
			// to remove systems which is having deny access to schedule
			// exception.
			if (Validations.validateIsNotNull(deniedSystems) && deniedSystems.size() != 0) {
				for (System deniedSystem : deniedSystems) {
					for (System sys : lstSystemsAvailable) {
						if (sys.getIdSystemPk().equals(deniedSystem.getIdSystemPk())) {
							tmpList.remove(sys);
						}
					}
				}
			}
			lstSystemsAvailable.clear();
			// To remove the duplicates
			if (tmpList != null && !tmpList.isEmpty()) {
				for (System system : tmpList) {
					if (!validateSystemInList(listSystems, system)) {
						System systemAllow = new System();
						systemAllow.setIdSystemPk(system.getIdSystemPk());
						systemAllow.setName(system.getName());
						systemAllow.setSystemUrl(system.getSystemUrl());
						systemAllow.setIndDisplayIcon(system.getIndDisplayIcon());
						listSystems.add(systemAllow);
					}
				}
			}
		return "selectSystem";
	}
	
	
	
	/**
	 * Close session action.
	 * 
	 */
	public void killSessionAction() {
		log.debug("killSessionAction");
		userInfo.setLogoutMotive(LogoutMotiveType.KILLSESSION);
		notificationBroker.getSetNotificationBean().remove(notificationBean);
		String userName = userInfo.getUserAccountSession().getUserName();
		JSFUtilities.setHttpSessionAttribute(GeneralConstants.LOGOUT_MOTIVE,LogoutMotiveType.KILLSESSION);		
		JSFUtilities.killSession();
		String message = getMessage();
		if(Validations.validateIsNotNullAndNotEmpty(message) && PropertiesConstants.SESSION_EXPIRED_USER_UNREGISTER.equals(message)){
			JSFUtilities.showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT, null,
					PropertiesConstants.SESSION_EXPIRED_USER_UNREGISTER,
					new Object[] { userInfo.getUserAccountSession().getUserName() });
			
		}else if (Validations.validateIsNotNullAndNotEmpty(message) && PropertiesConstants.SESSION_EXPIRED_USER_BLOCKED.equals(message)) {
			JSFUtilities.showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT, null,
					PropertiesConstants.SESSION_EXPIRED_USER_BLOCKED,
					new Object[] { userInfo.getUserAccountSession().getUserName() });
			
		} else if (Validations.validateIsNotNullAndNotEmpty(message) && PropertiesConstants.SESSION_EXPIRED_SYSTEM_PROFILE_BLOCKED.equals(message)) {
			JSFUtilities.showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT, null,
					PropertiesConstants.SESSION_EXPIRED_SYSTEM_PROFILE_BLOCKED,	null);
			
		}else if (Validations.validateIsNotNullAndNotEmpty(message) && PropertiesConstants.SESSION_CLOSED_BY_REMOTE_USER.equals(message)) {
			JSFUtilities.showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT, null,
					PropertiesConstants.SESSION_CLOSED_BY_REMOTE_USER,	null);
			
		} else {
			JSFUtilities.showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT, null,
					PropertiesConstants.SESSION_EXPIRED, null);
		}
		JSFUtilities.showComponent(":idCnfDialogKillSession");
		//kill sessions in remote wars
		userServiceFacade.closeSessionModules(userName);
	}
	
	/**
	 * Begin close session listener.
	 * 
	 * @param actionEvent
	 *            the action event
	 */
	public void beginCloseSessionListener(ActionEvent actionEvent) {
		JSFUtilities.hideGeneralDialogues();
		JSFUtilities.showComponent("cnfCloseSession");
	}

	/**
	 * Begin close system listener.
	 * 
	 * @param actionEvent
	 *            the action event
	 */
	public void beginCloseSystemListener(ActionEvent actionEvent) {
		JSFUtilities.hideGeneralDialogues();
		Object[] arrBodyData = { userInfo.getCurrentSystem() };
		JSFUtilities.showMessageOnDialog(null, null,PropertiesConstants.LOGIN_SESSION_CLOSE_SYSTEM_BODY,arrBodyData);
		JSFUtilities.showComponent("cnfCloseSystem");
	}
	
//	/**
//	 * To close remoteSession.
//	 * 
//	 * @return String
//	 */
//	public void closebrowserOnSession() {
//		try {
//			// To kill Remote User Session.
//			userInfo.setLogoutMotive(LogoutMotiveType.KILLREMOTEUSERSESSION);
//			remoteUserSession
//					.setLogoutMotiveFk(LogoutMotiveType.KILLREMOTEUSERSESSION
//							.getCode());
//			userServiceFacade.closeMultipleSessionFacade(remoteUserSession);
//			remoteUserSession = null;
//			// To audit Select System
//			auditSelectSystem();
//			// To remove session variable
//			JSFUtilities.removeHttpSessionAttribute("multipleUserSession");
//			// To close Reomote user Session
//			PushContext pushContext = PushContextFactory.getDefault()
//					.getPushContext();
//			// integration with notifications
//			BrowserWindow browser = new BrowserWindow();
//			browser.setNotificationType(NotificationType.KILLSESSION.getCode());
//			// TODO change hardcode message
//			browser.setSubject("Expire Session");
//			browser.setMessage(PropertiesConstants.SESSION_CLOSED_BY_REMOTE_USER);
//			pushContext.push(PropertiesConstants.PUSH_MSG + "/"
//					+ userInfo.getUserAccountSession().getUserName().trim(),
//					browser);
//		} catch (ServiceException e) {
//			excepcion.fire(new ExceptionToCatchEvent(e));
//		}
//
//	}
	
	/**
	 * Update messages notification.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	public void updateMessagesNotification() throws ServiceException {
		if (notificationBean.getTotalMessages() > 0) {
			LoggerUser loggerUser = new LoggerUser();
			loggerUser.setIpAddress(JSFUtilities.getRemoteIpAddress());
			loggerUser.setUserName(userInfo.getUserAccountSession().getUserName());
			loggerUser.setIdUserSessionPk(userInfo.getUserAccountSession().getIdUserSessionPk());
			loggerUser.setSessionId(userInfo.getUserAccountSession().getTicketSession());
			loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
			loggerUser.setIdPrivilegeOfSystem(GeneralConstants.THREE_VALUE_INTEGER);
			ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
			notificationServiceFacade.updateNotificationStatus(userInfo.getUserAccountSession().getUserName(), null,BooleanType.YES.getCode(), NotificationType.MESSAGE
							.getCode());
			notificationBean.setTotalMessages(0);
			notificationBean.setMenuMessageTitle(""+ notificationBean.getTotalMessages());
		}
	}

	/**
	 * Update process notification.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	public void updateProcessNotification() throws ServiceException {
		if (notificationBean.getTotalProcess() > 0) {
			LoggerUser loggerUser = new LoggerUser();
			loggerUser.setIpAddress(JSFUtilities.getRemoteIpAddress());
			loggerUser.setUserName(userInfo.getUserAccountSession().getUserName());
			loggerUser.setIdUserSessionPk(userInfo.getUserAccountSession().getIdUserSessionPk());
			loggerUser.setSessionId(userInfo.getUserAccountSession().getTicketSession());
			loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
			loggerUser.setIdPrivilegeOfSystem(GeneralConstants.THREE_VALUE_INTEGER);
			ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
			notificationServiceFacade.updateNotificationStatus(userInfo.getUserAccountSession().getUserName(), null,BooleanType.YES.getCode(), NotificationType.PROCESS
							.getCode());
			notificationBean.setTotalProcess(0);
			notificationBean.setMenuProcessTitle(""+ notificationBean.getTotalProcess());
		}
	}

	/**
	 * Update report notification.
	 */
	public void updateReportNotification() {
		// clean number reports
		notificationBean.setTotalReports(0);
		notificationBean.setMenuReportTitle(""+ notificationBean.getTotalReports());
	}
	
	/**
	 * Update direct access.
	 */
	public void updateDirectAccess() {
		JSFUtilities.hideGeneralDialogues();
		if (Validations.validateIsNull(tabHistory)) {
			tabHistory = new HashMap<String, String>();
		}
		// To check the tabHistory of current session start
		/*
		tabHistory.put(currentTabId, currentOptionUrl);
		if (tabHistory.size() > 1) {
			if (tabHistory.containsValue(currentOptionUrl)) {
				String tmpTab = null;
				for (Entry<String, String> entry : tabHistory.entrySet()) {
					if (currentOptionUrl.equals(entry.getValue())
							&& !currentTabId.equals(entry.getKey())) {
						tmpTab = entry.getKey();
					}
				}
				if (!currentTabId.equals(tmpTab)
						&& Validations.validateIsNotNullAndNotEmpty(tmpTab)) {
					tabHistory.remove(currentTabId);
					showMessageOnDialog(
							PropertiesConstants.DIALOG_HEADER_ALERT, null,
							PropertiesConstants.SCREEN_ALREADY_OPENED, null);
					JSFUtilities.showComponent(":idcnfclosecurrentpage");
					JSFUtilities.executeJavascriptFunction("generateFrame('')");
				}
			}
		}
		*/
		// To check the tabHistory of current session end

		if (currentOptionUrl != null && currentOptionName != null) {
			List<CatalogueLanguage> lstAccess = getLstDirectAccess();
			for (CatalogueLanguage cl : lstAccess) {
				if (cl.getDescription().equals(currentOptionUrl)) {
					lstAccess.remove(cl);
					break;
				}
			}
			CatalogueLanguage cal = new CatalogueLanguage();
			cal.setCatalogueName(currentOptionName.toUpperCase());
			cal.setDescription(currentOptionUrl);
			lstAccess.add(0, cal);
			if (lstAccess.size() > applicationParameter.getTotalLastUrlMenu().intValue()) {
				lstAccess.remove(applicationParameter.getTotalLastUrlMenu().intValue());
			}
			currentOptionName = null;
			currentOptionUrl = null;
			setSelDirectAccess("-1");
		}
	}
	
	/**
	 * Ocultar dialogos.
	 */
	public void hideDialogs() {
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		// Ocultar Dialogos Generales
		JSFUtilities.hideGeneralDialogues();
	}
	
	public void openPopupPassword(){
		Map<String,Object> options = new HashMap<String, Object>();
		options.put("modal", true);
		options.put("draggable", false);
		options.put("resizable", false);
		options.put("contentWidth", 365);
		RequestContext.getCurrentInstance().openDialog("/template/includes/dialogChangePasswd", options, null);
	}
	
	public void handleReturnPassword(SelectEvent event){
		if(event.getObject()!=null){
			JSFUtilities.executeJavascriptFunction("PF('showpwdDialog').show();");
		}
	}
	
	/**
	 * Metodo que carga la lista de sistemas que esten habilitados para ser mostrados 
	 * indicador que se encuentra en la tabla system.ind_display_icon
	 * 
	 * @return
	 */
	public List<System> getListSystemsDisplay(){
		List<System> listSystemsDisplay = new ArrayList<>();
		
		if(listSystems != null && listSystems.size()>0){			
			for(System system : listSystems){
				if(system.getIndDisplayIcon() != null && system.getIndDisplayIcon().equals(Integer.valueOf("1"))){
					listSystemsDisplay.add(system);
				}
			}
		}
		
		return listSystemsDisplay;
	}
	
	/**
	 * To get the systems list By Schedule.
	 * 
	 * @return list Systems
	 */
	private List<System> getSystemsBySchedule() throws ServiceException{
		List<System> lstSystemsBySchedule = new ArrayList<System>();
			UserAccountFilter userAccountFilter = new UserAccountFilter();
			userAccountFilter.setLoginUserAccount(userInfo.getUserAccountSession().getUserName());
			userAccountFilter.setIdUserAccountPK(userInfo.getUserAccountSession().getIdUserAccountPk());
			userAccountFilter.setInstitutionType(userInfo.getUserAccountSession().getInstitutionType());
			userAccountFilter.setUserAccountState(userInfo.getUserAccountSession().getState());
			ScheduleFilter scheduleFilter = new ScheduleFilter();
			scheduleFilter.setScheduleType(ScheduleType.INSTITUTIONS.getCode());
			scheduleFilter.setInstitutionType(userInfo.getUserAccountSession().getInstitutionType());
			scheduleFilter.setScheduleState(ScheduleStateType.CONFIRMED.getCode());
			// Validate if the institution of the user has registered schedule
			Schedule scheduleInstitution = systemServiceFacade.getScheduleByInstitutionServiceFacade(scheduleFilter);
			if (Validations.validateIsNull(scheduleInstitution)) {
				ScheduleDetailFilter scheduleDetailFilter = new ScheduleDetailFilter();
				scheduleDetailFilter.setDay(Calendar.getInstance().get(Calendar.DAY_OF_WEEK));
				scheduleDetailFilter.setHour(CommonsUtilities.getDefaultDateWithCurrentTime());
				scheduleDetailFilter.setScheduleDetailState(ScheduleDetailStateType.REGISTERED.getCode());
				scheduleFilter.setScheduleDetailFilter(scheduleDetailFilter);
				scheduleFilter.setScheduleType(ScheduleType.SYSTEMS.getCode());
				scheduleFilter.setUserAccountFilter(userAccountFilter);
				// If the institution dont have schedule
				// registered, list all the system
				// that have allowed access to the user for
				// the current date and time
				lstSystemsBySchedule.addAll(systemServiceFacade.listAvailiableSystemsByScheduleServiceFacade(scheduleFilter));
			} else {
				ScheduleDetailFilter scheduleDetailFilter = new ScheduleDetailFilter();
				scheduleDetailFilter.setDay(Calendar.getInstance().get(Calendar.DAY_OF_WEEK));scheduleDetailFilter.setHour(CommonsUtilities
						.getDefaultDateWithCurrentTime());
				scheduleDetailFilter.setScheduleDetailState(ScheduleDetailStateType.REGISTERED.getCode());
				scheduleFilter.setScheduleDetailFilter(scheduleDetailFilter);
				// If the institution of the user have
				// registered schedule, validate if
				// the access is enabled for the current day
				// and time
				ScheduleDetail scheduleDetailUser = systemServiceFacade.getScheduleDetailByUserServiceFacade(scheduleFilter);
				if (Validations.validateIsNotNullAndNotEmpty(scheduleDetailUser)) {
					scheduleFilter.setScheduleType(ScheduleType.SYSTEMS.getCode());
					scheduleFilter.setUserAccountFilter(userAccountFilter);
					// list all the system that have allowed
					// access to the user
					// for the current date and time
					lstSystemsBySchedule.addAll(systemServiceFacade.listAvailiableSystemsByScheduleServiceFacade(scheduleFilter));
				}
			}
		return lstSystemsBySchedule;
	}
	
	/**
	 * To get ExceptionalSystemList.
	 * 
	 * @param exceptionType
	 *            the exception type
	 * @return System list
	 */
	private List<System> getExceptionalSystemList(Integer exceptionType) throws ServiceException {
		List<System> exceptionSystemList = new ArrayList<System>();
			UserAccountFilter userAccountFilter = new UserAccountFilter();
			userAccountFilter.setLoginUserAccount(userInfo.getUserAccountSession().getUserName());
			userAccountFilter.setIdUserAccountPK(userInfo.getUserAccountSession().getIdUserAccountPk());
			userAccountFilter.setInstitutionType(userInfo.getUserAccountSession().getInstitutionType());
			userAccountFilter.setUserAccountState(userInfo.getUserAccountSession().getState());
			ScheduleExceptionFilter scheduleExceptionFilter = new ScheduleExceptionFilter();
			ScheduleExceptionDetailFilter scheduleExceptionDetailFilter = new ScheduleExceptionDetailFilter();
			scheduleExceptionDetailFilter.setUserFilter(userAccountFilter);
			scheduleExceptionFilter.setScheduleDetailExceptionFilter(scheduleExceptionDetailFilter);
			scheduleExceptionDetailFilter.setScheduleExceptionDetailState(ScheduleExceptionDetailStateType.CONFIRMED.getCode());
			scheduleExceptionFilter.setScheduleExceptionState(ScheduleExceptionStateType.CONFIRMED.getCode());
			scheduleExceptionFilter.setDate(CommonsUtilities.currentDate());
			scheduleExceptionFilter.setHour(CommonsUtilities.getDefaultDateWithCurrentTime());
			scheduleExceptionFilter.setScheduleExceptionType(exceptionType);
			// Validate if the user have some schedule exception of
			// exceptionType
			List<ScheduleException> lstScheduleException = systemServiceFacade.listScheduleExceptionByUserServiceFacade(scheduleExceptionFilter);
			if (Validations.validateIsNotNull(lstScheduleException) && !lstScheduleException.isEmpty()) {
				// For each system on the list of exceptions, add
				// the system to lstSystemsByUser
				for (ScheduleException scheduleException : lstScheduleException) {
					exceptionSystemList.add(systemServiceFacade.getSystemWithImage(scheduleException.getSystem().getIdSystemPk()));
				}
			}
		return exceptionSystemList;
	}
	
	/**
	 * Validate system in list.
	 * 
	 * @param lstSystems
	 *            the lst systems
	 * @param system
	 *            the system
	 * @return true, if successful
	 * @throws ServiceException
	 *             the service exception
	 */
	private boolean validateSystemInList(List<System> lstSystems, System system)
			throws ServiceException {
		for (System objSystemAux : lstSystems) {
			if (system.getIdSystemPk().equals(objSystemAux.getIdSystemPk())) {
				return true;
			}
		}
		return false;
	}
	
	public Boolean checkUatOrProd() {
		if(Validations.validateIsNotNull(stageApplication) 
				&& (stageApplication.equals(StageApplication.Production) || stageApplication.equals(StageApplication.SystemTest))) {
			return Boolean.TRUE;
		} else {
			return Boolean.FALSE;
		}
	}
	
	/**
	 * To close the Tab if user close the currentTab.
	 */
	public void closeTab() {
		tabHistory.remove(currentTabId);
	}
	

	
	/**
	 * Checks if is show disclaimer.
	 *
	 * @return true, if is show disclaimer
	 */
	public boolean isShowDisclaimer(){
		if(applicationParameter.getEnabledDisclaimer().equals(BooleanType.YES.getCode()) && showOneTime){
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if is show one time.
	 *
	 * @return true, if is show one time
	 */
	public boolean isShowOneTime() {
		return showOneTime;
	}

	/**
	 * Sets the show one time.
	 *
	 * @param showOneTime the new show one time
	 */
	public void setShowOneTime(boolean showOneTime) {
		this.showOneTime = showOneTime;
	}
	
	/**
	 * Gets the message channel.
	 *
	 * @return the message channel
	 */
	public String getMessageChannel() {
		return messageChannel;
	}

	/**
	 * Sets the message channel.
	 *
	 * @param messageChannel the new message channel
	 */
	public void setMessageChannel(String messageChannel) {
		this.messageChannel = messageChannel;
	}

	/**
	 * Gets the notification bean.
	 *
	 * @return the notification bean
	 */
	public NotificationBean getNotificationBean() {
		return notificationBean;
	}

	/**
	 * Sets the notification bean.
	 *
	 * @param notificationBean the new notification bean
	 */
	public void setNotificationBean(NotificationBean notificationBean) {
		this.notificationBean = notificationBean;
	}

	public System getCurrentSystem() {
		return currentSystem;
	}

	public void setCurrentSystem(System currentSystem) {
		this.currentSystem = currentSystem;
	}

	public List<System> getListSystems() {
		return listSystems;
	}

	public void setListSystems(List<System> listSystems) {
		this.listSystems = listSystems;
	}

	public String getUrlSourceLogin() {
		return urlSourceLogin;
	}

	public void setUrlSourceLogin(String urlSourceLogin) {
		this.urlSourceLogin = urlSourceLogin;
	}

	public Long getUserSessionRemote() {
		return userSessionRemote;
	}

	public void setUserSessionRemote(Long userSessionRemote) {
		this.userSessionRemote = userSessionRemote;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSelDirectAccess() {
		return selDirectAccess;
	}

	public void setSelDirectAccess(String selDirectAccess) {
		this.selDirectAccess = selDirectAccess;
	}

	public String getCurrentOptionName() {
		return currentOptionName;
	}

	public void setCurrentOptionName(String currentOptionName) {
		this.currentOptionName = currentOptionName;
	}

	public String getCurrentOptionUrl() {
		return currentOptionUrl;
	}

	public void setCurrentOptionUrl(String currentOptionUrl) {
		this.currentOptionUrl = currentOptionUrl;
	}

	public Map<String, String> getTabHistory() {
		return tabHistory;
	}

	public void setTabHistory(Map<String, String> tabHistory) {
		this.tabHistory = tabHistory;
	}

	/**
	 * Gets the current tab id.
	 * 
	 * @return the currentTabId
	 */
	public String getCurrentTabId() {
		return currentTabId;
	}

	/**
	 * Sets the current tab id.
	 * 
	 * @param currentTabId
	 *            the currentTabId to set
	 */
	public void setCurrentTabId(String currentTabId) {
		this.currentTabId = currentTabId;
	}

	public List<CatalogueLanguage> getLstDirectAccess() {
		return lstDirectAccess;
	}

	public void setLstDirectAccess(List<CatalogueLanguage> lstDirectAccess) {
		this.lstDirectAccess = lstDirectAccess;
	}

	public String getMessageChannelOmni() {
		return messageChannelOmni;
	}

	public void setMessageChannelOmni(String messageChannelOmni) {
		this.messageChannelOmni = messageChannelOmni;
	}

	/**
	 *  ADD BY JALOR , REFERENCE TO DATAMART 29/01/2016
	 */
	public void datamal(){

		if (Validations.validateIsNotNull(this.currentSystem) && ("SISTEMA_"+this.currentSystem.getName()).indexOf("DATAMART") > 0 ){
			
			/**
			 *  First close session DATAMART
			 */
			executeCloseDatamart();
			JSFUtilities.executeJavascriptFunction("executeDatamart( '" + applicationDTMConfiguration.getProperty("dtm.server.domain") + "','" +
			applicationDTMConfiguration.getProperty("dtm.user.key") + "','" + applicationDTMConfiguration.getProperty("dtm.user.pass") +"','" + 
			userInfo.getUserAccountSession().getTicketSession() +"' );");	
			
		}
	}

	/**
	 *  ADD BY JALOR , REFERENCE TO DATAMART 05/02/2016
	 */
	
	public void executeCloseDatamart(){

		if (Validations.validateIsNotNull(this.currentSystem) && this.currentSystem.getName().indexOf("DATAMART") >= 0){	
			JSFUtilities.executeJavascriptFunction("executeCloseDatamart('" + applicationDTMConfiguration.getProperty("dtm.server.domain") + "');");			
		}
	}

	public Boolean getIsUatOrProd() {
		return isUatOrProd;
	}

	public void setIsUatOrProd(Boolean isUatOrProd) {
		this.isUatOrProd = isUatOrProd;
	}

}
