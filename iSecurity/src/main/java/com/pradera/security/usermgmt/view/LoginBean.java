package com.pradera.security.usermgmt.view;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.event.Event;
import javax.enterprise.inject.Instance;
import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Table;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.apache.myfaces.extensions.cdi.core.api.scope.conversation.ViewAccessScoped;
import org.apache.myfaces.extensions.cdi.core.api.scope.conversation.WindowContext;
import org.omnifaces.util.Faces;
import org.primefaces.context.RequestContext;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.configuration.StageDependent;
import com.pradera.commons.configuration.UserAdmin;
import com.pradera.commons.configuration.application.ApplicationParameter;
import com.pradera.commons.configuration.type.StageApplication;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.security.model.type.LanguageType;
import com.pradera.commons.security.model.type.LoginMessageType;
import com.pradera.commons.security.model.type.LogoutMotiveType;
import com.pradera.commons.security.model.type.UserAccountStateType;
import com.pradera.commons.security.model.type.UserSessionAccessType;
import com.pradera.commons.security.model.type.UserSessionStateType;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.AESKeyType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.AESEncryptUtils;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.audit.facade.UserAuditingFacade;
import com.pradera.security.model.ExceptionUserPrivilege;
import com.pradera.security.model.HistoricalState;
import com.pradera.security.model.Holiday;
import com.pradera.security.model.OptionPrivilege;
import com.pradera.security.model.Schedule;
import com.pradera.security.model.ScheduleDetail;
import com.pradera.security.model.ScheduleException;
import com.pradera.security.model.System;
import com.pradera.security.model.UserAccount;
import com.pradera.security.model.UserSession;
import com.pradera.security.model.type.HolidayStateType;
import com.pradera.security.model.type.ScheduleDetailStateType;
import com.pradera.security.model.type.ScheduleExceptionDetailStateType;
import com.pradera.security.model.type.ScheduleExceptionStateType;
import com.pradera.security.model.type.ScheduleExceptionType;
import com.pradera.security.model.type.ScheduleStateType;
import com.pradera.security.model.type.ScheduleType;
import com.pradera.security.model.type.SystemStateType;
import com.pradera.security.model.type.UserAccountType;
import com.pradera.security.systemmgmt.facade.SystemServiceFacade;
import com.pradera.security.systemmgmt.view.filters.HolidayFilter;
import com.pradera.security.systemmgmt.view.filters.ScheduleDetailFilter;
import com.pradera.security.systemmgmt.view.filters.ScheduleExceptionDetailFilter;
import com.pradera.security.systemmgmt.view.filters.ScheduleExceptionFilter;
import com.pradera.security.systemmgmt.view.filters.ScheduleFilter;
import com.pradera.security.usermgmt.facade.UserServiceFacade;
import com.pradera.security.usermgmt.view.filters.UserAccountFilter;
import com.pradera.security.utils.view.LDAPUtilities;
import com.pradera.security.utils.view.LDAPUtility;
import com.pradera.security.utils.view.PropertiesConstants;
import com.pradera.security.utils.view.SecurityUtilities;

import nl.captcha.Captcha;


/**
 * <ul>
 * <li>
 * Project Pradera. Copyright 2012.</li>
 * </ul>
 * 
 * The Class LogueoBean.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 23/11/2012
 */
@Named
@ViewAccessScoped
public class LoginBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The excepcion. */
	@Inject
	transient Event<ExceptionToCatchEvent> excepcion;
	
	/** The user info. */
	@Inject
	Instance<UserInfo> userInfo;
	
	@Inject
	Instance<ManagerNavigationBean> managerNavigationBean;

	/** The user auditing facade. */
	@EJB
	UserAuditingFacade userAuditingFacade;
	
	/** The user service facade. */
	@EJB
	UserServiceFacade userServiceFacade;
	
	/** The system service facade. */
	@EJB
    SystemServiceFacade systemServiceFacade;
	
	
	/** The security path. */
	@Inject
	@StageDependent
	String securityPath;

	/** The window context. to kill all conversations */
	@Inject
	WindowContext windowContext;

	/** The user admin. */
	@Inject
	@UserAdmin
	private String userAdmin;

	/** The stage application. */
	@Inject
	StageApplication stageApplication;
	
	@Inject
	ApplicationParameter applicationParameter;

	/** The enabled captcha. */
	@Inject
	@Configurable("app.security.captcha.enabled")
	private Integer enabledCaptcha;
	
	/** The enabled totp. */
	private Integer enabledTotp = GeneralConstants.ONE_VALUE_INTEGER;
	
	/** The san configuration. */
	@Inject
	@Configurable("app.security.behavior.san.enabled")
	private boolean sanConfiguration;
	
	/** The captcha url. */
	private String captchaUrl;
	
	/** The totp qa image. */
	private String totpUrl;

	/** The usuario login. */
	@NotNull(message = "{login.user.required}")
	private String loginUser;

	/** The contrasena. */
	@NotNull(message = "{login.pwd.required}")
	private String password;
	
	/** The email. */
	@NotNull(message="{user.email.required}")
	private String email;
	
	/** The totp. */
	@NotNull(message="{user.totp.required}")
	private String totp;
	
	/** The ldap utilities. */
	@Inject
	@LDAPUtility
	LDAPUtilities ldapUtilities;

	@Inject
	@StageDependent
	private String serverPath;
	
	/** The lista sistemas por usuario. */
	private List<System> lstSystemsByUser;

	/** The selected system. */
	private System selectedSystem;

	/** The url source login. */
	private String urlSourceLogin;

	/** The module application. */
	private String moduleApplication;

	/** The idle time logout. */
	private String idleTimePararm;

	/** The session timeout interval. */
	private int sessionTimeoutInterval;

	/** The system. */
	private String system;

	/** The exception in schedule. */
	private boolean exceptionInSchedule;

	/** The userSessionUrl. */
	private String userSessionUrl;

	/** The remote UserSession. */
	private UserSession remoteUserSession;

	/** The user key. */
	private String userKey;

	/** The user pass. */
	private String userPass;
	
	/** The show version info. */
	private boolean showVersionInfo = true;
	
	/** The captcha answer. */
	private String captchaAnswer;
	
	/** The show forgot password. */
	private boolean showForgotPassword;
	
	/** The user info temporal. */
	private UserInfo userInfoTemporal;
	
	/** The return message. */
	private String returnMessage;
	
	private boolean renderTotpQR = false;

	private Boolean showMfa = Boolean.FALSE;
	
	private String userAccountMfaSecret = null;
	
	private LoginMessageType mfaLdapConnectionSuccess = null; 
			
	private UserAccount mfaUserAccount = null;
	
	private UserAccountFilter mfaUserAccountFilter = null;
	
	private StringBuffer mfaStringBuffer = null;
	
	private Boolean isUatOrProd = Boolean.FALSE;
	
	private Boolean captchaIsValid = Boolean.FALSE;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		this.isUatOrProd = checkUatOrProd();
		
		Object object = JSFUtilities.getSessionMap("UserAlreadyLogin");
		// To identify the idle time logout of the previous session
		if (stageApplication != null
				&& stageApplication.equals(StageApplication.Production)) {
			showVersionInfo = false;
		}
		
		enabledTotp = applicationParameter.getEnableTotp();
		
		if (getIdleTimePararm() != null) {
			object = null;
			setIdleTimePararm(null);
			// commented for working in development redirect to local
			// Added the condition for working if the stageApplication value is
			// not Development
			if (stageApplication != null && (!stageApplication.equals(StageApplication.Development))) {
				setUrlSourceLogin(null);
			}
		}

		if (object != null) {
			try {
				ExternalContext ec = FacesContext.getCurrentInstance()
						.getExternalContext();
				ec.redirect(ec.getRequestContextPath()
						+ "/pages/selectSystem.xhtml");
				return;
			} catch (IOException e) {
			}
		}
		if (!FacesContext.getCurrentInstance().isPostback()) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.getViewRoot().setLocale(new Locale("es"));
			JSFUtilities.putSessionMap("locale", context.getViewRoot()
					.getLocale().toString());
			HttpServletRequest request = (HttpServletRequest) FacesContext
					.getCurrentInstance().getExternalContext().getRequest();
			system = request.getContextPath();
			sessionTimeoutInterval = (request.getSession()
					.getMaxInactiveInterval()) * 1000 * 60;
		}
		// clean state of menu
		deleteCookie("panelMenu-menuPanelForm%3AmenuDepository",
				"/iSecurity/pages");
	}
	
	/**
	 * Open popup password.
	 */
	public void openPopupPassword(){
		JSFUtilities.executeJavascriptFunction("PF('dlgwForgot').show();");
		showForgotPassword=true;
		reloadCaptcha();
	}
	
	/**
	 * Instantiates a new login bean.
	 */
	public LoginBean(){
		//captcha servlet
		captchaUrl="/captcha.png";
		
	}
	
	/**
	 * Send password user.
	 */
	@LoggerAuditWeb
	public void sendPasswordUser(){
		if(captchaIsValid) {
			try{
				userServiceFacade.sendForgotPassword(loginUser, email);
				captchaAnswer="";
				JSFUtilities.executeJavascriptFunction("PF('cnfMsgForgotWvar').show();");
			}catch (Exception ex) {
				excepcion.fire(new ExceptionToCatchEvent(ex));
			}
		}
	}
	
	/**
	 * Close popus.
	 */
	public void closePopus(){
		email=null;
		showForgotPassword=false;
		JSFUtilities.executeJavascriptFunction("PF('dlgwForgot').hide();");
		reloadCaptcha();
	}
	
	/**
	 * Chech user exceptions.
	 *
	 * @param userSession the user session
	 * @param scheduleExceptionFilter the schedule exception filter
	 * @param userAccountFilter the user account filter
	 * @throws ServiceException the service exception
	 */
	private void checkUserExceptions(UserSession userSession,ScheduleExceptionFilter scheduleExceptionFilter,UserAccountFilter userAccountFilter) throws ServiceException{
		// Validate if the user have some schedule
		// exception to
		// access to a system
		List<ScheduleException> lstScheduleException = systemServiceFacade.
				listScheduleExceptionByUserServiceFacade(scheduleExceptionFilter);
		exceptionInSchedule = false;
		lstSystemsByUser = new ArrayList<System>();
		if (Validations.validateIsNotNull(lstScheduleException) && !lstScheduleException.isEmpty()) {
			exceptionInSchedule = true;
			// For each system on the list of
			// exceptions, add
			// the system to lstSystemsByUser
			for (ScheduleException scheduleException : lstScheduleException) {
				//lstSystemsByUser.add(systemServiceFacade
				//				.getSystemWithImage(scheduleException.getSystem().getIdSystemPk()));
				lstSystemsByUser.add(scheduleException.getSystem());
			}
			userSession.setScheduleAccessType(UserSessionAccessType.SCHEDULEEXCEPTION.getCode());
			returnMessage = "loginSuccess";
		}
		// To get the userExceptionprivileges for the
		// current date if available
		List<ExceptionUserPrivilege> userExceptionPrivileges = systemServiceFacade
				.listUserExceptionPrivilegeServiceFacade(userInfoTemporal.getUserAccountSession().getIdUserAccountPk());
		if (Validations.validateIsNotNull(userExceptionPrivileges) && !userExceptionPrivileges.isEmpty()) {
			// For each system on the list of
			// exceptions, add
			// the system to lstSystemsByUser
			for (ExceptionUserPrivilege userExceptionPrivile : userExceptionPrivileges) {
				//lstSystemsByUser.add(systemServiceFacade.getSystemWithImage(userExceptionPrivile.getSystem().getIdSystemPk()));
				lstSystemsByUser.add(userExceptionPrivile.getSystem());
			}
		}
		Holiday holiday = null;
		if (!exceptionInSchedule) {
			HolidayFilter holidayFilterSearch = new HolidayFilter();
			holidayFilterSearch.setHolidayState(HolidayStateType.REGISTERED.getCode());
			holidayFilterSearch.setHolidayDate(CommonsUtilities.currentDate());
			// Validate if current date is a holiday
			holiday = systemServiceFacade.findHolidayByFiltersServiceFacade(holidayFilterSearch);
		}
		if (Validations.validateIsNotNullAndNotEmpty(holiday)) {
			JSFUtilities.addContextMessage(
							"frmLogin:cmdLogin",
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities.getMessage(PropertiesConstants.ERROR_LOGIN_HOLIDAY),
							PropertiesUtilities.getMessage(PropertiesConstants.ERROR_LOGIN_HOLIDAY));
		} else {
			ScheduleFilter scheduleFilter = new ScheduleFilter();
			scheduleFilter.setScheduleType(ScheduleType.INSTITUTIONS.getCode());
			// START - Change to Schedule Option
			scheduleFilter.setInstitutionType(userAccountFilter.getInstitutionType());
			// END - Change to Schedule Option
			scheduleFilter.setScheduleState(ScheduleStateType.CONFIRMED.getCode());
			// Validate if the institution of the user
			// has
			// registered schedule
			Schedule scheduleInstitution = systemServiceFacade.getScheduleByInstitutionServiceFacade(scheduleFilter);
			if (Validations.validateIsNull(scheduleInstitution)) {
				ScheduleDetailFilter scheduleDetailFilter = new ScheduleDetailFilter();
				scheduleDetailFilter.setDay(Calendar.getInstance().get(Calendar.DAY_OF_WEEK));
				scheduleDetailFilter.setHour(CommonsUtilities.getDefaultDateWithCurrentTime());
				scheduleDetailFilter.setScheduleDetailState(ScheduleDetailStateType.REGISTERED.getCode());
				scheduleFilter.setScheduleDetailFilter(scheduleDetailFilter);
				scheduleFilter.setScheduleType(ScheduleType.SYSTEMS.getCode());
				scheduleFilter.setUserAccountFilter(userAccountFilter);
				// If the institution dont have schedule
				// registered, list all the system
				// that have allowed access to the user
				// for
				// the current date and time
				lstSystemsByUser.addAll(systemServiceFacade.
						listAvailiableSystemsByScheduleServiceFacade(scheduleFilter));
				if (!lstSystemsByUser.isEmpty()) {
					userSession.setScheduleAccessType(UserSessionAccessType.NORMAL.getCode());
					returnMessage = "loginSuccess";
				} else {
					// If the user don't have access at
					// least to one system, then return
					// to
					// login page
					JSFUtilities.addContextMessage(
									"frmLogin:cmdLogin",
									FacesMessage.SEVERITY_ERROR,
									PropertiesUtilities.getMessage(PropertiesConstants.ERROR_LOGIN_INVALID_SCHEDULE),
									PropertiesUtilities.getMessage(PropertiesConstants.ERROR_LOGIN_INVALID_SCHEDULE));
				}

			} else {
				ScheduleDetailFilter scheduleDetailFilter = new ScheduleDetailFilter();
				scheduleDetailFilter.setDay(Calendar.getInstance().get(Calendar.DAY_OF_WEEK));
				scheduleDetailFilter.setHour(CommonsUtilities.getDefaultDateWithCurrentTime());
				scheduleDetailFilter.setScheduleDetailState(ScheduleDetailStateType.REGISTERED.getCode());
				scheduleFilter.setScheduleDetailFilter(scheduleDetailFilter);
				// If the institution of the user have
				// registered schedule, validate if
				// the access is enabled for the current
				// day
				// and time
				ScheduleDetail scheduleDetailUser = systemServiceFacade
						.getScheduleDetailByUserServiceFacade(scheduleFilter);
				if (Validations.validateIsNotNullAndNotEmpty(scheduleDetailUser)) {
					scheduleFilter.setScheduleType(ScheduleType.SYSTEMS.getCode());
					scheduleFilter.setUserAccountFilter(userAccountFilter);
					// list all the system that have
					// allowed
					// access to the user
					// for the current date and time
					lstSystemsByUser.addAll(systemServiceFacade.listAvailiableSystemsByScheduleServiceFacade(scheduleFilter));
					if (!lstSystemsByUser.isEmpty()) {
						userSession.setScheduleAccessType(UserSessionAccessType.NORMAL.getCode());
						returnMessage = "loginSuccess";
					} else {
						// If the user don't have access
						// at
						// least to one system, then
						// return
						// to login page
						JSFUtilities.addContextMessage("frmLogin:cmdLogin",
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities.getMessage(PropertiesConstants.ERROR_LOGIN_INVALID_SCHEDULE),
										PropertiesUtilities.getMessage(PropertiesConstants.ERROR_LOGIN_INVALID_SCHEDULE));
						reloadCaptcha();
					}

				} else {
					JSFUtilities.addContextMessage("frmLogin:cmdLogin",
									FacesMessage.SEVERITY_ERROR,
									PropertiesUtilities.getMessage(PropertiesConstants.ERROR_LOGIN_INVALID_SCHEDULE),
									PropertiesUtilities.getMessage(PropertiesConstants.ERROR_LOGIN_INVALID_SCHEDULE));
					reloadCaptcha();
				}

			}

		}
	
	}
	
	/**
	 * Register block user.
	 *
	 * @param userAccount the user account
	 * @throws ServiceException the service exception
	 */
	private void registerBlockUser(UserAccount userAccount) throws ServiceException{
		userAccount.setState(UserAccountStateType.BLOCKED.getCode());
		//Register historial change user state
		HistoricalState historicalState=new HistoricalState();
		historicalState.setMotive(Integer.valueOf(6));
		historicalState.setMotiveDescription("Wrong PWD Attempts");
		historicalState.setCurrentState(UserAccountStateType.BLOCKED.getCode());
		historicalState.setRegistryUser(userAccount.getLoginUser());							
		historicalState.setTableName(userAccount.getClass().getAnnotation(Table.class).name());
		historicalState.setBeforeState( UserAccountStateType.CONFIRMED.getCode());
		historicalState.setIdRegister(Long.parseLong(userAccount.getIdUserPk().toString()));
		historicalState.setRegistryDate(CommonsUtilities.currentDateTime());
		historicalState.setLastModifyDate(new Date());
		historicalState.setLastModifyIp(JSFUtilities.getRemoteIpAddress());
		historicalState.setLastModifyUser(userAccount.getLoginUser());
		historicalState.setLastModifyPriv(0);
		ThreadLocalContextHolder.put(PropertiesConstants.HISTORICAL_STATE_LOGGER, historicalState);
		//To close the session of blocked user
		userAccount.setLastModifyDate(new Date());
		userAccount.setLastModifyIp(JSFUtilities.getRemoteIpAddress());
		userAccount.setLastModifyUser(userAccount.getLoginUser());
		userAccount.setLastModifyPriv(0);
		userServiceFacade.updateUserAccountState(userAccount);
	}

	/**
	 * Login action.
	 * 
	 * @return the string
	 */
	public String loginAction() {
		hideDialogs();
		returnMessage = null;
		showMfa = Boolean.FALSE;
		try {
			UserAccountFilter userAccountFilter = new UserAccountFilter();
			userAccountFilter.setLoginUserAccount(loginUser);
			// Validate Username on DB
			if (JSFUtilities.getSessionMap("userfirstlogin") != null){
				JSFUtilities.removeSessionMap("userfirstlogin");
			}
			UserAccount userAccount = userServiceFacade.validateUserServiceFacade(userAccountFilter);
			if (userAccount == null) {
				JSFUtilities.addContextMessage("frmLogin:cmdLogin",
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities.getMessage(PropertiesConstants.ERROR_LOGIN_USER_INVALID_PASSWORD),
								PropertiesUtilities.getMessage(PropertiesConstants.ERROR_LOGIN_USER_INVALID_PASSWORD));
			} else {
				if (userAccount.getState().intValue() != UserAccountStateType.CONFIRMED.getCode()) {
					if (userAccount.getState().intValue() == UserAccountStateType.ANNULLED.getCode()) {
						JSFUtilities.addContextMessage("frmLogin:cmdLogin",
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities.getMessage(PropertiesConstants.ERROR_LOGIN_USER_UNREGISTER),
										PropertiesUtilities.getMessage(PropertiesConstants.ERROR_LOGIN_USER_UNREGISTER));
					}
					if (userAccount.getState().intValue() == UserAccountStateType.BLOCKED.getCode()) {
						JSFUtilities.addContextMessage("frmLogin:cmdLogin",
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities.getMessage(PropertiesConstants.ERROR_LOGIN_USER_BLOCKED),
										PropertiesUtilities.getMessage(PropertiesConstants.ERROR_LOGIN_USER_BLOCKED));
					}
					if (userAccount.getState().intValue() == UserAccountStateType.REGISTERED.getCode()) {
						JSFUtilities.addContextMessage(
										"frmLogin:cmdLogin",
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities.getMessage(PropertiesConstants.ERROR_LOGIN_USER_INACTIVE),
										PropertiesUtilities.getMessage(PropertiesConstants.ERROR_LOGIN_USER_INACTIVE));
					}
					reloadCaptcha();
				} else {
					// Validate user and password on LDAP
					StringBuffer sb = new StringBuffer();
					LoginMessageType ldapConnectionSuccess = null;
					/*if(userAccount.getType().equals(UserAccountType.INTERNAL.getCode()) && !loginUser.equals(userAdmin)){
						if(sanConfiguration){
							//using Active Directory configuration
							ldapConnectionSuccess = ldapUtilities.authenticateUserActiveDirectory(loginUser,decryptPassword(userKey, userPass));
						} else {
							//in testing using ldap configuration
							ldapConnectionSuccess = ldapUtilities.authenticateUser(loginUser,decryptPassword(userKey, userPass), sb);
						}
					} else {
						ldapConnectionSuccess = ldapUtilities.authenticateUser(loginUser,decryptPassword(userKey, userPass), sb);
					}*/
					if(this.ldapUtilities.isEnableAuthentication()) {
						ldapConnectionSuccess = ldapUtilities.authenticateUser(loginUser,decryptPassword(userKey, userPass), sb);
					}else {
						ldapConnectionSuccess = LoginMessageType.LOGIN_SUCCESS;
					}
					password = "";
					userKey = "";
					userPass = "";
					if (ldapConnectionSuccess.getCode() < 0) {
						if(LoginMessageType.USER_LOCKED.equals(ldapConnectionSuccess) && userAccount.getState().intValue() == UserAccountStateType.CONFIRMED.getCode()){
							registerBlockUser(userAccount);
						}
						JSFUtilities.addContextMessage("frmLogin:cmdLogin",FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities.getMessage(ldapConnectionSuccess.getValue()),
								PropertiesUtilities.getMessage(ldapConnectionSuccess.getValue()));
						reloadCaptcha();
					} else {
						// For TOTP MFA Code implementation 
						if(!ldapConnectionSuccess.equals(LoginMessageType.INVALIED_CREDENTIALS) 
								&& BooleanType.YES.getCode().equals(this.enabledTotp)) { 
								
							if(userAccount.getMfaSecret() != null && !userAccount.getMfaSecret().isEmpty()) { // Validate for MFA
								userAccountMfaSecret = userAccount.getMfaSecret();
								mfaLdapConnectionSuccess = ldapConnectionSuccess;
								mfaStringBuffer = sb;
								mfaUserAccount = userAccount;
								mfaUserAccountFilter = userAccountFilter;
								showMfa = Boolean.TRUE;
								/*if(!this.userServiceFacade.validateMFACode(userAccount.getMfaSecret(), totp)) { //mfa failed									
									JSFUtilities.addContextMessage("frmLogin:cmdLogin",FacesMessage.SEVERITY_ERROR,
											PropertiesUtilities.getMessage(LoginMessageType.MFA_INVALIED.getValue()),
											PropertiesUtilities.getMessage(LoginMessageType.MFA_INVALIED.getValue()));
									totp = "";
									loginUser = null;
									captchaAnswer = null;
								}*/
								return returnMessage;
							}else { // register for MFA
								this.totpUrl = this.userServiceFacade.registerMFASecret(userAccount);
								JSFUtilities.executeJavascriptFunction("PF('mfaCodeDialog').show();");
								totp = "";
								loginUser = null;
								captchaAnswer = null;
								return returnMessage;
							}							
						} else {
							String loadDataMsg = this.loadUserLogin(ldapConnectionSuccess, userAccount, userAccountFilter, sb);
							if(loadDataMsg != null) {
								return loadDataMsg;
							}							
						}

					}
				}
			}
			loginUser = null;
			captchaAnswer = null;
		} catch (Exception ex) {
			ex.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		// Adding session attribute to avoid multiple sessions on same browser
		if (Validations.validateIsNotNullAndNotEmpty(returnMessage) && !showMfa) {
			JSFUtilities.putSessionMap("UserAlreadyLogin", true);
			//close scope
//			conversation.close();
		}
		return returnMessage;
	}
	
	public String verifyMFA() {		
		if(!this.userServiceFacade.validateMFACode(userAccountMfaSecret, totp)) { //mfa failed									
			JSFUtilities.addContextMessage("frmLogin:cmdLogin",FacesMessage.SEVERITY_ERROR,
					PropertiesUtilities.getMessage(LoginMessageType.MFA_INVALIED.getValue()),
					PropertiesUtilities.getMessage(LoginMessageType.MFA_INVALIED.getValue()));
			captchaAnswer = null;
		} else {

			try {
				String loadDataMsg = this.loadUserLogin(mfaLdapConnectionSuccess, mfaUserAccount, mfaUserAccountFilter, mfaStringBuffer);
				if(loadDataMsg != null) {
					return loadDataMsg;
				}	
			} catch (Exception ex) {
				ex.printStackTrace();
				excepcion.fire(new ExceptionToCatchEvent(ex));
			}
			totp = "";
			loginUser = null;
			captchaAnswer = null;
			mfaLdapConnectionSuccess = null;
			mfaUserAccount = null;
			mfaUserAccountFilter = null;
			mfaStringBuffer = null;
			
			if (Validations.validateIsNotNullAndNotEmpty(returnMessage)) {
				JSFUtilities.putSessionMap("UserAlreadyLogin", true);
			}
		}		
		return returnMessage;
	}
	
	private String loadUserLogin(LoginMessageType ldapConnectionSuccess, UserAccount userAccount, UserAccountFilter userAccountFilter, StringBuffer sb) throws ServiceException {
		String returnMsg = null;
		userInfoTemporal = new UserInfo();
		// Set Attributes to the Session
		userInfoTemporal.getUserAccountSession().setIdUserAccountPk(userAccount.getIdUserPk());
		userInfoTemporal.getUserAccountSession().setUserName(userAccount.getLoginUser());
		userInfoTemporal.getUserAccountSession().setFullName(userAccount.getFullName());
		userInfoTemporal.getUserAccountSession().setEmail(userAccount.getIdEmail());
		userInfoTemporal.getUserAccountSession().setState(userAccount.getState());
		userInfoTemporal.getUserAccountSession().setUserInstitutionName(userAccount.getInstitutionsSecurity().getInstitutionName());
		userInfoTemporal.getUserAccountSession().setIpAddress(JSFUtilities.getRemoteIpAddress());
		// Information about user type and roles
		userInfoTemporal.getUserAccountSession().setInstitutionType(userAccount.getInstitutionTypeFk());
		userInfoTemporal.getUserAccountSession().setParticipantCode(userAccount.getIdParticipantFk());
		userInfoTemporal.getUserAccountSession().setInstitutionCode(userAccount.getInstitutionsSecurity().getIdInstitutionSecurityPk());
		userInfoTemporal.getUserAccountSession().setIssuerCode(userAccount.getIdIssuerFk());
		userInfoTemporal.getUserAccountSession().setDescUserType(userAccount.getUserTypeDescription());		


		if(InstitutionType.ISSUER_DPF.getCode().equals(userInfoTemporal.getUserAccountSession().getInstitutionType())){
			userInfoTemporal.getUserAccountSession().setPartIssuerCode(userServiceFacade.getParticipantIssuerCode(userAccount.getIdIssuerFk()));
		}
		
		userInfoTemporal.getUserAccountSession().setRegulatorAgent(userAccount.getRegulatorAgent());
		// Responsability(Supervisor/Operator)
		userInfoTemporal.getUserAccountSession().setResponsability(userAccount.getResponsability());
		userInfoTemporal.getUserAccountSession().setDescInstitutionType(
				this.userServiceFacade.getParameter(userAccount.getInstitutionTypeFk()).getDescription());
		String language = (String) JSFUtilities.getSessionMap("locale");
		userInfoTemporal.setCurrentLanguage(language);
		// Make a registration of the session of user on DB
		UserSession userSession = new UserSession();
		userSession.setInitialDate(CommonsUtilities.currentDateTime());
		userSession.setUserAccount(userAccount);
		userSession.setRegistryUser((userInfoTemporal.getUserAccountSession().getUserName()));
		userSession.setRegistryDate(CommonsUtilities.currentDateTime());
		userSession.setLastModifyUser((userInfoTemporal.getUserAccountSession().getUserName()));
		userSession.setLastModifyDate(CommonsUtilities.currentDateTime());
		userSession.setLastModifyIp(userInfoTemporal.getUserAccountSession().getIpAddress());
		userSession.setScheduleAccessType(0);
		userSession.setState(UserSessionStateType.CONNECTED.getCode());
		userSession.setTicket(SecurityUtilities.generateSessionTicket());
		userSession.setLocale(language);
		userSession.setRegistryIp(JSFUtilities.getRemoteIpAddress());
		userInfoTemporal.getUserAccountSession().setTicketSession(userSession.getTicket());
		userInfoTemporal.getUserAccountSession().setLastSucessAccess(userSession.getInitialDate());
		if (ldapConnectionSuccess.equals(LoginMessageType.INVALIED_CREDENTIALS)) {
			userServiceFacade.registerUserSessionServiceFacade(userSession);
			userInfoTemporal.getUserAccountSession().setIdUserSessionPk(userSession.getIdUserSessionPk());
			userInfoTemporal.setLogoutMotive(LogoutMotiveType.INVALIEDCREDENTIALS);
			JSFUtilities.setHttpSessionAttribute(GeneralConstants.LOGOUT_MOTIVE,userInfoTemporal.getLogoutMotive());
			JSFUtilities.setHttpSessionAttribute(GeneralConstants.USER_INFO_ID, userInfoTemporal.getUserAccountSession().getIdUserSessionPk());
			FacesContext context = FacesContext.getCurrentInstance();
			context.getViewRoot().setLocale(new Locale(language));
			JSFUtilities.putSessionMap("locale", context.getViewRoot().getLocale().toString());
			if(sb.length() > 0){
				JSFUtilities.addContextMessage("frmLogin:cmdLogin",
						FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities.getMessage("login.invalied.credentials.attempts", sb.toString()),
						PropertiesUtilities.getMessage("login.invalied.credentials.attempts", sb.toString()));								
			}else{
				String summary,detail;
				if(sanConfiguration && userAccount.getType().equals(UserAccountType.INTERNAL.getCode())){
					summary=PropertiesUtilities.getMessage(PropertiesConstants.ERROR_LOGIN_ACTIVE_DIRECTORY);
					detail=PropertiesUtilities.getMessage(PropertiesConstants.ERROR_LOGIN_ACTIVE_DIRECTORY);
				} else {
					summary=PropertiesUtilities.getMessage(ldapConnectionSuccess.getValue());
					detail=PropertiesUtilities.getMessage(ldapConnectionSuccess.getValue());
				}
				JSFUtilities.addContextMessage("frmLogin:cmdLogin",FacesMessage.SEVERITY_ERROR,summary,detail);
			}
			reloadCaptcha();
		} else {
			userAccountFilter.setIdUserAccountPK(userAccount.getIdUserPk());
			// START - Change to Schedule Option
			userAccountFilter.setInstitutionType(userAccount.getInstitutionTypeFk());
			// END - Change to Schedule Option
			userAccountFilter.setIdInstitutionSecurity(userAccount.getInstitutionsSecurity().getIdInstitutionSecurityPk());
			// To set userAccountState
			userAccountFilter.setUserAccountState(userAccount.getState());
			ScheduleExceptionFilter scheduleExceptionFilter = new ScheduleExceptionFilter();
			ScheduleExceptionDetailFilter scheduleExceptionDetailFilter = new ScheduleExceptionDetailFilter();
			scheduleExceptionDetailFilter.setUserFilter(userAccountFilter);
			scheduleExceptionFilter.setScheduleDetailExceptionFilter(scheduleExceptionDetailFilter);
			scheduleExceptionDetailFilter.setScheduleExceptionDetailState(ScheduleExceptionDetailStateType.CONFIRMED.getCode());
			scheduleExceptionFilter.setScheduleExceptionState(ScheduleExceptionStateType.CONFIRMED.getCode());
			scheduleExceptionFilter.setDate(CommonsUtilities.currentDate());
			scheduleExceptionFilter.setHour(CommonsUtilities.getDefaultDateWithCurrentTime());
			scheduleExceptionFilter.setScheduleExceptionType(ScheduleExceptionType.ACCESS_ACEPT.getCode());
			// Check if the user is Admin not check exceptions
			if (userAdmin.equalsIgnoreCase(loginUser)) {
				lstSystemsByUser = systemServiceFacade.searchSystemByStateServiceFacade(SystemStateType.CONFIRMED.getCode());
				returnMessage = "loginSuccess";
			} else {
				//Check User Exceptions
				checkUserExceptions(userSession, scheduleExceptionFilter, userAccountFilter);
			}
			// to check if there is any deny schedule exception for the system
			scheduleExceptionFilter.setScheduleExceptionType(ScheduleExceptionType.ACCESS_REFUSE.getCode());
			List<ScheduleException> deniedSystems = systemServiceFacade.listScheduleExceptionByUserServiceFacade(scheduleExceptionFilter);
			List<System> lstSystemsAvailable = new ArrayList<System>();
			lstSystemsAvailable.addAll(lstSystemsByUser);
			// to remove systems which is having deny access to
			// schedule exception.
			if (Validations.validateIsNotNull(deniedSystems) && deniedSystems.size() != 0) {
				for (ScheduleException se : deniedSystems) {
					for (System sys : lstSystemsAvailable) {
						if (sys.getIdSystemPk().equals(se.getSystem().getIdSystemPk())) {
							selectedSystem = sys;
							lstSystemsByUser.remove(sys);
						}
					}
				}
				if (lstSystemsByUser.size() == 0) {
					JSFUtilities.addContextMessage("frmLogin:cmdLogin",
									FacesMessage.SEVERITY_ERROR,
									PropertiesUtilities.getMessage(PropertiesConstants.ERROR_LOGIN_INVALID_SCHEDULE),
									PropertiesUtilities.getMessage(PropertiesConstants.ERROR_LOGIN_INVALID_SCHEDULE));
					reloadCaptcha();
					return null;
				}
			}
			// To remove the duplicate systems
			lstSystemsAvailable.clear();
			lstSystemsAvailable.addAll(lstSystemsByUser);
			lstSystemsByUser.clear();
			// To remove the duplicates
			if (lstSystemsAvailable != null && !lstSystemsAvailable.isEmpty()) {
				for (System system : lstSystemsAvailable) {
					if (!validateSystemInList(lstSystemsByUser,system)) {
						lstSystemsByUser.add(system);
					}
				}
			}
			lstSystemsAvailable.clear();
			// To check whether the user is logged in another
			// session
			if (checkUserCurrentStatus() && (ldapConnectionSuccess.equals(LoginMessageType.LOGIN_SUCCESS) || ldapConnectionSuccess.equals(LoginMessageType.PWD_IN_EXP_WARING))) {
				JSFUtilities.putSessionMap("multipleUserSession", Boolean.TRUE);
				returnMessage = "multipleUserSession";
			}
			// Registering Current userSession and Login Success
			userServiceFacade.registerUserSessionServiceFacade(userSession);
			//Update access date user in system
			userServiceFacade.updateAccessSystemUser(userAccount.getIdUserPk());
			userInfoTemporal.getUserAccountSession().setIdUserSessionPk(userSession.getIdUserSessionPk());
			//Login is success so create userInfo Session Scoped
			UserInfo userInfoSuccess = createUserInfoSession();
			// Audit only when there is no any remotesession
			// active.
			if (remoteUserSession == null) {
				userAuditingFacade.registerUserAuditTrackingServiceFacade(
						"/iSecurity/pages/selectSystem.xhtml", "", "",
						CommonsUtilities.currentDateTime(), userInfoSuccess.getUserAccountSession().getIdUserSessionPk(), null);
			}
			JSFUtilities.setHttpSessionAttribute(
					GeneralConstants.USER_INFO_ID,userInfoSuccess.getUserAccountSession().getIdUserSessionPk());
			//remove captcha form session
			Faces.removeSessionAttribute("CAPTCHA_IDEPOSITARY");
			Faces.removeSessionAttribute("RELOAD");
			//end remove captcha
			if (returnMessage != null) {
				//Only show systems where user have privileges
				ManagerNavigationBean managerNavigation= managerNavigationBean.get();
				if(remoteUserSession!=null){
					managerNavigation.setUserSessionRemote(remoteUserSession.getIdUserSessionPk());
					
				}
				//If is development pass url
				managerNavigation.setUrlSourceLogin(urlSourceLogin);
				if (Validations.validateIsNotNullAndNotEmpty(lstSystemsByUser)) {
					List<System> tmpList = new ArrayList<System>();
					tmpList.addAll(lstSystemsByUser);
					lstSystemsByUser.clear();
					for (System s : tmpList) {
						List<OptionPrivilege> systemOptions = getProfilesInformation(s.getIdSystemPk(),userInfoSuccess);
						if (Validations.validateListIsNotNullAndNotEmpty(systemOptions)) {
							System systemAllow = new System();
							systemAllow.setIdSystemPk(s.getIdSystemPk());
							systemAllow.setName(s.getName());
							systemAllow.setSystemUrl(s.getSystemUrl());
							systemAllow.setIndDisplayIcon(s.getIndDisplayIcon());
							lstSystemsByUser.add(systemAllow);
						}
					}
				}
				//set systems allows
				managerNavigation.setListSystems(lstSystemsByUser);
				if (ldapConnectionSuccess.equals(LoginMessageType.PWD_MUST_CHANGE_EXP_WARN)
						|| ldapConnectionSuccess.equals(LoginMessageType.PWD_MUST_CHANGE_FIRST_LOGIN)
						|| ldapConnectionSuccess.equals(LoginMessageType.PWD_MUST_CHANGE_GRACE_LOGIN)) {
					JSFUtilities.addContextMessage(null,
									FacesMessage.SEVERITY_ERROR,
									PropertiesUtilities.getMessage(ldapConnectionSuccess.getValue()),
									PropertiesUtilities.getMessage(ldapConnectionSuccess.getValue()));
					JSFUtilities.putSessionMap("userfirstlogin", Boolean.TRUE);
					// Adding session attribute to avoid
					// multiple sessions on same browser
					JSFUtilities.putSessionMap("UserAlreadyLogin", true);
					return "chengePwd";
				} else if (ldapConnectionSuccess.equals(LoginMessageType.PWD_IN_EXP_WARING)) {
					JSFUtilities.addContextMessage(
									null,
									FacesMessage.SEVERITY_WARN,
									PropertiesUtilities.getMessage(ldapConnectionSuccess.getValue()),
									PropertiesUtilities.getMessage(ldapConnectionSuccess.getValue()));
				}
			} else {
				JSFUtilities.killSession();
				// KILL conversation
				windowContext.closeConversations();
				// To set the previous Language if the
				// validation fails
				FacesContext context = FacesContext.getCurrentInstance();
				context.getViewRoot().setLocale(new Locale(language));
				JSFUtilities.putSessionMap("locale", context.getViewRoot().getLocale().toString());
			}
		}
		
		return returnMsg;
	}
	
	/**
	 * Creates the user info session.
	 *
	 * @return the user info
	 */
	private UserInfo createUserInfoSession(){
		UserInfo userInfoSuccess = userInfo.get();
		userInfoSuccess.getUserAccountSession().setIdUserAccountPk(userInfoTemporal.getUserAccountSession().getIdUserAccountPk());
		userInfoSuccess.getUserAccountSession().setIdUserSessionPk(userInfoTemporal.getUserAccountSession().getIdUserSessionPk());
		userInfoSuccess.getUserAccountSession().setUserName(userInfoTemporal.getUserAccountSession().getUserName());
		userInfoSuccess.getUserAccountSession().setFullName(userInfoTemporal.getUserAccountSession().getFullName());
		userInfoSuccess.getUserAccountSession().setEmail(userInfoTemporal.getUserAccountSession().getEmail());
		userInfoSuccess.getUserAccountSession().setState(userInfoTemporal.getUserAccountSession().getState());
		userInfoSuccess.getUserAccountSession().setUserInstitutionName(userInfoTemporal.getUserAccountSession().getUserInstitutionName());
		userInfoSuccess.getUserAccountSession().setIpAddress(userInfoTemporal.getUserAccountSession().getIpAddress());
		userInfoSuccess.getUserAccountSession().setInstitutionType(userInfoTemporal.getUserAccountSession().getInstitutionType());
		userInfoSuccess.getUserAccountSession().setParticipantCode(userInfoTemporal.getUserAccountSession().getParticipantCode());
		userInfoSuccess.getUserAccountSession().setInstitutionCode(userInfoTemporal.getUserAccountSession().getInstitutionCode());
		userInfoSuccess.getUserAccountSession().setIssuerCode(userInfoTemporal.getUserAccountSession().getIssuerCode());
		userInfoSuccess.getUserAccountSession().setRegulatorAgent(userInfoTemporal.getUserAccountSession().getRegulatorAgent());
		userInfoSuccess.getUserAccountSession().setResponsability(userInfoTemporal.getUserAccountSession().getResponsability());
		userInfoSuccess.getUserAccountSession().setTicketSession(userInfoTemporal.getUserAccountSession().getTicketSession());
		userInfoSuccess.getUserAccountSession().setLastSucessAccess(userInfoTemporal.getUserAccountSession().getLastSucessAccess());
		userInfoSuccess.getUserAccountSession().setDescInstitutionType(userInfoTemporal.getUserAccountSession().getDescInstitutionType());
		userInfoSuccess.getUserAccountSession().setDescUserType(userInfoTemporal.getUserAccountSession().getDescUserType());
		userInfoSuccess.setCurrentLanguage(userInfoTemporal.getCurrentLanguage());
		return userInfoSuccess;
	}

	/**
	 * Used to check whether he user logged in another Machine or another
	 * Browser.
	 * 
	 * @return true, if successful
	 */
	public boolean checkUserCurrentStatus() {
		boolean userLogged = false;
		userSessionUrl = null;
		remoteUserSession = null;
		// get Current status of the logged user
		try {
			remoteUserSession = userServiceFacade.getCurrentUserStatus(loginUser);
			if (remoteUserSession != null) {
				userLogged = true;
				userSessionUrl = remoteUserSession.getLastModifyIp();
			}

		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return userLogged;
	}

	/**
	 * Ocultar dialogos.
	 */
	public void hideDialogs() {
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		// Ocultar Dialogos Generales
		JSFUtilities.hideGeneralDialogues();
	}

	/**
	 * Select menu option.
	 * 
	 * @return the string
	 */
	public String selectMenuOption() {
		String url = JSFUtilities.getRequestParameterMap("urlSystemOptionPrm");
		/*return url.replaceAll("http://localhost:8080/iSecurity", "")
				.replaceAll(".xhtml", "");*/
		return url.replaceAll(serverPath+"/iSecurity", "")
				.replaceAll(".xhtml", "");
	}

	/**
	 * Select menu option listener.
	 * 
	 * @param actionEvent
	 *            the action event
	 */
	public void selectMenuOptionListener(ActionEvent actionEvent) {
		try {

			if (CommonsUtilities.currentDateTime().after(userInfo.get().getEndTimeConnection())) {
				FacesContext facesContext = FacesContext.getCurrentInstance();
				String redirect = "/error/privilegesError"; // define the
															// navigation rule
															// that must be used
															// in order to
															// redirect the user
															// to the adequate
															// page...
				NavigationHandler myNav = facesContext.getApplication()
						.getNavigationHandler();
				myNav.handleNavigation(facesContext, null, redirect);
				facesContext.renderResponse();
			} else {
				StringBuilder sbJScript = new StringBuilder();
				sbJScript.append("generateFrame('");
				sbJScript.append(AESEncryptUtils.decrypt(AESKeyType.PRADERAKEY
						.getValue(), JSFUtilities
						.getRequestParameterMap("urlSystemOptionPrm")));
				sbJScript.append("?moduleId=");
				sbJScript.append(JSFUtilities
						.getRequestParameterMap("moduleSystemOptionPrm"));
				sbJScript.append("&userSessionTicket=");
				sbJScript.append(JSFUtilities
						.getRequestParameterMap("ticketSessionPrm"));
				sbJScript.append("&systemOptionId=");
				sbJScript.append(JSFUtilities
						.getRequestParameterMap("idSystemOptionPkPrm"));
				sbJScript.append("&privileges=");
				sbJScript.append(JSFUtilities
						.getRequestParameterMap("userAcctionPrm"));
				// add windowId
				sbJScript.append("&windowId=");
				sbJScript.append(JSFUtilities
						.getRequestParameterMap("windowId"));
				sbJScript.append("');");

				JSFUtilities.executeJavascriptFunction(sbJScript.toString());
			}

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}

	/**
	 * password change completed,then redirecting to select system page.
	 * 
	 * @return to select system page
	 */
	public String pwdChangeSucess() {
		String returnMessage = null;
		try {
			JSFUtilities.removeHttpSessionAttribute("userfirstlogin");
			if (lstSystemsByUser != null && lstSystemsByUser.size() == 1) {
				selectedSystem = lstSystemsByUser.get(0);
//				returnMessage = selectSystem();
			} else {
				returnMessage = "selectSystem";
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		return returnMessage;
	}

	/**
	 * Delete cookie. using for clear state menu after logout
	 * 
	 * @param name
	 *            the name
	 * @param path
	 *            the path
	 */
	private void deleteCookie(String name, String path) {
		Map<String, Object> props = new HashMap<String, Object>();
		// 0 delete inmediate
		props.put("maxAge", 0);
		props.put("path", path);
		FacesContext.getCurrentInstance().getExternalContext()
				.addResponseCookie(name, "", props);
	}

	/**
	 * Validate captcha.
	 *
	 * @param context the context
	 * @param componentToValidate the component to validate
	 * @param value the value
	 * @throws ValidatorException the validator exception
	 */
	public void validateCaptcha(FacesContext context,UIComponent componentToValidate,Object value) throws ValidatorException{
		Captcha simpleCaptcha = Faces.getSessionAttribute("CAPTCHA_IDEPOSITARY");
		 if (value != null && simpleCaptcha != null && simpleCaptcha.isCorrect(value.toString())){
			 captchaIsValid = Boolean.TRUE;
			 return;
		 } else {
			 Faces.setSessionAttribute("RELOAD", "?reload=true");
			// optional: clear field
			((HtmlInputText) componentToValidate).setSubmittedValue("");
			captchaIsValid = Boolean.FALSE;
			throw new ValidatorException(new FacesMessage("Captcha does not match"));
		 }
		
	}

	/**
	 * Gets the login user.
	 * 
	 * @return the login user
	 */
	public String getLoginUser() {
		return loginUser;
	}

	/**
	 * Sets the login user.
	 * 
	 * @param loginUser
	 *            the new login user
	 */
	public void setLoginUser(String loginUser) {
		if (loginUser != null) {
			loginUser = loginUser.toUpperCase();
		}
		this.loginUser = loginUser;
	}

	/**
	 * Gets the password.
	 * 
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 * 
	 * @param password
	 *            the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Gets the lst systems by user.
	 * 
	 * @return the lst systems by user
	 */
	public List<System> getLstSystemsByUser() {
		return lstSystemsByUser;
	}

	/**
	 * Sets the lst systems by user.
	 * 
	 * @param lstSystemsByUser
	 *            the new lst systems by user
	 */
	public void setLstSystemsByUser(List<System> lstSystemsByUser) {
		this.lstSystemsByUser = lstSystemsByUser;
	}

	/**
	 * Gets the selected system.
	 * 
	 * @return the selected system
	 */
	public System getSelectedSystem() {
		return selectedSystem;
	}

	/**
	 * Sets the selected system.
	 * 
	 * @param selectedSystem
	 *            the new selected system
	 */
	public void setSelectedSystem(System selectedSystem) {
		this.selectedSystem = selectedSystem;
	}

	// value change event listener for locale
	/**
	 * Locale code changed.
	 * 
	 * @param e
	 *            the e
	 */
	public void localeCodeChanged(AjaxBehaviorEvent e) {
		String newLocaleValue = ((UIInput) e.getComponent()).getValue()
				.toString();
		FacesContext.getCurrentInstance().getViewRoot()
				.setLocale(new Locale(newLocaleValue));
		JSFUtilities.putSessionMap("locale", new Locale(newLocaleValue));
	}

	/**
	 * Gets the url source login.
	 * 
	 * @return the url source login
	 */
	public String getUrlSourceLogin() {
		return urlSourceLogin;
	}

	/**
	 * Sets the url source login.
	 * 
	 * @param urlSourceLogin
	 *            the new url source login
	 */
	public void setUrlSourceLogin(String urlSourceLogin) {
		this.urlSourceLogin = urlSourceLogin;
	}

	/**
	 * Gets the module application.
	 * 
	 * @return the module application
	 */
	public String getModuleApplication() {
		return moduleApplication;
	}

	/**
	 * Sets the module application.
	 * 
	 * @param moduleApplication
	 *            the new module application
	 */
	public void setModuleApplication(String moduleApplication) {
		this.moduleApplication = moduleApplication;
	}

	/**
	 * Gets the session timeout interval.
	 * 
	 * @return the session timeout interval
	 */
	public int getSessionTimeoutInterval() {
		return sessionTimeoutInterval;
	}

	/**
	 * Sets the session timeout interval.
	 * 
	 * @param sessionTimeoutInterval
	 *            the new session timeout interval
	 */
	public void setSessionTimeoutInterval(int sessionTimeoutInterval) {
		this.sessionTimeoutInterval = sessionTimeoutInterval;
	}

	/**
	 * Session idle listener.
	 */
	public void sessionIdleListener() {
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("sessionExpiredConfirmation.show()");
	}

	/**
	 * Gets the system.
	 * 
	 * @return the system
	 */
	public String getSystem() {
		return system;
	}

	/**
	 * Sets the system.
	 * 
	 * @param system
	 *            the system to set
	 */
	public void setSystem(String system) {
		this.system = system;
	}

	/**
	 * Gets the user session url.
	 * 
	 * @return the userSessionUrl
	 */
	public String getUserSessionUrl() {
		return userSessionUrl;
	}

	/**
	 * Sets the user session url.
	 * 
	 * @param userSessionUrl
	 *            the userSessionUrl to set
	 */
	public void setUserSessionUrl(String userSessionUrl) {
		this.userSessionUrl = userSessionUrl;
	}

	/**
	 * To prevent the system has profiles.
	 * 
	 * @param idSystemPk
	 *            the id system pk
	 * @return the profiles information
	 */
	public List<OptionPrivilege> getProfilesInformation(Integer idSystemPk,UserInfo userInfoCurrent) {
		List<OptionPrivilege> lstSystemOption = null;
		UserAccountFilter userAccountFilter = new UserAccountFilter();
		userAccountFilter.setIdUserAccountPK(userInfoCurrent.getUserAccountSession().getIdUserAccountPk());
		userAccountFilter.setIdSystemPk(idSystemPk);
		String languageSelected = LanguageType.SPANISH.getValue();
		if (FacesContext.getCurrentInstance().getExternalContext()
				.getSessionMap().get("locale") != null) {
			languageSelected = FacesContext.getCurrentInstance()
					.getExternalContext().getSessionMap().get("locale")
					.toString();
		}
		for (LanguageType language : LanguageType.list) {
			if (language.getValue().equals(languageSelected)) {
				userAccountFilter.setIdLanguage(language.getCode());
				break;
			}
		}
		lstSystemOption = userServiceFacade.checkSystemProfiles(userAccountFilter);
		return lstSystemOption;
	}

	

	/**
	 * Gets the remote user session.
	 * 
	 * @return the remoteUserSession
	 */
	public UserSession getRemoteUserSession() {
		return remoteUserSession;
	}

	/**
	 * Sets the remote user session.
	 * 
	 * @param remoteUserSession
	 *            the remoteUserSession to set
	 */
	public void setRemoteUserSession(UserSession remoteUserSession) {
		this.remoteUserSession = remoteUserSession;
	}

	
	/**
	 * Show message on dialog.
	 *
	 * @param keyHeaderProperty the key header property
	 * @param headerData the header data
	 * @param keyBodyProperty the key body property
	 * @param bodyData the body data
	 */
	public void showMessageOnDialog(String keyHeaderProperty,Object[] headerData,String keyBodyProperty, Object[] bodyData){
		Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();	
		String headerMessageConfirmation = "";
		if(keyHeaderProperty!=null && !keyHeaderProperty.isEmpty()){
			headerMessageConfirmation = PropertiesUtilities.getMessage(locale, keyHeaderProperty,headerData);
		}
		String bodyMessageConfirmation = "";
		if(keyBodyProperty!=null && !keyBodyProperty.isEmpty()){
			bodyMessageConfirmation = PropertiesUtilities.getMessage(locale, keyBodyProperty,bodyData);
		}		
		JSFUtilities.putViewMap("bodyMessageView", bodyMessageConfirmation);
		JSFUtilities.putViewMap("headerMessageView", headerMessageConfirmation);
	}
	
	/**
	 * Validate system in list.
	 * 
	 * @param lstSystems
	 *            the lst systems
	 * @param system
	 *            the system
	 * @return true, if successful
	 * @throws ServiceException
	 *             the service exception
	 */
	private boolean validateSystemInList(List<System> lstSystems, System system)
			throws ServiceException {
		for (System objSystemAux : lstSystems) {
			if (system.getIdSystemPk().equals(objSystemAux.getIdSystemPk())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Gets the stage application.
	 * 
	 * @return the stage application
	 */
	public String getStageApplication() {
		return stageApplication.toString();
	}

	public Boolean checkUatOrProd() {
		if(Validations.validateIsNotNull(stageApplication) 
				&& (stageApplication.equals(StageApplication.Production) || stageApplication.equals(StageApplication.SystemTest))) {
			return Boolean.TRUE;
		} else {
			return Boolean.FALSE;
		}
	}

	/**
	 * To decrypt Password.
	 *
	 * @param encryptedPassword the encrypted password
	 * @param keyPass the key pass
	 * @return decryptedPassword
	 */
	public String decryptPassword(String encryptedPassword, String keyPass) {
		String pass = "";
		try {
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine engine = manager.getEngineByName("javascript");
			InputStreamReader isr = new InputStreamReader(
					LoginBean.class
							.getResourceAsStream("/META-INF/resources/js/des.js"));
			Reader reader = isr;
			engine.eval(reader);
			Invocable inv = (Invocable) engine;
			String key = "", vector = null;
			if (keyPass != null && keyPass.length() >= 48) {
				key = keyPass.substring(0, 48);
				if (keyPass.length() >= 64) {
					vector = keyPass.substring(48, 64);
				}
			}
			Object[] args = { key, encryptedPassword, 0,
					vector != null ? 1 : 0, vector, "" };
			pass = (String) inv.invokeFunction("des1", args);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pass;
	}

	/**
	 * Gets the user key.
	 *
	 * @return the user key
	 */
	public String getUserKey() {
		return userKey;
	}

	/**
	 * Sets the user key.
	 *
	 * @param userKey the new user key
	 */
	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}

	/**
	 * Gets the user pass.
	 *
	 * @return the user pass
	 */
	public String getUserPass() {
		return userPass;
	}

	/**
	 * Sets the user pass.
	 *
	 * @param userPass the new user pass
	 */
	public void setUserPass(String userPass) {
		this.userPass = userPass;
	}

	/**
	 * Checks if is show version info.
	 *
	 * @return the showVersionInfo
	 */
	public boolean isShowVersionInfo() {
		return showVersionInfo;
	}

	/**
	 * Sets the show version info.
	 *
	 * @param showVersionInfo the showVersionInfo to set
	 */
	public void setShowVersionInfo(boolean showVersionInfo) {
		this.showVersionInfo = showVersionInfo;
	}

	/**
	 * Gets the captcha answer.
	 *
	 * @return the captcha answer
	 */
	public String getCaptchaAnswer() {
		return captchaAnswer;
	}

	/**
	 * Sets the captcha answer.
	 *
	 * @param captchaAnswer the new captcha answer
	 */
	public void setCaptchaAnswer(String captchaAnswer) {
		this.captchaAnswer = captchaAnswer;
	}

	/**
	 * Gets the enabled captcha.
	 *
	 * @return the enabled captcha
	 */
	public Integer getEnabledCaptcha() {
		return enabledCaptcha;
	}

	/**
	 * Sets the enabled captcha.
	 *
	 * @param enabledCaptcha the new enabled captcha
	 */
	public void setEnabledCaptcha(Integer enabledCaptcha) {
		this.enabledCaptcha = enabledCaptcha;
	}

	/**
	 * Gets the captcha url.
	 *
	 * @return the captcha url
	 */
	public String getCaptchaUrl() {
		String reload = Faces.getSessionAttribute("RELOAD");
		return captchaUrl + StringUtils.defaultString(reload);
	}
	
	/**
	 * Reload captcha.
	 */
	public void reloadCaptcha(){
		Faces.setSessionAttribute("RELOAD", "?reload=true");
	}

	/**
	 * Reset captcha url.
	 */
	public void resetCaptchaUrl(){
		Faces.setSessionAttribute("RELOAD", "");
	} 
	
	/**
	 * Gets the idle time pararm.
	 * 
	 * @return the idleTimePararm
	 */
	public String getIdleTimePararm() {
		return idleTimePararm;
	}

	/**
	 * Sets the idle time pararm.
	 * 
	 * @param idleTimePararm
	 *            the idleTimePararm to set
	 */
	public void setIdleTimePararm(String idleTimePararm) {
		this.idleTimePararm = idleTimePararm;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		if (email != null) {
			email = email.toUpperCase();
		}
		this.email = email;
	}

	/**
	 * Checks if is show forgot password.
	 *
	 * @return true, if is show forgot password
	 */
	public boolean isShowForgotPassword() {
		return showForgotPassword;
	}

	/**
	 * Sets the show forgot password.
	 *
	 * @param showForgotPassword the new show forgot password
	 */
	public void setShowForgotPassword(boolean showForgotPassword) {
		this.showForgotPassword = showForgotPassword;
	}

	/**
	 * @return the enabledTotp
	 */
	public Integer getEnabledTotp() {
		return enabledTotp;
	}

	/**
	 * @param enabledTotp the enabledTotp to set
	 */
	public void setEnabledTotp(Integer enabledTotp) {
		this.enabledTotp = enabledTotp;
	}

	/**
	 * @return the totpUrl
	 */
	public String getTotpUrl() {
		return totpUrl;
	}

	/**
	 * @param totpUrl the totpUrl to set
	 */
	public void setTotpUrl(String totpUrl) {
		this.totpUrl = totpUrl;
	}

	/**
	 * @return the totp
	 */
	public String getTotp() {
		return totp;
	}

	/**
	 * @param totp the totp to set
	 */
	public void setTotp(String totp) {
		this.totp = totp;
	}

	/**
	 * @return the renderTotpQR
	 */
	public boolean isRenderTotpQR() {
		return renderTotpQR;
	}

	/**
	 * @param renderTotpQR the renderTotpQR to set
	 */
	public void setRenderTotpQR(boolean renderTotpQR) {
		this.renderTotpQR = renderTotpQR;
	}

	public Boolean getShowMfa() {
		return showMfa;
	}

	public void setShowMfa(Boolean showMfa) {
		this.showMfa = showMfa;
	}

	public Boolean getIsUatOrProd() {
		return isUatOrProd;
	}

	public void setIsUatOrProd(Boolean isUatOrProd) {
		this.isUatOrProd = isUatOrProd;
	}

	public Boolean getCaptchaIsValid() {
		return captchaIsValid;
	}

	public void setCaptchaIsValid(Boolean captchaIsValid) {
		this.captchaIsValid = captchaIsValid;
	}

}