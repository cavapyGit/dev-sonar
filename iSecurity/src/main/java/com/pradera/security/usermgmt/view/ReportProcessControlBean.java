package com.pradera.security.usermgmt.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Instance;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.processes.remote.types.ProcessLoggerStateType;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.services.remote.reports.ReportGenerationService;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.security.model.InstitutionsSecurity;
import com.pradera.security.model.Parameter;
import com.pradera.security.model.type.ParameterTableType;
import com.pradera.security.model.type.SystemProfileStateType;
import com.pradera.security.model.type.UserAccountType;
import com.pradera.security.systemmgmt.view.filters.InstitutionSecurityFilter;
import com.pradera.security.systemmgmt.view.filters.SystemProfileFilter;
import com.pradera.security.usermgmt.view.filters.ProcessControlReportFilter;
import com.pradera.security.usermgmt.view.filters.UserAccountFilter;
import com.pradera.security.utils.view.GenericBaseBean;
import com.pradera.security.utils.view.PropertiesConstants;

/**
 * @author PraderaSoftWare
 *
 */
@DepositaryWebBean
public class ReportProcessControlBean extends GenericBaseBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The user account filter. */
	private UserAccountFilter userAccountFilter;
	
	/** The process contol report filter. */
	private ProcessControlReportFilter processControlReportFilter;

	/** The institution type. */
	private Integer institutionType;
	
	/** The lst institution type for filter. */
	private List<Parameter> lstInstitutionTypeForFilter;
    
	/** The id institution security. */
	private Integer idInstitutionSecurity;
	
	/** The lst institutions security for filter. */
	private List<InstitutionsSecurity> lstInstitutionsSecurityForFilter;
	
	private List<ProcessLoggerStateType> processLoggerState; 
	
	

	/** The lst institution type for filter. */
	private List<Parameter> lstInstitutionTypeForFilterTemp;
    
	@Inject
	Instance<ReportGenerationService> reportService;
	
	/**
	 * Init.
	 */
	@PostConstruct
	public void init(){
		try {
	    	if (!FacesContext.getCurrentInstance().isPostback()) {
	    		
				 beginConversation();
				 SystemProfileFilter ps=new SystemProfileFilter();
				 ps.setSystemOptionState(SystemProfileStateType.REGISTERED.getCode());
				 lstInstitutions =userServiceFacade.searchAllInstitutionsServiceFacade();
				 listUserType();
				 processControlReportFilter = new ProcessControlReportFilter();
				 processControlReportFilter.setState(Integer.valueOf(-1));
				 processControlReportFilter.setInstitutionType(Integer.valueOf(-1));
				 processControlReportFilter.setInstitution(Integer.valueOf(-1));
				 processControlReportFilter.setUserType(Integer.valueOf(-1));
				 processControlReportFilter.setInitialDate(CommonsUtilities.currentDate());
				 processControlReportFilter.setFinalDate(CommonsUtilities.currentDate());
				 lstInstitutionTypeForFilter = new ArrayList<Parameter>();
				 lstInstitutionTypeForFilterTemp = userServiceFacade.listClientTypeByDefinitionServiceFacade(ParameterTableType.INSITUTIONS_STATE.getCode());
				 processLoggerState = ProcessLoggerStateType.list; // userServiceFacade.listClientTypeByDefinitionServiceFacade(ParameterTableType.USER_STATES.getCode());
			}
		} catch (Exception e) {
			//excepcion.fire(new ExceptionToCatch(e));
		}
	}
	
	/**
	 * Change user type for filter listener.
	 */
    public void changeUserTypeForFilterListener(){
		
	try {
			if(processControlReportFilter.getUserType().intValue()>0){
				if(processControlReportFilter.getUserType().intValue()==UserAccountType.INTERNAL.getCode().intValue()){
					lstInstitutionTypeForFilter=new ArrayList<Parameter>();
					lstInstitutionTypeForFilter.addAll(lstInstitutionTypeForFilterTemp);
					for(Parameter param:lstInstitutionTypeForFilterTemp){
						if(!InstitutionType.DEPOSITARY.getCode().equals(param.getIdParameterPk())){
							lstInstitutionTypeForFilter.remove(param);
						}
					}
					
					processControlReportFilter.setInstitutionType(Integer.parseInt(InstitutionType.DEPOSITARY.getCode().toString()));
					
					changeInstitutionTypeForFilterListener();
					HtmlSelectOneMenu combo=(HtmlSelectOneMenu)JSFUtilities.findViewComponent("reportprocessform:cboInstitution");
					UISelectItems items=(UISelectItems)combo.getChildren().get(1);
					List<InstitutionsSecurity> lstInstituions=(List<InstitutionsSecurity>)items.getValue();
					processControlReportFilter.setInstitution(Integer.valueOf(lstInstituions.get(0).getIdInstitutionSecurityPk()));
				}else{
					lstInstitutionTypeForFilter=new ArrayList<Parameter>();
					lstInstitutionTypeForFilter.addAll(lstInstitutionTypeForFilterTemp);
					for(Parameter param:lstInstitutionTypeForFilterTemp){
						if(InstitutionType.DEPOSITARY.getCode().equals(param.getIdParameterPk())){
							lstInstitutionTypeForFilter.remove(param);
						}
					}
					
					lstInstitutionsSecurityForFilter=new ArrayList<InstitutionsSecurity>();
					processControlReportFilter.setInstitutionType(Integer.valueOf(-1));
					processControlReportFilter.setInstitution(Integer.valueOf(-1));
				}
			} else{
				lstInstitutionTypeForFilter=new ArrayList<Parameter>();
				lstInstitutionsSecurityForFilter=new ArrayList<InstitutionsSecurity>();
				processControlReportFilter.setInstitutionType(Integer.valueOf(-1));
				processControlReportFilter.setInstitution(Integer.valueOf(-1));
			}
	    } catch (Exception e) {
		    //excepcion.fire( new ExceptionToCatch(e) );
		}
	}
	
	/**
	 * Change institution type for filter listener.
	 */
	public void changeInstitutionTypeForFilterListener(){
		try {
			if(processControlReportFilter.getInstitutionType()>0){
				InstitutionSecurityFilter insSecurityFilter=new InstitutionSecurityFilter();
				insSecurityFilter.setIdInstitutionTypeFk(processControlReportFilter.getInstitutionType() );
				lstInstitutionsSecurityForFilter=userServiceFacade.getInstitutionsByFilterServiceFacade(insSecurityFilter);
			}else{
				lstInstitutionsSecurityForFilter=new ArrayList<InstitutionsSecurity>();
				processControlReportFilter.setInstitution(Integer.valueOf(-1));
			}
		} catch (Exception e) {
			//excepcion.fire(new ExceptionToCatch(e));
		}
	}
	
	
	/**
	 * To send CorporativeProcessReport
	 */
	@LoggerAuditWeb
	public void sendCorporativeProcessReports(){
		try{
			LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());
			
			Map<String,String> reportParams = getReportParameters();
			Long reportId = ReportIdType.REPOERTING_AND_PROCESS_CONTROL.getCode();
			ReportUser reportUser = new ReportUser();
			reportUser.setUserName(loggerUser.getUserName());
			if(reportId != null){
				reportService.get().saveReportExecution(reportId, reportParams, null, reportUser, loggerUser);
			}
			JSFUtilities.showComponent(":formPopUp:idDialReportConfirm");
			JSFUtilities.showMessageOnDialog(null, null, PropertiesConstants.REPORT_SEND_SUCCESS_MSG, null);
			return;
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public Map<String,String> getReportParameters(){
		Map<String,String> parameters = new HashMap<String, String>();
		
		parameters.put("user_type", String.valueOf(processControlReportFilter.getUserType()));
		parameters.put("initial_date", CommonsUtilities.convertDatetoString(processControlReportFilter.getInitialDate()));
		parameters.put("final_date", CommonsUtilities.convertDatetoString(processControlReportFilter.getFinalDate()));
		if(Validations.validateIsNotNullAndPositive(processControlReportFilter.getInstitutionType())){
			parameters.put("institution_type", String.valueOf(processControlReportFilter.getInstitutionType()));
		}else{
			parameters.put("institution_type", "");
		}
		if(Validations.validateIsNotNullAndPositive(processControlReportFilter.getInstitution())){
			parameters.put("institution", String.valueOf(processControlReportFilter.getInstitution()));
		}else{
			parameters.put("institution", "");
		}
		if(processControlReportFilter.getUserCode() != null){
			parameters.put("user_code", processControlReportFilter.getUserCode());
		}else{
			parameters.put("user_code", "");
		}
		if(Validations.validateIsNotNullAndPositive(processControlReportFilter.getState())){
			parameters.put("state", String.valueOf(processControlReportFilter.getState()));
		}else{
			parameters.put("state", "");
		}
		
		return parameters;
	}
	/**
	 * Clean search users listener.
	 *
	 * @param event the event
	 */
	public void clean(){
		try {
			processControlReportFilter = new ProcessControlReportFilter();
			processControlReportFilter.setInitialDate(CommonsUtilities.currentDate());
			processControlReportFilter.setFinalDate(CommonsUtilities.currentDate());
		} catch (Exception e) {
			// excepcion.fire(new ExceptionToCatch(e));
		}
	}
	
	/**
	 * @return the processControlReportFilter
	 */
	public ProcessControlReportFilter getProcessControlReportFilter() {
		return processControlReportFilter;
	}

	/**
	 * @param processControlReportFilter the processControlReportFilter to set
	 */
	public void setProcessControlReportFilter(
			ProcessControlReportFilter processControlReportFilter) {
		this.processControlReportFilter = processControlReportFilter;
	}
	/**
	 * @return the userAccountFilter
	 */
	public UserAccountFilter getUserAccountFilter() {
		return userAccountFilter;
	}

	/**
	 * @param userAccountFilter the userAccountFilter to set
	 */
	public void setUserAccountFilter(UserAccountFilter userAccountFilter) {
		this.userAccountFilter = userAccountFilter;
	}

	/**
	 * @return the institutionType
	 */
	public Integer getInstitutionType() {
		return institutionType;
	}

	/**
	 * @param institutionType the institutionType to set
	 */
	public void setInstitutionType(Integer institutionType) {
		this.institutionType = institutionType;
	}

	/**
	 * @return the lstInstitutionTypeForFilter
	 */
	public List<Parameter> getLstInstitutionTypeForFilter() {
		return lstInstitutionTypeForFilter;
	}

	/**
	 * @param lstInstitutionTypeForFilter the lstInstitutionTypeForFilter to set
	 */
	public void setLstInstitutionTypeForFilter(
			List<Parameter> lstInstitutionTypeForFilter) {
		this.lstInstitutionTypeForFilter = lstInstitutionTypeForFilter;
	}

	/**
	 * @return the idInstitutionSecurity
	 */
	public Integer getIdInstitutionSecurity() {
		return idInstitutionSecurity;
	}

	/**
	 * @param idInstitutionSecurity the idInstitutionSecurity to set
	 */
	public void setIdInstitutionSecurity(Integer idInstitutionSecurity) {
		this.idInstitutionSecurity = idInstitutionSecurity;
	}

	/**
	 * @return the lstInstitutionsSecurityForFilter
	 */
	public List<InstitutionsSecurity> getLstInstitutionsSecurityForFilter() {
		return lstInstitutionsSecurityForFilter;
	}

	/**
	 * @param lstInstitutionsSecurityForFilter the lstInstitutionsSecurityForFilter to set
	 */
	public void setLstInstitutionsSecurityForFilter(
			List<InstitutionsSecurity> lstInstitutionsSecurityForFilter) {
		this.lstInstitutionsSecurityForFilter = lstInstitutionsSecurityForFilter;
	}

	/**
	 * @return the processLoggerState
	 */
	public List<ProcessLoggerStateType> getProcessLoggerState() {
		return processLoggerState;
	}

	/**
	 * @param processLoggerState the processLoggerState to set
	 */
	public void setProcessLoggerState(
			List<ProcessLoggerStateType> processLoggerState) {
		this.processLoggerState = processLoggerState;
	}

	/**
	 * @return the lstInstitutionTypeForFilterTemp
	 */
	public List<Parameter> getLstInstitutionTypeForFilterTemp() {
		return lstInstitutionTypeForFilterTemp;
	}

	/**
	 * @param lstInstitutionTypeForFilterTemp the lstInstitutionTypeForFilterTemp to set
	 */
	public void setLstInstitutionTypeForFilterTemp(
			List<Parameter> lstInstitutionTypeForFilterTemp) {
		this.lstInstitutionTypeForFilterTemp = lstInstitutionTypeForFilterTemp;
	}
}
