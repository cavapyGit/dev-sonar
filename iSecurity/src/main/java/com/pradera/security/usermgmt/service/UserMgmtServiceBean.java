package com.pradera.security.usermgmt.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.model.InstitutionTypeDetail;
import com.pradera.security.model.UserAccount;
import com.pradera.security.model.UserProfile;
import com.pradera.security.systemmgmt.service.CrudSecurityServiceBean;
import com.pradera.security.usermgmt.view.filters.UserAccountFilter;
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class UserMgmtServiceBean
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.1
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class UserMgmtServiceBean extends CrudSecurityServiceBean{
	
	public int validateLoginUserServiceBean(UserAccount user) throws ServiceException{
		String strQuery="Select Count(u) from Usuario u where u.usuarioLogin=:username";
		Query query=em.createQuery(strQuery);
		query.setParameter("username", user.getLoginUser());
		query.setHint("org.hibernate.cacheable", true);
		return Integer.parseInt(query.getSingleResult().toString());
	}
	
	public List<UserAccount> serachUserByFilterServiceBean(UserAccountFilter userFilter) throws ServiceException{
		CriteriaBuilder cb=em.getCriteriaBuilder();
		CriteriaQuery<UserAccount> cq=cb.createQuery(UserAccount.class);
		Root<UserAccount> usuarioRoot=cq.from(UserAccount.class);
		usuarioRoot.fetch("institucionSeguridad");		
		
		List<Predicate> criteriaParametros=new ArrayList<Predicate>();
		
		cq.select(usuarioRoot);

		/**
		 * @author mmacalupu
		 * This filter is used when we want to show the users and his schedule's exception
		 */
		if(Validations.validateIsNotNull(userFilter.getScheduleExceptionFilter())){
			usuarioRoot.fetch("excepcionHorarioDetalles",JoinType.LEFT);
			cq.distinct(true);			
		}
		/**End
		 */
		
		if(Validations.validateIsNotNullAndNotEmpty(userFilter.getLoginUserAccount())){
			ParameterExpression<String> p =cb.parameter(String.class,"usuarioLoginPrm");
			criteriaParametros.add(cb.like(usuarioRoot.<String>get("usuarioLogin"),p));
		}
		if(Validations.validateIsNotNullAndNotEmpty( userFilter.getUserAccountType())
				&& userFilter.getUserAccountType().intValue()>0){
			ParameterExpression<Integer> p =cb.parameter(Integer.class,"tipoUsuarioPrm");
			criteriaParametros.add(cb.equal(usuarioRoot.get("tipo"),p));
		}
		if(Validations.validateIsNotNullAndNotEmpty( userFilter.getIdInstitutionSecurity())
				&& userFilter.getIdInstitutionSecurity().intValue()>0){
			ParameterExpression<Integer> p =cb.parameter(Integer.class,"instSeguridadPrm");
			criteriaParametros.add(cb.equal(usuarioRoot.get("institucionSeguridad").get("IdInstitucionSeguridadPk"),p));
		}
		if(Validations.validateIsNotNullAndNotEmpty( userFilter.getUserAccountState() )
				&& userFilter.getUserAccountState().intValue()>0){
			ParameterExpression<Integer> p =cb.parameter(Integer.class,"estadoUsuarioPrm");
			criteriaParametros.add(cb.equal(usuarioRoot.get("estado"),p));
		}
		
		if(!criteriaParametros.isEmpty()){
			if (criteriaParametros.size() == 1) {
		        cq.where(criteriaParametros.get(0));
		    } else {
		        cq.where(cb.and(criteriaParametros.toArray(new Predicate[criteriaParametros.size()])));
		    }
		}
		
		TypedQuery<UserAccount> usuarioCriteria= em.createQuery(cq);
		
		if(Validations.validateIsNotNullAndNotEmpty(userFilter.getLoginUserAccount())){
			usuarioCriteria.setParameter("usuarioLoginPrm", userFilter.getLoginUserAccount());
		}
		if(Validations.validateIsNotNullAndNotEmpty( userFilter.getUserAccountType() )
				&& userFilter.getUserAccountType().intValue()>0){
			usuarioCriteria.setParameter("tipoUsuarioPrm", userFilter.getUserAccountType());
		}
		if(Validations.validateIsNotNullAndNotEmpty( userFilter.getIdInstitutionSecurity() )
				&& userFilter.getIdInstitutionSecurity().intValue()>0){
			usuarioCriteria.setParameter("instSeguridadPrm", userFilter.getIdInstitutionSecurity());
		}
		if(Validations.validateIsNotNullAndNotEmpty( userFilter.getUserAccountState())
				&& userFilter.getUserAccountState().intValue()>0){
			usuarioCriteria.setParameter("estadoUsuarioPrm", userFilter.getUserAccountState());
		}
		
		return (List<UserAccount>) usuarioCriteria.getResultList();
	}
	
	public void updateUserStateServiceBean(UserAccount user) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update Usuario U ");	
		sbQuery.append(" Set U.estado = :estadoParam,");
		sbQuery.append(" U.cambiarClave=:cambiarClavePrm,");
		sbQuery.append(" U.fechaModificacion = :fechaModificacionParam,");
		sbQuery.append(" U.usuarioModificacion = :usuarioModificacion,");
		sbQuery.append(" U.ipModificacion = :ipModificacionParam,");
		sbQuery.append(" U.privilegioOpcionModificacion = :privilegioOpcionModificacionParam ");
		sbQuery.append(" Where U.idUsuarioPk = :idUsuarioPrm ");
		
		Query query = em.createQuery(sbQuery.toString());
					
		query.setParameter("idUsuarioPrm",user.getIdUserPk());
		query.setParameter("estadoParam",user.getState());
		query.setParameter("cambiarClavePrm", user.getChangePassword());
		query.setParameter("fechaModificacionParam", user.getLastModifyDate());
		query.setParameter("usuarioModificacion", user.getLastModifyUser());
		query.setParameter("ipModificacionParam", user.getLastModifyIp());
		query.setParameter("privilegioOpcionModificacionParam", user.getLastModifyPriv());

		
		query.executeUpdate();
	}
	
	public List<InstitutionTypeDetail> searchInstitutionsTypeByUserServiceBean(int idUserPk, int state) throws ServiceException{
		Map<String,Object> parametros=new HashMap<String, Object>();
		parametros.put("idUsuarioPrm", idUserPk);
		parametros.put("estadoPrm", state);
		return (List<InstitutionTypeDetail >)findWithNamedQuery(InstitutionTypeDetail.SEARCH_BY_USER, parametros);
	}
	
	public List<UserProfile> searchProfilesRegiteredByUserServiceBean(UserProfile userProfile) throws ServiceException{
		String strQuery="Select PU from PerfilUsuario PU where PU.usuario.idUsuarioPk=:idUsuarioPrm and PU.estado=:estadoPrm";
		Query query=em.createQuery(strQuery);
		query.setParameter("idUsuarioPrm", userProfile.getUserAccount().getIdUserPk());
		query.setParameter("estadoPrm", userProfile.getState());
		query.setHint("org.hibernate.cacheable", true);
		return (List<UserProfile>) query.getResultList();
	}
	
	public UserAccount validateUserServiceBean(UserAccountFilter userAccountFilter) throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("Select U From UserAccount U ");
			sbQuery.append(" Where U.state = :userAccountStatePrm And U.loginUser = :loginUserPrm");
			
			Query query=em.createQuery(sbQuery.toString());
			query.setParameter("userAccountStatePrm", userAccountFilter.getUserAccountState());
			query.setParameter("loginUserPrm", userAccountFilter.getLoginUserAccount());
			query.setHint("org.hibernate.cacheable", true);
			return (UserAccount) query.getSingleResult();
		}catch(NoResultException ex){
			return null;
		}catch(NonUniqueResultException ex){
			return null;
		}
	}
	
	public void updateChangePasswordServiceBean(UserAccount user) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update Usuario U ");	
		sbQuery.append(" Set U.cambiarClave = :cambiaClaveParam,");
		sbQuery.append(" U.fechaModificacion = :fechaModificacionParam,");
		sbQuery.append(" U.usuarioModificacion = :usuarioModificacion,");
		sbQuery.append(" U.ipModificacion = :ipModificacionParam,");
		sbQuery.append(" U.privilegioOpcionModificacion = :privilegioOpcionModificacionParam ");
		sbQuery.append(" Where U.idUsuarioPk = :idUsuarioPrm ");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idUsuarioPrm",user.getIdUserPk());
		query.setParameter("cambiaClaveParam",user.getChangePassword());
		query.setParameter("fechaModificacionParam", user.getLastModifyDate());
		query.setParameter("usuarioModificacion", user.getLastModifyUser());
		query.setParameter("ipModificacionParam", user.getLastModifyIp());
		query.setParameter("privilegioOpcionModificacionParam", user.getLastModifyPriv());
		query.executeUpdate();
	}
}
