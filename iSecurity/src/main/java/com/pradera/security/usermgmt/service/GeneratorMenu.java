package com.pradera.security.usermgmt.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;

import javax.enterprise.context.RequestScoped;

import com.pradera.commons.menu.PraderaMenu;
import com.pradera.commons.menu.PraderaMenuOption;
import com.pradera.commons.menu.PraderaSubMenu;
import com.pradera.commons.security.model.type.LanguageType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.security.model.CatalogueLanguage;
import com.pradera.security.model.SystemOption;
import com.pradera.security.model.type.OptionType;
import com.pradera.security.utils.view.SecurityUtilities;
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Class MenuGenerador.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/11/2012
 */
@RequestScoped
public class GeneratorMenu implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2992771392292952653L;

	/** The lista opciones menu. */
	private List<SystemOption> systemOptionsList;

	/** The pradera menu. */
	private PraderaMenu praderaMenu;
	
	private static final String MENU_SEPARATOR = "-";
	
	/**
	 * Instantiates a new menu generador.
	 */
	public GeneratorMenu() {
		praderaMenu = new PraderaMenu();
		praderaMenu.setSubMenus(new TreeSet<PraderaSubMenu>());
	}
	
	/**
	 * generateMenu.
	 * this method is the main to create all menu, is very important
	 * data's content will be consistent
	 *
	 * @param menuOptionsList the lista opciones menu
	 * @return the pradera menu
	 */
	public PraderaMenu generateMenu(List<SystemOption> menuOptionsList, String systemUrl) { 
		this.systemOptionsList = menuOptionsList;
		
		for(SystemOption systemOption: this.getMenusParents()){
			PraderaSubMenu subMenuPradera = new PraderaSubMenu();
			subMenuPradera.setOptions(new TreeSet<PraderaMenuOption>());
			subMenuPradera.setSubMenus(new TreeSet<PraderaSubMenu>());
			subMenuPradera.setName(new HashMap<String, String>());
			for(SystemOption systemOptionSubMenu: getSubMenusFromParent(systemOption)){				
				if(systemOptionSubMenu.getOptionType().equals(OptionType.OPTION.getCode())){
					 PraderaMenuOption menuOption = new PraderaMenuOption();
					 menuOption.setIdMenuOptionPk(systemOptionSubMenu.getIdSystemOptionPk());
					 menuOption.setIdModulePk(systemOptionSubMenu.getModule());
					 menuOption.setName(new HashMap<String, String>());
					 for(CatalogueLanguage language : systemOptionSubMenu.getCatalogueLanguages()) {
						 menuOption.getName().put(LanguageType.get(language.getLanguage()).getValue(), language.getCatalogueName());
					 }
					 if(!systemOptionSubMenu.getUrlOption().equals(MENU_SEPARATOR)){
						 String urlNew = (systemUrl+systemOptionSubMenu.getUrlOption()).replaceAll("//","/");
						 menuOption.setUrl(urlNew.replaceFirst(":/", "://"));
					 }else{
						 menuOption.setUrl(systemOptionSubMenu.getUrlOption());
					 }
					 menuOption.setUserAcctions(SecurityUtilities.convertUserAcctionsFromPrivileges(systemOptionSubMenu.getOptionPrivileges()));
					 menuOption.setOrderOption(systemOptionSubMenu.getOrderOption());
					 subMenuPradera.getOptions().add(menuOption);
				}else if(systemOptionSubMenu.getOptionType().equals(OptionType.MODULE.getCode())){
					PraderaSubMenu subMenuChildren = new PraderaSubMenu();
					subMenuChildren.setName(new HashMap<String, String>());
					subMenuChildren.setOptions(new TreeSet<PraderaMenuOption>());
					subMenuChildren.setSubMenus(new TreeSet<PraderaSubMenu>());
					for(CatalogueLanguage language : systemOptionSubMenu.getCatalogueLanguages()) {
						subMenuChildren.getName().put(LanguageType.get(language.getLanguage()).getValue(), language.getCatalogueName());
						subMenuChildren.setIdMenuOptionPk(systemOptionSubMenu.getIdSystemOptionPk());
					 }
					subMenuChildren.setOrderOption(systemOptionSubMenu.getOrderOption());
					subMenuPradera.getSubMenus().add(subMenuChildren);
					getMenuChildrenFromOption(subMenuChildren, systemOptionSubMenu, systemUrl);
				}
			}
			for(CatalogueLanguage language : systemOption.getCatalogueLanguages()) {
				subMenuPradera.getName().put(LanguageType.get(language.getLanguage()).getValue(), language.getCatalogueName());
				subMenuPradera.setIdMenuOptionPk(systemOption.getIdSystemOptionPk());
			}
			subMenuPradera.setOrderOption(systemOption.getOrderOption());
			praderaMenu.getSubMenus().add(subMenuPradera);
		}
		return praderaMenu;
	}
	
	/**
	 * get getMenuChildrenFromOption.
	 * this method is useful to get Children from a Option
	 * be careful because is a recursive method and the data's content
	 * is important.
	 * *
	 * @param praderaSubMenu the pradera sub menu
	 * @param systemOptionParam the opcion sistema
	 */
	public void getMenuChildrenFromOption(PraderaSubMenu praderaSubMenu, SystemOption systemOptionParam, String systemUrl) {
		for(SystemOption systemOption: getSubMenusFromParent(systemOptionParam)){
			if(systemOptionParam.getIdSystemOptionPk().equals(systemOption.getParentSystemOption().getIdSystemOptionPk())) {
				if(systemOption.getOptionType().equals(OptionType.MODULE.getCode())) {
					PraderaSubMenu subMenu = new PraderaSubMenu();
					subMenu.setName(new HashMap<String, String>());
					subMenu.setOptions(new TreeSet<PraderaMenuOption>());
					subMenu.setSubMenus(new TreeSet<PraderaSubMenu>());
					for(CatalogueLanguage language : systemOption.getCatalogueLanguages()) {
						subMenu.getName().put(LanguageType.get(language.getLanguage()).getValue(), language.getCatalogueName());
						subMenu.setIdMenuOptionPk(systemOption.getIdSystemOptionPk());
					}
					subMenu.setOrderOption(systemOption.getOrderOption());
					praderaSubMenu.getSubMenus().add(subMenu);
					//call children
					getMenuChildrenFromOption(subMenu, systemOption, systemUrl);
				} else {
					PraderaMenuOption menuOption = new PraderaMenuOption();
					menuOption.setName(new HashMap<String, String>());
					menuOption.setIdMenuOptionPk(systemOption.getIdSystemOptionPk());
					menuOption.setIdModulePk(systemOption.getModule());
					for(CatalogueLanguage language : systemOption.getCatalogueLanguages()) {
						 menuOption.getName().put(LanguageType.get(language.getLanguage()).getValue(), language.getCatalogueName());
					 }
					if(!systemOption.getUrlOption().equals(MENU_SEPARATOR)){
						 String urlNew = (systemUrl+systemOption.getUrlOption()).replaceAll("//","/");
						 menuOption.setUrl(urlNew.replaceFirst(":/", "://"));
					 }else{
						 menuOption.setUrl(systemOption.getUrlOption());
					 }
					 menuOption.setUserAcctions(SecurityUtilities.convertUserAcctionsFromPrivileges(systemOption.getOptionPrivileges()));
					 menuOption.setOrderOption(systemOption.getOrderOption());
					 praderaSubMenu.getOptions().add(menuOption);
				}
			}
		}
	}
	
	
	
	/**
	 * Obtener getMenusParents.
	 *	This method is the first in invocate
	 *  because here you get only the menu parents
	 * @return the list
	 */
	public List<SystemOption> getMenusParents(){
		List<SystemOption> menuList = new ArrayList<SystemOption>();
		for(SystemOption systemOption: this.systemOptionsList){
			if(Validations.validateIsNull(systemOption.getParentSystemOption())){
				menuList.add(systemOption);
			}
		}
		return menuList;
	}
	
	/**
	 * getSubMenusFromParent.
	 * This method is useful to get all submenu from Parent, in this case
	 * the param is a parent node with many child 
	 * @param systemOptionParam the opcion sistema param
	 * @return the list
	 */
	public List<SystemOption> getSubMenusFromParent(SystemOption systemOptionParam){
		List<SystemOption> menuParentsList = new ArrayList<SystemOption>();
		for(SystemOption systemOption: this.systemOptionsList){
			if(systemOption.getParentSystemOption()!=null && systemOptionParam.getIdSystemOptionPk().equals(systemOption.getParentSystemOption().getIdSystemOptionPk())){
				menuParentsList.add(systemOption);
			}
		}
		return menuParentsList;
	}
}