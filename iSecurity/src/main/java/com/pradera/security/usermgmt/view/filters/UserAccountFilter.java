package com.pradera.security.usermgmt.view.filters;

import java.io.Serializable;

import com.pradera.security.model.Subsidiary;
import com.pradera.security.systemmgmt.view.filters.ScheduleExceptionFilter;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Class UserAccountFilter.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 13/12/2012
 */
public class UserAccountFilter implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id user account pk. */
	private Integer idUserAccountPK;
	
	/** The login user account. */
	private String loginUserAccount;
	
	/** The id institution security. */
	private Integer idInstitutionSecurity;
	
	/** The institution type. */
	private Integer institutionType;
	
	/** The user account type. */
	private Integer userAccountType;
	
	/** The user account state. */
	private Integer userAccountState;
	
	/** The id system pk. */
	private Integer idSystemPk;
	
	/** The id system option pk. */
	private Integer idSystemOptionPk;
	
	/** The schedule exception filter. */
	private ScheduleExceptionFilter scheduleExceptionFilter;	
	
	/** The id language. */
	private Integer idLanguage;
	
	/** The id Subsidiary Pk. */
	private Subsidiary subsidiary;
	
	/**
	 * The userName
	 */
	private String userName;
	
	/**
	 * The firstLastName
	 */
	private String firtsLastName;
	
	/**
	 * The secondLastName
	 */
	private String secondLastName;

	/**
	 * Gets the id institution security.
	 *
	 * @return the id institution security
	 */
	public Integer getIdInstitutionSecurity() {
		return idInstitutionSecurity;
	}
	
	/**
	 * Sets the id institution security.
	 *
	 * @param idInstitutionSecurity the new id institution security
	 */
	public void setIdInstitutionSecurity(Integer idInstitutionSecurity) {
		this.idInstitutionSecurity = idInstitutionSecurity;
	}
	
	/**
	 * Sets the user account type.
	 *
	 * @param userAccountType the new user account type
	 */
	public void setUserAccountType(Integer userAccountType) {
		this.userAccountType = userAccountType;
	}
	
	/**
	 * Gets the id user account pk.
	 *
	 * @return the id user account pk
	 */
	public Integer getIdUserAccountPK() {
		return idUserAccountPK;
	}
	
	/**
	 * Sets the id user account pk.
	 *
	 * @param idUserAccountPK the new id user account pk
	 */
	public void setIdUserAccountPK(Integer idUserAccountPK) {
		this.idUserAccountPK = idUserAccountPK;
	}
	
	/**
	 * Gets the login user account.
	 *
	 * @return the login user account
	 */
	public String getLoginUserAccount() {
		return loginUserAccount;
	}
	
	/**
	 * Sets the login user account.
	 *
	 * @param loginUserAccount the new login user account
	 */
	public void setLoginUserAccount(String loginUserAccount) {
		if(loginUserAccount!=null){
			loginUserAccount = loginUserAccount.toUpperCase();
		}
		this.loginUserAccount = loginUserAccount;
	}
	
	/**
	 * Gets the user account type.
	 *
	 * @return the user account type
	 */
	public Integer getUserAccountType() {
		return userAccountType;
	}
	
	/**
	 * Sets the id user account type.
	 *
	 * @param userAccountType the new id user account type
	 */
	public void setIdUserAccountType(Integer userAccountType) {
		this.userAccountType = userAccountType;
	}
	
	/**
	 * Gets the user account state.
	 *
	 * @return the user account state
	 */
	public Integer getUserAccountState() {
		return userAccountState;
	}
	
	/**
	 * Sets the user account state.
	 *
	 * @param userAccountState the new user account state
	 */
	public void setUserAccountState(Integer userAccountState) {
		this.userAccountState = userAccountState;
	}
	
	/**
	 * Gets the schedule exception filter.
	 *
	 * @return the schedule exception filter
	 */
	public ScheduleExceptionFilter getScheduleExceptionFilter() {
		return scheduleExceptionFilter;
	}
	
	/**
	 * Sets the schedule exception filter.
	 *
	 * @param scheduleExceptionFilter the new schedule exception filter
	 */
	public void setScheduleExceptionFilter(
			ScheduleExceptionFilter scheduleExceptionFilter) {
		this.scheduleExceptionFilter = scheduleExceptionFilter;
	}
	
	/**
	 * Gets the id system pk.
	 *
	 * @return the id system pk
	 */
	public Integer getIdSystemPk() {
		return idSystemPk;
	}
	
	/**
	 * Sets the id system pk.
	 *
	 * @param idSystemPk the new id system pk
	 */
	public void setIdSystemPk(Integer idSystemPk) {
		this.idSystemPk = idSystemPk;
	}
	
	/**
	 * Gets the id system option pk.
	 *
	 * @return the id system option pk
	 */
	public Integer getIdSystemOptionPk() {
		return idSystemOptionPk;
	}
	
	/**
	 * Sets the id system option pk.
	 *
	 * @param idSystemOptionPk the new id system option pk
	 */
	public void setIdSystemOptionPk(Integer idSystemOptionPk) {
		this.idSystemOptionPk = idSystemOptionPk;
	}

	/**
	 * Gets the institution type.
	 *
	 * @return the institution type
	 */
	public Integer getInstitutionType() {
		return institutionType;
	}

	/**
	 * Sets the institution type.
	 *
	 * @param institutionType the new institution type
	 */
	public void setInstitutionType(Integer institutionType) {
		this.institutionType = institutionType;
	}

	/**
	 * Gets the id language.
	 *
	 * @return the id language
	 */
	public Integer getIdLanguage() {
		return idLanguage;
	}

	/**
	 * Sets the id language.
	 *
	 * @param idLanguage the new id language
	 */
	public void setIdLanguage(Integer idLanguage) {
		this.idLanguage = idLanguage;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the firtsLastName
	 */
	public String getFirtsLastName() {
		return firtsLastName;
	}

	/**
	 * @param firtsLastName the firtsLastName to set
	 */
	public void setFirtsLastName(String firtsLastName) {
		this.firtsLastName = firtsLastName;
	}

	/**
	 * @return the secondLastName
	 */
	public String getSecondLastName() {
		return secondLastName;
	}

	/**
	 * @param secondLastName the secondLastName to set
	 */
	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}

	/**
	 * @return the subsidiary
	 */
	public Subsidiary getSubsidiary() {
		return subsidiary;
	}

	/**
	 * @param subsidiary the subsidiary to set
	 */
	public void setSubsidiary(Subsidiary subsidiary) {
		this.subsidiary = subsidiary;
	}
	
}