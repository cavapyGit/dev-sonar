package com.pradera.security.usermgmt.service;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.text.MessageFormat;
import java.util.Properties;

import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.apache.commons.io.IOUtils;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.model.UserAccount;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class AlertsServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 25/02/2013
 */
@Startup
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class AlertsServiceBean {

	/** The log. */
	@Inject
	PraderaLogger log;
	
	//private static final String SERVER_MAIL = "smtp1r.cp.quickhost.com";

	//private static final String FROM_MAIL = "pradera@praderatechnologies.com";

	//private static final String USER_MAIL = "rcaso@praderatechnologies.com";

	//private static final String PASSWORD_MAIL = "*pifoc81.o";
	/** Properties class object to load the properties file. */
	Properties tmpProp=new Properties();
	
	/**
	 * Default constructor.
	 */
	public AlertsServiceBean() {
		try{
			tmpProp.load(AlertsServiceBean.class.getResourceAsStream("/ParametersEmail.properties"));
	    } catch (IOException e) {
		    e.printStackTrace();
	    }
	}
    
    /**
     * This method is for send the registration information through a mail.
     *
     * @param userAccount the user account
     * @param password the password
     * @throws ServiceException the service exception
     * @throws EmailException the email exception
     * @throws MalformedURLException the malformed url exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
	public void sendRegistrationMail(UserAccount userAccount,String password) throws ServiceException, EmailException, MalformedURLException,IOException{
		FileOutputStream foStream=null;
		InputStream inputFile =null;
		try{
		String sndContent=PropertiesUtilities.getMessage("email.user.content.registration");
		
		HtmlEmail email = new HtmlEmail();
		email.setHostName(tmpProp.getProperty("alert.server.mail"));
		email.addTo(userAccount.getIdEmail(), userAccount.getLoginUser());
		email.addBcc(tmpProp.getProperty("alert.bcc.email"), tmpProp.getProperty("alert.bcc.name"));
	
		email.setFrom(tmpProp.getProperty("alert.from.mail"), tmpProp.getProperty("alert.pradera"));
		email.setSubject(tmpProp.getProperty("alert.subject"));
		email.setAuthentication(tmpProp.getProperty("alert.user.mail"), tmpProp.getProperty("alert.password.mail"));
		email.setSmtpPort(Integer.parseInt(tmpProp.getProperty("alert.smtpport")));
		//Load image from resources
		inputFile =  Thread.currentThread().getContextClassLoader().getResourceAsStream(tmpProp.getProperty("alert.url"));
        File tmpLogo =File.createTempFile("logo",".jpg");
        foStream = new FileOutputStream(tmpLogo);
        foStream.write(IOUtils.toByteArray(inputFile));
  	  	String cid = email.embed(tmpLogo,tmpProp.getProperty("alert.email.embed"));
		//
		// set the html message
		StringBuilder stbMensaje = new StringBuilder();
		stbMensaje.append("<html><img src=\"cid:" + cid + "\"></br>");
		stbMensaje.append(MessageFormat.format(sndContent, userAccount.getLoginUser(),password));
		stbMensaje.append("</br></html>");
		email.setHtmlMsg(stbMensaje.toString());
		// set the alternative message
		email.setTextMsg(MessageFormat.format(sndContent, userAccount.getLoginUser(),password));
		email.send();
		}
		catch (EmailException emailException) {
			log.info("Unable to send an email to the this User::::"+userAccount.getIdEmail());
		}finally{
			//Close file temp
			if(inputFile != null)
				inputFile.close();
			if(foStream != null)
	  	  		foStream.close();
		}
			
		
	}
	
	/**
	 * This method is for reset the password and send the registration information through a mail.
	 *
	 * @param userAccount the user account
	 * @param password the password
	 * @throws ServiceException the service exception
	 * @throws EmailException the email exception
	 * @throws MalformedURLException the malformed url exception
	 */
	public void sendResetPassword(UserAccount userAccount,String password) throws ServiceException, EmailException, MalformedURLException,IOException {
		
		String sndContent=PropertiesUtilities.getMessage("email.user.content.reset.password");
		
		HtmlEmail email = new HtmlEmail();
		email.setHostName(tmpProp.getProperty("alert.server.mail"));
		email.addTo(userAccount.getIdEmail(), userAccount.getLoginUser());
		email.addBcc(tmpProp.getProperty("alert.bcc.email"), tmpProp.getProperty("alert.bcc.name"));
	
		email.setFrom(tmpProp.getProperty("alert.from.mail"), tmpProp.getProperty("alert.pradera"));
		email.setSubject(tmpProp.getProperty("alert.subject"));
		email.setAuthentication(tmpProp.getProperty("alert.user.mail"), tmpProp.getProperty("alert.password.mail"));
		email.setSmtpPort(Integer.parseInt(tmpProp.getProperty("alert.smtpport")));

		//Load image from resources
		InputStream inputFile =  Thread.currentThread().getContextClassLoader().getResourceAsStream(tmpProp.getProperty("alert.url"));
        File tmpLogo =File.createTempFile("logo",".jpg");
        FileOutputStream foStream = new FileOutputStream(tmpLogo);
        foStream.write(IOUtils.toByteArray(inputFile));
  	  	String cid = email.embed(tmpLogo,tmpProp.getProperty("alert.email.embed"));
  	    
		// set the html message
		StringBuilder stbMensaje = new StringBuilder();
		stbMensaje.append("<html><img src=\"cid:" + cid + "\"></br>");
		stbMensaje.append(MessageFormat.format(sndContent,password));
		stbMensaje.append("</br></html>");
		email.setHtmlMsg(stbMensaje.toString());

		// set the alternative message
		email.setTextMsg(MessageFormat.format(sndContent, userAccount.getLoginUser(),password));
		email.send();
	}
}