package com.pradera.security.usermgmt.facade;

import static dev.samstevens.totp.util.Utils.getDataUriForImage;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.httpevent.NotificationEvent;
import com.pradera.commons.httpevent.publish.BrowserWindow;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.menu.PraderaMenuOption;
import com.pradera.commons.menu.PraderaSubMenu;
import com.pradera.commons.security.model.type.ChangePwdMessageType;
import com.pradera.commons.security.model.type.LanguageType;
import com.pradera.commons.security.model.type.UserAccountStateType;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.type.ModuleWarType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.audit.service.HistoricalStateInterceptor;
import com.pradera.security.model.CatalogueLanguage;
import com.pradera.security.model.Holiday;
import com.pradera.security.model.InstitutionTypeDetail;
import com.pradera.security.model.InstitutionsSecurity;
import com.pradera.security.model.OptionPrivilege;
import com.pradera.security.model.Parameter;
import com.pradera.security.model.ScheduleExceptionDetail;
import com.pradera.security.model.Subsidiary;
import com.pradera.security.model.SystemOption;
import com.pradera.security.model.UserAccount;
import com.pradera.security.model.UserManualProfile;
import com.pradera.security.model.UserProfile;
import com.pradera.security.model.UserProfileAllowed;
import com.pradera.security.model.UserSession;
import com.pradera.security.model.type.OptionStateType;
import com.pradera.security.model.type.ScheduleExceptionDetailStateType;
import com.pradera.security.model.type.UserAccountSituationType;
import com.pradera.security.model.type.UserAccountType;
import com.pradera.security.notification.facade.NotificationServiceFacade;
import com.pradera.security.systemmgmt.service.SystemOptionServiceBean;
import com.pradera.security.systemmgmt.service.UserExceptionServiceBean;
import com.pradera.security.systemmgmt.view.filters.InstitutionSecurityFilter;
import com.pradera.security.systemmgmt.view.filters.SystemProfileFilter;
import com.pradera.security.usermgmt.service.AlertsServiceBean;
import com.pradera.security.usermgmt.service.GeneratorMenu;
import com.pradera.security.usermgmt.service.UserAccountServiceBean;
import com.pradera.security.usermgmt.service.UserIntegrationServiceBean;
import com.pradera.security.usermgmt.service.UserSessionServiceBean;
import com.pradera.security.usermgmt.view.filters.UserAccountFilter;
import com.pradera.security.utils.view.LDAPUtilities;
import com.pradera.security.utils.view.LDAPUtility;
import com.pradera.security.utils.view.RestServiceNotification;
import com.pradera.security.utils.view.SecurityUtilities;

import dev.samstevens.totp.code.CodeGenerator;
import dev.samstevens.totp.code.DefaultCodeGenerator;
import dev.samstevens.totp.code.DefaultCodeVerifier;
import dev.samstevens.totp.code.HashingAlgorithm;
import dev.samstevens.totp.exceptions.QrGenerationException;
import dev.samstevens.totp.qr.QrData;
import dev.samstevens.totp.qr.QrGenerator;
import dev.samstevens.totp.qr.ZxingPngQrGenerator;
import dev.samstevens.totp.secret.DefaultSecretGenerator;
import dev.samstevens.totp.secret.SecretGenerator;
import dev.samstevens.totp.time.SystemTimeProvider;
import dev.samstevens.totp.time.TimeProvider;




/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class UserServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 25/03/2014
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class UserServiceFacade {

	@Inject
	ClientRestService clientRestService;
	
	/** The user integration service bean. */
	@EJB
	private UserIntegrationServiceBean  userIntegrationServiceBean;
	
	/** The user exception service bean. */
	@EJB
	private UserExceptionServiceBean userExceptionServiceBean;
	
	/** The user account service bean. */
	@EJB
	private UserAccountServiceBean userAccountServiceBean;
	
	/** The user session service bean. */
	@EJB
	private UserSessionServiceBean userSessionServiceBean;
	
	/** The system option service bean. */
	@EJB
	private SystemOptionServiceBean systemOptionServiceBean;
	
	/** The generator menu. */
	@Inject
	private GeneratorMenu generatorMenu;
	
	/** The alert service bean. */
	@EJB
	AlertsServiceBean alertServiceBean;
	
    /**  The transaction repository bean. */
    @Resource
    private TransactionSynchronizationRegistry transactionRegistry;
    
    /** The notification service facade. */
    @EJB
	protected NotificationServiceFacade notificationServiceFacade;
    
    /**  The eventEmail. */
    @Inject
	@NotificationEvent(NotificationType.EMAIL)
	private Event<BrowserWindow> eventEmail;
    
    /** The ldap utilities. */
    @Inject
    @LDAPUtility
	LDAPUtilities ldapUtilities;
    
    /** The rest service notification. */
    @Inject
    private RestServiceNotification restServiceNotification;
    
    /** The san configuration. */
    @Inject
	@Configurable("app.security.behavior.san.enabled")
	private boolean sanConfiguration;
    
    /** The number request password. */
    @Inject
	@Configurable("app.security.number.request.password")
    private Integer numberRequestPassword;
    
    /** The user administrator. */
    @Inject
	@Configurable("app.user.admin.key")
    private String userAdministrator;
    
    
	/**
	 * Confirm user sate.
	 *
	 * @param userAccount the user account
	 * @param password the password
	 * @throws ServiceException the service exception
	 * @throws Exception the exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED) 
	public void confirmUserSate(UserAccount  userAccount,String password) throws ServiceException,Exception {
   	 	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
   	 	userAccount.setLastModifyDate(loggerUser.getAuditTime());
   	 	userAccount.setLastModifyIp(loggerUser.getIpAddress());
   	 	userAccount.setLastModifyPriv(loggerUser.getUserAction().getIdPrivilegeConfirm());
		confirmUserServiceFacade(userAccount);
		if(this.ldapUtilities.isEnableAuthentication()) {
			ldapUtilities.addUser(userAccount, password);
		}
		//Se envia email y notificacion Confirmacion de Usuario
		Object[] parameters = {userAccount.getLoginUser()};
		restServiceNotification.sendNotification(loggerUser,BusinessProcessType.USER_CONFIRMATION.getCode(),parameters);
		if(userAccount.getType().equals(UserAccountType.EXTERNAL.getCode()) || !sanConfiguration){
		//Se envia email y notificacion Autorizacion de Usuario
		String message=PropertiesUtilities.getMessage(
				"email.user.content.confirmation.register",
				new Object[]{userAccount.getLoginUser(),password});
		BrowserWindow browser = new BrowserWindow();
		browser.setSubject(PropertiesUtilities.getMessage("email.user.subject.confirmation.register"));
		browser.setMessage(message);
		browser.setUserChannel(userAccount.getIdEmail()+";");
		browser.setNotificationType(NotificationType.EMAIL.getCode());
		browser.setPriority("1");
		eventEmail.fire(browser);
		}
	}
		   
	 /**
 	 * Register user service facade.
 	 *
 	 * @param userAccount the user account
 	 * @return true, if successful
 	 * @throws ServiceException the service exception
 	 */
 	public boolean registerUserServiceFacade(UserAccount userAccount) throws ServiceException{
	 	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

	 	userAccountServiceBean.createOnCascade(userAccount);
	 	
	 	//Se envia email y notificacion
		Object[] parameters = {userAccount.getLoginUser()};
		restServiceNotification.sendNotification(loggerUser,BusinessProcessType.USER_REGISTRATION.getCode(),parameters);
	 	
		return true;
 	}
	 
	 /**
 	 * Modify user service facade.
 	 *
 	 * @param userAccount the user account
 	 * @return true, if successful
 	 * @throws ServiceException the service exception
 	 */
 	public boolean modifyUserServiceFacade(UserAccount userAccount) throws ServiceException{
	 	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	 	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
	 	List<UserProfileAllowed> profilesAllowedUpdated= userAccount.getUserProfilesAllowed();
	 	userAccount.setUserProfilesAllowed(null);
	 	userAccountServiceBean.update(userAccount);
	 	if(profilesAllowedUpdated!= null){
	 		for(UserProfileAllowed profileAllowed:profilesAllowedUpdated){
	 			userAccountServiceBean.update(profileAllowed);
	 		}
	 	}
		//Se envia email y notificacion
		Object[] parameters = {userAccount.getLoginUser()};
		restServiceNotification.sendNotification(loggerUser,BusinessProcessType.USER_MODIFICATION.getCode(),parameters);
		return true;
	 }

    /**
     * Confirm user service facade.
     *
     * @param userAccount the user account
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    @Interceptors(HistoricalStateInterceptor.class)
 	public boolean confirmUserServiceFacade(UserAccount userAccount) throws ServiceException{
	 	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	 	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
	 	userAccountServiceBean.update(userAccount);
		//Se envia email y notificacion
		Object[] parameters = {userAccount.getLoginUser()};
		restServiceNotification.sendNotification(loggerUser,BusinessProcessType.USER_MODIFICATION.getCode(),parameters);
		return true;
	 }
    
	 /**
 	 * Update user account state service bean.
 	 *
 	 * @param usuarioAccount the usuario account
 	 * @throws ServiceException the service exception
 	 */
    @LoggerAuditWeb
    @Interceptors(HistoricalStateInterceptor.class)
 	public void updateUserAccountStateServiceBean(UserAccount usuarioAccount) throws ServiceException{
 	 	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
 	 	Long businessProcess = null;
 	 	
 	 	if(UserAccountStateType.CONFIRMED.getCode().equals(usuarioAccount.getState()) 
			&& UserAccountSituationType.PENDING_LOCK.getCode().equals(usuarioAccount.getSituation())){
 	 		
 	 		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeBlock());
 	 		businessProcess = BusinessProcessType.USER_REQUEST_BLOCK.getCode();
 	 	}
 	 	if(UserAccountStateType.BLOCKED.getCode().equals(usuarioAccount.getState()) 
 			&& UserAccountSituationType.ACTIVE.getCode().equals(usuarioAccount.getSituation())){
 	 		
 	 		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeBlock());
 	 		businessProcess = BusinessProcessType.USER_BLOCK.getCode();
 	 	}
 	 	if(UserAccountStateType.BLOCKED.getCode().equals(usuarioAccount.getState()) 
			&& UserAccountSituationType.PENDING_UNLOCK.getCode().equals(usuarioAccount.getSituation())){
 	 		
 	 		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeUnblock());
 	 		businessProcess = BusinessProcessType.USER_REQUEST_UNBLOCK.getCode();
 	 	}
 	 	if(UserAccountStateType.CONFIRMED.getCode().equals(usuarioAccount.getState()) 
 			&& UserAccountSituationType.ACTIVE.getCode().equals(usuarioAccount.getSituation())){
 	 		
 	 		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeUnblock());
 	 		businessProcess = BusinessProcessType.USER_UNBLOCK.getCode();
 	 	}
 	 	if(UserAccountStateType.REJECTED.getCode().equals(usuarioAccount.getState())){
 	 		
 	 		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
 	 		businessProcess = BusinessProcessType.USER_REJECT.getCode();
 	 	}
 	 	if(UserAccountSituationType.PENDING_UNREGISTER.getCode().equals(usuarioAccount.getSituation())){
 	 		
 	 		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
 	 		businessProcess = BusinessProcessType.USER_REQUEST_ANNULAR.getCode();
 	 	}
 	 	if(UserAccountStateType.ANNULLED.getCode().equals(usuarioAccount.getState()) 
 			&& UserAccountSituationType.ACTIVE.getCode().equals(usuarioAccount.getSituation())){
 	 		
 	 		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
 	 		businessProcess = BusinessProcessType.USER_ANNULAR.getCode();
 	 	}
 	 	
 	 	if(businessProcess != null){
 	 		usuarioAccount.setLastModifyIp(loggerUser.getIpAddress());
 	 		usuarioAccount.setLastModifyUser(loggerUser.getUserName());
 	 		usuarioAccount.setLastModifyPriv(loggerUser.getIdPrivilegeOfSystem()==null?0:loggerUser.getIdPrivilegeOfSystem());
 	 		usuarioAccount.setLastModifyDate(CommonsUtilities.currentDateTime());
 	 		userAccountServiceBean.updateUserAccountStateServiceBean(usuarioAccount);
 	 		
 	 		//Se envia email y notificacion
 			Object[] parameters = {usuarioAccount.getLoginUser()};
 			restServiceNotification.sendNotification(loggerUser,businessProcess,parameters);
 			 
 	 	}
	 }
    
	 
	 /**
	 * Search users with schedule exception by filter service facade.
	 *
	 * @param usuarioAccountFilter the usuario account filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<UserAccount> searchUsersWithScheduleExceptionByFilterServiceFacade(UserAccountFilter usuarioAccountFilter) throws ServiceException{
		List<UserAccount> userAccountList = userAccountServiceBean.searchUsersByFilterForScheduleException(usuarioAccountFilter);
		if(userAccountList ==null || userAccountList.isEmpty()){
			return new ArrayList<UserAccount>();
		}
				
		for(UserAccount userAccount : userAccountList){		
			List<ScheduleExceptionDetail> scheduleExceptionDetailList = new ArrayList<ScheduleExceptionDetail>();
			for(ScheduleExceptionDetail scheduleExceptionDetail: userAccount.getScheduleExceptionDetails()){
				if((scheduleExceptionDetail.getState().equals(ScheduleExceptionDetailStateType.REGISTERED.getCode()) 
					||scheduleExceptionDetail.getState().equals(ScheduleExceptionDetailStateType.CONFIRMED.getCode()))
					&& CommonsUtilities.isLessOrEqualDate(scheduleExceptionDetail.getScheduleException().getEndDate(),CommonsUtilities.currentDate(),true)){
					scheduleExceptionDetailList.add(scheduleExceptionDetail);
				}
			}
			userAccount.setScheduleExceptionDetails(scheduleExceptionDetailList);
		}
		return userAccountList;
	 }
	 
    /**
 	 * Update user account state service bean.
 	 *
 	 * @param usuarioAccount the usuario account
 	 * @throws ServiceException the service exception
 	 */
    @Interceptors(HistoricalStateInterceptor.class)
 	public void updateUserAccountState(UserAccount usuarioAccount) throws ServiceException{ 	 	
		 userAccountServiceBean.updateUserAccountStateServiceBean(usuarioAccount);
		 
		 LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(userAdministrator, JSFUtilities.getRemoteIpAddress());		 
		// Se envia email y notificacion
		 Object[] parameters = {usuarioAccount.getLoginUser()};
		 restServiceNotification.sendNotification(objLoggerUser, BusinessProcessType.REGISTER_LOCK_YOU_FOR_FAILED.getCode(), parameters);
	 }
    
	 /**
 	 * Search institution type by user service facade.
 	 *
 	 * @param idUserPk the id user pk
 	 * @param state the state
 	 * @return the list
 	 * @throws ServiceException the service exception
 	 */
 	public List<InstitutionTypeDetail> searchInstitutionTypeByUserServiceFacade(int idUserPk,int state) throws ServiceException{
		 return userAccountServiceBean.searchInstitutionTypeByUserServiceBean(idUserPk,state);
	 }
	 
	 /**
 	 * Search registered profiles by user service facade.
 	 *
 	 * @param userProfile the user profile
 	 * @return the list
 	 * @throws ServiceException the service exception
 	 */
 	public List<UserProfile> searchRegisteredProfilesByUserServiceFacade(UserProfile userProfile) throws ServiceException{
		 return userAccountServiceBean.searchRegisteredProfilesByUserServiceBean(userProfile);
	 }
 	
 	
	 /**
 	 * Search registered profiles allowed user.
 	 *
 	 * @param userProfile the user profile
 	 * @return the list
 	 * @throws ServiceException the service exception
 	 */
 	public List<UserProfileAllowed> searchRegisteredProfilesAllowedUser(UserProfile userProfile) throws ServiceException{
 		List<UserProfileAllowed> profilesAllowed = userAccountServiceBean.searchRegisteredProfilesAllowedUser(userProfile);
 		for(UserProfileAllowed profile : profilesAllowed){
 			profile.getSystemProfile().getSystem().getName();
 		}
		 return profilesAllowed;
	 }
	 
	 /**
 	 * Get a especific user account.
 	 *
 	 * @param userAccount the user account
 	 * @return the user account
 	 * @throws ServiceException the service exception
 	 */
 	public UserAccount getUserAccountByLogin(UserAccount userAccount) throws ServiceException{
		return userAccountServiceBean.getUserAccount(userAccount);
	 }
	 
 	/**
	  * Validate user service facade.
	  *
	  * @param userAccount the user account
	  * @return the user account
	  * @throws ServiceException the service exception
	  */
	 public UserAccount validateUserServiceFacade(UserAccountFilter userAccount) throws ServiceException{
		 return userAccountServiceBean.validateUserServiceBean(userAccount);
	 }
	 /**
 	 * Update change password service facade.
 	 *
 	 * @param userAccount the user account
 	 * @param password the password
 	 * @throws ServiceException the service exception
 	 * @throws Exception the exception
 	 */
 	@TransactionAttribute(TransactionAttributeType.REQUIRED)
 	public void updateChangePasswordServiceFacade(UserAccount userAccount,String password) throws ServiceException,Exception{
 		  LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
 		  loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		  
 		  userAccountServiceBean.updateChangePasswordServiceBean(userAccount);
 		  if(this.ldapUtilities.isEnableAuthentication()) {
 			  ldapUtilities.resetPassword(userAccount, password);
 		  }
		  
		  //Se envia email y notificacion
		  String message=PropertiesUtilities.getMessage(
				  "email.user.content.reset.password",
				  new Object[]{password,userAccount.getLoginUser()});
		  BrowserWindow browser = new BrowserWindow();
		  browser.setSubject(PropertiesUtilities.getMessage("email.user.subject.reset.password"));
		  browser.setMessage(message);
		  browser.setUserChannel(userAccount.getIdEmail()+";");
		  browser.setNotificationType(NotificationType.EMAIL.getCode());
		  browser.setPriority("1");
		  eventEmail.fire(browser);
		  
//		 // Se envia email y notificacion
//		 Object[] parameters = {userAccount.getLoginUser()};
//		 restServiceNotification.sendNotification(loggerUser, BusinessProcessType.PASSWORD_RESTART_REGISTER.getCode(), parameters);
		  
	 }
 	
 	@TransactionAttribute(TransactionAttributeType.REQUIRED)
 	public void resetMfaServiceFacade(UserAccount userAccount) throws ServiceException,Exception{
 		  LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
 		  loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
 		  userAccount.setAudit(loggerUser);
 		  
 		  userAccountServiceBean.resetMfaServiceBean(userAccount);		
 	}
 	/**
	  * Change password user.
	  *
	  * @param userAccount the user account
	  * @param userName the user name
	  * @param oldPassword the old password
	  * @param newPassword the new password
	  * @param confirmationPassword the confirmation password
	  * @return the change pwd message type
	  * @throws Exception the exception
	  */
	 public ChangePwdMessageType changePasswordUser(Integer userAccount,String userName,String oldPassword,String newPassword,String confirmationPassword) throws Exception{
 		userSessionServiceBean.updateUserAccountAccess(userAccount,null,CommonsUtilities.currentDateTime(),null);
 		return ldapUtilities.changePassword(userName, oldPassword, newPassword, confirmationPassword);
 	}
 	
 	/**
	  * Send forgot password.
	  *
	  * @param userName the user name
	  * @param email the email
	  * @throws ServiceException the service exception
	  * @throws Exception the exception
	  */
	 public void sendForgotPassword(String userName,String email) throws ServiceException,Exception{
 		UserAccount userAccount = userAccountServiceBean.validateUserEmail(userName, email);
 		if(userAccount != null){
 			//User is correct so check number try
 			if(userAccount.getCountRequestPassword().intValue() < numberRequestPassword.intValue()){
 				//update number request password
 				userSessionServiceBean.updateUserAccountAccess(userAccount.getIdUserPk(),null,null,
 					(userAccount.getCountRequestPassword().intValue()+1));
 				String password = SecurityUtilities.generatePassword().substring(0, ldapUtilities.getPwdMinLength());
 				//reset password
 				userAccount.setLoginUser(userName);
 				if(this.ldapUtilities.isEnableAuthentication()) {
 					ldapUtilities.resetPassword(userAccount, password);
 				}
				// Se envia email y notificacion
				String message = PropertiesUtilities.getMessage("email.user.content.forgot.password",
						new Object[] { password });
				BrowserWindow browser = new BrowserWindow();
				browser.setSubject(PropertiesUtilities.getMessage("email.user.subject.forgot.password"));
				browser.setMessage(message);
				browser.setUserChannel(email + ";");
				browser.setNotificationType(NotificationType.EMAIL.getCode());
				browser.setPriority("1");
				eventEmail.fire(browser);

				LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(userAdministrator,
						JSFUtilities.getRemoteIpAddress());
				// Se envia email y notificacion
				Object[] parameters = { userAccount.getLoginUser() };
				restServiceNotification.sendNotification(objLoggerUser,
						BusinessProcessType.REGISTER_FORGOT_YOUR_PASSWORD.getCode(), parameters);

 			} 
 		}
 	}

	/**
	 * Register user session service facade.
	 *
	 * @param userSession the user session
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean registerUserSessionServiceFacade(UserSession userSession) throws ServiceException{
		userSessionServiceBean.create(userSession,null);
		return true;
	}

	/**
	 * Kill all sessions by user service facade.
	 *
	 * @param userSession the user session
	 * @throws ServiceException the service exception
	 */
	public void killAllSessionsByUserServiceFacade(UserSession userSession) throws ServiceException{
		userSessionServiceBean.killAllSessionsByUserServiceBean(userSession);
	}
	
	/**
	 * Gets the user session by ticked service facade.
	 *
	 * @param userSession the user session
	 * @return the user session by ticked service facade
	 * @throws ServiceException the service exception
	 */
	public UserSession getUserSessionByTickedServiceFacade(UserSession userSession) throws ServiceException{
		return userSessionServiceBean.getUserSessionByTickedServiceBean(userSession);
	}
	
	/**
	 * List availiable privileges by user service facade.
	 *
	 * @param userAccountFilter the user account filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<OptionPrivilege> listAvailiablePrivilegesByUserServiceFacade(UserAccountFilter userAccountFilter) throws ServiceException{
		return userAccountServiceBean.listAvailiablePrivilegesByUserServiceBean(userAccountFilter);
	}
	
	/**
	 * Gets the institutions by filter service facade.
	 *
	 * @param filter the filter
	 * @return the institutions by filter service facade
	 * @throws ServiceException the service exception
	 */
	public List<InstitutionsSecurity> getInstitutionsByFilterServiceFacade(InstitutionSecurityFilter filter) throws ServiceException{ 
		return userIntegrationServiceBean.getInstitutionsByFilterServiceBean(filter);
	}

	/**
	 * Close session service facade.
	 *
	 * @param userSession the user session
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void closeSessionServiceFacade(UserSession userSession) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		userSession.setAudit(loggerUser);
		userSessionServiceBean.closeSessionServiceBean(userSession);	
	}
	
	/**
	 * Update access system user.
	 *
	 * @param userAccount the user account
	 * @throws ServiceException the service exception
	 */
	public void updateAccessSystemUser(Integer userAccount) throws ServiceException{
		//update access
		userSessionServiceBean.updateUserAccountAccess(userAccount,CommonsUtilities.currentDateTime(), null, 0);
	}
	
	/**
	 * Gets the options for menu generation.
	 *
	 * @param userAccountFilter the user account filter
	 * @param lstAvailiableOptionsUrl the lst availiable options url
	 * @return the options for menu generation
	 * @throws ServiceException the service exception
	 */
	public List<SystemOption> getOptionsForMenuGeneration(UserAccountFilter userAccountFilter,List<String> lstAvailiableOptionsUrl) throws ServiceException{
		List<SystemOption> lstSystemOption = new ArrayList<SystemOption>();
		//Get available privileges for option
		List<OptionPrivilege> lstAvailiableOptionPrivileges = userAccountServiceBean
				.listAvailiablePrivilegesByUserServiceBean(userAccountFilter);
		//To get the list of userException Option Privileges
		lstAvailiableOptionPrivileges.addAll(userExceptionServiceBean.listAvailablePrivilegesByUserExceptionServiceBean(userAccountFilter.getIdUserAccountPK()));
		//To remove duplicate values
		Map<Integer,OptionPrivilege> mapOptionPrivilege= new HashMap<Integer,OptionPrivilege>();
		for(OptionPrivilege optionPrivilege : lstAvailiableOptionPrivileges){
			if(!(mapOptionPrivilege.containsKey(optionPrivilege.getIdOptionPrivilegePk()))){
				mapOptionPrivilege.put(optionPrivilege.getIdOptionPrivilegePk(), optionPrivilege);
			}
		}
		lstAvailiableOptionPrivileges.clear();
		lstAvailiableOptionPrivileges.addAll(mapOptionPrivilege.values());
		mapOptionPrivilege.clear();
		
		if (!lstAvailiableOptionPrivileges.isEmpty()) {
			List<Integer> lstIdOptionPrivilege = new ArrayList<Integer>();
			for (OptionPrivilege optionPrivilege : lstAvailiableOptionPrivileges) {
				lstIdOptionPrivilege.add(optionPrivilege
						.getIdOptionPrivilegePk());
				if(optionPrivilege.getSystemOption().getUrlOption()!= null && !optionPrivilege.getSystemOption().getUrlOption().startsWith("/")){
					lstAvailiableOptionsUrl.add("/"+optionPrivilege.getSystemOption().getUrlOption());
				}else{
					lstAvailiableOptionsUrl.add(optionPrivilege.getSystemOption().getUrlOption());
				}
			}
			//Get system options from privileges
			SystemProfileFilter systemProfileFilter = new SystemProfileFilter();
			systemProfileFilter
					.setSystemOptionState(OptionStateType.CONFIRMED
							.getCode());
			systemProfileFilter.setIdSystem(userAccountFilter.getIdSystemPk());
			systemProfileFilter
					.setLstIdOptionPrivilege(lstIdOptionPrivilege);
			List<Integer> lstIdSystemOptionByPrivileges = systemOptionServiceBean.listIdSystemOptiosByOptionPrivilegeServiceBean(systemProfileFilter);
			//parameter language from login
			systemProfileFilter.setIdLanguage(userAccountFilter.getIdLanguage());
			systemProfileFilter
					.setLstIdSystemOption(lstIdSystemOptionByPrivileges);
			//Get options with language menu
			List<Object[]> lstObjSystemOption = systemOptionServiceBean
					.listSystemOptionsByIdsServiceBean(systemProfileFilter);
			SystemOption systemOption = null;
			SystemOption parentSystemOption = null;
			List<CatalogueLanguage> catalogueLanguages = null;
			CatalogueLanguage catalogueLanguage = null;
			List<OptionPrivilege> optionPrivileges = null;
			for (Object[] rowSystemOption : lstObjSystemOption) {
				systemOption = new SystemOption();
				systemOption.setIdSystemOptionPk(Integer.valueOf(
						rowSystemOption[0].toString()));
				if (rowSystemOption[1] != null) {
					parentSystemOption = new SystemOption();
					parentSystemOption.setIdSystemOptionPk(Integer.valueOf(
							rowSystemOption[1].toString()));
					systemOption.setParentSystemOption(parentSystemOption);
				}
				systemOption.setOrderOption(Integer.valueOf(rowSystemOption[3]
						.toString()));
				if (rowSystemOption[4] != null) {
					systemOption
							.setUrlOption(rowSystemOption[4].toString());
				}
				systemOption.setOptionType(Integer.valueOf(rowSystemOption[5]
						.toString()));
				if (rowSystemOption[7] != null) {
					systemOption.setModule(Integer.valueOf(rowSystemOption[7]
							.toString()));
				}

				catalogueLanguages = new ArrayList<CatalogueLanguage>();
				catalogueLanguage = new CatalogueLanguage();
				catalogueLanguage.setCatalogueName(rowSystemOption[2]
						.toString());
				catalogueLanguage.setLanguage(Integer.valueOf(
						rowSystemOption[6].toString()));
				catalogueLanguages.add(catalogueLanguage);

				systemOption.setCatalogueLanguages(catalogueLanguages);
											
				optionPrivileges = new ArrayList<OptionPrivilege>();
				for (OptionPrivilege optionPrivilege : lstAvailiableOptionPrivileges) {
					if (systemOption.getIdSystemOptionPk().equals(
							optionPrivilege.getSystemOption()
									.getIdSystemOptionPk())) {
						optionPrivileges.add(optionPrivilege);
					}
				}
				if (!optionPrivileges.isEmpty()) {
					systemOption.setOptionPrivileges(optionPrivileges);
				}

				lstSystemOption.add(systemOption);
			}
		}
		return lstSystemOption;
	}
	
	
	/**
	 * Gets the userinfo by ticket.
	 * that method should be call for modules remote
	 * @param userSession the user session
	 * @param moduleApplication the module application
	 * @return the user info by ticket
	 * @throws ServiceException the service exception
	 */
	public UserInfo getUserInfoByTicket(UserSession userSession,String moduleApplication) throws ServiceException {
		UserInfo userInfo= null;
		userSession = userSessionServiceBean.getUserSessionByTickedServiceBean(userSession);
		if (userSession != null) {
			//Ticket is valid
			UserAccount userAccount = userSession.getUserAccount();
			userInfo = new UserInfo();
			userInfo.setUserAccountSession(new UserAccountSession());
			userInfo.getUserAccountSession().setIdUserAccountPk(userAccount.getIdUserPk());
			userInfo.getUserAccountSession().setUserName(userAccount.getLoginUser());
			userInfo.getUserAccountSession().setFullName(userAccount.getFullName());
			userInfo.getUserAccountSession().setEmail(userAccount.getIdEmail());
			userInfo.getUserAccountSession().setState(userAccount.getState());
			userInfo.getUserAccountSession().setUserInstitutionName(userAccount.getInstitutionsSecurity().getInstitutionName());
			userInfo.getUserAccountSession().setIpAddress(userSession.getLastModifyIp());
			userInfo.getUserAccountSession().setInstitutionType(userAccount.getInstitutionTypeFk());
			userInfo.getUserAccountSession().setParticipantCode(userAccount.getIdParticipantFk());
			userInfo.getUserAccountSession().setInstitutionCode(userAccount.getInstitutionsSecurity().getIdInstitutionSecurityPk());
			userInfo.getUserAccountSession().setIssuerCode(userAccount.getIdIssuerFk());
			userInfo.getUserAccountSession().setRegulatorAgent(userAccount.getRegulatorAgent());
			Subsidiary objSubsidiary = null;
			if(userAccount.getSubsidiary()!=null){
				objSubsidiary = userSessionServiceBean.getSubsidiary(userAccount.getSubsidiary().getIdSubsidiaryPk());
				userInfo.getUserAccountSession().setIdSubsidiaryPk(objSubsidiary.getIdSubsidiaryPk());
				userInfo.getUserAccountSession().setDepartment(objSubsidiary.getDepartament());
			} else {
				userInfo.getUserAccountSession().setIdSubsidiaryPk(null);
				userInfo.getUserAccountSession().setDepartment(null);
			}			
			userInfo.getUserAccountSession().setDescInstitutionType(
					this.userExceptionServiceBean.getParameter(userAccount.getInstitutionTypeFk()).getDescription());
			//Responsability(Supervisor/operator)
			userInfo.getUserAccountSession().setResponsability(userAccount.getResponsability());
			userInfo.setCurrentLanguage(userSession.getLocale());
			//TODO check where load language
			userInfo.getUserAccountSession().setTicketSession(userSession.getTicket());
//			userInfo.getUserAccountSession().setLastSucessAccess(
//					userSession.getInitialDate());
			userInfo.getUserAccountSession().setIdUserSessionPk(userSession.getIdUserSessionPk());
			//TODO fields should be loading
			userInfo.setCurrentSystem(userSession.getSystem().getName());
//			userInfo.setEndTimeConnection(endTimeConnection);
			UserAccountFilter userAccountFilter = new UserAccountFilter();
			userAccountFilter.setIdUserAccountPK(userInfo.getUserAccountSession().getIdUserAccountPk());
			userAccountFilter.setIdSystemPk(userSession.getSystem().getIdSystemPk());
			//TODO read language from db 
			for (LanguageType language : LanguageType.list) {
				if (language.getValue().equals(userSession.getLocale())) {
					userAccountFilter.setIdLanguage(language.getCode());
					break;
				}
			}
//			userAccountFilter.setIdLanguage(LanguageType.SPANISH.getCode());
			//get options fo menu
			List<String> lstAvailiableOptionsUrl = new ArrayList<String>();
			List<SystemOption> lstSystemOption = getOptionsForMenuGeneration(userAccountFilter,lstAvailiableOptionsUrl);
			//Generate menu
			userInfo.setPraderaMenuAll(generatorMenu.generateMenu(lstSystemOption, userSession.getSystem().getSystemUrl()));
//			Not generate optionPrivileges for limit in length string to 65365
//			if (userInfo.getOptionPrivileges() == null) {
//				userInfo.setOptionPrivileges(new HashMap<String, UserAcctions>());
//			}
//			PraderaMenu praderaMenu = userInfo.getPraderaMenuAll();
//			if (praderaMenu != null) {
//				for (PraderaSubMenu subMenu : praderaMenu.getSubMenus()){
//					userInfo = fillUserActions(subMenu, userInfo);
//				}
//			}
//			//remote
//			if(StringUtils.isNotBlank(moduleApplication)) {
//				userInfo.setMenu(menuProducer.showMenu(userInfo,LanguageType.SPANISH.getValue(),moduleApplication));
//			} 
			userInfo.setAvailiableOptionsUrl(lstAvailiableOptionsUrl);	
			
			// Add for subsidiary
			if(userAccount.getSubsidiary() != null && objSubsidiary != null &&
					Validations.validateIsNotNullAndNotEmpty(userAccount.getSubsidiary().getIdSubsidiaryPk())){
				UserAccountFilter objUserAccountFilter = new UserAccountFilter();
				objUserAccountFilter.setIdInstitutionSecurity(userAccount.getInstitutionsSecurity().getIdInstitutionSecurityPk());
				objUserAccountFilter.setInstitutionType(userAccount.getInstitutionTypeFk());
				objUserAccountFilter.setSubsidiary(objSubsidiary);
				List<UserAccount> lstUserAccount = userAccountServiceBean.getLstUserAccountBySubsidiary(objUserAccountFilter);
				if(Validations.validateListIsNotNullAndNotEmpty(lstUserAccount)){
					Map<String, Integer> userAgencies = new HashMap<String, Integer>(); 
					for(UserAccount objUserAccount : lstUserAccount){
						userAgencies.put(objUserAccount.getLoginUser(), objUserAccount.getIdUserPk());
					}
					userInfo.setUserAgencies(userAgencies);
				}							
			}
			
			// add participant issuer code
			if(InstitutionType.ISSUER_DPF.getCode().equals(userInfo.getUserAccountSession().getInstitutionType())){
				userInfo.getUserAccountSession().setPartIssuerCode(getParticipantIssuerCode(userAccount.getIdIssuerFk()));
			}
			
			// add verified user is operator
			boolean isOperator = userSessionServiceBean.getUserIsOperator(userInfo.getUserAccountSession().getIdUserAccountPk());
			if(isOperator){
				userInfo.getUserAccountSession().setIsOperator(GeneralConstants.ONE_VALUE_INTEGER);
			}			
		}
		return userInfo;
	}
	
	/**
	 * Fill user actions.
	 *
	 * @param subMenu the sub menu
	 * @param userInfo the user info
	 * @return the user info
	 */
	public UserInfo fillUserActions(PraderaSubMenu subMenu, UserInfo userInfo){
		if(subMenu.getOptions() != null && subMenu.getOptions().size() > 0){
			for(PraderaMenuOption menuop : subMenu.getOptions()){
				userInfo.getOptionPrivileges().put(getAfterModule(menuop.getUrl()), menuop.getUserAcctions());
			}
		}
		if(subMenu.getSubMenus() != null && subMenu.getSubMenus().size() > 0){
			for(PraderaSubMenu sbMenu : subMenu.getSubMenus()){
				fillUserActions(sbMenu, userInfo);
			}
		}
		return userInfo;
	}
	
	/**
	 * return the relative url after the module from give url.
	 *
	 * @param url complete url
	 * @return relative url without module
	 */
	public String getAfterModule(String url){
		String module = "";
		if(url != null){
			String[] strArr = url.split("/");
			for(int i=0; i<strArr.length; i++){
				if(strArr[i] != null && strArr[i].trim().length() > 0 && !strArr[i].contains(":")&& !strArr[i].contains(".")){
					module = strArr[i];
					break;
				}
			}
		}
		return StringUtils.substringAfter(url, module);
	}
	
	/**
	 * Update current system user session.
	 *
	 * @param userSession the user session
	 */
	public void updateCurrentSystemUserSession(UserSession userSession) {
		UserSession userSessionRegistered = userSessionServiceBean.find(UserSession.class, userSession.getIdUserSessionPk());
		userSessionRegistered.setSystem(userSession.getSystem());
	}
	 /**
 	 * Validate Doc Numberuserr service facade.
 	 *
 	 * @param userAccount the user account
 	 * @return true, if successful
 	 * @throws ServiceException the service exception
 	 */
 	public boolean validateDocNumberServiceBean(UserAccount userAccount) throws ServiceException{
		 return userAccountServiceBean.validateDocNumberServiceBean(userAccount)>0;
	 }
 	
 	/**
	  * getting the institution name.
	  *
	  * @param instSecurityPk the inst security pk
	  * @return the institutions name
	  * @throws ServiceException the service exception
	  */
 	public String getInstitutionsName(Integer instSecurityPk)throws ServiceException{
 		 return userAccountServiceBean.getInstitutionsName(instSecurityPk);
 	}


	/**
	 * To Check whether the selected user is in session or not.
	 *
	 * @param selectedUserAccountBean the selected user account bean
	 * @return true if success
	 * @throws ServiceException the service exception
	 */
	public boolean isSelectedUserInSessionServiceBean(
			UserAccount selectedUserAccountBean)throws ServiceException  {
		Integer count = userAccountServiceBean.isSelectedUserInSessionServiceFacade(selectedUserAccountBean);
		if (count > 0){
			return true;
		}
		 return false;
	}
	
	/**
	 * Gets the holidays.
	 *
	 * @return the holidays
	 */
	public List<String> getHolidays(){
		List<String> list = null;
		SimpleDateFormat sdf = new SimpleDateFormat("M-dd-yyyy");
		List<Holiday> holidays = userAccountServiceBean.getHolidays();
		if(holidays != null){
			list = new ArrayList<String>();
			for (Holiday holiday : holidays) {
				list.add(sdf.format(holiday.getDateHoliday()));
			}
		}
		return list;
	}
	

	/**
	 * Gets the users with name service facade.
	 *
	 * @param loginUser the login user
	 * @return userlist
	 * @throws ServiceException the service exception
	 */
	public List<String> getUsersWithNameServiceFacade(String loginUser) throws ServiceException {
		return userAccountServiceBean.getUsersWithNameServiceBean(loginUser);
	}
	
	/**
	 * get UserDetails based on the userID.
	 *
	 * @param loginUser the login user
	 * @return the user account details facade
	 * @throws ServiceException the service exception
	 */
	public List<UserAccount> getUserAccountDetailsFacade(String loginUser) throws ServiceException{
		return userAccountServiceBean.getUserAccountDetailsServiceBean(loginUser);
	}
	
	/**
	 * Gets the modify date and state.
	 *
	 * @param tableName the table name
	 * @param columnName the column name
	 * @param idPk the id pk
	 * @return the modify date and state
	 */
	public Object[] getModifyDateAndState(String tableName,String columnName,Integer idPk){
		return userAccountServiceBean.getModifyDateAndState(tableName,columnName,idPk);
	}
	
	/**
	 * Verify user has the privileges is there or not.
	 *
	 * @param userAccountFilter the user account filter
	 * @return the list
	 */
	public List<OptionPrivilege> checkSystemProfiles(UserAccountFilter userAccountFilter){
		List<OptionPrivilege> lstAvailiableOptionPrivileges=new ArrayList<OptionPrivilege>();
		try {
			lstAvailiableOptionPrivileges = userAccountServiceBean.listAvailiablePrivilegesByUserServiceBean(userAccountFilter);
			lstAvailiableOptionPrivileges.addAll(userExceptionServiceBean.listAvailablePrivilegesByUserExceptionServiceBean(userAccountFilter.getIdUserAccountPK()));
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return lstAvailiableOptionPrivileges;
	}
	
	public List<SystemOption> checkSystemOptionsByUserSystemAndOptionFather(String userLogin, String mnemonicSystem,Integer parentSystemOption){
		List<SystemOption> lstAvailiableOptionPrivileges = new ArrayList<SystemOption>();
		try {
			lstAvailiableOptionPrivileges = userAccountServiceBean.listAvailiablePrivilegesByUserAndSystem(userLogin,mnemonicSystem,parentSystemOption);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return lstAvailiableOptionPrivileges;
	}
	
	/**
	 * Used to get the Current User status.
	 *
	 * @param logginUser 				String
	 * @return 			UserSession
	 * @throws ServiceException the service exception
	 */
	public UserSession getCurrentUserStatus(String logginUser) throws ServiceException{
		return userAccountServiceBean.getUserCurrentStatus(logginUser);
	}
	
	public List<UserSession> allRemoteConnectedByUser(String loginUser){
		return userAccountServiceBean.allRemoteConnectedByUser(loginUser);
	}
	/**
	 * To close the remote/current userSession.
	 *
	 * @param userSession the user session
	 * @throws ServiceException the service exception
	 */
	public void closeMultipleSessionFacade(UserSession userSession) throws ServiceException{
		userSessionServiceBean.closeMultipleSessionServiceBean(userSession);
	}	

	public void closeMultipleSessionFacade(UserSession userSession, Long currentSession, Integer idUser) throws ServiceException{
		userSessionServiceBean.closeMultipleSessionServiceBean(userSession,currentSession,idUser);
	}
	

	
	/**
	 * Search all institutions service facade.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<InstitutionsSecurity> searchAllInstitutionsServiceFacade() throws ServiceException{
			return userIntegrationServiceBean.getInstitutionsSecurityServiceBean();
	}
	
	 /**
 	 * List client type by definition service facade.
 	 *
 	 * @param idDefinicion the id definicion
 	 * @return the list
 	 * @throws ServiceException the service exception
 	 */
 	public List<Parameter> listClientTypeByDefinitionServiceFacade(Integer idDefinicion) throws ServiceException{
 		 List<Parameter> listmotives = userExceptionServiceBean.listByDefinitionServiceBean(idDefinicion);
		 if(listmotives!= null && listmotives.size()>0){
		     return listmotives;
		 }
		 else{
		     return new ArrayList<Parameter>();
		 }
	   }
 	
	public Parameter getParameter(Integer idParameterPk) throws ServiceException {
		return userExceptionServiceBean.getParameter(idParameterPk);
	}
	 
	 /**
 	 * Search institutions by filter service facade.
 	 *
 	 * @param filter the filter
 	 * @return the list
 	 * @throws ServiceException the service exception
 	 */
 	public List<InstitutionsSecurity> searchInstitutionsByFilterServiceFacade(InstitutionSecurityFilter filter) throws ServiceException{
		 return userIntegrationServiceBean.getInstitutionsSecurityWithScheduleServiceBean(filter);
	 }
	 
	 /**
 	 * Validate login user service facade.
 	 *
 	 * @param userAccount the user account
 	 * @return true, if successful
 	 * @throws ServiceException the service exception
 	 */
 	public boolean validateLoginUserServiceFacade(UserAccount userAccount) throws ServiceException{
		 return userAccountServiceBean.validateLoginUserServiceBean(userAccount)>0;
	 }
 	
	 
	 /**
	 * Search users by filter service facade.
	 *
	 * @param usuarioAccountFilter the usuario account filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<UserAccount> searchUsersByFilterServiceFacade(UserAccountFilter usuarioAccountFilter) throws ServiceException{
			return userAccountServiceBean.searchUsersByFilterServiceBean(usuarioAccountFilter);
	 }
	 
	/**
	  * To get the UserAccount by using id.
	  *
	  * @param idUserPk the id user pk
	  * @return UserAccount
	  */
   public UserAccount getLastestUserAccountDetailsFacade(Integer idUserPk){
   	return userAccountServiceBean.find(UserAccount.class, idUserPk);
   }
   
   /**
    * Gets the user document unregister.
    *
    * @param idUserPk the id user pk
    * @return the user document unregister
    */
   public byte[] getUserDocumentUnregister(Integer idUserPk){
   	return userAccountServiceBean.getUserDocumentUnregister(idUserPk);
   }
   
   /**
    * Gets the user document register.
    *
    * @param idUserPk the id user pk
    * @return the user document register
    */
   public byte[] getUserDocumentRegister(Integer idUserPk){
   	return userAccountServiceBean.getUserDocumentRegister(idUserPk);
   }
   
   /**
    * Url user manual file.
    *
    * @param systemOption the system option
    * @param userAccount the user account
    * @param system the system
    * @return the user manual profile
    */
   public UserManualProfile getUserManualFile(Integer systemOption,Integer userAccount,Integer system){
	   UserManualProfile userManual = null;
	   Integer profileUser = userAccountServiceBean.getUserProfile(system, userAccount);
	   if(profileUser != null){
		   userManual = userAccountServiceBean.getUserManual(systemOption, profileUser);
	   }
	   return userManual;
   }
   
   @Asynchronous
   @TransactionAttribute(TransactionAttributeType.SUPPORTS)
   public void closeSessionModules(String userName){
	   if(StringUtils.isNotBlank(userName)){
		   for(ModuleWarType war :ModuleWarType.list){
			   if(!war.equals(ModuleWarType.SECURITY)){
				   clientRestService.closeUserSessionModule(userName, war.getValue());
			   }
		   }
	   }
   }
   
   public Long getParticipantIssuerCode(String strIssuerCode) {
	   Long lngParticipantCode = null;
		if(Validations.validateIsNotNullAndNotEmpty(strIssuerCode)){
			lngParticipantCode = clientRestService.getParticipantIssuerCode(strIssuerCode);
		}
		return lngParticipantCode;
   }
   
   /**
    * validate the totp mfa code 
    * @param secret user secret 
    * @param totp mfa code to validate
    * @return boolean
    */
   public boolean validateMFACode(String secret, String code) {
	   TimeProvider timeProvider = new SystemTimeProvider();
	   CodeGenerator codeGenerator = new DefaultCodeGenerator(HashingAlgorithm.SHA1, 6);
	   DefaultCodeVerifier verifier = new DefaultCodeVerifier(codeGenerator, timeProvider);
	// sets the time period for codes to be valid for to 60 seconds
	   verifier.setTimePeriod(30);

	   // allow codes valid for 2 time periods before/after to pass as valid
	   verifier.setAllowedTimePeriodDiscrepancy(0);
		// secret = the shared secret for the user
		// code = the code submitted by the user
		return verifier.isValidCode(secret, code);
   }
   
   
   public String registerMFASecret(UserAccount userAccount) {
	   // Generates secrets that are 64 characters long
	   SecretGenerator secretGenerator = new DefaultSecretGenerator();
	   String secret = secretGenerator.generate();
	   QrData data = new QrData.Builder()
			   .label(userAccount.getLoginUser())
			   .secret(secret)
			   .issuer("CAVAPY")
			   .algorithm(HashingAlgorithm.SHA1) // More on this below
			   .digits(6)
			   .period(30)
			   .build();
	   QrGenerator generator = new ZxingPngQrGenerator();
	   try {
			byte[] imageData = generator.generate(data);
			String mimeType = generator.getImageMimeType();
			// mimeType = "image/png"
			String dataUri = getDataUriForImage(imageData, mimeType);
			// dataUri = data:image/png;base64,iVBORw0KGgoAAAANSU...
			userAccount.setMfaSecret(secret);
			this.userAccountServiceBean.updateUserAccountForMFASecret(userAccount);
			return dataUri;
		} catch (QrGenerationException | ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			   
	   return null;
   }
   
   public SystemOption getSystemOption(Integer idSystemOptionPk) {
	   return userAccountServiceBean.find(SystemOption.class,idSystemOptionPk);
   }
   
}