package com.pradera.security.usermgmt.service;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.ejb.Timer;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.audit.service.UserAuditingServiceBean;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class KillSessionUserServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-sep-2015
 */
@Stateless
public class KillSessionUserServiceBean {

    /**
     * Default constructor. 
     */
    public KillSessionUserServiceBean() {
        // TODO Auto-generated constructor stub
    }
    
    @Inject
    PraderaLogger log;
    
    /** The user auditingservice. */
    @EJB
    UserAuditingServiceBean userAuditingservice;
	

	/**
	 * Scheduled timeout.
	 * metodo usado para cerrar session por base de datos
	 * a usuarios que cerraron browser y no salieron por la aplicacion
	 * se ejecuta de lunes a sabado a la media noche con 5 minutos y 1 segundo
	 * @param t the t
	 */
	@Schedule(second="1", minute="5", hour="0", dayOfWeek="Mon-Sat",
      dayOfMonth="*", month="*", year="*", info="KillSession",persistent=false)
    public void scheduledTimeout(final Timer t) {
		/**
		 *  Ruben Carhuaricra 13-nov-2023 se comenta este metodo porque puede estar matando
		 *  al momento de cerrar la sesion del usuario ADMIN 
		 */
		/*try{
		userAuditingservice.closeAllOpenSessions();
		}catch(ServiceException sex){
			log.error(sex.getMessage());
		}*/
    }
}