package com.pradera.security.usermgmt.view;

import java.io.Serializable;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;
import com.pradera.security.model.UserAccount;
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class UserDataModel
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.1
 */
public class UserDataModel extends ListDataModel<UserAccount> implements Serializable,SelectableDataModel<UserAccount>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3820398655190202816L;

	public UserDataModel(){
		super();
	}
	
	public UserDataModel(List<UserAccount> data){
		super(data);
	}

	@Override
	public UserAccount getRowData(String rowKey) {
		// TODO Auto-generated method stub
		List<UserAccount> listaUsuario=(List<UserAccount>)getWrappedData();
		for(UserAccount usuario:listaUsuario){
			if(usuario.getIdUserPk().intValue()==Integer.parseInt( rowKey ))
				return usuario;
		}
		return null;
	}

	@Override
	public Object getRowKey(UserAccount usuario) {
		// TODO Auto-generated method stub
		return usuario.getIdUserPk();
	}
	
	public int getSize(){
		List<UserAccount> usuarios=(List<UserAccount>)getWrappedData();
		if(usuarios==null)
			return 0;
		return usuarios.size();
	}

}
