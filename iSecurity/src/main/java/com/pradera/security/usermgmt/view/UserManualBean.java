package com.pradera.security.usermgmt.view;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.omnifaces.util.Faces;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.configuration.StageDependent;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.menu.PraderaMenu;
import com.pradera.commons.menu.PraderaMenuOption;
import com.pradera.commons.menu.PraderaSubMenu;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.security.model.System;
import com.pradera.security.model.SystemOption;
import com.pradera.security.model.UserManualProfile;
import com.pradera.security.model.type.InstanceType;
import com.pradera.security.systemmgmt.view.OptionCatalogNodeBean;
import com.pradera.security.usermgmt.facade.UserServiceFacade;
import com.pradera.security.utils.view.PropertiesConstants;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class UserManualBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 14-abr-2015
 */
@DepositaryWebBean
public class UserManualBean extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1033940382528114548L;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	@Inject
	ManagerNavigationBean managerNavigationBean;
	
	@EJB
	UserServiceFacade userServiceFacade;
	
	@Inject
	transient PraderaLogger log;
	
	/** The system id selected. */
	private Integer systemIdSelected;
	
	/** The root. */
	private TreeNode root;
	
	@Inject
	@StageDependent
	private String fileRootPath;
	
	/**
	 * Instantiates a new user manual bean.
	 */
	public UserManualBean() {
		// TODO Auto-generated constructor stub
	}
	
	@PostConstruct
	public void init(){
		systemIdSelected = managerNavigationBean.getCurrentSystem().getIdSystemPk();
		loadUserMenu();
	}
	
	@LoggerAuditWeb
	public void downloadFile(Integer systemOptionId){
		if(systemOptionId!= null){
			UserManualProfile userManual = userServiceFacade.getUserManualFile(systemOptionId, userInfo.getUserAccountSession().getIdUserAccountPk(), systemIdSelected);
			
			SystemOption objSystemOption = userServiceFacade.getSystemOption(systemOptionId);
			
			if(objSystemOption!= null){
				Path pathFile = Paths.get(userManual.getUrlDirectory(),userManual.getFileName());
				if(Files.exists(pathFile)){
					try{
					Faces.sendFile(Files.readAllBytes(pathFile), userManual.getFileName(), true);
					} catch(IOException iox){
						log.error(iox.getMessage());
						showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.USER_MANUAL_NOT_FILE, null);
						JSFUtilities.showComponent("cnfEndTransactionSamePage");
					}
				} else {
					showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.USER_MANUAL_NOT_FILE, null);
					JSFUtilities.showComponent("cnfEndTransactionSamePage");
				}
			} else {
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.USER_MANUAL_NOT_FILE, null);
				JSFUtilities.showComponent("cnfEndTransactionSamePage");
			}
		}
	}
	
	public StreamedContent getStreamedContentFile(Integer systemOptionId){
		StreamedContent streamedContentFile = null;

		if(systemOptionId!= null){
			SystemOption objSystemOption = userServiceFacade.getSystemOption(systemOptionId);
			if(objSystemOption!= null){
				try{
					byte[] documentFile = CommonsUtilities.readFile(new File(fileRootPath+objSystemOption.getDocumentFilePath()+File.separator+objSystemOption.getDocumentFileName()));
					InputStream inputStream = null;
					inputStream = new ByteArrayInputStream(documentFile);
					streamedContentFile = new DefaultStreamedContent(inputStream, null,	objSystemOption.getDocumentFileName());
					inputStream.close();
				} catch(IOException iox){
					log.error(iox.getMessage());
					showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.USER_MANUAL_NOT_FILE, null);
					JSFUtilities.showComponent("cnfEndTransactionSamePage");
				}
				
			} else {
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.USER_MANUAL_NOT_FILE, null);
				JSFUtilities.showComponent("cnfEndTransactionSamePage");
			}
		}
		return streamedContentFile;
	}
	
	
	
	@LoggerAuditWeb
	public void loadUserMenu(){
		if(systemIdSelected!=null){
			OptionCatalogNodeBean optionCatalogueNodeBean = new OptionCatalogNodeBean();
			for(System sys:managerNavigationBean.getListSystems()){
				if(systemIdSelected.equals(sys.getIdSystemPk())){
					optionCatalogueNodeBean.setName(sys.getName());
					optionCatalogueNodeBean.setInstanceType(InstanceType.SYSTEM.getCode());
					break;
				}
			}
			root = new DefaultTreeNode("Root", null);
			TreeNode nodoNivel0 = new DefaultTreeNode(optionCatalogueNodeBean, root);
			generateMenuOptionsUser(nodoNivel0,userInfo.getCurrentLanguage(),userInfo.getPraderaMenuAll());
		} else {
			root = null;
		}
	}
	
	private void generateMenuOptionsUser(TreeNode nodeFather,String language,PraderaMenu praderaMenu){
		for(PraderaSubMenu subMenuPradera : praderaMenu.getSubMenus()) {
			OptionCatalogNodeBean optionNode = new OptionCatalogNodeBean();
			optionNode.setName(getLabelLanguage(subMenuPradera.getName(), language));
			TreeNode nodeSubmenu = new DefaultTreeNode(optionNode,nodeFather);
			if (subMenuPradera.getSubMenus() != null && !subMenuPradera.getSubMenus().isEmpty()) {
				for(PraderaSubMenu subMenuPraderaChildren: subMenuPradera.getSubMenus()) {
					OptionCatalogNodeBean optionModule = new OptionCatalogNodeBean();
					optionModule.setName(getLabelLanguage(subMenuPraderaChildren.getName(), language));
					TreeNode nodeModule = new DefaultTreeNode(optionModule,nodeSubmenu);
					generateRecursiveMenu(nodeModule,language,subMenuPraderaChildren);
				}
			}
			for (PraderaMenuOption menuOption : subMenuPradera.getOptions()) {
				OptionCatalogNodeBean option = new OptionCatalogNodeBean();
				option.setName(getLabelLanguage(menuOption.getName(), language));
				option.setIdSystemOptionPk(menuOption.getIdMenuOptionPk());
				option.setInstanceType(InstanceType.OPTION.getCode());
				TreeNode nodeOption = new DefaultTreeNode(option,nodeSubmenu);
			}
		}
	}
	
	private void generateRecursiveMenu(TreeNode nodeFather,String language,PraderaSubMenu praderaSubMenu){
		//options
		for(PraderaMenuOption opcionChildren : praderaSubMenu.getOptions()) {
			if (!opcionChildren.getUrl().contains("eport.xhtml")){
				OptionCatalogNodeBean option = new OptionCatalogNodeBean();
				option.setName(getLabelLanguage(opcionChildren.getName(), language));
				option.setIdSystemOptionPk(opcionChildren.getIdMenuOptionPk());
				option.setInstanceType(InstanceType.OPTION.getCode());
				TreeNode nodeOption = new DefaultTreeNode(option,nodeFather);
			}
		}
		//submenus
		if (praderaSubMenu.getSubMenus() != null) {
			for(PraderaSubMenu menuChildrenPradera : praderaSubMenu.getSubMenus()) {
				OptionCatalogNodeBean optionModule = new OptionCatalogNodeBean();
				optionModule.setName(getLabelLanguage(menuChildrenPradera.getName(), language));
				TreeNode nodeModule = new DefaultTreeNode(optionModule,nodeFather);
				generateRecursiveMenu(nodeModule,language,menuChildrenPradera);
			}
		}
	}
	
	private String getLabelLanguage(Map<String,String> names, String language) {
		if (names.get(language) != null) {
			return names.get(language);
		} else {
			//TODO replace call Type Idioma when refactory IdiomaType to PraderaCommons
			return names.get("es");
		}
	}

	/**
	 * Gets the system id selected.
	 *
	 * @return the system id selected
	 */
	public Integer getSystemIdSelected() {
		return systemIdSelected;
	}

	/**
	 * Sets the system id selected.
	 *
	 * @param systemIdSelected the new system id selected
	 */
	public void setSystemIdSelected(Integer systemIdSelected) {
		this.systemIdSelected = systemIdSelected;
	}

	/**
	 * Gets the root.
	 *
	 * @return the root
	 */
	public TreeNode getRoot() {
		return root;
	}

	/**
	 * Sets the root.
	 *
	 * @param root the new root
	 */
	public void setRoot(TreeNode root) {
		this.root = root;
	}


}
