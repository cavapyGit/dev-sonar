package com.pradera.security.usermgmt.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.pradera.commons.security.model.type.LogoutMotiveType;
import com.pradera.commons.security.model.type.UserSessionStateType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.model.InstitutionsSecurity;
import com.pradera.security.model.Subsidiary;
import com.pradera.security.model.System;
import com.pradera.security.model.UserAccount;
import com.pradera.security.model.UserSession;
import com.pradera.security.model.type.SystemProfileType;
import com.pradera.security.systemmgmt.service.CrudSecurityServiceBean;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Class UserSessionServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/12/2012
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class UserSessionServiceBean extends CrudSecurityServiceBean{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1574152985526504265L;
	/**
	 * Kill all sessions by user service bean.
	 *
	 * @param userSession the user session
	 * @throws ServiceException the service exception
	 */
	public void killAllSessionsByUserServiceBean(UserSession userSession) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update UserSession US ");	
		sbQuery.append(" Set US.state = :statePrm, ");
		sbQuery.append(" US.finalDate = :finalDatePrm, ");
		sbQuery.append(" US.logoutMotiveFk = :logoutMotivePrm, ");
		sbQuery.append(" US.lastModifyUser = :lastModifyUserPrm, ");
		sbQuery.append(" US.lastModifyDate = :lastModifyDatePrm, US.lastModifyIp = :lastModifyIpPrm, ");
		sbQuery.append(" US.lastModifyPriv = :lastModifyPrivPrm ");
		sbQuery.append(" Where US.userAccount.idUserPk = :idUserPrm And US.state = :stateFilterPrm ");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("statePrm", UserSessionStateType.DISCONNECTED.getCode());
		query.setParameter("finalDatePrm", CommonsUtilities.currentDateTime());
		query.setParameter("logoutMotivePrm", LogoutMotiveType.KILLSESSION.getCode());
		query.setParameter("lastModifyUserPrm",userSession.getLastModifyUser());
		query.setParameter("lastModifyDatePrm",userSession.getLastModifyDate());
		query.setParameter("lastModifyIpPrm",userSession.getLastModifyIp());
		query.setParameter("lastModifyPrivPrm",userSession.getLastModifyPriv());
		query.setParameter("idUserPrm",userSession.getUserAccount().getIdUserPk());
		query.setParameter("stateFilterPrm", UserSessionStateType.CONNECTED.getCode());
		query.executeUpdate();
	}
	
	/**
	 * Gets the user session by ticked service bean.
	 *
	 * @param userSession the user session
	 * @return the user session by ticked service bean
	 */
	public UserSession getUserSessionByTickedServiceBean(UserSession userSession){
		try{
			StringBuilder sbQuery = new StringBuilder(200);
			sbQuery.append(" select us.idUserSessionPk,us.lastModifyIp,us.locale,us.ticket,  ").
			append(" us.system.idSystemPk,us.system.name,us.system.systemUrl, ").
			append(" us.userAccount.idUserPk,us.userAccount.loginUser,us.userAccount.fullName, ").
			append(" us.userAccount.idEmail,us.userAccount.state,us.userAccount.institutionTypeFk, ").
			append(" us.userAccount.idParticipantFk,us.userAccount.idIssuerFk,us.userAccount.regulatorAgent,us.userAccount.responsability, ").
			append(" us.userAccount.institutionsSecurity.idInstitutionSecurityPk,us.userAccount.institutionsSecurity.institutionName, ").
			append(" us.userAccount.subsidiary.idSubsidiaryPk  ").
			append(" from UserSession us where us.ticket = :ticket and us.state= :state ");
			TypedQuery<Object[]> query = em.createQuery(sbQuery.toString(), Object[].class);
			query.setParameter("ticket", userSession.getTicket());
			query.setParameter("state", UserSessionStateType.CONNECTED.getCode());
			List<Object[]> userData = query.getResultList();
			if(!userData.isEmpty()){
				Object[] row = userData.get(0);
				UserSession session = new UserSession();
				session.setIdUserSessionPk((Long)row[0]);
				session.setLastModifyIp(row[1]!=null?row[1].toString():"");
				session.setLocale(row[2]!=null?row[2].toString():"");
				session.setTicket(row[3].toString());
				//System
				System system = new System();
				system.setIdSystemPk((Integer)row[4]);
				system.setName(row[5].toString());
				system.setSystemUrl(row[6].toString());
				//userAccount
				UserAccount userAccount = new UserAccount();
				userAccount.setIdUserPk((Integer)row[7]);
				userAccount.setLoginUser(row[8].toString());
				userAccount.setFullName(row[9].toString());
				userAccount.setIdEmail(row[10].toString());
				userAccount.setState((Integer)row[11]);
				userAccount.setInstitutionTypeFk((Integer)row[12]);
				userAccount.setIdParticipantFk(row[13]!=null?(Long)row[13]:null);
				userAccount.setIdIssuerFk(row[14]!=null?row[14].toString():null);
				userAccount.setRegulatorAgent((Integer)row[15]);
				userAccount.setResponsability(row[16]!=null?(Integer)row[16]:null);
				InstitutionsSecurity institution = new InstitutionsSecurity();
				institution.setIdInstitutionSecurityPk((Integer)row[17]);
				institution.setInstitutionName(row[18]!=null?row[18].toString():"");
				Subsidiary objSubsidiary = new Subsidiary();
				objSubsidiary.setIdSubsidiaryPk(row[19]!=null?(Long)row[19]:null);
				if(Validations.validateIsNotNullAndNotEmpty(objSubsidiary.getIdSubsidiaryPk())){
					userAccount.setSubsidiary(objSubsidiary);
				} else {
					userAccount.setSubsidiary(null);
				}				
				//setting mapping
				session.setSystem(system);
				session.setUserAccount(userAccount);
				userAccount.setInstitutionsSecurity(institution);
				return session;
			} else {
				return null;
			}
//			
//			CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
//			CriteriaQuery<UserSession> criteriaQuery = criteriaBuilder.createQuery(UserSession.class);
//			Root<UserSession> userSessionRoot = criteriaQuery.from(UserSession.class);
//			List<Predicate> predicateParameters = new ArrayList<Predicate>();
//	
//			criteriaQuery.select(userSessionRoot);
//			
//			if(Validations.validateIsNotNullAndNotEmpty(userSession.getTicket())){
//				predicateParameters.add(criteriaBuilder.equal(userSessionRoot.get("ticket"),userSession.getTicket()));
//			}
//			if(Validations.validateIsNotNullAndNotEmpty(userSession.getTicket())){
//				predicateParameters.add(criteriaBuilder.equal(userSessionRoot.get("state"),UserSessionStateType.CONNECTED.getCode()));
//			}
//			
//			criteriaQuery.where(criteriaBuilder.and(predicateParameters.toArray(new Predicate[predicateParameters.size()])));
//			
//			TypedQuery<UserSession> criteriaUserSession = em.createQuery(criteriaQuery);
//			return criteriaUserSession.getSingleResult();		
		} catch(NoResultException ex){
			return null;
		} catch(NonUniqueResultException ex){
			return null;
		}
	}
	
	/**
	 * Close session service bean.
	 *
	 * @param userSession the user session
	 * @throws ServiceException the service exception
	 */
	public void closeSessionServiceBean(UserSession userSession) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update UserSession US ");	
		sbQuery.append(" Set US.state = :statePrm, ");
		sbQuery.append(" US.finalDate = :finalDatePrm, ");
		sbQuery.append(" US.logoutMotiveFk = :logoutMotivePrm, ");
		sbQuery.append(" US.lastModifyUser = :lastModifyUserPrm, ");
		sbQuery.append(" US.lastModifyDate = :lastModifyDatePrm, US.lastModifyIp = :lastModifyIpPrm, ");
		sbQuery.append(" US.lastModifyPriv = :lastModifyPrivPrm ");
		sbQuery.append(" Where US.idUserSessionPk = :idUserSessionPrm");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("statePrm", UserSessionStateType.DISCONNECTED.getCode());
		query.setParameter("finalDatePrm", CommonsUtilities.currentDateTime());
		query.setParameter("logoutMotivePrm", LogoutMotiveType.LOGOUTSESSION.getCode());
		query.setParameter("lastModifyUserPrm",userSession.getLastModifyUser());
		query.setParameter("lastModifyDatePrm",userSession.getLastModifyDate());
		query.setParameter("lastModifyIpPrm",userSession.getLastModifyIp());
		query.setParameter("lastModifyPrivPrm",userSession.getLastModifyPriv());
		query.setParameter("idUserSessionPrm",userSession.getIdUserSessionPk());
		query.executeUpdate();
	}
	
	/**
	 * CloseMultipleSessionServiceBean.
	 *
	 * @param userSession the user session
	 * @throws ServiceException the service exception
	 */
	public void closeMultipleSessionServiceBean(UserSession userSession) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update UserSession US ");	
		sbQuery.append(" Set US.state = :statePrm, ");
		sbQuery.append(" US.finalDate = :finalDatePrm, ");
		sbQuery.append(" US.logoutMotiveFk = :logoutMotivePrm, ");
		sbQuery.append(" US.lastModifyUser = :lastModifyUserPrm, ");
		sbQuery.append(" US.lastModifyDate = :lastModifyDatePrm, US.lastModifyIp = :lastModifyIpPrm ");
		sbQuery.append(" Where US.idUserSessionPk = :idUserSessionPrm");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("statePrm", UserSessionStateType.DISCONNECTED.getCode());
		query.setParameter("finalDatePrm", CommonsUtilities.currentDateTime());
		query.setParameter("logoutMotivePrm", userSession.getLogoutMotiveFk());
		query.setParameter("lastModifyUserPrm",userSession.getLastModifyUser());
		query.setParameter("lastModifyDatePrm",CommonsUtilities.currentDateTime());
		query.setParameter("lastModifyIpPrm",userSession.getLastModifyIp());
		query.setParameter("idUserSessionPrm",userSession.getIdUserSessionPk());
		query.executeUpdate();
	}
	

	public void closeMultipleSessionServiceBean(UserSession userSession, Long currentSession, Integer idUser) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update UserSession US ");	
		sbQuery.append(" Set US.state = :statePrm, ");
		sbQuery.append(" US.finalDate = :finalDatePrm, ");
		sbQuery.append(" US.logoutMotiveFk = :logoutMotivePrm, ");
		sbQuery.append(" US.lastModifyUser = :lastModifyUserPrm, ");
		sbQuery.append(" US.lastModifyDate = :lastModifyDatePrm, US.lastModifyIp = :lastModifyIpPrm ");
		sbQuery.append(" Where US.idUserSessionPk != :idUserSessionPrm ");
		sbQuery.append(" and US.userAccount.idUserPk=:ID_USER ");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("statePrm", UserSessionStateType.DISCONNECTED.getCode());
		query.setParameter("finalDatePrm", CommonsUtilities.currentDateTime());
		query.setParameter("logoutMotivePrm", userSession.getLogoutMotiveFk());
		query.setParameter("lastModifyUserPrm",userSession.getLastModifyUser());
		query.setParameter("lastModifyDatePrm",CommonsUtilities.currentDateTime());
		query.setParameter("lastModifyIpPrm",userSession.getLastModifyIp());
		query.setParameter("idUserSessionPrm",currentSession);
		query.setParameter("ID_USER",idUser);
		query.executeUpdate();
	}
	
	/**
	 * Update user account access.
	 *
	 * @param userAccountId the user account id
	 * @param lastAccess the last access
	 * @param changePassword the change password
	 * @param requestPassword the request password
	 * @throws ServiceException the service exception
	 */
	public void updateUserAccountAccess(Integer userAccountId,Date lastAccess,Date changePassword,Integer requestPassword) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update UserAccount UC set UC.lastModifyDate =:lastDate ");
		if(lastAccess!=null){
			sbQuery.append(",UC.lastAccessDate = :lastAccess ");
		}
		if(changePassword!=null){
			sbQuery.append(" ,UC.lastPasswordChange = :lastChangePassword ");
		}
		if(requestPassword!=null){
			sbQuery.append(" ,UC.countRequestPassword = :countRequest ");
		}
		sbQuery.append(" Where UC.idUserPk = :idUserAccount ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("lastDate", CommonsUtilities.currentDateTime());
		query.setParameter("idUserAccount", userAccountId);
		if(lastAccess!=null){
			query.setParameter("lastAccess", lastAccess);
		}
		if(changePassword!=null){
			query.setParameter("lastChangePassword", changePassword);
		}
		if(requestPassword!=null){
			query.setParameter("countRequest", requestPassword);
		}
		query.executeUpdate();
	}
	
	public Subsidiary getSubsidiary(Long idSubsidiary) throws ServiceException{
		return find(Subsidiary.class, idSubsidiary);
	}
	
	/**
	 * User is operator.
	 *
	 * @param typeSignature the Type Signature
	 * @return the Exits active signature
	 * @throws ServiceException the service exception
	 */
	
	public boolean getUserIsOperator(Integer idUserPk)  throws ServiceException {
		try {			
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" select count(*) 							");			
			sbQuery.append(" from UserProfile up 						");
			sbQuery.append(" join up.userAccount ua 					");
			sbQuery.append(" join up.systemProfile sp 					");
			sbQuery.append(" join sp.system syst 						");
			sbQuery.append(" where syst.idSystemPk = 2					");	
			sbQuery.append(" and ua.idUserPk = :idUserPk				");
			sbQuery.append(" and sp.profileType = :profileType			");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idUserPk", idUserPk);
			query.setParameter("profileType", SystemProfileType.OPERATOR.getCode());
			if(Validations.validateIsNotNullAndNotEmpty(query.getSingleResult())){
				if(new Integer(query.getSingleResult().toString()) > 0){
					return true;
				} else {
					return false;
				}	
			}			
		} catch (NoResultException e) {
			return false;
		}
		return false;
	}
	
}