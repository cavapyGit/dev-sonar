package com.pradera.security.usermgmt.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.security.model.type.InstitutionStateType;
import com.pradera.commons.security.model.type.UserAccountStateType;
import com.pradera.commons.sessionuser.UserFilterTO;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.model.InstitutionsSecurity;
import com.pradera.security.model.UserAccount;
import com.pradera.security.model.type.SystemProfileType;
import com.pradera.security.model.type.SystemType;
import com.pradera.security.systemmgmt.service.CrudSecurityServiceBean;
import com.pradera.security.systemmgmt.view.filters.InstitutionSecurityFilter;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class UserIntegrationServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/02/2013
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class UserIntegrationServiceBean extends CrudSecurityServiceBean{
	
	
	private static final long serialVersionUID = -955116430767900941L;

	/**
	 * Gets the institutions security service bean.
	 *
	 * @return the institutions security service bean
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<InstitutionsSecurity> getInstitutionsSecurityServiceBean() throws ServiceException{
		StringBuffer sql = new StringBuffer();
		sql.append( "Select i from InstitutionsSecurity i order by i.idInstitutionSecurityPk");
		Query query = em.createQuery(sql.toString());
		query.setHint("org.hibernate.cacheable", true);
		return (List<InstitutionsSecurity>)query.getResultList();
	}
	
	/**
	 * Gets the institutions security with schedule service bean.
	 *
	 * @param filtro the filtro
	 * @return the institutions security with schedule service bean
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<InstitutionsSecurity> getInstitutionsSecurityWithScheduleServiceBean(InstitutionSecurityFilter filtro) throws ServiceException{
		StringBuffer sql = new StringBuffer();
		sql.append( "SELECT i FROM InstitutionsSecurity i LEFT JOIN FETCH  i.schedules");
		Query query = em.createQuery(sql.toString());
		query.setHint("org.hibernate.cacheable", true);
		return (List<InstitutionsSecurity>)query.getResultList();
	}
	
	/**
	 * Gets the institution security service bean.
	 *
	 * @param filter the filter
	 * @return the institution security service bean
	 * @throws ServiceException the service exception
	 */
	public InstitutionsSecurity getInstitutionSecurityServiceBean(InstitutionSecurityFilter filter) throws ServiceException{
		List<InstitutionsSecurity> institutionSecuritiesList = this.getInstitutionsSecurityWithScheduleServiceBean(filter);
		for(InstitutionsSecurity institutionSecurity: institutionSecuritiesList){
			if(filter.getIdInstitutionSecurityPK().equals(institutionSecurity.getIdInstitutionSecurityPk())){
				return institutionSecurity;
			}
		}
		return null;
	}
	
	/**
	 * Gets the institutions by filter service bean.
	 *
	 * @param filter the filter
	 * @return the institutions by filter service bean
	 * @throws ServiceException the service exception
	 */
	public List<InstitutionsSecurity> getInstitutionsByFilterServiceBean(InstitutionSecurityFilter filter) throws ServiceException{
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<InstitutionsSecurity> criteriaQuery = criteriaBuilder.createQuery(InstitutionsSecurity.class);
		Root<InstitutionsSecurity> institutionsSecurityRoot = criteriaQuery.from(InstitutionsSecurity.class);
		List<Predicate> criteriaParameters = new ArrayList<Predicate>();
		
		criteriaQuery.select(institutionsSecurityRoot);

		if(Validations.validateIsNotNullAndNotEmpty( filter.getIdInstitutionTypeFk() )){
			criteriaParameters.add(criteriaBuilder.equal(institutionsSecurityRoot.get("idInstitutionTypeFk"),filter.getIdInstitutionTypeFk()));
		}
		if(Validations.validateIsNull( filter.getIdState() )) {
			criteriaParameters.add(criteriaBuilder.equal(institutionsSecurityRoot.get("state"), InstitutionStateType.REGISTERED.getCode()));
		}
		
		if (criteriaParameters.size() == 0) {
	        throw new RuntimeException("no criteria");
	    } else if (criteriaParameters.size() == 1) {
	        criteriaQuery.where(criteriaParameters.get(0));
	    } else {
	        criteriaQuery.where(criteriaBuilder.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()])));
	    }
		
		TypedQuery<InstitutionsSecurity> institutionsSecurityCriteria = em.createQuery(criteriaQuery);
		institutionsSecurityCriteria.setHint("org.hibernate.cacheable", true);
		return institutionsSecurityCriteria.getResultList();
	}
	
	/**
	 * Gets the users to remote modules.
	 *
	 * @param userFilter the user filter
	 * @return the users remote
	 */
	public List<UserAccount> getUsersRemote(UserFilterTO userFilter){
		StringBuilder sbQuery = new StringBuilder(100);
		List<UserAccount> users = null;
		sbQuery.append("select ua from UserAccount ua ");
		if(userFilter.getProfileSystem() != null){
			sbQuery.append(" join ua.userProfiles pro ");
		}
		sbQuery.append(" where 1 = 1 ");
		//if(userFilter.getState() !=null) {
			sbQuery.append(" and ua.state = :stateUser ");
		//}
				
		if(userFilter.getResponsibility() != null){
			sbQuery.append(" and ua.responsability = :responsabilityUser ");
		}
		if(userFilter.getInstitutionType() != null) {		
			sbQuery.append(" and ua.institutionTypeFk = :institutionUser ");
			if(userFilter.getEntityCode() != null){
				InstitutionType institution = InstitutionType.get(userFilter.getInstitutionType());
				switch (institution) {
				case AFP:
				case PARTICIPANT:
					sbQuery.append(" and ua.idParticipantFk = :entityUser ");
					break;					
				case ISSUER:
					sbQuery.append(" and ua.idIssuerFk = :entityUser ");
					break;				
				case PARTICIPANT_INVESTOR:
					sbQuery.append(" and ua.idParticipantFk = :entityUser ");
					break;				
				case ISSUER_DPF:
					sbQuery.append(" and ua.idIssuerFk = :entityUser ");
					break;					
				default:					
					break;
				}
			}
		}
		if(StringUtils.isNotBlank(userFilter.getUserName())){
			sbQuery.append(" and ua.loginUser = :userName ");
		}
		if(Validations.validateIsNotNullAndPositive(userFilter.getIndExtInterface())){
			sbQuery.append(" and ua.indExtInterface = :extInterface ");
		}
		if(userFilter.getProfileSystem() != null){
			sbQuery.append(" and pro.systemProfile.idSystemProfilePk = :profileSystem ");
		}
		TypedQuery<UserAccount> query = em.createQuery(sbQuery.toString(), UserAccount.class);
		if(userFilter.getState() == null){
			query.setParameter("stateUser", UserAccountStateType.CONFIRMED.getCode());
		} else {
			query.setParameter("stateUser", userFilter.getState());
		}
		if(userFilter.getResponsibility() != null){
			query.setParameter("responsabilityUser", userFilter.getResponsibility());
		}
		if(userFilter.getInstitutionType() != null) {	
			query.setParameter("institutionUser", userFilter.getInstitutionType());
			if(userFilter.getEntityCode() != null){
				InstitutionType institution = InstitutionType.get(userFilter.getInstitutionType());
				switch (institution) {
				case AFP:
				case PARTICIPANT:
					query.setParameter("entityUser",  new BigDecimal(userFilter.getEntityCode().toString()).longValue());
					break;					
				case ISSUER:
					query.setParameter("entityUser", userFilter.getEntityCode().toString());
					break;
				case PARTICIPANT_INVESTOR:
					query.setParameter("entityUser",  new BigDecimal(userFilter.getEntityCode().toString()).longValue());
					break;
				case ISSUER_DPF:
					query.setParameter("entityUser", userFilter.getEntityCode().toString());
					break;
				default:					
					break;
				}
			}
		}if(StringUtils.isNotBlank(userFilter.getUserName())){
			query.setParameter("userName", userFilter.getUserName());
		}
		if(Validations.validateIsNotNullAndPositive(userFilter.getIndExtInterface())){
			query.setParameter("extInterface", userFilter.getIndExtInterface());
		}
		if(userFilter.getProfileSystem() != null){
			query.setParameter("profileSystem", userFilter.getProfileSystem());
		}
		query.setHint("org.hibernate.cacheable", true);
		users = query.getResultList();
		return users;
	}
	
	/**
	 * Gets the users to remote modules for Billing.
	 *
	 * @param userFilter the user filter
	 * @return the users remote
	 */
	@SuppressWarnings("unchecked")
	public List<UserAccount> getUsersRemoteForBilling(UserFilterTO userFilter){
		  StringBuilder sbQuery = new StringBuilder(100);
		  List<UserAccount> users = null;
		  /**for default users in state confirmed*/
		  sbQuery.append(" SELECT ");
		  sbQuery.append("  	UA.ID_ISSUER_FK,	");
		  sbQuery.append("  	UA.ID_PARTICIPANT_FK,	");
		  sbQuery.append("  	UA.INSTITUTION_TYPE_FK,	");
		  sbQuery.append("  	COUNT(*)	");
		  sbQuery.append(" FROM USER_ACCOUNT UA	");
		  sbQuery.append(" WHERE ");
		  sbQuery.append(" 		ua.STATE=:stateUser ");
		  
		  if(Validations.validateIsNotNullAndNotEmpty(userFilter.getRegisterDate())){
			  
			  sbQuery.append(" AND ");
			  sbQuery.append("  	TO_NUMBER( TO_CHAR(UA.REGISTRY_DATE,'yyyy') || TO_CHAR( UA.REGISTRY_DATE ,'MM') ) ");			  
			  sbQuery.append("  	<= 	");
			  sbQuery.append("  	TO_NUMBER( TO_CHAR(:dateCalculationPeriod,'yyyy') || ");
			  sbQuery.append("  	TO_CHAR(:dateCalculationPeriod ,'MM') ) ");
		  
		  }
		  
		  if(userFilter.getInstitutionType() != null) {
		   sbQuery.append(" and ua.INSTITUTION_TYPE_FK = :institutionUser ");
		   if(userFilter.getEntityCode() != null){
			    InstitutionType institution = InstitutionType.get(userFilter.getInstitutionType());
			    switch (institution) {
				    case AFP:
				    case PARTICIPANT:
				    	sbQuery.append(" and ua.ID_PARTICIPANT_FK = :entityUser ");
				    break;
				     
				    case ISSUER:
				    	sbQuery.append(" and ua.ID_ISSUER_FK = :entityUser ");
				    break;
				    
				    //others institutions
				    default:
				    break;
			    }
		   }
		  }
		  if(Validations.validateIsNotNullAndPositive(userFilter.getIndExtInterface())){
		   sbQuery.append(" and ua.IND_EXT_INTERFACE = :extInterface ");
		  }
		  
		  sbQuery.append(" GROUP BY ");
		  sbQuery.append("  	UA.ID_ISSUER_FK, ");
		  sbQuery.append("  	UA.ID_PARTICIPANT_FK, ");
		  sbQuery.append("  	UA.INSTITUTION_TYPE_FK ");		
		  sbQuery.append(" ORDER BY ");
		  sbQuery.append("  	ua.ID_ISSUER_FK, ");
		  sbQuery.append("  	ua.ID_PARTICIPANT_FK ");
		  
		  Query query = em.createNativeQuery(sbQuery.toString());
		  
		  
		   if(Validations.validateIsNotNullAndNotEmpty(userFilter.getRegisterDate())){
		    
			   query.setParameter("dateCalculationPeriod",CommonsUtilities.convertStringtoDate(userFilter.getRegisterDate()) );
		   }
		  
		  
		  if(userFilter.getState() == null){
			  query.setParameter("stateUser", UserAccountStateType.CONFIRMED.getCode());
		  } else {
			  query.setParameter("stateUser", userFilter.getState());
		  }
		  
		  if(userFilter.getInstitutionType() != null) {
		   query.setParameter("institutionUser", userFilter.getInstitutionType());
		   if(userFilter.getEntityCode() != null){
			    InstitutionType institution = InstitutionType.get(userFilter.getInstitutionType());
			    switch (institution) {
				    case AFP:
				    case PARTICIPANT:
				    	query.setParameter("entityUser",  new BigDecimal(userFilter.getEntityCode().toString()).longValue());
				    break;
				     
				    case ISSUER:
				    	query.setParameter("entityUser", userFilter.getEntityCode().toString());
				    break;
				    
				    //others institutions
				    default:
				    break;
			    }
		   }
		  }
		  if(Validations.validateIsNotNullAndPositive(userFilter.getIndExtInterface())){
		   query.setParameter("extInterface", userFilter.getIndExtInterface());
		  }
		   List<Object[]>  objectList = query.getResultList();
		   users=new ArrayList<UserAccount>();
		   for(int i=0;i<objectList.size();++i){
			    Object[] sResults = (Object[])objectList.get(i);
			    UserAccount user=new UserAccount();
			    user.setIdIssuerFk(sResults[0]==null?"":sResults[0].toString());
			    user.setIdParticipantFk(sResults[1]==null?0L:Long.parseLong(sResults[1].toString()));
			    user.setInstitutionTypeFk(sResults[2]==null?0:Integer.parseInt(sResults[2].toString()));
			    user.setUserCount(sResults[3]==null?0:Integer.parseInt(sResults[3].toString()));
			    users.add(user);
		   }
		  return users;
		 }
	
	/**
	 * Gets the users to remote modules.
	 *
	 * @param userFilter the user filter
	 * @return the users remote
	 */
	@SuppressWarnings("unchecked")
	public List<UserAccount> getUsersSupervisor(UserFilterTO userFilter){
		StringBuilder sbQuery = new StringBuilder();
		List<UserAccount> users = null;		
		sbQuery.append(" select ua.idUserPk, ua.loginUser, ua.idEmail							");
		sbQuery.append(" from UserProfile up 													");
		sbQuery.append(" join up.userAccount ua 												");
		sbQuery.append(" join up.systemProfile sp 												");
		sbQuery.append(" join sp.system syst 													");
		sbQuery.append(" join ua.institutionsSecurity isec 										");		
		if(Validations.validateIsNotNullAndNotEmpty(userFilter.getDepartment())){
			sbQuery.append(" join ua.subsidiary sub				");			
		}						
		sbQuery.append(" where 1=1	and syst.idSystemPk = 2	and up.state = 1 and sp.state = 3	");	
		if(Validations.validateIsNotNullAndNotEmpty(userFilter.getDepartment())){
			sbQuery.append(" and sub.departament = :departament ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(userFilter.getInstitutionSecurity())){
			sbQuery.append(" and isec.idInstitutionSecurityPk = :institutionSecurity ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(userFilter.getInstitutionType())){
			sbQuery.append(" and ua.institutionTypeFk = :institutionType ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(userFilter.getEntityCode())){
			sbQuery.append(" and ua.idParticipantFk = :entityUser ");
		}
		sbQuery.append(" and sp.profileType = :profileType ");
		Query query = em.createQuery(sbQuery.toString());
		if(Validations.validateIsNotNullAndNotEmpty(userFilter.getDepartment())){
			query.setParameter("departament", userFilter.getDepartment());
		}
		if(Validations.validateIsNotNullAndNotEmpty(userFilter.getInstitutionSecurity())){
			query.setParameter("institutionSecurity", userFilter.getInstitutionSecurity());
		}
		if(Validations.validateIsNotNullAndNotEmpty(userFilter.getInstitutionType())){
			query.setParameter("institutionType", userFilter.getInstitutionType());
		}
		if(Validations.validateIsNotNullAndNotEmpty(userFilter.getEntityCode())){
			query.setParameter("entityUser", new BigDecimal(userFilter.getEntityCode().toString()).longValue());
		}
		query.setParameter("profileType", SystemProfileType.SUPERVISOR.getCode());
		query.setHint("org.hibernate.cacheable", true);				
		List<Object> objectList = query.getResultList();
		users = new ArrayList<UserAccount>();
		for(int i=0; i<objectList.size(); ++i){
		    Object[] sResults = (Object[]) objectList.get(i);
		    UserAccount user = new UserAccount();
		    user.setIdUserPk(sResults[0]==null?0:Integer.parseInt(sResults[0].toString()));
		    user.setLoginUser(sResults[1]==null?"":sResults[1].toString());
		    user.setIdEmail(sResults[2]==null?"":sResults[2].toString());
		    users.add(user);
		}		
		return users;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserAccount> getUsersOperator(UserFilterTO userFilter){
		StringBuilder sbQuery = new StringBuilder();
		List<UserAccount> users = null;		
		sbQuery.append(" select ua.idUserPk, ua.loginUser, ua.idEmail							");
		sbQuery.append(" from UserProfile up 													");
		sbQuery.append(" join up.userAccount ua 												");
		sbQuery.append(" join up.systemProfile sp 												");
		sbQuery.append(" join sp.system syst 													");
		sbQuery.append(" join ua.institutionsSecurity isec 										");		
		if(Validations.validateIsNotNullAndNotEmpty(userFilter.getDepartment())){
			sbQuery.append(" join ua.subsidiary sub				");			
		}						
		sbQuery.append(" where 1=1	and syst.idSystemPk = 2	and up.state = 1 and sp.state = 3	");	
		if(Validations.validateIsNotNullAndNotEmpty(userFilter.getDepartment())){
			sbQuery.append(" and sub.departament = :departament ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(userFilter.getInstitutionSecurity())){
			sbQuery.append(" and isec.idInstitutionSecurityPk = :institutionSecurity ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(userFilter.getInstitutionType())){
			sbQuery.append(" and ua.institutionTypeFk = :institutionType ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(userFilter.getEntityCode())){
			sbQuery.append(" and ua.idParticipantFk = :entityUser ");
		}
		sbQuery.append(" and sp.profileType = :profileType ");
		Query query = em.createQuery(sbQuery.toString());
		if(Validations.validateIsNotNullAndNotEmpty(userFilter.getDepartment())){
			query.setParameter("departament", userFilter.getDepartment());
		}
		if(Validations.validateIsNotNullAndNotEmpty(userFilter.getInstitutionSecurity())){
			query.setParameter("institutionSecurity", userFilter.getInstitutionSecurity());
		}
		if(Validations.validateIsNotNullAndNotEmpty(userFilter.getInstitutionType())){
			query.setParameter("institutionType", userFilter.getInstitutionType());
		}
		if(Validations.validateIsNotNullAndNotEmpty(userFilter.getEntityCode())){
			query.setParameter("entityUser", new BigDecimal(userFilter.getEntityCode().toString()).longValue());
		}
		query.setParameter("profileType", SystemProfileType.OPERATOR.getCode());
		query.setHint("org.hibernate.cacheable", true);				
		List<Object> objectList = query.getResultList();
		users = new ArrayList<UserAccount>();
		for(int i=0; i<objectList.size(); ++i){
		    Object[] sResults = (Object[]) objectList.get(i);
		    UserAccount user = new UserAccount();
		    user.setIdUserPk(sResults[0]==null?0:Integer.parseInt(sResults[0].toString()));
		    user.setLoginUser(sResults[1]==null?"":sResults[1].toString());
		    user.setIdEmail(sResults[2]==null?"":sResults[2].toString());
		    users.add(user);
		}		
		return users;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserAccount> getUsersSupervisoroperator(UserFilterTO userFilter){
		StringBuilder sbQuery = new StringBuilder();
		List<UserAccount> users = null;		
		sbQuery.append(" select ua.idUserPk, ua.loginUser, ua.idEmail							");
		sbQuery.append(" from UserProfile up 													");
		sbQuery.append(" join up.userAccount ua 												");
		sbQuery.append(" join up.systemProfile sp 												");
		sbQuery.append(" join sp.system syst 													");
		sbQuery.append(" join ua.institutionsSecurity isec 										");		
		if(Validations.validateIsNotNullAndNotEmpty(userFilter.getDepartment())){
			sbQuery.append(" join ua.subsidiary sub				");			
		}						
		sbQuery.append(" where 1=1	and syst.idSystemPk = :idSystemPk and up.state = 1 and sp.state = 3	");	
		if(Validations.validateIsNotNullAndNotEmpty(userFilter.getDepartment())){
			sbQuery.append(" and sub.departament = :departament ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(userFilter.getInstitutionSecurity())){
			sbQuery.append(" and isec.idInstitutionSecurityPk = :institutionSecurity ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(userFilter.getInstitutionType())){
			sbQuery.append(" and ua.institutionTypeFk = :institutionType ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(userFilter.getEntityCode())){
			sbQuery.append(" and ua.idParticipantFk = :entityUser ");
		}
		
		sbQuery.append(" and sp.profileType in (:profileType) ");
		
		Query query = em.createQuery(sbQuery.toString());
		if(Validations.validateIsNotNullAndNotEmpty(userFilter.getDepartment())){
			query.setParameter("departament", userFilter.getDepartment());
		}
		if(Validations.validateIsNotNullAndNotEmpty(userFilter.getInstitutionSecurity())){
			query.setParameter("institutionSecurity", userFilter.getInstitutionSecurity());
		}
		if(Validations.validateIsNotNullAndNotEmpty(userFilter.getInstitutionType())){
			query.setParameter("institutionType", userFilter.getInstitutionType());
		}
		if(Validations.validateIsNotNullAndNotEmpty(userFilter.getEntityCode())){
			query.setParameter("entityUser", new BigDecimal(userFilter.getEntityCode().toString()).longValue());
		}
		
		List<Integer> profileType = new ArrayList<Integer>();
		profileType.add(SystemProfileType.OPERATOR.getCode());
		profileType.add(SystemProfileType.SUPERVISOR.getCode());
		query.setParameter("profileType", profileType);
		
		query.setParameter("idSystemPk", SystemType.CSDCORE.getCode());
		
		
		query.setHint("org.hibernate.cacheable", true);				
		List<Object> objectList = query.getResultList();
		users = new ArrayList<UserAccount>();
		for(int i=0; i<objectList.size(); ++i){
		    Object[] sResults = (Object[]) objectList.get(i);
		    UserAccount user = new UserAccount();
		    user.setIdUserPk(sResults[0]==null?0:Integer.parseInt(sResults[0].toString()));
		    user.setLoginUser(sResults[1]==null?"":sResults[1].toString());
		    user.setIdEmail(sResults[2]==null?"":sResults[2].toString());
		    users.add(user);
		}		
		return users;
	}
	
	/* inicio - parche opcion del menu 17/01/2017 */
	@SuppressWarnings("unchecked")
	public List<HashMap<String,Object>> getOptionsWithProfile(Integer profileFilter){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select DISTINCT (so.idSystemOptionPk), so.optionType, s.systemUrl||so.urlOption, cl.catalogueName, so.state ");
		sbQuery.append(" from SystemOption so 													");
		sbQuery.append(" join so.system s 														");
		sbQuery.append(" join so.optionPrivileges op 											");
		sbQuery.append(" join so.catalogueLanguages cl, 										");
		sbQuery.append(" ProfilePrivilege pp, 													");
		sbQuery.append(" SystemProfile sp														");	
		sbQuery.append(" WHERE	s.mnemonic = :mnemonic	AND									");		
		sbQuery.append(" (  																	");	
		sbQuery.append("   (so.optionType = :optionOne ) OR 									");	
		sbQuery.append("   (so.optionType = :optionTwo   AND  									");	
		sbQuery.append(" 	pp.optionPrivilege.idOptionPrivilegePk = op.idOptionPrivilegePk AND ");	
		sbQuery.append(" 	pp.systemProfile.idSystemProfilePk = :profileFilter )				");	
		sbQuery.append("  ) 																	");	
		sbQuery.append(" order by so.idSystemOptionPk, so.optionType asc 							");	
		
		Query query = em.createQuery(sbQuery.toString());
		
		query.setParameter("optionOne", new Integer(1));
		query.setParameter("optionTwo", new Integer(2));
		
		if(Validations.validateIsNotNullAndNotEmpty(profileFilter)){
			query.setParameter("profileFilter", profileFilter);
		}else{
			query.setParameter("profileFilter", new Integer(0));
		}
		//Modificacion del nemonico del datamart 
		query.setParameter("mnemonic","DTM");
		
		query.setHint("org.hibernate.cacheable", true);		
		
		List<Object> objectList = query.getResultList();
		
		
		List<HashMap<String,Object>> listMap = new ArrayList<HashMap<String,Object>>();
		
		for(int i=0; i<objectList.size(); ++i){
		    Object[] sResults = (Object[]) objectList.get(i);
		    
		    HashMap<String, Object> mapObject = new HashMap<String,Object>();

		    mapObject.put("idSystemOptionPk",sResults[0]==null?0:Integer.parseInt(sResults[0].toString()));
		    mapObject.put("optionType",sResults[1]==null?0:Integer.parseInt(sResults[1].toString()));
		    mapObject.put("urlOption",sResults[2]==null?"":sResults[2].toString());
		    mapObject.put("catalogueName",sResults[3]==null?"":sResults[3].toString());
		    mapObject.put("state",sResults[4]==null?"":sResults[4].toString());
		    
		    listMap.add(mapObject);
		}		
		return listMap;
	}
	/* fin - parche opcion del menu 17/01/2017 */
}