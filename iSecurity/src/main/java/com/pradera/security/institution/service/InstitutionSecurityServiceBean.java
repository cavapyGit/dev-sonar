package com.pradera.security.institution.service;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.Query;
import javax.ws.rs.core.Response.Status;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.security.model.type.InstitutionStateType;
import com.pradera.commons.services.remote.security.to.InstitutionTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.CustomWebServiceException;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.security.model.InstitutionsSecurity;
import com.pradera.security.systemmgmt.service.CrudSecurityServiceBean;
import com.pradera.security.systemmgmt.view.filters.InstitutionSecurityFilter;

@Stateless
@Interceptors(ContextHolderInterceptor.class)
public class InstitutionSecurityServiceBean extends CrudSecurityServiceBean{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8607792274566127763L;

	public void wsCreateInstituionSecurityService(InstitutionTO to) throws CustomWebServiceException {
		InstitutionSecurityFilter filter = new InstitutionSecurityFilter(to);
		filter.setDescription(null);
		filter.setMnemonic(null);
		
		List<InstitutionsSecurity> lstExistingInstitutions = filterInstitutionsSecurity(filter);
		if(Validations.validateListIsNotNullAndNotEmpty(lstExistingInstitutions)) {
			throw new CustomWebServiceException(ErrorServiceType.ERROR_INSTITUTION_SECURITY_ALREADY_EXISTS, null, Status.CONFLICT.getStatusCode());
		}
		
		InstitutionsSecurity ent = new InstitutionsSecurity();
		ent.setAudit(to.getLoggerUser());
		ent.setIdInstitutionTypeFk(to.getType());
		ent.setIdIssuer(to.getIssuerCode());
		ent.setIdParticipante(to.getParticipantCode());
		ent.setInstitutionName(to.getName());
		ent.setRegistryDate(new Date());
		ent.setRegistryUser(to.getLoggerUser().getUserName());
		ent.setMnemonicInstitution(to.getMnemonic());
		ent.setState(InstitutionStateType.REGISTERED.getCode());
		this.create(ent);
	}
	
	@SuppressWarnings("unchecked")
	public List<InstitutionsSecurity> filterInstitutionsSecurity(InstitutionSecurityFilter filter){
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT inse FROM InstitutionsSecurity inse");
		sb.append(" WHERE 1 = 1");
		if(Validations.validateIsNotNull(filter.getIdInstitutionSecurityPK())) {
			sb.append(" AND inse.idInstitutionSecurityPk = :idInstitutionSecurityPk");
		}
		if(Validations.validateIsNotNull(filter.getIdInstitutionTypeFk())) {
			sb.append(" AND inse.idInstitutionTypeFk = :idInstitutionTypeFk");
		}
		if(Validations.validateIsNotNull(filter.getDescription())) {
			sb.append(" AND inse.institutionName = :institutionName");
		}
		if(Validations.validateIsNotNull(filter.getMnemonic())) {
			sb.append(" AND inse.mnemonicInstitution = :mnemonicInstitution");
		}
		if(Validations.validateIsNotNull(filter.getIdState())) {
			sb.append(" AND inse.state = :state");
		}
		if(Validations.validateIsNotNull(filter.getIssuerCode())) {
			sb.append(" AND inse.idIssuer = :idIssuer");
		}
		if(Validations.validateIsNotNull(filter.getParticipantCode())) {
			sb.append(" AND inse.idParticipante = :idParticipante");
		}
		
		Query q = em.createQuery(sb.toString());
		
		if(Validations.validateIsNotNull(filter.getIdInstitutionSecurityPK())) {
			q.setParameter("idInstitutionSecurityPk", filter.getIdInstitutionSecurityPK());
		}
		if(Validations.validateIsNotNull(filter.getIdInstitutionTypeFk())) {
			q.setParameter("idInstitutionTypeFk", filter.getIdInstitutionTypeFk());
		}
		if(Validations.validateIsNotNull(filter.getDescription())) {
			q.setParameter("institutionName", filter.getDescription());
		}
		if(Validations.validateIsNotNull(filter.getMnemonic())) {
			q.setParameter("mnemonicInstitution", filter.getMnemonic());
		}
		if(Validations.validateIsNotNull(filter.getIdState())) {
			q.setParameter("state", filter.getIdState());
		}
		if(Validations.validateIsNotNull(filter.getIssuerCode())) {
			q.setParameter("idIssuer", filter.getIssuerCode());
		}
		if(Validations.validateIsNotNull(filter.getParticipantCode())) {
			q.setParameter("idParticipante", filter.getParticipantCode());
		}
		
		return (List<InstitutionsSecurity>) q.getResultList();
	}
	
	public void wsUpdateInstituionSecurityService(InstitutionTO to) throws CustomWebServiceException, ServiceException {
		InstitutionSecurityFilter filter = new InstitutionSecurityFilter(to);
		filter.setDescription(null);
		filter.setMnemonic(null);
		filter.setIdState(null);
		
		List<InstitutionsSecurity> lstExistingInstitutions = filterInstitutionsSecurity(filter);
		if(to.getState().equals(InstitutionStateType.REGISTERED.getCode()) && Validations.validateListIsNullOrEmpty(lstExistingInstitutions)) {
			InstitutionsSecurity ent = new InstitutionsSecurity();
			ent.setAudit(to.getLoggerUser());
			ent.setIdInstitutionTypeFk(to.getType());
			ent.setIdIssuer(to.getIssuerCode());
			ent.setIdParticipante(to.getParticipantCode());
			ent.setInstitutionName(to.getName());
			ent.setRegistryDate(new Date());
			ent.setRegistryUser(to.getLoggerUser().getUserName());
			ent.setMnemonicInstitution(to.getMnemonic());
			ent.setState(InstitutionStateType.REGISTERED.getCode());
			this.create(ent);
			return;
		} else if(Validations.validateListIsNullOrEmpty(lstExistingInstitutions)) {
			return;
		} else if (lstExistingInstitutions.size() > 1) {
			throw new CustomWebServiceException(ErrorServiceType.INSTITUTION_SECURITY_MORE_THAN_ONE_FOUND, null, Status.NOT_FOUND.getStatusCode());
		} 
		
		InstitutionsSecurity ent = lstExistingInstitutions.get(0);
		
		ent.setAudit(to.getLoggerUser());
		ent.setInstitutionName(to.getName());
		ent.setMnemonicInstitution(to.getMnemonic());
		ent.setState(to.getState());
		this.update(ent);
	}
}
