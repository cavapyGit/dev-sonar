package com.pradera.security.ppolicy.to;

import java.io.Serializable;

public class PasswordPolicyTO implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean pwdAllowUserChange;
	private long pwdExpireWarning;
	private long pwdFailureCountInterval;
	private int pwdGraceAuthNLimit;
	private int pwdInHistory;
	private boolean pwdLockout;
	private long pwdLockoutDuration;
	private long pwdMaxAge;
	private int pwdMaxFailure;
	private long pwdMinAge;
	private int pwdMinLength;
	private boolean pwdMustChange;
	public boolean isPwdAllowUserChange() {
		return pwdAllowUserChange;
	}
	public void setPwdAllowUserChange(boolean pwdAllowUserChange) {
		this.pwdAllowUserChange = pwdAllowUserChange;
	}
	public long getPwdExpireWarning() {
		return pwdExpireWarning;
	}
	public void setPwdExpireWarning(long pwdExpireWarning) {
		this.pwdExpireWarning = pwdExpireWarning;
	}
	public long getPwdFailureCountInterval() {
		return pwdFailureCountInterval;
	}
	public void setPwdFailureCountInterval(long pwdFailureCountInterval) {
		this.pwdFailureCountInterval = pwdFailureCountInterval;
	}
	public int getPwdGraceAuthNLimit() {
		return pwdGraceAuthNLimit;
	}
	public void setPwdGraceAuthNLimit(int pwdGraceAuthNLimit) {
		this.pwdGraceAuthNLimit = pwdGraceAuthNLimit;
	}
	public int getPwdInHistory() {
		return pwdInHistory;
	}
	public void setPwdInHistory(int pwdInHistory) {
		this.pwdInHistory = pwdInHistory;
	}
	public boolean isPwdLockout() {
		return pwdLockout;
	}
	public void setPwdLockout(boolean pwdLockout) {
		this.pwdLockout = pwdLockout;
	}
	public long getPwdLockoutDuration() {
		return pwdLockoutDuration;
	}
	public void setPwdLockoutDuration(long pwdLockoutDuration) {
		this.pwdLockoutDuration = pwdLockoutDuration;
	}
	public long getPwdMaxAge() {
		return pwdMaxAge;
	}
	public void setPwdMaxAge(long pwdMaxAge) {
		this.pwdMaxAge = pwdMaxAge;
	}
	public int getPwdMaxFailure() {
		return pwdMaxFailure;
	}
	public void setPwdMaxFailure(int pwdMaxFailure) {
		this.pwdMaxFailure = pwdMaxFailure;
	}
	public long getPwdMinAge() {
		return pwdMinAge;
	}
	public void setPwdMinAge(long pwdMinAge) {
		this.pwdMinAge = pwdMinAge;
	}
	public int getPwdMinLength() {
		return pwdMinLength;
	}
	public void setPwdMinLength(int pwdMinLength) {
		this.pwdMinLength = pwdMinLength;
	}
	public boolean isPwdMustChange() {
		return pwdMustChange;
	}
	public void setPwdMustChange(boolean pwdMustChange) {
		this.pwdMustChange = pwdMustChange;
	}
	
	public PasswordPolicyTO clone(){
		PasswordPolicyTO ppolicy = new PasswordPolicyTO();
		ppolicy.setPwdAllowUserChange(this.isPwdAllowUserChange());
		ppolicy.setPwdExpireWarning(this.getPwdExpireWarning());
		ppolicy.setPwdFailureCountInterval(this.getPwdFailureCountInterval());
		ppolicy.setPwdGraceAuthNLimit(this.getPwdGraceAuthNLimit());
		ppolicy.setPwdInHistory(this.getPwdInHistory());
		ppolicy.setPwdLockout(this.isPwdLockout());
		ppolicy.setPwdLockoutDuration(this.getPwdLockoutDuration());
		ppolicy.setPwdMaxAge(this.getPwdMaxAge());
		ppolicy.setPwdMaxFailure(this.getPwdMaxFailure());
		ppolicy.setPwdMinAge(this.getPwdMinAge());
		ppolicy.setPwdMinLength(this.getPwdMinLength());
		ppolicy.setPwdMustChange(this.isPwdMustChange());
		return ppolicy;
	}
	
}
