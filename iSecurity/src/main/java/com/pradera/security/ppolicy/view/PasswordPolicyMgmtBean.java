package com.pradera.security.ppolicy.view;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.security.ppolicy.to.PasswordPolicyTO;
import com.pradera.security.utils.view.GenericBaseBean;
import com.pradera.security.utils.view.LDAPUtilities;
import com.pradera.security.utils.view.LDAPUtility;
import com.pradera.security.utils.view.PropertiesConstants;

@DepositaryWebBean
public class PasswordPolicyMgmtBean extends GenericBaseBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private PasswordPolicyTO passwordPolicy;

	@Inject
	@LDAPUtility
	LDAPUtilities ldapUtilities;
	
	@PostConstruct
	public void init(){
		passwordPolicy = ldapUtilities.getPasswordPolicyDetails();
	}
	
	public void updatePasswordPolicy(){
		//hideDialogs();
		try{
			ldapUtilities.updatePasswordPolicyDetails(passwordPolicy);
			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS, null, "passpolicy.chngPasswordPolicy.successfully", null);
			JSFUtilities.showComponent("frmPasswordPolicy:cnfpwdsavesuccessfully");
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
	}
	
	public void beforeUpdatePasswordPolicy(){
		//hideDialogs();
		showMessageOnDialog(PropertiesConstants.USER_CONFIRM_HEADER_MODIFICATION, null, "passpolicy.cnf.chngPasswordPolicy", null);
		JSFUtilities.executeJavascriptFunction("PF('cnfwchngpwdpolicy').show()");
		
	}
	public void resetPasswordPolicy(){
		JSFUtilities.resetViewRoot();
		passwordPolicy = ldapUtilities.getPasswordPolicyDetails();
	}
	public void hideDialogs(){
		//JSFUtilities.hideGeneralDialogues();
		JSFUtilities.hideComponent("frmPasswordPolicy:cnfchngpwdpolicy");
	}

	/**
	 * @return the passwordPolicy
	 */
	public PasswordPolicyTO getPasswordPolicy() {
		return passwordPolicy;
	}

	/**
	 * @param passwordPolicy the passwordPolicy to set
	 */
	public void setPasswordPolicy(PasswordPolicyTO passwordPolicy) {
		this.passwordPolicy = passwordPolicy;
	}
	
}
