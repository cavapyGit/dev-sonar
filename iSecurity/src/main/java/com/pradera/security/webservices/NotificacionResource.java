package com.pradera.security.webservices;

import java.io.Serializable;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.pradera.commons.httpevent.NotificationEvent;
import com.pradera.commons.httpevent.publish.BrowserWindow;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.logging.PraderaLogger;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class NotificacionResource.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 11/05/2013
 */
@Path("/notifications")
@ApplicationScoped
public class NotificacionResource implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5048932707147579625L;
	
	/** The event process. */
	@Inject
	@NotificationEvent(NotificationType.PROCESS)
	private Event<BrowserWindow> eventProcess;
	
	/** The event message. */
	@Inject
	@NotificationEvent(NotificationType.MESSAGE)
	private Event<BrowserWindow> eventMessage;
	
	/** The event email. */
	@Inject
	@NotificationEvent(NotificationType.EMAIL)
	private Event<BrowserWindow> eventEmail;
	
	@Inject
	@NotificationEvent(NotificationType.REPORT)
	private Event<BrowserWindow> eventReport;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/**
	 * Notify event browser.
	 *
	 * @param browserWindow the browser window
	 * @return the response
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response notifyEventBrowser(BrowserWindow browserWindow) {
		try {
			NotificationType notify = NotificationType.get(browserWindow.getNotificationType());
			switch (notify) {
			case PROCESS:
				eventProcess.fire(browserWindow);
				log.info("fire event process "+browserWindow.toString());
				break;
	
			case MESSAGE:
				eventMessage.fire(browserWindow);
				log.info("fire event message "+browserWindow.toString());
				break;
			
			case EMAIL:
				eventEmail.fire(browserWindow);
				log.info("fire event email "+browserWindow.toString());
				break;
				
			case REPORT:
				eventReport.fire(browserWindow);
				log.info("fire event report "+browserWindow.toString());
			}
			
			
		} catch(Exception ex) {
			return Response.status(Status.INTERNAL_SERVER_ERROR.getStatusCode()).entity(ex.getMessage()).build();
		}
		return Response.status(Status.OK.getStatusCode()).entity("sucess").build();
	}
	
	@GET
	@Path("/user/{login}")
	public Response fireNotification(@PathParam("login")  String userName){
		BrowserWindow browserWindow = new BrowserWindow();
		browserWindow.setNotificationType(NotificationType.MESSAGE.getCode());
		browserWindow.setMessage("Prueba de Notificacion en cluster");
		browserWindow.setSubject("Prueba de replicacion");
		browserWindow.setUserChannel(userName);
		browserWindow.setIdNotification(1l);
		eventMessage.fire(browserWindow);
		return Response.status(Status.OK.getStatusCode()).entity("sucess").build();
	}

}
