package com.pradera.security.webservices.service;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.ejb.EJB;
import javax.enterprise.event.Event;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.apache.log4j.Logger;
import org.apache.myfaces.extensions.cdi.core.api.scope.conversation.WindowContext;

import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.security.model.type.LoginMessageType;
import com.pradera.commons.security.model.type.LogoutMotiveType;
import com.pradera.commons.security.model.type.UserAccountStateType;
import com.pradera.commons.security.model.type.UserSessionAccessType;
import com.pradera.commons.security.model.type.UserSessionStateType;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.audit.facade.UserAuditingFacade;
import com.pradera.security.model.ExceptionUserPrivilege;
import com.pradera.security.model.HistoricalState;
import com.pradera.security.model.Holiday;
import com.pradera.security.model.OptionPrivilege;
import com.pradera.security.model.Schedule;
import com.pradera.security.model.ScheduleDetail;
import com.pradera.security.model.ScheduleException;
import com.pradera.security.model.System;
import com.pradera.security.model.SystemProfile;
import com.pradera.security.model.UserAccount;
import com.pradera.security.model.UserSession;
import com.pradera.security.model.type.HolidayStateType;
import com.pradera.security.model.type.ParameterTableType;
import com.pradera.security.model.type.ScheduleDetailStateType;
import com.pradera.security.model.type.ScheduleExceptionDetailStateType;
import com.pradera.security.model.type.ScheduleExceptionStateType;
import com.pradera.security.model.type.ScheduleExceptionType;
import com.pradera.security.model.type.ScheduleStateType;
import com.pradera.security.model.type.ScheduleType;
import com.pradera.security.model.type.SystemStateType;
import com.pradera.security.model.type.UserAccountType;
import com.pradera.security.systemmgmt.facade.SystemServiceFacade;
import com.pradera.security.systemmgmt.service.SystemServiceBean;
import com.pradera.security.systemmgmt.view.filters.HolidayFilter;
import com.pradera.security.systemmgmt.view.filters.ScheduleDetailFilter;
import com.pradera.security.systemmgmt.view.filters.ScheduleExceptionDetailFilter;
import com.pradera.security.systemmgmt.view.filters.ScheduleExceptionFilter;
import com.pradera.security.systemmgmt.view.filters.ScheduleFilter;
import com.pradera.security.usermgmt.facade.UserServiceFacade;
import com.pradera.security.usermgmt.service.UserAccountServiceBean;
import com.pradera.security.usermgmt.view.ManagerNavigationBean;
import com.pradera.security.usermgmt.view.filters.UserAccountFilter;
import com.pradera.security.utils.view.LDAPUtilities;
import com.pradera.security.utils.view.LDAPUtility;
import com.pradera.security.utils.view.PropertiesConstants;
import com.pradera.security.utils.view.SecurityUtilities;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * Clase que contiene los metodos que son usados patra los servicios web
 * @author jquino
 *
 */
public class UsuarioServiceBean implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final Logger log = Logger.getLogger(UsuarioServiceBean.class);

	/** The excepcion. */
	@Inject
	transient Event<ExceptionToCatchEvent> excepcion;

	@EJB
	UserServiceFacade userServiceFacade;

	/** The system service facade. */
	@EJB
	SystemServiceFacade systemServiceFacade;

	@Inject
	Instance<ManagerNavigationBean> managerNavigationBean;

	/** The window context. to kill all conversations */
	// @Inject
	WindowContext windowContext;

	/** The user auditing facade. */
	@EJB
	UserAuditingFacade userAuditingFacade;
	
	/** The user account service bean. */
	@EJB
	private UserAccountServiceBean userAccountServiceBean;

//	@Inject
//	DatosInicialesSingleton datosInicialesSingleton;

	/** Nemonico del sistema por defecto */
	private String MNEMONIC_SISTEM_DEFAULT;

	/** The url source login. */
	private String urlSourceLogin;
	/** The user info temporal. */
	private UserInfo userInfoTemporal;
	/** The exception in schedule. */
	private boolean exceptionInSchedule;
	/** The remote UserSession. */
	private UserSession remoteUserSession;

	/** The email. */
	@NotNull(message = "{user.email.required}")
	private String email;

	/** The user info. */
	@Inject
	Instance<UserInfo> userInfo;
	@Inject
	private SystemServiceBean systemServiceBean;
	
	/** The ldap utilities. */
	@Inject
	@LDAPUtility
	LDAPUtilities ldapUtilities;

	private List<System> lstSystemsByUser;

	private boolean autenticado;
	private String errorCode;
	private String language;

	private static final char[] PASSWORD = "enfldsgbnlsngdlksdsgm"
			.toCharArray();
	private static final byte[] SALT = { (byte) 0xde, (byte) 0x33, (byte) 0x10,
			(byte) 0x12, (byte) 0xde, (byte) 0x33, (byte) 0x10, (byte) 0x12, };

	/**
	 * Constructor
	 */
	public UsuarioServiceBean() {

	}

	/**
	 * Metodo que se ejecuta posterior a la construccion de un determinado
	 * objeto
	 * 
	 * @throws BiometricoException
	 */
	@PostConstruct
	public void inicio() {

		language = "es";

		// get mnemonic system
		MNEMONIC_SISTEM_DEFAULT = "SDGD";
		log.info("menonico sistema2: " + MNEMONIC_SISTEM_DEFAULT);

		autenticado = false;
		errorCode = "";

	}

	public Map<String, Object> autenticacion(String loginUser, String password)
			throws ServiceException,
			GeneralSecurityException, IOException {

		Map<String, Object> mRespuesta = new HashMap<String, Object>();
		language = "es";

		String userAdmin = "BPM_ADMIN";

		UserAccount userAccount;

		// Desencriptar password
		try {

			password = decrypt(password);

		} catch (IllegalBlockSizeException e) {

			autenticado = false;
			errorCode = "Error en la contraseña encriptada";

			mRespuesta.put("autenticado", autenticado);
			mRespuesta.put("errorCode", errorCode);

			return mRespuesta;

		}

		autenticado = false;
		errorCode = "";

		String returnMessage = "";

		UserAccountFilter userAccountFilter = new UserAccountFilter();
		userAccountFilter.setLoginUserAccount(loginUser);

		try {

			//
			// validando la existencia del usuario
			//

			// validando la existencia del usuario
			userAccount = userServiceFacade.validateUserServiceFacade(userAccountFilter);

			// si el usuario es nulo
			if (userAccount == null) {

				// usuario no existe
				autenticado = false;

				errorCode = PropertiesConstants.ERROR_LOGIN_USER_INVALID_PASSWORD;

			} else {

				// usuario si existe, verificando estado del usuario
				if (userAccount.getState().intValue() != UserAccountStateType.CONFIRMED.getCode()) {

					// usuario en estado diferente a usuario confirmado
					if (userAccount.getState().intValue() == UserAccountStateType.ANNULLED.getCode()) {

						autenticado = false;
						errorCode = PropertiesConstants.ERROR_LOGIN_USER_UNREGISTER;

					}

					// si el usuario esta blockeado
					if (userAccount.getState().intValue() == UserAccountStateType.BLOCKED.getCode()) {

						autenticado = false;
						errorCode = PropertiesConstants.ERROR_LOGIN_USER_BLOCKED;

					}

					if (userAccount.getState().intValue() == UserAccountStateType.REGISTERED.getCode()) {

						autenticado = false;
						errorCode = PropertiesConstants.ERROR_LOGIN_USER_INACTIVE;

					}

				} else {

					// usuario confirmado

					// Validate user and password on LDAP
					// StringBuffer sb = new StringBuffer();
					LoginMessageType ldapConnectionSuccess = null;

					// USUARIOS INTERNOS siempre se conectan con DA
					boolean sanConfiguration = true;

					// si la cuenta del usuario es igual a "INTERNO"
					if (userAccount.getType().equals(UserAccountType.INTERNAL.getCode())) {

						if (sanConfiguration) {
							// using Active Directory configuration
							ldapConnectionSuccess = loginActiveDirectory(loginUser, password);
						} else {
							log.info("AUTENTICACION USUARIO EXTERNO");
						}

					} else {
						log.info("AUTENTICACION USUARIO EXTERNO");
					}

					password = "";

					// si el codigo de la respesta es menor a 0, no se pudo autenticar
					if (ldapConnectionSuccess.getCode() < 0) {

						// si la respuesta de la autenticacion es un usuario
						// blockeado
						if (LoginMessageType.USER_LOCKED.equals(ldapConnectionSuccess)&& userAccount.getState().intValue() == UserAccountStateType.CONFIRMED.getCode()) {

							// Registrando usuario bloqueado
							registerBlockUser(userAccount);
						}

						autenticado = false;
						errorCode = ldapConnectionSuccess.getValue();

					} else

					// si hay error de credenciales
					if (ldapConnectionSuccess.getCode() == 2) {
						// usuario con error de credencial o login

						autenticado = false;
						errorCode = ldapConnectionSuccess.getValue();
					} else {
						// no hay error en la autenticacion

						userInfoTemporal = new UserInfo();
						// Set Attributes to the Session
						userInfoTemporal.getUserAccountSession().setIdUserAccountPk(userAccount.getIdUserPk());
						userInfoTemporal.getUserAccountSession().setUserName(userAccount.getLoginUser());
						userInfoTemporal.getUserAccountSession().setFullName(userAccount.getFullName());
						userInfoTemporal.getUserAccountSession().setEmail(userAccount.getIdEmail());
						userInfoTemporal.getUserAccountSession().setState(userAccount.getState());
						userInfoTemporal.getUserAccountSession().setUserInstitutionName(userAccount.getInstitutionsSecurity().getInstitutionName());
						userInfoTemporal.getUserAccountSession().setIpAddress(getRemoteHostLocal());

						// Information about user type and roles
						userInfoTemporal.getUserAccountSession().setInstitutionType(userAccount.getInstitutionTypeFk());
						userInfoTemporal.getUserAccountSession().setParticipantCode(userAccount.getIdParticipantFk());
						userInfoTemporal.getUserAccountSession().setInstitutionCode(userAccount.getInstitutionsSecurity().getIdInstitutionSecurityPk());
						userInfoTemporal.getUserAccountSession().setIssuerCode(userAccount.getIdIssuerFk());
						userInfoTemporal.getUserAccountSession().setDescUserType(userAccount.getUserTypeDescription());		

						userInfoTemporal.getUserAccountSession().setRegulatorAgent(userAccount.getRegulatorAgent());
						// Responsability(Supervisor/Operator)
						userInfoTemporal.getUserAccountSession().setResponsability(userAccount.getResponsability());
						userInfoTemporal.getUserAccountSession().setDescInstitutionType(
								this.userServiceFacade.getParameter(userAccount.getInstitutionTypeFk()).getDescription());
						
						userInfoTemporal.setCurrentLanguage(language);

						// registrando la session del usuario

						// Make a registration of the session of user on DB
						UserSession userSession = new UserSession();
						userSession.setInitialDate(CommonsUtilities.currentDateTime());
						userSession.setUserAccount(userAccount);
						userSession.setRegistryUser((userInfoTemporal.getUserAccountSession().getUserName()));
						userSession.setRegistryDate(CommonsUtilities.currentDateTime());
						userSession.setLastModifyUser((userInfoTemporal.getUserAccountSession().getUserName()));
						userSession.setLastModifyDate(CommonsUtilities.currentDateTime());
						userSession.setLastModifyIp(userInfoTemporal.getUserAccountSession().getIpAddress());
						userSession.setScheduleAccessType(0);
						userSession.setState(UserSessionStateType.CONNECTED.getCode());
						userSession.setTicket(SecurityUtilities.generateSessionTicket());
						userSession.setLocale(language);
						/**Registro de ip para control de session*/
						userSession.setRegistryIp(getRemoteHostLocal());
						userInfoTemporal.getUserAccountSession().setTicketSession(userSession.getTicket());
						userInfoTemporal.getUserAccountSession().setLastSucessAccess(userSession.getInitialDate());

						// si las credenciales son invalidas
						if (ldapConnectionSuccess.equals(LoginMessageType.INVALIED_CREDENTIALS)) {

							// registrando la session del usuario

							// TODO:
							userServiceFacade.registerUserSessionServiceFacade(userSession);

							userInfoTemporal.getUserAccountSession().setIdUserSessionPk(userSession.getIdUserSessionPk());
							userInfoTemporal.setLogoutMotive(LogoutMotiveType.INVALIEDCREDENTIALS);

							JSFUtilities.setHttpSessionAttribute(GeneralConstants.LOGOUT_MOTIVE,userInfoTemporal.getLogoutMotive());
							JSFUtilities.setHttpSessionAttribute(GeneralConstants.USER_INFO_ID,userInfoTemporal.getUserAccountSession().getIdUserSessionPk());

						} else {

							// las credenciales son validas
							// registrando la session

							userAccountFilter.setIdUserAccountPK(userAccount.getIdUserPk());
							// START - Change to Schedule Option
							userAccountFilter.setInstitutionType(userAccount.getInstitutionTypeFk());
							// END - Change to Schedule Option
							userAccountFilter.setIdInstitutionSecurity(userAccount.getInstitutionsSecurity().getIdInstitutionSecurityPk());
							// To set userAccountState
							userAccountFilter.setUserAccountState(userAccount.getState());
							ScheduleExceptionFilter scheduleExceptionFilter = new ScheduleExceptionFilter();
							ScheduleExceptionDetailFilter scheduleExceptionDetailFilter = new ScheduleExceptionDetailFilter();
							scheduleExceptionDetailFilter.setUserFilter(userAccountFilter);
							scheduleExceptionFilter.setScheduleDetailExceptionFilter(scheduleExceptionDetailFilter);
							scheduleExceptionDetailFilter.setScheduleExceptionDetailState(ScheduleExceptionDetailStateType.CONFIRMED.getCode());
							scheduleExceptionFilter.setScheduleExceptionState(ScheduleExceptionStateType.CONFIRMED.getCode());
							scheduleExceptionFilter.setDate(CommonsUtilities.currentDate());
							scheduleExceptionFilter.setHour(CommonsUtilities.getDefaultDateWithCurrentTime());
							scheduleExceptionFilter.setScheduleExceptionType(ScheduleExceptionType.ACCESS_ACEPT.getCode());

							// Check if the user is Admin not check exceptions

							// Si el usuario es Administrador
							if (userAdmin.equalsIgnoreCase(loginUser)) {

								lstSystemsByUser = systemServiceFacade.searchSystemByStateServiceFacade(SystemStateType.CONFIRMED.getCode());

								autenticado = true;

							} else {
								// Check User Exceptions
								checkUserExceptions(userSession,scheduleExceptionFilter,userAccountFilter);
							}

							// to check if there is any deny schedule exception
							// for the system
							scheduleExceptionFilter.setScheduleExceptionType(ScheduleExceptionType.ACCESS_REFUSE.getCode());
							List<ScheduleException> deniedSystems = systemServiceFacade.listScheduleExceptionByUserServiceFacade(scheduleExceptionFilter);
							List<System> lstSystemsAvailable = new ArrayList<System>();
							lstSystemsAvailable.addAll(lstSystemsByUser);
							// to remove systems which is having deny access to
							// schedule exception.
							if (Validations.validateIsNotNull(deniedSystems)&& deniedSystems.size() != 0) {

								if (lstSystemsByUser.size() == 0) {

									autenticado = false;
									errorCode = PropertiesConstants.ERROR_LOGIN_INVALID_SCHEDULE;

									// TODO:
									returnMessage = null;
									// return null;
								}

							}

							// To remove the duplicate systems
							lstSystemsAvailable.clear();
							lstSystemsAvailable.addAll(lstSystemsByUser);
							lstSystemsByUser.clear();

							// To remove the duplicates
							if (lstSystemsAvailable != null&& !lstSystemsAvailable.isEmpty()) {

								for (System system : lstSystemsAvailable) {
									if (!validateSystemInList(lstSystemsByUser,system)) {
										lstSystemsByUser.add(system);
									}
								}
							}

							lstSystemsAvailable.clear();

							userServiceFacade.registerUserSessionServiceFacade(userSession);
							// Update access date user in system
							userServiceFacade.updateAccessSystemUser(userAccount.getIdUserPk());
							userInfoTemporal.getUserAccountSession().setIdUserSessionPk(userSession.getIdUserSessionPk());
							// Login is success so create userInfo Session
							// Scoped

							if (returnMessage != null) {

								// Only show systems where user have privileges
//								ManagerNavigationBean managerNavigation = managerNavigationBean.get();

//								if (remoteUserSession != null) {
//									managerNavigation.setUserSessionRemote(remoteUserSession.getIdUserSessionPk());
//								}

								// If is development pass url
//								managerNavigation.setUrlSourceLogin(urlSourceLogin);
//
//								if (Validations.validateIsNotNullAndNotEmpty(lstSystemsByUser)) {
//
//								}
//
								// set systems allows
//								managerNavigation.setListSystems(lstSystemsByUser);

								if (ldapConnectionSuccess.equals(LoginMessageType.PWD_MUST_CHANGE_EXP_WARN)
										|| ldapConnectionSuccess.equals(LoginMessageType.PWD_MUST_CHANGE_FIRST_LOGIN)
										|| ldapConnectionSuccess.equals(LoginMessageType.PWD_MUST_CHANGE_GRACE_LOGIN)) {

									autenticado = false;
									errorCode = ldapConnectionSuccess.getValue();

								} else if (ldapConnectionSuccess.equals(LoginMessageType.PWD_IN_EXP_WARING)) {
									autenticado = false;
									errorCode = ldapConnectionSuccess.getValue();
								}
							} else {
								// es nulo
							}
						}
						// returnMessage = "index";
					}
				}
			}

			loginUser = null;

			if (!errorCode.equalsIgnoreCase("")) {
				errorCode = PropertiesUtilities.getMessage(new Locale(language), errorCode);
			}

			mRespuesta.put("autenticado", autenticado);
			mRespuesta.put("errorCode", errorCode);

		} catch (ServiceException e) {
			e.printStackTrace();
		}

		return mRespuesta;
	}

	/**
	 * metodo que logea contra directorio activo
	 * 
	 * @param login
	 * @param password
	 * @return
	 * @throws Exception 
	 */
	public LoginMessageType loginActiveDirectory(String login, String password){

		LoginMessageType loginMessageType;
		try {
			loginMessageType = ldapUtilities.authenticateUserActiveDirectory(login, password);
			return loginMessageType;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Register block user.
	 * 
	 * @param userAccount
	 *            the user account
	 * @throws ServiceException
	 *             the service exception
	 */
	private void registerBlockUser(UserAccount userAccount)
			throws ServiceException {

		userAccount.setState(UserAccountStateType.BLOCKED.getCode());

		// Register historial change user state
		HistoricalState historicalState = new HistoricalState();
		historicalState.setMotive(Integer.valueOf(6));
		historicalState.setMotiveDescription("Wrong PWD Attempts");
		historicalState.setCurrentState(UserAccountStateType.BLOCKED.getCode());
		historicalState.setRegistryUser(userAccount.getLoginUser());
		historicalState.setTableName(userAccount.getClass().getAnnotation(Table.class).name());
		historicalState.setBeforeState(UserAccountStateType.CONFIRMED.getCode());
		historicalState.setIdRegister(Long.parseLong(userAccount.getIdUserPk().toString()));
		historicalState.setRegistryDate(CommonsUtilities.currentDateTime());
		historicalState.setLastModifyDate(new Date());
		historicalState.setLastModifyIp(getRemoteHostLocal());
		historicalState.setLastModifyUser(userAccount.getLoginUser());
		historicalState.setLastModifyPriv(0);
		ThreadLocalContextHolder.put(PropertiesConstants.HISTORICAL_STATE_LOGGER, historicalState);
		// To close the session of blocked user
		userAccount.setLastModifyDate(new Date());
		userAccount.setLastModifyIp(getRemoteHostLocal());
		userAccount.setLastModifyUser(userAccount.getLoginUser());
		userAccount.setLastModifyPriv(0);

		// userAccountServiceBean.updateUserAccountState(userAccount);
		// userAccountServiceBean.updateUserAccountState(userAccount);
	}

	/**
	 * Chech user exceptions.
	 * 
	 * @param userSession
	 *            the user session
	 * @param scheduleExceptionFilter
	 *            the schedule exception filter
	 * @param userAccountFilter
	 *            the user account filter
	 * @throws ServiceException
	 *             the service exception
	 * @throws edv.security.utils.ServiceException
	 */
	private void checkUserExceptions(UserSession userSession,
			ScheduleExceptionFilter scheduleExceptionFilter,
			UserAccountFilter userAccountFilter) throws ServiceException,
			ServiceException {
		// Validate if the user have some schedule
		// exception to access to a system
		List<ScheduleException> lstScheduleException = systemServiceFacade.listScheduleExceptionByUserServiceFacade(scheduleExceptionFilter);
		exceptionInSchedule = false;
		lstSystemsByUser = new ArrayList<System>();
		if (Validations.validateIsNotNull(lstScheduleException) && !lstScheduleException.isEmpty()) {
			exceptionInSchedule = true;
			// For each system on the list of
			// exceptions, add
			// the system to lstSystemsByUser
			for (ScheduleException scheduleException : lstScheduleException) {
				// lstSystemsByUser.add(systemServiceFacade
				// .getSystemWithImage(scheduleException.getSystem().getIdSystemPk()));
				lstSystemsByUser.add(scheduleException.getSystem());
			}
			userSession.setScheduleAccessType(UserSessionAccessType.SCHEDULEEXCEPTION.getCode());

			autenticado = true;

		}
		// To get the userExceptionprivileges for the
		// current date if available
		List<ExceptionUserPrivilege> userExceptionPrivileges = systemServiceFacade.listUserExceptionPrivilegeServiceFacade(userInfoTemporal.getUserAccountSession().getIdUserAccountPk());

		if (Validations.validateIsNotNull(userExceptionPrivileges) && !userExceptionPrivileges.isEmpty()) {
			// For each system on the list of
			// exceptions, add the system to lstSystemsByUser
			for (ExceptionUserPrivilege userExceptionPrivile : userExceptionPrivileges) {
				// lstSystemsByUser.add(systemServiceFacade.getSystemWithImage(userExceptionPrivile.getSystem().getIdSystemPk()));
				lstSystemsByUser.add(userExceptionPrivile.getSystem());
			}
		}

		Holiday holiday = null;
		if (!exceptionInSchedule) {
			HolidayFilter holidayFilterSearch = new HolidayFilter();
			holidayFilterSearch.setHolidayState(HolidayStateType.REGISTERED.getCode());
			holidayFilterSearch.setHolidayDate(CommonsUtilities.currentDate());
			// Validate if current date is a holiday
			holiday = systemServiceFacade.findHolidayByFiltersServiceFacade(holidayFilterSearch);
		}

		if (Validations.validateIsNotNullAndNotEmpty(holiday)) {

			//
			autenticado = false;
			errorCode = PropertiesConstants.ERROR_LOGIN_HOLIDAY;

		} else {

			ScheduleFilter scheduleFilter = new ScheduleFilter();
			scheduleFilter.setScheduleType(ScheduleType.INSTITUTIONS.getCode());
			// START - Change to Schedule Option
			scheduleFilter.setInstitutionType(userAccountFilter.getInstitutionType());
			// END - Change to Schedule Option
			scheduleFilter.setScheduleState(ScheduleStateType.CONFIRMED.getCode());

			// Validate if the institution of the user
			// has registered schedule
			Schedule scheduleInstitution = systemServiceFacade.getScheduleByInstitutionServiceFacade(scheduleFilter);

			if (Validations.validateIsNull(scheduleInstitution)) {
				ScheduleDetailFilter scheduleDetailFilter = new ScheduleDetailFilter();
				scheduleDetailFilter.setDay(Calendar.getInstance().get(Calendar.DAY_OF_WEEK));
				scheduleDetailFilter.setHour(CommonsUtilities.getDefaultDateWithCurrentTime());
				scheduleDetailFilter.setScheduleDetailState(ScheduleDetailStateType.REGISTERED.getCode());
				scheduleFilter.setScheduleDetailFilter(scheduleDetailFilter);
				scheduleFilter.setScheduleType(ScheduleType.SYSTEMS.getCode());
				scheduleFilter.setUserAccountFilter(userAccountFilter);
				// If the institution dont have schedule
				// registered, list all the system
				// that have allowed access to the user
				// for the current date and time
				lstSystemsByUser.addAll(systemServiceFacade.listAvailiableSystemsByScheduleServiceFacade(scheduleFilter));
				if (!lstSystemsByUser.isEmpty()) {
					userSession.setScheduleAccessType(UserSessionAccessType.NORMAL.getCode());
					autenticado = true;

				} else {
					// If the user don't have access at
					// least to one system, then return
					// to login page

					autenticado = false;
					errorCode = PropertiesConstants.ERROR_LOGIN_INVALID_SCHEDULE;
				}

			} else {

				ScheduleDetailFilter scheduleDetailFilter = new ScheduleDetailFilter();
				scheduleDetailFilter.setDay(Calendar.getInstance().get(Calendar.DAY_OF_WEEK));
				scheduleDetailFilter.setHour(CommonsUtilities.getDefaultDateWithCurrentTime());
				scheduleDetailFilter.setScheduleDetailState(ScheduleDetailStateType.REGISTERED.getCode());
				scheduleFilter.setScheduleDetailFilter(scheduleDetailFilter);
				// If the institution of the user have
				// registered schedule, validate if
				// the access is enabled for the current
				// day and time
				ScheduleDetail scheduleDetailUser = systemServiceFacade.getScheduleDetailByUserServiceFacade(scheduleFilter);

				if (Validations.validateIsNotNullAndNotEmpty(scheduleDetailUser)) {

					scheduleFilter.setScheduleType(ScheduleType.SYSTEMS.getCode());
					scheduleFilter.setUserAccountFilter(userAccountFilter);
					// list all the system that have
					// allowed access to the user
					// for the current date and time
					lstSystemsByUser.addAll(systemServiceFacade.listAvailiableSystemsByScheduleServiceFacade(scheduleFilter));

					if (!lstSystemsByUser.isEmpty()) {
						userSession.setScheduleAccessType(UserSessionAccessType.NORMAL.getCode());
						autenticado = true;
					} else {
						// If the user don't have access
						// at least to one system, then
						// return to login page
						autenticado = false;
						errorCode = PropertiesConstants.ERROR_LOGIN_INVALID_SCHEDULE;
					}
				} else {
					autenticado = false;
					errorCode = PropertiesConstants.ERROR_LOGIN_INVALID_SCHEDULE;
				}
			}
		}
	}

	/**
	 * Validate system in list.
	 * 
	 * @param lstSystems
	 *            the lst systems
	 * @param system
	 *            the system
	 * @return true, if successful
	 * @throws ServiceException
	 *             the service exception
	 */
	private boolean validateSystemInList(List<System> lstSystems, System system)throws ServiceException {
		for (System objSystemAux : lstSystems) {
			if (system.getIdSystemPk().equals(objSystemAux.getIdSystemPk())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Creates the user info session.
	 * 
	 * @return the user info
	 */
	private UserInfo createUserInfoSession() {

		UserInfo userInfoSuccess = new UserInfo();

		userInfoSuccess.getUserAccountSession().setIdUserAccountPk(userInfoTemporal.getUserAccountSession().getIdUserAccountPk());
		userInfoSuccess.getUserAccountSession().setIdUserSessionPk(userInfoTemporal.getUserAccountSession().getIdUserSessionPk());
		userInfoSuccess.getUserAccountSession().setUserName(userInfoTemporal.getUserAccountSession().getUserName());
		userInfoSuccess.getUserAccountSession().setFullName(userInfoTemporal.getUserAccountSession().getFullName());
		userInfoSuccess.getUserAccountSession().setEmail(userInfoTemporal.getUserAccountSession().getEmail());
		userInfoSuccess.getUserAccountSession().setState(userInfoTemporal.getUserAccountSession().getState());
		userInfoSuccess.getUserAccountSession().setUserInstitutionName(userInfoTemporal.getUserAccountSession().getUserInstitutionName());
		userInfoSuccess.getUserAccountSession().setIpAddress(userInfoTemporal.getUserAccountSession().getIpAddress());
		userInfoSuccess.getUserAccountSession().setInstitutionType(userInfoTemporal.getUserAccountSession().getInstitutionType());
		userInfoSuccess.getUserAccountSession().setParticipantCode(userInfoTemporal.getUserAccountSession().getParticipantCode());
		userInfoSuccess.getUserAccountSession().setInstitutionCode(userInfoTemporal.getUserAccountSession().getInstitutionCode());
		userInfoSuccess.getUserAccountSession().setIssuerCode(userInfoTemporal.getUserAccountSession().getIssuerCode());
		userInfoSuccess.getUserAccountSession().setRegulatorAgent(userInfoTemporal.getUserAccountSession().getRegulatorAgent());
		userInfoSuccess.getUserAccountSession().setResponsability(userInfoTemporal.getUserAccountSession().getResponsability());
		userInfoSuccess.getUserAccountSession().setTicketSession(userInfoTemporal.getUserAccountSession().getTicketSession());
		userInfoSuccess.getUserAccountSession().setLastSucessAccess(userInfoTemporal.getUserAccountSession().getLastSucessAccess());
		userInfoSuccess.getUserAccountSession().setDescInstitutionType(userInfoTemporal.getUserAccountSession().getDescInstitutionType());
		userInfoSuccess.setCurrentLanguage(userInfoTemporal.getCurrentLanguage());

		return userInfoSuccess;
	}

	/**
	 * To prevent the system has profiles.
	 * 
	 * @param idSystemPk
	 *            the id system pk
	 * @return the profiles information
	 */
	public List<OptionPrivilege> getProfilesInformation(Integer idSystemPk,UserAccount userAccount) {

		List<OptionPrivilege> lstSystemOption = null;
		UserAccountFilter userAccountFilter = new UserAccountFilter();
		userAccountFilter.setIdUserAccountPK(userAccount.getIdUserPk());
		userAccountFilter.setIdSystemPk(idSystemPk);

		lstSystemOption = userServiceFacade.checkSystemProfiles(userAccountFilter);
		return lstSystemOption;
	}
	
	/**
	 * Metodo que obtiene el perfil que un usuario tiene dentro de un sistema
	 * @param idSystemPk
	 * @param userAccount
	 * @return
	 */
	public SystemProfile getProfilesInformation(String mnemonicoSistema,String loginUser) {

		try {
			return userAccountServiceBean.getSystemProfile(mnemonicoSistema,loginUser);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static String encrypt(String property)throws GeneralSecurityException, UnsupportedEncodingException {
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBEWithMD5AndDES");
		SecretKey key = keyFactory.generateSecret(new PBEKeySpec(PASSWORD));
		Cipher pbeCipher = Cipher.getInstance("PBEWithMD5AndDES");
		pbeCipher.init(Cipher.ENCRYPT_MODE, key, new PBEParameterSpec(SALT, 20));
		return base64Encode(pbeCipher.doFinal(property.getBytes("UTF-8")));
	}

	private static String base64Encode(byte[] bytes) {
		// NB: This class is internal, and you probably should use another impl
		return new BASE64Encoder().encode(bytes);
	}

	private static String decrypt(String property)throws GeneralSecurityException, IOException {
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBEWithMD5AndDES");
		SecretKey key = keyFactory.generateSecret(new PBEKeySpec(PASSWORD));
		Cipher pbeCipher = Cipher.getInstance("PBEWithMD5AndDES");
		pbeCipher.init(Cipher.DECRYPT_MODE, key, new PBEParameterSpec(SALT, 20));
		return new String(pbeCipher.doFinal(base64Decode(property)), "UTF-8");
	}

	private static byte[] base64Decode(String property) throws IOException {
		// NB: This class is internal, and you probably should use another impl
		return new BASE64Decoder().decodeBuffer(property);
	}
	
	@SuppressWarnings("static-access")
	private static String getRemoteHostLocal() {

		String host = "";
		try {
			InetAddress address = InetAddress.getLocalHost();
			host = address.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return host;
	}

}