package com.pradera.security.webservices.service;

import java.io.IOException;
import java.io.Serializable;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.apache.log4j.Logger;

import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.model.SystemOption;
import com.pradera.security.model.UserAccount;
import com.pradera.security.systemmgmt.facade.SystemServiceFacade;
import com.pradera.security.systemmgmt.service.SystemOptionServiceBean;
import com.pradera.security.usermgmt.service.UserAccountServiceBean;

@WebService(serviceName = "UsuarioService")
public class UsuarioService implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	Instance<UserInfo> userInfo;

	@Inject
	UsuarioServiceBean usuarioServiceBean;
	@Inject
	SystemOptionServiceBean systemOptionServiceBean;
	@Inject
	private UserAccountServiceBean userAccountServiceBean;
	@Inject
	private SystemServiceFacade systemServiceFacade;

	private static Logger log = Logger.getLogger(UsuarioService.class);


	/**
	 * Metodo de autenticacion de usuario
	 * 
	 * @param nombreUsuario
	 * @return
	 * @throws ServiceException
	 * @throws IOException
	 * @throws GeneralSecurityException
	 */

	@WebMethod(operationName = "autenticacion")
	public String autenticacion(
			@WebParam(name = "nombreUsuario") String nombreUsuario,
			@WebParam(name = "password") String password)
			throws ServiceException, GeneralSecurityException, IOException {

		log.info("Llamando metodo autenticacion como Servicio Web...");

		StringBuilder respuesta = new StringBuilder();

		try {
			String errorCode = "";
			boolean autenticado = false;

			Map<String, Object> mapResuesta = usuarioServiceBean.autenticacion(nombreUsuario, password);

			autenticado = (boolean) mapResuesta.get("autenticado");
			errorCode = (String) mapResuesta.get("errorCode");

			respuesta = new StringBuilder();
			respuesta.append("<CONSULTA>");

			if (autenticado) {

				respuesta.append("<AUTENTICACION>");
				respuesta.append("true");
				respuesta.append("</AUTENTICACION>");

				respuesta.append("<COD_ERROR>");
				respuesta.append("");
				respuesta.append("</COD_ERROR>");
				
				UserAccount userAccount = systemServiceFacade.searchUserByUserLoginServiceFacade(nombreUsuario.trim().toUpperCase());

				// adicionando los datos del funcionario
				respuesta.append("<DATOS_PERSONALES>");
					respuesta.append("<PATERNO>");
					respuesta.append(userAccount.getFirtsLastName());
					respuesta.append("</PATERNO>");

					respuesta.append("<MATERNO>");
					respuesta.append(userAccount.getSecondLastName());
					respuesta.append("</MATERNO>");

					respuesta.append("<NOMBRE>");
					respuesta.append(userAccount.getFullName());
					respuesta.append("</NOMBRE>");
				respuesta.append("</DATOS_PERSONALES>");

			} else {

				respuesta.append("<AUTENTICACION>");
				respuesta.append("false");
				respuesta.append("</AUTENTICACION>");

				respuesta.append("<COD_ERROR>");
				respuesta.append(errorCode);
				respuesta.append("</COD_ERROR>");

			}

			respuesta.append("</CONSULTA>");

		} catch (Exception e) {

			log.error("Error en el metodo autenticacion");

			respuesta = new StringBuilder();
			respuesta.append("<CONSULTA>");
			respuesta.append("</CONSULTA>");

		} finally {

			log.info("...termino llamada metodo autenticacion como Servicio Web.");
			return respuesta.toString();

		}

	}

	/**
	 * Metodo que halla los perfiles de un usuario
	 * 
	 * @param nombreUsuario
	 * @return
	 * @throws ServiceException
	 */

	@WebMethod(operationName = "perfil")
	public String perfil(
			@WebParam(name = "nombreUsuario") String nombreUsuario,
			@WebParam(name = "sistema") String sistema) throws ServiceException {

		log.info("Llamando metodo perfil como Servicio Web...");

		StringBuilder respuesta = new StringBuilder();

		try {

			respuesta.append("<CONSULTA>");

			respuesta.append("<PERFIL>");
			respuesta.append(usuarioServiceBean.getProfilesInformation(sistema,nombreUsuario).getMnemonic());
			respuesta.append("</PERFIL>");

			respuesta.append("</CONSULTA>");

		} catch (Exception e) {

			log.error("Error en el metodo perfil");

			respuesta = new StringBuilder();
			respuesta.append("<CONSULTA>");
			respuesta.append("</CONSULTA>");

		} finally {
			log.info("...termino llamada metodo perfil como Servicio Web.");
			return respuesta.toString();
		}
	}

	
	/**
	 * Metodo que retorna el menu de un usuario para un sistema
	 * 
	 * @return
	 */
	@WebMethod(operationName = "menu_sistema")
	public String obtenerMenu(
			@WebParam(name = "nombreUsuario") String usuario,
			@WebParam(name = "mnemonico_sistema") String mnemonicoSistema) {

		log.info("Llamando metodo obtener_menu como Servicio Web...");

		StringBuilder respuesta = new StringBuilder();
		List<SystemOption> listaOpciones = new ArrayList<SystemOption>();
		
		//obteniendo el menu del sistema
		listaOpciones = systemOptionServiceBean.getMenu(usuario, mnemonicoSistema);
		
		try {
			
			respuesta.append("<CONSULTA>");
			
			if(!listaOpciones.isEmpty()){
				
				respuesta.append("<MENUS>");
			
				for (SystemOption menu: listaOpciones) {
					
					respuesta.append("<MENU>");
					
					respuesta.append("<ORDEN>");
					respuesta.append(menu.getOrderOption());
					respuesta.append("</ORDEN>");
					
					respuesta.append("<DESCRIPCION>");
					respuesta.append(menu.getCatalogueLanguages().get(0).getCatalogueName());
					respuesta.append("</DESCRIPCION>");
						
						respuesta.append("<SUBMENUS>");
						for (SystemOption opcion: menu.getSystemOptions() ) {
							respuesta.append("<SUBMENU>");
							
							respuesta.append("<ORDEN>");
							respuesta.append(opcion.getOrderOption());
							respuesta.append("</ORDEN>");
								
							respuesta.append("<DESCRIPCION>");
							respuesta.append(opcion.getCatalogueLanguages().get(0).getCatalogueName());
							respuesta.append("</DESCRIPCION>");
							
							respuesta.append("<URL>");
							respuesta.append(opcion.getUrlOption());
							respuesta.append("</URL>");
							
							respuesta.append("</SUBMENU>");
						}
						respuesta.append("</SUBMENUS>");
						
					respuesta.append("</MENU>");
				}
				
				respuesta.append("</MENUS>");
			}

			respuesta.append("</CONSULTA>");

		} catch (Exception e) {

			log.error("Error en el metodo obtener_menu");
			log.trace(e.getMessage());

			respuesta = new StringBuilder();
			respuesta.append("<CONSULTA>");
			respuesta.append("</CONSULTA>");

		} finally {

			log.info("...termino llamada metodo obtener_menu como Servicio Web.");
			return respuesta.toString();

		}
	}
	
	/**
	 * Metodo que retorna la lista de usuarios que tienen el perfil de un sistema enviado
	 * 
	 * @return
	 * @throws ServiceException 
	 */
	@WebMethod(operationName = "usuarios_por_perfil")
	public String usuariosPorPerfil(
			@WebParam(name = "mnemonico_sistema") String mnemonicoSistema,
			@WebParam(name = "mnemonico_perfil") String mnemonicoPerfil) throws ServiceException {

		log.info("Llamando metodo usuarios_por_perfil como Servicio Web...");

		StringBuilder respuesta = new StringBuilder();
		List <UserAccount> listaUserAccount;
		
		// Obteniendo la lista de los usuarios que cuentan con el perfil en el sistema enviado
		listaUserAccount = userAccountServiceBean.getUserAccountByProfileAndSystem(mnemonicoSistema, mnemonicoPerfil);
		
		try {
			
			respuesta.append("<CONSULTA>");
			
				for (UserAccount user: listaUserAccount) {
					
					respuesta.append("<FUNCIONARIO>");
						respuesta.append("<PATERNO>");
						if(user.getFirtsLastName() != null){
							respuesta.append(user.getFirtsLastName());
						}
						respuesta.append("</PATERNO>");
	
						respuesta.append("<MATERNO>");
						if(user.getSecondLastName() != null){
							respuesta.append(user.getSecondLastName());
						}
						respuesta.append("</MATERNO>");
	
						respuesta.append("<NOMBRE>");
						if(user.getFullName() != null){
							respuesta.append(user.getFullName());
						}
						respuesta.append("</NOMBRE>");
	
						respuesta.append("<LOGIN>");
						if(user.getLoginUser() != null){
							respuesta.append(user.getLoginUser());
						}
						respuesta.append("</LOGIN>");
					respuesta.append("</FUNCIONARIO>");
				}
				
			respuesta.append("</CONSULTA>");

		} catch (Exception e) {

			log.error("Error en el metodo usuarios_por_perfil");
			log.trace(e.getMessage());

			respuesta = new StringBuilder();
			respuesta.append("<CONSULTA>");
			respuesta.append("</CONSULTA>");

		} finally {

			log.info("...termino llamada metodo usuarios_por_perfil como Servicio Web.");
			return respuesta.toString();

		}
	}
	
}