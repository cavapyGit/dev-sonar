package com.pradera.security.webservices;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.pradera.commons.audit.UserTrackProcess;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserFilterTO;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.security.audit.service.UserAuditingServiceBean;
import com.pradera.security.model.InstitutionsSecurity;
import com.pradera.security.model.UserAccount;
import com.pradera.security.model.UserAuditDetail;
import com.pradera.security.model.UserAuditProcess;
import com.pradera.security.model.UserSession;
import com.pradera.security.systemmgmt.view.filters.InstitutionSecurityFilter;
import com.pradera.security.usermgmt.facade.UserServiceFacade;
import com.pradera.security.usermgmt.service.UserIntegrationServiceBean;
import com.pradera.security.usermgmt.service.UserSessionServiceBean;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class UserAccountResource.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 26/07/2013
 */
@Path("/users")
@Stateless
public class UserAccountResource implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3314825393111602930L;
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The user integration service. */
	@EJB
	UserIntegrationServiceBean userIntegrationService;
	
	@EJB
	UserSessionServiceBean userSessionServiceBean;
	
	@EJB
	UserAuditingServiceBean userAuditingService;
	
	@EJB
	UserServiceFacade userServiceFacade;
	
	/**
	 * Gets the user logins.
	 *
	 * @param userFilter the user filter
	 * @return the user logins
	 */
	@POST
	@Path("/logins")
	@Produces({MediaType.APPLICATION_JSON})
	public List<String> getUserLogins(UserFilterTO userFilter){
		List<String> usersLogins = new ArrayList<String>();
		for(UserAccount user : userIntegrationService.getUsersRemote(userFilter)) {
			usersLogins.add(user.getLoginUser());
		}
		return usersLogins;
	}
	
	/**
	 * Gets the user emails.
	 *
	 * @param userFilter the user filter
	 * @return the user emails
	 */
	@POST
	@Path("/emails")
	@Produces({MediaType.APPLICATION_JSON})
	public List<String> getUserEmails(UserFilterTO userFilter){
		List<String> usersEmails = new ArrayList<String>();
		for(UserAccount user : userIntegrationService.getUsersRemote(userFilter)) {
			usersEmails.add(user.getIdEmail());
		}
		return usersEmails;
	}
	
	/**
	 * Gets the users informations.
	 *
	 * @param userFilter the user filter
	 * @return the users
	 */
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public List<UserAccountSession> getUsers(UserFilterTO userFilter){
		List<UserAccountSession> accounts = new ArrayList<UserAccountSession>();
		for(UserAccount user : userIntegrationService.getUsersRemote(userFilter)) {
			UserAccountSession account = new UserAccountSession();
			if(Validations.validateIsNotNullAndNotEmpty(user.getSecondLastName())){
				account.setFullName(user.getFirtsLastName() + GeneralConstants.BLANK_SPACE + user.getSecondLastName() + GeneralConstants.BLANK_SPACE + user.getFullName());
			}else{
				account.setFullName(user.getFirtsLastName() + GeneralConstants.BLANK_SPACE + user.getFullName());
			}
			account.setEmail(user.getIdEmail());
			account.setUserName(user.getLoginUser());
			account.setRegulatorAgent(user.getRegulatorAgent());
			account.setResponsability(user.getResponsability());
			account.setState(user.getState());
			account.setPhoneNumber(user.getPhoneNumber());
			account.setParticipantCode(user.getIdParticipantFk());
			account.setIssuerCode(user.getIdIssuerFk());
			account.setInstitutionType(user.getInstitutionTypeFk());
			accounts.add(account);
		}
		return accounts;
	}
	
	@POST
	@Path("/institution")
	@Produces({MediaType.APPLICATION_JSON})
	public List<Object[]> getInstitutions(UserFilterTO userFilter){
		List<Object[]> intitutionLst = new ArrayList<Object[]>();
		InstitutionSecurityFilter filter = new InstitutionSecurityFilter();
		filter.setIdInstitutionTypeFk(userFilter.getInstitutionType());
		try{
			for(InstitutionsSecurity intitution : userIntegrationService.getInstitutionsByFilterServiceBean(filter)) {
				Object[] objArr = new Object[2];
				objArr[0] = intitution.getIdInstitutionSecurityPk();
				objArr[1] = intitution.getInstitutionName();
				
				intitutionLst.add(objArr);
			}
		} catch (Exception e) {
			log.error(e.getMessage());
        	e.printStackTrace();
		}
		return intitutionLst;
	}
	
	@POST
	@Path("/track")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response registerUserTrackProcess(UserTrackProcess userTrackProcess) {
		try{
			UserAuditProcess userAuditProcess = new UserAuditProcess();
			userAuditProcess.setRegistryDate(CommonsUtilities.currentDateTime());
			UserSession userSession = new UserSession();
			userSession.setIdUserSessionPk(userTrackProcess.getIdUserSessionPk());
			userAuditProcess.setUserSession(userSession);
			userAuditProcess.setClassName(userTrackProcess.getClassName());
			userAuditProcess.setBusinessMethod(userTrackProcess.getBusinessMethod());
			userAuditProcess.setIndError(userTrackProcess.getIndError());
			userAuditProcess.setLastModifyPriv(userTrackProcess.getIdPrivilegeSystem());
			userAuditProcess.setIdBusinessProcessFk(userTrackProcess.getIdBusinessProcess());
			userAuditProcess.setIdProcessLoggerFk(userTrackProcess.getIdProcessLogger());
			userAuditProcess.setIdReportLoggerFk(userTrackProcess.getIdProcessReport());
			if(userTrackProcess.getParameters() != null && !userTrackProcess.getParameters().isEmpty()){
				List<UserAuditDetail> parameters = new ArrayList<UserAuditDetail>();
				for(String key : userTrackProcess.getParameters().keySet()){
					UserAuditDetail userAuditDetail = new UserAuditDetail();
					userAuditDetail.setParamName(key);
					userAuditDetail.setParamValue(userTrackProcess.getParameters().get(key));
					userAuditDetail.setUserAuditProcess(userAuditProcess);
					parameters.add(userAuditDetail);
				}
				userAuditProcess.setUserAuditDetails(parameters);
			}
			userSessionServiceBean.create(userAuditProcess);
		}catch(Exception ex) {
			log.error(ex.toString());
			return Response.status(Status.INTERNAL_SERVER_ERROR.getStatusCode()).entity(ex.getMessage()).build();
		}
		return Response.status(Status.OK.getStatusCode()).entity("sucess").build();
	}
	
	@POST
	@Path("/closeSession")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response registerExitSystemByErrorPage(UserInfo userInfo) {
		try {
			userAuditingService.closeSessionServiceBean(userInfo);
			//close session in remote wars
			userServiceFacade.closeSessionModules(userInfo.getUserAccountSession().getUserName());
		} catch(Exception ex){
			log.error(ex.toString());
			return Response.status(Status.INTERNAL_SERVER_ERROR.getStatusCode()).entity(ex.getMessage()).build();
		}
		return  Response.status(Status.OK.getStatusCode()).entity("sucess").build();
	}

	/**
	 * Gets the users informations.
	 *
	 * @param userFilter the user filter
	 * @return the users
	 */
	@POST
	@Path("/billing")
	@Produces({MediaType.APPLICATION_JSON})
	public List<UserAccountSession> getUsersAccountForBilling(UserFilterTO userFilter){
		List<UserAccountSession> accounts = new ArrayList<UserAccountSession>();
		for(UserAccount user : userIntegrationService.getUsersRemoteForBilling(userFilter)) {
			UserAccountSession account = new UserAccountSession();			
			account.setParticipantCode(user.getIdParticipantFk());
			account.setIssuerCode(user.getIdIssuerFk());
			account.setInstitutionType(user.getInstitutionTypeFk());			
			account.setCountUser(user.getUserCount());
			accounts.add(account);
		}
		return accounts;
	}
	
	/**
	 * Gets the users informations.
	 *
	 * @param userFilter the user filter
	 * @return the users
	 */
	@POST
	@Path("/supervisor")
	@Produces({MediaType.APPLICATION_JSON})
	public List<UserAccountSession> getUsersSupervisor(UserFilterTO userFilter){
		List<UserAccountSession> accounts = new ArrayList<UserAccountSession>();
		for(UserAccount user : userIntegrationService.getUsersSupervisor(userFilter)) {
			UserAccountSession account = new UserAccountSession();
			if(Validations.validateIsNotNullAndNotEmpty(user.getSecondLastName())){
				account.setFullName(user.getFirtsLastName() + GeneralConstants.BLANK_SPACE + user.getSecondLastName() + GeneralConstants.BLANK_SPACE + user.getFullName());
			}else{
				account.setFullName(user.getFirtsLastName() + GeneralConstants.BLANK_SPACE + user.getFullName());
			}
			account.setEmail(user.getIdEmail());
			account.setUserName(user.getLoginUser());
			account.setRegulatorAgent(user.getRegulatorAgent());
			account.setResponsability(user.getResponsability());
			account.setState(user.getState());
			account.setPhoneNumber(user.getPhoneNumber());
			account.setParticipantCode(user.getIdParticipantFk());
			account.setIssuerCode(user.getIdIssuerFk());
			accounts.add(account);
		}
		return accounts;
	}
	
	@POST
	@Path("/operator")
	@Produces({MediaType.APPLICATION_JSON})
	public List<UserAccountSession> getUsersOperator(UserFilterTO userFilter){
		List<UserAccountSession> accounts = new ArrayList<UserAccountSession>();
		for(UserAccount user : userIntegrationService.getUsersOperator(userFilter)) {
			UserAccountSession account = new UserAccountSession();
			if(Validations.validateIsNotNullAndNotEmpty(user.getSecondLastName())){
				account.setFullName(user.getFirtsLastName() + GeneralConstants.BLANK_SPACE + user.getSecondLastName() + GeneralConstants.BLANK_SPACE + user.getFullName());
			}else{
				account.setFullName(user.getFirtsLastName() + GeneralConstants.BLANK_SPACE + user.getFullName());
			}
			account.setEmail(user.getIdEmail());
			account.setUserName(user.getLoginUser());
			account.setRegulatorAgent(user.getRegulatorAgent());
			account.setResponsability(user.getResponsability());
			account.setState(user.getState());
			account.setPhoneNumber(user.getPhoneNumber());
			account.setParticipantCode(user.getIdParticipantFk());
			account.setIssuerCode(user.getIdIssuerFk());
			accounts.add(account);
		}
		return accounts;
	}
	

	
	@POST
	@Path("/supervisoroperator")
	@Produces({MediaType.APPLICATION_JSON})
	public List<UserAccountSession> getUsersSupervisorOperator(UserFilterTO userFilter){
		List<UserAccountSession> accounts = new ArrayList<UserAccountSession>();
		for(UserAccount user : userIntegrationService.getUsersSupervisoroperator(userFilter)) {
			UserAccountSession account = new UserAccountSession();
			if(Validations.validateIsNotNullAndNotEmpty(user.getSecondLastName())){
				account.setFullName(user.getFirtsLastName() + GeneralConstants.BLANK_SPACE + user.getSecondLastName() + GeneralConstants.BLANK_SPACE + user.getFullName());
			}else{
				account.setFullName(user.getFirtsLastName() + GeneralConstants.BLANK_SPACE + user.getFullName());
			}
			account.setEmail(user.getIdEmail());
			account.setUserName(user.getLoginUser());
			account.setRegulatorAgent(user.getRegulatorAgent());
			account.setResponsability(user.getResponsability());
			account.setState(user.getState());
			account.setPhoneNumber(user.getPhoneNumber());
			account.setParticipantCode(user.getIdParticipantFk());
			account.setIssuerCode(user.getIdIssuerFk());
			accounts.add(account);
		}
		return accounts;
	}
	
}
