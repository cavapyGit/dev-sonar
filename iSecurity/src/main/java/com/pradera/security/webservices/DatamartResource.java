package com.pradera.security.webservices;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.model.TicketsDatamart;
import com.pradera.security.systemmgmt.facade.SystemServiceFacade;
import com.pradera.security.usermgmt.service.UserAccountServiceBean;
import com.pradera.security.usermgmt.service.UserIntegrationServiceBean;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class DatamartResource.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/01/2016
 */
@Path("/datamart")
@ApplicationScoped
public class DatamartResource implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3314825393111602930L;
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The user integration service. */
	@EJB
	UserIntegrationServiceBean userIntegrationService;
	
	@EJB
	UserAccountServiceBean userAccountServiceBean;
	
	@EJB
    SystemServiceFacade systemServiceFacade;

	
	/**
	 * Gets the option menu informations.
	 *
	 * @param filterProfile the profile pk filter
	 * @return map options
	 */
	@GET
	@Path("/menuoptions/{filterProfile}")
	@Produces({MediaType.APPLICATION_JSON+ ";charset=utf-8"})
	public Map<String, List<HashMap<String, Object>>> getInstitutions(@PathParam("system")Integer system,
																	  @PathParam("userAccount")Integer userAccount){
		log.info("com.pradera.security.webservices.DatamartResources.getInstitutions(@PathParam('system')Integer system,@PathParam('userAccount')Integer userAccount)");
		Integer filterProfile = userAccountServiceBean.getUserProfile(system,userAccount);
		Map<String, List<HashMap<String, Object>>> menuOption = getMenuOptions(filterProfile);
		return menuOption;
	}
	
	
	public Map<String, List<HashMap<String, Object>>> getMenuOptions(Integer filterProfile){	
		log.info("com.pradera.security.webservices.DatamartResources.getMenuOptions(Integer filterProfile)");
		Map<String, List<HashMap<String, Object>>> menuOption = new HashMap<String, List<HashMap<String, Object>>>();
		
		try{
			
			List<HashMap<String,Object>> listMap = userIntegrationService.getOptionsWithProfile(filterProfile);
			List<HashMap<String, Object>> listOptionMasters = new ArrayList<HashMap<String, Object>>();
			List<HashMap<String, Object>> listOptionChildrens = new ArrayList<HashMap<String, Object>>();
			Map<String, Object> optionMasters = null;
			
			for(HashMap<String,Object> map : listMap) {
				
				Integer optionType = (Integer) map.get("optionType");
				String optionName = (String) map.get("catalogueName");
				String optionUrl = (String) map.get("urlOption");
				String optionState = (String) map.get("state");
				
				
				if(optionType.equals(new Integer(1))){	//break; optionName Master
					
					optionMasters = new HashMap<String,Object>();
					listOptionChildrens = new ArrayList<HashMap<String, Object>>();
					
					optionMasters.put("optionName", optionName);
					optionMasters.put("optionChildrens", listOptionChildrens);
					listOptionMasters.add((HashMap<String, Object>) optionMasters);
					
				}else{	//optionName Children
					Map<String, Object> optionChildrens = new HashMap<String,Object>();
					optionChildrens.put("optionName", optionName);
					optionChildrens.put("optionUrl", optionUrl);
					optionChildrens.put("optionState", optionState);
					
					listOptionChildrens.add((HashMap<String, Object>) optionChildrens);
					
					if(optionMasters != null){
						optionMasters.put("optionChildrens", listOptionChildrens);
					}
				}
			
			}
			
			menuOption.put("optionMasters", listOptionMasters);
		
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
		
		return menuOption;
	}


	@GET
	@Path("/ticketOptions/{filterTicket}")
	@Produces({MediaType.APPLICATION_JSON+ ";charset=utf-8"})
	public Map<String, Object> getDataTicket(@PathParam("filterTicket")String ticket){	
		log.info("com.pradera.security.webservices.DatamartResources.getDataTicket(@PathParam('filterTicket')String ticket)");

		TicketsDatamart ticketsDatamart = new TicketsDatamart();
		try {
			ticketsDatamart = systemServiceFacade.getTicketDatamart(ticket);
			Map<String, List<HashMap<String, Object>>> menuOptions = getMenuOptions(ticketsDatamart.getProfileCode());
			ticketsDatamart.setMenuOptions(menuOptions);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Map<String, Object> mapTicketsDatamart = new HashMap<String, Object>();
		mapTicketsDatamart.put("institutionCode", ticketsDatamart.getInstitutionCode().toString());
		mapTicketsDatamart.put("institutionName", ticketsDatamart.getInstitutionName());
		mapTicketsDatamart.put("institutionTypeName", ticketsDatamart.getInstitutionTypeName());
		mapTicketsDatamart.put("institutionTypeCode", ticketsDatamart.getInstitutionTypeCode().toString());
		mapTicketsDatamart.put("ProfileCode", ticketsDatamart.getProfileCode().toString());
		mapTicketsDatamart.put("systemCode", ticketsDatamart.getSystemCode().toString());
		mapTicketsDatamart.put("usernameCode", ticketsDatamart.getUseraccountCode().toString());
		mapTicketsDatamart.put("expirationDate", ticketsDatamart.getExpirationDate().toString());
		mapTicketsDatamart.put("menuOptions", ticketsDatamart.getMenuOptions());
		
		return mapTicketsDatamart;
	}
	
	
}
