package com.pradera.security.webservices;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.model.SystemProfile;
import com.pradera.security.model.UserSession;
import com.pradera.security.model.type.SystemProfileStateType;
import com.pradera.security.systemmgmt.service.SystemProfileServiceBean;
import com.pradera.security.systemmgmt.service.SystemServiceBean;
import com.pradera.security.systemmgmt.view.filters.SystemProfileFilter;
import com.pradera.security.usermgmt.facade.UserServiceFacade;
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Class ConfiguracionServiceFacade.
 * is very important in the integration with different
 * war module. because here the information is shared 
 * to other contexts.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/11/2012
 */
@Path("sessionuserconfiguration")
@Stateless
public class ConfigurationResource implements Serializable {
   
    
    /** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8733409451168028268L;
	
	/** The user service facade. */
	@EJB 
    UserServiceFacade userServiceFacade;
	
	/** The system profile service. */
	@EJB
	SystemProfileServiceBean systemProfileService ;
	
	/** The system service. */
	@EJB
	SystemServiceBean systemService;
  
    
    /**
     * Gets the session user resource.
     * this web service is useful to expose session's user
     * for many wars
     *
     * @param sessionTickedId the session ticked id
     * @param module is the module name registered in each web.xml from wars
     * @return the session user resource
     * @throws ServiceException the service exception
     */
    @GET
    @Path("{sessionTicketId}/{module}")
    @Produces({MediaType.APPLICATION_JSON})
    public UserInfo getSessionUserResource(@PathParam("sessionTicketId") String sessionTickedId, @PathParam("module") String module)
    		throws ServiceException {
    	UserInfo userInfoSessionRest = null;
    	if (StringUtils.isNotBlank(sessionTickedId) && StringUtils.isNotBlank(module)) {
    		UserSession userSession = new UserSession();
    		userSession.setTicket(sessionTickedId);
    		userInfoSessionRest = userServiceFacade.getUserInfoByTicket(userSession, module);
    		if(userInfoSessionRest !=null){
	    		//Problem Gson
	    		userInfoSessionRest.setTimeZone(null);
    		}
    	} 
    	return userInfoSessionRest;
    }
    

    /**
     * Gets the profiles system.
     *
     * @param system the system
     * @return the profiles system
     * @throws ServiceException the service exception
     */
    @GET
	@Path("/profiles/{system}")
	@Produces({MediaType.APPLICATION_JSON})
	public List<Object[]> getProfilesSystem(@PathParam("system") Integer system) throws ServiceException {
		List<Object[]> profiles = new ArrayList<Object[]>();
		SystemProfileFilter systemProfileFilter = new SystemProfileFilter();
		systemProfileFilter.setProfileStateBaseQuery(SystemProfileStateType.CONFIRMED.getCode());
		systemProfileFilter.setIdSystemBaseQuery(system);
		for(SystemProfile systemProfile : systemProfileService.searchProfilesByFiltersServiceBean(systemProfileFilter)) {
				Object[] objArr = new Object[2];
				objArr[0] = systemProfile.getIdSystemProfilePk();
				objArr[1] = systemProfile.getName();
				profiles.add(objArr);
		}
		return profiles;
	}
	
	/**
	 * Gets the list system.
	 * in state confirmed
	 * @return the list system
	 * @throws ServiceException the service exception
	 */
	@GET
	@Path("/systems")
	@Produces({MediaType.APPLICATION_JSON})
	public List<Object[]> getListSystem() throws ServiceException{
		return systemService.getSystemList();
	}
    
}