package com.pradera.security.webservices;

import java.io.IOException;
import java.io.Serializable;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.pradera.commons.audit.UserAuditTrackingTO;
import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.security.model.type.LoginMessageType;
import com.pradera.commons.security.model.type.UserSessionStateType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.audit.service.UserAuditingServiceBean;
import com.pradera.security.model.UserAuditTracking;
import com.pradera.security.model.UserSession;
import com.pradera.security.utils.view.LDAPUtilities;
import com.pradera.security.utils.view.LDAPUtility;
import com.pradera.security.utils.view.SecurityUtilities;

/**
 * Session Bean implementation class AuditResource
 */
@Path("/audits")
@Stateless
@LocalBean
public class AuditResource implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8821899956484741876L;

	/** The log. */
	@Inject
	private PraderaLogger log;

	@EJB
	UserAuditingServiceBean userAuditingServiceBean;

	@Inject
	@LDAPUtility
	private LDAPUtilities ldapUtilities;

	@Inject
	@Configurable("app.security.behavior.san.enabled")
	private boolean sanConfiguration;

	@POST
	@Path("/closeAllSessions")
	public Response closeAllSessions() {
		try {
			userAuditingServiceBean.closeAllOpenSessions();
		} catch (Exception ex) {
			log.error(ex.getMessage());
			return Response
					.status(Status.INTERNAL_SERVER_ERROR.getStatusCode())
					.entity(ex.getMessage()).build();
		}
		return Response.status(Status.OK.getStatusCode()).entity("sucess")
				.build();
	}

	@POST
	@Path("/registerUserAuditTracking")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response registerUserAuditTracking(
			UserAuditTrackingTO userAuditTracking) {
		try {
			// Check if exist session
			if (userAuditTracking.getUserSession() != null) {
				Map<String, Object> param = new HashMap<>();
				param.put("id", userAuditTracking.getUserSession());
				UserSession userSession = (UserSession) userAuditingServiceBean
						.findObjectByQuery(
								"select us from UserSession us where us.idUserSessionPk =:id",
								param);
				if (userSession != null) {
					UserAuditTracking userAuditTable = new UserAuditTracking();
					userAuditTable.setUrl(userAuditTracking.getUrl());
					userAuditTable.setQueryString(userAuditTracking
							.getQueryString());
					userAuditTable.setExceptionMessage(userAuditTracking
							.getExceptionMessage());
					userAuditTable.setTimeAccess(CommonsUtilities
							.currentDateTime());
					userAuditTable.setUserSession(userSession);
					userAuditingServiceBean.create(userAuditTable);
				}

			}

		} catch (Exception ex) {
			log.error(ex.getMessage());
			return Response
					.status(Status.INTERNAL_SERVER_ERROR.getStatusCode())
					.entity(ex.getMessage()).build();
		}
		return Response.status(Status.OK.getStatusCode()).entity("sucess")
				.build();
	}

	/**
	 * Default constructor.
	 */
	public AuditResource() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Metodo de validacion de usuario
	 * 
	 * @param userName
	 *            ,pass
	 * @return
	 * @throws ServiceException
	 * @throws IOException
	 * @throws GeneralSecurityException
	 */
	@Performance
	@GET
	@Path("/verifyUsernamePassword")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response verifyUsernamePassword(
			@QueryParam("userName") String loginUser,
			@QueryParam("pass") String password) throws ServiceException {

		log.info("CDE::: ...Consultando usuario y contraseÃ±a en LDAP");
		LoginMessageType loginMessageType;
		try {
			// loginMessageType =
			// ldapUtilities.authenticateUserActiveDirectory(loginUser,
			// password);
			System.out.println("SEG:::::::::::"+sanConfiguration);
			if (sanConfiguration) {
				// using Active Directory configuration
				LoginMessageType ldapConnectionSuccess = null;
				ldapConnectionSuccess = ldapUtilities.authenticateUserActiveDirectory(loginUser, password);
				System.out.println("SEG:::RESPUESTA DE ACTIVE DIRECTORY:"+ldapConnectionSuccess.getValue());
				System.out.println("SEG:::STRING :"+ldapConnectionSuccess.toString());
				return Response.status(Status.OK.getStatusCode()).entity(ldapConnectionSuccess.getValue()).build();
			} else {
				if(this.ldapUtilities.isEnableAuthentication()) {
					loginMessageType = ldapUtilities.authenticateUser(loginUser,password, new StringBuffer());
					log.info("Respuesta:" + loginMessageType.toString());
					return Response.status(Status.OK.getStatusCode()).entity(loginMessageType.getValue()).build();
				} else {
					return Response.status(Status.OK.getStatusCode()).entity(LoginMessageType.LOGIN_SUCCESS.getValue()).build();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response.status(Status.OK.getStatusCode())
				.entity(LoginMessageType.LOGIN_FAIL.toString()).build();
	}

	/**
	 * Metodo de validacion de usuario
	 * 
	 * @param userName
	 *            ,pass
	 * @return
	 * @throws ServiceException
	 * @throws IOException
	 * @throws GeneralSecurityException
	 */
	@Performance
	@GET
	@Path("/isSupervisorOrManagerProfile/{userName}")
	@Produces({ MediaType.APPLICATION_JSON })
	public boolean isSupervisorOrManagerProfileTest(
			@PathParam("userName") String loginUser) throws ServiceException {
		log.info("CDE::: ...Consultando si el perfil es supervisor o gerente ");
		return userAuditingServiceBean.isSupervisorOrManagerProfile(loginUser);
	}

	/**
	 * Obtension de ip de registro
	 * 
	 * @param registryUser
	 *            ,ticket
	 * @return
	 * @throws ServiceException
	 * @throws IOException
	 * @throws GeneralSecurityException
	 */
	@Performance
	@GET
	@Path("/ipRegistrationAudit")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getIpRegistration(
			@QueryParam("registryUser") String registryUser,
			@QueryParam("ticket") String ticket) throws ServiceException {

		log.info("SEG::: ...Consultando la IP de registro con la cual inicio sesion ");
		String ipRegistration = null;
		try {
			ipRegistration = userAuditingServiceBean.getRegistrationIp(
					registryUser, ticket);
			log.info("CDE::..IP de autenticacion :" + ipRegistration);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// return
		// Response.status(Status.OK.getStatusCode()).entity(UserSessionStateType.NOTLOGGEDCONFIRMED.toString()).build();
		return Response.status(Status.OK.getStatusCode())
				.entity(ipRegistration).build();
	}
}
