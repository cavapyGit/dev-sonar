package com.pradera.security.webservices;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.pradera.commons.services.remote.security.to.InstitutionTO;
import com.pradera.integration.exception.CustomWebServiceException;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.institution.service.InstitutionSecurityServiceBean;

@Path("/institution")
@Stateless
public class InstitutionResource {

	@EJB 
	InstitutionSecurityServiceBean institutionSecurityServiceBean;	

	Logger log = Logger.getLogger(InstitutionResource.class);
	
	@POST
	@Path("/create")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createInstitutionSecurity(Object dataObject) throws ServiceException{
		log.info("Inicio web service de creacion de institution_security");		
		try { 
			Gson gson = new Gson();
			InstitutionTO req = gson.fromJson(gson.toJson(dataObject), InstitutionTO.class);
			
			institutionSecurityServiceBean.wsCreateInstituionSecurityService(req);
			log.info("Finalizó correctamente web service de creacion de institution_security");		
			return Response.status(Status.OK.getStatusCode()).entity("Success").build();
		} catch (JsonSyntaxException e) {
			log.info(String.format("Error en web service de creacion de institution_security: %d, %s", Status.BAD_REQUEST.getStatusCode(), "Invalid request body format"));
			return Response.status(Status.BAD_REQUEST.getStatusCode()).entity("Invalid request body format").build();
		} catch (CustomWebServiceException e) { 
			log.info(String.format("Error en web service de creacion de institution_security: %d, %s", e.getHttpStatusCode(), e.getMessage()));
			return Response.status(e.getHttpStatusCode()).entity(e.getMessage()).build();
		} catch (Exception e) {
			log.info("Error 500 en web service de creacion de institution_security");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Internal server error").build();
		}
	}
	
	@POST
	@Path("/update")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateInstitutionSecurity(Object dataObject) throws ServiceException{
		log.info("Inicio web service de update de institution_security");		
		try { 
			Gson gson = new Gson();
			InstitutionTO req = gson.fromJson(gson.toJson(dataObject), InstitutionTO.class);
			
			institutionSecurityServiceBean.wsUpdateInstituionSecurityService(req);
			log.info("Finalizó correctamente web service de update de institution_security");		
			return Response.status(Status.OK.getStatusCode()).entity("Success").build();
		} catch (JsonSyntaxException e) {
			log.info(String.format("Error en web service de update de institution_security: %d, %s", Status.BAD_REQUEST.getStatusCode(), "Invalid request body format"));
			return Response.status(Status.BAD_REQUEST.getStatusCode()).entity("Invalid request body format").build();
		} catch (CustomWebServiceException e) { 
			log.info(String.format("Error en web service de update de institution_security: %d, %s", e.getHttpStatusCode(), e.getMessage()));
			return Response.status(e.getHttpStatusCode()).entity(e.getMessage()).build();
		} catch (Exception e) {
			log.info("Error 500 en web service de update de institution_security");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Internal server error").build();
		}
	}
}
