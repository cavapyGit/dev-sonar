package com.pradera.security.systemmgmt.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.security.model.System;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class SystemDataModel
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public class SystemDataModel extends ListDataModel<System> implements Serializable,SelectableDataModel<System>{


	/**
	 * 
	 */
	private static final long serialVersionUID = 3247482369176354218L;
	private int size;
	
	
	/**
	 * Instantiates a new system data model.
	 */
	public SystemDataModel(){
		super();
	}
	
	/**
	 * Instantiates a new system data model.
	 *
	 * @param data the data
	 */
	public SystemDataModel(List<System> data){
		super(data);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public System getRowData(String rowKey) {
		// TODO Auto-generated method stub
		List<System> systems=(List<System>)getWrappedData();
        for(System system : systems) {  
            if(String.valueOf(system.getIdSystemPk()).equals(rowKey))
                return system;  
        }  
		return null;
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(System system) {
		// TODO Auto-generated method stub
		return system.getIdSystemPk();
	}

	/**
	 * Gets the size.
	 *
	 * @return the size
	 */
	public int getSize() {
		List<System> systems=(List<System>)getWrappedData();
		if(systems==null)
			return 0;
		return systems.size();
	}

	/**
	 * Sets the size.
	 *
	 * @param size the new size
	 */
	public void setSize(int size) {
		this.size = size;
	}
	
	
}
