package com.pradera.security.systemmgmt.view.filters;

import java.io.Serializable;

import com.pradera.integration.common.validation.Validations;
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class SystemFilter
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.1
 */
public class SystemFilter implements Serializable{

	private String systemName;
	private String systemMnemonic;
	private Integer idSystemPk;
	private Integer systemState;
	private String systemCode;
	
	/**
	 * Constructor
	 */
	public SystemFilter(){
		
	}

	/**
	 * @return systemName
	 */
	public String getSystemName() {
		return systemName;
	}

	/**
	 * @param systemName
	 */
	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	/**
	 * @return systemMnemonic
	 */
	public String getSystemMnemonic() {
		return systemMnemonic;
	}

	/**
	 * @param systemMnemonic
	 */
	public void setSystemMnemonic(String systemMnemonic) {
		if(systemMnemonic != null){
			systemMnemonic = systemMnemonic.toUpperCase();
		}
		this.systemMnemonic = systemMnemonic;
	}

	/**
	 * @return idSystemPk
	 */
	public Integer getIdSystemPk() {
		/*if(idSystemPk !=null && idSystemPk == 0){
			return null;
		}*/
		return idSystemPk;
	}

	/**
	 * @param idSystemPk
	 */
	public void setIdSystemPk(Integer idSystemPk) {
		this.idSystemPk = idSystemPk;
	}

	/**
	 * @return systemState
	 */
	public Integer getSystemState() {
		return systemState;
	}

	/**
	 * @param systemState
	 */
	public void setSystemState(Integer systemState) {
		this.systemState = systemState;
	}

	/**
	 * @return the systemCode
	 */
	public String getSystemCode() {
		return systemCode;
	}

	/**
	 * @param systemCode the systemCode to set
	 */
	public void setSystemCode(String systemCode) {
		if(Validations.validateIsNotNullAndNotEmpty(systemCode)){
		setIdSystemPk(Integer.valueOf(systemCode));
		}else{
			setIdSystemPk(null);
		}
		this.systemCode = systemCode;
	}
	
}
