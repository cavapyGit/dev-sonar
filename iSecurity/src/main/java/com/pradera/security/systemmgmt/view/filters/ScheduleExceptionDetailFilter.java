package com.pradera.security.systemmgmt.view.filters;

import java.io.Serializable;
import java.util.Date;

import com.pradera.security.usermgmt.view.filters.UserAccountFilter;

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class ScheduleExceptionDetailFilter
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.1
 */
public class ScheduleExceptionDetailFilter implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id schedule exception detail. */
	private Integer idScheduleExceptionDetail;
	
	/** The description. */
	private String description;
	
	/** The id state. */
	private Integer scheduleExceptionDetailState;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/** The usuario filtro. */
	private UserAccountFilter userFilter;
		
	/**
	 * The Constructor.
	 */
	public ScheduleExceptionDetailFilter() {
		super();
	}

	public UserAccountFilter getUserFilter() {
		return userFilter;
	}

	public void setUserFilter(UserAccountFilter userFilter) {
		this.userFilter = userFilter;
	}

	/**
	 * Gets the id schedule exception detail.
	 *
	 * @return the id schedule exception detail
	 */
	public Integer getIdScheduleExceptionDetail() {
		return idScheduleExceptionDetail;
	}
	
	/**
	 * Sets the id schedule exception detail.
	 *
	 * @param idScheduleExceptionDetail the id schedule exception detail
	 */
	public void setIdScheduleExceptionDetail(Integer idScheduleExceptionDetail) {
		this.idScheduleExceptionDetail = idScheduleExceptionDetail;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description.
	 *
	 * @param description the description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getScheduleExceptionDetailState() {
		return scheduleExceptionDetailState;
	}

	public void setScheduleExceptionDetailState(Integer scheduleExceptionDetailState) {
		this.scheduleExceptionDetailState = scheduleExceptionDetailState;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}
	
	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	
	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}
	
	/**
	 * Sets the final date.
	 *
	 * @param finalDate the final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}
}