package com.pradera.security.systemmgmt.service;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.model.CatalogueLanguage;
import com.pradera.security.model.OptionPrivilege;
import com.pradera.security.model.Parameter;
import com.pradera.security.model.SystemOption;
import com.pradera.security.model.type.OptionPrivilegeStateType;
import com.pradera.security.model.type.OptionStateType;
import com.pradera.security.systemmgmt.view.filters.SystemFilter;
import com.pradera.security.systemmgmt.view.filters.SystemProfileFilter;
import com.pradera.security.systemmgmt.view.filters.UserExceptionFilter;
import com.pradera.security.usermgmt.facade.UserServiceFacade;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class SystemOptionServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 13/03/2013
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class SystemOptionServiceBean extends CrudSecurityServiceBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = -807775756653999889L;

	/**
	 * Instantiates a new system option service bean.
	 */
	public SystemOptionServiceBean() {
		// TODO Auto-generated constructor stub
	}
	
	@Inject
	UserServiceFacade userServiceFacade;
	
	/**
	 * List system option by system filter service bean.
	 *
	 * @param systemFilterParam the system filter param
	 * @return the list system Options
	 * @throws ServiceException the service exception
	 * @author mmacalupu
	 * List system option by system filter service bean.
	 * this method is useful to get the menu from system's Id
	 */
	@SuppressWarnings("unchecked")
	public List<SystemOption> listSystemOptionBySystemFilterServiceBean(SystemFilter systemFilterParam) throws ServiceException{
		StringBuilder stringBuilderSql = new StringBuilder();
		stringBuilderSql.append("Select o From SystemOption o LEFT JOIN FETCH o.catalogueLanguages WHERE o.system.idSystemPk = :idSystemPrm and o.state = ");
		stringBuilderSql.append(":stateConfirmPrm ORDER BY o.idSystemOptionPk ASC");
		Query query = em.createQuery(stringBuilderSql.toString());
		query.setParameter("idSystemPrm", systemFilterParam.getIdSystemPk());
		query.setParameter("stateConfirmPrm",OptionStateType.CONFIRMED.getCode());
		query.setHint("org.hibernate.cacheable", true);
	    return ((List<SystemOption>)query.getResultList());
	}
	
	/**
	 * Register system option service bean.
	 *
	 * @param os the os
	 * @return the system option
	 * @throws ServiceException the service exception
	 */
	public SystemOption registerSystemOptionServiceBean(SystemOption os)throws ServiceException{
		em.persist(os);
		em.flush();
	   return os;	 
	}
	
	/**
	 * Search system option by id service bean.
	 *
	 * @param idOption the id option
	 * @return the system option
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SystemOption> searchSystemOptionByIdServiceBean(Integer idOption) throws ServiceException{
		Map<String,Object> parameters = new HashMap<String, Object>();
    	parameters.put("idOptionPrm", idOption);
    
    	return (List<SystemOption>)findWithNamedQuery(SystemOption.SYSTEMOPTION_SEARCH_BY_ID, parameters);
	}
	
	/**
	 * @param idOption
	 * @return
	 * @throws ServiceException
	 */
	public Date getSystemOptionsLastModfiedDetails(Integer idOption)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select so.lastModifyDate From SystemOption so Where so.idSystemOptionPk = :idSystemOptionPk");		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSystemOptionPk",idOption);
		query.setHint("org.hibernate.cacheable", true);
		return (Date)query.getSingleResult(); 
	}
	
	public SystemOption findSystemOptionsDetails(Integer idOption)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select o From SystemOption o inner join fetch o.system WHERE o.idSystemOptionPk = :idOptionPrm");		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idOptionPrm",idOption);
		return (SystemOption)query.getSingleResult(); 
	}
	/**
	 * Search privileges by definition service bean.
	 *
	 * @param idDefinition the id definition
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Parameter> searchPrivilegesByDefinitionServiceBean(Integer idDefinition) throws ServiceException{
		Query query = em.createQuery("Select o From Parameter o WHERE o.definition.idDefinitionPk = :idDefinitionPrm order by o.idParameterCode");
		query.setParameter("idDefinitionPrm", idDefinition);	
		query.setHint("org.hibernate.cacheable", true);
		return (List<Parameter>)query.getResultList();
	}
	
	/**
	 * List option privileges service bean.
	 *
	 * @param systemProfileFilter the system profile filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> listOptionPrivilegesServiceBean(SystemProfileFilter systemProfileFilter) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select OP.idOptionPrivilegePk, OP.idPrivilege, SO.idSystemOptionPk ");		
		sbQuery.append(" From SystemOption SO ");
		sbQuery.append(" Join SO.optionPrivileges OP ");		
		sbQuery.append(" Where OP.state = :optionPrivilegeStatePrm And SO.state = :systemOptionStatePrm ");
		sbQuery.append(" AND SO.system.idSystemPk = :idSystemPrm");	
		sbQuery.append(" Order By SO.idSystemOptionPk, OP.idPrivilege ");
		
		Query query = em.createQuery(sbQuery.toString());
		
		query.setParameter("optionPrivilegeStatePrm",systemProfileFilter.getOptionPrivilegeState());
		query.setParameter("systemOptionStatePrm",systemProfileFilter.getSystemOptionState());
		query.setParameter("idSystemPrm", systemProfileFilter.getIdSystem());
		query.setHint("org.hibernate.cacheable", true);
		return (List<Object[]>) query.getResultList();			   
	}
	 
	/**
	 * List option systems by language service bean.
	 *
	 * @param systemProfileFilter the system profile filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> listOptionSystemsByLanguageServiceBean(SystemProfileFilter systemProfileFilter) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select SO.idSystemOptionPk, SO.parentSystemOption.idSystemOptionPk, CL.catalogueName ");
		sbQuery.append(" From SystemOption SO");
		sbQuery.append(" Join SO.catalogueLanguages CL");		
		sbQuery.append(" Where CL.language = :languagePrm ");
		sbQuery.append(" And SO.state = :systemOptionStatePrm AND SO.system.idSystemPk = :idSystemPrm");		
		sbQuery.append(" Order By SO.parentSystemOption.idSystemOptionPk, SO.orderOption ");
		
		Query query = em.createQuery(sbQuery.toString());
			
		query.setParameter("languagePrm",systemProfileFilter.getIdLanguage());
		query.setParameter("systemOptionStatePrm",systemProfileFilter.getSystemOptionState());
		query.setParameter("idSystemPrm", systemProfileFilter.getIdSystem());
		query.setHint("org.hibernate.cacheable", true);
		return (List<Object[]>) query.getResultList();	    
	}
	
	/**
	 * List option catalogue privileges by system service bean.
	 *
	 * @param idSystem the id system
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> listOptionCataloguePrivilegesBySystemServiceBean(Integer idSystem) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select OS.idSystemOptionPk,OS.parentSystemOption.idSystemOptionPk, CI.catalogueName, OS.urlOption, OS.state, CI.language, OS.optionType, OS.orderOption,OS.lastModifyDate,OS.situation");
		sbQuery.append(" From SystemOption OS");
		sbQuery.append(" Join OS.catalogueLanguages CI");		
		sbQuery.append(" Where OS.system.idSystemPk = :idSystemPrm and OS.state != :deletedStatePrm and OS.state != :rejectedStatePrm ");
		sbQuery.append(" Order By OS.parentSystemOption.idSystemOptionPk, OS.orderOption ");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSystemPrm",idSystem);
		query.setParameter("deletedStatePrm", OptionStateType.DELETED.getCode());
		query.setParameter("rejectedStatePrm", OptionStateType.REJECTED.getCode());
		query.setHint("org.hibernate.cacheable", true);
		return (List<Object[]>) query.getResultList();
	}
	
	/**
	 * Search node child by option service bean.
	 *
	 * @param idOption the id option
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SystemOption> searchNodeChildByOptionServiceBean(Integer idOption) throws ServiceException{
		 Query query = em.createQuery("Select o From SystemOption o WHERE o.parentSystemOption.idSystemOptionPk = :idOptionPrm and o.state != :deleteState and o.state != :rejectState");
	     query.setParameter("idOptionPrm", idOption);
	     query.setParameter("deleteState", OptionStateType.DELETED.getCode());
		 query.setParameter("rejectState", OptionStateType.REJECTED.getCode());
		 query.setHint("org.hibernate.cacheable", true);
	     return  (List<SystemOption>)query.getResultList();
		    
	 }
	 
	 /**
 	 * List id system optios by option privilege service bean.
 	 *
 	 * @param systemProfilePrivilege the system profile privilege
 	 * @return the list
 	 * @throws ServiceException the service exception
 	 */
 	public List<Integer> listIdSystemOptiosByOptionPrivilegeServiceBean(SystemProfileFilter systemProfilePrivilege) throws ServiceException {
 			Map<String,List<Integer>> mapSubList = new HashMap<>();
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" Select Distinct SO.idSystemOptionPk");
			sbQuery.append(" From SystemOption SO ");
			sbQuery.append(" Join SO.optionPrivileges OP ");		
			sbQuery.append(" Where SO.state = :systemOptionStatePrm And SO.system.idSystemPk = :idSystemPrm");	
			if(systemProfilePrivilege.getLstIdOptionPrivilege() != null && systemProfilePrivilege.getLstIdOptionPrivilege().size() > 999){
				BigDecimal totalList = new BigDecimal(""+systemProfilePrivilege.getLstIdOptionPrivilege().size());
				BigDecimal limit = new BigDecimal(""+1000);
				int total = totalList.divide(limit, BigDecimal.ROUND_CEILING).intValue();
				int initial_from=0;
				int step_size = 1000;
				int max_range=1000;
				for(int index=0;index<total;index++){
					List<Integer> optionsSplit =  systemProfilePrivilege.getLstIdOptionPrivilege().subList(initial_from, max_range);
					mapSubList.put("param"+index, optionsSplit);
					initial_from = initial_from +step_size;
					max_range=step_size+max_range;
					if(max_range > systemProfilePrivilege.getLstIdOptionPrivilege().size()){
						max_range=systemProfilePrivilege.getLstIdOptionPrivilege().size();
					}
				}
			} else {
				mapSubList.put("param", systemProfilePrivilege.getLstIdOptionPrivilege());
			}
			Set<Entry<String, List<Integer>>> keyValues=  mapSubList.entrySet();
			Iterator<Entry<String, List<Integer>>>  itePSubList= keyValues.iterator();
			sbQuery.append(" and( ");
			while(itePSubList.hasNext()){
				Entry<String, List<Integer>> entry = itePSubList.next();
				sbQuery.append(" OP.idOptionPrivilegePk in (:"+entry.getKey()+")");
				if(itePSubList.hasNext()){
					sbQuery.append(" OR  ");
				}
			}
			sbQuery.append(" ) ");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("systemOptionStatePrm",systemProfilePrivilege.getSystemOptionState());
			query.setParameter("idSystemPrm", systemProfilePrivilege.getIdSystem());
			for(Entry<String, List<Integer>> entry : keyValues){
				query.setParameter(entry.getKey(), entry.getValue());
			}
			query.setHint("org.hibernate.cacheable", true);
			return (List<Integer>) query.getResultList();					    
	}
	 
	 /**
 	 * List system options by ids service bean.
 	 *
 	 * @param systemProfileFilter the system profile filter
 	 * @return the list
 	 * @throws ServiceException the service exception
 	 */
 	public List<Object[]> listSystemOptionsByIdsServiceBean(SystemProfileFilter systemProfileFilter) throws ServiceException{
			StringBuilder sbQuery = new StringBuilder();
			//Select recursivo para obtener los menus padres de cada menu hijo
			sbQuery.append(" SELECT DISTINCT T.ID_SYSTEM_OPTION_PK, T.ID_OPTION_FATHER, T.CATALOGUE_NAME, T.ORDER_OPTION, ");
			sbQuery.append(" T.URL_OPTION, T.OPTION_TYPE, T.LANGUAGE, T.MODULE ");
			sbQuery.append(" FROM ( ");
			sbQuery.append(" SELECT Distinct ");
			sbQuery.append(" SUBSTR(SYS_CONNECT_BY_PATH(OS.ID_SYSTEM_OPTION_PK, '\\') || '\\', ");
			sbQuery.append(" (INSTR(SYS_CONNECT_BY_PATH(OS.ID_SYSTEM_OPTION_PK, '\\')|| '\\','\\',1,1)+1), ");
			sbQuery.append(" (INSTR(SYS_CONNECT_BY_PATH(OS.ID_SYSTEM_OPTION_PK, '\\')|| '\\','\\',1,2))-2) AS ID_SYSTEM_OPTION_PK, ");
			sbQuery.append(" SUBSTR(SYS_CONNECT_BY_PATH(OS.ID_OPTION_FATHER, '\\') || '\\', ");
			sbQuery.append(" (INSTR(SYS_CONNECT_BY_PATH(OS.ID_OPTION_FATHER, '\\')|| '\\','\\',1,1)+1), ");
			sbQuery.append(" (INSTR(SYS_CONNECT_BY_PATH(OS.ID_OPTION_FATHER, '\\')|| '\\','\\',1,2))-2) AS ID_OPTION_FATHER, ");			
			sbQuery.append(" SUBSTR(SYS_CONNECT_BY_PATH(OS.ORDER_OPTION, '\\') || '\\', ");
			sbQuery.append(" (INSTR(SYS_CONNECT_BY_PATH(OS.ORDER_OPTION, '\\')|| '\\','\\',1,1)+1), ");
			sbQuery.append(" (INSTR(SYS_CONNECT_BY_PATH(OS.ORDER_OPTION, '\\')|| '\\','\\',1,2))-2) as ORDER_OPTION, ");
			sbQuery.append(" SUBSTR(SYS_CONNECT_BY_PATH(OS.URL_OPTION, '\\') || '\\', ");
			sbQuery.append(" (INSTR(SYS_CONNECT_BY_PATH(OS.URL_OPTION, '\\')|| '\\','\\',1,1)+1), ");
			sbQuery.append(" (INSTR(SYS_CONNECT_BY_PATH(OS.URL_OPTION, '\\')|| '\\','\\',1,2))-2) as URL_OPTION, ");
			sbQuery.append(" SUBSTR(SYS_CONNECT_BY_PATH(OS.OPTION_TYPE, '\\') || '\\', ");
			sbQuery.append(" (INSTR(SYS_CONNECT_BY_PATH(OS.OPTION_TYPE, '\\')|| '\\','\\',1,1)+1), ");
			sbQuery.append(" (INSTR(SYS_CONNECT_BY_PATH(OS.OPTION_TYPE, '\\')|| '\\','\\',1,2))-2) as OPTION_TYPE, ");
			sbQuery.append(" SUBSTR(SYS_CONNECT_BY_PATH(OS.MODULE, '\\') || '\\', ");
			sbQuery.append(" (INSTR(SYS_CONNECT_BY_PATH(OS.MODULE, '\\')|| '\\','\\',1,1)+1), ");
			sbQuery.append(" (INSTR(SYS_CONNECT_BY_PATH(OS.MODULE, '\\')|| '\\','\\',1,2))-2) as MODULE, ");
			sbQuery.append(" SUBSTR(SYS_CONNECT_BY_PATH(CI.LANGUAGE, '\\') || '\\', ");
			sbQuery.append(" (INSTR(SYS_CONNECT_BY_PATH(CI.LANGUAGE, '\\')|| '\\','\\',1,1)+1), ");
			sbQuery.append(" (INSTR(SYS_CONNECT_BY_PATH(CI.LANGUAGE, '\\')|| '\\','\\',1,2))-2) as LANGUAGE, ");
			sbQuery.append(" SUBSTR(SYS_CONNECT_BY_PATH(CI.CATALOGUE_NAME, '\\') || '\\', ");
			sbQuery.append(" (INSTR(SYS_CONNECT_BY_PATH(CI.CATALOGUE_NAME, '\\')|| '\\','\\',1,1)+1), ");
			sbQuery.append(" (INSTR(SYS_CONNECT_BY_PATH(CI.CATALOGUE_NAME, '\\')|| '\\','\\',1,2))-2) AS CATALOGUE_NAME, ");
			sbQuery.append(" LEVEL ");
			sbQuery.append(" FROM SYSTEM_OPTION OS ");
			sbQuery.append(" INNER JOIN CATALOGUE_LANGUAGE CI ON OS.ID_SYSTEM_OPTION_PK = CI.ID_SYSTEM_OPTION_FK ");
			sbQuery.append(" AND CI.LANGUAGE = :languagePrm ");
			sbQuery.append(" WHERE OS.ID_SYSTEM_FK = :idSystemPrm AND OS.STATE = :systemOptionStatePrm "); 
			sbQuery.append(" AND OS.ID_SYSTEM_OPTION_PK IN (:lstIdSystemOptionPrm) ");
			sbQuery.append(" CONNECT BY PRIOR OS.ID_SYSTEM_OPTION_PK = OS.ID_OPTION_FATHER  ");			
			sbQuery.append(" ) T ");
			sbQuery.append(" ORDER BY T.ID_OPTION_FATHER, T.ORDER_OPTION ");
			
			Query query = em.createNativeQuery(sbQuery.toString());
				
			query.setParameter("languagePrm",systemProfileFilter.getIdLanguage());
			query.setParameter("systemOptionStatePrm",systemProfileFilter.getSystemOptionState());
			query.setParameter("idSystemPrm", systemProfileFilter.getIdSystem());
			query.setParameter("lstIdSystemOptionPrm", systemProfileFilter.getLstIdSystemOption());
		
			return (List<Object[]>) query.getResultList();	    
	}
	 
	 /**
 	 * List privilege catalogue by option service bean.
 	 *
 	 * @param idOption the id option
 	 * @return the list
 	 * @throws ServiceException the service exception
 	 */
 	@SuppressWarnings("unchecked")
	public List<Object[]> listPrivilegeCatalogueByOptionServiceBean(Integer idOption) throws ServiceException{
		    StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" Select OS.idSystemOptionPk, OS.parentSystemOption.idSystemOptionPk, OS.urlOption, OS.state, OS.optionType,");
			sbQuery.append(" CI.language, CI.catalogueName, CI.description, CI.idCatalogueLanguagePk, ");
			sbQuery.append(" PO.idPrivilege, PO.idOptionPrivilegePk, OS.blockMotive, OS.unblockMotive, ");
			sbQuery.append(" OS.blockOtherMotive, OS.unblockOtherMotive,OS.documentFileName, OS.documentFilePath ");
			sbQuery.append(" From SystemOption OS");
			sbQuery.append(" Join OS.catalogueLanguages CI");
			sbQuery.append(" Join OS.optionPrivileges PO");
			sbQuery.append(" Where OS.idSystemOptionPk = :idSystemOption and OS.state != :deletedState and OS.state != :rejectState ");
			sbQuery.append(" and CI.systemOption.idSystemOptionPk = :idSystemOption and PO.systemOption.idSystemOptionPk = :idSystemOption and PO.state != :deleteStatePrivilege");
			sbQuery.append(" Order By OS.parentSystemOption.idSystemOptionPk, OS.orderOption ");
					
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idSystemOption",idOption);
			query.setParameter("deletedState", OptionStateType.DELETED.getCode());
			query.setParameter("rejectState", OptionStateType.REJECTED.getCode());
			query.setParameter("deleteStatePrivilege", OptionPrivilegeStateType.DELETED.getCode());
			query.setHint("org.hibernate.cacheable", true);
			return (List<Object[]>) query.getResultList();
			
	 }
	 
	 /**
 	 * List catalogue by option service bean.
 	 *
 	 * @param idOpcion the id opcion
 	 * @return the list
 	 * @throws ServiceException the service exception
 	 */
 	@SuppressWarnings("unchecked")
	public List<Object[]> listCatalogueByOptionServiceBean(Integer idOpcion) throws ServiceException{
		    
		    StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" Select OS.idSystemOptionPk, OS.parentSystemOption.idSystemOptionPk, OS.urlOption, OS.state, OS.optionType,");
			sbQuery.append(" CI.language, CI.catalogueName, CI.description, CI.idCatalogueLanguagePk, OS.blockMotive, OS.unblockMotive, ");
			sbQuery.append(" OS.blockOtherMotive, OS.unblockOtherMotive ");
			sbQuery.append(" From SystemOption OS");
			sbQuery.append(" Join OS.catalogueLanguages CI");
			sbQuery.append(" Where OS.idSystemOptionPk = :idSystemOptionPrm and OS.state != :deletedStatePrm and OS.state != :rejectedStatePrm ");
			sbQuery.append(" Order By OS.parentSystemOption.idSystemOptionPk, OS.orderOption ");
					
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idSystemOptionPrm",idOpcion);
			query.setParameter("deletedStatePrm", OptionStateType.DELETED.getCode());
			query.setParameter("rejectedStatePrm", OptionStateType.REJECTED.getCode());
			query.setHint("org.hibernate.cacheable", true);
			return (List<Object[]>) query.getResultList();
			 
	 }
	 
	 /**
 	 * Search option privilege by id service bean.
 	 *
 	 * @param idPrivilege the id privilege
 	 * @param idOption the id option
 	 * @return the option privilege
 	 * @throws ServiceException the service exception
 	 */
 	@SuppressWarnings("unchecked")
	public List<OptionPrivilege> searchOptionPrivilegeByIdServiceBean(Integer idPrivilege, Integer idOption) throws ServiceException{
			Map<String,Object> parameters = new HashMap<String, Object>();
	    	parameters.put("idPrivilege", idPrivilege);
	    	parameters.put("idOption", idOption);
	    	
	    	return (List<OptionPrivilege>)findWithNamedQuery(OptionPrivilege.OPTION_PRIVILEGE, parameters);
	 }
	 
 	/**
 	 * Search option privilege by id service bean.
 	 *
 	 * @param idPrivilege the id privilege
 	 * @param idOption the id option
 	 * @return the option privilege
 	 * @throws ServiceException the service exception
 	 */
 	@SuppressWarnings("unchecked")
	public List<OptionPrivilege> searchOptionPrivilegeByIdPk(List<Integer> idPrivilegePks) throws ServiceException{
 		String sql = "select optionPrivilege from OptionPrivilege optionPrivilege where optionPrivilege.idOptionPrivilegePk in (:idPrivilegePks)";
 		Map<String,Object> parameters = new HashMap<String, Object>();
    	parameters.put("idPrivilegePks", idPrivilegePks);
 		return (List<OptionPrivilege>)findByQueryString(sql,parameters);
 	}

	 /**
 	 * Search catalogue language by id service bean.
 	 *
 	 * @param idCatalogue the id catalogue
 	 * @return the List of catalogue language
 	 * @throws ServiceException the service exception
 	 */
 	@SuppressWarnings("unchecked")
	public List<CatalogueLanguage> searchCatalogueLanguageByIdServiceBean(Integer idCatalogue) throws ServiceException{
			Map<String,Object> parameters = new HashMap<String, Object>();
	    	parameters.put("idCatalogueLanguage", idCatalogue);
	    	return (List<CatalogueLanguage>)findWithNamedQuery(CatalogueLanguage.LANGUAGE_CATALOGUE, parameters);
	}
 
//	 public boolean deletePrivilegeServiceBean(List<OptionPrivilege> listaPrivilegios)throws ServiceException{
//		boolean flag=false;
//		StringBuilder sbQuery = new StringBuilder();
//		sbQuery.append(" Delete From OptionPrivilege po");	
//		sbQuery.append(" Where po.systemOption.idSystemOptionPk = :idSystemOptionPrm and po.state != :deletedStatePrm and po.state != :rejectedStatePrm ");
//				
//		Query query = em.createQuery(sbQuery.toString());
//		query.setParameter("idSystemOptionPrm",idOption);
//		query.setParameter("deletedStatePrm", OptionStateType.DELETED.getCode());
//		query.setParameter("rejectedStatePrm", OptionStateType.REJECTED.getCode());
//		query.executeUpdate();
//		
//		flag=true;
//		return flag;
//	 }
	 
	 /**
 * List system options by id service bean.
 *
 * @param userExceptionFilterParam the user exception filter param
 * @return the list
 * @throws ServiceException the service exception
 */
public List<Object[]> listSystemOptionsByIdServiceBean(UserExceptionFilter userExceptionFilterParam) throws ServiceException{
			StringBuilder sbQuery = new StringBuilder();
			//Select recursivo para obtener los menus padres de cada menu hijo
			sbQuery.append(" SELECT DISTINCT T.ID_SYSTEM_OPTION_PK, T.ID_OPTION_FATHER, T.CATALOGUE_NAME, T.ORDER_OPTION ");
			sbQuery.append(" FROM ( ");
			sbQuery.append(" SELECT Distinct ");
			sbQuery.append(" SUBSTR(SYS_CONNECT_BY_PATH(OS.ID_SYSTEM_OPTION_PK, '\\') || '\\', ");
			sbQuery.append(" (INSTR(SYS_CONNECT_BY_PATH(OS.ID_SYSTEM_OPTION_PK, '\\')|| '\\','\\',1,1)+1), ");
			sbQuery.append(" (INSTR(SYS_CONNECT_BY_PATH(OS.ID_SYSTEM_OPTION_PK, '\\')|| '\\','\\',1,2))-2) AS ID_SYSTEM_OPTION_PK, ");
			sbQuery.append(" SUBSTR(SYS_CONNECT_BY_PATH(OS.ID_OPTION_FATHER, '\\') || '\\', ");
			sbQuery.append(" (INSTR(SYS_CONNECT_BY_PATH(OS.ID_OPTION_FATHER, '\\')|| '\\','\\',1,1)+1), ");
			sbQuery.append(" (INSTR(SYS_CONNECT_BY_PATH(OS.ID_OPTION_FATHER, '\\')|| '\\','\\',1,2))-2) AS ID_OPTION_FATHER, ");			
			sbQuery.append(" SUBSTR(SYS_CONNECT_BY_PATH(OS.ORDER_OPTION, '\\') || '\\', ");
			sbQuery.append(" (INSTR(SYS_CONNECT_BY_PATH(OS.ORDER_OPTION, '\\')|| '\\','\\',1,1)+1), ");
			sbQuery.append(" (INSTR(SYS_CONNECT_BY_PATH(OS.ORDER_OPTION, '\\')|| '\\','\\',1,2))-2) as ORDER_OPTION, ");			
			sbQuery.append(" SUBSTR(SYS_CONNECT_BY_PATH(CI.CATALOGUE_NAME, '\\') || '\\', ");
			sbQuery.append(" (INSTR(SYS_CONNECT_BY_PATH(CI.CATALOGUE_NAME, '\\')|| '\\','\\',1,1)+1), ");
			sbQuery.append(" (INSTR(SYS_CONNECT_BY_PATH(CI.CATALOGUE_NAME, '\\')|| '\\','\\',1,2))-2) AS CATALOGUE_NAME, ");
			sbQuery.append(" LEVEL AS NIVEL ");
			sbQuery.append(" FROM SYSTEM_OPTION OS ");
			sbQuery.append(" INNER JOIN CATALOGUE_LANGUAGE CI ON OS.ID_SYSTEM_OPTION_PK = CI.ID_SYSTEM_OPTION_FK ");
			sbQuery.append(" AND CI.LANGUAGE = :languagePrm ");
			sbQuery.append(" WHERE OS.ID_SYSTEM_FK = :idSystemPrm "); 
			sbQuery.append(" AND OS.ID_SYSTEM_OPTION_PK IN (:listOptionsPrm) ");
			sbQuery.append(" CONNECT BY PRIOR OS.ID_SYSTEM_OPTION_PK = OS.ID_OPTION_FATHER  ");			
			sbQuery.append(" ) T ");
			sbQuery.append(" ORDER BY T.ID_OPTION_FATHER, T.ORDER_OPTION ");
			
			Query query = em.createNativeQuery(sbQuery.toString());
				
			query.setParameter("languagePrm",userExceptionFilterParam.getUserExceptionLanguage());			
			query.setParameter("idSystemPrm", userExceptionFilterParam.getIdSystemSelected());
			query.setParameter("listOptionsPrm", userExceptionFilterParam.getOptionsListPK());
			
			@SuppressWarnings("unchecked")
			List<Object[]> listOptions = (List<Object[]>) query.getResultList();		
		    	return listOptions;		    
	}
	 
	 /**
 	 * List id system options by privilege service bean.
 	 *
 	 * @param userExceptionFilter the user exception filter
 	 * @return the list
 	 * @throws ServiceException the service exception
 	 */
 	public List<Integer> listIdSystemOptionsByPrivilegeServiceBean(UserExceptionFilter userExceptionFilter) throws ServiceException {
 		List<Integer> privileges = userExceptionFilter.getPrivelegeListPK();
 		int chunkSize = 1000; // Define the size of each chunk
 	    
 	    List<Integer> resultList = new ArrayList<>();
 	    
 	   for (int i = 0; i < privileges.size(); i += chunkSize) {
 	        int endIndex = Math.min(i + chunkSize, privileges.size());
 	        List<Integer> sublist = privileges.subList(i, endIndex);
 	        
 	        StringBuilder sbQuery = new StringBuilder();
 	        sbQuery.append("SELECT DISTINCT OS.idSystemOptionPk");
 	        sbQuery.append(" FROM SystemOption OS");
 	        sbQuery.append(" JOIN OS.optionPrivileges PO ");        
 	        sbQuery.append(" WHERE OS.system.idSystemPk = :idSystemPrm");    
 	        sbQuery.append(" AND PO.idOptionPrivilegePk IN (:listPrivilegesPrm)");
 	        
 	        Query query = em.createQuery(sbQuery.toString());
 	        query.setParameter("idSystemPrm", userExceptionFilter.getIdSystemSelected());
 	        query.setParameter("listPrivilegesPrm", sublist);
 	        query.setHint("org.hibernate.cacheable", true);

 	        @SuppressWarnings("unchecked")
 	        List<Integer> options = (List<Integer>) query.getResultList();
 	        
 	        resultList.addAll(options);
 	    }
 	    
 	    return resultList; 	    
	}
	 
	 /**
 	 * Update order option byid system option.
 	 *
 	 * @param idSystemOption the id system option
 	 * @param orderOption the order option
 	 * @return the int
 	 * @throws ServiceException the service exception
 	 */
	public int updateOrderOptionByidSystemOption(Integer idSystemOption,Integer orderOption) throws ServiceException {
		 StringBuilder sbQuery = new StringBuilder();
		 sbQuery.append(" Update SystemOption so");
		 sbQuery.append(" set so.orderOption = so.orderOption+1");
		 sbQuery.append(" where so.parentSystemOption.idSystemOptionPk = :idSystemOptionprm And so.orderOption >= :orderOptionprm");

		 Query query = em.createQuery(sbQuery.toString());
		 
		 query.setParameter("idSystemOptionprm", idSystemOption);
		 query.setParameter("orderOptionprm", orderOption);
		 
		 return query.executeUpdate();
	 }
	
	/**
	 * Update order option byid system pk.
	 *
	 * @param idSystemPk the id system pk
	 * @param orderOption the order option
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int updateOrderOptionByidSystemPk(Integer idSystemPk,Integer orderOption) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		 sbQuery.append(" Update SystemOption so");
		 sbQuery.append(" set so.orderOption = so.orderOption+1");
		 sbQuery.append(" where so.system.idSystemPk = :idSystempkprm And so.orderOption >= :orderOptionprm And so.state = 1 and so.parentSystemOption.idSystemOptionPk is null");

		 Query query = em.createQuery(sbQuery.toString());
		 query.setParameter("idSystempkprm", idSystemPk);
		 query.setParameter("orderOptionprm", orderOption);
		 
		 return query.executeUpdate();
	
	}


	/**
	 * To get the option id with the given orderOption
	 * @param systemidpk
	 * @param orderOption
	 * @param optionidPk
	 * @param parentIdPk
	 * @return option id if exists else null
	 * @throws ServiceException
	 */
	public Integer getSystemOptionByOrderOptionServiceBean(
			Integer systemidpk,Integer orderOption,Integer optionidPk,Integer parentIdPk)throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select os.idSystemOptionPk from SystemOption os where os.orderOption = :orderOption and os.state != :state");
		
		if(Validations.validateIsNotNullAndNotEmpty(systemidpk) ){
			sbQuery.append(" and os.system.idSystemPk = :idSystempk");
		}
		if(Validations.validateIsNotNullAndNotEmpty(optionidPk) ){
			sbQuery.append(" and idSystemOptionPk != :idSystemOption");
		}
		if(Validations.validateIsNotNullAndNotEmpty(parentIdPk) ){
			sbQuery.append(" and parentSystemOption.idSystemOptionPk = :parentIdPk");
		}else{
			sbQuery.append(" and parentSystemOption.idSystemOptionPk is null");
		}
			
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("orderOption",orderOption);
		query.setParameter("state",OptionStateType.DELETED.getCode());
		if(Validations.validateIsNotNullAndNotEmpty(systemidpk) ){
			query.setParameter("idSystempk",systemidpk);
		}
		if(Validations.validateIsNotNullAndNotEmpty(optionidPk) ){
			query.setParameter("idSystemOption", optionidPk);
			}
		if(Validations.validateIsNotNullAndNotEmpty(parentIdPk) ){
			query.setParameter("parentIdPk", parentIdPk);
		}
		try{
			query.setHint("org.hibernate.cacheable", true);
			return (Integer)query.getSingleResult();
			}catch(NoResultException nre){
				return null;
			}
	}

	/**
	 * To get the maximum orderOpiton value
	 * @param parentidpk
	 * @param systemidpk
	 * @return Maximum OrderOption value
	 * @throws ServiceException
	 */
	public Integer getMaxOrderOptionServiceBean(Integer parentidpk,
			Integer systemidpk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select max(orderOption) from SystemOption os where os.state != :state");
		if(Validations.validateIsNotNullAndPositive(parentidpk)){
			sbQuery.append(" and parentSystemOption.idSystemOptionPk = :parentidPk");
		}
		if(Validations.validateIsNotNullAndPositive(systemidpk)){
			sbQuery.append(" and system.idSystemPk = :idSystemPk");
		}
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("state",OptionStateType.DELETED.getCode());
		if(Validations.validateIsNotNullAndPositive(parentidpk)){
			query.setParameter("parentidPk", parentidpk);
		}
		if(Validations.validateIsNotNullAndPositive(systemidpk)){
			query.setParameter("idSystemPk", systemidpk);
		}
		query.setHint("org.hibernate.cacheable", true);
		return (Integer)query.getSingleResult();
	}
	
	/**
	 * Metodo que obtiene el menu de un sistema a apartir del nombre de usuario y en mnemonico del sistema
	 * @param usuario
	 * @param mnemonicoSistema
	 * @return
	 */
	public List<SystemOption> getMenu(String usuario, String mnemonicoSistema) {

		List<SystemOption> menuSistema = new ArrayList<>();
		List<SystemOption> listaSystemOptionAux = new ArrayList<SystemOption>();

		//obteniendo los menus padres
		menuSistema = userServiceFacade.checkSystemOptionsByUserSystemAndOptionFather(usuario, mnemonicoSistema, null);
		
		//recorriendo la lista y obteniendo los hijos
		for (SystemOption sOption: menuSistema) {
			
			//obteniendo los hijos
			listaSystemOptionAux = userServiceFacade.checkSystemOptionsByUserSystemAndOptionFather(usuario, mnemonicoSistema, sOption.getIdSystemOptionPk());
			
			//seteando los hijos
			sOption.setSystemOptions(listaSystemOptionAux);
		}

		return menuSistema;

	}
	
}
