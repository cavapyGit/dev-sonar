package com.pradera.security.systemmgmt.facade;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.pradera.commons.interceptores.Performance;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.systemmgmt.service.SchedulerServiceBean;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class SchedulerServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/04/2014
 */
@Stateless
@Performance
public class SchedulerServiceFacade {

	/** The scheduler service. */
	@EJB
	SchedulerServiceBean schedulerService;
	
	/**
	 * Update timer.
	 *
	 * @param <T> the generic type
	 * @param t the t
	 * @throws ServiceException the service exception
	 */
	public <T> void updateTimer(T t) throws ServiceException {
		schedulerService.updateTimer(t);
	}
	
	public <T> void deleteTimer(T t) throws ServiceException {
		schedulerService.requestDelete(t);
	}
}
