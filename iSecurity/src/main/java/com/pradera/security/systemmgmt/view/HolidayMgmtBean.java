package com.pradera.security.systemmgmt.view;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.event.Event;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.security.model.HistoricalState;
import com.pradera.security.model.Holiday;
import com.pradera.security.model.type.DefinitionType;
import com.pradera.security.model.type.HolidayStateType;
import com.pradera.security.model.type.OtherMotiveType;
import com.pradera.security.systemmgmt.view.filters.HolidayFilter;
import com.pradera.security.utils.view.GenericBaseBean;
import com.pradera.security.utils.view.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Class AdmDiaFeriadoBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/11/2012
 */
@DepositaryWebBean
public class HolidayMgmtBean extends GenericBaseBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8172915822322244380L;

	/** The listado feriados. */
	private List<Holiday> holidayList;
	
	/** The model event feriados. */
	private ScheduleModel holidayEventModel;
	
	/** The dia feriado. */
	private Holiday holiday;
	
	/** The dia feriado filtro. */
	private HolidayFilter holidayFilter;
	//private ScheduleEvent evento;
	/** The df dia mes anio. */
	private DateFormat dfDayMonthYear;
	
	/** The mes anio seleccionado. */
	private String monthYearSelected;
	
	/** La dia habilitado. */
	private String dayAvailable;
	
	/** The historico estado. */
	private HistoricalState historicalState;
	
	/** The mostrar otro movivo. */
	private boolean showOtherMotive;
	
	/** The eliminar habilitado. */
	private boolean deleteAvailable;	
	
	private String holidayDesc;
	
	/** The excepcion. */
	@Inject
	Event<ExceptionToCatchEvent> exception;
	
	@Inject
	private UserInfo userInfo;
	
	private TimeZone timeZone;
	
	/**
	 * Instantiates a new adm dia feriado bean.
	 */
	public HolidayMgmtBean() {
		dfDayMonthYear = new SimpleDateFormat(PropertiesUtilities.getMessage(PropertiesConstants.DATE_FORMAT));		
		holidayFilter = new HolidayFilter();
		timeZone=TimeZone.getDefault();
	}
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		if (!FacesContext.getCurrentInstance().isPostback()) {					
			beginConversation();
			holidayFilter.setHolidayState(HolidayStateType.REGISTERED.getCode());
			listHolidaysByState();
			historicalState = new HistoricalState();
		}
	}
	
	/**
	 * Listar feriados por estado.
	 */
	public void listHolidaysByState(){
		try{
			holidayList = systemServiceFacade.listHolidaysByStateServiceFacade(holidayFilter.getHolidayState());
			holidayEventModel = new DefaultScheduleModel();
			
			if(!holidayList.isEmpty()){
				DefaultScheduleEvent scheduleEvent = null;
				
				for (Holiday objHoliday : holidayList) {
					scheduleEvent = new DefaultScheduleEvent(
							objHoliday.getDescription(), objHoliday.getDateHoliday(), objHoliday.getDateHoliday());
					scheduleEvent.setAllDay(true);
					scheduleEvent.setData(objHoliday);
					
					holidayEventModel.addEvent(scheduleEvent);
				}
			}
		
		} catch (Exception ex) {
			exception.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Inicia guardar.
	 */
	public void beginSaveListener(ActionEvent actionEvent) {
		if(Validations.validateIsNullOrEmpty(this.holidayDesc)){
			JSFUtilities.showRequiredDialog();
			this.setHolidayDesc("");
			String msg = PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_INSERT_DESCRIPTOIN);
			JSFUtilities.addContextMessage("frmHoliday:txtDescription",FacesMessage.SEVERITY_ERROR,msg,msg);
			JSFUtilities.hideComponent("frmConfSave:cnfSave");
			return;
		}
			
		//hideDialogsListener(null);
		//holiday.setDescription(holiday.getDescription().toUpperCase());
		holiday.setDescription(this.holidayDesc.toUpperCase());
		Object[] arrBodyData = {holiday.getDescription()};

		showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_REGISTER,
					null,
					PropertiesConstants.ADM_HOLIDAY_LBL_CONFIRM_REGISTER,					
					arrBodyData);
		JSFUtilities.showComponent("frmConfSave:cnfSave");	
	}

	/**
	 * Guardar.
	 */
	@LoggerAuditWeb
	public void saveListener(ActionEvent actionEvent) {
		try{
			//hideDialogsListener(null);
			
			holiday.setDescription(holiday.getDescription().toUpperCase());					
			holiday.setState(HolidayStateType.REGISTERED.getCode());			
			holiday.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			holiday.setRegistryDate(CommonsUtilities.currentDateTime());
			
			boolean isHolidayExist = systemServiceFacade.findHolidayExistenceFacade(holiday);
			if(isHolidayExist){
				Object[] arrBodyData={dfDayMonthYear.format(holiday.getDateHoliday())};
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT, null,
						PropertiesConstants.HOLIDAY_ALREADY_REGISTERED,arrBodyData);
				listHolidaysByState();
				JSFUtilities.showEndTransactionSamePageDialog();
				holiday = null;
				return;
			}
			boolean registerResult = systemServiceFacade.registerHolidayServiceFacade(holiday);
			
			if(registerResult){

				JSFUtilities.executeJavascriptFunction("PF('cnfwdSave').hide()");
				JSFUtilities.executeJavascriptFunction("PF('eventDialogWvar').hide()");
				Object[] arrBodyData = {holiday.getDescription(), dfDayMonthYear.format(holiday.getDateHoliday())};
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_SUCCESS, null,
						PropertiesConstants.SUCCESS_ADM_HOLIDAY_REGISTER_LABEL,
						arrBodyData);
				listHolidaysByState();
				
				//JSFUtilities.executeJavascriptFunction("PF('cnfwLocalEndTransaction').show()");
				JSFUtilities.showEndTransactionSamePageDialog();
			}
			
			holiday = null;
		} catch (Exception ex) {
			exception.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Inicia eliminar.
	 */
	public void beginDeleteListener(ActionEvent actionEvent) {
		try{
			//hideDialogsListener(null);
			historicalState = new HistoricalState();
			historicalState.setIdHistoricalStatePk(Long.valueOf(-1));
			
			if(lstMotives == null || lstMotives.isEmpty()){
				//showMotives();
				lstMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.HOLIDAY_DELETE_MOTIVES.getCode());
			}
			
			showOtherMotive = false;
			
			showMessageOnDialog(PropertiesConstants.LBL_MOTIVE_DELETE, null, null, null);
			
			UIInput input = (UIInput)JSFUtilities.findViewComponent("frmMotive:cboMotive");
			input.setValid(Boolean.TRUE);
			
			input = (UIInput)JSFUtilities.findViewComponent("frmMotive:txtOtherMotive");
			input.setValid(Boolean.TRUE);
			
			
			JSFUtilities.showComponent("frmMotive:cnfMotive");
	    } catch (Exception ex) {
	    	exception.fire(new ExceptionToCatchEvent(ex));
    	}
	}
	
	/**
	 * Eliminar.
	 */
	@LoggerAuditWeb
	public void deleteListener(ActionEvent actionEvent) {
		try{	
			//hideDialogsListener(null);
			holiday.setState(HolidayStateType.DELETED.getCode());
			holiday.setDeleteMotive(historicalState.getMotive());
			holiday.setDeleteOtherMotive(historicalState.getMotiveDescription());
			
			boolean deleteResult = systemServiceFacade.updateHolidayStateServiceFacade(holiday);				
			
			if(deleteResult) {
			JSFUtilities.executeJavascriptFunction("PF('cnfwdDelete').hide()");
			JSFUtilities.executeJavascriptFunction("PF('cnfwMotivo').hide()");
				JSFUtilities.executeJavascriptFunction("PF('eventDialogWvar').hide()");
				
				Object[] arrBodyData = {holiday.getDescription()};
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_SUCCESS, null,
						PropertiesConstants.SUCCESS_ADM_HOLIDAY_DELETE_LABEL,
						arrBodyData);
				listHolidaysByState();
				JSFUtilities.showEndTransactionSamePageDialog();
			}		
	
			holiday = null;
		} catch (Exception ex) {
			exception.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Seleccionar evento.
	 *
	 * @param selectEvent the select event
	 */
	public void selectEventListener(SelectEvent selectEvent) {			
		hideDialogsListener(null);
		
		ScheduleEvent event = (ScheduleEvent) selectEvent.getObject();

		Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		
		monthYearSelected = CommonsUtilities.convertDateToStringLocale(event.getStartDate(), "MMMMM yyyy", locale);
		
		if(validateDateSameMonth(event.getStartDate())){		
			holiday = (Holiday) event.getData();
			setOperationType(ViewOperationsType.CONSULT.getCode());
			if(holiday.getDateHoliday().before(CommonsUtilities.currentDate())){
				deleteAvailable = false;
			} else {
				deleteAvailable = true;
			}
			UIInput input = (UIInput)JSFUtilities.findViewComponent("frmHoliday:txtDescription");
			input.setValid(Boolean.TRUE);
			JSFUtilities.showComponent("frmHoliday:holidayDialog");
		}
	}

	/**
	 * Seleccionar fecha.
	 *
	 * @param selectedEventDate the select event
	 */
	public void selectDateListener(SelectEvent selectEvent) {
		
			hideDialogsListener(null);
			
			Date selectedEventDate = (Date) selectEvent.getObject();
			this.setHolidayDesc("");
			
			Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
			
			monthYearSelected = CommonsUtilities.convertDateToStringLocale(selectedEventDate, "MMMMM yyyy", locale);
			
			if(validateDateSameMonth(selectedEventDate)){
				List<ScheduleEvent> lstEvents = holidayEventModel.getEvents();	
				
				for (ScheduleEvent scheduleEvent : lstEvents) {
					
					if(selectedEventDate.equals(scheduleEvent.getStartDate()) && 
							selectedEventDate.equals(scheduleEvent.getEndDate())) {
						holiday = (Holiday) scheduleEvent.getData();
						this.setOperationType(ViewOperationsType.CONSULT.getCode());
						if(selectedEventDate.before(CommonsUtilities.currentDate())){
							deleteAvailable = false;
						} else {
							deleteAvailable = true;
						}
						UIInput input = (UIInput)JSFUtilities.findViewComponent("frmHoliday:txtDescription");
						input.setValid(Boolean.TRUE);
						JSFUtilities.showComponent("frmHoliday:holidayDialog");
						return;
					}
				}
				
				if(selectedEventDate.before(CommonsUtilities.currentDate())){
					showMessageOnDialog(
							PropertiesConstants.DIALOG_HEADER_ALERT, null, 
							PropertiesConstants.ERROR_ADM_HOLIDAY_REGISTER_PAST_DATE, null);
					JSFUtilities.addContextMessage(null,
							FacesMessage.SEVERITY_ERROR,"","");
					JSFUtilities.showValidationDialog();
				} else {
					if(userInfo.getUserAcctions().isRegister()){
						holiday = new Holiday();
						holiday.setDateHoliday(selectedEventDate);
						this.setOperationType(ViewOperationsType.REGISTER.getCode());
						UIInput input = (UIInput)JSFUtilities.findViewComponent("frmHoliday:txtDescription");
						input.setValid(Boolean.TRUE);
						JSFUtilities.showComponent("frmHoliday:holidayDialog");
					}else{
						JSFUtilities.showEndTransactionSamePageDialog();
						JSFUtilities.showMessageOnDialog(
								PropertiesConstants.DIALOG_HEADER_ALERT, null, 
								PropertiesConstants.ERROR_ADM_HOLIDAY_REGISTER_USER, null);
						return;
					}
					
				}
			
			}
	}	
	
	/**
	 * Valida fecha mismo mes.
	 *
	 * @param dateToCompare the fecha comparar
	 * @return true, if successful
	 */
	private boolean validateDateSameMonth(Date dateToCompare){
		
		try {
		Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		DateFormat dfMonthYear = new SimpleDateFormat("MMMMM yyyy", locale);
		
		Date dateSelectedMonthYear = dfMonthYear.parse(monthYearSelected);
		
		Calendar calSelectedMonthYear = Calendar.getInstance();
		Calendar calDateToCompare = Calendar.getInstance();
		
		calSelectedMonthYear.setTime(dateSelectedMonthYear);
		calDateToCompare.setTime(dateToCompare);
		return calSelectedMonthYear.get(Calendar.MONTH) == calDateToCompare.get(Calendar.MONTH);
		
		} catch (ParseException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Cambia seleccion motivo.
	 */
	public void changeSelectedMotiveListener(){
		
		if(OtherMotiveType.HOLIDAY_DELETE_OTHER_MOTIVE.getCode().equals(historicalState.getMotive())){
			showOtherMotive = true;
		} else {
			showOtherMotive = false;
		}
	}
	
	/**
	 * Acepta motivo eliminacion.
	 */
	public void acceptDeleteMotiveListener(){
		if(historicalState.getMotiveDescription()!=null && historicalState.getMotiveDescription().trim().length()==0){
			JSFUtilities.showRequiredDialog();
			historicalState.setMotiveDescription("");
			String msg = PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_INSERT_OTHERMOTIVE);
			JSFUtilities.addContextMessage("frmMotive:txtOtherMotive",FacesMessage.SEVERITY_ERROR,msg,msg);
			return;
		}
		//hideDialogsListener(null);
		if(historicalState.getMotiveDescription()!=null){
			historicalState.setMotiveDescription(historicalState.getMotiveDescription().toUpperCase());
		}
		
		Object[] arrBodyData = {holiday.getDescription().toUpperCase()};

		showMessageOnDialog(
				PropertiesConstants.LBL_CONFIRM_HEADER_CONFIRM,
				null,
				PropertiesConstants.ADM_HOLIDAY_LBL_CONFIRM_DELETE,					
				arrBodyData);
		
		JSFUtilities.showComponent("frmConfDelete:cnfDelete");
		//JSFUtilities.executeJavascriptFunction("PF('cnfwMotivo').hide()");				
	}
	
	/**
	 * Ocultar dialogos.
	 */
	public void hideDialogsListener(ActionEvent actionEvent){
		//Cargar variable para indicar que llegó al método action (No mostrar modal de requerido)
		JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
		
		//Ocultar Dialogos Generales
		JSFUtilities.hideGeneralDialogues();
		
		JSFUtilities.hideComponent("frmMotive:cnfMotive");
		JSFUtilities.hideComponent("frmHoliday:holidayDialog");
		JSFUtilities.hideComponent("frmConfDelete:cnfDelete");
		JSFUtilities.hideComponent("frmConfSave:cnfSave");
	}
	
	/**
	 * @return the holidayDesc
	 */
	public String getHolidayDesc() {
		return holidayDesc;
	}

	/**
	 * @param holidayDesc the holidayDesc to set
	 */
	public void setHolidayDesc(String holidayDesc) {
		this.holidayDesc = holidayDesc;
	}

	/**
	 * @return holidayList
	 */
	public List<Holiday> getHolidayList() {
		return holidayList;
	}

	/**
	 * @param holidayList
	 */
	public void setHolidayList(List<Holiday> holidayList) {
		this.holidayList = holidayList;
	}

	/**
	 * @return holidayEventModel
	 */ 
	public ScheduleModel getHolidayEventModel() {
		return holidayEventModel;
	}

	/**
	 * @param holidayEventModel
	 */
	public void setHolidayEventModel(ScheduleModel holidayEventModel) {
		this.holidayEventModel = holidayEventModel;
	}

	/**
	 * @return holiday
	 */
	public Holiday getHoliday() {
		return holiday;
	}

	/**
	 * @param holiday
	 */
	public void setHoliday(Holiday holiday) {
		this.holiday = holiday;
	}

	/**
	 * @return holidayFilter
	 */
	public HolidayFilter getHolidayFilter() {
		return holidayFilter;
	}

	/**
	 * @param holidayFilter
	 */
	public void setHolidayFilter(HolidayFilter holidayFilter) {
		this.holidayFilter = holidayFilter;
	}

	/**
	 * @return dfDayMonthYear
	 */
	public DateFormat getDfDayMonthYear() {
		return dfDayMonthYear;
	}

	/**
	 * @param dfDayMonthYear
	 */
	public void setDfDayMonthYear(DateFormat dfDayMonthYear) {
		this.dfDayMonthYear = dfDayMonthYear;
	}

	/**
	 * @return monthYearSelected
	 */
	public String getMonthYearSelected() {
		return monthYearSelected;
	}

	/**
	 * @param monthYearSelected
	 */
	public void setMonthYearSelected(String monthYearSelected) {
		this.monthYearSelected = monthYearSelected;
	}

	/**
	 * @return dayAvailable
	 */
	public String getDayAvailable() {
		return dayAvailable;
	}

	/**
	 * @param dayAvailable
	 */
	public void setDayAvailable(String dayAvailable) {
		this.dayAvailable = dayAvailable;
	}

	/**
	 * @return historicalState
	 */
	public HistoricalState getHistoricalState() {
		return historicalState;
	}

	/**
	 * @param historicalState
	 */
	public void setHistoricalState(HistoricalState historicalState) {
		this.historicalState = historicalState;
	}

	/**
	 * @return showOtherMotive
	 */
	public boolean isShowOtherMotive() {
		return showOtherMotive;
	}

	/**
	 * @param showOtherMotive
	 */
	public void setShowOtherMotive(boolean showOtherMotive) {
		this.showOtherMotive = showOtherMotive;
	}

	/**
	 * @return deleteAvailable
	 */
	public boolean isDeleteAvailable() {
		return deleteAvailable;
	}

	/**
	 * @param deleteAvailable
	 */
	public void setDeleteAvailable(boolean deleteAvailable) {
		this.deleteAvailable = deleteAvailable;
	}
		
	/**
	 * To close the motiveDialogs
	 */
	public void hideMotiveDialogs(){
		//Cargar variable para indicar que llegó al método action (No mostrar modal de requerido)
				JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
				
				//Ocultar Dialogos Generales
				JSFUtilities.hideGeneralDialogues();
				
				JSFUtilities.hideComponent("frmMotive:cnfMotive");
				JSFUtilities.hideComponent("frmConfDelete:cnfDelete");
				JSFUtilities.hideComponent("frmConfSave:cnfSave");
				
				historicalState.setMotive(Integer.valueOf(-1));
				showOtherMotive=false;
	}
	@PreDestroy
	void cleanup()
	{
		endConversation();
	}

	public TimeZone getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}
}
