package com.pradera.security.systemmgmt.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.component.outputpanel.OutputPanel;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.NodeUnselectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.primefaces.push.PushContext;
import org.primefaces.push.PushContextFactory;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.httpevent.publish.BrowserWindow;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.security.model.type.LanguageType;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.audit.facade.MonitoringUserAuditFacade;
import com.pradera.security.model.ExceptionUserPrivilege;
import com.pradera.security.model.OptionPrivilege;
import com.pradera.security.model.Parameter;
import com.pradera.security.model.ProfilePrivilege;
import com.pradera.security.model.UserAccount;
import com.pradera.security.model.UserPrivilegeDetail;
import com.pradera.security.model.type.DefinitionType;
import com.pradera.security.model.type.OptionPrivilegeStateType;
import com.pradera.security.model.type.OptionStateType;
import com.pradera.security.model.type.OtherMotiveType;
import com.pradera.security.model.type.PrivilegeType;
import com.pradera.security.model.type.SystemStateType;
import com.pradera.security.model.type.UserAccountType;
import com.pradera.security.model.type.UserExceptionSituationType;
import com.pradera.security.model.type.UserExceptionStateType;
import com.pradera.security.systemmgmt.service.ParameterSecurityServiceBean;
import com.pradera.security.systemmgmt.view.filters.SystemProfileFilter;
import com.pradera.security.systemmgmt.view.filters.UserExceptionFilter;
import com.pradera.security.usermgmt.facade.UserServiceFacade;
import com.pradera.security.usermgmt.view.UserDataModel;
import com.pradera.security.usermgmt.view.filters.UserAccountFilter;
import com.pradera.security.utils.view.GenericBaseBean;
import com.pradera.security.utils.view.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class OptionUserExceptionMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/01/2013
 */
@DepositaryWebBean
public class OptionUserExceptionMgmtBean extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The user info. */
    @Inject
    private UserInfo userInfo;
      
    @Inject
    UserPrivilege userPrivilege;
    
    @EJB
    ParameterSecurityServiceBean parameterSecurityService;
	
	/** The exception user privilege array. */
	private ExceptionUserPrivilege[] exceptionUserPrivilegeArray;	
	
	/** The user exception. */
	private UserExceptionFilter userException;
	
	/** The user exception filter. */
	private UserExceptionFilter userExceptionFilter;
	
	/** The user exception detail. */
	private UserExceptionFilter userExceptionDetail;
	
	/** The exception user data model. */
	private ExceptionUserDataModel exceptionUserDataModel;
	
	/** The exception user privilege. */
	private ExceptionUserPrivilege exceptionUserPrivilege;
	
	/** The user data model. */
	private UserDataModel userDataModel;
	
	/** The selected system. */
	private Integer selectedSystem;
	
	/** The root privileges. */
	private TreeNode rootPrivileges;
	
	/** The selected nodes. */
	private TreeNode[] selectedNodes; 
    
    /** The expand node. */
    private boolean expandNode;
    
    /** The full name. */
    private String fullName;
    
    /** The show warning. */
    private boolean showWarning;
    
    /** The show panel options component. */
    private boolean showPanelOptionsComponent;
    
    /** The show motive text. */
	private boolean showMotiveText;
	
	/** The motive text. */
	private String motiveText;
	
	/** The motive selected. */
	private Integer motiveSelected;
	
	private List<ProfilePrivilege> userProfiles;
 
	@EJB
	protected MonitoringUserAuditFacade userAuditFacade;
	
	/** The log. */
    Logger log = Logger.getLogger(OptionUserExceptionMgmtBean.class.getName());
    
    private boolean blCurrentLoadFile;
    
    private Map<Integer,String> situationsException;
    
    private boolean showConfirmDelete;
    
    /** The user service facade. */
	@EJB
	protected UserServiceFacade userServiceFacade;
    
    /**
     * Instantiates a new option user exception mgmt bean.
     */
    public OptionUserExceptionMgmtBean(){
		
		userException = new UserExceptionFilter();
		exceptionUserPrivilege = new ExceptionUserPrivilege();
		userExceptionFilter = new UserExceptionFilter();
		userExceptionFilter.setInitialDate(CommonsUtilities.currentDateTime());
    	userExceptionFilter.setFinalDate(CommonsUtilities.currentDateTime()); 
	
	}
	
    /**
     * Inits the.
     */
    @PostConstruct
	public void init() {
    	if (!FacesContext.getCurrentInstance().isPostback()) {
    		try{
    	    beginConversation();
	    	listUserType();
	    	listUserExceptionState();
//	    	lstBlockMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.USER_EXCEPTION_BLOCK_MOTIVES.getCode());
//			lstUnBlockMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.USER_EXCEPTION_DESBLOCK_MOTIVES.getCode());
			lstDeleteMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.USER_EXCEPTION_DELETE_MOTIVES.getCode());
			lstRejectMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.USER_EXCEPTION_REJECTED_MOTIVES.getCode());
			List<Integer> params =  new ArrayList<>();{}
			params.add(UserExceptionSituationType.ACTIVE.getCode());
			params.add(UserExceptionSituationType.PENDING_DELETE.getCode());
			List<Parameter> situations = parameterSecurityService.listParameterByIdentificator(params);
			situationsException = new HashMap<>();
			for(Parameter situation: situations){
				situationsException.put(situation.getIdParameterPk(), situation.getName());
			}
			//setting privilege
			PrivilegeComponent privilegeComp=new PrivilegeComponent();
			privilegeComp.setBtnModifyView(true);
			privilegeComp.setBtnBlockView(false);
			privilegeComp.setBtnUnblockView(false);
			privilegeComp.setBtnDeleteView(true);
			privilegeComp.setBtnConfirmView(true);
			privilegeComp.setBtnRejectView(true);
			userPrivilege.setPrivilegeComponent(privilegeComp);
    		}catch(Exception ex){
				excepcion.fire(new ExceptionToCatchEvent(ex));
			}	    	  
    	} else {
    		if(Validations.validateIsNotNull(exceptionUserDataModel) && 
    				Validations.validateIsNotNull(exceptionUserDataModel.getRowCount()) && 
    				exceptionUserDataModel.getRowCount() > 0 &&
    				Validations.validateIsNull(userExceptionFilter.getUserExceptionStateSelected())) {
    			List<ExceptionUserPrivilege> searchList = (List<ExceptionUserPrivilege>) exceptionUserDataModel.getWrappedData();
    			userExceptionFilter.setUserExceptionStateSelected(searchList.get(0).getState());
    		}
    	}
	}
    
    public void confirmAction(){  
    	if(showMotiveText){
			if (Validations.validateIsNullOrEmpty(motiveText)||	motiveText.trim().length() == 0) {
				this.setMotiveText("");
				JSFUtilities.showComponent("cnfMsgCustomValidation");
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT, null,
						PropertiesConstants.ERROR_MESSAGE_MOTIVE_REQUIRED,null);
				String errorMessage=PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_MOTIVE_REQUIRED);
				JSFUtilities.addContextMessage("formGeneral:dialogMotive",FacesMessage.SEVERITY_ERROR, errorMessage,errorMessage);
				return;
			}
		}
    	//hideDialogues();    	
    	//JSFUtilities.executeJavascriptFunction("PF('dlgMotive').hide()");
    	JSFUtilities.executeJavascriptFunction("PF('cnfwDelete').show()");
    }

    public void closeDlgMotive() {
    	
    	JSFUtilities.executeJavascriptFunction("PF('dlgMotive').hide()");
    	JSFUtilities.executeJavascriptFunction("PF('dlgDeleteOk').hide()");
    	JSFUtilities.executeJavascriptFunction("PF('dglConfirmException').hide()");
    	JSFUtilities.executeJavascriptFunction("PF('dglWarning').hide()");
    	hideDialogues();  
    }
    
    @LoggerAuditWeb
    public void openRejectRequestExceptionPrivilege(){
    	//hideDialogues();
    	showMessageOnDialog("lbl.confirm.header.record",null,"adm.exception.user.reject.request.delete",null);	
    	JSFUtilities.executeJavascriptFunction("PF('dglRejectDeleteException').show()");
		//JSFUtilities.showComponent("formGeneral:dialogRejectDeleteException");
    }
    
    @LoggerAuditWeb
    public void openConfirmRequestExceptionPrivilege(){    	
    	showMessageOnDialog("lbl.confirm.header.record",null,"adm.exception.user.confirm.delete",null);
    	JSFUtilities.executeJavascriptFunction("PF('dglConfirmDeleteException').show()");
		//JSFUtilities.showComponent("formGeneral:dialogConfirmDeleteException");
    }

    public void evaluateMotive(){
    	JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
    	if(isBlock()){
        	if(motiveSelected.equals(OtherMotiveType.USER_EXCEPTION_BLOCK_OTHER_MOTIVE.getCode())){
        		motiveText="";
        		showMotiveText = true;
        	}else {
        		showMotiveText = false;
    		}
        }
        else if(isUnBlock()){
        	if(motiveSelected.equals(OtherMotiveType.USER_EXCEPTION_UNBLOCK_OTHER_MOTIVE.getCode())){
        		motiveText="";
        		showMotiveText = true;
        	}else {
        		showMotiveText = false;
    		}
        } else if(isReject()){
        	if(motiveSelected.equals(OtherMotiveType.USER_EXCEPTION_REJECT_OTHER_MOTIVE.getCode())){
        		motiveText="";
        		showMotiveText = true;
        	}else {
        		showMotiveText = false;
    		}
        }else if(isDelete()){ 
        	if(motiveSelected.equals(OtherMotiveType.USER_EXCEPTION_DELETE_OTHER_MOTIVE.getCode())){
			motiveText="";
			showMotiveText=true;
        	} else {
        		showMotiveText = false;
        	}
		}
		else{
			showMotiveText=false;
		}
	}
	
	/**
	 * Loading user exception register.
	 */
	public void loadingUserExceptionRegister(){
		this.setOperationType(ViewOperationsType.REGISTER.getCode());
		userDataModel = new UserDataModel();		
		selectedSystem = -1;
		listSystemsByState(SystemStateType.CONFIRMED.getCode());
		this.setOperationType(ViewOperationsType.REGISTER.getCode());
		userException = new UserExceptionFilter();
		userException.setInitialDate(CommonsUtilities.currentDateTime());
		userException.setFinalDate(CommonsUtilities.currentDateTime());
		rootPrivileges = null;
		
	}
	/**
	 * Loading user exception modify.
	 */
	public String loadingUserExceptionModify(){
		try{
			userProfiles = new ArrayList<ProfilePrivilege>();
			if(exceptionUserPrivilegeArray.length>1 ||exceptionUserPrivilegeArray.length < 1){
				JSFUtilities.showComponent("cnfEndTransactionSamePage");
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT,null,PropertiesConstants.ERROR_SELECT_ONE_RECORD, null);
				return "";
			}else if(!exceptionUserPrivilegeArray[0].getState().equals(UserExceptionStateType.REGISTERED.getCode())){
				JSFUtilities.showComponent("cnfEndTransactionSamePage");
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT,null,
						PropertiesConstants.ERROR_WRONG_RECORD_MODIFY,null);
				return "";
			}
			else{
				this.setOperationType(ViewOperationsType.MODIFY.getCode());
			    listSystemsByState(SystemStateType.CONFIRMED.getCode());
			    userException = new UserExceptionFilter();
				ExceptionUserPrivilege excepUser=exceptionUserPrivilegeArray[0];
				userException.setUserExceptionCodeUser(excepUser.getUserAccount().getIdUserPk());
				userException.setInitialDate(excepUser.getInitialDate());
				userException.setFinalDate(excepUser.getEndDate());
				userException.setUserExceptionName(excepUser.getName());
				userException.setUserFullName(excepUser.getUserAccount().getFirtsLastName()+" "+excepUser.getUserAccount().getSecondLastName());
				userException.setUserExceptionDescription(excepUser.getDescription());
				userException.setIdPrivelegeException(excepUser.getIdExceptionUserPrivilegePk());
				userException.setUsuLogin(excepUser.getUserAccount().getLoginUser());
				userException.setIdSystemSelected(excepUser.getSystem().getIdSystemPk());
				userException.setFileName(excepUser.getNameFile());
				//load file
				userException.setLoadFile(systemServiceFacade.getExceptionUserPrivilegeFile(excepUser.getIdExceptionUserPrivilegePk()));
				//copy version
				userException.setVersion(excepUser.getVersion());
				userException.setRegistryDate(excepUser.getRegistryDate());
				userException.setRegistryUser(excepUser.getRegistryUser());
				userException.setUserAccount(excepUser.getUserAccount());
				if(excepUser.getUserAccount().getSecondLastName() != null)
					userException.setUserFullName(excepUser.getUserAccount().getFirtsLastName()+" "+excepUser.getUserAccount().getSecondLastName()+", "+excepUser.getUserAccount().getFullName());
				else
					userException.setUserFullName(excepUser.getUserAccount().getFirtsLastName()+", "+excepUser.getUserAccount().getFullName());
				//validate when the file was loaded from database
				if(Validations.validateIsNotNullAndNotEmpty(excepUser.getNameFile())){
					blCurrentLoadFile=false;
				}else{
					blCurrentLoadFile=true;
				}
				
				selectedSystem=excepUser.getSystem().getIdSystemPk();
				SystemProfileFilter systemProfileFilter = new SystemProfileFilter();
				systemProfileFilter.setIdSystem(excepUser.getSystem().getIdSystemPk());
		        
				if(LanguageType.SPANISH.getValue().equals(userInfo.getCurrentLanguage())){
					userException.setUserExceptionLanguage(LanguageType.SPANISH.getCode());
					systemProfileFilter.setIdLanguage(LanguageType.SPANISH.getCode());
				}else if(LanguageType.ENGLISH.getValue().equals(userInfo.getCurrentLanguage())){
					userException.setUserExceptionLanguage(LanguageType.ENGLISH.getCode());
					systemProfileFilter.setIdLanguage(LanguageType.ENGLISH.getCode());
				}else if(LanguageType.FRANCES.getValue().equals(userInfo.getCurrentLanguage())){
					userException.setUserExceptionLanguage(LanguageType.FRANCES.getCode());
					systemProfileFilter.setIdLanguage(LanguageType.FRANCES.getCode());
				}
				systemProfileFilter.setSystemOptionState(OptionStateType.CONFIRMED.getCode());
				systemProfileFilter.setOptionPrivilegeState(OptionPrivilegeStateType.REGISTERED.getCode());
				List<Object[]> lstSystemOptions = systemServiceFacade.listOptionSystemsByLanguageServiceFacade(systemProfileFilter);
				if(lstSystemOptions.isEmpty()){
					rootPrivileges = null;			
					JSFUtilities.addContextMessage("frmProfileMgmt:cboSystem",
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_PROFILE_PRIVILEGES_NO_EXIST),
							PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_PROFILE_PRIVILEGES_NO_EXIST));
					return null;
				}
					
				rootPrivileges = new DefaultTreeNode("Root", null);			
				
				List<Object[]> lstParentSystemOptions = getParentOptions(lstSystemOptions);		
				
				for (Object[] arrRow : lstParentSystemOptions) {
					loadingOptionsPrivilegeRecursive(arrRow,lstSystemOptions, rootPrivileges);
				}
				
				if(rootPrivileges.getChildCount()>0){
					List<Object[]> lstOptionsPrivileges = systemServiceFacade.listOptionPrivilegesServiceFacade(systemProfileFilter);
					
					if(lstOptionsPrivileges.isEmpty()){
						rootPrivileges = null;
						JSFUtilities.addContextMessage("frmGeneral:cboSystem",
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_PROFILE_PRIVILEGES_NO_EXIST),
								PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_PROFILE_PRIVILEGES_NO_EXIST));
						return null;
					}
					
					for (Object[] arrOptionPrivilegeRow : lstOptionsPrivileges) {
						loadingPrivilegeNodeRecursive(arrOptionPrivilegeRow,rootPrivileges);
					}
					contractPrivileges();
					List<Object[]> listExceptionOptionPrivilege = systemServiceFacade.searchOptionPrivilegesExceptionServiceFacade(userException);
					if(!listExceptionOptionPrivilege.isEmpty()){
						List<Integer> listPrivileges = new ArrayList<Integer>();
						
						for (Object[] arrFilaPrivilegio : listExceptionOptionPrivilege) {
							listPrivileges.add(new Integer(arrFilaPrivilegio[0].toString()));
						}
						userException.setPrivelegeListPK(listPrivileges);
						List<Integer> listOptions  = systemServiceFacade.listIdOptionsByPrivilegeServiceFacade(userException);
						userException.setOptionsListPK(listOptions);
						checkPrivilegeSelectionRecursive(listOptions, listPrivileges,rootPrivileges);	
					}
					loadUserPriviledges();
					if(!userProfiles.isEmpty()){					
						for (ProfilePrivilege profilePrivilege : userProfiles) {
							checkPrivilegeSelectionRecursive(profilePrivilege,rootPrivileges);					
						}					
					}
				} else {
					JSFUtilities.addContextMessage("frmGeneral:cboSystem",
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_PROFILE_PRIVILEGES_NO_EXIST),
							PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_PROFILE_PRIVILEGES_NO_EXIST));
					return null;
				}
			}
		}
			catch(Exception ex){
				excepcion.fire(new ExceptionToCatchEvent(ex));
			}
		
		return "goToUserOptionExceptionRegister";
	}
	
	/**
	 * Check privilege selection recursive.
	 *
	 * @param profilePrivilege the profile privilege
	 * @param node the node
	 */
	private void checkPrivilegeSelectionRecursive(List<Integer> lstOptions, List<Integer> lstPrivilages, TreeNode node){
		
		if(node.getData() instanceof OptionPrivilegeNodeBean){
			
			OptionPrivilegeNodeBean optionPrivilegeNodeBean = (OptionPrivilegeNodeBean) node.getData();
			if(optionPrivilegeNodeBean.getIdOptionPrivilege()!=null){
																				
				if(lstOptions.contains(optionPrivilegeNodeBean.getIdSystemOption()) 
						|| lstPrivilages.contains(optionPrivilegeNodeBean.getIdOptionPrivilege())){
					node.setSelected(true);
					validateSelectedParentRecursive(node);
					return;
				}
				
			} else {
				if(node.getChildCount()>0){
					for (TreeNode childNode : node.getChildren()) {
						checkPrivilegeSelectionRecursive(lstOptions, lstPrivilages, childNode);
					}
				}
			}
									
		} else {
			if(node.getChildCount()>0){
				for (TreeNode childNode : node.getChildren()) {
					checkPrivilegeSelectionRecursive(lstOptions, lstPrivilages, childNode);
				}
			}
		}
	}
	
	/**
	 * Validate selected parent recursive.
	 *
	 * @param node the node
	 */
	private void validateSelectedParentRecursive(TreeNode node){
		if(node.getParent() instanceof DefaultTreeNode){
			TreeNode parentNode = node.getParent();
			List<TreeNode> lstChildNodes = parentNode.getChildren();
			int childNodeQuantity = parentNode.getChildCount();
			int selectedChildNodeQuantity = 0;
			
			for (TreeNode childNode : lstChildNodes) {
				if(childNode.isSelected()){
					selectedChildNodeQuantity++;
				}
			}
			
			if(selectedChildNodeQuantity==childNodeQuantity){
				parentNode.setSelected(true);
			}
			
			validateSelectedParentRecursive(parentNode);
			
		}
	}
	
	
	/**
	 * Evaluate user type.
	 */
	public void evaluateUserType(){
		showInstitutionsByUserAccountType(userExceptionFilter.getUserExceptionSelectedType());
	
	}
	
	/**
	 * Before search user.
	 */
	public void beforeSearchUser(){
		userDataModel = null;
		fullName="";
		//hideDialogues();
		JSFUtilities.executeJavascriptFunction("PF('dlgSearchUser').show();");
		//JSFUtilities.showComponent("frmUserOptionExceptionMgmt:dialogSearchUser");
	}
	
	public void validateCodeUser(){
		if(userException.getUsuLogin() != null && userException.getUsuLogin().trim().length() > 0){
			try{
				selectedSystem = -1;
				rootPrivileges = null;
				// hideDialogues();				
				UserAccount user = systemServiceFacade.searchUserByUserLoginServiceFacade(userException.getUsuLogin().toUpperCase());			
				if(user == null){
					userException.setUsuLogin(null);
					userException.setUserFullName(null);
					//JSFUtilities.showEndTransactionSamePageDialog();				
					showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,
		    				PropertiesConstants.NO_EXIST_USER_LOGIN, null);
		    		JSFUtilities.showSimpleValidationDialog();									
					return;
				} else {
					userException.setUserExceptionCodeUser(user.getIdUserPk());
					userException.setUserAccount(user);
					if(user.getSecondLastName() != null)
						userException.setUserFullName(user.getFirtsLastName() + " " + user.getSecondLastName() + ", " + user.getFullName());
					else
						userException.setUserFullName(user.getFirtsLastName() + ", " + user.getFullName());
				}		      
			} catch(Exception ex){
				excepcion.fire(new ExceptionToCatchEvent(ex));
			}
		}else{
			userException.setUserFullName(null);			
		}
	}
	
	/**
	 * Search users.
	 */
	public void searchUsers(){
	try{
		
		
	  List<UserAccount>	listUser = new ArrayList<UserAccount>();
	 
	   String strFullName = Validations.validateIsNotNull(fullName) ? fullName.toUpperCase() : "";
	   
	   List<UserAccount>  list=systemServiceFacade.searchUsersByNameServiceFacade(strFullName); 
	      
	      if(list!=null && list.size()>0)
	      {	       
	       for(UserAccount usu : list)
	       {
	       usu.setUserTypeDescription(UserAccountType.get(usu.getType()).getValue());
	       listUser.add(usu);
	       }    	  
           }
      
      
      userDataModel = new UserDataModel(listUser);
      
      	}
		catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		
	}
	
	/**
	 * Loading code.
	 *
	 * @param event the event
	 */
	public void loadingCode(ActionEvent event){
		hideDialogues();
		UserAccount userAccount=(UserAccount)event.getComponent().getAttributes().get("usuDet");
		userException.setUsuLogin(userAccount.getLoginUser());
		userException.setUserExceptionCodeUser(userAccount.getIdUserPk());
		if(Validations.validateIsNotNullAndNotEmpty(userAccount.getSecondLastName())){
			userException.setUserFullName(userAccount.getFirtsLastName()+" "+userAccount.getSecondLastName()+", "+userAccount.getFullName());
		}else{
			userException.setUserFullName(userAccount.getFirtsLastName()+", "+userAccount.getFullName());
		}
	}
	
	/**
	 * File upload handler.
	 *
	 * @param event the event
	 */
	public void fileUploadHandler(FileUploadEvent event){
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);		
		JSFUtilities.hideGeneralDialogues();
		userException.setFileName(fUploadValidate(event.getFile(),"PF('cnfwMsgCustomValidationAcc').show();", null, getfUploadFileSize(), "pdf"));
		if(userException.getFileName()!=null){
			blCurrentLoadFile=true;
			try {
				userException.setLoadFile(event.getFile().getContents());
				JSFUtilities.updateComponent("otFileName");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Removes the document file.
	 */
	public void removeDocumentFile(){		
		if(userException.getLoadFile() != null){
			userException.setLoadFile(null);
		}
	}
	/**
	 * Validate filter.
	 *
	 * @return true, if successful
	 */
	public boolean validateFilter(){
		//hideDialogues();
		if(userExceptionFilter.getInitialDate().getTime()>userExceptionFilter.getFinalDate().getTime()){
	        JSFUtilities.addContextMessage("frmGeneral:popupCalInitial",FacesMessage.SEVERITY_ERROR,
			PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_EXCEPTIONUSER_DATE_INITIAL),
			PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_EXCEPTIONUSER_DATE_INITIAL));
			return false;
		}
		else if(userExceptionFilter.getFinalDate().getTime()<userExceptionFilter.getInitialDate().getTime()){	
			        JSFUtilities.addContextMessage("frmGeneral:popupCalEnd",FacesMessage.SEVERITY_ERROR,
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_EXCEPTIONUSER_DATE_FINAL),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_EXCEPTIONUSER_DATE_FINAL));
			return false;
		}
		else{
			return true;
		}
	}

	/**
	 * Search user exceptions.
	 */
	public void searchUserExceptions(){
		try{
		    if(validateFilter()){	
				//hideDialogues();
				userExceptionFilter.setUserExceptionCodeUser(null);
				if( StringUtils.isNotBlank(userExceptionFilter.getUsuLogin())){
					UserAccountFilter userAccountFilter = new UserAccountFilter();
					userAccountFilter.setLoginUserAccount(userExceptionFilter.getUsuLogin());
					List<UserAccount> listUser = userServiceFacade.searchUsersByFilterServiceFacade(userAccountFilter);
					UserAccount user = new UserAccount();
					if(listUser.size()>0){	  
					  user = listUser.get(0);
					  userExceptionFilter.setUserExceptionCodeUser(user.getIdUserPk());
					}else{
						userExceptionFilter.setUserExceptionCodeUser(-1);
					}
			}
			exceptionUserDataModel = new ExceptionUserDataModel(systemServiceFacade.searchUserExceptionsByFilterServiceFacade(userExceptionFilter));
			/*if(exceptionUserDataModel.getRowCount()>0 && (userExceptionFilter.getUserExceptionStateSelected().equals( UserExceptionStateType.DELETED.getCode()))){
				showPanelOptionsComponent=false;
			}
			else{*/
				showPanelOptionsComponent=true;
			//}
			}
		}catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Hide dialogues.
	 */
	public void hideDialogues(){
		JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
		JSFUtilities.hideGeneralDialogues();
		//JSFUtilities.hideComponent("formGeneral:dialogWarning");
		JSFUtilities.executeJavascriptFunction("PF('dglWarning').hide()");
		//JSFUtilities.hideComponent("formGeneral:dialogDelete");
		JSFUtilities.executeJavascriptFunction("PF('cnfwDelete').hide()");
		//JSFUtilities.hideComponent("formGeneral:dialogDeleteOk");
		JSFUtilities.executeJavascriptFunction("PF('dlgMotive').hide()");		
		//JSFUtilities.hideComponent("formGeneral:dialogMotive");
		JSFUtilities.executeJavascriptFunction("PF('cnfBackMotivePriv').hide()");	
		//JSFUtilities.hideComponent("formGeneral:modalBackMotive");

		JSFUtilities.executeJavascriptFunction("PF('dglConfirmException').hide()");	
		//JSFUtilities.hideComponent("formGeneral:dialogConfirmException");
		JSFUtilities.executeJavascriptFunction("PF('dglRejectDeleteException').hide()");	
		//JSFUtilities.hideComponent("formGeneral:dialogRejectDeleteException");
		JSFUtilities.executeJavascriptFunction("PF('dglConfirmDeleteException').hide()");	
		//JSFUtilities.hideComponent("formGeneral:dialogConfirmDeleteException");
		
		
		JSFUtilities.executeJavascriptFunction("PF('dglWarning').hide()");	
		//JSFUtilities.hideComponent("frmUserOptionExceptionMgmt:dialogWarning");
		JSFUtilities.executeJavascriptFunction("PF('cnfwAdmExcUsuPriv').hide()");	
		//JSFUtilities.hideComponent("frmUserOptionExceptionMgmt:cnfAdmExcUsuPriv");
		JSFUtilities.executeJavascriptFunction("PF('dlgSearchUser').hide()");	
		//JSFUtilities.hideComponent("frmUserOptionExceptionMgmt:dialogSearchUser");
		JSFUtilities.executeJavascriptFunction("PF('cnfClearExceptionNameWin').hide()");	
		//JSFUtilities.hideComponent("frmUserOptionExceptionMgmt:cnfClearExceptionName");   
		
		
	}
	
	
	
	/**
	 * Clean.
	 */
	public void clean(){
		userExceptionFilter = new UserExceptionFilter();
		userExceptionFilter.setInitialDate(CommonsUtilities.currentDateTime());
    	userExceptionFilter.setFinalDate(CommonsUtilities.currentDateTime()); 
		exceptionUserDataModel = null;
		userExceptionFilter.setUserExceptionSelectedType(-1);
		showPanelOptionsComponent=false;
		showConfirmDelete = false;
	}
	
	/**
	 * Used to Reset the Values i Registration
	 */
	public void clearException(){
		userException = new UserExceptionFilter();
		userException.setInitialDate(CommonsUtilities.currentDateTime());
    	userException.setFinalDate(CommonsUtilities.currentDateTime()); 
    	selectedSystem = -1;
    	rootPrivileges = null;
	}
	
	@LoggerAuditWeb
	public void confirmExceptionPrivilegeUser(){
		//hideDialogues();
		JSFUtilities.executeJavascriptFunction("PF('dglConfirmException').hide()");
		
		List<ExceptionUserPrivilege> exceptions = new ArrayList<>();
		for(ExceptionUserPrivilege exception:exceptionUserPrivilegeArray){
			exception.setState(UserExceptionStateType.CONFIRMED.getCode());
			exception.setSituation(UserExceptionSituationType.ACTIVE.getCode());
			exceptions.add(exception);
		}
		try{
			systemServiceFacade.confirmPrivilegeExceptionUser(exceptions);
			searchUserExceptions();

			JSFUtilities.executeJavascriptFunction("PF('dlgDeleteOk').show()");
			//JSFUtilities.showComponent("formGeneral:dialogDeleteOk");
		} catch(ServiceException sex){
			if(sex.getMessage()!=null){
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION,sex.getErrorService().getMessage());
				JSFUtilities.showSimpleValidationDialog();
				searchUserExceptions();
			}
		}catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	@LoggerAuditWeb
	public void deletePrivilegeExceptionUser() {
		//hideDialogues();
		
		JSFUtilities.executeJavascriptFunction("PF('dglConfirmDeleteException').hide()");
		JSFUtilities.executeJavascriptFunction("PF('dlgMotive').hide()");
		try{
			ExceptionUserPrivilege exceptionUser= exceptionUserPrivilegeArray[0];
			exceptionUser.setState(UserExceptionStateType.DELETED.getCode());
			exceptionUser.setSituation(UserExceptionSituationType.ACTIVE.getCode());
			systemServiceFacade.deletePrivilegeExceptionUser(exceptionUser);
			searchUserExceptions();
			List<Integer> listDelete = new ArrayList<Integer>();
			for(int i=0;i<exceptionUserPrivilegeArray.length;i++){
				listDelete.add(new Integer(exceptionUserPrivilegeArray[i].getIdExceptionUserPrivilegePk()));
			}
			//close remote session
			List<Object[]> exceptionsList = systemServiceFacade.getExceptionUserPrivilegesDetailsByIds(listDelete);
			for(Object[] privilege:exceptionsList){ 
				List<Object[]> list = userAuditFacade.getLastAccessSystemName((Integer)privilege[0], (Integer)privilege[2]);
				 if(Validations.validateIsNotNullAndNotEmpty(list) && list.size()>0){
					 	//PushContext pushContext = PushContextFactory.getDefault().getPushContext();
						//integration with notifications
						BrowserWindow browser = new BrowserWindow();
						browser.setNotificationType(NotificationType.KILLSESSION.getCode());
						browser.setSubject("Expire Session");
						browser.setMessage("Expire Session user "+(Integer)privilege[0]);
						//pushContext.push(PropertiesConstants.PUSH_MSG+"/"+privilege[1].toString(),browser);
						//omniChannel.send(browser,privilege[1].toString());
				 }
			}
			JSFUtilities.executeJavascriptFunction("PF('dlgDeleteOk').show()");
		} catch(ServiceException sex){
			if(sex.getMessage()!=null){
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION,sex.getErrorService().getMessage());
				JSFUtilities.showSimpleValidationDialog();
				searchUserExceptions();
			}
		}catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	@LoggerAuditWeb
	public void rejectDeletePrivilegeExceptionUser() {
		//hideDialogues();
		JSFUtilities.executeJavascriptFunction("PF('dglRejectDeleteException').hide()");		
		try{
			ExceptionUserPrivilege exceptionUser= exceptionUserPrivilegeArray[0];
			exceptionUser.setState(UserExceptionStateType.CONFIRMED.getCode());
			exceptionUser.setSituation(UserExceptionSituationType.ACTIVE.getCode());
			systemServiceFacade.rejectDeletePrivilegeExceptionUser(exceptionUser);
			searchUserExceptions();
			JSFUtilities.executeJavascriptFunction("PF('dlgDeleteOk').show()");
		} catch(ServiceException sex){
			if(sex.getMessage()!=null){
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION,sex.getErrorService().getMessage());
				JSFUtilities.showSimpleValidationDialog();
				searchUserExceptions();
			}
		}catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	
	
	/**
	 * Delete user exceptions.
	 */
	@LoggerAuditWeb
	public void deleteUserExceptions(){
		//hideDialogues();
		List<ExceptionUserPrivilege> exceptionsUsers = new ArrayList<>();
		try{
		for(int i=0;i<exceptionUserPrivilegeArray.length;i++){
			exceptionsUsers.add(exceptionUserPrivilegeArray[i]);
		}
		//rules
		//Block is using for reject
		//Unblock is using for delete
		if(ViewOperationsType.REJECT.getCode().equals(this.getOperationType())){
			for(ExceptionUserPrivilege exceptionUser: exceptionsUsers){
				exceptionUser.setState(UserExceptionStateType.REJECT.getCode());
				exceptionUser.setSituation(UserExceptionSituationType.ACTIVE.getCode());
				exceptionUser.setBlockMotive(this.getMotiveSelected());
				exceptionUser.setBlockOtherMotive(motiveText);
			}
			systemServiceFacade.registryRejectoRequestDeleteExceptionPrivilege(exceptionsUsers, UserExceptionStateType.REJECT.getCode());
//			systemServiceFacade.modifyStateUserExceptionByListServiceFacade(listDelete,state,UserExceptionSituationType.ACTIVE.getCode(),blockMotive,unBlockMotive,motiveDescriptionArr,parameters);
			searchUserExceptions();
		} else if(ViewOperationsType.DELETE.getCode().equals(this.getOperationType())){
			for(ExceptionUserPrivilege exceptionUser: exceptionsUsers){
				exceptionUser.setState(UserExceptionStateType.CONFIRMED.getCode());
				exceptionUser.setSituation(UserExceptionSituationType.PENDING_DELETE.getCode());
				exceptionUser.setUnblockMotive(this.getMotiveSelected());
				exceptionUser.setUnblockOtherMotive(motiveText);
			}
			systemServiceFacade.registryRejectoRequestDeleteExceptionPrivilege(exceptionsUsers, UserExceptionStateType.CONFIRMED.getCode());
//			systemServiceFacade.modifyStateUserExceptionByListServiceFacade(listDelete,state,UserExceptionSituationType.PENDING_DELETE.getCode(),blockMotive,unBlockMotive,motiveDescriptionArr,parameters);
			searchUserExceptions();
		}
		JSFUtilities.executeJavascriptFunction("PF('dlgDeleteOk').show()");
		} catch(ServiceException sex){
			if(sex.getMessage()!=null){
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION,sex.getErrorService().getMessage());
				JSFUtilities.showSimpleValidationDialog();
				searchUserExceptions();
			}
		}catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Detail exception user.
	 *
	 * @param event the event
	 */
	public void detailExceptionUser(ActionEvent event){
		try{
		    listSystemsByState(SystemStateType.CONFIRMED.getCode());
			
			userExceptionDetail = new UserExceptionFilter();
			ExceptionUserPrivilege excepUser=(ExceptionUserPrivilege)event.getComponent().getAttributes().get("excepDet");
			userExceptionDetail.setUserExceptionCodeUser(excepUser.getUserAccount().getIdUserPk());
			userExceptionDetail.setInitialDate(excepUser.getInitialDate());
			userExceptionDetail.setFinalDate(excepUser.getEndDate());
			userExceptionDetail.setUserExceptionName(excepUser.getName());
			userExceptionDetail.setUserExceptionDescription(excepUser.getDescription());
			userExceptionDetail.setIdPrivelegeException(excepUser.getIdExceptionUserPrivilegePk());
			userExceptionDetail.setBlockMotive(excepUser.getBlockMotive());
			userExceptionDetail.setUnblockMotive(excepUser.getUnblockMotive());
			userExceptionDetail.setBlockOtherMotive(excepUser.getBlockOtherMotive());
			userExceptionDetail.setUnblockOtherMotive(excepUser.getUnblockOtherMotive());
			userExceptionDetail.setState(excepUser.getState());
			userExceptionDetail.setUsuLogin(excepUser.getUserAccount().getLoginUser());
			userExceptionDetail.setFileName(excepUser.getNameFile());
			//load lazy file
			userExceptionDetail.setLoadFile(systemServiceFacade.getExceptionUserPrivilegeFile(excepUser.getIdExceptionUserPrivilegePk()));
			userExceptionDetail.setIdSystemSelected(excepUser.getSystem().getIdSystemPk());
			selectedSystem=excepUser.getSystem().getIdSystemPk();
	        Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();	
			String localeES=LanguageType.SPANISH.getValue();
			String localeEN=LanguageType.ENGLISH.getValue();
			String localeFR=LanguageType.FRANCES.getValue();
			if(locale.toString().equals(localeES)){
				userExceptionDetail.setUserExceptionLanguage(LanguageType.SPANISH.getCode());
			}
			if(locale.toString().equals(localeEN)){
				userExceptionDetail.setUserExceptionLanguage(LanguageType.ENGLISH.getCode());
			}
	        if(locale.toString().equals(localeFR)){
	        	userExceptionDetail.setUserExceptionLanguage(LanguageType.FRANCES.getCode());
			}
			
				
			List<Object[]> listExceptionOptionPrivilege = systemServiceFacade.searchOptionPrivilegesExceptionServiceFacade(userExceptionDetail);
			
			if(!listExceptionOptionPrivilege.isEmpty()){
				List<Integer> listPrivileges = new ArrayList<Integer>();
				
				for (Object[] arrFilaPrivilegio : listExceptionOptionPrivilege) {
					listPrivileges.add(new Integer(arrFilaPrivilegio[0].toString()));
				}
				
				
				userExceptionDetail.setPrivelegeListPK(listPrivileges);
				
				List<Integer> listOptions  = systemServiceFacade.listIdOptionsByPrivilegeServiceFacade(userExceptionDetail);
				
				
				userExceptionDetail.setOptionsListPK(listOptions);
				
				
				List<Object[]> listSystemOption = systemServiceFacade.listSystemOptionByIdServiceFacade(userExceptionDetail);
				
				rootPrivileges = new DefaultTreeNode("Root", null);			
				
				List<Object[]> listaOpcionesPadres = getParentOptions(listSystemOption);
				
				for (Object[] arrFila : listaOpcionesPadres) {
					loadingOptionsPrivilegeRecursive(arrFila,listSystemOption, rootPrivileges);
				}
				
				
				if(rootPrivileges.getChildCount() > 0) {
					
					for (Object[] arrFilaPrivilegio : listExceptionOptionPrivilege) {
						loadingPrivilegeNodeRecursive(arrFilaPrivilegio,rootPrivileges);
					}
					
					contractPrivileges();
				}
				
			}
		}catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	@LoggerAuditWeb
	public void beforeOnConfirm(ActionEvent ae){
		//hideDialogues();
		this.setOperationType(ViewOperationsType.CONFIRM.getCode());
		if(exceptionUserPrivilegeArray.length<1){
			Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
	    	Object[] headData = new Object[1];
			headData[0]=PropertiesUtilities.getMessage(locale,"btn.confirm");
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT,headData,
					"error.adm.exception.user.privileges.select.confirm.label",null);
			JSFUtilities.showComponent("formGeneral:dialogWarning");
		}else if(!exceptionUserPrivilegeArray[0].getState().equals(UserExceptionStateType.REGISTERED.getCode())){
			JSFUtilities.showComponent("cnfEndTransactionSamePage");
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT,null,
					"error.adm.exception.user.not.selected",null);
			return;
		}else{
//			try {
				showMotiveText = false;
				motiveSelected = GeneralConstants.NEGATIVE_ONE_INTEGER;
				showMessageOnDialog("lbl.confirm.header.record",null,"adm.exception.user.confirm.confirmation",null);			
				JSFUtilities.executeJavascriptFunction("PF('dglConfirmException').show()");
				//JSFUtilities.showComponent("formGeneral:dialogConfirmException");
				motiveText="";
//				lstMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.USER_EXCEPTION_DELETE_MOTIVES.getCode());
//			} catch (ServiceException e) {
//				e.printStackTrace();
//			}
		}
	}
	
	
	public void beforeOnReject(ActionEvent actionEvent){
		//hideDialogues();
	    this.setOperationType(ViewOperationsType.REJECT.getCode());
	    if(exceptionUserPrivilegeArray.length<1){
			Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
	    	Object[] headData = new Object[1];
			headData[0]=PropertiesUtilities.getMessage(locale,"btn.reject");
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT,headData,
					PropertiesConstants.ERROR_ADM_EXCEPTION_USER_PRIVILEGES_SELECT_DELETE_LABEL,null);
			JSFUtilities.showComponent("formGeneral:dialogWarning");
		} if(exceptionUserPrivilegeArray.length>1){
			Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
	    	Object[] headData = new Object[1];
			headData[0]=PropertiesUtilities.getMessage(locale,"btn.reject");
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT,headData,
					"error.msg.select.onerecord.reject",null);
			JSFUtilities.showComponent("formGeneral:dialogWarning");
		} else if(!exceptionUserPrivilegeArray[0].getState().equals(UserExceptionStateType.REGISTERED.getCode())){
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT,null,
					"error.adm.exception.user.not.registered",null);
			JSFUtilities.showComponent("cnfEndTransactionSamePage");
			return;
		}else{
			try {
				showMotiveText = false;
				motiveSelected = GeneralConstants.NEGATIVE_ONE_INTEGER;
				showMessageOnDialog(PropertiesConstants.LBL_MOTIVE_DELETE,null,null,null);			
				JSFUtilities.executeJavascriptFunction("PF('dlgMotive').show()");
				//JSFUtilities.showComponent("formGeneral:dialogMotive");
				motiveText="";
				showConfirmDelete=false;
				lstMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.USER_EXCEPTION_REJECTED_MOTIVES.getCode());
			} catch (ServiceException e) {
				e.printStackTrace();
			}
			
		}
	}
	
	/**
	 * Before on delete.
	 *
	 * @return the string
	 */
	public void beforeOnDelete(ActionEvent actionEvent){
	    //hideDialogues();
	    this.setOperationType(ViewOperationsType.DELETE.getCode());
	    if(exceptionUserPrivilegeArray.length<1){
			Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
	    	Object[] headData = new Object[1];
			headData[0]=PropertiesUtilities.getMessage(locale,"btn.delete");
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT,headData,
					PropertiesConstants.ERROR_ADM_EXCEPTION_USER_PRIVILEGES_SELECT_DELETE_LABEL,null);
			JSFUtilities.showComponent("formGeneral:dialogWarning");
		}else if(!exceptionUserPrivilegeArray[0].getState().equals(UserExceptionStateType.CONFIRMED.getCode())){
			JSFUtilities.showComponent("cnfEndTransactionSamePage");
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT,null,
					"error.adm.exception.user.not.confirmed",null);
			return;
		}else{
			try {
				showMessageOnDialog("lbl.motive.reject",null,null,null);	
				JSFUtilities.executeJavascriptFunction("PF('dlgMotive').show()");
				//JSFUtilities.showComponent("formGeneral:dialogMotive");
				lstMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.USER_EXCEPTION_DELETE_MOTIVES.getCode());
				ExceptionUserPrivilege exepPrivilege = exceptionUserPrivilegeArray[0];
				if(exepPrivilege.getSituation() != null && exepPrivilege.getSituation().equals(UserExceptionSituationType.ACTIVE.getCode())){
					showMotiveText = false;
					motiveText="";
					motiveSelected = GeneralConstants.NEGATIVE_ONE_INTEGER;
					showConfirmDelete=false;
				} else {
					//load data saved
					if(exepPrivilege.getUnblockMotive() != null && exepPrivilege.getUnblockMotive().equals(OtherMotiveType.USER_EXCEPTION_DELETE_OTHER_MOTIVE.getCode())){
					showMotiveText = true;
					} else {
						showMotiveText = false;
					}
					motiveText = exepPrivilege.getUnblockOtherMotive();
					motiveSelected = exepPrivilege.getUnblockMotive();
					showConfirmDelete=true;
				}
				
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		
	}
	/**
	 * Before on block.
	 *
	 * @return the string
	 */
	public void beforeOnBlock(ActionEvent actionEvent){
		
		//hideDialogues();
		this.setOperationType(ViewOperationsType.BLOCK.getCode());
		
		if(exceptionUserPrivilegeArray.length<1){
			
			Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
			Object[] headData = new Object[1];
			headData[0]=PropertiesUtilities.getMessage(locale,"btn.block");
			
			
			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT,headData,PropertiesConstants.ERROR_ADM_EXCEPTION_USER_PRIVILEGES_SELECT_DELETE_LABEL,null);
			
			JSFUtilities.showComponent("formGeneral:dialogWarning");
			
			
			
		}
		else{
			boolean selection=true;
			for(int i=0;i<exceptionUserPrivilegeArray.length;i++){
				if(UserExceptionStateType.ANNULLED.getCode().equals(exceptionUserPrivilegeArray[i].getState()) || UserExceptionStateType.DELETED.getCode().equals(exceptionUserPrivilegeArray[i].getState())){
					selection=false;
				}
			}
			if(selection){
				//showMotives();
				showMotiveText = false;
				motiveSelected = GeneralConstants.NEGATIVE_ONE_INTEGER;
				showMessageOnDialog(PropertiesConstants.LBL_MOTIVE_BLOCK,null,null,null);			
				JSFUtilities.executeJavascriptFunction("PF('dlgMotive').show()");
				//JSFUtilities.showComponent("formGeneral:dialogMotive");
				motiveText="";
			}else{
				JSFUtilities.showComponent("cnfEndTransactionSamePage");
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT,null,
						PropertiesConstants.ERROR_WRONG_RECORD_SELECTED,null);
			}
			
		}
		
	}
	/**
	 * Before on unblock.
	 *
	 * @return the string
	 */
	public void beforeOnUnblock(ActionEvent actionEvent){
		
		//hideDialogues();
		this.setOperationType(ViewOperationsType.UNBLOCK.getCode());
		
		if(exceptionUserPrivilegeArray.length<1){
			
			Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
			Object[] headData = new Object[1];
			headData[0]=PropertiesUtilities.getMessage(locale,"btn.unblock");
			
			
			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT,headData,PropertiesConstants.ERROR_ADM_EXCEPTION_USER_PRIVILEGES_SELECT_DELETE_LABEL,null);
			
			JSFUtilities.showComponent("formGeneral:dialogWarning");
			
			
			
		}
		else{
			boolean selection=true;
			for(int i=0;i<exceptionUserPrivilegeArray.length;i++){
				if(UserExceptionStateType.REGISTERED.getCode().equals(exceptionUserPrivilegeArray[i].getState()) || UserExceptionStateType.DELETED.getCode().equals(exceptionUserPrivilegeArray[i].getState())){
					selection=false;
				}
			}
			if(selection){
				//showMotives();
				showMotiveText = false;
				motiveSelected = GeneralConstants.NEGATIVE_ONE_INTEGER;
				showMessageOnDialog(PropertiesConstants.LBL_MOTIVE_UNBLOCK,null,null,null);			
				JSFUtilities.executeJavascriptFunction("PF('dlgMotive').show()");
				//JSFUtilities.showComponent("formGeneral:dialogMotive");
				motiveText="";
			}else{
				JSFUtilities.showComponent("cnfEndTransactionSamePage");
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT,null,
						PropertiesConstants.ERROR_WRONG_RECORD_SELECTED,null);
			}
			
		}
		
	}
	
	/**
	 * Load privileges by system.
	 */
	public void loadPrivilegesBySystem(){
		try{
			selectedNodes=null;
			userException.setIdSystemSelected(selectedSystem);
			
	        Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();	
			
			String localeES=LanguageType.SPANISH.getValue();
			String localeEN=LanguageType.ENGLISH.getValue();
			String localeFR=LanguageType.FRANCES.getValue();
			
			if(locale.toString().equals(localeES)){
				userException.setUserExceptionLanguage(LanguageType.SPANISH.getCode());
			}
			if(locale.toString().equals(localeEN)){
				userException.setUserExceptionLanguage(LanguageType.ENGLISH.getCode());
			}
	        if(locale.toString().equals(localeFR)){
	        	userException.setUserExceptionLanguage(LanguageType.FRANCES.getCode());
			}
			
			userException.setUserExceptionOptionState(OptionStateType.CONFIRMED.getCode());
			userException.setUserExceptionPrivelegeState(OptionPrivilegeStateType.REGISTERED.getCode());
			
			List<Object[]> listSystemOptions = systemServiceFacade.listSystemOptionsByLanguageUserExceptionServiceFacade(userException);
	        
			if(listSystemOptions.isEmpty()){
				rootPrivileges = null;		
				
				JSFUtilities.addContextMessage("frmUserOptionExceptionMgmt:cboSystem",FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_EXCEPCION_USER_PRIVILEDGES_NO_EXIST),
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_EXCEPCION_USER_PRIVILEDGES_NO_EXIST));
				return;
			}
				
			rootPrivileges = new DefaultTreeNode("Root", null);			
			
			List<Object[]> listParentOptions = getParentOptions(listSystemOptions);
			
			for (Object[] arrRow : listParentOptions) {
				loadingOptionsPrivilegeRecursive(arrRow,listSystemOptions, rootPrivileges);
			}
			
			
			if(rootPrivileges.getChildCount()>0){
				//lista de privilegios de todas las opciones del sistema seleccionado
				List<Object[]>  listaPrivilegiosOpciones = systemServiceFacade.listOptionPrivilegeServiceFacade(userException);
				
				if( listaPrivilegiosOpciones.isEmpty()){
					rootPrivileges = null;
					
					JSFUtilities.addContextMessage("frmUserOptionExceptionMgmt:cboSystem",FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_EXCEPCION_USER_PRIVILEDGES_NO_EXIST),
							PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_EXCEPCION_USER_PRIVILEDGES_NO_EXIST));
					return;
				}
				
				for (Object[] arrFilaPrivilegio :  listaPrivilegiosOpciones) {
					loadingPrivilegeNodeRecursive(arrFilaPrivilegio,rootPrivileges);
				}
				
				contractPrivileges();
			} else {
				
				JSFUtilities.addContextMessage("frmUserOptionExceptionMgmt:cboSystem",FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_EXCEPCION_USER_PRIVILEDGES_NO_EXIST),
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_EXCEPCION_USER_PRIVILEDGES_NO_EXIST));
				
			}
			loadUserPriviledges();
			if(!userProfiles.isEmpty()){					
				for (ProfilePrivilege profilePrivilege : userProfiles) {
					checkPrivilegeSelectionRecursive(profilePrivilege,rootPrivileges);					
				}					
			}
		}catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		
	}
	
	/**
	 * To load userPriviledges
	 */
	public void loadUserPriviledges(){
		try{
		userProfiles = systemServiceFacade.loadUserPriviledgesServiceFacade(userException.getUserExceptionCodeUser(),userException.getIdSystemSelected());
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Gets the parent options.
	 *
	 * @param listaOpcioncesSistema the lista opcionces sistema
	 * @return the parent options
	 */
	private List<Object[]> getParentOptions(List<Object[]> listaOpcioncesSistema){		
		List<Object[]> listaOpcionesPadres = new ArrayList<Object[]>();
					
		for (Object[] arrFila : listaOpcioncesSistema) {
			if (arrFila[1] == null ) {				
				listaOpcionesPadres.add(arrFila);
			}
		}
		
		return listaOpcionesPadres;
	}
	
	/**
	 * Loading options privilege recursive.
	 *
	 * @param arrFila the arr fila
	 * @param listSystemOption the list system option
	 * @param rootNode the root node
	 */
	private void loadingOptionsPrivilegeRecursive(
			Object[] arrFila, List<Object[]> listSystemOption, TreeNode rootNode){
		
		Integer idSystemOptionPk = null;
		Integer idParentSystemOption = null;
		
		OptionPrivilegeNodeBean optionPrivilegeNodeBean = new OptionPrivilegeNodeBean();		
				
		if(arrFila[0]!=null){
			idSystemOptionPk = new Integer(arrFila[0].toString());
			optionPrivilegeNodeBean.setIdSystemOption(idSystemOptionPk);
		}
		
		if(arrFila[1]!=null){
			idParentSystemOption = new Integer(arrFila[1].toString());
			optionPrivilegeNodeBean.setIdParentSystemOption(idParentSystemOption);
		}
		

		if(arrFila[2]!=null){
			optionPrivilegeNodeBean.setName(arrFila[2].toString());
		}
		
		TreeNode nodo = new DefaultTreeNode(optionPrivilegeNodeBean, rootNode);

			Integer idOpcionSistemaPadreHijo = null;
			
			for (Object[] objArrFilaHija : listSystemOption) {
				if(objArrFilaHija[1]!=null){
					idOpcionSistemaPadreHijo = new Integer(objArrFilaHija[1].toString());				
					
					if(idSystemOptionPk.intValue()==idOpcionSistemaPadreHijo.intValue()){						
					   loadingOptionsPrivilegeRecursive(objArrFilaHija, listSystemOption, nodo);					   					   
					}
					
				}
			}
				
	}
	
    /**
     * Loading privilege node recursive.
     *
     * @param arrRowPrivilege the arr row privilege
     * @param rootNode the root node
     */
    private void loadingPrivilegeNodeRecursive(Object[] arrRowPrivilege, TreeNode rootNode){
		if(rootNode.getData() instanceof OptionPrivilegeNodeBean){
			OptionPrivilegeNodeBean dataNodeOption = (OptionPrivilegeNodeBean) rootNode.getData();
			
			if(dataNodeOption.getIdSystemOption()!=null){

				if(dataNodeOption.getIdSystemOption().equals(new Integer(arrRowPrivilege[2].toString()))){
					
					OptionPrivilegeNodeBean optionPrivilegeNodeBean = new OptionPrivilegeNodeBean();
					optionPrivilegeNodeBean.setIdOptionPrivilege(new Integer(arrRowPrivilege[0].toString()));
					optionPrivilegeNodeBean.setIdPrivilege(new Integer(arrRowPrivilege[1].toString()));
					optionPrivilegeNodeBean.setName(PrivilegeType.get(new Integer(arrRowPrivilege[1].toString())).getDescription());
					
					new DefaultTreeNode(optionPrivilegeNodeBean, rootNode);
					
				} else{
					if(rootNode.getChildCount()>0){
						for (TreeNode nodoHijo : rootNode.getChildren()) {
							loadingPrivilegeNodeRecursive(arrRowPrivilege, nodoHijo);
						}
					}
				}
			}
									
		} else {
			if(rootNode.getChildCount()>0){
				for (TreeNode nodoHijo : rootNode.getChildren()) {
					loadingPrivilegeNodeRecursive(arrRowPrivilege, nodoHijo);
				}
			}
		}
	}
    
    /**
     * Contract privileges.
     */
    public void contractPrivileges(){		
		contractPrivilegeRecursive(rootPrivileges);
		expandNode = false;
	}
    
    /**
     * Contract privilege recursive.
     *
     * @param node the node
     */
    public void contractPrivilegeRecursive(TreeNode node){
		node.setExpanded(false);
		if(node.getChildCount()>0){
			List<TreeNode> lstNodosHijos = node.getChildren();
			for (TreeNode sonNode : lstNodosHijos) {
				contractPrivilegeRecursive(sonNode);
			}
		}
	}
    
    /**
     * Initial save.
     */
    public void initialSave(){
    	try{
	    	hideDialogues();
	    	String msg = null;
	    	
	    	if(userException.getInitialDate().getTime()>userException.getFinalDate().getTime()){
	    		msg = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_EXCEPTIONUSER_DATE_INITIAL);
		        JSFUtilities.addContextMessage("frmUserOptionExceptionMgmt:calFecIni", FacesMessage.SEVERITY_ERROR, msg, msg);	
		        JSFUtilities.showEndTransactionSamePageDialog();
		        JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.ERROR_ADM_EXCEPTIONUSER_DATE_INITIAL, null);
		        return;
	    	}else if(userException.getFinalDate().getTime()==userException.getInitialDate().getTime()){
	    		msg = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_EXCEPTIONUSER_DATE_FINAL);
	    		JSFUtilities.addContextMessage("frmUserOptionExceptionMgmt:calFecEnd", FacesMessage.SEVERITY_ERROR, msg, msg);
	    		JSFUtilities.showEndTransactionSamePageDialog();
	    		JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.ERROR_ADM_EXCEPTIONUSER_DATE_FINAL, null);
	    		return;
	    	}
	    	//To perform validation whether the user is having the Exception with same name
	    	if(isRegister()){
	    		boolean result = systemServiceFacade.validateUserExceptionServiceFacade(userException.getUserExceptionName());
	    		if(!result){    			 		
	    			JSFUtilities.showComponent("frmUserOptionExceptionMgmt:cnfClearExceptionName");   	
	    			JSFUtilities.showMessageOnDialog(null, null, PropertiesConstants.USER_EXCEPTION_EXCEPTIONNAME_INVALID,null);
	    			return;
	    		}
	    	}
	    	
	    	OutputPanel opnlPrivileges = (OutputPanel)JSFUtilities.findViewComponent("frmUserOptionExceptionMgmt:divPrivileges");
	    	
	    	if(selectedNodes==null || (selectedNodes!=null && selectedNodes.length<1)){ 
	    		msg = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_EXCEPTION_USER_PRIVILEGES_SELECT_SAVE_LABEL);
	        	JSFUtilities.addContextMessage("frmUserOptionExceptionMgmt:cboSystem", FacesMessage.SEVERITY_ERROR, msg, msg);
	        	JSFUtilities.showEndTransactionSamePageDialog();
	        	JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, 
	        									 PropertiesConstants.ERROR_ADM_EXCEPTION_USER_PRIVILEGES_SELECT_SAVE_LABEL, null);
	//				JSFUtilities.showRequiredDialog();
				opnlPrivileges.setStyle("border: 1px solid #CD0A0A;");
				return;
	    	}else{
	    		opnlPrivileges.setStyle("");
	 			
	    		Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
	        	Object[] headData = new Object[1];
	    		headData[0] = PropertiesUtilities.getMessage(locale,"btn.register");
	    		Object[] datoCuerpo = new Object[1];
	    		datoCuerpo[0]=userException.getUserExceptionName().toUpperCase();
	        	
	    		if(ViewOperationsType.REGISTER.getCode().equals(getOperationType())){
	    			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_CONFIRM, headData, PropertiesConstants.ADM_EXCEPTION_USER_CONFIRM_REGISTER, datoCuerpo);
	    		}else{
	    			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_MODIFY, headData, PropertiesConstants.ADM_EXCEPTION_USER_CONFIRM_MODIFY, datoCuerpo);
	    		}
	    		JSFUtilities.showComponent("frmUserOptionExceptionMgmt:cnfAdmExcUsuPriv");   		
    		}
    	}catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
    }
    
    
    /**
     * Save.
     */
    @LoggerAuditWeb
    public void save(){
    	if(ViewOperationsType.REGISTER.getCode().equals(getOperationType())){
			register();			
		}
    	else if(ViewOperationsType.MODIFY.getCode().equals(getOperationType())){
    		register();
    	}
    	userExceptionFilter = new UserExceptionFilter();
    	userExceptionFilter.setInitialDate(CommonsUtilities.currentDate());
    	userExceptionFilter.setFinalDate(CommonsUtilities.currentDate());
    	
    }
    
    /**
     * Register.
     */    
    public void register(){
    	try{
	    	hideDialogues();
	    	List<ExceptionUserPrivilege> userExceptions = systemServiceFacade.searchUserExceptionsServiceFacade(userException);
			if(userExceptions.size() > 0){
				Object[] obj = new Object[2];
	    		obj[0] = userExceptions.get(0).getSystem().getName();
	    		obj[1] = userExceptions.get(0).getName();
				JSFUtilities.showEndTransactionSamePageDialog();
				JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.ERROR_VALIDATION_USER_EXCEPTION_ALREADY_EXISTS, obj);
				return;
			}
	    	exceptionUserPrivilege = new ExceptionUserPrivilege();
	    	exceptionUserPrivilege.setName(userException.getUserExceptionName().toUpperCase());
	    	exceptionUserPrivilege.setDescription(userException.getUserExceptionDescription().toUpperCase());
	    	exceptionUserPrivilege.setInitialDate(userException.getInitialDate());
	    	exceptionUserPrivilege.setEndDate(userException.getFinalDate());
	    	exceptionUserPrivilege.setNameFile(userException.getFileName());
	    	exceptionUserPrivilege.setDocumentFile(userException.getLoadFile());
	        exceptionUserPrivilege.setSystem(systemServiceFacade.getLastestSysemDetails(selectedSystem));
//	    	UserAccount aux = new UserAccount();
//	    	aux.setIdUserPk(userException.getUserExceptionCodeUser());
	        
	        UserAccount aux = new UserAccount();
	        aux.setLoginUser(userException.getUsuLogin());
	        UserAccount objUserAccount = userServiceFacade.getUserAccountByLogin(aux);
	        exceptionUserPrivilege.setUserAccount(objUserAccount);
	    	//exceptionUserPrivilege.setUserAccount(userException.getUserAccount());
    		exceptionUserPrivilege.setState(UserExceptionStateType.REGISTERED.getCode());
	    	if(ViewOperationsType.REGISTER.getCode().equals(getOperationType())){
	        	exceptionUserPrivilege.setRegistryDate(CommonsUtilities.currentDateTime());  
	        	exceptionUserPrivilege.setRegistryUser(userInfo.getUserAccountSession().getUserName());
	    	}else if(ViewOperationsType.MODIFY.getCode().equals(getOperationType())){
	    		/*exceptionUserPrivilege.setState(UserExceptionStateType.REGISTERED.getCode());*/
	    		exceptionUserPrivilege.setRegistryDate(userException.getRegistryDate());
	    		exceptionUserPrivilege.setRegistryUser(userException.getRegistryUser());
	    		exceptionUserPrivilege.setIdExceptionUserPrivilegePk(userException.getIdPrivelegeException());
	    		exceptionUserPrivilege.setVersion(userException.getVersion());
			}
	    	UserPrivilegeDetail userPrivilegeDetail = null;						
	    	OptionPrivilegeNodeBean optionPrivileNodeBean = null;
			OptionPrivilege optionPrivilege = null;
			List<UserPrivilegeDetail> listUserPrivilegeDetail = new ArrayList<UserPrivilegeDetail>();
	    	for (TreeNode nodo : selectedNodes) {
				optionPrivileNodeBean = (OptionPrivilegeNodeBean) nodo.getData();
				/*boolean isNew=true;
				for(ProfilePrivilege profilePrivilege : userProfiles){
					if(optionPrivileNodeBean.getIdOptionPrivilege() != null && optionPrivileNodeBean.getIdOptionPrivilege().intValue() == 
							profilePrivilege.getOptionPrivilege().getIdOptionPrivilegePk()) {
						isNew= false;
					}
				}*/
				//if(optionPrivileNodeBean.getIdOptionPrivilege() != null && isNew){
				if(optionPrivileNodeBean.getIdOptionPrivilege() != null){
					/*if(ViewOperationsType.MODIFY.getCode().equals(getOperationType()) && Validations.validateListIsNotNullAndNotEmpty(userException.getPrivelegeListPK()) && 
							userException.getPrivelegeListPK().contains(optionPrivileNodeBean.getIdOptionPrivilege())){
						userException.getPrivelegeListPK().remove(optionPrivileNodeBean.getIdOptionPrivilege());
						continue;
					}*/
					optionPrivilege = new OptionPrivilege();
					optionPrivilege.setIdOptionPrivilegePk(optionPrivileNodeBean.getIdOptionPrivilege());
					
					userPrivilegeDetail = new UserPrivilegeDetail();
					userPrivilegeDetail.setExceptionUserPrivilege(exceptionUserPrivilege);
					userPrivilegeDetail.setOptionPrivilege(optionPrivilege);	
					userPrivilegeDetail.setRegistryUser(userInfo.getUserAccountSession().getUserName());
					userPrivilegeDetail.setRegistryDate(CommonsUtilities.currentDateTime());				
					listUserPrivilegeDetail.add(userPrivilegeDetail);
				}
			}    	
			exceptionUserPrivilege.setUserPrivilegeDetails(listUserPrivilegeDetail);
			boolean result = false;
			if(ViewOperationsType.REGISTER.getCode().equals(getOperationType())){
				result = systemServiceFacade.registerPrivilegeUserExceptionServiceFacade(exceptionUserPrivilege);
			}else if(ViewOperationsType.MODIFY.getCode().equals(getOperationType())){
//				result = systemServiceFacade.deletePrivilegeUserExceptionServiceFacade(userException.getPrivelegeListPK());
				result = systemServiceFacade.modifyPrivilegeUserExceptionServiceFacade(exceptionUserPrivilege,userException.getPrivelegeListPK());
			}
			if(result){
				Object[] arrBodyData = {exceptionUserPrivilege.getName()};
				if (ViewOperationsType.REGISTER.getCode().equals(getOperationType())) {
					showMessageOnDialog(
							PropertiesConstants.DIALOG_HEADER_SUCCESS,null,
							PropertiesConstants.SUCCESS_ADM_EXCEPTION_USER_REGISTER_LABEL,arrBodyData);
				} else if (ViewOperationsType.MODIFY.getCode().equals(getOperationType())) {
					showMessageOnDialog(
							PropertiesConstants.DIALOG_HEADER_SUCCESS,null,
							PropertiesConstants.SUCCESS_ADM_EXCEPTION_USER_MODIFY_LABEL,arrBodyData);
				}
				clean();
				userProfiles = new ArrayList<ProfilePrivilege>();
				JSFUtilities.showEndTransactionDialog();
			}
    	} catch(ServiceException sex){
			if(sex.getMessage()!=null){
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION,sex.getErrorService().getMessage());
				JSFUtilities.showSimpleValidationDialog();
				clean();
				userProfiles = new ArrayList<ProfilePrivilege>();
			}
    	} catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		
    }
        
    
    /**
     * Expand privileges.
     */
    public void expandPrivileges(){		
		expandPrivilegeRecursive(rootPrivileges);
		expandNode = true;
	}
	
	/**
	 * Expand privilege recursive.
	 *
	 * @param nodo the nodo
	 */
	public void expandPrivilegeRecursive(TreeNode nodo){
		nodo.setExpanded(true);
		if(nodo.getChildCount()>0){
			List<TreeNode> lstNodosHijos = nodo.getChildren();
			for (TreeNode nodoHijo : lstNodosHijos) {
				expandPrivilegeRecursive(nodoHijo);
			}
		}		
	}
    

	public String getmapSituationsExceptionDesc(Integer idParameterPk) {
		String desc = null;
		desc =  situationsException.get(idParameterPk);
		return desc != null ? desc : "POR CONFIRMAR"; 
	}
    /**
     * To return.
     *
     * @return the string
     */
    public String toReturn(){
    	exceptionUserPrivilege = null;
    	
    	return "main";
    }

	/**
	 * Gets the exception user privilege array.
	 *
	 * @return the exception user privilege array
	 */
	public ExceptionUserPrivilege[] getExceptionUserPrivilegeArray() {
		return exceptionUserPrivilegeArray;
	}

	/**
	 * Sets the exception user privilege array.
	 *
	 * @param exceptionUserPrivilegeArray the new exception user privilege array
	 */
	public void setExceptionUserPrivilegeArray(
			ExceptionUserPrivilege[] exceptionUserPrivilegeArray) {
		this.exceptionUserPrivilegeArray = exceptionUserPrivilegeArray;
	}

	/**
	 * Gets the user exception.
	 *
	 * @return the user exception
	 */
	public UserExceptionFilter getUserException() {
		return userException;
	}

	/**
	 * Sets the user exception.
	 *
	 * @param userException the new user exception
	 */
	public void setUserException(UserExceptionFilter userException) {
		this.userException = userException;
	}

	/**
	 * Gets the user exception filter.
	 *
	 * @return the user exception filter
	 */
	public UserExceptionFilter getUserExceptionFilter() {
		return userExceptionFilter;
	}

	/**
	 * Sets the user exception filter.
	 *
	 * @param userExceptionFilter the new user exception filter
	 */
	public void setUserExceptionFilter(UserExceptionFilter userExceptionFilter) {
		this.userExceptionFilter = userExceptionFilter;
	}

	/**
	 * Gets the user exception detail.
	 *
	 * @return the user exception detail
	 */
	public UserExceptionFilter getUserExceptionDetail() {
		return userExceptionDetail;
	}

	/**
	 * Sets the user exception detail.
	 *
	 * @param userExceptionDetail the new user exception detail
	 */
	public void setUserExceptionDetail(UserExceptionFilter userExceptionDetail) {
		this.userExceptionDetail = userExceptionDetail;
	}

	/**
	 * Gets the exception user data model.
	 *
	 * @return the exception user data model
	 */
	public ExceptionUserDataModel getExceptionUserDataModel() {
		return exceptionUserDataModel;
	}

	/**
	 * Sets the exception user data model.
	 *
	 * @param exceptionUserDataModel the new exception user data model
	 */
	public void setExceptionUserDataModel(
			ExceptionUserDataModel exceptionUserDataModel) {
		this.exceptionUserDataModel = exceptionUserDataModel;
	}

	/**
	 * Gets the exception user privilege.
	 *
	 * @return the exception user privilege
	 */
	public ExceptionUserPrivilege getExceptionUserPrivilege() {
		return exceptionUserPrivilege;
	}

	/**
	 * Sets the exception user privilege.
	 *
	 * @param exceptionUserPrivilege the new exception user privilege
	 */
	public void setExceptionUserPrivilege(
			ExceptionUserPrivilege exceptionUserPrivilege) {
		this.exceptionUserPrivilege = exceptionUserPrivilege;
	}

	/**
	 * Gets the user data model.
	 *
	 * @return the user data model
	 */
	public UserDataModel getUserDataModel() {
		return userDataModel;
	}

	/**
	 * Sets the user data model.
	 *
	 * @param userDataModel the new user data model
	 */
	public void setUserDataModel(UserDataModel userDataModel) {
		this.userDataModel = userDataModel;
	}

	/**
	 * Gets the selected system.
	 *
	 * @return the selected system
	 */
	public Integer getSelectedSystem() {
		return selectedSystem;
	}

	/**
	 * Sets the selected system.
	 *
	 * @param selectedSystem the new selected system
	 */
	public void setSelectedSystem(Integer selectedSystem) {
		this.selectedSystem = selectedSystem;
	}

	/**
	 * Gets the root privileges.
	 *
	 * @return the root privileges
	 */
	public TreeNode getRootPrivileges() {
		return rootPrivileges;
	}

	/**
	 * Sets the root privileges.
	 *
	 * @param rootPrivileges the new root privileges
	 */
	public void setRootPrivileges(TreeNode rootPrivileges) {
		this.rootPrivileges = rootPrivileges;
	}

	/**
	 * Gets the selected nodes.
	 *
	 * @return the selected nodes
	 */
	public TreeNode[] getSelectedNodes() {
		return selectedNodes;
	}

	/**
	 * Sets the selected nodes.
	 *
	 * @param selectedNodes the new selected nodes
	 */
	public void setSelectedNodes(TreeNode[] selectedNodes) {
		this.selectedNodes = selectedNodes;
	}

	/**
	 * Checks if is expand node.
	 *
	 * @return true, if is expand node
	 */
	public boolean isExpandNode() {
		return expandNode;
	}

	/**
	 * Sets the expand node.
	 *
	 * @param expandNode the new expand node
	 */
	public void setExpandNode(boolean expandNode) {
		this.expandNode = expandNode;
	}

	/**
	 * Gets the full name.
	 *
	 * @return the full name
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * Sets the full name.
	 *
	 * @param fullName the new full name
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * Checks if is show warning.
	 *
	 * @return true, if is show warning
	 */
	public boolean isShowWarning() {
		return showWarning;
	}

	/**
	 * Sets the show warning.
	 *
	 * @param showWarning the new show warning
	 */
	public void setShowWarning(boolean showWarning) {
		this.showWarning = showWarning;
	}

	/**
	 * Checks if is show panel options component.
	 *
	 * @return true, if is show panel options component
	 */
	public boolean isShowPanelOptionsComponent() {
		return showPanelOptionsComponent;
	}

	/**
	 * Sets the show panel options component.
	 *
	 * @param showPanelOptionsComponent the new show panel options component
	 */
	public void setShowPanelOptionsComponent(boolean showPanelOptionsComponent) {
		this.showPanelOptionsComponent = showPanelOptionsComponent;
	}

	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public boolean isShowMotiveText() {
		return showMotiveText;
	}

	public void setShowMotiveText(boolean showMotiveText) {
		this.showMotiveText = showMotiveText;
	}

	public String getMotiveText() {
		return motiveText;
	}

	public void setMotiveText(String motiveText) {
		this.motiveText = motiveText;
	}

	public Integer getMotiveSelected() {
		return motiveSelected;
	}

	public void setMotiveSelected(Integer motiveSelected) {
		this.motiveSelected = motiveSelected;
	}
   
	/**
	 * For Block Descripcion
	 * @return
	 */
	public String getMotiveDescripcion(){
	    String desc = "";
	    if(userExceptionDetail.getBlockMotive() != null){
			if(lstBlockMotives != null && lstBlockMotives.size() > 0 ){
				for(int i=0;i<lstBlockMotives.size();i++){
					if(userExceptionDetail.getBlockMotive().compareTo(lstBlockMotives.get(i).getIdParameterPk()) == 0){
							desc=lstBlockMotives.get(i).getName();
						}
					}
				}
			}
		return desc;
	}	
		/**
		 * For unBlock Descripcion
		 * @return
		 */
	public String getMotiveunBlockDescripcion(){
	    String desc = "";
	    if(userExceptionDetail.getUnblockMotive()!= null){
			if(lstUnBlockMotives != null && lstUnBlockMotives.size() > 0 ){
				for(int i=0;i<lstUnBlockMotives.size();i++){
					if(userExceptionDetail.getUnblockMotive().compareTo(lstUnBlockMotives.get(i).getIdParameterPk()) == 0){
							desc=lstUnBlockMotives.get(i).getName();
						}
					}
				}
			}
		return desc;
	}

	/**
	 * Check privilege selection recursive.
	 *
	 * @param profilePrivilege the profile privilege
	 * @param node the node
	 */
	private void checkPrivilegeSelectionRecursive(ProfilePrivilege profilePrivilege, TreeNode node){
		
		if(node.getData() instanceof OptionPrivilegeNodeBean){
			OptionPrivilegeNodeBean optionPrivilegeNodeBean = (OptionPrivilegeNodeBean) node.getData();
			
			if(optionPrivilegeNodeBean.getIdOptionPrivilege()!=null){
																				
				if(optionPrivilegeNodeBean.getIdOptionPrivilege().intValue() == profilePrivilege.getOptionPrivilege().getIdOptionPrivilegePk()){
					optionPrivilegeNodeBean.setIdProfilePrivilege(profilePrivilege.getIdProfilePrivilegePk());
					node.setSelected(true);
					validateSelectedParentRecursive(node);
					return;
				}
				
			} else {
				if(node.getChildCount()>0){
					for (TreeNode childNode : node.getChildren()) {
						checkPrivilegeSelectionRecursive(profilePrivilege, childNode);
					}
				}
			}
									
		} else {
			if(node.getChildCount()>0){
				for (TreeNode childNode : node.getChildren()) {
					checkPrivilegeSelectionRecursive(profilePrivilege, childNode);
				}
			}
		}
	}

	/**
	 * @return the userProfiles
	 */
	public List<ProfilePrivilege> getUserProfiles() {
		return userProfiles;
	}

	/**
	 * @param userProfiles the userProfiles to set
	 */
	public void setUserProfiles(List<ProfilePrivilege> userProfiles) {
		this.userProfiles = userProfiles;
	}
	
	
	/**
	 * To validate selection
	 * @param event
	 */
	public void onNodeUnSelect(NodeUnselectEvent event){
		TreeNode node = event.getTreeNode();
		if(node.getChildCount()>0){
			getChildNodes(node);
		} else{
			if(node.getData() instanceof OptionPrivilegeNodeBean){
				OptionPrivilegeNodeBean optionPrivilegeNodeBean = (OptionPrivilegeNodeBean) node.getData();
				
				if(optionPrivilegeNodeBean.getIdOptionPrivilege()!=null){
					
					for(ProfilePrivilege profilePrivilege : userProfiles){
						if(optionPrivilegeNodeBean.getIdOptionPrivilege().intValue() == profilePrivilege.getOptionPrivilege().getIdOptionPrivilegePk()){
							node.setSelected(true);
							validateSelectedParentRecursive(node);
							break;
						}
					}
				}
			}
		}
	}
	private void getChildNodes(TreeNode node){
			List<TreeNode> listTreeNodes= node.getChildren();
			for (TreeNode treeNode : listTreeNodes) {
				if(treeNode.getChildCount()>0){
					getChildNodes(treeNode);
				}else{
					if(treeNode.getData() instanceof OptionPrivilegeNodeBean){
						OptionPrivilegeNodeBean optionPrivilegeNodeBean = (OptionPrivilegeNodeBean) treeNode.getData();
						
						if(optionPrivilegeNodeBean.getIdOptionPrivilege()!=null){
							
							for(ProfilePrivilege profilePrivilege : userProfiles){
								if(optionPrivilegeNodeBean.getIdOptionPrivilege().intValue() == profilePrivilege.getOptionPrivilege().getIdOptionPrivilegePk()){
									treeNode.setSelected(true);
									validateSelectedParentRecursive(treeNode);
									break;
								}
							}
						}
					}
				}
			}
	}

	/**
	 * @return the currentDate
	 */
	public Date getCurrentDate() {
		return CommonsUtilities.currentDateTime();
	}
	
	@PreDestroy
	void cleanup()
	{
		endConversation();
	}
	
	/**
	 * Close registration confirmation dialog
	 */
	public void closeSaveConfirmationDialog(){
		hideDialogues();
		//JSFUtilities.hideComponent("frmUserOptionExceptionMgmt:cnfAdmExcUsuPriv");   		
	}
	
	/**
	 * Clear the exception name when it is already register
	 */
	public void clearExceptionName(){
		hideDialogues();
		userException.setUserExceptionName(null);  
		String msg = PropertiesUtilities.getMessage(PropertiesConstants.USER_EXCEPTION_EXCEPTIONNAME_INVALID);	
		JSFUtilities.addContextMessage("frmUserOptionExceptionMgmt:txtNameException", FacesMessage.SEVERITY_ERROR,msg , msg);
		 	
	}

	public boolean isBlCurrentLoadFile() {
		return blCurrentLoadFile;
	}

	public void setBlCurrentLoadFile(boolean blCurrentLoadFile) {
		this.blCurrentLoadFile = blCurrentLoadFile;
	}

	public Map<Integer,String> getSituationsException() {
		return situationsException;
	}

	public void setSituationsException(Map<Integer,String> situationsException) {
		this.situationsException = situationsException;
	}

	public boolean isShowConfirmDelete() {
		return showConfirmDelete;
	}

	public void setShowConfirmDelete(boolean showConfirmDelete) {
		this.showConfirmDelete = showConfirmDelete;
	}
}
