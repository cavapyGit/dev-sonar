package com.pradera.security.systemmgmt.view;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;

import org.primefaces.push.PushContext;
import org.primefaces.push.PushContextFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.httpevent.publish.BrowserWindow;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.security.model.type.UserAccountStateType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.audit.facade.MonitoringUserAuditFacade;
import com.pradera.security.model.ExceptionUserPrivilege;
import com.pradera.security.model.ScheduleException;
import com.pradera.security.model.ScheduleExceptionDetail;
import com.pradera.security.model.UserAccount;
import com.pradera.security.model.type.ScheduleExceptionDetailStateType;
import com.pradera.security.model.type.ScheduleExceptionStateType;
import com.pradera.security.model.type.ScheduleExceptionType;
import com.pradera.security.model.type.UserExceptionStateType;
import com.pradera.security.systemmgmt.facade.SystemServiceFacade;
import com.pradera.security.systemmgmt.view.filters.ScheduleExceptionFilter;
import com.pradera.security.systemmgmt.view.filters.UserExceptionFilter;
import com.pradera.security.usermgmt.facade.UserServiceFacade;
import com.pradera.security.usermgmt.view.filters.UserAccountFilter;
import com.pradera.security.utils.view.PropertiesConstants;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ScheduleExceptionBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 24/08/2013
 */
@Stateless
public class ScheduleExceptionBatch {
	
	/** The Constant logger. */
	private static final Logger logger = LoggerFactory.getLogger(ScheduleExceptionBatch.class);
	/** Schedule day of week*/
	private static final String DAY_OF_WEEK="*";
	/** schedule hour */
	private static final String HOUR="*";
	/** schedule minute */
	private static final String MINUTE="*";
	/**schedule second */
	private static final String SECOND="1";
	/**schedule Year */
	private static final String YEAR="2013";
	
	/** The system service facade. */
	@EJB
	protected SystemServiceFacade systemServiceFacade;
	
	@EJB
	protected UserServiceFacade userServiceFacade;
	
	@EJB
	protected MonitoringUserAuditFacade userAuditFacade;
	
	/**schedules defined*/
	@Schedule(dayOfWeek = DAY_OF_WEEK, hour = HOUR, minute = MINUTE, second = SECOND,year=YEAR, persistent = false)
	public void executeScheduleExceptionBatch() {
		automaticChangeStatus();
		automaticUserExceptionStatus();
		logOutUser();
		logoutBlockedUser();
	}
	
	/**
	 * Here changing the schedule exception status
	 */
	public void automaticChangeStatus(){
		logger.info("Security Schedule exception batch process started");
		try {
			ScheduleExceptionFilter scheduleException=new ScheduleExceptionFilter();
			scheduleException.setScheduleExceptionState(ScheduleExceptionStateType.REGISTERED.getCode());
			
			List<ScheduleException> scheduleExceptionList = systemServiceFacade.getScheduleExceptionServiceFacade(scheduleException);
			for(ScheduleException exception:scheduleExceptionList){
				if(exception.getEndDate().before(CommonsUtilities.currentDate())){
					logger.info("Schedule Exception End Hour : "+exception.getEndHour());
					logger.info("Schedule Exception End Hour 2222 : "+CommonsUtilities.getDefaultDateWithCurrentTime());
					Calendar calendar=Calendar.getInstance();
					calendar.setTime(exception.getEndHour());
					if(calendar.getTime().before(CommonsUtilities.currentDateTime())){
						exception = scheduleDetailsStaus(exception);
						systemServiceFacade.modifyScheduleExceptionServiceFacade(exception);
					}
				}
				else if(exception.getEndDate().equals(CommonsUtilities.currentDate())){
					Calendar calendar=Calendar.getInstance();
					calendar.setTime(exception.getEndHour());
					if(calendar.getTime().before(CommonsUtilities.getDefaultDateWithCurrentTime())){
						logger.info("Previous dates  states changed");
						exception = scheduleDetailsStaus(exception);
						systemServiceFacade.modifyScheduleExceptionServiceFacade(exception);
					}
				}
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	/**Here changing the schedule exception details status
	 * 
	 * @param exception
	 * @return
	 * @throws ServiceException
	 */
	public ScheduleException scheduleDetailsStaus(ScheduleException exception){
		try {
			logger.info("Security USER exception batch process started");
			List<ScheduleExceptionDetail> statusChange=null;
			ScheduleException temp = systemServiceFacade.getScheduleExceptionServiceFacade(exception);
			List<ScheduleExceptionDetail> detailsList = temp.getScheduleExceptionDetails();
			statusChange=new ArrayList<ScheduleExceptionDetail>();
			for(ScheduleExceptionDetail exceptionDetail:detailsList){
				exceptionDetail.setState(ScheduleExceptionDetailStateType.CANCELADO.getCode());
				statusChange.add(exceptionDetail);
			}
			exception.setState(ScheduleExceptionStateType.REVOCATE.getCode());
			exception.setScheduleExceptionDetails(statusChange);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return exception;
	}
	
	/**
	 * Here we changing the user exception status automatically
	 */
	public void automaticUserExceptionStatus(){
		try {
			UserExceptionFilter userExceptionFilter=new UserExceptionFilter();
			userExceptionFilter.setState(UserExceptionStateType.REGISTERED.getCode());
			userExceptionFilter.setUserExceptionStateSelected(UserExceptionStateType.REGISTERED.getCode());
			List<ExceptionUserPrivilege> exceptionsList = systemServiceFacade.searchUserExceptionsByFilterServiceFacade(userExceptionFilter);
			for(ExceptionUserPrivilege privilege:exceptionsList){
				
				if(privilege.getEndDate().before(CommonsUtilities.currentDateTime())){
					privilege.setState(UserExceptionStateType.ANNULLED.getCode());
					//update the exceptions
					systemServiceFacade.updaetUserExceptionsFacade(privilege);
					 List<Object[]> list = userAuditFacade.getLastAccessSystemName(privilege.getUserAccount().getIdUserPk(), privilege.getSystem().getIdSystemPk());
					 
					 if(Validations.validateIsNotNullAndNotEmpty(list) && list.size()>0){
						 	//PushContext pushContext = PushContextFactory.getDefault().getPushContext();
							//integration with notifications
							BrowserWindow browser = new BrowserWindow();
							browser.setNotificationType(NotificationType.KILLSESSION.getCode());
							browser.setSubject("Expire Session");
							browser.setMessage("Expire Session user "+privilege.getUserAccount().getIdUserPk());
							//pushContext.push(PropertiesConstants.PUSH_MSG+"/"+privilege.getUserAccount().getLoginUser(),browser);
					 }
				}
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	/**
	 * if create 'DENEGAR ACCESO' type exception,if user login and accessing the application,
	 * we are removing the users from the application.
	 */
	public void logOutUser(){
		try {
			ScheduleExceptionFilter scheduleException=new ScheduleExceptionFilter();
			scheduleException.setScheduleExceptionState(ScheduleExceptionStateType.REGISTERED.getCode());
			scheduleException.setScheduleExceptionType(ScheduleExceptionType.ACCESS_REFUSE.getCode());
			
			List<ScheduleException> scheduleExceptionList = systemServiceFacade.getScheduleExceptionServiceFacade(scheduleException);
			for(ScheduleException exception:scheduleExceptionList){
				 if(exception.getInitialDate().equals(CommonsUtilities.currentDate())){
					 Calendar calendar=Calendar.getInstance();
					 calendar.setTime(exception.getInitialHour());
					 if(calendar.getTime().before(CommonsUtilities.getDefaultDateWithCurrentTime())
							 || calendar.getTime().equals(CommonsUtilities.getDefaultDateWithCurrentTime())){
						 //force fully logout the user
						 ScheduleException temp = systemServiceFacade.getScheduleExceptionServiceFacade(exception);
						 for(ScheduleExceptionDetail exceptionDetail:temp.getScheduleExceptionDetails()){
							 UserAccount userAccount=exceptionDetail.getUserAccount();
							 
							 List<Object[]> list = userAuditFacade.getLastAccessSystemName(userAccount.getIdUserPk(), temp.getSystem().getIdSystemPk());
							 
							 if(Validations.validateIsNotNullAndNotEmpty(list) && list.size()>0){
								 	// pushContext = PushContextFactory.getDefault().getPushContext();
									//integration with notifications
									BrowserWindow browser = new BrowserWindow();
									browser.setNotificationType(NotificationType.KILLSESSION.getCode());
									browser.setSubject("Expire Session");
									browser.setMessage("Expire Session user "+userAccount.getFullName());
									//pushContext.push(PropertiesConstants.PUSH_MSG+"/"+userAccount.getLoginUser(),browser);
							 }
						 }
					 }
				 }
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	public void logoutBlockedUser(){
		UserAccountFilter userAccountFilter = new UserAccountFilter();
		userAccountFilter.setUserAccountState(UserAccountStateType.BLOCKED.getCode());
		try{
			List<UserAccount> lstUserAccountsBlocked = userServiceFacade.searchUsersByFilterServiceFacade(userAccountFilter);
			if(lstUserAccountsBlocked != null){
				for (UserAccount userAccount : lstUserAccountsBlocked) {
					//PushContext pushContext = PushContextFactory.getDefault().getPushContext();
					//integration with notifications
					BrowserWindow browser = new BrowserWindow();
					browser.setNotificationType(NotificationType.KILLSESSION.getCode());
					browser.setSubject("Expire Session");
					browser.setMessage("Expire Session user "+userAccount.getFullName());
					//pushContext.push(PropertiesConstants.PUSH_MSG+"/"+userAccount.getLoginUser(),browser);
				}
			}
		}catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	public void userExceptionBatch(){
		
	}
}
