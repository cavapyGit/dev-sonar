package com.pradera.security.systemmgmt.view;

import java.io.Serializable;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;
import com.pradera.security.model.SystemProfile;
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class SystemProfileDataModel
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.1
 */

public class SystemProfileDataModel extends ListDataModel<SystemProfile> implements Serializable,SelectableDataModel<SystemProfile>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -526987169578447677L;

	public SystemProfileDataModel(){
		
	}
	
	public SystemProfileDataModel(List<SystemProfile> data){
		super(data);
	}
	
	@Override
	public SystemProfile getRowData(String rowKey) {
		List<SystemProfile> lstSystemProfiles=(List<SystemProfile>)getWrappedData();
        for(SystemProfile systemProfile : lstSystemProfiles) {  
        	
            if(String.valueOf(systemProfile.getIdSystemProfilePk()).equals(rowKey))
                return systemProfile;  
        }
		return null;
	}

	@Override
	public Object getRowKey(SystemProfile systemProfile) {
		return systemProfile.getIdSystemProfilePk();
	}		

}
