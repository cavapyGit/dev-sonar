package com.pradera.security.systemmgmt.view.filters;

import java.io.Serializable;
import java.util.Date;
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class HolidayFilter
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.1
 */
public class HolidayFilter implements Serializable{

	private Integer holidayState;
	private Date holidayDate;
	
	/**
	 * @return the holidayState
	 */
	public Integer getHolidayState() {
		return holidayState;
	}

	/**
	 * @param holidayState the holidayState to set
	 */
	public void setHolidayState(Integer holidayState) {
		this.holidayState = holidayState;
	}

	/**
	 * @return the holidayDate
	 */
	public Date getHolidayDate() {
		return holidayDate;
	}

	/**
	 * @param holidayDate the holidayDate to set
	 */
	public void setHolidayDate(Date holidayDate) {
		this.holidayDate = holidayDate;
	}

	/**
	 * Constructor
	 */
	public HolidayFilter() {
		
	}

	
	
}
