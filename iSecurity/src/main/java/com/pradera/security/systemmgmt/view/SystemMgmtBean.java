package com.pradera.security.systemmgmt.view;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.persistence.Table;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.security.model.type.LanguageType;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.model.HistoricalState;
import com.pradera.security.model.Schedule;
import com.pradera.security.model.System;
import com.pradera.security.model.SystemProfile;
import com.pradera.security.model.type.DefinitionType;
import com.pradera.security.model.type.OptionPrivilegeStateType;
import com.pradera.security.model.type.OptionStateType;
import com.pradera.security.model.type.OtherMotiveType;
import com.pradera.security.model.type.PrivilegeType;
import com.pradera.security.model.type.ProfilePrivilegeStateType;
import com.pradera.security.model.type.ScheduleStateType;
import com.pradera.security.model.type.ScheduleType;
import com.pradera.security.model.type.SystemProfileStateType;
import com.pradera.security.model.type.SystemSituationType;
import com.pradera.security.model.type.SystemStateType;
import com.pradera.security.systemmgmt.view.filters.ScheduleFilter;
import com.pradera.security.systemmgmt.view.filters.SystemFilter;
import com.pradera.security.systemmgmt.view.filters.SystemProfileFilter;
import com.pradera.security.utils.view.GenericBaseBean;
import com.pradera.security.utils.view.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class SystemMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 29/01/2014
 */
@DepositaryWebBean
public class SystemMgmtBean extends GenericBaseBean implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	@Inject
	UserPrivilege userPrivilege;

	/** The system bean. */
	private System systemBean;
	
	/** The selected system bean. */
	private System selectedSystemBean;
	
	/** The system filter. */
	private SystemFilter systemFilter;
	
	/** The searchSystemFilter. */
	private SystemFilter searchSystemFilter;
	
	/** The system profile filter. */
	private SystemProfileFilter systemProfileFilter;
	
	/** The historical state. */
	private HistoricalState historicalState;
	
	/** The system data model. */
	private SystemDataModel systemDataModel;
	
	/** The list cbo system state. */
	private List<SelectItem> lstCboSystemState;
	
	/** The lst cbo schedule. */
	private List<Schedule> lstCboSchedule;
	
	/** The lst cbo Display System. */
	private List<SelectItem> lstCboDisplaySystem;
	
	/** The lst system profile. */
	private List<SystemProfile> lstSystemProfile;
	
    /** The tree root system options. */
    private TreeNode treeRootSystemOptions;
    
	/** The nodes expanded. */
	private boolean nodesExpanded;
    
    /** The show other motive. */
    private boolean showOtherMotive;
	
    /** The aux fupl system img. */
    private Boolean auxFuplSystemImg;
    
    /** The mnemonicexist. */
    private boolean mnemonicexist;
    
    /** The check name existence. */
    private boolean nameExist;
    
    /** The sys name. */
    private String sysName;
    
    /** The form state. */
    private Boolean formState;
    
    /** The system img temp. */
    private byte[] systemImgTemp;
    
    /** The system file temp. */
    private byte[] systemFileTemp;
    
    /** The file system image. */
    private StreamedContent fileSystemImage;
    
    /** The file register system. */
    private StreamedContent fileRegisterSystem;
    
    /** To display the block motives in detail screen. */
    private boolean showBlockMotive;
    
    /** To display the unblock motives in detail screen. */
    private boolean showUnblockMotive;
    
    /** The is confirm block or unblock. */
    private boolean isConfirmBlockOrUnblock;
    
    /** The is reject opcion. */
    private boolean isRejectOpcion;

	/** The system image name. */
	private String systemImageName;
	
	/** The system file name. */
	private String systemFileName;
	
	/** The upload file register types. */
	private String fUploadFileRegisterTypes="pdf";
	
	/** The upload file types display. */
	private String fUploadFileTypesDisplay="*.jpg, *.jpeg, *.png";	

	/**
	 * Instantiates a new system mgmt bean.
	 */
	public SystemMgmtBean(){ 
		systemFilter=new SystemFilter();
		selectedSystemBean=new System();
		systemBean=new System();
	}
	
	/**
	 * Init.
	 */
	@PostConstruct
	public void init(){		
		try {
			setFormState(false);
			loadListCboSystemState();//Combo de estados del Sistema
			loadListCboSchedule();//Combo de horarios para el Sistema
			loadListCboDisplaySystem();// combo que indica si se mostrara los sistemas o no
			
			lstBlockMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.SYSTEM_BLOCK_MOTIVES.getCode());//Motivos de bloqueo
			lstUnBlockMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.SYSTEM_DESBLOCK_MOTIVES.getCode());//Motivos de desbloqueo
			lstRejectMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.SYSTEM_REJECT_REGISTER_MOTIVES.getCode());//Motivos de Rechazo
			
			historicalState = new HistoricalState();
			historicalState.setMotive(Integer.valueOf(-1));
			//Privileges
			PrivilegeComponent privilegeComp=new PrivilegeComponent();
			privilegeComp.setBtnModifyView(true);
			privilegeComp.setBtnConfirmView(true);
			privilegeComp.setBtnRejectView(true);
			privilegeComp.setBtnBlockView(true);
			privilegeComp.setBtnUnblockView(true);
			userPrivilege.setPrivilegeComponent(privilegeComp);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	
	}
	
	/**
	 * Pre destroy.
	 */
	@PreDestroy
	public void preDestroy(){
		systemFilter = null;		
		selectedSystemBean = null;
		systemBean = null;		
		systemProfileFilter = null;
		
		lstCboSchedule = null;
		lstCboSystemState = null;
		lstSystemProfile = null;		
	}
	
	/**
	 * Register System Listener
	 * 
	 * This method is called when a user make click in register option in search page of systemMgmt.
	 *
	 * @param event the event
	 */
	public void registerSystemListener(ActionEvent event){
		try {
			this.setOperationType(ViewOperationsType.REGISTER.getCode());
			systemBean = new System();
			Schedule schedule=new Schedule();
			schedule.setIdSchedulePk(Integer.valueOf(-1) );
			systemBean.setSchedule(schedule);
			auxFuplSystemImg=null;
			//	remove the varible of session
			JSFUtilities.removeSessionMap("sessStreamedImage");
			//closing the dialog boxes
			nameExist=false;
			mnemonicexist=false;
			fileRegisterSystem=null;
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	/**
	 * System Modify Action
	 * 
	 * This method is called when a user make click in modify option in search page of systemMgmt
	 * Is responsible for validating that a system is selected and that its status is registered
	 * Return a navigation rule to System Management Page.
	 *
	 * @return the string
	 */
	public String systemModifyAction(){		
	    JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
		hideDialogs();
		mnemonicexist = false;
		nameExist=false;
		if(selectedSystemBean==null){
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT, null, 
					PropertiesConstants.ERROR_RECORD_NO_SELECTED, null);				
			JSFUtilities.showComponent("cnfMsgCustomValidation");
			return null;
		}else if(!(selectedSystemBean.getState().equals(SystemStateType.REGISTERED.getCode()) || 
				selectedSystemBean.getState().equals(SystemStateType.CONFIRMED.getCode())) ){
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT, null,
					PropertiesConstants.SYSTEM_ERROR_MODIFY, null);				
			JSFUtilities.showComponent("cnfMsgCustomValidation");
			return null;
		}
		
		this.setOperationType(ViewOperationsType.MODIFY.getCode());
		auxFuplSystemImg = Boolean.TRUE;
		formState = Boolean.FALSE;
		loadSelectedSystem();
		sysName = systemBean.getName();
		systemImgTemp = systemBean.getSystemImage();
		systemImageName = PropertiesUtilities.getMessage(PropertiesConstants.FUPL_ATTACHED_IMAGE);
		JSFUtilities.putSessionMap("sessStreamedImage",systemImgTemp);
		
		systemFileTemp = systemBean.getSystemProductionFile();
		fileRegisterSystem = systemBean.getStreamedFile();
		systemFileName = PropertiesUtilities.getMessage(PropertiesConstants.FUPL_ATTACHED_DOCUMENT);
			
		return "systemMgmtRule";
	}
	/**
	 * Load selected system.
	 */
	public void loadSelectedSystem(){
		systemBean = new System();
		systemBean.setBlockMotive(selectedSystemBean.getBlockMotive());
		systemBean.setBlockOtherMotive(selectedSystemBean.getBlockOtherMotive());
		systemBean.setDescription(selectedSystemBean.getDescription());
		systemBean.setIdSystemPk(selectedSystemBean.getIdSystemPk());
		systemBean.setLastModifyDate(selectedSystemBean.getLastModifyDate());
		systemBean.setLastModifyIp(selectedSystemBean.getLastModifyIp());
		systemBean.setLastModifyPriv(selectedSystemBean.getLastModifyPriv());
		systemBean.setLastModifyUser(selectedSystemBean.getLastModifyUser());
		systemBean.setMnemonic(selectedSystemBean.getMnemonic());
		systemBean.setName(selectedSystemBean.getName());
		systemBean.setOptionUrl(selectedSystemBean.getOptionUrl());
		systemBean.setRegistryDate(selectedSystemBean.getRegistryDate());
		systemBean.setRegistryUser(selectedSystemBean.getRegistryUser());
		systemBean.setSchedule(selectedSystemBean.getSchedule());
		systemBean.setScheduleExceptions(selectedSystemBean.getScheduleExceptions());
		systemBean.setSchemaDb(selectedSystemBean.getSchemaDb());
		systemBean.setState(selectedSystemBean.getState());
		systemBean.setSystemImage(selectedSystemBean.getSystemImage());
		systemBean.setSystemOptions(selectedSystemBean.getSystemOptions());
		systemBean.setSystemProfiles(selectedSystemBean.getSystemProfiles());
		systemBean.setSystemUrl(selectedSystemBean.getSystemUrl());
		systemBean.setUnblockMotive(selectedSystemBean.getUnblockMotive());
		systemBean.setUnblockOtherMotive(selectedSystemBean.getUnblockOtherMotive());
		systemBean.setUserSessions(selectedSystemBean.getUserSessions());
		systemBean.setSystemProductionFile(selectedSystemBean.getSystemProductionFile());
		systemBean.setVersion(selectedSystemBean.getVersion());
		systemBean.setIndDisplayIcon(selectedSystemBean.getIndDisplayIcon());
	}
	/**
	 * System details listener.
	 *
	 * This method of obtaining the selected system and find their assigned profiles
	 * @param event the event
	 */
	public void systemDetailsListener(ActionEvent event){
		try{
		systemBean=(System)event.getComponent().getAttributes().get("sisDet");
		systemProfileFilter=new SystemProfileFilter();
		systemProfileFilter.setIdSystemBaseQuery(systemBean.getIdSystemPk());	
		systemProfileFilter.setProfileStateBaseQuery(SystemProfileStateType.CONFIRMED.getCode());
		this.setOperationType(ViewOperationsType.DETAIL.getCode());	
				
		systemImgTemp = systemBean.getSystemImage();
		JSFUtilities.putSessionMap("sessStreamedImage",systemImgTemp);
		systemFileTemp = systemBean.getSystemProductionFile();
		
		lstSystemProfile=systemServiceFacade.searchProfilesByFiltersServiceFacade(systemProfileFilter);		
		loadProfilePrivilegesForDetail();	
		
		//To enable block unblock motives in the detail screen
		setShowBlockMotive(false);
		setShowUnblockMotive(false);
		if(SystemStateType.REGISTERED.getCode().equals(systemBean.getState())){
			if(Validations.validateIsNotNullAndPositive(systemBean.getUnblockMotive())){
				setShowUnblockMotive(true);
			}
		}else{
			setShowBlockMotive(true);
		}
		
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	/**
	 * Load profile privileges for detail.
	 */
	public void loadProfilePrivilegesForDetail() {
		try{
			systemProfileFilter.setIdSystem(systemBean.getIdSystemPk());
			systemProfileFilter.setIdLanguage(LanguageType.SPANISH.getCode());
			systemProfileFilter.setSystemOptionState(OptionStateType.CONFIRMED.getCode());
			systemProfileFilter.setOptionPrivilegeState(OptionPrivilegeStateType.REGISTERED.getCode());
			systemProfileFilter.setProfilePrivilegeState(ProfilePrivilegeStateType.REGISTERED.getCode());
			
			List<Object[]> lstProfilePrivileges = systemServiceFacade.listOptionPrivilegesByProfileServiceFacade(systemProfileFilter);
			treeRootSystemOptions = new DefaultTreeNode("Root", null);
			if(!lstProfilePrivileges.isEmpty()){
				List<Integer> lstIdOptionPrivilege = new ArrayList<Integer>();
				
				for (Object[] arrPrivilegeRow : lstProfilePrivileges) {
					lstIdOptionPrivilege.add(new Integer(arrPrivilegeRow[0].toString()));
				}
				
				systemProfileFilter.setLstIdOptionPrivilege(lstIdOptionPrivilege);
				
				List<Integer> lstIdSystemOptions  = systemServiceFacade.listIdSystemOptiosByOptionPrivilegeServiceFacade(systemProfileFilter);
				
				systemProfileFilter.setLstIdSystemOption(lstIdSystemOptions);
				
				
				List<Object[]> lstSystemOptions = systemServiceFacade.listSystemOptionsByIdsServiceFacade(systemProfileFilter);		
				
							
				
				List<Object[]> lstParentSystemOptions = getParentSystemOptions(lstSystemOptions);
				
				for (Object[] arrRow : lstParentSystemOptions) {
					loadPrivilegeOptionsRecursive(arrRow,lstSystemOptions, treeRootSystemOptions);
				}
				
				if(treeRootSystemOptions.getChildCount() > 0) {
					
					for (Object[] arrPrivilegeRow : lstProfilePrivileges) {
						loadPrivilegeNodeRecursive(arrPrivilegeRow,treeRootSystemOptions);
					}
					
				}
			}
		
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
							
	}
	/**
	 * Gets the parent system options.
	 *
	 * @param listaOpcioncesSistema the lista opcionces sistema
	 * @return the parent system options
	 */
	private List<Object[]> getParentSystemOptions(List<Object[]> listaOpcioncesSistema){		
		List<Object[]> lstParentSystemOptions = new ArrayList<Object[]>();
					
		for (Object[] arrRow : listaOpcioncesSistema) {
			if (arrRow[1] == null ) {				
				lstParentSystemOptions.add(arrRow);
			}
		}
		
		return lstParentSystemOptions;
	}
	
	/**
	 * Load privilege options recursive.
	 *
	 * @param arrRow the arr row
	 * @param lstSystemOptions the lst system options
	 * @param parentNode the parent node
	 */
	private void loadPrivilegeOptionsRecursive(
		Object[] arrRow, List<Object[]> lstSystemOptions, TreeNode parentNode){
		
		Integer idSystemOption = null;
		Integer idParentSystemOption = null;
		
		OptionPrivilegeNodeBean optionPrivilegeNodeBean = new OptionPrivilegeNodeBean();		
				
		if(arrRow[0]!=null){
			idSystemOption = new Integer(arrRow[0].toString());
			optionPrivilegeNodeBean.setIdSystemOption(idSystemOption);
		}
		
		if(arrRow[1]!=null){
			idParentSystemOption = new Integer(arrRow[1].toString());
			optionPrivilegeNodeBean.setIdParentSystemOption(idParentSystemOption);
		}
		

		if(arrRow[2]!=null){
			optionPrivilegeNodeBean.setName(arrRow[2].toString());
		}
		
		TreeNode node = new DefaultTreeNode(optionPrivilegeNodeBean, parentNode);

			Integer idParentSystemOptionFromChild = null;
			
			for (Object[] objArrFilaHija : lstSystemOptions) {
				if(objArrFilaHija[1]!=null){
					idParentSystemOptionFromChild = new Integer(objArrFilaHija[1].toString());				
					
					if(idSystemOption.intValue()==idParentSystemOptionFromChild.intValue()){						
					   loadPrivilegeOptionsRecursive(objArrFilaHija, lstSystemOptions, node);					   					   
					}
					
				}
			}
				
	}
	
	/**
	 * Load list cbo system state.
	 * 
	 * This method fill the list of System states
	 */
	public void loadListCboSystemState(){
		lstCboSystemState=new ArrayList<SelectItem>();
		for(SystemStateType state : SystemStateType.values()){
			lstCboSystemState.add(new SelectItem(state.getCode(),state.getValue()));
		}
	}
	
	/**
	 * Load list cbo Display System.
	 *
	 * This method fill the list of options select System 
	 * @throws ServiceException the service exception
	 */
	public void loadListCboDisplaySystem() throws ServiceException{
		
		lstCboDisplaySystem = new ArrayList<>();
		// No mostrar Icono en Sistema
		lstCboDisplaySystem.add(new SelectItem(0, "No mostrar sistema en CSDCORE"));
		// Mostrar Icono en Sistema
		lstCboDisplaySystem.add(new SelectItem(1, "Mostrar sistema en CSDCORE"));
	
	}
	
	/**
	 * Load list cbo schedule.
	 *
	 * This method fill the list of system schedules
	 * @throws ServiceException the service exception
	 */
	public void loadListCboSchedule() throws ServiceException{
		ScheduleFilter scheduleFilter=new ScheduleFilter();
		scheduleFilter.setScheduleState(ScheduleStateType.CONFIRMED.getCode());//Horarios en estado confirmado
		scheduleFilter.setScheduleType(ScheduleType.SYSTEMS.getCode());//Horarios para los Sistemas

		lstCboSchedule = systemServiceFacade.getSchedulesServiceFacade(scheduleFilter);
	
	}
	
	
	/**
	 * Search systems listener.
	 *
	 * @param event the event
	 */
	@LoggerAuditWeb
	public void searchSystemsListener(ActionEvent event){
		try {
			JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
			hideDialogs();
			loadSystems();
//			userInfo.getUserAcctions();
//			userInfo.getUserAccountSession();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Load systems.
	 *
	 * This method fill the systemDataModel with the systems list according to the search criteria
	 * @throws ServiceException the service exception
	 */
	public void loadSystems() {
		try{
			systemDataModel = new SystemDataModel(systemServiceFacade.getSystemsByFilterServiceFacade(systemFilter));
			searchSystemFilter = new SystemFilter();
			searchSystemFilter.setIdSystemPk(systemFilter.getIdSystemPk());
			searchSystemFilter.setSystemCode(systemFilter.getSystemCode());
			searchSystemFilter.setSystemMnemonic(systemFilter.getSystemMnemonic());
			searchSystemFilter.setSystemName(systemFilter.getSystemName());
			searchSystemFilter.setSystemState(systemFilter.getSystemState());
			//BeanUtils.copyProperties(searchSystemFilter, systemFilter);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Before save listener.
	 * This method is called to save or update a system
	 */
	public void beforeSaveListener(){
		try {
			hideDialogs();
			String regEx = "^(http|https)?://[a-zA-Z0-9-.]+.[a-zA-Z0-9]{2,3}([0-9]+)?/{0,1}(@{0,1}[a-zA-Z0-9]+.[a-zA-Z]{2,3}){0,1}/?([a-zA-Z0-9-._?,'/&amp;#/-]?)*";
			
			if(validationSystemManagement()){
				Pattern patt = Pattern.compile(regEx);
	            Matcher matcher = patt.matcher(systemBean.getSystemUrl());
	            if(matcher.matches())
	            	JSFUtilities.showComponent("frmAdmSystem:cnfAdmSystem");
	            else{
	            	JSFUtilities.showEndTransactionSamePageDialog();
	            	JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT,null,PropertiesConstants.EEROR_URL_PATTERN,null);
	            }
			}else{
				JSFUtilities.showEndTransactionSamePageDialog();
            	JSFUtilities.showMessageOnDialog(
            			PropertiesConstants.DIALOG_HEADER_ALERT,null,            			
            			"message.data.required",null);
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Save system listener.
	 *
	 * This method is called after confirming, if save or update the system
	 * 
	 * @param event the event
	 */
	@LoggerAuditWeb
	public void saveSystemListener(ActionEvent event){
		try {
			JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
			hideDialogs();
			if(!validationSystemManagement()){ 
				return;
			}
			JSFUtilities.showComponent("cnfEndTransaction");
			Schedule schedule = systemBean.getSchedule();
			//if the operation is register then save a new system
			Object[] argObj = new Object[2];
			String message="";
			if(isRegister()){
				//set data for audit columns
				systemBean.setRegistryUser(userInfo.getUserAccountSession().getUserName());
				systemBean.setRegistryDate(new Date());
				systemBean.setState(SystemStateType.REGISTERED.getCode());
				systemServiceFacade.registerSystemServiceFacade(systemBean);
				argObj[0]=systemBean.getName();
				argObj[1]=systemBean.getIdSystemPk();
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS,null,
						PropertiesConstants.SYSTEM_REGISTER_SUCCESS,
						argObj);
				systemBean.setSchedule(schedule);
				cleanMainSystemMgmt();
			}else if(isModify()) {
				//retrieve system image from session
				systemBean.setSystemImage(systemImgTemp);
				System tempSys = systemServiceFacade.getLastestSysemDetails(systemBean.getIdSystemPk());
				if(tempSys.getLastModifyDate().after(systemBean.getLastModifyDate())){
					argObj[0]=systemBean.getName();
					showMessageOnDialog(null,null,PropertiesConstants.SYSTEM_ERROR_MODIFIED_RECENT,argObj);
					cleanMainSystemMgmt();
					return;
				}else{
					systemServiceFacade.modifySystemServiceFacade(systemBean);
					argObj[0]=systemBean.getName();
					showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS,null,
							PropertiesConstants.SYSTEM_MODIFY_SUCCESS,
							argObj);
				}
				systemBean.setSchedule(schedule);
				cleanMainSystemMgmt();
			}
		} catch(ServiceException sex){
			if(sex.getMessage()!=null){
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION,sex.getErrorService().getMessage());
				JSFUtilities.showSimpleValidationDialog();
				cleanMainSystemMgmt();
			}
		}catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Validation System Management
	 * This method validates the system required values.
	 *
	 * @return 0, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean validationSystemManagement() throws ServiceException{
		JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
		nameExist=false;
		mnemonicexist=false;
		if(systemBean.getSystemImage()==null){			
			JSFUtilities.addContextMessage("frmAdmSystem:fuplFormImagenSystem", FacesMessage.SEVERITY_ERROR, 
					PropertiesUtilities.getMessage(PropertiesConstants.SYSTEM_ERROR_IMAGE_REQUIRED),
					PropertiesUtilities.getMessage(PropertiesConstants.SYSTEM_ERROR_IMAGE_REQUIRED));
			return false;
		}else if(systemBean.getSystemProductionFile()==null){
			JSFUtilities.addContextMessage("frmAdmSystem:fuplFormFileSystem", FacesMessage.SEVERITY_ERROR,
					PropertiesUtilities.getMessage(PropertiesConstants.SYSTEM_ERROR_PRODUCTION_FILE_REQUIRED),
					PropertiesUtilities.getMessage(PropertiesConstants.SYSTEM_ERROR_PRODUCTION_FILE_REQUIRED));
			return false;
		}
		return true;
	}
	
	/**
	 * Clean main system mgmt listener.
	 *
	 * @param event the event
	 */
	public void cleanMainSystemMgmtListener(ActionEvent event){
		try {
			cleanMainSystemMgmt();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clean main system mgmt.
	 * This method clean the datatable and entered filters
	 * @throws Exception the exception
	 */
	public void cleanMainSystemMgmt(){
		systemFilter=new SystemFilter();
		systemDataModel=null;
		selectedSystemBean=null;
	}
		
	/**
	 * Change selection motive listener.
	 */
	public void changeSelectionMotiveListener(){
		JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
        if(isBlock()){
        	if(OtherMotiveType.SYS_BLOCK_OTHER_MOTIVE.getCode().equals(historicalState.getMotive())){
        		showOtherMotive = true;
        		JSFUtilities.setValidViewComponent("frmMotive:cboMotive");
        		return;
        	}
        }else if(isUnBlock()){
        	if(OtherMotiveType.SYS_UNBLOCK_OTHER_MOTIVE.getCode().equals(historicalState.getMotive())){
        		showOtherMotive = true;
        		JSFUtilities.setValidViewComponent("frmMotive:cboMotive");
        		return;
        	}
        }else if(isReject()){
        	if(OtherMotiveType.SYS_REJECT_REGISTER_OTHER_MOTIVE.getCode().equals(historicalState.getMotive())){
        		showOtherMotive = true;
        		JSFUtilities.setValidViewComponent("frmMotive:cboMotive");
        		return;
        	}
        }
        showOtherMotive = false;
	}
	
	
	/**
	 * Action reject motive listener.
	 *
	 * @param event the event
	 */
	public void actionRejectMotiveListener(ActionEvent event){
		try {
			
			isRejectOpcion = true;
			JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
			JSFUtilities.hideComponent("cnfMsgCustomValidation");
			Object[] argObj = {selectedSystemBean.getMnemonic()};
			String messageConfirm = "";
			if(isBlock()){
				if(selectedSystemBean.getState().equals(SystemStateType.CONFIRMED.getCode()) && 
					selectedSystemBean.getSituation().equals(SystemSituationType.PENDING_LOCK.getCode())){
					
					messageConfirm = PropertiesConstants.SYSTEM_REJECT_BLOCK_MESSAGE;
					JSFUtilities.executeJavascriptFunction("PF('cnfwBlock').show()");
				}
				
			}else if(isUnBlock()){
				if(selectedSystemBean.getState().equals(SystemStateType.BLOCKED.getCode()) && 
					selectedSystemBean.getSituation().equals(SystemSituationType.PENDING_UNLOCK.getCode())){
						
						messageConfirm = PropertiesConstants.SYSTEM_REJECT_UNBLOCK_MESSAGE;
						JSFUtilities.executeJavascriptFunction("PF('cnfwUnblock').show()");
					}
			}
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_CONFIRM,null,
					messageConfirm,argObj);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	/**
	 * Action motive listener.
	 *
	 * @param event the event
	 */
	public void actionMotiveListener(ActionEvent event){
		try {
			JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);

			if(showOtherMotive==true){
				if (Validations.validateIsNullOrEmpty(this.getHistoricalState().getMotiveDescription())||
						this.getHistoricalState().getMotiveDescription().trim().length() == 0) {
					this.getHistoricalState().setMotiveDescription("");
					JSFUtilities.showComponent("cnfMsgCustomValidation");
					JSFUtilities.hideComponent("frmBlockSystem:cnfBlock");
					JSFUtilities.hideComponent("frmUnblockSystem:cnfUnblock");
					showMessageOnDialog(null, null,PropertiesConstants.ERROR_MESSAGE_MOTIVE_REQUIRED,null);
					String errorMessage=PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_MOTIVE_REQUIRED);
					JSFUtilities.addContextMessage("frmMotive:txtMotive",FacesMessage.SEVERITY_ERROR, errorMessage,errorMessage);
					return;
				}
			}		
			
			JSFUtilities.hideComponent("cnfMsgCustomValidation");
			Object[] argObj = {selectedSystemBean.getMnemonic()};
			String messageConfirm = "";
			if(isBlock()){
				if(selectedSystemBean.getState().equals(SystemStateType.CONFIRMED.getCode()) && 
					selectedSystemBean.getSituation().equals(SystemSituationType.ACTIVE.getCode())){
					
					messageConfirm = PropertiesConstants.SYSTEM_REQUEST_BLOCK_MESSAGE;
					
				}else if(selectedSystemBean.getState().equals(SystemStateType.CONFIRMED.getCode()) && 
						selectedSystemBean.getSituation().equals(SystemSituationType.PENDING_LOCK.getCode())){
					
					messageConfirm = PropertiesConstants.SYSTEM_BLOCK_MESSAGE;
				}				
				JSFUtilities.executeJavascriptFunction("PF('cnfwBlock').show()");
				
			}else if(isUnBlock()){
				if(selectedSystemBean.getState().equals(SystemStateType.BLOCKED.getCode()) && 
						selectedSystemBean.getSituation().equals(SystemSituationType.ACTIVE.getCode())){
					
					messageConfirm = PropertiesConstants.SYSTEM_REQUEST_UNBLOCK_MESSAGE;
					
				}else if(selectedSystemBean.getState().equals(SystemStateType.BLOCKED.getCode()) && 
						selectedSystemBean.getSituation().equals(SystemSituationType.PENDING_UNLOCK.getCode())){
					
					messageConfirm = PropertiesConstants.SYSTEM_UNBLOCK_MESSAGE;
				}
				JSFUtilities.executeJavascriptFunction("PF('cnfwUnblock').show()");
			}else if(isReject()){
				messageConfirm = PropertiesConstants.SYSTEM_REJECT_REGISTER_MESSAGE;			
				JSFUtilities.executeJavascriptFunction("PF('wConfirmDialogReject').show()");
			}
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_CONFIRM,null,
					messageConfirm,argObj);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
			
	/**
	 * Clear the Historical Value,User Clicks on the back button.
	 *
	 * @param event the event
	 */
	public void cleanMotiveListener(ActionEvent event){
		try{
			JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
			historicalState.setMotive(Integer.valueOf(-1));
			showOtherMotive=false;
			isConfirmBlockOrUnblock=false;
			isRejectOpcion=false;
			JSFUtilities.setValidViewComponent("frmMotive:cboMotive");
			JSFUtilities.showComponent("frmMotive:dlgMotive");
			JSFUtilities.executeJavascriptFunction("setInitialFormState('#frmMotive');");
			hideDialogs(); 
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	
	/**
	 * Before block system listener.
	 * 
	 * This method validate that system is selected and that this is not blocked
	 *
	 * @param event the event
	 */
	public void beforeBlockSystemListener(ActionEvent event){
		try {
			JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
			hideDialogs();
			showOtherMotive=false;
			isConfirmBlockOrUnblock=false;	
			isRejectOpcion=false;
			
			if(selectedSystemBean==null){
				JSFUtilities.showComponent("cnfMsgCustomValidation");
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT,null,PropertiesConstants.ERROR_RECORD_NO_SELECTED, null);
				return;				
			}
			if(!selectedSystemBean.getState().equals(SystemStateType.CONFIRMED.getCode())){
				JSFUtilities.showComponent("cnfMsgCustomValidation");
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT,null,
						PropertiesConstants.SYSTEM_BLOCK_ERROR, null);
				return;
			}
					
			this.setOperationType(ViewOperationsType.BLOCK.getCode());		
			
			if(	selectedSystemBean.getState().equals(SystemStateType.CONFIRMED.getCode()) && 
				selectedSystemBean.getSituation().equals(SystemSituationType.PENDING_LOCK.getCode())){
				isConfirmBlockOrUnblock = true;
			}
			cleanAndShowDlgMotive();  
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Before unblock system listener.
	 * 
	 * This method validate that system is selected and that this is not registered
	 *
	 * @param event the event
	 */
	public void beforeUnblockSystemListener(ActionEvent event){
		try {
			JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
			isConfirmBlockOrUnblock = false;
			showOtherMotive=false;
			isRejectOpcion=false;
			hideDialogs();
			
			if(selectedSystemBean==null){
				JSFUtilities.showComponent("cnfMsgCustomValidation");
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT, null,
						PropertiesConstants.ERROR_RECORD_NO_SELECTED,  null);
				return;
			}
			if(!selectedSystemBean.getState().equals(SystemStateType.BLOCKED.getCode())){				
				JSFUtilities.showComponent("cnfMsgCustomValidation");
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT, null,
						PropertiesConstants.SYSTEM_UNBLOCK_ERROR,  null);
				return;
			}
			
			this.setOperationType(ViewOperationsType.UNBLOCK.getCode());
			if(	selectedSystemBean.getState().equals(SystemStateType.BLOCKED.getCode()) && 
				selectedSystemBean.getSituation().equals(SystemSituationType.PENDING_UNLOCK.getCode())){
				isConfirmBlockOrUnblock = true;
			}
			cleanAndShowDlgMotive();	
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Before confirm system listener.
	 *
	 * @param event the event
	 */
	public void beforeConfirmSystemListener(ActionEvent event){
		try {
			JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);		
			showOtherMotive=false;
			hideDialogs();
			
			if(selectedSystemBean==null){
				JSFUtilities.showComponent("cnfMsgCustomValidation");
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,
						PropertiesConstants.ERROR_RECORD_NO_SELECTED,  null);
				return;
			}
			if(!selectedSystemBean.getState().equals(SystemStateType.REGISTERED.getCode())){		
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,
						PropertiesConstants.SYSTEM_CONFIRM_REGISTER_ERROR,  null);
				JSFUtilities.showComponent("cnfMsgCustomValidation");
				return;
			}

			this.setOperationType(ViewOperationsType.CONFIRM.getCode());			
			Object[] argObj = {selectedSystemBean.getName()};
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_CONFIRM,null,
					PropertiesConstants.SYSTEM_CONFIRM_REGISTER_MESSAGE, argObj);					
			JSFUtilities.executeJavascriptFunction("PF('wConfirmDialogConfirm').show()");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Before reject system listener.
	 *
	 * @param event the event
	 */
	public void beforeRejectSystemListener(ActionEvent event){
		try {
			JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
			hideDialogs();
			isConfirmBlockOrUnblock=false;
			
			if(selectedSystemBean==null){
				JSFUtilities.showComponent("cnfMsgCustomValidation");
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,
						PropertiesConstants.ERROR_RECORD_NO_SELECTED,  null);
				return;
			}
			if(!selectedSystemBean.getState().equals(SystemStateType.REGISTERED.getCode())){				
				JSFUtilities.showComponent("cnfMsgCustomValidation");
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,
						PropertiesConstants.SYSTEM_REJECT_REGISTER_ERROR,  null);
				return;
			}
			
			this.setOperationType(ViewOperationsType.REJECT.getCode());
			cleanAndShowDlgMotive(); 
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clean and show dlgmotive.
	 * 
	 * This method clean the data in the motive dialog
	 *
	 * @throws Exception the exception
	 */
	public void cleanAndShowDlgMotive() throws Exception{
		showOtherMotive=false;
		historicalState=new HistoricalState();
		historicalState.setMotive(Integer.valueOf(-1));	
		
		JSFUtilities.setValidViewComponent("frmMotive:txtMotive");
		JSFUtilities.setValidViewComponent("frmMotive:cboMotive");		
		
		if(this.isBlock()){
			this.historicalState.setMotive(isConfirmBlockOrUnblock ? selectedSystemBean.getBlockMotive() : Integer.valueOf(-1));
			if(isConfirmBlockOrUnblock && selectedSystemBean.getBlockMotive().equals(OtherMotiveType.SYS_BLOCK_OTHER_MOTIVE.getCode())){							
				this.historicalState.setMotiveDescription(selectedSystemBean.getBlockOtherMotive());
				showOtherMotive=true;
			}
			showMessageOnDialog2(PropertiesConstants.DIALOG_HEADER_BLOCKMOTIVE, null, null, null);
		}else if(this.isUnBlock()){
			this.historicalState.setMotive(isConfirmBlockOrUnblock ? selectedSystemBean.getUnblockMotive() : Integer.valueOf(-1));
			if (isConfirmBlockOrUnblock && selectedSystemBean.getUnblockMotive().equals(OtherMotiveType.SYS_UNBLOCK_OTHER_MOTIVE.getCode())){				
				this.historicalState.setMotiveDescription(selectedSystemBean.getUnblockOtherMotive());
				showOtherMotive=true;
			}	
			showMessageOnDialog2(PropertiesConstants.DIALOG_HEADER_UNBLOCKMOTIVE, null, null, null);
		}else if(this.isReject()){
			showMessageOnDialog2(PropertiesConstants.DIALOG_HEADER_MOTIVE_REJECT, null, null, null);
		}
		JSFUtilities.showComponent("frmMotive:dlgMotive");
		JSFUtilities.executeJavascriptFunction("setInitialFormState('#frmMotive');");
	}
	
	
	/**
	 * Block system listener.
	 * 
	 * This method block the selected system
	 */
	@LoggerAuditWeb
	public void blockSystemListener(){
		try {							
			JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);			
			hideDialogs();
			System tempSys = systemServiceFacade.getLastestSysemDetails(selectedSystemBean.getIdSystemPk());			
			if(tempSys.getLastModifyDate().after(selectedSystemBean.getLastModifyDate())){
				Object[] argObj= {selectedSystemBean.getName()};
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT,null,
						PropertiesConstants.SYSTEM_ERROR_MODIFIED_RECENT,argObj);
				JSFUtilities.showEndTransactionSamePageDialog();
				loadSystems();
				return;
			}
			String messageModal="";
			String messageNotification="";
			if(isRejectOpcion && (selectedSystemBean.getState().equals(SystemStateType.CONFIRMED.getCode()) && 
					selectedSystemBean.getSituation().equals(SystemSituationType.PENDING_LOCK.getCode()))){
				
				selectedSystemBean.setState(SystemStateType.CONFIRMED.getCode());
				selectedSystemBean.setSituation(SystemSituationType.ACTIVE.getCode());
				historicalState.setCurrentState(selectedSystemBean.getState());
				messageModal = PropertiesConstants.SYSTEM_REJECT_BLOCK_SUCCESS;
				messageNotification = PropertiesConstants.SYSTEM_REJECT_BLOCK_NOTIFICATION;				
			}else if(selectedSystemBean.getState().equals(SystemStateType.CONFIRMED.getCode()) && 
					selectedSystemBean.getSituation().equals(SystemSituationType.ACTIVE.getCode())){
				
				selectedSystemBean.setSituation(SystemSituationType.PENDING_LOCK.getCode());
				historicalState.setCurrentState(selectedSystemBean.getSituation());
				messageModal = PropertiesConstants.SYSTEM_REQUEST_BLOCK_SUCCESS;
				messageNotification = PropertiesConstants.SYSTEM_REQUEST_BLOCK_NOTIFICATION;
			}else if(selectedSystemBean.getState().equals(SystemStateType.CONFIRMED.getCode()) && 
					selectedSystemBean.getSituation().equals(SystemSituationType.PENDING_LOCK.getCode())){
				
				selectedSystemBean.setState(SystemStateType.BLOCKED.getCode());
				selectedSystemBean.setSituation(SystemSituationType.ACTIVE.getCode());
				historicalState.setCurrentState(selectedSystemBean.getState());
				messageModal = PropertiesConstants.SYSTEM_BLOCK_SUCCESS;
				messageNotification = PropertiesConstants.SYSTEM_BLOCK_NOTIFICATION;
			}
			selectedSystemBean.setBlockMotive(isRejectOpcion ? Integer.valueOf(-1) : historicalState.getMotive());
			selectedSystemBean.setBlockOtherMotive(isRejectOpcion ? "" : historicalState.getMotiveDescription());
			ThreadLocalContextHolder.put(
					PropertiesConstants.HISTORICAL_STATE_LOGGER, 
					createHistorialState(
							selectedSystemBean.getClass().getAnnotation(Table.class).name(), 
							SystemStateType.CONFIRMED.getCode(),
							Long.parseLong(selectedSystemBean.getIdSystemPk().toString()),
							historicalState));
			systemServiceFacade.updateSystemStateServiceFacade(selectedSystemBean);
			Object[] arrBodyData = {selectedSystemBean.getName()};
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_SUCCESS, null,
					messageModal, 
					arrBodyData);
			JSFUtilities.showEndTransactionSamePageDialog();
			JSFUtilities.executeJavascriptFunction("PF('dlgwMotive').hide();");
			if(systemDataModel.getSize()==1){
				systemFilter.setSystemState(selectedSystemBean.getState());
			}
			loadSystems();
		} catch(ServiceException sex){
			if(sex.getMessage()!=null){
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION,sex.getErrorService().getMessage());
				JSFUtilities.showSimpleValidationDialog();
				loadSystems();
			}
		}catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Confirm system listener.
	 */
	@LoggerAuditWeb
	public void confirmSystemListener(){
		try {							
			JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);			
			System tempSys = systemServiceFacade.getLastestSysemDetails(selectedSystemBean.getIdSystemPk());			
			if(tempSys.getLastModifyDate().after(selectedSystemBean.getLastModifyDate())){
				
				Object[] argObj= {selectedSystemBean.getName()};				
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT,null,
						PropertiesConstants.SYSTEM_ERROR_MODIFIED_RECENT,argObj);
				JSFUtilities.showEndTransactionSamePageDialog();
				loadSystems();
				return;
			}
			hideDialogs();
			selectedSystemBean.setState(SystemStateType.CONFIRMED.getCode());
			selectedSystemBean.setSituation(SystemSituationType.ACTIVE.getCode());
			historicalState.setCurrentState(selectedSystemBean.getState());				
			ThreadLocalContextHolder.put(
					PropertiesConstants.HISTORICAL_STATE_LOGGER, 
					createHistorialState(
							selectedSystemBean.getClass().getAnnotation(Table.class).name(), 
							SystemStateType.REGISTERED.getCode(),
							Long.parseLong(selectedSystemBean.getIdSystemPk().toString()),
							historicalState));
			systemServiceFacade.confirmSystemServiceFacade(selectedSystemBean);
			Object[] arrBodyData = {selectedSystemBean.getName()};
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_SUCCESS, null,
					PropertiesConstants.SYSTEM_CONFIRM_REGISTER_SUCCESS, 
					arrBodyData);
			JSFUtilities.showEndTransactionSamePageDialog();
			if(systemDataModel.getSize()==1){
				systemFilter.setSystemState(selectedSystemBean.getState());
			}
			loadSystems();
		} catch(ServiceException sex){
			if(sex.getMessage()!=null){
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION,sex.getErrorService().getMessage());
				JSFUtilities.showSimpleValidationDialog();
				loadSystems();
			}
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Reject system listener.
	 */
	@LoggerAuditWeb
	public void rejectSystemListener(){
		try {							
			JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
			
			System tempSys = systemServiceFacade.getLastestSysemDetails(selectedSystemBean.getIdSystemPk());			
			if(tempSys.getLastModifyDate().after(selectedSystemBean.getLastModifyDate())){
				Object[] argObj= {selectedSystemBean.getName()};
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT,null,
						PropertiesConstants.SYSTEM_ERROR_MODIFIED_RECENT,argObj);
				JSFUtilities.showEndTransactionSamePageDialog();
				loadSystems();
				return;
			}
			hideDialogs();
			selectedSystemBean.setState(SystemStateType.REJECTED.getCode());
			selectedSystemBean.setSituation(SystemSituationType.ACTIVE.getCode());
			selectedSystemBean.setRejectMotive(historicalState.getMotive());
			selectedSystemBean.setRejectOtherMotive(historicalState.getMotiveDescription());
			historicalState.setCurrentState(selectedSystemBean.getState());
			ThreadLocalContextHolder.put(
					PropertiesConstants.HISTORICAL_STATE_LOGGER, 
					createHistorialState(
							selectedSystemBean.getClass().getAnnotation(Table.class).name(), 
							SystemStateType.REGISTERED.getCode(),
							Long.parseLong(selectedSystemBean.getIdSystemPk().toString()), 
							historicalState));
			systemServiceFacade.updateSystemStateServiceFacade(selectedSystemBean);
			Object[] arrBodyData = {selectedSystemBean.getName()};			
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_SUCCESS, null,
					PropertiesConstants.SYSTEM_REJECT_REGISTER_SUCCESS, 
					arrBodyData);
			JSFUtilities.showEndTransactionSamePageDialog();
			JSFUtilities.executeJavascriptFunction("PF('dlgwMotive').hide();");
			if(systemDataModel.getSize()==1){
				systemFilter.setSystemState(selectedSystemBean.getState());
			}
			loadSystems();
			
		} catch(ServiceException sex){
			if(sex.getMessage()!=null){
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION,sex.getErrorService().getMessage());
				JSFUtilities.showSimpleValidationDialog();
				loadSystems();
			}
		}catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		
	}
	
	/**
	 * Unblock system listener.
	 *
	 * This method unblock the selected system
	 * @param event the event
	 */
	@LoggerAuditWeb
	public void unblockSystemListener(ActionEvent event){
		try {			
			JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
			hideDialogs();
			System tempSys = systemServiceFacade.getLastestSysemDetails(selectedSystemBean.getIdSystemPk());			
			if(tempSys.getLastModifyDate().after(selectedSystemBean.getLastModifyDate())){
				Object[] argObj = {selectedSystemBean.getName()};
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT,null,
						PropertiesConstants.SYSTEM_ERROR_MODIFIED_RECENT,argObj);
				JSFUtilities.showEndTransactionSamePageDialog();
				loadSystems();
				return;
			}
			String messageModal = "";
			String messageNotification = "";
			if(isRejectOpcion && (selectedSystemBean.getState().equals(SystemStateType.BLOCKED.getCode()) &&
					selectedSystemBean.getSituation().equals(SystemSituationType.PENDING_UNLOCK.getCode()))){
				selectedSystemBean.setState(SystemStateType.BLOCKED.getCode());
				selectedSystemBean.setSituation(SystemSituationType.ACTIVE.getCode());
				historicalState.setCurrentState(selectedSystemBean.getState());
				messageModal = PropertiesConstants.SYSTEM_REJECT_UNBLOCK_SUCCESS;
				messageNotification = PropertiesConstants.SYSTEM_REJECT_UNBLOCK_NOTIFICATION;				
			}else if(selectedSystemBean.getState().equals(SystemStateType.BLOCKED.getCode()) &&
				selectedSystemBean.getSituation().equals(SystemSituationType.ACTIVE.getCode())){
				selectedSystemBean.setSituation(SystemSituationType.PENDING_UNLOCK.getCode());
				historicalState.setCurrentState(selectedSystemBean.getSituation());
				messageModal = PropertiesConstants.SYSTEM_REQUEST_UNBLOCK_SUCCESS;
				messageNotification= PropertiesConstants.SYSTEM_REQUEST_UNBLOCK_NOTIFICATION;
			}else if(selectedSystemBean.getState().equals(SystemStateType.BLOCKED.getCode()) &&
					selectedSystemBean.getSituation().equals(SystemSituationType.PENDING_UNLOCK.getCode())){
				
				selectedSystemBean.setState(SystemStateType.CONFIRMED.getCode());
				selectedSystemBean.setSituation(SystemSituationType.ACTIVE.getCode());
				historicalState.setCurrentState(selectedSystemBean.getState());
				messageModal = PropertiesConstants.SYSTEM_UNBLOCK_SUCCESS;
				messageNotification=PropertiesConstants.SYSTEM_UNBLOCK_NOTIFICATION;
			}
			selectedSystemBean.setUnblockMotive(isRejectOpcion ? Integer.valueOf(-1) : historicalState.getMotive());
			selectedSystemBean.setUnblockOtherMotive(isRejectOpcion ? "" : historicalState.getMotiveDescription());
			ThreadLocalContextHolder.put(
					PropertiesConstants.HISTORICAL_STATE_LOGGER, 
					createHistorialState(
							selectedSystemBean.getClass().getAnnotation(Table.class).name(), 
							SystemStateType.BLOCKED.getCode(),
							Long.parseLong(selectedSystemBean.getIdSystemPk().toString()),
							historicalState));
			systemServiceFacade.updateSystemStateServiceFacade(selectedSystemBean);
			Object[] arrBodyData = {selectedSystemBean.getMnemonic()};
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_SUCCESS, null,
					messageModal, 
					arrBodyData);
			JSFUtilities.showEndTransactionSamePageDialog();
			JSFUtilities.executeJavascriptFunction("PF('dlgwMotive').hide();");
			if(systemDataModel.getSize()==1){
				systemFilter.setSystemState(selectedSystemBean.getState());
			}
			loadSystems();
		} catch(ServiceException sex){
			if(sex.getMessage()!=null){
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION,sex.getErrorService().getMessage());
				JSFUtilities.showSimpleValidationDialog();
				loadSystems();
			}
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Load privileges by system.
	 */
	public void loadPrivilegesBySystem() {

		try{
		systemProfileFilter=new SystemProfileFilter();
		systemProfileFilter.setIdSystemProfileBaseQuery(systemBean.getIdSystemPk());
		systemProfileFilter.setIdLanguage(LanguageType.SPANISH.getCode()); 
		systemProfileFilter.setSystemOptionState(OptionStateType.CONFIRMED.getCode());
		systemProfileFilter.setOptionPrivilegeState(OptionPrivilegeStateType.REGISTERED.getCode());
		
		List<Object[]> listSystemOptions = systemServiceFacade.listOptionSystemsByLanguageServiceFacade(systemProfileFilter);

			
		treeRootSystemOptions = new DefaultTreeNode("Root", null);			
		
		List<Object[]> listParentOptions = getParentOptions(listSystemOptions);
	
		
		for (Object[] arrRow : listParentOptions) {
			loadOptionPrivilegesRecursive(arrRow,listSystemOptions, treeRootSystemOptions);
		}
		
		
		
		if(treeRootSystemOptions.getChildCount()>0){
			List<Object[]> listaPrivilegiosOpciones = systemServiceFacade.listOptionPrivilegesServiceFacade(systemProfileFilter);
			
			
			for (Object[] arrRowPrivilege : listaPrivilegiosOpciones) {
				loadPrivilegeNodeRecursive(arrRowPrivilege,treeRootSystemOptions);
			}
			
			contractPrivileges();
		}
		
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
							
	}
	
	
	/**
	 * Gets the parent options.
	 *
	 * @param listSystemOptions the list system options
	 * @return the parent options
	 */
	private List<Object[]> getParentOptions(List<Object[]> listSystemOptions){		
		List<Object[]> listParentOptions = new ArrayList<Object[]>();
					
		for (Object[] arrRow : listSystemOptions) {
			if (arrRow[1] == null ) {				
				listParentOptions.add(arrRow);
			}
		}
		
		return listParentOptions;
	}
	
	/**
	 * Contract privileges.
	 */
	public void contractPrivileges(){		
		contractPrivilegesRecursive(treeRootSystemOptions);
		nodesExpanded = false;
	}
	
	/**
	 * Contract privileges recursive.
	 *
	 * @param node the node
	 */
	public void contractPrivilegesRecursive(TreeNode node){
		node.setExpanded(false);
		if(node.getChildCount()>0){
			List<TreeNode> lstChildrenNodes = node.getChildren();
			for (TreeNode childreNode : lstChildrenNodes) {
				contractPrivilegesRecursive(childreNode);
			}
		}
	}
	
	/**
	 * Load option privileges recursive.
	 *
	 * @param arrRow the arr row
	 * @param listSystemOptions the list system options
	 * @param parentNode the parent node
	 */
	private void loadOptionPrivilegesRecursive(
			Object[] arrRow, List<Object[]> listSystemOptions, TreeNode parentNode){
		
		Integer idSystemOptionPk = null;
		Integer idSystemOptionParentPk = null;
		
		OptionPrivilegeNodeBean optionPrivilegeNodeBean = new OptionPrivilegeNodeBean();		
				
		if(arrRow[0]!=null){
			idSystemOptionPk = new Integer(arrRow[0].toString());
			optionPrivilegeNodeBean.setIdSystemOption(idSystemOptionPk);
		}
		
		if(arrRow[1]!=null){
			idSystemOptionParentPk = new Integer(arrRow[1].toString());
			optionPrivilegeNodeBean.setIdParentSystemOption(idSystemOptionParentPk);
		}
		

		if(arrRow[2]!=null){
			optionPrivilegeNodeBean.setName(arrRow[2].toString());
		}

		TreeNode node = new DefaultTreeNode(optionPrivilegeNodeBean, parentNode);
			
			Integer idSystemOptionParentChild=null;
			
			for (Object[] objArrChildreNode : listSystemOptions) {
				if(objArrChildreNode[1]!=null){
					idSystemOptionParentChild = new Integer(objArrChildreNode[1].toString());				
					
					if(idSystemOptionPk.intValue()==idSystemOptionParentChild.intValue()){						
					   loadOptionPrivilegesRecursive(objArrChildreNode, listSystemOptions, node);					   					   
					}
					
				}
			}
				
	}
	
	/**
	 * Load privilege node recursive.
	 *
	 * @param arrRowPrivilege the arr row privilege
	 * @param parentNode the parent node
	 */
	private void loadPrivilegeNodeRecursive(Object[] arrRowPrivilege, TreeNode parentNode){
		
		if(parentNode.getData() instanceof OptionPrivilegeNodeBean){
			OptionPrivilegeNodeBean optionNodeData = (OptionPrivilegeNodeBean) parentNode.getData();
			
			if(optionNodeData.getIdSystemOption()!=null){
						
				Integer idSystemOptionFkPrivilege = new Integer(arrRowPrivilege[2].toString());
				
				if(optionNodeData.getIdSystemOption().intValue() == idSystemOptionFkPrivilege.intValue()){
					OptionPrivilegeNodeBean nodoOpcionPrivilegioBean = new OptionPrivilegeNodeBean();
					nodoOpcionPrivilegioBean.setIdOptionPrivilege(new Integer(arrRowPrivilege[0].toString()));
					nodoOpcionPrivilegioBean.setIdPrivilege(new Integer(arrRowPrivilege[1].toString()));
					nodoOpcionPrivilegioBean.setName(PrivilegeType.get(new Integer(arrRowPrivilege[1].toString())).getDescription());
					
				} else{
					if(parentNode.getChildCount()>0){
						for (TreeNode childrenNode : parentNode.getChildren()) {
							loadPrivilegeNodeRecursive(arrRowPrivilege, childrenNode);
						}
					}
				}
			}
									
		} else {
			if(parentNode.getChildCount()>0){
				for (TreeNode nodoHijo : parentNode.getChildren()) {
					loadPrivilegeNodeRecursive(arrRowPrivilege, nodoHijo);
				}
			}
		}
	}
	
	/**
	 * Change mnemonic listener.
	 */
	public void changeMnemonicListener(){
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		hideDialogs();	
		nameExist = false;
		if(Validations.validateIsNotNullAndNotEmpty(systemBean.getMnemonic())){
		try{			
			if(systemServiceFacade.getSystemWithMnemonic(systemBean.getMnemonic())){
				systemBean.setMnemonic(null);
				mnemonicexist = true;
			}else{
				mnemonicexist = false;
			}
		}
		catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		}
	}
	
	/**
	 * Validate the system name in on change event.
	 */
	public void changeNameListener(){
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		hideDialogs();
		Integer idSystemPk = null;
		mnemonicexist=false;
		if(Validations.validateIsNotNullAndNotEmpty(systemBean.getName())){
		try{
			if(isModify()){
				idSystemPk=systemBean.getIdSystemPk();
			}
			if(systemServiceFacade.getSystemWithName(systemBean.getName(),idSystemPk)){
				systemBean.setName(null);
				nameExist = true;
			}else{
				nameExist = false;
			}
		}
		catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		}
	}
	
	/**
	 * Button back action listener to avoid the losing system name of the selected row 
	 * when clear the system name then click on back button.
	 */
	public void backButtonListener(){
		if(isModify()){
			if(systemBean.getName() == null){
				systemBean.setName(sysName);
			}else{
				if(!systemBean.getName().equals(sysName)){
					systemBean.setName(sysName);
				}
			}
		}
	}
	/**
	 * Hide dialogs.
	 */
	public void hideDialogs(){
		JSFUtilities.hideComponent("frmMotive:dlgMotive");
		JSFUtilities.hideComponent("frmBlockSystem:cnfBlock");
		JSFUtilities.hideComponent("frmUnblockSystem:cnfUnblock");
		JSFUtilities.hideComponent("frmAdmSystem:cnfAdmSystem");
		JSFUtilities.hideGeneralDialogues();
	}
	
	/**
	 * File upload handler.
	 *
	 * @param event the event
	 */
	public void fileUploadHandler(FileUploadEvent event){
		hideDialogs();
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);				
		Object[] dataOnMessage = {event.getFile().getFileName(),event.getFile().getSize()/1000};
		showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, "opt.adm.system.fupl.ok",dataOnMessage);
		String fDisplayName=fUploadValidateFile(event.getFile(),"PF('cnfwMsgCustomValidationFupl').show()", null, null);
		 if(fDisplayName!=null){			 
			try {				
				InputStream inputImage = new ByteArrayInputStream(event.getFile().getContents());
				fileSystemImage = new DefaultStreamedContent(inputImage, event.getFile().getContentType(), event.getFile().getFileName());  
				if(isRegister() || isModify()){
					systemImageName = event.getFile().getFileName();
					systemBean.setSystemImage(event.getFile().getContents());					
				}
				systemImgTemp = event.getFile().getContents();
				JSFUtilities.putSessionMap("sessStreamedImage",systemImgTemp);
				//this variable is sent to session to be processed by the method getFileSystemImage in the ImageStreamerBean class and displayed on page
				auxFuplSystemImg=Boolean.TRUE;
				formState = Boolean.TRUE;
			} catch (Exception e) {
				e.printStackTrace();
			}
			 JSFUtilities.showValidationDialog();
		 }
	}
	
	/**
	 * File register upload handler.
	 *
	 * @param event the event
	 */
	public void fileRegisterUploadHandler(FileUploadEvent event){
		hideDialogs();
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);				
		Object[] dataOnMessage = {event.getFile().getFileName(),event.getFile().getSize()/1000};
		showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, "opt.adm.system.fupl.document.ok",dataOnMessage);
		String fDisplayName=fUploadValidateFile(event.getFile(),"PF('cnfwMsgCustomValidationFupl').show()", null,null, fUploadFileRegisterTypes);
		 if(fDisplayName!=null){			 
			try {				
				InputStream inputFile = new ByteArrayInputStream(event.getFile().getContents());
				fileRegisterSystem = new DefaultStreamedContent(inputFile, event.getFile().getContentType(), event.getFile().getFileName());  
				if(isRegister() || isModify()){
					systemFileName = event.getFile().getFileName();
					systemBean.setSystemProductionFile(event.getFile().getContents());					
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			 JSFUtilities.showValidationDialog();
		 }
	}
	
	/**
	 * Delete system image temp.
	 */
	public void deleteSystemImageTemp(){
		systemImgTemp = null;
		systemImageName = "";
		systemBean.setSystemImage(null);
		fileSystemImage = null;
		JSFUtilities.putSessionMap("sessStreamedImage",null);
	}
	
	/**
	 * Delete system file temp.
	 */
	public void deleteSystemFileTemp(){
		systemFileTemp = null;
		systemFileName = "";
		systemBean.setSystemProductionFile(null);
		fileRegisterSystem = null;
	}
	
	/*---------------------------------------------------------------------------------------------------------------------*/
	/**
	 * Checks if is show block motive.
	 *
	 * @return the showBlockMotive
	 */
	public boolean isShowBlockMotive() {
		return showBlockMotive;
	}	
	/**
	 * Checks if is show unblock motive.
	 *
	 * @return the showUnblockMotive
	 */
	public boolean isShowUnblockMotive() {
		return showUnblockMotive;
	}
	/**
	 * Checks if is show other motive.
	 *
	 * @return true, if is show other motive
	 */
	public boolean isShowOtherMotive() {
		return showOtherMotive;
	}
	/**
	 * Checks if is mnemonicexist.
	 *
	 * @return the mnemonicexist
	 */
	public boolean isMnemonicexist() {
		return mnemonicexist;
	}	
	/**
	 * Checks if is name exist.
	 *
	 * @return the nameExist
	 */
	public boolean isNameExist() {
		return nameExist;
	}
	/*---------------------------------------------------------------------------------------------------------------------*/
	/**
	 * For Block Descripcion.
	 *
	 * @return the motive descripcion
	 */
	public String getMotiveDescripcion(){
	    String desc = "";
	    if(systemBean.getBlockMotive() != null){
			if(lstBlockMotives != null && lstBlockMotives.size() > 0 ){
				for(int i=0;i<lstBlockMotives.size();i++){
					if(systemBean.getBlockMotive().compareTo(lstBlockMotives.get(i).getIdParameterPk()) == 0){
							desc=lstBlockMotives.get(i).getName();
						}
					}
				}
			}
		return desc;
	}	
	/**
	 * For unBlock Descripcion.
	 *
	 * @return the motiveun block descripcion
	 */
	public String getMotiveunBlockDescripcion(){
	    String desc = "";
	    if(systemBean.getUnblockMotive() != null){
			if(lstUnBlockMotives != null && lstUnBlockMotives.size() > 0 ){
				for(int i=0;i<lstUnBlockMotives.size();i++){
					if(systemBean.getUnblockMotive().compareTo(lstUnBlockMotives.get(i).getIdParameterPk()) == 0){
							desc=lstUnBlockMotives.get(i).getName();
						}
					}
				}
			}
		return desc;
	}	
	/*---------------------------------------------------------------------------------------------------------------------*/
	/**
	 * Sets the show block motive.
	 *
	 * @param showBlockMotive the showBlockMotive to set
	 */
	public void setShowBlockMotive(boolean showBlockMotive) {
		this.showBlockMotive = showBlockMotive;
	}
	/**
	 * Sets the show unblock motive.
	 *
	 * @param showUnblockMotive the showUnblockMotive to set
	 */
	public void setShowUnblockMotive(boolean showUnblockMotive) {
		this.showUnblockMotive = showUnblockMotive;
	}	
	/**
	 * Gets the file system image.
	 *
	 * @return the file system image
	 */
	public StreamedContent getFileSystemImage() {
		return fileSystemImage;
	}
	/**
	 * Sets the file system image.
	 *
	 * @param fileSystemImage the new file system image
	 */
	public void setFileSystemImage(StreamedContent fileSystemImage) {
		this.fileSystemImage = fileSystemImage;
	}
	/**
	 * Gets the lst system profile.
	 *
	 * @return the lst system profile
	 */
	public List<SystemProfile> getLstSystemProfile() {
		return lstSystemProfile;
	}
	/**
	 * Sets the lst system profile.
	 *
	 * @param lstSystemProfile the new lst system profile
	 */
	public void setLstSystemProfile(List<SystemProfile> lstSystemProfile) {
		this.lstSystemProfile = lstSystemProfile;
	}

	/**
	 * Gets the tree root system options.
	 *
	 * @return the tree root system options
	 */
	public TreeNode getTreeRootSystemOptions() {
		return treeRootSystemOptions;
	}
	/**
	 * Sets the tree root system options.
	 *
	 * @param treeRootSystemOptions the new tree root system options
	 */
	public void setTreeRootSystemOptions(TreeNode treeRootSystemOptions) {
		this.treeRootSystemOptions = treeRootSystemOptions;
	}
	/**
	 * Gets the historical state.
	 *
	 * @return the historical state
	 */
	public HistoricalState getHistoricalState() {
		return historicalState;
	}
	/**
	 * Sets the historical state.
	 *
	 * @param historicalState the new historical state
	 */
	public void setHistoricalState(HistoricalState historicalState) {
		this.historicalState = historicalState;
	}
	/**
	 * Sets the show other motive.
	 *
	 * @param showOtherMotive the new show other motive
	 */
	public void setShowOtherMotive(boolean showOtherMotive) {
		this.showOtherMotive = showOtherMotive;
	}
	/**
	 * Gets the aux fupl system img.
	 *
	 * @return the aux fupl system img
	 */
	public Boolean getAuxFuplSystemImg() {
		return auxFuplSystemImg;
	}
	/**
	 * Sets the aux fupl system img.
	 *
	 * @param auxFuplSystemImg the new aux fupl system img
	 */
	public void setAuxFuplSystemImg(Boolean auxFuplSystemImg) {
		this.auxFuplSystemImg = auxFuplSystemImg;
	}
	/**
	 * Sets the mnemonicexist.
	 *
	 * @param mnemonicexist the mnemonicexist to set
	 */
	public void setMnemonicexist(boolean mnemonicexist) {
		this.mnemonicexist = mnemonicexist;
	}
	/**
	 * Sets the name exist.
	 *
	 * @param nameExist the nameExist to set
	 */
	public void setNameExist(boolean nameExist) {
		this.nameExist = nameExist;
	}
	/**
	 * Gets the sys name.
	 *
	 * @return the sysName
	 */
	public String getSysName() {
		return sysName;
	}
	/**
	 * Sets the sys name.
	 *
	 * @param sysName the sysName to set
	 */
	public void setSysName(String sysName) {
		this.sysName = sysName;
	}
	/**
	 * Gets the form state.
	 *
	 * @return the formState
	 */
	public Boolean getFormState() {
		return formState;
	}
	/**
	 * Sets the form state.
	 *
	 * @param formState the formState to set
	 */
	public void setFormState(Boolean formState) {
		this.formState = formState;
	}
	/**
	 * Gets the system bean.
	 *
	 * @return the system bean
	 */
	public System getSystemBean() {
		return systemBean;	
	}
	/**
	 * Sets the system bean.
	 *
	 * @param sistemaBean the new system bean
	 */
	public void setSystemBean(System sistemaBean) {
		this.systemBean = sistemaBean;
	}
	/**
	 * Gets the listado cbo estado sistema.
	 *
	 * @return the listado cbo estado sistema
	 */
	public List<SelectItem> getLstCboSystemState() {
		return lstCboSystemState;
	}
	/**
	 * Sets the listado cbo estado sistema.
	 *
	 * @param lstCboSystemState the new lst cbo system state
	 */
	public void setLstCboSystemState(List<SelectItem> lstCboSystemState) {
		this.lstCboSystemState = lstCboSystemState;
	}
	/**
	 * Gets the system filter.
	 *
	 * @return the system filter
	 */
	public SystemFilter getSystemFilter() {
		return systemFilter;
	}
	/**
	 * Sets the system filter.
	 *
	 * @param systemFilter the new system filter
	 */
	public void setSystemFilter(SystemFilter systemFilter) {
		this.systemFilter = systemFilter;
	}
	/**
	 * Gets the system data model.
	 *
	 * @return the system data model
	 */
	public SystemDataModel getSystemDataModel() {
		return systemDataModel;
	}
	/**
	 * Sets the system data model.
	 *
	 * @param systemDataModel the new system data model
	 */
	public void setSystemDataModel(SystemDataModel systemDataModel) {
		this.systemDataModel = systemDataModel;
	}
	/**
	 * Gets the selected system bean.
	 *
	 * @return the selected system bean
	 */
	public System getSelectedSystemBean() {
		return selectedSystemBean;
	}
	/**
	 * Sets the selected system bean.
	 *
	 * @param selectedSystem the new selected system bean
	 */
	public void setSelectedSystemBean(System selectedSystem) {
		this.selectedSystemBean = selectedSystem;
	}
	/**
	 * Gets the lst cbo schedule.
	 *
	 * @return the lst cbo schedule
	 */
	public List<Schedule> getLstCboSchedule() {
		return lstCboSchedule;
	}
	/**
	 * Sets the lst cbo schedule.
	 *
	 * @param lstCboSchedule the new lst cbo schedule
	 */
	public void setLstCboSchedule(List<Schedule> lstCboSchedule) {
		this.lstCboSchedule = lstCboSchedule;
	}

	/**
	 * Checks if is confirm block or unblock.
	 *
	 * @return true, if is confirm block or unblock
	 */
	public boolean isConfirmBlockOrUnblock() {
		return isConfirmBlockOrUnblock;
	}

	/**
	 * Checks if is reject opcion.
	 *
	 * @return true, if is reject opcion
	 */
	public boolean isRejectOpcion() {
		return isRejectOpcion;
	}

	/**
	 * Sets the reject opcion.
	 *
	 * @param isRejectOpcion the new reject opcion
	 */
	public void setRejectOpcion(boolean isRejectOpcion) {
		this.isRejectOpcion = isRejectOpcion;
	}

	/**
	 * Sets the confirm block or unblock.
	 *
	 * @param isConfirmBlockOrUnblock the new confirm block or unblock
	 */
	public void setConfirmBlockOrUnblock(boolean isConfirmBlockOrUnblock) {
		this.isConfirmBlockOrUnblock = isConfirmBlockOrUnblock;
	}

	/**
	 * Gets the system image name.
	 *
	 * @return the system image name
	 */
	public String getSystemImageName() {
		return systemImageName;
	}

	/**
	 * Sets the system image name.
	 *
	 * @param systemImageName the new system image name
	 */
	public void setSystemImageName(String systemImageName) {
		this.systemImageName = systemImageName;
	}

	/**
	 * Gets the f upload file register types.
	 *
	 * @return the f upload file register types
	 */
	public String getfUploadFileRegisterTypes() {
		return fUploadFileRegisterTypes;
	}

	/**
	 * Sets the f upload file register types.
	 *
	 * @param fUploadFileRegisterTypes the new f upload file register types
	 */
	public void setfUploadFileRegisterTypes(String fUploadFileRegisterTypes) {
		this.fUploadFileRegisterTypes = fUploadFileRegisterTypes;
	}

	/* (non-Javadoc)
	 * @see com.pradera.security.utils.view.GenericBaseBean#getfUploadFileTypesDisplay()
	 */
	public String getfUploadFileTypesDisplay() {
		return fUploadFileTypesDisplay;
	}

	/* (non-Javadoc)
	 * @see com.pradera.security.utils.view.GenericBaseBean#setfUploadFileTypesDisplay(java.lang.String)
	 */
	public void setfUploadFileTypesDisplay(String fUploadFileTypesDisplay) {
		this.fUploadFileTypesDisplay = fUploadFileTypesDisplay;
	}

	/**
	 * Gets the file register system.
	 *
	 * @return the file register system
	 */
	public StreamedContent getFileRegisterSystem() {
		return fileRegisterSystem;
	}

	/**
	 * Sets the file register system.
	 *
	 * @param fileRegisterSystem the new file register system
	 */
	public void setFileRegisterSystem(StreamedContent fileRegisterSystem) {
		this.fileRegisterSystem = fileRegisterSystem;
	}

	/**
	 * Gets the system file name.
	 *
	 * @return the system file name
	 */
	public String getSystemFileName() {
		return systemFileName;
	}

	/**
	 * Sets the system file name.
	 *
	 * @param systemFileName the new system file name
	 */
	public void setSystemFileName(String systemFileName) {
		this.systemFileName = systemFileName;
	}

	public List<SelectItem> getLstCboDisplaySystem() {
		return lstCboDisplaySystem;
	}

	public void setLstCboDisplaySystem(List<SelectItem> lstCboDisplaySystem) {
		this.lstCboDisplaySystem = lstCboDisplaySystem;
	}	
	
	
}
