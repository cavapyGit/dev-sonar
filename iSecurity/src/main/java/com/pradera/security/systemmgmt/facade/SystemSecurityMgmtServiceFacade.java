package com.pradera.security.systemmgmt.facade;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.security.model.type.LanguageType;
import com.pradera.commons.services.remote.security.SystemSecurityMgmtService;
import com.pradera.integration.common.validation.Validations;
import com.pradera.security.model.SystemProfile;
import com.pradera.security.model.type.OptionPrivilegeStateType;
import com.pradera.security.model.type.OptionStateType;
import com.pradera.security.model.type.OptionType;
import com.pradera.security.model.type.PrivilegeType;
import com.pradera.security.model.type.ProfilePrivilegeStateType;
import com.pradera.security.systemmgmt.view.OptionPrivilegeNodeBean;
import com.pradera.security.systemmgmt.view.filters.SystemProfileFilter;

/**
 * The State less session bean class MonitoringReporteFacade
 * @Project : PraderaReportes
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.0
 */
@Stateless
@Performance
@Remote(SystemSecurityMgmtService.class)
public class SystemSecurityMgmtServiceFacade  implements SystemSecurityMgmtService
{

	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The system profile service bean. */
	/** The system service facade. */
	@EJB
    protected SystemServiceFacade systemServiceFacade;
    
	 /** The root privileges. */
    private TreeNode rootPrivileges;
    
    SimpleDateFormat dateFormat =  new SimpleDateFormat("dd/MM/YYYY");
	
	@Override
	public List<Object[]> getSystemProfileAndOptions(Integer idSystem,String strProfileMn,Integer state) throws Exception{
		
		
		String systemName = null;
		String  profileMN = null;
		Date profileRegDt = null; 
		String moduleName =null;
		String subModuleName =null;
		
		List<Object[]> listObj = new ArrayList<Object[]>();
 		SystemProfileFilter systemProfileFilter =  new SystemProfileFilter();
		systemProfileFilter.setIdSystemBaseQuery(idSystem);
		
		if(Validations.validateIsNotNullAndNotEmpty(strProfileMn)){
			systemProfileFilter.setProfileMnemonicBaseQuery(strProfileMn);
		}
		if(Validations.validateIsNotNullAndNotEmpty(state != null)){
			systemProfileFilter.setProfileStateBaseQuery(state);
		}
		List<SystemProfile> lstSystemProfile = systemServiceFacade.searchProfilesByFiltersServiceFacade(systemProfileFilter);
		
		for(SystemProfile profile: lstSystemProfile){
			systemName = profile.getSystem().getName();
			profileMN = profile.getName();
			profileRegDt = profile.getRegistryDate();
				
				systemProfileFilter.setIdSystem(profile.getSystem().getIdSystemPk());
				systemProfileFilter.setIdLanguage(LanguageType.SPANISH.getCode());
				systemProfileFilter.setSystemOptionState(OptionStateType.CONFIRMED.getCode());
				systemProfileFilter.setOptionPrivilegeState(OptionPrivilegeStateType.REGISTERED.getCode());
				systemProfileFilter.setProfilePrivilegeState(ProfilePrivilegeStateType.REGISTERED.getCode());
				systemProfileFilter.setIdSystemProfile(profile.getIdSystemProfilePk());
				
				List<Object[]> lstProfilePrivileges = systemServiceFacade.listOptionPrivilegesByProfileServiceFacade(systemProfileFilter);
				if(!lstProfilePrivileges.isEmpty()){
					List<Integer> lstIdOptionPrivilege = new ArrayList<Integer>();
					
					for (Object[] arrPrivilegeRow : lstProfilePrivileges) {
						lstIdOptionPrivilege.add(Integer.valueOf(arrPrivilegeRow[0].toString()));
					}
					
					systemProfileFilter.setLstIdOptionPrivilege(lstIdOptionPrivilege);
					
					List<Integer> lstIdSystemOptions  = systemServiceFacade.listIdSystemOptiosByOptionPrivilegeServiceFacade(systemProfileFilter);
					
					systemProfileFilter.setLstIdSystemOption(lstIdSystemOptions);
					
					
					List<Object[]> lstSystemOptions = systemServiceFacade.listSystemOptionsByIdsServiceFacade(systemProfileFilter);		
					
					rootPrivileges = new DefaultTreeNode("Root", null);			
					
					List<Object[]> lstParentSystemOptions = getParentSystemOptions(lstSystemOptions);
					
					for (Object[] arrRow : lstParentSystemOptions) {
						loadPrivilegeOptionsRecursive(arrRow,lstSystemOptions, rootPrivileges);
					}
					
					if(rootPrivileges.getChildCount() > 0) {
						
						for (Object[] arrPrivilegeRow : lstProfilePrivileges) {
							loadPrivilegeNodeRecursive(arrPrivilegeRow,rootPrivileges);
						}
						
					}
					
						List<TreeNode> modules = rootPrivileges.getChildren();
						for(TreeNode module: modules){
							OptionPrivilegeNodeBean optPNModule = (OptionPrivilegeNodeBean)module.getData();
							if(optPNModule.isOptionChk()){
								findAndPrepareProfPriv(module, systemName, profileMN, profileRegDt, "-", "-", optPNModule.getIdSystemOption(), optPNModule.getName(), listObj);
							}else{
								moduleName = optPNModule.getName();
								List<TreeNode> subModules = module.getChildren();
							    if(subModules != null){
							    	for (TreeNode subModuleNode : subModules) {
							    		OptionPrivilegeNodeBean optPNSubModule = (OptionPrivilegeNodeBean)subModuleNode.getData();
							    		if(optPNSubModule.isOptionChk()){
											findAndPrepareProfPriv(subModuleNode, systemName, profileMN, profileRegDt, moduleName, "-", optPNSubModule.getIdSystemOption(), optPNSubModule.getName(), listObj);
										}else{
											subModuleName = optPNSubModule.getName();
											List<TreeNode> optionNodes = subModuleNode.getChildren();
											if(optionNodes != null){
												 for (TreeNode optionNode : optionNodes) {
													 OptionPrivilegeNodeBean optNodeBean = (OptionPrivilegeNodeBean)optionNode.getData();
											    	 if(optNodeBean.isOptionChk()){
														findAndPrepareProfPriv(optionNode, systemName, profileMN, profileRegDt, moduleName, subModuleName, optNodeBean.getIdSystemOption(), optNodeBean.getName(), listObj);
													 }
												 }
											 }
										}
									}
							    }
							}
						}	
				}
				
		}
		
		return listObj;
	}
	
	private void findAndPrepareProfPriv(TreeNode node,String systemName,String  profileMN,Date profileRegDt,String moduleName,String subModuleName,Integer idOption,String optionName,List<Object[]> listObj){
		String dataReg = "";
		List<TreeNode> lstPrfPrev = node.getChildren();
		for (TreeNode prfPrevNode : lstPrfPrev) {
			OptionPrivilegeNodeBean prfPrevNodeBean = (OptionPrivilegeNodeBean)prfPrevNode.getData();
			Object[] objArr = new  Object[8] ;
			objArr[0] = systemName;
			objArr[1] = profileMN; 
			if(profileRegDt != null){
				dataReg = dateFormat.format(profileRegDt);
				objArr[2] = dataReg;
			}
			
			objArr[3] = moduleName;
			objArr[4] = subModuleName;
			objArr[5] = idOption;
			objArr[6] = optionName;
			objArr[7] = prfPrevNodeBean.getName();
			listObj.add(objArr);
		}
	}

	/**
	 * Load privilege options recursive.
	 *
	 * @param arrRow the arr row
	 * @param lstSystemOptions the lst system options
	 * @param parentNode the parent node
	 */
	private void loadPrivilegeOptionsRecursive(
		Object[] arrRow, List<Object[]> lstSystemOptions, TreeNode parentNode){
		
		Integer idSystemOption = null;
		Integer idParentSystemOption = null;
		
		OptionPrivilegeNodeBean optionPrivilegeNodeBean = new OptionPrivilegeNodeBean();		
				
		if(arrRow[0]!=null){
			idSystemOption = Integer.valueOf(arrRow[0].toString());
			optionPrivilegeNodeBean.setIdSystemOption(idSystemOption);
		}
		
		if(arrRow[1]!=null){
			idParentSystemOption = Integer.valueOf(arrRow[1].toString());
			optionPrivilegeNodeBean.setIdParentSystemOption(idParentSystemOption);
		}
		

		if(arrRow[2]!=null){
			optionPrivilegeNodeBean.setName(arrRow[2].toString());
		}
		
		if(arrRow[5]!=null && OptionType.OPTION.getCode().equals(Integer.valueOf(arrRow[5].toString()))){
			optionPrivilegeNodeBean.setOptionChk(true);
		}
		
		TreeNode node = new DefaultTreeNode(optionPrivilegeNodeBean, parentNode);

			Integer idParentSystemOptionFromChild = null;
			
			for (Object[] objArrFilaHija : lstSystemOptions) {
				if(objArrFilaHija[1]!=null){
					idParentSystemOptionFromChild = Integer.valueOf(objArrFilaHija[1].toString());				
					
					if(idSystemOption.intValue()==idParentSystemOptionFromChild.intValue()){						
					   loadPrivilegeOptionsRecursive(objArrFilaHija, lstSystemOptions, node);					   					   
					}
					
				}
			}
				
	}
	
	
	/**
	 * Gets the parent system options.
	 *
	 * @param listaOpcioncesSistema the lista opcionces sistema
	 * @return the parent system options
	 */
	private List<Object[]> getParentSystemOptions(List<Object[]> listaOpcioncesSistema){		
		List<Object[]> lstParentSystemOptions = new ArrayList<Object[]>();
					
		for (Object[] arrRow : listaOpcioncesSistema) {
			if (arrRow[1] == null ) {				
				lstParentSystemOptions.add(arrRow);
			}
		}
		
		return lstParentSystemOptions;
	}
	
	/**
	 * Load privilege node recursive.
	 *
	 * @param arrPrivilegeRow the arr privilege row
	 * @param parentNode the parent node
	 */
	private void loadPrivilegeNodeRecursive(Object[] arrPrivilegeRow, TreeNode parentNode){
		
		if(parentNode.getData() instanceof OptionPrivilegeNodeBean){
			OptionPrivilegeNodeBean dataNodoOpcion = (OptionPrivilegeNodeBean) parentNode.getData();
			
			if(dataNodoOpcion.getIdSystemOption()!=null){
				
				Integer idOpcionSistemaFkPrivilegio = Integer.valueOf(arrPrivilegeRow[2].toString());
				
				if(dataNodoOpcion.getIdSystemOption().intValue() == idOpcionSistemaFkPrivilegio.intValue()){
					OptionPrivilegeNodeBean nodoOpcionPrivilegioBean = new OptionPrivilegeNodeBean();
					nodoOpcionPrivilegioBean.setIdOptionPrivilege(Integer.valueOf(arrPrivilegeRow[0].toString()));
					nodoOpcionPrivilegioBean.setIdPrivilege(Integer.valueOf(arrPrivilegeRow[1].toString()));
					nodoOpcionPrivilegioBean.setName(PrivilegeType.get(Integer.valueOf(arrPrivilegeRow[1].toString())).getDescription());
					
					new DefaultTreeNode(nodoOpcionPrivilegioBean, parentNode);
				} else{
					if(parentNode.getChildCount()>0){
						for (TreeNode childNode : parentNode.getChildren()) {
							loadPrivilegeNodeRecursive(arrPrivilegeRow, childNode);
						}
					}
				}
			}
									
		} else {
			if(parentNode.getChildCount()>0){
				for (TreeNode childNode : parentNode.getChildren()) {
					loadPrivilegeNodeRecursive(arrPrivilegeRow, childNode);
				}
			}
		}
	}
	
}
