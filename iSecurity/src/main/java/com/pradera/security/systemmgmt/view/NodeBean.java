package com.pradera.security.systemmgmt.view;

import java.io.Serializable;
import java.util.List;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class NodeBean
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public class NodeBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The code. */
	private Integer code;
	
	/** The name. */
	private String name;
	
	/** The description. */
	private String description;
	
	/** The level. */
	private Integer level;
	
	/** The children node. */
	private List<NodeBean> childrenNode;

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the code to set
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the level.
	 *
	 * @return the level
	 */
	public Integer getLevel() {
		return level;
	}

	/**
	 * Sets the level.
	 *
	 * @param level the level to set
	 */
	public void setLevel(Integer level) {
		this.level = level;
	}

	/**
	 * Gets the children node.
	 *
	 * @return the childrenNode
	 */
	public List<NodeBean> getChildrenNode() {
		return childrenNode;
	}

	/**
	 * Sets the children node.
	 *
	 * @param childrenNode the childrenNode to set
	 */
	public void setChildrenNode(List<NodeBean> childrenNode) {
		this.childrenNode = childrenNode;
	}

	
}
