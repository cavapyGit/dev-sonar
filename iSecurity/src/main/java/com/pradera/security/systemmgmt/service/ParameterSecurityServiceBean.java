package com.pradera.security.systemmgmt.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.integration.exception.ServiceException;
import com.pradera.security.model.Parameter;

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class ParameterSecurityServiceBean
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ParameterSecurityServiceBean extends CrudSecurityServiceBean{

	
	 /** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3158890423768042524L;

	/**
	 * List parameter by definition service bean.
	 *
	 * @param idDefinition the id definition
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Parameter> listParameterByDefinitionServiceBean(Integer idDefinition)throws ServiceException{
		 	Map<String,Object> parameters = new HashMap<String, Object>();
		 	parameters.put("idDefinition", idDefinition);
	    	
	    	return (List<Parameter>)findWithNamedQuery(Parameter.PARAMETER_SEARCH_BY_DEFINITION, parameters);			
	 }

	/**
	 * List parameter by identificator.
	 *
	 * @param parametersId the parameters id
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Parameter> listParameterByIdentificator(List<Integer> parametersId)throws ServiceException{
	 	Map<String,Object> parameters = new HashMap<String, Object>();
	 	parameters.put("list", parametersId);
    	return (List<Parameter>)findByQueryString(" select p from Parameter p where p.idParameterPk in (:list) ", parameters);			
	}
	
	/**
	 * Parameter by pk.
	 *
	 * @param parametersId the parameters id
	 * @return the Object
	 * @throws ServiceException the service exception
	 */
	public Parameter getParameterByPk(Integer parametersId)throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("Select p from Parameter p where p.idParameterPk=:idParameter");	
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idParameter", parametersId);
			return (Parameter) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}		
	}
}
