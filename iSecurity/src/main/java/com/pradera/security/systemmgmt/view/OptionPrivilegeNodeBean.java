package com.pradera.security.systemmgmt.view;

import java.io.Serializable;
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class OptionPrivilegeNodeBean
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.1
 */
public class OptionPrivilegeNodeBean implements Serializable {
	
	private Integer idSystemOption;
	
	private Integer idParentSystemOption;
	
	private Integer idOptionPrivilege;
	
	private Integer idPrivilege;
	
	private String name;
	
	private boolean optionChk;
	
	/**
	 * @return the idSystemOption
	 */
	public Integer getIdSystemOption() {
		return idSystemOption;
	}

	/**
	 * @param idSystemOption the idSystemOption to set
	 */
	public void setIdSystemOption(Integer idSystemOption) {
		this.idSystemOption = idSystemOption;
	}

	/**
	 * @return the idParentSystemOption
	 */
	public Integer getIdParentSystemOption() {
		return idParentSystemOption;
	}

	/**
	 * @param idParentSystemOption the idParentSystemOption to set
	 */
	public void setIdParentSystemOption(Integer idParentSystemOption) {
		this.idParentSystemOption = idParentSystemOption;
	}

	/**
	 * @return the idOptionPrivilege
	 */
	public Integer getIdOptionPrivilege() {
		return idOptionPrivilege;
	}

	/**
	 * @param idOptionPrivilege the idOptionPrivilege to set
	 */
	public void setIdOptionPrivilege(Integer idOptionPrivilege) {
		this.idOptionPrivilege = idOptionPrivilege;
	}

	/**
	 * @return the idPrivilege
	 */
	public Integer getIdPrivilege() {
		return idPrivilege;
	}

	/**
	 * @param idPrivilege the idPrivilege to set
	 */
	public void setIdPrivilege(Integer idPrivilege) {
		this.idPrivilege = idPrivilege;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the idProfilePrivilege
	 */
	public Integer getIdProfilePrivilege() {
		return idProfilePrivilege;
	}

	/**
	 * @param idProfilePrivilege the idProfilePrivilege to set
	 */
	public void setIdProfilePrivilege(Integer idProfilePrivilege) {
		this.idProfilePrivilege = idProfilePrivilege;
	}

	private Integer idProfilePrivilege;
	
	/**
	 * Constructor
	 */
	public OptionPrivilegeNodeBean() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the optionChk
	 */
	public boolean isOptionChk() {
		return optionChk;
	}

	/**
	 * @param optionChk the optionChk to set
	 */
	public void setOptionChk(boolean optionChk) {
		this.optionChk = optionChk;
	}


}
