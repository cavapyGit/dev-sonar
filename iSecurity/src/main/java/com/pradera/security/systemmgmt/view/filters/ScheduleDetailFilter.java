package com.pradera.security.systemmgmt.view.filters;

import java.io.Serializable;
import java.util.Date;

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class ScheduleDetailFilter is useful to put filters in the FrontEnd
 * for example <p:selectMenu id='filterObject.day' ... />.
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.1
 */

public class ScheduleDetailFilter implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The day. */
	private Integer day;
	
	/** The id state. */
	private Integer scheduleDetailState;
	
	/** The hour. */
	private Date hour;

	/**
	 * The Constructor.
	 */
	public ScheduleDetailFilter() {
	}

	/**
	 * Gets the day.
	 *
	 * @return the day
	 */
	public Integer getDay() {
		return day;
	}

	/**
	 * Sets the day.
	 *
	 * @param day the day
	 */
	public void setDay(Integer day) {
		this.day = day;
	}

	public Integer getScheduleDetailState() {
		return scheduleDetailState;
	}

	public void setScheduleDetailState(Integer scheduleDetailState) {
		this.scheduleDetailState = scheduleDetailState;
	}

	/**
	 * Gets the hour.
	 *
	 * @return the hour
	 */
	public Date getHour() {
		return hour;
	}

	/**
	 * Sets the hour.
	 *
	 * @param hour the hour
	 */
	public void setHour(Date hour) {
		this.hour = hour;
	}
}