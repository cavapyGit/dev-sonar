package com.pradera.security.systemmgmt.view.filters;

import java.io.Serializable;
import java.util.List;
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class SystemProfileFilter
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.1
 */
public class SystemProfileFilter implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Integer idSystemProfileBaseQuery;
	private String profileMnemonicBaseQuery;
	private String profileNameBaseQuery;
	private Integer idSystemBaseQuery;
	private Integer	profileStateBaseQuery;
	
	private Integer idLanguage;	
	private Integer idSystem;
	private Integer systemOptionState;
	private Integer optionPrivilegeState;
	private Integer idSystemProfile;
	private Integer profilePrivilegeState;
	private List<Integer> lstIdSystemOption;
	private List<Integer> lstIdOptionPrivilege;	
	
	private Integer indUserType;
	private Integer institutionType;
	
	/**
	 * Constructor
	 */
	public SystemProfileFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return idSystemProfileBaseQuery
	 */
	public Integer getIdSystemProfileBaseQuery() {
		return idSystemProfileBaseQuery;
	}

	/**
	 * @param idSystemProfileBaseQuery
	 */
	public void setIdSystemProfileBaseQuery(Integer idSystemProfileBaseQuery) {
		this.idSystemProfileBaseQuery = idSystemProfileBaseQuery;
	}

	/**
	 * @return profileMnemonicBaseQuery
	 */
	public String getProfileMnemonicBaseQuery() {
		return profileMnemonicBaseQuery;
	}

	/**
	 * @param profileMnemonicBaseQuery
	 */
	public void setProfileMnemonicBaseQuery(String profileMnemonicBaseQuery) {
		if(profileMnemonicBaseQuery!=null){
			profileMnemonicBaseQuery = profileMnemonicBaseQuery.toUpperCase();
		}
		this.profileMnemonicBaseQuery = profileMnemonicBaseQuery;
	}

	/**
	 * @return profileNameBaseQuery
	 */
	public String getProfileNameBaseQuery() {
		return profileNameBaseQuery;
	}

	/**
	 * @param profileNameBaseQuery
	 */
	public void setProfileNameBaseQuery(String profileNameBaseQuery) {
		if(profileNameBaseQuery!=null){
			profileNameBaseQuery = profileNameBaseQuery.toUpperCase();
		}
		this.profileNameBaseQuery = profileNameBaseQuery;
	}

	/**
	 * @return idSystemBaseQuery
	 */
	public Integer getIdSystemBaseQuery() {
		return idSystemBaseQuery;
	}

	/**
	 * @param idSystemBaseQuery
	 */
	public void setIdSystemBaseQuery(Integer idSystemBaseQuery) {
		this.idSystemBaseQuery = idSystemBaseQuery;
	}

	/**
	 * @return profileStateBaseQuery
	 */
	public Integer getProfileStateBaseQuery() {
		return profileStateBaseQuery;
	}

	/**
	 * @param profileStateBaseQuery
	 */
	public void setProfileStateBaseQuery(Integer profileStateBaseQuery) {
		this.profileStateBaseQuery = profileStateBaseQuery;
	}

	/**
	 * @return idLanguage
	 */
	public Integer getIdLanguage() {
		return idLanguage;
	}

	/**
	 * @param idLanguage
	 */
	public void setIdLanguage(Integer idLanguage) {
		this.idLanguage = idLanguage;
	}

	/**
	 * @return idSystem
	 */
	public Integer getIdSystem() {
		return idSystem;
	}

	/**
	 * @param idSystem
	 */
	public void setIdSystem(Integer idSystem) {
		this.idSystem = idSystem;
	}

	/** 
	 * @return systemOptionState
	 */
	public Integer getSystemOptionState() {
		return systemOptionState;
	}

	/**
	 * @param systemOptionState
	 */
	public void setSystemOptionState(Integer systemOptionState) {
		this.systemOptionState = systemOptionState;
	}

	/**
	 * @return optionPrivilegeState
	 */
	public Integer getOptionPrivilegeState() {
		return optionPrivilegeState;
	}

	/**
	 * @param optionPrivilegeState
	 */
	public void setOptionPrivilegeState(Integer optionPrivilegeState) {
		this.optionPrivilegeState = optionPrivilegeState;
	}

	/**
	 * @return idSystemProfile
	 */
	public Integer getIdSystemProfile() {
		return idSystemProfile;
	}

	/**
	 * @param idSystemProfile
	 */
	public void setIdSystemProfile(Integer idSystemProfile) {
		this.idSystemProfile = idSystemProfile;
	}

	/**
	 * @return profilePrivilegeState
	 */
	public Integer getProfilePrivilegeState() {
		return profilePrivilegeState;
	}

	/**
	 * @param profilePrivilegeState
	 */
	public void setProfilePrivilegeState(Integer profilePrivilegeState) {
		this.profilePrivilegeState = profilePrivilegeState;
	}

	/**
	 * @return lstIdSystemOption
	 */
	public List<Integer> getLstIdSystemOption() {
		return lstIdSystemOption;
	}

	/**
	 * @param lstIdSystemOption
	 */
	public void setLstIdSystemOption(List<Integer> lstIdSystemOption) {
		this.lstIdSystemOption = lstIdSystemOption;
	}

	/**
	 * @return lstIdOptionPrivilege
	 */
	public List<Integer> getLstIdOptionPrivilege() {
		return lstIdOptionPrivilege;
	}

	/**
	 * @param lstIdOptionPrivilege
	 */
	public void setLstIdOptionPrivilege(List<Integer> lstIdOptionPrivilege) {
		this.lstIdOptionPrivilege = lstIdOptionPrivilege;
	}

	public Integer getIndUserType() {
		return indUserType;
	}

	public void setIndUserType(Integer indUserType) {
		this.indUserType = indUserType;
	}

	public Integer getInstitutionType() {
		return institutionType;
	}

	public void setInstitutionType(Integer institutionType) {
		this.institutionType = institutionType;
	}

	@Override
	public String toString() {
		return "SystemProfileFilter [idSystemProfileBaseQuery=" + idSystemProfileBaseQuery
				+ ", profileMnemonicBaseQuery=" + profileMnemonicBaseQuery + ", profileNameBaseQuery="
				+ profileNameBaseQuery + ", idSystemBaseQuery=" + idSystemBaseQuery + ", profileStateBaseQuery="
				+ profileStateBaseQuery + ", idLanguage=" + idLanguage + ", idSystem=" + idSystem
				+ ", systemOptionState=" + systemOptionState + ", optionPrivilegeState=" + optionPrivilegeState
				+ ", idSystemProfile=" + idSystemProfile + ", profilePrivilegeState=" + profilePrivilegeState
				+ ", indUserType=" + indUserType + ", institutionType=" + institutionType + "]";
	}
	
	

	
}
