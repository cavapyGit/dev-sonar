package com.pradera.security.systemmgmt.view.validators;

import java.util.Locale;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.security.utils.view.PropertiesConstants;
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class SelectOneMenuValidator
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.1
 */
@FacesValidator("com.pradera.security.systemmgmt.view.validators.SelectOneMenuValidator")
public class SelectOneMenuValidator implements Validator{

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		if(value == null || (Integer.valueOf(value.toString()) < 0) ) {
			SelectOneMenu combo = (SelectOneMenu) component;			
			Locale locale = context.getViewRoot().getLocale();
			Object[] parametros = {combo.getLabel() };
			String strMsg =  PropertiesUtilities.getMessage(locale,PropertiesConstants.ERROR_COMBO_SELECCIONE, parametros);
			FacesMessage msg = new FacesMessage(strMsg);
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}
	}
}