package com.pradera.security.systemmgmt.view.validators;

import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.utils.view.GeneralPropertiesConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.security.model.type.UserAccountDocumentType;

@FacesValidator("com.pradera.security.systemmgmt.view.validators.DocumentTypeValidator")
public class DocumentTypeValidator implements Validator {
	
	private String errorMessage;
	private boolean valid;

	public void validate(FacesContext context, UIComponent component, Object value) 
			throws ValidatorException { 
		
		JSFUtilities.hideGeneralDialogues();
		Integer documentType = (Integer) component.getAttributes().get("docType");
		if(Validations.validateIsNull(documentType) || Validations.validateIsNull(value)){
			return;
		}
		
		DocumentTypeValidator bean = null;
		if(((String)value).length()<4 || ((String)value).length()>15){
			bean = doDocumentTypeValidation(documentType, (String)value);
		}else{
			bean = new DocumentTypeValidator();
			bean.setValid(true);
		}
		
		if(!bean.isValid()){
			JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
			Locale locale = context.getViewRoot().getLocale();

			String facesMsg =  PropertiesUtilities.getMessage(GeneralPropertiesConstants.PROPERTYFILE,locale, 
					bean.getErrorMessage());
			
			FacesMessage msg = new FacesMessage(facesMsg);
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			JSFUtilities.showEndTransactionSamePageDialog();
			JSFUtilities.putViewMap("headerMessageView", PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT));
			JSFUtilities.putViewMap("bodyMessageView", facesMsg);
			
			throw new ValidatorException(msg);
		}

	}
	
	private DocumentTypeValidator doDocumentTypeValidation(Integer documentType, String documentNumberAsString){
		DocumentTypeValidator bean = new DocumentTypeValidator();
		bean.setValid(false);
		
		boolean matches = false;
		if (documentType.equals(UserAccountDocumentType.PAS.getCode()) || documentType.equals(UserAccountDocumentType.DIO.getCode()) 
				|| documentType.equals(UserAccountDocumentType.CDN.getCode()) || documentType.equals(UserAccountDocumentType.CIE.getCode()) ){
			matches = CommonsUtilities.matchAlphanumeric(documentNumberAsString);
			if(!matches){
				bean.setErrorMessage(GeneralPropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_ALPHANUMERIC);
				return bean;
			}
		}
		
		if (documentType.equals(UserAccountDocumentType.CI.getCode()) || documentType.equals(UserAccountDocumentType.RUC.getCode())){
			matches = CommonsUtilities.matchNumeric(documentNumberAsString);
			if(!matches){
				bean.setErrorMessage(GeneralPropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_NUMERIC);
				return bean;
			}
		}
		
		if(documentType.equals(UserAccountDocumentType.CI.getCode()) 
			|| documentType.equals(UserAccountDocumentType.RUC.getCode()) 
			|| documentType.equals(UserAccountDocumentType.DIO.getCode()) 
			|| documentType.equals(UserAccountDocumentType.CIE.getCode())){
			
			if(documentNumberAsString.length() == 15){
				if(documentType.equals(UserAccountDocumentType.CI.getCode()) && documentNumberAsString.substring(0, 2).compareTo("00") == 0){					
					bean.setErrorMessage(GeneralPropertiesConstants.ERROR_DOC_COUNT_ONE_ZERO_FIRST);
				}else if(documentType.equals(UserAccountDocumentType.DIO.getCode()) && documentNumberAsString.substring(0, 4).compareTo("0000") == 0){
					bean.setErrorMessage(GeneralPropertiesConstants.ERROR_DOC_COUNT_THREE_ZERO_FIRST);
				}else {
					bean.setValid(true);	
				}
			}else{
				if(Validations.validateIsNotNullAndNotEmpty(documentNumberAsString)){
					bean.setErrorMessage(GeneralPropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_6_15_DIGITS); // formato de CI no valido
				}	
			}			
			
		} else if (documentType.equals(UserAccountDocumentType.CDN.getCode())){			
			if(documentNumberAsString.length() == 21){
				bean.setValid(true);
			}else{
				bean.setErrorMessage(GeneralPropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_21_DIGITS); // formato de CI no valido
			}
			
		} else if (documentType.equals(UserAccountDocumentType.PAS.getCode())){
			if(documentNumberAsString.length() > 3 && documentNumberAsString.length() <= 15 ){
				if(documentType.equals(UserAccountDocumentType.PAS.getCode()) && documentNumberAsString.substring(0, 4).compareTo("0000") == 0){
					bean.setErrorMessage(GeneralPropertiesConstants.ERROR_DOC_COUNT_THREE_ZERO_VALID);
				}else {
					bean.setValid(true);	
				}
			}else{
				bean.setErrorMessage(GeneralPropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_6_15_DIGITS); 
			}
			
		} else {
			// TODO documento no soportado.
			bean.setErrorMessage(GeneralPropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_NOT_SUPPORTED);
		}
		
		return bean;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}
	
	

}
