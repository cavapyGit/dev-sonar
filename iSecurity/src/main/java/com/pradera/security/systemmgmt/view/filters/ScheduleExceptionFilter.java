package com.pradera.security.systemmgmt.view.filters;

import java.io.Serializable;
import java.util.Date;
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class ScheduleExceptionFilter
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.1
 */
public class ScheduleExceptionFilter implements Serializable{
	
	/** The Constant serialVersionUID. */
	
	private static final long serialVersionUID = 1L;
	private Integer scheduleExceptionType;
	private String description;
	private Integer scheduleExceptionState;
	private Integer scheduleExceptionSystem;
	private Date initialDate;
	private Date finalDate;
	private Date initialHour;
	private Date finalHour;
	private ScheduleExceptionDetailFilter scheduleDetailExceptionFilter; 
	private Date date;
	private Date hour; 
	private String descript;
		
	/**
	 * Constructor
	 */
	public ScheduleExceptionFilter() {
		super();
	}

	/**
	 * @return the scheduleExceptionType
	 */
	public Integer getScheduleExceptionType() {
		return scheduleExceptionType;
	}

	/**
	 * @param scheduleExceptionType the scheduleExceptionType to set
	 */
	public void setScheduleExceptionType(Integer scheduleExceptionType) {
		this.scheduleExceptionType = scheduleExceptionType;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description.toUpperCase();
	}

	/**
	 * @return the scheduleExceptionState
	 */
	public Integer getScheduleExceptionState() {
		return scheduleExceptionState;
	}

	/**
	 * @param scheduleExceptionState the scheduleExceptionState to set
	 */
	public void setScheduleExceptionState(Integer scheduleExceptionState) {
		this.scheduleExceptionState = scheduleExceptionState;
	}

	/**
	 * @return the scheduleExceptionSystem
	 */
	public Integer getScheduleExceptionSystem() {
		return scheduleExceptionSystem;
	}

	/**
	 * @param scheduleExceptionSystem the scheduleExceptionSystem to set
	 */
	public void setScheduleExceptionSystem(Integer scheduleExceptionSystem) {
		this.scheduleExceptionSystem = scheduleExceptionSystem;
	}

	/**
	 * @return the initialDate
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * @param initialDate the initialDate to set
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * @return the finalDate
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * @param finalDate the finalDate to set
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	/**
	 * @return the initialHour
	 */
	public Date getInitialHour() {
		return initialHour;
	}

	/**
	 * @param initialHour the initialHour to set
	 */
	public void setInitialHour(Date initialHour) {
		this.initialHour = initialHour;
	}

	/**
	 * @return the finalHour
	 */
	public Date getFinalHour() {
		return finalHour;
	}

	/**
	 * @param finalHour the finalHour to set
	 */
	public void setFinalHour(Date finalHour) {
		this.finalHour = finalHour;
	}

	/**
	 * @return the scheduleDetailExceptionFilter
	 */
	public ScheduleExceptionDetailFilter getScheduleDetailExceptionFilter() {
		return scheduleDetailExceptionFilter;
	}

	/**
	 * @param scheduleDetailExceptionFilter the scheduleDetailExceptionFilter to set
	 */
	public void setScheduleDetailExceptionFilter(
			ScheduleExceptionDetailFilter scheduleDetailExceptionFilter) {
		this.scheduleDetailExceptionFilter = scheduleDetailExceptionFilter;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the hour
	 */
	public Date getHour() {
		return hour;
	}

	/**
	 * @param hour the hour to set
	 */
	public void setHour(Date hour) {
		this.hour = hour;
	}

	/**
	 * @return the descript
	 */
	public String getDescript() {
		return descript;
	}

	/**
	 * @param descript the descript to set
	 */
	public void setDescript(String descript) {
		this.descript = descript;
	}
}