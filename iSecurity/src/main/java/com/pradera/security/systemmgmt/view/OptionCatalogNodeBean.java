package com.pradera.security.systemmgmt.view;

import java.io.Serializable;
import java.util.Date;

import com.pradera.security.model.type.InstanceType;
import com.pradera.security.model.type.OptionSituationType;
import com.pradera.security.model.type.OptionStateType;
import com.pradera.security.model.type.SystemSituationType;
import com.pradera.security.model.type.SystemStateType;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class OptionCatalogNodeBean
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public class OptionCatalogNodeBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 708260456688172344L;

	/** The id system option pk. */
	private Integer idSystemOptionPk;
	
	/** The id system root. */
	private Integer idSystemRoot;
	
	/** The state. */
	private Integer state;
	
	/** The name. */
	private String name;

	/** The option url. */
	private String optionUrl;
	
	/** The instance type. */
	private Integer instanceType;
	
	/** The option type. */
	private Integer optionType;	
	
	/** The id system pk. */
	private Integer idSystemPk;
	
	/** The order option. */
	private Integer orderOption;
	
	/** The last modify date. */
	private Date lastModifyDate;
	
	/** The situation. */
	private Integer situation;
	
	
	/**
	 * Gets the order option.
	 *
	 * @return the orderOption
	 */
	public Integer getOrderOption() {
		return orderOption;
	}


	/**
	 * Sets the order option.
	 *
	 * @param orderOption the orderOption to set
	 */
	public void setOrderOption(Integer orderOption) {
		this.orderOption = orderOption;
	}


	/**
	 * Gets the id system pk.
	 *
	 * @return the idSystemPk
	 */
	public Integer getIdSystemPk() {
		return idSystemPk;
	}


	/**
	 * Sets the id system pk.
	 *
	 * @param idSystemPk the idSystemPk to set
	 */
	public void setIdSystemPk(Integer idSystemPk) {
		this.idSystemPk = idSystemPk;
	}


	/**
	 * Instantiates a new option catalog node bean.
	 */
	public OptionCatalogNodeBean() {
		// TODO Auto-generated constructor stub
	}
	

	 /**
 	 * Gets the state icon.
 	 *
 	 * @return icon
 	 */
	public String getStateIcon() {
			String icon = null;
			if (state != null) {
				if(InstanceType.OPTION.getCode().equals(instanceType)){
				icon = OptionStateType.get(state).getIcon();
				}
				if(InstanceType.SYSTEM.getCode().equals(instanceType)){
			    icon= SystemStateType.get(state).getIcon();	
				}
				
				
			}
			return icon;
    }
	 
	 /**
 	 * Gets the description state option.
 	 *
 	 * @return description
 	 */
	public String getDescriptionStateOption() {
			String description = null;
			if (state != null) {
				if(InstanceType.OPTION.getCode().equals(instanceType)){
				description = OptionStateType.get(state).getValue();
				}
				if(InstanceType.SYSTEM.getCode().equals(instanceType)){				    
				description = SystemStateType.get(state).getValue();
				}
				
			}
			return description;
	 }


	/**
	 * Gets the description situation option.
	 *
	 * @return the description situation option
	 */
	public String getDescriptionSituationOption() {
			String description = null;
			if (situation != null) {
				if(InstanceType.OPTION.getCode().equals(instanceType)){
					description = OptionSituationType.get(situation).getValue();
				}
				if(InstanceType.SYSTEM.getCode().equals(instanceType)){				    
					description = SystemSituationType.get(situation).getValue();
				}
				
			}
		return description;
	 }

	/**
	 * Gets the id system root.
	 *
	 * @return idSystemRoot
	 */
	public Integer getIdSystemRoot() {
		return idSystemRoot;
	}


	/**
	 * Sets the id system root.
	 *
	 * @param idSystemRoot the new id system root
	 */
	public void setIdSystemRoot(Integer idSystemRoot) {
		this.idSystemRoot = idSystemRoot;
	}


	/**
	 * Gets the id option root.
	 *
	 * @return idSystemRoot
	 */
	public Integer getIdOptionRoot() {
		return idSystemRoot;
	}

	/**
	 * Sets the id option root.
	 *
	 * @param idSystemRoot the new id option root
	 */
	public void setIdOptionRoot(Integer idSystemRoot) {
		this.idSystemRoot = idSystemRoot;
	}

	/**
	 * Gets the name.
	 *
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the state.
	 *
	 * @return state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the option url.
	 *
	 * @return optionUrl
	 */
	public String getOptionUrl() {
		return optionUrl;
	}

	/**
	 * Sets the option url.
	 *
	 * @param optionUrl the new option url
	 */
	public void setOptionUrl(String optionUrl) {
		this.optionUrl = optionUrl;
	}

	/**
	 * Gets the instance type.
	 *
	 * @return instanceType
	 */
	public Integer getInstanceType() {
		return instanceType;
	}

	/**
	 * Sets the instance type.
	 *
	 * @param instanceType the new instance type
	 */
	public void setInstanceType(Integer instanceType) {
		this.instanceType = instanceType;
	}

	/**
	 * Gets the option type.
	 *
	 * @return optionType
	 */
	public Integer getOptionType() {
		return optionType;
	}

	/**
	 * Sets the option type.
	 *
	 * @param optionType the new option type
	 */
	public void setOptionType(Integer optionType) {
		this.optionType = optionType;
	}


	/**
	 * Gets the id system option pk.
	 *
	 * @return idSystemOptionPk
	 */
	public Integer getIdSystemOptionPk() {
		return idSystemOptionPk;
	}


	/**
	 * Sets the id system option pk.
	 *
	 * @param idSystemOptionPk the new id system option pk
	 */
	public void setIdSystemOptionPk(Integer idSystemOptionPk) {
		this.idSystemOptionPk = idSystemOptionPk;
	}


	/**
	 * Gets the last modify date.
	 *
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}


	/**
	 * Gets the situation.
	 *
	 * @return the situation
	 */
	public Integer getSituation() {
		return situation;
	}


	/**
	 * Sets the situation.
	 *
	 * @param situation the new situation
	 */
	public void setSituation(Integer situation) {
		this.situation = situation;
	}


	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

}
