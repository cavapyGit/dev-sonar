package com.pradera.security.systemmgmt.view;


import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.security.model.Parameter;



/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class ParameterDataModel
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.1
 */

public class ParameterDataModel extends ListDataModel<Parameter> implements Serializable,SelectableDataModel<Parameter>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3583921115183287284L;

	public ParameterDataModel(){
		
	}
	
	public ParameterDataModel(List<Parameter> data){
		super(data);
	}
	
	@Override
	public Parameter getRowData(String rowKey) {
		// TODO Auto-generated method stub
		List<Parameter> parametros=(List<Parameter>)getWrappedData();
        for(Parameter parameter : parametros) {  
        	
            if(String.valueOf(parameter.getIdParameterPk()).equals(rowKey))
                return parameter;  
        }  
		return null;
	}

	@Override
	public Object getRowKey(Parameter parameter) {
		// TODO Auto-generated method stub
		return parameter.getIdParameterPk();
	}
	
	

}
