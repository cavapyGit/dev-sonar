package com.pradera.security.systemmgmt.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import com.pradera.integration.exception.ServiceException;
import com.pradera.security.model.Holiday;
import com.pradera.security.model.type.HolidayStateType;
import com.pradera.security.systemmgmt.view.filters.HolidayFilter;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Class DiaFeriadoServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/11/2012
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class HolidayServiceBean extends CrudSecurityServiceBean{

	/**
	 * Listar feriados por estado.
	 *
	 * @param estado the estado
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Holiday> listHolidaysByStateServiceBean(Integer state) throws ServiceException{
		Map<String,Object> parameters = new HashMap<String, Object>();
		parameters.put("state", state);
    	return (List<Holiday>)findWithNamedQuery(Holiday.HOLIDAY_SEARCH_BY_STATE, parameters);
    }
	
	/**
	 * Actualizar estado dia feriado.
	 *
	 * @param diaFeriado the dia feriado
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int updateHolidayStateServiceBean(Holiday holiday) throws ServiceException {
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update Holiday HL ");	
		sbQuery.append(" Set HL.state = :statePrm, HL.lastModifyUser = :lastModifyUserPrm, ");
		sbQuery.append(" HL.lastModifyDate = :lastModifyDatePrm, HL.lastModifyIp = :lastModifyIpPrm, ");
		sbQuery.append(" HL.lastModifyPriv = :lastModifyPrivPrm, ");
		sbQuery.append(" HL.deleteMotive = :deleteMotivePrm, ");
		sbQuery.append(" HL.deleteOtherMotive = :deleteOtherMotivePrm ");
		sbQuery.append(" Where HL.idHolidayPk = :idHolidayPkPrm ");
		
		Query query = em.createQuery(sbQuery.toString());
					
		query.setParameter("statePrm",holiday.getState());
		query.setParameter("lastModifyUserPrm",holiday.getLastModifyUser());
		query.setParameter("lastModifyDatePrm",holiday.getLastModifyDate());
		query.setParameter("lastModifyIpPrm",holiday.getLastModifyIp());
		query.setParameter("lastModifyPrivPrm",holiday.getLastModifyPriv());
		query.setParameter("deleteMotivePrm",holiday.getDeleteMotive());
		query.setParameter("deleteOtherMotivePrm",holiday.getDeleteOtherMotive());
		query.setParameter("idHolidayPkPrm",holiday.getIdHolidayPk());
		
		return query.executeUpdate();

	}
	
	/**
	 * Obtener dia feriado por filtros.
	 *
	 * @param holidayFilter the dia feriado filtro
	 * @return the dia feriado
	 * @throws ServiceException the service exception
	 */
	public Holiday findHolidayByFiltersServiceBean(HolidayFilter holidayFilter) throws ServiceException {
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" Select H From Holiday H ");
			sbQuery.append(" Where H.state = :statePrm ");
			sbQuery.append(" And H.dateHoliday = :dateHolidayPrm ");
	
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("statePrm",holidayFilter.getHolidayState());
			query.setParameter("dateHolidayPrm",holidayFilter.getHolidayDate(), TemporalType.DATE);
			query.setHint("org.hibernate.cacheable", true);
			return (Holiday) query.getSingleResult();

		} catch(NoResultException ex){
			return null;
		} catch(NonUniqueResultException ex){
			return null;
		}
	}
	
	/**
	 * To find the day is already register as holiday 
	 * @param holiday
	 * @return record exist return 1
	 * @throws ServiceException
	 */
	 public int findHolidayExistence(Holiday holiday) throws ServiceException{
			StringBuilder sbQuery=new StringBuilder();
			sbQuery.append("Select Count(h) from Holiday h where h.dateHoliday=:dateHoliday and state = :state");
			Query query=em.createQuery(sbQuery.toString());
			query.setParameter("dateHoliday", holiday.getDateHoliday());
			query.setParameter("state", holiday.getState());
			query.setHint("org.hibernate.cacheable", true);
			return Integer.parseInt( query.getSingleResult().toString() );
	}

	/**
	 * Gets the list available holiday date.
	 *
	 * @return the list available holiday date
	 * @throws ServiceException the service exception
	 */
	public List<Date> getListAvailableHolidayDate() throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select H.dateHoliday From Holiday H ");
		sbQuery.append(" Where H.state = :statePrm ");	
		
		TypedQuery<Date> query = em.createQuery(sbQuery.toString(),Date.class);
		query.setParameter("statePrm", HolidayStateType.REGISTERED.getCode());
		//query.setHint("org.hibernate.cacheable", true);
		
		return query.getResultList();
	}
	
}
