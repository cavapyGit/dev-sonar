package com.pradera.security.systemmgmt.service;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.TransactionSynchronizationRegistry;

import org.hibernate.Session;

import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.security.entity.service.LoaderEntityServiceBean;
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class CrudSecurityServiceBean
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.1
 */
/**
 * @author Administrator
 *
 */
/**
 * @author Administrator
 *
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class CrudSecurityServiceBean implements Serializable
{
	@PersistenceContext(unitName="securityDS")
	protected EntityManager em;
	
	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The loader entity service. */
	@EJB
	LoaderEntityServiceBean loaderEntityService;
	
	/**
	 * @return the em
	 */
	public EntityManager getEm() {
		return em;
	}

	/**
	 * @param em the em to set
	 */
	public void setEm(EntityManager em) {
		this.em = em;
	}

	public <T> T create(T t) {
		Object object = transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		return create(t, object);
	}
	
	/**
	 * Creates the entity.
	 *
	 * @param <T> the generic type
	 * @param t the t
	 * @return the t
	 */
	public <T> T create(T t, Object object) {
		if (object instanceof LoggerUser) {
			if (t instanceof Auditable) {
				LoggerUser loggerUser = (LoggerUser)object;
				Auditable auditable = (Auditable)t;
				setAuditFields(auditable,loggerUser);
//				auditable.setAudit(loggerUser);
//				Map<String,List<? extends Auditable>> listAudit = auditable.getListForAudit();
//				if (listAudit != null) {
//					for(List<? extends Auditable> auditableList :listAudit.values()) {
//						loaderEntityService.setAudit(auditableList, loggerUser);
//					}
//				}
			}
		}
		this.em.persist(t);
		return t;
	}
	
	/**
	 * Sets the audit fields.
	 *
	 * @param auditable the auditable
	 * @param loggerUser the logger user
	 */
	private void setAuditFields(Auditable auditable,LoggerUser loggerUser) {
		if(auditable != null) {
			auditable.setAudit(loggerUser);
			Map<String,List<? extends Auditable>> listAudit = auditable.getListForAudit();
			if (listAudit != null) {
				//if have list for audit
				for(List<? extends Auditable> auditableList :listAudit.values()) {
					if (auditableList != null && loaderEntityService.isLoadedList(auditableList)){
						//if list is not null
						for (Auditable childrenAudit : auditableList) {
							if(childrenAudit == auditable){
								break;
							}
							setAuditFields(childrenAudit,loggerUser);
						}
					}
				}
			}
		}
	}
	

	public <T> T createOnCascade(T t) {
		Object object = transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		return createOnCascade(t, object);
	}
	
	
	/**
	 * Creates the entity.
	 *
	 * @param <T> the generic type
	 * @param t the t
	 * @return the t
	 */
	public <T> T createOnCascade(T t, Object object) {
		if (object instanceof LoggerUser) {
			if (t instanceof Auditable) {
				LoggerUser loggerUser = (LoggerUser)object;
				Auditable auditable = (Auditable)t;
				setAuditFields(auditable,loggerUser);
			}
		}
		this.em.persist(t);
		this.em.flush();
		return t;
	}
	

	public <T> T find(Class<T> type, Object id) {
		return (T) this.em.find(type, id);
	}

	public void delete(Class type, Object id) {
		Object ref = this.em.getReference(type, id);
		this.em.remove(ref);
	}

	public <T> T update(T t) throws ServiceException{
		Object object = transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		return update(t, object);
	}
	
	/**
	 * Update.
	 *
	 * @param <T> the generic type
	 * @param t the t
	 * @return the t
	 * @throws ServiceException the service exception
	 */
	public <T> T update(T t, Object object) throws ServiceException{
		if (object instanceof LoggerUser) {
			if (t instanceof Auditable) {
				LoggerUser loggerUser = (LoggerUser)object;
				Auditable auditable = (Auditable)t;
				setAuditFields(auditable,loggerUser);
			}
		}
		T mergeEntity = null;
		try {
		mergeEntity = this.em.merge(t);
		this.em.flush();
		} catch(OptimisticLockException oex) {
			throw new ServiceException(ErrorServiceType.CONCURRENCY_LOCK_UPDATE, oex);
		}
		return mergeEntity;
	}
	

	@SuppressWarnings("rawtypes")
	public List findWithNamedQuery(String namedQueryName) {
		return this.em.createNamedQuery(namedQueryName).getResultList();
	}

	@SuppressWarnings("rawtypes")
	public List findWithNamedQuery(String namedQueryName,
			Map<String, Object> parameters) {
		return findWithNamedQuery(namedQueryName, parameters, 0);
	}

	@SuppressWarnings("rawtypes")
	public List findWithNamedQuery(String queryName, int resultLimit) {
		return this.em.createNamedQuery(queryName).setMaxResults(resultLimit)
				.getResultList();
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> findByNativeQuery(String sql, Class<T> type) {
		return this.em.createNativeQuery(sql, type).getResultList();
	}

	@SuppressWarnings("unchecked")
	public List findWithNamedQuery(String namedQueryName, Map<String, Object> parameters, int resultLimit) {
		Set<Entry<String, Object>> rawParameters = parameters.entrySet();
		Query query = this.em.createNamedQuery(namedQueryName);
		if (resultLimit > 0)
			query.setMaxResults(resultLimit);
		for (Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		query.setHint("org.hibernate.cacheable", true);
		return (List<Object>) query.getResultList();
	}

	public <T> T find(Object id, Class<T> type) {
		return (T) this.em.find(type, id);
	}

	public <T> T updateOnCascade(T t) throws ServiceException{
		Object object = transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		return updateOnCascade(t, object);
	}
	
	/**
	 * Update.
	 *
	 * @param <T> the generic type
	 * @param t the t
	 * @return the t
	 * @throws ServiceException the service exception
	 */
	public <T> T updateOnCascade(T t, Object object) throws ServiceException{
		if (object instanceof LoggerUser) {
			if (t instanceof Auditable) {
				LoggerUser loggerUser = (LoggerUser)object;
				Auditable auditable = (Auditable)t;
				setAuditFields(auditable,loggerUser);
			}
		}
		T mergeEntity = null;
		try {
		mergeEntity = this.em.merge(t);
		this.em.flush();
		} catch(OptimisticLockException oex) {
			throw new ServiceException(ErrorServiceType.CONCURRENCY_LOCK_UPDATE, oex);
		}
		return mergeEntity;
	}
	
	public List findByQueryString(String sql) {
		return this.em.createQuery(sql).getResultList();
    }
	public List findByQueryString(String sql,Map<String, Object> parameters) {
        Set<Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = this.em.createQuery(sql);
        for (Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        query.setHint("org.hibernate.cacheable", true);
        return query.getResultList();
    }

	
	public Object findWithNamedQry(String namedQueryName,Map<String, Object> parameters) {
		 Set<Entry<String, Object>> rawParameters = parameters.entrySet();
	        Query query = this.em.createNamedQuery(namedQueryName);
	        for (Entry<String, Object> entry : rawParameters) {
	            query.setParameter(entry.getKey(), entry.getValue());
	        }
	        query.setHint("org.hibernate.cacheable", true);	
	        return query.getSingleResult();
	}
	public List findByNativeQuery(String strSqlquery) {
		Query query = this.em.createNativeQuery(strSqlquery);
		//query.setHint("org.hibernate.cacheable", true);
		return query.getResultList();
	}

	
	public Object findByNativeQueryForObject(String strSqlquery) {
		Query query = this.em.createNativeQuery(strSqlquery);
		try{
			query.setHint("org.hibernate.cacheable", true);
			return query.getSingleResult();
		}catch (Exception e) {
			return null;
		}
	}
	/**
	 * @param query
	 * @param Fk
	 */
	public void deleteByFk(String query,long Fk){
		 Query deleteQuery = this.em.createQuery(query);
		 deleteQuery.setParameter(1,Fk);
		 deleteQuery.executeUpdate();
	}
	/**
	 * Used to get Unique Object.
	 *
	 * @param sql String
	 * @param parameters Map<String,Object>
	 * @return Object
	 */
	public Object findObjectByQuery(String sql,Map<String, Object> parameters) {
		List<Object> list = new ArrayList<Object>();
		Set<Entry<String, Object>> rawParameters = parameters.entrySet();
		Query query = this.em.createQuery(sql);
		for (Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		query.setHint("org.hibernate.cacheable", true);
		list = query.getResultList();
		if(list.size() > 0)
			return list.get(0);
		else
			return null;
    } 
	
	/**
	 * @param sql
	 * @return
	 * @throws SQLException 
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void executeStatement(String sql) {
		  try {
	            // Get the underlying Hibernate Session from EntityManager
	            Session session = em.unwrap(Session.class);

	            // Use the Work interface to execute the SQL with a JDBC connection
	            session.doWork(connection -> {
	                try (java.sql.Statement statement = connection.createStatement()) {
	                    statement.executeUpdate(sql);
	                } catch (Exception e) {
	                    e.printStackTrace();
	                }
	            });
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
        
	}

}