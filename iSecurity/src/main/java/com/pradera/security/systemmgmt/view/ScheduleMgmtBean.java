package com.pradera.security.systemmgmt.view;	

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.persistence.Table;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.DayType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.model.HistoricalState;
import com.pradera.security.model.InstitutionsSecurity;
import com.pradera.security.model.Parameter;
import com.pradera.security.model.Schedule;
import com.pradera.security.model.ScheduleDetail;
import com.pradera.security.model.System;
import com.pradera.security.model.type.DefinitionType;
import com.pradera.security.model.type.OtherMotiveType;
import com.pradera.security.model.type.ParameterTableType;
import com.pradera.security.model.type.ScheduleDetailStateType;
import com.pradera.security.model.type.ScheduleSituationType;
import com.pradera.security.model.type.ScheduleStateType;
import com.pradera.security.model.type.ScheduleType;
import com.pradera.security.model.type.SystemProfileSituationType;
import com.pradera.security.model.type.SystemProfileStateType;
import com.pradera.security.model.type.UserAccountType;
import com.pradera.security.systemmgmt.facade.SystemServiceFacade;
import com.pradera.security.systemmgmt.view.filters.InstitutionSecurityFilter;
import com.pradera.security.systemmgmt.view.filters.ScheduleFilter;
import com.pradera.security.usermgmt.facade.UserServiceFacade;
import com.pradera.security.utils.view.GenericBaseBean;
import com.pradera.security.utils.view.PropertiesConstants;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class ScheduleMgmtBean
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
@DepositaryWebBean
public class ScheduleMgmtBean extends GenericBaseBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The user service facade. */
	@EJB
	UserServiceFacade userServiceFacade;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	@Inject
	UserPrivilege userPrivilege;
	
	/** The schedule filter. */
	private ScheduleFilter scheduleFilter;
	
	/** The Search Schedule filter. */
	private ScheduleFilter searchScheduleFilter;
	
	/** The schedule search. */
	private List<Schedule> scheduleSearch;

	/** The schedule session. */
	private Schedule scheduleSession;
	
	/** The schedule session. */
	private Schedule selectedScheduleSession;
	
	/** The schedule detail data model. */
	private GenericDataModel<ScheduleDetail> scheduleDetailDataModel;
		
	/** The institutions type list. */
	private List<Parameter> institutionsTypeList;
	
	/** The institutions type list of search page. */
	private List<Parameter> searchInstitutionsTypeList;
	
	/** The institutions type temporary list. */
	private List<Parameter> institutionsTypeListTemp;
	
	/** The user account type list. */
	private List<Parameter> userAccountTypeList;
	
	/** The Search page user account type list. */
	private List<Parameter> searchUserAccountTypeList;
	
	/** The user account type list temp. */
	private List<Parameter> userAccountTypeListTemp;
	
	/** The institution security list. */
	private List<InstitutionsSecurity> institutionSecurityList;
	
	/** The institution secururity list selected. */
	private List<InstitutionsSecurity> institutionSecururityListSelected;
	
	/** The user account type selected. */
	private Integer userAccountTypeSelected;
	
	/** The Search user account type selected. */
	private Integer searchUserAccountTypeSelected;
	
	/** The institution security selected. */
	private Integer institutionSecuritySelected;
	
	/** The last schedule cheked. */
	private ScheduleDetail lastScheduleCheked;
	
	/** The schedule data model. */
	private GenericDataModel<Schedule> scheduleDataModel;
	
	/** The historical state. */
	private HistoricalState historicalState;
	
	/** The initial hour. */
	private Date initialHour;
	
	/** The final hour. */
	private Date finalHour;
	
	/** The show other motive. */
	private boolean showOtherMotive;
	
	/** The system selected. */
	private Integer systemSelected;
	
	/** The initial hour. */
	private Date tabInitialHour;
	
	/** The final hour. */
	private Date tabFinalHour;
	
	/** The log. */
    Logger log = Logger.getLogger(ScheduleMgmtBean.class.getName());
	
	/** The schedule detail list. */
	private List<ScheduleDetail> scheduleDetailList ;
	
	/** The is confirm block or unblock. */
	private boolean isConfirmBlockOrUnblock;
	
	/** The schedule detail mon. */
	private ScheduleDetail scheduleDetailMon;
	
	/** The schedule detail tue. */
	private ScheduleDetail scheduleDetailTue;
	
	/** The schedule detail wed. */
	private ScheduleDetail scheduleDetailWed;
	
	/** The schedule detail thu. */
	private ScheduleDetail scheduleDetailThu;
	
	/** The schedule detail fri. */
	private ScheduleDetail scheduleDetailFri;
	
	/** The schedule detail sat. */
	private ScheduleDetail scheduleDetailSat;
	
	/** The schedule detail sun. */
	private ScheduleDetail scheduleDetailSun;
	
	/** The select all. */
	private boolean selectAll = true;
	
	/** The cevaldom institution. */
	//private boolean cevaldomInstitution = false;
	
	private boolean isRejectOption;
	
	/**
	 * Instantiates a new schedule mgmt bean.
	 */
	public ScheduleMgmtBean() {
		scheduleFilter = new ScheduleFilter();
		searchScheduleFilter = new ScheduleFilter();
		scheduleSearch = new ArrayList<Schedule>();//na
		scheduleSession = new Schedule();//na
		selectedScheduleSession = new Schedule();//na
		historicalState=new HistoricalState();
		historicalState.setMotive(Integer.valueOf(-1));
	
	}
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		try{
			beginConversation();
			institutionsTypeList = new ArrayList<Parameter>();
			userAccountTypeList = new ArrayList<Parameter>();
			searchUserAccountTypeList = new ArrayList<Parameter>();
			searchInstitutionsTypeList = new ArrayList<Parameter>();
			userAccountTypeListTemp = userServiceFacade.listClientTypeByDefinitionServiceFacade(ParameterTableType.USER_CODE.getCode());
			
			institutionsTypeListTemp = userServiceFacade.listClientTypeByDefinitionServiceFacade(ParameterTableType.INSITUTIONS_STATE.getCode());
			
			//To load the block and unblock motives for Schedule management based on the definition table FK value in the parameter table
			lstBlockMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.SYS_SCHEDULE_BLOCK_MOTIVES.getCode());
			lstUnBlockMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.SYS_SCHEDULE_DESBLOCK_MOTIVES.getCode());
			lstRejectMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.SYS_SCHEDULE_REJECT_MOTIVES.getCode());
			//setting privilege
			PrivilegeComponent privilegeComp=new PrivilegeComponent();
			privilegeComp.setBtnModifyView(true);
			privilegeComp.setBtnConfirmView(true);
			privilegeComp.setBtnRejectView(true);
			privilegeComp.setBtnBlockView(true);
			privilegeComp.setBtnUnblockView(true);
			userPrivilege.setPrivilegeComponent(privilegeComp);
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Pre destroy.
	 */
	@PreDestroy
	public void preDestroy(){
		endConversation();
		scheduleDetailDataModel = null;
		scheduleDetailList = null;
		institutionSecurityList = null;
		institutionSecururityListSelected = null;
		institutionsTypeList = null;
		scheduleSearch = null;
	}


	
	
	/**
	 * Gets the historical state.
	 *
	 * @return the historical state
	 */
	public HistoricalState getHistoricalState() {
		return historicalState;
	}

	/**
	 * Sets the historical state.
	 *
	 * @param historicalState the new historical state
	 */
	public void setHistoricalState(HistoricalState historicalState) {
		this.historicalState = historicalState;
	}

	/**
	 * Checks if is show other motive.
	 *
	 * @return true, if is show other motive
	 */
	public boolean isShowOtherMotive() {
		return showOtherMotive;
	}

	/**
	 * Sets the show other motive.
	 *
	 * @param showOtherMotive the new show other motive
	 */
	public void setShowOtherMotive(boolean showOtherMotive) {
		this.showOtherMotive = showOtherMotive;
	}

	/**
	 * Gets the schedule type list.
	 *
	 * @return the schedule type list
	 */
	public List<ScheduleType> getScheduleTypeList(){
		return ScheduleType.list;
	}
	
	/**
	 * Gets the schedule state type list.
	 *
	 * @return the schedule state type list
	 */
	public List<ScheduleStateType> getScheduleStateTypeList(){
		return ScheduleStateType.list;
	}
	
	/**
	 * Gets the system service facade.
	 *
	 * @return the system service facade
	 */
	public SystemServiceFacade getSystemServiceFacade() {
		return systemServiceFacade;
	}

	/**
	 * Sets the system service facade.
	 *
	 * @param systemServiceFacade the new system service facade
	 */
	public void setSystemServiceFacade(SystemServiceFacade systemServiceFacade) {
		this.systemServiceFacade = systemServiceFacade;
	}

	/**
	 * Gets the schedule filter.
	 *
	 * @return the schedule filter
	 */
	public ScheduleFilter getScheduleFilter() {
		return scheduleFilter;
	}

	/**
	 * Sets the schedule filter.
	 *
	 * @param scheduleFilter the new schedule filter
	 */
	public void setScheduleFilter(ScheduleFilter scheduleFilter) {
		this.scheduleFilter = scheduleFilter;
	}
	
	

	/**
	 * Gets the search schedule filter.
	 *
	 * @return the searchScheduleFilter
	 */
	public ScheduleFilter getSearchScheduleFilter() {
		return searchScheduleFilter;
	}

	/**
	 * Sets the search schedule filter.
	 *
	 * @param searchScheduleFilter the searchScheduleFilter to set
	 */
	public void setSearchScheduleFilter(ScheduleFilter searchScheduleFilter) {
		this.searchScheduleFilter = searchScheduleFilter;
	}

	/**
	 * Gets the schedule search.
	 *
	 * @return the schedule search
	 */
	public List<Schedule> getScheduleSearch() {
		return scheduleSearch;
	}

	/**
	 * Sets the schedule search.
	 *
	 * @param scheduleSearch the new schedule search
	 */
	public void setScheduleSearch(List<Schedule> scheduleSearch) {
		this.scheduleSearch = scheduleSearch;
	}

	/**
	 * Gets the schedule session.
	 *
	 * @return the schedule session
	 */
	public Schedule getScheduleSession() {
		return scheduleSession;
	}

	/**
	 * Sets the schedule session.
	 *
	 * @param scheduleSession the new schedule session
	 */
	public void setScheduleSession(Schedule scheduleSession) {
		this.scheduleSession = scheduleSession;
	}

	/**
	 * Gets the selected schedule session.
	 *
	 * @return the selected schedule session
	 */
	public Schedule getSelectedScheduleSession() {
		return selectedScheduleSession;
	}

	/**
	 * Sets the selected schedule session.
	 *
	 * @param selectedScheduleSession the new selected schedule session
	 */
	public void setSelectedScheduleSession(Schedule selectedScheduleSession) {
		this.selectedScheduleSession = selectedScheduleSession;
	}
	
	/**
	 * Gets the schedule detail data model.
	 *
	 * @return the schedule detail data model
	 */
	public GenericDataModel<ScheduleDetail> getScheduleDetailDataModel() {
		return scheduleDetailDataModel;
	}

	/**
	 * Sets the schedule detail data model.
	 *
	 * @param scheduleDetailDataModel the new schedule detail data model
	 */
	public void setScheduleDetailDataModel(
			GenericDataModel<ScheduleDetail> scheduleDetailDataModel) {
		this.scheduleDetailDataModel = scheduleDetailDataModel;
	}

	/**
	 * Gets the institutions type list.
	 *
	 * @return the institutions type list
	 */
	public List<Parameter> getInstitutionsTypeList() {
		return institutionsTypeList;
	}

	/**
	 * Sets the institutions type list.
	 *
	 * @param institutionsTypeList the new institutions type list
	 */
	public void setInstitutionsTypeList(List<Parameter> institutionsTypeList) {
		this.institutionsTypeList = institutionsTypeList;
	}
	

	/**
	 * Gets the search institutions type list.
	 *
	 * @return the searchInstitutionsTypeList
	 */
	public List<Parameter> getSearchInstitutionsTypeList() {
		return searchInstitutionsTypeList;
	}

	/**
	 * Sets the search institutions type list.
	 *
	 * @param searchInstitutionsTypeList the searchInstitutionsTypeList to set
	 */
	public void setSearchInstitutionsTypeList(
			List<Parameter> searchInstitutionsTypeList) {
		this.searchInstitutionsTypeList = searchInstitutionsTypeList;
	}

	/**
	 * Gets the institutions type list temp.
	 *
	 * @return the institutionsTypeListTemp
	 */
	public List<Parameter> getInstitutionsTypeListTemp() {
		return institutionsTypeListTemp;
	}

	/**
	 * Sets the institutions type list temp.
	 *
	 * @param institutionsTypeListTemp the institutionsTypeListTemp to set
	 */
	public void setInstitutionsTypeListTemp(
			List<Parameter> institutionsTypeListTemp) {
		this.institutionsTypeListTemp = institutionsTypeListTemp;
	}

	/**
	 * Gets the user account type list.
	 *
	 * @return the user account type list
	 */
	public List<Parameter> getUserAccountTypeList() {
		return userAccountTypeList;
	}

	/**
	 * Sets the user account type list.
	 *
	 * @param userAccountTypeList the new user account type list
	 */
	public void setUserAccountTypeList(List<Parameter> userAccountTypeList) {
		this.userAccountTypeList = userAccountTypeList;
	}	
	

	/**
	 * Gets the search user account type list.
	 *
	 * @return the searchUserAccountTypeList
	 */
	public List<Parameter> getSearchUserAccountTypeList() {
		return searchUserAccountTypeList;
	}

	/**
	 * Sets the search user account type list.
	 *
	 * @param searchUserAccountTypeList the searchUserAccountTypeList to set
	 */
	public void setSearchUserAccountTypeList(
			List<Parameter> searchUserAccountTypeList) {
		this.searchUserAccountTypeList = searchUserAccountTypeList;
	}

	/**
	 * Gets the user account type list temp.
	 *
	 * @return the userAccountTypeListTemp
	 */
	public List<Parameter> getUserAccountTypeListTemp() {
		return userAccountTypeListTemp;
	}

	/**
	 * Sets the user account type list temp.
	 *
	 * @param userAccountTypeListTemp the userAccountTypeListTemp to set
	 */
	public void setUserAccountTypeListTemp(List<Parameter> userAccountTypeListTemp) {
		this.userAccountTypeListTemp = userAccountTypeListTemp;
	}

	/**
	 * Gets the institution security list.
	 *
	 * @return the institution security list
	 */
	public List<InstitutionsSecurity> getInstitutionSecurityList() {
		return institutionSecurityList;
	}

	/**
	 * Sets the institution security list.
	 *
	 * @param institutionSecurityList the new institution security list
	 */
	public void setInstitutionSecurityList(
			List<InstitutionsSecurity> institutionSecurityList) {
		this.institutionSecurityList = institutionSecurityList;
	}

	/**
	 * Gets the institution secururity list selected.
	 *
	 * @return the institution secururity list selected
	 */
	public List<InstitutionsSecurity> getInstitutionSecururityListSelected() {
		return institutionSecururityListSelected;
	}

	/**
	 * Sets the institution secururity list selected.
	 *
	 * @param institutionSecururityListSelected the new institution secururity list selected
	 */
	public void setInstitutionSecururityListSelected(
			List<InstitutionsSecurity> institutionSecururityListSelected) {
		this.institutionSecururityListSelected = institutionSecururityListSelected;
	}

	/**
	 * Gets the user account type selected.
	 *
	 * @return the user account type selected
	 */
	public Integer getUserAccountTypeSelected() {
		return userAccountTypeSelected;
	}

	/**
	 * Sets the user account type selected.
	 *
	 * @param userAccountTypeSelected the new user account type selected
	 */
	public void setUserAccountTypeSelected(Integer userAccountTypeSelected) {
		this.userAccountTypeSelected = userAccountTypeSelected;
	}	

	/**
	 * Gets the search user account type selected.
	 *
	 * @return the searchUserAccountTypeSelected
	 */
	public Integer getSearchUserAccountTypeSelected() {
		return searchUserAccountTypeSelected;
	}

	/**
	 * Sets the search user account type selected.
	 *
	 * @param searchUserAccountTypeSelected the searchUserAccountTypeSelected to set
	 */
	public void setSearchUserAccountTypeSelected(
			Integer searchUserAccountTypeSelected) {
		this.searchUserAccountTypeSelected = searchUserAccountTypeSelected;
	}

	/**
	 * Gets the institution security selected.
	 *
	 * @return the institution security selected
	 */
	public Integer getInstitutionSecuritySelected() {
		return institutionSecuritySelected;
	}

	/**
	 * Sets the institution security selected.
	 *
	 * @param institutionSecuritySelected the new institution security selected
	 */
	public void setInstitutionSecuritySelected(Integer institutionSecuritySelected) {
		this.institutionSecuritySelected = institutionSecuritySelected;
	}

	/**
	 * Gets the last schedule cheked.
	 *
	 * @return the last schedule cheked
	 */
	public ScheduleDetail getLastScheduleCheked() {
		return lastScheduleCheked;
	}

	/**
	 * Sets the last schedule cheked.
	 *
	 * @param lastScheduleCheked the new last schedule cheked
	 */
	public void setLastScheduleCheked(ScheduleDetail lastScheduleCheked) {
		this.lastScheduleCheked = lastScheduleCheked;
	}

	/**
	 * Gets the schedule data model.
	 *
	 * @return the schedule data model
	 */
	public GenericDataModel<Schedule> getScheduleDataModel() {
		return scheduleDataModel;
	}

	/**
	 * Sets the schedule data model.
	 *
	 * @param scheduleDataModel the new schedule data model
	 */
	public void setScheduleDataModel(GenericDataModel<Schedule> scheduleDataModel) {
		this.scheduleDataModel = scheduleDataModel;
	}

	/**
	 * Load components.
	 */
	private void loadComponents(){
		institutionsTypeList = new ArrayList<Parameter>();
		institutionSecurityList = new ArrayList<InstitutionsSecurity>();
		lastScheduleCheked = new ScheduleDetail();
		this.institutionSecururityListSelected = new ArrayList<InstitutionsSecurity>();
		scheduleDetailList = new ArrayList<ScheduleDetail>();
		scheduleDetailMon = new ScheduleDetail();
		scheduleDetailTue = new ScheduleDetail();
		scheduleDetailWed = new ScheduleDetail();
		scheduleDetailThu = new ScheduleDetail();
		scheduleDetailFri = new ScheduleDetail();
		scheduleDetailSat = new ScheduleDetail();
		scheduleDetailSun = new ScheduleDetail();
		
		if(this.isRegister()){
			scheduleSession = new Schedule();
			for(DayType dayTipe: DayType.list){
				if(dayTipe.getCode().compareTo(Calendar.MONDAY)==0){
					scheduleDetailMon = new ScheduleDetail();
					scheduleDetailMon.setDay(Calendar.MONDAY);
					scheduleDetailList.add(scheduleDetailMon);
				}else
					if(dayTipe.getCode().compareTo(Calendar.TUESDAY)==0){
						scheduleDetailTue = new ScheduleDetail();
						scheduleDetailTue.setDay(Calendar.TUESDAY);
						scheduleDetailList.add(scheduleDetailTue);
					}else
						if(dayTipe.getCode().compareTo(Calendar.WEDNESDAY)==0){
							scheduleDetailWed = new ScheduleDetail();
							scheduleDetailWed.setDay(Calendar.WEDNESDAY);
							scheduleDetailList.add(scheduleDetailWed);
						}else
							if(dayTipe.getCode().compareTo(Calendar.THURSDAY)==0){
								scheduleDetailThu = new ScheduleDetail();
								scheduleDetailThu.setDay(Calendar.THURSDAY);
								scheduleDetailList.add(scheduleDetailThu);
							}else
								if(dayTipe.getCode().compareTo(Calendar.FRIDAY)==0){
									scheduleDetailFri = new ScheduleDetail();
									scheduleDetailFri.setDay(Calendar.FRIDAY);
									scheduleDetailList.add(scheduleDetailFri);
								}else
									if(dayTipe.getCode().compareTo(Calendar.SATURDAY)==0){
										scheduleDetailSat = new ScheduleDetail();
										scheduleDetailSat.setDay(Calendar.SATURDAY);
										scheduleDetailList.add(scheduleDetailSat);
									}else
										if(dayTipe.getCode().compareTo(Calendar.SUNDAY)==0){
											scheduleDetailSun = new ScheduleDetail();
											scheduleDetailSun.setDay(Calendar.SUNDAY);
											scheduleDetailList.add(scheduleDetailSun);
										}
			}
			scheduleSession.setScheduleDetails(scheduleDetailList);
		}else {
			this.setInitialHour(null);
			this.setFinalHour(null);
			List<ScheduleDetail> scheduleDetailListModify = new ArrayList<ScheduleDetail>();
			try{
			scheduleDetailListModify = systemServiceFacade.getScheduleDetailServiceFacade(scheduleSession.getIdSchedulePk());
			}catch (Exception e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}

			for(int i=0;i<scheduleDetailListModify.size();i++){
				for(int j=0;j<scheduleDetailListModify.size();j++){	
					boolean flag = Boolean.FALSE;
					if((scheduleDetailListModify.get(i).getDay().compareTo(scheduleDetailListModify.get(j).getDay())==0) || scheduleDetailListModify.size()==1){
						if(((scheduleDetailListModify.get(i).getInitialDate().compareTo(scheduleDetailListModify.get(j).getInitialDate())==0) && (scheduleDetailListModify.get(i).getEndDate().compareTo(scheduleDetailListModify.get(j).getEndDate())==0 )) || scheduleDetailListModify.size()==1){		
							if(scheduleDetailListModify.get(i).getDay().compareTo(Calendar.MONDAY) ==0){ 
								flag = Boolean.TRUE;
								scheduleDetailMon = new ScheduleDetail();
								scheduleDetailMon.setDay(Calendar.MONDAY);
								scheduleDetailMon.setInitialDate(scheduleDetailListModify.get(i).getInitialDate());
								scheduleDetailMon.setEndDate(scheduleDetailListModify.get(i).getEndDate());
								scheduleDetailMon.setSelected(flag);
								scheduleDetailList.add(scheduleDetailMon);
							}else
								if(scheduleDetailListModify.get(i).getDay().compareTo(Calendar.TUESDAY)==0){
									flag = Boolean.TRUE;
									scheduleDetailTue = new ScheduleDetail();
									scheduleDetailTue.setDay(Calendar.TUESDAY);
									scheduleDetailTue.setInitialDate(scheduleDetailListModify.get(i).getInitialDate());
									scheduleDetailTue.setEndDate(scheduleDetailListModify.get(i).getEndDate());
									scheduleDetailTue.setSelected(flag);
									scheduleDetailList.add(scheduleDetailTue);
								}else
									if(scheduleDetailListModify.get(i).getDay().compareTo(Calendar.WEDNESDAY)==0){
										flag = Boolean.TRUE;
										scheduleDetailWed = new ScheduleDetail();
										scheduleDetailWed.setDay(Calendar.WEDNESDAY);
										scheduleDetailWed.setInitialDate(scheduleDetailListModify.get(i).getInitialDate());
										scheduleDetailWed.setEndDate(scheduleDetailListModify.get(i).getEndDate());
										scheduleDetailWed.setSelected(flag);
										scheduleDetailList.add(scheduleDetailWed);
									}else
										if(scheduleDetailListModify.get(i).getDay().compareTo(Calendar.THURSDAY)==0){
											flag = Boolean.TRUE;
											scheduleDetailThu = new ScheduleDetail();
											scheduleDetailThu.setDay(Calendar.THURSDAY);
											scheduleDetailThu.setInitialDate(scheduleDetailListModify.get(i).getInitialDate());
											scheduleDetailThu.setEndDate(scheduleDetailListModify.get(i).getEndDate());
											scheduleDetailThu.setSelected(flag);
											scheduleDetailList.add(scheduleDetailThu);
										}else
											if(scheduleDetailListModify.get(i).getDay().compareTo(Calendar.FRIDAY)==0){
												flag = Boolean.TRUE;
												scheduleDetailFri = new ScheduleDetail();
												scheduleDetailFri.setDay(Calendar.FRIDAY);
												scheduleDetailFri.setInitialDate(scheduleDetailListModify.get(i).getInitialDate());
												scheduleDetailFri.setEndDate(scheduleDetailListModify.get(i).getEndDate());
												scheduleDetailFri.setSelected(flag);
												scheduleDetailList.add(scheduleDetailFri);
												
											}else
												if(scheduleDetailListModify.get(i).getDay().compareTo(Calendar.SATURDAY)==0){
													flag = Boolean.TRUE;
													scheduleDetailSat = new ScheduleDetail();
													scheduleDetailSat.setDay(Calendar.SATURDAY);
													scheduleDetailSat.setInitialDate(scheduleDetailListModify.get(i).getInitialDate());
													scheduleDetailSat.setEndDate(scheduleDetailListModify.get(i).getEndDate());
													scheduleDetailSat.setSelected(flag);
													scheduleDetailList.add(scheduleDetailSat);
													
												}else
													if(scheduleDetailListModify.get(i).getDay().compareTo(Calendar.SUNDAY)==0){
														flag = Boolean.TRUE;
														scheduleDetailSun = new ScheduleDetail();
														scheduleDetailSun.setDay(Calendar.SUNDAY);
														scheduleDetailSun.setInitialDate(scheduleDetailListModify.get(i).getInitialDate());
														scheduleDetailSun.setEndDate(scheduleDetailListModify.get(i).getEndDate());
														scheduleDetailSun.setSelected(flag);
														scheduleDetailList.add(scheduleDetailSun);
														
													}	
							scheduleSession.setScheduleDetails(scheduleDetailList);
						break;
						}
					}
				}
				
			}
		}
	}
	
	/**
	 * load initial dates.
	 */
	public void loadTableInitialDate(){
		Calendar cal = Calendar.getInstance();
		cal.clear();
		tabInitialHour = cal.getTime();
		cal.add(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		tabFinalHour = cal.getTime();
		scheduleSession.setScheduleDetails(null);
		scheduleDetailList = new ArrayList<ScheduleDetail>();
				if(scheduleDetailMon.isSelected()){
					scheduleDetailMon.setDay(Calendar.MONDAY);
					if(scheduleDetailMon.getInitialDate()==null){
						scheduleDetailMon.setInitialDate(tabInitialHour);
						scheduleDetailMon.setEndDate(tabFinalHour);
					}
					scheduleDetailList.add(scheduleDetailMon);
					scheduleSession.setScheduleDetails(scheduleDetailList);	
				}else{
					scheduleDetailMon = new ScheduleDetail();
					setSelectAll(false);
				}
				if(scheduleDetailTue.isSelected()){
					scheduleDetailTue.setDay(Calendar.TUESDAY);
					if(scheduleDetailTue.getInitialDate()==null){
						scheduleDetailTue.setInitialDate(tabInitialHour);
						scheduleDetailTue.setEndDate(tabFinalHour);
					}
					scheduleDetailList.add(scheduleDetailTue);
					scheduleSession.setScheduleDetails(scheduleDetailList);	
				}else{
					scheduleDetailTue = new ScheduleDetail();
					setSelectAll(false);
				}
				if(scheduleDetailWed.isSelected()){
					scheduleDetailWed.setDay(Calendar.WEDNESDAY);
					if(scheduleDetailWed.getInitialDate()==null){
						scheduleDetailWed.setInitialDate(tabInitialHour);			
						scheduleDetailWed.setEndDate(tabFinalHour);
					}
					scheduleDetailList.add(scheduleDetailWed);
					scheduleSession.setScheduleDetails(scheduleDetailList);	
				}else{
					scheduleDetailWed = new ScheduleDetail();
					setSelectAll(false);
				}
				if(scheduleDetailThu.isSelected()){
					scheduleDetailThu.setDay(Calendar.THURSDAY);
					if(scheduleDetailThu.getInitialDate()==null){
						scheduleDetailThu.setInitialDate(tabInitialHour);
						scheduleDetailThu.setEndDate(tabFinalHour);
					}
					scheduleDetailList.add(scheduleDetailThu);
					scheduleSession.setScheduleDetails(scheduleDetailList);		
				}else{
					scheduleDetailThu = new ScheduleDetail();
					setSelectAll(false);
				}
				if(scheduleDetailFri.isSelected()){
					if(scheduleDetailFri.getInitialDate()==null){
						scheduleDetailFri.setDay(Calendar.FRIDAY);
						scheduleDetailFri.setInitialDate(tabInitialHour);
						scheduleDetailFri.setEndDate(tabFinalHour);
					}
						scheduleDetailList.add(scheduleDetailFri);
						scheduleSession.setScheduleDetails(scheduleDetailList);		
				}else{
					scheduleDetailFri = new ScheduleDetail();
					setSelectAll(false);
				}
				if(scheduleDetailSat.isSelected()){
					if(scheduleDetailSat.getInitialDate()==null){
						scheduleDetailSat.setDay(Calendar.SATURDAY);
						scheduleDetailSat.setInitialDate(tabInitialHour);
						scheduleDetailSat.setEndDate(tabFinalHour);
					}
					scheduleDetailList.add(scheduleDetailSat);
					scheduleSession.setScheduleDetails(scheduleDetailList);		
				}else{
					scheduleDetailSat = new ScheduleDetail();
					setSelectAll(false);
				}
				if(scheduleDetailSun.isSelected()){
					if(scheduleDetailSun.getInitialDate()==null){
						scheduleDetailSun.setDay(Calendar.SUNDAY);
						scheduleDetailSun.setInitialDate(tabInitialHour);
						scheduleDetailSun.setEndDate(tabFinalHour);
					}
					scheduleDetailList.add(scheduleDetailSun);
					scheduleSession.setScheduleDetails(scheduleDetailList);		
				}else{
					scheduleDetailSun = new ScheduleDetail();
					setSelectAll(false);
				}
				
				if(Validations.validateListIsNotNullAndNotEmpty(scheduleSession.getScheduleDetails()) && scheduleSession.getScheduleDetails().size()==7){
					setSelectAll(true);
				}
				//Updating the description
				checkScheduleListener();
	}
	/**..
	 * Search schedule action.
	 */
	public void searchScheduleAction(){
		hideDialogs();
		List<Schedule> scheduleListFromSearch;
		if(!validationBeforeSearchOperation()){
		try {
			
			//this.getScheduleFilter().setInstitutionType(institutionSecuritySelected);
			if(searchUserAccountTypeSelected != null){
			List<Integer> institutelist = new ArrayList<Integer>();
			if(UserAccountType.INTERNAL.getCode().equals(searchUserAccountTypeSelected)){
				if(searchScheduleFilter.getInstitutionType() == null || searchScheduleFilter.getInstitutionType() == -1){
					for(InstitutionType institution : InstitutionType.listInternalInstitutions){
						institutelist.add(institution.getCode().intValue());
					}
				}
			}else if(UserAccountType.EXTERNAL.getCode().equals(searchUserAccountTypeSelected)){
				if(searchScheduleFilter.getInstitutionType()==null || searchScheduleFilter.getInstitutionType() == -1){
					for(InstitutionType institution : InstitutionType.listExternalInstitutions){
						institutelist.add(institution.getCode().intValue());
					}
				}
			}
			searchScheduleFilter.setLstInstitutiontype(institutelist);
			}
			scheduleListFromSearch = systemServiceFacade.getSchedulesServiceFacade(searchScheduleFilter);			
			List<Schedule> scheduleList = new ArrayList<Schedule>();
			long tmp = 0;
			for(Schedule obj : scheduleListFromSearch){
				if(obj.getIdSchedulePk() != tmp ){
					scheduleList.add(obj);
					tmp = obj.getIdSchedulePk();
				}
			}
			scheduleDataModel = new GenericDataModel<Schedule>(scheduleList);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		}
	}	
	
	/**
	 * Register schedule listener.
	 */
	public void registerScheduleListener(){
		this.setOperationType(ViewOperationsType.REGISTER.getCode());
		this.getUserAccountTypeList().removeAll(this.getUserAccountTypeList());
		this.getInstitutionsTypeList().removeAll(this.getInstitutionsTypeList());
		Calendar cal = Calendar.getInstance();
		cal.clear();
		initialHour = cal.getTime();
		tabInitialHour=initialHour;
		cal.add(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		finalHour = cal.getTime();
		tabFinalHour = initialHour;
		loadComponents();
		setSelectAll(true);
		selectAllListener();
		
	}

	/**
	 * Check schedule listener.
	 *
	 * @author mmacalupu
	 * This method is useful when a user make click en the checkbox's datatable
	 * get the last schedule inserted it's important to make rendered the minimal
	 * final schedule in same day.
	 */
	public void checkScheduleListener(){
		JSFUtilities.hideGeneralDialogues();
		if(scheduleSession.getScheduleDetails()!=null)
		for(ScheduleDetail scheduleDetail: scheduleSession.getScheduleDetails()){
			if(scheduleDetail.isSelected()){
				if(scheduleDetail.getDay().compareTo(Calendar.MONDAY)== 0 ){
					if(scheduleDetailMon.getInitialDate()!=null && scheduleDetailMon.getEndDate()!=null){
					   Calendar calInitDate = Calendar.getInstance();
					   calInitDate.setTime(scheduleDetailMon.getInitialDate());
					   Calendar calFinalDate = Calendar.getInstance();
					   calFinalDate.setTime(scheduleDetailMon.getEndDate());
					   if(calInitDate.get(Calendar.HOUR_OF_DAY) == calFinalDate.get(Calendar.HOUR_OF_DAY)){
						    if(calInitDate.get(Calendar.MINUTE) >= calFinalDate.get(Calendar.MINUTE)){
						     scheduleDetailMon.setEndDate(new Date(scheduleDetailMon.getInitialDate().getTime()+60000));
						    }
					   }
					   else if(calInitDate.get(Calendar.HOUR_OF_DAY) > calFinalDate.get(Calendar.HOUR_OF_DAY)){
						   scheduleDetailMon.setEndDate(new Date(scheduleDetailMon.getInitialDate().getTime()+60000));
					   }
					
					}
				}
				else if(scheduleDetail.getDay().compareTo(Calendar.TUESDAY) == 0 ){
						if(scheduleDetailTue.getInitialDate()!=null && scheduleDetailTue.getEndDate()!=null){
							   Calendar calInitDate = Calendar.getInstance();
							   calInitDate.setTime(scheduleDetailTue.getInitialDate());
							   Calendar calFinalDate = Calendar.getInstance();
							   calFinalDate.setTime(scheduleDetailTue.getEndDate());
							   if(calInitDate.get(Calendar.HOUR_OF_DAY) == calFinalDate.get(Calendar.HOUR_OF_DAY)){
							    if(calInitDate.get(Calendar.MINUTE) >= calFinalDate.get(Calendar.MINUTE)){
							     scheduleDetailTue.setEndDate(new Date(scheduleDetailTue.getInitialDate().getTime()+60000));
							    }
							   }
							   else if(calInitDate.get(Calendar.HOUR_OF_DAY)>calFinalDate.get(Calendar.HOUR_OF_DAY)){
								   scheduleDetailTue.setEndDate(new Date(scheduleDetailTue.getInitialDate().getTime()+60000));
							   }
							
							  }
				}else if(scheduleDetail.getDay().compareTo( Calendar.WEDNESDAY) == 0 ){
						if(scheduleDetailWed.getInitialDate()!=null && scheduleDetailWed.getEndDate()!=null){
							   Calendar calInitDate = Calendar.getInstance();
							   calInitDate.setTime(scheduleDetailWed.getInitialDate());
							   Calendar calFinalDate = Calendar.getInstance();
							   calFinalDate.setTime(scheduleDetailWed.getEndDate());
							   if(calInitDate.get(Calendar.HOUR_OF_DAY) == calFinalDate.get(Calendar.HOUR_OF_DAY)){
							    if(calInitDate.get(Calendar.MINUTE) >= calFinalDate.get(Calendar.MINUTE)){
							     scheduleDetailWed.setEndDate(new Date(scheduleDetailWed.getInitialDate().getTime()+60000));
							    }
							   }else if(calInitDate.get(Calendar.HOUR_OF_DAY)>calFinalDate.get(Calendar.HOUR_OF_DAY)){
								   scheduleDetailWed.setEndDate(new Date(scheduleDetailWed.getInitialDate().getTime()+60000));
							   }
							
							  }
					
				}else if(scheduleDetail.getDay().compareTo(Calendar.THURSDAY) == 0 ){
						if(scheduleDetailThu.getInitialDate()!=null && scheduleDetailThu.getEndDate()!=null){
							   Calendar calInitDate = Calendar.getInstance();
							   calInitDate.setTime(scheduleDetailThu.getInitialDate());
							   Calendar calFinalDate = Calendar.getInstance();
							   calFinalDate.setTime(scheduleDetailThu.getEndDate());
							   if(calInitDate.get(Calendar.HOUR_OF_DAY) == calFinalDate.get(Calendar.HOUR_OF_DAY)){
							    if(calInitDate.get(Calendar.MINUTE) >= calFinalDate.get(Calendar.MINUTE)){
							     scheduleDetailThu.setEndDate(new Date(scheduleDetailThu.getInitialDate().getTime()+60000));
							    }
							   }else if(calInitDate.get(Calendar.HOUR_OF_DAY)>calFinalDate.get(Calendar.HOUR_OF_DAY)){
								   scheduleDetailThu.setEndDate(new Date(scheduleDetailThu.getInitialDate().getTime()+60000));
							   }
							
							  }
					
				}else if(scheduleDetail.getDay().compareTo(Calendar.FRIDAY) == 0 ){
						if(scheduleDetailFri.getInitialDate()!=null && scheduleDetailFri.getEndDate()!=null){
							   Calendar calInitDate = Calendar.getInstance();
							   calInitDate.setTime(scheduleDetailFri.getInitialDate());
							   Calendar calFinalDate = Calendar.getInstance();
							   calFinalDate.setTime(scheduleDetailFri.getEndDate());
							   if(calInitDate.get(Calendar.HOUR_OF_DAY) == calFinalDate.get(Calendar.HOUR_OF_DAY)){
							    if(calInitDate.get(Calendar.MINUTE) >= calFinalDate.get(Calendar.MINUTE)){
							     scheduleDetailFri.setEndDate(new Date(scheduleDetailFri.getInitialDate().getTime()+60000));
							    }
							   }
							   else if(calInitDate.get(Calendar.HOUR_OF_DAY)>calFinalDate.get(Calendar.HOUR_OF_DAY)){
								   scheduleDetailFri.setEndDate(new Date(scheduleDetailFri.getInitialDate().getTime()+60000));
							   }
							
							  }
				}else if(scheduleDetail.getDay().compareTo(Calendar.SATURDAY) == 0 ){
					if(scheduleDetailSat.getInitialDate()!=null && scheduleDetailSat.getEndDate()!=null){
						   Calendar calInitDate = Calendar.getInstance();
						   calInitDate.setTime(scheduleDetailSat.getInitialDate());
						   Calendar calFinalDate = Calendar.getInstance();
						   calFinalDate.setTime(scheduleDetailSat.getEndDate());
						   if(calInitDate.get(Calendar.HOUR_OF_DAY) == calFinalDate.get(Calendar.HOUR_OF_DAY)){
						    if(calInitDate.get(Calendar.MINUTE) >= calFinalDate.get(Calendar.MINUTE)){
						    	scheduleDetailSat.setEndDate(new Date(scheduleDetailSat.getInitialDate().getTime()+60000));
						    }
						   }
						   else if(calInitDate.get(Calendar.HOUR_OF_DAY)>calFinalDate.get(Calendar.HOUR_OF_DAY)){
							   scheduleDetailSat.setEndDate(new Date(scheduleDetailSat.getInitialDate().getTime()+60000));
						   }
						
						  }
			}else if(scheduleDetail.getDay().compareTo(Calendar.SUNDAY) == 0 ){
				if(scheduleDetailSun.getInitialDate()!=null && scheduleDetailSun.getEndDate()!=null){
					   Calendar calInitDate = Calendar.getInstance();
					   calInitDate.setTime(scheduleDetailSun.getInitialDate());
					   Calendar calFinalDate = Calendar.getInstance();
					   calFinalDate.setTime(scheduleDetailSun.getEndDate());
					   if(calInitDate.get(Calendar.HOUR_OF_DAY) == calFinalDate.get(Calendar.HOUR_OF_DAY)){
					    if(calInitDate.get(Calendar.MINUTE) >= calFinalDate.get(Calendar.MINUTE)){
					    	scheduleDetailSun.setEndDate(new Date(scheduleDetailSun.getInitialDate().getTime()+60000));
					    }
					   }
					   else if(calInitDate.get(Calendar.HOUR_OF_DAY)>calFinalDate.get(Calendar.HOUR_OF_DAY)){
						   scheduleDetailSun.setEndDate(new Date(scheduleDetailSun.getInitialDate().getTime()+60000));
					   }
					
					  }
				}
			}
		}
				
	}
	
	 
	/**
	 * Save schedule.
	 */
	@LoggerAuditWeb
	public void saveSchedule(){
		JSFUtilities.hideGeneralDialogues();
		this.hideDialogs();
		try{
			String messageReturn="";
			if(isModify()){
				Schedule tmpSchedule = systemServiceFacade.getScheduleServiceFacade(scheduleSession);
				SimpleDateFormat  dateFormatter = new SimpleDateFormat(PropertiesUtilities.getMessage(PropertiesConstants.DATE_FORMAT_WITH_SECONDS));
				String strScheduleSession = dateFormatter.format(scheduleSession.getLastModifyDate());
				String strTmpSchedule = dateFormatter.format(tmpSchedule.getLastModifyDate());
				Calendar selectedSchedule = Calendar.getInstance();
				selectedSchedule.setTime(dateFormatter.parse(strScheduleSession));
				Calendar tempScheduleDate = Calendar.getInstance();
				tempScheduleDate.setTime(dateFormatter.parse(strTmpSchedule));
				if(tempScheduleDate.after(selectedSchedule)){
					showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.ERROR_SCHEDULE_ALREADY_MODIFIED, null);
					JSFUtilities.showEndTransactionDialog();
					this.getUserAccountTypeList().clear();
					this.getInstitutionsTypeList().clear();
					this.setScheduleFilter(new ScheduleFilter());
					//this.setScheduleDataModel(null);
					return;
				}
			}
				Schedule schedule = new Schedule();
				List<ScheduleDetail> scheduleDetailList = new ArrayList<ScheduleDetail>();
					for(ScheduleDetail scheduleDetail: scheduleSession.getScheduleDetails()){
						if(scheduleDetail.isSelected()){
							if(scheduleDetail.getDay()== Calendar.MONDAY){
								scheduleDetail.setInitialDate(scheduleDetailMon.getInitialDate());
								scheduleDetail.setEndDate(scheduleDetailMon.getEndDate());
							}
							else if(scheduleDetail.getDay()==  Calendar.TUESDAY){
								scheduleDetail.setInitialDate(scheduleDetailTue.getInitialDate());
								scheduleDetail.setEndDate(scheduleDetailTue.getEndDate());
							}
							else if(scheduleDetail.getDay()==  Calendar.WEDNESDAY){
								scheduleDetail.setInitialDate(scheduleDetailWed.getInitialDate());
								scheduleDetail.setEndDate(scheduleDetailWed.getEndDate());
							}
							else if(scheduleDetail.getDay()==  Calendar.THURSDAY){
								scheduleDetail.setInitialDate(scheduleDetailThu.getInitialDate());
								scheduleDetail.setEndDate(scheduleDetailThu.getEndDate());
							}
							else if(scheduleDetail.getDay()==  Calendar.FRIDAY){
								scheduleDetail.setInitialDate(scheduleDetailFri.getInitialDate());
								scheduleDetail.setEndDate(scheduleDetailFri.getEndDate());
							}					
							else if(scheduleDetail.getDay()==  Calendar.SATURDAY){
								scheduleDetail.setInitialDate(scheduleDetailSat.getInitialDate());
								scheduleDetail.setEndDate(scheduleDetailSat.getEndDate());
							}					
							else if(scheduleDetail.getDay()==  Calendar.SUNDAY){
								scheduleDetail.setInitialDate(scheduleDetailSun.getInitialDate());
								scheduleDetail.setEndDate(scheduleDetailSun.getEndDate());
							}					
									
								scheduleDetail.setSchedule(schedule);
								scheduleDetail.setRegistryUser(userInfo.getUserAccountSession().getUserName());
								scheduleDetail.setRegistryDate(new Date());
								scheduleDetail.setState(ScheduleDetailStateType.REGISTERED.getCode()); //added newly
								scheduleDetailList.add(scheduleDetail);
							
						}
					}
				schedule.setDescription(scheduleSession.getDescription());
				schedule.setScheduleType(scheduleSession.getScheduleType());
				schedule.setScheduleDetails(scheduleDetailList);
				schedule.setInstitutionType(this.getInstitutionSecuritySelected());
				if(this.isModify()){
//					put this logic in same transaction
//					systemServiceFacade.deleteScheduleDetailServiceFacade(scheduleSession.getIdSchedulePk());
					schedule.setIdSchedulePk(this.scheduleSession.getIdSchedulePk());
					schedule.setRegistryUser(this.scheduleSession.getRegistryUser());
					schedule.setRegistryDate(this.scheduleSession.getRegistryDate());
					schedule.setState(scheduleSession.getState());
					schedule.setSituation(scheduleSession.getSituation());
					messageReturn = PropertiesConstants.SCHEDULE_MESSAGE_MODIFY;
					systemServiceFacade.modifyScheduleServiceFacade(schedule);
					cleanVariablesListener();
				}else if(this.isRegister()){
					schedule.setState(ScheduleStateType.REGISTERED.getCode());
					schedule.setRegistryUser(userInfo.getUserAccountSession().getUserName());
					schedule.setRegistryDate(new Date());
					systemServiceFacade.registerScheduleServiceFacade(schedule);
					messageReturn = PropertiesConstants.SCHEDULE_MESSAGE_REGISTER;
					cleanVariablesListener();
				}
			showMessageOnDialog(
					PropertiesConstants.HEADER_MSG_SCHEDULE_SUCCESS_REGISTRATION, null, 
					messageReturn, new Object[]{String.valueOf(schedule.getIdSchedulePk())});
			JSFUtilities.showEndTransactionDialog();
		}catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Block schedule listener.
	 */
	@LoggerAuditWeb
	public void blockScheduleListener(){
		try{
			showOtherMotive = false;
			Schedule tmpSchedule = systemServiceFacade.getScheduleServiceFacade(scheduleSession);
			
			SimpleDateFormat  dateFormatter = new SimpleDateFormat(PropertiesUtilities.getMessage(PropertiesConstants.DATE_FORMAT_WITH_SECONDS));
			String strScheduleSession = dateFormatter.format(scheduleSession.getLastModifyDate());
			String strTmpSchedule = dateFormatter.format(tmpSchedule.getLastModifyDate());
			
			Calendar selectedSchedule = Calendar.getInstance();
			selectedSchedule.setTime(dateFormatter.parse(strScheduleSession));
		
			Calendar tempScheduleDate = Calendar.getInstance();
			tempScheduleDate.setTime(dateFormatter.parse(strTmpSchedule));
			
			if(tempScheduleDate.after(selectedSchedule)){				
				searchScheduleAction();
				showMessageOnDialog(null, null, PropertiesConstants.ERROR_SCHEDULE_ALREADY_BLOCKED, null);
				JSFUtilities.showEndTransactionDialog();
				return;
			}

			this.scheduleSession = tmpSchedule;
			
			String messageModal = "";
			if(isRejectOption && (this.scheduleSession.getState().equals(ScheduleStateType.CONFIRMED.getCode()) &&
					this.scheduleSession.getSituation().equals(ScheduleSituationType.PENDING_LOCK.getCode()))){
				this.scheduleSession.setSituation(ScheduleSituationType.ACTIVE.getCode());
				
				historicalState.setCurrentState(this.scheduleSession.getState());
				messageModal = PropertiesConstants.SYSTEM_SCHEDULE_REJECT_BLOCK_SUCCESS;
				
			}else if(this.scheduleSession.getState().equals(ScheduleStateType.CONFIRMED.getCode()) &&
				this.scheduleSession.getSituation().equals(ScheduleSituationType.ACTIVE.getCode())){
				
				this.scheduleSession.setSituation(ScheduleSituationType.PENDING_LOCK.getCode());
				
				historicalState.setCurrentState(this.scheduleSession.getSituation());
				messageModal = PropertiesConstants.SYSTEM_SCHEDULE_REQUEST_BLOCK_SUCCESS;
				
			}else if(this.scheduleSession.getState().equals(ScheduleStateType.CONFIRMED.getCode()) &&
					this.scheduleSession.getSituation().equals(ScheduleSituationType.PENDING_LOCK.getCode())){
				
				this.scheduleSession.setState(ScheduleStateType.BLOCKED.getCode());
				this.scheduleSession.setSituation(ScheduleSituationType.ACTIVE.getCode());	
				
				historicalState.setCurrentState(this.scheduleSession.getState());
				messageModal = PropertiesConstants.SYSTEM_SCHEDULE_BLOCK_SUCCESS;
			}
			
			this.scheduleSession.setBlockMotive(isRejectOption ? Integer.valueOf(-1) : historicalState.getMotive());
			this.scheduleSession.setBlockOtherMotive(isRejectOption ? "" : historicalState.getMotiveDescription());

			ThreadLocalContextHolder.put(
					PropertiesConstants.HISTORICAL_STATE_LOGGER, 
					createHistorialState(
							scheduleSession.getClass().getAnnotation(Table.class).name(), 
							ScheduleStateType.CONFIRMED.getCode(),
							scheduleSession.getIdSchedulePk(), 
							historicalState));
			
			systemServiceFacade.updateScheduleStateServiceFacade(this.getScheduleSession());
			
			this.searchScheduleAction();
			Object[] arrBodyData = {String.valueOf(scheduleSession.getIdSchedulePk())};
			showMessageOnDialog(
					PropertiesConstants.LBL_CONFIRM_HEADER_BLOCK, null, 
					messageModal, arrBodyData);			
			JSFUtilities.showEndTransactionDialog();			
		}catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Un block schedule listener.
	 */
	@LoggerAuditWeb
	public void unBlockScheduleListener(){
		try{
			Schedule tmpSchedule = systemServiceFacade.getScheduleServiceFacade(scheduleSession);
			SimpleDateFormat  dateFormatter = new SimpleDateFormat(PropertiesUtilities.getMessage(PropertiesConstants.DATE_FORMAT_WITH_SECONDS));
			String strScheduleSession = dateFormatter.format(scheduleSession.getLastModifyDate());
			String strTmpSchedule = dateFormatter.format(tmpSchedule.getLastModifyDate());
			Calendar selectedSchedule = Calendar.getInstance();
			selectedSchedule.setTime(dateFormatter.parse(strScheduleSession));
			Calendar tempScheduleDate = Calendar.getInstance();
			tempScheduleDate.setTime(dateFormatter.parse(strTmpSchedule));
			
			if(tempScheduleDate.after(selectedSchedule)){				
				searchScheduleAction();
				showMessageOnDialog(null, null, PropertiesConstants.ERROR_SCHEDULE_ALREADY_UNBLOCKED, null);
				JSFUtilities.showEndTransactionDialog();
				return;
			}

			this.scheduleSession = tmpSchedule;
			
			String messageModal = "";
			if(isRejectOption && this.scheduleSession.getState().equals(ScheduleStateType.BLOCKED.getCode()) &&
				this.scheduleSession.getSituation().equals(ScheduleSituationType.PENDING_UNLOCK.getCode())){
				
				this.scheduleSession.setSituation(ScheduleSituationType.ACTIVE.getCode());
				
				historicalState.setCurrentState(this.scheduleSession.getSituation());
				messageModal = PropertiesConstants.SYSTEM_SCHEDULE_REJECT_UNBLOCK_SUCCESS;
				
			}else if(this.scheduleSession.getState().equals(ScheduleStateType.BLOCKED.getCode()) &&
				this.scheduleSession.getSituation().equals(ScheduleSituationType.ACTIVE.getCode())){
				
				this.scheduleSession.setSituation(ScheduleSituationType.PENDING_UNLOCK.getCode());
				
				historicalState.setCurrentState(this.scheduleSession.getSituation());
				messageModal = PropertiesConstants.SYSTEM_SCHEDULE_REQUEST_UNBLOCK_SUCCESS;
				
			}else if(this.scheduleSession.getState().equals(ScheduleStateType.BLOCKED.getCode()) &&
					this.scheduleSession.getSituation().equals(ScheduleSituationType.PENDING_UNLOCK.getCode())){
				
				this.scheduleSession.setState(ScheduleStateType.CONFIRMED.getCode());
				this.scheduleSession.setSituation(ScheduleSituationType.ACTIVE.getCode());
				
				historicalState.setCurrentState(this.scheduleSession.getState());
				messageModal = PropertiesConstants.SYSTEM_SCHEDULE_UNBLOCK_SUCCESS;
			}				
			
			this.scheduleSession.setUnblockMotive(isRejectOption ? Integer.valueOf(-1) : historicalState.getMotive());
			this.scheduleSession.setUnblockOtherMotive(isRejectOption ? "" : historicalState.getMotiveDescription());
			
			ThreadLocalContextHolder.put(
					PropertiesConstants.HISTORICAL_STATE_LOGGER, 
					createHistorialState(
							scheduleSession.getClass().getAnnotation(Table.class).name(), 
							ScheduleStateType.BLOCKED.getCode(),
							scheduleSession.getIdSchedulePk(),
							historicalState));
			
			systemServiceFacade.updateScheduleStateServiceFacade(this.getScheduleSession());
			
			this.searchScheduleAction();
			Object[] arrBodyData = {String.valueOf(scheduleSession.getIdSchedulePk())};
			showMessageOnDialog(
					PropertiesConstants.LBL_CONFIRM_HEADER_UNBLOCK, null, 
					messageModal, arrBodyData);
			JSFUtilities.showEndTransactionDialog();
		}catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Confirm schedule listener.
	 */
	@LoggerAuditWeb
	public void confirmScheduleListener(){
		try{
			hideDialogs();
			Schedule tmpSchedule = systemServiceFacade.getScheduleServiceFacade(scheduleSession);
			if(tmpSchedule.getLastModifyDate().after(scheduleSession.getLastModifyDate())){				
				searchScheduleAction();
				showMessageOnDialog(null, null, PropertiesConstants.ERROR_SCHEDULE_ALREADY_UNBLOCKED, null);
				JSFUtilities.showEndTransactionDialog();
				return;
			}
			this.scheduleSession = tmpSchedule;
			this.scheduleSession.setState(ScheduleStateType.CONFIRMED.getCode());
			this.scheduleSession.setSituation(ScheduleSituationType.ACTIVE.getCode());
			//historial state
			historicalState.setCurrentState(ScheduleStateType.CONFIRMED.getCode());
			historicalState.setMotive(Integer.valueOf(-1));
			ThreadLocalContextHolder.put(
					PropertiesConstants.HISTORICAL_STATE_LOGGER, 
					createHistorialState(
							scheduleSession.getClass().getAnnotation(Table.class).name(), 
							ScheduleStateType.REGISTERED.getCode(),
							scheduleSession.getIdSchedulePk(), 
							historicalState));
			
			systemServiceFacade.confirmScheduleServiceFacade(this.getScheduleSession());
			
			this.searchScheduleAction();			
			Object[] arrBodyData = {String.valueOf(scheduleSession.getIdSchedulePk())};
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_SUCCESS,null,
					PropertiesConstants.SYSTEM_SCHEDULE_CONFIRM_SUCCESS, 
					arrBodyData);			
			JSFUtilities.showEndTransactionDialog();			
		}catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Reject schedule listener.
	 */
	@LoggerAuditWeb
	public void rejectScheduleListener(){
		try{
			Schedule tmpSchedule = systemServiceFacade.getScheduleServiceFacade(scheduleSession);
			if(tmpSchedule.getLastModifyDate().after(scheduleSession.getLastModifyDate())){				
				searchScheduleAction();
				showMessageOnDialog(null, null, PropertiesConstants.ERROR_SCHEDULE_ALREADY_UNBLOCKED, null);
				JSFUtilities.showEndTransactionDialog();
				return;
			}
			this.scheduleSession = tmpSchedule;
			this.scheduleSession.setState(ScheduleStateType.REJECT.getCode());
			this.scheduleSession.setSituation(ScheduleSituationType.ACTIVE.getCode());
			this.scheduleSession.setRejectMotive(historicalState.getMotive());
			this.scheduleSession.setRejectOtherMotive(historicalState.getMotiveDescription());
			
			historicalState.setCurrentState(ScheduleStateType.REJECT.getCode());
			
			ThreadLocalContextHolder.put(
					PropertiesConstants.HISTORICAL_STATE_LOGGER, 
					createHistorialState(
							scheduleSession.getClass().getAnnotation(Table.class).name(), 
							ScheduleStateType.REGISTERED.getCode(),
							scheduleSession.getIdSchedulePk(), 
							historicalState));
			
			systemServiceFacade.updateScheduleStateServiceFacade(this.getScheduleSession());
			
			this.searchScheduleAction();
			
			Object[] arrBodyData = {String.valueOf(scheduleSession.getIdSchedulePk())};
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_SUCCESS,null, 
					PropertiesConstants.SYSTEM_SCHEDULE_REJECT_SUCCESS,
					arrBodyData);
			
			JSFUtilities.showEndTransactionDialog();			
		}catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	
	/**
	 * Hide confirm dialog.
	 */
	public void hideConfirmDialog(){
		if(isBlock()){
			JSFUtilities.hideComponent("frmBlock:cnfBlock");
		}
		if(isUnBlock()){
			JSFUtilities.hideComponent("frmUnblock:cnfUnblock");
		}
		if(isConfirm()){
			JSFUtilities.hideComponent("frmConfirm:cnfConfirm");
		}
		if(isReject()){
			JSFUtilities.hideComponent("frmReject:cnfReject");
		}
	}
	
	/**
	 * Before modify schedule listener.
	 *
	 * @return the string
	 */
	public String beforeModifyScheduleListener(){
		try{
			hideDialogs();			
			if(Validations.validateIsNull(this.selectedScheduleSession)){
				this.setOperationType(ViewOperationsType.MODIFY.getCode());
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.ERROR_RECORD_NO_SELECTED, null);
				JSFUtilities.showValidationDialog();
				return null;
			} else if(!ScheduleStateType.REGISTERED.getCode().equals(this.selectedScheduleSession.getState())
					&& !ScheduleStateType.CONFIRMED.getCode().equals(this.selectedScheduleSession.getState())){
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.ERROR_ADM_SCHEDULE_MODIFY_IS_LOCK, null);
				JSFUtilities.showValidationDialog();
				return null;
		 	}
			
			fillData();
			this.setOperationType(ViewOperationsType.MODIFY.getCode());
			scheduleSession = systemServiceFacade.getScheduleServiceFacade(scheduleSession);
			if(this.scheduleSession.getScheduleType().equals(ScheduleType.SYSTEMS.getCode())){
				List<System> lstSystems = systemServiceFacade.getSystemsByScheduleServiceFacade(scheduleSession.getIdSchedulePk());
				scheduleSession.setSystems(lstSystems);
			}
			this.setOperationType(ViewOperationsType.MODIFY.getCode());
			loadComponents();
			if(scheduleSession.getScheduleType().equals(ScheduleType.INSTITUTIONS.getCode())){
				scheduleSession.setSystems(null);
				if(Validations.validateIsNotNull(userAccountTypeList)){
					userAccountTypeList.addAll(userAccountTypeListTemp);
					userAccountTypeSelected = searchUserAccountTypeSelected;
					}
				
				this.getInstitutionsTypeList().addAll(institutionsTypeListTemp);
				this.setInstitutionSecuritySelected(scheduleSession.getInstitutionType());
			}else{
				if(Validations.validateIsNotNull(userAccountTypeList)){
					userAccountTypeList.clear();
				}
			}
			return "scheduleMgmtRule";
		}catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		return null;		
	}
	
	/**
	 * Fill data.
	 */
	public void fillData(){
		scheduleSession = new Schedule();
		scheduleSession.setBlockMotive(selectedScheduleSession.getBlockMotive());
		scheduleSession.setBlockOtherMotive(selectedScheduleSession.getBlockOtherMotive());
		scheduleSession.setInstitutionType(selectedScheduleSession.getInstitutionType());
		scheduleSession.setScheduleType(selectedScheduleSession.getScheduleType());
		scheduleSession.setState(selectedScheduleSession.getState());
		scheduleSession.setSituation(selectedScheduleSession.getSituation());
		scheduleSession.setUnblockMotive(selectedScheduleSession.getUnblockMotive());
		scheduleSession.setUnblockOtherMotive(selectedScheduleSession.getUnblockOtherMotive());
		scheduleSession.setRejectMotive(selectedScheduleSession.getRejectMotive());
		scheduleSession.setRejectOtherMotive(selectedScheduleSession.getRejectOtherMotive());
		scheduleSession.setDescription(selectedScheduleSession.getDescription());
		scheduleSession.setIdSchedulePk(selectedScheduleSession.getIdSchedulePk());
		scheduleSession.setLastModifyDate(selectedScheduleSession.getLastModifyDate());
		scheduleSession.setLastModifyIp(selectedScheduleSession.getLastModifyIp());
		scheduleSession.setLastModifyPriv(selectedScheduleSession.getLastModifyPriv());
		scheduleSession.setLastModifyUser(selectedScheduleSession.getLastModifyUser());
		scheduleSession.setRegistryDate(selectedScheduleSession.getRegistryDate());
		scheduleSession.setRegistryUser(selectedScheduleSession.getRegistryUser());
		scheduleSession.setScheduleDetails(selectedScheduleSession.getScheduleDetails());
		scheduleSession.setSystems(selectedScheduleSession.getSystems());
	}
	
	/**
	 * Evaluate schedule type for search in the view(*.xhtml) 
	 */
	public void evaluateScheduleTypeForSearchListener(){
		this.scheduleDataModel = null;		
		this.getSearchUserAccountTypeList().removeAll(this.getSearchUserAccountTypeList());
		this.getInstitutionsTypeList().removeAll(this.getInstitutionsTypeList());
		this.getSearchScheduleFilter().setInstitutionType(null);
		this.setUserAccountTypeSelected(null);		
		if(Validations.validateIsNotNullAndPositive(this.searchScheduleFilter.getScheduleType()) && this.searchScheduleFilter.getScheduleType().equals(ScheduleType.INSTITUTIONS.getCode())){
			try{
				this.getSearchUserAccountTypeList().addAll(userAccountTypeListTemp);
			}catch(Exception ex){
				excepcion.fire(new ExceptionToCatchEvent(ex));
			}
		}else{
			if(Validations.validateIsNotNull(searchUserAccountTypeList)){
			searchUserAccountTypeList.clear();
			searchUserAccountTypeSelected = null;
			} 
			if(Validations.validateIsNotNull(searchInstitutionsTypeList)){
				searchInstitutionsTypeList.clear();
				searchScheduleFilter.setInstitutionType(null);
			}
		}
	}
	
	/**
	 * Evaluate schedule type in the view(*.xhtml).
	 */
	public void evaluateScheduleTypeListener(){
		hideDialogs();
		this.getUserAccountTypeList().removeAll(this.getUserAccountTypeList());
		this.getInstitutionsTypeList().removeAll(this.getInstitutionsTypeList());
		this.setInstitutionSecuritySelected(null);
		this.setUserAccountTypeSelected(null);
		if(this.scheduleSession.getScheduleType().equals(ScheduleType.INSTITUTIONS.getCode())){
			try{
				this.getUserAccountTypeList().addAll(userAccountTypeListTemp);
			}catch(Exception ex){
				excepcion.fire(new ExceptionToCatchEvent(ex));
			}
		}
	}
	
	/**
	 * Evaluate user account type listener.
	 */
	public void evaluateUserAccountTypeListener(){
		hideDialogs();
		this.getInstitutionsTypeList().removeAll(this.getInstitutionsTypeList());
		this.setInstitutionSecuritySelected(null);
		institutionsTypeList.addAll(institutionsTypeListTemp);
		if(this.userAccountTypeSelected.equals(UserAccountType.EXTERNAL.getCode())){
			for (Parameter param : institutionsTypeListTemp) {
				if(InstitutionType.DEPOSITARY.getCode().equals(param.getIdParameterPk())){
					institutionsTypeList.remove(param);	
                    //cevaldomInstitution = false;
				}
			}
		}else if(this.userAccountTypeSelected.equals(UserAccountType.INTERNAL.getCode())){
			for (Parameter param : institutionsTypeListTemp) {
				if(!InstitutionType.DEPOSITARY.getCode().equals(param.getIdParameterPk())){
					institutionsTypeList.remove(param);	
                    //cevaldomInstitution = true;
				}
			}
			this.setInstitutionSecuritySelected(InstitutionType.DEPOSITARY.getCode());
		}else{
			if(Validations.validateIsNotNull(institutionsTypeList)){
				institutionsTypeList.clear();
				institutionSecuritySelected = null;
			}
		}
//		if(this.userAccountTypeSelected.equals(UserAccountType.EXTERNAL.getCode())){
//			this.getInstitutionsTypeList().addAll(institutionsTypeListTemp);
//			this.getInstitutionsTypeList().remove(InstitutionType.CEVALDOM);
//			cevaldomInstitution = false; 
//		}else if(this.userAccountTypeSelected.equals(UserAccountType.INTERNAL.getCode())){
//			//this.getInstitutionsTypeList().add(InstitutionType.CEVALDOM);
//			this.setInstitutionSecuritySelected(InstitutionType.CEVALDOM.getCode());
//			cevaldomInstitution = true;
//		}
	}
	
	/**
	 * Evaluate institution security type listener.
	 */
	public void evaluateInstitutionSecurityTypeListener(){
		this.institutionSecurityList.removeAll(this.institutionSecurityList);
		InstitutionSecurityFilter institutionSecurityFilter = new InstitutionSecurityFilter();  
		institutionSecurityFilter.setIdScheduleState(ScheduleStateType.REGISTERED.getCode());
		List<InstitutionsSecurity> institutionsSecurityList = null;
		try{
			institutionsSecurityList = userServiceFacade.searchInstitutionsByFilterServiceFacade(institutionSecurityFilter);
			for(InstitutionsSecurity institutionsSecurity: institutionsSecurityList){
				if(this.institutionSecuritySelected.equals(institutionsSecurity.getIdInstitutionTypeFk())){
					this.institutionSecurityList.add(institutionsSecurity);
				}
			}
		}catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		
	}
	
	/**
	 * Select institution listener.
	 *
	 * @author mmacalupu
	 * this method is useful to handle the inistitutions' list
	 * selected in the checkbox's datatable
	 * @param institutionSecurity the institution security
	 */
	public void selectInstitutionListener(InstitutionsSecurity institutionSecurity){
		if(institutionSecurity.isSelected() /*&& !institutionSecurity.isHaveSchedule()*/){
			institutionSecururityListSelected.add(institutionSecurity);
		}else{
			institutionSecururityListSelected.remove(institutionSecurity);
		}
	}


	/**
	 * Before block schedule listener.
	 *
	 * @param e the e
	 */
	public void beforeBlockScheduleListener(ActionEvent e){		
		hideDialogs();	
		JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
		isConfirmBlockOrUnblock = false;
		isRejectOption = false;
		showOtherMotive = false;
		
        if(this.selectedScheduleSession==null){			
			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT,null,PropertiesConstants.ERROR_RECORD_NO_SELECTED, null);
			JSFUtilities.showValidationDialog();
			return;		
        }
        if(!selectedScheduleSession.getState().equals(ScheduleStateType.CONFIRMED.getCode())){
			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, 
					PropertiesConstants.SYSTEM_SCHEDULE_BLOCK_ERROR, null);
			JSFUtilities.showValidationDialog();
			return;
		}
		fillData();
		this.setOperationType(ViewOperationsType.BLOCK.getCode());
		
		if(this.scheduleSession.getState().equals(ScheduleStateType.CONFIRMED.getCode()) &&
			this.scheduleSession.getSituation().equals(ScheduleSituationType.PENDING_LOCK.getCode())){
			isConfirmBlockOrUnblock = true;	
		}
		
		generateMessageListener();
	}
	

	/**
	 * Before un block schedule listener.
	 *
	 * @param s the s
	 */
	public void beforeUnBlockScheduleListener(ActionEvent s){
		hideDialogs();	
		JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
		isConfirmBlockOrUnblock = false;
		isRejectOption = false;
		showOtherMotive = false;
		
		 if(this.selectedScheduleSession==null){
				
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT,null,PropertiesConstants.ERROR_RECORD_NO_SELECTED, null);
				JSFUtilities.showValidationDialog();
				return;
		 
		 }else if(ScheduleStateType.BLOCKED.getCode().intValue() != selectedScheduleSession.getState().intValue()){
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, 
						PropertiesConstants.SYSTEM_SCHEDULE_UNBLOCK_ERROR, null);
				JSFUtilities.showValidationDialog();
				return;
		 }	
	    fillData();
		this.setOperationType(ViewOperationsType.UNBLOCK.getCode());
		
		if(this.scheduleSession.getState().equals(ScheduleStateType.BLOCKED.getCode()) &&
			this.scheduleSession.getSituation().equals(ScheduleSituationType.PENDING_UNLOCK.getCode())){
			isConfirmBlockOrUnblock = true;
		}
		generateMessageListener();
	}
	
	/**
	 * Before confirm schedule listener.
	 *
	 * @param actionEvent the action event
	 */
	public void beforeConfirmScheduleListener(ActionEvent actionEvent){
		hideDialogs();	
		JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
		isConfirmBlockOrUnblock = false;
		isRejectOption = false;
		showOtherMotive = false;
 		if(this.selectedScheduleSession==null){		
			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT,null,PropertiesConstants.ERROR_RECORD_NO_SELECTED, null);
			JSFUtilities.showValidationDialog();
			return;
 		}
 		if(ScheduleStateType.REGISTERED.getCode().intValue() != selectedScheduleSession.getState().intValue()){
			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.SYSTEM_SCHEDULE_CONFIRM_ERROR, null);
			JSFUtilities.showValidationDialog();
			return;
 		}	
	    fillData();
		showOtherMotive = false;
		this.setOperationType(ViewOperationsType.CONFIRM.getCode());
		generateMessageListener();
	}	
	
	/**
	 * Before reject schedule listener.
	 *
	 * @param actionEvent the action event
	 */
	public void beforeRejectScheduleListener(ActionEvent actionEvent){
		hideDialogs();	
		JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
		isConfirmBlockOrUnblock = false;
		isRejectOption = false;
		showOtherMotive = false;
		
		 if(this.selectedScheduleSession==null){				
			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT,null,PropertiesConstants.ERROR_RECORD_NO_SELECTED, null);
			JSFUtilities.showValidationDialog();
			return;		 
		 }
		 if(ScheduleStateType.REGISTERED.getCode().intValue() != selectedScheduleSession.getState().intValue()){
			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.SYSTEM_SCHEDULE_REJECT_ERROR, null);
			JSFUtilities.showValidationDialog();
			return;
		 }	
	    fillData();	    
		this.setOperationType(ViewOperationsType.REJECT.getCode());
		generateMessageListener();
	}
	
	/**
	 * Generate mensage listener.
	 *
	 * @param viewOperationType the view operation type
	 */
	public void generateMensageListener(ViewOperationsType viewOperationType){
		this.setOperationType(viewOperationType.getCode());
		generateMessageListener();
	}
	
	/**
	 * Generate message listener.
	 */
	public void generateMessageListener(){
		historicalState.setMotive(Integer.valueOf(-1));
		historicalState = new HistoricalState();
		
		if ((isRegister() || isModify()) && validationBeforeRegisterhOperation()) {
			return;
		}
		if(isRegister() && Validations.validateIsNotNullAndPositive(userAccountTypeSelected) && Validations.validateIsNullOrNotPositive(institutionSecuritySelected)){
			JSFUtilities.showRequiredDialog();
			String msg = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_COMBO_SELECCIONE,PropertiesUtilities.getMessage("user.lbl.institution.type"));
			JSFUtilities.addContextMessage("mainForm:idCboInstitutionType", FacesMessage.SEVERITY_ERROR, msg, msg);
			return;
		}
		if(Validations.validateIsNullOrEmpty(scheduleSession.getDescription())){
			JSFUtilities.showRequiredDialog();
			String msg = PropertiesUtilities.getMessage("schedule.err.msg.description");
			JSFUtilities.addContextMessage("mainForm:txt_description", FacesMessage.SEVERITY_ERROR, msg, msg);
			return;
		}
		
		if(this.isRegister()){
			boolean haveDaySelectes = Boolean.FALSE;
			if(scheduleSession.getScheduleDetails() != null){
				for(ScheduleDetail scheduleDetail: scheduleSession.getScheduleDetails()){
						if(scheduleDetail.isSelected()){
							haveDaySelectes = Boolean.TRUE;
							break;
						}
				}
				if(!haveDaySelectes){
					String msg = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_SCHEDULE_DAYS_NO_SELECTED);
					JSFUtilities.addContextMessage("mainForm:idPnlSchedule", FacesMessage.SEVERITY_ERROR, msg, msg);
					showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.ERROR_ADM_SCHEDULE_DAYS_NO_SELECTED, null);
					JSFUtilities.showValidationDialog();
					return;
				}
			} else{
				String msg = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_SCHEDULE_DAYS_NO_SELECTED);
				JSFUtilities.addContextMessage("mainForm:idPnlSchedule", FacesMessage.SEVERITY_ERROR, msg, msg);
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.ERROR_ADM_SCHEDULE_DAYS_NO_SELECTED, null);
				JSFUtilities.showValidationDialog();
				return;
			}
			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.SCHEDULE_CONFIRM_REGISTER, null);
			JSFUtilities.showComponent(":mainForm:confirmScheduleMgmt");
			return;
		}
		if(this.isModify()){
			boolean haveDaySelectes = Boolean.FALSE;
				if(scheduleSession.getScheduleDetails() != null){
				for(ScheduleDetail scheduleDetail: scheduleSession.getScheduleDetails()){
						if(scheduleDetail.isSelected()){
							haveDaySelectes = Boolean.TRUE;
							break;
						}
				}
				if(!haveDaySelectes){
					String msg = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_SCHEDULE_DAYS_NO_SELECTED);
					JSFUtilities.addContextMessage("mainForm:idPnlSchedule", FacesMessage.SEVERITY_ERROR, msg, msg);
					showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.ERROR_ADM_SCHEDULE_DAYS_NO_SELECTED, null);
					JSFUtilities.showValidationDialog();
					return;
				}
			} else{
				String msg = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_SCHEDULE_DAYS_NO_SELECTED);
				JSFUtilities.addContextMessage("mainForm:idPnlSchedule", FacesMessage.SEVERITY_ERROR, msg, msg);
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.ERROR_ADM_SCHEDULE_DAYS_NO_SELECTED, null);
				JSFUtilities.showValidationDialog();
				return;
			}
			Object[] arrBodyData = {String.valueOf(scheduleSession.getIdSchedulePk())};
			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_CONFIRM, null, PropertiesConstants.SCHEDULE_CONFIRM_MODIFY, arrBodyData);
			JSFUtilities.showComponent(":mainForm:confirmScheduleMgmt");
			return;
		}
		if(this.isBlock()){
			showMotives();
			
			historicalState.setMotive(isConfirmBlockOrUnblock ? scheduleSession.getBlockMotive() : Integer.valueOf(-1));
			if(isConfirmBlockOrUnblock && scheduleSession.getBlockMotive().equals(OtherMotiveType.SYS_SCHEDULE_BLOCK_OTHER_MOTIVE.getCode())){							
				historicalState.setMotiveDescription(this.scheduleSession.getBlockOtherMotive());
				showOtherMotive=true;
			}
			JSFUtilities.setValidViewComponent("frmMotive:idcbomotive");
			JSFUtilities.setValidViewComponent("frmMotive:idothermotive");
			JSFUtilities.showComponent("frmMotive:dlgMotive");
			showMessageOnDialog2(PropertiesConstants.LBL_MOTIVE_BLOCK, null, null,null);
			return;
		}
		if(this.isUnBlock()){
			showMotives();
			
			historicalState.setMotive(isConfirmBlockOrUnblock ? this.scheduleSession.getUnblockMotive() : Integer.valueOf(-1));
			if (isConfirmBlockOrUnblock && scheduleSession.getUnblockMotive().equals(OtherMotiveType.SYS_SCHEDULE_UNBLOCK_OTHER_MOTIVE.getCode())){				
				this.historicalState.setMotiveDescription(scheduleSession.getUnblockOtherMotive());
				showOtherMotive=true;
			}	
			JSFUtilities.setValidViewComponent("frmMotive:idcbomotive");
			JSFUtilities.setValidViewComponent("frmMotive:idothermotive");
			JSFUtilities.showComponent("frmMotive:dlgMotive");
			showMessageOnDialog2(PropertiesConstants.LBL_MOTIVE_UNBLOCK, null, null,null);
			return;
		}
		if(this.isConfirm()){		
			Object [] data = {String.valueOf(scheduleSession.getIdSchedulePk())};
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_CONFIRM, null, 
					PropertiesConstants.SYSTEM_SCHEDULE_CONFIRM_MESSAGE, data);
			JSFUtilities.showComponent("frmConfirm:cnfConfirm");
			return;
			
		}
		if(this.isReject()){
			showMotives();
			JSFUtilities.setValidViewComponent("frmMotive:idcbomotive");
			JSFUtilities.setValidViewComponent("frmMotive:idothermotive");
			JSFUtilities.showComponent("frmMotive:dlgMotive");
			showMessageOnDialog2(PropertiesConstants.LBL_MOTIVE_REJECT, null, null,null);
			return;
		}		
	}
	
	/**
	 * This method return el name the action for navigate a other
	 * page across faces-config.xml.
	 *
	 * @return the string
	 */
	public String searchScheduleDetailAction(){
		try {
			List<com.pradera.security.model.System> systemListTemp = null;
			this.scheduleSession = systemServiceFacade.getScheduleServiceFacade(scheduleSession);
			if(this.scheduleSession.getScheduleType().equals(ScheduleType.SYSTEMS.getCode())){
				List<System> lstSystems = systemServiceFacade.getSystemsByScheduleServiceFacade(scheduleSession.getIdSchedulePk());
				this.scheduleSession.setSystems(lstSystems);
			}else{
				this.scheduleSession.setSystems(null);
			}
			this.setOperationType(ViewOperationsType.CONSULT.getCode());
		} catch (ServiceException ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		loadComponents();
		if(Validations.validateIsNotNullAndPositive(searchUserAccountTypeSelected)){
			userAccountTypeSelected = searchUserAccountTypeSelected;			
			institutionsTypeList.clear();
			institutionsTypeList.addAll(institutionsTypeListTemp);
			if(this.userAccountTypeSelected.equals(UserAccountType.EXTERNAL.getCode())){
				for (Parameter param : institutionsTypeListTemp) {
					if(InstitutionType.DEPOSITARY.getCode().equals(param.getIdParameterPk())){
						institutionsTypeList.remove(param);
					}
				}
			}else if(this.userAccountTypeSelected.equals(UserAccountType.INTERNAL.getCode())){
				for (Parameter param : institutionsTypeListTemp) {
					if(!InstitutionType.DEPOSITARY.getCode().equals(param.getIdParameterPk())){
						institutionsTypeList.remove(param);
					}
				}
			}
		}
		return "scheduleDetailMgmtRule";
	}
	
	/**
	 * Clean variables listener.
	 */
	public void cleanVariablesListener(){
		this.searchScheduleFilter = new ScheduleFilter();
		this.scheduleSession = new Schedule();
		scheduleSearch = new ArrayList<Schedule>();
		this.setSearchUserAccountTypeSelected(null);
		this.getSearchInstitutionsTypeList().removeAll(this.getSearchInstitutionsTypeList());
		this.getSearchUserAccountTypeList().removeAll(this.getSearchUserAccountTypeList());
		this.setScheduleDataModel(null);
		this.setSelectedScheduleSession(null);
	}
	
	/**
	 * Hide dialogs.
	 *
	 * @author mmacalupu
	 * Cargar variable para indicar que llegó al método action (No mostrar modal de requerido)
	 * Ocultar Dialogos Generale
	 */
	public void hideDialogs(){
		//JSFUtilities.hideComponent("frmMotive:dlgMotive");
		JSFUtilities.hideComponent("frmBlock:cnfBlock");
		JSFUtilities.hideComponent("frmUnblock:cnfUnblock");
		JSFUtilities.hideComponent("frmConfirm:cnfConfirm");
		JSFUtilities.hideComponent("frmReject:cnfReject");
		JSFUtilities.hideComponent(":mainForm:confirmScheduleMgmt");		
		JSFUtilities.hideGeneralDialogues();
	}
	
	/**
	 * Clone data model.
	 *
	 * @param list the list
	 * @return the list
	 */
	public static List<ScheduleDetail> cloneDataModel(List<ScheduleDetail> list){
		List<ScheduleDetail> listCloned = new ArrayList<ScheduleDetail>();
		for(Object object: list){
			ScheduleDetail detail = (ScheduleDetail)object;
			listCloned.add(detail.clone());			
		}
		return listCloned;
	}
	
	/**
	 * Action motive listener.
	 */
	public void actionMotiveListener(){
		try {
			
			if (this.getHistoricalState().getMotive() == -1) {
				JSFUtilities.showComponent("cnfMsgCustomValidation");
				showMessageOnDialog(null, null,
						PropertiesConstants.ERROR_MESSAGE_MOTIVE_REQUIRED, null);
				JSFUtilities.addContextMessage("frmMotive:idcbomotive", FacesMessage.SEVERITY_ERROR,PropertiesConstants.MOTIVE_REQUIRED,null);
				return;
			} 
			if(getHistoricalState().getMotive().equals(OtherMotiveType.SYS_SCHEDULE_BLOCK_OTHER_MOTIVE.getCode()) ||
					getHistoricalState().getMotive().equals(OtherMotiveType.SYS_SCHEDULE_UNBLOCK_OTHER_MOTIVE.getCode())){
				if(historicalState.getMotiveDescription()==null || historicalState.getMotiveDescription().trim().length() == 0){
	        		JSFUtilities.addContextMessage("frmMotive:idothermotive",FacesMessage.SEVERITY_ERROR,
	    					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_MOTIVE_REQUIRED),
	    					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_MOTIVE_REQUIRED));
	    			return;
	        	}
				
			}
			JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
			hideDialogs();
			if(isBlock()){
				Object[] data = {String.valueOf(scheduleSession.getIdSchedulePk())};				
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_CONFIRM, null, 
						scheduleSession.getSituation().equals(ScheduleSituationType.ACTIVE.getCode())
						? PropertiesConstants.SYSTEM_SCHEDULE_REQUEST_BLOCK_MESSAGE
						: PropertiesConstants.SYSTEM_SCHEDULE_BLOCK_MESSAGE, data);
				JSFUtilities.showComponent("frmBlock:cnfBlock");
				
			}else if(isUnBlock()){
				Object[] data = {String.valueOf(scheduleSession.getIdSchedulePk())};
				showMessageOnDialog(
						PropertiesConstants.LBL_CONFIRM_HEADER_UNBLOCK, null, 
						scheduleSession.getSituation().equals(ScheduleSituationType.ACTIVE.getCode())
						? PropertiesConstants.SYSTEM_SCHEDULE_REQUEST_UNBLOCK_MESSAGE
						: PropertiesConstants.SYSTEM_SCHEDULE_UNBLOCK_MESSAGE, data);
				JSFUtilities.showComponent("frmUnblock:cnfUnblock");
			}else if(isReject()){
				Object [] data = {String.valueOf(scheduleSession.getIdSchedulePk())};
				showMessageOnDialog(
						PropertiesConstants.LBL_CONFIRM_HEADER_REJECT, null, 
						PropertiesConstants.SYSTEM_SCHEDULE_REJECT_MESSAGE, data);
				JSFUtilities.showComponent("frmReject:cnfReject");
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	public void actionRejectMotiveListener(){
		try {
			
			isRejectOption = true;
			hideDialogs();
			
			Object[] data = {String.valueOf(scheduleSession.getIdSchedulePk())};
			String messageModal = "";
			if(isBlock()){
				if(scheduleSession.getState().equals(ScheduleStateType.CONFIRMED.getCode()) &&
					scheduleSession.getSituation().equals(ScheduleSituationType.PENDING_LOCK.getCode())){
					messageModal = PropertiesConstants.SYSTEM_SCHEDULE_REJECT_BLOCK_MESSAGE;
					JSFUtilities.showComponent("frmBlock:cnfBlock");
				}			
			}else if(isUnBlock()){
				if(scheduleSession.getState().equals(SystemProfileStateType.BLOCKED.getCode()) && 
						scheduleSession.getSituation().equals(SystemProfileSituationType.PENDING_UNLOCK.getCode())){
					messageModal = PropertiesConstants.SYSTEM_SCHEDULE_REJECT_UNBLOCK_MESSAGE;
					JSFUtilities.showComponent("frmUnblock:cnfUnblock");
				}
			}
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_CONFIRM,null,
					messageModal,data);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change selection motive listener.
	 */
	public void changeSelectionMotiveListener(){
		 JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);

		 if(isBlock()){
	        	if(OtherMotiveType.SYS_SCHEDULE_BLOCK_OTHER_MOTIVE.getCode().equals(historicalState.getMotive())){
	        		showOtherMotive = true;
	        	}else {
	    			showOtherMotive = false;
	    		}
	     }else if(isUnBlock()){
	        	if(OtherMotiveType.SYS_SCHEDULE_UNBLOCK_OTHER_MOTIVE.getCode().equals(historicalState.getMotive())){
	        		showOtherMotive = true;
	        	}else {
	    			showOtherMotive = false;
	    		}
	     }else if(isReject()){
	        	if(OtherMotiveType.SYS_SCHEDULE_REJECT_OTHER_MOTIVE.getCode().equals(historicalState.getMotive())){
	        		showOtherMotive = true;
	        	}else {
	    			showOtherMotive = false;
	    		}
	     }
	}
	
	/**
	 * To select all the days.
	 */
	public void selectAllListener(){
		if(isSelectAll()){
			scheduleDetailMon.setSelected(true);
			scheduleDetailTue.setSelected(true);
			scheduleDetailWed.setSelected(true);
			scheduleDetailThu.setSelected(true);
			scheduleDetailFri.setSelected(true);
			scheduleDetailSat.setSelected(true);
			scheduleDetailSun.setSelected(true);
		}else{
			scheduleDetailMon.setSelected(false);
			scheduleDetailTue.setSelected(false);
			scheduleDetailWed.setSelected(false);
			scheduleDetailThu.setSelected(false);
			scheduleDetailFri.setSelected(false);
			scheduleDetailSat.setSelected(false);
			scheduleDetailSun.setSelected(false);
		}
		loadTableInitialDate();
	}

	/**
	 * Gets the initial hour.
	 *
	 * @return the initial hour
	 */
	public Date getInitialHour() {
		return initialHour;
	}

	/**
	 * Sets the initial hour.
	 *
	 * @param initialHour the new initial hour
	 */
	public void setInitialHour(Date initialHour) {
		if(initialHour != null && finalHour != null && initialHour.after(finalHour)){
			finalHour = null;
		}
		this.initialHour = initialHour;
	}

	/**
	 * Gets the final hour.
	 *
	 * @return the final hour
	 */
	public Date getFinalHour() {
		return finalHour;
	}

	/**
	 * Sets the final hour.
	 *
	 * @param finalHour the new final hour
	 */
	public void setFinalHour(Date finalHour) {
		this.finalHour = finalHour;
	}
	
	/**
	 * Format hour from date.
	 *
	 * @param hour the hour
	 * @return the string
	 */
	public String formatHourFromDate(Date hour){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(hour);
		calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY));
		calendar.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE));
		int minuteInitialDate = calendar.get(Calendar.MINUTE);
		int hourInitialDate = calendar.get(Calendar.HOUR_OF_DAY);
		String time = String.format("%02d:%02d", hourInitialDate, minuteInitialDate);
		return time;
	}
	
	/**
	 * For Block Descripcion.
	 *
	 * @return the motive descripcion
	 */
	public String getMotiveDescripcion(){
	    String desc = "";
	    if(scheduleSession.getBlockMotive() != null){
			if(lstBlockMotives != null && lstBlockMotives.size() > 0 ){
				for(int i=0;i<lstBlockMotives.size();i++){
					if(scheduleSession.getBlockMotive().compareTo(lstBlockMotives.get(i).getIdParameterPk()) == 0){
							desc=lstBlockMotives.get(i).getName();
						}
					}
				}
			}
		return desc;
	}	
		
		/**
		 * For unBlock Descripcion.
		 *
		 * @return the motiveun block descripcion
		 */
	public String getMotiveunBlockDescripcion(){
	    String desc = "";
	    if(scheduleSession.getUnblockMotive() != null){
			if(lstUnBlockMotives != null && lstUnBlockMotives.size() > 0 ){
				for(int i=0;i<lstUnBlockMotives.size();i++){
					if(scheduleSession.getUnblockMotive().compareTo(lstUnBlockMotives.get(i).getIdParameterPk()) == 0){
							desc=lstUnBlockMotives.get(i).getName();
						}
					}
				}
			}
		return desc;
	}
	
	
	public String getMotiveRejectDescripcion(){
	    String desc = "";
	    if(scheduleSession.getRejectMotive() != null){
			if(lstRejectMotives != null && lstRejectMotives.size() > 0 ){
				for(int i=0;i<lstRejectMotives.size();i++){
					if(scheduleSession.getRejectMotive().compareTo(lstRejectMotives.get(i).getIdParameterPk()) == 0){
							desc=lstRejectMotives.get(i).getName();
						}
					}
				}
			}
		return desc;
	}

		/**
		 * Gets the system selected.
		 *
		 * @return the systemSelected
		 */
		public Integer getSystemSelected() {
			return systemSelected;
		}

		/**
		 * Sets the system selected.
		 *
		 * @param systemSelected the systemSelected to set
		 */
		public void setSystemSelected(Integer systemSelected) {
			this.systemSelected = systemSelected;
		}

		/**
		 * Gets the tab initial hour.
		 *
		 * @return the tabInitialHour
		 */
		public Date getTabInitialHour() {
			return tabInitialHour;
		}

		/**
		 * Sets the tab initial hour.
		 *
		 * @param tabInitialHour the tabInitialHour to set
		 */
		public void setTabInitialHour(Date tabInitialHour) {
			this.tabInitialHour = tabInitialHour;
		}

		/**
		 * Gets the tab final hour.
		 *
		 * @return the tabFinalHour
		 */
		public Date getTabFinalHour() {
			return tabFinalHour;
		}

		/**
		 * Sets the tab final hour.
		 *
		 * @param tabFinalHour the tabFinalHour to set
		 */
		public void setTabFinalHour(Date tabFinalHour) {
			this.tabFinalHour = tabFinalHour;
		}

		/**
		 * Gets the schedule detail mon.
		 *
		 * @return the scheduleDetailMon
		 */
		public ScheduleDetail getScheduleDetailMon() {
			return scheduleDetailMon;
		}

		/**
		 * Sets the schedule detail mon.
		 *
		 * @param scheduleDetailMon the scheduleDetailMon to set
		 */
		public void setScheduleDetailMon(ScheduleDetail scheduleDetailMon) {
			this.scheduleDetailMon = scheduleDetailMon;
		}

		/**
		 * Gets the schedule detail tue.
		 *
		 * @return the scheduleDetailTue
		 */
		public ScheduleDetail getScheduleDetailTue() {
			return scheduleDetailTue;
		}

		/**
		 * Sets the schedule detail tue.
		 *
		 * @param scheduleDetailTue the scheduleDetailTue to set
		 */
		public void setScheduleDetailTue(ScheduleDetail scheduleDetailTue) {
			this.scheduleDetailTue = scheduleDetailTue;
		}

		/**
		 * Gets the schedule detail wed.
		 *
		 * @return the scheduleDetailWed
		 */
		public ScheduleDetail getScheduleDetailWed() {
			return scheduleDetailWed;
		}

		/**
		 * Sets the schedule detail wed.
		 *
		 * @param scheduleDetailWed the scheduleDetailWed to set
		 */
		public void setScheduleDetailWed(ScheduleDetail scheduleDetailWed) {
			this.scheduleDetailWed = scheduleDetailWed;
		}

		/**
		 * Gets the schedule detail thu.
		 *
		 * @return the scheduleDetailThu
		 */
		public ScheduleDetail getScheduleDetailThu() {
			return scheduleDetailThu;
		}

		/**
		 * Sets the schedule detail thu.
		 *
		 * @param scheduleDetailThu the scheduleDetailThu to set
		 */
		public void setScheduleDetailThu(ScheduleDetail scheduleDetailThu) {
			this.scheduleDetailThu = scheduleDetailThu;
		}

		/**
		 * Gets the schedule detail fri.
		 *
		 * @return the scheduleDetailFri
		 */
		public ScheduleDetail getScheduleDetailFri() {
			return scheduleDetailFri;
		}

		/**
		 * Sets the schedule detail fri.
		 *
		 * @param scheduleDetailFri the scheduleDetailFri to set
		 */
		public void setScheduleDetailFri(ScheduleDetail scheduleDetailFri) {
			this.scheduleDetailFri = scheduleDetailFri;
		}
		
		/**
		 * hiding the confirmation dialog.
		 */
		public void institutionChanged(){
			JSFUtilities.hideComponent(":mainForm:confirmScheduleMgmt");
		}
		
		/**
		 * click on  limpair button, clear the all search results.
		 *
		 * @return the string
		 */
//		public void clearResults(){			
//			if(this.scheduleFilter.getScheduleType()!=null && this.scheduleFilter.getScheduleType().equals(ScheduleType.INSTITUTIONS.getCode())){
//				try{
//					this.getUserAccountTypeList().addAll(userAccountTypeListTemp);
//				}catch(Exception ex){
//					excepcion.fire(new ExceptionToCatch(ex));
//				}
//			}
//			userAccountTypeSelected=-1;
//			institutionsTypeList.clear();
//			if(Validations.validateIsNotNullAndPositive(userAccountTypeSelected)){
//				institutionsTypeList.clear();
//				institutionsTypeList.addAll(institutionsTypeListTemp);
//				if(this.userAccountTypeSelected.equals(UserAccountType.EXTERNAL.getCode())){
//					for (Parameter param : institutionsTypeListTemp) {
//						if(InstitutionType.CEVALDOM.getCode().equals(param.getIdParameterPk())){
//							institutionsTypeList.remove(param);
//						}
//					}
//				}else if(this.userAccountTypeSelected.equals(UserAccountType.INTERNAL.getCode())){
//					for (Parameter param : institutionsTypeListTemp) {
//						if(!InstitutionType.CEVALDOM.getCode().equals(param.getIdParameterPk())){
//							institutionsTypeList.remove(param);
//						}
//					}
//				}
//	   }
//			
//	}
//				if(this.userAccountTypeSelected.equals(UserAccountType.EXTERNAL.getCode())){
//					this.getInstitutionsTypeList().addAll(institutionsTypeListTemp);
//					for(Parameter param:institutionsTypeListTemp){
//						this.getInstitutionsTypeList().remove(param);
//					}
//					//this.getInstitutionsTypeList().remove(InstitutionType.CEVALDOM);
//				}else if(this.userAccountTypeSelected.equals(UserAccountType.INTERNAL.getCode())){
//					//this.getInstitutionsTypeList().add(InstitutionType.CEVALDOM);
//				}
		/**
		 * search page loading time clear the all results
		 */
//		public void clearRegisterValues(){
//			if(getOperationType()!=null && (getOperationType().equals(ViewOperationsType.REGISTER.getCode()) || 
//					getOperationType().equals(ViewOperationsType.MODIFY.getCode()) )){
//				cleanVariablesListener();
//				setOperationType(null);
//			}
//		}
		
		/**
		 * Going to search page
		 * @return The String type
		 */
		public String backSuccSearch(){
			return "search";
		}
		
		
		/**
		 * Evaluate Search page user account type listener.
		 */
		public void evaluateSearchUserAccountTypeListener(){
			hideDialogs(); 
			this.getSearchInstitutionsTypeList().removeAll(this.getSearchInstitutionsTypeList());
			//this variable is not related search page
		//	this.setInstitutionSecuritySelected(null);
			searchInstitutionsTypeList.addAll(institutionsTypeListTemp);
			if(UserAccountType.EXTERNAL.getCode().equals(this.searchUserAccountTypeSelected)){
				for (Parameter param : institutionsTypeListTemp) {
					if(InstitutionType.DEPOSITARY.getCode().equals(param.getIdParameterPk())){
						searchInstitutionsTypeList.remove(param);	
						//this variable is not related search page
	                   // cevaldomInstitution = false;
					}
				}
			}else if(UserAccountType.INTERNAL.getCode().equals(this.searchUserAccountTypeSelected)){
				for (Parameter param : institutionsTypeListTemp) {
					if(!InstitutionType.DEPOSITARY.getCode().equals(param.getIdParameterPk())){
						searchInstitutionsTypeList.remove(param);
						//this variable is not related search page
	                   // cevaldomInstitution = true;
					}
				}
				//this variable is not related search page
				//this.setInstitutionSecuritySelected(InstitutionType.CEVALDOM.getCode());
				
				
				//Not selected any UserAccount Type
			}else{
				if(Validations.validateIsNotNull(searchInstitutionsTypeList)){
				searchInstitutionsTypeList.clear();
				searchScheduleFilter.setInstitutionType(null);
				}
			}
//			if(this.searchUserAccountTypeSelected.equals(UserAccountType.EXTERNAL.getCode())){
//				this.getInstitutionsTypeList().addAll(institutionsTypeListTemp);
//				this.getInstitutionsTypeList().remove(InstitutionType.CEVALDOM);
//				cevaldomInstitution = false; 
//			}else if(this.searchUserAccountTypeSelected.equals(UserAccountType.INTERNAL.getCode())){
//				//this.getInstitutionsTypeList().add(InstitutionType.CEVALDOM);
//				this.setInstitutionSecuritySelected(InstitutionType.CEVALDOM.getCode());
//				cevaldomInstitution = true;
//			}
		}
		
		
		/**
		 * Verify required data filled for the search operation
		 *  
		 * @return boolean type
		 */
		private boolean validationBeforeSearchOperation(){
			JSFUtilities.hideGeneralDialogues();			
			boolean scheduleFlag = false;
			boolean userAccFlag = false;			
			boolean validReultFlag = false;			
			//Check Schedule Type selected or not
			if(Validations.validateIsNullOrNotPositive(searchScheduleFilter.getScheduleType())) {
				JSFUtilities.addContextMessage("mainForm:scheduleTypeSelect",FacesMessage.SEVERITY_ERROR,PropertiesUtilities.getMessage(PropertiesConstants.REQUIRED_SCHEDULETYPE),PropertiesUtilities.getMessage(PropertiesConstants.REQUIRED_SCHEDULETYPE));
				//JSFUtilities.showRequiredDialog();
				JSFUtilities.showSearchRequiredDialog();
				scheduleFlag = true;			
			}						
			//Check Client Type selected or not			
			if(Validations.validateIsNotNull(searchUserAccountTypeList) && searchUserAccountTypeList.size()>GeneralConstants.ONE_VALUE_INTEGER.intValue()) {
				if(Validations.validateIsNullOrNotPositive(searchUserAccountTypeSelected)) {
				JSFUtilities.addContextMessage("mainForm:idCboUserAccType",FacesMessage.SEVERITY_ERROR,PropertiesUtilities.getMessage(PropertiesConstants.REQUIRED_CLIENTTYPE),PropertiesUtilities.getMessage(PropertiesConstants.REQUIRED_CLIENTTYPE));
				//JSFUtilities.showRequiredDialog();
				JSFUtilities.showSearchRequiredDialog();
				userAccFlag = true;		
				}
			}		
			
			
			if(scheduleFlag || userAccFlag ){	
				FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("searchValidation",Boolean.TRUE);
				validReultFlag = true;						
			}
			return validReultFlag;
			
		}
		
		/**
		 * Verify required data filled for the register operation
		 *  
		 * @return boolean type
		 */
		private boolean validationBeforeRegisterhOperation(){
			
			boolean scheduleFlag = false;
			boolean userAccFlag = false;		
			boolean institutionFlag = false;		
			boolean validReultFlag = false;			
			//Check Schedule Type selected or not
			if(Validations.validateIsNullOrNotPositive(scheduleSession.getScheduleType())) {
				JSFUtilities.addContextMessage("mainForm:selectScheduleType",FacesMessage.SEVERITY_ERROR,PropertiesUtilities.getMessage(PropertiesConstants.REQUIRED_SCHEDULETYPE),PropertiesUtilities.getMessage(PropertiesConstants.REQUIRED_SCHEDULETYPE));
				JSFUtilities.showRequiredDialog();
				scheduleFlag = true;			
			}						
			//Check Client Type selected or not			
			if(Validations.validateIsNotNull(userAccountTypeList) && userAccountTypeList.size()>GeneralConstants.ONE_VALUE_INTEGER.intValue()) {
				if(Validations.validateIsNullOrNotPositive(userAccountTypeSelected)) {
				JSFUtilities.addContextMessage("mainForm:idCboUserAccType",FacesMessage.SEVERITY_ERROR,PropertiesUtilities.getMessage(PropertiesConstants.REQUIRED_CLIENTTYPE),PropertiesUtilities.getMessage(PropertiesConstants.REQUIRED_CLIENTTYPE));
				JSFUtilities.showRequiredDialog();
				userAccFlag = true;		
				}
			}	
			
			//Check Institution Type selected or not			
			if(Validations.validateIsNotNull(institutionsTypeList) && institutionsTypeList.size()>GeneralConstants.ONE_VALUE_INTEGER.intValue()) {
				if(Validations.validateIsNullOrNotPositive(institutionSecuritySelected)) {
				JSFUtilities.addContextMessage("mainForm:idCboInstitutionType",FacesMessage.SEVERITY_ERROR,PropertiesUtilities.getMessage(PropertiesConstants.REQUIRED_INSTITUTETYPE),PropertiesUtilities.getMessage(PropertiesConstants.REQUIRED_INSTITUTETYPE));
				JSFUtilities.showRequiredDialog();
				institutionFlag = true;		
				}
			}	
			
			
			if(scheduleFlag || userAccFlag || institutionFlag){
				validReultFlag = true;						
			}
			return validReultFlag;
			
		}

		/**
		 * Gets the schedule detail sat.
		 *
		 * @return the scheduleDetailSat
		 */
		public ScheduleDetail getScheduleDetailSat() {
			return scheduleDetailSat;
		}

		/**
		 * Sets the schedule detail sat.
		 *
		 * @param scheduleDetailSat the scheduleDetailSat to set
		 */
		public void setScheduleDetailSat(ScheduleDetail scheduleDetailSat) {
			this.scheduleDetailSat = scheduleDetailSat;
		}

		/**
		 * Gets the schedule detail sun.
		 *
		 * @return the scheduleDetailSun
		 */
		public ScheduleDetail getScheduleDetailSun() {
			return scheduleDetailSun;
		}

		/**
		 * Sets the schedule detail sun.
		 *
		 * @param scheduleDetailSun the scheduleDetailSun to set
		 */
		public void setScheduleDetailSun(ScheduleDetail scheduleDetailSun) {
			this.scheduleDetailSun = scheduleDetailSun;
		}

		/**
		 * Checks if is select all.
		 *
		 * @return the selectAll
		 */
		public boolean isSelectAll() {
			return selectAll;
		}

		/**
		 * Sets the select all.
		 *
		 * @param selectAll the selectAll to set
		 */
		public void setSelectAll(boolean selectAll) {
			this.selectAll = selectAll;
		}

		/**
		 * Checks if is confirm block or unblock.
		 *
		 * @return true, if is confirm block or unblock
		 */
		public boolean isConfirmBlockOrUnblock() {
			return isConfirmBlockOrUnblock;
		}

		/**
		 * Sets the confirm block or unblock.
		 *
		 * @param isConfirmBlockOrUnblock the new confirm block or unblock
		 */
		public void setConfirmBlockOrUnblock(boolean isConfirmBlockOrUnblock) {
			this.isConfirmBlockOrUnblock = isConfirmBlockOrUnblock;
		}

		public boolean isRejectOption() {
			return isRejectOption;
		}

		public void setRejectOption(boolean isRejectOption) {
			this.isRejectOption = isRejectOption;
		}

		
}