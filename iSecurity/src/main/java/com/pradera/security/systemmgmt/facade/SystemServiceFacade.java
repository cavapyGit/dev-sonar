package com.pradera.security.systemmgmt.facade;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.audit.service.HistoricalStateInterceptor;
import com.pradera.security.model.CatalogueLanguage;
import com.pradera.security.model.ExceptionUserPrivilege;
import com.pradera.security.model.Holiday;
import com.pradera.security.model.OptionPrivilege;
import com.pradera.security.model.Parameter;
import com.pradera.security.model.ProfilePrivilege;
import com.pradera.security.model.ProfilePrivilegeRequest;
import com.pradera.security.model.Schedule;
import com.pradera.security.model.ScheduleDetail;
import com.pradera.security.model.ScheduleException;
import com.pradera.security.model.ScheduleExceptionDetail;
import com.pradera.security.model.System;
import com.pradera.security.model.SystemOption;
import com.pradera.security.model.SystemProfile;
import com.pradera.security.model.SystemProfileRequest;
import com.pradera.security.model.TicketsDatamart;
import com.pradera.security.model.UserAccount;
import com.pradera.security.model.UserPrivilegeDetail;
import com.pradera.security.model.type.OptionSituationType;
import com.pradera.security.model.type.OptionStateType;
import com.pradera.security.model.type.OptionType;
import com.pradera.security.model.type.ProfilePrivilegeStateType;
import com.pradera.security.model.type.ScheduleExceptionDetailStateType;
import com.pradera.security.model.type.ScheduleExceptionStateType;
import com.pradera.security.model.type.ScheduleSituationType;
import com.pradera.security.model.type.ScheduleStateType;
import com.pradera.security.model.type.SystemProfileRequestStateType;
import com.pradera.security.model.type.SystemProfileSituationType;
import com.pradera.security.model.type.SystemProfileStateType;
import com.pradera.security.model.type.SystemSituationType;
import com.pradera.security.model.type.SystemStateType;
import com.pradera.security.model.type.UserExceptionStateType;
import com.pradera.security.systemmgmt.service.FileUploadSecServiceBean;
import com.pradera.security.systemmgmt.service.HolidayServiceBean;
import com.pradera.security.systemmgmt.service.ParameterSecurityServiceBean;
import com.pradera.security.systemmgmt.service.ScheduleExceptionServiceBean;
import com.pradera.security.systemmgmt.service.ScheduleServiceBean;
import com.pradera.security.systemmgmt.service.SystemOptionServiceBean;
import com.pradera.security.systemmgmt.service.SystemProfileServiceBean;
import com.pradera.security.systemmgmt.service.SystemServiceBean;
import com.pradera.security.systemmgmt.service.UserExceptionServiceBean;
import com.pradera.security.systemmgmt.view.OptionCatalogNodeBean;
import com.pradera.security.systemmgmt.view.filters.HolidayFilter;
import com.pradera.security.systemmgmt.view.filters.PrivilegeOptionFilter;
import com.pradera.security.systemmgmt.view.filters.ScheduleExceptionFilter;
import com.pradera.security.systemmgmt.view.filters.ScheduleFilter;
import com.pradera.security.systemmgmt.view.filters.SystemFilter;
import com.pradera.security.systemmgmt.view.filters.SystemProfileFilter;
import com.pradera.security.systemmgmt.view.filters.UserExceptionFilter;
import com.pradera.security.utils.view.PropertiesConstants;
import com.pradera.security.utils.view.RestServiceNotification;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Class SystemServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/12/2012
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class SystemServiceFacade{
    
    /** The crud security service bean. */
   /* @EJB
    ICrudServiceBean crudSecurityServiceBean;*/
    
    
    /** The system service bean. */
    @EJB
    SystemServiceBean systemServiceBean;
    
    /** The schedule service bean. */
    @EJB
    ScheduleServiceBean scheduleServiceBean;
    
    /** The schedule exception service bean. */
    @EJB
    ScheduleExceptionServiceBean scheduleExceptionServiceBean;
    
    /** The system option service bean. */
    @EJB
    SystemOptionServiceBean systemOptionServiceBean;
    
    /** The system profile service bean. */
    @EJB
    SystemProfileServiceBean systemProfileServiceBean;
    
    /** The holiday service bean. */
    @EJB
    HolidayServiceBean holidayServiceBean;
    
    /** The parameter service bean. */
    @EJB
    ParameterSecurityServiceBean parameterSecurityServiceBean;
    
    /** The user exception service bean. */
    @EJB
    UserExceptionServiceBean userExceptionServiceBean;
    
    /**  The transaction repository bean. */
    @Resource
    private TransactionSynchronizationRegistry transactionRegistry;
    
    /** The scheduler service facade. */
    @EJB
	private SchedulerServiceFacade schedulerServiceFacade;	
    
    /** The rest service notification. */
    @Inject
    private RestServiceNotification restServiceNotification;
    
    @EJB
    FileUploadSecServiceBean fileUploadServiceBean;
    
    /**
     * Register schedule service facade.
     *
     * @param schedule the schedule
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean registerScheduleServiceFacade(Schedule schedule) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister()); 

		scheduleServiceBean.registerScheduleInstitutionServiceBean(schedule);
		
		//Se envia email y notificacion
		Object[] parameters = {schedule.getDescription(),schedule.getIdSchedulePk()};
		restServiceNotification.sendNotification(loggerUser,BusinessProcessType.SYSTEM_ACCESS_TIMES_REGISTER.getCode(),parameters);
		
    	return true;
    }

    /**
     * Modify schedule service facade.
     *
     * @param schedule the schedule
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    @Interceptors(HistoricalStateInterceptor.class)
    public boolean confirmScheduleServiceFacade(Schedule schedule) throws ServiceException {
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
        scheduleServiceBean.modifyScheduleInstitutionServiceBean(schedule);
        //Se envia email y notificacion
  		Object[] parameters = {schedule.getDescription(),schedule.getIdSchedulePk()};
  		restServiceNotification.sendNotification(loggerUser,BusinessProcessType.SYSTEM_ACCESS_TIMES_CONFIRM.getCode(),parameters);
      		
  		return true;        
    }
    
    /**
     * Modify schedule service facade.
     *
     * @param schedule the schedule
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    @Interceptors(HistoricalStateInterceptor.class)
    public boolean modifyScheduleServiceFacade(Schedule schedule) throws ServiceException {
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
    	//delete old schedule
    	scheduleServiceBean.deleteScheduleDetailServiceFacade(schedule.getIdSchedulePk());
    	//register modifications
        scheduleServiceBean.modifyScheduleInstitutionServiceBean(schedule);
        //Se envia email y notificacion
  		Object[] parameters = {schedule.getDescription(),schedule.getIdSchedulePk()};
  		restServiceNotification.sendNotification(loggerUser,BusinessProcessType.SYSTEM_ACCESS_TIMES_MODIFICATION.getCode(),parameters);
      		
  		return true;        
    }
    
    /**
     * Update schedule state service facade.
     *
     * @param schedule the schedule
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    @Interceptors(HistoricalStateInterceptor.class)
    public boolean updateScheduleStateServiceFacade(Schedule schedule) throws ServiceException {
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	Long businessProcess = null;
 
		if (ScheduleStateType.CONFIRMED.getCode().equals(schedule.getState()) 
			&& schedule.getSituation().equals(ScheduleSituationType.PENDING_LOCK.getCode())){
			 
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeBlock());
			businessProcess = BusinessProcessType.SYSTEM_ACCESS_TIMES_REQUEST_BLOCK.getCode();
		}
		if (ScheduleStateType.BLOCKED.getCode().equals(schedule.getState()) 
			&& schedule.getSituation().equals(ScheduleSituationType.PENDING_UNLOCK.getCode())){
			 
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeUnblock());
			businessProcess = BusinessProcessType.SYSTEM_ACCESS_TIMES_REQUEST_UNBLOCK.getCode();
		}
		if(ScheduleStateType.CONFIRMED.getCode().equals(schedule.getState()) 
			&& schedule.getSituation().equals(ScheduleSituationType.ACTIVE.getCode())){
			 
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeUnblock());
			businessProcess = BusinessProcessType.SYSTEM_ACCESS_TIMES_UNBLOCK.getCode();
		}
		if(ScheduleStateType.BLOCKED.getCode().equals(schedule.getState()) 
			 && schedule.getSituation().equals(ScheduleSituationType.ACTIVE.getCode())){		
			
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeBlock());
			businessProcess = BusinessProcessType.SYSTEM_ACCESS_TIMES_BLOCK.getCode();
		}
		if(ScheduleStateType.REJECT.getCode().equals(schedule.getState())){			 
			
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
			businessProcess = BusinessProcessType.SYSTEM_ACCESS_TIMES_REJECT.getCode();
			 
		}
		
		if(businessProcess != null){			
	        scheduleServiceBean.modifyScheduleInstitutionServiceBean(schedule);
	        
	        //Se envia email y notificacion
	  		Object[] parameters = {schedule.getDescription(),schedule.getIdSchedulePk()};
	  		restServiceNotification.sendNotification(loggerUser,businessProcess,parameters);
		}else{
			return false;
		}
		
  		return true;        
    }
    
    /**
     * Register schedule exception service facade.
     *
     * @param scheduleException the schedule exception
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean registerScheduleExceptionServiceFacade(ScheduleException scheduleException) throws ServiceException {
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
   		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister()); 

    	scheduleException = scheduleExceptionServiceBean.create(scheduleException);
    	schedulerServiceFacade.updateTimer(scheduleException);
    	
    	//Se envia email y notificacion
  		Object[] parameters = {scheduleException.getIdScheduleExceptionPk()};
  		restServiceNotification.sendNotification(loggerUser,BusinessProcessType.EXCEPTION_TIMES_SYSTEM_ACCESS_REGISTRATION.getCode(),parameters);
    	    	
        return true;
    }
    
    /**
     * Modify schedule exception service facade.
     *
     * @param scheduleException the schedule exception
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean modifyScheduleExceptionServiceFacade(ScheduleException scheduleException) throws ServiceException {
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
    	if(Validations.validateIsNotNullAndNotEmpty(scheduleException.getScheduleExceptionDetails().get(0).getIdScheduleExeptionDetailPk())){
    		scheduleException = scheduleExceptionServiceBean.update(scheduleException, loggerUser);
    	}else{
    		scheduleException = scheduleExceptionServiceBean.create(scheduleException);
    	}
    	schedulerServiceFacade.updateTimer(scheduleException);
    	
    	Long businessProcess = null;
    	if(ScheduleExceptionStateType.REVOCATE.getCode().equals(scheduleException.getState())){
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeDelete());
    		businessProcess = BusinessProcessType.EXCEPTION_TIMES_SYSTEM_ACCESS_AUTOMATIC_DELETE.getCode(); 
    	}else{
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
    		businessProcess = BusinessProcessType.EXCEPTION_TIMES_SYSTEM_ACCESS_MODIFICATION.getCode();
    	}
    	
    	//Se envia email y notificacion
  		Object[] parameters = {scheduleException.getIdScheduleExceptionPk()};
  		restServiceNotification.sendNotification(loggerUser,businessProcess,parameters);
    	    	
        return true;
    }

    /**
     * Update schedule exception state service facade.
     *
     * @param scheduleException the schedule exception
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean updateScheduleExceptionStateServiceFacade(ScheduleException scheduleException) throws ServiceException {
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	Long businessProcess = null;
    	
    	if(ScheduleExceptionStateType.CONFIRMED.getCode().equals(scheduleException.getState())){
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
    		businessProcess = BusinessProcessType.EXCEPTION_TIMES_SYSTEM_ACCESS_CONFIRM.getCode();
    	}    	
    	if(ScheduleExceptionStateType.DELETED.getCode().equals(scheduleException.getState())){    		
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeDelete());
    		businessProcess = BusinessProcessType.EXCEPTION_TIMES_SYSTEM_ACCESS_MANUAL_DELETE.getCode();    		
    	}
    	if(ScheduleExceptionStateType.REJECT.getCode().equals(scheduleException.getState())){    		
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
    		businessProcess = BusinessProcessType.EXCEPTION_TIMES_SYSTEM_ACCESS_REJECT.getCode();
    	}
    	
    	if(businessProcess != null){
    		scheduleExceptionServiceBean.updateOnCascade(scheduleException);
    		schedulerServiceFacade.deleteTimer(scheduleException);
    		
    		//Se envia email y notificacion
      		Object[] parameters = {scheduleException.getIdScheduleExceptionPk()};
      		restServiceNotification.sendNotification(loggerUser,businessProcess,parameters);
        	  
    	}else{
    		return false;
    	}
        return true;
    }

    /**
     * Register system service facade.
     *
     * @param system the system
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean registerSystemServiceFacade(System system) throws ServiceException {
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
   		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister()); 
		systemServiceBean.create(system);
		//Se envia email y notificacion
		Object[] parameters = {system.getName(),system.getIdSystemPk()};
		restServiceNotification.sendNotification(loggerUser,BusinessProcessType.SYSTEM_REGISTRATION.getCode(),parameters);
        return true;
    }

    /**
     * Modify system service facade.
     *
     * @param system the system
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean modifySystemServiceFacade(System system) throws ServiceException {
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
    	systemServiceBean.updateSystemServiceBean(system,loggerUser);
    	//Se envia email y notificacion
		Object[] parameters = {system.getName()};
		restServiceNotification.sendNotification(loggerUser,BusinessProcessType.SYSTEM_MODIFICATION.getCode(),parameters);
        return true;
    }

    /**
     * Register system profile service facade.
     *
     * @param systemProfile the system profile
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean registerSystemProfileServiceFacade(SystemProfile systemProfile) throws ServiceException {
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
   		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

        systemProfileServiceBean.createOnCascade(systemProfile);
        
        //Se envia email y notificacion
      	Object[] parameters = {systemProfile.getName()};
      	restServiceNotification.sendNotification(loggerUser,BusinessProcessType.SYSTEM_PROFILE_REGISTRATION.getCode(),parameters);

        return true;
    }
    /**
     * Modify system profile service facade.
     *
     * @param systemProfile the system profile
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean modifySystemProfileServiceFacade(SystemProfile systemProfile) throws ServiceException {
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
        systemProfileServiceBean.update(systemProfile);
        //Se envia email y notificacion
      	Object[] parameters = {systemProfile.getName()};
      	restServiceNotification.sendNotification(loggerUser,BusinessProcessType.SYSTEM_PROFILE_MODIFICATION.getCode(),parameters);

      	return true;
    }

    /**
     * Update system profile state service facade.
     *
     * @param systemProfile the system profile
     * @return true, if successful
     * @throws ServiceException the service exception
     */
	@Interceptors(HistoricalStateInterceptor.class)
    public boolean updateSystemProfileStateServiceFacade(SystemProfile systemProfile) throws ServiceException {
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	Long businessProcess = null;
    	if(SystemProfileStateType.CONFIRMED.getCode().equals(systemProfile.getState())
			&& SystemProfileSituationType.PENDING_LOCK.getCode().equals(systemProfile.getSituation())){
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeBlock());
    		businessProcess = BusinessProcessType.SYSTEM_PROFILE_REQUEST_BLOCK.getCode();
    	} else if(SystemProfileStateType.CONFIRMED.getCode().equals(systemProfile.getState())
    			&& SystemProfileSituationType.PENDING_DELETE.getCode().equals(systemProfile.getSituation())){
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeDelete());
    	} else 	if(SystemProfileStateType.BLOCKED.getCode().equals(systemProfile.getState())
    			&& SystemProfileSituationType.ACTIVE.getCode().equals(systemProfile.getSituation())){
        		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeBlock());
        		businessProcess = BusinessProcessType.SYSTEM_PROFILE_BLOCK.getCode();
        } else if(SystemProfileStateType.BLOCKED.getCode().equals(systemProfile.getState())
			&& SystemProfileSituationType.PENDING_UNLOCK.getCode().equals(systemProfile.getSituation())){
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeUnblock());
    		businessProcess = BusinessProcessType.SYSTEM_PROFILE_REQUEST_UNBLOCK.getCode();
    	} else if(SystemProfileStateType.CONFIRMED.getCode().equals(systemProfile.getState())
    			&& SystemProfileSituationType.ACTIVE.getCode().equals(systemProfile.getSituation()) && !systemProfile.isRejectProfile()){
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeUnblock());
    		businessProcess = BusinessProcessType.SYSTEM_PROFILE_UNBLOCK.getCode();
    	} else if(SystemProfileStateType.CONFIRMED.getCode().equals(systemProfile.getState())
    			&& SystemProfileSituationType.ACTIVE.getCode().equals(systemProfile.getSituation()) && systemProfile.isRejectProfile()){
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRejectDelete());
    	} else if(SystemProfileStateType.REGISTERED.getCode().equals(systemProfile.getState())
    			&& SystemProfileSituationType.PENDING_REJECTED.getCode().equals(systemProfile.getSituation())){
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
    		businessProcess = BusinessProcessType.SYSTEM_PROFILE_REJECT_REGISTRATION.getCode();
    	} else if(SystemProfileStateType.REGISTERED.getCode().equals(systemProfile.getState())
    			&& systemProfile.getSituation() == null){
    		//rejected the rejected
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
    	} else if(SystemProfileStateType.REJECTED.getCode().equals(systemProfile.getState())
    			&& SystemProfileSituationType.ACTIVE.getCode().equals(systemProfile.getSituation())){
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
    		businessProcess = BusinessProcessType.SYSTEM_PROFILE_REJECT_REGISTRATION.getCode();
    	}  else if(SystemProfileStateType.DELETED.getCode().equals(systemProfile.getState())
    			&& SystemProfileSituationType.ACTIVE.getCode().equals(systemProfile.getSituation())){
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirmDelete());
    	}
    	loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
    	systemProfileServiceBean.updateSystemProfileStateServiceBean(systemProfile, loggerUser);
    	if(businessProcess != null){
    		//Se envia email y notificacion
          	Object[] parameters = {systemProfile.getName()};
          	restServiceNotification.sendNotification(loggerUser,businessProcess,parameters);
    	}
        return true;
    }
	
	/**
	 * Confirm system profile service facade.
	 *
	 * @param systemProfile the system profile
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@Interceptors(HistoricalStateInterceptor.class)
    public boolean confirmSystemProfileServiceFacade(SystemProfile systemProfile) throws ServiceException {
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
    	loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
    	systemProfileServiceBean.updateSystemProfileStateServiceBean(systemProfile, loggerUser);
		//Se envia email y notificacion
      	Object[] parameters = {systemProfile.getName()};
      	restServiceNotification.sendNotification(loggerUser,BusinessProcessType.SYSTEM_PROFILE_CONFIRM_REGISTRATION.getCode(),parameters);

        return true;
    }

    /**
     * Register system option service facade.
     *
     * @param opcionSistema the opcion sistema
     * @param systemOptionName the system option name
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean registerSystemOptionServiceFacade(SystemOption opcionSistema, String systemOptionName) throws ServiceException,  IOException {
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister()); 
	   
        systemOptionServiceBean.createOnCascade(opcionSistema);
        
		//Se envia email y notificacion
		Object[] parameters = {systemOptionName, opcionSistema.getSystem().getName()};
		restServiceNotification.sendNotification(loggerUser,BusinessProcessType.SYSTEM_OPTION_REGISTRATION.getCode(),parameters);
		
		if(opcionSistema.getDocumentFile()!= null) {
			InputStream inputStream = new ByteArrayInputStream(opcionSistema.getDocumentFile());
			fileUploadServiceBean.processFileUploadComponent(opcionSistema.getDocumentFilePath(), opcionSistema.getDocumentFileName(), inputStream);
		}
		
        return true;
    }

    /**
     * Confirm system option.
     *
     * @param systemOption the system option
     * @param systemOptionName the system option name
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean confirmSystemOption(SystemOption systemOption,String systemOptionName) throws ServiceException {
     	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
	  
        systemOptionServiceBean.update(systemOption);
        
        //Se envia email y notificacion
        Object[] parameters = {systemOptionName,systemOption.getSystem().getName()};
		restServiceNotification.sendNotification(loggerUser,BusinessProcessType.SYSTEM_OPTION_CONFIRM_REGISTRATION.getCode(),parameters);

        return true;
    }

    /**
     * Reject system option.
     *
     * @param auxSystemOption the aux system option
     * @param auxEventNode the aux event node
     * @param objNodoOpcionCatalogoBean the obj nodo opcion catalogo bean
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public String rejectSystemOption(SystemOption auxSystemOption, OptionCatalogNodeBean auxEventNode, OptionCatalogNodeBean objNodoOpcionCatalogoBean) 
    								throws ServiceException {
      	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
      	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject()); 
    	
      	String messageSuccess = null;
      	if(objNodoOpcionCatalogoBean.getState().equals(OptionStateType.REGISTERED.getCode())){
			auxSystemOption.setState(OptionStateType.REJECTED.getCode());			

			messageSuccess = objNodoOpcionCatalogoBean.getOptionType().equals(OptionType.OPTION.getCode())
					? PropertiesConstants.SUCCESS_ADM_OPTION_REJECT_LABEL
					: PropertiesConstants.SUCCESS_ADM_MODULE_REJECT_LABEL;
			
		}else if(objNodoOpcionCatalogoBean.getState().equals(OptionStateType.CONFIRMED.getCode()) &&
				objNodoOpcionCatalogoBean.getSituation().equals(OptionSituationType.PENDING_LOCK.getCode()) && auxEventNode==null){
			auxSystemOption.setState(OptionStateType.CONFIRMED.getCode());		
			
			messageSuccess = objNodoOpcionCatalogoBean.getOptionType().equals(OptionType.OPTION.getCode())
					? PropertiesConstants.SUCCESS_ADM_OPTION_REJECT_BLOCK_LABEL
					: PropertiesConstants.SUCCESS_ADM_MODULE_REJECT_BLOCK_LABEL;
		}else if(objNodoOpcionCatalogoBean.getState().equals(OptionStateType.CONFIRMED.getCode()) &&
			objNodoOpcionCatalogoBean.getSituation().equals(OptionSituationType.PENDING_LOCK.getCode()) && auxEventNode!=null){
			
			auxSystemOption.setState(OptionStateType.CONFIRMED.getCode());
			auxSystemOption.setSituation(OptionSituationType.ACTIVE.getCode());
			
			if(auxEventNode.getOptionType().equals(OptionType.OPTION.getCode())){
				messageSuccess = PropertiesConstants.SUCCESS_ADM_OPTION_REJECT_BLOCK_LABEL;
			}else{
				messageSuccess =  PropertiesConstants.SUCCESS_ADM_MODULE_REJECT_BLOCK_LABEL;
			}
		}else if(objNodoOpcionCatalogoBean.getState().equals(OptionStateType.BLOCKED.getCode()) &&
			objNodoOpcionCatalogoBean.getSituation().equals(OptionSituationType.PENDING_UNLOCK.getCode())){
			
			auxSystemOption.setState(OptionStateType.BLOCKED.getCode());
			auxSystemOption.setSituation(OptionSituationType.ACTIVE.getCode());
			
			if(auxEventNode.getOptionType().equals(OptionType.OPTION.getCode())){
				messageSuccess = PropertiesConstants.SUCCESS_ADM_OPTION_REJECT_UNBLOCK_LABEL;
			}else{
				messageSuccess =  PropertiesConstants.SUCCESS_ADM_MODULE_REJECT_UNBLOCK_LABEL;
			}
		} else if(objNodoOpcionCatalogoBean.getState().equals(OptionStateType.CONFIRMED.getCode()) &&
				objNodoOpcionCatalogoBean.getSituation().equals(OptionSituationType.PENDING_DELETE.getCode())) {
			auxSystemOption.setState(OptionStateType.CONFIRMED.getCode());
			auxSystemOption.setSituation(OptionSituationType.ACTIVE.getCode());
			if(auxEventNode.getOptionType().equals(OptionType.OPTION.getCode())){
				messageSuccess = "success.adm.option.reject.delete.label";
			}else{
				messageSuccess =  "success.adm.module.reject.delete.label";
			}
		}
        systemOptionServiceBean.update(auxSystemOption);
    	//Se envia email y notificacion
        Object[] parameters = {objNodoOpcionCatalogoBean.getName().toUpperCase(), auxSystemOption.getSystem().getName()};
		restServiceNotification.sendNotification(loggerUser,BusinessProcessType.SYSTEM_OPTION_REJECT_REGISTRATION.getCode(),parameters);
        return messageSuccess;
    }

    /**
     * Block system option.
     *
     * @param systemOption the system option
     * @param systemOptionName the system option name
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    @Interceptors(HistoricalStateInterceptor.class)
    public boolean updateSystemOptionState(SystemOption systemOption, String systemOptionName) throws ServiceException {
      	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);   
      	Long businessProcess = null;
      	
      	if(systemOption.getState().equals(OptionStateType.CONFIRMED.getCode()) 
  			&& systemOption.getSituation().equals(OptionSituationType.PENDING_LOCK.getCode())){
				
      		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeBlock());
      		businessProcess = BusinessProcessType.SYSTEM_OPTION_REQUEST_BLOCK.getCode();
		}
      	if(OptionStateType.BLOCKED.getCode().equals(systemOption.getState()) 
  			&& OptionSituationType.ACTIVE.getCode().equals(systemOption.getSituation())){
				
      		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeBlock());
      		businessProcess = BusinessProcessType.SYSTEM_OPTION_BLOCK.getCode();
		}
      	if(systemOption.getState().equals(OptionStateType.BLOCKED.getCode()) 
  			&& systemOption.getSituation().equals(OptionSituationType.PENDING_UNLOCK.getCode())){
				
      		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeUnblock());
      		businessProcess = BusinessProcessType.SYSTEM_OPTION_REQUEST_UNBLOCK.getCode();
		}
      	if(OptionStateType.CONFIRMED.getCode().equals(systemOption.getState()) 
  			&& OptionSituationType.ACTIVE.getCode().equals(systemOption.getSituation())){
				
      		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeUnblock());
      		businessProcess = BusinessProcessType.SYSTEM_OPTION_UNBLOCK.getCode();
		}
      	
      	if(businessProcess != null){
            systemOptionServiceBean.update(systemOption);
            
        	//Se envia email y notificacion        
            Object[] parameters = {systemOptionName,systemOption.getSystem().getName()};
    		restServiceNotification.sendNotification(loggerUser,businessProcess,parameters);
      	}else{
      		return false;
      	}
        
        return true;
    }
    
    /**
     * Update system state service facade.
     *
     * @param system the system
     * @throws ServiceException the service exception
     */
    @Interceptors(HistoricalStateInterceptor.class)
    public void updateSystemStateServiceFacade(System system) throws ServiceException {
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	Long businessProcess = null;
    	
    	if(SystemStateType.CONFIRMED.getCode().equals(system.getState()) && 
			SystemSituationType.PENDING_LOCK.getCode().equals(system.getSituation())){
			
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeBlock());
			businessProcess = BusinessProcessType.SYSTEM_REQUEST_BLOCK.getCode();			
		}
    	if(SystemStateType.BLOCKED.getCode().equals(system.getState()) && 
			SystemSituationType.ACTIVE.getCode().equals(system.getSituation())){
    		
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeBlock());
    		businessProcess = BusinessProcessType.SYSTEM_BLOCK.getCode();
    	}
    	
    	if(SystemStateType.BLOCKED.getCode().equals(system.getState()) &&
			SystemSituationType.PENDING_UNLOCK.getCode().equals(system.getSituation())){
    		
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeUnblock());
    		businessProcess = BusinessProcessType.SYSTEM_REQUEST_UNBLOCK.getCode();
    	}
    	
    	if(SystemStateType.CONFIRMED.getCode().equals(system.getState()) &&
			SystemSituationType.ACTIVE.getCode().equals(system.getSituation())){
    		
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeUnblock());
    		businessProcess = BusinessProcessType.SYSTEM_UNBLOCK.getCode();
    	}
    	
    	if(SystemStateType.REJECTED.getCode().equals(system.getState())){
    		
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
    		businessProcess = BusinessProcessType.SYSTEM_REJECT.getCode();
    		
    	}
    		
    	if(businessProcess != null){
    		systemServiceBean.updateSystemStateServiceBean(system,loggerUser);        
        
    		//Se envia email y notificacion
			Object[] parameters = {system.getName()};
			restServiceNotification.sendNotification(loggerUser,businessProcess,parameters);
	  		
    	}
    }
    
    /**
     * Confirm system service facade.
     *
     * @param system the system
     * @throws ServiceException the service exception
     */
    @Interceptors(HistoricalStateInterceptor.class)
    public void confirmSystemServiceFacade(System system) throws ServiceException {
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		systemServiceBean.updateSystemStateServiceBean(system,loggerUser);
		//Se envia email y notificacion
  		Object[] parameters = {system.getName()};
  		restServiceNotification.sendNotification(loggerUser,BusinessProcessType.SYSTEM_CONFIRM.getCode(),parameters);

    }
 
    /**
     * Register holiday service facade.
     *
     * @param holiday the holiday
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean registerHolidayServiceFacade(Holiday holiday) throws ServiceException {
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
   		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister()); 

        holidayServiceBean.create(holiday);
        
        //Se envia email y notificacion
        DateFormat dfDayMonthYear = new SimpleDateFormat(PropertiesUtilities.getMessage(PropertiesConstants.DATE_FORMAT));
        Object[] parameters = {holiday.getDescription(), dfDayMonthYear.format(holiday.getDateHoliday())};
  		restServiceNotification.sendNotification(loggerUser,BusinessProcessType.HOLIDAY_REGISTRATION.getCode(),parameters);
  		
        return true;
    }

    /**
     * Update holiday state service facade.
     *
     * @param holiday the holiday
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean updateHolidayStateServiceFacade(Holiday holiday) throws ServiceException {
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeDelete()); 
    	
        holidayServiceBean.updateHolidayStateServiceBean(holiday);
        
        //Se envia email y notificacion
        DateFormat dfDayMonthYear = new SimpleDateFormat(PropertiesUtilities.getMessage(PropertiesConstants.DATE_FORMAT));
        Object[] parameters = {holiday.getDescription(), dfDayMonthYear.format(holiday.getDateHoliday())};
  		restServiceNotification.sendNotification(loggerUser,BusinessProcessType.HOLIDAY_DELETE.getCode(),parameters);
  		
  		return true;        
    }

    /**
     * Delete system option.
     *
     * @param opcionSistema the opcion sistema
     * @param systemOptionName the system option name
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean deleteSystemOption(SystemOption opcionSistema,String systemOptionName) throws ServiceException {
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
   		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeDelete());
        systemOptionServiceBean.update(opcionSistema);
        //Se envia email y notificacion
  		Object[] parameters = {systemOptionName,opcionSistema.getSystem().getName()};
  		restServiceNotification.sendNotification(loggerUser,BusinessProcessType.SYSTEM_OPTION_DELETE.getCode(),parameters);

        return true;
    }

    /**
     * Modify system option service facade.
     *
     * @param systemOption the system option
     * @param systemOptionName the system option name
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean modifySystemOptionServiceFacade(SystemOption systemOption, String systemOptionName ) throws ServiceException, IOException{
      	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
      	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
    
        systemOptionServiceBean.updateOnCascade(systemOption);
        
		//Se envia email y notificacion
  		Object[] parameters = {systemOptionName,systemOption.getSystem().getName()};
  		restServiceNotification.sendNotification(loggerUser,BusinessProcessType.SYSTEM_OPTION_MODIFICATION.getCode(),parameters);
  		
  		if(systemOption.getDocumentFile()!= null) {
			InputStream inputStream = new ByteArrayInputStream(systemOption.getDocumentFile());
			fileUploadServiceBean.processFileUploadComponent(systemOption.getDocumentFilePath(), systemOption.getDocumentFileName(), inputStream);
		}
        
        return true;
    }

    /**
     * Register privilege user exception service facade.
     *
     * @param exceptionUserPreivileg the exception user preivileg
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean registerPrivilegeUserExceptionServiceFacade(ExceptionUserPrivilege exceptionUserPreivileg) throws ServiceException {
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
    	userExceptionServiceBean.create(exceptionUserPreivileg);
//    	Only create timer in confirmation
//    	schedulerServiceFacade.updateTimer(exceptionUserPreivileg);
    	//Se envia email y notificacion
  		Object[] parameters = {exceptionUserPreivileg.getName()};
  		restServiceNotification.sendNotification(loggerUser,BusinessProcessType.USER_PRIVILEGES_EXCEPTION_REGISTRATION.getCode(),parameters);
        return true;
    }
    
    /**
     * Modify privilege user exception service facade.
     *
     * @param exceptionUserPreivileg the exception user preivileg
     * @param oldPrivilegeList the old privilege list
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean modifyPrivilegeUserExceptionServiceFacade(ExceptionUserPrivilege exceptionUserPreivileg,List<Integer> oldPrivilegeList) throws ServiceException {
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
    	//delete old privileges
    	userExceptionServiceBean.deletePrivilegeUserExceptionServiceBean(oldPrivilegeList);
    	List<UserPrivilegeDetail> privilegesNews = exceptionUserPreivileg.getUserPrivilegeDetails();
    	//update exception
    	exceptionUserPreivileg.setUserPrivilegeDetails(null);
    	ExceptionUserPrivilege excpetionUser= userExceptionServiceBean.updateOnCascade(exceptionUserPreivileg);
    	//register privileges modification
    	for(UserPrivilegeDetail userPrivilege:privilegesNews){
    		userPrivilege.setExceptionUserPrivilege(excpetionUser);
    		userExceptionServiceBean.update(userPrivilege);
    	}
//    	Only create timer in confirmation
//    	schedulerServiceFacade.updateTimer(exceptionUserPreivileg);
    	//Se envia email y notificacion
  		Object[] parameters = {exceptionUserPreivileg.getName()};
  		restServiceNotification.sendNotification(loggerUser,BusinessProcessType.USER_PRIVILEGES_EXCEPTION_MODIFICATION.getCode(),parameters);
    	
    	
    	return true;
    }

    /**
     * Delete user exception by list service facade.
     *
     * @param list the list
     * @param state the state
     * @param situation the situation
     * @param rejectmotivo the rejectmotivo
     * @param deletemotivo the deletemotivo
     * @param motiveDescArr the motive desc arr
     * @param parametersNotification the parameters notification
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean modifyStateUserExceptionByListServiceFacade(List<Integer> list,Integer state,Integer situation,Integer rejectmotivo,Integer deletemotivo,String[] motiveDescArr,String parametersNotification) throws ServiceException {
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeDelete());
    	
        userExceptionServiceBean.modifyStateUserExceptionByUserServiceBean(list,state,situation,rejectmotivo,deletemotivo,motiveDescArr,loggerUser);
        
		//Se envia email y notificacion
//  		Object[] parameters = {parametersNotification};
//  		restServiceNotification.sendNotification(loggerUser,BusinessProcessType.USER_PRIVILEGES_EXCEPTION_MANUAL_DELETE.getCode(),parameters);
        
        return true;
    }
    
    /**
     * Registry rejecto request delete exception privilege.
     *
     * @param exceptions the exceptions
     * @param state the state
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean registryRejectoRequestDeleteExceptionPrivilege(List<ExceptionUserPrivilege> exceptions,Integer state) throws ServiceException{
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	if(UserExceptionStateType.REJECT.getCode().equals(state)){
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
    	} else if(UserExceptionStateType.CONFIRMED.getCode().equals(state)){
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeDelete());
    	}
    	for(ExceptionUserPrivilege excep : exceptions){
    		byte[] file =getExceptionUserPrivilegeFile(excep.getIdExceptionUserPrivilegePk());
    		ExceptionUserPrivilege exceptionMerge = userExceptionServiceBean.update(excep);
    		//set old file
    		exceptionMerge.setDocumentFile(file);
    		
    		//Se envia email y notificacion
      		Object[] parameters = {excep.getName()};
      		if(UserExceptionStateType.REJECT.getCode().equals(state)){
      			restServiceNotification.sendNotification(loggerUser,BusinessProcessType.USER_PRIVILEGES_EXCEPTION_REJECT.getCode(), parameters);
      		} else if(UserExceptionStateType.CONFIRMED.getCode().equals(state)){
      			restServiceNotification.sendNotification(loggerUser,BusinessProcessType.USER_PRIVILEGES_EXCEPTION_MANUAL_DELETE.getCode(), parameters);
      		}      		
    	}
    	return true;
    }
    
    /**
     * Confirm privilege exception user.
     *
     * @param exceptions the exceptions
     * @throws ServiceException the service exception
     */
    public void confirmPrivilegeExceptionUser(List<ExceptionUserPrivilege> exceptions) throws ServiceException{
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
    	for(ExceptionUserPrivilege excep : exceptions){
    		byte[] file =getExceptionUserPrivilegeFile(excep.getIdExceptionUserPrivilegePk());
    		ExceptionUserPrivilege exceptionMerge = userExceptionServiceBean.update(excep);
    		//set old file
    		exceptionMerge.setDocumentFile(file);
    		//timer cancelation
    		schedulerServiceFacade.updateTimer(exceptionMerge);
    		
    		//Se envia email y notificacion
      		Object[] parameters = {excep.getName()};
      		restServiceNotification.sendNotification(loggerUser,BusinessProcessType.USER_PRIVILEGES_EXCEPTION_CONFIRM.getCode(), parameters);
    		
    	}
//    	userExceptionServiceBean.updateStateSistuationExceptionPrivilege(exceptions,
//    			UserExceptionStateType.CONFIRMED.getCode(), UserExceptionSituationType.ACTIVE.getCode(), loggerUser);
//    	//create timers for confirmation exceptions
//    	for(ExceptionUserPrivilege exceptionUser : exceptions){
//    		schedulerServiceFacade.updateTimer(exceptionUser);
//    	}    	    	
    	
    }
    
    /**
     * Delete privilege exception user.
     *
     * @param exception the exception
     * @throws ServiceException the service exception
     */
    public void deletePrivilegeExceptionUser(ExceptionUserPrivilege exception) throws ServiceException{
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeDelete());
    	byte[] file =getExceptionUserPrivilegeFile(exception.getIdExceptionUserPrivilegePk());
		ExceptionUserPrivilege exceptionMerge = userExceptionServiceBean.update(exception);
		//set old file
		exceptionMerge.setDocumentFile(file);
//    	List<ExceptionUserPrivilege> listexceps = new ArrayList<>();
//    	listexceps.add(exception);
//    	userExceptionServiceBean.updateStateSistuationExceptionPrivilege(listexceps,
//    			UserExceptionStateType.DELETED.getCode(), UserExceptionSituationType.ACTIVE.getCode(), loggerUser);
//    	//delete timers registered
    	schedulerServiceFacade.deleteTimer(exceptionMerge);
		//Se envia email y notificacion
		Object[] parameters = {exceptionMerge.getName()};
		restServiceNotification.sendNotification(loggerUser,BusinessProcessType.USER_PRIVILEGES_EXCEPTION_MANUAL_DELETE.getCode(),parameters);
    }
    
    /**
     * Reject delete privilege exception user.
     *
     * @param exception the exception
     * @throws ServiceException the service exception
     */
    public void rejectDeletePrivilegeExceptionUser(ExceptionUserPrivilege exception) throws ServiceException{
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeDelete());
    	byte[] file =getExceptionUserPrivilegeFile(exception.getIdExceptionUserPrivilegePk());
		ExceptionUserPrivilege exceptionMerge = userExceptionServiceBean.update(exception);
		//set old file
		exceptionMerge.setDocumentFile(file);
//    	List<ExceptionUserPrivilege> listexceps = new ArrayList<>();
//    	listexceps.add(exception);
//    	userExceptionServiceBean.updateStateSistuationExceptionPrivilege(listexceps,
//    			UserExceptionStateType.CONFIRMED.getCode(), UserExceptionSituationType.ACTIVE.getCode(), loggerUser);
    }

    /**
     * Search privilege user detail by exception service facade.
     *
     * @param idException the id exception
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<UserPrivilegeDetail> searchPrivilegeUserDetailByExceptionServiceFacade(Integer idException) throws ServiceException {
    	List<UserPrivilegeDetail> listDetail = userExceptionServiceBean.searchPrivilegesDetailsByExceptionServiceBean(idException);
        if(listDetail!= null && listDetail.size()>0){
	    	return listDetail;
	    }
	    else{
	    	return new ArrayList<UserPrivilegeDetail>();
	    }
    }

    /**
     * Search option privileges exception service facade.
     *
     * @param userExceptionDetalle the user exception detalle
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<Object[]> searchOptionPrivilegesExceptionServiceFacade(UserExceptionFilter userExceptionDetalle) throws ServiceException {
        return userExceptionServiceBean.searchOptionPrivilegesExceptionServiceBean(userExceptionDetalle);
    }

    /**
     * List system option by id service facade.
     *
     * @param userExceptionFilter the user exception filter
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<Object[]> listSystemOptionByIdServiceFacade(UserExceptionFilter userExceptionFilter) throws ServiceException {
        return systemOptionServiceBean.listSystemOptionsByIdServiceBean(userExceptionFilter);
    }

    /**
     * List id options by privilege service facade.
     *
     * @param userExceptionFilter the user exception filter
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<Integer> listIdOptionsByPrivilegeServiceFacade(UserExceptionFilter userExceptionFilter) throws ServiceException {
        return systemOptionServiceBean.listIdSystemOptionsByPrivilegeServiceBean(userExceptionFilter);
    }

    /**
     * Validate system mnemonic service facade.
     *
     * @param system the system
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean validateSystemMnemonicServiceFacade(System system) throws ServiceException {
        return systemServiceBean.validateSystemMnemonicServiceBean(system)>0;
    }
    
    /**
     * List available systems by user service facade.
     *
     * @param user the user
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<System> listAvailableSystemsByUserServiceFacade(UserAccount user) throws ServiceException{
    	return systemServiceBean.listAvailableSystemsByUserServiceBean(user);
    }
    
    /**
     * List option privileges availables service facade.
     *
     * @param privilegeOptionFilter the privilege option filter
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<SystemOption> listOptionPrivilegesAvailablesServiceFacade(PrivilegeOptionFilter privilegeOptionFilter) throws ServiceException {
    	return userExceptionServiceBean.listOptionPrivilegesAvailablesServiceBean(privilegeOptionFilter);
    }
    
    /**
     * List schedule exception by user service facade.
     *
     * @param scheduleExceptionFilter the schedule exception filter
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<ScheduleException> listScheduleExceptionByUserServiceFacade(ScheduleExceptionFilter scheduleExceptionFilter) throws ServiceException {
        return scheduleExceptionServiceBean.listScheduleExceptionByUserServiceBean(scheduleExceptionFilter);
    }
    
    /**
     * Gets the schedule detail by user service facade.
     *
     * @param filtro the filtro
     * @return the schedule detail by user service facade
     * @throws ServiceException the service exception
     */
    public ScheduleDetail getScheduleDetailByUserServiceFacade(ScheduleFilter filtro) throws ServiceException {
    	return this.scheduleServiceBean.getScheduleDetailsByUserServiceBean(filtro);
    }
    
    /**
     * List availiable systems by schedule service facade.
     *
     * @param horarioFiltro the horario filtro
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<System> listAvailiableSystemsByScheduleServiceFacade(ScheduleFilter horarioFiltro) throws ServiceException{
    	return systemServiceBean.listAvailiableSystemsByScheduleServiceBean(horarioFiltro);
    }
    
    /**
     * Find holiday by filters service facade.
     *
     * @param holidayFilter the holiday filter
     * @return the holiday
     * @throws ServiceException the service exception
     */
    public Holiday findHolidayByFiltersServiceFacade(HolidayFilter holidayFilter) throws ServiceException {
    	return holidayServiceBean.findHolidayByFiltersServiceBean(holidayFilter);
    }

	/**
	 * Gets the schedule by institution service facade.
	 *
	 * @param scheduleFilter the schedule filter
	 * @return the schedule by institution service facade
	 * @throws ServiceException the service exception
	 */
	public Schedule getScheduleByInstitutionServiceFacade(ScheduleFilter scheduleFilter) throws ServiceException{
		return scheduleServiceBean.getScheduleByInstitutionServiceBean(scheduleFilter);
	}

	/**
	 * Gets the schedule detail by system service facade.
	 *
	 * @param scheduleFilter the schedule filter
	 * @return the schedule detail by system service facade
	 * @throws ServiceException the service exception
	 */
	public ScheduleDetail getScheduleDetailBySystemServiceFacade(ScheduleFilter scheduleFilter) throws ServiceException{
		return scheduleServiceBean.getScheduleDetailBySystemServiceBean(scheduleFilter);		
	}
	
	
	
    /**
     * Update the OrderOption cloumn in SystemOption.
     *
     * @param idSystemOption the id system option
     * @param orderOption the order option
     * @return true if successful
     * @throws ServiceException the service exception
     */
    public boolean updateOrderOptionByidSystemOptionServiceFacade(Integer idSystemOption,Integer orderOption) throws ServiceException {
        return systemOptionServiceBean.updateOrderOptionByidSystemOption(idSystemOption,orderOption) > 0;
    }
    
    
    /**
     * update the OrderOption for system level modules  in SystemOption.
     *
     * @param idSystemPk the id system pk
     * @param orderOption the order option
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean updateOrderOptionByidSystemPkServiceFacade(Integer idSystemPk,Integer orderOption) throws ServiceException {
    	return systemOptionServiceBean.updateOrderOptionByidSystemPk(idSystemPk, orderOption) > 0;
    }
    
    /**
     * Gets the system with mnemonic.
     *
     * @param mnemonic the mnemonic
     * @return the system with mnemonic
     * @throws ServiceException the service exception
     */
    public boolean getSystemWithMnemonic(String mnemonic) throws ServiceException{
    	List<?> lstSys = systemServiceBean.getSystemWithMnemonic(mnemonic);
    	if(lstSys != null && lstSys.size() > 0){
			return true;
		}else{
			return false;
		}
    }
    
    /**
     * Gets the system with name.
     *
     * @param name the name
     * @param idsystemPk the idsystem pk
     * @return the system with name
     * @throws ServiceException the service exception
     */
    public boolean getSystemWithName(String name,Integer idsystemPk) throws ServiceException{
    	List<?> lstSys = systemServiceBean.getSystemWithName(name,idsystemPk);
    	if(lstSys != null && lstSys.size() > 0){
			return true;
		}else{
			return false;
		}
    }
    
    /**
     * Search all system service facade.
     *
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<System> searchAllSystemServiceFacade() throws ServiceException {
    	return systemServiceBean.searchAllSystemServiceBean();
    }

	/**
	 * Gets the systems by schedule service facade.
	 *
	 * @param idSchedulePk the id schedule pk
	 * @return the systems by schedule service facade
	 * @throws ServiceException the service exception
	 */
	public List<System> getSystemsByScheduleServiceFacade(
			long idSchedulePk)throws ServiceException {
		return scheduleServiceBean.getSystemsByScheduleServiceBean(idSchedulePk);
	}
	
	/**
	 * Gets the system with image.
	 *
	 * @param intSystem the int system
	 * @return the system with image
	 */
	public System getSystemWithImage(Integer intSystem){
		System sys = null;
		sys = systemServiceBean.find(System.class, intSystem);
		if(sys != null){
			sys.getSystemImage();
		}
		return sys;
	}
	
	/**
	 * it gets the List of Schedule details.
	 *
	 * @param idSchedulePk the id schedule pk
	 * @return the schedule detail service facade
	 * @throws ServiceException the service exception
	 */
	public List<ScheduleDetail> getScheduleDetailServiceFacade(
			long idSchedulePk)throws ServiceException {
		return scheduleServiceBean.getScheduleDetailServiceFacade(idSchedulePk);
	}
	
	/**
	 * It deletes the already existed records with same schedulePk.
	 *
	 * @param idSchedulePk the id schedule pk
	 * @throws ServiceException the service exception
	 */
	public void deleteScheduleDetailServiceFacade(long idSchedulePk)throws ServiceException{
		scheduleServiceBean.deleteScheduleDetailServiceFacade(idSchedulePk);
	}

	
	/**
	 * To perform validation whether the Exception having with same name	 .
	 *
	 * @param userExceptionName the user exception name
	 * @return true if success
	 * @throws ServiceException the service exception
	 */
	public boolean validateUserExceptionServiceFacade(String userExceptionName) throws ServiceException {
		Long count = userExceptionServiceBean.validateUserExceptionServiceBean(userExceptionName);
		if (count == 0l) {
			return true;
		}
		return false;
	}

	/**
	 * Load user priviledges service facade.
	 *
	 * @param userExceptionCodeUser the user exception code user
	 * @param idsystem the idsystem
	 * @return list ProfilePrivilege
	 * @throws ServiceException the service exception
	 */
	public List<ProfilePrivilege> loadUserPriviledgesServiceFacade(
			Integer userExceptionCodeUser,Integer idsystem) throws ServiceException {
		return userExceptionServiceBean.loadUserPriviledgesServiceBean(userExceptionCodeUser,idsystem);
	}
	

	/**
	 * To get the option id with the given orderOption.
	 *
	 * @param systemidpk the systemidpk
	 * @param orderOption the order option
	 * @param optionidPk the optionid pk
	 * @param parentIdPk the parent id pk
	 * @return return id if exists else null
	 * @throws ServiceException the service exception
	 */
	public Integer getSystemOptionByOrderOptionServiceFacade(
			Integer systemidpk,Integer orderOption,Integer optionidPk,Integer parentIdPk) throws ServiceException {
		return systemOptionServiceBean.getSystemOptionByOrderOptionServiceBean(systemidpk,orderOption,optionidPk,parentIdPk);
	}

	/**
	 * To get Maximum OrderOptoin value.
	 *
	 * @param parentidpk the parentidpk
	 * @param systemidpk the systemidpk
	 * @return Maximum OrderOption value
	 * @throws ServiceException the service exception
	 */
	public Integer getMaxOrderOptionServiceFacade(Integer parentidpk,
			Integer systemidpk) throws ServiceException{
		return systemOptionServiceBean.getMaxOrderOptionServiceBean(parentidpk,systemidpk);
	}
	
	/**
	 * Gets the images systems.
	 *
	 * @param systems the systems
	 * @return the images systems
	 * @throws ServiceException the service exception
	 */
	public List<System> getImagesSystems(List<System> systems) throws ServiceException {
		return systemServiceBean.getImagesSystems(systems);
	}

	/**
	 * Search user exceptions service facade.
	 *
	 * @param userException the user exception
	 * @return list
	 * @throws ServiceException the service exception
	 */
	public List<ExceptionUserPrivilege> searchUserExceptionsServiceFacade(
			UserExceptionFilter userException) throws ServiceException {
		return userExceptionServiceBean.searchUserExceptionServiceBean(userException);
	}

	/**
	 * To get the Exception User Privilege.
	 *
	 * @param idUserAccountPk the id user account pk
	 * @return List ExceptionPrivileges
	 * @throws ServiceException the service exception
	 */
	public List<ExceptionUserPrivilege> listUserExceptionPrivilegeServiceFacade(
			Integer idUserAccountPk)throws ServiceException {
		return userExceptionServiceBean.listUserExceptionPrivilegeServiceBean(idUserAccountPk);
	}
	
	/**
	 * Gets the lastest sysem details.
	 *
	 * @param idSystemPk the id system pk
	 * @return the lastest sysem details
	 */
	public System getLastestSysemDetails(Integer idSystemPk){
		return systemServiceBean.getLastestSysemDetails(idSystemPk);
	}
	
	/**
	 * Check user with system profile service facade.
	 *
	 * @param systemID the system id
	 * @param userIDs the user i ds
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<UserAccount> checkUserWithSystemProfileServiceFacade(Integer systemID, List<Integer> userIDs) throws ServiceException{
		return scheduleExceptionServiceBean.checkUserWithSystemProfileServiceBean(systemID,userIDs);
	}

	/**
	 * To get the schedule Exception by system.
	 *
	 * @param idSystemPk the id system pk
	 * @param idUserAccountPk the id user account pk
	 * @return the schedule exception by system
	 * @throws ServiceException the service exception
	 */
	public ScheduleException getScheduleExceptionBySystem(Integer idSystemPk,
			Integer idUserAccountPk) throws ServiceException{
		return scheduleExceptionServiceBean.getScheduleExceptionBySystemServiceBean(idSystemPk,idUserAccountPk);
	}
	
	
	/**
	 * Updating the user exception status when date expires.
	 *
	 * @param userPrivileges the user privileges
	 * @throws ServiceException the service exception
	 */
	public void updaetUserExceptionsFacade(ExceptionUserPrivilege userPrivileges) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    		
    	scheduleExceptionServiceBean.updateOnCascade(userPrivileges);
    	
    	Long businessProcess = null;
    	if(UserExceptionStateType.ANNULLED.getCode().equals(userPrivileges.getState())){
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
    		businessProcess = BusinessProcessType.USER_PRIVILEGES_EXCEPTION_AUTOMATIC_DELETE.getCode();
    	}else{
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
    		businessProcess = BusinessProcessType.USER_PRIVILEGES_EXCEPTION_MODIFICATION.getCode();
    	}
    	
    	//Se envia email y notificacion
  		Object[] parameters = {userPrivileges.getName()};
  		restServiceNotification.sendNotification(loggerUser, businessProcess,parameters);
	}
	
	/**
	 * to get all the users.
	 *
	 * @return List<Object[]>
	 */
	public List<Object[]> getUsersList() {
		return userExceptionServiceBean.getUsersList();
	}
	
	/**
	 * Gets the systems by code and state.
	 *
	 * @param systemPks the system pks
	 * @return the systems by code and state
	 */
	public List<System> getSystemsByCodeAndState(List<Integer> systemPks){
		return systemServiceBean.searchSystemByIdAndState(systemPks);
	}
	
	/**
	 * to get ExceptionUserPrivileges Details By Ids.
	 *
	 * @param list ExceptionUserPrivileges ids
	 * @return List<Object[]>
	 */
	public List<Object[]> getExceptionUserPrivilegesDetailsByIds(List<Integer> list){
		List<Object[]> listUserExceptions = userExceptionServiceBean.getExceptionUserPrivilegesDetailsByIds(list);
        if(listUserExceptions!= null && listUserExceptions.size()>0){
	    	return listUserExceptions;
	    }
	    else{
	    	return new ArrayList<Object[]>();
	    }
	}

	/**
	 * To get Active Profile User list.
	 *
	 * @param systemProfile the system profile
	 * @return list
	 * @throws ServiceException the service exception
	 */
	public List<String> getActiveProfileUsersServiceFacade(
			SystemProfile systemProfile) throws ServiceException {
		return systemProfileServiceBean.getActiveProfileUsersServiceBean(systemProfile);
	}
	
	/**
	 * Gets the list available holiday date.
	 *
	 * @return the list available holiday date
	 * @throws ServiceException the service exception
	 */
	public List<Date> getListAvailableHolidayDate() throws ServiceException{
		return holidayServiceBean.getListAvailableHolidayDate();
	}

	/**
	 * Gets the system production file.
	 *
	 * @param idSystemPk the id system pk
	 * @return the system production file
	 */
	public byte[] getSystemProductionFile(Integer idSystemPk) {
		return systemServiceBean.getSystemProductionFile(idSystemPk);
	}
	
	/**
	 * Gets the exception user privilege file.
	 *
	 * @param idExceptionPrivilege the id exception privilege
	 * @return the exception user privilege file
	 */
	public byte[] getExceptionUserPrivilegeFile(Integer idExceptionPrivilege){
		return userExceptionServiceBean.getExceptionUserPrivilegeFile(idExceptionPrivilege);
	}
	
	/**
	 * Gets the system image.
	 *
	 * @param idSystemPk the id system pk
	 * @return the system image
	 */
	public byte[] getSystemImage(Integer idSystemPk){
		return systemServiceBean.getSystemImage(idSystemPk);
	}
	

    /**
     * Gets the schedules service facade.
     *
     * @param filter the filter
     * @return the schedules service facade
     * @throws ServiceException the service exception
     */
    public List<Schedule> getSchedulesServiceFacade(ScheduleFilter filter) throws ServiceException {
        return scheduleServiceBean.getSchedulesServiceBean(filter);
    }

    /**
     * Gets the schedule service facade.
     *
     * @param schedule the schedule
     * @return the schedule service facade
     * @throws ServiceException the service exception
     */
    public Schedule getScheduleServiceFacade(Schedule schedule) throws ServiceException {
    	return scheduleServiceBean.getScheduleServiceBean(schedule);
    }

    /**
     * To get Schedule by id.
     *
     * @param idSchedulePk the id schedule pk
     * @return Schedule
     * @throws ServiceException the service exception
     */
    public Schedule getScheduleById(Long idSchedulePk) throws ServiceException {
    	return scheduleServiceBean.find(Schedule.class, idSchedulePk);
    }
    
    /**
     * Gets the schedule exception service facade.
     *
     * @param filter the filter
     * @return the schedule exception service facade
     * @throws ServiceException the service exception
     */
    public List<ScheduleException> getScheduleExceptionServiceFacade(ScheduleExceptionFilter filter) throws ServiceException {
        return scheduleExceptionServiceBean.getScheduleExceptionsServiceBean(filter);
    }

    /**
     * Gets the schedule exception service facade.
     *
     * @param h the h
     * @return the schedule exception service facade
     * @throws ServiceException the service exception
     */
    public ScheduleException getScheduleExceptionServiceFacade(ScheduleException h) throws ServiceException {
    	ScheduleException scheduleException=scheduleExceptionServiceBean.getScheduleExceptionServiceBean(h);
    	if(Validations.validateIsNotNull(scheduleException)){
			List<ScheduleExceptionDetail> scheduleExceptionDetailList = new ArrayList<ScheduleExceptionDetail>();
			for(ScheduleExceptionDetail scheduleExceptionDetail: scheduleException.getScheduleExceptionDetails()){
				if(scheduleExceptionDetail.getState().equals(ScheduleExceptionDetailStateType.CONFIRMED.getCode())){
					scheduleExceptionDetailList.add(scheduleExceptionDetail);
				}
			}
			//scheduleException.getScheduleExceptionDetails().removeAll(scheduleExceptionDetailList);
		}
        return scheduleException;
    }
    
    /**
     * Gets the last modifed details.
     *
     * @param idScheduleExceptionPk the id schedule exception pk
     * @return the last modifed details
     * @throws ServiceException the service exception
     */
    public Date getLastModifedDetails(Long idScheduleExceptionPk)throws ServiceException{
    	 return scheduleExceptionServiceBean.getLastModifedDetails(idScheduleExceptionPk);
    }
    /**
     * Search system by state service facade.
     *
     * @param state the state
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<System> searchSystemByStateServiceFacade(Integer state) throws ServiceException {
        return systemServiceBean.searchSystemByStateServiceBean(state);
    }

    /**
     * Search system option by id service bean.
     *
     * @param idOpcion the id opcion
     * @return the system option
     * @throws ServiceException the service exception
     */
    public SystemOption searchSystemOptionByIdServiceBean(Integer idOpcion) throws ServiceException {
    	List<SystemOption> systemOptionList =systemOptionServiceBean.searchSystemOptionByIdServiceBean(idOpcion);
    	if(systemOptionList!= null && systemOptionList.size()>0){
 	    	return systemOptionList.get(0);
 	    }
 	    else{
 	    	return new SystemOption();
 	    }
       
    }
    /**
     * Search last ModifiedDate by id service bean.
     *
     * @param idOpcion the id opcion
     * @return the system option
     * @throws ServiceException the service exception
     */
    public Date getSystemOptionsLastModfiedDetails(Integer idOpcion)throws ServiceException {
    	return  systemOptionServiceBean.getSystemOptionsLastModfiedDetails(idOpcion);
    }
    
    /**
     * Find system options details.
     *
     * @param idOpcion the id opcion
     * @return the system option
     * @throws ServiceException the service exception
     */
    public SystemOption findSystemOptionsDetails(Integer idOpcion)throws ServiceException {
    	return  systemOptionServiceBean.findSystemOptionsDetails(idOpcion);
    }
    /**
     * List system options by system service facade.
     *
     * @param systemFilterParam the system filter param
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<SystemOption> listSystemOptionsBySystemServiceFacade(SystemFilter systemFilterParam) throws ServiceException {
    	List<SystemOption> listSystemOptions=systemOptionServiceBean.listSystemOptionBySystemFilterServiceBean(systemFilterParam);
    	 if(listSystemOptions!= null && listSystemOptions.size()>0){
 	    	return listSystemOptions;
 	    }else{
 	    	return new ArrayList<SystemOption>();
 	    }
    }

    /**
     * Search profiles by filters service facade.
     *
     * @param systemProfileFilter the system profile filter
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<SystemProfile> searchProfilesByFiltersServiceFacade(SystemProfileFilter systemProfileFilter) throws ServiceException {
        return systemProfileServiceBean.searchProfilesByFiltersServiceBean(systemProfileFilter);
    }

    /**
     * Search privileges by definition service bean.
     *
     * @param idDefinicion the id definicion
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<Parameter> searchPrivilegesByDefinitionServiceBean(Integer idDefinicion) throws ServiceException {
    	List<Parameter> privilegesList =systemOptionServiceBean.searchPrivilegesByDefinitionServiceBean(idDefinicion);
    	if(privilegesList!= null && privilegesList.size()>0){
	    	return privilegesList;
	    }
	    else{
	    	return new ArrayList<Parameter>();
	    }
    }
    /**
     * List option privileges service facade.
     *
     * @param systemProfileFilter the system profile filter
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<Object[]> listOptionPrivilegesServiceFacade(SystemProfileFilter systemProfileFilter) throws ServiceException {
        return systemOptionServiceBean.listOptionPrivilegesServiceBean(systemProfileFilter);
    }

    /**
     * List option systems by language service facade.
     *
     * @param systemProfileFilter the system profile filter
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<Object[]> listOptionSystemsByLanguageServiceFacade(SystemProfileFilter systemProfileFilter) throws ServiceException {
        return systemOptionServiceBean.listOptionSystemsByLanguageServiceBean(systemProfileFilter);
    }

    /**
     * Find system by id service facade.
     *
     * @param id the id
     * @return the system
     * @throws ServiceException the service exception
     */
    public System findSystemByIdServiceFacade(Integer id) throws ServiceException {
        return systemServiceBean.findSystemByIdServiceBean(id);
    }
    /**
     * List privileges by profile service facade.
     *
     * @param systemProfileFilter the system profile filter
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<ProfilePrivilege> listPrivilegesByProfileServiceFacade(SystemProfileFilter systemProfileFilter) throws ServiceException {
        return systemProfileServiceBean.listPrivilegesByProfileServiceBean(systemProfileFilter);
    }
    
    /**
     * To get the SystemProfile by using id.
     *
     * @param idSystemPk the id system pk
     * @return SystemProfile
     */
    public SystemProfile getLastestSysemProfileDetailsFacade(Integer idSystemPk){
    	return systemProfileServiceBean.find(SystemProfile.class, idSystemPk);
    }
	
	/**
	 * Gets the lats modified details.
	 *
	 * @param systemProfileFilter the system profile filter
	 * @return the lats modified details
	 */
	public Date getLatsModifiedDetails(SystemProfile systemProfileFilter){
		  return systemProfileServiceBean.getLatsModifiedDetails(systemProfileFilter);
	}
	

    /**
     * List option catalogue privileges by system service bean.
     *
     * @param idOption the id option
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<Object[]> listOptionCataloguePrivilegesBySystemServiceBean(Integer idOption) throws ServiceException {
    	List<Object[]> listOptions= systemOptionServiceBean.listOptionCataloguePrivilegesBySystemServiceBean(idOption);
    	 if(listOptions!= null && listOptions.size()>0){
		     return listOptions;
		 }
		 else{
		   return new ArrayList<Object[]>();
		 }
     }
    

    /**
     * Search node child by option service bean.
     *
     * @param idOption the id option
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<SystemOption> searchNodeChildByOptionServiceBean(Integer idOption) throws ServiceException {
    	 List<SystemOption> systemOption =systemOptionServiceBean.searchNodeChildByOptionServiceBean(idOption);
    	if(systemOption!= null && systemOption.size()>0){
	    	return systemOption;
	    }else{
	    	return new ArrayList<SystemOption>();
	    }
    }

    /**
     * Validate system name service facade.
     *
     * @param system the system
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean validateSystemNameServiceFacade(System system) throws ServiceException {
        return systemServiceBean.validateSystemNameServiceBean(system)>0;
    }

    /**
     * Gets the systems by filter service facade.
     *
     * @param filter the filter
     * @return the systems by filter service facade
     * @throws ServiceException the service exception
     */
    public List<System> getSystemsByFilterServiceFacade(SystemFilter filter) throws ServiceException {
        return systemServiceBean.getSystemsByFilterServiceBean(filter);
    }

    /**
     * List option privileges by profile service facade.
     *
     * @param systemProfileFilter the system profile filter
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<Object[]> listOptionPrivilegesByProfileServiceFacade(SystemProfileFilter systemProfileFilter) throws ServiceException {
        return systemProfileServiceBean.listOptionPrivilegesByProfileServiceBean(systemProfileFilter);
    }

    /**
     * List id system optios by option privilege service facade.
     *
     * @param systemProfileFilter the system profile filter
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<Integer> listIdSystemOptiosByOptionPrivilegeServiceFacade(SystemProfileFilter systemProfileFilter) throws ServiceException {
        return systemOptionServiceBean.listIdSystemOptiosByOptionPrivilegeServiceBean(systemProfileFilter);
    }

    /**
     * List system options by ids service facade.
     *
     * @param systemProfileFilter the system profile filter
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<Object[]> listSystemOptionsByIdsServiceFacade(SystemProfileFilter systemProfileFilter) throws ServiceException {
        return systemOptionServiceBean.listSystemOptionsByIdsServiceBean(systemProfileFilter);
    }

    /**
     * List holidays by state service facade.
     *
     * @param state the state
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<Holiday> listHolidaysByStateServiceFacade(Integer state) throws ServiceException {
        return holidayServiceBean.listHolidaysByStateServiceBean(state);
    }

    /**
     * To find the day is already register as holiday .
     *
     * @param holiday the holiday
     * @return if exist return true
     * @throws ServiceException the service exception
     */
    public boolean findHolidayExistenceFacade(Holiday holiday) throws ServiceException{
		return holidayServiceBean.findHolidayExistence(holiday) > 0;
	}
    /**
     * List privilege catalogue by option service bean.
     *
     * @param idOpcion the id opcion
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<Object[]> listPrivilegeCatalogueByOptionServiceBean(Integer idOpcion) throws ServiceException {
    	List<Object[]> listOptions =systemOptionServiceBean.listPrivilegeCatalogueByOptionServiceBean(idOpcion);
    	if(listOptions!= null && listOptions.size()>0){
		     return listOptions;
		}
		else{
			 return new ArrayList<Object[]>();
		}
     }

    /**
     * List parameter by definition service facade.
     *
     * @param idDefinitionPk the id definition pk
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<Parameter> listParameterByDefinitionServiceFacade(Integer idDefinitionPk) throws ServiceException {
        return parameterSecurityServiceBean.listParameterByDefinitionServiceBean(idDefinitionPk);
    }

    /**
     * Validate profile name service facade.
     *
     * @param systemProfile the system profile
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean validateProfileNameServiceFacade(SystemProfile systemProfile) throws ServiceException {
        return systemProfileServiceBean.validateProfileNameServiceBean(systemProfile) > 0;
    }

    /**
     * Validate profile mnemonic service facade.
     *
     * @param systemProfile the system profile
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean validateProfileMnemonicServiceFacade(SystemProfile systemProfile) throws ServiceException {
        return systemProfileServiceBean.validateProfileMnemonicServiceBean(systemProfile) > 0;
    }

    /**
     * List catalogue by option service bean.
     *
     * @param idOption the id option
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<Object[]> listCatalogueByOptionServiceBean(Integer idOption) throws ServiceException {
    	List<Object[]> optionList =systemOptionServiceBean.listCatalogueByOptionServiceBean(idOption);
    	if(optionList!= null && optionList.size()>0){
		     return optionList;
		}else{
		   return new ArrayList<Object[]>();
		}		
     }

    /**
     * Search privilege by id service facade.
     *
     * @param idPrivilege the id privilege
     * @param idOption the id option
     * @return the option privilege
     * @throws ServiceException the service exception
     */
    public OptionPrivilege searchPrivilegeByIdServiceFacade(Integer idPrivilege, Integer idOption) throws ServiceException {
    	List<OptionPrivilege> listPrivileges =systemOptionServiceBean.searchOptionPrivilegeByIdServiceBean(idPrivilege, idOption);
    	if(listPrivileges!= null && listPrivileges.size()>0){
 	    	return listPrivileges.get(0);
 	    }
 	    else{
 	    	return new OptionPrivilege();
 	    }
    }
    
    /**
     * Search privilege by id service facade.
     *
     * @param idPrivilegePks the id privilege pks
     * @return the option privilege
     * @throws ServiceException the service exception
     */
    public List<OptionPrivilege> searchOptionPrivilegeByIdPk(List<Integer> idPrivilegePks) throws ServiceException {
        return systemOptionServiceBean.searchOptionPrivilegeByIdPk(idPrivilegePks);
    }

    /**
     * Delete privilege service facade.
     *
     * @param listaPrivilegios the lista privilegios
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean deletePrivilegeServiceFacade(List<OptionPrivilege> listaPrivilegios) throws ServiceException {
    	
    	for(int i=0;i<listaPrivilegios.size();i++){
    		OptionPrivilege op = new OptionPrivilege();
    		op=listaPrivilegios.get(i);
    		userExceptionServiceBean.update(op);
    	}
       
        return true;
    }

    /**
     * Search catalogue language by id service facade.
     *
     * @param idCataloge the id cataloge
     * @return the catalogue language
     * @throws ServiceException the service exception
     */
    public CatalogueLanguage searchCatalogueLanguageByIdServiceFacade(Integer idCataloge) throws ServiceException {
    	List<CatalogueLanguage> listCatalogue = systemOptionServiceBean.searchCatalogueLanguageByIdServiceBean(idCataloge);
    	if(listCatalogue!= null && listCatalogue.size()>0){
 	    	return listCatalogue.get(0);
 	    }
 	    else{
 	    	return new CatalogueLanguage();
 	    }
       
    }

    /**
     * Search users by name service facade.
     *
     * @param name the name
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<UserAccount> searchUsersByNameServiceFacade(String name) throws ServiceException {
    	List<UserAccount> listUsers = userExceptionServiceBean.serchUsersByNameServiceBean(name);
        if(listUsers!= null && listUsers.size()>0){
	    	return listUsers;
	    }
	    else{
	    	return new ArrayList<UserAccount>();
	    }
    }
    
    /**
     * Search user by user login service facade.
     *
     * @param userLogin the user login
     * @return the user account
     * @throws ServiceException the service exception
     */
    public UserAccount searchUserByUserLoginServiceFacade(String userLogin) throws ServiceException {
    	return userExceptionServiceBean.searchUserByUserLoginServiceBean(userLogin);
    }

    /**
     * List system options by language user exception service facade.
     *
     * @param userExceptionFilter the user exception filter
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<Object[]> listSystemOptionsByLanguageUserExceptionServiceFacade(UserExceptionFilter userExceptionFilter) throws ServiceException {
        return userExceptionServiceBean.listSystemOptionsByLanguageServiceBean(userExceptionFilter);
    }

    /**
     * List option privilege service facade.
     *
     * @param userExceptionFilter the user exception filter
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<Object[]> listOptionPrivilegeServiceFacade(UserExceptionFilter userExceptionFilter) throws ServiceException {
        return userExceptionServiceBean.listSystemOptionsBySystemServiceBean(userExceptionFilter);
    }

    /**
     * delete privilege user exception service facade.
     *
     * @param lstPrivilages the lst privilages
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean deletePrivilegeUserExceptionServiceFacade(List<Integer> lstPrivilages) throws ServiceException {
    		userExceptionServiceBean.deletePrivilegeUserExceptionServiceBean(lstPrivilages);
    	return true;
    }
    
    /**
     * Search user exceptions by filter service facade.
     *
     * @param userExceptionFilter the user exception filter
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<ExceptionUserPrivilege> searchUserExceptionsByFilterServiceFacade(UserExceptionFilter userExceptionFilter) throws ServiceException {
    	List<ExceptionUserPrivilege> listUserExceptions = userExceptionServiceBean.searchUserExceptionsByFiltersServiceBean(userExceptionFilter);
        if(listUserExceptions!= null && listUserExceptions.size()>0){
	    	return listUserExceptions;
	    }
	    else{
	    	return new ArrayList<ExceptionUserPrivilege>();
	    }
    }

    /**
     * Gets the request profile modify.
     *
     * @param systemProfile the system profile
     * @return the request profile modify
     */
    public SystemProfileRequest getRequestProfileModify(SystemProfile systemProfile){
    	List<Integer> statesRequired = new ArrayList<>();
    	statesRequired.add(SystemProfileRequestStateType.REGISTERED.getCode());
    	return systemProfileServiceBean.getRegisteredProfileRequest(systemProfile.getIdSystemProfilePk(), statesRequired,false);
    }
    
    /**
     * Ger request profile request detail.
     *
     * @param systemProfile the system profile
     * @return the system profile request
     */
    public SystemProfileRequest gerRequestProfileRequestDetail(SystemProfile systemProfile){
    	List<Integer> statesRequired = new ArrayList<>();
    	statesRequired.add(SystemProfileRequestStateType.REGISTERED.getCode());
    	return systemProfileServiceBean.getRegisteredProfileRequest(systemProfile.getIdSystemProfilePk(), statesRequired,true);
    }
    
    /**
     * Register modify request profile.
     *
     * @param systemProfile the system profile
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean registerModifyRequestProfile(SystemProfile systemProfile) throws ServiceException{
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify()); 
    	loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
    	SystemProfileRequest systemProfileRequest = new SystemProfileRequest();
    	systemProfileRequest.setSystemProfile(systemProfile);
    	systemProfileRequest.setProfileName(systemProfile.getName());
    	systemProfileRequest.setProfileDescription(systemProfile.getDescription());
    	systemProfileRequest.setProfileType(systemProfile.getProfileType());
    	systemProfileRequest.setRegistryDate(CommonsUtilities.currentDateTime());
    	systemProfileRequest.setRegistryUser(loggerUser.getUserName());
    	systemProfileRequest.setRequestState(SystemProfileRequestStateType.REGISTERED.getCode());
    	List<ProfilePrivilegeRequest> privileges = new ArrayList<>();
    	ProfilePrivilegeRequest privilegeRequest = null;
    	for(ProfilePrivilege profilePrivilege : systemProfile.getProfilePrivileges()){
    		if(profilePrivilege.getState().equals(ProfilePrivilegeStateType.REGISTERED.getCode())){
    			privilegeRequest = new ProfilePrivilegeRequest();
    			//java.lang.System.out.println("systemProfileRequest::> "+systemProfileRequest.toString());
    			privilegeRequest.setSystemProfileRequest(systemProfileRequest);
    			privilegeRequest.setOptionPrivilege(profilePrivilege.getOptionPrivilege());
    			//java.lang.System.out.println("optionPrivilege::> "+profilePrivilege.getOptionPrivilege().toString());
    			//java.lang.System.out.println("privilegeRequest::> "+privilegeRequest.toString());
    			privileges.add(privilegeRequest);
    			privilegeRequest = null;
    		}
    	}
    	systemProfileRequest.setProfilePrivilegeRequests(privileges);
		//java.lang.System.out.println("systemProfileRequest::> "+systemProfileRequest.toString());
    	systemProfileServiceBean.create(systemProfileRequest);
    	//update situation of systemProfile original
    	systemProfileServiceBean.updateSituationProfile(systemProfile.getIdSystemProfilePk(),
    			SystemProfileSituationType.PENDING_MODIFICATE.getCode(), loggerUser);
    	
    	// Se envia email y notificacion
      	Object[] parameters = {systemProfile.getName()};
      	restServiceNotification.sendNotification(loggerUser,BusinessProcessType.SYSTEM_PROFILE_MODIFICATION.getCode(),parameters);
    	
    	return true;
    }
    
    /**
     * Confirm modify profile.
     *
     * @param systemProfileRequest the system profile request
     * @param systemProfile the system profile
     * @throws ServiceException the service exception
     */
    public void confirmModifyProfile(SystemProfileRequest systemProfileRequest,SystemProfile systemProfile) throws ServiceException{
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirmModify()); 
    	loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
    	systemProfileRequest.setRequestState(SystemProfileRequestStateType.CONFIRMED.getCode());
    	systemProfileRequest.setConfirmationDate(loggerUser.getAuditTime());
    	systemProfileRequest.setConfirmationUser(loggerUser.getUserName());
    	SystemProfileFilter systemProfileFilter = new SystemProfileFilter();
    	systemProfileFilter.setIdSystemProfile(systemProfile.getIdSystemProfilePk());
		systemProfileFilter.setProfilePrivilegeState(ProfilePrivilegeStateType.REGISTERED.getCode());

		//java.lang.System.out.println("systemProfileFilter::> "+systemProfileFilter.toString());
		
		List<ProfilePrivilege> profilePrivileges = listPrivilegesByProfileServiceFacade(systemProfileFilter);
		//delete old privileges
		for(ProfilePrivilege privilege : profilePrivileges){
			privilege.setState(ProfilePrivilegeStateType.DELETED.getCode());
			//java.lang.System.out.println("ProfilePrivilege::> "+privilege.toString());
		}
		//change data
		systemProfile.setName(systemProfileRequest.getProfileName());
		systemProfile.setDescription(systemProfileRequest.getProfileDescription());
		systemProfile.setSituation(SystemProfileSituationType.ACTIVE.getCode());
		systemProfile.setProfileType(systemProfileRequest.getProfileType());
		ProfilePrivilege profilePrivilege=null;
		for(ProfilePrivilegeRequest privilegeRequest : systemProfileRequest.getProfilePrivilegeRequests()){
			profilePrivilege = new ProfilePrivilege();
			profilePrivilege.setSystemProfile(systemProfile);
			profilePrivilege.setOptionPrivilege(privilegeRequest.getOptionPrivilege());
			profilePrivilege.setState(ProfilePrivilegeStateType.REGISTERED.getCode());
			profilePrivilege.setRegistryUser(loggerUser.getUserName());
			profilePrivilege.setRegistryDate(CommonsUtilities.currentDateTime());			
			profilePrivileges.add(profilePrivilege);
			//java.lang.System.out.println("privilegeRequest FOR UPDATE::> "+privilegeRequest.toString());
			//java.lang.System.out.println("profilePrivilege FOR::> "+profilePrivilege.toString());
		}
		//java.lang.System.out.println("systemProfile update::> "+systemProfile.toString());
		systemProfile.setProfilePrivileges(profilePrivileges);
		//update entities
		systemProfileServiceBean.update(systemProfileRequest);
		systemProfileServiceBean.update(systemProfile);
		
		// Se envia email y notificacion
      	Object[] parameters = {systemProfile.getName()};
      	restServiceNotification.sendNotification(loggerUser, 
      			BusinessProcessType.SYSTEM_PROFILE_CONFIRM_REGISTRATION.getCode(), parameters);
    }
    
    /**
     * Reject modify profile.
     *
     * @param systemProfileRequest the system profile request
     * @throws ServiceException the service exception
     */
    public void rejectModifyProfile(SystemProfileRequest systemProfileRequest) throws ServiceException{
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRejectModify()); 
    	loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
    	systemProfileRequest.setRequestState(SystemProfileRequestStateType.REJECTED.getCode());
    	systemProfileServiceBean.update(systemProfileRequest);
    	systemProfileServiceBean.updateSituationProfile(systemProfileRequest.getSystemProfile().getIdSystemProfilePk(),
    			SystemProfileSituationType.ACTIVE.getCode(), loggerUser);
    	
    	// Se envia email y notificacion
      	Object[] parameters = {systemProfileRequest.getSystemProfile().getName()};
      	restServiceNotification.sendNotification(loggerUser, 
      			BusinessProcessType.SYSTEM_PROFILE_REJECT_REGISTRATION.getCode(), parameters);
    }
    
    /**
	 * Parameter by pk.
	 *
	 * @param parametersId the parameters id
	 * @return the Object
	 * @throws ServiceException the service exception
	 */
	public Parameter getParameterByPk(Integer parametersId) throws ServiceException{
		return parameterSecurityServiceBean.getParameterByPk(parametersId);
	}
	/*add parche 03/04/2017*/
	public void saveTicketDatamart(TicketsDatamart ticketsDatamart) throws ServiceException{
		ticketsDatamart.setStartDate(CommonsUtilities.currentDate());
		ticketsDatamart.setLastDate(CommonsUtilities.currentDate());
		ticketsDatamart.setExpirationDate(CommonsUtilities.addDate(ticketsDatamart.getStartDate(), 1));		
		TicketsDatamart obj =  getTicketDatamart(ticketsDatamart.getIdTicketsDatamartPk());
		
		if( !(obj != null && obj.getIdTicketsDatamartPk() != null &&  !obj.getIdTicketsDatamartPk().equals("") ) ){
			systemProfileServiceBean.create(ticketsDatamart);
		}
	}
	
	public void reloadTicketDatamart(TicketsDatamart ticketsDatamart) throws ServiceException{
		ticketsDatamart.setLastDate(ticketsDatamart.getStartDate());
		ticketsDatamart.setStartDate(CommonsUtilities.currentDate());
		ticketsDatamart.setExpirationDate(CommonsUtilities.addDate(ticketsDatamart.getStartDate(), 1));		
		systemProfileServiceBean.update(ticketsDatamart);
	}
	
	public TicketsDatamart getTicketDatamart(String idTicketsDatamartPk) throws ServiceException{		
		return systemProfileServiceBean.find(idTicketsDatamartPk, TicketsDatamart.class);
	}
	/*end parche 03/04/2017*/
}