/*
 * 
 */
package com.pradera.security.systemmgmt.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.security.model.type.UserAccountStateType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.model.ScheduleException;
import com.pradera.security.model.ScheduleExceptionDetail;
import com.pradera.security.model.UserAccount;
import com.pradera.security.model.type.ScheduleDetailStateType;
import com.pradera.security.model.type.ScheduleExceptionStateType;
import com.pradera.security.model.type.ScheduleExceptionType;
import com.pradera.security.model.type.SystemProfileStateType;
import com.pradera.security.model.type.UserProfileStateType;
import com.pradera.security.systemmgmt.view.filters.ScheduleExceptionFilter;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Class ExcepcionHorarioServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 22/11/2012
 */
@Stateless
public class ScheduleExceptionServiceBean extends CrudSecurityServiceBean{	
	
	@Inject
	PraderaLogger log;
	/**
	 * Obtener excepcion horario service bean.
	 *
	 * @param h the h
	 * @return the excepcion horario
	 * @throws ServiceException the service exception
	 */
	public ScheduleException getScheduleExceptionServiceBean(ScheduleException scheduleException) throws ServiceException{
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT h from ScheduleException h LEFT JOIN FETCH h.scheduleExceptionDetails he LEFT JOIN FETCH he.userAccount us ").
			append("LEFT JOIN FETCH us.institutionsSecurity ").
			append("LEFT JOIN FETCH h.system ").
			append("WHERE h.idScheduleExceptionPk = ").
			append(scheduleException.getIdScheduleExceptionPk());
		 
		return (ScheduleException)em.createQuery(sql.toString(),ScheduleException.class).getSingleResult();
	}
	 /**
     * 
     * @param idScheduleExceptionPk
     * @return Date
     * @throws ServiceException
     */
    public Date getLastModifedDetails(Long idScheduleExceptionPk)throws ServiceException{
    	StringBuffer sql = new StringBuffer();
		sql.append(" Select max(EH.lastModifyDate) From ScheduleException EH ");
		sql.append(" Where EH.idScheduleExceptionPk = :idScheduleExceptionPk ");	
		Query query = em.createQuery(sql.toString());
		query.setParameter("idScheduleExceptionPk",idScheduleExceptionPk);
		query.setHint("org.hibernate.cacheable", true);
		return (Date)query.getSingleResult();
    }
	/**
	 * Obtener excepcion horarios service bean.
	 *
	 * @param h the h
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ScheduleException> getScheduleExceptionsServiceBean(ScheduleExceptionFilter scheduleExceptionFilter) throws ServiceException{
		//Este metodo usara Criteria para el Listado de Horarios
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<ScheduleException> criteriaQuery = criteriaBuilder.createQuery(ScheduleException.class);
		Root<ScheduleException> scheduleExceptionRoot = criteriaQuery.from(ScheduleException.class);
		List<Predicate> criteriaParameters = new ArrayList<Predicate>();
		criteriaQuery.orderBy(criteriaBuilder.asc(scheduleExceptionRoot.get("idScheduleExceptionPk")));
		criteriaQuery.select(scheduleExceptionRoot);

		if(Validations.validateIsNotNullAndNotEmpty(scheduleExceptionFilter.getScheduleDetailExceptionFilter())){
			Join<ScheduleException,ScheduleExceptionDetail> scheduleExceptionDetailJoin = scheduleExceptionRoot.join("scheduleExceptionDetails");
			if(Validations.validateIsNotNullAndNotEmpty(scheduleExceptionFilter.getScheduleDetailExceptionFilter().getUserFilter().getIdUserAccountPK())){
				Join<ScheduleExceptionDetail,UserAccount> joinUser = scheduleExceptionDetailJoin.join("userAccount",JoinType.INNER);
				criteriaParameters.add(criteriaBuilder.equal(joinUser.get("idUserPk"),scheduleExceptionFilter.getScheduleDetailExceptionFilter().getUserFilter().getIdUserAccountPK()));
				criteriaParameters.add(criteriaBuilder.equal(joinUser.get("state"),scheduleExceptionFilter.getScheduleDetailExceptionFilter().getUserFilter().getUserAccountState()));
			}
			if(Validations.validateIsNotNullAndNotEmpty(scheduleExceptionFilter.getScheduleDetailExceptionFilter().getScheduleExceptionDetailState())){
				criteriaParameters.add(criteriaBuilder.equal(scheduleExceptionDetailJoin.get("state"),scheduleExceptionFilter.getScheduleDetailExceptionFilter().getScheduleExceptionDetailState()));
			}
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(scheduleExceptionFilter.getScheduleExceptionType()) && Validations.validateIsPositiveNumber(scheduleExceptionFilter.getScheduleExceptionType()) ){
			criteriaParameters.add(criteriaBuilder.equal(scheduleExceptionRoot.get("exceptionTypeFk"),scheduleExceptionFilter.getScheduleExceptionType()));
		}
		if(Validations.validateIsNotNullAndNotEmpty(scheduleExceptionFilter.getInitialDate())){
			//criteriaParameters.add(criteriaBuilder.greaterThanOrEqualTo(scheduleExceptionRoot.<Date>get("initialDate"),scheduleExceptionFilter.getInitialDate()));
			
			//Initial date compared with register Date
			criteriaParameters.add(criteriaBuilder.greaterThanOrEqualTo(scheduleExceptionRoot.<Date>get("registryDate"),scheduleExceptionFilter.getInitialDate()));
			//criteriaParameters.add(criteriaBuilder.equal(joinUser.get("registryDate"),scheduleExceptionFilter.getScheduleDetailExceptionFilter()));
			
		}
		if(Validations.validateIsNotNullAndNotEmpty(scheduleExceptionFilter.getFinalDate()) ){
			//Final date compared with register Date
			criteriaParameters.add(criteriaBuilder.lessThan(scheduleExceptionRoot.<Date>get("registryDate"),CommonsUtilities.addDate(scheduleExceptionFilter.getFinalDate(), 1)));
		}
		if(Validations.validateIsNotNullAndNotEmpty(scheduleExceptionFilter.getInitialHour())){
			criteriaParameters.add(criteriaBuilder.greaterThanOrEqualTo(scheduleExceptionRoot.<Date>get("initialHour"),scheduleExceptionFilter.getInitialHour()));
		}
		if(Validations.validateIsNotNullAndNotEmpty(scheduleExceptionFilter.getFinalHour()) ){
			criteriaParameters.add(criteriaBuilder.lessThan(scheduleExceptionRoot.<Date>get("endHour"),scheduleExceptionFilter.getFinalHour()));
		}
		if(Validations.validateIsNotNullAndNotEmpty(scheduleExceptionFilter.getDescript())){
			criteriaParameters.add(criteriaBuilder.like(scheduleExceptionRoot.<String>get("description"),scheduleExceptionFilter.getDescript().toUpperCase()+"%"));
		}
		if(Validations.validateIsNotNullAndNotEmpty(scheduleExceptionFilter.getScheduleExceptionState()) &&  Validations.validateZeroOrPositiveNumber(scheduleExceptionFilter.getScheduleExceptionState())){
			criteriaParameters.add(criteriaBuilder.equal(scheduleExceptionRoot.get("state"),scheduleExceptionFilter.getScheduleExceptionState()));
		}
		
		if (criteriaParameters.size() == 0) {
			criteriaParameters.add(criteriaBuilder.like(scheduleExceptionRoot.<String>get("description"),"%"));
	    } else if (criteriaParameters.size() == 1) {
	        criteriaQuery.where(criteriaParameters.get(0));
	    } else {
	        criteriaQuery.where(criteriaBuilder.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()])));
	    }
		
		TypedQuery<ScheduleException> scheduleExceptionCriteria = em.createQuery(criteriaQuery);
		scheduleExceptionCriteria.setHint("org.hibernate.cacheable", true);
		return scheduleExceptionCriteria.getResultList();
	}
	
	
	/**
	 * Obtener excepcion horario por usuario.
	 *
	 * @param ehFiltro the eh filtro
	 * @return the excepcion horario
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ScheduleException> listScheduleExceptionByUserServiceBean(ScheduleExceptionFilter scheduleExceptionFilter) 
			throws ServiceException{
			StringBuffer sql = new StringBuffer();
			sql.append(" Select EH From ScheduleException EH ");
			sql.append(" Join EH.system S ");
			sql.append(" Join EH.scheduleExceptionDetails EHD ");
			sql.append(" Join EHD.userAccount U ");
			sql.append(" Join U.institutionsSecurity ISEG ");
			sql.append(" Where U.idUserPk = :idUserPrm And U.state = :userStatePrm  ");
			sql.append(" And EHD.state = :scheduleExcepDetailStatePrm And EH.state = :scheduleExcepStarePrm ");
			sql.append(" And EH.exceptionTypeFk = :exceptionTypePrm ");
			sql.append(" And EH.initialDate <= :initialdatePrm ");
			sql.append(" And EH.endDate >= :enddatePrm ");
			sql.append(" And :hourPrm Between EH.initialHour And EH.endHour ");
			
			Query query = em.createQuery(sql.toString());
			query.setParameter("idUserPrm", scheduleExceptionFilter.getScheduleDetailExceptionFilter().getUserFilter().getIdUserAccountPK());
			query.setParameter("userStatePrm", scheduleExceptionFilter.getScheduleDetailExceptionFilter().getUserFilter().getUserAccountState());
			query.setParameter("scheduleExcepDetailStatePrm",scheduleExceptionFilter.getScheduleDetailExceptionFilter().getScheduleExceptionDetailState());
			query.setParameter("scheduleExcepStarePrm", scheduleExceptionFilter.getScheduleExceptionState());
			query.setParameter("exceptionTypePrm", scheduleExceptionFilter.getScheduleExceptionType());
			query.setParameter("initialdatePrm", scheduleExceptionFilter.getDate(), TemporalType.DATE);
			query.setParameter("enddatePrm", scheduleExceptionFilter.getDate(), TemporalType.DATE);
			query.setParameter("hourPrm", scheduleExceptionFilter.getHour(), TemporalType.TIME);
			query.setHint("org.hibernate.cacheable", true);
			return (List<ScheduleException>)query.getResultList();	
	}
	/**
	 * To get the scheduleException by system
	 * @param idSystemPk
	 * @param idUserAccountPk
	 * @return
	 * @throws ServiceException
	 */
	public ScheduleException getScheduleExceptionBySystemServiceBean(
			Integer idSystemPk, Integer idUserAccountPk) throws ServiceException{
		try{
		StringBuffer sql = new StringBuffer();
		sql.append(" Select EH From ScheduleException EH ");
		sql.append(" Join EH.system S ");
		sql.append(" Join EH.scheduleExceptionDetails EHD ");
		sql.append(" Join EHD.userAccount U ");
		sql.append(" Where U.idUserPk = :idUserPrm And U.state = :userStatePrm  ");
		sql.append(" And EHD.state = :scheduleExcepDetailStatePrm And EH.state = :scheduleExcepStarePrm ");
		sql.append(" And EH.exceptionTypeFk = :exceptionTypePrm ");
		sql.append(" And S.idSystemPk = :idSystemPk ");
		sql.append(" And EH.initialDate <= :initialdatePrm ");
		sql.append(" And EH.endDate >= :enddatePrm ");
		sql.append(" And :hourPrm Between EH.initialHour And EH.endHour ");
		
		Query query = em.createQuery(sql.toString());
		query.setParameter("idUserPrm", idUserAccountPk);
		query.setParameter("userStatePrm", UserAccountStateType.CONFIRMED.getCode());
		query.setParameter("scheduleExcepDetailStatePrm",ScheduleDetailStateType.REGISTERED.getCode());
		query.setParameter("scheduleExcepStarePrm", ScheduleExceptionStateType.REGISTERED.getCode());
		query.setParameter("exceptionTypePrm",ScheduleExceptionType.ACCESS_ACEPT.getCode() );
		query.setParameter("idSystemPk",idSystemPk);
		query.setParameter("initialdatePrm", CommonsUtilities.currentDate(), TemporalType.DATE);
		query.setParameter("enddatePrm", CommonsUtilities.currentDate(), TemporalType.DATE);
		query.setParameter("hourPrm", CommonsUtilities.getDefaultDateWithCurrentTime(), TemporalType.TIME);
		query.setHint("org.hibernate.cacheable", true);
		return (ScheduleException) query.getSingleResult();
	} catch(NoResultException ex){
		return null;
	} catch(NonUniqueResultException ex){
		return null;
	}
	}
	public List<UserAccount> checkUserWithSystemProfileServiceBean(Integer systemID, List<Integer> userIDs)throws ServiceException{
		StringBuffer sql = new StringBuffer();
		sql.append(" Select UP.userAccount From UserProfile UP ");
		sql.append(" Join UP.systemProfile SP ");
		sql.append(" where UP.state=:upState ");
		sql.append(" and SP.state=:spState ");
		sql.append(" and  UP.userAccount.idUserPk in(:userIDs) ");
		sql.append(" and SP.system.idSystemPk=:sytemID ");
		
		Query query = em.createQuery(sql.toString());
		query.setParameter("upState",UserProfileStateType.REGISTERED.getCode());
		query.setParameter("spState",SystemProfileStateType.CONFIRMED.getCode());
		query.setParameter("userIDs",userIDs);
		query.setParameter("sytemID",systemID);
		query.setHint("org.hibernate.cacheable", true);
		return (List<UserAccount>)query.getResultList();	
	}

}