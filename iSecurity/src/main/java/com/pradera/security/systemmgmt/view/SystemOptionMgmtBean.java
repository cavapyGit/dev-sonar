
package com.pradera.security.systemmgmt.view;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PreDestroy;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.UploadedFile;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.configuration.StageDependent;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.security.model.type.LanguageType;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.utils.view.GeneralPropertiesConstants;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.model.CatalogueLanguage;
import com.pradera.security.model.HistoricalState;
import com.pradera.security.model.OptionPrivilege;
import com.pradera.security.model.Parameter;
import com.pradera.security.model.System;
import com.pradera.security.model.SystemOption;
import com.pradera.security.model.type.DefinitionType;
import com.pradera.security.model.type.InstanceType;
import com.pradera.security.model.type.OptionPrivilegeStateType;
import com.pradera.security.model.type.OptionSituationType;
import com.pradera.security.model.type.OptionStateType;
import com.pradera.security.model.type.OptionType;
import com.pradera.security.model.type.OtherMotiveType;
import com.pradera.security.model.type.PrivilegeType;
import com.pradera.security.model.type.SystemStateType;
import com.pradera.security.systemmgmt.facade.SystemServiceFacade;
import com.pradera.security.utils.view.GenericBaseBean;
import com.pradera.security.utils.view.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary. Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class SystemOptionMgmtBean.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 20/02/2013
 */
@DepositaryWebBean
public class SystemOptionMgmtBean extends GenericBaseBean implements
		Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The Constant log. */
	private static final Logger log = Logger
			.getLogger(SystemOptionMgmtBean.class);

	/** The user info. */
	@Inject
	UserInfo userInfo;

	/** The system option. */
	private SystemOption systemOption;

	/** The system selected. */
	private Integer systemSelected;

	/** The obj system. */
	private System objSystem;

	/** The obj catalog spanish language. */
	private CatalogueLanguage objCatalogSpanishLanguage;

	/** The obj catalog english language. */
	private CatalogueLanguage objCatalogEnglishLanguage;

	/** The obj catalogo frances language. */
	private CatalogueLanguage objCatalogoFrancesLanguage;

	/** The node selected. */
	private TreeNode nodeSelected;

	/** The root. */
	private TreeNode root;

	/** The obj system selected. */
	private System objSystemSelected;

	/** The activate confirm. */
	private boolean activateConfirm;

	/** The activate reject. */
	private boolean activateReject;

	/** The activate block. */
	private boolean activateBlock;

	/** The activate unblock. */
	private boolean activateUnblock;

	/** The activate delete. */
	private boolean activateDelete;

	/** The activate add. */
	private boolean activateAdd;

	/** The activate modify. */
	private boolean activateModify;

	/** The activate consult. */
	private boolean activateConsult;

	/** The activate context menu. */
	private boolean activateContextMenu;

	/** The message dialog confirmation. */
	private String messageDialogConfirmation;

	/** The label action. */
	private String labelAction;

	/** The list option type. */
	private List<OptionType> listOptionType;

	/** The opcion type. */
	private Integer optType;

	/** The view url. */
	private boolean viewURL;

	/** The url option. */
	private String urlOption;

	/** The parameter data model. */
	private ParameterDataModel parameterDataModel;

	/** The privilege option. */
	private OptionPrivilege privilegeOption;

	/** The list privileges. */
	private List<Parameter> listPrivileges;

	/** The privileges selected. */
	private Parameter[] privilegesSelected;

	/** The before privileges. */
	private List<OptionPrivilege> beforePrivileges;

	/** The flag next. */
	boolean flagNext = true;

	/** The list system option. */
	private List<SystemOption> listSystemOption = new ArrayList<SystemOption>();

	/** The aux event node. */
	private OptionCatalogNodeBean auxEventNode;

	/** The flag view tab spanish language. */
	private boolean flagViewTabSpanishLanguage;

	/** The flag view tab english language. */
	private boolean flagViewTabEnglishLanguage;

	/** The flag view tab frances language. */
	private boolean flagViewTabFrancesLanguage;

	/** The flag disable. */
	private boolean flagDisable;

	/** The flag view option mode. */
	private boolean flagViewOptionMode;

	/** The show dialog confirm. */
	private boolean showDialogConfirm;

	/** The motive selected. */
	private Integer motiveSelected;

	/** The show dialog motive. */
	private boolean showDialogMotive;

	/** The show end transaction. */
	private boolean showEndTransaction;

	/** The show motive text. */
	private boolean showMotiveText;

	/** The motive text. */
	private String motiveText = "";

	/** The Order of the option. */
	private Integer orderOption;

	/** The item not selected. */
	private boolean itemNotSelected;

	/** The intial modifed date. */
	private Date intialModifedDate;

	/** The bmotive descripcion. */
	private String bmotiveDescripcion;

	/** The ubmotive descripcion. */
	private String ubmotiveDescripcion;

	/**  block other motive description. */
	private String botherMotiveDescripcion;

	/**  unblock other motive description. */
	private String ubotherMotiveDescripcion;

	/** The active index. */
	private Integer activeIndex;

	/** The old order option. */
	private Integer oldOrderOption;
	
	/** The show confirm reject in modal. */
	private boolean showConfirmRejectInModal;
	
	private String pdfFileUploadFiltTypes="doc|docx|xls|xlsx|pdf";
	
	private String fUploadFileSizeMgmt="3000000";
	
	private String fUploadFileSizeDisplayMgmt="3000";
	
	private String documentFileName;
	
	private String documentFilePath;
	
	private byte[] documentFile;

	private String fUploadFileTypes="gif|jpg|jpeg|pdf|xml|png";
	
	@Inject
	@Configurable
	private String destinationPathOption;
	
	@Inject
	@StageDependent
	private String fileRootPath;
	/*-------------------------------------------------------------------------------------------------*/

	/**
	 * Instantiates a new system option mgmt bean.
	 */
	public SystemOptionMgmtBean() {
		systemOption = new SystemOption();
		objSystem = new System();
		objCatalogSpanishLanguage = new CatalogueLanguage();
		objCatalogEnglishLanguage = new CatalogueLanguage();
		objCatalogoFrancesLanguage = new CatalogueLanguage();

	}

	/**
	 * Inits the.
	 */
	public void init() {
		if (!FacesContext.getCurrentInstance().isPostback()) {
			try {
				beginConversation();
				listSystemsByState(SystemStateType.CONFIRMED.getCode());//Load all Confirmed Systems
				loadingMenuBySystem();
				setActiveIndex(GeneralConstants.ZERO_VALUE_INTEGER);
				// To load the block and unblock motives for system option based
				// on the definition table FK value in the parameter table
				lstBlockMotives = systemServiceFacade
						.listParameterByDefinitionServiceFacade(DefinitionType.SYS_OPT_BLOCK_MOTIVES
								.getCode());
				lstUnBlockMotives = systemServiceFacade
						.listParameterByDefinitionServiceFacade(DefinitionType.SYS_OPT_DESBLOCK_MOTIVES
								.getCode());
				
				lstMotives = systemServiceFacade
						.listParameterByDefinitionServiceFacade(DefinitionType.SYS_OPT_DELETE_MOTIVES
								.getCode());
				

				lstRejectMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.SYS_OPT_REJECT_MOTIVES.getCode());
				
			} catch (Exception ex) {
				excepcion.fire(new ExceptionToCatchEvent(ex));
			}
		}
	}

	/**
	 * Loading menu by system.
	 */
	public void loadingMenuBySystem() {
		try {
			activateContextMenu = false;
			
			if(systemSelected != null){
				List<Object[]> objList = systemServiceFacade.listOptionCataloguePrivilegesBySystemServiceBean(systemSelected);

				objSystemSelected = findSystemSelected();
				
				if (objSystemSelected != null) {
					OptionCatalogNodeBean optionCatalogueNodeBean = new OptionCatalogNodeBean();
					optionCatalogueNodeBean.setName(objSystemSelected.getName());
					optionCatalogueNodeBean.setOptionUrl(objSystemSelected.getOptionUrl());
					optionCatalogueNodeBean.setState(objSystemSelected.getState());
					optionCatalogueNodeBean.setSituation(objSystemSelected.getSituation());
					optionCatalogueNodeBean.setInstanceType(InstanceType.SYSTEM.getCode());
					optionCatalogueNodeBean.setIdSystemPk(objSystemSelected.getIdSystemPk());

					root = new DefaultTreeNode("Root", null);
					TreeNode nodoNivel0 = new DefaultTreeNode(optionCatalogueNodeBean, root);

					if (objList.size() > 0) {
						List<Object[]> listParentOption = loadingParentOption(objList);
						for (Object[] arrRow : listParentOption) {
							recursiveLoadingOptions(arrRow, objList, nodoNivel0);
						}
					}
					optionsOnFalse();
					activateContextMenu = false;
				}else{
					root = null;
				}
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}

	/**
	 * Loading parent option.
	 * 
	 * @param listaOpcionSistema
	 *            the lista opcion sistema
	 * @return the list
	 */
	public List<Object[]> loadingParentOption(List<Object[]> listaOpcionSistema) {
		List<Object[]> listParentOptions = new ArrayList<Object[]>();

		for (Object[] arrFila : listaOpcionSistema) {
			if (arrFila[1] == null) {
				listParentOptions.add(arrFila);
			}
		}

		return listParentOptions;
	}

	/**
	 * Recursive loading options.
	 * 
	 * @param arrFila
	 *            the arr fila
	 * @param lstSystemOption
	 *            the lst system option
	 * @param fatherNode
	 *            the father node
	 */
	public void recursiveLoadingOptions(Object[] arrFila,
			List<Object[]> lstSystemOption, TreeNode fatherNode) {

		Integer idSystemOptionPk = null;
		Integer idSystemOptionFather = null;
		Integer optionState = null;
		Integer idLanguage = null;
		Integer optionType = null;
		Integer orderOption = null;

		OptionCatalogNodeBean optionCatalogNodeBean = new OptionCatalogNodeBean();
		String language = FacesContext.getCurrentInstance().getViewRoot()
				.getLocale().toString();

		if (arrFila[5] != null) {
			idLanguage = new Integer(arrFila[5].toString());
			if (language.equals(LanguageType.get(idLanguage).getValue())) {
				if (arrFila[0] != null) {
					idSystemOptionPk = new Integer(arrFila[0].toString());
					optionCatalogNodeBean.setIdSystemOptionPk(idSystemOptionPk);
				}

				if (arrFila[1] != null) {
					idSystemOptionFather = new Integer(arrFila[1].toString());
					optionCatalogNodeBean.setIdOptionRoot(idSystemOptionFather);
				}

				if (arrFila[2] != null) {
					optionCatalogNodeBean.setName(arrFila[2].toString());
				}

				if (arrFila[3] != null) {
					optionCatalogNodeBean.setOptionUrl(arrFila[3].toString());
				}

				if (arrFila[4] != null) {
					optionState = new Integer(arrFila[4].toString());
					optionCatalogNodeBean.setState(optionState);
				}

				if (arrFila[6] != null) {
					optionType = new Integer(arrFila[6].toString());
					optionCatalogNodeBean.setOptionType(optionType);
				}

				if (arrFila[7] != null) {
					orderOption = new Integer(arrFila[7].toString());
					optionCatalogNodeBean.setOrderOption(orderOption);
				}
				if (arrFila[8] != null) {
					optionCatalogNodeBean.setLastModifyDate((Date) arrFila[8]);
				}
				
				if (arrFila[9] != null) {
					optionCatalogNodeBean.setSituation(new Integer(arrFila[9].toString()));	
				}							

				optionCatalogNodeBean.setInstanceType(InstanceType.OPTION
						.getCode());

				TreeNode nodo = new DefaultTreeNode(optionCatalogNodeBean,
						fatherNode);

				Integer idSystemOptionFatherSon = null;

				for (Object[] objArrRowChild : lstSystemOption) {
					if (objArrRowChild[1] != null) {
						idSystemOptionFatherSon = new Integer(
								objArrRowChild[1].toString());

						if (idSystemOptionPk.intValue() == idSystemOptionFatherSon
								.intValue()) {
							recursiveLoadingOptions(objArrRowChild,
									lstSystemOption, nodo);
						}

					}
				}
			}
		}

	}

	/**
	 * Find system selected.
	 * 
	 * @return the system
	 */
	public System findSystemSelected() {
		for (System sys : lstSystems) {
			if (systemSelected.equals(sys.getIdSystemPk())) {
				sys.setOptionUrl(sys.getSystemUrl());
				return sys;
			}
		}
		return null;
	}

	/**
	 * Event tree.
	 */
	public void eventTree() {
		objCatalogSpanishLanguage = new CatalogueLanguage();
		objCatalogEnglishLanguage = new CatalogueLanguage();
		objCatalogoFrancesLanguage = new CatalogueLanguage();
		
		flagNext = true;
		activateContextMenu = true;		
		itemNotSelected = false;
		showConfirmRejectInModal = false;
		
		optionsOnFalse();
		OptionCatalogNodeBean objNodoOpcionCatalogoBen = (OptionCatalogNodeBean) nodeSelected.getData();
		if (objNodoOpcionCatalogoBen.getInstanceType().equals(InstanceType.SYSTEM.getCode())) {
			activateAdd = true;
		} else {
			if (objNodoOpcionCatalogoBen.getState().equals(OptionStateType.REGISTERED.getCode())) {	
				if (objNodoOpcionCatalogoBen.getOptionType().equals(OptionType.MODULE.getCode())) {
					activateAdd = true;
				}
				activateModify = true;
				activateConsult = true;
				activateConfirm = true;
				activateReject = true;
//				activateDelete = true;
			} else if (objNodoOpcionCatalogoBen.getState().equals(OptionStateType.CONFIRMED.getCode()) &&
				objNodoOpcionCatalogoBen.getSituation().equals(OptionSituationType.ACTIVE.getCode())) {
				if (objNodoOpcionCatalogoBen.getOptionType().equals(OptionType.MODULE.getCode())) {
					activateAdd = true;
				}
				activateModify = true;
				activateConsult = true;
				activateBlock = true;
				activateDelete = true;
				
			} else if (objNodoOpcionCatalogoBen.getState().equals(OptionStateType.CONFIRMED.getCode()) &&
				objNodoOpcionCatalogoBen.getSituation().equals(OptionSituationType.PENDING_LOCK.getCode())) {					
				if (objNodoOpcionCatalogoBen.getOptionType().equals(OptionType.MODULE.getCode())) {
					activateAdd = true;
				}
				activateModify = true;
				activateConsult = true;
				activateBlock = true;
			} else if (objNodoOpcionCatalogoBen.getState().equals(OptionStateType.CONFIRMED.getCode()) &&
					objNodoOpcionCatalogoBen.getSituation().equals(OptionSituationType.PENDING_DELETE.getCode())) {					
					if (objNodoOpcionCatalogoBen.getOptionType().equals(OptionType.MODULE.getCode())) {
						activateAdd = true;
					}
					activateModify = true;
					activateConsult = true;
					activateDelete = true;
			} else if (objNodoOpcionCatalogoBen.getState().equals(OptionStateType.BLOCKED.getCode()) &&
				objNodoOpcionCatalogoBen.getSituation().equals(OptionSituationType.ACTIVE.getCode())) {
				activateUnblock = true;
//				activateDelete = true;
				activateConsult = true;
			} else if (objNodoOpcionCatalogoBen.getState().equals(OptionStateType.BLOCKED.getCode()) &&
				objNodoOpcionCatalogoBen.getSituation().equals(OptionSituationType.PENDING_UNLOCK.getCode())) {
				activateUnblock = true;
				activateConsult = true;
			}
		}
		activateContextMenu = true;
	}

	/**
	 * Options on false.
	 */
	public void optionsOnFalse() {
		activateAdd = false;
		activateModify = false;
		activateConsult = false;
		activateConfirm = false;
		activateReject = false;
		activateBlock = false;
		activateUnblock = false;
		activateDelete = false;
	}

	/**
	 * Adds the option.
	 */
	public void addOption() {
		try {
			parameterDataModel = null;
			optionsOnFalse();
			hideDialogues();
			this.setOperationType(ViewOperationsType.REGISTER.getCode());
			
			if (nodeSelected == null) {
				itemNotSelected = true;
				JSFUtilities.executeJavascriptFunction("PF('confitemSelectedmsgvar').show()");
				return;
			}

			setActiveIndex(GeneralConstants.ZERO_VALUE_INTEGER);

			activateAdd = true;
			objCatalogEnglishLanguage = new CatalogueLanguage();
			objCatalogSpanishLanguage = new CatalogueLanguage();
			objCatalogoFrancesLanguage = new CatalogueLanguage();
			privilegesSelected = null;
			urlOption = "";
			documentFile = null;
			documentFileName = "";
			Integer parentidpk = null;
			Integer systemidpk = null;
			OptionCatalogNodeBean selectedNode = (OptionCatalogNodeBean) nodeSelected.getData();
			if (Validations.validateIsNotNullAndNotEmpty(selectedNode.getIdSystemOptionPk())) {
				parentidpk = selectedNode.getIdSystemOptionPk();
			}
			if (selectedNode.getInstanceType().equals(InstanceType.SYSTEM.getCode())) {
				systemidpk = selectedNode.getIdSystemPk();
			}
			Integer maxOrderOption = systemServiceFacade.getMaxOrderOptionServiceFacade(parentidpk, systemidpk);
			if (Validations.validateIsNull(maxOrderOption)) {
				maxOrderOption = GeneralConstants.ZERO_VALUE_INTEGER;
			}
			orderOption = maxOrderOption + 1;
			oldOrderOption = maxOrderOption + 1;

			listOptionType = OptionType.list;
			optType = OptionType.OPTION.getCode();
			viewURL = true;
			listPrivileges = systemServiceFacade.searchPrivilegesByDefinitionServiceBean(DefinitionType.PRIVILEGES_OPTION.getCode());
			parameterDataModel = new ParameterDataModel(listPrivileges);
			flagDisable = false;
			flagViewTabSpanishLanguage = true;
			flagViewTabEnglishLanguage = true;
			flagViewTabFrancesLanguage = true;
			flagViewOptionMode = true;

			labelAction = PropertiesUtilities.getMessage(
					FacesContext.getCurrentInstance().getViewRoot().getLocale(),
					PropertiesConstants.ADM_OPTION_TABVIEW_TAB_HEADER_ADD);
			
			JSFUtilities.executeJavascriptFunction("PF('dlgTab').show()");
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Action back.
	 */
	public void actionBack() {
		if (!activateConsult) {
			JSFUtilities.hideGeneralDialogues();
			if (nodeSelected != null) {
				eventTree();
			}
			JSFUtilities.executeJavascriptFunction("PF('confitemSelectedmsgvar').hide()");
			JSFUtilities.executeJavascriptFunction("PF('dlgMotive').hide()");
			JSFUtilities.executeJavascriptFunction("PF('confirmation').hide()");
			JSFUtilities.executeJavascriptFunction("PF('confBackMotive').show()");
			showMotiveText = false;
			motiveSelected = -1;
			JSFUtilities.setValidViewComponent("frmSystemOptionMgmt:txtOtherMotive");
		} else if (activateConsult) {
			refreshHideDialogues();
			JSFUtilities.executeJavascriptFunction("PF('dlgTab').hide()");
		}
	}
	
	/**
	 * Back dialog system option.
	 */
	public void backDialogSystemOption(){
		parameterDataModel = new ParameterDataModel();
		privilegesSelected = new Parameter[0];
		setPrivilegesSelected(privilegesSelected);
		refreshHideDialogues();
	}

	/**
	 * Refresh hide dialogues.
	 */
	public void refreshHideDialogues() {
		hideDialogues();
		if (nodeSelected != null) {
			eventTree();
		}
		JSFUtilities.executeJavascriptFunction("PF('endTrasaction').hide()");
	}

	/**
	 * Hide dialogues.
	 */
	public void hideDialogues() {
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);

		JSFUtilities.hideGeneralDialogues();
		// Del Main
//		JSFUtilities.hideComponent("frmSystemOptionMgmt:modalDialogTab");
//		JSFUtilities.executeJavascriptFunction("PF('rejectwDialog').hide()");
//		JSFUtilities.executeJavascriptFunction("PF('confitemSelectedmsgvar').hide()");
//		JSFUtilities.hideComponent("frmSystemOptionMgmt:alreadyConfirmDialog");
	}

	/**
	 * Validate add.
	 * 
	 * @return true, if successful
	 */
	public boolean validate() {
		boolean flag = true;

		String messageError = "";

		Locale locale = FacesContext.getCurrentInstance().getViewRoot()
				.getLocale();

		String localeES = LanguageType.SPANISH.getValue();
		String localeEN = LanguageType.ENGLISH.getValue();
		String localeFR = LanguageType.FRANCES.getValue();

		/*
		 * if(Validations.validateIsNullOrEmpty(objCatalogSpanishLanguage.
		 * getCatalogueName()) && locale.toString().equals(localeES)){
		 * messageError =
		 * PropertiesConstants.ERROR_ADM_OPTION_CATALOGUELANGUAGE_REQUIRED;
		 * UIInput input1 = (UIInput)
		 * FacesContext.getCurrentInstance().getViewRoot
		 * ().findComponent("frmSystemOptionMgmt:tabGeneral:txtSpanishLanguage"
		 * ); input1.setValid(Boolean.FALSE); JSFUtilities.addContextMessage(
		 * "frmSystemOptionMgmt:tabGeneral:txtSpanishLanguage",
		 * FacesMessage.SEVERITY_ERROR,
		 * PropertiesUtilities.getMessage(messageError),
		 * PropertiesUtilities.getMessage(messageError)); flag=false;
		 * setActiveIndex(GeneralConstants.ZERO_VALUE_INTEGER); }
		 */

		if (Validations.validateIsNullOrEmpty(objCatalogoFrancesLanguage
				.getCatalogueName()) && locale.toString().equals(localeFR)) {
			JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,
					PropertiesConstants.ERROR_ADM_OPTION_CATALOGUELANGUAGE_REQUIRED, null);
			JSFUtilities.executeJavascriptFunction("PF('localCnfwEndTransactionSamePage').show();");

			setActiveIndex(GeneralConstants.ZERO_VALUE_INTEGER);
			
			return Boolean.FALSE;
		}
		if (Validations.validateIsNullOrEmpty(objCatalogEnglishLanguage
				.getCatalogueName()) && locale.toString().equals(localeEN)) {
			JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,
					PropertiesConstants.ERROR_ADM_OPTION_CATALOGUELANGUAGE_REQUIRED, null);
			JSFUtilities.executeJavascriptFunction("PF('localCnfwEndTransactionSamePage').show();");

			setActiveIndex(GeneralConstants.ZERO_VALUE_INTEGER);
			
			return Boolean.FALSE;
		}
		if (Validations.validateIsNullOrEmpty(objCatalogSpanishLanguage
				.getCatalogueName()) && locale.toString().equals(localeES)) {
			
			JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,
					PropertiesConstants.ERROR_ADM_OPTION_CATALOGUELANGUAGE_REQUIRED, null);
			JSFUtilities.executeJavascriptFunction("PF('localCnfwEndTransactionSamePage').show();");

			setActiveIndex(GeneralConstants.ZERO_VALUE_INTEGER);
			
			return Boolean.FALSE;
		}

		if (optType.equals(OptionType.OPTION.getCode())) {
			if (StringUtils.isBlank(urlOption)) {
				JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,
						PropertiesConstants.ERROR_ADM_OPTION_URLOPTION_REQUIRED, null);
				JSFUtilities.executeJavascriptFunction("PF('localCnfwEndTransactionSamePage').show();");
				return Boolean.FALSE;

			}
			if (privilegesSelected.length < 1) {
				JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,
						PropertiesConstants.ERROR_ADM_OPTION_PRIVILEGES_REQUIRED, null);
				JSFUtilities.executeJavascriptFunction("PF('localCnfwEndTransactionSamePage').show();");
				return Boolean.FALSE;

			}
		}
		if (messageError.equals("")) {
			showDialogConfirm = false;
			//JSFUtilities.showRequiredDialog();
			//hideDialogues();
		}

		return flag;

	}
	
	/**
	 * Validate url option.
	 */
	public void validateUrlOption(){
		JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
		JSFUtilities.executeJavascriptFunction("PF('confirmation').hide()");
		JSFUtilities.executeJavascriptFunction("PF('rejectwDialog').hide()");
		JSFUtilities.executeJavascriptFunction("PF('idRemoveOrderCfrDlgWin').hide()");
		
		String regEx = "^[a-zA-Z0-9]?/{0,1}(@{0,1}[a-zA-Z0-9]+.[a-zA-Z]{2,3}){0,1}/?([a-zA-Z0-9-._?,'/=&amp;#/-]?)*";
		Pattern patt = Pattern.compile(regEx);
		Matcher matcher = patt.matcher(urlOption == null ? "" : urlOption);        
        if(!matcher.matches()){
        	setUrlOption("");
			StringBuilder strbMessage = new StringBuilder();
			strbMessage.append(PropertiesUtilities.getMessage(PropertiesConstants.EEROR_URL_PATTERN));
			JSFUtilities.addContextMessage(
					"frmSystemOptionMgmt:txtOption",
					FacesMessage.SEVERITY_ERROR, 
					strbMessage.toString(),
					strbMessage.toString());
	    	JSFUtilities.showEndTransactionSamePageDialog();
	    	JSFUtilities.showMessageOnDialog(
	    			PropertiesConstants.DIALOG_HEADER_ALERT,null,
	    			PropertiesConstants.EEROR_URL_PATTERN,null);	            
		}else{
			JSFUtilities.hideGeneralDialogues();
		}
	}

	/**
	 * Confirm add option.
	 */
	public void registerOption() {
		// JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
		
		if (Validations.validateIsNullOrEmpty(orderOption) || orderOption <= 0) {
			setOrderOption(null);
			
			JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,
					PropertiesConstants.SYSTEM_OPTION_ORDER_REQUIRED, null);
			JSFUtilities.executeJavascriptFunction("PF('localCnfwEndTransactionSamePage').show();");
			return;
		}else {
			try {
				Integer parentidpk = null, systemidpk = null, orderOptionId, optionidPk = null;
				
				OptionCatalogNodeBean selectedNode = (OptionCatalogNodeBean) nodeSelected.getData();
				if (Validations.validateIsNotNullAndNotEmpty(selectedNode.getIdSystemOptionPk())) {
					parentidpk = selectedNode.getIdSystemOptionPk();
				}
				if (selectedNode.getInstanceType().equals(InstanceType.SYSTEM.getCode())) {
					systemidpk = selectedNode.getIdSystemPk();
				}
				orderOptionId = systemServiceFacade.getSystemOptionByOrderOptionServiceFacade(systemidpk,orderOption, optionidPk, parentidpk);
				if (Validations.validateIsNotNullAndNotEmpty(orderOptionId)) {
					JSFUtilities.executeJavascriptFunction("PF('idRemoveOrderCfrDlgWin').show()");
					showMessageOnDialog(
							PropertiesConstants.DIALOG_HEADER_ALERT, null,
							PropertiesConstants.ORDER_ALREADY_REGISTERED, null);
					return;
				}
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		// validateOrderOption();
		if (validate()) {
			JSFUtilities.executeJavascriptFunction("PF('dlgTab').show()");
			showEndTransaction = false;
			Object[] bodyData = new Object[1];
			Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();

			String localeES = LanguageType.SPANISH.getValue();
			String localeEN = LanguageType.ENGLISH.getValue();
			String localeFR = LanguageType.FRANCES.getValue();

			if (StringUtils.isNotBlank(objCatalogSpanishLanguage.getCatalogueName()) && locale.toString().equals(localeES)) {
				bodyData[0] = objCatalogSpanishLanguage.getCatalogueName();
			}
			if (StringUtils.isNotBlank(objCatalogEnglishLanguage.getCatalogueName()) && locale.toString().equals(localeEN)) {
				bodyData[0] = objCatalogEnglishLanguage.getCatalogueName();
			}
			if (StringUtils.isNotBlank(objCatalogoFrancesLanguage.getCatalogueName()) && locale.toString().equals(localeFR)) {
				bodyData[0] = objCatalogoFrancesLanguage.getCatalogueName();
			}
			if (Validations.validateIsNullOrEmpty(bodyData[0])) {
				if (StringUtils.isNotBlank(objCatalogSpanishLanguage.getCatalogueName())) {
					bodyData[0] = objCatalogSpanishLanguage.getCatalogueName();
				} else if (StringUtils.isNotBlank(objCatalogEnglishLanguage.getCatalogueName())) {
					bodyData[0] = objCatalogEnglishLanguage.getCatalogueName();
				} else {
					bodyData[0] = objCatalogoFrancesLanguage.getCatalogueName();
				}
			}
			// Object[] headerData = new Object[1];
			// headerData[0]=PropertiesUtilities.getMessage(locale,"btn.register");

			if (optType.equals(OptionType.OPTION.getCode())) {
				showDialogConfirm = true;
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_REGISTER,
						null,
						PropertiesConstants.LBL_CONFIRM_ADD_OPTION_ACTION,
						bodyData);
				JSFUtilities.executeJavascriptFunction("PF('confirmation').show()");
			} else if (optType.equals(OptionType.MODULE.getCode())) {
				showDialogConfirm = true;
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_REGISTER,
						null,
						PropertiesConstants.LBL_CONFIRM_ADD_MODULE_ACTION,
						bodyData);
				JSFUtilities.executeJavascriptFunction("PF('confirmation').show()");
			}
		}
	}

	/**
	 * Reject situation.
	 */
	@LoggerAuditWeb
	public void rejectSituation(){
		hideDialogues();
		OptionCatalogNodeBean objNodoOpcionCatalogoBean = (OptionCatalogNodeBean) nodeSelected.getData();
//		boolean resultado = false;
		try {
			SystemOption auxSystemOption = systemServiceFacade.findSystemOptionsDetails(objNodoOpcionCatalogoBean.getIdSystemOptionPk());
			if ((objNodoOpcionCatalogoBean.getLastModifyDate().getTime() / 1000 ) < (auxSystemOption.getLastModifyDate().getTime() / 1000)) {
				Object[] argObj = { objNodoOpcionCatalogoBean.getName() };
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,
									PropertiesConstants.SYSTEM_OPTION_ERROR_MODIFIED_RECENT, argObj);
				JSFUtilities.executeJavascriptFunction("PF('alreadyconfirmation').show()");
				loadingMenuBySystem();
				return;
			}
			
			showEndTransaction = true;			
			String messageSuccess = systemServiceFacade.rejectSystemOption(auxSystemOption, auxEventNode, objNodoOpcionCatalogoBean);
			if (messageSuccess!=null) {
				Object[] bodyData = {auxEventNode.getName().toUpperCase()};
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_SUCCESS, null,
						messageSuccess,
						bodyData);
				hideDialogues();
				JSFUtilities.executeJavascriptFunction("PF('endTrasaction').show()");
				loadingMenuBySystem();
				
			}

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Confirm actions.
	 */
	public void confirmActions() {
		JSFUtilities.executeJavascriptFunction("PF('confirmation').hide()");
		if (motiveSelected.equals(Integer.valueOf(-1))) {
			JSFUtilities.addContextMessage(
				"frmSystemOptionMgmt:cboMotive",
				FacesMessage.SEVERITY_ERROR,
				PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_MOTIVE_REQUIRED),
				PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_MOTIVE_REQUIRED));
			return;
		} else if (motiveSelected.equals(OtherMotiveType.SYS_OPT_BLOCK_OTHER_MOTIVE.getCode())
				|| motiveSelected.equals(OtherMotiveType.SYS_OPT_UNBLOCK_OTHER_MOTIVE.getCode())
				|| motiveSelected.equals(OtherMotiveType.SYS_OPT_DELETE_OTHER_MOTIVE.getCode())) {
			if (motiveText == null || motiveText.trim().length() == 0) {
				JSFUtilities.addContextMessage(
					"frmSystemOptionMgmt:txtOtherMotive",
					FacesMessage.SEVERITY_ERROR,
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_MOTIVE_REQUIRED),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_MOTIVE_REQUIRED));
				return;
			}
		}

		hideDialogues();

		if (activateReject) {
			hideDialogues();
			Object[] bodyData = new Object[1];
			OptionCatalogNodeBean objNodoOpcionCatalogoBean = (OptionCatalogNodeBean) nodeSelected.getData();
			if (objNodoOpcionCatalogoBean != null) {
				try {
					Date date = systemServiceFacade
							.getSystemOptionsLastModfiedDetails(objNodoOpcionCatalogoBean
									.getIdSystemOptionPk());
					if (date != null) {
						if (intialModifedDate.compareTo(date) < 0) {
							bodyData[0] = objNodoOpcionCatalogoBean.getName()
									.toUpperCase();
							showMessageOnDialog(
									PropertiesConstants.DIALOG_HEADER_ERROR,
									null,
									"error.options.already.modify",
									bodyData);
							JSFUtilities.executeJavascriptFunction("PF('alreadyconfirmation').show()");
							loadingMenuBySystem();
							return;
						}
					}
				} catch (Exception ex) {
					excepcion.fire(new ExceptionToCatchEvent(ex));
				}
			}
			confirmReject();
		} else if (activateBlock) {
			hideDialogues();
			try {
				OptionCatalogNodeBean objNodoOpcionCatalogoBean = (OptionCatalogNodeBean) nodeSelected.getData();
				Date date = systemServiceFacade.getSystemOptionsLastModfiedDetails(objNodoOpcionCatalogoBean.getIdSystemOptionPk());

				if (date != null) {
					if ((objNodoOpcionCatalogoBean.getLastModifyDate().getTime() / 1000) < (date.getTime() / 1000 )) {
						Object[] bodyData = {objNodoOpcionCatalogoBean.getName().toUpperCase()};
						showMessageOnDialog(
								PropertiesConstants.DIALOG_HEADER_ERROR,
								null,
								PropertiesConstants.ERROR_SYSTEM_OPTIONS_ALREADY_BLOCKED,
								bodyData);
						JSFUtilities.executeJavascriptFunction("PF('alreadyconfirmation').show()");
						loadingMenuBySystem();
						return;
					}
				}
			} catch (Exception ex) {
				excepcion.fire(new ExceptionToCatchEvent(ex));
			}
			confirmBlock();
		} else if (activateUnblock) {
			hideDialogues();
			Object[] bodyData = new Object[1];
			OptionCatalogNodeBean objNodoOpcionCatalogoBean = (OptionCatalogNodeBean) nodeSelected.getData();
			if (objNodoOpcionCatalogoBean != null) {
				try {
					Date date = systemServiceFacade
							.getSystemOptionsLastModfiedDetails(objNodoOpcionCatalogoBean
									.getIdSystemOptionPk());
					if (date != null) {
						if (intialModifedDate.compareTo(date) < 0) {
							bodyData[0] = objNodoOpcionCatalogoBean.getName()
									.toUpperCase();
							showMessageOnDialog(
									PropertiesConstants.DIALOG_HEADER_ERROR,
									null,
									PropertiesConstants.ERROR_SYSTEM_OPTIONS_ALREADY_UNBLOCKED,
									bodyData);
							JSFUtilities.executeJavascriptFunction("PF('alreadyconfirmation').show()");
							loadingMenuBySystem();
							return;
						}
					}
				} catch (Exception ex) {
					excepcion.fire(new ExceptionToCatchEvent(ex));
				}
			}
			confirmUnblock();
		} else if (activateDelete) {
			hideDialogues();
			Object[] bodyData = new Object[1];
			OptionCatalogNodeBean objNodoOpcionCatalogoBean = (OptionCatalogNodeBean) nodeSelected.getData();
			if (objNodoOpcionCatalogoBean != null) {
				try {
					Date date = systemServiceFacade
							.getSystemOptionsLastModfiedDetails(objNodoOpcionCatalogoBean
									.getIdSystemOptionPk());
					if (date != null) {
						if (intialModifedDate.compareTo(date) < 0) {
							bodyData[0] = objNodoOpcionCatalogoBean.getName()
									.toUpperCase();
							showMessageOnDialog(
									PropertiesConstants.DIALOG_HEADER_ERROR,
									null,
									"error.options.already.modify",
									bodyData);
							JSFUtilities.executeJavascriptFunction("PF('alreadyconfirmation').show()");
							loadingMenuBySystem();
							return;
						}
					}
				} catch (Exception ex) {
					excepcion.fire(new ExceptionToCatchEvent(ex));
				}
			}
			confirmDelete();
		}

	}
	
	/**
	 * Reject actions.
	 */
	public void rejectActions() {
		JSFUtilities.executeJavascriptFunction("PF('rejectwDialog').hide()");
		if (motiveSelected.equals(Integer.valueOf(-1))) {
			JSFUtilities.addContextMessage(
				"frmSystemOptionMgmt:cboMotive",
				FacesMessage.SEVERITY_ERROR,
				PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_MOTIVE_REQUIRED),
				PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_MOTIVE_REQUIRED));
			return;
		} else if (motiveSelected.equals(OtherMotiveType.SYS_OPT_BLOCK_OTHER_MOTIVE.getCode())
				|| motiveSelected.equals(OtherMotiveType.SYS_OPT_UNBLOCK_OTHER_MOTIVE.getCode())
				|| motiveSelected.equals(OtherMotiveType.SYS_OPT_DELETE_OTHER_MOTIVE.getCode())) {
			if (motiveText == null || motiveText.trim().length() == 0) {
				JSFUtilities.addContextMessage(
					"frmSystemOptionMgmt:txtOtherMotive",
					FacesMessage.SEVERITY_ERROR,
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_MOTIVE_REQUIRED),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_MOTIVE_REQUIRED));
				return;
			}
		}

		hideDialogues();

		if (activateBlock) {
			hideDialogues();
			try {
				OptionCatalogNodeBean objNodoOpcionCatalogoBean = (OptionCatalogNodeBean) nodeSelected.getData();
				Date date = systemServiceFacade.getSystemOptionsLastModfiedDetails(objNodoOpcionCatalogoBean.getIdSystemOptionPk());
				if (date != null) {
					if ((objNodoOpcionCatalogoBean.getLastModifyDate().getTime() / 1000) < (date.getTime() / 1000 )) {
						Object[] bodyData = {objNodoOpcionCatalogoBean.getName().toUpperCase()};
						showMessageOnDialog(
								PropertiesConstants.DIALOG_HEADER_ERROR,
								null,
								PropertiesConstants.ERROR_SYSTEM_OPTIONS_ALREADY_BLOCKED,
								bodyData);
						JSFUtilities.executeJavascriptFunction("PF('alreadyconfirmation').show()");
						loadingMenuBySystem();
						return;
					}
				}				
			} catch (Exception ex) {
				excepcion.fire(new ExceptionToCatchEvent(ex));
			}
			rejectBlock();
		} else if (activateUnblock) {
			hideDialogues();
			Object[] bodyData = new Object[1];
			OptionCatalogNodeBean objNodoOpcionCatalogoBean = (OptionCatalogNodeBean) nodeSelected.getData();
			if (objNodoOpcionCatalogoBean != null) {
				try {
					Date date = systemServiceFacade
							.getSystemOptionsLastModfiedDetails(objNodoOpcionCatalogoBean
									.getIdSystemOptionPk());
					if (date != null) {
						if (intialModifedDate.compareTo(date) < 0) {
							bodyData[0] = objNodoOpcionCatalogoBean.getName()
									.toUpperCase();
							showMessageOnDialog(
									PropertiesConstants.DIALOG_HEADER_ERROR,
									null,
									PropertiesConstants.ERROR_SYSTEM_OPTIONS_ALREADY_UNBLOCKED,
									bodyData);
							JSFUtilities.executeJavascriptFunction("PF('alreadyconfirmation').show()");
							loadingMenuBySystem();
							return;
						}
					}
				} catch (Exception ex) {
					excepcion.fire(new ExceptionToCatchEvent(ex));
				}
			}
			rejectUnblock();
		} else if(activateDelete){
			try {
				OptionCatalogNodeBean objNodoOpcionCatalogoBean = (OptionCatalogNodeBean) nodeSelected.getData();
				Date date = systemServiceFacade.getSystemOptionsLastModfiedDetails(objNodoOpcionCatalogoBean.getIdSystemOptionPk());
				if (date != null) {
					if ((objNodoOpcionCatalogoBean.getLastModifyDate().getTime() / 1000) < (date.getTime() / 1000 )) {
						Object[] bodyData = {objNodoOpcionCatalogoBean.getName().toUpperCase()};
						showMessageOnDialog(
								PropertiesConstants.DIALOG_HEADER_ERROR,
								null,
								"error.options.already.modify",
								bodyData);
						JSFUtilities.executeJavascriptFunction("PF('alreadyconfirmation').show()");
						loadingMenuBySystem();
						return;
					}
				}				
			} catch (Exception ex) {
				excepcion.fire(new ExceptionToCatchEvent(ex));
			}
			rejectDelete();
		}
	}
	

	/**
	 * Confirm reject.
	 */
	public void confirmReject() {
		hideDialogues();
		JSFUtilities.executeJavascriptFunction("PF('dlgMotive').show()");
		OptionCatalogNodeBean objNodoOpcionCatalogoBean = (OptionCatalogNodeBean) nodeSelected.getData();

		Object[] bodyData = new Object[1];
		Object[] headerData = new Object[1];
		Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		
		String MessageModal = "";
		if(objNodoOpcionCatalogoBean.getState().equals(OptionStateType.REGISTERED.getCode())){
			
			MessageModal = objNodoOpcionCatalogoBean.getOptionType().equals(OptionType.OPTION.getCode())
					? PropertiesConstants.LBL_CONFIRM_REJECT_OPTION_ACTION
					: PropertiesConstants.LBL_CONFIRM_REJECT_MODULE_ACTION;
										
		}else if(objNodoOpcionCatalogoBean.getState().equals(OptionStateType.CONFIRMED.getCode()) &&
				objNodoOpcionCatalogoBean.getSituation().equals(OptionSituationType.PENDING_LOCK.getCode())){
			
			MessageModal = objNodoOpcionCatalogoBean.getOptionType().equals(OptionType.OPTION.getCode())
					? PropertiesConstants.LBL_CONFIRM_REJECT_BLOCK_OPTION_ACTION
					: PropertiesConstants.LBL_CONFIRM_REJECT_BLOCK_MODULE_ACTION;
		}
		
		headerData[0] = PropertiesUtilities.getMessage(locale, "btn.reject");
		bodyData[0] = objNodoOpcionCatalogoBean.getName().toUpperCase();
	
		showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_CONFIRM, null, MessageModal, bodyData);
		JSFUtilities.executeJavascriptFunction("PF('confirmation').show()");
	}

	/**
	 * Confirm block.
	 */
	public void confirmBlock() {
		hideDialogues();
		JSFUtilities.executeJavascriptFunction("PF('dlgMotive').show()");
		OptionCatalogNodeBean objNodoOpcionCatalogoBean = (OptionCatalogNodeBean) nodeSelected.getData();		
		
		String messageModal = "";
		if(objNodoOpcionCatalogoBean.getState().equals(OptionStateType.CONFIRMED.getCode()) &&
			objNodoOpcionCatalogoBean.getSituation().equals(OptionSituationType.ACTIVE.getCode())){
			
			messageModal = objNodoOpcionCatalogoBean.getOptionType().equals(OptionType.OPTION.getCode())
							? PropertiesConstants.LBL_CONFIRM_BLOCK_OPTION_REQUEST_ACTION
							: PropertiesConstants.LBL_CONFIRM_BLOCK_MODULE_REQUEST_ACTION;
			
		}else if(objNodoOpcionCatalogoBean.getState().equals(OptionStateType.CONFIRMED.getCode()) &&
				objNodoOpcionCatalogoBean.getSituation().equals(OptionSituationType.PENDING_LOCK.getCode())){
			messageModal = objNodoOpcionCatalogoBean.getOptionType().equals(OptionType.OPTION.getCode())
					? PropertiesConstants.LBL_CONFIRM_BLOCK_OPTION_ACTION
					: PropertiesConstants.LBL_CONFIRM_BLOCK_MODULE_ACTION;
		}
		
		Object[] bodyData = {objNodoOpcionCatalogoBean.getName().toUpperCase()};
		showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_CONFIRM, null, messageModal, bodyData);
		JSFUtilities.executeJavascriptFunction("PF('confirmation').show()");
	}
	
	/**
	 * Reject block.
	 */
	public void rejectBlock() {
		hideDialogues();
		JSFUtilities.executeJavascriptFunction("PF('dlgMotive').show()");
		OptionCatalogNodeBean objNodoOpcionCatalogoBean = (OptionCatalogNodeBean) nodeSelected.getData();		
		
		String messageModal = "";
		if(objNodoOpcionCatalogoBean.getState().equals(OptionStateType.CONFIRMED.getCode()) &&
				objNodoOpcionCatalogoBean.getSituation().equals(OptionSituationType.PENDING_LOCK.getCode())){
			
			messageModal = objNodoOpcionCatalogoBean.getOptionType().equals(OptionType.OPTION.getCode())
					? PropertiesConstants.LBL_CONFIRM_REJECT_BLOCK_OPTION_ACTION
					: PropertiesConstants.LBL_CONFIRM_REJECT_BLOCK_MODULE_ACTION;
		}
		
		Object[] bodyData = {objNodoOpcionCatalogoBean.getName().toUpperCase()};
		showMessageOnDialog(
				PropertiesConstants.DIALOG_HEADER_BLOCKMOTIVE,
				null, 
				messageModal,
				bodyData);
		JSFUtilities.executeJavascriptFunction("PF('rejectwDialog').show()");
	}

	/**
	 * Confirm unblock.
	 */
	public void confirmUnblock() {
		hideDialogues();
		JSFUtilities.executeJavascriptFunction("PF('dlgMotive').show()");
		OptionCatalogNodeBean objNodoOpcionCatalogoBean = (OptionCatalogNodeBean) nodeSelected.getData();

		String messageModal = "";
		if(objNodoOpcionCatalogoBean.getState().equals(OptionStateType.BLOCKED.getCode()) &&
			objNodoOpcionCatalogoBean.getSituation().equals(OptionSituationType.ACTIVE.getCode())){
			
			messageModal = objNodoOpcionCatalogoBean.getOptionType().equals(OptionType.OPTION.getCode())
							? PropertiesConstants.LBL_CONFIRM_UNBLOCK_OPTION_REQUEST_ACTION
							: PropertiesConstants.LBL_CONFIRM_UNBLOCK_MODULE_REQUEST_ACTION;

		}else if(objNodoOpcionCatalogoBean.getState().equals(OptionStateType.BLOCKED.getCode()) &&
				objNodoOpcionCatalogoBean.getSituation().equals(OptionSituationType.PENDING_UNLOCK.getCode())){
			messageModal = objNodoOpcionCatalogoBean.getOptionType().equals(OptionType.OPTION.getCode())
					? PropertiesConstants.LBL_CONFIRM_UNBLOCK_OPTION_ACTION
					: PropertiesConstants.LBL_CONFIRM_UNBLOCK_MODULE_ACTION;
		}

		Object[] bodyData = {objNodoOpcionCatalogoBean.getName().toUpperCase()};
		showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_CONFIRM, null, messageModal, bodyData);
		JSFUtilities.executeJavascriptFunction("PF('confirmation').show()");
	}
	
	/**
	 * Reject unblock.
	 */
	public void rejectUnblock() {
		hideDialogues();
		JSFUtilities.executeJavascriptFunction("PF('dlgMotive').show()");
		OptionCatalogNodeBean objNodoOpcionCatalogoBean = (OptionCatalogNodeBean) nodeSelected.getData();

		String messageModal = "";
		if(objNodoOpcionCatalogoBean.getState().equals(OptionStateType.BLOCKED.getCode()) &&
				objNodoOpcionCatalogoBean.getSituation().equals(OptionSituationType.PENDING_UNLOCK.getCode())){
			messageModal = objNodoOpcionCatalogoBean.getOptionType().equals(OptionType.OPTION.getCode())
					? PropertiesConstants.LBL_CONFIRM_REJECT_UNBLOCK_OPTION_ACTION
					: PropertiesConstants.LBL_CONFIRM_REJECT_UNBLOCK_MODULE_ACTION;
		}

		Object[] bodyData = {objNodoOpcionCatalogoBean.getName().toUpperCase()};
		showMessageOnDialog(
				PropertiesConstants.DIALOG_HEADER_UNBLOCKMOTIVE,
				null, 
				messageModal,
				bodyData);
		JSFUtilities.executeJavascriptFunction("PF('rejectwDialog').show()");
	}
	
	public void rejectDelete() {
		hideDialogues();
		JSFUtilities.executeJavascriptFunction("PF('dlgMotive').show()");
		OptionCatalogNodeBean objNodoOpcionCatalogoBean = (OptionCatalogNodeBean) nodeSelected.getData();
		String messageModal = "";
		if(objNodoOpcionCatalogoBean.getState().equals(OptionStateType.CONFIRMED.getCode()) &&
				objNodoOpcionCatalogoBean.getSituation().equals(OptionSituationType.PENDING_DELETE.getCode())){
			messageModal = objNodoOpcionCatalogoBean.getOptionType().equals(OptionType.OPTION.getCode())
					? "lbl.confirm.reject.delete.option.action"
					: "lbl.confirm.reject.delete.module.action";
		}
		Object[] bodyData = {objNodoOpcionCatalogoBean.getName().toUpperCase()};
		showMessageOnDialog(
				PropertiesConstants.DIALOG_HEADER_UNBLOCKMOTIVE,
				null, 
				messageModal,
				bodyData);
		JSFUtilities.executeJavascriptFunction("PF('rejectwDialog').show()");
	}

	/**
	 * Confirm delete.
	 */
	public void confirmDelete() {
		hideDialogues();
		JSFUtilities.executeJavascriptFunction("PF('dlgMotive').show()");
		OptionCatalogNodeBean objOptionCatalogNodeBean = (OptionCatalogNodeBean) nodeSelected.getData();
		String messageModal = "";
		if(objOptionCatalogNodeBean.getState().equals(OptionStateType.CONFIRMED.getCode()) &&
				objOptionCatalogNodeBean.getSituation().equals(OptionSituationType.ACTIVE.getCode())){
			messageModal = objOptionCatalogNodeBean.getOptionType().equals(OptionType.OPTION.getCode())
							? "lbl.confirm.delete.option.request.action"
							: "lbl.confirm.delete.module.request.action";

		}else if(objOptionCatalogNodeBean.getState().equals(OptionStateType.CONFIRMED.getCode()) &&
				objOptionCatalogNodeBean.getSituation().equals(OptionSituationType.PENDING_DELETE.getCode())){
			messageModal = objOptionCatalogNodeBean.getOptionType().equals(OptionType.OPTION.getCode())
					? "lbl.confirm.delete.option.action"
					: "lbl.confirm.delete.module.action";
		}

		Object[] bodyData = {objOptionCatalogNodeBean.getName().toUpperCase()};
		showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_CONFIRM, null, messageModal, bodyData);
		JSFUtilities.executeJavascriptFunction("PF('confirmation').show()");
	}

	/**
	 * Confirm modify option.
	 */
	public void confirmModifyOption() {		
		if (Validations.validateIsNullOrEmpty(orderOption) || orderOption <= 0) {
			setOrderOption(null);
			StringBuilder strbMessage = new StringBuilder();
			strbMessage
					.append(PropertiesUtilities
							.getMessage(PropertiesConstants.SYSTEM_OPTION_ORDER_REQUIRED));
			JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,
					PropertiesConstants.SYSTEM_OPTION_ORDER_REQUIRED, null);
			JSFUtilities.executeJavascriptFunction("PF('localCnfwEndTransactionSamePage').show();");
			return;
		} else {
			try {
				Integer parentidpk = null, systemidpk = null, orderOptionId, optionidPk = null;

				OptionCatalogNodeBean parentNode = (OptionCatalogNodeBean) nodeSelected
						.getParent().getData();
				OptionCatalogNodeBean selectedNode = (OptionCatalogNodeBean) nodeSelected
						.getData();
				optionidPk = selectedNode.getIdSystemOptionPk();
				if (Validations.validateIsNotNullAndNotEmpty(parentNode
						.getIdSystemOptionPk())) {
					parentidpk = parentNode.getIdSystemOptionPk();
				}
				if (parentNode.getInstanceType().equals(
						InstanceType.SYSTEM.getCode())) {
					systemidpk = parentNode.getIdSystemPk();
				}
				orderOptionId = systemServiceFacade
						.getSystemOptionByOrderOptionServiceFacade(systemidpk,
								orderOption, optionidPk, parentidpk);
				if (Validations.validateIsNotNullAndNotEmpty(orderOptionId)) {
					orderOption = null;
					/*String msg = PropertiesUtilities
							.getMessage(PropertiesConstants.ORDER_ALREADY_REGISTERED);
					JSFUtilities.addContextMessage(
							"frmSystemOptionMgmt:txtOrderOption",
							FacesMessage.SEVERITY_ERROR, msg, msg);*/
					JSFUtilities.showMessageOnDialog(
							PropertiesConstants.DIALOG_HEADER_ALERT, null,
							PropertiesConstants.ORDER_ALREADY_REGISTERED, null);
					JSFUtilities.executeJavascriptFunction("PF('localCnfwEndTransactionSamePage').show();");
					return;
				}
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (validate()) {
			JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
			//hideDialogues();
			//JSFUtilities.executeJavascriptFunction("PF('dlgTab').show()");
			showEndTransaction = false;
			Object[] bodyData = new Object[1];
			Locale locale = FacesContext.getCurrentInstance().getViewRoot()
					.getLocale();

			String localeES = LanguageType.SPANISH.getValue();
			String localeEN = LanguageType.ENGLISH.getValue();
			String localeFR = LanguageType.FRANCES.getValue();

			if (flagViewTabSpanishLanguage
					&& StringUtils.isNotBlank(objCatalogSpanishLanguage
							.getCatalogueName())
					&& locale.toString().equals(localeES)) {
				bodyData[0] = objCatalogSpanishLanguage.getCatalogueName();
			}
			if (flagViewTabEnglishLanguage
					&& StringUtils.isNotBlank(objCatalogEnglishLanguage
							.getCatalogueName())
					&& locale.toString().equals(localeEN)) {
				bodyData[0] = objCatalogEnglishLanguage.getCatalogueName();
			}
			if (flagViewTabFrancesLanguage
					&& StringUtils.isNotBlank(objCatalogoFrancesLanguage
							.getCatalogueName())
					&& locale.toString().equals(localeFR)) {
				bodyData[0] = objCatalogoFrancesLanguage.getCatalogueName();
			}

			Object[] headerData = new Object[1];
			headerData[0] = PropertiesUtilities
					.getMessage(locale, "btn.modify");

			if (optType.equals(OptionType.OPTION.getCode())) {
				showDialogConfirm = true;

				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_MODIFY,
						null,
						PropertiesConstants.LBL_CONFIRM_MODIFY_OPTION_ACTION,
						bodyData);
				JSFUtilities.executeJavascriptFunction("PF('confirmation').show()");
			} else if (optType.equals(OptionType.MODULE.getCode())) {
				showDialogConfirm = true;

				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_MODIFY,
						null,
						PropertiesConstants.LBL_CONFIRM_MODIFY_MODULE_ACTION,
						bodyData);
				JSFUtilities.executeJavascriptFunction("PF('confirmation').show()");
			}

		}

	}

	/**
	 * Modify option.
	 */
	public void modifyOption() {

		try {
			optionsOnFalse();
			activateModify = true;
			if (nodeSelected == null) {
				itemNotSelected = true;
				JSFUtilities.executeJavascriptFunction("PF('confitemSelectedmsgvar').show()");
				return;
			}
			labelAction = PropertiesUtilities.getMessage(FacesContext
					.getCurrentInstance().getViewRoot().getLocale(),
					PropertiesConstants.ADM_OPTION_TABVIEW_TAB_HEADER_MODIFY);

			listOptionType = OptionType.list;

			objCatalogSpanishLanguage = new CatalogueLanguage();
			objCatalogEnglishLanguage = new CatalogueLanguage();
			objCatalogoFrancesLanguage = new CatalogueLanguage();

			privilegeOption = new OptionPrivilege();

			listPrivileges = new ArrayList<Parameter>();

			flagDisable = false;
			flagViewTabSpanishLanguage = false;
			flagViewTabEnglishLanguage = false;
			flagViewTabFrancesLanguage = false;
			flagViewOptionMode = false;
			viewURL = false;

			Integer languageCode;
			Integer optionType;
			Integer privilege;
			Integer idCatalog;
			Integer idPrivilege;

			Integer it = 0;

			OptionCatalogNodeBean objNodoOpcionCatalogoBean = (OptionCatalogNodeBean) nodeSelected
					.getData();
			orderOption = objNodoOpcionCatalogoBean.getOrderOption();
			// To validate on blur event
			oldOrderOption = objNodoOpcionCatalogoBean.getOrderOption();
			List<Object[]> list;

			Map<Integer, Parameter> mpParam = new HashMap<Integer, Parameter>();

			if (objNodoOpcionCatalogoBean.getOptionType().equals(
					OptionType.OPTION.getCode())) {

				list = systemServiceFacade
						.listPrivilegeCatalogueByOptionServiceBean(objNodoOpcionCatalogoBean
								.getIdSystemOptionPk());
			} else {
				list = systemServiceFacade
						.listCatalogueByOptionServiceBean(objNodoOpcionCatalogoBean
								.getIdSystemOptionPk());
			}

			for (Object[] arrFila : list) {
				it++;

				if (arrFila[4] != null) {
					optionType = new Integer(arrFila[4].toString());
					if (optionType.equals(OptionType.OPTION.getCode())) {
						flagViewOptionMode = true;
						optType = OptionType.OPTION.getCode();
						viewURL = true;
						urlOption = arrFila[2].toString();
						documentFileName = arrFila[15]!= null?arrFila[15].toString():"";
						documentFilePath = arrFila[16]!= null?arrFila[16].toString():"";

						privilege = new Integer(arrFila[9].toString());
						Parameter p = new Parameter();
						p.setName(PrivilegeType.get(privilege).name()
								.toString());
						p.setIdParameterCode(privilege);

						idPrivilege = new Integer(arrFila[10].toString());
						p.setIdParameterPk(idPrivilege);
						if (!mpParam.containsKey(p.getIdParameterCode())) {
							mpParam.put(p.getIdParameterCode(), p);
						}

					} else if (optionType.equals(OptionType.MODULE.getCode())) {
						flagViewOptionMode = false;
						optType = OptionType.MODULE.getCode();
						viewURL = false;
					}

				}
				if (arrFila[5] != null) {
					languageCode = new Integer(arrFila[5].toString());
					if (languageCode.equals(LanguageType.SPANISH.getCode())) {

						objCatalogSpanishLanguage.setCatalogueName(arrFila[6]
								.toString());
						if (arrFila[7] != null) {
							objCatalogSpanishLanguage.setDescription(arrFila[7]
									.toString());
						}
						idCatalog = new Integer(arrFila[8].toString());
						objCatalogSpanishLanguage
								.setIdCatalogueLanguagePk(idCatalog);
					}
					if (languageCode.equals(LanguageType.ENGLISH.getCode())) {

						objCatalogEnglishLanguage.setCatalogueName(arrFila[6]
								.toString());
						if (arrFila[7] != null) {
							objCatalogEnglishLanguage.setDescription(arrFila[7]
									.toString());
						}
						idCatalog = new Integer(arrFila[8].toString());
						objCatalogEnglishLanguage
								.setIdCatalogueLanguagePk(idCatalog);
					}
					if (languageCode.equals(LanguageType.FRANCES.getCode())) {

						objCatalogoFrancesLanguage.setCatalogueName(arrFila[6]
								.toString());
						if (arrFila[7] != null) {
							objCatalogoFrancesLanguage
									.setDescription(arrFila[7].toString());
						}
						idCatalog = new Integer(arrFila[8].toString());
						objCatalogoFrancesLanguage
								.setIdCatalogueLanguagePk(idCatalog);
					}
				}
			}

			flagViewTabSpanishLanguage = true;
			flagViewTabEnglishLanguage = true;
			flagViewTabFrancesLanguage = true;

			beforePrivileges = new ArrayList<OptionPrivilege>();
			OptionPrivilege op;

			listPrivileges = systemServiceFacade
					.searchPrivilegesByDefinitionServiceBean(DefinitionType.PRIVILEGES_OPTION
							.getCode());
			privilegesSelected = new Parameter[mpParam.size()];
			if (listPrivileges != null && listPrivileges.size() > 0) {
				int i = 0;
				for (Parameter p : listPrivileges) {
					/*
					 * Ambiguous using same attribute for 2 diferents values in
					 * mpParam getIdParameterPk() have primary key of
					 * OptionPrivilege and in listPrivileges the
					 * getIdParameterPk() have code of privilege of parameters
					 */
					if (mpParam.containsKey(p.getIdParameterPk())) {
						Parameter ptmp = mpParam.get(p.getIdParameterPk());
						privilegesSelected[i++] = p;
						op = new OptionPrivilege();
						op.setIdOptionPrivilegePk(ptmp.getIdParameterPk());
						beforePrivileges.add(op);
					}
				}
				setPrivilegesSelected(privilegesSelected);
				parameterDataModel = new ParameterDataModel(listPrivileges);
				this.setOperationType(ViewOperationsType.MODIFY.getCode());
				hideDialogues();
				JSFUtilities.executeJavascriptFunction("PF('dlgTab').show()");
			}

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Confirm option.
	 */
	public void confirmOption() {		
		optionsOnFalse();
		hideDialogues();
		this.setOperationType(ViewOperationsType.CONFIRM.getCode());
		if(nodeSelected == null){
			itemNotSelected = true;
			JSFUtilities.executeJavascriptFunction("PF('confitemSelectedmsgvar').show()");
			return;
		}
		activateConfirm=true;
		showDialogConfirm=true;

	    OptionCatalogNodeBean objNodoOpcionCatalogoBean = (OptionCatalogNodeBean)nodeSelected.getData();    
	    intialModifedDate = objNodoOpcionCatalogoBean.getLastModifyDate();
		
		Object[] bodyData = {objNodoOpcionCatalogoBean.getName()};
   
    	JSFUtilities.executeJavascriptFunction("PF('confirmation').show()");
		showMessageOnDialog(
				PropertiesConstants.DIALOG_HEADER_CONFIRM,
				null,
				objNodoOpcionCatalogoBean.getOptionType().equals(OptionType.OPTION.getCode())
				? PropertiesConstants.LBL_CONFIRM_OPTION_ACTION
				: PropertiesConstants.LBL_CONFIRM_MODULE_ACTION,
				bodyData);
	}

	/**
	 * Reject option.
	 */
	public void rejectOption() {
		try {
			optionsOnFalse();
			hideDialogues();
			if (nodeSelected == null) {
				itemNotSelected = true;
				JSFUtilities.executeJavascriptFunction("PF('confitemSelectedmsgvar').show()");
				return;
			}

			activateReject = true;
			motiveSelected = -1;
			showMotiveText = false;
			showDialogMotive = true;
			auxEventNode = (OptionCatalogNodeBean) nodeSelected.getData();
			intialModifedDate = auxEventNode.getLastModifyDate();
			this.setOperationType(ViewOperationsType.REJECT.getCode());
			showMessageOnDialog2(PropertiesConstants.LBL_MOTIVE_REJECT, null,null, null);
			JSFUtilities.executeJavascriptFunction("PF('dlgMotive').show()");
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}

	/**
	 * Block option.
	 *
	 * @param nodo the nodo
	 */
	public void blockOption(TreeNode nodo) {
		motiveText = null;
		showConfirmRejectInModal = false;
		if (!flagNext) {
			return;
		}
		if (nodeSelected == null) {
			itemNotSelected = true;
			JSFUtilities.executeJavascriptFunction("PF('confitemSelectedmsgvar').show()");
			return;
		}
		try {
			showDialogMotive = true;
			showEndTransaction = false;

			optionsOnFalse();
			hideDialogues();
			activateBlock = true;

			OptionCatalogNodeBean objNodoOpcionCatalogoBean = (OptionCatalogNodeBean) nodo.getData();
			/*
			if (objNodoOpcionCatalogoBean.getState().equals(OptionStateType.CONFIRMED.getCode())) {
				
				Object[] bodyData = new Object[2];
				Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
				bodyData[0] = PropertiesUtilities.getMessage(locale,"btn.block");
				bodyData[1] = OptionStateType.REGISTERED.getValue();

				if (objNodoOpcionCatalogoBean.getOptionType().equals(OptionType.MODULE.getCode())) {
					showMessageOnDialog(
							PropertiesConstants.DIALOG_HEADER_WARNING,
							null,
							PropertiesConstants.WARNING_ADM_OPTION_EXCEPTION_LABEL,
							bodyData);
					flagNext = false;
					showDialogMotive = false;
					showEndTransaction = true;
					JSFUtilities.executeJavascriptFunction("PF('endTrasaction').show()");
					return;
				} else if (objNodoOpcionCatalogoBean.getOptionType().equals(OptionType.OPTION.getCode())) {
					showMessageOnDialog(
							PropertiesConstants.DIALOG_HEADER_WARNING,
							null,
							PropertiesConstants.WARNING_ADM_MODULE_EXCEPTION_LABEL,
							bodyData);
					flagNext = false;
					showDialogMotive = false;
					showEndTransaction = true;
					JSFUtilities.executeJavascriptFunction("PF('endTrasaction').show()");
					return;
				}
			}
			
			if (nodo.getChildren().size() > 0) {
				for (int i = 0; i < nodo.getChildren().size(); i++) {
					blockOption(nodo.getChildren().get(i));
				}
			}*/

			if (flagNext) {
				systemOption = new SystemOption();
				listSystemOption = new ArrayList<SystemOption>();
				auxEventNode = (OptionCatalogNodeBean) nodeSelected.getData();
				// showMotives();
				flagNext = true;

				intialModifedDate = auxEventNode.getLastModifyDate();
				//
				if(objNodoOpcionCatalogoBean.getState().equals(OptionStateType.CONFIRMED.getCode()) &&
					objNodoOpcionCatalogoBean.getSituation().equals(OptionSituationType.PENDING_LOCK.getCode())){
					showConfirmRejectInModal = true;
					SystemOption systemOptionTemp = systemServiceFacade.searchSystemOptionByIdServiceBean(objNodoOpcionCatalogoBean.getIdSystemOptionPk());
					
					motiveSelected = systemOptionTemp.getBlockMotive();
					if(systemOptionTemp.getBlockMotive().equals(OtherMotiveType.SYS_OPT_BLOCK_OTHER_MOTIVE.getCode())){
						showMotiveText = true;
						motiveText = systemOptionTemp.getBlockOtherMotive();
					}else{
						showMotiveText = false;
					}
				}else{
					motiveSelected = -1;
					showMotiveText = false;
				}
				
				showMessageOnDialog2(PropertiesConstants.LBL_MOTIVE_BLOCK, null,	null, null);
				this.setOperationType(ViewOperationsType.BLOCK.getCode());
				JSFUtilities.executeJavascriptFunction("PF('dlgMotive').show()");				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Un block option.
	 *
	 * @param nodo the nodo
	 */
	public void unBlockOption(TreeNode nodo) {
		motiveText = null;
		showConfirmRejectInModal = false;
		if (!flagNext) {
			return;
		}
		if (nodeSelected == null) {
			itemNotSelected = true;
			JSFUtilities.executeJavascriptFunction("PF('confitemSelectedmsgvar').show()");
			return;
		}
		showDialogMotive = true;
		showEndTransaction = false;

		optionsOnFalse();
		hideDialogues();
		activateUnblock = true;

		OptionCatalogNodeBean objNodoOptionCatalogBean = (OptionCatalogNodeBean) nodo.getData();

		/*
		if (objNodoOptionCatalogBean.getOptionType().equals(OptionType.OPTION.getCode())
				&& objNodoOptionCatalogBean.getIdOptionRoot() != null) {

			for (int i = 0; i < root.getChildCount(); i++) {
				OptionCatalogNodeBean obj = (OptionCatalogNodeBean) root.getChildren().get(i).getData();

				if (obj.getIdSystemOptionPk() != null && (obj.getIdSystemOptionPk().equals(objNodoOptionCatalogBean.getIdSystemRoot()))) {
					if (obj.getState().equals(OptionStateType.BLOCKED.getCode())) {

						Object[] bodyData = new Object[2];
						Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
						bodyData[0] = PropertiesUtilities.getMessage(locale,"btn.unblock");
						bodyData[1] = OptionStateType.BLOCKED.getValue();

						if (objNodoOptionCatalogBean.getOptionType().equals(OptionType.MODULE.getCode())) {
							showMessageOnDialog(
									null,
									null,
									PropertiesConstants.WARNING_ADM_OPTION_EXCEPTION_LABEL,
									bodyData);
							showEndTransaction = true;
							showDialogMotive = false;
							flagNext = false;
							JSFUtilities.executeJavascriptFunction("PF('endTrasaction').show()");
							return;
						} else if (objNodoOptionCatalogBean.getOptionType().equals(OptionType.OPTION.getCode())) {
							showMessageOnDialog(
									null,
									null,
									PropertiesConstants.WARNING_ADM_MODULE_EXCEPTION_LABEL,
									bodyData);
							showEndTransaction = true;
							showDialogMotive = false;
							flagNext = false;
							JSFUtilities.executeJavascriptFunction("PF('endTrasaction').show()");
							return;
						}
					}
				}
			}
		}
		
		if (nodo.getChildren().size() > 0) {
			for (int i = 0; i < nodo.getChildren().size(); i++) {
				unBlockOption(nodo.getChildren().get(i));
			}
		}*/

		if (flagNext) {
			try {			
			
				systemOption = new SystemOption();
				listSystemOption = new ArrayList<SystemOption>();
				flagNext = true;
							
				auxEventNode = (OptionCatalogNodeBean) nodeSelected.getData();
				intialModifedDate = auxEventNode.getLastModifyDate();

				if(objNodoOptionCatalogBean.getState().equals(OptionStateType.BLOCKED.getCode()) &&
					objNodoOptionCatalogBean.getSituation().equals(OptionSituationType.PENDING_UNLOCK.getCode())){
					
					showConfirmRejectInModal = true;
					SystemOption systemOptionTemp = systemServiceFacade.searchSystemOptionByIdServiceBean(objNodoOptionCatalogBean.getIdSystemOptionPk());
					motiveSelected = systemOptionTemp.getUnblockMotive();
					
					if(systemOptionTemp.getUnblockMotive().equals(OtherMotiveType.SYS_OPT_UNBLOCK_OTHER_MOTIVE.getCode())){
						showMotiveText = true;
						motiveText = systemOptionTemp.getUnblockOtherMotive();
					}else{
						showMotiveText = false;
					}
				}else{
					motiveSelected = -1;
					showMotiveText = false;
				}
				
				showMessageOnDialog2(PropertiesConstants.LBL_MOTIVE_UNBLOCK, null,	null, null);
				this.setOperationType(ViewOperationsType.UNBLOCK.getCode());
				JSFUtilities.executeJavascriptFunction("PF('dlgMotive').show()");				
			}
			catch (ServiceException e) {
					// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	/**
	 * Delete option.
	 */
	public void deleteOption(TreeNode node) {
		motiveText = null;
		showConfirmRejectInModal = false;
		if (!flagNext) {
			return;
		}
		if (nodeSelected == null) {
			itemNotSelected = true;
			JSFUtilities.executeJavascriptFunction("PF('confitemSelectedmsgvar').show()");
			return;
		}
		try {
			showDialogMotive = true;
			showEndTransaction = false;
			optionsOnFalse();
			hideDialogues();
			activateDelete = true;
			OptionCatalogNodeBean objNodoOpcionCatalogoBean = (OptionCatalogNodeBean) node.getData();
			if (flagNext) {
				systemOption = new SystemOption();
				listSystemOption = new ArrayList<SystemOption>();
				auxEventNode = (OptionCatalogNodeBean) nodeSelected.getData();
				// showMotives();
				flagNext = true;
				intialModifedDate = auxEventNode.getLastModifyDate();
				//
				if(objNodoOpcionCatalogoBean.getState().equals(OptionStateType.CONFIRMED.getCode()) &&
					objNodoOpcionCatalogoBean.getSituation().equals(OptionSituationType.PENDING_DELETE.getCode())){
					showConfirmRejectInModal = true;
					SystemOption systemOptionTemp = systemServiceFacade.searchSystemOptionByIdServiceBean(objNodoOpcionCatalogoBean.getIdSystemOptionPk());
					motiveSelected = systemOptionTemp.getRemoveMotive();
					if(systemOptionTemp.getRemoveMotive().equals(OtherMotiveType.SYS_OPT_DELETE_OTHER_MOTIVE.getCode())){
						showMotiveText = true;
						motiveText = systemOptionTemp.getRemoveOtherMotive();
					}else{
						showMotiveText = false;
					}
				}else{
					motiveSelected = -1;
					showMotiveText = false;
				}
				this.setOperationType(ViewOperationsType.DELETE.getCode());
				showMessageOnDialog(PropertiesConstants.LBL_MOTIVE_DELETE, null,null, null);
				JSFUtilities.executeJavascriptFunction("PF('dlgMotive').show()");
				showMessageOnDialog2(PropertiesConstants.LBL_MOTIVE_DELETE, null, null, null);				
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Consult option.
	 */
	public void consultOption() {
		try {
			optionsOnFalse();
			activateConsult = true;
			this.setOperationType(ViewOperationsType.CONSULT.getCode());
			listOptionType = OptionType.list;
			if (nodeSelected == null) {
				itemNotSelected = true;
				JSFUtilities.executeJavascriptFunction("PF('confitemSelectedmsgvar').show()");
				return;
			}

			objCatalogSpanishLanguage = new CatalogueLanguage();
			objCatalogEnglishLanguage = new CatalogueLanguage();
			objCatalogoFrancesLanguage = new CatalogueLanguage();

			listPrivileges = new ArrayList<Parameter>();

			activateModify = false;
			activateAdd = false;
			flagDisable = true;
			flagViewTabSpanishLanguage = false;
			flagViewTabEnglishLanguage = false;
			flagViewTabFrancesLanguage = false;
			flagViewOptionMode = false;
			viewURL = false;

			labelAction = PropertiesUtilities.getMessage(FacesContext
					.getCurrentInstance().getViewRoot().getLocale(),
					PropertiesConstants.ADM_OPT_TABVIEW_TAB_HEADER_CONSULT);

			Integer codLanguage;
			Integer optionType;
			Integer privilege;

			OptionCatalogNodeBean objNodoOpcionCatalogoBean = (OptionCatalogNodeBean) nodeSelected
					.getData();
			orderOption = objNodoOpcionCatalogoBean.getOrderOption();
			List<Object[]> lista;
			// Set<String> stPrevilages = new TreeSet<String>();
			List<Integer> stPrivileges = new ArrayList<Integer>();

			if (objNodoOpcionCatalogoBean.getOptionType().equals(
					OptionType.OPTION.getCode())) {

				lista = systemServiceFacade
						.listPrivilegeCatalogueByOptionServiceBean(objNodoOpcionCatalogoBean
								.getIdSystemOptionPk());
			} else {
				lista = systemServiceFacade
						.listCatalogueByOptionServiceBean(objNodoOpcionCatalogoBean
								.getIdSystemOptionPk());
			}

			for (Object[] arrFila : lista) {

				if (arrFila[4] != null) {
					optionType = new Integer(arrFila[4].toString());
					if (optionType.equals(OptionType.OPTION.getCode())) {
						flagViewOptionMode = true;
						optType = OptionType.OPTION.getCode();
						viewURL = true;
						urlOption = arrFila[2].toString();
						documentFileName = arrFila[15]!= null?arrFila[15].toString():"";
						documentFilePath = arrFila[16]!= null?arrFila[16].toString():"";
						
						privilege = new Integer(arrFila[9].toString());
						stPrivileges.add(privilege);
						// stPrevilages.add(PrivilegeType.get(privilege).getDescription());
						bmotiveDescripcion = getBlockMotiveDescription((Integer) arrFila[11]);
						if (!objNodoOpcionCatalogoBean.getState().equals(
								OptionStateType.BLOCKED.getCode())) {
							ubmotiveDescripcion = getUnBlockMotiveDescription((Integer) arrFila[12]);
							if (arrFila[14] != null) {
								ubotherMotiveDescripcion = arrFila[14]
										.toString();
							}
						} else {
							ubmotiveDescripcion = null;
							;
							ubotherMotiveDescripcion = null;
						}
						if (arrFila[13] != null) {
							botherMotiveDescripcion = arrFila[13].toString();
						} else {
							botherMotiveDescripcion = null;
						}
					} else if (optionType.equals(OptionType.MODULE.getCode())) {
						bmotiveDescripcion = getBlockMotiveDescription((Integer) arrFila[9]);
						if (!objNodoOpcionCatalogoBean.getState().equals(
								OptionStateType.BLOCKED.getCode())) {
							ubmotiveDescripcion = getUnBlockMotiveDescription((Integer) arrFila[10]);
							if (arrFila[12] != null) {
								ubotherMotiveDescripcion = arrFila[12]
										.toString();
							}
						} else {
							ubmotiveDescripcion = null;
							ubotherMotiveDescripcion = null;
						}
						if (arrFila[11] != null) {
							botherMotiveDescripcion = arrFila[11].toString();
						} else {
							botherMotiveDescripcion = null;
						}
						flagViewOptionMode = false;
						optType = OptionType.MODULE.getCode();
						viewURL = false;
					}

					JSFUtilities.executeJavascriptFunction("PF('dlgTab').show()");
				}

				if (arrFila[5] != null) {
					codLanguage = new Integer(arrFila[5].toString());
					if (codLanguage.equals(LanguageType.SPANISH.getCode())) {
						flagViewTabSpanishLanguage = true;
						objCatalogSpanishLanguage.setCatalogueName(arrFila[6]
								.toString());
						if (arrFila[7] != null) {
							objCatalogSpanishLanguage.setDescription(arrFila[7]
									.toString());
						}
					}
					if (codLanguage.equals(LanguageType.ENGLISH.getCode())) {
						flagViewTabEnglishLanguage = true;
						objCatalogEnglishLanguage.setCatalogueName(arrFila[6]
								.toString());
						if (arrFila[7] != null) {
							objCatalogEnglishLanguage.setDescription(arrFila[7]
									.toString());
						}
					}
					if (codLanguage.equals(LanguageType.FRANCES.getCode())) {
						flagViewTabFrancesLanguage = true;
						objCatalogoFrancesLanguage.setCatalogueName(arrFila[6]
								.toString());
						if (arrFila[7] != null) {
							objCatalogoFrancesLanguage
									.setDescription(arrFila[7].toString());
						}
					}
				}
			}

			List<Parameter> privilegesList = systemServiceFacade
					.searchPrivilegesByDefinitionServiceBean(DefinitionType.PRIVILEGES_OPTION
							.getCode());
			for (Parameter parameter : privilegesList) {
				if (stPrivileges.contains(parameter.getIdParameterPk())) {
					Parameter p = new Parameter();
					p.setName(parameter.getName());
					listPrivileges.add(p);
				}
			}
			parameterDataModel = new ParameterDataModel(listPrivileges);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Gets the motive description.
	 *
	 * @param code the code
	 * @return the motive description
	 */
	public String getMotiveDescription(Integer code) {
		String str = "";
		if (lstMotives == null) {
			showMotives();
		}
		for (Parameter p : lstMotives) {
			if (p.getIdParameterPk().equals(code)) {
				str = p.getName();
				break;
			}
		}
		return str;
	}

	/**
	 * Gets the block motive description.
	 *
	 * @param code the code
	 * @return the block motive description
	 */
	public String getBlockMotiveDescription(Integer code) {
		String str = "";
		if (code != null) {
			for (Parameter p : lstBlockMotives) {
				if (p.getIdParameterPk().equals(code)) {
					str = p.getName();
					break;
				}
			}
		}
		return str;
	}

	/**
	 * Gets the un block motive description.
	 *
	 * @param code the code
	 * @return the un block motive description
	 */
	public String getUnBlockMotiveDescription(Integer code) {
		String str = "";
		if (code != null) {
			for (Parameter p : lstUnBlockMotives) {
				if (p.getIdParameterPk().equals(code)) {
					str = p.getName();
					break;
				}
			}
		}
		return str;
	}

	/**
	 * Refresh view url.
	 */
	public void refreshViewURL() {
		if (optType.equals(OptionType.MODULE.getCode())) {
			viewURL = false;
		} else {
			viewURL = true;
		}

	}

	/**
	 * Adds the node.
	 */
	public void addNode() {

		hideDialogues();
		showEndTransaction = false;

		try {

			systemOption = new SystemOption();

			List<OptionPrivilege> listaPrivilegios = new ArrayList<OptionPrivilege>();
			List<CatalogueLanguage> listaIdiomas = new ArrayList<CatalogueLanguage>();

			systemOption.setSystem(objSystemSelected);
			systemOption.setOptionType(optType);
			systemOption.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			systemOption.setRegistryDate(CommonsUtilities.currentDateTime());
			systemOption.setModule(Integer.valueOf(0));
			systemOption.setDocumentFile(documentFile);
			systemOption.setDocumentFileName(documentFileName);
			systemOption.setDocumentFilePath(destinationPathOption);
			if (optType.equals(OptionType.OPTION.getCode())) {
				systemOption.setUrlOption(urlOption);
				systemOption.setState(OptionStateType.REGISTERED.getCode());

				OptionCatalogNodeBean objNodoOpcionCatalogoBean = (OptionCatalogNodeBean) nodeSelected.getData();

				SystemOption auxSystemOption = new SystemOption();
				auxSystemOption.setIdSystemOptionPk(objNodoOpcionCatalogoBean.getIdSystemOptionPk());

				if (objNodoOpcionCatalogoBean.getInstanceType().equals(InstanceType.OPTION.getCode())) {

					systemOption.setParentSystemOption(auxSystemOption);

				}

				for (int i = 0; i < privilegesSelected.length; i++) {
					privilegeOption = new OptionPrivilege();

					privilegeOption.setRegistryDate(CommonsUtilities.currentDateTime());
					privilegeOption.setRegistryUser(userInfo.getUserAccountSession().getUserName());
					/*
					 * Ambiguous using same attribute for 2 diferents values in
					 * mpParam getIdParameterPk() have primary key of
					 * OptionPrivilege and in listPrivileges the
					 * getIdParameterPk() have code of privilege of parameters
					 */
					privilegeOption.setIdPrivilege(privilegesSelected[i].getIdParameterPk());
					privilegeOption.setSystemOption(systemOption);
					privilegeOption.setState(OptionPrivilegeStateType.REGISTERED.getCode());
					listaPrivilegios.add(privilegeOption);
				}
				systemOption.setOptionPrivileges(listaPrivilegios);
			} else {
				systemOption.setState(OptionStateType.REGISTERED.getCode());
				systemOption.setUrlOption("-");

				OptionCatalogNodeBean objNodoOpcionCatalogoBean = (OptionCatalogNodeBean) nodeSelected.getData();

				SystemOption objaux = new SystemOption();
				objaux.setIdSystemOptionPk(objNodoOpcionCatalogoBean.getIdSystemOptionPk());
				if (objNodoOpcionCatalogoBean.getInstanceType().equals(InstanceType.OPTION.getCode())) {
					systemOption.setParentSystemOption(objaux);
				}
			}

			systemOption.setOrderOption(orderOption);
			String optionName = " ";
			/* fixed null point when description is null */
			if (StringUtils.isNotBlank(objCatalogSpanishLanguage.getCatalogueName())) {
				objCatalogSpanishLanguage.setRegistryDate(CommonsUtilities.currentDateTime());
				objCatalogSpanishLanguage.setLanguage(LanguageType.SPANISH.getCode());
				objCatalogSpanishLanguage.setRegistryUser(userInfo.getUserAccountSession().getUserName());
				if (StringUtils.isNotBlank(objCatalogSpanishLanguage.getDescription())) {
					objCatalogSpanishLanguage.setDescription(objCatalogSpanishLanguage.getDescription());
				}
				objCatalogSpanishLanguage.setCatalogueName(objCatalogSpanishLanguage.getCatalogueName());
				objCatalogSpanishLanguage.setSystemOption(systemOption);
				listaIdiomas.add(objCatalogSpanishLanguage);

				optionName = objCatalogSpanishLanguage.getCatalogueName();
			}

			if (StringUtils.isNotBlank(objCatalogEnglishLanguage.getCatalogueName())) {
				objCatalogEnglishLanguage.setRegistryDate(CommonsUtilities.currentDateTime());
				objCatalogEnglishLanguage.setLanguage(LanguageType.ENGLISH.getCode());
				objCatalogEnglishLanguage.setRegistryUser(userInfo.getUserAccountSession().getUserName());
				if (StringUtils.isNotBlank(objCatalogEnglishLanguage.getDescription())) {
					objCatalogEnglishLanguage.setDescription(objCatalogEnglishLanguage.getDescription());
				}
				objCatalogEnglishLanguage.setCatalogueName(objCatalogEnglishLanguage.getCatalogueName());
				objCatalogEnglishLanguage.setSystemOption(systemOption);
				listaIdiomas.add(objCatalogEnglishLanguage);

				optionName = objCatalogEnglishLanguage.getCatalogueName();
			}

			if (StringUtils.isNotBlank(objCatalogoFrancesLanguage.getCatalogueName())) {
				objCatalogoFrancesLanguage.setRegistryDate(CommonsUtilities.currentDateTime());
				objCatalogoFrancesLanguage.setLanguage(LanguageType.FRANCES.getCode());
				objCatalogoFrancesLanguage.setRegistryUser(userInfo.getUserAccountSession().getUserName());
				if (StringUtils.isNotBlank(objCatalogoFrancesLanguage.getDescription())) {
					objCatalogoFrancesLanguage.setDescription(objCatalogoFrancesLanguage.getDescription());
				}
				objCatalogoFrancesLanguage.setCatalogueName(objCatalogoFrancesLanguage.getCatalogueName());
				objCatalogoFrancesLanguage.setSystemOption(systemOption);
				listaIdiomas.add(objCatalogoFrancesLanguage);

				optionName = objCatalogoFrancesLanguage.getCatalogueName();
			}

			systemOption.setCatalogueLanguages(listaIdiomas);

			boolean resultado;
			resultado = systemServiceFacade.registerSystemOptionServiceFacade(systemOption, objCatalogSpanishLanguage.getCatalogueName().toUpperCase());
			if (resultado) {

				Object[] bodyData = new Object[1];
				String idioma = FacesContext.getCurrentInstance().getViewRoot()
						.getLocale().toString();

				if (idioma.equals(LanguageType.SPANISH.getValue())
						&& objCatalogSpanishLanguage.getCatalogueName() != null) {
					bodyData[0] = objCatalogSpanishLanguage.getCatalogueName()
							.toUpperCase();
				}
				if (idioma.equals(LanguageType.ENGLISH.getValue())
						&& objCatalogEnglishLanguage.getCatalogueName() != null) {
					bodyData[0] = objCatalogEnglishLanguage.getCatalogueName()
							.toUpperCase();
				}
				if (idioma.equals(LanguageType.FRANCES.getValue())
						&& objCatalogoFrancesLanguage.getCatalogueName() != null) {
					bodyData[0] = objCatalogoFrancesLanguage.getCatalogueName()
							.toUpperCase();
				}
				/*
				 * if user login in Spanish/English/French ,and he/she may
				 * create the system option/module the data other than the user
				 * login language in that scenario get the option/module name.
				 */
				if (bodyData[0] == null
						|| (bodyData[0] != null && bodyData[0].toString()
								.trim().length() == 0)) {
					if (objCatalogSpanishLanguage.getCatalogueName() != null) {
						bodyData[0] = objCatalogSpanishLanguage
								.getCatalogueName().toUpperCase();
					} else if (objCatalogEnglishLanguage.getCatalogueName() != null) {
						bodyData[0] = objCatalogEnglishLanguage
								.getCatalogueName().toUpperCase();
					} else {
						bodyData[0] = objCatalogoFrancesLanguage
								.getCatalogueName().toUpperCase();
					}
				}

				if (optType.equals(OptionType.OPTION.getCode())) {
					showEndTransaction = true;
					// bodyData[0]= systemOption.getUrlOption();
					showMessageOnDialog(
							PropertiesConstants.DIALOG_HEADER_SUCCESS, null,
							PropertiesConstants.SUCCESS_ADM_OPTION_ADD_LABEL,
							bodyData);
				} else {

					showEndTransaction = true;
					showMessageOnDialog(
							PropertiesConstants.DIALOG_HEADER_SUCCESS, null,
							PropertiesConstants.SUCCESS_ADM_MODULE_ADD_LABEL,
							bodyData);
				}

				JSFUtilities.executeJavascriptFunction("PF('endTrasaction').show()");
				loadingMenuBySystem();
				JSFUtilities.executeJavascriptFunction("PF('dlgTab').hide()");
			}
		} catch (Exception ex) {

			excepcion.fire(new ExceptionToCatchEvent(ex));
		} finally {
			objCatalogSpanishLanguage = new CatalogueLanguage();
			objCatalogEnglishLanguage = new CatalogueLanguage();
			objCatalogoFrancesLanguage = new CatalogueLanguage();
			urlOption = "";
			privilegesSelected = null;
			systemOption = new SystemOption();
		}

	}

	/**
	 * Modify node.
	 */

	public void modifyNode() {

		showEndTransaction = false;
		List<OptionPrivilege> listPrivileges = new ArrayList<OptionPrivilege>();
		// List<OptionPrivilege> listPrivilegesDelete = new
		// ArrayList<OptionPrivilege>();
		List<CatalogueLanguage> languageList = new ArrayList<CatalogueLanguage>();
		List<Integer> previlagesKeys = new ArrayList<Integer>();
		List<OptionPrivilege> optionPrivileges = new ArrayList<OptionPrivilege>();

		String optionName;
		String optionDescription;

		OptionPrivilege aux = new OptionPrivilege();

		

		try {
			systemOption = new SystemOption();
			OptionCatalogNodeBean objOptionCatalogNodeBean = (OptionCatalogNodeBean) nodeSelected.getData();
			Object[] bodyData = new Object[1];
			if (objOptionCatalogNodeBean != null) {
				Date date = systemServiceFacade.getSystemOptionsLastModfiedDetails(objOptionCatalogNodeBean.getIdSystemOptionPk());
				if (date != null) {
					if ((objOptionCatalogNodeBean.getLastModifyDate().getTime() / 1000 ) < (date.getTime() / 1000)) {
						bodyData[0] = objOptionCatalogNodeBean.getName()
								.toUpperCase();
						if (optType.equals(OptionType.OPTION.getCode())) {
							showMessageOnDialog(
									PropertiesConstants.DIALOG_HEADER_ALERT,
									null,
									PropertiesConstants.ERROR_SYSTEM_OPTIONS_ALREADY_MODIFY,
									bodyData);
						} else {
							showMessageOnDialog(
									PropertiesConstants.DIALOG_HEADER_ALERT,
									null,
									PropertiesConstants.ERROR_SYSTEM_MODULE_ALREADY_MODIFY,
									bodyData);
						}
						JSFUtilities.executeJavascriptFunction("PF('alreadyconfirmation').show()");
						loadingMenuBySystem();
						return;
					}
				}
				systemOption.setIdSystemOptionPk(objOptionCatalogNodeBean.getIdSystemOptionPk());
			}
			

			systemOption = systemServiceFacade
					.searchSystemOptionByIdServiceBean(systemOption
							.getIdSystemOptionPk());
			systemOption.setDocumentFile(documentFile);
			systemOption.setDocumentFileName(documentFileName);
			systemOption.setDocumentFilePath(destinationPathOption);

			systemOption.setOrderOption(orderOption);

			systemOption.setSystem(objSystemSelected);

			if (optType.equals(OptionType.OPTION.getCode())) {
				// Only update privileges changed not all
				systemOption.setUrlOption(urlOption);
				Map<Integer, OptionPrivilege> privilegesMapOld = new HashMap<Integer, OptionPrivilege>();
				Map<Integer, OptionPrivilege> privilegesMapNew = new HashMap<Integer, OptionPrivilege>();
				Set<Integer> privilegesOld = new HashSet<Integer>();
				Set<Integer> privilegesNew = new HashSet<Integer>();
				for (int i = 0; i < beforePrivileges.size(); i++) {
					previlagesKeys.add(beforePrivileges.get(i)
							.getIdOptionPrivilegePk());
				}
				optionPrivileges = systemServiceFacade
						.searchOptionPrivilegeByIdPk(previlagesKeys);
				for (int i = 0; i < optionPrivileges.size(); i++) {
					privilegeOption = optionPrivileges.get(i);
					// privilegeOption=systemServiceFacade.searchOptionPrivilegeByIdPk(beforePrivileges.get(i).getIdOptionPrivilegePk());
					// privilegeOption.setState(OptionPrivilegeStateType.DELETED.getCode());
					privilegeOption.setSystemOption(systemOption);
					// save temp for comparations change
					privilegesOld.add(privilegeOption.getIdPrivilege());
					privilegesMapOld.put(privilegeOption.getIdPrivilege(),
							privilegeOption);
					// listPrivilegesDelete.add(privilegeOption);
				}

				for (int i = 0; i < privilegesSelected.length; i++) {
					privilegeOption = new OptionPrivilege();
					privilegeOption.setRegistryUser(userInfo
							.getUserAccountSession().getUserName());
					privilegeOption.setRegistryDate(CommonsUtilities
							.currentDateTime());
					SystemOption auxSystemOption = new SystemOption();
					auxSystemOption
							.setIdSystemOptionPk(objOptionCatalogNodeBean
									.getIdSystemOptionPk());
					privilegeOption.setSystemOption(auxSystemOption);
					privilegeOption
							.setState(OptionPrivilegeStateType.REGISTERED
									.getCode());
					privilegeOption.setIdPrivilege(privilegesSelected[i]
							.getIdParameterPk());
					privilegeOption.setSystemOption(systemOption);
					privilegeOption.setIdOptionPrivilegePk(null);
					// save temp for comparations change
					privilegesNew.add(privilegeOption.getIdPrivilege());
					privilegesMapNew.put(privilegeOption.getIdPrivilege(),
							privilegeOption);
					// listPrivileges.add(privilegeOption);
				}
				// get same privileges
				Set<Integer> intersection = new HashSet<Integer>(privilegesOld);
				intersection.retainAll(privilegesNew);
				// get added Privileges
				Set<Integer> newPrivileges = new HashSet<Integer>(privilegesNew);
				newPrivileges.removeAll(privilegesOld);
				// get delete privileges
				Set<Integer> deletePrivileges = new HashSet<Integer>(
						privilegesOld);
				deletePrivileges.removeAll(privilegesNew);
				// make privileges final
				// same privileges
				for (Integer privilege : intersection) {
					listPrivileges.add(privilegesMapOld.get(privilege));
				}
				// new privileges
				for (Integer privilege : newPrivileges) {
					listPrivileges.add(privilegesMapNew.get(privilege));
				}
				// delete privileges
				for (Integer privilege : deletePrivileges) {
					OptionPrivilege option = privilegesMapOld.get(privilege);
					option.setState(OptionPrivilegeStateType.DELETED.getCode());
					listPrivileges.add(option);
				}
				systemOption.setOptionPrivileges(listPrivileges);
			}

			if (StringUtils.isNotBlank(objCatalogSpanishLanguage
					.getCatalogueName())) {
				optionName = objCatalogSpanishLanguage.getCatalogueName();
				if (StringUtils.isNotBlank(objCatalogSpanishLanguage
						.getDescription())) {
					optionDescription = objCatalogSpanishLanguage
							.getDescription();
				} else {
					optionDescription = null;
				}

				objCatalogSpanishLanguage = systemServiceFacade
						.searchCatalogueLanguageByIdServiceFacade(objCatalogSpanishLanguage
								.getIdCatalogueLanguagePk());
				if (Validations.validateIsNull(objCatalogSpanishLanguage
						.getRegistryDate())) {
					objCatalogSpanishLanguage.setRegistryDate(CommonsUtilities
							.currentDateTime());
				}
				if (Validations.validateIsNullOrEmpty(objCatalogSpanishLanguage
						.getRegistryUser())) {
					objCatalogSpanishLanguage.setRegistryUser(userInfo
							.getUserAccountSession().getUserName());
				}
				objCatalogSpanishLanguage.setLanguage(LanguageType.SPANISH
						.getCode());
				objCatalogSpanishLanguage.setDescription(optionDescription);
				objCatalogSpanishLanguage.setCatalogueName(optionName);
				objCatalogSpanishLanguage.setSystemOption(systemOption);
				languageList.add(objCatalogSpanishLanguage);

			}

			optionDescription = null;
			if (StringUtils.isNotBlank(objCatalogEnglishLanguage
					.getCatalogueName())) {

				optionName = objCatalogEnglishLanguage.getCatalogueName();
				if (StringUtils.isNotBlank(objCatalogEnglishLanguage
						.getDescription())) {
					optionDescription = objCatalogEnglishLanguage
							.getDescription();
				}

				objCatalogEnglishLanguage = systemServiceFacade
						.searchCatalogueLanguageByIdServiceFacade(objCatalogEnglishLanguage
								.getIdCatalogueLanguagePk());
				if (Validations.validateIsNull(objCatalogEnglishLanguage
						.getRegistryDate())) {
					objCatalogEnglishLanguage.setRegistryDate(CommonsUtilities
							.currentDateTime());
				}
				if (Validations.validateIsNullOrEmpty(objCatalogEnglishLanguage
						.getRegistryUser())) {
					objCatalogEnglishLanguage.setRegistryUser(userInfo
							.getUserAccountSession().getUserName());
				}
				objCatalogEnglishLanguage.setLanguage(LanguageType.ENGLISH
						.getCode());
				objCatalogEnglishLanguage.setDescription(optionDescription);
				objCatalogEnglishLanguage.setCatalogueName(optionName);
				objCatalogEnglishLanguage.setSystemOption(systemOption);

				languageList.add(objCatalogEnglishLanguage);
			}

			optionDescription = null;
			if (StringUtils.isNotBlank(objCatalogoFrancesLanguage
					.getCatalogueName())) {
				optionName = objCatalogoFrancesLanguage.getCatalogueName();
				if (StringUtils.isNotBlank(objCatalogoFrancesLanguage
						.getDescription())) {
					optionDescription = objCatalogoFrancesLanguage
							.getDescription();
				}

				objCatalogoFrancesLanguage = systemServiceFacade
						.searchCatalogueLanguageByIdServiceFacade(objCatalogoFrancesLanguage
								.getIdCatalogueLanguagePk());
				if (Validations.validateIsNull(objCatalogoFrancesLanguage
						.getRegistryDate())) {
					objCatalogoFrancesLanguage.setRegistryDate(CommonsUtilities
							.currentDateTime());
				}
				if (Validations
						.validateIsNullOrEmpty(objCatalogoFrancesLanguage
								.getRegistryUser())) {
					objCatalogoFrancesLanguage.setRegistryUser(userInfo
							.getUserAccountSession().getUserName());
				}
				objCatalogoFrancesLanguage.setLanguage(LanguageType.FRANCES
						.getCode());
				objCatalogoFrancesLanguage.setDescription(optionDescription);
				objCatalogoFrancesLanguage.setCatalogueName(optionName);
				objCatalogoFrancesLanguage.setSystemOption(systemOption);

				languageList.add(objCatalogoFrancesLanguage);
			}

			systemOption.setCatalogueLanguages(languageList);

			boolean resultado;
			SystemOption oS = new SystemOption();
			String message = "", subject = "";
			String catalogueName = "";
			oS.setIdSystemOptionPk(systemOption.getIdSystemOptionPk());
			aux.setSystemOption(oS);
			/*
			 * only change state when change privilege not always and inserted
			 * news
			 */
			// if(optType.equals(OptionType.OPTION.getCode())){
			// systemServiceFacade.deletePrivilegeServiceFacade(listPrivilegesDelete);
			// }
			resultado = systemServiceFacade.modifySystemOptionServiceFacade(systemOption,objCatalogSpanishLanguage.getCatalogueName());

			if (resultado) {
				String idioma = FacesContext.getCurrentInstance().getViewRoot()
						.getLocale().toString();

				for (int i = 0; i < languageList.size(); i++) {
					if (LanguageType.get(languageList.get(i).getLanguage())
							.getValue().equals(idioma)) {
						bodyData[0] = languageList.get(i).getCatalogueName();
						catalogueName = languageList.get(i).getCatalogueName();
					}
				}

				if (optType.equals(OptionType.OPTION.getCode())) {
					showEndTransaction = true;

					showMessageOnDialog(
							PropertiesConstants.DIALOG_HEADER_SUCCESS,
							null,
							PropertiesConstants.SUCCESS_ADM_OPTION_MODIFY_LABEL,
							bodyData);
				} else {
					showEndTransaction = true;
					showMessageOnDialog(
							PropertiesConstants.DIALOG_HEADER_SUCCESS,
							null,
							PropertiesConstants.SUCCESS_ADM_MODULE_MODIFY_LABEL,
							bodyData);
				}

				JSFUtilities.executeJavascriptFunction("PF('endTrasaction').show()");
				loadingMenuBySystem();
				JSFUtilities.executeJavascriptFunction("PF('dlgTab').hide()");
			}

		} catch (Exception ex) {
			log.error(ex.getMessage());
			excepcion.fire(new ExceptionToCatchEvent(ex));
		} finally {
			objCatalogSpanishLanguage = new CatalogueLanguage();
			objCatalogEnglishLanguage = new CatalogueLanguage();
			objCatalogoFrancesLanguage = new CatalogueLanguage();
			urlOption = "";
			privilegesSelected = null;
			systemOption = new SystemOption();
		}

	}

	/**
	 * Validate motive.
	 * 
	 * @return true, if successful
	 */
	public boolean validateMotive() {
		boolean flag = true;

		String messageError = PropertiesConstants.ERROR_MESSAGE_MOTIVE_REQUIRED;

		if (motiveText.trim().equals("")) {
			flag = false;

			JSFUtilities.addContextMessage(null, FacesMessage.SEVERITY_ERROR,
					PropertiesUtilities.getMessage(messageError),
					PropertiesUtilities.getMessage(messageError));

		}

		return flag;
	}

	/**
	 * Action do.
	 */
	@LoggerAuditWeb
	public void actionDo() {

		flagNext = true;
		activateContextMenu = false;
		hideDialogues();
		
		if (activateAdd) {
			addNode();
		} else if (activateModify) {
			modifyNode();
		} else if (activateReject) {
			rejectNode();
		} else if (activateBlock) {
			blockNode(nodeSelected);
		} else if (activateUnblock) {
			unBlockNode(nodeSelected);
		} else if (activateDelete) {
			deleteNode(nodeSelected);
		} else if (activateConfirm) {
			confirmNode();
		}
		
		motiveSelected = -1;
	}

	/**
	 * Confirm node.
	 */
	public void confirmNode() {
		try {
			optionsOnFalse();
			hideDialogues();
			activateConfirm = true;
			
			OptionCatalogNodeBean objNodoOpcionCatalogoBean = (OptionCatalogNodeBean) nodeSelected.getData();
			intialModifedDate = objNodoOpcionCatalogoBean.getLastModifyDate();
			
			Date date = systemServiceFacade.getSystemOptionsLastModfiedDetails(objNodoOpcionCatalogoBean.getIdSystemOptionPk());
			if (date != null && intialModifedDate.compareTo(date) < 0) {
				Object[] bodyData1 = { objNodoOpcionCatalogoBean.getName().toUpperCase()};

				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT,
						null,
						PropertiesConstants.SYSTEM_OPTION_ERROR_MODIFIED_RECENT,
						bodyData1);
				JSFUtilities.executeJavascriptFunction("PF('alreadyconfirmation').show()");
				return;
			}
							
			SystemOption optionSystem = systemServiceFacade.searchSystemOptionByIdServiceBean(objNodoOpcionCatalogoBean.getIdSystemOptionPk());
			optionSystem.setState(OptionStateType.CONFIRMED.getCode());
			optionSystem.setSituation(OptionSituationType.ACTIVE.getCode());

			boolean result = systemServiceFacade.confirmSystemOption(optionSystem, objNodoOpcionCatalogoBean.getName());

			if (result) {
				Object[] bodyData = {objNodoOpcionCatalogoBean.getName()};
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_SUCCESS,
						null,
						objNodoOpcionCatalogoBean.getOptionType().equals(OptionType.OPTION.getCode())
						? PropertiesConstants.SUCCESS_ADM_OPTION_CONFIRM_LABEL
						: PropertiesConstants.SUCCESS_ADM_MODULE_CONFIRM_LABEL,
						bodyData);
				JSFUtilities.executeJavascriptFunction("PF('confirmation').hide()");
				JSFUtilities.executeJavascriptFunction("PF('endTrasaction').show()");
				loadingMenuBySystem();
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Reject node.
	 */
	public void rejectNode() {
		optionsOnFalse();
		activateReject = true;
		try {
			OptionCatalogNodeBean objNodoOpcionCatalogoBean = (OptionCatalogNodeBean) nodeSelected.getData();
			SystemOption systemOption = systemServiceFacade.searchSystemOptionByIdServiceBean(objNodoOpcionCatalogoBean.getIdSystemOptionPk());
			
			if ((objNodoOpcionCatalogoBean.getLastModifyDate().getTime() / 1000) < (systemOption.getLastModifyDate().getTime() / 1000)) {
				Object[] argObj = { objNodoOpcionCatalogoBean.getName() };
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT,
						null,
						PropertiesConstants.SYSTEM_OPTION_ERROR_MODIFIED_RECENT,
						argObj);
				JSFUtilities.executeJavascriptFunction("PF('alreadyconfirmation').show()");
				loadingMenuBySystem();
				return;
			}
			
			systemOption.setSituation(OptionSituationType.ACTIVE.getCode());
			systemOption.setRejectMotive(this.getMotiveSelected());
			systemOption.setRejectOtherMotive(motiveText);

			showEndTransaction = true;
			
			String messageModal = systemServiceFacade.rejectSystemOption(systemOption, null, objNodoOpcionCatalogoBean);
			
			if (messageModal!=null) {
				Object[] bodyData = {objNodoOpcionCatalogoBean.getName().toUpperCase()};
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_SUCCESS,
						null,
						messageModal,
						bodyData);
				JSFUtilities.executeJavascriptFunction("PF('endTrasaction').show()");
				loadingMenuBySystem();
			}

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}

	/**
	 * Block node.
	 *
	 * @param nodo the nodo
	 */
	public void blockNode(TreeNode nodo) {

		OptionCatalogNodeBean objNodoOpcionCatalogoBean = (OptionCatalogNodeBean) nodo.getData();
		boolean resultado = false;

		try {
			SystemOption auxSystemOption = systemServiceFacade.searchSystemOptionByIdServiceBean(objNodoOpcionCatalogoBean.getIdSystemOptionPk());
			
			if ((objNodoOpcionCatalogoBean.getLastModifyDate().getTime() / 1000) < ( auxSystemOption.getLastModifyDate().getTime() / 1000)) {
				Object[] argObj = { objNodoOpcionCatalogoBean.getName() };
				
				if ((objNodoOpcionCatalogoBean.getState().equals(OptionStateType.CONFIRMED.getCode()) &&
					objNodoOpcionCatalogoBean.getSituation().equals(OptionSituationType.PENDING_LOCK.getCode()) && 
					OptionStateType.BLOCKED.getCode().equals(auxSystemOption.getState())) ||
					(objNodoOpcionCatalogoBean.getState().equals(OptionStateType.CONFIRMED.getCode()) && 
					objNodoOpcionCatalogoBean.getSituation().equals(OptionSituationType.ACTIVE.getCode())	&&	
					auxSystemOption.getState().equals(OptionStateType.CONFIRMED.getCode()) &&
					auxSystemOption.getSituation().equals(OptionSituationType.PENDING_LOCK.getCode()))) {
					showMessageOnDialog(
							PropertiesConstants.DIALOG_HEADER_ALERT,
							null,
							PropertiesConstants.SYSTEM_OPTION_ERROR_ALREDY_BLOCKED,
							argObj);
				} else {
					showMessageOnDialog(
							PropertiesConstants.DIALOG_HEADER_ALERT,
							null,
							PropertiesConstants.SYSTEM_OPTION_ERROR_MODIFIED_RECENT,
							argObj);
				}
				JSFUtilities.executeJavascriptFunction("PF('alreadyconfirmation').show()");
				loadingMenuBySystem();
				return;
			}
			
			HistoricalState historical = new HistoricalState();
			String messageSuccess = "";
			if(objNodoOpcionCatalogoBean.getState().equals(OptionStateType.CONFIRMED.getCode()) &&
				objNodoOpcionCatalogoBean.getSituation().equals(OptionSituationType.ACTIVE.getCode())){
				
				auxSystemOption.setState(OptionStateType.CONFIRMED.getCode());
				auxSystemOption.setSituation(OptionSituationType.PENDING_LOCK.getCode());
				auxSystemOption.setBlockMotive(this.getMotiveSelected());
				auxSystemOption.setBlockOtherMotive(motiveText);
				
				historical.setMotive(this.getMotiveSelected());
				historical.setMotiveDescription(motiveText);
				historical.setCurrentState(auxSystemOption.getSituation());
				messageSuccess = auxEventNode.getOptionType().equals(OptionType.OPTION.getCode()) 
									? PropertiesConstants.SUCCESS_ADM_OPTION_REQUEST_BLOCK_LABEL
									: PropertiesConstants.SUCCESS_ADM_MODULE_REQUEST_BLOCK_LABEL;
				
			}else if(objNodoOpcionCatalogoBean.getState().equals(OptionStateType.CONFIRMED.getCode()) &&
				objNodoOpcionCatalogoBean.getSituation().equals(OptionSituationType.PENDING_LOCK.getCode())){
				
				auxSystemOption.setState(OptionStateType.BLOCKED.getCode());
				auxSystemOption.setSituation(OptionSituationType.ACTIVE.getCode());
				historical.setMotive(this.getMotiveSelected());
				historical.setMotiveDescription(motiveText);
				historical.setCurrentState(auxSystemOption.getState());
				messageSuccess = auxEventNode.getOptionType().equals(OptionType.OPTION.getCode()) 
									? PropertiesConstants.SUCCESS_ADM_OPTION_BLOCK_LABEL
									: PropertiesConstants.SUCCESS_ADM_MODULE_BLOCK_LABEL; 
			}

			listSystemOption.add(auxSystemOption);

			if (nodo.getChildCount() > 0) {
				for (int i = 0; i < nodo.getChildren().size(); i++) {
					blockNode(nodo.getChildren().get(i));
				}
			}

			auxSystemOption.setSystemOptions(listSystemOption);
			System auxSistema = new System();
			auxSistema.setIdSystemPk(systemSelected);
			auxSystemOption.setSystem(auxSistema);

			showEndTransaction = true;
			
			ThreadLocalContextHolder.put(
					PropertiesConstants.HISTORICAL_STATE_LOGGER,
					createHistorialState(
							auxSystemOption.getClass().getAnnotation(Table.class).name(),
							OptionStateType.CONFIRMED.getCode(), 
							Long.parseLong(auxSystemOption.getIdSystemOptionPk().toString()),
							historical));
			
			resultado = systemServiceFacade.updateSystemOptionState(auxSystemOption,auxEventNode.getName().toUpperCase());
			
			
			if (resultado) {

				Object[] bodyData = {auxEventNode.getName().toUpperCase()};
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_SUCCESS, null,
						messageSuccess,
						bodyData);
				hideDialogues();
				JSFUtilities.executeJavascriptFunction("PF('confirmation').hide()");
				JSFUtilities.executeJavascriptFunction("PF('endTrasaction').show()");
				loadingMenuBySystem();
			}

		} catch (Exception ex) {

			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}

	/**
	 * Un block node.
	 *
	 * @param nodo the nodo
	 */
	public void unBlockNode(TreeNode nodo) {

		OptionCatalogNodeBean objNodoOpcionCatalogoBean = (OptionCatalogNodeBean) nodo.getData();
		boolean result = false;
		try {

			SystemOption auxSystemOption = systemServiceFacade.searchSystemOptionByIdServiceBean(objNodoOpcionCatalogoBean.getIdSystemOptionPk());
			if ((objNodoOpcionCatalogoBean.getLastModifyDate().getTime() / 1000) < (auxSystemOption.getLastModifyDate().getTime() / 1000)) {
				Object[] argObj = { objNodoOpcionCatalogoBean.getName() };
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT,
						null,
						PropertiesConstants.SYSTEM_OPTION_ERROR_MODIFIED_RECENT,
						argObj);
				JSFUtilities.executeJavascriptFunction("PF('alreadyconfirmation').show()");
				loadingMenuBySystem();
				return;
			}
			
			HistoricalState historical = new HistoricalState();
			if(objNodoOpcionCatalogoBean.getState().equals(OptionStateType.BLOCKED.getCode()) &&
				objNodoOpcionCatalogoBean.getSituation().equals(OptionSituationType.ACTIVE.getCode())){
				auxSystemOption.setState(OptionStateType.BLOCKED.getCode());
				auxSystemOption.setSituation(OptionSituationType.PENDING_UNLOCK.getCode());
				auxSystemOption.setUnblockMotive(this.getMotiveSelected());
				auxSystemOption.setUnblockOtherMotive(motiveText);
				historical.setCurrentState(auxSystemOption.getSituation());
				
			}else if(objNodoOpcionCatalogoBean.getState().equals(OptionStateType.BLOCKED.getCode()) &&
					objNodoOpcionCatalogoBean.getSituation().equals(OptionSituationType.PENDING_UNLOCK.getCode())){
				auxSystemOption.setState(OptionStateType.CONFIRMED.getCode());
				auxSystemOption.setSituation(OptionSituationType.ACTIVE.getCode());
				historical.setCurrentState(auxSystemOption.getState());
			}
			
			System auxSystem = new System();
			auxSystem.setIdSystemPk(systemSelected);
			auxSystemOption.setSystem(auxSystem);
			listSystemOption.add(auxSystemOption);

			if (nodo.getChildCount() > 0) {
				for (int i = 0; i < nodo.getChildren().size(); i++) {
					unBlockNode(nodo.getChildren().get(i));
				}
			}

			auxSystemOption.setSystemOptions(listSystemOption);
			auxSystemOption.setSystem(auxSystemOption.getSystem());
			
			showEndTransaction = true;
			// Historical state
			
			historical.setMotive(this.getMotiveSelected());
			historical.setMotiveDescription(motiveText);
			
			ThreadLocalContextHolder.put(
					PropertiesConstants.HISTORICAL_STATE_LOGGER,
					createHistorialState(
							auxSystemOption.getClass().getAnnotation(Table.class).name(),OptionStateType.BLOCKED.getCode(), 
							Long.parseLong(auxSystemOption.getIdSystemOptionPk().toString()),
							historical));
			
			result = systemServiceFacade.updateSystemOptionState(auxSystemOption,auxEventNode.getName().toUpperCase());

			if (result) {

				Object[] bodyData = new Object[1];
				if (auxEventNode.getOptionType().equals(OptionType.OPTION.getCode())) {
					bodyData[0] = auxEventNode.getName().toUpperCase();
					showMessageOnDialog(
							PropertiesConstants.DIALOG_HEADER_SUCCESS,
							null,
							PropertiesConstants.SUCESS_ADM_OPTION_UNBLOCK_LABEL,
							bodyData);
				} else {
					bodyData[0] = auxEventNode.getName().toUpperCase();
					showMessageOnDialog(
							PropertiesConstants.DIALOG_HEADER_SUCCESS,
							null,
							PropertiesConstants.SUCCESS_ADM_MODULE_UNBLOCK_LABEL,
							bodyData);
				}
				hideDialogues();
				JSFUtilities.executeJavascriptFunction("PF('endTrasaction').show()");
				loadingMenuBySystem();
			}

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Delete node.
	 */
	public void deleteNode(TreeNode nodo) {
		optionsOnFalse();
		hideDialogues();
		activateDelete = true;
		OptionCatalogNodeBean objNodoOpcionCatalogoBean = (OptionCatalogNodeBean) nodo.getData();
		try {

			boolean resultado;
			SystemOption systemOption = systemServiceFacade.searchSystemOptionByIdServiceBean(objNodoOpcionCatalogoBean.getIdSystemOptionPk());
			if ((objNodoOpcionCatalogoBean.getLastModifyDate().getTime() / 1000) < (systemOption.getLastModifyDate().getTime() / 1000)) {
				Object[] argObj = { objNodoOpcionCatalogoBean.getName() };
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT,
						null,
						PropertiesConstants.SYSTEM_OPTION_ERROR_MODIFIED_RECENT,
						argObj);
				JSFUtilities.executeJavascriptFunction("PF('alreadyconfirmation').show()");
				loadingMenuBySystem();
				return;
			}
			HistoricalState historical = new HistoricalState();
			if(objNodoOpcionCatalogoBean.getState().equals(OptionStateType.CONFIRMED.getCode()) &&
				objNodoOpcionCatalogoBean.getSituation().equals(OptionSituationType.ACTIVE.getCode())){
				systemOption.setState(OptionStateType.CONFIRMED.getCode());
				systemOption.setSituation(OptionSituationType.PENDING_DELETE.getCode());
				systemOption.setRemoveMotive(this.getMotiveSelected());
				systemOption.setRemoveOtherMotive(motiveText);
				historical.setCurrentState(systemOption.getSituation());
				
			}else if(objNodoOpcionCatalogoBean.getState().equals(OptionStateType.CONFIRMED.getCode()) &&
					objNodoOpcionCatalogoBean.getSituation().equals(OptionSituationType.PENDING_DELETE.getCode())){
				systemOption.setState(OptionStateType.DELETED.getCode());
				systemOption.setSituation(OptionSituationType.ACTIVE.getCode());
				historical.setCurrentState(systemOption.getState());
			}
			System auxSystem = new System();
			auxSystem.setIdSystemPk(systemSelected);
			systemOption.setSystem(auxSystem);
			listSystemOption.add(systemOption);
			if (nodo.getChildCount() > 0) {
				for (int i = 0; i < nodo.getChildren().size(); i++) {
					deleteNode(nodo.getChildren().get(i));
				}
			}
			systemOption.setSystemOptions(listSystemOption);
			systemOption.setSystem(systemOption.getSystem());
			showEndTransaction = true;
			// Historical state
			historical.setMotive(this.getMotiveSelected());
			historical.setMotiveDescription(motiveText);
			ThreadLocalContextHolder.put(
					PropertiesConstants.HISTORICAL_STATE_LOGGER,
					createHistorialState(
							systemOption.getClass().getAnnotation(Table.class).name(),OptionStateType.DELETED.getCode(), 
							Long.parseLong(systemOption.getIdSystemOptionPk().toString()),historical));
			
//			
//
//			List<SystemOption> listaHijos = systemServiceFacade
//					.searchNodeChildByOptionServiceBean(systemOption
//							.getIdSystemOptionPk());
//
//			if (listaHijos != null && listaHijos.size() > 0) {
//
//				for (int i = 0; i < listaHijos.size(); i++) {
//					listaHijos.get(i).setState(
//							OptionStateType.DELETED.getCode());
//
//				}
//
//				systemOption.setSystemOptions(listaHijos);
//			}
			resultado = systemServiceFacade.deleteSystemOption(systemOption,objNodoOpcionCatalogoBean.getName());
			if (resultado) {

				Object[] bodyData = new Object[1];
				if (objNodoOpcionCatalogoBean.getOptionUrl() != null) {
					bodyData[0] = objNodoOpcionCatalogoBean.getName()
							.toUpperCase();
					showMessageOnDialog(
							PropertiesConstants.DIALOG_HEADER_SUCCESS,
							null,
							PropertiesConstants.SUCCESS_ADM_OPTION_DELETE_LABEL,
							bodyData);
				} else {
					bodyData[0] = objNodoOpcionCatalogoBean.getName()
							.toUpperCase();
					showMessageOnDialog(
							PropertiesConstants.DIALOG_HEADER_SUCCESS,
							null,
							PropertiesConstants.SUCCESS_ADM_MODULE_DELETE_LABEL,
							bodyData);
				}
				hideDialogues();
				JSFUtilities.executeJavascriptFunction("PF('endTrasaction').show()");
//				JSFUtilities.executeJavascriptFunction("PF('confirmation').hide()");
				loadingMenuBySystem();
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}

	/**
	 * Evaluate motive.
	 */
	public void evaluateMotive() {
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		if (isBlock()) {
			if (OtherMotiveType.SYS_OPT_BLOCK_OTHER_MOTIVE.getCode().equals(
					motiveSelected)) {
				motiveText = "";
				showMotiveText = true;
			} else {
				showMotiveText = false;
			}
		} else if (isUnBlock()) {
			if (OtherMotiveType.SYS_OPT_UNBLOCK_OTHER_MOTIVE.getCode().equals(
					motiveSelected)) {
				motiveText = "";
				showMotiveText = true;
			} else {
				showMotiveText = false;
			}
		} else if (isReject()) {
			if (OtherMotiveType.SYS_OPT_REJECT_OTHER_MOTIVE.getCode().equals(
					motiveSelected)) {
				motiveText = "";
				showMotiveText = true;
			} else {
				showMotiveText = false;
			}
		} else {
			if (OtherMotiveType.SYS_OPT_DELETE_OTHER_MOTIVE.getCode().equals(
					motiveSelected)) {
				motiveText = "";
				showMotiveText = true;
			} else {
				showMotiveText = false;
			}

		}
	}

	/*----------------------------------------------------------------------------------------------------*/
	
	
	/**
	 * File upload handler.
	 *
	 * @param event the event
	 */
	public void fileUploadHandler(FileUploadEvent event){
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);				
		Object [] dataOnMessage = { event.getFile().getFileName(), event.getFile().getSize() / 1000 };
		showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, "adm.opt.change.file", dataOnMessage);
		String fDisplayName = fUploadValidateFile(event.getFile(), null, null);
		if(fDisplayName != null){			 
			try {				
				documentFileName = event.getFile().getFileName();		
				documentFile = event.getFile().getContents();
//				holderAccountRequestHySession.setDocumentPdd(event.getFile().getContents());
//				holderAccountRequestHySession.setNameDocumentPdd(documentFileName);
			} catch (Exception e) {
				e.printStackTrace();
			}
//			 JSFUtilities.showValidationDialog();
		 }
	}

	public String fUploadValidateFile(UploadedFile fileUploaded, Matcher matcher, String maxSize){
		return fUploadValidateFile(fileUploaded, matcher, maxSize,null);
	}
	
	
	public String fUploadValidateFile(UploadedFile fileUploaded, Matcher matcher, String maxSize,String fUploadFileTypes){
		String displayName=null;
		String extensionFile=null;
		try{
			extensionFile= CommonsUtilities.getTrueExtensionFileUpload(fileUploaded.getInputstream());
		}catch(IOException ioex){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getGenericMessage(GeneralPropertiesConstants.FUPL_ERROR_INVALID_FILE_TYPE));
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}
//		String extensionFile = fileUploaded.getFileName().toLowerCase().substring(fileUploaded.getFileName().lastIndexOf(".")+1,fileUploaded.getFileName().length());
		Matcher matcherToUser= null;
		String maxSizeToUse=getfUploadFileSize();
		
		if(matcher!=null){
			matcherToUser=matcher;
		}
		if(maxSize!=null){
			maxSizeToUse=maxSize;
		}
		
		if (matcherToUser!=null && !matcherToUser.matches()) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getGenericMessage(GeneralPropertiesConstants.FUPL_ERROR_INVALID_FILE_TYPE));
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}
		
		if(fUploadFileTypes == null){
			fUploadFileTypes = this.fUploadFileTypes;
		}
		if (!fUploadFileTypes.contains(extensionFile) && matcherToUser==null) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getGenericMessage(GeneralPropertiesConstants.FUPL_ERROR_INVALID_FILE_TYPE));
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}
		
		if (fileUploaded.getSize() > Integer.valueOf(maxSizeToUse)) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getGenericMessage(GeneralPropertiesConstants.FUPL_ERROR_INVALID_FILE_SIZE));
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}
		if(fileUploaded.getFileName().length()>30){
			StringBuilder sbFileName=new StringBuilder();
			sbFileName.append("...");
			sbFileName.append( fileUploaded.getFileName().substring(fileUploaded.getFileName().length()-27) );
			displayName=sbFileName.toString();
		}else{
			displayName=fileUploaded.getFileName();
		}
		return displayName;
	}
	
	public void deleteDocumentFileManual(){
		documentFileName = "";
		documentFile = null;
//		JSFUtilities.putSessionMap("sessStreamedPhoto",null);
	}
	
	public StreamedContent getStreamedContentFile(){
		StreamedContent streamedContentFile = null;
		try {
			setDocumentFileRoot();
			InputStream inputStream = null;
			
			inputStream = new ByteArrayInputStream(documentFile);
			streamedContentFile = new DefaultStreamedContent(inputStream, null,	documentFileName);
			inputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return streamedContentFile;
	}
	
	
	/**
	 * Checks if is activate confirm.
	 * 
	 * @return true, if is activate confirm
	 */
	public boolean isActivateConfirm() {
		return activateConfirm;
	}

	/**
	 * Sets the activate confirm.
	 * 
	 * @param activateConfirm
	 *            the new activate confirm
	 */
	public void setActivateConfirm(boolean activateConfirm) {
		this.activateConfirm = activateConfirm;
	}

	/**
	 * Checks if is activate reject.
	 * 
	 * @return true, if is activate reject
	 */
	public boolean isActivateReject() {
		return activateReject;
	}

	/**
	 * Sets the activate reject.
	 * 
	 * @param activateReject
	 *            the new activate reject
	 */
	public void setActivateReject(boolean activateReject) {
		this.activateReject = activateReject;
	}

	/**
	 * Checks if is activate block.
	 * 
	 * @return true, if is activate block
	 */
	public boolean isActivateBlock() {
		return activateBlock;
	}

	/**
	 * Sets the activate block.
	 * 
	 * @param activateBlock
	 *            the new activate block
	 */
	public void setActivateBlock(boolean activateBlock) {
		this.activateBlock = activateBlock;
	}

	/**
	 * Checks if is activate unblock.
	 * 
	 * @return true, if is activate unblock
	 */
	public boolean isActivateUnblock() {
		return activateUnblock;
	}

	/**
	 * Sets the activate unblock.
	 * 
	 * @param activateUnblock
	 *            the new activate unblock
	 */
	public void setActivateUnblock(boolean activateUnblock) {
		this.activateUnblock = activateUnblock;
	}

	/**
	 * Checks if is activate delete.
	 * 
	 * @return true, if is activate delete
	 */
	public boolean isActivateDelete() {
		return activateDelete;
	}

	/**
	 * Sets the activate delete.
	 * 
	 * @param activateDelete
	 *            the new activate delete
	 */
	public void setActivateDelete(boolean activateDelete) {
		this.activateDelete = activateDelete;
	}

	/**
	 * Checks if is activate add.
	 * 
	 * @return true, if is activate add
	 */
	public boolean isActivateAdd() {
		return activateAdd;
	}

	/**
	 * Sets the activate add.
	 * 
	 * @param activateAdd
	 *            the new activate add
	 */
	public void setActivateAdd(boolean activateAdd) {
		this.activateAdd = activateAdd;
	}

	/**
	 * Checks if is activate modify.
	 * 
	 * @return true, if is activate modify
	 */
	public boolean isActivateModify() {
		return activateModify;
	}

	/**
	 * Sets the activate modify.
	 * 
	 * @param activateModify
	 *            the new activate modify
	 */
	public void setActivateModify(boolean activateModify) {
		this.activateModify = activateModify;
	}

	/**
	 * Checks if is activate consult.
	 * 
	 * @return true, if is activate consult
	 */
	public boolean isActivateConsult() {
		return activateConsult;
	}

	/**
	 * Sets the activate consult.
	 * 
	 * @param activateConsult
	 *            the new activate consult
	 */
	public void setActivateConsult(boolean activateConsult) {
		this.activateConsult = activateConsult;
	}

	/**
	 * Checks if is activate context menu.
	 * 
	 * @return true, if is activate context menu
	 */
	public boolean isActivateContextMenu() {
		return activateContextMenu;
	}

	/**
	 * Sets the activate context menu.
	 * 
	 * @param activateContextMenu
	 *            the new activate context menu
	 */
	public void setActivateContextMenu(boolean activateContextMenu) {
		this.activateContextMenu = activateContextMenu;
	}

	/**
	 * Gets the message dialog confirmation.
	 * 
	 * @return the message dialog confirmation
	 */
	public String getMessageDialogConfirmation() {
		return messageDialogConfirmation;
	}

	/**
	 * Sets the message dialog confirmation.
	 * 
	 * @param messageDialogConfirmation
	 *            the new message dialog confirmation
	 */
	public void setMessageDialogConfirmation(String messageDialogConfirmation) {
		this.messageDialogConfirmation = messageDialogConfirmation;
	}

	/**
	 * Gets the label action.
	 * 
	 * @return the label action
	 */
	public String getLabelAction() {
		return labelAction;
	}

	/**
	 * Sets the label action.
	 * 
	 * @param labelAction
	 *            the new label action
	 */
	public void setLabelAction(String labelAction) {
		this.labelAction = labelAction;
	}

	/**
	 * Gets the list option type.
	 * 
	 * @return the list option type
	 */
	public List<OptionType> getListOptionType() {
		return listOptionType;
	}

	/**
	 * Sets the list option type.
	 * 
	 * @param listOptionType
	 *            the new list option type
	 */
	public void setListOptionType(List<OptionType> listOptionType) {
		this.listOptionType = listOptionType;
	}

	/**
	 * Gets the opcion type.
	 * 
	 * @return the opcion type
	 */

	/**
	 * Checks if is view url.
	 * 
	 * @return true, if is view url
	 */
	public boolean isViewURL() {
		return viewURL;
	}

	/**
	 * Gets the opt type.
	 * 
	 * @return the opt type
	 */
	public Integer getOptType() {
		return optType;
	}

	/**
	 * Sets the opt type.
	 * 
	 * @param optType
	 *            the new opt type
	 */
	public void setOptType(Integer optType) {
		this.optType = optType;
	}

	/**
	 * Sets the view url.
	 * 
	 * @param viewURL
	 *            the new view url
	 */
	public void setViewURL(boolean viewURL) {
		this.viewURL = viewURL;
	}

	/**
	 * Gets the url option.
	 * 
	 * @return the url option
	 */
	public String getUrlOption() {
		return urlOption;
	}

	/**
	 * Sets the url option.
	 * 
	 * @param urlOption
	 *            the new url option
	 */
	public void setUrlOption(String urlOption) {
		this.urlOption = urlOption;
	}

	/**
	 * Gets the parameter data model.
	 * 
	 * @return the parameter data model
	 */
	public ParameterDataModel getParameterDataModel() {
		return parameterDataModel;
	}

	/**
	 * Sets the parameter data model.
	 * 
	 * @param parameterDataModel
	 *            the new parameter data model
	 */
	public void setParameterDataModel(ParameterDataModel parameterDataModel) {
		this.parameterDataModel = parameterDataModel;
	}

	/**
	 * Gets the privilege option.
	 * 
	 * @return the privilege option
	 */
	public OptionPrivilege getPrivilegeOption() {
		return privilegeOption;
	}

	/**
	 * Sets the privilege option.
	 * 
	 * @param privilegeOption
	 *            the new privilege option
	 */
	public void setPrivilegeOption(OptionPrivilege privilegeOption) {
		this.privilegeOption = privilegeOption;
	}

	/**
	 * Gets the list privileges.
	 * 
	 * @return the list privileges
	 */
	public List<Parameter> getListPrivileges() {
		return listPrivileges;
	}

	/**
	 * Sets the list privileges.
	 * 
	 * @param listPrivileges
	 *            the new list privileges
	 */
	public void setListPrivileges(List<Parameter> listPrivileges) {
		this.listPrivileges = listPrivileges;
	}

	/**
	 * Checks if is flag next.
	 * 
	 * @return true, if is flag next
	 */
	public boolean isFlagNext() {
		return flagNext;
	}

	/**
	 * Sets the flag next.
	 * 
	 * @param flagNext
	 *            the new flag next
	 */
	public void setFlagNext(boolean flagNext) {
		this.flagNext = flagNext;
	}

	/**
	 * Checks if is flag view tab spanish language.
	 * 
	 * @return true, if is flag view tab spanish language
	 */
	public boolean isFlagViewTabSpanishLanguage() {
		return flagViewTabSpanishLanguage;
	}

	/**
	 * Sets the flag view tab spanish language.
	 * 
	 * @param flagViewTabSpanishLanguage
	 *            the new flag view tab spanish language
	 */
	public void setFlagViewTabSpanishLanguage(boolean flagViewTabSpanishLanguage) {
		this.flagViewTabSpanishLanguage = flagViewTabSpanishLanguage;
	}

	/**
	 * Checks if is flag view tab english language.
	 * 
	 * @return true, if is flag view tab english language
	 */
	public boolean isFlagViewTabEnglishLanguage() {
		return flagViewTabEnglishLanguage;
	}

	/**
	 * Sets the flag view tab english language.
	 * 
	 * @param flagViewTabEnglishLanguage
	 *            the new flag view tab english language
	 */
	public void setFlagViewTabEnglishLanguage(boolean flagViewTabEnglishLanguage) {
		this.flagViewTabEnglishLanguage = flagViewTabEnglishLanguage;
	}

	/**
	 * Checks if is flag view tab frances language.
	 * 
	 * @return true, if is flag view tab frances language
	 */
	public boolean isFlagViewTabFrancesLanguage() {
		return flagViewTabFrancesLanguage;
	}

	/**
	 * Sets the flag view tab frances language.
	 * 
	 * @param flagViewTabFrancesLanguage
	 *            the new flag view tab frances language
	 */
	public void setFlagViewTabFrancesLanguage(boolean flagViewTabFrancesLanguage) {
		this.flagViewTabFrancesLanguage = flagViewTabFrancesLanguage;
	}

	/**
	 * Checks if is flag disable.
	 * 
	 * @return true, if is flag disable
	 */
	public boolean isFlagDisable() {
		return flagDisable;
	}

	/**
	 * Sets the flag disable.
	 * 
	 * @param flagDisable
	 *            the new flag disable
	 */
	public void setFlagDisable(boolean flagDisable) {
		this.flagDisable = flagDisable;
	}

	/**
	 * Checks if is flag view option mode.
	 * 
	 * @return true, if is flag view option mode
	 */
	public boolean isFlagViewOptionMode() {
		return flagViewOptionMode;
	}

	/**
	 * Sets the flag view option mode.
	 * 
	 * @param flagViewOptionMode
	 *            the new flag view option mode
	 */
	public void setFlagViewOptionMode(boolean flagViewOptionMode) {
		this.flagViewOptionMode = flagViewOptionMode;
	}

	/**
	 * Checks if is show dialog confirm.
	 * 
	 * @return true, if is show dialog confirm
	 */
	public boolean isShowDialogConfirm() {
		return showDialogConfirm;
	}

	/**
	 * Sets the show dialog confirm.
	 *
	 * @param showDialogConfirm the new show dialog confirm
	 */
	public void setShowDialogConfirm(boolean showDialogConfirm) {
		this.showDialogConfirm = showDialogConfirm;
	}

	/**
	 * Checks if is show dialog motive.
	 * 
	 * @return true, if is show dialog motive
	 */
	public boolean isShowDialogMotive() {
		return showDialogMotive;
	}

	/**
	 * Sets the show dialog motive.
	 * 
	 * @param showDialogMotive
	 *            the new show dialog motive
	 */
	public void setShowDialogMotive(boolean showDialogMotive) {
		this.showDialogMotive = showDialogMotive;
	}

	/**
	 * Checks if is show end transaction.
	 * 
	 * @return true, if is show end transaction
	 */
	public boolean isShowEndTransaction() {
		return showEndTransaction;
	}

	/**
	 * Sets the show end transaction.
	 * 
	 * @param showEndTransaction
	 *            the new show end transaction
	 */
	public void setShowEndTransaction(boolean showEndTransaction) {
		this.showEndTransaction = showEndTransaction;
	}

	/**
	 * Checks if is show motive text.
	 * 
	 * @return true, if is show motive text
	 */
	public boolean isShowMotiveText() {
		return showMotiveText;
	}

	/**
	 * Sets the show motive text.
	 * 
	 * @param showMotiveText
	 *            the new show motive text
	 */
	public void setShowMotiveText(boolean showMotiveText) {
		this.showMotiveText = showMotiveText;
	}

	/**
	 * Checks if is item not selected.
	 *
	 * @return the itemNotSelected
	 */
	public boolean isItemNotSelected() {
		return itemNotSelected;
	}

	/**
	 * Sets the item not selected.
	 *
	 * @param itemNotSelected            the itemNotSelected to set
	 */
	public void setItemNotSelected(boolean itemNotSelected) {
		this.itemNotSelected = itemNotSelected;
	}

	/**
	 * Validate order option.
	 */
	public void validateOrderOption() {
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);

		Integer systemidpk = null;
		Integer optionidPk = null;
		Integer parentidpk = null;
		Integer orderOptionId = null;
		optionidPk = null;
		JSFUtilities.hideGeneralDialogues();
		try {
			if (Validations.validateIsNotNullAndPositive(orderOption)) {
				if (orderOption.equals(oldOrderOption)) {
					return;
				} else {
					if (activateAdd) {
						OptionCatalogNodeBean selectedNode = (OptionCatalogNodeBean) nodeSelected
								.getData();
						if (Validations
								.validateIsNotNullAndNotEmpty(selectedNode
										.getIdSystemOptionPk())) {
							parentidpk = selectedNode.getIdSystemOptionPk();
						}
						if (selectedNode.getInstanceType().equals(
								InstanceType.SYSTEM.getCode())) {
							systemidpk = selectedNode.getIdSystemPk();
						}
						orderOptionId = systemServiceFacade
								.getSystemOptionByOrderOptionServiceFacade(
										systemidpk, orderOption, optionidPk,
										parentidpk);
						if (Validations
								.validateIsNotNullAndNotEmpty(orderOptionId)) {
							orderOption = null;
							String msg = PropertiesUtilities
									.getMessage(PropertiesConstants.ORDER_ALREADY_REGISTERED);
							JSFUtilities.addContextMessage(
									"frmSystemOptionMgmt:txtOrderOption",
									FacesMessage.SEVERITY_ERROR, msg, msg);
							JSFUtilities.showEndTransactionSamePageDialog();
							JSFUtilities
									.showMessageOnDialog(
											null,
											null,
											PropertiesConstants.ORDER_ALREADY_REGISTERED,
											null);
							return;
						} else {
							oldOrderOption = orderOption;
						}
					} else if (activateModify) {
						OptionCatalogNodeBean parentNode = (OptionCatalogNodeBean) nodeSelected
								.getParent().getData();
						OptionCatalogNodeBean selectedNode = (OptionCatalogNodeBean) nodeSelected
								.getData();
						optionidPk = selectedNode.getIdSystemOptionPk();
						if (Validations.validateIsNotNullAndNotEmpty(parentNode
								.getIdSystemOptionPk())) {
							parentidpk = parentNode.getIdSystemOptionPk();
						}
						if (parentNode.getInstanceType().equals(
								InstanceType.SYSTEM.getCode())) {
							systemidpk = parentNode.getIdSystemPk();
						}
						orderOptionId = systemServiceFacade
								.getSystemOptionByOrderOptionServiceFacade(
										systemidpk, orderOption, optionidPk,
										parentidpk);
						if (Validations
								.validateIsNotNullAndNotEmpty(orderOptionId)) {
							orderOption = null;
							String msg = PropertiesUtilities
									.getMessage(PropertiesConstants.ORDER_ALREADY_REGISTERED);
							JSFUtilities.addContextMessage(
									"frmSystemOptionMgmt:txtOrderOption",
									FacesMessage.SEVERITY_ERROR, msg, msg);
							JSFUtilities.showEndTransactionSamePageDialog();
							JSFUtilities
									.showMessageOnDialog(
											null,
											null,
											PropertiesConstants.ORDER_ALREADY_REGISTERED,
											null);
							return;
						} else {
							oldOrderOption = orderOption;
						}
					}
				}
			} else {
				orderOption = null;
				return;
			}
		} catch (Exception ex) {
			// excepcion.fire(new ExceptionToCatchEvent(ex));
			java.lang.System.out.println(ex);
		}
	}

	/**
	 * Cleanup.
	 */
	@PreDestroy
	void cleanup() {
		endConversation();
	}

	
	public void setDocumentFileRoot() throws IOException{
		documentFile = CommonsUtilities.readFile(new File(fileRootPath+documentFilePath+File.separator+documentFileName));
	}
	
	/**
	 * Validate order value.
	 */
	public void validateOrderValue() {
		JSFUtilities.executeJavascriptFunction("PF('idRemoveOrderCfrDlgWin').hide()");
		orderOption = null;
		String msg = PropertiesUtilities
				.getMessage(PropertiesConstants.ORDER_ALREADY_REGISTERED);
		JSFUtilities.addContextMessage("frmSystemOptionMgmt:txtOrderOption",
				FacesMessage.SEVERITY_ERROR, msg, msg);
	}

	/*----------------------------------------------------------------------------------------------------*/

	/**
	 * Gets the motive selected.
	 * 
	 * @return the motive selected
	 */
	public Integer getMotiveSelected() {
		return motiveSelected;
	}

	/**
	 * Sets the motive selected.
	 * 
	 * @param motiveSelected
	 *            the new motive selected
	 */
	public void setMotiveSelected(Integer motiveSelected) {
		this.motiveSelected = motiveSelected;
	}

	/**
	 * Gets the motive text.
	 * 
	 * @return the motive text
	 */
	public String getMotiveText() {
		return motiveText;
	}

	/**
	 * Sets the motive text.
	 * 
	 * @param motiveText
	 *            the new motive text
	 */
	public void setMotiveText(String motiveText) {
		this.motiveText = motiveText;
	}

	/**
	 * Gets the system selected.
	 * 
	 * @return the system selected
	 */
	public Integer getSystemSelected() {
		return systemSelected;
	}

	/**
	 * Sets the system selected.
	 * 
	 * @param systemSelected
	 *            the new system selected
	 */
	public void setSystemSelected(Integer systemSelected) {
		this.systemSelected = systemSelected;
	}

	/**
	 * Gets the bmotive descripcion.
	 *
	 * @return the bmotive descripcion
	 */
	public String getBmotiveDescripcion() {
		return bmotiveDescripcion;
	}

	/**
	 * Sets the bmotive descripcion.
	 *
	 * @param bmotiveDescripcion the new bmotive descripcion
	 */
	public void setBmotiveDescripcion(String bmotiveDescripcion) {
		this.bmotiveDescripcion = bmotiveDescripcion;
	}

	/**
	 * Gets the ubmotive descripcion.
	 *
	 * @return the ubmotive descripcion
	 */
	public String getUbmotiveDescripcion() {
		return ubmotiveDescripcion;
	}

	/**
	 * Sets the ubmotive descripcion.
	 *
	 * @param ubmotiveDescripcion the new ubmotive descripcion
	 */
	public void setUbmotiveDescripcion(String ubmotiveDescripcion) {
		this.ubmotiveDescripcion = ubmotiveDescripcion;
	}

	/**
	 * Gets the ubother motive descripcion.
	 *
	 * @return the ubotherMotiveDescripcion
	 */
	public String getUbotherMotiveDescripcion() {
		return ubotherMotiveDescripcion;
	}

	/**
	 * Gets the bother motive descripcion.
	 *
	 * @return the botherMotiveDescripcion
	 */
	public String getBotherMotiveDescripcion() {
		return botherMotiveDescripcion;
	}

	/**
	 * Sets the bother motive descripcion.
	 *
	 * @param botherMotiveDescripcion            the botherMotiveDescripcion to set
	 */
	public void setBotherMotiveDescripcion(String botherMotiveDescripcion) {
		this.botherMotiveDescripcion = botherMotiveDescripcion;
	}

	/**
	 * Sets the ubother motive descripcion.
	 *
	 * @param ubotherMotiveDescripcion            the ubotherMotiveDescripcion to set
	 */
	public void setUbotherMotiveDescripcion(String ubotherMotiveDescripcion) {
		this.ubotherMotiveDescripcion = ubotherMotiveDescripcion;
	}

	/**
	 * Gets the active index.
	 *
	 * @return the activeIndex
	 */
	public Integer getActiveIndex() {
		return activeIndex;
	}

	/**
	 * Sets the active index.
	 *
	 * @param activeIndex            the activeIndex to set
	 */
	public void setActiveIndex(Integer activeIndex) {
		this.activeIndex = activeIndex;
	}

	/**
	 * Gets the order option.
	 * 
	 * @return the orderOption
	 */
	public Integer getOrderOption() {
		return orderOption;
	}

	/**
	 * Sets the order option.
	 * 
	 * @param orderOption
	 *            the orderOption to set
	 */
	public void setOrderOption(Integer orderOption) {
		this.orderOption = orderOption;
	}

	/**
	 * Gets the old order option.
	 *
	 * @return the oldOrderOption
	 */
	public Integer getOldOrderOption() {
		return oldOrderOption;
	}

	/**
	 * Sets the old order option.
	 *
	 * @param oldOrderOption            the oldOrderOption to set
	 */
	public void setOldOrderOption(Integer oldOrderOption) {
		this.oldOrderOption = oldOrderOption;
	}

	/**
	 * Gets the list system option.
	 * 
	 * @return the list system option
	 */
	public List<SystemOption> getListSystemOption() {
		return listSystemOption;
	}

	/**
	 * Sets the list system option.
	 * 
	 * @param listSystemOption
	 *            the new list system option
	 */
	public void setListSystemOption(List<SystemOption> listSystemOption) {
		this.listSystemOption = listSystemOption;
	}

	/**
	 * Gets the aux event node.
	 * 
	 * @return the aux event node
	 */
	public OptionCatalogNodeBean getAuxEventNode() {
		return auxEventNode;
	}

	/**
	 * Sets the aux event node.
	 * 
	 * @param auxEventNode
	 *            the new aux event node
	 */
	public void setAuxEventNode(OptionCatalogNodeBean auxEventNode) {
		this.auxEventNode = auxEventNode;
	}

	/**
	 * Gets the privileges selected.
	 * 
	 * @return the privileges selected
	 */
	public Parameter[] getPrivilegesSelected() {
		return privilegesSelected;
	}

	/**
	 * Sets the privileges selected.
	 * 
	 * @param privilegesSelected
	 *            the new privileges selected
	 */
	public void setPrivilegesSelected(Parameter[] privilegesSelected) {
		this.privilegesSelected = privilegesSelected;
	}

	/**
	 * Gets the system service facade.
	 * 
	 * @return the system service facade
	 */
	public SystemServiceFacade getSystemServiceFacade() {
		return systemServiceFacade;
	}

	/**
	 * Sets the system service facade.
	 * 
	 * @param systemServiceFacade
	 *            the new system service facade
	 */
	public void setSystemServiceFacade(SystemServiceFacade systemServiceFacade) {
		this.systemServiceFacade = systemServiceFacade;
	}

	/**
	 * Gets the system option.
	 * 
	 * @return the system option
	 */
	public SystemOption getSystemOption() {
		return systemOption;
	}

	/**
	 * Sets the system option.
	 * 
	 * @param systemOption
	 *            the new system option
	 */
	public void setSystemOption(SystemOption systemOption) {
		this.systemOption = systemOption;
	}

	/**
	 * Gets the obj system.
	 * 
	 * @return the obj system
	 */
	public System getObjSystem() {
		return objSystem;
	}

	/**
	 * Sets the obj system.
	 * 
	 * @param objSystem
	 *            the new obj system
	 */
	public void setObjSystem(System objSystem) {
		this.objSystem = objSystem;
	}

	/**
	 * Gets the obj catalog spanish language.
	 * 
	 * @return the obj catalog spanish language
	 */
	public CatalogueLanguage getObjCatalogSpanishLanguage() {
		return objCatalogSpanishLanguage;
	}

	/**
	 * Sets the obj catalog spanish language.
	 * 
	 * @param objCatalogSpanishLanguage
	 *            the new obj catalog spanish language
	 */
	public void setObjCatalogSpanishLanguage(
			CatalogueLanguage objCatalogSpanishLanguage) {
		this.objCatalogSpanishLanguage = objCatalogSpanishLanguage;
	}

	/**
	 * Gets the obj catalog english language.
	 * 
	 * @return the obj catalog english language
	 */
	public CatalogueLanguage getObjCatalogEnglishLanguage() {
		return objCatalogEnglishLanguage;
	}

	/**
	 * Sets the obj catalog english language.
	 * 
	 * @param objCatalogEnglishLanguage
	 *            the new obj catalog english language
	 */
	public void setObjCatalogEnglishLanguage(
			CatalogueLanguage objCatalogEnglishLanguage) {
		this.objCatalogEnglishLanguage = objCatalogEnglishLanguage;
	}

	/**
	 * Gets the obj catalogo frances language.
	 * 
	 * @return the obj catalogo frances language
	 */
	public CatalogueLanguage getObjCatalogoFrancesLanguage() {
		return objCatalogoFrancesLanguage;
	}

	/**
	 * Sets the obj catalogo frances language.
	 * 
	 * @param objCatalogoFrancesLanguage
	 *            the new obj catalogo frances language
	 */
	public void setObjCatalogoFrancesLanguage(
			CatalogueLanguage objCatalogoFrancesLanguage) {
		this.objCatalogoFrancesLanguage = objCatalogoFrancesLanguage;
	}

	/**
	 * Gets the node selected.
	 * 
	 * @return the node selected
	 */
	public TreeNode getNodeSelected() {
		return nodeSelected;
	}

	/**
	 * Sets the node selected.
	 * 
	 * @param nodeSelected
	 *            the new node selected
	 */
	public void setNodeSelected(TreeNode nodeSelected) {
		this.nodeSelected = nodeSelected;
		if (this.nodeSelected == null) {
			itemNotSelected = true;
//			JSFUtilities.executeJavascriptFunction("PF('confitemSelectedmsgvar').show()");
		}
	}

	/**
	 * Gets the root.
	 * 
	 * @return the root
	 */
	public TreeNode getRoot() {
		return root;
	}

	/**
	 * Sets the root.
	 * 
	 * @param root
	 *            the new root
	 */
	public void setRoot(TreeNode root) {
		this.root = root;
	}

	/**
	 * Gets the obj system selected.
	 * 
	 * @return the obj system selected
	 */
	public System getObjSystemSelected() {
		return objSystemSelected;
	}

	/**
	 * Sets the obj system selected.
	 * 
	 * @param objSystemSelected
	 *            the new obj system selected
	 */
	public void setObjSystemSelected(System objSystemSelected) {
		this.objSystemSelected = objSystemSelected;
	}

	/**
	 * Checks if is show confirm reject in modal.
	 *
	 * @return true, if is show confirm reject in modal
	 */
	public boolean isShowConfirmRejectInModal() {
		return showConfirmRejectInModal;
	}

	/**
	 * Sets the show confirm reject in modal.
	 *
	 * @param showConfirmRejectInModal the new show confirm reject in modal
	 */
	public void setShowConfirmRejectInModal(boolean showConfirmRejectInModal) {
		this.showConfirmRejectInModal = showConfirmRejectInModal;
	}
	
	/**
	 * Gets the system production file streamed.
	 *
	 * @return the system production file streamed
	 */
	public StreamedContent getSystemProductionFileStreamed(){
		StreamedContent streamedFile = null;
		if(systemSelected != null && systemSelected >0){
			byte[] systemProductionFile = systemServiceFacade.getSystemProductionFile(systemSelected);
			if (systemProductionFile == null){
				return null;	
			}
			InputStream file = new ByteArrayInputStream(systemProductionFile);
			try{
				streamedFile=new DefaultStreamedContent(file,"application/pdf","document");
			}
			catch(Exception ex){ return null;}
		}
		return streamedFile;
	}

	public String getPdfFileUploadFiltTypes() {
		return pdfFileUploadFiltTypes;
	}

	public void setPdfFileUploadFiltTypes(String pdfFileUploadFiltTypes) {
		this.pdfFileUploadFiltTypes = pdfFileUploadFiltTypes;
	}

	public String getfUploadFileSizeMgmt() {
		return fUploadFileSizeMgmt;
	}

	public void setfUploadFileSizeMgmt(String fUploadFileSizeMgmt) {
		this.fUploadFileSizeMgmt = fUploadFileSizeMgmt;
	}

	public String getfUploadFileSizeDisplayMgmt() {
		return fUploadFileSizeDisplayMgmt;
	}

	public void setfUploadFileSizeDisplayMgmt(String fUploadFileSizeDisplayMgmt) {
		this.fUploadFileSizeDisplayMgmt = fUploadFileSizeDisplayMgmt;
	}

	public String getDocumentFileName() {
		return documentFileName;
	}

	public void setDocumentFileName(String documentFileName) {
		this.documentFileName = documentFileName;
	}

	public byte[] getDocumentFile() {
		return documentFile;
	}

	public void setDocumentFile(byte[] documentFile) {
		this.documentFile = documentFile;
	}
}
