package com.pradera.security.systemmgmt.view;

import java.io.Serializable;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class NodeSystemProfileBean
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public class NodeSystemProfileBean implements Serializable{
	   
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id sytem pk. */
	private Integer idSytemPk;
	
	/** The id system profile pk. */
	private Integer idSystemProfilePk;
	
	/** The name. */
	private String name;
	
	/** The description. */
	private String description;
	
	/** The level. */
	private Integer level;
		

	/**
	 * Gets the id sytem pk.
	 *
	 * @return the idSytemPk
	 */
	public Integer getIdSytemPk() {
		return idSytemPk;
	}


	/**
	 * Sets the id sytem pk.
	 *
	 * @param idSytemPk the idSytemPk to set
	 */
	public void setIdSytemPk(Integer idSytemPk) {
		this.idSytemPk = idSytemPk;
	}


	/**
	 * Gets the id system profile pk.
	 *
	 * @return the idSystemProfilePk
	 */
	public Integer getIdSystemProfilePk() {
		return idSystemProfilePk;
	}


	/**
	 * Sets the id system profile pk.
	 *
	 * @param idSystemProfilePk the idSystemProfilePk to set
	 */
	public void setIdSystemProfilePk(Integer idSystemProfilePk) {
		this.idSystemProfilePk = idSystemProfilePk;
	}


	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	/**
	 * Sets the name.
	 *
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}


	/**
	 * Sets the description.
	 *
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}


	/**
	 * Gets the level.
	 *
	 * @return the level
	 */
	public Integer getLevel() {
		return level;
	}


	/**
	 * Sets the level.
	 *
	 * @param level the level to set
	 */
	public void setLevel(Integer level) {
		this.level = level;
	}


	/**
	 * Instantiates a new node system profile bean.
	 */
	public NodeSystemProfileBean() {
		// TODO Auto-generated constructor stub
	}



}
