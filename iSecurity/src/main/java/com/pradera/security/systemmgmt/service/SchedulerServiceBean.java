package com.pradera.security.systemmgmt.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Singleton;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.model.ExceptionUserPrivilege;
import com.pradera.security.model.ScheduleException;
import com.pradera.security.model.type.ScheduleExceptionStateType;
import com.pradera.security.model.type.UserExceptionStateType;
import com.pradera.security.utils.view.TimerTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary. Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class SchedulerServiceBean.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 24/06/2013
 */
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class SchedulerServiceBean extends CrudSecurityServiceBean {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The timer service. */
	@Resource
	TimerService timerService;

	/** The log. */
	@Inject
	PraderaLogger log;

	/**
	 * Load timers.
	 */
	@PostConstruct
	public void loadTimers() {
		try {
			List<ScheduleException> listScheduleException = getScheduleException();
			List<ExceptionUserPrivilege> listExceptionUserPrivilege = getExceptionUserPrivilege();
			
			for (ScheduleException scheduleException : listScheduleException) {
				registerTimer(scheduleException);
			}
			
			for(ExceptionUserPrivilege exceptionUserPrivilege : listExceptionUserPrivilege){
				registerTimer(exceptionUserPrivilege);
			}			
		} catch (ServiceException ex) {
			log.info("error loading timers " + ex.getMessage());
		}
	}
	
	/**
	 * Register timer.
	 *
	 * @param <T> the generic type
	 * @param t the t
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public <T> void registerTimer (T t) {
		//ScheduleExpression scheduleExpression = null;
		TimerConfig timerConfig = null;
		Calendar cal = Calendar.getInstance();
		
		if(t.getClass().getName().equals(ScheduleException.class.getName())){
			ScheduleException scheduleException = (ScheduleException)t;
			TimerTO timerTO = new TimerTO(scheduleException.getIdScheduleExceptionPk(), scheduleException.getClass().getName());
			timerConfig = new TimerConfig(timerTO, Boolean.FALSE);
			//scheduleExpression = new ScheduleExpression();
			
			cal.setTime(scheduleException.getEndDate());
			
			Calendar hour = Calendar.getInstance();
			hour.setTime(scheduleException.getEndHour());	
			
			cal.set(Calendar.HOUR_OF_DAY, hour.get(Calendar.HOUR_OF_DAY));
			cal.set(Calendar.MINUTE, hour.get(Calendar.MINUTE));
			cal.set(Calendar.SECOND, hour.get(Calendar.SECOND));
			
			//scheduleExpression.start(cal.getTime());
		}else if(t.getClass().getName().equals(ExceptionUserPrivilege.class.getName())){
			ExceptionUserPrivilege exceptionUserPrivilege = (ExceptionUserPrivilege)t;
			TimerTO timerTO = new TimerTO(exceptionUserPrivilege.getIdExceptionUserPrivilegePk().longValue(), exceptionUserPrivilege.getClass().getName());
			timerConfig = new TimerConfig(timerTO, Boolean.FALSE);
			//scheduleExpression = new ScheduleExpression();
			
			cal.setTime(exceptionUserPrivilege.getEndDate());
			
			//scheduleExpression.start(cal.getTime());
		}
		
		if(timerConfig != null){
			timerService.createSingleActionTimer(cal.getTime(), timerConfig);
			//timerService.createCalendarTimer(scheduleExpression, timerConfig);
		}
	}
	
	/**
	 * Gets the schedule exception.
	 *
	 * @return the schedule exception
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ScheduleException> getScheduleException() throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select se from ScheduleException se ");
		sbQuery.append(" where se.state in :listStatePrm");
		sbQuery.append(" and se.endDate <= :endDatePrm");
		sbQuery.append(" and HOUR(se.endHour) <= :endHourPrm");
		sbQuery.append(" and MINUTE(se.endHour) <= :endMinutePrm");
		sbQuery.append(" and SECOND(se.endHour) <= :endSecondPrm");

		Query query = em.createQuery(sbQuery.toString());

		Calendar cal = Calendar.getInstance();
		query.setParameter("endDatePrm", cal.getTime());
		query.setParameter("endHourPrm", cal.get(Calendar.HOUR_OF_DAY));
		query.setParameter("endMinutePrm", cal.get(Calendar.MINUTE));
		query.setParameter("endSecondPrm", cal.get(Calendar.SECOND));
		List<Integer> listState = new ArrayList<Integer>();
		listState.add(ScheduleExceptionStateType.REGISTERED.getCode());
		listState.add(ScheduleExceptionStateType.CONFIRMED.getCode());
		query.setParameter("listStatePrm", listState);
		
		List<ScheduleException> listScheduleException = new ArrayList<ScheduleException>();
		
		try {
			listScheduleException = query.getResultList();
		} catch (NoResultException ex) {
			log.info("error in getScheduleException : " + ex.getMessage());
		}
		
		for(ScheduleException scheduleException : listScheduleException){
			TimerTO timerTO = new TimerTO(scheduleException.getIdScheduleExceptionPk(), scheduleException.getClass().getName());
			Timer timer = getTimer(timerTO);
			if(timer != null){
				listScheduleException.remove(scheduleException);
			}			
		}
		return listScheduleException;
	}
	
	/**
	 * Gets the exception user privilege.
	 *
	 * @return the exception user privilege
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ExceptionUserPrivilege> getExceptionUserPrivilege() throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select eup from ExceptionUserPrivilege eup ");
		sbQuery.append(" where eup.state = :statePrm");
		sbQuery.append(" and eup.endDate <= :endDatePrm");

		Query query = em.createQuery(sbQuery.toString());

		Calendar cal = Calendar.getInstance();
		query.setParameter("statePrm", UserExceptionStateType.REGISTERED.getCode());
		query.setParameter("endDatePrm", cal.getTime());

		List<ExceptionUserPrivilege> listExceptionUserPrivilege = new ArrayList<ExceptionUserPrivilege>();

		try {
			listExceptionUserPrivilege = query.getResultList();
		} catch (NoResultException ex) {
			log.info("error in getExceptionUserPrivilege : " + ex.getMessage());
		}
		
		for(ExceptionUserPrivilege exceptionUserPrivilege : listExceptionUserPrivilege){
			TimerTO timerTO = new TimerTO(exceptionUserPrivilege.getIdExceptionUserPrivilegePk().longValue(), exceptionUserPrivilege.getClass().getName());
			Timer timer = getTimer(timerTO);
			if(timer != null){
				listExceptionUserPrivilege.remove(exceptionUserPrivilege);
			}			
		}
		return listExceptionUserPrivilege;
	}

	/**
	 * Execute timer.
	 *
	 * @param timer the timer
	 */
	@Timeout
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void executeTimer(Timer timer) {		
		TimerTO timerTO = (TimerTO) timer.getInfo();
		StringBuilder sbQuery = new StringBuilder();
		Query query = null;
		
		if(timerTO.getEntityClass().equals(ScheduleException.class.getName())){			
			sbQuery.append(" update ScheduleException se");
			sbQuery.append(" set se.state = :statePrm");
			sbQuery.append(" where se.idScheduleExceptionPk = :idScheduleExceptionPkPrm");
			query = em.createQuery(sbQuery.toString());
			query.setParameter("statePrm", ScheduleExceptionStateType.REVOCATE.getCode());
			query.setParameter("idScheduleExceptionPkPrm", timerTO.getEntityId());
		}else if(timerTO.getEntityClass().equals(ExceptionUserPrivilege.class.getName())){
			sbQuery.append(" update ExceptionUserPrivilege eup");
			sbQuery.append(" set eup.state = :statePrm");
			sbQuery.append(" where eup.idExceptionUserPrivilegePk = :idExceptionUserPrivilegePkPrm");
			query = em.createQuery(sbQuery.toString());
			query.setParameter("statePrm", UserExceptionStateType.ANNULLED.getCode());
			query.setParameter("idExceptionUserPrivilegePkPrm", Integer.valueOf(timerTO.getEntityId().intValue()));
		}
		
		if(query != null){
			try {
				query.executeUpdate();
			} catch (NoResultException ex) {
				log.info("error in getScheduleException : " + ex.getMessage());
			}
		}
	}

	/**
	 * Update timer.
	 *
	 * @param <T> the generic type
	 * @param t the t
	 * @throws ServiceException the service exception
	 */
	public <T> void updateTimer(T t) throws ServiceException {
		TimerTO timerTO = null;
		
		if(t.getClass().getName().equals(ScheduleException.class.getName())){
			ScheduleException scheduleException = (ScheduleException)t;
			timerTO = new TimerTO(scheduleException.getIdScheduleExceptionPk(), scheduleException.getClass().getName());
						
		}else if(t.getClass().getName().equals(ExceptionUserPrivilege.class.getName())){
			ExceptionUserPrivilege exceptionUserPrivilege = (ExceptionUserPrivilege)t;
			timerTO = new TimerTO(exceptionUserPrivilege.getIdExceptionUserPrivilegePk().longValue(), exceptionUserPrivilege.getClass().getName());
		}
			
		if(timerTO != null){
			Timer timer = getTimer(timerTO);
			if(timer != null){
				deleteTimer(timerTO);				
			}
			registerTimer(t);
		}		
	}
	
	/**
	 * Request delete.
	 *
	 * @param <T> the generic type
	 * @param t the t
	 * @throws ServiceException the service exception
	 */
	public <T> void requestDelete(T t) throws ServiceException {
		TimerTO timerTO = null;
		
		if(t.getClass().getName().equals(ScheduleException.class.getName())){
			ScheduleException scheduleException = (ScheduleException)t;
			timerTO = new TimerTO(scheduleException.getIdScheduleExceptionPk(), scheduleException.getClass().getName());
						
		}else if(t.getClass().getName().equals(ExceptionUserPrivilege.class.getName())){
			ExceptionUserPrivilege exceptionUserPrivilege = (ExceptionUserPrivilege)t;
			timerTO = new TimerTO(exceptionUserPrivilege.getIdExceptionUserPrivilegePk().longValue(), exceptionUserPrivilege.getClass().getName());
		}
			
		if(timerTO != null){
			Timer timer = getTimer(timerTO);
			if(timer != null){
				deleteTimer(timerTO);				
			}
		}		
	}

	/**
	 * Gets the timer.
	 *
	 * @param timerTO the timer to
	 * @return the timer
	 */
	private Timer getTimer(TimerTO timerTO) {
		Collection<Timer> timers = timerService.getTimers();
		for (Timer t : timers) {
			if (timerTO.equals((TimerTO) t.getInfo())) {
				return t;
			}
		}
		return null;
	}

	/**
	 * Delete timer.
	 *
	 * @param timerTO the timer to
	 */
	public void deleteTimer(TimerTO timerTO) {
		Timer timer = getTimer(timerTO);
		if (timer != null) {
			timer.cancel();
		}
	}
}
