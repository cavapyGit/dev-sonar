package com.pradera.security.systemmgmt.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.model.System;
import com.pradera.security.model.UserAccount;
import com.pradera.security.model.type.SystemProfileStateType;
import com.pradera.security.model.type.SystemStateType;
import com.pradera.security.model.type.UserProfileStateType;
import com.pradera.security.systemmgmt.view.filters.ScheduleFilter;
import com.pradera.security.systemmgmt.view.filters.SystemFilter;
// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class SystemServiceBean
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class SystemServiceBean extends CrudSecurityServiceBean{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6866372397667725050L;

	/**
	 * Search system by state service bean.
	 *
	 * @param state the state
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<System> searchSystemByStateServiceBean(Integer state)
			throws ServiceException {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("state", state);
		return (List<System>) findWithNamedQuery(System.SYSTEM_SEARCH_BY_STATE,parameters);
	}
	
	
	/**
	 * Search system by id and state.
	 *
	 * @param systemIds the system ids
	 * @return the list
	 */
	public List<System> searchSystemByIdAndState(List<Integer> systemIds){
		String query = "select system from System system where system.state = :state and system.idSystemPk in :systemIdList order by system.name";
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("state", SystemStateType.REGISTERED.getCode());
		parameters.put("systemIdList", systemIds);
		List<System> lstSystem = findByQueryString(query,parameters);
		for (System system : lstSystem) {
			system.getSystemImage();
		}
		return lstSystem;
	}
	
	/**
	 * Search all system service bean.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<System> searchAllSystemServiceBean()
			throws ServiceException {
		return (List<System>) findWithNamedQuery(System.SYSTEM_SEARCH_ALL,new HashMap<String, Object>());
	}

	/**
	 * Gets the systems by filter service bean.
	 *
	 * @param sysFilter the sys filter
	 * @return the systems by filter service bean
	 * @throws ServiceException the service exception
	 */
	public List<System> getSystemsByFilterServiceBean(SystemFilter sysFilter)
			throws ServiceException {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<System> cq = cb.createQuery(System.class);
		Root<System> systemRoot = cq.from(System.class);
		cq.orderBy(cb.asc(systemRoot.get("mnemonic")) );
		systemRoot.fetch("schedule");

		List<Predicate> criteriaParameters = new ArrayList<Predicate>();

		cq.select(systemRoot);

		if (Validations.validateIsNotNull(sysFilter.getIdSystemPk())) {
			ParameterExpression<Integer> p = cb.parameter(Integer.class,
					"idSystemPkPrm");
			criteriaParameters.add(cb.equal(systemRoot.get("idSystemPk"), p));
		}
		if (Validations.validateIsNotNullAndNotEmpty(sysFilter.getSystemName())) {
			ParameterExpression<String> p = cb.parameter(String.class,
					"namePrm");
			criteriaParameters.add(cb.like(systemRoot.<String> get("name"),p));
		}
		if (Validations.validateIsNotNullAndNotEmpty(sysFilter
				.getSystemMnemonic())) {
			ParameterExpression<String> p = cb.parameter(String.class,
					"mnemonicPrm");
			criteriaParameters.add(cb.like(systemRoot.<String> get("mnemonic"),
					p));
		}
		if (Validations
				.validateIsNotNullAndNotEmpty(sysFilter.getSystemState())
				&& sysFilter.getSystemState().intValue() > -1) {
			ParameterExpression<Integer> p = cb.parameter(Integer.class,
					"statePrm");
			criteriaParameters.add(cb.equal(systemRoot.get("state"), p));
		}

		if (!criteriaParameters.isEmpty()) {
			if (criteriaParameters.size() == 1) {
				cq.where(criteriaParameters.get(0));
			} else {
				cq.where(cb.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()])));
			}
		}

		TypedQuery<System> systemCriteria = em.createQuery(cq);

		if (Validations.validateIsNotNull(sysFilter.getIdSystemPk())) {
			systemCriteria.setParameter("idSystemPkPrm",sysFilter.getIdSystemPk());
		}
		if (Validations.validateIsNotNullAndNotEmpty(sysFilter.getSystemName())) {
			systemCriteria.setParameter("namePrm", sysFilter.getSystemName().toUpperCase()+"%");
		}
		if (Validations.validateIsNotNullAndNotEmpty(sysFilter.getSystemMnemonic())) {
			systemCriteria.setParameter("mnemonicPrm", sysFilter.getSystemMnemonic().toUpperCase()+"%");
		}
		if (Validations.validateIsNotNullAndNotEmpty(sysFilter.getSystemState()) && sysFilter.getSystemState().intValue() > -1) {
			systemCriteria.setParameter("statePrm", sysFilter.getSystemState());
		}
		List<System> lstSystem = (List<System>) systemCriteria.getResultList();
		for (System system : lstSystem) {
			system.getSystemImage();
		}
		return lstSystem;
	}

	/**
	 * Find system by id service bean.
	 *
	 * @param id the id
	 * @return the system
	 * @throws ServiceException the service exception
	 */
	public System findSystemByIdServiceBean(Integer id) throws ServiceException {
		return find(System.class, id);
	}

	/**
	 * Validate system name service bean.
	 *
	 * @param system the system
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int validateSystemNameServiceBean(System system) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select count(v) from System v where v.name=:namePrm");

		if (system.getIdSystemPk() != null) {
			sbQuery.append(" and v.idSystemPk!=:idSystemPkPrm");
		}

		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("namePrm", system.getName());

		if (system.getIdSystemPk() != null) {
			query.setParameter("idSystemPkPrm", system.getIdSystemPk());
		}
		query.setHint("org.hibernate.cacheable", true);
		return Integer.parseInt(query.getSingleResult().toString());
	}

	/**
	 * Validate system mnemonic service bean.
	 *
	 * @param system the system
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int validateSystemMnemonicServiceBean(System system) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select count(v) from System v where v.mnemonic=:mnemonicPrm");
		if (system.getIdSystemPk() != null) {
			sbQuery.append(" and v.idSystemPk!=:idSystemPkPrm");
		}
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("mnemonicPrm", system.getMnemonic());

		if (system.getIdSystemPk() != null) {
			query.setParameter("idSystemPkPrm", system.getIdSystemPk());
		}
		query.setHint("org.hibernate.cacheable", true);
		return Integer.parseInt(query.getSingleResult().toString());
	}

	/**
	 * Update system state service bean.
	 *
	 * @param system the system
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateSystemStateServiceBean(System system , LoggerUser loggerUser)
			throws ServiceException {
		update(system, loggerUser);
		/*
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update System V ");
		sbQuery.append(" Set V.state = :statePrm,");
		sbQuery.append(" V.situation = :situationPrm, ");		
		sbQuery.append(" V.lastModifyDate = :lastModifyDatePrm,");
		sbQuery.append(" V.lastModifyUser = :lastModifyUserPrm,");
		sbQuery.append(" V.lastModifyIp = :lastModifyIpPrm,");
		sbQuery.append(" V.lastModifyPriv = :lastModifyPrivPrm ,");
		sbQuery.append(" V.blockMotive = :blockMotive, ");
		sbQuery.append(" V.unblockMotive = :unblockMotive, ");
		sbQuery.append(" V.rejectMotive = :rejectMotive, ");
		sbQuery.append(" V.blockOtherMotive = :blockOtherMotiveDes, ");
		sbQuery.append(" V.unblockOtherMotive = :unblockOtherMotiveDes, ");
		sbQuery.append(" V.rejectOtherMotive = :rejectOtherMotiveDes ");
		sbQuery.append(" Where V.idSystemPk = :idSystemPkPrm ");

		Query query = em.createQuery(sbQuery.toString());

		query.setParameter("idSystemPkPrm", system.getIdSystemPk());
		query.setParameter("statePrm", system.getState());
		query.setParameter("situationPrm", system.getSituation());
		query.setParameter("lastModifyDatePrm", loggerUser.getAuditTime());
		query.setParameter("lastModifyUserPrm",loggerUser.getUserName());
		query.setParameter("lastModifyIpPrm", loggerUser.getIpAddress());
		query.setParameter("lastModifyPrivPrm", loggerUser.getIdPrivilegeOfSystem());

		query.setParameter("blockMotive", system.getBlockMotive());
		query.setParameter("unblockMotive", system.getUnblockMotive());
		query.setParameter("rejectMotive", system.getRejectMotive());
		
		query.setParameter("blockOtherMotiveDes", system.getBlockOtherMotive());
		query.setParameter("unblockOtherMotiveDes", system.getUnblockOtherMotive());
		query.setParameter("rejectOtherMotiveDes", system.getRejectOtherMotive());

		query.executeUpdate();
		*/
		
	}

	/**
	 * List available systems by user service bean.
	 *
	 * @param user the user
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<System> listAvailableSystemsByUserServiceBean(UserAccount user) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select s from System s");
		sbQuery.append("  join s.systemProfile ps ");
		sbQuery.append("  join ps.userProfile pu ");
		sbQuery.append(" where  pu.user.idUserPk=:idUserPkPrm");
		sbQuery.append(" and  s.state=:stateSisPrm");
		sbQuery.append(" and  ps.state=:statePuPrm");
		sbQuery.append(" and  pu.state=:statePsPrm");
		sbQuery.append(" order by s.name");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idUserPkPrm", user.getIdUserPk());
		query.setParameter("stateSisPrm",
				SystemStateType.REGISTERED.getCode());
		query.setParameter("statePuPrm",
				UserProfileStateType.REGISTERED.getCode());
		query.setParameter("statePsPrm",
				SystemProfileStateType.REGISTERED.getCode());
		query.setHint("org.hibernate.cacheable", true);
		return (List<System>) query.getResultList();
	}
						 
	/**
	 * List availiable systems by schedule service bean.
	 *
	 * @param horarioFiltro the horario filtro
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<System> listAvailiableSystemsByScheduleServiceBean(ScheduleFilter horarioFiltro) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select s from System s ");
		sbQuery.append(" join s.systemProfiles ps ");
		sbQuery.append(" join ps.userProfiles pu ");
		sbQuery.append(" join s.schedule ho ");
		sbQuery.append(" join ho.scheduleDetails hod ");
		sbQuery.append(" Where  pu.userAccount.idUserPk=:idUserPkPrm ");
		sbQuery.append(" and  s.state=:stateSisPrm ");
		sbQuery.append(" and  ps.state=:statePsPrm ");
		sbQuery.append(" and  pu.state=:statePuPrm ");
		sbQuery.append(" And hod.state=:stateScheDetPrm  ");
		sbQuery.append(" And hod.day = :dayPrm And :timePrm Between hod.initialDate And hod.endDate ");
		sbQuery.append(" And ho.state = :stateSchePrm And ho.scheduleType = :scheduleTypePrm ");
		sbQuery.append(" order by s.name");

		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idUserPkPrm", horarioFiltro.getUserAccountFilter().getIdUserAccountPK());
		query.setParameter("stateSisPrm",SystemStateType.CONFIRMED.getCode());
		query.setParameter("statePuPrm",UserProfileStateType.REGISTERED.getCode());
		query.setParameter("statePsPrm",SystemProfileStateType.CONFIRMED.getCode());
		query.setParameter("stateScheDetPrm", horarioFiltro.getScheduleDetailFilter().getScheduleDetailState());
		query.setParameter("dayPrm", horarioFiltro.getScheduleDetailFilter().getDay());
		query.setParameter("timePrm", horarioFiltro.getScheduleDetailFilter().getHour(), TemporalType.TIME);
		query.setParameter("stateSchePrm", horarioFiltro.getScheduleState());
		query.setParameter("scheduleTypePrm", horarioFiltro.getScheduleType());
		query.setHint("org.hibernate.cacheable", true);
		List<System> lstResult = query.getResultList();
		List<System> lstSystemsAux = new ArrayList<System>();
		if(lstResult != null && !lstResult.isEmpty()){
		for (System system : lstResult) {
			if(!validateSystemInList(lstSystemsAux,system)){
				system.getSystemImage();
				lstSystemsAux.add(system);
			}
		}		
		}
		return lstSystemsAux;
	}
	
	/**
	 * Validate system in list.
	 *
	 * @param lstSystems the lst systems
	 * @param system the system
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	private boolean validateSystemInList(List<System> lstSystems, System system)throws ServiceException{
		for (System objSystemAux : lstSystems) {
			if(system.getIdSystemPk().equals(objSystemAux.getIdSystemPk())){
				return true;
			}
		}
		return false;
	}
	

	/**
	 * Gets the system with mnemonic.
	 *
	 * @param mnemonic the mnemonic
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<?> getSystemWithMnemonic(String mnemonic) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select s.idSystemPk from System s ");
		sbQuery.append(" Where s.mnemonic = :sysmnemonic and state != :statePrm");		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("sysmnemonic", mnemonic);	
		query.setParameter("statePrm", SystemStateType.REJECTED.getCode());
		query.setHint("org.hibernate.cacheable", true);
		return (List<?>)query.getResultList();
		
	}
	
	/**
	 * Gets the system with name.
	 *
	 * @param name the name
	 * @param idSystemPk the id system pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<?> getSystemWithName(String name,Integer idSystemPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select s.idSystemPk from System s ");
		sbQuery.append(" Where s.name = :name");
		if (idSystemPk != null) {
			sbQuery.append(" and s.idSystemPk!=:idSystemPkPrm");
		}
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("name", name);
		if (idSystemPk != null) {
			query.setParameter("idSystemPkPrm", idSystemPk);
		}
		query.setHint("org.hibernate.cacheable", true);
		return (List<?>)query.getResultList();
		
	}
	
	/**
	 * Update system state service bean.
	 *
	 * @param system the system
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateSystemServiceBean(System system,LoggerUser loggerUser)
			throws ServiceException {
		update(system, loggerUser);
		/*
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update System V ");
		sbQuery.append(" Set V.state = :statePrm,");
		sbQuery.append(" V.description = :descriptionPrm,");
		sbQuery.append(" V.name = :namePrm,");
		sbQuery.append(" V.systemImage = :systemImagePrm,");
		sbQuery.append(" V.systemProductionFile = :systemProductionFilePrm,");
		sbQuery.append(" V.systemUrl = :systemUrlPrm,");
		sbQuery.append(" V.schedule.idSchedulePk = :idSchedulePkPrm,");
		sbQuery.append(" V.schemaDb = :schemaDbPrm,");
		sbQuery.append(" V.lastModifyDate = :lastModifyDatePrm,");
		sbQuery.append(" V.lastModifyUser = :lastModifyUserPrm,");
		sbQuery.append(" V.lastModifyIp = :lastModifyIpPrm,");
		sbQuery.append(" V.lastModifyPriv = :lastModifyPrivPrm ");
		sbQuery.append(" Where V.idSystemPk = :idSystemPkPrm ");

		Query query = em.createQuery(sbQuery.toString());

		query.setParameter("idSystemPkPrm", system.getIdSystemPk());
		query.setParameter("statePrm", system.getState());
		query.setParameter("descriptionPrm", system.getDescription());
		query.setParameter("namePrm", system.getName());
		query.setParameter("systemImagePrm", system.getSystemImage());
		query.setParameter("systemProductionFilePrm", system.getSystemProductionFile());
		query.setParameter("systemUrlPrm", system.getSystemUrl());
		query.setParameter("schemaDbPrm", system.getSchemaDb());
		query.setParameter("idSchedulePkPrm", system.getSchedule().getIdSchedulePk());
		query.setParameter("lastModifyDatePrm", loggerUser.getAuditTime());
		query.setParameter("lastModifyUserPrm", loggerUser.getUserName());
		query.setParameter("lastModifyIpPrm", loggerUser.getIpAddress());
		query.setParameter("lastModifyPrivPrm", loggerUser.getUserAction().getIdPrivilegeModify());		
		query.executeUpdate();
		*/
	}
	
	/**
	 * Gets the images systems.
	 *
	 * @param systems the systems
	 * @return the images systems
	 * @throws ServiceException the service exception
	 */
	public List<System> getImagesSystems(List<System> systems) throws ServiceException {
		List<System> systemsImages = new ArrayList<System>();
		for (System system : systems) {
			system = em.merge(system);
			system.getSystemImage();
			systemsImages.add(system);
		}
		return systemsImages;
	}
	
	/**
	 * Gets the lastest sysem details.
	 *
	 * @param idSystemPk the id system pk
	 * @return the lastest sysem details
	 */
	public System getLastestSysemDetails(Integer idSystemPk){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select s from System s ");
		sbQuery.append(" Where s.idSystemPk=:idSystemPkPrm");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSystemPkPrm", idSystemPk);
		query.setHint("org.hibernate.cacheable", true);
		return (System)query.getSingleResult();
	}
	
	/**
	 * Gets the system production file.
	 *
	 * @param idSystemPk the id system pk
	 * @return the system production file
	 */
	public byte[] getSystemProductionFile(Integer idSystemPk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select S.systemProductionFile from System S where S.idSystemPk = :idSystemPkPrm ");	
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSystemPkPrm", idSystemPk);
		return  (byte[])query.getSingleResult(); 
	}
	
	/**
	 * Gets the system image.
	 *
	 * @param idSystemPk the id system pk
	 * @return the system image
	 */
	public byte[] getSystemImage(Integer idSystemPk){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select S.systemImage from System S where S.idSystemPk = :idSystemPkPrm ");	
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSystemPkPrm", idSystemPk);
		return  (byte[])query.getSingleResult(); 
	}
	
	/**
	 * Gets the system list.
	 * using in iDepositary monitoring business process
	 * @return the system list
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getSystemList() throws ServiceException {
		StringBuilder builder=new StringBuilder();
		  builder.append(" select s.id_system_pk, s.name ");
		  builder.append(" from system s ");
		  builder.append(" where ");
		  builder.append(" s.state = :statePrm ");
		  Query query = em.createNativeQuery(builder.toString());		  
		  query.setParameter("statePrm", SystemStateType.CONFIRMED.getCode());
		  return (List<Object[]>) query.getResultList();
	}
}
