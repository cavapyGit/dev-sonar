package com.pradera.security.systemmgmt.view.filters;

import java.io.Serializable;

import com.pradera.commons.services.remote.security.to.InstitutionTO;
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class InstitutionSecurityFilter
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.1
 */
public class InstitutionSecurityFilter implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idState;
	private String description;
	private Integer idScheduleState;
	private Integer idInstitutionSecurityPK;
	private Integer idInstitutionTypeFk;
	private String mnemonic;
	private String issuerCode;
	private Long participantCode;
	
	/**
	 * @return the idState
	 */
	public Integer getIdState() {
		return idState;
	}

	/**
	 * @param idState the idState to set
	 */
	public void setIdState(Integer idState) {
		this.idState = idState;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the idScheduleState
	 */
	public Integer getIdScheduleState() {
		return idScheduleState;
	}

	/**
	 * @param idScheduleState the idScheduleState to set
	 */
	public void setIdScheduleState(Integer idScheduleState) {
		this.idScheduleState = idScheduleState;
	}

	/**
	 * @return the idInstitutionSecurityPK
	 */
	public Integer getIdInstitutionSecurityPK() {
		return idInstitutionSecurityPK;
	}

	/**
	 * @param idInstitutionSecurityPK the idInstitutionSecurityPK to set
	 */
	public void setIdInstitutionSecurityPK(Integer idInstitutionSecurityPK) {
		this.idInstitutionSecurityPK = idInstitutionSecurityPK;
	}

	/**
	 * @return the idInstitutionTypeFk
	 */
	public Integer getIdInstitutionTypeFk() {
		return idInstitutionTypeFk;
	}

	/**
	 * @param idInstitutionTypeFk the idInstitutionTypeFk to set
	 */
	public void setIdInstitutionTypeFk(Integer idInstitutionTypeFk) {
		this.idInstitutionTypeFk = idInstitutionTypeFk;
	}

	public String getMnemonic() {
		return mnemonic;
	}

	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	public String getIssuerCode() {
		return issuerCode;
	}

	public void setIssuerCode(String issuerCode) {
		this.issuerCode = issuerCode;
	}


	public Long getParticipantCode() {
		return participantCode;
	}

	public void setParticipantCode(Long participantCode) {
		this.participantCode = participantCode;
	}

	/**
	 * Constructor
	 */
	public InstitutionSecurityFilter() {		
	}
	
	public InstitutionSecurityFilter(InstitutionTO to) {
		description = to.getName();
		idInstitutionTypeFk = to.getType();
		mnemonic = to.getMnemonic();
		issuerCode = to.getIssuerCode();
		participantCode = to.getParticipantCode();
	}
}
