package com.pradera.security.systemmgmt.view.filters;

import java.io.Serializable;

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class PrivilegeOptionFilter
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.1
 */
public class PrivilegeOptionFilter implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6509733311059672538L;

    private Integer idSystem;
	
	private Integer idUser;
	
	/**
	 * @return the idSystem
	 */
	public Integer getIdSystem() {
		return idSystem;
	}

	/**
	 * @param idSystem the idSystem to set
	 */
	public void setIdSystem(Integer idSystem) {
		this.idSystem = idSystem;
	}

	/**
	 * @return the idUser
	 */
	public Integer getIdUser() {
		return idUser;
	}

	/**
	 * @param idUser the idUser to set
	 */
	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	/**
	 * Constructor
	 */
	public PrivilegeOptionFilter() {
		// TODO Auto-generated constructor stub
	}


	
}
