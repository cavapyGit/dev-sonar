package com.pradera.security.systemmgmt.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.model.ExceptionUserPrivilege;
import com.pradera.security.model.OptionPrivilege;
import com.pradera.security.model.Parameter;
import com.pradera.security.model.ProfilePrivilege;
import com.pradera.security.model.SystemOption;
import com.pradera.security.model.UserAccount;
import com.pradera.security.model.UserPrivilegeDetail;
import com.pradera.security.model.type.OptionPrivilegeStateType;
import com.pradera.security.model.type.OptionStateType;
import com.pradera.security.model.type.ProfilePrivilegeStateType;
import com.pradera.security.model.type.UserExceptionStateType;
import com.pradera.security.model.type.UserProfileStateType;
import com.pradera.security.systemmgmt.view.filters.PrivilegeOptionFilter;
import com.pradera.security.systemmgmt.view.filters.UserExceptionFilter;

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class UserExceptionServiceBean
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class UserExceptionServiceBean extends CrudSecurityServiceBean {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5031947949890375579L;

	/**
	 * Instantiates a new user exception service bean.
	 */
	public UserExceptionServiceBean() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * To get the list of parameters.
	 *
	 * @param idDefinition the id definition
	 * @return list
	 * @throws ServiceException the service exception
	 */
	 @SuppressWarnings("unchecked")
	public List<Parameter> listByDefinitionServiceBean(Integer idDefinition) throws ServiceException{
		    Query query = em.createQuery("Select o From Parameter o WHERE o.definition.idDefinitionPk = :idDefinitionPrm");
			query.setParameter("idDefinitionPrm", idDefinition);	
			query.setHint("org.hibernate.cacheable", true);
			return (List<Parameter>)query.getResultList();
	 }
	 
	 public Parameter getParameter(Integer idParameterPk) throws ServiceException{
		    Query query = em.createQuery("Select o From Parameter o WHERE o.idParameterPk = :idParameterPk");
			query.setParameter("idParameterPk", idParameterPk);	
			query.setHint("org.hibernate.cacheable", true);
			return (Parameter)query.getSingleResult();
	 }
	 
 	/**
 	 * List system options by language service bean.
 	 *
 	 * @param userExceptionFilter the user exception filter
 	 * @return the list
 	 * @throws ServiceException the service exception
 	 */
 	public List<Object[]> listSystemOptionsByLanguageServiceBean(UserExceptionFilter userExceptionFilter) throws ServiceException {
		    StringBuilder sbQuery = new StringBuilder();
		    sbQuery.append(" Select SO.idSystemOptionPk, SO.parentSystemOption.idSystemOptionPk, CL.catalogueName ");
			sbQuery.append(" From SystemOption SO");
			sbQuery.append(" Join SO.catalogueLanguages CL");		
			sbQuery.append(" Where CL.language = :languagePrm ");
			sbQuery.append(" And SO.state = :systemOptionStatePrm AND SO.system.idSystemPk = :idSystemPrm");		
			sbQuery.append(" Order By SO.parentSystemOption.idSystemOptionPk, SO.orderOption ");
			
			Query query = em.createQuery(sbQuery.toString());
				
			query.setParameter("languagePrm",userExceptionFilter.getUserExceptionLanguage());
			query.setParameter("systemOptionStatePrm",userExceptionFilter.getUserExceptionOptionState());
			query.setParameter("idSystemPrm", userExceptionFilter.getIdSystemSelected());
			query.setHint("org.hibernate.cacheable", true);
			List<Object[]> listaOpcionces = (List<Object[]>) query.getResultList();
		
			
		    	return listaOpcionces;	    
	 }
	 
	 /**
 	 * List system options by system service bean.
 	 *
 	 * @param excepcionUsuarioFiltro the excepcion usuario filtro
 	 * @return the list
 	 * @throws ServiceException the service exception
 	 */
 	public List<Object[]> listSystemOptionsBySystemServiceBean(UserExceptionFilter excepcionUsuarioFiltro) throws ServiceException {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" Select PO.idOptionPrivilegePk, PO.idPrivilege, OS.idSystemOptionPk ");		
			sbQuery.append(" From SystemOption OS ");
			sbQuery.append(" Join OS.optionPrivileges PO ");		
			sbQuery.append(" Where PO.state = :statePrivilegePrm And  OS.state = :stateOptionPrm ");
			sbQuery.append(" AND OS.system.idSystemPk = :idSystemPrm");	
			sbQuery.append(" Order By OS.idSystemOptionPk, PO.idPrivilege ");			
			Query query = em.createQuery(sbQuery.toString());			
			query.setParameter("statePrivilegePrm",excepcionUsuarioFiltro.getUserExceptionPrivelegeState());
			query.setParameter("stateOptionPrm",excepcionUsuarioFiltro.getUserExceptionOptionState());
			query.setParameter("idSystemPrm", excepcionUsuarioFiltro.getIdSystemSelected());
			query.setHint("org.hibernate.cacheable", true);
			List<Object[]> listaOpcionesPrivilegios = (List<Object[]>) query.getResultList();		
		    return listaOpcionesPrivilegios;
		    
		}
	 
	 /**
 	 * To get list of user account.
 	 *
 	 * @param fullName the full name
 	 * @return list
 	 * @throws ServiceException the service exception
 	 */
	 @SuppressWarnings("unchecked")
	public List<UserAccount> serchUsersByNameServiceBean(String fullName) throws ServiceException{
		 Query query = em.createQuery("Select u From UserAccount u join fetch u.institutionsSecurity  Where u.fullName like :fullName order by u.loginUser");
		 query.setParameter("fullName", "%"+fullName+"%");	
		 query.setHint("org.hibernate.cacheable", true);
		 return (List<UserAccount>)query.getResultList();
	 }
	 
	 /**
 	 * Search user by user login service bean.
 	 *
 	 * @param userLogin the user login
 	 * @return the user account
 	 * @throws ServiceException the service exception
 	 */
 	public UserAccount searchUserByUserLoginServiceBean(String userLogin) throws ServiceException{
		 Query query = em.createQuery("Select u From UserAccount u Where u.loginUser = :userLogin");
		       query.setParameter("userLogin", userLogin);	
		       UserAccount user = null;
		try{
			query.setHint("org.hibernate.cacheable", true);
		user = (UserAccount)query.getSingleResult();
		}catch (NoResultException e) {
			user = null;
		}
		
		return user;
		
	 }
	 
	 /**
 	 * Search user exceptions by filters service bean.
 	 *
 	 * @param userExceptionFilter the user exception filter
 	 * @return the list
 	 * @throws ServiceException the service exception
 	 */
 	public List<ExceptionUserPrivilege> searchUserExceptionsByFiltersServiceBean(UserExceptionFilter userExceptionFilter) throws ServiceException{
		 
		 StringBuilder sbQuery = new StringBuilder();
		
		Integer flagOne=0;
		Integer flagTwo=0;
		Integer flagThree=0;
		 
		 sbQuery.append(" Select EPU From ExceptionUserPrivilege EPU Join fetch EPU.userAccount Join fetch EPU.system");		
		
		 sbQuery.append(" where EPU.state = :statePrm");
		 if(Validations.validateIsNotNullAndNotEmpty(userExceptionFilter.getInitialDate()) 
				 && Validations.validateIsNotNullAndNotEmpty(userExceptionFilter.getFinalDate()) ){
			 sbQuery.append(" and EPU.initialDate between :initialDatePrm And :endDatePrm ");
		 }
		
			
			 if(userExceptionFilter.getUserExceptionSelectedType()!= null && !(userExceptionFilter.getUserExceptionSelectedType().equals(-1))){
				 flagOne=1;
				 sbQuery.append(" And EPU.userAccount.type = :userTypePrm"); 
			 }
			 
			 if(userExceptionFilter.getInstitutionSelected()!= null && !(userExceptionFilter.getInstitutionSelected().equals(-1))){
				 flagTwo=2;
				 sbQuery.append(" And EPU.userAccount.institutionsSecurity.idInstitutionSecurityPk = :institutionPrm"); 
			 }
			 
			 if(userExceptionFilter.getUserExceptionCodeUser()!= null){
				 flagThree=3;
				 sbQuery.append(" And EPU.userAccount.idUserPk = :userCodePrm"); 
			 }
			 sbQuery.append(" order by EPU.userAccount.loginUser"); 
		 	 
	     Query query = em.createQuery(sbQuery.toString());
		
	     if(Validations.validateIsNotNullAndNotEmpty(userExceptionFilter.getInitialDate()) 
				 && Validations.validateIsNotNullAndNotEmpty(userExceptionFilter.getFinalDate()) ){	
	    	 query.setParameter("initialDatePrm",userExceptionFilter.getInitialDate());	
	    	 query.setParameter("endDatePrm",CommonsUtilities.addDate(userExceptionFilter.getFinalDate(), 1) );
	     }
		 query.setParameter("statePrm", userExceptionFilter.getUserExceptionStateSelected());
		 
		 
		 if(flagOne.equals(1)){
			 query.setParameter("userTypePrm",userExceptionFilter.getUserExceptionSelectedType());			
		 }
			
		 if(flagTwo.equals(2)){
		 query.setParameter("institutionPrm", userExceptionFilter.getInstitutionSelected());	
		 }
		 
		 if(flagThree.equals(3)){
		 query.setParameter("userCodePrm", userExceptionFilter.getUserExceptionCodeUser());	
		 }
		 //query.setHint("org.hibernate.cacheable", true);
			return (List<ExceptionUserPrivilege>)query.getResultList();
	 }
	 
	 
	 /**
 	 * Modify state user exception by user service bean.
 	 *
 	 * @param list the list
 	 * @param state the state
 	 * @param deleteMotive the delete motive
 	 * @param blockMotive the block motive
 	 * @param unBlockmotivo the un blockmotivo
 	 * @param motiveDescArr the motive desc arr
 	 * @param loggerUser the logger user
 	 * @throws ServiceException the service exception
 	 */
 	public void modifyStateUserExceptionByUserServiceBean(List<Integer> list,Integer state,Integer situation,Integer blockMotive,
			 Integer unBlockmotivo,String[] motiveDescArr, LoggerUser loggerUser) throws ServiceException{
		 Query query = em.createQuery("Update ExceptionUserPrivilege set state = :state ,blockMotive = :blockMotive ,unblockMotive = :unblockMotive ,lastModifyDate = :lastModifyDate, lastModifyIp = :lastModifyIp, lastModifyPriv = :lastModifyPriv, lastModifyUser = :lastModifyUser, " +
		 		                      " blockOtherMotive = :blockOtherMotivePrm , unblockOtherMotive = :unblockOtherMotivePrm, situation = :situation Where idExceptionUserPrivilegePk in (:list) ");
		
		 query.setParameter("state", state);
		 query.setParameter("blockMotive", blockMotive);
		 query.setParameter("unblockMotive", unBlockmotivo);
		 query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		 query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		 if(UserExceptionStateType.DELETED.getCode().equals(state)){
			 query.setParameter("lastModifyPriv", loggerUser.getUserAction().getIdPrivilegeDelete());
		 }else if(UserExceptionStateType.REJECT.getCode().equals(state)){
			 query.setParameter("lastModifyPriv", loggerUser.getUserAction().getIdPrivilegeReject());
		 }else if(UserExceptionStateType.REGISTERED.getCode().equals(state)){
			 query.setParameter("lastModifyPriv", loggerUser.getUserAction().getIdPrivilegeUnblock());
		 } else if(UserExceptionStateType.CONFIRMED.getCode().equals(state)){
			 query.setParameter("lastModifyPriv", loggerUser.getUserAction().getIdPrivilegeDelete());
		 }
		 query.setParameter("lastModifyUser", loggerUser.getUserName());
		 query.setParameter("blockOtherMotivePrm", motiveDescArr[1]);
		 query.setParameter("unblockOtherMotivePrm", motiveDescArr[2]);
		 query.setParameter("situation", situation);
		 query.setParameter("list", list);
		 
		 query.executeUpdate();
		
	 }
	 
	 /**
 	 * Update state sistuation exception privilege.
 	 *
 	 * @param exceptions the exceptions
 	 * @param state the state
 	 * @param situation the situation
 	 * @param loggerUser the logger user
 	 * @throws ServiceException the service exception
 	 */
 	public void updateStateSistuationExceptionPrivilege(List<ExceptionUserPrivilege> exceptions, Integer state, Integer situation,LoggerUser loggerUser) throws ServiceException{
		 List<Integer> idExceptions = new ArrayList<>();
		 for(ExceptionUserPrivilege exception : exceptions){
			 idExceptions.add(exception.getIdExceptionUserPrivilegePk());
		 }
		 Query query =em.createQuery(" update ExceptionUserPrivilege set state = :state,situation= :situation "
		 		+ " ,lastModifyDate = :lastModifyDate, lastModifyIp = :lastModifyIp, lastModifyPriv = :lastModifyPriv, lastModifyUser = :lastModifyUser "
		 		+ " where  idExceptionUserPrivilegePk in (:list)  ");
		 query.setParameter("state", state);
		 query.setParameter("situation", situation);
		 query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		 query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		 query.setParameter("lastModifyPriv", loggerUser.getIdPrivilegeOfSystem());
		 query.setParameter("lastModifyUser", loggerUser.getUserName());
		 query.setParameter("list", idExceptions);
		 query.executeUpdate();
	 }
	 
	/**
	 * To get the list of user privilege dettails .
	 *
	 * @param idExceptionUserPrivilege the id exception user privilege
	 * @return list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<UserPrivilegeDetail> searchPrivilegesDetailsByExceptionServiceBean(Integer idExceptionUserPrivilege) throws ServiceException{
		 		 
		 Query query = em.createQuery("Select DPU From UserPrivilegeDetail DPU  Where DPU.exceptionUserPrivilege.idExceptionUserPrivilegePk = :idExceptionUserPrivilege");
		 query.setParameter("idExceptionUserPrivilege", idExceptionUserPrivilege);
		 query.setHint("org.hibernate.cacheable", true);
		 return  (List<UserPrivilegeDetail>)query.getResultList();
			
	 }
	 
	 /**
 	 * Search option privileges exception service bean.
 	 *
 	 * @param userExceptionDetalle the user exception detalle
 	 * @return the list
 	 * @throws ServiceException the service exception
 	 */
 	public List<Object[]> searchOptionPrivilegesExceptionServiceBean(UserExceptionFilter userExceptionDetalle) throws ServiceException {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" Select distinct PO.idOptionPrivilegePk, PO.idPrivilege, OS.idSystemOptionPk ");		
			sbQuery.append(" From SystemOption OS ");
			sbQuery.append(" Join OS.optionPrivileges PO ");
			sbQuery.append(" Join PO.userPrivilegeDetails DP ");
			sbQuery.append(" Where OS.system.idSystemPk = :idSystemPrm");			
			sbQuery.append(" And DP.exceptionUserPrivilege.idExceptionUserPrivilegePk = :idExceptionPrivilegePrm");	
			sbQuery.append(" Order By OS.idSystemOptionPk, PO.idPrivilege ");
			Query query = em.createQuery(sbQuery.toString());

			query.setParameter("idSystemPrm", userExceptionDetalle.getIdSystemSelected());
			query.setParameter("idExceptionPrivilegePrm", userExceptionDetalle.getIdPrivelegeException());
			query.setHint("org.hibernate.cacheable", true);
			List<Object[]> listExceptionUserPrivilege = (List<Object[]>) query.getResultList();

		    return listExceptionUserPrivilege;	    
		}
	 
	 /**
 	 * List option privileges availables service bean.
 	 *
 	 * @param opcionPrivilegioFiltro the opcion privilegio filtro
 	 * @return the list
 	 * @throws ServiceException the service exception
 	 */
 	public List<SystemOption> listOptionPrivilegesAvailablesServiceBean(PrivilegeOptionFilter opcionPrivilegioFiltro) throws ServiceException {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" Select OS");		
			sbQuery.append(" From SystemOption OS ");
			sbQuery.append(" Join OS.catalogueLanguages CI ");
			sbQuery.append(" Join OS.optionPrivileges PO ");
			sbQuery.append(" Join PO.userPrivilegesDetails DP ");
			sbQuery.append(" Where PO.state = :statePrivilegePrm And  OS.state = :stateOptionPrm ");
			sbQuery.append(" And OS.system.idSystemPk = :idSystemPrm ");
			sbQuery.append(" And DP.exceptionUserPrivilege. = :idUserPrm ");	
			sbQuery.append(" And :datePrm between (DP.exceptionUserPrivilege.initialDate and DP.exceptionUserPrivilege.endDate) ");	
			sbQuery.append(" Order By OS.idSystemOptionPk, PO.idPrivilege ");

			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idSystemPrm", opcionPrivilegioFiltro.getIdSystem());
			query.setParameter("idUserPrm", opcionPrivilegioFiltro.getIdUser());
			query.setParameter("stateOptionPrm",OptionStateType.CONFIRMED.getCode());
			query.setParameter("statePrivilegePrm",OptionPrivilegeStateType.REGISTERED);
            query.setParameter("datePrm",new Date());
            query.setHint("org.hibernate.cacheable", true);
			List<SystemOption> listExceptionUserPrivilege = (List<SystemOption>) query.getResultList();
		    return listExceptionUserPrivilege;	    
		}
	 
	 	/**
	 	 * delete privilege user exception service facade.
	 	 *
	 	 * @param lstPrivilages the lst privilages
	 	 * @return true, if successful
	 	 * @throws ServiceException the service exception
	 	 */
	    public boolean deletePrivilegeUserExceptionServiceBean(List<Integer> lstPrivilages) throws ServiceException {
			if (Validations.validateIsNotNull(lstPrivilages)) {

				StringBuilder sb = new StringBuilder();
				sb.append("delete from UserPrivilegeDetail upd where upd.optionPrivilege.idOptionPrivilegePk in :lstPrivileges");
				
				Query q = em.createQuery(sb.toString());
				q.setParameter("lstPrivileges", lstPrivilages);
				q.executeUpdate();
				/*
				for (Integer i : lstPrivilages) {
					Query deleteQuery = this.em.createQuery(
							"delete from UserPrivilegeDetail upd where upd.optionPrivilege.idOptionPrivilegePk = ?");
					deleteQuery.setParameter(1, i);
					deleteQuery.executeUpdate();
				}
				*/
			}
	    	return true;
	    }

		/**
		 * To perform validation whether the Exception is having same name		 .
		 *
		 * @param userExceptionName the user exception name
		 * @return long
		 * @throws ServiceException the service exception
		 */
		public Long validateUserExceptionServiceBean(String userExceptionName)throws ServiceException {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" Select count(*) From ExceptionUserPrivilege EPU where EPU.name = :name");	
			Query query = em.createQuery(sbQuery.toString());
		//	query.setParameter("usuLogin", usuLogin);
			query.setParameter("name", userExceptionName);
			query.setHint("org.hibernate.cacheable", true);
			return (Long) query.getSingleResult();
			
		}
		
		/**
		 * Load user priviledges service bean.
		 *
		 * @param userExceptionCodeUser the user exception code user
		 * @param systemId the system id
		 * @return list ProfilePrivilege
		 * @throws ServiceException the service exception
		 */
		public List<ProfilePrivilege> loadUserPriviledgesServiceBean(
				Integer userExceptionCodeUser,Integer systemId) throws ServiceException {
			
			try{
				StringBuilder sbQuery = new StringBuilder();
				sbQuery.append(" Select PP ");
				sbQuery.append(" From ProfilePrivilege pp,SystemOption so,UserProfile up");
				sbQuery.append(" left join fetch pp.optionPrivilege op");
				sbQuery.append(" where so.idSystemOptionPk = op.systemOption.idSystemOptionPk ");
				sbQuery.append(" and op.idOptionPrivilegePk = pp.optionPrivilege.idOptionPrivilegePk ");
				sbQuery.append(" and up.systemProfile.idSystemProfilePk = pp.systemProfile.idSystemProfilePk ");
				sbQuery.append(" and up.userAccount.idUserPk = :iduserpk ");
				sbQuery.append(" and so.system.idSystemPk = :idSystemPrm");
				sbQuery.append(" And op.state = :optionPrivilegeStatePrm And  so.state = :systemOptionStatePrm ");
				sbQuery.append(" And pp.state = :profilePrivilegeStatePrm ");
				sbQuery.append(" And up.state = :userProfileStatePrm ");
				Query query = em.createQuery(sbQuery.toString());
				query.setParameter("optionPrivilegeStatePrm",OptionPrivilegeStateType.REGISTERED.getCode());
				query.setParameter("systemOptionStatePrm",OptionStateType.CONFIRMED.getCode());
				query.setParameter("profilePrivilegeStatePrm",ProfilePrivilegeStateType.REGISTERED.getCode());
				query.setParameter("idSystemPrm", systemId);
				query.setParameter("userProfileStatePrm", UserProfileStateType.REGISTERED.getCode());
				query.setParameter("iduserpk", userExceptionCodeUser);
				query.setHint("org.hibernate.cacheable", true);
				return  query.getResultList();
				
			}catch (NoResultException nre) {
				return new ArrayList<ProfilePrivilege>();
			}
		}

		/**
		 * Search user exception service bean.
		 *
		 * @param userException the user exception
		 * @return list ExceptionUserPrivilege
		 * @throws ServiceException the service exception
		 */
		public List<ExceptionUserPrivilege> searchUserExceptionServiceBean(
				UserExceptionFilter userException)throws ServiceException {
			try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" Select EUP From ExceptionUserPrivilege EUP where EUP.state not in (:state) ");
			if(Validations.validateIsNotNullAndPositive(userException.getUserExceptionCodeUser())){
			sbQuery.append(" and EUP.userAccount.idUserPk = :idUserPk");
			}
			if(Validations.validateIsNotNullAndPositive(userException.getIdSystemSelected())){
				sbQuery.append(" and EUP.system.idSystemPk = :idSystemPk");
			}
			if(Validations.validateIsNotNullAndNotEmpty(userException.getInitialDate())){
				sbQuery.append(" and (( :InitialDate >= EUP.initialDate and :InitialDate <=  EUP.endDate )");
			}
			if(Validations.validateIsNotNullAndNotEmpty(userException.getFinalDate())){
				sbQuery.append(" or ( :FinalDate >= EUP.initialDate and :FinalDate <= EUP.endDate ))");
			}
			if(Validations.validateIsNotNullAndPositive(userException.getIdPrivelegeException())){
				sbQuery.append(" and EUP.idExceptionUserPrivilegePk != :idExceptionPriviledge");
			}
			Query query = em.createQuery(sbQuery.toString());
			List<Integer> statesFilters= new ArrayList<>();
			statesFilters.add(UserExceptionStateType.DELETED.getCode());
			statesFilters.add(UserExceptionStateType.ANNULLED.getCode());
			statesFilters.add(UserExceptionStateType.REJECT.getCode());
			query.setParameter("state",statesFilters);
			if(Validations.validateIsNotNullAndPositive(userException.getUserExceptionCodeUser())){
			query.setParameter("idUserPk",userException.getUserExceptionCodeUser());
			}
			if(Validations.validateIsNotNullAndPositive(userException.getIdSystemSelected())){
				query.setParameter("idSystemPk",userException.getIdSystemSelected());
			}
			if(Validations.validateIsNotNullAndNotEmpty(userException.getInitialDate())){
				query.setParameter("InitialDate",userException.getInitialDate());
			}
			if(Validations.validateIsNotNullAndNotEmpty(userException.getFinalDate())){
				query.setParameter("FinalDate",userException.getFinalDate());
			}
			if(Validations.validateIsNotNullAndPositive(userException.getIdPrivelegeException())){
				query.setParameter("idExceptionPriviledge",userException.getIdPrivelegeException());
			}
			query.setHint("org.hibernate.cacheable", true);
			List<ExceptionUserPrivilege> listExceptions =  query.getResultList();
			
			if(listExceptions.size()>0){
			listExceptions.get(0).getSystem().getName();
			}
			
			return listExceptions;
			
				}catch (NoResultException nre) {
					return new ArrayList<ExceptionUserPrivilege>();
				}
		}

		/**
		 * List user exception privilege service bean.
		 * only look confirmed state
		 * @param idUserAccountPk the id user account pk
		 * @return list ExceptionUserPrivilege
		 * @throws ServiceException the service exception
		 */
		public List<ExceptionUserPrivilege> listUserExceptionPrivilegeServiceBean(
				Integer idUserAccountPk)throws ServiceException {
			try{
				StringBuilder sbQuery = new StringBuilder();
				sbQuery.append(" Select EUP From ExceptionUserPrivilege EUP where EUP.state = :state ");
				sbQuery.append(" and EUP.userAccount.idUserPk = :userId ");
				sbQuery.append(" and :currentTime between EUP.initialDate and EUP.endDate");
				Query query = em.createQuery(sbQuery.toString());
				query.setParameter("state", UserExceptionStateType.CONFIRMED.getCode());
				query.setParameter("userId",idUserAccountPk);
				query.setParameter("currentTime",CommonsUtilities.currentDateTime());
				query.setHint("org.hibernate.cacheable", true);
				List<ExceptionUserPrivilege> listExceptions =  query.getResultList();
				if(listExceptions.size()>0){
					for(ExceptionUserPrivilege eup : listExceptions){
						eup.getSystem().getIdSystemPk();
					}
					}
					return listExceptions;
			}catch (NoResultException nre) {
				return new ArrayList<ExceptionUserPrivilege>();
			}
		}

		/**
		 * To get the list of UserException OptionPrivileges.
		 *
		 * @param idUserAccountPK the id user account pk
		 * @return OptionPrivilege list
		 * @throws ServiceException the service exception
		 */
		@Performance
		public List<OptionPrivilege> listAvailablePrivilegesByUserExceptionServiceBean(
				Integer idUserAccountPK)throws ServiceException {
			try{
				StringBuilder sbQuery = new StringBuilder();
				sbQuery.append(" Select OP From ExceptionUserPrivilege EUP , UserPrivilegeDetail UPD , OptionPrivilege OP where EUP.state = :state ");
				sbQuery.append(" and EUP.idExceptionUserPrivilegePk = UPD.exceptionUserPrivilege.idExceptionUserPrivilegePk");
				sbQuery.append(" and UPD.optionPrivilege.idOptionPrivilegePk = OP.idOptionPrivilegePk");
				sbQuery.append(" and EUP.userAccount.idUserPk = :userId ");
				sbQuery.append(" and :currentTime between EUP.initialDate and EUP.endDate");
				Query query = em.createQuery(sbQuery.toString());
				query.setParameter("state", UserExceptionStateType.CONFIRMED.getCode());
				query.setParameter("userId",idUserAccountPK);
				query.setParameter("currentTime",CommonsUtilities.currentDateTime());
				query.setHint("org.hibernate.cacheable", true);
				List<OptionPrivilege> listExceptions =  query.getResultList();
					return listExceptions;
			}catch (NoResultException nre) {
				return new ArrayList<OptionPrivilege>();
			}
		}
		
		/**
		 * to get all the users.
		 *
		 * @return List<Object[]>
		 */
		public List<Object[]> getUsersList() {
			return (List<Object[]>) findByQueryString("Select ua.loginUser,ua.fullName From UserAccount ua Order By ua.loginUser ");
		}
		
		/**
		 * to get ExceptionUserPrivileges Details By Ids.
		 *
		 * @param list ExceptionUserPrivileges ids
		 * @return List<Object[]>
		 */
		public List<Object[]> getExceptionUserPrivilegesDetailsByIds(List<Integer> list){
			 Query query = em.createQuery("select eup.userAccount.idUserPk,eup.userAccount.loginUser,eup.system.idSystemPk from ExceptionUserPrivilege eup  Where eup.idExceptionUserPrivilegePk in (:list) ");
			 query.setParameter("list", list);
			return query.getResultList();
		}
		
		public byte[] getExceptionUserPrivilegeFile(Integer idException){
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("Select EUP.documentFile from ExceptionUserPrivilege EUP where EUP.idExceptionUserPrivilegePk = :idException ");	
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idException", idException);
			return  (byte[])query.getSingleResult(); 
		}
}