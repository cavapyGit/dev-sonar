package com.pradera.security.systemmgmt.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UISelectOne;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.persistence.Table;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.primefaces.push.PushContext;
import org.primefaces.push.PushContextFactory;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.httpevent.publish.BrowserWindow;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.security.model.type.LanguageType;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.model.HistoricalState;
import com.pradera.security.model.OptionPrivilege;
import com.pradera.security.model.Parameter;
import com.pradera.security.model.ProfilePrivilege;
import com.pradera.security.model.ProfilePrivilegeRequest;
import com.pradera.security.model.System;
import com.pradera.security.model.SystemProfile;
import com.pradera.security.model.SystemProfileRequest;
import com.pradera.security.model.UserAccount;
import com.pradera.security.model.type.DefinitionType;
import com.pradera.security.model.type.OptionPrivilegeStateType;
import com.pradera.security.model.type.OptionStateType;
import com.pradera.security.model.type.OtherMotiveType;
import com.pradera.security.model.type.ParameterTableType;
import com.pradera.security.model.type.PrivilegeType;
import com.pradera.security.model.type.ProfilePrivilegeStateType;
import com.pradera.security.model.type.SystemProfileSituationType;
import com.pradera.security.model.type.SystemProfileStateType;
import com.pradera.security.model.type.SystemStateType;
import com.pradera.security.systemmgmt.service.ParameterSecurityServiceBean;
import com.pradera.security.systemmgmt.view.filters.SystemProfileFilter;
import com.pradera.security.usermgmt.view.UserDataModel;
import com.pradera.security.utils.view.GenericBaseBean;
import com.pradera.security.utils.view.PropertiesConstants;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class SystemProfileMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/02/2013
 */
@DepositaryWebBean
public class SystemProfileMgmtBean extends GenericBaseBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -893651970218941185L;
	
	/** The log. */
    Logger log = Logger.getLogger(SystemProfileMgmtBean.class.getName());
    
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;
	
	/** The parameter security service. */
	@EJB
	ParameterSecurityServiceBean parameterSecurityService;
	
	/** The system profile. */
	private SystemProfile systemProfile;
	
	/** The selectedSystemProfile. */
	private SystemProfile selectedSystemProfile;

	/** The system profile filter. */
	private SystemProfileFilter systemProfileFilter;

	/** The system profile data model. */
	private SystemProfileDataModel systemProfileDataModel;

	/** The historical state. */
	private HistoricalState historicalState;

    /** The root privileges. */
    private TreeNode rootPrivileges;

    /** The selected privilege nodes. */
    private TreeNode[] selectedPrivilegeNodes; 

    /** The expanded nodes. */
    private boolean expandedNodes;

    /** The show other motive. */
    private boolean showOtherMotive;
    
    /** The is confirm bock or unblock. */
    private boolean isConfirmBockOrUnblock;
    
    /** The is reject option. */
    private boolean isRejectOption;
    
    /** The lst institution type. */
    private List<Parameter> lstInstitutionType;
    
    /** The lst institution type filter. */
    private List<Parameter> lstInstitutionTypeFilter;
    
    /** The list profile type. */
    private List<Parameter> lstProfileType;
    
    /** The bl ind confirm. */
    private boolean blIndConfirm;
    
    /** The situations profile. */
    private Map<Integer,String> situationsProfile;
    
	/**
	 * Instantiates a new system profile mgmt bean.
	 */
	public SystemProfileMgmtBean() {		
		systemProfile = new SystemProfile();
		systemProfileFilter = new SystemProfileFilter();
		historicalState = new HistoricalState();
	}
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		try {
			//Begin the conversation
			beginConversation();
			//List the Systems to display on the combo box of the page
			listAllSystems();
			//List the States of Profile to display on the combo box of the page
			listSystemProfileState();
			//To load the block and unblock motives for profile management based on the definition table FK value in the parameter table
			lstBlockMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.SYS_PROFILE_BLOCK_MOTIVES.getCode());
			lstUnBlockMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.SYS_PROFILE_DESBLOCK_MOTIVES.getCode());
			lstRejectMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.SYS_PROFILE_REJECT_MOTIVES.getCode());
			lstInstitutionTypeFilter = userServiceFacade.listClientTypeByDefinitionServiceFacade(ParameterTableType.INSITUTIONS_STATE.getCode());
			lstProfileType = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.PROFILE_TYPE.getCode());
			//situations
			List<Integer> situationsId= new ArrayList<>();
			for(SystemProfileSituationType situation:SystemProfileSituationType.list){
				situationsId.add(situation.getCode());
			}
			situationsProfile = new HashMap<>();
			for(Parameter param:parameterSecurityService.listParameterByIdentificator(situationsId)){
				situationsProfile.put(param.getIdParameterPk(), param.getName());
			}
			//setting privilege
			PrivilegeComponent privilegeComp=new PrivilegeComponent();
			privilegeComp.setBtnModifyView(true);
			privilegeComp.setBtnConfirmView(true);
			privilegeComp.setBtnRejectView(true);
			privilegeComp.setBtnBlockView(true);
			privilegeComp.setBtnUnblockView(true);
			privilegeComp.setBtnDeleteView(true);
			userPrivilege.setPrivilegeComponent(privilegeComp);
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Select user type.
	 */
	public void selectUserType(){
		lstInstitutionType = new ArrayList<Parameter>();
		lstInstitutionType.addAll(lstInstitutionTypeFilter);
		if(BooleanType.YES.getCode().equals(systemProfile.getIndUserType())){
			systemProfile.setExtern(true);
			for(Parameter param: lstInstitutionTypeFilter){
				if(InstitutionType.DEPOSITARY.getCode().equals(param.getIdParameterPk())){
					lstInstitutionType.remove(param);
				}
			}
		}else{
			systemProfile.setExtern(false);
			for(Parameter param: lstInstitutionTypeFilter){
				if(!InstitutionType.DEPOSITARY.getCode().equals(param.getIdParameterPk())){
					lstInstitutionType.remove(param);
				}
			}
			systemProfile.setInstitutionType(lstInstitutionType.get(0).getIdParameterPk());
		}
		
	}
	/**
	 * Search profiles listener.
	 *
	 * @param actionEvent the action event
	 */
	public void searchProfilesListener(ActionEvent actionEvent){
		try{
			//List the profiles in order to daplay the list on the page
			List<SystemProfile> lstSystemProfile = systemServiceFacade.searchProfilesByFiltersServiceFacade(systemProfileFilter);
			//Its necessary send a DataModel instead of List in order to use the radiobuttom column of PrimeFaces on Datatable
			
			if(Validations.validateListIsNotNullAndNotEmpty(lstSystemProfile)){
				for(SystemProfile systemprofile : lstSystemProfile){
					Parameter objParameter = systemServiceFacade.getParameterByPk(systemprofile.getInstitutionType());
					if(objParameter != null){
						systemprofile.setInstitutionName(objParameter.getName());
					}				
				}
			}	
			
			systemProfileDataModel = new SystemProfileDataModel(lstSystemProfile);
			
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Clean listener.
	 * The method is used to clean the administration page (entered filters and datatable)
	 *
	 * @param actionEvent the action event
	 */
	public void cleanListener(ActionEvent actionEvent){
		systemProfileFilter = new SystemProfileFilter();
		systemProfileDataModel = null;		
		selectedSystemProfile=null;
	}
	
	/**
	 * Load register profile
	 * Initializes the attributes to show a new SystemProfile bean on register page.
	 *
	 * @param actionEvent the action event
	 */
	public void loadRegisterProfileListener(ActionEvent actionEvent){
		lstInstitutionType = new ArrayList<Parameter>();
		lstInstitutionType.addAll(lstInstitutionTypeFilter);
		for(Parameter param: lstInstitutionTypeFilter){
			if(!InstitutionType.DEPOSITARY.getCode().equals(param.getIdParameterPk())){
				lstInstitutionType.remove(param);
			}
		}
		blIndConfirm=false;
		systemProfile= new SystemProfile();
		systemProfile.setIndUserType(BooleanType.NO.getCode());
		systemProfile.setInstitutionType(lstInstitutionType.get(0).getIdParameterPk());
		JSFUtilities.updateComponent(":frmProfileMgmt:customRadio");
		listSystemsByState(SystemStateType.CONFIRMED.getCode());

		System system = new System();
		system.setIdSystemPk(-1);
		
		systemProfile.setSystem(system);
		//Set the Operation of ManagedBean to "REGISTER"
		this.setOperationType(ViewOperationsType.REGISTER.getCode());
		rootPrivileges = null;
	}
	
	/**
	 * Load the data of system profile to modify.
	 *
	 * @return the string
	 */
	public String loadModifyProfileAction(){
		blIndConfirm=false;
		try {
			hideDialogsListener(null);
			//JSFUtilities.hideComponent("frmMotive:cnfMotive");
			JSFUtilities.executeJavascriptFunction("PF('cnfwMotive').hide()");
			if(selectedSystemProfile==null){
				//if the system profile was not selected, a message will be shown
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.ERROR_RECORD_NO_SELECTED, null);			
				JSFUtilities.addContextMessage(null,
						FacesMessage.SEVERITY_ERROR,"","");
				JSFUtilities.showValidationDialog();
				return null;
			} else if(SystemProfileStateType.BLOCKED.getCode().equals(selectedSystemProfile.getState()) ||
						SystemProfileStateType.REJECTED.getCode().equals(selectedSystemProfile.getState())
						|| SystemProfileStateType.DELETED.getCode().equals(selectedSystemProfile.getState())){
				//if the system profile to modify has a state different or registered, a message will be shown
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.ERROR_ADM_PROFILE_MODIFY_STATE_BLOCKED, null);
				JSFUtilities.addContextMessage(null,
						FacesMessage.SEVERITY_ERROR,"","");
				JSFUtilities.showValidationDialog();
				return null;
			}else if(SystemProfileStateType.CONFIRMED.getCode().equals(selectedSystemProfile.getState())
					&& !SystemProfileSituationType.ACTIVE.getCode().equals(selectedSystemProfile.getSituation())
					&& !SystemProfileSituationType.PENDING_MODIFICATE.getCode().equals(selectedSystemProfile.getSituation())){
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT,null, 
						"system.profile.error.situation.message", null);
				JSFUtilities.addContextMessage(null,FacesMessage.SEVERITY_ERROR,"","");
				JSFUtilities.showValidationDialog();
				return null;
			}
			lstInstitutionType = new ArrayList<Parameter>();
			lstInstitutionType.addAll(lstInstitutionTypeFilter);
			if(BooleanType.YES.getCode().equals(selectedSystemProfile.getIndUserType())){
				for(Parameter param: lstInstitutionTypeFilter){
					if(InstitutionType.DEPOSITARY.getCode().equals(param.getIdParameterPk())){
						lstInstitutionType.remove(param);
					}
				}
			}else{
				for(Parameter param: lstInstitutionTypeFilter){
					if(!InstitutionType.DEPOSITARY.getCode().equals(param.getIdParameterPk())){
						lstInstitutionType.remove(param);
					}
				}
			}
			//behavior according state
			if(SystemProfileStateType.CONFIRMED.getCode().equals(selectedSystemProfile.getState())){
				//check last modification
				if(selectedSystemProfile != null && systemServiceFacade.getLatsModifiedDetails(selectedSystemProfile).after(selectedSystemProfile.getLastModifyDate())){
					Object[] arrBodyData = {selectedSystemProfile.getName()};
					showMessageOnDialog(null, null,PropertiesConstants.SYSTEM_PROFILE_ERROR_MODIFIED_RECENT,arrBodyData);
					JSFUtilities.addContextMessage(null,FacesMessage.SEVERITY_ERROR,"","");
					JSFUtilities.showValidationDialog();
					return null;
				}
				SystemProfileRequest profileRequestModification = systemServiceFacade.getRequestProfileModify(selectedSystemProfile);
				//when not exists some request in state registered for profile show modificationPage
				if(profileRequestModification == null){
					blIndConfirm = true;
					loadSelectedProfile();
					//Load the Privileges to show them on the Tree
					loadProfilePrivilegesForModify();
				} else {
					//if exist request so show dialog for confirm o reject request
					loadSelectedProfile();
					//JSFUtilities.showComponent("frmModification:dlgModification");
					JSFUtilities.executeJavascriptFunction("PF('dlgModificationWidget').show()");
					return null;
				}
			} //if is registered state so is regular modification
			else{
				loadSelectedProfile();
				//Load the Privileges to show them on the Tree
				loadProfilePrivilegesForModify();
			}
			//Set the operation of Managed Bean to "MODIFY"
			this.setOperationType(ViewOperationsType.MODIFY.getCode());
			return "systemProfileMgmtRule";
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}
	
	/**
	 * To set the selectedProfile to systemProfile.
	 */
	public void loadSelectedProfile(){
		systemProfile = new SystemProfile();
		systemProfile.setIndUserType(selectedSystemProfile.getIndUserType());
		if(selectedSystemProfile.getInstitutionType()!=null){
			systemProfile.setInstitutionType(selectedSystemProfile.getInstitutionType());
		}	
		systemProfile.setIdSystemProfilePk(selectedSystemProfile.getIdSystemProfilePk());
		systemProfile.setDescription(selectedSystemProfile.getDescription());
		systemProfile.setLastModifyDate(selectedSystemProfile.getLastModifyDate());
		systemProfile.setLastModifyIp(selectedSystemProfile.getLastModifyIp());
		systemProfile.setLastModifyPriv(selectedSystemProfile.getLastModifyPriv());
		systemProfile.setLastModifyUser(selectedSystemProfile.getLastModifyUser());
		systemProfile.setMnemonic(selectedSystemProfile.getMnemonic());
		systemProfile.setName(selectedSystemProfile.getName());
		systemProfile.setProfilePrivileges(selectedSystemProfile.getProfilePrivileges());
		systemProfile.setUserProfiles(selectedSystemProfile.getUserProfiles());
		systemProfile.setRegistryDate(selectedSystemProfile.getRegistryDate());
		systemProfile.setRegistryUser(selectedSystemProfile.getRegistryUser());
		systemProfile.setState(selectedSystemProfile.getState());
		systemProfile.setSituation(selectedSystemProfile.getSituation());
		systemProfile.setSystem(selectedSystemProfile.getSystem());
		systemProfile.setBlockMotive(selectedSystemProfile.getBlockMotive());			
		systemProfile.setBlockOtherMotive(selectedSystemProfile.getBlockOtherMotive());
		systemProfile.setUnblockMotive(selectedSystemProfile.getUnblockMotive());	
		systemProfile.setUnblockOtherMotive(selectedSystemProfile.getUnblockOtherMotive());
		systemProfile.setRejectMotive(selectedSystemProfile.getRejectMotive());	
		systemProfile.setRejectOtherMotive(selectedSystemProfile.getRejectOtherMotive());
		systemProfile.setDeleteOtherMotive(selectedSystemProfile.getDeleteOtherMotive());
		systemProfile.setProfileType(selectedSystemProfile.getProfileType());
	}
	
	/**
	 * Save listener.
	 * This method is triggered when the button "Save" is selected on the page of Register and Modify
	 *
	 * @param actionEvent the action event
	 */
	@LoggerAuditWeb
	public void saveListener(ActionEvent actionEvent){							
		//Validate if the Current Operation of the ManagedBean is Register or Modify
		if(ViewOperationsType.REGISTER.getCode().equals(getOperationType())){
			register();			
		} else {
			modify();
		}

		systemProfile = null;
	}
	
	/**
	 * Register.
	 * The method save a new System Profile
	 */
	private void register(){
		try{
			hideDialogsListener(null);	
			systemProfile.setState(SystemProfileStateType.REGISTERED.getCode());
			
			//Set Test data for audit columns 
			systemProfile.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			systemProfile.setRegistryDate(CommonsUtilities.currentDateTime());
			
			//Find the System From Database
			System system = systemServiceFacade.findSystemByIdServiceFacade(systemProfile.getSystem().getIdSystemPk());
			
			systemProfile.setSystem(system);
						
			ProfilePrivilege profilePrivilege = null;						
			OptionPrivilegeNodeBean optionPrivilegeNodeBean = null;
			OptionPrivilege optionPrivilege = null;
			
			//This list will be contain the ProfilePrivileges to save on database
			List<ProfilePrivilege> profilePrivileges = new ArrayList<ProfilePrivilege>();
			
			//For of the selected Privilege Nodes
			for (TreeNode node : selectedPrivilegeNodes) {
				optionPrivilegeNodeBean = (OptionPrivilegeNodeBean) node.getData();
				/*Validate if the IdOptionPrivilege of optionPrivilegeNodeBean is different of null
				in order to check if the selected node is a privilege or just a system option
				If the optionPrivilegeNodeBean is a Privilege, it will be add to the list profilePrivileges
				*/
				if(optionPrivilegeNodeBean.getIdOptionPrivilege() != null) {
					
					optionPrivilege = new OptionPrivilege();
					optionPrivilege.setIdOptionPrivilegePk(optionPrivilegeNodeBean.getIdOptionPrivilege());
					
					profilePrivilege = new ProfilePrivilege();
					profilePrivilege.setSystemProfile(systemProfile);
					profilePrivilege.setOptionPrivilege(optionPrivilege);
					profilePrivilege.setState(ProfilePrivilegeStateType.REGISTERED.getCode());
					profilePrivilege.setRegistryUser(userInfo.getUserAccountSession().getUserName());
					profilePrivilege.setRegistryDate(CommonsUtilities.currentDateTime());			
					
					profilePrivileges.add(profilePrivilege);
				}
			}
			//Set the list of the adder ProfilePrivileges to the system profile
			systemProfile.setProfilePrivileges(profilePrivileges);
			
			//Register the system profile on database
			boolean registerResult = systemServiceFacade.registerSystemProfileServiceFacade(systemProfile);			
			
			if(registerResult){
				//If the system profile was registered without problems, a successful message will be shown 
				Object[] arrBodyData = {systemProfile.getMnemonic()};
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_SUCCESS, null,
						PropertiesConstants.SUCCESS_ADM_PROFILE_REGISTER_LABEL,
						arrBodyData);
				cleanListener(null);
				searchProfilesListener(null);
				JSFUtilities.showEndTransactionDialog();
			}
		
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		
		
	}
	
	/**
	 * Modify.
	 * This method modify the data of the selected system profile
	 */
	private void modify(){
		try{
			hideDialogsListener(null);	
			Object[] argObj=new Object[2];
			SystemProfile latestSystemProfile = systemServiceFacade.getLastestSysemProfileDetailsFacade(systemProfile.getIdSystemProfilePk());
			Calendar selectedProfile=Calendar.getInstance();
			selectedProfile.setTime(systemProfile.getLastModifyDate());
			Calendar tempProfile=Calendar.getInstance();
			tempProfile.setTime(latestSystemProfile.getLastModifyDate());
			
			if(tempProfile.after(selectedProfile.getTimeInMillis())){
			    argObj[0]=systemProfile.getName();
			    showMessageOnDialog(null,null,PropertiesConstants.SYSTEM_PROFILE_ERROR_MODIFIED_RECENT,argObj);
			    cleanListener(null);
			    searchProfilesListener(null);
			    JSFUtilities.showEndTransactionDialog();
			    return;
			}
			/*This list will be contain the ProfilePrivileges to save on database
			 * Profile Privileges can be add if not exists or update the state when 
			 * it will be removed.
			 * First get the list of current Profile Privileges of the System Profile of database
			 */
			List<ProfilePrivilege> profilePrivileges = systemProfile.getProfilePrivileges();		
			for (ProfilePrivilege profilePrivilege : profilePrivileges) {
				//validate if the privilege currently registered on database was selected on the tree
				if(!isSelectedProfilePrivilege(profilePrivilege)){
					//if was not selected, the state will be updated to "DETELED"
					profilePrivilege.setState(ProfilePrivilegeStateType.DELETED.getCode());
				}
			}
			ProfilePrivilege profilePrivilege = null;						
			OptionPrivilegeNodeBean optionPrivilegeNodeBean = null;
			OptionPrivilege optionPrivilege = null;
			for (TreeNode node : selectedPrivilegeNodes) {
				optionPrivilegeNodeBean = (OptionPrivilegeNodeBean) node.getData();
				/*Validate if the IdOptionPrivilege of optionPrivilegeNodeBean is different of null
				in order to check if the selected node is a privilege or just a system option
				Also validate if the IdProfilePrivilege is different of null in order to check
				if the selected privilege is currently registered on database
				*/
				if(optionPrivilegeNodeBean.getIdOptionPrivilege() != null
						&& optionPrivilegeNodeBean.getIdProfilePrivilege() == null) {
					optionPrivilege = new OptionPrivilege();
					optionPrivilege.setIdOptionPrivilegePk(optionPrivilegeNodeBean.getIdOptionPrivilege());
					profilePrivilege = new ProfilePrivilege();
					profilePrivilege.setSystemProfile(systemProfile);
					profilePrivilege.setOptionPrivilege(optionPrivilege);
					profilePrivilege.setState(ProfilePrivilegeStateType.REGISTERED.getCode());
					profilePrivilege.setRegistryUser(userInfo.getUserAccountSession().getUserName());
					profilePrivilege.setRegistryDate(CommonsUtilities.currentDateTime());			
					profilePrivileges.add(profilePrivilege);
				}
			}
			boolean modifyResult=false;
			String messageModal= null;
			//check if profile is in state confirmed so register modification in request
			if(blIndConfirm){
				modifyResult = systemServiceFacade.registerModifyRequestProfile(systemProfile);
				messageModal = "success.adm.profile.request.modify.label";
			} else {
				//Modify the system profile on database
				modifyResult = systemServiceFacade.modifySystemProfileServiceFacade(systemProfile);
				messageModal=PropertiesConstants.SUCCESS_ADM_PROFILE_MODIFY_LABEL;
			}
		//	List<UserProfile> usersList = systemServiceFacade.systemProfilesUserList(systemProfile.getIdSystemProfilePk());
			/*for(UserProfile userProfile:usersList){
				String loginUser=userProfile.getUserAccount().getLoginUser();
				log.info("login user:--"+loginUser);
				PushContext pushContext = PushContextFactory.getDefault().getPushContext();
				//integration with notifications
				BrowserWindow browser = new BrowserWindow();
				browser.setNotificationType(NotificationType.KILLSESSION.getCode());
				browser.setSubject("Expire Session");
				browser.setMessage("Expire Session user "+loginUser);
				pushContext.push(PropertiesConstants.PUSH_MSG+"/"+loginUser,browser);
			}*/
			
			if(modifyResult){		
				//If the system profile was modified without problems, a successful message will be shown 
				Object[] arrBodyData = {systemProfile.getName(),systemProfile.getMnemonic()};
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_SUCCESS, null,
						messageModal, 
						arrBodyData);
				cleanListener(null);
				searchProfilesListener(null);
				JSFUtilities.showEndTransactionDialog();
			}
		
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * The method check if a ProfilePrivilege was selected on the tree of the page when a system profile
	 * will be modified.
	 *
	 * @param profilePrivilege the profile privilege
	 * @return true, if is selected profile privilege
	 */
	private boolean isSelectedProfilePrivilege(ProfilePrivilege profilePrivilege){
								
		OptionPrivilegeNodeBean optionPrivilegeNodeBean = null;		
		
		//traverse the selected nodes (system Options and Privileges)
		for (TreeNode selectedNode : selectedPrivilegeNodes) {
			optionPrivilegeNodeBean = (OptionPrivilegeNodeBean) selectedNode.getData();
			/*If the IdProfilePrivilege of optionPrivilegeNodeBean is equal to the IdProfilePrivilegePk
			 * of profilePrivilege, return true
			 */
			if(optionPrivilegeNodeBean.getIdProfilePrivilege() != null && profilePrivilege.getIdProfilePrivilegePk() != null){
				if(optionPrivilegeNodeBean.getIdProfilePrivilege().intValue() == profilePrivilege.getIdProfilePrivilegePk().intValue()){
					return true;
				}
			}
		}
		
		return false;
	}
	
	/**
	 * This method is called when a motive is required in order to block or unblock a system profile
	 * and the user change the motive of the combobox.
	 */
	public void changeMotiveSelectionListener(){		
		//Validate the selected motive. When motive is "OTHER_MOTIVE", the text box for Other Motive will be shown
		JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
        if(isBlock()){
        	if(OtherMotiveType.SYS_PROFILE_BLOCK_OTHER_MOTIVE.getCode().equals(historicalState.getMotive())){
        		showOtherMotive = true;
        	}else {
    			showOtherMotive = false;
    		}
        }
        else if(isUnBlock()){
        	if(OtherMotiveType.SYS_PROFILE_UNBLOCK_OTHER_MOTIVE.getCode().equals(historicalState.getMotive())){
        		showOtherMotive = true;
        	}else {
    			showOtherMotive = false;
    		}
        }else if(isReject()){
        	if(OtherMotiveType.SYS_PROFILE_REJECT_OTHER_MOTIVE.getCode().equals(historicalState.getMotive())){
        		showOtherMotive = true;
        	}else {
    			showOtherMotive = false;
    		}
        }
	}
	
	/**
	 * Begin confirm listener.
	 *
	 * @param actionEvent the action event
	 */
	public void beginConfirmListener(ActionEvent actionEvent){
		hideDialogsListener(null);
		if(selectedSystemProfile==null){
			//If the system profile was not selected, an error message will be shown
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT, null, 
					PropertiesConstants.ERROR_RECORD_NO_SELECTED, null);
			JSFUtilities.addContextMessage(null,
					FacesMessage.SEVERITY_ERROR,"","");
			JSFUtilities.showValidationDialog();
			return;
		}
		if(!SystemProfileStateType.REGISTERED.getCode().equals(selectedSystemProfile.getState())){
			//If the state of the selected system profile is different to "REGISTERED", an error message will be shown
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT,null, 
					PropertiesConstants.SYSTEM_PROFILE_CONFIRM_REGISTER_ERROR, null);
			JSFUtilities.addContextMessage(null,
					FacesMessage.SEVERITY_ERROR,"","");
			JSFUtilities.showValidationDialog();
			return;
		}
		loadSelectedProfile();
		//Instance a new HistoricalState to save the motive of block
		historicalState = new HistoricalState();
		historicalState.setIdHistoricalStatePk(Long.valueOf(-1));		
		//Set the operation of Managed Bean to "BLOCK"
		this.setOperationType(ViewOperationsType.CONFIRM.getCode());
		
		Object[] bodyData = {
				systemProfile.getName().toUpperCase() ,
				systemProfile.getMnemonic().toUpperCase()};
		showMessageOnDialog(
				PropertiesConstants.DIALOG_HEADER_CONFIRM, null, 
				PropertiesConstants.SYSTEM_PROFILE_CONFIRM_MESSAGE, bodyData);
		//JSFUtilities.showComponent("frmConfConfirm:cnfConfirm");
		JSFUtilities.executeJavascriptFunction("PF('cnfwConfirm').show();");
	}
	
	/**
	 * Begin delete listener.
	 *
	 * @param actionEvent the action event
	 */
	public void beginDeleteListener(ActionEvent actionEvent){
		hideDialogsListener(null);
		historicalState = new HistoricalState();
		historicalState.setIdHistoricalStatePk(Long.valueOf(-1));
		showOtherMotive = false;
		isConfirmBockOrUnblock = false;
		isRejectOption = false;
		if(selectedSystemProfile==null){
			//If the system profile was not selected, an error message will be shown
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT, null, 
					PropertiesConstants.ERROR_RECORD_NO_SELECTED, null);
			JSFUtilities.addContextMessage(null,
					FacesMessage.SEVERITY_ERROR,"","");
			JSFUtilities.showValidationDialog();
			return;
		} else if(!SystemProfileStateType.CONFIRMED.getCode().equals(selectedSystemProfile.getState())){
			//If the state of the selected system profile is different to "CONFIRMED", an error message will be shown
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT,null, 
					"system.profile.delete.error", null);
			JSFUtilities.addContextMessage(null,
					FacesMessage.SEVERITY_ERROR,"","");
			JSFUtilities.showValidationDialog();
			return;
		} else if(!SystemProfileSituationType.ACTIVE.getCode().equals(selectedSystemProfile.getSituation())
				&& !SystemProfileSituationType.PENDING_DELETE.getCode().equals(selectedSystemProfile.getSituation())){
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT,null, 
					"system.profile.error.situation.message", null);
			JSFUtilities.addContextMessage(null,FacesMessage.SEVERITY_ERROR,"","");
			JSFUtilities.showValidationDialog();
			return;
		}
		loadSelectedProfile();
		this.setOperationType(ViewOperationsType.DELETE.getCode());
		showOtherMotive = true;
		if(systemProfile.getState().equals(SystemProfileStateType.CONFIRMED.getCode())
				&& systemProfile.getSituation().equals(SystemProfileSituationType.PENDING_DELETE.getCode())){
				isConfirmBockOrUnblock = true;			
				historicalState.setMotive(-1);
				historicalState.setMotiveDescription(systemProfile.getDeleteOtherMotive());
		}
		showMessageOnDialog2("system.lbl.eliminar", null, null, null);
		//JSFUtilities.showComponent("frmMotive:cnfMotive");
		JSFUtilities.executeJavascriptFunction("PF('cnfwMotive').show()");
	}
	
	/**
	 * Begin reject listener.
	 *
	 * @param actionEvent the action event
	 */
	public void beginRejectListener(ActionEvent actionEvent){
		hideDialogsListener(null);
		historicalState = new HistoricalState();
		historicalState.setIdHistoricalStatePk(Long.valueOf(-1));
		showOtherMotive = false;
		isConfirmBockOrUnblock = false;
		isRejectOption = false;
		
		if(selectedSystemProfile==null){
			//If the system profile was not selected, an error message will be shown
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT, null, 
					PropertiesConstants.ERROR_RECORD_NO_SELECTED, null);
			JSFUtilities.addContextMessage(null,
					FacesMessage.SEVERITY_ERROR,"","");
			JSFUtilities.showValidationDialog();
			return;
		}
		if(!SystemProfileStateType.REGISTERED.getCode().equals(selectedSystemProfile.getState())){
			//If the state of the selected system profile is different to "REGISTERED", an error message will be shown
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT,null, 
					PropertiesConstants.SYSTEM_PROFILE_REJECT_REGISTER_ERROR, null);
			JSFUtilities.addContextMessage(null,
					FacesMessage.SEVERITY_ERROR,"","");
			JSFUtilities.showValidationDialog();
			return;
		}
		loadSelectedProfile();
		
		this.setOperationType(ViewOperationsType.REJECT.getCode());
		
		if(systemProfile.getState().equals(SystemProfileStateType.REGISTERED.getCode())
				&& systemProfile.getSituation() != null
				&& systemProfile.getSituation().equals(SystemProfileSituationType.PENDING_REJECTED.getCode())){
				
				isConfirmBockOrUnblock = true;			
				historicalState.setMotive(systemProfile.getRejectMotive());
				
				if(systemProfile.getRejectMotive().equals(OtherMotiveType.SYS_PROFILE_REJECT_OTHER_MOTIVE.getCode())){
					showOtherMotive = true;
					historicalState.setMotiveDescription(systemProfile.getRejectOtherMotive());
				}
			}
		showMessageOnDialog2(PropertiesConstants.DIALOG_HEADER_MOTIVE_REJECT, null, null, null);
		//JSFUtilities.showComponent("frmMotive:cnfMotive");
		JSFUtilities.executeJavascriptFunction("PF('cnfwMotive').show()");
	}
	
	/**
	 * This Method will show the dialog of motives of block.
	 *
	 * @param actionEvent the action event
	 */
	public void beginBlockListener(ActionEvent actionEvent){
		hideDialogsListener(null);
		historicalState = new HistoricalState();
		historicalState.setIdHistoricalStatePk(Long.valueOf(-1));
		showOtherMotive = false;
		isConfirmBockOrUnblock = false;
		isRejectOption = false;
		
		if(selectedSystemProfile==null){
			//If the system profile was not selected, an error message will be shown
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT, null, 
					PropertiesConstants.ERROR_RECORD_NO_SELECTED, null);
			JSFUtilities.addContextMessage(null,
					FacesMessage.SEVERITY_ERROR,"","");
			JSFUtilities.showValidationDialog();
			return;
		} else if(!SystemProfileStateType.CONFIRMED.getCode().equals(selectedSystemProfile.getState())){
			//If the state of the selected system profile is different to "REGISTERED", an error message will be shown
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT,null, 
					PropertiesConstants.SYSTEM_PROFILE_BLOCK_ERROR, null);
			JSFUtilities.addContextMessage(null,FacesMessage.SEVERITY_ERROR,"","");
			JSFUtilities.showValidationDialog();
			return;
		} else if(!SystemProfileSituationType.ACTIVE.getCode().equals(selectedSystemProfile.getSituation())
				&& !SystemProfileSituationType.PENDING_LOCK.getCode().equals(selectedSystemProfile.getSituation())){
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT,null, 
					"system.profile.error.situation.message", null);
			JSFUtilities.addContextMessage(null,FacesMessage.SEVERITY_ERROR,"","");
			JSFUtilities.showValidationDialog();
			return;
		}
		
		loadSelectedProfile();
		
		this.setOperationType(ViewOperationsType.BLOCK.getCode());		
				
		if(systemProfile.getState().equals(SystemProfileStateType.CONFIRMED.getCode())
			&& systemProfile.getSituation().equals(SystemProfileSituationType.PENDING_LOCK.getCode())){
			
			isConfirmBockOrUnblock = true;			
			historicalState.setMotive(systemProfile.getBlockMotive());
			
			if(systemProfile.getBlockMotive().equals(OtherMotiveType.SYS_PROFILE_BLOCK_OTHER_MOTIVE.getCode())){
				showOtherMotive = true;
				historicalState.setMotiveDescription(systemProfile.getBlockOtherMotive());
			}
		}
		
		showMessageOnDialog2(PropertiesConstants.DIALOG_HEADER_BLOCKMOTIVE, null, null, null);
		//JSFUtilities.showComponent("frmMotive:cnfMotive");
		JSFUtilities.executeJavascriptFunction("PF('cnfwMotive').show()");
	}
	
	/**
	 * Accept modification.
	 *
	 * @param actionEvent the action event
	 */
	public void acceptModification(ActionEvent actionEvent){
		isRejectOption = false;
		hideDialogsListener(null);
		Object[] arrBodyData = {systemProfile.getName().toUpperCase()};
		//JSFUtilities.showComponent("frmConfModify:cnfModify");
		JSFUtilities.executeJavascriptFunction("PF('cnfModifyWidget').show();");
		showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_CONFIRM, null,"system.profile.modification.message", arrBodyData);
	}
	
	/**
	 * Reject modification.
	 *
	 * @param actionEvent the action event
	 */
	public void rejectModification(ActionEvent actionEvent){
		isRejectOption = true;
		hideDialogsListener(null);
		Object[] arrBodyData = {systemProfile.getName().toUpperCase()};
		//JSFUtilities.showComponent("frmConfModify:cnfModify");
		JSFUtilities.executeJavascriptFunction("PF('cnfModifyWidget').show();");
		showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_CONFIRM, null,"system.profile.reject.modification.message", arrBodyData);
	}
	
	/**
	 * This Method set the motive of Block and show the Confirmation for block.
	 *
	 * @param actionEvent the action event
	 */
	public void acceptMotiveListener(ActionEvent actionEvent){
		if(showOtherMotive==true){
			if (Validations.validateIsNullOrEmpty(this.getHistoricalState().getMotiveDescription())||
					this.getHistoricalState().getMotiveDescription().trim().length() == 0) {
				this.getHistoricalState().setMotiveDescription("");
				hideDialogsListener(actionEvent);
				JSFUtilities.showComponent("cnfMsgCustomValidation");					
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_ALERT, null,
						PropertiesConstants.ERROR_MESSAGE_MOTIVE_REQUIRED,null);
				String errorMessage=PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_MOTIVE_REQUIRED);
				JSFUtilities.addContextMessage("frmMotive:txtOtherMotive",FacesMessage.SEVERITY_ERROR, errorMessage,errorMessage);
				return;
			}
		}
		
		hideDialogsListener(null);
		Object[] arrBodyData = {
				systemProfile.getName().toUpperCase(),
				systemProfile.getMnemonic().toUpperCase()};
		String messageModal = "";
		
		if(this.isReject()){
			if( systemProfile.getSituation() == null){			
				messageModal = PropertiesConstants.SYSTEM_PROFILE_REJECT_REGISTER_MESSAGE;
			}else{
				messageModal = "system.profile.reject.message";
			}
			//JSFUtilities.showComponent("frmConfReject:cnfReject");
			JSFUtilities.executeJavascriptFunction("PF('cnfwReject').show();");
			
		}else if(this.isBlock()){
			if( systemProfile.getSituation().equals(SystemProfileSituationType.ACTIVE.getCode())){			
				messageModal = PropertiesConstants.SYSTEM_PROFILE_REQUEST_BLOCK_MESSAGE;
			}else{
				messageModal = PropertiesConstants.SYSTEM_PROFILE_BLOCK_MESSAGE;
			}
			//JSFUtilities.showComponent("frmConfBlock:cnfBlock");
			JSFUtilities.executeJavascriptFunction("PF('cnfwBlock').show();");
		}else if(this.isUnBlock()){
			if( systemProfile.getSituation().equals(SystemProfileSituationType.ACTIVE.getCode())){			
				messageModal = PropertiesConstants.SYSTEM_PROFILE_REQUEST_UNBLOCK_MESSAGE;
			}else{
				messageModal = PropertiesConstants.SYSTEM_PROFILE_UNBLOCK_MESSAGE;
			}
			//JSFUtilities.showComponent("frmConfUnblock:cnfUnblock");
			JSFUtilities.executeJavascriptFunction("PF('cnfwUnblock').show();");
		} else if(isDelete()){
			if( systemProfile.getSituation().equals(SystemProfileSituationType.ACTIVE.getCode())){			
				messageModal = "system.profile.request.delete.message";
			}else{
				messageModal = "system.profile.delete.message";
			}
			//JSFUtilities.showComponent("frmConfDelete:cnfDelete");
			JSFUtilities.executeJavascriptFunction("PF('cnfwDelete').show();");
		}
			
		showMessageOnDialog(
			PropertiesConstants.DIALOG_HEADER_CONFIRM, null,
			messageModal, arrBodyData);
	}
	
	/**
	 * Action reject motive listener.
	 *
	 * @param actionEvent the action event
	 */
	public void actionRejectMotiveListener(ActionEvent actionEvent){
		try {
			isRejectOption = true;
			hideDialogsListener(null);
			Object[] arrBodyData = {
					systemProfile.getName().toUpperCase(),
					systemProfile.getMnemonic().toUpperCase()};
			String messageModal = "";
			if(isBlock()){
				if(systemProfile.getState().equals(SystemProfileStateType.CONFIRMED.getCode()) && 
						systemProfile.getSituation().equals(SystemProfileSituationType.PENDING_LOCK.getCode())){			
					messageModal = PropertiesConstants.SYSTEM_PROFILE_REJECT_BLOCK_MESSAGE;
					//JSFUtilities.showComponent("frmConfBlock:cnfBlock");
					JSFUtilities.executeJavascriptFunction("PF('cnfwBlock').show();");
				}				
			}else if(isUnBlock()){
				if(systemProfile.getState().equals(SystemProfileStateType.BLOCKED.getCode()) && 
						systemProfile.getSituation().equals(SystemProfileSituationType.PENDING_UNLOCK.getCode())){
					messageModal = PropertiesConstants.SYSTEM_PROFILE_REJECT_UNBLOCK_MESSAGE;
					//JSFUtilities.showComponent("frmConfUnblock:cnfUnblock");
					JSFUtilities.executeJavascriptFunction("PF('cnfwUnblock').show();");
				}
			} else if(isReject()){
				if(systemProfile.getState().equals(SystemProfileStateType.REGISTERED.getCode()) && 
						systemProfile.getSituation().equals(SystemProfileSituationType.PENDING_REJECTED.getCode())){
					messageModal = "system.profile.reject.rejected.message";
					//JSFUtilities.showComponent("frmConfReject:cnfReject");
					JSFUtilities.executeJavascriptFunction("PF('cnfwReject').show();");
				}
			} else if(isDelete()){
				if(systemProfile.getState().equals(SystemProfileStateType.CONFIRMED.getCode()) && 
						systemProfile.getSituation().equals(SystemProfileSituationType.PENDING_DELETE.getCode())){
					messageModal = "system.profile.reject.delete.message";
					//JSFUtilities.showComponent("frmConfDelete:cnfDelete");
					JSFUtilities.executeJavascriptFunction("PF('cnfwDelete').show();");
				}
			}
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_CONFIRM,null,
					messageModal,arrBodyData);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * This method block the system profile.
	 *
	 * @param actionEvent the action event
	 */
	@LoggerAuditWeb
	public void blockListener(ActionEvent actionEvent) {
		try{
			hideDialogsListener(null);
			if(systemProfile != null){
			if(systemServiceFacade.getLatsModifiedDetails(systemProfile).after(systemProfile.getLastModifyDate())){
				Object[] arrBodyData = {systemProfile.getName()};
				showMessageOnDialog(null, null,PropertiesConstants.SYSTEM_PROFILE_ERROR_MODIFIED_RECENT,arrBodyData);
				JSFUtilities.addContextMessage(null,FacesMessage.SEVERITY_ERROR,"","");
				JSFUtilities.showValidationDialog();
				searchProfilesListener(null);
				return;
			}
			
			String messageModal = "";
			String messageNotification = "";
			boolean activeLogoutUsers = false;
			if(isRejectOption && (systemProfile.getState().equals(SystemProfileStateType.CONFIRMED.getCode())
					&&systemProfile.getSituation().equals(SystemProfileSituationType.PENDING_LOCK.getCode()))){
				systemProfile.setSituation(SystemProfileSituationType.ACTIVE.getCode());
				
				historicalState.setCurrentState(systemProfile.getSituation());
				messageModal = PropertiesConstants.SYSTEM_PROFILE_REJECT_BLOCK_SUCCESS;
				messageNotification = PropertiesConstants.SYSTEM_PROFILE_REJECT_BLOCK_NOTIFICATION;
				
			}else if(systemProfile.getState().equals(SystemProfileStateType.CONFIRMED.getCode())
					&& systemProfile.getSituation().equals(SystemProfileSituationType.ACTIVE.getCode())){
				systemProfile.setSituation(SystemProfileSituationType.PENDING_LOCK.getCode());
				
				historicalState.setCurrentState(systemProfile.getSituation());
				messageModal = PropertiesConstants.SYSTEM_PROFILE_REQUEST_BLOCK_SUCCESS;
				messageNotification = PropertiesConstants.SYSTEM_PROFILE_REQUEST_BLOCK_NOTIFICATION;
				
			}else if(systemProfile.getState().equals(SystemProfileStateType.CONFIRMED.getCode())
					&& systemProfile.getSituation().equals(SystemProfileSituationType.PENDING_LOCK.getCode())){
				systemProfile.setState(SystemProfileStateType.BLOCKED.getCode());
				systemProfile.setSituation(SystemProfileSituationType.ACTIVE.getCode());
				
				historicalState.setCurrentState(systemProfile.getState());
				messageModal = PropertiesConstants.SYSTEM_PROFILE_BLOCK_SUCCESS;
				messageNotification = PropertiesConstants.SYSTEM_PROFILE_BLOCK_NOTIFICATION;
				activeLogoutUsers = true;
			}
			
			systemProfile.setBlockMotive(isRejectOption ? Integer.valueOf(-1) : historicalState.getMotive());
			systemProfile.setBlockOtherMotive(isRejectOption ? "" : historicalState.getMotiveDescription());
			
			ThreadLocalContextHolder.put(
					PropertiesConstants.HISTORICAL_STATE_LOGGER, 
					createHistorialState(
							systemProfile.getClass().getAnnotation(Table.class).name(), 
							SystemProfileStateType.CONFIRMED.getCode(),
							Long.parseLong(systemProfile.getIdSystemProfilePk().toString()), 
							historicalState));
			
			boolean result = systemServiceFacade.updateSystemProfileStateServiceFacade(systemProfile);
			
			if(result){
				
				validateMotiveInputs();
				searchProfilesListener(null);
				Object[] arrBodyData = {
						systemProfile.getName().toUpperCase(),
						systemProfile.getMnemonic().toUpperCase()};					
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_SUCCESS, null,
						messageModal, 
						arrBodyData);
				JSFUtilities.showEndTransactionSamePageDialog();
				//JSFUtilities.hideComponent("frmMotive:cnfMotive");
									
				if(activeLogoutUsers){
					//Code to logout the Active users of this profile 
					List<String> users = systemServiceFacade.getActiveProfileUsersServiceFacade(systemProfile);
					for(String loginUser : users){
						//PushContext pushContext = PushContextFactory.getDefault().getPushContext();
						//integration with notifications
						BrowserWindow browser = new BrowserWindow();
						browser.setNotificationType(NotificationType.KILLSESSION.getCode());
						//TODO change hardcode message
						browser.setSubject("Expire Session");
						browser.setMessage(PropertiesConstants.SESSION_EXPIRED_SYSTEM_PROFILE_BLOCKED);
						log.info("calling push method");
						//pushContext.push(PropertiesConstants.PUSH_MSG+"/"+loginUser.trim(),browser);
					}
				}
			}		
			}
			systemProfile = null;	
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * 1
	 * This Method will show the dialog of motives of unblock.
	 *
	 * @param actionEvent the action event
	 */
	public void beginUnblockListener(ActionEvent actionEvent){
		hideDialogsListener(null);
		historicalState = new HistoricalState();
		historicalState.setIdHistoricalStatePk(Long.valueOf(-1));
		showOtherMotive = false;
		isConfirmBockOrUnblock = false;
		isRejectOption = false;
		if(selectedSystemProfile==null){
			//If the system profile was not selected, an error message will be shown
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.ERROR_RECORD_NO_SELECTED, null);
			JSFUtilities.addContextMessage(null,
					FacesMessage.SEVERITY_ERROR,"","");
			JSFUtilities.showValidationDialog();
			return;
		} else if(!SystemProfileStateType.BLOCKED.getCode().equals(selectedSystemProfile.getState())){
			//If the state of the selected system profile is "REGISTERED", an error message will be shown
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT, null, 
					PropertiesConstants.SYSTEM_PROFILE_UNBLOCK_ERROR, null);
			JSFUtilities.addContextMessage(null,FacesMessage.SEVERITY_ERROR,"","");
			JSFUtilities.showValidationDialog();
			return;
		} else if(!SystemProfileSituationType.ACTIVE.getCode().equals(selectedSystemProfile.getSituation())
				&& !SystemProfileSituationType.PENDING_UNLOCK.getCode().equals(selectedSystemProfile.getSituation())){
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT,null, 
					"system.profile.error.situation.message", null);
			JSFUtilities.addContextMessage(null,FacesMessage.SEVERITY_ERROR,"","");
			JSFUtilities.showValidationDialog();
			return;
		}
		loadSelectedProfile();
		this.setOperationType(ViewOperationsType.UNBLOCK.getCode());
		if(systemProfile.getState().equals(SystemProfileStateType.BLOCKED.getCode())
			&& systemProfile.getSituation().equals(SystemProfileSituationType.PENDING_UNLOCK.getCode())){
			
			isConfirmBockOrUnblock = true;			
			historicalState.setMotive(systemProfile.getUnblockMotive());
			
			if(OtherMotiveType.SYS_PROFILE_UNBLOCK_OTHER_MOTIVE.getCode().equals(systemProfile.getBlockMotive())){
				showOtherMotive = true;
				historicalState.setMotiveDescription(systemProfile.getUnblockOtherMotive());
			}
		}
		showMessageOnDialog2(PropertiesConstants.DIALOG_HEADER_UNBLOCKMOTIVE, null, null, null);		
		/*JSFUtilities.showComponent("frmMotive:cnfMotive");*/
		JSFUtilities.executeJavascriptFunction("PF('cnfwMotive').show()");
	}

	
	/**
	 * This method unblock the system profile.
	 *
	 * @param actionEvent the action event
	 */
	@LoggerAuditWeb
	public void unblockListener(ActionEvent actionEvent) {
		try{
			hideDialogsListener(null);

			if(systemServiceFacade.getLatsModifiedDetails(systemProfile).after(systemProfile.getLastModifyDate())){
				Object[] arrBodyData = {systemProfile.getName()};
				showMessageOnDialog(null, null,PropertiesConstants.SYSTEM_PROFILE_ERROR_MODIFIED_RECENT,arrBodyData);
				JSFUtilities.showEndTransactionSamePageDialog();
				JSFUtilities.addContextMessage(null,FacesMessage.SEVERITY_ERROR,"","");
				JSFUtilities.showValidationDialog();
				searchProfilesListener(null);
				return;					
			}

			String messageModal = "";
			if(isRejectOption && (systemProfile.getState().equals(SystemProfileStateType.BLOCKED.getCode()) &&
					systemProfile.getSituation().equals(SystemProfileSituationType.PENDING_UNLOCK.getCode()))){					
				systemProfile.setSituation(SystemProfileSituationType.ACTIVE.getCode());
				
				historicalState.setCurrentState(systemProfile.getSituation());
				messageModal = PropertiesConstants.SYSTEM_PROFILE_REJECT_UNBLOCK_SUCCESS;
				
			}else if(systemProfile.getState().equals(SystemProfileStateType.BLOCKED.getCode()) &&
					systemProfile.getSituation().equals(SystemProfileSituationType.ACTIVE.getCode())){					
				systemProfile.setSituation(SystemProfileSituationType.PENDING_UNLOCK.getCode());
				
				historicalState.setCurrentState(systemProfile.getSituation());
				messageModal = PropertiesConstants.SYSTEM_PROFILE_REQUEST_UNBLOCK_SUCCESS;

			}else if(systemProfile.getState().equals(SystemProfileStateType.BLOCKED.getCode()) &&
					systemProfile.getSituation().equals(SystemProfileSituationType.PENDING_UNLOCK.getCode())){
				systemProfile.setState(SystemProfileStateType.CONFIRMED.getCode());
				systemProfile.setSituation(SystemProfileSituationType.ACTIVE.getCode());
				
				historicalState.setCurrentState(systemProfile.getState());
				messageModal = PropertiesConstants.SYSTEM_PROFILE_UNBLOCK_SUCCESS;
			}
			
			systemProfile.setUnblockMotive(historicalState.getMotive()); 
			systemProfile.setUnblockOtherMotive(historicalState.getMotiveDescription());
			
			ThreadLocalContextHolder.put(
					PropertiesConstants.HISTORICAL_STATE_LOGGER, 
					createHistorialState(
							systemProfile.getClass().getAnnotation(Table.class).name(), 
							SystemProfileStateType.BLOCKED.getCode(),
							Long.parseLong(systemProfile.getIdSystemProfilePk().toString()), 
							historicalState));
			
			//Unblock the profile on database
			boolean result = systemServiceFacade.updateSystemProfileStateServiceFacade(systemProfile);
            
			if(result){
				
				validateMotiveInputs();
				//If the state of the profile was update to "REGISTERED" without problems, a successful message will be shown
				//JSFUtilities.hideComponent("frmMotive:cnfMotive");
				JSFUtilities.executeJavascriptFunction("PF('cnfwMotive').hide()");
				searchProfilesListener(null);				
				Object[] arrBodyData = {systemProfile.getName(),systemProfile.getMnemonic()};
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_SUCCESS, null,
						messageModal, 
						arrBodyData);
				JSFUtilities.showEndTransactionSamePageDialog();
			}
			systemProfile = null;
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Confirm listener.
	 *
	 * @param actionEvent the action event
	 */
	@LoggerAuditWeb
	public void confirmListener(ActionEvent actionEvent){
		try{
			hideDialogsListener(null);
			
			Integer error = checkModifyDateAndStatus(
					"SystemProfile",
					"idSystemProfilePk",
					systemProfile.getIdSystemProfilePk(),
					systemProfile.getLastModifyDate(),
					SystemProfileStateType.CONFIRMED.getCode());
			
			if(Validations.validateIsNotNullAndPositive(error)){
				Object[] arrBodyData = {systemProfile.getName().toUpperCase()};
				if(GeneralConstants.ONE_VALUE_INTEGER.equals(error)){	
					showMessageOnDialog(null, null,PropertiesConstants.SYSTEM_PROFILE_ERROR_MODIFIED_RECENT,arrBodyData);
				}else if(GeneralConstants.TWO_VALUE_INTEGER.equals(error)){	
					showMessageOnDialog(null, null,PropertiesConstants.ERROR_ADM_ALREADY_CONFIRM,arrBodyData);
				}
				
				JSFUtilities.showEndTransactionSamePageDialog();
				JSFUtilities.addContextMessage(null,FacesMessage.SEVERITY_ERROR,"","");
				JSFUtilities.showValidationDialog();
				searchProfilesListener(null);
				return;
			}
			
			systemProfile.setState(SystemProfileStateType.CONFIRMED.getCode());
			systemProfile.setSituation(SystemProfileSituationType.ACTIVE.getCode());
			
			historicalState.setCurrentState(SystemProfileStateType.CONFIRMED.getCode());
			historicalState.setMotive(Integer.valueOf(-1));

			ThreadLocalContextHolder.put(
					PropertiesConstants.HISTORICAL_STATE_LOGGER, 
					createHistorialState(
							systemProfile.getClass().getAnnotation(Table.class).name(), 
							SystemProfileStateType.REGISTERED.getCode(),
							Long.parseLong(systemProfile.getIdSystemProfilePk().toString()), 
							historicalState));

			boolean result = systemServiceFacade.confirmSystemProfileServiceFacade(systemProfile);
			
			if(result){
				searchProfilesListener(null);
				
				Object[] arrBodyData = {
						systemProfile.getName().toUpperCase(),
						systemProfile.getMnemonic().toUpperCase()};
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_SUCCESS, null,
						PropertiesConstants.SYSTEM_PROFILE_CONFIRM_SUCCESS, 
						arrBodyData);
				JSFUtilities.showEndTransactionSamePageDialog();				
			}
			systemProfile = null;		
			
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Modify confirmed listener.
	 *
	 * @param actionEvent the action event
	 */
	@LoggerAuditWeb
	public void modifyConfirmedListener(ActionEvent actionEvent){
		try {
			hideDialogsListener(null);
			if(systemProfile != null && systemServiceFacade.getLatsModifiedDetails(systemProfile).after(systemProfile.getLastModifyDate())){
				Object[] arrBodyData = {systemProfile.getName()};
				showMessageOnDialog(null, null,PropertiesConstants.SYSTEM_PROFILE_ERROR_MODIFIED_RECENT,arrBodyData);
				JSFUtilities.addContextMessage(null,FacesMessage.SEVERITY_ERROR,"","");
				JSFUtilities.showValidationDialog();
				searchProfilesListener(null);
				return;
			}
			if(systemProfile!=null){
				SystemProfileRequest profileRequest = systemServiceFacade.gerRequestProfileRequestDetail(systemProfile);
				String message =null;
				if(!isRejectOption){
					systemServiceFacade.confirmModifyProfile(profileRequest,systemProfile);
					message = "system.profile.modification.notification";
				} else {
					systemServiceFacade.rejectModifyProfile(profileRequest);
					message ="system.profile.reject.modification.success";
				}
				Object[] arrBodyData = {
						profileRequest.getProfileName().toUpperCase()};					
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_SUCCESS, null,message,arrBodyData);
				JSFUtilities.showEndTransactionSamePageDialog();
				searchProfilesListener(null);
			}
			
		}catch(ServiceException sex){
			if(sex.getMessage()!=null){
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION,sex.getErrorService().getMessage());
				JSFUtilities.showSimpleValidationDialog();
				searchProfilesListener(null);
			}
		}catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		isRejectOption = false;
		/*JSFUtilities.hideComponent("frmModification:dlgModification");*/
		JSFUtilities.executeJavascriptFunction("PF('dlgModificationWidget').hide()");
	}
	
	/**
	 * Delete listener.
	 *
	 * @param actionEvent the action event
	 */
	@LoggerAuditWeb
	public void deleteListener(ActionEvent actionEvent){
		try{
			hideDialogsListener(null);
			if(systemProfile != null){
			if(systemServiceFacade.getLatsModifiedDetails(systemProfile).after(systemProfile.getLastModifyDate())){
				Object[] arrBodyData = {systemProfile.getName()};
				showMessageOnDialog(null, null,PropertiesConstants.SYSTEM_PROFILE_ERROR_MODIFIED_RECENT,arrBodyData);
				JSFUtilities.addContextMessage(null,FacesMessage.SEVERITY_ERROR,"","");
				JSFUtilities.showValidationDialog();
				searchProfilesListener(null);
				return;
			}
			String messageModal = "";
			boolean activeLogoutUsers = false;
			if(isRejectOption && (systemProfile.getState().equals(SystemProfileStateType.CONFIRMED.getCode())
					&&systemProfile.getSituation().equals(SystemProfileSituationType.PENDING_DELETE.getCode()))){
				systemProfile.setSituation(SystemProfileSituationType.ACTIVE.getCode());
				historicalState.setCurrentState(systemProfile.getSituation());
				systemProfile.setRejectProfile(true);
				messageModal = "system.profile.reject.delete.success";
			}else if(systemProfile.getState().equals(SystemProfileStateType.CONFIRMED.getCode())
					&& systemProfile.getSituation().equals(SystemProfileSituationType.ACTIVE.getCode())){
				systemProfile.setSituation(SystemProfileSituationType.PENDING_DELETE.getCode());
				historicalState.setCurrentState(systemProfile.getSituation());
				messageModal = "system.profile.request.delete.success";
			}else if(systemProfile.getState().equals(SystemProfileStateType.CONFIRMED.getCode())
					&& systemProfile.getSituation().equals(SystemProfileSituationType.PENDING_DELETE.getCode())){
				systemProfile.setState(SystemProfileStateType.DELETED.getCode());
				systemProfile.setSituation(SystemProfileSituationType.ACTIVE.getCode());
				historicalState.setCurrentState(systemProfile.getState());
				messageModal = "system.profile.delete.success";
				activeLogoutUsers = true;
			}
			systemProfile.setDeleteOtherMotive(isRejectOption ? "" : historicalState.getMotiveDescription());
			historicalState.setMotive(-1);
			ThreadLocalContextHolder.put(
					PropertiesConstants.HISTORICAL_STATE_LOGGER, 
					createHistorialState(
							systemProfile.getClass().getAnnotation(Table.class).name(), 
							SystemProfileStateType.CONFIRMED.getCode(),
							Long.parseLong(systemProfile.getIdSystemProfilePk().toString()),historicalState));
			boolean result = systemServiceFacade.updateSystemProfileStateServiceFacade(systemProfile);
			if(result){
				validateMotiveInputs();
				searchProfilesListener(null);
				Object[] arrBodyData = {
						systemProfile.getName().toUpperCase(),
						systemProfile.getMnemonic().toUpperCase()};					
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_SUCCESS, null,
						messageModal, 
						arrBodyData);
				JSFUtilities.showEndTransactionSamePageDialog();
				/*JSFUtilities.hideComponent("frmMotive:cnfMotive");*/
				JSFUtilities.executeJavascriptFunction("PF('cnfwMotive').hide()");
				if(activeLogoutUsers){
					//Code to logout the Active users of this profile 
					List<String> users = systemServiceFacade.getActiveProfileUsersServiceFacade(systemProfile);
					for(String loginUser : users){
						PushContext pushContext = PushContextFactory.getDefault().getPushContext();
						//integration with notifications
						BrowserWindow browser = new BrowserWindow();
						browser.setNotificationType(NotificationType.KILLSESSION.getCode());
						//TODO change hardcode message
						browser.setSubject("Expire Session");
						browser.setMessage(PropertiesConstants.SESSION_EXPIRED_SYSTEM_PROFILE_BLOCKED);
						log.info("calling push method");
						//pushContext.push(PropertiesConstants.PUSH_MSG+"/"+loginUser.trim(),browser);
					}
				}
			}
			}
			systemProfile = null;	
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Reject listener.
	 *
	 * @param actionEvent the action event
	 */
	@LoggerAuditWeb
	public void rejectListener(ActionEvent actionEvent){
		try{
			hideDialogsListener(null);
			
			Integer error = checkModifyDateAndStatus(
					"SystemProfile",
					"idSystemProfilePk",
					systemProfile.getIdSystemProfilePk(),
					systemProfile.getLastModifyDate(),
					SystemProfileStateType.REJECTED.getCode());
			
			if(Validations.validateIsNotNullAndPositive(error)){
				Object[] arrBodyData = {systemProfile.getName().toUpperCase()};
				if(GeneralConstants.ONE_VALUE_INTEGER.equals(error)){	
					showMessageOnDialog(null, null,PropertiesConstants.SYSTEM_PROFILE_ERROR_MODIFIED_RECENT,arrBodyData);
				}else if(GeneralConstants.TWO_VALUE_INTEGER.equals(error)){	
					showMessageOnDialog(null, null,PropertiesConstants.ERROR_ADM_ALREADY_REJECT,arrBodyData);
				}
				
				JSFUtilities.showEndTransactionSamePageDialog();
				JSFUtilities.addContextMessage(null,FacesMessage.SEVERITY_ERROR,"","");
				JSFUtilities.showValidationDialog();
				searchProfilesListener(null);
				return;
			}
			String messageModal = "";
			String messageNotification = "";
			boolean activeLogoutUsers = false;
			if(isRejectOption && (systemProfile.getState().equals(SystemProfileStateType.REGISTERED.getCode())
					&& systemProfile.getSituation() != null
					&&systemProfile.getSituation().equals(SystemProfileSituationType.PENDING_REJECTED.getCode()))){
				systemProfile.setSituation(null);
				historicalState.setCurrentState(systemProfile.getSituation());
				messageModal = "system.profile.reject.rejected.success";
//				messageNotification = PropertiesConstants.SYSTEM_PROFILE_REJECT_BLOCK_NOTIFICATION;
			}else if(systemProfile.getState().equals(SystemProfileStateType.REGISTERED.getCode())
					&& systemProfile.getSituation() == null){
				systemProfile.setSituation(SystemProfileSituationType.PENDING_REJECTED.getCode());
				historicalState.setCurrentState(systemProfile.getSituation());
				messageModal = "system.profile.reject.register.success";
//				messageNotification = PropertiesConstants.SYSTEM_PROFILE_REQUEST_BLOCK_NOTIFICATION;
			}else if(systemProfile.getState().equals(SystemProfileStateType.REGISTERED.getCode())
					&& systemProfile.getSituation() != null
					&& systemProfile.getSituation().equals(SystemProfileSituationType.PENDING_REJECTED.getCode())){
				systemProfile.setState(SystemProfileStateType.REJECTED.getCode());
				systemProfile.setSituation(SystemProfileSituationType.ACTIVE.getCode());
				historicalState.setCurrentState(systemProfile.getState());
				messageModal = "system.profile.reject.confirm.success";
//				messageNotification = PropertiesConstants.SYSTEM_PROFILE_BLOCK_NOTIFICATION;
				activeLogoutUsers = true;
			}
			systemProfile.setRejectMotive(isRejectOption ? Integer.valueOf(-1) : historicalState.getMotive()); 
			systemProfile.setRejectOtherMotive(isRejectOption ? "" : historicalState.getMotiveDescription());
			historicalState.setCurrentState(SystemProfileStateType.REGISTERED.getCode());

			ThreadLocalContextHolder.put(
					PropertiesConstants.HISTORICAL_STATE_LOGGER, 
					createHistorialState(
							systemProfile.getClass().getAnnotation(Table.class).name(), 
							SystemProfileStateType.REGISTERED.getCode(),
							Long.parseLong(systemProfile.getIdSystemProfilePk().toString()), 
							historicalState));
			
			boolean result = systemServiceFacade.updateSystemProfileStateServiceFacade(systemProfile);
				if(result){
				validateMotiveInputs();
				searchProfilesListener(null);
				Object[] arrBodyData = {
						systemProfile.getName().toUpperCase(),
						systemProfile.getMnemonic().toUpperCase()};					
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_SUCCESS, null,
						messageModal, 
						arrBodyData);
				JSFUtilities.showEndTransactionSamePageDialog();
				//JSFUtilities.hideComponent("frmMotive:cnfMotive");
				JSFUtilities.executeJavascriptFunction("PF('cnfwMotive').hide()");
			}		   

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Expand the privileges of the Tree on page.
	 *
	 * @param actionEvent the action event
	 */
	public void expandPrivilegesListener(ActionEvent actionEvent){	
		//expand each node in recursive way
		expandPrivilegeRecursive(rootPrivileges);
		expandedNodes = true;
	}
	
	/**
	 * Expand each privilege node in recursive way.
	 *
	 * @param node the node
	 */
	public void expandPrivilegeRecursive(TreeNode node){
		//Expand the current node
		node.setExpanded(true);
		if(node.getChildCount()>0){
			List<TreeNode> lstChildNode = node.getChildren();
			//For each child node of the current node, call the method expandPrivilegeRecursive
			for (TreeNode childNode : lstChildNode) {
				expandPrivilegeRecursive(childNode);
			}
		}		
	}
	
	/**
	 * Collapse the privileges of the Tree on page.
	 *
	 * @param actionEvent the action event
	 */
	public void collapsePrivilegesListener(ActionEvent actionEvent){
		//collapse each node in recursive way
		collapsePrivilegeRecursive(rootPrivileges);
		expandedNodes = false;
	}
	
	/**
	 * Collapse each privilege node in recursive way.
	 *
	 * @param node the node
	 */
	public void collapsePrivilegeRecursive(TreeNode node){
		//Collapse the current node
		node.setExpanded(false);
		if(node.getChildCount()>0){
			List<TreeNode> lstChildNode = node.getChildren();
			//For each child node of the current node, call the method collapsePrivilegeRecursive
			for (TreeNode childNode : lstChildNode) {
				collapsePrivilegeRecursive(childNode);
			}
		}
	}
	
	/**
	 * This method is called in order to show the detail of the System Profile.
	 *
	 * @return the string
	 */
	public String detailAction(){		
		rootPrivileges = new DefaultTreeNode("Root", null);
		loadProfilePrivilegesForDetail();
		return "systemProfileDetailRule";
	}
	
	/**
	 * Detail request modify.
	 *
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String detailRequestModify() throws ServiceException{
		loadSelectedProfile();
		systemProfileFilter.setIdSystem(systemProfile.getSystem().getIdSystemPk());
		systemProfileFilter.setIdLanguage(LanguageType.SPANISH.getCode());
		systemProfileFilter.setSystemOptionState(OptionStateType.CONFIRMED.getCode());
		systemProfileFilter.setOptionPrivilegeState(OptionPrivilegeStateType.REGISTERED.getCode());
		systemProfileFilter.setProfilePrivilegeState(ProfilePrivilegeStateType.REGISTERED.getCode());
		systemProfileFilter.setIdSystemProfile(systemProfile.getIdSystemProfilePk());
		SystemProfileRequest profileRequest =  systemServiceFacade.gerRequestProfileRequestDetail(systemProfile);
		rootPrivileges = new DefaultTreeNode("Root", null);
		List<Object[]> lstProfilePrivileges = new ArrayList<>();
		if(profileRequest!=null){
			systemProfile.setName(profileRequest.getProfileName());
			systemProfile.setDescription(profileRequest.getProfileDescription());
			for(ProfilePrivilegeRequest privilefeRequest : profileRequest.getProfilePrivilegeRequests()){
				Object[] row = {privilefeRequest.getOptionPrivilege().getIdOptionPrivilegePk(),
						privilefeRequest.getOptionPrivilege().getIdPrivilege(),privilefeRequest.getOptionPrivilege().getSystemOption().getIdSystemOptionPk()};
				lstProfilePrivileges.add(row);
			}
			//sorting
			Collections.sort(lstProfilePrivileges, new Comparator<Object[]>() {
		        @Override
		        public int compare(Object[]  row1, Object[]  row2)
		        {

		            return  ((Integer)row1[2]).compareTo((Integer)row2[2]);
		        }
		    });
		}
//		
//		List<Object[]> lstProfilePrivileges = systemServiceFacade.listOptionPrivilegesByProfileServiceFacade(systemProfileFilter);
		if(!lstProfilePrivileges.isEmpty()){
			List<Integer> lstIdOptionPrivilege = new ArrayList<Integer>();
			for (Object[] arrPrivilegeRow : lstProfilePrivileges) {
				lstIdOptionPrivilege.add(Integer.valueOf(arrPrivilegeRow[0].toString()));
			}
			systemProfileFilter.setLstIdOptionPrivilege(lstIdOptionPrivilege);
			List<Integer> lstIdSystemOptions  = systemServiceFacade.listIdSystemOptiosByOptionPrivilegeServiceFacade(systemProfileFilter);
			systemProfileFilter.setLstIdSystemOption(lstIdSystemOptions);
			List<Object[]> lstSystemOptions = systemServiceFacade.listSystemOptionsByIdsServiceFacade(systemProfileFilter);		
			List<Object[]> lstParentSystemOptions = getParentSystemOptions(lstSystemOptions);
			for (Object[] arrRow : lstParentSystemOptions) {
				loadPrivilegeOptionsRecursive(arrRow,lstSystemOptions, rootPrivileges);
			}
			if(rootPrivileges.getChildCount() > 0) {
				for (Object[] arrPrivilegeRow : lstProfilePrivileges) {
					loadPrivilegeNodeRecursive(arrPrivilegeRow,rootPrivileges);
				}
				collapsePrivilegesListener(null);
			}
		}
		return "systemProfileDetailRule";
	}
	
	/**
	 * Return to the administration page.
	 *
	 * @return the string
	 */
	public String returnAction(){
		selectedSystemProfile = null;
		systemProfile = null;
		return "systemProfileSearchRule";
	}

	/**
	 * Load profile privileges for modify.
	 */
	private void loadProfilePrivilegesForModify() {
		try{
			loadPrivilegesBySystemListener();			
			if(rootPrivileges != null){
				if(rootPrivileges.getChildCount()>0){
					systemProfileFilter.setIdSystemProfile(systemProfile.getIdSystemProfilePk());
					systemProfileFilter.setProfilePrivilegeState(ProfilePrivilegeStateType.REGISTERED.getCode());
					
					List<ProfilePrivilege> profilePrivileges = systemServiceFacade.listPrivilegesByProfileServiceFacade(systemProfileFilter);
					
					systemProfile.setProfilePrivileges(profilePrivileges);
					
					if(!profilePrivileges.isEmpty()){					
						for (ProfilePrivilege profilePrivilege : profilePrivileges) {
							checkPrivilegeSelectionRecursive(profilePrivilege,rootPrivileges);					
						}					
					}
					
				}
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Load privileges by system listener.
	 */
	public void loadPrivilegesBySystemListener() {
		
		try{
		hideDialogsListener(null);
		systemProfileFilter.setIdSystem(systemProfile.getSystem().getIdSystemPk());
		if(LanguageType.SPANISH.getValue().equals(userInfo.getCurrentLanguage())){
			systemProfileFilter.setIdLanguage(LanguageType.SPANISH.getCode());
		}else if(LanguageType.ENGLISH.getValue().equals(userInfo.getCurrentLanguage())){
			systemProfileFilter.setIdLanguage(LanguageType.ENGLISH.getCode());
		}else if(LanguageType.FRANCES.getValue().equals(userInfo.getCurrentLanguage())){
			systemProfileFilter.setIdLanguage(LanguageType.FRANCES.getCode());
		}
		systemProfileFilter.setSystemOptionState(OptionStateType.CONFIRMED.getCode());
		systemProfileFilter.setOptionPrivilegeState(OptionPrivilegeStateType.REGISTERED.getCode());
		
		List<Object[]> lstSystemOptions = systemServiceFacade.listOptionSystemsByLanguageServiceFacade(systemProfileFilter);
		
		if(lstSystemOptions.isEmpty()){
			JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
			rootPrivileges = null;			
			JSFUtilities.addContextMessage("frmProfileMgmt:cboSystem",
					FacesMessage.SEVERITY_ERROR,
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_PROFILE_PRIVILEGES_NO_EXIST),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_PROFILE_PRIVILEGES_NO_EXIST));
			JSFUtilities.showEndTransactionSamePageDialog();
			JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.ERROR_ADM_PROFILE_PRIVILEGES_NO_EXIST, null);
			return;
		}
			
		rootPrivileges = new DefaultTreeNode("Root", null);			
		
		List<Object[]> lstParentSystemOptions = getParentSystemOptions(lstSystemOptions);		
		
		for (Object[] arrRow : lstParentSystemOptions) {
			loadPrivilegeOptionsRecursive(arrRow,lstSystemOptions, rootPrivileges);
		}
		
		if(rootPrivileges.getChildCount()>0){
			List<Object[]> lstOptionsPrivileges = systemServiceFacade.listOptionPrivilegesServiceFacade(systemProfileFilter);
			
			if(lstOptionsPrivileges.isEmpty()){
				rootPrivileges = null;
				JSFUtilities.addContextMessage("frmProfileMgmt:cboSystem",
						FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_PROFILE_PRIVILEGES_NO_EXIST),
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_PROFILE_PRIVILEGES_NO_EXIST));
				JSFUtilities.showEndTransactionSamePageDialog();
				JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.ERROR_ADM_PROFILE_PRIVILEGES_NO_EXIST, null);
				return;
			}
			
			for (Object[] arrOptionPrivilegeRow : lstOptionsPrivileges) {
				loadPrivilegeNodeRecursive(arrOptionPrivilegeRow,rootPrivileges);
			}
			
			collapsePrivilegesListener(null);
		} else {
			JSFUtilities.addContextMessage("frmProfileMgmt:cboSystem",
					FacesMessage.SEVERITY_ERROR,
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_PROFILE_PRIVILEGES_NO_EXIST),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_PROFILE_PRIVILEGES_NO_EXIST));
			JSFUtilities.showEndTransactionSamePageDialog();
			JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.ERROR_ADM_PROFILE_PRIVILEGES_NO_EXIST, null);
		}
		
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
							
	}
	
	/**
	 * Load privilege node recursive.
	 *
	 * @param arrPrivilegeRow the arr privilege row
	 * @param parentNode the parent node
	 */
	private void loadPrivilegeNodeRecursive(Object[] arrPrivilegeRow, TreeNode parentNode){
		
		if(parentNode.getData() instanceof OptionPrivilegeNodeBean){
			OptionPrivilegeNodeBean dataNodoOpcion = (OptionPrivilegeNodeBean) parentNode.getData();
			
			if(dataNodoOpcion.getIdSystemOption()!=null){
				
				Integer idOpcionSistemaFkPrivilegio = Integer.valueOf(arrPrivilegeRow[2].toString());
				
				if(dataNodoOpcion.getIdSystemOption().intValue() == idOpcionSistemaFkPrivilegio.intValue()){
					OptionPrivilegeNodeBean nodoOpcionPrivilegioBean = new OptionPrivilegeNodeBean();
					nodoOpcionPrivilegioBean.setIdOptionPrivilege(Integer.valueOf(arrPrivilegeRow[0].toString()));
					nodoOpcionPrivilegioBean.setIdPrivilege(Integer.valueOf(arrPrivilegeRow[1].toString()));
					nodoOpcionPrivilegioBean.setName(PrivilegeType.get(Integer.valueOf(arrPrivilegeRow[1].toString())).getDescription());
					
					new DefaultTreeNode(nodoOpcionPrivilegioBean, parentNode);
				} else{
					if(parentNode.getChildCount()>0){
						for (TreeNode childNode : parentNode.getChildren()) {
							loadPrivilegeNodeRecursive(arrPrivilegeRow, childNode);
						}
					}
				}
			}
									
		} else {
			if(parentNode.getChildCount()>0){
				for (TreeNode childNode : parentNode.getChildren()) {
					loadPrivilegeNodeRecursive(arrPrivilegeRow, childNode);
				}
			}
		}
	}

	/**
	 * Check privilege selection recursive.
	 *
	 * @param profilePrivilege the profile privilege
	 * @param node the node
	 */
	private void checkPrivilegeSelectionRecursive(ProfilePrivilege profilePrivilege, TreeNode node){
		
		if(node.getData() instanceof OptionPrivilegeNodeBean){
			OptionPrivilegeNodeBean optionPrivilegeNodeBean = (OptionPrivilegeNodeBean) node.getData();
			
			if(optionPrivilegeNodeBean.getIdOptionPrivilege()!=null){
																				
				if(optionPrivilegeNodeBean.getIdOptionPrivilege().intValue() == profilePrivilege.getOptionPrivilege().getIdOptionPrivilegePk()){
					optionPrivilegeNodeBean.setIdProfilePrivilege(profilePrivilege.getIdProfilePrivilegePk());
					node.setSelected(true);
					validateSelectedParentRecursive(node);
					return;
				}
				
			} else {
				if(node.getChildCount()>0){
					for (TreeNode childNode : node.getChildren()) {
						checkPrivilegeSelectionRecursive(profilePrivilege, childNode);
					}
				}
			}
									
		} else {
			if(node.getChildCount()>0){
				for (TreeNode childNode : node.getChildren()) {
					checkPrivilegeSelectionRecursive(profilePrivilege, childNode);
				}
			}
		}
	}
	
	/**
	 * Validate selected parent recursive.
	 *
	 * @param node the node
	 */
	private void validateSelectedParentRecursive(TreeNode node){
		if(node.getParent() instanceof DefaultTreeNode){
			TreeNode parentNode = node.getParent();
			List<TreeNode> lstChildNodes = parentNode.getChildren();
			int childNodeQuantity = parentNode.getChildCount();
			int selectedChildNodeQuantity = 0;
			
			for (TreeNode childNode : lstChildNodes) {
				if(childNode.isSelected()){
					selectedChildNodeQuantity++;
				}
			}
			
			if(selectedChildNodeQuantity==childNodeQuantity){
				parentNode.setSelected(true);
			}
			
			validateSelectedParentRecursive(parentNode);
			
		}
	}
	
	/**
	 * Load privilege options recursive.
	 *
	 * @param arrRow the arr row
	 * @param lstSystemOptions the lst system options
	 * @param parentNode the parent node
	 */
	private void loadPrivilegeOptionsRecursive(
		Object[] arrRow, List<Object[]> lstSystemOptions, TreeNode parentNode){
		
		Integer idSystemOption = null;
		Integer idParentSystemOption = null;
		
		OptionPrivilegeNodeBean optionPrivilegeNodeBean = new OptionPrivilegeNodeBean();		
				
		if(arrRow[0]!=null){
			idSystemOption = Integer.valueOf(arrRow[0].toString());
			optionPrivilegeNodeBean.setIdSystemOption(idSystemOption);
		}
		
		if(arrRow[1]!=null){
			idParentSystemOption = Integer.valueOf(arrRow[1].toString());
			optionPrivilegeNodeBean.setIdParentSystemOption(idParentSystemOption);
		}
		

		if(arrRow[2]!=null){
			optionPrivilegeNodeBean.setName(arrRow[2].toString());
		}
		
		TreeNode node = new DefaultTreeNode(optionPrivilegeNodeBean, parentNode);

			Integer idParentSystemOptionFromChild = null;
			
			for (Object[] objArrFilaHija : lstSystemOptions) {
				if(objArrFilaHija[1]!=null){
					idParentSystemOptionFromChild = Integer.valueOf(objArrFilaHija[1].toString());				
					
					if(idSystemOption.intValue()==idParentSystemOptionFromChild.intValue()){						
					   loadPrivilegeOptionsRecursive(objArrFilaHija, lstSystemOptions, node);					   					   
					}
					
				}
			}
				
	}
	
	/**
	 * Load profile privileges for detail.
	 */
	public void loadProfilePrivilegesForDetail() {
		try{
			lstInstitutionType = new ArrayList<Parameter>();	
			lstInstitutionType.addAll(lstInstitutionTypeFilter);
			systemProfileFilter.setIdSystem(systemProfile.getSystem().getIdSystemPk());
			systemProfileFilter.setIdLanguage(LanguageType.SPANISH.getCode());
			systemProfileFilter.setSystemOptionState(OptionStateType.CONFIRMED.getCode());
			systemProfileFilter.setOptionPrivilegeState(OptionPrivilegeStateType.REGISTERED.getCode());
			systemProfileFilter.setProfilePrivilegeState(ProfilePrivilegeStateType.REGISTERED.getCode());
			systemProfileFilter.setIdSystemProfile(systemProfile.getIdSystemProfilePk());						
			
			List<Object[]> lstProfilePrivileges = systemServiceFacade.listOptionPrivilegesByProfileServiceFacade(systemProfileFilter);
			
			if(!lstProfilePrivileges.isEmpty()){
				List<Integer> lstIdOptionPrivilege = new ArrayList<Integer>();
				
				for (Object[] arrPrivilegeRow : lstProfilePrivileges) {
					lstIdOptionPrivilege.add(Integer.valueOf(arrPrivilegeRow[0].toString()));
				}
				
				systemProfileFilter.setLstIdOptionPrivilege(lstIdOptionPrivilege);
				
				List<Integer> lstIdSystemOptions  = systemServiceFacade.listIdSystemOptiosByOptionPrivilegeServiceFacade(systemProfileFilter);
				
				systemProfileFilter.setLstIdSystemOption(lstIdSystemOptions);
				
				
				List<Object[]> lstSystemOptions = systemServiceFacade.listSystemOptionsByIdsServiceFacade(systemProfileFilter);		
				
				rootPrivileges = new DefaultTreeNode("Root", null);			
				
				List<Object[]> lstParentSystemOptions = getParentSystemOptions(lstSystemOptions);
				
				for (Object[] arrRow : lstParentSystemOptions) {
					loadPrivilegeOptionsRecursive(arrRow,lstSystemOptions, rootPrivileges);
				}
				
				if(rootPrivileges.getChildCount() > 0) {
					
					for (Object[] arrPrivilegeRow : lstProfilePrivileges) {
						loadPrivilegeNodeRecursive(arrPrivilegeRow,rootPrivileges);
					}
					
					collapsePrivilegesListener(null);
				}
				
			}
		
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
							
	}
	
	/**
	 * Gets the parent system options.
	 *
	 * @param listaOpcioncesSistema the lista opcionces sistema
	 * @return the parent system options
	 */
	private List<Object[]> getParentSystemOptions(List<Object[]> listaOpcioncesSistema){		
		List<Object[]> lstParentSystemOptions = new ArrayList<Object[]>();
					
		for (Object[] arrRow : listaOpcioncesSistema) {
			if (arrRow[1] == null ) {				
				lstParentSystemOptions.add(arrRow);
			}
		}
		
		return lstParentSystemOptions;
	}
	
	/**
	 * Begin save listener.
	 *
	 * @param actionEvent the action event
	 */
	public void beginSaveListener(ActionEvent actionEvent){
		try{
			hideDialogsListener(null);
			boolean validationFail = false;
			if(Validations.validateIsNullOrEmpty(systemProfile.getMnemonic())){
				JSFUtilities.addContextMessage("frmProfileMgmt:txtMnemonic",
						FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_INSERT_PROFILE_MNEMONIC),
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_INSERT_PROFILE_MNEMONIC));
				validationFail = true;
			}
			if(Validations.validateIsNullOrEmpty(systemProfile.getName())){
				JSFUtilities.addContextMessage("frmProfileMgmt:txtName",
						FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_INSERT_PROFILE_NAME),
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_INSERT_PROFILE_NAME));
				validationFail = true;
			}
			if(Validations.validateIsNullOrEmpty(systemProfile.getDescription())){
				JSFUtilities.addContextMessage("frmProfileMgmt:txtProfileDescription",
						FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_INSERT_PROFILE_DESCRIPTION),
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_INSERT_PROFILE_DESCRIPTION));
				validationFail = true;
			}
			boolean isSelectedPrivileges = validateSelectedPrivileges();
						
			if(!isSelectedPrivileges) {
				JSFUtilities.addContextMessage("frmProfileMgmt:treePrivileges",
						FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_PROFILE_PRIVILEGES_NO_SELECTED),
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_PROFILE_PRIVILEGES_NO_SELECTED));
				
				
				JSFUtilities.executeJavascriptFunction(
						"document.getElementById('frmProfileMgmt:divPrivileges').style.border='1px solid #CD0A0A'");
				validationFail = true;
			} else {
				JSFUtilities.executeJavascriptFunction(
					"document.getElementById('frmProfileMgmt:divPrivileges').style.border=''");
			}
			if(validationFail){
				JSFUtilities.removeRequestMap("executeAction");
				JSFUtilities.showRequiredDialog();
				return;
			}
				if(ViewOperationsType.REGISTER.getCode().equals(getOperationType())){
					Object[] arrBodyData = {systemProfile.getName()};			
					
					showMessageOnDialog(
							PropertiesConstants.DIALOG_HEADER_REGISTER,
							null,
							PropertiesConstants.ADM_PROFILLE_LBL_CONFIRM_REGISTER,
							arrBodyData);
					JSFUtilities.showComponent("frmProfileMgmt:cnfProfileMgmt");	
				} else{
						Object[] arrBodyData = {systemProfile.getName(),systemProfile.getMnemonic()};
						
						showMessageOnDialog(
								PropertiesConstants.DIALOG_HEADER_MODIFY,
								null,
								PropertiesConstants.ADM_PROFILE_LBL_CONFIRM_MODIFY,							
								arrBodyData );
						JSFUtilities.showComponent("frmProfileMgmt:cnfProfileMgmt");
				}
		
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		
	}
	
	
	/**
	 * To validate Mnemonic.
	 */
	public void validateMnemonicListener(){
		if(Validations.validateIsNotNullAndNotEmpty(systemProfile.getMnemonic())){
		hideDialogsListener(null);
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		if(ViewOperationsType.REGISTER.getCode().equals(getOperationType())){
			boolean mnemonicExists = true;
			try{
			mnemonicExists = systemServiceFacade.validateProfileMnemonicServiceFacade(systemProfile);
			}catch (Exception e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
			if(mnemonicExists){
				JSFUtilities.showComponent("frmProfileMgmt:confprofMnemonic");
				//systemProfile.setMnemonic(null);
				JSFUtilities.addContextMessage("frmProfileMgmt:txtMnemonic",
						FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_PROFILE_MNEMONIC_REPEATED),
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_PROFILE_MNEMONIC_REPEATED));
				//JSFUtilities.showComponent("frmProfileMgmt:idprofilevalidation");
				//JSFUtilities.showEndTransactionSamePageDialog();
				//JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.ERROR_ADM_PROFILE_MNEMONIC_REPEATED,null);
			}
		}
		}
	}
	
	/**
	 * Menmonic clear.
	 */
	public void menmonicClear(){
		JSFUtilities.hideComponent("frmProfileMgmt:confprofMnemonic");
		systemProfile.setMnemonic(null);
	}
	
	/**
	 * System name clear.
	 */
	public void systemNameClear(){
		JSFUtilities.hideComponent("frmProfileMgmt:confprofname");
		systemProfile.setName(null);
	}
	
	/**
	 * To validate Profile Name.
	 */
	public void validateProfileNameListener(){
		if(Validations.validateIsNotNullAndNotEmpty(systemProfile.getName())){
		hideDialogsListener(null);
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		boolean nameExists  = true;
		try{
		nameExists = systemServiceFacade.validateProfileNameServiceFacade(systemProfile);
		}catch(Exception e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		if(nameExists){
			JSFUtilities.showComponent("frmProfileMgmt:confprofname");
			//systemProfile.setName(null);
			JSFUtilities.addContextMessage("frmProfileMgmt:txtName",
					FacesMessage.SEVERITY_ERROR,
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_PROFILE_NAME_REPEATED),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_PROFILE_NAME_REPEATED));
			//JSFUtilities.showComponent("frmProfileMgmt:idprofilevalidation");
			//JSFUtilities.showEndTransactionSamePageDialog();
			//JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.ERROR_ADM_PROFILE_NAME_REPEATED, null);
		}	
		}
	}
	
	/**
	 * Validate selected privileges.
	 *
	 * @return true, if successful
	 */
	private boolean validateSelectedPrivileges(){
		if(selectedPrivilegeNodes.length==0){
			return false;
		} else {
			int selectedPrivilegesQuantity = 0;
			OptionPrivilegeNodeBean optionPrivilegeNodeBean = null;
			for (TreeNode selectedNode : selectedPrivilegeNodes) {
				optionPrivilegeNodeBean = (OptionPrivilegeNodeBean) selectedNode.getData();
				
				if(optionPrivilegeNodeBean.getIdOptionPrivilege() != null) {
					selectedPrivilegesQuantity++;
				}
			}
			
			hideDialogsListener(null);

			if(selectedPrivilegesQuantity == 0){
				return false;
			} else {
				return true;
			}
			
		}
	}
	
	/**
	 * Hide dialogs privileges.
	 *
	 * @param actionEvent the action event
	 */
	public void hideDialogsPrivileges(ActionEvent actionEvent){
		isRejectOption = false;
		hideDialogsListener(null);
		/*JSFUtilities.hideComponent("frmModification:dlgModification");
		JSFUtilities.hideComponent("frmMotive:cnfMotive");*/
		JSFUtilities.executeJavascriptFunction("PF('dlgModificationWidget').hide()");
		JSFUtilities.executeJavascriptFunction("PF('cnfwMotive').hide()");
	}
	
	/**
	 * Hide dialogs listener.
	 *
	 * @param actionEvent the action event
	 */
	public void hideDialogsListener(ActionEvent actionEvent){
		JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
		JSFUtilities.hideGeneralDialogues();

		/*JSFUtilities.hideComponent("frmConfBlock:cnfBlock");
		JSFUtilities.hideComponent("frmConfUnblock:cnfUnblock");
		JSFUtilities.hideComponent("frmConfConfirm:cnfConfirm");
		JSFUtilities.hideComponent("frmConfReject:cnfReject");
		JSFUtilities.hideComponent("frmProfileMgmt:cnfProfileMgmt");
		JSFUtilities.hideComponent("frmConfDelete:cnfDelete");
		JSFUtilities.hideComponent("frmConfModify:cnfModify");*/
		JSFUtilities.hideComponent("frmProfileMgmt:cnfProfileMgmt");
		//validateMotiveInputs();
	}
	
	/**
	 * Set to valid state to input values of Motive dialog.
	 */
	private void validateMotiveInputs(){
		JSFUtilities.setValidViewComponent("frmMotive:cboMotive");
		JSFUtilities.setValidViewComponent("frmMotive:txtOtherMotive");
		historicalState.setMotive(Integer.valueOf(-1));
		historicalState.setMotiveDescription(null);
		showOtherMotive = false;
		
		if(JSFUtilities.findViewComponent("frmMotive:cboMotive") instanceof UISelectOne){
			UISelectOne cboMotive = (UISelectOne) JSFUtilities.findViewComponent("frmMotive:cboMotive");
					//cboMotive.setValue(Integer.valueOf(-1));	
		}
	}
	
	/**
	 * For Block Descripcion.
	 *
	 * @return the motive descripcion
	 */
	public String getMotiveDescripcion(){
	    String desc = "";
	    if(systemProfile.getBlockMotive() != null){
			if(lstBlockMotives != null && lstBlockMotives.size() > 0 ){
				for(int i=0;i<lstBlockMotives.size();i++){
					if(systemProfile.getBlockMotive().compareTo(lstBlockMotives.get(i).getIdParameterPk()) == 0){
							desc=lstBlockMotives.get(i).getName();
						}
					}
				}
			}
		return desc;
	}	
		
		/**
		 * For unBlock Descripcion.
		 *
		 * @return the motiveun block descripcion
		 */
	public String getMotiveunBlockDescripcion(){
	    String desc = "";
	    if(systemProfile.getUnblockMotive() != null){
			if(lstUnBlockMotives != null && lstUnBlockMotives.size() > 0 ){
				for(int i=0;i<lstUnBlockMotives.size();i++){
					if(systemProfile.getUnblockMotive().compareTo(lstUnBlockMotives.get(i).getIdParameterPk()) == 0){
							desc=lstUnBlockMotives.get(i).getName();
						}
					}
				}
			}
		return desc;
	}	

	/**
	 * Gets the system profile.
	 *
	 * @return the system profile
	 */
	public SystemProfile getSystemProfile() {
		return systemProfile;
	}

	/**
	 * Sets the system profile.
	 *
	 * @param systemProfile the new system profile
	 */
	public void setSystemProfile(SystemProfile systemProfile) {
		this.systemProfile = systemProfile;
	}

	/**
	 * Gets the system profile filter.
	 *
	 * @return the system profile filter
	 */
	public SystemProfileFilter getSystemProfileFilter() {
		return systemProfileFilter;
	}

	/**
	 * Sets the system profile filter.
	 *
	 * @param systemProfileFilter the new system profile filter
	 */
	public void setSystemProfileFilter(SystemProfileFilter systemProfileFilter) {
		this.systemProfileFilter = systemProfileFilter;
	}

	/**
	 * Gets the system profile data model.
	 *
	 * @return the system profile data model
	 */
	public SystemProfileDataModel getSystemProfileDataModel() {
		return systemProfileDataModel;
	}

	/**
	 * Sets the system profile data model.
	 *
	 * @param systemProfileDataModel the new system profile data model
	 */
	public void setSystemProfileDataModel(
			SystemProfileDataModel systemProfileDataModel) {
		this.systemProfileDataModel = systemProfileDataModel;
	}

	/**
	 * Gets the historical state.
	 *
	 * @return the historical state
	 */
	public HistoricalState getHistoricalState() {
		return historicalState;
	}

	/**
	 * Sets the historical state.
	 *
	 * @param historicalState the new historical state
	 */
	public void setHistoricalState(HistoricalState historicalState) {
		this.historicalState = historicalState;
	}

	/**
	 * Gets the root privileges.
	 *
	 * @return the root privileges
	 */
	public TreeNode getRootPrivileges() {
		return rootPrivileges;
	}

	/**
	 * Sets the root privileges.
	 *
	 * @param rootPrivileges the new root privileges
	 */
	public void setRootPrivileges(TreeNode rootPrivileges) {
		this.rootPrivileges = rootPrivileges;
	}

	/**
	 * Gets the selected privilege nodes.
	 *
	 * @return the selected privilege nodes
	 */
	public TreeNode[] getSelectedPrivilegeNodes() {
		return selectedPrivilegeNodes;
	}

	/**
	 * Sets the selected privilege nodes.
	 *
	 * @param selectedPrivilegeNodes the new selected privilege nodes
	 */
	public void setSelectedPrivilegeNodes(TreeNode[] selectedPrivilegeNodes) {
		this.selectedPrivilegeNodes = selectedPrivilegeNodes;
	}

	/**
	 * Checks if is expanded nodes.
	 *
	 * @return true, if is expanded nodes
	 */
	public boolean isExpandedNodes() {
		return expandedNodes;
	}

	/**
	 * Sets the expanded nodes.
	 *
	 * @param expandedNodes the new expanded nodes
	 */
	public void setExpandedNodes(boolean expandedNodes) {
		this.expandedNodes = expandedNodes;
	}

	/**
	 * Checks if is show other motive.
	 *
	 * @return true, if is show other motive
	 */
	public boolean isShowOtherMotive() {
		return showOtherMotive;
	}

	/**
	 * Sets the show other motive.
	 *
	 * @param showOtherMotive the new show other motive
	 */
	public void setShowOtherMotive(boolean showOtherMotive) {
		this.showOtherMotive = showOtherMotive;
	}

	/**
	 * Gets the selected system profile.
	 *
	 * @return the selectedSystemProfile
	 */
	public SystemProfile getSelectedSystemProfile() {
		return selectedSystemProfile;
	}

	/**
	 * Sets the selected system profile.
	 *
	 * @param selectedSystemProfile the selectedSystemProfile to set
	 */
	public void setSelectedSystemProfile(SystemProfile selectedSystemProfile) {
		this.selectedSystemProfile = selectedSystemProfile;
	}
	
	/**
	 * Checks if is confirm bock or unblock.
	 *
	 * @return true, if is confirm bock or unblock
	 */
	public boolean isConfirmBockOrUnblock() {
		return isConfirmBockOrUnblock;
	}

	/**
	 * Sets the confirm bock or unblock.
	 *
	 * @param isConfirmBockOrUnblock the new confirm bock or unblock
	 */
	public void setConfirmBockOrUnblock(boolean isConfirmBockOrUnblock) {
		this.isConfirmBockOrUnblock = isConfirmBockOrUnblock;
	}

	/**
	 * Cleanup.
	 */
	@PreDestroy
	void cleanup()
	{
		endConversation();
	}

	/**
	 * Gets the lst institution type.
	 *
	 * @return the lst institution type
	 */
	public List<Parameter> getLstInstitutionType() {
		return lstInstitutionType;
	}

	/**
	 * Sets the lst institution type.
	 *
	 * @param lstInstitutionType the new lst institution type
	 */
	public void setLstInstitutionType(List<Parameter> lstInstitutionType) {
		this.lstInstitutionType = lstInstitutionType;
	}

	/**
	 * Checks if is bl ind confirm.
	 *
	 * @return true, if is bl ind confirm
	 */
	public boolean isBlIndConfirm() {
		return blIndConfirm;
	}

	/**
	 * Sets the bl ind confirm.
	 *
	 * @param blIndConfirm the new bl ind confirm
	 */
	public void setBlIndConfirm(boolean blIndConfirm) {
		this.blIndConfirm = blIndConfirm;
	}

	/**
	 * Gets the lst institution type filter.
	 *
	 * @return the lst institution type filter
	 */
	public List<Parameter> getLstInstitutionTypeFilter() {
		return lstInstitutionTypeFilter;
	}

	/**
	 * Sets the lst institution type filter.
	 *
	 * @param lstInstitutionTypeFilter the new lst institution type filter
	 */
	public void setLstInstitutionTypeFilter(List<Parameter> lstInstitutionTypeFilter) {
		this.lstInstitutionTypeFilter = lstInstitutionTypeFilter;
	}

	/**
	 * Gets the situations profile.
	 *
	 * @return the situations profile
	 */
	public Map<Integer,String> getSituationsProfile() {
		return situationsProfile;
	}

	/**
	 * Sets the situations profile.
	 *
	 * @param situationsProfile the situations profile
	 */
	public void setSituationsProfile(Map<Integer,String> situationsProfile) {
		this.situationsProfile = situationsProfile;
	}

	/**
	 * @return the lstProfileType
	 */
	public List<Parameter> getLstProfileType() {
		return lstProfileType;
	}

	/**
	 * @param lstProfileType the lstProfileType to set
	 */
	public void setLstProfileType(List<Parameter> lstProfileType) {
		this.lstProfileType = lstProfileType;
	}
	
}
