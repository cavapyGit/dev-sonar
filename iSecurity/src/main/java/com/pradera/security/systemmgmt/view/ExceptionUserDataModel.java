package com.pradera.security.systemmgmt.view;

import java.io.Serializable;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;
import com.pradera.security.model.ExceptionUserPrivilege;


/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class ExceptionUserDataModel
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.1
 */

public class ExceptionUserDataModel extends ListDataModel<ExceptionUserPrivilege> implements Serializable,SelectableDataModel<ExceptionUserPrivilege> {

    /**
	 * 
	 */
	private static final long serialVersionUID = 6634548741086393832L;

	public ExceptionUserDataModel(){
		
	}
	
	public ExceptionUserDataModel(List<ExceptionUserPrivilege> data){
		super(data);
	}
	
	@Override
	public ExceptionUserPrivilege getRowData(String rowKey) {
		List<ExceptionUserPrivilege> exceptionsUserPrivilegeList=(List<ExceptionUserPrivilege>)getWrappedData();
        for(ExceptionUserPrivilege exceptionUserPrivilege : exceptionsUserPrivilegeList) {  
        	
            if(String.valueOf(exceptionUserPrivilege.getIdExceptionUserPrivilegePk()).equals(rowKey))
                return exceptionUserPrivilege;  
        }  
		return null;
	}

	@Override
	public Object getRowKey(ExceptionUserPrivilege exceptionUserPrivilege) {
		return exceptionUserPrivilege.getIdExceptionUserPrivilegePk();
	}		

}
