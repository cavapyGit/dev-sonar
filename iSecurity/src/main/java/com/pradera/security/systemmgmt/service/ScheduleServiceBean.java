package com.pradera.security.	systemmgmt.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.model.Schedule;
import com.pradera.security.model.ScheduleDetail;
import com.pradera.security.model.System;
import com.pradera.security.model.type.ScheduleStateType;
import com.pradera.security.model.type.ScheduleType;
import com.pradera.security.systemmgmt.view.filters.ScheduleFilter;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Class HorarioServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 22/11/2012
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ScheduleServiceBean extends CrudSecurityServiceBean{
			
	/**
	 * 
	 */
	private static final long serialVersionUID = 8635442755139008370L;
	//This method should be another layer because is a Business Logic's Method
	/**
	 * Checks if is horario disponible service bean.
	 *This method is useful for login, you can see if that schedule
	 *is between the current date
	 * @param horarioParametro the horario parametro
	 * @return true, if is horario disponible service bean
	 */
	@SuppressWarnings("deprecation")
	public boolean isScheduleAvailableServiceBean(Schedule scheduleParameter) throws ServiceException{
		List<ScheduleDetail> scheduleDetailList;
		Schedule schedule ;
			schedule = getScheduleServiceBean(scheduleParameter);
			scheduleDetailList = schedule.getScheduleDetails();	
			
			for(ScheduleDetail scheduleDetail: scheduleDetailList){
				Date currentDay = new Date();
				if(scheduleDetail.getDay().equals(currentDay.getDay())){
					if((scheduleDetail.getInitialDate().getHours() >= currentDay.getHours() && 
					   scheduleDetail.getInitialDate().getMinutes() >= currentDay.getMinutes() &&
					   scheduleDetail.getInitialDate().getSeconds() >= currentDay.getSeconds()) && 
					   scheduleDetail.getInitialDate().getHours() <= currentDay.getHours() && 
					   scheduleDetail.getInitialDate().getMinutes() <= currentDay.getMinutes() &&
					   scheduleDetail.getInitialDate().getSeconds() <= currentDay.getSeconds()){
						return Boolean.TRUE;
					}
				}
			}
		return Boolean.FALSE;
	}

	/**
	 * Obtener horario service bean.
	 *
	 * @param h the h
	 * @return the horario
	 * @throws ServiceException the service exception
	 */
	public Schedule getScheduleServiceBean(Schedule h) throws ServiceException{
		
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Schedule> criteriaQuery = criteriaBuilder.createQuery(Schedule.class);
		Root<Schedule> scheduleRoot = criteriaQuery.from(Schedule.class);
		List<Predicate> predicateParameters = new ArrayList<Predicate>();
		
		Join<?, ?> scheduleDetailJoin= (Join<?, ?>)scheduleRoot.fetch("scheduleDetails");
		predicateParameters.add(criteriaBuilder.equal(scheduleDetailJoin.get("state"),ScheduleStateType.REGISTERED.getCode()));
		
		predicateParameters.add(criteriaBuilder.equal(scheduleRoot.get("idSchedulePk"),h.getIdSchedulePk()));
		criteriaQuery.where(criteriaBuilder.and(predicateParameters.toArray(new Predicate[predicateParameters.size()])));
		TypedQuery<Schedule> horarioCriteria = em.createQuery(criteriaQuery);
		horarioCriteria.setHint("org.hibernate.cacheable", true);
		return horarioCriteria.getSingleResult();
	}

	/**
	 * Registrar horario intitucion.
	 *
	 * @param horarioList the horario list
	 * @return true, if successful
	 * @throws ServiceException
	 */
	public boolean registerScheduleInstitutionServiceBean(Schedule schedule)throws ServiceException{
		this.createOnCascade(schedule);
		return true;
	}
	
	/**
	 * Modificar horario intitucion.
	 *
	 * @param horarioList the horario list
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean modifyScheduleInstitutionServiceBean(Schedule schedule) throws ServiceException{
		this.updateOnCascade(schedule);
		return true;
	}
	
	/**
	 * Obtener horarios service bean.
	 *
	 * @param h the h
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Schedule> getSchedulesServiceBean(ScheduleFilter scheduleFilter) throws ServiceException{
		//Este metodo usara Criteria para el Listado de Horarios
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Schedule> criteriaQuery = criteriaBuilder.createQuery(Schedule.class);
		Root<Schedule> scheduleRoot = criteriaQuery.from(Schedule.class);
		List<Predicate> predicateParameters = new ArrayList<Predicate>();
		criteriaQuery.orderBy(criteriaBuilder.asc(scheduleRoot.get("idSchedulePk")));
		criteriaQuery.select(scheduleRoot).distinct(false);
		
		if(Validations.validateIsNotNullAndNotEmpty(scheduleFilter.getScheduleType()) && scheduleFilter.getScheduleType().equals(ScheduleType.INSTITUTIONS.getCode())){
			if(Validations.validateIsNotNull(scheduleFilter.getInstitutionType()) && Validations.validateIsPositiveNumber(scheduleFilter.getInstitutionType())){
				predicateParameters.add(criteriaBuilder.equal(scheduleRoot.get("institutionType"),scheduleFilter.getInstitutionType()));
			}else if(Validations.validateIsNotNullAndNotEmpty(scheduleFilter.getLstInstitutiontype()) && 
									Validations.validateListIsNotNullAndNotEmpty(scheduleFilter.getLstInstitutiontype())){
				ParameterExpression<List> param = criteriaBuilder.parameter(List.class,"lstinstitutiontype");
				predicateParameters.add(scheduleRoot.get("institutionType").in(param));	
				
				
			}
			criteriaQuery.select(scheduleRoot).distinct(false); 
		}/*else if(Validations.validateIsNotNullAndNotEmpty(scheduleFilter.getScheduleType()) &&  scheduleFilter.getScheduleType().equals(ScheduleType.SYSTEMS.getCode())){
			scheduleRoot.fetch("systems",JoinType.LEFT);
		}*/
		if(Validations.validateIsNotNullAndNotEmpty(scheduleFilter.getScheduleType()) && scheduleFilter.getScheduleType() > -1){
			predicateParameters.add(criteriaBuilder.equal(scheduleRoot.get("scheduleType"),scheduleFilter.getScheduleType()));
		}
		if(Validations.validateIsNotNullAndNotEmpty(scheduleFilter.getDescription())){
			predicateParameters.add(criteriaBuilder.like(scheduleRoot.<String>get("description"),scheduleFilter.getDescription()+"%"));
		}
		if(Validations.validateIsNotNullAndNotEmpty(scheduleFilter.getScheduleState()) && scheduleFilter.getScheduleState() > -1 ){ 
			predicateParameters.add(criteriaBuilder.equal(scheduleRoot.get("state"),scheduleFilter.getScheduleState()));
		}
		

		if (predicateParameters.size() == 1) {
	        criteriaQuery.where(predicateParameters.get(0));
	    } else {
	        criteriaQuery.where(criteriaBuilder.and(predicateParameters.toArray(new Predicate[predicateParameters.size()])));
	    }
		
		TypedQuery<Schedule> criteriaSchedule = em.createQuery(criteriaQuery);
		if(Validations.validateIsNotNullAndNotEmpty(scheduleFilter.getScheduleType()) && scheduleFilter.getScheduleType().equals(ScheduleType.INSTITUTIONS.getCode())){
			if(Validations.validateIsNullOrNotPositive(scheduleFilter.getInstitutionType()) && Validations.validateIsNotNullAndNotEmpty(scheduleFilter.getLstInstitutiontype()) ){
				criteriaSchedule.setParameter("lstinstitutiontype", scheduleFilter.getLstInstitutiontype());
			}
		}
		criteriaSchedule.setHint("org.hibernate.cacheable", true);
		return criteriaSchedule.getResultList();
	}
	
	
	/**
	 * Obtener horario detalle por usuario.
	 *
	 * @param horarioFiltro the horario filtro
	 * @return the horario detalle
	 * @throws ServiceException the service exception
	 */
	public ScheduleDetail getScheduleDetailsByUserServiceBean(ScheduleFilter scheduleFilter) throws ServiceException{		
		try{
			StringBuffer sql = new StringBuffer();
			sql.append(" Select HD From ScheduleDetail HD ");
			sql.append(" Join Fetch HD.schedule H ");
			sql.append(" Where HD.state = :scheduleDetailStatePrm");
			sql.append(" And HD.day = :dayPrm And :hourPrm Between HD.initialDate And HD.endDate ");
			sql.append(" And H.state = :scheduleStatePrm And H.scheduleType = :scheduleTypePrm ");
			sql.append(" And H.institutionType = :idInstitutionTypePrm");
			
			Query query = em.createQuery(sql.toString());
			query.setParameter("scheduleDetailStatePrm", scheduleFilter.getScheduleDetailFilter().getScheduleDetailState());
			query.setParameter("dayPrm", scheduleFilter.getScheduleDetailFilter().getDay());
			query.setParameter("hourPrm", scheduleFilter.getScheduleDetailFilter().getHour(), TemporalType.TIME);
			query.setParameter("scheduleStatePrm", scheduleFilter.getScheduleState());
			query.setParameter("scheduleTypePrm", scheduleFilter.getScheduleType());
			query.setParameter("idInstitutionTypePrm", scheduleFilter.getInstitutionType());
			query.setHint("org.hibernate.cacheable", true);
			return (ScheduleDetail) query.getSingleResult();
			
		} catch(NoResultException ex){
			return null;
		} catch(NonUniqueResultException ex){
			return null;
		}
	}

	public Schedule getScheduleByInstitutionServiceBean(ScheduleFilter scheduleFilter) throws ServiceException{
		try{
			StringBuffer sql = new StringBuffer();
			sql.append(" Select H From Schedule H ");
			sql.append(" Where H.state = :scheduleStatePrm And H.scheduleType = :scheduleTypePrm ");
			sql.append(" And H.institutionType = :idInstitutionTypePrm");
			
			Query query = em.createQuery(sql.toString());
			query.setParameter("scheduleStatePrm", scheduleFilter.getScheduleState());
			query.setParameter("scheduleTypePrm", scheduleFilter.getScheduleType());
			query.setParameter("idInstitutionTypePrm", scheduleFilter.getInstitutionType());
			query.setHint("org.hibernate.cacheable", true);
			return (Schedule) query.getSingleResult();
			
		} catch(NoResultException ex){
			return null;
		} catch(NonUniqueResultException ex){
			return null;
		}
	}

	public ScheduleDetail getScheduleDetailBySystemServiceBean(ScheduleFilter scheduleFilter)throws ServiceException {
		try{
			StringBuffer sql = new StringBuffer();
			sql.append(" Select HD From Schedule H ");
			sql.append(" Join H.scheduleDetails HD ");
			sql.append(" Join H.systems Sys ");
			sql.append(" Where HD.state = :scheduleDetailStatePrm");
			sql.append(" And HD.day = :dayPrm And :hourPrm Between HD.initialDate And HD.endDate ");
			sql.append(" And H.state = :scheduleStatePrm And H.scheduleType = :scheduleTypePrm ");
			sql.append(" And Sys.idSystemPk = :idSystemPrm");
			
			Query query = em.createQuery(sql.toString());
			query.setParameter("scheduleDetailStatePrm", scheduleFilter.getScheduleDetailFilter().getScheduleDetailState());
			query.setParameter("dayPrm", scheduleFilter.getScheduleDetailFilter().getDay());
			query.setParameter("hourPrm", scheduleFilter.getScheduleDetailFilter().getHour(), TemporalType.TIMESTAMP);
			query.setParameter("scheduleStatePrm", scheduleFilter.getScheduleState());
			query.setParameter("scheduleTypePrm", scheduleFilter.getScheduleType());
			query.setParameter("idSystemPrm", scheduleFilter.getIdSystem());
			query.setHint("org.hibernate.cacheable", true);
			return (ScheduleDetail) query.getSingleResult();
			
		} catch(NoResultException ex){
			return null;
		} catch(NonUniqueResultException ex){
			return null;
		}
	}

	public List<System> getSystemsByScheduleServiceBean(long idSchedulePk)throws ServiceException {
		try{
			StringBuffer sql = new StringBuffer();
			sql.append(" Select s From System s ");
			sql.append(" where s.schedule.idSchedulePk = :idSchedulePk");
					
			Query query = em.createQuery(sql.toString());
			query.setParameter("idSchedulePk", idSchedulePk);
			
			query.setHint("org.hibernate.cacheable", true);
			
		return query.getResultList();
		}
		catch (Exception e) {
			return null;
		}
	}
	/**
	 * it gets the List of Schedule details
	 * @param idSchedulePk
	 * @return
	 * @throws ServiceException
	 */
	public List<ScheduleDetail> getScheduleDetailServiceFacade(long idSchedulePk)throws ServiceException {
		try{
			StringBuffer sql = new StringBuffer();
			sql.append(" Select sd From ScheduleDetail sd ");
			sql.append(" where sd.schedule.idSchedulePk = :idSchedulePk");
					
			Query query = em.createQuery(sql.toString());
			query.setParameter("idSchedulePk", idSchedulePk);
			
			query.setHint("org.hibernate.cacheable", true);
			
		return query.getResultList();
		}
		catch (Exception e) {
			return null;
		}
	}
	/**
	 * It deletes the already existed records with same schedulePk
	 * @param idSchedulePk
	 * @throws ServiceException
	 */
	public void deleteScheduleDetailServiceFacade(long idSchedulePk)throws ServiceException{
		try{
			StringBuffer sql = new StringBuffer();
			sql.append(" Delete From ScheduleDetail sd ");
			sql.append(" where sd.schedule.idSchedulePk = :idSchedulePk");
					
			Query query = em.createQuery(sql.toString());
			query.setParameter("idSchedulePk", idSchedulePk);
			query.executeUpdate();
		}
		catch (Exception e) {
		}
	}
}