package com.pradera.security.systemmgmt.view.filters;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.security.model.UserAccount;

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class UserExceptionFilter
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public class UserExceptionFilter implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The user exception selected type. */
	private Integer userExceptionSelectedType;
	
	/** The institution selected. */
	private Integer institutionSelected;
	
	/** The user exception state selected. */
	private Integer userExceptionStateSelected;
	
	/** The id system selected. */
	private Integer idSystemSelected;
	
	/** The user exception code user. */
	private Integer userExceptionCodeUser;
	
	/** The initial date. */
	@NotNull(message="{exception.user.date.start.required}")
	private Date initialDate;
	
	/** The final date. */
	@NotNull(message="{exception.user.date.end.required}")
    private Date finalDate;
    
    /** The user exception option state. */
    private Integer userExceptionOptionState;
    
    /** The user exception privelege state. */
    private Integer userExceptionPrivelegeState;
    
    /** The user exception language. */
    private Integer userExceptionLanguage;	
	
	/** The user exception name. */
	private String userExceptionName;
	
	/** The user exception description. */
	private String userExceptionDescription;
	
	/** The usu login. */
	private String usuLogin;
	
	/** The id privelege exception. */
	private Integer idPrivelegeException;
	
	/** The options list pk. */
	private List<Integer> optionsListPK;
	
	/** The privelege list pk. */
	private List<Integer> privelegeListPK;	
	
	/** The block motive. */
	private Integer blockMotive;
	
	/** The unblock motive. */
	private Integer unblockMotive;
	
	/** The state. */
	private Integer state;
	
	/** The user full name. */
	private String userFullName;
	
	/** The block other motive. */
	private String blockOtherMotive;
	
	/** The unblock other motive. */
	private String unblockOtherMotive;
	
	/** The current. */
	private Date current;
	
	/** The file name. */
	private String fileName;
	
	/** The load file. */
	private byte[] loadFile;
	
	/** The version. */
	private Long version;
	
	/** The registry date. */
	private Date registryDate;
	
	/** The registry user. */
	private String registryUser;
	
	private UserAccount userAccount;
	
	/**
	 * Constructor.
	 */
	public UserExceptionFilter(){
		current =  CommonsUtilities.currentDateTime();
	}


	

	/**
	 * Gets the institution selected.
	 *
	 * @return institutionSelected
	 */
	public Integer getInstitutionSelected() {
		return institutionSelected;
	}




	/**
	 * Sets the institution selected.
	 *
	 * @param institutionSelected the new institution selected
	 */
	public void setInstitutionSelected(Integer institutionSelected) {
		this.institutionSelected = institutionSelected;
	}




	/**
	 * Gets the user exception state selected.
	 *
	 * @return userExceptionStateSelected
	 */
	public Integer getUserExceptionStateSelected() {
		return userExceptionStateSelected;
	}

	/**
	 * Sets the user exception state selected.
	 *
	 * @param userExceptionStateSelected the new user exception state selected
	 */
	public void setUserExceptionStateSelected(Integer userExceptionStateSelected) {
		this.userExceptionStateSelected = userExceptionStateSelected;
	}

	/**
	 * Gets the id system selected.
	 *
	 * @return idSystemSelected
	 */
	public Integer getIdSystemSelected() {
		return idSystemSelected;
	}

	/**
	 * Sets the id system selected.
	 *
	 * @param idSystemSelected the new id system selected
	 */
	public void setIdSystemSelected(Integer idSystemSelected) {
		this.idSystemSelected = idSystemSelected;
	}

	/**
	 * Gets the user exception code user.
	 *
	 * @return userExceptionCodeUser
	 */
	public Integer getUserExceptionCodeUser() {
		return userExceptionCodeUser;
	}

	/**
	 * Sets the user exception code user.
	 *
	 * @param userExceptionCodeUser the new user exception code user
	 */
	public void setUserExceptionCodeUser(Integer userExceptionCodeUser) {
		this.userExceptionCodeUser = userExceptionCodeUser;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return initialDate
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		if(initialDate != null && finalDate != null && initialDate.after(finalDate)){
			Calendar cal = Calendar.getInstance();
			cal.setTime(initialDate);
			finalDate = cal.getTime();
		}
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return finalDate
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param fechaFin the new final date
	 */
	public void setFinalDate(Date fechaFin) {
		this.finalDate = fechaFin;
	}

	/**
	 * Gets the user exception selected type.
	 *
	 * @return userExceptionSelectedType
	 */
	public Integer getUserExceptionSelectedType() {
		return userExceptionSelectedType;
	}

	/**
	 * Sets the user exception selected type.
	 *
	 * @param userExceptionSelectedType the new user exception selected type
	 */
	public void setUserExceptionSelectedType(Integer userExceptionSelectedType) {
		this.userExceptionSelectedType = userExceptionSelectedType;
	}

	/**
	 * Gets the user exception option state.
	 *
	 * @return userExceptionOptionState
	 */
	public Integer getUserExceptionOptionState() {
		return userExceptionOptionState;
	}

	/**
	 * Sets the user exception option state.
	 *
	 * @param userExceptionOptionState the new user exception option state
	 */
	public void setUserExceptionOptionState(Integer userExceptionOptionState) {
		this.userExceptionOptionState = userExceptionOptionState;
	}

	/**
	 * Gets the user exception privelege state.
	 *
	 * @return userExceptionPrivelegeState
	 */
	public Integer getUserExceptionPrivelegeState() {
		return userExceptionPrivelegeState;
	}

	/**
	 * Sets the user exception privelege state.
	 *
	 * @param userExceptionPrivelegeState the new user exception privelege state
	 */
	public void setUserExceptionPrivelegeState(Integer userExceptionPrivelegeState) {
		this.userExceptionPrivelegeState = userExceptionPrivelegeState;
	}

	/**
	 * Gets the user exception language.
	 *
	 * @return userExceptionLanguage
	 */
	public Integer getUserExceptionLanguage() {
		return userExceptionLanguage;
	}

	/**
	 * Sets the user exception language.
	 *
	 * @param userExceptionLanguage the new user exception language
	 */
	public void setUserExceptionLanguage(Integer userExceptionLanguage) {
		this.userExceptionLanguage = userExceptionLanguage;
	}

	/** 
	 * @return userExceptionName
	 */
	public String getUserExceptionName() {
		return userExceptionName;
	}

	/**
	 * Sets the user exception name.
	 *
	 * @param userExceptionName the new user exception name
	 */
	public void setUserExceptionName(String userExceptionName) {
		this.userExceptionName = userExceptionName;
	}

	/**
	 * Gets the user exception description.
	 *
	 * @return userExceptionDescription
	 */
	public String getUserExceptionDescription() {
		return userExceptionDescription;
	}

	/**
	 * Sets the user exception description.
	 *
	 * @param userExceptionDescription the new user exception description
	 */
	public void setUserExceptionDescription(String userExceptionDescription) {
		this.userExceptionDescription = userExceptionDescription;
	}

	/**
	 * Gets the usu login.
	 *
	 * @return usuLogin
	 */
	public String getUsuLogin() {
		return usuLogin;
	}

	/**
	 * Sets the usu login.
	 *
	 * @param usuLogin the new usu login
	 */
	public void setUsuLogin(String usuLogin) {
		this.usuLogin = usuLogin;
	}

	/**
	 * Gets the id privelege exception.
	 *
	 * @return idPrivelegeException
	 */
	public Integer getIdPrivelegeException() {
		return idPrivelegeException;
	}

	/**
	 * Sets the id privelege exception.
	 *
	 * @param idPrivelegeException the new id privelege exception
	 */
	public void setIdPrivelegeException(Integer idPrivelegeException) {
		this.idPrivelegeException = idPrivelegeException;
	}

	/**
	 * Gets the options list pk.
	 *
	 * @return optionListPK
	 */
	public List<Integer> getOptionsListPK() {
		return optionsListPK;
	}

	/**
	 * Sets the options list pk.
	 *
	 * @param optionsListPK the new options list pk
	 */
	public void setOptionsListPK(List<Integer> optionsListPK) {
		this.optionsListPK = optionsListPK;
	}

	/**
	 * Gets the privelege list pk.
	 *
	 * @return privelegeListPk
	 */
	public List<Integer> getPrivelegeListPK() {
		return privelegeListPK;
	}

	/**
	 * Sets the privelege list pk.
	 *
	 * @param privelegeListPK the new privelege list pk
	 */
	public void setPrivelegeListPK(List<Integer> privelegeListPK) {
		this.privelegeListPK = privelegeListPK;
	}

	/**
	 * Gets the current.
	 *
	 * @return the current
	 */
	public Date getCurrent() {
		return current;
	}

	/**
	 * Sets the current.
	 *
	 * @param current the current to set
	 */
	public void setCurrent(Date current) {
		this.current = current;
	}

	/**
	 * Gets the block motive.
	 *
	 * @return the blockMotive
	 */
	public Integer getBlockMotive() {
		return blockMotive;
	}

	/**
	 * Sets the block motive.
	 *
	 * @param blockMotive the blockMotive to set
	 */
	public void setBlockMotive(Integer blockMotive) {
		this.blockMotive = blockMotive;
	}

	/**
	 * Gets the unblock motive.
	 *
	 * @return the unblockMotive
	 */
	public Integer getUnblockMotive() {
		return unblockMotive;
	}

	/**
	 * Sets the unblock motive.
	 *
	 * @param unblockMotive the unblockMotive to set
	 */
	public void setUnblockMotive(Integer unblockMotive) {
		this.unblockMotive = unblockMotive;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}


	/**
	 * Sets the state.
	 *
	 * @param state the state to set
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the user full name.
	 *
	 * @return the userFullName
	 */
	public String getUserFullName() {
		return userFullName;
	}


	/**
	 * Sets the user full name.
	 *
	 * @param userFullName the userFullName to set
	 */
	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}
	
	/**
	 * Gets the block other motive.
	 *
	 * @return the blockOtherMotive
	 */
	public String getBlockOtherMotive() {
		return blockOtherMotive;
	}

    /**
     * Sets the block other motive.
     *
     * @param blockOtherMotive the blockOtherMotive to set
     */
	public void setBlockOtherMotive(String blockOtherMotive) {
		this.blockOtherMotive = blockOtherMotive;
	}
	/**
	 * Gets the stream document file.
	 *
	 * @return the stream document file
	 */
	public StreamedContent getStreamDocumentFile(){
		InputStream is = null;
		if(loadFile!=null){
			is = new ByteArrayInputStream(loadFile);
			return new DefaultStreamedContent(is,"application/pdf","document");
		}
		return null;
	}

	/**
	 * Gets the unblock other motive.
	 *
	 * @return the unblockOtherMotive
	 */
	public String getUnblockOtherMotive() {
		return unblockOtherMotive;
	}

    /**
     * Sets the unblock other motive.
     *
     * @param unblockOtherMotive the unblockOtherMotive to set
     */
	public void setUnblockOtherMotive(String unblockOtherMotive) {
		this.unblockOtherMotive = unblockOtherMotive;
	}

	/**
	 * Gets the file name.
	 *
	 * @return the file name
	 */
	public String getFileName() {
		return fileName;
	}


	/**
	 * Sets the file name.
	 *
	 * @param fileName the new file name
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	/**
	 * Gets the load file.
	 *
	 * @return the load file
	 */
	public byte[] getLoadFile() {
		return loadFile;
	}

	/**
	 * Sets the load file.
	 *
	 * @param loadFile the new load file
	 */
	public void setLoadFile(byte[] loadFile) {
		this.loadFile = loadFile;
	}




	/**
	 * Gets the version.
	 *
	 * @return the version
	 */
	public Long getVersion() {
		return version;
	}




	/**
	 * Sets the version.
	 *
	 * @param version the new version
	 */
	public void setVersion(Long version) {
		this.version = version;
	}




	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}




	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}




	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}




	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}




	public UserAccount getUserAccount() {
		return userAccount;
	}




	public void setUserAccount(UserAccount userAccount) {
		this.userAccount = userAccount;
	}
}
