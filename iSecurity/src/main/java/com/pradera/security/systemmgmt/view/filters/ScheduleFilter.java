package com.pradera.security.systemmgmt.view.filters;

import java.io.Serializable;
import java.util.List;

import com.pradera.security.model.type.ScheduleType;
import com.pradera.security.usermgmt.view.filters.UserAccountFilter;

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class ScheduleFilter is useful to handle parameters in FrontEnd.
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date : 17/12/2012 
 * @version 1.1
 */

public class ScheduleFilter implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id schedule type. */
	private Integer scheduleType;
	
	/** The description. */
	private String description;
	
	/** The id state. */
	private Integer scheduleState;
	
	/** The id institution. */
//	private Integer idInstitution;
	
	/** The horario detalle filtro. */
	private ScheduleDetailFilter scheduleDetailFilter;
	
	/** The usuario filtro. */
	private UserAccountFilter userAccountFilter;
	
	/** The id system. */
	private Integer idSystem;
	
	/** The institution type. */
	private Integer institutionType;
	
	/** The institutionType list*/
	private List<Integer> lstInstitutiontype;

	/**
	 * The Constructor.
	 */
	public ScheduleFilter() {
		scheduleDetailFilter = new ScheduleDetailFilter();
		userAccountFilter = new UserAccountFilter();
	}

	
	/**
	 * Gets the schedule type.
	 *
	 * @return the schedule type
	 */
	public Integer getScheduleType() {
		return scheduleType;
	}


	/**
	 * Sets the schedule type.
	 *
	 * @param scheduleType the new schedule type
	 */
	public void setScheduleType(Integer scheduleType) {
		this.scheduleType = scheduleType;
	}


	/**
	 * Gets the schedule state.
	 *
	 * @return the schedule state
	 */
	public Integer getScheduleState() {
		return scheduleState;
	}


	/**
	 * Sets the schedule state.
	 *
	 * @param scheduleState the new schedule state
	 */
	public void setScheduleState(Integer scheduleState) {
		this.scheduleState = scheduleState;
	}


	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the description
	 */
	public void setDescription(String description) {
		if (description != null) {
			this.description = description.toUpperCase();
		}
		this.description = description;
	}


	/**
	 * Gets the id institution.
	 *
	 * @return the id institution
	 */
//	public Integer getIdInstitution() {
//		return idInstitution;
//	}


	/**
	 * Sets the id institution.
	 *
	 * @param idInstitution the new id institution
	 */
//	public void setIdInstitution(Integer idInstitution) {
//		this.idInstitution = idInstitution;
//	}


	/**
	 * Checks if is institution.
	 *
	 * @return true, if checks if is institution
	 */
	public boolean isInstitution() {
		return this.getScheduleType().equals(ScheduleType.INSTITUTIONS.getCode());
	}
	/**
	 * Gets the schedule detail filter.
	 *
	 * @return the schedule detail filter
	 */
	public ScheduleDetailFilter getScheduleDetailFilter() {
		return scheduleDetailFilter;
	}
	/**
	 * Sets the schedule detail filter.
	 *
	 * @param scheduleDetailFilter the new schedule detail filter
	 */
	public void setScheduleDetailFilter(ScheduleDetailFilter scheduleDetailFilter) {
		this.scheduleDetailFilter = scheduleDetailFilter;
	}
	/**
	 * Gets the user account filter.
	 *
	 * @return the user account filter
	 */
	public UserAccountFilter getUserAccountFilter() {
		return userAccountFilter;
	}
	/**
	 * Sets the user account filter.
	 *
	 * @param userAccountFilter the new user account filter
	 */
	public void setUserAccountFilter(UserAccountFilter userAccountFilter) {
		this.userAccountFilter = userAccountFilter;
	}
	/**
	 * Gets the id system.
	 *
	 * @return the id system
	 */
	public Integer getIdSystem() {
		return idSystem;
	}
	/**
	 * Sets the id system.
	 *
	 * @param idSystem the new id system
	 */
	public void setIdSystem(Integer idSystem) {
		this.idSystem = idSystem;
	}
	/**
	 * Gets the institution type.
	 *
	 * @return the institution type
	 */
	public Integer getInstitutionType() {
		return institutionType;
	}
	/**
	 * Sets the institution type.
	 *
	 * @param institutionType the new institution type
	 */
	public void setInstitutionType(Integer institutionType) {
		this.institutionType = institutionType;
	}


	/**
	 * @return the lstInstitutiontype
	 */
	public List<Integer> getLstInstitutiontype() {
		return lstInstitutiontype;
	}


	/**
	 * @param lstInstitutiontype the lstInstitutiontype to set
	 */
	public void setLstInstitutiontype(List<Integer> lstInstitutiontype) {
		this.lstInstitutiontype = lstInstitutiontype;
	}
}