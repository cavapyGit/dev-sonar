package com.pradera.security.systemmgmt.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.pradera.commons.security.model.type.UserSessionStateType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.model.ProfilePrivilege;
import com.pradera.security.model.SystemProfile;
import com.pradera.security.model.SystemProfileRequest;
import com.pradera.security.systemmgmt.view.filters.SystemProfileFilter;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Class PerfilSistemaServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/11/2012
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class SystemProfileServiceBean extends CrudSecurityServiceBean{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4225897903058437884L;

	/**
	 * Buscar perfiles por filtros.
	 *
	 * @param perfilFiltro the perfil filtro
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<SystemProfile> searchProfilesByFiltersServiceBean(SystemProfileFilter systemProfileFilter) throws ServiceException{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<SystemProfile> cq = cb.createQuery(SystemProfile.class);
		Root<SystemProfile> systemProfileRoot = cq.from(SystemProfile.class);
		systemProfileRoot.fetch("system");
		List<Predicate> criteriaParams = new ArrayList<Predicate>();
		cq.orderBy(cb.asc(systemProfileRoot.get("mnemonic")));
		
		cq.select(systemProfileRoot);
		
		
		if(Validations.validateIsNotNullAndNotEmpty(systemProfileFilter.getIdSystemBaseQuery())
				&& systemProfileFilter.getIdSystemBaseQuery()>0){
			ParameterExpression<Integer> p = cb.parameter(Integer.class,"idSystemPrm");
			criteriaParams.add(cb.equal(systemProfileRoot.get("system").get("idSystemPk"),p));
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(systemProfileFilter.getIndUserType())){
			ParameterExpression<Integer> p = cb.parameter(Integer.class,"indUserType");
			criteriaParams.add(cb.equal(systemProfileRoot.get("indUserType"),p));
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(systemProfileFilter.getInstitutionType())){
			ParameterExpression<Integer> p = cb.parameter(Integer.class,"instType");
			criteriaParams.add(cb.equal(systemProfileRoot.get("institutionType"),p));
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(systemProfileFilter.getProfileMnemonicBaseQuery())){
			ParameterExpression<String> p = cb.parameter(String.class,"profileMnemonicPrm");
			criteriaParams.add(cb.like(systemProfileRoot.<String>get("mnemonic"),p));
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(systemProfileFilter.getProfileNameBaseQuery())){
			ParameterExpression<String> p = cb.parameter(String.class,"profileNamePrm");
			criteriaParams.add(cb.like(systemProfileRoot.<String>get("name"),p));			
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(systemProfileFilter.getProfileStateBaseQuery())
				&& systemProfileFilter.getProfileStateBaseQuery()>0){
			ParameterExpression<Integer> p = cb.parameter(Integer.class,"profileStatePrm");
			criteriaParams.add(cb.equal(systemProfileRoot.get("state"),p));
		}
		
		ParameterExpression<Integer> p = cb.parameter(Integer.class,"profileStateDefaulPrm");
		criteriaParams.add(cb.lessThan(systemProfileRoot.get("state"),p));
				
		if(!criteriaParams.isEmpty()){
			if (criteriaParams.size() == 1) {
		        cq.where(criteriaParams.get(0));
		    } else {
		        cq.where(cb.and(criteriaParams.toArray(new Predicate[criteriaParams.size()])));
		    }
		}
				
		TypedQuery<SystemProfile> perfilCriteria = em.createQuery(cq);
		
		if(Validations.validateIsNotNullAndNotEmpty(systemProfileFilter.getIdSystemBaseQuery())
				&& systemProfileFilter.getIdSystemBaseQuery()>0){
			perfilCriteria.setParameter("idSystemPrm", systemProfileFilter.getIdSystemBaseQuery());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(systemProfileFilter.getIndUserType())){		
			perfilCriteria.setParameter("indUserType", systemProfileFilter.getIndUserType());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(systemProfileFilter.getInstitutionType())){		
			perfilCriteria.setParameter("instType", systemProfileFilter.getInstitutionType());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(systemProfileFilter.getProfileMnemonicBaseQuery())){		
			perfilCriteria.setParameter("profileMnemonicPrm", systemProfileFilter.getProfileMnemonicBaseQuery() + "%");
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(systemProfileFilter.getProfileNameBaseQuery())){
			perfilCriteria.setParameter("profileNamePrm", systemProfileFilter.getProfileNameBaseQuery() + "%");
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(systemProfileFilter.getProfileStateBaseQuery())
				&& systemProfileFilter.getProfileStateBaseQuery()>0){
			perfilCriteria.setParameter("profileStatePrm", systemProfileFilter.getProfileStateBaseQuery());
		}

		perfilCriteria.setParameter("profileStateDefaulPrm", 6);

        return (List<SystemProfile>) perfilCriteria.getResultList();
    }
	
	/**
	 * Listar privilegios por perfil.
	 *
	 * @param perfilFiltro the perfil filtro
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ProfilePrivilege> listPrivilegesByProfileServiceBean(SystemProfileFilter systemProfileFilter) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select PP From  ProfilePrivilege PP left join fetch PP.optionPrivilege");	
		sbQuery.append(" Where  PP.systemProfile.idSystemProfilePk = :idSystemProfilePrm ");
		sbQuery.append(" And PP.state = :profilePrivilegeStatePrm ");
		
		Query query = em.createQuery(sbQuery.toString());
					
		query.setParameter("idSystemProfilePrm",systemProfileFilter.getIdSystemProfile());
		query.setParameter("profilePrivilegeStatePrm",systemProfileFilter.getProfilePrivilegeState());
		query.setHint("org.hibernate.cacheable", true);
		return (List<ProfilePrivilege>) query.getResultList();	    
	}
	
	public Date getLatsModifiedDetails(SystemProfile systemProfileFilter ){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select SP.lastModifyDate From  SystemProfile SP ");	
		sbQuery.append("Where SP.idSystemProfilePk = :idSystemProfilePrm ");	
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSystemProfilePrm",systemProfileFilter.getIdSystemProfilePk());	
		query.setHint("org.hibernate.cacheable", true);
		return  (Date)query.getSingleResult(); 
	}
	/**
	 * Actualizar estado perfil sistema.
	 *
	 * @param perfilSistema the perfil sistema
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int updateSystemProfileStateServiceBean(SystemProfile systemProfile , LoggerUser loggerUser) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update SystemProfile SP ");	
		sbQuery.append(" Set SP.state = :statePrm, SP.lastModifyUser = :lastModifyUserPrm, ");
		sbQuery.append(" SP.situation = :situationPrm, ");
		sbQuery.append(" SP.lastModifyDate = :lastModifyDatePrm, SP.lastModifyIp = :lastModifyIpPrm, ");
		sbQuery.append(" SP.lastModifyPriv = :lastModifyPrivPrm, ");
		sbQuery.append(" SP.blockMotive = :blockMotive ,");
		sbQuery.append(" SP.rejectMotive = :rejectMotive ,");
		sbQuery.append(" SP.unblockMotive = :unblockMotive, ");
		sbQuery.append(" SP.blockOtherMotive = :blockOtherMotiveDes, ");
		sbQuery.append(" SP.unblockOtherMotive = :unblockOtherMotiveDes, ");
		sbQuery.append(" SP.rejectOtherMotive = :rejectOtherMotiveDes, ");
		sbQuery.append(" SP.deleteOtherMotive= :deleteOtherMotiveDes, ");
		sbQuery.append(" SP.profileType = :profileType ");
		sbQuery.append(" Where SP.idSystemProfilePk = :idSystemProfilePrm ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("statePrm",systemProfile.getState());
		query.setParameter("situationPrm",systemProfile.getSituation());
		query.setParameter("lastModifyUserPrm",loggerUser.getUserName());
		query.setParameter("lastModifyDatePrm",loggerUser.getAuditTime());
		query.setParameter("lastModifyIpPrm",loggerUser.getIpAddress());
		query.setParameter("lastModifyPrivPrm",loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("blockMotive",systemProfile.getBlockMotive());
		query.setParameter("unblockMotive",systemProfile.getUnblockMotive());
		query.setParameter("rejectMotive",systemProfile.getRejectMotive());
		query.setParameter("blockOtherMotiveDes", systemProfile.getBlockOtherMotive());
		query.setParameter("unblockOtherMotiveDes", systemProfile.getUnblockOtherMotive());
		query.setParameter("rejectOtherMotiveDes", systemProfile.getRejectOtherMotive());
		query.setParameter("deleteOtherMotiveDes", systemProfile.getDeleteOtherMotive());
		query.setParameter("idSystemProfilePrm",systemProfile.getIdSystemProfilePk());
		query.setParameter("profileType",systemProfile.getProfileType());
		return query.executeUpdate();
	}
	
	/**
	 * Listar privilegios opciones perfil.
	 *
	 * @param systemProfileFilter the perfil filtro
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> listOptionPrivilegesByProfileServiceBean(SystemProfileFilter systemProfileFilter) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select distinct OP.idOptionPrivilegePk, OP.idPrivilege, SO.idSystemOptionPk ");
		sbQuery.append(" From SystemOption SO ");
		sbQuery.append(" Join SO.optionPrivileges OP ");
		sbQuery.append(" Join OP.profilePrivileges PP ");
		sbQuery.append(" Where OP.state = :optionPrivilegeStatePrm And  SO.state = :systemOptionStatePrm ");
		sbQuery.append(" And PP.state = :profilePrivilegeStatePrm And SO.system.idSystemPk = :idSystemPrm");
		if(Validations.validateIsNotNull(systemProfileFilter.getIdSystemProfile())){
			sbQuery.append(" And PP.systemProfile.idSystemProfilePk = :idSystemProfilePrm");
		}
		sbQuery.append(" Order By SO.idSystemOptionPk, OP.idPrivilege ");

		Query query = em.createQuery(sbQuery.toString());

		query.setParameter("optionPrivilegeStatePrm",systemProfileFilter.getOptionPrivilegeState());
		query.setParameter("systemOptionStatePrm",systemProfileFilter.getSystemOptionState());
		query.setParameter("profilePrivilegeStatePrm",systemProfileFilter.getProfilePrivilegeState());
		query.setParameter("idSystemPrm", systemProfileFilter.getIdSystem());
		if(Validations.validateIsNotNull(systemProfileFilter.getIdSystemProfile())){
			query.setParameter("idSystemProfilePrm", systemProfileFilter.getIdSystemProfile());
		}
		query.setHint("org.hibernate.cacheable", true);
		return (List<Object[]>) query.getResultList();
	}
	
	/**
	 * Valida nombre perfil.
	 *
	 * @param systemProfile the perfil sistema
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int validateProfileNameServiceBean(SystemProfile systemProfile) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select Count(SP) from SystemProfile SP Where SP.name = :namePrm ");
		
		if(systemProfile.getIdSystemProfilePk()!=null){
			sbQuery.append(" And SP.idSystemProfilePk != :idSystemProfilePrm ");
		}
		
    	Query query=em.createQuery(sbQuery.toString());
    		query.setParameter("namePrm", systemProfile.getName());
    	
    	if(systemProfile.getIdSystemProfilePk()!=null){
    		query.setParameter("idSystemProfilePrm", systemProfile.getIdSystemProfilePk());
    	}
    	query.setHint("org.hibernate.cacheable", true);
    	return Integer.parseInt(query.getSingleResult().toString());
	}

	/**
	 * Valida nemonico perfil.
	 *
	 * @param systemProfile the perfil sistema
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int validateProfileMnemonicServiceBean(SystemProfile systemProfile) throws ServiceException{
		String strQuery="Select Count(SP) from SystemProfile SP Where SP.mnemonic = :mnemonicPrm";
    	Query query=em.createQuery(strQuery);
    	query.setParameter("mnemonicPrm", systemProfile.getMnemonic()); 
    	query.setHint("org.hibernate.cacheable", true);
    	return Integer.parseInt(query.getSingleResult().toString());
	}

	/**
	 * @param systemProfile
	 * @return list
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	public List<String> getActiveProfileUsersServiceBean(
			SystemProfile systemProfile)throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select ua.loginUser from UserAccount ua,UserSession us,UserProfile up where ");
		sbQuery.append("ua.idUserPk = us.userAccount.idUserPk and us.userAccount.idUserPk = up.userAccount.idUserPk and ");
		sbQuery.append("us.system.idSystemPk = :systemId and us.state = :sessionState and ");
		sbQuery.append("up.systemProfile.idSystemProfilePk = :idSystemProfile ");
		
		Query query = em.createQuery(sbQuery.toString());
		
		query.setParameter("systemId", systemProfile.getSystem().getIdSystemPk());
		query.setParameter("sessionState", UserSessionStateType.CONNECTED.getCode());
		query.setParameter("idSystemProfile", systemProfile.getIdSystemProfilePk());
		return (List<String>)query.getResultList();
	}
	
	public SystemProfileRequest getRegisteredProfileRequest(Integer systemProfile,List<Integer> requestStates,boolean fetchDetail){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select distinct spr from SystemProfileRequest spr ");
		if(fetchDetail){
			sbQuery.append(" join fetch spr.profilePrivilegeRequests ");
		}
		sbQuery.append(" where spr.systemProfile.idSystemProfilePk = :profile and spr.requestState in (:states) ");
		SystemProfileRequest request = null;
		TypedQuery<SystemProfileRequest> query = em.createQuery(sbQuery.toString(),SystemProfileRequest.class);
		query.setParameter("profile", systemProfile);
		query.setParameter("states", requestStates);
		List<SystemProfileRequest> result = query.getResultList();
		if(result.size() > 0){
			request = result.get(0);
		}
		return request;
	}
	
	public void updateSituationProfile(Integer systemProfile,Integer situation,LoggerUser loggerUser){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update SystemProfile SP ");	
		sbQuery.append(" Set SP.lastModifyUser = :lastModifyUserPrm, ");
		sbQuery.append(" SP.situation = :situationPrm, ");
		sbQuery.append(" SP.lastModifyDate = :lastModifyDatePrm, SP.lastModifyIp = :lastModifyIpPrm, ");
		sbQuery.append(" SP.lastModifyPriv = :lastModifyPrivPrm ");
		sbQuery.append(" Where SP.idSystemProfilePk = :idSystemProfilePrm ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("lastModifyUserPrm", loggerUser.getUserName());
		query.setParameter("situationPrm", situation);
		query.setParameter("lastModifyDatePrm", loggerUser.getAuditTime());
		query.setParameter("lastModifyIpPrm", loggerUser.getIpAddress());
		query.setParameter("lastModifyPrivPrm", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("idSystemProfilePrm", systemProfile);
		query.executeUpdate();
	}
}
