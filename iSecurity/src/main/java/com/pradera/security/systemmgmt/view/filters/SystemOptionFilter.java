package com.pradera.security.systemmgmt.view.filters;

import java.io.Serializable;
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class SystemOptionFilter
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.1
 */
public class SystemOptionFilter implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long idUserSessionPk;
	private Integer idSystemOptionPk;
	private Integer idModulePk;
	private String usserActionJson;
	
	/**
	 * Constructor
	 */
	public SystemOptionFilter(){
	}

	/**
	 * @return the idUserSessionPk
	 */
	public Long getIdUserSessionPk() {
		return idUserSessionPk;
	}

	/**
	 * @param idUserSessionPk the idUserSessionPk to set
	 */
	public void setIdUserSessionPk(Long idUserSessionPk) {
		this.idUserSessionPk = idUserSessionPk;
	}

	/**
	 * @return the idSystemOptionPk
	 */
	public Integer getIdSystemOptionPk() {
		return idSystemOptionPk;
	}

	/**
	 * @param idSystemOptionPk the idSystemOptionPk to set
	 */
	public void setIdSystemOptionPk(Integer idSystemOptionPk) {
		this.idSystemOptionPk = idSystemOptionPk;
	}

	/**
	 * @return the idModulePk
	 */
	public Integer getIdModulePk() {
		return idModulePk;
	}

	/**
	 * @param idModulePk the idModulePk to set
	 */
	public void setIdModulePk(Integer idModulePk) {
		this.idModulePk = idModulePk;
	}

	/**
	 * @return the usserActionJson
	 */
	public String getUsserActionJson() {
		return usserActionJson;
	}

	/**
	 * @param usserActionJson the usserActionJson to set
	 */
	public void setUsserActionJson(String usserActionJson) {
		this.usserActionJson = usserActionJson;
	}

}