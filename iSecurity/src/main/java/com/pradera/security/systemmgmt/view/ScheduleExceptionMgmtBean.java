package com.pradera.security.systemmgmt.view;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDate;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.security.model.type.UserAccountStateType;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.security.model.HistoricalState;
import com.pradera.security.model.InstitutionsSecurity;
import com.pradera.security.model.Parameter;
import com.pradera.security.model.ScheduleException;
import com.pradera.security.model.ScheduleExceptionDetail;
import com.pradera.security.model.UserAccount;
import com.pradera.security.model.type.DefinitionType;
import com.pradera.security.model.type.OtherMotiveType;
import com.pradera.security.model.type.ParameterTableType;
import com.pradera.security.model.type.ScheduleExceptionAccessType;
import com.pradera.security.model.type.ScheduleExceptionDetailStateType;
import com.pradera.security.model.type.ScheduleExceptionStateType;
import com.pradera.security.model.type.ScheduleExceptionType;
import com.pradera.security.model.type.ScheduleSituationType;
import com.pradera.security.model.type.ScheduleStateType;
import com.pradera.security.model.type.SystemStateType;
import com.pradera.security.model.type.UserAccountType;
import com.pradera.security.systemmgmt.facade.SystemServiceFacade;
import com.pradera.security.systemmgmt.view.filters.InstitutionSecurityFilter;
import com.pradera.security.systemmgmt.view.filters.ScheduleExceptionFilter;
import com.pradera.security.usermgmt.view.filters.UserAccountFilter;
import com.pradera.security.utils.view.GenericBaseBean;
import com.pradera.security.utils.view.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class ScheduleExceptionMgmtBean
 * 
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date :
 * @version 1.1
 */
@DepositaryWebBean
public class ScheduleExceptionMgmtBean extends GenericBaseBean implements
		Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The schedule exception filter. */
	private ScheduleExceptionFilter scheduleExceptionFilter;

	/** The schedule exception session. */
	private ScheduleException scheduleExceptionSession;

	/** The schedule exception data model. */
	private GenericDataModel<ScheduleException> scheduleExceptionDataModel;

	/** The user account list. */
	private List<UserAccount> userAccountList;

	/** The user account data model. */
	private GenericDataModel<UserAccount> userAccountDataModel;

	/** The schedule exception detail list. */
	private List<ScheduleExceptionDetail> scheduleExceptionDetailList;

	/** The lst institution type for filter. */
	private List<Parameter> lstInstitutionTypeForFilter;
	
	/** The lst institution type for filter. */
	private List<Parameter> lstInstitutionTypeForFilterTemp;

	/** The lst institutions security for filter. */
	private List<InstitutionsSecurity> lstInstitutionsSecurityForFilter;

	/** The hystorical state. */
	private HistoricalState hystoricalState;

	/** The show other motive. */
	private boolean showOtherMotive;

	/** The possible save motive. */
	private boolean possibleSaveMotive;
	
	/** The user account filter. */
	private UserAccountFilter userAccountFilter;

	/** The schedule exception temp. */
	private String scheduleExceptionTemp;

	/** The is massive event. */
	private Boolean isMassiveEvent;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	@Inject
	UserPrivilege userPrivilege;
	
	 /** The log. */
    Logger log = Logger.getLogger(ScheduleExceptionMgmtBean.class.getName());
	
	/** The form state. */
	private boolean formState;
	
	/** The schedule exception detailparamtemp. */
	private ScheduleExceptionDetail scheduleExceptionDetailparamtemp;
	
	/** The institution type. */
	private Boolean institutionType=false;
	private boolean blDataModified=false;
	
	/** The lst user types values. */
	protected List<Parameter> lstUserTypesValues;

	private Integer systemPkForFilterListener;
	
	/** The schedule created profiles. */
	private List<UserAccount> scheduleCreatedProfiles;
	
	/** The schedules not creatd profiles. */
	private List<UserAccount> schedulesNotCreatdProfiles;

	private boolean isDisabledWeekends;

	private Integer minHourForException;

	private Integer maxHourForException;
	
	/**
	 * Sets the lst user types values.
	 *
	 * @param lstUserTypesValues the lstUserTypesValues to set
	 */
	public void setLstUserTypesValues(List<Parameter> lstUserTypesValues) {
		this.lstUserTypesValues = lstUserTypesValues;
	}

	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Gets the lst institutions security for filter.
	 *
	 * @return the lst institutions security for filter
	 */
	public List<InstitutionsSecurity> getLstInstitutionsSecurityForFilter() {
		return lstInstitutionsSecurityForFilter;
	}

	/**
	 * Sets the lst institutions security for filter.
	 *
	 * @param lstInstitutionsSecurityForFilter the lst institutions security for filter
	 */
	public void setLstInstitutionsSecurityForFilter(
			List<InstitutionsSecurity> lstInstitutionsSecurityForFilter) {
		this.lstInstitutionsSecurityForFilter = lstInstitutionsSecurityForFilter;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * Gets the user account state type list.
	 *
	 * @return the user account state type list
	 */
	//Commented by Hema on 03/10/2013
//	public List<UserAccountStateType> getUserAccountStateTypeList() {
//		return UserAccountStateType
//				.listSomeElements(UserAccountStateType.CONFIRMED);
//	}

	public List<Parameter> getUserAccountStateTypeList() {
		List<Parameter> lstStates = new ArrayList<Parameter>();
		try{
			lstStates =  userServiceFacade.listClientTypeByDefinitionServiceFacade(ParameterTableType.USER_STATES.getCode());
		}catch (Exception e) {
			e.printStackTrace();
		}
		return lstStates;
	}
	
	/**
	 * Gets the lst user types values.
	 *
	 * @return the lst user types values
	 */
	public List<Parameter> getLstUserTypesValues() {
		List<Parameter> lstParamValues = new ArrayList<Parameter>();
		try{
			lstParamValues =  userServiceFacade.listClientTypeByDefinitionServiceFacade(ParameterTableType.USER_CODE.getCode());
		}catch (Exception e) {
			e.printStackTrace();
		}
		return lstParamValues;
	}
	
	/**
	 * Gets the institution type.
	 *
	 * @return the institutionType
	 */
	public Boolean getInstitutionType() {
		return institutionType;
	}

	/**
	 * Sets the institution type.
	 *
	 * @param institutionType the institutionType to set
	 */
	public void setInstitutionType(Boolean institutionType) {
		this.institutionType = institutionType;
	}
	/**
	 * Change institution type for filter listener.
	 */
	public void changeInstitutionTypeForFilterListener() {
		try {
			JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
			if (userAccountFilter.getInstitutionType() > 0) {
				InstitutionSecurityFilter insSecurityFilter = new InstitutionSecurityFilter();
				insSecurityFilter.setIdInstitutionTypeFk(userAccountFilter.getInstitutionType());
				lstInstitutionsSecurityForFilter = userServiceFacade.getInstitutionsByFilterServiceFacade(insSecurityFilter);
				if(Validations.validateIsNotNull(lstInstitutionsSecurityForFilter) && lstInstitutionsSecurityForFilter.size()==1){
					userAccountFilter.setIdInstitutionSecurity(lstInstitutionsSecurityForFilter.get(0).getIdInstitutionSecurityPk());
				}
			} else {
				lstInstitutionsSecurityForFilter = new ArrayList<InstitutionsSecurity>();
				userAccountFilter.setIdInstitutionSecurity(Integer.valueOf(-1));
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Gets the is massive event.
	 *
	 * @return the checks if is massive event
	 */
	public Boolean getIsMassiveEvent() {
		return isMassiveEvent;
	}
	
	/**
	 * Sets the is massive event.
	 *
	 * @param isMassiveEvent the checks if is massive event
	 */
	public void setIsMassiveEvent(Boolean isMassiveEvent) {
		this.isMassiveEvent = isMassiveEvent;
	}

	/**
	 * Gets the lst institution type for filter.
	 *
	 * @return the lst institution type for filter
	 */
	public List<Parameter> getLstInstitutionTypeForFilter() {
		return lstInstitutionTypeForFilter;
	}

	/**
	 * Sets the lst institution type for filter.
	 *
	 * @param lstInstitutionTypeForFilter the lst institution type for filter
	 */
	public void setLstInstitutionTypeForFilter(
			List<Parameter> lstInstitutionTypeForFilter) {
		this.lstInstitutionTypeForFilter = lstInstitutionTypeForFilter;
	}

	/**
	 * The Constructor.
	 */
	public ScheduleExceptionMgmtBean() {
		super();
		loadComponentes();
		isMassiveEvent = Boolean.FALSE;
		scheduleExceptionSession = new ScheduleException();
		userAccountFilter = new UserAccountFilter();
		//userAccountDataModel = new GenericDataModel<UserAccount>();
	}

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		try{
		setFormState(false);
		possibleSaveMotive = false;
		beginConversation();
		loadComponentes();
		super.listSystemsByState(SystemStateType.CONFIRMED.getCode());
		
		lstInstitutionTypeForFilterTemp = userServiceFacade.listClientTypeByDefinitionServiceFacade(ParameterTableType.INSITUTIONS_STATE.getCode());
		//setting privilege
		PrivilegeComponent privilegeComp=new PrivilegeComponent();
		privilegeComp.setBtnModifyView(true);
		privilegeComp.setBtnConfirmView(true);
		privilegeComp.setBtnRejectView(true);
		privilegeComp.setBtnDeleteView(true);
		userPrivilege.setPrivilegeComponent(privilegeComp);
		
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Pre destroy.
	 */
	@PreDestroy
	public void preDestroy(){
		endConversation();
		scheduleExceptionDetailList = null;
		lstInstitutionsSecurityForFilter = null;
		lstInstitutionTypeForFilter = null;
		userAccountList = null;
		
		
		// Get current size of heap in bytes
		long heapSize = Runtime.getRuntime().totalMemory();

		// Get maximum size of heap in bytes. The heap cannot grow beyond this size.
		// Any attempt will result in an OutOfMemoryException.
		long heapMaxSize = Runtime.getRuntime().maxMemory();

		// Get amount of free memory within the heap in bytes. This size will increase
		// after garbage collection and decrease as new objects are created.
		long heapFreeSize = Runtime.getRuntime().freeMemory();
		
		
		log.info("******************PRE DESTROY****************************");
		log.info("heapSize  	=" + heapSize);
		log.info("heapMaxSize   =" + heapMaxSize);
		log.info("heapFreeSize  =" + heapFreeSize);
		log.info("******************************************************************");
		
	}


	/**
	 * Gets the schedule exception type list.
	 *
	 * @return the schedule exception type list
	 */
	public List<ScheduleExceptionType> getScheduleExceptionTypeList() {
		return ScheduleExceptionType.list;
	}
	
	/**
	 * Gets the schedule exception access type list.
	 *
	 * @return the schedule exception access type list
	 */
	public List<ScheduleExceptionAccessType> getScheduleExceptionAccessTypeList(){
		return ScheduleExceptionAccessType.list;
	}

	/**
	 * Gets the schedule exception state type list.
	 *
	 * @return the schedule exception state type list
	 */
	public List<ScheduleExceptionStateType> getScheduleExceptionStateTypeList() {
		return ScheduleExceptionStateType.getSomeElements(
				ScheduleExceptionStateType.REGISTERED,
				ScheduleExceptionStateType.CONFIRMED,
				ScheduleExceptionStateType.REJECT,
				ScheduleExceptionStateType.DELETED,
				ScheduleExceptionStateType.REVOCATE);
	}
	
	/**
	 * Load componentes.
	 */
	public void loadComponentes() {
		hystoricalState = new HistoricalState();
		scheduleExceptionFilter = new ScheduleExceptionFilter();
		scheduleExceptionFilter.setInitialDate(CommonsUtilities.currentDate());
		scheduleExceptionFilter.setFinalDate(CommonsUtilities.currentDate());
		userAccountList = new ArrayList<UserAccount>();
	}

	/**
	 * Request register action.
	 *
	 * @return the string
	 */
	public String requestRegisterAction() {
		blDataModified=false;
		this.setOperationType(ViewOperationsType.REGISTER.getCode());
		tmpList.clear();
		this.scheduleExceptionSession = new ScheduleException();
		
		scheduleExceptionSession.setInitialDate(CommonsUtilities.currentDate());
		scheduleExceptionSession.setEndDate(CommonsUtilities.currentDate());
				
		minHourForException = ScheduleExceptionAccessType.NORMAL_EXCEPTION.getInitHour();
		maxHourForException = ScheduleExceptionAccessType.NORMAL_EXCEPTION.getEndHour();		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, minHourForException);
		cal.set(Calendar.MINUTE, 0);
		scheduleExceptionSession.setInitialHour(cal.getTime());		
		cal.set(Calendar.HOUR_OF_DAY, maxHourForException);
		scheduleExceptionSession.setEndHour(new Date(cal.getTime().getTime()-60000));
		
		lstInstitutionTypeForFilter=null;
		lstInstitutionsSecurityForFilter=null;
		institutionType = false;
		userAccountFilter = new UserAccountFilter();
		isDisabledWeekends = false;		
		loadComponentes();
		return "scheduleExceptionMgmtRule";
	}

	/**
	 * Solicita modificacion.
	 * 
	 * @return the string
	 */
	public String requestModifyAction() {
		this.setOperationType(ViewOperationsType.MODIFY.getCode());
		blDataModified=BooleanType.YES.getBooleanValue();
		try {
			tmpList.clear();
			exceptionsRemoved=false;
			if (Validations.validateIsNull(this.scheduleExceptionSession)) {
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,PropertiesConstants.ERROR_SELECT_MODIFY, null);
				JSFUtilities.showValidationDialog();
				return null;
			}
			if (!this.scheduleExceptionSession.getState().equals(ScheduleExceptionStateType.REGISTERED.getCode())) {
					showMessageOnDialog(
							PropertiesConstants.DIALOG_HEADER_ALERT, null,
							PropertiesConstants.ERROR_ADM_SCHEDULE_EXCEPTION_REVOKE,null);
					JSFUtilities.showValidationDialog();
					return null;
			}
			formState=false;
			loadComponentes();			
			scheduleExceptionSession = systemServiceFacade.getScheduleExceptionServiceFacade(scheduleExceptionSession);
			userAccountFilter.setUserAccountType(scheduleExceptionSession.getClientType());
			changeUserTypeForFilterListener();
			userAccountFilter.setInstitutionType(scheduleExceptionSession.getInstitutionTypeFk());
			changeInstitutionTypeForFilterListener();
			userAccountFilter.setIdInstitutionSecurity(scheduleExceptionSession.getIdInstitutionFk());
			
			for (ScheduleExceptionDetail scheduleExceptionDetail : this.scheduleExceptionSession
					.getScheduleExceptionDetails()) {
				if(ScheduleExceptionDetailStateType.REGISTERED.getCode().equals(scheduleExceptionDetail.getState())){
					scheduleExceptionDetail.setSelected(BooleanType.YES.getBooleanValue());
				}else if(ScheduleExceptionDetailStateType.CANCELADO.getCode().equals(scheduleExceptionDetail.getState())){
					scheduleExceptionDetail.setSelected(BooleanType.NO.getBooleanValue());
					tmpList.add(scheduleExceptionDetail);
				}
			}
			this.setOperationType(ViewOperationsType.MODIFY.getCode());
			for(ScheduleExceptionDetail exceptionDetail:scheduleExceptionSession.getScheduleExceptionDetails()){
				if(exceptionDetail.isSelected()){
					exceptionsRemoved=true;
					break;
				}
			}
			if(tmpList.size() > 0){
				scheduleExceptionSession.getScheduleExceptionDetails().removeAll(tmpList);
				tmpList.clear();
			}
			return "scheduleExceptionMgmtRule";
		} catch (Exception ex) {
			//ex.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		return null;
	}
	
	public void beforeConfirmListener(ActionEvent actionEvent){
		hideDialogs();
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		
		if (Validations.validateIsNull(this.scheduleExceptionSession)) {
			showMessageOnDialog(null, null,PropertiesConstants.ERROR_SELECT_MODIFY, null);
			JSFUtilities.showValidationDialog();
			return;
		}else if(!scheduleExceptionSession.getState().equals(ScheduleExceptionStateType.REGISTERED.getCode())){
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT, null, 
					PropertiesConstants.SYSTEM_SCHEDULE_EXCEPTION_CONFIRM_ERROR, null);
			JSFUtilities.showValidationDialog();
			return;
 		}	
		showOtherMotive = false;
		this.setOperationType(ViewOperationsType.CONFIRM.getCode());
		generateMessageListener();
	}
	
	public void beforeRejectListener(ActionEvent actionEvent){
		hideDialogs();
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		
		if (Validations.validateIsNull(this.scheduleExceptionSession)) {
			showMessageOnDialog(null, null,PropertiesConstants.ERROR_SELECT_MODIFY, null);
			JSFUtilities.showValidationDialog();
			return;
		}else if(!scheduleExceptionSession.getState().equals(ScheduleExceptionStateType.REGISTERED.getCode())){
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT, null, 
					PropertiesConstants.SYSTEM_SCHEDULE_EXCEPTION_REJECT_ERROR, null);
			JSFUtilities.showValidationDialog();
			return;
 		}	
		showOtherMotive = false;
		this.setOperationType(ViewOperationsType.REJECT.getCode());
		generateMessageListener();
	}

	/**
	 * Gets the tipo usuarios.
	 * 
	 * @return the tipo usuarios
	 */
	public List<UserAccountType> getUsersAccountTypeList() {
		return UserAccountType.list;
	}

	/**
	 * Before show user accounts listener.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void beforeShowUserAccountsListener() {
		userAccountDataModel=null;
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		hideDialogs();
		boolean iserrors = false;
		if(Validations.validateIsNullOrNotPositive(scheduleExceptionSession.getSystem().getIdSystemPk())){
			String str=PropertiesUtilities.getMessage(PropertiesConstants.WARNING_ADM_EXCEPTIONSCHEDULE_SYSTEM_NO_SELECTED);
			JSFUtilities.addContextMessage("formExcepcion:selectSystem", FacesMessage.SEVERITY_ERROR, str, str);
			JSFUtilities.showComponent("cnfEndTransactionSamePage");
			JSFUtilities.showMessageOnDialog(null, null, PropertiesConstants.WARNING_ADM_EXCEPTIONSCHEDULE_SYSTEM_NO_SELECTED, null);
			iserrors = true;
		}else{
			systemPkForFilterListener = scheduleExceptionSession.getSystem().getIdSystemPk();
			userAccountFilter.setUserAccountState(UserAccountStateType.CONFIRMED.getCode());
		}
		if(Validations.validateIsNull(userAccountFilter.getUserAccountType()) || userAccountFilter.getUserAccountType()<0){
			String str=PropertiesConstants.WARNING_ADM_EXCEPTIONSCHEDULE_TYPE_OF_CLIENT_NO_SELECTED;
			
			JSFUtilities.addContextMessage("formExcepcion:cboUserType", FacesMessage.SEVERITY_ERROR, str, str);
			JSFUtilities.showComponent("cnfEndTransactionSamePage");
			iserrors = true;
		}
		if(Validations.validateIsNull(userAccountFilter.getInstitutionType()) || userAccountFilter.getInstitutionType()<0){
			String str=PropertiesConstants.WARNING_ADM_EXCEPTIONSCHEDULE_TYPE_OF_INSTITUTION_NO_SELECTED;
			JSFUtilities.addContextMessage("formExcepcion:cboInstitutionType", FacesMessage.SEVERITY_ERROR, str, str);
			JSFUtilities.showComponent("cnfEndTransactionSamePage");
			iserrors = true;
		}
		if(Validations.validateIsNull(userAccountFilter.getIdInstitutionSecurity()) || userAccountFilter.getIdInstitutionSecurity()<0){
			String str=PropertiesConstants.WARNING_ADM_EXCEPTIONSCHEDULE_INSTITUTION_NO_SELECTED;
			JSFUtilities.addContextMessage("formExcepcion:cboInstitution", FacesMessage.SEVERITY_ERROR, str, str);
			JSFUtilities.showComponent("cnfEndTransactionSamePage");
			iserrors = true;
		}
		if(iserrors){
			return;
		}
		else
		 {
			JSFUtilities.showComponent("dialogUsserAccountForm:searchUserAccounts");
			if (Validations.validateIsNotNull(userAccountDataModel) && Validations.validateIsNotNullAndNotEmpty(((List<GenericDataModel>) userAccountDataModel.getWrappedData()))) {
				((List<GenericDataModel>) userAccountDataModel.getWrappedData()).removeAll((List<GenericDataModel>) userAccountDataModel.getWrappedData());
			}
			// searchUserAccounts();
			return;
		} 
	}

	/**
	 * Buscar excepciones horarios.
	 */
	public void searchScheduleExceptions() {
		try {
			List<ScheduleException> scheduleExceptionList = systemServiceFacade
					.getScheduleExceptionServiceFacade(scheduleExceptionFilter);
			scheduleExceptionDataModel = new GenericDataModel<ScheduleException>(
					scheduleExceptionList);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Generate user account massive.
	 */
	@SuppressWarnings("unchecked")
	public void generateUserAccountMassive() {
		this.setIsMassiveEvent(!this.getIsMassiveEvent());
		for (UserAccount userAccount : (List<UserAccount>) this
				.getUserAccountDataModel().getWrappedData()) {
			if (!haveScheduleExceptionBySystem(this.getScheduleExceptionSession().getSystem().getIdSystemPk(), userAccount)) {
				userAccount.setSelected(this.getIsMassiveEvent());
			}
		}
	}

	/**
	 * Search user accounts.
	 *
	 * @author mmacalupu Buscar usuarios. this Method is used for search users
	 * with some parameters and navigations' rules.
	 */
	public void searchUserAccounts() {
		this.scheduleExceptionFilter.setScheduleExceptionState(ScheduleExceptionStateType.REGISTERED.getCode());
		this.userAccountFilter.setScheduleExceptionFilter(this.scheduleExceptionFilter);
		this.userAccountFilter.setUserAccountState(UserAccountStateType.CONFIRMED.getCode());
		
		List<UserAccount> userAccountListSearch = null;
		try {
			userAccountListSearch = this.userServiceFacade.searchUsersWithScheduleExceptionByFilterServiceFacade(this.userAccountFilter);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		
		if (Validations.validateIsNotNull(userAccountListSearch) && !userAccountListSearch.isEmpty()
				&& Validations.validateIsNotNull(this.getScheduleExceptionSession().getScheduleExceptionDetails())) {
			
			for (UserAccount userAccount : userAccountListSearch) {
				jump: 
				for (ScheduleExceptionDetail scheduleExceptionDetail : this.getScheduleExceptionSession().getScheduleExceptionDetails()) {
					if (scheduleExceptionDetail.getUserAccount().equals(userAccount)) {
						userAccount.setSelected(BooleanType.YES.getBooleanValue());
						break jump;
					}
				}
			}
		}
		this.userAccountDataModel = new GenericDataModel<UserAccount>(userAccountListSearch);
	}
	
	/**
	 * Change system selected.
	 */
	public void changeSystemSelected(){
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		if(this.getScheduleExceptionSession().getScheduleExceptionDetails() != null){
			this.getScheduleExceptionSession().getScheduleExceptionDetails().removeAll(this.getScheduleExceptionSession().getScheduleExceptionDetails());
			//scheduleExceptionSession=new ScheduleException();
		}
	}

	/**
	 * Validate if user exist in list schedule detail.
	 *
	 * @param scheduleExceptionDetailList the schedule exception detail list
	 * @param userAccount the user account
	 * @return true, if validate if user exist in list schedule detail
	 */
	public boolean validateIfUserExistInListScheduleDetail(
			List<ScheduleExceptionDetail> scheduleExceptionDetailList,
			UserAccount userAccount) {
		if (Validations.validateIsNull(scheduleExceptionDetailList)) {
			return Boolean.FALSE;
		}
		for (ScheduleExceptionDetail scheduleExceptionDetail : scheduleExceptionDetailList) {
			if (scheduleExceptionDetail.getUserAccount().equals(userAccount)) {
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}

	/**
	 * Seleccionar usuario.
	 * 
	 * @param usuario
	 *            the usuario
	 */
	public void selectUserAccount(UserAccount usuario) {
	}

	/**
	 * Agregar usuarios seleccionados.
	 */
	public void addUsersAccountsSelectedListener() {
		exceptionsRemoved=true;
		List<ScheduleExceptionDetail> userSelectedInScheduleExceptionList = new ArrayList<ScheduleExceptionDetail>();
		for (UserAccount userAccount : (List<UserAccount>) this.userAccountDataModel
				.getDataList()) {
			if (userAccount.isSelected()) {
				if (!this.validateIfUserExistInListScheduleDetail(this
						.getScheduleExceptionSession()
						.getScheduleExceptionDetails(), userAccount)) {
					ScheduleExceptionDetail scheduleExceptionDetail = new ScheduleExceptionDetail();
					scheduleExceptionDetail.setUserAccount(userAccount);
					scheduleExceptionDetail.setSelected(BooleanType.YES
							.getBooleanValue());
					userSelectedInScheduleExceptionList
							.add(scheduleExceptionDetail);
				}
			} else {
				if (this.validateIfUserExistInListScheduleDetail(this
						.getScheduleExceptionSession()
						.getScheduleExceptionDetails(), userAccount)) {
					q: for (ScheduleExceptionDetail scheduleExceptionDetail : this
							.getScheduleExceptionSession()
							.getScheduleExceptionDetails()) {
						if (scheduleExceptionDetail.getUserAccount().equals(
								userAccount)) {
							this.getScheduleExceptionSession()
									.getScheduleExceptionDetails()
									.remove(scheduleExceptionDetail);
							break q;
						}
					}
				}
			}
		}
		if (Validations.validateIsNull(this.getScheduleExceptionSession()
				.getScheduleExceptionDetails())
				|| this.getScheduleExceptionSession()
						.getScheduleExceptionDetails().isEmpty()) {
			this.getScheduleExceptionSession().setScheduleExceptionDetails(
					userSelectedInScheduleExceptionList);
		} else {
			this.getScheduleExceptionSession().getScheduleExceptionDetails()
					.addAll(userSelectedInScheduleExceptionList);
		}
		if(Validations.validateListIsNullOrEmpty(userSelectedInScheduleExceptionList)){
			JSFUtilities.showEndTransactionSamePageDialog();
			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT,null,PropertiesConstants.ERROR_RECORD_NO_SELECTED, null);
			return;
		}
		setFormState(true);
		hideDialogs();
		JSFUtilities.executeJavascriptFunction("PF('busquedadUsuarioWidget').hide();");
		userAccountFilter.setLoginUserAccount("");
		userAccountFilter.setUserName(null);
		userAccountFilter.setFirtsLastName(null);
		userAccountFilter.setSecondLastName(null);
	}
	
	/** The exceptions removed. */
	private Boolean exceptionsRemoved=true;
	
	/**
	 * Gets the exceptions removed.
	 *
	 * @return the exceptionsRemoved
	 */
	public Boolean getExceptionsRemoved() {
		return exceptionsRemoved;
	}

	/**
	 * Sets the exceptions removed.
	 *
	 * @param exceptionsRemoved the exceptionsRemoved to set
	 */
	public void setExceptionsRemoved(Boolean exceptionsRemoved) {
		this.exceptionsRemoved = exceptionsRemoved;
	}
	
	/** The tmp list. */
	List<ScheduleExceptionDetail> tmpList=new ArrayList<ScheduleExceptionDetail>();
	/**
	 * Retirar usuario.
	 *
	 * @param scheduleExceptionDetailparam the schedule exception detailparam
	 */
	public void removeUserAccountListener(ScheduleExceptionDetail scheduleExceptionDetailparam) {
		hideDialogs();
 		if (scheduleExceptionDetailparamtemp.isSelected()) {
			scheduleExceptionDetailparamtemp.setSelected(BooleanType.NO.getBooleanValue());
			Object[] userCode={scheduleExceptionDetailparamtemp.getUserAccount().getLoginUser()};
			JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS, null,PropertiesConstants.SUCCESS_USER_ADD , userCode);
			scheduleExceptionDetailparamtemp.getUserAccount().setSelected(Boolean.FALSE);
			if(isModify()){
				tmpList.add(scheduleExceptionDetailparamtemp);
				scheduleExceptionSession.getScheduleExceptionDetails().removeAll(tmpList);
			}else{
				scheduleExceptionSession.getScheduleExceptionDetails().remove(scheduleExceptionDetailparamtemp);
			}
			
		} else {
			scheduleExceptionDetailparamtemp.setSelected(BooleanType.YES.getBooleanValue());
			JSFUtilities.showMessageOnDialog(null, null,PropertiesConstants.SUCCESS_USER_REMOVE , null);
		}
 		if(scheduleExceptionSession.getScheduleExceptionDetails() != null && scheduleExceptionSession.getScheduleExceptionDetails().size() > 0)
 			exceptionsRemoved = true;
 		else
 			exceptionsRemoved = false;
 		
		setFormState(true);
		//scheduleExceptionDetailparam = scheduleExceptionDetailparamtemp;
		JSFUtilities.showComponent("cnfEndTransactionSamePage");
	}

	/**
	 * Genera mensaje.
	 *
	 * @param operationType the operation type
	 */
	public void generateMessageListener(Integer operationType) {
		hideDialogs();
		this.setOperationType(operationType);
		generateMessageListener();
	}

	/**
	 * Ver detalle excepcion.
	 * 
	 * @param scheduleException
	 *            the e
	 * @return the string
	 */
	public String seeScheduleExceptionDetailAction(ScheduleException scheduleException) {
		List<ScheduleExceptionDetail> exceptionDetails = new ArrayList<ScheduleExceptionDetail>();
		try {
			//showMotives();//for to show motivo descrpcions in detail scren
			lstMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.SCHEDULE_EXCEPTION_DELETE_MOTIVES.getCode());
			this.scheduleExceptionSession = scheduleException;
			scheduleExceptionSession = systemServiceFacade.getScheduleExceptionServiceFacade(scheduleExceptionSession);
			if(scheduleExceptionSession != null){
				if( !(ScheduleExceptionStateType.DELETED.getCode().equals(scheduleExceptionSession.getState())
					|| ScheduleExceptionStateType.REVOCATE.getCode().equals(scheduleExceptionSession.getState()))){
					for (ScheduleExceptionDetail exceptionDetail : scheduleExceptionSession.getScheduleExceptionDetails()) {
						if(exceptionDetail.getState().equals(ScheduleExceptionDetailStateType.CANCELADO.getCode())){
							exceptionDetails.add(exceptionDetail);
						}
					} 
				}
				scheduleExceptionSession.getScheduleExceptionDetails().removeAll(exceptionDetails);
			}
			return "scheduleExceptionDetailRule";
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		return null;
	}

	/**
	 * Before delete listener.
	 *
	 * @param e the e
	 */
	public void beforeDeleteListener(ActionEvent e) {
		hideDialogs();
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		
		if (Validations.validateIsNull(this.scheduleExceptionSession)) {
			showMessageOnDialog(null, null,PropertiesConstants.ERROR_SELECT_MODIFY, null);
			JSFUtilities.showValidationDialog();
			return;
		}else if(!scheduleExceptionSession.getState().equals(ScheduleExceptionStateType.CONFIRMED.getCode())){
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_ALERT, null, 
					PropertiesConstants.SYSTEM_SCHEDULE_EXCEPTION_DELETE_ERROR, null);
			JSFUtilities.showValidationDialog();
			return;
 		}	
		this.setOperationType(ViewOperationsType.DELETE.getCode());		
		generateMessageListener();
		changeSelectionMotiveListener();
	}

	/**
	 * Genera mensaje.
	 */
	public void generateMessageListener() {
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		try{
			hideDialogs();
			if (this.scheduleExceptionSession == null) {
				if (isDelete()) {
					showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,	PropertiesConstants.ERROR_SELECT_DELETE, null);
					JSFUtilities.showValidationDialog();
					return;
				}
			} else if (Validations.validateIsNotNullAndNotEmpty(this.scheduleExceptionSession.getState())
					&& this.scheduleExceptionSession.getState().equals(	ScheduleExceptionStateType.REVOCATE.getCode())) {
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,	PropertiesConstants.ERROR_ADM_SCHEDULE_EXCEPTION_DELETE,null);
				JSFUtilities.showValidationDialog();
				return;
			} else if (Validations.validateIsNotNullAndNotEmpty(this.scheduleExceptionSession.getState())
						&& this.scheduleExceptionSession.getState().equals(	ScheduleExceptionStateType.DELETED.getCode())) {
					showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,	PropertiesConstants.ERROR_ADM_SCHEDULE_EXCEPTION_DELETE,null);
					JSFUtilities.showValidationDialog();
					return;
			} else if(isRegister() || isModify()){
				if((this.scheduleExceptionSession.getScheduleExceptionDetails() != null && this.scheduleExceptionSession.getScheduleExceptionDetails().size() == 0) ||
				this.scheduleExceptionSession.getScheduleExceptionDetails() == null){
					JSFUtilities.showEndTransactionSamePageDialog();
					JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,PropertiesConstants.DIALOG_USER_ERROR,null);
					return;
				}
			}
			if (this.isRegister()) {
				showMessageOnDialog(
						PropertiesConstants.DIALOG_HEADER_REGISTER,
						null,
						PropertiesConstants.SCHEDULEEXCEPTION_CONFIRM_REGISTER, null);
				JSFUtilities.showComponent("formExcepcion:cnfAdministrarExcepcion");
			} else if (this.isModify()) {
				Object[] arrDatosCuerpo = { this.scheduleExceptionSession.getIdScheduleExceptionPk() };
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_CONFIRM, null,
									PropertiesConstants.SCHEDULEEXCEPTION_CONFIRM_MODIFY, arrDatosCuerpo);
				JSFUtilities.showComponent("formExcepcion:cnfAdministrarExcepcion");
			} else if (this.isDelete()) {				
				if (this.getScheduleExceptionSession().getState().equals(ScheduleStateType.REGISTERED.getCode()) ||
					this.getScheduleExceptionSession().getState().equals(ScheduleStateType.CONFIRMED.getCode())) {
					showMessageOnDialog(PropertiesConstants.LBL_MOTIVE_DELETE,null, null, null);
					//showMotives();
					lstMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.SCHEDULE_EXCEPTION_DELETE_MOTIVES.getCode());
					hystoricalState = new HistoricalState();
					hystoricalState.setMotive(new Integer(-1));
					this.setShowOtherMotive(false);
					JSFUtilities.showComponent("frmMotive:dlgMotive");
				}
			} else if(this.isConfirm()){
				hystoricalState = new HistoricalState();
				hystoricalState.setMotive(new Integer(-1));
				Object [] data = {scheduleExceptionSession.getIdScheduleExceptionPk()};
				showMessageOnDialog(
						PropertiesConstants.LBL_CONFIRM_HEADER_CONFIRM,
						null,
						PropertiesConstants.SYSTEM_SCHEDULE_EXCEPTION_CONFIRM_MESSAGE,
						data);
				JSFUtilities.showComponent("frmConfirm:cnfConfirm");				
			} else if(this.isReject()){
				showMessageOnDialog(PropertiesConstants.LBL_MOTIVE_REJECT,null, null, null);
				lstMotives = systemServiceFacade.listParameterByDefinitionServiceFacade(DefinitionType.SCHEDULE_EXCEPTION_REJECT_MOTIVES.getCode());
				hystoricalState = new HistoricalState();
				hystoricalState.setMotive(new Integer(-1));
				this.setShowOtherMotive(false);
				JSFUtilities.showComponent("frmMotive:dlgMotive");
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	

	/**
	 * Gets the schedule created profiles.
	 *
	 * @return the scheduleCreatedProfiles
	 */
	public List<UserAccount> getScheduleCreatedProfiles() {
		return scheduleCreatedProfiles;
	}

	/**
	 * Sets the schedule created profiles.
	 *
	 * @param scheduleCreatedProfiles the scheduleCreatedProfiles to set
	 */
	public void setScheduleCreatedProfiles(List<UserAccount> scheduleCreatedProfiles) {
		this.scheduleCreatedProfiles = scheduleCreatedProfiles;
	}

	/**
	 * Gets the schedules not creatd profiles.
	 *
	 * @return the schedulesNotCreatdProfiles
	 */
	public List<UserAccount> getSchedulesNotCreatdProfiles() {
		return schedulesNotCreatdProfiles;
	}

	/**
	 * Sets the schedules not creatd profiles.
	 *
	 * @param schedulesNotCreatdProfiles the schedulesNotCreatdProfiles to set
	 */
	public void setSchedulesNotCreatdProfiles(
			List<UserAccount> schedulesNotCreatdProfiles) {
		this.schedulesNotCreatdProfiles = schedulesNotCreatdProfiles;
	}
	
	/**
	 * Used to change final Date When initial date is greater than final Date.
	 */
	public void changeDate(){
		if(scheduleExceptionFilter.getInitialDate() != null && scheduleExceptionFilter.getInitialDate().after(CommonsUtilities.currentDate())){
			scheduleExceptionFilter.setFinalDate(scheduleExceptionFilter.getInitialDate());
		}
	}
	
	/**
	 * save the Exception of Schedule.
	 */
	@LoggerAuditWeb
	public void saveScheduleExceptionAction() {
		try {
			scheduleCreatedProfiles=new ArrayList<UserAccount>();
			schedulesNotCreatdProfiles=new ArrayList<UserAccount>();
			ScheduleException scheduleException = this.scheduleExceptionSession;
			//Mantis
			Date date = systemServiceFacade.getLastModifedDetails(scheduleExceptionSession.getIdScheduleExceptionPk());
					if(date != null){
						if(scheduleExceptionSession.getLastModifyDate().compareTo(date) < 0){
							showMessageOnDialog(null, null,
									PropertiesConstants.ERROR_SELECTION_ALREADY_MODIFIED, null);
							JSFUtilities.showValidationDialog();
							return ;
					}
				}
			String messageReturn = null;
			scheduleExceptionDetailList = new ArrayList<ScheduleExceptionDetail>();
			//selected users for creating the exception
			//List<Integer> userIDs=new ArrayList<Integer>();
			//for(ScheduleExceptionDetail exceptionDetail:scheduleExceptionSession.getScheduleExceptionDetails()){
			//	userIDs.add(exceptionDetail.getUserAccount().getIdUserPk());
			//}
			//getting the usersList,checking with selected system is access is their or not for the selected users
			//List<UserAccount> userAccoutsList=systemServiceFacade.checkUserWithSystemProfileServiceFacade(scheduleException.getSystem().getIdSystemPk(),userIDs);
			//List<Integer> listIds=new ArrayList<Integer>();
			//for(UserAccount userAccount:userAccoutsList){
			//	listIds.add(userAccount.getIdUserPk());
			//}
			for (ScheduleExceptionDetail scheduleExceptionDetail : scheduleException.getScheduleExceptionDetails()) {
				//if(listIds.contains(scheduleExceptionDetail.getUserAccount().getIdUserPk())){
					if (scheduleExceptionDetail.isSelected()) {
						scheduleExceptionDetail.setScheduleException(scheduleException);
						scheduleExceptionDetail.setRegistryUser(userInfo.getUserAccountSession().getUserName());
						scheduleExceptionDetail.setDateRegister(new Date());
						scheduleExceptionDetail.setState(ScheduleExceptionDetailStateType.REGISTERED.getCode());
						scheduleExceptionDetail.setLastModifyPriv(userInfo.getLastModifyPriv());
					} else {
							scheduleExceptionDetail.setScheduleException(scheduleException);
							scheduleExceptionDetail.setRegistryUser(userInfo.getUserAccountSession().getUserName());
							scheduleExceptionDetail.setDateRegister(new Date());
							scheduleExceptionDetail.setState(ScheduleExceptionDetailStateType.CANCELADO.getCode());
							scheduleExceptionDetailList.add(scheduleExceptionDetail);
					}
					scheduleExceptionDetailList.add(scheduleExceptionDetail);
					//userIDs.remove(scheduleExceptionDetail.getUserAccount().getIdUserPk());
				//}
			}
			//If the exception modified, if user removed from the exception list we change the status to deleted state
			for (ScheduleExceptionDetail scheduleExceptionDetail : tmpList) {
				scheduleExceptionDetail.setScheduleException(scheduleException);
				scheduleExceptionDetail.setRegistryUser(userInfo.getUserAccountSession().getUserName());
				scheduleExceptionDetail.setDateRegister(new Date());
				scheduleExceptionDetail.setState(ScheduleExceptionDetailStateType.CANCELADO.getCode());
				scheduleExceptionDetailList.add(scheduleExceptionDetail);
			}
			//if(scheduleExceptionDetailList.size()>0){
				scheduleException.setState(ScheduleExceptionStateType.REGISTERED.getCode());
				scheduleException.setRegistryUser(userInfo.getUserAccountSession().getUserName());
				scheduleException.setRegistryDate(new Date());
				scheduleException.setScheduleExceptionDetails(scheduleExceptionDetailList);
				scheduleException.setClientType(userAccountFilter.getUserAccountType());
				scheduleException.setIdInstitutionFk(userAccountFilter.getIdInstitutionSecurity());
				scheduleException.setInstitutionTypeFk(userAccountFilter.getInstitutionType());
				//fixed the exception access type as NORMAL_EXCEPTION 
				//scheduleException.setExceptionAccessType(ScheduleExceptionAccessType.NORMAL_EXCEPTION.getCode());
			//}
			String message="",subject="";
			if (isRegister()) {
					systemServiceFacade.registerScheduleExceptionServiceFacade(scheduleException);
					messageReturn = PropertiesConstants.SCHEDULEEXCEPTION_REGISTER_SUCCESS;

				/*UserAccount userAccount=null;
				for(UserAccount userAcc:userAccoutsList){
					Integer index=null;
					for(int i=0;i<userIDs.size();i++){
						if(userIDs.get(i).equals(userAcc.getIdUserPk())){
							index=i;
							break;
						}
					}
					if(userIDs.contains(Integer.valueOf(userAcc.getIdUserPk()))){
						userAccount=new UserAccount();
						userAccount.setIdUserPk(userAccoutsList.get(index).getIdUserPk());
						userAccount.setLoginUser(userAccoutsList.get(index).getLoginUser());
						scheduleCreatedProfiles.add(userAccount);
					}
					else{
						userAccount=new UserAccount();
						userAccount.setIdUserPk(userAccoutsList.get(index).getIdUserPk());
						userAccount.setLoginUser(userAccoutsList.get(index).getLoginUser());
						schedulesNotCreatdProfiles.add(userAccount);
					}
				}
				if(userAccoutsList.size()<1){
					for(ScheduleExceptionDetail exceptionDetail:scheduleExceptionSession.getScheduleExceptionDetails()){
						userAccount=new UserAccount();
						userAccount.setIdUserPk(exceptionDetail.getUserAccount().getIdUserPk());
						userAccount.setLoginUser(exceptionDetail.getUserAccount().getLoginUser());
						schedulesNotCreatdProfiles.add(userAccount);
					}
				}*/
			} else if (isModify()) {
				//Mantis
				Date tempDate = systemServiceFacade.getLastModifedDetails(scheduleException.getIdScheduleExceptionPk());
					if(tempDate != null){
						if(scheduleException.getLastModifyDate().compareTo(date) < 0){
							showMessageOnDialog(null, null,PropertiesConstants.SCHEDULE_EXCEPTION_ERROR_MODIFIED_RECENT, null);
							JSFUtilities.showEndTransactionDialog();
							this.searchScheduleExceptions();
							return ;
						}
					}
				systemServiceFacade.modifyScheduleExceptionServiceFacade(scheduleException);
				messageReturn = PropertiesConstants.SCHEDULEEXCEPTION_MODIFY_SUCCESS;
				List<ScheduleException> scheduleExceptionList = systemServiceFacade.getScheduleExceptionServiceFacade(scheduleExceptionFilter);
				scheduleExceptionDataModel = new GenericDataModel<ScheduleException>(scheduleExceptionList);
			}
			Object[] arrDatosCuerpo = { scheduleException.getIdScheduleExceptionPk() };
			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS, null, messageReturn, arrDatosCuerpo);
			JSFUtilities.showEndTransactionDialog();
			scheduleExceptionDataModel = null;
			scheduleExceptionFilter = new ScheduleExceptionFilter();
			scheduleExceptionFilter.setInitialDate(CommonsUtilities.currentDate());
			scheduleExceptionFilter.setFinalDate(CommonsUtilities.currentDate());
			tmpList.clear();
			//scheduleExcp
			//JSFUtilities.showComponent("formExcepcion:addScheduleExc");
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Delete schedule exception listener. this method change schedule
	 * exception's status (state: DELETE)
	 */
	@LoggerAuditWeb
	public void deleteScheduleExceptionListener() {
		try {
			hideDialogs();
			//Mantis
			Date date =systemServiceFacade.getLastModifedDetails(scheduleExceptionSession.getIdScheduleExceptionPk());
					if(date != null){
						if(scheduleExceptionSession.getLastModifyDate().compareTo(date) < 0){
							showMessageOnDialog(null, null,PropertiesConstants.SCHEDULE_EXCEPTION_ERROR_MODIFIED_RECENT, null);
							JSFUtilities.showEndTransactionDialog();
							this.searchScheduleExceptions();
							return ;
					}
			}
			this.scheduleExceptionSession = this.systemServiceFacade.getScheduleExceptionServiceFacade(this.scheduleExceptionSession);
			this.scheduleExceptionSession.setState(ScheduleExceptionStateType.DELETED.getCode());
			this.scheduleExceptionSession.setSituation(ScheduleSituationType.ACTIVE.getCode());
			
			List<ScheduleExceptionDetail>  lstScheduleExceptionDetails = this.scheduleExceptionSession.getScheduleExceptionDetails();
			
			for(ScheduleExceptionDetail sed : lstScheduleExceptionDetails){
				sed.setState(ScheduleExceptionDetailStateType.CANCELADO.getCode());
			}
			this.scheduleExceptionSession.setScheduleExceptionDetails(lstScheduleExceptionDetails);
			this.scheduleExceptionSession.setRemoveMotive(hystoricalState.getMotive());
			this.scheduleExceptionSession.setBlockOtherMotive(hystoricalState.getMotiveDescription());
			
			this.systemServiceFacade.updateScheduleExceptionStateServiceFacade(scheduleExceptionSession);
			
			Object[] arrBodyData = { this.scheduleExceptionSession.getIdScheduleExceptionPk() };
			
			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS, null,
					PropertiesConstants.SYSTEM_SCHEDULE_EXCEPTION_DELETE_SUCCESS,
					arrBodyData);
			JSFUtilities.showEndTransactionDialog();
			this.searchScheduleExceptions();
			
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	@LoggerAuditWeb
	public void confirmScheduleExceptionListener(){
		try {
			hideDialogs();
			Date date = systemServiceFacade.getLastModifedDetails(scheduleExceptionSession.getIdScheduleExceptionPk());
			if(date.after(scheduleExceptionSession.getLastModifyDate())){
				showMessageOnDialog(null, null,PropertiesConstants.SCHEDULE_EXCEPTION_ERROR_MODIFIED_RECENT, null);
				JSFUtilities.showEndTransactionDialog();
				this.searchScheduleExceptions();
				return ;
			}
			this.scheduleExceptionSession = this.systemServiceFacade.getScheduleExceptionServiceFacade(this.scheduleExceptionSession);			
			this.scheduleExceptionSession.setState(ScheduleStateType.CONFIRMED.getCode());
			this.scheduleExceptionSession.setSituation(ScheduleSituationType.ACTIVE.getCode()); 
			
			for (ScheduleExceptionDetail scheduleExceptionDetail : this.scheduleExceptionSession.getScheduleExceptionDetails()) {
				scheduleExceptionDetail.setState(ScheduleExceptionDetailStateType.CONFIRMED.getCode());
			}
				
			//historial state
			this.hystoricalState.setCurrentState(ScheduleStateType.CONFIRMED.getCode());
			ThreadLocalContextHolder.put(
					PropertiesConstants.HISTORICAL_STATE_LOGGER, 
					createHistorialState(
							this.scheduleExceptionSession.getClass().getAnnotation(Table.class).name(), 
							ScheduleStateType.REGISTERED.getCode(),
							this.scheduleExceptionSession.getIdScheduleExceptionPk(), 
							this.hystoricalState));
			
			systemServiceFacade.updateScheduleExceptionStateServiceFacade(this.scheduleExceptionSession);
			
			this.searchScheduleExceptions();
			
			Object[] arrBodyData = {this.scheduleExceptionSession.getIdScheduleExceptionPk()};
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_SUCCESS, 
					null, PropertiesConstants.SYSTEM_SCHEDULE_EXCEPTION_CONFIRM_SUCCESS, 
					arrBodyData);
			
			JSFUtilities.showEndTransactionDialog();		
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	@LoggerAuditWeb
	public void rejectScheduleExceptionListener(){
		try {
			hideDialogs();
			Date date = systemServiceFacade.getLastModifedDetails(scheduleExceptionSession.getIdScheduleExceptionPk());
			if(date.after(scheduleExceptionSession.getLastModifyDate())){
				showMessageOnDialog(null, null,PropertiesConstants.SCHEDULE_EXCEPTION_ERROR_MODIFIED_RECENT, null);
				JSFUtilities.showEndTransactionDialog();
				this.searchScheduleExceptions();
				return ;
			}
			this.scheduleExceptionSession = this.systemServiceFacade.getScheduleExceptionServiceFacade(this.scheduleExceptionSession);
			
			this.scheduleExceptionSession.setState(ScheduleStateType.REJECT.getCode());
			this.scheduleExceptionSession.setSituation(ScheduleSituationType.ACTIVE.getCode());
			this.scheduleExceptionSession.setRejectMotive(this.hystoricalState.getMotive());
			this.scheduleExceptionSession.setRejectOtherMotive(this.hystoricalState.getMotiveDescription());
			
			for(ScheduleExceptionDetail sed : this.scheduleExceptionSession.getScheduleExceptionDetails()){
				sed.setState(ScheduleExceptionDetailStateType.CANCELADO.getCode());
			}
			
			//historial state
			this.hystoricalState.setCurrentState(ScheduleStateType.REJECT.getCode());
			ThreadLocalContextHolder.put(
					PropertiesConstants.HISTORICAL_STATE_LOGGER, 
					createHistorialState(
							this.scheduleExceptionSession.getClass().getAnnotation(Table.class).name(), 
							ScheduleStateType.REGISTERED.getCode(),
							this.scheduleExceptionSession.getIdScheduleExceptionPk(), 
							this.hystoricalState));
			
			systemServiceFacade.updateScheduleExceptionStateServiceFacade(this.scheduleExceptionSession);
			
			this.searchScheduleExceptions();
			
			Object[] arrBodyData = {this.scheduleExceptionSession.getIdScheduleExceptionPk()};
			
			showMessageOnDialog(
					PropertiesConstants.DIALOG_HEADER_SUCCESS, 
					null, PropertiesConstants.SYSTEM_SCHEDULE_EXCEPTION_REJECT_SUCCESS, 
					arrBodyData);
			
			JSFUtilities.showEndTransactionDialog();			
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	
//	/**
//	 * Bloquear excepcion.
//	 */
//	@LoggerAuditWeb
//	public void bloquearExcepcion() {
//		try {
//			ScheduleException scheduleException = systemServiceFacade
//					.getScheduleExceptionServiceFacade(this.scheduleExceptionSession);
//			scheduleException.setState(ScheduleExceptionStateType.LOCKED
//					.getCode());
//			scheduleException.setBlockMotive(hystoricalState.getMotive());//Man
//			
//			systemServiceFacade.updateScheduleExceptionStateServiceFacade(scheduleException);
//			
//			searchScheduleExceptions();
//			showMessageOnDialog(
//					PropertiesConstants.SUCCESS_SCHEDULEEXCEPTION_BLOCK, null,
//					PropertiesConstants.SUCCESS_SCHEDULEEXCEPTION_BLOCK, null);
//			
//		} catch (Exception ex) {
//			excepcion.fire(new ExceptionToCatchEvent(ex));
//		}
//
//	}
//
//	/**
//	 * Desbloquear excepcion.
//	 */
//	@LoggerAuditWeb
//	public void desbloquearExcepcion() {
//		try {
//			ScheduleException scheduleException = systemServiceFacade
//					.getScheduleExceptionServiceFacade(this.scheduleExceptionSession);
//			scheduleException.setState(ScheduleExceptionStateType.REGISTERED
//					.getCode());
//			scheduleException.setUnblockMotive(hystoricalState.getMotive());//Man
//			systemServiceFacade
//					.modifyScheduleExceptionServiceFacade(scheduleException);
//			searchScheduleExceptions();
//			showMessageOnDialog(
//					PropertiesConstants.SUCCESS_SCHEDULEEXCEPTION_UNBLOCK, null,
//					PropertiesConstants.SUCCESS_SCHEDULEEXCEPTION_UNBLOCK, null);
//		} catch (Exception ex) {
//			excepcion.fire(new ExceptionToCatchEvent(ex));
//		}
//	}

	/**
	 * Ocultar dialogos.
	 */
	public void hideDialogs() {
		JSFUtilities.putRequestMap("ejecutaAccion", Boolean.TRUE);
		JSFUtilities.hideGeneralDialogues();
		JSFUtilities.hideComponent("frmConfirm:cnfConfirm");
		JSFUtilities.hideComponent("frmDelete:cnfDelete");
		JSFUtilities.hideComponent("frmReject:cnfReject");
		JSFUtilities.hideComponent("frmMotive:dlgMotive");		
		JSFUtilities.hideComponent("formExcepcion:cnfAdministrarExcepcion");
		JSFUtilities.hideComponent("formExcepcion:cnfremoveUser");
		JSFUtilities.hideComponent("dialogUsserAccountForm:searchUserAccounts");
	}
	
	public void updateCalendars(){				
		if(scheduleExceptionSession.getExceptionAccessType().equals(ScheduleExceptionAccessType.SPECIAL_EXCEPTION.getCode())){
			isDisabledWeekends = false;	
		}else{
			isDisabledWeekends = true;
		}	
	}
	
	/**
	 * Gets the user account state list.
	 *
	 * @return the user account state list
	 */
	public List<UserAccountStateType> getUserAccountStateList() {
		return UserAccountStateType
				.listSomeElements(UserAccountStateType.REGISTERED);
	}
	/**
	 * Carga instituciones por tipo usu.
	 */
	public void loadInstitutionsByUserAccountType() {
		try {
			showInstitutionsByUserAccountType(userAccountFilter	.getUserAccountType());
			if (lstInstitutions.size() == 1
					&& lstInstitutions.get(0).getIdInstitutionTypeFk().equals(InstitutionType.DEPOSITARY.getCode())) {
				userAccountFilter.setIdInstitutionSecurity(lstInstitutions.get(0).getIdInstitutionSecurityPk());
				userAccountFilter.setInstitutionType(lstInstitutions.get(0).getIdInstitutionTypeFk());
				institutionType=true;
				changeInstitutionTypeForFilterListener();
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			//e.printStackTrace();
		}
	}

	/**
	 * Limpiar filtro usuario.
	 */
	public void cleanUserAccountFilter() {
		//this.userAccountFilter = new UserAccountFilter();
		userAccountFilter.setLoginUserAccount("");
		userAccountFilter.setUserName(null);
		userAccountFilter.setFirtsLastName(null);
		userAccountFilter.setSecondLastName(null);
		userAccountDataModel = null;
	}

	/**
	 * Clean variables listener.
	 */
	public void cleanVariablesListener() {
		this.scheduleExceptionFilter = new ScheduleExceptionFilter();
		scheduleExceptionFilter.setInitialDate(CommonsUtilities.currentDate());
		scheduleExceptionFilter.setFinalDate(CommonsUtilities.currentDate());
		this.scheduleExceptionSession = new ScheduleException();
		this.scheduleExceptionDataModel = null;
	}

	/**
	 * Gets the schedule exception filter.
	 *
	 * @return the schedule exception filter
	 */
	public ScheduleExceptionFilter getScheduleExceptionFilter() {
		return scheduleExceptionFilter;
	}

	/**
	 * Sets the schedule exception filter.
	 *
	 * @param scheduleExceptionFilter the schedule exception filter
	 */
	public void setScheduleExceptionFilter(
			ScheduleExceptionFilter scheduleExceptionFilter) {
		this.scheduleExceptionFilter = scheduleExceptionFilter;
	}

	/**
	 * Gets the schedule exception session.
	 *
	 * @return the schedule exception session
	 */
	public ScheduleException getScheduleExceptionSession() {
		return scheduleExceptionSession;
	}

	/**
	 * Sets the schedule exception session.
	 *
	 * @param scheduleExceptionSession the schedule exception session
	 */
	public void setScheduleExceptionSession(
			ScheduleException scheduleExceptionSession) {
		this.scheduleExceptionSession = scheduleExceptionSession;
	}

	/**
	 * Gets the schedule exception data model.
	 *
	 * @return the schedule exception data model
	 */
	public GenericDataModel<ScheduleException> getScheduleExceptionDataModel() {
		return scheduleExceptionDataModel;
	}

	/**
	 * Sets the schedule exception data model.
	 *
	 * @param scheduleExceptionDataModel the schedule exception data model
	 */
	public void setScheduleExceptionDataModel(
			GenericDataModel<ScheduleException> scheduleExceptionDataModel) {
		this.scheduleExceptionDataModel = scheduleExceptionDataModel;
	}

	/**
	 * Gets the user account list.
	 *
	 * @return the user account list
	 */
	public List<UserAccount> getUserAccountList() {
		return userAccountList;
	}

	/**
	 * Sets the user account list.
	 *
	 * @param userAccountList the user account list
	 */
	public void setUserAccountList(List<UserAccount> userAccountList) {
		this.userAccountList = userAccountList;
	}

	/**
	 * Gets the user account data model.
	 *
	 * @return the user account data model
	 */
	public GenericDataModel<UserAccount> getUserAccountDataModel() {
		return userAccountDataModel;
	}

	/**
	 * Sets the user account data model.
	 *
	 * @param userAccountDataModel the user account data model
	 */
	public void setUserAccountDataModel(
			GenericDataModel<UserAccount> userAccountDataModel) {
		this.userAccountDataModel = userAccountDataModel;
	}

	/**
	 * Gets the schedule exception detail list.
	 *
	 * @return the schedule exception detail list
	 */
	public List<ScheduleExceptionDetail> getScheduleExceptionDetailList() {
		return scheduleExceptionDetailList;
	}

	/**
	 * Sets the schedule exception detail list.
	 *
	 * @param scheduleExceptionDetailList the schedule exception detail list
	 */
	public void setScheduleExceptionDetailList(
			List<ScheduleExceptionDetail> scheduleExceptionDetailList) {
		this.scheduleExceptionDetailList = scheduleExceptionDetailList;
	}

	/**
	 * Gets the hystorical state.
	 *
	 * @return the hystorical state
	 */
	public HistoricalState getHystoricalState() {
		return hystoricalState;
	}

	/**
	 * Sets the hystorical state.
	 *
	 * @param hystoricalState the hystorical state
	 */
	public void setHystoricalState(HistoricalState hystoricalState) {
		this.hystoricalState = hystoricalState;
	}

	/**
	 * Checks if is show other motive.
	 *
	 * @return true, if checks if is show other motive
	 */
	public boolean isShowOtherMotive() {
		return showOtherMotive;
	}

	/**
	 * Sets the show other motive.
	 *
	 * @param showOtherMotive the show other motive
	 */
	public void setShowOtherMotive(boolean showOtherMotive) {
		this.showOtherMotive = showOtherMotive;
	}

	/**
	 * Gets the user account filter.
	 *
	 * @return the user account filter
	 */
	public UserAccountFilter getUserAccountFilter() {
		return userAccountFilter;
	}

	/**
	 * Sets the user account filter.
	 *
	 * @param userAccountFilter the user account filter
	 */
	public void setUserAccountFilter(UserAccountFilter userAccountFilter) {
		this.userAccountFilter = userAccountFilter;
	}

	/**
	 * Gets the schedule exception temp.
	 *
	 * @return the schedule exception temp
	 */
	public String getScheduleExceptionTemp() {
		return scheduleExceptionTemp;
	}

	/**
	 * Sets the schedule exception temp.
	 *
	 * @param scheduleExceptionTemp the schedule exception temp
	 */
	public void setScheduleExceptionTemp(String scheduleExceptionTemp) {
		this.scheduleExceptionTemp = scheduleExceptionTemp;
	}

	/**
	 * Gets the system service facade.
	 *
	 * @return the system service facade
	 */
	public SystemServiceFacade getSystemServiceFacade() {
		return systemServiceFacade;
	}

	/**
	 * Sets the system service facade.
	 *
	 * @param systemServiceFacade the system service facade
	 */
	public void setSystemServiceFacade(SystemServiceFacade systemServiceFacade) {
		this.systemServiceFacade = systemServiceFacade;
	}

	/**
	 * Action motive listener.
	 *
	 * @param event the event
	 */
	public void actionMotiveListener(ActionEvent event) {
		try {
			if (this.getHystoricalState().getMotive() == -1) {
				JSFUtilities.showComponent("cnfMsgCustomValidation");
				showMessageOnDialog(null, null,	PropertiesConstants.ERROR_MESSAGE_MOTIVE_REQUIRED, null);
				return;
			} else if (showOtherMotive==true) {
				if (StringUtils.isBlank(this.getHystoricalState().getMotiveDescription())) {
					JSFUtilities.hideComponent("frmDelete:cnfDelete");
					JSFUtilities.hideComponent("frmReject:cnfReject");
					JSFUtilities.showComponent("cnfMsgCustomValidation");
					showMessageOnDialog(null, null,PropertiesConstants.ERROR_MESSAGE_MOTIVE_REQUIRED,null);
					String errorMessage=PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_MOTIVE_REQUIRED);
					JSFUtilities.addContextMessage("frmMotive:idItxtOtherReason",FacesMessage.SEVERITY_ERROR, errorMessage,errorMessage);
					return;
				}
			}
			JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
			hideDialogs();
			JSFUtilities.showComponent("frmMotive:dlgMotive");
			if (this.isDelete()) {
				Object[] data = { this.scheduleExceptionSession.getIdScheduleExceptionPk() };
				showMessageOnDialog(
						null, null,
						PropertiesConstants.SYSTEM_SCHEDULE_EXCEPTION_DELETE_MESSAGE, data);
				JSFUtilities.showComponent("frmDelete:cnfDelete");
				JSFUtilities.executeJavascriptFunction("PF('dlgwMotive').hide();");
			}else if(this.isReject()){
				Object[] data = { this.scheduleExceptionSession.getIdScheduleExceptionPk() };
				showMessageOnDialog(
						null, null,
						PropertiesConstants.SYSTEM_SCHEDULE_EXCEPTION_REJECT_MESSAGE, data);
				JSFUtilities.showComponent("frmReject:cnfReject");
				JSFUtilities.executeJavascriptFunction("PF('dlgwMotive').hide();");
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Cancel delete motive listener.
	 */
	public void cancelMotiveListener(){
		hystoricalState.setMotive(GeneralConstants.NEGATIVE_ONE_INTEGER);
		hystoricalState.setMotiveDescription(null);
		JSFUtilities.hideComponent("frmDelete:cnfDelete");
		JSFUtilities.hideComponent("frmReject:cnfReject");
	}
	
	/**
	 * Change selection motive listener.
	 */
	public void changeSelectionMotiveListener() {
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		possibleSaveMotive = false;
		showOtherMotive = false;
		
		if(Validations.validateIsNotNullAndPositive(hystoricalState.getMotive())){		
			if(OtherMotiveType.SCHEDULE_EXCEPTION_DELETE_OTHER_MOTIVE.getCode().equals(hystoricalState.getMotive())){
				showOtherMotive = true;
				if(Validations.validateIsNotNullAndNotEmpty(hystoricalState.getMotiveDescription())){
					possibleSaveMotive = true;
				}
			}else if(OtherMotiveType.SCHEDULE_EXCEPTION_REJECT_OTHER_MOTIVE.getCode().equals(hystoricalState.getMotive())){
				showOtherMotive = true;
				if(Validations.validateIsNotNullAndNotEmpty(hystoricalState.getMotiveDescription())){
					possibleSaveMotive = true;
				}
			}else{
				possibleSaveMotive = true;
				showOtherMotive = false;
			}
		}
		
		
//		if(Validations.validateIsNotNullAndPositive(hystoricalState.getMotive())){
//			if(OtherMotiveType.SCHEDULE_EXCEPTION_DELETE_OTHER_MOTIVE.getCode().equals(hystoricalState.getMotive()) &&
//			   !Validations.validateIsNotNullAndNotEmpty(hystoricalState.getMotiveDescription())){
//				showOtherMotive = true;
//			}else if(Validations.validateIsNotNullAndNotEmpty(hystoricalState.getMotiveDescription())){
//				possibleSaveMotive = true;
//			}else if(!(OtherMotiveType.SCHEDULE_EXCEPTION_DELETE_OTHER_MOTIVE.getCode().equals(hystoricalState.getMotive())) ){
//				possibleSaveMotive = true;
//			}
//		}
	}

	/**
	 * Change user type for filter listener.
	 */
	public void changeUserTypeForFilterListener() {
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		userAccountFilter.setIdInstitutionSecurity(Integer.valueOf(-1));
		userAccountFilter.setInstitutionType(Integer.valueOf(-1));
		if (userAccountFilter.getUserAccountType().intValue() > 0) {
			institutionType=false;
			if (userAccountFilter.getUserAccountType().intValue() == UserAccountType.INTERNAL.getCode().intValue()) {
				lstInstitutionTypeForFilter = new ArrayList<Parameter>();
				lstInstitutionTypeForFilter.addAll(lstInstitutionTypeForFilterTemp);
				for(Parameter param:lstInstitutionTypeForFilterTemp){
					if(!InstitutionType.DEPOSITARY.getCode().equals(param.getIdParameterPk())){
						lstInstitutionTypeForFilter.remove(param);
					}
				}
				//lstInstitutionTypeForFilter = InstitutionType.listInternalInstitutions;
				JSFUtilities.putViewMap("lista", lstInstitutionTypeForFilter);
			} else {
				lstInstitutionTypeForFilter=new ArrayList<Parameter>();
				lstInstitutionTypeForFilter.addAll(lstInstitutionTypeForFilterTemp);
				for(Parameter param:lstInstitutionTypeForFilterTemp){
					if(InstitutionType.DEPOSITARY.getCode().equals(param.getIdParameterPk())){
						lstInstitutionTypeForFilter.remove(param);
					}
				}
				//lstInstitutionTypeForFilter = InstitutionType.listExternalInstitutions;
				JSFUtilities.putViewMap("lista", lstInstitutionTypeForFilter);
			}
		} else {
			lstInstitutionTypeForFilter = new ArrayList<Parameter>();
			lstInstitutionsSecurityForFilter = new ArrayList<InstitutionsSecurity>();
			userAccountFilter.setInstitutionType(Integer.valueOf(-1));
			userAccountFilter.setIdInstitutionSecurity(Integer.valueOf(-1));
		}
		if (userAccountFilter.getUserAccountType().intValue() == 0) {
			userAccountFilter.setInstitutionType(Integer.valueOf(0));
			userAccountFilter.setIdInstitutionSecurity(Integer.valueOf(0));
			institutionType=true;
		}
		JSFUtilities.setValidViewComponent("formExcepcion:cboInstitutionType");
		JSFUtilities.setValidViewComponent("formExcepcion:cboInstitution");
		loadInstitutionsByUserAccountType();
	}
	
	/**
	 * To show confirmation dialog.
	 *
	 * @param scheduleExceptionDetailparam the schedule exception detailparam
	 */
	public void beforeDeleteUser(ScheduleExceptionDetail scheduleExceptionDetailparam){
		hideDialogs();
		Object[] userCode={scheduleExceptionDetailparam.getUserAccount().getLoginUser()};
		if(scheduleExceptionDetailparam.isSelected()){
			JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT,null,PropertiesConstants.CNF_USER_REMOVE,userCode);
		}else{
			JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT,null,PropertiesConstants.CNF_USER_ADD,userCode);
		}
		scheduleExceptionDetailparamtemp = scheduleExceptionDetailparam;
		JSFUtilities.showComponent(":formExcepcion:cnfremoveUser");
	}

	/**
	 * Gets the schedule exception detailparamtemp.
	 *
	 * @return the scheduleExceptionDetailparamtemp
	 */
	public ScheduleExceptionDetail getScheduleExceptionDetailparamtemp() {
		return scheduleExceptionDetailparamtemp;
	}

	/**
	 * Sets the schedule exception detailparamtemp.
	 *
	 * @param scheduleExceptionDetailparamtemp the scheduleExceptionDetailparamtemp to set
	 */
	public void setScheduleExceptionDetailparamtemp(
			ScheduleExceptionDetail scheduleExceptionDetailparamtemp) {
		this.scheduleExceptionDetailparamtemp = scheduleExceptionDetailparamtemp;
	}	
	
	/**
	 * For Block Descripcion.
	 *
	 * @return the motive descripcion
	 */
	public String getMotiveDescripcion(){
	    String desc = "";
	    if(scheduleExceptionSession.getBlockMotive() != null){
			if(lstMotives != null && lstMotives.size() > 0 ){
				for(int i=0;i<lstMotives.size();i++){
					if(scheduleExceptionSession.getBlockMotive().compareTo(lstMotives.get(i).getIdParameterPk()) == 0){
							desc=lstMotives.get(i).getName();
						}
					}
				}
			}
		return desc;
	}	
		
		/**
		 * For unBlock Descripcion.
		 *
		 * @return the motiveun block descripcion
		 */
	public String getMotiveunBlockDescripcion(){
	    String desc = "";
	    if(scheduleExceptionSession.getUnblockMotive() != null){
			if(lstMotives != null && lstMotives.size() > 0 ){
				for(int i=0;i<lstMotives.size();i++){
					if(scheduleExceptionSession.getUnblockMotive().compareTo(lstMotives.get(i).getIdParameterPk()) == 0){
							desc=lstMotives.get(i).getName();
						}
					}
				}
			}
		return desc;
	}	
	
	/**
	 * For removeMotive Descripcion.
	 *
	 * @return the motiveeliminar descripcion
	 */
	public String getMotiveeliminarDescripcion(){
	    String desc = "";
	    if(scheduleExceptionSession.getRemoveMotive() != null){
			if(lstMotives != null && lstMotives.size() > 0 ){
				for(int i=0;i<lstMotives.size();i++){
					if(scheduleExceptionSession.getRemoveMotive().compareTo(lstMotives.get(i).getIdParameterPk()) == 0){
							desc=lstMotives.get(i).getName();
						}
					}
				}
			}
		return desc;
	}	
	
	/**
	 * Gets the InstitutionTypeFkdescrpicion description.
	 *
	 * @return the id institution fkdescrpicion
	 */
	public String getIdInstitutionFkdescrpicion(){
		String description = null;
		try{
		if(scheduleExceptionSession.getIdInstitutionFk() != null && scheduleExceptionSession.getIdInstitutionFk() > 0){
			description = userServiceFacade.getInstitutionsName(scheduleExceptionSession.getIdInstitutionFk());
		}else if(scheduleExceptionSession.getIdInstitutionFk() != null && scheduleExceptionSession.getIdInstitutionFk() <= 0){
			description = "TODOS";
		}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return  description;
		
	}
	
	
	/**
	 * List Of Holidays.
	 *
	 * @return the holiday list
	 */
	public String getHolidayList(){
		//List<String> weekEnds = getWeekendDates(getIntialDate(),CommonsUtilities.currentDate());
		List<String> list = userServiceFacade.getHolidays();
		StringBuffer  stb = new StringBuffer("[");
		Set<String> holidays = new HashSet<String>();
		/*for (String weekEndDay : weekEnds) {
			holidays.add(weekEndDay);
		}*/
		for (String listDay : list) {
			holidays.add(listDay);
		}
		if(holidays != null && holidays.size() > 0){
			for (Iterator<String> it = holidays.iterator(); it.hasNext(); ) {
				stb.append("\""+it.next()+"\"");
				stb.append(",");
			} 
		}
		if(!holidays.isEmpty()) {
			int comma = stb.lastIndexOf(",");
			stb.replace(comma,comma+1,"");
			stb.append("]");
			return stb.toString();
		} else {
			return "[]";
		}
	}
	
	/**
	 * Gets the weekend dates.
	 *
	 * @param startDate the start date
	 * @param endDate the end date
	 * @return the weekend dates
	 */
	private static List<String> getWeekendDates(Date startDate, Date endDate){
		SimpleDateFormat sdFormat = new SimpleDateFormat("dd/MM/yyyy");
		Calendar calendar = Calendar.getInstance();
		Calendar calendar2 = Calendar.getInstance();
		try{
			calendar.setTime(sdFormat.parse(sdFormat.format(startDate)));
			calendar2.setTime(sdFormat.parse(sdFormat.format(endDate)));
		}catch (Exception e) {
			// TODO: handle exception
		}
		LocalDate start = new LocalDate(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH)+1,calendar.get(Calendar.DAY_OF_MONTH));
		LocalDate end = new LocalDate(calendar2.get(Calendar.YEAR),calendar2.get(Calendar.MONTH)+1,calendar2.get(Calendar.DAY_OF_MONTH));
		List<String> result = new ArrayList<String>();
		SimpleDateFormat sdf = new SimpleDateFormat("M-dd-yyyy"); 
			for (LocalDate date = start;date.isBefore(end);date = date.plusDays(1)){
				int day = date.getDayOfWeek();
				if (day == DateTimeConstants.SATURDAY || day == DateTimeConstants.SUNDAY){
					Date dateUtil = date.toDateMidnight().toDate();
					result.add(sdf.format(dateUtil));
				}
			}
		return result;
	}      
	
	/**
	 * Gets the max date.
	 *
	 * @return the max date
	 */
	public Date getMaxDate(){
		return CommonsUtilities.currentDate();
	}
		
	/**
	 * Gets the intial date.
	 *
	 * @return the intialDate
	 */
	public Date getIntialDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -30);
		return calendar.getTime();
	}
	
	/**
	 * Clear  the final date when initial date changed.
	 */
	public void intialDateChanged(){
		scheduleExceptionSession.setEndDate(new Date(scheduleExceptionSession.getInitialDate().getTime()));
		
	}
	
	/**
	 * cleared popup values ,when creating the exception.
	 */
	public void clearData(){
		userAccountFilter.setLoginUserAccount("");
		userAccountFilter.setUserName(null);
		userAccountFilter.setFirtsLastName(null);
		userAccountFilter.setSecondLastName(null);
		JSFUtilities.hideComponent("dialogUsserAccountForm:searchUserAccounts");
	}
	
	/**
	 * Checks if is form state.
	 *
	 * @return the formState
	 */
	public boolean isFormState() {
		return formState;
	}
	
	/**
	 * Sets the form state.
	 *
	 * @param formState the formState to set
	 */
	public void setFormState(boolean formState) {
		this.formState = formState;
	}
	
	
	/**
	 * Checks if is possible save motive.
	 *
	 * @return true, if is possible save motive
	 */
	public boolean isPossibleSaveMotive() {
		return possibleSaveMotive;
	}
	
	/**
	 * Sets the possible save motive.
	 *
	 * @param possibleSaveMotive the new possible save motive
	 */
	public void setPossibleSaveMotive(boolean possibleSaveMotive) {
		this.possibleSaveMotive = possibleSaveMotive;
	}
	
	public Integer getSystemPkForFilterListener() {
		return systemPkForFilterListener;
	}

	public void setSystemPkForFilterListener(Integer systemPkForFilterListener) {
		this.systemPkForFilterListener = systemPkForFilterListener;
	}

	public boolean isDisabledWeekends() {
		return isDisabledWeekends;
	}

	public void setDisabledWeekends(boolean isDisabledWeekends) {
		this.isDisabledWeekends = isDisabledWeekends;
	}

	public Integer getMinHourForException() {
		return minHourForException;
	}

	public void setMinHourForException(Integer minHourForException) {
		this.minHourForException = minHourForException;
	}

	public Integer getMaxHourForException() {
		return maxHourForException;
	}

	public void setMaxHourForException(Integer maxHourForException) {
		this.maxHourForException = maxHourForException;
	}

	/**
	 * Check schedule final time listener.
	 */
	public void checkScheduleFinalTimeListener(){
		if(Validations.validateIsNotNull(scheduleExceptionSession.getInitialDate()) && Validations.validateIsNotNull(scheduleExceptionSession.getEndDate())){
			if(CommonsUtilities.truncateDateTime(scheduleExceptionSession.getInitialDate()).equals(CommonsUtilities.truncateDateTime(scheduleExceptionSession.getEndDate()))){
				
				Calendar calInitTime = Calendar.getInstance();
				calInitTime.setTime(scheduleExceptionSession.getInitialDate());
				
				Calendar calFinalTime = Calendar.getInstance();
				calFinalTime.setTime(scheduleExceptionSession.getEndDate());
								
				Calendar cal = Calendar.getInstance();				
				cal.set(Calendar.MINUTE, 0);
				//if is the same hour and is Normal Exception
				if(calInitTime.get(Calendar.HOUR_OF_DAY) == calFinalTime.get(Calendar.HOUR_OF_DAY)){
					//max hour permit
					if((calInitTime.get(Calendar.HOUR_OF_DAY) + 1)  == ScheduleExceptionAccessType.NORMAL_EXCEPTION.getEndHour() &&
						calInitTime.get(Calendar.MINUTE) == 59){
						scheduleExceptionSession.setInitialDate(new Date(scheduleExceptionSession.getEndDate().getTime()-60000));
						scheduleExceptionSession.setEndDate(new Date(scheduleExceptionSession.getInitialDate().getTime()+60000));
					}
					//if initHour is greater than endHour
					else if(calInitTime.get(Calendar.MINUTE) >= calFinalTime.get(Calendar.MINUTE)){
						scheduleExceptionSession.setEndDate(new Date(scheduleExceptionSession.getInitialDate().getTime()+60000));
					}
				}
				//if the initialHour is greater than endHour and is Normal Exception
				else if(calInitTime.get(Calendar.HOUR_OF_DAY) > calFinalTime.get(Calendar.HOUR_OF_DAY)){
					if((calInitTime.get(Calendar.HOUR_OF_DAY) + 1)  == ScheduleExceptionAccessType.NORMAL_EXCEPTION.getEndHour() &&
							calInitTime.get(Calendar.MINUTE) == 59){
						scheduleExceptionSession.setInitialDate(new Date(scheduleExceptionSession.getEndDate().getTime()-60000));
					}
					scheduleExceptionSession.setEndDate(new Date(scheduleExceptionSession.getInitialDate().getTime()+60000));					
				}
				JSFUtilities.hideGeneralDialogues();
			}
		}
	}
	
	public boolean haveScheduleExceptionBySystem(Integer systemPk,UserAccount user){
		if(Validations.validateIsNotNullAndNotEmpty(user.getScheduleExceptionDetails()) && user.getScheduleExceptionDetails().size()!=0){
    		for(ScheduleExceptionDetail scheduleExceptionDetail: user.getScheduleExceptionDetails()){
				if(systemPk.equals(scheduleExceptionDetail.getScheduleException().getSystem().getIdSystemPk())){
    				return Boolean.TRUE;
    			}
    		}
    	}
		return Boolean.FALSE;
	}

	public boolean isBlDataModified() {
		return blDataModified;
	}

	public void setBlDataModified(boolean blDataModified) {
		this.blDataModified = blDataModified;
	}
}