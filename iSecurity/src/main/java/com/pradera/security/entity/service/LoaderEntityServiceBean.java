package com.pradera.security.entity.service;


import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnitUtil;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.security.systemmgmt.service.CrudSecurityServiceBean;



/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class LoaderEntityServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 08/05/2013
 */
@Stateless
public class LoaderEntityServiceBean extends CrudSecurityServiceBean{
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/**
	 * Instantiates a new loader entity service bean.
	 */
	public LoaderEntityServiceBean() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Sets the audit.
	 *
	 * @param listEntities the list entities
	 * @param loggerUser the logger user
	 */
	public void setAudit(List<? extends Auditable> listEntities,LoggerUser loggerUser) {
		PersistenceUnitUtil persistenceUtil = em.getEntityManagerFactory().getPersistenceUnitUtil();
		try {
			if (listEntities != null && persistenceUtil.isLoaded(listEntities) && !listEntities.isEmpty()) {
				for (Auditable entity : listEntities) {
					try {
						entity.setAudit(loggerUser);
					} catch(Exception ex) {
						log.error(ex.getMessage());
					}
				}
			}
			} catch(Exception eix) {
				log.error(eix.getMessage());
			}
	}
	
	/**
	 * Load entity.
	 *
	 * @param <T> the generic type
	 * @param entity the entity
	 * @return the t
	 */
	public <T> T loadEntity(T entity,Object primaryKey) {
		if (!em.contains(entity)) {
			entity = (T)em.find(entity.getClass(),primaryKey);
		}
		return entity;
	}
	
	/**
	 * Checks if is loaded entity.
	 *
	 * @param <T> the generic type
	 * @param entity the entity
	 * @return true, if is loaded entity
	 */
	public <T> boolean isLoadedEntity(T entity,String atribute) {
		boolean valid = false;
		PersistenceUnitUtil persistenceUtil = em.getEntityManagerFactory().getPersistenceUnitUtil();
		try {
			if (entity != null && persistenceUtil.isLoaded(entity,atribute)){
				valid = true;
			}
		} catch(Exception eix) {
			log.error(eix.getMessage());
		}
		return valid;
	}
	
	/**
	 * Checks if is loaded list.
	 *
	 * @param list the list
	 * @return true, if is loaded list
	 */
	public boolean isLoadedList(List<? extends Auditable> list) {
		boolean returnValue = false;
		PersistenceUnitUtil persistenceUtil = em.getEntityManagerFactory().getPersistenceUnitUtil();
		try {
			if (list != null && list.size() > 0 && persistenceUtil.isLoaded(list)){
				returnValue = true;
			}
		} catch(Exception eix) {
			log.error(eix.getMessage());
		}
		return returnValue;
	}
	
}

