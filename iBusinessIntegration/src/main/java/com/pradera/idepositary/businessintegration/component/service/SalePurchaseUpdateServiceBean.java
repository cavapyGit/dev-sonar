package com.pradera.idepositary.businessintegration.component.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.AccessTimeout;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.RemoteLoggerUserInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.service.SalePurchaseUpdateComponentService;
import com.pradera.integration.component.business.to.AccountOperationTO;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.negotiation.type.AccountOperationReferenceType;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.NegotiationModalityType;
import com.pradera.model.settlement.SettlementAccountMarketfact;

@Singleton
public class SalePurchaseUpdateServiceBean implements SalePurchaseUpdateComponentService {
	
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	@EJB
    AccountsComponentService accountsComponentService;
	
	@EJB
	SalePurchaseAdditionalServiceBean additionalComponentServiceBean;
	
	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;
	
	@AccessTimeout(unit=TimeUnit.MINUTES,value=5)
    @Interceptors(RemoteLoggerUserInterceptor.class)
	public void blockSalePurchaseBalances(AccountOperationTO objAccountOperationTO, Long idBusinessProcess, Integer indMarketFact) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser.getUserAction().getIdPrivilegeSettlement()==null){ 
			if(loggerUser.getUserAction().getIdPrivilegeRegister()==null){
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
			}else{
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
			}
		}else if(loggerUser.getUserAction().getIdPrivilegeSettlement()!=null){
	        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSettlement());
		}
		if (!additionalComponentServiceBean.isBlockedBalanceOperation(objAccountOperationTO.getIdSettlementAccountOperation())) {
			//we have to distinguish the actions for sale and purchase positions 
			if (ComponentConstant.SALE_ROLE.equals(objAccountOperationTO.getIdRole()))
			{
				//we have to validate if the holder account has available balance to block
				BigDecimal availableBalance = additionalComponentServiceBean.getAvailableBalance(objAccountOperationTO.getIdParticipant(), 
																objAccountOperationTO.getIdHolderAccount(), objAccountOperationTO.getIdSecurityCode());
				//we have to work only with the holder accounts that cover the stock quantity to block available balance
				if (Validations.validateIsNotNull(availableBalance) && availableBalance.compareTo(objAccountOperationTO.getBlockQuantity()) >= 0)
				{
					if(objAccountOperationTO.getBlockQuantity().compareTo(BigDecimal.ZERO) > 0){
						//now we can call the component to generate the movements and block the balances in sale position 
						AccountsComponentTO objAccountsComponentTO = populateAccountComponent(objAccountOperationTO, idBusinessProcess,indMarketFact,loggerUser);
						// call the component
						accountsComponentService.executeAccountsComponent(objAccountsComponentTO);
					}
				
					/* only the stock reference for holder accounts in sale position must be updated   
					 * only for sale position we have to verify the stock reference to participant operation and mechanism operation.
					 * The stock reference for purchase position will be updated at the moment of settlement of the mechanism operation */
					additionalComponentServiceBean.updateStockSettlementAccountOperation(objAccountOperationTO.getIdSettlementAccountOperation(), 
																					AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode());
					
					boolean allSettlementAccountBlockedStock = additionalComponentServiceBean.isSettlementAccountOperationBlockedStock(
																						objAccountOperationTO.getIdSettlementOperation(),
																						AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode()); 
					if(allSettlementAccountBlockedStock){
						//all the participants has blocked the stocks in this mechanism operation, so we have to update the stock reference to the operation.
						additionalComponentServiceBean.updateStockSettlementOperation(objAccountOperationTO.getIdSettlementOperation(), 
																					AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode(), loggerUser);
					}

					//we consider a additional step to chained holder operations
					if (BooleanType.YES.getCode().equals(objAccountOperationTO.getIndInchain())) {
						List<Long> lstIdChainedHolderOperation= additionalComponentServiceBean.getListChainedSettlementAccountOperation(
																						objAccountOperationTO.getIdSettlementAccountOperation());
						additionalComponentServiceBean.updateChainedHolderOperationStockReference(lstIdChainedHolderOperation, 
																						AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode());
					}
				}
			}
			//at the purchase position just execute the component
			else if (ComponentConstant.PURCHARSE_ROLE.equals(objAccountOperationTO.getIdRole()))
			{
				//we don't have to consider SPlit modality to block in purchase role
				if (!NegotiationModalityType.COUPON_SPLIT_FIXED_INCOME.getCode().equals(objAccountOperationTO.getIdModality())) 
				{	
					if(objAccountOperationTO.getBlockQuantity().compareTo(BigDecimal.ZERO) > 0){
						//now we can call the component to generate the movements and update the balances in purchase position
						AccountsComponentTO objAccountsComponentTO= populateAccountComponent(objAccountOperationTO, idBusinessProcess,indMarketFact, loggerUser);
						// call the component
						accountsComponentService.executeAccountsComponent(objAccountsComponentTO);
					}
					//we have to update the reference block stock to HolderAccountOperation
					additionalComponentServiceBean.updateStockSettlementAccountOperation(objAccountOperationTO.getIdSettlementAccountOperation(), 
																						AccountOperationReferenceType.REFERENTIAL_SECURITIES.getCode());
				}
			}
			// Envia querys a la base de datos antes del comit para un comit mas rapido en la trasaccion
			additionalComponentServiceBean.flushTransaction();
		}
	}
	
	
	private AccountsComponentTO populateAccountComponent(AccountOperationTO objAccountOperationTO, Long idBusinessProcess,
														Integer indMarketFact, LoggerUser loggerUser) throws ServiceException
	{
		AccountsComponentTO objAccountsComponentTO= new AccountsComponentTO();
		Long operationType =  NegotiationModalityType.get(objAccountOperationTO.getIdModality()).getParameterOperationType();
		objAccountsComponentTO.setIdOperationType(operationType);
		objAccountsComponentTO.setIdBusinessProcess(idBusinessProcess);
		objAccountsComponentTO.setIdSettlementOperation(objAccountOperationTO.getIdSettlementOperation());
		objAccountsComponentTO.setOperationPart(objAccountOperationTO.getOperationPart());
		HolderAccountBalanceTO objHolderAccountBalanceTO = new HolderAccountBalanceTO();
		objHolderAccountBalanceTO.setIdHolderAccount(objAccountOperationTO.getIdHolderAccount());
		objHolderAccountBalanceTO.setIdParticipant(objAccountOperationTO.getIdParticipant());
		objHolderAccountBalanceTO.setIdSecurityCode(objAccountOperationTO.getIdSecurityCode());
		objHolderAccountBalanceTO.setStockQuantity(objAccountOperationTO.getBlockQuantity());
		
		if(ComponentConstant.ONE.equals(indMarketFact)){
			//we verify the settlement market facts against market fat balances, just for sellers
			if (ComponentConstant.SALE_ROLE.equals(objAccountOperationTO.getIdRole())) {
				verifyHolderMarkekfactBalance(objAccountOperationTO, loggerUser);
			}
			List<MarketFactAccountTO> lstMarketFactAccountTO = null; 
			List<SettlementAccountMarketfact> lstResult = additionalComponentServiceBean.getSettlementMarketFactAccount(
																			objAccountOperationTO.getIdSettlementAccountOperation());
			if(Validations.validateIsNotNullAndNotEmpty(lstResult)){
				lstMarketFactAccountTO = new ArrayList<MarketFactAccountTO>();
				for(SettlementAccountMarketfact settlementAccountMarketFact:lstResult){
					MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
					
					if (ComponentConstant.SALE_ROLE.equals(objAccountOperationTO.getIdRole())) {
						marketFactAccountTO.setQuantity(settlementAccountMarketFact.getMarketQuantity().subtract(settlementAccountMarketFact.getChainedQuantity()));

						if(!objAccountOperationTO.getIdMechanism().equals(NegotiationMechanismType.SIRTEX.getCode()) &&
							BooleanType.YES.getCode().equals(objAccountOperationTO.getIndReportingBalance()))
						{
							if(objAccountOperationTO.getOperationPart().equals(ComponentConstant.CASH_PART)){
								Object[] objMarketBalance = additionalComponentServiceBean.getObjMarketFactBalance(
										objAccountOperationTO.getIdParticipant(), 
										objAccountOperationTO.getIdHolderAccount(), 
										objAccountOperationTO.getIdSecurityCode(), 
										settlementAccountMarketFact.getMarketDate(), 
										settlementAccountMarketFact.getMarketRate(), 
										settlementAccountMarketFact.getMarketPrice());
								
								if(objMarketBalance!=null){
									BigDecimal marketPrice = (BigDecimal) objMarketBalance[2];
									if(objAccountOperationTO.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode())){
										//settlementAccountMarketFact.setMarketPrice(marketPrice);
										additionalComponentServiceBean.updateSettlementTermMarketFact(
												objAccountOperationTO.getIdSettlementAccountOperation(), marketPrice, loggerUser);
									}
								}
							}
						}
					} else if (ComponentConstant.PURCHARSE_ROLE.equals(objAccountOperationTO.getIdRole())) {
						marketFactAccountTO.setQuantity(settlementAccountMarketFact.getMarketQuantity());
						if(!objAccountOperationTO.getIdMechanism().equals(NegotiationMechanismType.SIRTEX.getCode())){
							// purchase role will receive market fact rate and price when instrument type is fix
							if(objAccountOperationTO.getOperationPart().equals(ComponentConstant.CASH_PART)){
								if(objAccountOperationTO.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode())){
									settlementAccountMarketFact.setMarketPrice(objAccountOperationTO.getCashPrice());
									additionalComponentServiceBean.updateSettlementMarketFact(
											objAccountOperationTO.getIdSettlementAccountOperation(), 
											settlementAccountMarketFact.getMarketPrice(), loggerUser);
								}
							}
						}
					}
					
					marketFactAccountTO.setMarketDate(settlementAccountMarketFact.getMarketDate());
					marketFactAccountTO.setMarketPrice(settlementAccountMarketFact.getMarketPrice());
					marketFactAccountTO.setMarketRate(settlementAccountMarketFact.getMarketRate());
					
					if(marketFactAccountTO.getQuantity().compareTo(BigDecimal.ZERO) > 0){
						lstMarketFactAccountTO.add(marketFactAccountTO);
					}
				}
			}
			objHolderAccountBalanceTO.setLstMarketFactAccounts(lstMarketFactAccountTO);							
		}
		List<HolderAccountBalanceTO> lstHolderAccountBalance = new ArrayList<HolderAccountBalanceTO>();
		lstHolderAccountBalance.add(objHolderAccountBalanceTO);
		if (ComponentConstant.SALE_ROLE.equals(objAccountOperationTO.getIdRole()))
			objAccountsComponentTO.setLstSourceAccounts(lstHolderAccountBalance);
		else if (ComponentConstant.PURCHARSE_ROLE.equals(objAccountOperationTO.getIdRole()))
			objAccountsComponentTO.setLstTargetAccounts(lstHolderAccountBalance);
		objAccountsComponentTO.setIndMarketFact(indMarketFact);
		return objAccountsComponentTO;
	}
	
	
	private void verifyHolderMarkekfactBalance(AccountOperationTO objAccountOperationTO, LoggerUser loggerUser) {
		//we get the list market facts about settlement account
		List<SettlementAccountMarketfact> lstResult = additionalComponentServiceBean.getSettlementMarketFactAccount(
																		objAccountOperationTO.getIdSettlementAccountOperation());
		Map<Long,HolderMarketFactBalance> mpHolderMarketfactBalance= new HashMap<Long, HolderMarketFactBalance>();
		List<SettlementAccountMarketfact> lstNewSettlementAccountMarketfacts= new ArrayList<SettlementAccountMarketfact>();
		List<SettlementAccountMarketfact> lstChainedSettlementAccountMarketfacts= new ArrayList<SettlementAccountMarketfact>();
		List<Long> lstOldSettlementAccountMarketfacts= new ArrayList<Long>();
		for (SettlementAccountMarketfact settlementAccountMarketfact: lstResult) {
			//for each settlement account market fact we must verify the market fact balances
			BigDecimal blockQuantity= settlementAccountMarketfact.getMarketQuantity().subtract(settlementAccountMarketfact.getChainedQuantity());
			if (blockQuantity.compareTo(BigDecimal.ZERO) > 0) {
				//we get the list of market fact balances
				List<HolderMarketFactBalance> lstHolderMarketFactBalances= additionalComponentServiceBean.getListHolderMarketFactBalance(
											objAccountOperationTO.getIdHolderAccount(), 
											objAccountOperationTO.getIdSecurityCode(), 
											settlementAccountMarketfact.getMarketDate(), 
											settlementAccountMarketfact.getMarketRate());
				if (Validations.validateListIsNotNullAndNotEmpty(lstHolderMarketFactBalances) &&
					lstHolderMarketFactBalances.size() > GeneralConstants.ONE_VALUE_INTEGER.intValue()) 
				{
					int i = 0;
					reinit:
					while (i<lstHolderMarketFactBalances.size()) {
						HolderMarketFactBalance holderMarketFactBalance= lstHolderMarketFactBalances.get(i);
						if (blockQuantity.compareTo(BigDecimal.ZERO) > 0) {
							HolderMarketFactBalance objHolderMarketFactBalance= null; //this'll save the modified object for each iteration
							if (mpHolderMarketfactBalance.containsKey(holderMarketFactBalance.getIdMarketfactBalancePk())) {
								objHolderMarketFactBalance= mpHolderMarketfactBalance.get(holderMarketFactBalance.getIdMarketfactBalancePk());
							} else {
								try {
									objHolderMarketFactBalance= holderMarketFactBalance.clone();
									additionalComponentServiceBean.detach(objHolderMarketFactBalance);
								} catch (CloneNotSupportedException e) {
									e.printStackTrace();
								}
							}
							if (objHolderMarketFactBalance.getAvailableBalance().compareTo(BigDecimal.ZERO) > 0) 
							{
								//we create the new SettlementAccountMarketfact according the market price
								SettlementAccountMarketfact newSettlementAccountMarketfact= null;
								try {
									newSettlementAccountMarketfact= settlementAccountMarketfact.clone();
								} catch (CloneNotSupportedException e) {
									e.printStackTrace();
								}
								if (blockQuantity.compareTo(objHolderMarketFactBalance.getAvailableBalance()) >= 0) {
									newSettlementAccountMarketfact.setMarketQuantity(objHolderMarketFactBalance.getAvailableBalance());
									objHolderMarketFactBalance.setAvailableBalance(BigDecimal.ZERO);
								} else {
									newSettlementAccountMarketfact.setMarketQuantity(blockQuantity);
									objHolderMarketFactBalance.setAvailableBalance(objHolderMarketFactBalance.getAvailableBalance().subtract(blockQuantity));
								}
								newSettlementAccountMarketfact.setIdSettAccountMarketfactPk(null);
								newSettlementAccountMarketfact.setChainedQuantity(BigDecimal.ZERO);
								newSettlementAccountMarketfact.setMarketPrice(objHolderMarketFactBalance.getMarketPrice()); //this will be the difference
								lstNewSettlementAccountMarketfacts.add(newSettlementAccountMarketfact);
								//we update the reference holder market fact balance
								blockQuantity= blockQuantity.subtract(newSettlementAccountMarketfact.getMarketQuantity());
								mpHolderMarketfactBalance.put(objHolderMarketFactBalance.getIdMarketfactBalancePk(), objHolderMarketFactBalance);
								
								i = 0;
								continue reinit;
							}
						}
						++i;
					}
					//after this, we've create the new instance of SettlementAccountMarketfact to be created (considering the market price)
					if (BigDecimal.ZERO.compareTo(blockQuantity) >= 0) {
						//at this time, the block quantity must have covered by Holder market fact balances with different market prices
						if (settlementAccountMarketfact.getChainedQuantity().compareTo(BigDecimal.ZERO) > 0) {
							//the chained account market fact will be updated
							settlementAccountMarketfact.setMarketQuantity(settlementAccountMarketfact.getChainedQuantity());
							lstChainedSettlementAccountMarketfacts.add(settlementAccountMarketfact);
						} else {
							//these account market fact will be removed
							lstOldSettlementAccountMarketfacts.add(settlementAccountMarketfact.getIdSettAccountMarketfactPk());
						}
					}
				}
			}
		}
		//now we have all the change respecting of settlement account market fact
		if (Validations.validateListIsNotNullAndNotEmpty(lstOldSettlementAccountMarketfacts)) {
			additionalComponentServiceBean.updateSettlementAccountMarketfact(lstOldSettlementAccountMarketfacts, loggerUser);
		}
		if (Validations.validateListIsNotNullAndNotEmpty(lstChainedSettlementAccountMarketfacts)) {
			additionalComponentServiceBean.updateList(lstChainedSettlementAccountMarketfacts);
		}
		if (Validations.validateListIsNotNullAndNotEmpty(lstNewSettlementAccountMarketfacts)) {
			additionalComponentServiceBean.saveAll(lstNewSettlementAccountMarketfacts);
		}
	}
	
}
