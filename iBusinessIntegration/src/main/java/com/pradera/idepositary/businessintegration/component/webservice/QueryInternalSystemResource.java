package com.pradera.idepositary.businessintegration.component.webservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.transaction.TransactionSynchronizationRegistry;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.common.validation.to.ValidationOperationType;
import com.pradera.integration.common.validation.to.ValidationProcessTO;
import com.pradera.integration.component.accounts.service.AccountsRemoteService;
import com.pradera.integration.component.accounts.to.CuiQueryTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.custody.service.CustodyRemoteService;
import com.pradera.integration.component.custody.to.AuthorizedSignatureQueryTO;
import com.pradera.integration.component.custody.to.InstitutionQueryTO;
import com.pradera.integration.component.custody.to.PhysicalCertificateQueryTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.remote.ThreadRemoteContextHolder;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionStateType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionType;

@Path("/QueryInternalSystemResource")
@Stateless
public class QueryInternalSystemResource implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** The server configuration. */
	@Inject
	@InjectableResource(location = "ServerConfiguration.properties")
	private Properties serverConfiguration;
	
	@Inject
	private PraderaLogger log;
	
	/** The interface component service bean. */
	@Inject
    private Instance<InterfaceComponentService> interfaceComponentServiceBean;
	
	/** The parameter service bean. */
	@EJB
	private ParameterServiceBean parameterServiceBean;
	
	/** The interface component service. */
	@EJB
	InterfaceComponentService interfaceComponentService;
	
	@Inject
	private Instance<AccountsRemoteService> accountsRemoteService;
	
	@Inject
	private Instance<CustodyRemoteService> custodyRemoteService;
	
	/** The registry. */
	@Resource
	private TransactionSynchronizationRegistry registry;		
	
	/** The str audits user. */
	String strAuditsUser;
	
	/** The str ip address. */
	String strIpAddress;
	
	@PostConstruct
	public void init(){
		strAuditsUser = serverConfiguration.getProperty("webservice.audits.users.key");
		strIpAddress = serverConfiguration.getProperty("webservice.audits.ip");
	}
	
	/**
	 * Gets the service operation.
	 *
	 * @param xmlParam the xml param
	 * @return the service operation
	 */
	@Performance
	@POST
	@Path("/ccc")
	@Produces(MediaType.APPLICATION_XML)
	public Response getServiceOperation(String xmlParam){
		
		log.info(":::: INICIO SERVICIO CCC ::::");
		ProcessFileTO objProcessFileTO = null;
		ValidationProcessTO objValidationProcessTO = null;
		
		String schemaResponse= null;
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		StringBuilder xmlResponse= new StringBuilder();
		
		List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		
		//Objeto que contendra los datos de ingreso a la consulta
		CuiQueryTO cuiQueryTO = null; 
		
		try {
			
			/*
			 * STEP 01:
			 * Parse the from XML file to TO object   
			 */
			log.info(":::: INICIO VALIDACION ARCHIVO ::::");
			objProcessFileTO = parameterServiceBean.validateInterfaceFile(
																ComponentConstant.INTERFACE_ACCOUNT_OPERATION_CUI_QUERY, 
																xmlParam, ComponentConstant.CUI_QUERY_SCHEMA, Boolean.FALSE);
			log.info(":::: FIN VALIDACION ARCHIVO ::::");
			
			
			/*
			 * STEP 02:
			 * Call the Remote service 
			 */
			log.info(":::: INICIO LLAMADA REMOTA ::::");
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				
				Long idInterfaceProcess = interfaceComponentService.saveExternalInterfaceReceptionTx(objProcessFileTO,ExternalInterfaceReceptionType.WEBSERVICE.getCode(), objLoggerUser);
				objValidationProcessTO = objProcessFileTO.getValidationProcessTO();
				
				for(ValidationOperationType operation : objValidationProcessTO.getLstOperations()){
					
					cuiQueryTO = (CuiQueryTO) operation;
					cuiQueryTO.setIdInterfaceProcess(idInterfaceProcess);
					
					schemaResponse = ComponentConstant.CUI_QUERY_SCHEMA_RESPONSE;
					//obteniendo la lista de resultados
					lstRecordValidationTypes = accountsRemoteService.get().getListCuiQuery(cuiQueryTO);
					objValidationProcessTO.setLstValidations(lstRecordValidationTypes);
				}
			}
			log.info(":::: FIN LLAMADA REMOTA ::::");
		} catch (Exception ex) {
			ex.printStackTrace();
			objValidationProcessTO = parameterServiceBean.generateErrorValidationProcess(cuiQueryTO, ex);
			processState= ExternalInterfaceReceptionStateType.FAILED.getCode();
		}
		finally {
			/*
			 * STEP 03:
			 * Generate the response XML file
			 */
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			if (objValidationProcessTO!=null && Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {				
				RecordValidationType objRecordValidationType = objValidationProcessTO.getLstValidations().get(0);
				if(Validations.validateIsNullOrEmpty(objRecordValidationType.getErrorCode())){
					schemaResponse = ComponentConstant.CUI_QUERY_SCHEMA_RESPONSE;
				} else if(GeneralConstants.WEBSERVICES_ERROR_1000.compareTo(objRecordValidationType.getErrorCode()) != 0){
					schemaResponse = ComponentConstant.CUI_QUERY_SCHEMA_RESPONSE;
				}								
				xmlResponse.append(interfaceComponentServiceBean.get().saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
															objValidationProcessTO.getLstValidations(), schemaResponse, 
															ComponentConstant.CUI_QUERY_TAG_RESPONSE, ComponentConstant.CUI_QUERY_RECORD_TAG_RESPONSE, processState, objLoggerUser));
			} else {
				xmlResponse.append("NO HAY DATOS");
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");				
			
		}
		
		log.info(":::: FIN SERVICIO CCC ::::");
		return Response.status(201).entity(xmlResponse.toString()).build();
	}
	
	/**
	 * Gets the service operation.
	 *
	 * @param xmlParam the xml param
	 * @return the service operation
	 */
	@Performance
	@POST
	@Path("/cfa")
	@Produces(MediaType.APPLICATION_XML)
	public Response getAuthorizedSignatureServiceOperation(String xmlParam){
		
		log.info(":::: INICIO SERVICIO CFA ::::");
		ProcessFileTO objProcessFileTO = null;
		ValidationProcessTO objValidationProcessTO = null;
		
		String schemaResponse= null;
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		StringBuilder xmlResponse= new StringBuilder();
		
		List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		
		//Objeto que contendra los datos de ingreso a la consulta
		AuthorizedSignatureQueryTO authorizedSignatureQueryTO = null;
		
		try {
			
			/*
			 * STEP 01:
			 * Parse the from XML file to TO object   
			 */
			log.info(":::: INICIO VALIDACION ARCHIVO ::::");
			objProcessFileTO = parameterServiceBean.validateInterfaceFile(
																ComponentConstant.INTERFACE_CUSTODY_OPERATION_AUTHORIZED_SIGNATURE_QUERY, 
																xmlParam, ComponentConstant.AUTHORIZED_SIGNATURE_SCHEMA, Boolean.FALSE);
			log.info(":::: FIN VALIDACION ARCHIVO ::::");
			
			/*
			 * STEP 02:
			 * Call the Remote service 
			 */
			log.info(":::: INICIO LLAMADA REMOTA ::::");
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				
				Long idInterfaceProcess = interfaceComponentService.saveExternalInterfaceReceptionTx(objProcessFileTO,ExternalInterfaceReceptionType.WEBSERVICE.getCode(), objLoggerUser);
				objValidationProcessTO = objProcessFileTO.getValidationProcessTO();
				
				for(ValidationOperationType operation : objValidationProcessTO.getLstOperations()){
					authorizedSignatureQueryTO = (AuthorizedSignatureQueryTO) operation;
					authorizedSignatureQueryTO.setIdInterfaceProcess(idInterfaceProcess);
					
					schemaResponse = ComponentConstant.AUTHORIZED_SIGNATURE_SCHEMA_RESPONSE;
					lstRecordValidationTypes = custodyRemoteService.get().getAuthorizedSignatureList(authorizedSignatureQueryTO);
					objValidationProcessTO.setLstValidations(lstRecordValidationTypes);
				}
			}
			log.info(":::: FIN LLAMADA REMOTA ::::");
		} catch (Exception ex) {
			ex.printStackTrace();
			objValidationProcessTO = parameterServiceBean.generateErrorValidationProcess(authorizedSignatureQueryTO, ex);
			processState= ExternalInterfaceReceptionStateType.FAILED.getCode();
		}
		finally {
			/*
			 * STEP 03:
			 * Generate the response XML file
			 */
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			if (objValidationProcessTO!=null && Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {				
				RecordValidationType objRecordValidationType = objValidationProcessTO.getLstValidations().get(0);
				if(Validations.validateIsNullOrEmpty(objRecordValidationType.getErrorCode())){
					schemaResponse = ComponentConstant.AUTHORIZED_SIGNATURE_SCHEMA_RESPONSE;
				} else if(GeneralConstants.WEBSERVICES_ERROR_1000.compareTo(objRecordValidationType.getErrorCode()) != 0){
					schemaResponse = ComponentConstant.AUTHORIZED_SIGNATURE_SCHEMA_RESPONSE;
				}								
				xmlResponse.append(interfaceComponentServiceBean.get().saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
															objValidationProcessTO.getLstValidations(), schemaResponse, 
															ComponentConstant.AUTHORIZED_SIGNATURE_TAG_RESPONSE, ComponentConstant.AUTHORIZED_SIGNATURE_RECORD_TAG_RESPONSE, processState, objLoggerUser));
			} else {
				xmlResponse.append("NO HAY DATOS");
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");				
			
		}
		
		log.info(":::: FIN SERVICIO CFA ::::");
		return Response.status(201).entity(xmlResponse.toString()).build();
	}
	
	/**
	 * Gets the service operation: list institutions.
	 *
	 * @param xmlParam the xml param
	 * @return the service operation
	 */
	@Performance
	@POST
	@Path("/cin")
	@Produces(MediaType.APPLICATION_XML)
	public Response getInstitutionsServiceOperation(String xmlParam){
		
		log.info(":::: INICIO SERVICIO CIN ::::");
		ProcessFileTO objProcessFileTO = null;
		ValidationProcessTO objValidationProcessTO = null;
		
		String schemaResponse= null;
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		StringBuilder xmlResponse= new StringBuilder();
		
		List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		
		//Objeto que contendra los datos de ingreso a la consulta
		InstitutionQueryTO institutionQueryTO = null;
		
		try {
			
			/*
			 * STEP 01:
			 * Parse the from XML file to TO object   
			 */
			log.info(":::: INICIO VALIDACION ARCHIVO ::::");
			objProcessFileTO = parameterServiceBean.validateInterfaceFile(ComponentConstant.INTERFACE_INSTITUTION_QUERY, 
																		  xmlParam, ComponentConstant.INSTITUTION_QUERY_SCHEMA, Boolean.FALSE);
			log.info(":::: FIN VALIDACION ARCHIVO ::::");
			
			/*
			 * STEP 02:
			 * Call the Remote service 
			 */
			log.info(":::: INICIO LLAMADA REMOTA ::::");
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				
				Long idInterfaceProcess = interfaceComponentService.saveExternalInterfaceReceptionTx(objProcessFileTO,ExternalInterfaceReceptionType.WEBSERVICE.getCode(), objLoggerUser);
				objValidationProcessTO = objProcessFileTO.getValidationProcessTO();
				
				for(ValidationOperationType operation : objValidationProcessTO.getLstOperations()){
					institutionQueryTO = (InstitutionQueryTO) operation;
					institutionQueryTO.setIdInterfaceProcess(idInterfaceProcess);
					
					schemaResponse = ComponentConstant.INSTITUTION_QUERY_SCHEMA_RESPONSE;
					lstRecordValidationTypes = custodyRemoteService.get().getInstitutionList();
					objValidationProcessTO.setLstValidations(lstRecordValidationTypes);
				}
			}
			log.info(":::: FIN LLAMADA REMOTA ::::");
		} catch (Exception ex) {
			ex.printStackTrace();
			objValidationProcessTO = parameterServiceBean.generateErrorValidationProcess(institutionQueryTO, ex);
			processState= ExternalInterfaceReceptionStateType.FAILED.getCode();
		}
		finally {
			/*
			 * STEP 03:
			 * Generate the response XML file
			 */
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			if (objValidationProcessTO!=null && Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {				
				RecordValidationType objRecordValidationType = objValidationProcessTO.getLstValidations().get(0);
				if(Validations.validateIsNullOrEmpty(objRecordValidationType.getErrorCode())){
					schemaResponse = ComponentConstant.INSTITUTION_QUERY_SCHEMA_RESPONSE;
				} else if(GeneralConstants.WEBSERVICES_ERROR_1000.compareTo(objRecordValidationType.getErrorCode()) != 0){
					schemaResponse = ComponentConstant.INSTITUTION_QUERY_SCHEMA_RESPONSE;
				}								
				xmlResponse.append(interfaceComponentServiceBean.get().saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
															objValidationProcessTO.getLstValidations(), schemaResponse, 
															ComponentConstant.INSTITUTION_QUERY_TAG_RESPONSE, ComponentConstant.INSTITUTION_QUERY_RECORD_TAG_RESPONSE, processState, objLoggerUser));
			} else { xmlResponse.append("NO HAY DATOS");
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");				
			
		}
		
		log.info(":::: FIN SERVICIO CIN ::::");
		return Response.status(201).entity(xmlResponse.toString()).build();
	}
	
	
	/**
	 * Gets the service operation.
	 *
	 * @param xmlParam the xml param
	 * @return the service operation
	 */
	@Performance
	@POST
	@Path("/ctf")
	@Produces(MediaType.APPLICATION_XML)
	public Response physicalCertificateQuery (String xmlParam){
		
		log.info(":::: INICIO SERVICIO CTF ::::");
		ProcessFileTO objProcessFileTO = null;
		ValidationProcessTO objValidationProcessTO = null;
		
		String schemaResponse= null;
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		StringBuilder xmlResponse= new StringBuilder();
		
		List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		
		//Objeto que contendra los datos de ingreso a la consulta
		PhysicalCertificateQueryTO physicalCertificateQueryTO = null;
		
		try {
			
			/*
			 * STEP 01:
			 * Parse the from XML file to TO object   
			 */
			log.info(":::: INICIO VALIDACION ARCHIVO ::::");
			objProcessFileTO = parameterServiceBean.validateInterfaceFile(
																ComponentConstant.INTERFACE_PHYSICAL_CERTIFICATE_QUERY, 
																xmlParam, ComponentConstant.PHYSICAL_CERTIFICATE_QUERY_SCHEMA, Boolean.FALSE);
			log.info(":::: FIN VALIDACION ARCHIVO ::::");
			
			
			/*
			 * STEP 02:
			 * Call the Remote service 
			 */
			log.info(":::: INICIO LLAMADA REMOTA ::::");
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				
				Long idInterfaceProcess = interfaceComponentService.saveExternalInterfaceReceptionTx(objProcessFileTO,ExternalInterfaceReceptionType.WEBSERVICE.getCode(), objLoggerUser);
				objValidationProcessTO = objProcessFileTO.getValidationProcessTO();
				
				for(ValidationOperationType operation : objValidationProcessTO.getLstOperations()){
					physicalCertificateQueryTO = (PhysicalCertificateQueryTO) operation;
					physicalCertificateQueryTO.setIdInterfaceProcess(idInterfaceProcess);
					
					schemaResponse = ComponentConstant.PHYSICAL_CERTIFICATE_QUERY_RESPONSE;
					//obteniendo la lista de resultados
					lstRecordValidationTypes = custodyRemoteService.get().getListPhysicalCertificate(physicalCertificateQueryTO);
					objValidationProcessTO.setLstValidations(lstRecordValidationTypes);
				}
			}
			log.info(":::: FIN LLAMADA REMOTA ::::");
		} catch (Exception ex) {
			ex.printStackTrace();
			objValidationProcessTO = parameterServiceBean.generateErrorValidationProcess(physicalCertificateQueryTO, ex);
			processState= ExternalInterfaceReceptionStateType.FAILED.getCode();
		}
		finally {
			/*
			 * STEP 03:
			 * Generate the response XML file
			 */
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			if (objValidationProcessTO!=null && Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {				
				RecordValidationType objRecordValidationType = objValidationProcessTO.getLstValidations().get(0);
				if(Validations.validateIsNullOrEmpty(objRecordValidationType.getErrorCode())){
					schemaResponse = ComponentConstant.PHYSICAL_CERTIFICATE_QUERY_RESPONSE;
				} else if(GeneralConstants.WEBSERVICES_ERROR_1000.compareTo(objRecordValidationType.getErrorCode()) != 0){
					schemaResponse = ComponentConstant.PHYSICAL_CERTIFICATE_QUERY_SCHEMA_RESPONSE_INCORRECT;
				}								
				xmlResponse.append(interfaceComponentServiceBean.get().saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
															objValidationProcessTO.getLstValidations(), schemaResponse, 
															ComponentConstant.PHYSICAL_CERTIFICATE_QUERY_TAG_RESPONSE, ComponentConstant.PHYSICAL_CERTIFICATE_QUERY_RECORD_TAG_RESPONSE, processState, objLoggerUser));
			} else {
				xmlResponse.append("NO HAY DATOS");
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");				
			
		}
		
		log.info(":::: FIN SERVICIO CTF ::::");
		return Response.status(201).entity(xmlResponse.toString()).build();
	}
	
}
