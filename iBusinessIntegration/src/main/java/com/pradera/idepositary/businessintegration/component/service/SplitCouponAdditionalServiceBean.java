package com.pradera.idepositary.businessintegration.component.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.splitcoupons.SplitCouponMarketFact;
import com.pradera.model.custody.splitcoupons.SplitCouponOperation;
import com.pradera.model.custody.splitcoupons.SplitCouponRequest;
import com.pradera.model.custody.splitcoupons.SubproductCode;
import com.pradera.model.custody.type.PhysicalCertificateStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.AmortizationPaymentSchedule;
import com.pradera.model.issuancesecuritie.InterestPaymentSchedule;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;

@Stateless
public class SplitCouponAdditionalServiceBean  extends CrudDaoServiceBean{
	
	private static Map<Integer,Object> PARAMETERS_VALUE = new HashMap<Integer, Object>();
	
	private void putParameterValue(Integer parameterId){
		PARAMETERS_VALUE.put(parameterId, this.find(ParameterTable.class, parameterId));
	}
	
	public Object getParameterValue(Integer parameterId){
		if(PARAMETERS_VALUE.get(parameterId)==null){
			putParameterValue(parameterId);
		}
		return PARAMETERS_VALUE.get(parameterId);
	}

	public SplitCouponRequest getDetachmentCouponRequest(Long idSplitOperationPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT scr FROM SplitCouponRequest scr");
		sbQuery.append("	INNER JOIN FETCH scr.holder");
		sbQuery.append("	INNER JOIN FETCH scr.holderAccount");
		sbQuery.append("	INNER JOIN FETCH scr.participant");
		sbQuery.append("	INNER JOIN FETCH scr.security se");
		sbQuery.append("	INNER JOIN FETCH se.issuance");
		sbQuery.append("	INNER JOIN FETCH se.issuer");
		sbQuery.append("	INNER JOIN FETCH scr.splitCouponOperations sco");
		sbQuery.append("	INNER JOIN FETCH sco.programInterestCoupon pic");
		sbQuery.append("	LEFT JOIN FETCH pic.splitSecurities");
		sbQuery.append("	INNER JOIN FETCH pic.interestPaymentSchedule");
		sbQuery.append("	WHERE scr.idSplitOperationPk = :idSplitOperationPk");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSplitOperationPk", idSplitOperationPk);
		return (SplitCouponRequest) query.getSingleResult();
	}
	
	public HolderAccountBalance getBalances(SplitCouponRequest splitCouponRequest) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT hab FROM HolderAccountBalance hab");
			sbQuery.append("	WHERE hab.id.idParticipantPk = :idParticipantPk");
			sbQuery.append("	AND hab.id.idHolderAccountPk = :idHolderAccountPk");
			sbQuery.append("	AND hab.id.idSecurityCodePk = :idSecurityCodePk");
			sbQuery.append("	ORDER BY hab.totalBalance");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idParticipantPk", splitCouponRequest.getParticipant().getIdParticipantPk());
			query.setParameter("idHolderAccountPk", splitCouponRequest.getHolderAccount().getIdHolderAccountPk());
			query.setParameter("idSecurityCodePk", splitCouponRequest.getSecurity().getIdSecurityCodePk());
			return (HolderAccountBalance) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}		
	}
	
	public BigDecimal getGreaterBalanceCoupon(List<SplitCouponOperation> splitDetailList) throws ServiceException{
		BigDecimal greaterBalanceCoupon = BigDecimal.ZERO;
		for(SplitCouponOperation splitCouponOperation:splitDetailList){
			BigDecimal splitCouponQuantity = splitCouponOperation.getOperationQuantity();
			/**VERIFICAMOS EL CUPON QUE TIENE MAYOR SALDO A DESPRENDER*/
			if(splitCouponQuantity.compareTo(greaterBalanceCoupon)>0){
				greaterBalanceCoupon = splitCouponQuantity;
			}
		}
		if(greaterBalanceCoupon.equals(BigDecimal.ZERO)){
			throw new ServiceException(ErrorServiceType.INCONSISTENCY_DATA);
		}
		return greaterBalanceCoupon;
	}
	
	public BigDecimal findLowerBalance(Map<Integer,BigDecimal> remainingCouponBalance, BigDecimal greaterBalance){
		BigDecimal lowerBalance = greaterBalance;
		for(BigDecimal balance: remainingCouponBalance.values()){
			if(!balance.equals(BigDecimal.ZERO) && lowerBalance.compareTo(balance)>0){
				lowerBalance = balance;
			}
		}
		if(greaterBalance.equals(lowerBalance))
			return BigDecimal.ZERO;
		return lowerBalance;
	}
	
	public BigDecimal generateNominalValueForCoupon(ProgramInterestCoupon coupon, BigDecimal nominalValue) throws ServiceException{
		/***VN.C(i) = VNi * (TRc * (PI/CAL));*/
		BigDecimal vn_i   = nominalValue;
		BigDecimal trc    = coupon.getInterestRate().divide(GeneralConstants.HUNDRED_VALUE_BIGDECIMAL);
		BigDecimal vn     = vn_i.multiply(trc);
		return vn;
	}
	
	public void updateSecuritiesBalances(Security securityModify, BigDecimal quantity, Long operationType) throws ServiceException{
		BigDecimal newSharedBalance=BigDecimal.ZERO, newCirculationBalance=BigDecimal.ZERO,  newPlacedBalance = BigDecimal.ZERO;
		
		/**ACCORDING WITH THE OPERATION, SUM OR SUBTRACT BALANCES*/
		if (operationType == ComponentConstant.SUM) {
			newSharedBalance = securityModify.getShareBalance().add(quantity);
			newCirculationBalance = securityModify.getCirculationBalance().add(quantity);
		} else if (operationType == ComponentConstant.SUBTRACTION) {
			newSharedBalance = securityModify.getShareBalance().subtract(quantity);
			newCirculationBalance = securityModify.getCirculationBalance().subtract(quantity);
		}
		
		/**IF BALANCE IT'S NEGATIVE, THROWS ERROR OF NEGATIVE BALANCE*/
		if(newSharedBalance.compareTo(BigDecimal.ZERO)<0 || newCirculationBalance.compareTo(BigDecimal.ZERO)<0 || newPlacedBalance.compareTo(BigDecimal.ZERO)<0){
			throw new ServiceException(ErrorServiceType.NEGATIVE_BALANCE);
		}
	    
		/**UPDATE BALANCES, ON SECURITY*/
		securityModify.setShareBalance(newSharedBalance);
		securityModify.setCirculationBalance(newCirculationBalance);
		/**UPDATE CAPITAL OR AMOUNTS, ON SECURITY*/
		securityModify.setShareCapital(securityModify.getShareBalance().multiply(securityModify.getCurrentNominalValue()));
		securityModify.setCirculationAmount(securityModify.getCirculationBalance().multiply(securityModify.getCurrentNominalValue()));
		/**UPDATE SECURITY ENTITY OBJECT*/
		update(securityModify);
	}
	
	@SuppressWarnings("unchecked")
	public List<Integer> getPendingCoupons(String idSecurityCodePk){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT pic.couponNumber FROM ProgramInterestCoupon pic");
		sbQuery.append("	WHERE pic.interestPaymentSchedule.security.idSecurityCodePk = :idSecurityCodePk");
//		sbQuery.append("	AND pic.stateProgramInterest = :state");
		sbQuery.append("	ORDER BY pic.couponNumber");
		Query query = em.createQuery(sbQuery.toString());
//		query.setParameter("state", ProgramScheduleStateType.PENDING.getCode());
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		return (List<Integer>)query.getResultList();
	}
	
	public AmortizationPaymentSchedule findAmortizationPaymentSchedule(String idSecurityCodePk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT aps FROM AmortizationPaymentSchedule aps");
		sbQuery.append("	INNER JOIN FETCH aps.programAmortizationCoupons pac");
		sbQuery.append("	WHERE aps.security.idSecurityCodePk = :idSecurityCodePk");
		sbQuery.append("	AND pac.stateProgramAmortization = :state");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		query.setParameter("state", ProgramScheduleStateType.PENDING.getCode());
		return (AmortizationPaymentSchedule) query.getSingleResult();
	}

	public InterestPaymentSchedule findInterestPaymentSchedule(String idSecurityCodePk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ips FROM InterestPaymentSchedule ips");
		sbQuery.append("	INNER JOIN FETCH ips.programInterestCoupons pic");
		sbQuery.append("	WHERE ips.security.idSecurityCodePk = :idSecurityCodePk");
		sbQuery.append("	AND pic.stateProgramInterest = :state");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		query.setParameter("state", ProgramScheduleStateType.PENDING.getCode());
		return (InterestPaymentSchedule) query.getSingleResult();
	}
	
	public SubproductCode findSubproductCode(String couponsRelated) {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT sc FROM SubproductCode sc");
			sbQuery.append("	WHERE sc.couponsRelated = :couponsRelated");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("couponsRelated", couponsRelated);
			return (SubproductCode)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public SubproductCode findLastSubproductCode() {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT sc FROM SubproductCode sc");
			sbQuery.append("	WHERE rownum < 2");
			sbQuery.append("	ORDER BY sc.idSubprodcutCodePk DESC");
			Query query = em.createQuery(sbQuery.toString());
			return (SubproductCode)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<HolderMarketFactBalance> findMarketFactBalance(String idSecurityCodePk, Long idHolderAccountPk){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT hmfb");
		sbQuery.append("	FROM HolderMarketFactBalance hmfb");
		sbQuery.append("	WHERE hmfb.security.idSecurityCodePk = :idSecurityCodePk");
		sbQuery.append("	AND hmfb.holderAccount.idHolderAccountPk = :idHolderAccountPk");
		sbQuery.append("	ORDER BY hmfb.marketDate, hmfb.marketRate ASC");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		query.setParameter("idHolderAccountPk", idHolderAccountPk);
		return (List<HolderMarketFactBalance>)query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<SplitCouponMarketFact> findSplitCouponMarketFact(Long idSplitOperationPk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT smf");
		sbQuery.append("	FROM SplitCouponMarketFact smf");
		sbQuery.append("	WHERE smf.splitCouponRequest.idSplitOperationPk = :idSplitOperationPk");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSplitOperationPk", idSplitOperationPk);
		return (List<SplitCouponMarketFact>)query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<PhysicalCertificate> findCertificates(String idSecurityCodePk) {
		StringBuilder sb = new StringBuilder();
		sb.append("		SELECT pc");
		sb.append("		FROM PhysicalCertificate PC");
		sb.append("		WHERE pc.securities.idSecurityCodePk = :idSecurityCodePk");
		sb.append("		AND pc.state = :physicalState");
		Query q = em.createQuery(sb.toString());
		q.setParameter("idSecurityCodePk", idSecurityCodePk);
		q.setParameter("physicalState", PhysicalCertificateStateType.CONFIRMED.getCode());
		return(List<PhysicalCertificate>) q.getResultList();
	}
}
