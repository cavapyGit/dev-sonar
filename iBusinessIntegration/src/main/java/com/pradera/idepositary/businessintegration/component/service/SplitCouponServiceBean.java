package com.pradera.idepositary.businessintegration.component.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.AccessTimeout;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.jfree.chart.plot.ThermometerPlot;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.RemoteLoggerUserInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.securities.service.SecuritiesServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.service.SecuritiesComponentService;
import com.pradera.integration.component.business.service.SplitCouponComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.component.business.to.SecuritiesComponentTO;
import com.pradera.integration.component.business.to.SplitCouponOperationTO;
import com.pradera.integration.component.securities.service.SecuritiesRemoteService;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.SecuritiesOperation;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.splitcoupons.SplitCouponMarketFact;
import com.pradera.model.custody.splitcoupons.SplitCouponOperation;
import com.pradera.model.custody.splitcoupons.SplitCouponRequest;
import com.pradera.model.custody.splitcoupons.SubproductCode;
import com.pradera.model.custody.splitcouponstype.SplitCouponStateType;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.AmortizationPaymentSchedule;
import com.pradera.model.issuancesecuritie.InterestPaymentSchedule;
import com.pradera.model.issuancesecuritie.ProgramAmortizationCoupon;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.SecurityForeignDepository;
import com.pradera.model.issuancesecuritie.SecurityInvestor;
import com.pradera.model.issuancesecuritie.SecurityNegotiationMechanism;
import com.pradera.model.issuancesecuritie.type.AmortizationOnType;
import com.pradera.model.issuancesecuritie.type.CalendarDayType;
import com.pradera.model.issuancesecuritie.type.CalendarMonthType;
import com.pradera.model.issuancesecuritie.type.CalendarType;
import com.pradera.model.issuancesecuritie.type.CapitalPaymentModalityType;
import com.pradera.model.issuancesecuritie.type.InterestClassType;
import com.pradera.model.issuancesecuritie.type.InterestPaymentModalityType;
import com.pradera.model.issuancesecuritie.type.InterestType;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.model.issuancesecuritie.type.PaymentScheduleStateType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.issuancesecuritie.type.SecurityTermType;
import com.pradera.model.valuatorprocess.ValuatorProcessClass;

@Singleton
public class SplitCouponServiceBean implements SplitCouponComponentService {

	
	@EJB SplitCouponAdditionalServiceBean additionalComponentServiceBean;
	@EJB ParameterServiceBean parameterServiceBean;
	@Inject SecuritiesServiceBean securitiesServiceBean;
	@Inject Instance<SecuritiesRemoteService> securitiesRemoteService;
	@Inject Instance<AccountsComponentService> accountsComponentService;
	@Inject Instance<SecuritiesComponentService> securitiesComponentService;
	
	@Resource TransactionSynchronizationRegistry transactionRegistry;
	
	@Inject @Configurable String idIssuerBC;
	@Inject @Configurable String idIssuerTGN;
	
	@Override
    @AccessTimeout(unit=TimeUnit.MINUTES,value=20)
    @Interceptors(RemoteLoggerUserInterceptor.class)
	public void confirmSplitCouponProcess(SplitCouponOperationTO splitCouponOperationTO) throws ServiceException {
		List<Security> securitiesList = new ArrayList<Security>();
		//List<String> secList = new ArrayList<>();
 		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);  
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
	    /**FIND SPLIT REQUEST OBJECT*/
	    Long splitRequestId = splitCouponOperationTO.getIdSplitCouponOperationPk();
	    SplitCouponRequest splitCouponRequest = additionalComponentServiceBean.getDetachmentCouponRequest(splitRequestId);
		/**VARIABLE QUE MANEJARA EL MAYOR SALDO DEL CUPON A DESPRENDER*/
		BigDecimal greaterBalanceCoupon = additionalComponentServiceBean.getGreaterBalanceCoupon(splitCouponRequest.getSplitCouponOperations());
		HolderAccountBalance balances = additionalComponentServiceBean.getBalances(splitCouponRequest);
		BigDecimal availableBalance = balances.getAvailableBalance();
		BigDecimal totalBalance = balances.getTotalBalance();
		if(!SplitCouponStateType.APPROVED.getCode().equals(splitCouponRequest.getRequestState())){
			throw new ServiceException(ErrorServiceType.SPLIT_COUPON_REQUEST_MODIFIED_STATE, new Object[]{splitCouponRequest.getIdSplitOperationPk().toString()});
		}
		if(ParameterOperationType.SECURITIES_DETACHMENT_NOT_BLOCKED.getCode().equals(splitCouponRequest.getCustodyOperation().getOperationType()) && 
		   greaterBalanceCoupon.compareTo(availableBalance) == 1){
			throw new ServiceException(ErrorServiceType.SPLIT_COUPON_REQUEST_INVALID_CONFIRM, new Object[]{splitCouponRequest.getIdSplitOperationPk().toString()});
		}
		splitCouponRequest.setRequestState(SplitCouponStateType.CONFIRMED.getCode());
		splitCouponRequest.getCustodyOperation().setState(SplitCouponStateType.CONFIRMED.getCode());
		splitCouponRequest.getCustodyOperation().setConfirmUser(loggerUser.getUserName());
		splitCouponRequest.getCustodyOperation().setConfirmDate(CommonsUtilities.currentDateTime());
		List<Integer> lstCoupons = new ArrayList<Integer>();
		/** SE LLAMA AL COMPONENTE PARA LOS VALORES **/
		SecuritiesOperation securitiesOperation = createSecOperation(splitCouponRequest.getSecurity(), splitCouponRequest.getCustodyOperation(), greaterBalanceCoupon);
		securitiesComponentService.get().executeSecuritiesComponent(getSecuritiesComponentTO(securitiesOperation,
															BusinessProcessType.COUPON_DETACHMENT_OPERATION_CONFIRM.getCode(), false));
		/**ACTUALIZAMOS LOS MONTOS DEL VALOR A SER DESPRENDIDO*/
		additionalComponentServiceBean.updateSecuritiesBalances(splitCouponRequest.getSecurity(), greaterBalanceCoupon, ComponentConstant.SUBTRACTION);
		
		/**LISTA PARA ALMACENAR CUENTAS_MATRIZ E INSERTAR SALDO DE CUPONES DESPRENDIDOS*/
		List<HolderAccountBalanceTO> targeAccountBalanceTOs = new ArrayList<HolderAccountBalanceTO>();
		/**LISTA PARA ALMACENAR HECHOS DE MERCADO EN LAS CUENTAS_MATRIZ*/
		List<MarketFactAccountTO> _lstMarketFactTO = getMarketFactAccountFromSplit(splitCouponRequest.getIdSplitOperationPk(), splitCouponOperationTO.getIndMarketFact(),BooleanType.YES.getCode());
		List<MarketFactAccountTO> _lstMarketFactSrcTO = getMarketFactAccountFromSplit(splitCouponRequest.getIdSplitOperationPk(), splitCouponOperationTO.getIndMarketFact(),BooleanType.NO.getCode());

		/**VALOR A SER DESPRENDIDO*/
		Security securityDetach    = additionalComponentServiceBean.find(Security.class, splitCouponRequest.getSecurity().getIdSecurityCodePk());
		/**VARIABLE PARA MANEJAR EL PRINCIPAL VALOR DESPRENDIDO(PADRE,ABUELO,BISABUELO,TATARABUELO, ETC)*/
		Security securityRef       = securityDetach.getSecurity();
		/**OBTENEMOS EL VERDADERO PRINCIPAL VALOR, PUEDE SER PAPA DEL SUB-PRODUCTO o EL VALOR A DESPRENDER ES EL PRINCIPAL*/
		Security securityFather    = securityRef!=null?securityRef:securityDetach;
		/**OBTENEMOS EL CODIGO DEL PRINCIPAL VALOR*/
		String securityMainCode  = securityFather.getIdSecurityCodeOnly();
		/**OBTENEMOS EL DEL SUBPRODUCTO A RELACIONAR, SIEMPRE Y CUANDO EL VALOR A DESPRENDER ES SUB-PRODUCTO*/
		String securityProductCode = securityRef!=null?securityDetach.getIdSecurityCodePk():null;
		/** UNA VEZ REALIZADO UN SPLIT SE DEBE MARCAR EN SI **/
		securityDetach.setIndHasSplitSecurities(BooleanType.YES.getCode());
		/** EN CASO SEA FISICO SE BUSCA EN BOVEDA **/
		List<PhysicalCertificate> lstCertificate = new ArrayList<PhysicalCertificate>();
		if(securityFather.getIssuanceForm().equals(IssuanceType.PHYSICAL.getCode())) {
			lstCertificate = additionalComponentServiceBean.findCertificates(securityFather.getIdSecurityCodePk());
		}
		for(SplitCouponOperation splitCouponOperation:splitCouponRequest.getSplitCouponOperations()){
			/**VARIABLE PARA MANEJAR EL INTERES RELACIONADO AL DESPRENDIMIENTO*/
			ProgramInterestCoupon programInteresCoupon = additionalComponentServiceBean.find(ProgramInterestCoupon.class, splitCouponOperation.getProgramInterestCoupon().getIdProgramInterestPk());
			/**VARIABLE PARA MANEJAR LA CANTIDAD A DESPRENDER EN EL CUPON*/
			BigDecimal splitCouponQuantity = splitCouponOperation.getOperationQuantity();
			/**OBTENEMOS LOS HECHOS DE MERCADO APROPIADOS PARA UN DETERMINADO CUPON*/
			List<MarketFactAccountTO> lstMarketFactTO = new ArrayList<MarketFactAccountTO>();
			if(Validations.validateListIsNotNullAndNotEmpty(_lstMarketFactTO)) {
				lstMarketFactTO = getMarketFactAccountsByQuantity(_lstMarketFactTO, splitCouponQuantity);
			}		
			/**INSERTAMOS A LISTA QUE MANEJARA CUENTA MATRIZ CON NUEVO VALOR-COUPON GENERADO*/
			lstCoupons.add(splitCouponOperation.getProgramInterestCoupon().getCouponNumber());
			String securityCouponCodePk = null;
			/**GENERATE TEMPORALLY SECURITY COUPON CODE*/
			String idSecurityCodePkTemp = generateNewSecurityCode(securityMainCode,splitCouponOperation.getProgramInterestCoupon(), null);
			String securityCouponCode = SecurityClassType.CUP.name().concat("-").concat(idSecurityCodePkTemp);
			/** SI ES FISICO EL CODIGO DE VALOR VIENE DEL CUPON GUARDADO EN BOVEDA **/
			String physicIdSecurityCode = null;
			if(securityFather.getIssuanceForm().equals(IssuanceType.PHYSICAL.getCode())) {
				securityCouponCode = getPhysicalCouponPendingSecurityCode(securityFather.getIdSecurityCodePk(), lstCertificate, splitCouponOperation.getProgramInterestCoupon() );
				idSecurityCodePkTemp = securityCouponCode.substring(SecurityClassType.get(securityFather.getSecurityClass()).name().length() + 1);
				physicIdSecurityCode = securityCouponCode;
			}
			Security securityCoupon = additionalComponentServiceBean.find(Security.class, securityCouponCode);
			/** SE VALIDA SI EL CUPON YA EXISTE COMO VALOR **/
			if(securityCoupon!=null){
				/**UPDATE BALANCES OF SECURITY-COUPON*/
				additionalComponentServiceBean.updateSecuritiesBalances(securityCoupon, splitCouponQuantity, ComponentConstant.SUM);
				/**INSERTAMOS A LISTA QUE MANEJARA CUENTA MATRIZ CON NUEVO VALOR-COUPON GENERADO*/
				securityCouponCodePk = securityCoupon.getIdSecurityCodePk();
				targeAccountBalanceTOs.addAll(getHolderAccountBalanceTOs(splitCouponRequest, splitCouponQuantity, securityCouponCodePk, lstMarketFactTO));
				/** SE LLAMA AL COMPONENTE PARA LOS VALORES **/
				securitiesOperation = createSecOperation(securityCoupon,splitCouponRequest.getCustodyOperation(), splitCouponQuantity);
				securitiesComponentService.get().executeSecuritiesComponent(getSecuritiesComponentTO(securitiesOperation,
																				BusinessProcessType.COUPON_DETACHMENT_OPERATION_CONFIRM.getCode(), true));				
				/**RELACIONAMOS EL DESPRENDIMIENTO CON EL CRONOGRAMA DE INTERES DESPRENDIDO*/
				programInteresCoupon.setSplitSecurities(securityCoupon);
				/**ACTUALIZAMOS REFERENCIA DE SPLIT_SECURITIES EN EL PROGRAMA DE INTERES*/				
				securityCoupon.setFsinCode(securitiesServiceBean.getShortCfiCode(securityCoupon));
				securityCoupon.setCfiCode(securitiesServiceBean.generateCFICode(securityCoupon));
				additionalComponentServiceBean.update(securityCoupon);
				continue;
			}
			
			/**CREATE NEW SECURITY BY DETACHMENT COUPON**/
			Security security = createSecurity(splitCouponQuantity, securityFather,programInteresCoupon, loggerUser, idSecurityCodePkTemp, securityProductCode, physicIdSecurityCode);						
			/**CREAMOS UN NUEVO FSIN y CFI*/
//			try {
//				security.setFsinCode(securitiesServiceBean.getShortCfiCode(security));
//				security.setCfiCode(securitiesServiceBean.generateCFICode(security));
//			} catch (Exception e) {
//				Log.error(e.getMessage(), e);
//			}
			
			/**INSERTAMOS A LISTA QUE MANEJARA CUENTA MATRIZ CON NUEVO VALOR-COUPON GENERADO*/
			securityCouponCodePk = security.getIdSecurityCodePk();
			targeAccountBalanceTOs.addAll(getHolderAccountBalanceTOs(splitCouponRequest, splitCouponQuantity, securityCouponCodePk, lstMarketFactTO));
		
			/** SE LLAMA AL COMPONENTE PARA LOS VALORES **/
			securitiesOperation = createSecOperation(security, splitCouponRequest.getCustodyOperation(), splitCouponQuantity);
			securitiesComponentService.get().executeSecuritiesComponent(getSecuritiesComponentTO(securitiesOperation,
																BusinessProcessType.COUPON_DETACHMENT_OPERATION_CONFIRM.getCode(), true));
			/** create amortization schedule for new security **/
			AmortizationPaymentSchedule amortizationPaymentSchedule = createAmortizationPaymentSchedule(security, programInteresCoupon, loggerUser);
			security.setAmortizationPaymentSchedule(amortizationPaymentSchedule);
			securitiesServiceBean.createAmoPaymentScheduleCorpEvents(security, loggerUser);
			/** set the new security in the coupon **/
			programInteresCoupon.setSplitSecurities(security);
			additionalComponentServiceBean.update(programInteresCoupon);
			/**Validation to get automatically valuator Class*/
			// Se comenta la linea porque no va a haber conexion remota para evaluación
			/*String valuatorClassPk =null;
			try {
				valuatorClassPk = securitiesRemoteService.get().getAutomaticValuatorClassPk(security.getIdSecurityCodePk());
			} catch (Exception e) {
			}
			if(valuatorClassPk!=null){
				ValuatorProcessClass valuatorClass = new ValuatorProcessClass(valuatorClassPk);
				security.setValuatorProcessClass(valuatorClass);
			}*/
			/**TEMP, ONLY TO SEE SECURITIES IN DEBUGING TIME*/
			securitiesList.add(security);
		}
		List<Integer> lstPendingCoupons = additionalComponentServiceBean.getPendingCoupons(securityDetach.getIdSecurityCodePk());

		/**FIND REMAINING COUPON PRODUCTS TO CREATE SUB-PRODUCTS*/
		Map<List<Integer>,BigDecimal> remainingCouponProducts = getSecurityProductsToCreate(splitCouponRequest, lstPendingCoupons, totalBalance);
		/**ITERATE ALL REMAINING COUPONS TO CREATE SUB-PRODUCTS*/
		for(Map.Entry<List<Integer>, BigDecimal> entry : remainingCouponProducts.entrySet()){
			List<Integer> lstCouponsSubProduct = entry.getKey();
			List<Integer> lstRemaingCouponsSubProduct = new ArrayList<Integer>();
			BigDecimal subProductBalance = entry.getValue();
			for(Integer coupon: lstPendingCoupons){
				if(!lstCouponsSubProduct.contains(coupon)){
					lstRemaingCouponsSubProduct.add(coupon);
				}
			}
			/**BEGIN OF PROCESS TO CREATE SECURITY SUBPRODUCT*/
//			Security securityFather = splitCouponRequest.getSecurity();
			AmortizationPaymentSchedule sourceAmortization = additionalComponentServiceBean.findAmortizationPaymentSchedule(securityFather.getIdSecurityCodePk());
			InterestPaymentSchedule sourceInterest = additionalComponentServiceBean.findInterestPaymentSchedule(securityFather.getIdSecurityCodePk());
			String idSecurityCodePkTemp = generateNewSecurityCode(securityMainCode, null, lstRemaingCouponsSubProduct);
			String securityClass = ((ParameterTable) additionalComponentServiceBean.getParameterValue(securityFather.getSecurityClass())).getText1();
			String securityProdCod= securityClass.concat("-").concat(idSecurityCodePkTemp);
			Security securityProduct = additionalComponentServiceBean.find(Security.class, securityProdCod);
			if(Validations.validateIsNull(securityProduct)){
				/**	CREATE THE SECURITY SUBPRODUCT **/
				securityProduct = createSecurity(subProductBalance,securityFather, null, loggerUser, idSecurityCodePkTemp,securityProductCode, null);
				/** SE LLAMA AL COMPONENTE PARA LOS VALORES **/
				securitiesOperation = createSecOperation(securityProduct, splitCouponRequest.getCustodyOperation(), subProductBalance);
				securitiesComponentService.get().executeSecuritiesComponent(getSecuritiesComponentTO(securitiesOperation,
																	BusinessProcessType.COUPON_DETACHMENT_OPERATION_CONFIRM.getCode(), true));
				/** CREATE THE AMORTIZATION PAYMENT SCHEDULE FOR SUBPRODUCT **/
				AmortizationPaymentSchedule amortizationPaymentSchedule = new AmortizationPaymentSchedule();
				amortizationPaymentSchedule.setAmortizationFactor(sourceAmortization.getAmortizationFactor());
				amortizationPaymentSchedule.setAmortizationType(securityProduct.getAmortizationType());
				amortizationPaymentSchedule.setCalendarType(securityProduct.getCalendarType());
				amortizationPaymentSchedule.setCorporativeDays(securityProduct.getCorporativeProcessDays());
				amortizationPaymentSchedule.setPeriodicity(sourceAmortization.getPeriodicity());
				amortizationPaymentSchedule.setRegistryDays(sourceAmortization.getRegistryDays());
				amortizationPaymentSchedule.setPeriodicityDays(sourceAmortization.getPeriodicityDays());
				amortizationPaymentSchedule.setScheduleState(PaymentScheduleStateType.REGTERED.getCode());
				amortizationPaymentSchedule.setSecurity(securityProduct);
				amortizationPaymentSchedule.setPaymentModality(CapitalPaymentModalityType.AT_MATURITY.getCode());
				
				additionalComponentServiceBean.create(amortizationPaymentSchedule);
				List<ProgramAmortizationCoupon> lstProgramAmortizationCoupon = new ArrayList<ProgramAmortizationCoupon>();
				for(ProgramAmortizationCoupon sourceProgramAmortizationCoupon:sourceAmortization.getProgramAmortizationCoupons()){
					lstProgramAmortizationCoupon.add(createAmortizationCoupon(securityProduct, amortizationPaymentSchedule, sourceProgramAmortizationCoupon, loggerUser));
				}
				securityProduct.setAmortizationPaymentSchedule(amortizationPaymentSchedule);
				securityProduct.getAmortizationPaymentSchedule().setProgramAmortizationCoupons(lstProgramAmortizationCoupon);
				securitiesServiceBean.createAmoPaymentScheduleCorpEvents(securityProduct, loggerUser);
				/** CREATE THE INTEREST PAYMENT SCHEDULE FOR SUBPRODUCT **/
				if(Validations.validateListIsNotNullAndNotEmpty(lstCouponsSubProduct)){
					InterestPaymentSchedule interestPaymentSchedule = new InterestPaymentSchedule();
					interestPaymentSchedule.setInterestFactor(sourceInterest.getInterestFactor());
					interestPaymentSchedule.setInterestType(securityProduct.getInterestType());
					interestPaymentSchedule.setCalendarType(securityProduct.getCalendarType());
					interestPaymentSchedule.setPeriodicity(sourceInterest.getPeriodicity());
					interestPaymentSchedule.setPeriodicityDays(interestPaymentSchedule.getPeriodicityDays());
					interestPaymentSchedule.setRegistryDays(sourceInterest.getRegistryDays());
					interestPaymentSchedule.setScheduleState(PaymentScheduleStateType.REGTERED.getCode());
					interestPaymentSchedule.setSecurity(securityProduct);
					interestPaymentSchedule.setCalendarMonth(sourceInterest.getCalendarMonth());
					interestPaymentSchedule.setCalendarDays(sourceInterest.getCalendarDays());
					interestPaymentSchedule.setExpirationDate(sourceInterest.getExpirationDate());
					additionalComponentServiceBean.create(interestPaymentSchedule);
					List<ProgramInterestCoupon> lstProgramaInterestCoupon = new ArrayList<ProgramInterestCoupon>();
					for(ProgramInterestCoupon programInterestCoupon:sourceInterest.getProgramInterestCoupons()){
						if(lstCouponsSubProduct.contains(programInterestCoupon.getCouponNumber())){
							lstProgramaInterestCoupon.add(createInterestCoupon(securityProduct, interestPaymentSchedule, programInterestCoupon, loggerUser));
						}
					}
					securityProduct.setInterestPaymentSchedule(interestPaymentSchedule);
					securityProduct.getInterestPaymentSchedule().setProgramInterestCoupons(lstProgramaInterestCoupon);
					/**Method to calculate coupon amount on ProgramInterest*/
					securitiesServiceBean.createIntPaymentScheduleCorpEvents(securityProduct, loggerUser);
					/**Validation to get automatically valuator Class*/
					/*String valuatorClassPk = null;
					try {
						valuatorClassPk = securitiesRemoteService.get().getAutomaticValuatorClassPk(securityProduct.getIdSecurityCodePk());
					} catch (Exception e) {
						// TODO: handle exception
					}
					if(valuatorClassPk!=null){
						ValuatorProcessClass valuatorClass = new ValuatorProcessClass(valuatorClassPk);
						securityProduct.setValuatorProcessClass(valuatorClass);
					}*/
				}
			}else{
				
				/** SE LLAMA AL COMPONENTE PARA LOS VALORES **/
				securitiesOperation = createSecOperation(securityProduct, splitCouponRequest.getCustodyOperation(), subProductBalance);
				securitiesComponentService.get().executeSecuritiesComponent(getSecuritiesComponentTO(securitiesOperation,
																	BusinessProcessType.COUPON_DETACHMENT_OPERATION_CONFIRM.getCode(), true));
				
				additionalComponentServiceBean.updateSecuritiesBalances(securityProduct, subProductBalance, ComponentConstant.SUM);
			}
			securitiesList.add(securityProduct);
			splitCouponRequest.setSecurityProduct(securityProduct);
			/**OBTENEMOS LOS HECHOS DE MERCADO APROPIADOS PARA UN DETERMINADO SUBPRODUCTO*/
			List<MarketFactAccountTO> lstMarketFactTO = new ArrayList<MarketFactAccountTO>();
			if(Validations.validateListIsNotNullAndNotEmpty(_lstMarketFactTO)) {
				lstMarketFactTO = getMarketFactAccountsByQuantity(_lstMarketFactTO, subProductBalance);				
			}
			/**INSERTAMOS SALDOS DEL SUBPRODUCTO EN LA CUENTA MATRIZ*/
			targeAccountBalanceTOs.addAll(getHolderAccountBalanceTOs(splitCouponRequest, subProductBalance, securityProduct.getIdSecurityCodePk(), lstMarketFactTO));
//			if(securityProduct.getSecurityClass().equals(SecurityClassType.BTS.getCode())||
//					securityProduct.getSecurityClass().equals(SecurityClassType.LBS.getCode()))
//				secList.add(securityProduct.getIdSecurityCodePk());
		}
		
		/*** SE LLAMA AL COMPONENTE PARA RETIRAR LOS SALDOS E INGRESAR A LOS NUEVOS VALORES***/
//		List<HolderAccountBalanceTO> accountBalanceTOs = getHolderAccountBalanceTOs(splitCouponRequest,greaterBalanceCoupon, lstMarketFactTO);
		List<HolderAccountBalanceTO> accountBalanceTOs = getHolderAccountBalanceTOs(splitCouponRequest,greaterBalanceCoupon, _lstMarketFactSrcTO);
		accountsComponentService.get().executeAccountsComponent(getAccountsComponentTO(splitCouponRequest.getIdSplitOperationPk(),
																BusinessProcessType.COUPON_DETACHMENT_OPERATION_CONFIRM.getCode(),
																accountBalanceTOs, targeAccountBalanceTOs, 
																splitCouponRequest.getCustodyOperation().getOperationType(), splitCouponOperationTO));
//		throw new ServiceException();
		
	}
	
	/**METODO QUE MANEJA LOS HECHOS DE MERCADO A PARTIR DE UNA DETERMINADA CANTIDAD, SE ASUMEN QUE SOLO HAY 1 HECHO DE MERCADO, CASO CONTRARIO
	 * EL CRITERIO A USAR LOS HM DEBE SER PROPORCIONADO POR LA DEPOSITARIA*/
	public List<MarketFactAccountTO> getMarketFactAccountsByQuantity(List<MarketFactAccountTO> markFactAccounts, BigDecimal quantity) throws ServiceException{
		/**PRIMERO DEBEMOS SABER LA CANTIDAD TOTAL DE LA OPERACION SPLIT*/
		BigDecimal splitQuantityRequest = BigDecimal.ZERO;
		for(MarketFactAccountTO markAccount: markFactAccounts){
			splitQuantityRequest = splitQuantityRequest.add(markAccount.getQuantity());
		}

		/**SI EL DESPRENDIMIENTO TIENE CANTIDAD IGUAL EN TODA LA CUPONERA*/
		if(splitQuantityRequest.equals(quantity)){
			return markFactAccounts;
		}else{
			/**EL DESPRENDIMIENTO TIENE CANTIDADES PARCIALES*/
			if(markFactAccounts.isEmpty()){
				throw new ServiceException(ErrorServiceType.SPLIT_COUPON_INVALID_DETACHMENT);
			}else if(markFactAccounts.size() == BigDecimal.ONE.intValue()){/**PARA EL CASO DE 1 SOLO HECHO DE MERCADO, SE ASIGNA LA CANTIDAD COMO PARAMETRO*/
				List<MarketFactAccountTO> marketFactAccounts = new ArrayList<MarketFactAccountTO>();
				for(MarketFactAccountTO markAccount: markFactAccounts){
					MarketFactAccountTO newMarkAccount = new MarketFactAccountTO();
					newMarkAccount.setMarketDate(markAccount.getMarketDate());
					newMarkAccount.setMarketRate(markAccount.getMarketRate());
//					newMarkAccount.setMarketPrice(markAccount.getMarketPrice());
					newMarkAccount.setMarketPrice(BigDecimal.ZERO);
					newMarkAccount.setQuantity(quantity);
					marketFactAccounts.add(newMarkAccount);
				}
				return marketFactAccounts;
			}else{
				/**CRITERIO A DEFINIR POR LA DEPOSITARIA, SOLO PARA EL CASO DE SIRTEX*/
				throw new ServiceException(ErrorServiceType.SPLIT_COUPON_INVALID_MARKETFACT);
			}
		}
	}
	
	public Map<List<Integer>,BigDecimal> getSecurityProductsToCreate(SplitCouponRequest splitRequest, List<Integer> allCoupons,
																	 BigDecimal totalBalance)throws ServiceException{
		
		/**GET LIST SPLIT COUPON*/
		List<SplitCouponOperation> splitDetailList = splitRequest.getSplitCouponOperations();
		/**MAP WILL HANDLE ALL COUPONS WITH BALANCES FOR SUB-PRODUCTS*/
		Map<List<Integer>, BigDecimal> counponsForProducts = new HashMap<List<Integer>,BigDecimal>();
		/**GET THE GREATHER BALANCE'S COUPON */
		BigDecimal detachSplitBalance = additionalComponentServiceBean.getGreaterBalanceCoupon(splitDetailList);
		/**MAP WILL HANDLE COUPONS REMAINING TO COMPLETE SPLIT OPERATION*/
		Map<Integer,BigDecimal> mapRemainingBalance = new HashMap<Integer,BigDecimal>();
		/**VAR TO HANDLE IF SPLIT COUPON IS PARTIAL(CASE OF SIRTEX) OR TOTAL*/
		Boolean splitIsPartial = Boolean.FALSE;
		
		for(SplitCouponOperation subDetail: splitDetailList){
			Integer couponNumber = subDetail.getProgramInterestCoupon().getCouponNumber();
			BigDecimal operationQuantity = subDetail.getOperationQuantity();
			/**UPDATE REMAINING MAP TO HANDLE THE DATA*/
			BigDecimal remainingBalance  = detachSplitBalance.subtract(operationQuantity);
			mapRemainingBalance.put(couponNumber, remainingBalance);
			/**IF REMAINING QUANTITY OF SPLIT_DETAIL IS DIFFERENT THAN 0 IT'S PARTIAL*/
			if(!splitIsPartial && !remainingBalance.equals(BigDecimal.ZERO)){
				splitIsPartial = Boolean.TRUE;
			}
		}
		
		/**ITERATE ALL REMAINING COUPONS AND PUT GREATHER BALANCE ON COUPONS WHO DON'T BE IN SPLIT REQUEST*/
		for(Integer coupon: allCoupons){
			if(mapRemainingBalance.get(coupon)==null){
				mapRemainingBalance.put(coupon, detachSplitBalance);
			}
		}
		
		/**IF SPLIT IS QUANTITY TOTAL FOR ALL DETAIL*/
		if(!splitIsPartial){
			List<Integer> couponsRelated = new ArrayList<Integer>();
			for(Map.Entry<Integer, BigDecimal> entry : mapRemainingBalance.entrySet()){
				if(!entry.getValue().equals(BigDecimal.ZERO)){
					couponsRelated.add(entry.getKey());
				}
			}
			/**ASSIGMENT COUPONS RELATED WITH ONE QUANTITY BY DEFAULT IT'S GREATHER COUPON'S BALANCE*/
			counponsForProducts.put(couponsRelated, detachSplitBalance);
		}else{
			/**ITERATE THE LOWER COUPON BALANCE TO HANDLE REMAING WITH RESPECTIVELY QUANTITY */
			BigDecimal lowerBalance = additionalComponentServiceBean.findLowerBalance(mapRemainingBalance, detachSplitBalance);
			BigDecimal capitalBalance = BigDecimal.ZERO;
			while(!lowerBalance.equals(BigDecimal.ZERO)){
				List<Integer> couponsRelated = new ArrayList<Integer>();
				/**SUBSCTRACT TO LOWER BALANCE ALL COUPON WHO HAVE ENOUGH BALANCE*/
				for(Map.Entry<Integer, BigDecimal> entry : mapRemainingBalance.entrySet()){
					Integer couponNumber = entry.getKey();
					BigDecimal balance = entry.getValue();
					/**VERIFIED IF REMAINING COUPON HAVE ENOUGH BALANCE && PUT COUNPON_RELATE TO THE SUBPRODUCT */
					if(lowerBalance.compareTo(balance)<=0){
						couponsRelated.add(couponNumber);
						mapRemainingBalance.put(couponNumber, balance.subtract(lowerBalance));
					}
				}
				counponsForProducts.put(couponsRelated,lowerBalance);
				capitalBalance = capitalBalance.add(lowerBalance);
				/**FIND LOWER BALANCE'S COUPON AGAIN*/
				lowerBalance = additionalComponentServiceBean.findLowerBalance(mapRemainingBalance, detachSplitBalance);
			}
			/**IN THE CASE ALL COUPONS WERE DETACHMENTED THE MAKRO IT'S MANDATORY IN NEW SUBPRODUCT WITHOUT COUPONS*/
			if(!capitalBalance.equals(detachSplitBalance)){
				counponsForProducts.put(new ArrayList<Integer>(),detachSplitBalance.subtract(capitalBalance));
			}
		}
		return counponsForProducts;
//		counponsForProducts;
//		throw new ServiceException(ErrorServiceType.TRADE_OPERATION_ACEPTED_QUANTITY);
	}

	public List<MarketFactAccountTO> getMarketFactAccountFromSplit(Long splitOperationId, Integer indMarketFact, Integer indZeroPrice) throws ServiceException{
		List<SplitCouponMarketFact> lstSplitCouponMarketFact = null;
		List<MarketFactAccountTO> lstMarketFactTO = null;
		if(BooleanType.YES.getCode().equals(indMarketFact)){
			lstSplitCouponMarketFact = additionalComponentServiceBean.findSplitCouponMarketFact(splitOperationId);
		}
		if(Validations.validateListIsNotNullAndNotEmpty(lstSplitCouponMarketFact)){
			lstMarketFactTO = new ArrayList<MarketFactAccountTO>();
			for(SplitCouponMarketFact splitCouponMarketFact:lstSplitCouponMarketFact){
				MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
				marketFactAccountTO.setMarketDate(splitCouponMarketFact.getMarketDate());
				marketFactAccountTO.setMarketRate(splitCouponMarketFact.getMarketRate());
				if(indZeroPrice.equals(BooleanType.YES.getCode())){
					marketFactAccountTO.setMarketPrice(BigDecimal.ZERO);
				}else{
					marketFactAccountTO.setMarketPrice(splitCouponMarketFact.getMarketPrice());
				}
				marketFactAccountTO.setQuantity(splitCouponMarketFact.getOperationQuantity());
				lstMarketFactTO.add(marketFactAccountTO);
			}
		}
		return lstMarketFactTO;
	}

	private String generateNewSecurityCode(String idSecurityCodePk, ProgramInterestCoupon programInterestCoupon, List<Integer> lstCoupons) throws ServiceException{
		String idSecurityCode = idSecurityCodePk;
		if(Validations.validateIsNotNullAndNotEmpty(programInterestCoupon)){
			if(programInterestCoupon.getCouponNumber().toString().length() < 2)
				idSecurityCode += GeneralConstants.HYPHEN +GeneralConstants.ZERO_VALUE_STRING + GeneralConstants.ZERO_VALUE_STRING  + programInterestCoupon.getCouponNumber().toString();
			else if(programInterestCoupon.getCouponNumber().toString().length() < 3)
				idSecurityCode += GeneralConstants.HYPHEN + GeneralConstants.ZERO_VALUE_STRING + programInterestCoupon.getCouponNumber().toString();
			else
				idSecurityCode += GeneralConstants.HYPHEN + programInterestCoupon.getCouponNumber().toString();
		}else{
			//Se recupera la codificacion segun los cupones desprendidos
			String couponsRelated = GeneralConstants.EMPTY_STRING;
			for(Integer couponNumber:lstCoupons){
				couponsRelated += couponNumber.toString() + GeneralConstants.STR_COMMA.substring(0, 1);
			}
			couponsRelated = couponsRelated.substring(0, couponsRelated.length() - 1);
			SubproductCode subproductCode = additionalComponentServiceBean.findSubproductCode(couponsRelated);
			if(Validations.validateIsNotNullAndNotEmpty(subproductCode)){
				String codification = null;
				codification  = subproductCode.getFirstLetterCode();
				codification += subproductCode.getSecondLetterCode();
				codification += subproductCode.getThirdLetterCode();
				codification += subproductCode.getFourthLetterCode();
				idSecurityCode += GeneralConstants.HYPHEN + codification;
			}else{
				subproductCode = additionalComponentServiceBean.findLastSubproductCode();
				if(Validations.validateIsNullOrEmpty(subproductCode)){
					subproductCode = new SubproductCode();
					subproductCode.setCouponsRelated(couponsRelated);
					subproductCode.setFirstLetterCode(GeneralConstants.ZERO_VALUE_STRING);
					subproductCode.setSecondLetterCode(GeneralConstants.ZERO_VALUE_STRING);
					subproductCode.setThirdLetterCode(GeneralConstants.ZERO_VALUE_STRING);
					subproductCode.setFourthLetterCode(GeneralConstants.ONE_VALUE_STRING);
					additionalComponentServiceBean.create(subproductCode);
					String codification = null;
					codification  = subproductCode.getFirstLetterCode();
					codification += subproductCode.getSecondLetterCode();
					codification += subproductCode.getThirdLetterCode();
					codification += subproductCode.getFourthLetterCode();
					idSecurityCode += GeneralConstants.HYPHEN + codification;
				}else{
					String letter = null;
					SubproductCode newSubproductCode = new SubproductCode();
					newSubproductCode.setCouponsRelated(couponsRelated);
					if(GeneralConstants.ZERO_VALUE_STRING.equals(subproductCode.getSecondLetterCode())
							&& GeneralConstants.ZERO_VALUE_STRING.equals(subproductCode.getThirdLetterCode())
							&& GeneralConstants.ZERO_VALUE_STRING.equals(subproductCode.getFirstLetterCode())){
						letter = findLetter(subproductCode.getFourthLetterCode());
						if(Validations.validateIsNullOrEmpty(letter)){
							newSubproductCode.setFourthLetterCode(GeneralConstants.ONE_VALUE_STRING);
							newSubproductCode.setThirdLetterCode(GeneralConstants.ONE_VALUE_STRING);
							newSubproductCode.setSecondLetterCode(subproductCode.getSecondLetterCode());
							newSubproductCode.setFirstLetterCode(subproductCode.getFirstLetterCode());
						}else{
							newSubproductCode.setFirstLetterCode(subproductCode.getFirstLetterCode());
							newSubproductCode.setSecondLetterCode(subproductCode.getSecondLetterCode());
							newSubproductCode.setThirdLetterCode(subproductCode.getThirdLetterCode());
							newSubproductCode.setFourthLetterCode(letter);
						}
					}else if(GeneralConstants.ZERO_VALUE_STRING.equals(subproductCode.getFirstLetterCode())
							&& GeneralConstants.ZERO_VALUE_STRING.equals(subproductCode.getSecondLetterCode())){
						letter = findLetter(subproductCode.getFourthLetterCode());
						if(Validations.validateIsNullOrEmpty(letter)){
							letter = findLetter(subproductCode.getThirdLetterCode());
							if(Validations.validateIsNullOrEmpty(letter)){
								newSubproductCode.setFirstLetterCode(subproductCode.getFirstLetterCode());
								newSubproductCode.setSecondLetterCode(GeneralConstants.ONE_VALUE_STRING);
								newSubproductCode.setThirdLetterCode(GeneralConstants.ONE_VALUE_STRING);
								newSubproductCode.setFourthLetterCode(GeneralConstants.ONE_VALUE_STRING);
							}else{
								newSubproductCode.setFirstLetterCode(subproductCode.getFirstLetterCode());
								newSubproductCode.setSecondLetterCode(subproductCode.getSecondLetterCode());
								newSubproductCode.setThirdLetterCode(letter);
								newSubproductCode.setFourthLetterCode(GeneralConstants.ONE_VALUE_STRING);
							}
						}else{
							newSubproductCode.setFirstLetterCode(subproductCode.getFirstLetterCode());
							newSubproductCode.setSecondLetterCode(subproductCode.getSecondLetterCode());
							newSubproductCode.setThirdLetterCode(subproductCode.getThirdLetterCode());
							newSubproductCode.setFourthLetterCode(letter);							
						}
					}else if(GeneralConstants.ZERO_VALUE_STRING.equals(subproductCode.getFirstLetterCode())){
						letter = findLetter(subproductCode.getFourthLetterCode());
						if(Validations.validateIsNullOrEmpty(letter)){
							letter = findLetter(subproductCode.getThirdLetterCode());
							if(Validations.validateIsNullOrEmpty(letter)){
								letter = findLetter(subproductCode.getSecondLetterCode());
								if(Validations.validateIsNullOrEmpty(letter)){
									newSubproductCode.setFirstLetterCode(GeneralConstants.ONE_VALUE_STRING);
									newSubproductCode.setSecondLetterCode(GeneralConstants.ONE_VALUE_STRING);
									newSubproductCode.setThirdLetterCode(GeneralConstants.ONE_VALUE_STRING);
									newSubproductCode.setFourthLetterCode(GeneralConstants.ONE_VALUE_STRING);
								}else{
									newSubproductCode.setFirstLetterCode(subproductCode.getFirstLetterCode());
									newSubproductCode.setSecondLetterCode(letter);
									newSubproductCode.setThirdLetterCode(GeneralConstants.ONE_VALUE_STRING);
									newSubproductCode.setFourthLetterCode(GeneralConstants.ONE_VALUE_STRING);
								}
							}else{
								newSubproductCode.setFirstLetterCode(subproductCode.getFirstLetterCode());
								newSubproductCode.setSecondLetterCode(subproductCode.getSecondLetterCode());
								newSubproductCode.setThirdLetterCode(letter);
								newSubproductCode.setFourthLetterCode(GeneralConstants.ONE_VALUE_STRING);
							}
						}else{
							newSubproductCode.setFirstLetterCode(subproductCode.getFirstLetterCode());
							newSubproductCode.setSecondLetterCode(subproductCode.getSecondLetterCode());
							newSubproductCode.setThirdLetterCode(subproductCode.getThirdLetterCode());
							newSubproductCode.setFourthLetterCode(letter);
						}	
					}else{
						letter = findLetter(subproductCode.getFourthLetterCode());
						if(Validations.validateIsNullOrEmpty(letter)){
							letter = findLetter(subproductCode.getThirdLetterCode());
							if(Validations.validateIsNullOrEmpty(letter)){
								letter = findLetter(subproductCode.getSecondLetterCode());
								if(Validations.validateIsNullOrEmpty(letter)){
									newSubproductCode.setFirstLetterCode(letter);
									newSubproductCode.setSecondLetterCode(GeneralConstants.ONE_VALUE_STRING);
									newSubproductCode.setThirdLetterCode(GeneralConstants.ONE_VALUE_STRING);
									newSubproductCode.setFourthLetterCode(GeneralConstants.ONE_VALUE_STRING);
								}else{
									newSubproductCode.setFirstLetterCode(subproductCode.getFirstLetterCode());
									newSubproductCode.setSecondLetterCode(letter);
									newSubproductCode.setThirdLetterCode(GeneralConstants.ONE_VALUE_STRING);
									newSubproductCode.setFourthLetterCode(GeneralConstants.ONE_VALUE_STRING);
								}
							}else{
								newSubproductCode.setFirstLetterCode(subproductCode.getFirstLetterCode());
								newSubproductCode.setSecondLetterCode(subproductCode.getSecondLetterCode());
								newSubproductCode.setThirdLetterCode(letter);
								newSubproductCode.setFourthLetterCode(GeneralConstants.ONE_VALUE_STRING);
							}
						}else{
							newSubproductCode.setFirstLetterCode(subproductCode.getFirstLetterCode());
							newSubproductCode.setSecondLetterCode(subproductCode.getSecondLetterCode());
							newSubproductCode.setThirdLetterCode(subproductCode.getThirdLetterCode());
							newSubproductCode.setFourthLetterCode(letter);
						}				
					}									
					additionalComponentServiceBean.create(newSubproductCode);
					String codification = null;
					codification  = newSubproductCode.getFirstLetterCode();
					codification += newSubproductCode.getSecondLetterCode();
					codification += newSubproductCode.getThirdLetterCode();
					codification += newSubproductCode.getFourthLetterCode();
					idSecurityCode += GeneralConstants.HYPHEN + codification;
				}				
			}				
		}
		return idSecurityCode;
	}
	
	private String findLetter(String letter){
		switch (letter) {
		case "1":
			return "2";
		case "2":
			return "3";
		case "3":
			return "4";
		case "4":
			return "5";
		case "5":
			return "6";
		case "6":
			return "7";
		case "7":
			return "8";
		case "8":
			return "9";
		case "9":
			return "A";
		case "A":
			return "B";
		case "B":
			return "C";
		case "C":
			return "D";
		case "D":
			return "E";
		case "E":
			return "F";
		case "F":
			return "G";
		case "G":
			return "H";
		case "H":
			return "I";
		case "I":
			return "J";
		case "J":
			return "K";
		case "K":
			return "L";
		case "L":
			return "M";
		case "M":
			return "N";
		case "N":
			return "O";
		case "O":
			return "P";
		case "P":
			return "Q";
		case "Q":
			return "R";
		case "R":
			return "S";
		case "S":
			return "T";
		case "T":
			return "U";
		case "U":
			return "V";
		case "V":
			return "W";
		case "W":
			return "X";
		case "X":
			return "Y";
		case "Y":
			return "Z";
		default:
			return null;
		}
	}
	
	private Security createSecurity(BigDecimal quantity, Security securitySource, ProgramInterestCoupon interestCoupon, 
									LoggerUser loggerUser, String idSecurityCodePkTemp, String subProductoCode,
									String physicIdSecurityCodePk) throws ServiceException{
		Security security = new Security();
		
		/**VARIABLE TO HANDLE ACTUAL NOMINAL VALUE*/
		BigDecimal newNominalValue;
		String suffixBCBcode= null;
		Integer indFci = BooleanType.NO.getCode();
		if(Validations.validateIsNotNullAndNotEmpty(interestCoupon)){
			/**CALCULATE NOMINAL VALUE FOR SECURITY-COUPON*/
			BigDecimal nominalValue = securitySource.getCurrentNominalValue();
			newNominalValue  		= additionalComponentServiceBean.generateNominalValueForCoupon(interestCoupon, nominalValue);
			
			/**New*/
			security.setExpirationDate(interestCoupon.getExperitationDate());
			security.setIndIsCoupon(BooleanType.YES.getCode());
			security.setIndIsDetached(BooleanType.NO.getCode());
			security.setIndSplitCoupon(BooleanType.NO.getCode());
			security.setIssuanceDate(securitySource.getIssuanceDate());
			security.setMaximumInvesment(BigDecimal.ZERO);
			security.setMinimumInvesment(newNominalValue);
			security.setSecurityClass(SecurityClassType.CUP.getCode());
			if (idIssuerBC.equalsIgnoreCase(securitySource.getIssuer().getIdIssuerPk()) ||
				idIssuerTGN.equalsIgnoreCase(securitySource.getIssuer().getIdIssuerPk())) {
				suffixBCBcode = GeneralConstants.EMPTY_STRING;
				if (interestCoupon.getCouponNumber().toString().length() == GeneralConstants.ONE_VALUE_INTEGER) {
					suffixBCBcode += GeneralConstants.ZERO_VALUE_STRING;
					suffixBCBcode += GeneralConstants.ZERO_VALUE_STRING;
				}else if (interestCoupon.getCouponNumber().toString().length() == GeneralConstants.TWO_VALUE_INTEGER) {
					suffixBCBcode += GeneralConstants.ZERO_VALUE_STRING;
				}
				suffixBCBcode += interestCoupon.getCouponNumber(); 
			}
			security = processSecurityTerm(security);
			security = processSecurityPayment(security);
		}else{
			newNominalValue = securitySource.getCurrentNominalValue();
			
			/**New*/
			security.setExpirationDate(securitySource.getExpirationDate());
			security.setIndIsCoupon(BooleanType.NO.getCode());
			security.setIndIsDetached(BooleanType.YES.getCode());
			security.setIndSplitCoupon(securitySource.getIndSplitCoupon());
			security.setIssuanceDate(securitySource.getIssuanceDate());
			security.setMaximumInvesment(securitySource.getMaximumInvesment());
			security.setMinimumInvesment(securitySource.getMinimumInvesment());
			security.setSecurityClass(securitySource.getSecurityClass());
			security.setSecurityMonthsTerm(securitySource.getSecurityMonthsTerm());
			security.setSecurityDaysTerm(securitySource.getSecurityDaysTerm());
			security.setSecurityTerm(securitySource.getSecurityTerm());
			
			/**Configuration for amortization information*/
			security.setAmortizationFactor(securitySource.getAmortizationFactor());
			security.setAmortizationOn(securitySource.getAmortizationOn());
			security.setAmortizationPeriodicity(securitySource.getAmortizationPeriodicity());
			security.setAmortizationPeriodicityDays(securitySource.getAmortizationPeriodicityDays());
			security.setAmortizationType(securitySource.getAmortizationType());
			security.setCapitalPaymentModality(securitySource.getCapitalPaymentModality());
			security.setPaymentFirstExpirationDate(security.getExpirationDate());
			
			/**Configuration for interest information*/
			security.setCalendarDays(securitySource.getCalendarDays());
			security.setCalendarMonth(securitySource.getCalendarMonth());
			security.setCalendarType(securitySource.getCalendarType());
			security.setCashNominal(securitySource.getCashNominal());
			security.setCouponFirstExpirationDate(securitySource.getCouponFirstExpirationDate());
			security.setInterestPaymentModality(securitySource.getInterestPaymentModality());
			security.setPeriodicity(securitySource.getPeriodicity());
			security.setPeriodicityDays(securitySource.getPeriodicityDays());
			security.setInterestRatePeriodicity(securitySource.getInterestRatePeriodicity());
			security.setInterestRate(securitySource.getInterestRate());
			security.setInterestType(securitySource.getInterestType());
			security.setMaximumRate(securitySource.getMaximumRate());
			security.setMinimumRate(securitySource.getMinimumRate());
			security.setNumberCoupons(securitySource.getNumberCoupons());
			security.setOperationType(securitySource.getOperationType());
			security.setRateType(securitySource.getRateType());
			security.setRateValue(securitySource.getRateValue());
			security.setSpread(securitySource.getSpread());
			security.setYield(securitySource.getYield());
			
			if(subProductoCode!=null){
				security.setIdSubproductRefFk(new Security(subProductoCode));
			}
			
			if (idIssuerBC.equalsIgnoreCase(securitySource.getIssuer().getIdIssuerPk()) ||
				idIssuerTGN.equalsIgnoreCase(securitySource.getIssuer().getIdIssuerPk())) {
				suffixBCBcode = GeneralConstants.EMPTY_STRING;
				suffixBCBcode += GeneralConstants.ZERO_VALUE_STRING + GeneralConstants.ZERO_VALUE_STRING + GeneralConstants.ZERO_VALUE_STRING;
			}
			
			indFci = securitySource.getSecurityInvestor().getIndCfi();
		}
		if (Validations.validateIsNotNull(suffixBCBcode)) {
			ParameterTable pTableSecClass=parameterServiceBean.getParameterTableById(securitySource.getSecurityClass());
			String strClassCode= GeneralConstants.BRACKET_OPEN + pTableSecClass.getText3() + GeneralConstants.BRACKET_CLOSE;
			security.setIdSecurityBcbCode(strClassCode + securitySource.getIdSecurityCodeOnly() + GeneralConstants.HYPHEN + suffixBCBcode);
		}
		security.setAmortizationAmount(BigDecimal.ZERO);
		security.setAmortizationPaymentSchedule(null);
		security.setCalendarMonth(security.getCalendarMonth());
		security.setCirculationBalance(quantity);
		security.setCirculationAmount(quantity.multiply(newNominalValue));//TODO
		security.setCorporativeProcessDays(securitySource.getCorporativeProcessDays());
		security.setCurrency(securitySource.getCurrency());
		security.setCurrentNominalValue(newNominalValue);
		security.setDepositRegistryDate(securitySource.getDepositRegistryDate());
		security.setDescription(securitySource.getDescription());
		security.setDesmaterializedBalance(BigDecimal.ZERO);
		security.setEconomicActivity(securitySource.getEconomicActivity());
		security.setFinancialIndex(securitySource.getFinancialIndex());
		security.setIdGroupFk(securitySource.getIdGroupFk());
		security.setIdSecurityCodeOnly(idSecurityCodePkTemp);
		security.setIndCapitalizableInterest(securitySource.getIndCapitalizableInterest());
		security.setIndConvertibleStock(securitySource.getIndConvertibleStock());
		security.setIndEarlyRedemption(securitySource.getIndEarlyRedemption());
		security.setIndexed(securitySource.getIndexed());
		security.setIndExtendedTerm(securitySource.getIndExtendedTerm());
		security.setIndHolderDetail(securitySource.getIndHolderDetail());
		security.setIndHasSplitSecurities(BooleanType.NO.getCode());
		security.setIndIsFractionable(securitySource.getIndIsFractionable());
		security.setIndIssuanceManagement(securitySource.getIndIssuanceManagement());
		security.setIndPaymentBenefit(securitySource.getIndPaymentBenefit());
		security.setIndReceivableCustody(securitySource.getIndReceivableCustody());
		security.setIndSecuritization(securitySource.getIndSecuritization());
		security.setIndSubordinated(securitySource.getIndSubordinated());
		security.setIndTaxExempt(securitySource.getIndTaxExempt());
		security.setIndTraderSecondary(securitySource.getIndTraderSecondary());
		security.setInitialNominalValue(newNominalValue);
		security.setInscriptionDate(securitySource.getInscriptionDate());
		security.setInstrumentType(securitySource.getInstrumentType());
		security.setInterestFactor(securitySource.getInterestFactor());
		security.setInterestPaymentSchedule(null);
		security.setIssuance(securitySource.getIssuance());
		security.setIssuanceCountry(securitySource.getIssuanceCountry());
		security.setIssuanceForm(securitySource.getIssuanceForm());
		security.setIssuer(securitySource.getIssuer());
		
		security.setMnemonic(securitySource.getMnemonic());
		security.setPaymentAgent(securitySource.getPaymentAgent());
		security.setPaymentRequirement(securitySource.getPaymentRequirement());
		security.setPhysicalBalance(BigDecimal.ZERO);
		security.setPlacedAmount(BigDecimal.ZERO);
		security.setPlacedBalance(quantity);
		security.setRegistryDate(CommonsUtilities.currentDateTime());
		security.setRegistryUser(loggerUser.getUserName());
		security.setRetirementDate(securitySource.getRetirementDate());
		security.setSecurityInvestor(securitySource.getSecurityInvestor());
		security.setSecurity(securitySource);
		security.setSecuritySerial(securitySource.getSecuritySerial());
		security.setSecuritySource(securitySource.getSecuritySource());
		security.setSecurityType(securitySource.getSecurityType());
		security.setSerialProgram(securitySource.getSerialProgram());
		security.setShareBalance(quantity);
		security.setShareCapital(quantity.multiply(security.getCurrentNominalValue()));
		security.setSpread(securitySource.getSpread());
		security.setStateSecurity(SecurityStateType.REGISTERED.getCode());
		security.setStockRegistryDays(securitySource.getStockRegistryDays());

		/**ATTRIBUTES TO HANDLE SECURITY CLASS FROM PARAMETER TABLE*/
		Integer securityClass = security.getSecurityClass();
		ParameterTable securityClassParam = (ParameterTable) additionalComponentServiceBean.getParameterValue(securityClass);
		/**ATTRIBUTES GENERATE WITH SECURITY OBJETC*/
		if(security.getAlternativeCode()!=null){
			security.setAlternativeCode(securitiesServiceBean.alternativeCodeGenerate(security));
		}
		if(physicIdSecurityCodePk != null) {
			security.setIdSecurityCodePk(physicIdSecurityCodePk);
		}else {
			security.setIdSecurityCodePk(securityClassParam.getText1().concat("-").concat(security.getIdSecurityCodeOnly()));
		}
		security.setIdIsinCode(securitiesServiceBean.securityIsinGenerate(security));
		security.setValuatorProcessClass(securitySource.getValuatorProcessClass());
		//CFI
		//security.setCfiCode(securitiesServiceBean.generateCFICode(security));
		//security.setFsinCode(securitiesServiceBean.getShortCfiCode(security));
		additionalComponentServiceBean.create(security);
		/**we set the list of Negotiation mechanism to this securities*/
		List<SecurityNegotiationMechanism> lstSecurityNegotiationMechanisms= securitiesServiceBean.getListSecurityNegotiationMechanism(securitySource.getIdSecurityCodePk());
		if (Validations.validateListIsNotNullAndNotEmpty(lstSecurityNegotiationMechanisms)) {
			List<SecurityNegotiationMechanism> lstNewSecurityNegotiationMechanisms = new ArrayList<SecurityNegotiationMechanism>();
			for (SecurityNegotiationMechanism sourceSecurityNegotiationMechanism: lstSecurityNegotiationMechanisms){
				SecurityNegotiationMechanism securityNegotiationMechanism= null;
				try {
					securityNegotiationMechanism = sourceSecurityNegotiationMechanism.clone();
				} catch (CloneNotSupportedException e) {
					e.printStackTrace();
				}
				securityNegotiationMechanism.setSecurity(security);
				securityNegotiationMechanism.setIdSecurityNegMechPk(null);
				additionalComponentServiceBean.create(securityNegotiationMechanism);
				lstNewSecurityNegotiationMechanisms.add(securityNegotiationMechanism);
			}
			security.setSecurityNegotiationMechanisms(lstNewSecurityNegotiationMechanisms);
		}
		
		//we set the list of foreign depositaries to this securities 
		List<SecurityForeignDepository> lstSecurityForeignDepositories= securitiesServiceBean.getListSecurityForeignDepositories(securitySource.getIdSecurityCodePk());
		if (Validations.validateListIsNotNullAndNotEmpty(lstSecurityForeignDepositories)) {
			for (SecurityForeignDepository sourceSecurityForeignDepository: lstSecurityForeignDepositories){
				SecurityForeignDepository securityForeignDepository= null;
				try {
					securityForeignDepository = sourceSecurityForeignDepository.clone();
				} catch (CloneNotSupportedException e) {
					e.printStackTrace();
				}
				securityForeignDepository.getId().setIdSecurityCodePk(security.getIdSecurityCodePk());
				additionalComponentServiceBean.create(securityForeignDepository);
			}
		}
		
		/**WE CREATE SECURITY_INVESTOR*/
		SecurityInvestor securitInvestorSource = securitySource.getSecurityInvestor();
		SecurityInvestor securitInvestorTarget = new SecurityInvestor();
		securitInvestorTarget.setIndForeign(securitInvestorSource.getIndForeign());
		securitInvestorTarget.setIndJuridical(securitInvestorSource.getIndJuridical());
		securitInvestorTarget.setIndCfi(indFci);
		securitInvestorTarget.setIndLocal(securitInvestorSource.getIndLocal());
		securitInvestorTarget.setIndNatural(securitInvestorSource.getIndNatural());
		securitInvestorTarget.setSecurityInvestorState(securitInvestorSource.getSecurityInvestorState());
		securitInvestorTarget.setSecurity(security);
		additionalComponentServiceBean.create(securitInvestorTarget);
		return security;
	}

	
	public Security processSecurityTerm(Security security){
		if(security.getIssuanceDate()!=null && security.getExpirationDate()!=null){
			Integer months=CommonsUtilities.getMonthsBetween(security.getIssuanceDate(), security.getExpirationDate());
			Integer days=CommonsUtilities.getDaysBetween(security.getIssuanceDate(), security.getExpirationDate());
			security.setSecurityMonthsTerm(months);
			security.setSecurityDaysTerm(days);
			
			if(security.getSecurityDaysTerm()<=SecurityTermType.SHORT_TERM_INS.getMaxTermDays()){
				security.setSecurityTerm( SecurityTermType.SHORT_TERM_INS.getCode() );
			}else if(security.getSecurityDaysTerm()<=SecurityTermType.MEDIUM_TERM_INS.getMaxTermDays()){
				security.setSecurityTerm( SecurityTermType.MEDIUM_TERM_INS.getCode() );
			}else {
				security.setSecurityTerm( SecurityTermType.LONG_TERM_INS.getCode() );
			}
		}
		return security;
	}
	
	public Security processSecurityPayment(Security security){
		/**Configuration for interest information*/
		security.setCalendarDays(CalendarDayType._360.getCode());
		security.setCalendarMonth(CalendarMonthType.ACTUAL.getCode());
		security.setCalendarType(CalendarType.COMMERCIAL.getCode());
		security.setCashNominal(InterestClassType.EFFECTIVE.getCode());
		security.setCouponFirstExpirationDate(security.getExpirationDate());
		security.setInterestPaymentModality(InterestPaymentModalityType.AT_MATURITY.getCode());
		security.setPeriodicity(null);
		security.setPeriodicityDays(null);
		security.setInterestRatePeriodicity(null);
		security.setInterestRate(BigDecimal.ZERO);
		security.setInterestType(InterestType.CERO.getCode());
		security.setMaximumRate(null);
		security.setMinimumRate(null);
		security.setNumberCoupons(BigDecimal.ZERO.intValue());
		security.setOperationType(null);
		security.setRateType(null);
		security.setRateValue(null);
		security.setSpread(null);
		security.setYield(null);
		
		/**Configuration for amortization information*/
		security.setAmortizationFactor(GeneralConstants.HUNDRED_VALUE_BIGDECIMAL);
		security.setAmortizationOn(AmortizationOnType.CAPITAL.getCode());
		security.setAmortizationPeriodicity(null);
		security.setAmortizationPeriodicityDays(null);
		security.setAmortizationType(null);
		security.setCapitalPaymentModality(CapitalPaymentModalityType.AT_MATURITY.getCode());
		security.setPaymentFirstExpirationDate(security.getExpirationDate());
		
		return security;
	}
	
	private AmortizationPaymentSchedule createAmortizationPaymentSchedule(Security security, ProgramInterestCoupon sourceProgramInterestCoupon, LoggerUser loggerUser) 
			throws ServiceException{
		AmortizationPaymentSchedule amortizationPaymentSchedule = new AmortizationPaymentSchedule();
		amortizationPaymentSchedule.setAmortizationFactor(GeneralConstants.HUNDRED_VALUE_BIGDECIMAL); //it is total amortization
		amortizationPaymentSchedule.setAmortizationType(security.getAmortizationType());
		amortizationPaymentSchedule.setRegistryDays(sourceProgramInterestCoupon.getInterestPaymentCronogram().getRegistryDays());
		amortizationPaymentSchedule.setCalendarType(security.getCalendarType());
		amortizationPaymentSchedule.setCorporativeDays(security.getCorporativeProcessDays());
		amortizationPaymentSchedule.setPeriodicity(security.getPeriodicity());
		amortizationPaymentSchedule.setPeriodicityDays(sourceProgramInterestCoupon.getInterestPaymentCronogram().getPeriodicityDays());
		amortizationPaymentSchedule.setScheduleState(PaymentScheduleStateType.REGTERED.getCode());
		amortizationPaymentSchedule.setPaymentModality(CapitalPaymentModalityType.AT_MATURITY.getCode());
		amortizationPaymentSchedule.setSecurity(security);
		additionalComponentServiceBean.create(amortizationPaymentSchedule);
		
		List<ProgramAmortizationCoupon> lstProgramAmortizationCoupons= new ArrayList<ProgramAmortizationCoupon>();
		ProgramAmortizationCoupon programAmortizationCoupon = new ProgramAmortizationCoupon();
		programAmortizationCoupon.setAmortizationAmount(BigDecimal.ZERO);
		programAmortizationCoupon.setAmortizationFactor(amortizationPaymentSchedule.getAmortizationFactor());
		programAmortizationCoupon.setAmortizationPaymentSchedule(amortizationPaymentSchedule);
		programAmortizationCoupon.setBeginingDate(sourceProgramInterestCoupon.getBeginingDate());
		programAmortizationCoupon.setCorporativeDate(sourceProgramInterestCoupon.getCorporativeDate());	
		programAmortizationCoupon.setCouponNumber(sourceProgramInterestCoupon.getCouponNumber());
		programAmortizationCoupon.setCurrency(security.getCurrency());
		programAmortizationCoupon.setCutoffDate(sourceProgramInterestCoupon.getCutoffDate());
		programAmortizationCoupon.setExpirationDate(sourceProgramInterestCoupon.getExperitationDate());
		programAmortizationCoupon.setIndRounding(sourceProgramInterestCoupon.getIndRounding());
		programAmortizationCoupon.setPaymentDate(sourceProgramInterestCoupon.getPaymentDate());
		programAmortizationCoupon.setRegisterDate(sourceProgramInterestCoupon.getCutoffDate());
		programAmortizationCoupon.setRegistryDate(CommonsUtilities.currentDateTime());
		programAmortizationCoupon.setRegistryUser(loggerUser.getUserName());
		programAmortizationCoupon.setStateProgramAmortization(ProgramScheduleStateType.PENDING.getCode());
		BigDecimal nominalValue = security.getInitialNominalValue();
		Double	amortFactor = amortizationPaymentSchedule.getAmortizationFactor().doubleValue();
		programAmortizationCoupon.setCouponAmount(CommonsUtilities.getPercentageOf(nominalValue, amortFactor));
		additionalComponentServiceBean.create(programAmortizationCoupon);
		lstProgramAmortizationCoupons.add(programAmortizationCoupon);
		
		amortizationPaymentSchedule.setProgramAmortizationCoupons(lstProgramAmortizationCoupons);
		return amortizationPaymentSchedule;
	}
	
	private ProgramAmortizationCoupon createAmortizationCoupon(Security security, AmortizationPaymentSchedule amortizationPaymentSchedule,
											ProgramAmortizationCoupon sourceProgramAmortizationCoupon, 
											LoggerUser loggerUser) throws ServiceException{		
		ProgramAmortizationCoupon programAmortizationCoupon = new ProgramAmortizationCoupon();
		programAmortizationCoupon.setAmortizationPaymentSchedule(amortizationPaymentSchedule);
		programAmortizationCoupon.setAmortizationAmount(sourceProgramAmortizationCoupon.getAmortizationAmount());
		programAmortizationCoupon.setAmortizationFactor(sourceProgramAmortizationCoupon.getAmortizationFactor());		
		programAmortizationCoupon.setBeginingDate(sourceProgramAmortizationCoupon.getBeginingDate());
		programAmortizationCoupon.setCorporativeDate(sourceProgramAmortizationCoupon.getCorporativeDate());
		programAmortizationCoupon.setCouponNumber(sourceProgramAmortizationCoupon.getCouponNumber());
		programAmortizationCoupon.setCurrency(security.getCurrency());
		programAmortizationCoupon.setCutoffDate(sourceProgramAmortizationCoupon.getCutoffDate());
		programAmortizationCoupon.setExpirationDate(sourceProgramAmortizationCoupon.getExpirationDate());
		programAmortizationCoupon.setIndRounding(sourceProgramAmortizationCoupon.getIndRounding());
		programAmortizationCoupon.setPaymentDate(sourceProgramAmortizationCoupon.getPaymentDate());
		programAmortizationCoupon.setRegisterDate(sourceProgramAmortizationCoupon.getCutoffDate());
		programAmortizationCoupon.setRegistryDate(CommonsUtilities.currentDateTime());
		programAmortizationCoupon.setRegistryUser(loggerUser.getUserName());
		programAmortizationCoupon.setStateProgramAmortization(ProgramScheduleStateType.PENDING.getCode());
		BigDecimal nominalValue = security.getInitialNominalValue();
		Double amortizationAmount = sourceProgramAmortizationCoupon.getAmortizationFactor().doubleValue();
		programAmortizationCoupon.setCouponAmount(CommonsUtilities.getPercentageOf(nominalValue,amortizationAmount));
		return additionalComponentServiceBean.create(programAmortizationCoupon);
	}
	
	private ProgramInterestCoupon createInterestCoupon(Security security, InterestPaymentSchedule interestPaymentSchedule,
													   ProgramInterestCoupon interestCouponSource, LoggerUser loggerUser) throws ServiceException{
		ProgramInterestCoupon programInterestCoupon = new ProgramInterestCoupon();
		programInterestCoupon.setInterestPaymentCronogram(interestPaymentSchedule);
		programInterestCoupon.setInterestFactor(interestPaymentSchedule.getInterestFactor());		
		programInterestCoupon.setBeginingDate(interestCouponSource.getBeginingDate());
		programInterestCoupon.setCorporativeDate(interestCouponSource.getCorporativeDate());
		programInterestCoupon.setCouponNumber(interestCouponSource.getCouponNumber());
		programInterestCoupon.setCurrency(security.getCurrency());
		programInterestCoupon.setCutoffDate(interestCouponSource.getCutoffDate());
		programInterestCoupon.setExperitationDate(interestCouponSource.getExperitationDate());
		programInterestCoupon.setIndRounding(interestCouponSource.getIndRounding());
		programInterestCoupon.setPaymentDate(interestCouponSource.getPaymentDate());
		programInterestCoupon.setPaymentDays(CommonsUtilities.getDaysBetween(interestCouponSource.getBeginingDate(),interestCouponSource.getExperitationDate()));
		programInterestCoupon.setRegistryDate(CommonsUtilities.currentDateTime());
		programInterestCoupon.setStateProgramInterest(ProgramScheduleStateType.PENDING.getCode());
		programInterestCoupon.setInterestRate(interestCouponSource.getInterestRate());
		programInterestCoupon.setCouponAmount(getCouponInterestAmount(programInterestCoupon,security));
		return additionalComponentServiceBean.create(programInterestCoupon);
	}
	
	private List<HolderAccountBalanceTO> getHolderAccountBalanceTOs(SplitCouponRequest splitCouponRequest, 
													BigDecimal quantity, List<MarketFactAccountTO> lstMarketFactTO) throws ServiceException{
		List<HolderAccountBalanceTO> accountBalanceTOs = new ArrayList<HolderAccountBalanceTO>();
		HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
		holderAccountBalanceTO.setIdHolderAccount(splitCouponRequest.getHolderAccount().getIdHolderAccountPk());
		holderAccountBalanceTO.setIdParticipant(splitCouponRequest.getParticipant().getIdParticipantPk());
		holderAccountBalanceTO.setIdSecurityCode(splitCouponRequest.getSecurity().getIdSecurityCodePk());
		holderAccountBalanceTO.setStockQuantity(quantity);
		if(Validations.validateListIsNotNullAndNotEmpty(lstMarketFactTO))
			holderAccountBalanceTO.setLstMarketFactAccounts(lstMarketFactTO);
		accountBalanceTOs.add(holderAccountBalanceTO);
		return accountBalanceTOs;
	}
	
	private List<HolderAccountBalanceTO> getHolderAccountBalanceTOs(SplitCouponRequest splitCouponRequest, 
												BigDecimal quantity, String idSecurityCodePk, List<MarketFactAccountTO> lstMarketFactTO) throws ServiceException{
		List<HolderAccountBalanceTO> accountBalanceTOs = new ArrayList<HolderAccountBalanceTO>();
		HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
		holderAccountBalanceTO.setIdHolderAccount(splitCouponRequest.getHolderAccount().getIdHolderAccountPk());
		holderAccountBalanceTO.setIdParticipant(splitCouponRequest.getParticipant().getIdParticipantPk());
		holderAccountBalanceTO.setIdSecurityCode(idSecurityCodePk);
		holderAccountBalanceTO.setStockQuantity(quantity);
		if(Validations.validateListIsNotNullAndNotEmpty(lstMarketFactTO)){
			holderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
			for(MarketFactAccountTO marketFactAccount: lstMarketFactTO){
				try {
					holderAccountBalanceTO.getLstMarketFactAccounts().add(marketFactAccount.clone());
				} catch (CloneNotSupportedException e) {
					e.printStackTrace();
				}
			}
		}
		accountBalanceTOs.add(holderAccountBalanceTO);
		return accountBalanceTOs;
	}
	
	private AccountsComponentTO getAccountsComponentTO(Long idCustodyOperation, Long businessProcessType, 
								List<HolderAccountBalanceTO> accountBalanceTOs, List<HolderAccountBalanceTO> targetAccountBalanceTOs, 
								Long operationType, SplitCouponOperationTO splitCouponOperationTO){		
		AccountsComponentTO objAccountComponent = new AccountsComponentTO();		
		objAccountComponent.setIdBusinessProcess(businessProcessType);
		objAccountComponent.setIdCustodyOperation(idCustodyOperation);
		objAccountComponent.setLstSourceAccounts(accountBalanceTOs);
		if(Validations.validateListIsNotNullAndNotEmpty(targetAccountBalanceTOs))
			objAccountComponent.setLstTargetAccounts(targetAccountBalanceTOs);
		objAccountComponent.setIdOperationType(operationType);
		objAccountComponent.setIndMarketFact(splitCouponOperationTO.getIndMarketFact());
		return objAccountComponent;
	}
	
	private SecuritiesOperation createSecOperation(Security security, CustodyOperation custodyOperation, BigDecimal quantity) {
		SecuritiesOperation securitiesOperation = new SecuritiesOperation();
		securitiesOperation.setSecurities(security);
		securitiesOperation.setCustodyOperation(custodyOperation);
		securitiesOperation.setOperationDate(CommonsUtilities.currentDate());
		securitiesOperation.setStockQuantity(quantity);
		securitiesOperation.setCashAmount(quantity.multiply(security.getCurrentNominalValue()));
		securitiesOperation.setOperationType(ParameterOperationType.SEC_SPLIT_SECURITIES_DETACHMENT.getCode());	
		return additionalComponentServiceBean.create(securitiesOperation);
	}
	
	private SecuritiesComponentTO getSecuritiesComponentTO(SecuritiesOperation objSecuritiesOperation, Long businessType, boolean blTarget){
		SecuritiesComponentTO securitiesComponentTO = new SecuritiesComponentTO();
		securitiesComponentTO.setIdBusinessProcess(businessType);
		securitiesComponentTO.setIdOperationType(objSecuritiesOperation.getOperationType());
		securitiesComponentTO.setIdSecuritiesOperation(objSecuritiesOperation.getIdSecuritiesOperationPk());
		securitiesComponentTO.setIdSecurityCode(objSecuritiesOperation.getSecurities().getIdSecurityCodePk());
		securitiesComponentTO.setDematerializedBalance(objSecuritiesOperation.getStockQuantity());
		securitiesComponentTO.setPhysicalBalance(objSecuritiesOperation.getStockQuantity());
		if(blTarget)
			securitiesComponentTO.setIndSourceTarget(ComponentConstant.TARGET);
		return securitiesComponentTO;
	}
	
	/***
	 * Method to calculate CouponAmount in each interest coupon
	 * @param security
	 */
	
	public BigDecimal getCouponInterestAmount(ProgramInterestCoupon intCoupon, Security security) throws ServiceException{
		List<ProgramAmortizationCoupon> programInterestList = security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons();
		Date expInterestDate = intCoupon.getExperitationDate();
		BigDecimal afectedAmount = BigDecimal.ZERO;
		BigDecimal interestRate = intCoupon.getInterestRate();
		
		for(ProgramAmortizationCoupon amortCoupon: programInterestList){
			Date expAmortizationDate = amortCoupon.getExpirationDate();
			if(expAmortizationDate.equals(expInterestDate) || expAmortizationDate.after(expInterestDate)){
				break;
			}
			afectedAmount = afectedAmount.add(amortCoupon.getCouponAmount());
		}
		BigDecimal nominalValCoup = security.getCurrentNominalValue().subtract(CommonsUtilities.getPercentageOf(afectedAmount,interestRate.doubleValue()));
		return CommonsUtilities.getPercentageOf(nominalValCoup,interestRate.doubleValue());
	}
	
	public String getPhysicalCouponPendingSecurityCode(String idSecurityCodePk, List<PhysicalCertificate> lstCertificate, ProgramInterestCoupon programInterestCoupon){
		String idTempSecurityCodePk = idSecurityCodePk.concat("-");
		for(PhysicalCertificate cert : lstCertificate) {
			if(Validations.validateIsNotNull(cert.getIdPendingSecurityCodePk()) 
					&& programInterestCoupon.getCouponNumber().equals(Integer.valueOf(cert.getIdPendingSecurityCodePk().substring(idTempSecurityCodePk.length())))) {
				return cert.getIdPendingSecurityCodePk();
			}
		} 
		
		return null;
	}
}