package com.pradera.idepositary.businessintegration.component.webservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.transaction.TransactionSynchronizationRegistry;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.DepositarySetupBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.common.validation.to.ValidationOperationType;
import com.pradera.integration.common.validation.to.ValidationProcessTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.corporatives.service.CorporateProcessRemoteService;
import com.pradera.integration.component.corporatives.to.CorporateInformationTO;
import com.pradera.integration.component.corporatives.to.InterestCouponPaymentTO;
import com.pradera.integration.component.custody.to.ReverseRegisterTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.remote.ThreadRemoteContextHolder;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionStateType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CorporateProcessResource.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/09/2015
 */
@Path("/CorporateProcessResource")
@Stateless
public class CorporateProcessResource implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The corporate process remote service. */
	@Inject
	private Instance<CorporateProcessRemoteService> corporateProcessRemoteService;
	
	/** The interface component service bean. */
	@Inject
    private Instance<InterfaceComponentService> interfaceComponentServiceBean;
	
	/** The parameter service bean. */
	@EJB
	private ParameterServiceBean parameterServiceBean;
	
	/** The registry. */
	@Resource
	private TransactionSynchronizationRegistry registry;
	
	/** The depositary setup bean. */
	@Inject
	DepositarySetupBean depositarySetupBean;
	
	/** The server configuration. */
	@Inject
	@InjectableResource(location = "ServerConfiguration.properties")
	private Properties serverConfiguration;
	
	/** The audits user. */
	String strAuditsUser;
	
	/** The audits IP address. */
	String strIpAddress;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		strAuditsUser = serverConfiguration.getProperty("webservice.audits.users.key");
		strIpAddress = serverConfiguration.getProperty("webservice.audits.ip");
	}
	
	/**
	 * Pay interest coupon.
	 *
	 * @param xmlParam the xml param
	 * @return the response
	 */
	@Performance
	@POST
	@Path("/pcu")
	@Produces(MediaType.APPLICATION_XML)
	public Response payInterestCoupon(String xmlParam)
	{
		log.info(":::: INICIO SERVICIO PCU ::::");		
		ProcessFileTO objProcessFileTO= null;
		ValidationProcessTO objValidationProcessTO = null;
		CorporateInformationTO corporateInformationTO= null;
		InterestCouponPaymentTO interestCouponPaymentTO= null;
		List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
		Integer processState= ExternalInterfaceReceptionStateType.CORRECT.getCode();
		StringBuilder xmlResponse= new StringBuilder();
		
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		try {
			/*
			 * STEP 01:
			 * Parse the from XML file to TO object   
			 */
			log.info(":::: INICIO VALIDACION ARCHIVO ::::");
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(
															ComponentConstant.INTERFACE_INTEREST_PAYMENT_PCU, 
															xmlParam, ComponentConstant.PCU_DPF_NAME, Boolean.FALSE);
			log.info(":::: FIN VALIDACION ARCHIVO ::::");
			
			/*
			 * STEP 02:
			 * Call the Remote service from Corporative Process
			 */
			log.info(":::: INICIO LLAMADA REMOTA ::::");
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				Long idInterfaceProcess= interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(objProcessFileTO, 
																				ExternalInterfaceReceptionType.WEBSERVICE.getCode(), objLoggerUser);
				objValidationProcessTO = objProcessFileTO.getValidationProcessTO();
				for(ValidationOperationType objValidationOperationType : objValidationProcessTO.getLstOperations()){
					corporateInformationTO= (CorporateInformationTO) objValidationOperationType;
					corporateInformationTO.setIdInterfaceProcess(idInterfaceProcess);
					lstRecordValidationTypes= corporateProcessRemoteService.get().payInterestCoupon(corporateInformationTO);
					objValidationProcessTO.setLstValidations(lstRecordValidationTypes);
				}
			}
			log.info(":::: FIN LLAMADA REMOTA ::::");
		} catch (Exception ex) {
			ex.printStackTrace();
			objValidationProcessTO= parameterServiceBean.generateErrorValidationProcess(corporateInformationTO, ex);
			List<CorporateInformationTO> lstCorporateInformationTOs= new ArrayList<CorporateInformationTO>();
			lstCorporateInformationTOs.add(corporateInformationTO);
			corporateInformationTO.setErrorCode(objValidationProcessTO.getLstValidations().get(0).getErrorCode());
			corporateInformationTO.setErrorDescription(objValidationProcessTO.getLstValidations().get(0).getErrorDescription());
			interestCouponPaymentTO= new InterestCouponPaymentTO();
			interestCouponPaymentTO.setLstCorporateInformationTO(lstCorporateInformationTOs);
			objValidationProcessTO.getLstValidations().get(0).setOperationRef(interestCouponPaymentTO);
			processState= ExternalInterfaceReceptionStateType.FAILED.getCode();
		} finally {
			/*
			 * STEP 03:
			 * Generate the response XML file
			 */
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			if (objValidationProcessTO!=null && Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {
				xmlResponse.append(interfaceComponentServiceBean.get().saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
															objValidationProcessTO.getLstValidations(), objProcessFileTO.getStreamResponseFileDir(), 
															ComponentConstant.DPF_TAG, ComponentConstant.OPERATIONS_RECORD_TAG, processState, objLoggerUser));
			} else {
				xmlResponse.append("NO HAY DATOS");
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");
		}
		log.info(":::: FIN SERVICIO PCU ::::");
		return Response.status(201).entity(xmlResponse.toString()).build();
	}
	
	/**
	 * Reverse payment interest coupon.
	 *
	 * @param xmlParam the xml param
	 * @return the response
	 */
	@Performance
	@POST
	@Path("/rpc")
	@Produces(MediaType.APPLICATION_XML)
	public Response reversePaymentInterestCoupon(String xmlParam)
	{
		log.info(":::: INICIO SERVICIO RPC ::::");		
		ProcessFileTO objProcessFileTO= null;
		ValidationProcessTO objValidationProcessTO = null;
		ReverseRegisterTO reverseRegisterTO= null;
		List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
		Integer processState= ExternalInterfaceReceptionStateType.CORRECT.getCode();
		StringBuilder xmlResponse= new StringBuilder();
		
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		try {
			/*
			 * STEP 01:
			 * Parse the from XML file to TO object   
			 */
			log.info(":::: INICIO VALIDACION ARCHIVO ::::");
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(
															ComponentConstant.INTERFACE_REVERSE_INTEREST_PAYMENT_RPC, 
															xmlParam, ComponentConstant.RPC_DPF_NAME, Boolean.FALSE);
			log.info(":::: FIN VALIDACION ARCHIVO ::::");
			
			/*
			 * STEP 02:
			 * Call the Remote service from Corporative Process
			 */
			log.info(":::: INICIO LLAMADA REMOTA ::::");
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				Long idInterfaceProcess= interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(objProcessFileTO, 
																				ExternalInterfaceReceptionType.WEBSERVICE.getCode(), objLoggerUser);
				objValidationProcessTO = objProcessFileTO.getValidationProcessTO();
				for(ValidationOperationType objValidationOperationType : objValidationProcessTO.getLstOperations()){
					reverseRegisterTO= (ReverseRegisterTO) objValidationOperationType;
					reverseRegisterTO.setIdInterfaceProcess(idInterfaceProcess);
					lstRecordValidationTypes= corporateProcessRemoteService.get().reversePaymentInterestCoupon(reverseRegisterTO);
					objValidationProcessTO.setLstValidations(lstRecordValidationTypes);
				}
			}
			log.info(":::: FIN LLAMADA REMOTA ::::");
		} catch (Exception ex) {
			ex.printStackTrace();
			objValidationProcessTO= parameterServiceBean.generateErrorValidationProcess(reverseRegisterTO, ex);
			processState= ExternalInterfaceReceptionStateType.FAILED.getCode();
		} finally {
			/*
			 * STEP 03:
			 * Generate the response XML file
			 */
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			if (objValidationProcessTO !=null && Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {
				xmlResponse.append(interfaceComponentServiceBean.get().saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
														objValidationProcessTO.getLstValidations(), objProcessFileTO.getStreamResponseFileDir(), 
														ComponentConstant.DPF_TAG, ComponentConstant.OPERATION_RECORD_TAG, processState, objLoggerUser));
			} else {
				xmlResponse.append("NO HAY DATOS");
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");
		}
		log.info(":::: FIN SERVICIO RPC ::::");
		return Response.status(201).entity(xmlResponse.toString()).build();
	}
}
