package com.pradera.idepositary.businessintegration.component.service;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.RemoteLoggerUserInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GenericPropertiesConstants;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.OperationInterfaceTO;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.generalparameters.to.ServiceStatusQueryTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.generalparameter.externalinterface.ExtensionFileType;
import com.pradera.model.generalparameter.externalinterface.ExternalInterface;
import com.pradera.model.generalparameter.externalinterface.InterfaceProcess;
import com.pradera.model.generalparameter.externalinterface.InterfaceTransaction;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionStateType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceStateType;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceExtensionFileType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.TradeOperation;

@Stateless
public class InterfaceComponentServiceBean extends CrudDaoServiceBean implements InterfaceComponentService {

	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	@EJB
	AdditionalComponentServiceBean additionalComponentServiceBean;
	
	@EJB
	ParticipantServiceBean participantServiceBean;
	
	@Override
	@Interceptors(RemoteLoggerUserInterceptor.class)
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Long saveExternalInterfaceReceptionTx(ProcessFileTO processFileTO, Integer processType, LoggerUser loggerUser) { 
		InterfaceProcess interfaceReception = new InterfaceProcess();
		interfaceReception.setFileName(processFileTO.getFileName());
		interfaceReception.setProcessDate(CommonsUtilities.currentDateTime());
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("interfaceName", processFileTO.getInterfaceName());
		ExternalInterface externalInterfaceObj = findObjectByNamedQuery(ExternalInterface.INTERFACE_GET_BY_NAME,params);
		
		interfaceReception.setIdExternalInterfaceFk(externalInterfaceObj);
		interfaceReception.setIdExtensionFileTypeFk(new ExtensionFileType(InterfaceExtensionFileType.XML.getCode().longValue()));
		interfaceReception.setFileUpload(processFileTO.getProcessFile());
		interfaceReception.setProcessState(ExternalInterfaceReceptionStateType.PENDING.getCode());
		interfaceReception.setProcessType(processType);
		interfaceReception.setRegistryDate(CommonsUtilities.currentDateTime());
		interfaceReception.setRegistryUser(loggerUser.getUserName());
		interfaceReception.setAudit(loggerUser);
		interfaceReception.setLastModifyApp(1);
		additionalComponentServiceBean.create(interfaceReception);
		processFileTO.setIdProcessFilePk(interfaceReception.getIdInterfaceProcessPk());
		return processFileTO.getIdProcessFilePk();
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public String saveExternalInterfaceResponseFileTx(Long idInterfaceReception, List<RecordValidationType> lstOperations, 
									String mappingResponse, String rootTag, String recordTag, Integer processState, LoggerUser loggerUser) {
		
		byte[] responseFile = CommonsUtilities.generateInterfaceResponseFile(lstOperations,mappingResponse, rootTag, recordTag);
		
		additionalComponentServiceBean.updateExternalInterfaceState(idInterfaceReception, processState, responseFile, null, loggerUser);
		
		try {
			return new String(responseFile, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	
	@Override
	public String saveExternalInterfaceUploadFileTx(Long idInterfaceProcess, List<RecordValidationType> lstOperations, 
									String mappingResponse, String rootTag, String recordTag, Integer processState, LoggerUser loggerUser) {
		
		byte[] uploadFile = CommonsUtilities.generateInterfaceResponseFile(lstOperations,mappingResponse, rootTag, recordTag);
		
		additionalComponentServiceBean.updateExternalInterfaceState(idInterfaceProcess, processState, null, uploadFile, loggerUser);
		
		try {
			return new String(uploadFile, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(RemoteLoggerUserInterceptor.class)
	public void updateExternalInterfaceStateTx(Long idInterfaceReception, Integer processState) {
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER.name());
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		additionalComponentServiceBean.updateExternalInterfaceState(idInterfaceReception, processState, null, null, loggerUser);
		
	}
	
	@Override
	public Long saveInterfaceTransaction(OperationInterfaceTO operationInterfaceTO, Integer transactionState, LoggerUser loggerUser){
		
		InterfaceTransaction interfaceTransaction = new InterfaceTransaction();
		if(operationInterfaceTO.getIdSecurityCode()!=null){
			interfaceTransaction.setSecurity(new Security(operationInterfaceTO.getIdSecurityCode()));
		}
		if(operationInterfaceTO.getIdParticipant()!=null){
			interfaceTransaction.setParticipant(new Participant(operationInterfaceTO.getIdParticipant()));
		}
		if(operationInterfaceTO.getIdCustodyOperation()!=null){
			interfaceTransaction.setCustodyOperation(new CustodyOperation(operationInterfaceTO.getIdCustodyOperation()));
		}
		if (Validations.validateIsNotNull(operationInterfaceTO.getIdCorporativeOperation())) {
			interfaceTransaction.setCorporativeOperation(new CorporativeOperation(operationInterfaceTO.getIdCorporativeOperation()));
		}
		if (Validations.validateIsNotNull(operationInterfaceTO.getIdTradeOperation())) {
			interfaceTransaction.setTradeOperation(new TradeOperation(operationInterfaceTO.getIdTradeOperation()));
		}
//		if (Validations.validateIsNotNull(operationInterfaceTO.getIdFundsOperation())) {
//			interfaceTransaction.setFundsOperation(new FundsOperation(operationInterfaceTO.getIdFundsOperation()));
//		}
		interfaceTransaction.setObligationNumber(operationInterfaceTO.getObligationNumber());
		interfaceTransaction.setOperationNumber(operationInterfaceTO.getOperationNumber());
		interfaceTransaction.setInterfaceProcess(new InterfaceProcess(operationInterfaceTO.getIdInterfaceProcess()));
		interfaceTransaction.setTransactionState(transactionState);
		
		create(interfaceTransaction, loggerUser);
		
		return interfaceTransaction.getIdInterfaceTransactionPk();
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Long saveInterfaceTransactionTx(OperationInterfaceTO operationInterfaceTO, Integer transactionState, LoggerUser loggerUser){
		
		return saveInterfaceTransaction(operationInterfaceTO, transactionState, loggerUser);
	}

	@Override
	public void updateInterfaceProcessResponseFile(Long idInterfaceProcess, Integer processState, byte[] xmlFile, LoggerUser loggerUser) {
		additionalComponentServiceBean.updateExternalInterfaceState(idInterfaceProcess, processState, xmlFile, null, loggerUser);
	}
	
	@Override
	public void updateInterfaceProcessUploadFile(Long idInterfaceProcess,
			Integer processState, byte[] xmlFile, LoggerUser loggerUser) {
		additionalComponentServiceBean.updateExternalInterfaceState(idInterfaceProcess, processState, null, xmlFile, loggerUser);
	}
	
	@Override
	public List<RecordValidationType> getServiceStatus(ServiceStatusQueryTO objServiceStatusQueryTO, LoggerUser loggerUser) throws ServiceException {
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        List<RecordValidationType> lstRecordValidationTypes = new ArrayList<RecordValidationType>();
        try {
        	lstRecordValidationTypes = validateServiceStatus(objServiceStatusQueryTO);
        	if(Validations.validateListIsNullOrEmpty(lstRecordValidationTypes)){        		
        		RecordValidationType objRecordValidationType = CommonsUtilities.populateRecordCodeValidation(objServiceStatusQueryTO, 
						GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
				lstRecordValidationTypes.add(objRecordValidationType);				
        	}
        } catch (Exception e) {
        	e.printStackTrace();        	
        	throw e;
        }
		return lstRecordValidationTypes;
	}
	
	public List<RecordValidationType> validateServiceStatus(ServiceStatusQueryTO objServiceStatusQueryTO)  throws ServiceException {
		List<RecordValidationType> lstRecordValidationType= new ArrayList<RecordValidationType>();
		
		// OPERATION CODE
		
		if (!ComponentConstant.INTERFACE_QUERY_SERVICE_STATUS_DPF.equalsIgnoreCase(objServiceStatusQueryTO.getOperationCode())) {
			RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
					objServiceStatusQueryTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION, null);
			lstRecordValidationType.add(recordValidationType);
			return lstRecordValidationType;
		}
		
		// PARTICIPANT
		
		Participant objParticipant = new Participant();
		objParticipant.setMnemonic(objServiceStatusQueryTO.getEntityNemonic());
		objParticipant = participantServiceBean.findParticipantByFiltersServiceBean(objParticipant);
		if (Validations.validateIsNull(objParticipant)) {
			RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
					objServiceStatusQueryTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_ENTITY_CODE, null);
			lstRecordValidationType.add(recordValidationType);
			return lstRecordValidationType;
		} else {
			objServiceStatusQueryTO.setIdParticipant(objParticipant.getIdParticipantPk());
		}
		
		// VALIDATE OPERATION DATE IS TODAY
		
		Date operationDate = CommonsUtilities.truncateDateTime(objServiceStatusQueryTO.getOperationDate());
		Date date = CommonsUtilities.currentDate();
		if(operationDate.compareTo(date) != 0){
			RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(objServiceStatusQueryTO,
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_DATE, null);
			lstRecordValidationType.add(recordValidationType);			
			return lstRecordValidationType;
		}
		
		// VALIDATE STATUS 
		
		List<ExternalInterface> lstExternalInterface = additionalComponentServiceBean.getListExternalInterface();		
		if(Validations.validateListIsNotNullAndNotEmpty(lstExternalInterface)){
			for(ExternalInterface objExternalInterface : lstExternalInterface){
				if(!ExternalInterfaceStateType.CONFIRMED.getCode().equals(objExternalInterface.getExternalInterfaceState())){					
					RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(objServiceStatusQueryTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_QUERY_SERVICE_STATUS_WEBSERVICES, null,
							new Object[]{ objExternalInterface.getDescription() });
					
					lstRecordValidationType.add(recordValidationType);
				}
			}
		}
		
		return lstRecordValidationType;
	}

//	@Override
	public void updateExternalInterfaceState(Long idInterfaceReception, Integer processState,byte[] xmlFile, LoggerUser loggerUser) {
		additionalComponentServiceBean.updateExternalInterfaceState(idInterfaceReception, processState, null, xmlFile, loggerUser);
	}
	
	@Override
	public void updateFundsInterfaceTransaction(Long idInterfaceTransaction, Long idFundsOperation) {
		additionalComponentServiceBean.updateFundsInterfaceTransaction(idInterfaceTransaction, idFundsOperation);
	}
	
	@Override	
	public String saveExternalInterfaceResponseFileTx(List<RecordValidationType> lstOperations, 
									String mappingResponse, String rootTag, String recordTag) {		
		byte [] responseFile = CommonsUtilities.generateInterfaceResponseFile(lstOperations, mappingResponse, rootTag, recordTag);						
		try {
			return new String(responseFile, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}		
		return null;		
	}
	
}
