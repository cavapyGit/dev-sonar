package com.pradera.idepositary.businessintegration.component.service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BusinessComponentControl.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-jul-2015
 */
@Singleton
@Startup
public class BusinessComponentControl {
	
	/** The timer service. */
	@Resource
	TimerService timerService;
	
	/** The additional component service bean. */
	@EJB
	AdditionalComponentServiceBean additionalComponentServiceBean;

    /**
     * Default constructor. 
     */
    public BusinessComponentControl() {
        // TODO Auto-generated constructor stub
    }
    
    /**
     * Inits the.
     */
    @PostConstruct
    public void init(){
    	TimerConfig timerConfig= new TimerConfig("Loading component entities", false);
        //load queries after 5 ms
        timerService.createSingleActionTimer(5, timerConfig);
    }
    
    /**
     * Load component entities cache.
     */
    @Timeout
    public void loadComponentEntitiesCache(){
    	additionalComponentServiceBean.getListAllMovementBehavior();
    	additionalComponentServiceBean.getListAllMovementType();
    	additionalComponentServiceBean.getListAllOperationMovement();
    	additionalComponentServiceBean.getListAllOperationType();
    	//Only load BusinessProcess
    	additionalComponentServiceBean.getListAllBusinessProcess();
    	additionalComponentServiceBean.getListAllFundsOperationType();
    	additionalComponentServiceBean.getAllBalanceType();
    	//Settlements
    	additionalComponentServiceBean.getListAllMechanismModality();
    	additionalComponentServiceBean.getListAllNegotiationMechanism();
    	additionalComponentServiceBean.getListAllNegotiationModality();
    	
    	//Accounting Account
    	additionalComponentServiceBean.getListAllAccountingAccount();
    }

}
