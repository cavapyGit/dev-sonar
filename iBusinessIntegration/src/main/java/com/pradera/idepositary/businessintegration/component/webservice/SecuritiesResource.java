package com.pradera.idepositary.businessintegration.component.webservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.transaction.TransactionSynchronizationRegistry;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.common.validation.to.ValidationOperationType;
import com.pradera.integration.common.validation.to.ValidationProcessTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.business.to.OperationQueryTO;
import com.pradera.integration.component.custody.service.CustodyRemoteService;
import com.pradera.integration.component.custody.to.SecurityEntryQueryTO;
import com.pradera.integration.component.securities.service.SecuritiesRemoteService;
import com.pradera.integration.component.securities.to.IssuanceRegisterTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.remote.ThreadRemoteContextHolder;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionStateType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SecuritiesResource.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/09/2015
 */
@Path("/SecuritiesResource")
@Stateless
public class SecuritiesResource implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The server configuration. */
	@Inject
	@InjectableResource(location = "ServerConfiguration.properties")
	private Properties serverConfiguration;
	
	/** The registry. */
	@Resource
	private TransactionSynchronizationRegistry registry;
	
	/** The parameter service bean. */
	@EJB
	private ParameterServiceBean parameterServiceBean;
	
	/** The interface component service. */
	@EJB
	InterfaceComponentService interfaceComponentService;
	
	/** The securities remote service. */
	@Inject
	private Instance<SecuritiesRemoteService> securitiesRemoteService;
	
	/** The custody remote service. */
	@Inject
	private Instance<CustodyRemoteService> custodyRemoteService;
	
	/** The interface component service bean. */
	@Inject
    private Instance<InterfaceComponentService> interfaceComponentServiceBean;
	
	/** The i depositary setup. */
	@Inject @DepositarySetup IdepositarySetup iDepositarySetup;
	
	/** The audits user. */
	String strAuditsUser;
	
	/** The audits IP address. */
	String strIpAddress;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		strAuditsUser = serverConfiguration.getProperty("webservice.audits.users.key");
		strIpAddress = serverConfiguration.getProperty("webservice.audits.ip");
	}
	
	/**
	 * Creates the account entry issuance bcb tgn.
	 *
	 * @param xmlParam the xml param
	 * @return the response
	 */
	@Performance
	@POST
	@Path("/rva")
	@Produces(MediaType.APPLICATION_XML)
	public Response createAccountEntryIssuanceBcbTgn(String xmlParam)
	{
		log.info(":::: inicio servicio rva ::::");
		ProcessFileTO objProcessFileTO = null;
		ValidationProcessTO objValidationProcessTO = null;
		IssuanceRegisterTO issuanceRegisterTO = null;
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		StringBuilder xmlResponse= new StringBuilder();
		
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		
		try {
		
			/*
			 * STEP 01:
			 * Parse the from XML file to TO object   
			 */
			
			objProcessFileTO = parameterServiceBean.validateInterfaceFile(ComponentConstant.INTERFACE_ISUANCE_BBXBTX,xmlParam, ComponentConstant.RVA_DPF_NAME, Boolean.FALSE);
			/*
			 * STEP 02:
			 * Call the Remote service 
			 */		
			
			Long idInterfaceProcess = interfaceComponentService.saveExternalInterfaceReceptionTx(objProcessFileTO,ExternalInterfaceReceptionType.WEBSERVICE.getCode(), objLoggerUser);
			objValidationProcessTO = objProcessFileTO.getValidationProcessTO();
			
			for(ValidationOperationType operation : objValidationProcessTO.getLstOperations()){
				
				issuanceRegisterTO = (IssuanceRegisterTO) operation;
				issuanceRegisterTO.setIdInterfaceProcess(idInterfaceProcess);				
				try {				
					objValidationProcessTO.getLstValidations().add(securitiesRemoteService.get().createIssuanceSecurityTx(issuanceRegisterTO, objLoggerUser));
					
				} catch (Exception e) {
					objValidationProcessTO.getLstValidations().addAll(parameterServiceBean.generateErrorValidationProcess(issuanceRegisterTO, e).getLstValidations());									
											
				} 																		
			}					
			
		} catch (Exception ex) {	
			if(objValidationProcessTO!=null){
				objValidationProcessTO.getLstValidations().addAll(parameterServiceBean.generateErrorValidationProcess(issuanceRegisterTO, ex).getLstValidations());
			}
			interfaceComponentService.saveInterfaceTransactionTx(issuanceRegisterTO, BooleanType.NO.getCode(), objLoggerUser);
			
		} finally {
			
			/*
			 * STEP 03:
			 * Generate the response XML file
			 */
			
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			if (objValidationProcessTO !=null && Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {
				xmlResponse.append(interfaceComponentService.saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
															objValidationProcessTO.getLstValidations(), objProcessFileTO.getStreamResponseFileDir(), 
															ComponentConstant.INTERFACE_TAG_ISSUANCE_BCB, ComponentConstant.INTERFACE_RECORD_ISSUANCE_BCB, processState, objLoggerUser));
			} else {
				xmlResponse.append("NO HAY DATOS");
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");
		}
		log.info(":::: FIN SERVICIO RVA ::::");
		return Response.status(201).entity(xmlResponse.toString()).build();
	}
	
	/**
	 * Gets the security expiration bcb tgn.
	 *
	 * @param xmlParam the xml param
	 * @return the security expiration bcb tgn
	 */
	@Performance
	@POST
	@Path("/con")
	@Produces(MediaType.APPLICATION_XML)
	public Response getSecurityExpirationBcbTgn(String xmlParam)
	{
		log.info(":::: inicio consulta de vencimientos BCB ::::");
		ProcessFileTO objProcessFileTO = null;
		ValidationProcessTO objValidationProcessTO = null;
		OperationQueryTO operationQueryTO = null;
		String schemaResponse= null;
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		StringBuilder xmlResponse= new StringBuilder();
		List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		
		try {
		
			/*
			 * STEP 01:
			 * Parse the from XML file to TO object   
			 */
			
			objProcessFileTO = parameterServiceBean.validateInterfaceFile(ComponentConstant.INTERFACE_SECURITY_QUERY_CONW,xmlParam, ComponentConstant.CON_BBXBTX_NAME, Boolean.FALSE);
			/*
			 * STEP 02:
			 * Call the Remote service 
			 */		
			
			Long idInterfaceProcess = interfaceComponentService.saveExternalInterfaceReceptionTx(objProcessFileTO,ExternalInterfaceReceptionType.WEBSERVICE.getCode(), objLoggerUser);
			objValidationProcessTO = objProcessFileTO.getValidationProcessTO();
			
			for(ValidationOperationType operation : objValidationProcessTO.getLstOperations()){
				
				operationQueryTO = (OperationQueryTO) operation;
				operationQueryTO.setIdInterfaceProcess(idInterfaceProcess);
				
				SecurityEntryQueryTO securityEntryQueryTO= new SecurityEntryQueryTO(operationQueryTO);
				
				lstRecordValidationTypes= custodyRemoteService.get().getListBbxBtxSecuritiesQueryOperation(securityEntryQueryTO);
				
				objValidationProcessTO.setLstValidations(lstRecordValidationTypes);
				
			}					
			
		} catch (Exception ex) {		
			
			ex.printStackTrace();
			objValidationProcessTO= parameterServiceBean.generateErrorValidationProcess(operationQueryTO, ex);
			processState= ExternalInterfaceReceptionStateType.FAILED.getCode();
			
		} finally {
			
			/*
			 * STEP 03:
			 * Generate the response XML file
			 */
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			
			if (objValidationProcessTO != null && !objValidationProcessTO.getLstValidations().get(0).getErrorCode().equals("0000")){
				schemaResponse= ComponentConstant.BBX_BTX_SCHEMA_RESPONSE_INCORRECT;
			}else{
				schemaResponse= ComponentConstant.BBX_BTX_SCHEMA_RESPONSE;
			}
			
			if (objValidationProcessTO != null && Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {
				xmlResponse.append(interfaceComponentServiceBean.get().saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
															objValidationProcessTO.getLstValidations(), schemaResponse, 
															ComponentConstant.BBX_BTX_TAG, ComponentConstant.QUERY_BBX_BTX_RECORD_TAG, processState, objLoggerUser));
			} else {
				xmlResponse.append("NO HAY DATOS");
			}
			
			
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");	
			
		}
		log.info(":::: FIN SERVICIO RVA ::::");
		return Response.status(201).entity(xmlResponse.toString()).build();
	}

}
