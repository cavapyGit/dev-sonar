package com.pradera.idepositary.businessintegration.component.service;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.ejb.AccessTimeout;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import com.pradera.commons.contextholder.interceptor.RemoteLoggerUserInterceptor;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.FundsComponentService;
import com.pradera.integration.component.business.to.FundsComponentTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.component.HolderAccountMovement;
import com.pradera.model.component.MovementBehavior;
import com.pradera.model.component.MovementType;
import com.pradera.model.component.OperationMovement;
import com.pradera.model.component.type.ParameterBalanceType;
import com.pradera.model.funds.CashAccountMovement;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.guarantees.AccountGuaranteeBalance;
import com.pradera.model.guarantees.GuaranteeMovement;
import com.pradera.model.guarantees.GuaranteeOperation;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class FundsComponentServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 30/03/2014
 */
@Singleton
public class FundsComponentServiceBean extends CrudDaoServiceBean implements FundsComponentService {
	
	/** The logger. */
	@Inject
	private PraderaLogger logger;
	
	/** The additional component service bean. */
	@EJB
	AdditionalComponentServiceBean additionalComponentServiceBean;

    /**
     * Default constructor. 
     */
    public FundsComponentServiceBean() {
        // TODO Auto-generated constructor stub
    }

    /**
	 * Main method to generate the cash movement and update the cash amounts in the institution cash account
	 * @param FundsComponentTO objFundsComponentTO
	 * @throws ServiceException
	 */
	@Override
	@AccessTimeout(unit=TimeUnit.MINUTES,value=5)
    @Interceptors(RemoteLoggerUserInterceptor.class)
	public void executeFundsComponent(FundsComponentTO objFundsComponentTO)	throws ServiceException {
		try {
			/*
			 * STEP 01: 
			 * To get the list of movement types to affect the institution cash account
			 * To this it's required: the business process and the funds operation type
			 */
			if (Validations.validateIsNull(objFundsComponentTO.getIdBusinessProcess())) {
				logger.error("idBusinessProcess: " +objFundsComponentTO.getIdBusinessProcess()
						+ ", idFundsOperationType: " +objFundsComponentTO.getIdFundsOperationType()
						+ ", indSourceTarget: " +ComponentConstant.SOURCE);
				throw new ServiceException(ErrorServiceType.BUSINESS_PROCESS_NOFOUND, ErrorServiceType.BUSINESS_PROCESS_NOFOUND.getMessage());
			}
			if (Validations.validateIsNull(objFundsComponentTO.getIdFundsOperationType())) {
				logger.error("idBusinessProcess: " +objFundsComponentTO.getIdBusinessProcess()
						+ ", idFundsOperationType: " +objFundsComponentTO.getIdFundsOperationType()
						+ ", indSourceTarget: " +ComponentConstant.SOURCE);
				throw new ServiceException(ErrorServiceType.OPERATION_TYPE_NOFOUND, ErrorServiceType.OPERATION_TYPE_NOFOUND.getMessage());
			}
			List<MovementType> lstMovementTypes = additionalComponentServiceBean.getListMovementTypes(objFundsComponentTO.getIdBusinessProcess(), null, 
																									objFundsComponentTO.getIdFundsOperationType(), ComponentConstant.SOURCE);
			if (Validations.validateListIsNullOrEmpty(lstMovementTypes)) 
			{
				logger.error("idBusinessProcess: " +objFundsComponentTO.getIdBusinessProcess()
							+ ", idFundsOperationType: " +objFundsComponentTO.getIdFundsOperationType()
							+ ", indSourceTarget: " +ComponentConstant.SOURCE);
				throw new ServiceException(ErrorServiceType.MOVEMENT_TYPE_NOFOUND, ErrorServiceType.MOVEMENT_TYPE_NOFOUND.getMessage());
			}
			
			List<OperationMovement> operationsMovement=additionalComponentServiceBean.allOperationMovements(objFundsComponentTO.getIdBusinessProcess(), null, 
					objFundsComponentTO.getIdFundsOperationType(),null);
			
//			List<OperationMovement> operationsMovements=additionalComponentServiceBean.allOperationMovements(null, null, 
//					objFundsComponentTO.getIdFundsOperationType(),objFundsComponentTO.getOperationState());
			
			//List<MovementType> movementsTypes =operationsMovements.stream().map(x->x.getMovementType()).collect(Collectors.toList());
			
//			if (operationsMovements.isEmpty()) 
//			{
//				logger.error("idBusinessProcess: " +objFundsComponentTO.getIdBusinessProcess()
//							+ ", idFundsOperationType: " +objFundsComponentTO.getIdFundsOperationType()
//							+ ", indSourceTarget: " +ComponentConstant.SOURCE);
//				//throw new ServiceException(ErrorServiceType.MOVEMENT_TYPE_NOFOUND, ErrorServiceType.MOVEMENT_TYPE_NOFOUND.getMessage());
//				return;
//			}
			
			/*
			 * STEP 02:
			 * To generate the cash account movements
			 */
			
//			if (Validations.validateListIsNotNullAndNotEmpty(lstMovementTypes)) {
//				generateCashAccountMovements(objFundsComponentTO, lstMovementTypes);
//			}
			if (Validations.validateListIsNotNullAndNotEmpty(operationsMovement)) {
				for(OperationMovement operationMovement:operationsMovement) {
					if(operationMovement.isSourceTargetBoth() || operationMovement.isSourceTargetManaged() || operationMovement.isSourceTargetManager()) {
						generateCashAccountMovements(objFundsComponentTO, lstMovementTypes);
					}
				}	
			}
			if (Validations.validateListIsNotNullAndNotEmpty(operationsMovement)) {
				for(OperationMovement operationMovement:operationsMovement) {
					if(operationMovement.isSourceTargetBoth()) {
						generateCashAccountMovementsCentralizing(objFundsComponentTO, lstMovementTypes);
					}
				}	
			}
			
			/*
			 * STEP 03:
			 * To update the cash account amounts 
			 */
//			if (Validations.validateListIsNotNullAndNotEmpty(lstMovementTypes)) {
//				updateCashAccountAmounts(objFundsComponentTO, lstMovementTypes);
//			}
			if (Validations.validateListIsNotNullAndNotEmpty(operationsMovement)) {
				for(OperationMovement operationMovement:operationsMovement) {
					if(operationMovement.isSourceTargetBoth() || operationMovement.isSourceTargetManaged() || operationMovement.isSourceTargetManager()) {
						updateCashAccountAmounts(objFundsComponentTO, lstMovementTypes);
					}
				}	
			}
			if (Validations.validateListIsNotNullAndNotEmpty(operationsMovement)) {
				for(OperationMovement operationMovement:operationsMovement) {
					if(operationMovement.isSourceTargetBoth()) {
						updateCashAccountAmountsCentralizing(objFundsComponentTO, lstMovementTypes);
					}
				}	
			}
		} catch (ServiceException e) {
			logger.error("idBusinessProcess: " +objFundsComponentTO.getIdBusinessProcess()
								+", idFundsOperationType: " +objFundsComponentTO.getIdFundsOperationType()
								+", idFundsOperation: " +objFundsComponentTO.getIdFundsOperation()
								+", idInstitutionCashAccount: " +objFundsComponentTO.getIdInstitutionCashAccount()
								+", cashAmount: " +objFundsComponentTO.getCashAmount());
			throw e;
		}
		
	}
	
	/**
	 * Method to generate the movements from the cash account according the list of movement types
	 * @param FundsComponentTO objFundsComponentTO
	 * @param List<MovementType> lstMovementTypes
	 * @throws ServiceException
	 */
	private void generateCashAccountMovements(FundsComponentTO objFundsComponentTO, List<MovementType> lstMovementTypes) throws ServiceException{
		FundsOperation objFundsOperation = additionalComponentServiceBean.getFundsOperation(objFundsComponentTO.getIdFundsOperation());
		for (int i=0; i<lstMovementTypes.size(); ++i)
		{
			MovementType objMovementType= lstMovementTypes.get(i);
			CashAccountMovement objCashAccountMovement = new CashAccountMovement();
			objCashAccountMovement.setCurrency(objFundsOperation.getCurrency());
			objCashAccountMovement.setFundsOperation(objFundsOperation);
			objCashAccountMovement.setInstitutionCashAccount(objFundsOperation.getInstitutionCashAccount());
			objCashAccountMovement.setMovementAmount(objFundsComponentTO.getCashAmount());
			objCashAccountMovement.setMovementDate(CommonsUtilities.currentDateTime());
			objCashAccountMovement.setMovementClass(objMovementType.getIndMovementClass());
			objCashAccountMovement.setMovementType(objMovementType);
			//saving the cash account movement
			this.create(objCashAccountMovement);
			//we verify for guarantee movements
			if (ComponentConstant.ONE.equals(objMovementType.getIndGuaranteeMovement())) {
				GuaranteeOperation objGuaranteeOperation = additionalComponentServiceBean.getGuaranteeOperation(objFundsComponentTO.getIdGuaranteeOperation());
				objGuaranteeOperation.getHolderAccountOperation().getIdHolderAccountOperationPk();
				GuaranteeMovement objGuaranteeMovement= generateGuaranteeMovements(null, objCashAccountMovement, objGuaranteeOperation);
				additionalComponentServiceBean.updateAccountGuaranteeBalance(objGuaranteeMovement, lstMovementTypes);
			}
		}
	}
	
	/**
	 * Method to generate the movements from the cash account according the list of movement types
	 * @param FundsComponentTO objFundsComponentTO
	 * @param List<MovementType> lstMovementTypes
	 * @throws ServiceException
	 */
	private void generateCashAccountMovementsCentralizing(FundsComponentTO objFundsComponentTO, List<MovementType> lstMovementTypes) throws ServiceException{
		FundsOperation objFundsOperation = additionalComponentServiceBean.getOneFundsOperation(objFundsComponentTO.getIdFundsOperation());
		for (int i=0; i<lstMovementTypes.size(); ++i)
		{
			MovementType objMovementType= lstMovementTypes.get(i);
			CashAccountMovement objCashAccountMovement = new CashAccountMovement();
			objCashAccountMovement.setCurrency(objFundsOperation.getCurrency());
			objCashAccountMovement.setFundsOperation(objFundsOperation);
			objCashAccountMovement.setInstitutionCashAccount(objFundsOperation.getInstitutionCashAccount().getInstitutionCashAccount());
			objCashAccountMovement.setMovementAmount(objFundsComponentTO.getCashAmount());
			objCashAccountMovement.setMovementDate(CommonsUtilities.currentDateTime());
			objCashAccountMovement.setMovementClass(objMovementType.getIndMovementClass());
			objCashAccountMovement.setMovementType(objMovementType);
			//saving the cash account movement
			this.create(objCashAccountMovement);
			//we verify for guarantee movements
			if (ComponentConstant.ONE.equals(objMovementType.getIndGuaranteeMovement())) {
				GuaranteeOperation objGuaranteeOperation = additionalComponentServiceBean.getGuaranteeOperation(objFundsComponentTO.getIdGuaranteeOperation());
				objGuaranteeOperation.getHolderAccountOperation().getIdHolderAccountOperationPk();
				GuaranteeMovement objGuaranteeMovement= generateGuaranteeMovements(null, objCashAccountMovement, objGuaranteeOperation);
				additionalComponentServiceBean.updateAccountGuaranteeBalance(objGuaranteeMovement, lstMovementTypes);
			}
		}
	}
	
	/**
	 * Method to update the amount from the institution cash account according the list of movement types
	 * @param FundsComponentTO objFundsComponentTO
	 * @param List<MovementType> lstMovementTypes
	 * @throws ServiceException
	 */
	private void updateCashAccountAmounts(FundsComponentTO objFundsComponentTO, List<MovementType> lstMovementTypes) throws ServiceException{
		try {
			InstitutionCashAccount objInstitutionCashAccount = additionalComponentServiceBean.getInstitutionCashAccount(objFundsComponentTO.getIdInstitutionCashAccount());
			for (int i=0; i< lstMovementTypes.size(); ++i)
			{
				MovementType objMovementType = lstMovementTypes.get(i);
				List<MovementBehavior> lstMovementBehaviors = additionalComponentServiceBean.getListMovementBehavior(objMovementType.getIdMovementTypePk());
				for (int j=0; j<lstMovementBehaviors.size(); ++j)
				{
					MovementBehavior objMovementBehavior = lstMovementBehaviors.get(j);
					final int balanceType = objMovementBehavior.getBalanceType().getIdBalanceTypePk().intValue();
					switch (balanceType) {
						case ComponentConstant.TOTAL_AMOUNT:
							logger.debug("Amount to modify : " + ParameterBalanceType.TOTAL_AMOUNT.getDescripcion());
							objInstitutionCashAccount.setTotalAmount(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
									objInstitutionCashAccount.getTotalAmount(), objFundsComponentTO.getCashAmount(), balanceType));
						break;
						case ComponentConstant.AVAILABLE_AMOUNT:
							logger.debug("Amount to modify : " + ParameterBalanceType.AVAILABLE_AMOUNT.getDescripcion());
							objInstitutionCashAccount.setAvailableAmount(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
									objInstitutionCashAccount.getAvailableAmount(), objFundsComponentTO.getCashAmount(), balanceType));
						break;
//						case ComponentConstant.DEPOSIT_AMOUNT:
//							logger.debug("Amount to modify : " + ParameterBalanceType.DEPOSIT_AMOUNT.getDescripcion());
//							objInstitutionCashAccount.setDepositAmount(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
//									objInstitutionCashAccount.getDepositAmount(), objFundsComponentTO.getCashAmount(), balanceType));
//						break;
//						case ComponentConstant.RETIREMENT_AMOUNT:
//							logger.debug("Amount to modify : " + ParameterBalanceType.RETIREMENT_AMOUNT.getDescripcion());
//							objInstitutionCashAccount.setRetirementAmount(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
//									objInstitutionCashAccount.getRetirementAmount(), objFundsComponentTO.getCashAmount(), balanceType));
//						break;
//						case ComponentConstant.MARGIN_AMOUNT:
//							logger.debug("Amount to modify : " + ParameterBalanceType.MARGIN_AMOUNT.getDescripcion());
//							objInstitutionCashAccount.setMarginAmount(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
//									objInstitutionCashAccount.getMarginAmount(), objFundsComponentTO.getCashAmount(), balanceType));
//						break;
					}
				}
			}
			//now we have to update the cash account
			this.update(objInstitutionCashAccount);
		} catch (ServiceException objException){
			logger.error(("updateCashAccountAmounts"));
			logger.debug(objException.getMessage());
			throw objException;
		}	 
	}
	
	/**
	 * Method to update the amount from the institution cash account according the list of movement types
	 * @param FundsComponentTO objFundsComponentTO
	 * @param List<MovementType> lstMovementTypes
	 * @throws ServiceException
	 */
	private void updateCashAccountAmountsCentralizing(FundsComponentTO objFundsComponentTO, List<MovementType> lstMovementTypes) throws ServiceException{
		try {
			FundsOperation objFundsOperation = additionalComponentServiceBean.getOneFundsOperation(objFundsComponentTO.getIdFundsOperation());
			InstitutionCashAccount objInstitutionCashAccount = additionalComponentServiceBean.getInstitutionCashAccount(objFundsOperation.getInstitutionCashAccount().getInstitutionCashAccount().getIdInstitutionCashAccountPk());
			for (int i=0; i< lstMovementTypes.size(); ++i)
			{
				MovementType objMovementType = lstMovementTypes.get(i);
				List<MovementBehavior> lstMovementBehaviors = additionalComponentServiceBean.getListMovementBehavior(objMovementType.getIdMovementTypePk());
				for (int j=0; j<lstMovementBehaviors.size(); ++j)
				{
					MovementBehavior objMovementBehavior = lstMovementBehaviors.get(j);
					final int balanceType = objMovementBehavior.getBalanceType().getIdBalanceTypePk().intValue();
					switch (balanceType) {
						case ComponentConstant.TOTAL_AMOUNT:
							logger.debug("Amount to modify : " + ParameterBalanceType.TOTAL_AMOUNT.getDescripcion());
							objInstitutionCashAccount.setTotalAmount(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
									objInstitutionCashAccount.getTotalAmount(), objFundsComponentTO.getCashAmount(), balanceType));
						break;
						case ComponentConstant.AVAILABLE_AMOUNT:
							logger.debug("Amount to modify : " + ParameterBalanceType.AVAILABLE_AMOUNT.getDescripcion());
							objInstitutionCashAccount.setAvailableAmount(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
									objInstitutionCashAccount.getAvailableAmount(), objFundsComponentTO.getCashAmount(), balanceType));
						break;
//						case ComponentConstant.DEPOSIT_AMOUNT:
//							logger.debug("Amount to modify : " + ParameterBalanceType.DEPOSIT_AMOUNT.getDescripcion());
//							objInstitutionCashAccount.setDepositAmount(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
//									objInstitutionCashAccount.getDepositAmount(), objFundsComponentTO.getCashAmount(), balanceType));
//						break;
//						case ComponentConstant.RETIREMENT_AMOUNT:
//							logger.debug("Amount to modify : " + ParameterBalanceType.RETIREMENT_AMOUNT.getDescripcion());
//							objInstitutionCashAccount.setRetirementAmount(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
//									objInstitutionCashAccount.getRetirementAmount(), objFundsComponentTO.getCashAmount(), balanceType));
//						break;
//						case ComponentConstant.MARGIN_AMOUNT:
//							logger.debug("Amount to modify : " + ParameterBalanceType.MARGIN_AMOUNT.getDescripcion());
//							objInstitutionCashAccount.setMarginAmount(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
//									objInstitutionCashAccount.getMarginAmount(), objFundsComponentTO.getCashAmount(), balanceType));
//						break;
					}
				}
			}
			//now we have to update the cash account
			this.update(objInstitutionCashAccount);
		} catch (ServiceException objException){
			logger.error(("updateCashAccountAmounts"));
			logger.debug(objException.getMessage());
			throw objException;
		}	 
	}
	
	/**
	 * Method to create the Guarantee movements to the guarantee operation according the generated holder account movement
	 * @param HolderAccountMovement objHolderAccountMovement
	 * @param GuaranteeOperation objGuaranteeOperation
	 * @throws Exception 
	 */
	private GuaranteeMovement generateGuaranteeMovements(HolderAccountMovement objHolderAccountMovement
			, CashAccountMovement objCashAccountMovement,GuaranteeOperation objGuaranteeOperation) throws ServiceException{
		GuaranteeMovement objGuaranteeMovement = new GuaranteeMovement();
		String idIsinCode= null;
		Long idInstitutionCashAccount= null;
		if (objGuaranteeOperation.getSecurities() != null)
			idIsinCode= objGuaranteeOperation.getSecurities().getIdIsinCode();
		if (objCashAccountMovement != null)
			idInstitutionCashAccount= objCashAccountMovement.getInstitutionCashAccount().getIdInstitutionCashAccountPk();
		//we get the account guarantee balance from the holder account inside the mechanism operation 
		AccountGuaranteeBalance objAccountGuaranteeBalance = additionalComponentServiceBean.getAccountGuaranteeBalance(objGuaranteeOperation.
																					getHolderAccountOperation().getIdHolderAccountOperationPk(), objGuaranteeOperation.getGuaranteeClass(),
																					idIsinCode, idInstitutionCashAccount);
		//if doesn't exist the account guarantee then we have to create it
		if (objAccountGuaranteeBalance == null) {
			objAccountGuaranteeBalance= new AccountGuaranteeBalance();
			objAccountGuaranteeBalance.setCurrency(objGuaranteeOperation.getCurrency());
			objAccountGuaranteeBalance.setGuaranteeClass(objGuaranteeOperation.getGuaranteeClass());
			objAccountGuaranteeBalance.setHolderAccount(objGuaranteeOperation.getHolderAccount());
			objAccountGuaranteeBalance.setHolderAccountOperation(objGuaranteeOperation.getHolderAccountOperation());
			objAccountGuaranteeBalance.setParticipant(objGuaranteeOperation.getParticipant());
			if (objGuaranteeOperation.getSecurities() != null)
				objAccountGuaranteeBalance.setSecurities(objGuaranteeOperation.getSecurities());
			if (objCashAccountMovement != null)
				objAccountGuaranteeBalance.setInstitutionCashAccount(objCashAccountMovement.getInstitutionCashAccount());
			this.create(objAccountGuaranteeBalance);
		}
		objGuaranteeMovement.setAccountGuaranteeBalance(objAccountGuaranteeBalance);
		objGuaranteeMovement.setGuaranteeOperation(objGuaranteeOperation);
		objGuaranteeMovement.setCurrency(objGuaranteeOperation.getCurrency());
		objGuaranteeMovement.setGuaranteeClass(objGuaranteeOperation.getGuaranteeClass());
		objGuaranteeMovement.setGuaranteeType(objGuaranteeOperation.getGuaranteeType());
		objGuaranteeMovement.setMovementDate(CommonsUtilities.currentDateTime());
		objGuaranteeMovement.setMovementQuantity(objGuaranteeOperation.getGuaranteeBalance());
		if (objHolderAccountMovement != null) {
			objGuaranteeMovement.setHolderAccountMovement(objHolderAccountMovement);
			objGuaranteeMovement.setMovementType(objHolderAccountMovement.getMovementType());
		}
		if (objCashAccountMovement != null) {
			objGuaranteeMovement.setCashAccountMovement(objCashAccountMovement);
			objGuaranteeMovement.setMovementType(objCashAccountMovement.getMovementType());
		}
		//save the guarantee movement
		this.create(objGuaranteeMovement);
		return objGuaranteeMovement;
	}

}
