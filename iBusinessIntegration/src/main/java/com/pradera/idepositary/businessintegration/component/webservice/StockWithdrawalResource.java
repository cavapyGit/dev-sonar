package com.pradera.idepositary.businessintegration.component.webservice;

import java.io.Serializable;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.transaction.TransactionSynchronizationRegistry;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.DepositarySetupBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.ValidationOperationType;
import com.pradera.integration.common.validation.to.ValidationProcessTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.custody.service.CustodyRemoteService;
import com.pradera.integration.component.custody.to.ReverseRegisterTO;
import com.pradera.integration.component.custody.to.StockEarlyPaymentRegisterTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.remote.ThreadRemoteContextHolder;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionStateType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class StockWithdrawalResource.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/09/2015
 */
@Path("/StockWithdrawalResource")
@Stateless
public class StockWithdrawalResource implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The custody remote service. */
	@Inject
	private Instance<CustodyRemoteService> custodyRemoteService;
	
	/** The interface component service bean. */
	@Inject
    private Instance<InterfaceComponentService> interfaceComponentServiceBean;
	
	/** The parameter service bean. */
	@EJB
	private ParameterServiceBean parameterServiceBean;
	
	/** The registry. */
	@Resource
	private TransactionSynchronizationRegistry registry;
	
	/** The depositary setup bean. */
	@Inject
	DepositarySetupBean depositarySetupBean;
	
	/** The interface component service. */
	@EJB
	InterfaceComponentService interfaceComponentService;
	
	/** The server configuration. */
	@Inject
	@InjectableResource(location = "ServerConfiguration.properties")
	private Properties serverConfiguration;
	
	/** The audits user. */
	String strAuditsUser;
	
	/** The audits IP address. */
	String strIpAddress;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		strAuditsUser = serverConfiguration.getProperty("webservice.audits.users.key");
		strIpAddress = serverConfiguration.getProperty("webservice.audits.ip");
	}
	
	/**
	 * Stocks early payment.
	 *
	 * @param xmlParam the xml param
	 * @return the response
	 */
	@Performance
	@POST
	@Path("/ran")
	@Produces(MediaType.APPLICATION_XML)
	public Response stocksEarlyPayment(String xmlParam)
	{
		log.info(":::: INICIO SERVICIO RAN ::::");
		ProcessFileTO objProcessFileTO = null;
		ValidationProcessTO objValidationProcessTO = null;
		StockEarlyPaymentRegisterTO stockEarlyPaymentRegisterTO = null;
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		StringBuilder xmlResponse= new StringBuilder();
		
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		try {
			
			/*
			 * STEP 01:
			 * Parse the from XML file to TO object   
			 */
			
			log.info(":::: INICIO VALIDACION ARCHIVO ::::");
			objProcessFileTO = parameterServiceBean.validateInterfaceFile(
																ComponentConstant.INTERFACE_EARLY_PAYMENT_DPF, 
																xmlParam, ComponentConstant.RAN_DPF_NAME, Boolean.FALSE);
			log.info(":::: FIN VALIDACION ARCHIVO ::::");
						
			/*
			 * STEP 02:
			 * Call the Remote service from stocks Early Payment
			 */		
			
			log.info(":::: INICIO LLAMADA REMOTA ::::");			
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				Long idInterfaceProcess = interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(objProcessFileTO, 
																				ExternalInterfaceReceptionType.WEBSERVICE.getCode(), objLoggerUser);
				objValidationProcessTO = objProcessFileTO.getValidationProcessTO();
				for(ValidationOperationType objValidationOperationType : objValidationProcessTO.getLstOperations()){
					stockEarlyPaymentRegisterTO = (StockEarlyPaymentRegisterTO) objValidationOperationType;					
					stockEarlyPaymentRegisterTO.setIdInterfaceProcess(idInterfaceProcess);
					stockEarlyPaymentRegisterTO.setDpfInterface(true);
					objValidationProcessTO.getLstValidations().add(custodyRemoteService.get().createEarlyPaymentOperationTx(objLoggerUser, stockEarlyPaymentRegisterTO));					
				}
			}
			log.info(":::: FIN LLAMADA REMOTA ::::");
			
		} catch (Exception ex) {
			ex.printStackTrace();
			objValidationProcessTO= parameterServiceBean.generateErrorValidationProcess(stockEarlyPaymentRegisterTO, ex);
			processState= ExternalInterfaceReceptionStateType.FAILED.getCode();
		} finally {		
			
			/*
			 * STEP 03:
			 * Generate the response XML file
			 */
			
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			if (objValidationProcessTO!=null && Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {
				xmlResponse.append(interfaceComponentServiceBean.get().saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
															objValidationProcessTO.getLstValidations(), objProcessFileTO.getStreamResponseFileDir(), 
															ComponentConstant.DPF_TAG, ComponentConstant.OPERATION_RECORD_TAG, processState, objLoggerUser));
			} else {
				xmlResponse.append("NO HAY DATOS");
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");																
		}
		log.info(":::: FIN SERVICIO RAN ::::");
		return Response.status(201).entity(xmlResponse.toString()).build();
	}
	
	/**
	 * Renew securities.
	 *
	 * @param xmlParam the xml param
	 * @return the response
	 */
	@Performance
	@POST
	@Path("/ren")
	@Produces(MediaType.APPLICATION_XML)
	public Response renewSecurities(String xmlParam)
	{
		log.info(":::: INICIO SERVICIO REN ::::");
		ProcessFileTO objProcessFileTO = null;
		ValidationProcessTO objValidationProcessTO = null;
		StockEarlyPaymentRegisterTO stockEarlyPaymentRegisterTO = null;		
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		StringBuilder xmlResponse= new StringBuilder();
		
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		try {
			
			/*
			 * STEP 01:
			 * Parse the from XML file to TO object   
			 */
			
			log.info(":::: INICIO VALIDACION ARCHIVO ::::");
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(
																ComponentConstant.INTERFACE_RENEW_SECURITIES_DPF, 
																xmlParam, ComponentConstant.REN_DPF_NAME, Boolean.FALSE);
			log.info(":::: FIN VALIDACION ARCHIVO ::::");
			
			/*
			 * STEP 02:
			 * Call the Remote service from Corporative Process
			 */		
			
			log.info(":::: INICIO LLAMADA REMOTA ::::");			
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				Long idInterfaceProcess = interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(objProcessFileTO, 
																				ExternalInterfaceReceptionType.WEBSERVICE.getCode(), objLoggerUser);
				objValidationProcessTO = objProcessFileTO.getValidationProcessTO();
				for(ValidationOperationType objValidationOperationType : objValidationProcessTO.getLstOperations()){
					stockEarlyPaymentRegisterTO = (StockEarlyPaymentRegisterTO) objValidationOperationType;					
					stockEarlyPaymentRegisterTO.setIdInterfaceProcess(idInterfaceProcess);
					stockEarlyPaymentRegisterTO.setDpfInterface(true);
					objValidationProcessTO.getLstValidations().add(custodyRemoteService.get().renewSecuritiesOperationTx(objLoggerUser, stockEarlyPaymentRegisterTO));																					
				}				
			}			
			log.info(":::: FIN LLAMADA REMOTA ::::");													
		} catch (Exception ex) {
			//ex.printStackTrace();
			objValidationProcessTO= parameterServiceBean.generateErrorValidationProcess(stockEarlyPaymentRegisterTO, ex);
			processState= ExternalInterfaceReceptionStateType.FAILED.getCode();
		} finally {
			
			/*
			 * STEP 03:
			 * Generate the response XML file
			 */
			
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			if (objValidationProcessTO!=null && Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {
				xmlResponse.append(interfaceComponentServiceBean.get().saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
															objValidationProcessTO.getLstValidations(), objProcessFileTO.getStreamResponseFileDir(), 
															ComponentConstant.DPF_TAG, ComponentConstant.OPERATION_RECORD_TAG, processState, objLoggerUser));
			} else {
				xmlResponse.append("NO HAY DATOS");
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");			
		}
		log.info(":::: FIN SERVICIO REN ::::");
		return Response.status(201).entity(xmlResponse.toString()).build();
	}
	
	/**
	 * Reverse stocks early payment.
	 *
	 * @param xmlParam the xml param
	 * @return the response
	 */
	@Performance
	@POST
	@Path("/rra")
	@Produces(MediaType.APPLICATION_XML)
	public Response reverseStocksEarlyPayment(String xmlParam)
	{
		log.info(":::: INICIO SERVICIO RRA ::::");
		ProcessFileTO objProcessFileTO = null;
		ValidationProcessTO objValidationProcessTO = null;
		ReverseRegisterTO reverseRegisterTO = null;		
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		StringBuilder xmlResponse= new StringBuilder();
		
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		try {
			
			/*
			 * STEP 01:
			 * Parse the from XML file to TO object   
			 */
			
			log.info(":::: INICIO VALIDACION ARCHIVO ::::");
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(
																ComponentConstant.INTERFACE_REVERSE_EARLY_PAYMENT_DPF, 
																xmlParam, ComponentConstant.RRA_DPF_NAME, Boolean.FALSE);
			log.info(":::: FIN VALIDACION ARCHIVO ::::");
			
			/*
			 * STEP 02:
			 * Call the Remote service from Corporative Process
			 */
			
			log.info(":::: INICIO LLAMADA REMOTA ::::");			
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				Long idInterfaceProcess = interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(objProcessFileTO, 
																				ExternalInterfaceReceptionType.WEBSERVICE.getCode(), objLoggerUser);
				objValidationProcessTO = objProcessFileTO.getValidationProcessTO();
				for(ValidationOperationType objValidationOperationType : objValidationProcessTO.getLstOperations()){
					reverseRegisterTO = (ReverseRegisterTO) objValidationOperationType;	
					reverseRegisterTO.setIdInterfaceProcess(idInterfaceProcess);
					reverseRegisterTO.setDpfInterface(true);
					objValidationProcessTO.getLstValidations().add(custodyRemoteService.get().reverseEarlyPaymentOperationTx(objLoggerUser, reverseRegisterTO));																				
				}				
			}			
			log.info(":::: FIN LLAMADA REMOTA ::::");
		
		} catch (Exception ex) {
			ex.printStackTrace();
			objValidationProcessTO = parameterServiceBean.generateErrorValidationProcess(reverseRegisterTO, ex);
			processState= ExternalInterfaceReceptionStateType.FAILED.getCode();
		} finally {
			
			/*
			 * STEP 03:
			 * Generate the response XML file
			 */
			
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			if (objValidationProcessTO!=null && Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {
				xmlResponse.append(interfaceComponentServiceBean.get().saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
															objValidationProcessTO.getLstValidations(), objProcessFileTO.getStreamResponseFileDir(), 
															ComponentConstant.DPF_TAG, ComponentConstant.OPERATION_RECORD_TAG, processState, objLoggerUser));
			} else {
				xmlResponse.append("NO HAY DATOS");
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");			
		}
		
		log.info(":::: FIN SERVICIO RRA ::::");		
		return Response.status(201).entity(xmlResponse.toString()).build();
	}
	
	/**
	 * Reverse renew securities.
	 *
	 * @param xmlParam the xml param
	 * @return the response
	 */
	@Performance
	@POST
	@Path("/rre")
	@Produces(MediaType.APPLICATION_XML)
	public Response reverseRenewSecurities(String xmlParam)
	{
		log.info(":::: INICIO SERVICIO RRE ::::");
		ProcessFileTO objProcessFileTO = null;
		ValidationProcessTO objValidationProcessTO = null;
		ReverseRegisterTO reverseRegisterTO = null;		
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		StringBuilder xmlResponse= new StringBuilder();
		
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		
		try {
			
			/*
			 * STEP 01:
			 * Parse the from XML file to TO object   
			 */
			
			log.info(":::: INICIO VALIDACION ARCHIVO ::::");
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(
																ComponentConstant.INTERFACE_REVERSE_RENEW_SECURITIES_DPF, 
																xmlParam, ComponentConstant.RRE_DPF_NAME, Boolean.FALSE);
			log.info(":::: FIN VALIDACION ARCHIVO ::::");
			
			/*
			 * STEP 02:
			 * Call the Remote service from Corporative Process
			 */
			
			log.info(":::: INICIO LLAMADA REMOTA ::::");			
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				Long idInterfaceProcess = interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(objProcessFileTO, 
						ExternalInterfaceReceptionType.WEBSERVICE.getCode(), objLoggerUser);				
				objValidationProcessTO = objProcessFileTO.getValidationProcessTO();				
				for(ValidationOperationType objValidationOperationType : objValidationProcessTO.getLstOperations()){
					reverseRegisterTO = (ReverseRegisterTO) objValidationOperationType;	
					reverseRegisterTO.setIdInterfaceProcess(idInterfaceProcess);
					reverseRegisterTO.setDpfInterface(true);
					objValidationProcessTO.getLstValidations().add(custodyRemoteService.get().reverseRenewSecuritiesOperationTx(objLoggerUser, reverseRegisterTO));																				
				}												
			}			
			log.info(":::: FIN LLAMADA REMOTA ::::");
			
		} catch (Exception ex) {
			ex.printStackTrace();
			objValidationProcessTO= parameterServiceBean.generateErrorValidationProcess(reverseRegisterTO, ex);
			processState= ExternalInterfaceReceptionStateType.FAILED.getCode();
		} finally {	
		
			/*
			 * STEP 03:
			 * Generate the response XML file
			 */
			
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			if (objValidationProcessTO !=null && Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {
				xmlResponse.append(interfaceComponentServiceBean.get().saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
															objValidationProcessTO.getLstValidations(), objProcessFileTO.getStreamResponseFileDir(), 
															ComponentConstant.DPF_TAG, ComponentConstant.OPERATION_RECORD_TAG, processState, objLoggerUser));
			} else {
				xmlResponse.append("NO HAY DATOS");
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");			
		}
		
		log.info(":::: FIN SERVICIO RRE ::::");		
		return Response.status(201).entity(xmlResponse.toString()).build();
	}
	
	/**
	 * Stocks early payment bcb.
	 *
	 * @param xmlParam the xml param
	 * @return the response
	 */
	@Performance
	@POST
	@Path("/rea") // Originalmente es RAN
	@Produces(MediaType.APPLICATION_XML)
	public Response stocksEarlyPaymentBcb(String xmlParam)
	{
		log.info(":::: inicio servicio rea ::::");
		ProcessFileTO objProcessFileTO = null;
		ValidationProcessTO objValidationProcessTO = null;
		StockEarlyPaymentRegisterTO stockEarlyPaymentRegisterTO = null;
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		StringBuilder xmlResponse= new StringBuilder();
		
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		
		try {
		
			/*
			 * STEP 01:
			 * Parse the from XML file to TO object   
			 */
			
			objProcessFileTO = parameterServiceBean.validateInterfaceFile(ComponentConstant.INTERFACE_EARLY_PAYMENT_BCBTGN,xmlParam, ComponentConstant.RAN_WSBCBTGN_NAME, Boolean.FALSE);
			/*
			 * STEP 02:
			 * Call the Remote service 
			 */		
			
			Long idInterfaceProcess = interfaceComponentService.saveExternalInterfaceReceptionTx(objProcessFileTO,ExternalInterfaceReceptionType.WEBSERVICE.getCode(), objLoggerUser);
			objValidationProcessTO = objProcessFileTO.getValidationProcessTO();
			
			for(ValidationOperationType operation : objValidationProcessTO.getLstOperations()){
				
				stockEarlyPaymentRegisterTO = (StockEarlyPaymentRegisterTO) operation;
				stockEarlyPaymentRegisterTO.setIdInterfaceProcess(idInterfaceProcess);
				
				try {				
					objValidationProcessTO.getLstValidations().add(custodyRemoteService.get().createEarlyPaymentOperationBcbTgnTx(objLoggerUser, stockEarlyPaymentRegisterTO));					
//					interfaceComponentService.saveInterfaceTransactionTx(stockEarlyPaymentRegisterTO, BooleanType.YES.getCode(), objLoggerUser);									
				} catch (Exception e) {
					objValidationProcessTO.getLstValidations().addAll(parameterServiceBean.generateErrorValidationProcess(stockEarlyPaymentRegisterTO, e).getLstValidations());									
//					interfaceComponentService.saveInterfaceTransactionTx(stockEarlyPaymentRegisterTO, BooleanType.NO.getCode(), objLoggerUser);								
				} 																		
			}					
			
		} catch (Exception ex) {	
			if(objValidationProcessTO!=null){
				objValidationProcessTO.getLstValidations().addAll(parameterServiceBean.generateErrorValidationProcess(stockEarlyPaymentRegisterTO, ex).getLstValidations());
			}
			interfaceComponentService.saveInterfaceTransactionTx(stockEarlyPaymentRegisterTO, BooleanType.NO.getCode(), objLoggerUser);
			
		} finally {
			
			/*
			 * STEP 03:
			 * Generate the response XML file
			 */
			
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			if (objValidationProcessTO!=null && Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {
				xmlResponse.append(interfaceComponentService.saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
															objValidationProcessTO.getLstValidations(), objProcessFileTO.getStreamResponseFileDir(), 
															ComponentConstant.INTERFACE_TAG_STOCK_EARLY_BCBTGN, ComponentConstant.INTERFACE_RECORD_ISSUANCE_BCB, processState, objLoggerUser));
			} else {
				xmlResponse.append("NO HAY DATOS");
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");
		}
		log.info(":::: FIN SERVICIO REA ::::");
		return Response.status(201).entity(xmlResponse.toString()).build();
	}
	
	@Performance
	@POST
	@Path("/rca") // Originalmente es RAN
	@Produces(MediaType.APPLICATION_XML)
	public Response earlyPaymentNoDpf(String xmlParam){
		log.info(":::: inicio servicio RCA ::::");
		ProcessFileTO objProcessFileTO = null;
		ValidationProcessTO objValidationProcessTO = null;
		StockEarlyPaymentRegisterTO stockEarlyPaymentRegisterTO = null;
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		StringBuilder xmlResponse= new StringBuilder();
		
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		
		try {
		
			/*
			 * STEP 01:
			 * Parse the from XML file to TO object   
			 */
			objProcessFileTO = parameterServiceBean.validateInterfaceFile(ComponentConstant.INTERFACE_EARLY_PAYMENT_NO_DPF,xmlParam, ComponentConstant.RAN_WSBCBTGN_NAME, Boolean.FALSE);
			
			/*
			 * STEP 02:
			 * Call the Remote service 
			 */		
			Long idInterfaceProcess = interfaceComponentService.saveExternalInterfaceReceptionTx(objProcessFileTO,ExternalInterfaceReceptionType.WEBSERVICE.getCode(), objLoggerUser);
			objValidationProcessTO = objProcessFileTO.getValidationProcessTO();
			
			for(ValidationOperationType operation : objValidationProcessTO.getLstOperations()){
				
				stockEarlyPaymentRegisterTO = (StockEarlyPaymentRegisterTO) operation;
				stockEarlyPaymentRegisterTO.setIdInterfaceProcess(idInterfaceProcess);
				
				try {				
					objValidationProcessTO.getLstValidations().add(custodyRemoteService.get().createEarlyPaymentNoDPFOperationTx(objLoggerUser, stockEarlyPaymentRegisterTO));					
//					interfaceComponentService.saveInterfaceTransactionTx(stockEarlyPaymentRegisterTO, BooleanType.YES.getCode(), objLoggerUser);									
				} catch (Exception e) {
					objValidationProcessTO.getLstValidations().addAll(parameterServiceBean.generateErrorValidationProcess(stockEarlyPaymentRegisterTO, e).getLstValidations());									
//					interfaceComponentService.saveInterfaceTransactionTx(stockEarlyPaymentRegisterTO, BooleanType.NO.getCode(), objLoggerUser);								
				} 																		
			}					
			
		} catch (Exception ex) {	
			if(objValidationProcessTO!=null){
				objValidationProcessTO.getLstValidations().addAll(parameterServiceBean.generateErrorValidationProcess(stockEarlyPaymentRegisterTO, ex).getLstValidations());
			}
			interfaceComponentService.saveInterfaceTransactionTx(stockEarlyPaymentRegisterTO, BooleanType.NO.getCode(), objLoggerUser);
			
		} finally {
			
			/*
			 * STEP 03:
			 * Generate the response XML file
			 */
			
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			if (objValidationProcessTO!=null && Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {
				xmlResponse.append(interfaceComponentService.saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
															objValidationProcessTO.getLstValidations(), objProcessFileTO.getStreamResponseFileDir(), 
															ComponentConstant.INTERFACE_TAG_STOCK_EARLY_NO_DPF, ComponentConstant.INTERFACE_RECORD_ISSUANCE_BCB, processState, objLoggerUser));
			} else {
				xmlResponse.append("NO HAY DATOS");
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");
		}
		log.info(":::: FIN SERVICIO RCA ::::");
		return Response.status(201).entity(xmlResponse.toString()).build();
	}
	
	@Performance
	@POST
	@Path("/rpa")
	@Produces(MediaType.APPLICATION_XML)
	public Response reverseEarlyPaymentNoDpf(String xmlParam)
	{
		log.info(":::: INICIO SERVICIO RPA (Reversion de Cancelacion Anticipada (No DPF)) ::::");
		ProcessFileTO objProcessFileTO = null;
		ValidationProcessTO objValidationProcessTO = null;
		ReverseRegisterTO reverseRegisterTO = null;		
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		StringBuilder xmlResponse= new StringBuilder();
		
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		try {
			
			/*
			 * STEP 01:
			 * Parse the from XML file to TO object   
			 */
			log.info(":::: INICIO VALIDACION ARCHIVO ::::");
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(
																ComponentConstant.INTERFACE_REVERSE_EARLY_PAYMENT_NO_DPF, 
																xmlParam, ComponentConstant.RPA_NO_DPF_NAME, Boolean.FALSE);
			log.info(":::: FIN VALIDACION ARCHIVO ::::");
			
			/*
			 * STEP 02:
			 * Call the Remote service from Corporative Process
			 */
			
			log.info(":::: INICIO LLAMADA REMOTA ::::");			
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				Long idInterfaceProcess = interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(objProcessFileTO, 
																				ExternalInterfaceReceptionType.WEBSERVICE.getCode(), objLoggerUser);
				objValidationProcessTO = objProcessFileTO.getValidationProcessTO();
				for(ValidationOperationType objValidationOperationType : objValidationProcessTO.getLstOperations()){
					reverseRegisterTO = (ReverseRegisterTO) objValidationOperationType;	
					reverseRegisterTO.setIdInterfaceProcess(idInterfaceProcess);
					reverseRegisterTO.setDpfInterface(true);
					objValidationProcessTO.getLstValidations().add(custodyRemoteService.get().revreverseEarlyPaymentNoDpfTx(objLoggerUser, reverseRegisterTO));																				
				}				
			}			
			log.info(":::: FIN LLAMADA REMOTA ::::");
		
		} catch (Exception ex) {
			ex.printStackTrace();
			objValidationProcessTO = parameterServiceBean.generateErrorValidationProcess(reverseRegisterTO, ex);
			processState= ExternalInterfaceReceptionStateType.FAILED.getCode();
		} finally {
			
			/*
			 * STEP 03:
			 * Generate the response XML file
			 */
			
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			if (objValidationProcessTO!=null && Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {
				xmlResponse.append(interfaceComponentServiceBean.get().saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
															objValidationProcessTO.getLstValidations(), objProcessFileTO.getStreamResponseFileDir(), 
															ComponentConstant.INTERFACE_TAG_REVERT_CAN_ANTICIPADA_RESPONSE, ComponentConstant.INTERFACE_RECORD_ISSUANCE_BCB, processState, objLoggerUser));
			} else {
				xmlResponse.append("NO HAY DATOS");
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");			
		}
		
		log.info(":::: FIN SERVICIO RRA ::::");		
		return Response.status(201).entity(xmlResponse.toString()).build();
	}

}
