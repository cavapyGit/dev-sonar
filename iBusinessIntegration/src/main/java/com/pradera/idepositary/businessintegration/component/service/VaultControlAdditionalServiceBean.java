package com.pradera.idepositary.businessintegration.component.service;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.component.business.to.VaultControlTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.VaultControlComponent;
import com.pradera.model.component.VaultControlRules;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.issuancesecuritie.Security;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;


@Stateless
public class VaultControlAdditionalServiceBean extends CrudDaoServiceBean{
	
	public List<VaultControlRules> verifyYearVaultControlRules(Date expirationDate) {
		
		Calendar time = Calendar.getInstance();
		time.setTime(expirationDate);
		
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("SELECT VCR FROM VaultControlRules VCR ");
		stringBuffer.append("WHERE EXTRACT(YEAR FROM VCR.expirationDate) = :expirationDate ");
		stringBuffer.append("ORDER BY VCR.id ASC");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("expirationDate", time.get(Calendar.YEAR));
		
		return (List<VaultControlRules>)query.getResultList();
		
		
	}
	
	public List<VaultControlRules> getBoxesForExpirationDateRule() {
		
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("SELECT VCR FROM VaultControlRules VCR ");
		stringBuffer.append("WHERE VCR.indBoxForIssuers = : indBoxForIssuers ");
		stringBuffer.append("AND VCR.expirationDate IS NULL ");
		stringBuffer.append("ORDER BY VCR.id ASC");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("indBoxForIssuers", 0);
		
		return (List<VaultControlRules>)query.getResultList();
		
		
	}
	
	@SuppressWarnings("unchecked")
	public List<VaultControlComponent> getCertificatesPerYear(Date fechaVencimiento) {
		Calendar time = Calendar.getInstance();
		time.setTime(fechaVencimiento);
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT VCC");
		stringBuffer.append(" FROM VaultControlComponent VCC inner join VCC.vaultControlRules");
		stringBuffer.append(" WHERE EXTRACT(YEAR FROM VCC.vaultControlRules.expirationDate) = :fechaVencimientio  ");
		stringBuffer.append(" AND (VCC.armario, VCC.nivel, VCC.caja) NOT IN (SELECT VCC.armario, VCC.nivel, VCC.caja FROM VaultControlComponent VCC ");
		stringBuffer.append(" WHERE VCC.armario = 2 AND VCC.nivel IN (4, 5))");
		stringBuffer.append(" ORDER BY  VCC.armario ASC, VCC.nivel ASC, VCC.caja ASC, VCC.indiceCaja ASC ");
		//stringBuffer.append(" and HMB.availableBalance > 0 ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("fechaVencimientio", time.get(Calendar.YEAR));
		
		return (List<VaultControlComponent>)query.getResultList();
		
	}
	
	/*@SuppressWarnings("unchecked")
	public List<VaultControlComponent> getPositionsForException() {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT VCC FROM VaultControlComponent VCC ");
		stringBuffer.append(" WHERE VCC.armario = 2 AND VCC.nivel IN (4, 5) AND VCC.idSecurityCodeFk IS NULL AND VCC.valor IS NULL ");
		stringBuffer.append(" ORDER BY VCC.armario ASC, VCC.nivel ASC, VCC.caja ASC, VCC.indiceCaja ASC ");
		
		Query query= em.createQuery(stringBuffer.toString());
		
		return (List<VaultControlComponent>)query.getResultList();
	}*/
	
	public void saveCertificates(VaultControlTO vaultControlTO, LoggerUser loggerUser) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append("UPDATE VaultControlComponent set ");
		stringBuffer.append("certificado = :certificado, valor = :valor, idSecurityCodeFk = :idSecurityCodeFk , registryDate = :registryDate ");
		
		if (loggerUser != null) {
			stringBuffer.append(",lastModifyUser = :lastModifyUser, lastModifyIp = :lastModifyIp, lastModifyApp = :lastModifyApp, lastModifyDate = :lastModifyDate ");
		}
		
		if(vaultControlTO.getIndCoupon() != null) {
			stringBuffer.append(", indCoupon = :indCoupon ");
		}
		
		if(vaultControlTO.getCouponNumber() != null) {
			stringBuffer.append(", couponNumber = :couponNumber ");
		}
		
		if(vaultControlTO.getIdPendingSecurityCodePk() != null) {
			stringBuffer.append(", idPendingSecurityCodePk = :idPendingSecurityCodePk ");
		}
		
		if(vaultControlTO.getIdPhysicalCertificateFk() != null) {
			stringBuffer.append(", idPhysicalCertificateFk = :idPhysicalCertificateFk ");
		}
		
		//stringBuffer.append("WHERE armario = :armario and nivel = :nivel and boveda = :boveda and ");
		stringBuffer.append("WHERE armario = :armario and nivel = :nivel and ");
		stringBuffer.append("caja = :caja and indiceCaja = :indiceCaja");
		
		if(vaultControlTO.getFolio() != null) {
			stringBuffer.append(" AND folio = :folio");
		}
	
 		
		Query query = em.createQuery(stringBuffer.toString());
		
		query.setParameter("idSecurityCodeFk", vaultControlTO.getIdSecurityCodeFk());
		query.setParameter("certificado", vaultControlTO.getCertificado());
		query.setParameter("valor", vaultControlTO.getValor());
		query.setParameter("armario", vaultControlTO.getArmario());
		query.setParameter("nivel", vaultControlTO.getNivel());
		//query.setParameter("boveda", vaultControlTO.getBoveda());
		query.setParameter("caja", vaultControlTO.getCaja());
		query.setParameter("indiceCaja", vaultControlTO.getIndiceCaja());
		query.setParameter("registryDate", CommonsUtilities.currentDate());
		
		if (loggerUser != null) {
			query.setParameter("lastModifyUser", loggerUser.getUserName());
			query.setParameter("lastModifyIp", loggerUser.getIpAddress());
			query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
			query.setParameter("lastModifyDate", loggerUser.getAuditTime());		
		}
		
		if(vaultControlTO.getFolio() != null) {
			query.setParameter("folio", vaultControlTO.getFolio());
		}
		
		if(vaultControlTO.getIndCoupon() != null) {
			query.setParameter("indCoupon", vaultControlTO.getIndCoupon());
		}
		
		if(vaultControlTO.getCouponNumber() != null) {
			query.setParameter("couponNumber", vaultControlTO.getCouponNumber());
		}
		
		if(vaultControlTO.getIdPendingSecurityCodePk() != null) {
			query.setParameter("idPendingSecurityCodePk", vaultControlTO.getIdPendingSecurityCodePk());
		}
		
		if(vaultControlTO.getIdPhysicalCertificateFk() != null) {
			query.setParameter("idPhysicalCertificateFk", vaultControlTO.getIdPhysicalCertificateFk());
		}
		
		query.executeUpdate();
		em.flush();
		em.clear();

	}
	
	public Long verifyCertificate(String idSecurityCodeFk) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT COUNT(VCC.idSecurityCodeFk) FROM ");
		stringBuffer.append(" VaultControlComponent VCC");
		stringBuffer.append(" WHERE idSecurityCodeFk = :idSecurityCodeFk");
		
		Query query = em.createQuery(stringBuffer.toString());
		
		query.setParameter("idSecurityCodeFk", idSecurityCodeFk);
		
		return (Long) query.getSingleResult();

	}
	
	public Long verifyCertificateWithIdCertificatePk(Long idPhysicalCertificateFk) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT COUNT(VCC.idSecurityCodeFk) FROM ");
		stringBuffer.append(" VaultControlComponent VCC");
		stringBuffer.append(" WHERE idPhysicalCertificateFk = :idPhysicalCertificateFk");
		
		Query query = em.createQuery(stringBuffer.toString());
		
		query.setParameter("idPhysicalCertificateFk", idPhysicalCertificateFk);
		
		return (Long) query.getSingleResult();

	}
	
	
	
	public void deleteCertificate(String idSecurityCodeFk) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" UPDATE VaultControlComponent set ");
		stringBuffer.append(" certificado = NULL, valor = NULL, idSecurityCodeFk = NULL, ");
		stringBuffer.append(" indCoupon = NULL, couponNumber = NULL, idPendingSecurityCodePk = NULL, idPhysicalCertificateFk = NULL ");
		stringBuffer.append(" WHERE idSecurityCodeFk = :idSecurityCodeFk ");
		
		Query query = em.createQuery(stringBuffer.toString());
		
		query.setParameter("idSecurityCodeFk", idSecurityCodeFk);
		
		query.executeUpdate();
	}
	
	public void deleteCertificateWithIdCertificatePk(Long idPhysicalCertificateFK) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" UPDATE VaultControlComponent set ");
		stringBuffer.append(" certificado = NULL, valor = NULL, idSecurityCodeFk = NULL, ");
		stringBuffer.append(" indCoupon = NULL, couponNumber = NULL, idPendingSecurityCodePk = NULL, idPhysicalCertificateFk = NULL");
		stringBuffer.append(" WHERE idPhysicalCertificateFk = :idPhysicalCertificateFk ");
		
		Query query = em.createQuery(stringBuffer.toString());
		
		query.setParameter("idPhysicalCertificateFk", idPhysicalCertificateFK);
		
		query.executeUpdate();
	}
	
	public Integer verifyInstrumentType(String idSecurityCodeFk) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append("SELECT SE.instrumentType FROM Security SE ");
		stringBuffer.append("WHERE SE.idSecurityCodePk = :idSecurityCodeFk");
		
		Query query = em.createQuery(stringBuffer.toString());
		
		query.setParameter("idSecurityCodeFk", idSecurityCodeFk);
		
		return (Integer) query.getSingleResult();
	}
	
	public String verifyIssuer(String idSecurityCodeFk) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append("SELECT SE.ID_ISSUER_FK FROM SECURITY SE ");
		stringBuffer.append("WHERE SE.ID_SECURITY_CODE_PK = :idSecurityCodeFk");
		
		Query query = em.createNativeQuery(stringBuffer.toString());
		
		query.setParameter("idSecurityCodeFk", idSecurityCodeFk);
		
		return (String) query.getSingleResult();
	}
	
	public Long verifyVaultControlRulesExist() {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("SELECT COUNT(1) FROM VaultControlRules VCR");
		
		Query query = em.createQuery(stringBuffer.toString());

		return (Long) query.getSingleResult();

	}
	
	@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
	public Integer getMaxBoxQuantityPerLevel() {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("SELECT PT.shortInteger FROM ParameterTable PT WHERE PT.parameterTablePk = 2826");
		Query query = em.createQuery(stringBuffer.toString());
		return (Integer) query.getSingleResult();
		
	}
	
	public Integer getMaxLevel() {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("SELECT PT.shortInteger FROM ParameterTable PT WHERE PT.parameterTablePk = 2827");
		Query query = em.createQuery(stringBuffer.toString());
		return (Integer) query.getSingleResult();
	}
	
	public Integer getLockersQuantityPerVault() {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("SELECT PT.shortInteger FROM ParameterTable PT WHERE PT.parameterTablePk = 2828");
		Query query = em.createQuery(stringBuffer.toString());
		return (Integer) query.getSingleResult();
	}
	
	public VaultControlRules getLastRule() {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("SELECT VCR FROM VaultControlRules VCR where ROWNUM = 1 ORDER BY VCR.id DESC ");
		Query query = em.createQuery(stringBuffer.toString());
		return (VaultControlRules) query.getSingleResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<VaultControlComponent> getCertificatesPerBox(Date fechaVencimiento, VaultControlRules filter) {
		//Calendar time = Calendar.getInstance();
		//time.setTime(fechaVencimiento);
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT VCC");
		stringBuffer.append(" FROM VaultControlComponent VCC inner join VCC.vaultControlRules");
		//stringBuffer.append(" WHERE EXTRACT(YEAR FROM VCC.vaultControlRules.expirationDate) = :fechaVencimientio AND ");
		stringBuffer.append(" WHERE ");
		stringBuffer.append(" VCC.armario = :armario AND VCC.nivel = :nivel AND VCC.caja = :caja");
		stringBuffer.append(" ORDER BY  VCC.armario ASC, VCC.nivel ASC, VCC.caja ASC, VCC.folio ASC, VCC.indiceCaja ASC ");
		//stringBuffer.append(" and HMB.availableBalance > 0 ");
		
		Query query= em.createQuery(stringBuffer.toString());
		//query.setParameter("fechaVencimientio", time.get(Calendar.YEAR));
		query.setParameter("armario", filter.getCabinet().toString());
		query.setParameter("nivel", filter.getLevel());
		query.setParameter("caja", filter.getBox());
		
		
		return (List<VaultControlComponent>)query.getResultList();
		
	}
	
	public List<VaultControlComponent> getCertificatesPerBox(VaultControlRules filter) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT VCC ");
		stringBuffer.append(" FROM VaultControlComponent VCC ");
		stringBuffer.append(" WHERE ");
		stringBuffer.append(" VCC.armario = :armario AND VCC.nivel = :nivel AND VCC.caja = :caja ");
		stringBuffer.append(" AND VCC.idSecurityCodeFk IS NULL ");
		stringBuffer.append(" ORDER BY  VCC.armario ASC, VCC.nivel ASC, VCC.caja ASC, VCC.folio ASC, VCC.indiceCaja ASC ");
		//stringBuffer.append(" and HMB.availableBalance > 0 ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("armario", filter.getCabinet().toString());
		query.setParameter("nivel", filter.getLevel());
		query.setParameter("caja", filter.getBox());
		
		
		return (List<VaultControlComponent>)query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<VaultControlComponent> getCertificatesPerBoxForIssuers(VaultControlTO filter) {
		//Calendar time = Calendar.getInstance();
		//time.setTime(fechaVencimiento);
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT VCC");
		//stringBuffer.append(" FROM VaultControlComponent VCC inner join VCC.vaultControlRules");
		stringBuffer.append(" FROM VaultControlComponent VCC WHERE ");
		//stringBuffer.append(" WHERE EXTRACT(YEAR FROM VCC.vaultControlRules.fechaVencimiento) = :fechaVencimientio  ");
		stringBuffer.append(" VCC.armario = :armario AND VCC.nivel = :nivel AND VCC.caja = :caja and VCC.folio = :folio");
		stringBuffer.append(" ORDER BY  VCC.armario ASC, VCC.nivel ASC, VCC.caja ASC, VCC.folio ASC, VCC.indiceCaja ASC ");
		//stringBuffer.append(" and HMB.availableBalance > 0 ");
		
		Query query= em.createQuery(stringBuffer.toString());
		//query.setParameter("fechaVencimientio", time.get(Calendar.YEAR));
		query.setParameter("armario", filter.getArmario().toString());
		query.setParameter("nivel", filter.getNivel());
		query.setParameter("caja", filter.getCaja());
		query.setParameter("folio", filter.getFolio());
		
		
		return (List<VaultControlComponent>)query.getResultList();
		
	}
	
	public List<Object[]> getIssuersPerBox(VaultControlTO filter) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("SELECT DISTINCT(SE.ID_ISSUER_FK), CB.FOLIO  FROM CONTROL_BOVEDA CB INNER JOIN SECURITY SE ON ");
		stringBuffer.append("CB.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK WHERE CB.ARMARIO = 2 AND CB.NIVEL = :nivel AND CB.CAJA = :caja");
		
		Query query= em.createNativeQuery(stringBuffer.toString());
		query.setParameter("nivel", filter.getNivel());
		query.setParameter("caja", filter.getCaja());
		
		return (List<Object[]>)query.getResultList();
	}
	
	public List<String> getIssuersPerBox(VaultControlRules filter) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT DISTINCT(SE.ID_ISSUER_FK) FROM CONTROL_BOVEDA CB INNER JOIN SECURITY SE ON ");
		stringBuffer.append(" CB.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK WHERE CB.ARMARIO = :cabinet AND CB.NIVEL = :nivel AND CB.CAJA = :caja");
		
		Query query= em.createNativeQuery(stringBuffer.toString());
		query.setParameter("cabinet", filter.getCabinet());
		query.setParameter("nivel", filter.getLevel());
		query.setParameter("caja", filter.getBox());
		
		return (List<String>)query.getResultList();
	}
	
	public Security getSecurity(String idSecurityCodeFk) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append("SELECT SE FROM Security SE ");
		stringBuffer.append("WHERE SE.idSecurityCodePk = :idSecurityCodeFk");
		
		Query query = em.createQuery(stringBuffer.toString());
		
		query.setParameter("idSecurityCodeFk", idSecurityCodeFk);
		
		return (Security) query.getSingleResult();
	}
	
	public String getSecurityReference(String idSecurityCodeFk) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append("SELECT SE.ID_REF_SECURITY_CODE_FK FROM SECURITY SE ");
		stringBuffer.append("WHERE SE.ID_SECURITY_CODE_PK = :idSecurityCodeFk");
		
		Query query = em.createNativeQuery(stringBuffer.toString());
		
		query.setParameter("idSecurityCodeFk", idSecurityCodeFk);
		
		return (String) query.getSingleResult();
	}
	
	public List<PhysicalCertificate> getPhysicalCertificates(String idSecurityCodeFk) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append("Select p From PhysicalCertificate p ");
		stringBuffer.append("WHERE p.securities.idSecurityCodePk = :idSecurityCodeFk ");
		stringBuffer.append("AND p.state = :state ");
		
		Query query = em.createQuery(stringBuffer.toString());

		query.setParameter("idSecurityCodeFk", idSecurityCodeFk);
		query.setParameter("state", 601);
		
		return (List<PhysicalCertificate>) query.getResultList();	
	}
	
	public PhysicalCertificate getPhysicalCertificate(Long idPhysicalCertificatePk) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append("Select p From PhysicalCertificate p ");
		stringBuffer.append("WHERE p.idPhysicalCertificatePk = :idPhysicalCertificatePk ");
		
		Query query = em.createQuery(stringBuffer.toString());

		query.setParameter("idPhysicalCertificatePk", idPhysicalCertificatePk);
		
		return (PhysicalCertificate) query.getSingleResult();	
	}
	
	
	public void createVaultControlRules(VaultControlRules vaultControlRules) {
		this.create(vaultControlRules);
	}
	
	public List<VaultControlRules> getBoxesWithoutRules() {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append("SELECT p FROM VaultControlRules p ");
		stringBuffer.append("WHERE p.expirationDate IS NULL AND p.indBoxForIssuers IS NULL ");
		stringBuffer.append("ORDER BY p.id ASC ");
		
		Query query = em.createQuery(stringBuffer.toString());
		
		return (List<VaultControlRules>) query.getResultList();
	}
	
	public void modifyBoxVaultControl(VaultControlRules vaultControlRule) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" BEGIN SP_MODIFY_BOX_VAULT_CONTROL(");
		stringBuffer.append(" 	:certificatesPerBox, ");
		stringBuffer.append(" 	:cabinet, ");
		stringBuffer.append(" 	:level, ");
		stringBuffer.append(" 	:box); ");
		stringBuffer.append(" 	END; ");

		Query query= em.createNativeQuery(stringBuffer.toString());
		
		query.setParameter("certificatesPerBox", vaultControlRule.getCertificatesPerBox());
		query.setParameter("cabinet", vaultControlRule.getCabinet());
		query.setParameter("level", vaultControlRule.getLevel());
		query.setParameter("box", vaultControlRule.getBox());
		
		query.executeUpdate();
	}
	
	public void modifyBoxVaultControlForActions(VaultControlRules vaultControlRule) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" BEGIN SP_MODIFY_BOX_VAULT_FOR_ACTION(");
		stringBuffer.append(" 	:certificatesPerBox, ");
		stringBuffer.append(" 	:issuersPerBox, ");
		stringBuffer.append(" 	:cabinet, ");
		stringBuffer.append(" 	:level, ");
		stringBuffer.append(" 	:box); ");
		stringBuffer.append(" 	END; ");

		Query query= em.createNativeQuery(stringBuffer.toString());
		
		query.setParameter("certificatesPerBox", vaultControlRule.getCertificatesPerBox());
		query.setParameter("issuersPerBox", vaultControlRule.getIssuersPerBox());
		query.setParameter("cabinet", vaultControlRule.getCabinet());
		query.setParameter("level", vaultControlRule.getLevel());
		query.setParameter("box", vaultControlRule.getBox());
		
		query.executeUpdate();
	}
	
	public void updateVaultControlRules(VaultControlRules vaultControlRules) throws ServiceException {
		this.update(vaultControlRules);
	}
	
	public List<VaultControlRules> getBoxesForActions() {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append("SELECT p FROM VaultControlRules p ");
		stringBuffer.append("WHERE p.indBoxForIssuers = :indBoxForIssuers ");
		stringBuffer.append("ORDER BY p.id ASC ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("indBoxForIssuers", 1);

		return (List<VaultControlRules>) query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<VaultControlComponent> getCertificatesPerBoxForIssuers(VaultControlRules filter) {
		//Calendar time = Calendar.getInstance();
		//time.setTime(fechaVencimiento);
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT VCC");
		//stringBuffer.append(" FROM VaultControlComponent VCC inner join VCC.vaultControlRules");
		stringBuffer.append(" FROM VaultControlComponent VCC WHERE ");
		//stringBuffer.append(" WHERE EXTRACT(YEAR FROM VCC.vaultControlRules.fechaVencimiento) = :fechaVencimientio  ");
		stringBuffer.append(" VCC.armario = :armario AND VCC.nivel = :nivel AND VCC.caja = :caja ");
		stringBuffer.append(" ORDER BY  VCC.armario ASC, VCC.nivel ASC, VCC.caja ASC, VCC.folio ASC, VCC.indiceCaja ASC ");
		//stringBuffer.append(" and HMB.availableBalance > 0 ");
		
		Query query= em.createQuery(stringBuffer.toString());
		//query.setParameter("fechaVencimientio", time.get(Calendar.YEAR));
		query.setParameter("armario", filter.getCabinet().toString());
		query.setParameter("nivel", filter.getLevel());
		query.setParameter("caja", filter.getBox());		
		
		return (List<VaultControlComponent>)query.getResultList();
		
	}
	
	public Long getFreeSpacePerBox(VaultControlRules vaultControlRules) {
		
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT count(*) FROM VaultControlComponent p ");
		stringBuffer.append(" WHERE p.armario = :cabinet ");
		stringBuffer.append(" AND p.nivel = :level ");
		stringBuffer.append(" AND p.caja = :box ");
		stringBuffer.append(" AND p.idSecurityCodeFk IS NULL ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("cabinet", vaultControlRules.getCabinet().toString());
		query.setParameter("level", vaultControlRules.getLevel());
		query.setParameter("box", vaultControlRules.getBox());

		return (Long) query.getSingleResult();
		
	}
	
	public void updateExpirationDate(VaultControlTO vaultControlTO) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" UPDATE REGLAS_BOVEDA SET FECHA_VENCIMIENTO = :expirationDate ");
		stringBuffer.append(" WHERE ARMARIO = :cabinet ");
		stringBuffer.append(" AND NIVEL = :level ");
		stringBuffer.append(" AND CAJA = :box ");
		
		Query query = em.createNativeQuery(stringBuffer.toString());
		query.setParameter("cabinet", vaultControlTO.getArmario());
		query.setParameter("level", vaultControlTO.getNivel());
		query.setParameter("box", vaultControlTO.getCaja());
		query.setParameter("expirationDate", vaultControlTO.getExpirationDate());

	}
	
}
