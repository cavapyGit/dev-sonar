package com.pradera.idepositary.businessintegration.component.webservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.transaction.TransactionSynchronizationRegistry;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.DepositarySetupBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.common.validation.to.ValidationOperationType;
import com.pradera.integration.common.validation.to.ValidationProcessTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.settlements.service.SettlementRemoteService;
import com.pradera.integration.component.settlements.to.SirtexOperationConsultResponseTO;
import com.pradera.integration.component.settlements.to.SirtexOperationConsultTO;
import com.pradera.integration.component.settlements.to.SirtexOperationStateResponseTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.remote.ThreadRemoteContextHolder;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionStateType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionType;

// TODO: Auto-generated Javadoc
/**
 * Project iDepositary.
 * 
 * The Class SirtexOperationResource.
 *
 * @author EDV.
 * @version 1.0 , 19/12/2017
 */
@Path("/SirtexOperationResource")
@Stateless
public class SirtexOperationResource implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The server configuration. */
	@Inject 
	@InjectableResource(location = "ServerConfiguration.properties")
	private Properties serverConfiguration;
	
	/** The log. */
	@Inject private PraderaLogger log;	
	
	/** The parameter service bean. */
	@EJB private ParameterServiceBean parameterServiceBean;
	
	/** The registry. */
	@Resource private TransactionSynchronizationRegistry registry;
	
	/** The settlement remote service. */
	@Inject private Instance<SettlementRemoteService> settlementRemoteService;
	
	/** The interface component service bean. */
	@Inject private Instance<InterfaceComponentService> interfaceComponentServiceBean;
	
	/** The audits user. */
	private String strAuditsUser;
	/** The audits IP address. */
	private String strIpAddress;
	
	/** The obj validation process to. */
	ValidationProcessTO objValidationProcessTO = null;
	
	/** The depositary setup bean. */
	@Inject
	DepositarySetupBean depositarySetupBean;

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		strAuditsUser = serverConfiguration.getProperty("webservice.audits.users.key");
		strIpAddress = serverConfiguration.getProperty("webservice.audits.ip");
	}
	
	/**
	 * SIRTEX CONSULT CSX
	 *
	 * @param xmlParam the xml param
	 * @return the response
	 */
	@Performance
	@POST
	@Path("/csx")
	@Produces(MediaType.APPLICATION_XML)
	public Response consultSirtexOperation(String xmlParam)
	{			
		log.info(":::: INICIO SERVICIO CSX ::::");		
		ProcessFileTO objProcessFileTO = null;		
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		SirtexOperationConsultTO sirtexOperationTO = null;
		StringBuilder xmlResponse= new StringBuilder();
		
		try {
			
			/*
			 * STEP 01:
			 * Parse the from XML file to TO object   
			 */
			
			log.info(":::: INICIO VALIDACION ARCHIVO ::::");
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(
																	ComponentConstant.INTERFACE_SIRTEX_OPERATION_CONSULT,
																	xmlParam, ComponentConstant.SIRTEX_OPERATION_CON, Boolean.FALSE);
			log.info(":::: FIN VALIDACION ARCHIVO ::::");
			
			/*
			 * STEP 02:
			 * Call the Remote service from TRA
			 */	
			
			log.info(":::: INICIO LLAMADA REMOTA ::::");	
			if(Validations.validateIsNotNull(objProcessFileTO)){
				Long idInterfaceProcess= interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(objProcessFileTO, 
																							ExternalInterfaceReceptionType.WEBSERVICE.getCode(), objLoggerUser);
				objValidationProcessTO = objProcessFileTO.getValidationProcessTO();
				for(ValidationOperationType objValidationOperationType : objValidationProcessTO.getLstOperations()){
					sirtexOperationTO = (SirtexOperationConsultTO) objValidationOperationType;
					sirtexOperationTO.setIdInterfaceProcess(idInterfaceProcess);
					sirtexOperationTO.setDpfInterface(true);
					objValidationProcessTO.getLstValidations().add(settlementRemoteService.get().sirtexOperationConsult(sirtexOperationTO));
				}
			}
			log.info(":::: FIN LLAMADA REMOTA ::::");
		} catch (Exception e) {
			e.printStackTrace();
			objValidationProcessTO= parameterServiceBean.generateErrorValidationProcess(sirtexOperationTO, e);
			processState= ExternalInterfaceReceptionStateType.FAILED.getCode();
		}finally{
			
			/*
			 * STEP 03:
			 * Generate the response XML file
			 */
			
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			if (Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {
				xmlResponse.append(interfaceComponentServiceBean.get().saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
															objValidationProcessTO.getLstValidations(), objProcessFileTO.getStreamResponseFileDir(), 
															ComponentConstant.INTERFACE_SIRTEX_TAG, ComponentConstant.INTERFACE_SIRTEX_OPERATION_TAG, 
															processState, objLoggerUser));
			} else {
				xmlResponse.append("NO HAY DATOS");
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");
		}
		log.info(":::: FIN SERVICIO CSX ::::");
		return Response.status(201).entity(xmlResponse.toString()).build();
	}
	
	
	/**
	 * SIRTEX CONSULT CSX
	 *
	 * @param xmlParam the xml param
	 * @return the response
	 */
	@Performance
	@POST
	@Path("/ros")
	@Produces(MediaType.APPLICATION_XML)
	public Response reviewSirtexOperation(String xmlParam)
	{			
		log.info(":::: INICIO SERVICIO ROS ::::");		
		ProcessFileTO objProcessFileTO = null;		
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		SirtexOperationConsultResponseTO sirtexOperationTO = null;
		StringBuilder xmlResponse= new StringBuilder();
		
		try {
			
			/*
			 * STEP 01:
			 * Parse the from XML file to TO object   
			 */
			
			log.info(":::: INICIO VALIDACION ARCHIVO ::::");
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(
																	ComponentConstant.INTERFACE_SIRTEX_OPERATION_REVIEW,
																	xmlParam, ComponentConstant.SIRTEX_OPERATION_REV, Boolean.FALSE);
			log.info(":::: FIN VALIDACION ARCHIVO ::::");
			
			/*
			 * STEP 02:
			 * Call the Remote service from ROS
			 */	
			
			log.info(":::: INICIO LLAMADA REMOTA ::::");	
			if(Validations.validateIsNotNull(objProcessFileTO)){
				Long idInterfaceProcess= interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(objProcessFileTO, 
																							ExternalInterfaceReceptionType.WEBSERVICE.getCode(), objLoggerUser);
				objValidationProcessTO = objProcessFileTO.getValidationProcessTO();
				for(ValidationOperationType objValidationOperationType : objValidationProcessTO.getLstOperations()){
					sirtexOperationTO = (SirtexOperationConsultResponseTO) objValidationOperationType;
					sirtexOperationTO.setIdInterfaceProcess(idInterfaceProcess);
					sirtexOperationTO.setDpfInterface(true);
					objValidationProcessTO.getLstValidations().add(settlementRemoteService.get().sirtexOperationReview(sirtexOperationTO,ComponentConstant.INTERFACE_SIRTEX_OPERATION_REVIEW));
				}
				
				List<RecordValidationType> listoProcess = new ArrayList<>();
				RecordValidationType recordValidationType = new RecordValidationType();
				SirtexOperationConsultResponseTO consultRespons = new SirtexOperationConsultResponseTO();
				List<SirtexOperationStateResponseTO> list2 = new ArrayList<>();
				
				for (RecordValidationType list : objValidationProcessTO.getLstValidations()){
					SirtexOperationConsultResponseTO test = (SirtexOperationConsultResponseTO)list.getOperationRef();
					list2.add(test.getSirtexStateOperations().get(0));
					recordValidationType.setEmptyProp(list.getEmptyProp());
					recordValidationType.setErrorCode(list.getErrorCode());
					recordValidationType.setErrorDescription(list.getErrorDescription());
				}
				consultRespons.setSirtexStateOperations(list2);
				recordValidationType.setOperationRef(consultRespons);
				listoProcess.add(recordValidationType);
				
				objValidationProcessTO.setLstValidations(listoProcess);
			}
			log.info(":::: FIN LLAMADA REMOTA ::::");
		} catch (Exception e) {
			e.printStackTrace();
			objValidationProcessTO= parameterServiceBean.generateErrorValidationProcess(sirtexOperationTO, e);
			processState= ExternalInterfaceReceptionStateType.FAILED.getCode();
		}finally{
			
			/*
			 * STEP 03:
			 * Generate the response XML file
			 */
			
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			if (Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {
				xmlResponse.append(interfaceComponentServiceBean.get().saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
															objValidationProcessTO.getLstValidations(), objProcessFileTO.getStreamResponseFileDir(), 
															ComponentConstant.INTERFACE_SIRTEX_STATE_TAG, ComponentConstant.INTERFACE_SIRTEX_OPERATION_TAG, 
															processState, objLoggerUser));
			} else {
				xmlResponse.append("NO HAY DATOS");
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");
		}
		log.info(":::: FIN SERVICIO CSX ::::");
		return Response.status(201).entity(xmlResponse.toString()).build();
	}
	
	/**
	 * SIRTEX CONSULT CSX
	 *
	 * @param xmlParam the xml param
	 * @return the response
	 */
	@Performance
	@POST
	@Path("/rox")
	@Produces(MediaType.APPLICATION_XML)
	public Response rejectSirtexOperation(String xmlParam)
	{			
		log.info(":::: INICIO SERVICIO ROS ::::");		
		ProcessFileTO objProcessFileTO = null;		
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		SirtexOperationConsultResponseTO sirtexOperationTO = null;
		StringBuilder xmlResponse= new StringBuilder();
		
		try {
			
			/*
			 * STEP 01:
			 * Parse the from XML file to TO object   
			 */
			
			log.info(":::: INICIO VALIDACION ARCHIVO ::::");
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(
																	ComponentConstant.INTERFACE_SIRTEX_OPERATION_REJECT,
																	xmlParam, ComponentConstant.SIRTEX_OPERATION_REJ, Boolean.FALSE);
			log.info(":::: FIN VALIDACION ARCHIVO ::::");
			
			/*
			 * STEP 02:
			 * Call the Remote service from ROS
			 */	
			
			log.info(":::: INICIO LLAMADA REMOTA ::::");	
			if(Validations.validateIsNotNull(objProcessFileTO)){
				Long idInterfaceProcess= interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(objProcessFileTO, 
																							ExternalInterfaceReceptionType.WEBSERVICE.getCode(), objLoggerUser);
				objValidationProcessTO = objProcessFileTO.getValidationProcessTO();
				for(ValidationOperationType objValidationOperationType : objValidationProcessTO.getLstOperations()){
					sirtexOperationTO = (SirtexOperationConsultResponseTO) objValidationOperationType;
					sirtexOperationTO.setIdInterfaceProcess(idInterfaceProcess);
					sirtexOperationTO.setDpfInterface(true);
					objValidationProcessTO.getLstValidations().add(settlementRemoteService.get().sirtexOperationReview(sirtexOperationTO,ComponentConstant.INTERFACE_SIRTEX_OPERATION_REJECT));
				}
				
				List<RecordValidationType> listoProcess = new ArrayList<>();
				RecordValidationType recordValidationType = new RecordValidationType();
				SirtexOperationConsultResponseTO consultRespons = new SirtexOperationConsultResponseTO();
				List<SirtexOperationStateResponseTO> list2 = new ArrayList<>();
				
				for (RecordValidationType list : objValidationProcessTO.getLstValidations()){
					SirtexOperationConsultResponseTO test = (SirtexOperationConsultResponseTO)list.getOperationRef();
					list2.add(test.getSirtexStateOperations().get(0));
					recordValidationType.setEmptyProp(list.getEmptyProp());
					recordValidationType.setErrorCode(list.getErrorCode());
					recordValidationType.setErrorDescription(list.getErrorDescription());
				}
				consultRespons.setSirtexStateOperations(list2);
				recordValidationType.setOperationRef(consultRespons);
				listoProcess.add(recordValidationType);
				
				objValidationProcessTO.setLstValidations(listoProcess);
			}
			log.info(":::: FIN LLAMADA REMOTA ::::");
		} catch (Exception e) {
			e.printStackTrace();
			objValidationProcessTO= parameterServiceBean.generateErrorValidationProcess(sirtexOperationTO, e);
			processState= ExternalInterfaceReceptionStateType.FAILED.getCode();
		}finally{
			
			/*
			 * STEP 03:
			 * Generate the response XML file
			 */
			
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			if (Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {
				xmlResponse.append(interfaceComponentServiceBean.get().saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
															objValidationProcessTO.getLstValidations(), objProcessFileTO.getStreamResponseFileDir(), 
															ComponentConstant.INTERFACE_SIRTEX_STATE_TAG, ComponentConstant.INTERFACE_SIRTEX_OPERATION_TAG, 
															processState, objLoggerUser));
			} else {
				xmlResponse.append("NO HAY DATOS");
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");
		}
		log.info(":::: FIN SERVICIO CSX ::::");
		return Response.status(201).entity(xmlResponse.toString()).build();
	}
	
	
	/**
	 * SIRTEX CONSULT CSX
	 *
	 * @param xmlParam the xml param
	 * @return the response
	 */
	@Performance
	@POST
	@Path("/cos")
	@Produces(MediaType.APPLICATION_XML)
	public Response confirmSirtexOperation(String xmlParam)
	{			
		log.info(":::: INICIO SERVICIO ROS ::::");		
		ProcessFileTO objProcessFileTO = null;		
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		SirtexOperationConsultResponseTO sirtexOperationTO = null;
		StringBuilder xmlResponse= new StringBuilder();
		
		try {
			
			/*
			 * STEP 01:
			 * Parse the from XML file to TO object   
			 */
			
			log.info(":::: INICIO VALIDACION ARCHIVO ::::");
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(
																	ComponentConstant.INTERFACE_SIRTEX_OPERATION_CONFIRM,
																	xmlParam, ComponentConstant.SIRTEX_OPERATION_COS, Boolean.FALSE);
			log.info(":::: FIN VALIDACION ARCHIVO ::::");
			
			/*
			 * STEP 02:
			 * Call the Remote service from ROS
			 */	
			
			log.info(":::: INICIO LLAMADA REMOTA ::::");	
			if(Validations.validateIsNotNull(objProcessFileTO)){
				Long idInterfaceProcess= interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(objProcessFileTO, 
																							ExternalInterfaceReceptionType.WEBSERVICE.getCode(), objLoggerUser);
				objValidationProcessTO = objProcessFileTO.getValidationProcessTO();
				for(ValidationOperationType objValidationOperationType : objValidationProcessTO.getLstOperations()){
					sirtexOperationTO = (SirtexOperationConsultResponseTO) objValidationOperationType;
					sirtexOperationTO.setIdInterfaceProcess(idInterfaceProcess);
					sirtexOperationTO.setDpfInterface(true);
					objValidationProcessTO.getLstValidations().add(settlementRemoteService.get().sirtexOperationReview(sirtexOperationTO,ComponentConstant.INTERFACE_SIRTEX_OPERATION_CONFIRM));
				}
				
				List<RecordValidationType> listoProcess = new ArrayList<>();
				RecordValidationType recordValidationType = new RecordValidationType();
				SirtexOperationConsultResponseTO consultRespons = new SirtexOperationConsultResponseTO();
				List<SirtexOperationStateResponseTO> list2 = new ArrayList<>();
				
				for (RecordValidationType list : objValidationProcessTO.getLstValidations()){
					SirtexOperationConsultResponseTO test = (SirtexOperationConsultResponseTO)list.getOperationRef();
					list2.add(test.getSirtexStateOperations().get(0));
					recordValidationType.setEmptyProp(list.getEmptyProp());
					recordValidationType.setErrorCode(list.getErrorCode());
					recordValidationType.setErrorDescription(list.getErrorDescription());
				}
				consultRespons.setSirtexStateOperations(list2);
				recordValidationType.setOperationRef(consultRespons);
				listoProcess.add(recordValidationType);
				
				objValidationProcessTO.setLstValidations(listoProcess);
			}
			log.info(":::: FIN LLAMADA REMOTA ::::");
		} catch (Exception e) {
			e.printStackTrace();
			objValidationProcessTO= parameterServiceBean.generateErrorValidationProcess(sirtexOperationTO, e);
			processState= ExternalInterfaceReceptionStateType.FAILED.getCode();
		}finally{
			
			/*
			 * STEP 03:
			 * Generate the response XML file
			 */
			
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			if (Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {
				xmlResponse.append(interfaceComponentServiceBean.get().saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
															objValidationProcessTO.getLstValidations(), objProcessFileTO.getStreamResponseFileDir(), 
															ComponentConstant.INTERFACE_SIRTEX_STATE_TAG, ComponentConstant.INTERFACE_SIRTEX_OPERATION_TAG, 
															processState, objLoggerUser));
			} else {
				xmlResponse.append("NO HAY DATOS");
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");
		}
		log.info(":::: FIN SERVICIO CSX ::::");
		return Response.status(201).entity(xmlResponse.toString()).build();
	}
	
	
}