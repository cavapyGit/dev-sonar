package com.pradera.idepositary.businessintegration.component.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.AccessTimeout;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import com.pradera.commons.contextholder.interceptor.RemoteLoggerUserInterceptor;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.SecuritiesComponentService;
import com.pradera.integration.component.business.to.IssuanceComponentTO;
import com.pradera.integration.component.business.to.PrimaryPlacementComponentTO;
import com.pradera.integration.component.business.to.SecuritiesComponentTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.component.IssuanceMovement;
import com.pradera.model.component.IssuanceOperation;
import com.pradera.model.component.MovementBehavior;
import com.pradera.model.component.MovementType;
import com.pradera.model.component.SecuritiesMovement;
import com.pradera.model.component.SecuritiesOperation;
import com.pradera.model.component.type.ParameterBalanceType;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.SecurityHistoryBalance;
import com.pradera.model.issuancesecuritie.type.IssuanceStateType;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class SecuritiesComponentServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 30/03/2014
 */
@Singleton
public class SecuritiesComponentServiceBean extends CrudDaoServiceBean implements SecuritiesComponentService {
	
	/** The logger. */
	@Inject
	private PraderaLogger logger;
	
	/** The additional component service bean. */
	@EJB
	AdditionalComponentServiceBean additionalComponentServiceBean;

	@Resource
	SessionContext sessionContext;

    /**
     * Default constructor. 
     */
    public SecuritiesComponentServiceBean() {
        // TODO Auto-generated constructor stub
    }
    
	/* (non-Javadoc)
	 * @see com.pradera.integration.component.business.service.SecuritiesComponentService#executeSecuritiesComponent(com.pradera.integration.component.business.to.SecuritiesComponentTO)
	 */
	@Override
	@AccessTimeout(unit=TimeUnit.MINUTES,value=5)
    @Interceptors(RemoteLoggerUserInterceptor.class)
	public void executeSecuritiesComponent(SecuritiesComponentTO objSecuritiesComponentTO) throws ServiceException {
		// TODO Auto-generated method stub
		try {
			/*
			 * STEP 01: 
			 * To get the list of movement types to affect the security
			 * To this it's required: the business process and the operation type
			 */
			List<MovementType> lstMovementTypes = additionalComponentServiceBean.getListMovementTypes(objSecuritiesComponentTO.getIdBusinessProcess(), 
																	objSecuritiesComponentTO.getIdOperationType(), null, objSecuritiesComponentTO.getIndSourceTarget());
		
			if (Validations.validateListIsNullOrEmpty(lstMovementTypes)) 
			{
				logger.error("idBusinessProcess: " +objSecuritiesComponentTO.getIdBusinessProcess()
									+ ", idOperationType: " +objSecuritiesComponentTO.getIdOperationType()
									+ ", indSourceTarget: " +objSecuritiesComponentTO.getIndSourceTarget());
				throw new ServiceException(ErrorServiceType.SECURITIES_MOVEMENT_TYPE_NOFOUND, ErrorServiceType.SECURITIES_MOVEMENT_TYPE_NOFOUND.getMessage());
			}
			/*
			 * STEP 02:
			 * To generate the securities movements for the security
			 */
			if (Validations.validateListIsNotNullAndNotEmpty(lstMovementTypes)){
				generateSecuritiesMovements(objSecuritiesComponentTO, lstMovementTypes, objSecuritiesComponentTO.getIdSecuritiesOperation());
			}
			
			/*
			 * STEP 03:
			 * To update the securities balances 
			 */
			if (Validations.validateListIsNotNullAndNotEmpty(lstMovementTypes)){
				updateSecuritiesBalance(objSecuritiesComponentTO, lstMovementTypes);
			}
			
			// verificando si se realizar el llamado al componente de emision
			// enviada en objeto ObjSecuritiesComponentTO
			if(objSecuritiesComponentTO.isIssuanceExecute())
			{
				System.out.println(" objSecuritiesComponentTO.isIssuanceExecute() ");
				/*
				 * STEP 04:
				 * To execute issuance component
				 */
				System.out.println(" objSecuritiesComponentTO.getIdOperationType():: "+objSecuritiesComponentTO.getIdOperationType());
				// agregar la validacion para que se pueda o no ejecutar segun un flag enviado en el objeto objSecuritiesComponentTO
				Long operationTypeCascadeId = additionalComponentServiceBean.getOperationTypeCascadeId(objSecuritiesComponentTO.getIdOperationType());
				if (Validations.validateIsNotNull(operationTypeCascadeId))
				{
					System.out.println(" Validations.validateIsNotNull(operationTypeCascadeId) ");
					createOperationOnCascade(objSecuritiesComponentTO, operationTypeCascadeId, objSecuritiesComponentTO.getIdBusinessProcess());
				}
			}
			
			/*
			 * STEP 05:
			 * To create security history balance
			 */
			if(Validations.validateIsNotNull(objSecuritiesComponentTO) && Validations.validateIsNotNull(objSecuritiesComponentTO.getIdSecurityCode())){
				createSecurityHistoryBalance(objSecuritiesComponentTO.getIdSecurityCode());
			}
			
		} catch (ServiceException e) {
			sessionContext.setRollbackOnly();
			logger.error("idIsinCode: " +objSecuritiesComponentTO.getIdSecurityCode()
								+", idBusinessProcess: " +objSecuritiesComponentTO.getIdBusinessProcess()
								+", idOperationType: " +objSecuritiesComponentTO.getIdOperationType()
								+", indSourceTarget: " +objSecuritiesComponentTO.getIndSourceTarget()
								+", dematerializedBalance: " +objSecuritiesComponentTO.getDematerializedBalance()
								+", physicalBalance: " +objSecuritiesComponentTO.getPhysicalBalance()
								+", placedBalance: " +objSecuritiesComponentTO.getPlacedBalance()
								+", circulationBalance: " +objSecuritiesComponentTO.getCirculationBalance()
								+", shareBalance: " +objSecuritiesComponentTO.getShareBalance());
			throw e;
		}
	}
	/**
	 * Create Security History Balance
	 * @param idSecurityCodePk security pk
	 * @throws ServiceException the Service Exception
	 */
	public void createSecurityHistoryBalance(String idSecurityCodePk) throws ServiceException{
		//SAVE SECURITY HISTORY BALANCE
		SecurityHistoryBalance securityHistoryBalance = new SecurityHistoryBalance();
		Security objSecurity = additionalComponentServiceBean.getSecurities(idSecurityCodePk);
		securityHistoryBalance.setSecurity(new Security());
		securityHistoryBalance.getSecurity().setIdSecurityCodePk(objSecurity.getIdSecurityCodePk());
		securityHistoryBalance.setUpdateBalanceDate(CommonsUtilities.currentDateTime());
		securityHistoryBalance.setNominalValue(objSecurity.getCurrentNominalValue());
		securityHistoryBalance.setPlacedAmount(objSecurity.getPlacedAmount());
		securityHistoryBalance.setPlacedBalance(objSecurity.getPlacedBalance());
		securityHistoryBalance.setShareCapital(objSecurity.getShareCapital());
		securityHistoryBalance.setShareBalance(objSecurity.getShareBalance());
		securityHistoryBalance.setPhysicalBalance(objSecurity.getPhysicalBalance());
		securityHistoryBalance.setDesmaterializedBalance(objSecurity.getDesmaterializedBalance());
		securityHistoryBalance.setAmortizationAmount(objSecurity.getAmortizationAmount());
		create(securityHistoryBalance);
	}
	/**
	 * Method to generate the securities movements according the list of movement types.
	 *
	 * @param objSecuritiesComponent the obj securities component
	 * @param lstMovementTypes the lst movement types
	 * @param idSecuritiesOperation the id securities operation
	 * @throws ServiceException the service exception
	 */
	private void generateSecuritiesMovements(SecuritiesComponentTO objSecuritiesComponent, List<MovementType> lstMovementTypes, Long idSecuritiesOperation) throws ServiceException{
		Object[] objSecuritiesOperationData = additionalComponentServiceBean.getSecuritiesOperationData(idSecuritiesOperation);
		for (int j=0; j<lstMovementTypes.size(); ++j)
		{
			MovementType objmMovementType = lstMovementTypes.get(j);
			SecuritiesMovement objSecuritiesMovement = new SecuritiesMovement();
			objSecuritiesMovement.setSecuritiesOperation(new SecuritiesOperation((Long)objSecuritiesOperationData[0]));
			objSecuritiesMovement.setSecurities(new Security((String)objSecuritiesOperationData[3]));
			objSecuritiesMovement.setNominalValue((BigDecimal)objSecuritiesOperationData[4]);
			objSecuritiesMovement.setMovementType(objmMovementType);
			objSecuritiesMovement.setMovementQuantity((BigDecimal)objSecuritiesOperationData[5]);
//			if (objmMovementType.getReferenceBalanceType().equals(ParameterBalanceType.PHYSICAL_BALANCE.getCode()))
//				objSecuritiesMovement.setMovementQuantity(objSecuritiesComponent.getPhysicalBalance());
//			else if (objmMovementType.getReferenceBalanceType().equals(ParameterBalanceType.DEMATERIALIZED_BALANCE.getCode()))
//				objSecuritiesMovement.setMovementQuantity(objSecuritiesComponent.getDematerializedBalance());
//			else if (objmMovementType.getReferenceBalanceType().equals(ParameterBalanceType.SHARE_BALANCE.getCode()))
//				objSecuritiesMovement.setMovementQuantity(objSecuritiesComponent.getShareBalance());
//			else if (objmMovementType.getReferenceBalanceType().equals(ParameterBalanceType.CIRCULATION_BALANCE.getCode()))
//				objSecuritiesMovement.setMovementQuantity(objSecuritiesComponent.getCirculationBalance());
//			else if (objmMovementType.getReferenceBalanceType().equals(ParameterBalanceType.PLACED_BALANCE.getCode()))
//				objSecuritiesMovement.setMovementQuantity(objSecuritiesComponent.getPlacedBalance());
			objSecuritiesMovement.setMovementDate(CommonsUtilities.currentDateTime());
			//saving the securities movement
			this.create(objSecuritiesMovement);
		}
	}
	
	/**
	 * Method to update the securities balances according the list of movement types.
	 *
	 * @param objSecuritiesComponentTO the obj securities component to
	 * @param lstMovementTypes the lst movement types
	 * @throws ServiceException the service exception
	 */
	private void updateSecuritiesBalance(SecuritiesComponentTO objSecuritiesComponentTO, List<MovementType> lstMovementTypes) throws ServiceException{
		try {
			Security objSecurity = additionalComponentServiceBean.getSecurities(objSecuritiesComponentTO.getIdSecurityCode());
			
			for (int j=0; j<lstMovementTypes.size(); ++j)
			{
				MovementType objMovementType = lstMovementTypes.get(j);
				// we get the movement behaviors to affects the corresponding balances inside the securities 
				List<MovementBehavior> lstMovementBehaviors = additionalComponentServiceBean.getListMovementBehavior(objMovementType.getIdMovementTypePk());
				for (int k=0; k<lstMovementBehaviors.size(); ++k)
				{
					
					
					MovementBehavior objMovementBehavior = lstMovementBehaviors.get(k);
					final int balanceType = objMovementBehavior.getBalanceType().getIdBalanceTypePk().intValue();
					switch (balanceType) {
						case ComponentConstant.SHARE_BALANCE:
							//abro para que siempre sume y reste
							//if( objSecurity.getIndIsManaged() != null && objSecurity.getIndIsManaged().equals(1) && 
							//	objSecurity.getIssuanceForm()!= null && objSecurity.getIssuanceForm().equals(IssuanceType.DEMATERIALIZED.getCode()) ){
								logger.debug("Balance to modify :" + ParameterBalanceType.SHARE_BALANCE.getDescripcion());
								objSecurity.setShareBalance(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
										objSecurity.getShareBalance(), objSecuritiesComponentTO.getShareBalance(),balanceType));
								objSecurity.setShareCapital(objSecurity.getCurrentNominalValue().multiply(objSecurity.getShareBalance()));
							//}
						break;
						case ComponentConstant.CIRCULATION_BALANCE:
							//abro para que siempre sume y reste
							//if( objSecurity.getIndIsManaged() != null && objSecurity.getIndIsManaged().equals(1) && 
							//	objSecurity.getIssuanceForm()!= null && objSecurity.getIssuanceForm().equals(IssuanceType.DEMATERIALIZED.getCode()) ){
								logger.debug("Balance to modify :" + ParameterBalanceType.CIRCULATION_BALANCE.getDescripcion());
								objSecurity.setCirculationBalance(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
										objSecurity.getCirculationBalance(), objSecuritiesComponentTO.getCirculationBalance(),balanceType));
								objSecurity.setCirculationAmount(objSecurity.getCurrentNominalValue().multiply(objSecurity.getCirculationBalance()));
							//}
						break;
						case ComponentConstant.PLACED_BALANCE:
							//abro para que siempre sume y reste
							//if( objSecurity.getIssuanceForm()!= null && objSecurity.getIssuanceForm().equals(IssuanceType.DEMATERIALIZED.getCode()) ){
								if(objSecurity.getPlacedBalance() == null){
									objSecurity.setPlacedBalance(BigDecimal.ZERO);
									objSecurity.setPlacedAmount(BigDecimal.ZERO);
								}
								logger.debug("Balance to modify :" + ParameterBalanceType.PLACED_BALANCE.getDescripcion());
								objSecurity.setPlacedBalance(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
										objSecurity.getPlacedBalance(), objSecuritiesComponentTO.getPlacedBalance(),balanceType));
								objSecurity.setPlacedAmount(objSecurity.getCurrentNominalValue().multiply(objSecurity.getPlacedBalance()));
							//}
						break;
						//se comento este campo para que no afecte el saldo desmaterializado y fisico - se descomento
						case ComponentConstant.PHYSICAL_BALANCE:
							logger.debug("Balance to modify :" + ParameterBalanceType.PHYSICAL_BALANCE.getDescripcion());
							objSecurity.setPhysicalBalance(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
									objSecurity.getPhysicalBalance(), objSecuritiesComponentTO.getPhysicalBalance(), balanceType));
						break;
						case ComponentConstant.DEMATERIALIZED_BALANCE:
							//abro para que siempre sume y reste
							//if(objSecurity.getIndIsManaged() != null && objSecurity.getIndIsManaged().equals(1)){
								logger.debug("security is managed - Balance to modify :" + ParameterBalanceType.DEMATERIALIZED_BALANCE.getDescripcion());
								objSecurity.setDesmaterializedBalance(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
										objSecurity.getDesmaterializedBalance(), objSecuritiesComponentTO.getDematerializedBalance(), balanceType));
							/*}else {
								logger.debug("security not managed - Balance no modify :" + ParameterBalanceType.DEMATERIALIZED_BALANCE.getDescripcion());
							}*/
						break;
					}
				
					
				}

				if (objSecurity.getShareBalance().compareTo(BigDecimal.ZERO) == 0) {
					if(objSecuritiesComponentTO.isEarlyPayment()){
						objSecurity.setStateSecurity(SecurityStateType.REDEEMED.getCode());
//						objSecurity.setExpirationDate(CommonsUtilities.currentDate());
					} else if(objSecuritiesComponentTO.isSecuritiesRenew()){
						objSecurity.setStateSecurity(SecurityStateType.EXPIRED.getCode());
					}
				}
				//Added by reversion early payment
				else{
					if(objSecuritiesComponentTO.isReverseEarlyPayment()){
						objSecurity.setStateSecurity(SecurityStateType.REGISTERED.getCode());
					}
				}
				this.update(objSecurity);
			}
		} catch (ServiceException objException){
			sessionContext.setRollbackOnly();
			logger.error("updateSecuritiesBalance");
			logger.debug(objException.getMessage());
			throw objException;
		}
	}
	
	/**
	 * Method to create cascade operation for securities in issuance.
	 *
	 * @param objSecuritiesComponentTO the obj securities component to
	 * @param operationTypeCascadeId the obj operation type
	 * @param idBusinessProcess the id business process
	 * @throws ServiceException the service exception
	 */
	private void createOperationOnCascade(SecuritiesComponentTO objSecuritiesComponentTO, Long operationTypeCascadeId, Long idBusinessProcess) throws ServiceException{
		try {
			System.out.println("createOperationOnCascade:: operationTypeCascadeId:"+operationTypeCascadeId+ ", idBusinessProcess:"+idBusinessProcess);
			Object[] objSecuritiesOperationData = additionalComponentServiceBean.getSecuritiesOperationData(objSecuritiesComponentTO.getIdSecuritiesOperation());
			IssuanceOperation objIssuanceOperation = new IssuanceOperation();
			objIssuanceOperation.setCashAmount((BigDecimal)objSecuritiesOperationData[1]);
			objIssuanceOperation.setIssuance(find(Issuance.class,(String)objSecuritiesOperationData[2]));
			//DPA - issue 158
			// SOLO PARA EMISIONES DE DPF's Y DPA's
			if((objIssuanceOperation.getIssuance().getSecurityClass().equals(SecurityClassType.DPA.getCode()) ||
					objIssuanceOperation.getIssuance().getSecurityClass().equals(SecurityClassType.DPF.getCode())) 
				&& operationTypeCascadeId.equals(ParameterOperationType.SEC_EARLY_REDEMPTION.getCode())){
				objIssuanceOperation.getIssuance().setStateIssuance(IssuanceStateType.EXPIRED.getCode());
			}else{
				if(operationTypeCascadeId.equals(ParameterOperationType.SEC_EARLY_REDEMPTION_REVERSION.getCode())){
					objIssuanceOperation.getIssuance().setStateIssuance(IssuanceStateType.AUTHORIZED.getCode());
				}
			}
			
			objIssuanceOperation.setOperationDate(CommonsUtilities.currentDateTime());
			objIssuanceOperation.setOperationType(operationTypeCascadeId);
			objIssuanceOperation.setSecuritiesOperation(new SecuritiesOperation((Long)objSecuritiesOperationData[0]));
			//save the issuance operation
			additionalComponentServiceBean.create(objIssuanceOperation);
			
			//now we have to excecute the issuance component
			IssuanceComponentTO objIssuanceComponentTO = new IssuanceComponentTO();
			objIssuanceComponentTO.setCashAmount(objIssuanceOperation.getCashAmount());
			objIssuanceComponentTO.setIdBusinessProcess(idBusinessProcess);
			objIssuanceComponentTO.setIdIssuance(objIssuanceOperation.getIssuance().getIdIssuanceCodePk());
			objIssuanceComponentTO.setIdIssuanceOperation(objIssuanceOperation.getIdIssuanceOperationPk());
			objIssuanceComponentTO.setIdOperationType(operationTypeCascadeId);
			objIssuanceComponentTO.setIndSourceTarget(objSecuritiesComponentTO.getIndSourceTarget());
			
			executeIssuanceComponent(objIssuanceComponentTO);
			System.out.println("executeIssuanceComponent:: IssuanceOperation:"+objIssuanceOperation.getIdIssuanceOperationPk()+", objIssuanceComponentTO:"+operationTypeCascadeId+ ", idBusinessProcess:"+idBusinessProcess);
			
		} catch (ServiceException objException){
			sessionContext.setRollbackOnly();
			logger.error(("createOperationOnCascade"));
			logger.debug(objException.getMessage());
			throw objException;
		}
	}
	
	/**
	 * Main method to generate the movements and update the balances for issuance  .
	 *
	 * @param objIssuanceComponentTO the obj issuance component to
	 * @throws ServiceException the service exception
	 */
	private void executeIssuanceComponent(IssuanceComponentTO objIssuanceComponentTO) throws ServiceException {
		/*
		 * STEP 01: 
		 * To get the list of movement types to affect the issuance
		 * To this it's required: the business process and the operation type
		 */
		List<MovementType> lstMovementTypes = additionalComponentServiceBean.getListMovementTypes(objIssuanceComponentTO.getIdBusinessProcess(), 
																	objIssuanceComponentTO.getIdOperationType(), null, objIssuanceComponentTO.getIndSourceTarget());
	
		if (Validations.validateListIsNullOrEmpty(lstMovementTypes)) 
		{
			logger.error("idBusinessProcess: " +objIssuanceComponentTO.getIdBusinessProcess()
								+ ", idOperationType: " +objIssuanceComponentTO.getIdOperationType()
								+ ", indSourceTarget: " +objIssuanceComponentTO.getIndSourceTarget());
			throw new ServiceException(ErrorServiceType.ISSUANCE_MOVEMENT_TYPE_NOFOUND, ErrorServiceType.ISSUANCE_MOVEMENT_TYPE_NOFOUND.getMessage());
		}
		
		/*
		 * STEP 02:
		 * To generate the securities movements for the security
		 */
		if (Validations.validateListIsNotNullAndNotEmpty(lstMovementTypes))
			generateIssuanceMovements(objIssuanceComponentTO, lstMovementTypes);
		
		/*
		 * STEP 03:
		 * To update the securities balances 
		 */
		if (Validations.validateListIsNotNullAndNotEmpty(lstMovementTypes)){
			updateIssuanceAmounts(objIssuanceComponentTO, lstMovementTypes);
		}
		
	}
	
	/**
	 * Method to generate the issuance movements according the list of movement types .
	 *
	 * @param objIssuanceComponentTO the obj issuance component to
	 * @param lstMovementTypes the lst movement types
	 * @throws ServiceException the service exception
	 */
	private void generateIssuanceMovements(IssuanceComponentTO objIssuanceComponentTO, List<MovementType> lstMovementTypes) throws ServiceException{
		IssuanceOperation objIssuanceOperation = additionalComponentServiceBean.getIssuanceOperation(objIssuanceComponentTO.getIdIssuanceOperation());
		for (int i=0; i<lstMovementTypes.size(); ++i){
			MovementType objMovementType = lstMovementTypes.get(i);
			IssuanceMovement objIssuanceMovement = new IssuanceMovement();
			objIssuanceMovement.setIssuance(objIssuanceOperation.getIssuance());
			objIssuanceMovement.setIssuanceOperation(objIssuanceOperation);
			objIssuanceMovement.setMovementAmount(objIssuanceOperation.getCashAmount());
			objIssuanceMovement.setMovementDate(CommonsUtilities.currentDateTime());
			objIssuanceMovement.setMovementType(objMovementType);
			// save the issuance movement
			this.create(objIssuanceMovement);
		}
	}
	
	@Override
	@AccessTimeout(unit=TimeUnit.MINUTES,value=5)
    @Interceptors(RemoteLoggerUserInterceptor.class)
    public void updatePrimaryPlacement(PrimaryPlacementComponentTO primaryPlacementComponentTO) throws ServiceException{
    	
    	additionalComponentServiceBean.updatePrimaryPlacement(primaryPlacementComponentTO);
    	
    }
	
	/**
	 * Method to update the issuance balances according the list of movement types.
	 *
	 * @param objIssuanceComponentTO the obj issuance component to
	 * @param lstMovementTypes the lst movement types
	 * @throws ServiceException the service exception
	 */
	private void updateIssuanceAmounts(IssuanceComponentTO objIssuanceComponentTO, List<MovementType> lstMovementTypes) throws ServiceException	{
		try {
			Issuance objIssuance = additionalComponentServiceBean.getIssuance(objIssuanceComponentTO.getIdIssuance());
			for (int i=0; i<lstMovementTypes.size(); ++i)
			{
				MovementType objMovementType = lstMovementTypes.get(i);
				// we get the movement behaviors to affects the corresponding balances inside the securities 
				List<MovementBehavior> lstMovementBehaviors = additionalComponentServiceBean.getListMovementBehavior(objMovementType.getIdMovementTypePk());
				for (int j=0; j<lstMovementBehaviors.size(); ++j)
				{
					MovementBehavior objMovementBehavior = lstMovementBehaviors.get(j);
					final int balanceType = objMovementBehavior.getBalanceType().getIdBalanceTypePk().intValue();
					switch (balanceType) {
						case ComponentConstant.ISSUE_AMOUNT:
							logger.debug("Balance to modify :" + ParameterBalanceType.ISSUE_AMOUNT.getDescripcion());
							objIssuance.setIssuanceAmount(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
									objIssuance.getIssuanceAmount(), objIssuanceComponentTO.getCashAmount(), balanceType));
						break;
						case ComponentConstant.PLACED_AMOUNT:
							logger.debug("Balance to modify :" + ParameterBalanceType.PLACED_AMOUNT.getDescripcion());
							objIssuance.setPlacedAmount(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
									objIssuance.getPlacedAmount(), objIssuanceComponentTO.getCashAmount(), balanceType));
						break;
						case ComponentConstant.AMORTIZED_AMOUNT:
							logger.debug("Balance to modify :" + ParameterBalanceType.AMORTIZED_AMOUNT.getDescripcion());
							objIssuance.setAmortizationAmount(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
									objIssuance.getAmortizationAmount(), objIssuanceComponentTO.getCashAmount(), balanceType));
						break;
						case ComponentConstant.CIRCULATION_AMOUNT:
							logger.debug("Balance to modify :" + ParameterBalanceType.CIRCULATION_AMOUNT.getDescripcion());
							objIssuance.setCirculationAmount(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
									objIssuance.getCirculationAmount(), objIssuanceComponentTO.getCashAmount(), balanceType));
						break;
					}
				}
			}
			this.update(objIssuance);
			em.flush();
		} catch (ServiceException objException){
			sessionContext.setRollbackOnly();
			logger.error(("updateIssuanceAmounts"));
			logger.debug(objException.getMessage());
			throw objException;
		}
	}

}
