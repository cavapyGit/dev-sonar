package com.pradera.idepositary.businessintegration.component.webservice.filters;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import com.pradera.commons.logging.PraderaLogger;

public class CORSFilter implements Filter {

	@Inject
	private PraderaLogger log;
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse     servletResponse, FilterChain filterChain) 
		throws IOException, ServletException {
		// TODO Auto-generated method stub
		final HttpServletResponse response = (HttpServletResponse) servletResponse;
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, HEAD, OPTIONS");
        response.setHeader("Access-Control-Allow-Headers", "Origin, Accept, x-auth-token, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
        
        log.info("Access-Control-Allow-Origin : "+response.getHeader("Access-Control-Allow-Origin"));
        log.info("Access-Control-Allow-Credentials : "+response.getHeader("Access-Control-Allow-Credentials"));
        log.info("Access-Control-Allow-Methods : "+response.getHeader("Access-Control-Allow-Methods"));
        log.info("Access-Control-Allow-Headers : "+response.getHeader("Access-Control-Allow-Headers"));
        log.info("CORSFilter -> Sucess Setting headers Acces-Control");
        
        filterChain.doFilter(servletRequest, servletResponse);
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

}