package com.pradera.idepositary.businessintegration.component.service;

import java.math.BigDecimal;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.custody.splitcoupons.SplitCouponRequest;
import com.pradera.model.custody.splitcouponstype.SplitCouponStateType;
import com.pradera.model.process.BusinessProcess;

@Stateless
@Performance
public class SplitCouponNotificatorServiceBean extends CrudDaoServiceBean{
	
	@Inject
	NotificationServiceFacade notificationServiceFacade;
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	public void init(@Observes HolderAccountBalanceTO holderAccountBalanceTO) {
		List<SplitCouponRequest> lstResult = getSplitCouponRequest(holderAccountBalanceTO);		
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
			BigDecimal availableBalance = getAvailableBalance(holderAccountBalanceTO);
			if(availableBalance.compareTo(BigDecimal.ZERO) > 0){
				LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
				for(SplitCouponRequest splitCouponRequest:lstResult){
					BigDecimal operationQuantity = splitCouponRequest.getSplitCouponOperations().get(0).getOperationQuantity();
					if(availableBalance.compareTo(operationQuantity) >= 0){
						BusinessProcess businessProcess = new BusinessProcess();
						businessProcess.setIdBusinessProcessPk(BusinessProcessType.SPECIAL_REBLOCK_PROCESS.getCode());
						notificationServiceFacade.sendNotification(loggerUser.getUserName(), businessProcess, null
														, new Object[]{splitCouponRequest.getIdSplitOperationPk().toString()});
						availableBalance = availableBalance.subtract(operationQuantity);
					}
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private List<SplitCouponRequest> getSplitCouponRequest(HolderAccountBalanceTO holderAccountBalanceTO){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT scr");
		sbQuery.append("	FROM SplitCouponRequest scr");
		sbQuery.append("	INNER JOIN FETCH scr.splitCouponOperations");
		sbQuery.append("	INNER JOIN FETCH scr.custodyOperation co");
		sbQuery.append("	WHERE scr.participant.idParticipantPk = :idParticipantPk");
		sbQuery.append("	AND scr.holderAccount.idHolderAccountPk = :idHolderAccountPk");
		sbQuery.append("	AND scr.security.idSecurityCodePk = :idSecurityCodePk");
		sbQuery.append("	AND scr.requestState = :requestState");
		sbQuery.append("	AND co.operationType = :operationType");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idParticipantPk", holderAccountBalanceTO.getIdParticipant());
		query.setParameter("idHolderAccountPk", holderAccountBalanceTO.getIdHolderAccount());
		query.setParameter("idSecurityCodePk", holderAccountBalanceTO.getIdSecurityCode());
		query.setParameter("requestState", SplitCouponStateType.APPROVED.getCode());
		query.setParameter("operationType", ParameterOperationType.SECURITIES_DETACHMENT_NOT_BLOCKED.getCode());
		return (List<SplitCouponRequest>)query.getResultList();
	}
	
	private BigDecimal getAvailableBalance(HolderAccountBalanceTO holderAccountBalanceTO){
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT availableBalance");
			sbQuery.append("	FROM HolderAccountBalance hab");
			sbQuery.append("	WHERE hab.participant.idParticipantPk = :idParticipantPk");
			sbQuery.append("	AND hab.holderAccount.idHolderAccountPk = :idHolderAccountPk");
			sbQuery.append("	AND hab.security.idSecurityCodePk = :idSecurityCodePk");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idParticipantPk", holderAccountBalanceTO.getIdParticipant());
			query.setParameter("idHolderAccountPk", holderAccountBalanceTO.getIdHolderAccount());
			query.setParameter("idSecurityCodePk", holderAccountBalanceTO.getIdSecurityCode());
			return (BigDecimal)query.getSingleResult();
		} catch (NoResultException e) {
			return BigDecimal.ZERO;
		}		
	}
}
