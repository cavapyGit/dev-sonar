package com.pradera.idepositary.businessintegration.component.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;


import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.service.VaultControlService;
import com.pradera.integration.component.business.to.VaultControlTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.VaultControlComponent;
import com.pradera.model.component.VaultControlRules;
import com.pradera.model.custody.physical.PhysicalCertificate;


@Singleton
@Startup
public class VaultControlServiceBean implements VaultControlService {
	
	@EJB
	VaultControlAdditionalServiceBean vaultControlAdditionalServiceBean;
	
	/** The log. */
	@Inject
	private PraderaLogger log;
		
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void insertCertificates(Integer cupones, Date fechaVencimiento, List<VaultControlTO> certificates, LoggerUser loggerUser) throws ServiceException {
		// TODO Auto-generated method stub
		
		
		for(int i = 0; i < certificates.size(); i++) {
			
	        log.info(":::: INICIO EJECUCION ALGORITMO DE BOVEDA ::::");
	        long startTime = System.nanoTime(); // Tiempo inicial
			
			String valor = certificates.get(i).getIdSecurityCodeFk();
			Long idPhysicalCertificate = certificates.get(i).getIdPhysicalCertificateFk();
			Integer instrumentType = vaultControlAdditionalServiceBean.verifyInstrumentType(valor);
			Date expirationDate = null;
			//VaultControlTO result = new VaultControlTO();
			//List<VaultControlComponent> result = new ArrayList<VaultControlComponent>();
			VaultControlRules result = new VaultControlRules();
			List<PhysicalCertificate> physicalCertificates = new ArrayList<>();
			List<VaultControlTO> securitiesToSave = new ArrayList<VaultControlTO>();
			List<VaultControlComponent> boxPositions = new ArrayList<VaultControlComponent>();
			Integer counterCertificatesSave = 0;
			
			if(idPhysicalCertificate==null) {
				physicalCertificates = vaultControlAdditionalServiceBean.getPhysicalCertificates(valor);
			}else {//solo se le envia los certificados fisicos como parametros a los serializados 
				PhysicalCertificate physicalCertificateSerializable = vaultControlAdditionalServiceBean.getPhysicalCertificate(idPhysicalCertificate); 
				physicalCertificates.add(physicalCertificateSerializable);
			}
			
			
			if(Validations.validateIsNotNull(physicalCertificates) && !physicalCertificates.isEmpty()) {
				
				for(int j = 0; j < physicalCertificates.size(); j++) {
					
					PhysicalCertificate iteration = physicalCertificates.get(j);
					
					if(Validations.validateIsNotNull(iteration.getIndCupon()) && iteration.getIndCupon().equals(0)) {
						expirationDate = physicalCertificates.get(j).getExpirationDate();
					}
					
					VaultControlTO certificate = new VaultControlTO();

					certificate.setIdPhysicalCertificateFk(iteration.getIdPhysicalCertificatePk());
					certificate.setIdSecurityCodeFk(valor);
					certificate.setIndCoupon(iteration.getIndCupon());
					certificate.setCouponNumber(iteration.getCuponNumber());
					certificate.setIdPendingSecurityCodePk(iteration.getIdPendingSecurityCodePk());
					
					securitiesToSave.add(certificate);
											
				}
				
				Integer securityClass = physicalCertificates.get(0).getSecurityClass();
				
				if(Validations.validateIsNotNull(instrumentType) && instrumentType == 399) {
					
					result = calculatePositionForActions(securitiesToSave.size(), valor, loggerUser);
								
				} else {
					
					result = getBestPosition(securitiesToSave.size(), expirationDate, securityClass,loggerUser);
					
				}
				
			}
					
			if(Validations.validateIsNotNull(result) && Validations.validateIsNotNull(securitiesToSave) && !securitiesToSave.isEmpty()) {
				
				boxPositions = vaultControlAdditionalServiceBean.getCertificatesPerBox(result);
				
				for(int j = 0; j < boxPositions.size(); j++) {
					
					VaultControlComponent iteration = boxPositions.get(j);
					VaultControlTO certificate = securitiesToSave.get(counterCertificatesSave);
					
					if(iteration.getIdSecurityCodeFk() == null) {
						iteration.setIdPhysicalCertificateFk(certificate.getIdPhysicalCertificateFk());
						iteration.setIdSecurityCodeFk(certificate.getIdSecurityCodeFk());
						iteration.setIndCoupon(certificate.getIndCoupon());
						iteration.setCouponNumber(certificate.getCouponNumber());
						iteration.setIdPendingSecurityCodePk(certificate.getIdPendingSecurityCodePk());
						iteration.setRegistryDate(CommonsUtilities.currentDateTime());
						
						if(loggerUser != null) {
							iteration.setAudit(loggerUser);
						} else {
							iteration.setLastModifyDate(CommonsUtilities.currentDateTime());
						}
						
						vaultControlAdditionalServiceBean.update(iteration);
						counterCertificatesSave += 1;
					}
					
					if(counterCertificatesSave == securitiesToSave.size()) {
						break;
					}
					
				}
							
			};		
			
	        long endTime = System.nanoTime(); // Tiempo final
	        
	        long tiempoTotalNano = endTime - startTime; // Tiempo total en nanosegundos
	        long tiempoTotalMillis = TimeUnit.NANOSECONDS.toMillis(tiempoTotalNano); // Convertir a milisegundos
	        
	        //System.out.println("EJECUCION ALGORITMO DE BOVEDA");
	        //System.out.println("Tiempo total (ms): " + tiempoTotalMillis);	  
	        
	        log.info("Tiempo total algoritmo boveda(ms): " + tiempoTotalMillis);
	        log.info(":::: FIN EJECUCION ALGORITMO DE BOVEDA ::::");
	
		}
		
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void deleteCertificates(String idSecurityCodeFk) {
		// TODO Auto-generated method stub
			
		Long result = vaultControlAdditionalServiceBean.verifyCertificate(idSecurityCodeFk);

		//si encuentra retira certificado de la boveda
		if(result > 0L) {
			vaultControlAdditionalServiceBean.deleteCertificate(idSecurityCodeFk);
		}
		
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void deleteCertificates(Long idPhysicalCertificateFk) throws ServiceException {
		// TODO Auto-generated method stub
		Long result = vaultControlAdditionalServiceBean.verifyCertificateWithIdCertificatePk(idPhysicalCertificateFk);
		
		//si encuentra retira certificado de la boveda
		if(result > 0L) {
			vaultControlAdditionalServiceBean.deleteCertificateWithIdCertificatePk(idPhysicalCertificateFk);
		}

		
	}
	
	public VaultControlRules getBestPosition(Integer cupones, Date expirationDate, Integer securityClass, LoggerUser loggerUser) throws ServiceException {
		
		//METHOD THAT CALCULATE VAULT RULE TO USE
		
		List<VaultControlRules> listVaultControlRules = new ArrayList<VaultControlRules>();
		listVaultControlRules = (List<VaultControlRules>)vaultControlAdditionalServiceBean.verifyYearVaultControlRules(expirationDate);

		//SOLO SE VALIDA CAJAS QUE TENGAN LA CANTIDAD DE CUPONES COMO ESPACIOS LIBRES Y QUE TENGAN LA MISMA CLASE DE VALOR
		
		//listVaultControlRules = listVaultControlRules.stream().filter(box -> Integer.valueOf(cupones) <= vaultControlAdditionalServiceBean.getFreeSpacePerBox(box).intValue())
		//.filter(box -> securityClass.equals(box.getSecuritiesClass())).collect(Collectors.toList());
		
		Optional<VaultControlRules> chosenBox = listVaultControlRules.stream()
				.filter(box -> Integer.valueOf(cupones) <= vaultControlAdditionalServiceBean.getFreeSpacePerBox(box).intValue())
				.filter(box -> securityClass.equals(box.getSecuritiesClass()))
				.findFirst();
				
		if(chosenBox.isPresent()) {
			return chosenBox.get();
		}

		//SE VALIDAN CAJAS QUE TENGAN FECHA VENCIMIENTO NULL PERO QUE ESTEN CONFIGURADAS PARA LA REGLA DE FECHA DE VENCIIMENTO
		List<VaultControlRules> listBoxWithoutExpirationDate = (List<VaultControlRules>)vaultControlAdditionalServiceBean.getBoxesForExpirationDateRule();
		Optional<VaultControlRules> chosenBoxWithoutExpirationDate = listBoxWithoutExpirationDate.stream().filter(box -> Integer.valueOf(cupones) <= vaultControlAdditionalServiceBean.getFreeSpacePerBox(box).intValue())
				.filter(box -> box.getCertificatesPerBox().equals(vaultControlAdditionalServiceBean.getFreeSpacePerBox(box).intValue())) //verificamos si la caja esta vacia
				.filter(box -> securityClass.equals(box.getSecuritiesClass())).findFirst();
		
		if(chosenBoxWithoutExpirationDate.isPresent()) {
			
			chosenBoxWithoutExpirationDate.get().setExpirationDate(expirationDate);
			chosenBoxWithoutExpirationDate.get().setAudit(loggerUser);

			return chosenBoxWithoutExpirationDate.get();
			
		} 
			
		VaultControlRules newRule = new VaultControlRules();
		List<VaultControlRules> emptyBoxes = vaultControlAdditionalServiceBean.getBoxesWithoutRules();
		
		if(emptyBoxes != null && !emptyBoxes.isEmpty()) {
			
			//indFindedPosition = emptyBoxes.stream().anyMatch(x -> x.getCertificatesPerBox().equals(cupones));	
			Optional<VaultControlRules> result = emptyBoxes.stream().filter(box -> Integer.valueOf(box.getCertificatesPerBox()) >= Integer.valueOf(cupones))
					.filter(box -> box.getCertificatesPerBox().equals(vaultControlAdditionalServiceBean.getFreeSpacePerBox(box).intValue())) //verificamos si la caja esta vacia
					.findFirst();			
			//emptyBoxes.stream().findFirst()
			
			if(result.isPresent()) {
				newRule.setBox(result.get().getBox());
				newRule.setLevel(result.get().getLevel());
				newRule.setCabinet(result.get().getCabinet());
				newRule.setExpirationDate(expirationDate);
				newRule.setId(result.get().getId());
				newRule.setSecuritiesClass(securityClass);
				newRule.setIndBoxForIssuers(0);
				newRule.setCertificatesPerBox(result.get().getCertificatesPerBox());
				
	            if(Validations.validateIsNotNull(loggerUser)) {
	            	newRule.setLastModifyApp(loggerUser.getIdPrivilegeOfSystem());
	            	newRule.setLastModifyDate(loggerUser.getAuditTime());
	                newRule.setLastModifyIp(loggerUser.getIpAddress());
	                newRule.setLastModifyUser(loggerUser.getUserName());
	            }
	            
	            vaultControlAdditionalServiceBean.updateVaultControlRules(newRule);
	            
	            return newRule;
								
			}
			
		}
		
		return null;
				
	}
	
	public VaultControlRules calculatePositionForActions(Integer cupones, String valor, LoggerUser loggerUser) throws ServiceException {
		
        //LAS REGLAS SERAN CREADAS AL MOMENTO DE CREAR EL ARMARIO
        //SIN TENER LAS FECHA DE VENCIMIENTO DEFINIDAS NI LOS EMISORES DEFINIDOS
        
        //VAULT RULES WILL BE CREATED WHEN VAULTS ARE CREATED
        //WITHOUT HAVING DEFINED EXPIRATION DATE
		
		List<VaultControlComponent> positionsCalculated = new ArrayList<VaultControlComponent>();
		List<VaultControlComponent> lista = new ArrayList<VaultControlComponent>();
		
		String issuer = vaultControlAdditionalServiceBean.verifyIssuer(valor);
		
		List<VaultControlRules> boxesForActions = vaultControlAdditionalServiceBean.getBoxesForActions();
		List<VaultControlRules> boxesForActionsWithoutIssuersLimit = new ArrayList<VaultControlRules>();
		
		
		//SOLO SE VALIDA CAJAS QUE TENGAN LA CANTIDAD DE CUPONES COMO ESPACIOS LIBRES
		boxesForActions = boxesForActions.stream().filter(box -> Integer.valueOf(cupones) <= vaultControlAdditionalServiceBean.getFreeSpacePerBox(box).intValue())
				.collect(Collectors.toList());
		
		
		if(!boxesForActions.isEmpty()) {
			for(int i = 0; i < boxesForActions.size(); i++) {
				List<String> issuersPerBox = vaultControlAdditionalServiceBean.getIssuersPerBox(boxesForActions.get(i));
				
				Boolean issuerExistInBox = issuersPerBox.stream().anyMatch(x -> x.equals(issuer));
				
				if(issuerExistInBox) {
					return boxesForActions.get(i);
				} else if(issuersPerBox.size() < boxesForActions.get(i).getIssuersPerBox()) {
					boxesForActionsWithoutIssuersLimit.add(boxesForActions.get(i));
				}
				
			}
			
			if(!boxesForActionsWithoutIssuersLimit.isEmpty()) {
				return boxesForActionsWithoutIssuersLimit.get(0);
			}
			
		}
		
	
		VaultControlRules newRule = new VaultControlRules();
		Boolean indFindedPosition = false;
		List<VaultControlRules> emptyBoxes = vaultControlAdditionalServiceBean.getBoxesWithoutRules();
		
		if(emptyBoxes != null && !emptyBoxes.isEmpty()) {
			
			//indFindedPosition = emptyBoxes.stream().anyMatch(x -> x.getCertificatesPerBox().equals(cupones));	
			Optional<VaultControlRules> result = emptyBoxes.stream().filter(box -> Integer.valueOf(box.getCertificatesPerBox()) >= Integer.valueOf(cupones))
					.filter(box -> box.getCertificatesPerBox().equals(vaultControlAdditionalServiceBean.getFreeSpacePerBox(box).intValue()))
					.findFirst();			
			//emptyBoxes.stream().findFirst()
			
			if(result.isPresent()) {
				newRule.setBox(result.get().getBox());
				newRule.setLevel(result.get().getLevel());
				newRule.setCabinet(result.get().getCabinet());
				newRule.setId(result.get().getId());
				newRule.setIndBoxForIssuers(1);
				newRule.setIssuersPerBox(1); //1 emisor por defecto por caja
				newRule.setCertificatesPerBox(result.get().getCertificatesPerBox());
								
	            if(Validations.validateIsNotNull(loggerUser)) {
	            	newRule.setLastModifyApp(loggerUser.getIdPrivilegeOfSystem());
	            	newRule.setLastModifyDate(loggerUser.getAuditTime());
	                newRule.setLastModifyIp(loggerUser.getIpAddress());
	                newRule.setLastModifyUser(loggerUser.getUserName());
	            }
	            
	            vaultControlAdditionalServiceBean.updateVaultControlRules(newRule);

	            return newRule;
			}
		}
		return null;
		
	}
	
	/*private List<VaultControlComponent> calculatePosition(Integer cantidadCupones, List<VaultControlComponent> lista) {
		
		Integer contador = 0;
        VaultControlComponent aux = null;
        //VaultControlTO auxTO = new VaultControlTO();
        List<VaultControlComponent> result = new ArrayList<VaultControlComponent>();
        Boolean firstValue = true;
        Integer counter = 0;
        
        for(int i = 0; i < lista.size(); i++) {
        	
        	VaultControlComponent iteration = lista.get(i);
        	
        	if(iteration.getIdSecurityCodeFk() != null) {
        		result.add(iteration);
        	}
        	
        	counter += 1;
        	
        	if(counter == cantidadCupones) {
        		return result;
        	}

        }
		
        return result;

	}*/
	
}