package com.pradera.idepositary.businessintegration.component.webservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.transaction.TransactionSynchronizationRegistry;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.DepositarySetupBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.common.validation.to.ValidationOperationType;
import com.pradera.integration.common.validation.to.ValidationProcessTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.business.to.OperationQueryTO;
import com.pradera.integration.component.corporatives.service.CorporateProcessRemoteService;
import com.pradera.integration.component.corporatives.to.InterestPaymentQueryTO;
import com.pradera.integration.component.custody.service.CustodyRemoteService;
import com.pradera.integration.component.custody.to.AccountBalanceQueryTO;
import com.pradera.integration.component.custody.to.AccountEntryQueryTO;
import com.pradera.integration.component.custody.to.SecuritiesRenewalQueryTO;
import com.pradera.integration.component.custody.to.StockEarlyPaymentQueryTO;
import com.pradera.integration.component.generalparameters.to.ServiceStatusQueryTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.remote.ThreadRemoteContextHolder;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionStateType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BusinessOperationResource.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/09/2015
 */
@Path("/BusinessOperationResource")
@Stateless
public class BusinessOperationResource implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The custody remote service. */
	@Inject
	private Instance<CustodyRemoteService> custodyRemoteService;
	
	/** The corporate process remote service. */
	@Inject
	private Instance<CorporateProcessRemoteService> corporateProcessRemoteService;
	
	/** The interface component service bean. */
	@Inject
    private Instance<InterfaceComponentService> interfaceComponentServiceBean;
	
	/** The parameter service bean. */
	@EJB
	private ParameterServiceBean parameterServiceBean;
	
	/** The registry. */
	@Resource
	private TransactionSynchronizationRegistry registry;
	
	/** The depositary setup bean. */
	@Inject
	DepositarySetupBean depositarySetupBean;
	
	/** The server configuration. */
	@Inject
	@InjectableResource(location = "ServerConfiguration.properties")
	private Properties serverConfiguration;
	
	/** The fail response. */
	String FAILD_RESPONSE = "schema/edvresponse/query_failed_response.xml";
	
	/** The audits user. */
	String strAuditsUser;
	
	/** The audits IP address. */
	String strIpAddress;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		strAuditsUser = serverConfiguration.getProperty("webservice.audits.users.key");
		strIpAddress = serverConfiguration.getProperty("webservice.audits.ip");
	}
	
	/**
	 * Gets the service operation.
	 *
	 * @param xmlParam the xml param
	 * @return the service operation
	 */
	@Performance
	@POST
	@Path("/con")
	@Produces(MediaType.APPLICATION_XML)
	public Response getServiceOperation(String xmlParam)
	{
		log.info(":::: INICIO SERVICIO CON ::::");
		ProcessFileTO objProcessFileTO = null;
		ValidationProcessTO objValidationProcessTO = null;
		OperationQueryTO operationQueryTO = null;
		String schemaResponse= null;
		List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		StringBuilder xmlResponse= new StringBuilder();
		
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		try {
			log.info(":::: INICIO VALIDACION ARCHIVO ::::");
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(
															ComponentConstant.INTERFACE_CUSTODY_OPERATION_QUERY_CON, 
															xmlParam, ComponentConstant.CON_DPF_NAME, Boolean.FALSE);
			log.info(":::: FIN VALIDACION ARCHIVO ::::");
			
			log.info(":::: INICIO LLAMADA REMOTA ::::");
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				Long idInterfaceProcess= interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(objProcessFileTO, 
																			ExternalInterfaceReceptionType.WEBSERVICE.getCode(), objLoggerUser);
				objValidationProcessTO = objProcessFileTO.getValidationProcessTO();
				for(ValidationOperationType objValidationOperationType : objValidationProcessTO.getLstOperations()){
					operationQueryTO= (OperationQueryTO) objValidationOperationType;
					operationQueryTO.setIdInterfaceProcess(idInterfaceProcess);
					if (ComponentConstant.INTERFACE_ACCOUNT_ENTRY_QUERY.equalsIgnoreCase(operationQueryTO.getOperationCode())) {
						AccountEntryQueryTO accountEntryQueryTO= new AccountEntryQueryTO(operationQueryTO);
						schemaResponse= ComponentConstant.CRG_SCHEMA_RESPONSE;
						lstRecordValidationTypes= custodyRemoteService.get().getListDpfAccountEntryOperation(accountEntryQueryTO);
					} else if (ComponentConstant.INTERFACE_EARLY_PAYMENT_QUERY.equalsIgnoreCase(operationQueryTO.getOperationCode())) {
						StockEarlyPaymentQueryTO stockEarlyPaymentQueryTO= new StockEarlyPaymentQueryTO(operationQueryTO);
						schemaResponse= ComponentConstant.CRA_SCHEMA_RESPONSE;
						lstRecordValidationTypes= custodyRemoteService.get().getListDpfEarlyPaymentOperation(stockEarlyPaymentQueryTO);
					} else if (ComponentConstant.INTERFACE_SECURITIES_RENEWAL_QUERY.equalsIgnoreCase(operationQueryTO.getOperationCode())) {
						SecuritiesRenewalQueryTO securitiesRenewalQueryTO= new SecuritiesRenewalQueryTO(operationQueryTO);
						schemaResponse= ComponentConstant.CRE_SCHEMA_RESPONSE;
						lstRecordValidationTypes= custodyRemoteService.get().getListDpfSecuritiesRenewalOperation(securitiesRenewalQueryTO);
					} else if (ComponentConstant.INTERFACE_INTEREST_PAYMENT_QUERY.equalsIgnoreCase(operationQueryTO.getOperationCode())) {
						InterestPaymentQueryTO interestPaymentQueryTO= new InterestPaymentQueryTO(operationQueryTO);
						schemaResponse= ComponentConstant.CPC_SCHEMA_RESPONSE;
						lstRecordValidationTypes= corporateProcessRemoteService.get().getListDpfInterestPaymentCoupon(interestPaymentQueryTO);
					}
					objValidationProcessTO.setLstValidations(lstRecordValidationTypes);
				}
			}
			log.info(":::: FIN LLAMADA REMOTA ::::");
		} catch (Exception ex) {
			ex.printStackTrace();
			objValidationProcessTO= parameterServiceBean.generateErrorValidationProcess(operationQueryTO, ex);
			processState= ExternalInterfaceReceptionStateType.FAILED.getCode();
		} finally {
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			if (objValidationProcessTO!=null && Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {				
				RecordValidationType objRecordValidationType = objValidationProcessTO.getLstValidations().get(0);
				if(Validations.validateIsNullOrEmpty(objRecordValidationType.getErrorCode())){
					schemaResponse = FAILD_RESPONSE;
				} else if(GeneralConstants.WEBSERVICES_ERROR_1000.compareTo(objRecordValidationType.getErrorCode()) != 0){
					schemaResponse = FAILD_RESPONSE;
				}								
				xmlResponse.append(interfaceComponentServiceBean.get().saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
															objValidationProcessTO.getLstValidations(), schemaResponse, 
															ComponentConstant.DPF_TAG, ComponentConstant.QUERY_RECORD_TAG, processState, objLoggerUser));
			} else {
				xmlResponse.append("NO HAY DATOS");
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");																
		}
		return Response.status(201).entity(xmlResponse.toString()).build();
	}
	
	/**
	 * Gets the account entry operation.
	 *
	 * @param xmlParam the xml param
	 * @return the account entry operation
	 */
	@Performance
	@POST
	@Path("/civ")
	@Produces(MediaType.APPLICATION_XML)
	public Response getAccountEntryOperation(String xmlParam)
	{
		log.info(":::: INICIO SERVICIO CIV ::::");
		ProcessFileTO objProcessFileTO = null;
		ValidationProcessTO objValidationProcessTO = null;
		AccountBalanceQueryTO accountBalanceQueryTO= null;
		List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		StringBuilder xmlResponse= new StringBuilder();
		
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		try {
			log.info(":::: INICIO VALIDACION ARCHIVO ::::");
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(
															ComponentConstant.INTERFACE_SINGLE_QUERY_BALANCE_CIV, 
															xmlParam, ComponentConstant.CIV_DPF_NAME, Boolean.FALSE);
			log.info(":::: FIN VALIDACION ARCHIVO ::::");
			
			log.info(":::: INICIO LLAMADA REMOTA ::::");
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				Long idInterfaceProcess= interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(objProcessFileTO, 
																			ExternalInterfaceReceptionType.WEBSERVICE.getCode(), objLoggerUser);
				objValidationProcessTO = objProcessFileTO.getValidationProcessTO();
				for(ValidationOperationType objValidationOperationType : objValidationProcessTO.getLstOperations()){
					accountBalanceQueryTO= (AccountBalanceQueryTO) objValidationOperationType;
					accountBalanceQueryTO.setIdInterfaceProcess(idInterfaceProcess);
					lstRecordValidationTypes= custodyRemoteService.get().getListDpfHolderAccountBalance(accountBalanceQueryTO);
					objValidationProcessTO.setLstValidations(lstRecordValidationTypes);
				}
			}
			log.info(":::: FIN LLAMADA REMOTA ::::");
		} catch (Exception ex) {
			objValidationProcessTO= parameterServiceBean.generateErrorValidationProcess(accountBalanceQueryTO, ex);
			processState= ExternalInterfaceReceptionStateType.FAILED.getCode();
		} finally {		
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			if (objValidationProcessTO!=null && Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {
				xmlResponse.append(interfaceComponentServiceBean.get().saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
															objValidationProcessTO.getLstValidations(), objProcessFileTO.getStreamResponseFileDir(), 
															ComponentConstant.DPF_TAG, ComponentConstant.QUERY_RECORD_TAG, processState, objLoggerUser));
			} else {
				xmlResponse.append("NO HAY DATOS");
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");																
		}
		
		return Response.status(201).entity(xmlResponse.toString()).build();
	}
	
	/**
	 * Gets the web service state.
	 *
	 * @param xmlParam the xml param
	 * @return the web service state
	 */
	@Performance
	@POST
	@Path("/ese")
	@Produces(MediaType.APPLICATION_XML)
	public Response getWebServiceState(String xmlParam)
	{
		log.info(":::: INICIO SERVICIO ESE ::::");
		ProcessFileTO objProcessFileTO = null;
		ValidationProcessTO objValidationProcessTO = null;
		OperationQueryTO operationQueryTO = null;
		StringBuilder xmlResponse = new StringBuilder();
		List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();		
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		
		try {
			
			/*
			 * STEP 01:
			 * Parse the from XML file to TO object   
			 */
			
			log.info(":::: INICIO VALIDACION ARCHIVO ::::");
			objProcessFileTO = parameterServiceBean.validateInterfaceFile(
																ComponentConstant.INTERFACE_QUERY_SERVICE_STATUS_DPF, 
																xmlParam, ComponentConstant.ESE_DPF_NAME, Boolean.FALSE);
			log.info(":::: FIN VALIDACION ARCHIVO ::::");
						
			/*
			 * STEP 02:
			 * Call the Remote service from Corporative Process
			 */		
			
			log.info(":::: INICIO LLAMADA REMOTA ::::");			
			
			if (Validations.validateIsNotNull(objProcessFileTO)) {				
				objValidationProcessTO = objProcessFileTO.getValidationProcessTO();
				for(ValidationOperationType objValidationOperationType : objValidationProcessTO.getLstOperations()){					
					operationQueryTO = (OperationQueryTO) objValidationOperationType;				
					ServiceStatusQueryTO objServiceStatusQueryTO = new ServiceStatusQueryTO(operationQueryTO);
					lstRecordValidationTypes = interfaceComponentServiceBean.get().getServiceStatus(objServiceStatusQueryTO, objLoggerUser);
					objValidationProcessTO.setLstValidations(lstRecordValidationTypes);
				}
			}
			log.info(":::: FIN LLAMADA REMOTA ::::");
			
		} catch (Exception ex) {
			ex.printStackTrace();
			objValidationProcessTO = parameterServiceBean.generateErrorValidationProcess(operationQueryTO, ex);
		} finally {		
			
			/*
			 * STEP 03:
			 * Generate the response XML file
			 */
			
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			
			if (objValidationProcessTO!=null && Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {
				xmlResponse.append(interfaceComponentServiceBean.get().saveExternalInterfaceResponseFileTx(objValidationProcessTO.getLstValidations(), objProcessFileTO.getStreamResponseFileDir(), 
															ComponentConstant.DPF_TAG, ComponentConstant.OPERATION_RECORD_TAG));
			} else {
				xmlResponse.append("NO HAY DATOS");
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");																
		}
		log.info(":::: FIN SERVICIO ESE ::::");
		
		return Response.status(201).entity(xmlResponse.toString()).build();
	}
}
