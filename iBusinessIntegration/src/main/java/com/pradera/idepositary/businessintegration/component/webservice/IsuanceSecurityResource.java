package com.pradera.idepositary.businessintegration.component.webservice;

import java.io.Serializable;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.transaction.TransactionSynchronizationRegistry;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.ValidationOperationType;
import com.pradera.integration.common.validation.to.ValidationProcessTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.custody.service.CustodyRemoteService;
import com.pradera.integration.component.custody.to.AccountEntryRegisterTO;
import com.pradera.integration.component.custody.to.ReverseRegisterTO;
import com.pradera.integration.component.securities.service.SecuritiesRemoteService;
import com.pradera.integration.component.securities.to.IssuanceRegisterTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.remote.ThreadRemoteContextHolder;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionStateType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionType;

@Path("/IsuanceSecurityResource")
@Stateless
public class IsuanceSecurityResource implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private PraderaLogger log;

	@Inject
	@InjectableResource(location = "ServerConfiguration.properties")
	private Properties serverConfiguration;

	@Resource
	private TransactionSynchronizationRegistry registry;

	/** The parameter service bean. */
	@EJB
	private ParameterServiceBean parameterServiceBean;

	/** The interface component service. */
	@EJB
	InterfaceComponentService interfaceComponentService;

	@Inject
	private Instance<SecuritiesRemoteService> securitiesRemoteService;
	@Inject
	private Instance<CustodyRemoteService> custodyRemoteService;

	@Inject
	@DepositarySetup
	IdepositarySetup iDepositarySetup;

	String strAuditsUser;

	String strIpAddress;

	@PostConstruct
	public void init() {
		strAuditsUser = serverConfiguration
				.getProperty("webservice.audits.users.key");
		strIpAddress = serverConfiguration.getProperty("webservice.audits.ip");
	}

	/**
	 * metodo para realizar el registro de emision y valor(es)
	 * <br>
	 * Inicialmente esta implementado especificamente para valores de tipo BTX y BBX
	 * <br>
	 * Pero se puede utilizar par las demas clases de valor con las debidas adecuaciones 
	 * @param xmlParam
	 * @return
	 */
	@Performance
	@POST
	@Path("/rva")
	@Produces(MediaType.APPLICATION_XML)
	public Response createIssuanceAndSecurities(String xmlParam) {
		log.info(":::: Inicio del servico RVA ::::");
		ProcessFileTO objProcessFileTO = null;
		ValidationProcessTO objValidationProcessTO = null;
		IssuanceRegisterTO issuanceRegisterTO = null;
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		StringBuilder xmlResponse = new StringBuilder();

		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(),objLoggerUser);

		try {

			/*
			 * STEP 01: Parse the from XML file to TO object
			 */
			objProcessFileTO = parameterServiceBean.validateInterfaceFile(	ComponentConstant.INTERFACE_ISUANCE_SECURITIES, 
																			xmlParam,
																			ComponentConstant.RVA_DPF_NAME, 
																			Boolean.FALSE);
			/*
			 * STEP 02: Call the Remote service
			 */
			Long idInterfaceProcess = interfaceComponentService.saveExternalInterfaceReceptionTx(	objProcessFileTO,
																									ExternalInterfaceReceptionType.WEBSERVICE.getCode(),
																									objLoggerUser);
			objValidationProcessTO = objProcessFileTO.getValidationProcessTO();

			for (ValidationOperationType operation : objValidationProcessTO.getLstOperations()) {

				issuanceRegisterTO = (IssuanceRegisterTO) operation;
				issuanceRegisterTO.setIdInterfaceProcess(idInterfaceProcess);
				try {
					// realializando el registro de la emision y el valor via webService
					objValidationProcessTO.getLstValidations().add(securitiesRemoteService.get().createIssuanceSecurityTx(issuanceRegisterTO, objLoggerUser));
				} catch (Exception e) {
					objValidationProcessTO.getLstValidations().addAll(parameterServiceBean.generateErrorValidationProcess(issuanceRegisterTO, e).getLstValidations());
				}
			}
		} catch (Exception ex) {
			if (objValidationProcessTO != null) {
				objValidationProcessTO.getLstValidations().addAll(parameterServiceBean.generateErrorValidationProcess(issuanceRegisterTO, ex).getLstValidations());
			}
			interfaceComponentService.saveInterfaceTransactionTx(issuanceRegisterTO,BooleanType.NO.getCode(), objLoggerUser);
		} finally {

			/*
			 * STEP 03: Generate the response XML file
			 */
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			if (objValidationProcessTO != null
					&& Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {
				xmlResponse.append(interfaceComponentService.saveExternalInterfaceResponseFileTx(	objProcessFileTO.getIdProcessFilePk(),
																									objValidationProcessTO.getLstValidations(),
																									objProcessFileTO.getStreamResponseFileDir(),
																									ComponentConstant.INTERFACE_TAG_ISSUANCE_RESPONSE,
																									ComponentConstant.INTERFACE_RECORD_ISSUANCE_BCB,
																									processState, objLoggerUser));
			} else {
				xmlResponse.append("NO HAY DATOS");
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");
		}
		log.info(":::: FIN SERVICIO RVA ::::");
		return Response.status(201).entity(xmlResponse.toString()).build();
	}
	
	/**
	 * Placements Securities
	 * Issue 1239 Desarrollo de Servicios Web BCB Venta Directa RCO
	 * @param xmlParam the xml param
	 * @return placing a security
	 */
	@Performance
	@POST
	@Path("/rco")
	@Produces(MediaType.APPLICATION_XML)
	public Response createPlacementSecurity(String xmlParam)
	{
	
			log.info(":::: Inicio del servico RCO ::::");
			ProcessFileTO objProcessFileTO = null;
			ValidationProcessTO objValidationProcessTO = null;
			IssuanceRegisterTO issuanceRegisterTO = null;
			Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
			StringBuilder xmlResponse = new StringBuilder();

			LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
			ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(),objLoggerUser);

			try {

				/*
				 * STEP 01: Parse the from XML file to TO object
				 */
				objProcessFileTO = parameterServiceBean.validateInterfaceFile(	ComponentConstant.INTERFACE_SECURITY_PLACEMENT_RCO, xmlParam,ComponentConstant.RCO_BBX_NAME, Boolean.FALSE);
				/*
				 * STEP 02: Call the Remote service
				 */
				Long idInterfaceProcess = interfaceComponentService.saveExternalInterfaceReceptionTx(	objProcessFileTO,
																										ExternalInterfaceReceptionType.WEBSERVICE.getCode(),
																										objLoggerUser);
				objValidationProcessTO = objProcessFileTO.getValidationProcessTO();

				for (ValidationOperationType operation : objValidationProcessTO.getLstOperations()) {

					AccountEntryRegisterTO accountEntryRegisterTO = (AccountEntryRegisterTO) operation;
					accountEntryRegisterTO.setIdInterfaceProcess(idInterfaceProcess);
					
					try{
						objValidationProcessTO.getLstValidations().add(custodyRemoteService.get().createAccountEntryNewOperationTx(objLoggerUser,accountEntryRegisterTO));
					} catch (Exception e) {
						objValidationProcessTO.getLstValidations().addAll(parameterServiceBean.generateErrorValidationProcess(accountEntryRegisterTO, e).getLstValidations());
					} 
				}
			} catch (Exception ex) {
				if (objValidationProcessTO != null) {
					objValidationProcessTO.getLstValidations().addAll(parameterServiceBean.generateErrorValidationProcess(issuanceRegisterTO, ex).getLstValidations());
				}
				interfaceComponentService.saveInterfaceTransactionTx(issuanceRegisterTO,BooleanType.NO.getCode(), objLoggerUser);
			} finally {

				/*
				 * STEP 03: Generate the response XML file
				 */
				log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
				if (objValidationProcessTO != null
						&& Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {
					xmlResponse.append(interfaceComponentService.saveExternalInterfaceResponseFileTx(	objProcessFileTO.getIdProcessFilePk(),
																										objValidationProcessTO.getLstValidations(),
																										objProcessFileTO.getStreamResponseFileDir(),
																										ComponentConstant.REG_PLACEMENT_TAG_RESPONSE,
																										ComponentConstant.INTERFACE_RECORD_ISSUANCE_BCB,
																										processState, objLoggerUser));
				} else {
					xmlResponse.append("NO HAY DATOS");
				}
				log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");
			}
			log.info(":::: FIN SERVICIO RCO ::::");
			return Response.status(201).entity(xmlResponse.toString()).build();
	}
	
	/**
	 * Reverse registration placement no dpf.
	 * Issue 1239 Desarrollo de Servicios Web BCB Venta Directa RRC
	 * @param xmlParam the xml param
	 * @return the response
	 */
	@Performance
	@POST
	@Path("/rrc")
	@Produces(MediaType.APPLICATION_XML)
	public Response reverseRegistrationPlacement(String xmlParam)
	{
		log.info(":::: INICIO SERVICIO RRC ::::");
		ProcessFileTO objProcessFileTO = null;
		ValidationProcessTO objValidationProcessTO = null;
		ReverseRegisterTO reverseRegisterTO = null;		
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		StringBuilder xmlResponse= new StringBuilder();
		
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		
		try {
			/*
			 * STEP 01:
			 * Parse the from XML file to TO object   
			 */
			log.info(":::: INICIO VALIDACION ARCHIVO ::::");
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(
																ComponentConstant.INTERFACE_EARLY_PAYMENT_OF_PLACEMENT, 
																xmlParam, ComponentConstant.RRC_BBXBTX_NAME, Boolean.FALSE);
			log.info(":::: FIN VALIDACION ARCHIVO ::::");
			
			/*
			 * STEP 02:
			 * Call the Remote service 
			 */
			log.info(":::: INICIO LLAMADA REMOTA ::::");			
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				Long idInterfaceProcess = interfaceComponentService.saveExternalInterfaceReceptionTx(
						objProcessFileTO
						, ExternalInterfaceReceptionType.WEBSERVICE.getCode()
						, objLoggerUser);
				objValidationProcessTO = objProcessFileTO.getValidationProcessTO();
				for(ValidationOperationType objValidationOperationType : objValidationProcessTO.getLstOperations()){
					
					reverseRegisterTO = (ReverseRegisterTO) objValidationOperationType;	
					reverseRegisterTO.setIdInterfaceProcess(idInterfaceProcess);
					
					objValidationProcessTO.getLstValidations().add(custodyRemoteService.get().reverseRegistrationPlacementOperationTx(objLoggerUser, reverseRegisterTO));																				
				}				
			}			
			log.info(":::: FIN LLAMADA REMOTA ::::");
			
		} catch (Exception ex) {
			ex.printStackTrace();
			objValidationProcessTO= parameterServiceBean.generateErrorValidationProcess(reverseRegisterTO, ex);
			processState= ExternalInterfaceReceptionStateType.FAILED.getCode();
		} finally {
			
			/*
			 * STEP 03:
			 * Generate the response XML file
			 */
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			if (objValidationProcessTO!= null && Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {
				xmlResponse.append(interfaceComponentService.saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
															objValidationProcessTO.getLstValidations(), objProcessFileTO.getStreamResponseFileDir(), 
															ComponentConstant.REV_PLACEMENT_TAG_RESPONSE, ComponentConstant.INTERFACE_RECORD_ISSUANCE_BCB, processState, objLoggerUser));
			} else {
				xmlResponse.append("NO HAY DATOS");
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");
		}
		log.info(":::: FIN SERVICIO RRC ::::");		
		return Response.status(201).entity(xmlResponse.toString()).build();
	}
	
}
