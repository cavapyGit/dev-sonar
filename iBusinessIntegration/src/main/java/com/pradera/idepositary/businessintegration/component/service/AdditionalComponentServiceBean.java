package com.pradera.idepositary.businessintegration.component.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.PrimaryPlacementComponentTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounting.AccountingAccount;
import com.pradera.model.accounts.Participant;
import com.pradera.model.component.BalanceType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountBalancePK;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.component.IssuanceOperation;
import com.pradera.model.component.MovementBehavior;
import com.pradera.model.component.MovementType;
import com.pradera.model.component.OperationMovement;
import com.pradera.model.component.OperationType;
import com.pradera.model.component.type.ParameterBalanceType;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.affectation.type.AffectationStateType;
import com.pradera.model.custody.affectation.type.LevelAffectationType;
import com.pradera.model.custody.blockoperation.UnblockOperation;
import com.pradera.model.custody.changeownership.ChangeOwnershipOperation;
import com.pradera.model.custody.coupongrant.CouponGrantBalance;
import com.pradera.model.custody.coupongrant.CouponGrantOperation;
import com.pradera.model.custody.coupongrant.UnblockGrantOperation;
import com.pradera.model.custody.participantunion.ParticipantUnionOperation;
import com.pradera.model.custody.reversals.type.RequestReversalsStateType;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.transfersecurities.SecurityTransferOperation;
import com.pradera.model.funds.CashAccountDetail;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.funds.FundsOperationType;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.generalparameter.externalinterface.ExternalInterface;
import com.pradera.model.guarantees.AccountGuaranteeBalance;
import com.pradera.model.guarantees.GuaranteeMovement;
import com.pradera.model.guarantees.GuaranteeOperation;
import com.pradera.model.guarantees.type.GuaranteeClassType;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegment;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegmentDetail;
import com.pradera.model.issuancesecuritie.placementsegment.type.PlacementSegmentDetailStateType;
import com.pradera.model.issuancesecuritie.placementsegment.type.PlacementSegmentStateType;
import com.pradera.model.issuancesecuritie.placementsegment.type.PlacementSegmentStructStateType;
import com.pradera.model.issuancesecuritie.type.SecurityPlacementType;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.InternationalOperation;
import com.pradera.model.negotiation.MechanismModality;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.SwapOperation;
import com.pradera.model.negotiation.TradeOperation;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.settlement.SettlementOperation;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AdditionalComponentServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 24/09/2015
 */
@Stateless
public class AdditionalComponentServiceBean extends CrudDaoServiceBean{

	/** The logger. */
	@Inject
	private PraderaLogger logger;
	
	/** The session context. */
	@Resource
	SessionContext sessionContext;
	
	
	/**
	 * Method to get list of movement types depending the business process, operation type or funds operation type and indicator of SOURCE or TARGET.
	 *
	 * @param idBusinessProcess the id business process
	 * @param operationType the operation type
	 * @param fundsOperationType the funds operation type
	 * @param indSourceTarget the ind source target
	 * @return the list movement types
	 */
	@SuppressWarnings("unchecked")
	public List<MovementType> getListMovementTypes(Long idBusinessProcess, Long operationType, Long fundsOperationType, Long indSourceTarget)
	{
		List<MovementType> lstMovementTypes = new ArrayList<MovementType>();
		try {
			StringBuilder stringBuilderSql = new StringBuilder();
			stringBuilderSql.append(" SELECT MT FROM MovementType MT, OperationMovement OM");
			stringBuilderSql.append(" WHERE OM.businessProcess.idBusinessProcessPk = :idBusinessProcess ");
			if (operationType != null) {
				stringBuilderSql.append(" and OM.operationType.idOperationTypePk = :operationType ");
			} else if (fundsOperationType != null) {
				stringBuilderSql.append(" and OM.fundsOperationType.idFundsOperationTypePk = :fundsOperationType ");
			}
			stringBuilderSql.append(" and OM.indSourceTarget = :indSourceTarget ");
			stringBuilderSql.append(" and OM.movementType.idMovementTypePk = MT.idMovementTypePk ");
			stringBuilderSql.append(" order by OM.executionOrder") ;
			 
			Query query = em.createQuery(stringBuilderSql.toString());
			query.setParameter("idBusinessProcess", idBusinessProcess);
			if (operationType != null) {
				query.setParameter("operationType", operationType);
			} else if (fundsOperationType != null) {
				query.setParameter("fundsOperationType", fundsOperationType);
			}
			query.setParameter("indSourceTarget", indSourceTarget);
			query.setHint("org.hibernate.cacheable", true);
			lstMovementTypes = (List<MovementType>)query.getResultList();
		} catch (NoResultException e) {
			logger.error(("getListMovementTypes"));
			logger.debug(e.getMessage());
		}
		
		return lstMovementTypes;
	}
	
	public List<OperationMovement> allOperationMovements(Long idBusinessProcess, Long operationType, Long fundsOperationType, Integer operationState){
		List<OperationMovement> lstOperationMovement = new ArrayList<OperationMovement>();
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append(" SELECT om FROM OperationMovement om ");
		sbQuery.append(" join fetch om.movementType mt ");
		sbQuery.append(" left join fetch om.operationType ot ");
		sbQuery.append(" left join fetch om.fundsOperationType fot ");
		sbQuery.append(" join fetch om.businessProcess bp ");
		sbQuery.append(" where 1=1 ");
		Map<String, Object> params=new HashMap<>();
		if(idBusinessProcess!=null) {
			sbQuery.append(" AND bp.idBusinessProcessPk=:BUSINESS_PROCESS ");
			//params.put("BUSINESS_PROCESS", idBusinessProcess);
		}
		if(operationType!=null) {
			sbQuery.append(" AND ot.idOperationTypePk=:OPE_TYPE ");
			//params.put("OPE_TYPE", idBusinessProcess);
		}
		if(fundsOperationType!=null) {
			sbQuery.append(" AND fot.idFundsOperationTypePk=:FUND_OPE_TYPE ");
			//params.put("FUND_OPE_TYPE", fundsOperationType);
		}
		if(operationState!=null) {
			sbQuery.append(" AND om.idState=:OPE_STATE ");
			//params.put("FOP_STATE", operationState.longValue());
		}
//		if(indSourceTarget!=null) {
//			sbQuery.append(" AND bp.idBusinessProcessPk=:BUSINESS_PROCESS ");
//			params.put("BUSINESS_PROCESS", idBusinessProcess);
//		}		
		Query query= em.createQuery( sbQuery.toString() );
		if(idBusinessProcess!=null) {
			query.setParameter("BUSINESS_PROCESS", idBusinessProcess);
		}
		if(operationType!=null) {
			query.setParameter("OPE_TYPE", idBusinessProcess);
		}
		if(fundsOperationType!=null) {
			query.setParameter("FUND_OPE_TYPE", fundsOperationType);
		}
		if(operationState!=null) {
			query.setParameter("OPE_STATE", operationState.longValue());
		}
		lstOperationMovement = (List<OperationMovement>)query.getResultList();
		//super.buildQueryParameters(query, params);
		return lstOperationMovement;
	}
	
	public FundsOperation getOneFundsOperation(Long idFundsOperation){
		
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append(" SELECT fo FROM FundsOperation fo ");
		sbQuery.append(" INNER JOIN FETCH fo.institutionCashAccount ica ");
		sbQuery.append(" INNER JOIN FETCH ica.institutionCashAccount ica2 ");
		sbQuery.append(" where 1=1 ");
		Map<String, Object> params=new HashMap<>();
		if(idFundsOperation!=null) {
			sbQuery.append(" AND fo.idFundsOperationPk=:idFundsOperation ");
		}	
		Query query= em.createQuery( sbQuery.toString() );
		if(idFundsOperation!=null) {
			query.setParameter("idFundsOperation", idFundsOperation);
		}
		
		List<FundsOperation> FundsOperation= (List<FundsOperation>)query.getResultList();
		
		return FundsOperation.get(0);
	}
	
	
	/**
	 * Gets the list movement types cascade.
	 *
	 * @param idMovementType the id movement type
	 * @return the list movement types cascade
	 */
	@SuppressWarnings("unchecked")
	public List<MovementType> getListMovementTypesCascade(Long idMovementType)
	{
		List<MovementType> lstMovementTypes = new ArrayList<MovementType>();
		try {
			StringBuilder stringBuilderSql = new StringBuilder();
			stringBuilderSql.append(" SELECT MT FROM MovementType MT where MT.movementTypeCascade.idMovementTypePk = :idMovementTypePk ");
			
			Query query = em.createQuery(stringBuilderSql.toString());
			
			query.setParameter("idMovementTypePk", idMovementType);
			lstMovementTypes = (List<MovementType>)query.getResultList();
			
		} catch (NoResultException e) {
			logger.error(("getListMovementTypesCascade"));
			logger.debug(e.getMessage());
		}
		
		return lstMovementTypes;
	}
	
	
	/**
	 * Gets the operation type.
	 *
	 * @param idOperationType the id operation type
	 * @return the operation type
	 */
	public OperationType getOperationType(Long idOperationType)
	{
		OperationType objOperationType = null;
		try {
			objOperationType= this.find(OperationType.class, idOperationType);
		} catch (NoResultException e) {
			logger.error(("getOperationType"));
			logger.debug(e.getMessage());
		}
		return objOperationType;
	}
	
	
	/**
	 * Gets the corporative operation.
	 *
	 * @param idCorporativeOperation the id corporative operation
	 * @return the corporative operation
	 */
	public CorporativeOperation getCorporativeOperation(Long idCorporativeOperation)
	{
		CorporativeOperation objCorporativeOperation = null;
		try {
			objCorporativeOperation= this.find(CorporativeOperation.class, idCorporativeOperation);
		} catch (NoResultException e) {
			logger.error(("getCorporativeOperation"));
			logger.debug(e.getMessage());
		}
		
		return objCorporativeOperation;
	}
	
	/**
	 * Gets the settlement operation.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @return the settlement operation
	 */
	public SettlementOperation getSettlementOperation(Long idSettlementOperation)
	{
		SettlementOperation objSettlementOperation = null;
		try {
			objSettlementOperation= this.find(SettlementOperation.class, idSettlementOperation);
		} catch (NoResultException e) {
			logger.error(("getSettlementOperation"));
			logger.debug(e.getMessage());
		}
		  
		return objSettlementOperation;
	}
	
	/**
	 * Gets the trade operation.
	 *
	 * @param idTradeOperation the id trade operation
	 * @return the trade operation
	 */
	public TradeOperation getTradeOperation(Long idTradeOperation)
	{
		TradeOperation objTradeOperation = null;
		try {
			objTradeOperation= this.find(TradeOperation.class, idTradeOperation);
		} catch (NoResultException e) {
			logger.error(("getTradeOperation"));
			logger.debug(e.getMessage());
		}
		  
		return objTradeOperation;
	}
	
	/**
	 * Gets the custody operation.
	 *
	 * @param idCustodyOperation the id custody operation
	 * @return the custody operation
	 */
	public CustodyOperation getCustodyOperation(Long idCustodyOperation)
	{
		CustodyOperation objCustodyOperation = null;
		try {
			objCustodyOperation= this.find(CustodyOperation.class, idCustodyOperation);
		} catch (NoResultException e) {
			logger.error(("getCustodyOperation"));
			logger.debug(e.getMessage());
		}

		return objCustodyOperation;
	}
	
	/**
	 * Gets the guarantee operation.
	 *
	 * @param idGuaranteeOperation the id guarantee operation
	 * @return the guarantee operation
	 */
	public GuaranteeOperation getGuaranteeOperation(Long idGuaranteeOperation)
	{
		GuaranteeOperation objGuaranteeOperation = null;
		try {
			objGuaranteeOperation= this.find(GuaranteeOperation.class, idGuaranteeOperation);
		} catch (NoResultException e) {
			logger.error(("getGuaranteeOperation"));
			logger.debug(e.getMessage());
		}
		  
		return objGuaranteeOperation;
	}
	
	/**
	 * Gets the list movement behavior.
	 *
	 * @param idMovementType the id movement type
	 * @return the list movement behavior
	 */
	@SuppressWarnings("unchecked")
	public List<MovementBehavior> getListMovementBehavior(Long idMovementType)
	{
		List<MovementBehavior> lstMovementBehaviors= new ArrayList<MovementBehavior>();
		try {
			StringBuilder stringBuilderSql = new StringBuilder();
			stringBuilderSql.append("SELECT MB FROM MovementBehavior MB inner join fetch MB.balanceType BT WHERE MB.movementType.idMovementTypePk = :idMovementType");
			Query query = em.createQuery(stringBuilderSql.toString());
			query.setParameter("idMovementType", idMovementType);
			query.setHint("org.hibernate.cacheable", true);
			lstMovementBehaviors = (List<MovementBehavior>) query.getResultList(); 
		} catch (NoResultException e) {
			logger.error(("getListMovementBehavior"));
			logger.debug(e.getMessage());
		}
		
		return lstMovementBehaviors;
	}
	
	/**
	 * Gets the holder account balance.
	 *
	 * @param objHolderAccountBalanceTO the obj holder account balance to
	 * @return the holder account balance
	 */
	public HolderAccountBalance getHolderAccountBalance(HolderAccountBalanceTO objHolderAccountBalanceTO)
	{
		HolderAccountBalance objHolderAccountBalance = null;
		try {
			StringBuilder stringBuilderSql = new StringBuilder();
			stringBuilderSql.append("SELECT HAB FROM HolderAccountBalance HAB WHERE HAB.id.idParticipantPk = :idParticipant ");
			stringBuilderSql.append("and HAB.id.idHolderAccountPk = :idHolderAccount ");
			stringBuilderSql.append("and HAB.id.idSecurityCodePk = :idSecurityCode");
	  
			Query query = em.createQuery(stringBuilderSql.toString());
			query.setParameter("idParticipant", objHolderAccountBalanceTO.getIdParticipant());
			query.setParameter("idHolderAccount", objHolderAccountBalanceTO.getIdHolderAccount());
			query.setParameter("idSecurityCode", objHolderAccountBalanceTO.getIdSecurityCode());
			
			objHolderAccountBalance = (HolderAccountBalance) query.getSingleResult();
		} catch (NoResultException e) {
			logger.error(("getHolderAccountBalance"));
		}
		
		return objHolderAccountBalance;
	}
	
	/**
	 * Gets the securities operation data.
	 *
	 * @param idSecuritiesOperation the id securities operation
	 * @return the securities operation data
	 */
	public Object[] getSecuritiesOperationData(Long idSecuritiesOperation)
	{
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" select so.idSecuritiesOperationPk, so.cashAmount, "); // 0 1 
		sbQuery.append(" s.issuance.idIssuanceCodePk, s.idSecurityCodePk, nvl(s.currentNominalValue,0), "); // 2 3 4
		sbQuery.append(" so.stockQuantity "); 
		sbQuery.append(" from SecuritiesOperation so ");
		sbQuery.append(" inner join so.securities s where ");
		sbQuery.append(" so.idSecuritiesOperationPk = :idSecuritiesOperation ");

		parameters.put("idSecuritiesOperation", idSecuritiesOperation);
		
		try{
			return (Object[]) findObjectByQueryString(sbQuery.toString(), parameters);
		}catch(NoResultException ne){
			logger.error("no result exception");
		}
		return null; 
	}
	
	
	/**
	 * Gets the issuance.
	 *
	 * @param idIssuance the id issuance
	 * @return the issuance
	 */
	public Issuance getIssuance(String idIssuance)
	{
		Issuance objIssuance= null;
		try {
			objIssuance= this.find(Issuance.class, idIssuance);
		} catch (NoResultException e) {
			logger.error(("getIssuance"));
			logger.debug(e.getMessage());
		}
		
		return objIssuance;
	}
	
	
	/**
	 * Gets the issuance operation.
	 *
	 * @param idIssuanceOperation the id issuance operation
	 * @return the issuance operation
	 */
	public IssuanceOperation getIssuanceOperation(Long idIssuanceOperation)
	{
		IssuanceOperation objIssuanceOperation= null;
		try {
			objIssuanceOperation = this.find(IssuanceOperation.class, idIssuanceOperation); 
		} catch (NoResultException e) {
			logger.error(("getIssuanceOperation"));
			logger.debug(e.getMessage());
		}
		
		return objIssuanceOperation;
	}
	
	
	/**
	 * Gets the block operation.
	 *
	 * @param idBlockOperation the id block operation
	 * @return the block operation
	 */
	public BlockOperation getBlockOperation(Long idBlockOperation)
	{
		BlockOperation objBlockOperation = null;
		try {
			objBlockOperation= this.find(BlockOperation.class, idBlockOperation);
		} catch (NoResultException e) {
			logger.error(("getBlockOperation"));
			logger.debug(e.getMessage());
		}
		
		return objBlockOperation;
	}
	
	
	/**
	 * Gets the unblock operation.
	 *
	 * @param idUnblockOperation the id unblock operation
	 * @return the unblock operation
	 */
	public UnblockOperation getUnblockOperation(Long idUnblockOperation)
	{
		UnblockOperation objUnblockOperation = null;
		try {
			objUnblockOperation = this.find(UnblockOperation.class, idUnblockOperation);
		} catch (NoResultException e) {
			logger.error(("getUnblockOperation"));
			logger.debug(e.getMessage());
		}
		return objUnblockOperation;
	}
	
	
	/**
	 * Gets the account guarantee balance.
	 *
	 * @param idHolderAccountOperation the id holder account operation
	 * @param guaranteeClass the guarantee class
	 * @param idSecurityCode the id security code
	 * @param idInstitutionCashAccount the id institution cash account
	 * @return the account guarantee balance
	 */
	public AccountGuaranteeBalance getAccountGuaranteeBalance(Long idHolderAccountOperation, Integer guaranteeClass, String idSecurityCode, Long idInstitutionCashAccount)
	{
		AccountGuaranteeBalance objAccountGuaranteeBalance = null;
		StringBuffer stringBuffer = new StringBuffer();
		try {
			stringBuffer.append(" SELECT AGB FROM AccountGuaranteeBalance AGB ");
			stringBuffer.append(" WHERE AGB.holderAccountOperation.idHolderAccountOperationPk = :idHolderAccountOperation ");
			stringBuffer.append(" and AGB.guaranteeClass = :guaranteeClass ");
			if (GuaranteeClassType.SECURITIES.getCode().equals(guaranteeClass))
				stringBuffer.append(" and AGB.securities.idSecurityCodePk = :idSecurityCode ");
			else if (GuaranteeClassType.FUNDS.getCode().equals(guaranteeClass))
				stringBuffer.append(" and AGB.institutionCashAccount.idInstitutionCashAccountPk = :idInstitutionCashAccount ");
			
			TypedQuery<AccountGuaranteeBalance> query = em.createQuery(stringBuffer.toString(), AccountGuaranteeBalance.class);
			query.setParameter("idHolderAccountOperation", idHolderAccountOperation);
			query.setParameter("guaranteeClass", guaranteeClass);
			if (GuaranteeClassType.SECURITIES.getCode().equals(guaranteeClass))
				query.setParameter("idSecurityCode", idSecurityCode);
			else if (GuaranteeClassType.FUNDS.getCode().equals(guaranteeClass))
				query.setParameter("idInstitutionCashAccount", idInstitutionCashAccount);
			
			objAccountGuaranteeBalance = query.getSingleResult();
		} catch (NoResultException e) {
			//e.printStackTrace();
		}
		return objAccountGuaranteeBalance;
	}
	
	/**
	 * Gets the account guarantee balance.
	 *
	 * @param idAccountGuaranteeBalance the id account guarantee balance
	 * @return the account guarantee balance
	 */
	public AccountGuaranteeBalance getAccountGuaranteeBalance(Long idAccountGuaranteeBalance)
	{
		AccountGuaranteeBalance objAccountGuaranteeBalance = null;
		try {
			objAccountGuaranteeBalance = this.find(AccountGuaranteeBalance.class, idAccountGuaranteeBalance);
		} catch (NoResultException e) {
			e.printStackTrace();
		}
		return objAccountGuaranteeBalance;
	}
	
	/**
	 * Gets the securities.
	 *
	 * @param idSecurityCode the id security code
	 * @return the securities
	 */
	public Security getSecurities(String idSecurityCode)
	{
		Security objSecurity = null;
		try {
			objSecurity = this.find(Security.class, idSecurityCode);
		} catch (NoResultException e) {
			logger.error(("getSecurities"));
			logger.debug(e.getMessage());
		}
		return objSecurity;
	}
	
	
	/**
	 * Gets the mechanism operation.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @return the mechanism operation
	 */
	public MechanismOperation getMechanismOperation(Long idMechanismOperation)
	{
		MechanismOperation objMechanismOperation = null;
		try {
			objMechanismOperation = this.find(MechanismOperation.class, idMechanismOperation);
		} catch (NoResultException e) {
			logger.error(("getMechanismOperation"));
			logger.debug(e.getMessage());
		}
		return objMechanismOperation;
	}
	
	
	/**
	 * Gets the international operation.
	 *
	 * @param idInternationalOperation the id international operation
	 * @return the international operation
	 */
	public InternationalOperation getInternationalOperation(Long idInternationalOperation)
	{
		InternationalOperation objInternationalOperation = null;
		try {
			objInternationalOperation = this.find(InternationalOperation.class, idInternationalOperation);
		} catch (NoResultException e) {
			logger.error(("getInternationalOperation"));
			logger.debug(e.getMessage());
		}
		return objInternationalOperation;
	}
	
	
	/**
	 * Gets the security transfer operation.
	 *
	 * @param idSecurityTransferOperation the id security transfer operation
	 * @return the security transfer operation
	 */
	public SecurityTransferOperation getSecurityTransferOperation(Long idSecurityTransferOperation)
	{
		SecurityTransferOperation objSecurityTransferOperation = null;
		try {
			objSecurityTransferOperation = this.find(SecurityTransferOperation.class, idSecurityTransferOperation);
		} catch (NoResultException e) {
			logger.error(("getSecurityTransferOperation"));
			logger.debug(e.getMessage());
		}
		return objSecurityTransferOperation;
	}
	
	
	/**
	 * Gets the participant union operation.
	 *
	 * @param idParticipantUnionOperation the id participant union operation
	 * @return the participant union operation
	 */
	public ParticipantUnionOperation getParticipantUnionOperation(Long idParticipantUnionOperation)
	{
		ParticipantUnionOperation objParticipantUnionOperation = null;
		try {
			objParticipantUnionOperation = this.find(ParticipantUnionOperation.class, idParticipantUnionOperation);
		} catch (NoResultException e) {
			logger.error(("getParticipantUnionOperation"));
			logger.debug(e.getMessage());
		}
		return objParticipantUnionOperation;
	}
	
	
	/**
	 * Gets the change ownership operation.
	 *
	 * @param idChangeOwnershipOperation the id change ownership operation
	 * @return the change ownership operation
	 */
	public ChangeOwnershipOperation getChangeOwnershipOperation(Long idChangeOwnershipOperation)
	{
		ChangeOwnershipOperation objChangeOwnershipOperation = null;
		try {
			objChangeOwnershipOperation = this.find(ChangeOwnershipOperation.class, idChangeOwnershipOperation);
		} catch (NoResultException e) {
			logger.error(("getChangeOwnershipOperation"));
			logger.debug(e.getMessage());
		}
		return objChangeOwnershipOperation;
	}
	
	
	/**
	 * Gets the institution cash account.
	 *
	 * @param idInstitutionCashAccount the id institution cash account
	 * @return the institution cash account
	 */
	public InstitutionCashAccount getInstitutionCashAccount(Long idInstitutionCashAccount)
	{
		InstitutionCashAccount objInstitutionCashAccount = null;
		try {
			objInstitutionCashAccount = this.find(InstitutionCashAccount.class, idInstitutionCashAccount);
		} catch (NoResultException e) {
			logger.error(("getInstitutionCashAccount"));
			logger.debug(e.getMessage());
		}
		return objInstitutionCashAccount;
	}
	
	
	/**
	 * Gets the funds operation.
	 *
	 * @param idFundsOperation the id funds operation
	 * @return the funds operation
	 */
	public FundsOperation getFundsOperation(Long idFundsOperation)
	{
		FundsOperation objFundsOperation = null;
		try {
			objFundsOperation = this.find(FundsOperation.class, idFundsOperation);
		} catch (NoResultException e) {
			logger.error(("getFundsOperation"));
			logger.debug(e.getMessage());
		}
		return objFundsOperation;
	}
	
	
	/**
	 * Gets the swap operation.
	 *
	 * @param idSwapOperation the id swap operation
	 * @return the swap operation
	 */
	public SwapOperation getSwapOperation(Long idSwapOperation)
	{
		SwapOperation objSwapOperation = null;
		try {
			objSwapOperation = this.find(SwapOperation.class, idSwapOperation);
		} catch (NoResultException e) {
			logger.error(("getSwapOperation"));
			logger.debug(e.getMessage());
		}
		return objSwapOperation;
	}
	
	
	/**
	 * Gets the holder account operation.
	 *
	 * @param idHolderAccountOperation the id holder account operation
	 * @return the holder account operation
	 */
	public HolderAccountOperation getHolderAccountOperation(Long idHolderAccountOperation)
	{
		HolderAccountOperation objHolderAccountOperation = null;
		try {
			objHolderAccountOperation = this.find(HolderAccountOperation.class, idHolderAccountOperation);
		} catch (NoResultException e) {
			logger.error(("getHolderAccountOperation"));
			logger.debug(e.getMessage());
		}
		return objHolderAccountOperation;
	}
	
	/**
	 * Have reblocks with out unblock request.
	 *
	 * @param objHolderAccountBalanceTO the obj holder account balance to
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean haveReblocksWithOutUnblockRequest(HolderAccountBalanceTO objHolderAccountBalanceTO)throws ServiceException{
		List<Long> lstResult = haveReblocks(objHolderAccountBalanceTO);
		boolean blExist = false;
		for(Long idBlockOperation:lstResult){
			if(!haveUnblockOperationRegister(idBlockOperation)){
				blExist = true;
				break;
			}				
		}
		return blExist;
	}
	
	/**
	 * Gets the settlement operation details.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @param idHolderAccount the id holder account
	 * @return the settlement operation details
	 */
	public Object[] getSettlementOperationDetails(Long idSettlementOperation, Long idHolderAccount){
		StringBuffer stringBuffer= new StringBuffer();
		try {
			stringBuffer.append(" SELECT distinct HAO.inchargeStockParticipant.idParticipantPk,  ");
			stringBuffer.append(" 		 MO.buyerParticipant.idParticipantPk,  "); //1
			stringBuffer.append(" 		 MO.sellerParticipant.idParticipantPk,  "); //2
			stringBuffer.append(" 		 MO.mechanisnModality.id.idNegotiationMechanismPk,  "); //3
			stringBuffer.append(" 		 MO.mechanisnModality.id.idNegotiationModalityPk,  "); //4
			stringBuffer.append(" 		 MO.operationDate,  "); // 5
			stringBuffer.append(" 		 MO.operationNumber,  "); //6
			stringBuffer.append(" 		 SAO.role, "); // 7
			stringBuffer.append(" 		 MO.idMechanismOperationPk "); // 8
			stringBuffer.append(" FROM SettlementOperation SO, SettlementAccountOperation SAO, HolderAccountOperation HAO , MechanismOperation MO ");
			stringBuffer.append(" WHERE SO.idSettlementOperationPk = SAO.settlementOperation.idSettlementOperationPk ");
			stringBuffer.append(" and   HAO.idHolderAccountOperationPk = SAO.holderAccountOperation.idHolderAccountOperationPk ");
			stringBuffer.append(" and   HAO.mechanismOperation.idMechanismOperationPk = MO.idMechanismOperationPk ");
			stringBuffer.append(" and   SO.mechanismOperation.idMechanismOperationPk = MO.idMechanismOperationPk ");
			stringBuffer.append(" and   SO.idSettlementOperationPk = :idSettlementOperation ");
			stringBuffer.append(" and   HAO.holderAccount.idHolderAccountPk = :idHolderAccount ");
			stringBuffer.append(" and   SAO.operationState = :confirmedState ");
			
			Query query = em.createQuery(stringBuffer.toString());
			query.setParameter("idHolderAccount", idHolderAccount);
			query.setParameter("idSettlementOperation", idSettlementOperation);
			query.setParameter("confirmedState", HolderAccountOperationStateType.CONFIRMED.getCode());
			return (Object[]) query.getSingleResult();
		} catch (NoResultException | NonUniqueResultException ne) {
			logger.error("no settlement operation details for idHolderAccount: "+idHolderAccount+ ", idSettlementOperation: "+idSettlementOperation);
			ne.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Have unblock operation register.
	 *
	 * @param idBlockOperationPk the id block operation pk
	 * @return true, if successful
	 */
	public boolean haveUnblockOperationRegister(Long idBlockOperationPk){
		try{
			String strQuery = 	"	SELECT UO.blockOperation.idBlockOperationPk" +
								"	FROM UnblockOperation UO" +
								"	WHERE UO.blockOperation.idBlockOperationPk = :idBlockOperationPk" +
								"	AND UO.custodyOperation.state = :state";
			Query query = em.createQuery(strQuery);
			query.setParameter("idBlockOperationPk", idBlockOperationPk);
			query.setParameter("state", RequestReversalsStateType.REGISTERED.getCode());
			if(Validations.validateIsNotNullAndNotEmpty(query.getSingleResult()))
				return true;
		}catch (NoResultException e) {
			return false;
		}
		return false;
	}
	
	/**
	 * Have reblocks.
	 *
	 * @param objHolderAccountBalanceTO the obj holder account balance to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Long> haveReblocks(HolderAccountBalanceTO objHolderAccountBalanceTO) throws ServiceException{
		String strQuery = "			SELECT BO.idBlockOperationPk" +
				"		FROM BlockOperation BO" +
				"		WHERE BO.holderAccount.idHolderAccountPk = :idHolderAccountPk" +
				"		AND BO.participant.idParticipantPk = :idParticipantPk" +
				"		AND BO.securities.idSecurityCodePk = :idSecurityCodePk" +
				"		AND BO.blockLevel = :level" +
				"		AND BO.actualBlockBalance > 0" +
				"		AND BO.blockState = :state";	
		Query query = em.createQuery(strQuery);
		query.setParameter("idHolderAccountPk", objHolderAccountBalanceTO.getIdHolderAccount());
		query.setParameter("idParticipantPk", objHolderAccountBalanceTO.getIdParticipant());
		query.setParameter("idSecurityCodePk", objHolderAccountBalanceTO.getIdSecurityCode());
		query.setParameter("state", AffectationStateType.BLOCKED.getCode());
		query.setParameter("level", LevelAffectationType.REBLOCK.getCode());
		List<Long> lstResult = query.getResultList();
		return lstResult;
	}


	/**
	 * Collect reblocks.
	 *
	 * @param objHolderAccountBalanceTO the obj holder account balance to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Long> collectReblocks(
			HolderAccountBalanceTO objHolderAccountBalanceTO) throws ServiceException{
		List<Long> lstResult = haveReblocks(objHolderAccountBalanceTO);
		List<Long> lstReblockOperations = new ArrayList<Long>();
		for(Long idBlockOperation:lstResult){
			if(!haveUnblockOperationRegister(idBlockOperation)){
				lstReblockOperations.add(idBlockOperation);
			}
		}
		return lstReblockOperations;
	}


	/**
	 * Find available balance.
	 *
	 * @param idHolAccBal the id hol acc bal
	 * @return the holder account balance
	 */
	public HolderAccountBalance findAvailableBalance(HolderAccountBalancePK idHolAccBal) {
		try{
			String strQuery = "		SELECT hab " +
							  "		FROM HolderAccountBalance hab inner join fetch " +
							  "		hab.holderMarketfactBalance hmb " +
							  "		WHERE hab.availableBalance > 0" +
							  "		AND hmb.availableBalance > 0" +
							  "		AND hab.id.idHolderAccountPk = :idHolderAccountPk" +
							  "		AND hab.id.idSecurityCodePk = :idSecurityCodePk" +
							  "		AND hab.id.idParticipantPk = :idParticipantPk order by hmb.marketRate, hmb.marketPrice desc  ";
			Query query = em.createQuery(strQuery);		
			query.setParameter("idHolderAccountPk", idHolAccBal.getIdHolderAccountPk());
			query.setParameter("idParticipantPk", idHolAccBal.getIdParticipantPk());
			query.setParameter("idSecurityCodePk", idHolAccBal.getIdSecurityCodePk());
			return (HolderAccountBalance)query.getSingleResult();
		}catch (NoResultException e) {
			return null;
		}
	}
	
	
	/**
	 * Gets the coupon grant balance.
	 *
	 * @param idProgramInterestCoupon the id program interest coupon
	 * @param idHolderAccount the id holder account
	 * @param balanceType the balance type
	 * @return the coupon grant balance
	 */
	public CouponGrantBalance getCouponGrantBalance(Long idProgramInterestCoupon, Long idHolderAccount, Long balanceType)
	{
		CouponGrantBalance objCouponGrantBalance= null;
		StringBuffer stringBuffer= new StringBuffer();
		try {
			stringBuffer.append(" SELECT CGB FROM CouponGrantBalance CGB ");
			stringBuffer.append(" WHERE CGB.holderAccount.idHolderAccountPk = :idHolderAccount ");
			stringBuffer.append(" and CGB.programInterestCoupon.idProgramInterestPk = :idProgramInterestCoupon ");
			stringBuffer.append(" and CGB.balanceType = :balanceType ");
			
			TypedQuery<CouponGrantBalance> query = em.createQuery(stringBuffer.toString(),CouponGrantBalance.class);
			query.setParameter("idHolderAccount", idHolderAccount);
			query.setParameter("idProgramInterestCoupon", idProgramInterestCoupon);
			query.setParameter("balanceType", balanceType);
			
			objCouponGrantBalance = query.getSingleResult();
			
		} catch (NoResultException e) {
			e.printStackTrace();
		}
		return objCouponGrantBalance;
	}
	
	/**
	 * Gets the coupon grant operation.
	 *
	 * @param idCouponGrantOperation the id coupon grant operation
	 * @return the coupon grant operation
	 */
	public CouponGrantOperation getCouponGrantOperation(Long idCouponGrantOperation)
	{
		return this.find(CouponGrantOperation.class, idCouponGrantOperation);
	}
	
	/**
	 * Gets the unblock grant operation.
	 *
	 * @param idUnblockGrantOperation the id unblock grant operation
	 * @return the unblock grant operation
	 */
	public UnblockGrantOperation getUnblockGrantOperation(Long idUnblockGrantOperation)
	{
		return this.find(UnblockGrantOperation.class, idUnblockGrantOperation);
	}
	
	/**
	 * Gets the market fact balance.
	 *
	 * @param idParticipant the id participant
	 * @param idHolderAccount the id holder account
	 * @param idSecurityCode the id security code
	 * @param marketDate the market date
	 * @param marketRate the market rate
	 * @param marketPrice the market price
	 * @return the market fact balance
	 * @throws NonUniqueResultException the non unique result exception
	 * @throws ServiceException the service exception
	 */
	public HolderMarketFactBalance getMarketFactBalance(Long idParticipant, Long idHolderAccount, String idSecurityCode, 
				Date marketDate, BigDecimal marketRate, BigDecimal marketPrice) throws NonUniqueResultException, ServiceException
	{
		HolderMarketFactBalance objHolderMarketFactBalance= null;
		StringBuffer stringBuffer = new StringBuffer();
		try {
			stringBuffer.append(" SELECT HMB FROM HolderMarketFactBalance HMB ");
			stringBuffer.append(" WHERE HMB.holderAccountBalance.id.idParticipantPk = :idParticipant ");
			stringBuffer.append(" and HMB.holderAccountBalance.id.idHolderAccountPk = :idHolderAccount ");
			stringBuffer.append(" and HMB.holderAccountBalance.id.idSecurityCodePk = :idSecurityCode ");
			stringBuffer.append(" and trunc(HMB.marketDate) = trunc(:marketDate) ");
			stringBuffer.append(" and HMB.indActive = :activeConstant ");
			if (marketRate != null) {
				stringBuffer.append(" and HMB.marketRate = :marketRate ");
			} 
			if (marketPrice != null) {
				stringBuffer.append(" and HMB.marketPrice = :marketPrice ");
			}
//			else{
//				stringBuffer.append(" and HMB.marketPrice is null		");
//			}
			
			TypedQuery<HolderMarketFactBalance> query = em.createQuery(stringBuffer.toString(), HolderMarketFactBalance.class);
			query.setParameter("idParticipant", idParticipant);
			query.setParameter("idHolderAccount", idHolderAccount);
			query.setParameter("idSecurityCode", idSecurityCode);
			query.setParameter("marketDate", marketDate);
			query.setParameter("activeConstant", ComponentConstant.ONE);
			if (marketRate != null) {
				query.setParameter("marketRate", marketRate);
			}
			if (marketPrice != null) {
				query.setParameter("marketPrice", marketPrice);
			}
			objHolderMarketFactBalance = query.getSingleResult();
		} catch (NoResultException e) {
			logger.error("getMarketFactBalance");
		} catch (NonUniqueResultException e) {
			logger.error("idParticipant: " +idParticipant
			+ ", idHolderAccount: " +idHolderAccount
			+ ", idSecurityCode: " +idSecurityCode
			+ ", marketDate: " +marketDate
			+ ", marketRate: " +marketRate
			+ ", marketPrice: " +marketPrice);
			throw new ServiceException(ErrorServiceType.MULTIPLE_MARKET_RATE_PRICE_FOUND, ErrorServiceType.MULTIPLE_MARKET_RATE_PRICE_FOUND.getMessage());
		}
		return objHolderMarketFactBalance;
	}
	
	
	/**
	 * Gets the list holder market fact balance.
	 *
	 * @param idParticipant the id participant
	 * @param idHolderAccount the id holder account
	 * @param idSecurityCode the id security code
	 * @param balanceType the balance type
	 * @return the list holder market fact balance
	 */
	public List<HolderMarketFactBalance> getListHolderMarketFactBalance(Long idParticipant, Long idHolderAccount, String idSecurityCode, Long balanceType)
	{
		List<HolderMarketFactBalance> lstHolderMarketFactBalances= null;
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT HMB FROM HolderMarketFactBalance HMB ");
		stringBuffer.append(" WHERE HMB.holderAccountBalance.id.idParticipantPk = :idParticipant ");
		stringBuffer.append(" and HMB.holderAccountBalance.id.idHolderAccountPk = :idHolderAccount ");
		stringBuffer.append(" and HMB.holderAccountBalance.id.idSecurityCodePk = :idSecurityCode ");
		if (balanceType != null )
		{
			if (ParameterBalanceType.AVAILABLE_BALANCE.getCode().equals(balanceType)) {
				stringBuffer.append(" and HMB.availableBalance > 0 ");
			} else if (ParameterBalanceType.PAWN_BALANCE.getCode().equals(balanceType)) {
				stringBuffer.append(" and HMB.pawnBalance > 0 ");
			} else if (ParameterBalanceType.BAN_BALANCE.getCode().equals(balanceType)) {
				stringBuffer.append(" and HMB.banBalance > 0 ");
			} else if (ParameterBalanceType.OTHERS_BALANCE.getCode().equals(balanceType)) {
				stringBuffer.append(" and HMB.otherBlockBalance > 0 ");
			}
		}
		stringBuffer.append(" ORDER BY HMB.marketDate ");
		
		TypedQuery<HolderMarketFactBalance> query = em.createQuery(stringBuffer.toString(), HolderMarketFactBalance.class);
		query.setParameter("idParticipant", idParticipant);
		query.setParameter("idHolderAccount", idHolderAccount);
		query.setParameter("idSecurityCode", idSecurityCode);
		lstHolderMarketFactBalances= query.getResultList();
		return lstHolderMarketFactBalances;
	}
	
	/**
	 * Gets the business process.
	 *
	 * @param idBusinessProcess the id business process
	 * @return the business process
	 */
	public BusinessProcess getBusinessProcess(Long idBusinessProcess) {
		return this.find(BusinessProcess.class, idBusinessProcess);
	}
	
	/**
	 * Method to update the balances according the behavior.
	 *
	 * @param behavior the behavior
	 * @param originalBalance the original balance
	 * @param stockQuantity the stock quantity
	 * @param balanceType the balance type
	 * @return BigDecimal
	 * @throws ServiceException the service exception
	 */
	public BigDecimal updateBalance(Long behavior, BigDecimal originalBalance, BigDecimal stockQuantity, int balanceType) throws ServiceException
	{
		BigDecimal resultBalance= new BigDecimal(0);
		if (Validations.validateIsNull(originalBalance)) {
			originalBalance= BigDecimal.ZERO;
		}
		
		if (Validations.validateIsNull(stockQuantity)) {
			stockQuantity= BigDecimal.ZERO;
		}
		
		try {
			resultBalance= resultBalance.add(originalBalance);
			if (originalBalance.compareTo(BigDecimal.ZERO) < 0) {
				throw new ServiceException(ErrorServiceType.NEGATIVE_BALANCE,
															PropertiesUtilities.getExceptionMessage(ErrorServiceType.NEGATIVE_BALANCE.getMessage()));
			}
			if (ComponentConstant.SUM == behavior.longValue()) {
				resultBalance= resultBalance.add(stockQuantity);
			}
			else if (ComponentConstant.SUBTRACTION == behavior.longValue()){
				resultBalance= resultBalance.subtract(stockQuantity);
			}
			if (resultBalance.compareTo(BigDecimal.ZERO) < 0) {
				throw new ServiceException(ErrorServiceType.NEGATIVE_BALANCE,
						ErrorServiceType.NEGATIVE_BALANCE.getMessage());
			}
		} catch (ServiceException objException){
			String balanceMessage = "balanceType: " +balanceType +", balance origen: "+ originalBalance.toString() + ", balance operation: "+stockQuantity.toString();
			objException.getConcatenatedParams().add(balanceMessage);
			sessionContext.setRollbackOnly();
			logger.error(objException.getMessage());
			
			logger.error(("method updateBalance ::: " + balanceMessage));
			throw objException;
		}
		
		return resultBalance;
	}
	
	/**
	 * Update account guarantee balance.
	 *
	 * @param objGuaranteeMovement the obj guarantee movement
	 * @param lstMovementTypes the lst movement types
	 * @throws ServiceException the service exception
	 */
	public void updateAccountGuaranteeBalance(GuaranteeMovement objGuaranteeMovement, List<MovementType> lstMovementTypes) throws ServiceException	{
		try {
			//we get the account guarantee balance
			AccountGuaranteeBalance objAccountGuaranteeBalance = getAccountGuaranteeBalance(objGuaranteeMovement.getAccountGuaranteeBalance().getIdAccountGuarBalancePk());
			for (int i=0; i< lstMovementTypes.size(); ++i)
			{
				MovementType objMovementType = lstMovementTypes.get(i);
				List<MovementBehavior> lstMovementBehaviors = getListMovementBehavior(objMovementType.getIdMovementTypePk());
				for (int j=0; j<lstMovementBehaviors.size(); ++j)
				{
					MovementBehavior objMovementBehavior = lstMovementBehaviors.get(j);
					final int balanceType = objMovementBehavior.getBalanceType().getIdBalanceTypePk().intValue();
					switch (balanceType) {
						case ComponentConstant.TOTAL_GUARANTEE:
							logger.info("balance to modify : " + ParameterBalanceType.TOTAL_GUARANTEE.getDescripcion());
							objAccountGuaranteeBalance.setTotalGuarantee(updateBalance(objMovementBehavior.getIdBehavior(), 
									objAccountGuaranteeBalance.getTotalGuarantee(), objGuaranteeMovement.getMovementQuantity(), balanceType));
						break;
						case ComponentConstant.PRINCIPAL_GUARANTEE:
							logger.info("balance to modify : " + ParameterBalanceType.PRINCIPAL_GUARANTEE.getDescripcion());
							objAccountGuaranteeBalance.setPrincipalGuarantee(updateBalance(objMovementBehavior.getIdBehavior(), 
									objAccountGuaranteeBalance.getPrincipalGuarantee(), objGuaranteeMovement.getMovementQuantity(), balanceType));
						break;
						case ComponentConstant.MARGIN_GUARANTEE:
							logger.info("balance to modify : " + ParameterBalanceType.MARGIN_GUARANTEE.getDescripcion());
							objAccountGuaranteeBalance.setMarginGuarantee(updateBalance(objMovementBehavior.getIdBehavior(), 
									objAccountGuaranteeBalance.getMarginGuarantee(), objGuaranteeMovement.getMovementQuantity(), balanceType));
						break;
						case ComponentConstant.PRINCIPAL_DIVIDENDS_GUARANTEE:
							logger.info("balance to modify : " + ParameterBalanceType.PRINCIPAL_DIVIDENDS_GUARANTEE.getDescripcion());
							objAccountGuaranteeBalance.setPrincipalDividendsGuarantee(updateBalance(objMovementBehavior.getIdBehavior(), 
									objAccountGuaranteeBalance.getPrincipalDividendsGuarantee(), objGuaranteeMovement.getMovementQuantity(), balanceType));
						break;
						case ComponentConstant.MARGIN_DIVIDENDS_GUARANTEE:
							logger.info("balance to modify : " + ParameterBalanceType.MARGIN_DIVIDENDS_GUARANTEE.getDescripcion());
							objAccountGuaranteeBalance.setMarginDividendsGuarantee(updateBalance(objMovementBehavior.getIdBehavior(), 
									objAccountGuaranteeBalance.getMarginDividendsGuarantee(), objGuaranteeMovement.getMovementQuantity(), balanceType));
						break;
						case ComponentConstant.INTEREST_MARGIN_GUARANTEE:
							logger.info("balance to modify : " + ParameterBalanceType.INTEREST_MARGIN_GUARANTEE.getDescripcion());
							objAccountGuaranteeBalance.setInterestMarginGuarantee(updateBalance(objMovementBehavior.getIdBehavior(), 
									objAccountGuaranteeBalance.getInterestMarginGuarantee(), objGuaranteeMovement.getMovementQuantity(), balanceType));
						break;
					}
				}
			}
			//now we have to update the account guarantee balance
			this.update(objAccountGuaranteeBalance);
		} catch (ServiceException objException){
			logger.error(("updateAccountGuaranteeBalance"));
			objException.printStackTrace();
			throw objException;
		}	 
	}


	/**
	 * Gets the operation type cascade id.
	 *
	 * @param idOperationType the id operation type
	 * @return the operation type cascade id
	 */
	public Long getOperationTypeCascadeId(Long idOperationType) {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select ot.operationTypeCascade.idOperationTypePk "); 
		sbQuery.append(" from OperationType ot where ot.idOperationTypePk  = :idOperationType ");
		
		parameters.put("idOperationType", idOperationType);
		
		return (Long) findObjectByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Update primary placement.
	 *
	 * @param primaryPlacementComponentTO the primary placement component to
	 * @throws ServiceException the service exception
	 */
	public void updatePrimaryPlacement(PrimaryPlacementComponentTO primaryPlacementComponentTO) throws ServiceException{
		BigDecimal stockQuantity = primaryPlacementComponentTO.getStockQuantity();
		String idSecurityCode = primaryPlacementComponentTO.getIdSecurityCode();
		Long idPlacementParticipant = primaryPlacementComponentTO.getIdPlacementParticipant();
		Date registerDate = primaryPlacementComponentTO.getUpdateDate();
		
		PlacementSegment placementSegment = getPlacementSegment(idSecurityCode,idPlacementParticipant,registerDate);
		
		if(placementSegment == null){
			throw new ServiceException(ErrorServiceType.PRIMARY_PLACEMENT_NOT_EXIST);
		}
		
		if(primaryPlacementComponentTO.isValidateOpenPlacement() && !placementSegment.getPlacementSegmentState().equals(PlacementSegmentStateType.OPENED.getCode())){
			throw new ServiceException(ErrorServiceType.PRIMARY_PLACEMENT_NOT_OPEN);
		}
		
		for(PlacementSegmentDetail placementSegmentDetail : placementSegment.getPlacementSegmentDetails()){
			if(placementSegmentDetail.getSecurity().getIdSecurityCodePk().equals(idSecurityCode)){
				BigDecimal operationAmount = stockQuantity.multiply(placementSegmentDetail.getSecurity().getCurrentNominalValue());
				if(placementSegmentDetail.getPlacementSegmentDetType().equals(SecurityPlacementType.FIXED.getCode())){
					
					BigDecimal requestedAmount = placementSegmentDetail.getRequestAmount();
					
					if(ComponentConstant.SUM == primaryPlacementComponentTO.getMode()){
						BigDecimal placedAmount = placementSegmentDetail.getPlacedAmount();
						BigDecimal newRequestedAmount = requestedAmount.add(operationAmount);
						if(newRequestedAmount.compareTo(placedAmount) > 0){
							throw new ServiceException(ErrorServiceType.PLACEMENT_WITHOUT_BALANCE);
						}
						
						placementSegmentDetail.setRequestAmount(newRequestedAmount);
						
						BigDecimal fixedRequestedAmount = placementSegment.getRequestFixedAmount();
						placementSegment.setRequestFixedAmount(fixedRequestedAmount.add(operationAmount));
					}else if(ComponentConstant.SUBTRACTION == primaryPlacementComponentTO.getMode()){
						BigDecimal newRequestedAmount = requestedAmount.subtract(operationAmount);
						placementSegmentDetail.setRequestAmount(newRequestedAmount);
						
						BigDecimal fixedRequestedAmount = placementSegment.getRequestFixedAmount();
						placementSegment.setRequestFixedAmount(fixedRequestedAmount.subtract(operationAmount));
						
						if(placementSegmentDetail.getRequestAmount().compareTo(BigDecimal.ZERO) < 0 || placementSegment.getRequestFixedAmount().compareTo(BigDecimal.ZERO) < 0){
							throw new ServiceException(ErrorServiceType.NEGATIVE_BALANCE);
						}
						
					}
					
					break;
				
				}else if(placementSegmentDetail.getPlacementSegmentDetType().equals(SecurityPlacementType.FLOATING.getCode())){//Si es flotante
					
					BigDecimal floatingAmount = placementSegment.getFloatingAmount();
					BigDecimal requestedFloatingAmount = placementSegment.getRequestFloatingAmount();
					
					if(ComponentConstant.SUM == primaryPlacementComponentTO.getMode()){
						BigDecimal newRequestedFloatingAmount = requestedFloatingAmount.add(operationAmount);
						
						if(newRequestedFloatingAmount.compareTo(floatingAmount) > 0){
							throw new ServiceException(ErrorServiceType.PLACEMENT_WITHOUT_BALANCE);
						}
						
						placementSegment.setRequestFloatingAmount(newRequestedFloatingAmount);
						
						BigDecimal requestedAmount = placementSegmentDetail.getRequestAmount();
						placementSegmentDetail.setRequestAmount(requestedAmount.add(operationAmount));
						
					}else if(ComponentConstant.SUBTRACTION == primaryPlacementComponentTO.getMode()){
						BigDecimal newRequestedFloatingAmount = requestedFloatingAmount.subtract(operationAmount);
						placementSegment.setRequestFloatingAmount(newRequestedFloatingAmount);
						
						BigDecimal requestedAmount = placementSegmentDetail.getRequestAmount();
						placementSegmentDetail.setRequestAmount(requestedAmount.subtract(operationAmount));
						
						if(placementSegment.getRequestFloatingAmount().compareTo(BigDecimal.ZERO) < 0 || placementSegmentDetail.getRequestAmount().compareTo(BigDecimal.ZERO) < 0){
							throw new ServiceException(ErrorServiceType.NEGATIVE_BALANCE);
						}
						
					}
					
					
					break;
				}
			}
		}
		update(placementSegment);
	}


	/**
	 * Gets the placement segment.
	 *
	 * @param idSecurityCode the id security code
	 * @param idPlacementParticipant the id placement participant
	 * @param registerDate the register date
	 * @return the placement segment
	 */
	private PlacementSegment getPlacementSegment(String idSecurityCode, Long idPlacementParticipant, Date registerDate) {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" select ps from PlacementSegment ps inner join fetch ps.placementSegmentDetails psd ");
		sbQuery.append(" inner join fetch psd.security sec ");
		sbQuery.append(" inner join ps.placementSegParticipaStructs psp "); 
		sbQuery.append(" where sec.idSecurityCodePk = :idSecurityCode ");
		sbQuery.append(" and psp.participant.idParticipantPk  = :idParticipant ");
		sbQuery.append(" and psp.statePlacSegParticipant  = :statePlacSegPart ");
		sbQuery.append(" and psd.statePlacementSegmentDet = :placementSegmentDetailState ");
		sbQuery.append(" and :registerDate between trunc(ps.openDate) and trunc(ps.closingDate) ");
		
		parameters.put("registerDate", registerDate);
		parameters.put("idSecurityCode", idSecurityCode);
		parameters.put("idParticipant", idPlacementParticipant);
		parameters.put("statePlacSegPart", PlacementSegmentStructStateType.REGISTERED.getCode());
		parameters.put("placementSegmentDetailState", PlacementSegmentDetailStateType.REGISTERED.getCode());
		
		return (PlacementSegment) findObjectByQueryString(sbQuery.toString(), parameters);
	}


	/**
	 * Gets the security details.
	 *
	 * @param idSecurityCode the id security code
	 * @return the security details
	 */
	public Object[] getSecurityDetails(String idSecurityCode) {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" select s.currentNominalValue, s.instrumentType from Security s where s.idSecurityCodePk = :idSecurityCode");
		
		parameters.put("idSecurityCode", idSecurityCode);
		
		try{
			return (Object[]) findObjectByQueryString(sbQuery.toString(), parameters);
		}catch (NoResultException nre){
			return null;
		}
	}
	
	/**
	 * Update external interface state.
	 *
	 * @param idInterfaceReception the id interface reception
	 * @param processState the process state
	 * @param responseFile the response file
	 * @param uploadFile the upload file
	 * @param loggerUser the logger user
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateExternalInterfaceState(Long idInterfaceReception, Integer processState, byte[] responseFile, byte[] uploadFile, LoggerUser loggerUser){
		StringBuilder stringSQL = new StringBuilder();
		stringSQL.append("UPDATE InterfaceProcess ");
		stringSQL.append(" SET processState = :processState ");
		if(responseFile!=null){
			stringSQL.append(" , fileResponse = :fileResponse ");
		}
		if (Validations.validateIsNotNull(uploadFile)) {
			stringSQL.append(" , fileUpload = :uploadFile ");
		}
		stringSQL.append(" , lastModifyApp = :lastModifyApp ");
		stringSQL.append(" , lastModifyDate = :lastModifyDate ");
		stringSQL.append(" , lastModifyIp = :lastModifyIp ");
		stringSQL.append(" , lastModifyUser = :lastModifyUser ");
		stringSQL.append(" WHERE idInterfaceProcessPk = :idInterfaceProcess ");
		Query queryJpql = em.createQuery(stringSQL.toString());
		queryJpql.setParameter("idInterfaceProcess", idInterfaceReception);
		queryJpql.setParameter("processState", processState);
		if(responseFile!=null){
			queryJpql.setParameter("fileResponse", responseFile);
		}
		if (Validations.validateIsNotNull(uploadFile)) {
			queryJpql.setParameter("uploadFile", uploadFile);
		}
		queryJpql.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem() == null ? Integer.valueOf(0) : loggerUser.getIdPrivilegeOfSystem());
		queryJpql.setParameter("lastModifyDate", loggerUser.getAuditTime());
		queryJpql.setParameter("lastModifyIp", loggerUser.getIpAddress());
		queryJpql.setParameter("lastModifyUser", loggerUser.getUserName());
		queryJpql.executeUpdate();
	}
	
	
	/**
	 * Gets the participant by mnemonic.
	 *
	 * @param strMnemonic the str mnemonic
	 * @return the participant by mnemonic
	 */
	public Participant getParticipantByMnemonic(String strMnemonic) {	
		Participant objParticipant = null;
		try {
			StringBuilder stringBuilderSql = new StringBuilder();
			stringBuilderSql.append("SELECT PAR FROM Participant PAR WHERE PAR.mnemonic = :parMnemonic ");			 
			Query query = em.createQuery(stringBuilderSql.toString());
			query.setParameter("parMnemonic", strMnemonic);						
			objParticipant = (Participant) query.getSingleResult();
		} catch (NoResultException e) {
			logger.error(("getParticipantByMnemonic"));
		}		
		return objParticipant;
	}
	
	/**
	 * Gets the list external interface.
	 *
	 * @return the list external interface
	 */
	@SuppressWarnings("unchecked")
	public List<ExternalInterface> getListExternalInterface() {
		List<ExternalInterface> lstExternalInterface = new ArrayList<ExternalInterface>();
		try {
			StringBuilder stringBuilderSql = new StringBuilder();
			stringBuilderSql.append(" SELECT EI FROM ExternalInterface EI ");
			stringBuilderSql.append(" WHERE EI.indAccessWebservice = :parIndicadorOne ");
			stringBuilderSql.append(" and EI.indInterfaceType = :parIndicadorOne ");
			Query query = em.createQuery(stringBuilderSql.toString());			
			query.setParameter("parIndicadorOne", ComponentConstant.ONE);
			lstExternalInterface = (List<ExternalInterface>) query.getResultList();	
		} catch (NoResultException e){
			return null;
		}
		return lstExternalInterface;
	}

	/**
	 * Gets the list all movement type.
	 *
	 * @return the list all movement type
	 */
	public void getListAllMovementType() {
		StringBuilder stringBuffer = new StringBuilder();
		stringBuffer.append(" SELECT MT ");
		stringBuffer.append(" FROM MovementType MT ");
		
		TypedQuery<MovementType> typedQuery= em.createQuery(stringBuffer.toString(), MovementType.class);
		typedQuery.getResultList();
	}
	
	/**
	 * Gets the list all movement behavior.
	 *
	 * @return the list all movement behavior
	 */
	public void getListAllMovementBehavior() {
		StringBuilder stringBuffer = new StringBuilder();
		stringBuffer.append(" SELECT MB ");
		stringBuffer.append(" FROM MovementBehavior MB ");
		
		TypedQuery<MovementBehavior> typedQuery= em.createQuery(stringBuffer.toString(), MovementBehavior.class);
		typedQuery.getResultList();
	}
	
	/**
	 * Gets the list all operation type.
	 *
	 * @return the list all operation type
	 */
	public void getListAllOperationType() {
		StringBuilder stringBuffer = new StringBuilder();
		stringBuffer.append(" SELECT OT ");
		stringBuffer.append(" FROM OperationType OT ");
		
		TypedQuery<OperationType> typedQuery= em.createQuery(stringBuffer.toString(), OperationType.class);
		typedQuery.getResultList();
	}
	
	/**
	 * Gets the list all operation movement.
	 *
	 * @return the list all operation movement
	 */
	public void getListAllOperationMovement() {
		StringBuilder stringBuffer = new StringBuilder();
		stringBuffer.append(" SELECT OM ");
		stringBuffer.append(" FROM OperationMovement OM ");
		
		TypedQuery<OperationMovement> typedQuery= em.createQuery(stringBuffer.toString(), OperationMovement.class);
		typedQuery.getResultList();
	}
	
	/**
	 * Gets the list all business process.
	 *
	 * @return the list all business process
	 */
	public void getListAllBusinessProcess() {
		StringBuilder stringBuffer = new StringBuilder();
		stringBuffer.append(" SELECT BP ");
		stringBuffer.append(" FROM BusinessProcess BP ");
		
		TypedQuery<BusinessProcess> typedQuery= em.createQuery(stringBuffer.toString(), BusinessProcess.class);
		typedQuery.getResultList();
	}
	
	/**
	 * Gets the list all funds operation type.
	 *
	 * @return the list all funds operation type
	 */
	public void getListAllFundsOperationType() {
		StringBuilder stringBuffer = new StringBuilder();
		stringBuffer.append(" SELECT FOT ");
		stringBuffer.append(" FROM FundsOperationType FOT ");
		
		TypedQuery<FundsOperationType> typedQuery= em.createQuery(stringBuffer.toString(), FundsOperationType.class);
		typedQuery.getResultList();
	}
	
	/**
	 * Gets the all balance type.
	 *
	 * @return the all balance type
	 */
	public void getAllBalanceType(){
		TypedQuery<BalanceType> query = em.createQuery("select b from BalanceType b", BalanceType.class);
		query.getResultList();
	}
	
	/**
	 * Update funds interface transaction.
	 *
	 * @param idInterfaceTransaction the id interface transaction
	 * @param idFundsOperation the id funds operation
	 */
	//@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateFundsInterfaceTransaction(Long idInterfaceTransaction, Long idFundsOperation) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE InterfaceTransaction ");
		stringBuffer.append(" SET fundsOperation.idFundsOperationPk = :idFundsOperation ");
		stringBuffer.append(" WHERE idInterfaceTransactionPk = :idInterfaceTransaction ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idFundsOperation", idFundsOperation);
		query.setParameter("idInterfaceTransaction", idInterfaceTransaction);
		
		query.executeUpdate();
	}
	
	/**
	 * Gets the list all mechanism modality.
	 *
	 * @return the list all mechanism modality
	 */
	public void getListAllMechanismModality() {
		StringBuilder stringBuffer = new StringBuilder();
		stringBuffer.append(" SELECT MM ");
		stringBuffer.append(" FROM MechanismModality MM ");		
		TypedQuery<MechanismModality> typedQuery= em.createQuery(stringBuffer.toString(), MechanismModality.class);
		typedQuery.getResultList();
	}
	
	
	/**
	 * Gets the list all Accounting Account
	 * 
	 *  @return the list all Accounting Account
	 */
	public void getListAllAccountingAccount() {
		StringBuilder stringBuffer = new StringBuilder();
		stringBuffer.append(" SELECT AA ");
		stringBuffer.append(" FROM AccountingAccount AA ");		
		TypedQuery<AccountingAccount> typedQuery= em.createQuery(stringBuffer.toString(), AccountingAccount.class);
		typedQuery.getResultList();
	}
	
	/**
	 * Gets the list all negotiation mechanism.
	 *
	 * @return the list all negotiation mechanism
	 */
	public void getListAllNegotiationMechanism() {
		StringBuilder stringBuffer = new StringBuilder();
		stringBuffer.append(" SELECT NM ");
		stringBuffer.append(" FROM NegotiationMechanism NM ");		
		TypedQuery<NegotiationMechanism> typedQuery= em.createQuery(stringBuffer.toString(), NegotiationMechanism.class);
		typedQuery.getResultList();
	}
	
	/**
	 * Gets the list all negotiation modality.
	 *
	 * @return the list all negotiation modality
	 */
	public void getListAllNegotiationModality() {
		StringBuilder stringBuffer = new StringBuilder();
		stringBuffer.append(" SELECT NMO ");
		stringBuffer.append(" FROM NegotiationModality NMO ");		
		TypedQuery<NegotiationModality> typedQuery= em.createQuery(stringBuffer.toString(), NegotiationModality.class);
		typedQuery.getResultList();
	}
	
}
