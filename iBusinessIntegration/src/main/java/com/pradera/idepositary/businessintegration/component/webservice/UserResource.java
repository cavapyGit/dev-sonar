package com.pradera.idepositary.businessintegration.component.webservice;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;

@Path("/UserResource")
@Stateless
public class UserResource implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterServiceBean;
	
	@Inject
	private PraderaLogger log;
	
	/**
	 * Gets the user logins.
	 *
	 * @param userFilter the user filter
	 * @return the user logins
	 */
	
	@Performance
	@GET
	@Path("/issuercode/{code}")
	@Produces({MediaType.APPLICATION_JSON})
	public Long getParticipantIssuerCode(@PathParam("code") String issuerCode) {
		log.info(":::: inicio obtener emisor como participante ::::");
		log.info(":::: codigo desde securidad:::: " + issuerCode);
		return parameterServiceBean.getParticipantForIssuerDpfDpa(issuerCode);
	}
	
}
