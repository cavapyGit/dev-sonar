package com.pradera.idepositary.businessintegration.component.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.negotiation.type.AccountOperationReferenceType;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.InChargeNegotiationType;
import com.pradera.model.settlement.SettlementAccountMarketfact;
import com.pradera.model.settlement.type.ChainedOperationStateType;
import com.pradera.model.settlement.type.OperationPartType;

@Stateless
public class SalePurchaseAdditionalServiceBean extends CrudDaoServiceBean {

	private static final  Logger logger = LoggerFactory.getLogger(SalePurchaseAdditionalServiceBean.class);
	
	/**
	 * Method to get the balance from a HolderAccountBalance entity according the balance type
	 * @param Long idParticipant
	 * @param Long idHolderAccount
	 * @param String idIsinCode
	 * @param Long idBalanceType
	 * @return BigDecimal
	 */
	public BigDecimal getAvailableBalance(Long idParticipant, Long idHolderAccount, String idSecurityCode){
		logger.info("getListSaleHolderAccountOperations(idParticipant: "+ idParticipant +", idHolderAccount: "+ idHolderAccount + 
					", idIsinCode: "+ idSecurityCode +")");
		
		BigDecimal balance= null;
		StringBuffer stringBufferSql = new StringBuffer();
		try {
			stringBufferSql.append(" SELECT ");
			stringBufferSql.append(" HAB.availableBalance ");
			stringBufferSql.append(" FROM HolderAccountBalance HAB ");
			stringBufferSql.append(" WHERE HAB.id.idParticipantPk = :idParticipant ");
			stringBufferSql.append(" and HAB.id.idHolderAccountPk = :idHolderAccount ");
			stringBufferSql.append(" and HAB.id.idSecurityCodePk = :idIsinCode ");
			
			Query query = em.createQuery(stringBufferSql.toString());
			
			query.setParameter("idParticipant", idParticipant);
			query.setParameter("idHolderAccount", idHolderAccount);
			query.setParameter("idIsinCode", idSecurityCode);
			
			balance = new BigDecimal(query.getSingleResult().toString());
		} catch (NoResultException e) {
			logger.error(("getAccountBalance"));
			logger.info(e.getMessage());
		}
		
		return balance;
	}
	
	public Object[] getObjMarketFactBalance(Long idParticipant, Long idHolderAccount, String idSecurityCode, Date marketDate, BigDecimal marketRate, BigDecimal marketPrice){
		Object[] objArray= null;
		StringBuffer stringBufferSql = new StringBuffer();
		try {
			stringBufferSql.append(" SELECT ");
			stringBufferSql.append(" HMB.availableBalance ,");
			stringBufferSql.append(" HMB.marketRate ,");
			stringBufferSql.append(" HMB.marketPrice  ");
			stringBufferSql.append(" FROM HolderMarketFactBalance HMB ");
			stringBufferSql.append(" WHERE HMB.holderAccount.idHolderAccountPk = :idHolderAccount ");
			stringBufferSql.append(" and HMB.participant.idParticipantPk = :idParticipant ");
			stringBufferSql.append(" and HMB.security.idSecurityCodePk = :idSecurityCode ");
			stringBufferSql.append(" and HMB.marketDate = :marketDate ");
			stringBufferSql.append(" and HMB.indActive = :indicatorOne ");
			if(marketRate!=null){
				stringBufferSql.append(" and HMB.marketRate = :marketRate ");
			}
			if(marketPrice!=null){
				stringBufferSql.append(" and HMB.marketPrice = :marketPrice ");
			}
			stringBufferSql.append(" order by HMB.lastModifyDate desc ");
			
			Query query = em.createQuery(stringBufferSql.toString()).setFirstResult(0).setMaxResults(1);

			query.setParameter("idParticipant", idParticipant);
			query.setParameter("idHolderAccount", idHolderAccount);
			query.setParameter("idSecurityCode", idSecurityCode);
			query.setParameter("indicatorOne", ComponentConstant.ONE);
			query.setParameter("marketDate", marketDate);
			if(marketRate!=null){
				query.setParameter("marketRate", marketRate);
			}
			if(marketPrice!=null){
				query.setParameter("marketPrice", marketPrice);
			}
			
			
			objArray = (Object[])query.getSingleResult();
		} catch (NoResultException e) {
			logger.error(("getAccountBalance"));
			logger.info(e.getMessage());
		}
		
		return objArray;
	}
	
	/**
	 * Method to update the stock reference from HolderAccountOperation. This happens when blocks the balance or settles the operation
	 * @param Long idHolderAccountOperation
	 * @param Long idReferenceStock
	 * @return
	 */
	public int updateStockSettlementAccountOperation(Long idSettlementAccountOperation, Long idReferenceStock)
	{			
		StringBuilder strQuery = new StringBuilder();
		
		strQuery.append(" UPDATE SettlementAccountOperation SET stockReference = :idReferenceStock, stockBlockDate= :updateDate ");
		strQuery.append(" WHERE idSettlementAccountPk = :idSettlementAccountOperation ");
		
		Query query = em.createQuery(strQuery.toString());
		
		query.setParameter("idReferenceStock", idReferenceStock);
		query.setParameter("idSettlementAccountOperation", idSettlementAccountOperation);
		query.setParameter("updateDate", CommonsUtilities.currentDateTime());
		
		return query.executeUpdate();	
	}
	
	public void updateBlockedStockParticipantMechanismOperation(Long idMechanismOperation, Long idParticipant, Integer operationPart, BigDecimal stockQuantity)
	{
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" UPDATE ParticipantOperation SET blockedStock = blockedStock + :stockQuantity ");
		strQuery.append(" WHERE mechanismOperation.idMechanismOperationPk = :idMechanismOperation ");
		strQuery.append(" and participant.idParticipantPk = :idParticipant ");
		strQuery.append(" and operationPart = :operationPart ");
		strQuery.append(" and role = :idRole ");
		strQuery.append(" and inchargeType = :idInchargeType ");
		
		Query query = em.createQuery(strQuery.toString());
		
		query.setParameter("stockQuantity", stockQuantity);
		query.setParameter("idMechanismOperation", idMechanismOperation);
		query.setParameter("idParticipant", idParticipant);
		query.setParameter("operationPart", operationPart);
		query.setParameter("idRole", ComponentConstant.SALE_ROLE);
		query.setParameter("idInchargeType", InChargeNegotiationType.INCHARGE_STOCK.getCode());
		query.executeUpdate();
	}
	
	
	public void updateBlockedStockParticipantSwapOperation(Long idSwapOperation, Long idParticipant, BigDecimal stockQuantity)
	{
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" UPDATE ParticipantOperation SET blockedStock = blockedStock + :stockQuantity ");
		strQuery.append(" WHERE swapOperation.idSwapOperationPk = :idSwapOperation ");
		strQuery.append(" and participant.idParticipantPk = :idParticipant ");
		strQuery.append(" and operationPart = :operationPart ");
		strQuery.append(" and role = :idRole ");
		strQuery.append(" and inchargeType = :idInchargeType ");
		
		Query query = em.createQuery(strQuery.toString());
		
		query.setParameter("stockQuantity", stockQuantity);
		query.setParameter("idSwapOperation", idSwapOperation);
		query.setParameter("idParticipant", idParticipant);
		query.setParameter("operationPart", OperationPartType.CASH_PART.getCode());
		query.setParameter("idRole", ComponentConstant.SALE_ROLE);
		query.setParameter("idInchargeType", InChargeNegotiationType.INCHARGE_STOCK.getCode());
		query.executeUpdate();
	}
	
	
	public int updateStockSettlementOperation(Long idSettlementOperation, Long idReferenceStock, LoggerUser loggerUser) throws ServiceException
	{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE SettlementOperation set stockReference = :idStockReference ");
		stringBuffer.append(" , stockBlockDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idSettlementOperationPk = :idSettlementOperation ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idSettlementOperation", idSettlementOperation);
		query.setParameter("idStockReference", idReferenceStock);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();
		
	}
	
	@SuppressWarnings("unchecked")
	public List<SettlementAccountMarketfact> getSettlementMarketFactAccount(Long idSettlementAccountOperationPk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT sam FROM SettlementAccountMarketfact sam ");
		sbQuery.append("	WHERE sam.settlementAccountOperation.idSettlementAccountPk = :idSettlementAccount ");
		sbQuery.append("	and sam.indActive = :indActive ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSettlementAccount", idSettlementAccountOperationPk);
		query.setParameter("indActive", BooleanType.YES.getCode());
		return (List<SettlementAccountMarketfact>)query.getResultList();
	}

	public boolean isAccountOperationBlockedStock(Long idHolderAccountOperation,Long stockReference) {
		StringBuilder stringQuery = new StringBuilder();
		stringQuery.append(" SELECT sao.holderAccountOperation.stockQuantity, nvl(sum(sao.stockQuantity),0) ");
		stringQuery.append(" FROM SettlementAccountOperation sao ");
		stringQuery.append(" where sao.holderAccountOperation.idHolderAccountOperationPk = :idHolderAccountOperation ");
		stringQuery.append(" and sao.operationState = :confirmedState ");
		stringQuery.append(" and sao.stockReference = :stockReference ");
		stringQuery.append(" group by sao.holderAccountOperation.stockQuantity");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idHolderAccountOperation", idHolderAccountOperation);
		parameters.put("confirmedState", HolderAccountOperationStateType.CONFIRMED.getCode());
		parameters.put("stockReference", stockReference);
		
		Object[] arrObjs = (Object[]) findObjectByQueryString(stringQuery.toString(), parameters);
		BigDecimal totalQuantity = (BigDecimal) arrObjs[0];
		BigDecimal stockQuantity = (BigDecimal) arrObjs[1];
		if(stockQuantity.compareTo(totalQuantity) == 0){
			return true;
		}

		return false;
	}

	public boolean isHolderAccountOperationBlockedStock(Long idMechanismOperation, Integer operationPart) {
		StringBuilder stringQuery = new StringBuilder();
		stringQuery.append(" SELECT hao.mechanismOperation.stockQuantity, nvl(sum(hao.stockQuantity),0) ");
		stringQuery.append(" FROM HolderAccountOperation hao ");
		stringQuery.append(" where hao.mechanismOperation.idMechanismOperationPk = :idMechanismOperation ");
		stringQuery.append(" and hao.holderAccountState = :confirmedState ");
		stringQuery.append(" and hao.stockReference = :stockReference ");
		stringQuery.append(" and hao.role = :role ");
		stringQuery.append(" and hao.operationPart = :operationPart ");
		stringQuery.append(" group by hao.mechanismOperation.stockQuantity");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idMechanismOperation", idMechanismOperation);
		parameters.put("confirmedState", HolderAccountOperationStateType.CONFIRMED.getCode());
		parameters.put("stockReference", AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode());
		parameters.put("role", ComponentConstant.SALE_ROLE);
		parameters.put("operationPart", operationPart);
		
		Object[] arrObjs = (Object[]) findObjectByQueryString(stringQuery.toString(), parameters);
		BigDecimal totalQuantity = (BigDecimal)arrObjs[0];
		BigDecimal blockedQuantity = (BigDecimal)arrObjs[1];
		if(totalQuantity.compareTo(blockedQuantity) == 0){
			return true;
		}
		return false;
	}

	public boolean isSettlementAccountOperationBlockedStock(Long idSettlementOperation, Long code) {
		StringBuilder stringQuery = new StringBuilder();
		stringQuery.append(" SELECT sao.settlementOperation.stockQuantity, nvl(sum(sao.stockQuantity),0) ");
		stringQuery.append(" FROM SettlementAccountOperation sao ");
		stringQuery.append(" where sao.settlementOperation.idSettlementOperationPk = :idSettlementOperation ");
		stringQuery.append(" and sao.operationState = :confirmedState ");
		stringQuery.append(" and sao.stockReference = :stockReference ");
		stringQuery.append(" and sao.role = :role ");
		stringQuery.append(" group by sao.settlementOperation.stockQuantity");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idSettlementOperation", idSettlementOperation);
		parameters.put("confirmedState", HolderAccountOperationStateType.CONFIRMED.getCode());
		parameters.put("stockReference", AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode());
		parameters.put("role", ComponentConstant.SALE_ROLE);
		
		Object[] arrObjs = (Object[]) findObjectByQueryString(stringQuery.toString(), parameters);
		BigDecimal totalQuantity = (BigDecimal)arrObjs[0];
		BigDecimal blockedQuantity = (BigDecimal)arrObjs[1];
		if(totalQuantity.compareTo(blockedQuantity) == 0){
			return true;
		}
		return false;
	}	
	
	public void updateChainedHolderOperationStockReference(List<Long> lstIdChainedHolderOperation, Long stockReference)
	{
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" UPDATE ChainedHolderOperation SET stockReference= :stockReference ");
		stringBuffer.append(" WHERE idChainedHolderOperationPk in (:lstIdChainedHolderOperation) ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("lstIdChainedHolderOperation", lstIdChainedHolderOperation);
		query.setParameter("stockReference", stockReference);
		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	public List<Long> getListChainedSettlementAccountOperation(Long idSettlementAccountOperation)
	{		
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT HCD.chainedHolderOperation.idChainedHolderOperationPk "); //0
		stringBuffer.append(" FROM HolderChainDetail HCD ");
		stringBuffer.append(" WHERE HCD.settlementAccountMarketfact.settlementAccountOperation.idSettlementAccountPk = :idSettlementAccountOperation ");
		stringBuffer.append(" and HCD.chainedHolderOperation.chaintState = :chainState ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("chainState", ChainedOperationStateType.CONFIRMED.getCode());
		query.setParameter("idSettlementAccountOperation", idSettlementAccountOperation);
		
		return query.getResultList();
	}

	public int updateSettlementMarketFactOLD(Long idSettlementAccount, BigDecimal marketPrice, LoggerUser loggerUser) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE SettlementAccountMarketfact set marketPrice = :marketPrice ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE settlementAccountOperation.idSettlementAccountPk in (  ");
		stringBuffer.append(" 			:idSettlementAccount ) or ");
		stringBuffer.append("       settlementAccountOperation.idSettlementAccountPk in (  ");
		stringBuffer.append("           select saoref.idSettlementAccountPk from SettlementAccountOperation saoref ");
		stringBuffer.append("           where saoref.settlementAccountOperation.idSettlementAccountPk = :idSettlementAccount ) ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idSettlementAccount", idSettlementAccount);
		query.setParameter("marketPrice", marketPrice);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();
	}
	
	public int updateSettlementMarketFact(Long idSettlementAccount, BigDecimal marketPrice, LoggerUser loggerUser) {
		StringBuilder stringBuffer = new StringBuilder();
		
		stringBuffer.append(" UPDATE SETTLEMENT_ACCOUNT_MARKETFACT SETTLEMENT_ACCOUNT_MARKETFACT1 ");
		stringBuffer.append(" SET MARKET_PRICE = :marketPrice, ");
		stringBuffer.append(" LAST_MODIFY_APP = :lastModifyApp, ");
		stringBuffer.append(" LAST_MODIFY_DATE = :lastModifyDate, ");
		stringBuffer.append(" LAST_MODIFY_IP = :lastModifyIp, ");
		stringBuffer.append(" LAST_MODIFY_USER = :lastModifyUser ");
		stringBuffer.append(" WHERE SETTLEMENT_ACCOUNT_MARKETFACT1.ROWID ");
		stringBuffer.append(" IN (SELECT SETTLEMENT_ACCOUNT_MARKETFACT2.ROWID ");
		stringBuffer.append(" FROM SETTLEMENT_ACCOUNT_MARKETFACT SETTLEMENT_ACCOUNT_MARKETFACT2 ");
		stringBuffer.append(" WHERE ID_SETTLEMENT_ACCOUNT_FK IN (:idSettlementAccount) ");
		stringBuffer.append(" UNION ALL ");
		stringBuffer.append(" SELECT SETTLEMENT_ACCOUNT_MARKETFACT3.ROWID ");
		stringBuffer.append(" FROM SETTLEMENT_ACCOUNT_MARKETFACT SETTLEMENT_ACCOUNT_MARKETFACT3 ");
		stringBuffer.append(" WHERE ID_SETTLEMENT_ACCOUNT_FK IN ( ");
		stringBuffer.append(" SELECT /*+ INDEX_JOIN(SETTLEMENT1_) */ settlement1_.ID_SETTLEMENT_ACCOUNT_PK ");
		stringBuffer.append(" FROM SETTLEMENT_ACCOUNT_OPERATION settlement1_ ");
		stringBuffer.append(" WHERE settlement1_.ID_SETTLEMENT_ACCOUNT_FK = :idSettlementAccount) ");
		stringBuffer.append(" ) ");
		
		Query query = em.createNativeQuery(stringBuffer.toString());
		query.setParameter("idSettlementAccount", idSettlementAccount);
		query.setParameter("marketPrice", marketPrice);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();
	}
	
	public int updateSettlementTermMarketFact(Long idSettlementAccount, BigDecimal marketPrice, LoggerUser loggerUser) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE SettlementAccountMarketfact set marketPrice = :marketPrice ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE settlementAccountOperation.idSettlementAccountPk = ( ");
		stringBuffer.append("           select saoref.idSettlementAccountPk from SettlementAccountOperation saoref ");
		stringBuffer.append("           where saoref.settlementAccountOperation.idSettlementAccountPk = :idSettlementAccount ) ");
				
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idSettlementAccount", idSettlementAccount);
		query.setParameter("marketPrice", marketPrice);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();
	}
	
	public List<HolderMarketFactBalance> getListHolderMarketFactBalance(Long idHolderAccount, String idSecurityCode, Date marketDate, BigDecimal marketRate) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT HMB ");
		stringBuffer.append(" FROM HolderMarketFactBalance HMB ");
		stringBuffer.append(" WHERE HMB.holderAccount.idHolderAccountPk = :idHolderAccount ");
		stringBuffer.append(" and HMB.security.idSecurityCodePk = :idSecurityCode ");
		stringBuffer.append(" and HMB.marketRate = :marketRate ");
		stringBuffer.append(" and HMB.marketDate = :marketDate ");
		stringBuffer.append(" and HMB.indActive = :indActive ");
		//stringBuffer.append(" and HMB.availableBalance > 0 ");
		
		TypedQuery<HolderMarketFactBalance> typedQuery= em.createQuery(stringBuffer.toString(), HolderMarketFactBalance.class);
		typedQuery.setParameter("idHolderAccount", idHolderAccount);
		typedQuery.setParameter("idSecurityCode", idSecurityCode);
		typedQuery.setParameter("marketRate", marketRate);
		typedQuery.setParameter("marketDate", marketDate);
		typedQuery.setParameter("indActive", BooleanType.YES.getCode());
		
		return typedQuery.getResultList();
	}
	
	public void updateSettlementAccountMarketfact(List<Long> lstSettlementAccountMarketfact, LoggerUser loggerUser) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" UPDATE SettlementAccountMarketfact ");
		stringBuffer.append(" SET indActive = :indActive ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idSettAccountMarketfactPk in (:lstSettlementAccountMarketfact) ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("indActive", BooleanType.NO.getCode());
		query.setParameter("lstSettlementAccountMarketfact", lstSettlementAccountMarketfact);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
	}
	
	public boolean isBlockedBalanceOperation(Long idSettlementAccountOperation) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT SAO.stockReference ");
		stringBuffer.append(" FROM SettlementAccountOperation SAO ");
		stringBuffer.append(" WHERE SAO.idSettlementAccountPk = :idSettlementAccountOperation ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("idSettlementAccountOperation", idSettlementAccountOperation);
		Object stockReference= query.getSingleResult();
		if (Validations.validateIsNotNull(stockReference)) {
			return true;
		}
		return false;
	}
}
