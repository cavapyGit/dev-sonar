package com.pradera.idepositary.businessintegration.component.webservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.transaction.TransactionSynchronizationRegistry;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.common.validation.to.ValidationOperationType;
import com.pradera.integration.common.validation.to.ValidationProcessTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.business.to.QueryParametersConsultCdpf;
import com.pradera.integration.component.custody.service.CustodyRemoteService;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.remote.ThreadRemoteContextHolder;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionStateType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class QueryOperationDpf.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/09/2015
 */
@Path("/QueryOperationDpf")
@Stateless
public class QueryOperationDpf implements Serializable{

	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The str audits user. */
	String strAuditsUser;
	
	/** The str ip address. */
	String strIpAddress;
	
	/** The interface component service bean. */
	@Inject
    private Instance<InterfaceComponentService> interfaceComponentServiceBean;
	
	/** The parameter service bean. */
	@EJB
	private ParameterServiceBean parameterServiceBean;
	
	/** The custody remote service. */
	@Inject
	private Instance<CustodyRemoteService> custodyRemoteService;
	
	/** The registry. */
	@Resource
	private TransactionSynchronizationRegistry registry;		
	
	/** The server configuration. */
	@Inject
	@InjectableResource(location = "ServerConfiguration.properties")
	private Properties serverConfiguration;
	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		strAuditsUser = serverConfiguration.getProperty("webservice.audits.users.key");
		strIpAddress = serverConfiguration.getProperty("webservice.audits.ip");
	}
	
	/**
	 * Gets the service operation.
	 *
	 * @param xmlParam the xml param
	 * @return the service operation
	 */
	@Performance
	@POST
	@Path("/con")
	@Produces(MediaType.APPLICATION_XML)
	public Response getServiceOperation(String xmlParam)
	{
		log.info(":::: INICIO SERVICIO CON ::::");
		String schemaResponse= null;
		ProcessFileTO objProcessFileTO = null;
		ValidationProcessTO objValidationProcessTO = null;
		QueryParametersConsultCdpf queryParametersConsultCdpf = null;
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		StringBuilder xmlResponse= new StringBuilder();		
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
		log.info(":::: INICIO VALIDACION ARCHIVO ::::");
		try {
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(
																ComponentConstant.INTERFACE_CUSTODY_OPERATION_QUERY_CONCDF, 
																xmlParam, ComponentConstant.CDPF_SCHEMA_QUERY_CONCDF, Boolean.FALSE);
			log.info(":::: INICIO LLAMADA REMOTA ::::");
			schemaResponse= ComponentConstant.CUSTODY_OPERATION_OBEJET_RESPONSE;
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				Long idInterfaceProcess = interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(objProcessFileTO, 
																				ExternalInterfaceReceptionType.WEBSERVICE.getCode(), objLoggerUser);
				objValidationProcessTO = objProcessFileTO.getValidationProcessTO();
				for(ValidationOperationType objValidationOperationType : objValidationProcessTO.getLstOperations()){
					queryParametersConsultCdpf= (QueryParametersConsultCdpf) objValidationOperationType;
					queryParametersConsultCdpf.setIdInterfaceProcess(idInterfaceProcess);
					queryParametersConsultCdpf.setDpfInterface(true);
					schemaResponse= ComponentConstant.CUSTODY_OPERATION_OBEJET_RESPONSE;
					lstRecordValidationTypes= custodyRemoteService.get().getListRegisteredDpf(queryParametersConsultCdpf);
					objValidationProcessTO.setLstValidations(lstRecordValidationTypes);
				}
			}
			log.info(":::: FIN LLAMADA REMOTA ::::");
		} catch (Exception ex) {
			ex.printStackTrace();
			objValidationProcessTO= parameterServiceBean.generateErrorValidationProcess(queryParametersConsultCdpf, ex);
			processState= ExternalInterfaceReceptionStateType.FAILED.getCode();
		}
		finally {
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			String schemaResponseError=ComponentConstant.CUSTODY_OPERATION_OBEJET_RESPONSE_ERROR;
			if (objValidationProcessTO!=null && objValidationProcessTO.getLstOperations().size()!=0) {
				RecordValidationType type=new RecordValidationType();
				type=lstRecordValidationTypes.get(0);
				queryParametersConsultCdpf= (QueryParametersConsultCdpf) type.getOperationRef();
				if(queryParametersConsultCdpf.isError()){
					xmlResponse.append(interfaceComponentServiceBean.get().saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
							objValidationProcessTO.getLstValidations(), schemaResponseError, 
							ComponentConstant.DPF_TAG, "bloqueaDPFResponse", processState, objLoggerUser));
				}
				else{
					xmlResponse.append(interfaceComponentServiceBean.get().saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
															objValidationProcessTO.getLstValidations(), schemaResponse, 
															ComponentConstant.DPF_TAG, ComponentConstant.TAG_CONSULT_DPF, processState, objLoggerUser));
				}
			} 
			else {
				if(objValidationProcessTO!=null){
					xmlResponse.append(interfaceComponentServiceBean.get().saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
							objValidationProcessTO.getLstValidations(), schemaResponseError, 
							ComponentConstant.DPF_TAG, "bloqueaDPFResponse", processState, objLoggerUser));
				}
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");																
		}
		return Response.status(201).entity(xmlResponse.toString()).build();
	}
}
