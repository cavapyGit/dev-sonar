package com.pradera.idepositary.businessintegration.component.webservice;

import java.io.Serializable;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.transaction.TransactionSynchronizationRegistry;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.ValidationOperationType;
import com.pradera.integration.common.validation.to.ValidationProcessTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.custody.service.CustodyRemoteService;
import com.pradera.integration.component.custody.to.AccountEntryRegisterTO;
import com.pradera.integration.component.custody.to.ReverseRegisterTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.remote.ThreadRemoteContextHolder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionStateType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountEntryResource.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/09/2015
 */
@Path("/AccountEntryResource")
@Stateless
public class AccountEntryResource implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The registry. */
	@Resource
	private TransactionSynchronizationRegistry registry;
	
	/** The interface component service. */
	@EJB
	InterfaceComponentService interfaceComponentService;
	
	/** The parameter service bean. */
	@EJB
	private ParameterServiceBean parameterServiceBean;
	
	/** The custody remote service. */
	@Inject
	private Instance<CustodyRemoteService> custodyRemoteService;
	
	/** The iDepositary setup. */
	@Inject 
	@DepositarySetup 
	IdepositarySetup iDepositarySetup;
	
	/** The server configuration. */
	@Inject
	@InjectableResource(location = 	"ServerConfiguration.properties")
	private Properties serverConfiguration;
	
	/** The audits user. */
	String strAuditsUser;
	
	/** The audits IP address. */
	String strIpAddress;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		strAuditsUser = serverConfiguration.getProperty("webservice.audits.users.key");
		strIpAddress = serverConfiguration.getProperty("webservice.audits.ip");
	}			
	
	/**
	 * Creates the account entry operation.
	 *
	 * @param xmlParam the xml param
	 * @return the response
	 */
	@Performance
	@POST
	@Path("/reg")
	@Produces(MediaType.APPLICATION_XML)
	public Response createAccountEntryOperation(String xmlParam)
	{
		log.info(":::: inicio servicio reg ::::");
		ProcessFileTO objProcessFileTO = null;
		ValidationProcessTO objValidationProcessTO = null;
		AccountEntryRegisterTO accountEntryRegisterTO = null;
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		StringBuilder xmlResponse= new StringBuilder();
		
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		try {
			
			/*
			 * STEP 01:
			 * Parse the from XML file to TO object   
			 */
			
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(ComponentConstant.INTERFACE_ACCOUNT_ENTRY_DPF, 
																xmlParam, ComponentConstant.REG_DPF_NAME, Boolean.FALSE);
						
			/*
			 * STEP 02:
			 * Call the Remote service 
			 */		
			
			Long idInterfaceProcess = interfaceComponentService.saveExternalInterfaceReceptionTx(objProcessFileTO, 
																ExternalInterfaceReceptionType.WEBSERVICE.getCode(), objLoggerUser);
			
			objValidationProcessTO = objProcessFileTO.getValidationProcessTO();
			for(ValidationOperationType objValidationOperationType : objValidationProcessTO.getLstOperations()){
				accountEntryRegisterTO = (AccountEntryRegisterTO) objValidationOperationType;					
				accountEntryRegisterTO.setIdInterfaceProcess(idInterfaceProcess);
				accountEntryRegisterTO.setDpfInterface(true);
				objValidationProcessTO.getLstValidations().add(custodyRemoteService.get().createAccountEntryOperationTx(objLoggerUser, accountEntryRegisterTO));														
			}				
			
		} catch (Exception ex) {
			objValidationProcessTO= parameterServiceBean.generateErrorValidationProcess(accountEntryRegisterTO, ex);
			processState= ExternalInterfaceReceptionStateType.FAILED.getCode();
		} finally {		
			
			/*
			 * STEP 03:
			 * Generate the response XML file
			 */
			
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			if (objValidationProcessTO!=null && Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {
				xmlResponse.append(interfaceComponentService.saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
															objValidationProcessTO.getLstValidations(), objProcessFileTO.getStreamResponseFileDir(), 
															ComponentConstant.DPF_TAG, ComponentConstant.OPERATION_RECORD_TAG, processState, objLoggerUser));
			} else {
				xmlResponse.append("NO HAY DATOS");
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");																
		}
		log.info(":::: FIN SERVICIO REG ::::");
		return Response.status(201).entity(xmlResponse.toString()).build();
	}
	
	/**
	 * Reverse account entry operation.
	 *
	 * @param xmlParam the xml param
	 * @return the response
	 */
	@Performance
	@POST
	@Path("/rrg")
	@Produces(MediaType.APPLICATION_XML)
	public Response reverseAccountEntryOperation(String xmlParam)
	{
		log.info(":::: INICIO SERVICIO RRG ::::");
		ProcessFileTO objProcessFileTO = null;
		ValidationProcessTO objValidationProcessTO = null;
		ReverseRegisterTO reverseRegisterTO = null;		
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		StringBuilder xmlResponse= new StringBuilder();
		
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		
		try {
			/*
			 * STEP 01:
			 * Parse the from XML file to TO object   
			 */
			log.info(":::: INICIO VALIDACION ARCHIVO ::::");
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(
																ComponentConstant.INTERFACE_REVERSE_ACCOUNT_ENTRY_DPF, 
																xmlParam, ComponentConstant.RRG_DPF_NAME, Boolean.FALSE);
			log.info(":::: FIN VALIDACION ARCHIVO ::::");
			
			/*
			 * STEP 02:
			 * Call the Remote service to create the securities, holders, holder accounts and account entry operation 
			 */
			
			log.info(":::: INICIO LLAMADA REMOTA ::::");			
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				Long idInterfaceProcess = interfaceComponentService.saveExternalInterfaceReceptionTx(objProcessFileTO, 
																				ExternalInterfaceReceptionType.WEBSERVICE.getCode(), objLoggerUser);
				objValidationProcessTO = objProcessFileTO.getValidationProcessTO();
				for(ValidationOperationType objValidationOperationType : objValidationProcessTO.getLstOperations()){
					reverseRegisterTO = (ReverseRegisterTO) objValidationOperationType;	
					reverseRegisterTO.setIdInterfaceProcess(idInterfaceProcess);
					reverseRegisterTO.setDpfInterface(true);
					objValidationProcessTO.getLstValidations().add(custodyRemoteService.get().reverseAccountEntryOperationTx(objLoggerUser, reverseRegisterTO));																				
				}				
			}			
			log.info(":::: FIN LLAMADA REMOTA ::::");
			
		} catch (Exception ex) {
			ex.printStackTrace();
			objValidationProcessTO= parameterServiceBean.generateErrorValidationProcess(reverseRegisterTO, ex);
			processState= ExternalInterfaceReceptionStateType.FAILED.getCode();
		} finally {
			
			/*
			 * STEP 03:
			 * Generate the response XML file
			 */
			
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			if (objValidationProcessTO!= null && Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {
				xmlResponse.append(interfaceComponentService.saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
															objValidationProcessTO.getLstValidations(), objProcessFileTO.getStreamResponseFileDir(), 
															ComponentConstant.DPF_TAG, ComponentConstant.OPERATION_RECORD_TAG, processState, objLoggerUser));
			} else {
				xmlResponse.append("NO HAY DATOS");
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");
		}
		log.info(":::: FIN SERVICIO RRG ::::");		
		return Response.status(201).entity(xmlResponse.toString()).build();
	}
	
	/**
	 * Creates the account entry operation bcb tgn.
	 *
	 * @param xmlParam the xml param
	 * @return the response
	 */
	@Performance
	@POST
	@Path("/rco")
	@Produces(MediaType.APPLICATION_XML)
	public Response createAccountEntryOperationBcbTgn(String xmlParam)
	{
		log.info(":::: inicio servicio rco ::::");
		ProcessFileTO objProcessFileTO = null;
		ValidationProcessTO objValidationProcessTO = null;
		AccountEntryRegisterTO accountEntryRegisterTO = null;
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		StringBuilder xmlResponse= new StringBuilder();
		
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		try {
			
			/*
			 * STEP 01:
			 * Parse the from XML file to TO object   
			 */
			
			objProcessFileTO = parameterServiceBean.validateInterfaceFile(ComponentConstant.INTERFACE_ACCOUNT_ENTRY_WEB, 
																xmlParam, ComponentConstant.RCO_DPF_NAME, Boolean.FALSE);
						
			/*
			 * STEP 02:
			 * Call the Remote service 
			 */		
			
			Long idInterfaceProcess = interfaceComponentService.saveExternalInterfaceReceptionTx(objProcessFileTO, 
																ExternalInterfaceReceptionType.WEBSERVICE.getCode(), objLoggerUser);
			
			objValidationProcessTO = objProcessFileTO.getValidationProcessTO();
			for(ValidationOperationType objValidationOperationType : objValidationProcessTO.getLstOperations()){
				accountEntryRegisterTO = (AccountEntryRegisterTO) objValidationOperationType;					
				accountEntryRegisterTO.setIdInterfaceProcess(idInterfaceProcess);
//				accountEntryRegisterTO.setDpfInterface(true);
				try {
					objValidationProcessTO.getLstValidations().add(custodyRemoteService.get().createAccountEntryOperationTx(objLoggerUser, accountEntryRegisterTO));
				} catch (Exception ex) {
					objValidationProcessTO.getLstValidations().addAll(parameterServiceBean.generateErrorValidationProcess(accountEntryRegisterTO, ex).getLstValidations());
//					objValidationProcessTO= parameterServiceBean.generateErrorValidationProcess(accountEntryRegisterTO, ex);
					processState= ExternalInterfaceReceptionStateType.FAILED.getCode();
				}
				
			}				
			
		} catch (Exception ex) {
			objValidationProcessTO= parameterServiceBean.generateErrorValidationProcess(accountEntryRegisterTO, ex);
			processState= ExternalInterfaceReceptionStateType.FAILED.getCode();
		} finally {		
			
			/*
			 * STEP 03:
			 * Generate the response XML file
			 */
			
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			if (objValidationProcessTO != null && Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {
				xmlResponse.append(interfaceComponentService.saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
															objValidationProcessTO.getLstValidations(), objProcessFileTO.getStreamResponseFileDir(), 
															ComponentConstant.REG_PLACEMENT_TAG, ComponentConstant.OPERATION_PLACEMENT_TAG, processState, objLoggerUser));
			} else {
				xmlResponse.append("NO HAY DATOS");
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");																
		}
		log.info(":::: FIN SERVICIO RAN ::::");
		return Response.status(201).entity(xmlResponse.toString()).build();
	}
	
	/**
	 * Gets the participant by issuer dpf.
	 *
	 * @param issuerDpf the issuer dpf
	 * @return the participant by issuer dpf
	 */
	@POST
	@Path("/partByIssuer")
	@Produces(MediaType.APPLICATION_XML)
	public Long getParticipantByIssuerDpf(String issuerDpf){
		Participant p = iDepositarySetup.getParticipantByIssuerCode().get(issuerDpf);
		if(p!=null){
			return p.getIdParticipantPk();
		}
		return null;
	}
	
}
