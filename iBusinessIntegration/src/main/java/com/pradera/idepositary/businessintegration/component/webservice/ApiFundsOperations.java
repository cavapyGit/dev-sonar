package com.pradera.idepositary.businessintegration.component.webservice;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.transaction.TransactionSynchronizationRegistry;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.report.remote.ReportRegisterTO;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.swift.service.AutomaticReceptionFundsService;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.ValidationOperationType;
import com.pradera.integration.common.validation.to.ValidationProcessTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.custody.service.CustodyRemoteService;
import com.pradera.integration.component.custody.to.AccountEntryRegisterTO;
import com.pradera.integration.component.custody.to.ReverseRegisterTO;
import com.pradera.integration.component.funds.to.FundsApiRegisterTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.remote.ThreadRemoteContextHolder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.component.type.ParameterFundsOperationType;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.type.FundsOperationGroupType;
import com.pradera.model.funds.type.FundsType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionStateType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.negotiation.MechanismOperation;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountEntryResource.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/09/2015
 */
@Path("/ApiFundsOperations")
@Stateless
public class ApiFundsOperations implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The registry. */
	@Resource
	private TransactionSynchronizationRegistry registry;
	
	
	/** The iDepositary setup. */
	@Inject 
	@DepositarySetup 
	IdepositarySetup iDepositarySetup;
	
	/** The server configuration. */
	@Inject
	@InjectableResource(location = 	"ServerConfiguration.properties")
	private Properties serverConfiguration;
	
	/** The reception service. */
	@EJB
	AutomaticReceptionFundsService receptionService;
	
	/** The audits user. */
	String strAuditsUser;
	
	/** The audits IP address. */
	String strIpAddress;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		strAuditsUser = serverConfiguration.getProperty("webservice.audits.users.key");
		strIpAddress = serverConfiguration.getProperty("webservice.audits.ip");
	}			
	
	@POST
	@Path("/create/participant")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes(MediaType.APPLICATION_JSON)
	public Response registerFundsIssuer(Object dataObject) throws ServiceException {
		Gson gson = new Gson();
		try {
			FundsApiRegisterTO req = gson.fromJson(gson.toJson(dataObject), FundsApiRegisterTO.class);
			BigDecimal totalAmount= new BigDecimal("1000");
			Participant participant= new Participant();
			Issuer issuer= new Issuer();
			InstitutionCashAccount institutionCashAccount= new InstitutionCashAccount();
			InstitutionBankAccount institutionBankAccount= new InstitutionBankAccount();
			Integer currency = new Integer("0");
			String paymentReference = new String();
			MechanismOperation mechanismOperation = new MechanismOperation();
			
			FundsOperation objFundsOperation= new FundsOperation();
			
			objFundsOperation.setInstitutionCashAccount(institutionCashAccount);
			objFundsOperation.setRegistryUser("API");
			objFundsOperation.setRegistryDate(CommonsUtilities.currentDate());
			objFundsOperation.setCurrency(currency);
			objFundsOperation.setIndAutomatic(ComponentConstant.ONE);
			objFundsOperation.setIndCentralized(ComponentConstant.ZERO);
			objFundsOperation.setOperationAmount(totalAmount);
			objFundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REGISTERED.getCode());
			objFundsOperation.setFundsOperationClass(FundsType.IND_FUND_OPERATION_CLASS_TRANSFER.getCode());
			objFundsOperation.setFundsOperationGroup(MasterTableType.PARAMETER_TABLE_OPERATION_GROUP_SETTLEMENT.getCode());//PARAMETER_TABLE_OPERATION_GROUP_BENEFITS
			objFundsOperation.setFundsOperationType(ParameterFundsOperationType.OPERATIONS_COLLECTION_WITHDRAWL.getCode());
			objFundsOperation.setIndCentralized(BooleanType.NO.getCode());
			objFundsOperation.setRegistryUser("API");
			objFundsOperation.setParticipant(participant);
			objFundsOperation.setMechanismOperation(mechanismOperation);
			

			return Response.status(Status.OK.getStatusCode()).entity("Success").build();
		} catch (JsonSyntaxException e) {
			return Response.status(Status.BAD_REQUEST.getStatusCode()).entity("Invalid request body format").build();
		}
	}
	
	@POST
	@Path("/create/issuer")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes(MediaType.APPLICATION_JSON)
	public Response searchNotifications(Object dataObject) throws ServiceException {
		Gson gson = new Gson();
		try {
			FundsApiRegisterTO req = gson.fromJson(gson.toJson(dataObject), FundsApiRegisterTO.class);
			BigDecimal totalAmount= new BigDecimal("1000");
			Participant participant= new Participant();
			Issuer issuer= new Issuer();
			InstitutionCashAccount institutionCashAccount= new InstitutionCashAccount();
			InstitutionBankAccount institutionBankAccount= new InstitutionBankAccount();
			Integer currency = new Integer("0");
			String paymentReference = new String();
			MechanismOperation mechanismOperation = new MechanismOperation();
			
			FundsOperation objFundsOperation= new FundsOperation();

			
			
			Integer correlative = receptionService.getNextFundsOperationNumber(FundsType.IND_FUND_OPERATION_CLASS_RECEPTION.getCode(), FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
			
			FundsOperation fundsOperation = new FundsOperation();
			//InstitutionCashAccount participantCashAccount = allocationBenefitService.find(participantIdInstitutionCashAccount, InstitutionCashAccount.class);
			fundsOperation.setFundOperationNumber(correlative);
//			fundsOperation.setBank(participantBank);
//			fundsOperation.setInstitutionCashAccount(participantCashAccount);
//			fundsOperation.setIndCentralized(BooleanType.NO.getCode());
//			fundsOperation.setBenefitPaymentAllocation(benefitPaymentAllocation);
			fundsOperation.setOperationAmount(totalAmount);
			fundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
			fundsOperation.setIndAutomatic(BooleanType.YES.getCode());
			fundsOperation.setRegistryUser("API");
			fundsOperation.setRegistryDate(CommonsUtilities.currentDate());
			fundsOperation.setFundsOperationGroup(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
			//fundsOperation.setFundsOperationType(fundOperationType);
			fundsOperation.setFundsOperationClass(FundsType.IND_FUND_OPERATION_CLASS_RECEPTION.getCode());
			fundsOperation.setCurrency(currency);
			//fundsOperation.setCorporativeOperation(corporativeOperation);
			
			//allocationBenefitService.create(fundsOperation);
			//executeFundComponentState(fundsOperation, BusinessProcessType.FUNDS_DEPOSIT_CONFIRM.getCode(), MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
			
			

			return Response.status(Status.OK.getStatusCode()).entity("Success").build();
		} catch (JsonSyntaxException e) {
			return Response.status(Status.BAD_REQUEST.getStatusCode()).entity("Invalid request body format").build();
		}
	}
	
}
