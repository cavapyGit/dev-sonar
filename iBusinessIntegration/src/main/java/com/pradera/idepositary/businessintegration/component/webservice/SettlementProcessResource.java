package com.pradera.idepositary.businessintegration.component.webservice;

import java.io.Serializable;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.transaction.TransactionSynchronizationRegistry;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.DepositarySetupBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.ValidationOperationType;
import com.pradera.integration.common.validation.to.ValidationProcessTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.funds.to.FundsTransferRegisterTO;
import com.pradera.integration.component.settlements.service.SettlementRemoteService;
import com.pradera.integration.component.settlements.to.OverTheCounterRegisterTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.remote.ThreadRemoteContextHolder;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionStateType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlementProcessResource.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/09/2015
 */
@Path("/SettlementProcessResource")
@Stateless
public class SettlementProcessResource implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The server configuration. */
	@Inject 
	@InjectableResource(location = 	"ServerConfiguration.properties")
	private Properties serverConfiguration;
	
	/** The log. */
	@Inject private PraderaLogger log;	
	
	/** The parameter service bean. */
	@EJB private ParameterServiceBean parameterServiceBean;
	
	/** The registry. */
	@Resource private TransactionSynchronizationRegistry registry;
	
	/** The settlement remote service. */
	@Inject private Instance<SettlementRemoteService> settlementRemoteService;
	
	/** The interface component service bean. */
	@Inject private Instance<InterfaceComponentService> interfaceComponentServiceBean;
	
	/** The audits user. */
	private String strAuditsUser;
	/** The audits IP address. */
	private String strIpAddress;
	
	/** The obj validation process to. */
	ValidationProcessTO objValidationProcessTO = null;
	
	/** The depositary setup bean. */
	@Inject
	DepositarySetupBean depositarySetupBean;

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		strAuditsUser = serverConfiguration.getProperty("webservice.audits.users.key");
		strIpAddress = serverConfiguration.getProperty("webservice.audits.ip");
	}
	
	/**
	 * Register ot coperation.
	 *
	 * @param xmlParam the xml param
	 * @return the response
	 */
	@Performance
	@POST
	@Path("/tra")
	@Produces(MediaType.APPLICATION_XML)
	public Response registerOTCoperation(String xmlParam)
	{			
		log.info(":::: INICIO SERVICIO TRA ::::");		
		ProcessFileTO objProcessFileTO = null;		
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		OverTheCounterRegisterTO overTheCounterOperation = null;
		StringBuilder xmlResponse= new StringBuilder();
		
		try {
			
			/*
			 * STEP 01:
			 * Parse the from XML file to TO object   
			 */
			
			log.info(":::: INICIO VALIDACION ARCHIVO ::::");
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(
																	ComponentConstant.INTERFACE_OTC_OPERATION_DPF,
																	xmlParam, ComponentConstant.TRA_DPF_NAME, Boolean.FALSE);
			log.info(":::: FIN VALIDACION ARCHIVO ::::");
			
			/*
			 * STEP 02:
			 * Call the Remote service from TRA
			 */	
			
			log.info(":::: INICIO LLAMADA REMOTA ::::");	
			if(Validations.validateIsNotNull(objProcessFileTO)){
				Long idInterfaceProcess= interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(objProcessFileTO, 
																							ExternalInterfaceReceptionType.WEBSERVICE.getCode(), objLoggerUser);
				objValidationProcessTO = objProcessFileTO.getValidationProcessTO();
				for(ValidationOperationType objValidationOperationType : objValidationProcessTO.getLstOperations()){
					overTheCounterOperation = (OverTheCounterRegisterTO) objValidationOperationType;
					overTheCounterOperation.setIdInterfaceProcess(idInterfaceProcess);
					overTheCounterOperation.setDpfInterface(true);
					objValidationProcessTO.getLstValidations().add(settlementRemoteService.get().registerOTCoperation(overTheCounterOperation));
				}
			}
			log.info(":::: FIN LLAMADA REMOTA ::::");
		} catch (Exception e) {
			e.printStackTrace();
			objValidationProcessTO= parameterServiceBean.generateErrorValidationProcess(overTheCounterOperation, e);
			processState= ExternalInterfaceReceptionStateType.FAILED.getCode();
		}finally{
			
			/*
			 * STEP 03:
			 * Generate the response XML file
			 */
			
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			if (Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {
				xmlResponse.append(interfaceComponentServiceBean.get().saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
															objValidationProcessTO.getLstValidations(), objProcessFileTO.getStreamResponseFileDir(), 
															ComponentConstant.DPF_TAG, ComponentConstant.OPERATION_RECORD_TAG, processState, objLoggerUser));
			} else {
				xmlResponse.append("NO HAY DATOS");
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");
		}
		log.info(":::: FIN SERVICIO TRA ::::");
		return Response.status(201).entity(xmlResponse.toString()).build();
	}
	
	/**
	 * Register ot coperation bcb.
	 *
	 * @param xmlParam the xml param
	 * @return the response
	 */
	@Performance
	@POST
	@Path("/rte")
	@Produces(MediaType.APPLICATION_XML)
	public Response registerOTCoperationBcb(String xmlParam)
	{			
		log.info(":::: INICIO SERVICIO RTE ::::");		
		ProcessFileTO objProcessFileTO = null;		
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		OverTheCounterRegisterTO overTheCounterOperation = null;
		StringBuilder xmlResponse= new StringBuilder();
		
		try {
			
			/*
			 * STEP 01:
			 * Parse the from XML file to TO object   
			 */
			
			log.info(":::: INICIO VALIDACION ARCHIVO ::::");
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(
																	ComponentConstant.INTERFACE_OTC_OPERATION_WEB_BCB,
																	xmlParam, ComponentConstant.RTE_DPF_NAME, Boolean.FALSE);
			log.info(":::: FIN VALIDACION ARCHIVO ::::");
			
			/*
			 * STEP 02:
			 * Call the Remote service from TRA
			 */	
			
			log.info(":::: INICIO LLAMADA REMOTA ::::");	
			if(Validations.validateIsNotNull(objProcessFileTO)){
				Long idInterfaceProcess= interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(objProcessFileTO, 
																							ExternalInterfaceReceptionType.WEBSERVICE.getCode(), objLoggerUser);
				objValidationProcessTO = objProcessFileTO.getValidationProcessTO();
				for(ValidationOperationType objValidationOperationType : objValidationProcessTO.getLstOperations()){
					overTheCounterOperation = (OverTheCounterRegisterTO) objValidationOperationType;
					overTheCounterOperation.setIdInterfaceProcess(idInterfaceProcess);
//					overTheCounterOperation.setDpfInterface(true);
					try {
						objValidationProcessTO.getLstValidations().add(settlementRemoteService.get().registerOTCoperation(overTheCounterOperation));
					} catch (Exception ex) {
						objValidationProcessTO.getLstValidations().addAll(parameterServiceBean.generateErrorValidationProcess(overTheCounterOperation, ex).getLstValidations());
//						objValidationProcessTO= parameterServiceBean.generateErrorValidationProcess(accountEntryRegisterTO, ex);
						processState= ExternalInterfaceReceptionStateType.FAILED.getCode();
					}
				
				}
			}
			log.info(":::: FIN LLAMADA REMOTA ::::");
		} catch (Exception e) {
			e.printStackTrace();
			objValidationProcessTO= parameterServiceBean.generateErrorValidationProcess(overTheCounterOperation, e);
			processState= ExternalInterfaceReceptionStateType.FAILED.getCode();
		}finally{
			
			/*
			 * STEP 03:
			 * Generate the response XML file
			 */
			
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			if (Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {
				xmlResponse.append(interfaceComponentServiceBean.get().saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
															objValidationProcessTO.getLstValidations(), objProcessFileTO.getStreamResponseFileDir(), 
															ComponentConstant.TAG_BCB_RTE, ComponentConstant.OPERATION_TAG_BCB, processState, objLoggerUser));
			} else {
				xmlResponse.append("NO HAY DATOS");
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");
		}
		log.info(":::: FIN SERVICIO RTE ::::");
		return Response.status(201).entity(xmlParam.toString()).build();
	}
	
	/**
	 * Funds deposit lip.
	 *
	 * @param xmlParam the xml param
	 * @return the response
	 */
	@Performance
	@POST
	@Path("/dep")
	@Produces(MediaType.APPLICATION_XML)
	public Response fundsDepositLip(String xmlParam){
		ProcessFileTO objProcessFileTO = null;		
		LoggerUser objLoggerUser = CommonsUtilities.getLoggerUser(strAuditsUser, strIpAddress);
		ThreadRemoteContextHolder.put(registry.getTransactionKey().toString(), objLoggerUser);
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		FundsTransferRegisterTO fundsTransferRegisterTO = null;
		StringBuilder xmlResponse= new StringBuilder();
		
		try {
			
			/*
			 * STEP 01:
			 * Parse the from XML file to TO object   
			 */
			
			log.info(":::: INICIO VALIDACION ARCHIVO ::::");
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(
																ComponentConstant.INTERFACE_DEPOSIT_OPERATION_LIP, 
																xmlParam, ComponentConstant.LIP_DEP_NAME, Boolean.FALSE);
			log.info(":::: FIN VALIDACION ARCHIVO ::::");
			
			/*
			 * STEP 02:
			 * Call the Remote service from deposit of funds lip
			 */	
			
			log.info(":::: INICIO LLAMADA REMOTA ::::");
			if(Validations.validateIsNotNull(objProcessFileTO)){
				Long idInterfaceProcess= interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(
																							objProcessFileTO, ExternalInterfaceReceptionType.WEBSERVICE.getCode(), 
																							objLoggerUser);
				objValidationProcessTO = objProcessFileTO.getValidationProcessTO();
				for(ValidationOperationType objValidationOperationType : objValidationProcessTO.getLstOperations()){
					fundsTransferRegisterTO = (FundsTransferRegisterTO) objValidationOperationType;
					fundsTransferRegisterTO.setIdInterfaceProcess(idInterfaceProcess);
					objValidationProcessTO.getLstValidations().add(settlementRemoteService.get().depositOfFundsLip(fundsTransferRegisterTO, objLoggerUser));
				}
			}
			log.info(":::: FIN LLAMADA REMOTA ::::");
			
		} catch (Exception e) {
			e.printStackTrace();
			objValidationProcessTO= parameterServiceBean.generateErrorValidationProcess(fundsTransferRegisterTO, e);
			processState= ExternalInterfaceReceptionStateType.FAILED.getCode();
		} finally {
			
			/*
			 * STEP 03:
			 * Generate the response XML file
			 */
			
			log.info(":::: INICIO GENERACION ARCHIVO DE RESPUESTA ::::");
			if (Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstValidations())) {
				xmlResponse.append(interfaceComponentServiceBean.get().saveExternalInterfaceResponseFileTx(objProcessFileTO.getIdProcessFilePk(),
															objValidationProcessTO.getLstValidations(), objProcessFileTO.getStreamResponseFileDir(), 
															ComponentConstant.INTERFACE_TAG_ROWSET, ComponentConstant.DEPOSIT_LIP_ROW, processState, objLoggerUser));
			} else {
				xmlResponse.append("NO HAY DATOS");
			}
			log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");
		}
		
		return Response.status(201).entity(xmlParam.toString()).build();
	}
	
}