package com.pradera.idepositary.businessintegration.component.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.AccessTimeout;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.commons.contextholder.interceptor.RemoteLoggerUserInterceptor;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.helperui.service.SecuritiesQueryServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.component.BlockMarketFactOperation;
import com.pradera.model.component.BlockMovement;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountBalancePK;
import com.pradera.model.component.HolderAccountMovement;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.component.HolderMarketFactMovement;
import com.pradera.model.component.MovementBehavior;
import com.pradera.model.component.MovementType;
import com.pradera.model.component.OperationType;
import com.pradera.model.component.type.ParameterBalanceType;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.affectation.type.AffectationStateType;
import com.pradera.model.custody.affectation.type.LevelAffectationType;
import com.pradera.model.custody.blockoperation.UnblockOperation;
import com.pradera.model.custody.changeownership.ChangeOwnershipOperation;
import com.pradera.model.custody.coupongrant.CouponGrantBalance;
import com.pradera.model.custody.coupongrant.CouponGrantOperation;
import com.pradera.model.custody.coupongrant.UnblockGrantOperation;
import com.pradera.model.custody.participantunion.ParticipantUnionOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.transfersecurities.SecurityTransferOperation;
import com.pradera.model.custody.type.BlockMarketFactOperationStateType;
import com.pradera.model.funds.CashAccountMovement;
import com.pradera.model.generalparameter.type.ParameterProcessType;
import com.pradera.model.guarantees.AccountGuaranteeBalance;
import com.pradera.model.guarantees.GuaranteeMovement;
import com.pradera.model.guarantees.GuaranteeOperation;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.InternationalOperation;
import com.pradera.model.negotiation.TradeOperation;
import com.pradera.model.operations.RequestCorrelativeType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.settlement.SettlementOperation;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class AccountsComponentServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 30/03/2014
 */
@Startup
@Singleton
public class AccountsComponentServiceBean extends CrudDaoServiceBean implements AccountsComponentService {
	
	/** The logger. */
	@Inject
	private PraderaLogger logger;
	
	/** The additional component service bean. */
	@EJB
	AdditionalComponentServiceBean additionalComponentServiceBean;
	
	/** The securities query service bean. */
	@EJB
	SecuritiesQueryServiceBean securitiesQueryServiceBean;
	
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	@Inject
	Event<HolderAccountBalanceTO> eventSplitCouponNotificator;

	@Resource
	SessionContext sessionContext;
	
    /**
     * Default constructor. 
     */
    public AccountsComponentServiceBean() {
        // TODO Auto-generated constructor stub
    }

  
	/**
	 * Main method to generate movements and update the balances in the holder accounts.
	 *
	 * @param objAccountComponent the obj account component
	 * @throws ServiceException the service exception
	 * @see AccountsComponentService#executeAccountsComponent(AccountsComponentTO)
	 */
    @Override
    @AccessTimeout(unit = TimeUnit.MINUTES, value = 1440)
    @TransactionTimeout(unit = TimeUnit.MINUTES, value = 1440)
    @Interceptors(RemoteLoggerUserInterceptor.class)
    public void executeAccountsComponent(AccountsComponentTO objAccountComponent) throws ServiceException {
    	
    	try {
			/*
			 * STEP 01: 
			 * To get the list of movement types to affect the holder accounts
			 * To this it's required: the business process and the operation type
			 */
			if (Validations.validateIsNull(objAccountComponent.getIdBusinessProcess())) {
				logger.error("idBusinessProcess: " +objAccountComponent.getIdBusinessProcess()
						+ ", idOperationType: " +objAccountComponent.getIdOperationType()
						+ ", indSourceTarget: " +ComponentConstant.SOURCE);
				throw new ServiceException(ErrorServiceType.BUSINESS_PROCESS_NOFOUND, ErrorServiceType.BUSINESS_PROCESS_NOFOUND.getMessage());
			}
			if (Validations.validateIsNull(objAccountComponent.getIdOperationType())) {
				logger.error("idBusinessProcess: " +objAccountComponent.getIdBusinessProcess()
						+ ", idOperationType: " +objAccountComponent.getIdOperationType()
						+ ", indSourceTarget: " +ComponentConstant.SOURCE);
				throw new ServiceException(ErrorServiceType.OPERATION_TYPE_NOFOUND, ErrorServiceType.OPERATION_TYPE_NOFOUND.getMessage());
			}
			
			List<HolderAccountBalanceTO> lstSourceHolderAccounts= objAccountComponent.getLstSourceAccounts();
			List<HolderAccountBalanceTO> lstTargetHolderAccounts= objAccountComponent.getLstTargetAccounts();
			
			List<MovementType> lstSourceMovementTypes = null;
			List<MovementType> lstTargetMovementTypes = null;
			
			if (Validations.validateListIsNotNullAndNotEmpty(lstSourceHolderAccounts))
			{
				lstSourceMovementTypes = additionalComponentServiceBean.getListMovementTypes(objAccountComponent.getIdBusinessProcess(), 
																									objAccountComponent.getIdOperationType(), null, ComponentConstant.SOURCE);
				if (Validations.validateListIsNullOrEmpty(lstSourceMovementTypes)) 
				{
					logger.error("idBusinessProcess: " +objAccountComponent.getIdBusinessProcess()
										+ ", idOperationType: " +objAccountComponent.getIdOperationType()
										+ ", indSourceTarget: " +ComponentConstant.SOURCE);
					throw new ServiceException(ErrorServiceType.MOVEMENT_TYPE_NOFOUND, ErrorServiceType.MOVEMENT_TYPE_NOFOUND.getMessage());
				}
			}
			if (Validations.validateListIsNotNullAndNotEmpty(lstTargetHolderAccounts))
			{
				lstTargetMovementTypes = additionalComponentServiceBean.getListMovementTypes(objAccountComponent.getIdBusinessProcess(), 
																									objAccountComponent.getIdOperationType(), null, ComponentConstant.TARGET);
				if (Validations.validateListIsNullOrEmpty(lstTargetMovementTypes))
				{
					logger.error("idBusinessProcess: " +objAccountComponent.getIdBusinessProcess()
										+ ", idOperationType: " +objAccountComponent.getIdOperationType()
										+ ", indSourceTarget: " +ComponentConstant.TARGET);
					throw new ServiceException(ErrorServiceType.MOVEMENT_TYPE_NOFOUND, ErrorServiceType.MOVEMENT_TYPE_NOFOUND.getMessage());
				}
			}
			
			/*
			 * STEP 02:
			 * To generate the holder account movements for each SOURCE holder accounts as well as TARGET holder accounts 
			 */
			//to get the business objects from the PK operations 
			Map<String,Object> mpBusinessObjects = getBusinessObjects(objAccountComponent);
			//to get the operation type from the operation
			OperationType objOperationType = additionalComponentServiceBean.getOperationType(objAccountComponent.getIdOperationType());
			
			//generate the holder account movements for source holder accounts
			if (Validations.validateListIsNotNullAndNotEmpty(lstSourceMovementTypes) &&
				 Validations.validateListIsNotNullAndNotEmpty(lstSourceHolderAccounts)) 
			{
				generateHolderAccountMovements(lstSourceHolderAccounts, lstSourceMovementTypes, mpBusinessObjects, 
										 objOperationType, objAccountComponent.getIndMarketFact(), objAccountComponent.getOperationPart());
			}
			//generate the holder account movements for target holder accounts
			if (Validations.validateListIsNotNullAndNotEmpty(lstTargetMovementTypes) &&
				 Validations.validateListIsNotNullAndNotEmpty(lstTargetHolderAccounts)) 
			{
				generateHolderAccountMovements(lstTargetHolderAccounts, lstTargetMovementTypes, mpBusinessObjects, 
										 objOperationType, objAccountComponent.getIndMarketFact(), objAccountComponent.getOperationPart());
			}
			
			/*
			 * STEP 03:
			 * To update the balance inside the holder accounts
			 */
			// update the balance to source holder accounts. The reblock operations doesn't update any balances
			if (Validations.validateListIsNotNullAndNotEmpty(lstSourceMovementTypes) && 
				Validations.validateListIsNotNullAndNotEmpty(lstSourceHolderAccounts) &&
				objOperationType.getIndReblockOperation().equals(ComponentConstant.ZERO)) 
			{
				updateHolderAccountBalance(lstSourceHolderAccounts, lstSourceMovementTypes,objAccountComponent.getIndMarketFact());
			}
				
			// update the balance to target holder accounts. The reblock operations doesn't update any balances
			if (Validations.validateListIsNotNullAndNotEmpty(lstTargetMovementTypes) && 
				Validations.validateListIsNotNullAndNotEmpty(lstTargetHolderAccounts) &&
				objOperationType.getIndReblockOperation().equals(ComponentConstant.ZERO)) 
			{
				updateHolderAccountBalance(lstTargetHolderAccounts, lstTargetMovementTypes,objAccountComponent.getIndMarketFact());
			}
			
			/*
			 * STEP 04:
			 * To activate the searching for available balance to reblock operations
			 */
			BusinessProcess objBusinessProcess = additionalComponentServiceBean.getBusinessProcess(objAccountComponent.getIdBusinessProcess());
			if (BooleanType.YES.getCode().equals(objOperationType.getIndActivateReblock()) && //the operation type activate the reblock process
				 BooleanType.YES.getCode().equals(objBusinessProcess.getIndActivateReblock()))   //the business process activate the reblock process
			{
				//to set the business process to activate the reblock
				if (Validations.validateListIsNotNullAndNotEmpty(lstSourceHolderAccounts) && 
					 Validations.validateListIsNullOrEmpty(lstTargetHolderAccounts)) {
					//we activate the process to source list only if not exists target list
					sendActivateReblockBatchProcess(lstSourceHolderAccounts, objAccountComponent.getIndMarketFact());
				}
				//to send the batch process to target holder account
				if (Validations.validateListIsNotNullAndNotEmpty(lstTargetHolderAccounts)) {
					//we activate the process to target list only if not exists source list
					sendActivateReblockBatchProcess(lstTargetHolderAccounts, objAccountComponent.getIndMarketFact());
				}
			}
			/*
			 * STEP 05:
			/** for split coupon request **/
//			validateSplitCouponRequest(lstSourceHolderAccounts);
//			validateSplitCouponRequest(lstTargetHolderAccounts);
		} catch (ServiceException objException) {
			
			sessionContext.setRollbackOnly();
			objException.getConcatenatedParams().add(objAccountComponent);
			logger.error(":::::::::::::::::::: ExecutorComponentServiceBean.executeAccountsComponent ::::::::::::::::::::::::");
			logger.error("idBusinessProcess: "+objAccountComponent.getIdBusinessProcess() +
								", idOperationType: "+ objAccountComponent.getIdOperationType() +
								", idCorporativeOperation: "+ objAccountComponent.getIdCorporativeOperation() +
								", idCustodyOperation: "+ objAccountComponent.getIdCustodyOperation() +
								", idGuaranteeOperation: "+ objAccountComponent.getIdGuaranteeOperation() +
								", idTradeOperation: "+ objAccountComponent.getIdTradeOperation() +
								", idSettlementOperation: "+ objAccountComponent.getIdSettlementOperation());
			throw objException;
		}    	
    }
    
    /**
     * Method to generate the list of holder account movements to holder accounts.
     *
     * @param lstHolderAccounts the lst holder accounts
     * @param lstMovementTypes the lst movement types
     * @param mpBusinessObjects the mp business objects
     * @param objOperationType the obj operation type
     * @param indMarketFact the ind market fact
     * @param operationPart the operation part
     * @throws ServiceException the service exception
     */
    private void generateHolderAccountMovements(List<HolderAccountBalanceTO> lstHolderAccounts, List<MovementType> lstMovementTypes, 
			Map<String,Object> mpBusinessObjects, OperationType objOperationType, Integer indMarketFact, Integer operationPart) throws ServiceException {
		try {
			for (HolderAccountBalanceTO objHolderAccountBalanceTO: lstHolderAccounts){
				//we have to validate that the HolderAccountBalance exists in DB
				HolderAccountBalance objHolderAccountBalance = additionalComponentServiceBean.getHolderAccountBalance(objHolderAccountBalanceTO);
				// if doesn't exist the account balance, then we must to create it
				if (objHolderAccountBalance == null){
					objHolderAccountBalance = new HolderAccountBalance();
					objHolderAccountBalance.getId().setIdParticipantPk(objHolderAccountBalanceTO.getIdParticipant());
					objHolderAccountBalance.getId().setIdHolderAccountPk(objHolderAccountBalanceTO.getIdHolderAccount());
					objHolderAccountBalance.getId().setIdSecurityCodePk(objHolderAccountBalanceTO.getIdSecurityCode());
					this.create(objHolderAccountBalance);
				} 
				
				objHolderAccountBalanceTO.setHolderAccountBalance(objHolderAccountBalance);
				
				//its generates movements for each movement type from list
				for (MovementType objMovementType: lstMovementTypes){
					
					HolderAccountMovement objHolderAccountMovement = null;
					
					if(!ComponentConstant.ONE.equals(objMovementType.getIndMarketfactMovement())){
						
						Object[] arrSecurity =  additionalComponentServiceBean.getSecurityDetails(objHolderAccountBalanceTO.getIdSecurityCode());
						
						//Movement to be inserted
						objHolderAccountMovement = new HolderAccountMovement();
						objHolderAccountMovement.setHolderAccountBalance(objHolderAccountBalance);
						if (objHolderAccountBalanceTO.getFinalDate() != null) { //this date is special only for corporative processes
							objHolderAccountMovement.setMovementDate(objHolderAccountBalanceTO.getFinalDate());
						} else {
							objHolderAccountMovement.setMovementDate(CommonsUtilities.currentDateTime());
						}
						objHolderAccountMovement.setMovementQuantity(objHolderAccountBalanceTO.getStockQuantity());
						objHolderAccountMovement.setMovementType(objMovementType);
						objHolderAccountMovement.setCorporativeOperation((CorporativeOperation) mpBusinessObjects.get(ComponentConstant.CORPORATIVE_OPERATION));
						objHolderAccountMovement.setCustodyOperation((CustodyOperation) mpBusinessObjects.get(ComponentConstant.CUSTODY_OPERATION));
						objHolderAccountMovement.setTradeOperation((TradeOperation) mpBusinessObjects.get(ComponentConstant.TRADE_OPERATION));
						objHolderAccountMovement.setGuaranteeOperation((GuaranteeOperation) mpBusinessObjects.get(ComponentConstant.GUARANTEE_OPERATION));
						objHolderAccountMovement.setRefCustodyOperation((CustodyOperation) mpBusinessObjects.get(ComponentConstant.REF_CUSTODY_OPERATION));
						objHolderAccountMovement.setNominalValue((BigDecimal)arrSecurity[0]);
						if(objHolderAccountBalanceTO.getCashPrice()!=null) {
							objHolderAccountMovement.setOperationPrice(objHolderAccountBalanceTO.getCashPrice());	
						}else if(objHolderAccountBalanceTO.getOperationPrice()!=null) {
							objHolderAccountMovement.setOperationPrice(objHolderAccountBalanceTO.getOperationPrice()); //default value	
						}else {
							objHolderAccountMovement.setOperationPrice(objHolderAccountMovement.getNominalValue()); //default value	
						}
						objHolderAccountMovement.setOperationDate(CommonsUtilities.currentDateTime());
						objHolderAccountMovement.setIdSourceParticipant(objHolderAccountBalanceTO.getIdParticipant());
						objHolderAccountMovement.setIdTargetParticipant(objHolderAccountBalanceTO.getIdParticipant());
						
						setOperationDataMovement(objHolderAccountMovement, mpBusinessObjects, objHolderAccountBalanceTO, objOperationType, operationPart);
						this.create(objHolderAccountMovement);
						
						//if the movement belongs to block or unblock operations then we have to generate the block movement
						if (ComponentConstant.ONE.equals(objMovementType.getIndBlockMovement())) {
							generateBlockMovements(objHolderAccountMovement, objHolderAccountMovement.getCustodyOperation(), objOperationType);
						}
						// if the movement belongs to guarantee operation then we have to generate the guarantee movement
						if (ComponentConstant.ONE.equals(objMovementType.getIndGuaranteeMovement()) &&
							!objHolderAccountBalanceTO.isNormalQuantity())  //this is only for settlement movement with chain quantity. The normal quantity doesn't require guarantee movement 
						{
							objHolderAccountMovement.getGuaranteeOperation().getIdGuaranteeOperationPk();
							//we generate the guarantee movement
							GuaranteeMovement objGuaranteeMovement= generateGuaranteeMovements(objHolderAccountMovement, null, objHolderAccountMovement.getGuaranteeOperation());
							//we update the account guarantee balance
							additionalComponentServiceBean.updateAccountGuaranteeBalance(objGuaranteeMovement, lstMovementTypes);
						}
						//if the movement belong to guarantee operation then we have to generate the grant movement
						if (!ComponentConstant.ZERO.equals(objMovementType.getIndGrantMovement())) {
						//we update the coupon grant balance
							updateCouponGrantBalance(objHolderAccountMovement, objHolderAccountMovement.getCustodyOperation(), objOperationType);
						}
					}
					
					//if the movement must create movements to market fact balance
					if (ComponentConstant.ONE.equals(indMarketFact)) {
						//we create the market fact movement according the list of market fact balance and movement type
						generateMarketFactMovements(objMovementType, objHolderAccountMovement, objHolderAccountBalanceTO);
					}
					
				}
			}
		} catch (ServiceException objException){
			sessionContext.setRollbackOnly();
			logger.error(("generateHolderAccountMovements"));
			logger.error(objException.getMessage());
			throw objException;
		}
	}
    
    /**
     * Method to update the balances inside the holder accounts according the movement types.
     *
     * @param lstHolderAccounts the lst holder accounts
     * @param lstMovementTypes the lst movement types
     * @param indMarketFact 
     * @throws ServiceException the service exception
     */
	private void updateHolderAccountBalance(List<HolderAccountBalanceTO> lstHolderAccounts, List<MovementType> lstMovementTypes, Integer indMarketFact) throws ServiceException
	{
		// Each account balance must to be affected by the movements 
		for (int j=0; j<lstMovementTypes.size(); ++j)
		{
			MovementType objMovementType = lstMovementTypes.get(j);
			// we get the movement behaviors to affects the corresponding balances inside the account balance 
			List<MovementBehavior> lstMovementBehaviors = additionalComponentServiceBean.getListMovementBehavior(objMovementType.getIdMovementTypePk());
			
			for (int i=0; i< lstHolderAccounts.size(); ++i)
			{
				
				HolderAccountBalanceTO holderAccountBalanceTO = lstHolderAccounts.get(i);
				HolderAccountBalance objHolderAccountBalance = (HolderAccountBalance) holderAccountBalanceTO.getHolderAccountBalance();
				BigDecimal movementQuantity= holderAccountBalanceTO.getStockQuantity();
				//The way to affects each balance inside the account balance according the behavior
				
				if(!ComponentConstant.ONE.equals(objMovementType.getIndMarketfactMovement())){
					try{
						
						for (int k=0; k<lstMovementBehaviors.size(); ++k)
						{
							MovementBehavior objMovementBehavior = lstMovementBehaviors.get(k);
							final int balanceType = objMovementBehavior.getBalanceType().getIdBalanceTypePk().intValue();
							Long behavior = objMovementBehavior.getIdBehavior();
							
							switch (balanceType)
							{
								case ComponentConstant.TOTAL_BALANCE:
									logger.info("Balance to modify :" + ParameterBalanceType.TOTAL_BALANCE.getDescripcion());
									objHolderAccountBalance.setTotalBalance(additionalComponentServiceBean.updateBalance(behavior, 
											objHolderAccountBalance.getTotalBalance(), movementQuantity, balanceType));
								break;
								case ComponentConstant.AVAILABLE_BALANCE:
									logger.info("Balance to modify :" + ParameterBalanceType.AVAILABLE_BALANCE.getDescripcion());
									objHolderAccountBalance.setAvailableBalance(additionalComponentServiceBean.updateBalance(behavior, 
											objHolderAccountBalance.getAvailableBalance(), movementQuantity, balanceType));
								break;
								case ComponentConstant.TRANSIT_BALANCE:
									logger.info("Balance to modify :" + ParameterBalanceType.TRANSIT_BALANCE.getDescripcion());
									objHolderAccountBalance.setTransitBalance(additionalComponentServiceBean.updateBalance(behavior,
											objHolderAccountBalance.getTransitBalance(), movementQuantity, balanceType));
								break;
								case ComponentConstant.PAWN_BALANCE:
									logger.info("Balance to modify :" + ParameterBalanceType.PAWN_BALANCE.getDescripcion());
									objHolderAccountBalance.setPawnBalance(additionalComponentServiceBean.updateBalance(behavior, 
											objHolderAccountBalance.getPawnBalance(), movementQuantity, balanceType));
								break;
								case ComponentConstant.BAN_BALANCE:
									logger.info("Balance to modify :" + ParameterBalanceType.BAN_BALANCE.getDescripcion());
									objHolderAccountBalance.setBanBalance(additionalComponentServiceBean.updateBalance(behavior, 
											objHolderAccountBalance.getBanBalance(), movementQuantity, balanceType));
								break;
								case ComponentConstant.OTHERS_BALANCE:
									logger.info("Balance to modify :" + ParameterBalanceType.OTHERS_BALANCE.getDescripcion());
									objHolderAccountBalance.setOtherBlockBalance(additionalComponentServiceBean.updateBalance(behavior, 
											objHolderAccountBalance.getOtherBlockBalance(), movementQuantity, balanceType));
								break;
								case ComponentConstant.RESERVE_BALANCE:
									logger.info("Balance to modify :" + ParameterBalanceType.RESERVE_BALANCE.getDescripcion());
									objHolderAccountBalance.setReserveBalance(additionalComponentServiceBean.updateBalance(behavior, 
											objHolderAccountBalance.getReserveBalance(), movementQuantity, balanceType));
								break;
								case ComponentConstant.OPPOSITION_BALANCE:
									logger.info("Balance to modify :" + ParameterBalanceType.OPPOSITION_BALANCE.getDescripcion());
									objHolderAccountBalance.setOppositionBalance(additionalComponentServiceBean.updateBalance(behavior, 
											objHolderAccountBalance.getOppositionBalance(), movementQuantity, balanceType));
								break;
								case ComponentConstant.ACCREDITATION_BALANCE:
									logger.info("Balance to modify :" + ParameterBalanceType.ACCREDITATION_BALANCE.getDescripcion());
									objHolderAccountBalance.setAccreditationBalance(additionalComponentServiceBean.updateBalance(behavior, 
											objHolderAccountBalance.getAccreditationBalance(), movementQuantity, balanceType));
								break;
								case ComponentConstant.REPORTING_BALANCE:
									logger.info("Balance to modify :" + ParameterBalanceType.REPORTING_BALANCE.getDescripcion());
									objHolderAccountBalance.setReportingBalance(additionalComponentServiceBean.updateBalance(behavior,
											objHolderAccountBalance.getReportingBalance(), movementQuantity, balanceType));
								break;
								case ComponentConstant.REPORTED_BALANCE:
									logger.info("Balance to modify :" + ParameterBalanceType.REPORTED_BALANCE.getDescripcion());
									objHolderAccountBalance.setReportedBalance(additionalComponentServiceBean.updateBalance(behavior,
											objHolderAccountBalance.getReportedBalance(), movementQuantity, balanceType));
								break;
								case ComponentConstant.MARGIN_BALANCE:
									logger.info("Balance to modify :" + ParameterBalanceType.MARGIN_BALANCE.getDescripcion());
									objHolderAccountBalance.setMarginBalance(additionalComponentServiceBean.updateBalance(behavior,
											objHolderAccountBalance.getMarginBalance(), movementQuantity, balanceType));
								break;
								case ComponentConstant.PURCHASE_BALANCE:
									logger.info("Balance to modify :" + ParameterBalanceType.PURCHASE_BALANCE.getDescripcion());
									objHolderAccountBalance.setPurchaseBalance(additionalComponentServiceBean.updateBalance(behavior,
											objHolderAccountBalance.getPurchaseBalance(), movementQuantity, balanceType));
								break;
								case ComponentConstant.SALE_BALANCE:
									logger.info("Balance to modify :" + ParameterBalanceType.SALE_BALANCE.getDescripcion());
									objHolderAccountBalance.setSaleBalance(additionalComponentServiceBean.updateBalance(behavior,
											objHolderAccountBalance.getSaleBalance(), movementQuantity, balanceType));
								break;
								case ComponentConstant.LENDER_BALANCE:
									logger.info("Balance to modify :" + ParameterBalanceType.LENDER_BALANCE.getDescripcion());
									objHolderAccountBalance.setLenderBalance(additionalComponentServiceBean.updateBalance(behavior,
											objHolderAccountBalance.getLenderBalance(), movementQuantity, balanceType));
								break;
								case ComponentConstant.LOANABLE_BALANCE:
									logger.info("Balance to modify :" + ParameterBalanceType.LOANABLE_BALANCE.getDescripcion());
									objHolderAccountBalance.setLoanableBalance(additionalComponentServiceBean.updateBalance(behavior,
											objHolderAccountBalance.getLoanableBalance(), movementQuantity, balanceType));
								break;
								case ComponentConstant.BORROWER_BALANCE:
									logger.info("Balance to modify :" + ParameterBalanceType.BORROWER_BALANCE.getDescripcion());
									objHolderAccountBalance.setBorrowerBalance(additionalComponentServiceBean.updateBalance(behavior,
											objHolderAccountBalance.getBorrowerBalance(), movementQuantity, balanceType));
								break;
		
							}
						}
						
					} catch (ServiceException objException){
						objException.getConcatenatedParams().add(holderAccountBalanceTO);
						sessionContext.setRollbackOnly();
						logger.error(objException.getMessage());
						logger.error(("updateHolderAccountBalance ::: idHolderAccount: "+objHolderAccountBalance.getId().getIdHolderAccountPk() 
						+", idParticipant: " + objHolderAccountBalance.getId().getIdParticipantPk())
						+", idSecurityCode: "+objHolderAccountBalance.getId().getIdSecurityCodePk());
						throw objException;
					}
					
					this.update(objHolderAccountBalance);
				}
				
				//if we must update the market fact balance
				if (BooleanType.YES.getCode().equals(indMarketFact)) {
					updateMarketFactBalance(holderAccountBalanceTO, objMovementType, lstMovementBehaviors);
				}
				
			}
		}
	}
	
	/**
	 * Send activate reblock batch process.
	 *
	 * @param lstHolderAccountBalance the lst holder account balance
	 * @param objBusinessProcess the obj business process
	 * @throws ServiceException the service exception
	 */
	private void sendActivateReblockBatchProcess(List<HolderAccountBalanceTO> lstHolderAccountBalance, Integer indMarketFact) throws ServiceException
	{
		//for each holder account we have to search for available balance 
		for (HolderAccountBalanceTO objHolderAccountBalanceTO: lstHolderAccountBalance )
		{
			//we verify if this holder account has reblock operation with balances pending to block
			boolean hasReblock= additionalComponentServiceBean.haveReblocksWithOutUnblockRequest(objHolderAccountBalanceTO);
			if (hasReblock) {
				activeReblocks(objHolderAccountBalanceTO,indMarketFact);
			}
		}
	}
	
	/**
	 * Return the map with the instance of business objects according the ID oh the business operations.
	 *
	 * @param objAccountComponent the obj account component
	 * @return Map<String,Object>
	 */
	private Map<String,Object> getBusinessObjects(AccountsComponentTO objAccountComponent)
	{
		Map<String,Object> mpBusinessObjects = new HashMap<String, Object>();
		if (objAccountComponent.getIdCorporativeOperation() != null){
			mpBusinessObjects.put(ComponentConstant.CORPORATIVE_OPERATION, 
					additionalComponentServiceBean.getCorporativeOperation(objAccountComponent.getIdCorporativeOperation()));
		}
		if (objAccountComponent.getIdCustodyOperation() != null){
			mpBusinessObjects.put(ComponentConstant.CUSTODY_OPERATION, 
					additionalComponentServiceBean.getCustodyOperation(objAccountComponent.getIdCustodyOperation()));
		}
		if (objAccountComponent.getIdRefCustodyOperation() != null){
			mpBusinessObjects.put(ComponentConstant.REF_CUSTODY_OPERATION, 
					additionalComponentServiceBean.getCustodyOperation(objAccountComponent.getIdRefCustodyOperation()));
		}
		if (objAccountComponent.getIdTradeOperation() != null){
			mpBusinessObjects.put(ComponentConstant.TRADE_OPERATION, 
					additionalComponentServiceBean.getTradeOperation(objAccountComponent.getIdTradeOperation()));
		}
		if (objAccountComponent.getIdSettlementOperation() != null){
			mpBusinessObjects.put(ComponentConstant.SETTLEMENT_OPERATION, 
					additionalComponentServiceBean.getSettlementOperation(objAccountComponent.getIdSettlementOperation()));
		}
		if (objAccountComponent.getIdGuaranteeOperation() != null){
			mpBusinessObjects.put(ComponentConstant.GUARANTEE_OPERATION, 
					additionalComponentServiceBean.getGuaranteeOperation(objAccountComponent.getIdGuaranteeOperation()));
		}
			
		return mpBusinessObjects;
	}
	

	/**
	 * Method to fill up the information about the business operation inside the movement.
	 *
	 * @param objHolderAccountMovement the obj holder account movement
	 * @param mpBusinessObjects the mp business objects
	 * @param objHolderAccountBalanceTO the obj holder account balance to
	 * @param objOperationType the obj operation type
	 * @param operationPart the operation part
	 */
	private void setOperationDataMovement(HolderAccountMovement objHolderAccountMovement, Map<String,Object> mpBusinessObjects, 
											HolderAccountBalanceTO objHolderAccountBalanceTO, OperationType objOperationType, Integer operationPart)
	{
		if (mpBusinessObjects.get(ComponentConstant.CORPORATIVE_OPERATION) != null)
		{
			CorporativeOperation objCorporativeOperation = (CorporativeOperation) mpBusinessObjects.get(ComponentConstant.CORPORATIVE_OPERATION); 
			objHolderAccountMovement.setOperationNumber(objCorporativeOperation.getIdCorporativeOperationPk());//nuevo recien agregado
			objHolderAccountMovement.setCurrency(objCorporativeOperation.getCurrency());
			objHolderAccountMovement.setNominalValue(objCorporativeOperation.getSecurities().getCurrentNominalValue());
			objHolderAccountMovement.setOperationDate(objCorporativeOperation.getRegistryDate());
			objHolderAccountMovement.setIdSourceParticipant(objHolderAccountMovement.getHolderAccountBalance().getId().getIdParticipantPk());
			objHolderAccountMovement.setIdTargetParticipant(objHolderAccountMovement.getHolderAccountBalance().getId().getIdParticipantPk());
		}
		else if (mpBusinessObjects.get(ComponentConstant.TRADE_OPERATION) != null || mpBusinessObjects.get(ComponentConstant.SETTLEMENT_OPERATION) != null)
		{
			// if we are checking a MCN or OTC operation
			if (mpBusinessObjects.get(ComponentConstant.SETTLEMENT_OPERATION) != null && objOperationType.getProcessType().equals(ParameterProcessType.NEGOTIATION_MECHANISM.getCode()))
			{
				SettlementOperation objSettlementOperation = (SettlementOperation) mpBusinessObjects.get(ComponentConstant.SETTLEMENT_OPERATION);
				
				Object[] objOperation = additionalComponentServiceBean.getSettlementOperationDetails(objSettlementOperation.getIdSettlementOperationPk(), objHolderAccountBalanceTO.getIdHolderAccount());
				
				Long idStockInchargeParticipant = (Long)objOperation[0];
				Long idSellerParticipant = (Long)objOperation[1];
				Long idBuyerParticipant = (Long)objOperation[2];
				Integer idRole = (Integer)objOperation[7];
				Long idTradeOperation = (Long)objOperation[8];
				if (ComponentConstant.SALE_ROLE.equals(idRole)) {
					objHolderAccountMovement.setIdSourceParticipant(idStockInchargeParticipant);
					objHolderAccountMovement.setIdTargetParticipant(idBuyerParticipant);
				} else if (ComponentConstant.PURCHARSE_ROLE.equals(idRole)) {
					objHolderAccountMovement.setIdSourceParticipant(idSellerParticipant);
					objHolderAccountMovement.setIdTargetParticipant(idStockInchargeParticipant);
				}
				
				objHolderAccountMovement.setTradeOperation(new TradeOperation(idTradeOperation));
				objHolderAccountMovement.setIdMechanism((Long)objOperation[3]);
				objHolderAccountMovement.setIdModality((Long)objOperation[4]);
				objHolderAccountMovement.setOperationDate((Date)objOperation[5]);
				objHolderAccountMovement.setOperationNumber((Long)objOperation[6]);
				objHolderAccountMovement.setCurrency(objSettlementOperation.getSettlementCurrency());
				objHolderAccountMovement.setOperationPrice(objSettlementOperation.getSettlementPrice());
				//we verify if this movement has guarantee operation
				if (objHolderAccountBalanceTO.getIdGuaranteeOperation() != null) {
					GuaranteeOperation objGuaranteeOperation= additionalComponentServiceBean.getGuaranteeOperation(
							objHolderAccountBalanceTO.getIdGuaranteeOperation());
					objHolderAccountMovement.setGuaranteeOperation(objGuaranteeOperation);
				}
			}
			// if we are checking an International operation
			else if (mpBusinessObjects.get(ComponentConstant.TRADE_OPERATION) != null && objOperationType.getProcessType().equals(ParameterProcessType.NEGOTIATION_INTERNATIONAL.getCode()))
			{
				TradeOperation objTradeOperation = (TradeOperation) mpBusinessObjects.get(ComponentConstant.TRADE_OPERATION);
				
				InternationalOperation objInternationalOperation = additionalComponentServiceBean.getInternationalOperation(objTradeOperation.getIdTradeOperationPk()); 
				objHolderAccountMovement.setCurrency(objInternationalOperation.getCurrency());
				objHolderAccountMovement.setIdSourceParticipant(objHolderAccountMovement.getHolderAccountBalance().getId().getIdParticipantPk());
				objHolderAccountMovement.setIdTargetParticipant(objHolderAccountMovement.getHolderAccountBalance().getId().getIdParticipantPk());
				objHolderAccountMovement.setNominalValue(objInternationalOperation.getSecurities().getCurrentNominalValue());
				objHolderAccountMovement.setOperationDate(objInternationalOperation.getOperationDate());
				objHolderAccountMovement.setOperationNumber(objInternationalOperation.getOperationNumber());
				objHolderAccountMovement.setOperationPrice(objInternationalOperation.getOperationPrice());
			}
		}
		else if (mpBusinessObjects.get(ComponentConstant.CUSTODY_OPERATION) != null)
		{
			CustodyOperation objCustodyOperation = (CustodyOperation) mpBusinessObjects.get(ComponentConstant.CUSTODY_OPERATION);
			// if we are working a security transfer
			if (objOperationType.getProcessType().equals(ParameterProcessType.SECURITIES_TRANSFER.getCode()))
			{
				SecurityTransferOperation objSecurityTransferOperation = additionalComponentServiceBean.getSecurityTransferOperation(
																							objCustodyOperation.getIdCustodyOperationPk());
				objHolderAccountMovement.setIdSourceParticipant(objSecurityTransferOperation.getSourceParticipant().getIdParticipantPk());
				objHolderAccountMovement.setIdTargetParticipant(objSecurityTransferOperation.getTargetParticipant().getIdParticipantPk());
				objHolderAccountMovement.setNominalValue(objSecurityTransferOperation.getSecurities().getCurrentNominalValue());
				objHolderAccountMovement.setOperationDate(objCustodyOperation.getOperationDate());
				objHolderAccountMovement.setOperationNumber(objCustodyOperation.getOperationNumber());
			}
			// if we are working an union of participants
			else if (objOperationType.getProcessType().equals(ParameterProcessType.UNION_PARTICIPANT.getCode()))
			{
				ParticipantUnionOperation objParticipantUnionOperation = additionalComponentServiceBean.getParticipantUnionOperation(
																							objCustodyOperation.getIdCustodyOperationPk());
				objHolderAccountMovement.setIdSourceParticipant(objParticipantUnionOperation.getSourceParticipant().getIdParticipantPk());
				objHolderAccountMovement.setIdTargetParticipant(objParticipantUnionOperation.getTargetParticipant().getIdParticipantPk());
				objHolderAccountMovement.setOperationDate(objCustodyOperation.getOperationDate());
				objHolderAccountMovement.setOperationNumber(objCustodyOperation.getOperationNumber());
			}
			// if we are working a change of ownership
			else if (objOperationType.getProcessType().equals(ParameterProcessType.CHANGE_OWNERSHIP_SECURITIES.getCode()))
			{
				ChangeOwnershipOperation objChangeOwnershipOperation = additionalComponentServiceBean.getChangeOwnershipOperation(
																							objCustodyOperation.getIdCustodyOperationPk());
				objHolderAccountMovement.setIdSourceParticipant(objChangeOwnershipOperation.getSourceParticipant().getIdParticipantPk());
				objHolderAccountMovement.setIdTargetParticipant(objChangeOwnershipOperation.getTargetParticipant().getIdParticipantPk());
				objHolderAccountMovement.setOperationDate(objCustodyOperation.getOperationDate());
				objHolderAccountMovement.setOperationNumber(objCustodyOperation.getOperationNumber());
			}
			// if we are working a block, unblock, accreditation, retirement, dematerialization, rectification
			else if (objOperationType.getProcessType().equals(ParameterProcessType.SECURITIES_BLOCK.getCode()) || 
					 objOperationType.getProcessType().equals(ParameterProcessType.SECURITIES_UNBLOCK.getCode()) ||
					 objOperationType.getProcessType().equals(ParameterProcessType.SECURITIES_ACCREDITATION.getCode()) || 
					 objOperationType.getProcessType().equals(ParameterProcessType.SECURITIES_RETIREMENT.getCode()) ||
					 objOperationType.getProcessType().equals(ParameterProcessType.SECURITIES_DEMATERIALIZATION.getCode()) || 
					 objOperationType.getProcessType().equals(ParameterProcessType.RECTIFICATION_OPERATIONS.getCode()) ||
					 objOperationType.getProcessType().equals(ParameterProcessType.COUPON_GRANT_BLOCK.getCode()) ||
					 objOperationType.getProcessType().equals(ParameterProcessType.COUPON_GRANT_UNBLOCK.getCode()) ||
					 objOperationType.getProcessType().equals(ParameterProcessType.COUPON_DETACHMENT.getCode()))
			{
				objHolderAccountMovement.setIdSourceParticipant(objHolderAccountMovement.getHolderAccountBalance().getId().getIdParticipantPk());
				objHolderAccountMovement.setIdTargetParticipant(objHolderAccountMovement.getHolderAccountBalance().getId().getIdParticipantPk());
				objHolderAccountMovement.setOperationDate(objCustodyOperation.getOperationDate());
				objHolderAccountMovement.setOperationNumber(objCustodyOperation.getOperationNumber());
			}
		}
		else if (mpBusinessObjects.get(ComponentConstant.GUARANTEE_OPERATION) != null)
		{
			GuaranteeOperation objGuaranteeOperation = (GuaranteeOperation) mpBusinessObjects.get(ComponentConstant.GUARANTEE_OPERATION);
			objHolderAccountMovement.setCurrency(objGuaranteeOperation.getCurrency());
			objHolderAccountMovement.setIdSourceParticipant(objGuaranteeOperation.getParticipant().getIdParticipantPk());
			objHolderAccountMovement.setIdTargetParticipant(objGuaranteeOperation.getParticipant().getIdParticipantPk());
			objHolderAccountMovement.setOperationDate(CommonsUtilities.currentDateTime());
			objHolderAccountMovement.setOperationNumber(objGuaranteeOperation.getIdGuaranteeOperationPk());
			//objHolderAccountMovement.setTradeOperation(objGuaranteeOperation.getHolderAccountOperation().getMechanismOperation().getTradeOperation());
		}
	}
	
	/**
	 * Generate market fact movements.
	 * @param objMovementType 
	 *
	 * @param objHolderAccountMovement the obj holder account movement
	 * @param objHolderAccountBalanceTO the obj holder account balance to
	 * @throws ServiceException the service exception
	 */
	private void generateMarketFactMovements(MovementType objMovementType, HolderAccountMovement objHolderAccountMovement, HolderAccountBalanceTO objHolderAccountBalanceTO) throws ServiceException {
		
		List<MarketFactAccountTO> lstMarketFactAccounts= objHolderAccountBalanceTO.getLstMarketFactAccounts();
		Long idParticipant= objHolderAccountBalanceTO.getIdParticipant();
		Long idHolderAccount= objHolderAccountBalanceTO.getIdHolderAccount();
		String idSecurityCode= objHolderAccountBalanceTO.getIdSecurityCode();
		
		try {
			if (Validations.validateListIsNotNullAndNotEmpty(lstMarketFactAccounts)) 
			{
			for (MarketFactAccountTO objMarketFactAccountTO: lstMarketFactAccounts) {
				Date marketDate= objMarketFactAccountTO.getMarketDate();
				BigDecimal marketRate= objMarketFactAccountTO.getMarketRate();
				BigDecimal marketPrice= objMarketFactAccountTO.getMarketPrice();
				if (Validations.validateIsNull(marketRate) && Validations.validateIsNull(marketPrice)) { //one of these parameters is mandatory
					logger.error("idParticipant: " +idParticipant
					+ ", idHolderAccount: " +idHolderAccount
					+ ", idSecurityCode: " +idSecurityCode
					+ ", marketDate: " +marketDate
					+ ", marketRate: " +marketRate
					+ ", marketPrice: " +marketPrice);
					throw new ServiceException(ErrorServiceType.NO_MARKET_RATE_PRICE_FOUND, ErrorServiceType.NO_MARKET_RATE_PRICE_FOUND.getMessage());
				}
				//we get the information about the sub holder account
				HolderMarketFactBalance objHolderMarketFactBalance= null;
				objHolderMarketFactBalance = additionalComponentServiceBean.getMarketFactBalance(idParticipant, idHolderAccount, idSecurityCode, marketDate, marketRate, marketPrice);
				
				if (Validations.validateIsNull(objHolderMarketFactBalance)) {
					// if the sub account doesn't exist then we must create a new one
					objHolderMarketFactBalance = new HolderMarketFactBalance();
					objHolderMarketFactBalance.setHolderAccountBalance((HolderAccountBalance)objHolderAccountBalanceTO.getHolderAccountBalance());
					objHolderMarketFactBalance.setIndActive(ComponentConstant.ONE);
					objHolderMarketFactBalance.setMarketDate(marketDate);
					objHolderMarketFactBalance.setMarketPrice(marketPrice);
					objHolderMarketFactBalance.setMarketRate(marketRate);
					this.create(objHolderMarketFactBalance);
				}
				objMarketFactAccountTO.setObjHolderMarketFactBalance(objHolderMarketFactBalance);
				
				HolderMarketFactMovement objHolderMarketFactMovement = new HolderMarketFactMovement();
				objHolderMarketFactMovement.setHolderAccount(new HolderAccount(idHolderAccount));
				objHolderMarketFactMovement.setMarketDate(objMarketFactAccountTO.getMarketDate());
				objHolderMarketFactMovement.setMarketPrice(objMarketFactAccountTO.getMarketPrice());
				objHolderMarketFactMovement.setMarketRate(objMarketFactAccountTO.getMarketRate());
				objHolderMarketFactMovement.setMovementQuantity(objMarketFactAccountTO.getQuantity());
				objHolderMarketFactMovement.setMovementType(objMovementType);
				objHolderMarketFactMovement.setParticipant(new Participant(idParticipant));
				objHolderMarketFactMovement.setSecurities(new Security(idSecurityCode));
				if(objHolderAccountMovement!=null){
					objHolderMarketFactMovement.setHolderAccountMovement(objHolderAccountMovement);
					objHolderMarketFactMovement.setMovementDate(objHolderAccountMovement.getMovementDate());
				}else{
					objHolderMarketFactMovement.setMovementDate(CommonsUtilities.currentDateTime());
				}
				this.create(objHolderMarketFactMovement);
			}
		 }else{
			 logger.error("idParticipant: " +idParticipant
						+ ", idHolderAccount: " +idHolderAccount
						+ ", idSecurityCode: " +idSecurityCode);
			 throw new ServiceException(ErrorServiceType.BALANCE_WITHOUT_MARKETFACT);
		 }
		} catch (ServiceException e) {
			e.getConcatenatedParams().add(objHolderAccountBalanceTO);
			throw e;
		}
	 }
	
	/**
	 * Method to create the block movement to block and unblock operations according the generated holder account movement.
	 *
	 * @param objHolderAccountMovement the obj holder account movement
	 * @param objCustodyOperation the obj custody operation
	 * @param objOperationType the obj operation type
	 * @throws ServiceException the service exception
	 */
	private void generateBlockMovements(HolderAccountMovement objHolderAccountMovement, CustodyOperation objCustodyOperation, 
										OperationType objOperationType) throws ServiceException {
		BlockMovement objBlockMovement = new BlockMovement();
		// if we are working pawn, ban, other block, reserve or opposition block operation
		if (objOperationType.getProcessType().equals(ParameterProcessType.SECURITIES_BLOCK.getCode()))
		{
			BlockOperation objBlockOperation = additionalComponentServiceBean.getBlockOperation(objCustodyOperation.getIdCustodyOperationPk());
			
			HolderAccountBalance balance = objHolderAccountMovement.getHolderAccountBalance();
			HolderAccount holderAccount = find(HolderAccount.class, balance.getId().getIdHolderAccountPk());
			Security security = find(Security.class, balance.getId().getIdSecurityCodePk());
			Participant participant = find(Participant.class, balance.getId().getIdParticipantPk());
			
			objBlockMovement.setBlockOperation(objBlockOperation);
			objBlockMovement.setHolderAccount(holderAccount);
			objBlockMovement.setParticipant(participant);
			objBlockMovement.setSecurities(security);
			objBlockMovement.setHolderAccountMovement(objHolderAccountMovement);
			objBlockMovement.setMovementDate(objHolderAccountMovement.getMovementDate());
			objBlockMovement.setMovementQuantity(objHolderAccountMovement.getMovementQuantity());
			objBlockMovement.setMovementType(objHolderAccountMovement.getMovementType());
		}
		// if we are working an unblock operation
		else if (objOperationType.getProcessType().equals(ParameterProcessType.SECURITIES_UNBLOCK.getCode()))
		{
			UnblockOperation objUnblockOperation = additionalComponentServiceBean.getUnblockOperation(objCustodyOperation.getIdCustodyOperationPk());
			objBlockMovement.setHolderAccount(objHolderAccountMovement.getHolderAccountBalance().getHolderAccount());
			objBlockMovement.setParticipant(objHolderAccountMovement.getHolderAccountBalance().getParticipant());
			objBlockMovement.setSecurities(objHolderAccountMovement.getHolderAccountBalance().getSecurity());
			objBlockMovement.setHolderAccountMovement(objHolderAccountMovement);
			objBlockMovement.setMovementDate(objHolderAccountMovement.getMovementDate());
			objBlockMovement.setMovementQuantity(objHolderAccountMovement.getMovementQuantity());
			objBlockMovement.setMovementType(objHolderAccountMovement.getMovementType());
			objBlockMovement.setUnblockOperation(objUnblockOperation);
		}
		//we save the block movement
		this.create(objBlockMovement);
	}
	
	
	/**
	 * Method to create the Guarantee movements to the guarantee operation according the generated holder account movement.
	 *
	 * @param objHolderAccountMovement the obj holder account movement
	 * @param objCashAccountMovement the obj cash account movement
	 * @param objGuaranteeOperation the obj guarantee operation
	 * @return the guarantee movement
	 * @throws ServiceException the service exception
	 */
	private GuaranteeMovement generateGuaranteeMovements(HolderAccountMovement objHolderAccountMovement, CashAccountMovement objCashAccountMovement,
																							GuaranteeOperation objGuaranteeOperation) throws ServiceException
	{
		GuaranteeMovement objGuaranteeMovement = new GuaranteeMovement();
		String idSecurityCode= null;
		Long idInstitutionCashAccount= null;
		if (objGuaranteeOperation.getSecurities() != null){
			idSecurityCode= objGuaranteeOperation.getSecurities().getIdSecurityCodePk();
		}
		if (objCashAccountMovement != null){
			idInstitutionCashAccount= objCashAccountMovement.getInstitutionCashAccount().getIdInstitutionCashAccountPk();
		}
		//we get the account guarantee balance from the holder account inside the mechanism operation 
		AccountGuaranteeBalance objAccountGuaranteeBalance = additionalComponentServiceBean.getAccountGuaranteeBalance(objGuaranteeOperation.
																					getHolderAccountOperation().getIdHolderAccountOperationPk(), objGuaranteeOperation.getGuaranteeClass(),
																					idSecurityCode, idInstitutionCashAccount);
		//if doesn't exist the account guarantee then we have to create it
		if (objAccountGuaranteeBalance == null) {
			objAccountGuaranteeBalance= new AccountGuaranteeBalance();
			objAccountGuaranteeBalance.setCurrency(objGuaranteeOperation.getCurrency());
			objAccountGuaranteeBalance.setGuaranteeClass(objGuaranteeOperation.getGuaranteeClass());
			objAccountGuaranteeBalance.setHolderAccount(objGuaranteeOperation.getHolderAccount());
			objAccountGuaranteeBalance.setHolderAccountOperation(objGuaranteeOperation.getHolderAccountOperation());
			objAccountGuaranteeBalance.setParticipant(objGuaranteeOperation.getParticipant());
			if (objGuaranteeOperation.getSecurities() != null)
				objAccountGuaranteeBalance.setSecurities(objGuaranteeOperation.getSecurities());
			if (objCashAccountMovement != null)
				objAccountGuaranteeBalance.setInstitutionCashAccount(objCashAccountMovement.getInstitutionCashAccount());
			this.create(objAccountGuaranteeBalance);
		}
		objGuaranteeMovement.setAccountGuaranteeBalance(objAccountGuaranteeBalance);
		objGuaranteeMovement.setGuaranteeOperation(objGuaranteeOperation);
		objGuaranteeMovement.setCurrency(objGuaranteeOperation.getCurrency());
		objGuaranteeMovement.setGuaranteeClass(objGuaranteeOperation.getGuaranteeClass());
		objGuaranteeMovement.setGuaranteeType(objGuaranteeOperation.getGuaranteeType());
		objGuaranteeMovement.setMovementDate(CommonsUtilities.currentDateTime());
		objGuaranteeMovement.setMovementQuantity(objGuaranteeOperation.getGuaranteeBalance());
		if (objHolderAccountMovement != null) {
			objGuaranteeMovement.setHolderAccountMovement(objHolderAccountMovement);
			objGuaranteeMovement.setMovementType(objHolderAccountMovement.getMovementType());
		}
		if (objCashAccountMovement != null) {
			objGuaranteeMovement.setCashAccountMovement(objCashAccountMovement);
			objGuaranteeMovement.setMovementType(objCashAccountMovement.getMovementType());
		}
		//save the guarantee movement
		this.create(objGuaranteeMovement);
		return objGuaranteeMovement;
	}
	
	/**
	 * Update market fact balance.
	 *
	 * @param objHolderAccountBalanceTO the obj holder account balance to
	 * @param objMovementType the obj movement type
	 * @param lstMovementBehaviors the lst movement behaviors
	 * @throws ServiceException the service exception
	 */
	private void updateMarketFactBalance(HolderAccountBalanceTO objHolderAccountBalanceTO, MovementType objMovementType, List<MovementBehavior> lstMovementBehaviors) throws ServiceException{
	List<MarketFactAccountTO> lstMarketFactAccountTO= objHolderAccountBalanceTO.getLstMarketFactAccounts();
	if (Validations.validateListIsNotNullAndNotEmpty(lstMarketFactAccountTO)){
		for (MarketFactAccountTO objMarketFactAccountTO: lstMarketFactAccountTO){
			try {
				/*
				HolderMarketFactBalance objHolderMarketFactBalance = additionalComponentServiceBean.getMarketFactBalance(
										objHolderAccountBalanceTO.getIdParticipant(), objHolderAccountBalanceTO.getIdHolderAccount(), objHolderAccountBalanceTO.getIdSecurityCode(), 
										objMarketFactAccountTO.getMarketDate(), objMarketFactAccountTO.getMarketRate(), objMarketFactAccountTO.getMarketPrice());
				*/
				HolderMarketFactBalance objHolderMarketFactBalance = (HolderMarketFactBalance) objMarketFactAccountTO.getObjHolderMarketFactBalance();
				for (MovementBehavior objMovementBehavior: lstMovementBehaviors){
					final int balanceType = objMovementBehavior.getBalanceType().getIdBalanceTypePk().intValue();
					//The way to affects each balance inside the account balance according the behavior
					switch (balanceType){
						case ComponentConstant.TOTAL_BALANCE:
						logger.info("Market Fact Balance to modify :" + ParameterBalanceType.TOTAL_BALANCE.getDescripcion());
						objHolderMarketFactBalance.setTotalBalance(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
						objHolderMarketFactBalance.getTotalBalance(), objMarketFactAccountTO.getQuantity(), balanceType));
						break;
						case ComponentConstant.AVAILABLE_BALANCE:
						logger.info("Market Fact Balance to modify :" + ParameterBalanceType.AVAILABLE_BALANCE.getDescripcion());
						objHolderMarketFactBalance.setAvailableBalance(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
						objHolderMarketFactBalance.getAvailableBalance(), objMarketFactAccountTO.getQuantity(), balanceType));
						break;
						case ComponentConstant.TRANSIT_BALANCE:
						logger.info("Market Fact Balance to modify :" + ParameterBalanceType.TRANSIT_BALANCE.getDescripcion());
						objHolderMarketFactBalance.setTransitBalance(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
						objHolderMarketFactBalance.getTransitBalance(), objMarketFactAccountTO.getQuantity(), balanceType));
						break;
						case ComponentConstant.PAWN_BALANCE:
						logger.info("Market Fact Balance to modify :" + ParameterBalanceType.PAWN_BALANCE.getDescripcion());
						objHolderMarketFactBalance.setPawnBalance(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
						objHolderMarketFactBalance.getPawnBalance(), objMarketFactAccountTO.getQuantity(), balanceType));
						break;
						case ComponentConstant.BAN_BALANCE:
						logger.info("Market Fact Balance to modify :" + ParameterBalanceType.BAN_BALANCE.getDescripcion());
						objHolderMarketFactBalance.setBanBalance(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
						objHolderMarketFactBalance.getBanBalance(), objMarketFactAccountTO.getQuantity(), balanceType));
						break;
						case ComponentConstant.OTHERS_BALANCE:
						logger.info("Market Fact Balance to modify :" + ParameterBalanceType.OTHERS_BALANCE.getDescripcion());
						objHolderMarketFactBalance.setOtherBlockBalance(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
						objHolderMarketFactBalance.getOtherBlockBalance(), objMarketFactAccountTO.getQuantity(), balanceType));
						break;
						case ComponentConstant.RESERVE_BALANCE:
						logger.info("Market Fact Balance to modify :" + ParameterBalanceType.RESERVE_BALANCE.getDescripcion());
						objHolderMarketFactBalance.setReserveBalance(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
						objHolderMarketFactBalance.getReserveBalance(), objMarketFactAccountTO.getQuantity(), balanceType));
						break;
						case ComponentConstant.OPPOSITION_BALANCE:
						logger.info("Market Fact Balance to modify :" + ParameterBalanceType.OPPOSITION_BALANCE.getDescripcion());
						objHolderMarketFactBalance.setOppositionBalance(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
						objHolderMarketFactBalance.getOppositionBalance(), objMarketFactAccountTO.getQuantity(), balanceType));
						break;
						case ComponentConstant.ACCREDITATION_BALANCE:
						logger.info("Market Fact Balance to modify :" + ParameterBalanceType.ACCREDITATION_BALANCE.getDescripcion());
						objHolderMarketFactBalance.setAccreditationBalance(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
						objHolderMarketFactBalance.getAccreditationBalance(), objMarketFactAccountTO.getQuantity(), balanceType));
						break;
						case ComponentConstant.REPORTING_BALANCE:
						logger.info("Market Fact Balance to modify :" + ParameterBalanceType.REPORTING_BALANCE.getDescripcion());
						objHolderMarketFactBalance.setReportingBalance(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
						objHolderMarketFactBalance.getReportingBalance(), objMarketFactAccountTO.getQuantity(), balanceType));
						break;
						case ComponentConstant.REPORTED_BALANCE:
						logger.info("Market Fact Balance to modify :" + ParameterBalanceType.REPORTED_BALANCE.getDescripcion());
						objHolderMarketFactBalance.setReportedBalance(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
						objHolderMarketFactBalance.getReportedBalance(), objMarketFactAccountTO.getQuantity(), balanceType));
						break;
						case ComponentConstant.MARGIN_BALANCE:
						logger.info("Market Fact Balance to modify :" + ParameterBalanceType.MARGIN_BALANCE.getDescripcion());
						objHolderMarketFactBalance.setMarginBalance(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
						objHolderMarketFactBalance.getMarginBalance(), objMarketFactAccountTO.getQuantity(), balanceType));
						break;
						case ComponentConstant.PURCHASE_BALANCE:
						logger.info("Market Fact Balance to modify :" + ParameterBalanceType.PURCHASE_BALANCE.getDescripcion());
						objHolderMarketFactBalance.setPurchaseBalance(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
						objHolderMarketFactBalance.getPurchaseBalance(), objMarketFactAccountTO.getQuantity(), balanceType));
						break;
						case ComponentConstant.SALE_BALANCE:
						logger.info("Market Fact Balance to modify :" + ParameterBalanceType.SALE_BALANCE.getDescripcion());
						objHolderMarketFactBalance.setSaleBalance(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
						objHolderMarketFactBalance.getSaleBalance(), objMarketFactAccountTO.getQuantity(), balanceType));
						break;
						case ComponentConstant.LENDER_BALANCE:
						logger.info("Market Fact Balance to modify :" + ParameterBalanceType.LENDER_BALANCE.getDescripcion());
						objHolderMarketFactBalance.setLenderBalance(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
						objHolderMarketFactBalance.getLenderBalance(), objMarketFactAccountTO.getQuantity(), balanceType));
						break;
						case ComponentConstant.LOANABLE_BALANCE:
						logger.info("Market Fact Balance to modify :" + ParameterBalanceType.LOANABLE_BALANCE.getDescripcion());
						objHolderMarketFactBalance.setLoanableBalance(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
						objHolderMarketFactBalance.getLoanableBalance(), objMarketFactAccountTO.getQuantity(), balanceType));
						break;
						case ComponentConstant.BORROWER_BALANCE:
						logger.info("Market Fact Balance to modify :" + ParameterBalanceType.BORROWER_BALANCE.getDescripcion());
						objHolderMarketFactBalance.setBorrowerBalance(additionalComponentServiceBean.updateBalance(objMovementBehavior.getIdBehavior(), 
						objHolderMarketFactBalance.getBorrowerBalance(), objMarketFactAccountTO.getQuantity(), balanceType));
						break;
					}
				}
				
				this.update(objHolderMarketFactBalance);
				
			} catch (ServiceException objException) {
				
				objException.getConcatenatedParams().add(objHolderAccountBalanceTO);
				objException.getConcatenatedParams().add(objMarketFactAccountTO);
				
				sessionContext.setRollbackOnly();;
				logger.error(objException.getMessage());
				logger.error(":::::::::::::::::::: updateMarketFactBalance :::::::::::::::::::");
				logger.error("idHolderAccount: "+objHolderAccountBalanceTO.getIdHolderAccount() 
				+", idParticipant: " + objHolderAccountBalanceTO.getIdParticipant()
				+", idSecurityCode: "+objHolderAccountBalanceTO.getIdSecurityCode()
				+", marketDate: "+objMarketFactAccountTO.getMarketDate()
				+", marketRate: "+objMarketFactAccountTO.getMarketRate()
				+", marketPrice: "+objMarketFactAccountTO.getMarketPrice());
				throw objException;
			}
			
		 }
		}else{
			logger.error(":::::::::: " + ErrorServiceType.BALANCE_WITHOUT_MARKETFACT.getMessage() + "::::::::::");
			logger.error("idHolderAccount: " + objHolderAccountBalanceTO.getIdHolderAccount() 
						+ ", idSecurityCode: " + objHolderAccountBalanceTO.getIdSecurityCode());
			throw new ServiceException(ErrorServiceType.BALANCE_WITHOUT_MARKETFACT);
		}
		
	}
	
	/**
	 * Active reblocks.
	 *
	 * @param objHolderAccountBalanceTO the obj holder account balance to
	 */
	private void activeReblocks(HolderAccountBalanceTO objHolderAccountBalanceTO, Integer indMarketFact) throws ServiceException{
		try {
			//Recolectamos todos los rebloqueos de la cuenta	
			List<Long> lstReblocks = additionalComponentServiceBean.collectReblocks(objHolderAccountBalanceTO);
			HolderAccountBalancePK idHolAccBal= new HolderAccountBalancePK();
			idHolAccBal.setIdHolderAccountPk(objHolderAccountBalanceTO.getIdHolderAccount());
			idHolAccBal.setIdSecurityCodePk(objHolderAccountBalanceTO.getIdSecurityCode());
			idHolAccBal.setIdParticipantPk(objHolderAccountBalanceTO.getIdParticipant());
			for(Long lngIdBlockOperationPk:lstReblocks){
				//traemos el saldo disponible
				HolderAccountBalance balance = additionalComponentServiceBean.findAvailableBalance(idHolAccBal);
				if(balance != null){
					makeNewBlockForReblock(lngIdBlockOperationPk, balance, indMarketFact);
				}
				
			}
		} catch (ServiceException e) {			
			e.printStackTrace();
			logger.error("activeReblocks");
			throw e;
		}		
	}
	

	/**
	 * Make new block for reblock.
	 *
	 * @param lngIdBlockOperationPk the lng id block operation pk
	 * @param availableBalance the available balance
	 */
	private void makeNewBlockForReblock(Long lngIdBlockOperationPk, HolderAccountBalance balance, Integer indMarketFact) throws ServiceException{
		try{
			BlockOperation reblockOpe = this.find(BlockOperation.class, lngIdBlockOperationPk);
			BigDecimal availableBalance = balance.getAvailableBalance();
			/** 
			 * Verificamos que la cantidad actual sea mayor a 0
			 * y el saldo disponible sea mayor a 0
			 **/
			if(reblockOpe.getActualBlockBalance().compareTo(BigDecimal.ZERO) == 1 
					&& availableBalance.compareTo(BigDecimal.ZERO) == 1){
				
				//Se crea un nuevo bloqueo
				BlockOperation objBlockOperation = new BlockOperation();
				CustodyOperation objCustodyOperation = new CustodyOperation();
				//Se coloca la referencia del rebloqueo que genera este bloqueo
				objBlockOperation.setBlockOperationRef(reblockOpe);	
				Long operationNumber = getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_BLOCKING);
				objBlockOperation.setDocumentNumber(operationNumber);
				
				objCustodyOperation.setOperationDate(CommonsUtilities.currentDateTime());
				objCustodyOperation.setRegistryDate(CommonsUtilities.currentDateTime());
				objCustodyOperation.setApprovalDate(CommonsUtilities.currentDateTime());
				objCustodyOperation.setConfirmDate(CommonsUtilities.currentDateTime());
				objCustodyOperation.setState(AffectationStateType.BLOCKED.getCode());
				objCustodyOperation.setRegistryUser(reblockOpe.getCustodyOperation().getConfirmUser());
				objCustodyOperation.setApprovalUser(reblockOpe.getCustodyOperation().getConfirmUser());
				objCustodyOperation.setConfirmUser(reblockOpe.getCustodyOperation().getConfirmUser());
				objCustodyOperation.setOperationNumber(operationNumber);
				//Campos de auditoria
				objBlockOperation.setLastModifyUser(reblockOpe.getLastModifyUser());
				objBlockOperation.setLastModifyIp(reblockOpe.getLastModifyIp());
				objBlockOperation.setLastModifyDate(CommonsUtilities.currentDateTime());					
				objCustodyOperation.setLastModifyUser(reblockOpe.getCustodyOperation().getLastModifyUser());
				objCustodyOperation.setLastModifyIp(reblockOpe.getCustodyOperation().getLastModifyIp());
				objCustodyOperation.setLastModifyDate(CommonsUtilities.currentDateTime());					
				objBlockOperation.setLastModifyApp(reblockOpe.getLastModifyApp());					
				objCustodyOperation.setLastModifyApp(reblockOpe.getCustodyOperation().getLastModifyApp());
				objBlockOperation.setBlockState(AffectationStateType.BLOCKED.getCode());
				objBlockOperation.setBlockLevel(LevelAffectationType.BLOCK.getCode());					
				//El blockrequest para este nuevo blockoperation sera el mismo q el padre (rebloqueo)
				objBlockOperation.setBlockRequest(reblockOpe.getBlockRequest());
				//Segun el tipo de operacion que tenia el rebloqueo sera para este bloqueo
				Long idRegisterBusinessProcess= null;
				Long idConfirmBusinessProcess= null;
				if(ParameterOperationType.SECURITIES_RESERVE_REBLOCK.getCode().equals(reblockOpe.getCustodyOperation().getOperationType())) {
					objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_RESERVE_BLOCK.getCode());
				} else if(ParameterOperationType.SECURITIES_PAWN_REBLOCK.getCode().equals(reblockOpe.getCustodyOperation().getOperationType())) {
					objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_PAWN_BLOCK.getCode());
					idRegisterBusinessProcess= BusinessProcessType.SECURITY_BLOCK_PAWN_REGISTER.getCode();
					idConfirmBusinessProcess= BusinessProcessType.SECURITY_BLOCK_PAWN_CONFIRM.getCode();
				} else if(ParameterOperationType.SECURITIES_BAN_REBLOCK.getCode().equals(reblockOpe.getCustodyOperation().getOperationType())) {
					objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_BAN_BLOCK.getCode());
					idRegisterBusinessProcess= BusinessProcessType.SECURITY_BLOCK_REGISTER.getCode();
					idConfirmBusinessProcess= BusinessProcessType.SECURITY_BLOCK_CONFIRM.getCode();
				} else if(ParameterOperationType.SECURITIES_OTHERS_REBLOCK.getCode().equals(reblockOpe.getCustodyOperation().getOperationType())) {
					objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_OTHERS_BLOCK.getCode());
					idRegisterBusinessProcess= BusinessProcessType.SECURITY_BLOCK_REGISTER.getCode();
					idConfirmBusinessProcess= BusinessProcessType.SECURITY_BLOCK_CONFIRM.getCode();
				} else if(ParameterOperationType.SECURITIES_OPPOSITION_REBLOCK.getCode().equals(reblockOpe.getCustodyOperation().getOperationType())) {
					objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_OPPOSITION_BLOCK.getCode());
				}
				objBlockOperation.setCustodyOperation(objCustodyOperation);
				objBlockOperation.setBlockLevel(LevelAffectationType.BLOCK.getCode());
				//Se coloca los beneficios que tenia el rebloqueo al nuevo bloqueo
				fillBenefits(reblockOpe, objBlockOperation);					
				//Se coloca la matriz
				objBlockOperation.setParticipant(reblockOpe.getParticipant());
				objBlockOperation.setHolderAccount(reblockOpe.getHolderAccount());
				objBlockOperation.setSecurities(reblockOpe.getSecurities());
				objBlockOperation.setNominalValue(reblockOpe.getSecurities().getCurrentNominalValue());
				
				if(indMarketFact.equals(ComponentConstant.ONE)){
					objBlockOperation.setBlockMarketFactOperations(new ArrayList<BlockMarketFactOperation>());
					BigDecimal leftBalance = reblockOpe.getActualBlockBalance();
					for(HolderMarketFactBalance holderMarketFactBalance : balance.getHolderMarketfactBalance()){
						BigDecimal blockBalance = BigDecimal.ZERO;
						if( holderMarketFactBalance.getAvailableBalance().compareTo(leftBalance) >= 0){ // bloqueo menor o igual al disponible
							blockBalance = leftBalance;
						}else{ 																			// bloqueo mayor al disponible
							blockBalance = holderMarketFactBalance.getAvailableBalance();
						}
						leftBalance = leftBalance.subtract(blockBalance);
						
						BlockMarketFactOperation objBlockMarketFactOperation = new BlockMarketFactOperation();
						objBlockMarketFactOperation.setActualBlockBalance(blockBalance);
						objBlockMarketFactOperation.setOriginalBlockBalance(blockBalance);
						objBlockMarketFactOperation.setBlockMarketState(BlockMarketFactOperationStateType.REGISTER.getCode());
						objBlockMarketFactOperation.setBlockOperation(objBlockOperation);
//						objBlockMarketFactOperation.setHolderMarketFactBalance(holderMarketFactBalance);
						objBlockMarketFactOperation.setMarketDate(holderMarketFactBalance.getMarketDate());
						objBlockMarketFactOperation.setMarketPrice(holderMarketFactBalance.getMarketPrice());
						objBlockMarketFactOperation.setMarketRate(holderMarketFactBalance.getMarketRate());
						objBlockMarketFactOperation.setRegistryDate(CommonsUtilities.currentDateTime());
						objBlockMarketFactOperation.setRegistryUser(reblockOpe.getCustodyOperation().getConfirmUser());
						
						objBlockOperation.getBlockMarketFactOperations().add(objBlockMarketFactOperation);
						
						if(leftBalance.compareTo(BigDecimal.ZERO) <= 0){
							break;
						}
					}
				}
				
				BigDecimal blockBalance = BigDecimal.ZERO; 
				//Si la cantidad actual del rebloqueo es mayor al saldo disponible de la cuenta				
				if(reblockOpe.getActualBlockBalance().compareTo(availableBalance) == 1){
					blockBalance  = availableBalance;
					objBlockOperation.setActualBlockBalance(blockBalance);
					objBlockOperation.setOriginalBlockBalance(blockBalance);
					reblockOpe.setActualBlockBalance(reblockOpe.getActualBlockBalance().subtract(blockBalance));
					//reblockOpe.getBlockRequest().setCurrentBlockAmount(reblockOpe.getBlockRequest().getCurrentBlockAmount().add(availableBalance));
				}else{//Si la cantidad actual del rebloqueo es menor o igual al saldo disponible de la cuenta
					blockBalance  = reblockOpe.getActualBlockBalance();
					objBlockOperation.setActualBlockBalance(blockBalance);
					objBlockOperation.setOriginalBlockBalance(blockBalance);
					reblockOpe.setActualBlockBalance(BigDecimal.ZERO);
					reblockOpe.setBlockState(AffectationStateType.UNBLOCKED.getCode());
					//reblockOpe.getBlockRequest().setCurrentBlockAmount(reblockOpe.getBlockRequest().getCurrentBlockAmount().add(objBlockOperation.getActualBlockBalance()));					
				}
				
				if(indMarketFact.equals(ComponentConstant.ONE)){
					for(BlockMarketFactOperation rblkMarketFactOperation : reblockOpe.getBlockMarketFactOperations()){
						if(rblkMarketFactOperation.getActualBlockBalance().compareTo(blockBalance) >= 0){
							rblkMarketFactOperation.setActualBlockBalance(rblkMarketFactOperation.getActualBlockBalance().subtract(blockBalance));
							break;
						}else{
							rblkMarketFactOperation.setActualBlockBalance(BigDecimal.ZERO);
							blockBalance = blockBalance.subtract(rblkMarketFactOperation.getActualBlockBalance());
						}
					}
				}
				
				this.create(objBlockOperation);
				this.update(reblockOpe);								
				List<HolderAccountBalanceTO> accountBalanceTOs = getHolderAccountBalanceTOs(objBlockOperation);
				/**SE GENERA EL PRIMER MOVIMIENTO DEL BLOQUEO DE DISPONIBLE A TRANSITO**/
				this.executeAccountsComponent(getAccountsComponentTO(objBlockOperation.getIdBlockOperationPk(),objBlockOperation.getBlockOperationRef().getIdBlockOperationPk(),
																						 idRegisterBusinessProcess, accountBalanceTOs,
																						 objBlockOperation.getCustodyOperation().getOperationType(), indMarketFact));
				/**SE GENERA EL SEGUNDO MOVIMIENTO DEL BLOQUEO DE TRANSITO A BLOQUEO**/
				this.executeAccountsComponent(getAccountsComponentTO(objBlockOperation.getIdBlockOperationPk(),objBlockOperation.getBlockOperationRef().getIdBlockOperationPk(),
																						 idConfirmBusinessProcess, accountBalanceTOs,
																						 objBlockOperation.getCustodyOperation().getOperationType(), indMarketFact));
			}
		}catch (ServiceException e) {
			sessionContext.setRollbackOnly();
			logger.error("activeReblocks");
			throw e;
		}		
	}
	
	/**
	 * Fill benefits.
	 *
	 * @param reblockOpe the reblock ope
	 * @param objBlockOperation the obj block operation
	 */
	private void fillBenefits(BlockOperation reblockOpe, BlockOperation objBlockOperation){
		if(BooleanType.YES.getCode().equals(reblockOpe.getIndRescue()))
			objBlockOperation.setIndRescue(BooleanType.YES.getCode());
		else
			objBlockOperation.setIndRescue(BooleanType.NO.getCode());
		
		if(BooleanType.YES.getCode().equals(reblockOpe.getIndAmortization()))
			objBlockOperation.setIndAmortization(BooleanType.YES.getCode());
		else
			objBlockOperation.setIndAmortization(BooleanType.NO.getCode());
	
		if(BooleanType.YES.getCode().equals(reblockOpe.getIndCashDividend()))
			objBlockOperation.setIndCashDividend(BooleanType.YES.getCode());
		else
			objBlockOperation.setIndCashDividend(BooleanType.NO.getCode());
	
		if(BooleanType.YES.getCode().equals(reblockOpe.getIndInterest()))
			objBlockOperation.setIndInterest(BooleanType.YES.getCode());
		else
			objBlockOperation.setIndInterest(BooleanType.NO.getCode());
	
		if(BooleanType.YES.getCode().equals(reblockOpe.getIndReturnContributions()))
			objBlockOperation.setIndReturnContributions(BooleanType.YES.getCode());
		else
			objBlockOperation.setIndReturnContributions(BooleanType.NO.getCode());
	
		if(BooleanType.YES.getCode().equals(reblockOpe.getIndStockDividend()))
			objBlockOperation.setIndStockDividend(BooleanType.YES.getCode());
		else
			objBlockOperation.setIndStockDividend(BooleanType.NO.getCode());
	
		if(BooleanType.YES.getCode().equals(reblockOpe.getIndSuscription()))
			objBlockOperation.setIndSuscription(BooleanType.YES.getCode());
		else
			objBlockOperation.setIndSuscription(BooleanType.NO.getCode());
	}
	
	/**
	 * Gets the accounts component to.
	 *
	 * @param idCustodyOperation the id custody operation
	 * @param idCustodyOperationRef the id custody operation ref
	 * @param businessProcessType the business process type
	 * @param accountBalanceTOs the account balance t os
	 * @param operationType the operation type
	 * @return the accounts component to
	 */
	private AccountsComponentTO getAccountsComponentTO(Long idCustodyOperation, Long idCustodyOperationRef, 
			Long businessProcessType, List<HolderAccountBalanceTO> accountBalanceTOs, Long operationType, Integer indMarketFact){		
		AccountsComponentTO objAccountComponent = new AccountsComponentTO();		
		objAccountComponent.setIdBusinessProcess(businessProcessType);
		objAccountComponent.setIdCustodyOperation(idCustodyOperation);	
		objAccountComponent.setIdRefCustodyOperation(idCustodyOperationRef);
		objAccountComponent.setLstSourceAccounts(accountBalanceTOs);
		objAccountComponent.setIdOperationType(operationType);
		objAccountComponent.setIndMarketFact(indMarketFact);
		return objAccountComponent;
	}	
	

	/**
	 * Gets the holder account balance t os.
	 *
	 * @param objBlockOperation the obj block operation
	 * @return the holder account balance t os
	 * @throws ServiceException the service exception
	 */
	private List<HolderAccountBalanceTO> getHolderAccountBalanceTOs(BlockOperation objBlockOperation) throws ServiceException{
		List<HolderAccountBalanceTO> accountBalanceTOs = new ArrayList<HolderAccountBalanceTO>();
		HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
		holderAccountBalanceTO.setIdHolderAccount(objBlockOperation.getHolderAccount().getIdHolderAccountPk());
		holderAccountBalanceTO.setIdParticipant(objBlockOperation.getParticipant().getIdParticipantPk());
		holderAccountBalanceTO.setIdSecurityCode(objBlockOperation.getSecurities().getIdSecurityCodePk());
		holderAccountBalanceTO.setStockQuantity(objBlockOperation.getOriginalBlockBalance());
		if(Validations.validateIsNotNullAndNotEmpty(objBlockOperation.getBlockMarketFactOperations())){
			holderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
			for(BlockMarketFactOperation blockMarketFactOperation : objBlockOperation.getBlockMarketFactOperations()){
				MarketFactAccountTO objMarketFactAccountTO = new MarketFactAccountTO();
				objMarketFactAccountTO.setMarketDate(blockMarketFactOperation.getMarketDate());
				objMarketFactAccountTO.setMarketPrice(blockMarketFactOperation.getMarketPrice());
				objMarketFactAccountTO.setMarketRate(blockMarketFactOperation.getMarketRate());
				objMarketFactAccountTO.setQuantity(blockMarketFactOperation.getActualBlockBalance());
				holderAccountBalanceTO.getLstMarketFactAccounts().add(objMarketFactAccountTO);
			}
		}
		accountBalanceTOs.add(holderAccountBalanceTO);
		return accountBalanceTOs;
	}
	
	/**
	 * Update coupon grant balance.
	 *
	 * @param objHolderAccountMovement the obj holder account movement
	 * @param objCustodyOperation the obj custody operation
	 * @param objOperationType the obj operation type
	 * @throws ServiceException the service exception
	 */
	private void updateCouponGrantBalance(HolderAccountMovement objHolderAccountMovement,
			CustodyOperation objCustodyOperation,OperationType objOperationType) throws ServiceException{
		ProgramInterestCoupon objProgramInterestCoupon= null;
		try {
			//we get the Program Interest Coupon from the operation
			if (objOperationType.getProcessType().equals(ParameterProcessType.COUPON_GRANT_BLOCK.getCode())) {
				CouponGrantOperation objCouponGrantOperation = additionalComponentServiceBean.getCouponGrantOperation(objCustodyOperation.getIdCustodyOperationPk());
				objProgramInterestCoupon = objCouponGrantOperation.getProgramInterestCoupon();
			} else if (objOperationType.getProcessType().equals(ParameterProcessType.COUPON_GRANT_UNBLOCK.getCode())) {
				UnblockGrantOperation objUnblockGrantOperation = additionalComponentServiceBean.getUnblockGrantOperation(objCustodyOperation.getIdCustodyOperationPk());
				objUnblockGrantOperation.getCouponGrantOperation().getIdCouponGrantOperationPk();
				objProgramInterestCoupon = objUnblockGrantOperation.getCouponGrantOperation().getProgramInterestCoupon();
			}
			CouponGrantBalance objCouponGrantBalance=null;
			if(objProgramInterestCoupon!=null){
			//we must update or create the CouponGrantBalance to manage the coupon balance for each holder account
			objCouponGrantBalance = additionalComponentServiceBean.getCouponGrantBalance(objProgramInterestCoupon.getIdProgramInterestPk(), 
																							objHolderAccountMovement.getHolderAccountBalance().getId().getIdHolderAccountPk(),
																							objHolderAccountMovement.getMovementType().getReferenceBalanceType());
			}
			//if doesn't exists any balance to this coupon for this holder account then we must create a new one
			if(Validations.validateIsNull(objCouponGrantBalance)) {
				objCouponGrantBalance = new CouponGrantBalance();
				HolderAccount objHolderAccount = new HolderAccount();
				objHolderAccount.setIdHolderAccountPk(objHolderAccountMovement.getHolderAccountBalance().getId().getIdHolderAccountPk());
				objCouponGrantBalance.setHolderAccount(objHolderAccount);
				objCouponGrantBalance.setBalanceType(objHolderAccountMovement.getMovementType().getReferenceBalanceType());
				Participant objParticipant= new Participant();
				objParticipant.setIdParticipantPk(objHolderAccountMovement.getHolderAccountBalance().getId().getIdParticipantPk());
				objCouponGrantBalance.setParticipant(objParticipant);
				objCouponGrantBalance.setProgramInterestCoupon(objProgramInterestCoupon);
				Security objSecurity= new Security();
				objSecurity.setIdSecurityCodePk(objHolderAccountMovement.getHolderAccountBalance().getId().getIdSecurityCodePk());
				objCouponGrantBalance.setSecurity(objSecurity);
				objCouponGrantBalance.setGrantedBalance(additionalComponentServiceBean.updateBalance(new Long(objHolderAccountMovement.getMovementType().getIndGrantMovement()), 
																					objCouponGrantBalance.getGrantedBalance(), objHolderAccountMovement.getMovementQuantity(), 
																					objHolderAccountMovement.getMovementType().getReferenceBalanceType().intValue()));
				this.create(objCouponGrantBalance);
			} else {
				objCouponGrantBalance.setGrantedBalance(additionalComponentServiceBean.updateBalance(new Long(objHolderAccountMovement.getMovementType().getIndGrantMovement()), 
																					objCouponGrantBalance.getGrantedBalance(), objHolderAccountMovement.getMovementQuantity(), 
																					objHolderAccountMovement.getMovementType().getReferenceBalanceType().intValue()));
				this.update(objCouponGrantBalance);
			}
		} catch (ServiceException e) {
			sessionContext.setRollbackOnly();
			throw e;
		}
	}                          
	                         
	@SuppressWarnings("unused")
	private void validateSplitCouponRequest(List<HolderAccountBalanceTO> lstHolderAccountBalanceTO) throws ServiceException{
		if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountBalanceTO)){
			for(HolderAccountBalanceTO holderAccountBalanceTO:lstHolderAccountBalanceTO){
				eventSplitCouponNotificator.fire(holderAccountBalanceTO);
		    }
		}
	}
}
