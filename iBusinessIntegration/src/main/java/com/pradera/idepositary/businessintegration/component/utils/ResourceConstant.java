package com.pradera.idepositary.businessintegration.component.utils;

public class ResourceConstant {

	public static final Long TYPE_RAN_DPF = new Long(19);
	
	public static final String RAN_DPF = "RAN";
	
	public static final String RAN_DPF_NAME = "earlyfile_dpf.xml";
}
