package com.pradera.businessintegration.operativedaily;

import static com.pradera.model.operativedaily.type.OperativeDailyState.FINALIZED;
import static com.pradera.model.operativedaily.type.OperativeDailyState.PROCESSING;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.businessintegration.operativedaily.resources.SynchronizeBatchInterceptor;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.processes.scheduler.SynchronizedBatch;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.operativedaily.OperativeDailyDetail;
import com.pradera.model.operativedaily.OperativeDailyProcess;
import com.pradera.model.operativedaily.OperativeDailySetup;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * The Class OperativeDailyFacade.
 */
@Singleton
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class OperativeDailyProcessExecution {
	
	/** The operative daily service. */
	@EJB private OperativeDailyService operativeDailyService;
	
	/** The operative daily facade. */
	@EJB private OperativeDailyFacade operativeDailyFacade;
	
	/** The daily process execution. */
	@EJB private OperativeDailyProcessExecution dailyProcessExecution;
	
	/** The client rest service. */
	@Inject ClientRestService clientRestService;
	
	/** The transaction registry. */
	@Resource TransactionSynchronizationRegistry transactionRegistry;
	
	/**
	 * Execute main operative daily process.
	 *
	 * @param process the process
	 * @throws ServiceException the service exception
	 */
//	@Lock(LockType.READ)
	public void executeMainOperativeDailyProcess(OperativeDailyProcess process) throws ServiceException{
		/**Getting logger user*/
		final LoggerUser logUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		
//		Long operativeProcessPk = process.getIdOperativeDailyProcessPk();
//		OperativeDailyProcess operativeProcess = operativeDailyService.find(OperativeDailyProcess.class, operativeProcessPk);
		List<OperativeDailyDetail> listWithDetails = process.getOperativeDailyDetails();
//		Date processDate = process.getProcessDate();
		
		for(OperativeDailyDetail operativeDetail: listWithDetails){
			boolean isDetailFinish = operativeDetail.getProcessState().equals(FINALIZED.getCode());
			boolean isDetailRuning = operativeDetail.getProcessState().equals(PROCESSING.getCode());
			Integer indx = listWithDetails.indexOf(operativeDetail);
			if(indx.equals(listWithDetails.size()-1)){
				operativeDetail.setIndLastDetail(ComponentConstant.ONE);
			}
			OperativeDailySetup dailySetup = operativeDetail.getOperativeDailySetup();
			if(dailySetup.getOperativeDailySetup()!=null && !(isDetailFinish || isDetailRuning)){
				dailyProcessExecution.executeBusinessProcess(operativeDetail, logUser);
			}
		}
	}
	
	/**
	 * Execute business process.
	 *
	 * @param operativeDetail the operative detail
	 * @param logUser the log user
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	@Interceptors({SynchronizeBatchInterceptor.class})
	public void executeBusinessProcess(OperativeDailyDetail operativeDetail, LoggerUser logUser) throws ServiceException{
		try{
			OperativeDailySetup dailySetup = operativeDetail.getOperativeDailySetup();
			BusinessProcess bProcess = dailySetup.getBusinessProcess();
			Date processDate = operativeDetail.getOperativeDailyProcess().getProcessDate();
			SynchronizedBatch synchronizedBatch = getSyncronizedBatch(bProcess, logUser.getUserName(), processDate);
			clientRestService.excuteSyncBatchProcess(synchronizedBatch, bProcess.getModuleName());
			String validationQuery = operativeDetail.getOperativeDailySetup().getValidationQuery();
			
			if(validationQuery!=null){
				Map<String,Object> parameter = new HashMap<>();
				parameter.put("processDate", processDate);
				Integer quantity = ((BigDecimal)operativeDailyService.getObjectByNativreQuery(validationQuery, parameter)).intValue();
				
				if(!quantity.equals(ComponentConstant.ONE)){
					throw new ServiceException();
				}
			}
		}catch(ServiceException ex){
			ex.printStackTrace();
			throw ex;
		}
	}
	
	/**
	 * Gets the syncronized batch.
	 *
	 * @param bProcess the b process
	 * @param userName the user name
	 * @param processDate the process date
	 * @return the syncronized batch
	 */
	@Lock(LockType.READ)
	public SynchronizedBatch getSyncronizedBatch(BusinessProcess bProcess, String userName, Date processDate){
		SynchronizedBatch synchronizedBatch = new SynchronizedBatch();
		synchronizedBatch.setBusinessProcess(bProcess.getIdBusinessProcessPk());
		synchronizedBatch.setUserName(userName);
		
		Map<String,String> parameters = new HashMap<>();
		parameters.put(OperativeDailyConstants.PROCESS_DATE_PARAMETER, CommonsUtilities.convertDateToString(processDate));
		synchronizedBatch.setParameters(parameters);
		
		return synchronizedBatch;
	}
}