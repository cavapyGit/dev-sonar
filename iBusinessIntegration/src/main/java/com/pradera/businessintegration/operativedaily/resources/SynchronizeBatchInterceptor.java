package com.pradera.businessintegration.operativedaily.resources;

import java.io.Serializable;
import java.util.Date;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.apache.commons.lang3.exception.ExceptionUtils;

import com.pradera.businessintegration.operativedaily.OperativeDailyFacade;
import com.pradera.commons.processes.scheduler.SynchronizeBatchEvent;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.operativedaily.OperativeDailyDetail;

@SynchronizedBatchDispatcher
@Interceptor
public class SynchronizeBatchInterceptor implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EJB OperativeDailyFacade operativeDailyFacade;
	@Inject ClientRestService clientRestService;
	
	public SynchronizeBatchInterceptor(){
		
	}

	@AroundInvoke
	public Object aroundProcessInvoke(InvocationContext ic) throws Exception {
		String errorDetail = "";
		Integer indYes = BooleanType.YES.getCode();
		Integer indNot = BooleanType.NO.getCode();
		Date processDate = null;
		OperativeDailyDetail operativeDetail = getOperativeDetail(ic.getParameters());
		if(operativeDetail!=null){
			LoggerUser loggerUser = getLoggerUser(ic.getParameters());
			Long operativeDetPk = operativeDetail.getIdOperationDailyDetailPk();
			processDate = operativeDetail.getOperativeDailyProcess().getProcessDate();
			Integer indLastDet = operativeDetail.getIndLastDetail();
			Double rowQuantity = operativeDetail.getRowQuantity();
			operativeDailyFacade.startOperativeDailyLogger(operativeDetail,loggerUser);
			clientRestService.fireSynchronizedBatchEvent(getSynchronizedEvent(operativeDetPk,processDate, indYes, indNot, null,rowQuantity,indNot));
			try {
				return ic.proceed();
			}catch(Exception ex){
				errorDetail = ExceptionUtils.getMessage(ex);
				throw ex;
			}finally{
				operativeDailyFacade.finishOperativeDailyLogger(operativeDetail,errorDetail,loggerUser);
				clientRestService.fireSynchronizedBatchEvent(getSynchronizedEvent(operativeDetPk,processDate, indNot, indYes, errorDetail,rowQuantity,indLastDet));
			}
		}
		return ic.proceed();
	}
	
	/**
	 * Gets the notification event.
	 *
	 * @param eventCode the event code
	 * @param indBegin the ind begin
	 * @param indFinish the ind finish
	 * @param hasError the has error
	 * @return the notification event
	 */
	public SynchronizeBatchEvent getSynchronizedEvent(Long eventCode,Date pDate, Integer indBegin, Integer indFinish,  String errorDetail, Double rowQuant, Integer last){
		SynchronizeBatchEvent notificationEvent = new SynchronizeBatchEvent();
		notificationEvent.setIndHasError(BooleanType.get(errorDetail!=null).getBinaryValue());
		notificationEvent.setErrorDetail(errorDetail);
		notificationEvent.setEventTypeCode(eventCode);
		notificationEvent.setPercentage(ComponentConstant.ZERO);
		notificationEvent.setIsAllFinish(BooleanType.NO.getCode());
		notificationEvent.setRowQuantity(rowQuant!=null?rowQuant.longValue():ComponentConstant.ZERO);
		notificationEvent.setProcessDate(CommonsUtilities.convertDateToString(pDate,CommonsUtilities.DATE_PATTERN));
		notificationEvent.setIsAllFinish(last);
		
		if(indBegin.equals(BooleanType.YES.getCode())){
			notificationEvent.setIndBegin(BooleanType.YES.getCode());
			notificationEvent.setBeginTime(CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),CommonsUtilities.DATETIME_PATTERN));
		}else if(indBegin.equals(BooleanType.NO.getCode())){
			notificationEvent.setIndFinish(BooleanType.YES.getCode());
			notificationEvent.setFinishTime(CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),CommonsUtilities.DATETIME_PATTERN));
		}
		return notificationEvent;
	}
	
	/**
	 * Gets the operative detail.
	 *
	 * @param parameters the parameters
	 * @return the operative detail
	 */
	public OperativeDailyDetail getOperativeDetail(Object[] parameters) {
		for(Object parameter: parameters){
			if(parameter instanceof OperativeDailyDetail){
				return (OperativeDailyDetail)parameter;
			}
		}
		return null;
	}
	
	/**
	 * Gets the logger user.
	 *
	 * @param parameters the parameters
	 * @return the logger user
	 */
	public LoggerUser getLoggerUser(Object[] parameters) {
		for(Object parameter: parameters){
			if(parameter instanceof LoggerUser){
				return (LoggerUser)parameter;
			}
		}
		return null;
	}
}