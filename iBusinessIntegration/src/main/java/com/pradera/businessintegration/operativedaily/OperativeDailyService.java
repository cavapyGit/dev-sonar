package com.pradera.businessintegration.operativedaily;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.ejb.Singleton;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;


import javax.persistence.Query;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.operativedaily.OperativeDailyLogger;
import com.pradera.model.operativedaily.OperativeDailyProcess;
import com.pradera.model.operativedaily.OperativeDailySetup;

@Singleton
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class OperativeDailyService extends CrudDaoServiceBean{

	
	/**
	 * Gets the operative daily process.
	 *
	 * @param processDate the process date
	 * @return the operative daily process
	 * @throws ServiceException the service exception
	 */
	public OperativeDailyProcess getOperativeDailyProcess(Date processDate) throws ServiceException{
		/**Instance map*/
		Map<String,Object> paramters = new HashMap<>();
		/**Builder to create query*/
		StringBuilder stringQuery = new StringBuilder();
		stringQuery.append("Select proc From OperativeDailyProcess proc					");
		stringQuery.append("Inner Join Fetch proc.operativeDailyDetails det				");
		stringQuery.append("Where proc.processDate = :processDate						");
		stringQuery.append("Order By det.executionOrder									");
		/**Setting parameters*/
		paramters.put("processDate", processDate);
		
		return (OperativeDailyProcess) findObjectByQueryString(stringQuery.toString(), paramters);		
	}
	
	/**
	 * Gets the operative daily setup.
	 *
	 * @return the operative daily setup
	 * @throws ServiceException the service exception
	 */
	public OperativeDailySetup getOperativeDailySetup() throws ServiceException{
		/**Builder to create query*/
		StringBuilder stringQuery = new StringBuilder();
		stringQuery.append("Select Distinct sett From OperativeDailySetup sett			");
		stringQuery.append("Inner Join Fetch sett.operativeDailySetups det 				");
		stringQuery.append("Where sett.operativeDailySetup Is Null						");
		stringQuery.append("Order By sett.executionOrder Asc 							");
		/**Setting parameters*/
		return (OperativeDailySetup) findObjectByQueryString(stringQuery.toString(),new HashMap<String,Object>());
	}
	
	/**
	 * Gets the operative daily logger.
	 *
	 * @param operativeDailyDetail the operative daily detail
	 * @return the operative daily logger
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<OperativeDailyLogger> getOperativeDailyLogger(Long operativeDailyDetail) throws ServiceException{
		/**Instance map*/
		Map<String,Object> paramters = new HashMap<>();
		/**Builder to create query*/
		StringBuilder stringQuery = new StringBuilder();
		stringQuery.append("Select logg From OperativeDailyLogger logg						");
		stringQuery.append("Where logg.operativeDailyDetail.idOperationDailyDetailPk = :id	");
		stringQuery.append("Order By logg.idOperationDailyLoggerPk Asc 						");
		/**Setting parameters*/
		paramters.put("id", operativeDailyDetail);
		
		try{
			return (List<OperativeDailyLogger>) findListByQueryString(stringQuery.toString(),paramters);
		}catch(NoResultException ex){
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<OperativeDailyLogger> getOperativeDailyLoggerRuning(Long operativeDailyDetail) throws ServiceException{
		/**Instance map*/
		Map<String,Object> paramters = new HashMap<>();
		/**Builder to create query*/
		StringBuilder stringQuery = new StringBuilder();
		stringQuery.append("Select logg From OperativeDailyLogger logg						");
		stringQuery.append("Where logg.operativeDailyDetail.idOperationDailyDetailPk = :id	");
		stringQuery.append("	  And logg.finishTime is Null								");
		stringQuery.append("Order By logg.idOperationDailyLoggerPk Asc 						");
		/**Setting parameters*/
		paramters.put("id", operativeDailyDetail);
		
		try{
			return (List<OperativeDailyLogger>) findListByQueryString(stringQuery.toString(),paramters);
		}catch(NoResultException ex){
			return null;
		}
	}
	
	public Object getObjectByNativreQuery(String query, Map<String,Object> parameter){
		Query emQuery = (Query) em.createNativeQuery(query);
		Set<Entry<String, Object>> rawParameters = parameter.entrySet();
		for (Entry<String, Object> entry : rawParameters) {
			emQuery.setParameter(entry.getKey(), entry.getValue());
		}
		return emQuery.getSingleResult();
	}
	
}