package com.pradera.businessintegration.operativedaily;

import static com.pradera.model.operativedaily.type.OperativeDailyState.FINALIZED;
import static com.pradera.model.operativedaily.type.OperativeDailyState.WAITING;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.operativedaily.OperativeDailyDetail;
import com.pradera.model.operativedaily.OperativeDailyLogger;
import com.pradera.model.operativedaily.OperativeDailyProcess;
import com.pradera.model.operativedaily.OperativeDailySetup;
import com.pradera.model.operativedaily.type.OperativeDailyState;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * The Class OperativeDailyFacade.
 */
@Singleton
@Performance
@Path("/OperativeEntryResource")
@Interceptors(ContextHolderInterceptor.class)
public class OperativeDailyFacade {

	/** The operative daily service. */
	@EJB OperativeDailyService operativeDailyService;
	
	/** The transaction registry. */
	@Resource TransactionSynchronizationRegistry transactionRegistry;
	
	/** The batch process service facade. */
	@EJB private BatchProcessServiceFacade batchProcessServiceFacade;
	
	/**
	 * Gets the operative daily process.
	 *
	 * @param processDate the process date
	 * @return the operative daily process
	 * @throws ServiceException the service exception
	 */
	public OperativeDailyProcess getOperativeDailyProcess(Date processDate) throws ServiceException{
		return operativeDailyService.getOperativeDailyProcess(processDate);
	}
	
	/**
	 * Creates the new operative process.
	 *
	 * @param processDate the process date
	 * @return the operative daily process
	 * @throws ServiceException the service exception
	 */
	public OperativeDailyProcess createNewOperativeProcess(Date processDate) throws ServiceException{
		/**Getting loggerUser from transaction*/
		LoggerUser logUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		
		/**Initialize and setting data at object*/
		OperativeDailyProcess dailyProcess = new OperativeDailyProcess();
		
		dailyProcess.setProcessDate(processDate);
		dailyProcess.setBeginTime(CommonsUtilities.currentDateTime());
		dailyProcess.setProcessState(OperativeDailyState.REGISTER.getCode());
		
		dailyProcess.setRegisterUser(logUser.getUserName());
		dailyProcess.setRegisterDate(CommonsUtilities.currentDateTime());
		
		/**Persist on BD*/
		operativeDailyService.create(dailyProcess);
		
		/**Setup And persist details*/
		dailyProcess.setOperativeDailyDetails(createOperativeDetails(dailyProcess));
		
		return dailyProcess;
	}
	
	/**
	 * Creates the operative details.
	 *
	 * @param process the process
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<OperativeDailyDetail> createOperativeDetails(OperativeDailyProcess process) throws ServiceException{
		List<OperativeDailyDetail> operativeDetailList = new ArrayList<>();
		List<OperativeDailySetup> listOfSetup = operativeDailyService.getOperativeDailySetup().getOperativeDailySetups();
		Collections.sort(listOfSetup,COMPARE_BY_ORDER);
		for(OperativeDailySetup operativeDailySetup: listOfSetup){
			operativeDetailList.add(getOperativeDailyDetail(process,operativeDailySetup));
		}
		operativeDailyService.saveAll(operativeDetailList);
		return operativeDetailList;
	}
	
	/** The compare by time. */
	public static Comparator<OperativeDailySetup> COMPARE_BY_ORDER = new Comparator<OperativeDailySetup>() {
		 public int compare(OperativeDailySetup o1, OperativeDailySetup o2) {
				/**ORDENAMOS LOS CUPONES POR NUMERO DE CUPON*/
		        if ( o1.getExecutionOrder() < o2.getExecutionOrder()) { return -1; } 
				return 1;
			}
	    };
	
	/**	
	 * Gets the operative daily detail.
	 *
	 * @param process the process
	 * @param setup the setup
	 * @return the operative daily detail
	 */
	public OperativeDailyDetail getOperativeDailyDetail(OperativeDailyProcess process, OperativeDailySetup setup){
		OperativeDailyDetail operativeDetail = new OperativeDailyDetail();
		operativeDetail.setOperativeDailySetup(setup);
		operativeDetail.setOperativeDailyProcess(process);
		operativeDetail.setExecutionsCount(ComponentConstant.ZERO);
		operativeDetail.setBeginTime(CommonsUtilities.currentDateTime());
		operativeDetail.setExecutionOrder(setup.getExecutionOrder());
		operativeDetail.setProcessState(OperativeDailyState.REGISTER.getCode());
		
		if(setup.getNroRowQuery()!=null){
			Map<String,Object> parameter = new HashMap<>();
			parameter.put("processDate", process.getProcessDate());
			Double rowQuantity = ((BigDecimal)operativeDailyService.getObjectByNativreQuery(setup.getNroRowQuery(), parameter)).doubleValue();
			operativeDetail.setRowQuantity(rowQuantity);
		}else{
			operativeDetail.setRowQuantity(ComponentConstant.ZERO.doubleValue());
		}
		
		return operativeDetail;
	}
	
	/**
	 * Creates the operative logger.
	 *
	 * @param operativeDetail the operative detail
	 * @return the operative daily logger
	 * @throws ServiceException the service exception
	 */
	public OperativeDailyLogger createOperativeLogger(OperativeDailyDetail operativeDetail,LoggerUser loggerUser) throws ServiceException{
		OperativeDailyLogger operativeLogger = new OperativeDailyLogger();
		operativeLogger.setBeginTime(CommonsUtilities.currentDateTime());
		operativeLogger.setOperativeDailyDetail(operativeDetail);
		operativeLogger.setIndError(BooleanType.NO.getCode());
		operativeDailyService.create(operativeLogger,loggerUser);
		return operativeLogger;
	}
	
	/**
	 * Finish operative logger.
	 *
	 * @param operativeLogger the operative logger
	 * @param error the error
	 * @return the operative daily logger
	 * @throws ServiceException the service exception
	 */
	public OperativeDailyLogger finishLoggerExecution(OperativeDailyLogger operativeLogger, Object error,LoggerUser loggerUser) throws ServiceException{
		Long loggerPk = operativeLogger.getIdOperationDailyLoggerPk();
		operativeLogger = operativeDailyService.find(OperativeDailyLogger.class, loggerPk);
		operativeLogger.setFinishTime(CommonsUtilities.currentDateTime());
		operativeLogger.setIndError(BooleanType.get(!error.toString().isEmpty()).getBinaryValue());	
		operativeLogger.setErrorDetail(error.toString());
		operativeDailyService.update(operativeLogger,loggerUser);
		return operativeLogger;
	}
	
	/**
	 * Update state to operative process.
	 *
	 * @param processPk the process pk
	 * @param operativeDailyState the operative daily state
	 * @throws ServiceException the service exception
	 */
	public void updateStateToOperativeProcess(Long processPk, Integer operativeDailyState) throws ServiceException{
		OperativeDailyProcess operativeProcess = operativeDailyService.find(OperativeDailyProcess.class, processPk);
		operativeProcess.setProcessState(operativeDailyState);
		operativeDailyService.update(operativeProcess);
	}
	

	
	/**
	 * Start operative daily logger.
	 *
	 * @param operativeDetail the operative detail
	 * @throws ServiceException the service exception
	 */
	@Asynchronous
	public void startOperativeDailyLogger(OperativeDailyDetail operativeDetail, LoggerUser loggerUser) throws ServiceException{
		Long dailyDetailPk = operativeDetail.getIdOperationDailyDetailPk();
		operativeDetail = operativeDailyService.find(OperativeDailyDetail.class, dailyDetailPk);
		List<OperativeDailyLogger> loggers = operativeDailyService.getOperativeDailyLogger(dailyDetailPk);
		createOperativeLogger(operativeDetail,loggerUser);
		if(loggers==null || loggers.isEmpty()){
			operativeDetail.setExecutionsCount(ComponentConstant.ONE);
			operativeDetail.setBeginTime(CommonsUtilities.currentDateTime());
			operativeDetail.setProcessState(OperativeDailyState.PROCESSING.getCode());
		}else{
			Integer executionCount = loggers.size();
			operativeDetail.setExecutionsCount(++executionCount);
		}
	}
	
	
	/**
	 * Finish operative daily logger.
	 *
	 * @param operativeDetail the operative detail
	 * @param indError the ind error
	 * @throws ServiceException the service exception
	 */
	@Asynchronous
	public void finishOperativeDailyLogger(OperativeDailyDetail operativeDetail, Object errorDetail, LoggerUser loggerUser) throws ServiceException{
		Integer hasError = BooleanType.get(!errorDetail.toString().isEmpty()).getBinaryValue();
		Long dailyDetailPk = operativeDetail.getIdOperationDailyDetailPk();
		
		operativeDetail = operativeDailyService.find(OperativeDailyDetail.class, dailyDetailPk);
		
		for(OperativeDailyLogger procLogger: operativeDailyService.getOperativeDailyLoggerRuning(dailyDetailPk)){
			finishLoggerExecution(procLogger, errorDetail,loggerUser);
		}
		
		if(hasError.equals(BooleanType.YES.getCode())){
			operativeDetail.setProcessState(WAITING.getCode());
			updateStateToOperativeProcess(operativeDetail.getOperativeDailyProcess().getIdOperativeDailyProcessPk(), WAITING.getCode());
		}else{
			operativeDetail.setProcessState(FINALIZED.getCode());
			operativeDetail.setFinishTime(CommonsUtilities.currentDateTime());
		}
		operativeDailyService.update(operativeDetail,loggerUser);
	}
	
	/**
	 * Test batch.
	 *
	 * @param userNameParam the user name param
	 * @param ipParam the ip param
	 */
	@GET
	@Path("/startExecution/{userParam}/{ipParam}")
	public void testBatch(@PathParam("userParam")String userNameParam, @PathParam("ipParam")String ipParam ){
    	LoggerUser loggerUser = CommonsUtilities.getLoggerUser(userNameParam, ipParam);
    	//How is required_new transaction dont propagate transactionRegistry, so we propagate with threadLocal
        ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
		
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.OPERATIVE_DAILY_PROCESS.getCode());
		Map<String,Object> parameter = new HashMap<>();
		parameter.put(OperativeDailyConstants.PROCESS_DATE_PARAMETER, CommonsUtilities.convertDateToString(new Date(), CommonsUtilities.DATETIME_PATTERN));
		try {
			batchProcessServiceFacade.registerBatch(userNameParam,businessProcess,parameter);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}