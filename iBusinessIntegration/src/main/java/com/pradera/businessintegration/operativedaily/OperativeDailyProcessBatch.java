package com.pradera.businessintegration.operativedaily;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.operativedaily.OperativeDailyProcess;
import static com.pradera.model.operativedaily.type.OperativeDailyState.*;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;

@RequestScoped
@BatchProcess(name="OperativeDailyProcessBatch")
public class OperativeDailyProcessBatch implements JobExecution{
	
	@EJB OperativeDailyFacade operativeDailyFacade;
	
	@EJB OperativeDailyProcessExecution processExecution;
	
	@Override
	public void startJob(ProcessLogger processLogger) {
		/**Process Date By Default it's the current date*/
		Date processDate = CommonsUtilities.truncateDateTime(CommonsUtilities.currentDate());
		/**Begin the process to get parameters*/
		List<ProcessLoggerDetail> lstProcessLoggerDetails = processLogger.getProcessLoggerDetails();
		/**Iterate all parameters*/
		for (ProcessLoggerDetail objDetail: lstProcessLoggerDetails){
			if(objDetail.getParameterName().equals(OperativeDailyConstants.PROCESS_DATE_PARAMETER)){
				/**Getting date && Truncate hours in date*/
				processDate = CommonsUtilities.convertStringtoDate(objDetail.getParameterValue(),CommonsUtilities.DATETIME_PATTERN);
				processDate = CommonsUtilities.truncateDateTime(processDate);
			}
		}
		
		// TODO Auto-generated method stub
		try {
			beginOperativeDaily(processDate);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Begin operative daily.
	 *
	 * @param processDate the process date
	 * @throws ServiceException the service exception
	 */
	public void beginOperativeDaily(Date processDate) throws ServiceException{
		OperativeDailyProcess operativeDailyProcess = operativeDailyFacade.getOperativeDailyProcess(processDate);
		if(operativeDailyProcess == null){
			operativeDailyProcess = operativeDailyFacade.createNewOperativeProcess(processDate);
		}
		
		Integer processState = operativeDailyProcess.getProcessState();
		if(processState.equals(WAITING.getCode()) || processState.equals(REGISTER.getCode())){
			processExecution.executeMainOperativeDailyProcess(operativeDailyProcess);
		}
	}

	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}
}