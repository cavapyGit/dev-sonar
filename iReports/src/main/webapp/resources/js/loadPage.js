/*
 * Demo about report 3
 * */
function loadParticipantDemo(){
	alert('Este es el reporte 3 y desactivo el estado');
	state_rnt.disable();
}
//demo report 3 onchange
function changeMenu(widgetName){
	alert('nombre jsf '+widgetName.id+' valor :'+ widgetName.getSelectedValue());
}
function replaceInvalidCharacter(clientId,expresion){
	var idEscape = PrimeFaces.escapeClientId(clientId);
	$(idEscape).val($(idEscape).val().replace(expresion,''));
}
function getClientIds(){
	stringIds ='';
	for (var i = 0; i < arguments.length; i++) {
		//trim
	    stringIds = stringIds+ arguments[i].id + ' ';
	}
	return $.trim(stringIds);
}

/*La primera carga a la pantalla (120)*/
/*Ocultar todos los elementos dinamicos, para metodo limpiar*/
function hideElementDynamicConsolidateReportBilling(filter_billing,billing_service,entity_collection){
	var filterBillingEscape = PrimeFaces.escapeClientId(filter_billing.id);
	var helperBillingEscape = PrimeFaces.escapeClientId(billing_service.id);
	var entityCollectionEscape = PrimeFaces.escapeClientId(entity_collection.id);
	
	/*Hide combo Billing*/
	$(filterBillingEscape).parent().prev().css("display", "none");
	$(filterBillingEscape).css("display", "none");
	
	/*Hide helper BillingService*/
	$(helperBillingEscape).parent().parent().parent().parent().parent().prev().css("display", "none");
	$(helperBillingEscape).parent().parent().parent().parent().parent().css("display", "none");
	/*Hide combo entity*/
	$(entityCollectionEscape).parent().prev().css("display", "none");
	$(entityCollectionEscape).css("display", "none");
	
}

/*Funcion del combo de numero de billing*/
function showHelperBilling(filter_billing,billing_service){
	var filter=filter_billing.value;
	var helperBilling = PrimeFaces.escapeClientId(billing_service.id);
	if(filter==1){
		$(helperBilling).parent().parent().parent().parent().parent().prev().removeAttr('style');
		$(helperBilling).parent().parent().parent().parent().parent().removeAttr('style');
	}
	else{
		$(helperBilling).parent().parent().parent().parent().parent().prev().css("display", "none");
		$(helperBilling).parent().parent().parent().parent().parent().css("display", "none");
	}
}


/*Dinamica dependiendo del primer combo*/
function dynamicComboReport(filter_search,filter_billing,billing_service,entity_collection){//}(widgetName1,widgetName2,widgetName3){
	
	var filterSearch = filter_search.value;
	var filterBillingEscape = PrimeFaces.escapeClientId(filter_billing.id);
	var helperBillingEscape = PrimeFaces.escapeClientId(billing_service.id);
	var entityCollectionEscape = PrimeFaces.escapeClientId(entity_collection.id);
	/*Caso en que valor seleccionado es servicio*/
	if(filterSearch==1){
		
		/*Show combo Billing*/
		$(filterBillingEscape).parent().prev().removeAttr('style');
		$(filterBillingEscape).removeAttr('style');
		
		/*Hide helper BillingService*/
		$(helperBillingEscape).parent().parent().parent().parent().parent().prev().css("display", "none");
		$(helperBillingEscape).parent().parent().parent().parent().parent().css("display", "none");
		
		/*Hide combo entity*/
		$(entityCollectionEscape).parent().prev().css("display", "none");
		$(entityCollectionEscape).css("display", "none");
	}
	/*En caso que el valor seleccionado sea entidad*/
	else if(filterSearch==2){
		/*Hide combo Billing*/
		$(filterBillingEscape).parent().prev().css("display", "none");
		$(filterBillingEscape).css("display", "none");
		
		/*Hide helper BillingService*/
		$(helperBillingEscape).parent().parent().parent().parent().parent().prev().css("display", "none");
		$(helperBillingEscape).parent().parent().parent().parent().parent().css("display", "none");
		
		/*Show combo entity*/
		$(entityCollectionEscape).parent().prev().removeAttr('style');
		$(entityCollectionEscape).removeAttr('style');
	}
	else{
		hideElementDynamicConsolidateReportBilling(filter_billing,billing_service,entity_collection);
	}
	assignFilter(filterSearch);
}



/*mostrar todos los elementos dinamicos(no tiene uso)*/
function showElementDynamicConsolidateReportBilling(filter_billing,billing_service,entity_collection){
	var filterBillingEscape = PrimeFaces.escapeClientId(filter_billing.id);
	var helperBillingEscape = PrimeFaces.escapeClientId(billing_service.id);
	var entityCollectionEscape = PrimeFaces.escapeClientId(entity_collection.id);
	
	/*Show combo Billing*/
	$(filterBillingEscape).parent().prev().removeAttr('style');
	$(filterBillingEscape).removeAttr('style');
	
	/*Show helper BillingService*/
	$(helperBillingEscape).parent().parent().parent().parent().parent().prev().removeAttr('style');
	$(helperBillingEscape).parent().parent().parent().parent().parent().removeAttr('style');
	/*Show combo entity*/
	$(entityCollectionEscape).parent().prev().removeAttr('style');
	$(entityCollectionEscape).removeAttr('style');
}



/*La primera carga a la pantalla reporte Consolidado de cobros(133)*/
function hideElementDynamicConsolidateCollectionRecord(issuer,participant,holder,other_number,afp){
	var issuerEscape = PrimeFaces.escapeClientId(issuer.id);
	var participantEscape = PrimeFaces.escapeClientId(participant.id);
	var afpEscape = PrimeFaces.escapeClientId(afp.id);
	var holderEscape = PrimeFaces.escapeClientId(holder.id);
	var otherNumberEscape=PrimeFaces.escapeClientId(other_number.id);
	
	/*Hide helper Issuer*/
	$(issuerEscape).parent().parent().parent().parent().parent().prev().css("display", "none");
	$(issuerEscape).parent().parent().parent().parent().parent().css("display", "none");
	
	/*Hide combo Participant*/
	$(participantEscape).parent().prev().css("display", "none");
	$(participantEscape).css("display", "none");

	/*Hide combo AFP*/
	$(afpEscape).parent().prev().css("display", "none");
	$(afpEscape).css("display", "none");
	
	/*Hide helper Holder*/
	$(holderEscape).parent().parent().parent().parent().parent().prev().css("display", "none");
	$(holderEscape).parent().parent().parent().parent().parent().css("display", "none");
	
	/*Hide input Text Other Entity Collection*/
	$(otherNumberEscape).parent().parent().css("display", "none");
}


function dynamicComboConsolidateCollectionReport(entity_collection,issuer,participant,holder,other_number,afp){
	var entityCollection = entity_collection.value;
	var issuerEscape = PrimeFaces.escapeClientId(issuer.id);
	var participantEscape = PrimeFaces.escapeClientId(participant.id);
	var afpEscape=PrimeFaces.escapeClientId(afp.id);
	var holderEscape = PrimeFaces.escapeClientId(holder.id);
	var otherNumberEscape=PrimeFaces.escapeClientId(other_number.id);
	/*Caso en que valor seleccionado emisor*/
	if(entityCollection==1406){
		
		/*Hide helper Issuer*/
		$(issuerEscape).parent().parent().parent().parent().parent().prev().removeAttr('style');
		$(issuerEscape).parent().parent().parent().parent().parent().removeAttr('style');
		
		/*Hide combo Participant*/
		$(participantEscape).parent().prev().css("display", "none");
		$(participantEscape).css("display", "none");
		
		/*Hide combo AFP*/
		$(afpEscape).parent().prev().css("display", "none");
		$(afpEscape).css("display", "none");
		
		/*Hide helper Holder*/
		$(holderEscape).parent().parent().parent().parent().parent().prev().css("display", "none");
		$(holderEscape).parent().parent().parent().parent().parent().css("display", "none");
		
		/*Hide all Row input Text Other Entity Collection*/
		$(otherNumberEscape).parent().parent().css("display", "none");
		
	}
	/*En caso que el valor seleccionado participante*/
	else if(entityCollection==1407){
		/*Hide helper Issuer*/
		$(issuerEscape).parent().parent().parent().parent().parent().prev().css("display", "none");
		$(issuerEscape).parent().parent().parent().parent().parent().css("display", "none");
		
		/*Show combo Participant*/
		$(participantEscape).parent().prev().removeAttr('style');
		$(participantEscape).removeAttr('style');

		/*Hide combo AFP*/
		$(afpEscape).parent().prev().css("display", "none");
		$(afpEscape).css("display", "none");
		
		/*Hide helper Holder*/
		$(holderEscape).parent().parent().parent().parent().parent().prev().css("display", "none");
		$(holderEscape).parent().parent().parent().parent().parent().css("display", "none");
		
		/*Hide all Row input Text Other Entity Collection*/
		$(otherNumberEscape).parent().parent().css("display", "none");
		
	}
	/*En caso que el valor seleccionado AFP*/
	else if(entityCollection==1726){
		/*Hide helper Issuer*/
		$(issuerEscape).parent().parent().parent().parent().parent().prev().css("display", "none");
		$(issuerEscape).parent().parent().parent().parent().parent().css("display", "none");
		
		/*Hide combo Participant*/
		$(participantEscape).parent().prev().css("display", "none");
		$(participantEscape).css("display", "none");
		
		/*Show combo AFP*/
		$(afpEscape).parent().prev().removeAttr('style');
		$(afpEscape).removeAttr('style');
		
		/*Hide helper Holder*/
		$(holderEscape).parent().parent().parent().parent().parent().prev().css("display", "none");
		$(holderEscape).parent().parent().parent().parent().parent().css("display", "none");
		
		/*Hide all Row input Text Other Entity Collection*/
		$(otherNumberEscape).parent().parent().css("display", "none");
		
	}
	/*En caso que el valor seleccionado titular*/
	else if(entityCollection==1408){
		/*Hide helper Issuer*/
		$(issuerEscape).parent().parent().parent().parent().parent().prev().css("display", "none");
		$(issuerEscape).parent().parent().parent().parent().parent().css("display", "none");
		
		/*Hide combo Participant*/
		$(participantEscape).parent().prev().css("display", "none");
		$(participantEscape).css("display", "none");

		/*Hide combo AFP*/
		$(afpEscape).parent().prev().css("display", "none");
		$(afpEscape).css("display", "none");
		
		/*Hide helper Holder*/
		$(holderEscape).parent().parent().parent().parent().parent().prev().removeAttr('style');
		$(holderEscape).parent().parent().parent().parent().parent().removeAttr('style');
		
		/*Hide all Row input Text Other Entity Collection*/
		$(otherNumberEscape).parent().parent().css("display", "none");
		
	}/*En caso que el valor seleccionado otros*/
	else if(entityCollection==1530){
		/*Hide helper Issuer*/
		$(issuerEscape).parent().parent().parent().parent().parent().prev().css("display", "none");
		$(issuerEscape).parent().parent().parent().parent().parent().css("display", "none");
		
		/*Hide combo Participant*/
		$(participantEscape).parent().prev().css("display", "none");
		$(participantEscape).css("display", "none");

		/*Hide combo AFP*/
		$(afpEscape).parent().prev().css("display", "none");
		$(afpEscape).css("display", "none");
		
		/*Hide helper Holder*/
		$(holderEscape).parent().parent().parent().parent().parent().prev().css("display", "none");
		$(holderEscape).parent().parent().parent().parent().parent().css("display", "none");
		
		/*Show all Row input Text Other Entity Collection*/
		$(otherNumberEscape).parent().parent().removeAttr('style');
	}
	else{
		hideElementDynamicConsolidateCollectionRecord(issuer,participant,holder,other_number,afp);
	}
}

function changeSelectedSystem() {
	var selectedSystemId = system.getSelectedValue();
	
	loadTableListBySystem(selectedSystemId,table.id,column.id);
}

function changeSelectedTable() {
	var selectedSystemId = system.getSelectedValue();
	var selectedTableName = table.getSelectedValue();
	
	loadColumnListByTable(selectedSystemId,selectedTableName,column.id);
}

	
	
	


