package org.pentaho.reporting.engine.classic.core.function;

import java.math.BigDecimal;

import org.pentaho.reporting.engine.classic.core.event.ReportEvent;
import org.pentaho.reporting.engine.classic.core.function.AbstractFunction;
import org.pentaho.reporting.engine.classic.core.function.Expression;
import org.pentaho.reporting.engine.classic.core.function.FieldAggregationFunction;
import org.pentaho.reporting.engine.classic.core.function.FunctionUtilities;


public class SingleSumFunction extends AbstractFunction  implements FieldAggregationFunction{
	
	private static final long serialVersionUID = 0x2281879eed361627L;
	protected static final BigDecimal ZERO = new BigDecimal(0.0D);
	private transient BigDecimal sum;
	private String group;
	private String field;
	 
	/*Display Function  Name*/
	public String getCanonicalName() {
		return "Group Sum(Single)";
	}
	
	/* Constructor */
	public SingleSumFunction() {
		sum = ZERO;
	}

	/* Argumented Constructor */
	public SingleSumFunction(String name) {
		this();
		setName(name);
	}

	/* Function is called when report is initialized */
	public void reportInitialized(ReportEvent event) {
		sum = ZERO;
	}

	/* Function is called when when group starts. When Group changes, add the value of field. */
	public void groupStarted(ReportEvent event) {
		Object fieldValue = getDataRow().get(getField());
		if (fieldValue == null)
			return;
		if (!(fieldValue instanceof Number))
			return;
		Number n = (Number) fieldValue;
		if (FunctionUtilities.isDefinedGroup(getGroup(), event))
		{
			if (n instanceof BigDecimal)
				sum = sum.add((BigDecimal) n);
			else
				sum = sum.add(new BigDecimal(n.toString()));
		}
	}

	/* Get Value of group */
	public String getGroup() {
		return group;
	}

	/* Set Value of group */
	public void setGroup(String name) {
		group = name;
	}

	/* Get Value of field */
	public String getField() {
		return field;
	}

	/* Set Value of field */
	public void setField(String field) {
		this.field = field;
	}

	/* Get Value of sum */
	public Object getValue() {
		return sum;
	}
	/* Get Value of sum */
	protected BigDecimal getSum() {
		return sum;
	}
    
	/* Set Value of sum */
	protected void setSum(BigDecimal sum) {
		if (sum == null) {
			throw new NullPointerException("Sum must not be null");
		} else {
			this.sum = sum;
			return;
		}
	}

	/* Gets the instance of class */
	public Expression getInstance() {
		SingleSumFunction function = (SingleSumFunction) super.getInstance();
		function.sum = ZERO;
		return function;
	}

	@Override
	public String getCrosstabFilterGroup() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setCrosstabFilterGroup(String groupName) {
		// TODO Auto-generated method stub
		
	}

	
	
}
