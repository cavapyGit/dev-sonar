package org.pentaho.reporting.engine.classic.core.function;

import java.math.BigDecimal;
import java.util.HashMap;

import org.pentaho.reporting.engine.classic.core.event.ReportEvent;
import org.pentaho.reporting.engine.classic.core.function.AbstractFunction;
import org.pentaho.reporting.engine.classic.core.function.Expression;
import org.pentaho.reporting.engine.classic.core.function.FieldAggregationFunction;
import org.pentaho.reporting.engine.classic.core.function.FunctionUtilities;



public class TwoGroupSumFunction extends AbstractFunction implements FieldAggregationFunction
{

	private static final long serialVersionUID = 0x2281879eed361627L;
	protected static final BigDecimal ZERO_BIGDECIMAL = new BigDecimal(0.0D);
	protected static final Integer ZERO_INTEGER = new Integer(0);
	private transient BigDecimal sum;
	private transient int count;
	private transient HashMap<Integer, BigDecimal> previousValue = new HashMap<Integer, BigDecimal>();
	private String group;
	private String field;
	private String conditionField;


	/**
	 * @return the conditionField
	 */
	public String getConditionField() {
		return conditionField;
	}

	/**
	 * @param conditionField the conditionField to set
	 */
	public void setConditionField(String conditionField) {
		this.conditionField = conditionField;
	}

	/*Display Function  Name*/
	public String getCanonicalName() {
		return "Group Sum(Dual)";
	}

	/* Constructor */
	public TwoGroupSumFunction() {
		sum = ZERO_BIGDECIMAL;
		previousValue.clear();
	}

	/* Argumented Constructor */
	public TwoGroupSumFunction(String name) {
		this();
		setName(name);
	}
    
	/* Function is called when report is initialized */
	public void reportInitialized(ReportEvent event) {
		sum = ZERO_BIGDECIMAL;
	}
	
	/* Function is called when when group starts */
	public void groupStarted(ReportEvent event) {
		if (FunctionUtilities.isDefinedGroup(getGroup(), event)) {
			sum = ZERO_BIGDECIMAL;
			count = ZERO_INTEGER;
			previousValue.clear();
		}
	}

	/* Get Value of group */
	public String getGroup() {
		return group;
	}

	/* Set Value of group */
	public void setGroup(String name) {
		group = name;
	}

	/* Get Value of field */
	public String getField() {
		return field;
	}

	/* Set Value of field */
	public void setField(String field) {
		this.field = field;
	}
   
	/* This method is called for every row. Add Value of records */
	public void itemsAdvanced(ReportEvent event) {
		Object fieldValue = getDataRow().get(getField());
		Object conditionalValue = getDataRow().get(getConditionField());
		if (conditionalValue == null)
			return;
		if (!(conditionalValue instanceof Number))
			return;
		if (fieldValue == null)
			return;
		if (!(fieldValue instanceof Number))
			return;
		Number n = (Number) fieldValue;
		Number nCondition = (Number) conditionalValue;
		//Putting conditionalValue in hash map
		previousValue.put(count,((BigDecimal) nCondition).setScale(2, BigDecimal.ROUND_UP));
		// Adding the first fieldValue to sum
		if (count == ZERO_INTEGER) {
			if (n instanceof BigDecimal)
				sum = sum.add((BigDecimal) n);
			else
				sum = sum.add(new BigDecimal(n.toString()));
		} else {
			// if current conditionalValue is different from previous conditionalValue, it will add fieldValue to sum
			if (!(previousValue.get(count - 1)).equals(previousValue.get(count))) {
				if (n instanceof BigDecimal)
					sum = sum.add((BigDecimal) n);
				else
					sum = sum.add(new BigDecimal(n.toString()));
			}
		}
		count++;
	}

	/* Get Value of sum */
	public Object getValue() {
		return sum;
	}

	/* Get Value of sum */
	protected BigDecimal getSum() {
		return sum;
	}

	/* Set Value of sum */
	protected void setSum(BigDecimal sum) {
		if (sum == null) {
			throw new NullPointerException("Sum must not be null");
		} else {
			this.sum = sum;
			return;
		}
	}

	/* Gets the instance of class */
	public Expression getInstance() {
		TwoGroupSumFunction function = (TwoGroupSumFunction) super.getInstance();
		function.sum = ZERO_BIGDECIMAL;
		return function;
	}

	@Override
	public String getCrosstabFilterGroup() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setCrosstabFilterGroup(String groupName) {
		// TODO Auto-generated method stub
		
	}

}
