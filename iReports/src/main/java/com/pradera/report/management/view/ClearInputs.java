package com.pradera.report.management.view;

import javax.el.ELContext;
import javax.el.ValueExpression;
import javax.faces.component.EditableValueHolder;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.visit.VisitResult;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.extensions.util.visitcallback.VisitTaskExecutor;

import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.utils.CommonsUtilities;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ClearInputs.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/09/2013
 */
public class ClearInputs implements VisitTaskExecutor  {
	
	/** The el context. */
	private ELContext elContext;  
    
    /** The request context. */
    private RequestContext requestContext; 
    
    /** The components. */
    private int components;
    
    private UserAccountSession userAccountSession;

	/**
	 * Instantiates a new clear inputs.
	 *
	 * @param elContext the el context
	 * @param requestContext the request context
	 * @param components the components
	 */
	public ClearInputs(ELContext elContext, RequestContext requestContext,int components) {
		this.elContext = elContext;  
	    this.requestContext = requestContext;
	    this.components=components;
	}
	
	public ClearInputs(ELContext elContext, RequestContext requestContext,int components,UserAccountSession userAccountSession) {
		this.elContext = elContext;  
	    this.requestContext = requestContext;
	    this.components=components;
	    this.userAccountSession=userAccountSession;
	}

	/* (non-Javadoc)
	 * @see org.primefaces.extensions.util.visitcallback.VisitTaskExecutor#execute(javax.faces.component.UIComponent)
	 */
	@Override
	public VisitResult execute(UIComponent component) {
		UIInput input = (UIInput) component;  
        String idFilter = input.getId();  
        // reset UI  
        input.resetValue();
        ValueExpression ve = null;
     // reset value in bean
        switch (idFilter) {
		case "text":
			ve = input.getValueExpression("value");  
            if (ve != null) {  
                ve.setValue(elContext, StringUtils.EMPTY);  
            }
         // update the corresponding input during response  
            requestContext.update(input.getClientId());
            //delete handled id from the list, so that similar inputs should not be executed again
           --components;
			break;
		case "calendar":
		case "calendarFuture":
		case "calendarAll":
			ve = input.getValueExpression("value");  
            if (ve != null) {  
                ve.setValue(elContext, CommonsUtilities.currentDate());  
            }
         // update the corresponding input during response  
            requestContext.update(input.getClientId());
            //delete handled id from the list, so that similar inputs should not be executed again
           --components;
			break;
		case "parameters":
		case "combo":
		case "multiple":
			ve = input.getValueExpression("value");  
            if (ve != null) {  
                ve.setValue(elContext, null);  
            }
         // update the corresponding input during response  
            requestContext.update(input.getClientId());
            //delete handled id from the list, so that similar inputs should not be executed again
           --components;
			break;
			
		case "securities":
		case "holders":
		case "accounts":
		case "billingService":
			ve = input.getValueExpression("value");  
            if (ve != null) {  
                ve.setValue(elContext, null);  
            }
         // update the corresponding input during response  
            requestContext.update(input.getClientId());
            //delete handled id from the list, so that similar inputs should not be executed again
           --components;
			break;
		//TODO review clear when user security is necessary
		case "issuers":
			ve = input.getValueExpression("value");  
            if (ve != null && !userAccountSession.isIssuerInstitucion() && !userAccountSession.isIssuerDpfInstitucion()) {  
                ve.setValue(elContext, null);  
            }
         // update the corresponding input during response  
            requestContext.update(input.getClientId());
            //delete handled id from the list, so that similar inputs should not be executed again
           --components;
			break;
		//TODO review clear when user security is necesary
		case "participants":
			ve = input.getValueExpression("value");  
            if (ve != null && !userAccountSession.isParticipantInstitucion() && !userAccountSession.isParticipantInvestorInstitucion()) {  
                ve.setValue(elContext, null);  
            }
         // update the corresponding input during response  
            requestContext.update(input.getClientId());
            //delete handled id from the list, so that similar inputs should not be executed again
           --components;
			break;
			
		//descriptions helpers dont minus components only update
		case "holdersDescription":
		case "billingServiceDescription":
		case "securitiesDescription":
			ve = input.getValueExpression("value");  
            if (ve != null) {  
                ve.setValue(elContext, null);  
            }
            requestContext.update(input.getClientId());
            break;
		case "dtAccounts":
			ve = input.getValueExpression("value");
	        if (ve != null) {
	            ve.setValue(elContext, null);
	        }
	        requestContext.update(input.getClientId());
	        break;
	        
		case "issuersDescription":
			ve = input.getValueExpression("value");
            if (ve != null && !userAccountSession.isIssuerInstitucion() && !userAccountSession.isIssuerDpfInstitucion()) {
                ve.setValue(elContext, null);
            }
            requestContext.update(input.getClientId());
			break;
		}

		return (components > 0) ? VisitResult.REJECT : VisitResult.COMPLETE;
	}

	/* (non-Javadoc)
	 * @see org.primefaces.extensions.util.visitcallback.VisitTaskExecutor#shouldExecute(javax.faces.component.UIComponent)
	 */
	@Override
	public boolean shouldExecute(UIComponent component) {
		//TODO maybe in future select another condition
		if(component instanceof EditableValueHolder) {
			return true;
		}
		return false;
	}

}
