package com.pradera.report.management.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.report.ReportFilter;
import com.pradera.model.report.type.ReportFilterType;
import com.pradera.report.generation.executor.ReportConstant;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class DynamicFieldsServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 16/08/2013
 */
@Stateless
public class DynamicFieldsServiceBean extends CrudDaoServiceBean {

	/** The participant service. */
	@EJB
	ParticipantServiceBean participantService;
	
	/** The parameter service. */
	@EJB
	ParameterServiceBean parameterService;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/**
	 * Fill select menu filter.
	 *
	 * @param reportFilter the report filter
	 */
	public void fillSelectMenuFilter(ReportFilter reportFilter) throws ServiceException {
		if(reportFilter.getFilterType().equals(ReportFilterType.COMBO_PARAMETERS.getCode())) {
			if(reportFilter.getMasterTable()!=null){
				ParameterTableTO parameter = new ParameterTableTO(); 
				parameter.setMasterTableFk(reportFilter.getMasterTable());
				
				//if usser is Issuer DPF Institution - search by security class DPF and DPA
				if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()
						&& parameter.getMasterTableFk().equals(MasterTableType.SECURITIES_CLASS.getCode())){
					List<Integer> listSecutrityClass = new ArrayList<>();
					listSecutrityClass.add(SecurityClassType.DPA.getCode());
					listSecutrityClass.add(SecurityClassType.DPF.getCode());
					parameter.setLstParameterTablePk(listSecutrityClass);
				}
				
				parameter.setState(BooleanType.YES.getCode());
				parameter.setOrderbyParameterTableCd(BooleanType.NO.getCode());
				parameter.setOrderbyParameterName(BooleanType.YES.getCode());
				reportFilter.setSelectMenu(new LinkedHashMap<String,Object>());
				for(ParameterTable param : parameterService.getListParameterTableServiceBean(parameter)){
					if(parameter.getMasterTableFk().equals(MasterTableType.SECURITIES_CLASS.getCode())){
						reportFilter.getSelectMenu().put(
								param.getText1()+" - "+param.getParameterName(),
								param.getParameterTablePk());
					}else{
						reportFilter.getSelectMenu().put(
								param.getParameterName(),
								param.getParameterTablePk());
					}
				}
			}
		} else if(reportFilter.getFilterType().equals(ReportFilterType.HELP_PARTICIPANTS.getCode())) 
		{
			if(StringUtils.isNotBlank(reportFilter.getSqlListQuery())) {
				boolean indRequired = reportFilter.getIndRequired().equals(BooleanType.YES.getCode());
				Map<String,Object> params = new HashMap<String,Object>();
				reportFilter.setSelectMenu(new LinkedHashMap<String,Object>());
				List<Object[]> filters= (List<Object[]>)parameterService.findByNativeQuery(reportFilter.getSqlListQuery(), params);
				reportFilter.setSelectMenu(new LinkedHashMap<String,Object>());
				reportFilter.getSelectMenu().put(PropertiesUtilities.getMessage(indRequired?"cbo.opt.select":"cbo.opt.all"),null);
				for(Object[] row : filters){
					//According standar index 0 is value and index 1 is label
					reportFilter.getSelectMenu().put(row[1].toString(), row[0]);
				}
			}
		}
		else if(reportFilter.getFilterName().equals(ReportConstant.SECURITY_CLASS_PARAM)){
			
				ParameterTableTO parameter = new ParameterTableTO();
							
				//if usser is Issuer DPF Institution - search by security class DPF and DPA
				if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
					List<Integer> listSecutrityClass = new ArrayList<>();
					listSecutrityClass.add(SecurityClassType.DPA.getCode());
					listSecutrityClass.add(SecurityClassType.DPF.getCode());
					parameter.setLstParameterTablePk(listSecutrityClass);
					
					parameter.setState(BooleanType.YES.getCode());
					parameter.setOrderbyParameterTableCd(BooleanType.NO.getCode());
					parameter.setOrderbyParameterName(BooleanType.YES.getCode());
					reportFilter.setSelectMenu(new LinkedHashMap<String,Object>());
					for(ParameterTable param : parameterService.getListParameterTableServiceBean(parameter)){
						reportFilter.getSelectMenu().put(param.getText1()+" - "+param.getParameterName(), param.getParameterTablePk());
					}
					
				}else			
				//query
				if(StringUtils.isNotBlank(reportFilter.getSqlListQuery())) {
					Map<String,Object> params = new HashMap<String,Object>();
					reportFilter.setSelectMenu(new LinkedHashMap<String,Object>());
					List<Object[]> filters= (List<Object[]>)parameterService.findByNativeQuery(reportFilter.getSqlListQuery(), params);
					reportFilter.setSelectMenu(new LinkedHashMap<String,Object>());
					for(Object[] row : filters){
						//According standar index 0 is value and index 1 is label
						reportFilter.getSelectMenu().put(row[1].toString(), row[0]);
					}
			}
		}else
			//query
			if(StringUtils.isNotBlank(reportFilter.getSqlListQuery())) {
				Map<String,Object> params = new HashMap<String,Object>();
				reportFilter.setSelectMenu(new LinkedHashMap<String,Object>());
				List<Object[]> filters= (List<Object[]>)parameterService.findByNativeQuery(reportFilter.getSqlListQuery(), params);
				reportFilter.setSelectMenu(new LinkedHashMap<String,Object>());
				for(Object[] row : filters){
					//According standar index 0 is value and index 1 is label
					reportFilter.getSelectMenu().put(row[1].toString(), row[0]);
				}
			}
	}
	
	/**
	 * Fill select menu participants.
	 *
	 * @param reportFilter the report filter
	 * @param participantPk the participant pk
	 * @throws ServiceException the service exception
	 */
	public void fillSelectMenuParticipants(ReportFilter reportFilter,Long participantPk) throws ServiceException{
		reportFilter.setSelectMenu(new LinkedHashMap<String,Object>());
		Participant partFilter = new Participant();
		if(participantPk != null) {
			partFilter.setIdParticipantPk(participantPk);
		}
		else{
			reportFilter.getSelectMenu().put(PropertiesUtilities.getMessage("cbo.opt.all"),null);
		}
		for(Participant participant: participantService.getLisParticipantServiceBean(partFilter)) {
			reportFilter.getSelectMenu().put(participant.getMnemonic()+" - "+participant.getIdParticipantPk()+" - "+
											participant.getDescription(),participant.getIdParticipantPk());
		}
	}
	

    /**
     * Default constructor. 
     */
    public DynamicFieldsServiceBean() {
        // TODO Auto-generated constructor stub
    }

}
