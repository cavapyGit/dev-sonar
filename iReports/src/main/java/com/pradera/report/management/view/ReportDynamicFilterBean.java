package com.pradera.report.management.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.visit.VisitContext;
import javax.faces.component.visit.VisitHint;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.PrimeFaces;
import org.primefaces.context.RequestContext;
import org.primefaces.extensions.component.dynaform.DynaForm;
import org.primefaces.extensions.model.dynaform.DynaFormControl;
import org.primefaces.extensions.model.dynaform.DynaFormModel;
import org.primefaces.extensions.util.visitcallback.ExecutableVisitCallback;
import org.primefaces.extensions.util.visitcallback.VisitTaskExecutor;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.RequestType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.AccountHelperInputTO;
import com.pradera.core.component.helperui.to.BillingServiceTo;
import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.core.component.helperui.to.HolderHelperOutputTO;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.core.component.helperui.to.IssuerSearcherTO;
import com.pradera.core.component.helperui.to.IssuerTO;
import com.pradera.core.component.helperui.to.SecuritiesSearcherTO;
import com.pradera.core.component.helperui.to.SecuritiesTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.corporatives.type.CorporativeEventReportType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.report.Report;
import com.pradera.model.report.ReportFilter;
import com.pradera.model.report.type.ReportFilterType;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.facade.ReportGenerationLocalServiceFacade;
import com.pradera.report.management.facade.ReportMgmtServiceFacade;
import com.pradera.report.util.view.PropertiesConstants;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ReportDynamicFilterBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/08/2013
 */
@DepositaryWebBean
@LoggerCreateBean
public class ReportDynamicFilterBean extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1021565488441091179L;
	
	/** The report mgmt facade. */
	@EJB
	ReportMgmtServiceFacade reportMgmtFacade;
	
	/** The report generation facade. */
	@EJB
	ReportGenerationLocalServiceFacade reportGenerationFacade;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	@EJB
	GeneralParametersFacade parametersServiceFacade;

	@EJB
	private ParticipantServiceBean participantServiceBean;
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The log. */
	@Inject
	private transient PraderaLogger log;
	
	/** The model. */
	private DynaFormModel form;
	
	/** The report id. */
	private Long reportId;
	
	/** The report. */
	private Report report;
	
	/** The parameters. */
	private Map<String,Object> parameters;
	
	/** The max date initial. */
	private Date maxDateInitial;
	
	/** The min date end. */
	private Date minDateEnd;
	
	/** The max date end. */
	private Date maxDateEnd;
	
	/** The name validation method. */
	private String nameValidationMethod;
	
	/** The participant. */
	private Long participant;
	private Long idHolder;
	
	/** The issuer. */
	private String issuer;
	
	/** The issuer descripton. */
	private String issuerDesc;
	
	/** The issuer searcher. */
	private IssuerSearcherTO issuerSearcher = new IssuerSearcherTO();
	
	/** The filter selected. */
	private ReportFilter filterSelected;
	
	
	private ReportUser reportUser;
	
	private String	referenceRate = "";
	
	private String  serviceCode;
	
	/** The Constant CONFIRM_DIALOG_SUCCESS. */
	private static final String CONFIRM_DIALOG_SUCCESS=":frmReportForm:reportGenerationOk";
	
	/** The Constant VISIT_HINTS. */
	public static final Set<VisitHint> VISIT_HINTS = EnumSet.of(VisitHint.SKIP_UNRENDERED);  
	
	/** The Constant PARTICIPANT. */
	public static final String PARTICIPANT ="participants";
	
	/** The Constant HOLDER. */
	public static final String HOLDER ="holders";
	
	/** The Constant Security. */
	public static final String SECURITY = "securities";
	
	public static final String ISSUER = "issuers";
	
	/** The Constant ACCOUNT. */
	public static final String ACCOUNT ="accounts";
	
	/** The Constant UPDATE_COMPONENTS. */
	public static final String UPDATE_COMPONENTS ="update_components";
	
	/** The Constant HOLDERS_HELPER. */
	public static final String HOLDERS_HELPER = "holdersHelper";
	
	/** The Constant SECURITIES_HELPER. */
	public static final String SECURITIES_HELPER ="securitiesHelper";
	
	/** The Constant ACCOUNTS_HELPER. */
	public static final String ACCOUNTS_HELPER ="accountsHelper";
	
	/** The Constant BILLING_HELPER. */
	public static final String BILLING_HELPER ="billingHelper";
	
	/** The Constant COMBO. */
	public static final String COMBO ="combo";
	
	/** The Constant COMBO_PARAMETERS. */
	public static final String COMBO_PARAMETERS ="parameters";
	/** The Constant COMBO_PARAMETERS. */
	public static final String COMBO_SERVICE ="comboService";
	
	public static final String SYSTEM = "system";
	
	public static final String TABLE = "table";
	
	public static final String COLUMN = "column";
	
	/*** EL VALOR SELECCIONADO***/
	private Integer filter_search;
	private Integer closedStateType = HolderAccountStatusType.CLOSED.getCode();
	
	//idParticipantBC
	@Inject 
	@Configurable 
	String idParticipantBC;
	
	private List<HolderAccountHelperResultTO> holderAccounts;
	private HolderAccount holderAccountTO;
	private int lastPos=-1;
	private boolean showElement;
	
	public Integer getFilter_search() {
		return filter_search;
	}

	public void setFilter_search(Integer filter_search) {
		this.filter_search = filter_search;
	}
	
	@PostConstruct
	public void init() throws ServiceException{
		reportUser= new ReportUser();
	}

	/**
	 * Submit form.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String submitForm() {
		//If try fire report
		Map<String,String> parameters = new HashMap<String, String>();
		Map<String,String> parameterDetails = new HashMap<String, String>();
		boolean isEventType = false;
		boolean isReportType = false;
		String eventType = "";
		String reportType = "";
		Boolean accountListSelected = Boolean.FALSE;
		Boolean accountFilterExists = Boolean.FALSE;
		Integer accountNumberEntered = null;
		//from screen report is only user copy inbox report
		reportUser.setUserName(userInfo.getUserAccountSession().getUserName());
		//how report is send for screen so for default send notification
		reportUser.setShowNotification(BooleanType.YES.getCode());
		try {
			for (DynaFormControl dynaFormControl : form.getControls()) {
				if (dynaFormControl.getData() instanceof ReportFilter) {
					ReportFilter reportFilter = (ReportFilter)dynaFormControl.getData();
					String value = null;
					ReportFilterType filterType = ReportFilterType.get(reportFilter.getFilterType());
					if(participant!=null && reportFilter.getFilterType()==1384) {
						reportFilter.setValue(participant);
					}
					
//					if(!reportMgmtFacade.isReportForBCB(reportId)){
//						if((userInfo.getUserAccountSession().isParticipantInstitucion() || userInfo.getUserAccountSession().isParticipantInvestorInstitucion())
//								&& filterType.getCode().equals(ReportFilterType.HELP_PARTICIPANTS.getCode()) && participant != null ){
//							reportFilter.setValue(this.participant.toString());
//						} else if((userInfo.getUserAccountSession().isIssuerInstitucion())
//								&& filterType.getCode().equals(ReportFilterType.HELP_ISSUERS.getCode())){
//							IssuerTO issuer =(IssuerTO)reportFilter.getValue();
//							issuer.setIssuerCode(this.issuer);
//							
//							//is report in participant related issuer group and the code participant is not null 
//							if(userInfo.getUserAccountSession().getPartIssuerCode() != null
//									&& reportMgmtFacade.isReportInPartIssuerGroup(reportId)){
//								reportFilter.setValue(this.participant.toString());
//							}
//						}else if((userInfo.getUserAccountSession().isIssuerDpfInstitucion())
//								&& filterType.getCode().equals(ReportFilterType.HELP_PARTICIPANTS.getCode())){
//							if(userInfo.getUserAccountSession().getPartIssuerCode() != null
//									&& reportMgmtFacade.isReportInPartIssuerGroup(reportId)){
//								reportFilter.setValue(this.participant.toString());
//							}
//						}
//					}
										
					if(reportFilter.getValue() != null || reportFilter.getMultipleValue()!=null) {
						switch (filterType) {
						case CALENDAR_FUTURE: //CALENDARIO FUTURO
							value = CommonsUtilities.convertDatetoString((Date)reportFilter.getValue());
							break;
						case CALENDAR_DISABLED: //CALENDARIO VENCIMIENTO DE VALORES REPORT 370
							value = CommonsUtilities.convertDatetoString((Date)reportFilter.getValue());
							break;
							
						case CALENDAR:
							value = CommonsUtilities.convertDatetoString((Date)reportFilter.getValue());
							break;
						
						case OPTIONAL_CALENDAR:
							value =(reportFilter.getValue()!=null?CommonsUtilities.convertDatetoString((Date)reportFilter.getValue()):"");
							break;
							
						case CALENDAR_ALL_DATE:
								value = CommonsUtilities.convertDatetoString((Date)reportFilter.getValue());
							break;
							
						case HELP_ISSUERS:
							IssuerTO issuer =(IssuerTO)reportFilter.getValue();
							value = issuer.getIssuerCode();
							parameterDetails.put(reportFilter.getFilterName(), issuer.getIssuerBusinessName());
							break;
							
						case HELP_HOLDERS:
							HolderHelperOutputTO holder =  (HolderHelperOutputTO)reportFilter.getValue();
							if(holder.getHolderId()!= null){
								value = holder.getHolderId().toString();
								parameterDetails.put(reportFilter.getFilterName(), holder.getFullName());
							}
							break;
						
						case COMBO:
						case COMBO_SIMPLE:
						case COMBO_PARAMETERS:
							value = reportFilter.getValue().toString();
//							value=StringUtils.join(reportFilter.getHolderAccounts());
							Iterator<Entry<String, Object>> iterator= reportFilter.getSelectMenu().entrySet().iterator();
							while(iterator.hasNext()) {
								Entry<String, Object> entry = iterator.next();
								if(null!=entry.getValue() && entry.getValue().toString().equals(value)) {
									parameterDetails.put(reportFilter.getFilterName(), entry.getKey());
								}
							}
							break;
						case HELP_PARTICIPANTS:
							value = reportFilter.getValue().toString();
							Iterator<Entry<String, Object>> iteratorPart= reportFilter.getSelectMenu().entrySet().iterator();
							while(iteratorPart.hasNext()) {
								Entry<String, Object> entry = iteratorPart.next();
								if(null!=entry.getValue() && entry.getValue().toString().equals(value)) {
									System.out.println("HELP PARTICIPANT ->>"+reportFilter.getFilterName() +" : "+reportFilter.getKeyFilter());
									parameterDetails.put(reportFilter.getFilterName(), entry.getKey());
								}
							}
							break;
						case COMBO_MULTIPLE:
							List<String> intermediateList = new ArrayList<String>();
							List<String> multipleValue = reportFilter.getMultipleValue();
							//multipleValue.contains(reportFilter.getMultipleValue());
							value = StringUtils.join(reportFilter.getMultipleValue().toArray(), ",");
							Iterator<Entry<String, Object>> iteratorcm= reportFilter.getSelectMenu().entrySet().iterator();
							while(iteratorcm.hasNext()) {
								Entry<String, Object> entry = iteratorcm.next();
								if(null!=entry.getValue()) {
								   if(multipleValue.contains(entry.getValue().toString())){
									   intermediateList.add(entry.getKey());
								   }
								}
							}
							parameterDetails.put(reportFilter.getFilterName(), StringUtils.join(intermediateList).length()>200?StringUtils.join(intermediateList).substring(0, 200):StringUtils.join(intermediateList));
							break;	
						case HELP_SECURITIES:
							SecuritiesTO securities = (SecuritiesTO)reportFilter.getValue();
							value = securities.getSecuritiesCode();
							parameterDetails.put(reportFilter.getFilterName(), securities.getDescription());
							break;
							
						case HELP_ACCOUNTS:
							HolderAccountHelperResultTO account = (HolderAccountHelperResultTO)reportFilter.getValue();
							accountNumberEntered = account.getAccountNumber();
							accountFilterExists = Boolean.TRUE;
							List<HolderAccount> holderAccount = reportFilter.getHolderAccounts();
							if(holderAccount!=null){
								for (int i = 0; i < holderAccount.size(); i++) {
									HolderAccount hAccount = holderAccount.get(i);
									if(hAccount.getSelected().equals(BooleanType.YES.getBooleanValue())){
										value = hAccount.getIdHolderAccountPk().toString();
										parameterDetails.put(reportFilter.getFilterName(), hAccount.getAccountNumber().toString());
										accountListSelected = Boolean.TRUE;
										break;//
									}
								}
							}
//								else{
//								value = reportFilter.getValue().toString();
//							}
							break;
						case HELP_BILLING:
						BillingServiceTo billingService=(BillingServiceTo)reportFilter.getValue();
						if(billingService.getIdBillingServicePk()!=null)
						{
							value=billingService.getServiceCode();
						}
						break;
						
						default:
							value = reportFilter.getValue().toString();
							break;
						}
					}
					
					parameters.put(reportFilter.getFilterName(), value);
					if("event_type".equals(reportFilter.getFilterName())){
						isEventType = true;
						eventType = value;
					}else if("report_type".equals(reportFilter.getFilterName())){
						isReportType = true;
						reportType = value;
					}
				}
			}
			//Institution Type Default Parameter
			String institutionType = userInfo.getUserAccountSession().getInstitutionType().toString();
			parameters.put(ReportConstant.INSTITUTION_TYPE, institutionType);
    		
			//Id of Issuer DPF Institution Type Default Parameter
			String issuerCode = null;
			if(userInfo.getUserAccountSession().isIssuerDpfInstitucion())
				issuerCode = userInfo.getUserAccountSession().getIssuerCode();
			parameters.put(ReportConstant.ISSUER_CODE_INSTITUTION_TYPE, issuerCode);
			
			//Id of Participant Investor Institution Type Default Parameter
			String participantCode = null;
			if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion())
				participantCode = userInfo.getUserAccountSession().getParticipantCode().toString();
			parameters.put(ReportConstant.PARTICIPANT_INVESTOR_INSTITUTION_TYPE, participantCode);
			
			if(accountFilterExists = Boolean.TRUE && accountListSelected == Boolean.FALSE && accountNumberEntered != null){
				Object[] parameter = new Object[]{accountNumberEntered};
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_NOT_SELECTED, parameter));
				JSFUtilities.showValidationDialog();
			}else{
				if(Validations.validateIsNotNullAndPositive(reportId)){
					if(reportId.equals(ReportIdType.CLIENT_PORTAFOLIO.getCode()) && ReportConstant.LABEL_EXCEL_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.CLIENT_PORTAFOLIO_POI.getCode();
					}else if(reportId.equals(ReportIdType.CLIENT_PORTAFOLIO_POI.getCode()) && ReportConstant.LABEL_PDF_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.CLIENT_PORTAFOLIO.getCode();
					}
					
					if(reportId.equals(ReportIdType.SETTLED_OPERATIONS.getCode()) && ReportConstant.LABEL_EXCEL_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.SETTLED_OPERATIONS_POI.getCode();
					}else if(reportId.equals(ReportIdType.SETTLED_OPERATIONS_POI.getCode()) && ReportConstant.LABEL_PDF_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.SETTLED_OPERATIONS.getCode();
					}
					
					
					if(reportId.equals(ReportIdType.DETAILS_TITLES_PENDING_EXPIRATION.getCode()) && ReportConstant.LABEL_EXCEL_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.DETAILS_TITLES_PENDING_EXPIRATION_POI.getCode();
					}else if(reportId.equals(ReportIdType.DETAILS_TITLES_PENDING_EXPIRATION_POI.getCode()) && ReportConstant.LABEL_PDF_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.DETAILS_TITLES_PENDING_EXPIRATION.getCode();
					}
					
					if(reportId.equals(ReportIdType.REGISTRATION_ASSIGNMENT_HOLDERS.getCode()) && ReportConstant.LABEL_EXCEL_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.REGISTRATION_ASSIGNMENT_HOLDERS_POI.getCode();
					}else if(reportId.equals(ReportIdType.REGISTRATION_ASSIGNMENT_HOLDERS_POI.getCode()) && ReportConstant.LABEL_PDF_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.REGISTRATION_ASSIGNMENT_HOLDERS.getCode();
					}
					
					//NUEVO
					if(reportId.equals(ReportIdType.COMMISIONS_ANNOTATION_ACCOUNT.getCode()) && ReportConstant.LABEL_EXCEL_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.COMMISIONS_ANNOTATION_ACCOUNT_POI.getCode();
					}else if(reportId.equals(ReportIdType.COMMISIONS_ANNOTATION_ACCOUNT_POI.getCode()) && ReportConstant.LABEL_PDF_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.COMMISIONS_ANNOTATION_ACCOUNT.getCode();
					}

					if(reportId.equals(ReportIdType.BLOCKAGES_UNLOCK.getCode()) && ReportConstant.LABEL_EXCEL_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.BLOCKAGES_UNLOCK_POI.getCode();
					}else if(reportId.equals(ReportIdType.BLOCKAGES_UNLOCK_POI.getCode()) && ReportConstant.LABEL_PDF_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.BLOCKAGES_UNLOCK.getCode();
					}
					
					if(reportId.equals(ReportIdType.COMMISIONS_SUMMARY.getCode()) && ReportConstant.LABEL_EXCEL_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.COMMISIONS_SUMMARY_POI.getCode();
					}else if(reportId.equals(ReportIdType.COMMISIONS_SUMMARY_POI.getCode()) && ReportConstant.LABEL_PDF_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.COMMISIONS_SUMMARY.getCode();
					}
					
					if(reportId.equals(ReportIdType.COMMISIONS_ISSUANCE_DPF.getCode()) && ReportConstant.LABEL_EXCEL_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.COMMISIONS_ISSUANCE_DPF_POI.getCode();
					}else if(reportId.equals(ReportIdType.COMMISIONS_SUMMARY_POI.getCode()) && ReportConstant.LABEL_PDF_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.COMMISIONS_ISSUANCE_DPF.getCode();
					}
					
					if(reportId.equals(ReportIdType.COMMISIONS_PHYSICAL_CUSTODY.getCode()) && ReportConstant.LABEL_EXCEL_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.COMMISIONS_PHYSICAL_CUSTODY_POI.getCode();
					}else if(reportId.equals(ReportIdType.COMMISIONS_PHYSICAL_CUSTODY_POI.getCode()) && ReportConstant.LABEL_PDF_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.COMMISIONS_PHYSICAL_CUSTODY.getCode();
					}
					
					if(reportId.equals(ReportIdType.COMMISIONS_SUMMARY_PORTFOLIO.getCode()) && ReportConstant.LABEL_EXCEL_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.COMMISIONS_SUMMARY_PORTFOLIO_POI.getCode();
					}else if(reportId.equals(ReportIdType.COMMISIONS_SUMMARY_PORTFOLIO_POI.getCode()) && ReportConstant.LABEL_PDF_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.COMMISIONS_SUMMARY_PORTFOLIO.getCode();
					}
					
					if(reportId.equals(ReportIdType.COMMISIONS_SUMMARY.getCode()) && ReportConstant.LABEL_EXCEL_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.COMMISIONS_SUMMARY_POI.getCode();
					}else if(reportId.equals(ReportIdType.COMMISIONS_SUMMARY_POI.getCode()) && ReportConstant.LABEL_PDF_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.COMMISIONS_SUMMARY.getCode();
					}
					
					if(reportId.equals(ReportIdType.REQUEST_ACCRE_HOLDER_NOMINAL_VALUE.getCode()) && ReportConstant.LABEL_EXCEL_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.REQUEST_ACCRE_HOLDER_NOMINAL_VALUE_POI.getCode();
					}else if(reportId.equals(ReportIdType.REQUEST_ACCRE_HOLDER_NOMINAL_VALUE_POI.getCode()) && ReportConstant.LABEL_PDF_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.REQUEST_ACCRE_HOLDER_NOMINAL_VALUE.getCode();
					}
					
					if(reportId.equals(ReportIdType.BALANCE_TRANSFER_AVAILABLE.getCode()) && ReportConstant.LABEL_EXCEL_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.BALANCE_TRANSFER_AVAILABLE_POI.getCode();
					}else if(reportId.equals(ReportIdType.BALANCE_TRANSFER_AVAILABLE_POI.getCode()) && ReportConstant.LABEL_PDF_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.BALANCE_TRANSFER_AVAILABLE.getCode();
					}
					
					if(reportId.equals(ReportIdType.ENTRIES_VOLUNTARY_ACCOUNT_ISSUER.getCode()) && ReportConstant.LABEL_EXCEL_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.ENTRIES_VOLUNTARY_ACCOUNT_ISSUER_POI.getCode();
					}else if(reportId.equals(ReportIdType.ENTRIES_VOLUNTARY_ACCOUNT_ISSUER_POI.getCode()) && ReportConstant.LABEL_PDF_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.ENTRIES_VOLUNTARY_ACCOUNT_ISSUER.getCode();
					}
					
					if(reportId.equals(ReportIdType.TRADING_RECORD_BBV.getCode()) && ReportConstant.LABEL_EXCEL_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.TRADING_RECORD_BBV_POI.getCode();
					}else if(reportId.equals(ReportIdType.TRADING_RECORD_BBV_POI.getCode()) && ReportConstant.LABEL_PDF_FORMAT.equals(parameterDetails.get("reportFormats"))){
						reportId=ReportIdType.TRADING_RECORD_BBV.getCode();
					}
					
					// TODO CODIGO DE PRUEBA ISSUE 1053 (ESTO HAY QUE ADICONAR LOS ID DE COMMONS)
					if (reportId.equals(125L) && ReportConstant.LABEL_EXCEL_FORMAT.equals(parameterDetails.get("reportFormats"))) {
						reportId = 309L;
					} else if (reportId.equals(309L) && ReportConstant.LABEL_PDF_FORMAT.equals(parameterDetails.get("reportFormats"))) {
						reportId = 125L;
					}
					
				    //CODIGO DE PRUEBA ISSUE 924 (ESTO HAY QUE ADICONAR LOS ID DE COMMONS)
					if (reportId.equals(69L) && ReportConstant.LABEL_EXCEL_FORMAT.equals(parameterDetails.get("reportFormats"))) {
						reportId = 313L;
					} else if (reportId.equals(313L) && ReportConstant.LABEL_PDF_FORMAT.equals(parameterDetails.get("reportFormats"))) {
						reportId = 69L;
					}
					
					//  CODIGO DE PRUEBA ISSUE 1097 (ESTO HAY QUE ADICONAR LOS ID DE COMMONS)
					if (reportId.equals(314L) && ReportConstant.LABEL_EXCEL_FORMAT.equals(parameterDetails.get("reportFormats"))) {
						reportId = 312L;
					} else if (reportId.equals(312L) && ReportConstant.LABEL_PDF_FORMAT.equals(parameterDetails.get("reportFormats"))) {
						reportId = 314L;
					}
					
					// ISSUE 1166 se adicono un reporte en formato POI
					if (reportId.equals(162L) && ReportConstant.LABEL_EXCEL_FORMAT.equals(parameterDetails.get("reportFormats"))) {
						reportId = 318L;
					} else if (reportId.equals(318L) && ReportConstant.LABEL_PDF_FORMAT.equals(parameterDetails.get("reportFormats"))) {
						reportId = 162L;
					}
					
					// ISSUE 1200 se adicono un reporte en formato POI
					if (reportId.equals(323L) && ReportConstant.LABEL_EXCEL_FORMAT.equals(parameterDetails.get("reportFormats"))) {
						reportId = 325L;
					} else if (reportId.equals(325L) && ReportConstant.LABEL_PDF_FORMAT.equals(parameterDetails.get("reportFormats"))) {
						reportId = 323L;
					}
					
					// ISSUE 1228
					if (reportId.equals(327L) && ReportConstant.LABEL_EXCEL_FORMAT.equals(parameterDetails.get("reportFormats"))) {
						reportId = 328L;
					} else if (reportId.equals(328L) && ReportConstant.LABEL_PDF_FORMAT.equals(parameterDetails.get("reportFormats"))) {
						reportId = 327L;
					}
		
					// ISSUE 1028 se adicono un reporte en formato POI
					if (reportId.equals(324L) && ReportConstant.LABEL_EXCEL_FORMAT.equals(parameterDetails.get("reportFormats"))) {
						reportId = 326L;
					} else if (reportId.equals(326L) && ReportConstant.LABEL_PDF_FORMAT.equals(parameterDetails.get("reportFormats"))) {
						reportId = 324L;
					}
					
					// ISSUE 1285 se adicono un reporte en formato POI
					if (reportId.equals(222L) && ReportConstant.LABEL_EXCEL_FORMAT.equals(parameterDetails.get("reportFormats"))) {
						reportId = 335L;
					} else if (reportId.equals(335L) && ReportConstant.LABEL_PDF_FORMAT.equals(parameterDetails.get("reportFormats"))) {
						reportId = 222L;
					}
					
					// ISSUE 1337 se adicono un reporte en formato POI
					if (reportId.equals(279L) && ReportConstant.LABEL_EXCEL_FORMAT.equals(parameterDetails.get("reportFormats"))) {
						reportId = 341L;
					} else if (reportId.equals(341L) && ReportConstant.LABEL_PDF_FORMAT.equals(parameterDetails.get("reportFormats"))) {
						reportId = 279L;
					}
					
					// ISSUE 1341 se adicono un reporte en formato POI
					if (reportId.equals(342L) && ReportConstant.LABEL_EXCEL_FORMAT.equals(parameterDetails.get("reportFormats"))) {
						reportId = 345L;
					} else if (reportId.equals(345L) && ReportConstant.LABEL_PDF_FORMAT.equals(parameterDetails.get("reportFormats"))) {
						reportId = 342L;
					}
					
					reportGenerationFacade.saveReportExecution(reportId, parameters, parameterDetails, reportUser);
					RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje de alerta", "Reporte generado exitosamente"));
					
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
							PropertiesUtilities.getMessage(PropertiesConstants.REPORT_SUCESS_GENERATION));
					JSFUtilities.showComponent(CONFIRM_DIALOG_SUCCESS); 
//					JSFUtilities.executeJavascriptFunction("PF('reportGenerationOkWidget').show()");  
					
				}else{
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(PropertiesConstants.ALERT_MESSAGE),
							PropertiesUtilities.getMessage(PropertiesConstants.REPORT_SUCESS_FAIL));
					JSFUtilities.showComponent(CONFIRM_DIALOG_SUCCESS);
//					JSFUtilities.executeJavascriptFunction("PF('reportGenerationOkWidget').show()");  
					RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje de alerta", "Reporte no pudo ser generado"));
				}
			}
			
		} catch(Exception ex){
			ex.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(ex));
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje de alerta", "Reporte no pudo ser generado"));
		}
		return null;
	}
	
	/**
	 * to find the report id based on combination of event type and report type.
	 *
	 * @param strReportComb the str report comb
	 * @return Long reportId
	 */
	public Long findReportId(String strReportComb){
		
		Long reportId = Long.valueOf(0);
		
		if(( ImportanceEventType.CASH_DIVIDENDS.getCode()+"-"+CorporativeEventReportType.CONTROL_OF_PAYMENTS.getCode()).equals(strReportComb)){
				reportId = ReportIdType.DIVIDEND_PAYMENT_IN_PAYMENTS_CONTROL.getCode(); 
		} else if(( ImportanceEventType.PREFERRED_SUSCRIPTION.getCode()+"-"+CorporativeEventReportType.CONTROL_OF_PAYMENTS.getCode()).equals(strReportComb)){
				reportId = ReportIdType.SUBSCRIPTION_FEE_PAYMENT_CONTORL.getCode();
		} else if(( ImportanceEventType.ACTION_DIVIDENDS.getCode()+"-"+CorporativeEventReportType.CONTROL_OF_PAYMENTS.getCode()).equals(strReportComb)){
				reportId = ReportIdType.SHARE_DIVIDEND_PAYMENT_CONTROL.getCode();
		} else if(( ImportanceEventType.INTEREST_PAYMENT.getCode()+"-"+CorporativeEventReportType.CONTROL_OF_PAYMENTS.getCode()).equals(strReportComb)){
				reportId = ReportIdType.INTEREST_PAYMENT_CONTROL.getCode();
		} else if(( ImportanceEventType.CAPITAL_AMORTIZATION.getCode()+"-"+CorporativeEventReportType.CONTROL_OF_PAYMENTS.getCode()).equals(strReportComb)){
				reportId = ReportIdType.AMORTIZATION_PAY_CONTOL_OF_PAYMENTS.getCode();
		} else if(( ImportanceEventType.INTEREST_PAYMENT.getCode()+"-"+CorporativeEventReportType.HOLDER_ACCOUNT_FOR_PARTICIPANTS.getCode()).equals(strReportComb)){
				reportId = ReportIdType.INEREST_PAY_PARTICIPANTS_ACCHDR.getCode();
		} else if(( ImportanceEventType.INTEREST_PAYMENT.getCode()+"-"+CorporativeEventReportType.FOR_SECURITY_ISSUER.getCode()).equals(strReportComb)){
				reportId = ReportIdType.INTEREST_PAYMENT_SECURITY_FOR_ISSUER.getCode();
		} else if(( ImportanceEventType.INTEREST_PAYMENT.getCode()+"-"+CorporativeEventReportType.BLOCKED_BY_BALANCE.getCode()).equals(strReportComb)){
			    reportId = ReportIdType.INTEREST_PAYMENT_BY_BLOCKED_BALANCE.getCode();
	    } else if(( ImportanceEventType.INTEREST_PAYMENT.getCode()+"-"+CorporativeEventReportType.REPURCHASE_BY_MARGIN.getCode()).equals(strReportComb)){
				reportId = ReportIdType.INTEREST_PAY_REPURCHASE_AND_MARGIN.getCode();
		} else if(( ImportanceEventType.CAPITAL_AMORTIZATION.getCode()+"-"+CorporativeEventReportType.HOLDER_ACCOUNT_FOR_PARTICIPANTS.getCode()).equals(strReportComb)){
				reportId = ReportIdType.AMORTIZATION_PAYMENT_ACCHDR_PART.getCode();
		} else if(( ImportanceEventType.CAPITAL_AMORTIZATION.getCode()+"-"+CorporativeEventReportType.FOR_SECURITY_ISSUER.getCode()).equals(strReportComb)){
				reportId = ReportIdType.AMORTIZATION_PAYMENT_SECURITY_ISSUER.getCode();
		} else if(( ImportanceEventType.CAPITAL_AMORTIZATION.getCode()+"-"+CorporativeEventReportType.BLOCKED_BY_BALANCE.getCode()).equals(strReportComb)){
				reportId = ReportIdType.AMORTIZATION_PAYMENT_BALLOCKED.getCode();
		} else if(( ImportanceEventType.CAPITAL_AMORTIZATION.getCode()+"-"+CorporativeEventReportType.REPURCHASE_BY_MARGIN.getCode()).equals(strReportComb)){
				reportId = ReportIdType.AMORTIZATION_PAY_REPURCHASE_MARGIN.getCode();
		} else if(( ImportanceEventType.CASH_DIVIDENDS.getCode()+"-"+CorporativeEventReportType.HOLDER_ACCOUNT_FOR_PARTICIPANTS.getCode()).equals(strReportComb)){
				reportId = ReportIdType.CASH_DIVIDENDS_PAY_BY_PARTS_ACCHDR.getCode();
		} else if(( ImportanceEventType.CASH_DIVIDENDS.getCode()+"-"+CorporativeEventReportType.FOR_SECURITY_ISSUER.getCode()).equals(strReportComb)){
				reportId = ReportIdType.CASH_DIVIDENDS_PAID_BY_SECURITY_ISSUER.getCode();
		} else if(( ImportanceEventType.CASH_DIVIDENDS.getCode()+"-"+CorporativeEventReportType.BLOCKED_BY_BALANCE.getCode()).equals(strReportComb)){
				reportId = ReportIdType.CASH_DIVIDENDS_PAID_BY_BALLOCKED.getCode();
		} else if(( ImportanceEventType.CASH_DIVIDENDS.getCode()+"-"+CorporativeEventReportType.REPURCHASE_BY_MARGIN.getCode()).equals(strReportComb)){
				reportId = ReportIdType.CASH_DIVIDENDS_PAID_REPURCH_MARGIN.getCode();
		} else if(( ImportanceEventType.ACTION_DIVIDENDS.getCode()+"-"+CorporativeEventReportType.HOLDER_ACCOUNT_FOR_PARTICIPANTS.getCode()).equals(strReportComb)){
				reportId = ReportIdType.SHARE_DIVIDEND_PAY_BY_ACCHDR_PART.getCode();
		} else if(( ImportanceEventType.ACTION_DIVIDENDS.getCode()+"-"+CorporativeEventReportType.FOR_SECURITY_ISSUER.getCode()).equals(strReportComb)){
				reportId = ReportIdType.SHARE_DIVIDEND_PAY_FOR_SECURTIY_ISSUER.getCode();
		} else if(( ImportanceEventType.ACTION_DIVIDENDS.getCode()+"-"+CorporativeEventReportType.BLOCKED_BY_BALANCE.getCode()).equals(strReportComb)){
				reportId = ReportIdType.DIVIDENDS_PAID_BY_BLOCKED_BALANCE.getCode();
		} else if(( ImportanceEventType.ACTION_DIVIDENDS.getCode()+"-"+CorporativeEventReportType.REPURCHASE_BY_MARGIN.getCode()).equals(strReportComb)){
				reportId = ReportIdType.DIVIDENDS_PAID_ON_REPURCHASE_MARGIN.getCode();
		} else if(( ImportanceEventType.PREFERRED_SUSCRIPTION.getCode()+"-"+CorporativeEventReportType.HOLDER_ACCOUNT_FOR_PARTICIPANTS.getCode()).equals(strReportComb)){
				reportId = ReportIdType.PAYMENT_ACCOUNT_HOLDER_PARTICIPANTS.getCode();
		} else if(( ImportanceEventType.PREFERRED_SUSCRIPTION.getCode()+"-"+CorporativeEventReportType.FOR_SECURITY_ISSUER.getCode()).equals(strReportComb)){
				reportId = ReportIdType.SUBSCRIPTION_PAYMT_FOR_SECURITY_ISSUER.getCode();
		}else if(( ImportanceEventType.PREFERRED_SUSCRIPTION.getCode()+"-"+CorporativeEventReportType.BLOCKED_BY_BALANCE.getCode()).equals(strReportComb)){
				reportId = ReportIdType.SUBSCRIPTION_PAYMT_BY_BLOCKED_BALANCE.getCode();
	    }else if(( ImportanceEventType.PREFERRED_SUSCRIPTION.getCode()+"-"+CorporativeEventReportType.REPURCHASE_BY_MARGIN.getCode()).equals(strReportComb)){
				reportId = ReportIdType.SUBSCRIPTION_PAY_REPURCHASE_MARGIN.getCode();
		}else if(( ImportanceEventType.CAPITAL_REDUCTION.getCode()+"-"+CorporativeEventReportType.HOLDER_ACCOUNT_FOR_PARTICIPANTS.getCode()).equals(strReportComb)){
				reportId = ReportIdType.SPECIAL_PROCESS_HOLDER_PARTICIPANT.getCode();
		} else if(( ImportanceEventType.CAPITAL_REDUCTION.getCode()+"-"+CorporativeEventReportType.FOR_SECURITY_ISSUER.getCode()).equals(strReportComb)){
				reportId = ReportIdType.SPECIAL_REPORT_FOR_SECURITY_ISSUER.getCode();
		} else if(( ImportanceEventType.CAPITAL_REDUCTION.getCode()+"-"+CorporativeEventReportType.REPURCHASE_BY_MARGIN.getCode()).equals(strReportComb)){
				reportId = ReportIdType.SPECIAL_REPORT_ON_REPURCHASE_MARGIN.getCode();
		} else if(( ImportanceEventType.CAPITAL_REDUCTION.getCode()+"-"+CorporativeEventReportType.BLOCKED_BY_BALANCE.getCode()).equals(strReportComb)){
				reportId = ReportIdType.SPECIAL_REPORT_BY_BLOCKED_BALANCE.getCode();
		} else if(( ImportanceEventType.CAPITAL_REDUCTION.getCode()+"-"+CorporativeEventReportType.SPECIAL_PROCESS_CONTROL_LIST.getCode()).equals(strReportComb)){
				reportId = ReportIdType.SPECIAL_PROCESS_LIST_OF_CONTROL.getCode();
		}
		return reportId;
	}
	
	/**
	 * Load.
	 */
	
	public void load(){
		FacesContext contex = FacesContext.getCurrentInstance();
		try {
		if(!contex.isPostback()){
			//load first time
			if(reportId != null) {
				report = reportMgmtFacade.getReportInfo(reportId);
				form = reportMgmtFacade.loadReporte(reportId,userInfo.getUserAccountSession(),userInfo.getCurrentLanguage());
			}
			try {
				ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
				Map<String, Object> sessionMap = externalContext.getSessionMap();
				sessionMap.put(GeneralConstants.REPORT_BY_PROCESS, reportId);
			} catch (Exception e) {
				e.printStackTrace();
			}
			//choose item by default
			for (DynaFormControl dynaFormControl : form.getControls()) {  
				if (dynaFormControl.getData() instanceof ReportFilter) {
					ReportFilter reportFilter = (ReportFilter)dynaFormControl.getData();
					ReportFilterType filterType = ReportFilterType.get(reportFilter.getFilterType());
					
					switch (filterType) {
					case COMBO:
					case COMBO_SIMPLE:
						LinkedHashMap<String, Object> value= reportFilter.getSelectMenu();
						if(Validations.validateIsNotNull(value)){
							if(value.containsValue(BigDecimal.valueOf(RequestType.DEPOSITEO.getCode()))){
								//when we want to show an specific an element, set 1
								showElement=BooleanType.YES.getBooleanValue();
							}else{
								showElement=BooleanType.NO.getBooleanValue();
							}
							if((userInfo.getUserAccountSession().isParticipantInstitucion() || userInfo.getUserAccountSession().isParticipantInvestorInstitucion())
									&& userInfo.getUserAccountSession().getParticipantCode().toString().equals(idParticipantBC)
									&& reportFilter.getFilterName().equals(ReportConstant.BCB_FORMAT)){
								reportFilter.setValue(GeneralConstants.ONE_VALUE_INTEGER);
//								reportFilter.setDisabled(true);
							}
							//issue 615
							if (reportFilter.getFilterName().equals(ReportConstant.BCB_FORMAT) && reportMgmtFacade.isReportTransferValPurchase(reportId)){
								reportFilter.setDisabled(false);
							}
							if(reportId.equals(ReportIdType.CLIENT_PORTAFOLIO.getCode()) && reportFilter.getFilterName().equalsIgnoreCase("portafolio_type")){
								reportFilter.setValue(IssuanceType.DEMATERIALIZED.getCode());
							}
							
							// issue 1360: restriccion para perfil EMISOR BOLSA
							if(reportId.equals(222L) && reportFilter.getFilterName().equalsIgnoreCase("issuer")){
								if( Validations.validateIsNotNull(userInfo.getUserAccountSession())
										&& Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getIssuerCode())){
									reportFilter.setValue(userInfo.getUserAccountSession().getIssuerCode());
									reportFilter.setDisabled(true);
								}else if ( Validations.validateIsNotNull(userInfo.getUserAccountSession())
											&& userInfo.getUserAccountSession().isDepositaryInstitution() ){
									reportFilter.setValue(null);
								}else {
									reportFilter.setValue(null);
									reportFilter.setDisabled(true);
								}
							}
						}
						break;
					case COMBO_PARAMETERS:
						if(reportId.equals(new Long(1)) && reportFilter.getFilterName().equals(ReportConstant.PARTICIPANT_STATE_PARAM)){
							reportFilter.setValue(ParticipantStateType.REGISTERED.getCode());
						}
						break;
					case CALENDAR:
						if(reportFilter.getFilterName().equals(ReportConstant.DATE_DISABLED)){
							reportFilter.setDisabled(true);
						}
						if(!parameters.containsKey(ReportConstant.DATE_INITIAL_PARAM)) {
							parameters.put(ReportConstant.DATE_INITIAL_PARAM, ReportConstant.DATE_INITIAL_PARAM);
						}
						if(!parameters.containsKey(ReportConstant.DATE_END_PARAM)) {
							parameters.put(ReportConstant.DATE_END_PARAM, ReportConstant.DATE_END_PARAM);
						}
						if(!parameters.containsKey(ReportConstant.COURT_DATE_PARAM)) {
							parameters.put(ReportConstant.COURT_DATE_PARAM, ReportConstant.COURT_DATE_PARAM);
						}
						validationInitialEndCalendar();
						break;
					case OPTIONAL_CALENDAR: 
						reportFilter.setValue(null);					
						break; 
					case CALENDAR_ALL_DATE:
						if(reportId.equals(new Long(75))){
							maxDateInitial= CommonsUtilities.addDaysToDate(CommonsUtilities.currentDate(),-1);
							reportFilter.setValue(CommonsUtilities.addDaysToDate(CommonsUtilities.currentDate(),-1));
							}
						else
							maxDateInitial= CommonsUtilities.currentDate();
						break;
					case CALENDAR_DISABLED:
							reportFilter.setValue(CommonsUtilities.addDaysToDate(CommonsUtilities.currentDate(),1));
						break;
					case HELP_PARTICIPANTS:
						if(reportMgmtFacade.isReportForBCB(reportId)){
							break;
						}	
						if(userInfo.getUserAccountSession().isBcbInstitution() && reportMgmtFacade.isReportTransferValPurchase(reportId)){
							break;
						}
						if(userInfo.getUserAccountSession().isParticipantInstitucion() || userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
							this.participant = userInfo.getUserAccountSession().getParticipantCode();
							
						//is institution type a emisor dpf and report in participant related issuer group and the code participant is not null
						}else if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()
										&& userInfo.getUserAccountSession().getPartIssuerCode() != null
										&& reportMgmtFacade.isReportInPartIssuerGroup(reportId)){
							this.participant = userInfo.getUserAccountSession().getPartIssuerCode();
						}
						if(userInfo.getUserAccountSession().isIssuerInstitucion() && (reportId.equals(ReportIdType.CLIENT_PORTAFOLIO.getCode())||reportId.equals(ReportIdType.CLIENT_PORTAFOLIO_POI.getCode()))){
							reportFilter.setDisabled(false);
						}
						if(!parameters.containsKey(PARTICIPANT)) {
							parameters.put(PARTICIPANT, PARTICIPANT);
						}
						break;
						
					case HELP_ISSUERS:
						/**
						 * Si el Participante es inversionista y para el reporte de Facturacion, setearle el 
						 * emisor
						 */
						if(userInfo.getUserAccountSession().isIssuerInstitucion() || userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
							this.issuer = userInfo.getUserAccountSession().getIssuerCode();
						}else{
							//issue 679 inclusion de participantes en la regla
							if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion() 
									|| userInfo.getUserAccountSession().isParticipantInstitucion()){
								IssuerTO issuerTo =null;
								if(ReportIdType.CALCULATION_ISSUANCE_DEMATERIALIZATED.getCode().equals(reportId)){
									issuerTo=participantServiceBean.getIssuerByParticipant(userInfo.getUserAccountSession().getParticipantCode());
									reportFilter.setDisabled(true);
								}
								if(ReportIdType.CALCULATION_ISSUANCE_DPF_REPORT.getCode().equals(reportId)){
									issuerTo=participantServiceBean.getIssuerMnemonicIsParticipant(userInfo.getUserAccountSession().getParticipantCode());
									reportFilter.setDisabled(true);
								}
								if(Validations.validateIsNotNull(issuerTo)){
									issuer=issuerTo.getMnemonic();
									issuerDesc=issuerTo.getIssuerDesc();
									reportFilter.setValue(issuerTo);
									reportFilter.setDisabled(true);
								}
							}
							if(ReportIdType.CLIENT_PORTAFOLIO.getCode().equals(this.reportId)){
								if(userInfo.getUserAccountSession().isParticipantInstitucion()
										|| userInfo.getUserAccountSession().isParticipantInvestorInstitucion()
										|| userInfo.getUserAccountSession().isAfpInstitution()
										|| userInfo.getUserAccountSession().isBcbInstitution()
										|| userInfo.getUserAccountSession().isStockExchange()){
									reportFilter.setDisabled(false);
								}
							}
							
							//issue 1097: si se trata del reporte 314 y un ususario BCB se deshabilita el helper del emisor
							if (reportId==314L && Validations.validateIsNotNull(userInfo.getUserAccountSession().getParticipantCode())
									&& userInfo.getUserAccountSession().getParticipantCode() == 114L) {
								reportFilter.setDisabled(true);
							}
						}
						break;
					case HELP_HOLDERS:
						if(!parameters.containsKey(PARTICIPANT)) {
							parameters.put(PARTICIPANT, PARTICIPANT);
						}
						if(!parameters.containsKey(HOLDER)) {
							parameters.put(HOLDER, HOLDER);
						}
						break;
					case HELP_ACCOUNTS :
						if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()
								&& userInfo.getUserAccountSession().getPartIssuerCode() != null){
							this.participant = userInfo.getUserAccountSession().getPartIssuerCode();
						}
						
						if(!parameters.containsKey(PARTICIPANT)) {
							parameters.put(PARTICIPANT, PARTICIPANT);
						}
						if(!parameters.containsKey(HOLDER)) {
							parameters.put(HOLDER, HOLDER);
						}
						if(!parameters.containsKey(ACCOUNT)) {
							parameters.put(ACCOUNT, ACCOUNT);
						}
						break;
					case HELP_SECURITIES :
						if(userInfo.getUserAccountSession().isIssuerInstitucion() || userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
							this.issuer = userInfo.getUserAccountSession().getIssuerCode();
						}
						break;
					}
				}
			}
			
		}
		} catch(ServiceException sexc) {
			
			sexc.printStackTrace();
		} catch (Exception ex) {
			log.error(ex.toString());
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Clean.
	 */
	public void clean(){
		JSFUtilities.hideComponent(CONFIRM_DIALOG_SUCCESS);
		FacesContext fc = FacesContext.getCurrentInstance();  
        DynaForm dynaForm = (DynaForm) fc.getViewRoot().findComponent(":frmReportForm:dynaForm");  
        VisitTaskExecutor visitTaskExecutor =  new ClearInputs(fc.getELContext(), RequestContext.getCurrentInstance(),
        		form.getControls().size(),userInfo.getUserAccountSession());  
        // clear inputs in the visit callback  
        ExecutableVisitCallback visitCallback = new ExecutableVisitCallback(visitTaskExecutor);  
        dynaForm.visitTree(VisitContext.createVisitContext(fc, null, VISIT_HINTS), visitCallback);

//        if(reportId != null) {
//			report = reportMgmtFacade.getReportInfo(reportId);
//			try {
//				form = reportMgmtFacade.loadReporte(reportId,userInfo.getUserAccountSession(),userInfo.getCurrentLanguage());
//			} catch (Exception ex) {
//				log.error(ex.toString());
//				excepcion.fire(new ExceptionToCatchEvent(ex));
//			}
//		}
        
        for (DynaFormControl dynaFormControl : form.getControls()) {  
			if (dynaFormControl.getData() instanceof ReportFilter) {
				ReportFilter reportFilter = (ReportFilter)dynaFormControl.getData();
				if(reportFilter.getValue() != null) {
					ReportFilterType filterType = ReportFilterType.get(reportFilter.getFilterType());
					switch (filterType) {
					case HELP_ACCOUNTS:
						HolderAccountHelperResultTO holderHelper= new HolderAccountHelperResultTO();
						reportFilter.setHolderAccounts(null);
						reportFilter.setValue(holderHelper);
						idHolder = null;
						break;
					case COMBO_MULTIPLE:
						reportFilter.setMultipleValue(null);
						break;
					case COMBO_SIMPLE:
						reportFilter.setValue(null);
						break;
					case HELP_ISSUERS:
						/**
						 * Si el Participante es inversionista y para el reporte de Facturacion, setearle el 
						 * emisor
						 */
						if(userInfo.getUserAccountSession().isIssuerInstitucion() || userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
							this.issuer = userInfo.getUserAccountSession().getIssuerCode();
						}else{
							reportFilter.setValue(new IssuerTO());
							//issue 679 inclusion de participantes en la regla
							if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion() 
									|| userInfo.getUserAccountSession().isParticipantInstitucion()){
								IssuerTO issuerTo =null;
								if(ReportIdType.CALCULATION_ISSUANCE_DEMATERIALIZATED.getCode().equals(reportId)){
									issuerTo=participantServiceBean.getIssuerByParticipant(userInfo.getUserAccountSession().getParticipantCode());
									reportFilter.setDisabled(true);
								}
								if(ReportIdType.CALCULATION_ISSUANCE_DPF_REPORT.getCode().equals(reportId)){
									issuerTo=participantServiceBean.getIssuerMnemonicIsParticipant(userInfo.getUserAccountSession().getParticipantCode());
									reportFilter.setDisabled(true);
								}
								if(Validations.validateIsNotNull(issuerTo)){
									issuer=issuerTo.getMnemonic();
									issuerDesc=issuerTo.getIssuerDesc();
									reportFilter.setValue(issuerTo);
									reportFilter.setDisabled(true);
								}
							}
							if(ReportIdType.CLIENT_PORTAFOLIO.getCode().equals(this.reportId)){
								if(userInfo.getUserAccountSession().isParticipantInstitucion()
										|| userInfo.getUserAccountSession().isParticipantInvestorInstitucion()
										|| userInfo.getUserAccountSession().isAfpInstitution()
										|| userInfo.getUserAccountSession().isBcbInstitution()
										|| userInfo.getUserAccountSession().isStockExchange()){
									reportFilter.setDisabled(false);
								}
							}
							
							//issue 1097: si se trata del reporte 314 y un ususario BCB se deshabilita el helper del emisor
							if (reportId==314L && Validations.validateIsNotNull(userInfo.getUserAccountSession().getParticipantCode())
									&& userInfo.getUserAccountSession().getParticipantCode() == 114L) {
								reportFilter.setDisabled(true);
							}
						}
						break;
					case HELP_PARTICIPANTS:
						if(!userInfo.getUserAccountSession().isParticipantInstitucion() 
								&& !userInfo.getUserAccountSession().isParticipantInvestorInstitucion()
								){
							reportFilter.setValue(null);
						}
						break;
					case CALENDAR_FUTURE:
						//min date end calendar
						this.minDateEnd = CommonsUtilities.currentDate();
						// max date initial calendar
						this.maxDateInitial = CommonsUtilities.currentDate();
						break;
					}
				}
//				if(reportId.equals(ReportIdType.CLIENT_PORTAFOLIO.getCode()) && reportFilter.getFilterName().equalsIgnoreCase("portafolio_type")){
//					reportFilter.setValue(IssuanceType.DEMATERIALIZED.getCode());
//				}
//				if(reportId.equals(ReportIdType.CLIENT_PORTAFOLIO_POI.getCode()) && reportFilter.getFilterName().equalsIgnoreCase("portafolio_type")){
//					reportFilter.setValue(IssuanceType.DEMATERIALIZED.getCode());
//				}
//				try {
//					aditionalValidatios(reportFilter);
//				} catch (ServiceException e) {
//					e.printStackTrace();
//				}
			}
        }
        
//        if(!reportMgmtFacade.isReportForBCB(reportId)){
//        	 if(!(userInfo.getUserAccountSession().isParticipantInstitucion() || 
//        			 userInfo.getUserAccountSession().isParticipantInvestorInstitucion())){
//             	this.participant = null;
//            
//             	if(!(userInfo.getUserAccountSession().isIssuerInstitucion() ||
//               		 userInfo.getUserAccountSession().isIssuerDpfInstitucion())){
//                	this.issuer = null;
//                	
//                	if(userInfo.getUserAccountSession().getPartIssuerCode() == null){
//                		this.participant = null;
//                	}
//                }
//        	 }
//             this.idHolder = null;
//        }
	}

	public void validationInitialEndCalendar() {
		ReportFilter initialCalendar = getFilterByName(ReportConstant.DATE_INITIAL_PARAM);
		ReportFilter endCalendar = getFilterByName(ReportConstant.DATE_END_PARAM);
		ReportFilter courtDate = getFilterByName(ReportConstant.COURT_DATE_PARAM);
		if(initialCalendar != null && endCalendar != null) {
			//min date end calendar
			this.minDateEnd = (Date)initialCalendar.getValue();
			//max date end calendar
			this.maxDateEnd = getCurrentSystemDate();
			// max date initial calendar
			this.maxDateInitial = (Date)endCalendar.getValue();
		} else if (courtDate != null) {
			this.maxDateEnd = getCurrentSystemDate();
			// max date initial calendar
			this.maxDateInitial = (Date)courtDate.getValue();
		}
	}
	
	/**
	 * Validation simple filters.
	 */
	public void validationSimpleFilters(){
		ReportFilter initialCalendar = null;
		ReportFilter endCalendar =null;
		ReportFilter participant = null;
		ReportFilter holder = null;
		ReportFilter security = null;
		ReportFilter account = null;
		ReportFilter issuer = null;

		if(Validations.validateIsNotNull(nameValidationMethod)) {
			switch (nameValidationMethod) {
				
				case "validateIssuer":
					issuer = getFilterByName(ReportConstant.ISSUER_PARAM);
					if(issuer != null && issuer != null) {
						issuer.setNameRelationFilters(ReportFilterType.HELP_ISSUERS.name());
					}
					break;
					
				case "validateInitialEndCalendar":
					initialCalendar = getFilterByName(ReportConstant.DATE_INITIAL_PARAM);
					endCalendar = getFilterByName(ReportConstant.DATE_END_PARAM);
					if(initialCalendar != null && endCalendar != null) {
						//min date end calendar
						this.minDateEnd = (Date)initialCalendar.getValue();
						//max date end calendar
						this.maxDateEnd = getCurrentSystemDate();
						// max date initial calendar
						this.maxDateInitial = (Date)endCalendar.getValue();
					}
					ArrayList<String> calendars = new ArrayList<String>();
					calendars.add(parameters.get(ReportConstant.DATE_INITIAL_PARAM).toString());
					calendars.add(parameters.get(ReportConstant.DATE_END_PARAM).toString());
					RequestContext.getCurrentInstance().update(calendars);
					break;
					
				case "validateHolderParticipant":
					participant = getFilterByName(parameters.get(PARTICIPANT).toString());
					holder = getFilterByName(parameters.get(HOLDER).toString());
					if(holder != null && participant != null) {
						participant.setNameRelationFilters(ReportFilterType.HELP_HOLDERS.name());
						holder.setNameRelationFilters(ReportFilterType.HELP_HOLDERS.name());
						if(participant!=null && participant.getValue()!=null){
							this.participant = Long.valueOf(participant.getValue().toString());
						}
						//update Componentes
						String componentsId = (String)parameters.get(UPDATE_COMPONENTS);
						if(StringUtils.isNotBlank(componentsId)){
							List<String> updateFilters =CommonsUtilities.getListOfComponentsForUpdate(componentsId);
							//get id for update helper binding
							for(String filter : updateFilters){
								if(filter.contains(HOLDER)){
									String helperId = StringUtils.substringBefore(filter, HOLDER);
									updateFilters.add(helperId+HOLDERS_HELPER);
									break;
								}
							}
							RequestContext.getCurrentInstance().update(updateFilters);
						} 
					}
				break;
				
				case "validateSecurityParticipant":
					participant = getFilterByName(parameters.get(PARTICIPANT).toString());
					security = getFilterByName(parameters.get(SECURITY).toString());
					if(security != null && participant != null){
						participant.setNameRelationFilters(ReportFilterType.HELP_SECURITIES.name());
						security.setNameRelationFilters(ReportFilterType.HELP_SECURITIES.name());
						//update Componentes
						String componentsId = (String)parameters.get(UPDATE_COMPONENTS);
						if(StringUtils.isNotBlank(componentsId)){
							List<String> updateFilters =CommonsUtilities.getListOfComponentsForUpdate(componentsId);
							//get id for update helper securities binding
							for(String filter : updateFilters){
								if(filter.contains(SECURITY)){
									String helperId = StringUtils.substringBefore(filter, SECURITY);
									updateFilters.add(helperId+SECURITIES_HELPER);
									break;
								}
							}
							RequestContext.getCurrentInstance().update(updateFilters);
						} 
					}
				break;
				
				case "validateAccountParticipant":
					participant = getFilterByName(parameters.get(PARTICIPANT).toString());
					account = getFilterByName(parameters.get(ACCOUNT).toString());
					if(account != null && participant != null){
						participant.setNameRelationFilters(ReportFilterType.HELP_ACCOUNTS.name());
						account.setNameRelationFilters(ReportFilterType.HELP_ACCOUNTS.name());
						//update Componentes
						String componentsId = (String)parameters.get(UPDATE_COMPONENTS);
						if(StringUtils.isNotBlank(componentsId)){
							List<String> updateFilters =CommonsUtilities.getListOfComponentsForUpdate(componentsId);
							//get id for update helper securities binding
							for(String filter : updateFilters){
								if(filter.contains(ACCOUNT)){
									String helperId = StringUtils.substringBefore(filter, ACCOUNT);
									updateFilters.add(helperId+ACCOUNTS_HELPER);
									break;
								}
							}
							RequestContext.getCurrentInstance().update(updateFilters);
						} 
					}
				break;
				
				case "validateAccountNumberCUI":
					holder = getFilterByName(parameters.get(HOLDER).toString());
					account = getFilterByName(parameters.get(ACCOUNT).toString());
					if(account != null && holder != null){
						holder.setNameRelationFilters(ReportFilterType.HELP_ACCOUNTS.name());
						account.setNameRelationFilters(ReportFilterType.HELP_ACCOUNTS.name());
						
						//update Components
						String componentsId = (String)parameters.get(UPDATE_COMPONENTS);
						if(StringUtils.isNotBlank(componentsId)){
							List<String> updateFilters =CommonsUtilities.getListOfComponentsForUpdate(componentsId);
							//get id for update helper securities binding
							for(String filter : updateFilters){
								if(filter.contains(ACCOUNT)){
									String helperId = StringUtils.substringBefore(filter, ACCOUNT);
									updateFilters.add(helperId+ACCOUNTS_HELPER);
									break;
								}
							}
							RequestContext.getCurrentInstance().update(updateFilters);
						} 
					}
				break;

				case "validateOldPartAndHolderAndAccount":
					participant = getFilterByName(parameters.get(PARTICIPANT).toString());
					holder = getFilterByName(parameters.get(HOLDER).toString());
					account = getFilterByName(parameters.get(ACCOUNT).toString());
					if(account!=null && holder!=null && participant!=null){
						participant.setNameRelationFilters(ReportFilterType.HELP_ACCOUNTS.name());
						holder.setNameRelationFilters(ReportFilterType.HELP_ACCOUNTS.name());
						account.setNameRelationFilters(ReportFilterType.HELP_ACCOUNTS.name());
						//update Components
						String componentsId = (String)parameters.get(UPDATE_COMPONENTS);
						if(StringUtils.isNotBlank(componentsId)){
							List<String> updateFilters =CommonsUtilities.getListOfComponentsForUpdate(componentsId);
							//get id for update helper securities binding
							for (int i = 0; i < updateFilters.size(); i++) {
								if(updateFilters.get(i).contains(HOLDER) && updateFilters.get(i+1).contains(ACCOUNT)){
									String helperId = StringUtils.substringBefore(updateFilters.get(i+1), ACCOUNT);
									String helperId2 = StringUtils.substringBefore(updateFilters.get(i), HOLDER);
									updateFilters.add(helperId+ACCOUNTS_HELPER);
									updateFilters.add(helperId2+HOLDERS_HELPER);
									break;
								}
							}
							RequestContext.getCurrentInstance().update(updateFilters);
						} 
					}
				break;
				
				case "validatePartAndHolderAndAccount":
					participant = getFilterByName(Validations.validateIsNotNull(parameters.get(PARTICIPANT))? parameters.get(PARTICIPANT).toString() : null);
					holder = getFilterByName(Validations.validateIsNotNull(parameters.get(HOLDER))? parameters.get(HOLDER).toString() : null);
					account = getFilterByName(Validations.validateIsNotNull(parameters.get(ACCOUNT))? parameters.get(ACCOUNT).toString() : null);
					if(account!=null && holder!=null && participant!=null){
						participant.setNameRelationFilters(ReportFilterType.HELP_ACCOUNTS.name());
						holder.setNameRelationFilters(ReportFilterType.HELP_ACCOUNTS.name());
						account.setNameRelationFilters(ReportFilterType.HELP_ACCOUNTS.name());
						//update Components
						
						List<String> updateFilters = new ArrayList<String>();
						updateFilters.add(participant.getFilterName());
						//get id for update helper securities binding
						for (int i = 0; i < updateFilters.size(); i++) {
							if(updateFilters.get(i).contains(HOLDER) && updateFilters.get(i+1).contains(ACCOUNT)){
								String helperId = StringUtils.substringBefore(updateFilters.get(i+1), ACCOUNT);
								String helperId2 = StringUtils.substringBefore(updateFilters.get(i), HOLDER);
								updateFilters.add(helperId+ACCOUNTS_HELPER);
								updateFilters.add(helperId2+HOLDERS_HELPER);
								break;
							}
						}
						RequestContext.getCurrentInstance().update(updateFilters);
					}
				break;
				
				case "updateTypeSearchBilling":
					
					if(this.filter_search.equals(GeneralConstants.ONE_VALUE_INTEGER)){
						reportId=ReportIdType.CONSOLIDATED_BY_SERVICES_REPORT.getCode();
					}
					else if(this.filter_search.equals(GeneralConstants.TWO_VALUE_INTEGER)){
						reportId=ReportIdType.CONSOLIDATED_BY_ENTITY_REPORT.getCode();
					}
				break;
				
				case "loadTableListBySystem":
					
					changeSelectedSystem();
					
				break;
				
				case "loadColumnListByTable":
					
					changeSelectedTable();
				
				break;
	
			}
		}
	} 
	
	/**
	 * Gets the filter by name.
	 *
	 * @param filterName the filter name
	 * @return the filter by name
	 */
	public ReportFilter getFilterByName(String filterName){
		ReportFilter reportFilter = null;
		if(StringUtils.isNotBlank(filterName)) {
			for (DynaFormControl dynaFormControl : form.getControls()) {
				if(((ReportFilter)dynaFormControl.getData()).getFilterName().equals(filterName)) {
					reportFilter = (ReportFilter)dynaFormControl.getData();
					break;
				}
			}
		} 
		return reportFilter;
	}
	
	/**
	 * On date selecte.
	 *
	 * @param container the container
	 * @param calendar the calendar
	 */
	public void onDateSelected(String container,String calendar) {
		String uiCalendarId = null;
		if(calendar.equals(ReportConstant.DATE_INITIAL_PARAM)) {
			//update end calendar
			uiCalendarId=parameters.get(ReportConstant.DATE_END_PARAM).toString();
			ReportFilter initialCalendar= getFilterByName(ReportConstant.DATE_INITIAL_PARAM);
			if(initialCalendar != null) {
				this.minDateEnd = (Date)initialCalendar.getValue();
			}
		} else {
			//update initial calendar
			uiCalendarId=parameters.get(ReportConstant.DATE_INITIAL_PARAM).toString();
			ReportFilter endCalendar= getFilterByName(ReportConstant.DATE_END_PARAM);
			if(endCalendar != null) {
				this.maxDateInitial = (Date)endCalendar.getValue();
			}
		}
		RequestContext.getCurrentInstance().update(uiCalendarId);
	} 
	
	/**
	 * On update participant.
	 *
	 * @param participant the participant
	 */
	public void onUpdateParticipant(ReportFilter rf, String componentId){
		if(rf.getValue()!=null){
			Long participant = Long.valueOf(rf.getValue().toString());
			this.participant = participant;
		}else{
			this.participant = null;
		}
		ArrayList<String> updates= new ArrayList<String>();
		if(rf.isValidate()){
			//updates.add(componentId+":"+ReportFilterType.HELP_ACCOUNTS.name());
			updates.add(componentId+":"+ACCOUNTS_HELPER);
		}
		RequestContext.getCurrentInstance().update(updates);
	}
	
	public void onUpdateHolder(HolderHelperOutputTO holder){
		if(Validations.validateIsNotNull(holder)){
			this.idHolder = holder.getHolderId();
		}
	}
	
	/**
	 * On update issuer.
	 *
	 * @param issuerName the issuer name
	 */
	public void onUpdateIssuer(String issuerName){
		if(StringUtils.isNotBlank(issuerName)){
			ReportFilter issuer= getFilterByName(issuerName);
			for (DynaFormControl dynaFormControl : form.getControls()) {
				if(((ReportFilter)dynaFormControl.getData()).getFilterType() == 1485) {
					issuer = (ReportFilter)dynaFormControl.getData();
				}
			}
			if(issuer!=null){
				this.issuer = ((IssuerTO)issuer.getValue()).getIssuerCode();
			}
		}
	}
	
	/**
	 * Checks if is validate range dates.
	 *
	 * @return true, if is validate range dates
	 */
	public boolean isValidateRangeDates(){
		return parameters.containsKey(ReportConstant.DATE_INITIAL_PARAM) && parameters.containsKey(ReportConstant.DATE_END_PARAM)
				&& (maxDateEnd != null && maxDateInitial != null && minDateEnd != null);
	}
	
	
	public void searchAccountByCode(ReportFilter reportFilter,String componentId) throws ServiceException{
		HolderAccountHelperResultTO holderAccount = (HolderAccountHelperResultTO)reportFilter.getValue();
		if(holderAccount.getAccountNumber() != null){
			AccountHelperInputTO accountInput = new AccountHelperInputTO();
			if(userInfo.getUserAccountSession().isParticipantInstitucion() || userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
				accountInput.setIdParticipantPK(userInfo.getUserAccountSession().getParticipantCode());
			} else{
				accountInput.setIdParticipantPK(this.participant);
			}
			if(this.idHolder!=null){
				accountInput.setIdHolderPk(this.idHolder);
			}
			accountInput.setUserIssuer(false);
			accountInput.setUserIssuerDpf(false);
			if(userInfo.getUserAccountSession().isIssuerDpfInstitucion() || userInfo.getUserAccountSession().isIssuerInstitucion()){
				List<HolderAccount> lstHolderAccount= null;				
				accountInput.setIssuerFk(userInfo.getUserAccountSession().getIssuerCode());	
				if(userInfo.getUserAccountSession().isIssuerInstitucion()){
					accountInput.setUserIssuer(true);
				}
				if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
					accountInput.setUserIssuerDpf(true);
				}
				accountInput.setAccountNumber(holderAccount.getAccountNumber().intValue());
				Holder holder= new Holder();
				accountInput.setHolder(holder);
				holderAccounts = helperComponentFacade.searchAccountExtensionByFilter(accountInput);
				if(holderAccounts==null || holderAccounts.size()==0){
					JSFUtilities.putViewMap("headerMessageView",PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING));
					JSFUtilities.putViewMap("bodyMessageView",PropertiesUtilities.getGenericMessage(GeneralConstants.HELPER_ACCOUNT_NOT_FOUND));
					holderAccount.setAccountNumber(null);
					holderAccount.setHolderDescriptionList(new ArrayList<String>());					
					JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationReportsWidget').show();");
				}else{
					lstHolderAccount= new ArrayList<HolderAccount>();
					lstHolderAccount = fillHolderAccount(holderAccounts);
					for(HolderAccount objHolderAccount : lstHolderAccount){
						objHolderAccount.setSelected(true);
					}
					reportFilter.setHolderAccounts(lstHolderAccount);
				}
			}else{ 
				accountInput.setAccountNumber(holderAccount.getAccountNumber().intValue());
				List<HolderAccount> lstHolderAccount= null;
				if(this.participant ==null){
					//TODO WHEN VALIDATIONS WILL BE REQUIRED
//					RequestContext.getCurrentInstance().execute("cnfwMsgCustomValidationReportsWidget.show()");
//					JSFUtilities.putViewMap("headerMessageView",PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING));
//					JSFUtilities.putViewMap("bodyMessageView",PropertiesUtilities.getGenericMessage(GeneralConstants.HELPER_ACCOUNT_PARTICIPANT_REQUIRED));
//					holderAccount.setAccountPk(null);
//					holderAccount.setAccountNumber(null);
//					holderAccount.setAlternateCode(null);
//					holderAccount.setHolderDescriptionList(new ArrayList<String>());

//					if (Validations.validateIsNotNull(holderAccount.getHolderAccount().getHolder().getIdHolderPk())) {
//						accountInput.setAccountNumber(holderAccount.getAccountNumber());
//						holder.setIdHolderPk(holderAccount.getHolderAccount().getHolder().getIdHolderPk());
						Holder holder= new Holder();
						accountInput.setHolder(holder);
						// Searching holder accounts by participant
						holderAccounts = helperComponentFacade.searchAccountExtensionByFilter(accountInput);
						//setting list holderAccounts from reportFilter to show the grill
						lstHolderAccount= new ArrayList<HolderAccount>();
						lstHolderAccount = fillHolderAccount(holderAccounts);
						reportFilter.setHolderAccounts(lstHolderAccount);
						if(holderAccounts != null  && holderAccounts.size() == 0){
							if(this.idHolder!=null){
								JSFUtilities.putViewMap("headerMessageView",PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING));
								JSFUtilities.putViewMap("bodyMessageView",PropertiesUtilities.getGenericMessage(GeneralConstants.HELPER_ACCOUNT_NOT_RELATED_WITH_HOLDER));
							}else{
								JSFUtilities.putViewMap("headerMessageView",PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING));
								JSFUtilities.putViewMap("bodyMessageView",PropertiesUtilities.getGenericMessage(GeneralConstants.HELPER_ACCOUNT_NOT_FOUND));
							}
							holderAccount.setAccountNumber(null);
							holderAccount.setHolderDescriptionList(new ArrayList<String>());
							
							JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationReportsWidget').show();");
						}
						//TODO WHEN VALIDATIONS WILL BE REQUIRED									
//						 else{
//							RequestContext.getCurrentInstance().execute("cnfwMsgCustomValidationReportsWidget.show()");
//							JSFUtilities.putViewMap("headerMessageView",PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING));
//							JSFUtilities.putViewMap("bodyMessageView",PropertiesUtilities.getGenericMessage(GeneralConstants.HELPER_ACCOUNT_HOLDER_REQUIRED));
//							holderAccount.setAccountPk(null);
//							holderAccount.setAccountNumber(null);
//							holderAccount.setHolderDescriptionList(new ArrayList<String>());
//					}
				} else {
					List<HolderAccountHelperResultTO> resultList = helperComponentFacade.searchAccountExtensionByFilter(accountInput);
					if(!resultList.isEmpty()){
						lstHolderAccount= new ArrayList<HolderAccount>();
						lstHolderAccount = fillHolderAccount(resultList);
						reportFilter.setHolderAccounts(lstHolderAccount);
//						reportFilter.setValue(resultList.get(0));
					} else {
						RequestContext.getCurrentInstance().execute("PF('cnfwMsgCustomValidationReportsWidget').show()");
						JSFUtilities.putViewMap("headerMessageView",PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING));
						JSFUtilities.putViewMap("bodyMessageView",PropertiesUtilities.getGenericMessage(GeneralConstants.HELPER_ACCOUNT_NOT_FOUND));
						JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationReportsWidget').show();");
						holderAccount.setAccountPk(null);
						holderAccount.setAccountNumber(null);
						holderAccount.setAlternateCode(null);
//						holderAccount.setHolderDescription(holderDescription);
						holderAccount.setHolderDescriptionList(new ArrayList<String>());
						reportFilter.setHolderAccounts(null);
					}
				}
			}	
		} else {
			holderAccount.setAccountPk(null);
			holderAccount.setAccountNumber(null);
			holderAccount.setHolderDescriptionList(new ArrayList<String>());
			reportFilter.setHolderAccounts(new ArrayList<HolderAccount>());
		}
		ArrayList<String> updates = new ArrayList<String>();
		updates.add(componentId+":accounts");
//		updates.add(componentId+":accountsDescription");
//		updates.add(componentId+":accountsAlternative");
		updates.add(componentId+":accountResultList");
		updates.add("frmReportForm:cnfMsgCustomValidationReports");
		RequestContext.getCurrentInstance().update(updates);
	}
	
	public List<HolderAccount> fillHolderAccount(List<HolderAccountHelperResultTO> accountsList){
		List<HolderAccount> lstHolderAccount= new ArrayList<HolderAccount>();
		for (HolderAccountHelperResultTO haHelper : accountsList) {
			HolderAccount ha = new HolderAccount();
			Participant p = new Participant();
			ha.setAccountNumber(haHelper.getAccountNumber().intValue());
			ha.setAlternateCode(haHelper.getAlternateCode());
			ha.setStateAccountDescription(haHelper.getAccountStatusDescription());
			p.setDisplayCodeMnemonic(haHelper.getParticipant().getDisplayCodeMnemonic());
			p.setDescription(haHelper.getParticipant().getDescription());
			ha.setParticipant(p);
			ha.setStateAccount(haHelper.getHolderAccount().getStateAccount());
			ha.setIdHolderAccountPk(haHelper.getAccountPk());
			lstHolderAccount.add(ha);
		}
		return lstHolderAccount;
	}
	/**
	 * Search security by code.
	 *
	 * @param reportFilter the report filter
	 * @param componentId the component id
	 * @throws ServiceException the service exception
	 */
	public void searchSecurityByCode(ReportFilter reportFilter,String componentId) throws ServiceException{
		SecuritiesTO securities = (SecuritiesTO)reportFilter.getValue();
		if(StringUtils.isNotBlank(securities.getSecuritiesCode())){
			SecuritiesSearcherTO  searcher = new SecuritiesSearcherTO();
			searcher.setIsinCode(securities.getSecuritiesCode());
			if(userInfo.getUserAccountSession().isParticipantInstitucion() || userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
				searcher.setParticipantCode(userInfo.getUserAccountSession().getParticipantCode());
			}
			if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
				List<Integer> listClassSecurity = new ArrayList<>();
				listClassSecurity.add(SecurityClassType.DPA.getCode());
				listClassSecurity.add(SecurityClassType.DPF.getCode());
				searcher.setListClassSecuritie(listClassSecurity);
			}
			searcher.setIssuerCode(issuer);
			List<SecuritiesTO>  securityList = helperComponentFacade.findSecuritiesFromHelperNativeQuery(searcher);
			if(!securityList.isEmpty()){
				reportFilter.setValue(securityList.get(0));
			} else {
				RequestContext.getCurrentInstance().execute("PF('cnfwMsgCustomValidationReportsWidget').show()");
				JSFUtilities.putViewMap("headerMessageView",PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING));
				JSFUtilities.putViewMap("bodyMessageView",PropertiesUtilities.getGenericMessage(GeneralConstants.HELPER_SECURITY_NOT_FOUND));
				securities.setSecuritiesCode(null);
				securities.setDescription(null);
			}
		} else {
			securities.setSecuritiesCode(null);
			securities.setDescription(null);
		}
		ArrayList<String> updates = new ArrayList<String>();
		updates.add(componentId+":securities");
		updates.add(componentId+":securitiesDescription");
		updates.add("frmReportForm:cnfMsgCustomValidationReports");
		RequestContext.getCurrentInstance().update(updates);
	}
	
	/**
	 * Search holder by code.
	 *
	 * @param reportFilter the report filter
	 * @param componentId the component id
	 * @throws ServiceException the service exception
	 */
	public void searchHolderByCode(ReportFilter reportFilter, String componentId) throws ServiceException{
		HolderHelperOutputTO holder = (HolderHelperOutputTO)reportFilter.getValue();
		if(holder.getHolderId() != null){
			HolderTO holdSearcher = new HolderTO();
			holdSearcher.setHolderId(holder.getHolderId());
			holdSearcher.setUserIssuer(false);
			holdSearcher.setUserIssuerDpf(false);
			if(userInfo.getUserAccountSession().isParticipantInstitucion() || userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
				holdSearcher.setPartIdPk(userInfo.getUserAccountSession().getParticipantCode());
			}else if(userInfo.getUserAccountSession().isIssuerInstitucion() || userInfo.getUserAccountSession().isIssuerDpfInstitucion() ){
				holdSearcher.setIssuerFk(userInfo.getUserAccountSession().getIssuerCode());
				if(userInfo.getUserAccountSession().isIssuerInstitucion()){
					holdSearcher.setUserIssuer(true);
				}					
				if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
					holdSearcher.setUserIssuerDpf(true);
				}					
			} else if (Validations.validateIsNotNull(participant)) {
				holdSearcher.setPartIdPk(participant);
			}
			List<HolderHelperOutputTO>  holders = helperComponentFacade.searchHolderExtension(holdSearcher);
			if(!holders.isEmpty()) {
				reportFilter.setValue(holders.get(0));
				this.idHolder = holder.getHolderId();				 
			} else {
				Holder isHolder = new Holder();
				//busca holder por id
				isHolder = helperComponentFacade.findHolderByCode(holder.getHolderId());
				if(Validations.validateIsNotNullAndNotEmpty(isHolder)) {
					if (userInfo.getUserAccountSession().isIssuerInstitucion() || userInfo.getUserAccountSession().isIssuerDpfInstitucion() ) {
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getGenericMessage(GeneralConstants.REPORT_HELPER_HOLDER_NOTEXIST_ISSUER, this.idHolder));
						JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationReportsWidget').show();");	
					} else {
						Object[] argObj = new Object[1];
						argObj[0] = holder.getHolderId();
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getGenericMessage(GeneralConstants.REPORT_HELPER_HOLDER_NOTEXIST, argObj));
						JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationReportsWidget').show();");			
					}
				} else {
					Object[] argObj = new Object[1];
					argObj[0] = holder.getHolderId();					
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getGenericMessage(GeneralConstants.REPORT_HELPER_HOLDER_NOTEXIST, argObj));
					JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationReportsWidget').show();");					
				}
				holder.setHolderId(null);
				holder.setFullName(null);
			}
		} else {
			holder.setHolderId(null);
			holder.setFullName(null);
		}
		ArrayList<String> updates = new ArrayList<String>();
		updates.add(componentId+":holders");
		updates.add(componentId+":holdersDescription");
		if(reportFilter.isValidate()){
			updates.add(componentId+":"+ReportFilterType.HELP_ACCOUNTS.name());
		}
		updates.add("frmReportForm:cnfMsgCustomValidationReports");
		RequestContext.getCurrentInstance().update(updates);
	}
	
	/**
	 * Search issuerfor code.
	 *
	 * @param reportFilter the report filter
	 * @param componentId the component id
	 * @throws ServiceException the service exception
	 */
	public void searchIssuerforCode(ReportFilter reportFilter,String componentId) throws ServiceException{
		this.issuer=null;
		IssuerTO issuer = (IssuerTO)reportFilter.getValue();
		if(StringUtils.isNotBlank(issuer.getMnemonic())){
			issuerSearcher.setMnemonic(issuer.getMnemonic().trim());
			IssuerTO result = helperComponentFacade.findIssuerByCode(issuerSearcher);
			if(result != null) {
				if(IssuerStateType.PENDING.getCode().equals(result.getIssuerState())){
					RequestContext.getCurrentInstance().execute("PF('cnfwMsgCustomValidationReportsWidget').show()");
					JSFUtilities.putViewMap("bodyMessageView",PropertiesUtilities.getGenericMessage(GeneralConstants.HELPER_ISSUER_STATE_PENDING));
					JSFUtilities.putViewMap("headerMessageView",PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT));
					issuer.setIssuerDesc(null);
					issuer.setMnemonic(null);
					issuer.setIssuerBusinessName(null);
				}else {
					reportFilter.setValue(result);
					//save the issuer pk to send to security's help
					this.issuer=result.getIssuerCode();
				}	
			} else {
				RequestContext.getCurrentInstance().execute("PF('cnfwMsgCustomValidationReportsWidget').show()");
				JSFUtilities.putViewMap("bodyMessageView",PropertiesUtilities.getGenericMessage(GeneralConstants.HELPER_ISSUER_NOT_FOUND));
				JSFUtilities.putViewMap("headerMessageView",PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING));
				issuer.setIssuerDesc(null);
				issuer.setMnemonic(null);
				issuer.setIssuerBusinessName(null);
			}
		} else {
			issuer.setIssuerDesc(null);
			issuer.setIssuerCode(null);
			issuer.setIssuerBusinessName(null);
		}
		ArrayList<String> updates = new ArrayList<String>();
		updates.add(componentId+":issuers");
		updates.add(componentId+":issuersDescription");
		updates.add(componentId+":securitiesHelper");
		updates.add("frmReportForm:cnfMsgCustomValidationReports");
		RequestContext.getCurrentInstance().update(updates);
	}
	
	/**
	 * Search BillingService by code.
	 *
	 * @param reportFilter the report filter
	 * @param componentId the component id
	 * @throws ServiceException the service exception
	 */
	 public void searchBillingServiceByCode(ReportFilter reportFilter,String componentId) throws ServiceException{
        BillingServiceTo billingService = (BillingServiceTo)reportFilter.getValue();
        //modificacion merge 643
        Long participantID = null;
        if(!userInfo.getUserAccountSession().isIssuerInstitucion()){
              participantID = userInfo.getUserAccountSession().getParticipantCode();
        }
        if(!Validations.validateIsNotNullAndPositive(participantID )&&!Validations.validateIsNotNullAndNotEmpty(participantID)){               
              participantID = GeneralConstants.PARTICIPANT_EDB_CODE;
        }     
        //--    
        if(StringUtils.isNotBlank(billingService.getServiceCode())){
              String  searcher=billingService.getServiceCode();
              //modificacion merge 643
              BillingServiceTo billingResult = helperComponentFacade.getBillingService(searcher,participantID);
              //--
              if(Validations.validateIsNotNullAndNotEmpty(billingResult)){
                    reportFilter.setValue(billingResult);
              } else {
                    RequestContext.getCurrentInstance().execute("PF('cnfwMsgCustomValidationReportsWidget').show()");
                    JSFUtilities.putViewMap("headerMessageView",PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING));
                    JSFUtilities.putViewMap("bodyMessageView",PropertiesUtilities.getGenericMessage(GeneralConstants.HELPER_BILLING_NOT_FOUND));
                    billingService.setServiceCode(null);
                    billingService.setServiceName(null);
              }
        } else {
              billingService.setServiceCode(null);
              billingService.setServiceName(null);
        }
        ArrayList<String> updates = new ArrayList<String>();
        updates.add(componentId+":billingService");
        updates.add(componentId+":billingServiceDescription");
        updates.add("frmReportForm:cnfMsgCustomValidationReports");
        RequestContext.getCurrentInstance().update(updates);
  }
	 
	 /**
	 * Excecute aditional validations
	 *
	 * @param reportFilter the report filter
	 * @param componentId the component id
	 * @throws ServiceException the service exception
	 */
	public void aditionalValidatios(ReportFilter reportFilter) throws ServiceException{
		
		if(reportId.equals(ReportIdType.CLIENT_PORTAFOLIO.getCode()) || reportId.equals(ReportIdType.CLIENT_PORTAFOLIO_POI.getCode())
				|| reportId.equals(ReportIdType.REPORT_DEMATERIALIZED_TOTAL_PORTFOLIO_CLIENTS.getCode()) ){
			if(reportFilter.getFilterName().equalsIgnoreCase("portafolio_type") && reportFilter.getValue()!=null){
				boolean disabled = false;
				if(reportFilter.getValue().toString().equalsIgnoreCase(IssuanceType.PHYSICAL.getCode().toString())){
					disabled = true;
				}
				if(reportFilter.getValue().toString().equalsIgnoreCase(IssuanceType.DEMATERIALIZED.getCode().toString())){
					disabled = false;
				}
				for (DynaFormControl dynaFormControl : form.getControls()) {
					if (dynaFormControl.getData() instanceof ReportFilter) {
						ReportFilter reportFilterAux = (ReportFilter)dynaFormControl.getData();
						if(reportFilterAux.getFilterName().equals("suspended_securities")) {
							reportFilterAux.setValue(null);
							reportFilterAux.setDisabled(disabled);
							break;
						}
					}
				}
			}
		}
	}
	
	private void changeSelectedSystem(){
		
		try {
			
			String strSystemIdPrm = parameters.get(SYSTEM).toString();
			String idCboTable = parameters.get(TABLE).toString();
			String idCboColumn = parameters.get(COLUMN).toString();
			
			Integer idSystemPk = null;
			if(Validations.validateIsNotNullAndNotEmpty(strSystemIdPrm)){
				idSystemPk = Integer.valueOf(strSystemIdPrm);
			}
			
			ReportFilter filterTable = getFilterByName(TABLE);
			ReportFilter filterColumn = getFilterByName(COLUMN);

			if(Validations.validateIsNotNullAndPositive(idSystemPk)){
				//find schema Name by Id
				String schemaDB = reportMgmtFacade.getSystemSchemaById(idSystemPk);
				reportMgmtFacade.fillSelectMenuFilterWithTables(schemaDB, filterTable);
			} else {
				filterTable.setSelectMenu(new LinkedHashMap<String,Object>());
			}
			filterTable.setValue(null);
			
			//remove items from filter column
			filterColumn.setSelectMenu(new LinkedHashMap<String,Object>());
			filterColumn.setValue(null);
		
			List<String> updateFilters = new ArrayList<String>();
			updateFilters.add(idCboTable);
			updateFilters.add(idCboColumn);
			RequestContext.getCurrentInstance().update(updateFilters);
			
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	private void changeSelectedTable(){
		
		try {
			
			String strSystemIdPrm = parameters.get(SYSTEM).toString();
			String selectedTable = parameters.get(TABLE).toString();
			String idCboColumn = parameters.get(COLUMN).toString();
			
			Integer idSystemPk = null;
			if(Validations.validateIsNotNullAndNotEmpty(strSystemIdPrm)){
				idSystemPk = Integer.valueOf(strSystemIdPrm);
			}
			
			ReportFilter filterColumn = getFilterByName(COLUMN);

			if(Validations.validateIsNotNullAndPositive(idSystemPk)){
				//find schema Name by Id
				String schemaDB = reportMgmtFacade.getSystemSchemaById(idSystemPk);
				reportMgmtFacade.fillSelectMenuFilterWithColumns(schemaDB, selectedTable, filterColumn);
			} else {
				filterColumn.setSelectMenu(new LinkedHashMap<String,Object>());
			}
			filterColumn.setValue(null);
			
			List<String> updateFilters = new ArrayList<String>();
			updateFilters.add(idCboColumn);
			RequestContext.getCurrentInstance().update(updateFilters);
			
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	
	public void validateSelectionAccount(HolderAccount ha, ReportFilter data, String componentId){
		if(ha.getSelected()==true){
			for (int i = 0; i < data.getHolderAccounts().size(); i++) {	
				if(lastPos>=0 && i==0){
					HolderAccount haLast = data.getHolderAccounts().get(lastPos);
					haLast.setSelected(false);
					data.getHolderAccounts().set(lastPos, haLast);
				}
				HolderAccount holderAccount = data.getHolderAccounts().get(i);
				if(!holderAccount.getSelected().equals(ha.getSelected())){
					holderAccount.setSelected(false);
					data.getHolderAccounts().set(i, holderAccount);
				}else{
					lastPos=i;
				}
			}
		}
		
		ArrayList<String> updates = new ArrayList<String>();
		updates.add(componentId+":accountResultList");
		RequestContext.getCurrentInstance().update(updates);
	}
	
	public String eventCombo(String val){
		if(reportId == 200){ //reporte de resumen de comisiones
			referenceRate = val;
		}
		return null;
	}

	/**
	 * Gets the form.
	 *
	 * @return the form
	 */
	public DynaFormModel getForm() {
		return form;
	}

	/**
	 * Sets the form.
	 *
	 * @param form the new form
	 */
	public void setForm(DynaFormModel form) {
		this.form = form;
	}

	/**
	 * Gets the report id.
	 *
	 * @return the report id
	 */
	public Long getReportId() {
		return reportId;
	}

	/**
	 * Sets the report id.
	 *
	 * @param reporteId the new report id
	 */
	public void setReportId(Long reporteId) {
		if(reporteId != null) {
			this.reportId = reporteId;
		}
	}

	/**
	 * Instantiates a new report dynamic filter bean.
	 */
	public ReportDynamicFilterBean() {
		setParameters(new HashMap<String, Object>());
	}




	/**
	 * Gets the report.
	 *
	 * @return the report
	 */
	public Report getReport() {
		return report;
	}




	/**
	 * Sets the report.
	 *
	 * @param report the new report
	 */
	public void setReport(Report report) {
		this.report = report;
	}


	/**
	 * Gets the parameters.
	 *
	 * @return the parameters
	 */
	public Map<String,Object> getParameters() {
		return parameters;
	}


	/**
	 * Sets the parameters.
	 *
	 * @param parameters the parameters
	 */
	public void setParameters(Map<String,Object> parameters) {
		this.parameters = parameters;
	}


	/**
	 * Gets the name validation method.
	 *
	 * @return the name validation method
	 */
	public String getNameValidationMethod() {
		return nameValidationMethod;
	}


	/**
	 * Sets the name validation method.
	 *
	 * @param nameValidationMethod the new name validation method
	 */
	public void setNameValidationMethod(String nameValidationMethod) {
		this.nameValidationMethod = nameValidationMethod;
	}

	/**
	 * Gets the max date initial.
	 *
	 * @return the max date initial
	 */
	public Date getMaxDateInitial() {
		return maxDateInitial;
	}

	/**
	 * Sets the max date initial.
	 *
	 * @param maxDateInitial the new max date initial
	 */
	public void setMaxDateInitial(Date maxDateInitial) {
		this.maxDateInitial = maxDateInitial;
	}

	/**
	 * Gets the min date end.
	 *
	 * @return the min date end
	 */
	public Date getMinDateEnd() {
		return minDateEnd;
	}

	/**
	 * Sets the min date end.
	 *
	 * @param minDateEnd the new min date end
	 */
	public void setMinDateEnd(Date minDateEnd) {
		this.minDateEnd = minDateEnd;
	}

	/**
	 * Gets the max date end.
	 *
	 * @return the max date end
	 */
	public Date getMaxDateEnd() {
		return maxDateEnd;
	}

	/**
	 * Sets the max date end.
	 *
	 * @param maxDateEnd the new max date end
	 */
	public void setMaxDateEnd(Date maxDateEnd) {
		this.maxDateEnd = maxDateEnd;
	}

	/**
	 * Gets the filter selected.
	 *
	 * @return the filter selected
	 */
	public ReportFilter getFilterSelected() {
		return filterSelected;
	}

	/**
	 * Sets the filter selected.
	 *
	 * @param filterSelected the new filter selected
	 */
	public void setFilterSelected(ReportFilter filterSelected) {
		this.filterSelected = filterSelected;
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Long getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Long participant) {
		this.participant = participant;
	}

	public String getIssuer() {
		return issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}


	public ReportUser getReportUser() {
		return reportUser;
	}

	public void setReportUser(ReportUser reportUser) {
		this.reportUser = reportUser;
	}

	public Long getIdHolder() {
		return idHolder;
	}

	public void setIdHolder(Long idHolder) {
		this.idHolder = idHolder;
	}

	public Integer getClosedStateType() {
		return closedStateType;
	}

	public void setClosedStateType(Integer closedStateType) {
		this.closedStateType = closedStateType;
	}

	public HolderAccount getHolderAccountTO() {
		return holderAccountTO;
	}

	public void setHolderAccountTO(HolderAccount holderAccountTO) {
		this.holderAccountTO = holderAccountTO;
	}

	public boolean isShowElement() {
		return showElement;
	}

	public void setShowElement(boolean showElement) {
		this.showElement = showElement;
	}

	/**
	 * @return the referenceRate
	 */
	public String getReferenceRate() {
		return referenceRate;
	}

	/**
	 * @param referenceRate the referenceRate to set
	 */
	public void setReferenceRate(String referenceRate) {
		this.referenceRate = referenceRate;
	}

	/**
	 * @return the serviceCode
	 */
	public String getServiceCode() {
		return serviceCode;
	}

	/**
	 * @param serviceCode the serviceCode to set
	 */
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	
}
