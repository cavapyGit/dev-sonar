package com.pradera.report.management.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.helperui.service.IssuerQueryServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.report.monitorining.to.ReportLoggerTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ReportMgmtServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 22/09/2015
 */
@Stateless
public class ReportMgmtServiceBean extends CrudDaoServiceBean {

	/** The participant service bean. */
	@EJB
	ParticipantServiceBean participantServiceBean;
	
	/** The issuer service bean. */
	@EJB
	IssuerQueryServiceBean issuerServiceBean;
	
	/** The PARTICIPANT RELATED ISSUER GROUP. */
	/** The Constant ID_EXPIRATION_SECURITIES_BY_ISSUER_REPORT. */
	private static final Long ID_EXPIRATION_SECURITIES_BY_ISSUER_REPORT = 64L;
	/** The Constant ID_HOLDER_RELATED_DATA__REPORT. */
	private static final Long ID_HOLDER_RELATED_DATA__REPORT = 28L;
	/** The Constant ID_BALANCE_TRANSFER_AVAILABLE_REPORT. */
	private static final Long ID_BALANCE_TRANSFER_AVAILABLE_REPORT = 123L;
	/** The Constant ID_REQUESTS_ACCRE_CERTIF_HOLDER_REPORT. */
	private static final Long ID_REQUESTS_ACCRE_CERTIF_HOLDER_REPORT = 40L;
	/** The Constant ID_ENTRIES_VOLUNTARY_ACCOUNT_ISSUER_REPORT. */
	private static final Long ID_ENTRIES_VOLUNTARY_ACCOUNT_ISSUER_REPORT = 15L;
	/** The Constant ID_ENTRIES_VOLUNTARY_ACCOUNT_PARTICIPANT_REPORT. */
	private static final Long ID_ENTRIES_VOLUNTARY_ACCOUNT_PARTICIPANT_REPORT = 16L;
	/** The Constant ID_RENOVATIONS_REPORT. */
	private static final Long ID_RENOVATIONS_REPORT = 175L;
	/** The Constant ID_REQUEST_OTC_REPORT. */
	private static final Long ID_REQUEST_OTC__REPORT = 138L;
	
	/** The Constant ID_CLIENT_PORTFOLIO_REPORT. */
	private static final Long ID_CLIENT_PORTFOLIO_REPORT = 186L;
	
	/**
	 * Find report loggers.
	 *
	 * @param reportLoggerFilter the report logger filter
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> findReportLoggers(ReportLoggerTO reportLoggerFilter) {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select Distinct(rl.idReportLoggerPk), "); //0
		sbQuery.append("  r.idReportPk, "); //1
		sbQuery.append("  r.mnemonico, "); //2
		sbQuery.append("  r.name, "); //3
		sbQuery.append("  rl.indError, "); //4
		sbQuery.append("  rl.registryDate, "); //5
		sbQuery.append("  rl.endDate, "); //6
		sbQuery.append("  rl.reportState, "); //7
		sbQuery.append("  (select p.parameterName from ParameterTable p where p.parameterTablePk  = rl.reportState ) as state, "); //8
		sbQuery.append("  rl.errorDetail, "); //9
		sbQuery.append("  interface.description, "); //10
		sbQuery.append("  (select sum(rfl.dataReportLength) from ReportLoggerFile rfl where rfl.reportLogger.idReportLoggerPk=rl.idReportLoggerPk ) , "); //11
		sbQuery.append("  rl.reportFormat "); // 12
		sbQuery.append("  from ReportLogger rl inner join rl.report r inner join rl.reportLoggerUsers user  ");
		sbQuery.append("  					   left join rl.externalInterfaceSend send");
		sbQuery.append("  					   left join send.externalInterface  interface ");
		sbQuery.append("  where ");
		sbQuery.append("  rl.registryDate >= :initialDate and ");
		sbQuery.append("  rl.registryDate < :finalDate ").
		append(" and user.accessUser = :user ");
		if(Validations.validateIsNotNullAndNotEmpty(reportLoggerFilter.getDescription())){
			sbQuery.append("  and r.name like :description ");
			if(reportLoggerFilter.getDescription().indexOf(GeneralConstants.ASTERISK_STRING) != -1) {
				parameters.put("description", reportLoggerFilter.getDescription().replace(GeneralConstants.ASTERISK_CHAR, 
						GeneralConstants.PERCENTAGE_CHAR));
			} else {
				parameters.put("description", reportLoggerFilter.getDescription());
			}
		}
		if(Validations.validateIsNotNullAndNotEmpty(reportLoggerFilter.getMnemonic())){
			sbQuery.append("  and r.mnemonico like :mnemonic ");
			if(reportLoggerFilter.getMnemonic().indexOf(GeneralConstants.ASTERISK_STRING) != -1) {
				parameters.put("mnemonic", reportLoggerFilter.getMnemonic().replace(GeneralConstants.ASTERISK_CHAR, 
						GeneralConstants.PERCENTAGE_CHAR));
			} else {
				parameters.put("mnemonic", reportLoggerFilter.getMnemonic());
			}
		}
		if(reportLoggerFilter.getModuleProcess()!=null){
			sbQuery.append("  and r.idepositaryModule.businessProcess.idBusinessProcessPk = :module ");
			parameters.put("module", reportLoggerFilter.getModuleProcess());
		}
		if(reportLoggerFilter.getState()!=null){
			sbQuery.append("  and rl.reportState = :status ");
			parameters.put("status", reportLoggerFilter.getState());
		}
		sbQuery.append("  order by rl.registryDate desc ");
		parameters.put("initialDate", reportLoggerFilter.getInitialDate());
		parameters.put("finalDate", CommonsUtilities.addDaysToDate(reportLoggerFilter.getFinalDate(), Integer.valueOf(1)) );
		parameters.put("user", reportLoggerFilter.getAccessUser());
		return findListByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Checks if is report in part issuer group.
	 *
	 * @param reportId the report id
	 * @return the boolean
	 */
	public Boolean isReportInPartIssuerGroup(Long reportId){
		Boolean isInGroup = Boolean.FALSE;
		if(reportId.equals(ID_EXPIRATION_SECURITIES_BY_ISSUER_REPORT)
				|| reportId.equals(ID_BALANCE_TRANSFER_AVAILABLE_REPORT)
				|| reportId.equals(ID_HOLDER_RELATED_DATA__REPORT)
				|| reportId.equals(ID_REQUEST_OTC__REPORT)
				|| reportId.equals(ID_REQUESTS_ACCRE_CERTIF_HOLDER_REPORT)
				|| reportId.equals(ID_ENTRIES_VOLUNTARY_ACCOUNT_ISSUER_REPORT)
				|| reportId.equals(ID_ENTRIES_VOLUNTARY_ACCOUNT_PARTICIPANT_REPORT)
				|| reportId.equals(ID_RENOVATIONS_REPORT)){
			isInGroup = Boolean.TRUE;
		}
		return isInGroup;
	}
	
	/**
	 * Checks if is report in part issuer group.
	 *
	 * @param reportId the report id
	 * @return the boolean
	 */
	public Boolean isClientPortFolioReport(Long reportId){
		Boolean isInGroup = Boolean.FALSE;
		if(reportId.equals(ID_CLIENT_PORTFOLIO_REPORT)){
			isInGroup = Boolean.TRUE;
		}
		return isInGroup;
	}
	
	/**
	 * Checks if is report for bcb.
	 *
	 * @param reportId the report id
	 * @return true, if is report for bcb
	 */
	public boolean isReportForBCB(Long reportId) {
		if(reportId.equals(ReportIdType.NET_SETTLEMENT_POSITIONS_PRE.getCode())
				|| reportId.equals(ReportIdType.NET_POSITIONS_WITHDRAWALS.getCode()) ){
			return true;
		}
		return false;
	}
	
	public boolean isReportsSettlements(Long reportId) {
		if(ReportIdType.DETAIL_EXPIRATION_REPURCHASE.equals(reportId)
				|| ReportIdType.DETAIL_EXPIRATION_REPURCHASE.equals(reportId)
				|| ReportIdType.DETAIL_EXPIRATION_REPURCHASE.equals(reportId)
				|| ReportIdType.DETAIL_EXPIRATION_REPURCHASE.equals(reportId)
				|| ReportIdType.DETAIL_EXPIRATION_REPURCHASE.equals(reportId)
				|| ReportIdType.DETAIL_EXPIRATION_REPURCHASE.equals(reportId)){
			
		}
		return false;
	}
	
	public boolean isReportTransferValPurchase(Long reportId) {
		if(reportId.equals(ReportIdType.TRANSFER_VAL_REPURCHASE_BCB.getCode())){
			return true;
		}
		return false;
	}
	
}
