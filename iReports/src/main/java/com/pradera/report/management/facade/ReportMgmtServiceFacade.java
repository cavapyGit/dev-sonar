package com.pradera.report.management.facade;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.extensions.model.dynaform.DynaFormControl;
import org.primefaces.extensions.model.dynaform.DynaFormLabel;
import org.primefaces.extensions.model.dynaform.DynaFormModel;
import org.primefaces.extensions.model.dynaform.DynaFormRow;
import org.primefaces.model.UploadedFile;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.report.ReportCodeType;
import com.pradera.commons.security.model.type.ExternalInterfaceType;
import com.pradera.commons.security.model.type.LanguageType;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.FileData;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.ftp.UploadFtp;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.service.IssuerQueryServiceBean;
import com.pradera.core.component.helperui.to.BillingServiceTo;
import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.core.component.helperui.to.HolderHelperOutputTO;
import com.pradera.core.component.helperui.to.IssuerSearcherTO;
import com.pradera.core.component.helperui.to.IssuerTO;
import com.pradera.core.component.helperui.to.SecuritiesTO;
import com.pradera.core.component.swift.to.FTPParameters;
import com.pradera.core.framework.usersaccount.service.UserAccountControlServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.type.ProcessStateType;
import com.pradera.model.report.Report;
import com.pradera.model.report.ReportFilter;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.model.report.ReportLoggerFile;
import com.pradera.model.report.type.ReportFilterType;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.model.settlement.type.SettlementScheduleType;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.account.service.AccountReportServiceBean;
import com.pradera.report.generation.executor.security.service.AuditConsolidatedServiceBean;
import com.pradera.report.management.service.DynamicFieldsServiceBean;
import com.pradera.report.management.service.ReportMgmtServiceBean;
import com.pradera.report.monitorining.to.ReportLoggerTO;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ReportMgmtServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/08/2013
 */
@Stateless
@Interceptors(ContextHolderInterceptor.class)
public class ReportMgmtServiceFacade implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4617756740227946764L;
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	@Inject
	UserInfo userInfo;
	
	/** The dao service. */
	@EJB
	CrudDaoServiceBean daoService;
	
	/** The dynamic fields service. */
	@EJB
	DynamicFieldsServiceBean dynamicFieldsService;
	
	/** The report mgmt service bean. */
	@EJB
	ReportMgmtServiceBean reportMgmtServiceBean;
	
	/** The user account service. */
	@EJB
	UserAccountControlServiceBean userAccountService;
	
	/** The issuer query service. */
	@EJB
	IssuerQueryServiceBean  issuerQueryService;
	
	/** The audit consolidated report. */
	@EJB
	private AuditConsolidatedServiceBean auditConsolidatedReport;
	
	/** The parameters service facade. */
	@EJB
	GeneralParametersFacade parametersServiceFacade;
	
	/** The account report service. */
	@EJB
	private AccountReportServiceBean accountReportService;
	
	/** The uploaded file. */
	private UploadedFile uploadedFile;

	/** The report id. */
	private Long reportId;
	
    /**
     * Default constructor. 
     */
    public ReportMgmtServiceFacade() {
        // TODO Auto-generated constructor stub
    }
    
    /** issue 1097: metodo para eliminar los filtros 1957,1963,1964,1965 cuando se trate de un perfil CONTABCB */
    private List<ReportFilter> noRedenderedFilterToContaBCB(List<ReportFilter> filters){
    	List<ReportFilter> lst=new ArrayList<>();
    	for(ReportFilter rf:filters)
    		if(rf.getIdReportFilterPk() != 1957L
    				&& rf.getIdReportFilterPk() != 1963L
    				&& rf.getIdReportFilterPk() != 1964L
    				&& rf.getIdReportFilterPk() != 1965L){
    			lst.add(rf);
    		}
    	return lst;
    }
    
    /**
     * Load reporte.
     *
     * @param reportId the report id
     * @param userAccount the user account
     * @param language the language
     * @return the dyna form model
     * @throws ServiceException the service exception
     */
    public DynaFormModel loadReporte(Long reportId,UserAccountSession userAccount,String language) throws ServiceException{
    	DynaFormModel model = new DynaFormModel();
    	this.reportId = null;
    	if(reportId!=null){
    		 Report report = daoService.find(Report.class, reportId);
    		 this.reportId = reportId; 
    		 if (report !=null) {
    			 List<ReportFilter> filters = report.getReportFilters();
    			 
    			 //issue 1097 se eliminan los filtros 1957,1963,1964,1965 cuando se trate de un perfil CONTABCB
				 if (reportId == 314L && Validations.validateIsNotNull(userAccount.getParticipantCode()) && userAccount.getParticipantCode() == 114L){
					 filters=noRedenderedFilterToContaBCB(filters);
				 }
    			 
    			 BigDecimal number_fields = new BigDecimal(filters.size());
				 BigDecimal number_columns = new BigDecimal(report.getNrColumns().intValue());
				 double rest = number_fields.divide(number_columns).doubleValue();
				 int number_rows = (int)Math.ceil(rest);
				 DynaFormRow rowForm;
				 int column_current = 0;
				 int max_index = 0;
				 //create report type filter
				 rowForm = model.createRegularRow();
				 fillReportFormats(rowForm,report);
				 //exists filters
				 if(number_rows >0){
					 boolean hasFilters = true;
					 while(hasFilters){
						 //create row
						 rowForm = model.createRegularRow();
						 for(int index_column =0;index_column<number_columns.intValue();index_column++){
							 
							 if(max_index < number_fields.intValue()){
								//logic generate field
								 ReportFilter filter = filters.get(max_index);
								 //boolean rendered filter label and input
								 if(!userInfo.getUserAccountSession().getInstitutionType().equals(InstitutionType.DEPOSITARY.getCode()))
								 if(filter.getRendered()!=null&&filter.getRenderedInstitutionType()!=null)
								 if(filter.getRendered().equals(BooleanType.NO.getCode())){
									 if(userInfo.getUserAccountSession().getInstitutionType().equals(filter.getRenderedInstitutionType())){
										 max_index++;
										 break;
									 }
								 } 
								 ReportFilterType filterType = ReportFilterType.get(filter.getFilterType());
								 customizeFilter(filterType,filter,userAccount);
								 
								 //issue 1097: cuando se trate del perfil CONTABCB, en el filtro de 
								 // ESTADO DE COBRO solo [PROCESADO,FACTURADO]
								if (Validations.validateIsNotNull(userAccount.getParticipantCode()) && userAccount.getParticipantCode() == 114L)
									if (filter.getIdReportFilterPk() == 1959L) {
										Map<String, Object> filteredToContaBcb = new LinkedHashMap<>();
										for (Map.Entry<String, Object> item : filter.getSelectMenu().entrySet())
											if (item.getKey().equals("PROCESADO") || item.getKey().equals("FACTURADO"))
												filteredToContaBcb.put(item.getKey(), item.getValue());

										filter.setSelectMenu((LinkedHashMap<String, Object>) filteredToContaBcb);
									}
								 
								 //create widget in screen
								 DynaFormLabel label11 = rowForm.addLabel(getLabelLanguage(filter,language));
								 DynaFormControl control12 = null;
								 //ground count
								 max_index++;
								 //if break row jump
								 if(BooleanType.NO.getCode().equals(filter.getIndBreakRow())){
									 control12 = rowForm.addControl(filter, filterType.name(), 1,1);  
									 label11.setForControl(control12);
								 } else {
									 control12 = rowForm.addControl(filter, filterType.name(), (number_columns.intValue()+1),1); 
									 label11.setForControl(control12);
									 break;
								 }
								 // break;
							 }
							 
						 }
						 //check available fields
						 if(max_index >= number_fields.intValue()){
							 hasFilters = false;
						 }
					 }
				 }
				 
//				 for (int row = 0 ; row < number_rows ; row++) {
//					 	//iterate all rows
//					 	rowForm = model.createRegularRow();
//					 	max_index = number_columns.intValue() * (row + 1);
//						if (max_index > number_fields.intValue()) {
//							max_index = number_fields.intValue();
//						}
//						for(column_current = (row * number_columns.intValue()); column_current < max_index; column_current++) {
//							for(int i=0;i<number_fields.intValue();i++){
//								ReportFilter filter = filters.get(i);
//								if((column_current+1) == filter.getScreenOrder()){
//									ReportFilterType filterType = ReportFilterType.get(filter.getFilterType());
//									customizeFilter(filterType,filter,userAccount);
//									DynaFormLabel label11 = rowForm.addLabel(filter.getFilterLabel(), 1, 1); 
//									DynaFormControl control12 = rowForm.addControl(filter, filterType.name(), 1, 1);  
//								    label11.setForControl(control12); 
//								}
//							}
//						}
//				 }
    		 }
    	}
    	return model;
    }

    /**
     * Gets the label language.
     *
     * @param reportFilter the report filter
     * @param language the language
     * @return the label language
     */
    public String getLabelLanguage(ReportFilter reportFilter,String language){
    	String label = null;
    	if(StringUtils.isNotBlank(language)){
    		if(language.equals(LanguageType.SPANISH.getValue())){
    			label = reportFilter.getFilterLabel();
    		} else {
    			//for default show english
    			label = reportFilter.getFilterLabelEnglish();
    		}
    	} else {
    		label = reportFilter.getFilterLabel();
    	}
    	return label;
    }
    
    /**
     * Customize filter.
     *
     * @param filterType the filter type
     * @param filter the filter
     * @param userAccount the user account
     * @throws ServiceException the service exception
     */
    private void customizeFilter(ReportFilterType filterType,ReportFilter filter,UserAccountSession userAccount) throws ServiceException{
    	switch (filterType) {
		case CALENDAR_FUTURE:
		case CALENDAR_ALL_DATE:
			filter.setValue(CommonsUtilities.currentDate());
			break;
		case TEXT:
			
			break;
		case CALENDAR:
			filter.setValue(CommonsUtilities.currentDate());
			break;
		
		case HELP_PARTICIPANTS:		
			Long participantCode = null;
			if(isReportForBCB(filter.getReport().getIdReportPk())){
				dynamicFieldsService.fillSelectMenuParticipants(filter,participantCode);
				break;
			}
			if(userAccount.isBcbInstitution() && isReportTransferValPurchase(reportId)){
				dynamicFieldsService.fillSelectMenuParticipants(filter,participantCode);
				break;
			}
			if(userAccount.isParticipantInstitucion() || userAccount.isParticipantInvestorInstitucion()){
				participantCode = userAccount.getParticipantCode();
				filter.setDisabled(true);
			}
			if(userAccount.isIssuerInstitucion() || userAccount.isIssuerDpfInstitucion()){
				filter.setDisabled(true);
			}
			if(userAccount.isIssuerDpfInstitucion()
					&& userAccount.getPartIssuerCode() != null
					&& reportMgmtServiceBean.isReportInPartIssuerGroup(this.reportId)){
				participantCode = userAccount.getPartIssuerCode();
			}
			if(userAccount.isIssuerDpfInstitucion() && reportMgmtServiceBean.isClientPortFolioReport(this.reportId)){
				filter.setDisabled(false);
			}
			if(filter.getSqlListQuery()!=null){
				dynamicFieldsService.fillSelectMenuFilter(filter);
			}
			else{
				dynamicFieldsService.fillSelectMenuParticipants(filter,participantCode);
			}
			break;
			
		case HELP_SECURITIES:
			filter.setValue(new SecuritiesTO());
			break;
			
		case HELP_HOLDERS:
			filter.setValue(new HolderHelperOutputTO());
			break;
		case HELP_ACCOUNTS :
			filter.setValue(new HolderAccountHelperResultTO());
			break;
			
		case HELP_ISSUERS:
			filter.setValue(new IssuerTO());
			if(userAccount.isIssuerInstitucion() || userAccount.isIssuerDpfInstitucion()){
				//if is issuer get data and disable helper
				IssuerSearcherTO issuerSearchTO = new IssuerSearcherTO();
				issuerSearchTO.setIssuerCode(userAccount.getIssuerCode());
				Issuer issuer = issuerQueryService.getIssuerByCode(issuerSearchTO);
				if(issuer != null){
					IssuerTO issuerTO = (IssuerTO)filter.getValue();
					issuerTO.setMnemonic(issuer.getMnemonic());
					issuerTO.setIssuerCode(issuer.getIdIssuerPk());
					issuerTO.setIssuerDesc(issuer.getBusinessName());
					issuerTO.setIssuerBusinessName(issuer.getBusinessName() );
					issuerTO.setIssuerDocNumber(issuer.getDocumentNumber());
					issuerTO.setIssuerDocType(issuer.getDocumentType());
					issuerTO.setIssuerState(issuer.getStateIssuer());
					issuerTO.setIssuerGeographicLocation(issuer.getGeographicLocation());
					issuerTO.setIssuerSector( issuer.getEconomicSector() );
					issuerTO.setIssuerActivity( issuer.getEconomicActivity() );
					filter.setDisabled(true);
				}
			}
			
			if(reportMgmtServiceBean.isClientPortFolioReport(this.reportId)){
				if(userAccount.isParticipantInstitucion()
						|| userAccount.isParticipantInvestorInstitucion()
						|| userAccount.isAfpInstitution()
						|| userAccount.isBcbInstitution()
						|| userAccount.isStockExchange()){
					filter.setDisabled(false);
				}
			}
			
			break;
		case COMBO:
		case COMBO_SIMPLE:
			if(filter.getSqlListQuery()!=null){
			   dynamicFieldsService.fillSelectMenuFilter(filter);
			}
			break;
		case COMBO_MULTIPLE:
			if(filter.getSqlListQuery()!=null){
			   dynamicFieldsService.fillSelectMenuFilter(filter);
			}
			break;
		case COMBO_PARAMETERS:
			dynamicFieldsService.fillSelectMenuFilter(filter);
			break;
		case HELP_BILLING:
			filter.setValue(new BillingServiceTo());
			break;
		}
    }
    
    /**
     * Gets the report info.
     *
     * @param reportId the report id
     * @return the report info
     */
    public Report getReportInfo(Long reportId) {
    	 return daoService.find(Report.class, reportId);
    }
    
    /**
     * Gets the users from institution.
     *
     * @param institution the institution
     * @param institutionCode the institution code
     * @return the users from institution
     */
    public List<UserAccountSession> getUsersFromInstitution(InstitutionType institution,Object institutionCode){
    	return userAccountService.getUsersInformation(null, institution, institutionCode, null,null,ExternalInterfaceType.NO.getCode());
    	
    }
    
    /**
     * Gets the report logger details.
     *
     * @param idReportLoggerPk the id report logger pk
     * @return the report logger details
     */
    @SuppressWarnings("unchecked")
	public List<ReportLoggerDetail> getReportLoggerDetails(Long idReportLoggerPk) {
   	   Map<String, Object> parameters = new HashMap<String, Object>();
   	   parameters.put("idReportLoggerPk", idReportLoggerPk);
   	   return daoService.findWithNamedQuery(ReportLoggerDetail.FIND_BY_REPORT_LOGGER, parameters);
    }
    
    /**
     * Gets the report logger file info.
     *
     * @param idReportLoggerPk the id report logger pk
     * @return the report logger file info
     */
    @SuppressWarnings("unchecked")
   	public List<ReportLoggerFile> getReportLoggerFileInfo(Long idReportLoggerPk) {
      	   Map<String, Object> parameters = new HashMap<String, Object>();
      	   parameters.put("reportLogger", idReportLoggerPk);
      	   return daoService.findWithNamedQuery(ReportLoggerFile.REPORTE_FIND_BY_FK, parameters);
    }
    
   	/**
	    * Gets the report logger file dataReport to reports on database.
	    *
	    * @param idReportFilePk the id report file pk
	    * @return the report logger file content
	    */
	 public byte[] getReportLoggerFileContent(Long idReportFilePk) {
      	   Map<String, Object> parameters = new HashMap<String, Object>();
      	   parameters.put("idReportFilePk", idReportFilePk);
      	 return (byte[])daoService.findObjectByNamedQuery(ReportLoggerFile.REPORTE_DATA_CONTENT_BY_PK, parameters);
    }
	 
	/**
	    * Gets the report logger file dataReport to reports on disk.
	    *
	    * @param idReportFilePk the id report file pk
	    * @return the report logger file content
	    */
	   public ReportLoggerFile getReportLoggerFileConst(Long idReportFilePk) {
    	   Map<String, Object> parameters = new HashMap<String, Object>();
    	   parameters.put("idReportFilePk", idReportFilePk);
    	   ReportLoggerFile repLog = new ReportLoggerFile();
    	   repLog= daoService.findObjectByNamedQuery(ReportLoggerFile.REPORTE_CONTENT_BY_PK, parameters);
    	   return repLog;
  }
    
    /**
     * Find report loggers.
     *
     * @param reportLoggerFilter the report logger filter
     * @return the list
     */
    public List<ReportLoggerTO> findReportLoggers(ReportLoggerTO reportLoggerFilter){
    	List<Object[]> result = reportMgmtServiceBean.findReportLoggers(reportLoggerFilter);
    	if(result.size()>0){
    		List<ReportLoggerTO> reports = new ArrayList<ReportLoggerTO>();
    		for (Object[] reportInfo : result) {
    			ReportLoggerTO reportLogger = new ReportLoggerTO();
    			reportLogger.setIdReportLoggerPk((Long)reportInfo[0]);
    			reportLogger.setIdReportPk((Long)reportInfo[1]);
    			reportLogger.setMnemonic(reportInfo[2]==null?null:(String)reportInfo[2]);
    			reportLogger.setName(reportInfo[3]==null?null:(String)reportInfo[3]);
    			reportLogger.setIndError(reportInfo[4]==null?null:(Integer)reportInfo[4]);
    			reportLogger.setExecutionDate((Date)reportInfo[5]);
    			reportLogger.setState((Integer)reportInfo[7]);
    			reportLogger.setStateDescription((String)reportInfo[8]);
    			reportLogger.setErrorDetail(reportInfo[9]==null?null:(String)reportInfo[9]);
    			if(reportInfo[10]!=null){
    				reportLogger.setName(reportInfo[10].toString());
    			}
    			if(reportInfo[11]!=null){
    				reportLogger.setTotalFileLength(reportInfo[11].toString()+GeneralConstants.REPORT_SIZE_MB);
    			}
    			if(reportInfo[12]!=null){
    				reportLogger.setReportFormat(new Integer(reportInfo[12].toString()));
    			}    			
    			reports.add(reportLogger);
    		}
    		return reports;
    	}
    	return null;
    }
    
    /**
     * Gets the business process level.
     *
     * @param level the level
     * @return the business process level
     */
    @SuppressWarnings("unchecked")
	public List<BusinessProcess> getBusinessProcessLevel(Integer level){
		Map<String, Object> parameters = new HashMap<String, Object>();
   	    parameters.put("level", level);
   	    parameters.put("status", ProcessStateType.REGISTERED.getCode());
   	    return daoService.findWithNamedQuery(BusinessProcess.PROCESS_FIND_BY_LEVEL, parameters);
	}
    
    /**
     * Fill report formats.
     *
     * @param rowForm the row form
     * @param report the report
     * @throws ServiceException the service exception
     */
    public void fillReportFormats(DynaFormRow rowForm,Report report) throws ServiceException{
    	ParameterTableTO parameterFilter = new ParameterTableTO();
		 parameterFilter.setState(BooleanType.YES.getCode());
		 parameterFilter.setMasterTableFk(MasterTableType.REPORT_FORMAT.getCode());
		 parameterFilter.setIndicator4(BooleanType.YES.getCode());
		 List<ParameterTable> lstReportFormats= parametersServiceFacade.getListParameterTableServiceBean(parameterFilter);
		 Map<Integer, String> mpReportFormats = new HashMap<Integer, String>();
			for(ParameterTable paramTab:lstReportFormats){
				mpReportFormats.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
			}
		 DynaFormLabel label=rowForm.addLabel("Formato del Reporte");
		 ReportFilter reportfilter=new ReportFilter();
		 reportfilter.setIdReportFilterPk(new Long(0));
		 reportfilter.setIdReportFilterPk(report.getIdReportPk());
		 reportfilter.setFilterLabel("Formato del Reporte");
		 reportfilter.setFilterType(ReportFilterType.COMBO_SIMPLE.getCode());
		 reportfilter.setFilterName("reportFormats");
		 reportfilter.setScreenOrder(0);
		 reportfilter.setIndRequired(1);
		 reportfilter.setIndValidationUser(0);
		 reportfilter.setIndViewer(1);
		 reportfilter.setMasterTable(18);
		 reportfilter.setIndBreakRow(1);
		 reportfilter.setSelectMenu(new LinkedHashMap<String,Object>());
		 if(report.getIndPdfFormat().equals(GeneralConstants.ONE_VALUE_INTEGER)){
			 reportfilter.getSelectMenu().put(mpReportFormats.get(ReportFormatType.PDF.getCode()),  
					 						  ReportFormatType.PDF.getCode());
		 }
		 if(report.getIndTxtFormat().equals(GeneralConstants.ONE_VALUE_INTEGER)){
			 reportfilter.getSelectMenu().put(mpReportFormats.get(ReportFormatType.TXT.getCode()),  
					 						  ReportFormatType.TXT.getCode());
		 }
		 if(report.getIndXlsFormat().equals(GeneralConstants.ONE_VALUE_INTEGER)){
			 reportfilter.getSelectMenu().put(mpReportFormats.get(ReportFormatType.XLS.getCode()),  
					 						  ReportFormatType.XLS.getCode());
		 }
		 if(GeneralConstants.ONE_VALUE_INTEGER.equals(report.getIndPoiFormat())){
			 reportfilter.getSelectMenu().put(mpReportFormats.get(ReportFormatType.POI.getCode()),  
					 						  ReportFormatType.POI.getCode());
		 }
		 if(report.getIndExcelDelimitedFormat().equals(GeneralConstants.ONE_VALUE_INTEGER)) {
			 reportfilter.getSelectMenu().put(mpReportFormats.get(ReportFormatType.EXCEL_DELIMITADO.getCode()),  
					  ReportFormatType.EXCEL_DELIMITADO.getCode());
		 }
		 ReportFilterType filterTypeInitial = ReportFilterType.get(reportfilter.getFilterType());
		 DynaFormControl control = rowForm.addControl(reportfilter, filterTypeInitial.name(),1,1);  
		 label.setForControl(control); 
    }
    
    /**
     * Gets the system schema by id.
     *
     * @param idSystemPk the id system pk
     * @return the system schema by id
     * @throws ServiceException the service exception
     */
    public String getSystemSchemaById(Integer idSystemPk) throws ServiceException{
    	return auditConsolidatedReport.getSystemSchemaById(idSystemPk);
    }
    
    /**
     * Fill select menu filter with tables.
     *
     * @param schemaBD the schema bd
     * @param reportFilter the report filter
     * @throws ServiceException the service exception
     */
    public void fillSelectMenuFilterWithTables(String schemaBD, ReportFilter reportFilter)
			throws ServiceException {
    	auditConsolidatedReport.fillSelectMenuFilterWithTables(schemaBD, reportFilter);
    }
    
    /**
     * Fill select menu filter with columns.
     *
     * @param schemaBD the schema bd
     * @param tableName the table name
     * @param reportFilter the report filter
     * @throws ServiceException the service exception
     */
    public void fillSelectMenuFilterWithColumns(String schemaBD,String tableName,ReportFilter reportFilter)
			throws ServiceException {
    	auditConsolidatedReport.fillSelectMenuFilterWithColumns(schemaBD, tableName, reportFilter);
    }

	/**
	 * Gets the parameters service facade.
	 *
	 * @return the parameters service facade
	 */
	public GeneralParametersFacade getParametersServiceFacade() {
		return parametersServiceFacade;
	}

	/**
	 * Sets the parameters service facade.
	 *
	 * @param parametersServiceFacade the new parameters service facade
	 */
	public void setParametersServiceFacade(
			GeneralParametersFacade parametersServiceFacade) {
		this.parametersServiceFacade = parametersServiceFacade;
	}

	/**
	 * Checks if is report in part issuer group.
	 *
	 * @param reportId the report id
	 * @return the boolean
	 */
	public Boolean isReportInPartIssuerGroup(Long reportId){
		return reportMgmtServiceBean.isReportInPartIssuerGroup(reportId);
	}
	
	/**
	 * Checks if is report for bcb.
	 *
	 * @param reportId the report id
	 * @return true, if is report for bcb
	 */
	public boolean isReportForBCB(Long reportId) {
		return reportMgmtServiceBean.isReportForBCB(reportId);
	}
	
	/**
	 * Checks if is report transfer val purchase
	 *
	 * @param reportId the report id
	 * @return true, if is report for bcb
	 */
	public boolean isReportTransferValPurchase(Long reportId) {
		return reportMgmtServiceBean.isReportTransferValPurchase(reportId);
	}

	/**
	 * Gets the report id.
	 *
	 * @return the reportId
	 */
	public Long getReportId() {
		return reportId;
	}

	/**
	 * Sets the report id.
	 *
	 * @param reportId the reportId to set
	 */
	public void setReportId(Long reportId) {
		this.reportId = reportId;
	}
	
	/**
	 * Process send reports FTP.
	 *
	 * @param listReportLoggerTO the list report logger to
	 * @param objParameterTable the object parameter table
	 * @param isCurrentdate the is current date
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public boolean processSendReportsFtp(List<ReportLoggerTO> listReportLoggerTO, ParameterTable objParameterTable, 
			boolean isCurrentDate) throws ServiceException, IOException{
		boolean statusProcess = true;
		if(Validations.validateListIsNotNullAndNotEmpty(listReportLoggerTO)){
			List<FileData> listDataReport = new ArrayList<FileData>();
			for(ReportLoggerTO objReportLoggerTO : listReportLoggerTO){
				if(objReportLoggerTO.isSelected()){
					FileData objFileData = new FileData();					
					String filePath = objParameterTable.getDescription().trim();					
					if(isCurrentDate) {
						filePath = generateFilePath(filePath, objParameterTable.getParameterTablePk(), CommonsUtilities.currentDate());
					} else {
						filePath = generateFilePath(filePath, objParameterTable.getParameterTablePk(), objReportLoggerTO.getExecutionDate());
					}					
					String reportFormat = ReportConstant.EXTENDS_PDF;
					if(Validations.validateIsNotNullAndNotEmpty(objReportLoggerTO.getReportFormat())){
						reportFormat = GeneralConstants.DOT + ReportFormatType.get(objReportLoggerTO.getReportFormat()).getValue();
					}
					String reportName = generateNameReport(objReportLoggerTO, isCurrentDate);					
					List<ReportLoggerFile> lstReportLoggerFile = getReportLoggerFileInfo(objReportLoggerTO.getIdReportLoggerPk());
					if(Validations.validateListIsNotNullAndNotEmpty(lstReportLoggerFile)){
						ReportLoggerFile objReportLoggerFile = lstReportLoggerFile.get(0);
						ReportLoggerFile reportDisk= getReportLoggerFileConst(objReportLoggerFile.getIdLoggerFilePk());
						if(reportDisk.getNameTrace()!=null){
							String reportDiskRute=(reportDisk.getNameTrace()+reportDisk.getNameFile());
							Path path = Paths.get(reportDiskRute);
							byte[] reportData = Files.readAllBytes(path);
							if(reportData != null){
								objFileData.setReportData(reportData);
							}
						}else{
							byte [] reportData = getReportLoggerFileContent(objReportLoggerFile.getIdLoggerFilePk());
							if(reportData != null){
								objFileData.setReportData(reportData);
							}
						}
						objFileData.setReportName(reportName + reportFormat);
						objFileData.setFilePath(filePath);
						listDataReport.add(objFileData);
					}																			
				}
			}			
			if(Validations.validateListIsNotNullAndNotEmpty(listDataReport)){
				statusProcess = sendFtpUpload(listDataReport, objParameterTable);
			}
		}
		return statusProcess;
	}
	
	/**
	 * Send FTP upload.
	 *
	 * @param listFileData the list file data
	 * @param objParameterTable the object parameter table
	 * @return true, if successful
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public boolean sendFtpUpload(List<FileData> listFileData, ParameterTable objParameterTable) throws IOException {				
		FTPParameters ftpParameters = new FTPParameters();		
		ftpParameters.setServer(objParameterTable.getText3().trim());
		ftpParameters.setUser(objParameterTable.getText1().trim());
		ftpParameters.setPass(objParameterTable.getText2().trim());
		ftpParameters.setRemotePath(objParameterTable.getDescription());
		ftpParameters.setListFileData(listFileData);							
		UploadFtp upload= new UploadFtp(ftpParameters);
		return upload.registerAndSendFileMassive();		
	}
	
	/**
	 * Generate name report.
	 *
	 * @param objReportLoggerTO the object report logger to
	 * @param isCurrentDate the is current date
	 * @return the string
	 */
	public String generateNameReport(ReportLoggerTO objReportLoggerTO, boolean isCurrentDate) {
		String nameReport = ReportConstant.NAME_REPORT_DEAFULT;
		String strParticipant = GeneralConstants.EMPTY_STRING;
		String strCurrency = GeneralConstants.EMPTY_STRING;
		String strMnemonicReport = GeneralConstants.EMPTY_STRING;	
		String strDate = GeneralConstants.EMPTY_STRING;	
		String strScheludeType = GeneralConstants.EMPTY_STRING;
		List<Long> listReportCodeType = ReportCodeType.listPk;	
		if(isCurrentDate){
			strDate = CommonsUtilities.convertDateToStringReport(CommonsUtilities.currentDate());
		} else {
			strDate = CommonsUtilities.convertDateToStringReport(objReportLoggerTO.getExecutionDate());
		}
		if(listReportCodeType.contains(objReportLoggerTO.getIdReportPk())){
			strMnemonicReport = ReportCodeType.get(objReportLoggerTO.getIdReportPk()).getMnemonic();									
			List<ReportLoggerDetail> listReportLoggerDetail = getReportLoggerDetails(objReportLoggerTO.getIdReportLoggerPk());
			if(Validations.validateListIsNotNullAndNotEmpty(listReportLoggerDetail)){
				for(ReportLoggerDetail objReportLoggerDetail : listReportLoggerDetail){								
					if(ReportConstant.PARTICIPANT_PARAM.compareTo(objReportLoggerDetail.getFilterName()) == 0){
						if(Validations.validateIsNotNullAndNotEmpty(objReportLoggerDetail.getFilterValue())){
							Long idParticipantPk = new Long(objReportLoggerDetail.getFilterValue().toString());
							List<Object[]> lstObjParticipant = accountReportService.getParticipantDescriptionByParticipantId(idParticipantPk);
							if(Validations.validateListIsNotNullAndNotEmpty(lstObjParticipant)){
								Object[] objParticipant = lstObjParticipant.get(0);
								strParticipant = objParticipant[2].toString() + GeneralConstants.UNDERLINE_SEPARATOR;								
							}							
						}
					}					
					if(ReportConstant.CURRENCY.compareTo(objReportLoggerDetail.getFilterName()) == 0){
						if(Validations.validateIsNotNullAndNotEmpty(objReportLoggerDetail.getFilterValue())){
							Integer parameterTablePk = new Integer(objReportLoggerDetail.getFilterValue().toString());
							ParameterTable objParameterTable = reportMgmtServiceBean.find(ParameterTable.class, parameterTablePk);
							strCurrency = objParameterTable.getDescription() + GeneralConstants.UNDERLINE_SEPARATOR;
						}
					}
					if(ReportConstant.STAGE.compareTo(objReportLoggerDetail.getFilterName()) == 0 
							|| ReportConstant.SCHEDULE_TYPE.compareTo(objReportLoggerDetail.getFilterName()) == 0){
						if(Validations.validateIsNotNullAndNotEmpty(objReportLoggerDetail.getFilterValue())){
							strScheludeType = SettlementScheduleType.lookup.get(Integer.parseInt(objReportLoggerDetail.getFilterValue().toString())).getDescription()
									+ GeneralConstants.UNDERLINE_SEPARATOR;
						}
					}
				}				
				if(Validations.validateIsNotNullAndNotEmpty(strMnemonicReport)){
					nameReport = nameReport + strMnemonicReport;
				}
				if(Validations.validateIsNotNullAndNotEmpty(strScheludeType)){
					nameReport = nameReport + strScheludeType;
				}
				if(Validations.validateIsNotNullAndNotEmpty(strCurrency)){
					nameReport = nameReport + strCurrency;
				}
				if(Validations.validateIsNotNullAndNotEmpty(strParticipant)){
					nameReport = nameReport + strParticipant;
				}
				if(Validations.validateIsNotNullAndNotEmpty(strDate)){
					nameReport = nameReport + strDate;
				}				
			}
		} else {
			return objReportLoggerTO.getMnemonic() + GeneralConstants.UNDERLINE_SEPARATOR + strDate;
		}
		return nameReport;
	}
	
	/**
	 * Generate file path.
	 *
	 * @param filePath the file path
	 * @param parameterPk the parameter pk
	 * @param dtProcessDate the dt process date
	 * @return the string
	 */
	private String generateFilePath(String filePath, Integer parameterPk, Date dtProcessDate) {
		String strRemotePath = filePath;
		if(ReportConstant.PARAMTER_FTP_SETTLEMENTS.equals(parameterPk)){			
			String year = CommonsUtilities.convertDateToStringLocaleES(dtProcessDate, "yyyy");
			String mes = CommonsUtilities.convertDateToStringLocaleES(dtProcessDate, "MMMM");
			String day = CommonsUtilities.convertDateToStringLocaleES(dtProcessDate, "dd");
			day = StringUtils.leftPad(day, 2, "0");
			strRemotePath = strRemotePath + year + "/" + mes + "/" + day + "/";
		}		
		return strRemotePath;
	}
	
}
