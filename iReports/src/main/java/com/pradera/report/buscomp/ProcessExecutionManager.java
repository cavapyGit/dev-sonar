package com.pradera.report.buscomp;

import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.type.QueueType;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ProcessExecutionManager.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/07/2013
 */
@ApplicationScoped
public class ProcessExecutionManager implements Serializable {

	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** serial version id. */
	private static final long serialVersionUID = 1L;
	
	@Inject @Configurable
	Integer maxThreadQueueOne;
	
	@Inject @Configurable
	Integer maxThreadQueueTwo;
	
	@Inject @Configurable
	Integer maxThreadQueueThree;
	
	@Inject @Configurable
	String reportLogo;
	
	/** The cpus number. */
	private Integer cpusNumber;
	
	/** The max threads. */
	private Integer maxThreads; 
	
	//FIXME configuration
	/** The scalefactor. */
	private Integer scalefactor = Integer.valueOf(10);
	
	/** The executor service one. */
	private ExecutorService executorServiceOne;
	
	/** The executor service two. */
	private ExecutorService executorServiceTwo;
	
	/** The executor service three. */
	private ExecutorService executorServiceThree;

	
	/**
	 * Default constructor of ProcessExecutionManager.
	 */
	@PostConstruct
	public void ProcessExecutionManagerInit(){
		
		cpusNumber = Integer.valueOf(Runtime.getRuntime().availableProcessors());
		
		maxThreads = cpusNumber * scalefactor;
		
		maxThreads = (maxThreads > 0 ? maxThreads : 1);
		
		BasicThreadFactory factoryQueueOne = new BasicThreadFactory.Builder()
	     .namingPattern("Thread Report queueOne Nro %d")
	     .daemon(true)
//	     .priority(Thread.MAX_PRIORITY)
	     .build();
		
		BasicThreadFactory factoryQueueTwo = new BasicThreadFactory.Builder()
	     .namingPattern("Thread Report queueTwo Nro %d")
	     .daemon(true)
//	     .priority(Thread.MAX_PRIORITY)
	     .build();
		
		BasicThreadFactory factoryQueueThree = new BasicThreadFactory.Builder()
	     .namingPattern("Thread Report queueThree Nro %d")
	     .daemon(true)
//	     .priority(Thread.MAX_PRIORITY)
	     .build();
		
		//Instances ThreaddPool
		executorServiceOne = new ThreadPoolExecutor(maxThreadQueueOne,
				maxThreadQueueOne, 10, TimeUnit.MINUTES, 
				new ArrayBlockingQueue<Runnable>(maxThreadQueueOne, true),factoryQueueOne,
				new ThreadPoolExecutor.CallerRunsPolicy());
		
		executorServiceTwo = new ThreadPoolExecutor(maxThreadQueueTwo,
				maxThreadQueueTwo, 10, TimeUnit.MINUTES, 
				new ArrayBlockingQueue<Runnable>(maxThreadQueueTwo, true),factoryQueueTwo,
				new ThreadPoolExecutor.CallerRunsPolicy());
		
		executorServiceThree = new ThreadPoolExecutor(maxThreadQueueThree,
				maxThreadQueueThree, 20, TimeUnit.MINUTES, 
				new ArrayBlockingQueue<Runnable>(maxThreadQueueThree, true),factoryQueueThree,
				new ThreadPoolExecutor.CallerRunsPolicy());
		
		//save logo report in temp file
		
		try(InputStream inputIO = this.getClass().getResourceAsStream(reportLogo)){
			String separator = System.getProperty("file.separator");
			String basePath = System.getProperty("java.io.tmpdir");
			String uniqueName = basePath+separator+ ReportConstant.LOGO_RESOURCE_PATH;
			Files.deleteIfExists(Paths.get(uniqueName));
			Files.write(Paths.get(uniqueName), IOUtils.toByteArray(inputIO), StandardOpenOption.CREATE_NEW);
		} catch (Exception eio) {
			log.error("error save report logo image : "+eio.getMessage());
		}
	}
	
	
	/**
	 * This method is to start the report generation process.
	 *
	 * @param queueNumber the queue number
	 * @param genericReport the generic report
	 */
	public void startReportProcess(Integer queueNumber, GenericReport genericReport) {
		log.info("\n\n------------------->ProcessExecutionManager.startReportProcess() started");
		QueueType queue = QueueType.get(queueNumber);
		switch (queue) {
		case ONE:
			executorServiceOne.submit(genericReport);
			break;
		case TWO:
			executorServiceTwo.submit(genericReport);
			break;
		case THREE:
			executorServiceThree.submit(genericReport);
			break;
		}
		
		log.info("\n\n------------------->ProcessExecutionManager.startReportProcess() completed");
	}
	
	@PreDestroy
	public void releasePool(){
		executorServiceOne.shutdown();
		executorServiceTwo.shutdown();
		executorServiceThree.shutdown();
	}
}
