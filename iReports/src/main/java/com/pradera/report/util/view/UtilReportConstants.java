package com.pradera.report.util.view;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import com.pradera.report.generation.executor.custody.BlockRequestStatusChangeReport;

public class UtilReportConstants {
	
	public static BufferedImage readLogoReport() {
		
		InputStream inputFileLogo = BlockRequestStatusChangeReport.class.getClassLoader().getResourceAsStream("/logo-cavapy.png");
		BufferedImage logo = null;
		
		try {
			logo = ImageIO.read(inputFileLogo);
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		return logo;
		
	}
	
}