package com.pradera.report.util.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.report.generation.executor.custody.ConciliationDailyOperationsReport;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.service.ReportManageServiceBean;
import com.pradera.report.generation.service.ReportUtilServiceBean;

@BatchProcess(name = "DailyReconciliationReportBatch")
@RequestScoped
public class DailyReconciliationReportBatch implements JobExecution, Serializable {

	/**
	 * 
	 */
	@EJB
    ReportUtilServiceBean reportUtilService; 
	
	@Inject
    ReportManageServiceBean reportManageService;
	@EJB
	CustodyReportServiceBean custodyReportServiceBean;
	
	@Inject 
	private UserInfo userInfo;
	/** The report user. */
	private ReportUser reportUser;
	private String dateSearch = null;
	
	private Map<String,String> parameters;
	private Map<String,String> parameterDetails;
	private static final long serialVersionUID = 1L;
	@Override
	public void startJob(ProcessLogger processLogger) {
		dateSearch=null;
		List<ProcessLoggerDetail> lstProcessLoggerDetails = processLogger.getProcessLoggerDetails();
		for (ProcessLoggerDetail objDetail: lstProcessLoggerDetails){
			if (StringUtils.equalsIgnoreCase(GeneralConstants.PARAMETER_DATE, objDetail.getParameterName())) {
				dateSearch=objDetail.getParameterValue();
			}
		}
		executeDailyReportConciliation();
	}
	@LoggerAuditWeb
	private void executeDailyReportConciliation() {
		parameters = new HashMap<String, String>();
		parameterDetails = new HashMap<String, String>();
		
		parameters.put("date", dateSearch);
		reportUser.setUserName(userInfo.getUserAccountSession().getUserName());
		reportUser.setShowNotification(BooleanType.YES.getCode());
		try {
			ReportLogger logger=saveReportExecution(122L,parameters, parameterDetails,reportUser);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	private ReportLogger saveReportExecution(Long reportId,Map<String,String> parameters,Map<String,String> parameterDetails,ReportUser reportUser) throws ServiceException{
    	ReportLogger reportLogger = reportUtilService.getReportLogger(reportId, parameters, parameterDetails);
    	//save indicator about notification
    	if(reportUser != null){
    		//report format to generate
    		if(Validations.validateIsNotNull(reportUser.getReportFormat())){
    			reportLogger.setReportFormat(reportUser.getReportFormat());
    		}else{
    			reportLogger.setReportFormat(Integer.parseInt(parameters.get("reportFormats")));
    		}
    		reportLogger.setIndSendNotification(reportUser.getShowNotification());
    	} else {
    		//if no setting format for default is pdf
    		reportLogger.setReportFormat(ReportFormatType.PDF.getCode());
    		reportLogger.setIndSendNotification(BooleanType.NO.getCode());
    	}
    	return reportManageService.saveReportExecution(reportLogger, reportUser);
    }
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}

}
