package com.pradera.report.util.view;

/**
 *
 * @author Henry
 */public class SignPosition{
    private int page;
    private Float x;
    private Float y;

    public SignPosition() {
        x=0.0F;
        y=0.0F;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public Float getX() {
        return x;
    }

    public void setX(Float x) {
        this.x = x;
    }

    public Float getY() {
        return y;
    }

    public void setY(Float y) {
        this.y = y;
    }
    @Override
    public String toString(){
    	return "\nx:"+x+"\ny:"+y+"\nPag:"+page;
    }
 }