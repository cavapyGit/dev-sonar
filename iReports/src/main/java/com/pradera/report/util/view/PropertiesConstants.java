package com.pradera.report.util.view;

// TODO: Auto-generated Javadoc
/**
 * The Class PropertiesConstants.
 */
public class PropertiesConstants {
	
	/** The Constant REPORT_SUCESS_GENERATION. */
	public static final String REPORT_SUCESS_GENERATION = "message.report.generation.success";

	/** The Constant REPORT_SUCESS_FAIL. */
	public static final String REPORT_SUCESS_FAIL = "message.report.generation.fail";
	
	/** The Constant ALERT_MESSAGE. */
	public static final String ALERT_MESSAGE = "lbl.header.alert";
	
	/** The Constant IDLE_TIME_EXPIRED. */
	public static final String IDLE_TIME_EXPIRED = "idle.session.expired";

	/** The Constant NO_FOUND_DATA. */
	public static final String NO_FOUND_DATA="No se encontro data disponible";
	
	/** The Constant FORMAT_REPORT_LABEL. */
	public static final String FORMAT_REPORT_LABEL="lbl.format.report";
	
	/** The Constant FORMAT_REPORT_LABEL_TAG. */
	public static final String FORMAT_REPORT_LABEL_TAG="lbl.format.report.label";
	
	/** The Constant ACCOUNT_NOT_SELECTED. */
	public static final String ACCOUNT_NOT_SELECTED="lbl.account.not.selected";
	
	public static final String NO_FOUND_DATA_BLANK=" ";
	
	/** The Constant REPORT_IND_ERROR. */
	public static final String REPORT_IND_ERROR = "lbl.report.ind.error";
	
	/** The Constant OBJ_REPORT_NOT_SELECTED. */
	public static final String OBJ_REPORT_NOT_SELECTED = "lbl.obj.report.not.selected";
	
	/** The Constant CONFIRM_REPORT_SENDER_FTP. */
	public static final String CONFIRM_REPORT_SENDER_FTP = "confirm.report.sender.ftp";
	
	/** The Constant FOLDER_OBJ_PARAMETER_NOT_SELECTED. */
	public static final String FOLDER_OBJ_PARAMETER_NOT_SELECTED = "folder.obj.parameter.not.selected";
	
	/** The Constant CONFIRM_REPORT_SENDER_FTP_SUCESS. */
	public static final String CONFIRM_REPORT_SENDER_FTP_SUCESS = "confirm.report.sender.ftp.sucess";
	
	/** The Constant CONFIRM_REPORT_SENDER_FTP_SUCESS. */
	public static final String ERROR_OBJ_REPORT_NOT_SEND = "error.obj.report.not.send";
	
	/**
	 * Instantiates a new properties constants.
	 */
	public PropertiesConstants() {
		// TODO Auto-generated constructor stub
	}

}
