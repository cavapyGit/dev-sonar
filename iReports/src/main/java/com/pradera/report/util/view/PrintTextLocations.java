/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pradera.report.util.view;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.util.PDFTextStripper;
import org.apache.pdfbox.util.TextPosition;

import com.pradera.integration.common.validation.Validations;

public class PrintTextLocations extends PDFTextStripper {

    private StringBuilder tWord = new StringBuilder();
    private final String searchString="Nota";
    private final String[] seekA=new String []{searchString};
    private List<String> wordList = new ArrayList<String>();
    private String newWord;
    private String sWord;
    
    private boolean is1stChar = true;
    private boolean lineMatch;
    private int pageNo;
    private double lastYVal;
    private List allPages;
    private Float height;
    private boolean continuar;
    public static SignPosition position;
    public PrintTextLocations()
            throws IOException {
        super.setSortByPosition(true);
        height=792.0F;
    }
    public synchronized SignPosition getPosition(byte[] fileContent) throws IOException, InterruptedException{
    	if(!Validations.validateIsNotNullAndNotEmpty(fileContent))
    		return new SignPosition();
        continuar =  true;
        PDDocument document = null;
        try {
            ByteArrayInputStream arrayInputStream=new ByteArrayInputStream(fileContent);
            document = PDDocument.load(arrayInputStream);
            PrintTextLocations printer = new PrintTextLocations();
            allPages = document.getDocumentCatalog().getAllPages();
            for (int i = 0; i < allPages.size()&&continuar; i++)
            {
                PDPage page = (PDPage) allPages.get(i);
                PDStream contents = page.getContents();
                if (contents != null){
                	printer.processStream(page, page.findResources(), page.getContents().getStream());
                }
            }
        }
        catch (Exception ex){
        	ex.printStackTrace();
        }
        finally {
            if (document != null)
            {
                document.close();               
            }
        }
        position.setPage(allPages.size());
        return position;
    }
    @Override
    protected void processTextPosition(TextPosition text)
    {
    	try {
    		String tChar = text.getCharacter();
            String REGEX = "[,.\\[\\](:;!?)/]";
            char c = tChar.charAt(0);
            lineMatch = matchCharLine(text);
            if ((!tChar.matches(REGEX)) && (!Character.isWhitespace(c)))
            {
                if ((!is1stChar) && (lineMatch == true))
                {
                    appendChar(tChar);
                } else if (is1stChar == true) {
                    setWordCoord(text, tChar);
                }
            } else {
                endWord(text);
            }
		} catch (Exception e) {
			e.printStackTrace();
			position=new SignPosition();
		}
    }

    private void appendChar(String tChar) {
        tWord.append(tChar);
        is1stChar = false;
    }

    private void setWordCoord(TextPosition texto, String textoABuscar)
    {
       tWord.append("(").append(pageNo).append(")[").append(roundVal(texto.getXDirAdj())).append(":").append(roundVal(texto.getYDirAdj())).append("] ").append(textoABuscar);
       is1stChar = false;
    }
    private void endWord(TextPosition textPosition)
    {
    	try {
    		 newWord = tWord.toString().replaceAll("[^\\x00-\\x7F]", "");
    	        sWord = newWord.substring(newWord.lastIndexOf(' ') + 1);
    	        if (!"".equals(sWord))
    	        {
    	            if (Arrays.asList(seekA).contains(sWord))
    	            {
    	            	String coord = newWord.substring(newWord.indexOf('[')+1,newWord.lastIndexOf(']'));
    	        		StringTokenizer tokenizer = new StringTokenizer(coord,":");
    	        		position=new SignPosition();
    	        		position.setX(Float.parseFloat(tokenizer.nextToken()));
    	        		position.setY(height-Float.parseFloat(tokenizer.nextToken()));
    	                wordList.add(newWord);
    	                continuar=false;
    	            }
    	            else if (sWord.equals(searchString))
    	            {
    	                wordList.add(newWord);
    	            }
    	        }
    	        tWord.delete(0, tWord.length());
    	        is1stChar = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
    }

    private boolean matchCharLine(TextPosition text) {       
        Double yVal = roundVal(Float.valueOf(text.getYDirAdj()));    
        if (yVal == lastYVal) {
            return true;
        }
        lastYVal = yVal;
        endWord(text);
        return false;
    }

    private Double roundVal(Float yVal) {
        DecimalFormat rounded = new DecimalFormat("0.0'0'");
        Double yValDub = new Double(rounded.format(yVal).replace(',','.'));
        return yValDub;
    }
}