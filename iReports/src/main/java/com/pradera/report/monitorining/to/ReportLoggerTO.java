package com.pradera.report.monitorining.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.model.report.ReportLoggerFile;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ReportLoggerTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/09/2013
 */
public class ReportLoggerTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The access user. */
	private String accessUser;
	
	/** The id report logger pk. */
	private Long idReportLoggerPk;
	
	/** The id report pk. */
	private Long idReportPk;
	
	/** The mnemonic. */
	private String mnemonic;
	
	/** The name. */
	private String name;
	
	/** The ind error. */
	private Integer indError;
	
	/** The description. */
	private String description;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/** The execution date. */
	private Date executionDate;
	
	/** The end date. */
	private Date endDate;
	
	/** The state description. */
	private String stateDescription;
	
	/** The state. */
	private Integer state;
	
	/** The error detail. */
	private String errorDetail;
	
	/** The module process. */
	private Long moduleProcess;
	
	/** The total file length. */
	private String totalFileLength;
	
	/** The report logger details. */
	private List<ReportLoggerDetail> reportLoggerDetails;
	
	/** The report logger files. */
	private List<ReportLoggerFile> reportLoggerFiles;
	
	/** The selected. */
	private  boolean selected;
	
	/** The report format. */
	private Integer reportFormat;
	
	/**
	 * Instantiates a new report logger to.
	 */
	public ReportLoggerTO() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Instantiates a new report logger to.
	 *
	 * @param initialDate the initial date
	 * @param finalDate the final date
	 */
	public ReportLoggerTO(Date initialDate, Date finalDate) {
		super();
		this.initialDate = initialDate;
		this.finalDate = finalDate;
	}
	
	/**
	 * Gets the id report logger pk.
	 *
	 * @return the idReportLoggerPk
	 */
	public Long getIdReportLoggerPk() {
		return idReportLoggerPk;
	}
	
	/**
	 * Sets the id report logger pk.
	 *
	 * @param idReportLoggerPk the idReportLoggerPk to set
	 */
	public void setIdReportLoggerPk(Long idReportLoggerPk) {
		this.idReportLoggerPk = idReportLoggerPk;
	}
	
	/**
	 * Gets the id report pk.
	 *
	 * @return the idReportPk
	 */
	public Long getIdReportPk() {
		return idReportPk;
	}
	
	/**
	 * Sets the id report pk.
	 *
	 * @param idReportPk the idReportPk to set
	 */
	public void setIdReportPk(Long idReportPk) {
		this.idReportPk = idReportPk;
	}
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 *
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the ind error.
	 *
	 * @return the indError
	 */
	public Integer getIndError() {
		return indError;
	}
	
	/**
	 * Sets the ind error.
	 *
	 * @param indError the indError to set
	 */
	public void setIndError(Integer indError) {
		this.indError = indError;
	}
	
	/**
	 * Gets the end date.
	 *
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}
	
	/**
	 * Sets the end date.
	 *
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}
	
	/**
	 * Sets the mnemonic.
	 *
	 * @param mnemonic the mnemonic to set
	 */
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description.
	 *
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Gets the initial date.
	 *
	 * @return the initialDate
	 */
	public Date getInitialDate() {
		return initialDate;
	}
	
	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the initialDate to set
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	
	/**
	 * Gets the final date.
	 *
	 * @return the finalDate
	 */
	public Date getFinalDate() {
		return finalDate;
	}
	
	/**
	 * Sets the final date.
	 *
	 * @param finalDate the finalDate to set
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}
	
	/**
	 * Gets the execution date.
	 *
	 * @return the executionDate
	 */
	public Date getExecutionDate() {
		return executionDate;
	}
	
	/**
	 * Sets the execution date.
	 *
	 * @param executionDate the executionDate to set
	 */
	public void setExecutionDate(Date executionDate) {
		this.executionDate = executionDate;
	}
	
	/**
	 * Gets the state description.
	 *
	 * @return the stateDescription
	 */
	public String getStateDescription() {
		return stateDescription;
	}
	
	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the stateDescription to set
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the state to set
	 */
	public void setState(Integer state) {
		this.state = state;
	}
	
	/**
	 * Gets the report logger details.
	 *
	 * @return the reportLoggerDetails
	 */
	public List<ReportLoggerDetail> getReportLoggerDetails() {
		return reportLoggerDetails;
	}
	
	/**
	 * Sets the report logger details.
	 *
	 * @param reportLoggerDetails the reportLoggerDetails to set
	 */
	public void setReportLoggerDetails(List<ReportLoggerDetail> reportLoggerDetails) {
		this.reportLoggerDetails = reportLoggerDetails;
	}
	
	/**
	 * Gets the report logger files.
	 *
	 * @return the reportLoggerFiles
	 */
	public List<ReportLoggerFile> getReportLoggerFiles() {
		return reportLoggerFiles;
	}
	
	/**
	 * Sets the report logger files.
	 *
	 * @param reportLoggerFiles the reportLoggerFiles to set
	 */
	public void setReportLoggerFiles(List<ReportLoggerFile> reportLoggerFiles) {
		this.reportLoggerFiles = reportLoggerFiles;
	}
	
	/**
	 * Gets the error detail.
	 *
	 * @return the errorDetail
	 */
	public String getErrorDetail() {
		return errorDetail;
	}
	
	/**
	 * Sets the error detail.
	 *
	 * @param errorDetail the errorDetail to set
	 */
	public void setErrorDetail(String errorDetail) {
		this.errorDetail = errorDetail;
	}

	/**
	 * Gets the access user.
	 *
	 * @return the access user
	 */
	public String getAccessUser() {
		return accessUser;
	}

	/**
	 * Sets the access user.
	 *
	 * @param accessUser the new access user
	 */
	public void setAccessUser(String accessUser) {
		this.accessUser = accessUser;
	}

	/**
	 * Gets the module process.
	 *
	 * @return the module process
	 */
	public Long getModuleProcess() {
		return moduleProcess;
	}

	/**
	 * Sets the module process.
	 *
	 * @param moduleProcess the new module process
	 */
	public void setModuleProcess(Long moduleProcess) {
		this.moduleProcess = moduleProcess;
	}

	/**
	 * Gets the total file length.
	 *
	 * @return the total file length
	 */
	public String getTotalFileLength() {
		return totalFileLength;
	}

	/**
	 * Sets the total file length.
	 *
	 * @param totalFileLength the new total file length
	 */
	public void setTotalFileLength(String totalFileLength) {
		this.totalFileLength = totalFileLength;
	}

	/**
	 * Checks if is selected.
	 *
	 * @return the selected
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * Sets the selected.
	 *
	 * @param selected the selected to set
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	/**
	 * @return the reportFormat
	 */
	public Integer getReportFormat() {
		return reportFormat;
	}

	/**
	 * @param reportFormat the reportFormat to set
	 */
	public void setReportFormat(Integer reportFormat) {
		this.reportFormat = reportFormat;
	}
	
}
