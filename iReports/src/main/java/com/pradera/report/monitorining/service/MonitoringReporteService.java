package com.pradera.report.monitorining.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.Query;

import com.pradera.commons.reportes.audit.view.filter.MonitoringReportsFilter;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.report.Report;
import com.pradera.model.report.ReportLoggerUser;

/**
 * The class MonitoringReporteService
 * @Project : PraderaReportes
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.0
 */
@Stateless
public class MonitoringReporteService extends CrudDaoServiceBean{
	

	/** The log. */
	Logger log = Logger.getLogger(MonitoringReporteService.class.getName());
	
	
	/**
	 * to get systems as list
	 * @return List<Object[]>
	 */
	public List<Object[]> getSystemsList() {
		Map<String, Object> parameters = new HashMap<String, Object>();
//		parameters.put("state", SystemStateType.REGISTERED.getCode());
		return (List<Object[]>) findListByQueryString("Select S.idSystemPk,S.name From System S Where S.state=:state",parameters);
	}

	/**
	 * to get the Modules list based on system
	 * @param idSystem (Integer)
	 * @param localeId
	 * @return List<Object[]>
	 */
	public List<Object[]> getModulesList(Integer idSystem,Integer localeId) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select OS.idSystemOptionPk ,CI.catalogueName ");
		sbQuery.append(" From SystemOption OS");
		sbQuery.append(" Join OS.catalogueLanguages CI");		
		sbQuery.append(" Where OS.system.idSystemPk = :idSystemPrm and OS.state != :deletedStatePrm and OS.state != :rejectedStatePrm ");
		sbQuery.append(" and OS.parentSystemOption is null ");
		sbQuery.append(" and CI.language =:languageId ");
		sbQuery.append(" Order By OS.parentSystemOption.idSystemOptionPk, OS.orderOption ");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSystemPrm",idSystem);
//		query.setParameter("deletedStatePrm", OptionStateType.DELETED.getCode());
//		query.setParameter("rejectedStatePrm", OptionStateType.REJECTED.getCode());
		query.setParameter("languageId", localeId);
		List<Object[]> listOptions = (List<Object[]>) query.getResultList();
		 if(listOptions!= null && listOptions.size()>0){
		     return listOptions;
		 }
		 else{
		   return new ArrayList<Object[]>();
		 }
	}

	/**
	 * to get the subModules list based on system and Module
	 * @param idSystem
	 * @param idModule
	 * @param localeId
	 * @return List<Object[]>
	 */
	public List<Object[]> getSubModulesList(Integer idSystem,Integer idModule,Integer localeId) {
		List<Object[]> listSubModules = new ArrayList<Object[]>();
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select OS.idSystemOptionPk ");
		sbQuery.append(" From SystemOption OS");
		sbQuery.append(" Join OS.catalogueLanguages CI");		
		sbQuery.append(" Where OS.system.idSystemPk = :idSystemPrm and OS.state != :deletedStatePrm and OS.state != :rejectedStatePrm ");
		sbQuery.append(" and OS.parentSystemOption.idSystemOptionPk =:idModule ");
		sbQuery.append(" and CI.language =:languageId ");
		sbQuery.append(" Order By OS.parentSystemOption.idSystemOptionPk, OS.orderOption ");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSystemPrm",idSystem);
		query.setParameter("idModule",idModule);
//		query.setParameter("deletedStatePrm", OptionStateType.DELETED.getCode());
//		query.setParameter("rejectedStatePrm", OptionStateType.REJECTED.getCode());
		query.setParameter("languageId", localeId);
		List<Object> listObjs = (List<Object>) query.getResultList();
		String strIds = ""; 
		if(listObjs!= null && listObjs.size()>0){
		    for (Object object : listObjs) {
				strIds = strIds +object.toString()+",";
			}
		    if(strIds.contains(",")){
		    	strIds=strIds.substring(0, strIds.length()-1);
		    
			    StringBuilder sbQuery2 = new StringBuilder();
				sbQuery2.append(" Select distinct OS.parentSystemOption.idSystemOptionPk ");
				sbQuery2.append(" From SystemOption OS");
				sbQuery2.append(" Join OS.catalogueLanguages CI");		
				sbQuery2.append(" Where OS.system.idSystemPk = :idSystemPrm and OS.state != :deletedStatePrm and OS.state != :rejectedStatePrm ");
				sbQuery2.append(" and OS.parentSystemOption.idSystemOptionPk in ("+strIds+") ");
				sbQuery2.append(" and CI.language =:languageId ");
				sbQuery2.append(" Order By OS.parentSystemOption.idSystemOptionPk ");
				
				Query query2 = em.createQuery(sbQuery2.toString());
				query2.setParameter("idSystemPrm",idSystem);
//				query2.setParameter("deletedStatePrm", OptionStateType.DELETED.getCode());
//				query2.setParameter("rejectedStatePrm", OptionStateType.REJECTED.getCode());
				query2.setParameter("languageId", localeId);
				String strSubModulesIds = "";
				listObjs  = (List<Object>) query2.getResultList();
				for (Object object : listObjs) {
						strSubModulesIds = strSubModulesIds +object.toString()+",";
				}
				if(strSubModulesIds.contains(",")){
				   	strSubModulesIds=strSubModulesIds.substring(0, strSubModulesIds.length()-1);
				   	listSubModules = findListByQueryString("select cl.systemOption.idSystemOptionPk,cl.catalogueName from CatalogueLanguage cl  where cl.systemOption.idSystemOptionPk in ("+strSubModulesIds+") and cl.language="+localeId);
				}
		    }
			return listSubModules;
		 }
		 else{
		   return new ArrayList<Object[]>();
		 }
	}
	
	/**
	 * to get the subModules list based on system and Module
	 * @param idSystem
	 * @param idModule
	 * @param localeId
	 * @return List<Object[]>
	 */
	public List<Object[]> getOptionsList(Integer idSystem,Integer idModule, Integer subModule,Integer localeId) {
		List<Object[]> listoptions =null;
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select OS.idSystemOptionPk,CI.catalogueName ");
		sbQuery.append(" From SystemOption OS");
		sbQuery.append(" Join OS.catalogueLanguages CI");
		sbQuery.append(" Join OS.parentSystemOption SM");
		sbQuery.append(" Where OS.system.idSystemPk = :idSystemPrm and OS.state != :deletedStatePrm and OS.state != :rejectedStatePrm ");
		sbQuery.append(" and SM.parentSystemOption.idSystemOptionPk =:idModule ");
		sbQuery.append(" and CI.language =:languageId ");
		if(subModule != null && subModule > 0){
			sbQuery.append(" and SM.idSystemOptionPk =:subModule ");
		}
		sbQuery.append(" Order By OS.idSystemOptionPk, OS.orderOption ");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSystemPrm",idSystem);
		query.setParameter("idModule",idModule);
//		query.setParameter("deletedStatePrm", OptionStateType.DELETED.getCode());
//		query.setParameter("rejectedStatePrm", OptionStateType.REJECTED.getCode());
		if(subModule != null && subModule > 0){
			query.setParameter("subModule", subModule);
		}
		query.setParameter("languageId", localeId);
		listoptions = (List<Object[]>) query.getResultList();
		return listoptions;
	}
	/**
	 * to get all the users
	 * @return List<Object[]>
	 */
	public List<Object[]> getUsersList() {
		return (List<Object[]>) findListByQueryString("Select ua.loginUser,ua.fullName From UserAccount ua Order By ua.loginUser ");
	}
	
	/**
	 * to get reports based on search criteria 
	 * @param monitoringReportsFilter
	 * @param localeId
	 * @return List<ReportLoggerUser>
	 */
	public List<ReportLoggerUser> getSearchResultList(MonitoringReportsFilter monitoringReportsFilter,Integer localeId) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select  rlu from ReportLoggerUser rlu  join fetch rlu.reportLogger where ");
		List<ReportLoggerUser> listReports = new ArrayList<ReportLoggerUser>();
		StringBuffer sbIdOptions = new StringBuffer();
		String strIdOptions = null;
		Map<String, Object> mapParameters = new HashMap<String, Object>();
		List<Object[]> listOptions = null;
		/*if(monitoringReportsFilter.getIdOption()==0){
			listOptions = getOptionsList(monitoringReportsFilter.getIdSystem(), monitoringReportsFilter.getIdModule(),localeId);
		}else{
			listOptions = getOptionsList(monitoringReportsFilter.getIdSystem(), monitoringReportsFilter.getIdOption(),localeId);
		}*/
		/*listOptions = getOptionsList(monitoringReportsFilter.getIdSystem(), monitoringReportsFilter.getIdModule(), monitoringReportsFilter.getIdOption(),localeId);
		for (Object[] objects : listOptions) {
			sbIdOptions.append(objects[0].toString()+",");
		}
		if(sbIdOptions.toString().contains(",")){
			 strIdOptions= sbIdOptions.substring(0, sbIdOptions.length()-1);
		}*/
		List<Object> listIdReports =null;
		String strIdReports = ""; 
		//if(strIdOptions!=null){
			String strIdReportsQry = "select r.idReportPk from Report r where 1=1 ";
			if(monitoringReportsFilter.getIdReport()!=null && monitoringReportsFilter.getIdReport()!=0){
				strIdReportsQry = strIdReportsQry +" and r.idReportPk = "+monitoringReportsFilter.getIdReport();
			}
			if(monitoringReportsFilter.getReportName()!=null && monitoringReportsFilter.getReportName().trim().length()!=0){
				strIdReportsQry = strIdReportsQry +" and r.name like '"+monitoringReportsFilter.getReportName().trim()+"%' ";
			}
			listIdReports = findListByQueryString(strIdReportsQry);
			for (Object object : listIdReports) {
				strIdReports = strIdReports + object.toString() +",";
			}
			if(strIdReports.contains(",")){
				strIdReports = strIdReports.substring(0, strIdReports.length()-1);
				sbQuery.append(" rlu.reportLogger.report.idReportPk  in ("+strIdReports+") ");
				if(Validations.validateIsNotNullAndNotEmpty(monitoringReportsFilter.getUserName())){
					sbQuery.append(" and rlu.accessUser = '"+monitoringReportsFilter.getUserName().trim()+"' ");
				}
				if(Validations.validateIsNotNull(monitoringReportsFilter.getStartdate())){
					sbQuery.append(" and rlu.reportLogger.registryDate >= :startDate ");
					mapParameters.put("startDate", monitoringReportsFilter.getStartdate());
				}
				if(Validations.validateIsNotNull(monitoringReportsFilter.getEndDate())){
					sbQuery.append(" and rlu.reportLogger.registryDate < :endDate ");
					mapParameters.put("endDate", monitoringReportsFilter.getEndDate());
				}
			}else{
				return listReports;
			}
		//}else{
		//	return listReports;
	//	}
		sbQuery.append(" order by rlu.reportLogger.idReportLoggerPk desc");
		Query query = em.createQuery(sbQuery.toString());
		if(Validations.validateIsNotNull(monitoringReportsFilter.getStartdate())){
			query.setParameter("startDate", monitoringReportsFilter.getStartdate());
		}
		if(Validations.validateIsNotNull(monitoringReportsFilter.getEndDate())){
			Calendar cal = Calendar.getInstance();  
			cal.setTime(monitoringReportsFilter.getEndDate());  
			cal.add(Calendar.DAY_OF_YEAR, 1); 
			query.setParameter("endDate", cal.getTime());
		}
		listReports = query.getResultList();
		
		return listReports;
	}
	
	public void createReport(Report report){
		create(report);
		em.find(Report.class, 2L);
		StringBuilder sb = new StringBuilder();
		sb.append("Update Report  r.lastModiyApp=5");
		Query query = em.createQuery(sb.toString());
		query.executeUpdate();
	}
	
}
