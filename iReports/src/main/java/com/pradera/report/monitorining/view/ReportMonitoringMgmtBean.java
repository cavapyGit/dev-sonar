package com.pradera.report.monitorining.view;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.omnifaces.util.Faces;
import org.primefaces.model.StreamedContent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.security.model.type.LogoutMotiveType;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.IssuerSearcherTO;
import com.pradera.core.component.helperui.to.IssuerTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantClassType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.report.ReportLoggerFile;
import com.pradera.report.management.facade.ReportMgmtServiceFacade;
import com.pradera.report.monitorining.to.ReportLoggerTO;
import com.pradera.report.util.view.PropertiesConstants;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ReportMonitoringMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/09/2013
 */
@DepositaryWebBean
@LoggerCreateBean
public class ReportMonitoringMgmtBean extends GenericBaseBean {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The report mgmt service facade. */
	@EJB
	ReportMgmtServiceFacade reportMgmtServiceFacade;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	/** The parameters facade. */
	@EJB
	GeneralParametersFacade parametersFacade;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The country residence. */
	@Inject @Configurable
	private Integer countryResidence; 
		
	/** The reports data model. */
	private GenericDataModel<ReportLoggerTO> reportsDataModel;
	
	/** The report logger filter. */
	private ReportLoggerTO reportLoggerFilter;
	
	/** The search filter. */
	private Integer searchFilter;
	
	/** The selected report logger. */
	private ReportLoggerTO selectedReportLogger;
	
	/** The list users types. */
	private List<SelectItem> listUsersTypes;
	
	/** The list institutions. */
	private List<SelectItem> listInstitutions;
	
	/** The list state. */
	private List<SelectItem> listState;
	
	/** The list modules. */
	private List<BusinessProcess> listModules;
	
	/** The users accounts. */
	private List<UserAccountSession> usersAccounts;
	
	/** The user type. */
	private Integer userType;
	
	/** The institution type. */
	private Object institutionType;
	
	/** The user selected. */
	private String userSelected;
	
	/** The is user depositary. */
	private boolean isUserDepositary;	
	
	/** The use current date. */
	private Integer generateDate;
	
	/** The list folder FTP. */
	private List<ParameterTable> lstFolderFtp;
	
	/** The selected parameter table. */
	private ParameterTable selectedParameterTable;
	

	/**
	 * Inits the.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException{
		reportLoggerFilter = new ReportLoggerTO(getCurrentSystemDate(),getCurrentSystemDate());
		listUsersTypes = new ArrayList<>();
		for(InstitutionType institution : InstitutionType.list){
			listUsersTypes.add(new SelectItem(institution.getCode(), institution.getValue()));
		}
		usersAccounts= new ArrayList<>();
		listInstitutions = new ArrayList<>();
		
		ParameterTableTO parameterFilter = new ParameterTableTO();
		parameterFilter.setState(BooleanType.YES.getCode());
		parameterFilter.setMasterTableFk(MasterTableType.REPORT_LOGGER_STATE.getCode());
		listState = new ArrayList<>();
		for(ParameterTable parameter : parametersFacade.getListParameterTableServiceBean(parameterFilter)){
			listState.add(new SelectItem(parameter.getParameterTablePk(), parameter.getParameterName()));
		}
		
		listModules =  reportMgmtServiceFacade.getBusinessProcessLevel(Integer.valueOf(0));
		
		Holiday holidayFilter = new Holiday();
		holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		GeographicLocation countryFilter = new GeographicLocation();
		countryFilter.setIdGeographicLocationPk(countryResidence);
		holidayFilter.setGeographicLocation(countryFilter);		    
		allHolidays = CommonsUtilities.convertListDateToString(parametersFacade.getListHolidayDateServiceFacade(holidayFilter));	
		
		// Add send FTP folders settlements 
		validateUserSession();		
		lstFolderFtp = new ArrayList<ParameterTable>();
		loadFolderFpt();	
		generateDate = Integer.valueOf(0);
	}
	
	/**
	 * Load folder FTP.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadFolderFpt() throws ServiceException {
		ParameterTableTO parameterFTP = new ParameterTableTO();
		parameterFTP.setState(BooleanType.YES.getCode());
		parameterFTP.setMasterTableFk(MasterTableType.PARAMETERS_FOLDERS_FTP.getCode());
		lstFolderFtp = parametersFacade.getListParameterTableServiceBean(parameterFTP);	
	}
	
	/**
	 * Validate user session.
	 */
	private void validateUserSession() {		
		if (userInfo.getUserAccountSession().isDepositaryInstitution()) {
			isUserDepositary = Boolean.TRUE;
		} else {
			isUserDepositary = Boolean.FALSE;
		}
	}
	
	/**
	 * Search reports by filters.
	 */
	public void searchReportsByFilters(){
		if(StringUtils.isBlank(userSelected)){
			//if not selected user so using user login
			reportLoggerFilter.setAccessUser(userInfo.getUserAccountSession().getUserName());
		} else {
			reportLoggerFilter.setAccessUser(userSelected);
		}
		reportsDataModel = new GenericDataModel<ReportLoggerTO>(reportMgmtServiceFacade.findReportLoggers(reportLoggerFilter));
	}
	
	/**
	 * Clear filters.
	 */
	public void clearFilters(){
		reportLoggerFilter.setMnemonic(null);
		reportLoggerFilter.setDescription(null);
	}

	/**
	 * Clean all.
	 */
	public void cleanAll(){
		reportLoggerFilter = new ReportLoggerTO(getCurrentSystemDate(),getCurrentSystemDate());
		reportsDataModel = null;
		searchFilter=null;
		usersAccounts.clear();
		listInstitutions.clear();
		institutionType=null;
		userSelected=null;
		userType=null;
	}
	
	/**
	 * Display params.
	 *
	 * @param param the param
	 */
	public void displayParams(ReportLoggerTO param){
		selectedReportLogger = param;
		if(selectedReportLogger.getReportLoggerDetails()==null){
			selectedReportLogger.setReportLoggerDetails(reportMgmtServiceFacade.getReportLoggerDetails(param.getIdReportLoggerPk()));
		}
	}

	/**
	 * Display errors.
	 *
	 * @param param the param
	 */
	public void displayErrors(ReportLoggerTO param){
		selectedReportLogger = param;
	}
	
	/**
	 * Display files.
	 *
	 * @param param the param
	 */
	public void displayFiles(ReportLoggerTO param){
		selectedReportLogger = param;
		if (selectedReportLogger.getReportLoggerFiles() == null) {
			List<ReportLoggerFile> lstReportLoggerFile = reportMgmtServiceFacade.getReportLoggerFileInfo(param.getIdReportLoggerPk());
			for(ReportLoggerFile objReportLoggerFile: lstReportLoggerFile){
				if(Validations.validateIsNotNull(objReportLoggerFile.getDataReportLength())){
					objReportLoggerFile.setDataSizeDescription(objReportLoggerFile.getDataReportLength()+GeneralConstants.REPORT_SIZE_MB);
				}
			}
			selectedReportLogger.setReportLoggerFiles(lstReportLoggerFile);
		}
	}
	
	/**
	 * Gets the streamed content.
	 *
	 * @param file the file
	 * @return the streamed content
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public StreamedContent getStreamedContent(ReportLoggerFile file) throws IOException{
		ReportLoggerFile reportLoggerFile = reportMgmtServiceFacade.getReportLoggerFileConst(file.getIdLoggerFilePk());
			if(reportLoggerFile!= null){
				Path pathFile = Paths.get(reportLoggerFile.getNameTrace(),reportLoggerFile.getNameFile());
				if(Files.exists(pathFile)){
					try{
					Faces.sendFile(Files.readAllBytes(pathFile), reportLoggerFile.getNameFile(), true);
					} catch(IOException iox){
//						log.error(iox.getMessage());
					}
				}else{
					return getStreamedDataContent(file);
				}
			}
			return null;
	}
	
	/**
	 *  Get stream content, report 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public StreamedContent getStreamedDataContent(ReportLoggerFile file) throws IOException{
		byte[] fileContent = reportMgmtServiceFacade.getReportLoggerFileContent(file.getIdLoggerFilePk());
		return getStreamedContentFromFile(fileContent, null, file.getNameFile());
	}
	
	/**
	 * Select user types.
	 *
	 * @throws ServiceException the service exception
	 */
	public void selectUserTypes() throws ServiceException{
		if(userType!= null){
			InstitutionType institution = InstitutionType.get(userType);
			switch (institution) {
			case AFP:
				Participant partAfp = new Participant();
				partAfp.setAccountClass(ParticipantClassType.AFP.getCode());
				List<Participant> afpList = helperComponentFacade.findParticipantNativeQueryFacade(partAfp);
				listInstitutions.clear();
				for(Participant participant:afpList){
					listInstitutions.add(new SelectItem((Object)participant.getIdParticipantPk(), participant.getMnemonic()+"-"+participant.getIdParticipantPk()+"-"+participant.getDescription()));
				}
				usersAccounts.clear();
				break;
				
			case PARTICIPANT:	
				List<Participant> participantList = helperComponentFacade.findParticipantNativeQueryFacade(new Participant());
				listInstitutions.clear();
				for(Participant participant:participantList){
					listInstitutions.add(new SelectItem((Object)participant.getIdParticipantPk(), participant.getMnemonic()+"-"+participant.getIdParticipantPk()+"-"+participant.getDescription()));
				}
				usersAccounts.clear();
				break;
			case ISSUER:
				List<IssuerTO> issuerList = helperComponentFacade.findIssuerByHelperMnemonic(new IssuerSearcherTO());
				listInstitutions.clear();
				for(IssuerTO issuer : issuerList){
					listInstitutions.add(new SelectItem((Object)issuer.getIssuerCode(), issuer.getMnemonic()+"-"+issuer.getIssuerDesc(),issuer.getIssuerCode()));
				}
				usersAccounts.clear();
				break;
			default:
				usersAccounts = reportMgmtServiceFacade.getUsersFromInstitution(institution, null);	
				listInstitutions.clear();
				break;
			}
		} else {
			listInstitutions.clear();
			usersAccounts.clear();
		}
	}
	
	/**
	 * Select institution.
	 */
	public void selectInstitution(){
		if(institutionType!=null){
			InstitutionType institution = InstitutionType.get(userType);
			String issuerCode = null;
			Long participantCode = null;
			if(institution.equals(InstitutionType.ISSUER)){
				issuerCode = institutionType.toString();
				usersAccounts = reportMgmtServiceFacade.getUsersFromInstitution(institution, issuerCode);
			} else if(institution.equals(InstitutionType.PARTICIPANT) || institution.equals(InstitutionType.AFP)){
				participantCode = Long.valueOf(institutionType.toString());
				usersAccounts = reportMgmtServiceFacade.getUsersFromInstitution(institution, participantCode);
			}
			
		}else{
			usersAccounts.clear();
		}
	}
	
	/**
	 * Validation selection report.
	 *
	 * @param objReportLoggerTO the object report logger to
	 */
	public void validationSelectionReport(ReportLoggerTO objReportLoggerTO) {
		if(objReportLoggerTO != null) {
			if(Validations.validateIsNotNullAndNotEmpty(objReportLoggerTO.getIndError()) 
					&& GeneralConstants.ONE_VALUE_INTEGER.equals(objReportLoggerTO.getIndError())){
				objReportLoggerTO.setSelected(Boolean.FALSE);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.REPORT_IND_ERROR));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
		} 
	}
	
	/**
	 * Validation send report.
	 */
	public void validationSendReport() {				
		if(Validations.validateListIsNotNullAndNotEmpty(reportsDataModel.getDataList())){	
			int count = 0;
			for(ReportLoggerTO objReportLoggerTO : reportsDataModel.getDataList()){
				if(objReportLoggerTO.isSelected()){
					count++;
				}
			}
			if(count <= 0){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.OBJ_REPORT_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}			
			JSFUtilities.executeJavascriptFunction("PF('dlgwSendFtp').show()");			
		} 
	}
	
	/**
	 * Before action.
	 */
	public void beforeAction(){
		if(selectedParameterTable != null){
			Object[] bodyData = new Object[1];				
			bodyData[0] = selectedParameterTable.getDescription();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_APPROVE), 
					PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_REPORT_SENDER_FTP, bodyData));
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
		} else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.FOLDER_OBJ_PARAMETER_NOT_SELECTED));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
	}
	
	/**
	 * Process send reports FTP.
	 *
	 * @throws ServiceException the service exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void processSendReportsFtp() throws ServiceException, IOException {
		if(selectedParameterTable != null){
			boolean currentDate = true;
			if(GeneralConstants.ONE_VALUE_INTEGER.equals(generateDate)){
				currentDate = false;
			}
			boolean blSuccess = reportMgmtServiceFacade.processSendReportsFtp(reportsDataModel.getDataList(), selectedParameterTable, currentDate);
			if(blSuccess){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_REPORT_SENDER_FTP_SUCESS));
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			} else {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_OBJ_REPORT_NOT_SEND));
				JSFUtilities.showSimpleValidationDialog();
			}
			generateDate = Integer.valueOf(0);
			searchReportsByFilters();
		} else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.FOLDER_OBJ_PARAMETER_NOT_SELECTED));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
	}
	
	
	/**
	 * Gets the report logger filter.
	 *
	 * @return the reportLoggerFilter
	 */
	public ReportLoggerTO getReportLoggerFilter() {
		return reportLoggerFilter;
	}

	/**
	 * Sets the report logger filter.
	 *
	 * @param reportLoggerFilter the reportLoggerFilter to set
	 */
	public void setReportLoggerFilter(ReportLoggerTO reportLoggerFilter) {
		this.reportLoggerFilter = reportLoggerFilter;
	}

	/**
	 * Gets the search filter.
	 *
	 * @return the searchFilter
	 */
	public Integer getSearchFilter() {
		return searchFilter;
	}

	/**
	 * Sets the search filter.
	 *
	 * @param searchFilter the searchFilter to set
	 */
	public void setSearchFilter(Integer searchFilter) {
		this.searchFilter = searchFilter;
	}

	/**
	 * Gets the reports data model.
	 *
	 * @return the reportsDataModel
	 */
	public GenericDataModel<ReportLoggerTO> getReportsDataModel() {
		return reportsDataModel;
	}

	/**
	 * Sets the reports data model.
	 *
	 * @param reportsDataModel the reportsDataModel to set
	 */
	public void setReportsDataModel(GenericDataModel<ReportLoggerTO> reportsDataModel) {
		this.reportsDataModel = reportsDataModel;
	}
	
	/**
	 * Gets the selected report logger.
	 *
	 * @return the selectedReportLogger
	 */
	public ReportLoggerTO getSelectedReportLogger() {
		return selectedReportLogger;
	}

	/**
	 * Sets the selected report logger.
	 *
	 * @param selectedReportLogger the selectedReportLogger to set
	 */
	public void setSelectedReportLogger(ReportLoggerTO selectedReportLogger) {
		this.selectedReportLogger = selectedReportLogger;
	}

	/**
	 * Gets the list users types.
	 *
	 * @return the list users types
	 */
	public List<SelectItem> getListUsersTypes() {
		return listUsersTypes;
	}

	/**
	 * Sets the list users types.
	 *
	 * @param listUsersTypes the new list users types
	 */
	public void setListUsersTypes(List<SelectItem> listUsersTypes) {
		this.listUsersTypes = listUsersTypes;
	}

	/**
	 * Gets the list institutions.
	 *
	 * @return the list institutions
	 */
	public List<SelectItem> getListInstitutions() {
		return listInstitutions;
	}

	/**
	 * Sets the list institutions.
	 *
	 * @param listInstitutions the new list institutions
	 */
	public void setListInstitutions(List<SelectItem> listInstitutions) {
		this.listInstitutions = listInstitutions;
	}

	/**
	 * Gets the user type.
	 *
	 * @return the user type
	 */
	public Integer getUserType() {
		return userType;
	}

	/**
	 * Sets the user type.
	 *
	 * @param userType the new user type
	 */
	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	/**
	 * Gets the institution type.
	 *
	 * @return the institution type
	 */
	public Object getInstitutionType() {
		return institutionType;
	}

	/**
	 * Sets the institution type.
	 *
	 * @param institutionType the new institution type
	 */
	public void setInstitutionType(Object institutionType) {
		this.institutionType = institutionType;
	}

	/**
	 * Gets the users accounts.
	 *
	 * @return the users accounts
	 */
	public List<UserAccountSession> getUsersAccounts() {
		return usersAccounts;
	}

	/**
	 * Sets the users accounts.
	 *
	 * @param usersAccounts the new users accounts
	 */
	public void setUsersAccounts(List<UserAccountSession> usersAccounts) {
		this.usersAccounts = usersAccounts;
	}

	/**
	 * Gets the user selected.
	 *
	 * @return the user selected
	 */
	public String getUserSelected() {
		return userSelected;
	}

	/**
	 * Sets the user selected.
	 *
	 * @param userSelected the new user selected
	 */
	public void setUserSelected(String userSelected) {
		this.userSelected = userSelected;
	}

	/**
	 * Gets the list state.
	 *
	 * @return the list state
	 */
	public List<SelectItem> getListState() {
		return listState;
	}

	/**
	 * Sets the list state.
	 *
	 * @param listState the new list state
	 */
	public void setListState(List<SelectItem> listState) {
		this.listState = listState;
	}

	/**
	 * Gets the list modules.
	 *
	 * @return the list modules
	 */
	public List<BusinessProcess> getListModules() {
		return listModules;
	}

	/**
	 * Sets the list modules.
	 *
	 * @param listModules the new list modules
	 */
	public void setListModules(List<BusinessProcess> listModules) {
		this.listModules = listModules;
	}
	
	/**
	 * Idle time logout user session.
	 */
	public void sessionLogout(){
		try{
			JSFUtilities.setHttpSessionAttribute(GeneralConstants.LOGOUT_MOTIVE, LogoutMotiveType.EXPIREDSESSION);
			JSFUtilities.killSession(); 
			JSFUtilities.showMessageOnDialog(null, null,PropertiesConstants.IDLE_TIME_EXPIRED, null);
			JSFUtilities.showComponent(":idCnfDialogKillSession");
		} catch (Exception ex) {
				excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Checks if is user depositary.
	 *
	 * @return the isUserDepositary
	 */
	public boolean isUserDepositary() {
		return isUserDepositary;
	}

	/**
	 * Sets the user depositary.
	 *
	 * @param isUserDepositary the isUserDepositary to set
	 */
	public void setUserDepositary(boolean isUserDepositary) {
		this.isUserDepositary = isUserDepositary;
	}

	/**
	 * Gets the lst folder ftp.
	 *
	 * @return the lstFolderFtp
	 */
	public List<ParameterTable> getLstFolderFtp() {
		return lstFolderFtp;
	}

	/**
	 * Sets the lst folder ftp.
	 *
	 * @param lstFolderFtp the lstFolderFtp to set
	 */
	public void setLstFolderFtp(List<ParameterTable> lstFolderFtp) {
		this.lstFolderFtp = lstFolderFtp;
	}

	/**
	 * Gets the selected parameter table.
	 *
	 * @return the selectedParameterTable
	 */
	public ParameterTable getSelectedParameterTable() {
		return selectedParameterTable;
	}

	/**
	 * Sets the selected parameter table.
	 *
	 * @param selectedParameterTable the selectedParameterTable to set
	 */
	public void setSelectedParameterTable(ParameterTable selectedParameterTable) {
		this.selectedParameterTable = selectedParameterTable;
	}

	/**
	 * @return the generateDate
	 */
	public Integer getGenerateDate() {
		return generateDate;
	}

	/**
	 * @param generateDate the generateDate to set
	 */
	public void setGenerateDate(Integer generateDate) {
		this.generateDate = generateDate;
	}
	
}
