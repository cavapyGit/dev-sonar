package com.pradera.report;

import java.io.Serializable;



/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ReportContext.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/07/2013
 */
public class ReportContext implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1211565009572598334L;
	
	/** The template report url. */
	private String templateReportUrl;
	
	/** The process logger id. */
	private Long processLoggerId;
	
	/** The report implementation bean. */
	private String reportImplementationBean;
	
	/** The xml data source. */
	private String xmlDataSource;
	
	/** The report type. */
	private Integer reportType;
	
	/** The report query. */
	private String reportQuery;
	
	/** The sub report query. */
	private String subReportQuery;
	
	/** The priority. */
	private Integer priority;
	/**
	 * Gets the template report url.
	 *
	 * @return the template report url
	 */
	public String getTemplateReportUrl() {
		return templateReportUrl;
	}


	/**
	 * Sets the template report url.
	 *
	 * @param templateReportUrl the new template report url
	 */
	public void setTemplateReportUrl(String templateReportUrl) {
		this.templateReportUrl = templateReportUrl;
	}


	/**
	 * Gets the process logger id.
	 *
	 * @return the process logger id
	 */
	public Long getProcessLoggerId() {
		return processLoggerId;
	}


	/**
	 * Sets the process logger id.
	 *
	 * @param processLoggerId the new process logger id
	 */
	public void setProcessLoggerId(Long processLoggerId) {
		this.processLoggerId = processLoggerId;
	}


	/**
	 * Gets the report implementation bean.
	 *
	 * @return the report implementation bean
	 */
	public String getReportImplementationBean() {
		return reportImplementationBean;
	}


	/**
	 * Sets the report implementation bean.
	 *
	 * @param reportImplementationBean the new report implementation bean
	 */
	public void setReportImplementationBean(String reportImplementationBean) {
		this.reportImplementationBean = reportImplementationBean;
	}


	/**
	 * Gets the xml data source.
	 *
	 * @return the xml data source
	 */
	public String getXmlDataSource() {
		return xmlDataSource;
	}


	/**
	 * Sets the xml data source.
	 *
	 * @param xmlDataSource the new xml data source
	 */
	public void setXmlDataSource(String xmlDataSource) {
		this.xmlDataSource = xmlDataSource;
	}


	/**
	 * Instantiates a new report context.
	 */
	public ReportContext() {
		// TODO Auto-generated constructor stub
	}


	/**
	 * Gets the priority.
	 *
	 * @return the priority
	 */
	public Integer getPriority() {
		return priority;
	}


	/**
	 * Sets the priority.
	 *
	 * @param priority the new priority
	 */
	public void setPriority(Integer priority) {
		this.priority = priority;
	}


	/**
	 * Gets the report type.
	 *
	 * @return the report type
	 */
	public Integer getReportType() {
		return reportType;
	}


	/**
	 * Sets the report type.
	 *
	 * @param reportType the new report type
	 */
	public void setReportType(Integer reportType) {
		this.reportType = reportType;
	}


	/**
	 * Gets the report query.
	 *
	 * @return the report query
	 */
	public String getReportQuery() {
		return reportQuery;
	}


	/**
	 * Sets the report query.
	 *
	 * @param reportQuery the new report query
	 */
	public void setReportQuery(String reportQuery) {
		this.reportQuery = reportQuery;
	}


	/**
	 * @return the subReportQuery
	 */
	public String getSubReportQuery() {
		return subReportQuery;
	}


	/**
	 * @param subReportQuery the subReportQuery to set
	 */
	public void setSubReportQuery(String subReportQuery) {
		this.subReportQuery = subReportQuery;
	}

}
