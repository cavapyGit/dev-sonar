package com.pradera.report.batch;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.report.generation.facade.ReportGenerationLocalServiceFacade;

@BatchProcess(name = "DailySummaryReportConciliationBatch")
@RequestScoped
public class DailySummaryReportConciliationBatch implements JobExecution {
	
	/** The report user. */
	private ReportUser reportUser;
	
	/** The report generation facade. */
	@EJB
	private ReportGenerationLocalServiceFacade reportGenerationFacade;
	
	@Inject
	private UserInfo userInfo;	
	
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void startJob(ProcessLogger arg0) {
		executeDailySummaryReportConciliation();
	}
	@LoggerAuditWeb
	private void executeDailySummaryReportConciliation() {
		reportUser.setUserName(userInfo.getUserAccountSession().getUserName());
		reportUser.setReportFormat(ReportFormatType.PDF.getCode());
		reportUser.setShowNotification(BooleanType.NO.getCode());
		Map<String,String> parameters = getParametersDailySummaryReportConciliation();
		Map<String,String> parameterDetails = new HashMap<String, String>();
		try {
			reportGenerationFacade.saveReportExecution(194L,parameters, parameterDetails,reportUser);
		} catch (ServiceException e) {
			System.out.println("Error al ejecutar el reporte de Resumen de conciliacion Diaria mediante Batch");
			e.printStackTrace();
		}
	}
	public Map<String,String> getParametersDailySummaryReportConciliation(){
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("date", CommonsUtilities.convertDatetoString(new Date()));
		return parameters;
	}
}
