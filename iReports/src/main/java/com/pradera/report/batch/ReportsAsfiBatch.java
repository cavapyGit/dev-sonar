package com.pradera.report.batch;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.report.generation.executor.settlement.service.SettlementReportServiceBean;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;
import com.pradera.report.generation.facade.ReportGenerationLocalServiceFacade;
import com.pradera.report.generation.service.ReportUtilServiceBean;

@BatchProcess(name = "ReportsAsfiBatch")
@RequestScoped
public class ReportsAsfiBatch implements JobExecution {
	
	@Inject
	ReportUtilServiceBean reportUtilServiceBean;
	@EJB
	SettlementReportServiceBean settlementReportServiceBean;
	
	@Inject
	ReportGenerationLocalServiceFacade reportLocalServiceFacade;
	@Override
	
	public void startJob(ProcessLogger processLogger) {		
		
		//Reporte solo en formato TXT
				GenericsFiltersTO gfto = new GenericsFiltersTO();
				//Filtro de Fecha
				gfto.setFinalDt(CommonsUtilities.convertDatetoString(CommonsUtilities.addDate(CommonsUtilities.currentDate(), -1)));
				try {
					//Metodo para generar el archivo
					Map<String,String> parameters = new HashMap<String, String>();
					//Generamos los datos del reporte 4
					ReportLogger reportLogger4 = reportLocalServiceFacade.getReportLoggerAsfi(285L, parameters, null);
					generateReportTXT4(gfto,reportLogger4);
					
					//Generamos los datos del reporte 5
					//Modificamos la fecha para recuperar los datos de cartera
					if(CommonsUtilities.truncateDateTime(CommonsUtilities.convertStringtoDate(gfto.getFinalDt()))
							.equals(CommonsUtilities.addDate(CommonsUtilities.currentDate(), -1))){
						gfto.setFinalDt(CommonsUtilities.convertDatetoString(CommonsUtilities.addDate(CommonsUtilities.convertStringtoDate(gfto.getFinalDt()), +1)));
					    gfto.setInitialDt(CommonsUtilities.convertDatetoString(CommonsUtilities.addDate(CommonsUtilities.convertStringtoDate(gfto.getFinalDt()), -1)));
					}else{
						gfto.setInitialDt(CommonsUtilities.convertDatetoString(CommonsUtilities.addDate(CommonsUtilities.convertStringtoDate(gfto.getFinalDt()), -1)));
					}
					
					gfto.setDate(CommonsUtilities.convertDatetoString(CommonsUtilities.addDate(CommonsUtilities.currentDate(), -1)));
					
					ReportLogger reportLogger5 = reportLocalServiceFacade.getReportLoggerAsfi(286L, parameters, null);
					generateReportTXT5(gfto,reportLogger5);
					
				} catch (ServiceException e) {
					e.printStackTrace();
				}
	}

	
	/**
	 * Metodo para la generacion en TXT de los datos del reporte 4 ASFI
	 * @param gfto
	 * @throws ServiceException
	 */
	private void generateReportTXT4(GenericsFiltersTO gfto, ReportLogger reportLogger) throws ServiceException{
	//private void generateReportTXT(GenericsFiltersTO gfto) throws ServiceException{	
		
		//Declaramos variables
		StringBuilder sbTxtFormat = new StringBuilder();
		
		//Generamos el nombre del archivo
		String fileName = getNameAccordingFund4(gfto)+"EDB";
		//Query con la informacion que saldra en el reporte
		List <Object[]> listToRep = settlementReportServiceBean.getListSecuritiesSettlementss(gfto);
		
		//Si el reporte tiene datos 
		if(listToRep.size() > GeneralConstants.ZERO_VALUE_INTEGER){
			for (Object [] unitString : listToRep){
				
				//Concatenamos todo en una sola cadena
				sbTxtFormat.append(GeneralConstants.STR_COMILLA);
				sbTxtFormat.append(unitString[0].toString());
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				sbTxtFormat.append(unitString[1].toString());
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				sbTxtFormat.append(unitString[2].toString());
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				sbTxtFormat.append(unitString[3].toString());
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				if(!String.valueOf(unitString[5]).equals(GeneralConstants.ZERO_VALUE_STRING)){ sbTxtFormat.append(String.valueOf(unitString[5])); }else{ sbTxtFormat.append(""); }
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				if(!String.valueOf(unitString[6]).equals(GeneralConstants.ZERO_VALUE_STRING)){ sbTxtFormat.append(String.valueOf(unitString[6])); }else{ sbTxtFormat.append(""); }
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				if(!String.valueOf(unitString[7]).equals(GeneralConstants.ZERO_VALUE_STRING)){ sbTxtFormat.append(String.valueOf(unitString[7])); }else{ sbTxtFormat.append(""); }
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				if(!String.valueOf(unitString[8]).equals(GeneralConstants.ZERO_VALUE_STRING)){ sbTxtFormat.append(String.valueOf(unitString[8])); }else{ sbTxtFormat.append(""); }
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				if(!String.valueOf(unitString[12]).equals(GeneralConstants.ZERO_VALUE_STRING)){ sbTxtFormat.append(String.valueOf(unitString[12])); }else{ sbTxtFormat.append(""); }
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				if(!String.valueOf(unitString[9]).equals(GeneralConstants.ZERO_VALUE_STRING)){ sbTxtFormat.append(String.valueOf(unitString[9])); }else{ sbTxtFormat.append(""); }
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				if(!String.valueOf(unitString[10]).equals(GeneralConstants.ZERO_VALUE_STRING)){ sbTxtFormat.append(String.valueOf(unitString[10])); }else{ sbTxtFormat.append(""); }
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				if(!String.valueOf(unitString[11]).equals(GeneralConstants.ZERO_VALUE_STRING)){ sbTxtFormat.append(String.valueOf(unitString[11])); }else{ sbTxtFormat.append(""); }
				sbTxtFormat.append(GeneralConstants.STR_COMILLA);
				
				//Salto de linea
				sbTxtFormat.append(GeneralConstants.STR_END_LINE_TXT);
			}
			//Generamos el archivo 
			reportUtilServiceBean.saveCustomReportFileAsfi(fileName, ReportFormatType.TXT.getCode(), sbTxtFormat.toString().getBytes(), reportLogger);
		}else{
			//Si el reporte no tiene datos
			sbTxtFormat.append(GeneralConstants.STR_COMILLA + "SIN DATOS" + GeneralConstants.STR_COMILLA);
			reportUtilServiceBean.saveCustomReportFileAsfi(fileName, ReportFormatType.TXT.getCode(), sbTxtFormat.toString().getBytes(), reportLogger);
		}
	}
	
	/**
	 * Metodo para la generacion en TXT de los datos del reporte 5 ASFI
	 * @param gfto
	 * @throws ServiceException
	 */
	private void generateReportTXT5(GenericsFiltersTO gfto, ReportLogger reportLogger){
		
		//Declaramos variables
		StringBuilder sbTxtFormat = new StringBuilder();     
		//Generamos el nombre del archivo
		String fileName = getNameAccordingFund5(gfto)+"EDB";
		//Query con la informacion que saldra en el reporte
		List <Object[]> listToRep = settlementReportServiceBean.getListCompensationSettlements(gfto);
		
		//Si el reporte tiene datos 
		if(listToRep.size() > GeneralConstants.ZERO_VALUE_INTEGER){
			for (Object [] unitString : listToRep){
						
				//Concatenamos todo en una sola cadena
				sbTxtFormat.append(GeneralConstants.STR_COMILLA);
				sbTxtFormat.append(unitString[0].toString());
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				sbTxtFormat.append(unitString[1].toString());
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				sbTxtFormat.append(unitString[2].toString());
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				sbTxtFormat.append(unitString[3].toString());
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				sbTxtFormat.append(unitString[4].toString());
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				if(!String.valueOf(unitString[7]).equals(GeneralConstants.ZERO_VALUE_STRING)){ sbTxtFormat.append(String.valueOf(unitString[7])); }else{ sbTxtFormat.append(""); }
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				if(!String.valueOf(unitString[6]).equals(GeneralConstants.ZERO_VALUE_STRING)){ sbTxtFormat.append(String.valueOf(unitString[6])); }else{ sbTxtFormat.append(""); }
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				//if(!String.valueOf(unitString[8]).equals(GeneralConstants.ZERO_VALUE_STRING)){ sbTxtFormat.append(String.valueOf(unitString[8])); }else{ sbTxtFormat.append(""); }
				//sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				if(!String.valueOf(unitString[9]).equals(GeneralConstants.ZERO_VALUE_STRING)){ sbTxtFormat.append(String.valueOf(unitString[9])); }else{ sbTxtFormat.append(""); }
				sbTxtFormat.append(GeneralConstants.STR_COMILLA);
						
				//Salto de linea
				sbTxtFormat.append(GeneralConstants.STR_END_LINE_TXT);
			}
			//Generamos el archivo 
			reportUtilServiceBean.saveCustomReportFileAsfi(fileName, ReportFormatType.TXT.getCode(), sbTxtFormat.toString().getBytes(), reportLogger);
		}else{
			//Si el reporte no tiene datos
			sbTxtFormat.append(GeneralConstants.STR_COMILLA + "SIN DATOS" + GeneralConstants.STR_COMILLA);
			reportUtilServiceBean.saveCustomReportFileAsfi(fileName, ReportFormatType.TXT.getCode(), sbTxtFormat.toString().getBytes(), reportLogger);
		}
	}
	
	
	/**
	 * Metodo para generar el nombre del archivo
	 * @param gfto
	 * @return
	 */
	private String getNameAccordingFund4(GenericsFiltersTO gfto){
		String name="";
		String firstWord="M";
		String strDate= gfto.getFinalDt();
		String strDay = strDate.substring(0,2);
		String strMonth = strDate.substring(3,5);
		String strYear = strDate.substring(8,10);
		String lastWord="K";
		String reportCode="";

		name=firstWord;
		name+=strYear+strMonth+strDay;
		name+=lastWord;
		name+=GeneralConstants.DOT+reportCode;
		return name;
	}
	
	/**
	 * Metodo para generar el nombre del archivo
	 * @param gfto
	 * @return
	 */
	private String getNameAccordingFund5(GenericsFiltersTO gfto){
		String name="";
		String firstWord="M";
		String strDate= gfto.getFinalDt();
		String strDay = strDate.substring(0,2);
		String strMonth = strDate.substring(3,5);
		String strYear = strDate.substring(8,10);
		String lastWord="L";
		String reportCode="";

		name=firstWord;
		name+=strYear+strMonth+strDay;
		name+=lastWord;
		name+=GeneralConstants.DOT+reportCode;
		return name;
	}
	
//	public ReportContext createContext(ReportLogger reportLogger) {
//		ReportContext reportContext = null;
//		if (reportLogger != null) {
//			reportContext = new ReportContext();
//			reportContext.setProcessLoggerId(reportLogger.getIdReportLoggerPk());
//			
//			reportContext.setPriority(reportLogger.getReport().getIndPriority());
//			
//			reportContext.setReportImplementationBean(reportLogger.getReport().getReportBeanName());
//			
//			reportContext.setTemplateReportUrl(reportLogger.getReport().getTemplateName());
//			
//			reportContext.setXmlDataSource(reportLogger.getReport().getDatasourceXml());
//			reportContext.setReportQuery(reportLogger.getReport().getQueryReport());
//			reportContext.setSubReportQuery(reportLogger.getReport().getSubQueryReport());
//			reportContext.setReportType(reportLogger.getReport().getReportType());
//		}
//		return reportContext;
//	}


	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}
}
