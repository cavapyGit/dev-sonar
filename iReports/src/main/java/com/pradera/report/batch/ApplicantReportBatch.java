package com.pradera.report.batch;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
import com.pradera.model.report.ReportLogger;
import com.pradera.report.ReportContext;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.facade.ReportGenerationLocalServiceFacade;
import com.pradera.report.generation.service.ReportManageServiceBean;
import com.pradera.report.generation.service.ReportUtilServiceBean;
/**
* 
* <ul><li>Copyright EDV 2020.</li></ul> 
* @author RCHIARA.
* 
*
*/
@BatchProcess(name = "ApplicantReportBatch")
@RequestScoped
public class ApplicantReportBatch implements JobExecution {
	
	@Inject @ReportProcess(name="ApplicantReportPortfolio")
	GenericReport genericReport;
	@Inject
	ReportUtilServiceBean reportUtilServiceBean;
	@Inject
	ReportManageServiceBean reportManageServiceBean;
	@Inject
	ReportGenerationLocalServiceFacade reportLocalServiceFacade;	
	
	@Override
	public void startJob(ProcessLogger processLogger){
		List<ProcessLoggerDetail> processLoggerDetails = processLogger.getProcessLoggerDetails();
		Long idHolderPk = new Long(0);
		Long reportId = new Long(0);
		Long idApplicantConsultRequest = new Long(0);
		String holderDesc = "";
		String username = "";
		for(ProcessLoggerDetail processLoggerDetail: processLoggerDetails){
			if(processLoggerDetail.getParameterName().equals("idHolderPk")){
				idHolderPk = Long.valueOf((processLoggerDetail.getParameterValue()));
			}else if (processLoggerDetail.getParameterName().equals("idApplicantConsultRequest")) {
				idApplicantConsultRequest = Long.valueOf(processLoggerDetail.getParameterValue());
			}else if(processLoggerDetail.getParameterName().equals("holderDesc")){
				holderDesc = processLoggerDetail.getParameterValue();
			}else if(processLoggerDetail.getParameterName().equals("idReportPk")){
				reportId = Long.valueOf((processLoggerDetail.getParameterValue()));
			}else if(processLoggerDetail.getParameterName().equals("userNameLogged")){
				username = processLoggerDetail.getParameterValue();
			}
		}
		try {
			executeReport(idHolderPk, idApplicantConsultRequest, holderDesc, reportId, username);
		} catch (ServiceException e) {
			e.printStackTrace();
		}		
	}
	
	public void executeReport(Long idHolderPk, Long idApplicantConsultRequest, String holderDesc, 
			Long reportId, String username) throws ServiceException{		
		Map<String,String> parameters = new HashMap<String, String>();
		parameters.put("cui_holder", idHolderPk.toString() );
		parameters.put("cui_description", holderDesc);
		parameters.put("portafolio_type", IssuanceType.DEMATERIALIZED.getCode().toString());
		parameters.put("date", CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));
				
		ReportLogger reportLogger = reportLocalServiceFacade.getReportLogger(reportId, parameters, null);
		reportLogger.setPhysicalName("EDVRPT16_"+idApplicantConsultRequest.toString()+idHolderPk.toString());
		
	    ReportContext reportContext = createContext(reportLogger);
	    genericReport.setReportContext(reportContext);
	    genericReport.setReportLogger(reportLogger);
	    genericReport.runOutsideThread();	    
	}
	
	public ReportContext createContext(ReportLogger reportLogger) {		
		ReportContext reportContext = null;
		if (reportLogger != null) {
			reportContext = new ReportContext();			
			reportContext.setProcessLoggerId(reportLogger.getIdReportLoggerPk());			
			reportContext.setPriority(reportLogger.getReport().getIndPriority());			
			reportContext.setReportImplementationBean(reportLogger.getReport().getReportBeanName());			
			reportContext.setTemplateReportUrl(reportLogger.getReport().getTemplateName());			
			reportContext.setXmlDataSource(reportLogger.getReport().getDatasourceXml());
			reportContext.setReportQuery(reportLogger.getReport().getQueryReport());
			reportContext.setSubReportQuery(reportLogger.getReport().getSubQueryReport());
			reportContext.setReportType(reportLogger.getReport().getReportType());
		}
		return reportContext;		
	}	

	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}	
}
