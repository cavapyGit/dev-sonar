package com.pradera.report.batch;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;

import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.billing.type.EntityCollectionType;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
import com.pradera.report.generation.facade.ReportGenerationLocalServiceFacade;


@BatchProcess(name = "GenerateDataReportAsfiBatch")
@RequestScoped
public class GenerateDataReportAsfiBatch implements JobExecution {
	
	
	@EJB
	private ReportGenerationLocalServiceFacade reportGenerationFacade;

	@Override
	public void startJob(ProcessLogger processLogger) {
		String strCutDate = CommonsUtilities.convertDateToString(CommonsUtilities.currentDate(), "dd/MM/yyyy");
		List<ProcessLoggerDetail> processLoggerDetails = processLogger.getProcessLoggerDetails();
		if (Validations.validateListIsNotNullAndNotEmpty(processLoggerDetails)){
			for(ProcessLoggerDetail processLoggerDetail: processLoggerDetails){
				if(processLoggerDetail.getParameterName().equals("cutDate"))
					strCutDate = processLoggerDetail.getParameterValue();				
			}
		}
		//Enviamos el reporte de caracteristicas del titular
		ReportUser reportUser = new ReportUser();
		reportUser.setUserName(EntityCollectionType.MANAGER.getValue());
		reportUser.setShowNotification(BooleanType.YES.getCode());
		Map<String,String> parameters = new HashMap<String, String>();
		parameters.put("date",strCutDate);
		parameters.put("automatic", "true");
		parameters.put("reportFormats", "1391");
		
		Map<String,String> parameterDetails = new HashMap<String, String>();
		reportUser.setShowNotification(BooleanType.YES.getCode());
		try {
			/**Reporte de caracteristicas del valor ARCHIVO B*/
			//SecurityCharacteristicReport  77
			reportGenerationFacade.saveReportExecution(ReportIdType.PORTAFOLIO_CHARACTERISTICS_SECURITY.getCode(),parameters, parameterDetails,reportUser);
			
			/**Carcateriticas del cupon ARCHIVO E*/
			//CouponsCharacteristicsReport 10
			Thread.sleep(240000);
			reportGenerationFacade.saveReportExecution(ReportIdType.PORTAFOLIO_CHARACTERISTICS_CUOPONS.getCode() ,parameters, parameterDetails,reportUser);
			
			/**Detalle de custodi SAFI FIC ARCHIVO U*/
			//CustodyDetailAgbAndSafiAndFicReport 8
			Thread.sleep(240000);
			Map<String,String> parameters8 = new HashMap<String, String>();
			parameters8.put("date",strCutDate);
			parameters8.put("automatic", "true");
			parameters8.put("report_format", "1391");
			parameters8.put("reportFormats", "1391");
			reportGenerationFacade.saveReportExecution(ReportIdType.PORTAFOLIO_CUSTODY_AGB_SAFI_FIC.getCode(),parameters8, parameterDetails,reportUser);
			/**Carcateriticas del cupon desprendido ARCHIVO AF*/
			Thread.sleep(240000);
			//DetachedCouponsCharReport 70
			Map<String,String> parameters70 = new HashMap<String, String>();
			parameters70.put("registry_date",strCutDate);
			parameters70.put("automatic", "true");
			parameters70.put("reportFormats", "1391");
			Map<String,String> parameterDetails70 = new HashMap<String, String>();
			reportGenerationFacade.saveReportExecution(ReportIdType.PORTAFOLIO_CHARACTERISTICS_CUOPONS_SUB.getCode(),parameters70, parameterDetails70,reportUser);
			Thread.sleep(600000);
		} catch (ServiceException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}

}
