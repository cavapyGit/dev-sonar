package com.pradera.report.batch;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
import com.pradera.model.report.ReportLogger;
import com.pradera.report.ReportContext;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.account.service.AccountReportServiceBean;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.facade.ReportGenerationLocalServiceFacade;
import com.pradera.report.generation.service.ReportManageServiceBean;
import com.pradera.report.generation.service.ReportUtilServiceBean;

@BatchProcess(name = "ModifyingOfDataCuiBatch")
@RequestScoped
public class ModifyingOfDataCuiBatch implements JobExecution {
	
	@Inject @ReportProcess(name="ModifyingOfDataCuiReport")
	GenericReport genericReport;
	@Inject
	ReportUtilServiceBean reportUtilServiceBean;
	@Inject
	ReportManageServiceBean reportManageServiceBean;
	@Inject
	ReportGenerationLocalServiceFacade reportLocalServiceFacade;
	@EJB
	AccountReportServiceBean accountReportServiceBean;
	@Override
	public void startJob(ProcessLogger processLogger) {		
		List<ProcessLoggerDetail> processLoggerDetails = processLogger.getProcessLoggerDetails();
		Long holderRequestId = new Long(0);
		Long reportId = new Long(0);
		String username = "";
		for(ProcessLoggerDetail processLoggerDetail: processLoggerDetails){
			/*** GETTING PARAMETERS ***/
			if(processLoggerDetail.getParameterName().equals("idHolderRequestPk")){
				holderRequestId = Long.valueOf((processLoggerDetail.getParameterValue()));
			}else if(processLoggerDetail.getParameterName().equals("idReportPk")){
				reportId = Long.valueOf((processLoggerDetail.getParameterValue()));
			}else if(processLoggerDetail.getParameterName().equals("userNameLogged")){
				username = processLoggerDetail.getParameterValue();
			}
		}
		try {
			executeReport(holderRequestId, reportId, username);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

	
	public void executeReport(Long holderRequestId, Long reportId, String username) throws ServiceException{		
		Map<String,String> parameters = new HashMap<String, String>();
		parameters.put("idHolderRequestPk", holderRequestId.toString());
				
		ReportLogger reportLogger = reportLocalServiceFacade.getReportLogger(reportId, parameters, null);
		reportLogger.setPhysicalName("CVPRPT119"+ holderRequestId.toString());
		
	    ReportContext reportContext = createContext(reportLogger);
	    genericReport.setReportContext(reportContext);
	    genericReport.setReportLogger(reportLogger);
	    genericReport.runOutsideThread();
	} 
	
	
	public ReportContext createContext(ReportLogger reportLogger) {
		ReportContext reportContext = null;
		if (reportLogger != null) {
			reportContext = new ReportContext();
			reportContext.setProcessLoggerId(reportLogger.getIdReportLoggerPk());
			
			reportContext.setPriority(reportLogger.getReport().getIndPriority());
			
			reportContext.setReportImplementationBean(reportLogger.getReport().getReportBeanName());
			
			reportContext.setTemplateReportUrl(reportLogger.getReport().getTemplateName());
			
			reportContext.setXmlDataSource(reportLogger.getReport().getDatasourceXml());
			reportContext.setReportQuery(reportLogger.getReport().getQueryReport());
			reportContext.setSubReportQuery(reportLogger.getReport().getSubQueryReport());
			reportContext.setReportType(reportLogger.getReport().getReportType());
		}
		return reportContext;
	}

	
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}
