package com.pradera.report.batch;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerUser;
import com.pradera.report.ReportContext;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.billing.service.BillingServiceReportServiceBean;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.facade.ReportGenerationLocalServiceFacade;
import com.pradera.report.generation.service.ReportManageServiceBean;
import com.pradera.report.generation.service.ReportUtilServiceBean;

@BatchProcess(name = "ConsolidatedCollectionByEntityReportBatch")
@RequestScoped
public class ConsolidatedCollectionByEntityReportBatch implements JobExecution {
	
	@Inject @ReportProcess(name="ConsolidatedCollectionByEntityReport")
	GenericReport genericReport;
	@Inject
	ReportUtilServiceBean reportUtilServiceBean;
	@Inject
	ReportManageServiceBean reportManageServiceBean;
	@Inject
	ReportGenerationLocalServiceFacade reportLocalServiceFacade;
	@EJB
	BillingServiceReportServiceBean billingServiceReportServiceBean;
	
	
	@Override
	public void startJob(ProcessLogger processLogger) {		
		List<ProcessLoggerDetail> processLoggerDetails = processLogger.getProcessLoggerDetails();
		Long reportId = new Long(0);
		String username = "";
		Map<String,String> parametersReport = new HashMap<String, String>();

		
		for(ProcessLoggerDetail processLoggerDetail: processLoggerDetails){

			if(processLoggerDetail.getParameterName().equals("entity_collection")){
				parametersReport.put("entity_collection", processLoggerDetail.getParameterValue());
			}
			if(processLoggerDetail.getParameterName().equals("collection_state")){
				parametersReport.put("collection_state", processLoggerDetail.getParameterValue());
			}
			if(processLoggerDetail.getParameterName().equals("issuer")){
				parametersReport.put("issuer", processLoggerDetail.getParameterValue());
			}
			if(processLoggerDetail.getParameterName().equals("participant")){
				parametersReport.put("participant", processLoggerDetail.getParameterValue());
			}
			if(processLoggerDetail.getParameterName().equals("holder")){
				parametersReport.put("holder", processLoggerDetail.getParameterValue());
			}
			if(processLoggerDetail.getParameterName().equals("other_number")){
				parametersReport.put("other_number", processLoggerDetail.getParameterValue());
			}
			if(processLoggerDetail.getParameterName().equals("other_description")){
				parametersReport.put("other_description", processLoggerDetail.getParameterValue());
			}
			if(processLoggerDetail.getParameterName().equals("afp")){
				parametersReport.put("afp", processLoggerDetail.getParameterValue());
			}
			if(processLoggerDetail.getParameterName().equals("date_initial")){
				parametersReport.put("date_initial", processLoggerDetail.getParameterValue());
			}
			if(processLoggerDetail.getParameterName().equals("date_end")){
				parametersReport.put("date_end", processLoggerDetail.getParameterValue());
			}
			
			if(processLoggerDetail.getParameterName().equals("idReportPk")){
				reportId = Long.valueOf((processLoggerDetail.getParameterValue()));
			}
			
			if(processLoggerDetail.getParameterName().equals("userNameLogged")){
				username = processLoggerDetail.getParameterValue();
			}
			
		}
		
		if(!parametersReport.containsKey("entity_collection")) parametersReport.put("entity_collection", null);
		if(!parametersReport.containsKey("collection_state")) parametersReport.put("collection_state", null);
		if(!parametersReport.containsKey("issuer")) parametersReport.put("issuer", null);
		if(!parametersReport.containsKey("participant")) parametersReport.put("participant", null);
		if(!parametersReport.containsKey("holder")) parametersReport.put("holder", null);
		if(!parametersReport.containsKey("other_number")) parametersReport.put("other_number", null);
		if(!parametersReport.containsKey("other_description")) parametersReport.put("other_description", null);
		if(!parametersReport.containsKey("afp")) parametersReport.put("afp", null);
		if(!parametersReport.containsKey("date_initial")) parametersReport.put("date_initial", CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));
		if(!parametersReport.containsKey("date_end")) parametersReport.put("date_end", CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));
		
		// Obteniendo los servicios que tienen cobros
		List<String> listCodeService = billingServiceReportServiceBean.getServiceCodeFromCollectionRecord(parametersReport);
		for(String codeService: listCodeService){
			parametersReport.put("p_service_code",codeService);
			try {
				executeReport(reportId,parametersReport, username);
			} catch (ServiceException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void executeReport(Long reportId,Map<String,String> parametersReport, String username) throws ServiceException, IOException{		
//		Map<String,String> parameters = new HashMap<String, String>();
				
		ReportLogger reportLogger = reportLocalServiceFacade.getReportLogger(reportId, parametersReport, null);
		
	    ReportLoggerUser loggerUser = new ReportLoggerUser();
		loggerUser.setAccessUser(reportLogger.getLastModifyUser());
		loggerUser.setAssignedDate(CommonsUtilities.currentDateTime());
		loggerUser.setReportLogger(reportLogger);
		loggerUser.setLastModifyApp(reportLogger.getLastModifyApp());
		loggerUser.setLastModifyDate(reportLogger.getLastModifyDate());
		loggerUser.setLastModifyIp(reportLogger.getLastModifyIp());
		loggerUser.setLastModifyUser(reportLogger.getLastModifyUser());
		reportUtilServiceBean.create(loggerUser);
		
	    ReportContext reportContext = createContext(reportLogger);
	    genericReport.setReportContext(reportContext);
	    genericReport.setReportLogger(reportLogger);
	    genericReport.runOutsideThread();
	}
	
	public ReportContext createContext(ReportLogger reportLogger) {
		ReportContext reportContext = null;
		if (reportLogger != null) {
			reportContext = new ReportContext();
			reportContext.setProcessLoggerId(reportLogger.getIdReportLoggerPk());
			
			reportContext.setPriority(reportLogger.getReport().getIndPriority());
			
			reportContext.setReportImplementationBean(reportLogger.getReport().getReportBeanName());
			
			reportContext.setTemplateReportUrl(reportLogger.getReport().getTemplateName());
			
			reportContext.setXmlDataSource(reportLogger.getReport().getDatasourceXml());
			reportContext.setReportQuery(reportLogger.getReport().getQueryReport());
			reportContext.setSubReportQuery(reportLogger.getReport().getSubQueryReport());
			reportContext.setReportType(reportLogger.getReport().getReportType());
		}
		return reportContext;
	}

	
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}
