package com.pradera.report.generation.executor.account.service;

import java.io.ByteArrayOutputStream;

import com.pradera.model.report.ReportLogger;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;


@ReportProcess(name="AssetLaundryByListTypeReport")
public class AssetLaundryByListTypeReport extends GenericReport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static int LIST_TYPE               =0;
	private static int CODE_AL                               = 1;
	private static int NAME_SOCIAL_REASON                    = 2;
	private static int FIRST_LAST_NAME_AL                    = 3;
	private static int SECOND_LAST_NAME_AL                   = 4;
	private static int DOCUMENT_TYPE                         = 5;
	private static int DOCUMENT_NUMBER                       = 6;
	private static int RESIDENCE_COUNTRY                     = 7;
	private static int REIGSTRY_DATE                         = 8;
	private static int RNT                                   = 9;
	private static int NAME_SOCIAL_REASON_HOLDER             = 10;
	private static int DOCUMENT_TYPE_HOLDER                  = 11;
	private static int DOCUMENT_NUMBER_HOLDER                = 12;
	private static int NATIONALITY_HOLDER                    = 13;
	private static int RESIDENCE_COUNTRY_HOLDER              = 14;
	private static int PARTICIPANT_CREATOR_HOLDER            = 15;
	private static int IND_PARTICIPANT                       = 16;
	private static int IND_ISSUER                            = 17;
	private static int ID_REPRESENTANT                       = 18;
	private static int NAME_SOCIAL_REASON_REPRESENTANT       = 19;
	private static int NATIONALITY_REPRESNETANT              = 20;
	private static int RESIDENCE_COUNTRY_REPRESENTANT        = 21;
	

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		
 		return null;
	}
	
	
	

}
