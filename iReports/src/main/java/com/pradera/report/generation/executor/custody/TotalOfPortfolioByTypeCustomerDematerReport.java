package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.management.RuntimeErrorException;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.executor.custody.to.ClientPortafolioTO;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "TotalOfPortfolioByTypeCustomerDematerReport")
public class TotalOfPortfolioByTypeCustomerDematerReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;

	@EJB
	private CustodyReportServiceBean custodyReportServiceBean;

	@Inject
	private PraderaLogger log;

	public TotalOfPortfolioByTypeCustomerDematerReport() {}
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {

		ClientPortafolioTO to = new ClientPortafolioTO();

		for(ReportLoggerDetail param: getReportLogger().getReportLoggerDetails()){
			if(param.getFilterName().equals("date_initial")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					to.setCutDate(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("portafolio_type")){
				if (Validations.validateIsNotNullAndNotEmpty(param.getFilterValue())){
					to.setPortafolioType(Integer.valueOf(param.getFilterValue()));
				}
			}
		} 

		//VALIDAR TIPO DE CAMBIO A LA FECHA DE CORTE
		try{
			custodyReportServiceBean.getExchangeRate(to.getCutDate());
		} catch (ServiceException e) {
			if(e.getErrorService().equals(ErrorServiceType.NOT_EXCHANGE_RATE)){
				throw new RuntimeErrorException(null, ReportConstant.ERROR_EXCHANGE_RATE + " " + to.getCutDate());
			}
			log.error(e.getMessage());
		}
		
		String strQuery = null;
		String title = null;
		
		if(to.getPortafolioType().equals(IssuanceType.DEMATERIALIZED.getCode())){
			strQuery = custodyReportServiceBean.getTotalOfPortfolioByTypeCustomerDemater(to);
			title = "REPORTE DE MONTO TOTAL DE CARTERA POR TIPO DE CLIENTE - DESMATERIALIZADO";
		}else{
			strQuery = custodyReportServiceBean.getTotalOfPortfolioByTypeCustomerPhysical(to);
			title = "REPORTE DE MONTO TOTAL DE CARTERA POR TIPO DE CLIENTE - FISICO";
		}
		
		Map<String,Object> parametersRequired = new HashMap<>();
		System.out.println(strQuery);
		parametersRequired.put("str_Query", strQuery);
		parametersRequired.put("title", title);
		parametersRequired.put("initialDt", to.getCutDate());
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		return parametersRequired;
	}
}
