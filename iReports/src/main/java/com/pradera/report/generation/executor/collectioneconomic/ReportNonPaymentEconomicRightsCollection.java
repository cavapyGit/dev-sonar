package com.pradera.report.generation.executor.collectioneconomic;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.edv.collection.economic.rigth.PaymentAgent;

@ReportProcess(name = "ReportNonPaymentEconomicRightsCollection")
public class ReportNonPaymentEconomicRightsCollection extends GenericReport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6222061741058613010L;

	@Inject
	PraderaLogger log;

	@EJB
	private ParameterServiceBean parameterService;

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		return null;
	}

	@Override
	public void addParametersQueryReport() {
	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		Map<String, Object> parametersRequired = new HashMap<>();
		
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();


		for (ReportLoggerDetail r : loggerDetails) {
			/*participante*/
			if (r.getFilterName().equals("p_participant"))
				if (r.getFilterValue() != null){
					parametersRequired.put("p_participant", r.getFilterValue());
					Participant participant = parameterService.find(Participant.class, Long.parseLong(r.getFilterValue()));
					parametersRequired.put("p_participant_desc", participant.getMnemonic()+" - "+participant.getDescription());
				}					
				else
					parametersRequired.put("p_participant", null);
			/*Agente pagador*/
			if (r.getFilterName().equals("p_payment_agent"))
				if (r.getFilterValue() != null){
					parametersRequired.put("p_payment_agent", r.getFilterValue());
					PaymentAgent paymentAgent = parameterService.find(PaymentAgent.class, Long.parseLong(r.getFilterValue()));
					parametersRequired.put("p_payment_agent_desc", paymentAgent.getMnemonic()+ " - "+paymentAgent.getDescription());
				}
				else
					parametersRequired.put("p_payment_agent", null);
			/*Emisor*/
			if (r.getFilterName().equals("p_issuer"))
				if (r.getFilterValue() != null){
					parametersRequired.put("p_issuer", r.getFilterValue());
					Issuer issuer = parameterService.find(Issuer.class, r.getFilterValue());
					parametersRequired.put("p_issuer_desc", issuer.getMnemonic()+" - "+issuer.getDescription());
				}
				else
					parametersRequired.put("p_issuer", null);
			/*Moneda*/
			if (r.getFilterName().equals("p_currency"))
				if (r.getFilterValue() != null){
					parametersRequired.put("p_currency", r.getFilterValue());
					ParameterTable parameterTable = parameterService.find(ParameterTable.class, Integer.parseInt(r.getFilterValue()));
					parametersRequired.put("p_currency_desc", parameterTable.getText1()+" - "+parameterTable.getParameterName());
				}
				else
					parametersRequired.put("p_currency", null);
			/*Clase de valor*/
			if (r.getFilterName().equals("p_security_class"))
				if (r.getFilterValue() != null){
					parametersRequired.put("p_security_class", r.getFilterValue());
					ParameterTable parameterTable = parameterService.find(ParameterTable.class, Integer.parseInt(r.getFilterValue()));
					parametersRequired.put("p_security_class_desc", parameterTable.getText1()+" - "+parameterTable.getParameterName());
				}
				else
					parametersRequired.put("p_security_class", null);
			/**Fecha de pago inicial*/
			if (r.getFilterName().equals("p_payment_date_init"))
				if (r.getFilterValue() != null)
					parametersRequired.put("p_payment_date_initial", r.getFilterValue());
				else
					parametersRequired.put("p_payment_date_initial", null);
			/**Fecha de pago final*/
			if (r.getFilterName().equals("p_payment_date_end"))
				if (r.getFilterValue() != null)
					parametersRequired.put("p_payment_date_final", r.getFilterValue());
				else
					parametersRequired.put("p_payment_date_final", null);
		}
		return parametersRequired;
	}

}
