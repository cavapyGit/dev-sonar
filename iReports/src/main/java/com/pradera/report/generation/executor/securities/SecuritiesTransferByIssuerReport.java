package com.pradera.report.generation.executor.securities;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "SecuritiesTransferByIssuerReport")
public class SecuritiesTransferByIssuerReport extends GenericReport {
	private static final long serialVersionUID = 1L;

	@Inject
	PraderaLogger log;
	
	@EJB
	private ParameterServiceBean parameterService;

	public SecuritiesTransferByIssuerReport() {
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParametersQueryReport() {
		List<ReportLoggerDetail> listaLogger = getReportLogger()
				.getReportLoggerDetails();
		if (listaLogger == null) {
			listaLogger = new ArrayList<ReportLoggerDetail>();
		}

	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		GenericsFiltersTO gfto = new GenericsFiltersTO();
		
		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("id_issuer")){
				if (listaLogger.get(i).getFilterValue()!=null){
					gfto.setIdIssuer(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("id_security_code")){
				if (listaLogger.get(i).getFilterValue()!=null){
					gfto.setIdSecurityCodePk(listaLogger.get(i).getFilterValue().toString());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("cui_holder")){
				if (listaLogger.get(i).getFilterValue()!=null){
					gfto.setCui(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("initial_date")){
				if (listaLogger.get(i).getFilterValue()!=null){
					gfto.setInitialDt(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("final_date")){
				if (listaLogger.get(i).getFilterValue()!=null){
					gfto.setFinalDt(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("bcb_format")){
				if (listaLogger.get(i).getFilterValue()!=null && !listaLogger.get(i).getFilterValue().equals("0")){
					gfto.setIndBCB(listaLogger.get(i).getFilterValue());
				}
			}
		}
		
		Map<String, Object> parametersRequired = new HashMap<String, Object>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> secClassDocType 			= new HashMap<Integer, String>();
		Map<Integer,String> secSecClass 			= new HashMap<Integer, String>();
		Map<Integer,String> secMnemonic 			= new HashMap<Integer, String>();
		try {
			filter.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secClassDocType.put(param.getParameterTablePk(), param.getIndicator1());
			}
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secSecClass.put(param.getParameterTablePk(), param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secMnemonic.put(param.getParameterTablePk(), param.getText1());
			}
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}

		
		parametersRequired .put("initial_date", gfto.getInitialDt());
		parametersRequired .put("final_date", gfto.getFinalDt());
		parametersRequired .put("cui_holder", gfto.getCui());
		parametersRequired .put("id_security_code", gfto.getIdSecurityCodePk());
		parametersRequired .put("id_issuer", gfto.getIdIssuer());
		parametersRequired.put("mDocType", secClassDocType);
		parametersRequired.put("mSecClass", secSecClass);
		parametersRequired.put("mMnemonic", secMnemonic);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());

		return parametersRequired;
	}

}
