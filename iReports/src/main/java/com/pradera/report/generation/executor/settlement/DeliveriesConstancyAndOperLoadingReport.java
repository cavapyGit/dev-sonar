package com.pradera.report.generation.executor.settlement;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.massive.type.MechanismFileProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;

@ReportProcess(name = "DeliveriesConstancyAndOperLoadingReport")
public class DeliveriesConstancyAndOperLoadingReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;
	@Inject
	private PraderaLogger log;
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		GenericsFiltersTO annasTransferTO = new GenericsFiltersTO();
		String participantDesc=null;

		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("process_type")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					annasTransferTO.setIdInteger(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("participant")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					annasTransferTO.setParticipant(listaLogger.get(i).getFilterValue());
					participantDesc = listaLogger.get(i).getFilterDescription();
				}
			}
			if(listaLogger.get(i).getFilterName().equals("date_initial")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					annasTransferTO.setInitialDt(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("date_end")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					annasTransferTO.setFinalDt(listaLogger.get(i).getFilterValue());
				}
			}	
			if(listaLogger.get(i).getFilterName().equals("institution_type")){
				if(listaLogger.get(i).getFilterValue()!=null){
					annasTransferTO.setInstitutionType(listaLogger.get(i).getFilterValue());
				}
			}
		}
		
		Map<String,Object> parametersRequired = new HashMap<>();
		Map<Integer,String> processTypeDesc = new HashMap<Integer, String>();
		ParameterTableTO  filter = new ParameterTableTO();
		List<Integer> lstPks= new ArrayList<Integer>();
		lstPks.add(MechanismFileProcessType.MCN_OPERATIONS_UPLOAD.getCode());
		if(annasTransferTO.getInstitutionType()!=null && annasTransferTO.getInstitutionType().equals(InstitutionType.DEPOSITARY.getCode().toString())){
			lstPks.add(MechanismFileProcessType.MCN_ASSIGNMENT_UPLOAD.getCode());
		}
		
		try {
			filter.setMasterTableFk(MasterTableType.LOADING_FILE_OPERATIONS_PROCESS_TYPE.getCode());
			filter.setLstParameterTablePk(lstPks);
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				processTypeDesc.put(param.getParameterTablePk(), param.getDescription());
			}
			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}

		parametersRequired.put("process_type_desc", processTypeDesc);
		if(annasTransferTO.getParticipant()!=null){
			parametersRequired.put("p_participant", annasTransferTO.getParticipant());
			parametersRequired.put("participant_desc", participantDesc);
		}
		
		return parametersRequired;
	}
}
