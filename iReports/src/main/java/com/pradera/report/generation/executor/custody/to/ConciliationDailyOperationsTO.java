package com.pradera.report.generation.executor.custody.to;

import java.io.Serializable;

public class ConciliationDailyOperationsTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1020892331570711702L;
	private String participant;
	private String cui_code;
	private String holder_account;
	private String issuer;
	private String security_class;
	private String security_code;
    private String descSecurity_class;
	private String currency_code;
	private String descCurrency_code;
	private String portofolio_code;
	private String descPortofolio_code;
	private String desHolder_account;
	private String descIssuer;
	private String desParticipant;
	private String descCui_code;
	private String date;
	private String beforeDate;
	public ConciliationDailyOperationsTO() {
		participant=null;
		cui_code=null;
		holder_account=null;
		issuer=null;
		security_class=null;
		security_code=null;
		date=null;
	}
	public String getParticipant() {
		return participant;
	}
	public void setParticipant(String participant) {
		this.participant = participant;
	}
	public String getCui_code() {
		return cui_code;
	}
	public void setCui_code(String cui_code) {
		this.cui_code = cui_code;
	}
	public String getHolder_account() {
		return holder_account;
	}
	public void setHolder_account(String holder_account) {
		this.holder_account = holder_account;
	}
	public String getIssuer() {
		return issuer;
	}
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	public String getSecurity_class() {
		return security_class;
	}
	public void setSecurity_class(String security_class) {
		this.security_class = security_class;
	}
	public String getSecurity_code() {
		return security_code;
	}
	public void setSecurity_code(String security_code) {
		this.security_code = security_code;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getDescSecurity_class() {
		return descSecurity_class;
	}
	public void setDescSecurity_class(String descSecurity_class) {
		this.descSecurity_class = descSecurity_class;
	}
	public String getCurrency_code() {
		return currency_code;
	}
	public void setCurrency_code(String currency_code) {
		this.currency_code = currency_code;
	}
	public String getDescCurrency_code() {
		return descCurrency_code;
	}
	public void setDescCurrency_code(String descCurrency_code) {
		this.descCurrency_code = descCurrency_code;
	}
	public String getPortofolio_code() {
		return portofolio_code;
	}
	public void setPortofolio_code(String portofolio_code) {
		this.portofolio_code = portofolio_code;
	}
	public String getDescPortofolio_code() {
		return descPortofolio_code;
	}
	public void setDescPortofolio_code(String descPortofolio_code) {
		this.descPortofolio_code = descPortofolio_code;
	}
	public String getBeforeDate() {
		return beforeDate;
	}
	public void setBeforeDate(String beforeDate) {
		this.beforeDate = beforeDate;
	}
	public String getDesHolder_account() {
		return desHolder_account;
	}
	public void setDesHolder_account(String desHolder_account) {
		this.desHolder_account = desHolder_account;
	}
	public String getDescIssuer() {
		return descIssuer;
	}
	public void setDescIssuer(String descIssuer) {
		this.descIssuer = descIssuer;
	}
	public String getDesParticipant() {
		return desParticipant;
	}
	public void setDesParticipant(String desParticipant) {
		this.desParticipant = desParticipant;
	}
	public String getDescCui_code() {
		return descCui_code;
	}
	public void setDescCui_code(String descCui_code) {
		this.descCui_code = descCui_code;
	}
}
