package com.pradera.report.generation.executor.settlement;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;

import com.pradera.commons.utils.GeneralConstants;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.settlement.service.SettlementReportServiceBean;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;
import com.pradera.report.generation.service.ReportUtilServiceBean;

/**
 * Reporte 4 ASFI pk 285
 * @author oquisbert
 * 11/07/2017
 */
@ReportProcess(name = "SecuritiesSettlementReport")
public class SecuritiesSettlementReport extends GenericReport{

	private static final long serialVersionUID = 1L;

	@EJB
	private ReportUtilServiceBean reportUtilServiceBean;
	
	@EJB
	private SettlementReportServiceBean settlementReportServiceBean;

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		//Reporte solo en formato TXT
		GenericsFiltersTO gfto = new GenericsFiltersTO();
		for(ReportLoggerDetail param: getReportLogger().getReportLoggerDetails()){
			if(param.getFilterName().equals("cut_date")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					//Filtro de Fecha
					gfto.setFinalDt(param.getFilterValue().toString());
				}
			}
		}
		try {
			//Metodo para generar el archivo
			generateReportTXT(gfto);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
    	return null;
	}
	/**
	 * Metodo para la generacion en TXT de los datos del reporte 4 ASFI
	 * @param gfto
	 * @throws ServiceException
	 */
	private void generateReportTXT(GenericsFiltersTO gfto) throws ServiceException{
		
		//Declaramos variables
		StringBuilder sbTxtFormat = new StringBuilder();
		//Generamos el nombre del archivo
		String fileName = getNameAccordingFund(gfto)+"TXT";
		//Query con la informacion que saldra en el reporte
		List <Object[]> listToRep = settlementReportServiceBean.getListSecuritiesSettlementss(gfto);
		
		//Si el reporte tiene datos 
		if(listToRep.size() > GeneralConstants.ZERO_VALUE_INTEGER){
			boolean firstLoop = true;
			for (Object [] unitString : listToRep){
				
				if(firstLoop){
					firstLoop = false;
				}else{
					//Salto de linea
					sbTxtFormat.append(GeneralConstants.STR_END_LINE_TXT);
				}
				
				//Concatenamos todo en una sola cadena
				sbTxtFormat.append(GeneralConstants.STR_COMILLA);
				sbTxtFormat.append(unitString[0].toString());
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				sbTxtFormat.append(unitString[1].toString());
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				sbTxtFormat.append(unitString[2].toString());
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				sbTxtFormat.append(unitString[3].toString());
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				if(!String.valueOf(unitString[5]).equals(GeneralConstants.ZERO_VALUE_STRING)){ sbTxtFormat.append(String.valueOf(unitString[5])); }else{ sbTxtFormat.append("0"); }
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				if(!String.valueOf(unitString[6]).equals(GeneralConstants.ZERO_VALUE_STRING)){ sbTxtFormat.append(String.valueOf(unitString[6])); }else{ sbTxtFormat.append("0"); }
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				if(!String.valueOf(unitString[7]).equals(GeneralConstants.ZERO_VALUE_STRING)){ sbTxtFormat.append(String.valueOf(unitString[7])); }else{ sbTxtFormat.append("0"); }
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				if(!String.valueOf(unitString[8]).equals(GeneralConstants.ZERO_VALUE_STRING)){ sbTxtFormat.append(String.valueOf(unitString[8])); }else{ sbTxtFormat.append("0"); }
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				if(!String.valueOf(unitString[12]).equals(GeneralConstants.ZERO_VALUE_STRING)){ sbTxtFormat.append(String.valueOf(unitString[12])); }else{ sbTxtFormat.append("0"); }
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				if(!String.valueOf(unitString[9]).equals(GeneralConstants.ZERO_VALUE_STRING)){ sbTxtFormat.append(String.valueOf(unitString[9])); }else{ sbTxtFormat.append("0"); }
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				if(!String.valueOf(unitString[10]).equals(GeneralConstants.ZERO_VALUE_STRING)){ sbTxtFormat.append(String.valueOf(unitString[10])); }else{ sbTxtFormat.append("0"); }
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				if(!String.valueOf(unitString[11]).equals(GeneralConstants.ZERO_VALUE_STRING)){ sbTxtFormat.append(String.valueOf(unitString[11])); }else{ sbTxtFormat.append("0"); }
				sbTxtFormat.append(GeneralConstants.STR_COMILLA);
				
			}
			//Generamos el archivo 
			reportUtilServiceBean.saveCustomReportFile(fileName, ReportFormatType.TXT.getCode(), sbTxtFormat.toString().getBytes(), getReportLogger());
		}else{
			//Si el reporte no tiene datos
			sbTxtFormat.append(GeneralConstants.STR_COMILLA + "SIN DATOS" + GeneralConstants.STR_COMILLA);
			reportUtilServiceBean.saveCustomReportFile(fileName, ReportFormatType.TXT.getCode(), sbTxtFormat.toString().getBytes(), getReportLogger());
		}
		
	}
	
	/**
	 * Metodo para generar el nombre del archivo
	 * @param gfto
	 * @return
	 */
	private String getNameAccordingFund(GenericsFiltersTO gfto){
		String name="";
		String firstWord="M";
		String strDate= gfto.getFinalDt();
		String strDay = strDate.substring(0,2);
		String strMonth = strDate.substring(3,5);
		String strYear = strDate.substring(8,10);
		String lastWord="AK";
		String reportCode="";

		name=firstWord;
		name+=strYear+strMonth+strDay;
		name+=lastWord;
		name+=GeneralConstants.DOT+reportCode;
		return name;
	}
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
}
