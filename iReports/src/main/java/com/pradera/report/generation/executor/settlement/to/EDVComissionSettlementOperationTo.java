package com.pradera.report.generation.executor.settlement.to;

import java.io.Serializable;

public class EDVComissionSettlementOperationTo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String modality;
	private String participant;
	private String cuiCode;
	private String holderAccount;
	private String securityClass;
	private String initialDate;
	private String finalDate;
	
	// constructor
	public EDVComissionSettlementOperationTo(){}

	public String getModality() {
		return modality;
	}

	public void setModality(String modality) {
		this.modality = modality;
	}

	public String getParticipant() {
		return participant;
	}

	public void setParticipant(String participant) {
		this.participant = participant;
	}

	public String getCuiCode() {
		return cuiCode;
	}

	public void setCuiCode(String cuiCode) {
		this.cuiCode = cuiCode;
	}

	public String getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(String holderAccount) {
		this.holderAccount = holderAccount;
	}

	public String getSecurityClass() {
		return securityClass;
	}

	public void setSecurityClass(String securityClass) {
		this.securityClass = securityClass;
	}

	public String getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(String initialDate) {
		this.initialDate = initialDate;
	}

	public String getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(String finalDate) {
		this.finalDate = finalDate;
	}
}
