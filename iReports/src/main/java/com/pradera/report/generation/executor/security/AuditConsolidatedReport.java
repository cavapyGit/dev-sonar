package com.pradera.report.generation.executor.security;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.security.service.AuditConsolidatedServiceBean;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

@ReportProcess(name="AuditConsolidatedReport")
public class AuditConsolidatedReport extends GenericReport{

	private static final long serialVersionUID = 1L;
	@Inject
	PraderaLogger log;

	@EJB
	private AuditConsolidatedServiceBean auditConsolidatedReport;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	//declarando posiciones del query
	private static int USER_CODE=0;
	private static int SESSION_ID=1;
	private static int LOGIN_DT=2;
	private static int LOGOUT_DT=3;
	private static int IP=4;
	private static int DISCONNECTION_MOTIVE=5;
	private static int EXECUTE_DT=6;
	private static int OPTION_ID=7;
	private static int OPTION_NAME=8;
	private static int PRIVILEGE=9;
	private static int BUSINESS_METHOD=10;
	private static int PROCESS_NAME=11;
	private static int PROCESS_TYPE=12;
	private static int PROCESS_STATE=13;
	private static int PARAMETERS=14;
	private static int PARAMETER_VALUE=15;
	
	public AuditConsolidatedReport(){
		//TODO
	} 
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		boolean existData = false;
		String user_code=null;
		String ip=null;
		Long process_type=null;
		Long process_state=null;
		Date date_initial=null;
		Date date_end=null;
		//Read parameters from DB, all parameters are string type
		for(ReportLoggerDetail loggerDetail : reportLogger.getReportLoggerDetails()) {
			if(loggerDetail.getFilterName().equals(ReportConstant.USER_CODE)) {
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					user_code = loggerDetail.getFilterValue();
					//sbParametersReport.append(loggerDetail.getFilterValue()).append(GeneralConstants.STR_COMMA);
				}				
			}else if(loggerDetail.getFilterName().equals(ReportConstant.IP)) {
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					ip = loggerDetail.getFilterValue();
				}
			}else if(loggerDetail.getFilterName().equals(ReportConstant.PROCESS_TYPE)) {
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					process_type = Long.valueOf(loggerDetail.getFilterValue());
				}
			}else if(loggerDetail.getFilterName().equals(ReportConstant.PROCESS_STATE)) {
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					process_state = Long.valueOf(loggerDetail.getFilterValue());
				}
			}else if(loggerDetail.getFilterName().equals(ReportConstant.START_DATE)){
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					date_initial = CommonsUtilities.convertStringtoDate(loggerDetail.getFilterValue().toString());
				}
			}else if (loggerDetail.getFilterName().equals(ReportConstant.END_DATE)){
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					date_end = CommonsUtilities.convertStringtoDate(loggerDetail.getFilterValue().toString());
				}
			}						
		}
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			//query
			List<Object[]> list = auditConsolidatedReport.getUserAuditForReport(user_code, ip, process_type,
																		process_state, date_initial, date_end);
			if(!list.isEmpty()) {
				existData = true;
			}
			
			XMLOutputFactory xof = XMLOutputFactory.newInstance();
			XMLStreamWriter xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(baos));
			//open document file
			xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");
			//root tag of report
			/**
			 * report
			 * */
			xmlsw.writeStartElement(ReportConstant.REPORT);

			createHeaderReport(xmlsw, reportLogger);
			
			if(existData) {
				createTagString(xmlsw,"flag_exist_data","true");
				//method specific to each implementation of reports
				xmlsw.writeStartElement("userBySession");
				createBodyReport(xmlsw, list);
				xmlsw.writeEndElement();
			} else {
				//when not exist data we need to create empty structure xml
				createTagString(xmlsw,"flag_exist_data","false");			
			}
			String date_initial_format = CommonsUtilities.convertDateToString(date_initial,CommonsUtilities.DATE_PATTERN);
			createTagString(xmlsw,"initial_date",date_initial_format);
			
			String date_end_format = CommonsUtilities.convertDateToString(date_end,CommonsUtilities.DATE_PATTERN);
			createTagString(xmlsw,"end_date",date_end_format);
			
			//close report tag
			xmlsw.writeEndElement();
			//close document
			xmlsw.writeEndDocument();
			xmlsw.flush();
	        xmlsw.close();
		} catch (Exception ex) {
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		return baos;
	}
	
	public void createBodyReport(XMLStreamWriter xmlsw,List<Object[]> list)  throws XMLStreamException{
		ParameterTable objPt=null;
		String procTypeDesc = "---";
		String procStatDesc = "---";
		for(Object[] ob : list) {
			xmlsw.writeStartElement("processBySession");
				createTagString(xmlsw, "userCode", ob[USER_CODE]);
				createTagString(xmlsw, "sessionId", ob[SESSION_ID]);
				createTagString(xmlsw, "loginDt", ob[LOGIN_DT]);
				createTagString(xmlsw, "logoutDt", ob[LOGOUT_DT]);
				createTagString(xmlsw, "ip", ob[IP]);
				createTagString(xmlsw, "disconMotive", ob[DISCONNECTION_MOTIVE]);
				
				createTagString(xmlsw, "execDate", ob[EXECUTE_DT]);
				createTagString(xmlsw, "idOption", ob[OPTION_ID]);
				createTagString(xmlsw, "nameOption", ob[OPTION_NAME]);
				createTagString(xmlsw, "privilege", ob[PRIVILEGE]);
				createTagString(xmlsw, "busMethod", ob[BUSINESS_METHOD]);
				createTagString(xmlsw, "processName", ob[PROCESS_NAME]);
				Integer procType=null;
				if(Validations.validateIsNotNullAndNotEmpty(ob[PROCESS_TYPE])){
					procType=Integer.parseInt(ob[PROCESS_TYPE].toString());
					objPt = parameterService.getParameterDetail(procType);
					procTypeDesc = objPt.getParameterName();
				}
				createTagString(xmlsw, "processType", procTypeDesc);
				Integer procStat=null;
				if(Validations.validateIsNotNullAndNotEmpty(ob[PROCESS_STATE])){
					procStat=Integer.parseInt(ob[PROCESS_STATE].toString());
					objPt = parameterService.getParameterDetail(procStat);
					procStatDesc = objPt.getParameterName();
				}
				createTagString(xmlsw, "processState", procStatDesc);
				createTagString(xmlsw, "parameters", ob[PARAMETERS]);
				createTagString(xmlsw, "parameterValue", ob[PARAMETER_VALUE]);	
			xmlsw.writeEndElement();
		}
	}
}
