package com.pradera.report.generation.executor.account;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.account.service.AccountReportServiceBean;
import com.pradera.report.generation.executor.account.to.NoteToExternalTO;

@ReportProcess(name = "DocsNoteToExternalReport")
public class DocsNoteToExternalReport extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;

	@EJB
	private HolidayQueryServiceBean holidayQueryServiceBean;
	
	@EJB
	private AccountReportServiceBean reportServiceBean;

	@Inject
	PraderaLogger log;

	public static final String ARCHETYPE_MRS = "a ";

	public static final String EXECUTIVE_DIRECTOR = "DIRECTOR EJECUTIVO";
	public static final String EXECUTIVE_DIRECTOR_ARCH_MRS = "DIRECTORA EJECUTIVA a.i.";
	public static final String INSURANCE_DIRECTOR = "Director de Seguros";
	public static final String LEGAL_REPRESENTATIVE = "REPRESENTANTE LEGAL";

	public static final String ASFI = "AUTORIDAD DE SUPERVISI\u00d3N DEL SISTEMA FINANCIERO";
	public static final String APS = "AUTORIDAD DE PENSIONES Y SEGUROS";
	public static final String APS_INSURANCE_ADDRESS = "DIRECCI\u00d3N DE SEGUROS\n"
			+ "AUTORIDAD DE FISCALIZACI\u00d3N\nDE PENSIONES Y SEGUROS (APS)";
	public static final String DATA_FIDELIS = "DATA FIDELIS S.A.";

	public static final String ATN = "Gonzalo Bravo - Director de Supervisi\u00f3n de Valores";

	public static final String BODY_DOC_1 = "De nuestra consideraci\u00f3n:<br><br>"
			+ " El cumplimiento a su nota SPVS-DIGC-022/2009 de fecha 29 de enero de 2009, adjuntamos a la"
			+ " presente el formulario mensual impreso INV-CUST-CC, con las carteras valoradas del Fondo"
			+ " de Renta Universal de Vejez (FRUV) a cargo de la Vitalicia Seguros y Reaseguros de Vida"
			+ " S.A. y Bisa SAFI S.A., correspondiente al ";
	public static final String BODY_DOC_2 = "De nuestra consideraci\u00f3n:<br><br>"
			+ " En cumplimiento a la nota SPVS-IV-CC-053/2004 recibida en fecha 9 de noviembre de 2004, y"
			+ " a la nota ASFI/DSV/R-56747/2011 de fecha 3 de junio de 2001, adjuntamos a la presente el"
			+ " formulario mensual impreso INV-CUST-CC, con las carteras valoradas de las Compa\u00f1\u00edas"
			+ " Aseguradoras: Seguros Pr\u00f3vida S.A (dos cuentas)., La Vitalicia Seguros y Reaseguros de Vida"
			+ " S.A (dos cuentas)., Bisa Seguros y Reaseguros S.A., Seguros Illimani S.A., Fortaleza Seguros"
			+ " y Reaseguros S.A., Adri\u00e1tica Seguros y Reaseguros S.A., Alianza Compa\u00f1\u00eda de Seguros y"
			+ " Reaseguros S.A., La Boliviana Ciacruz de Seguros y Reaseguros S.A., Seguros y Reaseguros"
			+ " Credinform International S.A., Seguros y Reaseguros Generales 24 de Septiembre S.A.,"
			+ " Alianza Vida Seguros y Reaseguros S.A., Nacional Vida Seguros de Personas S.A., BUPA"
			+ " Insurance (Bolivia) S.A., La Boliviana Ciacruz Seguros Personales S.A., Latina Seguros"
			+ " Patrimoniales S.A. y Crediseguro S.A. Seguros Personales, correspondientes al ";
	public static final String BODY_DOC_3_1 = "De nuestra consideraci\u00f3n:<br><br>"
			+ " El cumplimiento a su nota SPVS-IP-CIRCULAR 040/2003 de fecha 21 de abril de 2003, adjuntamos"
			+ " a la presente el formulario mensual impreso INV-CUST-CC, con las carteras valoradas del"
			+ " Fondo de Capitalizaci\u00f3n Colectiva (FCC) a cargo de las Administradoras de Fondo de Pensiones"
			+ " AFP Futuro de Bolivia S.A. y BBVA Previsi\u00f3n AFP S.A., correspondientes al ";
	public static final String BODY_DOC_3_2 = ".<br><br>Asimismo, adjuntamos el formulario correspondiente"
			+ " a las carteras valoradas del Fondo de Capitalizaci\u00f3n Individual (FCI), a cargo de las"
			+ " Administradorasde Fondos de Pensiones AFP Futuro de Bolivia S.A. y BBVA Previsi\u00f3n AFP S.A.,"
			+ " correspondientes al ";
	public static final String BODY_DOC_4 = "De nuestra consideraci\u00f3n:<br><br>"
			+ " En atenci\u00f3n a su nota Cite: APS/DS/JFC/18884/2013, mediante la cual nos solicita, el env\u00edo"
			+ " mensual del Detalle de Bloqueos Vigentes de la cartera de inversiones de las entidades de"
			+ " Seguros y Reaseguros reguladas por las APS y observaci\u00f3n a lo dispuesto por la Autoridad de"
			+ " Supervisi\u00f3n del Sistema Financiero (ASFI), mediante nota ASFI/DSV/R-56747/2011 ajuntamos a"
			+ " la presente remitimos el Reporte de Bloqueos Vigentes al ";
	public static final String BODY_DOC_4_LIST = " de las siguientes entidades aseguradoras:<br>"
			+ "<li>La Boliviana Ciacruz Seguros y Reaseguros S.A.</li>"
			+ "<li>Seguros y Reaseguros Credinform International S.A.</li>"
			+ "<li>Seguros y Ilimani S.A.</li>"
			+ "<li>Alianza Compa\u00f1ia de Seguros y Reaseguros S.A.</li>"
			+ "<li>Bisa Seguros y Reaseguros S.A.</li>"
			+ "<li>Compa\u00f1ia de Seguros y Reaseguros Fortaleza S.A.</li>"
			+ "<li>Latina Seguros Patrimoniales S.A.</li>"
			+ "<li>BUPA Insurance Bolivia S.A.</li>"
			+ "<li>La Vitalicia Seguros y Reaseguros de Vida S.A.</li>"
			+ "<li>La Boliviana Ciacruz Seguros Personales S.A.</li>"
			+ "<li>Seguros Provida S.A.</li>"
			+ "<li>Nacional Vida Seguros de Personas S.A.</li>"
			+ "<li>Alianza Vida Seguros y Reaseguros S.A.</li>";
	public static final String BODY_DOC_5 = "De nuestra consideraci\u00f3n:<br><br>"
			+ " El cumplimiento a lo establecido en nuestro Reglamento Interno, adjuntamos a la presente"
			+ " el Reporte de Asignaci\u00f3n de C\u00f3digos ISIN y CFI de fecha ";
	public static final String BODY_DOC_6 = "De nuestra consideraci\u00f3n:<br><br>"
			+ " El cumplimiento a lo establecido en nuestro Convenio, adjuntamos a la presente el Reporte"
			+ " de Asignaci\u00f3n de C\u00f3digos ISIN y CFI de fecha ";

	public static final String BODY_DOC_FAREWELL_1 = ".<br><br>Sin otro particular, saludamos a usted."
			+ "<br><br>Atentamente,";
	public static final String BODY_DOC_FAREWELL_2 = "<br>Sin otro particular, le tenemos el agrado de"
			+ " saludar a usted atentamente.";

	public DocsNoteToExternalReport() {}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParametersQueryReport() {

	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {

		Map<String, Object> parametersRequired = new HashMap<>();
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		NoteToExternalTO noteExternalTO = new NoteToExternalTO();

		// DATOS DE ENTRADA DEL SISTEMA
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.REFERENCE_NOTE_EXTERNAL)) {
				noteExternalTO.setIdReferencePk(Integer.parseInt(r.getFilterValue()));
			}
			if (r.getFilterName().equals(ReportConstant.QUOTE_NOTE_EXTERNAL)) {
				noteExternalTO.setQuote(r.getFilterValue());
			}
			if (r.getFilterName().equals(ReportConstant.ATTACHED_SHEETS)) {
				noteExternalTO.setAttachedSheets(r.getFilterValue());
			}
		}

		// DESCRIPCION DE LA REFERENCIA SELECCIONADA
		try {
			Integer idReference = noteExternalTO.getIdReferencePk();
			String desReference = parameterService.findParameterTableDescription(idReference);
			noteExternalTO.setDesReference(desReference);
		} catch (Exception ex) {
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}

		// DATOS SELECCIONADOS SEGUN PK DE LA REFERENCIA
		switch (noteExternalTO.getIdReferencePk()) {
			//DOC_1
			case 2007:
				noteExternalTO.setArchetype(ARCHETYPE_MRS);
				noteExternalTO.setRecipient(getRecipientNoteToExternal(2013));
				noteExternalTO.setOccupation(EXECUTIVE_DIRECTOR_ARCH_MRS);
				noteExternalTO.setDepartment(ASFI);
				noteExternalTO.setAtn(ATN);		
				noteExternalTO.setDateHeader(CommonsUtilities.convertDateToStringLocaleES(getFirstDate(),CommonsUtilities.DATE_PATTERN_DOCUMENT));
				noteExternalTO.setBodyDoc(BODY_DOC_1 
						+ CommonsUtilities.convertDateToStringLocaleES(getUltimateDate(),CommonsUtilities.DATE_PATTERN_DOCUMENT)
						+ BODY_DOC_FAREWELL_1);
				break;
			//DOC_2	
			case 2008:
				noteExternalTO.setRecipient(getRecipientNoteToExternal(2014));
				noteExternalTO.setOccupation(EXECUTIVE_DIRECTOR);
				noteExternalTO.setDepartment(APS);
				noteExternalTO.setDateHeader(CommonsUtilities.convertDateToStringLocaleES(getFirstDate(),CommonsUtilities.DATE_PATTERN_DOCUMENT));
				noteExternalTO.setBodyDoc(BODY_DOC_2
						+ CommonsUtilities.convertDateToStringLocaleES(getUltimateDate(),CommonsUtilities.DATE_PATTERN_DOCUMENT)
						+ BODY_DOC_FAREWELL_1);
				break;
			//DOC_3
			case 2009:
				noteExternalTO.setRecipient(getRecipientNoteToExternal(2014));
				noteExternalTO.setOccupation(EXECUTIVE_DIRECTOR);
				noteExternalTO.setDepartment(APS);
				noteExternalTO.setDateHeader(CommonsUtilities.convertDateToStringLocaleES(getFirstDate(),CommonsUtilities.DATE_PATTERN_DOCUMENT));
				noteExternalTO.setBodyDoc(BODY_DOC_3_1
						+ CommonsUtilities.convertDateToStringLocaleES(getUltimateDate(),CommonsUtilities.DATE_PATTERN_DOCUMENT)
						+ BODY_DOC_3_2
						+ CommonsUtilities.convertDateToStringLocaleES(getUltimateDate(),CommonsUtilities.DATE_PATTERN_DOCUMENT)
						+ BODY_DOC_FAREWELL_1);
				break;
			//DOC_4
			case 2010:
				noteExternalTO.setRecipient(getRecipientNoteToExternal(2015));
				noteExternalTO.setOccupation(INSURANCE_DIRECTOR);
				noteExternalTO.setDepartment(APS_INSURANCE_ADDRESS);
				noteExternalTO.setDateHeader(CommonsUtilities.convertDateToStringLocaleES(getFirstDate(),CommonsUtilities.DATE_PATTERN_DOCUMENT));
				noteExternalTO.setBodyDoc(BODY_DOC_4
						+ CommonsUtilities.convertDateToStringLocaleES(getUltimateDate(),CommonsUtilities.DATE_PATTERN_DOCUMENT)
						+ BODY_DOC_4_LIST
						+ BODY_DOC_FAREWELL_2);
				break;
			//DOC_5
			case 2011:
				noteExternalTO.setArchetype(ARCHETYPE_MRS);
				noteExternalTO.setRecipient(getRecipientNoteToExternal(2013));
				noteExternalTO.setOccupation(EXECUTIVE_DIRECTOR_ARCH_MRS);
				noteExternalTO.setDepartment(ASFI);
				noteExternalTO.setAtn(ATN);
				noteExternalTO.setDateHeader(CommonsUtilities.convertDateToStringLocaleES(getFirstDate(),CommonsUtilities.DATE_PATTERN_DOCUMENT));
				noteExternalTO.setBodyDoc(BODY_DOC_5
						+ getDateLastCodeGeneration()
						+ BODY_DOC_FAREWELL_1);
				break;
			//DOC_6
			case 2012:
				noteExternalTO.setArchetype(ARCHETYPE_MRS);
				noteExternalTO.setRecipient(getRecipientNoteToExternal(2016));
				noteExternalTO.setOccupation(LEGAL_REPRESENTATIVE);
				noteExternalTO.setDepartment(DATA_FIDELIS);
				noteExternalTO.setDateHeader(CommonsUtilities.convertDateToStringLocaleES(getFirstDate(),CommonsUtilities.DATE_PATTERN_DOCUMENT));
				noteExternalTO.setBodyDoc(BODY_DOC_6
						+ getDateLastCodeGeneration()
						+ BODY_DOC_FAREWELL_1);
				break;
		}

		parametersRequired.put("dateHeader", noteExternalTO.getDateHeader());
		parametersRequired.put("quote", noteExternalTO.getQuote());
		parametersRequired.put("archetype", noteExternalTO.getArchetype());
		parametersRequired.put("recipient", noteExternalTO.getRecipient());
		parametersRequired.put("ocupation", noteExternalTO.getOccupation());
		parametersRequired.put("department", noteExternalTO.getDepartment());
		parametersRequired.put("atn", noteExternalTO.getAtn());
		parametersRequired.put("desReference", noteExternalTO.getDesReference());
		parametersRequired.put("bodyDoc", noteExternalTO.getBodyDoc());
		parametersRequired.put("attached_sheets", noteExternalTO.getAttachedSheets());

		return parametersRequired;
	}

	// METODO PARA OBTENER LA ULTIMA FECHA DE GENERACION DE CODIGOS ISIN Y CFI
	public String getDateLastCodeGeneration() {
		return CommonsUtilities.convertDateToStringLocaleES(
				reportServiceBean.getDateLastCodeGeneration(),
				ReportConstant.DATE_FORMAT_DOCUMENT);
	}

	// METODO PARA OBTENER EL DESTINATARIO DEL DOCUMENTO
	public String getRecipientNoteToExternal(Integer parameterTablePk) {
		String recipient = "";
		try {
			recipient = parameterService.findParameterTableDescription(parameterTablePk);
		} catch (Exception ex) {
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		return recipient;
	}
	
	// METODO PARA OBTENER EL PRIMER DIA HABIL DEL MES
	public Date getFirstDate(){
		Date firstDate=CommonsUtilities.getFirstBusinessDayOfMonth();
		while (holidayQueryServiceBean.isNonWorkingDayServiceBean(firstDate, false)==true) {
			firstDate=CommonsUtilities.addDaysToDate(firstDate, 1);
		}
		return firstDate;
	}
	// METODO PARA OBTENER EL ULTIMO DIA HABIL DEL MES
	
	public Date getUltimateDate(){
		Date ultimateDate=CommonsUtilities.getFirstBusinessDayOfMonth();
		while (holidayQueryServiceBean.isNonWorkingDayServiceBean(ultimateDate, false)==true) {
			ultimateDate=CommonsUtilities.addDaysToDate(ultimateDate, -1);
		}
		return ultimateDate;
	}
}