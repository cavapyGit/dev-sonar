package com.pradera.report.generation.executor.issuances;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.issuances.to.CouponsDetachmentTO;
import com.pradera.report.generation.executor.securities.service.PaymentChronogramBySecurity;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "ConstancyOfDetachmentReport")
public class ConstancyOfDetachmentReport extends GenericReport{
	private static final long serialVersionUID = 1L;
	
	@EJB
	private PaymentChronogramBySecurity securitiesService;
	@Inject
	private PraderaLogger log;
	@EJB
	private ParameterServiceBean parameterService;

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		CouponsDetachmentTO securitiesTO = new CouponsDetachmentTO();

		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("request_number")){
				if (listaLogger.get(i).getFilterValue()!=null && !listaLogger.get(i).getFilterValue().equals("0")){
					securitiesTO.setRequestNumber(listaLogger.get(i).getFilterValue());
				}
			}
		}

		//get all the query in a string format
		String strQuery = securitiesService.getConstancyOfDetachment(securitiesTO);

		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> secClassMnemonic = new HashMap<Integer, String>();
		Map<Integer,String> requestState = new HashMap<Integer, String>();
		try {
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secClassMnemonic.put(param.getParameterTablePk(), param.getText1());
			} 
			filter.setMasterTableFk(MasterTableType.SPLIT_COUPON_STATE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				requestState.put(param.getParameterTablePk(), param.getDescription());
			}
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		parametersRequired.put("str_query", strQuery);
		if(securitiesTO.getRequestState()!=null){
			parametersRequired.put("request_state_desc", requestState.get(Integer.valueOf(securitiesTO.getRequestState()).intValue()));
		}
		parametersRequired.put("map_reqStateDesc", requestState);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		
		return parametersRequired;
	}

}
