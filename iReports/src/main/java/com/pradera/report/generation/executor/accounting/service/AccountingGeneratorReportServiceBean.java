package com.pradera.report.generation.executor.accounting.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.service.AccountAccountingQueryServiceBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounting.AccountingAccount;
import com.pradera.model.accounting.AccountingProcess;
import com.pradera.model.generalparameter.type.DailyExchangeRoleType;
import com.pradera.model.generalparameter.type.InformationSourceType;
import com.pradera.report.generation.executor.accounting.to.AccountingProcessTo;
import com.pradera.report.generation.executor.accounting.to.OperationBalanceTo;
import com.pradera.report.generation.executor.billing.to.DailyExchangeRateFilter;
// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class BillingServiceReportServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/12/2014
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class AccountingGeneratorReportServiceBean extends CrudDaoServiceBean  implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The participant service bean. */
	@EJB
	ParticipantServiceBean participantServiceBean;
	
	/** The accounting service bean. */
	@EJB 
	AccountAccountingQueryServiceBean accountingServiceBean;
	
	/** The accounting report query service bean. */
	@EJB
	AccountingReportQueryServiceBean accountingReportQueryServiceBean;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	/** The accounting report service. */
	@Inject
	private AccountingReportServiceBean accountingReportService;
	

	/** The Constant STR_ZERO. */
	public static final Integer INCREMENT_MAX = 1000;
	
	/**
	 * Instantiates a new accounting generator report service bean.
	 */
	public AccountingGeneratorReportServiceBean(){

	}
	
	/**
	 * Report DocumentSecurityReport.
	 *
	 * @param filter the filter
	 * @return the map
	 * @throws ServiceException **
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	@Performance
	@TransactionTimeout(unit=TimeUnit.HOURS,value=5)
	public Map<String, Object>  findSquaringOperations(AccountingProcessTo filter) throws ServiceException{

		List<AccountingAccount> 		listAllAccount=null;
		List<AccountingAccount> 		listAccountingAccount=null;
		Map<String, Object> 			parameters = new HashMap<String, Object>();
		Map<Integer,Map> 				exchangeCalculation= new HashMap<Integer,Map>();
		Map<String,OperationBalanceTo>	mapBalance=new HashMap<String,OperationBalanceTo>();
		DailyExchangeRateFilter 		dailyExchangeRateFilter = new DailyExchangeRateFilter();
		OperationBalanceTo 				balanceAvailable=null;
		OperationBalanceTo 				balanceReported=null;
		OperationBalanceTo 				balanceReporting=null;
		OperationBalanceTo 				balanceBlock=null;
		OperationBalanceTo 				balanceOtherBlock=null;
		OperationBalanceTo 				balancePhysical=null;
		OperationBalanceTo 				balanceNotPlaced=null;
		AccountingAccount 				accountingAccount=new AccountingAccount();
		
		try{
			AccountingProcess accountingProcess=accountingReportQueryServiceBean.findAccountingProcessToFilter(filter);
			
			dailyExchangeRateFilter.setIdSourceinformation(InformationSourceType.BCRD.getCode());	
			dailyExchangeRateFilter.setInitialDate((accountingProcess.getExecutionDate()==null?null:CommonsUtilities.truncateDateTime(accountingProcess.getExecutionDate()) ));
			dailyExchangeRateFilter.setFinalDate((accountingProcess.getExecutionDate()==null?null:CommonsUtilities.truncateDateTime(accountingProcess.getExecutionDate()) ));
			exchangeCalculation=accountingReportService.chargeExchangeMemoryBuyBs(dailyExchangeRateFilter, DailyExchangeRoleType.BUY.getCode());
			parameters.put(GeneralConstants.MATRIX_EXCHANGE_RATE_BUY,exchangeCalculation);
			
			listAllAccount=accountingServiceBean.findAccountingAccountForReports(accountingAccount);
			
			/**
			 * Data to Operations SAN
			 */
			balanceAvailable=accountingReportQueryServiceBean.findBalanceForAvailable(filter);
			balanceAvailable=accountingReportService.appliedDailyExchangeToOperations(balanceAvailable, parameters);
			balanceReported=accountingReportQueryServiceBean.findBalanceForReported(filter);
			balanceReported=accountingReportService.appliedDailyExchangeToOperations(balanceReported, parameters);
			balanceReporting=accountingReportQueryServiceBean.findBalanceForReporting(filter);
			balanceReporting=accountingReportService.appliedDailyExchangeToOperations(balanceReporting, parameters);
			balanceBlock=accountingReportQueryServiceBean.findBalanceForBlocking(filter);
			balanceBlock=accountingReportService.appliedDailyExchangeToOperations(balanceBlock, parameters);
			balanceOtherBlock=accountingReportQueryServiceBean.findBalanceForOtherBlock(filter);
			balanceOtherBlock=accountingReportService.appliedDailyExchangeToOperations(balanceOtherBlock, parameters);
			balancePhysical=accountingReportQueryServiceBean.findBalanceForPhysicalBalance(filter);
			balancePhysical=accountingReportService.appliedDailyExchangeToOperations(balancePhysical, parameters);
			balanceNotPlaced=accountingReportQueryServiceBean.findBalanceForNotPlaced(filter);
			balanceNotPlaced=accountingReportService.appliedDailyExchangeToOperations(balanceNotPlaced, parameters);

			
			mapBalance.put(GeneralConstants.DESMATERIALIZADO, balanceAvailable);
			mapBalance.put(GeneralConstants.REPORTADO, balanceReported);
			mapBalance.put(GeneralConstants.REPORTANTE, balanceReporting);
			mapBalance.put(GeneralConstants.BLOQUEOS, balanceBlock);
			mapBalance.put(GeneralConstants.BLOQUEO_OTROS, balanceOtherBlock);
			mapBalance.put(GeneralConstants.FISICOS, balancePhysical);
			mapBalance.put(GeneralConstants.NOT_PLACED, balanceNotPlaced);
			
			
			/***
			 * Data to Accounting Book
			 */
			listAccountingAccount=accountingReportQueryServiceBean.findBalanceForAccounting(accountingProcess);
			listAccountingAccount=
					accountingReportService.addAccountingAccountByCurrency(listAllAccount, listAccountingAccount, parameters);
			
			/**
			 * Ordernar por portafolio y Tipo de Renta
			 */
			Collections.sort(listAccountingAccount, new Comparator<AccountingAccount>(){
				@Override
				public int compare(AccountingAccount o1, AccountingAccount o2) {
					String port1=o1.getPortFolio().toString().concat(o1.getInstrumentType().toString());
					String port2=o2.getPortFolio().toString().concat(o2.getInstrumentType().toString());
					return port1.compareTo(port2);
				}
				});
			
			parameters.put(GeneralConstants.SQUARING_OPERATIONS, mapBalance);
			parameters.put(GeneralConstants.SQUARING_ACCOUNTING, listAccountingAccount);
			parameters.put(GeneralConstants.ACCOUNTING_PROCESS,  accountingProcess);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		

	return parameters;
	}

	
	/**
	 * Report DocumentSecurityReport.
	 *
	 * @param filter the filter
	 * @return the map
	 * @throws ServiceException **
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	@Performance
	@TransactionTimeout(unit=TimeUnit.HOURS,value=5)
	public Map<String, Object>  findSquaringStartOperations(AccountingProcessTo filter) throws ServiceException{

		List<AccountingAccount> 		listAllAccount=null;
		List<AccountingAccount> 		listAccountingAccount=null;
		Map<String, Object> 			parameters = new HashMap<String, Object>();
		Map<Integer,Map> 				exchangeCalculation= new HashMap<Integer,Map>();
		Map<String,OperationBalanceTo>	mapBalance=new HashMap<String,OperationBalanceTo>();
		DailyExchangeRateFilter 		dailyExchangeRateFilter = new DailyExchangeRateFilter();
		OperationBalanceTo 				balanceAvailable=null;
		OperationBalanceTo 				balanceReported=null;
		OperationBalanceTo 				balanceReporting=null;
		OperationBalanceTo 				balanceBlock=null;
		OperationBalanceTo 				balanceOtherBlock=null;
		OperationBalanceTo 				balancePhysical=null;
		OperationBalanceTo 				balanceNotPlaced=null;
		AccountingAccount 				accountingAccount=new AccountingAccount();
		
		try{
			AccountingProcess accountingProcess=accountingReportQueryServiceBean.findAccountingProcessToFilter(filter);
			
			dailyExchangeRateFilter.setIdSourceinformation(InformationSourceType.BCRD.getCode());	
			dailyExchangeRateFilter.setInitialDate((accountingProcess.getExecutionDate()==null?null:CommonsUtilities.truncateDateTime(accountingProcess.getExecutionDate()) ));
			dailyExchangeRateFilter.setFinalDate((accountingProcess.getExecutionDate()==null?null:CommonsUtilities.truncateDateTime(accountingProcess.getExecutionDate()) ));
			exchangeCalculation=accountingReportService.chargeExchangeMemoryBuyBs(dailyExchangeRateFilter, DailyExchangeRoleType.BUY.getCode());
			parameters.put(GeneralConstants.MATRIX_EXCHANGE_RATE_BUY,exchangeCalculation);
			
			listAllAccount=accountingServiceBean.findAccountingAccountForReports(accountingAccount);
			
			/**
			 * Data to Operations SAN
			 */
			balanceAvailable=accountingReportQueryServiceBean.findBalanceForAvailable(filter);
			balanceAvailable=accountingReportService.appliedDailyExchangeToOperations(balanceAvailable, parameters);
			balanceReported=accountingReportQueryServiceBean.findBalanceForReporting(filter);
			balanceReported=accountingReportService.appliedDailyExchangeToOperations(balanceReported, parameters);
			balanceReporting=accountingReportQueryServiceBean.findBalanceForReporting(filter);
			balanceReporting=accountingReportService.appliedDailyExchangeToOperations(balanceReporting, parameters);
			balanceBlock=accountingReportQueryServiceBean.findBalanceForBlocking(filter);
			balanceBlock=accountingReportService.appliedDailyExchangeToOperations(balanceBlock, parameters);
			balanceOtherBlock=accountingReportQueryServiceBean.findBalanceForOtherBlock(filter);
			balanceOtherBlock=accountingReportService.appliedDailyExchangeToOperations(balanceOtherBlock, parameters);
			balancePhysical=accountingReportQueryServiceBean.findBalanceForPhysicalBalance(filter);
			balancePhysical=accountingReportService.appliedDailyExchangeToOperations(balancePhysical, parameters);
			balanceNotPlaced=accountingReportQueryServiceBean.findBalanceForNotPlaced(filter);
			balanceNotPlaced=accountingReportService.appliedDailyExchangeToOperations(balanceNotPlaced, parameters);

			
			mapBalance.put(GeneralConstants.DESMATERIALIZADO, balanceAvailable);
			mapBalance.put(GeneralConstants.REPORTADO, balanceReported);
			mapBalance.put(GeneralConstants.REPORTANTE, balanceReporting);
			mapBalance.put(GeneralConstants.BLOQUEOS, balanceBlock);
			mapBalance.put(GeneralConstants.BLOQUEO_OTROS, balanceOtherBlock);
			mapBalance.put(GeneralConstants.FISICOS, balancePhysical);
			mapBalance.put(GeneralConstants.NOT_PLACED, balanceNotPlaced);
			
			
			/***
			 * Data to Accounting Book
			 */
			listAccountingAccount=accountingReportQueryServiceBean.findBalanceForAccountingStart(accountingProcess);
			listAccountingAccount=
					accountingReportService.addAccountingAccountByCurrency(listAllAccount, listAccountingAccount, parameters);
			
			/**
			 * Ordernar por portafolio y Tipo de Renta
			 */
			Collections.sort(listAccountingAccount, new Comparator<AccountingAccount>(){
				@Override
				public int compare(AccountingAccount o1, AccountingAccount o2) {
					String port1=o1.getPortFolio().toString().concat(o1.getInstrumentType().toString());
					String port2=o2.getPortFolio().toString().concat(o2.getInstrumentType().toString());
					return port1.compareTo(port2);
				}
				});
			
			parameters.put(GeneralConstants.SQUARING_OPERATIONS, mapBalance);
			parameters.put(GeneralConstants.SQUARING_ACCOUNTING, listAccountingAccount);
			parameters.put(GeneralConstants.ACCOUNTING_PROCESS,  accountingProcess);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		

	return parameters;
	}


	
	/**
	 * Report DocumentSecurityReport.
	 *
	 * @param filter the filter
	 * @return the map
	 * @throws ServiceException **
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	@Performance
	@TransactionTimeout(unit=TimeUnit.HOURS,value=5)
	public Map<String, Object>  findSquaringMonthlyOperations(AccountingProcessTo filter) throws ServiceException{

		List<AccountingAccount> 		listAllAccount=null;
		List<AccountingAccount> 		listAccountingAccount=null;
		Map<String, Object> 			parameters = new HashMap<String, Object>();
		Map<Integer,Map> 				exchangeCalculation= new HashMap<Integer,Map>();
		Map<String,OperationBalanceTo>	mapBalance=new HashMap<String,OperationBalanceTo>();
		DailyExchangeRateFilter 		dailyExchangeRateFilter = new DailyExchangeRateFilter();
		OperationBalanceTo 				balanceAvailable=null;
		OperationBalanceTo 				balanceReported=null;
		OperationBalanceTo 				balanceReporting=null;
		OperationBalanceTo 				balanceBlock=null;
		OperationBalanceTo 				balanceOtherBlock=null;
		OperationBalanceTo 				balancePhysical=null;
		OperationBalanceTo 				balanceNotPlaced=null;
		AccountingAccount 				accountingAccount=new AccountingAccount();
		
		try{
			AccountingProcess accountingProcess=accountingReportQueryServiceBean.findAccountingProcessToFilter(filter);
			
			dailyExchangeRateFilter.setIdSourceinformation(InformationSourceType.BCRD.getCode());	
			dailyExchangeRateFilter.setInitialDate((accountingProcess.getExecutionDate()==null?null:CommonsUtilities.truncateDateTime(accountingProcess.getExecutionDate()) ));
			dailyExchangeRateFilter.setFinalDate((accountingProcess.getExecutionDate()==null?null:CommonsUtilities.truncateDateTime(accountingProcess.getExecutionDate()) ));
			exchangeCalculation=accountingReportService.chargeExchangeMemoryBuyBs(dailyExchangeRateFilter, DailyExchangeRoleType.BUY.getCode());
			parameters.put(GeneralConstants.MATRIX_EXCHANGE_RATE_BUY,exchangeCalculation);
			
			listAllAccount=accountingServiceBean.findAccountingAccountForReports(accountingAccount);
			filter.setExecutionDate(accountingProcess.getExecutionDate());
			/**
			 * Data to Operations SAN
			 */
			balanceAvailable=accountingReportQueryServiceBean.findBalanceForAvailableMonthly(filter);
			balanceAvailable=accountingReportService.appliedDailyExchangeToOperations(balanceAvailable, parameters);
			balanceReported=accountingReportQueryServiceBean.findBalanceForReportingMonthly(filter);
			balanceReported=accountingReportService.appliedDailyExchangeToOperations(balanceReported, parameters);
			balanceReporting=accountingReportQueryServiceBean.findBalanceForReportingMonthly(filter);
			balanceReporting=accountingReportService.appliedDailyExchangeToOperations(balanceReporting, parameters);
			balanceBlock=accountingReportQueryServiceBean.findBalanceForBlockingMonthly(filter);
			balanceBlock=accountingReportService.appliedDailyExchangeToOperations(balanceBlock, parameters);
			balanceOtherBlock=accountingReportQueryServiceBean.findBalanceForOtherBlockMonthly(filter);
			balanceOtherBlock=accountingReportService.appliedDailyExchangeToOperations(balanceOtherBlock, parameters);
			balancePhysical=accountingReportQueryServiceBean.findBalanceForPhysicalBalanceMonthly(filter);
			balancePhysical=accountingReportService.appliedDailyExchangeToOperations(balancePhysical, parameters);
			balanceNotPlaced=accountingReportQueryServiceBean.findBalanceForNotPlacedMonthly(filter);
			balanceNotPlaced=accountingReportService.appliedDailyExchangeToOperations(balanceNotPlaced, parameters);

			
			mapBalance.put(GeneralConstants.DESMATERIALIZADO, balanceAvailable);
			mapBalance.put(GeneralConstants.REPORTADO, balanceReported);
			mapBalance.put(GeneralConstants.REPORTANTE, balanceReporting);
			mapBalance.put(GeneralConstants.BLOQUEOS, balanceBlock);
			mapBalance.put(GeneralConstants.BLOQUEO_OTROS, balanceOtherBlock);
			mapBalance.put(GeneralConstants.FISICOS, balancePhysical);
			mapBalance.put(GeneralConstants.NOT_PLACED, balanceNotPlaced);
			
			
			/***
			 * Data to Accounting Book
			 */
			listAccountingAccount=accountingReportQueryServiceBean.findBalanceForAccountingMonthly(accountingProcess);
			listAccountingAccount=
					accountingReportService.addAccountingAccountByCurrency(listAllAccount, listAccountingAccount, parameters);
			
			/**
			 * Ordernar por portafolio y Tipo de Renta
			 */
			Collections.sort(listAccountingAccount, new Comparator<AccountingAccount>(){
				@Override
				public int compare(AccountingAccount o1, AccountingAccount o2) {
					String port1=o1.getPortFolio().toString().concat(o1.getInstrumentType().toString());
					String port2=o2.getPortFolio().toString().concat(o2.getInstrumentType().toString());
					return port1.compareTo(port2);
				}
				});
			
			parameters.put(GeneralConstants.SQUARING_OPERATIONS, mapBalance);
			parameters.put(GeneralConstants.SQUARING_ACCOUNTING, listAccountingAccount);
			parameters.put(GeneralConstants.ACCOUNTING_PROCESS,  accountingProcess);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		

	return parameters;
	}
	
	/**
	 * Report DocumentSecurityReport
	 * ACCOUNTING_SEATS_REPORT.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException **
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	@Performance
	@TransactionTimeout(unit=TimeUnit.HOURS,value=5)
	public List<AccountingAccount>  findAccountBalance(AccountingProcessTo filter) throws ServiceException{
		
		Map<String, Object> 			parameters = new HashMap<String, Object>();
		Map<Integer,Map> 				exchangeCalculation= new HashMap<Integer,Map>();

		
		AccountingAccount 				accountingAccount=new AccountingAccount();
		DailyExchangeRateFilter 		dailyExchangeRateFilter = new DailyExchangeRateFilter();
		
		List<AccountingAccount> 		listAllAccount=null;
		
		try{
			AccountingProcess accountingProcess=accountingReportQueryServiceBean.findAccountingProcessToFilter(filter);
			
			dailyExchangeRateFilter.setIdSourceinformation(InformationSourceType.BCRD.getCode());	
			dailyExchangeRateFilter.setInitialDate((accountingProcess.getExecutionDate()==null?null:CommonsUtilities.truncateDateTime(accountingProcess.getExecutionDate()) ));
			dailyExchangeRateFilter.setFinalDate((accountingProcess.getExecutionDate()==null?null:CommonsUtilities.truncateDateTime(accountingProcess.getExecutionDate()) ));
			exchangeCalculation=accountingReportService.chargeExchangeMemoryBuyBs(dailyExchangeRateFilter, DailyExchangeRoleType.BUY.getCode());
			parameters.put(GeneralConstants.MATRIX_EXCHANGE_RATE_BUY,exchangeCalculation);
		
			
//			listAllAccount=accountingServiceBean.findAccountingAccountByFilter(accountingAccount);
			listAllAccount=accountingServiceBean.findAccountingAccountForReports(accountingAccount);
			Collections.sort(listAllAccount);
			TreeSet<AccountingAccount> listOrderBy=new TreeSet<AccountingAccount>(listAllAccount);
			List<AccountingAccount> listAccFinal=new ArrayList<>(listOrderBy);

			em.flush();
			em.clear();

			
			List<AccountingAccount> listAcc=new ArrayList<AccountingAccount>();
			
			
			listAcc=accountingReportQueryServiceBean.gettingAccountingAccountByBalanceProcess(accountingProcess);
			
			
			listAccFinal=accountingReportService.addListAccountings(listAccFinal, listAcc);
			/** 
			 * Aplicar los tipos de cambio
			 * */
			listAllAccount= accountingReportService.setAmountAllCurrencies(listAccFinal, parameters);
			 
			listAllAccount= accountingReportService.toAddAccountingFather(listAllAccount);
		}catch(Exception e){
			e.printStackTrace();
		}

		return listAllAccount;
	}
	
	
	/**
	 * Report DocumentSecurityReport
	 * ACCOUNTING_BALANCE_REPORT.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException **
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	@Performance
	@TransactionTimeout(unit=TimeUnit.HOURS,value=5)
	public List<AccountingAccount>  findAccountBalanceHistory(AccountingProcessTo filter) throws ServiceException{
		
		Map<String, Object> 			parameters = new HashMap<String, Object>();
		Map<Integer,Map> 				exchangeCalculation= new HashMap<Integer,Map>();

		
		AccountingAccount 				accountingAccount=new AccountingAccount();
		DailyExchangeRateFilter 		dailyExchangeRateFilter = new DailyExchangeRateFilter();
		
		List<AccountingAccount> 		listAllAccount=null;
		
		try{
			AccountingProcess accountingProcess=accountingReportQueryServiceBean.findAccountingProcessToFilter(filter);
			
			dailyExchangeRateFilter.setIdSourceinformation(InformationSourceType.BCRD.getCode());	
			dailyExchangeRateFilter.setInitialDate((accountingProcess.getExecutionDate()==null?null:CommonsUtilities.truncateDateTime(accountingProcess.getExecutionDate()) ));
			dailyExchangeRateFilter.setFinalDate((accountingProcess.getExecutionDate()==null?null:CommonsUtilities.truncateDateTime(accountingProcess.getExecutionDate()) ));
			exchangeCalculation=accountingReportService.chargeExchangeMemoryBuyBs(dailyExchangeRateFilter, DailyExchangeRoleType.BUY.getCode());
			parameters.put(GeneralConstants.MATRIX_EXCHANGE_RATE_BUY,exchangeCalculation);
		
			listAllAccount=accountingServiceBean.findAccountingAccountForReports(accountingAccount);
			Collections.sort(listAllAccount);
			TreeSet<AccountingAccount> listOrderBy=new TreeSet<AccountingAccount>(listAllAccount);
			List<AccountingAccount> listAccFinal=new ArrayList<>(listOrderBy);

			em.flush();
			em.clear();

			
			List<AccountingAccount> listAcc=new ArrayList<AccountingAccount>();
			
			
			listAcc=accountingReportQueryServiceBean.gettingAccountingAccountByBalanceProcessHistory(accountingProcess);
			
			
			listAccFinal=accountingReportService.addListAccountings(listAccFinal, listAcc);
			/** 
			 * Aplicar los tipos de cambio
			 * */
			listAllAccount= accountingReportService.setAmountAllCurrencies(listAccFinal, parameters);
			 
			listAllAccount= accountingReportService.toAddAccountingFather(listAllAccount);
		}catch(Exception e){
			e.printStackTrace();
		}

		return listAllAccount;
	}
	
}

