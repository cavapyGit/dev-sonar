package com.pradera.report.generation.executor.corporative;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.corporative.service.CorporativeReportServiceBean;
import com.pradera.report.generation.executor.corporative.to.CorporativeProcesInterestTO;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

@ReportProcess(name="InterestPaymentControlListReport")
public class InterestPaymentControlListReport extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6417535267455863179L;
	
	@Inject
	PraderaLogger log;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	CorporativeReportServiceBean reportCorporative;
	
	private static Integer STATE_DESCRIPTION = new Integer(0);
	private static Integer EMISOR_DESCRIPTION = new Integer(1);
	private static Integer EMISION_DESCRIPTION = new Integer(2);
	private static Integer VALOR_DESCRIPTION = new Integer(3);
	private static Integer MONEDA_DESCRIPTION = new Integer(4);
	private static Integer FECHA_PAGO = new Integer(5);
	private static Integer FECHA_REGISTRO = new Integer(6);
	private static Integer NUMERO_CUPON = new Integer(7);
	private static Integer VALOR_NOMINAL = new Integer(8);
	private static Integer FACTOR_INT = new Integer(9);
	private static Integer IMPUESTO = new Integer(10);
	private static Integer IND_REDONDEO = new Integer(11);
	private static Integer FECHA_CORTE = new Integer(12);
	private static Integer PARTICIPANTE = new Integer(13);
	private static Integer SALDO_STOCK_FECHA_CORTE = new Integer(14);
	private static Integer SALDO_INTEREST_NETO_MONEDA = new Integer(15);
	private static Integer SALDO_NUMER_TITUTLARES = new Integer(16);
	private static Integer VALORES_STOCK_FECHA_CORTE = new Integer(17);
	private static Integer VALORES_INTEREST_NETO_MONEDA = new Integer(18);
	private static Integer VALORES_NUMERO_TITULARES = new Integer(19);
	private static Integer TOTAL_STOCK_FECHA_CORTE = new Integer(20);
	private static Integer TOTAL_INTEREST_NETO_MONEDA = new Integer(21);
	private static Integer CODIGO_ALTERNO = new Integer(22);
	
	public InterestPaymentControlListReport() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		boolean flagDataExist = false;
		List<Object[]> CorporativePRocessInterestList =null;
		
		CorporativeProcesInterestTO corporativeTo = reportLoggerCorporativeProcessInterestTo(reportLogger);
		CorporativePRocessInterestList =  reportCorporative.searchInterestPaymentControl(corporativeTo);
		
		if(CorporativePRocessInterestList.size()>0)
			flagDataExist=true;
			
		try {
			
			XMLOutputFactory xof = XMLOutputFactory.newInstance();
			XMLStreamWriter xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(baos));
			
			xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");//<XML>
			xmlsw.writeStartElement(ReportConstant.REPORT);//<report>
			
			createHeaderReport( xmlsw, reportLogger );//<header></header>
			
			xmlsw.writeStartElement("DataQuery");//<DataQuery>
				if(flagDataExist){
					createBodyReport(xmlsw, CorporativePRocessInterestList);//<body></body>
				}else{
					createBodyReportMock(xmlsw);
				}
			xmlsw.writeEndElement();//</DataQuery>
			
			String hourEnd = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),ReportConstant.DATE_FORMAT_TIME_SECOND);
			createTagString(xmlsw, ReportConstant.END_HOUR, hourEnd); //<end_hour></end_hour>
	//		createTagString(xmlsw, ReportConstant.DATE_INITIAL_PARAM, dateFormat.format(corporativeTo.getOperationInitDate()));
	//		createTagString(xmlsw, ReportConstant.DATE_END_PARAM, dateFormat.format(corporativeTo.getOperationEndDate()));
	//		createTagString(xmlsw, ReportConstant.PARAMETERS_TAG_REPORT, strParameters(corporativeTo)); //<parameters></parameters>
			
			xmlsw.writeEndElement();//</report>
			xmlsw.writeEndDocument();//</XML>
			
			xmlsw.flush();
	        xmlsw.close();
	        
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return baos;
	}
	
	
	/**
	 * Creates the body report.
	 * good practice factoring
	 * @param xmlsw the xmlsw
	 */
	
	public void createBodyReportMock(XMLStreamWriter xmlsw)  throws XMLStreamException{

		xmlsw.writeStartElement("reports");
			xmlsw.writeStartElement("report");
				createTagString (xmlsw, "state_description",  "");
				createTagString (xmlsw, "emisor_description", "" );
				createTagString (xmlsw, "emision_description",  "" );
				createTagString (xmlsw, "valor_description", "" );
				createTagString (xmlsw, "moneda_description",  "" );
				createTagString (xmlsw, "fecha_pago",  "" );
				createTagString (xmlsw, "fecha_registro", "" );
				createTagString (xmlsw, "impuesto",  "" );
				createTagString (xmlsw, "ind_redondeo",  "" );
				createTagString (xmlsw, "numero_cupon",  "" );
				createTagString (xmlsw, "valor_nominal",  "" );
				createTagString (xmlsw, "factor_int",  "" );
				createTagString (xmlsw, "fecha_corte",  "" );
				createTagString (xmlsw, "participante",  "" );
				createTagString (xmlsw, "saldo_stock_fecha_corte",  "" );
				createTagString (xmlsw, "saldo_interest_neto_moneda",  "" );
				createTagString (xmlsw, "saldo_numer_titulares",  "" );
				createTagString (xmlsw, "valores_stock_fecha_corte",  "" );
				createTagString (xmlsw, "valores_interest_neto_moneda",  "" );
				createTagString (xmlsw, "valores_numero_titulares",  "" );
				createTagString (xmlsw, "total_stock_fecha_corte",  "" );
				createTagString (xmlsw, "total_interest_neto_moneda",  "" );
				createTagString (xmlsw, "codigo_alterno",  "" );

			xmlsw.writeEndElement();
		xmlsw.writeEndElement();

	}
	
	public void createBodyReport(XMLStreamWriter xmlsw,
								 List<Object[]> transferAvailableList)  throws XMLStreamException{

		xmlsw.writeStartElement("reports");
		for(Object[] queryResult : transferAvailableList) {
			
			xmlsw.writeStartElement("report");
			
			if(Validations.validateIsNotNull(queryResult[STATE_DESCRIPTION])){
				createTagString (xmlsw, "state_description",  queryResult[STATE_DESCRIPTION].toString() );
			}else{
				createTagString (xmlsw, "state_description",  "");
			}
			
			if(Validations.validateIsNotNull(queryResult[EMISOR_DESCRIPTION])){
				createTagString (xmlsw, "emisor_description", queryResult[EMISOR_DESCRIPTION].toString() );
			}else{
				createTagString (xmlsw, "emisor_description", "" );
			}
			
			if(Validations.validateIsNotNull(queryResult[EMISION_DESCRIPTION])){
				createTagString (xmlsw, "emision_description",  queryResult[EMISION_DESCRIPTION].toString() );
			}else{
				createTagString (xmlsw, "emision_description",  "" );
			}
			
			if(Validations.validateIsNotNull(queryResult[VALOR_DESCRIPTION])){
				
				createTagString (xmlsw, "valor_description",  queryResult[VALOR_DESCRIPTION].toString() );
			}else{
				createTagString (xmlsw, "valor_description", "" );
			}
			
			if(Validations.validateIsNotNull(queryResult[MONEDA_DESCRIPTION])){
				createTagString (xmlsw, "moneda_description",  queryResult[MONEDA_DESCRIPTION].toString() );
			}else{
				createTagString (xmlsw, "moneda_description",  "" );
			}
			
			if(Validations.validateIsNotNull(queryResult[FECHA_PAGO])){
				createTagString (xmlsw, "fecha_pago",  queryResult[FECHA_PAGO].toString() );
			}else{
				createTagString (xmlsw, "fecha_pago",  "" );
			}
			
			if(Validations.validateIsNotNull(queryResult[FECHA_REGISTRO])){
				createTagString (xmlsw, "fecha_registro",  queryResult[FECHA_REGISTRO].toString() );
			}else{
				createTagString (xmlsw, "fecha_registro", "" );
			}

			if(Validations.validateIsNotNull(queryResult[IMPUESTO])){
				createTagString (xmlsw, "impuesto", queryResult[IMPUESTO].toString()  );
			}else{
				createTagString (xmlsw, "impuesto",  "" );
			}
			
			if(Validations.validateIsNotNull(queryResult[IND_REDONDEO])){
				createTagString (xmlsw, "ind_redondeo", queryResult[IND_REDONDEO].toString() );
			}else{
				createTagString (xmlsw, "ind_redondeo",  "" );
			}
					
			if(Validations.validateIsNotNull(queryResult[NUMERO_CUPON])){
				createTagString (xmlsw, "numero_cupon",  queryResult[NUMERO_CUPON].toString() );
			}else{
				createTagString (xmlsw, "numero_cupon",  "" );
			}
			
			if(Validations.validateIsNotNull(queryResult[VALOR_NOMINAL])){
				createTagString (xmlsw, "valor_nominal",  queryResult[VALOR_NOMINAL].toString() );
			}else{
				createTagString (xmlsw, "valor_nominal",  "" );
			}
			
			if(Validations.validateIsNotNull(queryResult[FACTOR_INT])){
				createTagString (xmlsw, "factor_int",  queryResult[FACTOR_INT].toString() );
			}else{
				createTagString (xmlsw, "factor_int",  "" );
			}
			
			if(Validations.validateIsNotNull(queryResult[FECHA_CORTE])){
				createTagString (xmlsw, "fecha_corte",  queryResult[FECHA_CORTE].toString() );
			}else{
				createTagString (xmlsw, "fecha_corte",  "" );
			}
						
			if(Validations.validateIsNotNull(queryResult[PARTICIPANTE])){
				createTagString (xmlsw, "participante",  queryResult[PARTICIPANTE].toString() );
			}else{
				createTagString (xmlsw, "participante",  "" );
			}
			
			if(Validations.validateIsNotNull(queryResult[SALDO_STOCK_FECHA_CORTE])){
				createTagString (xmlsw, "saldo_stock_fecha_corte",  queryResult[SALDO_STOCK_FECHA_CORTE].toString() );
			}else{
				createTagString (xmlsw, "saldo_stock_fecha_corte",  "" );
			}
			
			if(Validations.validateIsNotNull(queryResult[SALDO_INTEREST_NETO_MONEDA])){
				createTagString (xmlsw, "saldo_interest_neto_moneda",  queryResult[SALDO_INTEREST_NETO_MONEDA].toString() );
			}else{
				createTagString (xmlsw, "saldo_interest_neto_moneda",  "" );
			}
			
			if(Validations.validateIsNotNull(queryResult[SALDO_NUMER_TITUTLARES])){
				createTagString (xmlsw, "saldo_numer_titulares",  queryResult[SALDO_NUMER_TITUTLARES].toString() );
			}else{
				createTagString (xmlsw, "saldo_numer_titulares",  "" );
			}
			
			if(Validations.validateIsNotNull(queryResult[VALORES_STOCK_FECHA_CORTE])){
				createTagString (xmlsw, "valores_stock_fecha_corte",  queryResult[VALORES_STOCK_FECHA_CORTE].toString() );
			}else{
				createTagString (xmlsw, "valores_stock_fecha_corte",  "" );
			}
			
			if(Validations.validateIsNotNull(queryResult[VALORES_INTEREST_NETO_MONEDA])){
				createTagString (xmlsw, "valores_interest_neto_moneda",  queryResult[VALORES_INTEREST_NETO_MONEDA].toString() );
			}else{
				createTagString (xmlsw, "valores_interest_neto_moneda",  "" );
			}
			
			if(Validations.validateIsNotNull(queryResult[VALORES_NUMERO_TITULARES])){
				createTagString (xmlsw, "valores_numero_titulares",  queryResult[VALORES_NUMERO_TITULARES].toString() );
			}else{
				createTagString (xmlsw, "valores_numero_titulares",  "" );
			}
			
			if(Validations.validateIsNotNull(queryResult[TOTAL_STOCK_FECHA_CORTE])){
				createTagString (xmlsw, "total_stock_fecha_corte",  queryResult[TOTAL_STOCK_FECHA_CORTE].toString() );
			}else{
				createTagString (xmlsw, "total_stock_fecha_corte",  "" );
			}

			if(Validations.validateIsNotNull(queryResult[TOTAL_INTEREST_NETO_MONEDA])){
				createTagString (xmlsw, "total_interest_neto_moneda",  queryResult[TOTAL_INTEREST_NETO_MONEDA].toString() );
			}else{
				createTagString (xmlsw, "total_interest_neto_moneda",  "" );
			}

			if(Validations.validateIsNotNull(queryResult[CODIGO_ALTERNO])){
				createTagString (xmlsw, "codigo_alterno",  queryResult[CODIGO_ALTERNO].toString() );
			}else{
				createTagString (xmlsw, "codigo_alterno",  "" );
			}
						
			xmlsw.writeEndElement();
			
		}
		xmlsw.writeEndElement();
	}
	
	public CorporativeProcesInterestTO reportLoggerCorporativeProcessInterestTo(ReportLogger reportLogger){
		
		CorporativeProcesInterestTO objecto = new CorporativeProcesInterestTO();
		
		for (ReportLoggerDetail detail : reportLogger.getReportLoggerDetails()) {

			if( detail.getFilterName().equals(ReportConstant.CORP_PROCESS_ID) ){
				if(Validations.validateIsNotNull(detail.getFilterValue())){
					objecto.setProcessId(new Long(detail.getFilterValue().toString()));
				}
			}else if( detail.getFilterName().equals(ReportConstant.CUTOFF_DATE) ){
				if(Validations.validateIsNotNull(detail.getFilterValue())){
					objecto.setCutoffDate( CommonsUtilities.convertStringtoDate(detail.getFilterValue().toString()) );
				}
			}else if( detail.getFilterName().equals(ReportConstant.DELIVERY_DATE) ){
				if(Validations.validateIsNotNull(detail.getFilterValue())){
					objecto.setDeliveryDate( CommonsUtilities.convertStringtoDate((detail.getFilterValue().toString())) );
				}
			}else if( detail.getFilterName().equals(ReportConstant.CORPORATE_EVENT_TYPE) ){
				if(Validations.validateIsNotNull(detail.getFilterValue())){
					objecto.setEventType(new Integer(detail.getFilterValue().toString()));
				}
			}else if( detail.getFilterName().equals(ReportConstant.CODE_ISIN) ){
				if(Validations.validateIsNotNull(detail.getFilterValue())){
					objecto.setCodeValue(detail.getFilterValue().toString());
				}
			}else if( detail.getFilterName().equals(ReportConstant.DEST_CODE_ISIN) ){
				if(Validations.validateIsNotNull(detail.getFilterValue())){
					objecto.setDestCodeValue(detail.getFilterValue().toString());
				}
			}else if( detail.getFilterName().equals(ReportConstant.REPORT_TYPE) ){
				if(Validations.validateIsNotNull(detail.getFilterValue())){
					objecto.setReportType( new Integer(detail.getFilterValue().toString()) );
				}
			}else if( detail.getFilterName().equals(ReportConstant.STATE_PARAM) ){
				if(Validations.validateIsNotNull(detail.getFilterValue())){
					objecto.setState( new Integer(detail.getFilterValue().toString()) );
				}
			}
			
		}
		
		return objecto;
	}

	public String strParameters(CorporativeProcesInterestTO interestTo){
		String parametrosRetorno = "";
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		parametrosRetorno += "Tipo de Evento: ";
		if( Validations.validateIsNotNullAndPositive(interestTo.getEventType()) ){
			parametrosRetorno += ""+interestTo.getEventType();
		}else{
			parametrosRetorno += "--";
		}


		parametrosRetorno += ". ";
		
		return parametrosRetorno;
	}

	
}
