package com.pradera.report.generation.executor.securities;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.model.report.ReportLogger;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "DetailDialSecAtMarketPricesDayReport")
public class DetailDialSecAtMarketPricesDayReport extends GenericReport{
private static final long serialVersionUID = 1L;

@Inject  PraderaLogger log;

public DetailDialSecAtMarketPricesDayReport() {
}
	
@Override
public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public void addParametersQueryReport() { }

@Override
public Map<String, Object> getCustomJasperParameters() {
	Map<String,Object> parametersRequired = new HashMap<>();
	
	// TODO Auto-generated method stub
	parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());

	return parametersRequired;
	}

}
