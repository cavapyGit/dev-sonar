package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.executor.custody.to.GenericCustodyOperationTO;

@ReportProcess(name = "EarlyRedemptionsOfDirectSaleBCBReport")
public class EarlyRedemptionsOfDirectSaleBCBReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;

	@EJB
	private CustodyReportServiceBean custodyReportServiceBean;
	
	@Inject
	private PraderaLogger log;
	
	
	public EarlyRedemptionsOfDirectSaleBCBReport() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		GenericCustodyOperationTO gfto = new GenericCustodyOperationTO();

		for(ReportLoggerDetail param: getReportLogger().getReportLoggerDetails()){
			if(param.getFilterName().equals("cui_code")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setHolder(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("holder_account")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setHolderAccount(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("participant")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setParticipant(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("security_class_key")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setSecurities(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("date_initial")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setInitialDate(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("date_end")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setFinalDate(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("state")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setState(param.getFilterValue().toString());
				}
			}
		} 
		
		Integer institutionType = custodyReportServiceBean.searchInstitutionType(super.getLoggerUser());
		String strQuery = custodyReportServiceBean.getEarlyRedemptionsOfDirectSaleBCBReport(gfto,institutionType); 
		
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> secSecurityClass			= new HashMap<Integer, String>();
		Map<Integer,String> secDocType		 			= new HashMap<Integer, String>();
		Map<Integer,String> secCurrency	 				= new HashMap<Integer, String>();

		
		try {
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secSecurityClass.put(param.getParameterTablePk(), param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secDocType.put(param.getParameterTablePk(), param.getIndicator1());
			}
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secCurrency.put(param.getParameterTablePk(), param.getDescription());
			}


			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}

		parametersRequired.put("str_Query", strQuery);
		parametersRequired.put("initialDt", gfto.getInitialDate());
		parametersRequired.put("finalDt", gfto.getFinalDate());
		parametersRequired.put("mSecurityClass", secSecurityClass);
		parametersRequired.put("mDocumentType", secDocType);
		parametersRequired.put("mCurrency", secCurrency);
		return parametersRequired;
	}
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
}
