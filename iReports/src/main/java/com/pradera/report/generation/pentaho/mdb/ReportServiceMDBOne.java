package com.pradera.report.generation.pentaho.mdb;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.type.ReportLoggerStateType;
import com.pradera.report.ReportContext;
import com.pradera.report.buscomp.ProcessExecutionManager;
import com.pradera.report.generation.ReportProcessLiteral;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.service.ReportUtilServiceBean;
import com.pradera.report.generation.type.QueueType;
 

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ReportServiceMDBOne.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/07/2013
 */
@MessageDriven(mappedName = "queue/ReportProcessReceiveQueueOne", activationConfig = {
@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/ReportProcessReceiveQueueOne"),
@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "AUTO_ACKNOWLEDGE") })
public class ReportServiceMDBOne implements MessageListener {
 
	/** The log. */
	@Inject
	PraderaLogger log;

	/** The process execution manager. */
	@Inject
	ProcessExecutionManager processExecutionManager;
	
	/** The generic report. */
	@Inject @Any
	Instance<GenericReport> genericReport;
	
	/** The dao service. */
	@EJB
	CrudDaoServiceBean daoService;
	
	@EJB
	ReportUtilServiceBean reportUtilService;
	
	/**
	 * This method is automatically called by MDB when the Message object is placed in the Queue.
	 *
	 * @param msg Message object
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void onMessage(final Message msg) {
		if (msg instanceof ObjectMessage) {
			final ObjectMessage objectMessage = (ObjectMessage) msg;
			try {
				ReportContext reportContext = (ReportContext)objectMessage.getObject();
				if(reportContext!=null){
					try {
						//get report implementation
						GenericReport report = getReport(reportContext.getReportImplementationBean());
						report.setReportContext(reportContext);
						//load reportLogger
						Map<String,Object> parameter = new HashMap<String, Object>();
						parameter.put("reportLogger", reportContext.getProcessLoggerId());
						ReportLogger reportLogger = daoService.findEntityByNamedQuery(ReportLogger.class,ReportLogger.REPORTLOGGER__FIND_DETAILS, parameter);
						report.setReportLogger(reportLogger);
						processExecutionManager.startReportProcess(QueueType.ONE.getCode(), report);
					
					}catch (Exception exc) {
						ReportLogger reportLogger = daoService.find(ReportLogger.class, reportContext.getProcessLoggerId());
						reportUtilService.updateStateReportLoggerTx(reportLogger,ReportLoggerStateType.FAILED.getCode(), exc.getMessage());
					}
				}else{
					log.info("report message is null");	
				}
			}catch (Exception exc) {
				//change state from reportLogger
				log.error(exc.getMessage());
				exc.printStackTrace();
				
			}
			
		} else {
			log.warn("\n\n\t\t#####################got unknown messagetype##################\n\n");
		}
		log.info("\n\n-------------------> onMessage completed");	
	}
 
	/**
	 * Gets the report.
	 *
	 * @param reportName the report name
	 * @return the report
	 */
	private GenericReport getReport(String reportName){
		return this.genericReport.select(new ReportProcessLiteral(reportName)).get();
	}
}
