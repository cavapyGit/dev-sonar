package com.pradera.report.generation.pentaho;

import java.io.IOException;

import javax.ejb.EJB;

import org.pentaho.reporting.engine.classic.core.DataFactory;
import org.pentaho.reporting.engine.classic.extensions.datasources.xpath.XPathDataFactory;

import com.pradera.model.report.type.ReportType;
import com.pradera.report.generation.service.ReportUtilServiceBean;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class BeanReportGenerator.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/07/2013
 */
@ReportImplementation(type=ReportType.BEAN)
public class BeanReportGenerator extends ReportGenerator {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 94566587510360560L;
	
	/** The report util service. */
	@EJB
	ReportUtilServiceBean reportUtilService;

	/**
	 * Instantiates a new bean report generator.
	 */
	public BeanReportGenerator() {
		super();
	}

	/* (non-Javadoc)
	 * @see com.pradera.report.generation.pentaho.ReportGenerator#getDataFactory()
	 */
	@Override
	public DataFactory getDataFactory() {
		// query result is xml report logger
		 XPathDataFactory dataFactory = new XPathDataFactory();
		 try {
		 String xmlTemp = reportUtilService.getXmlDataSourceReport(getReportLogger());
		 dataFactory.setXqueryDataFile(xmlTemp);
		 dataFactory.setQuery(QUERY_NAME, getXmlDataSource(),false);
		 } catch(IOException iox) {
			 iox.printStackTrace();
			 throw new RuntimeException(iox);
		 }
		return dataFactory;
	}

	@Override
	public void setXmlSubDataSource(String subXmlDataSource) {
		// TODO Auto-generated method stub
		
	}

	
}
