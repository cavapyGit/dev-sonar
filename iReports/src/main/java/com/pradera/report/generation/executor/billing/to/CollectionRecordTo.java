package com.pradera.report.generation.executor.billing.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.Issuer;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CollectionRecordTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class CollectionRecordTo implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id collection record pk. */
	private Long idCollectionRecordPk;
	
	/** The client type. */
	private Integer clientType;
	
	/** The collection amount. */
	private BigDecimal collectionAmount;
	
	/** The gross amount. */
	private BigDecimal grossAmount;
	
	/** The tax applied. */
	private BigDecimal taxApplied;
	
	/** The ind billed. */
	private Integer indBilled;
	
	/** The currency type. */
	private Integer currencyType;
	
	/** The movement count. */
	private Integer movementCount;
	
	/** The other entity description. */
	private String otherEntityDescription;
	
	/** The other entity document. */
	private String otherEntityDocument;
	
	/** The record state. */
	private Integer recordState;
	
	/** The holder account. */
	private HolderAccount holderAccount;
	
	/** The issuer. */
	private Issuer issuer;
	
	/** The participant. */
	private Participant participant;
	
	/** The holder. */
	private Holder holder;
	
	/** The entity collection. */
	private Integer entityCollection;
	
	/** The processed service to. */
	private ProcessedServiceTo processedServiceTo;
	
	/** The description entity collection. */
	private String descriptionEntityCollection;
	
	/** The description ind billed. */
	private String descriptionIndBilled;
	
	/** The calculation date. */
	private Date calculationDate;
	
	/** The operation date. */
	private Date operationDate;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/** The description status record. */
	private String descriptionStatusRecord;
	
	/** The id processed service fk. */
	private Long idProcessedServiceFk;
	
	/** The description currency type. */
	private String descriptionCurrencyType;
	
	/** The collection amount accumulated. */
	private BigDecimal collectionAmountAccumulated;
	
	/** The gross amount accumulated. */
	private BigDecimal grossAmountAccumulated;
	
	/** The tax applied accumulated. */
	private BigDecimal taxAppliedAccumulated;
	
	/** The issuer code. */
	private String issuerCode;
	
	/** The participant code. */
	private Long participantCode;
	
	/** The holder code. */
	private Long holderCode; 
	
	/**
	 * Gets the id collection record pk.
	 *
	 * @return the id collection record pk
	 */
	public Long getIdCollectionRecordPk() {
		return idCollectionRecordPk;
	}
	
	/**
	 * Sets the id collection record pk.
	 *
	 * @param idCollectionRecordPk the new id collection record pk
	 */
	public void setIdCollectionRecordPk(Long idCollectionRecordPk) {
		this.idCollectionRecordPk = idCollectionRecordPk;
	}
	
	/**
	 * Gets the client type.
	 *
	 * @return the client type
	 */
	public Integer getClientType() {
		return clientType;
	}
	
	/**
	 * Sets the client type.
	 *
	 * @param clientType the new client type
	 */
	public void setClientType(Integer clientType) {
		this.clientType = clientType;
	}
	
	/**
	 * Gets the collection amount.
	 *
	 * @return the collection amount
	 */
	public BigDecimal getCollectionAmount() {
		return collectionAmount;
	}
	
	/**
	 * Sets the collection amount.
	 *
	 * @param collectionAmount the new collection amount
	 */
	public void setCollectionAmount(BigDecimal collectionAmount) {
		this.collectionAmount = collectionAmount;
	}
	
	/**
	 * Gets the gross amount.
	 *
	 * @return the gross amount
	 */
	public BigDecimal getGrossAmount() {
		return grossAmount;
	}
	
	/**
	 * Sets the gross amount.
	 *
	 * @param grossAmount the new gross amount
	 */
	public void setGrossAmount(BigDecimal grossAmount) {
		this.grossAmount = grossAmount;
	}
	
	/**
	 * Gets the tax applied.
	 *
	 * @return the tax applied
	 */
	public BigDecimal getTaxApplied() {
		return taxApplied;
	}
	
	/**
	 * Sets the tax applied.
	 *
	 * @param taxApplied the new tax applied
	 */
	public void setTaxApplied(BigDecimal taxApplied) {
		this.taxApplied = taxApplied;
	}
	
	/**
	 * Gets the ind billed.
	 *
	 * @return the ind billed
	 */
	public Integer getIndBilled() {
		return indBilled;
	}
	
	/**
	 * Sets the ind billed.
	 *
	 * @param indBilled the new ind billed
	 */
	public void setIndBilled(Integer indBilled) {
		this.indBilled = indBilled;
	}
	
	/**
	 * Gets the currency type.
	 *
	 * @return the currency type
	 */
	public Integer getCurrencyType() {
		return currencyType;
	}
	
	/**
	 * Sets the currency type.
	 *
	 * @param currencyType the new currency type
	 */
	public void setCurrencyType(Integer currencyType) {
		this.currencyType = currencyType;
	}
	
	/**
	 * Gets the movement count.
	 *
	 * @return the movement count
	 */
	public Integer getMovementCount() {
		return movementCount;
	}
	
	/**
	 * Sets the movement count.
	 *
	 * @param movementCount the new movement count
	 */
	public void setMovementCount(Integer movementCount) {
		this.movementCount = movementCount;
	}
	
	/**
	 * Gets the other entity description.
	 *
	 * @return the other entity description
	 */
	public String getOtherEntityDescription() {
		return otherEntityDescription;
	}
	
	/**
	 * Sets the other entity description.
	 *
	 * @param otherEntityDescription the new other entity description
	 */
	public void setOtherEntityDescription(String otherEntityDescription) {
		this.otherEntityDescription = otherEntityDescription;
	}
	
	/**
	 * Gets the other entity document.
	 *
	 * @return the other entity document
	 */
	public String getOtherEntityDocument() {
		return otherEntityDocument;
	}
	
	/**
	 * Sets the other entity document.
	 *
	 * @param otherEntityDocument the new other entity document
	 */
	public void setOtherEntityDocument(String otherEntityDocument) {
		this.otherEntityDocument = otherEntityDocument;
	}
	
	/**
	 * Gets the record state.
	 *
	 * @return the record state
	 */
	public Integer getRecordState() {
		return recordState;
	}
	
	/**
	 * Sets the record state.
	 *
	 * @param recordState the new record state
	 */
	public void setRecordState(Integer recordState) {
		this.recordState = recordState;
	}
	
	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}
	
	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}
	
	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}
	
	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}
	
	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}
	
	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}
	
	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}
	
	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}
	
	/**
	 * Gets the entity collection.
	 *
	 * @return the entity collection
	 */
	public Integer getEntityCollection() {
		return entityCollection;
	}
	
	/**
	 * Sets the entity collection.
	 *
	 * @param entityCollection the new entity collection
	 */
	public void setEntityCollection(Integer entityCollection) {
		this.entityCollection = entityCollection;
	}
	
	/**
	 * Gets the processed service to.
	 *
	 * @return the processed service to
	 */
	public ProcessedServiceTo getProcessedServiceTo() {
		return processedServiceTo;
	}
	
	/**
	 * Sets the processed service to.
	 *
	 * @param processedServiceTo the new processed service to
	 */
	public void setProcessedServiceTo(ProcessedServiceTo processedServiceTo) {
		this.processedServiceTo = processedServiceTo;
	}
	
	/**
	 * Gets the description entity collection.
	 *
	 * @return the description entity collection
	 */
	public String getDescriptionEntityCollection() {
		return descriptionEntityCollection;
	}
	
	/**
	 * Sets the description entity collection.
	 *
	 * @param descriptionEntityCollection the new description entity collection
	 */
	public void setDescriptionEntityCollection(String descriptionEntityCollection) {
		this.descriptionEntityCollection = descriptionEntityCollection;
	}
	
	/**
	 * Gets the description ind billed.
	 *
	 * @return the description ind billed
	 */
	public String getDescriptionIndBilled() {
		return descriptionIndBilled;
	}
	
	/**
	 * Sets the description ind billed.
	 *
	 * @param descriptionIndBilled the new description ind billed
	 */
	public void setDescriptionIndBilled(String descriptionIndBilled) {
		this.descriptionIndBilled = descriptionIndBilled;
	}
	
	/**
	 * Gets the calculation date.
	 *
	 * @return the calculation date
	 */
	public Date getCalculationDate() {
		return calculationDate;
	}
	
	/**
	 * Sets the calculation date.
	 *
	 * @param calculationDate the new calculation date
	 */
	public void setCalculationDate(Date calculationDate) {
		this.calculationDate = calculationDate;
	}
	
	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return operationDate;
	}
	
	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}
	
	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}
	
	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	
	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}
	
	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}
	
	/**
	 * Gets the description status record.
	 *
	 * @return the description status record
	 */
	public String getDescriptionStatusRecord() {
		return descriptionStatusRecord;
	}
	
	/**
	 * Sets the description status record.
	 *
	 * @param descriptionStatusRecord the new description status record
	 */
	public void setDescriptionStatusRecord(String descriptionStatusRecord) {
		this.descriptionStatusRecord = descriptionStatusRecord;
	}
	
	/**
	 * Gets the id processed service fk.
	 *
	 * @return the id processed service fk
	 */
	public Long getIdProcessedServiceFk() {
		return idProcessedServiceFk;
	}
	
	/**
	 * Sets the id processed service fk.
	 *
	 * @param idProcessedServiceFk the new id processed service fk
	 */
	public void setIdProcessedServiceFk(Long idProcessedServiceFk) {
		this.idProcessedServiceFk = idProcessedServiceFk;
	}
	
	/**
	 * Gets the description currency type.
	 *
	 * @return the description currency type
	 */
	public String getDescriptionCurrencyType() {
		return descriptionCurrencyType;
	}
	
	/**
	 * Sets the description currency type.
	 *
	 * @param descriptionCurrencyType the new description currency type
	 */
	public void setDescriptionCurrencyType(String descriptionCurrencyType) {
		this.descriptionCurrencyType = descriptionCurrencyType;
	}
	
	/**
	 * Gets the collection amount accumulated.
	 *
	 * @return the collection amount accumulated
	 */
	public BigDecimal getCollectionAmountAccumulated() {
		return collectionAmountAccumulated;
	}
	
	/**
	 * Sets the collection amount accumulated.
	 *
	 * @param collectionAmountAccumulated the new collection amount accumulated
	 */
	public void setCollectionAmountAccumulated(
			BigDecimal collectionAmountAccumulated) {
		this.collectionAmountAccumulated = collectionAmountAccumulated;
	}
	
	/**
	 * Gets the gross amount accumulated.
	 *
	 * @return the gross amount accumulated
	 */
	public BigDecimal getGrossAmountAccumulated() {
		return grossAmountAccumulated;
	}
	
	/**
	 * Sets the gross amount accumulated.
	 *
	 * @param grossAmountAccumulated the new gross amount accumulated
	 */
	public void setGrossAmountAccumulated(BigDecimal grossAmountAccumulated) {
		this.grossAmountAccumulated = grossAmountAccumulated;
	}
	
	/**
	 * Gets the tax applied accumulated.
	 *
	 * @return the tax applied accumulated
	 */
	public BigDecimal getTaxAppliedAccumulated() {
		return taxAppliedAccumulated;
	}
	
	/**
	 * Sets the tax applied accumulated.
	 *
	 * @param taxAppliedAccumulated the new tax applied accumulated
	 */
	public void setTaxAppliedAccumulated(BigDecimal taxAppliedAccumulated) {
		this.taxAppliedAccumulated = taxAppliedAccumulated;
	}
	
	/**
	 * Gets the issuer code.
	 *
	 * @return the issuer code
	 */
	public String getIssuerCode() {
		return issuerCode;
	}
	
	/**
	 * Sets the issuer code.
	 *
	 * @param issuerCode the new issuer code
	 */
	public void setIssuerCode(String issuerCode) {
		this.issuerCode = issuerCode;
	}
	
	/**
	 * Gets the participant code.
	 *
	 * @return the participant code
	 */
	public Long getParticipantCode() {
		return participantCode;
	}
	
	/**
	 * Sets the participant code.
	 *
	 * @param participantCode the new participant code
	 */
	public void setParticipantCode(Long participantCode) {
		this.participantCode = participantCode;
	}
	
	/**
	 * Gets the holder code.
	 *
	 * @return the holder code
	 */
	public Long getHolderCode() {
		return holderCode;
	}
	
	/**
	 * Sets the holder code.
	 *
	 * @param holderCode the new holder code
	 */
	public void setHolderCode(Long holderCode) {
		this.holderCode = holderCode;
	}
	
	
	
}
