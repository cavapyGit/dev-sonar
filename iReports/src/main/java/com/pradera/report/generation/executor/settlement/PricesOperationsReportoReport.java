package com.pradera.report.generation.executor.settlement;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;

@ReportProcess(name = "PricesOperationsReportoReport")
public class PricesOperationsReportoReport extends GenericReport {

	private static final long serialVersionUID = 1L;

	@Inject
	PraderaLogger log;

	@EJB
	private ParameterServiceBean parameterService;

	@EJB
	ParticipantServiceBean participantServiceBean;

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {

		Map<String, Object> parametersRequired = new HashMap<>();
		Map<Integer, String> currency = new HashMap<Integer, String>();
		Map<Integer, String> participantMnemonic = new HashMap<Integer, String>();
		ParameterTableTO filter = new ParameterTableTO();

		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		String participantDescription = null;
		String currencyDescription = null;
		String securityClassDescription = null;
		String cuidHolder = null;
		Holder holderDesc = null;
		String holderAccount = null;
		HolderAccount holderAccountDesc =null;
		
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.PARTICIPANT_PARAM))
				if (r.getFilterValue() != null) {
					participantDescription = r.getFilterDescription();
				} else {
					participantDescription = "TODOS";
				}
			if (r.getFilterName().equals(ReportConstant.CURRENCY))
				if (r.getFilterValue() != null) {
					currencyDescription = r.getFilterDescription();
				} else {
					currencyDescription = "TODOS";
				}
			if (r.getFilterName().equals(ReportConstant.SECURITY_CODE))
				if (r.getFilterValue() != null) {
					securityClassDescription = r.getFilterValue();
				} else {
					securityClassDescription = "TODOS";
				}
			if (r.getFilterName().equals(ReportConstant.CUI_HOLDER_PARAM))
				if (r.getFilterValue() != null) {
					cuidHolder = r.getFilterValue();
					holderDesc = parameterService.find(Holder.class,Long.parseLong(cuidHolder));
				}
			if (r.getFilterName().equals(ReportConstant.ACCOUNT_HOLDER))
				if (r.getFilterValue() != null) {
					holderAccount =  r.getFilterValue();
					holderAccountDesc = parameterService.find(HolderAccount.class,Long.parseLong(holderAccount));
				}
		}

		try {
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for (ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				currency.put(param.getParameterTablePk(), param.getDescription());
			}
			for (Participant participant : participantServiceBean.getLisParticipantServiceBean(new Participant())) {
				participantMnemonic.put(participant.getIdParticipantPk().intValue(), participant.getMnemonic());
			}
		} catch (Exception ex) {
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		parametersRequired.put("participant_description", participantDescription);
		parametersRequired.put("currency_description", currencyDescription);
		parametersRequired.put("security_class_description", securityClassDescription);
		parametersRequired.put("map_currency", currency);
		parametersRequired.put("p_participant_mnemonic", participantMnemonic);
		parametersRequired.put("p_cui",cuidHolder);
		parametersRequired.put("p_cui_desc",holderDesc==null?null:holderDesc.getIdHolderPk()+" - "+holderDesc.getFullName());
		parametersRequired.put("p_holder_account",holderAccount);
		parametersRequired.put("p_holder_account_desc",holderAccountDesc==null?null:holderAccountDesc.getAccountNumber()+" - "+holderAccountDesc.getDescription());
		return parametersRequired;
	}

}
