package com.pradera.report.generation.executor.settlement;

import java.io.ByteArrayOutputStream;
import java.text.MessageFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounts.Participant;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.settlement.service.SettlementReportServiceBean;

@ReportProcess(name="RecordsOnSanctionsUnfulfillmentReport")
public class RecordsOnSanctionsUnfulfillmentReport extends GenericReport{
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private ParticipantServiceBean participantServiceBean;
	
	@EJB
	private SettlementReportServiceBean settlementReportServiceBean;
	
	@Inject PraderaLogger log;
	
	/** The message configuration. */
	@Inject
	@InjectableResource(location = "GenericConstants.properties")
	private Properties messageConfiguration;
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String,Object> parametersRequired = new HashMap<>();
		
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();

		String messagePenaltyOperation = messageConfiguration.getProperty("message.sanction.penalty.operation");
		String messageResolutionsLast = messageConfiguration.getProperty("message.sanction.resolution.last");
		String messagePenalty = messageConfiguration.getProperty("message.sanction.penalty");
		
		String idParticipantPenalty=null;
		String idParticipant=null;
		String strParticipantDesc=null;
		String strSettDate=null;
		String penaltyAmount = null;
		String penaltyMotive = null;
		String penaltyMotiveOther = null;
		String penaltyLevel = null;
		String ballotPenalty = null;
		String amountLetters=null;
		String levelLetters=null;
		String strPenaltyMotive=null;
		String strPenalty=null;
		String strPenaltyOperation=null;
		String strResolutionNumber=null;
		String strDateTime=null;
		
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals("idParticipantPenalty") && r.getFilterValue()!=null){
				idParticipantPenalty = r.getFilterValue();
			}	
		}
		List<Object[]>  lstResult=settlementReportServiceBean.getBallotPartPenalty(idParticipantPenalty);
		for(Object[] obj:lstResult){
			idParticipantPenalty=obj[0].toString();
			strSettDate=CommonsUtilities.convertDateToString((Date)obj[1], "DD/MMM/YYYY");
			penaltyAmount=obj[2].toString();
			penaltyMotive=obj[3].toString();
			if(Validations.validateIsNotNullAndNotEmpty(obj[4])){
				penaltyMotiveOther=obj[4].toString();
			}
			penaltyLevel=obj[5].toString();
			idParticipant=obj[6].toString();
			ballotPenalty=obj[7].toString();
			strResolutionNumber=obj[8].toString();
			strDateTime=strResolutionNumber=obj[9].toString();
		}
		
		switch (penaltyAmount){
		case "50":
			amountLetters ="Cincuenta";
			break;
		case "100":
			amountLetters ="Cien";
			break;
		case "150":
			amountLetters ="Ciento Cincuenta";
			break;
		case "200":
			amountLetters ="Doscientos";
			break;
		case "250":
			amountLetters ="Doscientos Ciuncuenta";
			break;
		default:
			amountLetters ="";
			break;
		}
		
		switch (penaltyLevel){
		case "1":
			levelLetters ="Primer Nivel:";
			break;
		case "2":
			levelLetters ="Segundo Nivel:";
			break;
		case "3":
			levelLetters ="Tercer Nivel:";
			break;
		case "4":
			levelLetters ="Cuarto Nivel:";
			break;
		case "5":
			levelLetters ="Quinto Nivel:";
			break;
		default:
			levelLetters ="";
			break;
		}
		
		switch (penaltyMotive){
		case "1824"://melor x fondos
			strPenaltyMotive ="Mecanismo de Liquidaci\u00F3n de Operaciones Retrasadas - MELOR, debido a que fueron retiradas de la Segunda Etapa de Liquidaci\u00F3n por falta de fondos.";
			break;
		case "1825"://melor x valores
			strPenaltyMotive ="Mecanismo de Liquidaci\u00F3n de Operaciones Retrasadas - MELOR, debido a que fueron retiradas de la Segunda Etapa de Liquidaci\u00F3n por falta de Valores.";
			break;
		case "1826"://Melid x fondos 
			strPenaltyMotive ="Fueron declaradas incumplidas por no cubrir sus obligaciones de entrega de fondos o valores en los plazos establecidos por la EDV";
			break;
		case "1827"://Melid x valores
			strPenaltyMotive ="Tercer Nivel";
			break;
		case "1828"://INCUMPLIMIENTO POR FONDOS
			strPenaltyMotive ="Cuarto Nivel";
			break;
		case "1829"://INCUMPLIMIENTO POR VALORES
			strPenaltyMotive ="Quinto Nivel";
			break;
		case "1834"://OTROS
			strPenaltyMotive ="Segundo Nivel";
			break;
		}
		
		Participant partSanctions= participantServiceBean.find(Participant.class, new Long(idParticipant));
		strParticipantDesc=partSanctions.getDescription();
		//Resuelve:
		strPenalty="Sancionar con una multa de $us. "+ penaltyAmount + amountLetters+"00/100 D\u00f3lares Americanos) a" +strParticipantDesc +
				", de conformidad a lo dispuesto en el art\u00EDculo X.6 , par\u00E1grafo II,"+levelLetters+
				" y art\u00EDcuklo X.7 del Reglamento, por liquidar operaciones a trav\u00E9s de los mecanismos "
				+ "preventivos establecidos por la EDV para reducir el riesgo de incumplimiento.";
		
		strPenaltyOperation=MessageFormat.format(messagePenaltyOperation,strSettDate,strParticipantDesc,ballotPenalty);
		
		StringBuilder iterationMotive=new StringBuilder();
		lstResult=settlementReportServiceBean.getHistorySanctionsByParticipant(new Long(idParticipant),idParticipantPenalty);
		for(Object[] obj:lstResult){
			
			iterationMotive.append("Que, ").append(strParticipantDesc).append(", mediante la Resoluci\u00f3n No").
			append(obj[0]).append(" emitida por la Gerencia de Liquidaciones en fecha ").append(obj[1]).
			append(", fue sancionada por liquidar operaciones a trav\u00E9s de").append(obj[2]).append("\n");
		}
		
		strPenalty=MessageFormat.format(messagePenalty ,penaltyAmount,amountLetters,strParticipantDesc,levelLetters);
		strPenaltyOperation="Que, en la liquidaci\u00f3n de fecha "+strSettDate+" las operaciones realizadas por " +strParticipantDesc + " , seg\u00FAn papeletas de la BBV Nos. "+
				ballotPenalty+" tuvieron que liquidarse a trav\u00E9s del "+strPenaltyMotive;
		strPenaltyOperation=MessageFormat.format(messagePenaltyOperation,strParticipantDesc,strSettDate,strParticipantDesc,ballotPenalty);
		
		parametersRequired.put(ReportConstant.LEVEL_SANCTIONS, partSanctions);
		parametersRequired.put(ReportConstant.STR_LEVEL_SANCTIONS,"I. "+ levelLetters);
		parametersRequired.put(ReportConstant.MOTIVE_SANCTIONS,strPenaltyMotive);
		parametersRequired.put(ReportConstant.AMOUNT_SANCTIONS, "$us. "+ penaltyAmount);
		parametersRequired.put(ReportConstant.PART_SANCTIONS, strParticipantDesc);
		parametersRequired.put(ReportConstant.STR_PENALTY_OPE, strPenaltyOperation);
		parametersRequired.put(ReportConstant.STR_PENALTY, strPenalty);
		parametersRequired.put(ReportConstant.STR_RESOLUTION_NUMBER, strResolutionNumber);
		parametersRequired.put(ReportConstant.STR_DATE_TIME, strDateTime);
		parametersRequired.put(ReportConstant.STR_ITERATION_MOTIVE, iterationMotive);
		parametersRequired.put(ReportConstant.STR_PENALTY_LEVEL, penaltyLevel);
		
		
		return parametersRequired;
	}
}
