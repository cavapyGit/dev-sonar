package com.pradera.report.generation.executor.accounting;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.model.accounting.AccountingAccount;
import com.pradera.model.accounting.type.AccountingStartType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.accounting.service.AccountingGeneratorReportServiceBean;
import com.pradera.report.generation.executor.accounting.to.AccountingMatrixDetailTo;
import com.pradera.report.generation.executor.accounting.to.AccountingProcessTo;
import com.pradera.report.generation.poi.ReportPoiGeneratorServiceBean;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * Report 206. Reporte del Acumulado de todos los dias
 * The Class DocumentSecurityReport.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@ReportProcess(name="DocumentSecurityReport")
public class DocumentSecurityReport extends GenericReport{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	
	/** The accounting poi generator service bean. */
	@Inject
	private ReportPoiGeneratorServiceBean accountingPoiGeneratorServiceBean;
	
	/** The accounting generator report service bean. */
	@Inject
	private AccountingGeneratorReportServiceBean accountingGeneratorReportServiceBean;

	/** The parameter service. */
	@EJB
	private ParameterServiceBean parameterService;
	
	/**
	 * Instantiates a new document security report.
	 */
	public DocumentSecurityReport() {
	}
	
	/** The list accounting matrix detail. */
	List<AccountingMatrixDetailTo>	listAccountingMatrixDetail;
	
	/** The list accounting account. */
	List<AccountingAccount>	listAccountingAccount;
	
	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#generateData(com.pradera.model.report.ReportLogger)
	 */
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		
		AccountingProcessTo accountingProcessTo;
		accountingProcessTo=new AccountingProcessTo();
		StringBuilder sbParametersReport = new StringBuilder();
		boolean existData = false;
		for(ReportLoggerDetail loggerDetail : reportLogger.getReportLoggerDetails()) {

	
			if(loggerDetail.getFilterName().equals(ReportConstant.PROCESS_ACCOUNTING_PK)){
				
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					accountingProcessTo.setIdAccountingProcessPk(Long.parseLong(loggerDetail.getFilterValue()));
					sbParametersReport.append(loggerDetail.getFilterValue()).append(GeneralConstants.STR_COMMA);
				}
			}
			else if(loggerDetail.getFilterName().equals(ReportConstant.BRANCH)){
				
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					accountingProcessTo.setProcessType(Integer.parseInt(loggerDetail.getFilterValue()));
					sbParametersReport.append(loggerDetail.getFilterValue()).append(GeneralConstants.STR_COMMA);
				}
			}

			else if(loggerDetail.getFilterName().equals(ReportConstant.DATE_INITIAL_PARAM)){
				
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					accountingProcessTo.setExecutionDate(CommonsUtilities.convertStringtoDate(loggerDetail.getFilterValue()));
					sbParametersReport.append(loggerDetail.getFilterValue()).append(GeneralConstants.STR_COMMA);
				}
			}else if(loggerDetail.getFilterName().equals(GeneralConstants.REPORT_BY_PROCESS)){
				
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					accountingProcessTo.setReportByProcess(Integer.parseInt(loggerDetail.getFilterValue()));
					sbParametersReport.append(loggerDetail.getFilterValue()).append(GeneralConstants.STR_COMMA);
				}
			}
		}
		

		accountingProcessTo.setStartType(AccountingStartType.CONTINUE.getCode());
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		byte[] arrayByteExcel = new byte[16384];

		try {
			
			listAccountingAccount=this.accountingGeneratorReportServiceBean.findAccountBalanceHistory(accountingProcessTo);
			try {
				BigDecimal descuadre = null;//
				Integer primaryKey = 2440;
				ParameterTable amountParameter = parameterService.find(ParameterTable.class, primaryKey);
				descuadre = new BigDecimal(amountParameter.getText1());
				
				BigDecimal dif = BigDecimal.ZERO;
				
				for (AccountingAccount acc : listAccountingAccount) {
					if(acc.getAccountCode().equals("705.00")){
						dif = acc.getAmount().subtract(descuadre);
						acc.setAmount(dif);
//						acc.setAmountBOB(dif);
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		if(!listAccountingAccount.isEmpty()){
			
			existData = true;
			
		}
		
		

		if(existData) {
			
			/**GENERATE FILE EXCEL ***/
			arrayByteExcel=accountingPoiGeneratorServiceBean.writeFileExcelAccountingBalanceHistory(listAccountingAccount,reportLogger,accountingProcessTo);

		}
		else{
			arrayByteExcel=accountingPoiGeneratorServiceBean.writeFileExcelAccountingBalanceHistory(listAccountingAccount,reportLogger,accountingProcessTo);
		}
		
		/**SETTING ARRAY BYTE TO GENERIC REPORT FOR PERSIST**/
		
//		arrayBytePdf=accountingPdfGeneratorServiceBean.writeFileExcelAccountingBalance(arrayByteExcel);
//		setArrayByte(arrayByteExcel);
		List<byte[]> bytes = new ArrayList<>();
		bytes.add(arrayByteExcel);
		setListByte(bytes);
		
		/**FILE**/
		
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		return baos;
	}
}
