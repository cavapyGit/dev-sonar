package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.pradera.commons.configuration.StageDependent;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.custody.to.AccountEntryRegisterTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.custody.type.CatType;
import com.pradera.model.custody.type.RetirementOperationMotiveType;
import com.pradera.model.custody.type.RetirementOperationStateType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.executor.custody.to.AccreditationOperationTO;
import com.pradera.report.generation.executor.custody.to.RetirementOperationTO;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccreditationCertificateOwnershipReport.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/09/2015
 */
@ReportProcess(name="AccreditationCertificateOwnershipReport")
public class AccreditationCertificateOwnershipReport extends GenericReport{
	
	/** The Constant serialVersionUID. */	
	private static final long serialVersionUID = 1L;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The custody report service bean. */
	@EJB
	CustodyReportServiceBean custodyReportServiceBean;
	
	/** The folder pentaho. */
	@Inject @StageDependent
	protected String folderJasper;

/** The third paragraph. */
//	private StringBuilder secondParagraph= new StringBuilder();	
	private StringBuilder thirdParagraph= new StringBuilder();
	
	/** The first paragraph. */
	private StringBuilder firstParagraph= new StringBuilder();
	
	/** The motive. */
	private String motive = "";
	
	/** The days. */
	private Integer days = 0;
	
	/** The expiration dt request. */
	private String expirationDtRequest="";
	
	/** The complete date. */
	private Date completeDate= CommonsUtilities.currentDateWithTime();

	/** The holder description. */
	private static int HOLDER_DESCRIPTION		= 0;
	
	/** The cui. */
	private static int CUI       				= 1;
	
	/** The participant desc. */
	private static int PARTICIPANT_DESC			= 2;
	
	/** The sec desc. */
	private static int SEC_DESC					= 3;
	
	/** The motive desc. */
	private static int MOTIVE_DESC				= 4;
	
	/** The id security code pk. */
	private static int ID_SECURITY_CODE_PK      = 5;
	
	/** The expiration dt request. */
	private static int EXPIRATION_DT_REQUEST	= 6;
	
	/** The validity days. */
	private static int VALIDITY_DAYS			= 7;
	
	/** The security class desc. */
	private static int SECURITY_CLASS_DESC		= 8;
	
	/** The currency. */
	private static int CURRENCY					= 9;
	
	/** The nominal value. */
	private static int NOMINAL_VALUE			= 10;
	
	/** The issuance date. */
	private static int ISSUANCE_DATE			= 11;
	
	/** The life term. */
	private static int LIFE_TERM				= 12;
	
	/** The interest rate. */
	private static int INTEREST_RATE			= 13;
	
	/** The date type. */
	private static int DATE_TYPE				= 14;
	
	/** The coupon number int. */
	private static int COUPON_NUMBER_INT		= 15;
	
	/** The expiration date int. */
	private static int EXPIRATION_DATE_INT		= 16;
	
	/** The life term coupon int. */
	private static int LIFE_TERM_COUPON_INT		= 17;
	
	/** The calendar days. */
	private static int CALENDAR_DAYS			= 18;
	
	/** The amount total payed. */
	private static int AMOUNT_TOTAL_PAYED		= 19;
	
	/** The state interest. */
	private static int STATE_INTEREST		    = 20;
	
	/** The coupon number amort. */
	private static int COUPON_NUMBER_AMORT		= 21;
	
	/** The expiration date amort. */
	private static int EXPIRATION_DATE_AMORT	= 22;
	
	/** The life term coupon amort. */
	private static int LIFE_TERM_COUPON_AMORT	= 23;
	
	/** The capital amort pndi. */
	private static int CAPITAL_AMORT_PNDI       = 24;
	
	/** The capital amort. */
	private static int CAPITAL_AMORT			= 25;
	
	/** The state amort. */
	private static int STATE_AMORT				= 26;
	
	/** The total balance. */
	private static int TOTAL_BALANCE			= 27;
	
	/** The reporting balance. */
	private static int REPORTING_BALANCE    	= 28;
	
	/** The ban balance. */
	private static int BAN_BALANCE    			= 29;
	
	/** The pawn balance. */
	private static int PAWN_BALANCE    			= 30;
	
	/** The available balance. */
	private static int AVAILABLE_BALANCE		= 31;
	
	/** The instrument type. */
	private static int INSTRUMENT_TYPE			= 32;
	
	/** The begining date. */
	private static int BEGINING_DATE			= 33;
	
	/** The desmaterialized balance. */
	private static int DESMATERIALIZED_BALANCE	= 34;
	
	/** The expiration date standar. */
	private static int EXPIRATION_DATE_STANDAR  = 35;
	
	/** The registry date. */
	private static int REGISTRY_DATE 			= 36;
	
	/** The account number. */
	private static int ACCOUNT_NUMBER 			= 37;
	
	/** The id security code only. */
	private static int ID_SECURITY_CODE_ONLY    = 38;
	
	/** The id issuer fk. */
	private static int ID_ISSUER_FK    			= 39;
	
	/** The id isin code. */
	private static int ID_ISIN_CODE    			= 40;
	
	/** The cfi code. */
	private static int CFI_CODE    				= 41;
	
	/** The interest amortization. */
	private static int INTEREST_AMORTIZATION	= 42;
	
	/** The certificate number. */
	private static int CERTIFICATE_NUMBER		= 43;
	
	/** The request number. */
	private static int REQUEST_NUMBER			= 44;
	
	/** The number coupons. */
	private static int NUMBER_COUPONS			= 45;
	
	/** The number coupons int. */
	private static int NUMBER_COUPONS_INT		= 46;
	
	/** The security code cat. */
	private static int SECURITY_CODE_CAT		= 47;
	
	/** The INITIAL value. */
	private static int INITIAL_VALUE			= 48;
	
	/** The ISSUER */
	private static int ISSUER					= 49;
	
	/** The SECURITY_CLASS_DESC_COMPLETE */
	private static int SECURITY_CLASS_DESC_COMPLETE					= 50;
	
	/** The SECURITY_CLASS_DESC_COMPLETE */
	private static int AVAL						= 51;
	
	/** The SECURITY_CLASS_DESC_COMPLETE */
	private static int CONS_EMI					= 52;
	
	/** The SECURITY_CLASS_DESC_COMPLETE */
	private static int LUG_EMI					= 53;
	
	/** The SECURITY_CLASS_DESC_COMPLETE */
	private static int RMV						= 54;
	
	/** The SECURITY_CLASS_DESC_COMPLETE */
	private static int DOC_EMI					= 55;
		
	/** The SECURITY_CLASS_DESC_COMPLETE */
	private static int LUG_PAGO					= 57;
	
	/** The SECURITY_CLASS_DESC_COMPLETE */
	private static int PROTESTO					= 58;
	
	/** The SECURITY_CLASS_DESC_COMPLETE */
	private static int MONTO_EMISION			= 56;
	
	/** The CAT_TYPE */	
	private static int CAT_TYPE					= 59;
	
	/** The CAT_TYPE */	
	private static int OTHER_BLOCK_BALANCE		= 60;
	
	/** The security EXPIRATION_DATE */	
	private static int SECURITY_EXPIRATION_DATE		= 61;
	
	private static int TESTIMONY_NUMBER		= 62;
	
	private static int ISSUANCE_DESCRIPTION		= 63;
	
	private static int AMOUNT_SERIE		= 64;
	
	private static int SECURITY_CLASS		= 65;
	
	/** The CAT_TYPE */	
	private static int ROLE		= 66;
	
	private static int SECURITY_EXPIRATION_FONDO_DATE		= 67;
	
	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#generateData(com.pradera.model.report.ReportLogger)
	 */
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		AccreditationOperationTO accreditationOperationTO =  new AccreditationOperationTO();
		//Read parameters from DB, all parameters are string type
		for(ReportLoggerDetail loggerDetail : reportLogger.getReportLoggerDetails()) {
			
			accreditationOperationTO.setIdReportPk(loggerDetail.getReportLogger().getReport().getIdReportPk());
			
			if(loggerDetail.getFilterName().equals(ReportConstant.OPERATION_NUMBER_PARAM) &&
				Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	) {
				accreditationOperationTO.setIdAccreditationOperationPk(Long.valueOf(loggerDetail.getFilterValue()));
				break;
			}			
		}

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			//GETTING ALL DATA FROM QUERY
			List<Object[]> listObjects = custodyReportServiceBean.getAccreditationCertification(accreditationOperationTO);

			XMLOutputFactory xof = XMLOutputFactory.newInstance();
			XMLStreamWriter xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(baos));
			//open document file
			xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");
			//root tag of report
			/**
			 * report
			 **/
			xmlsw.writeStartElement(ReportConstant.REPORT);
			/**
			 * Create to header report
			 * start_hour
			 * report_title
			 * mnemonic_report
			 * clasification_report
			 * generation_date
			 * user_name
			 * mnemonic_entity
			 * **/
			createHeaderReport(xmlsw, reportLogger);
			xmlsw.writeStartElement("accreditationOperationDetails");
			
//			createTagString(xmlsw, "ID_ACCREDITATION_PK", 	 accreditationOperationTO.getIdAccreditationOperationPk());
			if(Validations.validateListIsNotNullAndNotEmpty(listObjects)){
				/** Creating the body **/
				createBodyReport(xmlsw, listObjects);
			}else{
				createEmptyReport(xmlsw);
			}
			//end accreditationOperationDetails
			xmlsw.writeEndElement();
			//END REPORT TAG
			xmlsw.writeEndElement();
			//close document
			xmlsw.writeEndDocument();
			xmlsw.flush();
	        xmlsw.close();
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		return baos;
	}
	
	/**
	 * Creates the empty report.
	 *
	 * @param xmlsw the xmlsw
	 * @throws XMLStreamException the XML stream exception
	 */
	private void createEmptyReport(XMLStreamWriter xmlsw) throws XMLStreamException {
		String empty="---";
		String zero="0";
		createTagString(xmlsw, "CERTIFICATE_NUMBER",  empty);
		createTagString(xmlsw, "ID_ACCREDITATION_PK", empty);
		createTagString(xmlsw, "HOLDER_DESCRIPTION",  empty);
		createTagString(xmlsw, "CUI",  empty);
		createTagString(xmlsw, "PARTICIPANT_DESC",  empty);
		createTagString(xmlsw, "SEC_DESC",  empty);
		createTagString(xmlsw, "SEC_SERIAL",  empty);
		createTagString(xmlsw, "REGISTRY_DATE",  empty);
		createTagString(xmlsw, "ACCOUNT_NUMBER",  empty);
		createTagString(xmlsw, "PARTICIPANT_DESC",  empty);
		createTagString(xmlsw, "SECURITY_CLASS_DESC",  empty);
		createTagString(xmlsw, "ID_SECURITY_CODE_ONLY",  empty);
		createTagString(xmlsw, "NOMINAL_VALUE",  zero);
		createTagString(xmlsw, "INITIAL_VALUE",  zero);
		createTagString(xmlsw, "ID_ISSUER_FK",  empty);
		createTagString(xmlsw, "CURRENCY",  empty);
		createTagString(xmlsw, "ID_ISIN_CODE",  empty);
		createTagString(xmlsw, "CFI_CODE",  empty);
		createTagString(xmlsw, "ISSUANCE_DATE",  empty);
		createTagString(xmlsw, "LIFE_TERM",  zero);
		createTagString(xmlsw, "INTEREST_RATE",  zero);
		createTagString(xmlsw, "DATE_TYPE",  empty);
		createTagString(xmlsw, "TOTAL_BALANCE",  zero);
		createTagString(xmlsw, "REPORTING_BALANCE",  zero);
		createTagString(xmlsw, "BAN_BALANCE",  zero);
		createTagString(xmlsw, "PAWN_BALANCE",  zero);
		createTagString(xmlsw, "AVAILABLE_BALANCE",  zero);
		createTagString(xmlsw, "OTHER_BLOCK_BALANCE",  zero);
		createTagString(xmlsw, "REAL_EXPIRATION_DATE",  empty);//Issue 746
		createTagString(xmlsw, "firstParagraph",  empty);
		createTagString(xmlsw, "secondParagraph",  empty);
		createTagString(xmlsw, "thirdParagraph",  empty);
		xmlsw.writeStartElement("coupons");
			createTagString(xmlsw, "COUPON_NUMBER",  empty);
			createTagString(xmlsw, "ID_SECURITY_CODE_PK",  zero);
			createTagString(xmlsw, "EXPIRATION_DATE",  empty);
			createTagString(xmlsw, "LIFE_TERM_COUPON",  zero);
			createTagString(xmlsw, "STATE",  empty);
			createTagString(xmlsw, "AMOUNT_TOTAL_PAYED",  zero);
			createTagString(xmlsw, "CAPITAL_AMORT_PNDI",  zero);
			createTagString(xmlsw, "CAPITAL_AMORT",  zero);
			createTagString(xmlsw, "INTEREST_AMORTIZATION",  zero);
			createTagString(xmlsw, "TOTAL_EXPIRATION_SECURITY",  zero);
		xmlsw.writeEndElement();
	}

	/**
	 * Creates the body report.
	 *
	 * @param xmlsw the xmlsw
	 * @param accreditationOperations the accreditation operations
	 * @throws XMLStreamException the XML stream exception
	 * @throws ServiceException the service exception
	 */
	public void createBodyReport(XMLStreamWriter xmlsw,List<Object[]> accreditationOperations)  throws XMLStreamException, ServiceException{
		String empty_large="------------------------";
		String empty="---";
		String zero="0";
		String lastSecurity=null;
		BigDecimal interestRate=new BigDecimal(1);
		BigDecimal nominalValue=new BigDecimal(1);
		BigDecimal amountEmision=new BigDecimal(1);
		BigDecimal initialValue=new BigDecimal(1);
		BigDecimal amountSerie=new BigDecimal(1);
		//BY ROW
		for(Object[] accreditationOp : accreditationOperations){
			StringBuilder sbMess1= new StringBuilder();
			
			if(accreditationOp[ID_SECURITY_CODE_PK].toString().equals(lastSecurity)){
				couponsDetail(accreditationOp, xmlsw, interestRate, nominalValue);
			}else{
				if(accreditationOp[CERTIFICATE_NUMBER]!=null){
					createTagString(xmlsw, "ID_ACCREDITATION_PK", 	 accreditationOp[REQUEST_NUMBER]);
				}else{
					createTagString(xmlsw, "ID_ACCREDITATION_PK", 	 empty);
				}
				
				if(accreditationOp[CERTIFICATE_NUMBER]!=null){
					createTagString(xmlsw, "CERTIFICATE_NUMBER",  accreditationOp[CERTIFICATE_NUMBER]);
				}else{
					createTagString(xmlsw, "CERTIFICATE_NUMBER",  empty);
				}
				if(accreditationOp[SECURITY_CODE_CAT]!=null){
					createTagString(xmlsw, "SECURITY_CODE_CAT",  accreditationOp[SECURITY_CODE_CAT].toString());
				}
				createTagString(xmlsw, "PATH_SUBREPORT",  folderJasper+GeneralConstants.SLASH +"custody"+GeneralConstants.SLASH);
				
//				List<DigitalSignature> lstDigitalSignature = custodyReportServiceBean.getListDigitalSignatureBlob(DigitalSignatureType.FIRST_SIGNATURE.getCode());
				
//				if(Validations.validateListIsNotNullAndNotEmpty(lstDigitalSignature)){
//					byte[] encodedImage = Base64.encodeBase64(lstDigitalSignature.get(0).getSignature());
//					String encodeImg = new String(encodedImage);					
//					createTagString(xmlsw, "FIRST_DIGITAL", encodeImg);
//				}
				
//				List<DigitalSignature> lstDigitalSignatureSecond = custodyReportServiceBean.getListDigitalSignatureBlob(DigitalSignatureType.SECOND_SIGNATURE.getCode());
				
//				if(Validations.validateListIsNotNullAndNotEmpty(lstDigitalSignatureSecond)){
//					byte[] encodedImage = Base64.encodeBase64(lstDigitalSignatureSecond.get(0).getSignature());
//					String encodeImg = new String(encodedImage);					
//					createTagString(xmlsw, "SECOND_DIGITAL", encodeImg);
//				}
				
				if(accreditationOp[REGISTRY_DATE]!=null){
					Date registryDt= (Date)accreditationOp[REGISTRY_DATE];
					createTagString(xmlsw, "REGISTRY_DATE", CommonsUtilities.convertDateToStringLocaleES(registryDt, CommonsUtilities.DATE_PATTERN_DOCUMENT));
				}else{
					createTagString(xmlsw, "REGISTRY_DATE",  empty);
				}

					sbMess1.append("Que, el titular: ");
					if(accreditationOp[HOLDER_DESCRIPTION]!=null){
						sbMess1.append(accreditationOp[HOLDER_DESCRIPTION].toString()+", ");//VER ULTIMA COMA
						createTagString(xmlsw, "HOLDER_DESCRIPTION",  accreditationOp[HOLDER_DESCRIPTION]);
					}else{
						sbMess1.append(empty_large);
						createTagString(xmlsw, "HOLDER_DESCRIPTION",  empty);
					}
					
					sbMess1.append("con C\u00f3digo \u00danico de Identificaci\u00f3n (CUI) ");
					if(accreditationOp[CUI]!=null){
						sbMess1.append(accreditationOp[CUI].toString());
						createTagString(xmlsw, "CUI",  accreditationOp[CUI]);
					}else{
						sbMess1.append(empty_large);
						createTagString(xmlsw, "CUI",  empty);
					}
					
					sbMess1.append(", Cuenta Titular ");
					sbMess1.append(accreditationOp[ACCOUNT_NUMBER]);
					createTagString(xmlsw, "ACCOUNT_NUMBER",  accreditationOp[ACCOUNT_NUMBER]);
					
					sbMess1.append(", registrado en el Depositante ");
					if(accreditationOp[PARTICIPANT_DESC]!=null){
						sbMess1.append(accreditationOp[PARTICIPANT_DESC]);
						createTagString(xmlsw, "PARTICIPANT_DESC",  accreditationOp[PARTICIPANT_DESC]);
					}else{
						sbMess1.append(empty_large);
						createTagString(xmlsw, "PARTICIPANT_DESC",  empty);
					}
					
					if(accreditationOp[CAT_TYPE].equals(CatType.CAT.getCode())){
						sbMess1.append(" tiene registrado el valor ");//MOSTRAR UNO O VARIOS VALORES-----------------------------------------------------------------
					}else{
						sbMess1.append(" mantiene el Código Valor ");//MOSTRAR UNO O VARIOS VALORES-----------------------------------------------------------------
					}
					
					sbMess1.append(accreditationOp[ID_SECURITY_CODE_PK]);
					createTagString(xmlsw, "SEC_DESC",  accreditationOp[ID_SECURITY_CODE_PK]);
					lastSecurity = accreditationOp[ID_SECURITY_CODE_PK].toString();
					
					sbMess1.append(" en el Sistema de Registro de Anotaciones en Cuenta a cargo de la Caja de Valores del Paraguay S.A., con las siguientes" +
								   " especificaciones y caracter\u00edsticas: ");
					createTagString(xmlsw, "CERTIFY",  sbMess1);
					
				
				if(accreditationOp[SECURITY_CLASS_DESC]!=null){
					createTagString(xmlsw, "SECURITY_CLASS_DESC",  accreditationOp[SECURITY_CLASS_DESC]);
				}else{
					createTagString(xmlsw, "SECURITY_CLASS_DESC",  empty);
				}
				//issue 708
				if(accreditationOp[AVAL]!=null){
					createTagString(xmlsw, "AVAL",  accreditationOp[AVAL]);
				}else{
					createTagString(xmlsw, "AVAL",  empty);
				}
				
				if(accreditationOp[CONS_EMI]!=null){
					createTagString(xmlsw, "CONS_EMI",  accreditationOp[CONS_EMI]);
				}else{
					createTagString(xmlsw, "CONS_EMI",  empty);
				}
				
				if(accreditationOp[LUG_EMI]!=null){
					createTagString(xmlsw, "LUG_EMI",  accreditationOp[LUG_EMI]);
				}else{
					createTagString(xmlsw, "LUG_EMI",  empty);
				}

				if(accreditationOp[RMV]!=null){
					createTagString(xmlsw, "RMV",  accreditationOp[RMV]);
				}else{
					createTagString(xmlsw, "RMV",  empty);
				}
				
				if(accreditationOp[DOC_EMI]!=null){
					createTagString(xmlsw, "DOC_EMI",  accreditationOp[DOC_EMI]);
				}else{
					createTagString(xmlsw, "DOC_EMI",  empty);
				}
				
				if(accreditationOp[LUG_PAGO]!=null){
					createTagString(xmlsw, "LUG_PAGO",  accreditationOp[LUG_PAGO]);
				}else{
					createTagString(xmlsw, "LUG_PAGO",  empty);
				}

				if(accreditationOp[PROTESTO]!=null){
					createTagString(xmlsw, "PROTESTO",  accreditationOp[PROTESTO]);
				}else{
					createTagString(xmlsw, "PROTESTO",  empty);
				}

				if(accreditationOp[MONTO_EMISION]!=null){
					amountEmision = (BigDecimal)accreditationOp[MONTO_EMISION];
					createTagString(xmlsw, "MONTO_EMISION",  formatDecimal(amountEmision));
				}else{
					createTagString(xmlsw, "MONTO_EMISION",  zero);
				}
				
				if(accreditationOp[ID_SECURITY_CODE_ONLY]!=null){
					createTagString(xmlsw, "ID_SECURITY_CODE_ONLY",  accreditationOp[ID_SECURITY_CODE_ONLY]);
				}else{
					createTagString(xmlsw, "ID_SECURITY_CODE_ONLY",  empty);
				}
				
				if(accreditationOp[NOMINAL_VALUE]!=null){
					nominalValue = (BigDecimal)accreditationOp[NOMINAL_VALUE];
					createTagString(xmlsw, "NOMINAL_VALUE",  formatDecimal(nominalValue));
				}else{
					createTagString(xmlsw, "NOMINAL_VALUE",  zero);
				}
				
				if(accreditationOp[INITIAL_VALUE]!=null){
					initialValue = (BigDecimal)accreditationOp[INITIAL_VALUE];
					createTagString(xmlsw, "INITIAL_VALUE",  formatDecimal(initialValue));
				}else{
					createTagString(xmlsw, "INITIAL_VALUE",  zero);
				}
				
				if(accreditationOp[ID_ISSUER_FK]!=null){
					createTagString(xmlsw, "ID_ISSUER_FK",  accreditationOp[ID_ISSUER_FK]);//SI ES DPF: SERIE ALTERNA, SI ES VALOR DISTINTO A DPF
				}else{
					createTagString(xmlsw, "ID_ISSUER_FK",  empty);
				}
				
				if(accreditationOp[CURRENCY]!=null){
					createTagString(xmlsw, "CURRENCY",  accreditationOp[CURRENCY]);
				}else{
					createTagString(xmlsw, "CURRENCY",  empty);
				}
				
				if(accreditationOp[ID_ISIN_CODE]!=null){
					createTagString(xmlsw, "ID_ISIN_CODE",  accreditationOp[ID_ISIN_CODE]);
				}else{
					createTagString(xmlsw, "ID_ISIN_CODE",  empty);
				}
				
				if(accreditationOp[CFI_CODE]!=null){
					createTagString(xmlsw, "CFI_CODE",  accreditationOp[CFI_CODE]);
				}else{
					createTagString(xmlsw, "CFI_CODE",  empty);
				}
				
				if(accreditationOp[ISSUER]!=null){
					createTagString(xmlsw, "ISSUER",  accreditationOp[ISSUER]);
				}else{
					createTagString(xmlsw, "ISSUER",  empty);
				}
				
				if(accreditationOp[SECURITY_CLASS_DESC_COMPLETE]!=null){
					createTagString(xmlsw, "SECURITY_CLASS_DESC_COMPLETE",  accreditationOp[SECURITY_CLASS_DESC_COMPLETE]);
				}else{
					createTagString(xmlsw, "SECURITY_CLASS_DESC_COMPLETE",  empty);
				}
				
				if(accreditationOp[ISSUANCE_DATE]!=null){
					createTagString(xmlsw, "ISSUANCE_DATE",  accreditationOp[ISSUANCE_DATE]);
				}else{
					createTagString(xmlsw, "ISSUANCE_DATE",  empty);
				}
				
				if(accreditationOp[LIFE_TERM]!=null){
					//BigDecimal lifeTerm= new BigDecimal(accreditationOp[LIFE_TERM].toString());
					createTagString(xmlsw, "LIFE_TERM",  accreditationOp[LIFE_TERM]);
				}else{
					createTagString(xmlsw, "LIFE_TERM",  zero);
				}
				
				if(accreditationOp[VALIDITY_DAYS] != null){
					BigDecimal lifeTerm= new BigDecimal(accreditationOp[VALIDITY_DAYS].toString());
					createTagString(xmlsw, "VALIDITY_DAYS",  lifeTerm);
					days = new Integer(accreditationOp[VALIDITY_DAYS].toString());
				}else{
					createTagString(xmlsw, "VALIDITY_DAYS",  zero);
				}
				
				if(accreditationOp[INTEREST_RATE]!=null){
					//interestRate=((BigDecimal)accreditationOp[INTEREST_RATE]).divide(new BigDecimal(100));
					createTagString(xmlsw, "INTEREST_RATE", CommonsUtilities.formatDecimal(((BigDecimal)accreditationOp[INTEREST_RATE]), ReportConstant.PERCENT_FORMAT_DECIMAL)+"% Anual");
				}else{
					createTagString(xmlsw, "INTEREST_RATE",  zero);
				}
				
				if(accreditationOp[DATE_TYPE]!=null){
					createTagString(xmlsw, "DATE_TYPE",  accreditationOp[DATE_TYPE]);
				}else{
					createTagString(xmlsw, "DATE_TYPE",  empty);
				}

				couponsDetail(accreditationOp, xmlsw, interestRate, nominalValue);

				if(accreditationOp[TOTAL_BALANCE]!=null){
					createTagString(xmlsw, "TOTAL_BALANCE",  accreditationOp[TOTAL_BALANCE]);
				}else{
					createTagString(xmlsw, "TOTAL_BALANCE",  zero);
				}
				
				//issue 541
				if(Validations.validateIsNotNullAndNotEmpty(accreditationOp[ROLE])){
					//1 es COMPRA
					if(Integer.valueOf(accreditationOp[ROLE].toString()).equals(GeneralConstants.ONE_VALUE_INTEGER)){
						if(accreditationOp[REPORTING_BALANCE]!=null){
							createTagString(xmlsw, "REPORTING_BALANCE",  accreditationOp[REPORTING_BALANCE]);
							createTagString(xmlsw, "REPORTED_BALANCE",  zero);
						}else{
							createTagString(xmlsw, "REPORTING_BALANCE",  zero);
							createTagString(xmlsw, "REPORTED_BALANCE",  zero);
						}
					}else{
						//2 es VENTA
						if(accreditationOp[REPORTING_BALANCE]!=null){
							createTagString(xmlsw, "REPORTED_BALANCE",  accreditationOp[REPORTING_BALANCE]);
							createTagString(xmlsw, "REPORTING_BALANCE",  zero);
						}else{
							createTagString(xmlsw, "REPORTING_BALANCE",  zero);
							createTagString(xmlsw, "REPORTED_BALANCE",  zero);
						}
					}
				}else{
					if(accreditationOp[REPORTING_BALANCE]!=null){
						createTagString(xmlsw, "REPORTING_BALANCE",  accreditationOp[REPORTING_BALANCE]);
						createTagString(xmlsw, "REPORTED_BALANCE",  accreditationOp[REPORTING_BALANCE]);
					}else{
						createTagString(xmlsw, "REPORTING_BALANCE",  zero);
						createTagString(xmlsw, "REPORTED_BALANCE",  zero);
					}
				}
				
				if(accreditationOp[BAN_BALANCE]!=null){
					createTagString(xmlsw, "BAN_BALANCE",  accreditationOp[BAN_BALANCE]);
				}else{
					createTagString(xmlsw, "BAN_BALANCE",  zero);
				}
				
				if(accreditationOp[PAWN_BALANCE]!=null){
					createTagString(xmlsw, "PAWN_BALANCE",  accreditationOp[PAWN_BALANCE]);
				}else{
					createTagString(xmlsw, "PAWN_BALANCE",  zero);
				}
				
				if(accreditationOp[AVAILABLE_BALANCE]!=null){
					createTagString(xmlsw, "AVAILABLE_BALANCE",  accreditationOp[AVAILABLE_BALANCE]);
				}else{
					createTagString(xmlsw, "AVAILABLE_BALANCE",  zero);
				}
				
				if(accreditationOp[OTHER_BLOCK_BALANCE]!=null){
					createTagString(xmlsw, "OTHER_BLOCK_BALANCE",  accreditationOp[OTHER_BLOCK_BALANCE]);
				}else{
					createTagString(xmlsw, "OTHER_BLOCK_BALANCE",  zero);
				}

				if(accreditationOp[MOTIVE_DESC]!=null){
					motive= accreditationOp[MOTIVE_DESC].toString();
				}
				
				if(accreditationOp[EXPIRATION_DT_REQUEST]!=null){
					Date expRequest=(Date) accreditationOp[EXPIRATION_DT_REQUEST];
					expirationDtRequest = CommonsUtilities.convertDateToStringLocaleES(expRequest, CommonsUtilities.DATE_PATTERN_DOCUMENT);
				}
				
				//issue 940
				if(accreditationOp[TESTIMONY_NUMBER]!=null){
					createTagString(xmlsw, "NRO_TESTIMONIO",  accreditationOp[TESTIMONY_NUMBER]);
				}else{
					createTagString(xmlsw, "NRO_TESTIMONIO",  empty);
				}
				if(accreditationOp[ISSUANCE_DESCRIPTION]!=null){
					createTagString(xmlsw, "DENOMINACION_EMISION",  accreditationOp[ISSUANCE_DESCRIPTION]);
				}else{
					createTagString(xmlsw, "DENOMINACION_EMISION",  empty);
				}
				
				//issue 940
				// si la clase de valor es PGB colocar el monto capital, caso contrario colocar no aplica
				if(accreditationOp[SECURITY_CLASS].toString().equals(SecurityClassType.PGB.getCode().toString())){
					if(accreditationOp[AMOUNT_SERIE]!=null){
						amountSerie = (BigDecimal)accreditationOp[AMOUNT_SERIE];
						createTagString(xmlsw, "MONTO_SERIE",  formatDecimal(amountSerie));
					}else{
						createTagString(xmlsw, "MONTO_SERIE",  zero);
					}
				}else{
					//se coloca N/A de : No Aplica
					createTagString(xmlsw, "MONTO_SERIE",  "N/A");
				}
				
				//issue 746
				
				AccountEntryRegisterTO accountEntryRegisterTO = new AccountEntryRegisterTO();
				List<RetirementOperationTO> listRetirementOperationTOs = verifyEarlyRedemption(accountEntryRegisterTO,accreditationOp[SECURITY_CLASS_DESC]+"-"+accreditationOp[ID_SECURITY_CODE_ONLY]);
				Date expDate;
				
				//si no tiene redenciones anticipadas
				if(listRetirementOperationTOs.isEmpty()){
					expDate=(Date) accreditationOp[SECURITY_EXPIRATION_DATE];
				}else{
					//si tiene,, la fecha real sera la fecha de operacion
					expDate=(Date) listRetirementOperationTOs.get(0).getInitialDate();
				}
				
				//Solo para valores CFC
				if(accreditationOp[SECURITY_CLASS].toString().equals(SecurityClassType.CFC.getCode().toString())){
					expDate=(Date) accreditationOp[SECURITY_EXPIRATION_FONDO_DATE];
				}
				
				createTagString(xmlsw, "REAL_EXPIRATION_DATE", CommonsUtilities.convertDateToStringLocaleES(expDate, CommonsUtilities.DATE_PATTERN));

			}
		}
		//INTRANSFERIBLE negrita
		firstParagraph.append("El presente Certificado es INTRANSFERIBLE y ser\u00E1n nulos los actos de su disposici\u00F3n o gravamen sobre el mismo. Mientras el plazo de");
		firstParagraph.append(" vigencia del presente certificado no haya caducado o hasta que este sea restituido a la Caja de Valores del Paraguay S.A.,");
		firstParagraph.append(" CAVAPY no dar\u00E1 curso a transferencias, ni efectuar\u00E1 nuevas inscripciones respecto de los valores certificados. \n\n");
		firstParagraph.append("El presente certificado se emite en cumplimiento de las normas legales y tiene la finalidad u objeto de realizar el ejercicio: ");
		firstParagraph.append(motive + " y solamente acredita la materia objeto de certificaci\u00F3n por "+days+" d\u00EDa(s) calendario, a partir de la fecha de su");
		firstParagraph.append(" emisi\u00F3n, siendo su fecha de vencimiento el "+ expirationDtRequest + ".");
		createTagString(xmlsw, "firstParagraph",  firstParagraph);

		thirdParagraph.append("Asunción, "+ CommonsUtilities.convertDateToStringLocaleES(completeDate, CommonsUtilities.DATE_PATTERN_DOCUMENT_WITH_HOURS));
		createTagString(xmlsw, "thirdParagraph",  thirdParagraph);
	}
		
	/**
	 * Coupons detail.
	 *
	 * @param accreditationOp the accreditation op
	 * @param xmlsw the xmlsw
	 * @param interestRate the interest rate
	 * @param nominalValue the nominal value
	 * @throws XMLStreamException the XML stream exception
	 */
	public void couponsDetail(Object[] accreditationOp, XMLStreamWriter xmlsw, BigDecimal interestRate, BigDecimal nominalValue) throws XMLStreamException{
		String empty="---";
		String zero="0";
		Date expirationDt=new Date();
		
		xmlsw.writeStartElement("coupons");
		if(accreditationOp[NUMBER_COUPONS] != null && Integer.parseInt(accreditationOp[NUMBER_COUPONS].toString()) > 0
				&& InstrumentType.FIXED_INCOME.getCode().equals(((BigDecimal)accreditationOp[INSTRUMENT_TYPE]).intValue())){
			if(accreditationOp[COUPON_NUMBER_INT]!=null && accreditationOp[COUPON_NUMBER_AMORT]!=null){
				if(accreditationOp[COUPON_NUMBER_INT].equals(accreditationOp[COUPON_NUMBER_AMORT])){
					//same Coupon
					createTagString(xmlsw, "COUPON_NUMBER",  accreditationOp[COUPON_NUMBER_INT]);//accreditationOp[COUPON_NUMBER_AMORT]
				}else if(accreditationOp[COUPON_NUMBER_INT]!=null){
					createTagString(xmlsw, "COUPON_NUMBER",  accreditationOp[COUPON_NUMBER_INT]);
				}else if(accreditationOp[COUPON_NUMBER_AMORT]!=null){
					createTagString(xmlsw, "COUPON_NUMBER",  accreditationOp[COUPON_NUMBER_AMORT]);
				}else{
					createTagString(xmlsw, "COUPON_NUMBER",  empty);
				}
			}else if(accreditationOp[COUPON_NUMBER_INT]!=null){
				createTagString(xmlsw, "COUPON_NUMBER",  accreditationOp[COUPON_NUMBER_INT]);
			}else if(accreditationOp[COUPON_NUMBER_AMORT]!=null){
				createTagString(xmlsw, "COUPON_NUMBER",  accreditationOp[COUPON_NUMBER_AMORT]);
			}else{
				createTagString(xmlsw, "COUPON_NUMBER",  empty);
			}
		}else{
			createTagString(xmlsw, "COUPON_NUMBER",  empty);
		}
		
		if(accreditationOp[ID_SECURITY_CODE_PK]!=null){
			createTagString(xmlsw, "ID_SECURITY_CODE_PK",  accreditationOp[ID_SECURITY_CODE_PK]);
		}else{
			createTagString(xmlsw, "ID_SECURITY_CODE_PK",  zero);
		}
		
		//				if(accreditationOp[EXPIRATION_DATE_INT]!=null && accreditationOp[EXPIRATION_DATE_AMORT]!=null){
		//					if(accreditationOp[EXPIRATION_DATE_INT].equals(accreditationOp[EXPIRATION_DATE_AMORT])){
		//						//same Coupon
		//						createTagString(xmlsw, "EXPIRATION_DATE",  accreditationOp[EXPIRATION_DATE_INT]);//accreditationOp[EXPIRATION_DATE_AMORT]
		//					}else{
		//						createTagString(xmlsw, "EXPIRATION_DATE",  empty);
		//					}
		//				}else 
		if(accreditationOp[NUMBER_COUPONS] != null && Integer.parseInt(accreditationOp[NUMBER_COUPONS].toString()) > 0
				&& InstrumentType.FIXED_INCOME.getCode().equals(((BigDecimal)accreditationOp[INSTRUMENT_TYPE]).intValue())){
			if(accreditationOp[EXPIRATION_DATE_INT]!=null){
				expirationDt=(Date)accreditationOp[EXPIRATION_DATE_STANDAR];
				createTagString(xmlsw, "EXPIRATION_DATE",  accreditationOp[EXPIRATION_DATE_INT]);
			}else if(accreditationOp[EXPIRATION_DATE_AMORT]!=null){
				createTagString(xmlsw, "EXPIRATION_DATE",  accreditationOp[EXPIRATION_DATE_AMORT]);
			}else{
				createTagString(xmlsw, "EXPIRATION_DATE",  empty);
			}				
		}else{
			createTagString(xmlsw, "EXPIRATION_DATE",  empty);
		}
		
		
		//				if(accreditationOp[LIFE_TERM_COUPON_INT]!=null && accreditationOp[LIFE_TERM_COUPON_AMORT]!=null){
		//					if(accreditationOp[LIFE_TERM_COUPON_INT].equals(accreditationOp[LIFE_TERM_COUPON_AMORT])){
		//						//same coupon
		//						createTagString(xmlsw, "LIFE_TERM_COUPON",  accreditationOp[LIFE_TERM_COUPON_INT]);//accreditationOp[LIFE_TERM_COUPON_AMORT]
		//					}else{
		//						createTagString(xmlsw, "LIFE_TERM_COUPON",  zero);
		//					}
		//				}else 
		if(accreditationOp[NUMBER_COUPONS] != null && Integer.parseInt(accreditationOp[NUMBER_COUPONS].toString()) > 0
				&& InstrumentType.FIXED_INCOME.getCode().equals(((BigDecimal)accreditationOp[INSTRUMENT_TYPE]).intValue())){
			if(accreditationOp[LIFE_TERM_COUPON_INT]!=null){
				createTagString(xmlsw, "LIFE_TERM_COUPON",  accreditationOp[LIFE_TERM_COUPON_INT]);
			}else if(accreditationOp[LIFE_TERM_COUPON_AMORT]!=null){
				createTagString(xmlsw, "LIFE_TERM_COUPON",  accreditationOp[LIFE_TERM_COUPON_AMORT]);
			}else{
				createTagString(xmlsw, "LIFE_TERM_COUPON",  zero);
			}
		}else{
			createTagString(xmlsw, "LIFE_TERM_COUPON",  empty);
		}
		
		
		//				if(accreditationOp[STATE_INTEREST]!=null && accreditationOp[STATE_AMORT]!=null){
		//					if(accreditationOp[STATE_INTEREST].equals(accreditationOp[STATE_AMORT])){
		//						//same state
		//						createTagString(xmlsw, "STATE",  accreditationOp[STATE_INTEREST]);
		//					}else{
		//						createTagString(xmlsw, "STATE",  empty);
		//					}	
		//				}else 
		if(accreditationOp[STATE_INTEREST]!=null){
			createTagString(xmlsw, "STATE_COUPON",  accreditationOp[STATE_INTEREST]);
		}else if(accreditationOp[STATE_AMORT]!=null){
			createTagString(xmlsw, "STATE_COUPON",  accreditationOp[STATE_AMORT]);
		}else{
			createTagString(xmlsw, "STATE_COUPON",  empty);
		}
		
//			//getting total amount of interest
		BigDecimal calendarDays= new BigDecimal(1);
		Date beginingDt= new Date();
		BigDecimal desmaterialized= new BigDecimal(1);
		if(accreditationOp[CALENDAR_DAYS]!=null && !accreditationOp[CALENDAR_DAYS].toString().equals(zero)){
			calendarDays=(BigDecimal)accreditationOp[CALENDAR_DAYS];
		}
		if(accreditationOp[BEGINING_DATE]!=null){
			beginingDt=(Date)accreditationOp[BEGINING_DATE];
		}
		if(accreditationOp[DESMATERIALIZED_BALANCE]!=null){
			desmaterialized=(BigDecimal)accreditationOp[DESMATERIALIZED_BALANCE];
		}
		
		if(ProgramScheduleStateType.PENDING.getValue().equals(accreditationOp[STATE_INTEREST])){
			Calendar expirationDate	= Calendar.getInstance();
			Calendar beginingDate	= Calendar.getInstance();			
			//Comparing dates
			expirationDate.setTime(expirationDt);
			beginingDate.setTime(beginingDt);
			//Getting hours from dates
			int hoursLeftToExpire= (int)((expirationDate.getTimeInMillis() - beginingDate.getTimeInMillis()) / (60 * 60 * 1000));
			int daysLeft = hoursLeftToExpire/24; //Getting days left
			BigDecimal diffBetweenDates= new BigDecimal(daysLeft);
			BigDecimal amountTotalPayedPndi= interestRate.multiply(nominalValue.divide(calendarDays, RoundingMode.HALF_DOWN)).multiply(
											 diffBetweenDates.add(new BigDecimal(1))).multiply(desmaterialized);
			createTagString(xmlsw, "AMOUNT_TOTAL_PAYED_PNDI",  formatDecimal(amountTotalPayedPndi));
		}else{
			if(accreditationOp[AMOUNT_TOTAL_PAYED]!=null){
				createTagString(xmlsw, "AMOUNT_TOTAL_PAYED",  formatDecimal(accreditationOp[AMOUNT_TOTAL_PAYED]));
			}else{
				createTagString(xmlsw, "AMOUNT_TOTAL_PAYED",  zero);
			}
			
		}
		
		if(accreditationOp[NUMBER_COUPONS] != null && Integer.parseInt(accreditationOp[NUMBER_COUPONS].toString()) > 0
				&& InstrumentType.FIXED_INCOME.getCode().equals(((BigDecimal)accreditationOp[INSTRUMENT_TYPE]).intValue())
				&& Integer.parseInt(accreditationOp[NUMBER_COUPONS_INT].toString()) > 0 ){
			if(accreditationOp[INTEREST_AMORTIZATION]!=null){
				createTagString(xmlsw, "INTEREST_AMORTIZATION",  formatDecimal(accreditationOp[INTEREST_AMORTIZATION]));
			}else{
				createTagString(xmlsw, "INTEREST_AMORTIZATION",  GeneralConstants.ZERO_DECIMAL_STRING);
			}
		}else{
			createTagString(xmlsw, "INTEREST_AMORTIZATION",  empty);
		}
		
		
		BigDecimal capitalAmort = GeneralConstants.ZERO;
		BigDecimal interestAmort = GeneralConstants.ZERO;
		
		if(accreditationOp[NUMBER_COUPONS] != null && Integer.parseInt(accreditationOp[NUMBER_COUPONS].toString()) > 0
				&& InstrumentType.FIXED_INCOME.getCode().equals(((BigDecimal)accreditationOp[INSTRUMENT_TYPE]).intValue())){
			if(accreditationOp[CAPITAL_AMORT_PNDI] != null){
				createTagString(xmlsw, "CAPITAL_AMORT",  formatDecimal(accreditationOp[CAPITAL_AMORT_PNDI]));
			}else{
				createTagString(xmlsw, "CAPITAL_AMORT",  GeneralConstants.ZERO_DECIMAL_STRING);
			}
			
			if(accreditationOp[CAPITAL_AMORT_PNDI] != null)
				capitalAmort = new BigDecimal(accreditationOp[CAPITAL_AMORT_PNDI].toString());
			if(accreditationOp[INTEREST_AMORTIZATION] != null)
				interestAmort = new BigDecimal(accreditationOp[INTEREST_AMORTIZATION].toString());
			
			createTagString(xmlsw, "TOTAL_EXPIRATION_SECURITY",  formatDecimal(capitalAmort.add(interestAmort)));
			
		}else{
			createTagString(xmlsw, "CAPITAL_AMORT",  empty);
			createTagString(xmlsw, "INTEREST_AMORTIZATION",  empty);
			nominalValue = (BigDecimal)accreditationOp[NOMINAL_VALUE];
			createTagString(xmlsw, "TOTAL_EXPIRATION_SECURITY",  formatDecimal(nominalValue));
		}
		
		
		//			if(ProgramScheduleStateType.PAID.getValue().equals(accreditationOp[STATE_INTEREST])){
		//				createTagString(xmlsw, "CHARGED",  BooleanType.YES.getValue());//only if the coupon was paid
		//			}else{
		//				createTagString(xmlsw, "CHARGED",  BooleanType.NO.getValue());
		//			}
		//close tag coupons
		xmlsw.writeEndElement();
	}
	
	//Verificando las redenciones anticipadas
	public List<RetirementOperationTO> verifyEarlyRedemption(AccountEntryRegisterTO accountEntryRegisterTO,String code) throws ServiceException {
		RetirementOperationTO retirementOperationTO = new RetirementOperationTO();
        
        retirementOperationTO.setState(RetirementOperationStateType.CONFIRMED.getCode());
        retirementOperationTO.setMotive(RetirementOperationMotiveType.REDEMPTION_FIXED_INCOME.getCode());
        //
        retirementOperationTO.setSecurity(new Security(code));
        // buscando operacion de Redencion anticipada
        return custodyReportServiceBean.searchRetirementOperations(retirementOperationTO);
        
	}
	
}
