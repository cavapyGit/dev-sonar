package com.pradera.report.generation.executor.issuances;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.service.HolderQueryServiceBean;
import com.pradera.core.component.helperui.to.HolderSearchTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.issuances.services.IssuancesReportServiceBean;
import com.pradera.report.generation.executor.issuances.to.RelationshipOfBuyersOfBankSharesTO;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RelationshipOfBuyersOfBankSharesReport.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01-sep-2015
 */
@ReportProcess(name = "RelationshipOfBuyersOfBankSharesReport")
public class RelationshipOfBuyersOfBankSharesReport extends GenericReport{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The parameter service. */
	@EJB
	private ParameterServiceBean parameterService;
	
	/** The participant service bean. */
	@EJB
	private ParticipantServiceBean participantServiceBean;

	/** The holder query service bean. */
	@EJB
	private HolderQueryServiceBean holderQueryServiceBean;
	
	/** The issuances report service. */
	@EJB
	private IssuancesReportServiceBean issuancesReportService;
	
	/** The accounts facade. */
	@EJB
	private AccountsFacade accountsFacade;
	
	/** The helper component facade. */
	@EJB
	private HelperComponentFacade helperComponentFacade;
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/**
	 * Instantiates a new relationship of buyers of bank shares report.
	 */
	public RelationshipOfBuyersOfBankSharesReport() {
		// TODO Auto-generated constructor stub
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#generateData(com.pradera.model.report.ReportLogger)
	 */
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#getCustomJasperParameters()
	 */
	@Override
	public Map<String, Object> getCustomJasperParameters() {

		RelationshipOfBuyersOfBankSharesTO gfto = new RelationshipOfBuyersOfBankSharesTO();

		for(ReportLoggerDetail param: getReportLogger().getReportLoggerDetails()){
			if(param.getFilterName().equals("participant")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setParticipant(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("cui_code")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setCui(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("holder_account")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setHolderAccount(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("security_class_key")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setSecurities(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("state_request")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setState(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("date_initial")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setInitialDate(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("date_end")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setFinalDate(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("institution_type")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setInstitutionType(param.getFilterValue().toString());
				}
			}
		} 
		
		if(gfto.getInstitutionType()!=null && Integer.valueOf(gfto.getInstitutionType()).equals(InstitutionType.PARTICIPANT_INVESTOR.getCode())){
			Participant objParticipant = accountsFacade.getParticipantByPk(Long.valueOf(gfto.getParticipant()));					
			HolderAccountTO holderAccountTO = new HolderAccountTO();
			holderAccountTO.setParticipantTO(objParticipant.getIdParticipantPk());
			
			List<Long> lstHolderPk = new ArrayList<Long>();		
			
			if(Validations.validateIsNotNull(gfto.getCui())) {
				lstHolderPk.add(Long.valueOf(gfto.getCui()));
			}else{
				HolderSearchTO holderSearchTO = new HolderSearchTO();
				holderSearchTO.setDocumentType(objParticipant.getDocumentType());
				holderSearchTO.setDocumentNumber(objParticipant.getDocumentNumber());
				holderSearchTO.setState(HolderStateType.REGISTERED.getCode());
				List<Holder> lstHolder = helperComponentFacade.findHolders(holderSearchTO);
				if(lstHolder!=null && lstHolder.size()>0){
					for(Holder objHolder : lstHolder){
						lstHolderPk.add(objHolder.getIdHolderPk());
					}
				}
			}			
			
			holderAccountTO.setLstHolderPk(lstHolderPk);
			List<HolderAccount> lstHolderAccount = null;
			if(holderAccountTO.getLstHolderPk()!=null && holderAccountTO.getLstHolderPk().size()>0){
				 try {
					lstHolderAccount = accountsFacade.getHolderAccountListComponentServiceFacade(holderAccountTO);
				} catch (ServiceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}						
			List<Long> lstHolderAccountPk = new ArrayList<Long>();
			if(lstHolderAccount!=null && lstHolderAccount.size()>0){
				for(HolderAccount objHolderAccount : lstHolderAccount){
					lstHolderAccountPk.add(objHolderAccount.getIdHolderAccountPk());
				}
			}
			gfto.setLstHolderAccountPk(lstHolderAccountPk);
		}
		String strQuery = issuancesReportService.getRelationshipOfBuyersOfBankSharesReport(gfto); 
		
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> secClassState 			= new HashMap<Integer, String>();
		Map<Integer,String> secClassDocType 		= new HashMap<Integer, String>();
		Participant participant = new Participant();
		Holder holder = new Holder();
		HolderAccount holderAccount = new HolderAccount();

		
		try {
			filter.setMasterTableFk(MasterTableType.DEMATERIALIZATION_STATE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secClassState.put(param.getParameterTablePk(), param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secClassDocType.put(param.getParameterTablePk(), param.getIndicator1());
			}

			if(Validations.validateIsNotNull(gfto.getParticipant())){
				participant.setIdParticipantPk(Long.valueOf(gfto.getParticipant()));
				participant = participantServiceBean.findParticipantByFiltersServiceBean(participant);
			}
			if(Validations.validateIsNotNull(gfto.getCui())){
				holder = holderQueryServiceBean.findHolderByCode(Long.valueOf(gfto.getCui()));
			}
			if(Validations.validateIsNotNull(gfto.getHolderAccount())){
				holderAccount = issuancesReportService.findHolderAccountByPk(Long.valueOf(gfto.getHolderAccount()));
			}
			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}

		parametersRequired.put("strQuer", strQuery);
		parametersRequired.put("initialDt", gfto.getInitialDate());
		parametersRequired.put("finalDt", gfto.getFinalDate());
		parametersRequired.put("mState", secClassState);
		parametersRequired.put("mDocType", secClassDocType);
		
		parametersRequired.put("pParticipant", participant.getDescription());
		parametersRequired.put("pHolder", holder.getFullName());
		parametersRequired.put("pAccount", holderAccount.getAccountNumber());
		parametersRequired.put("pValor", gfto.getSecurities());
		if(Validations.validateIsNotNullAndNotEmpty(gfto.getState())){
			parametersRequired.put("pState", secClassState.get(new Integer(gfto.getState())));
		} else {
			parametersRequired.put("pState", GeneralConstants.EMPTY_STRING);
		}
				
		return parametersRequired;
		
		
	}

}
