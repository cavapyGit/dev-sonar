
package com.pradera.report.generation.poi;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.pentaho.reporting.engine.classic.core.modules.output.table.xls.helper.HSSFCellStyleProducer;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.helperui.to.AccountingAccountTo;
import com.pradera.core.component.helperui.to.AccountingParameterTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounting.AccountingAccount;
import com.pradera.model.accounting.AccountingParameter;
import com.pradera.model.accounting.AccountingProcess;
import com.pradera.model.accounting.type.AccountingBranchType;
import com.pradera.model.accounting.type.AccountingConciliationType;
import com.pradera.model.accounting.type.AccountingParameterStateType;
import com.pradera.model.accounting.type.AccountingParameterType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.DailyExchangeRoleType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.report.ReportLogger;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.accounting.service.AccountingReportQueryServiceBean;
import com.pradera.report.generation.executor.accounting.service.AccountingReportServiceBean;
import com.pradera.report.generation.executor.accounting.to.AccountingProcessTo;
import com.pradera.report.generation.executor.accounting.to.AccountingReceiptTo;
import com.pradera.report.generation.executor.accounting.to.OperationBalanceTo;
import com.pradera.report.generation.executor.custody.to.ExpirationReportTO;
import com.pradera.report.generation.executor.custody.to.XmlOperationsSirtex;
import com.pradera.report.generation.executor.custody.to.XmlRegSirtex;
import com.pradera.report.generation.executor.custody.to.XmlSeccSirtex;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ReportPoiGeneratorServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 11/09/2015
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class ReportPoiGeneratorServiceBean  {
	
	/** The accounting report query service bean. */
	@Inject
	AccountingReportQueryServiceBean accountingReportQueryServiceBean;
	
	/** The accounting report service bean. */
	@Inject
	AccountingReportServiceBean 	accountingReportServiceBean;
	
	/** Work Bock of the Excel*. */
	protected HSSFWorkbook hSSFWorkbook;
	
	/** Work Bock of the Excel*. */
	protected SXSSFWorkbook  test;
	
	/** The c s title report. */
	protected HSSFCellStyle cSTitleReport;
	
	/** The c s title bold. */
	protected HSSFCellStyle cSTitleBold;
	
	/** The c s title back blue. */
	protected HSSFCellStyle cSTitleBackBlue;
	
	/** The c s title. */
	protected HSSFCellStyle cSTitle;
	
	/** The c s title. */
	protected XSSFCellStyle cSTitleX;
	
	/** The c s title header back blue ten. */
	protected HSSFCellStyle cSTitleHeaderBackBlueTen;
	
	/** The c s title header back blue twelve. */
	protected HSSFCellStyle cSTitleHeaderBackBlueTwelve;
	
	/** The c s title header back blue twelve. */
	protected HSSFCellStyle cSContentCenter;
	
	/** The c s total account. */
	protected HSSFCellStyle cSTotalAccount;
	
	/** The c s total account amount quantity. */
	protected HSSFCellStyle cSTotalAccountAmountQuantity;
	
	/** The c s amount debit quantity. */
	protected HSSFCellStyle cSAmountDebitQuantity;
	
	/** The c s total amount assets quantity. */
	protected HSSFCellStyle cSTotalAmountAssetsQuantity;
	
	/** The c s total account amount. */
	protected HSSFCellStyle cSTotalAccountAmount;
	
	/** The c s amount debit. */
	protected HSSFCellStyle cSAmountDebit;
	
	/** The c s amount assets. */
	protected HSSFCellStyle cSAmountAssets;
	
	/** The c s accounting account. */
	protected HSSFCellStyle cSAccountingAccount;
	
	/** The c s accounting account. */
	protected XSSFCellStyle cSAccountingAccountX;
	
	protected HSSFCellStyle cSAccountingAccountAmountI1091;
	
	protected HSSFCellStyle cSAccountingAccountOnlyNumber1179;
	
	protected HSSFCellStyle cSAccountingAccountDate1179;
	
	protected HSSFCellStyle cSAccountingAccountAmountBoldI1091;
	
	protected HSSFCellStyle cSAccountingAccountPercentI1091;
	
	/** The c s accounting account father. */
	protected HSSFCellStyle cSAccountingAccountFather;
	
	/** The cs border. */
	protected HSSFCellStyle csBorder;
	
	/** The cs border. */
	protected XSSFCellStyle csBorderX;
	
	/** The cs description accounting account. */
	protected HSSFCellStyle csDescriptionAccountingAccount;
	
	/** The cs description accounting account. */
	protected HSSFCellStyle csDate;
	
	/** The c f title report. */
	protected Font cFTitleReport;
	
	/** The c f title bold. */
	protected Font cFTitleBold;
	
	/** The c f title back blue. */
	protected Font cFTitleBackBlue;
	
	/** The c f title. */
	protected Font cFTitle;
	
	/** The c f title header back blue ten. */
	protected Font cFTitleHeaderBackBlueTen;
	
	/** The c f title header back blue twelve. */
	protected Font cFTitleHeaderBackBlueTwelve;
	
	/** The c f total account. */
	protected Font cFTotalAccount;
	
	/** The c f total account amount quantity. */
	protected Font cFTotalAccountAmountQuantity;
	
	/** The c f amount debit quantity. */
	protected Font cFAmountDebitQuantity;
	
	/** The c f amount assets quantity. */
	protected Font cFAmountAssetsQuantity;
	
	/** The c f total account amount. */
	protected Font cFTotalAccountAmount;
	
	/** The c f amount debit. */
	protected Font cFAmountDebit;
	
	/** The c f amount assets. */
	protected Font cFAmountAssets;
	
	/** The c f accounting account. */
	protected Font cFAccountingAccount;
	
	/** The c f accounting account father. */
	protected Font cFAccountingAccountFather;
	
	/** The c f description accounting account. */
	protected Font cFDescriptionAccountingAccount;
	
	protected Font cFAccountingAccountAmountBoldI1091;
	
	/** The d f total account amount quantity. */
	protected DataFormat dFTotalAccountAmountQuantity;
	
	/** The d f amount debit quantity. */
	protected DataFormat dFAmountDebitQuantity;
	
	/** The d f amount assets quantity. */
	protected DataFormat dFAmountAssetsQuantity;
	
	/** The d f total account amount. */
	protected DataFormat dFTotalAccountAmount;
	
	/** The d f amount debit. */
	protected DataFormat dFAmountDebit;
	
	/** The d f amount assets. */
	protected DataFormat dFAmountAssets;
	
	/** The Max Rows Excel 2007*/
	private final int MAX_ROWS_EXCEL_2007=50000;
	/**
	 * Instantiates a new accounting poi generator service bean.
	 */
	public ReportPoiGeneratorServiceBean(){
		

	}
	
	/**
	 * Inits the cell style.
	 */
	public void initCellStyle(){
		
		hSSFWorkbook = new HSSFWorkbook();
		
		 cSTitleReport 					= hSSFWorkbook.createCellStyle();
		 cSTitleBold 					= hSSFWorkbook.createCellStyle();
		 cSTitleBackBlue				= hSSFWorkbook.createCellStyle();
		 cSTitle 						= hSSFWorkbook.createCellStyle();
		 cSTitleHeaderBackBlueTen		= hSSFWorkbook.createCellStyle();
		 cSTitleHeaderBackBlueTwelve	= hSSFWorkbook.createCellStyle();
		 cSTotalAccount 				= hSSFWorkbook.createCellStyle();
		 cSContentCenter				= hSSFWorkbook.createCellStyle();
		 csDate							= hSSFWorkbook.createCellStyle();
		 cSTotalAccountAmountQuantity 	= hSSFWorkbook.createCellStyle();
		 cSAmountDebitQuantity 			= hSSFWorkbook.createCellStyle();
		 cSTotalAmountAssetsQuantity 	= hSSFWorkbook.createCellStyle();
		 cSTotalAccountAmount 			= hSSFWorkbook.createCellStyle();
		 cSAmountDebit 					= hSSFWorkbook.createCellStyle();
		 cSAmountAssets 				= hSSFWorkbook.createCellStyle();
		 cSAccountingAccount 			= hSSFWorkbook.createCellStyle();
		 cSAccountingAccountFather 		= hSSFWorkbook.createCellStyle();
		 csBorder 						= hSSFWorkbook.createCellStyle();
		 csDescriptionAccountingAccount = hSSFWorkbook.createCellStyle();
		 cSAccountingAccountAmountI1091	= hSSFWorkbook.createCellStyle();
		 cSAccountingAccountAmountBoldI1091 = hSSFWorkbook.createCellStyle();
		 cSAccountingAccountPercentI1091= hSSFWorkbook.createCellStyle();
		 cSAccountingAccountOnlyNumber1179 = hSSFWorkbook.createCellStyle();
		 cSAccountingAccountDate1179 = hSSFWorkbook.createCellStyle();
	}
	
	/**
	 * Inits the font.
	 */
	public void initFont(){
		
		cFTitleReport					=	hSSFWorkbook.createFont();
		cFTitleBold						=	hSSFWorkbook.createFont();
		cFTitleBackBlue					= 	hSSFWorkbook.createFont();
		cFTitle							=	hSSFWorkbook.createFont();
		cFTitleHeaderBackBlueTen		= 	hSSFWorkbook.createFont();
		cFTitleHeaderBackBlueTwelve		= 	hSSFWorkbook.createFont();
		cFTotalAccount					=	hSSFWorkbook.createFont();
		cFTotalAccountAmountQuantity	=	hSSFWorkbook.createFont();
		cFAmountDebitQuantity			=	hSSFWorkbook.createFont();
		cFAmountAssetsQuantity			=	hSSFWorkbook.createFont();
		cFTotalAccountAmount			=	hSSFWorkbook.createFont();
		cFAmountDebit					=	hSSFWorkbook.createFont();
		cFAmountAssets					=	hSSFWorkbook.createFont();
		cFAccountingAccount				=	hSSFWorkbook.createFont();
		cFAccountingAccountFather		=	hSSFWorkbook.createFont();
		cFDescriptionAccountingAccount	=	hSSFWorkbook.createFont();
		cFAccountingAccountAmountBoldI1091 = hSSFWorkbook.createFont();
	}
	
	/**
	 * Inits the data format.
	 */
	public void initDataFormat(){

		dFTotalAccountAmountQuantity 	= hSSFWorkbook.createDataFormat();
		dFAmountDebitQuantity 			= hSSFWorkbook.createDataFormat();
		dFAmountAssetsQuantity 			= hSSFWorkbook.createDataFormat();
		dFTotalAccountAmount 			= hSSFWorkbook.createDataFormat();
		dFAmountDebit 					= hSSFWorkbook.createDataFormat();
		dFAmountAssets 					= hSSFWorkbook.createDataFormat();

	}
	
	
	
	

	/**
	 * *
	 * Returns the range of columns on which act the formula. Ex: (B2: F2)
	 *
	 * @param numRow the num row
	 * @return the string
	 */
    private static String generateRangeFormulaInRow(int numRow) {
    	/***The column where the first time the situation will be F (ASCII code 70))*/
        final byte columnF = 70;
        final char firstColumn = (char)columnF;
        final char lastColumn = (char)columnF + 1;
        return "(" + firstColumn + numRow + ":" + lastColumn + numRow + ")";
    }
    
    /**
     * Generate Range Formula in Column Select.
     *
     * @param numColumn the num column
     * @param begin the begin
     * @param end the end
     * @return Ex. B28:B35  (1,28,65)
     */
    private static String generateRangeFormulaInColumn(Integer numColumn,int begin,int end) {
    	/***The column where the first time the situation will be F (ASCII code 70))*/
        final byte columnA= 65;
        final char columnRange = (char)(columnA+numColumn);
//        final char lastColumn = (char)columnF + 1;
        return "(" + columnRange + begin + ":" + columnRange + end + ")";
    }
    
    /**
     * Function SUB DEBIT-ASSETS=BALANCE.
     *
     * @param numRow the num row
     * @return the string
     */
    private static String funtionSubBalance(int numRow) {
    	final byte columnF = 70;
        final char firstColumn = (char)columnF;
        final char lastColumn = (char)columnF + 1;
    	return  ""+firstColumn + numRow+"-"+ lastColumn + numRow;
    }
	
	/**
	 * Style Header Bold
	 * *.
	 *
	 * @return the style title report
	 */
	public HSSFCellStyle getStyleTitleReport() {
		HSSFCellStyle cellStyle = cSTitleReport;
	    final Font cellFont = cFTitleReport;
	    cellFont.setFontHeightInPoints((short)20);
	    cellFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    cellStyle.setFont(cellFont);
	    return cellStyle;
	}
	
	/**
	 * Style Header Bold
	 * *.
	 *
	 * @return the style title bold
	 */
	public HSSFCellStyle getStyleTitleBold() {
		HSSFCellStyle cellStyle = cSTitleBold;
	    final Font cellFont = cFTitleBold;
	    cellFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    cellStyle.setFont(cellFont);
	    return cellStyle;
	}
	
	public HSSFCellStyle getStyleTitleBoldCenter() {
		HSSFCellStyle cellStyle = cSTitleBold;
	    final Font cellFont = cFTitleBold;
	    cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
	    cellFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    cellStyle.setFont(cellFont);
	    return cellStyle;
	}
	
	/**
	 * Style Header debit assets
	 * *.
	 *
	 * @return the style title
	 */
	public HSSFCellStyle getStyleTitle() {
		HSSFCellStyle cellStyle = cSTitle;
	    final Font cellFont = cFTitle;
	    cellFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER_SELECTION);
	    cellStyle.cloneStyleFrom(setBorder(2,2,2,2));
	    cellStyle.setFont(cellFont);
	    cellStyle.setFillBackgroundColor(HSSFColor.LIGHT_BLUE.index);
	    return cellStyle;
	}
	
	public XSSFCellStyle getStyleTitleX() {
		
		XSSFWorkbook wb = new XSSFWorkbook();
		
		cSTitleX = wb.createCellStyle();
		
		XSSFCellStyle cellStyle = cSTitleX;
	    final Font cellFont = cFTitle;
	    cellFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    cellStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER_SELECTION);
	    cellStyle.cloneStyleFrom(setBorderX(2,2,2,2));
	    cellStyle.setFont(cellFont);
	    
	    XSSFColor myColor = new XSSFColor(Color.LIGHT_GRAY);
	    
	    cellStyle.setFillBackgroundColor(myColor);
	    return cellStyle;
	}
	
	/**
	 * Style Header Bold Background Blue
	 * *.
	 *
	 * @return the style title bold back blue
	 */
	public HSSFCellStyle getStyleTitleBoldBackBlue() {
		HSSFCellStyle cellStyle = cSTitleBackBlue;
	    final Font cellFont = cFTitleBackBlue;
	    cellFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    cellFont.setFontHeight((short)400);
	    cellFont.setColor(HSSFColor.BLACK.index);
	    cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
	    cellStyle.cloneStyleFrom(setBorder(2,2,2,2));
	    cellStyle.setFont(cellFont);
	    //BackGround Color Blue
	    cellStyle.setFillPattern(CellStyle.BIG_SPOTS);
	    cellStyle.setFillForegroundColor(HSSFColor.WHITE.index);
	    cellStyle.setFillBackgroundColor(HSSFColor.LIGHT_BLUE.index);
	    return cellStyle;
	}

	
	/**
	 * Style Header Bold Background Blue
	 * *.
	 *
	 * @return the style header title bold back blue ten
	 */
	public HSSFCellStyle getStyleHeaderTitleBoldBackBlueTen() {
		HSSFCellStyle cellStyle = cSTitleHeaderBackBlueTen;
	    final Font cellFont = cFTitleHeaderBackBlueTen;
	    cellFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    cellFont.setFontHeight((short)200);
	    cellFont.setColor(HSSFColor.BLACK.index);
	    cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
	    cellStyle.cloneStyleFrom(setBorder(2,2,2,2));
	    cellStyle.setFont(cellFont);
	    //BackGround COlor Blue
	    cellStyle.setFillPattern(CellStyle.BIG_SPOTS);
	    cellStyle.setFillForegroundColor(HSSFColor.WHITE.index);
	    cellStyle.setFillBackgroundColor(HSSFColor.LIGHT_BLUE.index);
	    return cellStyle;
	}
	
	/**
	 * Style Header Bold Background Blue
	 * *.
	 *
	 * @return the style header title bold back blue twelve
	 */
	public HSSFCellStyle getStyleHeaderTitleBoldBackBlueTwelve() {
		HSSFCellStyle cellStyle = cSTitleHeaderBackBlueTwelve;
	    final Font cellFont = cFTitleHeaderBackBlueTwelve;
	    cellFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    cellFont.setFontHeight((short)220);
	    cellFont.setColor(HSSFColor.BLACK.index);
	    cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
	    cellStyle.cloneStyleFrom(setBorder(2,2,2,2));
	    cellStyle.setFont(cellFont);
	    //BackGround COlor Blue
	    cellStyle.setFillPattern(CellStyle.DIAMONDS);
	    cellStyle.setFillForegroundColor(HSSFColor.WHITE.index);
	    cellStyle.setFillBackgroundColor(HSSFColor.LIGHT_BLUE.index);
	    return cellStyle;
	}
	
	/**
	 * Style Center
	 * *.
	 *
	 * @return the style header title bold back blue ten
	 */
	public HSSFCellStyle getStyleContentCenter() {
		HSSFCellStyle cellStyle = cSContentCenter;
	    cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
	    cellStyle.cloneStyleFrom(setBorder(0,1,0,1));
	    return cellStyle;
	}
	
	/**
	 * Style Date
	 * *.
	 *
	 * @return the style header title bold back blue ten
	 */
	public HSSFCellStyle getStyleContentDate() {
	    HSSFCellStyle cellStyle = csDate;
	    cellStyle.setDataFormat((short)14);
	    cellStyle.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
	    cellStyle.cloneStyleFrom(setBorder(0,1,0,1));
	    
	    return cellStyle;
	}
	
	/**
	 * Style Total Account Label.
	 *
	 * @return the style total account
	 */
	public HSSFCellStyle getStyleTotalAccount() {
		HSSFCellStyle cellStyle = cSTotalAccount;
		 
	    final Font cellFont = cFTotalAccount;
	    cellFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    cellStyle.cloneStyleFrom(setBorder(2,2,2,2));
	    cellStyle.setFont(cellFont);
	    return cellStyle;
	}
	
	/**
	 * Style Total Account Amount.
	 *
	 * @return the style total account amount quantity
	 */
	public HSSFCellStyle getStyleTotalAccountAmountQuantity() {
		HSSFCellStyle cellStyle = cSTotalAccountAmountQuantity;
		DataFormat format = dFTotalAccountAmountQuantity;
	    final Font cellFont = cFTotalAccountAmountQuantity;
	    cellFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    cellStyle.cloneStyleFrom(setBorder(2,2,2,2));
	    cellStyle.setDataFormat(format.getFormat("#,##0"));
	    cellStyle.setFont(cellFont);
	    return cellStyle;
	}
	
	/**
	 * Get Style Amount Debit.
	 *
	 * @return the style amount debit quantity
	 */
	public HSSFCellStyle getStyleAmountDebitQuantity() {
		HSSFCellStyle cellStyle = cSAmountDebitQuantity;
		DataFormat format = dFAmountDebitQuantity;
	    final Font cellFont = cFAmountDebitQuantity;
	    cellFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    cellStyle.setFont(cellFont);
	    cellStyle.cloneStyleFrom(setBorder(0,1,0,1));
	    cellStyle.setDataFormat(format.getFormat("#,##0"));
	    return cellStyle;
	}
	
	/**
	 * Get Style Amount Assets.
	 *
	 * @return the style amount assets quantity
	 */
	public HSSFCellStyle getStyleAmountAssetsQuantity() {
		
		HSSFCellStyle cellStyle = cSTotalAmountAssetsQuantity;
		DataFormat format = dFAmountAssetsQuantity;
	    final Font cellFont = cFAmountAssetsQuantity;
	    cellFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    cellStyle.setFont(cellFont);
	    cellStyle.cloneStyleFrom(setBorder(0,1,0,1));
	    cellStyle.setDataFormat(format.getFormat("#,##0"));
	    return cellStyle;
	}
	
	/**
	 * Style Total Account Amount.
	 *
	 * @return the style total account amount
	 */
	public HSSFCellStyle getStyleTotalAccountAmount() {
		HSSFCellStyle cellStyle = cSTotalAccountAmount;
		DataFormat format = dFTotalAccountAmount;
	    final Font cellFont = cFTotalAccountAmount;
	    cellFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    cellStyle.cloneStyleFrom(setBorder(2,2,2,2));
	    cellStyle.setDataFormat(format.getFormat("#,##0.0000"));
	    cellStyle.setFont(cellFont);
	    return cellStyle;
	}
	
	/**
	 * Get Style Amount Debit.
	 *
	 * @return the style amount debit
	 */
	public HSSFCellStyle getStyleAmountDebit() {
		HSSFCellStyle cellStyle = cSAmountDebit;
		DataFormat format = dFAmountDebit;
	    final Font cellFont = cFAmountDebit;
	    cellFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    cellStyle.setFont(cellFont);
	    cellStyle.cloneStyleFrom(setBorder(0,1,0,1));
	    cellStyle.setDataFormat(format.getFormat("#,##0.0000"));
	    return cellStyle;
	}
	
	/**
	 * Get Style Amount Assets.
	 *
	 * @return the style amount assets
	 */
	public HSSFCellStyle getStyleAmountAssets() {
		
		HSSFCellStyle cellStyle = cSAmountAssets;
		DataFormat format = dFAmountAssets;
	    final Font cellFont = cFAmountAssets;
	    cellFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    cellStyle.setFont(cellFont);
	    cellStyle.cloneStyleFrom(setBorder(0,1,0,1));
	    cellStyle.setDataFormat(format.getFormat("#,##0.0000"));
	    return cellStyle;
	}
	
	/**
	 * Get Style Accounting Account.
	 *
	 * @return the style accounting account
	 */
	public HSSFCellStyle getStyleAccountingAccount() {
		
		HSSFCellStyle cellStyle = cSAccountingAccount;
	    final Font cellFont = cFAccountingAccount;
	    cellStyle.setFont(cellFont);
	    cellStyle.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
	    cellStyle.cloneStyleFrom(setBorder(0,1,0,1));
	    return cellStyle;
	}
	
	/**
	 * Get Style Accounting Account.
	 *
	 * @return the style accounting account
	 */
	public XSSFCellStyle getStyleAccountingAccountX() {
		
		XSSFWorkbook wb = new XSSFWorkbook();
		
		cSAccountingAccountX = wb.createCellStyle();
		
		XSSFCellStyle cellStyle = cSAccountingAccountX;
	    final Font cellFont = cFAccountingAccount;
	    cellStyle.setFont(cellFont);
	    cellStyle.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
	    cellStyle.cloneStyleFrom(setBorderX(0,1,0,1));
	    return cellStyle;
	}
	
	public HSSFCellStyle getStyleAccountingAccountLeft() {
		
		HSSFCellStyle cellStyle = cSAccountingAccount;
	    final Font cellFont = cFAccountingAccount;
	    cellStyle.setFont(cellFont);
	    cellStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
	    cellStyle.cloneStyleFrom(setBorder(0,1,0,1));
	    return cellStyle;
	}

	public HSSFCellStyle getStyleAccountingAccountOnlyNumberIssue1179() {
		
		HSSFCellStyle cellStyle = cSAccountingAccountOnlyNumber1179;
	    final Font cellFont = cFAccountingAccount;
	    cellStyle.setFont(cellFont);
	    cellStyle.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
	    cellStyle.cloneStyleFrom(setBorder(0,1,0,1));
	    cellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0"));
	    return cellStyle;
	}
	
	public HSSFCellStyle getStyleAccountingAccountDateIssue1179() {
		
		HSSFCellStyle cellStyle = cSAccountingAccountDate1179;
	    final Font cellFont = cFAccountingAccount;
	    cellStyle.setFont(cellFont);
	    cellStyle.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
	    cellStyle.cloneStyleFrom(setBorder(0,1,0,1));
	    cellStyle.setDataFormat((short) HSSFDataFormat.getNumberOfBuiltinBuiltinFormats());
	    return cellStyle;
	}
	
	/**
	 * estilo para celdas con montos monetarios issue 1091
	 * @return
	 */
	public HSSFCellStyle getStyleAccountingAccountAmountIssue1091() {
		
		HSSFCellStyle cellStyle = cSAccountingAccountAmountI1091;
	    final Font cellFont = cFAccountingAccount;
	    cellStyle.setFont(cellFont);
	    cellStyle.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
	    cellStyle.cloneStyleFrom(setBorder(0,1,0,1));
	    cellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0.00"));
	    return cellStyle;
	}

	public HSSFCellStyle getStyleAccountingAccountAmountBoldIssue1091() {
		
		HSSFCellStyle cellStyle = cSAccountingAccountAmountBoldI1091;
	    final Font cellFont = cFAccountingAccountAmountBoldI1091;
	    cellFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    cellStyle.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
	    cellStyle.cloneStyleFrom(setBorder(2,2,2,2));
	    cellStyle.setFont(cellFont);
	    cellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0.00"));
	    return cellStyle;
	}
	
	/**
	 * estilo para celdas con porcentaje issue 1091
	 * @return
	 */
	public HSSFCellStyle getStyleAccountingAccountPercentIssue1091() {
		
		HSSFCellStyle cellStyle = cSAccountingAccountPercentI1091;
	    final Font cellFont = cFAccountingAccount;
	    cellStyle.setFont(cellFont);
	    cellStyle.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
	    cellStyle.cloneStyleFrom(setBorder(0,1,0,1));
	    cellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.00%"));
	    return cellStyle;
	}
	
	/**
	 * Get Style Accounting Account.
	 *
	 * @return the style accounting account father
	 */
	public HSSFCellStyle getStyleAccountingAccountFather() {
		
		HSSFCellStyle cellStyle = cSAccountingAccountFather;
	    final Font cellFont = cFAccountingAccountFather;
	    cellStyle.setFont(cellFont);
	    cellStyle.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
	    cellFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    cellStyle.cloneStyleFrom(setBorder(0,1,0,1));
	    return cellStyle;
	}
	
	/**
	 * *
	 * Set Border to Cell .
	 *
	 * @param top the top
	 * @param right the right
	 * @param bottom the bottom
	 * @param left the left
	 * @return the HSSF cell style
	 */
	public HSSFCellStyle setBorder(Integer top,Integer right,Integer bottom,Integer left){
		HSSFCellStyle cellStyle = csBorder;
		cellStyle.setBorderTop(top.shortValue());
		cellStyle.setBorderRight(right.shortValue());
		cellStyle.setBorderBottom(bottom.shortValue());
		cellStyle.setBorderLeft(left.shortValue());
		return cellStyle;
	}
	
	public XSSFCellStyle setBorderX(Integer top,Integer right,Integer bottom,Integer left){
		
		XSSFWorkbook wb = new XSSFWorkbook();
		
		csBorderX = wb.createCellStyle();
		
		XSSFCellStyle cellStyle = csBorderX;
		cellStyle.setBorderTop(top.shortValue());
		cellStyle.setBorderRight(right.shortValue());
		cellStyle.setBorderBottom(bottom.shortValue());
		cellStyle.setBorderLeft(left.shortValue());
		return cellStyle;
	}
	
	/**
	 * Get Style Description.
	 *
	 * @return the style description accounting account
	 */
	public HSSFCellStyle getStyleDescriptionAccountingAccount() {
		
		HSSFCellStyle cellStyle = csDescriptionAccountingAccount;
	    final Font cellFont = cFDescriptionAccountingAccount;
	    cellStyle.setFont(cellFont);
	    cellStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
	    return cellStyle;
	}
	
	/**
	 * *
	 * Add Formula and Style.
	 *
	 * @param celda the celda
	 * @param style the style
	 * @param formula the formula
	 */
    private void addFormulaAndStyle(HSSFCell celda, HSSFCellStyle style, String formula) {
        celda.setCellFormula(formula);
        celda.setCellStyle(style);
    }
	
	/**
	 * addContentAndStyle.
	 *
	 * @param celda the celda
	 * @param style the style
	 * @param value the value
	 */
	private void addContentAndStyle(HSSFCell celda, HSSFCellStyle style,Double value) {
	    celda.setCellValue(value);
	    celda.setCellStyle(style);
	}
	
	/**
	 * addContentAndStyle.
	 *
	 * @param celda the celda
	 * @param style the style
	 * @param value the value
	 */
	private void addContentAndStyle(HSSFCell celda, HSSFCellStyle style,Date value) {
	    celda.setCellValue(value);
	    celda.setCellStyle(style);
	}
	
	/**
	 * addContentAndStyle.
	 *
	 * @param celda the celda
	 * @param style the style
	 * @param value the value
	 */
	private void addContentAndStyle(HSSFCell celda, HSSFCellStyle style,String value) {
	    celda.setCellValue(value);
	    celda.setCellStyle(style);
	}
	
	/**
	 * addContentAndStyle.
	 *
	 * @param cell the celda
	 * @param style the style
	 * @param value the value
	 */
	private void addContentAndStyle2(Cell  cell, XSSFCellStyle style,String value) {
	    cell.setCellValue(value);
	    cell.setCellStyle(style);
	}
	
	private void addContentAndStyle2D(Cell celda, XSSFCellStyle style,Double value) {
	    celda.setCellValue(value);
	    celda.setCellStyle(style);
	}
	
	/**
	 * addContentAndStyle.
	 *
	 * @param celda the celda
	 * @param style the style
	 * @param value the value
	 */
	private void addContentAndStyle(HSSFCell celda, HSSFCellStyle style,Integer value) {
	    celda.setCellValue(value);
	    celda.setCellStyle(style);
	}
	
	/**
	 * Adds the formula and style.
	 *
	 * @param celda the celda
	 * @param formula the formula
	 */
	private void addFormulaAndStyle(HSSFCell celda, String formula) {
        celda.setCellFormula(formula);
    }
	
	/**
	 * addContentAndStyle.
	 *
	 * @param celda the celda
	 * @param value the value
	 */
	private void addContentAndStyle(HSSFCell celda, String value) {
	    celda.setCellValue(value);
	}
	
	/**
	 * addContentAndStyle.
	 *
	 * @param celda the celda
	 * @param value the value
	 */
	private void addContentAndStyle(HSSFCell celda, Integer value) {
	    celda.setCellValue(value);
	}
	
	/**
	 * addContentAndStyle.
	 *
	 * @param celda the celda
	 * @param value the value
	 */
	private void addContentAndStyle(HSSFCell celda, Date value) {
	    celda.setCellValue(value);
	}
	
	/**
	 * *
	 * addContentAndStyle.
	 *
	 * @param celda the celda
	 * @param value the value
	 */
	private void addContentAndStyle(HSSFCell celda, Double value) {
	    celda.setCellValue(value);
	}
	
	/**
	 * adjust Columns of Content.
	 *
	 * @param sheet Sheet
	 * @param columnToCompare Row to more Number Colums
	 */
	private void adjustColumns(HSSFSheet sheet, int columnToCompare ) {
//		short numberColumns = sheet.getRow(columnToCompare).getLastCellNum();
		for (int i = 0; i <= columnToCompare; i++) {
			sheet.autoSizeColumn(i);
		}
	}

	/**
	 * Merge cell.
	 *
	 * @param sheet the sheet
	 * @param firstRow the first row
	 * @param lastRow the last row
	 * @param firstCol the first col
	 * @param lastCol the last col
	 */
	public void mergeCell(HSSFSheet sheet,int firstRow, int lastRow, int firstCol, int lastCol ){
		
		sheet.addMergedRegion(new CellRangeAddress(firstRow, lastRow, firstCol, lastCol));
		
	}

	/**
	 * Report 85 Accounting Balance.
	 *
	 * @param listAccountingAccount the list accounting account
	 * @param reportLogger the report logger
	 * @param accountingProcessTo the accounting process to
	 * @return the byte[]
	 */
	public byte[] writeFileExcelAccountingBalance(List<AccountingAccount> listAccountingAccount,ReportLogger reportLogger,AccountingProcessTo accountingProcessTo){
		byte[] arrayByte=null;
		String strStartHour = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),ReportConstant.DATE_FORMAT_TIME_SECOND);
		String titleReport=null;
		AccountingProcess accountingProcess=accountingReportQueryServiceBean.findAccountingProcessToFilter(accountingProcessTo);
		AccountingParameterTO accountingParameterTO=new AccountingParameterTO();
		accountingParameterTO.setIdAccountingParameterPk(accountingProcess.getProcessType());
		accountingParameterTO.setParameterType(AccountingParameterType.TIPO_DIARIO_AUXILIAR.getCode());
		accountingParameterTO.setStatus(AccountingParameterStateType.ACTIVED.getCode());
		List<AccountingParameter>	listTypeAccount=accountingReportQueryServiceBean.findListParameterAccountingByFilter(accountingParameterTO);
		 if(Validations.validateListIsNotNullAndNotEmpty(listTypeAccount)){
			 titleReport=reportLogger.getReport().getTitle().concat(GeneralConstants.DASH).concat(listTypeAccount.get(0).getDescription());
		 }else{
			 titleReport=reportLogger.getReport().getTitle();
		 }
		 
		HSSFCellStyle stleAmountDebit=null;
		HSSFCellStyle stleAmountAssets=null;

		initCellStyle();
		initFont();
		initDataFormat();
		
		stleAmountDebit=getStyleAmountDebitQuantity();
		stleAmountAssets=getStyleAmountAssetsQuantity();
		try
		    {
			/**Created a new sheet within the workBock**/
	    	
	    	HSSFSheet sheet = hSSFWorkbook.createSheet("BookEntry");
	    	
	        /*****
	         * BEGIN - REPORT HEADER 
	         */
	        /**DATE*/
	        HSSFRow row1= sheet.createRow((short)1);
	        addContentAndStyle(row1.createCell(4),getStyleTitleBold(),titleReport);
	        addContentAndStyle(row1.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_FECHA);
	        addContentAndStyle(row1.createCell(8),CommonsUtilities.convertDatetoString(reportLogger.getRegistryDate()));
	        
	        /**HOUR*/
	        HSSFRow row2= sheet.createRow((short)2);
	        addContentAndStyle(row2.createCell(7),getStyleTitleBold(), ReportConstant.LABEL_HORA);
	        addContentAndStyle(row2.createCell(8), strStartHour);
	        
	        /**CLASIFICATION*/
	        HSSFRow row3= sheet.createRow((short)3);
	        addContentAndStyle(row3.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_CLASIFICACION);
	        addContentAndStyle(row3.createCell(8),reportLogger.getReport().getReportClasification());
	
	        
	        /**Usuario**/
	        HSSFRow row5= sheet.createRow((short)5);
	        addContentAndStyle(row5.createCell(0),getStyleTitleBold(),ReportConstant.LABEL_USUARIO);
	        addContentAndStyle(row5.createCell(1),reportLogger.getRegistryUser());
	        
	        /****
	         * END - REPORT HEADER 
	         */
	        HSSFRow row8= sheet.createRow((short)8);
	        addContentAndStyle(row8.createCell(4),ReportConstant.LABEL_FECHA_PROCESO+CommonsUtilities.convertDatetoString(accountingProcess.getExecutionDate()));
		       
	
	        int indrow=12;
	
	
	    	

    		
    		/**Created a new row within the sheet**/
	        indrow++;
	        
	       /**Created Header DEBE - HABER*/
	        HSSFRow row11 = sheet.createRow((int)indrow);
	        addContentAndStyle(row11.createCell(2),getStyleTitle(),ReportConstant.LABEL_CUENTA);
	        addContentAndStyle(row11.createCell(3),getStyleTitle(),GeneralConstants.EMPTY_STRING);
	        addContentAndStyle(row11.createCell(4),getStyleTitle(),ReportConstant.LABEL_DESCRIPCION);
	        addContentAndStyle(row11.createCell(5),getStyleTitle(),ReportConstant.LABEL_CANTIDAD);
	        addContentAndStyle(row11.createCell(6),getStyleTitle(),ReportConstant.LABEL_MONEDA_ORIGEN);
	        addContentAndStyle(row11.createCell(7),getStyleTitle(),ReportConstant.LABEL_DOLARES);
	        addContentAndStyle(row11.createCell(8),getStyleTitle(),ReportConstant.LABEL_BOLIVIANOS);
	        indrow++;
	        
	        /****
	         * LIST ACCOUNTING
	         */
	        int i=indrow;
	        BigDecimal sumDebit=new BigDecimal(0);
        	BigDecimal sumAssets=new BigDecimal(0);
	        for (AccountingAccount accountingAccountAsset : listAccountingAccount) {
	        	
	        	HSSFRow rowAccount = sheet.createRow((int)i);
	        	
	        	/**Account Code**/
	        	if(accountingAccountAsset.getIndAccumulation().equals(GeneralConstants.ONE_VALUE_INTEGER)&&
	        			Validations.validateIsNull(accountingAccountAsset.getAccountingAccount())){
	        		/**Account father max**/
	        		addContentAndStyle(rowAccount.createCell(2),getStyleAccountingAccountFather(),accountingAccountAsset.getAccountCode());
	        		addContentAndStyle(rowAccount.createCell(3),getStyleAccountingAccount(),GeneralConstants.EMPTY_STRING);
	        		
	        		sumDebit=CommonsUtilities.addTwoDecimal(accountingAccountAsset.getAmountDebit(), sumDebit, 2);
	        		sumAssets=CommonsUtilities.addTwoDecimal(accountingAccountAsset.getAmountAssets(), sumAssets, 2);
	        	}
	        	else{
	        		/**Other Account no father**/
	        		addContentAndStyle(rowAccount.createCell(2),getStyleAccountingAccount(),GeneralConstants.EMPTY_STRING);
	        		addContentAndStyle(rowAccount.createCell(3),getStyleAccountingAccount(),accountingAccountAsset.getAccountCode());
	        	}
	        	
	        	/**Account Description**/
	        	addContentAndStyle(rowAccount.createCell(4),getStyleDescriptionAccountingAccount(),accountingAccountAsset.getDescription());
	        	

	        	if(Validations.validateIsNotNull(accountingAccountAsset.getQuantity())){
	        		Double quantity=accountingAccountAsset.getQuantity().doubleValue();
	        		addContentAndStyle(rowAccount.createCell(5),stleAmountDebit,quantity);
	        	}
        		/**Account Amount Origin Currency**/
	        	if(Validations.validateIsNotNull(accountingAccountAsset.getAmount())){
	        		Double valueAmountOrigin=accountingAccountAsset.getAmount().doubleValue();
	        		addContentAndStyle(rowAccount.createCell(6),stleAmountDebit,valueAmountOrigin);
	        	}
	        	else{
	        		addContentAndStyle(rowAccount.createCell(6),stleAmountDebit,0);
	        	}
	        	/**Amount To USD Currency**/
	        	if(Validations.validateIsNotNull(accountingAccountAsset.getAmountUSD())){
	        		Double valueAmountUSD=accountingAccountAsset.getAmountUSD().doubleValue();
	        		addContentAndStyle(rowAccount.createCell(7),stleAmountAssets,valueAmountUSD);
	        	}else{
	        		addContentAndStyle(rowAccount.createCell(7),stleAmountAssets,0);
	        	}
	        	
	        	/**Amount To BOB Currency**/
	        	if(Validations.validateIsNotNull(accountingAccountAsset.getAmountBOB())){
	        		Double valueAmountBOB=accountingAccountAsset.getAmountBOB().doubleValue();
	        		addContentAndStyle(rowAccount.createCell(8),stleAmountAssets,valueAmountBOB);
	        	}else{
	        		addContentAndStyle(rowAccount.createCell(8),stleAmountAssets,0);
	        	}

	        	
	        	i++;
			
			}

	        i++;
	        indrow=i;
	        indrow++;


        	indrow++;
    	
	    	
	    	//Creamos una celda de tipo fecha y la mostramos
	        //indicando un patron de formato
	        HSSFCellStyle cellStyle = hSSFWorkbook.createCellStyle();
	        cellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat(ReportConstant.LABEL_DATE_FORMAT));
	
	
	        /**adjustColumns**/
	        adjustColumns(sheet,1);
	        //Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_BALANCE);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	         
	       	 hSSFWorkbook.write(bos);
	        
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        
	        hSSFWorkbook.write(fileOut);
	        
	        fileOut.close();
	    }
	    catch(IOException e)
	    {
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
    return arrayByte;
	}
	
	/**
	 * Report 206 Accounting Balance.
	 *
	 * @param listAccountingAccount the list accounting account
	 * @param reportLogger the report logger
	 * @param accountingProcessTo the accounting process to
	 * @return the byte[]
	 */
	public byte[] writeFileExcelAccountingBalanceHistory(List<AccountingAccount> listAccountingAccount,ReportLogger reportLogger,AccountingProcessTo accountingProcessTo){
		byte[] arrayByte=null;
		String strStartHour = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),ReportConstant.DATE_FORMAT_TIME_SECOND);
		String titleReport=null;
		AccountingProcess accountingProcess=accountingReportQueryServiceBean.findAccountingProcessToFilter(accountingProcessTo);
		AccountingParameterTO accountingParameterTO=new AccountingParameterTO();
		accountingParameterTO.setIdAccountingParameterPk(accountingProcess.getProcessType());
		accountingParameterTO.setParameterType(AccountingParameterType.TIPO_DIARIO_AUXILIAR.getCode());
		accountingParameterTO.setStatus(AccountingParameterStateType.ACTIVED.getCode());
		List<AccountingParameter>	listTypeAccount=accountingReportQueryServiceBean.findListParameterAccountingByFilter(accountingParameterTO);
		if(Validations.validateListIsNotNullAndNotEmpty(listTypeAccount)){
			 titleReport=reportLogger.getReport().getTitle().concat(GeneralConstants.DASH).concat(listTypeAccount.get(0).getDescription());
		}else{
			 titleReport=reportLogger.getReport().getTitle();
		}
		 
		HSSFCellStyle stleAmountDebit=null;
		HSSFCellStyle stleAmountAssets=null;

		initCellStyle();
		initFont();
		initDataFormat();
		
		stleAmountDebit=getStyleAmountDebitQuantity();
		stleAmountAssets=getStyleAmountAssetsQuantity();
		try
		    {
			/**Created a new sheet within the workBock**/
	    	
	    	HSSFSheet sheet = hSSFWorkbook.createSheet("BookEntry");
	        /*****
	         * BEGIN - REPORT HEADER 
	         */
	        /**DATE*/
	        HSSFRow row1= sheet.createRow((short)1);
	        addContentAndStyle(row1.createCell(4),getStyleTitleBold(),titleReport);
	        addContentAndStyle(row1.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_FECHA);
	        addContentAndStyle(row1.createCell(8),CommonsUtilities.convertDatetoString(reportLogger.getRegistryDate()));
	        
	        /**HOUR*/
	        HSSFRow row2= sheet.createRow((short)2);
	        addContentAndStyle(row2.createCell(7),getStyleTitleBold(), ReportConstant.LABEL_HORA);
	        addContentAndStyle(row2.createCell(8), strStartHour);
	        
	        /**CLASIFICATION*/
	        HSSFRow row3= sheet.createRow((short)3);
	        addContentAndStyle(row3.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_CLASIFICACION);
	        addContentAndStyle(row3.createCell(8),reportLogger.getReport().getReportClasification());
	
	        
	        /**Usuario**/
	        HSSFRow row5= sheet.createRow((short)5);
	        addContentAndStyle(row5.createCell(0),getStyleTitleBold(),ReportConstant.LABEL_USUARIO);
	        addContentAndStyle(row5.createCell(1),reportLogger.getRegistryUser());
	        
	        /****
	         * END - REPORT HEADER 
	         */
	        HSSFRow row8= sheet.createRow((short)8);
	        addContentAndStyle(row8.createCell(4),ReportConstant.LABEL_FECHA_PROCESO+CommonsUtilities.convertDatetoString(accountingProcess.getExecutionDate()));
		       
	
	        int indrow=12;
	
	
	    	

    		
    		/**Created a new row within the sheet**/
	        indrow++;
	        
	       /**Created Header DEBE - HABER*/
	        HSSFRow row11 = sheet.createRow((int)indrow);
	        addContentAndStyle(row11.createCell(2),getStyleTitle(),ReportConstant.LABEL_CUENTA);
	        addContentAndStyle(row11.createCell(3),getStyleTitle(),GeneralConstants.EMPTY_STRING);
	        addContentAndStyle(row11.createCell(4),getStyleTitle(),ReportConstant.LABEL_DESCRIPCION);
	        addContentAndStyle(row11.createCell(5),getStyleTitle(),ReportConstant.LABEL_CANTIDAD);
	        addContentAndStyle(row11.createCell(6),getStyleTitle(),ReportConstant.LABEL_MONEDA_ORIGEN);
	        addContentAndStyle(row11.createCell(7),getStyleTitle(),ReportConstant.LABEL_DOLARES);
	        addContentAndStyle(row11.createCell(8),getStyleTitle(),ReportConstant.LABEL_BOLIVIANOS);
	        indrow++;
	        
	        /****
	         * LIST ACCOUNTING
	         */
	        int i=indrow;
	        BigDecimal sumDebit=new BigDecimal(0);
        	BigDecimal sumAssets=new BigDecimal(0);
	        for (AccountingAccount accountingAccountAsset : listAccountingAccount) {
	        	
	        	HSSFRow rowAccount = sheet.createRow((int)i);
	        	
	        	/**Account Code**/
	        	if(accountingAccountAsset.getIndAccumulation().equals(GeneralConstants.ONE_VALUE_INTEGER)&&
	        			Validations.validateIsNull(accountingAccountAsset.getAccountingAccount())){
	        		/**Account father max**/
	        		addContentAndStyle(rowAccount.createCell(2),getStyleAccountingAccountFather(),accountingAccountAsset.getAccountCode());
	        		addContentAndStyle(rowAccount.createCell(3),getStyleAccountingAccount(),GeneralConstants.EMPTY_STRING);
	        		
	        		sumDebit=CommonsUtilities.addTwoDecimal(accountingAccountAsset.getAmountDebit(), sumDebit, 2);
	        		sumAssets=CommonsUtilities.addTwoDecimal(accountingAccountAsset.getAmountAssets(), sumAssets, 2);
	        	}
	        	else{
	        		/**Other Account no father**/
	        		addContentAndStyle(rowAccount.createCell(2),getStyleAccountingAccount(),GeneralConstants.EMPTY_STRING);
	        		addContentAndStyle(rowAccount.createCell(3),getStyleAccountingAccount(),accountingAccountAsset.getAccountCode());
	        	}
	        	
	        	/**Account Description**/
	        	addContentAndStyle(rowAccount.createCell(4),getStyleDescriptionAccountingAccount(),accountingAccountAsset.getDescription());
	        	

	        	if(Validations.validateIsNotNull(accountingAccountAsset.getQuantity())){
	        		Double quantity=accountingAccountAsset.getQuantity().doubleValue();
	        		addContentAndStyle(rowAccount.createCell(5),stleAmountDebit,quantity);
	        	}
        		/**Account Amount Origin Currency**/
	        	if(Validations.validateIsNotNull(accountingAccountAsset.getAmount())){
	        		Double valueAmountOrigin=accountingAccountAsset.getAmount().doubleValue();
	        		addContentAndStyle(rowAccount.createCell(6),stleAmountDebit,valueAmountOrigin);
	        	}
	        	else{
	        		addContentAndStyle(rowAccount.createCell(6),stleAmountDebit,0);
	        	}
	        	/**Amount To USD Currency**/
	        	if(Validations.validateIsNotNull(accountingAccountAsset.getAmountUSD())){
	        		Double valueAmountUSD=accountingAccountAsset.getAmountUSD().doubleValue();
	        		addContentAndStyle(rowAccount.createCell(7),stleAmountAssets,valueAmountUSD);
	        	}else{
	        		addContentAndStyle(rowAccount.createCell(7),stleAmountAssets,0);
	        	}
	        	
	        	/**Amount To BOB Currency**/
	        	if(Validations.validateIsNotNull(accountingAccountAsset.getAmountBOB())){
	        		Double valueAmountBOB=accountingAccountAsset.getAmountBOB().doubleValue();
	        		addContentAndStyle(rowAccount.createCell(8),stleAmountAssets,valueAmountBOB);
	        	}else{
	        		addContentAndStyle(rowAccount.createCell(8),stleAmountAssets,0);
	        	}

	        	
	        	i++;
			
			}

	        i++;
	        indrow=i;
	        indrow++;


        	indrow++;
    	
	    	
	    	//Creamos una celda de tipo fecha y la mostramos
	        //indicando un patron de formato
	        HSSFCellStyle cellStyle = hSSFWorkbook.createCellStyle();
	        cellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat(ReportConstant.LABEL_DATE_FORMAT));
	
	
	        /**adjustColumns**/
	        adjustColumns(sheet,1);
	        //Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_BALANCE_HISTORY);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	         
	       	 hSSFWorkbook.write(bos);
	        
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        
	        hSSFWorkbook.write(fileOut);
	        
	        fileOut.close();
	    }
	    catch(IOException e)
	    {
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
    return arrayByte;
	}
	
	/**
	 * Write file excel accounting balance t.
	 *
	 * @param listAccountingAccount the list accounting account
	 * @param reportLogger the report logger
	 * @param accountingProcessTo the accounting process to
	 * @return the byte[]
	 */
	public byte[] writeFileExcelAccountingBalanceT(List<AccountingAccount> listAccountingAccount,ReportLogger reportLogger,
													AccountingProcessTo accountingProcessTo){
		byte[] arrayByte=null;
		String strStartHour = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),ReportConstant.DATE_FORMAT_TIME_SECOND);
		String titleReport=null;

		AccountingParameterTO accountingParameterTO=new AccountingParameterTO();
		accountingParameterTO.setIdAccountingParameterPk(accountingProcessTo.getProcessType());
		accountingParameterTO.setParameterType(AccountingParameterType.TIPO_DIARIO_AUXILIAR.getCode());
		accountingParameterTO.setStatus(AccountingParameterStateType.ACTIVED.getCode());
		List<AccountingParameter>	listTypeAccount=accountingReportQueryServiceBean.findListParameterAccountingByFilter(accountingParameterTO);
		 if(Validations.validateListIsNotNullAndNotEmpty(listTypeAccount)){
			 titleReport=reportLogger.getReport().getTitle().concat(GeneralConstants.DASH).concat(listTypeAccount.get(0).getDescription());
		 }else{
			 titleReport=reportLogger.getReport().getTitle();
		 }
		 
		HSSFCellStyle stleAmountDebit=null;
		HSSFCellStyle stleAmountAssets=null;

		initCellStyle();
		initFont();
		initDataFormat();
		
		stleAmountDebit=getStyleAmountDebitQuantity();
		stleAmountAssets=getStyleAmountAssetsQuantity();
		try
		    {
			/**Created a new sheet within the workBock**/
	    	
	    	HSSFSheet sheet = hSSFWorkbook.createSheet("BookEntry");
	    	
	        /*****
	         * BEGIN - REPORT HEADER 
	         */
	        /**DATE*/
	        HSSFRow row1= sheet.createRow((short)1);
	        addContentAndStyle(row1.createCell(4),getStyleTitleBold(),titleReport);
	        addContentAndStyle(row1.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_FECHA);
	        addContentAndStyle(row1.createCell(8),CommonsUtilities.convertDatetoString(reportLogger.getRegistryDate()));
	        
	        /**HOUR*/
	        HSSFRow row2= sheet.createRow((short)2);
	        addContentAndStyle(row2.createCell(7),getStyleTitleBold(), ReportConstant.LABEL_HORA);
	        addContentAndStyle(row2.createCell(8), strStartHour);
	        
	        /**CLASIFICATION*/
	        HSSFRow row3= sheet.createRow((short)3);
	        addContentAndStyle(row3.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_CLASIFICACION);
	        addContentAndStyle(row3.createCell(8),reportLogger.getReport().getReportClasification());
	
	        
	        /**Usuario**/
	        HSSFRow row5= sheet.createRow((short)5);
	        addContentAndStyle(row5.createCell(0),getStyleTitleBold(),ReportConstant.LABEL_USUARIO);
	        addContentAndStyle(row5.createCell(1),reportLogger.getRegistryUser());
	        
	        /****
	         * END - REPORT HEADER 
	         */
	        HSSFRow row8= sheet.createRow((short)8);
	        addContentAndStyle(row8.createCell(4),ReportConstant.LABEL_PERIOD+CommonsUtilities.convertDatetoString(accountingProcessTo.getInitialDate())+" al "+CommonsUtilities.convertDatetoString(accountingProcessTo.getFinalDate()));
		       
	
	        int indrow=12;
	
	
	    	

    		
    		/**Created a new row within the sheet**/
//	        HSSFRow row10 = sheet.createRow((int)indrow);
//	        addContentAndStyle(row10.createCell(4),"FECHA: "+CommonsUtilities.convertDatetoString(accReceipt.getReceiptDate()));  
//	        addContentAndStyle(row10.createCell(5),"RECIBO");
//	        addContentAndStyle(row10.createCell(6),accReceipt.getNumberReceipt());
	        indrow++;
	        
	       /**Created Header DEBE - HABER*/
	        HSSFRow row11 = sheet.createRow((int)indrow);
	        addContentAndStyle(row11.createCell(2),getStyleTitle(),ReportConstant.LABEL_CUENTA);
	        addContentAndStyle(row11.createCell(3),getStyleTitle(),GeneralConstants.EMPTY_STRING);
	        addContentAndStyle(row11.createCell(4),getStyleTitle(),ReportConstant.LABEL_DESCRIPCION);
	        addContentAndStyle(row11.createCell(5),getStyleTitle(),ReportConstant.LABEL_DEBE);
	        addContentAndStyle(row11.createCell(6),getStyleTitle(),ReportConstant.LABEL_HABER);
	        addContentAndStyle(row11.createCell(7),getStyleTitle(),ReportConstant.LABEL_SALDO);
	        indrow++;
	        
	        /****
	         * LIST ACCOUNTING
	         */
	        int i=indrow;
	        BigDecimal sumDebit=new BigDecimal(0);
        	BigDecimal sumAssets=new BigDecimal(0);
	        for (AccountingAccount accountingAccountAsset : listAccountingAccount) {
	        	
	        	HSSFRow rowAccount = sheet.createRow((int)i);
	        	
	        	/**Account Code**/
	        	if(accountingAccountAsset.getIndAccumulation().equals(GeneralConstants.ONE_VALUE_INTEGER)&&
	        			Validations.validateIsNull(accountingAccountAsset.getAccountingAccount())){
	        		/**Account father max**/
	        		addContentAndStyle(rowAccount.createCell(2),getStyleAccountingAccountFather(),accountingAccountAsset.getAccountCode());
	        		addContentAndStyle(rowAccount.createCell(3),getStyleAccountingAccount(),GeneralConstants.EMPTY_STRING);
	        		
	        		sumDebit=CommonsUtilities.addTwoDecimal(accountingAccountAsset.getAmountDebit(), sumDebit, 2);
	        		sumAssets=CommonsUtilities.addTwoDecimal(accountingAccountAsset.getAmountAssets(), sumAssets, 2);
	        	}
	        	else{
	        		/**Other Account no father**/
	        		addContentAndStyle(rowAccount.createCell(2),getStyleAccountingAccount(),GeneralConstants.EMPTY_STRING);
	        		addContentAndStyle(rowAccount.createCell(3),getStyleAccountingAccount(),accountingAccountAsset.getAccountCode());
	        	}
	        	
	        	/**Account Description**/
	        	addContentAndStyle(rowAccount.createCell(4),getStyleDescriptionAccountingAccount(),accountingAccountAsset.getDescription());
	        	

        		/**Account Amount debit**/
	        	if(Validations.validateIsNotNull(accountingAccountAsset.getAmountDebit())){
	        		Double valueDebit=accountingAccountAsset.getAmountDebit().doubleValue();
	        		addContentAndStyle(rowAccount.createCell(5),stleAmountDebit,valueDebit);
	        	}
	        	else{
	        		addContentAndStyle(rowAccount.createCell(5),stleAmountDebit,0);
	        	}
	        	/**Amount Assets**/
	        	if(Validations.validateIsNotNull(accountingAccountAsset.getAmountAssets())){
	        		Double valueAsset=accountingAccountAsset.getAmountAssets().doubleValue();
	        		addContentAndStyle(rowAccount.createCell(6),stleAmountAssets,valueAsset);
	        	}else{
	        		addContentAndStyle(rowAccount.createCell(6),stleAmountAssets,0);
	        	}
	        	
	        	addFormulaAndStyle(rowAccount.createCell(7),stleAmountAssets,funtionSubBalance(i+1));

	        	
	        	i++;
			
			}
	        
	      
	        
	        /**End of each line seat**/
	        HSSFRow rowAccount = sheet.createRow((int)i);
	        
	        for (int j = 2; j < sheet.getRow(i-1).getLastCellNum(); j++) {
	        	addContentAndStyle(rowAccount.createCell(j),setBorder(1,0,0,0),"");
			}
	        Double totalDebit=sumDebit.doubleValue();
	        Double totalAssets=sumAssets.doubleValue();
	        
	        addContentAndStyle(rowAccount.createCell(4),getStyleTotalAccount(),ReportConstant.LABEL_TOTAL);
	        addContentAndStyle(rowAccount.createCell(5),getStyleTotalAccountAmount(),totalDebit);
	        addContentAndStyle(rowAccount.createCell(6),getStyleTotalAccountAmount(),totalAssets);

	        addFormulaAndStyle(rowAccount.createCell(7),getStyleTotalAccountAmount(),funtionSubBalance(i+1));
	        i++;
	        indrow=i;
	        indrow++;
	        HSSFRow rowGlosa = sheet.createRow((int)i);
        	addContentAndStyle(rowGlosa.createCell(2),ReportConstant.LABEL_GLOSA);
        	//addContentAndStyle(rowGlosa.createCell(4),accReceipt.getGloss());

        	indrow++;
    	
	    	
	    	//Creamos una celda de tipo fecha y la mostramos
	        //indicando un patron de formato
	        HSSFCellStyle cellStyle = hSSFWorkbook.createCellStyle();
	        cellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat(ReportConstant.LABEL_DATE_FORMAT));
	
	
	        /**adjustColumns**/
	        adjustColumns(sheet,1);
	        //Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_SECURITY);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	         
	       	 hSSFWorkbook.write(bos);
	        
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        
	        hSSFWorkbook.write(fileOut);
	        
	        fileOut.close();
	    }
	    catch(IOException e)
	    {
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
    return arrayByte;
	}
	
	/**
	 * Create excel For Accounting*.
	 *
	 * @param listAccountingReceipt the list accounting receipt
	 * @param reportLogger the report logger
	 * @param accountingProcessTo the accounting process to
	 * @return the byte[]
	 */
	public byte[] writeFileExcel(List<AccountingReceiptTo> listAccountingReceipt,ReportLogger reportLogger,AccountingProcessTo accountingProcessTo)
	{
		String strStartHour = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),ReportConstant.DATE_FORMAT_TIME_SECOND);
		String titleReport=null;
		
		AccountingParameterTO accountingParameterTO=new AccountingParameterTO();
		accountingParameterTO.setIdAccountingParameterPk(accountingProcessTo.getProcessType());
		accountingParameterTO.setParameterType(AccountingParameterType.TIPO_DIARIO_AUXILIAR.getCode());
		accountingParameterTO.setStatus(AccountingParameterStateType.ACTIVED.getCode());
		 List<AccountingParameter>	listTypeAccount=accountingReportQueryServiceBean.findListParameterAccountingByFilter(accountingParameterTO);
		 if(Validations.validateListIsNotNullAndNotEmpty(listTypeAccount)){
			 titleReport=reportLogger.getReport().getTitle().concat(GeneralConstants.DASH).concat(listTypeAccount.get(0).getDescription());
		 }else{
			 titleReport=reportLogger.getReport().getTitle();
		 }
		
		
		byte[] arrayByte=null;
		
		initCellStyle();
		initFont();
		initDataFormat();
	    try
	    {
	    	Integer branchType=accountingProcessTo.getBranch();
	    	HSSFCellStyle stleAmountDebit=null;
	    	HSSFCellStyle stleAmountAssets=null;
	    	
	    	if(AccountingBranchType.CONTABILIDAD_CANTIDADES_VALORES.getCode().equals(branchType)){
	    		stleAmountDebit=getStyleAmountDebitQuantity();
	    		stleAmountAssets=getStyleAmountAssetsQuantity();
	    				
	    	}else{
	    		stleAmountDebit=getStyleAmountDebit();
	    		stleAmountAssets=getStyleAmountAssets();
	    	}
	
	    	/**Created a new sheet within the workBock**/
	    	
	    	HSSFSheet sheet = hSSFWorkbook.createSheet("BookEntry");
	    	
	        /*****
	         * BEGIN - REPORT HEADER 
	         */
	        /**DATE*/
	        HSSFRow row1= sheet.createRow((short)1);
	        addContentAndStyle(row1.createCell(4),getStyleTitleBold(),titleReport);
	        addContentAndStyle(row1.createCell(7),getStyleTitleBold(),"FECHA: ");
	        addContentAndStyle(row1.createCell(8),CommonsUtilities.convertDatetoString(reportLogger.getRegistryDate()));
	        
	        /**HOUR*/
	        HSSFRow row2= sheet.createRow((short)2);
	        addContentAndStyle(row2.createCell(7),getStyleTitleBold(), "HORA:");
	        addContentAndStyle(row2.createCell(8), strStartHour);
	        
	        /**CLASIFICATION*/
	        HSSFRow row3= sheet.createRow((short)3);
	        addContentAndStyle(row3.createCell(7),getStyleTitleBold(),"CLASIFICACION:");
	        addContentAndStyle(row3.createCell(8),reportLogger.getReport().getReportClasification());

	        
	        /**Usuario**/
	        HSSFRow row5= sheet.createRow((short)5);
	        addContentAndStyle(row5.createCell(0),getStyleTitleBold(),"USUARIO:");
	        addContentAndStyle(row5.createCell(1),reportLogger.getRegistryUser());
	        
	        /****
	         * END - REPORT HEADER 
	         */
	        
	        
	        
	        
	        HSSFRow row8= sheet.createRow((short)8);
	        addContentAndStyle(row8.createCell(4),"Periodo del "+CommonsUtilities.convertDatetoString(accountingProcessTo.getInitialDate())+" al "+CommonsUtilities.convertDatetoString(accountingProcessTo.getFinalDate()));
		       
	
	        int indrow=12;
	        for (AccountingReceiptTo accReceipt : listAccountingReceipt) {

	        	
	        	if(accReceipt.getStatus().equals(GeneralConstants.ONE_VALUE_INTEGER)){
	        		
	        		/**Created a new row within the sheet**/
			        HSSFRow row10 = sheet.createRow((int)indrow);
			        addContentAndStyle(row10.createCell(4),"FECHA: "+CommonsUtilities.convertDatetoString(accReceipt.getReceiptDate()));  
			        addContentAndStyle(row10.createCell(5),"RECIBO");
			        addContentAndStyle(row10.createCell(6),accReceipt.getNumberReceipt());
			        indrow++;
			        
			       /**Created Header DEBE - HABER*/
			        HSSFRow row11 = sheet.createRow((int)indrow);
			        addContentAndStyle(row11.createCell(2),getStyleTitle(),"CUENTA");
			        addContentAndStyle(row11.createCell(3),getStyleTitle(),GeneralConstants.EMPTY_STRING);
			        addContentAndStyle(row11.createCell(4),getStyleTitle(),"DESCRIPCIoN");
			        addContentAndStyle(row11.createCell(5),getStyleTitle(),"DEBE");
			        addContentAndStyle(row11.createCell(6),getStyleTitle(),"HABER");
			        addContentAndStyle(row11.createCell(7),getStyleTitle(),"SALDO");
			        indrow++;
			        
			        /****
			         * LIST ACCOUNTING
			         */
			        int i=indrow;
			        BigDecimal sumDebit=new BigDecimal(0);
		        	BigDecimal sumAssets=new BigDecimal(0);
			        for (AccountingAccountTo accountingAccountAsset : accReceipt.getListAccountingAssets()) {
			        	
			        	HSSFRow rowAccount = sheet.createRow((int)i);
			        	
			        	/**Account Code**/
			        	if(accountingAccountAsset.getIndAccumulation().equals(GeneralConstants.ONE_VALUE_INTEGER)&&
			        			Validations.validateIsNullOrNotPositive(accountingAccountAsset.getIdAccountingAccountFk())){
			        		/**Account father max**/
			        		addContentAndStyle(rowAccount.createCell(2),getStyleAccountingAccountFather(),accountingAccountAsset.getAccountCode());
			        		addContentAndStyle(rowAccount.createCell(3),getStyleAccountingAccount(),GeneralConstants.EMPTY_STRING);
			        		
			        		sumDebit=CommonsUtilities.addTwoDecimal(accountingAccountAsset.getAmountDebit(), sumDebit, 4);
			        		sumAssets=CommonsUtilities.addTwoDecimal(accountingAccountAsset.getAmountAssets(), sumAssets, 4);
			        	}
			        	else{
			        		/**Other Account no father**/
			        		addContentAndStyle(rowAccount.createCell(2),getStyleAccountingAccount(),GeneralConstants.EMPTY_STRING);
			        		addContentAndStyle(rowAccount.createCell(3),getStyleAccountingAccount(),accountingAccountAsset.getAccountCode());
			        	}
			        	
			        	/**Account Description**/
			        	addContentAndStyle(rowAccount.createCell(4),getStyleDescriptionAccountingAccount(),accountingAccountAsset.getDescription());
			        	/**Only Father Show*/
			        	if(accountingAccountAsset.getIndAccumulation().equals(GeneralConstants.ONE_VALUE_INTEGER)&&
			        			Validations.validateIsNullOrNotPositive(accountingAccountAsset.getIdAccountingAccountFk())){
			        		/**Account Amount debit**/
				        	if(Validations.validateIsNotNull(accountingAccountAsset.getAmountDebit())){
				        		Double valueDebit=accountingAccountAsset.getAmountDebit().doubleValue();
				        		addContentAndStyle(rowAccount.createCell(5),stleAmountDebit,valueDebit);
				        	}
				        	else{
				        		addContentAndStyle(rowAccount.createCell(5),stleAmountDebit,0);
				        	}
				        	/**Amount Assets**/
				        	if(Validations.validateIsNotNull(accountingAccountAsset.getAmountAssets())){
				        		Double valueAsset=accountingAccountAsset.getAmountAssets().doubleValue();
				        		addContentAndStyle(rowAccount.createCell(6),stleAmountAssets,valueAsset);
				        	}else{
				        		addContentAndStyle(rowAccount.createCell(6),stleAmountAssets,0);
				        	}
				        	
				        	addFormulaAndStyle(rowAccount.createCell(7),stleAmountAssets,funtionSubBalance(i+1));
				        	
				        	
			        	}else{
			        		/**Account Not Father**/
			        		addContentAndStyle(rowAccount.createCell(5),getStyleAccountingAccount(),GeneralConstants.BLANK_SPACE);
			        		addContentAndStyle(rowAccount.createCell(6),getStyleAccountingAccount(),GeneralConstants.BLANK_SPACE);
			        		addContentAndStyle(rowAccount.createCell(7),getStyleAccountingAccount(),GeneralConstants.BLANK_SPACE);
			        	}
			        	
			        	i++;
					
					}
			        
			      
			        
			        /**End of each line seat**/
			        HSSFRow rowAccount = sheet.createRow((int)i);
			        
			        for (int j = 2; j < sheet.getRow(i-1).getLastCellNum(); j++) {
			        	addContentAndStyle(rowAccount.createCell(j),setBorder(1,0,0,0),"");
					}
			        Double totalDebit=sumDebit.doubleValue();
			        Double totalAssets=sumAssets.doubleValue();
			        
			        addContentAndStyle(rowAccount.createCell(4),getStyleTotalAccount(),"TOTAL");
			        addContentAndStyle(rowAccount.createCell(5),getStyleTotalAccountAmount(),totalDebit);
			        addContentAndStyle(rowAccount.createCell(6),getStyleTotalAccountAmount(),totalAssets);

			        addFormulaAndStyle(rowAccount.createCell(7),getStyleTotalAccountAmount(),funtionSubBalance(i+1));
			        i++;
			        indrow=i;
			        indrow++;
			        HSSFRow rowGlosa = sheet.createRow((int)i);
		        	addContentAndStyle(rowGlosa.createCell(2),"GLOSA");
		        	addContentAndStyle(rowGlosa.createCell(4),accReceipt.getGloss());

		        	indrow++;
	        	}
	        	
	        	
			}
	        
	        
	        
	
	        //Creamos una celda de tipo fecha y la mostramos
	        //indicando un patron de formato
	        HSSFCellStyle cellStyle = hSSFWorkbook.createCellStyle();
	        cellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("d/m/yy h:mm"));
	
	
	        /**adjustColumns**/
	        adjustColumns(sheet,1);
	        //Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream("ExcelNuevoFinal.xls");
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	         
	       	 hSSFWorkbook.write(bos);
	        
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        
	        hSSFWorkbook.write(fileOut);
	        
	        fileOut.close();
	    }
	    catch(IOException e)
	    {
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
	    return arrayByte;
	}
	
	
	
	/**
	 * Reporte de Cuadre De Operaciones 
	 * Conciliacion .
	 *
	 * @param parameters the parameters
	 * @param reportLogger the report logger
	 * @param accountingProcessTo the accounting process to
	 * @return the byte[]
	 */
	public byte[] writeFileExcelAccountingSquare(Map<String, Object> parameters , ReportLogger reportLogger,
												AccountingProcessTo accountingProcessTo ){
		byte[] arrayByte=null;
		String strStartHour = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),ReportConstant.DATE_FORMAT_TIME_SECOND);
		String titleReport=null;
		AccountingProcess accountingProcess=accountingReportQueryServiceBean.findAccountingProcessToFilter(accountingProcessTo);
		AccountingParameterTO accountingParameterTO=new AccountingParameterTO();
		accountingParameterTO.setIdAccountingParameterPk(accountingProcess.getProcessType());
		accountingParameterTO.setParameterType(AccountingParameterType.TIPO_DIARIO_AUXILIAR.getCode());
		accountingParameterTO.setStatus(AccountingParameterStateType.ACTIVED.getCode());
		List<AccountingParameter>	listTypeAccount=accountingReportQueryServiceBean.findListParameterAccountingByFilter(accountingParameterTO);
		if(Validations.validateListIsNotNullAndNotEmpty(listTypeAccount)){
			 titleReport=reportLogger.getReport().getTitle().concat(GeneralConstants.DASH).concat(listTypeAccount.get(0).getDescription());
		}else{
			 titleReport=reportLogger.getReport().getTitle();
		}


		initCellStyle();
		initFont();
		initDataFormat();
		

		try{
			
			
			BigDecimal dailyExchangeUSD=accountingReportServiceBean.processExchange(parameters,CurrencyType.USD.getCode(),
		    		CurrencyType.PYG.getCode(),DailyExchangeRoleType.BUY.getCode());
			BigDecimal dailyExchangeUFV=accountingReportServiceBean.processExchange(parameters,CurrencyType.UFV.getCode(),
		    		CurrencyType.PYG.getCode(),DailyExchangeRoleType.BUY.getCode());
			BigDecimal dailyExchangeECU=accountingReportServiceBean.processExchange(parameters,CurrencyType.EU.getCode(),
		    		CurrencyType.PYG.getCode(),DailyExchangeRoleType.BUY.getCode());


			
			/**Created a new sheet within the workBock**/
	    	
	    	HSSFSheet sheet = hSSFWorkbook.createSheet("BookEntry");

	        /*****
	         * BEGIN - REPORT HEADER 
	         */
	        /** TITLE REPORT */
	        HSSFRow row1= sheet.createRow((short)0);
	        addContentAndStyle(row1.createCell(1),getStyleTitleBoldBackBlue(),titleReport);
	        
	        /**CABECERA TABLA*/
	        HSSFRow row4= sheet.createRow((short)4);
	        addContentAndStyle(row4.createCell(0),getStyleHeaderTitleBoldBackBlueTen(), ReportConstant.LABEL_DATE_TIME_CLOSE);
	        addContentAndStyle(row4.createCell(1),getStyleHeaderTitleBoldBackBlueTen(), ReportConstant.LABEL_SYSTEM_DATE);
	        addContentAndStyle(row4.createCell(2),getStyleHeaderTitleBoldBackBlueTen(), ReportConstant.LABEL_TC_USD);
	        addContentAndStyle(row4.createCell(3),getStyleHeaderTitleBoldBackBlueTen(), ReportConstant.LABEL_TC_UFV);
	        addContentAndStyle(row4.createCell(4),getStyleHeaderTitleBoldBackBlueTen(), ReportConstant.LABEL_TC_ECU);
	      	        
	        /**DETALLE TABLA**/
	        HSSFRow row5= sheet.createRow((short)5);

	        addContentAndStyle(row5.createCell(0),  CommonsUtilities.convertDatetoString(accountingProcess.getExecutionDate()).concat(GeneralConstants.BLANK_SPACE).concat(strStartHour) );
	        addContentAndStyle(row5.createCell(1),  CommonsUtilities.convertDatetoString(reportLogger.getRegistryDate()));
	        addContentAndStyle(row5.createCell(2),  dailyExchangeUSD.doubleValue());
	        addContentAndStyle(row5.createCell(3),  dailyExchangeUFV.doubleValue());
	        addContentAndStyle(row5.createCell(4),  dailyExchangeECU.doubleValue());
	        
	        /****
	         * END - REPORT HEADER 
	         */
	        HSSFRow row8= sheet.createRow((short)8);
	        addContentAndStyle(row8.createCell(0), getStyleHeaderTitleBoldBackBlueTwelve(), ReportConstant.LABEL_ACCOUNTING_SQUARING);

	        /**TABLA CUADRE CONTABLE*/
	        HSSFRow row9= sheet.createRow((short)9);
	        addContentAndStyle(row9.createCell(0), getStyleHeaderTitleBoldBackBlueTwelve(), GeneralConstants.BLANK_SPACE);
	        addContentAndStyle(row9.createCell(1), getStyleHeaderTitleBoldBackBlueTwelve(), ReportConstant.LABEL_PORTFOLIO_BALANCE);
	        addContentAndStyle(row9.createCell(7), getStyleHeaderTitleBoldBackBlueTwelve(), ReportConstant.LABEL_ACCOUNTING_BALANCE);
	        addContentAndStyle(row9.createCell(14), getStyleHeaderTitleBoldBackBlueTwelve(), GeneralConstants.BLANK_SPACE);
		    
	        /**CABECERA DE LAS DESCRIPCIONES*/
	        HSSFRow row10= sheet.createRow((short)10);
	        addContentAndStyle(row10.createCell(0), getStyleHeaderTitleBoldBackBlueTwelve(), ReportConstant.LABEL_DESCRIPTION);
	        addContentAndStyle(row10.createCell(1), getStyleHeaderTitleBoldBackBlueTwelve(), ReportConstant.LABEL_CURRENCY_BOB);
	        addContentAndStyle(row10.createCell(2), getStyleHeaderTitleBoldBackBlueTwelve(), ReportConstant.LABEL_CURRENCY_USD);
	        addContentAndStyle(row10.createCell(3), getStyleHeaderTitleBoldBackBlueTwelve(), ReportConstant.LABEL_CURRENCY_MV);
	        addContentAndStyle(row10.createCell(4), getStyleHeaderTitleBoldBackBlueTwelve(), ReportConstant.LABEL_CURRENCY_UFV);
	        addContentAndStyle(row10.createCell(5), getStyleHeaderTitleBoldBackBlueTwelve(), ReportConstant.LABEL_CURRENCY_EURO);
	        addContentAndStyle(row10.createCell(6), getStyleHeaderTitleBoldBackBlueTwelve(), ReportConstant.LABEL_TOTAL_CURRENCY_BOB);
	        addContentAndStyle(row10.createCell(7), getStyleHeaderTitleBoldBackBlueTwelve(), ReportConstant.LABEL_ACCOUNT);
	        addContentAndStyle(row10.createCell(8), getStyleHeaderTitleBoldBackBlueTwelve(), ReportConstant.LABEL_CURRENCY_BOB);
	        addContentAndStyle(row10.createCell(9), getStyleHeaderTitleBoldBackBlueTwelve(), ReportConstant.LABEL_CURRENCY_USD);
	        addContentAndStyle(row10.createCell(10), getStyleHeaderTitleBoldBackBlueTwelve(), ReportConstant.LABEL_CURRENCY_MV);
	        addContentAndStyle(row10.createCell(11), getStyleHeaderTitleBoldBackBlueTwelve(), ReportConstant.LABEL_CURRENCY_UFV);
	        addContentAndStyle(row10.createCell(12), getStyleHeaderTitleBoldBackBlueTwelve(), ReportConstant.LABEL_CURRENCY_EURO);
	        addContentAndStyle(row10.createCell(13), getStyleHeaderTitleBoldBackBlueTwelve(), ReportConstant.LABEL_TOTAL_CURRENCY_BOB);
	        addContentAndStyle(row10.createCell(14), getStyleHeaderTitleBoldBackBlueTwelve(), ReportConstant.LABEL_DIFERENCE);

	
	
	        Map<String,OperationBalanceTo> paramOperation=
	        		(Map<String, OperationBalanceTo>) parameters.get(GeneralConstants.SQUARING_OPERATIONS);
	        OperationBalanceTo 	balanceAvailable=paramOperation.get(GeneralConstants.DESMATERIALIZADO);
			OperationBalanceTo 	balanceReporting=paramOperation.get(GeneralConstants.REPORTANTE);
			OperationBalanceTo 	balanceBlock=paramOperation.get(GeneralConstants.BLOQUEOS);
			OperationBalanceTo 	balanceOtherBlock=paramOperation.get(GeneralConstants.BLOQUEO_OTROS);
			OperationBalanceTo 	balancePhysical=paramOperation.get(GeneralConstants.FISICOS);
			OperationBalanceTo 	balanceNotPlaced=paramOperation.get(GeneralConstants.NOT_PLACED);


			BigDecimal totalAmountOperation=new BigDecimal(0);
			BigDecimal amountOperAvailable=balanceAvailable.getAmountToBOB();
			BigDecimal amountOperReporting=balanceReporting.getAmountToBOB();
			BigDecimal amountOperBlock=balanceBlock.getAmountToBOB();
			BigDecimal amountOperOtherBlock=balanceOtherBlock.getAmountToBOB();
			BigDecimal amountOperPhysical=balancePhysical.getAmountToBOB();
			BigDecimal amountOperNotPlaced=balanceNotPlaced.getAmountToBOB();
			
			BigDecimal totalAmountAccounting=new BigDecimal(0);
			BigDecimal amountAccountingAvailable=new BigDecimal(0);
			BigDecimal amountAccountingReporting=new BigDecimal(0);
			BigDecimal amountAccountingBlock=new BigDecimal(0);
			BigDecimal amountAccountingOtherBlock=new BigDecimal(0);
			BigDecimal amountAccountingPhysical=new BigDecimal(0);
			BigDecimal amountAccountingNotPlaced=new BigDecimal(0);	
			
			BigDecimal totalRemainder=new BigDecimal(0);
			BigDecimal amountRemainderAvailable=new BigDecimal(0);
			BigDecimal amountRemainderReporting=new BigDecimal(0);
			BigDecimal amountRemainderBlock=new BigDecimal(0);
			BigDecimal amountRemainderOtherBlock=new BigDecimal(0);
			BigDecimal amountRemainderPhysical=new BigDecimal(0);
			BigDecimal amountRemainderNotPlaced=new BigDecimal(0);
			
			List<AccountingAccount> listAccountingAccount=
					(List<AccountingAccount>) parameters.get(GeneralConstants.SQUARING_ACCOUNTING);



	        
	        HSSFRow row11 = sheet.createRow((int)11);
	        addContentAndStyle(row11.createCell(0),getStyleTitle(), ReportConstant.LABEL_CUSTODY_BALANCE_SECURITY_AVAILABLE);
	        addContentAndStyle(row11.createCell(1),getStyleTitle(), balanceAvailable.getBalanceBOB().doubleValue());
	        addContentAndStyle(row11.createCell(2),getStyleTitle(), balanceAvailable.getBalanceUSD().doubleValue());
	        addContentAndStyle(row11.createCell(3),getStyleTitle(), balanceAvailable.getBalanceMVL().doubleValue());
	        addContentAndStyle(row11.createCell(4),getStyleTitle(), balanceAvailable.getBalanceUFV().doubleValue());
	        addContentAndStyle(row11.createCell(5),getStyleTitle(), balanceAvailable.getBalanceECU().doubleValue());
	        addContentAndStyle(row11.createCell(6),getStyleHeaderTitleBoldBackBlueTwelve(), amountOperAvailable.doubleValue());
	        totalAmountOperation=CommonsUtilities.addTwoDecimal(totalAmountOperation, amountOperAvailable, 4);

	        HSSFRow row12 = sheet.createRow((int)12);
	        HSSFRow row13 = sheet.createRow((int)13);
	        HSSFRow row15 = sheet.createRow((int)15);
	        HSSFRow row16 = sheet.createRow((int)16);
	        HSSFRow row18 = sheet.createRow((int)18);
	        HSSFRow row19 = sheet.createRow((int)19);
	        HSSFRow row21 = sheet.createRow((int)21);
	        HSSFRow row22 = sheet.createRow((int)22);
	        HSSFRow row24 = sheet.createRow((int)24);
	        HSSFRow row26 = sheet.createRow((int)26);
	        HSSFRow row27 = sheet.createRow((int)27);

	        HSSFRow row14 = sheet.createRow((int)14);
	        addContentAndStyle(row14.createCell(0),getStyleTitle(), ReportConstant.LABEL_BALANCE_SECURITY_REPOTED);
	        addContentAndStyle(row14.createCell(1),getStyleTitle(), balanceReporting.getBalanceBOB().doubleValue());
	        addContentAndStyle(row14.createCell(2),getStyleTitle(), balanceReporting.getBalanceUSD().doubleValue());
	        addContentAndStyle(row14.createCell(3),getStyleTitle(), balanceReporting.getBalanceMVL().doubleValue());
	        addContentAndStyle(row14.createCell(4),getStyleTitle(), balanceReporting.getBalanceUFV().doubleValue());
	        addContentAndStyle(row14.createCell(5),getStyleTitle(), balanceReporting.getBalanceECU().doubleValue());
	        addContentAndStyle(row14.createCell(6),getStyleHeaderTitleBoldBackBlueTwelve(), amountOperReporting.doubleValue());
	        totalAmountOperation=CommonsUtilities.addTwoDecimal(totalAmountOperation, amountOperReporting, 4);
	        
	        HSSFRow row17 = sheet.createRow((int)17);
	        addContentAndStyle(row17.createCell(0),getStyleTitle(), ReportConstant.LABEL_BALANCE_SECURITY_LOCK);
	        addContentAndStyle(row17.createCell(1),getStyleTitle(), balanceBlock.getBalanceBOB().doubleValue());
	        addContentAndStyle(row17.createCell(2),getStyleTitle(), balanceBlock.getBalanceUSD().doubleValue());
	        addContentAndStyle(row17.createCell(3),getStyleTitle(), balanceBlock.getBalanceMVL().doubleValue());
	        addContentAndStyle(row17.createCell(4),getStyleTitle(), balanceBlock.getBalanceUFV().doubleValue());
	        addContentAndStyle(row17.createCell(5),getStyleTitle(), balanceBlock.getBalanceECU().doubleValue());
	        addContentAndStyle(row17.createCell(6),getStyleHeaderTitleBoldBackBlueTwelve(), amountOperBlock.doubleValue());
	        totalAmountOperation=CommonsUtilities.addTwoDecimal(totalAmountOperation, amountOperBlock, 4);
	        
	        HSSFRow row20 = sheet.createRow((int)20);
	        addContentAndStyle(row20.createCell(0),getStyleTitle(), ReportConstant.LABEL_BALANCE_SECURITY_OTHER_LOCK);
	        addContentAndStyle(row20.createCell(1),getStyleTitle(), balanceOtherBlock.getBalanceBOB().doubleValue());
	        addContentAndStyle(row20.createCell(2),getStyleTitle(), balanceOtherBlock.getBalanceUSD().doubleValue());
	        addContentAndStyle(row20.createCell(3),getStyleTitle(), balanceOtherBlock.getBalanceMVL().doubleValue());
	        addContentAndStyle(row20.createCell(4),getStyleTitle(), balanceOtherBlock.getBalanceUFV().doubleValue());
	        addContentAndStyle(row20.createCell(5),getStyleTitle(), balanceOtherBlock.getBalanceECU().doubleValue());
	        addContentAndStyle(row20.createCell(6),getStyleHeaderTitleBoldBackBlueTwelve(), amountOperOtherBlock.doubleValue());
	        totalAmountOperation=CommonsUtilities.addTwoDecimal(totalAmountOperation, amountOperOtherBlock, 4);
	        
	        HSSFRow row23 = sheet.createRow((int)23);
	        addContentAndStyle(row23.createCell(0),getStyleTitle(), ReportConstant.LABEL_BALANCE_SECURITY_PHYSICAL);
	        addContentAndStyle(row23.createCell(1),getStyleTitle(), balancePhysical.getBalanceBOB().doubleValue());
	        addContentAndStyle(row23.createCell(2),getStyleTitle(), balancePhysical.getBalanceUSD().doubleValue());
	        addContentAndStyle(row23.createCell(3),getStyleTitle(), balancePhysical.getBalanceMVL().doubleValue());
	        addContentAndStyle(row23.createCell(4),getStyleTitle(), balancePhysical.getBalanceUFV().doubleValue());
	        addContentAndStyle(row23.createCell(5),getStyleTitle(), balancePhysical.getBalanceECU().doubleValue());
	        addContentAndStyle(row23.createCell(6),getStyleHeaderTitleBoldBackBlueTwelve(), amountOperPhysical.doubleValue());
	        totalAmountOperation=CommonsUtilities.addTwoDecimal(totalAmountOperation, amountOperPhysical, 4);
	        
	        HSSFRow row25 = sheet.createRow((int)25);
	        addContentAndStyle(row25.createCell(0),getStyleTitle(), ReportConstant.LABEL_SECURITY_PENDING);
	        addContentAndStyle(row25.createCell(1),getStyleTitle(), balanceNotPlaced.getBalanceBOB().doubleValue());
	        addContentAndStyle(row25.createCell(2),getStyleTitle(), balanceNotPlaced.getBalanceUSD().doubleValue());
	        addContentAndStyle(row25.createCell(3),getStyleTitle(), balanceNotPlaced.getBalanceMVL().doubleValue());
	        addContentAndStyle(row25.createCell(4),getStyleTitle(), balanceNotPlaced.getBalanceUFV().doubleValue());
	        addContentAndStyle(row25.createCell(5),getStyleTitle(), balanceNotPlaced.getBalanceECU().doubleValue());
	        addContentAndStyle(row25.createCell(6),getStyleHeaderTitleBoldBackBlueTwelve(), amountOperNotPlaced.doubleValue());
	        totalAmountOperation=CommonsUtilities.addTwoDecimal(totalAmountOperation, amountOperNotPlaced, 4);
	        
	        HSSFRow row28 = sheet.createRow((int)28);
	        addContentAndStyle(row28.createCell(5),getStyleTitle(), ReportConstant.LABEL_TOTAL);
	        addContentAndStyle(row28.createCell(6),getStyleTitle(), totalAmountOperation.doubleValue());
	    	

	        /**LABEL ACCOUNTING ACCOUNT*/
			addContentAndStyle(row11.createCell(7),getStyleTitle(), ReportConstant.LABEL_ACCOUNT_IF_AVAILABLE);
			addContentAndStyle(row12.createCell(7),getStyleTitle(), ReportConstant.LABEL_ACCOUNT_IV_AVAILABLE);
			addContentAndStyle(row13.createCell(7),getStyleTitle(), ReportConstant.LABEL_ACCOUNT_IM_AVAILABLE);
			
			addContentAndStyle(row14.createCell(7),getStyleTitle(), ReportConstant.LABEL_ACCOUNT_IF_REPOTED);
			addContentAndStyle(row15.createCell(7),getStyleTitle(), ReportConstant.LABEL_ACCOUNT_IV_REPOTED);
			addContentAndStyle(row16.createCell(7),getStyleTitle(), ReportConstant.LABEL_ACCOUNT_IM_REPOTED);
			
			addContentAndStyle(row17.createCell(7),getStyleTitle(), ReportConstant.LABEL_ACCOUNT_IF_LOCK);
			addContentAndStyle(row18.createCell(7),getStyleTitle(), ReportConstant.LABEL_ACCOUNT_IV_LOCK);
			addContentAndStyle(row19.createCell(7),getStyleTitle(), ReportConstant.LABEL_ACCOUNT_IM_LOCK);
			
			addContentAndStyle(row20.createCell(7),getStyleTitle(), ReportConstant.LABEL_ACCOUNT_IF_OTHER_LOCK);
			addContentAndStyle(row21.createCell(7),getStyleTitle(), ReportConstant.LABEL_ACCOUNT_IV_OTHER_LOCK);
			addContentAndStyle(row22.createCell(7),getStyleTitle(), ReportConstant.LABEL_ACCOUNT_IM_OTHER_LOCK);
			
			addContentAndStyle(row23.createCell(7),getStyleTitle(), ReportConstant.LABEL_ACCOUNT_IF_PHYSICAL);
			addContentAndStyle(row24.createCell(7),getStyleTitle(), ReportConstant.LABEL_ACCOUNT_IV_PHYSICAL);
			
			addContentAndStyle(row25.createCell(7),getStyleTitle(), ReportConstant.LABEL_ACCOUNT_IF_NOT_PLACED);
			addContentAndStyle(row26.createCell(7),getStyleTitle(), ReportConstant.LABEL_ACCOUNT_IV_NOT_PLACED);
			addContentAndStyle(row27.createCell(7),getStyleTitle(), ReportConstant.LABEL_ACCOUNT_IM_NOT_PLACED);
			
			/**
			 * Fill Zero By Default
			 */
	        for (int i = 11; i <= 27; i++) {
	        	HSSFRow rowiter = sheet.getRow(i);
				for (int j = 8; j <= 13; j++) {
					addContentAndStyle(rowiter.createCell(j),getStyleTitle(), new Double(0));
				}
			}
	        
	        for (AccountingAccount accountingAccount : listAccountingAccount) {
				
	        	if(AccountingConciliationType.DEMATERIALIZED_BALANCE_AVAILABLE.getCode().equals(accountingAccount.getPortFolio())){
	        		
        			
	        		//11
	        		if(InstrumentType.FIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
		     	        addContentAndStyle(row11.createCell(8),getStyleTitle(), accountingAccount.getAmountBOB().doubleValue());
		     	        addContentAndStyle(row11.createCell(9),getStyleTitle(), accountingAccount.getAmountUSD().doubleValue());
		     	        addContentAndStyle(row11.createCell(10),getStyleTitle(), accountingAccount.getAmountMVL().doubleValue());
		     	        addContentAndStyle(row11.createCell(11),getStyleTitle(), accountingAccount.getAmountUFV().doubleValue());
		     	        addContentAndStyle(row11.createCell(12),getStyleTitle(), accountingAccount.getAmountEU().doubleValue());
		     	        amountAccountingAvailable=CommonsUtilities.addTwoDecimal(amountAccountingAvailable, accountingAccount.getAmountToBOB(), 4);
	        		//12	
	        		}else if(InstrumentType.VARIABLE_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
		     	        addContentAndStyle(row12.createCell(8),getStyleTitle(), accountingAccount.getAmountBOB().doubleValue());
		     	        addContentAndStyle(row12.createCell(9),getStyleTitle(), accountingAccount.getAmountUSD().doubleValue());
		     	        addContentAndStyle(row12.createCell(10),getStyleTitle(), accountingAccount.getAmountMVL().doubleValue());
		     	        addContentAndStyle(row12.createCell(11),getStyleTitle(), accountingAccount.getAmountUFV().doubleValue());
		     	        addContentAndStyle(row12.createCell(12),getStyleTitle(), accountingAccount.getAmountEU().doubleValue());
			     	    amountAccountingAvailable=CommonsUtilities.addTwoDecimal(amountAccountingAvailable, accountingAccount.getAmountToBOB(), 4);
	        		//13
	        		}else if(InstrumentType.MIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
		     	        addContentAndStyle(row13.createCell(8),getStyleTitle(), accountingAccount.getAmountBOB().doubleValue());
		     	        addContentAndStyle(row13.createCell(9),getStyleTitle(), accountingAccount.getAmountUSD().doubleValue());
		     	        addContentAndStyle(row13.createCell(10),getStyleTitle(), accountingAccount.getAmountMVL().doubleValue());
		     	        addContentAndStyle(row13.createCell(11),getStyleTitle(), accountingAccount.getAmountUFV().doubleValue());
		     	        addContentAndStyle(row13.createCell(12),getStyleTitle(), accountingAccount.getAmountEU().doubleValue());
		     	        amountAccountingAvailable=CommonsUtilities.addTwoDecimal(amountAccountingAvailable, accountingAccount.getAmountToBOB(), 4);
	        		}
	        		
	        	}else if(AccountingConciliationType.DEMATERIALIZED_BALANCE_REPORTED.getCode().equals(accountingAccount.getPortFolio())){
	        		//14
	        		if(InstrumentType.FIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
		     	        addContentAndStyle(row14.createCell(8),getStyleTitle(), accountingAccount.getAmountBOB().doubleValue());
		     	        addContentAndStyle(row14.createCell(9),getStyleTitle(), accountingAccount.getAmountUSD().doubleValue());
		     	        addContentAndStyle(row14.createCell(10),getStyleTitle(), accountingAccount.getAmountMVL().doubleValue());
		     	        addContentAndStyle(row14.createCell(11),getStyleTitle(), accountingAccount.getAmountUFV().doubleValue());
		     	        addContentAndStyle(row14.createCell(12),getStyleTitle(), accountingAccount.getAmountEU().doubleValue());
		     	        amountAccountingReporting=CommonsUtilities.addTwoDecimal(amountAccountingReporting, accountingAccount.getAmountToBOB(), 4);
	        		//15
	        		}else if(InstrumentType.VARIABLE_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
		     	        addContentAndStyle(row15.createCell(8),getStyleTitle(), accountingAccount.getAmountBOB().doubleValue());
		     	        addContentAndStyle(row15.createCell(9),getStyleTitle(), accountingAccount.getAmountUSD().doubleValue());
		     	        addContentAndStyle(row15.createCell(10),getStyleTitle(), accountingAccount.getAmountMVL().doubleValue());
		     	        addContentAndStyle(row15.createCell(11),getStyleTitle(), accountingAccount.getAmountUFV().doubleValue());
		     	        addContentAndStyle(row15.createCell(12),getStyleTitle(), accountingAccount.getAmountEU().doubleValue());
		     	        amountAccountingReporting=CommonsUtilities.addTwoDecimal(amountAccountingReporting, accountingAccount.getAmountToBOB(), 4);
	        		//16
	        		}else if(InstrumentType.MIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
		     	        addContentAndStyle(row16.createCell(8),getStyleTitle(), accountingAccount.getAmountBOB().doubleValue());
		     	        addContentAndStyle(row16.createCell(9),getStyleTitle(), accountingAccount.getAmountUSD().doubleValue());
		     	        addContentAndStyle(row16.createCell(10),getStyleTitle(), accountingAccount.getAmountMVL().doubleValue());
		     	        addContentAndStyle(row16.createCell(11),getStyleTitle(), accountingAccount.getAmountUFV().doubleValue());
		     	        addContentAndStyle(row16.createCell(12),getStyleTitle(), accountingAccount.getAmountEU().doubleValue());
		     	        amountAccountingReporting=CommonsUtilities.addTwoDecimal(amountAccountingReporting, accountingAccount.getAmountToBOB(), 4);
	        		}
	        		
	        	}else if(AccountingConciliationType.DEMATERIALIZED_BALANCE_LOCK.getCode().equals(accountingAccount.getPortFolio())){
	        		//17
	        		if(InstrumentType.FIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
		     	        addContentAndStyle(row17.createCell(8),getStyleTitle(),  accountingAccount.getAmountBOB().doubleValue());
		     	        addContentAndStyle(row17.createCell(9),getStyleTitle(),  accountingAccount.getAmountUSD().doubleValue());
		     	        addContentAndStyle(row17.createCell(10),getStyleTitle(), accountingAccount.getAmountMVL().doubleValue());
		     	        addContentAndStyle(row17.createCell(11),getStyleTitle(), accountingAccount.getAmountUFV().doubleValue());
		     	        addContentAndStyle(row17.createCell(12),getStyleTitle(), accountingAccount.getAmountEU().doubleValue());
		     	        amountAccountingBlock=CommonsUtilities.addTwoDecimal(amountAccountingBlock, accountingAccount.getAmountToBOB(), 4);
	        		//18
	        		}else if(InstrumentType.VARIABLE_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
		     	        addContentAndStyle(row18.createCell(8),getStyleTitle(),  accountingAccount.getAmountBOB().doubleValue());
		     	        addContentAndStyle(row18.createCell(9),getStyleTitle(),  accountingAccount.getAmountUSD().doubleValue());
		     	        addContentAndStyle(row18.createCell(10),getStyleTitle(), accountingAccount.getAmountMVL().doubleValue());
		     	        addContentAndStyle(row18.createCell(11),getStyleTitle(), accountingAccount.getAmountUFV().doubleValue());
		     	        addContentAndStyle(row18.createCell(12),getStyleTitle(), accountingAccount.getAmountEU().doubleValue());
		     	        amountAccountingBlock=CommonsUtilities.addTwoDecimal(amountAccountingBlock, accountingAccount.getAmountToBOB(), 4);
	        		//19
	        		}else if(InstrumentType.MIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
		     	        addContentAndStyle(row19.createCell(8),getStyleTitle(), accountingAccount.getAmountBOB().doubleValue());
		     	        addContentAndStyle(row19.createCell(9),getStyleTitle(), accountingAccount.getAmountUSD().doubleValue());
		     	        addContentAndStyle(row19.createCell(10),getStyleTitle(), accountingAccount.getAmountMVL().doubleValue());
		     	        addContentAndStyle(row19.createCell(11),getStyleTitle(), accountingAccount.getAmountUFV().doubleValue());
		     	        addContentAndStyle(row19.createCell(12),getStyleTitle(), accountingAccount.getAmountEU().doubleValue());
		     	        amountAccountingBlock=CommonsUtilities.addTwoDecimal(amountAccountingBlock, accountingAccount.getAmountToBOB(), 4);
	        			
	        		}
	        		
	        		
	        	}else if(AccountingConciliationType.DEMATERIALIZED_BALANCE_LOCK_OTHER.getCode().equals(accountingAccount.getPortFolio())){
	        		//20
	        		if(InstrumentType.FIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
		     	        addContentAndStyle(row20.createCell(8),getStyleTitle(), accountingAccount.getAmountBOB().doubleValue());
		     	        addContentAndStyle(row20.createCell(9),getStyleTitle(), accountingAccount.getAmountUSD().doubleValue());
		     	        addContentAndStyle(row20.createCell(10),getStyleTitle(), accountingAccount.getAmountMVL().doubleValue());
		     	        addContentAndStyle(row20.createCell(11),getStyleTitle(), accountingAccount.getAmountUFV().doubleValue());
		     	        addContentAndStyle(row20.createCell(12),getStyleTitle(), accountingAccount.getAmountEU().doubleValue());
		     	        amountAccountingOtherBlock=CommonsUtilities.addTwoDecimal(amountAccountingOtherBlock, accountingAccount.getAmountToBOB(), 4);
	        		//21
	        		}else if(InstrumentType.VARIABLE_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
		     	        addContentAndStyle(row21.createCell(8),getStyleTitle(), accountingAccount.getAmountBOB().doubleValue());
		     	        addContentAndStyle(row21.createCell(9),getStyleTitle(), accountingAccount.getAmountUSD().doubleValue());
		     	        addContentAndStyle(row21.createCell(10),getStyleTitle(), accountingAccount.getAmountMVL().doubleValue());
		     	        addContentAndStyle(row21.createCell(11),getStyleTitle(), accountingAccount.getAmountUFV().doubleValue());
		     	        addContentAndStyle(row21.createCell(12),getStyleTitle(), accountingAccount.getAmountEU().doubleValue());
		     	        amountAccountingOtherBlock=CommonsUtilities.addTwoDecimal(amountAccountingOtherBlock, accountingAccount.getAmountToBOB(), 4);
	        		//22
	        		}else if(InstrumentType.MIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
		     	        addContentAndStyle(row22.createCell(8),getStyleTitle(), accountingAccount.getAmountBOB().doubleValue());
		     	        addContentAndStyle(row22.createCell(9),getStyleTitle(), accountingAccount.getAmountUSD().doubleValue());
		     	        addContentAndStyle(row22.createCell(10),getStyleTitle(), accountingAccount.getAmountMVL().doubleValue());
		     	        addContentAndStyle(row22.createCell(11),getStyleTitle(), accountingAccount.getAmountUFV().doubleValue());
		     	        addContentAndStyle(row22.createCell(12),getStyleTitle(), accountingAccount.getAmountEU().doubleValue());
		     	        amountAccountingOtherBlock=CommonsUtilities.addTwoDecimal(amountAccountingOtherBlock, accountingAccount.getAmountToBOB(), 4);
	        			
	        		}
	        		
	        		
	        	}else if(AccountingConciliationType.PHYSICAL_BALANCE.getCode().equals(accountingAccount.getPortFolio())){
	        		//23
	        		if(InstrumentType.FIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
		     	        addContentAndStyle(row23.createCell(8),getStyleTitle(), accountingAccount.getAmountBOB().doubleValue());
		     	        addContentAndStyle(row23.createCell(9),getStyleTitle(), accountingAccount.getAmountUSD().doubleValue());
		     	        addContentAndStyle(row23.createCell(10),getStyleTitle(), accountingAccount.getAmountMVL().doubleValue());
		     	        addContentAndStyle(row23.createCell(11),getStyleTitle(), accountingAccount.getAmountUFV().doubleValue());
		     	        addContentAndStyle(row23.createCell(12),getStyleTitle(), accountingAccount.getAmountEU().doubleValue());
		     	        amountAccountingPhysical=CommonsUtilities.addTwoDecimal(amountAccountingPhysical, accountingAccount.getAmountToBOB(), 4);
	        		//24
	        		}else if(InstrumentType.VARIABLE_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
		     	        addContentAndStyle(row24.createCell(8),getStyleTitle(), accountingAccount.getAmountBOB().doubleValue());
		     	        addContentAndStyle(row24.createCell(9),getStyleTitle(), accountingAccount.getAmountUSD().doubleValue());
		     	        addContentAndStyle(row24.createCell(10),getStyleTitle(), accountingAccount.getAmountMVL().doubleValue());
		     	        addContentAndStyle(row24.createCell(11),getStyleTitle(), accountingAccount.getAmountUFV().doubleValue());
		     	        addContentAndStyle(row24.createCell(12),getStyleTitle(), accountingAccount.getAmountEU().doubleValue());
		     	        amountAccountingPhysical=CommonsUtilities.addTwoDecimal(amountAccountingPhysical, accountingAccount.getAmountToBOB(), 4);
	        			
	        		}
	        		
	        		
	        	}else if(AccountingConciliationType.SECURITIES_OUTSTANDING_PLACE.getCode().equals(accountingAccount.getPortFolio())){
	        		//25
	        		if(InstrumentType.FIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
		     	        addContentAndStyle(row25.createCell(8),getStyleTitle(), accountingAccount.getAmountBOB().doubleValue());
		     	        addContentAndStyle(row25.createCell(9),getStyleTitle(), accountingAccount.getAmountUSD().doubleValue());
		     	        addContentAndStyle(row25.createCell(10),getStyleTitle(), accountingAccount.getAmountMVL().doubleValue());
		     	        addContentAndStyle(row25.createCell(11),getStyleTitle(), accountingAccount.getAmountUFV().doubleValue());
		     	        addContentAndStyle(row25.createCell(12),getStyleTitle(), accountingAccount.getAmountEU().doubleValue());
		     	        amountAccountingNotPlaced=CommonsUtilities.addTwoDecimal(amountAccountingNotPlaced, accountingAccount.getAmountToBOB(), 4);
	        		//26
	        		}else if(InstrumentType.VARIABLE_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
		     	        addContentAndStyle(row26.createCell(8),getStyleTitle(), accountingAccount.getAmountBOB().doubleValue());
		     	        addContentAndStyle(row26.createCell(9),getStyleTitle(), accountingAccount.getAmountUSD().doubleValue());
		     	        addContentAndStyle(row26.createCell(10),getStyleTitle(), accountingAccount.getAmountMVL().doubleValue());
		     	        addContentAndStyle(row26.createCell(11),getStyleTitle(), accountingAccount.getAmountUFV().doubleValue());
		     	        addContentAndStyle(row26.createCell(12),getStyleTitle(), accountingAccount.getAmountEU().doubleValue());
		     	        amountAccountingNotPlaced=CommonsUtilities.addTwoDecimal(amountAccountingNotPlaced, accountingAccount.getAmountToBOB(), 4);
	        		//27
	        		}else if(InstrumentType.MIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
		     	        addContentAndStyle(row27.createCell(8),getStyleTitle(), accountingAccount.getAmountBOB().doubleValue());
		     	        addContentAndStyle(row27.createCell(9),getStyleTitle(), accountingAccount.getAmountUSD().doubleValue());
		     	        addContentAndStyle(row27.createCell(10),getStyleTitle(), accountingAccount.getAmountMVL().doubleValue());
		     	        addContentAndStyle(row27.createCell(11),getStyleTitle(), accountingAccount.getAmountUFV().doubleValue());
		     	        addContentAndStyle(row27.createCell(12),getStyleTitle(), accountingAccount.getAmountEU().doubleValue());
		     	        amountAccountingNotPlaced=CommonsUtilities.addTwoDecimal(amountAccountingNotPlaced, accountingAccount.getAmountToBOB(), 4);
	        			
	        		}
	        		
	        	}
			}
	        
	        /**
	         * Sum To Total Amount Accounting
	         */
	        totalAmountAccounting=CommonsUtilities.addTwoDecimal(totalAmountAccounting, amountAccountingAvailable, 4);
	        totalAmountAccounting=CommonsUtilities.addTwoDecimal(totalAmountAccounting, amountAccountingReporting, 4);
	        totalAmountAccounting=CommonsUtilities.addTwoDecimal(totalAmountAccounting, amountAccountingBlock, 4);
	        totalAmountAccounting=CommonsUtilities.addTwoDecimal(totalAmountAccounting, amountAccountingOtherBlock, 4);
	        totalAmountAccounting=CommonsUtilities.addTwoDecimal(totalAmountAccounting, amountAccountingPhysical, 4);
	        totalAmountAccounting=CommonsUtilities.addTwoDecimal(totalAmountAccounting, amountAccountingNotPlaced, 4);
	        
	        
	        /**
	         * Total To Bs Accounting
	         */
	        addContentAndStyle(row11.createCell(13),getStyleHeaderTitleBoldBackBlueTwelve(), amountAccountingAvailable.doubleValue());
	        addContentAndStyle(row14.createCell(13),getStyleHeaderTitleBoldBackBlueTwelve(), amountAccountingReporting.doubleValue());
	        addContentAndStyle(row17.createCell(13),getStyleHeaderTitleBoldBackBlueTwelve(), amountAccountingBlock.doubleValue());
	        addContentAndStyle(row20.createCell(13),getStyleHeaderTitleBoldBackBlueTwelve(), amountAccountingOtherBlock.doubleValue());
	        addContentAndStyle(row23.createCell(13),getStyleHeaderTitleBoldBackBlueTwelve(), amountAccountingPhysical.doubleValue());
	        addContentAndStyle(row25.createCell(13),getStyleHeaderTitleBoldBackBlueTwelve(), amountAccountingNotPlaced.doubleValue());
	        
	        addContentAndStyle(row28.createCell(5),getStyleTitle(), ReportConstant.LABEL_TOTAL);
	        addContentAndStyle(row28.createCell(13),getStyleHeaderTitleBoldBackBlueTwelve(), totalAmountAccounting.doubleValue());
	        
	        /**Diferencia*/
	        
	        amountRemainderAvailable=CommonsUtilities.subtractTwoDecimal(amountOperAvailable, amountAccountingAvailable, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        totalRemainder=CommonsUtilities.addTwoDecimal(totalRemainder, amountRemainderAvailable, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO );
	        amountRemainderReporting=CommonsUtilities.subtractTwoDecimal(amountOperReporting, amountAccountingReporting, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        totalRemainder=CommonsUtilities.addTwoDecimal(totalRemainder, amountRemainderReporting, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        amountRemainderBlock=CommonsUtilities.subtractTwoDecimal(amountOperBlock, amountAccountingBlock, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        totalRemainder=CommonsUtilities.addTwoDecimal(totalRemainder, amountRemainderBlock, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        amountRemainderOtherBlock=CommonsUtilities.subtractTwoDecimal(amountOperOtherBlock, amountAccountingOtherBlock, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        totalRemainder=CommonsUtilities.addTwoDecimal(totalRemainder, amountRemainderOtherBlock, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        amountRemainderPhysical=CommonsUtilities.subtractTwoDecimal(amountOperPhysical, amountAccountingPhysical, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        totalRemainder=CommonsUtilities.addTwoDecimal(totalRemainder, amountRemainderPhysical, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        amountRemainderNotPlaced=CommonsUtilities.subtractTwoDecimal(amountOperNotPlaced, amountAccountingNotPlaced, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        totalRemainder=CommonsUtilities.addTwoDecimal(totalRemainder, amountRemainderNotPlaced, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        
	        addContentAndStyle(row11.createCell(14),getStyleHeaderTitleBoldBackBlueTwelve(), amountRemainderAvailable.doubleValue());
	        addContentAndStyle(row14.createCell(14),getStyleHeaderTitleBoldBackBlueTwelve(), amountRemainderReporting.doubleValue());
	        addContentAndStyle(row17.createCell(14),getStyleHeaderTitleBoldBackBlueTwelve(), amountRemainderBlock.doubleValue());
	        addContentAndStyle(row20.createCell(14),getStyleHeaderTitleBoldBackBlueTwelve(), amountRemainderOtherBlock.doubleValue());
	        addContentAndStyle(row23.createCell(14),getStyleHeaderTitleBoldBackBlueTwelve(), amountRemainderPhysical.doubleValue());
	        addContentAndStyle(row25.createCell(14),getStyleHeaderTitleBoldBackBlueTwelve(), amountRemainderNotPlaced.doubleValue());
	        addContentAndStyle(row28.createCell(14),getStyleHeaderTitleBoldBackBlueTwelve(), totalRemainder.doubleValue());

	        
	        /**
	         * Merges Cells
	         */
	        mergeCell(sheet,  0,  0, 1, 10); //TITLE
	        mergeCell(sheet,  8,  8, 0, 14);//CUADRE CONTABLE
	        mergeCell(sheet,  9,  9, 1,  6); //Saldos en Cartera
	        mergeCell(sheet,  9,  9, 7, 13);//Saldos Contables

	        mergeCell(sheet, 11, 13, 0, 0);//LABEL Custodia de Valores Anotados en Cuenta
	        mergeCell(sheet, 11, 13, 1, 1);
	        mergeCell(sheet, 11, 13, 2, 2);
	        mergeCell(sheet, 11, 13, 3, 3);
	        mergeCell(sheet, 11, 13, 4, 4);
	        mergeCell(sheet, 11, 13, 5, 5);
	        mergeCell(sheet, 11, 13, 6, 6);

	        mergeCell(sheet, 14, 16, 0, 0);//LABEL Valores Desmaterializados Dados en Reporto
	        mergeCell(sheet, 14, 16, 1, 1);
	        mergeCell(sheet, 14, 16, 2, 2);
	        mergeCell(sheet, 14, 16, 3, 3);
	        mergeCell(sheet, 14, 16, 4, 4);
	        mergeCell(sheet, 14, 16, 5, 5);
	        mergeCell(sheet, 14, 16, 6, 6);

	        mergeCell(sheet, 17, 19, 0, 0);//LABEL Valores Desmaterializados Restringidos
	        mergeCell(sheet, 17, 19, 1, 1);
	        mergeCell(sheet, 17, 19, 2, 2);
	        mergeCell(sheet, 17, 19, 3, 3);
	        mergeCell(sheet, 17, 19, 4, 4);
	        mergeCell(sheet, 17, 19, 5, 5);
	        mergeCell(sheet, 17, 19, 6, 6);

	        mergeCell(sheet, 20, 22, 0, 0);//LABEL Valores Desmaterializados con Otras Restringiones
	        mergeCell(sheet, 20, 22, 1, 1);
	        mergeCell(sheet, 20, 22, 2, 2);
	        mergeCell(sheet, 20, 22, 3, 3);
	        mergeCell(sheet, 20, 22, 4, 4);
	        mergeCell(sheet, 20, 22, 5, 5);
	        mergeCell(sheet, 20, 22, 6, 6);

	        mergeCell(sheet, 23, 24, 0, 0);//LABEL Custodia de Valores Fisicos
	        mergeCell(sheet, 23, 24, 1, 1);
	        mergeCell(sheet, 23, 24, 2, 2);
	        mergeCell(sheet, 23, 24, 3, 3);
	        mergeCell(sheet, 23, 24, 4, 4);
	        mergeCell(sheet, 23, 24, 5, 5);
	        mergeCell(sheet, 23, 24, 6, 6);

	        mergeCell(sheet, 25, 27, 0, 0);//LABEL Valores Pendientes de Colocacion
	        mergeCell(sheet, 25, 27, 1, 1);
	        mergeCell(sheet, 25, 27, 2, 2);
	        mergeCell(sheet, 25, 27, 3, 3);
	        mergeCell(sheet, 25, 27, 4, 4);
	        mergeCell(sheet, 25, 27, 5, 5);
	        mergeCell(sheet, 25, 27, 6, 6);

	        //Total BS Accounting
	        mergeCell(sheet, 11, 13, 13, 13);
	        mergeCell(sheet, 14, 16, 13, 13);
	        mergeCell(sheet, 17, 19, 13, 13);
	        mergeCell(sheet, 20, 22, 13, 13);
	        mergeCell(sheet, 23, 24, 13, 13);
	        mergeCell(sheet, 25, 27, 13, 13);

	        //Difference Operations vs Accounting
	        mergeCell(sheet, 11, 13, 14, 14);
	        mergeCell(sheet, 14, 16, 14, 14);
	        mergeCell(sheet, 17, 19, 14, 14);
	        mergeCell(sheet, 20, 22, 14, 14);
	        mergeCell(sheet, 23, 24, 14, 14);
	        mergeCell(sheet, 25, 27, 14, 14);
	        
	    	
	        /**adjustColumns**/
	        adjustColumns(sheet,11);
	        //Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_SQUARING);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	         
	       	hSSFWorkbook.write(bos);
	        
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        
	        hSSFWorkbook.write(fileOut);
	        
	        fileOut.close();
	    }
	    catch(IOException e)
	    {
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
    return arrayByte;
	}
	
	
	
	protected SXSSFWorkbook book ;
	//protected XSSFWorkbook book ;
	
	/**
	 * Client PortFolio Report
	 * @param parameters
	 * @param reportLogger
	 * @return
	 */
	public byte[] writeFileExcelClientPortfolio2(Map<String, Object> parameters , ReportLogger reportLogger  ){
		
		byte[] arrayByte=null;

		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("str_query");

		initCellStyle();
		initFont();
		initDataFormat();
		book = new SXSSFWorkbook(); 
		
		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007;
						
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						
						
						createSheetPortFolio2(book,queryMain.subList(i, ifinal),CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),"HHmmssSSSSS"));
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{
	        		//Solo un HSSFSheet
	        		createSheetPortFolio2(book,queryMain,CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),"HHmmssSSSSS"));
	        	}
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_CLIENT_PORTFOLIO);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        
	        bos.writeTo(fileOut);
	         
	       	book.write(bos);
	        
	        arrayByte = bos.toByteArray();
	        
	      /*  book.write(fileOut);*/
	       // bos.close();
	        fileOut.close();
	        
			
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
		return arrayByte;
	}
	
	
	
	
	/**
	 * 
	 * @param hSSFWorkbookS
	 * @param queryMain
	 */
	public void createSheetPortFolio2(SXSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, String number){
		
		
		int INDEX_PARTICIPANT=0;
		int INDEX_HOLDER_ACCOUNT=1;
		int INDEX_CUI=2;
		int INDEX_SECURITY_CLASS=3;
		int INDEX_SECURITY_CODE=4;
		int INDEX_ISSUER_MNEMONIC=5;
		int INDEX_CURRENCY_MNEMONIC=6;
		int INDEX_ISSUER_DATE=7;
		int INDEX_PLAZO=8;
		int INDEX_DUE_DATE=9;	     // fecha de vencimiento	
		int INDEX_RATE_NOMINAL=10;
		int INDEX_FINAL_RATE=11;
		int INDEX_TOTAL_BALANCE=12;
		int INDEX_AVAILABLE_BALANCE=13;
		int INDEX_REPORTED_BALANCE=14;
		int INDEX_REPORTING_BALANCE=15;
		int INDEX_PAWN_BALANCE=16;
		int INDEX_BAN_BALANCE=17;
		int INDEX_OTHER_BLOCK_BALANCE=18;
		int INDEX_ACCREDITATION_BALANCE=19;
		int INDEX_MARKFACT=20;
		int INDEX_RATE=21;
		int INDEX_TOTAL_AMOUNT=22;
		int INDEX_VN_CURRENT=23;
		int INDEX_TOTAL=24;
		final int INDEX_ALTERNATE_CODE=25;	 // codigo alterno		
		final int INDEX_NAME=26;			 // nombre
		final int INDEX_TYPE_OF_PERSON=27;   // tipo de persona
		final int INDEX_CI_NIT=28;			 // nit 0 nit
		final int INDEX_ECONOMIC_ACTIVITY=29;			 // serie
		final int INDEX_SERIE=30;			 // serie
		final int INDEX_INITIAL_NVALUE_USD=31;
		final int INDEX_CURRENT_NVALUE_USD=32;
		final int INDEX_MARKET_PRICE_USD=33;
		
		
		/**Created a new sheet within the workBock**/
		
		SXSSFSheet sheet =  (SXSSFSheet) book.createSheet("ClientsPortFolio"+number);
		
    	//HSSFSheet sheet = hSSFWorkbook.createSheet("ClientsPortFolio"+number);
    	
		/*****
         * BEGIN - REPORT HEADER 
         */
        
		/**
		 * 1corte : Participante
		 * 2corte : Oferta
		 * 3corte : Moneda-Clase valor
		 */
        int indrow=1;


        /**Created Header DEBE - HABER*/
        //SXSSFRow rowHeader =  (SXSSFRow) sheet.createRow((int)indrow);
        Row rowHeader = sheet.createRow((int)indrow);
        //HSSFRow rowHeader = sheet.createRow((int)indrow);
        addContentAndStyle2(rowHeader.createCell(INDEX_PARTICIPANT),getStyleTitleX(),ReportConstant.LABEL_PARTICIPANT);
        addContentAndStyle2(rowHeader.createCell(INDEX_HOLDER_ACCOUNT),getStyleTitleX(),ReportConstant.LABEL_HOLDER_ACCOUNT);
        addContentAndStyle2(rowHeader.createCell(INDEX_HOLDER_ACCOUNT),getStyleTitleX(),ReportConstant.LABEL_HOLDER_ACCOUNT);
        addContentAndStyle2(rowHeader.createCell(INDEX_CUI),getStyleTitleX(),ReportConstant.LABEL_CUI);
        addContentAndStyle2(rowHeader.createCell(INDEX_SECURITY_CLASS),getStyleTitleX(),ReportConstant.LABEL_SECURITY_CLASS);
        addContentAndStyle2(rowHeader.createCell(INDEX_SECURITY_CODE),getStyleTitleX(),ReportConstant.LABEL_SECURITY_CODE);
        addContentAndStyle2(rowHeader.createCell(INDEX_ISSUER_MNEMONIC),getStyleTitleX(),ReportConstant.LABEL_ISSUER);
        addContentAndStyle2(rowHeader.createCell(INDEX_CURRENCY_MNEMONIC),getStyleTitleX(),ReportConstant.LABEL_CURRENCY_MNEMONIC);
        addContentAndStyle2(rowHeader.createCell(INDEX_ISSUER_DATE),getStyleTitleX(),ReportConstant.LABEL_ISSUANCE_DATE);
        addContentAndStyle2(rowHeader.createCell(INDEX_PLAZO),getStyleTitleX(),ReportConstant.LABEL_PLAZO);
        addContentAndStyle2(rowHeader.createCell(INDEX_DUE_DATE),getStyleTitleX(),ReportConstant.LABEL_DUE_DATE);
        addContentAndStyle2(rowHeader.createCell(INDEX_RATE_NOMINAL),getStyleTitleX(),ReportConstant.LABEL_VN);
        addContentAndStyle2(rowHeader.createCell(INDEX_FINAL_RATE),getStyleTitleX(),ReportConstant.LABEL_NOMINAL_RATE);
        addContentAndStyle2(rowHeader.createCell(INDEX_TOTAL_BALANCE),getStyleTitleX(),ReportConstant.LABEL_TOTAL_BALANCE);
        addContentAndStyle2(rowHeader.createCell(INDEX_AVAILABLE_BALANCE),getStyleTitleX(),ReportConstant.LABEL_AVAILABLE_BALANCE);
        addContentAndStyle2(rowHeader.createCell(INDEX_REPORTED_BALANCE),getStyleTitleX(),ReportConstant.LABEL_REPORTED_BALANCE);
        addContentAndStyle2(rowHeader.createCell(INDEX_REPORTING_BALANCE),getStyleTitleX(),ReportConstant.LABEL_REPORTING_BALANCE);
        addContentAndStyle2(rowHeader.createCell(INDEX_PAWN_BALANCE),getStyleTitleX(),ReportConstant.LABEL_PAWN_BALANCE);
        addContentAndStyle2(rowHeader.createCell(INDEX_BAN_BALANCE),getStyleTitleX(),ReportConstant.LABEL_BAN_BALANCE);
        addContentAndStyle2(rowHeader.createCell(INDEX_OTHER_BLOCK_BALANCE),getStyleTitleX(),ReportConstant.LABEL_OTHER_BLOCK_BALANCE);
        addContentAndStyle2(rowHeader.createCell(INDEX_ACCREDITATION_BALANCE),getStyleTitleX(),ReportConstant.LABEL_ACCREDITATION_BALANCE);
        addContentAndStyle2(rowHeader.createCell(INDEX_MARKFACT),getStyleTitleX(),ReportConstant.LABEL_MARKFACT);
        addContentAndStyle2(rowHeader.createCell(INDEX_RATE),getStyleTitleX(),ReportConstant.LABEL_RATE_PERCENT);
        addContentAndStyle2(rowHeader.createCell(INDEX_TOTAL_AMOUNT),getStyleTitleX(),ReportConstant.LABEL_TOTAL_AMOUNT);
        addContentAndStyle2(rowHeader.createCell(INDEX_VN_CURRENT),getStyleTitleX(),ReportConstant.LABEL_CURRENT_NV);
        addContentAndStyle2(rowHeader.createCell(INDEX_TOTAL),getStyleTitleX(),ReportConstant.LABEL_TOTAL_CURRENT_NV);
        	//1. COD_ALTER2NO,2. NOMBRE, 3. FECHA DE VENCIMIENTO,4. TIPO_PERSONA,5. CI_NIT,6. SERIE,7. PRIMARIO/SECUNDARIO			
        addContentAndStyle2(rowHeader.createCell(INDEX_ALTERNATE_CODE),getStyleTitleX(),ReportConstant.LABEL_ALTERNATE_CODE);
        addContentAndStyle2(rowHeader.createCell(INDEX_NAME),getStyleTitleX(),ReportConstant.LABEL_NAME);
        addContentAndStyle2(rowHeader.createCell(INDEX_TYPE_OF_PERSON),getStyleTitleX(),ReportConstant.LABEL_TYPE_OF_PERSON);
        addContentAndStyle2(rowHeader.createCell(INDEX_CI_NIT),getStyleTitleX(),ReportConstant.LABEL_CI_NIT);
        addContentAndStyle2(rowHeader.createCell(INDEX_ECONOMIC_ACTIVITY),getStyleTitleX(),ReportConstant.LABEL_ECONOMIC_ACTIVITY);
        addContentAndStyle2(rowHeader.createCell(INDEX_SERIE),getStyleTitleX(),ReportConstant.LABEL_SERIE);
        addContentAndStyle2(rowHeader.createCell(INDEX_INITIAL_NVALUE_USD),getStyleTitleX(),ReportConstant.LABEL_INITIAL_NVALUE_USD);
        addContentAndStyle2(rowHeader.createCell(INDEX_CURRENT_NVALUE_USD),getStyleTitleX(),ReportConstant.LABEL_CURRENT_NVALUE_USD);
        addContentAndStyle2(rowHeader.createCell(INDEX_MARKET_PRICE_USD),getStyleTitleX(),ReportConstant.LABEL_MARKET_PRICE_USD);
        
                
        for (Object[] query : queryMain) {
        	
        	
        	String participant=query[2]!=null?String.valueOf(query[2]):"";
        	String holderAccount=query[10]!=null?String.valueOf(query[10]):"";
        	String cui= query[8]!=null?String.valueOf(query[8]):"";
        	String securityClass= query[12]!=null?String.valueOf(query[12]):"";
        	String securityCode=query[13]!=null?String.valueOf(query[13]):"";
        	String issuerMnemonic=query[5]!=null?String.valueOf(query[5]):"";
        	String currencyMnemonic=query[4]!=null?String.valueOf(query[4]):"";
        	String issuanceDate=query[14]!=null?String.valueOf(query[14]):"";
        	Double plazo=query[15]!=null ? Double.parseDouble(query[15].toString()) : Double.parseDouble("0");;
        	String expirationDate=query[43]!=null?String.valueOf(query[43]):"";
        	BigDecimal rateNominal=query[16]!=null ? new BigDecimal(query[16].toString()) : BigDecimal.ZERO;
        	BigDecimal finalRate=query[17]!=null ? new BigDecimal(query[17].toString()) : BigDecimal.ZERO;
        	Double totalBalance=query[22]!=null ? Double.parseDouble(query[22].toString()) : Double.parseDouble("0");;
        	Double availableBalance=query[23]!=null ? Double.parseDouble(query[23].toString()) : Double.parseDouble("0");;
        	Double reportedBalance = query[29]!=null ? Double.parseDouble(query[29].toString()) : Double.parseDouble("0");;
        	Double reportingBalance = query[30]!=null ? Double.parseDouble(query[30].toString()) : Double.parseDouble("0");;
        	Double pawnBalance = query[24]!=null ? Double.parseDouble(query[24].toString()) : Double.parseDouble("0");;
        	Double banBalance = query[25]!=null ? Double.parseDouble(query[25].toString()) : Double.parseDouble("0");;
        	Double otherBlockBalance = query[26]!=null ? Double.parseDouble(query[26].toString()) : Double.parseDouble("0");;
        	Double accreditationBalance = query[27]!=null ? Double.parseDouble(query[27].toString()) : Double.parseDouble("0");;
        	BigDecimal markFact =query[35]!=null ? new BigDecimal(query[35].toString()) : BigDecimal.ZERO;
        	BigDecimal rate =query[34]!=null ? new BigDecimal(query[34].toString()) : BigDecimal.ZERO;
        	BigDecimal totalAmount = query[36]!=null ? new BigDecimal(query[36].toString()) : BigDecimal.ZERO;
        	BigDecimal currentNominalValue = query[40]!=null ? new BigDecimal(query[40].toString()) : BigDecimal.ZERO;
        	BigDecimal totalToCurrentNominalValue = query[41]!=null ? new BigDecimal(query[41].toString()) : BigDecimal.ZERO;
        	
        	String alternateCode=query[42]!=null?String.valueOf(query[42]):"";
        	String holders= query[9]!=null?String.valueOf(query[9]):"";
        	String holderType=query[44]!=null?String.valueOf(query[44]):"";
        	String nitCi=query[45]!=null?String.valueOf(query[45]):"";
        	
        	String economicActivity=query[47]!=null?String.valueOf(query[47]):"";
        	
        	String serie=query[46]!=null?String.valueOf(query[46]):"";
        	
        	Double initialNValueUSD = query[48]!=null ? Double.parseDouble(query[48].toString()) : Double.parseDouble("0");;
        	Double currentNValueUSD = query[49]!=null ? Double.parseDouble(query[49].toString()) : Double.parseDouble("0");;
        	Double marketPriceUSD = query[50]!=null ? Double.parseDouble(query[50].toString()) : Double.parseDouble("0");;
        	
	        indrow++;

        	
	        /**
	         * La data 
	         */
	        //HSSFRow rowData = sheet.createRow((int)indrow);
	        Row rowData = sheet.createRow((int)indrow);
	        
	        addContentAndStyle2(rowData.createCell(INDEX_PARTICIPANT),getStyleAccountingAccountX(), participant);
	        addContentAndStyle2(rowData.createCell(INDEX_HOLDER_ACCOUNT),getStyleAccountingAccountX(), holderAccount);
	        addContentAndStyle2(rowData.createCell(INDEX_CUI),getStyleAccountingAccountX(), cui);
	        addContentAndStyle2(rowData.createCell(INDEX_SECURITY_CLASS),getStyleAccountingAccountX(), securityClass);
	        addContentAndStyle2(rowData.createCell(INDEX_SECURITY_CODE),getStyleAccountingAccountX(), securityCode);
	        addContentAndStyle2(rowData.createCell(INDEX_ISSUER_MNEMONIC),getStyleAccountingAccountX(), issuerMnemonic);
	        addContentAndStyle2(rowData.createCell(INDEX_CURRENCY_MNEMONIC),getStyleAccountingAccountX(), currencyMnemonic);
	        addContentAndStyle2(rowData.createCell(INDEX_ISSUER_DATE),getStyleAccountingAccountX(), issuanceDate);
	        addContentAndStyle2D(rowData.createCell(INDEX_PLAZO),getStyleAccountingAccountX(), plazo);
	        addContentAndStyle2(rowData.createCell(INDEX_DUE_DATE),getStyleAccountingAccountX(), expirationDate);
	        addContentAndStyle2D(rowData.createCell(INDEX_RATE_NOMINAL),getStyleAccountingAccountX(), rateNominal.doubleValue());//CommonsUtilities.getFormatOfAmount(rateNominal, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        addContentAndStyle2D(rowData.createCell(INDEX_FINAL_RATE),getStyleAccountingAccountX(), finalRate.doubleValue());
	        addContentAndStyle2D(rowData.createCell(INDEX_TOTAL_BALANCE),getStyleAccountingAccountX(), totalBalance );
	        addContentAndStyle2D(rowData.createCell(INDEX_AVAILABLE_BALANCE),getStyleAccountingAccountX(), availableBalance);
	        addContentAndStyle2D(rowData.createCell(INDEX_REPORTED_BALANCE),getStyleAccountingAccountX(), reportedBalance);
	        addContentAndStyle2D(rowData.createCell(INDEX_REPORTING_BALANCE),getStyleAccountingAccountX(), reportingBalance);
	        addContentAndStyle2D(rowData.createCell(INDEX_PAWN_BALANCE),getStyleAccountingAccountX(), pawnBalance);
	        addContentAndStyle2D(rowData.createCell(INDEX_BAN_BALANCE),getStyleAccountingAccountX(), banBalance);
	        addContentAndStyle2D(rowData.createCell(INDEX_OTHER_BLOCK_BALANCE),getStyleAccountingAccountX(), otherBlockBalance);
	        addContentAndStyle2D(rowData.createCell(INDEX_ACCREDITATION_BALANCE),getStyleAccountingAccountX(), accreditationBalance);
	        addContentAndStyle2D(rowData.createCell(INDEX_MARKFACT),getStyleAccountingAccountX(), markFact.doubleValue());
	        addContentAndStyle2D(rowData.createCell(INDEX_RATE),getStyleAccountingAccountX(), rate.doubleValue());
	        addContentAndStyle2D(rowData.createCell(INDEX_TOTAL_AMOUNT),getStyleAccountingAccountX(), totalAmount.doubleValue());
	        addContentAndStyle2D(rowData.createCell(INDEX_VN_CURRENT),getStyleAccountingAccountX(), currentNominalValue.doubleValue());
	        addContentAndStyle2D(rowData.createCell(INDEX_TOTAL),getStyleAccountingAccountX(), totalToCurrentNominalValue.doubleValue());
	        addContentAndStyle2(rowData.createCell(INDEX_ALTERNATE_CODE),getStyleAccountingAccountX(), alternateCode);
	        addContentAndStyle2(rowData.createCell(INDEX_NAME),getStyleAccountingAccountX(), holders);
	        addContentAndStyle2(rowData.createCell(INDEX_TYPE_OF_PERSON),getStyleAccountingAccountX(), holderType);
	        addContentAndStyle2(rowData.createCell(INDEX_CI_NIT),getStyleAccountingAccountX(), nitCi);
	        addContentAndStyle2(rowData.createCell(INDEX_SERIE),getStyleAccountingAccountX(), serie);
	        
	        addContentAndStyle2(rowData.createCell(INDEX_ECONOMIC_ACTIVITY),getStyleAccountingAccountX(), economicActivity);
	        addContentAndStyle2D(rowData.createCell(INDEX_INITIAL_NVALUE_USD),getStyleAccountingAccountX(), initialNValueUSD);
	        addContentAndStyle2D(rowData.createCell(INDEX_CURRENT_NVALUE_USD),getStyleAccountingAccountX(), currentNValueUSD);
	        addContentAndStyle2D(rowData.createCell(INDEX_MARKET_PRICE_USD),getStyleAccountingAccountX(), marketPriceUSD);
	        
		}
        
        /**adjustColumns**/
        //adjustColumns(sheet,15);

	}
	

	
	/**
	 * Client PortFolio Report
	 * @param parameters
	 * @param reportLogger
	 * @return
	 */
	public byte[] writeFileExcelClientPortfolio(Map<String, Object> parameters , ReportLogger reportLogger  ){
		
		byte[] arrayByte=null;

		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("str_query");

		initCellStyle();
		initFont();
		initDataFormat();
		
		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007;
						
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						
						
						createSheetPortFolio(hSSFWorkbook,queryMain.subList(i, ifinal),CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),"HHmmssSSSSS"));
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{
	        		//Solo un HSSFSheet
	        		createSheetPortFolio(hSSFWorkbook,queryMain,CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),"HHmmssSSSSS"));
	        	}
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_CLIENT_PORTFOLIO);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	         
	       	hSSFWorkbook.write(bos);
	        
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        
	        hSSFWorkbook.write(fileOut);
	        
	        fileOut.close();
			
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
		return arrayByte;
	}
	
	
	
	
	/**
	 * 
	 * @param hSSFWorkbook
	 * @param queryMain
	 */
	public void createSheetPortFolio(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, String number){
		
		int INDEX_CUT_DATE=0;
		int INDEX_PARTICIPANT=1;
		int INDEX_HOLDER_ACCOUNT=2;
		int INDEX_CUI=3;
		int INDEX_SECURITY_CLASS=4;
		int INDEX_SECURITY_CODE=5;
		int INDEX_ISSUER_MNEMONIC=6;
		int INDEX_CURRENCY_MNEMONIC=7;
		int INDEX_ISSUER_DATE=8;
		int INDEX_PLAZO=9;
		int INDEX_DUE_DATE=10;	     // fecha de vencimiento	
		int INDEX_RATE_NOMINAL=11;
		int INDEX_FINAL_RATE=12;
		int INDEX_TOTAL_BALANCE=13;
		int INDEX_AVAILABLE_BALANCE=14;
		int INDEX_REPORTED_BALANCE=15;
		int INDEX_REPORTING_BALANCE=16;
		int INDEX_PAWN_BALANCE=17;
		int INDEX_BAN_BALANCE=18;
		int INDEX_OTHER_BLOCK_BALANCE=19;
		int INDEX_ACCREDITATION_BALANCE=20;
		int INDEX_MARKFACT=21;
		int INDEX_RATE=22;
		int INDEX_TOTAL_AMOUNT=23;
		int INDEX_VN_CURRENT=24;
		int INDEX_TOTAL=25;
		final int INDEX_ALTERNATE_CODE=26;	 // codigo alterno		
		final int INDEX_NAME=27;			 // nombre
		final int INDEX_TYPE_OF_PERSON=28;   // tipo de persona
		final int INDEX_CI_NIT=29;			 // nit 0 nit
		final int INDEX_ECONOMIC_ACTIVITY=30;			 // serie
		final int INDEX_INVESTOR_TYPE=31;
		final int INDEX_SERIE=32;			 // serie
		final int INDEX_INITIAL_NVALUE_USD=33;
		final int INDEX_CURRENT_NVALUE_USD=34;
		final int INDEX_MARKET_PRICE_USD=35;
		
		
		
		/**Created a new sheet within the workBock**/
    	HSSFSheet sheet = hSSFWorkbook.createSheet("ClientsPortFolio"+number);
    	
		/*****
         * BEGIN - REPORT HEADER 
         */
        
		/**
		 * 1corte : Participante
		 * 2corte : Oferta
		 * 3corte : Moneda-Clase valor
		 */
        int indrow=1;


        /**Created Header DEBE - HABER*/
        HSSFRow rowHeader = sheet.createRow((int)indrow);
        addContentAndStyle(rowHeader.createCell(INDEX_PARTICIPANT),getStyleTitle(),ReportConstant.LABEL_PARTICIPANT);
        addContentAndStyle(rowHeader.createCell(INDEX_HOLDER_ACCOUNT),getStyleTitle(),ReportConstant.LABEL_HOLDER_ACCOUNT);
        addContentAndStyle(rowHeader.createCell(INDEX_HOLDER_ACCOUNT),getStyleTitle(),ReportConstant.LABEL_HOLDER_ACCOUNT);
        addContentAndStyle(rowHeader.createCell(INDEX_CUI),getStyleTitle(),ReportConstant.LABEL_CUI);
        addContentAndStyle(rowHeader.createCell(INDEX_SECURITY_CLASS),getStyleTitle(),ReportConstant.LABEL_SECURITY_CLASS);
        addContentAndStyle(rowHeader.createCell(INDEX_SECURITY_CODE),getStyleTitle(),ReportConstant.LABEL_SECURITY_CODE);
        addContentAndStyle(rowHeader.createCell(INDEX_ISSUER_MNEMONIC),getStyleTitle(),ReportConstant.LABEL_ISSUER);
        addContentAndStyle(rowHeader.createCell(INDEX_CURRENCY_MNEMONIC),getStyleTitle(),ReportConstant.LABEL_CURRENCY_MNEMONIC);
        addContentAndStyle(rowHeader.createCell(INDEX_ISSUER_DATE),getStyleTitle(),ReportConstant.LABEL_ISSUANCE_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_PLAZO),getStyleTitle(),ReportConstant.LABEL_PLAZO);
        addContentAndStyle(rowHeader.createCell(INDEX_DUE_DATE),getStyleTitle(),ReportConstant.LABEL_DUE_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_RATE_NOMINAL),getStyleTitle(),ReportConstant.LABEL_VN);
        addContentAndStyle(rowHeader.createCell(INDEX_FINAL_RATE),getStyleTitle(),ReportConstant.LABEL_NOMINAL_RATE);
        addContentAndStyle(rowHeader.createCell(INDEX_TOTAL_BALANCE),getStyleTitle(),ReportConstant.LABEL_TOTAL_BALANCE);
        addContentAndStyle(rowHeader.createCell(INDEX_AVAILABLE_BALANCE),getStyleTitle(),ReportConstant.LABEL_AVAILABLE_BALANCE);
        addContentAndStyle(rowHeader.createCell(INDEX_REPORTED_BALANCE),getStyleTitle(),ReportConstant.LABEL_REPORTING_BALANCE);
        addContentAndStyle(rowHeader.createCell(INDEX_REPORTING_BALANCE),getStyleTitle(),ReportConstant.LABEL_REPORTED_BALANCE);
        addContentAndStyle(rowHeader.createCell(INDEX_PAWN_BALANCE),getStyleTitle(),ReportConstant.LABEL_PAWN_BALANCE);
        addContentAndStyle(rowHeader.createCell(INDEX_BAN_BALANCE),getStyleTitle(),ReportConstant.LABEL_BAN_BALANCE);
        addContentAndStyle(rowHeader.createCell(INDEX_OTHER_BLOCK_BALANCE),getStyleTitle(),ReportConstant.LABEL_OTHER_BLOCK_BALANCE);
        addContentAndStyle(rowHeader.createCell(INDEX_ACCREDITATION_BALANCE),getStyleTitle(),ReportConstant.LABEL_ACCREDITATION_BALANCE);
        addContentAndStyle(rowHeader.createCell(INDEX_MARKFACT),getStyleTitle(),ReportConstant.LABEL_MARKFACT);
        addContentAndStyle(rowHeader.createCell(INDEX_RATE),getStyleTitle(),ReportConstant.LABEL_RATE_PERCENT);
        addContentAndStyle(rowHeader.createCell(INDEX_TOTAL_AMOUNT),getStyleTitle(),ReportConstant.LABEL_TOTAL_AMOUNT);
        addContentAndStyle(rowHeader.createCell(INDEX_VN_CURRENT),getStyleTitle(),ReportConstant.LABEL_CURRENT_NV);
        addContentAndStyle(rowHeader.createCell(INDEX_TOTAL),getStyleTitle(),ReportConstant.LABEL_TOTAL_CURRENT_NV);
        	//1. COD_ALTERNO,2. NOMBRE, 3. FECHA DE VENCIMIENTO,4. TIPO_PERSONA,5. CI_NIT,6. SERIE,7. PRIMARIO/SECUNDARIO			
        addContentAndStyle(rowHeader.createCell(INDEX_ALTERNATE_CODE),getStyleTitle(),ReportConstant.LABEL_ALTERNATE_CODE);
        addContentAndStyle(rowHeader.createCell(INDEX_NAME),getStyleTitle(),ReportConstant.LABEL_NAME);
        addContentAndStyle(rowHeader.createCell(INDEX_TYPE_OF_PERSON),getStyleTitle(),ReportConstant.LABEL_TYPE_OF_PERSON);
        addContentAndStyle(rowHeader.createCell(INDEX_CI_NIT),getStyleTitle(),ReportConstant.LABEL_CI_NIT);
        addContentAndStyle(rowHeader.createCell(INDEX_ECONOMIC_ACTIVITY),getStyleTitle(),ReportConstant.LABEL_ECONOMIC_ACTIVITY);
        addContentAndStyle(rowHeader.createCell(INDEX_SERIE),getStyleTitle(),ReportConstant.LABEL_SERIE);
        addContentAndStyle(rowHeader.createCell(INDEX_INITIAL_NVALUE_USD),getStyleTitle(),ReportConstant.LABEL_INITIAL_NVALUE_USD);
        addContentAndStyle(rowHeader.createCell(INDEX_CURRENT_NVALUE_USD),getStyleTitle(),ReportConstant.LABEL_CURRENT_NVALUE_USD);
        addContentAndStyle(rowHeader.createCell(INDEX_MARKET_PRICE_USD),getStyleTitle(),ReportConstant.LABEL_MARKET_PRICE_USD);

        addContentAndStyle(rowHeader.createCell(INDEX_CUT_DATE),getStyleTitle(),ReportConstant.LABEL_CUT_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_INVESTOR_TYPE),getStyleTitle(),ReportConstant.LABEL_INVESTOR_TYPE);
        
                
        for (Object[] query : queryMain) {
        	
        	
        	String participant=query[2]!=null?String.valueOf(query[2]):"";
        	String holderAccount=query[10]!=null?String.valueOf(query[10]):"";
        	String cui= query[8]!=null?String.valueOf(query[8]):"";
        	String securityClass= query[12]!=null?String.valueOf(query[12]):"";
        	String securityCode=query[13]!=null?String.valueOf(query[13]):"";
        	String issuerMnemonic=query[5]!=null?String.valueOf(query[5]):"";
        	String currencyMnemonic=query[4]!=null?String.valueOf(query[4]):"";
        	String issuanceDate=query[14]!=null?String.valueOf(query[14]):"";
        	Double plazo=query[15]!=null ? Double.parseDouble(query[15].toString()) : Double.parseDouble("0");;
        	String expirationDate=query[43]!=null?String.valueOf(query[43]):"";
        	BigDecimal rateNominal=query[16]!=null ? new BigDecimal(query[16].toString()) : BigDecimal.ZERO;
        	BigDecimal finalRate=query[17]!=null ? new BigDecimal(query[17].toString()) : BigDecimal.ZERO;
        	Double totalBalance=query[22]!=null ? Double.parseDouble(query[22].toString()) : Double.parseDouble("0");;
        	Double availableBalance=query[23]!=null ? Double.parseDouble(query[23].toString()) : Double.parseDouble("0");;
        	Double reportedBalance = query[29]!=null ? Double.parseDouble(query[29].toString()) : Double.parseDouble("0");;
        	Double reportingBalance = query[30]!=null ? Double.parseDouble(query[30].toString()) : Double.parseDouble("0");;
        	Double pawnBalance = query[24]!=null ? Double.parseDouble(query[24].toString()) : Double.parseDouble("0");;
        	Double banBalance = query[25]!=null ? Double.parseDouble(query[25].toString()) : Double.parseDouble("0");;
        	Double otherBlockBalance = query[26]!=null ? Double.parseDouble(query[26].toString()) : Double.parseDouble("0");;
        	Double accreditationBalance = query[27]!=null ? Double.parseDouble(query[27].toString()) : Double.parseDouble("0");;
        	BigDecimal markFact =query[35]!=null ? new BigDecimal(query[35].toString()) : BigDecimal.ZERO;
        	BigDecimal rate =query[34]!=null ? new BigDecimal(query[34].toString()) : BigDecimal.ZERO;
        	BigDecimal totalAmount = query[36]!=null ? new BigDecimal(query[36].toString()) : BigDecimal.ZERO;
        	BigDecimal currentNominalValue = query[40]!=null ? new BigDecimal(query[40].toString()) : BigDecimal.ZERO;
        	BigDecimal totalToCurrentNominalValue = query[41]!=null ? new BigDecimal(query[41].toString()) : BigDecimal.ZERO;
        	
        	String alternateCode=query[42]!=null?String.valueOf(query[42]):"";
        	String holders= query[9]!=null?String.valueOf(query[9]):"";
        	String holderType=query[44]!=null?String.valueOf(query[44]):"";
        	String nitCi=query[45]!=null?String.valueOf(query[45]):"";
        	
        	String economicActivity=query[47]!=null?String.valueOf(query[47]):"";
        	
        	String serie=query[46]!=null?String.valueOf(query[46]):"";
        	
        	Double initialNValueUSD = query[48]!=null ? Double.parseDouble(query[48].toString()) : Double.parseDouble("0");;
        	Double currentNValueUSD = query[49]!=null ? Double.parseDouble(query[49].toString()) : Double.parseDouble("0");;
        	Double marketPriceUSD = query[50]!=null ? Double.parseDouble(query[50].toString()) : Double.parseDouble("0");;
        	
        	String cutDate=query[51]!=null?String.valueOf(query[51]):"";
        	String investorType=query[52]!=null?String.valueOf(query[52]):"";
        	
	        indrow++;

        	
	        /**
	         * La data 
	         */
	        HSSFRow rowData = sheet.createRow((int)indrow);
	        addContentAndStyle(rowData.createCell(INDEX_PARTICIPANT),getStyleAccountingAccount(), participant);
	        addContentAndStyle(rowData.createCell(INDEX_HOLDER_ACCOUNT),getStyleAccountingAccount(), holderAccount);
	        addContentAndStyle(rowData.createCell(INDEX_CUI),getStyleAccountingAccount(), cui);
	        addContentAndStyle(rowData.createCell(INDEX_SECURITY_CLASS),getStyleAccountingAccount(), securityClass);
	        addContentAndStyle(rowData.createCell(INDEX_SECURITY_CODE),getStyleAccountingAccount(), securityCode);
	        addContentAndStyle(rowData.createCell(INDEX_ISSUER_MNEMONIC),getStyleAccountingAccount(), issuerMnemonic);
	        addContentAndStyle(rowData.createCell(INDEX_CURRENCY_MNEMONIC),getStyleAccountingAccount(), currencyMnemonic);
	        addContentAndStyle(rowData.createCell(INDEX_ISSUER_DATE),getStyleAccountingAccount(), issuanceDate);
	        addContentAndStyle(rowData.createCell(INDEX_PLAZO),getStyleAccountingAccount(), plazo);
	        addContentAndStyle(rowData.createCell(INDEX_DUE_DATE),getStyleAccountingAccount(), expirationDate);
	        addContentAndStyle(rowData.createCell(INDEX_RATE_NOMINAL),getStyleAccountingAccount(), rateNominal.doubleValue());//CommonsUtilities.getFormatOfAmount(rateNominal, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        addContentAndStyle(rowData.createCell(INDEX_FINAL_RATE),getStyleAccountingAccount(), finalRate.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_TOTAL_BALANCE),getStyleAccountingAccount(), totalBalance );
	        addContentAndStyle(rowData.createCell(INDEX_AVAILABLE_BALANCE),getStyleAccountingAccount(), availableBalance);
	        addContentAndStyle(rowData.createCell(INDEX_REPORTED_BALANCE),getStyleAccountingAccount(), reportedBalance);
	        addContentAndStyle(rowData.createCell(INDEX_REPORTING_BALANCE),getStyleAccountingAccount(), reportingBalance);
	        addContentAndStyle(rowData.createCell(INDEX_PAWN_BALANCE),getStyleAccountingAccount(), pawnBalance);
	        addContentAndStyle(rowData.createCell(INDEX_BAN_BALANCE),getStyleAccountingAccount(), banBalance);
	        addContentAndStyle(rowData.createCell(INDEX_OTHER_BLOCK_BALANCE),getStyleAccountingAccount(), otherBlockBalance);
	        addContentAndStyle(rowData.createCell(INDEX_ACCREDITATION_BALANCE),getStyleAccountingAccount(), accreditationBalance);
	        addContentAndStyle(rowData.createCell(INDEX_MARKFACT),getStyleAccountingAccount(), markFact.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_RATE),getStyleAccountingAccount(), rate.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_TOTAL_AMOUNT),getStyleAccountingAccount(), totalAmount.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_VN_CURRENT),getStyleAccountingAccount(), currentNominalValue.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_TOTAL),getStyleAccountingAccount(), totalToCurrentNominalValue.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_ALTERNATE_CODE),getStyleAccountingAccount(), alternateCode);
	        addContentAndStyle(rowData.createCell(INDEX_NAME),getStyleAccountingAccount(), holders);
	        addContentAndStyle(rowData.createCell(INDEX_TYPE_OF_PERSON),getStyleAccountingAccount(), holderType);
	        addContentAndStyle(rowData.createCell(INDEX_CI_NIT),getStyleAccountingAccount(), nitCi);
	        addContentAndStyle(rowData.createCell(INDEX_SERIE),getStyleAccountingAccount(), serie);
	        
	        addContentAndStyle(rowData.createCell(INDEX_ECONOMIC_ACTIVITY),getStyleAccountingAccount(), economicActivity);
	        addContentAndStyle(rowData.createCell(INDEX_INITIAL_NVALUE_USD),getStyleAccountingAccount(), initialNValueUSD);
	        addContentAndStyle(rowData.createCell(INDEX_CURRENT_NVALUE_USD),getStyleAccountingAccount(), currentNValueUSD);
	        addContentAndStyle(rowData.createCell(INDEX_MARKET_PRICE_USD),getStyleAccountingAccount(), marketPriceUSD);

	        addContentAndStyle(rowData.createCell(INDEX_CUT_DATE),getStyleAccountingAccount(), cutDate);
	        addContentAndStyle(rowData.createCell(INDEX_INVESTOR_TYPE),getStyleAccountingAccount(), investorType);
	        
		}
        
        /**adjustColumns**/
        //adjustColumns(sheet,15);

	}
	
	/**
	 * Settled Operations Report / Operaciones Liquidadas
	 * @param parameters
	 * @param reportLogger
	 * @return
	 */
	public byte[] writeFileExcelSettledOperations(Map<String, Object> parameters , ReportLogger reportLogger  ){
		
		byte[] arrayByte=null;
		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("str_query");
		initCellStyle();
		initFont();
		initDataFormat();
		
		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						createSheetSettledOperations(hSSFWorkbook,queryMain.subList(i, ifinal),queryMain.subList(i, ifinal).size());
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{//Solo un HSSFSheet
	        		createSheetSettledOperations(hSSFWorkbook,queryMain,queryMain.size());
	        	}
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_SETTLED_OPERATIONS);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	       	hSSFWorkbook.write(bos);
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        hSSFWorkbook.write(fileOut);
	        fileOut.close();
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
		return arrayByte;
	}
	
	/**
	 * 
	 * @param hSSFWorkbook
	 * @param queryMain
	 */
	public void createSheetSettledOperations(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, int number){
		
		int INDEX_OPERATION_DATE=		0;
		int INDEX_MODALITY=				1;
		int INDEX_OPERATION_PART=		2;
		int INDEX_MNEMONIC_SELLER_PART=	3;
		int INDEX_SELL_HOLDER_ACCOUNT=	4;
		int INDEX_SELL_CUI_NAME = 		5;
		int INDEX_SELL_CUI_ACT_ECO = 	6;
		int INDEX_SELL_CUI_TYPE = 		7;
		int INDEX_MNEMONIC_BUYER_PART=	8;
		int INDEX_BUY_HOLDER_ACCOUNT=	9;
		int INDEX_BUY_CUI_NAME = 		10;
		int INDEX_BUY_CUI_ACT_ECO = 	11;
		int INDEX_BUY_CUI_TYPE = 		12;
		int INDEX_SECURITY_CODE=		13;
		int INDEX_CURRENCY=				14;
		int INDEX_STOCK_QUANTITY=		15;
		int INDEX_OPERATION_PRICE=		16;
		int INDEX_OPERATION_AMOUNT=		17;
		int INDEX_OPERATION_AMOUNT_USD=	18;
		int INDEX_INSTANCIA=			19;
		
		int INDEX_RATE_NEGOTIATION=		20;
		int INDEX_SELAR=				21;
		int INDEX_BALLOT_AND_SEQUENCIAL=22;

		/**Created a new sheet within the workBock**/
    	HSSFSheet sheet = hSSFWorkbook.createSheet("SettledOperations"+number);
    	
		/*****
         * BEGIN - REPORT HEADER 
         */

        int indrow=1;


        /**Created Header DEBE - HABER*/
        HSSFRow rowHeader = sheet.createRow((int)indrow);
        addContentAndStyle(rowHeader.createCell(INDEX_OPERATION_DATE),getStyleTitle(),ReportConstant.LABEL_OPERATION_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_MODALITY),getStyleTitle(),ReportConstant.LABEL_MODALITY);
//        addContentAndStyle(rowHeader.createCell(INDEX_BALLOT_NUMBER),getStyleTitle(),ReportConstant.LABEL_BALLOT_NUMBER);
//        addContentAndStyle(rowHeader.createCell(INDEX_SEQUENTIAL),getStyleTitle(),ReportConstant.LABEL_SEQUENTIAL);
        addContentAndStyle(rowHeader.createCell(INDEX_OPERATION_PART),getStyleTitle(),ReportConstant.LABEL_OPERATION_PART);
        addContentAndStyle(rowHeader.createCell(INDEX_MNEMONIC_SELLER_PART),getStyleTitle(),ReportConstant.LABEL_SELL_PARTICIPANT);
        addContentAndStyle(rowHeader.createCell(INDEX_SELL_HOLDER_ACCOUNT),getStyleTitle(),ReportConstant.LABEL_SELL_HOLDER_ACCOUNT);
        addContentAndStyle(rowHeader.createCell(INDEX_SELL_CUI_NAME),getStyleTitle(),ReportConstant.LABEL_SELL_CUI_NAME);
        addContentAndStyle(rowHeader.createCell(INDEX_SELL_CUI_ACT_ECO),getStyleTitle(),ReportConstant.LABEL_SELL_CUI_ACT_ECO);
        addContentAndStyle(rowHeader.createCell(INDEX_SELL_CUI_TYPE),getStyleTitle(),ReportConstant.LABEL_SELL_CUI_TYPE);
        addContentAndStyle(rowHeader.createCell(INDEX_MNEMONIC_BUYER_PART),getStyleTitle(),ReportConstant.LABEL_BUY_PARTICIPANT);
        addContentAndStyle(rowHeader.createCell(INDEX_BUY_HOLDER_ACCOUNT),getStyleTitle(),ReportConstant.LABEL_BUY_HOLDER_ACCOUNT);
        addContentAndStyle(rowHeader.createCell(INDEX_BUY_CUI_NAME),getStyleTitle(),ReportConstant.LABEL_BUY_CUI_NAME);
        addContentAndStyle(rowHeader.createCell(INDEX_BUY_CUI_ACT_ECO),getStyleTitle(),ReportConstant.LABEL_BUY_CUI_ACT_ECO);
        addContentAndStyle(rowHeader.createCell(INDEX_BUY_CUI_TYPE),getStyleTitle(),ReportConstant.LABEL_BUY_CUI_TYPE);
        addContentAndStyle(rowHeader.createCell(INDEX_SECURITY_CODE),getStyleTitle(),ReportConstant.LABEL_SECURITY_CLASS_CODE);
        addContentAndStyle(rowHeader.createCell(INDEX_CURRENCY),getStyleTitle(),ReportConstant.LABEL_CURRENCY_MNEMONIC);
        addContentAndStyle(rowHeader.createCell(INDEX_STOCK_QUANTITY),getStyleTitle(),ReportConstant.LABEL_STOCK_QUANTITY);
        addContentAndStyle(rowHeader.createCell(INDEX_OPERATION_PRICE),getStyleTitle(),ReportConstant.LABEL_OPERATION_PRICE);
//        addContentAndStyle(rowHeader.createCell(INDEX_RATE),getStyleTitle(),ReportConstant.LABEL_RATE_PERCENT);
//        addContentAndStyle(rowHeader.createCell(INDEX_SETTLEMENT_DAYS),getStyleTitle(),ReportConstant.LABEL_PLAZO);
        addContentAndStyle(rowHeader.createCell(INDEX_OPERATION_AMOUNT),getStyleTitle(),ReportConstant.LABEL_TOTAL_OPERATION_AMOUNT);
        addContentAndStyle(rowHeader.createCell(INDEX_OPERATION_AMOUNT_USD),getStyleTitle(),ReportConstant.LABEL_TOTAL_OPERATION_AMOUNT_USD);
//        addContentAndStyle(rowHeader.createCell(INDEX_SETTLEMENT_TYPE),getStyleTitle(),ReportConstant.LABEL_SETTLEMENT_TYPE);
        addContentAndStyle(rowHeader.createCell(INDEX_INSTANCIA),getStyleTitle(),ReportConstant.LABEL_INSTANCIA);
        // issue 1259: a requerimiento de usuario se adiconan 3 columnas mas al areporte de liquidaciones
        addContentAndStyle(rowHeader.createCell(INDEX_RATE_NEGOTIATION),getStyleTitle(),ReportConstant.LABEL_NEGOTIATION_RATE);
        addContentAndStyle(rowHeader.createCell(INDEX_SELAR),getStyleTitle(),ReportConstant.LABEL_SELAR);
        addContentAndStyle(rowHeader.createCell(INDEX_BALLOT_AND_SEQUENCIAL),getStyleTitle(),ReportConstant.LABEL_BALLOT_NUMBER);

        
        for (Object[] query : queryMain) {
        	
        	String operationDate=CommonsUtilities.convertDatetoString((Date) query[0]);
        	String modality=query[1].toString();
//        	Double ballotNumber=query[2]!=null ? Double.parseDouble(query[2].toString()) : Double.parseDouble("0");;
//        	Double sequential=query[3]!=null ? Double.parseDouble(query[3].toString()) : Double.parseDouble("0");;
        	String operationPart=query[4].toString();
        	String mneonicSellPart=query[26].toString();
        	String sellHolderAccount=query[11]!=null ?String.valueOf(query[11]):"";
        	String sellCuiName = query[12]!=null ?String.valueOf(query[12]):"";
        	String sellCuiActEco = query[13]!=null ?String.valueOf(query[13]):"";
        	String sellCuiType = query[14]!=null ?String.valueOf(query[14]):"";
        	String mneonicBuyPart=query[25].toString();
        	String buyHolderAccount=query[6]!=null ?String.valueOf(query[6]):"";
        	String buyCuiName = query[7]!=null ?String.valueOf(query[7]):"";
        	String buyCuiActEco = query[8]!=null ?String.valueOf(query[8]):"";
        	String buyCuiType = query[9]!=null ?String.valueOf(query[9]):"";
        	String securityCode=query[15].toString();
        	String currency=query[16].toString();
        	Double stockQuantity=query[17]!=null ? Double.parseDouble(query[17].toString()) : Double.parseDouble("0");;
        	Double operationPrice=query[18]!=null ? Double.parseDouble(query[18].toString()) : Double.parseDouble("0");;
//        	BigDecimal rate =query[13]!=null ? new BigDecimal(query[13].toString()) : BigDecimal.ZERO;
//        	Double settlementDays=query[14]!=null ? Double.parseDouble(query[14].toString()) : Double.parseDouble("0");;
        	BigDecimal operationAmount = query[21]!=null ? new BigDecimal(query[21].toString()) : BigDecimal.ZERO;
        	BigDecimal operationAmountUsd = query[22]!=null ? new BigDecimal(query[22].toString()) : BigDecimal.ZERO;
//        	String settlementType=query[17].toString();
//        	System.out.println(""+String.valueOf(query[18]));
        	String instancia=query[24]!=null ?String.valueOf(query[24]):"";
        	
        	String selar = query[27]!=null ?String.valueOf(query[27]):"";
        	BigDecimal rateNegotiation = query[28]!=null ? new BigDecimal(query[28].toString()) : BigDecimal.ZERO;
        	String ballotNumberAndSequential = (query[2]!=null ? query[2].toString(): "")+"/"+(query[3]!=null ? query[3].toString(): "");

	        indrow++;
	        
	        /**
	         * La data 
	         */
	        HSSFRow rowData = sheet.createRow((int)indrow);
	        addContentAndStyle(rowData.createCell(INDEX_OPERATION_DATE),getStyleAccountingAccount(), operationDate);
	        addContentAndStyle(rowData.createCell(INDEX_MODALITY),getStyleAccountingAccount(), modality);
//	        addContentAndStyle(rowData.createCell(INDEX_BALLOT_NUMBER),getStyleAccountingAccount(), ballotNumber.doubleValue());
//	        addContentAndStyle(rowData.createCell(INDEX_SEQUENTIAL),getStyleAccountingAccount(), sequential.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_OPERATION_PART),getStyleAccountingAccount(), operationPart);
	        addContentAndStyle(rowData.createCell(INDEX_MNEMONIC_SELLER_PART),getStyleAccountingAccount(), mneonicSellPart);
	        addContentAndStyle(rowData.createCell(INDEX_SELL_HOLDER_ACCOUNT),getStyleAccountingAccount(), sellHolderAccount);
	        addContentAndStyle(rowData.createCell(INDEX_SELL_CUI_NAME),getStyleAccountingAccount(), sellCuiName);
	        addContentAndStyle(rowData.createCell(INDEX_SELL_CUI_ACT_ECO),getStyleAccountingAccount(), sellCuiActEco);
	        addContentAndStyle(rowData.createCell(INDEX_SELL_CUI_TYPE),getStyleAccountingAccount(), sellCuiType);
	        addContentAndStyle(rowData.createCell(INDEX_MNEMONIC_BUYER_PART),getStyleAccountingAccount(), mneonicBuyPart);
	        addContentAndStyle(rowData.createCell(INDEX_BUY_HOLDER_ACCOUNT),getStyleAccountingAccount(), buyHolderAccount);
	        addContentAndStyle(rowData.createCell(INDEX_BUY_CUI_NAME),getStyleAccountingAccount(), buyCuiName);
	        addContentAndStyle(rowData.createCell(INDEX_BUY_CUI_ACT_ECO),getStyleAccountingAccount(), buyCuiActEco);
	        addContentAndStyle(rowData.createCell(INDEX_BUY_CUI_TYPE),getStyleAccountingAccount(), buyCuiType);
	        addContentAndStyle(rowData.createCell(INDEX_SECURITY_CODE),getStyleAccountingAccount(), securityCode);
	        addContentAndStyle(rowData.createCell(INDEX_CURRENCY),getStyleAccountingAccount(), currency);
	        addContentAndStyle(rowData.createCell(INDEX_STOCK_QUANTITY),getStyleAccountingAccount(), stockQuantity.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_OPERATION_PRICE),getStyleAccountingAccount(), operationPrice.doubleValue());
//	        addContentAndStyle(rowData.createCell(INDEX_RATE),getStyleAccountingAccount(), rate.doubleValue());
//	        addContentAndStyle(rowData.createCell(INDEX_SETTLEMENT_DAYS),getStyleAccountingAccount(), settlementDays.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_OPERATION_AMOUNT),getStyleAccountingAccount(), operationAmount.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_OPERATION_AMOUNT_USD),getStyleAccountingAccount(), operationAmountUsd.doubleValue());
//	        addContentAndStyle(rowData.createCell(INDEX_SETTLEMENT_TYPE),getStyleAccountingAccount(), settlementType);
	        addContentAndStyle(rowData.createCell(INDEX_INSTANCIA),getStyleAccountingAccount(), instancia);
	        
	        addContentAndStyle(rowData.createCell(INDEX_RATE_NEGOTIATION),getStyleAccountingAccount(), rateNegotiation.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_SELAR),getStyleAccountingAccount(), selar);
	        addContentAndStyle(rowData.createCell(INDEX_BALLOT_AND_SEQUENCIAL),getStyleAccountingAccount(), ballotNumberAndSequential);
		}
        
        /**adjustColumns**/
        //adjustColumns(sheet,15);

	}
	
	
	
	
	
	/**
	 * Settled Operations Report / Operaciones Liquidadas
	 * @param parameters
	 * @param reportLogger
	 * @return
	 */
	public byte[] writeFileExcelRegistrationAssignment(Map<String, Object> parameters , ReportLogger reportLogger  ){
		
		byte[] arrayByte=null;
		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("str_query");
		initCellStyle();
		initFont();
		initDataFormat();
		
		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						createSheetRegistrationAssignment(hSSFWorkbook,queryMain.subList(i, ifinal),queryMain.subList(i, ifinal).size());
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{//Solo un HSSFSheet
	        		createSheetRegistrationAssignment(hSSFWorkbook,queryMain,queryMain.size());
	        	}
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_REGISTRATION_ASSIGNMENT);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	       	hSSFWorkbook.write(bos);
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        hSSFWorkbook.write(fileOut);
	        fileOut.close();
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
		return arrayByte;
	}
	
	/**
	 * 
	 * @param hSSFWorkbook
	 * @param queryMain
	 */
	public void createSheetRegistrationAssignment(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, int number){
		
		int INDEX_BALLOT_NUMBER=0;
		int INDEX_SEQUENTIAL=1;
		int INDEX_MODALITY=2;
		int INDEX_OPERATION=3;
		int INDEX_HOLDER_ACCOUNT=4;
		int INDEX_HOLDER_DESCRIPTION=5;
		int INDEX_HOLDER_DESCRIPTION_DETAIL=6;
		int INDEX_ENCHARGE=7;
		int INDEX_SECURITY_CODE=8;
		int INDEX_STOCK_QUANTITY=9;
		int INDEX_MARKET_DATE=10;
		int INDEX_RATE=11;
		int INDEX_OPERATION_PRICE=12;
		int INDEX_STATE=13;
	
		/**Created a new sheet within the workBock**/
    	HSSFSheet sheet = hSSFWorkbook.createSheet("RegistrationAssignment"+number);
    	
		/***** BEGIN - REPORT HEADER  */

        int indrow=1;

        /**Created Header DEBE - HABER*/
        HSSFRow rowHeader = sheet.createRow((int)indrow);
       
        addContentAndStyle(rowHeader.createCell(INDEX_BALLOT_NUMBER),getStyleTitle(),ReportConstant.LABEL_BALLOT_NUMBER);
        addContentAndStyle(rowHeader.createCell(INDEX_SEQUENTIAL),getStyleTitle(),ReportConstant.LABEL_SEQUENTIAL);
        addContentAndStyle(rowHeader.createCell(INDEX_MODALITY),getStyleTitle(),ReportConstant.LABEL_MODALITY);
        addContentAndStyle(rowHeader.createCell(INDEX_OPERATION),getStyleTitle(),ReportConstant.LABEL_OPERATION);
        addContentAndStyle(rowHeader.createCell(INDEX_HOLDER_ACCOUNT),getStyleTitle(),ReportConstant.LABEL_HOLDER_ACCOUNT);
        addContentAndStyle(rowHeader.createCell(INDEX_HOLDER_DESCRIPTION),getStyleTitle(),ReportConstant.LABEL_CUI);
        addContentAndStyle(rowHeader.createCell(INDEX_HOLDER_DESCRIPTION_DETAIL),getStyleTitle(),ReportConstant.LABEL_HOLDER);
        addContentAndStyle(rowHeader.createCell(INDEX_ENCHARGE),getStyleTitle(),ReportConstant.LABEL_ENCHARGE);
        addContentAndStyle(rowHeader.createCell(INDEX_SECURITY_CODE),getStyleTitle(),ReportConstant.LABEL_SECURITY_CLASS_CODE);
        addContentAndStyle(rowHeader.createCell(INDEX_STOCK_QUANTITY),getStyleTitle(),ReportConstant.LABEL_STOCK_QUANTITY);
        addContentAndStyle(rowHeader.createCell(INDEX_MARKET_DATE),getStyleTitle(),ReportConstant.LABEL_MARKETFACT_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_RATE),getStyleTitle(),ReportConstant.LABEL_RATE_PERCENT_MARKETFACT);
        addContentAndStyle(rowHeader.createCell(INDEX_OPERATION_PRICE),getStyleTitle(),ReportConstant.LABEL_MARKFACT);
        addContentAndStyle(rowHeader.createCell(INDEX_STATE),getStyleTitle(),ReportConstant.LABEL_STATE);
        
        for (Object[] query : queryMain) {
        	
        	Double ballotNumber=query[7]!=null ? Double.parseDouble(query[7].toString()) : Double.parseDouble("0");;
        	Double sequential=query[8]!=null ? Double.parseDouble(query[8].toString()) : Double.parseDouble("0");;
        	String modality=query[9].toString();
        	String operation=query[13].toString();
        	String holderAccount=query[10].toString();
        	String holderDescription=query[14].toString();
        	String encharge=query[5].toString();
        	String securityCode=query[15].toString();
        	Double stockQuantity=query[16]!=null ? Double.parseDouble(query[16].toString()) : Double.parseDouble("0");;
        	String marketDate=CommonsUtilities.convertDatetoString((Date) query[17]);
        	BigDecimal rate =query[18]!=null ? new BigDecimal(query[18].toString()) : BigDecimal.ZERO;
        	Double operationPrice=query[19]!=null ? Double.parseDouble(query[19].toString()) : Double.parseDouble("0");;
        	String state=query[20].toString();
        	String holderDescriptionDetail=query[24].toString();	
        	
	        indrow++;
	        
	        /** La data */
	        HSSFRow rowData = sheet.createRow((int)indrow);

	        addContentAndStyle(rowData.createCell(INDEX_BALLOT_NUMBER),getStyleAccountingAccount(), ballotNumber.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_SEQUENTIAL),getStyleAccountingAccount(), sequential.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_MODALITY),getStyleAccountingAccount(), modality);
	        addContentAndStyle(rowData.createCell(INDEX_OPERATION),getStyleAccountingAccount(), operation);
	        addContentAndStyle(rowData.createCell(INDEX_HOLDER_ACCOUNT),getStyleAccountingAccount(), holderAccount);
	        addContentAndStyle(rowData.createCell(INDEX_HOLDER_DESCRIPTION),getStyleAccountingAccount(), holderDescription);
	        addContentAndStyle(rowData.createCell(INDEX_HOLDER_DESCRIPTION_DETAIL),getStyleAccountingAccount(), holderDescriptionDetail);
	        addContentAndStyle(rowData.createCell(INDEX_ENCHARGE),getStyleAccountingAccount(), encharge );
	        addContentAndStyle(rowData.createCell(INDEX_SECURITY_CODE),getStyleAccountingAccount(), securityCode);
	        addContentAndStyle(rowData.createCell(INDEX_STOCK_QUANTITY),getStyleAccountingAccount(), stockQuantity.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_MARKET_DATE),getStyleAccountingAccount(), marketDate);
	        addContentAndStyle(rowData.createCell(INDEX_RATE),getStyleAccountingAccount(), rate.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_OPERATION_PRICE),getStyleAccountingAccount(), operationPrice.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_STATE),getStyleAccountingAccount(), state );
		}
        /**adjustColumns**/
        //adjustColumns(sheet,15);
	}
public byte[] writeFileExcelCommissionsAnnotationAccountNonBcb(Map<String, Object> parameters , ReportLogger reportLogger){
		
		byte[] arrayByte=null;
		
		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("lstObjects");
		
		initCellStyle();
		initFont();
		initDataFormat();
		
		
		
		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						writeFileExcelCommissionsAnnotationAccountNonBcb(hSSFWorkbook,queryMain.subList(i, ifinal),ifinal,reportLogger);
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{
	        		//Solo un HSSFSheet
	        		writeFileExcelCommissionsAnnotationAccountNonBcb(hSSFWorkbook,queryMain,queryMain.size(),reportLogger);
	        	}
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_CLIENT_PORTFOLIO);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	         
	       	hSSFWorkbook.write(bos);
	        
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        
	        hSSFWorkbook.write(fileOut);
	        
	        fileOut.close();
			
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    } 
		return arrayByte;
	}
	
	public byte[] writeFileExcelCommissionsAnnotationAccount(Map<String, Object> parameters , ReportLogger reportLogger){
		
		byte[] arrayByte=null;
		
		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("lstObjects");
		
		initCellStyle();
		initFont();
		initDataFormat();
		
		
		
		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						writeFileExcelCommissionsAnnotationAccount(hSSFWorkbook,queryMain.subList(i, ifinal),ifinal,reportLogger);
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{
	        		//Solo un HSSFSheet
	        		writeFileExcelCommissionsAnnotationAccount(hSSFWorkbook,queryMain,queryMain.size(),reportLogger);
	        	}
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_CLIENT_PORTFOLIO);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	         
	       	hSSFWorkbook.write(bos);
	        
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        
	        hSSFWorkbook.write(fileOut);
	        
	        fileOut.close();
			
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    } 
		return arrayByte;
	}
	
	/**
	 * issue 1179: reporte de mercado priamrio y secudario General
	 * @param parameters
	 * @param reportLogger
	 * @return
	 */
	public byte[] writeFileExcelSecondaryyMarketGral1179(Map<String, Object> parameters , ReportLogger reportLogger){
		byte[] arrayByte=null;
		
		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("lstObjects");
		
		initCellStyle();
		initFont();
		initDataFormat();

		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						writeFileExcelSecondaryyMarketGral1179(hSSFWorkbook,queryMain.subList(i, ifinal),ifinal,reportLogger);
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{
	        		//Solo un HSSFSheet
	        		writeFileExcelSecondaryyMarketGral1179(hSSFWorkbook,queryMain,queryMain.size(),reportLogger);
	        	}
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_CLIENT_PORTFOLIO);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	         
	       	hSSFWorkbook.write(bos);
	        
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        
	        hSSFWorkbook.write(fileOut);
	        
	        fileOut.close();
			
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    } 
		return arrayByte;
	}
	
	/**
	 * metodo para la generacion de reporte de mercado secundario issue 1005
	 * @param parameters
	 * @param reportLogger
	 * @return
	 */
	public byte[] writeFileExcelSecondaryMarket(Map<String, Object> parameters , ReportLogger reportLogger){
		
		byte[] arrayByte=null;
		
		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("lstObjects");
		
		initCellStyle();
		initFont();
		initDataFormat();

		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						writeFileExcelSecondaryMarket(hSSFWorkbook,queryMain.subList(i, ifinal),ifinal,reportLogger);
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{
	        		//Solo un HSSFSheet
	        		writeFileExcelSecondaryMarket(hSSFWorkbook,queryMain,queryMain.size(),reportLogger);
	        	}
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_CLIENT_PORTFOLIO);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	         
	       	hSSFWorkbook.write(bos);
	        
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        
	        hSSFWorkbook.write(fileOut);
	        
	        fileOut.close();
			
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    } 
		return arrayByte;
	}
	
	public void writeFileExcelSecondaryyMarketGral1179(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, int number,ReportLogger reportLogger) {
		//int INDEX_ID = 1;
		int INDEX_NRO_OPERACION = 0;
		int INDEX_FECHA_OPS = 1;
		int INDEX_PART_VENTA = 2;
		int INDEX_MODALIDAD_VENTA = 3;
		int INDEX_CUI_VENTA = 4;
		int INDEX_NOMBRE_VENTA = 5;
		int INDEX_CARNET_VENTA = 6;
		int INDEX_PART_COMPRA = 7;
		int INDEX_MODALIDAD_COMPRA = 8;
		int INDEX_CUI_COMPRA = 9;
		int INDEX_NOMBRE_COMPRA = 10;
		int INDEX_CARNET_COMPRA = 11;
		int INDEX_MODALIDAD_NEG = 12;
		int INDEX_MONEDA = 13;
		int INDEX_CANT_TOTAL_VAL = 14;
		int INDEX_CLAVE_VALOR = 15;
		int INDEX_VAL_NOMINAL = 16;
		int INDEX_PRECIO = 17;
		int INDEX_TASA = 18;
		int INDEX_PAPELETA_SEQ = 19;
		int INDEX_PRECIO_LIQUIDACION = 20;
		int INDEX_FECHA_OPERA_REPORTO = 21;
		int INDEX_FECHA_LIQ_PLAZO = 22;
		int INDEX_PRECIO_CURVA_ASFI = 23;
		int INDEX_ESTADO_OPERACION = 24;
		int INDEX_TASA_VALOR = 25;
		int INDEX_ACTIVIDAD_ECO_COMPRADOR = 26;
		int INDEX_ACTIVIDAD_ECO_VENDEDOR = 27;
		int INDEX_OBSERVACIONES = 28;
		
		//creando la hoja
		HSSFSheet sheet = hSSFWorkbook.createSheet("Mercado Secundario Gral.");
		
		//cabecera del reporte
//		String titleReport=reportLogger.getReport().getTitle();
//		String strStartHour = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),ReportConstant.DATE_FORMAT_TIME_SECOND);
//		
//		HSSFRow row0= sheet.createRow((short)0);
//        sheet.addMergedRegion(new CellRangeAddress(0,0,1,8));
//        addContentAndStyle(row0.createCell(1),getStyleTitleBoldCenter(),titleReport);
//        
//        /**DATE*/
//        HSSFRow row1= sheet.createRow((short)1);
//        addContentAndStyle(row1.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_FECHA);
//        addContentAndStyle(row1.createCell(8),CommonsUtilities.convertDatetoString(reportLogger.getRegistryDate()));
//        
//        /**HOUR*/
//        HSSFRow row2= sheet.createRow((short)2);
//        addContentAndStyle(row2.createCell(7),getStyleTitleBold(), ReportConstant.LABEL_HORA);
//        addContentAndStyle(row2.createCell(8), strStartHour);
//        
//        /**CLASIFICATION*/
//        HSSFRow row3= sheet.createRow((short)3);
//        addContentAndStyle(row3.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_CLASIFICACION);
//        addContentAndStyle(row3.createCell(8),reportLogger.getReport().getReportClasification());
//
//        
//        /**Usuario**/
//        HSSFRow row5= sheet.createRow((short)4);
//        addContentAndStyle(row5.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_USUARIO);
//        addContentAndStyle(row5.createCell(8),reportLogger.getRegistryUser());
        
        int indrow=0;
        
        HSSFRow rowHeader = sheet.createRow((int)indrow);
        //addContentAndStyle(rowHeader.createCell(INDEX_ID),getStyleTitle(),ReportConstant.LABEL_ID_COLUMN);
        addContentAndStyle(rowHeader.createCell(INDEX_NRO_OPERACION),getStyleTitle(),ReportConstant.LABEL_NRO_OPERATION);
        addContentAndStyle(rowHeader.createCell(INDEX_FECHA_OPS),getStyleTitle(),ReportConstant.LABEL_DATE_OPERATION);
        addContentAndStyle(rowHeader.createCell(INDEX_PART_VENTA),getStyleTitle(),ReportConstant.LABEL_SELL_PARTICIPANT);
        addContentAndStyle(rowHeader.createCell(INDEX_MODALIDAD_VENTA),getStyleTitle(),ReportConstant.LABEL_MODALITY);
        addContentAndStyle(rowHeader.createCell(INDEX_CUI_VENTA),getStyleTitle(),ReportConstant.LABEL_SELL_CUI);
        addContentAndStyle(rowHeader.createCell(INDEX_NOMBRE_VENTA),getStyleTitle(),ReportConstant.LABEL_SELL_CUI_NAME);
        addContentAndStyle(rowHeader.createCell(INDEX_CARNET_VENTA),getStyleTitle(),ReportConstant.LABEL_CI);
        addContentAndStyle(rowHeader.createCell(INDEX_PART_COMPRA),getStyleTitle(),ReportConstant.LABEL_BUYER_PARTICIPANT);
        addContentAndStyle(rowHeader.createCell(INDEX_MODALIDAD_COMPRA),getStyleTitle(),ReportConstant.LABEL_MODALITY);
        addContentAndStyle(rowHeader.createCell(INDEX_CUI_COMPRA),getStyleTitle(),ReportConstant.LABEL_BUY_CUI);
        addContentAndStyle(rowHeader.createCell(INDEX_NOMBRE_COMPRA),getStyleTitle(),ReportConstant.LABEL_BUY_CUI_NAME);
        addContentAndStyle(rowHeader.createCell(INDEX_CARNET_COMPRA),getStyleTitle(),ReportConstant.LABEL_CI);
        addContentAndStyle(rowHeader.createCell(INDEX_MODALIDAD_NEG),getStyleTitle(),ReportConstant.LABEL_MODALITY);
        addContentAndStyle(rowHeader.createCell(INDEX_MONEDA),getStyleTitle(),ReportConstant.LABEL_CURRENCY);
        addContentAndStyle(rowHeader.createCell(INDEX_CANT_TOTAL_VAL),getStyleTitle(),ReportConstant.LABEL_TOTAL_QUANTITY);
        addContentAndStyle(rowHeader.createCell(INDEX_CLAVE_VALOR),getStyleTitle(),ReportConstant.LABEL_SECURITY_CODE);
        addContentAndStyle(rowHeader.createCell(INDEX_VAL_NOMINAL),getStyleTitle(),ReportConstant.LABEL_VN);
        addContentAndStyle(rowHeader.createCell(INDEX_PRECIO),getStyleTitle(),ReportConstant.LABEL_OPERATION_PRICE);
        addContentAndStyle(rowHeader.createCell(INDEX_TASA),getStyleTitle(),ReportConstant.LABEL_RATE);
        addContentAndStyle(rowHeader.createCell(INDEX_PAPELETA_SEQ),getStyleTitle(),ReportConstant.LABEL_BALLOT_NUMBER);
        addContentAndStyle(rowHeader.createCell(INDEX_PRECIO_LIQUIDACION),getStyleTitle(),ReportConstant.LABEL_OPERATION_PRICE_SETTLEMENT);
        addContentAndStyle(rowHeader.createCell(INDEX_FECHA_OPERA_REPORTO),getStyleTitle(),ReportConstant.LABEL_OPERATION_FECHA_OPERA_REPORTO);
        addContentAndStyle(rowHeader.createCell(INDEX_FECHA_LIQ_PLAZO),getStyleTitle(),ReportConstant.LABEL_OPERATION_FECHA_LIQ_PLAZO);
        addContentAndStyle(rowHeader.createCell(INDEX_PRECIO_CURVA_ASFI),getStyleTitle(),ReportConstant.LABEL_OPERATION_PRICE_CURVE);
        
        addContentAndStyle(rowHeader.createCell(INDEX_TASA_VALOR),getStyleTitle(),ReportConstant.LABEL_OPERATION_SECURITY_RATE);
        addContentAndStyle(rowHeader.createCell(INDEX_ESTADO_OPERACION),getStyleTitle(),ReportConstant.LABEL_OPERATION_STATE);
        addContentAndStyle(rowHeader.createCell(INDEX_ACTIVIDAD_ECO_COMPRADOR),getStyleTitle(),ReportConstant.LABEL_OPERATION_ECONOMIC_BUY);
        addContentAndStyle(rowHeader.createCell(INDEX_ACTIVIDAD_ECO_VENDEDOR),getStyleTitle(),ReportConstant.LABEL_OPERATION_ECONOMIC_SELL);
        
        addContentAndStyle(rowHeader.createCell(INDEX_OBSERVACIONES),getStyleTitle(),ReportConstant.LABEL_OPERATION_OBSERVATIONS);
        
        HSSFRow rowData;
        for(Object[] rowResult:queryMain) {
        	indrow++;
        	rowData = sheet.createRow((int)indrow);
        	//addContentAndStyle(rowData.createCell(INDEX_ID),getStyleAccountingAccount(), rowResult[0] != null ? Double.parseDouble(rowResult[0].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_NRO_OPERACION),getStyleAccountingAccount(), rowResult[1] != null ? Double.parseDouble(rowResult[1].toString()) : 0.00);
        	
        	// trasnforamndo a elda formato fecha
        	try {
				addContentAndStyle(rowData.createCell(INDEX_FECHA_OPS),getStyleAccountingAccountDateIssue1179(), rowResult[2] != null ? new SimpleDateFormat("dd/MM/yyyy").parse(rowResult[2].toString()) : null);
			} catch (Exception e) {
				e.printStackTrace();
				addContentAndStyle(rowData.createCell(INDEX_FECHA_OPS),getStyleAccountingAccount(), rowResult[2] != null ? rowResult[2].toString() : "-");
			}
        	addContentAndStyle(rowData.createCell(INDEX_PART_VENTA),getStyleAccountingAccountLeft(), rowResult[3] != null ? rowResult[3].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_MODALIDAD_VENTA),getStyleAccountingAccountLeft(), rowResult[4] != null ? rowResult[4].toString() : "-");
        	
        	// transformando los cui a celdas numericas
        	//addContentAndStyle(rowData.createCell(INDEX_CUI_VENTA),getStyleAccountingAccountLeft(), rowResult[5] != null ? rowResult[5].toString() : "-");
        	if(rowResult[5]!=null){
        		if(rowResult[5].toString().contains(",")){
        			addContentAndStyle(rowData.createCell(INDEX_CUI_VENTA),getStyleAccountingAccountOnlyNumberIssue1179(), rowResult[5] != null ? rowResult[5].toString() : "-");	
        		}else{
        			addContentAndStyle(rowData.createCell(INDEX_CUI_VENTA),getStyleAccountingAccount(), Double.parseDouble(rowResult[5].toString()));
        		}
        	}else{
        		addContentAndStyle(rowData.createCell(INDEX_CUI_VENTA),getStyleAccountingAccount(), 00.00);
        	}
        	
        	
        	addContentAndStyle(rowData.createCell(INDEX_NOMBRE_VENTA),getStyleAccountingAccountLeft(), rowResult[6] != null ? rowResult[6].toString() : "-");
        	
        	//transformando el NIT en formato numero
        	//addContentAndStyle(rowData.createCell(INDEX_CARNET_VENTA),getStyleAccountingAccount(), rowResult[7] != null ? rowResult[10].toString() : "-");
        	if(rowResult[7]!=null){
        		// si no logra parsear a double, significa q contiene una (,) o una letra ala tratarse de CI/NIT 
        		try {
        			addContentAndStyle(rowData.createCell(INDEX_CARNET_VENTA),getStyleAccountingAccount(), Double.parseDouble(rowResult[7].toString()));
				} catch (Exception e) {
					addContentAndStyle(rowData.createCell(INDEX_CARNET_VENTA),getStyleAccountingAccountOnlyNumberIssue1179(), rowResult[7] != null ? rowResult[7].toString() : "-");
				}
        	}else{
        		addContentAndStyle(rowData.createCell(INDEX_CARNET_VENTA),getStyleAccountingAccount(), 00.00);
        	}
        	
        	addContentAndStyle(rowData.createCell(INDEX_PART_COMPRA),getStyleAccountingAccountLeft(), rowResult[8] != null ? rowResult[8].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_MODALIDAD_COMPRA),getStyleAccountingAccountLeft(), rowResult[9] != null ? rowResult[9].toString() : "-");
        	
        	// transformando los cui a celdas numericas
        	//addContentAndStyle(rowData.createCell(INDEX_CUI_COMPRA),getStyleAccountingAccount(), rowResult[10] != null ? rowResult[10].toString() : "-");
        	if(rowResult[10]!=null){
        		if(rowResult[10].toString().contains(",")){
        			addContentAndStyle(rowData.createCell(INDEX_CUI_COMPRA),getStyleAccountingAccountOnlyNumberIssue1179(), rowResult[10] != null ? rowResult[10].toString() : "-");	
        		}else{
        			addContentAndStyle(rowData.createCell(INDEX_CUI_COMPRA),getStyleAccountingAccount(), Double.parseDouble(rowResult[10].toString()));
        		}
        	}else{
        		addContentAndStyle(rowData.createCell(INDEX_CUI_COMPRA),getStyleAccountingAccount(), 00.00);
        	}
        	
        	addContentAndStyle(rowData.createCell(INDEX_NOMBRE_COMPRA),getStyleAccountingAccountLeft(), rowResult[11] != null ? rowResult[11].toString() : "-");
        	
        	//transformando el NIT en formato numero
        	//addContentAndStyle(rowData.createCell(INDEX_CARNET_COMPRA),getStyleAccountingAccountLeft(), rowResult[12] != null ? rowResult[12].toString() : "-");
        	if(rowResult[12]!=null){
        		// si no logra parsear a double, significa q contiene una (,) o una letra ala tratarse de CI/NIT 
        		try {
        			addContentAndStyle(rowData.createCell(INDEX_CARNET_COMPRA),getStyleAccountingAccount(), Double.parseDouble(rowResult[12].toString()));
				} catch (Exception e) {
					addContentAndStyle(rowData.createCell(INDEX_CARNET_COMPRA),getStyleAccountingAccountOnlyNumberIssue1179(), rowResult[12] != null ? rowResult[12].toString() : "-");
				}
        	}else{
        		addContentAndStyle(rowData.createCell(INDEX_CARNET_COMPRA),getStyleAccountingAccount(), 00.00);
        	}
        	
        	
        	addContentAndStyle(rowData.createCell(INDEX_MODALIDAD_NEG),getStyleAccountingAccountLeft(), rowResult[13] != null ? rowResult[13].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_MONEDA),getStyleAccountingAccountLeft(), rowResult[14] != null ? rowResult[14].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_CANT_TOTAL_VAL),getStyleAccountingAccount(), rowResult[15] != null ? Double.parseDouble(rowResult[15].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_CLAVE_VALOR),getStyleAccountingAccountLeft(), rowResult[16] != null ? rowResult[16].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_VAL_NOMINAL),getStyleAccountingAccountAmountIssue1091(), rowResult[17] != null ? Double.parseDouble(rowResult[17].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_PRECIO),getStyleAccountingAccountAmountIssue1091(), rowResult[18] != null ?  Double.parseDouble(rowResult[18].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_TASA),getStyleAccountingAccountAmountIssue1091(), rowResult[19] != null ?  Double.parseDouble(rowResult[19].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_PAPELETA_SEQ),getStyleAccountingAccountLeft(), rowResult[20] != null ? rowResult[20].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_PRECIO_LIQUIDACION),getStyleAccountingAccountAmountIssue1091(), rowResult[21] != null ?  Double.parseDouble(rowResult[21].toString()) : 0.00);
        	
        	addContentAndStyle(rowData.createCell(INDEX_FECHA_OPERA_REPORTO),getStyleAccountingAccountLeft(), rowResult[22] != null ? rowResult[22].toString() : "-");
        	//addContentAndStyle(rowData.createCell(INDEX_FECHA_LIQ_PLAZO),getStyleAccountingAccountLeft(), rowResult[23] != null ? rowResult[23].toString() : "-");
        	
        	try {
				addContentAndStyle(rowData.createCell(INDEX_FECHA_LIQ_PLAZO),getStyleAccountingAccountDateIssue1179(), rowResult[23] != null ? new SimpleDateFormat("dd/MM/yyyy").parse(rowResult[23].toString()) : null);
			} catch (Exception e) {
				e.printStackTrace();
				addContentAndStyle(rowData.createCell(INDEX_FECHA_LIQ_PLAZO),getStyleAccountingAccount(), rowResult[23] != null ? rowResult[23].toString() : "-");
			}
        	
        	addContentAndStyle(rowData.createCell(INDEX_PRECIO_CURVA_ASFI),getStyleAccountingAccountAmountIssue1091(), rowResult[24] != null ?  Double.parseDouble(rowResult[24].toString()) : 0.00);
        	
        	addContentAndStyle(rowData.createCell(INDEX_TASA_VALOR),getStyleAccountingAccountAmountIssue1091(), rowResult[26] != null ?  Double.parseDouble(rowResult[26].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_ESTADO_OPERACION),getStyleAccountingAccountLeft(), rowResult[25] != null ? rowResult[25].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_ACTIVIDAD_ECO_COMPRADOR),getStyleAccountingAccountLeft(), rowResult[27] != null ? rowResult[27].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_ACTIVIDAD_ECO_VENDEDOR),getStyleAccountingAccountLeft(), rowResult[28] != null ? rowResult[28].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_OBSERVACIONES),getStyleAccountingAccountLeft(), rowResult[29] != null ? rowResult[29].toString() : "-");
        }
	}
	
	/**
	 * metodo para la generacion de reporte de mercado secundario en Apache POI (issue 1005)
	 * @param hSSFWorkbook
	 * @param queryMain
	 * @param number
	 * @param reportLogger
	 */
	public void writeFileExcelSecondaryMarket(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, int number,ReportLogger reportLogger) {
		
		int INDEX_DATE_OPERATION = 1;
		int INDEX_AGENCY_BUYER = 2;
		int INDEX_CUI_BUYER = 3;
		int INDEX_CUI_BUYER_NAME = 4;
		int INDEX_AGENCY_SELLER = 5;
		int INDEX_CUI_SELLER = 6;
		int INDEX_CUI_SELLER_NAME = 7;
		int INDEX_SECURITY_CODE = 8;
		int INDEX_ISSUER = 9;
		int INDEX_ISSUER_JURIDICAL_NAME = 10;
		int INDEX_ISSUANCE_DATE = 11;
		int INDEX_SECURITY_TERM = 12;
		int INDEX_DATE_EXPIDERD = 13;
		int INDEX_CURRENCY = 14;
		int INDEX_NOMINAL_VALUE = 15;
		int INDEX_INTEREST_RATE = 16;
		int INDEX_NEGOTIATION_RATE = 17;
		int INDEX_NEGOTIATION_PRICE = 18;
		int INDEX_PERIOD_PAYMENT_INTERES = 19;
		int INDEX_PERIOD_PAYMENT_AMORTIZATION = 20;
		int INDEX_LAST_DATE_PAYMENT_INT = 21;
		int INDEX_LAST_DATE_PAYMENT_AMR = 22;
		int INDEX_NEXT_DATE_PAYMENT_INT = 23;
		int INDEX_NEXT_DATE_PAYMENT_AMR = 24;
		int INDEX_QUANTITY = 25;
		int INDEX_BALLOT_NUMBER = 26;
		int INDEX_PRECIO_LIQUIDACION = 27;
		
		//creando la hoja
		HSSFSheet sheet = hSSFWorkbook.createSheet("Mercado Secundario");
		
		//cabecera del reporte
		String titleReport=reportLogger.getReport().getTitle();
		String strStartHour = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),ReportConstant.DATE_FORMAT_TIME_SECOND);
		
		HSSFRow row0= sheet.createRow((short)0);
        sheet.addMergedRegion(new CellRangeAddress(0,0,1,8));
        addContentAndStyle(row0.createCell(1),getStyleTitleBoldCenter(),titleReport);
        
        /**DATE*/
        HSSFRow row1= sheet.createRow((short)1);
        addContentAndStyle(row1.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_FECHA);
        addContentAndStyle(row1.createCell(8),CommonsUtilities.convertDatetoString(reportLogger.getRegistryDate()));
        
        /**HOUR*/
        HSSFRow row2= sheet.createRow((short)2);
        addContentAndStyle(row2.createCell(7),getStyleTitleBold(), ReportConstant.LABEL_HORA);
        addContentAndStyle(row2.createCell(8), strStartHour);
        
        /**CLASIFICATION*/
        HSSFRow row3= sheet.createRow((short)3);
        addContentAndStyle(row3.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_CLASIFICACION);
        addContentAndStyle(row3.createCell(8),reportLogger.getReport().getReportClasification());

        
        /**Usuario**/
        HSSFRow row5= sheet.createRow((short)4);
        addContentAndStyle(row5.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_USUARIO);
        addContentAndStyle(row5.createCell(8),reportLogger.getRegistryUser());
        
        int indrow=8;
        
        HSSFRow rowHeader = sheet.createRow((int)indrow);
        addContentAndStyle(rowHeader.createCell(INDEX_DATE_OPERATION),getStyleTitle(),ReportConstant.LABEL_OPERATION_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_AGENCY_BUYER),getStyleTitle(),ReportConstant.LABEL_BUY_PARTICIPANT);
        addContentAndStyle(rowHeader.createCell(INDEX_CUI_BUYER),getStyleTitle(),"CUI COMPRA");
        addContentAndStyle(rowHeader.createCell(INDEX_CUI_BUYER_NAME),getStyleTitle(),ReportConstant.LABEL_BUY_CUI_NAME);
        addContentAndStyle(rowHeader.createCell(INDEX_AGENCY_SELLER),getStyleTitle(),ReportConstant.LABEL_SELL_PARTICIPANT);
        addContentAndStyle(rowHeader.createCell(INDEX_CUI_SELLER),getStyleTitle(),"CUI VENTA");
        addContentAndStyle(rowHeader.createCell(INDEX_CUI_SELLER_NAME),getStyleTitle(),ReportConstant.LABEL_SELL_CUI_NAME);
        addContentAndStyle(rowHeader.createCell(INDEX_SECURITY_CODE),getStyleTitle(),ReportConstant.LABEL_SECURITY_CLASS_CODE);
        addContentAndStyle(rowHeader.createCell(INDEX_ISSUER),getStyleTitle(),ReportConstant.LABEL_ISSUER);
        addContentAndStyle(rowHeader.createCell(INDEX_ISSUER_JURIDICAL_NAME),getStyleTitle(),ReportConstant.LABEL_BUSINESS_NAME);
        addContentAndStyle(rowHeader.createCell(INDEX_ISSUANCE_DATE),getStyleTitle(),ReportConstant.LABEL_ISSUANCE_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_SECURITY_TERM),getStyleTitle(),ReportConstant.LABEL_SECURITY_TERM);
        addContentAndStyle(rowHeader.createCell(INDEX_DATE_EXPIDERD),getStyleTitle(),ReportConstant.LABEL_DUE_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_CURRENCY),getStyleTitle(),ReportConstant.LABEL_CURRENCY);
        addContentAndStyle(rowHeader.createCell(INDEX_NOMINAL_VALUE),getStyleTitle(),ReportConstant.LABEL_NOMINAL_VALUE);
        addContentAndStyle(rowHeader.createCell(INDEX_INTEREST_RATE),getStyleTitle(),ReportConstant.LABEL_RATE_PERCENT);
        addContentAndStyle(rowHeader.createCell(INDEX_NEGOTIATION_RATE),getStyleTitle(),ReportConstant.LABEL_NEGOTIATION_RATE);
        addContentAndStyle(rowHeader.createCell(INDEX_NEGOTIATION_PRICE),getStyleTitle(),ReportConstant.LABEL_OPERATION_PRICE);
        addContentAndStyle(rowHeader.createCell(INDEX_PERIOD_PAYMENT_INTERES),getStyleTitle(),ReportConstant.LABEL_PERIOD_PAYMENT_INTERES);
        addContentAndStyle(rowHeader.createCell(INDEX_PERIOD_PAYMENT_AMORTIZATION),getStyleTitle(),ReportConstant.LABEL_PERIOD_PAYMENT_AMORTIZATION);
        addContentAndStyle(rowHeader.createCell(INDEX_LAST_DATE_PAYMENT_INT),getStyleTitle(),ReportConstant.LABEL_LAST_DATE_PAYMENT_INT);
        addContentAndStyle(rowHeader.createCell(INDEX_LAST_DATE_PAYMENT_AMR),getStyleTitle(),ReportConstant.LABEL_LAST_DATE_PAYMENT_AMR);
        addContentAndStyle(rowHeader.createCell(INDEX_NEXT_DATE_PAYMENT_INT),getStyleTitle(),ReportConstant.LABEL_NEXT_DATE_PAYMENT_INT);
        addContentAndStyle(rowHeader.createCell(INDEX_NEXT_DATE_PAYMENT_AMR),getStyleTitle(),ReportConstant.LABEL_NEXT_DATE_PAYMENT_AMR);
        addContentAndStyle(rowHeader.createCell(INDEX_QUANTITY),getStyleTitle(),ReportConstant.LABEL_STOCK_QUANTITY);
        addContentAndStyle(rowHeader.createCell(INDEX_BALLOT_NUMBER),getStyleTitle(),ReportConstant.LABEL_BALLOT_NUMBER);
        addContentAndStyle(rowHeader.createCell(INDEX_PRECIO_LIQUIDACION),getStyleTitle(),ReportConstant.LABEL_OPERATION_PRICE_SETTLEMENT);
        
        HSSFRow rowData;
        for(Object[] rowResult:queryMain) {
        	indrow++;
        	rowData = sheet.createRow((int)indrow);
        	addContentAndStyle(rowData.createCell(INDEX_DATE_OPERATION),getStyleAccountingAccount(), rowResult[0] != null ? rowResult[0].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_AGENCY_BUYER),getStyleAccountingAccount(), rowResult[1] != null ? rowResult[1].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_CUI_BUYER),getStyleAccountingAccount(), rowResult[2] != null ? rowResult[2].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_CUI_BUYER_NAME),getStyleAccountingAccount(), rowResult[3] != null ? rowResult[3].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_AGENCY_SELLER),getStyleAccountingAccount(), rowResult[4] != null ? rowResult[4].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_CUI_SELLER),getStyleAccountingAccount(), rowResult[5] != null ? rowResult[5].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_CUI_SELLER_NAME),getStyleAccountingAccount(), rowResult[6] != null ? rowResult[6].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_SECURITY_CODE),getStyleAccountingAccount(), rowResult[7] != null ? rowResult[7].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_ISSUER),getStyleAccountingAccount(), rowResult[8] != null ? rowResult[8].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_ISSUER_JURIDICAL_NAME),getStyleAccountingAccount(), rowResult[9] != null ? rowResult[9].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_ISSUANCE_DATE),getStyleAccountingAccount(), rowResult[10] != null ? rowResult[10].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_SECURITY_TERM),getStyleAccountingAccount(), rowResult[11] != null ? rowResult[11].toString() : "0");
        	addContentAndStyle(rowData.createCell(INDEX_DATE_EXPIDERD),getStyleAccountingAccount(), rowResult[12] != null ? rowResult[12].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_CURRENCY),getStyleAccountingAccount(), rowResult[13] != null ? rowResult[13].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_NOMINAL_VALUE),getStyleAccountingAccount(), rowResult[14] != null ? rowResult[14].toString() : "0");
        	addContentAndStyle(rowData.createCell(INDEX_INTEREST_RATE),getStyleAccountingAccount(), rowResult[15] != null ? rowResult[15].toString() : "0");
        	addContentAndStyle(rowData.createCell(INDEX_NEGOTIATION_RATE),getStyleAccountingAccount(), rowResult[16] != null ? rowResult[16].toString() : "0");
        	addContentAndStyle(rowData.createCell(INDEX_NEGOTIATION_PRICE),getStyleAccountingAccount(), rowResult[17] != null ? rowResult[17].toString() : "0");
        	addContentAndStyle(rowData.createCell(INDEX_PERIOD_PAYMENT_INTERES),getStyleAccountingAccount(), rowResult[18] != null ? rowResult[18].toString() : "0");
        	addContentAndStyle(rowData.createCell(INDEX_PERIOD_PAYMENT_AMORTIZATION),getStyleAccountingAccount(), rowResult[19] != null ? rowResult[19].toString() : "0");
        	addContentAndStyle(rowData.createCell(INDEX_LAST_DATE_PAYMENT_INT),getStyleAccountingAccount(), rowResult[20] != null ? CommonsUtilities.convertDateToString((Date)(rowResult[20]),ReportConstant.DATE_FORMAT_STANDAR) : "-");
        	addContentAndStyle(rowData.createCell(INDEX_LAST_DATE_PAYMENT_AMR),getStyleAccountingAccount(), rowResult[21] != null ? CommonsUtilities.convertDateToString((Date)(rowResult[21]),ReportConstant.DATE_FORMAT_STANDAR) : "-");
        	addContentAndStyle(rowData.createCell(INDEX_NEXT_DATE_PAYMENT_INT),getStyleAccountingAccount(), rowResult[22] != null ? CommonsUtilities.convertDateToString((Date)(rowResult[22]),ReportConstant.DATE_FORMAT_STANDAR) : "-");
        	addContentAndStyle(rowData.createCell(INDEX_NEXT_DATE_PAYMENT_AMR),getStyleAccountingAccount(), rowResult[23] != null ? CommonsUtilities.convertDateToString((Date)(rowResult[23]),ReportConstant.DATE_FORMAT_STANDAR) : "-");
        	addContentAndStyle(rowData.createCell(INDEX_QUANTITY),getStyleAccountingAccount(), rowResult[24] != null ? rowResult[24].toString() : "0");
        	addContentAndStyle(rowData.createCell(INDEX_BALLOT_NUMBER),getStyleAccountingAccount(), rowResult[25] != null ? rowResult[25].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_PRECIO_LIQUIDACION),getStyleAccountingAccount(), rowResult[26] != null ? rowResult[26].toString() : "0");
        }
	}
	
	/**
	 * issue 1189: REPORTE DE RELACION DE BLOQUEOS Y DESBLOQUEOS POI
	 * @param parameters
	 * @param reportLogger
	 * @return
	 */
	public byte[] writeFileExcelRelationshipBlockAndUnlock(Map<String, Object> parameters , ReportLogger reportLogger){
		byte[] arrayByte=null;
		
		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("lstObjects");
		
		initCellStyle();
		initFont();
		initDataFormat();

		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						writeFileExcelRelationshipBlockAndUnlock(hSSFWorkbook,queryMain.subList(i, ifinal),ifinal,reportLogger);
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{
	        		//Solo un HSSFSheet
	        		writeFileExcelRelationshipBlockAndUnlock(hSSFWorkbook,queryMain,queryMain.size(),reportLogger);
	        	}
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_CLIENT_PORTFOLIO);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	         
	       	hSSFWorkbook.write(bos);
	        
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        
	        hSSFWorkbook.write(fileOut);
	        
	        fileOut.close();
			
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    } 
		return arrayByte;
	}
	
	/**
	 * issue 1179: reporte Mercado Primario y Secudario General
	 * @param parameters
	 * @param reportLogger
	 * @return
	 */
	public byte[] writeFileExcelPrimaryyMarketGral1179(Map<String, Object> parameters , ReportLogger reportLogger){
		byte[] arrayByte=null;
		
		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("lstObjects");
		
		initCellStyle();
		initFont();
		initDataFormat();

		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						writeFileExcelPrimaryyMarketGral1179(hSSFWorkbook,queryMain.subList(i, ifinal),ifinal,reportLogger);
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{
	        		//Solo un HSSFSheet
	        		writeFileExcelPrimaryyMarketGral1179(hSSFWorkbook,queryMain,queryMain.size(),reportLogger);
	        	}
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_CLIENT_PORTFOLIO);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	         
	       	hSSFWorkbook.write(bos);
	        
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        
	        hSSFWorkbook.write(fileOut);
	        
	        fileOut.close();
			
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    } 
		return arrayByte;
	}
	
	/**
	 * issue 1191: REPORTE DE TRANSFERENCIAS EXTRABURSATILES OTC SIRTEX
	 * @param parameters
	 * @param reportLogger
	 * @return
	 */
	public byte[] writeFileExcelTransferExtrabursatilSirtex(Map<String, Object> parameters , ReportLogger reportLogger){
		
		byte[] arrayByte=null;
		
		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("lstObjects");
		
		initCellStyle();
		initFont();
		initDataFormat();

		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						writeFileExcelTransferExtrabursatilSirtex(hSSFWorkbook,queryMain.subList(i, ifinal),ifinal,reportLogger);
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{
	        		//Solo un HSSFSheet
	        		writeFileExcelTransferExtrabursatilSirtex(hSSFWorkbook,queryMain,queryMain.size(),reportLogger);
	        	}
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_CLIENT_PORTFOLIO);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	         
	       	hSSFWorkbook.write(bos);
	        
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        
	        hSSFWorkbook.write(fileOut);
	        
	        fileOut.close();
			
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    } 
		return arrayByte;
	}
	
	/**
	 * metodo para la generacion de reporte de mercado primario issue 1005
	 * @param parameters
	 * @param reportLogger
	 * @return
	 */
	public byte[] writeFileExcelPrimaryMarket(Map<String, Object> parameters , ReportLogger reportLogger){
		
		byte[] arrayByte=null;
		
		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("lstObjects");
		
		initCellStyle();
		initFont();
		initDataFormat();

		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						writeFileExcelPrimaryMarket(hSSFWorkbook,queryMain.subList(i, ifinal),ifinal,reportLogger);
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{
	        		//Solo un HSSFSheet
	        		writeFileExcelPrimaryMarket(hSSFWorkbook,queryMain,queryMain.size(),reportLogger);
	        	}
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_CLIENT_PORTFOLIO);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	         
	       	hSSFWorkbook.write(bos);
	        
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        
	        hSSFWorkbook.write(fileOut);
	        
	        fileOut.close();
			
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    } 
		return arrayByte;
	}
	
	/**
	 * issue 1189: REPORTE DE RELACION DE BLOQUEOS Y DESBLOQUEOS POI
	 * @param hSSFWorkbook
	 * @param queryMain
	 * @param number
	 * @param reportLogger
	 */
	public void writeFileExcelRelationshipBlockAndUnlock(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, int number,ReportLogger reportLogger) {
		int INDEX_0FECHA_DESBLOQUEO=0;
		int INDEX_1FECHA_REGITRO=1;
		int INDEX_2CLASE_VALOR=2;
		int INDEX_3CODIGO_VALOR=3;
		int INDEX_4PARTICIPANTE=4;
		int INDEX_5CUI=5;
		int INDEX_6CTA_TITULAR=6;
		int INDEX_7TIPO_DOCUMENTO=7;
		int INDEX_8NUM_DOCUMENTO=8;
		int INDEX_9ENTIDAD_BLOQUEO=9;
		int INDEX_10TIPO_DOC_ENTIDAD_BLOQUEO=10;
		int INDEX_11NUM_DOC_ENTIDAD_BLOQUEO=11;
		int INDEX_12TIPO_BLOQUEO=12;
		int INDEX_13CANT_VALORES_BLOQUEADOS=13;
		int INDEX_14SALD_VALORES_BLOQUEADOS=14;
		int INDEX_15NIVEL_AFECTACION=15;
		int INDEX_16ESTADO=16;
		int INDEX_17FECHA_EMISION_VALOR=17;
		int INDEX_18FECHA_VENC_VALOR=18;
		int INDEX_19EMISOR=19;
		int INDEX_20USUARIO=20;
		
		//creando la hoja
		HSSFSheet sheet = hSSFWorkbook.createSheet("Relacion de Bloqueos y Desbloqueos");
		
		//cabecera del reporte
		String titleReport=reportLogger.getReport().getTitle();
		String strStartHour = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),ReportConstant.DATE_FORMAT_TIME_SECOND);
		
		HSSFRow row0= sheet.createRow((short)0);
        sheet.addMergedRegion(new CellRangeAddress(0,0,1,8));
        addContentAndStyle(row0.createCell(1),getStyleTitleBoldCenter(),titleReport);
        
        /**DATE*/
        HSSFRow row1= sheet.createRow((short)1);
        addContentAndStyle(row1.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_FECHA);
        addContentAndStyle(row1.createCell(8),CommonsUtilities.convertDatetoString(reportLogger.getRegistryDate()));
        
        /**HOUR*/
        HSSFRow row2= sheet.createRow((short)2);
        addContentAndStyle(row2.createCell(7),getStyleTitleBold(), ReportConstant.LABEL_HORA);
        addContentAndStyle(row2.createCell(8), strStartHour);
        
        /**CLASIFICATION*/
        HSSFRow row3= sheet.createRow((short)3);
        addContentAndStyle(row3.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_CLASIFICACION);
        addContentAndStyle(row3.createCell(8),reportLogger.getReport().getReportClasification());

        
        /**Usuario**/
        HSSFRow row5= sheet.createRow((short)4);
        addContentAndStyle(row5.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_USUARIO);
        addContentAndStyle(row5.createCell(8),reportLogger.getRegistryUser());
        
        int indrow=8;
        
        HSSFRow rowHeader = sheet.createRow((int)indrow);
        
        addContentAndStyle(rowHeader.createCell(INDEX_0FECHA_DESBLOQUEO),getStyleTitle(),ReportConstant.LABEL_UNLOCK_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_1FECHA_REGITRO),getStyleTitle(),ReportConstant.LABEL_LOCK_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_2CLASE_VALOR),getStyleTitle(),ReportConstant.LABEL_SECURITY_CLASS);
        addContentAndStyle(rowHeader.createCell(INDEX_3CODIGO_VALOR),getStyleTitle(),ReportConstant.LABEL_SECURITY_CLASS_CODE);
        addContentAndStyle(rowHeader.createCell(INDEX_4PARTICIPANTE),getStyleTitle(),ReportConstant.LABEL_PARTICIPANT);
        addContentAndStyle(rowHeader.createCell(INDEX_5CUI),getStyleTitle(),ReportConstant.LABEL_CUI);
        addContentAndStyle(rowHeader.createCell(INDEX_6CTA_TITULAR),getStyleTitle(),ReportConstant.LABEL_HOLDER_ACCOUNT);
        addContentAndStyle(rowHeader.createCell(INDEX_7TIPO_DOCUMENTO),getStyleTitle(),ReportConstant.LABEL_DOCUMENT_TYPE);
        addContentAndStyle(rowHeader.createCell(INDEX_8NUM_DOCUMENTO),getStyleTitle(),ReportConstant.LABEL_DOCUMENT_NUM);
        addContentAndStyle(rowHeader.createCell(INDEX_9ENTIDAD_BLOQUEO),getStyleTitle(),ReportConstant.LABEL_LOCK_ENTITY);
        addContentAndStyle(rowHeader.createCell(INDEX_10TIPO_DOC_ENTIDAD_BLOQUEO),getStyleTitle(),ReportConstant.LABEL_LOCK_ENTITY_TYPE_DOC);
        addContentAndStyle(rowHeader.createCell(INDEX_11NUM_DOC_ENTIDAD_BLOQUEO),getStyleTitle(),ReportConstant.LABEL_LOCK_ENTITY_NUM_DOC);
        addContentAndStyle(rowHeader.createCell(INDEX_12TIPO_BLOQUEO),getStyleTitle(),ReportConstant.LABEL_LOCK_TYPE);
        addContentAndStyle(rowHeader.createCell(INDEX_13CANT_VALORES_BLOQUEADOS),getStyleTitle(),ReportConstant.LABEL_QUANTITY_LOCK_SECURITIES);
        addContentAndStyle(rowHeader.createCell(INDEX_14SALD_VALORES_BLOQUEADOS),getStyleTitle(),ReportConstant.LABEL_AVAILABLE_LOCK_SECURITIES);
        addContentAndStyle(rowHeader.createCell(INDEX_15NIVEL_AFECTACION),getStyleTitle(),ReportConstant.LABEL_LEVEL_AFECTATION);
        addContentAndStyle(rowHeader.createCell(INDEX_16ESTADO),getStyleTitle(),ReportConstant.LABEL_STATE);
        addContentAndStyle(rowHeader.createCell(INDEX_17FECHA_EMISION_VALOR),getStyleTitle(),ReportConstant.LABEL_ISSUANCE_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_18FECHA_VENC_VALOR),getStyleTitle(),ReportConstant.LABEL_DUE_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_19EMISOR),getStyleTitle(),ReportConstant.LABEL_ISSUER);
        addContentAndStyle(rowHeader.createCell(INDEX_20USUARIO),getStyleTitle(),ReportConstant.LABEL_USER);
        
        HSSFRow rowData;
        
        for(Object[] rowResult:queryMain) {
        	indrow++;
        	rowData = sheet.createRow((int)indrow);
        	addContentAndStyle(rowData.createCell(INDEX_0FECHA_DESBLOQUEO), getStyleAccountingAccount(), rowResult[20] != null ? CommonsUtilities.convertDateToString((Date)(rowResult[20]),ReportConstant.DATE_FORMAT_STANDAR) : "-");
        	addContentAndStyle(rowData.createCell(INDEX_1FECHA_REGITRO), getStyleAccountingAccount(), rowResult[0] != null ? CommonsUtilities.convertDateToString((Date)(rowResult[0]),ReportConstant.DATE_FORMAT_STANDAR) : "-");
        	addContentAndStyle(rowData.createCell(INDEX_2CLASE_VALOR),getStyleAccountingAccountLeft(), rowResult[1] != null ? rowResult[1].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_3CODIGO_VALOR),getStyleAccountingAccountLeft(), rowResult[2] != null ? rowResult[2].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_4PARTICIPANTE),getStyleAccountingAccountLeft(), rowResult[3] != null ? rowResult[3].toString() : "-");
        	
        	//transformando el cui en formato numero
        	//addContentAndStyle(rowData.createCell(INDEX_5CUI),getStyleAccountingAccountLeft(), rowResult[4] != null ? rowResult[4].toString() : "-");
        	if(rowResult[4]!=null){
        		if(rowResult[4].toString().contains(",")){
        			addContentAndStyle(rowData.createCell(INDEX_5CUI),getStyleAccountingAccountOnlyNumberIssue1179(), rowResult[4] != null ? rowResult[4].toString() : "-");	
        		}else{
        			addContentAndStyle(rowData.createCell(INDEX_5CUI),getStyleAccountingAccount(), Double.parseDouble(rowResult[4].toString()));
        		}
        	}else{
        		addContentAndStyle(rowData.createCell(INDEX_5CUI),getStyleAccountingAccount(), 00.00);
        	}
        	
        	addContentAndStyle(rowData.createCell(INDEX_6CTA_TITULAR),getStyleAccountingAccount(), rowResult[5] != null ? Double.parseDouble(rowResult[5].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_7TIPO_DOCUMENTO),getStyleAccountingAccountLeft(), rowResult[6] != null ? rowResult[6].toString() : "-");
        	
        	//transformando el NIT/CI del cui en formato numero
        	//addContentAndStyle(rowData.createCell(INDEX_8NUM_DOCUMENTO),getStyleAccountingAccountLeft(), rowResult[7] != null ? rowResult[7].toString() : "-");
        	if(rowResult[7]!=null){
        		// si no logra parsear a double, significa q contiene una (,) o una letra ala tratarse de CI/NIT 
        		try {
        			addContentAndStyle(rowData.createCell(INDEX_8NUM_DOCUMENTO),getStyleAccountingAccount(), Double.parseDouble(rowResult[7].toString()));
				} catch (Exception e) {
					addContentAndStyle(rowData.createCell(INDEX_8NUM_DOCUMENTO),getStyleAccountingAccountOnlyNumberIssue1179(), rowResult[7] != null ? rowResult[7].toString() : "-");
				}
        	}else{
        		addContentAndStyle(rowData.createCell(INDEX_8NUM_DOCUMENTO),getStyleAccountingAccount(), 00.00);
        	}
        	
        	addContentAndStyle(rowData.createCell(INDEX_9ENTIDAD_BLOQUEO),getStyleAccountingAccountLeft(), rowResult[8] != null ? rowResult[8].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_10TIPO_DOC_ENTIDAD_BLOQUEO),getStyleAccountingAccountLeft(), rowResult[9] != null ? rowResult[9].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_11NUM_DOC_ENTIDAD_BLOQUEO),getStyleAccountingAccount(), rowResult[10] != null ? Double.parseDouble(rowResult[10].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_12TIPO_BLOQUEO),getStyleAccountingAccountLeft(), rowResult[11] != null ? rowResult[11].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_13CANT_VALORES_BLOQUEADOS),getStyleAccountingAccount(), rowResult[12] != null ? Double.parseDouble(rowResult[12].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_14SALD_VALORES_BLOQUEADOS),getStyleAccountingAccount(), rowResult[13] != null ? Double.parseDouble(rowResult[13].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_15NIVEL_AFECTACION),getStyleAccountingAccountLeft(), rowResult[14] != null ? rowResult[14].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_16ESTADO),getStyleAccountingAccountLeft(), rowResult[15] != null ? rowResult[15].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_17FECHA_EMISION_VALOR),getStyleAccountingAccount(),rowResult[16] != null ? CommonsUtilities.convertDateToString((Date)(rowResult[16]),ReportConstant.DATE_FORMAT_STANDAR) : "-");
        	addContentAndStyle(rowData.createCell(INDEX_18FECHA_VENC_VALOR),getStyleAccountingAccount(),rowResult[17] != null ? CommonsUtilities.convertDateToString((Date)(rowResult[17]),ReportConstant.DATE_FORMAT_STANDAR) : "-");
        	addContentAndStyle(rowData.createCell(INDEX_19EMISOR),getStyleAccountingAccountLeft(), rowResult[18] != null ? rowResult[18].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_20USUARIO),getStyleAccountingAccountLeft(), rowResult[19] != null ? rowResult[19].toString() : "-");
        	
        }
	}
	
	public void writeFileExcelPrimaryyMarketGral1179(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, int number,ReportLogger reportLogger) {
		int INDEX_EMISOR=0;
		int INDEX_TIPO_INST=1;
		int INDEX_CLAVE_VALOR=2;
		int INDEX_CUI=3;
		int INDEX_FEC_EMISION=4;
		int INDEX_FEC_HR_REGISTRO=5;
		int INDEX_CANTIDAD=6;
		int INDEX_FEC_VENC=7;
		int INDEX_MON_TOTAL=8;
		int INDEX_TASA_NOMINAL=9;
		int INDEX_MONEDA=10;
		int INDEX_RAZON_SOCIAL=11;
		int INDEX_NIT=12;
		
		//creando la hoja
		HSSFSheet sheet = hSSFWorkbook.createSheet("Mercado Primario Gral.");
		
		//cabecera del reporte
//		String titleReport=reportLogger.getReport().getTitle();
//		String strStartHour = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),ReportConstant.DATE_FORMAT_TIME_SECOND);
//		
//		HSSFRow row0= sheet.createRow((short)0);
//        sheet.addMergedRegion(new CellRangeAddress(0,0,1,8));
//        addContentAndStyle(row0.createCell(1),getStyleTitleBoldCenter(),titleReport);
//        
//        /**DATE*/
//        HSSFRow row1= sheet.createRow((short)1);
//        addContentAndStyle(row1.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_FECHA);
//        addContentAndStyle(row1.createCell(8),CommonsUtilities.convertDatetoString(reportLogger.getRegistryDate()));
//        
//        /**HOUR*/
//        HSSFRow row2= sheet.createRow((short)2);
//        addContentAndStyle(row2.createCell(7),getStyleTitleBold(), ReportConstant.LABEL_HORA);
//        addContentAndStyle(row2.createCell(8), strStartHour);
//        
//        /**CLASIFICATION*/
//        HSSFRow row3= sheet.createRow((short)3);
//        addContentAndStyle(row3.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_CLASIFICACION);
//        addContentAndStyle(row3.createCell(8),reportLogger.getReport().getReportClasification());
//
//        
//        /**Usuario**/
//        HSSFRow row5= sheet.createRow((short)4);
//        addContentAndStyle(row5.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_USUARIO);
//        addContentAndStyle(row5.createCell(8),reportLogger.getRegistryUser());
        
        int indrow=0;
        
        HSSFRow rowHeader = sheet.createRow((int)indrow);
        addContentAndStyle(rowHeader.createCell(INDEX_EMISOR),getStyleTitle(),ReportConstant.LABEL_ISSUER);
        addContentAndStyle(rowHeader.createCell(INDEX_TIPO_INST),getStyleTitle(),ReportConstant.LABEL_INSTRUMENT_TYPE);
        addContentAndStyle(rowHeader.createCell(INDEX_CLAVE_VALOR),getStyleTitle(),ReportConstant.LABEL_SECURITY_CODE);
        addContentAndStyle(rowHeader.createCell(INDEX_CUI),getStyleTitle(),ReportConstant.LABEL_CUI);
        addContentAndStyle(rowHeader.createCell(INDEX_FEC_EMISION),getStyleTitle(),ReportConstant.LABEL_ISSUANCE_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_FEC_HR_REGISTRO),getStyleTitle(),ReportConstant.LABEL_DATETIME_REGISTER);
        addContentAndStyle(rowHeader.createCell(INDEX_CANTIDAD),getStyleTitle(),ReportConstant.TAG_CANTIDAD);
        addContentAndStyle(rowHeader.createCell(INDEX_FEC_VENC),getStyleTitle(),ReportConstant.LABEL_DUE_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_MON_TOTAL),getStyleTitle(),ReportConstant.LABEL_TOTAL_AMOUNT);
        addContentAndStyle(rowHeader.createCell(INDEX_TASA_NOMINAL),getStyleTitle(),ReportConstant.LABEL_RATE_NOMINAL);
        addContentAndStyle(rowHeader.createCell(INDEX_MONEDA),getStyleTitle(),ReportConstant.LABEL_CURRENCY);
        addContentAndStyle(rowHeader.createCell(INDEX_RAZON_SOCIAL),getStyleTitle(),ReportConstant.LABEL_BUSSINESS_NAME);
        addContentAndStyle(rowHeader.createCell(INDEX_NIT),getStyleTitle(),ReportConstant.LABEL_NIT);
        
        HSSFRow rowData;
        
        for(Object[] rowResult:queryMain) {
        	indrow++;
        	rowData = sheet.createRow((int)indrow);
        	addContentAndStyle(rowData.createCell(INDEX_EMISOR),getStyleAccountingAccountLeft(), rowResult[0] != null ? rowResult[0].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_TIPO_INST),getStyleAccountingAccountLeft(), rowResult[1] != null ? rowResult[1].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_CLAVE_VALOR),getStyleAccountingAccountLeft(), rowResult[2] != null ? rowResult[2].toString() : "-");
        	
        	//transformando el cui en formato numero
        	//addContentAndStyle(rowData.createCell(INDEX_CUI),getStyleAccountingAccount(), rowResult[3] != null ? rowResult[3].toString() : "-");
        	if(rowResult[3]!=null){
        		if(rowResult[3].toString().contains(",")){
        			addContentAndStyle(rowData.createCell(INDEX_CUI),getStyleAccountingAccountOnlyNumberIssue1179(), rowResult[3] != null ? rowResult[3].toString() : "-");	
        		}else{
        			addContentAndStyle(rowData.createCell(INDEX_CUI),getStyleAccountingAccount(), Double.parseDouble(rowResult[3].toString()));
        		}
        	}else{
        		addContentAndStyle(rowData.createCell(INDEX_CUI),getStyleAccountingAccount(), 00.00);
        	}
        	
        	
        	addContentAndStyle(rowData.createCell(INDEX_FEC_EMISION),getStyleAccountingAccount(), rowResult[4] != null ? rowResult[4].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_FEC_HR_REGISTRO),getStyleAccountingAccount(), rowResult[5] != null ? rowResult[5].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_CANTIDAD),getStyleAccountingAccount(), rowResult[6] != null ? Double.parseDouble(rowResult[6].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_FEC_VENC),getStyleAccountingAccount(), rowResult[7] != null ? rowResult[7].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_MON_TOTAL),getStyleAccountingAccountAmountIssue1091(), rowResult[8] != null ? Double.parseDouble(rowResult[8].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_TASA_NOMINAL),getStyleAccountingAccountAmountIssue1091(), rowResult[9] != null ? Double.parseDouble(rowResult[9].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_MONEDA),getStyleAccountingAccountLeft(), rowResult[10] != null ? rowResult[10].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_RAZON_SOCIAL),getStyleAccountingAccountLeft(), rowResult[11] != null ? rowResult[11].toString() : "-");
        	
        	//transformando el NIT en formato numero
        	//addContentAndStyle(rowData.createCell(INDEX_NIT),getStyleAccountingAccountLeft(), rowResult[12] != null ? rowResult[12].toString() : "-");
        	if(rowResult[12]!=null){
        		// si no logra parsear a double, significa q contiene una (,) o una letra ala tratarse de CI/NIT 
        		try {
        			addContentAndStyle(rowData.createCell(INDEX_NIT),getStyleAccountingAccount(), Double.parseDouble(rowResult[12].toString()));
				} catch (Exception e) {
					addContentAndStyle(rowData.createCell(INDEX_NIT),getStyleAccountingAccountOnlyNumberIssue1179(), rowResult[12] != null ? rowResult[12].toString() : "-");
				}
        	}else{
        		addContentAndStyle(rowData.createCell(INDEX_NIT),getStyleAccountingAccount(), 00.00);
        	}
        }
	}

	/**
	 * issue 1191: REPORTE DE TRANSFERENCIAS EXTRABURSATILES OTC SIRTEX
	 * @param hSSFWorkbook
	 * @param queryMain
	 * @param number
	 * @param reportLogger
	 */
	public void writeFileExcelTransferExtrabursatilSirtex(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, int number,ReportLogger reportLogger) {
		
		int INDEX_01FECHA=1;
		int INDEX_02PART_COMP=2;
		int INDEX_03PART_VEND=3;
		int INDEX_04CLIENT_COMP=4;
		int INDEX_05CLIENT_VEND=5;
		int INDEX_06CUI_COMP=6;
		int INDEX_07ACT_CUI_COMP=7;
		int INDEX_08CUI_VEND=8;
		int INDEX_09ACT_CUI_VEND=9;
		int INDEX_10SEC_CLASS=10;
		int INDEX_11ID_SEC_CLASS_PK=11;
		int INDEX_12EMISOR=12;
		int INDEX_13MONEDA=13;
		int INDEX_14FECHA_EMISION=14;
		int INDEX_15FECHA_VENC=15;
		int INDEX_16PLAZO_RESTANTE=16;
		int INDEX_17FORMA_PAGO=17;
		int INDEX_18VAL_NOM_INICIAL=18;
		int INDEX_19VAL_NOM_ACTUAL=19;
		int INDEX_20CANT_OPERACION=20;
		int INDEX_21TASA_EMISION=21;
		int INDEX_22PLAZO_REPORTO=22;
		int INDEX_23TIPO_PLAZO=23;
		int INDEX_25MECANISMO=24;
		int INDEX_26MODALIDAD=25;
		int INDEX_27VAL_PUB_PRIV=26;
		int INDEX_28VAL_NOM_INI_USD=27;
		int INDEX_29VAL_NOM_ACT_USD=28;
		int INDEX_30FECHA_LIQ_CONTADO=29;
		int INDEX_31FECHA_LIQ_PLAZO=30;
		
		//creando la hoja
		HSSFSheet sheet = hSSFWorkbook.createSheet("Transf. Extrabursatil OTC-SIRTEX");
		
		//cabecera del reporte
		String titleReport=reportLogger.getReport().getTitle();
		String strStartHour = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),ReportConstant.DATE_FORMAT_TIME_SECOND);
		
		HSSFRow row0= sheet.createRow((short)0);
        sheet.addMergedRegion(new CellRangeAddress(0,0,1,8));
        addContentAndStyle(row0.createCell(1),getStyleTitleBoldCenter(),titleReport);
        
        /**DATE*/
        HSSFRow row1= sheet.createRow((short)1);
        addContentAndStyle(row1.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_FECHA);
        addContentAndStyle(row1.createCell(8),CommonsUtilities.convertDatetoString(reportLogger.getRegistryDate()));
        
        /**HOUR*/
        HSSFRow row2= sheet.createRow((short)2);
        addContentAndStyle(row2.createCell(7),getStyleTitleBold(), ReportConstant.LABEL_HORA);
        addContentAndStyle(row2.createCell(8), strStartHour);
        
        /**CLASIFICATION*/
        HSSFRow row3= sheet.createRow((short)3);
        addContentAndStyle(row3.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_CLASIFICACION);
        addContentAndStyle(row3.createCell(8),reportLogger.getReport().getReportClasification());

        
        /**Usuario**/
        HSSFRow row5= sheet.createRow((short)4);
        addContentAndStyle(row5.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_USUARIO);
        addContentAndStyle(row5.createCell(8),reportLogger.getRegistryUser());
        
        int indrow=8;
        
        HSSFRow rowHeader = sheet.createRow((int)indrow);
        addContentAndStyle(rowHeader.createCell(INDEX_01FECHA),getStyleTitle(),ReportConstant.LABEL_OPERATION_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_02PART_COMP),getStyleTitle(),ReportConstant.LABEL_BUY_PARTICIPANT);
        addContentAndStyle(rowHeader.createCell(INDEX_03PART_VEND),getStyleTitle(),ReportConstant.LABEL_SELL_PARTICIPANT);
        addContentAndStyle(rowHeader.createCell(INDEX_04CLIENT_COMP),getStyleTitle(),ReportConstant.LABEL_CLIENT_BUY);
        addContentAndStyle(rowHeader.createCell(INDEX_05CLIENT_VEND),getStyleTitle(),ReportConstant.LABEL_CLIENT_SELL);
        addContentAndStyle(rowHeader.createCell(INDEX_06CUI_COMP),getStyleTitle(),ReportConstant.LABEL_BUY_CUI);
        addContentAndStyle(rowHeader.createCell(INDEX_07ACT_CUI_COMP),getStyleTitle(),ReportConstant.LABEL_BUY_CUI_ACT_ECO);
        addContentAndStyle(rowHeader.createCell(INDEX_08CUI_VEND),getStyleTitle(),ReportConstant.LABEL_SELL_CUI);
        addContentAndStyle(rowHeader.createCell(INDEX_09ACT_CUI_VEND),getStyleTitle(),ReportConstant.LABEL_SELL_CUI_ACT_ECO);
        addContentAndStyle(rowHeader.createCell(INDEX_10SEC_CLASS),getStyleTitle(),ReportConstant.LABEL_SECURITY_CLASS);
        addContentAndStyle(rowHeader.createCell(INDEX_11ID_SEC_CLASS_PK),getStyleTitle(),ReportConstant.LABEL_SECURITY_CODE);
        addContentAndStyle(rowHeader.createCell(INDEX_12EMISOR),getStyleTitle(),ReportConstant.LABEL_ISSUER);
        addContentAndStyle(rowHeader.createCell(INDEX_13MONEDA),getStyleTitle(),ReportConstant.LABEL_CURRENCY);
        addContentAndStyle(rowHeader.createCell(INDEX_14FECHA_EMISION),getStyleTitle(),ReportConstant.LABEL_ISSUANCE_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_15FECHA_VENC),getStyleTitle(),ReportConstant.LABEL_DUE_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_16PLAZO_RESTANTE),getStyleTitle(),ReportConstant.LABEL_TERM_REMAINING);
        addContentAndStyle(rowHeader.createCell(INDEX_17FORMA_PAGO),getStyleTitle(),ReportConstant.LABEL_WAY_TO_PAY);
        addContentAndStyle(rowHeader.createCell(INDEX_18VAL_NOM_INICIAL),getStyleTitle(),ReportConstant.LABEL_VN_INITIAL);
        addContentAndStyle(rowHeader.createCell(INDEX_19VAL_NOM_ACTUAL),getStyleTitle(),ReportConstant.LABEL_VN_CURRENT);
        addContentAndStyle(rowHeader.createCell(INDEX_20CANT_OPERACION),getStyleTitle(),ReportConstant.LABEL_STOCK_QUANTITY);
        addContentAndStyle(rowHeader.createCell(INDEX_21TASA_EMISION),getStyleTitle(),ReportConstant.LABEL_RATE_ISSUANCE);
        addContentAndStyle(rowHeader.createCell(INDEX_22PLAZO_REPORTO),getStyleTitle(),ReportConstant.LABEL_TERM_REPORTO);
        addContentAndStyle(rowHeader.createCell(INDEX_23TIPO_PLAZO),getStyleTitle(),ReportConstant.LABEL_TYPE_TERM);
        addContentAndStyle(rowHeader.createCell(INDEX_25MECANISMO),getStyleTitle(),ReportConstant.LABEL_MECHANISM_NEG);
        addContentAndStyle(rowHeader.createCell(INDEX_26MODALIDAD),getStyleTitle(),ReportConstant.LABEL_MODALITY_NEG);
        addContentAndStyle(rowHeader.createCell(INDEX_27VAL_PUB_PRIV),getStyleTitle(),ReportConstant.LABEL_SECURITY_PUBLIC_PRIVATE);
        addContentAndStyle(rowHeader.createCell(INDEX_28VAL_NOM_INI_USD),getStyleTitle(),ReportConstant.LABEL_VN_INITIAL_USD);
        addContentAndStyle(rowHeader.createCell(INDEX_29VAL_NOM_ACT_USD),getStyleTitle(),ReportConstant.LABEL_VN_CURRENT_USD);
        addContentAndStyle(rowHeader.createCell(INDEX_30FECHA_LIQ_CONTADO),getStyleTitle(),ReportConstant.LABEL_SETTLEMENT_COUNT);
        addContentAndStyle(rowHeader.createCell(INDEX_31FECHA_LIQ_PLAZO),getStyleTitle(),ReportConstant.LABEL_SETTLEMENT_TERM);
        
        HSSFRow rowData;
        
        for(Object[] rowResult:queryMain) {
        	
        	indrow++;
        	rowData = sheet.createRow((int)indrow);
        	addContentAndStyle(rowData.createCell(INDEX_01FECHA),getStyleAccountingAccount(), rowResult[0] != null ? CommonsUtilities.convertDateToString((Date)(rowResult[0]),ReportConstant.DATE_FORMAT_STANDAR) : "-");
        	addContentAndStyle(rowData.createCell(INDEX_02PART_COMP),getStyleAccountingAccountLeft(), rowResult[1] != null ? rowResult[1].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_03PART_VEND),getStyleAccountingAccountLeft(), rowResult[2] != null ? rowResult[2].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_04CLIENT_COMP),getStyleAccountingAccountLeft(), rowResult[3] != null ? rowResult[3].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_05CLIENT_VEND),getStyleAccountingAccountLeft(), rowResult[4] != null ? rowResult[4].toString() : "-");
        	
        	//transformando el cui en formato numero
        	//addContentAndStyle(rowData.createCell(INDEX_06CUI_COMP),getStyleAccountingAccount(), rowResult[5] != null ? rowResult[5].toString() : "-");
        	if(rowResult[5]!=null){
        		if(rowResult[5].toString().contains(",")){
        			addContentAndStyle(rowData.createCell(INDEX_06CUI_COMP),getStyleAccountingAccountOnlyNumberIssue1179(), rowResult[5] != null ? rowResult[5].toString() : "-");	
        		}else{
        			addContentAndStyle(rowData.createCell(INDEX_06CUI_COMP),getStyleAccountingAccount(), Double.parseDouble(rowResult[5].toString()));
        		}
        	}else{
        		addContentAndStyle(rowData.createCell(INDEX_06CUI_COMP),getStyleAccountingAccount(), 00.00);
        	}
        	
        	addContentAndStyle(rowData.createCell(INDEX_07ACT_CUI_COMP),getStyleAccountingAccountLeft(), rowResult[6] != null ? rowResult[6].toString() : "-");
        	
        	//transformando el cui en formato numero
        	//addContentAndStyle(rowData.createCell(INDEX_08CUI_VEND),getStyleAccountingAccountLeft(), rowResult[6] != null ? rowResult[6].toString() : "-");
        	if(rowResult[7]!=null){
        		if(rowResult[7].toString().contains(",")){
        			addContentAndStyle(rowData.createCell(INDEX_08CUI_VEND),getStyleAccountingAccountOnlyNumberIssue1179(), rowResult[7] != null ? rowResult[7].toString() : "-");	
        		}else{
        			addContentAndStyle(rowData.createCell(INDEX_08CUI_VEND),getStyleAccountingAccount(), Double.parseDouble(rowResult[7].toString()));
        		}
        	}else{
        		addContentAndStyle(rowData.createCell(INDEX_08CUI_VEND),getStyleAccountingAccount(), 00.00);
        	}
        	
        	addContentAndStyle(rowData.createCell(INDEX_09ACT_CUI_VEND),getStyleAccountingAccountLeft(), rowResult[8] != null ? rowResult[8].toString() : "-");
        	
        	addContentAndStyle(rowData.createCell(INDEX_10SEC_CLASS),getStyleAccountingAccountLeft(), rowResult[9] != null ? rowResult[9].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_11ID_SEC_CLASS_PK),getStyleAccountingAccountLeft(), rowResult[10] != null ? rowResult[10].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_12EMISOR),getStyleAccountingAccountLeft(), rowResult[11] != null ? rowResult[11].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_13MONEDA),getStyleAccountingAccountLeft(), rowResult[12] != null ? rowResult[12].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_14FECHA_EMISION),getStyleAccountingAccount(), rowResult[13] != null ? CommonsUtilities.convertDateToString((Date)(rowResult[13]),ReportConstant.DATE_FORMAT_STANDAR) : "-");
        	addContentAndStyle(rowData.createCell(INDEX_15FECHA_VENC),getStyleAccountingAccount(), rowResult[14] != null ? CommonsUtilities.convertDateToString((Date)(rowResult[14]),ReportConstant.DATE_FORMAT_STANDAR) : "-");
        	addContentAndStyle(rowData.createCell(INDEX_16PLAZO_RESTANTE),getStyleAccountingAccount(), rowResult[15] != null ? Double.parseDouble(rowResult[15].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_17FORMA_PAGO),getStyleAccountingAccountLeft(), rowResult[16] != null ? rowResult[16].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_18VAL_NOM_INICIAL),getStyleAccountingAccountAmountIssue1091(), rowResult[17] != null ? Double.parseDouble(rowResult[17].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_19VAL_NOM_ACTUAL),getStyleAccountingAccountAmountIssue1091(), rowResult[18] != null ? Double.parseDouble(rowResult[18].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_20CANT_OPERACION),getStyleAccountingAccountOnlyNumberIssue1179(), rowResult[19] != null ? Double.parseDouble(rowResult[19].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_21TASA_EMISION),getStyleAccountingAccountAmountIssue1091(), rowResult[20] != null ? Double.parseDouble(rowResult[20].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_22PLAZO_REPORTO),getStyleAccountingAccount(), rowResult[21] != null ? Double.parseDouble(rowResult[21].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_23TIPO_PLAZO),getStyleAccountingAccountLeft(), rowResult[22] != null ? rowResult[22].toString() : "-");
        	
        	addContentAndStyle(rowData.createCell(INDEX_25MECANISMO),getStyleAccountingAccountLeft(), rowResult[23] != null ? rowResult[23].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_26MODALIDAD),getStyleAccountingAccountLeft(), rowResult[24] != null ? rowResult[24].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_27VAL_PUB_PRIV),getStyleAccountingAccountLeft(), rowResult[25] != null ? rowResult[25].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_28VAL_NOM_INI_USD),getStyleAccountingAccountAmountIssue1091(), rowResult[26] != null ? Double.parseDouble(rowResult[26].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_29VAL_NOM_ACT_USD),getStyleAccountingAccountAmountIssue1091(), rowResult[27] != null ? Double.parseDouble(rowResult[27].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_30FECHA_LIQ_CONTADO),getStyleAccountingAccount(), rowResult[28] != null ? CommonsUtilities.convertDateToString((Date)(rowResult[28]),ReportConstant.DATE_FORMAT_STANDAR) : "-");
        	addContentAndStyle(rowData.createCell(INDEX_31FECHA_LIQ_PLAZO),getStyleAccountingAccount(), rowResult[29] != null ? CommonsUtilities.convertDateToString((Date)(rowResult[29]),ReportConstant.DATE_FORMAT_STANDAR) : "-");
        	
        }
	}
	
	/**
	 * metodo para la generacion de reporte de mercado primario en Apache POI (issue 1005)
	 * @param hSSFWorkbook
	 * @param queryMain
	 * @param number
	 * @param reportLogger
	 */
	public void writeFileExcelPrimaryMarket(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, int number,ReportLogger reportLogger) {
		
		int INDEX_DATE_OPERATION=1;
		int INDEX_AGENCY_BUYER=2;
		int INDEX_CUI_BUYER=3;
		int INDEX_CUI_BUYER_NAME=4;
		int INDEX_SECURITY_CODE=5;
		int INDEX_ISSUER=6;
		int INDEX_ISSUER_JURIDICAL_NAME=7;
		int INDEX_ISSUANCE_DATE=8;
		int INDEX_SECURITY_TERM=9;
		int INDEX_DATE_EXPIDERD=10;
		int INDEX_CURRENCY=11;
		int INDEX_NOMINAL_VALUE=12;
		int INDEX_INTEREST_RATE=13;
		int INDEX_NEGOTIATION_RATE=14;
		int INDEX_NEGOTIATION_PRICE=15;
		int INDEX_PERIOD_PAYMENT_INTERES=16;
		int INDEX_PERIOD_PAYMENT_AMORTIZATION=17;
		int INDEX_DATE_PAYMENT_INTEREST=18;
		int INDEX_DATE_PAYMENT_AMORTIZATION=19;
		int INDEX_QUANTITY=20;
		int INDEX_CP=21;
		
		
		//creando la hoja
		HSSFSheet sheet = hSSFWorkbook.createSheet("Mercado Primario");
		
		//cabecera del reporte
		String titleReport=reportLogger.getReport().getTitle();
		String strStartHour = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),ReportConstant.DATE_FORMAT_TIME_SECOND);
		
		HSSFRow row0= sheet.createRow((short)0);
        sheet.addMergedRegion(new CellRangeAddress(0,0,1,8));
        addContentAndStyle(row0.createCell(1),getStyleTitleBoldCenter(),titleReport);
        
        /**DATE*/
        HSSFRow row1= sheet.createRow((short)1);
        addContentAndStyle(row1.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_FECHA);
        addContentAndStyle(row1.createCell(8),CommonsUtilities.convertDatetoString(reportLogger.getRegistryDate()));
        
        /**HOUR*/
        HSSFRow row2= sheet.createRow((short)2);
        addContentAndStyle(row2.createCell(7),getStyleTitleBold(), ReportConstant.LABEL_HORA);
        addContentAndStyle(row2.createCell(8), strStartHour);
        
        /**CLASIFICATION*/
        HSSFRow row3= sheet.createRow((short)3);
        addContentAndStyle(row3.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_CLASIFICACION);
        addContentAndStyle(row3.createCell(8),reportLogger.getReport().getReportClasification());

        
        /**Usuario**/
        HSSFRow row5= sheet.createRow((short)4);
        addContentAndStyle(row5.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_USUARIO);
        addContentAndStyle(row5.createCell(8),reportLogger.getRegistryUser());
        
        int indrow=8;
        
        HSSFRow rowHeader = sheet.createRow((int)indrow);
        addContentAndStyle(rowHeader.createCell(INDEX_DATE_OPERATION),getStyleTitle(),ReportConstant.LABEL_OPERATION_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_AGENCY_BUYER),getStyleTitle(),ReportConstant.LABEL_BUY_PARTICIPANT);
        addContentAndStyle(rowHeader.createCell(INDEX_CUI_BUYER),getStyleTitle(),"CUI");
        addContentAndStyle(rowHeader.createCell(INDEX_CUI_BUYER_NAME),getStyleTitle(),ReportConstant.LABEL_BUY_CUI_NAME);
        addContentAndStyle(rowHeader.createCell(INDEX_SECURITY_CODE),getStyleTitle(),ReportConstant.LABEL_SECURITY_CLASS_CODE);
        addContentAndStyle(rowHeader.createCell(INDEX_ISSUER),getStyleTitle(),ReportConstant.LABEL_ISSUER);
        addContentAndStyle(rowHeader.createCell(INDEX_ISSUER_JURIDICAL_NAME),getStyleTitle(),ReportConstant.LABEL_BUSINESS_NAME);
        addContentAndStyle(rowHeader.createCell(INDEX_ISSUANCE_DATE),getStyleTitle(),ReportConstant.LABEL_ISSUANCE_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_SECURITY_TERM),getStyleTitle(),ReportConstant.LABEL_SECURITY_TERM);
        addContentAndStyle(rowHeader.createCell(INDEX_DATE_EXPIDERD),getStyleTitle(),ReportConstant.LABEL_DUE_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_CURRENCY),getStyleTitle(),ReportConstant.LABEL_CURRENCY);
        addContentAndStyle(rowHeader.createCell(INDEX_NOMINAL_VALUE),getStyleTitle(),ReportConstant.LABEL_NOMINAL_VALUE);
        addContentAndStyle(rowHeader.createCell(INDEX_INTEREST_RATE),getStyleTitle(),ReportConstant.LABEL_RATE_PERCENT);
        addContentAndStyle(rowHeader.createCell(INDEX_NEGOTIATION_RATE),getStyleTitle(),ReportConstant.LABEL_NEGOTIATION_RATE);
        addContentAndStyle(rowHeader.createCell(INDEX_NEGOTIATION_PRICE),getStyleTitle(),ReportConstant.LABEL_OPERATION_PRICE);
        addContentAndStyle(rowHeader.createCell(INDEX_PERIOD_PAYMENT_INTERES),getStyleTitle(),ReportConstant.LABEL_PERIOD_PAYMENT_INTERES);
        addContentAndStyle(rowHeader.createCell(INDEX_PERIOD_PAYMENT_AMORTIZATION),getStyleTitle(),ReportConstant.LABEL_PERIOD_PAYMENT_AMORTIZATION);
        addContentAndStyle(rowHeader.createCell(INDEX_DATE_PAYMENT_INTEREST),getStyleTitle(),ReportConstant.LABEL_DATE_PAYMENT_INTEREST);
        addContentAndStyle(rowHeader.createCell(INDEX_DATE_PAYMENT_AMORTIZATION),getStyleTitle(),ReportConstant.LABEL_DATE_PAYMENT_AMORTIZATION);
        addContentAndStyle(rowHeader.createCell(INDEX_QUANTITY),getStyleTitle(),ReportConstant.LABEL_STOCK_QUANTITY);
        addContentAndStyle(rowHeader.createCell(INDEX_CP),getStyleTitle(),ReportConstant.LABEL_CP);
        
        HSSFRow rowData;
        
        for(Object[] rowResult:queryMain) {
        	
        	indrow++;
        	rowData = sheet.createRow((int)indrow);
        	addContentAndStyle(rowData.createCell(INDEX_DATE_OPERATION),getStyleAccountingAccount(), rowResult[0] != null ? rowResult[0].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_AGENCY_BUYER),getStyleAccountingAccount(), rowResult[1] != null ? rowResult[1].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_CUI_BUYER),getStyleAccountingAccount(), rowResult[2] != null ? rowResult[2].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_CUI_BUYER_NAME),getStyleAccountingAccount(), rowResult[3] != null ? rowResult[3].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_SECURITY_CODE),getStyleAccountingAccount(), rowResult[4] != null ? rowResult[4].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_ISSUER),getStyleAccountingAccount(), rowResult[5] != null ? rowResult[5].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_ISSUER_JURIDICAL_NAME),getStyleAccountingAccount(), rowResult[6] != null ? rowResult[6].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_ISSUANCE_DATE),getStyleAccountingAccount(), rowResult[7] != null ? rowResult[7].toString() : "-");
        	//TODO colocar formato para numero si asi lo requiere el usuario
        	addContentAndStyle(rowData.createCell(INDEX_SECURITY_TERM),getStyleAccountingAccount(), rowResult[8] != null ? rowResult[8].toString() : "0");
        	addContentAndStyle(rowData.createCell(INDEX_DATE_EXPIDERD),getStyleAccountingAccount(), rowResult[9] != null ? rowResult[9].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_CURRENCY),getStyleAccountingAccount(), rowResult[10] != null ? rowResult[10].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_NOMINAL_VALUE),getStyleAccountingAccount(), rowResult[11] != null ? rowResult[11].toString() : "0");
        	addContentAndStyle(rowData.createCell(INDEX_INTEREST_RATE),getStyleAccountingAccount(), rowResult[12] != null ? rowResult[12].toString() : "0");
        	addContentAndStyle(rowData.createCell(INDEX_NEGOTIATION_RATE),getStyleAccountingAccount(), rowResult[13] != null ? rowResult[13].toString() : "0");
        	addContentAndStyle(rowData.createCell(INDEX_NEGOTIATION_PRICE),getStyleAccountingAccount(), rowResult[14] != null ? rowResult[14].toString() : "0");
        	addContentAndStyle(rowData.createCell(INDEX_PERIOD_PAYMENT_INTERES),getStyleAccountingAccount(), rowResult[15] != null ? rowResult[15].toString() : "0");
        	addContentAndStyle(rowData.createCell(INDEX_PERIOD_PAYMENT_AMORTIZATION),getStyleAccountingAccount(), rowResult[16] != null ?  rowResult[16].toString() : "0");
        	addContentAndStyle(rowData.createCell(INDEX_DATE_PAYMENT_INTEREST),getStyleAccountingAccount(), rowResult[17] != null ? CommonsUtilities.convertDateToString((Date)(rowResult[17]),ReportConstant.DATE_FORMAT_STANDAR) : "-");
        	addContentAndStyle(rowData.createCell(INDEX_DATE_PAYMENT_AMORTIZATION),getStyleAccountingAccount(), rowResult[18] != null ? CommonsUtilities.convertDateToString((Date)(rowResult[18]),ReportConstant.DATE_FORMAT_STANDAR) : "-");
        	addContentAndStyle(rowData.createCell(INDEX_QUANTITY),getStyleAccountingAccount(), rowResult[19] != null ? rowResult[19].toString() : "0");
        	addContentAndStyle(rowData.createCell(INDEX_CP),getStyleAccountingAccount(), rowResult[20] != null ? rowResult[20].toString() : "-");
        }
	}
	
public void writeFileExcelCommissionsAnnotationAccountNonBcb(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, int number,ReportLogger reportLogger) {
		
		int INDEX_SERVICIO=1;
		int INDEX_DATE=2;
		int INDEX_CUI=3;
		int INDEX_DESCRIPTION_CUI=4;
		int INDEX_SECURITY_CLASS=5;
		int INDEX_CURRENCY=6;
		int INDEX_CURRENCY_ORIGINAL=7;
		int INDEX_TOTAL_USD=8;
		int INDEX_CODIGO=9;
		int INDEX_TARIFA=10;
		int INDEX_COMISION_USD=11;
		int INDEX_EXCHANGE_RATE=12;
		int INDEX_COMISION_BOB=13;
	
		
		/**Created a new sheet within the workBock**/
    	HSSFSheet sheet = hSSFWorkbook.createSheet("CommissionsAnnotationAccount " + number);
    	
		/*****
         * BEGIN - REPORT HEADER 
         */
    	
    	String titleReport=reportLogger.getReport().getTitle();
    	String strStartHour = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),ReportConstant.DATE_FORMAT_TIME_SECOND);
    	
    	/**TITLE*/
        HSSFRow row0= sheet.createRow((short)0);
        sheet.addMergedRegion(new CellRangeAddress(0,0,1,8));
        addContentAndStyle(row0.createCell(1),getStyleTitleBoldCenter(),titleReport);
        
        /**DATE*/
        HSSFRow row1= sheet.createRow((short)1);
        addContentAndStyle(row1.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_FECHA);
        addContentAndStyle(row1.createCell(8),CommonsUtilities.convertDatetoString(reportLogger.getRegistryDate()));
        
        /**HOUR*/
        HSSFRow row2= sheet.createRow((short)2);
        addContentAndStyle(row2.createCell(7),getStyleTitleBold(), ReportConstant.LABEL_HORA);
        addContentAndStyle(row2.createCell(8), strStartHour);
        
        /**CLASIFICATION*/
        HSSFRow row3= sheet.createRow((short)3);
        addContentAndStyle(row3.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_CLASIFICACION);
        addContentAndStyle(row3.createCell(8),reportLogger.getReport().getReportClasification());

        
        /**Usuario**/
        HSSFRow row5= sheet.createRow((short)4);
        addContentAndStyle(row5.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_USUARIO);
        addContentAndStyle(row5.createCell(8),reportLogger.getRegistryUser());
        
        /****
         * END - REPORT HEADER 
         */
        
        int indrow=8;

        /**Created Header**/
        HSSFRow rowHeader = sheet.createRow((int)indrow);
        addContentAndStyle(rowHeader.createCell(INDEX_SERVICIO),getStyleTitle(),ReportConstant.LABEL_SERVICE);
        addContentAndStyle(rowHeader.createCell(INDEX_DATE),getStyleTitle(),ReportConstant.LABEL_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_CUI),getStyleTitle(),ReportConstant.LABEL_CUI);
        addContentAndStyle(rowHeader.createCell(INDEX_DESCRIPTION_CUI),getStyleTitle(),ReportConstant.LABEL_CUI_DESCRIPTION);
        addContentAndStyle(rowHeader.createCell(INDEX_SECURITY_CLASS),getStyleTitle(),ReportConstant.LABEL_SECURITY_CLASS);
        addContentAndStyle(rowHeader.createCell(INDEX_CURRENCY),getStyleTitle(),ReportConstant.LABEL_CURRENCY_MNEMONIC);
        addContentAndStyle(rowHeader.createCell(INDEX_CURRENCY_ORIGINAL),getStyleTitle(),ReportConstant.LABEL_CURRENCY_ORIGINAL);
        addContentAndStyle(rowHeader.createCell(INDEX_TOTAL_USD),getStyleTitle(),ReportConstant.LABEL_TOTAL_USD);
        addContentAndStyle(rowHeader.createCell(INDEX_CODIGO),getStyleTitle(),ReportConstant.LABEL_CODIGO_TARIFA);
        addContentAndStyle(rowHeader.createCell(INDEX_TARIFA),getStyleTitle(),ReportConstant.LABEL_TARIFA);
        addContentAndStyle(rowHeader.createCell(INDEX_COMISION_USD),getStyleTitle(),ReportConstant.LABEL_COMISSION_USD);
        addContentAndStyle(rowHeader.createCell(INDEX_EXCHANGE_RATE),getStyleTitle(),ReportConstant.LABEL_EXCHANGE_RATE);
        addContentAndStyle(rowHeader.createCell(INDEX_COMISION_BOB),getStyleTitle(),ReportConstant.LABEL_COMISSION_BOB);
        
     // esta es la nueva forma de mostrar los datos a peticion del issue 1091
        int rowInitData=indrow+2;
        
        HSSFRow rowData;
        for(Object[] rowResult:queryMain) {
        	indrow++;
        	rowData = sheet.createRow((int)indrow);
        	addContentAndStyle(rowData.createCell(INDEX_SERVICIO),getStyleAccountingAccount(), rowResult[2] != null ? rowResult[2].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_DATE),getStyleAccountingAccount(), rowResult[7] != null ? CommonsUtilities.convertDateToString((Date)(rowResult[7]),ReportConstant.DATE_FORMAT_STANDAR) : "-");
        	addContentAndStyle(rowData.createCell(INDEX_CUI),getStyleAccountingAccount(), rowResult[9] != null ? rowResult[9].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_DESCRIPTION_CUI),getStyleAccountingAccount(), rowResult[10] != null ? rowResult[10].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_SECURITY_CLASS),getStyleAccountingAccount(), rowResult[12] != null ? rowResult[12].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_CURRENCY),getStyleAccountingAccount(), rowResult[14] != null ? rowResult[14].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_CURRENCY_ORIGINAL),getStyleAccountingAccountAmountIssue1091(), rowResult[19] != null ? Double.parseDouble(rowResult[19].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_TOTAL_USD),getStyleAccountingAccountAmountIssue1091(), rowResult[20] != null ? Double.parseDouble(rowResult[20].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_CODIGO),getStyleAccountingAccount(), rowResult[0] != null ? rowResult[0].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_TARIFA),getStyleAccountingAccountPercentIssue1091(), rowResult[15] != null ? Double.parseDouble(rowResult[15].toString())/100 : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_COMISION_USD),getStyleAccountingAccountAmountIssue1091(), rowResult[17] != null ? Double.parseDouble(rowResult[17].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_EXCHANGE_RATE),getStyleAccountingAccountAmountIssue1091(), rowResult[16] != null ? Double.parseDouble(rowResult[16].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_COMISION_BOB),getStyleAccountingAccountAmountIssue1091(), rowResult[18] != null ? Double.parseDouble(rowResult[18].toString()) : 0.00);
        }
        
     // sumando las columnas de TOTAL MONEDA ORIGINAL, TOTAL EN USD, COMISION EN USD, COMISION EN BOB
        rowData = sheet.createRow(indrow+1);
        addContentAndStyle(rowData.createCell(INDEX_CURRENCY),getStyleAccountingAccountAmountBoldIssue1091(), "TOTAL");
        addFormulaAndStyle(rowData.createCell(INDEX_CURRENCY_ORIGINAL),getStyleAccountingAccountAmountBoldIssue1091(), "SUM(H"+rowInitData+":H"+(rowInitData+queryMain.size()-1)+")");
        addFormulaAndStyle(rowData.createCell(INDEX_TOTAL_USD),getStyleAccountingAccountAmountBoldIssue1091(), "SUM(I"+rowInitData+":I"+(rowInitData+queryMain.size()-1)+")");
        addFormulaAndStyle(rowData.createCell(INDEX_COMISION_USD),getStyleAccountingAccountAmountBoldIssue1091(), "SUM(L"+rowInitData+":L"+(rowInitData+queryMain.size()-1)+")");
        addFormulaAndStyle(rowData.createCell(INDEX_COMISION_BOB),getStyleAccountingAccountAmountBoldIssue1091(), "SUM(N"+rowInitData+":N"+(rowInitData+queryMain.size()-1)+")");
	
	}
	
	public void writeFileExcelCommissionsAnnotationAccount(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, int number,ReportLogger reportLogger) {
		
		int INDEX_SERVICIO=1;
		int INDEX_DATE=2;
		int INDEX_CUI=3;
		int INDEX_DESCRIPTION_CUI=4;
		int INDEX_SECURITY_CLASS=5;
		int INDEX_CURRENCY=6;
		int INDEX_CURRENCY_ORIGINAL=7;
		int INDEX_TOTAL_USD=8;
		int INDEX_CODIGO=9;
		int INDEX_TARIFA=10;
		int INDEX_COMISION_USD=11;
		int INDEX_EXCHANGE_RATE=12;
		int INDEX_COMISION_BOB=13;
	
		
		/**Created a new sheet within the workBock**/
    	HSSFSheet sheet = hSSFWorkbook.createSheet("CommissionsAnnotationAccount " + number);
    	
		/*****
         * BEGIN - REPORT HEADER 
         */
    	
    	String titleReport=reportLogger.getReport().getTitle();
    	String strStartHour = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),ReportConstant.DATE_FORMAT_TIME_SECOND);
    	
    	/**TITLE*/
        HSSFRow row0= sheet.createRow((short)0);
        sheet.addMergedRegion(new CellRangeAddress(0,0,1,8));
        addContentAndStyle(row0.createCell(1),getStyleTitleBoldCenter(),titleReport);
        
        /**DATE*/
        HSSFRow row1= sheet.createRow((short)1);
        addContentAndStyle(row1.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_FECHA);
        addContentAndStyle(row1.createCell(8),CommonsUtilities.convertDatetoString(reportLogger.getRegistryDate()));
        
        /**HOUR*/
        HSSFRow row2= sheet.createRow((short)2);
        addContentAndStyle(row2.createCell(7),getStyleTitleBold(), ReportConstant.LABEL_HORA);
        addContentAndStyle(row2.createCell(8), strStartHour);
        
        /**CLASIFICATION*/
        HSSFRow row3= sheet.createRow((short)3);
        addContentAndStyle(row3.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_CLASIFICACION);
        addContentAndStyle(row3.createCell(8),reportLogger.getReport().getReportClasification());

        
        /**Usuario**/
        HSSFRow row5= sheet.createRow((short)4);
        addContentAndStyle(row5.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_USUARIO);
        addContentAndStyle(row5.createCell(8),reportLogger.getRegistryUser());
        
        /****
         * END - REPORT HEADER 
         */
        
        int indrow=8;

        /**Created Header**/
        HSSFRow rowHeader = sheet.createRow((int)indrow);
        addContentAndStyle(rowHeader.createCell(INDEX_SERVICIO),getStyleTitle(),ReportConstant.LABEL_OPERATION_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_DATE),getStyleTitle(),ReportConstant.LABEL_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_CUI),getStyleTitle(),ReportConstant.LABEL_CUI);
        addContentAndStyle(rowHeader.createCell(INDEX_DESCRIPTION_CUI),getStyleTitle(),ReportConstant.LABEL_CUI_DESCRIPTION);
        addContentAndStyle(rowHeader.createCell(INDEX_SECURITY_CLASS),getStyleTitle(),ReportConstant.LABEL_SECURITY_CLASS);
        addContentAndStyle(rowHeader.createCell(INDEX_CURRENCY),getStyleTitle(),ReportConstant.LABEL_CURRENCY_MNEMONIC);
        addContentAndStyle(rowHeader.createCell(INDEX_CURRENCY_ORIGINAL),getStyleTitle(),ReportConstant.LABEL_CURRENCY_ORIGINAL);
        addContentAndStyle(rowHeader.createCell(INDEX_TOTAL_USD),getStyleTitle(),ReportConstant.LABEL_TOTAL_USD);
        addContentAndStyle(rowHeader.createCell(INDEX_CODIGO),getStyleTitle(),ReportConstant.LABEL_CODIGO_TARIFA);
        addContentAndStyle(rowHeader.createCell(INDEX_TARIFA),getStyleTitle(),ReportConstant.LABEL_TARIFA);
        addContentAndStyle(rowHeader.createCell(INDEX_COMISION_USD),getStyleTitle(),ReportConstant.LABEL_COMISSION_USD);
        addContentAndStyle(rowHeader.createCell(INDEX_EXCHANGE_RATE),getStyleTitle(),ReportConstant.LABEL_EXCHANGE_RATE);
        addContentAndStyle(rowHeader.createCell(INDEX_COMISION_BOB),getStyleTitle(),ReportConstant.LABEL_COMISSION_BOB);
        
     // esta es la nueva forma de mostrar los datos a peticion del issue 1091
        int rowInitData=indrow+2;
        
        HSSFRow rowData;
        for(Object[] rowResult:queryMain) {
        	indrow++;
        	rowData = sheet.createRow((int)indrow);
        	addContentAndStyle(rowData.createCell(INDEX_SERVICIO),getStyleAccountingAccount(), rowResult[0] != null ? rowResult[0].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_DATE),getStyleAccountingAccount(), rowResult[1] != null ? CommonsUtilities.convertDateToString((Date)(rowResult[1]),ReportConstant.DATE_FORMAT_STANDAR) : "-");
        	addContentAndStyle(rowData.createCell(INDEX_CUI),getStyleAccountingAccount(), rowResult[2] != null ? rowResult[2].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_DESCRIPTION_CUI),getStyleAccountingAccount(), rowResult[3] != null ? rowResult[3].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_SECURITY_CLASS),getStyleAccountingAccount(), rowResult[4] != null ? rowResult[4].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_CURRENCY),getStyleAccountingAccount(), rowResult[5] != null ? rowResult[5].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_CURRENCY_ORIGINAL),getStyleAccountingAccountAmountIssue1091(), rowResult[6] != null ? Double.parseDouble(rowResult[6].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_TOTAL_USD),getStyleAccountingAccountAmountIssue1091(), rowResult[7] != null ? Double.parseDouble(rowResult[7].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_CODIGO),getStyleAccountingAccount(), rowResult[8] != null ? rowResult[8].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_TARIFA),getStyleAccountingAccountPercentIssue1091(), rowResult[9] != null ? Double.parseDouble(rowResult[9].toString())/100 : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_COMISION_USD),getStyleAccountingAccountAmountIssue1091(), rowResult[10] != null ? Double.parseDouble(rowResult[10].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_EXCHANGE_RATE),getStyleAccountingAccountAmountIssue1091(), rowResult[11] != null ? Double.parseDouble(rowResult[11].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_COMISION_BOB),getStyleAccountingAccountAmountIssue1091(), rowResult[12] != null ? Double.parseDouble(rowResult[12].toString()) : 0.00);
        }
        
     // sumando las columnas de TOTAL MONEDA ORIGINAL, TOTAL EN USD, COMISION EN USD, COMISION EN BOB
        rowData = sheet.createRow(indrow+1);
        addContentAndStyle(rowData.createCell(INDEX_CURRENCY),getStyleAccountingAccountAmountBoldIssue1091(), "TOTAL");
        addFormulaAndStyle(rowData.createCell(INDEX_CURRENCY_ORIGINAL),getStyleAccountingAccountAmountBoldIssue1091(), "SUM(H"+rowInitData+":H"+(rowInitData+queryMain.size()-1)+")");
        addFormulaAndStyle(rowData.createCell(INDEX_TOTAL_USD),getStyleAccountingAccountAmountBoldIssue1091(), "SUM(I"+rowInitData+":I"+(rowInitData+queryMain.size()-1)+")");
        addFormulaAndStyle(rowData.createCell(INDEX_COMISION_USD),getStyleAccountingAccountAmountBoldIssue1091(), "SUM(L"+rowInitData+":L"+(rowInitData+queryMain.size()-1)+")");
        addFormulaAndStyle(rowData.createCell(INDEX_COMISION_BOB),getStyleAccountingAccountAmountBoldIssue1091(), "SUM(N"+rowInitData+":N"+(rowInitData+queryMain.size()-1)+")");
	
	}
	
	
	public void subTotalCui(HSSFSheet sheet,int indrow,int columnTotal,BigDecimal totalComissionUSD,int columnUSD,BigDecimal totalComissionBOB,int columnBOB){
		indrow++;
		HSSFRow rowData = sheet.createRow((int)indrow);
		addContentAndStyle(rowData.createCell(columnTotal),getStyleTitleBold(), "TOTAL");
		String strTotalComissionUSD = CommonsUtilities.roundAmount(totalComissionUSD).toString();
		addContentAndStyle(rowData.createCell(columnUSD),getStyleTotalAccountAmount(), strTotalComissionUSD);
		String strTotalComissionBOB = CommonsUtilities.roundAmount(totalComissionBOB).toString();
		addContentAndStyle(rowData.createCell(columnBOB),getStyleTotalAccountAmount(), strTotalComissionBOB);
	}
	
	public byte[] writeFileExcelCommissionsSummary(Map<String, Object> parameters , ReportLogger reportLogger){
		
		byte[] arrayByte=null;
		
		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("lstObjects");
		
		initCellStyle();
		initFont();
		initDataFormat();
		
		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						writeFileExcelCommissionsSummary(hSSFWorkbook,queryMain.subList(i, ifinal),ifinal,reportLogger);
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{
	        		//Solo un HSSFSheet
	        		writeFileExcelCommissionsSummary(hSSFWorkbook,queryMain,queryMain.size(),reportLogger);
	        	}
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_CLIENT_PORTFOLIO);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	         
	       	hSSFWorkbook.write(bos);
	        
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        
	        hSSFWorkbook.write(fileOut);
	        
	        fileOut.close();
			
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
		return arrayByte;
	}
	
	public void writeFileExcelCommissionsSummary(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, int number,ReportLogger reportLogger){
		
		int INDEX_PARTICIPANTE=1;
		int INDEX_TITULAR=2;
		int INDEX_TARIFARIO=3;
		int INDEX_CODE_SERVICIO=4;
		int INDEX_SERVICE_NAME=5;
		int INDEX_CURRENCY=6;
		int INDEX_COMISION_UFV=7;
		int INDEX_COMISION_USD=8;
		int INDEX_COMISION_BOB=9;
	
		
		/**Created a new sheet within the workBock**/
    	HSSFSheet sheet = hSSFWorkbook.createSheet("CommissionsAnnotationAccount " + number);
    	
		/*****
         * BEGIN - REPORT HEADER 
         */
    	
    	String titleReport=reportLogger.getReport().getTitle();
    	String strStartHour = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),ReportConstant.DATE_FORMAT_TIME_SECOND);
    	
    	/**TITLE*/
        HSSFRow row0= sheet.createRow((short)0);
        sheet.addMergedRegion(new CellRangeAddress(0,0,1,8));
        addContentAndStyle(row0.createCell(1),getStyleTitleBoldCenter(),titleReport);
        
        /**DATE*/
        HSSFRow row1= sheet.createRow((short)1);
        addContentAndStyle(row1.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_FECHA);
        addContentAndStyle(row1.createCell(8),CommonsUtilities.convertDatetoString(reportLogger.getRegistryDate()));
        
        /**HOUR*/
        HSSFRow row2= sheet.createRow((short)2);
        addContentAndStyle(row2.createCell(7),getStyleTitleBold(), ReportConstant.LABEL_HORA);
        addContentAndStyle(row2.createCell(8), strStartHour);
        
        /**CLASIFICATION*/
        HSSFRow row3= sheet.createRow((short)3);
        addContentAndStyle(row3.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_CLASIFICACION);
        addContentAndStyle(row3.createCell(8),reportLogger.getReport().getReportClasification());

        
        /**Usuario**/
        HSSFRow row5= sheet.createRow((short)4);
        addContentAndStyle(row5.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_USUARIO);
        addContentAndStyle(row5.createCell(8),reportLogger.getRegistryUser());
        
        /****
         * END - REPORT HEADER 
         */
        
        int indrow=8;

        /**Created Header**/
        HSSFRow rowHeader = sheet.createRow((int)indrow);
        addContentAndStyle(rowHeader.createCell(INDEX_PARTICIPANTE),getStyleTitle(),ReportConstant.LABEL_PARTICIPANT);
        addContentAndStyle(rowHeader.createCell(INDEX_TITULAR),getStyleTitle(),ReportConstant.LABEL_HOLDER);
        addContentAndStyle(rowHeader.createCell(INDEX_TARIFARIO),getStyleTitle(),ReportConstant.LABEL_TARIFARIO);
        addContentAndStyle(rowHeader.createCell(INDEX_CODE_SERVICIO),getStyleTitle(),ReportConstant.LABEL_CODE_SERVICE);
        addContentAndStyle(rowHeader.createCell(INDEX_SERVICE_NAME),getStyleTitle(),ReportConstant.LABEL_SERVICE);
        addContentAndStyle(rowHeader.createCell(INDEX_CURRENCY),getStyleTitle(),ReportConstant.LABEL_CURRENCY_MNEMONIC);
        addContentAndStyle(rowHeader.createCell(INDEX_COMISION_UFV),getStyleTitle(),ReportConstant.LABEL_COMISSION_UFV);
        addContentAndStyle(rowHeader.createCell(INDEX_COMISION_USD),getStyleTitle(),ReportConstant.LABEL_COMISSION_USD);
        addContentAndStyle(rowHeader.createCell(INDEX_COMISION_BOB),getStyleTitle(),ReportConstant.LABEL_COMISSION_BOB);

        BigDecimal totalComissionBOB = GeneralConstants.ZERO;
        BigDecimal totalComissionUSD = GeneralConstants.ZERO;
        String lastCui = GeneralConstants.EMPTY_STRING;
        
        for (Object[] query : queryMain) {
        	
        	String participante=query[2].toString() + " - " + query[3].toString();
        	String titular=query[5].toString();
        	String tarifario=query[6].toString();
        	String codigoServicio=query[7].toString();
        	String servicio=query[8].toString();
        	String currency=query[10].toString();
        	BigDecimal comissionUFV = query[11]!=null ? new BigDecimal(query[11].toString()) : BigDecimal.ZERO;
        	String strComissionUFV = CommonsUtilities.formatDecimal(comissionUFV,ReportConstant.FORMAT_ONLY_TWO_DECIMAL);
        	BigDecimal comissionUSD = query[12]!=null ? new BigDecimal(query[12].toString()) : BigDecimal.ZERO;
        	String strComissionUSD = CommonsUtilities.formatDecimal(comissionUSD,ReportConstant.FORMAT_ONLY_TWO_DECIMAL);
        	BigDecimal comissionBOB = query[13]!=null ? new BigDecimal(query[13].toString()) : BigDecimal.ZERO;
        	String strComissionBOB = CommonsUtilities.formatDecimal(comissionBOB,ReportConstant.FORMAT_ONLY_TWO_DECIMAL);
        	
        	String cui=query[4]!=null ? query[4].toString() : GeneralConstants.EMPTY_STRING;
        	if(Validations.validateIsNotNullAndNotEmpty(lastCui) && !cui.equals(lastCui)){
        		subTotalCui(sheet,indrow,INDEX_COMISION_UFV,totalComissionUSD,INDEX_COMISION_USD,totalComissionBOB,INDEX_COMISION_BOB);
        		totalComissionBOB = GeneralConstants.ZERO;
                totalComissionUSD = GeneralConstants.ZERO;
                indrow++;
        	}
        	totalComissionBOB = totalComissionBOB.add(comissionBOB);
        	totalComissionUSD = totalComissionUSD.add(comissionUSD);
        	lastCui = cui;

	        indrow++;
	        
	        /**
	         * La data 
	         */
	        HSSFRow rowData = sheet.createRow((int)indrow);
	        addContentAndStyle(rowData.createCell(INDEX_PARTICIPANTE),getStyleAccountingAccount(), participante);
	        addContentAndStyle(rowData.createCell(INDEX_TITULAR),getStyleAccountingAccount(), titular);
	        addContentAndStyle(rowData.createCell(INDEX_TARIFARIO),getStyleAccountingAccount(), tarifario);
	        addContentAndStyle(rowData.createCell(INDEX_CODE_SERVICIO),getStyleAccountingAccount(), codigoServicio);
	        addContentAndStyle(rowData.createCell(INDEX_SERVICE_NAME),getStyleAccountingAccount(), servicio);
	        addContentAndStyle(rowData.createCell(INDEX_CURRENCY),getStyleAccountingAccount(), currency);
	        addContentAndStyle(rowData.createCell(INDEX_COMISION_UFV),getStyleAmountDebit(), strComissionUFV);
	        addContentAndStyle(rowData.createCell(INDEX_COMISION_USD),getStyleAmountDebit(), strComissionUSD);
	        addContentAndStyle(rowData.createCell(INDEX_COMISION_BOB),getStyleAmountDebit(), strComissionBOB);
	        
		}
        subTotalCui(sheet,indrow,INDEX_COMISION_UFV,totalComissionUSD,INDEX_COMISION_USD,totalComissionBOB,INDEX_COMISION_BOB);
        adjustColumns(sheet,INDEX_COMISION_BOB);
	}
	
	public void writeFileExcelCommissionsIssuanceDPF(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, int number,ReportLogger reportLogger){
		
		int INDEX_TARIFARIO=1;
		int INDEX_SERVICE_NAME=2;
		int INDEX_EMISOR=3;
		int INDEX_FECHA=4;
		int INDEX_CODIGO_TARIFA=5;
		int INDEX_CLASE_VALOR=6;
		int INDEX_VALOR=7;
		int INDEX_MONTO_MONEDA_ORIGINAL=8;
		int INDEX_MONTO_USD=9;
	
		
		/**Created a new sheet within the workBock**/
    	HSSFSheet sheet = hSSFWorkbook.createSheet("CommissionsAnnotationAccount " + number);
    	
		/*****
         * BEGIN - REPORT HEADER 
         */
    	
    	String titleReport=reportLogger.getReport().getTitle();
    	String strStartHour = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),ReportConstant.DATE_FORMAT_TIME_SECOND);
    	
    	/**TITLE*/
        HSSFRow row0= sheet.createRow((short)0);
        sheet.addMergedRegion(new CellRangeAddress(0,0,1,8));
        addContentAndStyle(row0.createCell(1),getStyleTitleBoldCenter(),titleReport);
        
        /**DATE*/
        HSSFRow row1= sheet.createRow((short)1);
        addContentAndStyle(row1.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_FECHA);
        addContentAndStyle(row1.createCell(8),CommonsUtilities.convertDatetoString(reportLogger.getRegistryDate()));
        
        /**HOUR*/
        HSSFRow row2= sheet.createRow((short)2);
        addContentAndStyle(row2.createCell(7),getStyleTitleBold(), ReportConstant.LABEL_HORA);
        addContentAndStyle(row2.createCell(8), strStartHour);
        
        /**CLASIFICATION*/
        HSSFRow row3= sheet.createRow((short)3);
        addContentAndStyle(row3.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_CLASIFICACION);
        addContentAndStyle(row3.createCell(8),reportLogger.getReport().getReportClasification());

        
        /**Usuario**/
        HSSFRow row5= sheet.createRow((short)4);
        addContentAndStyle(row5.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_USUARIO);
        addContentAndStyle(row5.createCell(8),reportLogger.getRegistryUser());
        
        /****
         * END - REPORT HEADER 
         */
        
        int indrow=8;

        /**Created Header**/
        
        HSSFRow rowHeader = sheet.createRow((int)indrow);
        addContentAndStyle(rowHeader.createCell(INDEX_TARIFARIO),getStyleTitle(),ReportConstant.LABEL_TARIFARIO);
        addContentAndStyle(rowHeader.createCell(INDEX_SERVICE_NAME),getStyleTitle(),ReportConstant.LABEL_SERVICE);
        addContentAndStyle(rowHeader.createCell(INDEX_EMISOR),getStyleTitle(),ReportConstant.LABEL_ISSUER);
        addContentAndStyle(rowHeader.createCell(INDEX_FECHA),getStyleTitle(),ReportConstant.LABEL_ISSUANCE_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_CODIGO_TARIFA),getStyleTitle(),ReportConstant.LABEL_CODIGO_TARIFA);
        addContentAndStyle(rowHeader.createCell(INDEX_CLASE_VALOR),getStyleTitle(),ReportConstant.LABEL_SECURITY_CLASS);
        addContentAndStyle(rowHeader.createCell(INDEX_VALOR),getStyleTitle(),ReportConstant.LABEL_SECURITY_CODE);
        addContentAndStyle(rowHeader.createCell(INDEX_MONTO_MONEDA_ORIGINAL),getStyleTitle(),ReportConstant.LABEL_MONTO_MONEDA_ORIGINAL);
        addContentAndStyle(rowHeader.createCell(INDEX_MONTO_USD),getStyleTitle(),ReportConstant.LABEL_MONTO_MONEDA_USD);

        BigDecimal totalMontoMonedaOriginalBOB = GeneralConstants.ZERO;
        BigDecimal totalMontoMonedaUSD = GeneralConstants.ZERO;
        String lastIssuer = GeneralConstants.EMPTY_STRING;
        for (Object[] query : queryMain) {
        	
        	String tarifario=query[1].toString() + " - (" + query[3].toString() + ") " + query[2].toString();
        	String servicio="(" + query[3].toString() + ") " + query[2].toString();
        	String emisor=query[7].toString();
        	String fecha=query[13].toString();
        	String codigoTarifa=query[1].toString();
        	String claseValor=query[9].toString();
        	String claveValor=query[11].toString();
        	BigDecimal montoMonedaOriginal = query[15]!=null ? new BigDecimal(query[15].toString()) : BigDecimal.ZERO;
        	String strmontoMonedaOriginal = CommonsUtilities.formatDecimal(montoMonedaOriginal,ReportConstant.FORMAT_ONLY_TWO_DECIMAL);
        	BigDecimal montoMonedaUSD = query[16]!=null ? new BigDecimal(query[16].toString()) : BigDecimal.ZERO;
        	String strmontoMonedaUSD = CommonsUtilities.formatDecimal(montoMonedaUSD,ReportConstant.FORMAT_ONLY_TWO_DECIMAL);
        	
        	String issuer=query[6].toString();
        	if(Validations.validateIsNotNullAndNotEmpty(lastIssuer) && !issuer.equals(lastIssuer)){
        		subTotalCui(sheet,indrow,INDEX_CODIGO_TARIFA,totalMontoMonedaOriginalBOB,INDEX_MONTO_MONEDA_ORIGINAL,totalMontoMonedaUSD,INDEX_MONTO_USD);
        		totalMontoMonedaOriginalBOB = GeneralConstants.ZERO;
                totalMontoMonedaUSD = GeneralConstants.ZERO;
                indrow++;
        	}
        	totalMontoMonedaOriginalBOB = totalMontoMonedaOriginalBOB.add(montoMonedaOriginal);
        	totalMontoMonedaUSD = totalMontoMonedaUSD.add(montoMonedaUSD);
        	lastIssuer = issuer;

	        indrow++;
	        
	        /**
	         * La data 
	         */
	        HSSFRow rowData = sheet.createRow((int)indrow);
	        addContentAndStyle(rowData.createCell(INDEX_TARIFARIO),getStyleAccountingAccount(), tarifario);
	        addContentAndStyle(rowData.createCell(INDEX_SERVICE_NAME),getStyleAccountingAccount(), servicio);
	        addContentAndStyle(rowData.createCell(INDEX_EMISOR),getStyleAccountingAccount(), emisor);
	        addContentAndStyle(rowData.createCell(INDEX_FECHA),getStyleAccountingAccount(), fecha);
	        addContentAndStyle(rowData.createCell(INDEX_CODIGO_TARIFA),getStyleAccountingAccount(), codigoTarifa);
	        addContentAndStyle(rowData.createCell(INDEX_CLASE_VALOR),getStyleAccountingAccount(), claseValor);
	        addContentAndStyle(rowData.createCell(INDEX_VALOR),getStyleAmountDebit(), claveValor);
	        addContentAndStyle(rowData.createCell(INDEX_MONTO_MONEDA_ORIGINAL),getStyleAmountDebit(), strmontoMonedaOriginal);
	        addContentAndStyle(rowData.createCell(INDEX_MONTO_USD),getStyleAmountDebit(), strmontoMonedaUSD);
	        
		}
        subTotalCui(sheet,indrow,INDEX_CODIGO_TARIFA,totalMontoMonedaOriginalBOB,INDEX_MONTO_MONEDA_ORIGINAL,totalMontoMonedaUSD,INDEX_MONTO_USD);
        adjustColumns(sheet,INDEX_MONTO_USD);
	}
	
	public byte[] writeFileExcelCommissionsIssuanceDPF(Map<String, Object> parameters , ReportLogger reportLogger  ){
		
		byte[] arrayByte=null;

		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("lstObjects");

		initCellStyle();
		initFont();
		initDataFormat();
		
		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						writeFileExcelCommissionsIssuanceDPF(hSSFWorkbook,queryMain.subList(i, ifinal),queryMain.subList(i, ifinal).size(),reportLogger);
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{
	        		//Solo un HSSFSheet
	        		writeFileExcelCommissionsIssuanceDPF(hSSFWorkbook,queryMain,queryMain.size(),reportLogger);
	        	}
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_COMMISSIONS_ISSUANCE_DPF);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	         
	       	hSSFWorkbook.write(bos);
	        
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        
	        hSSFWorkbook.write(fileOut);
	        
	        fileOut.close();
			
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
		return arrayByte;
	}
	
	public void writeFileExcelCommissionsPhysicalCustody(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, int number,ReportLogger reportLogger){
		
		int INDEX_TARIFARIO=1;
		int INDEX_SERVICE_NAME=2;
		int INDEX_PARTICIPANTE=3;
		int INDEX_CLIENTE=4;
		int INDEX_FECHA=5;
		int INDEX_CUI = 6;
		int INDEX_CUI_DESCRIPTION = 7;
		int INDEX_CLASE_VALOR=8;
		int INDEX_CURRENCY=9;
		int INDEX_TOTAL_MONEDA_ORIGINAL=10;
		int INDEX_TOTAL_USD=11;
		int INDEX_CODIGO_TARIFA=12;
		int INDEX_TARIFA=13;
		int INDEX_COMISION_USD=13;
		int INDEX_TIPO_CAMBIO=14;
		int INDEX_COMISION_BOB=15;
	
		
		/**Created a new sheet within the workBock**/
    	HSSFSheet sheet = hSSFWorkbook.createSheet("CommissionsAnnotationAccount " + number);
    	
		/*****
         * BEGIN - REPORT HEADER 
         */
    	
    	String titleReport=reportLogger.getReport().getTitle();
    	String strStartHour = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),ReportConstant.DATE_FORMAT_TIME_SECOND);
    	
    	/**TITLE*/
        HSSFRow row0= sheet.createRow((short)0);
        sheet.addMergedRegion(new CellRangeAddress(0,0,1,8));
        addContentAndStyle(row0.createCell(1),getStyleTitleBoldCenter(),titleReport);
        
        /**DATE*/
        HSSFRow row1= sheet.createRow((short)1);
        addContentAndStyle(row1.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_FECHA);
        addContentAndStyle(row1.createCell(8),CommonsUtilities.convertDatetoString(reportLogger.getRegistryDate()));
        
        /**HOUR*/
        HSSFRow row2= sheet.createRow((short)2);
        addContentAndStyle(row2.createCell(7),getStyleTitleBold(), ReportConstant.LABEL_HORA);
        addContentAndStyle(row2.createCell(8), strStartHour);
        
        /**CLASIFICATION*/
        HSSFRow row3= sheet.createRow((short)3);
        addContentAndStyle(row3.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_CLASIFICACION);
        addContentAndStyle(row3.createCell(8),reportLogger.getReport().getReportClasification());

        
        /**Usuario**/
        HSSFRow row5= sheet.createRow((short)4);
        addContentAndStyle(row5.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_USUARIO);
        addContentAndStyle(row5.createCell(8),reportLogger.getRegistryUser());
        
        /****
         * END - REPORT HEADER 
         */
        
        int indrow=8;

        /**Created Header**/
        
        HSSFRow rowHeader = sheet.createRow((int)indrow);
        addContentAndStyle(rowHeader.createCell(INDEX_TARIFARIO),getStyleTitle(),ReportConstant.LABEL_TARIFARIO);
        addContentAndStyle(rowHeader.createCell(INDEX_SERVICE_NAME),getStyleTitle(),ReportConstant.LABEL_SERVICE);
        addContentAndStyle(rowHeader.createCell(INDEX_PARTICIPANTE),getStyleTitle(),ReportConstant.LABEL_PARTICIPANT);
        addContentAndStyle(rowHeader.createCell(INDEX_CLIENTE),getStyleTitle(),ReportConstant.TAG_CLIENT);
        addContentAndStyle(rowHeader.createCell(INDEX_FECHA),getStyleTitle(),ReportConstant.LABEL_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_CUI),getStyleTitle(),ReportConstant.LABEL_CUI);
        addContentAndStyle(rowHeader.createCell(INDEX_CUI_DESCRIPTION),getStyleTitle(),ReportConstant.LABEL_CUI_DESCRIPTION);
        addContentAndStyle(rowHeader.createCell(INDEX_CLASE_VALOR),getStyleTitle(),ReportConstant.LABEL_SECURITY_CLASS);
        addContentAndStyle(rowHeader.createCell(INDEX_CURRENCY),getStyleTitle(),ReportConstant.TAG_MONEDA);
        addContentAndStyle(rowHeader.createCell(INDEX_TOTAL_MONEDA_ORIGINAL),getStyleTitle(),ReportConstant.LABEL_CURRENCY_ORIGINAL);
        addContentAndStyle(rowHeader.createCell(INDEX_TOTAL_USD),getStyleTitle(),ReportConstant.LABEL_TOTAL_USD);
        addContentAndStyle(rowHeader.createCell(INDEX_CODIGO_TARIFA),getStyleTitle(),ReportConstant.LABEL_CODIGO_TARIFA);
        addContentAndStyle(rowHeader.createCell(INDEX_TARIFA),getStyleTitle(),ReportConstant.LABEL_TARIFA);
        addContentAndStyle(rowHeader.createCell(INDEX_COMISION_USD),getStyleTitle(),ReportConstant.LABEL_COMISSION_USD);
        addContentAndStyle(rowHeader.createCell(INDEX_TIPO_CAMBIO),getStyleTitle(),ReportConstant.LABEL_EXCHANGE_RATE);
        addContentAndStyle(rowHeader.createCell(INDEX_COMISION_BOB),getStyleTitle(),ReportConstant.LABEL_COMISSION_BOB);

        BigDecimal totalComissionBOB = GeneralConstants.ZERO;
        BigDecimal totalComissionUSD = GeneralConstants.ZERO;
        String lastCui = GeneralConstants.EMPTY_STRING;
        for (Object[] query : queryMain) {
        	
        	String tarifario=query[0].toString();
        	String servicio="("+query[1].toString()+")"+query[2].toString();
        	String participante="("+query[4].toString()+")"+query[6].toString();
        	String cliente="("+query[9].toString()+")"+query[10].toString();
        	String fecha=query[8].toString();
        	String descripcionCui=query[10].toString();
        	String claseValor=query[12].toString();
        	String moneda=query[14].toString();
        	BigDecimal totalMonedaOriginal = query[19]!=null ? new BigDecimal(query[19].toString()) : BigDecimal.ZERO;
        	String strTotalMonedaOriginal = CommonsUtilities.formatDecimal(totalMonedaOriginal,ReportConstant.FORMAT_ONLY_TWO_DECIMAL);
        	BigDecimal totalMonedaUSD = query[20]!=null ? new BigDecimal(query[20].toString()) : BigDecimal.ZERO;
        	String strTotalMonedaUSD = CommonsUtilities.formatDecimal(totalMonedaUSD,ReportConstant.FORMAT_ONLY_TWO_DECIMAL);
        	String codigoTarifa=query[0].toString();
        	BigDecimal tarifa = query[15]!=null ? new BigDecimal(query[15].toString()) : BigDecimal.ZERO;
        	String strTarifa = CommonsUtilities.formatDecimal(tarifa,ReportConstant.FORMAT_ONLY_FOUR_DECIMALS) + GeneralConstants.BLANK_SPACE + GeneralConstants.PERCENTAGE_STRING;
        	BigDecimal comisionUSD = query[17]!=null ? new BigDecimal(query[17].toString()) : BigDecimal.ZERO;
        	String strComisionUSD = CommonsUtilities.formatDecimal(comisionUSD,ReportConstant.FORMAT_ONLY_TWO_DECIMAL);
        	BigDecimal tipoCambio = query[16]!=null ? new BigDecimal(query[16].toString()) : BigDecimal.ZERO;
        	String strTipoCambio = CommonsUtilities.formatDecimal(tipoCambio,ReportConstant.FORMAT_ONLY_TWO_DECIMAL);
        	BigDecimal comisionBOB = query[18]!=null ? new BigDecimal(query[18].toString()) : BigDecimal.ZERO;
        	String strComisionBOB = CommonsUtilities.formatDecimal(comisionBOB,ReportConstant.FORMAT_ONLY_TWO_DECIMAL);
        	String cui=query[9].toString();

        	if(Validations.validateIsNotNullAndNotEmpty(lastCui) && !cui.equals(lastCui)){
        		subTotalCui(sheet,indrow,INDEX_CODIGO_TARIFA,totalComissionUSD,INDEX_COMISION_USD,totalComissionBOB,INDEX_COMISION_BOB);
        		totalComissionBOB = GeneralConstants.ZERO;
                totalComissionUSD = GeneralConstants.ZERO;
                indrow++;
        	}
        	totalComissionBOB = totalComissionBOB.add(comisionBOB);
        	totalComissionUSD = totalComissionUSD.add(comisionUSD);
        	lastCui = cui;
        	
	        indrow++;
	        
	        /**
	         * La data 
	         */
	        HSSFRow rowData = sheet.createRow((int)indrow);
	        addContentAndStyle(rowData.createCell(INDEX_TARIFARIO),getStyleAccountingAccount(), tarifario);
	        addContentAndStyle(rowData.createCell(INDEX_SERVICE_NAME),getStyleAccountingAccount(), servicio);
	        addContentAndStyle(rowData.createCell(INDEX_PARTICIPANTE),getStyleAccountingAccount(), participante);
	        addContentAndStyle(rowData.createCell(INDEX_CLIENTE),getStyleAccountingAccount(), cliente);
	        addContentAndStyle(rowData.createCell(INDEX_FECHA),getStyleAccountingAccount(), fecha);
	        addContentAndStyle(rowData.createCell(INDEX_CUI),getStyleAccountingAccount(), cui);
	        addContentAndStyle(rowData.createCell(INDEX_CUI_DESCRIPTION),getStyleAccountingAccount(), descripcionCui);
	        addContentAndStyle(rowData.createCell(INDEX_CLASE_VALOR),getStyleAccountingAccount(), claseValor);
	        addContentAndStyle(rowData.createCell(INDEX_CURRENCY),getStyleAccountingAccount(), moneda);
	        addContentAndStyle(rowData.createCell(INDEX_TOTAL_MONEDA_ORIGINAL),getStyleAccountingAccount(),strTotalMonedaOriginal);
	        addContentAndStyle(rowData.createCell(INDEX_TOTAL_USD),getStyleAmountDebit(), strTotalMonedaUSD);
	        addContentAndStyle(rowData.createCell(INDEX_CODIGO_TARIFA),getStyleAmountDebit(), codigoTarifa);
	        addContentAndStyle(rowData.createCell(INDEX_TARIFA),getStyleAmountDebit(), strTarifa);
	        addContentAndStyle(rowData.createCell(INDEX_COMISION_USD),getStyleAmountDebit(), strComisionUSD);
	        addContentAndStyle(rowData.createCell(INDEX_TIPO_CAMBIO),getStyleAmountDebit(), strTipoCambio);
	        addContentAndStyle(rowData.createCell(INDEX_COMISION_BOB),getStyleAmountDebit(), strComisionBOB);
	        
		}
        subTotalCui(sheet,indrow,INDEX_TARIFA,totalComissionUSD,INDEX_COMISION_USD,totalComissionBOB,INDEX_COMISION_BOB);
        adjustColumns(sheet,INDEX_COMISION_BOB);
	}
	
	public byte[] writeFileExcelCommissionsPhysicalCustody(Map<String, Object> parameters , ReportLogger reportLogger  ){
		
		byte[] arrayByte=null;

		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("lstObjects");

		initCellStyle();
		initFont();
		initDataFormat();
		
		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						writeFileExcelCommissionsPhysicalCustody(hSSFWorkbook,queryMain.subList(i, ifinal),queryMain.subList(i, ifinal).size(),reportLogger);
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{
	        		//Solo un HSSFSheet
	        		writeFileExcelCommissionsPhysicalCustody(hSSFWorkbook,queryMain,queryMain.size(),reportLogger);
	        	}
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_COMMISSIONS_PHYSICAL_CUSTODY);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	         
	       	hSSFWorkbook.write(bos);
	        
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        
	        hSSFWorkbook.write(fileOut);
	        
	        fileOut.close();
			
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
		return arrayByte;
	}
	
	public void writeFileExcelCommissionsSummaryPortfolio(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, int number,ReportLogger reportLogger){
		
		int INDEX_PARTICIPANTE=1;
		int INDEX_CUI=2;
		int INDEX_CUI_DESCRIPTION=3;
		int INDEX_SERVICIO=4;
		int INDEX_REFERENCIA_USD=5;
		int INDEX_COMISION_USD=6;
		int INDEX_COMISION_BOB=7;
	
		
		/**Created a new sheet within the workBock**/
    	HSSFSheet sheet = hSSFWorkbook.createSheet("CommissionsSummaryPortfolio " + number);
    	
		/*****
         * BEGIN - REPORT HEADER 
         */
    	
    	String titleReport=reportLogger.getReport().getTitle();
    	String strStartHour = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),ReportConstant.DATE_FORMAT_TIME_SECOND);
    	
    	/**TITLE*/
        HSSFRow row0= sheet.createRow((short)0);
        sheet.addMergedRegion(new CellRangeAddress(0,0,1,6));
        addContentAndStyle(row0.createCell(1),getStyleTitleBoldCenter(),titleReport);
        
        /**DATE*/
        HSSFRow row1= sheet.createRow((short)1);
        addContentAndStyle(row1.createCell(6),getStyleTitleBold(),ReportConstant.LABEL_FECHA);
        addContentAndStyle(row1.createCell(7),CommonsUtilities.convertDatetoString(reportLogger.getRegistryDate()));
        
        /**HOUR*/
        HSSFRow row2= sheet.createRow((short)2);
        addContentAndStyle(row2.createCell(6),getStyleTitleBold(), ReportConstant.LABEL_HORA);
        addContentAndStyle(row2.createCell(7), strStartHour);
        
        /**CLASIFICATION*/
        HSSFRow row3= sheet.createRow((short)3);
        addContentAndStyle(row3.createCell(6),getStyleTitleBold(),ReportConstant.LABEL_CLASIFICACION);
        addContentAndStyle(row3.createCell(7),reportLogger.getReport().getReportClasification());

        
        /**Usuario**/
        HSSFRow row5= sheet.createRow((short)4);
        addContentAndStyle(row5.createCell(6),getStyleTitleBold(),ReportConstant.LABEL_USUARIO);
        addContentAndStyle(row5.createCell(7),reportLogger.getRegistryUser());
        
        /****
         * END - REPORT HEADER 
         */
        
        int indrow=8;

        /**Created Header**/
        
        HSSFRow rowHeader = sheet.createRow((int)indrow);
        addContentAndStyle(rowHeader.createCell(INDEX_PARTICIPANTE),getStyleTitle(),ReportConstant.LABEL_PARTICIPANT);
        addContentAndStyle(rowHeader.createCell(INDEX_CUI),getStyleTitle(),ReportConstant.LABEL_CUI);
        addContentAndStyle(rowHeader.createCell(INDEX_CUI_DESCRIPTION),getStyleTitle(),ReportConstant.LABEL_CUI_DESCRIPTION);
        addContentAndStyle(rowHeader.createCell(INDEX_SERVICIO),getStyleTitle(),ReportConstant.LABEL_SERVICE);
        addContentAndStyle(rowHeader.createCell(INDEX_REFERENCIA_USD),getStyleTitle(),ReportConstant.LABEL_VALOR_REFERENCIA_USD);
        addContentAndStyle(rowHeader.createCell(INDEX_COMISION_USD),getStyleTitle(),ReportConstant.LABEL_COMISSION_USD);
        addContentAndStyle(rowHeader.createCell(INDEX_COMISION_BOB),getStyleTitle(),ReportConstant.LABEL_COMISSION_BOB);

        
        for (Object[] query : queryMain) {
        	
        	String participante="("+query[1].toString()+")"+query[3].toString();
        	String cui=query[4]!=null ? query[4].toString() : GeneralConstants.EMPTY_STRING;
        	String descripcionCui=query[5].toString();
        	String servicio="("+query[6].toString()+")"+query[7].toString();
        	BigDecimal valorReferenciaUSD = query[8]!=null ? new BigDecimal(query[8].toString()) : BigDecimal.ZERO;
        	String strValorReferenciaUSD = CommonsUtilities.formatDecimal(valorReferenciaUSD,ReportConstant.FORMAT_ONLY_TWO_DECIMAL);
        	BigDecimal comisionUSD = query[9]!=null ? new BigDecimal(query[9].toString()) : BigDecimal.ZERO;
        	String strComisionUSD = CommonsUtilities.formatDecimal(comisionUSD,ReportConstant.FORMAT_ONLY_TWO_DECIMAL);
        	BigDecimal comisionBOB = query[10]!=null ? new BigDecimal(query[10].toString()) : BigDecimal.ZERO;
        	String strComisionBOB = CommonsUtilities.formatDecimal(comisionBOB,ReportConstant.FORMAT_ONLY_TWO_DECIMAL);

	        indrow++;
	        
	        /**
	         * La data 
	         */
	        HSSFRow rowData = sheet.createRow((int)indrow);
	        addContentAndStyle(rowData.createCell(INDEX_PARTICIPANTE),getStyleAccountingAccount(), participante);
	        addContentAndStyle(rowData.createCell(INDEX_CUI),getStyleAccountingAccount(), cui);
	        addContentAndStyle(rowData.createCell(INDEX_CUI_DESCRIPTION),getStyleAccountingAccount(), descripcionCui);
	        addContentAndStyle(rowData.createCell(INDEX_SERVICIO),getStyleAccountingAccount(), servicio);
	        addContentAndStyle(rowData.createCell(INDEX_REFERENCIA_USD),getStyleAmountDebit(), strValorReferenciaUSD);
	        addContentAndStyle(rowData.createCell(INDEX_COMISION_USD),getStyleAmountDebit(), strComisionUSD);
	        addContentAndStyle(rowData.createCell(INDEX_COMISION_BOB),getStyleAmountDebit(), strComisionBOB);
	        
		}
        adjustColumns(sheet,INDEX_COMISION_BOB);
	}
	
	/**
	 * issue 924
	 * issue 1097
	 * metodo para la generacion del reporte en Apache POI
	 * REPORTE CONSOLIDADO COBROS POR ENTIDAD
	 * @param parameters
	 * @param reportLogger
	 * @return
	 */
	public byte[] writeFileExcelConsCollectionByEntityPoi(Map<String, Object> parameters , ReportLogger reportLogger  ){
		
		byte[] arrayByte=null;

		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("lstObjects");

		initCellStyle();
		initFont();
		initDataFormat();
		
		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						writeFileConsCollectionByEntityBuidPoi(hSSFWorkbook,queryMain.subList(i, ifinal),queryMain.subList(i, ifinal).size(),reportLogger);
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{
	        		//Solo un HSSFSheet
	        		writeFileConsCollectionByEntityBuidPoi(hSSFWorkbook,queryMain,queryMain.size(),reportLogger);
	        	}
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_COMMISSIONS_SUMMARY_PORTFOLIO);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	         
	       	hSSFWorkbook.write(bos);
	        
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        
	        hSSFWorkbook.write(fileOut);
	        
	        fileOut.close();
			
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
		return arrayByte;
	}
	
	
	/**
	 * metodo para la generacion del reporte en Apache POI
	 * REPORTE CONSOLIDADO COBROS POR ENTIDAD
	 * issue 1097
	 * @param hSSFWorkbook
	 * @param queryMain
	 * @param number
	 * @param reportLogger
	 */
	public void writeFileConsCollectionByEntityBuidPoi(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, int number,ReportLogger reportLogger) {
		
		int INDEX_DATE = 1;
		int INDEX_STATE = 2;
		int INDEX_RATE_CODE = 3;
		int INDEX_SERVICE_CODE = 4;
		int INDEX_PORTFOLIO_USD = 5;
		int INDEX_COMMISSION_USD = 6;
		int INDEX_RATE_EXCHANGE = 7;
		int INDEX_COMMISSION_BS = 8;
		
		//creando la hoja
		HSSFSheet sheet = hSSFWorkbook.createSheet("Consolidado Cobros por Entidad");
		
		//cabecera del reporte
		String titleReport=reportLogger.getReport().getTitle();
		String strStartHour = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),ReportConstant.DATE_FORMAT_TIME_SECOND);
		
		HSSFRow row0= sheet.createRow((short)0);
        sheet.addMergedRegion(new CellRangeAddress(0,0,1,8));
        addContentAndStyle(row0.createCell(1),getStyleTitleBoldCenter(),titleReport);
        
        /**DATE*/
        HSSFRow row1= sheet.createRow((short)1);
        addContentAndStyle(row1.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_FECHA);
        addContentAndStyle(row1.createCell(8),CommonsUtilities.convertDatetoString(reportLogger.getRegistryDate()));
        
        /**HOUR*/
        HSSFRow row2= sheet.createRow((short)2);
        addContentAndStyle(row2.createCell(7),getStyleTitleBold(), ReportConstant.LABEL_HORA);
        addContentAndStyle(row2.createCell(8), strStartHour);
        
        /**CLASIFICATION*/
        HSSFRow row3= sheet.createRow((short)3);
        addContentAndStyle(row3.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_CLASIFICACION);
        addContentAndStyle(row3.createCell(8),reportLogger.getReport().getReportClasification());

        
        /**Usuario**/
        HSSFRow row5= sheet.createRow((short)4);
        addContentAndStyle(row5.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_USUARIO);
        addContentAndStyle(row5.createCell(8),reportLogger.getRegistryUser());
        
        int indrow=8;
        
        HSSFRow rowHeader = sheet.createRow((int)indrow);
        addContentAndStyle(rowHeader.createCell(INDEX_DATE),getStyleTitle(),ReportConstant.LABEL_FECHA);
        addContentAndStyle(rowHeader.createCell(INDEX_STATE),getStyleTitle(),ReportConstant.LABEL_STATE);
        addContentAndStyle(rowHeader.createCell(INDEX_RATE_CODE),getStyleTitle(),ReportConstant.LABEL_CODIGO_TARIFA);
        addContentAndStyle(rowHeader.createCell(INDEX_SERVICE_CODE),getStyleTitle(),ReportConstant.LABEL_SERVICE);
        addContentAndStyle(rowHeader.createCell(INDEX_PORTFOLIO_USD),getStyleTitle(),ReportConstant.LABEL_CARTERA_USD);
        addContentAndStyle(rowHeader.createCell(INDEX_COMMISSION_USD),getStyleTitle(),ReportConstant.LABEL_COMISSION_USD);
        addContentAndStyle(rowHeader.createCell(INDEX_RATE_EXCHANGE),getStyleTitle(),ReportConstant.LABEL_EXCHANGE_RATE);
        addContentAndStyle(rowHeader.createCell(INDEX_COMMISSION_BS),getStyleTitle(),ReportConstant.LABEL_COMISSION_BOB);
        
        int rowInitData=indrow+2;
        HSSFRow rowData;
        for(Object[] rowResult:queryMain) {
        	indrow++;
        	rowData = sheet.createRow((int)indrow);
        	addContentAndStyle(rowData.createCell(INDEX_DATE),getStyleAccountingAccount(), rowResult[0] != null ? CommonsUtilities.convertDateToString((Date)(rowResult[4]),ReportConstant.DATE_FORMAT_STANDAR) : "-");
        	addContentAndStyle(rowData.createCell(INDEX_STATE),getStyleAccountingAccount(), rowResult[1] != null ? rowResult[7].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_RATE_CODE),getStyleAccountingAccount(), rowResult[2] != null ? rowResult[8].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_SERVICE_CODE),getStyleAccountingAccount(), rowResult[3] != null ? rowResult[9].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_PORTFOLIO_USD),getStyleAccountingAccountAmountIssue1091(), rowResult[4] != null ? Double.parseDouble(rowResult[19].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_COMMISSION_USD),getStyleAccountingAccountAmountIssue1091(), rowResult[5] != null ? Double.parseDouble(rowResult[12].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_RATE_EXCHANGE),getStyleAccountingAccountAmountIssue1091(), rowResult[6] != null ? Double.parseDouble(rowResult[14].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_COMMISSION_BS),getStyleAccountingAccountAmountIssue1091(), rowResult[7] != null ? Double.parseDouble(rowResult[13].toString()) : 0.00);
        }
        
        // SUAMNDO LA COLUMNA DE COMISION BOB
        rowData = sheet.createRow(indrow+1);
        addContentAndStyle(rowData.createCell(INDEX_RATE_EXCHANGE),getStyleAccountingAccountAmountBoldIssue1091(), "TOTAL");
        addFormulaAndStyle(rowData.createCell(INDEX_COMMISSION_BS),getStyleAccountingAccountAmountBoldIssue1091(), "SUM(I"+rowInitData+":I"+(rowInitData+queryMain.size()-1)+")");
	}
	
	/**
	 * metodo para la generacion del reporte 
	 * REPORTE LISTADO GENERAL DE EMISORES POI
	 * @param parameters
	 * @param reportLogger
	 * @return
	 */
	public byte[] writeFileExcelGeneralListIssuers(Map<String, Object> parameters , ReportLogger reportLogger  ){
		
		byte[] arrayByte=null;

		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("lstObjects");

		initCellStyle();
		initFont();
		initDataFormat();
		
		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						writeFileExcelGeneralListIssuersPoi(hSSFWorkbook,queryMain.subList(i, ifinal),queryMain.subList(i, ifinal).size(),reportLogger);
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{
	        		//Solo un HSSFSheet
	        		writeFileExcelGeneralListIssuersPoi(hSSFWorkbook,queryMain,queryMain.size(),reportLogger);
	        	}
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_COMMISSIONS_SUMMARY_PORTFOLIO);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	         
	       	hSSFWorkbook.write(bos);
	        
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        
	        hSSFWorkbook.write(fileOut);
	        
	        fileOut.close();
			
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
		return arrayByte;
	}
	
	/**
	 * metodo para la generacion del reporte en Apache POI
	 * REPORTE DE REGISTRO DE VALORES EN SISTEMA DE ANOTACION EN CUENTA VOLUNTARIA POR VENTA Y TESORO DIRECTO
	 * issue 1053
	 * @param hSSFWorkbook
	 * @param queryMain
	 * @param number
	 * @param reportLogger
	 */
	public void writeFileExcelGeneralListIssuersPoi(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, int number,ReportLogger reportLogger) {
		
		int INDEX_ISSUER_PK = 1;
		int INDEX_BUSSINESS_NAME = 2;
		int INDEX_MNEMONIC = 3;
		int INDEX_DOCUMNET_TYPE = 4;
		int INDEX_DOCUMENT_NUMBER = 5;
		int INDEX_ID_HOLDER_FK = 6;
		int INDEX_PHONE_NUMBER = 7;
		int INDEX_ECONOMIC_ACTIVITY = 8;
		int INDEX_REGISTRY_DATE = 9;
		int INDEX_STATE_ISSUER = 10;
		
		//creando la hoja
		HSSFSheet sheet = hSSFWorkbook.createSheet("Listado General de Emisores");
		
		//cabecera del reporte
		String titleReport=reportLogger.getReport().getTitle();
		String strStartHour = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),ReportConstant.DATE_FORMAT_TIME_SECOND);
		
		HSSFRow row0= sheet.createRow((short)0);
        sheet.addMergedRegion(new CellRangeAddress(0,0,1,8));
        addContentAndStyle(row0.createCell(1),getStyleTitleBoldCenter(),titleReport);
        
        /**DATE*/
        HSSFRow row1= sheet.createRow((short)1);
        addContentAndStyle(row1.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_FECHA);
        addContentAndStyle(row1.createCell(8),CommonsUtilities.convertDatetoString(reportLogger.getRegistryDate()));
        
        /**HOUR*/
        HSSFRow row2= sheet.createRow((short)2);
        addContentAndStyle(row2.createCell(7),getStyleTitleBold(), ReportConstant.LABEL_HORA);
        addContentAndStyle(row2.createCell(8), strStartHour);
        
        /**CLASIFICATION*/
        HSSFRow row3= sheet.createRow((short)3);
        addContentAndStyle(row3.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_CLASIFICACION);
        addContentAndStyle(row3.createCell(8),reportLogger.getReport().getReportClasification());

        
        /**Usuario**/
        HSSFRow row5= sheet.createRow((short)4);
        addContentAndStyle(row5.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_USUARIO);
        addContentAndStyle(row5.createCell(8),reportLogger.getRegistryUser());
        
        int indrow=8;
        
        HSSFRow rowHeader = sheet.createRow((int)indrow);
        addContentAndStyle(rowHeader.createCell(INDEX_ISSUER_PK),getStyleTitle(),ReportConstant.LABEL_CODE_ISSUER);
        addContentAndStyle(rowHeader.createCell(INDEX_BUSSINESS_NAME),getStyleTitle(),ReportConstant.LABEL_BUSSINESS_NAME);
        addContentAndStyle(rowHeader.createCell(INDEX_MNEMONIC),getStyleTitle(),ReportConstant.LABEL_MNEMONIC_ISSUER);
        addContentAndStyle(rowHeader.createCell(INDEX_DOCUMNET_TYPE),getStyleTitle(),ReportConstant.LABEL_DOCUMENT_TYPE);
        addContentAndStyle(rowHeader.createCell(INDEX_DOCUMENT_NUMBER),getStyleTitle(),ReportConstant.LABEL_DOCUMENT_NUM);
        addContentAndStyle(rowHeader.createCell(INDEX_ID_HOLDER_FK),getStyleTitle(),ReportConstant.LABEL_CUI);
        addContentAndStyle(rowHeader.createCell(INDEX_PHONE_NUMBER),getStyleTitle(),ReportConstant.LABEL_PHONE_NUMBER);
        addContentAndStyle(rowHeader.createCell(INDEX_ECONOMIC_ACTIVITY),getStyleTitle(),ReportConstant.LABEL_ECONOMIC_ACTIVITY);
        addContentAndStyle(rowHeader.createCell(INDEX_REGISTRY_DATE),getStyleTitle(),ReportConstant.LABEL_DATE_REGISTER);
        addContentAndStyle(rowHeader.createCell(INDEX_STATE_ISSUER),getStyleTitle(),ReportConstant.LABEL_STATE);
        
        HSSFRow rowData;
        for(Object[] rowResult:queryMain) {
        	indrow++;
        	rowData = sheet.createRow((int)indrow);
        	addContentAndStyle(rowData.createCell(INDEX_ISSUER_PK),getStyleAccountingAccountLeft(), 		rowResult[0] != null ? rowResult[0].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_BUSSINESS_NAME),getStyleAccountingAccountLeft(), 	rowResult[1] != null ? rowResult[1].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_MNEMONIC),getStyleAccountingAccountLeft(), 			rowResult[2] != null ? rowResult[2].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_DOCUMNET_TYPE),getStyleAccountingAccountLeft(), 	rowResult[3] != null ? rowResult[3].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_DOCUMENT_NUMBER),getStyleAccountingAccountLeft(), 	rowResult[4] != null ? rowResult[4].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_ID_HOLDER_FK),getStyleAccountingAccountLeft(), 		rowResult[5] != null ? rowResult[5].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_PHONE_NUMBER),getStyleAccountingAccountLeft(), 		rowResult[6] != null ? rowResult[6].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_ECONOMIC_ACTIVITY),getStyleAccountingAccountLeft(), rowResult[7] != null ? rowResult[7].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_REGISTRY_DATE),getStyleAccountingAccountLeft(), 	rowResult[8] != null ? rowResult[8].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_STATE_ISSUER),getStyleAccountingAccountLeft(), 		rowResult[9] != null ? rowResult[9].toString() : "-");
        	
        }
	}
	
	public byte[] writeFileExcelEDVComissionSettlementOperation(Map<String, Object> parameters , ReportLogger reportLogger  ){
		
		byte[] arrayByte=null;

		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("lstObjects");

		initCellStyle();
		initFont();
		initDataFormat();
		
		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						writeFileExcelEDVComissionSettlementOperationPoi(hSSFWorkbook,queryMain.subList(i, ifinal),queryMain.subList(i, ifinal).size(),reportLogger);
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{
	        		//Solo un HSSFSheet
	        		writeFileExcelEDVComissionSettlementOperationPoi(hSSFWorkbook,queryMain,queryMain.size(),reportLogger);
	        	}
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_COMMISSIONS_SUMMARY_PORTFOLIO);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	         
	       	hSSFWorkbook.write(bos);
	        
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        
	        hSSFWorkbook.write(fileOut);
	        
	        fileOut.close();
			
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
		return arrayByte;
	}
	
	public void writeFileExcelEDVComissionSettlementOperationPoi(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, int number,ReportLogger reportLogger) {
		
		int INDEX_PARTICIPANT_COMISSION = 1;
		int INDEX_MONTO_COMISISON = 2;
		int INDEX_PARTICIPANT_APORTE = 4;
		int INDEX_MONTO_APORTE = 5;
		
		//creando la hoja
		HSSFSheet sheet = hSSFWorkbook.createSheet("Comision por Liq Operaciones");
		
		//cabecera del reporte
		String titleReport=reportLogger.getReport().getTitle();
		String strStartHour = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),ReportConstant.DATE_FORMAT_TIME_SECOND);
		
		HSSFRow row0= sheet.createRow((short)0);
        sheet.addMergedRegion(new CellRangeAddress(0,0,1,8));
        addContentAndStyle(row0.createCell(1),getStyleTitleBoldCenter(),titleReport);
        
        /**DATE*/
        HSSFRow row1= sheet.createRow((short)1);
        addContentAndStyle(row1.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_FECHA);
        addContentAndStyle(row1.createCell(8),CommonsUtilities.convertDatetoString(reportLogger.getRegistryDate()));
        
        /**HOUR*/
        HSSFRow row2= sheet.createRow((short)2);
        addContentAndStyle(row2.createCell(7),getStyleTitleBold(), ReportConstant.LABEL_HORA);
        addContentAndStyle(row2.createCell(8), strStartHour);
        
        /**CLASIFICATION*/
        HSSFRow row3= sheet.createRow((short)3);
        addContentAndStyle(row3.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_CLASIFICACION);
        addContentAndStyle(row3.createCell(8),reportLogger.getReport().getReportClasification());

        
        /**Usuario**/
        HSSFRow row5= sheet.createRow((short)4);
        addContentAndStyle(row5.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_USUARIO);
        addContentAndStyle(row5.createCell(8),reportLogger.getRegistryUser());
        
        int indrow=8;
        
        HSSFRow rowHeader = sheet.createRow((int)indrow);
        addContentAndStyle(rowHeader.createCell(INDEX_PARTICIPANT_COMISSION),getStyleTitle(),ReportConstant.LABEL_BAG_AGENCY);
        addContentAndStyle(rowHeader.createCell(INDEX_MONTO_COMISISON),getStyleTitle(),ReportConstant.LABEL_COMISSION_USD);
        addContentAndStyle(rowHeader.createCell(INDEX_PARTICIPANT_APORTE),getStyleTitle(),ReportConstant.LABEL_BAG_AGENCY);
        addContentAndStyle(rowHeader.createCell(INDEX_MONTO_APORTE),getStyleTitle(),ReportConstant.LABEL_CONTRIBUTION_FUND);
        
        int rowInitData=indrow+2;
        
        HSSFRow rowData;
        for(Object[] rowResult:queryMain) {
        	indrow++;
        	rowData = sheet.createRow((int)indrow);
        	addContentAndStyle(rowData.createCell(INDEX_PARTICIPANT_COMISSION),getStyleAccountingAccountLeft(), rowResult[1] != null ? rowResult[1].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_MONTO_COMISISON),getStyleAccountingAccountAmountIssue1091(), rowResult[2] != null ? Double.parseDouble(rowResult[2].toString()) : 0.00);
        	addContentAndStyle(rowData.createCell(INDEX_PARTICIPANT_APORTE),getStyleAccountingAccountLeft(), rowResult[1] != null ? rowResult[1].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_MONTO_APORTE),getStyleAccountingAccountAmountIssue1091(), rowResult[3] != null ? Double.parseDouble(rowResult[3].toString()) : 0.00);
        }
        
        // aca se tiene los totales
        rowData = sheet.createRow(indrow+1);
        addContentAndStyle(rowData.createCell(INDEX_PARTICIPANT_COMISSION),getStyleAccountingAccountAmountBoldIssue1091(), "TOTAL");
        addFormulaAndStyle(rowData.createCell(INDEX_MONTO_COMISISON),getStyleAccountingAccountAmountBoldIssue1091(), "SUM(C"+rowInitData+":C"+(rowInitData+queryMain.size()-1)+")");
        addContentAndStyle(rowData.createCell(INDEX_PARTICIPANT_APORTE),getStyleAccountingAccountAmountBoldIssue1091(), "TOTAL");
        addFormulaAndStyle(rowData.createCell(INDEX_MONTO_APORTE),getStyleAccountingAccountAmountBoldIssue1091(), "SUM(F"+rowInitData+":F"+(rowInitData+queryMain.size()-1)+")");
	}
	
	/**
	 * metodo para la generacion del reporte 
	 * REPORTE DE REGISTRO DE VALORES EN SISTEMA DE ANOTACION EN CUENTA VOLUNTARIA POR VENTA Y TESORO DIRECTO
	 * issue 1053
	 * @param parameters
	 * @param reportLogger
	 * @return
	 */
	public byte[] writeFileExcelRegisterSecuritiesEntSysDirect(Map<String, Object> parameters , ReportLogger reportLogger  ){
		
		byte[] arrayByte=null;

		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("lstObjects");

		initCellStyle();
		initFont();
		initDataFormat();
		
		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						writeFileExcelRegisterSecuritiesEntSysIsDirect(hSSFWorkbook,queryMain.subList(i, ifinal),queryMain.subList(i, ifinal).size(),reportLogger);
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{
	        		//Solo un HSSFSheet
	        		writeFileExcelRegisterSecuritiesEntSysIsDirect(hSSFWorkbook,queryMain,queryMain.size(),reportLogger);
	        	}
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_COMMISSIONS_SUMMARY_PORTFOLIO);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	         
	       	hSSFWorkbook.write(bos);
	        
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        
	        hSSFWorkbook.write(fileOut);
	        
	        fileOut.close();
			
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
		return arrayByte;
	}
	
	/**
	 * metodo para la generacion del reporte en Apache POI
	 * REPORTE DE REGISTRO DE VALORES EN SISTEMA DE ANOTACION EN CUENTA VOLUNTARIA POR VENTA Y TESORO DIRECTO
	 * issue 1053
	 * @param hSSFWorkbook
	 * @param queryMain
	 * @param number
	 * @param reportLogger
	 */
	public void writeFileExcelRegisterSecuritiesEntSysIsDirect(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, int number,ReportLogger reportLogger) {
		
		int INDEX_SECURITY_CODE = 1;
		int INDEX_CURRENCY = 2;
		int INDEX_NOMINAL_VALUE = 3;
		int INDEX_ISSUANCE_DATE = 4;
		int INDEX_EXPIRATION_DATE = 5;
		int INDEX_INTEREST_RATE = 6;
		int INDEX_MNEMONIC = 7;
		int INDEX_DEPARTAMENT = 8;
		int INDEX_LOCALITY = 9;
		int INDEX_ACCOUNT_NUMBER = 10;
		int INDEX_CUI = 11;
		int INDEX_DESCRIPTION = 12;
		int INDEX_IS_RESIDENT = 13;
		int INDEX_DOCUMENT_TYPE = 14;
		int INDEX_NUM_DOCUMENT = 15;
		int INDEX_TOTAL_BALANCE = 16;
		int INDEX_NOMINAL_TOTAL = 17;
		
		//creando la hoja
		HSSFSheet sheet = hSSFWorkbook.createSheet("ACV Venta Directa");
		
		//cabecera del reporte
		String titleReport=reportLogger.getReport().getTitle();
		String strStartHour = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),ReportConstant.DATE_FORMAT_TIME_SECOND);
		
		HSSFRow row0= sheet.createRow((short)0);
        sheet.addMergedRegion(new CellRangeAddress(0,0,1,8));
        addContentAndStyle(row0.createCell(1),getStyleTitleBoldCenter(),titleReport);
        
        /**DATE*/
        HSSFRow row1= sheet.createRow((short)1);
        addContentAndStyle(row1.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_FECHA);
        addContentAndStyle(row1.createCell(8),CommonsUtilities.convertDatetoString(reportLogger.getRegistryDate()));
        
        /**HOUR*/
        HSSFRow row2= sheet.createRow((short)2);
        addContentAndStyle(row2.createCell(7),getStyleTitleBold(), ReportConstant.LABEL_HORA);
        addContentAndStyle(row2.createCell(8), strStartHour);
        
        /**CLASIFICATION*/
        HSSFRow row3= sheet.createRow((short)3);
        addContentAndStyle(row3.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_CLASIFICACION);
        addContentAndStyle(row3.createCell(8),reportLogger.getReport().getReportClasification());

        
        /**Usuario**/
        HSSFRow row5= sheet.createRow((short)4);
        addContentAndStyle(row5.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_USUARIO);
        addContentAndStyle(row5.createCell(8),reportLogger.getRegistryUser());
        
        int indrow=8;
        
        HSSFRow rowHeader = sheet.createRow((int)indrow);
        addContentAndStyle(rowHeader.createCell(INDEX_SECURITY_CODE),getStyleTitle(),ReportConstant.LABEL_SECURITY_CLASS_CODE);
        addContentAndStyle(rowHeader.createCell(INDEX_CURRENCY),getStyleTitle(),ReportConstant.LABEL_CURRENCY_MNEMONIC);
        addContentAndStyle(rowHeader.createCell(INDEX_NOMINAL_VALUE),getStyleTitle(),ReportConstant.LABEL_VN);
        addContentAndStyle(rowHeader.createCell(INDEX_ISSUANCE_DATE),getStyleTitle(),ReportConstant.LABEL_DATE_ISSUANCE);
        addContentAndStyle(rowHeader.createCell(INDEX_EXPIRATION_DATE),getStyleTitle(),ReportConstant.LABEL_DUE_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_INTEREST_RATE),getStyleTitle(),ReportConstant.LABEL_NOMINAL_RATE);
        addContentAndStyle(rowHeader.createCell(INDEX_MNEMONIC),getStyleTitle(),ReportConstant.LABEL_PLACING_ENTITY);
        addContentAndStyle(rowHeader.createCell(INDEX_DEPARTAMENT),getStyleTitle(),ReportConstant.LABEL_DEPARTAMENT);
        addContentAndStyle(rowHeader.createCell(INDEX_LOCALITY),getStyleTitle(),ReportConstant.LABEL_LOCALITY);
        addContentAndStyle(rowHeader.createCell(INDEX_ACCOUNT_NUMBER),getStyleTitle(),ReportConstant.LABEL_HOLDER_ACCOUNT);
        addContentAndStyle(rowHeader.createCell(INDEX_CUI),getStyleTitle(),ReportConstant.LABEL_CUI);
        addContentAndStyle(rowHeader.createCell(INDEX_DESCRIPTION),getStyleTitle(),ReportConstant.LABEL_CUI_DESCRIPTION);
        addContentAndStyle(rowHeader.createCell(INDEX_IS_RESIDENT),getStyleTitle(),ReportConstant.LABEL_IS_RESIDENT);
        addContentAndStyle(rowHeader.createCell(INDEX_DOCUMENT_TYPE),getStyleTitle(),ReportConstant.LABEL_DOCUMENT_TYPE);
        addContentAndStyle(rowHeader.createCell(INDEX_NUM_DOCUMENT),getStyleTitle(),ReportConstant.LABEL_DOCUMENT_NUM);
        addContentAndStyle(rowHeader.createCell(INDEX_TOTAL_BALANCE),getStyleTitle(),ReportConstant.LABEL_STOCK_QUANTITY);
        addContentAndStyle(rowHeader.createCell(INDEX_NOMINAL_TOTAL),getStyleTitle(),ReportConstant.LABEL_TOTAL_NV);
        
        HSSFRow rowData;
        for(Object[] rowResult:queryMain) {
        	indrow++;
        	rowData = sheet.createRow((int)indrow);
        	addContentAndStyle(rowData.createCell(INDEX_SECURITY_CODE),getStyleAccountingAccount(), rowResult[0] != null ? rowResult[0].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_CURRENCY),getStyleAccountingAccount(), rowResult[1] != null ? rowResult[1].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_NOMINAL_VALUE),getStyleAccountingAccount(), rowResult[2] != null ? rowResult[2].toString() : "0");
        	addContentAndStyle(rowData.createCell(INDEX_ISSUANCE_DATE),getStyleAccountingAccount(), rowResult[3] != null ? rowResult[3].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_EXPIRATION_DATE),getStyleAccountingAccount(), rowResult[4] != null ? rowResult[4].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_INTEREST_RATE),getStyleAccountingAccount(), rowResult[5] != null ? rowResult[5].toString() : "0");
        	addContentAndStyle(rowData.createCell(INDEX_MNEMONIC),getStyleAccountingAccount(), rowResult[6] != null ? rowResult[6].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_DEPARTAMENT),getStyleAccountingAccount(), rowResult[7] != null ? rowResult[7].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_LOCALITY),getStyleAccountingAccount(), rowResult[8] != null ? rowResult[8].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_ACCOUNT_NUMBER),getStyleAccountingAccount(), rowResult[9] != null ? rowResult[9].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_CUI),getStyleAccountingAccount(), rowResult[10] != null ? rowResult[10].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_DESCRIPTION),getStyleAccountingAccount(), rowResult[11] != null ? rowResult[11].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_IS_RESIDENT),getStyleAccountingAccount(), rowResult[12] != null ? rowResult[12].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_DOCUMENT_TYPE),getStyleAccountingAccount(), rowResult[13] != null ? rowResult[13].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_NUM_DOCUMENT),getStyleAccountingAccount(), rowResult[14] != null ? rowResult[14].toString() : "-");
        	addContentAndStyle(rowData.createCell(INDEX_TOTAL_BALANCE),getStyleAccountingAccount(), rowResult[15] != null ? rowResult[15].toString() : "0");
        	addContentAndStyle(rowData.createCell(INDEX_NOMINAL_TOTAL),getStyleAccountingAccount(), rowResult[16] != null ? rowResult[16].toString() : "0");
        }
	}
	
	public byte[] writeFileExcelCommissionsSummaryPortfolio(Map<String, Object> parameters , ReportLogger reportLogger  ){
		
		byte[] arrayByte=null;

		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("lstObjects");

		initCellStyle();
		initFont();
		initDataFormat();
		
		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						writeFileExcelCommissionsSummaryPortfolio(hSSFWorkbook,queryMain.subList(i, ifinal),queryMain.subList(i, ifinal).size(),reportLogger);
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{
	        		//Solo un HSSFSheet
	        		writeFileExcelCommissionsSummaryPortfolio(hSSFWorkbook,queryMain,queryMain.size(),reportLogger);
	        	}
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_COMMISSIONS_SUMMARY_PORTFOLIO);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	         
	       	hSSFWorkbook.write(bos);
	        
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        
	        hSSFWorkbook.write(fileOut);
	        
	        fileOut.close();
			
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
		return arrayByte;
	}
	
	/**
	 * Bloqueos y Desbloqueos / BlockagesUnlock
	 * @param parameters
	 * @param reportLogger
	 * @return
	 */
	public byte[] writeFileExcelBlockagesUnlock(Map<String, Object> parameters , ReportLogger reportLogger  ){
		
		byte[] arrayByte=null;
		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("str_query");
		initCellStyle();
		initFont();
		initDataFormat();
		
		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						createSheetBlockagesUnlock(hSSFWorkbook,queryMain.subList(i, ifinal),queryMain.subList(i, ifinal).size());
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{//Solo un HSSFSheet
	        		createSheetBlockagesUnlock(hSSFWorkbook,queryMain,queryMain.size());
	        	}
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_BLOCKAGES_UNLOCK);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	       	hSSFWorkbook.write(bos);
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        hSSFWorkbook.write(fileOut);
	        fileOut.close();
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
		return arrayByte;
	}
	
	/**
	 * 
	 * @param hSSFWorkbook
	 * @param queryMain
	 */
	public void createSheetBlockagesUnlock(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, int number){
		
		int INDEX_PARTICIPANT=0;
		int INDEX_ACCOUNT_NUMBER=1;
		int INDEX_HOLDER=2;
		int INDEX_BLOCK_OPERATION=3;
		int INDEX_REQ_BLOCK_OPERATION=4;
		int INDEX_UNLOCK_OPERATION=5;
		int INDEX_REQ_UNLOCK_OPERATION=6;
		int INDEX_BLOCK_DATE=7;
		int INDEX_UNLOCK_DATE=8;
		int INDEX_BLOCK_TYPE=9;
		int INDEX_SECURITY=10;
		int INDEX_STOCK_QUANTITY=11;
		int INDEX_STATE=12;
		int INDEX_USER=13;
	
		/**Created a new sheet within the workBock**/
    	HSSFSheet sheet = hSSFWorkbook.createSheet("BlockagesUnlock"+number);
    	
		/***** BEGIN - REPORT HEADER  */

        int indrow=1;

        /**Created Header DEBE - HABER*/
        HSSFRow rowHeader = sheet.createRow((int)indrow);
       
        addContentAndStyle(rowHeader.createCell(INDEX_PARTICIPANT),getStyleTitle(),ReportConstant.LABEL_PARTICIPANT);
        addContentAndStyle(rowHeader.createCell(INDEX_ACCOUNT_NUMBER),getStyleTitle(),ReportConstant.LABEL_HOLDER_ACCOUNT);
        addContentAndStyle(rowHeader.createCell(INDEX_HOLDER),getStyleTitle(),ReportConstant.LABEL_HOLDER);
        addContentAndStyle(rowHeader.createCell(INDEX_BLOCK_OPERATION),getStyleTitle(),ReportConstant.LABEL_BLOCK_OPERATION);
        addContentAndStyle(rowHeader.createCell(INDEX_REQ_BLOCK_OPERATION),getStyleTitle(),ReportConstant.LABEL_REQ_BLOCK_NUMBER);
        addContentAndStyle(rowHeader.createCell(INDEX_UNLOCK_OPERATION),getStyleTitle(),ReportConstant.LABEL_UNLOCK_OPERATION);
        addContentAndStyle(rowHeader.createCell(INDEX_REQ_UNLOCK_OPERATION),getStyleTitle(),ReportConstant.LABEL_REQ_UNLOCK_NUMBER);
        addContentAndStyle(rowHeader.createCell(INDEX_BLOCK_DATE),getStyleTitle(),ReportConstant.LABEL_DATE_BLOCK);
        addContentAndStyle(rowHeader.createCell(INDEX_UNLOCK_DATE),getStyleTitle(),ReportConstant.LABEL_DATE_UNLOCK);
        addContentAndStyle(rowHeader.createCell(INDEX_BLOCK_TYPE),getStyleTitle(),ReportConstant.LABEL_BLOCK_TYPE);
        addContentAndStyle(rowHeader.createCell(INDEX_SECURITY),getStyleTitle(),ReportConstant.LABEL_SECURITY_CLASS_CODE);
        addContentAndStyle(rowHeader.createCell(INDEX_STOCK_QUANTITY),getStyleTitle(),ReportConstant.LABEL_STOCK_QUANTITY);
        addContentAndStyle(rowHeader.createCell(INDEX_STATE),getStyleTitle(),ReportConstant.LABEL_STATE);
        addContentAndStyle(rowHeader.createCell(INDEX_USER),getStyleTitle(),ReportConstant.LABEL_USUARIO);
        
        for (Object[] query : queryMain) {
        	
        	String participant=query[13].toString();
        	String accountNumber=query[14].toString();
        	String holder=query[15].toString();
        	String blockOperation=query[0].toString();
        	String reqBlockOperation=query[1].toString();
        	String unlockOperation=query[2]!=null ? (query[2].toString()) : ("0");;
        	String reqUnlockOperation=query[4]!=null ? (query[3].toString()) : ("0");;
        	String blockDate=query[5].toString();
        	String unlockDate=query[6]!=null ? (query[5].toString()) : ("0");;
        	String blockType=query[7].toString();
        	String security=query[8].toString();
        	Double stockQuantity=query[10]!=null ? Double.parseDouble(query[10].toString()) : Double.parseDouble("0");;
        	String user=query[11].toString();
        	String blockState=query[12].toString();
        	
	        indrow++;
	        /** La data */
	        HSSFRow rowData = sheet.createRow((int)indrow);

	        addContentAndStyle(rowData.createCell(INDEX_PARTICIPANT),getStyleAccountingAccount(), participant);
	        addContentAndStyle(rowData.createCell(INDEX_ACCOUNT_NUMBER),getStyleAccountingAccount(), accountNumber);
	        addContentAndStyle(rowData.createCell(INDEX_HOLDER),getStyleAccountingAccount(), holder);
	        addContentAndStyle(rowData.createCell(INDEX_BLOCK_OPERATION),getStyleAccountingAccount(), blockOperation);
	        addContentAndStyle(rowData.createCell(INDEX_REQ_BLOCK_OPERATION),getStyleAccountingAccount(), reqBlockOperation);
	        addContentAndStyle(rowData.createCell(INDEX_UNLOCK_OPERATION),getStyleAccountingAccount(), unlockOperation);
	        addContentAndStyle(rowData.createCell(INDEX_REQ_UNLOCK_OPERATION),getStyleAccountingAccount(), reqUnlockOperation );
	        addContentAndStyle(rowData.createCell(INDEX_BLOCK_DATE),getStyleAccountingAccount(), blockDate);
	        addContentAndStyle(rowData.createCell(INDEX_UNLOCK_DATE),getStyleAccountingAccount(), unlockDate);
	        addContentAndStyle(rowData.createCell(INDEX_BLOCK_TYPE),getStyleAccountingAccount(), blockType);
	        addContentAndStyle(rowData.createCell(INDEX_SECURITY),getStyleAccountingAccount(), security);
	        addContentAndStyle(rowData.createCell(INDEX_STOCK_QUANTITY),getStyleAccountingAccount(), stockQuantity.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_STATE),getStyleAccountingAccount(), blockState );
	        addContentAndStyle(rowData.createCell(INDEX_USER),getStyleAccountingAccount(), user );
		}
        /**adjustColumns**/
        //adjustColumns(sheet,15);
	}
	
	
	/**
	 * Solicitudes de cat a valor nominal / RequestAccreHolderNominalValuePoiReport
	 * @param parameters
	 * @param reportLogger
	 * @return
	 */
	public byte[] writeFileExcelRequestAccreditation(Map<String, Object> parameters , ReportLogger reportLogger  ){
		
		byte[] arrayByte=null;
		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("str_query");
		initCellStyle();
		initFont();
		initDataFormat();
		
		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						createSheetRequestAccreditation(hSSFWorkbook,queryMain.subList(i, ifinal),queryMain.subList(i, ifinal).size());
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{//Solo un HSSFSheet
	        		createSheetRequestAccreditation(hSSFWorkbook,queryMain,queryMain.size());
	        	}
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_REQUEST_ACCREDITATION);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	       	hSSFWorkbook.write(bos);
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        hSSFWorkbook.write(fileOut);
	        fileOut.close();
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
		return arrayByte;
	}
	
	/**
	 * Solicitudes de cat a valor nominal / RequestAccreHolderNominalValuePoiReport
	 * @param hSSFWorkbook
	 * @param queryMain
	 */
	public void createSheetRequestAccreditation(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, int number){
		
		int INDEX_PARTICIPANT=1;
		int INDEX_OPERATION=3;
		int INDEX_MOTIVE=2;
		int INDEX_REGISTER_DATE=0;
		int INDEX_ISSUANCE_DATE=4;
		int INDEX_SECURITY=5;
		int INDEX_CURRENCY=8;
		int INDEX_CERT_TYPE=6;
		int INDEX_BLOCK_TYPE=7;
		int INDEX_TOTAL_BALANCE=9;
		int INDEX_UNIT_NOMINAL_VALUE=10;
		int INDEX_TOTAL_NOMINAL_VALUE_USD=11;
		int INDEX_STATE=12;

		/**Created a new sheet within the workBock**/
    	HSSFSheet sheet = hSSFWorkbook.createSheet("RequestAccreditation"+number);
    	
		/***** BEGIN - REPORT HEADER  */

        int indrow=1;

        /**Created Header DEBE - HABER*/
        HSSFRow rowHeader = sheet.createRow((int)indrow);
       
        addContentAndStyle(rowHeader.createCell(INDEX_PARTICIPANT),getStyleTitle(),ReportConstant.LABEL_PARTICIPANT);
        addContentAndStyle(rowHeader.createCell(INDEX_OPERATION),getStyleTitle(),ReportConstant.LABEL_REQUEST_OPERATION);
        addContentAndStyle(rowHeader.createCell(INDEX_MOTIVE),getStyleTitle(),ReportConstant.LABEL_MOTIVE);
        addContentAndStyle(rowHeader.createCell(INDEX_REGISTER_DATE),getStyleTitle(),ReportConstant.LABEL_DATE_REGISTER);
        addContentAndStyle(rowHeader.createCell(INDEX_ISSUANCE_DATE),getStyleTitle(),ReportConstant.LABEL_DATE_ISSUANCE);
        addContentAndStyle(rowHeader.createCell(INDEX_SECURITY),getStyleTitle(),ReportConstant.LABEL_SECURITY_CLASS_CODE);
        addContentAndStyle(rowHeader.createCell(INDEX_CURRENCY),getStyleTitle(),ReportConstant.LABEL_CURRENCY);
        addContentAndStyle(rowHeader.createCell(INDEX_CERT_TYPE),getStyleTitle(),ReportConstant.LABEL_CERTIFICATION_TYPE);
        addContentAndStyle(rowHeader.createCell(INDEX_BLOCK_TYPE),getStyleTitle(),ReportConstant.LABEL_BLOCK_TYPE);
        addContentAndStyle(rowHeader.createCell(INDEX_TOTAL_BALANCE),getStyleTitle(),ReportConstant.LABEL_TOTAL_BALANCE_CERT);
        addContentAndStyle(rowHeader.createCell(INDEX_UNIT_NOMINAL_VALUE),getStyleTitle(),ReportConstant.LABEL_INIT_NOMINAL_VALUE);
        addContentAndStyle(rowHeader.createCell(INDEX_TOTAL_NOMINAL_VALUE_USD),getStyleTitle(),ReportConstant.LABEL_TOTAL_NOMINAL_VALUE);
        addContentAndStyle(rowHeader.createCell(INDEX_STATE),getStyleTitle(),ReportConstant.LABEL_STATE);
        
        for (Object[] query : queryMain) {
        	
        	String participant=query[1].toString();
        	String operation=query[3].toString();
        	String motive=query[2].toString();
        	String registerDate=query[0].toString();
        	String issuanceDate=query[4].toString();
        	String security=query[5].toString();
        	String currency=query[8].toString();
        	String certificationType=query[6].toString();
        	String blockType=query[7]!=null ? (query[7].toString()) : ("0");;
        	Double totalBalanceAccre=query[9]!=null ? Double.parseDouble(query[9].toString()) : Double.parseDouble("0");;
        	Double unitNominalValue=query[10]!=null ? Double.parseDouble(query[10].toString()) : Double.parseDouble("0");;
        	Double totalNominalValueUSD=query[12]!=null ? Double.parseDouble(query[12].toString()) : Double.parseDouble("0");;
        	String state=query[13].toString();
        	
	        indrow++;
	        /** La data */
	        HSSFRow rowData = sheet.createRow((int)indrow);

	        addContentAndStyle(rowData.createCell(INDEX_PARTICIPANT),getStyleAccountingAccount(), participant);
	        addContentAndStyle(rowData.createCell(INDEX_OPERATION),getStyleAccountingAccount(), operation);
	        addContentAndStyle(rowData.createCell(INDEX_MOTIVE),getStyleAccountingAccount(), motive);
	        addContentAndStyle(rowData.createCell(INDEX_REGISTER_DATE),getStyleAccountingAccount(), registerDate);
	        addContentAndStyle(rowData.createCell(INDEX_ISSUANCE_DATE),getStyleAccountingAccount(), issuanceDate);
	        addContentAndStyle(rowData.createCell(INDEX_SECURITY),getStyleAccountingAccount(), security);
	        addContentAndStyle(rowData.createCell(INDEX_CURRENCY),getStyleAccountingAccount(), currency );
	        addContentAndStyle(rowData.createCell(INDEX_CERT_TYPE),getStyleAccountingAccount(), certificationType);
	        addContentAndStyle(rowData.createCell(INDEX_BLOCK_TYPE),getStyleAccountingAccount(), blockType);
	        addContentAndStyle(rowData.createCell(INDEX_TOTAL_BALANCE),getStyleAccountingAccount(), totalBalanceAccre.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_UNIT_NOMINAL_VALUE),getStyleAccountingAccount(), unitNominalValue.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_TOTAL_NOMINAL_VALUE_USD),getStyleAccountingAccount(), totalNominalValueUSD.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_STATE),getStyleAccountingAccount(), state );
		}
        /**adjustColumns**/
        //adjustColumns(sheet,15);
	}
	
public void writeFileExcelExpirationSecurities(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, int number,ReportLogger reportLogger,ExpirationReportTO expirationReportTO){
		
		int INDEX_CLASE_VALOR=1;
		int INDEX_CLAVE_VALOR=2;
		int INDEX_NUMERO_CUPON=3;
		int INDEX_FECHA_VENCIMIENTO=4;
		int INDEX_MONTO=5;
		int INDEX_CANTIDAD=6;
		int INDEX_TIPO_DOCUMENTO=7;
		int INDEX_NUMERO_DOCUMENTO=8;
		int INDEX_NOMBRE=9;
		int INDEX_AP_PATERNO=10;
		int INDEX_AP_MATERNO=11;
		int INDEX_PERSONA=12;
		int INDEX_FECHA_NACIMIENTO=13;
		int INDEX_ESTADO=14;
		int INDEX_CANTIDAD_BLOQUEADOS=15;
	
		
		/**Created a new sheet within the workBock**/
    	HSSFSheet sheet = hSSFWorkbook.createSheet("REP_COLOCACIONES_TGN.PHP-2");
    	
		/*****
         * BEGIN - REPORT HEADER 
         */
    	
    	String titleReport=reportLogger.getReport().getTitle();
    	String strStartHour = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),ReportConstant.DATE_FORMAT_TIME_SECOND);
    	
    	/**TITLE*/
        HSSFRow row0= sheet.createRow((short)0);
        sheet.addMergedRegion(new CellRangeAddress(0,0,1,9));
        addContentAndStyle(row0.createCell(1),getStyleTitleBoldCenter(),titleReport);
        
        HSSFRow rowSubTittle= sheet.createRow((short)1);
        sheet.addMergedRegion(new CellRangeAddress(1,1,1,9));
        addContentAndStyle(rowSubTittle.createCell(1),getStyleTitleBoldCenter(),"Tesoro Directo");
        
        HSSFRow rowFechas= sheet.createRow((short)2);
        sheet.addMergedRegion(new CellRangeAddress(2,2,1,9));
        addContentAndStyle(rowFechas.createCell(1),getStyleTitleBoldCenter(),"Del " + expirationReportTO.getInitialDate() + " al " + expirationReportTO.getEndDate());
        
        /**DATE*/
        HSSFRow row1= sheet.createRow((short)3);
        addContentAndStyle(row1.createCell(10),getStyleTitleBold(),ReportConstant.LABEL_FECHA);
        addContentAndStyle(row1.createCell(11),CommonsUtilities.convertDatetoString(reportLogger.getRegistryDate()));
        
        /**HOUR*/
        HSSFRow row2= sheet.createRow((short)4);
        addContentAndStyle(row2.createCell(10),getStyleTitleBold(), ReportConstant.LABEL_HORA);
        addContentAndStyle(row2.createCell(11), strStartHour);
        
        /**CLASIFICATION*/
        HSSFRow row3= sheet.createRow((short)5);
        addContentAndStyle(row3.createCell(10),getStyleTitleBold(),ReportConstant.LABEL_CLASIFICACION);
        addContentAndStyle(row3.createCell(11),reportLogger.getReport().getReportClasification());

        
        /**Usuario**/
        HSSFRow row5= sheet.createRow((short)6);
        addContentAndStyle(row5.createCell(10),getStyleTitleBold(),ReportConstant.LABEL_USUARIO);
        addContentAndStyle(row5.createCell(11),reportLogger.getRegistryUser());
        
        /****
         * END - REPORT HEADER 
         */
        
        int indrow=8;

        /**Created Header**/
        
        HSSFRow rowHeader = sheet.createRow((int)indrow);
        addContentAndStyle(rowHeader.createCell(INDEX_CLASE_VALOR),getStyleTitle(),ReportConstant.LABEL_SECURITY_CLASS);
        addContentAndStyle(rowHeader.createCell(INDEX_CLAVE_VALOR),getStyleTitle(),"CLAVE VALOR");
        addContentAndStyle(rowHeader.createCell(INDEX_NUMERO_CUPON),getStyleTitle(),"NUMERO CUPON");
        addContentAndStyle(rowHeader.createCell(INDEX_FECHA_VENCIMIENTO),getStyleTitle(),"FECHA VENCIMIENTO");
        addContentAndStyle(rowHeader.createCell(INDEX_MONTO),getStyleTitle(),"MONTO");
        addContentAndStyle(rowHeader.createCell(INDEX_CANTIDAD),getStyleTitle(),"CANTIDAD");
        addContentAndStyle(rowHeader.createCell(INDEX_TIPO_DOCUMENTO),getStyleTitle(),"TIPO DOCUMENTO");
        addContentAndStyle(rowHeader.createCell(INDEX_NUMERO_DOCUMENTO),getStyleTitle(),"NUMERO DOCUMENTO");
        addContentAndStyle(rowHeader.createCell(INDEX_NOMBRE),getStyleTitle(),"NOMBRE");
        addContentAndStyle(rowHeader.createCell(INDEX_AP_PATERNO),getStyleTitle(),"AP PATERNO");
        addContentAndStyle(rowHeader.createCell(INDEX_AP_MATERNO),getStyleTitle(),"AP MATERNO");
        addContentAndStyle(rowHeader.createCell(INDEX_PERSONA),getStyleTitle(),"PERSONA");
        addContentAndStyle(rowHeader.createCell(INDEX_FECHA_NACIMIENTO),getStyleTitle(),"FECHA NACIMIENTO");
        addContentAndStyle(rowHeader.createCell(INDEX_ESTADO),getStyleTitle(),"ESTADO");
        addContentAndStyle(rowHeader.createCell(INDEX_CANTIDAD_BLOQUEADOS),getStyleTitle(),"BLOQUEADOS");

        
        for (Object[] query : queryMain) {
        	
        	String claseValor=query[0]!=null ? query[0].toString() : GeneralConstants.EMPTY_STRING;
        	String claveValor=query[1]!=null ? query[1].toString() : GeneralConstants.EMPTY_STRING;
        	String numeroCupon=query[2]!=null ? query[2].toString() : GeneralConstants.EMPTY_STRING;
        	String fechaVencimiento=query[3]!=null ? query[3].toString() : GeneralConstants.EMPTY_STRING;
        	String monto = query[4]!=null ? query[4].toString() : GeneralConstants.EMPTY_STRING;
        	String cantidad = query[5]!=null ? query[5].toString() : GeneralConstants.EMPTY_STRING;
        	String tipoDocumento = GeneralConstants.EMPTY_STRING;
        	if(Validations.validateIsNotNullAndNotEmpty(query[6])){
        		tipoDocumento = query[6].toString();
        	}
        	String numDocumento = GeneralConstants.EMPTY_STRING;
        	if(Validations.validateIsNotNullAndNotEmpty(query[7])){
        		numDocumento = query[7].toString();
        	}
        	String nombre = GeneralConstants.EMPTY_STRING;
        	if(Validations.validateIsNotNullAndNotEmpty(query[8])){
        		nombre = query[8].toString();
        	}
        	String apPaterno = GeneralConstants.EMPTY_STRING;
        	if(Validations.validateIsNotNullAndNotEmpty(query[9])){
        		apPaterno = query[9].toString();
        	}
        	String apMaterno = GeneralConstants.EMPTY_STRING;
        	if(Validations.validateIsNotNullAndNotEmpty(query[10])){
        		apMaterno = query[10].toString();
        	}
        	String persona = GeneralConstants.EMPTY_STRING;
        	if(Validations.validateIsNotNullAndNotEmpty(query[11])){
        		persona = query[11].toString();
        	}
        	String fechaNacimiento = GeneralConstants.EMPTY_STRING;
        	if(Validations.validateIsNotNullAndNotEmpty(query[12])){
        		fechaNacimiento = query[12].toString();
        	}
        	String estado = GeneralConstants.EMPTY_STRING;
        	if(Validations.validateIsNotNullAndNotEmpty(query[13])){
        		estado = query[13].toString();
        	}
        	String cantidadBloqueado = query[14]!=null ? query[14].toString() : GeneralConstants.EMPTY_STRING;

	        indrow++;
	        
	        /**
	         * La data 
	         */
	        HSSFRow rowData = sheet.createRow((int)indrow);
	        addContentAndStyle(rowData.createCell(INDEX_CLASE_VALOR),getStyleAccountingAccount(),claseValor);
	        addContentAndStyle(rowData.createCell(INDEX_CLAVE_VALOR),getStyleAccountingAccount(),claveValor);
	        addContentAndStyle(rowData.createCell(INDEX_NUMERO_CUPON),getStyleAccountingAccount(),numeroCupon);
	        addContentAndStyle(rowData.createCell(INDEX_FECHA_VENCIMIENTO),getStyleAccountingAccount(),fechaVencimiento);
	        addContentAndStyle(rowData.createCell(INDEX_MONTO),getStyleAccountingAccount(),monto);
	        addContentAndStyle(rowData.createCell(INDEX_CANTIDAD),getStyleAccountingAccount(),cantidad);
	        addContentAndStyle(rowData.createCell(INDEX_TIPO_DOCUMENTO),getStyleAccountingAccount(),tipoDocumento);
	        addContentAndStyle(rowData.createCell(INDEX_NUMERO_DOCUMENTO),getStyleAccountingAccount(),numDocumento);
	        addContentAndStyle(rowData.createCell(INDEX_NOMBRE),getStyleAccountingAccount(),nombre);
	        addContentAndStyle(rowData.createCell(INDEX_AP_PATERNO),getStyleAccountingAccount(),apPaterno);
	        addContentAndStyle(rowData.createCell(INDEX_AP_MATERNO),getStyleAccountingAccount(),apMaterno);
	        addContentAndStyle(rowData.createCell(INDEX_PERSONA),getStyleAccountingAccount(),persona);
	        addContentAndStyle(rowData.createCell(INDEX_FECHA_NACIMIENTO),getStyleAccountingAccount(),fechaNacimiento);
	        addContentAndStyle(rowData.createCell(INDEX_ESTADO),getStyleAccountingAccount(),estado);
	        addContentAndStyle(rowData.createCell(INDEX_CANTIDAD_BLOQUEADOS),getStyleAccountingAccount(),cantidadBloqueado);
		}
        adjustColumns(sheet,INDEX_CANTIDAD_BLOQUEADOS);
	}
	
	public byte[] writeFileExcelExpirationSecurities(Map<String, Object> parameters , ReportLogger reportLogger ,ExpirationReportTO expirationReportTO){
		
		byte[] arrayByte=null;

		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("lstObjects");

		initCellStyle();
		initFont();
		initDataFormat();
		
		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						writeFileExcelExpirationSecurities(hSSFWorkbook,queryMain.subList(i, ifinal),queryMain.subList(i, ifinal).size(),reportLogger,expirationReportTO);
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{
	        		//Solo un HSSFSheet
	        		writeFileExcelExpirationSecurities(hSSFWorkbook,queryMain,queryMain.size(),reportLogger,expirationReportTO);
	        	}
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_COMMISSIONS_SUMMARY_PORTFOLIO);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	         
	       	hSSFWorkbook.write(bos);
	        
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        
	        hSSFWorkbook.write(fileOut);
	        
	        fileOut.close();
			
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
		return arrayByte;
	}
	
	/**
	 * Traspaso de valores disponibles / BalanceTransferAvailablePoiReport
	 * @param parameters
	 * @param reportLogger
	 * @return
	 */
	public byte[] writeFileExcelBalanceTransferAvailable(Map<String, Object> parameters , ReportLogger reportLogger  ){
		
		byte[] arrayByte=null;
		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("str_query");
		initCellStyle();
		initFont();
		initDataFormat();
		
		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						createSheetBalanceTransferAvailable(hSSFWorkbook,queryMain.subList(i, ifinal),queryMain.subList(i, ifinal).size());
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{//Solo un HSSFSheet
	        		createSheetBalanceTransferAvailable(hSSFWorkbook,queryMain,queryMain.size());
	        	}
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_BALANCE_TRANSFER_AVAILABLE);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	       	hSSFWorkbook.write(bos);
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        hSSFWorkbook.write(fileOut);
	        fileOut.close();
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
		return arrayByte;
	}
	
	/**
	 * Traspaso de valores disponibles / BalanceTransferAvailablePoiReport
	 * @param hSSFWorkbook
	 * @param queryMain
	 */
	public void createSheetBalanceTransferAvailable(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, int number){
		
		int INDEX_OPERATION=0;
		int INDEX_CUI=1;
		int INDEX_TRANSFER_TYPE=2;
		int INDEX_REGISTER_DATE=3;
		int INDEX_PARTICIPANT_ORIGIN=4;
		int INDEX_ACCOUNT_NUMBER_ORIGIN=5;
		int INDEX_PARTICIPANT_DESTINATION=6;
		int INDEX_ACCOUNT_NUMBER_DESTINATION=7;
		int INDEX_MARKET_DATE=8;
		int INDEX_MARKET_RATE=9;
		int INDEX_MARKET_PRICE=10;
		int INDEX_SECURITY=11;
		int INDEX_STOCK_QUANTITY=12;
		int INDEX_STATE=13;
		int INDEX_USER=14;
		int INDEX_SECURITY_TERM=15;

		/**Created a new sheet within the workBock**/
    	HSSFSheet sheet = hSSFWorkbook.createSheet("BalanceTransferAvailable"+number);
    	
		/***** BEGIN - REPORT HEADER  */

        int indrow=1;

        /**Created Header DEBE - HABER*/
        HSSFRow rowHeader = sheet.createRow((int)indrow);
       
        addContentAndStyle(rowHeader.createCell(INDEX_OPERATION),getStyleTitle(),ReportConstant.LABEL_REQUEST_NUMBER);
        addContentAndStyle(rowHeader.createCell(INDEX_CUI),getStyleTitle(),ReportConstant.LABEL_HOLDER);
        addContentAndStyle(rowHeader.createCell(INDEX_TRANSFER_TYPE),getStyleTitle(),ReportConstant.LABEL_TRANSFER_TYPE);
        addContentAndStyle(rowHeader.createCell(INDEX_REGISTER_DATE),getStyleTitle(),ReportConstant.LABEL_DATE_REGISTER);
        addContentAndStyle(rowHeader.createCell(INDEX_PARTICIPANT_ORIGIN),getStyleTitle(),ReportConstant.LABEL_PARTICIPANT_ORIGIN);
        addContentAndStyle(rowHeader.createCell(INDEX_ACCOUNT_NUMBER_ORIGIN),getStyleTitle(),ReportConstant.LABEL_ACCOUNT_ORIGIN);
        addContentAndStyle(rowHeader.createCell(INDEX_PARTICIPANT_DESTINATION),getStyleTitle(),ReportConstant.LABEL_PARTICIPANT_DESTINATION);
        addContentAndStyle(rowHeader.createCell(INDEX_ACCOUNT_NUMBER_DESTINATION),getStyleTitle(),ReportConstant.LABEL_ACCOUNT_DESTINATION);
        addContentAndStyle(rowHeader.createCell(INDEX_MARKET_DATE),getStyleTitle(),ReportConstant.LABEL_MARKETFACT_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_MARKET_RATE),getStyleTitle(),ReportConstant.LABEL_RATE_PERCENT_MARKETFACT);
        addContentAndStyle(rowHeader.createCell(INDEX_MARKET_PRICE),getStyleTitle(),ReportConstant.LABEL_MARKFACT);
        addContentAndStyle(rowHeader.createCell(INDEX_SECURITY),getStyleTitle(),ReportConstant.LABEL_SECURITY_CLASS_CODE);
        addContentAndStyle(rowHeader.createCell(INDEX_STOCK_QUANTITY),getStyleTitle(),ReportConstant.LABEL_STOCK_QUANTITY);
        addContentAndStyle(rowHeader.createCell(INDEX_SECURITY_TERM),getStyleTitle(),ReportConstant.LABEL_SECURITY_TERM);
        addContentAndStyle(rowHeader.createCell(INDEX_STATE),getStyleTitle(),ReportConstant.LABEL_STATE);
        addContentAndStyle(rowHeader.createCell(INDEX_USER),getStyleTitle(),ReportConstant.LABEL_USER);
        
        for (Object[] query : queryMain) {
        	
        	String operation=query[0].toString();
        	String cui=query[1]!=null ? (query[1].toString()) : (" ");;
        	String transferType=query[4].toString();
        	String registerDate=CommonsUtilities.convertDatetoString((Date) query[5]) ;
        	String participantOrigin=query[6].toString();
        	String accountNumberOrigin=query[7]!=null ? (query[7].toString()) : (" ");;
        	String participantDestination=query[8]!=null ? (query[8].toString()) : ("0");;
        	String accountNumberDestination=query[9]!=null ? (query[9].toString()) : ("0");;
        	String marketDate=query[11].toString();
        	Double marketRate=query[12]!=null ? Double.parseDouble(query[12].toString()) : Double.parseDouble("0");;
        	Double marketPrice=query[13]!=null ? Double.parseDouble(query[13].toString()) : Double.parseDouble("0");;
        	String security=query[14].toString();
        	Double stockQuantity=query[15]!=null ? Double.parseDouble(query[15].toString()) : Double.parseDouble("0");;
        	String state=query[16].toString();
        	String user=query[17].toString();
        	String securityTerm = "";
        	if(Validations.validateIsNotNullAndNotEmpty(query[18])){
        		securityTerm = query[18].toString(); 
        	}
        	
	        indrow++;
	        /** La data */
	        HSSFRow rowData = sheet.createRow((int)indrow);

	        addContentAndStyle(rowData.createCell(INDEX_OPERATION),getStyleAccountingAccount(), operation);
	        addContentAndStyle(rowData.createCell(INDEX_CUI),getStyleAccountingAccount(), cui);
	        addContentAndStyle(rowData.createCell(INDEX_TRANSFER_TYPE),getStyleAccountingAccount(), transferType);
	        addContentAndStyle(rowData.createCell(INDEX_REGISTER_DATE),getStyleAccountingAccount(), registerDate);
	        addContentAndStyle(rowData.createCell(INDEX_PARTICIPANT_ORIGIN),getStyleAccountingAccount(), participantOrigin);
	        addContentAndStyle(rowData.createCell(INDEX_ACCOUNT_NUMBER_ORIGIN),getStyleAccountingAccount(), accountNumberOrigin);
	        addContentAndStyle(rowData.createCell(INDEX_PARTICIPANT_DESTINATION),getStyleAccountingAccount(), participantDestination );
	        addContentAndStyle(rowData.createCell(INDEX_ACCOUNT_NUMBER_DESTINATION),getStyleAccountingAccount(), accountNumberDestination);
	        addContentAndStyle(rowData.createCell(INDEX_MARKET_DATE),getStyleAccountingAccount(), marketDate);
	        addContentAndStyle(rowData.createCell(INDEX_MARKET_RATE),getStyleAccountingAccount(), marketRate.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_MARKET_PRICE),getStyleAccountingAccount(), marketPrice.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_SECURITY),getStyleAccountingAccount(), security);
	        addContentAndStyle(rowData.createCell(INDEX_STOCK_QUANTITY),getStyleAccountingAccount(), stockQuantity.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_STATE),getStyleAccountingAccount(), state);
	        addContentAndStyle(rowData.createCell(INDEX_USER),getStyleAccountingAccount(), user );
	        addContentAndStyle(rowData.createCell(INDEX_SECURITY_TERM),getStyleAccountingAccount(), securityTerm );
		}
        /**adjustColumns**/
        //adjustColumns(sheet,15);
	}
	
	/**
	 * Anotacion en cuenta voluntaria por emisor / ExcelEntriesVoluntaryAccountPoiIssuer
	 * @param parameters
	 * @param reportLogger
	 * @return
	 */
	public byte[] writeFileExcelEntriesVoluntaryAccountIssuer(Map<String, Object> parameters , ReportLogger reportLogger  ){
		
		byte[] arrayByte=null;
		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("str_query");
		initCellStyle();
		initFont();
		initDataFormat();
		
		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						createSheetEntriesVoluntaryAccountIssuer(hSSFWorkbook,queryMain.subList(i, ifinal),queryMain.subList(i, ifinal).size());
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{//Solo un HSSFSheet
	        		createSheetEntriesVoluntaryAccountIssuer(hSSFWorkbook,queryMain,queryMain.size());
	        	}
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_ENTRIES_VOLUNTARY_ACCOUNT_ISSUER);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	       	hSSFWorkbook.write(bos);
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        hSSFWorkbook.write(fileOut);
	        fileOut.close();
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
		return arrayByte;
	}
	
	/**
	 * Traspaso de valores disponibles / ExcelEntriesVoluntaryAccountPoiIssuer
	 * @param hSSFWorkbook
	 * @param queryMain
	 */
	public void createSheetEntriesVoluntaryAccountIssuer(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, int number){
		
		int INDEX_PARTICIPANT=0;
		int INDEX_REQUEST_NUMBER=1;
		int INDEX_REQUEST_DATE=2;
		int INDEX_HOLDER_ACCOUNT=2;
		int INDEX_HOLDER=3;
		int INDEX_MOTIVE=4;
		int INDEX_SECURITY=5;
		int INDEX_TITLE_NUMBER=6;
		int INDEX_DATE=7;
		int INDEX_STATE=8;
		int INDEX_OBSERVATIONS=9;

		/**Created a new sheet within the workBock**/
    	HSSFSheet sheet = hSSFWorkbook.createSheet("EntriesVoluntarIssuer"+number);
    	
		/***** BEGIN - REPORT HEADER  */

        int indrow=1;

        /**Created Header DEBE - HABER*/
        HSSFRow rowHeader = sheet.createRow((int)indrow);
       
        addContentAndStyle(rowHeader.createCell(INDEX_PARTICIPANT),getStyleTitle(),ReportConstant.LABEL_PARTICIPANT);
        addContentAndStyle(rowHeader.createCell(INDEX_REQUEST_NUMBER),getStyleTitle(),ReportConstant.LABEL_REQUEST_NUMBER);
        addContentAndStyle(rowHeader.createCell(INDEX_REQUEST_DATE),getStyleTitle(),ReportConstant.LABEL_REQUEST_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_HOLDER_ACCOUNT),getStyleTitle(),ReportConstant.LABEL_HOLDER_ACCOUNT);
        addContentAndStyle(rowHeader.createCell(INDEX_HOLDER),getStyleTitle(),ReportConstant.LABEL_HOLDER);
        addContentAndStyle(rowHeader.createCell(INDEX_MOTIVE),getStyleTitle(),ReportConstant.LABEL_MOTIVE);
        addContentAndStyle(rowHeader.createCell(INDEX_SECURITY),getStyleTitle(),ReportConstant.LABEL_SECURITY_CLASS_CODE);
        addContentAndStyle(rowHeader.createCell(INDEX_TITLE_NUMBER),getStyleTitle(),ReportConstant.LABEL_TITLE_NUMBER);
        addContentAndStyle(rowHeader.createCell(INDEX_DATE),getStyleTitle(),ReportConstant.LABEL_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_STATE),getStyleTitle(),ReportConstant.LABEL_STATE);
        addContentAndStyle(rowHeader.createCell(INDEX_OBSERVATIONS),getStyleTitle(),ReportConstant.LABEL_OBSERVATIONS);
        
        for (Object[] query : queryMain) {
        	
        	String participant=query[0].toString();
        	String requestNumber=query[1].toString();
        	String requestDate=query[2].toString() ;
        	String holderAccount=query[4].toString();
        	String holder=query[5].toString();
        	String motive=query[6].toString();
        	String security=query[7].toString();
        	String titleNumber=query[8]!=null ? (query[8].toString()) : ("0");;
        	String date=query[10].toString() ;
        	String state=query[11].toString();
        	String observations=query[12]!=null ? (query[12].toString()) : ("0");;
        	
	        indrow++;
	        /** La data */
	        HSSFRow rowData = sheet.createRow((int)indrow);

	        addContentAndStyle(rowData.createCell(INDEX_PARTICIPANT),getStyleAccountingAccount(), participant);
	        addContentAndStyle(rowData.createCell(INDEX_REQUEST_NUMBER),getStyleAccountingAccount(), requestNumber);
	        addContentAndStyle(rowData.createCell(INDEX_REQUEST_DATE),getStyleAccountingAccount(), requestDate);
	        addContentAndStyle(rowData.createCell(INDEX_HOLDER_ACCOUNT),getStyleAccountingAccount(), holderAccount);
	        addContentAndStyle(rowData.createCell(INDEX_HOLDER),getStyleAccountingAccount(), holder);
	        addContentAndStyle(rowData.createCell(INDEX_MOTIVE),getStyleAccountingAccount(), motive);
	        addContentAndStyle(rowData.createCell(INDEX_SECURITY),getStyleAccountingAccount(), security );
	        addContentAndStyle(rowData.createCell(INDEX_TITLE_NUMBER),getStyleAccountingAccount(), titleNumber);
	        addContentAndStyle(rowData.createCell(INDEX_DATE),getStyleAccountingAccount(), date);
	        addContentAndStyle(rowData.createCell(INDEX_STATE),getStyleAccountingAccount(), state);
	        addContentAndStyle(rowData.createCell(INDEX_OBSERVATIONS),getStyleAccountingAccount(), observations);
		}
        /**adjustColumns**/
        //adjustColumns(sheet,15);
	}
	
	/**
	 * Trading Record BBV / Registro de operaciones BBV
	 * @param parameters
	 * @param reportLogger
	 * @return
	 */
	public byte[] writeFileExcelTradingRecordBBV(Map<String, Object> parameters , ReportLogger reportLogger  ){
		
		byte[] arrayByte=null;
		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("str_query");
		initCellStyle();
		initFont();
		initDataFormat();
		
		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						createSheetTradingRecordBBV(hSSFWorkbook,queryMain.subList(i, ifinal),queryMain.subList(i, ifinal).size());
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{//Solo un HSSFSheet
	        		createSheetTradingRecordBBV(hSSFWorkbook,queryMain,queryMain.size());
	        	}
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_TRADING_RECORD_BBV);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	       	hSSFWorkbook.write(bos);
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        hSSFWorkbook.write(fileOut);
	        fileOut.close();
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
		return arrayByte;
	}
	
	/**
	 * 
	 * @param hSSFWorkbook
	 * @param queryMain
	 */
	public void createSheetTradingRecordBBV(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, int number){
		
		int INDEX_MODALITY=0;
		int INDEX_BALLOT_NUMBER_SEQUENTIAL=1;
		int INDEX_BUY_PARTICIPANT=2;
		int INDEX_SELLER_PARTICIPANT=3;
		int INDEX_SECURITY_CODE=4;
		int INDEX_TERM_SETTLEMENT_DAYS=5;
		int INDEX_RATE=6;
		int INDEX_STOCK_QUANTITY=7;
		int INDEX_OPERATION_PRICE=8;
		int INDEX_CURRENCY=9;
		int INDEX_TOTAL=10;
		int INDEX_STATE=11;
		int INDEX_BALLOT_NUMBER_SEQUENTIAL_TR=12;
		int INDEX_SELLER_PARTICIPANT_TR=13;
		int INDEX_TERM_SETTLEMENT_DAYS_TR=14;
		int INDEX_RATE_TR=15;
		int INDEX_OPERATION_PRICE_TR=16;
		int INDEX_MARKET_DATE_TR=17;
		/**Created a new sheet within the workBock**/
    	HSSFSheet sheet = hSSFWorkbook.createSheet("TradingRecordBBV"+number);
		/***** BEGIN - REPORT HEADER  */
        int indrow=1;

        /**Created Header DEBE - HABER*/
        HSSFRow rowHeader = sheet.createRow((int)indrow);
       
        addContentAndStyle(rowHeader.createCell(INDEX_MODALITY),getStyleTitle(),ReportConstant.LABEL_MODALITY);
        addContentAndStyle(rowHeader.createCell(INDEX_BALLOT_NUMBER_SEQUENTIAL),getStyleTitle(),ReportConstant.LABEL_BALLOT_NUMBER+ReportConstant.LABEL_SEQUENTIAL);
        addContentAndStyle(rowHeader.createCell(INDEX_BUY_PARTICIPANT),getStyleTitle(),ReportConstant.LABEL_BUY_PARTICIPANT);
        addContentAndStyle(rowHeader.createCell(INDEX_SELLER_PARTICIPANT),getStyleTitle(),ReportConstant.LABEL_SELL_PARTICIPANT);
        addContentAndStyle(rowHeader.createCell(INDEX_SECURITY_CODE),getStyleTitle(),ReportConstant.LABEL_SECURITY_CLASS_CODE);
        addContentAndStyle(rowHeader.createCell(INDEX_TERM_SETTLEMENT_DAYS),getStyleTitle(),ReportConstant.LABEL_PLAZO);
        addContentAndStyle(rowHeader.createCell(INDEX_RATE),getStyleTitle(),ReportConstant.LABEL_RATE_PERCENT);
        addContentAndStyle(rowHeader.createCell(INDEX_STOCK_QUANTITY),getStyleTitle(),ReportConstant.LABEL_STOCK_QUANTITY);
        addContentAndStyle(rowHeader.createCell(INDEX_OPERATION_PRICE),getStyleTitle(),ReportConstant.LABEL_OPERATION_PRICE);
        addContentAndStyle(rowHeader.createCell(INDEX_CURRENCY),getStyleTitle(),ReportConstant.LABEL_CURRENCY);
        addContentAndStyle(rowHeader.createCell(INDEX_TOTAL),getStyleTitle(),ReportConstant.LABEL_TOTAL);
        addContentAndStyle(rowHeader.createCell(INDEX_STATE),getStyleTitle(),ReportConstant.LABEL_STATE);
        
        addContentAndStyle(rowHeader.createCell(INDEX_BALLOT_NUMBER_SEQUENTIAL_TR),getStyleTitle(),ReportConstant.LABEL_BALLOT_NUMBER+ReportConstant.LABEL_SEQUENTIAL+"TR");
        addContentAndStyle(rowHeader.createCell(INDEX_SELLER_PARTICIPANT_TR),getStyleTitle(),ReportConstant.LABEL_SELL_PARTICIPANT+"TR");
        addContentAndStyle(rowHeader.createCell(INDEX_TERM_SETTLEMENT_DAYS_TR),getStyleTitle(),ReportConstant.LABEL_PLAZO+"TR");
        addContentAndStyle(rowHeader.createCell(INDEX_RATE_TR),getStyleTitle(),ReportConstant.LABEL_RATE_PERCENT+"TR");
        addContentAndStyle(rowHeader.createCell(INDEX_OPERATION_PRICE_TR),getStyleTitle(),ReportConstant.LABEL_OPERATION_PRICE+"TR");
        addContentAndStyle(rowHeader.createCell(INDEX_MARKET_DATE_TR),getStyleTitle(),ReportConstant.LABEL_DATE+"TR");
        
        for (Object[] query : queryMain) {
        	
        	String modality=query[1].toString();
        	String ballotNumberSequential=query[2].toString();
        	String buyerParticipant=query[3].toString();
        	String sellerParticipant=query[4].toString();
        	String securityCode=query[5].toString();
        	Double termSettlementDays=query[6]!=null ? Double.parseDouble(query[6].toString()) : Double.parseDouble("0");;
        	BigDecimal rate =query[7]!=null ? new BigDecimal(query[7].toString()) : BigDecimal.ZERO;
        	Double stockQuantity=query[8]!=null ? Double.parseDouble(query[8].toString()) : Double.parseDouble("0");;
        	Double operationPrice=query[9]!=null ? Double.parseDouble(query[9].toString()) : Double.parseDouble("0");;
        	String currency=query[10].toString();
        	BigDecimal total =query[11]!=null ? new BigDecimal(query[11].toString()) : BigDecimal.ZERO;
        	String state=query[12].toString();
        	
        	String ballotNumberSequentialRt=query[13]!=null ? (query[13].toString()) : ("-");;
        	String sellerParticipantRt=query[14]!=null ? (query[14].toString()) : ("-");;
        	Double termSettlementDaysRt=query[15]!=null ? Double.parseDouble(query[15].toString()) : Double.parseDouble("0");;
        	BigDecimal rateRt =query[16]!=null ? new BigDecimal(query[16].toString()) : BigDecimal.ZERO;
        	Double operationPriceRt=query[17]!=null ? Double.parseDouble(query[17].toString()) : Double.parseDouble("0");;
        	String marketDateRt=query[18]!=null ? (CommonsUtilities.convertDatetoString((Date) query[18])) : ("0");;   

        	indrow++;
	        
	        /** La data */
	        HSSFRow rowData = sheet.createRow((int)indrow);

	        addContentAndStyle(rowData.createCell(INDEX_MODALITY),getStyleAccountingAccount(), modality);
	        addContentAndStyle(rowData.createCell(INDEX_BALLOT_NUMBER_SEQUENTIAL),getStyleAccountingAccount(), ballotNumberSequential);
	        addContentAndStyle(rowData.createCell(INDEX_BUY_PARTICIPANT),getStyleAccountingAccount(), buyerParticipant);
	        addContentAndStyle(rowData.createCell(INDEX_SELLER_PARTICIPANT),getStyleAccountingAccount(), sellerParticipant);
	        addContentAndStyle(rowData.createCell(INDEX_SECURITY_CODE),getStyleAccountingAccount(), securityCode);
	        addContentAndStyle(rowData.createCell(INDEX_TERM_SETTLEMENT_DAYS),getStyleAccountingAccount(), termSettlementDays);
	        addContentAndStyle(rowData.createCell(INDEX_RATE),getStyleAccountingAccount(), rate.doubleValue() );
	        addContentAndStyle(rowData.createCell(INDEX_STOCK_QUANTITY),getStyleAccountingAccount(), stockQuantity.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_OPERATION_PRICE),getStyleAccountingAccount(), operationPrice.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_CURRENCY),getStyleAccountingAccount(), currency);
	        addContentAndStyle(rowData.createCell(INDEX_TOTAL),getStyleAccountingAccount(), total.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_STATE),getStyleAccountingAccount(), state);
	        addContentAndStyle(rowData.createCell(INDEX_BALLOT_NUMBER_SEQUENTIAL_TR),getStyleAccountingAccount(), ballotNumberSequentialRt );
	        addContentAndStyle(rowData.createCell(INDEX_SELLER_PARTICIPANT_TR),getStyleAccountingAccount(), sellerParticipantRt );
	        addContentAndStyle(rowData.createCell(INDEX_TERM_SETTLEMENT_DAYS_TR),getStyleAccountingAccount(), termSettlementDaysRt );
	        addContentAndStyle(rowData.createCell(INDEX_RATE_TR),getStyleAccountingAccount(), rateRt.doubleValue() );
	        addContentAndStyle(rowData.createCell(INDEX_OPERATION_PRICE_TR),getStyleAccountingAccount(), operationPriceRt );
	        addContentAndStyle(rowData.createCell(INDEX_MARKET_DATE_TR),getStyleAccountingAccount(), marketDateRt );
		}
        /**adjustColumns**/
        //adjustColumns(sheet,15);
	}
	
	/**
	 * Bloqueos y Desbloqueos / BlockagesUnlock
	 * @param parameters
	 * @param reportLogger
	 * @return
	 */
	public byte[] writeFileExpirationSecuritiesByIssuer(Map<String, Object> parameters , ReportLogger reportLogger  ){
		
		byte[] arrayByte=null;
		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("str_query");
		initCellStyle();
		initFont();
		initDataFormat();
		
		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						createSheetExpirationSecuritiesByIssuer(hSSFWorkbook,queryMain.subList(i, ifinal),queryMain.subList(i, ifinal).size());
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{//Solo un HSSFSheet
	        		createSheetExpirationSecuritiesByIssuer(hSSFWorkbook,queryMain,queryMain.size());
	        	}
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_EXPIRATION_SECURITIES_BY_ISSUER);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	       	hSSFWorkbook.write(bos);
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        hSSFWorkbook.write(fileOut);
	        fileOut.close();
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
		return arrayByte;
	}
	
	/**
	 * 
	 * @param hSSFWorkbook
	 * @param queryMain
	 */
	public void createSheetExpirationSecuritiesByIssuer(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, int number){
		
		int INDEX_PARTICIPANT=0;
		int INDEX_ACCOUNT_NUMBER=1;
		int INDEX_HOLDER=2;
		int INDEX_BLOCK_OPERATION=3;
		int INDEX_REQ_BLOCK_OPERATION=4;
		int INDEX_UNLOCK_OPERATION=5;
		int INDEX_REQ_UNLOCK_OPERATION=6;
		int INDEX_BLOCK_DATE=7;
		int INDEX_UNLOCK_DATE=8;
		int INDEX_BLOCK_TYPE=9;
		int INDEX_SECURITY=10;
		int INDEX_STOCK_QUANTITY=11;
		int INDEX_STATE=12;
		int INDEX_USER=13;
	
		/**Created a new sheet within the workBock**/
    	HSSFSheet sheet = hSSFWorkbook.createSheet("ExpSecByIssuer"+number);
    	
		/***** BEGIN - REPORT HEADER  */

        int indrow=1;

        /**Created Header DEBE - HABER*/
        HSSFRow rowHeader = sheet.createRow((int)indrow);
       
        addContentAndStyle(rowHeader.createCell(INDEX_PARTICIPANT),getStyleTitle(),ReportConstant.LABEL_PARTICIPANT);
        addContentAndStyle(rowHeader.createCell(INDEX_ACCOUNT_NUMBER),getStyleTitle(),ReportConstant.LABEL_HOLDER_ACCOUNT);
        addContentAndStyle(rowHeader.createCell(INDEX_HOLDER),getStyleTitle(),ReportConstant.LABEL_HOLDER);
        addContentAndStyle(rowHeader.createCell(INDEX_BLOCK_OPERATION),getStyleTitle(),ReportConstant.LABEL_BLOCK_OPERATION);
        addContentAndStyle(rowHeader.createCell(INDEX_REQ_BLOCK_OPERATION),getStyleTitle(),ReportConstant.LABEL_REQ_BLOCK_NUMBER);
        addContentAndStyle(rowHeader.createCell(INDEX_UNLOCK_OPERATION),getStyleTitle(),ReportConstant.LABEL_UNLOCK_OPERATION);
        addContentAndStyle(rowHeader.createCell(INDEX_REQ_UNLOCK_OPERATION),getStyleTitle(),ReportConstant.LABEL_REQ_UNLOCK_NUMBER);
        addContentAndStyle(rowHeader.createCell(INDEX_BLOCK_DATE),getStyleTitle(),ReportConstant.LABEL_DATE_BLOCK);
        addContentAndStyle(rowHeader.createCell(INDEX_UNLOCK_DATE),getStyleTitle(),ReportConstant.LABEL_DATE_UNLOCK);
        addContentAndStyle(rowHeader.createCell(INDEX_BLOCK_TYPE),getStyleTitle(),ReportConstant.LABEL_BLOCK_TYPE);
        addContentAndStyle(rowHeader.createCell(INDEX_SECURITY),getStyleTitle(),ReportConstant.LABEL_SECURITY_CLASS_CODE);
        addContentAndStyle(rowHeader.createCell(INDEX_STOCK_QUANTITY),getStyleTitle(),ReportConstant.LABEL_STOCK_QUANTITY);
        addContentAndStyle(rowHeader.createCell(INDEX_STATE),getStyleTitle(),ReportConstant.LABEL_STATE);
        addContentAndStyle(rowHeader.createCell(INDEX_USER),getStyleTitle(),ReportConstant.LABEL_USUARIO);
        
        for (Object[] query : queryMain) {
        	
        	String participant=query[13].toString();
        	String accountNumber=query[14].toString();
        	String holder=query[15].toString();
        	String blockOperation=query[0].toString();
        	String reqBlockOperation=query[1].toString();
        	String unlockOperation=query[2]!=null ? (query[2].toString()) : ("0");;
        	String reqUnlockOperation=query[4]!=null ? (query[3].toString()) : ("0");;
        	String blockDate=query[5].toString();
        	String unlockDate=query[6]!=null ? (query[5].toString()) : ("0");;
        	String blockType=query[7].toString();
        	String security=query[8].toString();
        	Double stockQuantity=query[10]!=null ? Double.parseDouble(query[10].toString()) : Double.parseDouble("0");;
        	String user=query[11].toString();
        	String blockState=query[12].toString();
        	
	        indrow++;
	        /** La data */
	        HSSFRow rowData = sheet.createRow((int)indrow);

	        addContentAndStyle(rowData.createCell(INDEX_PARTICIPANT),getStyleAccountingAccount(), participant);
	        addContentAndStyle(rowData.createCell(INDEX_ACCOUNT_NUMBER),getStyleAccountingAccount(), accountNumber);
	        addContentAndStyle(rowData.createCell(INDEX_HOLDER),getStyleAccountingAccount(), holder);
	        addContentAndStyle(rowData.createCell(INDEX_BLOCK_OPERATION),getStyleAccountingAccount(), blockOperation);
	        addContentAndStyle(rowData.createCell(INDEX_REQ_BLOCK_OPERATION),getStyleAccountingAccount(), reqBlockOperation);
	        addContentAndStyle(rowData.createCell(INDEX_UNLOCK_OPERATION),getStyleAccountingAccount(), unlockOperation);
	        addContentAndStyle(rowData.createCell(INDEX_REQ_UNLOCK_OPERATION),getStyleAccountingAccount(), reqUnlockOperation );
	        addContentAndStyle(rowData.createCell(INDEX_BLOCK_DATE),getStyleAccountingAccount(), blockDate);
	        addContentAndStyle(rowData.createCell(INDEX_UNLOCK_DATE),getStyleAccountingAccount(), unlockDate);
	        addContentAndStyle(rowData.createCell(INDEX_BLOCK_TYPE),getStyleAccountingAccount(), blockType);
	        addContentAndStyle(rowData.createCell(INDEX_SECURITY),getStyleAccountingAccount(), security);
	        addContentAndStyle(rowData.createCell(INDEX_STOCK_QUANTITY),getStyleAccountingAccount(), stockQuantity.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_STATE),getStyleAccountingAccount(), blockState );
	        addContentAndStyle(rowData.createCell(INDEX_USER),getStyleAccountingAccount(), user );
		}
        /**adjustColumns**/
        //adjustColumns(sheet,15);
	}
	
	
	/**
	 * Bloqueos y Desbloqueos / BlockagesUnlock
	 * @param parameters
	 * @param reportLogger
	 * @return
	 */
	public byte[] writeFileExcelDetailsOfTitlesPendingExpiration(Map<String, Object> parameters , ReportLogger reportLogger  ){
		
		byte[] arrayByte=null;
		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("str_Query");
		initCellStyle();
		initFont();
		initDataFormat();
		
		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						createSheetDetailsOfTitlesPendingExpiration(hSSFWorkbook,queryMain.subList(i, ifinal),queryMain.subList(i, ifinal).size());
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{//Solo un HSSFSheet
	        		createSheetDetailsOfTitlesPendingExpiration(hSSFWorkbook,queryMain,queryMain.size());
	        	}
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_DETAILS_TITLES_PENDING_EXP);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	       	hSSFWorkbook.write(bos);
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        hSSFWorkbook.write(fileOut);
	        fileOut.close();
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
		return arrayByte;
	}
	
	/**
	 * 
	 * @param hSSFWorkbook
	 * @param queryMain
	 */
	public void createSheetDetailsOfTitlesPendingExpiration(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, int number){
		
		int INDEX_SECURITY_CLASS=0;
		int INDEX_CURRENCY=1;
		int INDEX_DATE=2;
		int INDEX_PARTICIPANT=3;
		int INDEX_ACCOUNT_NUMBER=4;
		int INDEX_HOLDER=5;
		int INDEX_HOLDER_DESCRIPTION=6;
		int INDEX_SECURITY=7;
		int INDEX_COUPON=8;
		int INDEX_STOCK_QUANTITY=9;
		int INDEX_STOCK_AVAILABLE=10;
		int INDEX_STOCK_PAWN=11;
		int INDEX_STOCK_BAN=12;
		int INDEX_STOCK_CAT=13;
		int INDEX_INTEREST_AMOUNT=14;
		int INDEX_AMORTIZATION_AMOUNT=15;
		int INDEX_TOTAL=16;
	
		/**Created a new sheet within the workBock**/
    	HSSFSheet sheet = hSSFWorkbook.createSheet("DetailsTitleExp"+number);
    	
		/***** BEGIN - REPORT HEADER  */

        int indrow=1;

        /**Created Header DEBE - HABER*/
        HSSFRow rowHeader = sheet.createRow((int)indrow);
       
        addContentAndStyle(rowHeader.createCell(INDEX_SECURITY_CLASS),getStyleTitle(),ReportConstant.LABEL_SECURITY_CLASS);
        addContentAndStyle(rowHeader.createCell(INDEX_CURRENCY),getStyleTitle(),ReportConstant.LABEL_CURRENCY);
        addContentAndStyle(rowHeader.createCell(INDEX_DATE),getStyleTitle(),ReportConstant.LABEL_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_PARTICIPANT),getStyleTitle(),ReportConstant.LABEL_PARTICIPANT);
        addContentAndStyle(rowHeader.createCell(INDEX_ACCOUNT_NUMBER),getStyleTitle(),ReportConstant.LABEL_HOLDER_ACCOUNT);
        addContentAndStyle(rowHeader.createCell(INDEX_HOLDER),getStyleTitle(),ReportConstant.LABEL_CUI);
        addContentAndStyle(rowHeader.createCell(INDEX_HOLDER_DESCRIPTION),getStyleTitle(),ReportConstant.LABEL_HOLDER);
        addContentAndStyle(rowHeader.createCell(INDEX_SECURITY),getStyleTitle(),ReportConstant.LABEL_SECURITY_CLASS_CODE);
        addContentAndStyle(rowHeader.createCell(INDEX_COUPON),getStyleTitle(),ReportConstant.LABEL_COUPON);
        addContentAndStyle(rowHeader.createCell(INDEX_STOCK_QUANTITY),getStyleTitle(),ReportConstant.LABEL_STOCK_QUANTITY);
        addContentAndStyle(rowHeader.createCell(INDEX_STOCK_AVAILABLE),getStyleTitle(),ReportConstant.LABEL_AVAILABLE);
        addContentAndStyle(rowHeader.createCell(INDEX_STOCK_PAWN),getStyleTitle(),ReportConstant.LABEL_PAWN);
        addContentAndStyle(rowHeader.createCell(INDEX_STOCK_BAN),getStyleTitle(),ReportConstant.LABEL_BAN);
        addContentAndStyle(rowHeader.createCell(INDEX_STOCK_CAT),getStyleTitle(),ReportConstant.LABEL_CAT);
        addContentAndStyle(rowHeader.createCell(INDEX_INTEREST_AMOUNT),getStyleTitle(),ReportConstant.LABEL_INTEREST_AMOUNT);
        addContentAndStyle(rowHeader.createCell(INDEX_AMORTIZATION_AMOUNT),getStyleTitle(),ReportConstant.LABEL_AMORTIZATION_AMOUNT);
        addContentAndStyle(rowHeader.createCell(INDEX_TOTAL),getStyleTitle(),ReportConstant.LABEL_TOTAL);
        
        for (Object[] query : queryMain) {
        	
        	String securityClass=query[0].toString();
        	String currency=query[1].toString();
        	String date=query[2]!=null ? query[2].toString() : ("0");;  //issue 483
        	String participant=query[3].toString();
        	String holderAccount=query[4].toString();
        	String holder=query[5].toString();
        	String holderDescription=query[6].toString();
        	String security=query[7].toString();
        	String coupon=query[8].toString();
        	Double stockQuantity=query[9]!=null ? Double.parseDouble(query[9].toString()) : Double.parseDouble("0");;
        	Double stockAvailable=query[10]!=null ? Double.parseDouble(query[10].toString()) : Double.parseDouble("0");;
        	Double stockPawn=query[11]!=null ? Double.parseDouble(query[11].toString()) : Double.parseDouble("0");;
        	Double stockBan=query[12]!=null ? Double.parseDouble(query[12].toString()) : Double.parseDouble("0");;
        	Double stockAcreditation=query[13]!=null ? Double.parseDouble(query[13].toString()) : Double.parseDouble("0");;
        	Double interestAmount=query[14]!=null ? Double.parseDouble(query[14].toString()) : Double.parseDouble("0");;
        	Double amortizationAmount=query[15]!=null ? Double.parseDouble(query[15].toString()) : Double.parseDouble("0");;
        	Double total=query[16]!=null ? Double.parseDouble(query[16].toString()) : Double.parseDouble("0");;
        	
	        indrow++;
	        /** La data */
	        HSSFRow rowData = sheet.createRow((int)indrow);

	        addContentAndStyle(rowData.createCell(INDEX_SECURITY_CLASS),getStyleAccountingAccount(), securityClass);
	        addContentAndStyle(rowData.createCell(INDEX_CURRENCY),getStyleAccountingAccount(), currency);
	        addContentAndStyle(rowData.createCell(INDEX_DATE),getStyleAccountingAccount(), date);
	        addContentAndStyle(rowData.createCell(INDEX_PARTICIPANT),getStyleAccountingAccount(), participant);
	        addContentAndStyle(rowData.createCell(INDEX_ACCOUNT_NUMBER),getStyleAccountingAccount(), holderAccount);
	        addContentAndStyle(rowData.createCell(INDEX_HOLDER),getStyleAccountingAccount(), holder);
	        addContentAndStyle(rowData.createCell(INDEX_HOLDER_DESCRIPTION),getStyleAccountingAccount(), holderDescription );
	        addContentAndStyle(rowData.createCell(INDEX_SECURITY),getStyleAccountingAccount(), security);
	        addContentAndStyle(rowData.createCell(INDEX_COUPON),getStyleAccountingAccount(), coupon);
	        addContentAndStyle(rowData.createCell(INDEX_STOCK_QUANTITY),getStyleAccountingAccount(), stockQuantity.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_STOCK_AVAILABLE),getStyleAccountingAccount(), stockAvailable.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_STOCK_PAWN),getStyleAccountingAccount(), stockPawn.doubleValue());
	        addContentAndStyle(rowData.createCell(INDEX_STOCK_BAN),getStyleAccountingAccount(), stockBan.doubleValue() );
	        addContentAndStyle(rowData.createCell(INDEX_STOCK_CAT),getStyleAccountingAccount(), stockAcreditation.doubleValue() );
	        addContentAndStyle(rowData.createCell(INDEX_INTEREST_AMOUNT),getStyleAccountingAccount(), interestAmount.doubleValue() );
	        addContentAndStyle(rowData.createCell(INDEX_AMORTIZATION_AMOUNT),getStyleAccountingAccount(), amortizationAmount.doubleValue() );
	        addContentAndStyle(rowData.createCell(INDEX_TOTAL),getStyleAccountingAccount(), total.doubleValue() );
		}
        /**adjustColumns**/
        //adjustColumns(sheet,15);
	}
public byte[] writeFileExcelClientPortfolioDpfSirtex(Map<String, Object> parameters , ReportLogger reportLogger  ){
		
		byte[] arrayByte=null;

		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("str_query");

		initCellStyle();
		initFont();
		initDataFormat();
		
		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						
						
						createSheetPortFolioSirtex(hSSFWorkbook,queryMain.subList(i, ifinal),CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),"HHmmssSSSSS"));
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{
	        		//Solo un HSSFSheet
	        		createSheetPortFolioSirtex(hSSFWorkbook,queryMain,CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),"HHmmssSSSSS"));
	        	}
			}else{
				//Solo un HSSFSheet
        		createSheetPortFolioSirtex(hSSFWorkbook,queryMain,CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),"HHmmssSSSSS"));
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_CLIENT_PORTFOLIO);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	         
	       	hSSFWorkbook.write(bos);
	        
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        
	        hSSFWorkbook.write(fileOut);
	        
	        fileOut.close();
			
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
		return arrayByte;
	}
/**
 * 
 * @param hSSFWorkbook
 * @param queryMain
 */
public void createSheetPortFolioSirtex(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, String number){
	
	
	int INDEX_PARTICIPANT=0;
	int INDEX_HOLDER_ACCOUNT=1;
	int INDEX_CUI=2;
	int INDEX_SECURITY_CLASS=3;
	int INDEX_SECURITY_CODE=4;
	int INDEX_ISSUER_MNEMONIC=5;
	int INDEX_CURRENCY_MNEMONIC=6;
	int INDEX_ISSUER_DATE=7;
	int INDEX_PLAZO=8;
	final int INDEX_DUE_DATE=9;	     // fecha de vencimiento	
	int INDEX_RATE_NOMINAL=10;
	int INDEX_FINAL_RATE=11;
	int INDEX_TOTAL_BALANCE=12;
	int INDEX_AVAILABLE_BALANCE=13;
	int INDEX_REPORTED_BALANCE=14;
	int INDEX_MARKFACT=15;
	int INDEX_RATE=16;
	int INDEX_TOTAL_AMOUNT=17;
	int INDEX_VN_CURRENT=18;
	int INDEX_TOTAL=19;
	final int INDEX_ALTERNATE_CODE=20;	 // codigo alterno		
	final int INDEX_NAME=21;			 // nombre
	final int INDEX_TYPE_OF_PERSON=22;   // tipo de persona
	final int INDEX_CI_NIT=23;			 // nit 0 nit
	final int INDEX_SERIE=24;			 // serie
	//final int INDEX_PRIMARY_SECONDARY=25;// primario o secundario	
	final int INDEX_FIRST_HOLDER = 25;
	final int INDEX_COUPON = 26;
	
	/**Created a new sheet within the workBock**/
	HSSFSheet sheet = hSSFWorkbook.createSheet("ClientsPortFolio"+number);
	
	/*****
     * BEGIN - REPORT HEADER 
     */
    
	/**
	 * 1corte : Participante
	 * 2corte : Oferta
	 * 3corte : Moneda-Clase valor
	 */
    int indrow=1;


    /**Created Header DEBE - HABER*/
    HSSFRow rowHeader = sheet.createRow((int)indrow);
    addContentAndStyle(rowHeader.createCell(INDEX_PARTICIPANT),getStyleTitle(),ReportConstant.LABEL_PARTICIPANT);
    addContentAndStyle(rowHeader.createCell(INDEX_HOLDER_ACCOUNT),getStyleTitle(),ReportConstant.LABEL_HOLDER_ACCOUNT);
    addContentAndStyle(rowHeader.createCell(INDEX_HOLDER_ACCOUNT),getStyleTitle(),ReportConstant.LABEL_HOLDER_ACCOUNT);
    addContentAndStyle(rowHeader.createCell(INDEX_CUI),getStyleTitle(),ReportConstant.LABEL_CUI);
    addContentAndStyle(rowHeader.createCell(INDEX_SECURITY_CLASS),getStyleTitle(),ReportConstant.LABEL_SECURITY_CLASS);
    addContentAndStyle(rowHeader.createCell(INDEX_SECURITY_CODE),getStyleTitle(),ReportConstant.LABEL_SECURITY_CODE);
    addContentAndStyle(rowHeader.createCell(INDEX_ISSUER_MNEMONIC),getStyleTitle(),ReportConstant.LABEL_ISSUER);
    addContentAndStyle(rowHeader.createCell(INDEX_CURRENCY_MNEMONIC),getStyleTitle(),ReportConstant.LABEL_CURRENCY_MNEMONIC);
    addContentAndStyle(rowHeader.createCell(INDEX_ISSUER_DATE),getStyleTitle(),ReportConstant.LABEL_ISSUANCE_DATE);
    addContentAndStyle(rowHeader.createCell(INDEX_PLAZO),getStyleTitle(),ReportConstant.LABEL_PLAZO);
    addContentAndStyle(rowHeader.createCell(INDEX_DUE_DATE),getStyleTitle(),ReportConstant.LABEL_DUE_DATE);
    addContentAndStyle(rowHeader.createCell(INDEX_RATE_NOMINAL),getStyleTitle(),ReportConstant.LABEL_VN);
    addContentAndStyle(rowHeader.createCell(INDEX_FINAL_RATE),getStyleTitle(),ReportConstant.LABEL_NOMINAL_RATE);
    addContentAndStyle(rowHeader.createCell(INDEX_TOTAL_BALANCE),getStyleTitle(),ReportConstant.LABEL_TOTAL_BALANCE);
    addContentAndStyle(rowHeader.createCell(INDEX_AVAILABLE_BALANCE),getStyleTitle(),ReportConstant.LABEL_AVAILABLE_BALANCE);
    addContentAndStyle(rowHeader.createCell(INDEX_REPORTED_BALANCE),getStyleTitle(),ReportConstant.LABEL_REPORTED_BALANCE);
    addContentAndStyle(rowHeader.createCell(INDEX_MARKFACT),getStyleTitle(),ReportConstant.LABEL_MARKFACT);
    addContentAndStyle(rowHeader.createCell(INDEX_RATE),getStyleTitle(),ReportConstant.LABEL_RATE_PERCENT);
    addContentAndStyle(rowHeader.createCell(INDEX_TOTAL_AMOUNT),getStyleTitle(),ReportConstant.LABEL_TOTAL_AMOUNT);
    addContentAndStyle(rowHeader.createCell(INDEX_VN_CURRENT),getStyleTitle(),ReportConstant.LABEL_CURRENT_NV);
    addContentAndStyle(rowHeader.createCell(INDEX_TOTAL),getStyleTitle(),ReportConstant.LABEL_TOTAL_CURRENT_NV);
    //1. COD_ALTERNO,2. NOMBRE, 3. FECHA DE VENCIMIENTO,4. TIPO_PERSONA,5. CI_NIT,6. SERIE,7. PRIMARIO/SECUNDARIO			
    addContentAndStyle(rowHeader.createCell(INDEX_ALTERNATE_CODE),getStyleTitle(),ReportConstant.LABEL_ALTERNATE_CODE);
    addContentAndStyle(rowHeader.createCell(INDEX_NAME),getStyleTitle(),ReportConstant.LABEL_NAME);
    addContentAndStyle(rowHeader.createCell(INDEX_TYPE_OF_PERSON),getStyleTitle(),ReportConstant.LABEL_TYPE_OF_PERSON);
    addContentAndStyle(rowHeader.createCell(INDEX_CI_NIT),getStyleTitle(),ReportConstant.LABEL_CI_NIT);
    addContentAndStyle(rowHeader.createCell(INDEX_SERIE),getStyleTitle(),ReportConstant.LABEL_SERIE);
    //addContentAndStyle(rowHeader.createCell(INDEX_PRIMARY_SECONDARY),getStyleTitle(),ReportConstant.LABEL_PRIMARY_SECONDARY);

    boolean sw = true;
    for (Object[] query : queryMain) {
    	
    	
    	String participant=query[2]!=null?String.valueOf(query[2]):"";
    	String holderAccount=query[10]!=null?String.valueOf(query[10]):"";
    	String cui= query[8]!=null?String.valueOf(query[8]):"";
    	String securityClass= query[12]!=null?String.valueOf(query[12]):"";
    	String securityCode=query[13]!=null?String.valueOf(query[13]):"";
    	String issuerMnemonic=query[5]!=null?String.valueOf(query[5]):"";
    	String currencyMnemonic=query[4]!=null?String.valueOf(query[4]):"";
    	String issuanceDate=query[14]!=null?String.valueOf(query[14]):"";
    	Double plazo=query[15]!=null ? Double.parseDouble(query[15].toString()) : Double.parseDouble("0");;
    	String expirationDate=query[43]!=null?String.valueOf(query[43]):"";
    	BigDecimal rateNominal=query[16]!=null ? new BigDecimal(query[16].toString()) : BigDecimal.ZERO;
    	BigDecimal finalRate=query[17]!=null ? new BigDecimal(query[17].toString()) : BigDecimal.ZERO;
    	Double totalBalance=query[22]!=null ? Double.parseDouble(query[22].toString()) : Double.parseDouble("0");;
    	Double availableBalance=query[23]!=null ? Double.parseDouble(query[23].toString()) : Double.parseDouble("0");;
    	Double reportedBalance = query[29]!=null ? Double.parseDouble(query[29].toString()) : Double.parseDouble("0");;
    	BigDecimal markFact =query[35]!=null ? new BigDecimal(query[35].toString()) : BigDecimal.ZERO;
    	BigDecimal rate =query[34]!=null ? new BigDecimal(query[34].toString()) : BigDecimal.ZERO;
    	BigDecimal totalAmount = query[36]!=null ? new BigDecimal(query[36].toString()) : BigDecimal.ZERO;
    	BigDecimal currentNominalValue = query[40]!=null ? new BigDecimal(query[40].toString()) : BigDecimal.ZERO;
    	BigDecimal totalToCurrentNominalValue = query[41]!=null ? new BigDecimal(query[41].toString()) : BigDecimal.ZERO;
    	//
    	String alternateCode=query[42]!=null?String.valueOf(query[42]):"";
    	String holders= query[9]!=null?String.valueOf(query[9]):"";
    	String holderType=query[44]!=null?String.valueOf(query[44]):"";
    	String nitCi=query[45]!=null?String.valueOf(query[45]):"";
    	String serie=query[46]!=null?String.valueOf(query[46]):"";
    	
    	String firtHolder = null;
    	String indCoupon = null;
    	
    	try {
    		if(query[47]!=null)
    			firtHolder = query[47]!=null?String.valueOf(query[47]):"";
        	if(query[47]!=null)	
        		indCoupon =query[48]!=null?String.valueOf(query[48]):"";
    		if(sw){
				addContentAndStyle(rowHeader.createCell(INDEX_FIRST_HOLDER),getStyleTitle(),ReportConstant.LABEL_FIRST_HOLDER);
        		addContentAndStyle(rowHeader.createCell(INDEX_COUPON),getStyleTitle(),ReportConstant.WITH_COUPONS);
				sw=false;
			}
		} catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException) {
			
		}
    	indrow++;
    	
        /**
         * La data 
         */
        HSSFRow rowData = sheet.createRow((int)indrow);
        addContentAndStyle(rowData.createCell(INDEX_PARTICIPANT),getStyleAccountingAccount(), participant);
        addContentAndStyle(rowData.createCell(INDEX_HOLDER_ACCOUNT),getStyleAccountingAccount(), holderAccount);
        addContentAndStyle(rowData.createCell(INDEX_CUI),getStyleAccountingAccount(), cui);
        addContentAndStyle(rowData.createCell(INDEX_SECURITY_CLASS),getStyleAccountingAccount(), securityClass);
        addContentAndStyle(rowData.createCell(INDEX_SECURITY_CODE),getStyleAccountingAccount(), securityCode);
        addContentAndStyle(rowData.createCell(INDEX_ISSUER_MNEMONIC),getStyleAccountingAccount(), issuerMnemonic);
        addContentAndStyle(rowData.createCell(INDEX_CURRENCY_MNEMONIC),getStyleAccountingAccount(), currencyMnemonic);
        addContentAndStyle(rowData.createCell(INDEX_ISSUER_DATE),getStyleAccountingAccount(), issuanceDate);
        addContentAndStyle(rowData.createCell(INDEX_PLAZO),getStyleAccountingAccount(), plazo);
        addContentAndStyle(rowData.createCell(INDEX_DUE_DATE),getStyleAccountingAccount(), expirationDate);
        addContentAndStyle(rowData.createCell(INDEX_RATE_NOMINAL),getStyleAccountingAccount(), rateNominal.doubleValue());//CommonsUtilities.getFormatOfAmount(rateNominal, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
        addContentAndStyle(rowData.createCell(INDEX_FINAL_RATE),getStyleAccountingAccount(), finalRate.doubleValue());
        addContentAndStyle(rowData.createCell(INDEX_TOTAL_BALANCE),getStyleAccountingAccount(), totalBalance );
        addContentAndStyle(rowData.createCell(INDEX_AVAILABLE_BALANCE),getStyleAccountingAccount(), availableBalance);
        addContentAndStyle(rowData.createCell(INDEX_REPORTED_BALANCE),getStyleAccountingAccount(), reportedBalance);
        addContentAndStyle(rowData.createCell(INDEX_MARKFACT),getStyleAccountingAccount(), markFact.doubleValue());
        addContentAndStyle(rowData.createCell(INDEX_RATE),getStyleAccountingAccount(), rate.doubleValue());
        addContentAndStyle(rowData.createCell(INDEX_TOTAL_AMOUNT),getStyleAccountingAccount(), totalAmount.doubleValue());
        addContentAndStyle(rowData.createCell(INDEX_VN_CURRENT),getStyleAccountingAccount(), currentNominalValue.doubleValue());
        addContentAndStyle(rowData.createCell(INDEX_TOTAL),getStyleAccountingAccount(), totalToCurrentNominalValue.doubleValue());
        
        //
        addContentAndStyle(rowData.createCell(INDEX_ALTERNATE_CODE),getStyleAccountingAccount(), alternateCode);
        addContentAndStyle(rowData.createCell(INDEX_NAME),getStyleAccountingAccount(), holders);
        addContentAndStyle(rowData.createCell(INDEX_TYPE_OF_PERSON),getStyleAccountingAccount(), holderType);
        addContentAndStyle(rowData.createCell(INDEX_CI_NIT),getStyleAccountingAccount(), nitCi);
        addContentAndStyle(rowData.createCell(INDEX_SERIE),getStyleAccountingAccount(), serie);
        if(firtHolder!=null)
        	addContentAndStyle(rowData.createCell(INDEX_FIRST_HOLDER),getStyleAccountingAccount(), firtHolder);
        if(indCoupon!=null)
        	addContentAndStyle(rowData.createCell(INDEX_COUPON),getStyleAccountingAccount(), indCoupon);
	}
    
    /**adjustColumns**/
    //adjustColumns(sheet,15);
}
	
public byte[] writeFileExcelClientSirtex(List<XmlOperationsSirtex> operationsSirtexs, ReportLogger reportLogger  ) throws IOException{
	
	byte[] arrayByte=null;
	
	initCellStyle();
	initFont();
	initDataFormat();
	HSSFSheet sheet = hSSFWorkbook.createSheet("Reporte BCB excel");
	HSSFCellStyle style=hSSFWorkbook.createCellStyle();
	
	HSSFDataFormat format=hSSFWorkbook.createDataFormat();
	style.setDataFormat(format.getFormat("###,###,###,##0.00"));
	
	Object[][] bookData = new Object[10000][14];
	bookData[0][0]="PARTICIPANTE";
	bookData[0][1]="CLIENTE";
	bookData[0][2]="SERIE_F_EDV";
	bookData[0][3]="SERIE_F_BCB";
	bookData[0][4]="EMISOR";	
	bookData[0][5]="MONEDA";
	bookData[0][6]="F_EMISION";
	bookData[0][7]="F_VENC";
	bookData[0][8]="PLAZO_RESTANTE";
	bookData[0][9]="FORMA_PAGO";
	bookData[0][10]="VAL_INI";
	bookData[0][11]="VAL_FINAL";
	bookData[0][12]="CANTIDAD_TITULOS";
	bookData[0][13]="TASA_RENDIMIENTO";
	
	XmlOperationsSirtex operationsSirtex = operationsSirtexs.get(0);
	List<XmlRegSirtex> regSirtexs = operationsSirtex.getRegSirtexs();
	List<XmlSeccSirtex> listSirtex = regSirtexs.get(0).getSeccSirtexs();
	
	int rowCount = 1;//fila
	for (XmlSeccSirtex xmlSeccSirtex : listSirtex) {
		Security security = null;
		ParameterTable parameterTable = null;
		for (int i = 0; i < 14; i++) {
			switch (i) {
				/*AGENCIA DE BOLSA*/
				case 0:
					bookData[rowCount][i] = xmlSeccSirtex.getExchangeAgency();
				break;
				/*CLIENT*/
				case 1:
					bookData[rowCount][i] = xmlSeccSirtex.getClient();
				break;
				/*SERIE_F_EDV*/
				case 2:
					bookData[rowCount][i] = xmlSeccSirtex.getSecurityKey();
					security = accountingReportServiceBean.find(Security.class, xmlSeccSirtex.getSecurityKey());
				break;
				/*SERIE F BCB*/
				case 3:
					bookData[rowCount][i] = xmlSeccSirtex.getSerieF();
					break;
				/*EMISOR*/
				case 4:
					bookData[rowCount][i] = security.getIssuer().getMnemonic();
					break;
				/*MONEDA*/
				case 5:
					parameterTable = accountingReportServiceBean.find(ParameterTable.class, security.getCurrency());
					bookData[rowCount][i] = parameterTable.getIndicator3();
					break;
				/*F_EMISION*/
				case 6:
					bookData[rowCount][i] = xmlSeccSirtex.getIssuanceDate();
					break;
				/*F_VENC*/
				case 7:
					bookData[rowCount][i] = xmlSeccSirtex.getExpirationDate();
					break;
				/*PLAZO_RESTANTE*/
				case 8:
					bookData[rowCount][i] = xmlSeccSirtex.getTerm();
					break;
				/*FORMA_PAGO*/
				case 9:
					if(Validations.validateIsNotNullAndNotEmpty(security.getInterestPaymentModality())){
						parameterTable = accountingReportServiceBean.find(ParameterTable.class, security.getInterestPaymentModality());
						bookData[rowCount][i] = parameterTable.getDescription();
					}else{
						bookData[rowCount][i] = "";
					}
					break;
				/*VAL_INI*/
				case 10:					
					bookData[rowCount][i] = new Double(security.getInitialNominalValue().doubleValue());
					break;
				/*VAL_FINAL*/
				case 11:
					BigDecimal valFinal = new BigDecimal(xmlSeccSirtex.getFinalNominalValue());
					bookData[rowCount][i] = valFinal.doubleValue();
					break;
				/*CANTIDAD_TITULOS*/
				case 12:
					bookData[rowCount][i] = xmlSeccSirtex.getStockQuantity().toString();
					break;
				case 13:
					bookData[rowCount][i] = security.getInterestRate().doubleValue();
					break;
			default:
				break;
			}
		}
		rowCount++;
	}
	rowCount = 0;
	for (Object[] aBook : bookData) {
		Row row = sheet.createRow(++rowCount);
		
		int columnCount = 0;
		
		for (Object field : aBook) {
			Cell cell = row.createCell(++columnCount);
			if (field instanceof String) {
				cell.setCellValue((String) field);
			} else if (field instanceof Double) {
				cell.setCellValue((Double) field);
			}
			cell.setCellStyle(style);
		}
		
	}
    FileOutputStream fileOut = new FileOutputStream("ReporteH.xls");
	
    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    bos.writeTo(fileOut);
   	hSSFWorkbook.write(bos);
   	bos.close();
    arrayByte = bos.toByteArray();
    hSSFWorkbook.write(fileOut);
    fileOut.close();
   return arrayByte;
}
	public byte[] writeFileExcelRequestCollection(List<Object[]>list) throws IOException{
		
		byte[] arrayByte=null;
		
		initCellStyle();
		initFont();
		initDataFormat();
		HSSFSheet sheet = hSSFWorkbook.createSheet("Reporte BCB excel");
		
		
		/**Formato numerico*/
		HSSFCellStyle style=hSSFWorkbook.createCellStyle();
		HSSFDataFormat format=hSSFWorkbook.createDataFormat();
		style.setDataFormat(format.getFormat("###,###,###,##0.00"));
		
		HSSFCellStyle styleBorder=hSSFWorkbook.createCellStyle(); style.setBorderBottom(HSSFCellStyle.BORDER_THIN); style.setBorderTop(HSSFCellStyle.BORDER_THIN); style.setBorderRight(HSSFCellStyle.BORDER_THIN); style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		
		Object[][] bookData = new Object[10000][18];
		bookData[0][0]="Participante";
		bookData[0][1]="Nro Solicitud";
		bookData[0][2]="Fecha de Vencimiento";	
		bookData[0][3]="Fecha de Cobro";
		bookData[0][4]="CUI";
		bookData[0][5]="Moneda Origen";
		bookData[0][6]="Código Valor";
		bookData[0][7]="Cantidad de Valores";
		bookData[0][8]="Nro de Cupon";
		bookData[0][9]="Interes";
		bookData[0][10]="Amortizacion";
		bookData[0][11]="Monto al Vencimiento";
		bookData[0][12]="Moneda de Cobro";
		bookData[0][13]="Monto a Cobrar";
		bookData[0][14]="Banco";
		bookData[0][15]="Moneda Cuenta";
		bookData[0][16]="Nro Cuenta";
		bookData[0][17]="Estado solicitud";
		
		
		int rowCount = 1;//fila
		for (Object[] objects : list) {
			for (int i = 0; i < 18; i++) {
				switch (i) {
					/**bookData[0][0]="Participante";*/
					case 0:
						bookData[rowCount][i] = objects[2].toString();
					break;
					/**bookData[0][1]="Nro Solicitud"*/
					case 1:
						String reqNumber = objects[0].toString();
						bookData[rowCount][i] = reqNumber;
					break;
					/**Fecha de Vencimiento*/
					case 2:
						BigDecimal inst = new BigDecimal(objects[24].toString());
						if(inst.intValue()==InstrumentType.FIXED_INCOME.getCode())
							bookData[rowCount][i] = objects[18].toString();
						else
							bookData[rowCount][i] = "";
					break;
					/**Fecha de Cobro*/
					case 3:
						bookData[rowCount][i] = objects[17].toString();
					break;
					/**Cui*/
					case 4:
						bookData[rowCount][i] = objects[7]!=null?objects[7].toString():"0";
					break;
					/**Moneda Origen*/
					case 5:
						bookData[rowCount][i] = objects[5]!=null?objects[5].toString():"-";
					break;
					/**Clase - Clave de Valor*/
					case 6:
						bookData[rowCount][i] = objects[10]!=null?objects[10].toString():"-";
					break;
					/**Cantidad de valores*/
					case 7:
						String quantity = objects[11]!=null?objects[11].toString():"0";
						bookData[rowCount][i] = quantity;
					break;
					/**Nro de Cupon*/
					case 8:			
						String couponNumber = objects[9]!=null?objects[9].toString():"";
						bookData[rowCount][i] = couponNumber;
					break;
					/**Interes*/
					case 9:
						String interest = objects[16]!=null?objects[16].toString():"0";
						bookData[rowCount][i] = Double.parseDouble(interest);
					break;
					/**Amortizacion*/
					case 10:
						String amortization = objects[15]!=null?objects[15].toString():"0";
						bookData[rowCount][i] = Double.parseDouble(amortization);
					break;
					/**Monto al Vencimiento*/
					case 11:
						String amountExpiration = objects[14]!=null?objects[14].toString():"";
						bookData[rowCount][i] = Double.parseDouble(amountExpiration);
					break;
					/**Moneda de Cobro*/
					case 12:
						bookData[rowCount][i] = objects[6].toString();
					break;
					/**Monto a Cobrar*/
					case 13:
						String paymentAmount = objects[13]!=null?objects[13].toString():"0";
						bookData[rowCount][i] = Double.parseDouble(paymentAmount);
					break;
					/**Banco*/
					case 14:
						bookData[rowCount][i] = objects[20].toString();
					break;
					/**bookData[0][15]="Moneda Cuenta"*/
					case 15:
						bookData[rowCount][i] = objects[6].toString();
					break;
					/**	bookData[0][16]="Nro Cuenta"*/
					case 16:
						bookData[rowCount][i] = objects[21].toString();
					break;
					/**bookData[0][17]="Estado solicitud"*/
					case 17:
						bookData[rowCount][i] = objects[23].toString();
					break;
				default:
					break;
				}
			}
			rowCount++;
		}
		
		
		rowCount = 0;
		for (Object[] aBook : bookData) {
			Row row = sheet.createRow(++rowCount);
			
			int columnCount = 0;
			
			for (Object field : aBook) {
				Cell cell = row.createCell(++columnCount);
				if (field instanceof String) {
					cell.setCellValue((String) field);
				} else if (field instanceof Double) {
					cell.setCellValue((Double) field);
				}
				if(rowCount==1){
					cell.setCellStyle(getStyleTitleBold());
					//cell.setCellStyle(styleBorder);
				}
				else
					cell.setCellStyle(style);
			}
		}
	    FileOutputStream fileOut = new FileOutputStream("ReporteCobros.xls");
		
	    ByteArrayOutputStream bos = new ByteArrayOutputStream();
	    bos.writeTo(fileOut);
	   	hSSFWorkbook.write(bos);
	   	bos.close();
	    arrayByte = bos.toByteArray();
	    hSSFWorkbook.write(fileOut);
	    fileOut.close();
	   return arrayByte;
	}
	
	/**
	 * issue 1028
	 * metodo para la generacion del reporte en Apache POI
	 * REPORTE DE TRAMO POR PARTICIPANTE COLOCADOR
	 * @param parameters
	 * @param reportLogger
	 * @return
	 */
	public byte[] writeFileExcelConsPlacementSegmentReportPoi(Map<String, Object> parameters , ReportLogger reportLogger){
		
		byte[] arrayByte = null;

		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("lstObjects");

		initCellStyle();
		initFont();
		initDataFormat();
		
		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						writeFileConsPlacementSegmentReportPoi(hSSFWorkbook,queryMain.subList(i, ifinal),queryMain.subList(i, ifinal).size(),reportLogger);
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{
	        		//Solo un HSSFSheet
	        		writeFileConsPlacementSegmentReportPoi(hSSFWorkbook,queryMain,queryMain.size(),reportLogger);
	        	}
			}else{
				writeFileConsPlacementSegmentReportPoi(hSSFWorkbook,queryMain,queryMain.size(),reportLogger);
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_PLACEMENT_SEGMENT);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	         
	       	hSSFWorkbook.write(bos);
	        
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        
	        hSSFWorkbook.write(fileOut);
	        
	        fileOut.close();
			
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
		return arrayByte;
	}
	
	/**
	 * metodo para la generacion del reporte en Apache POI
	 * REPORTE DE TRAMO POR PARTICIPANTE COLOCADOR
	 * issue 1028
	 * @param hSSFWorkbook
	 * @param queryMain
	 * @param number
	 * @param reportLogger
	 */
	public void writeFileConsPlacementSegmentReportPoi(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, int number,ReportLogger reportLogger) {
		
		int FINAL_COLUMN = 16;
		
		
		//creando la hoja
		HSSFSheet sheet = hSSFWorkbook.createSheet(reportLogger.getReport().getTitle());
		
		/* CABECERA */
        
        /**DATE*/
        HSSFRow row0= sheet.createRow((short)0);
        addContentAndStyle(row0.createCell(FINAL_COLUMN),CommonsUtilities.convertDateToString(reportLogger.getRegistryDate(), ReportConstant.DATE_FORMAT_HEADER));
        
        /**USER**/
        HSSFRow row1= sheet.createRow((short)1);
        addContentAndStyle(row1.createCell(FINAL_COLUMN),reportLogger.getRegistryUser());
        
        /**CLASIFICATION*/
        HSSFRow row2= sheet.createRow((short)2);
        addContentAndStyle(row2.createCell(FINAL_COLUMN),reportLogger.getReport().getReportClasification());
        
        /**MNEMONIC REPORT*/
        HSSFRow row3= sheet.createRow((short)3);
        addContentAndStyle(row3.createCell(FINAL_COLUMN),reportLogger.getReport().getMnemonico());
        
        /* TITULO */
        
        sheet.addMergedRegion(new CellRangeAddress(1,2,0,FINAL_COLUMN - 1));
        addContentAndStyle(row1.createCell(0),getStyleTitleBoldCenter(),reportLogger.getReport().getTitle());
		
        /* CABECERA DATA*/
        int indrow = 8;
        HSSFRow rowHeader = sheet.createRow((int)indrow);
        addContentAndStyle(rowHeader.createCell(0),getStyleTitle(),"PARTICIPANTE");
        addContentAndStyle(rowHeader.createCell(1),getStyleTitle(),"EMISOR");
        addContentAndStyle(rowHeader.createCell(2),getStyleTitle(),"CODIGO EMISION");
        addContentAndStyle(rowHeader.createCell(3),getStyleTitle(),"DESCRIPCION EMISION");
        addContentAndStyle(rowHeader.createCell(4),getStyleTitle(),"CLASE - CLAVE DE VALOR");
        addContentAndStyle(rowHeader.createCell(5),getStyleTitle(),"VALOR NOMINAL");
        addContentAndStyle(rowHeader.createCell(6),getStyleTitle(),"MONEDA");
        addContentAndStyle(rowHeader.createCell(7),getStyleTitle(),"INICIO DE COLOCACION");
        addContentAndStyle(rowHeader.createCell(8),getStyleTitle(),"FINAL DE COLOCACION");
        addContentAndStyle(rowHeader.createCell(9),getStyleTitle(),"PLAZO DE COLOCACION");
        addContentAndStyle(rowHeader.createCell(10),getStyleTitle(),"CANTIDAD DE VALORES");
        addContentAndStyle(rowHeader.createCell(11),getStyleTitle(),"MONTO A COLOCAR");
        addContentAndStyle(rowHeader.createCell(12),getStyleTitle(),"MONTO COLOCADO");
        addContentAndStyle(rowHeader.createCell(13),getStyleTitle(),"MONTO PENDIENTE DE COLOCACION");
        addContentAndStyle(rowHeader.createCell(14),getStyleTitle(),"CANTIDAD DE VALORES CONTABLES");
        addContentAndStyle(rowHeader.createCell(15),getStyleTitle(),"FECHA REGISTRO");
        addContentAndStyle(rowHeader.createCell(16),getStyleTitle(),"ESTADO");
        
        
        HSSFCellStyle getDataStyle = hSSFWorkbook.createCellStyle();
        getDataStyle.setDataFormat((short)14);
        getDataStyle.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
        
        
        /** DATA */
        
        HSSFRow rowData;
        for(Object[] rowResult:queryMain) {
        	indrow++;
        	rowData = sheet.createRow((int)indrow);
        	
            addContentAndStyle(rowData.createCell(0),getStyleContentCenter() ,rowResult[0] != null? rowResult[0].toString() : ""); //PARTICIPANTE
            addContentAndStyle(rowData.createCell(1),getStyleContentCenter() ,rowResult[2] != null? rowResult[2].toString() : ""); //EMISOR
            addContentAndStyle(rowData.createCell(2),getStyleContentCenter() ,rowResult[3] != null? rowResult[3].toString() : ""); //CODIGO EMISION
            addContentAndStyle(rowData.createCell(3),rowResult[4] != null? rowResult[4].toString() : ""); //DESCRIPCION EMISION
            addContentAndStyle(rowData.createCell(4),getStyleContentCenter() ,rowResult[5] != null? rowResult[5].toString() : ""); //CLASE - CLAVE DE VALOR
            addContentAndStyle(rowData.createCell(5),getStyleAccountingAccount() ,rowResult[6] != null? Double.parseDouble(rowResult[6].toString()) : 0.00); //VALOR NOMINAL
            addContentAndStyle(rowData.createCell(6),getStyleContentCenter() ,rowResult[7] != null? rowResult[7].toString() : ""); //MONEDA
            addContentAndStyle(rowData.createCell(7),getDataStyle,rowResult[8] != null? String.valueOf(rowResult[8]) : ""); //INICIO DE COLOCACION
            addContentAndStyle(rowData.createCell(8),getDataStyle ,rowResult[9] != null? String.valueOf(rowResult[9]) : ""); //FINAL DE COLOCACION
            addContentAndStyle(rowData.createCell(9),getStyleAccountingAccount() ,rowResult[10] != null? Double.parseDouble(rowResult[10].toString()) : 0.00); //PLAZO DE COLOCACION
            addContentAndStyle(rowData.createCell(10),getStyleAccountingAccount(),rowResult[11] != null? Double.parseDouble(rowResult[11].toString()) : 0.00); //CANTIDAD DE VALORES
            addContentAndStyle(rowData.createCell(11),getStyleAccountingAccount(),rowResult[12] != null? Double.parseDouble(rowResult[12].toString()) : 0.00); //MONTO A COLOCAR
            addContentAndStyle(rowData.createCell(12),getStyleAccountingAccount(),rowResult[13] != null? Double.parseDouble(rowResult[13].toString()) : 0.00); //MONTO COLOCADO
            addContentAndStyle(rowData.createCell(13),getStyleAccountingAccount(),rowResult[14] != null? Double.parseDouble(rowResult[14].toString()) : 0.00); //MONTO PENDIENTE DE COLOCACION
            addContentAndStyle(rowData.createCell(14),getStyleAccountingAccount(),rowResult[15] != null? Double.parseDouble(rowResult[15].toString()) : 0); //CANTIDAD DE VALORES CONTABLES
            addContentAndStyle(rowData.createCell(15),getDataStyle,rowResult[1] != null? String.valueOf(rowResult[1]) : ""); //FECHA REGISTRO
            addContentAndStyle(rowData.createCell(16),getStyleContentCenter(),rowResult[16] != null? rowResult[16].toString() : ""); //ESTADO

            sheet.autoSizeColumn(0);
            sheet.autoSizeColumn(1);
            sheet.autoSizeColumn(2);
            sheet.autoSizeColumn(4);
            sheet.autoSizeColumn(16);
            
        }
        
	}
	
	


/**
 * metodo para la generacion del reporte 
 * REPORTE REPORTE DE CODIGO UNICO DE IDENTIFICACION POI
 * @param parameters
 * @param reportLogger
 * @return
 */
public byte[] writeFileExcelListHoldersCui(Map<String, Object> parameters , ReportLogger reportLogger  ){
	
	byte[] arrayByte=null;

	/**The Data*/
	List<Object[]> queryMain=(List<Object[]>) parameters.get("lstObjects");
	String dateInitial = (String) parameters.get("dateInitial");
	String dateEnd = (String) parameters.get("dateEnd");

	initCellStyle();
	initFont();
	initDataFormat();
	
	try{
		if(queryMain!=null && queryMain.size()>0){
			if(queryMain.size()>MAX_ROWS_EXCEL_2007){
        		//Hacer cortes por cada 65535
				for(int i=0;i<queryMain.size();){
					int ifinal=i+MAX_ROWS_EXCEL_2007-1;
					if(ifinal>=queryMain.size()){
						 ifinal=queryMain.size();
					 }
					writeFileExcelListHoldersCuiPoi(hSSFWorkbook,queryMain.subList(i, ifinal),queryMain.subList(i, ifinal).size(),reportLogger,dateInitial,dateEnd);
					i=i+MAX_ROWS_EXCEL_2007;
				}
        	}else{
        		//Solo un HSSFSheet
        		writeFileExcelListHoldersCuiPoi(hSSFWorkbook,queryMain,queryMain.size(),reportLogger,dateInitial,dateEnd);
        	}
		}
		
		//Escribimos los resultados a un fichero Excel
        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_COMMISSIONS_SUMMARY_PORTFOLIO);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.writeTo(fileOut);
         
       	hSSFWorkbook.write(bos);
        
       	bos.close();
        arrayByte = bos.toByteArray();
        
        hSSFWorkbook.write(fileOut);
        
        fileOut.close();
		
	}
    catch(IOException e){
    	e.printStackTrace();
        System.out.println("Error al escribir el fichero.");
        return null;
    }
	return arrayByte;
}


/**
 * metodo para la generacion del reporte en Apache POI
 * REPORTE DE CODIGO UNICO DE IDENTIFICACION POI
 * issue 1200
 * @param hSSFWorkbook
 * @param queryMain
 * @param number
 * @param reportLogger
 */
public void writeFileExcelListHoldersCuiPoi(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, int number,ReportLogger reportLogger,String dateInitial,String dateEnd) {
	
	int INDEX_REGISTRY_DATE = 1;
	int INDEX_ID_PARTICIPANT_FK = 2;
	int INDEX_ID_HOLDER_PK = 3;
	int INDEX_DOCUMENT_TYPE = 4;
	int INDEX_DOCUMENT_NUMBER = 5;
	int INDEX_FULL_NAME = 6;
	int INDEX_SEX = 7;
	int INDEX_BIRTH_DATE = 8;
	int INDEX_HOLDER_TYPE = 9;
	int INDEX_ECONOMIC_ACTIVITY = 10;
	int INDEX_STATE_HOLDER = 11;
	int INDEX_SECURITY_BALANCE = 12;
	int INDEX_HOLDER_ACCOUNTS = 13;
	
	//creando la hoja
	HSSFSheet sheet = hSSFWorkbook.createSheet("Reporte CUI");
	
	//cabecera del reporte
	String titleReport=reportLogger.getReport().getTitle();
	String strStartHour = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),ReportConstant.DATE_FORMAT_TIME_SECOND);
	
	HSSFRow row0= sheet.createRow((short)0);
    sheet.addMergedRegion(new CellRangeAddress(0,0,3,10));
    addContentAndStyle(row0.createCell(3),getStyleTitleBoldCenter(),titleReport);
    
    if(dateInitial!=null && dateEnd!=null){
    	/**DATE INI*/
        HSSFRow row1= sheet.createRow((short)1);
        addContentAndStyle(row1.createCell(5),getStyleTitleBold(),ReportConstant.LABEL_FECHA_INICIO);
        addContentAndStyle(row1.createCell(6),CommonsUtilities.convertDatetoString(CommonsUtilities.convertStringtoDate(dateInitial)));
        
        /**DATE FIN*/
        addContentAndStyle(row1.createCell(7),getStyleTitleBold(),ReportConstant.LABEL_FECHA_FIN);
        addContentAndStyle(row1.createCell(8),CommonsUtilities.convertDatetoString(CommonsUtilities.convertStringtoDate(dateEnd)));	
    }
    
    /**DATE*/
    HSSFRow row2= sheet.createRow((short)2);
    addContentAndStyle(row2.createCell(12),getStyleTitleBold(),ReportConstant.LABEL_FECHA);
    addContentAndStyle(row2.createCell(13),CommonsUtilities.convertDatetoString(reportLogger.getRegistryDate()));
    
    /**HOUR*/
    HSSFRow row3= sheet.createRow((short)3);
    addContentAndStyle(row3.createCell(12),getStyleTitleBold(), ReportConstant.LABEL_HORA);
    addContentAndStyle(row3.createCell(13), strStartHour);
    
    /**CLASIFICATION*/
    HSSFRow row4= sheet.createRow((short)4);
    addContentAndStyle(row4.createCell(12),getStyleTitleBold(),ReportConstant.LABEL_CLASIFICACION);
    addContentAndStyle(row4.createCell(13),reportLogger.getReport().getReportClasification());

    
    /**Usuario**/
    HSSFRow row5= sheet.createRow((short)5);
    addContentAndStyle(row5.createCell(12),getStyleTitleBold(),ReportConstant.LABEL_USUARIO);
    addContentAndStyle(row5.createCell(13),reportLogger.getRegistryUser());
    
    int indrow=8;
    
    HSSFRow rowAux= sheet.createRow((short)indrow-1);
    sheet.addMergedRegion(new CellRangeAddress(indrow-1,indrow-1,INDEX_SECURITY_BALANCE,INDEX_HOLDER_ACCOUNTS));
    addContentAndStyle(rowAux.createCell(INDEX_SECURITY_BALANCE),getStyleTitleBold(),ReportConstant.LABEL_FACT_DATE);
    
    HSSFRow rowHeader = sheet.createRow((int)indrow);
    addContentAndStyle(rowHeader.createCell(INDEX_REGISTRY_DATE),getStyleTitle(),ReportConstant.LABEL_DATE_REGISTER);
    addContentAndStyle(rowHeader.createCell(INDEX_ID_PARTICIPANT_FK),getStyleTitle(),ReportConstant.LABEL_PARTICIPANT+" CREADOR");
    addContentAndStyle(rowHeader.createCell(INDEX_ID_HOLDER_PK),getStyleTitle(),ReportConstant.LABEL_CUI);
    addContentAndStyle(rowHeader.createCell(INDEX_DOCUMENT_TYPE),getStyleTitle(),ReportConstant.LABEL_DOCUMENT_TYPE);
    addContentAndStyle(rowHeader.createCell(INDEX_DOCUMENT_NUMBER),getStyleTitle(),ReportConstant.LABEL_DOCUMENT_NUM);
    addContentAndStyle(rowHeader.createCell(INDEX_FULL_NAME),getStyleTitle(),ReportConstant.LABEL_DESCRIPCION_TITULAR);
    addContentAndStyle(rowHeader.createCell(INDEX_SEX),getStyleTitle(),ReportConstant.LABEL_GENERO);
    addContentAndStyle(rowHeader.createCell(INDEX_BIRTH_DATE),getStyleTitle(),ReportConstant.LABEL_EDAD);
    addContentAndStyle(rowHeader.createCell(INDEX_HOLDER_TYPE),getStyleTitle(),ReportConstant.LABEL_TIPO_TITULAR);
    addContentAndStyle(rowHeader.createCell(INDEX_ECONOMIC_ACTIVITY),getStyleTitle(),ReportConstant.LABEL_ECONOMIC_ACTIVITY);
    addContentAndStyle(rowHeader.createCell(INDEX_STATE_HOLDER),getStyleTitle(),ReportConstant.LABEL_STATE);
    addContentAndStyle(rowHeader.createCell(INDEX_SECURITY_BALANCE),getStyleTitle(),ReportConstant.LABEL_SECURITY_BALANCE);
    addContentAndStyle(rowHeader.createCell(INDEX_HOLDER_ACCOUNTS),getStyleTitle(),ReportConstant.LABEL_HOLDER_ACCOUNTS);
    
    HSSFRow rowData;
    for(Object[] rowResult:queryMain) {
    	indrow++;
    	rowData = sheet.createRow((int)indrow);
    	addContentAndStyle(rowData.createCell(INDEX_REGISTRY_DATE),getStyleAccountingAccountLeft(), 		rowResult[0] != null ? rowResult[0].toString() : "-");
    	addContentAndStyle(rowData.createCell(INDEX_ID_PARTICIPANT_FK),getStyleAccountingAccountLeft(), 	rowResult[1] != null ? rowResult[1].toString() : "-");
    	addContentAndStyle(rowData.createCell(INDEX_ID_HOLDER_PK),getStyleAccountingAccountLeft(), 			rowResult[2] != null ? rowResult[2].toString() : "-");
    	addContentAndStyle(rowData.createCell(INDEX_DOCUMENT_TYPE),getStyleAccountingAccountLeft(), 		rowResult[3] != null ? rowResult[3].toString() : "-");
    	addContentAndStyle(rowData.createCell(INDEX_DOCUMENT_NUMBER),getStyleAccountingAccountLeft(), 		rowResult[4] != null ? rowResult[4].toString() : "-");
    	addContentAndStyle(rowData.createCell(INDEX_FULL_NAME),getStyleAccountingAccountLeft(), 			rowResult[5] != null ? rowResult[5].toString() : "-");
    	addContentAndStyle(rowData.createCell(INDEX_SEX),getStyleAccountingAccountLeft(), 					rowResult[6] != null ? rowResult[6].toString() : "-");
    	addContentAndStyle(rowData.createCell(INDEX_BIRTH_DATE),getStyleAccountingAccountLeft(), 			rowResult[7] != null ? rowResult[7].toString() : "-");
    	addContentAndStyle(rowData.createCell(INDEX_HOLDER_TYPE),getStyleAccountingAccountLeft(), 			rowResult[8] != null ? rowResult[8].toString() : "-");
    	addContentAndStyle(rowData.createCell(INDEX_ECONOMIC_ACTIVITY),getStyleAccountingAccountLeft(), 	rowResult[9] != null ? rowResult[9].toString() : "-");
    	addContentAndStyle(rowData.createCell(INDEX_STATE_HOLDER),getStyleAccountingAccountLeft(), 			rowResult[10] != null ? rowResult[10].toString() : "-");
    	addContentAndStyle(rowData.createCell(INDEX_SECURITY_BALANCE),getStyleAccountingAccountLeft(), 		rowResult[11] != null ? rowResult[11].toString() : "-");
    	addContentAndStyle(rowData.createCell(INDEX_HOLDER_ACCOUNTS),getStyleAccountingAccountLeft(), 		rowResult[12] != null ? rowResult[12].toString() : "-");
    	
    }
}

	/**
	 * issue 1228
	 * metodo para la generacion del reporte en Apache POI
	 * REPORTE DE TRAMO POR PARTICIPANTE COLOCADOR
	 * @param parameters
	 * @param reportLogger
	 * @return
	 */
	public byte[] writeFileExcelConsVoluntaryAccountEntriesReportPoi(Map<String, Object> parameters , ReportLogger reportLogger){
		
		byte[] arrayByte = null;
	
		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("lstObjects");
	
		initCellStyle();
		initFont();
		initDataFormat();
		
		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						writeFileConsVoluntaryAccountEntriesReportPoi(hSSFWorkbook,queryMain.subList(i, ifinal),queryMain.subList(i, ifinal).size(),reportLogger);
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{
	        		//Solo un HSSFSheet
	        		writeFileConsVoluntaryAccountEntriesReportPoi(hSSFWorkbook,queryMain,queryMain.size(),reportLogger);
	        	}
			}else{
				writeFileConsVoluntaryAccountEntriesReportPoi(hSSFWorkbook,queryMain,queryMain.size(),reportLogger);
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_VOLUNTARY_ACCOUNT_ENTRIES);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	         
	       	hSSFWorkbook.write(bos);
	        
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        
	        hSSFWorkbook.write(fileOut);
	        
	        fileOut.close();
			
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
		return arrayByte;
	}

	/**
	 * metodo para la generacion del reporte en Apache POI
	 * REPORTE DE ANOTACION EN CUENTA
	 * issue 1228
	 * @param hSSFWorkbook
	 * @param queryMain
	 * @param number
	 * @param reportLogger
	 */
	public void writeFileConsVoluntaryAccountEntriesReportPoi(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, int number,ReportLogger reportLogger) {
		
		int FINAL_COLUMN = 21;
		
		
		//creando la hoja
		HSSFSheet sheet = hSSFWorkbook.createSheet(reportLogger.getReport().getTitle());
		
		/* CABECERA */
	    
	    /**DATE*/
	    HSSFRow row0= sheet.createRow((short)0);
	    addContentAndStyle(row0.createCell(FINAL_COLUMN),CommonsUtilities.convertDateToString(reportLogger.getRegistryDate(), ReportConstant.DATE_FORMAT_HEADER));
	    
	    /**USER**/
	    HSSFRow row1= sheet.createRow((short)1);
	    addContentAndStyle(row1.createCell(FINAL_COLUMN),reportLogger.getRegistryUser());
	    
	    /**CLASIFICATION*/
	    HSSFRow row2= sheet.createRow((short)2);
	    addContentAndStyle(row2.createCell(FINAL_COLUMN),reportLogger.getReport().getReportClasification());
	    
	    /**MNEMONIC REPORT*/
	    HSSFRow row3= sheet.createRow((short)3);
	    addContentAndStyle(row3.createCell(FINAL_COLUMN),reportLogger.getReport().getMnemonico());
	    
	    /* TITULO */
	    
	    sheet.addMergedRegion(new CellRangeAddress(1,2,0,FINAL_COLUMN - 1));
	    addContentAndStyle(row1.createCell(0),getStyleTitleBoldCenter(),reportLogger.getReport().getTitle());
		
	    /* CABECERA DATA*/
	    int indrow = 8;
	    HSSFRow rowHeader = sheet.createRow((int)indrow);
	    addContentAndStyle(rowHeader.createCell(0),getStyleTitle(), "FECHA SOLICITUD CONFIRMADA");
	    addContentAndStyle(rowHeader.createCell(1),getStyleTitle(), "PARTICIPANTE");
	    addContentAndStyle(rowHeader.createCell(2),getStyleTitle(), "CUENTA TITULAR");
	    addContentAndStyle(rowHeader.createCell(3),getStyleTitle(), "CUI");
	    addContentAndStyle(rowHeader.createCell(4),getStyleTitle(), "TIPO DE PERSONA");
	    addContentAndStyle(rowHeader.createCell(5),getStyleTitle(), "ACTIVIDAD ECONOMICA");
	    addContentAndStyle(rowHeader.createCell(6),getStyleTitle(), "MOTIVO");
	    addContentAndStyle(rowHeader.createCell(7),getStyleTitle(), "CLASE DE VALOR");
	    addContentAndStyle(rowHeader.createCell(8),getStyleTitle(), "EMISOR");
	    addContentAndStyle(rowHeader.createCell(9),getStyleTitle(), "CLAVE DE VALOR");
	    addContentAndStyle(rowHeader.createCell(10),getStyleTitle(),"MONEDA");
	    addContentAndStyle(rowHeader.createCell(11),getStyleTitle(),"FECHA DE EMISION ");
	    addContentAndStyle(rowHeader.createCell(12),getStyleTitle(),"PLAZO");
	    addContentAndStyle(rowHeader.createCell(13),getStyleTitle(),"FECHA DE VENCIMIENTO");
	    addContentAndStyle(rowHeader.createCell(14),getStyleTitle(),"VALOR NOMINAL EN MONEDA ORIGINAL");
	    addContentAndStyle(rowHeader.createCell(15),getStyleTitle(),"TASA DE EMISION");
	    addContentAndStyle(rowHeader.createCell(16),getStyleTitle(),"TASA DE ADQUISICION");
	    addContentAndStyle(rowHeader.createCell(17),getStyleTitle(),"PRECIO DE ADQUISICION");
	    addContentAndStyle(rowHeader.createCell(18),getStyleTitle(),"CANTIDAD DE VALORES");
	    addContentAndStyle(rowHeader.createCell(19),getStyleTitle(),"VALOR NOMINAL ACTUAL");
	    addContentAndStyle(rowHeader.createCell(20),getStyleTitle(),"MONTO TOTAL MONEDA ORIGINAL");
	    addContentAndStyle(rowHeader.createCell(21),getStyleTitle(),"MONTO TOTAL USD");
	    
	    /** DATA */
	    HSSFRow rowData;
        for(Object[] rowResult:queryMain) {
        	indrow++;
        	rowData = sheet.createRow((int)indrow);
	    	
        	addContentAndStyle(rowData.createCell( 0),getStyleAccountingAccount(),rowResult[ 0] != null? rowResult[ 0].toString():""); //FECHA SOLICITUD CONFIRMADA
        	addContentAndStyle(rowData.createCell( 1),getStyleAccountingAccount(),rowResult[ 1] != null? rowResult[ 1].toString():""); //PARTICIPANTE
        	addContentAndStyle(rowData.createCell( 2),getStyleAccountingAccount(),rowResult[ 2] != null? rowResult[ 2].toString():""); //CUENTA TITULAR
        	addContentAndStyle(rowData.createCell( 3),getStyleAccountingAccount(),rowResult[ 3] != null? rowResult[ 3].toString():""); //CUI
        	addContentAndStyle(rowData.createCell( 4),getStyleAccountingAccount(),rowResult[ 4] != null? rowResult[ 4].toString():""); //TIPO DE PERSONA
        	addContentAndStyle(rowData.createCell( 5),getStyleAccountingAccount(),rowResult[ 5] != null? rowResult[ 5].toString():""); //ACTIVIDAD ECONOMICA
        	addContentAndStyle(rowData.createCell( 6),getStyleAccountingAccount(),rowResult[ 6] != null? rowResult[ 6].toString():""); //MOTIVO
        	addContentAndStyle(rowData.createCell( 7),getStyleAccountingAccount(),rowResult[ 7] != null? rowResult[ 7].toString():""); //CLASE DE VALOR
        	addContentAndStyle(rowData.createCell( 8),getStyleAccountingAccount(),rowResult[ 8] != null? rowResult[ 8].toString():""); //EMISOR
        	addContentAndStyle(rowData.createCell( 9),getStyleAccountingAccount(),rowResult[ 9] != null? rowResult[ 9].toString():""); //CLAVE DE VALOR
        	addContentAndStyle(rowData.createCell(10),getStyleAccountingAccount(),rowResult[10] != null? rowResult[10].toString():""); //MONEDA
        	addContentAndStyle(rowData.createCell(11),getStyleAccountingAccount(),rowResult[11] != null? rowResult[11].toString():""); //FECHA DE EMISION 
        	addContentAndStyle(rowData.createCell(12),getStyleAccountingAccount(),rowResult[12] != null? Double.parseDouble(rowResult[12].toString()):0.00); //PLAZO
        	addContentAndStyle(rowData.createCell(13),getStyleAccountingAccount(),rowResult[13] != null? rowResult[13].toString():""); //FECHA DE VENCIMIENTO
        	addContentAndStyle(rowData.createCell(14),getStyleAccountingAccount(),rowResult[14] != null? Double.parseDouble(rowResult[14].toString()):0.00); //VALOR NOMINAL EN MONEDA ORIGINAL
        	addContentAndStyle(rowData.createCell(15),getStyleAccountingAccount(),rowResult[15] != null? Double.parseDouble(rowResult[15].toString()):0.00); //TASA DE EMISION
        	addContentAndStyle(rowData.createCell(16),getStyleAccountingAccount(),rowResult[16] != null? Double.parseDouble(rowResult[16].toString()):0.00); //TASA DE ADQUISICION
        	addContentAndStyle(rowData.createCell(17),getStyleAccountingAccount(),rowResult[17] != null? Double.parseDouble(rowResult[17].toString()):0.00); //PRECIO DE ADQUISICION
        	addContentAndStyle(rowData.createCell(18),getStyleAccountingAccount(),rowResult[18] != null? Integer.parseInt(rowResult[18].toString()):0.00); //CANTIDAD DE VALORES
        	addContentAndStyle(rowData.createCell(19),getStyleAccountingAccount(),rowResult[19] != null? Double.parseDouble(rowResult[19].toString()):0.00); //VALOR NOMINAL ACTUAL
        	addContentAndStyle(rowData.createCell(20),getStyleAccountingAccount(),rowResult[20] != null? Double.parseDouble(rowResult[20].toString()):0.00); //MONTO TOTAL MONEDA ORIGINAL
        	addContentAndStyle(rowData.createCell(21),getStyleAccountingAccount(),rowResult[21] != null? Double.parseDouble(rowResult[21].toString()):0.00); //MONTO TOTAL USD
	
	    }
	    
	}

	/**
	 * issue 1285
	 * metodo para la generacion del reporte en Apache POI
	 * REPORTE DE ACCIONES
	 * @param parameters
	 * @param reportLogger
	 * @return
	 */
	public byte[] writeFileExcelActionsReportPoi(Map<String, Object> parameters, ReportLogger reportLogger){
		
		byte[] arrayByte = null;
	
		/**The Data*/
		List<Object[]> queryMain = (List<Object[]>) parameters.get("lstObjects");
	
		initCellStyle();
		initFont();
		initDataFormat();
		
		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007-1;
						if(ifinal>=queryMain.size()) ifinal=queryMain.size();
						writeFileActionsReportPoi(hSSFWorkbook,queryMain.subList(i, ifinal),queryMain.subList(i, ifinal).size(),reportLogger);
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{
	        		//Solo un HSSFSheet
	        		writeFileActionsReportPoi(hSSFWorkbook,queryMain,queryMain.size(),reportLogger);
	        	}
			}else{
				writeFileActionsReportPoi(hSSFWorkbook,queryMain,queryMain.size(),reportLogger);
			}
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_ACTIONS);
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	       	hSSFWorkbook.write(bos);
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        hSSFWorkbook.write(fileOut);
	        fileOut.close();
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
		return arrayByte;
	}
	
	/**
	 * metodo para la generacion del reporte en Apache POI
	 * REPORTE DE ACCIONES
	 * issue 1285
	 * @param hSSFWorkbook
	 * @param queryMain
	 * @param number
	 * @param reportLogger
	 */
	public void writeFileActionsReportPoi(HSSFWorkbook hSSFWorkbook, List<Object[]> queryMain, int number, ReportLogger reportLogger) {
		
		int FINAL_COLUMN = 10;
		HSSFSheet sheet = hSSFWorkbook.createSheet(reportLogger.getReport().getTitle());
		
		/* CABECERA */
	    
	    /**DATE*/
	    HSSFRow row0= sheet.createRow((short)0);
	    addContentAndStyle(row0.createCell(FINAL_COLUMN),CommonsUtilities.convertDateToString(reportLogger.getRegistryDate(), ReportConstant.DATE_FORMAT_HEADER));
	    
	    /**USER**/
	    HSSFRow row1= sheet.createRow((short)1);
	    addContentAndStyle(row1.createCell(FINAL_COLUMN),reportLogger.getRegistryUser());
	    
	    /**CLASIFICATION*/
	    HSSFRow row2= sheet.createRow((short)2);
	    addContentAndStyle(row2.createCell(FINAL_COLUMN),reportLogger.getReport().getReportClasification());
	    
	    /**MNEMONIC REPORT*/
	    HSSFRow row3= sheet.createRow((short)3);
	    addContentAndStyle(row3.createCell(FINAL_COLUMN),reportLogger.getReport().getMnemonico());
	    
	    /* TITULO */
	    sheet.addMergedRegion(new CellRangeAddress(1,2,0,FINAL_COLUMN - 1));
	    addContentAndStyle(row1.createCell(0),getStyleTitleBoldCenter(),reportLogger.getReport().getTitle());
		
	    /* DATA CABECERA */
	    int indrow = 8;
	    HSSFRow rowHeader = sheet.createRow((int)indrow);
	    addContentAndStyle(rowHeader.createCell(0),getStyleTitle(), "CUENTA TITULAR");
	    addContentAndStyle(rowHeader.createCell(1),getStyleTitle(), "RUT");
	    addContentAndStyle(rowHeader.createCell(2),getStyleTitle(), "DESCRIPCION DEL TITULAR");
	    addContentAndStyle(rowHeader.createCell(3),getStyleTitle(), "TIPO DE CUENTA");
	    addContentAndStyle(rowHeader.createCell(4),getStyleTitle(), "CODIGO DE VALOR");
	    addContentAndStyle(rowHeader.createCell(5),getStyleTitle(), "VALOR NOMINAL");
	    addContentAndStyle(rowHeader.createCell(6),getStyleTitle(), "BLOQ. CAT");
	    addContentAndStyle(rowHeader.createCell(7),getStyleTitle(), "BLOQ. REQ. JUDICIAL");
	    addContentAndStyle(rowHeader.createCell(8),getStyleTitle(), "BLOQ. PRENDA");
	    addContentAndStyle(rowHeader.createCell(9),getStyleTitle(), "CANTIDAD DISPONIBLE");
	    addContentAndStyle(rowHeader.createCell(10),getStyleTitle(),"CANTIDAD TOTAL");
	    
	    /** DATA */
	    HSSFRow rowData;
        for(Object[] rowResult:queryMain) {
        	indrow++;
        	rowData = sheet.createRow((int)indrow);
	    	
        	addContentAndStyle(rowData.createCell( 0),getStyleAccountingAccount(),rowResult[ 0] != null? rowResult[ 0].toString():""); //CUENTA TITULAR
        	addContentAndStyle(rowData.createCell( 1),getStyleAccountingAccount(),rowResult[ 1] != null? rowResult[ 1].toString():""); //CUI
        	addContentAndStyle(rowData.createCell( 2),getStyleAccountingAccount(),rowResult[ 2] != null? rowResult[ 2].toString():""); //DESCRIPCION DEL TITULAR
        	addContentAndStyle(rowData.createCell( 3),getStyleAccountingAccount(),rowResult[ 3] != null? rowResult[ 3].toString():""); //TIPO DE CUENTA
        	addContentAndStyle(rowData.createCell( 4),getStyleAccountingAccount(),rowResult[ 4] != null? rowResult[ 4].toString():""); //CLASE-CLAVE VALOR
        	addContentAndStyle(rowData.createCell( 5),getStyleAccountingAccount(),rowResult[ 5] != null? Double.parseDouble(rowResult[ 5].toString()):0.00); //VALOR NOMINAL
        	addContentAndStyle(rowData.createCell( 6),getStyleAccountingAccount(),rowResult[ 6] != null? Integer.parseInt(rowResult[6].toString()):0.00); //BLOQUEO - CAT
        	addContentAndStyle(rowData.createCell( 7),getStyleAccountingAccount(),rowResult[ 7] != null? Integer.parseInt(rowResult[7].toString()):0.00); //BLOQUEO - REQ JUDICIAL
        	addContentAndStyle(rowData.createCell( 8),getStyleAccountingAccount(),rowResult[ 8] != null? Integer.parseInt(rowResult[8].toString()):0.00); //BLOQUEO - PRENDA
        	addContentAndStyle(rowData.createCell( 9),getStyleAccountingAccount(),rowResult[ 9] != null? Double.parseDouble(rowResult[ 9].toString()):0.00); //CANTIDAD DISPONIBLE
        	addContentAndStyle(rowData.createCell(10),getStyleAccountingAccount(),rowResult[10] != null? Double.parseDouble(rowResult[10].toString()):0.00); //CANTIDAD TOTAL
	    }
	}
	
	/**
	 * Transfer Operation Report
	 * Issue 1337
	 * @param parameters
	 * @param reportLogger
	 * @return
	 */
	public byte[] writeFileExcelTransferOperation(Map<String, Object> parameters , ReportLogger reportLogger  ){
		
		byte[] arrayByte=null;

		/**The Data*/
		List<Object[]> queryMain=(List<Object[]>) parameters.get("str_query");

		initCellStyle();
		initFont();
		initDataFormat();
		
		try{
			if(queryMain!=null && queryMain.size()>0){
				if(queryMain.size()>MAX_ROWS_EXCEL_2007){
	        		//Hacer cortes por cada 65535
					for(int i=0;i<queryMain.size();){
						int ifinal=i+MAX_ROWS_EXCEL_2007;
						
						if(ifinal>=queryMain.size()){
							 ifinal=queryMain.size();
						 }
						
						createSheetTransferOperation(hSSFWorkbook,queryMain.subList(i, ifinal),CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),"HHmmssSSSSS"));
						i=i+MAX_ROWS_EXCEL_2007;
					}
	        	}else{
	        		//Solo un HSSFSheet
	        		createSheetTransferOperation(hSSFWorkbook,queryMain,CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),"HHmmssSSSSS"));
	        	}
			}
			
			//Escribimos los resultados a un fichero Excel
	        FileOutputStream fileOut = new FileOutputStream(ReportConstant.LABEL_NAME_FILE_EXCEL_TRANSFER_OPERATION);
	
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        bos.writeTo(fileOut);
	         
	       	hSSFWorkbook.write(bos);
	        
	       	bos.close();
	        arrayByte = bos.toByteArray();
	        
	        hSSFWorkbook.write(fileOut);
	        
	        fileOut.close();
			
		}
	    catch(IOException e){
	    	e.printStackTrace();
	        System.out.println("Error al escribir el fichero.");
	        return null;
	    }
		return arrayByte;
	}
	
	/**
	 * 
	 * @param hSSFWorkbook
	 * @param queryMain
	 */
	public void createSheetTransferOperation(HSSFWorkbook hSSFWorkbook,List<Object[]> queryMain, String number){
		
		final int INDEX_DATE=0;
		final int INDEX_SECURITY_CLASS_CODE=1;
		final int INDEX_ALTERNATE_SERIES=2;
		final int INDEX_PARTE=3;
		final int INDEX_CANTIDAD=4;
		final int INDEX_ROL=5;
		final int INDEX_PARTICIPANTE=6;
		final int INDEX_CI_NIT=7;
		final int INDEX_CUI=8;
		final int INDEX_TITULAR_ORIGEN=9;
		final int INDEX_ROL_DES=10;
		final int INDEX_PARTICIPANTE_DES=11;
		final int INDEX_CI_NIT_DES=12;
		final int INDEX_CUI_DES=13;
		final int INDEX_TITULAR_DESTINO=14;
		final int INDEX_MECANISMO=15;
		final int INDEX_MODALIDAD=16;
		
		
		/**Created a new sheet within the workBock**/
    	HSSFSheet sheet = hSSFWorkbook.createSheet("TransferOperation"+number);
    	sheet.addMergedRegion(new CellRangeAddress(Integer.parseInt("2"),Integer.parseInt("2"),Integer.parseInt("5"),Integer.parseInt("9")));
    	sheet.addMergedRegion(new CellRangeAddress(Integer.parseInt("2"),Integer.parseInt("2"),Integer.parseInt("10"),Integer.parseInt("14")));
    	
		/*****
         * BEGIN - REPORT HEADER 
         */
    	HSSFRow rowHeaderSub = sheet.createRow((int)1);
    	addContentAndStyle(rowHeaderSub.createCell(4),getStyleTitleReport(),"TRANSFERENCIAS DE VALORES");
    	rowHeaderSub = sheet.createRow((int)2);
    	addContentAndStyle(rowHeaderSub.createCell(5),getStyleTitleBoldCenter(),"DATOS DE ORIGEN");
    	addContentAndStyle(rowHeaderSub.createCell(10),getStyleTitleBoldCenter(),"DATOS DE DESTINO");
    	
        int indrow=3;


        /**Created Header*/
        HSSFRow rowHeader = sheet.createRow((int)indrow);
        addContentAndStyle(rowHeader.createCell(INDEX_DATE),getStyleTitle(),ReportConstant.LABEL_DATE);
        addContentAndStyle(rowHeader.createCell(INDEX_SECURITY_CLASS_CODE),getStyleTitle(),ReportConstant.LABEL_SECURITY_CLASS_CODE);
        addContentAndStyle(rowHeader.createCell(INDEX_ALTERNATE_SERIES),getStyleTitle(),ReportConstant.LABEL_ALTERNATE_SERIES);
        addContentAndStyle(rowHeader.createCell(INDEX_PARTE),getStyleTitle(),ReportConstant.LABEL_PART);
        addContentAndStyle(rowHeader.createCell(INDEX_CANTIDAD),getStyleTitle(),ReportConstant.LABEL_CANTIDAD);
        addContentAndStyle(rowHeader.createCell(INDEX_ROL),getStyleTitle(),ReportConstant.LABEL_ROL);
        addContentAndStyle(rowHeader.createCell(INDEX_PARTICIPANTE),getStyleTitle(),ReportConstant.LABEL_PARTICIPANT);
        addContentAndStyle(rowHeader.createCell(INDEX_CI_NIT),getStyleTitle(),ReportConstant.LABEL_CI_NIT);
        addContentAndStyle(rowHeader.createCell(INDEX_CUI),getStyleTitle(),ReportConstant.LABEL_CUI);
        addContentAndStyle(rowHeader.createCell(INDEX_TITULAR_ORIGEN),getStyleTitle(),ReportConstant.LABEL_ORIGIN_HOLDER);
        addContentAndStyle(rowHeader.createCell(INDEX_ROL_DES),getStyleTitle(),ReportConstant.LABEL_ROL);
        addContentAndStyle(rowHeader.createCell(INDEX_PARTICIPANTE_DES),getStyleTitle(),ReportConstant.LABEL_PARTICIPANT);
        addContentAndStyle(rowHeader.createCell(INDEX_CI_NIT_DES),getStyleTitle(),ReportConstant.LABEL_CI_NIT);
        addContentAndStyle(rowHeader.createCell(INDEX_CUI_DES),getStyleTitle(),ReportConstant.LABEL_CUI);
        addContentAndStyle(rowHeader.createCell(INDEX_TITULAR_DESTINO),getStyleTitle(),ReportConstant.LABEL_ORIGIN_DESTINATION);
        addContentAndStyle(rowHeader.createCell(INDEX_MECANISMO),getStyleTitle(),ReportConstant.LABEL_OPERATION_TYPE);
        addContentAndStyle(rowHeader.createCell(INDEX_MODALIDAD),getStyleTitle(),ReportConstant.LABEL_MODALIDAD);
        
        
        
        
        HSSFCellStyle cellStyle = hSSFWorkbook.createCellStyle();
        cellStyle.setDataFormat((short)14);
        
        for (Object[] query : queryMain) {
        
        	String fecha=query[3]!=null?String.valueOf(query[3]):"";
        	String claseClaveValor=query[4]!=null?String.valueOf(query[4]):"";
        	String codigoAlternativo=query[5]!=null?String.valueOf(query[5]):"";
        	String parte=query[1]!=null?String.valueOf(query[1]):"";
        	String cantidad=query[6]!=null?String.valueOf(query[6]):"";
        	String rol=query[7]!=null?String.valueOf(query[7]):"";
        	String participante=query[8]!=null?String.valueOf(query[8]):"";
        	String ciNti=query[9]!=null?String.valueOf(query[9]):"";
        	String cui=query[10]!=null?String.valueOf(query[10]):"";
        	String titularOrigen=query[11]!=null?String.valueOf(query[11]):"";
        	String rolDes=query[12]!=null?String.valueOf(query[12]):"";
        	String participanteDes=query[13]!=null?String.valueOf(query[13]):"";
        	String ciNtiDes=query[14]!=null?String.valueOf(query[14]):"";
        	String cuiDes=query[15]!=null?String.valueOf(query[15]):"";
        	String titularOrigenDes=query[16]!=null?String.valueOf(query[16]):"";
        	Integer mecanismo=(Integer) (query[17]!=null?new Integer(String.valueOf(query[17])):"");
        	String modalidad=query[19]!=null?String.valueOf(query[19]):"";
        	
	        indrow++;

	        /**
	         * La data 
	         */
	        HSSFRow rowData = sheet.createRow((int)indrow);
	        addContentAndStyle(rowData.createCell(INDEX_DATE),cellStyle,CommonsUtilities.convertStringtoDate(fecha));
	        addContentAndStyle(rowData.createCell(INDEX_SECURITY_CLASS_CODE),getStyleContentCenter(), claseClaveValor);
	        addContentAndStyle(rowData.createCell(INDEX_ALTERNATE_SERIES),getStyleAccountingAccountLeft(), codigoAlternativo);
	        addContentAndStyle(rowData.createCell(INDEX_PARTE),getStyleAccountingAccount(), parte);
	        addContentAndStyle(rowData.createCell(INDEX_CANTIDAD),getStyleAccountingAccount(), cantidad);
	        addContentAndStyle(rowData.createCell(INDEX_ROL),getStyleAccountingAccount(), rol);
	        addContentAndStyle(rowData.createCell(INDEX_PARTICIPANTE),getStyleAccountingAccount(), participante);
	        addContentAndStyle(rowData.createCell(INDEX_CI_NIT),getStyleAccountingAccount(), ciNti.replace("<br>", "; "));
	        addContentAndStyle(rowData.createCell(INDEX_CUI),getStyleAccountingAccount(), cui.replace("<br>", "; "));
	        addContentAndStyle(rowData.createCell(INDEX_TITULAR_ORIGEN),getStyleAccountingAccount(), titularOrigen.replace("<br>", "; "));
	        addContentAndStyle(rowData.createCell(INDEX_ROL_DES),getStyleAccountingAccount(), rolDes);
	        addContentAndStyle(rowData.createCell(INDEX_PARTICIPANTE_DES),getStyleAccountingAccount(), participanteDes);
	        addContentAndStyle(rowData.createCell(INDEX_CI_NIT_DES),getStyleAccountingAccount(), ciNtiDes.replace("<br>", "; "));
	        addContentAndStyle(rowData.createCell(INDEX_CUI_DES),getStyleAccountingAccount(), cuiDes.replace("<br>", "; "));
	        addContentAndStyle(rowData.createCell(INDEX_TITULAR_DESTINO),getStyleAccountingAccount(), titularOrigenDes.replace("<br>", "; "));
	        String mecanismoDes = "";
	        if(mecanismo.equals(GeneralConstants.THREE_VALUE_INTEGER)||mecanismo.equals(new Integer("5"))){
	        	mecanismoDes = "EXTRABURSATILES";
	        }
	        if(mecanismo.equals(GeneralConstants.ONE_VALUE_INTEGER)){
	        	mecanismoDes = "BURSATILES";
	        }
	        if(mecanismo.equals(GeneralConstants.SIX_VALUE_INTEGER)){
	        	mecanismoDes = "INTERCAMBIO DE VALORES";
	        }
	        addContentAndStyle(rowData.createCell(INDEX_MECANISMO),getStyleAccountingAccount(), mecanismoDes);
	        addContentAndStyle(rowData.createCell(INDEX_MODALIDAD),getStyleAccountingAccount(), modalidad);
	        
		}
        

	}

}
