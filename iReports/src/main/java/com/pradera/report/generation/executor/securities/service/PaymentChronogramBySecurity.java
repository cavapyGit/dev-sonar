package com.pradera.report.generation.executor.securities.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.custody.splitcouponstype.SplitCouponStateType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.report.Report;
import com.pradera.report.generation.executor.issuances.to.CouponsDetachmentTO;
import com.pradera.report.generation.executor.issuances.to.PaymentChronogramTO;
import com.pradera.report.generation.executor.securities.to.SecuritiesByIssuanceAndIssuerTO;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class PaymentChronogramBySecurity extends CrudDaoServiceBean{
	
	public PaymentChronogramBySecurity() {
		// TODO Auto-generated constructor stub
	}

	public List<Object[]> getIssuerListByParameters(
			SecuritiesByIssuanceAndIssuerTO securitiesTO) {//TODO
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" SELECT ");
		sbQuery.append(" 	(SELECT (I.business_name ||' - '||I.ID_ISSUER_PK) FROM ISSUER I WHERE I.ID_ISSUER_PK = S.ID_ISSUER_FK) ISSUER, ");
		sbQuery.append(" 	(SELECT (Issua.ID_ISSUANCE_CODE_pK ||' - '||Issua.description) FROM issuance Issua WHERE Issua.ID_ISSUANCE_CODE_pK = S.ID_ISSUANCE_CODE_fK) ISSUANCE, ");
		sbQuery.append(" 	S.ID_ISIN_CODE_PK CODIGO, ");
		sbQuery.append(" 	S.DESCRIPTION, ");
		sbQuery.append(" 	(SELECT PT.PARAMETER_NAME FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK = S.INSTRUMENT_TYPE) INSTRUMENT, ");
		sbQuery.append(" 	S.REGISTRY_DATE, ");
		sbQuery.append(" 	S.ISSUANCE_DATE, ");
		sbQuery.append(" 	S.EXPIRATION_DATE, ");
		sbQuery.append(" 	(SELECT PT.PARAMETER_NAME FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK = S.SECURITY_SOURCE) ORIGEN, ");
		sbQuery.append(" 	(SELECT PT.DESCRIPTION FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK = S.CURRENCY) MONEDA, ");
		sbQuery.append(" 	S.SHARE_CAPITAL, ");
		sbQuery.append(" 	(SELECT PT.PARAMETER_NAME FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK = S.ISSUANCE_FORM) ISSUANCE_FORM, ");
		sbQuery.append(" 	(SELECT PT.PARAMETER_NAME FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK = S.STATE_SECURITY) STATE ");
		sbQuery.append(" FROM	");
		sbQuery.append("  	SECURITY S ");
		sbQuery.append(" WHERE  ");
		  
		if (Validations.validateIsNotNullAndNotEmpty(securitiesTO
				.getFinalDate()) && Validations.validateIsNotNullAndNotEmpty(securitiesTO
						.getInitialDate())) {
			sbQuery.append(" trunc(S.REGISTRY_DATE) BETWEEN :initialDate AND :finalDate ");
		}

		if (Validations.validateIsNotNullAndNotEmpty(securitiesTO
				.getIdIssuerPk())) {
			sbQuery.append(" AND S.ID_ISSUER_FK=:idIssuerPk ");
		}
		if (Validations
				.validateIsNotNullAndNotEmpty(securitiesTO.getIdIssuancePk())) {
			sbQuery.append(" AND S.ID_ISSUANCE_CODE_FK=:idIssuanceFk ");
		}		
		
		sbQuery.append(" ORDER BY ");
		sbQuery.append("  	S.ID_ISSUER_FK,	");
		sbQuery.append("  	S.ID_ISSUANCE_CODE_FK,	");
		sbQuery.append("  	S.ID_ISIN_CODE_PK	");

		Query query = em.createNativeQuery(sbQuery.toString());

		if (Validations.validateIsNotNullAndNotEmpty(securitiesTO
				.getIdIssuerPk())) {
			query.setParameter("idIssuerPk", securitiesTO.getIdIssuerPk());
		}
		if (Validations
				.validateIsNotNullAndNotEmpty(securitiesTO.getIdIssuancePk())) {
			query.setParameter("idIssuanceFk", securitiesTO.getIdIssuancePk());
		}
		if (Validations.validateIsNotNullAndNotEmpty(securitiesTO
				.getInitialDate())) {
			query.setParameter("initialDate", securitiesTO.getInitialDate());
		}
		if (Validations.validateIsNotNullAndNotEmpty(securitiesTO
				.getFinalDate())) {
			query.setParameter("finalDate", securitiesTO.getFinalDate());
		}

		return query.getResultList();
	}

	/*it only returns the query to send to the jasper as PARAMETER*/
	 public String getQueryAssignmentISINAndCFICode(SecuritiesByIssuanceAndIssuerTO secTO){
	    	StringBuilder sbQuery= new StringBuilder();
	    	
	    	sbQuery.append(" select");
			sbQuery.append(" s.id_isin_code,");//0
			sbQuery.append(" s.FISN_CODE, ");
			sbQuery.append(" s.id_security_code_pk,");//1
			sbQuery.append(" s.issuance_date,");//2
			sbQuery.append(" s.expiration_date,");//3
			//sbQuery.append(" NVL(s.initial_nominal_value,0) as initial_nominal_value,");//4
			sbQuery.append(" case when s.security_class =  1945 then NVL(s.CURRENT_NOMINAL_VALUE, 0) ");
			sbQuery.append(" else NVL(s.INITIAL_NOMINAL_VALUE, 0) ");
			sbQuery.append(" END AS initial_nominal_value, ");
			sbQuery.append(" (CASE ");
			sbQuery.append(" 	WHEN (IND_IS_DETACHED = 1 OR IND_IS_COUPON = 1) ");
			sbQuery.append("		THEN NVL(s.share_balance,0) ");
			sbQuery.append("	ELSE NVL((SELECT ROUND(ISS.ISSUANCE_AMOUNT / case when s.security_class =  1945 then NVL(s.CURRENT_NOMINAL_VALUE, 0) else NVL(s.INITIAL_NOMINAL_VALUE, 0)  END,2) FROM ISSUANCE ISS WHERE ISS.ID_ISSUANCE_CODE_PK = s.ID_ISSUANCE_CODE_FK),0) ");
			sbQuery.append(" END) AS share_balance,");
			sbQuery.append(" s.cfi_code,");//6
			sbQuery.append(" s.currency");//7
			sbQuery.append(" from security s ");
			sbQuery.append(" where 1 = 1 ");//s.cfi_code is not null and s.id_isin_code is not null");
			sbQuery.append(" and s.security_class not in (420,1976,415,414,1854,1855,2241,2246,1954) ");
			if(secTO.getStateSecurityCode()!=null&&secTO.getStateSecurityCode().equals(SecurityStateType.REGISTERED.getCode())) {
				sbQuery.append(" and TRUNC(s.registry_date) between TO_DATE(:iniDt,'dd/MM/yyyy') and TO_DATE(:endDt,'dd/MM/yyyy')");
				sbQuery.append(" and s.state_security = "+secTO.getStateSecurityCode());
			}
			if(secTO.getStateSecurityCode()!=null&&secTO.getStateSecurityCode().equals(SecurityStateType.EXPIRED.getCode())) {
				sbQuery.append(" and s.STATE_SECURITY IN (1372,833,834,641,642,643,644,131,2033) ");
				sbQuery.append(" and TRUNC(s.EXPIRATION_DATE) between TO_DATE(:iniDt,'dd/MM/yyyy') and TO_DATE(:endDt,'dd/MM/yyyy')");
			}		
			
			if(secTO.getStateSecurityCode()== null) {
				sbQuery.append(" and s.STATE_SECURITY IN (1372,833,834,641,642,643,644,131,2033) ");
				sbQuery.append(" and TRUNC(s.registry_date) between TO_DATE(:iniDt,'dd/MM/yyyy') and TO_DATE(:endDt,'dd/MM/yyyy')");
				
			}
			
			if(secTO.getIdIsinCode()!=null){
				sbQuery.append(" and s.id_isin_code like :isinCode");
			}
			if(secTO.getIdSecurityCodePk()!=null){
	    		sbQuery.append(" and s.id_security_code_pk=:idSecCode");
	    	}
			if(secTO.getCfiCode()!=null){
	    		sbQuery.append(" and s.cfi_code like :cfiCode");
	    	}
			if(secTO.getSecurityClass()!=null){
	    		sbQuery.append(" and s.security_class = "+secTO.getSecurityClass());
	    	}
			if(secTO.getFsinCode()!=null){
	    		sbQuery.append(" and s.FISN_CODE like :fsinCode");
	    	}
			//sbQuery.append(" order by s.id_isin_code, s.id_security_code_pk");
			
			String strQueryFormatted = null;
	    	strQueryFormatted= sbQuery.toString().replace(":iniDt", "'"+CommonsUtilities.convertDateToString(secTO.getInitialDate(),"dd/MM/yyyy")+"'");
	    	strQueryFormatted= strQueryFormatted.replace(":endDt", "'"+CommonsUtilities.convertDateToString(secTO.getFinalDate(),"dd/MM/yyyy")+"'");
	    	if(secTO.getIdIsinCode()!=null){
	    		strQueryFormatted= strQueryFormatted.replace(":isinCode", "'%"+secTO.getIdIsinCode()+"%'");
			}
			if(secTO.getIdSecurityCodePk()!=null){
				strQueryFormatted= strQueryFormatted.replace(":idSecCode", "'"+secTO.getIdSecurityCodePk()+"'");
	    	}
			if(secTO.getCfiCode()!=null){
				strQueryFormatted= strQueryFormatted.replace(":cfiCode", "'%"+secTO.getCfiCode()+"%'");
	    	}
			if(secTO.getFsinCode()!=null){
				strQueryFormatted= strQueryFormatted.replace(":fsinCode", "'%"+secTO.getFsinCode()+"%'");
	    	}
	    	return strQueryFormatted;
	    }
   
	 public List<PaymentChronogramTO> getCouponsCharacteristics(PaymentChronogramTO couponsTO){
	    	StringBuilder sbQuery= new StringBuilder();
	    	
			sbQuery.append(" WITH  PROGRAM_AMORTIZATION  AS ");
			sbQuery.append(" ( ");
			sbQuery.append(" SELECT ");
			sbQuery.append(" APS.ID_SECURITY_CODE_FK, ");
			sbQuery.append(" PAC.COUPON_AMOUNT, ");
			sbQuery.append(" PAC.EXPRITATION_DATE ");
			sbQuery.append(" FROM "); 
			sbQuery.append(" PROGRAM_AMORTIZATION_COUPON PAC ");
			sbQuery.append(" INNER JOIN ");  
			sbQuery.append(" AMORTIZATION_PAYMENT_SCHEDULE APS ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK=APS.ID_AMO_PAYMENT_SCHEDULE_PK ");
			sbQuery.append(" WHERE ");
			sbQuery.append(" STATE_PROGRAM_AMORTIZATON=1350 ");
			sbQuery.append(" ) ");
			sbQuery.append(" Select ");
			sbQuery.append(" 'EDB' as custodio, ");//0
			sbQuery.append(" TO_CHAR(SEC.REGISTRY_DATE,'yyyy-MM-dd') REGISTRY_DATE, ");//1
			sbQuery.append(" SEC.SECURITY_CLASS, ");//2
			sbQuery.append(" sec.ID_SECURITY_CODE_ONLY, ");//3
			sbQuery.append(" pic.COUPON_NUMBER, ");//4
			sbQuery.append(" TO_CHAR(pic.EXPERITATION_DATE,'yyyy-MM-dd') EXPERITATION_DATE, ");//5
			sbQuery.append(" NVL(ROUND(pac.COUPON_AMOUNT,2),0) AS COUPON_AMOUNT_AMO, ");//6
			sbQuery.append(" NVL(ROUND(pic.COUPON_AMOUNT,2),0) AS COUPON_AMOUNT_INT, ");//7
			sbQuery.append(" (NVL(ROUND(pac.COUPON_AMOUNT,2),0)+NVL(ROUND(pic.COUPON_AMOUNT,2),0)) AS TOTAL ");//8
			sbQuery.append(" from ");
			sbQuery.append(" Security sec "); 
			sbQuery.append(" inner join ");
			sbQuery.append(" INTEREST_PAYMENT_SCHEDULE ips on sec.ID_SECURITY_CODE_PK=ips.ID_SECURITY_CODE_FK ");
			sbQuery.append(" inner join ");
			sbQuery.append(" PROGRAM_INTEREST_COUPON pic on pic.ID_INT_PAYMENT_SCHEDULE_FK=ips.ID_INT_PAYMENT_SCHEDULE_PK  "); 
			sbQuery.append(" left join ");
			sbQuery.append(" AMORTIZATION_PAYMENT_SCHEDULE aps on sec.ID_SECURITY_CODE_PK=aps.ID_SECURITY_CODE_FK ");
			sbQuery.append(" left join ");
			sbQuery.append(" PROGRAM_AMORTIZATION pac on trunc(pic.EXPERITATION_DATE)=trunc(pac.EXPRITATION_DATE) and pac.ID_SECURITY_CODE_FK=ips.ID_SECURITY_CODE_FK ");
			sbQuery.append(" Where 																															");
			//sbQuery.append(" pic.STATE_PROGRAM_INTEREST=1350  and SEC.INTEREST_PAYMENT_MODALITY<>1527  and sec.ind_is_coupon=0  and sec.ind_is_detached=0 	");
			sbQuery.append(" SEC.INTEREST_PAYMENT_MODALITY<>1527  and sec.ind_is_coupon=0  and sec.ind_is_detached=0 	");
			sbQuery.append(" and trunc(sec.registry_date)=to_date(:date, 'dd/MM/yyyy')																		");
			sbQuery.append(" AND SEC.SECURITY_CLASS IN (1936,409,420,2352) ");
			
			sbQuery.append(" order by sec.id_security_code_only, pic.coupon_number");
			
			Query query = em.createNativeQuery(sbQuery.toString()); 

			query.setParameter("date", CommonsUtilities.convertDatetoString(couponsTO.getDate()));
			
			PaymentChronogramTO cc= null;
			List<Object[]> lstObjects = query.getResultList();
			List<PaymentChronogramTO> lstCC = new ArrayList<PaymentChronogramTO>();
			for (int i = 0; i < lstObjects.size(); i++) {
				cc= new PaymentChronogramTO();
				Object[] objList = (Object[]) lstObjects.get(i);
				cc.setCustodian(objList[0].toString());
				cc.setRegistryDate(objList[1].toString());
				cc.setSecurityClass(((BigDecimal)objList[2]).intValue());
				cc.setIdSecurityOnly(objList[3].toString());
				cc.setCouponNumberInt(((BigDecimal)objList[4]).intValue());
				cc.setExpirationDate(objList[5].toString());
				cc.setAmortizationFactor((BigDecimal) objList[6]);
				cc.setInterestFactor((BigDecimal)objList[7]);
				cc.setTotalCoupon((BigDecimal)objList[8]);
				lstCC.add(cc);
			}
	    	return lstCC;
	    }
    
    public String getPaymentChronogram(PaymentChronogramTO couponsTO){
    	StringBuilder sbQuery= new StringBuilder();
    	
    	sbQuery.append(" select i.mnemonic,i.business_name, ");
    	sbQuery.append(" s.id_security_code_pk, s.initial_nominal_value, s.security_days_term, s.interest_rate, ");
    	sbQuery.append(" pis.coupon_number, pis.experitation_date, pis.interest_rate,pis.coupon_amount, ");
    	sbQuery.append(" 'INT' as ind ");
    	sbQuery.append(" from security s ");
    	sbQuery.append(" inner join issuer i on s.id_issuer_fk=i.id_issuer_pk ");
    	sbQuery.append(" inner join interest_payment_schedule ips on s.id_security_code_pk=ips.id_security_code_fk ");
    	sbQuery.append(" inner join program_interest_coupon pis on ips.id_int_payment_schedule_pk=pis.id_int_payment_schedule_fk ");
    	sbQuery.append(" where 1=1 ");
    	if(Validations.validateIsNotNull(couponsTO.getStrState())){
    		sbQuery.append(" and i.state_issuer=" + couponsTO.getStrState() + " ");
    	}
    	if(Validations.validateIsNotNull(couponsTO.getStrEconomicActivity())){
    		sbQuery.append(" and i.economic_activity=" + couponsTO.getStrEconomicActivity() + " ");
    	}
		sbQuery.append(" UNION ALL ");
		sbQuery.append(" select i.mnemonic,i.business_name, ");
		sbQuery.append(" s.id_security_code_pk, s.initial_nominal_value, s.security_days_term, s.interest_rate, ");
		sbQuery.append(" pac.coupon_number, pac.expritation_date, pac.AMORTIZATION_AMOUNT,pac.coupon_amount, ");
		sbQuery.append(" 'AMORT' as ind ");
		sbQuery.append(" from security s ");
		sbQuery.append(" inner join issuer i on s.id_issuer_fk=i.id_issuer_pk ");
		sbQuery.append(" inner join amortization_payment_schedule aps on s.id_security_code_pk=aps.id_security_code_fk ");
		sbQuery.append(" inner join program_amortization_coupon pac on aps.id_amo_payment_schedule_pk=pac.id_amo_payment_schedule_fk ");
    	sbQuery.append(" where 1=1 ");
    	if(Validations.validateIsNotNull(couponsTO.getStrState())){
    		sbQuery.append(" and i.state_issuer=" + couponsTO.getStrState() + " ");
    	}
    	if(Validations.validateIsNotNull(couponsTO.getStrEconomicActivity())){
    		sbQuery.append(" and i.economic_activity=" + couponsTO.getStrEconomicActivity() + " ");
    	}
		sbQuery.append(" order by mnemonic, id_security_code_pk, coupon_number ");
		
		
    	return sbQuery.toString();
    }
    
    /*it only returns the query to send to the jasper as PARAMETER*/
    public String getExpRelatOfSecByIssuer(GenericsFiltersTO gfTO){
    	StringBuilder sbQuery= new StringBuilder();
    	
    	sbQuery.append(" select");
		sbQuery.append(" s.currency,");
		sbQuery.append(" s.expiration_date,");
		if(gfTO.getIndBCB().equals(BooleanType.YES.getCode().toString())){
			sbQuery.append(" s.ID_SECURITY_CODE_PK as ID_SECURITY_CODE_PK,");
		}else{
			sbQuery.append(" (SELECT TEXT3 FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK=s.SECURITY_CLASS) ||'-'|| s.ID_SECURITY_BCB_CODE as ID_SECURITY_CODE_PK,");
		}
		sbQuery.append(" s.description,");
		sbQuery.append(" NVL(s.initial_nominal_value,0) as initial_nominal_value,");
		sbQuery.append(" s.issuance_date,");
		sbQuery.append(" NVL(s.interest_rate,0) as interest_rate,");
		sbQuery.append(" h.id_holder_pk,");
		sbQuery.append(" h.full_name,");
		sbQuery.append(" h.document_type,");
		sbQuery.append(" h.document_number,");
		sbQuery.append(" DECODE(h.ind_residence,1,'RE','NR') as ind_residence,");
		sbQuery.append(" ha.account_number,");
		sbQuery.append(" hab.TOTAL_BALANCE,");
		sbQuery.append(" decode(nvl(hab.accreditation_balance,0)+(nvl(hab.BAN_BALANCE,0)+nvl(hab.OTHER_BLOCK_BALANCE,0)+nvl(hab.PAWN_BALANCE,0)),0,'NO','SI') has_lock,");
		sbQuery.append(" pic.COUPON_NUMBER,");
		sbQuery.append(" i.BUSINESS_NAME,");
		sbQuery.append(" i.MNEMONIC,");
		sbQuery.append(" (NVL(pac.COUPON_AMOUNT,0)+NVL(pic.COUPON_AMOUNT,0)) AS TOTAL_COUPONS_AMOUNT");
		sbQuery.append(" from security s");
		sbQuery.append(" inner join ISSUER i on s.id_issuer_fk=i.id_issuer_pk");
		sbQuery.append(" left join interest_payment_schedule ips on s.id_security_code_pk=ips.id_security_code_fk");
		sbQuery.append(" left join program_interest_coupon pic on ips.id_int_payment_schedule_pk=pic.id_int_payment_schedule_fk");
		sbQuery.append(" left join AMORTIZATION_PAYMENT_SCHEDULE APS ON s.id_security_code_pk=aps.id_security_code_fk");
		sbQuery.append(" left join PROGRAM_AMORTIZATION_COUPON PAC ON APS.ID_AMO_PAYMENT_SCHEDULE_PK=PAC.ID_AMO_PAYMENT_SCHEDULE_FK");
		sbQuery.append(" inner join HOLDER_ACCOUNT_BALANCE hab on s.ID_SECURITY_CODE_PK=hab.ID_SECURITY_CODE_PK");
		sbQuery.append(" inner join holder_account ha on hab.ID_HOLDER_ACCOUNT_PK= ha.ID_HOLDER_ACCOUNT_PK");
		sbQuery.append(" inner join holder_account_detail had on ha.ID_HOLDER_ACCOUNT_PK=had.ID_HOLDER_ACCOUNT_FK");
		sbQuery.append(" inner join holder h on had.ID_HOLDER_FK=h.id_holder_pk");
		sbQuery.append(" where had.ind_representative=:indRep and pic.EXPERITATION_DATE=s.expiration_date");
		sbQuery.append(" and trunc(s.EXPIRATION_DATE) between TO_DATE(:iniDt,'dd/MM/yyyy') and TO_DATE(:endDt,'dd/MM/yyyy')");
		if(gfTO.getIndBCB().equals("2")){
			sbQuery.append(" and s.ID_SECURITY_BCB_CODE IS NOT NULL");
		}
		if(gfTO.getCui()!=null){
			sbQuery.append(" and h.id_holder_pk= :cui");
		}
		if(gfTO.getIdHolderAccountPk()!=null){
    		sbQuery.append(" and ha.id_holder_account_pk=:accountN");
    	}
		if(gfTO.getIdIssuer()!=null){
    		sbQuery.append(" and s.ID_ISSUER_FK= :idIssuer");
    	}
		if(gfTO.getIdSecurityCodePk()!=null){
			sbQuery.append(" and s.ID_SECURITY_CODE_PK=:idSecCode");
		}
    	
		sbQuery.append(" order by s.ID_ISSUER_FK, s.currency, s.expiration_date, s.ID_SECURITY_CODE_PK, h.id_holder_pk");
		
		String strQueryFormatted = null;
    	strQueryFormatted= sbQuery.toString().replace(":iniDt", "'"+gfTO.getInitialDt()+"'");
    	strQueryFormatted= strQueryFormatted.replace(":endDt", "'"+gfTO.getFinalDt()+"'");
    	strQueryFormatted= strQueryFormatted.replace(":indRep", BooleanType.YES.getCode().toString());
//    	strQueryFormatted= strQueryFormatted.replace(":securityClass", SecurityClassType.CUP.getCode().toString());
    	if(gfTO.getCui()!=null){
    		strQueryFormatted= strQueryFormatted.replace(":cui", gfTO.getCui());
		}
		if(gfTO.getIdHolderAccountPk()!=null){
			strQueryFormatted= strQueryFormatted.replace(":accountN", gfTO.getIdHolderAccountPk());
    	}
		if(gfTO.getIdIssuer()!=null){
			strQueryFormatted= strQueryFormatted.replace(":idIssuer", "'"+gfTO.getIdIssuer()+"'");
    	}
		if(gfTO.getIdSecurityCodePk()!=null){
			strQueryFormatted= strQueryFormatted.replace(":idSecCode", "'"+gfTO.getIdSecurityCodePk()+"'");
    	}
    	
    	return strQueryFormatted;
    }
    
    public List<PaymentChronogramTO> getDetachedCouponsCharact(GenericsFiltersTO filtersTO){
    	StringBuilder sbQuery= new StringBuilder();
    	
    	sbQuery.append(" select Distinct ");
		sbQuery.append(" 'EDB' as custodio,");//0
		sbQuery.append(" trunc(sec.registry_date),");//1
		sbQuery.append(" s.security_class,");//2
		sbQuery.append(" s.id_security_code_only,");//3
		sbQuery.append(" pic.coupon_number,");//4
		sbQuery.append(" (SELECT COUNT(*) FROM SPLIT_COUPON_DETAIL SCD WHERE SCD.ID_PROGRAM_INTEREST_FK=pic.ID_PROGRAM_INTEREST_PK) as is_coupon,");//5
		sbQuery.append(" sco.ID_SUBPRODUCT_CODE_FK,");//6
		sbQuery.append(" s.ID_SECURITY_CODE_PK");//7 
		sbQuery.append(" from security s");
		sbQuery.append(" inner join SPLIT_COUPON_OPERATION sco on sco.ID_SECURITY_CODE_FK=s.ID_SECURITY_CODE_PK");
		sbQuery.append(" inner join security sec on sec.ID_SECURITY_CODE_PK  = sco.ID_SUBPRODUCT_CODE_FK		"); 
		sbQuery.append(" inner join interest_payment_schedule ips on s.id_security_code_pk=ips.id_security_code_fk");
		sbQuery.append(" inner join program_interest_coupon pic on ips.id_int_payment_schedule_pk=pic.id_int_payment_schedule_fk");
		sbQuery.append(" where s.IND_HAS_SPLIT_SECURITIES=1 and s.IND_SPLIT_COUPON=1 and sco.request_state=:state");
		sbQuery.append(" and pic.STATE_PROGRAM_INTEREST=:stateCoupon and trunc(sec.registry_date)=to_date(:date, 'dd/MM/yyyy')");
		sbQuery.append(" order by s.ID_SECURITY_CODE_PK, pic.coupon_number");
		
		Query query = em.createNativeQuery(sbQuery.toString());
 
		query.setParameter("date", filtersTO.getDate());
		query.setParameter("state", SplitCouponStateType.CONFIRMED.getCode());
		query.setParameter("stateCoupon", ProgramScheduleStateType.PENDING.getCode());
		
		PaymentChronogramTO cc= null;
		List<Object[]> lstObjects = query.getResultList();
		List<PaymentChronogramTO> lstObjectsAux = new ArrayList<PaymentChronogramTO>();
		String strSecurities = "";
		int cont = 0;
		List<String> lstCouponsDetached = new ArrayList<String>();
    	List<String> lstCouponsIndexed = new ArrayList<String>();
		
		for (int i = 0; i < lstObjects.size(); i++) {
			Object[] objListAux = (Object[]) lstObjects.get(i);
			if(strSecurities.equals(GeneralConstants.EMPTY_STRING)){
				cc= new PaymentChronogramTO();
				cc.setCustodian(objListAux[0].toString());
				cc.setRegistryDt((Date)objListAux[1]);
				cc.setSecurityClass(((BigDecimal)objListAux[2]).intValue());
				cc.setIdSecurityOnly(objListAux[6].toString().substring(4, objListAux[6].toString().length()));
				if(Integer.valueOf(objListAux[5].toString())>=BooleanType.YES.getCode()){
					lstCouponsDetached.add(((BigDecimal)objListAux[4]).intValue()+";"); 
				}else{
					lstCouponsIndexed.add(((BigDecimal)objListAux[4]).intValue()+";"); 
				}
				lstObjectsAux.add(cc);
			}else{
				if(strSecurities.equals(objListAux[6].toString())){
					if(Integer.valueOf(objListAux[5].toString())>=BooleanType.YES.getCode()){
						lstCouponsDetached.add(((BigDecimal)objListAux[4]).intValue()+";");  
					}else{ 
		    			lstCouponsIndexed.add(((BigDecimal)objListAux[4]).intValue()+";"); 
					}
				}else{
					cc= new PaymentChronogramTO();
					lstCouponsDetached = new ArrayList<String>();
			    	lstCouponsIndexed = new ArrayList<String>();
					cc.setCustodian(objListAux[0].toString());
					cc.setRegistryDt((Date)objListAux[1]);
					cc.setSecurityClass(((BigDecimal)objListAux[2]).intValue());
					cc.setIdSecurityOnly(objListAux[6].toString().substring(4, objListAux[6].toString().length()));
					if(Integer.valueOf(objListAux[5].toString())>=BooleanType.YES.getCode()){
						lstCouponsDetached.add(((BigDecimal)objListAux[4]).intValue()+";"); 
					}else{ 
		    			lstCouponsIndexed.add(((BigDecimal)objListAux[4]).intValue()+";"); 
					}
					cont++;
					lstObjectsAux.add(cc);
		    	}
			}

			String strDetachedCoupons = lstCouponsDetached.toString().replace(", ", "");
			String strIndexedCoupons = lstCouponsIndexed.toString().replace(", ", "");

			lstObjectsAux.get(cont).setStrCouponNumbers(strDetachedCoupons);
    		lstObjectsAux.get(cont).setValidCoupon(strIndexedCoupons);	

			strSecurities = objListAux[6].toString();
		}
    	return lstObjectsAux;
    }
    
    /*it only returns the query to send to the jasper as PARAMETER*/
	public String getConstancyOfDetachment(CouponsDetachmentTO secTO){
    	StringBuilder sbQuery= new StringBuilder();
    	
    	sbQuery.append(" select");
    	sbQuery.append(" 	s.ID_SECURITY_CODE_PK,");//0
		sbQuery.append(" 	sco.ID_SUBPRODUCT_CODE_FK,");//1
		sbQuery.append(" 	I_CUP.ID_SPLIT_SECURITY_CODE_FK,");//2
		sbQuery.append(" 	sco.id_split_operation_pk, ");//3
		sbQuery.append(" 	sco.request_state,");//4
		sbQuery.append(" 	sco.last_modify_date,");//5
		sbQuery.append(" 	'('||p.mnemonic||') '||p.description as participant,");//6
		sbQuery.append(" 	'('||h.id_holder_pk||') '||h.full_name as cui,");//7
		sbQuery.append(" 	(select circulation_balance from security se where s.id_ref_security_code_fk=se.id_security_code_pk) as circulation_balance_origin,");//8
		sbQuery.append(" 	SDET.operation_quantity,");//9
		sbQuery.append(" 	sco.comments");//10
		sbQuery.append(" from SPLIT_COUPON_OPERATION sco ");
		sbQuery.append(" 	Inner join SECURITY s On s.id_security_code_pk = sco.ID_SECURITY_CODE_FK");
		sbQuery.append(" 	INNER JOIN SPLIT_COUPON_DETAIL SDET ON SDET.ID_SPLIT_OPERATION_FK = sco.ID_SPLIT_OPERATION_PK");
		sbQuery.append(" 	INNER JOIN PROGRAM_INTEREST_COUPON I_CUP ON I_CUP.ID_PROGRAM_INTEREST_PK = SDET.ID_PROGRAM_INTEREST_FK");
		sbQuery.append(" 	inner join PARTICIPANT p on sco.id_participant_fk=p.id_participant_pk");
		sbQuery.append(" 	inner join HOLDER h on sco.id_holder_fk=h.id_holder_pk");
		sbQuery.append(" where 1=1");//isCoupon
		sbQuery.append(" 	and sco.request_state = 1746 ");
		if(secTO.getIdHolderAccountPk()!=null){
			sbQuery.append(" and sco.id_holder_account_fk= :accountN");
		}
		if(secTO.getCui()!=null){
    		sbQuery.append(" and sco.id_holder_fk=:cui");
    	}
		if(secTO.getDateChangeState()!=null){
    		sbQuery.append(" and trunc(sco.last_modify_date)= to_date(:changeStateDt)");
    	}
		if(secTO.getIdSecurityCodePk()!=null){
			sbQuery.append(" and s.id_security_code_pk= :idSecurityCode");
		}
		if(secTO.getParticipantCode()!=null){
    		sbQuery.append(" and sco.id_participant_fk=:idParticipant");
    	}
		if(secTO.getRequestNumber()!=null){
    		sbQuery.append(" and sco.id_split_operation_pk= :requestDetachment");
    	}
		if(secTO.getRequestState()!=null){
    		sbQuery.append(" and sco.request_state=:requestState");
    	}
		sbQuery.append(" order by s.ID_REF_SECURITY_CODE_FK, s.id_security_code_pk, I_CUP.ID_SPLIT_SECURITY_CODE_FK ");
		
		String strQueryFormatted = sbQuery.toString();
    	if(secTO.getIdHolderAccountPk()!=null){
    		strQueryFormatted= strQueryFormatted.replace(":accountN", secTO.getIdHolderAccountPk());
		}
		if(secTO.getCui()!=null){
			strQueryFormatted= strQueryFormatted.replace(":cui", secTO.getCui());
    	}
		if(secTO.getDateChangeState()!=null){
			strQueryFormatted= strQueryFormatted.replace(":changeStateDt", "'"+secTO.getDateChangeState()+"'");
    	}
		if(secTO.getIdSecurityCodePk()!=null){
    		strQueryFormatted= strQueryFormatted.replace(":idSecurityCode", secTO.getIdSecurityCodePk());
		}
		if(secTO.getParticipantCode()!=null){
			strQueryFormatted= strQueryFormatted.replace(":idParticipant", secTO.getParticipantCode());
    	}
		if(secTO.getRequestNumber()!=null){
			strQueryFormatted= strQueryFormatted.replace(":requestDetachment", secTO.getRequestNumber());
    	}
		if(secTO.getRequestState()!=null){
    		strQueryFormatted= strQueryFormatted.replace(":requestState", secTO.getRequestState());
		}

    	return strQueryFormatted;
    }
	
	@SuppressWarnings("unchecked")
	public List<SecuritiesByIssuanceAndIssuerTO> getSecurityCharacteristics(SecuritiesByIssuanceAndIssuerTO charactsecTO){
		Query query = em.createNativeQuery(((Report)find(Report.class, charactsecTO.getIdReport())).getQueryReport());
		query.setParameter("date", CommonsUtilities.convertDatetoString(charactsecTO.getDate()));
		query.setParameter("motive", charactsecTO.getMotive());
		query.setParameter("stateAnnot", charactsecTO.getStateAnnotation());
		
		List<Object[]> lstObjects = query.getResultList();
		 
		SecuritiesByIssuanceAndIssuerTO cc= null;
		List<SecuritiesByIssuanceAndIssuerTO> lstCC = new ArrayList<SecuritiesByIssuanceAndIssuerTO>();
		for (int i = 0; i < lstObjects.size(); i++) {
			cc= new SecuritiesByIssuanceAndIssuerTO();
			Object[] objList = (Object[]) lstObjects.get(i);
			if(Validations.validateIsNotNull(objList[0])){
				cc.setSecurityClass(((BigDecimal)objList[0]).intValue());
			}
			if(Validations.validateIsNotNull(objList[1])){
				cc.setIdIssuerPk(objList[1].toString().trim());
			}
			if(Validations.validateIsNotNull(objList[2])){
				cc.setIdSecurityOnly(objList[2].toString().trim());
			}
			
			if(Validations.validateIsNotNull(objList[3])){
				cc.setCustodian(objList[3].toString().trim());
			}
			/**Fecha de la cual se requiere el reporte*/
			if(Validations.validateIsNotNull(objList[4])){
				cc.setDateInformation((Date)objList[4]);
			}
			if(Validations.validateIsNotNull(objList[5])){
				cc.setRegistryDt((Date)objList[5]);
			}
			if(Validations.validateIsNotNull(objList[6])){
				cc.setMnemonic(objList[6].toString().trim());
			}
			if(Validations.validateIsNotNull(objList[7])){
				cc.setCurrency(((BigDecimal)objList[7]).intValue());
			}
			if(Validations.validateIsNotNull(objList[8])){
				cc.setIninomval(((BigDecimal)objList[8]));
			}
			if(Validations.validateIsNotNull(objList[9])){
				cc.setIssuancedate((Date)objList[9]);
			}
			if(Validations.validateIsNotNull(objList[10])){
				cc.setExpirationDtInt((Date)objList[10]);
			}
			//--issue 578 reporte caracteristicas valor B 
			if(Validations.validateIsNotNull(objList[11])){
				cc.setIssuanceAmount((BigDecimal)objList[11]);
			}
			if(Validations.validateIsNotNull(objList[12])){
				cc.setMaximumInvestment((BigDecimal)objList[12]);
			}
			//--
			if(Validations.validateIsNotNull(objList[13])){
				cc.setShareBalance(((BigDecimal)objList[13]).intValue());
			}
			if(Validations.validateIsNotNull(objList[14])){
				cc.setMarketrate(((BigDecimal)objList[14]));
			}
			if(Validations.validateIsNotNull(objList[15])){
				cc.setDiscountrate(((BigDecimal)objList[15]));
			}
			Integer couponsAmount=((BigDecimal)objList[16]).intValue();
			if(Validations.validateIsNotNullAndPositive(couponsAmount)){ 
				cc.setCouponNumberInt(couponsAmount);
				//cc.setCouponNumberInt(BooleanType.YES.getCode());
			}else{
				cc.setCouponNumberInt(BooleanType.NO.getCode());
			}
			if(Validations.validateIsNotNull(objList[17])){
				cc.setAmortizationType(((BigDecimal)objList[17]).intValue());
			}
			if(Validations.validateIsNotNull(objList[18])){
				cc.setPeriodicity(objList[18].toString().trim());
			}
			if(Validations.validateIsNotNull(objList[19])){
				cc.setNegotiatingmanner(objList[19].toString().trim());
			}
			if(Validations.validateIsNotNull(objList[20])){
				cc.setMarketmoney(objList[20].toString().trim());
			}
			
			if(objList[21]!=null){
				cc.setParticipantPlacement((String) objList[21]);
			}
			
			lstCC.add(cc);
		}
    	return lstCC;
    }
}