package com.pradera.report.generation.facade;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.io.IOUtils;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSProcessable;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.report.ReportFile;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.services.remote.reports.ReportGenerationService;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.FileData;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.report.generation.service.CMSProcessableRange;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfDate;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfPKCS7;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignature;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfString;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Project iDepositary. Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ReportGenerationServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 09/09/2015
 */
@Stateless
@Remote(ReportGenerationService.class)
public class ReportGenerationServiceFacade implements ReportGenerationService {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6495143512962244545L;

	/** The report generation local service facade. */
	@EJB
	ReportGenerationLocalServiceFacade reportGenerationLocalServiceFacade;
	
	@Inject @Configurable
	String reportLogo;

	/**
	 * Save report execution.
	 *
	 * @param reportId         the report id
	 * @param parameters       the parameters
	 * @param parameterDetails the parameter details
	 * @param reportUser       the report user who have grant to see report
	 * @param loggerUser       the logger user
	 * @return the id of report logger generate
	 * @throws ServiceException the service exception
	 */
	@Override
	public Long saveReportExecution(Long reportId, Map<String, String> parameters, Map<String, String> parameterDetails,
			ReportUser reportUser, LoggerUser loggerUser) throws ServiceException {
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
		ReportLogger reportLogger = reportGenerationLocalServiceFacade.saveReportExecution(reportId, parameters,
				parameterDetails, reportUser);
		return reportLogger.getIdReportLoggerPk();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pradera.commons.services.remote.reports.ReportGenerationService#
	 * getDataReport(java.lang.Long)
	 */
	@Override
	public byte[] getDataReport(Long reportLoggerId) throws ServiceException {
		return reportGenerationLocalServiceFacade.getDataReport(reportLoggerId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pradera.commons.services.remote.reports.ReportGenerationService#
	 * saveReportFile(java.lang.Long, com.pradera.commons.report.ReportUser,
	 * java.util.Map, java.util.Map, com.pradera.integration.contextholder.LoggerUser,
	 * java.util.List)
	 */
	@Override
	public Long saveReportFile(Long reportId, ReportUser reportUser, Map<String, String> parameters,
			Map<String, String> parameterDetails, LoggerUser loggerUser, List<ReportFile> files)
			throws ServiceException {
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
		ReportLogger reportLogger = reportGenerationLocalServiceFacade.saveReportFile(reportId, parameters,
				parameterDetails, files, reportUser);
		return reportLogger.getIdReportLoggerPk();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pradera.commons.services.remote.reports.ReportGenerationService#
	 * setAccreditationCertificate(java.lang.Long, java.lang.Long, java.lang.String,
	 * com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public void setAccreditationCertificate(Long reportId, Long accreditationId, String username, LoggerUser loggerUser)
			throws ServiceException {
		reportGenerationLocalServiceFacade.setAccreditationCertificate(reportId, accreditationId, username, loggerUser);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pradera.commons.services.remote.reports.ReportGenerationService#
	 * sendModifyingOfData(java.lang.Long, java.lang.Long, java.lang.String,
	 * com.pradera.integration.contextholder.LoggerUser, java.lang.String)
	 */
	@Override
	public void sendModifyingOfData(Long reportId, Long holderRequestId, String username, LoggerUser loggerUser,
			String nameParam) throws ServiceException {
		reportGenerationLocalServiceFacade.sendModifyingOfData(reportId, holderRequestId, username, loggerUser,
				nameParam);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pradera.commons.services.remote.reports.ReportGenerationService#
	 * sendReportParticipantUnification(java.lang.Long, java.lang.Long,
	 * com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public void sendReportParticipantUnification(Long reportId, Long idSourceParticipant, LoggerUser loggerUser)
			throws ServiceException {
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
		Map<String, String> param = new HashMap<String, String>();
		Map<String, String> parameterDetails = new HashMap<String, String>();

		param.put("idSourceParticipantPk", idSourceParticipant.toString());
		param.put("idReportPk", reportId.toString());

		ReportUser reportUser = new ReportUser();
		reportUser.setUserName(loggerUser.getUserName());
		reportUser.setReportFormat(ReportFormatType.PDF.getCode());
		// how report is send for screen so for default send notification
		reportUser.setShowNotification(BooleanType.YES.getCode());

		reportGenerationLocalServiceFacade.saveReportExecution(reportId, param, parameterDetails, reportUser);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pradera.commons.services.remote.reports.ReportGenerationService#
	 * saveReportInterfaces(java.lang.Long, java.util.Map, java.util.Map,
	 * com.pradera.commons.report.ReportUser,
	 * com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public Long saveReportInterfaces(Long reportId, Map<String, String> parameters,
			Map<String, String> parameterDetails, ReportUser reportUser, LoggerUser loggerUser)
			throws ServiceException {
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
		ReportLogger objReportLogger = reportGenerationLocalServiceFacade.saveReportExecution(reportId, parameters,
				parameterDetails, reportUser);
		return objReportLogger.getIdReportLoggerPk();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pradera.commons.services.remote.reports.ReportGenerationService#
	 * getDataReport(java.lang.Long)
	 */
	@Override
	public Object[] getDataReportObject(Long reportLoggerId) throws ServiceException {
		return reportGenerationLocalServiceFacade.getDataReportObject(reportLoggerId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pradera.commons.services.remote.reports.ReportGenerationService#
	 * sendFtpUpload(java.util.List, java.lang.Integer)
	 */
	@Override
	public boolean sendFtpUpload(List<FileData> listFileData, Integer parameterTablePk) throws IOException {
		return reportGenerationLocalServiceFacade.sendFtpUpload(listFileData, parameterTablePk);
	}

	/*
	 * (non-Javadoc) REPORTE DE CARTERA BUSQUEDA DE PERSONAS
	 */
	@Override
	public void sendApplicantConsult(Long reportId, Long holderPk, Long idApplicantConsultRequest, String holderDesc,
			String username, LoggerUser loggerUser) throws ServiceException {
		reportGenerationLocalServiceFacade.sendApplicantConsult(reportId, holderPk, idApplicantConsultRequest,
				holderDesc, username, loggerUser);
	}

	@Override
	public byte[] signPdfDocument(byte[] pdfDoc, int pagNum, PrivateKey key, Certificate[] chain, int x, int y) {
		//Security.addProvider(new BouncyCastleProvider());
		//PdfSignature dic = new PdfSignature(PdfName.ADOBE_PPKLITE, PdfName.ADBE_PKCS7_DETACHED);
		//dic.setName(PdfPKCS7.getSubjectFields((X509Certificate) chain1[0]).getField("CN"));

		PdfReader pdfReader;
		try {
			Security.addProvider(new BouncyCastleProvider());
			
			pdfReader = new PdfReader(new ByteArrayInputStream(pdfDoc));
			ByteArrayOutputStream fout=new ByteArrayOutputStream();
			
			com.itextpdf.text.pdf.PdfStamper pdfStamper;
			pdfStamper = PdfStamper.createSignature(pdfReader, fout, '\0',null,true);
			PdfSignatureAppearance sap = pdfStamper.getSignatureAppearance();
			
			
			InputStream inputIO = this.getClass().getResourceAsStream(reportLogo);
			byte[] bytes = IOUtils.toByteArray(inputIO);
            Image image = Image.getInstance(bytes);
	        image.scaleToFit(40,40);
	        image.setAbsolutePosition(50, 10);
	        PdfContentByte contentByte = pdfStamper.getOverContent(pdfReader.getNumberOfPages());
	        contentByte.addImage(image);
	        BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA_BOLD, BaseFont.WINANSI, BaseFont.EMBEDDED);
	        contentByte.saveState();
	        contentByte.beginText();
	        contentByte.setColorFill(new BaseColor(128,128,128));
	        contentByte.setFontAndSize(bf, 5); 
	        String signName = PdfPKCS7.getSubjectFields((X509Certificate) chain[0]).getField("CN");
			String office = PdfPKCS7.getSubjectFields((X509Certificate) chain[0]).getField("T");
	        String dateString = CommonsUtilities.convertDateToString(CommonsUtilities.currentDate(),CommonsUtilities.DATE_PATTERN);
	        contentByte.showTextAligned(Element.ALIGN_LEFT,signName, x, y, 0f);
	        contentByte.showTextAligned(Element.ALIGN_LEFT,office, x, y, 0f);
	        contentByte.showTextAligned(Element.ALIGN_LEFT,dateString, x, y, 0f);
	        contentByte.showTextAligned(Element.ALIGN_LEFT,"La Paz - Bolivia", x, y, 0f);
	        contentByte.saveState();
	        contentByte.endText();
	        contentByte.restoreState();
	        
			sap.setSignDate(new GregorianCalendar());
			sap.setCrypto(null, chain, null, null);
			sap.setAcro6Layers(true);
			PdfSignature dic = new PdfSignature(PdfName.ADOBE_PPKLITE, PdfName.ADBE_PKCS7_DETACHED);
			dic.setDate(new PdfDate(sap.getSignDate()));
			dic.setName(PdfPKCS7.getSubjectFields((X509Certificate) chain[0]).getField("CN"));
			/*if (sap.getReason() != null) {
				dic.setReason(sap.getReason());
			}
			if (sap.getLocation() != null) {
				dic.setLocation(sap.getLocation());
			}*/
			sap.setCryptoDictionary(dic);
			int csize = 4000;
			HashMap exc = new HashMap();
			exc.put(PdfName.CONTENTS, new Integer(csize * 2 + 2));
			sap.preClose(exc);

			CMSSignedDataGenerator generator = new CMSSignedDataGenerator();
			generator.addSigner(key, (X509Certificate) chain[0], CMSSignedDataGenerator.DIGEST_SHA256);

			ArrayList listCert = new ArrayList();
			for (int i = 0; i < chain.length; i++) {
				listCert.add(chain[i]);
			}
			CertStore chainStore;
				chainStore = CertStore.getInstance("Collection", new CollectionCertStoreParameters(listCert),
						"BC");
				generator.addCertificatesAndCRLs(chainStore);
				CMSProcessable content = new com.pradera.report.generation.service.CMSProcessableRange(sap);
				CMSSignedData signedData = generator.generate(content, false, "BC");
				byte[] pk = signedData.getEncoded();
				byte[] outc = new byte[csize];
				PdfDictionary dic2 = new PdfDictionary();
				System.arraycopy(pk, 0, outc, 0, pk.length);
				dic2.put(PdfName.CONTENTS, new PdfString(outc).setHexWriting(true));
				sap.close(dic2);
			//pdfStamper.close();
			byte[] data = fout.toByteArray();
			return data;
	}catch (Exception e) {
		// TODO: handle exception
	}
	return null;
	}
}
