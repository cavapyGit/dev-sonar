package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;

import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;


//issue 632
@ReportProcess(name = "EntriesVoluntaryAccountParticipantRequestReport")
public class EntriesVoluntaryAccountParticipantRequestReport extends GenericReport{
	
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ParameterServiceBean parameterService;
	@EJB
	private CustodyReportServiceBean custodyReportServiceBean;
	
	@Override
	public Map<String, Object> getCustomJasperParameters(){
		
		Map<String, Object> parametersRequired= new HashMap<String, Object>();
		
		ParameterTableTO  filter = new ParameterTableTO();
		
		Map<Integer,String> map_currency = new HashMap<Integer, String>();
		
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		
		String participant_filter = null;
		
		
		// DATOS DE ENTRADA DEL SISTEMA
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.PARTICIPANT_PARAM)){
				if(r.getFilterValue() != null){
					participant_filter = r.getFilterDescription();
				}else{
					for (ReportLoggerDetail t : loggerDetails) {
						if (t.getFilterName().equals(ReportConstant.ISSUER_PARAM)){
							if(t.getFilterValue() != null){
								participant_filter = custodyReportServiceBean.getIssuerDetails(t.getFilterValue());
							}else{
								participant_filter = "TODOS";
							}
						}
					}
				}
			}
		}
		
		filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
			map_currency.put(param.getParameterTablePk(), param.getDescription());
		}
		
		parametersRequired.put("participant_filter", participant_filter);
		parametersRequired.put("map_currency", map_currency);
		
		return parametersRequired;
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		return null;
	}
}
