package com.pradera.report.generation.executor.corporative;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.corporative.service.CorporativeReportServiceBean;

@ReportProcess(name = "PaymentSubscriptionRepMarReport")
public class PaymentSubscriptionRepMarReport extends GenericReport{

	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	private CorporativeReportServiceBean corporativeReportServiceBean;

	@Inject
	private PraderaLogger log;

	public PaymentSubscriptionRepMarReport() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {

		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> currency = new HashMap<Integer, String>();
		Map<Integer,String> state = new HashMap<Integer, String>();
		
		try {
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				currency.put(param.getParameterTablePk(), param.getParameterName());
			}
			filter.setMasterTableFk(MasterTableType.CORPORATIVE_PROCESS_STATE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				state.put(param.getParameterTablePk(), param.getParameterName());
			}
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		parametersRequired.put("p_currency", currency);
		parametersRequired.put("p_state", state);
		
		return parametersRequired;
		
		
	}
}
