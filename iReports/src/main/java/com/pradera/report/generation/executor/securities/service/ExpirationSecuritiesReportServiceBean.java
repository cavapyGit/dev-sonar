package com.pradera.report.generation.executor.securities.service;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.report.generation.executor.securities.to.ExpirationSecuritiesReportTO;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)

public class ExpirationSecuritiesReportServiceBean extends CrudDaoServiceBean {
	
	public String queryExpirationSecuritiesReport(ExpirationSecuritiesReportTO filter) {
		
		StringBuilder sbQuery= new StringBuilder();
		
//		sbQuery.append("SELECT 'CUPONES' AS TITULO, I.BUSINESS_NAME, ");
//		sbQuery.append("(SELECT P.PARAMETER_NAME FROM PARAMETER_TABLE P WHERE P.PARAMETER_TABLE_PK=S.CURRENCY) AS DESCRIPTION_CU, ");
//		sbQuery.append("(SELECT P.TEXT1 FROM PARAMETER_TABLE P WHERE P.PARAMETER_TABLE_PK=S.CURRENCY) AS MNEMONIC_CU, ");
//		sbQuery.append("TO_CHAR(PIC.EXPERITATION_DATE, 'DD/MM/YYYY') AS EXPIRATION_DATE, ");
//		sbQuery.append(" (CASE "+ filter.getBcbFormat() + " WHEN 1 THEN S.ID_SECURITY_CODE_PK ");
//		sbQuery.append("WHEN 2 THEN (substr(S.ID_SECURITY_BCB_CODE,1,instr(S.ID_SECURITY_BCB_CODE,'-',-1) - 1) || '-' || LPAD(PIC.COUPON_NUMBER,3,0)) END) AS SERIAL, ");
//		sbQuery.append("S.ID_SECURITY_CODE_PK, ");
//		sbQuery.append("(SELECT P.PARAMETER_NAME FROM PARAMETER_TABLE P WHERE P.PARAMETER_TABLE_PK=S.SECURITY_CLASS) AS DESCRIPTION, ");
//		sbQuery.append("to_date('dd/mm/yyyy','') AS ISSUANCE_DATE, NVL(pic.interest_rate,0) AS INTEREST_RATE, PIC.COUPON_NUMBER, HA.ACCOUNT_NUMBER, ");
//		sbQuery.append("decode(nvl(hab.accreditation_balance,0)+(nvl(hab.BAN_BALANCE,0)+nvl(hab.OTHER_BLOCK_BALANCE,0)+nvl(hab.PAWN_BALANCE,0)),0,'NO','SI') AS S_N_BLOQUEO, ");
//		sbQuery.append("HAB.TOTAL_BALANCE, HAB.AVAILABLE_BALANCE, HAB.PAWN_BALANCE, HAB.BAN_BALANCE, HAB.ACCREDITATION_BALANCE, ");
//		sbQuery.append("CASE WHEN S.IND_PREPAID = 1 THEN ROUND(S.INITIAL_NOMINAL_VALUE,2) ELSE ROUND(PIC.COUPON_AMOUNT,2) END INITIAL_NOMINAL_VALUE, ");
//		sbQuery.append("CASE WHEN S.IND_PREPAID = 1 THEN ROUND(ROUND(S.INITIAL_NOMINAL_VALUE,2)*HAB.TOTAL_BALANCE,2) ELSE ROUND(ROUND(PIC.COUPON_AMOUNT,2)*HAB.TOTAL_BALANCE,2) END MONTO, ");
//		sbQuery.append("S.CURRENCY, I.ID_ISSUER_PK, HA.ID_HOLDER_ACCOUNT_PK, PIC.EXPERITATION_DATE FECHA_EXP, hab.REPORTING_BALANCE, PART.DESCRIPTION AS PARTICIPANT_DESCRIPTION FROM HOLDER_ACCOUNT_BALANCE hab ");
//		sbQuery.append("INNER JOIN SECURITY S ON s.ID_SECURITY_CODE_PK=hab.ID_SECURITY_CODE_PK ");
//		sbQuery.append("INNER JOIN HOLDER_ACCOUNT ha ON hab.ID_HOLDER_ACCOUNT_PK= ha.ID_HOLDER_ACCOUNT_PK ");
//		sbQuery.append("INNER JOIN ISSUER I ON S.ID_ISSUER_FK = I.ID_ISSUER_PK ");
//		sbQuery.append("INNER JOIN ISSUANCE ISS ON ISS.ID_ISSUANCE_CODE_PK = S.ID_ISSUANCE_CODE_FK " );
//		sbQuery.append("INNER JOIN interest_payment_schedule ips ON s.id_security_code_pk=ips.id_security_code_fk ");
//		sbQuery.append("INNER JOIN program_interest_coupon pic ON ips.id_int_payment_schedule_pk=pic.id_int_payment_schedule_fk ");
//		sbQuery.append("INNER JOIN PARTICIPANT PART ON PART.ID_PARTICIPANT_PK = HAB.ID_PARTICIPANT_PK ");
//		sbQuery.append("WHERE 1=1");
		
		sbQuery.append("SELECT subt.participante, ");
		sbQuery.append("FN_GET_HOLDER_DESC(subt.ID_HOLDER_ACCOUNT_PK,'-',2) AS RUT, ");
		sbQuery.append("subt.ACCOUNT_NUMBER AS CUENTA_TITULAR, ");
		sbQuery.append("FN_GET_HOLDER_DESC(subt.ID_HOLDER_ACCOUNT_PK,'-',0) AS TITULAR, ");
		sbQuery.append("subt.MOTIVO, ");
		sbQuery.append("subt.BUSINESS_NAME, ");
		sbQuery.append("subt.ID_SECURITY_CODE_PK, ");
		sbQuery.append("subt.DESCRIPTION, ");
		sbQuery.append("subt.TITULO, ");
		sbQuery.append("subt.COUPON, ");
		sbQuery.append("CASE WHEN subt.ELECTRONICO = 1 THEN 'SI' WHEN (subt.ELECTRONICO = 0 AND subt.COUPON IS NULL) THEN NULL  ");
		sbQuery.append("ELSE 'NO' END AS ELECTRONICO,  ");
		sbQuery.append("(SELECT pt.DESCRIPTION FROM PARAMETER_TABLE pt WHERE pt.PARAMETER_TABLE_PK=subt.SECURITY_CURRENCY) AS moneda, ");
		sbQuery.append("TO_CHAR(subt.ISSUANCE_DATE, 'DD/MM/YYYY') AS ISSUANCE_DATE, ");
		sbQuery.append("TO_CHAR(subt.EXPIRATION_DATE, 'DD/MM/YYYY') AS EXPIRATION_DATE, ");
		sbQuery.append("nvl(subt.FACTOR,0) AS FACTOR, ");
		sbQuery.append("CASE WHEN subt.IND_PREPAID = 1 THEN ROUND(subt.INITIAL_NOMINAL_VALUE,2) ELSE ROUND(subt.PAYMENT_AMOUNT,2) END INITIAL_NOMINAL_VALUE,  ");
		sbQuery.append("CASE WHEN subt.IND_PREPAID = 1 THEN ROUND(ROUND(subt.INITIAL_NOMINAL_VALUE,2)*subt.TOTAL_BALANCE,2) ELSE ROUND(ROUND(subt.PAYMENT_AMOUNT,2)*subt.TOTAL_BALANCE,2) END MONTO, ");
		sbQuery.append("decode(nvl(subt.accreditation_balance,0)+(nvl(subt.BAN_BALANCE,0)+nvl(subt.OTHER_BLOCK_BALANCE,0)+nvl(subt.PAWN_BALANCE,0)),0,'NO','SI') AS S_N_BLOQUEO, ");
		sbQuery.append("subt.TOTAL_BALANCE, subt.AVAILABLE_BALANCE, subt.ACCREDITATION_BALANCE, subt.BAN_BALANCE, subt.PAWN_BALANCE, subt.REPORTING_BALANCE ");
		sbQuery.append("FROM ( ");
		sbQuery.append("SELECT p.DESCRIPTION AS participante,ha.ID_HOLDER_ACCOUNT_PK, ");
		sbQuery.append("ha.ACCOUNT_NUMBER ,FN_GET_HOLDER_DESC(ha.ID_HOLDER_ACCOUNT_PK,'-',0), ");
		sbQuery.append("(SELECT pt.PARAMETER_NAME FROM PARAMETER_TABLE pt WHERE PARAMETER_TABLE_PK = aao.MOTIVE) AS MOTIVO, ");
		sbQuery.append("i.BUSINESS_NAME , ");
		sbQuery.append("s.ID_SECURITY_CODE_PK , ");
		sbQuery.append("(SELECT P.PARAMETER_NAME FROM PARAMETER_TABLE P WHERE P.PARAMETER_TABLE_PK=s.SECURITY_CLASS) AS DESCRIPTION, ");
		sbQuery.append("'VALOR' AS TITULO, ");
		sbQuery.append("NULL AS COUPON, ");
		sbQuery.append("0 AS ELECTRONICO, ");
		sbQuery.append("s.CURRENCY AS SECURITY_CURRENCY, ");
		sbQuery.append("s.ISSUANCE_DATE AS ISSUANCE_DATE, ");
		sbQuery.append("s.INITIAL_NOMINAL_VALUE, ");
		sbQuery.append("s.IND_PREPAID , ");
		sbQuery.append("hab.TOTAL_BALANCE , ");
		sbQuery.append("hab.AVAILABLE_BALANCE , ");
		sbQuery.append("hab.ACCREDITATION_BALANCE , ");
		sbQuery.append("hab.BAN_BALANCE , ");
		sbQuery.append("hab.REPORTING_BALANCE , ");
		sbQuery.append("hab.OTHER_BLOCK_BALANCE , ");
		sbQuery.append("hab.PAWN_BALANCE , ");
		sbQuery.append("aps.ID_AMO_PAYMENT_SCHEDULE_PK AS PK_SCHEDULE,pac.ID_PROGRAM_AMORTIZATION_PK AS PK_PROGRAM, ");
		sbQuery.append("pac.EXPRITATION_DATE AS EXPIRATION_DATE, pac.COUPON_NUMBER AS COUPON_NUMBER, s.INTEREST_RATE AS FACTOR, pac.AMORTIZATION_AMOUNT AS PAYMENT_AMOUNT,pac.STATE_PROGRAM_AMORTIZATON AS PAYMENT_STATE  ");
		sbQuery.append(",pc2.ID_PHYSICAL_CERTIFICATE_PK  ");
		sbQuery.append(",aao.MOTIVE , pc2.IND_ELECTRONIC_CUPON  ");
		sbQuery.append(",p.ID_PARTICIPANT_PK ");
		sbQuery.append(",i.ID_ISSUER_PK ");
		sbQuery.append("FROM SECURITY s  ");
		sbQuery.append("INNER JOIN AMORTIZATION_PAYMENT_SCHEDULE aps ON aps.ID_SECURITY_CODE_FK = s.ID_SECURITY_CODE_PK  ");
		sbQuery.append("INNER JOIN PROGRAM_AMORTIZATION_COUPON pac ON pac.ID_AMO_PAYMENT_SCHEDULE_FK = aps.ID_AMO_PAYMENT_SCHEDULE_PK ");
		sbQuery.append("LEFT JOIN PHYSICAL_CERTIFICATE pc2 ON pc2.ID_SECURITY_CODE_FK = s.ID_SECURITY_CODE_PK ");
		sbQuery.append("INNER JOIN ACCOUNT_ANNOTATION_OPERATION aao ON aao.ID_PHYSICAL_CERTIFICATE_FK = pc2.ID_PHYSICAL_CERTIFICATE_PK ");
		sbQuery.append("	AND pc2.EXPIRATION_DATE = pac.EXPRITATION_DATE  ");
		sbQuery.append("	AND pc2.ID_PROGRAM_INTEREST_FK IS NULL  ");
		sbQuery.append("INNER JOIN HOLDER_ACCOUNT_BALANCE hab ON hab.ID_SECURITY_CODE_PK = s.ID_SECURITY_CODE_PK ");
		sbQuery.append("	AND hab.TOTAL_BALANCE > 0 ");
		sbQuery.append("INNER JOIN HOLDER_ACCOUNT ha ON ha.ID_HOLDER_ACCOUNT_PK = hab.ID_HOLDER_ACCOUNT_PK  ");
		sbQuery.append("INNER JOIN PARTICIPANT p ON hab.ID_PARTICIPANT_PK = p.ID_PARTICIPANT_PK  ");
		sbQuery.append("INNER JOIN ISSUER i ON i.ID_ISSUER_PK = s.ID_ISSUER_FK  ");
		sbQuery.append("WHERE s.INSTRUMENT_TYPE = 124 ");
		sbQuery.append("AND s.ISSUANCE_FORM = 400 ");
		sbQuery.append("UNION ");
		sbQuery.append("SELECT p.DESCRIPTION,ha.ID_HOLDER_ACCOUNT_PK, ");
		sbQuery.append("ha.ACCOUNT_NUMBER,FN_GET_HOLDER_DESC(ha.ID_HOLDER_ACCOUNT_PK,'-',0), ");
		sbQuery.append("(SELECT pt2.DESCRIPTION FROM PARAMETER_TABLE pt2 WHERE pt2.PARAMETER_TABLE_PK = aao.MOTIVE), ");
//		sbQuery.append("(SELECT pt.PARAMETER_NAME FROM PARAMETER_TABLE pt WHERE PARAMETER_TABLE_PK =  ");
//		sbQuery.append("(SELECT DISTINCT aao3.MOTIVE FROM ACCOUNT_ANNOTATION_OPERATION aao3 WHERE aao3.ID_SECURITY_CODE_FK = s1.ID_SECURITY_CODE_PK ");
//		sbQuery.append("AND aao3.ID_HOLDER_ACCOUNT_FK = hab2.ID_HOLDER_ACCOUNT_PK)), ");
		sbQuery.append("i.BUSINESS_NAME , ");
		sbQuery.append("s1.ID_SECURITY_CODE_PK , ");
		sbQuery.append("(SELECT P.PARAMETER_NAME FROM PARAMETER_TABLE P WHERE P.PARAMETER_TABLE_PK=s1.SECURITY_CLASS) AS DESCRIPTION, ");
		sbQuery.append("'CUPONES' AS TITULO, ");
		sbQuery.append("pic.COUPON_NUMBER , ");
		sbQuery.append("pc.IND_ELECTRONIC_CUPON, ");
		sbQuery.append("s1.CURRENCY, ");
		sbQuery.append("s1.ISSUANCE_DATE, ");
		sbQuery.append("s1.INITIAL_NOMINAL_VALUE, ");
		sbQuery.append("s1.IND_PREPAID , ");
		sbQuery.append("hab2.TOTAL_BALANCE , ");
		sbQuery.append("hab2.AVAILABLE_BALANCE , ");
		sbQuery.append("hab2.ACCREDITATION_BALANCE , ");
		sbQuery.append("hab2.BAN_BALANCE , ");
		sbQuery.append("hab2.REPORTING_BALANCE , ");
		sbQuery.append("hab2.OTHER_BLOCK_BALANCE , ");
		sbQuery.append("hab2.PAWN_BALANCE , ");
		sbQuery.append("ips.ID_INT_PAYMENT_SCHEDULE_PK ,pic.ID_PROGRAM_INTEREST_PK , ");
		sbQuery.append("pic.EXPERITATION_DATE , pic.COUPON_NUMBER , pic.INTEREST_RATE , pic.COUPON_AMOUNT ,pic.STATE_PROGRAM_INTEREST ");
		sbQuery.append(",pc.ID_PHYSICAL_CERTIFICATE_PK  ");
		sbQuery.append(", aao.MOTIVE , pc.IND_ELECTRONIC_CUPON  ");
		sbQuery.append(",p.ID_PARTICIPANT_PK ");
		sbQuery.append(",i.ID_ISSUER_PK ");
		sbQuery.append("FROM SECURITY s1  ");
		sbQuery.append("INNER JOIN INTEREST_PAYMENT_SCHEDULE ips ON ips.ID_SECURITY_CODE_FK = s1.ID_SECURITY_CODE_PK  ");
		sbQuery.append("INNER JOIN PROGRAM_INTEREST_COUPON pic ON pic.ID_INT_PAYMENT_SCHEDULE_FK = ips.ID_INT_PAYMENT_SCHEDULE_PK ");
		sbQuery.append("INNER JOIN ACCOUNT_ANNOTATION_CUPON aac ON aac.ID_PROGRAM_INTEREST_FK = pic.ID_PROGRAM_INTEREST_PK ");
		sbQuery.append("INNER JOIN ACCOUNT_ANNOTATION_OPERATION aao ON aac.ID_ANNOTATION_OPERATION_FK = aao.ID_ANNOTATION_OPERATION_PK ");
		sbQuery.append("INNER JOIN PHYSICAL_CERTIFICATE pc ON aao.ID_PHYSICAL_CERTIFICATE_FK = pc.ID_PHYSICAL_CERTIFICATE_PK ");
//		sbQuery.append("LEFT JOIN PHYSICAL_CERTIFICATE pc ON pc.ID_PROGRAM_INTEREST_FK = pic.ID_PROGRAM_INTEREST_PK  ");
//		sbQuery.append("INNER JOIN ACCOUNT_ANNOTATION_OPERATION aao ON aao.ID_PHYSICAL_CERTIFICATE_FK = pc.ID_PHYSICAL_CERTIFICATE_PK ");
//		sbQuery.append("		AND pc.EXPIRATION_DATE = pic.EXPERITATION_DATE  ");
//		sbQuery.append("		AND pc.ID_PROGRAM_INTEREST_FK IS NULL ");
//		sbQuery.append("left JOIN ACCOUNT_ANNOTATION_OPERATION aao2 ON aao2.ID_PHYSICAL_CERTIFICATE_FK = pc.ID_PHYSICAL_CERTIFICATE_PK  ");
		sbQuery.append("INNER JOIN HOLDER_ACCOUNT_BALANCE hab2 ON hab2.ID_SECURITY_CODE_PK = s1.ID_SECURITY_CODE_PK  ");
		sbQuery.append("	AND hab2.TOTAL_BALANCE > 0 ");
		sbQuery.append("INNER JOIN HOLDER_ACCOUNT ha ON ha.ID_HOLDER_ACCOUNT_PK = hab2.ID_HOLDER_ACCOUNT_PK  ");
		sbQuery.append("INNER JOIN PARTICIPANT p ON hab2.ID_PARTICIPANT_PK = p.ID_PARTICIPANT_PK  ");
		sbQuery.append("INNER JOIN ISSUER i ON i.ID_ISSUER_PK = s1.ID_ISSUER_FK ");
		sbQuery.append("WHERE s1.INSTRUMENT_TYPE = 124  ");
		sbQuery.append("	AND s1.ISSUANCE_FORM = 400 ");
		sbQuery.append("UNION  ");
		sbQuery.append("SELECT DISTINCT p.DESCRIPTION,ha.ID_HOLDER_ACCOUNT_PK, ");
		sbQuery.append("ha.ACCOUNT_NUMBER,FN_GET_HOLDER_DESC(ha.ID_HOLDER_ACCOUNT_PK,'-',0), ");
		sbQuery.append("(SELECT pt.PARAMETER_NAME FROM PARAMETER_TABLE pt WHERE PARAMETER_TABLE_PK = aao4.MOTIVE), ");
		sbQuery.append("i.BUSINESS_NAME , ");
		sbQuery.append("s.ID_SECURITY_CODE_PK , ");
		sbQuery.append("(SELECT P.PARAMETER_NAME FROM PARAMETER_TABLE P WHERE P.PARAMETER_TABLE_PK=s.SECURITY_CLASS) AS DESCRIPTION, ");
		sbQuery.append("'VALOR' AS TITULO, ");
		sbQuery.append("NULL AS COUPON, ");
		sbQuery.append("0 AS ELECTRONICO, ");
		sbQuery.append("s.CURRENCY, ");
		sbQuery.append("s.ISSUANCE_DATE, ");
		sbQuery.append("s.INITIAL_NOMINAL_VALUE, ");
		sbQuery.append("s.IND_PREPAID , ");
		sbQuery.append("hab.TOTAL_BALANCE , ");
		sbQuery.append("hab.AVAILABLE_BALANCE , ");
		sbQuery.append("hab.ACCREDITATION_BALANCE , ");
		sbQuery.append("hab.BAN_BALANCE , ");
		sbQuery.append("hab.REPORTING_BALANCE , ");
		sbQuery.append("hab.OTHER_BLOCK_BALANCE , ");
		sbQuery.append("hab.PAWN_BALANCE , ");
		sbQuery.append("aps.ID_AMO_PAYMENT_SCHEDULE_PK AS PK_SCHEDULE,pac.ID_PROGRAM_AMORTIZATION_PK AS PK_PROGRAM, ");
		sbQuery.append("pac.EXPRITATION_DATE AS EXPIRATION_DATE, pac.COUPON_NUMBER AS COUPON_NUMBER, s.INTEREST_RATE AS FACTOR, pac.AMORTIZATION_AMOUNT AS PAYMENT_AMOUNT,pac.STATE_PROGRAM_AMORTIZATON AS PAYMENT_STATE ");
		sbQuery.append(",NULL  ");
		sbQuery.append(",NULL , NULL  ");
		sbQuery.append(",p.ID_PARTICIPANT_PK ");
		sbQuery.append(",i.ID_ISSUER_PK ");
		sbQuery.append("FROM SECURITY s  ");
		sbQuery.append("INNER JOIN AMORTIZATION_PAYMENT_SCHEDULE aps ON aps.ID_SECURITY_CODE_FK = s.ID_SECURITY_CODE_PK  ");
		sbQuery.append("INNER JOIN PROGRAM_AMORTIZATION_COUPON pac ON pac.ID_AMO_PAYMENT_SCHEDULE_FK = aps.ID_AMO_PAYMENT_SCHEDULE_PK ");
		sbQuery.append("INNER JOIN HOLDER_ACCOUNT_BALANCE hab ON hab.ID_SECURITY_CODE_PK = s.ID_SECURITY_CODE_PK ");
		sbQuery.append("	AND hab.TOTAL_BALANCE > 0 ");
		sbQuery.append("INNER JOIN HOLDER_ACCOUNT ha ON ha.ID_HOLDER_ACCOUNT_PK = hab.ID_HOLDER_ACCOUNT_PK  ");
		sbQuery.append("INNER JOIN PARTICIPANT p ON hab.ID_PARTICIPANT_PK = p.ID_PARTICIPANT_PK  ");
		sbQuery.append("INNER JOIN ISSUER i ON i.ID_ISSUER_PK = s.ID_ISSUER_FK ");
		sbQuery.append("INNER JOIN ACCOUNT_ANNOTATION_OPERATION aao4 ON aao4.ID_HOLDER_ACCOUNT_FK = hab.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append("AND aao4.ID_SECURITY_CODE_FK = s.ID_SECURITY_CODE_PK ");
		sbQuery.append("WHERE s.INSTRUMENT_TYPE = 124 ");
		sbQuery.append("AND s.ISSUANCE_FORM = 132 ");
		sbQuery.append("UNION  ");
		sbQuery.append("SELECT p.DESCRIPTION,ha.ID_HOLDER_ACCOUNT_PK, ");
		sbQuery.append("ha.ACCOUNT_NUMBER,FN_GET_HOLDER_DESC(ha.ID_HOLDER_ACCOUNT_PK,'-',0), ");
		sbQuery.append("(SELECT pt.PARAMETER_NAME FROM PARAMETER_TABLE pt WHERE PARAMETER_TABLE_PK = aao4.MOTIVE), ");
		sbQuery.append("i.BUSINESS_NAME , ");
		sbQuery.append("s1.ID_SECURITY_CODE_PK , ");
		sbQuery.append("(SELECT P.PARAMETER_NAME FROM PARAMETER_TABLE P WHERE P.PARAMETER_TABLE_PK=s1.SECURITY_CLASS) AS DESCRIPTION, ");
		sbQuery.append("'CUPONES' AS TITULO, ");
		sbQuery.append("pic.COUPON_NUMBER , ");
		sbQuery.append("NULL, ");
		sbQuery.append("s1.CURRENCY, ");
		sbQuery.append("s1.ISSUANCE_DATE, ");
		sbQuery.append("s1.INITIAL_NOMINAL_VALUE, ");
		sbQuery.append("s1.IND_PREPAID , ");
		sbQuery.append("hab2.TOTAL_BALANCE , ");
		sbQuery.append("hab2.AVAILABLE_BALANCE , ");
		sbQuery.append("hab2.ACCREDITATION_BALANCE , ");
		sbQuery.append("hab2.BAN_BALANCE , ");
		sbQuery.append("hab2.REPORTING_BALANCE , ");
		sbQuery.append("hab2.OTHER_BLOCK_BALANCE , ");
		sbQuery.append("hab2.PAWN_BALANCE , ");
		sbQuery.append("ips.ID_INT_PAYMENT_SCHEDULE_PK ,pic.ID_PROGRAM_INTEREST_PK , ");
		sbQuery.append("pic.EXPERITATION_DATE , pic.COUPON_NUMBER , pic.INTEREST_RATE , pic.COUPON_AMOUNT ,pic.STATE_PROGRAM_INTEREST ");
		sbQuery.append(",NULL ");
		sbQuery.append(", NULL , NULL ");
		sbQuery.append(",p.ID_PARTICIPANT_PK ");
		sbQuery.append(",i.ID_ISSUER_PK ");
		sbQuery.append("FROM SECURITY s1  ");
		sbQuery.append("INNER JOIN INTEREST_PAYMENT_SCHEDULE ips ON ips.ID_SECURITY_CODE_FK = s1.ID_SECURITY_CODE_PK  ");
		sbQuery.append("INNER JOIN PROGRAM_INTEREST_COUPON pic ON pic.ID_INT_PAYMENT_SCHEDULE_FK = ips.ID_INT_PAYMENT_SCHEDULE_PK  ");
		sbQuery.append("INNER JOIN HOLDER_ACCOUNT_BALANCE hab2 ON hab2.ID_SECURITY_CODE_PK = s1.ID_SECURITY_CODE_PK  ");
		sbQuery.append("	AND hab2.TOTAL_BALANCE > 0 ");
		sbQuery.append("INNER JOIN HOLDER_ACCOUNT ha ON ha.ID_HOLDER_ACCOUNT_PK = hab2.ID_HOLDER_ACCOUNT_PK  ");
		sbQuery.append("INNER JOIN PARTICIPANT p ON hab2.ID_PARTICIPANT_PK = p.ID_PARTICIPANT_PK  ");
		sbQuery.append("INNER JOIN ISSUER i ON i.ID_ISSUER_PK = s1.ID_ISSUER_FK ");
		sbQuery.append("INNER JOIN ACCOUNT_ANNOTATION_OPERATION aao4 ON aao4.ID_HOLDER_ACCOUNT_FK = hab2.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append("AND aao4.ID_SECURITY_CODE_FK = s1.ID_SECURITY_CODE_PK ");
		sbQuery.append("WHERE s1.INSTRUMENT_TYPE = 124  ");
		sbQuery.append("AND s1.ISSUANCE_FORM = 132 ");
		sbQuery.append(") subt ");
		sbQuery.append("WHERE 1 = 1 ");
		
		if(filter.getParticipant() != null) {
			sbQuery.append("AND subt.ID_PARTICIPANT_PK = " + filter.getParticipant().toString());
		}
		
		if(filter.getInitialDate() != null) {
			sbQuery.append("AND trunc(subt.EXPIRATION_DATE) >= to_date('" + CommonsUtilities.convertDatetoString(filter.getInitialDate()) + "','dd/MM/yyyy') ");
		}
		
		if(filter.getFinalDate() != null) {
			sbQuery.append("AND trunc(subt.EXPIRATION_DATE) <= to_date('" + CommonsUtilities.convertDatetoString(filter.getFinalDate())+ "','dd/MM/yyyy') "); //REVISAR PARAMETRO
		}
		
		sbQuery.append("AND subt.TOTAL_BALANCE > 0 ");
		
		if(filter.getIssuer() != null) {
			sbQuery.append(" AND subt.ID_ISSUER_PK = '" + filter.getIssuer() + "' "); //REVISAR PARAMTRO
		}
		
		if(filter.getHolderAccount() != null) {
			sbQuery.append(" AND subt.ACCOUNT_NUMBER = " + filter.getHolderAccount().toString());

		}
		
		if(filter.getHolder() != null && filter.getParticipant() != null) {
			sbQuery.append(" AND " + filter.getHolder() + " IS NULL or subt.ID_HOLDER_ACCOUNT_PK IN (SELECT had.ID_HOLDER_ACCOUNT_FK FROM HOLDER_ACCOUNT_DETAIL had INNER JOIN HOLDER_ACCOUNT ha ON had.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK WHERE had.ID_HOLDER_FK = " + filter.getHolder());
			sbQuery.append(" AND had.ind_representative=1 ");
			sbQuery.append(" AND ha.ID_PARTICIPANT_FK = " + filter.getParticipant().toString() + ") ");
			
		}
		
		if(filter.getHolder() != null && filter.getParticipant() == null) {
			sbQuery.append(" AND " + filter.getHolder() + " IS NULL or subt.ID_HOLDER_ACCOUNT_PK IN (SELECT had.ID_HOLDER_ACCOUNT_FK FROM HOLDER_ACCOUNT_DETAIL had INNER JOIN HOLDER_ACCOUNT ha ON had.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK WHERE had.ID_HOLDER_FK = " + filter.getHolder());
			sbQuery.append(" AND had.ind_representative=1) ");
		}
		
		if(filter.getSecurityCode() != null) {
			sbQuery.append("AND " + filter.getSecurityCode() + " IS NULL or subt.ID_SECURITY_CODE_PK = " + filter.getSecurityCode() + " ) "); //revisar parAMETRO
		}
	
//		if(filter.getInstitutionType() != null && filter.getParticipantInvestorInstitutionType() != null) {
//			sbQuery.append("AND (" + filter.getInstitutionType() + " <> 187 or (" + filter.getInstitutionType() + " = 187 ");
//			sbQuery.append(" AND (EXISTS (SELECT 1 FROM HOLDER_ACCOUNT_DETAIL HAD ");
//			sbQuery.append("INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK INNER JOIN PARTICIPANT P ON P.ID_PARTICIPANT_PK = " + filter.getParticipantInvestorInstitutionType());
//			sbQuery.append(" WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK AND HA.ID_PARTICIPANT_FK = P.ID_PARTICIPANT_PK ");
//			sbQuery.append(" AND H.DOCUMENT_TYPE = P.DOCUMENT_TYPE AND H.DOCUMENT_NUMBER = P.DOCUMENT_NUMBER))))");
//		}
		
//		if(filter.getWithoutDpf() != null) {
//			sbQuery.append(" AND case when '" + filter.getWithoutDpf() + "' = '2' then ( S.SECURITY_CLASS ) else 1 end != 420 ");
//		}
		
//		if(filter.getInstitutionType() != null) {
//			sbQuery.append("AND ((" + filter.getInstitutionType() + " = 186 ");
//			sbQuery.append("AND ((" + filter.getSecurityClass() + " is null AND S.SECURITY_CLASS IN (1976,420)) "); //REVISAR PARAMETROS
//			sbQuery.append("OR (" + filter.getSecurityClass() + " is not null AND " + filter.getSecurityClass() + " = S.SECURITY_CLASS))) "); //REVISAR_PARAMETROS
//			sbQuery.append("OR (" + filter.getInstitutionType() +" <> 186 AND (" + filter.getSecurityClass() + " is null or " + filter.getSecurityClass() + " = S.SECURITY_CLASS))) "); //REVISR PARAMETROS
//		}
		
//		sbQuery.append("AND CASE '" + filter.getBcbFormat()  + "' WHEN '1' THEN S.ID_SECURITY_CODE_PK WHEN '2' THEN S.ID_SECURITY_BCB_CODE END IS NOT NULL AND "); //REVIAR PARAMTROS
//		sbQuery.append("(('" + filter.getBcbFormat() + "' = '1') OR ('" + filter.getBcbFormat() + "' = '2' AND S.SECURITY_CLASS NOT IN (414,415))) ");
//		sbQuery.append("UNION ");
//		sbQuery.append("SELECT 'VALORES' AS TITULO, I.BUSINESS_NAME, (SELECT P.PARAMETER_NAME FROM PARAMETER_TABLE P WHERE P.PARAMETER_TABLE_PK=S.CURRENCY) AS DESCRIPTION_CU, ");
//		sbQuery.append("(SELECT P.TEXT1 FROM PARAMETER_TABLE P WHERE P.PARAMETER_TABLE_PK=S.CURRENCY) AS MNEMONIC_CU, TO_CHAR(S.EXPIRATION_DATE, 'DD/MM/YYYY') AS EXPIRATION_DATE, ");
//		sbQuery.append("(CASE WHEN "+ filter.getBcbFormat() + " = 1 THEN S.ID_SECURITY_CODE_PK WHEN( " + filter.getBcbFormat() + "= 2 AND S.SECURITY_CLASS = 421) "); //REVISAR PARAMETROS
//		sbQuery.append("THEN substr(S.ID_SECURITY_BCB_CODE,1,instr(S.ID_SECURITY_BCB_CODE,'-',-1) - 1) || '-' || ");
//		sbQuery.append("LPAD(substr(S.ID_SECURITY_BCB_CODE,instr(S.ID_SECURITY_BCB_CODE,'-',-1) + 1 ,LENGTH(S.ID_SECURITY_BCB_CODE)),3,0) ");
//		sbQuery.append("ELSE S.ID_SECURITY_BCB_CODE END) AS SERIAL, S.ID_SECURITY_CODE_PK, ");
//		sbQuery.append("(SELECT P.PARAMETER_NAME FROM PARAMETER_TABLE P WHERE P.PARAMETER_TABLE_PK=S.SECURITY_CLASS) AS DESCRIPTION, ");
//		sbQuery.append("S.ISSUANCE_DATE, NVL(S.INTEREST_RATE,0) AS INTEREST_RATE, 0 AS COUPON_NUMBER, HA.ACCOUNT_NUMBER, ");
//		sbQuery.append("decode(nvl(hab.accreditation_balance,0)+(nvl(hab.BAN_BALANCE,0)+nvl(hab.OTHER_BLOCK_BALANCE,0)+nvl(hab.PAWN_BALANCE,0)),0,'NO','SI') AS S_N_BLOQUEO, ");
//		sbQuery.append("HAB.TOTAL_BALANCE, HAB.AVAILABLE_BALANCE, HAB.PAWN_BALANCE, HAB.BAN_BALANCE, HAB.ACCREDITATION_BALANCE, CASE WHEN S.IND_PREPAID = 1 ");
//		sbQuery.append("THEN (ROUND(NVL(S.INITIAL_NOMINAL_VALUE,0),2)) ELSE (ROUND(NVL(PAC.COUPON_AMOUNT,0),2)) END AS INITIAL_NOMINAL_VALUE, CASE WHEN S.IND_PREPAID = 1 ");
//		sbQuery.append("THEN ((ROUND(NVL(S.INITIAL_NOMINAL_VALUE,0),2)) * HAB.TOTAL_BALANCE) ELSE ((ROUND(NVL(PAC.COUPON_AMOUNT,0),2)) * HAB.TOTAL_BALANCE) END AS MONTO, ");
//		sbQuery.append("S.CURRENCY, I.ID_ISSUER_PK, HA.ID_HOLDER_ACCOUNT_PK, PAC.expritation_date AS FECHA_EXP, hab.REPORTING_BALANCE, PART.DESCRIPTION AS PARTICIPANT_DESCRIPTION ");
//		sbQuery.append("FROM SECURITY S INNER JOIN ISSUER I ON S.ID_ISSUER_FK = I.ID_ISSUER_PK INNER JOIN ISSUANCE ISS ON ISS.ID_ISSUANCE_CODE_PK = S.ID_ISSUANCE_CODE_FK ");
//		sbQuery.append("INNER JOIN HOLDER_ACCOUNT_BALANCE hab on s.ID_SECURITY_CODE_PK=hab.ID_SECURITY_CODE_PK INNER JOIN holder_account ha on hab.ID_HOLDER_ACCOUNT_PK= ha.ID_HOLDER_ACCOUNT_PK ");
//		sbQuery.append("INNER JOIN amortization_payment_schedule APS  ON APS.ID_SECURITY_CODE_FK = S.id_security_code_pk INNER JOIN PROGRAM_AMORTIZATION_COUPON PAC    ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK ");
//		sbQuery.append("INNER JOIN PARTICIPANT PART ON PART.ID_PARTICIPANT_PK = HAB.ID_PARTICIPANT_PK WHERE 1 = 1 ");		
//	
//		if(filter.getParticipant() != null) {
//			sbQuery.append("AND PART.ID_PARTICIPANT_PK = " + filter.getParticipant().toString());
//		}
//		
//		if(filter.getInitialDate() != null) {
//			sbQuery.append("AND trunc(PAC.expritation_date) >= to_date('" + CommonsUtilities.convertDatetoString(filter.getInitialDate()) +"','dd/MM/yyyy') ");
//		}
//		
//		if(filter.getInitialDate() != null) {
//			sbQuery.append("AND trunc(PAC.expritation_date) <= to_date('" + CommonsUtilities.convertDatetoString(filter.getFinalDate())+"','dd/MM/yyyy') ");
//		}
//		
//		sbQuery.append(" AND HAB.TOTAL_BALANCE > 0 ");
//		
//		if(filter.getIssuer() != null) {
//			sbQuery.append("AND i.id_issuer_pk= '" + filter.getIssuer() + "' ");
//		}
//		
//		if(filter.getHolderAccount() != null) {
//			sbQuery.append("AND ha.account_number= " + filter.getHolderAccount().toString());
//		}
//		
//		if(filter.getHolder() != null) {
//			sbQuery.append(" AND ( " + filter.getHolder() + " IS NULL or (EXISTS");
//			sbQuery.append("(SELECT had.ID_HOLDER_FK FROM HOLDER_ACCOUNT_DETAIL had WHERE had.ID_HOLDER_FK = " + filter.getHolder() + " AND had.ind_representative=1 AND had.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK))) ");
//		}
//		
//		if(filter.getSecurityCode() != null) {
//			sbQuery.append("AND ('" + filter.getSecurityCode() + "' IS NULL or S.ID_SECURITY_CODE_PK = '" + filter.getSecurityCode() + "') ");
//		}
//		
//		if (filter.getInstitutionType() != null && filter.getParticipantInvestorInstitutionType() != null) {
//			sbQuery.append("AND (" + filter.getInstitutionType() + " <> 187 or (" + filter.getInstitutionType() +" = 187 AND (EXISTS (SELECT 1 FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
//			sbQuery.append("INNER JOIN PARTICIPANT P ON P.ID_PARTICIPANT_PK = " + filter.getParticipantInvestorInstitutionType() + " WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ");
//			sbQuery.append(" AND HA.ID_PARTICIPANT_FK = P.ID_PARTICIPANT_PK AND H.DOCUMENT_TYPE = P.DOCUMENT_TYPE AND H.DOCUMENT_NUMBER = P.DOCUMENT_NUMBER )))) ");
//		}
//		
//		if(filter.getWithoutDpf() != null) {
//			sbQuery.append("AND case when '" + filter.getWithoutDpf() + "' = '2' then ( S.SECURITY_CLASS ) else 1 end != 420");
//		}
//		
//		if(filter.getInstitutionType() != null && filter.getSecurityClass() != null) {
//			sbQuery.append(" AND ((" + filter.getInstitutionType() + " = 186 AND ((" + filter.getSecurityClass() + " is null AND S.SECURITY_CLASS IN (1976,420)) ");
//			sbQuery.append("OR (" + filter.getSecurityClass() + " is not null AND " + filter.getSecurityClass() + " = S.SECURITY_CLASS))) OR (" + filter.getInstitutionType() + " <> 186 ");
//			sbQuery.append("AND (" + filter.getSecurityClass() + " is null or " + filter.getSecurityClass() + " = S.SECURITY_CLASS))) ");
//		}
//		
//		sbQuery.append("AND CASE '" + filter.getBcbFormat() + "' WHEN '1' THEN S.ID_SECURITY_CODE_PK  WHEN '2' THEN S.ID_SECURITY_BCB_CODE END IS NOT NULL ");
//		sbQuery.append("AND (( '" + filter.getBcbFormat() +"' = '1') OR ('" + filter.getBcbFormat() + "' = '2' AND S.SECURITY_CLASS NOT IN (414,415))) ORDER BY  22,1 desc,24,21,7,6,23");
		
		return sbQuery.toString();
	}
	
	private List<Object[]> getRutAndFullname(Integer holderAccountPk) {
		StringBuilder sbQuery= new StringBuilder();
		
		sbQuery.append("SELECT   HAD.ID_HOLDER_FK CUI, H.FULL_NAME FROM HOLDER_ACCOUNT_DETAIL HAD, HOLDER H ");
		sbQuery.append("WHERE H.ID_HOLDER_PK = HAD.ID_HOLDER_FK AND HAD.ID_HOLDER_ACCOUNT_FK= " + holderAccountPk.toString());
		
		Query queryExecution= em.createNativeQuery(sbQuery.toString());
		List<Object[]> listResult = queryExecution.getResultList();
		
		return listResult;
		
	}
	
	public byte[] generateExcelDelimitated(Map<String,Object> parametersRequired, ExpirationSecuritiesReportTO filter) {
		
		String query = this.queryExpirationSecuritiesReport(filter);
		
		
		
		List<ExpirationSecuritiesReportTO> listResult = this.getReportData(query);
		
		byte[] bytes = null;
		
		List<String> lstHeadsSheet = new ArrayList<String>();
		lstHeadsSheet.add("DEPOSITANTE;RUT TITULAR;CUENTA TITULAR;TITULAR;MOTIVO DE INGRESO;EMISOR;CODIGO VALOR;DESCRIPCION;TITULO;N CUPON;CUPON ELECTRÓNICO;MONEDA;FECHA DE EMISION;FECHA DE VENCIMIENTO;TASA NOMINAL;"
				+ "VALOR FINAL;MONTO;BLOQUEO;CANTIDAD;DISPONIBLE;CAT;EMBARGO;PRENDA;RPDOR");
		
        String [] headerSheet = new String[lstHeadsSheet.size()];
		int m = 0;
		for (String title : lstHeadsSheet) {
			headerSheet[m] = title;
			m = ++m;
		}
		XSSFWorkbook libro= new XSSFWorkbook();
		String hoja = "Hoja 1";
		XSSFSheet hojas = libro.createSheet(hoja);
		CellStyle styleTitle = libro.createCellStyle();
	    Font fontTitle = libro.createFont();
	    fontTitle.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    fontTitle.setFontHeightInPoints((short)14); 
	    styleTitle.setFont(fontTitle);
	    
	    CellStyle styleSubTitle = libro.createCellStyle();
	    Font fontSubTitle = libro.createFont();
	    fontSubTitle.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    fontSubTitle.setFontHeightInPoints((short)10); 
	    styleSubTitle.setFont(fontSubTitle);
	    
		CellStyle style = libro.createCellStyle();
	    Font font = libro.createFont();
	    font.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    font.setFontHeightInPoints((short)10); 
	    style.setFont(font);
	    
	    XSSFRow row=hojas.createRow(0); 
		XSSFCell cellTitle= row.createCell(0); // 3 columna en ubicarse el titulo
		cellTitle.setCellStyle(styleTitle);  
		cellTitle.setCellValue("REPORTE DE VENCIMIENTO DE VALORES");
		
		if(listResult != null && listResult.size() > 0) {
			
			int i = 0;
			
			row = hojas.createRow(++i); 
    		for (int k = 0; k <headerSheet.length; k++) {
    			XSSFCell cell= row.createCell(k); 
    			cell.setCellStyle(style); 
    			cell.setCellValue(headerSheet[k]); 
    		}
    		
    		for(ExpirationSecuritiesReportTO expirationSecuritiesReportTO : listResult) {
				row = hojas.createRow(++i); 
				int j = 0;
				StringBuilder result = new StringBuilder();
				
				result.append(expirationSecuritiesReportTO.getParticipantDescription());
				result.append(";");
				result.append(expirationSecuritiesReportTO.getRut());
				result.append(";");
				result.append(expirationSecuritiesReportTO.getAccountNumber());
				result.append(";");
				result.append(expirationSecuritiesReportTO.getHolderDescription());
				result.append(";");
				result.append(expirationSecuritiesReportTO.getMotive());
				result.append(";");
				result.append(expirationSecuritiesReportTO.getBusinessName());
				result.append(";");
				result.append(expirationSecuritiesReportTO.getIdSecurityCodePk());
				result.append(";");
				result.append(expirationSecuritiesReportTO.getDescription());
				result.append(";");
				result.append(expirationSecuritiesReportTO.getTitulo());
				result.append(";");
				result.append(expirationSecuritiesReportTO.getCouponNumber() != null ? expirationSecuritiesReportTO.getCouponNumber() : "");
				result.append(";");
				result.append(expirationSecuritiesReportTO.getElectronicCoupon());
				result.append(";");
				result.append(expirationSecuritiesReportTO.getDescriptionCu());
				result.append(";");
				result.append(expirationSecuritiesReportTO.getIssuanceDate());
				result.append(";");
				result.append(expirationSecuritiesReportTO.getExpirationDate());
				result.append(";");
				result.append(expirationSecuritiesReportTO.getInterestRate() != null ? expirationSecuritiesReportTO.getInterestRate() : "");
				result.append(";");
				result.append(expirationSecuritiesReportTO.getInitialNominalValue());
				result.append(";");
				result.append(expirationSecuritiesReportTO.getMonto());
				result.append(";");
				result.append(expirationSecuritiesReportTO.getSnBloqueo());
				result.append(";");
				result.append(expirationSecuritiesReportTO.getTotalBalance());
				result.append(";");
				result.append(expirationSecuritiesReportTO.getAvailableBalance());
				result.append(";");
				result.append(expirationSecuritiesReportTO.getAcreditationBalance());
				result.append(";");
				result.append(expirationSecuritiesReportTO.getBanBalance());
				result.append(";");
				result.append(expirationSecuritiesReportTO.getPawnBalance());
				result.append(";");
				result.append(expirationSecuritiesReportTO.getReportingBalance());
				result.append(";");
				
				XSSFCell
				cell = row.createCell(j);   cell.setCellValue(result.toString());
    		}
			
		}
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		//OutputStream ous = new OutputStream
		
		try {
			try {
				libro.write(bos);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} finally {
		    try {
				bos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		bytes = bos.toByteArray();
		return bytes;
	}
	
	private List<ExpirationSecuritiesReportTO> getReportData(String query) {
		
		Query queryExecution= em.createNativeQuery(query);
		List<Object[]> listResult = queryExecution.getResultList();
		List<Object[]>  rutAndName = new ArrayList<Object[]>();
		
		List<ExpirationSecuritiesReportTO> listReportData = new ArrayList<ExpirationSecuritiesReportTO>();
		
		if(listResult != null && listResult.size() > 0) {
			
			for(Object[] iterator : listResult) {
				ExpirationSecuritiesReportTO expirationSecuritiesReportTO = new ExpirationSecuritiesReportTO();
				expirationSecuritiesReportTO.setParticipantDescription(iterator[0].toString());
				expirationSecuritiesReportTO.setRut(iterator[1].toString());
				expirationSecuritiesReportTO.setAccountNumber(new BigDecimal(iterator[2].toString()));
				expirationSecuritiesReportTO.setHolderDescription(iterator[3].toString());
				expirationSecuritiesReportTO.setMotive(iterator[4]!= null ? iterator[4].toString():"");
				expirationSecuritiesReportTO.setBusinessName(iterator[5].toString());
				expirationSecuritiesReportTO.setIdSecurityCodePk(iterator[6].toString());
				expirationSecuritiesReportTO.setDescription(iterator[7].toString());
				expirationSecuritiesReportTO.setTitulo(iterator[8].toString());
				expirationSecuritiesReportTO.setCouponNumber(iterator[10] != null ? iterator[9].toString(): "");
				expirationSecuritiesReportTO.setElectronicCoupon(iterator[10] != null ? iterator[10].toString(): "");
				expirationSecuritiesReportTO.setDescriptionCu(iterator[11].toString());
				expirationSecuritiesReportTO.setIssuanceDate(iterator[12] != null ? iterator[12].toString() : "");
				expirationSecuritiesReportTO.setExpirationDate(iterator[13].toString());
				expirationSecuritiesReportTO.setInterestRate(new BigDecimal(iterator[14].toString()));
				expirationSecuritiesReportTO.setInitialNominalValue(new BigDecimal(iterator[15].toString()));
				expirationSecuritiesReportTO.setMonto(new BigDecimal(iterator[16].toString()));
				expirationSecuritiesReportTO.setSnBloqueo(iterator[17].toString());
				expirationSecuritiesReportTO.setTotalBalance(new BigDecimal(iterator[18].toString()));
				expirationSecuritiesReportTO.setAvailableBalance(new BigDecimal(iterator[19].toString()));
				expirationSecuritiesReportTO.setAcreditationBalance(new BigDecimal(iterator[20].toString()));
				expirationSecuritiesReportTO.setBanBalance(new BigDecimal(iterator[21].toString()));
				expirationSecuritiesReportTO.setPawnBalance(new BigDecimal(iterator[22].toString()));
				expirationSecuritiesReportTO.setReportingBalance(new BigDecimal(iterator[23].toString()));
				
//				expirationSecuritiesReportTO.setTitulo(iterator[0].toString());
//				expirationSecuritiesReportTO.setBusinessName(iterator[1].toString());
//				expirationSecuritiesReportTO.setDescriptionCu(iterator[2].toString());
//				expirationSecuritiesReportTO.setMnemonicCu(iterator[3].toString());
//				expirationSecuritiesReportTO.setExpirationDate(iterator[4].toString());
//				expirationSecuritiesReportTO.setSerial(iterator[5].toString());
//				expirationSecuritiesReportTO.setIdSecurityCodePk(iterator[6].toString());
//				expirationSecuritiesReportTO.setDescription(iterator[7].toString());
//				expirationSecuritiesReportTO.setIssuanceDate(iterator[8] != null ? iterator[8].toString() : "");
//				expirationSecuritiesReportTO.setInterestRate(new BigDecimal(iterator[9].toString()));
//				expirationSecuritiesReportTO.setCouponNumber(new BigDecimal(iterator[10].toString()));
//				expirationSecuritiesReportTO.setAccountNumber(new BigDecimal(iterator[11].toString()));
//				expirationSecuritiesReportTO.setSnBloqueo(iterator[12].toString());
//				expirationSecuritiesReportTO.setTotalBalance(new BigDecimal(iterator[13].toString()));
//				expirationSecuritiesReportTO.setAvailableBalance(new BigDecimal(iterator[14].toString()));
//				expirationSecuritiesReportTO.setPawnBalance(new BigDecimal(iterator[15].toString()));
//				expirationSecuritiesReportTO.setBanBalance(new BigDecimal(iterator[16].toString()));
//				expirationSecuritiesReportTO.setAcreditationBalance(new BigDecimal(iterator[17].toString()));
//				expirationSecuritiesReportTO.setInitialNominalValue(new BigDecimal(iterator[18].toString()));
//				expirationSecuritiesReportTO.setMonto(new BigDecimal(iterator[19].toString()));
//				expirationSecuritiesReportTO.setCurrency(new BigDecimal(iterator[20].toString()));
//				expirationSecuritiesReportTO.setIdIssuerPk(iterator[21].toString());
//				expirationSecuritiesReportTO.setIdHolderAccountPk(Integer.valueOf(iterator[22].toString()));
//				expirationSecuritiesReportTO.setReportingBalance(new BigDecimal(iterator[24].toString()));
//				expirationSecuritiesReportTO.setParticipantDescription(iterator[25].toString());
				
//				rutAndName = getRutAndFullname(expirationSecuritiesReportTO.getIdHolderAccountPk());
//				
//				for(int i = 0; i < rutAndName.size(); i++) {
//					expirationSecuritiesReportTO.setRut(Integer.valueOf(rutAndName.get(i)[0].toString()));
//					expirationSecuritiesReportTO.setHolderDescription(rutAndName.get(i)[1].toString());
//				}
				
				listReportData.add(expirationSecuritiesReportTO);
				
			}
			
		}
		
		
		
		return listReportData;
		
	}
	
}

