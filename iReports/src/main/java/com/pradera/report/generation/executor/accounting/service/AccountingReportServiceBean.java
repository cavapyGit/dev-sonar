package com.pradera.report.generation.executor.accounting.service;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounting.AccountingAccount;
import com.pradera.model.accounting.type.AccountingNatureAccountType;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.DailyExchangeRoleType;
import com.pradera.report.generation.executor.accounting.to.OperationBalanceTo;
import com.pradera.report.generation.executor.billing.to.DailyExchangeRateFilter;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingServiceReportServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/12/2014
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class AccountingReportServiceBean extends CrudDaoServiceBean  implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The participant service bean. */
	@EJB
	ParticipantServiceBean participantServiceBean;
	
	/** The accounting report query service bean. */
	@EJB
	AccountingReportQueryServiceBean accountingReportQueryServiceBean;
	
	/**  Begin the constant generic. */
	public static final Integer CURRENCY_TYPE_PK = Integer.valueOf(53);

	/**
	 * Instantiates a new accounting report service bean.
	 */
	public AccountingReportServiceBean(){

	}
	

	
	
	
	
	/**
	 * Find accounting.
	 *
	 * @param listAccounting the list accounting
	 * @param code the code
	 * @return the accounting account
	 */
	public AccountingAccount findAccounting(List<AccountingAccount> listAccounting, String code){
		
		
		AccountingAccount accSearch=null;
		for (AccountingAccount accountingAccount : listAccounting) {
			
			if(accountingAccount.getAccountCode().equals(code)){
				return accountingAccount;
			}
		}
		
		return accSearch;
		
	}

	/**
	 * Find account parent asc.
	 *
	 * @param accountingAccountToLocal the accounting account to local
	 * @return the list
	 */
	public List<AccountingAccount> findAccountParentAsc(AccountingAccount accountingAccountToLocal){
		
		List<AccountingAccount> listAccumulateAccountingAccount = null;
		
		/**  GETTING ONE ACCOUNTING ACCOUNT 
		 * **/
		AccountingAccount accountingAccount =accountingAccountToLocal; 
	    
	     	
	    
		if(Validations.validateIsNotNull(accountingAccount.getAccountingAccount())){
			
			/**CALL TO RECURSIVE **/
			
			AccountingAccount accAccountFather = accountingAccount.getAccountingAccount();
			
			accAccountFather.setAmount(accountingAccountToLocal.getAmount());
			accAccountFather.setNatureType(accountingAccountToLocal.getNatureType());
			accAccountFather.setIndAccumulation(accountingAccountToLocal.getIndAccumulation());
			listAccumulateAccountingAccount=findAccountParentAsc(accAccountFather);
		}

		if(accountingAccountToLocal.getNatureType().equals(AccountingNatureAccountType.DEBIT.getCode())){
			accountingAccount.setAmountDebit(accountingAccountToLocal.getAmount());
		}
		else if(accountingAccountToLocal.getNatureType().equals(AccountingNatureAccountType.ASSETS.getCode())){
			accountingAccount.setAmountAssets(accountingAccountToLocal.getAmount());
		}
		
		accountingAccount.setAmount(accountingAccountToLocal.getAmount());
	    /**agregas primero al padre y luego a lo q se acumulo en result ( en el mismo orden)**/
			
		List<AccountingAccount> listFinal = null;
		if(Validations.validateListIsNotNullAndNotEmpty(listAccumulateAccountingAccount)){
			 listAccumulateAccountingAccount.add(accountingAccount);
			 return listAccumulateAccountingAccount;
		}else{
			listFinal =new ArrayList<AccountingAccount>();		
			/*** agregas primero al padre y luego a lo q se acumulo en result ( en el mismo orden)*/
			listFinal.add(accountingAccount);
		}
		
		return listFinal ;
			
	}
	
	/**
	 * Find account parent with currency asc.
	 *
	 * @param accountingAccountToLocal the accounting account to local
	 * @return the list
	 */
	public List<AccountingAccount> findAccountParentWithCurrencyAsc(AccountingAccount accountingAccountToLocal){
		
		List<AccountingAccount> listAccumulateAccountingAccount = null;
		
		/**  GETTING ONE ACCOUNTING ACCOUNT 
		 * **/
		AccountingAccount accountingAccount =accountingAccountToLocal; 
	    
		if(Validations.validateIsNotNull(accountingAccount.getAccountingAccount())){
			
			/**CALL TO RECURSIVE **/
			
			AccountingAccount accAccountFather = accountingAccount.getAccountingAccount();
			accAccountFather.setNatureType(accountingAccount.getNatureType());
			accAccountFather.setIndAccumulation(accountingAccount.getIndAccumulation());
			listAccumulateAccountingAccount=findAccountParentWithCurrencyAsc(accAccountFather);
		}
		
	    /**agregas primero al padre y luego a lo q se acumulo en result ( en el mismo orden)**/
			
		List<AccountingAccount> listFinal = null;
		if(Validations.validateListIsNotNullAndNotEmpty(listAccumulateAccountingAccount)){
			 listAccumulateAccountingAccount.add(accountingAccount);
			 return listAccumulateAccountingAccount;
		}else{
			listFinal =new ArrayList<AccountingAccount>();		
			/*** agregas primero al padre y luego a lo q se acumulo en result ( en el mismo orden)*/
			listFinal.add(accountingAccount);
		}
		
		return listFinal ;
			
	}
	
	/**
	 * **
	 * Add ListAccounting.
	 *
	 * @param listGeneral : List To All Accounting Account
	 * @param listAccount : List To DB
	 * @return the list
	 */
	public List<AccountingAccount> addListAccountings(List<AccountingAccount> listGeneral,List<AccountingAccount> listAccount){

		for (AccountingAccount accountingAccount : listAccount) {
			for (AccountingAccount account : listGeneral) {
				
				/**Nature type**/
				if(account.getAccountCode().equals(accountingAccount.getAccountCode())){
					
					
					/**If account have the same nature type. Add amount**/
					if(accountingAccount.getNatureType().equals(account.getNatureType())){
						if(account.getNatureType().equals(AccountingNatureAccountType.DEBIT.getCode())){
							account.setAmountDebit(CommonsUtilities.addTwoDecimal(account.getAmountDebit(), accountingAccount.getAmount(), 2));
						}
						else if(account.getNatureType().equals(AccountingNatureAccountType.ASSETS.getCode())){
							account.setAmountAssets(CommonsUtilities.addTwoDecimal(account.getAmountAssets(), accountingAccount.getAmount(), 2));
						}
						
					}
					else{
						/**Other nature**/
						if(accountingAccount.getNatureType().equals(AccountingNatureAccountType.DEBIT.getCode())){
							account.setAmountDebit(CommonsUtilities.addTwoDecimal(account.getAmountDebit(), accountingAccount.getAmount(), 2));
						}
						else if(accountingAccount.getNatureType().equals(AccountingNatureAccountType.ASSETS.getCode())){
							account.setAmountAssets(CommonsUtilities.addTwoDecimal(account.getAmountAssets(), accountingAccount.getAmount(), 2));
						}
						
					}
					/**Add Movement Security Quantity  for Accounting Account **/
					BigDecimal quantity=null;
					if(AccountingNatureAccountType.DEBIT.getCode().equals(accountingAccount.getNatureType())){
						quantity=accountingAccount.getQuantity();
					}else if(AccountingNatureAccountType.ASSETS.getCode().equals(accountingAccount.getNatureType())){
						quantity=accountingAccount.getQuantity().negate();
					}
					account.setQuantity(CommonsUtilities.addTwoDecimal(account.getQuantity(),quantity,4));
					
					break;
				}
				
			}
		}
		
		return listGeneral;
	}
	
	/**
	 * **
	 * Add ListAccounting.
	 *
	 * @param listGeneral the list general
	 * @param listAccount the list account
	 * @param natureType the nature type
	 * @return the list
	 */
	public List<AccountingAccount> addListAccountings(List<AccountingAccount> listGeneral,List<AccountingAccount> listAccount,Integer natureType){

		for (AccountingAccount accountingAccount : listAccount) {
			for (AccountingAccount account : listGeneral) {
				
				/**Nature type**/
				if(account.getAccountCode().equals(accountingAccount.getAccountCode())){
					
					
					/**If account have the same nature type. Add ammount**/
					if(accountingAccount.getNatureType().equals(account.getNatureType())){
						if(account.getNatureType().equals(AccountingNatureAccountType.DEBIT.getCode())){
							account.setAmountDebit(CommonsUtilities.addTwoDecimal(account.getAmountDebit(), accountingAccount.getAmount(), 4));
						}
						else if(account.getNatureType().equals(AccountingNatureAccountType.ASSETS.getCode())){
							account.setAmountAssets(CommonsUtilities.addTwoDecimal(account.getAmountAssets(), accountingAccount.getAmount(), 4));
						}
					}
					else{
						/**Other nature**/
						if(accountingAccount.getNatureType().equals(AccountingNatureAccountType.DEBIT.getCode())){
							account.setAmountDebit(CommonsUtilities.addTwoDecimal(account.getAmountDebit(), accountingAccount.getAmount(), 4));
						}
						else if(accountingAccount.getNatureType().equals(AccountingNatureAccountType.ASSETS.getCode())){
							account.setAmountAssets(CommonsUtilities.addTwoDecimal(account.getAmountAssets(), accountingAccount.getAmount(), 4));
						}
					}
					
					account.setQuantity(CommonsUtilities.addTwoDecimal(account.getQuantity(), accountingAccount.getQuantity(), 4));
					
					break;
				}
				
			}
		}
		
		return listGeneral;
	}
	

	/**
	 * **
	 * Method To Report Squaring Balance To Accounting.
	 *
	 * @param listGeneral : List To All Accounting Account
	 * @param listAccount : List To DB
	 * @param parameters the parameters
	 * @return the list
	 */
	public List<AccountingAccount> addAccountingAccountByCurrency(List<AccountingAccount> listGeneral,
																  List<AccountingAccount> listAccount,
																  Map<String, Object> parameters){

		for (AccountingAccount accountingAccount : listAccount) {
			for (AccountingAccount account : listGeneral) {
				
				/**Nature type**/
				if(accountingAccount.getPortFolio().equals(account.getPortFolio())&&
						accountingAccount.getInstrumentType().equals(account.getInstrumentType())
						){
					
					BigDecimal amountBOB=null;
					BigDecimal amountUSD=null;
					BigDecimal amountUFV=null;
					BigDecimal amountEU=null;
					BigDecimal amountMVL=null;
					
					if(AccountingNatureAccountType.DEBIT.getCode().equals(accountingAccount.getNatureType())){
						amountBOB=accountingAccount.getAmountBOB();
						amountUSD=accountingAccount.getAmountUSD();
						amountUFV=accountingAccount.getAmountUFV();
						amountEU=accountingAccount.getAmountEU();
						amountMVL=accountingAccount.getAmountMVL();
					}else if(AccountingNatureAccountType.ASSETS.getCode().equals(accountingAccount.getNatureType())){
						amountBOB=accountingAccount.getAmountBOB().negate();
						amountUSD=accountingAccount.getAmountUSD().negate();
						amountUFV=accountingAccount.getAmountUFV().negate();
						amountEU=accountingAccount.getAmountEU().negate();
						amountMVL=accountingAccount.getAmountMVL().negate();
					}
					account.setAmountBOB(CommonsUtilities.addTwoDecimal(account.getAmountBOB(),amountBOB,GeneralConstants.TWO_VALUE_INTEGER));
					account.setAmountUSD(CommonsUtilities.addTwoDecimal(account.getAmountUSD(),amountUSD,GeneralConstants.TWO_VALUE_INTEGER));
					account.setAmountUFV(CommonsUtilities.addTwoDecimal(account.getAmountUFV(),amountUFV,GeneralConstants.TWO_VALUE_INTEGER));
					account.setAmountEU(CommonsUtilities.addTwoDecimal(account.getAmountEU(),amountEU,GeneralConstants.TWO_VALUE_INTEGER));
					account.setAmountMVL(CommonsUtilities.addTwoDecimal(account.getAmountMVL(),amountMVL,GeneralConstants.TWO_VALUE_INTEGER));

					
					break;
				}
			}

		}
		
		/**
		 * Remove To Account Without Amount
		 */
		for (Iterator iterator = listGeneral.iterator(); iterator.hasNext();) {
			AccountingAccount accountingAccount = (AccountingAccount) iterator.next();
			
			if((!Validations.validateIsNotNullAndPositive(accountingAccount.getAmountBOB()))&&
					(!Validations.validateIsNotNullAndPositive(accountingAccount.getAmountUSD()))&&
					(!Validations.validateIsNotNullAndPositive(accountingAccount.getAmountUFV()))&&
					(!Validations.validateIsNotNullAndPositive(accountingAccount.getAmountEU()))&&
					(!Validations.validateIsNotNullAndPositive(accountingAccount.getAmountMVL()))
					){
				iterator.remove();
			}
		}
		
		
		BigDecimal amountBOB	=null;
		BigDecimal amountUSD	=null;
		BigDecimal amountUFV	=null;
		BigDecimal amountMVL	=null;
		BigDecimal amountEU		=null;
		BigDecimal rate			=null;
		BigDecimal totalAmount	=null;
		
		/**
		 * Changed All Amount to Different Currency To BOB
		 */
		for (AccountingAccount accountingAccount : listGeneral) {
			
			
			amountBOB=accountingAccount.getAmountBOB();
			
			/**USD*/
			rate=processExchange(parameters,CurrencyType.USD.getCode(),
		    		CurrencyType.PYG.getCode(),DailyExchangeRoleType.BUY.getCode());
			amountUSD=CommonsUtilities.multiplyDecimals(accountingAccount.getAmountUSD(), rate, GeneralConstants.TWO_VALUE_INTEGER);
			totalAmount=CommonsUtilities.addTwoDecimal(amountBOB, amountUSD, GeneralConstants.TWO_VALUE_INTEGER);
			
			/**UFV*/
			rate=processExchange(parameters,CurrencyType.UFV.getCode(),
		    		CurrencyType.PYG.getCode(),DailyExchangeRoleType.BUY.getCode());
			amountUFV=CommonsUtilities.multiplyDecimals(accountingAccount.getAmountUFV(), rate, GeneralConstants.TWO_VALUE_INTEGER);
			totalAmount=CommonsUtilities.addTwoDecimal(totalAmount, amountUFV, GeneralConstants.TWO_VALUE_INTEGER);
			
			/**DMV*/
			rate=processExchange(parameters,CurrencyType.DMV.getCode(),
		    		CurrencyType.PYG.getCode(),DailyExchangeRoleType.BUY.getCode());
			amountMVL=CommonsUtilities.multiplyDecimals(accountingAccount.getAmountMVL(), rate, GeneralConstants.TWO_VALUE_INTEGER);
			totalAmount=CommonsUtilities.addTwoDecimal(totalAmount, amountMVL, GeneralConstants.TWO_VALUE_INTEGER);
			
			/**EU*/
			rate=processExchange(parameters,CurrencyType.EU.getCode(),
		    		CurrencyType.PYG.getCode(),DailyExchangeRoleType.BUY.getCode());
			amountEU=CommonsUtilities.multiplyDecimals(accountingAccount.getAmountEU(), rate, GeneralConstants.TWO_VALUE_INTEGER);
			totalAmount=CommonsUtilities.addTwoDecimal(totalAmount, amountEU, GeneralConstants.TWO_VALUE_INTEGER);
			
			/**Setting Amount total to Currency BOB*/
			accountingAccount.setAmountToBOB(totalAmount);
		}

		
		return listGeneral;
	}
	
	
	/**
	 * Applied Exchange Rate for Amounts for Report Accounting Balance.
	 *
	 * @param listAccount the list account
	 * @param parameters the parameters
	 * @return the list
	 */
	public List<AccountingAccount> setAmountAllCurrencies (List<AccountingAccount> listAccount,Map parameters){

		if(Validations.validateListIsNotNullAndNotEmpty(listAccount)){
			
			/**
			 * Delete Accounts without Amount
			 */
			for (Iterator iterator = listAccount.iterator(); iterator.hasNext();) {
				AccountingAccount accountingAccount = (AccountingAccount) iterator.next();
				
				if(!(Validations.validateIsNotNullAndPositive(accountingAccount.getAmountAssets())||
						Validations.validateIsNotNullAndPositive(accountingAccount.getAmountDebit()))	){
					iterator.remove();
				}
				
			}
			
			/**
			 * Set Amount by Currency
			 */
			for (AccountingAccount accountingAccount : listAccount) {
				Integer currency =accountingAccount.getCurrency();
				BigDecimal amountDebit=accountingAccount.getAmountDebit()==null?BigDecimal.ZERO:accountingAccount.getAmountDebit();
				BigDecimal amountAssets=accountingAccount.getAmountAssets()==null?BigDecimal.ZERO:accountingAccount.getAmountAssets();
				BigDecimal amount=CommonsUtilities.subtractTwoDecimal(amountDebit, amountAssets, 2);
				BigDecimal rateUSD;
				BigDecimal rateBOB;
				BigDecimal amountUSD;
				BigDecimal amountBOB;
				accountingAccount.setAmount(amount);
						
						
						
						
				/**To Set USD Currency*/
				rateUSD=processExchange(parameters,currency,
			    		CurrencyType.USD.getCode(),DailyExchangeRoleType.BUY.getCode());
				amountUSD=CommonsUtilities.multiplyDecimals(amount, rateUSD, 2);
				accountingAccount.setAmountUSD(amountUSD);
				
				/**To Set BOB Currency*/
				rateBOB=processExchange(parameters,currency,
						CurrencyType.PYG.getCode(),DailyExchangeRoleType.BUY.getCode());
				amountBOB=CommonsUtilities.multiplyDecimals(amount, rateBOB, 2);
				accountingAccount.setAmountBOB(amountBOB);
			}
		}

		return listAccount;
	}
	
	
	
	/**
	 * To Add Accounting Father to Accounting Participant.
	 *
	 * @param listAccount the list account
	 * @return the list
	 */
	public List<AccountingAccount> toAddAccountingFather(List<AccountingAccount> listAccount){

		List<AccountingAccount> listAccountWhitFather=new ArrayList<>();
		for (AccountingAccount accountingAccount : listAccount) {
			listAccountWhitFather.addAll(findAccountParentWithCurrencyAsc(accountingAccount));
		}
		
		listAccountWhitFather.removeAll(listAccount);
		TreeSet<AccountingAccount> listOrderBy=new TreeSet<AccountingAccount>(listAccountWhitFather);
		listAccountWhitFather= new ArrayList<>(listOrderBy);
		listAccountWhitFather.addAll(listAccount);
		Collections.sort(listAccountWhitFather, Collections.reverseOrder());

		for (AccountingAccount accountingAccount : listAccountWhitFather) {
			if(Validations.validateIsNotNull(accountingAccount.getAccountingAccount())){
				AccountingAccount accFather=accountingAccount.getAccountingAccount();

				BigDecimal amount=CommonsUtilities.addTwoDecimal(accountingAccount.getAmount(), accFather.getAmount(), 2);
				BigDecimal amountUSD=CommonsUtilities.addTwoDecimal(accountingAccount.getAmountUSD(), accFather.getAmountUSD(), 2);
				BigDecimal amountBOB=CommonsUtilities.addTwoDecimal(accountingAccount.getAmountBOB(), accFather.getAmountBOB(), 2);
				BigDecimal quantity=CommonsUtilities.addTwoDecimal(accountingAccount.getQuantity(), accFather.getQuantity(), 2);
				accFather.setAmount(amount);
				accFather.setAmountUSD(amountUSD);
				accFather.setAmountBOB(amountBOB);
				accFather.setQuantity(quantity);
			}
			
		}

		Collections.sort(listAccountWhitFather);
		return listAccountWhitFather;
	}
	
	/**
	 * Add Amounts according to nature Type of Accounting Account to Document Security Report
	 * and delete Accounting Account that have Amounts zero .
	 *
	 * @param listAllGeneral All Accounts
	 * @param listAccount list with Amounts
	 * @return the list
	 */
	public List<AccountingAccount> addListAccountingTotal(List<AccountingAccount> listAllGeneral,
														  List<AccountingAccount> listAccount){

		 
		Collections.sort(listAccount);
		for (AccountingAccount accountingAccount : listAccount) {
			for (AccountingAccount account : listAllGeneral) {
				if(account.getAccountCode().equals(accountingAccount.getAccountCode())){
					
					BigDecimal amountAssets=CommonsUtilities.addTwoDecimal(account.getAmountAssets(), accountingAccount.getAmountAssets(), 8);
					BigDecimal amountDebit=CommonsUtilities.addTwoDecimal(account.getAmountDebit(), accountingAccount.getAmountDebit(), 8);
					
					account.setAmountAssets(amountAssets);
					account.setAmountDebit(amountDebit);
					
					break;
				}
			}
			
		}
		for (Iterator iterator = listAllGeneral.iterator(); iterator.hasNext();) {
			AccountingAccount accountingAccount = (AccountingAccount) iterator.next();
			
			if(!(Validations.validateIsNotNullAndPositive(accountingAccount.getAmountAssets())||
					Validations.validateIsNotNullAndPositive(accountingAccount.getAmountDebit()))	){
				iterator.remove();
			}
			
		}
		
		return listAllGeneral;
	}

	
	/**
	 * Clone List T.
	 *
	 * @param <T> the generic type
	 * @param list the list
	 * @return the list
	 */
	public  static  <T> List<T>  cloneList(List<T> list) {
		List<T> clone = new ArrayList<T>(list.size());
		for (T item: list) {	
			clone.add(item);
		}
		return clone;
	}
	
	/**
	 * Get Buy Price.
	 *
	 * @param dailyExchangeRateFilter the daily exchange rate filter
	 * @return the ting buy price
	 * @throws ServiceException the service exception
	 */
	public BigDecimal gettingBuyPrice(DailyExchangeRateFilter dailyExchangeRateFilter) throws ServiceException{
		
		BigDecimal referencePrice = BigDecimal.ZERO; 
		
		if(Validations.validateIsNotNull(dailyExchangeRateFilter.getInitialDate()) &&
			 Validations.validateIsNotNull(dailyExchangeRateFilter.getFinalDate()) ){

			 DailyExchangeRates dailyExchangeRates = null; 
			 List<DailyExchangeRates> listDailyExchangeRates = 
					 accountingReportQueryServiceBean.searchExchangeMoneyTypeRandomListService(dailyExchangeRateFilter);								 
			 
			 if (Validations.validateListIsNotNullAndNotEmpty(listDailyExchangeRates)){
				 dailyExchangeRates = listDailyExchangeRates.get(0);						
				 referencePrice	= new  BigDecimal(dailyExchangeRates.getBuyPrice().toString());
			 }
			 
			 else {
				 throw new ServiceException(ErrorServiceType.BILLING_PROCESS_EXCHANGE_RATE);
			 }
			 
		 }else if (Validations.validateIsNull(dailyExchangeRateFilter.getInitialDate()) ||
			Validations.validateIsNull(dailyExchangeRateFilter.getFinalDate())){
			 
			 throw new ServiceException(ErrorServiceType.BILLING_PROCESS_EXCHANGE_RATE);
			 
		 }
		 
		 return referencePrice;
	}
	
	/**
	 * Process exchange.
	 *
	 * @param parameters the parameters
	 * @param origin the origin
	 * @param target the target
	 * @param type buySell 1 Buy 2 Sell
	 * @return the big decimal
	 */
	public BigDecimal processExchange(Map parameters, Integer origin, Integer target, Integer type){
		BigDecimal price=null;
		
		Map<Integer,Map> matrixExchange=null;
		
		if(DailyExchangeRoleType.BUY.getCode().equals(type)){
			matrixExchange=(Map<Integer,Map>) parameters.get(GeneralConstants.MATRIX_EXCHANGE_RATE_BUY);
		}else if(DailyExchangeRoleType.SELL.getCode().equals(type)){
			matrixExchange=(Map<Integer,Map>) parameters.get(GeneralConstants.MATRIX_EXCHANGE_RATE_SELL);
		}

		if(CurrencyType.DMV.getCode().equals(origin)&&
				CurrencyType.USD.getCode().equals(target)){
			return BigDecimal.ONE;
		}
		if(matrixExchange != null && !matrixExchange.isEmpty()){
			
			Map<Integer,BigDecimal> listCurrencyTarget=(Map<Integer,BigDecimal>)matrixExchange.get(origin);
			
			if(listCurrencyTarget!=null && !listCurrencyTarget.isEmpty()){

				price=(BigDecimal)listCurrencyTarget.get(target);
				
			}
				
		}
		
		return price;
	}
	
	/**
	 * Process exchange to range.
	 *
	 * @param parameters the parameters
	 * @param operationDate the operation date
	 * @param origin the origin
	 * @param target the target
	 * @param type the type
	 * @return the big decimal
	 */
	public BigDecimal processExchangeToRange(Map parameters, Date operationDate,
												Integer origin, Integer target, Integer type){
		Map<Date,Map> 		exchangeRange	=(Map<Date,Map>) parameters.get(GeneralConstants.MATRIX_EXCHANGE_RATE_RANGE);
		Map<Integer,Map> 	matrixExchange	=(Map<Integer,Map>)exchangeRange.get(operationDate);
		BigDecimal 			price			=null;
		
		if(matrixExchange != null && !matrixExchange.isEmpty()){
			
			Map<Integer,BigDecimal> listCurrencyTarget=(Map<Integer,BigDecimal>)matrixExchange.get(origin);
			
			if(listCurrencyTarget!=null && !listCurrencyTarget.isEmpty()){

				price=(BigDecimal)listCurrencyTarget.get(target);
				
			}
				
		}
		return price;
	}
	
	/**
	 * For Calculation
	 * Get Matrix with Exchange Currency Price Buy.
	 *
	 * @param dailyExchangeRateFilter the daily exchange rate filter
	 * @param buySell 1 Buy 2 Sell
	 * @return the map
	 * @throws ServiceException the service exception
	 */
	public Map<Integer,Map>  chargeExchangeMemoryBuyBs(DailyExchangeRateFilter dailyExchangeRateFilter, Integer buySell) throws ServiceException{
		
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		Map<Integer,BigDecimal> changeTarget= new HashMap<Integer,BigDecimal>();
		Map<Integer,Map> exchangeMemory= new HashMap<Integer,Map>();
		BigDecimal price=null,buyPriceTemp=null,buyPriceTempBs=null,buyPrice=null;

		parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
		parameterTableTO.setMasterTableFk(CURRENCY_TYPE_PK);
		List<ParameterTable> listCurrency=
				accountingReportQueryServiceBean.getListParameterTable(parameterTableTO);
		
		for (ParameterTable origin : listCurrency) {

			Integer currencyOrigin=origin.getIndicator4();
			changeTarget= new HashMap<Integer,BigDecimal>();
				for (ParameterTable target : listCurrency) {

					buyPriceTemp = BigDecimal.ONE;
					buyPriceTempBs = BigDecimal.ONE;
					Integer currencyTarget=target.getIndicator4();
					if(!currencyOrigin.equals(currencyTarget)){
						if(!currencyOrigin.equals(CurrencyType.PYG.getCode())){
							 
							 dailyExchangeRateFilter.setIdCurrency(currencyOrigin);
							 buyPriceTemp = gettingBuyPrice(dailyExchangeRateFilter);
							 
							 if(!currencyTarget.equals(CurrencyType.PYG.getCode())){
								 dailyExchangeRateFilter.setIdCurrency(currencyTarget);
								 buyPriceTempBs = gettingBuyPrice(dailyExchangeRateFilter);
								 buyPriceTempBs=CommonsUtilities.divideTwoDecimal(BigDecimal.ONE, buyPriceTempBs, GeneralConstants.ROUNDING_DIGIT_NUMBER_TEN);
							 }
							 price=CommonsUtilities.multiplyDecimals(buyPriceTemp, buyPriceTempBs, GeneralConstants.ROUNDING_DIGIT_NUMBER_TEN);
						 }else{
							 dailyExchangeRateFilter.setIdCurrency(currencyTarget);
							 buyPriceTemp = gettingBuyPrice(dailyExchangeRateFilter);
							 price=CommonsUtilities.divideTwoDecimal(BigDecimal.ONE, buyPriceTemp, GeneralConstants.ROUNDING_DIGIT_NUMBER_TEN);
						 }
					}else{
						price =new BigDecimal(1) ; /** FACTOR 1**/
					}
					changeTarget.put(target.getParameterTablePk(), price);
				}
				exchangeMemory.put(origin.getParameterTablePk(), changeTarget);
			}

		return exchangeMemory;
		
	}
	
	
	/**
	 * *
	 * applied.
	 *
	 * @param operationBalance the operation balance
	 * @param parameters the parameters
	 * @return the operation balance to
	 */
	
	public OperationBalanceTo appliedDailyExchangeToOperations(OperationBalanceTo operationBalance,Map<String, Object> parameters){
		BigDecimal amountBOB	=null;
		BigDecimal amountUSD	=null;
		BigDecimal amountUFV	=null;
		BigDecimal amountMVL	=null;
		BigDecimal amountEU		=null;
		BigDecimal rate			=null;
		BigDecimal totalAmount	=null;
		
		amountBOB=operationBalance.getBalanceBOB();
		
		/**USD*/
		rate=processExchange(parameters,CurrencyType.USD.getCode(),
	    		CurrencyType.PYG.getCode(),DailyExchangeRoleType.BUY.getCode());
		amountUSD=CommonsUtilities.multiplyDecimals(operationBalance.getBalanceUSD(), rate, GeneralConstants.TWO_VALUE_INTEGER);
		totalAmount=CommonsUtilities.addTwoDecimal(amountBOB, amountUSD, GeneralConstants.TWO_VALUE_INTEGER);
		
		/**UFV*/
		rate=processExchange(parameters,CurrencyType.UFV.getCode(),
	    		CurrencyType.PYG.getCode(),DailyExchangeRoleType.BUY.getCode());
		amountUFV=CommonsUtilities.multiplyDecimals(operationBalance.getBalanceUFV(), rate, GeneralConstants.TWO_VALUE_INTEGER);
		totalAmount=CommonsUtilities.addTwoDecimal(totalAmount, amountUFV, GeneralConstants.TWO_VALUE_INTEGER);
		
		/**DMV*/
		rate=processExchange(parameters,CurrencyType.DMV.getCode(),
	    		CurrencyType.PYG.getCode(),DailyExchangeRoleType.BUY.getCode());
		amountMVL=CommonsUtilities.multiplyDecimals(operationBalance.getBalanceMVL(), rate, GeneralConstants.TWO_VALUE_INTEGER);
		totalAmount=CommonsUtilities.addTwoDecimal(totalAmount, amountMVL, GeneralConstants.TWO_VALUE_INTEGER);
		
		/**EU*/
		rate=processExchange(parameters,CurrencyType.EU.getCode(),
	    		CurrencyType.PYG.getCode(),DailyExchangeRoleType.BUY.getCode());
		amountEU=CommonsUtilities.multiplyDecimals(operationBalance.getBalanceECU(), rate, GeneralConstants.TWO_VALUE_INTEGER);
		totalAmount=CommonsUtilities.addTwoDecimal(totalAmount, amountEU, GeneralConstants.TWO_VALUE_INTEGER);
		
		/**Setting Amount total to Currency BOB*/
		operationBalance.setAmountToBOB(totalAmount);
		
		
		return deleteValuesNullables(operationBalance);
		
	}
	
	/**
	 * Delete values nullables.
	 *
	 * @param operationBalance the operation balance
	 * @return the operation balance to
	 */
	public OperationBalanceTo deleteValuesNullables(OperationBalanceTo operationBalance){
		
		if(!Validations.validateIsNotNullAndPositive(operationBalance.getBalanceBOB())){
			operationBalance.setBalanceBOB(BigDecimal.ZERO);
		}
		if(!Validations.validateIsNotNullAndPositive(operationBalance.getBalanceUSD())){
			operationBalance.setBalanceUSD(BigDecimal.ZERO);
		}
		if(!Validations.validateIsNotNullAndPositive(operationBalance.getBalanceMVL())){
			operationBalance.setBalanceMVL(BigDecimal.ZERO);
		}
		if(!Validations.validateIsNotNullAndPositive(operationBalance.getBalanceUFV())){
			operationBalance.setBalanceUFV(BigDecimal.ZERO);
		}
		if(!Validations.validateIsNotNullAndPositive(operationBalance.getBalanceECU())){
			operationBalance.setBalanceECU(BigDecimal.ZERO);
		}
		if(!Validations.validateIsNotNullAndPositive(operationBalance.getAmountToBOB())){
			operationBalance.setAmountToBOB(BigDecimal.ZERO);
		}
		
		return operationBalance;
	}
	
	/**
	 * Delete value null accounting account.
	 *
	 * @param listGeneral the list general
	 */
	public void deleteValueNullAccountingAccount(List<AccountingAccount> listGeneral){
		for (Iterator iterator = listGeneral.iterator(); iterator.hasNext();) {
			AccountingAccount accountingAccount = (AccountingAccount) iterator.next();

			if(!Validations.validateIsNotNullAndPositive(accountingAccount.getAmountBOB())){
				accountingAccount.setAmountBOB(BigDecimal.ZERO);
			}
			if(!Validations.validateIsNotNullAndPositive(accountingAccount.getAmountUSD())){
				accountingAccount.setAmountUSD(BigDecimal.ZERO);
			}
			if(!Validations.validateIsNotNullAndPositive(accountingAccount.getAmountUFV())){
				accountingAccount.setAmountUFV(BigDecimal.ZERO);
			}
			if(!Validations.validateIsNotNullAndPositive(accountingAccount.getAmountEU())){
				accountingAccount.setAmountEU(BigDecimal.ZERO);
			}
			if(!Validations.validateIsNotNullAndPositive(accountingAccount.getAmountMVL())){
				accountingAccount.setAmountMVL(BigDecimal.ZERO);
			}
			
		}
	}
	

}

