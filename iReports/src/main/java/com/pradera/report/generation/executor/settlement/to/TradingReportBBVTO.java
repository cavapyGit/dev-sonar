package com.pradera.report.generation.executor.settlement.to;

import java.io.Serializable;
import java.util.Date;

public class TradingReportBBVTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3865109153954249573L;
	
	private Long participantPk;
	private Long modality;
	private String securityClass;
	private String strSettlementDate;
	
	public Long getModality() {
		return modality;
	}
	public void setModality(Long modality) {
		this.modality = modality;
	}
	public Long getParticipantPk() {
		return participantPk;
	}
	public void setParticipantPk(Long participantPk) {
		this.participantPk = participantPk;
	}
	public String getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(String securityClass) {
		this.securityClass = securityClass;
	}
	public String getStrSettlementDate() {
		return strSettlementDate;
	}
	public void setStrSettlementDate(String strSettlementDate) {
		this.strSettlementDate = strSettlementDate;
	}

}
