package com.pradera.report.generation.executor.issuances;

import java.io.ByteArrayOutputStream;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.executor.issuances.services.IssuancesReportServiceBean;
import com.pradera.report.generation.executor.issuances.to.StockOfSecuritiesByBookEntryTO;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;
import com.pradera.report.generation.service.ReportUtilServiceBean;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "StockOfSecuritiesByBookEntryReport")
public class StockOfSecuritiesByBookEntryReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	private IssuancesReportServiceBean issuancesReportService;

	@EJB
	private CustodyReportServiceBean custodyReportServiceBean;
	
	@Inject
	private PraderaLogger log;
	
	@EJB
	private ReportUtilServiceBean reportUtilServiceBean;

	private final String NEW_LINE = GeneralConstants.STR_NEW_LINEA;
	private final String COMM = GeneralConstants.STR_COMMA_WITHOUT_SPACE;
	private final String COMM_COM =  GeneralConstants.STR_COMMA_WITHOUT_SPACE+GeneralConstants.STR_COMILLA;
	private final String COM_COMM_COM = GeneralConstants.STR_COMILLA+GeneralConstants.STR_COMMA_WITHOUT_SPACE+GeneralConstants.STR_COMILLA;
	private final String COM_COMM = GeneralConstants.STR_COMILLA+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
	private final String COM =  GeneralConstants.STR_COMILLA;
	
	public StockOfSecuritiesByBookEntryReport() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		GenericsFiltersTO gfto = new GenericsFiltersTO();

		for(ReportLoggerDetail param: getReportLogger().getReportLoggerDetails()){
			if(param.getFilterName().equals("final_date")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setFinalDt(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("reportFormats")){
				if(param.getFilterDescription().equalsIgnoreCase("TEXTO"))
				if (Validations.validateIsNotNullAndPositive(Integer.valueOf(param.getFilterValue()))){
					gfto.setReportFormat(Integer.valueOf(param.getFilterValue()));
				}
			}
		}
		
		Long idStkCalcProcess= null;
		
		Date cuteoff = CommonsUtilities.convertStringtoDate(gfto.getFinalDt());
		Calendar calendario = Calendar.getInstance();
		calendario.setTime(cuteoff);
		calendario.add(Calendar.DATE, -7);
		gfto.setInitialDt(gfto.getFinalDt());
		
		String strQuery = "";
		
		strQuery = issuancesReportService.getQueryStockOfSecuritiesByBookEntryReport(gfto, idStkCalcProcess); 
		String strQueryFormatted = issuancesReportService.getQueryFormatForJasper(gfto, strQuery, idStkCalcProcess);
		
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> secClassCurrency 			= new HashMap<Integer, String>();
		Map<Integer,String> secClassMnemonic 			= new HashMap<Integer, String>();
		Map<Integer,String> secClassSecurityMnemo 		= new HashMap<Integer, String>();
		try {

			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secClassCurrency.put(param.getParameterTablePk(), param.getParameterName());
				secClassMnemonic.put(param.getParameterTablePk(), "("+param.getDescription()+")");
			}
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secClassSecurityMnemo.put(param.getParameterTablePk(), param.getText1());
			}
			
			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		if(ReportFormatType.TXT.getCode().equals(gfto.getReportFormat())){
	    	strQueryFormatted = issuancesReportService.getQueryFormatForJasper(gfto, strQuery, null);
			generateReportTXT(gfto,strQueryFormatted);
	    	return null;
		}

		parametersRequired.put("str_Query", strQueryFormatted);
		parametersRequired.put("mCurrency", secClassCurrency);
		parametersRequired.put("mMnemonic", secClassMnemonic);
		parametersRequired.put("finalDt", gfto.getFinalDt());
		parametersRequired.put("mSecurity", secClassSecurityMnemo);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());

		return parametersRequired;
	}
	private void generateReportTXT(GenericsFiltersTO gfto,String strQuery){
		StringBuilder sbTxtFormat = new StringBuilder();
		String fileName = getNameAccordingFund(gfto)+"CPY";
		List<StockOfSecuritiesByBookEntryTO> bookEntryTOs = issuancesReportService.getQueryStockOfSecuritiesByBookEntryTO(strQuery);
		if(!Validations.validateIsNotNullAndNotEmpty(bookEntryTOs)){
			sbTxtFormat.append(COM+"SIN DATOS"+COM);
			reportUtilServiceBean.saveCustomReportFile(fileName, ReportFormatType.TXT.getCode(), sbTxtFormat.toString().getBytes(), getReportLogger());
			return;
		}
		sbTxtFormat.append(COM.concat("CPY").concat(COM).concat(","));
		sbTxtFormat.append(COM.concat("FECHA CORTE").concat(COM).concat(","));
		sbTxtFormat.append(COM.concat("CLASE VALOR").concat(COM).concat(","));
		sbTxtFormat.append(COM.concat("MONEDA").concat(COM).concat(","));
		sbTxtFormat.append(COM.concat("VALOR NOMINAL").concat(COM).concat(","));
		sbTxtFormat.append(COM.concat("BALANCE TOTAL").concat(COM));
		sbTxtFormat.append(NEW_LINE);
		sbTxtFormat.append(getStockSecuritiesByBook(bookEntryTOs,CurrencyType.PYG.getCode(),gfto));
//		sbTxtFormat.append(GeneralConstants.STR_NEW_LINEA);
		sbTxtFormat.append(getStockSecuritiesByBook(bookEntryTOs,CurrencyType.UFV.getCode(),gfto));
//		sbTxtFormat.append(GeneralConstants.STR_NEW_LINEA);
		sbTxtFormat.append(getStockSecuritiesByBook(bookEntryTOs,CurrencyType.DMV.getCode(),gfto));
//		sbTxtFormat.append(GeneralConstants.STR_NEW_LINEA);
		sbTxtFormat.append(getStockSecuritiesByBook(bookEntryTOs,CurrencyType.EU.getCode(),gfto));
//		sbTxtFormat.append(GeneralConstants.STR_NEW_LINEA);
		sbTxtFormat.append(getStockSecuritiesByBook(bookEntryTOs,CurrencyType.USD.getCode(),gfto));

		reportUtilServiceBean.saveCustomReportFile(fileName, ReportFormatType.TXT.getCode(), sbTxtFormat.toString().getBytes(), getReportLogger());
	}
	/**
	 * Obtiene de la lista la linea(texto) para una clase de valor y moneda
	 * */
	private String getStockSecuritiesByBook(List<StockOfSecuritiesByBookEntryTO> bookEntryTOs,Integer currency, GenericsFiltersTO gfto){
		StringBuilder txtFormat = new StringBuilder();
		boolean firstLoop=true;
		for (StockOfSecuritiesByBookEntryTO to : bookEntryTOs) {
			if(!to.getCurrency().equals(currency))
				continue;
			if(to.getCurrency().equals(CurrencyType.PYG.getCode())
					&& firstLoop){
				firstLoop = false;
			}else{
				txtFormat.append(NEW_LINE);	
			}

			txtFormat.append(COM+"CPY"+
			/*1*/
			COM_COMM_COM+getDateString(gfto.getFinalDt())+
			/*2*/
			COM_COMM_COM+to.getSecurtyClassMnemonic()+
		    /*3*/
			COM_COMM_COM+getCurrencyDescription(to.getCurrency())+
		    /*4*/
			COM_COMM_COM+to.getNominalValue().setScale(2, RoundingMode.HALF_UP)+
		    /*5*/
			COM_COMM_COM+to.getTotalBalance()+COM);
			
		}
		return txtFormat.toString();
	}
	private long getAmountValues(List<StockOfSecuritiesByBookEntryTO> bookEntryTOs, Integer currency){
		long count = 0;
		for (StockOfSecuritiesByBookEntryTO to : bookEntryTOs) 
			if(to.getCurrency().equals(currency))
				count = count+1;
		return count;
	}
	
	private String getCurrencyDescription(Integer currency) {
		if(currency.equals(CurrencyType.PYG.getCode()))
		    return "PYG";
		if(currency.equals(CurrencyType.USD.getCode()))
		    return "USD";
		
		return null;
	}
	
	private int getCurrencyAsfi(Integer currency){
		if(currency.equals(CurrencyType.PYG.getCode()))
			return 10;
		if(currency.equals(CurrencyType.DMV.getCode()))
			return 11;
		if(currency.equals(CurrencyType.USD.getCode()))
			return 12;
		if(currency.equals(CurrencyType.UFV.getCode()))
			return 14;
		if(currency.equals(CurrencyType.EU.getCode()))
			return 15;
		return 0;
	}
	private String getDateString(String templateDate){
		Date date = CommonsUtilities.convertStringtoDate(templateDate, GeneralConstants.DATE_FORMAT_PATTERN);
		return CommonsUtilities.convertDateToString(date,GeneralConstants.DATE_FORMAT_PATTERN_TXT);
	}
	private String getNameAccordingFund(GenericsFiltersTO gfto){
		String name="";
		String firstWord="M";
		String strDate= gfto.getFinalDt();
		String strDay = strDate.substring(0,2);
		String strMonth = strDate.substring(3,5);
		String strYear = strDate.substring(8,10);
		String lastWord="AR";
		String reportCode="";

		name=firstWord;
		name+=strYear+strMonth+strDay;
		name+=lastWord;
		name+=GeneralConstants.DOT+reportCode;
		return name;
	}
}
