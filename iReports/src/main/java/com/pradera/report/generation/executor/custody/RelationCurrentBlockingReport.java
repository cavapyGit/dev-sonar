package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.management.RuntimeErrorException;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.service.HolderQueryServiceBean;
import com.pradera.core.component.helperui.service.IssuerQueryServiceBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.executor.custody.to.GenericCustodyOperationTO;
import com.pradera.report.util.view.UtilReportConstants;


@ReportProcess(name="RelationCurrentBlockingReport")
public class RelationCurrentBlockingReport extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@Inject  PraderaLogger log; 
	
	@EJB
	ParticipantServiceBean participantServiceBean;
	
	@EJB
	IssuerQueryServiceBean issuerServiceBean; 
	
	@EJB
	CustodyReportServiceBean custodyReportServiceBean;
	
	@EJB
	HolderQueryServiceBean holderQueryServiceBean;
	
	@EJB
	private HolderAccountComponentServiceBean accountComponentServiceBean;
	
	private GenericCustodyOperationTO genericCustodyOperationTO; 
	
	public RelationCurrentBlockingReport() {

	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void addParametersQueryReport() {}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		
		Map<Integer, String> blockType = new HashMap<Integer, String>();
		
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		genericCustodyOperationTO = new GenericCustodyOperationTO();
		String issuerCode = null;
		String issuerDescription = null;
		String participantCode = null;
		String participantDescription = null;
		String date = null;
		String blockTypeParam = null;
		String holderCode = null;
		String holderDescription = null;
		String security = null;
		String holderAccount = null;
		String currency = null;
		String securityClass = null;
		Map<Integer,String> currencyType = new HashMap<Integer, String>();
		
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.PARTICIPANT_PARAM) && r.getFilterValue()!=null){
				participantCode = r.getFilterValue();
				genericCustodyOperationTO.setParticipant(participantCode);
			}
			if (r.getFilterName().equals(ReportConstant.ISSUER_PARAM)){
				issuerCode = r.getFilterValue();
				genericCustodyOperationTO.setIssuer(issuerCode);
			}
			if (r.getFilterName().equals(ReportConstant.CUTTOFDATE)){
				date = r.getFilterValue();
				genericCustodyOperationTO.setInitialDate(date);
			}
			if (r.getFilterName().equals(ReportConstant.BLOCK_TYPE_PARAM)){
				blockTypeParam = r.getFilterValue();
				genericCustodyOperationTO.setBlockType(blockTypeParam);
			}
			if (r.getFilterName().equals(ReportConstant.CUI_HOLDER_PARAM)){
				genericCustodyOperationTO.setHolder(r.getFilterValue());
				holderCode = r.getFilterValue();
			}
			if (r.getFilterName().equals(ReportConstant.CURRENCY_TYPE_PARAM)){
				genericCustodyOperationTO.setCurrency(r.getFilterValue());
			}
			if (r.getFilterName().equals(ReportConstant.HOLDER_ACCOUNT)){
				genericCustodyOperationTO.setHolderAccount(r.getFilterValue());
			}
			if (r.getFilterName().equals(ReportConstant.SECURITY_CLASS_PARAM)){
				genericCustodyOperationTO.setSecurityClass(r.getFilterValue());
			}
			if (r.getFilterName().equals(ReportConstant.SECURITY_KEY)){
				genericCustodyOperationTO.setSecurities(r.getFilterValue());
				security = r.getFilterValue();
			}
			if (r.getFilterName().equals(ReportConstant.INSTITUTION_TYPE)){
				genericCustodyOperationTO.setInstitutionType(r.getFilterValue());
			}			
		}
		
		Long idStockCalculation = null;
		Date cutdate = CommonsUtilities.convertStringtoDate(date);
		
		try {
			if(cutdate.before(CommonsUtilities.currentDate())){
				idStockCalculation = custodyReportServiceBean.searchIdStockCalculationProcess(cutdate);
			}
		} catch (ServiceException e) {
			if(e.getErrorService().equals(ErrorServiceType.STOCK_CALCULATION_PROCESS_NOT_EXIST)){
				throw new RuntimeErrorException(null, ReportConstant.ERROR_STOCK_CALCULATION_PROCESS_NOT_EXIST + " " + date);
			}
			log.error(e.getMessage());
		}

		try {
			
			if(participantCode != null){
				Participant participant = participantServiceBean.find(Participant.class, new Long(participantCode));
				participantDescription = participant.getMnemonic() + " - " + participant.getDescription();
			}else{
				participantDescription = "TODOS";
			}
			
			if(issuerCode != null){
				Issuer issuer = issuerServiceBean.find(Issuer.class, issuerCode);
				issuerDescription = issuer.getMnemonic();
			}else{
				issuerDescription = "TODOS";
			}
			
			if(holderCode != null){
				Holder holder = holderQueryServiceBean.find(Holder.class, new Long(holderCode));
				holderDescription = holder.getIdHolderPk() + " - "+  holder.getFullName();
			}else{
				holderDescription = "TODOS";
			}
			
			if(genericCustodyOperationTO.getHolderAccount() != null)
				holderAccount = accountComponentServiceBean.find(new Long(genericCustodyOperationTO.getHolderAccount()),HolderAccount.class).getAccountNumber().toString();
			else
				holderAccount = "TODOS";
			
			if(genericCustodyOperationTO.getSecurityClass() != null){
				ParameterTable parameterClass = parameterService.getParameterTableById(Integer.parseInt(genericCustodyOperationTO.getSecurityClass()));
				securityClass=parameterClass.getParameterName();
			}else{
				securityClass = "TODOS";
			}
			
			if(genericCustodyOperationTO.getCurrency() != null){
				ParameterTable parameterClass = parameterService.getParameterTableById(Integer.parseInt(genericCustodyOperationTO.getCurrency()));
				currency = parameterClass.getParameterName();
			}else{
				currency = "TODOS";
			}
			
			if(security == null){
				security = "TODOS";
			}
			
			
			filter.setMasterTableFk(MasterTableType.TYPE_AFFECTATION.getCode());
			for (ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				blockType.put(param.getParameterTablePk(), param.getDescription());
			}
			
			String strQuery = custodyReportServiceBean.getRelationCurrentBlock(genericCustodyOperationTO, idStockCalculation);

			parametersRequired.put("p_participant", participantDescription);
			parametersRequired.put("p_issuer", issuerDescription);
			parametersRequired.put("p_blockType", blockType);
			parametersRequired.put("idStockCalculation", idStockCalculation);
			parametersRequired.put("p_holderDescription", holderDescription);
			parametersRequired.put("p_securityClass", securityClass);
			parametersRequired.put("p_currency", currency);
			parametersRequired.put("p_securityCode", security);
			parametersRequired.put("p_cutDate", date);
			parametersRequired.put("p_holderAccount", holderAccount);
			
			parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
			parametersRequired.put("strQuery", strQuery);
		
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		// TODO Auto-generated method stub
		return parametersRequired;
	}
	
}
