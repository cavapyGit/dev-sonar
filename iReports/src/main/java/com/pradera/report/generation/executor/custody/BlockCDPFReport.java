package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import com.pradera.model.generalparameter.ParameterTable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.service.IssuerQueryServiceBean;
import com.pradera.core.component.helperui.to.IssuerSearcherTO;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.util.view.UtilReportConstants;


@ReportProcess(name="BlockCDPFReport")
public class BlockCDPFReport extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@Inject  PraderaLogger log; 
	
	@EJB
	ParticipantServiceBean participantServiceBean;

	@EJB
	private IssuerQueryServiceBean issuerQueryServiceBean;

	@EJB
	private HolderAccountComponentServiceBean accountComponentServiceBean;
	
	public BlockCDPFReport() {
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void addParametersQueryReport() {
	}
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		IssuerSearcherTO issuerSearcherTO= new IssuerSearcherTO();
		Map<Integer,String> TypeBlock = new HashMap<Integer, String>();
		Map<Integer,String> mnemonic_sc = new HashMap<Integer, String>();
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		String holderAccountCode,issuerCode= null;
		String issuerName = null;
		String securitiesClass = null;
		String state = null;
		String stateDescription = null;
		String securitieClassDesc = null;
		// DATOS DE ENTRADA DEL SISTEMA
		for (ReportLoggerDetail r : loggerDetails) {	
			if (r.getFilterName().equals(ReportConstant.ACCOUNT_HOLDER))
				holderAccountCode = r.getFilterValue();
			if (r.getFilterName().equals("p_issuer_account"))
				issuerCode = r.getFilterValue();
			if (r.getFilterName().equals("p_value_class"))
				securitiesClass = r.getFilterValue();
			if (r.getFilterName().equals("p_state"))
				state = r.getFilterValue();
		}
		try {
			if (securitiesClass!=null)
			{	
				ParameterTable parameterClass = parameterService.getParameterTableById(Integer.parseInt(securitiesClass));
				securitieClassDesc=parameterClass.getParameterName();
			} else
				securitieClassDesc="TODOS";
			if(state!=null){
				ParameterTable parameterClass = parameterService.getParameterTableById(Integer.parseInt(state));
				stateDescription=parameterClass.getParameterName();
			} else 
				stateDescription="TODOS";
			if (issuerCode!=null){
				issuerSearcherTO.setIssuerCode(issuerCode);
				Issuer  issuer=issuerQueryServiceBean.getIssuerByCode(issuerSearcherTO);
				issuerName=issuer.getMnemonic()+" - "+issuerCode+" - " +issuer.getBusinessName();
			}
			else issuerName="TODOS";
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		// TODO Auto-generated method stub
		parametersRequired.put("issuerName", issuerName);
		parametersRequired.put("securitieClassDesc", securitieClassDesc);
		parametersRequired.put("stateDescription", stateDescription);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		return parametersRequired;
		
	}

}
