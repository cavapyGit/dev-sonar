package com.pradera.report.generation.executor.corporative.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class CorporativeProcessResultTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long rntCode;
	
	private Long participant;
	
	private Integer accountNumber;
	
	private String isinCode;
	
	private Integer benefitType;
	
	private Date initialDate;
	
	private Date finalDate;
	
	private Integer indOriginTarget;
	
	private List<Integer>benefitList;
	
	public Long getRntCode() {
		return rntCode;
	}
	public void setRntCode(Long rntCode) {
		this.rntCode = rntCode;
	}
	public Long getParticipant() {
		return participant;
	}
	public void setParticipant(Long participant) {
		this.participant = participant;
	}
	public Integer getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getIsinCode() {
		return isinCode;
	}
	public void setIsinCode(String isinCode) {
		this.isinCode = isinCode;
	}
	public Integer getBenefitType() {
		return benefitType;
	}
	public void setBenefitType(Integer benefitType) {
		this.benefitType = benefitType;
	}
	public Date getInitialDate() {
		return initialDate;
	}
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	public Date getFinalDate() {
		return finalDate;
	}
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}
	public Integer getIndOriginTarget() {
		return indOriginTarget;
	}
	public void setIndOriginTarget(Integer indOriginTarget) {
		this.indOriginTarget = indOriginTarget;
	}
	public List<Integer> getBenefitList() {
		return benefitList;
	}
	public void setBenefitList(List<Integer> benefitList) {
		this.benefitList = benefitList;
	}
}
