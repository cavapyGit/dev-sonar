package com.pradera.report.generation.executor.security;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.security.service.AuditConsolidatedServiceBean;
import com.pradera.report.generation.executor.security.to.AuditTrackTO;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

@ReportProcess(name="AuditSensitiveFieldsReport")
public class AuditSensitiveFieldsReport extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6417535267455863179L;
	
	@Inject
	PraderaLogger log;
	
	@EJB
	private AuditConsolidatedServiceBean auditReportService;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	private static int TABLE_NAME = 0;
	private static int COLUMN_NAME = 1;
	private static int REGISTRY_DATE = 2;
	private static int EVENT_TYPE= 3;
	private static int USER_REGISTRY = 4;
	private static int ID_AUDIT_TRACK_LOG = 5;
	private static int OLD_VALUE = 6;
	private static int NEW_VALUE = 7;
	private static int TRIGGER_NAME = 8;
	private static int PRIMARY_KEY_ROW = 9;

	public AuditSensitiveFieldsReport() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		AuditTrackTO auditTrackTO = new AuditTrackTO();
		Boolean existData = false;

		//Read parameters from DB, all parameters are string type
		for(ReportLoggerDetail loggerDetail : reportLogger.getReportLoggerDetails()) {
			if(loggerDetail.getFilterName().equals(ReportConstant.SYSTEM)) {
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					auditTrackTO.setIdSystemPk(Integer.valueOf(loggerDetail.getFilterValue()));										
				}				
			} else if(loggerDetail.getFilterName().equals(ReportConstant.TABLE)) {
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					auditTrackTO.setTableName(loggerDetail.getFilterValue());					
				}
			} else if(loggerDetail.getFilterName().equals(ReportConstant.COLUMN)) {
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					auditTrackTO.setColumnName(loggerDetail.getFilterValue());					
				}
			} else if(loggerDetail.getFilterName().equals(ReportConstant.USER)) {
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					auditTrackTO.setUserName(loggerDetail.getFilterValue());					
				}
			} else if(loggerDetail.getFilterName().equals(ReportConstant.DML_ACTION_TYPE)) {
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					auditTrackTO.setEventType(Integer.valueOf(loggerDetail.getFilterValue()));					
				}
			} else if(loggerDetail.getFilterName().equals(ReportConstant.START_DATE)) {
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					auditTrackTO.setStartDate(CommonsUtilities.convertStringtoDate(loggerDetail.getFilterValue()));					
				}
			} else if(loggerDetail.getFilterName().equals(ReportConstant.END_DATE)) {
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					auditTrackTO.setEndDate(CommonsUtilities.convertStringtoDate(loggerDetail.getFilterValue()));					
				}
			}
			
		}
		//checking parameter
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
		List<Object[]> lstAuditFields = auditReportService.getAuditedSensitiveFieldList(auditTrackTO);
		if(!lstAuditFields.isEmpty()) {
			existData = true;			
		}
		
		
		XMLOutputFactory xof = XMLOutputFactory.newInstance();
		XMLStreamWriter xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(baos));
		//open document file
		xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");
		//root tag of report
		/**
		 * report
		 * */
		xmlsw.writeStartElement(ReportConstant.REPORT);
		/**
		 * Create to header report
		 * start_hour
		 * report_title
		 * mnemonic_report
		 * clasification_report
		 * generation_date
		 * user_name
		 * mnemonic_entity
		 * **/
		createHeaderReport(xmlsw, reportLogger);
		xmlsw.writeStartElement("auditFields");
		if(existData) {
			//method specific to each implementation of reports
			createBodyReport(xmlsw, lstAuditFields);
		} else {
			//when not exist data we need to create empty structure xml
			xmlsw.writeStartElement("auditField");					
			xmlsw.writeEndElement();
		}
//		createTagString(xmlsw, "totalParticipants", participants.size());
		//close participantClass
		xmlsw.writeEndElement();
		
		//Report Id
		createTagString(xmlsw,ReportConstant.REPORT_ID,reportLogger.getReport().getIdReportPk());
		
		//time finish and parameters
		String hourEnd = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),
				ReportConstant.DATE_FORMAT_TIME_SECOND);
		createTagString(xmlsw, ReportConstant.END_HOUR, hourEnd);
		//parameters
		createTagString(xmlsw, ReportConstant.START_DATE, CommonsUtilities.convertDatetoString(auditTrackTO.getStartDate()));
		createTagString(xmlsw, ReportConstant.END_DATE, CommonsUtilities.convertDatetoString(auditTrackTO.getEndDate()));
		createTagString(xmlsw, ReportConstant.SYSTEM_NAME, auditReportService.getSystemDescriptionById(auditTrackTO.getIdSystemPk()));
				
		createTagString(xmlsw,ReportConstant.DATA_FOUND,BooleanType.get(existData).name());		
		
		//close report tag
		xmlsw.writeEndElement();
		//close document
		xmlsw.writeEndDocument();
		xmlsw.flush();
        xmlsw.close();
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		return baos;
	}
	
	
	public void createBodyReport(XMLStreamWriter xmlsw, List<Object[]> lstAuditFields)  throws XMLStreamException{

		for(Object[] objAuditField : lstAuditFields) {

			//detail report
			xmlsw.writeStartElement("auditField");
			
			if(Validations.validateIsNotNullAndNotEmpty(objAuditField[TABLE_NAME])){
				createTagString(xmlsw, "table_name", objAuditField[TABLE_NAME]);
			} else {
				createTagString(xmlsw, "table_name", GeneralConstants.EMPTY_STRING);
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(objAuditField[COLUMN_NAME])){
				createTagString(xmlsw, "column_name", objAuditField[COLUMN_NAME]);
			} else {
				createTagString(xmlsw, "column_name", GeneralConstants.EMPTY_STRING);
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(objAuditField[REGISTRY_DATE])){
				createTagString(xmlsw, "reg_date", CommonsUtilities.convertDatetoString((Date)objAuditField[REGISTRY_DATE]));
			} else {
				createTagString(xmlsw, "reg_date", GeneralConstants.EMPTY_STRING);
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(objAuditField[REGISTRY_DATE])){
				createTagString(xmlsw, "reg_time", CommonsUtilities.convertDatetoTimeString((Date)objAuditField[REGISTRY_DATE]));
			} else {
				createTagString(xmlsw, "reg_time", GeneralConstants.EMPTY_STRING);
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(objAuditField[EVENT_TYPE])){
				createTagString(xmlsw, "event_type", objAuditField[EVENT_TYPE]);
			} else {
				createTagString(xmlsw, "event_type", GeneralConstants.EMPTY_STRING);
			}
			

			if(Validations.validateIsNotNullAndNotEmpty(objAuditField[USER_REGISTRY])){
				createTagString(xmlsw, "user_registry", objAuditField[USER_REGISTRY]);
			} else {
				createTagString(xmlsw, "user_registry", GeneralConstants.EMPTY_STRING);
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(objAuditField[ID_AUDIT_TRACK_LOG])){
				createTagString(xmlsw, "id_audit_track_pk", objAuditField[ID_AUDIT_TRACK_LOG]);
			} else {
				createTagString(xmlsw, "id_audit_track_pk", GeneralConstants.EMPTY_STRING);
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(objAuditField[OLD_VALUE])){
				createTagString(xmlsw, "old_value", objAuditField[OLD_VALUE]);
			} else {
				createTagString(xmlsw, "old_value", GeneralConstants.EMPTY_STRING);
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(objAuditField[NEW_VALUE])){
				createTagString(xmlsw, "new_value", objAuditField[NEW_VALUE]);
			} else {
				createTagString(xmlsw, "new_value", GeneralConstants.EMPTY_STRING);
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(objAuditField[TRIGGER_NAME])){
				createTagString(xmlsw, "trigger_name", objAuditField[TRIGGER_NAME]);
			} else {
				createTagString(xmlsw, "trigger_name", GeneralConstants.EMPTY_STRING);
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(objAuditField[PRIMARY_KEY_ROW])){
				createTagString(xmlsw, "primary_key_row", objAuditField[PRIMARY_KEY_ROW]);
			} else {
				createTagString(xmlsw, "primary_key_row", GeneralConstants.EMPTY_STRING); 
			}
				
			xmlsw.writeEndElement();
		}
		
	}

}
