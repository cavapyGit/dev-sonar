package com.pradera.report.generation.executor.corporative;

import java.io.ByteArrayOutputStream;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.corporateevents.to.CorporativeOperationTO;
import com.pradera.core.component.helperui.service.SecuritiesQueryServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.corporative.service.CorporativeReportServiceBean;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

@ReportProcess(name="AmortizationPaymentAccHdrPartReport")
public class AmortizationPaymentAccHdrPartReport extends GenericReport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private static int CUENTA 						= 0;
	private static int ID_PARTICIPANT_FK 			= 1;
	private static int RNT_DESCRIPCION 				= 2;
	private static int EMISOR 						= 3;
	private static int ID_ISSUANCE_CODE_FK 			= 4;
	private static int VALOR 						= 5;
	private static int CURRENCY 					= 6;
	private static int CUTOFF_DATE 					= 7;
	private static int DELIVERY_DATE 				= 8;
	private static int REGISTRY_DATE 				= 9;
	private static int COUPON_NUMBER 				= 10;
	private static int VALOR_NOMINAL 				= 11;
	private static int FACTOR_INT 					= 12;
	private static int IMPUESTO 					= 13;
	private static int IND_ROUND 					= 14;
	private static int ID_ORIGIN_ISIN_CODE_FK 		= 15;
	private static int ID_HOLDER_ACCOUNT_FK 		= 16;
	private static int SALDO_CONTABLE 				= 17;
	private static int SALDO_DISPONIBLE 			= 18;
	private static int participant_id 				= 19;
	private static int AMORTIZACION_BRUTA 			= 20;
	private static int COMISION_CUSTODIA 			= 21;
	private static int AMORTIZACION_NETA_CONTABLE 	= 22;
	private static int AMORTIZACION_NETA_DISPONIBLE	= 23;
	private static int STATE 						= 24;
	private static int RETENCION 					= 25;
	private static int HOLDER_COUNT 				= 26;
	private static int ID_HOLDER_ACCOUNT_PK 		= 27;
	
	
	
	
	/** The corporative report service bean. */
	@EJB
	CorporativeReportServiceBean corporativeReportServiceBean;
	
	/** The securities query service bean. */
	@EJB
	SecuritiesQueryServiceBean securitiesQueryServiceBean;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		boolean existData = false;
		CorporativeOperationTO corporativeOpTO = new CorporativeOperationTO();
		
		//Read parameters from DB, all parameters are string type
				for(ReportLoggerDetail loggerDetail : reportLogger.getReportLoggerDetails()) {	
					
					if(loggerDetail.getFilterName().equals(ReportConstant.CORP_PROCESS_ID) &&
							Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	){
						corporativeOpTO.setId(Long.valueOf(loggerDetail.getFilterValue()));
						}
					
					if(loggerDetail.getFilterName().equals(ReportConstant.CORPORATE_EVENT_TYPE) &&
							Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	) {
						corporativeOpTO.setCorporativeEventType(Integer.valueOf(loggerDetail.getFilterValue()));
						}
						
					if(loggerDetail.getFilterName().equals(ReportConstant.REPORT_TYPE) &&
							Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	){
						corporativeOpTO.setReportType(Integer.valueOf(loggerDetail.getFilterValue()));
						}
					if(loggerDetail.getFilterName().equals(ReportConstant.STATE_PARAM) &&
							Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	) {
						corporativeOpTO.setState(Integer.valueOf(loggerDetail.getFilterValue()));
						}
						
					if(loggerDetail.getFilterName().equals(ReportConstant.DEST_CODE_ISIN) &&
							Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	){
						corporativeOpTO.setTargetIsinCode(loggerDetail.getFilterValue());
						}
							
					if(loggerDetail.getFilterName().equals(ReportConstant.CUTOFF_DATE) &&
							Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	){
						corporativeOpTO.setCutOffDate(CommonsUtilities.convertStringtoDate(loggerDetail.getFilterValue()));
						}
					
					if(loggerDetail.getFilterName().equals(ReportConstant.DELIVERY_DATE) &&
							Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	) {
						corporativeOpTO.setDeliveryDate(CommonsUtilities.convertStringtoDate(loggerDetail.getFilterValue()));
						}
						
					if(loggerDetail.getFilterName().equals(ReportConstant.CODE_ISIN) &&
							Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	){
						corporativeOpTO.setSourceIsinCode(loggerDetail.getFilterValue());
						}
				}
				
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				
				try {
					//GETTING ALL DATA FROM QUERY
					List<Object[]> listObjects = corporativeReportServiceBean.getAmortizationPaymentAccHdrPart(corporativeOpTO);

					Security sec = securitiesQueryServiceBean.getSecurityHelpServiceBean(corporativeOpTO.getSourceIsinCode());
					
					if(Validations.validateIsNotNullAndNotEmpty(listObjects) && listObjects.size()>0){
						existData = true;
					}
					
					XMLOutputFactory xof = XMLOutputFactory.newInstance();
					XMLStreamWriter xmlsw;
					
					
						xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(baos));
						//open document file
						xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");
						
						//REPORT TAG
						xmlsw.writeStartElement(ReportConstant.REPORT);
						
						//BODY
						if(existData){
							//HEADER
							createHeaderReport(xmlsw,reportLogger);	
							
							xmlsw.writeStartElement("participant"); //START HEADER REPORT ELEMENT	
								
								createTagString(xmlsw, "VALOR", sec.getIdSecurityCodePk()+" - "+sec.getDescription());
								createTagString(xmlsw, "EMISOR", sec.getIssuer().getIdIssuerPk()+" - "+sec.getIssuer().getBusinessName());
								createTagString(xmlsw, "CUTOFF_DATE", CommonsUtilities.convertDateToString(corporativeOpTO.getCutOffDate(),"dd/MM/YYYY"));
								createTagString(xmlsw, "VALOR_NOMINAL", sec.getCurrentNominalValue());
								createTagString(xmlsw, "ID_ISSUANCE_CODE_FK", sec.getIssuance().getIdIssuanceCodePk());
								createTagString(xmlsw, "CURRENCY", CurrencyType.get(sec.getCurrency()).getValue());
								createTagString(xmlsw, "DELIVERY_DATE", CommonsUtilities.convertDateToString(corporativeOpTO.getDeliveryDate(),"dd/MM/YYYY"));
								createTagString(xmlsw, "FACTOR_INT", listObjects.get(0)[FACTOR_INT].toString());
								createTagString(xmlsw, "REGISTRY_DATE", listObjects.get(0)[REGISTRY_DATE].toString());
								createTagString(xmlsw, "IMPUESTO", listObjects.get(0)[IMPUESTO].toString());
								createTagString(xmlsw, "COUPON_NUMBER", listObjects.get(0)[COUPON_NUMBER].toString());
								createTagString(xmlsw, "IND_ROUND", listObjects.get(0)[IND_ROUND].toString());
								
								createBodyReport(xmlsw, listObjects);
								
							xmlsw.writeEndElement(); //END HEADER REPORT ELEMENT					
							
						}
					
						//END REPORT TAG
						xmlsw.writeEndElement();
						//close document
						xmlsw.writeEndDocument();
						xmlsw.flush();
				        xmlsw.close();
							
				} catch (XMLStreamException | ServiceException e) {
					log.error(e.getMessage());
					e.printStackTrace();
					throw new RuntimeException();
				} 
					
	 return baos;
	}
	
	public void createBodyReport(XMLStreamWriter xmlsw,List<Object[]> listObjects)  throws XMLStreamException{
		xmlsw.writeStartElement("rows"); //START ROWS TAG	
		
		//BY ROW
		for(Object[] obj : listObjects){
			xmlsw.writeStartElement("row"); //START ROW TAG
			
			createTagString(xmlsw, "CUENTA", obj[CUENTA]);
			createTagString(xmlsw, "ID_PARTICIPANT_FK", obj[ID_PARTICIPANT_FK]);
			createTagString(xmlsw, "RNT_DESCRIPCIÓN", obj[RNT_DESCRIPCION]);
			createTagString(xmlsw, "EMISOR" , obj[EMISOR]);
			createTagString(xmlsw, "ID_ISSUANCE_CODE_FK", obj[ID_ISSUANCE_CODE_FK]);
			createTagString(xmlsw, "ID_ORIGIN_ISIN_CODE_FK", obj[ID_ORIGIN_ISIN_CODE_FK]);
			createTagString(xmlsw, "ID_HOLDER_ACCOUNT_FK", obj[ID_HOLDER_ACCOUNT_FK]);
			createTagString(xmlsw, "SALDO_CONTABLE", obj[SALDO_CONTABLE]);
			createTagString(xmlsw, "SALDO_DISPONIBLE", obj[SALDO_DISPONIBLE]);
			createTagString(xmlsw, "participant_id", obj[participant_id]);
			createTagString(xmlsw, "AMORTIZACIÓN_BRUTA", obj[AMORTIZACION_BRUTA]);
			createTagString(xmlsw, "COMISION_CUSTODIA", obj[COMISION_CUSTODIA]);
			createTagString(xmlsw, "AMORTIZACIÓN_NETA_CONTABLE", obj[AMORTIZACION_NETA_CONTABLE]);
			createTagString(xmlsw, "AMORTIZACIÓN_NETA_DISPONIBLE", obj[AMORTIZACION_NETA_DISPONIBLE]);
			createTagString(xmlsw, "STATE", obj[STATE]);
			createTagString(xmlsw, "RETENCION", obj[RETENCION]);
			createTagString(xmlsw, "HOLDER_COUNT", obj[HOLDER_COUNT]);
			
			xmlsw.writeEndElement();	//END ROW TAG
		}
		
		xmlsw.writeEndElement();	//END ROWS TAG
	}

}
