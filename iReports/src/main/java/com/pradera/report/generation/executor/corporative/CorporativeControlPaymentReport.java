package com.pradera.report.generation.executor.corporative;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.corporateevents.to.CorporativeOperationTO;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.corporative.service.CorporativeReportServiceBean;
import com.pradera.report.generation.executor.corporative.to.HolderBalanceAndInterestTO;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CorporativeControlPaymentReport.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01-sep-2015
 */
@ReportProcess(name="CorporativeControlPaymentReport")
public class CorporativeControlPaymentReport extends GenericReport {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The participant. */
	private static int PARTICIPANT    = 0;
	
	/** The balance stock. */
	private static int BALANCE_STOCK  = 1;
	
	/** The interes net. */
	private static int INTERES_NET    = 2;
	
	/** The holder numers. */
	private static int HOLDER_NUMERS  = 3;
	
	/*Constants for the header corporativeOperation
	 * 
	 */
	
	/** The issuer. */
	private static int ISSUER         = 0;
	
	/** The isin. */
	private static int ISIN            = 1;
	
	/** The nominal. */
	private static int NOMINAL         = 2;
	
	/** The tax. */
	private static int TAX             = 3;
	
	/** The interest. */
	private static int INTEREST        = 4;
	
	/** The round. */
	private static int ROUND           = 5;
	
	/** The issuance. */
	private static int ISSUANCE        = 6;
	
	/** The cupon number. */
	private static int CUPON_NUMBER    = 7;
	
	/** The registry date. */
	private static int REGISTRY_DATE   = 8;
	
	/** The cutoff date. */
	private static int CUTOFF_DATE     = 9;
	
	/** The delivery date. */
	private static int DELIVERY_DATE   = 10;
	
	/** The alternative code. */
	private static int ALTERNATIVE_CODE   = 11;
	
	/** The currency. */
	private static int CURRENCY           = 12;
	
	/** The coupon begin date. */
	private static int COUPON_BEGIN_DATE=13;
	
	/** The couping end date. */
	private static int COUPING_END_DATE=14;

 
	/** The list balance. */
	private Map<String,HolderBalanceAndInterestTO> listBalance;
	
	/** The corporative report service bean. */
	@Inject
	private CorporativeReportServiceBean corporativeReportServiceBean;

	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#generateData(com.pradera.model.report.ReportLogger)
	 */
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		listBalance = new HashMap<String,HolderBalanceAndInterestTO>();


		CorporativeOperationTO corporativeOperationTO = this.getReportToFromReportLogger(reportLogger);
		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();

		XMLOutputFactory xof = XMLOutputFactory.newInstance();
		try{

 
			List<Object[]> list = this.corporativeReportServiceBean.getBalanceAndTotalHolder(corporativeOperationTO);
            Object[] tempObjects = this.corporativeReportServiceBean.searchCorporativeOperationByFilter(corporativeOperationTO);

			XMLStreamWriter xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(arrayOutputStream));
			//open document file
			xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");
			//root tag of report
			/**
			 * report
			 **/
			xmlsw.writeStartElement(ReportConstant.REPORT);
			/**
			 * Create to header report
			 * start_hour
			 * report_title
			 * mnemonic_report
			 * clasification_report
			 * generation_date
			 * user_name
			 * mnemonic_entity
			 * **/
			createHeaderReport(xmlsw, reportLogger);
			//Create all the headers of the reports for no result statement
			createTagString(xmlsw, "issuer", tempObjects[ISSUER]);
			createTagString(xmlsw, "isin_code", tempObjects[ISIN]);
			createTagString(xmlsw, "issuance", tempObjects[ISSUANCE]);
			
			createTagDecimal(xmlsw, "nominal", tempObjects[NOMINAL]);
			DecimalFormat format = new DecimalFormat("###.0000");
			DecimalFormat interestFormat = new DecimalFormat("0.0000");
			Double interest =Double.parseDouble(tempObjects[INTEREST].toString());
			Double tax =Double.parseDouble(tempObjects[TAX].toString());
			
			String strInterest = interestFormat.format(interest);
			String strTax = format.format(tax);
			Double decimalTax = new Double(tax);
			decimalTax = decimalTax*100;
			
			//BigDecimal result = decimalTax.multiply(new BigDecimal(100));
			if(tax.equals(0.0)){
				createTagString(xmlsw, "tax", "0%");
			}
			else{
				createTagString(xmlsw, "tax", format.format(decimalTax)+"%");
			}
			//strInterest = strInterest + "%";
			String temp = strInterest+"%";
			createTagString(xmlsw, "interest", temp);			 
			createTagString(xmlsw, "round", tempObjects[ROUND]);
			createTagString(xmlsw, "coupon", tempObjects[CUPON_NUMBER]);
			createTagString(xmlsw, "cutoff_date", tempObjects[CUTOFF_DATE]);
			createTagString(xmlsw, "delivery_date", tempObjects[DELIVERY_DATE]);
			createTagString(xmlsw, "registry_date", tempObjects[REGISTRY_DATE]);
			String state = CorporateProcessStateType.get(corporativeOperationTO.getState()).getValue();
			createTagString(xmlsw, "corporate_state", state);
			createTagString(xmlsw, "alternate_code", tempObjects[ALTERNATIVE_CODE]);
			createTagString(xmlsw, "currency", tempObjects[CURRENCY]);
			createTagString(xmlsw, "cupon_begin_date", tempObjects[COUPON_BEGIN_DATE].toString());
			createTagString(xmlsw, "cupon_expiration_date", tempObjects[COUPING_END_DATE].toString());

			getBalanceByParticipant(list);

			 list = this.corporativeReportServiceBean.getReportBalanceAndTotalHolder(corporativeOperationTO);

			getBalanceByParticipant(list);
		 
		//	if(Validations.validateListIsNotNullAndNotEmpty(listBalance)){
				/** Creating the body **/
				createBodyReport(xmlsw);
			//}
			//END REPORT TAG
			xmlsw.writeEndElement();
			//close document
			xmlsw.writeEndDocument();
			xmlsw.flush();
			xmlsw.close();
		}catch(XMLStreamException e){
			e.printStackTrace();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return arrayOutputStream;

	}
	
	/**
	 * Gets the balance by participant.
	 *
	 * @param objects the objects
	 * @return the balance by participant
	 */
	public void getBalanceByParticipant(List<Object[]> objects){		
		HolderBalanceAndInterestTO balanceTo;
		if(listBalance.isEmpty()){
			for(Object[] temp:objects){
				balanceTo = new HolderBalanceAndInterestTO();
				balanceTo.setBalanceCutoff(new BigDecimal(temp[BALANCE_STOCK].toString()));
				balanceTo.setBalanceNet(new BigDecimal(temp[INTERES_NET].toString()));
				balanceTo.setTotalHolder(Integer.parseInt(temp[HOLDER_NUMERS].toString()));
				
				balanceTo.setBalanceCutoffReport(BigDecimal.ZERO);
				balanceTo.setBalanceNetReport(BigDecimal.ZERO);
				balanceTo.setTotalHolderReport(0);
				listBalance.put(temp[PARTICIPANT].toString(), balanceTo);
			}
		}
		else{			
			Map<String,HolderBalanceAndInterestTO> tempMap= new HashMap<String,HolderBalanceAndInterestTO>();
			tempMap.putAll(listBalance);
		   for(Map.Entry<String, HolderBalanceAndInterestTO> entry : tempMap.entrySet()){
			   for(Object[] temp: objects){
				   String tempParticipant = temp[PARTICIPANT].toString();
				   if(entry.getKey().equals(tempParticipant)){
					   listBalance.get(tempParticipant).setBalanceCutoffReport(new BigDecimal(temp[BALANCE_STOCK].toString()));
					   listBalance.get(tempParticipant).setBalanceNetReport((new BigDecimal(temp[INTERES_NET].toString())));
					   listBalance.get(tempParticipant).setTotalHolderReport(Integer.parseInt((temp[HOLDER_NUMERS].toString())));
				   }
			   }
			 
		   }
			
		}
		
	}
	
	/**
	 * Creates the body report.
	 *
	 * @param xmlsw the xmlsw
	 */
	public void createBodyReport(XMLStreamWriter xmlsw){
		for(Map.Entry<String, HolderBalanceAndInterestTO> entry: listBalance.entrySet()){
			
			try {
				xmlsw.writeStartElement("balance");
				HolderBalanceAndInterestTO balanceTo = entry.getValue();
				createTagString(xmlsw, "participant", entry.getKey());
				createTagString(xmlsw, "balance",balanceTo.getBalanceCutoff());
				createTagString(xmlsw, "interes",balanceTo.getBalanceNet());
				createTagString(xmlsw, "holders", balanceTo.getTotalHolder());
				
				
				createTagString(xmlsw, "balance_report", balanceTo.getBalanceCutoffReport()!=null?balanceTo.getBalanceCutoffReport():0);
				createTagString(xmlsw, "interes_report", balanceTo.getBalanceNetReport()!=null?balanceTo.getBalanceNetReport():0);
				createTagString(xmlsw, "holders_report", balanceTo.getTotalHolderReport()!=null?balanceTo.getTotalHolderReport():0);
				
				
				BigDecimal totalBalance = balanceTo.getBalanceCutoff().add(balanceTo.getBalanceCutoffReport());
				BigDecimal totalInteres= balanceTo.getBalanceNet().add(balanceTo.getBalanceNetReport());
				Integer totalHolder = balanceTo.getTotalHolder()+balanceTo.getTotalHolderReport();
				createTagString(xmlsw, "balance_total", totalBalance);
				createTagString(xmlsw, "interes_total", totalInteres);
				createTagString(xmlsw, "holders_total", totalHolder);

				xmlsw.writeEndElement();

			} catch (XMLStreamException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
 		 
	}
	
	
	
	/**
	 * Gets the report to from report logger.
	 *
	 * @param reportLogger the report logger
	 * @return the report to from report logger
	 */
	public CorporativeOperationTO getReportToFromReportLogger(ReportLogger reportLogger){
		CorporativeOperationTO corporativeOperationTO = new CorporativeOperationTO();
		for(ReportLoggerDetail detail:reportLogger.getReportLoggerDetails()){
			if(ReportConstant.CORP_PROCESS_ID.equals(detail.getFilterName())){
				corporativeOperationTO.setId(detail.getFilterValue()!=null?Long.parseLong(detail.getFilterValue()):null);
			}
			else if(ReportConstant.CUTOFF_DATE.equals(detail.getFilterName())){
				corporativeOperationTO.setCutOffDate(CommonsUtilities.convertStringtoDate(detail.getFilterValue()));
			}else if(ReportConstant.DELIVERY_DATE.equals(detail.getFilterName())){
				corporativeOperationTO.setDeliveryDate(CommonsUtilities.convertStringtoDate(detail.getFilterValue()));
			}
			else if(ReportConstant.CORPORATE_EVENT_TYPE.equals(detail.getFilterName())){
				corporativeOperationTO.setCorporativeEventType(Integer.parseInt(detail.getFilterValue()));
			}
			else if(ReportConstant.CODE_ISIN.equals(detail.getFilterName())){
				corporativeOperationTO.setSourceIsinCode(detail.getFilterValue());
			}
			else if(ReportConstant.DEST_CODE_ISIN.equals(detail.getFilterName()))
			{
				if(detail.getFilterValue()!=null){
					corporativeOperationTO.setTargetIsinCode(detail.getFilterValue());
				}
			}
			else if(ReportConstant.STATE_PARAM.equals(detail.getFilterName())){
				corporativeOperationTO.setState(Integer.parseInt(detail.getFilterValue()));
			}
		}
		
		return corporativeOperationTO;
	}

}
