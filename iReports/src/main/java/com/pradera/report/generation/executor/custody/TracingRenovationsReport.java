package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.service.IssuerQueryServiceBean;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;

@ReportProcess(name="TracingRenovationsReport")
public class TracingRenovationsReport extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@Inject  PraderaLogger log; 
	
	@EJB
	ParticipantServiceBean participantServiceBean;

	@EJB
	private HolderAccountComponentServiceBean accountComponentServiceBean;
	
	public TracingRenovationsReport() {
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void addParametersQueryReport() {
	}
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String,Object> parametersRequired = new HashMap<>();
		
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		String participantDescription = null;
		String participantCode = null;
		String currencyDescription = null;
		String currencyCode = null;
		// DATOS DE ENTRADA DEL SISTEMA
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.PARTICIPANT_PARAM))
				participantCode = r.getFilterValue();
		}
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.CURRENCY_TYPE))
				currencyCode = r.getFilterValue();
		}
		try {
			if(participantCode != null){
				Participant participant = participantServiceBean.find(Participant.class, new Long(participantCode));
				participantDescription = participant.getMnemonic() + " - " + participant.getDescription();
			}else
				participantDescription = "TODOS";
			
			if(currencyCode!= null){
			Integer intCurrencyCode = new Integer(currencyCode);
			ParameterTable param = parameterService.getParameterTableById(intCurrencyCode);
			currencyDescription = param.getDescription();
			}else
				currencyDescription = "TODOS";
			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
//		parametersRequired.put("participantDescription", participantDescription);
//		parametersRequired.put("currencyDescription", currencyDescription);
		
		// TODO Auto-generated method stub
		return parametersRequired;
		
	}

}
