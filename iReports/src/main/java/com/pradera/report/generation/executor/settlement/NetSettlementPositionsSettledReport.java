package com.pradera.report.generation.executor.settlement;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;

@ReportProcess(name="NetSettlementPositionsSettledReport")
public class NetSettlementPositionsSettledReport extends GenericReport{
	
private static final long serialVersionUID = 1L;
	
	@Inject PraderaLogger log;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> settlementCurrency = new HashMap<Integer, String>();
		
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		String scheduleTypeCode = null;
		String currencyTypeCode = null;
		String scheduleTypeDescription = null;
		String currencyTypeDescription = null;
		
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.SCHEDULE_TYPE))
				scheduleTypeCode = r.getFilterValue();
			else if (r.getFilterName().equals(ReportConstant.CURRENCY_TYPE))
				currencyTypeCode = r.getFilterValue();
		}
		
		try{
			if(scheduleTypeCode != null){
				Integer intScheduleTypeCode = new Integer(scheduleTypeCode);
				ParameterTable param = parameterService.getParameterTableById(intScheduleTypeCode);
				scheduleTypeDescription = param.getDescription();
			}
			if(currencyTypeCode != null){
				Integer intCurrencyTypeCode = new Integer(currencyTypeCode);
				ParameterTable param = parameterService.getParameterTableById(intCurrencyTypeCode);
				currencyTypeDescription = param.getDescription();
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}	
		
		try {
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				settlementCurrency.put(param.getParameterTablePk(), param.getDescription());
			}
			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		parametersRequired.put("fecha_actual", dateFormat.format(date));
		parametersRequired.put("schedule_type_description", scheduleTypeDescription);
		parametersRequired.put("p_settlement_currency", settlementCurrency);
		parametersRequired.put("p_description_currency", currencyTypeDescription);
		
		return parametersRequired;
	}

}
