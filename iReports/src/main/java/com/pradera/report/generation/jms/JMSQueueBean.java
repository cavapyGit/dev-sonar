package com.pradera.report.generation.jms;

import javax.jms.MessageProducer;

import com.pradera.model.report.ReportLogger;
import com.pradera.report.ReportContext;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class JMSQueueBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/07/2013
 */
public class JMSQueueBean   implements java.io.Serializable {

	/** serial version id. */
	private static final long serialVersionUID = 1L;
    
	/** MessageProducer reference. */
	protected MessageProducer jmsMessageProducer;
	
	
	/**
	 * Creates the context.
	 *
	 * @param reportLogger the report logger
	 * @return the report context
	 */
	public ReportContext createContext(ReportLogger reportLogger){
		ReportContext reportContext = null;
		if (reportLogger != null) {
			reportContext = new ReportContext();
			reportContext.setProcessLoggerId(reportLogger.getIdReportLoggerPk());
			reportContext.setPriority(reportLogger.getReport().getIndPriority());
			reportContext.setReportImplementationBean(reportLogger.getReport().getReportBeanName());
			reportContext.setTemplateReportUrl(reportLogger.getReport().getTemplateName());
			reportContext.setXmlDataSource(reportLogger.getReport().getDatasourceXml());
			reportContext.setReportQuery(reportLogger.getReport().getQueryReport());
			reportContext.setSubReportQuery(reportLogger.getReport().getSubQueryReport());
			reportContext.setReportType(reportLogger.getReport().getReportType());
		}
		return reportContext;
	}
    
	/**
	 * Gets the jms message producer.
	 *
	 * @return the jmsMessageProducer
	 */
	public MessageProducer getJmsMessageProducer() {
		return jmsMessageProducer;
	}

	/**
	 * Sets the jms message producer.
	 *
	 * @param jmsMessageProducer the jmsMessageProducer to set
	 */
	public void setJmsMessageProducer(MessageProducer jmsMessageProducer) {
		this.jmsMessageProducer = jmsMessageProducer;
	}

	

}
