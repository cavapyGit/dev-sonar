package com.pradera.report.generation.executor.account.to;

import java.io.Serializable;
import java.util.Date;

public class PositionsParticipantAccountTO  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long participant;
	private String issuer;
	private String securityClass;
	private Date courtDate;
	public Long getParticipant() {
		return participant;
	}
	public void setParticipant(Long participant) {
		this.participant = participant;
	}
	public String getIssuer() {
		return issuer;
	}
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	public String getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(String securityClass) {
		this.securityClass = securityClass;
	}
	public Date getCourtDate() {
		return courtDate;
	}
	public void setCourtDate(Date courtDate) {
		this.courtDate = courtDate;
	}
	
	

	

}
