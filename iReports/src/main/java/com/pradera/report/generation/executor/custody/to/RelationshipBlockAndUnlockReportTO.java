package com.pradera.report.generation.executor.custody.to;

import java.io.Serializable;


public class RelationshipBlockAndUnlockReportTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// filtros del reporte
	private String idParticipant;
	private String idHolder;
	private String accountNumber;
	private String idIssuer;
	private String idSecurityCode;
	private String securityClass;
	private String lockType;
	private String levelAffectation;
	private String stateRequest;
	private String initialDate;
	private String finalDate;
	public String getIdParticipant() {
		return idParticipant;
	}
	public void setIdParticipant(String idParticipant) {
		this.idParticipant = idParticipant;
	}
	public String getIdHolder() {
		return idHolder;
	}
	public void setIdHolder(String idHolder) {
		this.idHolder = idHolder;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getIdIssuer() {
		return idIssuer;
	}
	public void setIdIssuer(String idIssuer) {
		this.idIssuer = idIssuer;
	}
	public String getIdSecurityCode() {
		return idSecurityCode;
	}
	public void setIdSecurityCode(String idSecurityCode) {
		this.idSecurityCode = idSecurityCode;
	}
	public String getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(String securityClass) {
		this.securityClass = securityClass;
	}
	public String getLockType() {
		return lockType;
	}
	public void setLockType(String lockType) {
		this.lockType = lockType;
	}
	public String getLevelAffectation() {
		return levelAffectation;
	}
	public void setLevelAffectation(String levelAffectation) {
		this.levelAffectation = levelAffectation;
	}
	public String getStateRequest() {
		return stateRequest;
	}
	public void setStateRequest(String stateRequest) {
		this.stateRequest = stateRequest;
	}
	public String getInitialDate() {
		return initialDate;
	}
	public void setInitialDate(String initialDate) {
		this.initialDate = initialDate;
	}
	public String getFinalDate() {
		return finalDate;
	}
	public void setFinalDate(String finalDate) {
		this.finalDate = finalDate;
	}
	
}
