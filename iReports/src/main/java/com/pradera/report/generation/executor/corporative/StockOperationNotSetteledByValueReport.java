package com.pradera.report.generation.executor.corporative;

import java.io.ByteArrayOutputStream;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.corporateevents.to.CorporativeOperationTO;
import com.pradera.core.component.helperui.service.SecuritiesQueryServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.corporative.service.CorporativeReportServiceBean;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

// TODO: Auto-generated Javadoc
/**
 * The Class StockOperationNotSetteledByValueReport.
 */
@ReportProcess(name="StockOperationNotSetteledByValueReport")
public class StockOperationNotSetteledByValueReport  extends GenericReport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private static int SECURITY_CODE 			= 0;
	private static int MODALITY 				= 1;
	private static int MECHANISM 				= 2;
	private static int OPER_NUMBER 				= 3;
	private static int OPERATION_DATE 			= 4;
	private static int CASH_SETTLEMENT_DATE 	= 5;
	private static int TERM_SETTLEMENT_DATE 	= 6;
	private static int STOCK_QUANTITY 			= 7;
	private static int MNEMONIC_BUYER 			= 8;
	private static int MNEMONIC_SELLER 			= 9;
	private static int STATE_DESC 				= 10;
	
	
	@EJB
	CorporativeReportServiceBean corporativeReportServiceBean;
	
	@EJB
	SecuritiesQueryServiceBean securitiesQueryServiceBean;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#generateData(com.pradera.model.report.ReportLogger)
	 */
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		boolean existData = false;
		CorporativeOperationTO corporativeOpTO = new CorporativeOperationTO();
		
		//Read parameters from DB, all parameters are string type
		for(ReportLoggerDetail loggerDetail : reportLogger.getReportLoggerDetails()) {				
			if(loggerDetail.getFilterName().equals(ReportConstant.CUTOFF_DATE) &&
					Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	){
				corporativeOpTO.setCutOffDate(CommonsUtilities.convertStringtoDate(loggerDetail.getFilterValue()));
				}
			
			if(loggerDetail.getFilterName().equals(ReportConstant.REGISTER_DATE) &&
					Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	) {
				corporativeOpTO.setDeliveryDate(CommonsUtilities.convertStringtoDate(loggerDetail.getFilterValue()));
				}
				
			if(loggerDetail.getFilterName().equals(ReportConstant.ISIN_CODE_PARAM) &&
					Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	){
				corporativeOpTO.setSourceIsinCode(loggerDetail.getFilterValue());
				}
			
		}
				
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		try {
			//GETTING ALL DATA FROM QUERY
			List<Object[]> listObjects = corporativeReportServiceBean.getStockOperationNotSetteledByValue(corporativeOpTO);
			
			Security sec = securitiesQueryServiceBean.getSecurityHelpServiceBean(corporativeOpTO.getSourceIsinCode());

			if(Validations.validateIsNotNullAndNotEmpty(listObjects) && listObjects.size()>0){
				existData = true;
			}
			
			XMLOutputFactory xof = XMLOutputFactory.newInstance();
			XMLStreamWriter xmlsw;
			
			
				xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(baos));
				//open document file
				xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");
				
				//REPORT TAG
				xmlsw.writeStartElement(ReportConstant.REPORT);
				
				//BODY
				if(existData){
					//HEADER
					createHeaderReport(xmlsw,reportLogger);	
					
					xmlsw.writeStartElement("headerReportFilter"); //START HEADER REPORT ELEMENT	
						createTagString(xmlsw, "securityCode", sec.getIdSecurityCodePk());
						createTagString(xmlsw, "securityDesc", sec.getDescription());
						createTagString(xmlsw, "cutOff", corporativeOpTO.getCutOffDate().toString());
						createTagString(xmlsw, "registerDate", corporativeOpTO.getRegistryDate().toString());
					xmlsw.writeEndElement(); //END HEADER REPORT ELEMENT
					
					createBodyReport(xmlsw, listObjects);
				}
			
				//END REPORT TAG
				xmlsw.writeEndElement();
				//close document
				xmlsw.writeEndDocument();
				xmlsw.flush();
		        xmlsw.close();
					
		} catch (XMLStreamException | ServiceException e) {
			log.error(e.getMessage());
			e.printStackTrace();
			throw new RuntimeException();
		} 
		return baos;
	}

	
	public void createBodyReport(XMLStreamWriter xmlsw,List<Object[]> objConsolidateParentAccountList)  throws XMLStreamException{
		xmlsw.writeStartElement("rows"); //START ROWS TAG	
		
		//BY ROW
		for(Object[] objConsolidate : objConsolidateParentAccountList){
			xmlsw.writeStartElement("row"); //START ROW TAG
			
			createTagString(xmlsw, "VALOR", objConsolidate[SECURITY_CODE]);
			createTagString(xmlsw, "MODALIDAD", objConsolidate[MODALITY]);
			createTagString(xmlsw, "MECANISM", objConsolidate[MECHANISM]);
			createTagString(xmlsw, "N_OPERATION", objConsolidate[OPER_NUMBER]);
			createTagString(xmlsw, "FECHA_OPERATION", objConsolidate[OPERATION_DATE]);
			createTagString(xmlsw, "FECHA_LIQUIDACION_CONTADO", objConsolidate[CASH_SETTLEMENT_DATE]);
			createTagString(xmlsw, "FECHA_LIQUIDACION_PLAZO", objConsolidate[TERM_SETTLEMENT_DATE]);
			createTagString(xmlsw, "CANTIDAD", objConsolidate[STOCK_QUANTITY]);
			createTagString(xmlsw, "COMPRADOR", objConsolidate[MNEMONIC_BUYER]);
			createTagString(xmlsw, "VENDEDOR", objConsolidate[MNEMONIC_SELLER]);
			createTagString(xmlsw, "ESTADO", objConsolidate[STATE_DESC]);
			
			xmlsw.writeEndElement();	//END ROW TAG
		}
		
		xmlsw.writeEndElement();	//END ROWS TAG
	}
}
