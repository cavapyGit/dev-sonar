package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;

import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.util.view.UtilReportConstants;
//reporte de anotaciones en cuenta voluntaria a valor nominal
@ReportProcess(name = "RegisterSecuritiesEntrySystemIssuerReport")
public class RegisterSecuritiesEntrySystemIssuerReport extends GenericReport{
	
	private static final long serialVersionUID = 1L;
	private final String MAP_REASON_DESMATERIALIZATION="map_reason_desmaterialization";
	private final String MAP_MNEMONIC_SC="p_mnemonic_sc";
	@EJB
	private ParameterServiceBean parameterService;
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		return null;
	}
	@Override
	public Map<String, Object> getCustomJasperParameters(){
		Map<String, Object> map=new HashMap<String, Object>();
		Map<Integer,String> reasonsDematerialisation = new HashMap<Integer, String>();
		Map<Integer,String> typesCurrency = new HashMap<Integer, String>();
		Map<Integer,String> p_mnemonic_sc = new HashMap<Integer, String>();
		ParameterTableTO  parameterFilter = new ParameterTableTO();
	    parameterFilter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
	    parameterFilter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			parameterFilter.setMasterTableFk(MasterTableType.DEMATERIALIZATION_MOTIVE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(parameterFilter)) 
				reasonsDematerialisation.put(param.getParameterTablePk(), param.getDescription());
			map.put(MAP_REASON_DESMATERIALIZATION, reasonsDematerialisation);
			
			parameterFilter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(parameterFilter)) 
				p_mnemonic_sc.put(param.getParameterTablePk(), param.getText1());
			map.put(MAP_MNEMONIC_SC, p_mnemonic_sc);
		map.put("logo_path", UtilReportConstants.readLogoReport());	
		return map;
	}
}
