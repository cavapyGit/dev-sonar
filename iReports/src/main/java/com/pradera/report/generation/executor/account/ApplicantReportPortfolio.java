package com.pradera.report.generation.executor.account;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.management.RuntimeErrorException;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.account.service.ApplicantReportServiceBean;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.executor.custody.to.ClientPortafolioTO;
/**
* 
* <ul><li>Copyright EDV 2020.</li></ul> 
* @author RCHIARA.
*
*/
@ReportProcess(name = "ApplicantReportPortfolio")
public class ApplicantReportPortfolio extends GenericReport{
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ParameterServiceBean parameterService;	
	@EJB
	private ApplicantReportServiceBean applicantReportServiceBean;	
	@EJB
	private CustodyReportServiceBean custodyReportService;
	@Inject
	private PraderaLogger log;	
	private ClientPortafolioTO clientPortafolioTO;
	
	public ApplicantReportPortfolio(){}
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		String cuiDescription = null;
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		Map<String,Object> parametersRequired = new HashMap<>();
		clientPortafolioTO = new ClientPortafolioTO();
				
		for (int i = 0; i < listaLogger.size(); i++) {			
			if(listaLogger.get(i).getFilterName().equals("cui_holder")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setCui(new Long(listaLogger.get(i).getFilterValue()).longValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("cui_description")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					cuiDescription = listaLogger.get(i).getFilterValue().toString();
				}
			}
			if(listaLogger.get(i).getFilterName().equals("portafolio_type")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setPortafolioType(Integer.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("date")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){					
					clientPortafolioTO.setDateMarketFact(CommonsUtilities.convertStringtoDate(listaLogger.get(i).getFilterValue()));
					clientPortafolioTO.setDateMarketFactString(listaLogger.get(i).getFilterValue());
				}
			}			
		}
		
		clientPortafolioTO.setIsParticipantInvestor(false);
		if(clientPortafolioTO.getInstitutionType()!=null && Integer.valueOf(clientPortafolioTO.getInstitutionType()).equals(InstitutionType.PARTICIPANT_INVESTOR.getCode())){
			clientPortafolioTO.setIsParticipantInvestor(true);
		}

		try{
			custodyReportService.getExchangeRate(clientPortafolioTO.getDateMarketFactString());
		}catch (ServiceException e){
			if(e.getErrorService().equals(ErrorServiceType.NOT_EXCHANGE_RATE)){
				throw new RuntimeErrorException(null, ReportConstant.ERROR_EXCHANGE_RATE + " " + clientPortafolioTO.getDateMarketFactString());
			}
			log.error(e.getMessage());
		}
		
		String strQuery = null;
		Long idStkCalcProcess= null;		
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> currencyTypeShort = new HashMap<Integer, String>();
		Map<Integer,String> currencyType = new HashMap<Integer, String>();
		Map<Integer,String> securityClassShort = new HashMap<Integer, String>();
		Map<Integer,String> securityClass = new HashMap<Integer, String>();
		
		try {
			
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				currencyTypeShort.put(param.getParameterTablePk(), param.getText1());
				currencyType.put(param.getParameterTablePk(), param.getParameterName());
			}
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				securityClassShort.put(param.getParameterTablePk(), param.getText1());
				securityClass.put(param.getParameterTablePk(), param.getParameterName());
			}
		
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		String title="CARTERA DE CLIENTES REPRESENTADA EN ANOTACION EN CUENTA";
		String CUI = clientPortafolioTO.getCui().toString() + " - " + cuiDescription;		
		
		if(clientPortafolioTO.getPortafolioType().equals(IssuanceType.DEMATERIALIZED.getCode())){
			strQuery = applicantReportServiceBean.getQueryClientPortafolio(clientPortafolioTO, idStkCalcProcess);
		}else{
//			TODO: DEFINIR REPORTE DE CARTERA FISICA
			strQuery = applicantReportServiceBean.getQueryClientPortafolio(clientPortafolioTO);
		}
		
		parametersRequired.put("report_title_customize", title);
		parametersRequired.put("str_query", strQuery);
		parametersRequired.put("p_securityClass", securityClassShort);
		parametersRequired.put("p_securityClassDescription", securityClass);
		parametersRequired.put("p_participant_desc", clientPortafolioTO.getParticipantDesc());
		parametersRequired.put("p_currency", currencyTypeShort);
		parametersRequired.put("p_description", currencyType);
		parametersRequired.put("p_currency_desc", currencyType.get(clientPortafolioTO.getIdCurrency()));		
		parametersRequired.put("p_account_holder", "Todos");
		parametersRequired.put("p_CUI", CUI);
		parametersRequired.put("p_issuerDescription", "Todos");
		parametersRequired.put("p_securityClassDesc", "Todos");
		parametersRequired.put("security_code", null);
		
		return parametersRequired;		
	}
}
