package com.pradera.report.generation.executor.account;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "RequestOperationsCUIHolderReport")
public class RequestOperationsCUIHolderReport extends GenericReport {
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@Inject
	PraderaLogger log;

	
	public RequestOperationsCUIHolderReport() {
	}
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParametersQueryReport() {

	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		
		Map<Integer,String> typeDocument = new HashMap<Integer, String>();
		Map<Integer,String> requestType = new HashMap<Integer, String>();
		Map<Integer,String> motive = new HashMap<Integer, String>();
		Map<Integer,String> requesterType = new HashMap<Integer, String>();
		Map<Integer,String> state = new HashMap<Integer, String>();
		
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		String participantDescription = null;
		String requesterDescription = null;
		String stateDescription = null;
		
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.PARTICIPANT_PARAM)){
				if(r.getFilterValue()!=null)
					participantDescription = r.getFilterDescription();
				else
					participantDescription = "TODOS";
			}
			
			if (r.getFilterName().equals(ReportConstant.REQUESTER_PARAM)){
				if(r.getFilterValue()!=null)
					requesterDescription = r.getFilterDescription();
				else
					requesterDescription = "TODOS";
			}
			
			if (r.getFilterName().equals(ReportConstant.REQUEST_STATE_PARAM)){
				if(r.getFilterValue()!=null)
					stateDescription = r.getFilterDescription();
				else
					stateDescription = "TODOS";
			}
		}
		
		try {
		
			filter.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				typeDocument.put(param.getParameterTablePk(), param.getIndicator1());
			}
			
			filter.setMasterTableFk(MasterTableType.HOLDER_REQUEST_TYPE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				requestType.put(param.getParameterTablePk(), param.getDescription());
			}
			
			filter.setMasterTableFk(MasterTableType.HOLDER_REQUEST_REJECT_MOTIVE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				motive.put(param.getParameterTablePk(), param.getDescription());
			}
			
			filter.setMasterTableFk(MasterTableType.HOLDER_REQUEST_ANNULAR_MOTIVE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				motive.put(param.getParameterTablePk(), param.getDescription());
			}
			
			filter.setMasterTableFk(MasterTableType.HOLDER_BLOCK_MOTIVE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				motive.put(param.getParameterTablePk(), param.getDescription());
			}
			
			filter.setMasterTableFk(MasterTableType.HOLDER_UNBLOCK_MOTIVE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				motive.put(param.getParameterTablePk(), param.getDescription());
			}
			
			filter.setMasterTableFk(MasterTableType.PARAMETER_REQUESTER_BLOCK_UNBLOC_HOLDERACCOUNT.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				requesterType.put(param.getParameterTablePk(), param.getDescription());
			}
			
			filter.setMasterTableFk(MasterTableType.STATE_CREATION_HOLDER_REQUEST.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				state.put(param.getParameterTablePk(), param.getDescription());
			}
		
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		parametersRequired.put("p_typeDocument", typeDocument);
		parametersRequired.put("p_requestType", requestType);
		parametersRequired.put("p_motive", motive);
		parametersRequired.put("p_requesterType", requesterType);
		parametersRequired.put("p_state", state);
		parametersRequired.put("participant_description", participantDescription);
		parametersRequired.put("requester_description", requesterDescription);
		parametersRequired.put("state_description", stateDescription);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		
		return parametersRequired;
	}
	
	

}
