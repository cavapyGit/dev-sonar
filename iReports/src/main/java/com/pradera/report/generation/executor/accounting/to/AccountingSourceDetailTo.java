package com.pradera.report.generation.executor.accounting.to;

import java.io.Serializable;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingSourceDetailTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class AccountingSourceDetailTo implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1951313903320043669L;
	
	/** The id accounting source detail pk. */
	private Long 	idAccountingSourceDetailPk;
	
	/** The id accounting source logger fk. */
	private Long 	idAccountingSourceLoggerFk;
	
	/** The parameter name. */
	private String 	parameterName;
	
	/** The parameter value. */
	private String 	parameterValue;
	
	/** The parameter type. */
	private Integer parameterType;
	
	/** The position. */
	private Integer position;
	
	/** The parameter description. */
	private String 	parameterDescription;
	
	
	
	
	/** VALUES FOR SECURITIES*. */
	private String movementQuantity;
	
	/** The participant. */
	private Long participant;
	
	/** The holder account. */
	private String holderAccount;
	
	/** The isin code. */
	private String isinCode;
	
	/** The movement type. */
	private String movementType;
	
	/** The movement date. */
	private Date   movementDate;
	
	
	
	/**
	 * Gets the id accounting source detail pk.
	 *
	 * @return the idAccountingSourceDetailPk
	 */
	public Long getIdAccountingSourceDetailPk() {
		return idAccountingSourceDetailPk;
	}
	
	/**
	 * Sets the id accounting source detail pk.
	 *
	 * @param idAccountingSourceDetailPk the idAccountingSourceDetailPk to set
	 */
	public void setIdAccountingSourceDetailPk(Long idAccountingSourceDetailPk) {
		this.idAccountingSourceDetailPk = idAccountingSourceDetailPk;
	}
	
	/**
	 * Gets the id accounting source logger fk.
	 *
	 * @return the idAccountingSourceLoggerFk
	 */
	public Long getIdAccountingSourceLoggerFk() {
		return idAccountingSourceLoggerFk;
	}
	
	/**
	 * Sets the id accounting source logger fk.
	 *
	 * @param idAccountingSourceLoggerFk the idAccountingSourceLoggerFk to set
	 */
	public void setIdAccountingSourceLoggerFk(Long idAccountingSourceLoggerFk) {
		this.idAccountingSourceLoggerFk = idAccountingSourceLoggerFk;
	}
	
	/**
	 * Gets the parameter name.
	 *
	 * @return the parameterName
	 */
	public String getParameterName() {
		return parameterName;
	}
	
	/**
	 * Sets the parameter name.
	 *
	 * @param parameterName the parameterName to set
	 */
	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}
	
	/**
	 * Gets the parameter value.
	 *
	 * @return the parameterValue
	 */
	public String getParameterValue() {
		return parameterValue;
	}
	
	/**
	 * Sets the parameter value.
	 *
	 * @param parameterValue the parameterValue to set
	 */
	public void setParameterValue(String parameterValue) {
		this.parameterValue = parameterValue;
	}
	
	/**
	 * Gets the parameter type.
	 *
	 * @return the parameterType
	 */
	public Integer getParameterType() {
		return parameterType;
	}
	
	/**
	 * Sets the parameter type.
	 *
	 * @param parameterType the parameterType to set
	 */
	public void setParameterType(Integer parameterType) {
		this.parameterType = parameterType;
	}
	
	/**
	 * Gets the position.
	 *
	 * @return the position
	 */
	public Integer getPosition() {
		return position;
	}
	
	/**
	 * Sets the position.
	 *
	 * @param position the position to set
	 */
	public void setPosition(Integer position) {
		this.position = position;
	}
	
	/**
	 * Gets the parameter description.
	 *
	 * @return the parameterDescription
	 */
	public String getParameterDescription() {
		return parameterDescription;
	}
	
	/**
	 * Sets the parameter description.
	 *
	 * @param parameterDescription the parameterDescription to set
	 */
	public void setParameterDescription(String parameterDescription) {
		this.parameterDescription = parameterDescription;
	}
	
	/**
	 * Gets the movement quantity.
	 *
	 * @return the movementQuantity
	 */
	public String getMovementQuantity() {
		return movementQuantity;
	}
	
	/**
	 * Sets the movement quantity.
	 *
	 * @param movementQuantity the movementQuantity to set
	 */
	public void setMovementQuantity(String movementQuantity) {
		this.movementQuantity = movementQuantity;
	}
	
	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Long getParticipant() {
		return participant;
	}
	
	/**
	 * Sets the participant.
	 *
	 * @param participant the participant to set
	 */
	public void setParticipant(Long participant) {
		this.participant = participant;
	}
	
	/**
	 * Gets the holder account.
	 *
	 * @return the holderAccount
	 */
	public String getHolderAccount() {
		return holderAccount;
	}
	
	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the holderAccount to set
	 */
	public void setHolderAccount(String holderAccount) {
		this.holderAccount = holderAccount;
	}
	
	/**
	 * Gets the isin code.
	 *
	 * @return the isinCode
	 */
	public String getIsinCode() {
		return isinCode;
	}
	
	/**
	 * Sets the isin code.
	 *
	 * @param isinCode the isinCode to set
	 */
	public void setIsinCode(String isinCode) {
		this.isinCode = isinCode;
	}
	
	/**
	 * Gets the movement type.
	 *
	 * @return the movementType
	 */
	public String getMovementType() {
		return movementType;
	}
	
	/**
	 * Sets the movement type.
	 *
	 * @param movementType the movementType to set
	 */
	public void setMovementType(String movementType) {
		this.movementType = movementType;
	}
	
	/**
	 * Gets the movement date.
	 *
	 * @return the movementDate
	 */
	public Date getMovementDate() {
		return movementDate;
	}
	
	/**
	 * Sets the movement date.
	 *
	 * @param movementDate the movementDate to set
	 */
	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}
	
	
	

}
