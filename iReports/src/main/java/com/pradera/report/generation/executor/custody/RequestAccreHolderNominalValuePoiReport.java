package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.executor.custody.to.RequestAccreditationCertificationTO;
import com.pradera.report.generation.poi.ReportPoiGeneratorServiceBean;
import com.pradera.report.generation.service.ReportUtilServiceBean;

@ReportProcess(name = "RequestAccreHolderNominalValuePoiReport")
public class RequestAccreHolderNominalValuePoiReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private CustodyReportServiceBean custodyReportServiceBean;
	@EJB
	private ReportUtilServiceBean reportUtilServiceBean;

	/** The Report poi generator service bean. */
	@Inject
	private ReportPoiGeneratorServiceBean reportPoiGeneratorServiceBean;
	@Inject
	private PraderaLogger log;

	private RequestAccreditationCertificationTO requestAccreditationCertificationTO;
	
	public RequestAccreHolderNominalValuePoiReport() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {


		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] arrayByteExcel = new byte[16384];
		List<Object[]> queryMain=new ArrayList<Object[]>();
		
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		Map<String,Object> parametersRequired = new HashMap<>();
		requestAccreditationCertificationTO = new RequestAccreditationCertificationTO();

		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("state")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					requestAccreditationCertificationTO.setRequestState(Integer.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("date_end")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
//					registrationAssignmentHoldersTO.setDateInitial(CommonsUtilities.convertStringtoDate(listaLogger.get(i).getFilterValue()));
					requestAccreditationCertificationTO.setStrDateEnd(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("date_initial")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
//					registrationAssignmentHoldersTO.setDateInitial(CommonsUtilities.convertStringtoDate(listaLogger.get(i).getFilterValue()));
					requestAccreditationCertificationTO.setStrDateInitial(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("motive")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					requestAccreditationCertificationTO.setMotive(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("request_number")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					requestAccreditationCertificationTO.setRequestNumber(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("cui_code")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					requestAccreditationCertificationTO.setIdHolderPk(Long.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("holder_account")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					requestAccreditationCertificationTO.setIdHolderAccountPk(Long.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("participant")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					requestAccreditationCertificationTO.setParticipantPk(Long.valueOf(listaLogger.get(i).getFilterValue()));
					requestAccreditationCertificationTO.setParticipantDesc(listaLogger.get(i).getFilterDescription());
				}
			}
			
		}
		
			String strQuery = null;
			strQuery = custodyReportServiceBean.getQueryRequestAccreditation(requestAccreditationCertificationTO);
			queryMain=custodyReportServiceBean.getQueryListByClass(strQuery);
			
			parametersRequired.put("str_query", queryMain);
			
			/**GENERATE FILE EXCEL ***/
			arrayByteExcel=reportPoiGeneratorServiceBean.writeFileExcelRequestAccreditation(parametersRequired,reportLogger);
			
			/**SETTING ARRAY BYTE TO GENERIC REPORT FOR PERSIST**/
			List<byte[]> bytes = new ArrayList<>();
			bytes.add(arrayByteExcel);
			setListByte(bytes);

			return baos;
	}
}