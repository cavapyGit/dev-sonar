package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.helperui.service.IssuerQueryServiceBean;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "SecuritiesTransactionReport")
public class SecuritiesTransactionReport extends GenericReport{
private static final long serialVersionUID = 1L;

@Inject  PraderaLogger log;

@EJB
private ParameterServiceBean parameterService;

@EJB
private IssuerQueryServiceBean issuerQueryServiceBean;

@EJB
private HolderAccountComponentServiceBean accountComponentServiceBean;

@EJB
private ParticipantServiceBean participantServiceBean;

public SecuritiesTransactionReport() {
}
	
@Override
public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public void addParametersQueryReport() { }

@Override
public Map<String, Object> getCustomJasperParameters() {
	Map<String,Object> parametersRequired = new HashMap<>();
	
	List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
	String issuerCode = null;
	String issuerMnemonic = null;
	String holderAccountCode = null;
	String holderAccountNumber = null;
	String participantCode = null;
	String participantMnemonic = null;
	String currencyCode = null;
	String currencyMnemonic = null;
	String securityClassCode = null;
	String securityClassDescription = null;

	// DATOS DE ENTRADA DEL SISTEMA
	for (ReportLoggerDetail r : loggerDetails) {
		if (r.getFilterName().equals(ReportConstant.PARTICIPANT_PARAM))
			participantCode = r.getFilterValue();
		if (r.getFilterName().equals(ReportConstant.ISSUER_PARAM))
			issuerCode = r.getFilterValue();
		if (r.getFilterName().equals(ReportConstant.HOLDER_ACCOUNT))
			holderAccountCode = r.getFilterValue();
		if (r.getFilterName().equals(ReportConstant.CURRENCY_TYPE))
			currencyCode = r.getFilterValue();
		if (r.getFilterName().equals(ReportConstant.SECURITY_CLASS_PARAM))
			securityClassCode = r.getFilterValue();
	}
	if(issuerCode != null)
		issuerMnemonic = issuerQueryServiceBean.find(issuerCode,Issuer.class).getMnemonic();
	else
		issuerMnemonic = "TODOS";
	
	if(holderAccountCode != null)
		holderAccountNumber = accountComponentServiceBean.find(new Long(holderAccountCode),HolderAccount.class).getAccountNumber().toString();
	else
		holderAccountNumber = "TODOS";
	
	if(participantCode != null)
		participantMnemonic = participantServiceBean.find(new Long(participantCode),Participant.class).getMnemonic();
	else
		participantMnemonic = "TODOS";
	
	if(currencyCode != null)
		currencyMnemonic = parameterService.find(new Integer(currencyCode),ParameterTable.class).getParameterName();
	else
		currencyMnemonic = "TODOS";
	
	if(securityClassCode != null)
		securityClassDescription = parameterService.find(new Integer(securityClassCode),ParameterTable.class).getParameterName();
	else
		securityClassDescription = "TODOS";
	
	parametersRequired.put("participant_mnemonic", participantMnemonic);
	parametersRequired.put("issuer_mnemonic", issuerMnemonic);
	parametersRequired.put("holder_account_number", holderAccountNumber);
	parametersRequired.put("currency_description", currencyMnemonic);
	parametersRequired.put("security_class_description", securityClassDescription);
	parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
	// TODO Auto-generated method stub
	return parametersRequired;
	}

}
