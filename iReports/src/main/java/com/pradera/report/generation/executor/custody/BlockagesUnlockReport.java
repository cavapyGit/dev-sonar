package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "BlockagesUnlockReport")
public class BlockagesUnlockReport extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;
	@Inject
	PraderaLogger log;
	
	@EJB
	ParticipantServiceBean participantServiceBean;

	public BlockagesUnlockReport() {
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParametersQueryReport() {}

	@Override
	public Map<String, Object> getCustomJasperParameters() {

		Map<String, Object> parametersRequired = new HashMap<>();
		ParameterTableTO filter = new ParameterTableTO();

		Map<Integer, String> blockType = new HashMap<Integer, String>();
		Map<Integer, String> valueClass = new HashMap<Integer, String>();
		Map<Integer, String> valueClassDescription = new HashMap<Integer, String>();
		Map<Integer, String> valueRequestStateDescription = new HashMap<Integer, String>();

		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		
		String participantDescription = null;
		String holderAccountNumber = null;
		String requestStateDescription = null;
		String blockTypeDescription = null;
		String securityClassMnemonic = null;
		String cuiDescription = null;
		
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.PARTICIPANT_PARAM))
				if(r.getFilterValue() != null){
					participantDescription = r.getFilterDescription();
				}else{
					participantDescription = "TODOS";
				}
			if (r.getFilterName().equals(ReportConstant.ACCOUNT_HOLDER))
				if(r.getFilterValue() != null){
					holderAccountNumber = r.getFilterDescription();
				}else{
					holderAccountNumber = "TODAS";
				}
			if (r.getFilterName().equals(ReportConstant.REQUEST_STATE_PARAM))
				if(r.getFilterValue() != null){
					requestStateDescription = r.getFilterDescription();
				}else{
					requestStateDescription = "TODOS";
				}
			if (r.getFilterName().equals(ReportConstant.BLOCK_TYPE_PARAM))
				if(r.getFilterValue() != null){
					blockTypeDescription = r.getFilterDescription();
				}else{
					blockTypeDescription = "TODOS";
				}
			if (r.getFilterName().equals(ReportConstant.VALUE_CLASS_PARAM))
				if(r.getFilterValue() != null){
					securityClassMnemonic = r.getFilterDescription();
				}else{
					securityClassMnemonic = "TODOS";
				}
			if (r.getFilterName().equals(ReportConstant.CUI_HOLDER_PARAM))
				if(r.getFilterValue() != null){
					cuiDescription = r.getFilterDescription();
				}else{
					cuiDescription = "TODOS";
				}
		}
		
		try {
			filter.setMasterTableFk(MasterTableType.TYPE_AFFECTATION.getCode());
			for (ParameterTable param : parameterService
					.getListParameterTableServiceBean(filter)) {
				blockType.put(param.getParameterTablePk(),param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for (ParameterTable param : parameterService
					.getListParameterTableServiceBean(filter)) {
				valueClass.put(param.getParameterTablePk(), param.getText1());
			}
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for (ParameterTable param : parameterService
					.getListParameterTableServiceBean(filter)) {
				valueClassDescription.put(param.getParameterTablePk(), param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.STATE_AFFECTATION_REQUEST.getCode());
			for (ParameterTable param : parameterService
					.getListParameterTableServiceBean(filter)) {
				valueRequestStateDescription.put(param.getParameterTablePk(), param.getDescription());
			}

		} catch (Exception ex) {
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		parametersRequired.put("p_blockType", blockType);
		parametersRequired.put("p_valueClass", valueClass);
		parametersRequired.put("p_valueClassDescription", valueClassDescription);
		parametersRequired.put("p_valueRequestStateDescription", valueRequestStateDescription);
		parametersRequired.put("participant_description", participantDescription);
		parametersRequired.put("holder_account_number", holderAccountNumber);
		parametersRequired.put("request_state_description", requestStateDescription);
		parametersRequired.put("block_type_description", blockTypeDescription);
		parametersRequired.put("security_class_mnemonic", securityClassMnemonic);
		parametersRequired.put("cui_description", cuiDescription);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());

		return parametersRequired;
	}

}
