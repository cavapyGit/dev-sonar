package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.management.RuntimeErrorException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.executor.custody.to.ClientPortafolioTO;
import com.pradera.report.generation.executor.custody.to.XmlOperationsSirtex;
import com.pradera.report.generation.executor.custody.to.XmlReportSirtexByParticipant;
import com.pradera.report.generation.executor.custody.to.XmlSecOperations;
import com.pradera.report.generation.executor.securities.service.IssuerListReportServiceBean;
import com.pradera.report.generation.facade.ReportGenerationLocalServiceFacade;
import com.pradera.report.generation.service.ReportUtilServiceBean;

@ReportProcess(name = "PortfolioSirtexOperationsByParticipantReport")
public class PortfolioSirtexOperationsByParticipantReport extends GenericReport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7715501446648706686L;

	@EJB
	private ParameterServiceBean parameterService;
	@EJB
	private CustodyReportServiceBean custodyReportService;
	@EJB
	private ReportUtilServiceBean reportUtilServiceBean;
	@EJB
	private HolderAccountComponentServiceBean accountComponentServiceBean;
	
	@EJB
	private IssuerListReportServiceBean issuerServiceBean;
	@Inject
	private PraderaLogger log;
	
	/** The report generation facade. */
	@EJB
	private ReportGenerationLocalServiceFacade reportGenerationFacade;
	private ClientPortafolioTO clientPortafolioTO;
	
	//private static final Long ID_REPORT_SIRTEX_BY_PARTICIPANT_EXCEL = 303L;
	
	public PortfolioSirtexOperationsByParticipantReport() {
		// TODO Auto-generated constructor stub/*
	}
	

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		String cuiDescription = null;
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		Map<String,String> parametersReportExcel = new HashMap<>();
		clientPortafolioTO = new ClientPortafolioTO();

		for (int i = 0; i < listaLogger.size(); i++) {
			
			/*Tipo de reporte*/
			if(listaLogger.get(i).getFilterName().equals("reportFormats")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setReportFormat(Integer.parseInt(listaLogger.get(i).getFilterValue()));
					clientPortafolioTO.setReportFormatDesc(listaLogger.get(i).getFilterDescription());
					parametersReportExcel.put("reportFormats", listaLogger.get(i).getFilterValue());
				}
			}
			
			if(listaLogger.get(i).getFilterName().equals("participant")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setIdParticipant(Long.valueOf(listaLogger.get(i).getFilterValue()));
					clientPortafolioTO.setParticipantDesc(listaLogger.get(i).getFilterDescription());
					parametersReportExcel.put("participant", listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("cui_holder")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setCui(new Long(listaLogger.get(i).getFilterValue()).longValue());
					cuiDescription = listaLogger.get(i).getFilterDescription().toString();
					parametersReportExcel.put("cui_holder", listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("security_class")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setSecurityClass(listaLogger.get(i).getFilterValue());
					parametersReportExcel.put("security_class", listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("currency")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setIdCurrency(Integer.valueOf(listaLogger.get(i).getFilterValue()));
					parametersReportExcel.put("currency", listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("security_code")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setIdSecurityCode(listaLogger.get(i).getFilterValue());
					parametersReportExcel.put("security_code", listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("portafolio_type")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setPortafolioType(Integer.valueOf(listaLogger.get(i).getFilterValue()));
					parametersReportExcel.put("portafolio_type", listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("available_balance")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setShowAvailableBalance(Integer.valueOf(listaLogger.get(i).getFilterValue()));
					parametersReportExcel.put("available_balance", listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("date_disabled")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					
					clientPortafolioTO.setDateMarketFact(CommonsUtilities.convertStringtoDate(listaLogger.get(i).getFilterValue()));
					clientPortafolioTO.setDateMarketFactString(listaLogger.get(i).getFilterValue());
					parametersReportExcel.put("date_disabled", listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("account_holder")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setHolderAccountPk(Long.valueOf(listaLogger.get(i).getFilterValue()));
					parametersReportExcel.put("account_holder", listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("issuer")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setIdIssuer(listaLogger.get(i).getFilterValue());
					parametersReportExcel.put("issuer", listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("institution_type")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setInstitutionType(listaLogger.get(i).getFilterValue());
					parametersReportExcel.put("institution_type", listaLogger.get(i).getFilterValue());
				}
			}
		}
		//VALIDAMOS SI ES TIPO PARTICIPANTE INVERSIONISTA
		clientPortafolioTO.setIsParticipantInvestor(false);
		if(clientPortafolioTO.getInstitutionType()!=null && Integer.valueOf(clientPortafolioTO.getInstitutionType()).equals(InstitutionType.PARTICIPANT_INVESTOR.getCode())){
			clientPortafolioTO.setIsParticipantInvestor(true);
		}
		//VALIDAR TIPO DE CAMBIO A LA FECHA DE CORTE
		try{
			custodyReportService.getExchangeRate(clientPortafolioTO.getDateMarketFactString());
		} catch (ServiceException e){
			if(e.getErrorService().equals(ErrorServiceType.NOT_EXCHANGE_RATE)){
				throw new RuntimeErrorException(null, ReportConstant.ERROR_EXCHANGE_RATE + " " + clientPortafolioTO.getDateMarketFactString());
			}
			log.error(e.getMessage());
		}
		
		String holderAccountNumber = GeneralConstants.EMPTY_STRING;
		String CUI = GeneralConstants.EMPTY_STRING;
		String issuerDescription = GeneralConstants.EMPTY_STRING;
		String securityClassDesc = GeneralConstants.EMPTY_STRING;

			ParameterTableTO  filter = new ParameterTableTO();
			Map<Integer,String> currencyTypeShort = new HashMap<Integer, String>();
			Map<Integer,String> currencyType = new HashMap<Integer, String>();
			Map<Integer,String> securityClassShort = new HashMap<Integer, String>();
			Map<Integer,String> securityClass = new HashMap<Integer, String>();
			try {
				filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
				for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
					currencyTypeShort.put(param.getParameterTablePk(), param.getText1());
					currencyType.put(param.getParameterTablePk(), param.getParameterName());
				}
				filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
				for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
					securityClassShort.put(param.getParameterTablePk(), param.getText1());
					securityClass.put(param.getParameterTablePk(), param.getParameterName());
				}
				//PARA LA CUENTA TITULAR
				if(clientPortafolioTO.getHolderAccountPk() != null)
					holderAccountNumber = accountComponentServiceBean.find(clientPortafolioTO.getHolderAccountPk(),HolderAccount.class).getAccountNumber().toString();
				else
					holderAccountNumber = "Todos";
				//PARA EL CUI DEL TITULAR
				if(clientPortafolioTO.getCui() != null)
					CUI = clientPortafolioTO.getCui().toString() + " - " + cuiDescription;
				else
					CUI = "Todos";
				//EMISOR
				if(clientPortafolioTO.getIdIssuer() != null){
					Issuer iss = issuerServiceBean.find(Issuer.class, clientPortafolioTO.getIdIssuer());
					issuerDescription = iss.getMnemonic() + " - " + iss.getBusinessName();
				}else{
					issuerDescription = "Todos";
				}
				//CLASE DE VALOR
				if(clientPortafolioTO.getSecurityClass() != null){
					ParameterTable parameterClass = parameterService.getParameterTableById(Integer.parseInt(clientPortafolioTO.getSecurityClass()));
					securityClassDesc=parameterClass.getParameterName();
				}else{
					securityClassDesc = "Todos";
				}
				
			}catch(Exception ex){
				ex.printStackTrace();
			}
			/*TXT a XML*/
			if(clientPortafolioTO.getReportFormat().equals(ReportFormatType.TXT.getCode())){
				JAXBContext contextObj;
				try {
					 contextObj = JAXBContext.newInstance(XmlReportSirtexByParticipant.class);
					 XmlReportSirtexByParticipant jaxbElementMain = new XmlReportSirtexByParticipant();
					 List<XmlOperationsSirtex> operationsSirtexs = custodyReportService.getQueryClientOperationsDpfSirtexByParticipant(clientPortafolioTO,false);
					 XmlSecOperations secOperations =  new XmlSecOperations();
					 secOperations.setOperationsSirtexs(operationsSirtexs);
					 jaxbElementMain.setSecOperations(secOperations);
					 jaxbElementMain.setSecOperations(secOperations);
					
					 Marshaller marshallerObj = contextObj.createMarshaller();  
					 marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); 
					 StringWriter xmlResult = new StringWriter();
					 marshallerObj.marshal(jaxbElementMain, xmlResult);
					 reportUtilServiceBean.saveCustomReportFile("REPORT_BCB_XML"+(int) Math.ceil(Math.random() * 100), ReportFormatType.XML.getCode(), xmlResult.toString().getBytes(), getReportLogger());
					 return null;
				} catch (JAXBException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}  
				return null;
			}
		return null;
	}


	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
}
