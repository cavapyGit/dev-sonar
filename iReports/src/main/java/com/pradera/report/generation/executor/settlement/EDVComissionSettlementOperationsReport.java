package com.pradera.report.generation.executor.settlement;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;

@ReportProcess(name="EDVComissionSettlementOperationsReport")
public class EDVComissionSettlementOperationsReport extends GenericReport{
	
	private static final long serialVersionUID = 1L;
	
	@Inject PraderaLogger log;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> securityClassNnemonic = new HashMap<Integer, String>();
		Map<Integer,String> securityClassDescription = new HashMap<Integer, String>();
		Map<Integer,String> currencyNnemonic = new HashMap<Integer, String>();
		Map<Integer,String> currencyDescription = new HashMap<Integer, String>();
		
		try {
			
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				securityClassNnemonic.put(param.getParameterTablePk(), param.getText1());
			}
			
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				securityClassDescription.put(param.getParameterTablePk(), param.getParameterName());
			}
			
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				currencyNnemonic.put(param.getParameterTablePk(), param.getDescription());
			}
			
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				currencyDescription.put(param.getParameterTablePk(), param.getParameterName());
			}
			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		parametersRequired.put("p_security_class_mnemonic", securityClassNnemonic);
		parametersRequired.put("p_security_class_description", securityClassDescription);
		parametersRequired.put("p_currency_mnemonic", currencyNnemonic);
		parametersRequired.put("p_currency_description", currencyDescription);
		
		return parametersRequired;
	}

}
