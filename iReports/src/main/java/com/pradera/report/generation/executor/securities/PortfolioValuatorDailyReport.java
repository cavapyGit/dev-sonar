package com.pradera.report.generation.executor.securities;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.securities.service.PortfolioVauatorDailyReportServiceBean;

@ReportProcess(name = "PortfolioValuatorDailyReport")
public class PortfolioValuatorDailyReport extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private PortfolioVauatorDailyReportServiceBean portfolioVauatorDailyServiceBean;

	@Inject
	PraderaLogger log;

	public PortfolioValuatorDailyReport() {}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParametersQueryReport() {

	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {

		Map<String, Object> parametersRequired = new HashMap<>();
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		Long participant = null;
		Long cui_code = null;
		Long accounts = null;

		// DATOS DE ENTRADA DEL SISTEMA
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.PARTICIPANT_PARAM) && r.getFilterValue() != null) {
				participant = Long.valueOf(r.getFilterValue().toString());
			}
			if (r.getFilterName().equals(ReportConstant.CUI_CODE_PARAM) && r.getFilterValue() != null) {
				cui_code = Long.valueOf(r.getFilterValue().toString());
			}
			if (r.getFilterName().equals(ReportConstant.ACCOUNTS_PARAM) && r.getFilterValue() != null) {
				accounts = Long.valueOf(r.getFilterValue().toString());
			}
		}

		List<Long> accountsID = new ArrayList<>();
		try {
			
			if( cui_code != null && accounts == null){
				accountsID = portfolioVauatorDailyServiceBean.getAccountsID(cui_code, participant);
			}
		} catch (Exception ex) {
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}

		parametersRequired.put("accounts", accountsID.toArray());

		return parametersRequired;
	}
}