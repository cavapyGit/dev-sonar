package com.pradera.report.generation.executor.securities;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.billing.service.BillingServiceReportServiceBean;
import com.pradera.report.generation.executor.billing.to.ConsolidatedCollectionByEntityTO;
import com.pradera.report.generation.executor.securities.service.SecuritiesReportServiceBean;
import com.pradera.report.generation.poi.ReportPoiGeneratorServiceBean;

@ReportProcess(name = "PlacementSegmentReportPoi")
public class PlacementSegmentReportPoi  extends GenericReport{

	private static final long serialVersionUID = 1L;
	
	@EJB
	private SecuritiesReportServiceBean securitiesReportServiceBean;
	@Inject
	private ReportPoiGeneratorServiceBean reportPoiGeneratorServiceBean;

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		Map<String,Object> parametersRequired = new HashMap<>();
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] arrayByteExcel = new byte[16384];
		
		
		for (ReportLoggerDetail  loggerDetail: loggerDetails) {
			
			if (loggerDetail.getFilterName().equals(ReportConstant.PARTICIPANT_PARAM)){
				if(loggerDetail.getFilterValue() != null){
					parametersRequired.put(ReportConstant.PARTICIPANT_PARAM,loggerDetail.getFilterValue().toString());
					parametersRequired.put(ReportConstant.PARTICIPANT_DESC,loggerDetail.getFilterDescription());
				}else{
					parametersRequired.put(ReportConstant.PARTICIPANT_DESC,"TODOS");
				}
			}
			if (loggerDetail.getFilterName().equals(ReportConstant.ISSUER_PARAM)){
				if(loggerDetail.getFilterValue() != null){
					parametersRequired.put(ReportConstant.ISSUER_PARAM,loggerDetail.getFilterValue().toString());
					parametersRequired.put(ReportConstant.ISSUER_DESC,loggerDetail.getFilterDescription());
				}else{
					parametersRequired.put(ReportConstant.ISSUER_DESC,"TODOS");
				}
			}
			if (loggerDetail.getFilterName().equals(ReportConstant.SECURITY_CLASS_PARAM)){
				if(loggerDetail.getFilterValue() != null){
					parametersRequired.put(ReportConstant.SECURITY_CLASS_PARAM,loggerDetail.getFilterValue().toString());
					parametersRequired.put(ReportConstant.SECURITY_CLASS_DESC,loggerDetail.getFilterDescription());
				}else{
					parametersRequired.put(ReportConstant.SECURITY_CLASS_DESC,"TODAS");
				}
			}
			if (loggerDetail.getFilterName().equals(ReportConstant.SECURITY_CODE)){
				if(loggerDetail.getFilterValue() != null){
					parametersRequired.put(ReportConstant.SECURITY_CODE,loggerDetail.getFilterValue().toString());
					parametersRequired.put(ReportConstant.SECURITY_CODE_DESC,loggerDetail.getFilterDescription());
				}else{
					parametersRequired.put(ReportConstant.SECURITY_CODE_DESC,"TODOS");
				}
			}
			if (loggerDetail.getFilterName().equals(ReportConstant.DATE_END_PARAM)){
				if(loggerDetail.getFilterValue() != null)
					parametersRequired.put(ReportConstant.DATE_END_PARAM,loggerDetail.getFilterValue().toString());
			}
			if (loggerDetail.getFilterName().equals(ReportConstant.DATE_INITIAL_PARAM)){
				if(loggerDetail.getFilterValue() != null)
					parametersRequired.put(ReportConstant.DATE_INITIAL_PARAM,loggerDetail.getFilterValue().toString());
			}
		}
		
		List<Object[]> lstObject = securitiesReportServiceBean.getPlacementSegmentListByParameters(parametersRequired);
		parametersRequired.put("lstObjects", lstObject);
		
		/**GENERATE FILE EXCEL ***/
		arrayByteExcel = reportPoiGeneratorServiceBean.writeFileExcelConsPlacementSegmentReportPoi(parametersRequired,reportLogger);
		/**SETTING ARRAY BYTE TO GENERIC REPORT FOR PERSIST**/
		List<byte[]> bytes = new ArrayList<>();
		bytes.add(arrayByteExcel);
		setListByte(bytes);
		
		return baos;
	}

}
