package com.pradera.report.generation.executor.corporative.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class HolderBalanceAndInterestTO implements Comparator<HolderBalanceAndInterestTO>, Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer country;
	private Integer economicActivity;
	private Integer currency;
	private Integer indResidence;
	private String countryStr;
	private Integer groupList;
	private Integer indInterestBalance;
	private BigDecimal balanceCutoff;
	private BigDecimal balanceNet;
	private Integer totalHolder;
	private BigDecimal balanceCutoffReport;
	private BigDecimal balanceNetReport;
	private Integer totalHolderReport;

	 
	public BigDecimal getBalanceCutoff() {
		return balanceCutoff;
	}
	public void setBalanceCutoff(BigDecimal balanceCutoff) {
		this.balanceCutoff = balanceCutoff;
	}
	public BigDecimal getBalanceNet() {
		return balanceNet;
	}
	public void setBalanceNet(BigDecimal balanceNet) {
		this.balanceNet = balanceNet;
	}
	public Integer getTotalHolder() {
		return totalHolder;
	}
	public void setTotalHolder(Integer totalHolder) {
		this.totalHolder = totalHolder;
	}
	public BigDecimal getBalanceCutoffReport() {
		return balanceCutoffReport;
	}
	public void setBalanceCutoffReport(BigDecimal balanceCutoffReport) {
		this.balanceCutoffReport = balanceCutoffReport;
	}
	public BigDecimal getBalanceNetReport() {
		return balanceNetReport;
	}
	public void setBalanceNetReport(BigDecimal balanceNetReport) {
		this.balanceNetReport = balanceNetReport;
	}
	public Integer getTotalHolderReport() {
		return totalHolderReport;
	}
	public void setTotalHolderReport(Integer totalHolderReport) {
		this.totalHolderReport = totalHolderReport;
	}
	public Integer getIndInterestBalance() {
		return indInterestBalance;
	}
	public void setIndInterestBalance(Integer indInterestBalance) {
		this.indInterestBalance = indInterestBalance;
	}
	public Integer getGroupList() {
		return groupList;
	}
	public void setGroupList(Integer groupList) {
		this.groupList = groupList;
	}
	public String getCountryStr() {
		return countryStr;
	}
	public void setCountryStr(String countryStr) {
		this.countryStr = countryStr;
	}
	private Map<Integer,BigDecimal> mapBalances;
	
	public Integer getCountry() {
		return country;
	}
	public HolderBalanceAndInterestTO() {
		Calendar calendar = Calendar.getInstance();
		mapBalances = new HashMap<Integer,BigDecimal>();
  		Map<String, Integer> mapMonths = calendar.getDisplayNames(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH);
 		for(Map.Entry<String, Integer> entry: mapMonths.entrySet()){
 			mapBalances.put(entry.getValue(), BigDecimal.ZERO);
 			
 		}

		
 	}
	public void setCountry(Integer country) {
		this.country = country;
	}
	public Integer getEconomicActivity() {
		return economicActivity;
	}
	public void setEconomicActivity(Integer economicActivity) {
		this.economicActivity = economicActivity;
	}
	public Integer getCurrency() {
		return currency;
	}
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	public Integer getIndResidence() {
		return indResidence;
	}
	public void setIndResidence(Integer indResidence) {
		this.indResidence = indResidence;
	}
	public Map<Integer, BigDecimal> getMapBalances() {
		return mapBalances;
	}
	public void setMapBalances(Map<Integer, BigDecimal> mapBalances) {
		this.mapBalances = mapBalances;
	}
	@Override
	public int compare(HolderBalanceAndInterestTO o1,HolderBalanceAndInterestTO o2) {
		return o1.getGroupList().compareTo(o2.getGroupList());
 	}
 
 

}
