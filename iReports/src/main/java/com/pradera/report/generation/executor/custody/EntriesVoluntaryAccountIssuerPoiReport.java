package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.executor.custody.to.BalanceTransferAvailableTO;
import com.pradera.report.generation.executor.custody.to.EntriesVoluntaryAccountIssuerTO;
import com.pradera.report.generation.executor.custody.to.RequestAccreditationCertificationTO;
import com.pradera.report.generation.poi.ReportPoiGeneratorServiceBean;
import com.pradera.report.generation.service.ReportUtilServiceBean;

@ReportProcess(name = "EntriesVoluntaryAccountIssuerPoiReport")
public class EntriesVoluntaryAccountIssuerPoiReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private CustodyReportServiceBean custodyReportServiceBean;
	@EJB
	private ReportUtilServiceBean reportUtilServiceBean;

	/** The Report poi generator service bean. */
	@Inject
	private ReportPoiGeneratorServiceBean reportPoiGeneratorServiceBean;
	@Inject
	private PraderaLogger log;

	private EntriesVoluntaryAccountIssuerTO entriesVoluntaryAccountIssuerTO;
	
	public EntriesVoluntaryAccountIssuerPoiReport() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {


		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] arrayByteExcel = new byte[16384];
		List<Object[]> queryMain=new ArrayList<Object[]>();
		
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		Map<String,Object> parametersRequired = new HashMap<>();
		entriesVoluntaryAccountIssuerTO = new EntriesVoluntaryAccountIssuerTO();

		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("date_end")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
//					registrationAssignmentHoldersTO.setDateInitial(CommonsUtilities.convertStringtoDate(listaLogger.get(i).getFilterValue()));
					entriesVoluntaryAccountIssuerTO.setStrDateEnd(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("date_initial")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
//					registrationAssignmentHoldersTO.setDateInitial(CommonsUtilities.convertStringtoDate(listaLogger.get(i).getFilterValue()));
					entriesVoluntaryAccountIssuerTO.setStrDateInitial(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("operation_state")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					entriesVoluntaryAccountIssuerTO.setOperationState(Integer.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("reason")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					entriesVoluntaryAccountIssuerTO.setMotive(Integer.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("key_value")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					entriesVoluntaryAccountIssuerTO.setSecurity(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("class_security")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					entriesVoluntaryAccountIssuerTO.setSecurityClass(Integer.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("cui_holder")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					entriesVoluntaryAccountIssuerTO.setIdHolderPk(Long.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("account_holder")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					entriesVoluntaryAccountIssuerTO.setIdHolderAccountPk(Long.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("participant")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					entriesVoluntaryAccountIssuerTO.setParticipantPk(Long.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("issuer")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					entriesVoluntaryAccountIssuerTO.setIssuer(listaLogger.get(i).getFilterValue());
				}
			}
			
			if(listaLogger.get(i).getFilterName().equals("request_number")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					entriesVoluntaryAccountIssuerTO.setRequestNumber(Integer.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("institution_type")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					entriesVoluntaryAccountIssuerTO.setInstitutionType(Integer.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
		}
		
			String strQuery = null;
			strQuery = custodyReportServiceBean.getQueryAccountVoluntaryEntryByIssuer(entriesVoluntaryAccountIssuerTO);
			queryMain=custodyReportServiceBean.getQueryListByClass(strQuery);
			
			parametersRequired.put("str_query", queryMain);
			
			/**GENERATE FILE EXCEL ***/
			arrayByteExcel=reportPoiGeneratorServiceBean.writeFileExcelEntriesVoluntaryAccountIssuer(parametersRequired,reportLogger);
			
			/**SETTING ARRAY BYTE TO GENERIC REPORT FOR PERSIST**/
			List<byte[]> bytes = new ArrayList<>();
			bytes.add(arrayByteExcel);
			setListByte(bytes);

			return baos;
	}
}