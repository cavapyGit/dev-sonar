package com.pradera.report.generation.executor.settlement;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.integration.common.validation.Validations;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.billing.service.BillingServiceReportServiceBean;
import com.pradera.report.generation.executor.settlement.service.SettlementReportServiceBean;
import com.pradera.report.generation.executor.settlement.to.EDVComissionSettlementOperationTo;
import com.pradera.report.generation.poi.ReportPoiGeneratorServiceBean;

@ReportProcess(name="EDVComissionSettlementOperationsReportPoi")
public class EDVComissionSettlementOperationsReportPoi extends GenericReport  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private BillingServiceReportServiceBean billingServiceReportServiceBean;
	
	@EJB
	private SettlementReportServiceBean settlementReportServiceBean;
	
	/** The Report poi generator service bean. */
	@Inject
	private ReportPoiGeneratorServiceBean reportPoiGeneratorServiceBean;

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] arrayByteExcel = new byte[16384];
		EDVComissionSettlementOperationTo parameterTo=new EDVComissionSettlementOperationTo();
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		Map<String,Object> parametersRequired = new HashMap<>();
		
		for (int i = 0; i < listaLogger.size(); i++){
			
			if(listaLogger.get(i).getFilterName().equals("modality"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
			    	parameterTo.setModality(listaLogger.get(i).getFilterValue());
			    }
		    if(listaLogger.get(i).getFilterName().equals("participant"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
			    	parameterTo.setParticipant(listaLogger.get(i).getFilterValue());
			    }
		    if(listaLogger.get(i).getFilterName().equals("cui_code"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
			    	parameterTo.setCuiCode(listaLogger.get(i).getFilterValue());
			    }
		    if(listaLogger.get(i).getFilterName().equals("holder_account"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
			    	parameterTo.setHolderAccount(listaLogger.get(i).getFilterValue());
			    }
		    if(listaLogger.get(i).getFilterName().equals("security_class"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
			    	parameterTo.setSecurityClass(listaLogger.get(i).getFilterValue());
			    }
		    if(listaLogger.get(i).getFilterName().equals("date_initial"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
			    	parameterTo.setInitialDate(listaLogger.get(i).getFilterValue());
			    }
		    if(listaLogger.get(i).getFilterName().equals("date_end"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
			    	parameterTo.setFinalDate(listaLogger.get(i).getFilterValue());
			    }
		}
		
		String strQueryFormated = settlementReportServiceBean.getEDVCommisionSettlementOperationReport(parameterTo);
		List<Object[]> lstObject = settlementReportServiceBean.getQueryListByClass(strQueryFormated);
		parametersRequired.put("lstObjects", lstObject);
		
		/**GENERATE FILE EXCEL ***/
		arrayByteExcel = reportPoiGeneratorServiceBean.writeFileExcelEDVComissionSettlementOperation(parametersRequired,reportLogger);
		
		/**SETTING ARRAY BYTE TO GENERIC REPORT FOR PERSIST**/
		List<byte[]> bytes = new ArrayList<>();
		bytes.add(arrayByteExcel);
		setListByte(bytes);
		
		return baos;
	}

}
