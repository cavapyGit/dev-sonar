package com.pradera.report.generation.executor.account;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.account.service.ApplicantReportServiceBean;
import com.pradera.report.util.view.UtilReportConstants;
/**
* 
* <ul><li>Copyright EDV 2020.</li></ul> 
* @author RCHIARA.
* 
*
**/
@ReportProcess(name="PersonSearchLog")
public class PersonSearchLog extends GenericReport {
	private static final long serialVersionUID = 1L;
	
	@Inject
	PraderaLogger log;
	
	@EJB	
	private ApplicantReportServiceBean applicantReportServiceBean;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		Map<String,Object> parameters = new HashMap<>();
		Date date_initial = null, date_end = null;
		String doc_number = null;
		
		for (int i = 0; i < listaLogger.size(); i++) {			
			if(listaLogger.get(i).getFilterName().equals("date_initial")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					date_initial = CommonsUtilities.convertStringtoDate(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("date_end")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					date_end = CommonsUtilities.convertStringtoDate(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("doc_number")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					doc_number = listaLogger.get(i).getFilterValue();
				}
			}
		}
		
		String strQuery = null;
		String title = "REPORTE DE BUSQUEDA DE PERSONAS";
		strQuery = applicantReportServiceBean.getQueryPersonSearchLog(date_initial, date_end, doc_number);		
		parameters.put("report_title_customize", title);
		parameters.put("str_query", strQuery);
		parameters.put("date_initial",CommonsUtilities.convertDateToString(date_initial,"dd/MM/yyyy"));
		parameters.put("date_end",CommonsUtilities.convertDateToString(date_end,"dd/MM/yyyy"));
		parameters.put("logo_path", UtilReportConstants.readLogoReport());

		return parameters;		
	}

}
