package com.pradera.report.generation.executor.settlement;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.integration.common.validation.Validations;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.settlement.service.SettlementReportServiceBean;
import com.pradera.report.generation.executor.settlement.to.TransferExtrabursatilSirtexTO;
import com.pradera.report.generation.poi.ReportPoiGeneratorServiceBean;

@ReportProcess(name = "TransferExtrabursatilSirtexReportPoi")
public class TransferExtrabursatilSirtexReportPoi extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private SettlementReportServiceBean settlementReportServiceBean;
	@Inject
	private ReportPoiGeneratorServiceBean reportPoiGeneratorServiceBean;

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] arrayByteExcel = new byte[16384];
		TransferExtrabursatilSirtexTO transferExtrabursatilSirtexTO = new TransferExtrabursatilSirtexTO();
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		Map<String,Object> parametersRequired = new HashMap<>();
		
		for (int i = 0; i < listaLogger.size(); i++){
			
			if(listaLogger.get(i).getFilterName().equals("p_participant"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	transferExtrabursatilSirtexTO.setIdParticipant(listaLogger.get(i).getFilterValue());
			
			if(listaLogger.get(i).getFilterName().equals("p_cui"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	transferExtrabursatilSirtexTO.setIdHolder(listaLogger.get(i).getFilterValue());
			
			if(listaLogger.get(i).getFilterName().equals("issuer"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	transferExtrabursatilSirtexTO.setIdIssuer(listaLogger.get(i).getFilterValue());
			
			if(listaLogger.get(i).getFilterName().equals("security_class"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	transferExtrabursatilSirtexTO.setSecurityClass(listaLogger.get(i).getFilterValue());
			
			if(listaLogger.get(i).getFilterName().equals("security_code"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	transferExtrabursatilSirtexTO.setIdSecurityCodePk(listaLogger.get(i).getFilterValue());
			
			if(listaLogger.get(i).getFilterName().equals("currency"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	transferExtrabursatilSirtexTO.setCurrency(listaLogger.get(i).getFilterValue());
			
			if(listaLogger.get(i).getFilterName().equals("mechanism"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	transferExtrabursatilSirtexTO.setMecanismoNeg(listaLogger.get(i).getFilterValue());
			
			if(listaLogger.get(i).getFilterName().equals("val_pub_priv"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	transferExtrabursatilSirtexTO.setValPublicOrPrivate(listaLogger.get(i).getFilterValue());
			
			if(listaLogger.get(i).getFilterName().equals("modality"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	transferExtrabursatilSirtexTO.setModalidadNeg(listaLogger.get(i).getFilterValue());
			
			if(listaLogger.get(i).getFilterName().equals("way_to_py"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	transferExtrabursatilSirtexTO.setFormaPago(listaLogger.get(i).getFilterValue());

			if(listaLogger.get(i).getFilterName().equals("p_initial_date"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	transferExtrabursatilSirtexTO.setInitialDate(listaLogger.get(i).getFilterValue());
			
			if(listaLogger.get(i).getFilterName().equals("p_final_date"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	transferExtrabursatilSirtexTO.setFinalDate(listaLogger.get(i).getFilterValue());
		}
		
		String strQueryFormated = settlementReportServiceBean.getQueryTransferExtrabursatilSirtex(transferExtrabursatilSirtexTO);
		List<Object[]> lstObject = settlementReportServiceBean.getQueryListByClass(strQueryFormated);
		parametersRequired.put("lstObjects", lstObject);
		
		arrayByteExcel = reportPoiGeneratorServiceBean.writeFileExcelTransferExtrabursatilSirtex(parametersRequired, reportLogger);
		
		List<byte[]> bytes = new ArrayList<>();
		bytes.add(arrayByteExcel);
		setListByte(bytes);
		
		return baos;
	}

}
