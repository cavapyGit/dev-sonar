package com.pradera.report.generation.executor.billing;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.billing.type.EntityCollectionType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.util.view.UtilReportConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ConsolidatedCollectionByEntityReport.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@ReportProcess(name = "ConsolidatedCollectionByEntityReport")
public class ConsolidatedCollectionByEntityReport extends GenericReport {
  
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The parameter service. */
	@EJB
	private ParameterServiceBean parameterService;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	@EJB 
	private BatchProcessServiceFacade batchProcessServiceFacade;
	
	/**
	 * Instantiates a new consolidated collection by entity report.
	 */
	public ConsolidatedCollectionByEntityReport() {
	}

	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#generateData(com.pradera.model.report.ReportLogger)
	 */
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		return null;
	}
 
	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#addParametersQueryReport()
	 */
	@Override
	public void addParametersQueryReport() {
		

	}

	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#getCustomJasperParameters()
	 */
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		Map<String,Object> parametersRequired = new HashMap<>();
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		boolean automatic = Boolean.FALSE;
		Map<String,Object> parametersBatch = new HashMap<String, Object>();	
		
		for (ReportLoggerDetail  loggerDetail: loggerDetails) {
			
			if (loggerDetail.getFilterName().equals("entity_collection")){
				if (loggerDetail.getFilterValue() != null) parametersBatch.put("entity_collection", loggerDetail.getFilterValue());
			}
			if (loggerDetail.getFilterName().equals("collection_state")){
				if (loggerDetail.getFilterValue() != null) parametersBatch.put("collection_state", loggerDetail.getFilterValue());
			}
			if (loggerDetail.getFilterName().equals("issuer")){
				if (loggerDetail.getFilterValue() != null) parametersBatch.put("issuer", loggerDetail.getFilterValue());
			}
			if (loggerDetail.getFilterName().equals("participant")){
				if (loggerDetail.getFilterValue() != null) parametersBatch.put("participant", loggerDetail.getFilterValue());
			}
			if (loggerDetail.getFilterName().equals("holder")){
				if (loggerDetail.getFilterValue() != null) parametersBatch.put("holder", loggerDetail.getFilterValue());
			}
			if (loggerDetail.getFilterName().equals("other_number")){
				if (loggerDetail.getFilterValue() != null) parametersBatch.put("other_number", loggerDetail.getFilterValue());
			}
			if (loggerDetail.getFilterName().equals("other_description")){
				if (loggerDetail.getFilterValue() != null) parametersBatch.put("other_description", loggerDetail.getFilterValue());
			}
			if (loggerDetail.getFilterName().equals("afp")){
				if (loggerDetail.getFilterValue() != null) parametersBatch.put("afp", loggerDetail.getFilterValue());
			}
			if (loggerDetail.getFilterName().equals("date_initial")){
				if (loggerDetail.getFilterValue() != null) parametersBatch.put("date_initial", loggerDetail.getFilterValue());
			}
			if (loggerDetail.getFilterName().equals("date_end")){
				if (loggerDetail.getFilterValue() != null) parametersBatch.put("date_end", loggerDetail.getFilterValue());
			}
			if (loggerDetail.getFilterName().equals("all_services") && loggerDetail.getFilterValue().equalsIgnoreCase(BooleanType.YES.getValue())){
				automatic = true;
				//seteando nulo en el servicio por que se enviara de todos por batchero
				for(ReportLoggerDetail  logger: loggerDetails){
					if (logger.getFilterName().equals("p_service_code") && logger.getFilterValue() != null){
						logger.setFilterValue(null);
					}
				}
			}
		}
		
		//Generando Batchero para enviar todos los servicio
		if(automatic){
			parametersBatch.put("idReportPk", getReportLogger().getReport().getIdReportPk());
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(40877L);//Agregar a bussines process type
			try {
				batchProcessServiceFacade.registerBatch(EntityCollectionType.MANAGER.getValue(),businessProcess,parametersBatch);
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		
		return parametersRequired;
	}

}
