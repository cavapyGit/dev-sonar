package com.pradera.report.generation.executor.negotiation.to;

public class GenericNegotiationTO {

	private String participant;
	private String reportOf;
	private String formatBcb;
	private String initialDate;
	private String finalDate;
	
	public String getParticipant() {
		return participant;
	}
	public void setParticipant(String participant) {
		this.participant = participant;
	}
	public String getReportOf() {
		return reportOf;
	}
	public void setReportOf(String reportOf) {
		this.reportOf = reportOf;
	}
	public String getFormatBcb() {
		return formatBcb;
	}
	public void setFormatBcb(String formatBcb) {
		this.formatBcb = formatBcb;
	}
	public String getInitialDate() {
		return initialDate;
	}
	public void setInitialDate(String initialDate) {
		this.initialDate = initialDate;
	}
	public String getFinalDate() {
		return finalDate;
	}
	public void setFinalDate(String finalDate) {
		this.finalDate = finalDate;
	}
	
	
	
}
