package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.management.RuntimeErrorException;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.executor.custody.to.GenericCustodyOperationTO;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "OverdueTitlesReport")
public class OverdueTitlesReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@Inject PraderaLogger log;

	@EJB
	private ParameterServiceBean parameterService;
	@EJB
	private CustodyReportServiceBean custodyReportService;

	public OverdueTitlesReport() {
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		return null;
	}

	@Override
	public void addParametersQueryReport() {
		List<ReportLoggerDetail> listaLogger = getReportLogger()
				.getReportLoggerDetails();
		if (listaLogger == null) {
			listaLogger = new ArrayList<ReportLoggerDetail>();
		}
	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		Map<String,Object> parametersRequired = new HashMap<>();
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		GenericCustodyOperationTO to = new GenericCustodyOperationTO();
		
		
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.CUI_CODE_PARAM)){
				if(r.getFilterValue() != null){
					to.setHolder(r.getFilterValue());
				}
			}
			if (r.getFilterName().equals(ReportConstant.HOLDER_ACCOUNT)){
				if(r.getFilterValue() != null){
					to.setHolderAccount(r.getFilterValue());
				}
			}
			if (r.getFilterName().equals(ReportConstant.VALUE_SERIES)){
				if(r.getFilterValue() != null){
					to.setSecurities(r.getFilterValue());
				}
			}
			if (r.getFilterName().equals(ReportConstant.CLASS_VALUE)){
				if(r.getFilterValue() != null){
					to.setSecurityClass(r.getFilterValue());
				}
			}
			if (r.getFilterName().equals(ReportConstant.CURRENCY)){
				if(r.getFilterValue() != null){
					to.setCurrency(r.getFilterValue());
				}
			}
			if (r.getFilterName().equals(ReportConstant.PARTICIPANT_PARAM)){
				if(r.getFilterValue() != null){
					to.setParticipant(r.getFilterValue());
				}
			}
			if (r.getFilterName().equals(ReportConstant.DATE_INITIAL_PARAM)){
				if(r.getFilterValue() != null){
					to.setInitialDate(r.getFilterValue());
				}
			}
			if (r.getFilterName().equals(ReportConstant.DATE_END_PARAM)){
				if(r.getFilterValue() != null){
					to.setFinalDate(r.getFilterValue());
				}
			}
			if (r.getFilterName().equals(ReportConstant.STATE_PARAM)){
				if(r.getFilterValue() != null){
					to.setState(r.getFilterValue());
				}
			}
		}
		
		String strQuery = custodyReportService.getOverdueTitles(to);
		parametersRequired.put("str_query", strQuery);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		
		return parametersRequired;
	}
}