package com.pradera.report.generation.executor.issuances.to;

import java.io.Serializable;

/**
 * issue 1200: to para capturar los parametros del reporte REPORTE DE CODIGO
 * UNICO DE IDENTIFICACION
 * 
 * @author equinajo
 */
public class ListHoldersCuiReportTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer docType;
	private Integer participant;
	private String docNumber;
	private Integer stateHolder;
	private String securitiesBalance;
	private Integer typePerson;
	private Integer economicActivity;
	private String dateInitial;
	private String dateEnd;

	// cosntructor
	public ListHoldersCuiReportTO() {
	}

	// TODO getter and setter
	public Integer getDocType() {
		return docType;
	}

	public void setDocType(Integer docType) {
		this.docType = docType;
	}

	public Integer getParticipant() {
		return participant;
	}

	public void setParticipant(Integer participant) {
		this.participant = participant;
	}

	public String getDocNumber() {
		return docNumber;
	}

	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}

	public Integer getStateHolder() {
		return stateHolder;
	}

	public void setStateHolder(Integer stateHolder) {
		this.stateHolder = stateHolder;
	}

	public String getSecuritiesBalance() {
		return securitiesBalance;
	}

	public void setSecuritiesBalance(String securitiesBalance) {
		this.securitiesBalance = securitiesBalance;
	}

	public Integer getTypePerson() {
		return typePerson;
	}

	public void setTypePerson(Integer typePerson) {
		this.typePerson = typePerson;
	}

	public Integer getEconomicActivity() {
		return economicActivity;
	}

	public void setEconomicActivity(Integer economicActivity) {
		this.economicActivity = economicActivity;
	}

	public String getDateInitial() {
		return dateInitial;
	}

	public void setDateInitial(String dateInitial) {
		this.dateInitial = dateInitial;
	}

	public String getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(String dateEnd) {
		this.dateEnd = dateEnd;
	}

}
