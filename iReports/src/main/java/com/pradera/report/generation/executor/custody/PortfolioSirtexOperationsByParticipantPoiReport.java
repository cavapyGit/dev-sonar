package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.executor.custody.to.ClientPortafolioTO;
import com.pradera.report.generation.executor.custody.to.XmlOperationsSirtex;
import com.pradera.report.generation.poi.ReportPoiGeneratorServiceBean;

@ReportProcess(name = "PortfolioSirtexOperationsByParticipantPoiReport")
public class PortfolioSirtexOperationsByParticipantPoiReport extends GenericReport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3978984983876600055L;
	
	private ClientPortafolioTO clientPortafolioTO;
	
	@EJB
	private CustodyReportServiceBean custodyReportService;
	
	@Inject
	private ReportPoiGeneratorServiceBean reportPoiGeneratorServiceBean;
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] arrayByteExcel = new byte[16384];
		
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		clientPortafolioTO = new ClientPortafolioTO();

		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("participant")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setIdParticipant(Long.valueOf(listaLogger.get(i).getFilterValue()));
					clientPortafolioTO.setParticipantDesc(listaLogger.get(i).getFilterDescription());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("cui_holder")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setCui(new Long(listaLogger.get(i).getFilterValue()).longValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("security_class")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setSecurityClass(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("currency")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setIdCurrency(Integer.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("security_code")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setIdSecurityCode(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("portafolio_type")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setPortafolioType(Integer.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("available_balance")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setShowAvailableBalance(Integer.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("date_disabled")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					
					clientPortafolioTO.setDateMarketFact(CommonsUtilities.convertStringtoDate(listaLogger.get(i).getFilterValue()));
					clientPortafolioTO.setDateMarketFactString(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("account_holder")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setHolderAccountPk(Long.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("issuer")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setIdIssuer(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("institution_type")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setInstitutionType(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("reportFormats")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setReportFormat(Integer.parseInt(listaLogger.get(i).getFilterValue()));
				}
			}
		}
		List<XmlOperationsSirtex> operationsSirtexs = custodyReportService.getQueryClientOperationsDpfSirtexByParticipant(clientPortafolioTO,true);
		/**GENERATE FILE EXCEL ***/
		try {
			arrayByteExcel=reportPoiGeneratorServiceBean.writeFileExcelClientSirtex(operationsSirtexs,reportLogger);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/**SETTING ARRAY BYTE TO GENERIC REPORT FOR PERSIST**/
		List<byte[]> bytes = new ArrayList<>();
		bytes.add(arrayByteExcel);
		setListByte(bytes);
		return baos;
	}
}
