package com.pradera.report.generation.executor.account.to;

import java.io.Serializable;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * The Class SummaryPossesionHolderAcountTO.
 */
public class SummaryPossesionHolderAcountTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The id holder pk. */
	private Long idHolderPk;
	
	/** The account number. */
	private Integer accountNumber;
	
	/** The Valuation type. */
	private Integer ValuationType;
	
	/** The id isin code pk. */
	private String idIsinCodePk;
	
	/** The cuttoff date. */
	private Date cuttoffDate;

	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * Gets the id holder pk.
	 *
	 * @return the id holder pk
	 */
	public Long getIdHolderPk() {
		return idHolderPk;
	}

	/**
	 * Sets the id holder pk.
	 *
	 * @param idHolderPk the new id holder pk
	 */
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}

	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public Integer getAccountNumber() {
		return accountNumber;
	}

	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * Gets the valuation type.
	 *
	 * @return the valuation type
	 */
	public Integer getValuationType() {
		return ValuationType;
	}

	/**
	 * Sets the valuation type.
	 *
	 * @param valuationType the new valuation type
	 */
	public void setValuationType(Integer valuationType) {
		ValuationType = valuationType;
	}

	/**
	 * Gets the id isin code pk.
	 *
	 * @return the id isin code pk
	 */
	public String getIdIsinCodePk() {
		return idIsinCodePk;
	}

	/**
	 * Sets the id isin code pk.
	 *
	 * @param idIsinCodePk the new id isin code pk
	 */
	public void setIdIsinCodePk(String idIsinCodePk) {
		this.idIsinCodePk = idIsinCodePk;
	}

	/**
	 * Gets the cuttoff date.
	 *
	 * @return the cuttoff date
	 */
	public Date getCuttoffDate() {
		return cuttoffDate;
	}

	/**
	 * Sets the cuttoff date.
	 *
	 * @param cuttoffDate the new cuttoff date
	 */
	public void setCuttoffDate(Date cuttoffDate) {
		this.cuttoffDate = cuttoffDate;
	}

	/**
	 * Gets the serialversionuid.
	 *
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
