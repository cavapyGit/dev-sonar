package com.pradera.report.generation.executor.custody.to;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


@XmlAccessorType(XmlAccessType.FIELD) 
public class XmlSecOperations implements  Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2848021971858736761L;
	@XmlElement(name = "SECC_OPERACION")
	private List<XmlOperationsSirtex> operationsSirtexs;
	
	public List<XmlOperationsSirtex> getOperationsSirtexs() {
		return operationsSirtexs;
	}
	public void setOperationsSirtexs(List<XmlOperationsSirtex> operationsSirtexs) {
		this.operationsSirtexs = operationsSirtexs;
	}
}
