
package com.pradera.report.generation.executor.account;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.management.RuntimeErrorException;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.type.EconomicActivityType;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.account.service.AccountReportServiceBean;
import com.pradera.report.generation.executor.account.to.PortafolioDetailCustodianTO;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.service.ReportUtilServiceBean;
import com.pradera.report.util.view.PropertiesConstants;

@ReportProcess(name = "PortafolioDetailCustodianReport")
public class PortafolioDetailCustodianReport extends GenericReport{
	private static final long serialVersionUID = 1L;
	@EJB
	private ParameterServiceBean parameterService;
	@EJB
	private AccountReportServiceBean accountReportService;
	@EJB
	private ReportUtilServiceBean reportUtilServiceBean;
	@Inject
	private PraderaLogger log;
	@EJB
	CustodyReportServiceBean custodyReportServiceBean;
	
	private String issuer_Ametex="BO047";
	private String AFP_FUTURO = "FUTURO DE BOLIVIA";
	private String AFP_PREVISION = "PREVISION";
	private String BISA_SAFI = "BISA";
	private String FCC="FCC";
	private String FCI="FCI";
	private String FRD="FRD";
	private Integer reportFormat;
	private PortafolioDetailCustodianTO pdcTO;
	public static final String EIGHT_ZERO_DECIMAL_STRING ="0.00000000";
	
	public PortafolioDetailCustodianReport() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		String title=null;
		BigDecimal exchange_rate_dollar=null;
		BigDecimal exchange_rate_ufv=null;
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		Map<String,Object> parametersRequired = new HashMap<>();
		pdcTO = new PortafolioDetailCustodianTO();

		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("cui_holder")){
				if (Validations.validateIsNotNullAndPositive(Long.valueOf(listaLogger.get(i).getFilterValue()))){
					pdcTO.setCui(new Long(listaLogger.get(i).getFilterValue()).longValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("cui_description")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					pdcTO.setHolderDesc(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("document_type")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					pdcTO.setDocumentType(Integer.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("document_number")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					pdcTO.setDocumentNumber(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("security_pk")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					pdcTO.setIdSecurityCode(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("fund_type")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					pdcTO.setFundType(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("all_fund_types_jasper")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					pdcTO.setLstFundTypes(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("all_fund_types_txt")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					pdcTO.setListFundTypes(fillFunds());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("economic_activity")){
				if (Validations.validateIsNotNullAndPositive(Integer.valueOf(listaLogger.get(i).getFilterValue()))){
					pdcTO.setEconomicActivity(Integer.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("date")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					pdcTO.setDateMarketFact(CommonsUtilities.convertStringtoDate(listaLogger.get(i).getFilterValue()));
					pdcTO.setDateMarketFactString(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("report_format")){
				if (Validations.validateIsNotNullAndPositive(Integer.valueOf(listaLogger.get(i).getFilterValue()))){
					reportFormat= Integer.valueOf(listaLogger.get(i).getFilterValue());
				}
			}
		}
		
		
		try {
			//set the issuer ametex
			pdcTO.setIdIssuer(issuer_Ametex);
			//get all the query in a string format
			String strQuery = accountReportService.getQueryPortafolioDetailCustodian(pdcTO);
			if(ReportFormatType.PDF.getCode().equals(reportFormat)){
				String strQueryFormatted = accountReportService.getQueryFormatForJasper(pdcTO, strQuery);
				//get exchange rate
				DailyExchangeRates currency=new DailyExchangeRates();
				currency.setIdCurrency(CurrencyType.USD.getCode());
				currency.setDateRate(pdcTO.getDateMarketFact());
				exchange_rate_dollar= accountReportService.getExchangeRateByCurrencyType(currency);
				currency.setIdCurrency(CurrencyType.UFV.getCode());
				exchange_rate_ufv = accountReportService.getExchangeRateByCurrencyType(currency);
	
				ParameterTableTO  filter = new ParameterTableTO();
				Map<Integer,String> docTypeShort = new HashMap<Integer, String>();
				try {
					filter.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
					for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
						docTypeShort.put(param.getParameterTablePk(), param.getIndicator1());
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
				
				if(FRD.equals(pdcTO.getFundType())){
					title="DETALLE DE CUSTODIA DEL FONDO DE RENTA UNIVERSAL DE VEJEZ";//DEL FRUV
				}else if(FCI.equals(pdcTO.getFundType())){
					title="DETALLE DE CUSTODIA AFP FCI";
				}else if(FCC.equals(pdcTO.getFundType())){
					title="DETALLE DE CUSTODIA AFP FCC";
				}else{
					title="DETALLE DE CUSTODIA ASEGURADORAS";
				}
				
				parametersRequired.put("report_title_customize", title);
				parametersRequired.put("str_query", strQueryFormatted);
				parametersRequired.put("p_docTypeShort", docTypeShort);
				parametersRequired.put("exchange_rate", exchange_rate_dollar.toString());
				parametersRequired.put("exchange_rate_ufv", CommonsUtilities.formatDecimal(exchange_rate_ufv, ReportConstant.FORMAT_ONLY_FOUR_DECIMALS));
				parametersRequired.put("holder_description", pdcTO.getHolderDesc());
				parametersRequired.put("document_type", docTypeShort.get(pdcTO.getDocumentType()));
				parametersRequired.put("document_number", pdcTO.getDocumentNumber());
			}else if(ReportFormatType.TXT.getCode().equals(reportFormat)){
				List<PortafolioDetailCustodianTO> lstPDC= accountReportService.getPortafolioDetailCustodianTxt(pdcTO, strQuery);
				StringBuilder sbTxtFormat = new StringBuilder();
				String comilla= "\"";
				BigDecimal dataNegative=new BigDecimal("-9999");
				for (PortafolioDetailCustodianTO pdc: lstPDC) {
					sbTxtFormat.append(comilla+pdc.getInstrumentSecClass()+comilla);
					sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
					sbTxtFormat.append(comilla+pdc.getIdSecurityCode()+comilla);
					sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
					if(pdc.getMarketRate().equals(GeneralConstants.ZERO_VALUE_BIGDECIMAL)
							|| pdc.getMarketRate().equals(dataNegative) ){
						sbTxtFormat.append(comilla+EIGHT_ZERO_DECIMAL_STRING+comilla);
					}else{
						sbTxtFormat.append(comilla+CommonsUtilities.formatDecimal(pdc.getMarketRate(), ReportConstant.FORMAT_ONLY_EIGTH_DECIMALS)+comilla);
					}
					sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
					if(pdc.getInitialNominalValue().equals(GeneralConstants.ZERO_VALUE_BIGDECIMAL)){
						sbTxtFormat.append(comilla+GeneralConstants.ZERO_DECIMAL_STRING+comilla);
					}else{
						sbTxtFormat.append(comilla+CommonsUtilities.formatDecimal(pdc.getInitialNominalValue(), ReportConstant.FORMAT_ONLY_TWO_DECIMAL)+comilla);
					}
					sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
					sbTxtFormat.append(comilla+CommonsUtilities.formatDecimal(pdc.getNominalValueOrig(), ReportConstant.FORMAT_ONLY_TWO_DECIMAL)+comilla);
					sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
					//moneda original del valor
					BigDecimal bdMarketPriceCurrencyOrig= new BigDecimal(CommonsUtilities.formatDecimal(pdc.getMarketPrice(), ReportConstant.FORMAT_ONLY_TWO_DECIMAL));
					sbTxtFormat.append(comilla+bdMarketPriceCurrencyOrig+comilla);
					sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
					//Bolivianos
					BigDecimal bdMarketPriceOrig= new BigDecimal(CommonsUtilities.formatDecimal(pdc.getMarkPriceOrig(), ReportConstant.FORMAT_ONLY_TWO_DECIMAL));
					sbTxtFormat.append(comilla+bdMarketPriceOrig.toString()+comilla);
					sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
					sbTxtFormat.append(comilla+pdc.getTotalBalance()+comilla);
					sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
					sbTxtFormat.append(comilla+CommonsUtilities.formatDecimal(bdMarketPriceCurrencyOrig.multiply(pdc.getTotalBalance()), ReportConstant.FORMAT_ONLY_TWO_DECIMAL)+comilla);
					sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
					if(FCC.equals(pdc.getFundType())){ // || FCI.equals(pdc.getFundType())
						sbTxtFormat.append(comilla+pdc.getSecCodeOnCustody()+comilla);
					}else{
						if(pdc.getSecCodeOnCustody().equals("D")){
							sbTxtFormat.append(comilla+"DES"+comilla);
						}else{
							sbTxtFormat.append(comilla+"FIS"+comilla);
						}
					}
					if(FRD.equals(pdc.getFundType())){
						sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
						sbTxtFormat.append(comilla+pdc.getIndReporto()+comilla);
					}
					sbTxtFormat.append(GeneralConstants.STR_END_LINE_TXT);
				}
				//get name according to fund
				PortafolioDetailCustodianTO custody=null;
				if(lstPDC!=null && lstPDC.size()>0){
					custody =lstPDC.get(0);
				}else{
					sbTxtFormat.append(PropertiesConstants.NO_FOUND_DATA);
				}
				String fileName= getNameAccordingFund(custody);
				reportUtilServiceBean.saveCustomReportFile(fileName, ReportFormatType.TXT.getCode(), sbTxtFormat.toString().getBytes(), getReportLogger());
			}
		} catch (ServiceException e) {
			if(e.getErrorService().equals(ErrorServiceType.NOT_EXCHANGE_RATE)){
				if(exchange_rate_dollar==null){
					throw new RuntimeErrorException(null, "No existe tipo  de cambio en Dolares para la fecha "+ CommonsUtilities.convertDatetoString(pdcTO.getDateMarketFact()));
				}else{
					throw new RuntimeErrorException(null, "No existe tipo de cambio en UFV para la fecha "+ CommonsUtilities.convertDatetoString(pdcTO.getDateMarketFact()));
				}
			}else{
				e.printStackTrace();
			}
			log.error(e.getMessage());
		}
		
		return parametersRequired;
	}
	
	private List<String> fillFunds(){
		List<String> lstFunds= new ArrayList<String>();
		
		lstFunds.add(FCC);
		lstFunds.add(FCI);
		lstFunds.add(FRD);

		return lstFunds;
	}
	
	private String getNameAccordingFund(PortafolioDetailCustodianTO custody){
		String name="";
		if(Validations.validateIsNotNullAndNotEmpty(custody)){
			String idFCIandFRD="83";
			String futuro_bolivia="01";
			String prevision="02";
			String la_vitalicia_bisa="03";
			String custody_code="CC";
			String strDate= CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate());
			if(!strDate.equals(CommonsUtilities.convertDatetoString(pdcTO.getDateMarketFact()))){
				strDate=CommonsUtilities.convertDatetoString(pdcTO.getDateMarketFact());
			}
			String strDay = strDate.substring(0,2);
			String strMonth = strDate.substring(3,5);
			String strYear = strDate.substring(6,10);
			//N° que identifica al custodio
			if(FCI.equals(custody.getFundType()) || FRD.equals(custody.getFundType())){
				name = idFCIandFRD;
			}else if(custody.getTransferNumberString()!=null){
				if(FCC.equals(custody.getFundType()))
					name = custody.getTransferNumberString();
				else
					name = custody.getTransferNumber().toString();
			}else{
				if(custody.getHolderDesc().toUpperCase().contains(AFP_FUTURO) && FCC.equals(custody.getFundType())){
					name+=futuro_bolivia;
				}else if(custody.getHolderDesc().toUpperCase().contains(AFP_PREVISION) && FCC.equals(custody.getFundType())){
					name+=prevision;
				}
			}
			//caracter que identifica al tipo de documento enviado
			if(EconomicActivityType.ADMINISTRADORAS_FONDOS_PENSIONES.getCode().equals(custody.getEconomicActivity())){
				if(FCC.equals(custody.getFundType()))
					name+=custody_code;//+custody.getTransferNumber();
				else
					name+=custody_code+custody.getTransferNumberString();
			}
			
			//N° que identifica a la gestora(optional)
			if(Validations.validateIsNotNullAndNotEmpty(custody.getHolderDesc())){
				if(custody.getHolderDesc().toUpperCase().contains(AFP_FUTURO) && FCI.equals(custody.getFundType())){
					name+=futuro_bolivia;
				}else if(custody.getHolderDesc().toUpperCase().contains(AFP_PREVISION) && FCI.equals(custody.getFundType())){
					name+=prevision;
				}else if(custody.getHolderDesc().toUpperCase().contains(BISA_SAFI) && (FRD.equals(custody.getFundType()) || "FRUV".equals(custody.getFundType()))){
					name+=la_vitalicia_bisa;
				}
			}
			//añoMesDia
			name+=strYear+strMonth+strDay;
			//codigo que identifiaca al fondo y/o aseguradora
			if(FCC.equals(custody.getFundType())){
				name+=GeneralConstants.DOT+FCC;
			}else if(FCI.equals(custody.getFundType())){
				name+=GeneralConstants.DOT+FCI;
			}else if(FRD.equals(custody.getFundType())){
				name+=GeneralConstants.DOT+FRD;
			}else{
				name+=GeneralConstants.DOT+custody_code;
			}
		}else{
			name =getReportLogger().getPhysicalName();
		}
		
		return name;
	}
}
