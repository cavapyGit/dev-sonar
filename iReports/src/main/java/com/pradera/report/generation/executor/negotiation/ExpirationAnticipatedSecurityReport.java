package com.pradera.report.generation.executor.negotiation;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.model.report.ReportLogger;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;

@ReportProcess(name = "ExpirationAnticipatedSecurityReport")
public class ExpirationAnticipatedSecurityReport extends GenericReport{
	
	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;
	@Inject
	PraderaLogger log;
	
	@Inject 
	@Configurable 
	String idIssuerBC;
	
	@Inject 
	@Configurable 
	String idIssuerTGN;

	public ExpirationAnticipatedSecurityReport() {
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParametersQueryReport() {

	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String,Object> parametersRequired = new HashMap<>();
		
		parametersRequired.put("idIssuerBC", idIssuerBC);
		parametersRequired.put("idIssuerTGN", idIssuerTGN);
		
		return parametersRequired;
	}
}
