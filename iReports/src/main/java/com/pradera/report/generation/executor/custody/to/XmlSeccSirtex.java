package com.pradera.report.generation.executor.custody.to;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


@XmlAccessorType(XmlAccessType.FIELD) 
public class XmlSeccSirtex implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3056439695792979072L;

	@XmlElement(name = "NRO_OPER_SRX")
	private String numberOperationSirtex;
	
	@XmlElement(name = "FECHA_OPER_SRX")
	private String dateOperationSirtex;
	
	@XmlElement(name = "COD_PART_VENDEDOR")
	private String sellerParticipantCode;
	
	@XmlElement(name = "CLAVE_VALOR")
	private String securityKey;
	
	@XmlElement(name = "FECHA_EMISION")
	private String issuanceDate;
	
	@XmlElement(name = "FECHA_VENCI")
	private String expirationDate;
	
	@XmlElement(name = "MODALIDAD")
	private String modality;
	
	@XmlElement(name = "TASA")
	private String rate;
	
	@XmlElement(name = "PLAZO")
	private String term;
	
	@XmlElement(name = "CANT_OFERTADA")
	private String quantityOffere;
	
	@XmlElement(name = "CANT_ACEPTADA")
	private String acceptedAmount;

	@XmlElement(name = "SERIE_F_BCB")
	private String serieF;
	
	@XmlElement(name = "VALOR_INICIAL")
	private String initialNominalValue;
	
	@XmlElement(name = "VALOR_FINAL")
	private String finalNominalValue;
	
	@XmlElement(name = "PLAZO_RESTANTE")
	private String remainingTerm;
	
	@XmlElement(name = "SECC_CUPON")
	private XmlListSeccSirtex listCoupons;
	
	private BigDecimal stockQuantity;
	
	private String exchangeAgency;
	
	private String client;

	public String getNumberOperationSirtex() {
		return numberOperationSirtex;
	}

	public void setNumberOperationSirtex(String numberOperationSirtex) {
		this.numberOperationSirtex = numberOperationSirtex;
	}

	public String getDateOperationSirtex() {
		return dateOperationSirtex;
	}

	public void setDateOperationSirtex(String dateOperationSirtex) {
		this.dateOperationSirtex = dateOperationSirtex;
	}

	public String getSellerParticipantCode() {
		return sellerParticipantCode;
	}

	public void setSellerParticipantCode(String sellerParticipantCode) {
		this.sellerParticipantCode = sellerParticipantCode;
	}

	public String getSecurityKey() {
		return securityKey;
	}

	public void setSecurityKey(String securityKey) {
		this.securityKey = securityKey;
	}

	public String getIssuanceDate() {
		return issuanceDate;
	}

	public void setIssuanceDate(String issuanceDate) {
		this.issuanceDate = issuanceDate;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getModality() {
		return modality;
	}

	public void setModality(String modality) {
		this.modality = modality;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public String getQuantityOffere() {
		return quantityOffere;
	}

	public void setQuantityOffere(String quantityOffere) {
		this.quantityOffere = quantityOffere;
	}

	public String getAcceptedAmount() {
		return acceptedAmount;
	}

	public void setAcceptedAmount(String acceptedAmount) {
		this.acceptedAmount = acceptedAmount;
	}

	public String getSerieF() {
		return serieF;
	}

	public void setSerieF(String serieF) {
		this.serieF = serieF;
	}

	public String getInitialNominalValue() {
		return initialNominalValue;
	}

	public void setInitialNominalValue(String initialNominalValue) {
		this.initialNominalValue = initialNominalValue;
	}

	public String getFinalNominalValue() {
		return finalNominalValue;
	}

	public void setFinalNominalValue(String finalNominalValue) {
		this.finalNominalValue = finalNominalValue;
	}

	public String getRemainingTerm() {
		return remainingTerm;
	}

	public void setRemainingTerm(String remainingTerm) {
		this.remainingTerm = remainingTerm;
	}

	public XmlListSeccSirtex getListCoupons() {
		return listCoupons;
	}

	public void setListCoupons(XmlListSeccSirtex listCoupons) {
		this.listCoupons = listCoupons;
	}

	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}

	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	public String getExchangeAgency() {
		return exchangeAgency;
	}

	public void setExchangeAgency(String exchangeAgency) {
		this.exchangeAgency = exchangeAgency;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}
}
