package com.pradera.report.generation.executor.securities;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.billing.type.EntityCollectionType;
import com.pradera.model.custody.type.DematerializationCertificateMotiveType;
import com.pradera.model.custody.type.DematerializationStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.collectioneconomic.service.ParserUtilServiceReport;
import com.pradera.report.generation.executor.securities.service.PaymentChronogramBySecurity;
import com.pradera.report.generation.executor.securities.to.SecuritiesByIssuanceAndIssuerTO;
import com.pradera.report.generation.service.ReportUtilServiceBean;
import com.pradera.report.util.view.PropertiesConstants;

@ReportProcess(name = "SecurityCharacteristicReport")
public class SecurityCharacteristicReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private PaymentChronogramBySecurity securitiesService;
	
	@Inject
	private PraderaLogger log;
	@EJB
	private ParameterServiceBean parameterService;
	@EJB
	private ReportUtilServiceBean reportUtilServiceBean;
	
	@Inject
	private ParserUtilServiceReport parserUtilServiceReport;
	
	@EJB 
	private BatchProcessServiceFacade batchProcessServiceFacade;

	
	private SecuritiesByIssuanceAndIssuerTO charactsecTO = new SecuritiesByIssuanceAndIssuerTO();
	
	/*private static final String[] strDays = new String[]{
		"Domingo",
		"Lunes",
		"Martes",
		"Miercoles",
		"Jueves",
		"Viernes",
		"Sabado"};
	*/
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		charactsecTO.setIdReport(getReportLogger().getReport().getIdReportPk());
		charactsecTO.setMotive(DematerializationCertificateMotiveType.DEMATERIALIZATION_OTC.getCode());
		charactsecTO.setStateAnnotation(DematerializationStateType.CONFIRMED.getCode());
		
		boolean automatic = false;
		boolean logic = true;
		Long idReportLoggerDetailDate = null;
		
		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("date")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					charactsecTO.setDate(CommonsUtilities.convertStringtoDate(listaLogger.get(i).getFilterValue()));
					idReportLoggerDetailDate  = listaLogger.get(i).getIdLoggerDetailPk();
					charactsecTO.setRegistryDt(CommonsUtilities.convertStringtoDate(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("logic")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					if(listaLogger.get(i).getFilterValue().equals("true"))
						logic = true;
					else
						logic = false;
				}
			}
			if(listaLogger.get(i).getFilterName().equals("automatic")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					if(listaLogger.get(i).getFilterValue().equals("true"))
						automatic = true;
				}
			}
		}
		/***Verificamos si el batchero se ayer se ejecuto */
		/***En caso de que no se ejecuto lo ejecuta para poblar datos*/
		if(automatic==false){
			Date beforeDate = CommonsUtilities.addDate(CommonsUtilities.currentDate(), -1);
			boolean existReport = parserUtilServiceReport.verifyReportsAsfi(beforeDate,77L);
			if(!existReport){
				Map<String,Object> parameters = new HashMap<String, Object>();	
				parameters.put("cutDate", CommonsUtilities.convertDatetoString(beforeDate));
				
				BusinessProcess businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(40741L);//Agregar a bussines process type
				try {
					batchProcessServiceFacade.registerBatch(EntityCollectionType.MANAGER.getValue(),businessProcess,parameters);
				} catch (ServiceException e) {
					e.printStackTrace();
				}
			}
		}
		
		/**Proceso enviado por el batchero, se ejecuta como estaba orgiginalmente*/
		if(automatic){
			/**Proceso normal y automatico*/
			List<Integer> lstSecurityClass= new ArrayList<Integer>();
			lstSecurityClass.add(SecurityClassType.BTS.getCode());
			lstSecurityClass.add(SecurityClassType.CDS.getCode());
			lstSecurityClass.add(SecurityClassType.LTS.getCode());
			lstSecurityClass.add(SecurityClassType.DPF.getCode());//-
			lstSecurityClass.add(SecurityClassType.CUP.getCode());//-
			lstSecurityClass.add(SecurityClassType.BBS.getCode());
			lstSecurityClass.add(SecurityClassType.LBS.getCode());
			lstSecurityClass.add(SecurityClassType.PGS.getCode());//-
			lstSecurityClass.add(SecurityClassType.DPA.getCode());//-
			lstSecurityClass.add(SecurityClassType.LRS.getCode());//-
			lstSecurityClass.add(SecurityClassType.BRS.getCode());//-
			
			charactsecTO.setLstSecurityClass(lstSecurityClass);
			charactsecTO.setDateForName(charactsecTO.getRegistryDt());
			charactsecTO.setDateInformation(charactsecTO.getRegistryDt());
			List<SecuritiesByIssuanceAndIssuerTO> lstSecurities = securitiesService.getSecurityCharacteristics(charactsecTO);
			buildReportSecurityCharacteristics(lstSecurities,charactsecTO.getDateForName());
			return null;
		}
		if(!logic){
			/**Proceso normal y automatico*/
			List<Integer> lstSecurityClass= new ArrayList<Integer>();
			lstSecurityClass.add(SecurityClassType.BTS.getCode());
			lstSecurityClass.add(SecurityClassType.CDS.getCode());
			lstSecurityClass.add(SecurityClassType.LTS.getCode());
			lstSecurityClass.add(SecurityClassType.DPF.getCode());//-
			lstSecurityClass.add(SecurityClassType.CUP.getCode());//-
			lstSecurityClass.add(SecurityClassType.BBS.getCode());
			lstSecurityClass.add(SecurityClassType.LBS.getCode());
			lstSecurityClass.add(SecurityClassType.PGS.getCode());//-
			lstSecurityClass.add(SecurityClassType.DPA.getCode());//-
			lstSecurityClass.add(SecurityClassType.LRS.getCode());//-
			lstSecurityClass.add(SecurityClassType.BRS.getCode());//-
			
			charactsecTO.setLstSecurityClass(lstSecurityClass);
			charactsecTO.setDateForName(charactsecTO.getRegistryDt());
			charactsecTO.setDateInformation(charactsecTO.getRegistryDt());
			List<SecuritiesByIssuanceAndIssuerTO> lstSecurities = parserUtilServiceReport.getSecurityCharacteristicsFromFile(charactsecTO,77L);
			buildReportSecurityCharacteristics(lstSecurities,charactsecTO.getDateForName());
			return null;
		}
		List<Integer> lstSecurityClass = null;
		List<SecuritiesByIssuanceAndIssuerTO> lstSecurities = new ArrayList<>();
		/**Logica de fechas*/
		Calendar now = Calendar.getInstance();
		now.setTime(charactsecTO.getRegistryDt());
		//System.out.println("Hoy es : " + strDays[now.get(Calendar.DAY_OF_WEEK) - 1]);
		if(now.get(Calendar.DAY_OF_WEEK)==3||now.get(Calendar.DAY_OF_WEEK)==4||now.get(Calendar.DAY_OF_WEEK)==2){
			if(now.get(Calendar.DAY_OF_WEEK)==3){
				/**Dia martes que debe obetener del dia lunes*/
				lstSecurityClass = new ArrayList<>();
				//lstSecurityClass.add(SecurityClassType.BTS.getCode());
				lstSecurityClass.add(SecurityClassType.CDS.getCode());
				lstSecurityClass.add(SecurityClassType.LTS.getCode());
				lstSecurityClass.add(SecurityClassType.DPF.getCode());
				lstSecurityClass.add(SecurityClassType.CUP.getCode());
				lstSecurityClass.add(SecurityClassType.BBS.getCode());
				lstSecurityClass.add(SecurityClassType.LBS.getCode());
				lstSecurityClass.add(SecurityClassType.PGS.getCode());
				lstSecurityClass.add(SecurityClassType.DPA.getCode());
				lstSecurityClass.add(SecurityClassType.LRS.getCode());
				lstSecurityClass.add(SecurityClassType.BRS.getCode());
				charactsecTO.setLstSecurityClass(lstSecurityClass);
				List<SecuritiesByIssuanceAndIssuerTO> lstMonday = new ArrayList<>();
				Date monday  = CommonsUtilities.addDate(charactsecTO.getRegistryDt(),-1);
				charactsecTO.setDateForName(monday);
				charactsecTO.setDate(monday);
				/**Obtiene datos desde report_logger_file*/
				lstMonday = parserUtilServiceReport.getSecurityCharacteristicsFromFile(charactsecTO,77L);
				/**Dia sabado*/
				List<SecuritiesByIssuanceAndIssuerTO> lstSaturday = new ArrayList<>();
				Date saturday  = CommonsUtilities.addDate(charactsecTO.getRegistryDt(),-3);
				charactsecTO.setDate(saturday);
				lstSecurityClass.add(SecurityClassType.BTS.getCode());
				lstSaturday = parserUtilServiceReport.getSecurityCharacteristicsFromFile(charactsecTO,77L);
				lstSaturday.addAll(lstMonday);
				/**Dias sabado y lunes en el orden respectivo*/
				lstSecurities.addAll(lstSaturday);
			}
			if(now.get(Calendar.DAY_OF_WEEK)==4){
				/**Dia martes que sera ejecutado el dia miercoles*/
				lstSecurityClass= new ArrayList<>();
				lstSecurityClass.add(SecurityClassType.BTS.getCode());
				charactsecTO.setLstSecurityClass(lstSecurityClass);
				
				Date tuesday  = CommonsUtilities.addDate(charactsecTO.getRegistryDt(), -1);
				charactsecTO.setDateForName(tuesday);
				/**Dia lunes con fecha de viernes*/
				Date friday  = CommonsUtilities.addDate(charactsecTO.getRegistryDt(), -5);
				charactsecTO.setDate(friday);
				List<SecuritiesByIssuanceAndIssuerTO> lstFriday = new ArrayList<>();
				lstFriday = parserUtilServiceReport.getSecurityCharacteristicsFromFile(charactsecTO,77L);
				
				Date monday = CommonsUtilities.addDate(charactsecTO.getRegistryDt(), -2);
				charactsecTO.setDate(monday);
				List<SecuritiesByIssuanceAndIssuerTO> lstMonday = new ArrayList<>();
				lstMonday = parserUtilServiceReport.getSecurityCharacteristicsFromFile(charactsecTO,77L);
								
				lstSecurityClass= new ArrayList<Integer>();
				lstSecurityClass.add(SecurityClassType.BTS.getCode());
				lstSecurityClass.add(SecurityClassType.CDS.getCode());
				lstSecurityClass.add(SecurityClassType.LTS.getCode());
				lstSecurityClass.add(SecurityClassType.DPF.getCode());
				lstSecurityClass.add(SecurityClassType.CUP.getCode());
				lstSecurityClass.add(SecurityClassType.BBS.getCode());
				lstSecurityClass.add(SecurityClassType.LBS.getCode());
				lstSecurityClass.add(SecurityClassType.PGS.getCode());
				lstSecurityClass.add(SecurityClassType.DPA.getCode());
				lstSecurityClass.add(SecurityClassType.LRS.getCode());
				lstSecurityClass.add(SecurityClassType.BRS.getCode());
				charactsecTO.setLstSecurityClass(lstSecurityClass);
				
				List<SecuritiesByIssuanceAndIssuerTO> lstTuesday = new ArrayList<>();
				charactsecTO.setDate(tuesday);
				charactsecTO.setDateForName(tuesday);
				lstTuesday = parserUtilServiceReport.getSecurityCharacteristicsFromFile(charactsecTO,77L);
				//orden primero BTS(vierne, lunes y martes)
				lstFriday.addAll(lstMonday);
				lstFriday.addAll(lstTuesday);
				lstSecurities.addAll(lstFriday);
			}
			if(now.get(Calendar.DAY_OF_WEEK)==2){
				/**Dia viernes que se ejecuta el lunes*/
				List<SecuritiesByIssuanceAndIssuerTO> lstFriday = new ArrayList<>();
				/**excluir BTS*/
				lstSecurityClass= new ArrayList<Integer>();
				lstSecurityClass.add(SecurityClassType.CDS.getCode());
				lstSecurityClass.add(SecurityClassType.LTS.getCode());
				lstSecurityClass.add(SecurityClassType.DPF.getCode());
				lstSecurityClass.add(SecurityClassType.CUP.getCode());
				lstSecurityClass.add(SecurityClassType.BBS.getCode());
				lstSecurityClass.add(SecurityClassType.LBS.getCode());
				lstSecurityClass.add(SecurityClassType.PGS.getCode());
				lstSecurityClass.add(SecurityClassType.DPA.getCode());
				lstSecurityClass.add(SecurityClassType.LRS.getCode());
				lstSecurityClass.add(SecurityClassType.BRS.getCode());
				charactsecTO.setLstSecurityClass(lstSecurityClass);
				Date friday  = CommonsUtilities.addDate(charactsecTO.getRegistryDt(),-3);
				charactsecTO.setDate(friday);
				charactsecTO.setDateForName(friday);
				lstFriday = parserUtilServiceReport.getSecurityCharacteristicsFromFile(charactsecTO,77L);
				lstSecurities.addAll(lstFriday);
			}
		}else{
			lstSecurityClass = new ArrayList<>();
			lstSecurityClass.add(SecurityClassType.BTS.getCode());
			lstSecurityClass.add(SecurityClassType.CDS.getCode());
			lstSecurityClass.add(SecurityClassType.LTS.getCode());
			lstSecurityClass.add(SecurityClassType.DPF.getCode());
			lstSecurityClass.add(SecurityClassType.CUP.getCode());
			lstSecurityClass.add(SecurityClassType.BBS.getCode());
			lstSecurityClass.add(SecurityClassType.LBS.getCode());
			lstSecurityClass.add(SecurityClassType.PGS.getCode());
			lstSecurityClass.add(SecurityClassType.DPA.getCode());
			lstSecurityClass.add(SecurityClassType.LRS.getCode());
			lstSecurityClass.add(SecurityClassType.BRS.getCode());
			charactsecTO.setLstSecurityClass(lstSecurityClass);
			charactsecTO.setDate(charactsecTO.getRegistryDt());
			charactsecTO.setDateForName(charactsecTO.getRegistryDt());
			lstSecurities = parserUtilServiceReport.getSecurityCharacteristicsFromFile(charactsecTO,77L);
			
			if(lstSecurities==null||lstSecurities.size()==0)
				lstSecurities = securitiesService.getSecurityCharacteristics(charactsecTO);
		}
		if(automatic==false&&logic==true){
		buildReportSecurityCharacteristics(lstSecurities,charactsecTO.getDateForName());
		ReportLoggerDetail detail = parameterService.find(ReportLoggerDetail.class, idReportLoggerDetailDate);
		detail.setFilterValue(CommonsUtilities.convertDateToString(charactsecTO.getDateForName(), "dd/MM/yyyy"));
		try {
			parserUtilServiceReport.updateWithOutFlush(detail);
		} catch (ServiceException e) {
			e.printStackTrace();
		}}
		return null;
	}
	
	public String getNameAccordingFund(){
		String name="";
		String custody_code="EDB";
		String firstWord="M";
		String lastWord="B";
		String strDate= CommonsUtilities.convertDatetoString(charactsecTO.getDateForName());//date information
		String strDay = strDate.substring(0,2);
		String strMonth = strDate.substring(3,5);
		String strYear = strDate.substring(8,10);

		name=firstWord;
		//aniMesDia
		name+=strYear+strMonth+strDay;
		name+=lastWord;
		name+=GeneralConstants.DOT+custody_code;
		
		return name;
	}
	
	/**
	 * Construimos el reporte*/
	private void buildReportSecurityCharacteristics(List<SecuritiesByIssuanceAndIssuerTO> lstSecurities,Date dateForName){
		StringBuilder sbTxtFormat = new StringBuilder();
		String comilla= "\"";

		//Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> security_class = new HashMap<Integer, String>();
		Map<Integer,String> currency = new HashMap<Integer, String>();
		Map<Integer,String> mnemonic = new HashMap<Integer, String>();
		Map<Integer,String> motiveDesm = new HashMap<Integer, String>();
		try {
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				security_class.put(param.getParameterTablePk(), param.getParameterName());
				mnemonic.put(param.getParameterTablePk(), param.getText1());
			}
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				currency.put(param.getParameterTablePk(), param.getParameterName());
			}
			filter.setMasterTableFk(MasterTableType.DEMATERIALIZATION_MOTIVE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				motiveDesm.put(param.getParameterTablePk(), param.getParameterName());
			}
		
			for (SecuritiesByIssuanceAndIssuerTO cc: lstSecurities) {
				if(cc.getSecurityClass()!=null){
					sbTxtFormat.append(comilla+mnemonic.get(cc.getSecurityClass())+comilla);
				}else{
					sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
				}
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				if(cc.getIdSecurityOnly()!=null){
					sbTxtFormat.append(comilla+cc.getIdSecurityOnly()+comilla);
				}else{
					sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
				}
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				if(cc.getMnemonic()!=null){
					sbTxtFormat.append(comilla+cc.getMnemonic()+comilla);
				}else{
					sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
				}
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				if(cc.getCustodian()!=null){
					sbTxtFormat.append(comilla+cc.getCustodian()+comilla);
				}else{
					sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
				}
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				if(dateForName!=null){
					sbTxtFormat.append(comilla+CommonsUtilities.convertDateToString(dateForName, GeneralConstants.DATE_FORMAT_PATTERN_TXT)+comilla);
				}else{
					sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
				}
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				if(cc.getRegistryDt()!=null){
					sbTxtFormat.append(comilla+CommonsUtilities.convertDateToString(cc.getDateInformation(), GeneralConstants.DATE_FORMAT_PATTERN_TXT)+comilla);
				}else{
					sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
				}
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				if(cc.getParticipantPlacement()!=null){
					if(cc.getParticipantPlacement().length()>1)
						sbTxtFormat.append(comilla+cc.getParticipantPlacement()+comilla);
					else{
						/*Solo PGS*/
						String part = parserUtilServiceReport.searchParticipant("PGS-"+cc.getIdSecurityOnly());
						sbTxtFormat.append(comilla+part+comilla);
					}
				}else{
					sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
				}
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				//MONEDA
				if(cc.getCurrency()!=null){
					String currencyCode="";
					if(cc.getCurrency().equals(CurrencyType.PYG.getCode())){
						currencyCode="10";//CurrencyType.PYG.getCodeAsfi().toString();//"10";
					}else if(cc.getCurrency().equals(CurrencyType.DMV.getCode())){
						currencyCode="11";//CurrencyType.DMV.getCodeAsfi().toString();//"11";
					}else if(cc.getCurrency().equals(CurrencyType.USD.getCode())){
						currencyCode="12";//CurrencyType.USD.getCodeAsfi().toString();//"12";
					}else if(cc.getCurrency().equals(CurrencyType.UFV.getCode())){ 
						currencyCode="14";//CurrencyType.UFV.getCodeAsfi().toString();//"14";
					}else if(cc.getCurrency().equals(CurrencyType.EU.getCode())){
						currencyCode="15";//CurrencyType.EU.getCodeAsfi().toString();//"15";
					}
					sbTxtFormat.append(comilla+currencyCode+comilla);
				}else{
					sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
				}
				//VALOR INICIAL
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				if(cc.getIninomval()!=null){
					sbTxtFormat.append(comilla+CommonsUtilities.formatDecimal(cc.getIninomval(), ReportConstant.FORMAT_ONLY_TWO_DECIMAL)+comilla);
				}else{
					sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
				}
				//FCHA DE EMISION 
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				if(cc.getIssuancedate()!=null){
					sbTxtFormat.append(comilla+CommonsUtilities.convertDateToString(cc.getIssuancedate(), GeneralConstants.DATE_FORMAT_PATTERN_TXT)+comilla);
				}else{
					sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
				}
				//FECHA DE EMISION ENTERA
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				if(cc.getExpirationDtInt()!=null){
					sbTxtFormat.append(comilla+CommonsUtilities.convertDateToString(cc.getExpirationDtInt(), GeneralConstants.DATE_FORMAT_PATTERN_TXT)+comilla);
				}else{
					sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
				}
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				//MONTO DE LA EMISION
				if(cc.getIssuanceAmount()!=null){
					sbTxtFormat.append(comilla+CommonsUtilities.formatDecimal(cc.getIssuanceAmount(), ReportConstant.FORMAT_ONLY_TWO_DECIMAL)+comilla);
				}else{
					sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
				}
				//MAXIMO
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				if(cc.getMaximumInvestment()!=null){
					sbTxtFormat.append(comilla+CommonsUtilities.formatDecimal(cc.getMaximumInvestment(), ReportConstant.FORMAT_ONLY_TWO_DECIMAL)+comilla);
				}else{
					sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
				}
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				//--Valores desmaterializados
				//En dpf siempre es uno, preguntar a Rebeca si lo vamos a cambiar a 1 por mas de que el valor ya se haya ya redimido
				if(cc.getShareBalance()!=null){
					if(cc.getShareBalance().intValue()==0)
						sbTxtFormat.append(comilla+parserUtilServiceReport.dematerializedSecurities("PGS-"+cc.getIdSecurityOnly()));
					else
						sbTxtFormat.append(comilla+cc.getShareBalance()+comilla);
				}else{
						sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
				}
				//TASA
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				if(cc.getMarketrate()!=null){
					sbTxtFormat.append(comilla+CommonsUtilities.formatDecimal(cc.getMarketrate(), ReportConstant.FORMAT_ONLY_FOUR_DECIMALS)+comilla);
				}else{
					sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
				}
				//
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				if(cc.getDiscountrate()!=null){
					sbTxtFormat.append(comilla+CommonsUtilities.formatDecimal(cc.getDiscountrate(), ReportConstant.FORMAT_ONLY_FOUR_DECIMALS)+comilla);
				}else{
					sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
				}
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				if(cc.getCouponNumberInt()!=null){
					sbTxtFormat.append(comilla+cc.getCouponNumberInt()+comilla);
				}else{
					sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
				}
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				if(cc.getAmortizationType()!=null){
					sbTxtFormat.append(comilla+cc.getAmortizationType()+comilla);
				}else{
					sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
				}
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				if(cc.getPeriodicity()!=null){
					sbTxtFormat.append(comilla+cc.getPeriodicity()+comilla);
				}else{
					sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla); 
				}
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				if(cc.getNegotiatingmanner()!=null){
					sbTxtFormat.append(comilla+cc.getNegotiatingmanner()+comilla); 
//					sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla); 
				}else{
					sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
				}
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				if(cc.getMarketmoney()!=null){  
					sbTxtFormat.append(comilla+cc.getMarketmoney()+comilla);
				}else{
					sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
				}
				sbTxtFormat.append(GeneralConstants.STR_END_LINE_TXT); 
			}
			if(sbTxtFormat.toString().isEmpty())
				sbTxtFormat.append(PropertiesConstants.NO_FOUND_DATA_BLANK);
			String physicalName = null;
			physicalName= getNameAccordingFund();
			reportUtilServiceBean.saveCustomReportFile(physicalName, ReportFormatType.TXT.getCode(), sbTxtFormat.toString().getBytes(), getReportLogger());
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
	}
}
