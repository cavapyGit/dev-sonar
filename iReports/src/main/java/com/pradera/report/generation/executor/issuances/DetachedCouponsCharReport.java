package com.pradera.report.generation.executor.issuances;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.collectioneconomic.service.ParserUtilServiceReport;
import com.pradera.report.generation.executor.issuances.to.PaymentChronogramTO;
import com.pradera.report.generation.executor.securities.service.PaymentChronogramBySecurity;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;
import com.pradera.report.generation.service.ReportUtilServiceBean;
import com.pradera.report.util.view.PropertiesConstants;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "DetachedCouponsCharReport")
public class DetachedCouponsCharReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private PaymentChronogramBySecurity securitiesService;
	@Inject
	private PraderaLogger log;
	@EJB
	private ParameterServiceBean parameterService;
	@EJB
	private ReportUtilServiceBean reportUtilServiceBean;
	
	@Inject
	private ParserUtilServiceReport parserUtilServiceReport;
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		reportLogger.getReport().setTitle("Hola");
		reportLogger.getReport().setName("SUBPRODUCTO");
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		GenericsFiltersTO filtersTO = new GenericsFiltersTO();
		boolean automatic = false;
		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("registry_date")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					filtersTO.setDate(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("automatic")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					if(listaLogger.get(i).getFilterValue().equals("true"))
						automatic = true;
				}
			}
		}
	
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> secClassMnemonic = new HashMap<Integer, String>();
		try {
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secClassMnemonic.put(param.getParameterTablePk(), param.getText1());
			}
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		if(listaLogger.get(0).getReportLogger().getReportFormat().equals(ReportFormatType.TXT.getCode())){
			List<PaymentChronogramTO> lstCoupons;
			String date =  filtersTO.getDate();//CommonsUtilities.convertDateToString(CommonsUtilities.currentDate(),"dd/MM/yyyy");
			if(automatic){
				filtersTO.setDate(date);
				lstCoupons= securitiesService.getDetachedCouponsCharact(filtersTO);
			}
			else{
				filtersTO.setDate(date);
				lstCoupons= parserUtilServiceReport.getDetachedCouponsCharactFromFile(filtersTO);
			}
				
			StringBuilder sbTxtFormat = new StringBuilder();
			String comilla= "\"";
			
			for (PaymentChronogramTO cc: lstCoupons) {
				sbTxtFormat.append(comilla+cc.getCustodian()+comilla);
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				sbTxtFormat.append(comilla+CommonsUtilities.convertDateToString(cc.getRegistryDt(), GeneralConstants.DATE_FORMAT_PATTERN_TXT)+comilla);
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				sbTxtFormat.append(comilla+secClassMnemonic.get(cc.getSecurityClass())+comilla);
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				sbTxtFormat.append(comilla+cc.getIdSecurityOnly()+comilla);
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				if(cc.getStrCouponNumbers().equals("[]")){
					sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
				}else{
					String strCouponsDetached=cc.getStrCouponNumbers().replace("[", GeneralConstants.EMPTY_STRING);
					String strDetachedCoupons=strCouponsDetached.replace("]", GeneralConstants.EMPTY_STRING);
					sbTxtFormat.append(comilla+strDetachedCoupons.substring(0, strDetachedCoupons.length()-1)+comilla);
				}
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				if(cc.getValidCoupon().equals("[]")){
					sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
				}else{
					String strCouponsIndexed=cc.getValidCoupon().replace("[", GeneralConstants.EMPTY_STRING);
					String strIndexedCoupons=strCouponsIndexed.replace("]", GeneralConstants.EMPTY_STRING);
					sbTxtFormat.append(comilla+strIndexedCoupons.substring(0, strIndexedCoupons.length()-1)+comilla);
				}
				sbTxtFormat.append(GeneralConstants.STR_END_LINE_TXT);
			}
			if(Validations.validateIsNotNull(lstCoupons) && lstCoupons.size()==0){
				getReportLogger().setReportFormat(ReportFormatType.TXT.getCode());
				sbTxtFormat.append(PropertiesConstants.NO_FOUND_DATA_BLANK);
			}
			String strDate=filtersTO.getDate();
			String strYear= strDate.substring(8, strDate.length());
			String strMonth= strDate.substring(3, 5);
			String strDay= strDate.substring(0, 2);
			getReportLogger().setPhysicalName("M"+strYear+strMonth+strDay+"AF.EDB");
			reportUtilServiceBean.saveCustomReportFile(getReportLogger().getPhysicalName(), ReportFormatType.TXT.getCode(), 
														sbTxtFormat.toString().getBytes(), getReportLogger());
		}
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());

		return parametersRequired;
	}

}
