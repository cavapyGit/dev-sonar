package com.pradera.report.generation.executor.corporative;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.corporateevents.facade.CoporateEventComponentFacade;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.corporative.service.CorporativeReportServiceBean;
import com.pradera.report.generation.executor.corporative.to.EvidenceOfPerceivedIncomeTO;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;


@ReportProcess(name="EvidenceOfEarnedIncomeReport")
public class EvidenceOfEarnedIncomeReport extends GenericReport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Inject
	PraderaLogger log;
	@EJB
	private CorporativeReportServiceBean serviceBean;
	@EJB
	private ParameterServiceBean parameterServiceBean;
	@EJB
	private CoporateEventComponentFacade componentFacade;
	
	private final static int CUENTA_VAL		 		= 0;
	private final static int TITULAR				= 1;
	private final static int EMISION				= 2;
	private final static int VALOR					= 3;	
	private final static int VENCIMIENTO			= 4;
	private final static int VALOR_NOMINAL			= 5;
	private final static int MONTO_INVERSION		= 6;
	private final static int TOTAL_BALANCE			= 7;
	private final static int PARTICIPANTE			= 8;
	private final static int INTERESES_PAGADOS		= 9;
	private final static int TIPO_DOCUMENTO			= 10;
	private final static int DOCUMENT_NUMBER		= 11;	
	private final static int BALANCE_BLOQUEADO		= 12;	
	private final static int EMISOR					= 13;
	private final static int INTERES				= 14;
	private final static int ID_HOLDER_ACCOUNT		= 15;
	private final static int CODIGO_ISIN			= 16;
	private final static int ISSUANCE_DATE			= 17;
	private final static int TIPO_INSTRUMENTO		= 18;
		
	private EvidenceOfPerceivedIncomeTO evidenceOfPerceivedIncomeTO;
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		evidenceOfPerceivedIncomeTO = this.reportLoggerFilters(reportLogger);
		
		try{
			//GETTING ALL DATA FROM QUERY
			List<Object[]> listObjects = serviceBean.getEvidenceOfEarnedIncome(evidenceOfPerceivedIncomeTO);
			
			XMLOutputFactory xof = XMLOutputFactory.newInstance();
			XMLStreamWriter xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(baos));
			//open document file
			xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");
			//root tag of report
			/**
			 * report
			 **/
			xmlsw.writeStartElement(ReportConstant.REPORT);
			/**
			 * Create to header report
			 * start_hour
			 * report_title
			 * mnemonic_report
			 * clasification_report
			 * generation_date
			 * user_name
			 * mnemonic_entity
			 * **/
			ParameterTableTO parameterTableTO = new ParameterTableTO();			
			parameterTableTO.setParameterTablePk(evidenceOfPerceivedIncomeTO.getSignType());
			
			List<ParameterTable> parameterTables = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);
			
			String initialDate = CommonsUtilities.convertDateToString(evidenceOfPerceivedIncomeTO.getInitialDate(), ReportConstant.DATE_FORMAT_STANDAR);
			String endDate = CommonsUtilities.convertDateToString(evidenceOfPerceivedIncomeTO.getEndDate(), ReportConstant.DATE_FORMAT_STANDAR);
			createTagString(xmlsw, "INTERESES_PAGADOS_LBL", "Intereses Pagados del " + initialDate + " al " + endDate );
			
			
			createTagString(xmlsw, "IND_COPROPIEDAD", 	parameterTables.get(0).getIndicator1());
			
			if(parameterTables.get(0).getIndicator1().equals(BooleanType.YES.getCode().toString())){
				createTagString(xmlsw, "NOMBRE_FIRMA", 	parameterTables.get(0).getDescription().toLowerCase());
				createTagString(xmlsw, "CARGO_FIRMA", 	parameterTables.get(0).getText2().toLowerCase());
				
				createTagString(xmlsw, "NOMBRE_FIRMA1", 	parameterTables.get(0).getText1().toLowerCase());
				createTagString(xmlsw, "CARGO_FIRMA2", 	parameterTables.get(0).getText3().toLowerCase());
			}else{
				createTagString(xmlsw, "NOMBRE_FIRMA", 	parameterTables.get(0).getParameterName().toLowerCase());
				createTagString(xmlsw, "CARGO_FIRMA", 	parameterTables.get(0).getObservation().toLowerCase());
			}
			createHeaderReport(xmlsw, reportLogger);
			
			if(Validations.validateListIsNotNullAndNotEmpty(listObjects)){
				createBodyReport(xmlsw, listObjects);
			}
			//END REPORT TAG
			xmlsw.writeEndElement();
			//close document
			xmlsw.writeEndDocument();
			xmlsw.flush();
	        xmlsw.close();
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		return baos;
	}
	
	private void createBodyReport(XMLStreamWriter xmlsw,List<Object[]> listObjects) throws XMLStreamException {
		xmlsw.writeStartElement("rows"); //START ROWS TAG
		
		//Getting dates
		String initialDay   = CommonsUtilities.convertDateToString(evidenceOfPerceivedIncomeTO.getInitialDate(),"dd");
		String initialMonth = CommonsUtilities.convertDateToString(evidenceOfPerceivedIncomeTO.getInitialDate(),"MM");
		String initialYear  = CommonsUtilities.convertDateToString(evidenceOfPerceivedIncomeTO.getInitialDate(),"yyyy");
		
		String initialDateStr = initialDay + " de " + getMonth(initialMonth) + " del año " + initialYear;
		
		//Getting dates
		String endDay   = CommonsUtilities.convertDateToString(evidenceOfPerceivedIncomeTO.getEndDate(),"dd");
		String endMonth = CommonsUtilities.convertDateToString(evidenceOfPerceivedIncomeTO.getEndDate(),"MM");
		String endYear  = CommonsUtilities.convertDateToString(evidenceOfPerceivedIncomeTO.getEndDate(),"yyyy");
		
		String endDateStr = endDay + " de " + getMonth(endMonth) + " del año " + endYear;
		
		createTagString(xmlsw, "initialDateStr", initialDateStr);
		createTagString(xmlsw, "endDateStr", endDateStr);
		
		
		//BY ROW
		for(Object[] object : listObjects){
			Integer instrumentType = Integer.valueOf(object[TIPO_INSTRUMENTO].toString());
			if(instrumentType.equals(InstrumentType.VARIABLE_INCOME.getCode()))
				continue;
			
			Double paidInterest 	= 0.00D;
			Double earnedInterest 	= 0.00D;
			if(Validations.validateIsNotNull(object[INTERESES_PAGADOS])){
				paidInterest = ((BigDecimal)object[INTERESES_PAGADOS]).doubleValue();
			}
			Long idHolderAccountPk = Long.valueOf((object[ID_HOLDER_ACCOUNT].toString()));
			String isinCode = object[CODIGO_ISIN].toString();
			//Getting Earned Interest
			earnedInterest = componentFacade.getEarnedIncome(idHolderAccountPk,isinCode, evidenceOfPerceivedIncomeTO.getEndDate()).doubleValue();
			
			xmlsw.writeStartElement("row"); //START ROW TAG
				createTagString(xmlsw, "PARTICIPANTE",   	object[PARTICIPANTE]);
				createTagString(xmlsw, "CUENTA_VAL", 	    object[CUENTA_VAL]);				
				createTagString(xmlsw, "TITULAR",		 	object[TITULAR]);
				createTagString(xmlsw, "EMISION", 			object[EMISION]);
				createTagString(xmlsw, "ISIN_CODE", 		object[CODIGO_ISIN]);
				createTagString(xmlsw, "VALOR", 			object[VALOR]);
				createTagString(xmlsw, "VENCIMIENTO",	 	object[VENCIMIENTO]);
				createTagString(xmlsw, "CANT_TITULOS", 		object[TOTAL_BALANCE]);	
				createTagString(xmlsw, "VALOR_NOMINAL",		object[VALOR_NOMINAL]);
				createTagString(xmlsw, "MONTO_INVERSION", 	object[MONTO_INVERSION]);
				createTagString(xmlsw, "INTERES",		 	object[INTERES]);
				createTagString(xmlsw, "EMISOR",		 	"VALORES EMITIDOS POR " + object[EMISOR]);
				createTagString(xmlsw, "BALANCE_BLOQUEADO",	object[BALANCE_BLOQUEADO]);
				createTagString(xmlsw, "FECHA_EMISION",		object[ISSUANCE_DATE]);
				createTagString(xmlsw, "INTERESES_PAGADOS", paidInterest);
				createTagString(xmlsw, "INTERESES_DEVENGADOS", earnedInterest);

				createTagString(xmlsw, "INTERESES_DEVENGADOS_LBL", "Monto devengado hasta " + 
																	CommonsUtilities.convertDateToString(evidenceOfPerceivedIncomeTO.getEndDate(), ReportConstant.DATE_FORMAT_STANDAR));
				createTagString(xmlsw, "BALANCE_BLOQUEADO_LBL", "Monto bloqueado hasta " + 
																CommonsUtilities.convertDateToString(evidenceOfPerceivedIncomeTO.getEndDate(), ReportConstant.DATE_FORMAT_STANDAR));
				
				createTagString(xmlsw, "BALANCE_INVERSION_LBL", "Monto inversi\u00f3n hasta " + 
																 CommonsUtilities.convertDateToString(evidenceOfPerceivedIncomeTO.getEndDate(), ReportConstant.DATE_FORMAT_STANDAR));

				StringBuilder header = new StringBuilder();
				header.append("CEVALDOM DEP\u00d3SITO CENTRALIZADO DE VALORES, S. A.,  sociedad comercial debidamente  organizada ");
				header.append("de conformidad con las leyes de la Rep\u00fablica Dominicana, con domicilio y asiento social en la ");
				header.append("avenida Gustavo Mej\u00eda Ricart No. 54,  Solazar Business Center, Piso 18, Ensanche Naco, Santo ");
				header.append("Domingo, CERTIFICA que " + object[TITULAR] + "(" + object[TIPO_DOCUMENTO] + " "+ object[DOCUMENT_NUMBER] + ") percibi\u00f3 ");
				header.append("ingresos por concepto de los intereses generados por las inversiones realizadas en valores ");
				header.append("anotados en cuentas registrados en CEVALDOM, durante el per\u00edodo " + initialDateStr);
				header.append(" hasta " + endDateStr + "." + "\t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t");
				createTagString(xmlsw, "CONTENT", header);	
				
			xmlsw.writeEndElement();
		}
		//Getting dates
		String currentDay   = CommonsUtilities.convertDateToString(CommonsUtilities.currentDate(),"dd");
		String currentMonth = CommonsUtilities.convertDateToString(CommonsUtilities.currentDate(),"MM");
		String currentYear  = CommonsUtilities.convertDateToString(CommonsUtilities.currentDate(),"yyyy");
		String currentDate = currentDay + " de " + getMonth(currentMonth) + " del año " + currentYear;
		
		String currentTime  = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),ReportConstant.DATE_FORMAT_TIME_SECOND_WITH_AM_OR_PM);
		
		String footer = "Dada en Santo Domingo, Rep\u00fablica Dominicana, a los " + currentDate + ", siendo las " + currentTime + ".";
		createTagString(xmlsw, "footer", footer);
		xmlsw.writeEndElement(); // END ROWS TAG		
	}
	
	public EvidenceOfPerceivedIncomeTO reportLoggerFilters(ReportLogger reportLogger){
		EvidenceOfPerceivedIncomeTO evidenceOfPerceivedIncomeTO = new EvidenceOfPerceivedIncomeTO();
		
		for (ReportLoggerDetail detail : reportLogger.getReportLoggerDetails()) {
			if( detail.getFilterName().equals(ReportConstant.INITIAL_DATE_PARAM) ){
				if(Validations.validateIsNotNull(detail.getFilterValue())){
					evidenceOfPerceivedIncomeTO.setInitialDate(CommonsUtilities.convertStringtoDate(detail.getFilterValue().toString()));
				}			
			}else if( detail.getFilterName().equals(ReportConstant.END_DATE_PARAM) ){
				if(Validations.validateIsNotNull(detail.getFilterValue())){
					evidenceOfPerceivedIncomeTO.setEndDate(CommonsUtilities.convertStringtoDate(detail.getFilterValue().toString()));
				}			
			}else if( detail.getFilterName().equals(ReportConstant.SIGN_TYPE)  ){
				if(Validations.validateIsNotNull(detail.getFilterValue())){
					evidenceOfPerceivedIncomeTO.setSignType(new Integer(detail.getFilterValue().toString()));
				}
			}else if( detail.getFilterName().equals(ReportConstant.PARTICIPANT_PARAM)  ){
				if(Validations.validateIsNotNull(detail.getFilterValue())){
					evidenceOfPerceivedIncomeTO.setIdParticipantPk(new Long(detail.getFilterValue().toString()));
				}
			}else if( detail.getFilterName().equals(ReportConstant.ACCOUNT_NUMBER_PARAM)  ){
				if(Validations.validateIsNotNull(detail.getFilterValue())){
					evidenceOfPerceivedIncomeTO.setIdHolderAccountPk(new Long(detail.getFilterValue().toString()));
				}
			}else if( detail.getFilterName().equals(ReportConstant.HOLDER_PARAM)  ){
				if(Validations.validateIsNotNull(detail.getFilterValue())){
					evidenceOfPerceivedIncomeTO.setIdHolderPk(new Long(detail.getFilterValue().toString()));
				}
			}else if( detail.getFilterName().equals(ReportConstant.ISSUER_PARAM)  ){
				if(Validations.validateIsNotNull(detail.getFilterValue())){
					evidenceOfPerceivedIncomeTO.setIdIssuanceCodePk((detail.getFilterValue().toString()));
				}
			}else if( detail.getFilterName().equals(ReportConstant.ISIN_CODE_PARAM)  ){
				if(Validations.validateIsNotNull(detail.getFilterValue())){
					evidenceOfPerceivedIncomeTO.setIdIsinCodePk((detail.getFilterValue().toString()));
				}
			}
		}
		return evidenceOfPerceivedIncomeTO;
		
	}
	public String getMonth(String month){
		switch(month){
		case "01": return "enero";
		case "02": return "febrero";
		case "03": return "marzo";
		case "04": return "abril";
		case "05": return "mayo";
		case "06": return "junio";
		case "07": return "julio";
		case "08": return "agosto";
		case "09": return "septiembre";
		case "10": return "octubre";
		case "11": return "noviembre";
		case "12": return "diciembre";
		default: return "";
		}
	}
}
