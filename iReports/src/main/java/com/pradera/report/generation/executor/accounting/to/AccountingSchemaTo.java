package com.pradera.report.generation.executor.accounting.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.core.component.helperui.to.AccountingAccountTo;
 


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingSchemaTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class AccountingSchemaTo implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3632201120647874452L;
	
	
	
	/** The id accounting schemaa pk. */
	private Long idAccountingSchemaaPk;
	
	/** The schema code. */
	private String schemaCode;
	
	/** The schema code consult. */
	private String schemaCodeConsult;
	
	/** The description. */
	private String description;
	
	/** The branch type. */
	private Integer branchType; 
	
	/** The status. */
	private Integer status;
	
	/** The status description. */
	private String statusDescription;
	
	/** The register date. */
	private String registerDate;
	
	/** The modify date. */
	private String modifyDate;
	
	/** The register user. */
	private String registerUser;
	
	/** The modify user. */
	private String modifyUser;
	
	/** The cancel user. */
	private String cancelUser;
	
	/** The cancel date. */
	private String cancelDate;
	
	/** The list accounting account. */
	private List<AccountingAccountTo> listAccountingAccount;
	
 
	
	/**
	 * Gets the id accounting schemaa pk.
	 *
	 * @return the id accounting schemaa pk
	 */
	public Long getIdAccountingSchemaaPk() {
		return idAccountingSchemaaPk;
	}
	
	/**
	 * Sets the id accounting schemaa pk.
	 *
	 * @param idAccountingSchemaaPk the new id accounting schemaa pk
	 */
	public void setIdAccountingSchemaaPk(Long idAccountingSchemaaPk) {
		this.idAccountingSchemaaPk = idAccountingSchemaaPk;
	}
	
	/**
	 * Gets the schema code.
	 *
	 * @return the schema code
	 */
	public String getSchemaCode() {
		return schemaCode;
	}
	
	/**
	 * Sets the schema code.
	 *
	 * @param schemaCode the new schema code
	 */
	public void setSchemaCode(String schemaCode) {
		this.schemaCode = schemaCode;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Gets the branch type.
	 *
	 * @return the branch type
	 */
	public Integer getBranchType() {
		return branchType;
	}
	
	/**
	 * Sets the branch type.
	 *
	 * @param branchType the new branch type
	 */
	public void setBranchType(Integer branchType) {
		this.branchType = branchType;
	}
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}
	
	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	/**
	 * Gets the register date.
	 *
	 * @return the register date
	 */
	public String getRegisterDate() {
		return registerDate;
	}
	
	/**
	 * Sets the register date.
	 *
	 * @param registerDate the new register date
	 */
	public void setRegisterDate(String registerDate) {
		this.registerDate = registerDate;
	}
	
	/**
	 * Gets the modify date.
	 *
	 * @return the modify date
	 */
	public String getModifyDate() {
		return modifyDate;
	}
	
	/**
	 * Sets the modify date.
	 *
	 * @param modifyDate the new modify date
	 */
	public void setModifyDate(String modifyDate) {
		this.modifyDate = modifyDate;
	}
	
	/**
	 * Gets the register user.
	 *
	 * @return the register user
	 */
	public String getRegisterUser() {
		return registerUser;
	}
	
	/**
	 * Sets the register user.
	 *
	 * @param registerUser the new register user
	 */
	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}
	
	/**
	 * Gets the modify user.
	 *
	 * @return the modify user
	 */
	public String getModifyUser() {
		return modifyUser;
	}
	
	/**
	 * Sets the modify user.
	 *
	 * @param modifyUser the new modify user
	 */
	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}
	
	/**
	 * Gets the cancel user.
	 *
	 * @return the cancel user
	 */
	public String getCancelUser() {
		return cancelUser;
	}
	
	/**
	 * Sets the cancel user.
	 *
	 * @param cancelUser the new cancel user
	 */
	public void setCancelUser(String cancelUser) {
		this.cancelUser = cancelUser;
	}
	
	/**
	 * Gets the cancel date.
	 *
	 * @return the cancel date
	 */
	public String getCancelDate() {
		return cancelDate;
	}
	
	/**
	 * Sets the cancel date.
	 *
	 * @param cancelDate the new cancel date
	 */
	public void setCancelDate(String cancelDate) {
		this.cancelDate = cancelDate;
	}
	
	/**
	 * Gets the schema code consult.
	 *
	 * @return the schema code consult
	 */
	public String getSchemaCodeConsult() {
		return schemaCodeConsult;
	}
	
	/**
	 * Sets the schema code consult.
	 *
	 * @param schemaCodeConsult the new schema code consult
	 */
	public void setSchemaCodeConsult(String schemaCodeConsult) {
		this.schemaCodeConsult = schemaCodeConsult;
	}
	
	/**
	 * Gets the status description.
	 *
	 * @return the status description
	 */
	public String getStatusDescription() {
		return statusDescription;
	}
	
	/**
	 * Sets the status description.
	 *
	 * @param statusDescription the new status description
	 */
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}
	
	/**
	 * Gets the list accounting account.
	 *
	 * @return the list accounting account
	 */
	public List<AccountingAccountTo> getListAccountingAccount() {
		return listAccountingAccount;
	}
	
	/**
	 * Sets the list accounting account.
	 *
	 * @param listAccountingAccount the new list accounting account
	 */
	public void setListAccountingAccount(
			List<AccountingAccountTo> listAccountingAccount) {
		this.listAccountingAccount = listAccountingAccount;
	}
	 
  
	

}
