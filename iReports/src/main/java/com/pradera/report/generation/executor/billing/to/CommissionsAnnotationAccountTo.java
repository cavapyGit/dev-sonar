package com.pradera.report.generation.executor.billing.to;

import java.io.Serializable;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingServiceTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class CommissionsAnnotationAccountTo implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	private String serviceCode;
	private Long idParticipantPk;
	private Long cui;
	private Integer institutionType;
	private Long entityCode;
	private String dateInitial;
	private String dateEnd;
	private String tarifaCode;
	private String issuerCode;
	private String strEntityCode;
	
	public CommissionsAnnotationAccountTo() {
		
	}

	/**
	 * @return the serviceCode
	 */
	public String getServiceCode() {
		return serviceCode;
	}

	/**
	 * @param serviceCode the serviceCode to set
	 */
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	/**
	 * @return the idParticipantPk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * @param idParticipantPk the idParticipantPk to set
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * @return the cui
	 */
	public Long getCui() {
		return cui;
	}

	/**
	 * @param cui the cui to set
	 */
	public void setCui(Long cui) {
		this.cui = cui;
	}

	/**
	 * @return the institutionType
	 */
	public Integer getInstitutionType() {
		return institutionType;
	}

	/**
	 * @param institutionType the institutionType to set
	 */
	public void setInstitutionType(Integer institutionType) {
		this.institutionType = institutionType;
	}

	/**
	 * @return the entityCode
	 */
	public Long getEntityCode() {
		return entityCode;
	}

	/**
	 * @param entityCode the entityCode to set
	 */
	public void setEntityCode(Long entityCode) {
		this.entityCode = entityCode;
	}

	/**
	 * @return the dateInitial
	 */
	public String getDateInitial() {
		return dateInitial;
	}

	/**
	 * @param dateInitial the dateInitial to set
	 */
	public void setDateInitial(String dateInitial) {
		this.dateInitial = dateInitial;
	}

	/**
	 * @return the dateEnd
	 */
	public String getDateEnd() {
		return dateEnd;
	}

	/**
	 * @param dateEnd the dateEnd to set
	 */
	public void setDateEnd(String dateEnd) {
		this.dateEnd = dateEnd;
	}

	/**
	 * @return the tarifaCode
	 */
	public String getTarifaCode() {
		return tarifaCode;
	}

	/**
	 * @param tarifaCode the tarifaCode to set
	 */
	public void setTarifaCode(String tarifaCode) {
		this.tarifaCode = tarifaCode;
	}

	/**
	 * @return the issuerCode
	 */
	public String getIssuerCode() {
		return issuerCode;
	}

	/**
	 * @param issuerCode the issuerCode to set
	 */
	public void setIssuerCode(String issuerCode) {
		this.issuerCode = issuerCode;
	}

	/**
	 * @return the strEntityCode
	 */
	public String getStrEntityCode() {
		return strEntityCode;
	}

	/**
	 * @param strEntityCode the strEntityCode to set
	 */
	public void setStrEntityCode(String strEntityCode) {
		this.strEntityCode = strEntityCode;
	}
	
	
}
