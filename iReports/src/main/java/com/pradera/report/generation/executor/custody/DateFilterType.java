/**
 * 
 */
package com.pradera.report.generation.executor.custody;

/**
 * @author PRADERA
 *
 */
public enum DateFilterType {
	
	ADMISSION_DATE(1, "FECHA DE INGRESO"),
	
	EXPIRATION_DATE(2, "FECHA DE VENCIMIENTO"),
	
	CUT_DATE(3, "FECHA DE CORTE");
	
	private Integer code;
	
	private String value;

	private DateFilterType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	public Integer getCode() {
		return code;
	}


	public String getValue() {
		return value;
	}

}
