package com.pradera.report.generation.executor.securities;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.framework.entity.producer.DepositaryDataBase;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;

@ReportProcess(name = "DpfFractionationTrackingReport")
public class DpfFractionationTrackingReport extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ParticipantServiceBean participantServiceBean;
	
	public DpfFractionationTrackingReport() {
		// TODO Auto-generated constructor stub
	}

	@Inject @DepositaryDataBase
	protected EntityManager em;
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		Map<String, Object> parametersRequired = new HashMap<>();
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		
		for (ReportLoggerDetail r : loggerDetails){
			if (r.getFilterName().equals("issuer")){
				if(r.getFilterValue() != null){
					parametersRequired.put("issuer_mnemonic",r.getFilterDescription());
				}else parametersRequired.put("issuer_mnemonic","TODOS");
			}
			
			if (r.getFilterName().equals("participant")){
				if (r.getFilterValue() != null)
					parametersRequired.put("participant_mnemonic",r.getFilterDescription());
				else parametersRequired.put("participant_mnemonic","TODOS");
			}
			
			if (r.getFilterName().equals("account_holder")){
				if (r.getFilterValue() != null){
					parametersRequired.put("account_number",r.getFilterDescription());
				}else parametersRequired.put("account_number","TODAS");
			}
			
			if (r.getFilterName().equals("cui")){
				if(r.getFilterValue() != null){
					parametersRequired.put("full_name", r.getFilterDescription());
				}else parametersRequired.put("full_name", "TODOS");
			}
			
			if (r.getFilterName().equals("class_security")){
				if(r.getFilterValue() != null){
					parametersRequired.put("security_mnemonic", r.getFilterDescription());
				}
				else parametersRequired.put("security_mnemonic", "TODAS");
			}

			if (r.getFilterName().equals("currency")){
				if(r.getFilterValue() != null  )
					parametersRequired.put("currency_description", r.getFilterDescription());
				else parametersRequired.put("currency_description", "TODAS");
			}
			
		}
		return parametersRequired;
	}
}
