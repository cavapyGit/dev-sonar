package com.pradera.report.generation.executor.custody.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ExpirationReportTO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String endDate;
	private String initialDate;
	private Long cui;
	private String securityCode;
	private Integer securityClass;
	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}
	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	/**
	 * @return the initialDate
	 */
	public String getInitialDate() {
		return initialDate;
	}
	/**
	 * @param initialDate the initialDate to set
	 */
	public void setInitialDate(String initialDate) {
		this.initialDate = initialDate;
	}
	/**
	 * @return the cui
	 */
	public Long getCui() {
		return cui;
	}
	/**
	 * @param cui the cui to set
	 */
	public void setCui(Long cui) {
		this.cui = cui;
	}
	/**
	 * @return the securityCode
	 */
	public String getSecurityCode() {
		return securityCode;
	}
	/**
	 * @param securityCode the securityCode to set
	 */
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
	/**
	 * @return the securityClass
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}
	/**
	 * @param securityClass the securityClass to set
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}
	
	
	
}
