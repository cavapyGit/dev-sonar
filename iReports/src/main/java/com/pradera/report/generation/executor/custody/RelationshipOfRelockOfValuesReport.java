package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounts.Holder;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.executor.custody.to.GenericCustodyOperationTO;
import com.pradera.report.generation.executor.securities.service.PaymentChronogramBySecurity;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "RelationshipOfRelockOfValuesReport")
public class RelationshipOfRelockOfValuesReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;

	@EJB
	private CustodyReportServiceBean custodyReportServiceBean;
	
	@Inject
	private PraderaLogger log;

	public RelationshipOfRelockOfValuesReport() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {

		GenericCustodyOperationTO gfto = new GenericCustodyOperationTO();

		for(ReportLoggerDetail param: getReportLogger().getReportLoggerDetails()){
			if(param.getFilterName().equals("participant")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setParticipant(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("cui_code")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setHolder(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("issuer_code")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setIssuer(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("block_type")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setBlockType(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("date_initial")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setInitialDate(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("date_end")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setFinalDate(param.getFilterValue().toString());
				}
			}
		} 
		
		String strQuery = custodyReportServiceBean.getRelationshipOfRelockOfValuesReport(gfto); 
		
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> secSecurityClass			= new HashMap<Integer, String>();
		Map<Integer,String> secBlockType	 			= new HashMap<Integer, String>();

		
		try {
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secSecurityClass.put(param.getParameterTablePk(), param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.TYPE_AFFECTATION.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secBlockType.put(param.getParameterTablePk(), param.getDescription());
			}

			
			
			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}

		if(!Validations.validateIsNotNull(gfto.getHolder())){
			gfto.setHolder("Todos");
		}else {
			Holder holder =  custodyReportServiceBean.searchHolder(Long.valueOf(gfto.getHolder()));
			gfto.setHolder("("+holder.getMnemonic()+") "+holder.getFullName());
		}
		
		parametersRequired.put("str_Query", strQuery);
		parametersRequired.put("initialDt", gfto.getInitialDate());
		parametersRequired.put("finalDt", gfto.getFinalDate());
		parametersRequired.put("mSecurityClass", secSecurityClass);
		parametersRequired.put("mBlockType", secBlockType);
		parametersRequired.put("pHolder", gfto.getHolder());
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		
		return parametersRequired;
		
		
	}

}
