package com.pradera.report.generation.executor.billing.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.component.MovementType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ServiceRateTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class ServiceRateTo implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id service rate pk. */
	private Long 	idServiceRatePk;
	
	/** The rate code. */
	private Integer  rateCode;
	
	/** The rate name. */
	private String  rateName;
	
	/** The currency rate. */
	private Integer currencyRate;
	
	/** The rate status. */
	private Integer rateStatus;
	
	/** The new rate status. */
	private Integer newRateStatus;
	
	/** The end effective date. */
	private Date endEffectiveDate = CommonsUtilities.currentDate();
	
	/** The initial effective date. */
	private Date initialEffectiveDate = CommonsUtilities.currentDate();
	
	/** The applied tax selected. */
	private Integer appliedTaxSelected;
    
    /** The description rate. */
    private String descriptionRate;
    
    /** The description status. */
    private String descriptionStatus;
	
	/** The description type. */
	private String descriptionType;
	
	/** The description currency. */
	private String descriptionCurrency;
	
	/** The rate type. */
	private Integer rateType;
	
	
	
	/**  only for Fixed. */
	private BigDecimal amountRate;
	
	/**  only for Percentage. */
	private BigDecimal percentageRate;
	
	/**  only for Staggered. */
	private Integer sequenceNumber;	
	
	/** The unit. */
	private Integer unit=1; 
	
	/** The disable seq number. */
	private Boolean disableSeqNumber;
	
	/** The disable btn generate. */
	private Boolean disableBtnGenerate;
	
	/** The disable unit. */
	private Boolean disableUnit;
	
	/** The min scale value. */
	private BigDecimal minScaleValue;
	
	/** The max scale value. */
	private BigDecimal maxScaleValue;
	
	/**  only for Movement. */
	private String descriptionMovementType;
	
	/** The code movement type. */
	private Long codeMovementType;
	
	/** The rate amount. */
	private BigDecimal rateAmount;
	
	/** The movement type selected. */
	private MovementType movementTypeSelected;
	
	/** The movement type search. */
	private MovementType movementTypeSearch;	
	
	/** The list rate movement to. */
	private List<RateMovementTo> listRateMovementTo;
	
	/** The data model movement type list. */
	private GenericDataModel<MovementType> dataModelMovementTypeList;
	
	/** The rate percent. */
	private BigDecimal ratePercent;
	
	/**
	 * Gets the id service rate pk.
	 *
	 * @return the id service rate pk
	 */
	public Long getIdServiceRatePk() {
		return idServiceRatePk;
	}
	
	/**
	 * Sets the id service rate pk.
	 *
	 * @param idServiceRatePk the new id service rate pk
	 */
	public void setIdServiceRatePk(Long idServiceRatePk) {
		this.idServiceRatePk = idServiceRatePk;
	}
	
	/**
	 * Gets the rate code.
	 *
	 * @return the rate code
	 */
	public Integer getRateCode() {
		return rateCode;
	}
	
	/**
	 * Sets the rate code.
	 *
	 * @param rateCode the new rate code
	 */
	public void setRateCode(Integer rateCode) {
		this.rateCode = rateCode;
	}
	
	/**
	 * Gets the rate name.
	 *
	 * @return the rate name
	 */
	public String getRateName() {
		return rateName;
	}
	
	/**
	 * Sets the rate name.
	 *
	 * @param rateName the new rate name
	 */
	public void setRateName(String rateName) {
		this.rateName = rateName;
	}
	
	/**
	 * Gets the currency rate.
	 *
	 * @return the currency rate
	 */
	public Integer getCurrencyRate() {
		return currencyRate;
	}
	
	/**
	 * Sets the currency rate.
	 *
	 * @param currencyRate the new currency rate
	 */
	public void setCurrencyRate(Integer currencyRate) {
		this.currencyRate = currencyRate;
	}
	
	/**
	 * Gets the rate status.
	 *
	 * @return the rate status
	 */
	public Integer getRateStatus() {
		return rateStatus;
	}
	
	/**
	 * Sets the rate status.
	 *
	 * @param rateStatus the new rate status
	 */
	public void setRateStatus(Integer rateStatus) {
		this.rateStatus = rateStatus;
	}
	
	/**
	 * Gets the new rate status.
	 *
	 * @return the new rate status
	 */
	public Integer getNewRateStatus() {
		return newRateStatus;
	}
	
	/**
	 * Sets the new rate status.
	 *
	 * @param newRateStatus the new new rate status
	 */
	public void setNewRateStatus(Integer newRateStatus) {
		this.newRateStatus = newRateStatus;
	}
	
	/**
	 * Gets the end effective date.
	 *
	 * @return the end effective date
	 */
	public Date getEndEffectiveDate() {
		return endEffectiveDate;
	}
	
	/**
	 * Sets the end effective date.
	 *
	 * @param endEffectiveDate the new end effective date
	 */
	public void setEndEffectiveDate(Date endEffectiveDate) {
		this.endEffectiveDate = endEffectiveDate;
	}
	
	/**
	 * Gets the initial effective date.
	 *
	 * @return the initial effective date
	 */
	public Date getInitialEffectiveDate() {
		return initialEffectiveDate;
	}
	
	/**
	 * Sets the initial effective date.
	 *
	 * @param initialEffectiveDate the new initial effective date
	 */
	public void setInitialEffectiveDate(Date initialEffectiveDate) {
		this.initialEffectiveDate = initialEffectiveDate;
	}
	
	/**
	 * Gets the applied tax selected.
	 *
	 * @return the applied tax selected
	 */
	public Integer getAppliedTaxSelected() {
		return appliedTaxSelected;
	}
	
	/**
	 * Sets the applied tax selected.
	 *
	 * @param appliedTaxSelected the new applied tax selected
	 */
	public void setAppliedTaxSelected(Integer appliedTaxSelected) {
		this.appliedTaxSelected = appliedTaxSelected;
	}
	
	/**
	 * Gets the description rate.
	 *
	 * @return the description rate
	 */
	public String getDescriptionRate() {
		return descriptionRate;
	}
	
	/**
	 * Sets the description rate.
	 *
	 * @param descriptionRate the new description rate
	 */
	public void setDescriptionRate(String descriptionRate) {
		this.descriptionRate = descriptionRate;
	}
	
	/**
	 * Gets the description status.
	 *
	 * @return the description status
	 */
	public String getDescriptionStatus() {
		return descriptionStatus;
	}
	
	/**
	 * Sets the description status.
	 *
	 * @param descriptionStatus the new description status
	 */
	public void setDescriptionStatus(String descriptionStatus) {
		this.descriptionStatus = descriptionStatus;
	}
	
	/**
	 * Gets the description type.
	 *
	 * @return the description type
	 */
	public String getDescriptionType() {
		return descriptionType;
	}
	
	/**
	 * Sets the description type.
	 *
	 * @param descriptionType the new description type
	 */
	public void setDescriptionType(String descriptionType) {
		this.descriptionType = descriptionType;
	}
	
	/**
	 * Gets the description currency.
	 *
	 * @return the description currency
	 */
	public String getDescriptionCurrency() {
		return descriptionCurrency;
	}
	
	/**
	 * Sets the description currency.
	 *
	 * @param descriptionCurrency the new description currency
	 */
	public void setDescriptionCurrency(String descriptionCurrency) {
		this.descriptionCurrency = descriptionCurrency;
	}
	
	/**
	 * Gets the rate type.
	 *
	 * @return the rate type
	 */
	public Integer getRateType() {
		return rateType;
	}
	
	/**
	 * Sets the rate type.
	 *
	 * @param rateType the new rate type
	 */
	public void setRateType(Integer rateType) {
		this.rateType = rateType;
	}
	
	/**
	 * Gets the amount rate.
	 *
	 * @return the amount rate
	 */
	public BigDecimal getAmountRate() {
		return amountRate;
	}
	
	/**
	 * Sets the amount rate.
	 *
	 * @param amountRate the new amount rate
	 */
	public void setAmountRate(BigDecimal amountRate) {
		this.amountRate = amountRate;
	}
	
	/**
	 * Gets the percentage rate.
	 *
	 * @return the percentage rate
	 */
	public BigDecimal getPercentageRate() {
		return percentageRate;
	}
	
	/**
	 * Sets the percentage rate.
	 *
	 * @param percentageRate the new percentage rate
	 */
	public void setPercentageRate(BigDecimal percentageRate) {
		this.percentageRate = percentageRate;
	}
	
	/**
	 * Gets the sequence number.
	 *
	 * @return the sequence number
	 */
	public Integer getSequenceNumber() {
		return sequenceNumber;
	}
	
	/**
	 * Sets the sequence number.
	 *
	 * @param sequenceNumber the new sequence number
	 */
	public void setSequenceNumber(Integer sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	
	/**
	 * Gets the unit.
	 *
	 * @return the unit
	 */
	public Integer getUnit() {
		return unit;
	}
	
	/**
	 * Sets the unit.
	 *
	 * @param unit the new unit
	 */
	public void setUnit(Integer unit) {
		this.unit = unit;
	}
	
	/**
	 * Gets the disable seq number.
	 *
	 * @return the disable seq number
	 */
	public Boolean getDisableSeqNumber() {
		return disableSeqNumber;
	}
	
	/**
	 * Sets the disable seq number.
	 *
	 * @param disableSeqNumber the new disable seq number
	 */
	public void setDisableSeqNumber(Boolean disableSeqNumber) {
		this.disableSeqNumber = disableSeqNumber;
	}
	
	/**
	 * Gets the disable btn generate.
	 *
	 * @return the disable btn generate
	 */
	public Boolean getDisableBtnGenerate() {
		return disableBtnGenerate;
	}
	
	/**
	 * Sets the disable btn generate.
	 *
	 * @param disableBtnGenerate the new disable btn generate
	 */
	public void setDisableBtnGenerate(Boolean disableBtnGenerate) {
		this.disableBtnGenerate = disableBtnGenerate;
	}
	
	/**
	 * Gets the disable unit.
	 *
	 * @return the disable unit
	 */
	public Boolean getDisableUnit() {
		return disableUnit;
	}
	
	/**
	 * Sets the disable unit.
	 *
	 * @param disableUnit the new disable unit
	 */
	public void setDisableUnit(Boolean disableUnit) {
		this.disableUnit = disableUnit;
	}
	
	/**
	 * Gets the min scale value.
	 *
	 * @return the min scale value
	 */
	public BigDecimal getMinScaleValue() {
		return minScaleValue;
	}
	
	/**
	 * Sets the min scale value.
	 *
	 * @param minScaleValue the new min scale value
	 */
	public void setMinScaleValue(BigDecimal minScaleValue) {
		this.minScaleValue = minScaleValue;
	}
	
	/**
	 * Gets the max scale value.
	 *
	 * @return the max scale value
	 */
	public BigDecimal getMaxScaleValue() {
		return maxScaleValue;
	}
	
	/**
	 * Sets the max scale value.
	 *
	 * @param maxScaleValue the new max scale value
	 */
	public void setMaxScaleValue(BigDecimal maxScaleValue) {
		this.maxScaleValue = maxScaleValue;
	}
	
	/**
	 * Gets the description movement type.
	 *
	 * @return the description movement type
	 */
	public String getDescriptionMovementType() {
		return descriptionMovementType;
	}
	
	/**
	 * Sets the description movement type.
	 *
	 * @param descriptionMovementType the new description movement type
	 */
	public void setDescriptionMovementType(String descriptionMovementType) {
		this.descriptionMovementType = descriptionMovementType;
	}
	
	/**
	 * Gets the code movement type.
	 *
	 * @return the code movement type
	 */
	public Long getCodeMovementType() {
		return codeMovementType;
	}
	
	/**
	 * Sets the code movement type.
	 *
	 * @param codeMovementType the new code movement type
	 */
	public void setCodeMovementType(Long codeMovementType) {
		this.codeMovementType = codeMovementType;
	}
	
	/**
	 * Gets the rate amount.
	 *
	 * @return the rate amount
	 */
	public BigDecimal getRateAmount() {
		return rateAmount;
	}
	
	/**
	 * Sets the rate amount.
	 *
	 * @param rateAmount the new rate amount
	 */
	public void setRateAmount(BigDecimal rateAmount) {
		this.rateAmount = rateAmount;
	}
	
	/**
	 * Gets the movement type selected.
	 *
	 * @return the movement type selected
	 */
	public MovementType getMovementTypeSelected() {
		return movementTypeSelected;
	}
	
	/**
	 * Sets the movement type selected.
	 *
	 * @param movementTypeSelected the new movement type selected
	 */
	public void setMovementTypeSelected(MovementType movementTypeSelected) {
		this.movementTypeSelected = movementTypeSelected;
	}
	
	/**
	 * Gets the movement type search.
	 *
	 * @return the movement type search
	 */
	public MovementType getMovementTypeSearch() {
		return movementTypeSearch;
	}
	
	/**
	 * Sets the movement type search.
	 *
	 * @param movementTypeSearch the new movement type search
	 */
	public void setMovementTypeSearch(MovementType movementTypeSearch) {
		this.movementTypeSearch = movementTypeSearch;
	}
	
	/**
	 * Gets the data model movement type list.
	 *
	 * @return the data model movement type list
	 */
	public GenericDataModel<MovementType> getDataModelMovementTypeList() {
		return dataModelMovementTypeList;
	}
	
	/**
	 * Sets the data model movement type list.
	 *
	 * @param dataModelMovementTypeList the new data model movement type list
	 */
	public void setDataModelMovementTypeList(
			GenericDataModel<MovementType> dataModelMovementTypeList) {
		this.dataModelMovementTypeList = dataModelMovementTypeList;
	}
	
	/**
	 * Gets the rate percent.
	 *
	 * @return the rate percent
	 */
	public BigDecimal getRatePercent() {
		return ratePercent;
	}
	
	/**
	 * Sets the rate percent.
	 *
	 * @param ratePercent the new rate percent
	 */
	public void setRatePercent(BigDecimal ratePercent) {
		this.ratePercent = ratePercent;
	}
	
	/**
	 * Gets the list rate movement to.
	 *
	 * @return the list rate movement to
	 */
	public List<RateMovementTo> getListRateMovementTo() {
		return listRateMovementTo;
	}
	
	/**
	 * Sets the list rate movement to.
	 *
	 * @param listRateMovementTo the new list rate movement to
	 */
	public void setListRateMovementTo(List<RateMovementTo> listRateMovementTo) {
		this.listRateMovementTo = listRateMovementTo;
	}
	
	
   
}
