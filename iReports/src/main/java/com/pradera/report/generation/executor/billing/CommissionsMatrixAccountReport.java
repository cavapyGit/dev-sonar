package com.pradera.report.generation.executor.billing;
import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.helperui.service.BillingServiceQueryServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;

@ReportProcess(name = "CommissionsMatrixAccountReport")
public class CommissionsMatrixAccountReport extends GenericReport {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The parameter service. */
	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	private BillingServiceQueryServiceBean billingServiceQueryServiceBean;
	
	@EJB
	private ParticipantServiceBean participantServiceBean;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/**
	 * Instantiates a new commissions annotation account report.
	 */
	public CommissionsMatrixAccountReport() {	
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#addParametersQueryReport()
	 */
	@Override
	public void addParametersQueryReport() {		
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#getCustomJasperParameters()
	 */
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		Map<String, Object> parametersRequired = new HashMap<>();
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		String serviceCode=null;
		for (int i = 0; i < listaLogger.size(); i++){
			if(listaLogger.get(i).getFilterName().equals("p_service_code")){
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
			    	serviceCode = listaLogger.get(i).getFilterValue();
			    }
			}				
		}
		/** parametros */
		ArrayList<Object[]> result = new ArrayList<Object[]>();
		if(Validations.validateIsNotNullAndNotEmpty(serviceCode)){
			result = billingServiceQueryServiceBean.getRateParametersByServiceCode(serviceCode);
			if(Validations.validateIsNotEmpty(result)) {
				for (Object[] item : result) {
					if(item.length == 4) {
						BigDecimal p_monto_desc = (BigDecimal) item[0]; 
						p_monto_desc = p_monto_desc.setScale(2, BigDecimal.ROUND_HALF_UP);						
						BigDecimal p_monto_imp = (BigDecimal) item[1]; 
						p_monto_imp = p_monto_imp.setScale(2, BigDecimal.ROUND_HALF_UP);
						BigDecimal p_monto_total = (BigDecimal) item[2]; 
						p_monto_total = p_monto_total.setScale(2, BigDecimal.ROUND_HALF_UP);
						BigDecimal p_cobro_dia = (BigDecimal) item[3]; 
						p_cobro_dia = p_cobro_dia.setScale(2, BigDecimal.ROUND_DOWN);
					
						parametersRequired.put("p_monto_desc", p_monto_desc.toString());
						parametersRequired.put("p_monto_imp", p_monto_imp.toString());
						parametersRequired.put("p_monto_total", p_monto_total.toString());
						parametersRequired.put("p_cobro_dia", p_cobro_dia.toString());
					}					
				}
			}
		}
		String periodoDias = (isLeapYear()) ? "366" : "365";
		parametersRequired.put("p_dias_anio", periodoDias);
		
		int currentYear, lastYear;
		Calendar anio = Calendar.getInstance();
		currentYear = anio.get(Calendar.YEAR);
		lastYear = anio.get(Calendar.YEAR) - 1;
		parametersRequired.put("p_current_year", String.valueOf(currentYear));
		parametersRequired.put("p_last_year", String.valueOf(lastYear));
		
		return parametersRequired;
	}
		
	/** verify current leap year */
	public boolean isLeapYear() {
		Calendar anio = Calendar.getInstance();
		GregorianCalendar cal = (GregorianCalendar) GregorianCalendar.getInstance();
		return cal.isLeapYear(anio.get(Calendar.YEAR));
	}

}
