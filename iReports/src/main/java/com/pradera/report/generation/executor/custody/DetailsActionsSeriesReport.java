package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserFilterTO;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.service.IssuerQueryServiceBean;
import com.pradera.core.component.helperui.to.IssuerSearcherTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "DetailsActionsSeriesReport")
public class DetailsActionsSeriesReport extends GenericReport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;	
	@EJB
	private IssuerQueryServiceBean issuerQueryServiceBean;
	@Inject PraderaLogger log;
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void addParametersQueryReport() {
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		if (listaLogger == null) {
			listaLogger = new ArrayList<ReportLoggerDetail>();
		}
		ReportLoggerDetail loggerDetail = new ReportLoggerDetail();
		loggerDetail.setFilterName("issuer_desc");
		String issuerCode= null;
		for (ReportLoggerDetail r : listaLogger) {	
			if (r.getFilterName().equals("issuer"))
				issuerCode = r.getFilterValue();
		}
		if (issuerCode!=null){
			IssuerSearcherTO issuerSearcherTO= new IssuerSearcherTO();
			issuerSearcherTO.setIssuerCode(issuerCode);
			Issuer issuer;
			try {
				issuer = issuerQueryServiceBean.getIssuerByCode(issuerSearcherTO);
				issuerCode=issuer.getMnemonic()+" - "+issuerCode+" - " +issuer.getBusinessName();
				loggerDetail.setFilterValue(issuerCode);
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}else{
			loggerDetail.setFilterValue("TODOS");
		}
		listaLogger.add(loggerDetail);
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		ParameterTableTO  filter = new ParameterTableTO();
		Map<String,Object> parametersRequired = new HashMap<>();
		Map<Integer,String> security_class_desc = new HashMap<Integer, String>();//security_class_desc_list
		
		UserFilterTO userFilter=new UserFilterTO();
		userFilter.setUserName(getReportLogger().getRegistryUser());

		try {
			
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				security_class_desc.put(param.getParameterTablePk(), param.getText1() + " - " + param.getDescription());
			}
			
		}catch(Exception ex){
			ex.printStackTrace();
			throw new RuntimeException();
		}
		parametersRequired.put("security_class_desc_list", security_class_desc);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());

		return parametersRequired;
	}
}
