package com.pradera.report.generation.executor.account;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.GeographicLocationServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.GeographicLocationType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;

@ReportProcess(name = "FATCANotificationReport")
public class FATCANotificationReport extends GenericReport {
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	private GeographicLocationServiceBean geographicLocationServiceBean;
	
	@Inject
	PraderaLogger log;

	
	public FATCANotificationReport() {
	}
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParametersQueryReport() {

	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		GeographicLocationTO filterG = new GeographicLocationTO();
		
		Map<Integer,String> nationality = new HashMap<Integer, String>();
		Map<Integer,String> typeDocument = new HashMap<Integer, String>();
		
		try {
		
			filterG.setGeographicLocationType(GeographicLocationType.COUNTRY.getCode());
			for(GeographicLocation param : geographicLocationServiceBean.getListGeographicLocation(filterG)) {
				nationality.put(param.getIdGeographicLocationPk(), param.getName());
			}
			
			filter.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				typeDocument.put(param.getParameterTablePk(), param.getIndicator1());
			}
		
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		parametersRequired.put("p_typeDocument", typeDocument);
		parametersRequired.put("p_nationality", nationality);
		
		
		return parametersRequired;
	}
	
	

}
