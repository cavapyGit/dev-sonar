package com.pradera.report.generation.executor.collectioneconomic.to;

import java.io.Serializable;
import java.util.Date;


public class RequestCollectionTO implements Serializable{
	
	/**Tipo de reporte*/
	private Integer reportType;
	
	/**Id de participante para la busqueda*/
	private Long idParticipantPk;
	
	/**Id de titular CUI idHolderPK*/ 
	private Long idHoderPk;
	
	/**Id de clase - calve de valor */
	private String idSecurityCodePk;
	
	/**Fecha de expiracion*/
	private String dueDate;

	/**Fecha de pago para cobro de derechos economicos*/
	private String paymentDate;
	
	/**Numero de solicitud que se registro*/
	private Long idReqCollectionEconomicPk;
	
	/**Fecha inicial en el cual se registro la solicitud*/
	private String initialDate;
	
	/**Fecha final de busqueda*/
	private String finalDate;
	
	public RequestCollectionTO() {
		// TODO Auto-generated constructor stub
	}

	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	public Long getIdHoderPk() {
		return idHoderPk;
	}

	public void setIdHoderPk(Long idHoderPk) {
		this.idHoderPk = idHoderPk;
	}

	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}


	public Long getIdReqCollectionEconomicPk() {
		return idReqCollectionEconomicPk;
	}

	public void setIdReqCollectionEconomicPk(Long idReqCollectionEconomicPk) {
		this.idReqCollectionEconomicPk = idReqCollectionEconomicPk;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(String initialDate) {
		this.initialDate = initialDate;
	}

	public String getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(String finalDate) {
		this.finalDate = finalDate;
	}

	public Integer getReportType() {
		return reportType;
	}

	public void setReportType(Integer reportType) {
		this.reportType = reportType;
	}
}
