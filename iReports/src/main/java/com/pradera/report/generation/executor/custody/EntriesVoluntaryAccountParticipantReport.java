package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;

import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.util.view.UtilReportConstants;
//Reporte de anotaciones en cuenta voluntaria por participante
@ReportProcess(name = "EntriesVoluntaryAccountParticipantReport")
public class EntriesVoluntaryAccountParticipantReport extends GenericReport{
	
	private static final long serialVersionUID = 1L;
	//parametros para el tipo de moneda
	private final String MAP_CURRENCY="map_currency";
	
	private Map<String, Object> map= new HashMap<String, Object>();
	@EJB
	private ParameterServiceBean parameterService;
	
	@Override
	public Map<String, Object> getCustomJasperParameters(){
		Map<Integer,String> typesCurrency = new HashMap<Integer, String>();
		ParameterTableTO  parameterFilter = new ParameterTableTO();
	    parameterFilter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(parameterFilter)) 
			typesCurrency.put(param.getParameterTablePk(), param.getDescription());
		map.put(MAP_CURRENCY, typesCurrency);
		map.put("logo_path", UtilReportConstants.readLogoReport());
//		Iterator it = map.entrySet().iterator();
//		while (it.hasNext()) {
//			Map.Entry e = (Map.Entry)it.next();
//			System.out.println(e.getKey() + ":" + e.getValue());
//		}
		return map;
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		return null;
	}
}
