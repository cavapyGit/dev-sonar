package com.pradera.report.generation.executor.settlement.to;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ChainedOperationPendingTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/09/2015
 */
public class ChainedOperationPendingTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	
	/** The str date initial. */
	private String strDateInitial;
	

	/** The str date end. */
	private String strDateEnd;
	
	/** The participant. */
	private String participant;


	/**
	 * Gets the str date initial.
	 *
	 * @return the strDateInitial
	 */
	public String getStrDateInitial() {
		return strDateInitial;
	}


	/**
	 * Sets the str date initial.
	 *
	 * @param strDateInitial the strDateInitial to set
	 */
	public void setStrDateInitial(String strDateInitial) {
		this.strDateInitial = strDateInitial;
	}


	/**
	 * Gets the str date end.
	 *
	 * @return the strDateEnd
	 */
	public String getStrDateEnd() {
		return strDateEnd;
	}


	/**
	 * Sets the str date end.
	 *
	 * @param strDateEnd the strDateEnd to set
	 */
	public void setStrDateEnd(String strDateEnd) {
		this.strDateEnd = strDateEnd;
	}


	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public String getParticipant() {
		return participant;
	}


	/**
	 * Sets the participant.
	 *
	 * @param participant the participant to set
	 */
	public void setParticipant(String participant) {
		this.participant = participant;
	}
	
	
}
