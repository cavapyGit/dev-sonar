package com.pradera.report.generation.executor.settlement;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserFilterTO;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;

@ReportProcess(name = "SettlementTypeExchangeReport")
public class SettlementTypeExchangeReport extends GenericReport {

	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;
	@Inject
	PraderaLogger log;
	
	@EJB
	private ParticipantServiceBean participantServiceBean;
	
	@Inject
	ClientRestService clientRestService;
	
	public SettlementTypeExchangeReport() {
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParametersQueryReport() {

	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {

		Map<String, Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> request_status = new HashMap<Integer, String>();
		
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		String partSourceCode = null;
		String partSourceMnemonic = null;
		
		/*String partTargetCode = null;
		String partTargetMnemonic = null;*/
		
		String isParticipant = null;
		
		UserFilterTO userFilter=new UserFilterTO();
		userFilter.setUserName(getReportLogger().getRegistryUser());

//		List<UserAccountSession> lstUserAccount=clientRestService.getUsersInformation(userFilter);
//		if(lstUserAccount.size()==GeneralConstants.ONE_VALUE_INTEGER){
//			UserAccountSession objUserAccountSession=lstUserAccount.get(0);
//			if(objUserAccountSession.getParticipantCode()!=null){
//				isParticipant = objUserAccountSession.getParticipantCode().toString();
//			}
//		}
//		//SI ES NULO LO MUESTRO
//		parametersRequired.put("isParticipant", isParticipant);
		
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.PARTICIPANT_SOURCE))
				partSourceCode = r.getFilterValue();
			/*if (r.getFilterName().equals(ReportConstant.PARTICIPANT_TARGET))
				partTargetCode = r.getFilterValue();*/
		}
		parametersRequired.put("isParticipant", partSourceCode);
		if(partSourceCode != null)
			partSourceMnemonic = participantServiceBean.find(new Long(partSourceCode),Participant.class).getMnemonic();
		else
			partSourceMnemonic = "TODOS";
		
		/*if(partTargetCode != null)
			partTargetMnemonic = participantServiceBean.find(new Long(partTargetCode),Participant.class).getMnemonic();
		else
			partTargetMnemonic = "TODOS";*/
		
		try {
			filter.setMasterTableFk(MasterTableType.SETTLEMENT_REQUEST_STATE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				request_status.put(param.getParameterTablePk(), param.getParameterName());
			}
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		parametersRequired.put("part_source_mnemonic", partSourceMnemonic);
		//parametersRequired.put("part_target_mnemonic", partTargetMnemonic);
		parametersRequired.put("p_request_status", request_status);
		// TODO Auto-generated method stub
		return parametersRequired;
	}
	
}
