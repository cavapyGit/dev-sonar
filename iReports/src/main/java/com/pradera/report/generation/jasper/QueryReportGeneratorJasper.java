package com.pradera.report.generation.jasper;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.inject.Alternative;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRParameter;

import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.model.report.type.ReportType;
import com.pradera.report.generation.pentaho.ReportImplementation;
import com.pradera.report.generation.service.ReportUtilServiceBean;

@Alternative
@ReportImplementation(type=ReportType.QUERY)
public class QueryReportGeneratorJasper extends ReportGeneratorJasper{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5683602706183459531L;
	
	@EJB
	ReportUtilServiceBean reportUtilService;
	
	Connection connection;

	public QueryReportGeneratorJasper() {
		setReportType(ReportType.QUERY);
	}
	
	@Override
	public Map<String, Object> getReportParameters() {
		Map<String, Object> parameter = super.getReportParameters();
		//Add parameters specific to report
		if (getReportLogger().getReportLoggerDetails()!= null 
				&& !getReportLogger().getReportLoggerDetails().isEmpty()){
			
			for(ReportLoggerDetail loggerDetil : getReportLogger().getReportLoggerDetails() ){
				parameter.put(loggerDetil.getFilterName(), loggerDetil.getFilterValue());
			}
		}
		connection=reportUtilService.getConnectioniDepositary();
		parameter.put(JRParameter.REPORT_CONNECTION, connection);
		return parameter;
	}

	@Override
	public JRDataSource getDataSource() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void releaseResources() {
		if(connection!= null){
			try{
			connection.close();
			} catch(SQLException sex){
				log.error(sex.getMessage());
			}
		}
		
	}

	@Override
	public void generateReportsByFile(byte[] arrayByte) {
		
		
	}
	@Override
	public void generateReportsByListFile(List<byte[]> listArrayByte) {
		
	}
}
