package com.pradera.report.generation.executor.negotiation;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.model.accounts.Participant;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;

@ReportProcess(name="DemandOperationsBCBReport")
public class DemandOperationsBCBReport extends GenericReport{
	
	private static final long serialVersionUID = 1L;
	
	@Inject PraderaLogger log;
	
	@EJB
	ParticipantServiceBean participantServiceBean;
	
	/*@Inject 
	@Configurable 
	String idIssuerBC;*/
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String,Object> parametersRequired = new HashMap<>();
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		String participantCode = null;
		String participantDescription = null;
		
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.PARTICIPANT_PARAM) && r.getFilterValue()!=null)
				participantCode = r.getFilterValue();
		}
		
		try {
			
			if(participantCode != null){
				Participant participant = participantServiceBean.find(Participant.class, new Long(participantCode));
				participantDescription = participant.getMnemonic() + " - " + participant.getIdParticipantPk() + " - " + participant.getDescription();
			}
			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		//parametersRequired.put("idIssuerBC", idIssuerBC);
		parametersRequired.put("participant_description", participantDescription);
		
		return parametersRequired;
	}

}
