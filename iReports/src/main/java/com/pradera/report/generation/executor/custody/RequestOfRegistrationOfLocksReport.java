package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "RequestOfRegistrationOfLocksReport")
public class RequestOfRegistrationOfLocksReport extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@Inject
	PraderaLogger log;
	
	public RequestOfRegistrationOfLocksReport() {}
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		return null;
	}

	@Override
	public void addParametersQueryReport() {}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filtro = new ParameterTableTO();
		Map<Integer,String> typeBlock = new HashMap<Integer, String>();
		Map<Integer,String> levelblock = new HashMap<Integer, String>();
		Map<Integer,String> securityclass = new HashMap<Integer, String>();
		Map<Integer,String> state = new HashMap<Integer, String>();
		
		try {
			filtro.setMasterTableFk(MasterTableType.TYPE_AFFECTATION.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filtro)) {
				typeBlock.put(param.getParameterTablePk(), param.getDescription());
			}
			filtro.setMasterTableFk(MasterTableType.LEVEL_AFFECTATION.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filtro)) {
				levelblock.put(param.getParameterTablePk(), param.getDescription());
			}
			filtro.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
				for(ParameterTable param : parameterService.getListParameterTableServiceBean(filtro)) {
				securityclass.put(param.getParameterTablePk(), param.getText1());			
			}
			filtro.setMasterTableFk(MasterTableType.STATE_AFFECTATION_REQUEST.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filtro)) {
				state.put(param.getParameterTablePk(), param.getDescription());
			}
		
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		parametersRequired.put("p_typeblock", typeBlock);
		parametersRequired.put("p_levelblock", levelblock);
		parametersRequired.put("p_securityclass",securityclass);
		parametersRequired.put("p_state", state);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		
		return parametersRequired;
	}
	
}
