package com.pradera.report.generation.executor.custody.to;

import java.io.Serializable;
import java.util.Date;

public class SecurityTransferOperationTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//form
	private String idIsinCodePk;			//SecurityTransferOperation (reference Security)
	private Integer AccountNumber;	//HolderAccount
	private Integer sourceAccountNumber;	//HolderAccount
	private Integer targetAccountNumber;	//HolderAccount
	private Long sourceIdParticipantPk;		//SecurityTransferOperation	(reference Participant)
	private Long targetIdParticipantPk;		//SecurityTransferOperation	(reference Participant)
	private Long idParticipantPk;		//SecurityTransferOperation	(reference Participant)
	private Integer transferType;			//SecurityTransferOperation
	private Date operationInitDate;			//CustodyOperation
	private Date operationEndDate;			//CustodyOperation
	private Long operationNumber;			//CustodyOperation
	private Long operationType;				//CustodyOperation
	private Integer state;						//SecurityTransferOperation
	private Integer action;							//Action
	private Integer blockType;							//Action

	//opcionales
	private Long idCustodyOperationPk;		//CustodyOperation	= pk_SecurityTransferOperation 
	private Long sourceIdHolderAccountPk;	//SecurityTransferOperation	(reference HolderAccount)
	private Long targetIdHolderAccountPk;	//SecurityTransferOperation	(reference HolderAccount)
	private Long idHolderAccountPk;	//SecurityTransferOperation	(reference HolderAccount)
	
	
	
	public String getIdIsinCodePk() {
		return idIsinCodePk;
	}
	public void setIdIsinCodePk(String idIsinCodePk) {
		this.idIsinCodePk = idIsinCodePk;
	}
	public Integer getSourceAccountNumber() {
		return sourceAccountNumber;
	}
	public void setSourceAccountNumber(Integer sourceAccountNumber) {
		this.sourceAccountNumber = sourceAccountNumber;
	}
	public Integer getTargetAccountNumber() {
		return targetAccountNumber;
	}
	public void setTargetAccountNumber(Integer targetAccountNumber) {
		this.targetAccountNumber = targetAccountNumber;
	}
	public Long getSourceIdParticipantPk() {
		return sourceIdParticipantPk;
	}
	public void setSourceIdParticipantPk(Long sourceIdParticipantPk) {
		this.sourceIdParticipantPk = sourceIdParticipantPk;
	}
	public Long getTargetIdParticipantPk() {
		return targetIdParticipantPk;
	}
	public void setTargetIdParticipantPk(Long targetIdParticipantPk) {
		this.targetIdParticipantPk = targetIdParticipantPk;
	}
	public Integer getTransferType() {
		return transferType;
	}
	public void setTransferType(Integer transferType) {
		this.transferType = transferType;
	}
	public Date getOperationInitDate() {
		return operationInitDate;
	}
	public void setOperationInitDate(Date operationInitDate) {
		this.operationInitDate = operationInitDate;
	}
	public Date getOperationEndDate() {
		return operationEndDate;
	}
	public void setOperationEndDate(Date operationEndDate) {
		this.operationEndDate = operationEndDate;
	}
	public Long getOperationNumber() {
		return operationNumber;
	}
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}
	public Long getOperationType() {
		return operationType;
	}
	public void setOperationType(Long operationType) {
		this.operationType = operationType;
	}
	public Long getIdCustodyOperationPk() {
		return idCustodyOperationPk;
	}
	public void setIdCustodyOperationPk(Long idCustodyOperationPk) {
		this.idCustodyOperationPk = idCustodyOperationPk;
	}
	public Long getSourceIdHolderAccountPk() {
		return sourceIdHolderAccountPk;
	}
	public void setSourceIdHolderAccountPk(Long sourceIdHolderAccountPk) {
		this.sourceIdHolderAccountPk = sourceIdHolderAccountPk;
	}
	public Long getTargetIdHolderAccountPk() {
		return targetIdHolderAccountPk;
	}
	public void setTargetIdHolderAccountPk(Long targetIdHolderAccountPk) {
		this.targetIdHolderAccountPk = targetIdHolderAccountPk;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public Integer getAction() {
		return action;
	}
	public void setAction(Integer action) {
		this.action = action;
	}
	public Integer getAccountNumber() {
		return AccountNumber;
	}
	public void setAccountNumber(Integer accountNumber) {
		AccountNumber = accountNumber;
	}
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}
	public Integer getBlockType() {
		return blockType;
	}
	public void setBlockType(Integer blockType) {
		this.blockType = blockType;
	}
	
}
