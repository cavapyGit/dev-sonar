package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.service.IssuerQueryServiceBean;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;


@ReportProcess(name="TransfersReport")
public class TransfersReport extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@Inject  PraderaLogger log; 
	
	@EJB
	ParticipantServiceBean participantServiceBean;

	@EJB
	private IssuerQueryServiceBean issuerQueryServiceBean;

	@EJB
	private HolderAccountComponentServiceBean accountComponentServiceBean;
	
	public TransfersReport() {
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void addParametersQueryReport() {
	}
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		
		Map<Integer,String> block = new HashMap<Integer, String>();
		Map<Integer,String> mnemonic_sc = new HashMap<Integer, String>();
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		String valueClass = null;
		String issuerCode = null;
		String issuerMnemonic = null;
		String holderAccountCode = null;
		String holderAccountNumber = null;
		String serie =null;
		String dateInitial = null;
		String dateFinal = null;
		
		// DATOS DE ENTRADA DEL SISTEMA
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals("p_issuer_account"))
				issuerCode = r.getFilterValue();	
			if (r.getFilterName().equals("p_value_class"))
				valueClass = r.getFilterValue();
			if (r.getFilterName().equals("p_account_holder"))
				holderAccountCode = r.getFilterValue();
			if (r.getFilterName().equals("p_serie"))
				serie = r.getFilterValue();	
			if (r.getFilterName().equals("date_initial"))
				dateInitial = r.getFilterValue();
			if (r.getFilterName().equals("date_end"))
				dateFinal = r.getFilterValue();
		}
		if(holderAccountCode != null)
			holderAccountNumber = accountComponentServiceBean.find(new Long(holderAccountCode),HolderAccount.class).getAccountNumber().toString();
		else
			holderAccountNumber = "TODOS";

		try {
			
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				mnemonic_sc.put(param.getParameterTablePk(), param.getText1());
			}
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		parametersRequired.put("p_mnemonic_sc", mnemonic_sc);
		// TODO Auto-generated method stub
		return parametersRequired;
		
	}

}
