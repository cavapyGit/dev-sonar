package com.pradera.report.generation.executor.corporative.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.corporateevents.to.CorporativeOperationSubQueryTO;
import com.pradera.core.component.corporateevents.to.CorporativeOperationTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.corporatives.stockcalculation.StockCalculationProcess;
import com.pradera.model.corporatives.stockcalculation.type.StockClassType;
import com.pradera.model.corporatives.stockcalculation.type.StockStateType;
import com.pradera.model.corporatives.stockcalculation.type.StockType;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.corporative.to.CorporativeProcesInterestTO;
import com.pradera.report.generation.executor.corporative.to.CorporativeProcessResultTO;
import com.pradera.report.generation.executor.corporative.to.EvidenceOfPerceivedIncomeTO;
import com.pradera.report.generation.executor.corporative.to.GenericCorporativesTO;
import com.pradera.report.generation.executor.corporative.to.StockCalculationProcessTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CorporativeReportServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 22-abr-2015
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class CorporativeReportServiceBean extends CrudDaoServiceBean{

  /**
   * Instantiates a new corporative report service bean.
   */
  public CorporativeReportServiceBean(){

  }
  
  /**
   * Gets the corporative payment header.
   *
   * @param corporativeFilter the corporative filter
   * @return the corporative payment header
   */
  public List<Object[]> getCorporativePaymentHeader(CorporativeProcesInterestTO corporativeFilter){
	  StringBuilder sbQuery = new StringBuilder();
	  Map<String,Object> mapParam = new HashMap<String,Object>();
	  sbQuery.append("select co.securities.issuer.mnemonic,co.securities.issuer.idIssuerPk,co.securities.issuer.businessName,co.securities.issuance.idIssuanceCodePk, ")//0,1,2,3
	  .append(" co.securities.idSecurityCodePk,co.securities.currentNominalValue,co.corporativeEventType.corporativeEventType, ")//4,5,6
	  .append(" co.currency,co.cutoffDate,co.registryDate,co.deliveryDate,co.indRound, ")//7,8,9,10,11
	  .append(" co.deliveryFactor,co.taxFactor,pic.couponNumber,pac.couponNumber,co.state,co.idCorporativeOperationPk,co.securities.currency, ")//12,13,14,15,16,17,18
	  .append(" (select sum(nvl(cpr.taxAmount,0)) from CorporativeProcessResult cpr where cpr.corporativeOperation.idCorporativeOperationPk = :processId) ")//19
	  .append("  from CorporativeOperation co left join co.programInterestCoupon pic ")
	  .append(" left join co.programAmortizationCoupon pac where ");
	  if(corporativeFilter.getProcessId()!=null){
		  sbQuery.append(" co.idCorporativeOperationPk = :processId ");
		  mapParam.put("processId", corporativeFilter.getProcessId());
	  }
	//if don't have operation number so to use will implement other filters like security,dates
	  TypedQuery<Object[]> query = em.createQuery(sbQuery.toString(), Object[].class);
	  Iterator<Entry<String, Object>> it = mapParam.entrySet().iterator();
	    // Iterating
	    while (it.hasNext()) {      
	      // Building the entry
	      Entry<String, Object> entry =  it.next();
	      // Setting Param
	      query.setParameter(entry.getKey(), entry.getValue());
	    }
	  return query.getResultList();
  }
  
  /**
   * Gets the corporative special header.
   *
   * @param corporativeFilter the corporative filter
   * @return the corporative special header
   */
  public List<Object[]> getCorporativeSpecialHeader(CorporativeProcesInterestTO corporativeFilter){
	  StringBuilder sbQuery = new StringBuilder();
	  Map<String,Object> mapParam = new HashMap<String,Object>();
	  sbQuery.append("select co.securities.issuer.mnemonic,co.securities.issuer.idIssuerPk,co.securities.issuer.businessName,co.securities.issuance.idIssuanceCodePk, ")//0,1,2,3
	  .append(" co.securities.idSecurityCodePk,co.securities.currentNominalValue,co.corporativeEventType.corporativeEventType, ")//4,5,6
	  .append(" co.currency,co.cutoffDate,co.registryDate,co.deliveryDate,co.indRound, ")//7,8,9,10,11
	  .append(" co.deliveryFactor,co.taxFactor,0,0,co.state,co.idCorporativeOperationPk,co.securities.currency, ")//12,13,14,15,16,17,18
	  .append(" (select sum(nvl(cpr.taxAmount,0)) from CorporativeProcessResult cpr where cpr.corporativeOperation.idCorporativeOperationPk = :processId), ")//19
	  .append(" (select description from CorporativeEventType where idCorporativeEventTypePk = co.corporativeEventType.idCorporativeEventTypePk) ")//20
	  .append("  from CorporativeOperation co  ")
	  .append("  where ");
	  if(corporativeFilter.getProcessId()!=null){
		  sbQuery.append(" co.idCorporativeOperationPk = :processId ");
		  mapParam.put("processId", corporativeFilter.getProcessId());
	  }
	//if don't have operation number so to use will implement other filters like security,dates
	  TypedQuery<Object[]> query = em.createQuery(sbQuery.toString(), Object[].class);
	  Iterator<Entry<String, Object>> it = mapParam.entrySet().iterator();
	    // Iterating
	    while (it.hasNext()) {      
	      // Building the entry
	      Entry<String, Object> entry =  it.next();
	      // Setting Param
	      query.setParameter(entry.getKey(), entry.getValue());
	    }
	  return query.getResultList();
  }
  /**
   * Gets the stock calculation number.
   *
   * @param stockFilter the stock filter
   * @return the stock calculation number
   */
  public Long getStockCalculationNumber(StockCalculationProcessTO stockFilter){
	  StringBuilder sbQuery = new StringBuilder();
	  Long stock = null;
	  sbQuery.append(" select scp.idStockCalculationPk from StockCalculationProcess scp ").
	  append(" where scp.security.idSecurityCodePk = :security ").
	  append(" and scp.cutoffDate = :cutDate and scp.registryDate = :registryDate ").
	  append(" and scp.stockType = :type and scp.stockClass = :class ").
	  append(" and scp.participant is null and scp.holder is null ");
	  TypedQuery<Long> query = em.createQuery(sbQuery.toString(), Long.class);
	  query.setParameter("security", stockFilter.getSecurity());
	  query.setParameter("cutDate", stockFilter.getCutOffDate());
	  query.setParameter("registryDate", stockFilter.getRegistryDate());
	  query.setParameter("type", stockFilter.getStockType());
	  query.setParameter("class", stockFilter.getStockClass());
	  try{
		  stock = query.getSingleResult();
	  } catch(NoResultException | NonUniqueResultException nux){
		  
	  }
	  return stock;
  }
  
  /**
   * Gets the stock control process.
   *
   * @param corporativeFilter the corporative filter
   * @return the stock control process
   */
  public List<Object[]> getStockControlProcess(CorporativeProcesInterestTO corporativeFilter,Object[] corporativeInformation){
	  Integer processType = new Integer(corporativeInformation[6].toString());
	  StringBuilder sbQuery = new StringBuilder();
	  sbQuery.append(" SELECT PARTICIPANT,SUM(AVAILABLE_STOCK),SUM(AVAILABLE_PROCESS),HOLDERS_STOCK,SUM(BLOCKED_STOCK),SUM(BLOCKED_PROCESS), ").
	  append(" HOLDERS_BLOCKED,SUM(TOTAL_STOCK),SUM(TOTAL_PROCESS)    ").
	  append(" from( SELECT SCB.ID_PARTICIPANT_FK PARTICIPANT,SCB.AVAILABLE_BALANCE AVAILABLE_STOCK,CPR.AVAILABLE_BALANCE AVAILABLE_PROCESS, ").
	  append(" (SELECT COUNT(DISTINCT(HAD.ID_HOLDER_ACCOUNT_FK)) FROM STOCK_CALCULATION_BALANCE SB,HOLDER_ACCOUNT HAA, holder_account_detail HAD  ").
	  append(" WHERE SB.ID_PARTICIPANT_FK = SCB.ID_PARTICIPANT_FK AND HAD.id_holder_account_fk=HAA.id_holder_account_pk  ").
	  append(" AND SB.ID_STOCK_CALCULATION_FK = SCB.ID_STOCK_CALCULATION_FK AND SB.ID_HOLDER_ACCOUNT_FK = HAA.ID_HOLDER_ACCOUNT_PK  ").
	  append(" AND SB.AVAILABLE_BALANCE > 0 ) HOLDERS_STOCK , ").
	  append(" (SCB.PAWN_BALANCE + SCB.BAN_BALANCE + SCB.OTHER_BLOCK_BALANCE +SCB.REPORTING_BALANCE) BLOCKED_STOCK, ").
	  append(" (CPR.PAWN_BALANCE + CPR.BAN_BALANCE + CPR.OTHER_BLOCK_BALANCE +CPR.REPORTING_BALANCE) BLOCKED_PROCESS, ").
	  append(" (SELECT COUNT(DISTINCT(HAD.ID_HOLDER_ACCOUNT_FK)) FROM STOCK_CALCULATION_BALANCE SB,HOLDER_ACCOUNT HAA, holder_account_detail HAD ").
	  append(" WHERE SB.ID_PARTICIPANT_FK = SCB.ID_PARTICIPANT_FK AND HAD.id_holder_account_fk=HAA.id_holder_account_pk  ").
	  append(" AND SB.ID_STOCK_CALCULATION_FK = SCB.ID_STOCK_CALCULATION_FK AND SB.ID_HOLDER_ACCOUNT_FK = HAA.ID_HOLDER_ACCOUNT_PK  ").
	  append(" AND (SB.PAWN_BALANCE > 0 or SB.BAN_BALANCE > 0 or SB.OTHER_BLOCK_BALANCE > 0 or SB.REPORTING_BALANCE > 0) ) AS HOLDERS_BLOCKED, ").
	  append(" SCB.TOTAL_BALANCE TOTAL_STOCK, ").
	  append(" CPR.TOTAL_BALANCE TOTAL_PROCESS ").
	  append(" from CORPORATIVE_PROCESS_RESULT CPR,STOCK_CALCULATION_BALANCE SCB,CORPORATIVE_OPERATION co  ").
	  append(" where  CPR.ID_CORPORATIVE_OPERATION_FK = co.ID_CORPORATIVE_OPERATION_PK ").
	  append(" and SCB.ID_HOLDER_ACCOUNT_FK = cpr.ID_HOLDER_ACCOUNT_FK ").
	  append(" and SCB.ID_PARTICIPANT_FK = cpr.ID_PARTICIPANT_FK ");
	  if(!processType.equals(ImportanceEventType.PREFERRED_SUSCRIPTION.getCode())){
		  sbQuery.append(" and SCB.ID_SECURITY_CODE_FK = CPR.ID_SECURITY_CODE_FK ");
	  }	
	  sbQuery.append(" and CPR.ID_CORPORATIVE_OPERATION_FK  = :corporative and SCB.ID_STOCK_CALCULATION_FK = :stock ").
	  append(" and (:state is null or co.state = :state) ").
	  append(" AND SCB.ID_PARTICIPANT_FK = CPR.ID_PARTICIPANT_FK AND CPR.IND_ORIGIN_TARGET = 2 AND (SCB.TOTAL_BALANCE + SCB.REPORTED_BALANCE) > 0 ").
	  append(" ) GROUP BY PARTICIPANT,HOLDERS_STOCK,HOLDERS_BLOCKED ORDER BY PARTICIPANT ");
	  Query query = em.createNativeQuery(sbQuery.toString());
	  query.setParameter("corporative", corporativeFilter.getProcessId());
	  query.setParameter("stock", corporativeFilter.getStockId());
	  query.setParameter("state", corporativeFilter.getState());
	  return query.getResultList();
  }
  
  /**
   * Gets the stock special control process.
   *
   * @param corporativeFilter the corporative filter
   * @param corporativeInformation the corporative information
   * @return the stock special control process
   */
  public List<Object[]> getStockSpecialControlProcess(CorporativeProcesInterestTO corporativeFilter,Object[] corporativeInformation){
	  StringBuilder sbQuery = new StringBuilder();
	  sbQuery.append(" SELECT PARTICIPANT,SUM(AVAILABLE_STOCK),SUM(AVAILABLE_PROCESS),HOLDERS_STOCK,SUM(BLOCKED_STOCK),SUM(BLOCKED_PROCESS), ").
	  append(" HOLDERS_BLOCKED,SUM(TOTAL_STOCK),SUM(TOTAL_PROCESS)    ").
	  append(" from( SELECT cpro.ID_PARTICIPANT_FK PARTICIPANT,cpro.AVAILABLE_BALANCE AVAILABLE_STOCK,CPR.AVAILABLE_BALANCE AVAILABLE_PROCESS, ").
	  append(" (SELECT COUNT(DISTINCT(HAD.id_holder_fk)) FROM CORPORATIVE_PROCESS_RESULT SB,HOLDER_ACCOUNT HAA, holder_account_detail HAD  ").
	  append(" WHERE SB.ID_PARTICIPANT_FK = cpro.ID_PARTICIPANT_FK AND HAD.id_holder_account_fk=HAA.id_holder_account_pk  ").
	  append(" AND SB.ID_CORPORATIVE_OPERATION_FK = cpro.ID_CORPORATIVE_OPERATION_FK AND SB.ID_HOLDER_ACCOUNT_FK = HAA.ID_HOLDER_ACCOUNT_PK  ").
	  append(" AND SB.AVAILABLE_BALANCE > 0 ) HOLDERS_STOCK ,  ").
	  append(" (cpro.PAWN_BALANCE + cpro.BAN_BALANCE + cpro.OTHER_BLOCK_BALANCE +cpro.REPORTING_BALANCE) BLOCKED_STOCK,  ").
	  append(" (CPR.PAWN_BALANCE + CPR.BAN_BALANCE + CPR.OTHER_BLOCK_BALANCE +CPR.REPORTING_BALANCE) BLOCKED_PROCESS,  ").
	  append(" (SELECT COUNT(DISTINCT(HAD.id_holder_fk)) FROM CORPORATIVE_PROCESS_RESULT SB,HOLDER_ACCOUNT HAA, holder_account_detail HAD  ").
	  append(" WHERE SB.ID_PARTICIPANT_FK = cpro.ID_PARTICIPANT_FK AND HAD.id_holder_account_fk=HAA.id_holder_account_pk  ").
	  append(" AND SB.ID_CORPORATIVE_OPERATION_FK = cpro.ID_CORPORATIVE_OPERATION_FK AND SB.ID_HOLDER_ACCOUNT_FK = HAA.ID_HOLDER_ACCOUNT_PK   ").
	  append("  AND (SB.PAWN_BALANCE > 0 or SB.BAN_BALANCE > 0 or SB.OTHER_BLOCK_BALANCE > 0 or SB.REPORTING_BALANCE > 0) ) AS HOLDERS_BLOCKED,  ").
	  append(" cpro.TOTAL_BALANCE TOTAL_STOCK,  ").
	  append(" CPR.TOTAL_BALANCE TOTAL_PROCESS  ").
	  append(" from ").
	  append("  CORPORATIVE_PROCESS_RESULT cpro, HOLDER_ACCOUNT ha, PARTICIPANT p, ").
	  append("  CORPORATIVE_OPERATION co, ISSUER iss, SECURITY s,SECURITY saux,CORPORATIVE_PROCESS_RESULT cpr ").
	  append("  where ").
	  append("        cpro.ID_CORPORATIVE_OPERATION_FK = co.ID_CORPORATIVE_OPERATION_PK ").
	  append("       and cpro.ID_CORPORATIVE_OPERATION_FK= :corporative ").
	  append("       and cpr.ID_CORPORATIVE_OPERATION_FK= :corporative ").
      append("       and cpro.ID_PARTICIPANT_FK=cpr.ID_PARTICIPANT_FK ").
	  append("        and cpro.ID_HOLDER_ACCOUNT_FK=cpr.ID_HOLDER_ACCOUNT_FK ").
	  append("        and cpro.ID_HOLDER_ACCOUNT_FK=ha.ID_HOLDER_ACCOUNT_PK ").
	  append("       and cpro.ID_PARTICIPANT_FK=p.ID_PARTICIPANT_PK ").
      append("         and cpro.ID_SECURITY_CODE_FK = s.ID_SECURITY_CODE_PK ").
	  append("       and cpr.ID_SECURITY_CODE_FK = saux.ID_SECURITY_CODE_PK ").
      append("        and s.ID_ISSUER_FK = iss.ID_ISSUER_PK ").
	  append("         and cpr.IND_REMANENT=0 ").
	  append("        and cpro.IND_ORIGIN_TARGET=1 ").
	  append("        and cpro.IND_REMANENT=0 ").
      append("        and cpr.IND_ORIGIN_TARGET=2 ").
      append("         and (:state is null or co.state = :state) ").
	  append("         and (:eventType != 13 and co.CORPORATIVE_EVENT_TYPE = :eventType) ").
	  append("        and saux.ID_SECURITY_CODE_PK = :securityCode ").
	  append("        AND (cpro.TOTAL_BALANCE + cpro.REPORTING_BALANCE)>0 ").
      append("   UNION ").
      append(" SELECT cpro.ID_PARTICIPANT_FK PARTICIPANT,cpro.AVAILABLE_BALANCE AVAILABLE_STOCK,0 AVAILABLE_PROCESS, ").
      append(" (SELECT COUNT(DISTINCT(HAD.id_holder_fk)) FROM CORPORATIVE_PROCESS_RESULT SB,HOLDER_ACCOUNT HAA, holder_account_detail HAD  ").
      append(" WHERE SB.ID_PARTICIPANT_FK = cpro.ID_PARTICIPANT_FK AND HAD.id_holder_account_fk=HAA.id_holder_account_pk  ").
      append("  AND SB.ID_CORPORATIVE_OPERATION_FK = cpro.ID_CORPORATIVE_OPERATION_FK AND SB.ID_HOLDER_ACCOUNT_FK = HAA.ID_HOLDER_ACCOUNT_PK  ").
      append(" AND SB.AVAILABLE_BALANCE > 0 ) HOLDERS_STOCK ,  ").
      append(" (cpro.PAWN_BALANCE + cpro.BAN_BALANCE + cpro.OTHER_BLOCK_BALANCE +cpro.REPORTING_BALANCE) BLOCKED_STOCK,  ").
      append(" 0 BLOCKED_PROCESS,  ").
      append(" (SELECT COUNT(DISTINCT(HAD.id_holder_fk)) FROM CORPORATIVE_PROCESS_RESULT SB,HOLDER_ACCOUNT HAA, holder_account_detail HAD  ").
      append("  WHERE SB.ID_PARTICIPANT_FK = cpro.ID_PARTICIPANT_FK AND HAD.id_holder_account_fk=HAA.id_holder_account_pk  ").
      append("  AND SB.ID_CORPORATIVE_OPERATION_FK = cpro.ID_CORPORATIVE_OPERATION_FK AND SB.ID_HOLDER_ACCOUNT_FK = HAA.ID_HOLDER_ACCOUNT_PK   ").
      append(" AND (SB.PAWN_BALANCE > 0 or SB.BAN_BALANCE > 0 or SB.OTHER_BLOCK_BALANCE > 0 or SB.REPORTING_BALANCE > 0) ) AS HOLDERS_BLOCKED,  ").
      append(" cpro.TOTAL_BALANCE TOTAL_STOCK,  ").
      append(" 0 TOTAL_PROCESS   ").
      append(" from ").
      append("  CORPORATIVE_PROCESS_RESULT cpro, HOLDER_ACCOUNT ha, PARTICIPANT p, ").
      append("  CORPORATIVE_OPERATION co, ISSUER iss, SECURITY s,SECURITY saux ").
      append(" where ").
      append("        cpro.ID_CORPORATIVE_OPERATION_FK = co.ID_CORPORATIVE_OPERATION_PK ").
      append("        and cpro.ID_CORPORATIVE_OPERATION_FK= :corporative ").
      append("        and cpro.ID_HOLDER_ACCOUNT_FK=ha.ID_HOLDER_ACCOUNT_PK ").
      append("         and cpro.ID_PARTICIPANT_FK=p.ID_PARTICIPANT_PK ").
      append("         and cpro.ID_SECURITY_CODE_FK = s.ID_SECURITY_CODE_PK ").
      append("         and cpro.ID_SECURITY_CODE_FK = saux.ID_SECURITY_CODE_PK ").
      append("         and s.ID_ISSUER_FK = iss.ID_ISSUER_PK ").
      append("         and cpro.IND_ORIGIN_TARGET=1 ").
      append("         and cpro.IND_REMANENT=0 ").
      append("         and (:state is null or co.state = :state) ").
      append("          and (:eventType = 13 and co.CORPORATIVE_EVENT_TYPE = :eventType) ").
      append("          and saux.ID_SECURITY_CODE_PK = :securityCode ").
      append("          AND (cpro.TOTAL_BALANCE + cpro.REPORTING_BALANCE)>0 ").
      append("   ) GROUP BY PARTICIPANT,HOLDERS_STOCK,HOLDERS_BLOCKED ORDER BY PARTICIPANT ");	  
	  
	  Query query = em.createNativeQuery(sbQuery.toString());
	  query.setParameter("corporative", corporativeFilter.getProcessId());
	  query.setParameter("securityCode", corporativeFilter.getSecurityCode());
	  query.setParameter("eventType", corporativeFilter.getEventType());
	  query.setParameter("state", corporativeFilter.getState());
	  return query.getResultList();
  }
  
  /**
   * Gets the stock blocked process.
   *
   * @param corporativeFilter the corporative filter
   * @return the stock blocked process
   */
  public List<Object[]> getStockBlockedProcess(CorporativeProcesInterestTO corporativeFilter,Object[] corporativeInformation){
	  Integer processType = new Integer(corporativeInformation[6].toString());
	  StringBuilder sbQuery = new StringBuilder();
	  sbQuery.append(" SELECT DISTINCT SCB.ID_PARTICIPANT_FK,PA.MNEMONIC,PA.DESCRIPTION, ").
	  append(" HA.ID_HOLDER_ACCOUNT_PK,HA.ACCOUNT_NUMBER, ").
	  append(" FN_GET_HOLDER_NAME(ha.ID_HOLDER_ACCOUNT_PK) CUI, ").
	  append(" SCB.PAWN_BALANCE -        nvl((SELECT SUM(BLOCK_BALANCE) FROM BLOCK_STOCK_CALCULATION where ID_STOCK_CALCULATION_FK = SCB.ID_STOCK_CALCULATION_FK and BLOCK_TYPE = 1115 and (decode(co.CORPORATIVE_EVENT_TYPE,14,IND_INTEREST,decode(co.CORPORATIVE_EVENT_TYPE,15,IND_AMORTIZATION,decode(co.CORPORATIVE_EVENT_TYPE,2,IND_CASH_DIVIDEND,IND_STOCK_DIVIDEND)))= 0)),0) PAWN_BALANCE, ").
	  append(" SCB.BAN_BALANCE -         nvl((SELECT SUM(BLOCK_BALANCE) FROM BLOCK_STOCK_CALCULATION where ID_STOCK_CALCULATION_FK = SCB.ID_STOCK_CALCULATION_FK and BLOCK_TYPE = 1117 and (decode(co.CORPORATIVE_EVENT_TYPE,14,IND_INTEREST,decode(co.CORPORATIVE_EVENT_TYPE,15,IND_AMORTIZATION,decode(co.CORPORATIVE_EVENT_TYPE,2,IND_CASH_DIVIDEND,IND_STOCK_DIVIDEND)))= 0)),0) BAN_BALANCE, ").
	  append(" SCB.OTHER_BLOCK_BALANCE - nvl((SELECT SUM(BLOCK_BALANCE) FROM BLOCK_STOCK_CALCULATION where ID_STOCK_CALCULATION_FK = SCB.ID_STOCK_CALCULATION_FK and BLOCK_TYPE = 1118 and (decode(co.CORPORATIVE_EVENT_TYPE,14,IND_INTEREST,decode(co.CORPORATIVE_EVENT_TYPE,15,IND_AMORTIZATION,decode(co.CORPORATIVE_EVENT_TYPE,2,IND_CASH_DIVIDEND,IND_STOCK_DIVIDEND)))= 0)),0) OTHER_BLOCK_BALANCE, ").
	  append(" (SCB.PAWN_BALANCE+SCB.BAN_BALANCE+SCB.OTHER_BLOCK_BALANCE) - nvl((SELECT SUM(BLOCK_BALANCE) FROM BLOCK_STOCK_CALCULATION where ID_STOCK_CALCULATION_FK = SCB.ID_STOCK_CALCULATION_FK and BLOCK_TYPE in(1115,1117,1118) and (decode(co.CORPORATIVE_EVENT_TYPE,14,IND_INTEREST,decode(co.CORPORATIVE_EVENT_TYPE,15,IND_AMORTIZATION,decode(co.CORPORATIVE_EVENT_TYPE,2,IND_CASH_DIVIDEND,IND_STOCK_DIVIDEND)))= 0)),0) total_stock, ").
	  append(" CPR.PAWN_BALANCE PB,CPR.BAN_BALANCE BB,CPR.OTHER_BLOCK_BALANCE OB, ").
	  append(" (CPR.PAWN_BALANCE+CPR.BAN_BALANCE+CPR.OTHER_BLOCK_BALANCE) total_proc ").
	  append(" from CORPORATIVE_PROCESS_RESULT CPR,STOCK_CALCULATION_BALANCE SCB,PARTICIPANT PA,HOLDER_ACCOUNT HA ,CORPORATIVE_OPERATION co ").
	  append(" where  CPR.ID_CORPORATIVE_OPERATION_FK = co.ID_CORPORATIVE_OPERATION_PK ").
	  append(" AND CPR.ID_CORPORATIVE_OPERATION_FK  = :corporative and SCB.ID_STOCK_CALCULATION_FK = :stock ");
	  if(!processType.equals(ImportanceEventType.PREFERRED_SUSCRIPTION.getCode())){
		  sbQuery.append(" AND CPR.ID_SECURITY_CODE_FK = SCB.ID_SECURITY_CODE_FK ");
	  }		  
	  sbQuery.append(" AND CPR.ID_HOLDER_ACCOUNT_FK = SCB.ID_HOLDER_ACCOUNT_FK ").
	  append(" AND CPR.ID_PARTICIPANT_FK = SCB.ID_PARTICIPANT_FK ").
	  append(" AND PA.ID_PARTICIPANT_PK = SCB.ID_PARTICIPANT_FK ").
	  append(" AND HA.ID_HOLDER_ACCOUNT_PK = CPR.ID_HOLDER_ACCOUNT_FK ").
	  append(" AND CPR.IND_ORIGIN_TARGET = 2  ").
	  append(" AND nvl(CPR.IND_REMANENT,0) = 0  ").
	  append(" and (:state is null or co.state = :state) ").
	  append(" AND (SCB.PAWN_BALANCE >0 OR SCB.BAN_BALANCE>0 OR SCB.OTHER_BLOCK_BALANCE>0 ) ").
	  append(" order by PA.MNEMONIC ");
	  Query query = em.createNativeQuery(sbQuery.toString());
	  query.setParameter("corporative", corporativeFilter.getProcessId());
	  query.setParameter("stock", corporativeFilter.getStockId());
	  query.setParameter("state", corporativeFilter.getState());
	  return query.getResultList();
  }
  
  /**
   * Gets the stock special blocked process.
   *
   * @param corporativeFilter the corporative filter
   * @param corporativeInformation the corporative information
   * @return the stock special blocked process
   */
  public List<Object[]> getStockSpecialBlockedProcess(CorporativeProcesInterestTO corporativeFilter,Object[] corporativeInformation){
	  StringBuilder sbQuery = new StringBuilder();
	  sbQuery.append(" SELECT DISTINCT cpro.ID_PARTICIPANT_FK,P.MNEMONIC PARTICIPANTE,P.DESCRIPTION,  ").
	  append(" HA.ID_HOLDER_ACCOUNT_PK,HA.ACCOUNT_NUMBER, ").
	  append(" FN_GET_HOLDER_NAME(ha.ID_HOLDER_ACCOUNT_PK) CUI, ").
	  append(" cpro.PAWN_BALANCE PAWN_BALANCE, ").
	  append(" cpro.BAN_BALANCE BAN_BALANCE,  ").
	  append(" cpro.OTHER_BLOCK_BALANCE OTHER_BLOCK_BALANCE, "). 
	  append(" (cpro.PAWN_BALANCE+cpro.BAN_BALANCE+cpro.OTHER_BLOCK_BALANCE) total_stock, "). 
	  append(" CPR.PAWN_BALANCE PB,CPR.BAN_BALANCE BB,CPR.OTHER_BLOCK_BALANCE OB,  ").
	  append(" (CPR.PAWN_BALANCE+CPR.BAN_BALANCE+CPR.OTHER_BLOCK_BALANCE) total_proc  ").
	  append(" from ").
	  append("  CORPORATIVE_PROCESS_RESULT cpro, HOLDER_ACCOUNT ha, PARTICIPANT p, ").
	  append("   CORPORATIVE_OPERATION co, SECURITY s,SECURITY saux,CORPORATIVE_PROCESS_RESULT cpr ").
      append(" where ").
      append("        cpro.ID_CORPORATIVE_OPERATION_FK = co.ID_CORPORATIVE_OPERATION_PK ").
      append("        and cpro.ID_CORPORATIVE_OPERATION_FK= :corporative ").
      append("         and cpr.ID_CORPORATIVE_OPERATION_FK= :corporative ").
      append("         and cpro.ID_PARTICIPANT_FK=cpr.ID_PARTICIPANT_FK ").
      append("         and cpro.ID_HOLDER_ACCOUNT_FK=cpr.ID_HOLDER_ACCOUNT_FK ").
      append("         and cpro.ID_HOLDER_ACCOUNT_FK=ha.ID_HOLDER_ACCOUNT_PK ").
      append("          and cpro.ID_PARTICIPANT_FK=p.ID_PARTICIPANT_PK ").
      append("          and cpro.ID_SECURITY_CODE_FK = s.ID_SECURITY_CODE_PK ").
      append("          and cpr.ID_SECURITY_CODE_FK = saux.ID_SECURITY_CODE_PK ").
      append("          and cpr.IND_REMANENT=0 ").
      append("         and cpro.IND_ORIGIN_TARGET=1 ").
      append("         and cpro.IND_REMANENT=0 ").
      append("         and cpr.IND_ORIGIN_TARGET=2 ").
      append("         and (:state is null or co.state = :state) ").
      append("         and (:eventType != 13 and co.CORPORATIVE_EVENT_TYPE = :eventType) ").
      append("         and saux.ID_SECURITY_CODE_PK = :securityCode ").
      append("        AND (cpro.PAWN_BALANCE >0 OR cpro.BAN_BALANCE>0 OR cpro.OTHER_BLOCK_BALANCE>0 ) ").
      append(" UNION ").
      append(" SELECT DISTINCT cpro.ID_PARTICIPANT_FK,P.MNEMONIC PARTICIPANTE,P.DESCRIPTION, ").
      append(" HA.ID_HOLDER_ACCOUNT_PK,HA.ACCOUNT_NUMBER, ").
      append(" FN_GET_HOLDER_NAME(ha.ID_HOLDER_ACCOUNT_PK) CUI, ").
      append(" cpro.PAWN_BALANCE PAWN_BALANCE, ").
      append(" cpro.BAN_BALANCE BAN_BALANCE,  ").
      append(" cpro.OTHER_BLOCK_BALANCE OTHER_BLOCK_BALANCE, "). 
      append("  (cpro.PAWN_BALANCE+cpro.BAN_BALANCE+cpro.OTHER_BLOCK_BALANCE) total_stock, "). 
      append("   0 PB,0 BB,0 OB,  ").
      append("  0 total_proc   ").
      append("  from ").
      append("   CORPORATIVE_PROCESS_RESULT cpro, HOLDER_ACCOUNT ha, PARTICIPANT p, ").
      append("   CORPORATIVE_OPERATION co, ISSUER iss, SECURITY s,SECURITY saux ").
      append("  where ").
      append("         cpro.ID_CORPORATIVE_OPERATION_FK = co.ID_CORPORATIVE_OPERATION_PK ").
      append("       and cpro.ID_CORPORATIVE_OPERATION_FK= :corporative ").
      append("        and cpro.ID_HOLDER_ACCOUNT_FK=ha.ID_HOLDER_ACCOUNT_PK ").
      append("         and cpro.ID_PARTICIPANT_FK=p.ID_PARTICIPANT_PK ").
      append("        and cpro.ID_SECURITY_CODE_FK = s.ID_SECURITY_CODE_PK ").
      append("        and cpro.ID_SECURITY_CODE_FK = saux.ID_SECURITY_CODE_PK ").
      append("       and s.ID_ISSUER_FK = iss.ID_ISSUER_PK ").
	  append("        and cpro.IND_ORIGIN_TARGET=1 ").
	  append("         and cpro.IND_REMANENT=0 ").
	  append("         and (:state is null or co.state = :state) ").
	  append("         and (:eventType = 13 and co.CORPORATIVE_EVENT_TYPE = :eventType) ").
	  append("          and saux.ID_SECURITY_CODE_PK = :securityCode ").
	  append("         AND (cpro.PAWN_BALANCE >0 OR cpro.BAN_BALANCE>0 OR cpro.OTHER_BLOCK_BALANCE>0 ) ").
	  append("   order by PARTICIPANTE ");
	  
	  Query query = em.createNativeQuery(sbQuery.toString());
	  query.setParameter("corporative", corporativeFilter.getProcessId());
	  query.setParameter("securityCode", corporativeFilter.getSecurityCode());
	  query.setParameter("eventType", corporativeFilter.getEventType());
	  query.setParameter("state", corporativeFilter.getState());
	  return query.getResultList();
  }
  /**
   * Gets the stock detail balance blocked process.
   *
   * @param corporativeProcess the corporative process
   * @param holderAccount the holder account
   * @param corporativeType the corporative type
   * @return the stock detail balance blocked process
   */
  public List<Object[]> getStockDetailBalanceBlockedProcess(Long corporativeProcess,Long holderAccount,Integer corporativeType){
	  StringBuilder sbQuery = new StringBuilder();
	  sbQuery.append(" select bo.DOCUMENT_NUMBER,br.BLOCK_NUMBER nr_cite,br.BLOCK_TYPE,bo.ACTUAL_BLOCK_BALANCE,bcr.BLOCKED_BALANCE ").
	  append(" ,be.FULL_NAME,bcr.IND_ORIGIN_DESTINY,bcr.ID_CORPORATIVE_PROCESS_RESULT ").
	  append(" from  BLOCK_CORPORATIVE_RESULT bcr join BLOCK_OPERATION bo on (bcr.ID_BLOCK_OPERATION_FK=bo.ID_BLOCK_OPERATION_PK)  ").
	  append(" join block_request br on (bo.ID_BLOCK_REQUEST_FK=br.ID_BLOCK_REQUEST_PK)   ").
	  append(" join BLOCK_ENTITY be on (br.ID_BLOCK_ENTITY_FK=be.ID_BLOCK_ENTITY_PK) ").
	  append(" where bcr.ID_CORPORATIVE_OPERATION_FK= :corporative and bcr.ID_HOLDER_ACCOUNT_FK= :account ");
	  if(corporativeType.equals(ImportanceEventType.CAPITAL_AMORTIZATION.getCode())){
		sbQuery.append(" and bcr.IND_ORIGIN_DESTINY ="+ReportConstant.BALANCE_DESTINY);  
		sbQuery.append(" and bcr.IND_AMORTIZATION ="+BooleanType.YES.getCode());
	  }else if(corporativeType.equals(ImportanceEventType.INTEREST_PAYMENT.getCode())){
		  sbQuery.append(" and bcr.IND_INTEREST ="+BooleanType.YES.getCode());
	  }else if(corporativeType.equals(ImportanceEventType.CASH_DIVIDENDS.getCode())){
		  sbQuery.append(" and bcr.IND_CASH_DIVIDEND ="+BooleanType.YES.getCode());
	  }else if(corporativeType.equals(ImportanceEventType.ACTION_DIVIDENDS.getCode())){
		  sbQuery.append(" and bcr.IND_STOCK_DIVIDEND ="+BooleanType.YES.getCode());
		  sbQuery.append(" and bcr.IND_REMANENT ="+BooleanType.NO.getCode()); 
	  }else if(corporativeType.equals(ImportanceEventType.SECURITIES_EXCLUSION.getCode())
				||corporativeType.equals(ImportanceEventType.CONVERTIBILY_BONE_TO_ACTION.getCode())
				||corporativeType.equals(ImportanceEventType.CHANGE_NOMINAL_VALUE_NO_CAP_VAR.getCode())
				||corporativeType.equals(ImportanceEventType.CHANGE_NOMAL_VALUE_CAP_VAR.getCode())
				||corporativeType.equals(ImportanceEventType.SECURITIES_FUSION.getCode())
				||corporativeType.equals(ImportanceEventType.SECURITIES_UNIFICATION.getCode())
				||corporativeType.equals(ImportanceEventType.SECURITIES_EXCISION.getCode())
				||corporativeType.equals(ImportanceEventType.CAPITAL_REDUCTION.getCode())){
			sbQuery.append(" and bcr.IND_ORIGIN_DESTINY ="+ReportConstant.BALANCE_ORIGIN);  
	  }
	  Query query = em.createNativeQuery(sbQuery.toString());
	  query.setParameter("corporative", corporativeProcess);
	  query.setParameter("account", holderAccount);
	  return query.getResultList();
  }
  
  /**
   * Validate exchange.
   *
   * @param lstResultPk the lst result pk
   * @return true, if successful
   */
  public boolean validateExchange(List<Long> lstResultPk){
	  boolean validate = false;
	  Object obj = null;
	  StringBuilder sbQuery = new StringBuilder();
	  sbQuery.append(" select SUM(nvl(cpr.EXCHANGE_AMOUNT,0)) from CORPORATIVE_PROCESS_RESULT cpr where cpr.ID_CORPORATIVE_PROCESS_RESULT in (:lstResult)");
	  Query query = em.createNativeQuery(sbQuery.toString());
	  query.setParameter("lstResult", lstResultPk);
	  obj = query.getSingleResult();
	  if(obj!=null){
		  BigDecimal objSum = (BigDecimal)obj;
		  if(objSum.compareTo(BigDecimal.ZERO)==1){
			  validate = true;
		  }else{
			  validate = false;
		  }
	  }
	
	  return validate;
  }
  /**
   * Gets the benefits control delivered investors info.
   *
   * @param corporativeProcessResultTO the corporative process result to
   * @return the benefits control delivered investors info
   * @throws ServiceException the service exception
   */
  public List<Object[]> getBenefitsControlDeliveredInvestorsInfo(CorporativeProcessResultTO corporativeProcessResultTO) throws ServiceException{
    StringBuilder sbQuery = new StringBuilder();
    Map<String,Object> mapParam = new HashMap<String,Object>();

    sbQuery.append("    SELECT " );
    sbQuery.append("           DISTINCT CPR.idCorporativeProcessResult,");
    sbQuery.append("           PA.idParticipantPk," );
    sbQuery.append("           PA.mnemonic," );
    sbQuery.append("           HA.idHolderAccountPk," );
    sbQuery.append("           HA.accountNumber," );
    sbQuery.append("           CO.corporativeEventType," );
    sbQuery.append("           SE.idIsinCodePk," );
    sbQuery.append("           CO.deliveryFactor," );
    sbQuery.append("           CPR.totalBalance," );
    sbQuery.append("           CO.deliveryDate," );
    sbQuery.append("           CO.currency," );
    sbQuery.append("           CPR.indOriginTarget,");
    sbQuery.append("       	   CO.registryDate,");
    sbQuery.append("           CPR.custodyAmount,");
    sbQuery.append("           CPR.taxAmount,");
    sbQuery.append("           CPR.penalityAmount,");
    sbQuery.append("           HO.idHolderPk," );
    sbQuery.append("           HO.fullName," ); 
    sbQuery.append("           HO.documentType," );
    sbQuery.append("           HO.documentNumber," );
    sbQuery.append("           HO.legalAddress" );
    sbQuery.append("      FROM CorporativeProcessResult CPR" );
    sbQuery.append("      JOIN CPR.participant PA");
    sbQuery.append("      JOIN CPR.holderAccount HA");
    sbQuery.append("      JOIN HA.holderAccountDetails HAD");
    sbQuery.append("      JOIN HAD.holder HO");
    sbQuery.append("      JOIN CPR.corporativeOperation CO");
    sbQuery.append("      JOIN CPR.security SE");
    sbQuery.append("     WHERE CPR.indRemanent=0");

    if(Validations.validateIsNotNull(corporativeProcessResultTO.getRntCode())){
      sbQuery.append(" AND HAD.holder.idHolderPk =:idHolderPk");
      mapParam.put("idHolderPk", corporativeProcessResultTO.getRntCode());
    }
    if(Validations.validateIsNotNull(corporativeProcessResultTO.getIndOriginTarget())){
      sbQuery.append(" AND CPR.indOriginTarget=:indOriginTarget");
      mapParam.put("indOriginTarget", corporativeProcessResultTO.getIndOriginTarget());
    }
    if(Validations.validateIsNotNull(corporativeProcessResultTO.getAccountNumber())){
      sbQuery.append(" AND HA.accountNumber =:accountNumber");
      mapParam.put("accountNumber", corporativeProcessResultTO.getAccountNumber());
    }
    if(Validations.validateIsNotNull(corporativeProcessResultTO.getIsinCode())){
      sbQuery.append(" AND SE.idIsinCodeFk =:idIsinCodeFk");
      mapParam.put("idIsinCodeFk", corporativeProcessResultTO.getIsinCode());
    }
    if(Validations.validateIsNotNull(corporativeProcessResultTO.getBenefitType())){
      sbQuery.append(" AND CO.corporativeEventType =:benefitType");
      mapParam.put("benefitType", corporativeProcessResultTO.getBenefitType());
    }
    if(Validations.validateIsNotNull(corporativeProcessResultTO.getBenefitList())){
      sbQuery.append(" AND CO.corporativeEventType in (:benefitTypeLst)");
      mapParam.put("benefitTypeLst", corporativeProcessResultTO.getBenefitList());
    }
    if(Validations.validateIsNotNull(corporativeProcessResultTO.getParticipant())){
      sbQuery.append(" AND PA.idParticipantPk =:idParticipantPk");
      mapParam.put("idParticipantPk", corporativeProcessResultTO.getParticipant());
    }

    if(Validations.validateIsNotNull(corporativeProcessResultTO.getInitialDate()) && Validations.validateIsNotNull(corporativeProcessResultTO.getFinalDate()) ){
      sbQuery.append("     AND CO.creationDate >=:initialDate");
      sbQuery.append("     AND CO.creationDate <=:endDate");
      mapParam.put("initialDate",corporativeProcessResultTO.getInitialDate());
      mapParam.put("endDate",corporativeProcessResultTO.getFinalDate());
    }
    sbQuery.append("  order by HO.idHolderPk");
    //CREATING QUERY
    Query query = em.createQuery(sbQuery.toString());         
    // Building Itarator
    @SuppressWarnings("rawtypes")
    Iterator it = mapParam.entrySet().iterator();
    // Iterating
    while (it.hasNext()) {      
      // Building the entry
      Entry<String, Object> entry = (Entry<String, Object>) it.next();
      // Setting Param
      query.setParameter(entry.getKey(), entry.getValue());
    }
    return query.getResultList();
  }
  /**
   * Search holders.
   *
   * @param idHolderAccountPk the id holder account pk
   * @return the list
   * @throws ServiceException the service exception
   */
  public List<Holder> searchHolders(Long idHolderAccountPk) throws ServiceException{
    StringBuilder sbQuery = new StringBuilder();
    Map<String,Object> mapParam = new HashMap<String,Object>();
    sbQuery.append("  SELECT " ); 
    sbQuery.append("         HAD.holder.idHolderPk," );
    sbQuery.append("         HAD.holder.fullName," ); 
    sbQuery.append("         HAD.holder.documentType," );
    sbQuery.append("         HAD.holder.documentNumber," );
    sbQuery.append("         HAD.holder.legalAddress" );
    sbQuery.append("   FROM  HolderAccountDetail HAD");
    sbQuery.append("  WHERE  HAD.holderAccount.idHolderAccountPk=:idHolderAccountPk");

    Query query = em.createQuery(sbQuery.toString());
    query.setParameter("idHolderAccountPk", idHolderAccountPk);

    List<Object> objects =(List<Object>) query.getResultList(); 
    List<Holder> holders = new ArrayList<Holder>();
    for(int i=0;i<objects.size();++i){
      Object [] object = (Object[]) objects.get(i);           
      Holder holder = new Holder();

      holder.setIdHolderPk((Long)object[0]);
      holder.setFullName((String)object[1]);
      holder.setDocumentType((Integer)object[2]);
      holder.setDocumentNumber((String)object[3]);
      holder.setLegalAddress((String)object[4]);
      holders.add(holder);
    }
    return holders;
  }



  /**
   * Search balance by corporative for report and margin.
   *
   * @param filter the filter
   * @return the list
   */
  public List<Object[]> searchBalanceByCorporativeForReportAndMargin(CorporativeOperationTO filter){
    StringBuilder sbQuery = new StringBuilder();

    sbQuery.append("SELECT DISTINCT ");
    sbQuery.append("  ha.account_number,");
    sbQuery.append("  h.holder_type,");
    sbQuery.append("  h.id_holder_pk rnt,");
    sbQuery.append("  h.full_name full_name,");
    sbQuery.append("  substr(ha.ALTERNATE_CODE, instr(ha.ALTERNATE_CODE,'('), 1+instr(ha.ALTERNATE_CODE,  ')')-instr(ha.ALTERNATE_CODE, '(')) as alternate_code,");
    sbQuery.append("  part.id_participant_pk|| ' - ' || part.mnemonic  participant, ");
    sbQuery.append("  part.id_participant_pk participant_id,");
    sbQuery.append("  scb.TOTAL_BALANCE SALDO_CONTABLE, ");
    sbQuery.append("  scb.REPORTING_BALANCE SALDO_REPORTE,");
    sbQuery.append("  scb.MARGIN_BALANCE SALDO_MARGEN,");
    sbQuery.append("  (cpr.total_balance - nvl(cpr.TAX_AMOUNT,0) - nvl(cpr.CUSTODY_AMOUNT,0)) INTERES_NETO_CONTABLE,");
    sbQuery.append("  (cpr.reporting_balance - nvl(crd.REPORTING_TAX_AMOUNT,0) - nvl(crd.REPORTING_CUSTODY_AMOUNT,0)) INTERES_NETO_REPORTE,");
    sbQuery.append("  (cpr.margin_balance - nvl(crd.MARGIN_TAX_AMOUNT,0) - nvl(crd.MARGIN_CUSTODY_AMOUNT,0)) INTERES_NETO_MARGEN,");
    sbQuery.append("  (cpr.AVAILABLE_BALANCE - nvl(crd.AVAILABLE_TAX_AMOUNT,0) - nvl(crd.AVAILABLE_CUSTODY_AMOUNT,0)) inerest_neto_disponible,");
    sbQuery.append("  scb.AVAILABLE_BALANCE saldo_disponible, ");
    sbQuery.append("  cpr.TOTAL_BALANCE interest_bruto,");
    sbQuery.append("  NVL(cpr.custody_amount,0) comision_custodia,");
    sbQuery.append("  NVL(cpr.tax_amount,0) retencion,");
    sbQuery.append("  scb.pawn_balance saldo_prenda,");//*Baalance for blocked reports
    sbQuery.append("  scb.ban_balance saldo_mbargo,");
    sbQuery.append("  scb.opposition_balance oposicion,");
    sbQuery.append("  scb.reserve_balance encaje,");
    sbQuery.append("  scb.other_block_balance otros,");
    sbQuery.append("  cpr.pawn_balance i_prenda,  ");
    sbQuery.append("  cpr.ban_balance i_mbargo, ");
    sbQuery.append("  cpr.opposition_balance i_oposicion,  ");
    sbQuery.append("  cpr.reserve_balance i_encaje,  ");
    sbQuery.append("  cpr.other_block_balance i_otros, ");
    sbQuery.append("  ha.id_holder_account_pk,");
    sbQuery.append(" ( nvl(scb.pawn_balance,0)+ nvl(scb.ban_balance,0)+nvl(scb.opposition_balance,0)+nvl(scb.other_block_balance,0)+nvl(scb.reserve_balance,0)) total_b_balance,");
    sbQuery.append(" ( nvl(cpr.pawn_balance,0)+ nvl(cpr.ban_balance,0)+nvl(cpr.opposition_balance,0)+nvl(cpr.other_block_balance,0)+nvl(cpr.reserve_balance,0)) i_total_b_balance");



    sbQuery.append("  FROM CORPORATIVE_OPERATION coop,");
    sbQuery.append("       CORPORATIVE_PROCESS_RESULT cpr,");
    sbQuery.append("       STOCK_CALCULATION_BALANCE scb,");
    sbQuery.append("       CORPORATIVE_RESULT_DETAIL crd,");
    sbQuery.append("       SECURITY sec,");
    sbQuery.append("       HOLDER h,");
    sbQuery.append("       PARTICIPANT part,");
    sbQuery.append("       ISSUER isr,");
    sbQuery.append("       HOLDER_ACCOUNT_DETAIL had,");
    sbQuery.append("       HOLDER_ACCOUNT ha,");
    sbQuery.append("       PROGRAM_INTEREST_COUPON pac ");
    sbQuery.append("   WHERE  cpr.ID_CORPORATIVE_OPERATION_FK =  coop.ID_CORPORATIVE_OPERATION_PK and ");
    sbQuery.append("          crd.ID_CORPORATIVE_PROCESS_RESULT = cpr.ID_CORPORATIVE_PROCESS_RESULT and");
    sbQuery.append("          scb.ID_PARTICIPANT_FK = cpr.ID_PARTICIPANT_FK and");
    sbQuery.append("          scb.id_holder_account_fk = cpr.ID_HOLDER_ACCOUNT_FK and ");
    sbQuery.append("          cpr.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK and ");
    sbQuery.append("          cpr.ID_PARTICIPANT_FK = part.ID_PARTICIPANT_PK and");
    sbQuery.append("          scb.ID_PARTICIPANT_FK = part.ID_PARTICIPANT_PK and");
    sbQuery.append("          scb.ID_ISIN_CODE_FK = sec.ID_ISIN_CODE_PK and");
    sbQuery.append("          isr.ID_ISSUER_PK = coop.ID_ISSUER_FK and");
    sbQuery.append("          had.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK and ");
    sbQuery.append("          had.ID_HOLDER_FK = h.ID_HOLDER_PK and");
    sbQuery.append("          had.ID_HOLDER_ACCOUNT_FK = cpr.ID_HOLDER_ACCOUNT_FK AND ");
    sbQuery.append("          coop.ID_PROGRAM_INTEREST_FK = pac.ID_PROGRAM_INTEREST_PK and");
    sbQuery.append("          cpr.IND_ORIGIN_TARGET = :indTwo and ");
    sbQuery.append("          cpr.IND_REMANENT = :indZero  and ");
    sbQuery.append("          sec.ID_ISIN_CODE_PK=coop.id_origin_isin_code_fk and");
    //sbQuery.append("          coop.ID_CORPORATIVE_OPERATION_PK = decode(:process_id,null,coop.ID_CORPORATIVE_OPERATION_PK,:process_id)  and");
    //sbQuery.append("          (0 = 0 or coop.id_target_isin_code_fk =:dest_code_value) and ");
    sbQuery.append("          cpr.IND_ORIGIN_TARGET = :indTwo and ");
    sbQuery.append("          trunc(coop.delivery_date)=trunc(:delivery_date)  and");
    sbQuery.append("          coop.corporative_event_type = :event_type and ");
    sbQuery.append("          coop.state = :corporate_state and");
    sbQuery.append("          scb.ID_STOCK_CALCULATION_FK = ");
    sbQuery.append("              (select max(sp.ID_STOCK_CALCULATION_PK) ");
    sbQuery.append("                  from STOCK_CALCULATION_PROCESS sp");
    sbQuery.append("                  where sp.stock_type      =  :stock_type and ");
    sbQuery.append("                        sp.STOCK_STATE     = :stock_state and ");
    sbQuery.append("                        sp.STOCK_CLASS     = :stock_class  and ");
    sbQuery.append("                        sp.CUTOFF_DATE     = trunc(:cutoff_date) and");
    sbQuery.append("                        sp.ID_ISIN_CODE_FK = decode(:isin_code,null,sp.ID_ISIN_CODE_FK,:isin_code))");
    sbQuery.append(" ORDER BY part.id_participant_pk ");

    Query query = em.createNativeQuery(sbQuery.toString());
    query.setParameter("indZero", BooleanType.NO.getCode());
    query.setParameter("indTwo",2);
    //query.setParameter("event_type", 1276);
    query.setParameter("event_type",filter.getCorporativeEventType());
    query.setParameter("corporate_state",filter.getState());
    //query.setParameter("corporate_state", 1312);
    query.setParameter("stock_type", StockType.CORPORATIVE.getCode());
    query.setParameter("stock_class",StockClassType.NORMAL.getCode());
    query.setParameter("stock_state",StockStateType.FINISHED.getCode());
    query.setParameter("cutoff_date", filter.getCutOffDate());
    query.setParameter("delivery_date",filter.getDeliveryDate());
    //query.setParameter("corporate_state", filter.getCorporativeEventSate());
    //query.setParameter("corporate_state", 1312);
    //query.setParameter("isin_code", "DO2006845013");
    query.setParameter("isin_code", filter.getSourceIsinCode());
    sbQuery.append("");
    sbQuery.append("");
    sbQuery.append("");
    sbQuery.append("");

    return query.getResultList();


  }
  
  /**
   * Search interest payment control.
   *
   * @param filter the filter
   * @return the list
   */
  public List<Object[]> searchInterestPaymentControl(CorporativeProcesInterestTO filter){
    StringBuilder sbQuery = new StringBuilder();

    sbQuery.append("SELECT ");
        sbQuery.append(" DISTINCT   PT.PARAMETER_NAME STATE, ");  //0 - STATE_DESCRIPTION
        sbQuery.append(" ISS.ID_ISSUER_PK ||' - '||ISS.BUSINESS_NAME EMISOR, ");//1 - EMISOR_DESCRIPTION
        sbQuery.append(" SEC.ID_ISSUANCE_CODE_FK EMISION, ");//2 - EMISION_DESCRIPTION
        sbQuery.append(" SEC.ID_ISIN_CODE_PK || ' - ' || SEC.DESCRIPTION VALOR, "); //3 - VALOR_DESCRIPTION
        sbQuery.append(" CURR.PARAMETER_NAME MONDEDA, ");//4 - MONEDA_DESCRIPTION
        sbQuery.append(" TO_CHAR(COP.DELIVERY_DATE,'dd/MM/YYYY') FECHA_PAGO, ");//5 - FECHA_PAGO
        sbQuery.append(" TO_CHAR(COP.REGISTRY_DATE,'dd/MM/YYYY') FECHA_REGISTRO, ");//6 - FECHA_REGISTRO
        sbQuery.append(" PRO.COUPON_NUMBER N_CUPON, ");//7 - NUMERO_CUPON
        sbQuery.append(" TO_CHAR(DECODE(SEC.CURRENT_NOMINAL_VALUE,NULL,0,SEC.CURRENT_NOMINAL_VALUE),'9999999999990.99') VALOR_NOMINAL, ");//8 - VALOR_NOMINAL
        sbQuery.append(" DECODE(COP.DELIVERY_FACTOR,NULL,0,COP.DELIVERY_FACTOR) || '%' FACTOR_INT, ");//9 - FACTOR_INT
        sbQuery.append(" DECODE(COP.TAX_FACTOR,NULL,0,COP.TAX_FACTOR) || '%' IMPUESTO, ");//10 - IMPUESTO
        sbQuery.append(" DECODE(COP.IND_ROUND,0,'NO',1,'YES') IND_REDONDEO, ");//11 - IND_REDONDEO
        sbQuery.append(" TO_CHAR(COP.CUTOFF_DATE,'dd/MM/YYYY') FECHA_CORTE, ");//12 - FECHA_CORTE
        sbQuery.append(" PART.MNEMONIC ||' - '|| PART.ID_PARTICIPANT_PK PARTCIPANT, ");//13 - PARTICIPANTE
        sbQuery.append(" SUM(SCB.TOTAL_BALANCE) SALDO_STOCK_FECHA_DE_CORTE, ");//14 - SALDO_STOCK_FECHA_CORTE
        sbQuery.append(" SUM(CPR.TOTAL_BALANCE) SALDO_INTEREST_NETO_MODEDA, ");//15 - SALDO_INTEREST_NETO_MONEDA
        sbQuery.append(" (SELECT COUNT(DISTINCT(HAD.id_holder_fk))   " +
                 " FROM STOCK_CALCULATION_BALANCE SB,HOLDER_ACCOUNT HAA, holder_account_detail HAD  " +
                 " WHERE SB.ID_PARTICIPANT_FK = SCB.ID_PARTICIPANT_FK AND HAD.id_holder_account_fk=HAA.id_holder_account_pk AND SB.ID_STOCK_CALCULATION_FK = SCB.ID_STOCK_CALCULATION_FK AND SB.ID_HOLDER_ACCOUNT_FK = HAA.ID_HOLDER_ACCOUNT_PK AND SB.TOTAL_BALANCE > 0 ) SALDO_NUMERO_DE_TITULARES ,");//16 - SALDO_NUMER_TITUTLARES
        sbQuery.append(" SUM(SCB.REPORTED_BALANCE) VALORES_STOCK_FECHA_DE_CORTE, ");//17 - VALORES_STOCK_FECHA_CORTE
        sbQuery.append(" SUM(CPR.REPORTED_BALANCE) VALORES_INTEREST_NETO_MODEDA ,");//18 - VALORES_INTEREST_NETO_MONEDA
        sbQuery.append(" (SELECT COUNT(DISTINCT(HAD.id_holder_fk))  "+
                 " FROM STOCK_CALCULATION_BALANCE SB,HOLDER_ACCOUNT HAA, holder_account_detail HAD  " +
                 " WHERE SB.ID_PARTICIPANT_FK = SCB.ID_PARTICIPANT_FK AND HAD.id_holder_account_fk=HAA.id_holder_account_pk AND SB.ID_STOCK_CALCULATION_FK = SCB.ID_STOCK_CALCULATION_FK AND SB.ID_HOLDER_ACCOUNT_FK = HAA.ID_HOLDER_ACCOUNT_PK AND SB.REPORTED_BALANCE > 0 ) VALORES_NUMERO_DE_TITULARES ,");//19 - VALORES_NUMERO_TITULARES
        sbQuery.append(" (SUM(SCB.TOTAL_BALANCE) + SUM(SCB.REPORTED_BALANCE))  TOTAL_STOCK_FECHA_DE_CORTE, ");//20 - TOTAL_STOCK_FECHA_CORTE
        sbQuery.append(" (SUM(CPR.TOTAL_BALANCE) + SUM(CPR.REPORTED_BALANCE))  TOTAL_INTEREST_NETO_MODEDA, ");//21 - TOTAL_INTEREST_NETO_MONEDA
        sbQuery.append(" SEC.ALTERNATIVE_CODE ");//22 - CODIGO_ALTERNO
    sbQuery.append(" FROM    CORPORATIVE_OPERATION COP, "+
                "CORPORATIVE_PROCESS_RESULT CPR, "+
                "STOCK_CALCULATION_BALANCE SCB, "+
                "PARTICIPANT PART, "+
                "HOLDER_ACCOUNT HA, "+
                "SECURITY SEC, "+
                "PROGRAM_INTEREST_COUPON PRO, "+
                "PARAMETER_TABLE CURR, "+
                "PARAMETER_TABLE PT, "+
                "ISSUER ISS ");
    sbQuery.append(" WHERE  CPR.ID_CORPORATIVE_OPERATION_FK = COP.ID_CORPORATIVE_OPERATION_PK "+
                " AND SCB.ID_PARTICIPANT_FK = CPR.ID_PARTICIPANT_FK "+
                " AND SCB.ID_HOLDER_ACCOUNT_FK = CPR.ID_HOLDER_ACCOUNT_FK "+
                " AND CPR.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK "+
                " AND CPR.ID_PARTICIPANT_FK = PART.ID_PARTICIPANT_PK "+
                " AND CPR.IND_ORIGIN_TARGET = 2 "+
                " AND CPR.IND_REMANENT = 0 "+
                " AND SEC.ID_ISIN_CODE_PK=COP.ID_ORIGIN_ISIN_CODE_FK "+
                " AND PRO.ID_PROGRAM_INTEREST_PK = COP.ID_PROGRAM_INTEREST_FK "+
                " AND CURR.PARAMETER_TABLE_PK = SEC.CURRENCY "+
                " AND PT.PARAMETER_TABLE_PK = COP.STATE "+
                " AND ISS.ID_ISSUER_PK = COP.ID_ISSUER_FK ");
    
      if(Validations.validateIsNotNullAndPositive(filter.getProcessId()))
         sbQuery.append(" AND COP.ID_CORPORATIVE_OPERATION_PK = :processId ");
      
      if(Validations.validateIsNotNull(filter.getDestCodeValue()))
         sbQuery.append(" AND COP.ID_ORIGIN_ISIN_CODE_FK = :destCodeValue ");

      if(Validations.validateIsNotNull(filter.getCutoffDate()))
         sbQuery.append(" AND TRUNC(COP.CUTOFF_DATE) = TRUNC(:cutoffDate) ");

      if(Validations.validateIsNotNull(filter.getDeliveryDate()))
         sbQuery.append(" AND TRUNC(COP.DELIVERY_DATE) = TRUNC(:deliveryDate) ");
      
      if(Validations.validateIsNotNullAndPositive(filter.getEventType()))
         sbQuery.append(" AND COP.CORPORATIVE_EVENT_TYPE = :eventType ");
      
      if(Validations.validateIsNotNullAndPositive(filter.getState()))
         sbQuery.append(" AND COP.STATE = :state ");

      if(Validations.validateIsNotNull(filter.getCodeValue()))
         sbQuery.append(" AND COP.ID_ORIGIN_ISIN_CODE_FK = :codeValue ");
      
         sbQuery.append(" AND SCB.ID_STOCK_CALCULATION_FK = ( "+
                " SELECT MAX(SP.ID_STOCK_CALCULATION_PK) "+
                " FROM STOCK_CALCULATION_PROCESS SP  "+
                " WHERE SP.STOCK_TYPE = 1626 "+
                " AND SP.STOCK_STATE = 1327 "+
                " AND SP.STOCK_CLASS = 1328 "+
                " AND TRUNC(SP.REGISTRY_DATE) = TRUNC(COP.REGISTRY_DATE) ");
      if(Validations.validateIsNotNull(filter.getCutoffDate()))
         sbQuery.append(" AND TRUNC(SP.CUTOFF_DATE) = TRUNC(:cutoffDate) ");
      if(Validations.validateIsNotNull(filter.getCodeValue()))
         sbQuery.append(" AND SP.ID_ISIN_CODE_FK = :codeValue ");
      
         sbQuery.append(" ) ");
      
    sbQuery.append("GROUP BY  PART.MNEMONIC, "+
                " PART.ID_PARTICIPANT_PK, "+
                " SEC.ID_ISSUANCE_CODE_FK, "+
                " SEC.ID_ISIN_CODE_PK, "+
                " SEC.DESCRIPTION, "+
                " COP.DELIVERY_DATE, "+
                " COP.REGISTRY_DATE, "+
                " PRO.COUPON_NUMBER, "+
                " SEC.CURRENT_NOMINAL_VALUE, "+
                " COP.DELIVERY_FACTOR, "+
                " COP.TAX_FACTOR, "+
                " COP.IND_ROUND, "+
                " COP.CUTOFF_DATE, "+
                " ISS.ID_ISSUER_PK, "+
                " ISS.BUSINESS_NAME, "+
                " CURR.PARAMETER_NAME, "+
                " PT.PARAMETER_NAME, "+
                " SCB.ID_STOCK_CALCULATION_FK, "+
                " SCB.ID_PARTICIPANT_FK," +
                " SEC.ALTERNATIVE_CODE ");
    sbQuery.append("ORDER BY  PARTCIPANT");
         
    Query query = em.createNativeQuery(sbQuery.toString());


    if(Validations.validateIsNotNullAndPositive(filter.getProcessId()))
      query.setParameter("processId",filter.getProcessId());
    
    if(Validations.validateIsNotNull(filter.getDestCodeValue()))
      query.setParameter("destCodeValue",filter.getDestCodeValue());

    if(Validations.validateIsNotNull(filter.getCutoffDate()))
      query.setParameter("cutoffDate",filter.getCutoffDate());

    if(Validations.validateIsNotNull(filter.getDeliveryDate()))
      query.setParameter("deliveryDate",filter.getDeliveryDate());
    
    if(Validations.validateIsNotNullAndPositive(filter.getEventType()))
      query.setParameter("eventType",filter.getEventType());
    
    if(Validations.validateIsNotNullAndPositive(filter.getState()))
      query.setParameter("state",filter.getState());

    if(Validations.validateIsNotNull(filter.getCodeValue()))
      query.setParameter("codeValue",filter.getCodeValue());
    

    return query.getResultList();

  }

  /**
   * Search balance for interest by security issuer.
   *
   * @param filter the filter
   * @return the list
   */
  public List<Object[]> searchBalanceForInterestBySecurityIssuer(CorporativeOperationTO filter){
    StringBuilder sbQuery = new StringBuilder();

    sbQuery.append("SELECT  ");
    sbQuery.append("  ha.account_number,");
    sbQuery.append("  LISTAGG(h.id_holder_pk||'-'||h.full_name,chr(13)||chr(10)) within group (order by h.full_name) as names,");
    sbQuery.append("  part.id_participant_pk|| ' - ' || part.mnemonic  participant, ");
    sbQuery.append("  scb.TOTAL_BALANCE SALDO_CONTABLE, ");
    sbQuery.append("  (cpr.total_balance - nvl(cpr.TAX_AMOUNT,0) - nvl(cpr.CUSTODY_AMOUNT,0)) INTERES_NETO_CONTABLE,");
    sbQuery.append("  (cpr.AVAILABLE_BALANCE - nvl(crd.AVAILABLE_TAX_AMOUNT,0) - nvl(crd.AVAILABLE_CUSTODY_AMOUNT,0)) inerest_neto_disponible,");
    sbQuery.append("  scb.AVAILABLE_BALANCE saldo_disponible, ");
    sbQuery.append("  cpr.AVAILABLE_BALANCE interest_bruto,");
    sbQuery.append("  NVL(cpr.custody_amount,0) comision_custodia,");
    sbQuery.append("  NVL(cpr.tax_amount,0) retencion ");
    sbQuery.append("  FROM CORPORATIVE_OPERATION coop,");
    sbQuery.append("       CORPORATIVE_PROCESS_RESULT cpr,");
    sbQuery.append("       CORPORATIVE_RESULT_DETAIL crd,");
    sbQuery.append("       STOCK_CALCULATION_BALANCE scb,");
    sbQuery.append("       SECURITY sec,");
    sbQuery.append("       HOLDER h,");
    sbQuery.append("       PARTICIPANT part,");
    sbQuery.append("       ISSUER isr,");
    sbQuery.append("       HOLDER_ACCOUNT_DETAIL had,");
    sbQuery.append("       HOLDER_ACCOUNT ha,");
    sbQuery.append("       PROGRAM_INTEREST_COUPON pac ");
    sbQuery.append("   WHERE  cpr.ID_CORPORATIVE_OPERATION_FK =  coop.ID_CORPORATIVE_OPERATION_PK and ");
    sbQuery.append("          cpr.ID_CORPORATIVE_PROCESS_RESULT = crd.ID_CORPORATIVE_PROCESS_RESULT and");
    sbQuery.append("          scb.ID_PARTICIPANT_FK = cpr.ID_PARTICIPANT_FK and");
    sbQuery.append("          scb.id_holder_account_fk = cpr.ID_HOLDER_ACCOUNT_FK and ");
    sbQuery.append("          cpr.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK and ");
    sbQuery.append("          cpr.ID_PARTICIPANT_FK = part.ID_PARTICIPANT_PK and");
    sbQuery.append("          scb.ID_PARTICIPANT_FK = part.ID_PARTICIPANT_PK and");
    sbQuery.append("          scb.ID_ISIN_CODE_FK = sec.ID_ISIN_CODE_PK and");
    sbQuery.append("          isr.ID_ISSUER_PK = coop.ID_ISSUER_FK and");
    sbQuery.append("          had.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK and ");
    sbQuery.append("          had.ID_HOLDER_FK = h.ID_HOLDER_PK and");
    sbQuery.append("          had.ID_HOLDER_ACCOUNT_FK = cpr.ID_HOLDER_ACCOUNT_FK AND ");
    sbQuery.append("          coop.ID_PROGRAM_INTEREST_FK = pac.ID_PROGRAM_INTEREST_PK and");
    sbQuery.append("          cpr.IND_ORIGIN_TARGET = :indTwo and ");
    sbQuery.append("          cpr.IND_REMANENT = :indZero  and ");
    sbQuery.append("          sec.ID_ISIN_CODE_PK=coop.id_origin_isin_code_fk and");
    //sbQuery.append("          coop.ID_CORPORATIVE_OPERATION_PK = decode(:process_id,null,coop.ID_CORPORATIVE_OPERATION_PK,:process_id)  and");
    //sbQuery.append("          (0 = 0 or coop.id_target_isin_code_fk =:dest_code_value) and ");
    sbQuery.append("          cpr.IND_ORIGIN_TARGET = :indTwo and ");
    sbQuery.append("          trunc(coop.delivery_date)=trunc(:delivery_date)  and");
    sbQuery.append("          coop.corporative_event_type = :event_type and ");
    sbQuery.append("          coop.state = :corporate_state and");
    sbQuery.append("          scb.ID_STOCK_CALCULATION_FK = ");
    sbQuery.append("              (select max(sp.ID_STOCK_CALCULATION_PK) ");
    sbQuery.append("                  from STOCK_CALCULATION_PROCESS sp");
    sbQuery.append("                  where sp.stock_type      =  :stock_type and ");
    sbQuery.append("                        sp.STOCK_STATE     = :stock_state and ");
    sbQuery.append("                        sp.STOCK_CLASS     = :stock_class  and ");
    sbQuery.append("                        trunc(sp.CUTOFF_DATE) = trunc(:cutoff_date) and");
    sbQuery.append("                        sp.ID_ISIN_CODE_FK = decode(:isin_code,null,sp.ID_ISIN_CODE_FK,:isin_code))");
    sbQuery.append(" group by ");
    sbQuery.append("     ha.account_number,");
    sbQuery.append("     part.id_participant_pk|| ' - '|| part.mnemonic,"); 
    sbQuery.append("     scb.TOTAL_BALANCE,");
    sbQuery.append("     (cpr.total_balance - NVL(cpr.TAX_AMOUNT,0) - NVL(cpr.CUSTODY_AMOUNT,0)),");
    sbQuery.append("     (cpr.AVAILABLE_BALANCE - nvl(crd.AVAILABLE_TAX_AMOUNT,0) - nvl(crd.AVAILABLE_CUSTODY_AMOUNT,0)),");
    sbQuery.append("     scb.AVAILABLE_BALANCE,");
    sbQuery.append("     cpr.AVAILABLE_BALANCE,");
    sbQuery.append("      NVL(cpr.custody_amount,0),");
    sbQuery.append("      NVL(cpr.tax_amount,0)");


    Query query = em.createNativeQuery(sbQuery.toString());
    query.setParameter("indZero", BooleanType.NO.getCode());
    query.setParameter("indTwo",2);
    //query.setParameter("event_type", 1276);
    query.setParameter("event_type",filter.getCorporativeEventType());
    query.setParameter("corporate_state",filter.getState());
    //query.setParameter("corporate_state", 1312);
    query.setParameter("stock_type", StockType.CORPORATIVE.getCode());
    query.setParameter("stock_class",StockClassType.NORMAL.getCode());
    query.setParameter("stock_state",StockStateType.FINISHED.getCode());
    query.setParameter("cutoff_date", filter.getCutOffDate());
    query.setParameter("delivery_date",filter.getDeliveryDate());
    //query.setParameter("corporate_state", filter.getCorporativeEventSate());
    //query.setParameter("corporate_state", 1312);
    //query.setParameter("isin_code", "DO2006845013");
    query.setParameter("isin_code", filter.getSourceIsinCode());
    sbQuery.append("");
    sbQuery.append("");
    sbQuery.append("");
    sbQuery.append("");

    return query.getResultList();


  }



  /**
   * Search corporative operation by filter.
   *
   * @param filter the filter
   * @return the object[]
   */
  public Object[] searchCorporativeOperationByFilter(CorporativeOperationTO filter){
    StringBuilder sbQuery = new StringBuilder();

    sbQuery.append("select i.id_issuer_pk||'-'||i.business_name as emisor,");
    sbQuery.append("       s.id_isin_code_pk||'-'||s.description as valor,");
    sbQuery.append("       s.current_nominal_value as nominal,");
    //sbQuery.append(" ( select tax_rate from tax_holder_rate where corporate_event_type=co.corporative_event_type and tax_rate>0 and rownum=1 ) as impuesto,");// JH
    sbQuery.append("       nvl(co.tax_factor,0) impuesto,");//  segun zenteno asi debe ser
    sbQuery.append("       co.delivery_factor interes,");
    sbQuery.append("       decode(ind_round,0,'NO',1,'SI') redondeo,s.id_issuance_code_fk,");
    sbQuery.append("       pic.coupon_number, ");
    sbQuery.append("       to_char(co.registry_date,'dd/mm/yyyy'),");
    sbQuery.append("       to_char(co.cutoff_date,'dd/mm/yyyy'),  ");
    sbQuery.append("       to_char(co.delivery_date,'dd/mm/yyyy'), ");
    sbQuery.append("       s.alternative_code,");
    sbQuery.append("       curr.description,");
    sbQuery.append("       to_char(pic.begining_date,'dd/mm/yyyy'),");
    sbQuery.append("       to_char(pic.EXPERITATION_DATE,'dd/mm/yyyy')");

    sbQuery.append("  from corporative_operation co,");
    sbQuery.append("       security s, ");
    sbQuery.append("       issuer i,");
    sbQuery.append("       parameter_table p,");
    sbQuery.append("       program_interest_coupon pic,");
    sbQuery.append("       parameter_table curr");
    sbQuery.append("  where co.id_origin_isin_code_fk=s.id_isin_code_pk");
    sbQuery.append("    and s.currency=curr.parameter_table_pk");
    sbQuery.append("    and co.id_issuer_fk=i.id_issuer_pk");
    sbQuery.append("    and co.id_program_interest_fk=pic.id_program_interest_pk");
    sbQuery.append("    and co.currency=p.parameter_table_pk");
    sbQuery.append("    and co.cutoff_date=:cutoff_date");
    sbQuery.append("    and co.delivery_date=:delivery_date");
    sbQuery.append("    and co.state=:state");
    sbQuery.append("    and co.corporative_event_type=:event_type");
    sbQuery.append("    and co.id_origin_isin_code_fk=:isin_code");

    Query query = em.createNativeQuery(sbQuery.toString());
    query.setParameter("cutoff_date", filter.getCutOffDate());
    query.setParameter("delivery_date", filter.getDeliveryDate());
    query.setParameter("state", filter.getState());
    query.setParameter("event_type", filter.getCorporativeEventType());
    query.setParameter("isin_code", filter.getSourceIsinCode());

    try{
      Object[] temp = (Object[]) query.getSingleResult();
      return temp;

    }catch(NoResultException ex){
      return null;

    }




  }


  /**
   * Search stock calculation balance by stock process.
   *
   * @param stockProcess the stock process
   * @return the list
   */
  public List<Object[]>  searchStockCalculationBalanceByStockProcess(Long stockProcess){
    StringBuilder sbQuery = new StringBuilder();
    sbQuery.append("SELECT h.legal_residence_country PAIS,");
    sbQuery.append("       s.economic_activity ACTIVIDAD_ECONOMICA,");
    sbQuery.append("       s.currency MONEDA,");
    sbQuery.append("       H.IND_RESIDENCE,");
    sbQuery.append("       SUM(SCB.TOTAL_BALANCE), ");
    sbQuery.append("       geo.name");
    sbQuery.append(" FROM STOCK_CALCULATION_BALANCE SCB,");
    sbQuery.append("      SECURITY S,");
    sbQuery.append("      HOLDER_ACCOUNT HA,");
    sbQuery.append("      HOLDER_ACCOUNT_DETAIL HAD,");
    sbQuery.append("      HOLDER H,");
    sbQuery.append("      GEOGRAPHIC_LOCATION GEO,");
    sbQuery.append("      PARAMETER_TABLE ECO,");
    sbQuery.append("      PARAMETER_TABLE CUR");
    sbQuery.append("  WHERE SCB.ID_ISIN_CODE_FK=S.ID_ISIN_CODE_PK");
    sbQuery.append("    AND   SCB.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK");
    sbQuery.append("    AND   HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK");   
    sbQuery.append("    AND   HAD.ID_HOLDER_FK=H.ID_HOLDER_PK");
    sbQuery.append("    AND   h.legal_residence_country=GEO.id_geographic_location_pk");

    sbQuery.append("   AND   S.ECONOMIC_ACTIVITY=ECO.PARAMETER_TABLE_PK");
    sbQuery.append("   AND   S.CURRENCY=CUR.PARAMETER_TABLE_PK");
    sbQuery.append("      and SCB.id_stock_calculation_fk=:stockProcess");
    sbQuery.append(" GROUP BY  h.legal_residence_country,");
    sbQuery.append("           s.economic_activity  ,");
    sbQuery.append("           H.IND_RESIDENCE,");
    sbQuery.append("           s.currency,");
    sbQuery.append("           geo.name");

    Query query = em.createNativeQuery(sbQuery.toString());
    query.setParameter("stockProcess", stockProcess);

    return (List<Object[]>) query.getResultList();
    //    sbQuery.append("");
    //    sbQuery.append("");
    //    sbQuery.append("");
    //    sbQuery.append("");
    //    sbQuery.append("");
    //    sbQuery.append("");
    //    sbQuery.append("");
  } 

  /**
   * Gets the corporate process result by delivery date.
   *
   * @param deliveryDate the delivery date
   * @return the corporate process result by delivery date
   */
  public List<Object[]> getCorporateProcessResultByDeliveryDate(Date deliveryDate){
    StringBuilder sbQuery = new StringBuilder();
    sbQuery.append("select  h.legal_residence_country PAIS,");
    sbQuery.append("        s.economic_activity ACTIVIDAD,");
    sbQuery.append("        s.CURRENCY MONEDA,");
    sbQuery.append("        H.IND_RESIDENCE RESIDENTE,");
    sbQuery.append("        SUM(CPR.TOTAL_BALANCE),");
    sbQuery.append("        geo.name");

    sbQuery.append("   from corporative_process_result cpr,");
    sbQuery.append("        SECURITY S,");
    sbQuery.append("        HOLDER_ACCOUNT HA,");
    sbQuery.append("        HOLDER_ACCOUNT_DETAIL HAD,");
    sbQuery.append("        HOLDER H,");
    sbQuery.append("        geographic_location GEO,");
    sbQuery.append("        PARAMETER_TABLE P1,");
    sbQuery.append("        PARAMETER_TABLE P2,");
    sbQuery.append("        CORPORATIVE_OPERATION CO");
    sbQuery.append("  WHERE CPR.ID_ISIN_CODE_FK=S.ID_ISIN_CODE_PK");
    sbQuery.append("  AND   CPR.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK");
    sbQuery.append("  AND   HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK");
    sbQuery.append("  AND   HAD.ID_HOLDER_FK=H.ID_HOLDER_PK");
    sbQuery.append("  AND   h.legal_residence_country=geo.id_geographic_location_pk");
    sbQuery.append("  AND   s.economic_activity=P1.PARAMETER_TABLE_PK");
    sbQuery.append("  AND   S.CURRENCY=P2.PARAMETER_TABLE_PK");
    sbQuery.append("  AND   CPR.ID_CORPORATIVE_OPERATION_FK=CO.ID_CORPORATIVE_OPERATION_PK");
    sbQuery.append("  AND   CO.CORPORATIVE_EVENT_TYPE=:eventType");
    sbQuery.append("  AND   co.ind_payed=:indOne");
    sbQuery.append("  AND   CO.STATE=:state");
    sbQuery.append("  AND CO.DELIVERY_DATE<=trunc(:deliveryDate)");
    sbQuery.append("   GROUP BY H.legal_residence_country,");
    sbQuery.append("         S.economic_activity,");
    sbQuery.append("         S.CURRENCY,");
    sbQuery.append("         H.IND_RESIDENCE," +
        "geo.name");
    Query query = em.createNativeQuery(sbQuery.toString());
    query.setParameter("indOne", 1);
    query.setParameter("state", CorporateProcessStateType.DEFINITIVE.getCode());
    query.setParameter("eventType", ImportanceEventType.INTEREST_PAYMENT.getCode());
    query.setParameter("deliveryDate", deliveryDate);

    return (List<Object[]>)query.getResultList(); 

  }


  /**
   * Gets the holder balanace information from stock.
   *
   * @param stock the stock
   * @param filterTo the filter to
   * @return the holder balanace information from stock
   */
  public List<Object[]> getHolderBalanaceInformationFromStock(Long stock,CorporativeOperationTO filterTo){
    StringBuilder sbQuery = new StringBuilder();
    sbQuery.append("select distinct p.mnemonic as participante,");
    sbQuery.append("       ha.account_number,");
    sbQuery.append("       h.id_holder_pk as rnt,");
    sbQuery.append("       ha.alternate_code as alterno,");
    sbQuery.append("       h.full_name,");
    sbQuery.append("       (select parameter_name from parameter_table where parameter_table_pk=h.holder_type) as persona,");
    sbQuery.append("       (select parameter_name from parameter_table where parameter_table_pk=h.document_type )||h.document_number as documento,");
    sbQuery.append("       s.id_isin_code_pk as isin,");
    sbQuery.append("       isu.business_name insti,");
    sbQuery.append("       s.id_issuance_code_fk emision,");
    sbQuery.append("       s.description descripcion,");
    sbQuery.append("       (select description from parameter_table where parameter_table_pk=s.currency) as moneda,");
    sbQuery.append("       s.current_nominal_value nominal,");
    sbQuery.append("       scb.total_balance as posicion,");
    sbQuery.append("       (s.current_nominal_value*scb.total_balance) as custodia");
    sbQuery.append(" from stock_calculation_balance scb,");
    sbQuery.append("      participant p,");
    sbQuery.append("      holder_account ha,");
    sbQuery.append("      holder_account_detail had,");
    sbQuery.append("      holder h,");
    sbQuery.append("      security s,");
    sbQuery.append("      issuer isu ");
    sbQuery.append("where scb.id_participant_fk=p.id_participant_pk");
    sbQuery.append("  and scb.id_holder_account_fk=ha.id_holder_account_pk");
    sbQuery.append("  and had.id_holder_account_fk=ha.id_holder_account_pk");
    sbQuery.append("  AND HAD.IND_REPRESENTATIVE=1");
    sbQuery.append("  and h.id_holder_pk=had.id_holder_fk");
    sbQuery.append("  and scb.id_isin_code_fk=s.id_isin_code_pk");
    sbQuery.append("  and s.id_issuer_fk=isu.id_issuer_pk"); 
    if(Validations.validateIsNotNull(filterTo.getParticipantId())){
      sbQuery.append("  and scb.id_participant_fk=:participant");
    }
    
    if(Validations.validateIsNotNull(stock)){
      sbQuery.append(" and scb.ID_STOCK_CALCULATION_FK=:stock ");
    }
    Query query = em.createNativeQuery(sbQuery.toString());
    if(Validations.validateIsNotNull(filterTo.getParticipantId())){
      query.setParameter("participant", filterTo.getParticipantId());
    }
    
    if(Validations.validateIsNotNull(stock)){
      query.setParameter("stock", stock);

    }

    return (List<Object[]>)query.getResultList();
  }
  
  /**
   * Gets the balance and total holder.
   *
   * @param filter the filter
   * @return the balance and total holder
   * @throws ServiceException the service exception
   */
  public List<Object[]> getBalanceAndTotalHolder(CorporativeOperationTO filter)throws ServiceException{
    StringBuilder sbQuery = new StringBuilder();
    sbQuery.append("SELECT   part.mnemonic,");
    sbQuery.append("         sum(scb.total_balance) saldo_contable_f_corte,");
    sbQuery.append("         sum(cpr.total_balance) sd_interes_neto,");
    sbQuery.append("         count(distinct h.id_holder_pk) titulares");
    sbQuery.append("    FROM ");
    sbQuery.append("        CORPORATIVE_OPERATION coop,");
    sbQuery.append("        CORPORATIVE_PROCESS_RESULT cpr,");
    sbQuery.append("        STOCK_CALCULATION_BALANCE scb,");
    sbQuery.append("        SECURITY sec,");
    sbQuery.append("        HOLDER h,");
    sbQuery.append("        PARTICIPANT part,");
    sbQuery.append("        ISSUER isr,");
    sbQuery.append("        HOLDER_ACCOUNT_DETAIL had,");
    sbQuery.append("        HOLDER_ACCOUNT ha,");
    sbQuery.append("        PROGRAM_INTEREST_COUPON pac ");
    sbQuery.append(" WHERE ");
    sbQuery.append("       cpr.ID_CORPORATIVE_OPERATION_FK =  coop.ID_CORPORATIVE_OPERATION_PK ");
    sbQuery.append(" and   scb.ID_PARTICIPANT_FK = cpr.ID_PARTICIPANT_FK ");
    sbQuery.append(" and   scb.id_holder_account_fk = cpr.ID_HOLDER_ACCOUNT_FK ");
    sbQuery.append(" and   cpr.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK ");
    sbQuery.append(" and   cpr.ID_PARTICIPANT_FK = part.ID_PARTICIPANT_PK");
    sbQuery.append(" and   scb.ID_PARTICIPANT_FK = part.ID_PARTICIPANT_PK ");
    sbQuery.append(" and   scb.ID_ISIN_CODE_FK = sec.ID_ISIN_CODE_PK ");
    sbQuery.append(" and   isr.ID_ISSUER_PK = coop.ID_ISSUER_FK ");
    sbQuery.append(" and   had.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK ");
    sbQuery.append(" and   had.ID_HOLDER_FK = h.ID_HOLDER_PK ");
    sbQuery.append(" AND   coop.ID_PROGRAM_INTEREST_FK = pac.ID_PROGRAM_INTEREST_PK ");
    sbQuery.append(" and   cpr.IND_ORIGIN_TARGET = 2 ");
    sbQuery.append(" and   cpr.IND_REMANENT = 0  ");
    sbQuery.append(" and   sec.ID_ISIN_CODE_PK=coop.id_origin_isin_code_fk ");
    sbQuery.append(" and   cpr.IND_ORIGIN_TARGET = 2 ");
    sbQuery.append(" and   trunc(coop.delivery_date)=trunc(:delivery_date)");
    sbQuery.append(" and   coop.corporative_event_type = :event_type");
    sbQuery.append(" and   coop.state = :state");
    sbQuery.append(" and   scb.total_balance >0");
    sbQuery.append(" and   scb.ID_STOCK_CALCULATION_FK =(");
    sbQuery.append(" select ");
    sbQuery.append("      max(sp.ID_STOCK_CALCULATION_PK)  ");
    sbQuery.append("  from");
    sbQuery.append("     STOCK_CALCULATION_PROCESS sp   ");    
    sbQuery.append(" where");
    sbQuery.append("     sp.stock_type      =  1626 ");
    sbQuery.append(" and sp.STOCK_STATE     = 1327 ");
    sbQuery.append(" and sp.STOCK_CLASS     = 1328 ");
    sbQuery.append(" and sp.CUTOFF_DATE     = trunc(:cutoff_date) ");
    sbQuery.append(" and sp.ID_ISIN_CODE_FK = :isin");
    sbQuery.append("  ) group by part.mnemonic ");
    Query query = em.createNativeQuery(sbQuery.toString());
    query.setParameter("cutoff_date", filter.getCutOffDate());
    query.setParameter("state",filter.getState());
    query.setParameter("event_type",filter.getCorporativeEventType());
    query.setParameter("delivery_date",filter.getDeliveryDate());
    query.setParameter("isin",filter.getSourceIsinCode());
    return query.getResultList();
 
    
  }
  
  /**
   * Gets the report balance and total holder.
   *
   * @param filter the filter
   * @return the report balance and total holder
   * @throws ServiceException the service exception
   */
  public List<Object[]> getReportBalanceAndTotalHolder(CorporativeOperationTO filter)throws ServiceException{
    StringBuilder sbQuery = new StringBuilder();
    sbQuery.append("SELECT   part.mnemonic,");
    sbQuery.append("         sum(scb.reported_balance) saldo_contable_f_corte,");
    sbQuery.append("         sum(cpr.reported_balance) sd_interes_neto,");
    sbQuery.append("         count(distinct h.id_holder_pk) titulares");
    sbQuery.append("    FROM ");
    sbQuery.append("        CORPORATIVE_OPERATION coop,");
    sbQuery.append("        CORPORATIVE_PROCESS_RESULT cpr,");
    sbQuery.append("        STOCK_CALCULATION_BALANCE scb,");
    sbQuery.append("        SECURITY sec,");
    sbQuery.append("        HOLDER h,");
    sbQuery.append("        PARTICIPANT part,");
    sbQuery.append("        ISSUER isr,");
    sbQuery.append("        HOLDER_ACCOUNT_DETAIL had,");
    sbQuery.append("        HOLDER_ACCOUNT ha,");
    sbQuery.append("        PROGRAM_INTEREST_COUPON pac ");
    sbQuery.append(" WHERE ");
    sbQuery.append("       cpr.ID_CORPORATIVE_OPERATION_FK =  coop.ID_CORPORATIVE_OPERATION_PK ");
    sbQuery.append(" and   scb.ID_PARTICIPANT_FK = cpr.ID_PARTICIPANT_FK ");
    sbQuery.append(" and   scb.id_holder_account_fk = cpr.ID_HOLDER_ACCOUNT_FK ");
    sbQuery.append(" and   cpr.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK ");
    sbQuery.append(" and   cpr.ID_PARTICIPANT_FK = part.ID_PARTICIPANT_PK");
    sbQuery.append(" and   scb.ID_PARTICIPANT_FK = part.ID_PARTICIPANT_PK ");
    sbQuery.append(" and   scb.ID_ISIN_CODE_FK = sec.ID_ISIN_CODE_PK ");
    sbQuery.append(" and   isr.ID_ISSUER_PK = coop.ID_ISSUER_FK ");
    sbQuery.append(" and   had.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK ");
    sbQuery.append(" and   had.ID_HOLDER_FK = h.ID_HOLDER_PK ");
    sbQuery.append(" AND   coop.ID_PROGRAM_INTEREST_FK = pac.ID_PROGRAM_INTEREST_PK ");
    sbQuery.append(" and   cpr.IND_ORIGIN_TARGET = 2 ");
    sbQuery.append(" and   cpr.IND_REMANENT = 0  ");
    sbQuery.append(" and   sec.ID_ISIN_CODE_PK=coop.id_origin_isin_code_fk ");
    sbQuery.append(" and   cpr.IND_ORIGIN_TARGET = 2 ");
    sbQuery.append(" and   trunc(coop.delivery_date)=trunc(:delivery_date)");
    sbQuery.append(" and   coop.corporative_event_type = :event_type");
    sbQuery.append(" and   coop.state = :state");
    sbQuery.append(" and   scb.reported_balance >0");
    sbQuery.append(" and   scb.ID_STOCK_CALCULATION_FK =(");
    sbQuery.append(" select ");
    sbQuery.append("      max(sp.ID_STOCK_CALCULATION_PK)  ");
    sbQuery.append("  from");
    sbQuery.append("     STOCK_CALCULATION_PROCESS sp   ");    
    sbQuery.append(" where");
    sbQuery.append("     sp.stock_type      =  1626 ");
    sbQuery.append(" and sp.STOCK_STATE     = 1327 ");
    sbQuery.append(" and sp.STOCK_CLASS     = 1328 ");
    sbQuery.append(" and sp.CUTOFF_DATE     = trunc(:cutoff_date) ");
    sbQuery.append(" and sp.ID_ISIN_CODE_FK = :isin");
    sbQuery.append("  ) group by part.mnemonic ");
    Query query = em.createNativeQuery(sbQuery.toString());
    query.setParameter("cutoff_date", filter.getCutOffDate());
    query.setParameter("state",filter.getState());
    query.setParameter("event_type",filter.getCorporativeEventType());
    query.setParameter("delivery_date",filter.getDeliveryDate());
    query.setParameter("isin",filter.getSourceIsinCode());
    return query.getResultList();
 
    
  }
  
  /**
   * Gets the dividens paid by frozen balance.
   *
   * @param filter the filter
   * @return the dividens paid by frozen balance
   */
  public List<Object[]> getDividensPaidByFrozenBalance(CorporativeOperationTO filter){
    StringBuilder sbQuery = new StringBuilder();
    
    sbQuery.append(" SELECT DISTINCT ");
    sbQuery.append(" TO_CHAR(co.TAX_AMOUNT ,'99999999990.99')  || '%' IMPUESTO, ");
    sbQuery.append(" part.MNEMONIC  ||' - '  || part.ID_PARTICIPANT_PK PARTCIPANT, ");
    sbQuery.append(" part.ID_PARTICIPANT_PK, ");
    sbQuery.append(" ha.ACCOUNT_NUMBER CUENTA, ");
    sbQuery.append("  h.ID_HOLDER_PK  || ' - '  || NVL( ");
    sbQuery.append(" CASE ");
    sbQuery.append(" WHEN h.HOLDER_TYPE=394 ");
    sbQuery.append(" THEN h.FULL_NAME ");
    sbQuery.append(" ELSE h.Name  ||' '||h.FIRST_LAST_NAME||' '||h.SECOND_LAST_NAME  END, '') RNT_DESCRIPCION, ");
    sbQuery.append(" scb.PAWN_BALANCE SALDO_PRENDA, ");
    sbQuery.append(" scb.BAN_BALANCE SALDO_EMBARGO, ");
    sbQuery.append(" scb.OPPOSITION_BALANCE SALDO_OPOSICION, ");
    sbQuery.append(" scb.RESERVE_BALANCE SALDO_ENCAJE, ");
    sbQuery.append(" scb.OTHER_BLOCK_BALANCE SALDO_OTROS, ");
    sbQuery.append(" (scb.PAWN_BALANCE+scb.BAN_BALANCE+scb.OPPOSITION_BALANCE+scb.RESERVE_BALANCE+scb.OTHER_BLOCK_BALANCE) SALDO_TOTAL, ");
    sbQuery.append(" cpr.PAWN_BALANCE BENEFICIO_PRENDA, ");
    sbQuery.append(" cpr.BAN_BALANCE BENEFICIO_EMBARGO, ");
    sbQuery.append(" cpr.OPPOSITION_BALANCE BENEFICIO_OPOSICION, ");
    sbQuery.append(" cpr.RESERVE_BALANCE BENEFICIO_ENCAJE, ");
    sbQuery.append(" cpr.OTHER_BLOCK_BALANCE BENEFICIO_OTROS, ");
    sbQuery.append(" (cpr.PAWN_BALANCE+cpr.BAN_BALANCE+cpr.OPPOSITION_BALANCE+cpr.RESERVE_BALANCE+cpr.OTHER_BLOCK_BALANCE) BENEFICIO_TOTAL, ");
    sbQuery.append(" h.ID_HOLDER_PK, ");
    sbQuery.append(" (SELECT PARAMETER_NAME ");
    sbQuery.append(" FROM PARAMETER_TABLE ");
    sbQuery.append(" WHERE PARAMETER_TABLE_PK = co.STATE) STATE, ");
    sbQuery.append(" co.ID_CORPORATIVE_OPERATION_PK id_corporative_pk, ");
    sbQuery.append(" cpr.id_corporative_process_result cpr_id, ");
    sbQuery.append(" ha.ID_HOLDER_ACCOUNT_PK  ");
    sbQuery.append(" FROM ");
    sbQuery.append(" CORPORATIVE_OPERATION co, ");
    sbQuery.append(" CORPORATIVE_PROCESS_RESULT cpr, ");
    sbQuery.append(" STOCK_CALCULATION_BALANCE scb, ");
    sbQuery.append(" SECURITY sec, ");
    sbQuery.append(" HOLDER h, ");
    sbQuery.append(" PARAMETER_TABLE pt, ");
    sbQuery.append(" PARAMETER_TABLE paratab_event_type, ");
    sbQuery.append(" PARAMETER_TABLE paratab, ");
    sbQuery.append(" PARTICIPANT part, ");
    sbQuery.append(" ISSUER iss, ");
    sbQuery.append(" HOLDER_ACCOUNT_DETAIL had, ");
    sbQuery.append(" HOLDER_ACCOUNT ha, ");
    sbQuery.append(" BLOCK_OPERATION bo, ");
    sbQuery.append(" BLOCK_REQUEST br, ");
    sbQuery.append(" BLOCK_ENTITY be, ");
    sbQuery.append(" BLOCK_CORPORATIVE_RESULT bcr, ");
    sbQuery.append(" block_stock_calculation bscal ");
    sbQuery.append(" WHERE sec.ID_ISIN_CODE_PK                =co.ID_ORIGIN_ISIN_CODE_FK ");
    sbQuery.append(" AND cpr.ID_HOLDER_ACCOUNT_FK             =ha.ID_HOLDER_ACCOUNT_PK ");
    sbQuery.append(" AND had.ID_HOLDER_ACCOUNT_FK             =cpr.ID_HOLDER_ACCOUNT_FK ");
    sbQuery.append(" AND had.ID_HOLDER_FK                     =h.ID_HOLDER_PK ");
    sbQuery.append(" AND pt.PARAMETER_TABLE_PK                =sec.CURRENCY ");
    sbQuery.append(" AND iss.ID_ISSUER_PK                     =co.ID_ISSUER_FK ");
    sbQuery.append(" AND part.ID_PARTICIPANT_PK               =cpr.ID_PARTICIPANT_FK ");
    sbQuery.append(" AND co.ID_CORPORATIVE_OPERATION_PK       =cpr.ID_CORPORATIVE_OPERATION_FK ");
    sbQuery.append(" AND cpr.id_holder_account_fk             =scb.id_holder_account_fk ");
    sbQuery.append(" AND scb.ID_PARTICIPANT_FK                =cpr.ID_PARTICIPANT_FK ");
    sbQuery.append(" AND paratab.parameter_table_pk           =co.state ");
    sbQuery.append(" AND paratab_event_type.parameter_table_pk=co.corporative_event_type ");
    sbQuery.append(" AND cpr.IND_ORIGIN_TARGET                =2 ");
    sbQuery.append(" AND bo.ID_PARTICIPANT_FK                 =part.id_participant_pk ");
    sbQuery.append(" AND bo.ID_BLOCK_REQUEST_FK               =br.ID_BLOCK_REQUEST_PK ");
    sbQuery.append(" AND br.ID_BLOCK_ENTITY_FK                =be.ID_BLOCK_ENTITY_PK ");
    sbQuery.append(" AND bo.ID_BLOCK_OPERATION_PK             =bcr.ID_BLOCK_OPERATION_FK ");
    sbQuery.append(" AND bcr.id_corporative_operation_fk      = co.ID_CORPORATIVE_OPERATION_PK ");
    sbQuery.append(" AND bcr.id_corporative_process_result   IN (cpr.id_corporative_process_result) ");
    sbQuery.append(" AND bcr.id_holder_account_fk             =cpr.ID_HOLDER_ACCOUNT_FK ");
    sbQuery.append(" AND bcr.id_participant_fk                =part.id_participant_pk ");
    sbQuery.append(" AND bscal.id_holder_account_fk           =cpr.ID_HOLDER_ACCOUNT_FK ");
    sbQuery.append(" AND bscal.id_participant_fk              =part.id_participant_pk ");
    sbQuery.append(" AND bscal.ID_BLOCK_OPERATION_FK          =bcr.ID_BLOCK_OPERATION_FK ");
    sbQuery.append(" AND bscal.BLOCK_TYPE                     =bcr.BLOCK_TYPE ");
    if(Validations.validateIsNotNullAndNotEmpty(filter.getId())){
      sbQuery.append(" AND co.ID_CORPORATIVE_OPERATION_PK =:process_id ");
    }
    if(Validations.validateIsNotNullAndNotEmpty(filter.getDeliveryDate())){
      sbQuery.append(" AND TRUNC(co.DELIVERY_DATE) = :delivery_date ");
    }
    if(Validations.validateIsNotNullAndNotEmpty(filter.getState())){
      sbQuery.append(" AND co.STATE =:state ");
    }
    if(Validations.validateIsNotNullAndNotEmpty(filter.getCorporativeEventType())){
      sbQuery.append(" AND co.CORPORATIVE_EVENT_TYPE=:event_type ");
    }
    sbQuery.append(" AND cpr.IND_ORIGIN_TARGET= 2 ");
    sbQuery.append(" AND scb.ID_STOCK_CALCULATION_FK= ");
    sbQuery.append(" (SELECT MAX(sp.ID_STOCK_CALCULATION_PK) ");
    sbQuery.append(" FROM STOCK_CALCULATION_PROCESS sp ");
    sbQuery.append(" WHERE (sp.stock_type   = 1626 ");
    sbQuery.append(" OR sp.stock_type       = 205) ");
    sbQuery.append(" AND sp.STOCK_STATE     = 1327 ");
    sbQuery.append(" AND sp.STOCK_CLASS     = 1328 ");
    if(Validations.validateIsNotNullAndNotEmpty(filter.getCutOffDate())){
      sbQuery.append(" AND TRUNC(sp.CUTOFF_DATE) = :cutoff_date ");
    }
    if(Validations.validateIsNotNullAndNotEmpty(filter.getSourceIsinCode())){
      sbQuery.append(" AND sp.ID_ISIN_CODE_FK = :code_value) ");
    }
    sbQuery.append(" ORDER BY part.ID_PARTICIPANT_PK, ");
    sbQuery.append(" h.ID_HOLDER_PK");
    
    Query query = em.createNativeQuery(sbQuery.toString());
    
    if(Validations.validateIsNotNullAndNotEmpty(filter.getId())){
      query.setParameter("process_id", filter.getId());
    }
    if(Validations.validateIsNotNullAndNotEmpty(filter.getDeliveryDate())){
      query.setParameter("delivery_date", filter.getDeliveryDate(),TemporalType.DATE);
    }
    if(Validations.validateIsNotNullAndNotEmpty(filter.getSourceIsinCode())){
      query.setParameter("code_value", filter.getSourceIsinCode());
    }
    if(Validations.validateIsNotNullAndNotEmpty(filter.getState())){
      query.setParameter("state", filter.getState());
    }
    if(Validations.validateIsNotNullAndNotEmpty(filter.getCorporativeEventType())){
      query.setParameter("event_type", filter.getCorporativeEventType());
    }
    if(Validations.validateIsNotNullAndNotEmpty(filter.getCutOffDate())){
      query.setParameter("cutoff_date", filter.getCutOffDate(),TemporalType.DATE);
    }
    
    return query.getResultList();
  }
  
  /**
   * Gets the dividens paid by frozen balance sub query.
   *
   * @param copTO the cop to
   * @return the dividens paid by frozen balance sub query
   */
  public List<Object[]> getDividensPaidByFrozenBalanceSubQuery(CorporativeOperationSubQueryTO copTO){
    StringBuilder sbQuery = new StringBuilder();
    
    sbQuery.append(" SELECT DISTINCT "); 
    sbQuery.append(" decode(bo.DOCUMENT_NUMBER,null,'N.A',bo.DOCUMENT_NUMBER) DOCUMENT_NUMBER, ");
    sbQuery.append(" decode(br.BLOCK_NUMBER,null,'N.A',br.BLOCK_NUMBER) block_number, ");
    sbQuery.append(" decode(pr.PARAMETER_NAME,null,'N.A',pr.PARAMETER_NAME) block_type, ");
    sbQuery.append(" decode(bscal.BLOCK_BALANCE,null,'N.A',bscal.BLOCK_BALANCE) block_balance, ");
    sbQuery.append(" decode(bcr.BLOCKED_BALANCE,null,'N.A',bcr.BLOCKED_BALANCE) BENEFICIO, ");
    sbQuery.append(" decode(be.ID_BLOCK_ENTITY_PK,null,'N.A',be.ID_BLOCK_ENTITY_PK) || '-' || DECODE(be.FULL_NAME,null,'N.A',be.FULL_NAME) block_entity ");
    sbQuery.append(" from  ");
    sbQuery.append(" BLOCK_OPERATION bo, ");
    sbQuery.append(" BLOCK_REQUEST br, ");
    sbQuery.append(" BLOCK_ENTITY be, ");
    sbQuery.append(" PARAMETER_TABLE pr, ");
    sbQuery.append(" BLOCK_CORPORATIVE_RESULT bcr, ");
    sbQuery.append(" block_stock_calculation bscal ");
    sbQuery.append(" where ");
    if(Validations.validateIsNotNullAndNotEmpty(copTO.getParticipantId())){
      sbQuery.append(" bo.ID_PARTICIPANT_FK = decode(:id_participant,null,-1,:id_participant) ");
    }
    sbQuery.append(" and bo.ID_BLOCK_REQUEST_FK = br.ID_BLOCK_REQUEST_PK ");
    sbQuery.append(" and br.ID_BLOCK_ENTITY_FK = be.ID_BLOCK_ENTITY_PK ");
    sbQuery.append(" and pr.PARAMETER_TABLE_PK = bcr.BLOCK_TYPE ");
    sbQuery.append(" and bo.ID_BLOCK_OPERATION_PK = bcr.ID_BLOCK_OPERATION_FK ");
    if(Validations.validateIsNotNullAndNotEmpty(copTO.getCorporativeOperationId())){
      sbQuery.append(" and bcr.id_corporative_operation_fk=:corp_operation_id ");
    }
    if(Validations.validateIsNotNullAndNotEmpty(copTO.getCorporativeProcessResultId())){
      sbQuery.append(" and bcr.id_corporative_process_result in (:cpr_id) ");
    }
    if(Validations.validateIsNotNullAndNotEmpty(copTO.getHolderAccountId())){
      sbQuery.append(" and bcr.id_holder_account_fk = :holder_account_pk ");
    }
    if(Validations.validateIsNotNullAndNotEmpty(copTO.getParticipantId())){
      sbQuery.append(" and bcr.id_participant_fk = :id_participant ");
    }
    if(Validations.validateIsNotNullAndNotEmpty(copTO.getHolderAccountId())){
      sbQuery.append(" and bscal.id_holder_account_fk = :holder_account_pk ");
    }
    if(Validations.validateIsNotNullAndNotEmpty(copTO.getParticipantId())){
      sbQuery.append(" and bscal.id_participant_fk = :holder_account_pk ");
    }
    sbQuery.append(" and bscal.ID_BLOCK_OPERATION_FK  = bcr.ID_BLOCK_OPERATION_FK ");
    sbQuery.append(" and bscal.BLOCK_TYPE = bcr.BLOCK_TYPE ");
    
    Query query = em.createNativeQuery(sbQuery.toString());
    
    return query.getResultList();
  }
    
  /**
   * Gets the stock operation not setteled by value.
   *
   * @param filter the filter
   * @return the stock operation not setteled by value
   */
  public List<Object[]> getStockOperationNotSetteledByValue(CorporativeOperationTO filter){
    StringBuilder sbQuery = new StringBuilder();
    
    sbQuery.append(" (select ");
    sbQuery.append(" sec.ID_ISIN_CODE_PK |, ");
    sbQuery.append(" nmo.MODALITY_NAME, ") ; 
    sbQuery.append(" nm.MECHANISM_NAME,  ");
    sbQuery.append(" mo.OPERATION_NUMBER n_operation, ") ;
    sbQuery.append(" to_char(mo.OPERATION_DATE,'dd/mm/yyyy') FECHA_OPERATION, ") ;
    sbQuery.append(" to_char(CASH_SETTLEMENT_DATE,'dd/mm/yyyy') FECHA_LIQUIDACI0N_CONTADO, ") ;
    sbQuery.append(" to_char(mo.TERM_SETTLEMENT_DATE,'dd/mm/yyyy') FECHA_LIQUIDACI0N_PLAZO,  ");
    sbQuery.append(" mo.STOCK_QUANTITY CANTIDAD,  ");
    sbQuery.append(" buyer.MNEMONIC COMPRADOR,  ");
    sbQuery.append(" seller.MNEMONIC VENDEDOR,  ");
    sbQuery.append(" param.DESCRIPTION ESTADO,  ");
    sbQuery.append(" mo.ID_NEGOTIATION_MECHANISM_FK ");
    sbQuery.append(" from ");
    sbQuery.append(" mechanism_operation mo, ");
    sbQuery.append(" participant seller, ");
    sbQuery.append(" participant buyer, ");
    sbQuery.append(" negotiation_mechanism nm, ");
    sbQuery.append(" negotiation_modality nmo, ");
    sbQuery.append(" parameter_table param, ");
    sbQuery.append(" security sec ");
    sbQuery.append(" where  ");
    sbQuery.append(" mo.operation_state in (612,614) ");
    sbQuery.append(" and ( ");
    sbQuery.append(" (mo.cash_settlement_date = :cutoff_date and mo.cash_prepaid_date is null and mo.cash_extended_date is null) ");
    sbQuery.append(" or ");
    sbQuery.append(" (mo.cash_extended_date = :cutoff_date) ");
    sbQuery.append(" or  ");
    sbQuery.append(" (mo.cash_prepaid_date = :cutoff_date)) ");
    sbQuery.append(" and (mo.id_isin_code_fk=:isin_code) ");
    sbQuery.append(" and nm.id_negotiation_mechanism_pk = mo.id_negotiation_mechanism_fk ");
    sbQuery.append(" and buyer.id_participant_pk = mo.id_buyer_participant_fk ");
    sbQuery.append(" and seller.id_participant_pk = mo.id_seller_participant_fk ");
    sbQuery.append(" and nmo.id_negotiation_modality_pk = mo.id_negotiation_modality_fk ");
    sbQuery.append(" and param.parameter_table_pk=mo.operation_state ");
    sbQuery.append(" and sec.id_isin_code_pk=mo.id_isin_code_fk) ");
    sbQuery.append(" UNION ");
    sbQuery.append(" (select  ");
    sbQuery.append(" sec.ID_ISIN_CODE_PK || ' - ' || sec.DESCRIPTION VALOR,  ");
    sbQuery.append(" nmo.MODALITY_NAME MODALIDAD,   ");
    sbQuery.append(" nm.MECHANISM_NAME MECANISM,  ");
    sbQuery.append(" mo.OPERATION_NUMBER n_operation,  ");
    sbQuery.append(" to_char(mo.OPERATION_DATE,'dd/mm/yyyy') FECHA_OPERATION,  ");
    sbQuery.append(" to_char(CASH_SETTLEMENT_DATE,'dd/mm/yyyy') FECHA_LIQUIDACI0N_CONTADO,  ");
    sbQuery.append(" to_char(mo.TERM_SETTLEMENT_DATE,'dd/mm/yyyy') FECHA_LIQUIDACI0N_PLAZO,  ");
    sbQuery.append(" mo.STOCK_QUANTITY CANTIDAD,  ");
    sbQuery.append(" buyer.MNEMONIC COMPRADOR,  ");
    sbQuery.append(" seller.MNEMONIC VENDEDOR,  ");
    sbQuery.append(" param.DESCRIPTION ESTADO,  ");
    sbQuery.append(" mo.ID_NEGOTIATION_MECHANISM_FK");
    sbQuery.append(" from mechanism_operation mo, ");
    sbQuery.append(" participant seller, ");
    sbQuery.append(" participant buyer, ");
    sbQuery.append(" negotiation_mechanism nm, ");
    sbQuery.append(" negotiation_modality nmo, ");
    sbQuery.append(" parameter_table param, ");
    sbQuery.append(" security sec ");
    sbQuery.append(" where  ");
    sbQuery.append(" mo.operation_state = 615 ");
    sbQuery.append(" and ( ");
    sbQuery.append(" (mo.term_settlement_date = :cutoff_date and mo.term_prepaid_date is null and mo.term_extended_date is null) ");
    sbQuery.append(" or ");
    sbQuery.append(" (mo.term_extended_date = :cutoff_date) ");
    sbQuery.append(" or  ");
    sbQuery.append(" (mo.term_prepaid_date = :cutoff_date)) ");
    sbQuery.append(" and (mo.id_isin_code_fk=:isin_code) ");
    sbQuery.append(" and nm.id_negotiation_mechanism_pk = mo.id_negotiation_mechanism_fk ");
    sbQuery.append(" and buyer.id_participant_pk = mo.id_buyer_participant_fk ");
    sbQuery.append(" and seller.id_participant_pk = mo.id_seller_participant_fk ");
    sbQuery.append(" and param.parameter_table_pk=mo.operation_state ");
    sbQuery.append(" and sec.id_isin_code_pk=mo.id_isin_code_fk )");
    
    Query query = em.createNativeQuery(sbQuery.toString());
    
    query.setParameter("cutoff_date", filter.getCutOffDate());
    query.setParameter("isin_code", filter.getSourceIsinCode());
    
    return query.getResultList();
  }
  
  /**
   * Gets the amortization payment acc hdr part.
   *
   * @param filter the filter
   * @return the amortization payment acc hdr part
   */
  public List<Object[]> getAmortizationPaymentAccHdrPart(CorporativeOperationTO filter){
    StringBuilder sbQuery = new StringBuilder();
    
    sbQuery.append(" SELECT ha.ACCOUNT_NUMBER CUENTA, ");
    sbQuery.append(" p.mnemonic|| ' - ' || p.id_participant_pk AS ID_PARTICIPANT_FK, ");
    sbQuery.append("  h.ID_HOLDER_PK || ' - ' || NVL(CASE WHEN h.HOLDER_TYPE=394 THEN h.FULL_NAME ELSE h.Name||' '||h.FIRST_LAST_NAME||' '||h.SECOND_LAST_NAME END, '') AS RNT_DESCRIPCION, ");
    sbQuery.append(" issuer.ID_ISSUER_PK||' - '||issuer.BUSINESS_NAME EMISOR, ");
    sbQuery.append(" sc.ID_ISSUANCE_CODE_FK, ");
    sbQuery.append(" sc.ID_ISIN_CODE_PK|| ' - '|| sc.DESCRIPTION VALOR, ");
    sbQuery.append(" (SELECT PARAMETER_NAME FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = sc.CURRENCY ) CURRENCY, ");
    sbQuery.append(" TO_CHAR(co.CUTOFF_DATE,'dd/MM/YYYY') CUTOFF_DATE, ");
    sbQuery.append(" TO_CHAR(co.DELIVERY_DATE,'dd/MM/YYYY') DELIVERY_DATE, ");
    sbQuery.append(" TO_CHAR(co.REGISTRY_DATE,'dd/MM/YYYY') REGISTRY_DATE, ");
    sbQuery.append(" pac.COUPON_NUMBER, ");
    sbQuery.append(" TO_CHAR(sc.CURRENT_NOMINAL_VALUE,'99,99,99,9990.99') VALOR_NOMINAL, ");
    sbQuery.append(" DECODE(co.DELIVERY_FACTOR,NULL,0,co.DELIVERY_FACTOR)|| '%' FACTOR_INT, ");
    sbQuery.append(" DECODE(co.TAX_FACTOR,NULL,0.00,co.TAX_FACTOR)|| '%' IMPUESTO, ");
    sbQuery.append(" DECODE(co.IND_ROUND,0,'NO',1,'YES') IND_ROUND, ");
    sbQuery.append(" co.ID_ORIGIN_ISIN_CODE_FK , ");
    sbQuery.append(" cpr.ID_HOLDER_ACCOUNT_FK, ");
    sbQuery.append(" scb.TOTAL_BALANCE     AS SALDO_CONTABLE, ");
    sbQuery.append(" scb.AVAILABLE_BALANCE AS SALDO_DISPONIBLE, ");
    sbQuery.append(" p.id_participant_pk participant_id, ");
    sbQuery.append(" scb.total_balance AMORTIZACION_BRUTA, ");
    sbQuery.append(" cpr.CUSTODY_AMOUNT COMISION_CUSTODIA, ");
    sbQuery.append(" DECODE((cpr.TOTAL_BALANCE     -(DECODE(cpr.CUSTODY_AMOUNT,NULL,0,cpr.CUSTODY_AMOUNT) + DECODE(cpr.TAX_AMOUNT,NULL,0,cpr.TAX_AMOUNT))),NULL,0,((cpr.TOTAL_BALANCE -(DECODE(cpr.CUSTODY_AMOUNT,NULL,0,cpr.CUSTODY_AMOUNT) + DECODE(cpr.TAX_AMOUNT,NULL,0,cpr.TAX_AMOUNT))))) AMORTIZACION_NETA_CONTABLE, ");
    sbQuery.append(" DECODE((cpr.AVAILABLE_BALANCE - (DECODE(cpr.CUSTODY_AMOUNT,NULL,0,cpr.CUSTODY_AMOUNT) + DECODE(cpr.TAX_AMOUNT,NULL,0,cpr.TAX_AMOUNT))),NULL,0,((cpr.AVAILABLE_BALANCE - (DECODE(cpr.CUSTODY_AMOUNT,NULL,0,cpr.CUSTODY_AMOUNT) + DECODE(cpr.TAX_AMOUNT,NULL,0,cpr.TAX_AMOUNT))))) AMORTIZACION_NETA_DISPONIBLE, ");
    sbQuery.append(" (SELECT PARAMETER_NAME FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = co.STATE) STATE, ");
    sbQuery.append(" DECODE(cpr.TAX_AMOUNT,NULL,0.00,cpr.TAX_AMOUNT) RETENCION , ");
    sbQuery.append("  SUBSTR(ha.ALTERNATE_CODE, instr(ha.ALTERNATE_CODE,'('), 1+instr(ha.ALTERNATE_CODE, ')')-instr(ha.ALTERNATE_CODE, '(')) HOLDER_COUNT, ");
    sbQuery.append(" ha.ID_HOLDER_ACCOUNT_PK ");
    sbQuery.append(" FROM CORPORATIVE_OPERATION co, ");
    sbQuery.append(" STOCK_CALCULATION_BALANCE scb, ");
    sbQuery.append(" PROGRAM_AMORTIZATION_COUPON pac, ");
    sbQuery.append(" SECURITY sc, ");
    sbQuery.append(" CORPORATIVE_PROCESS_RESULT cpr, ");
    sbQuery.append(" HOLDER_ACCOUNT ha, ");
    sbQuery.append(" HOLDER_ACCOUNT_BALANCE hab, ");
    sbQuery.append(" PARTICIPANT p, ");
    sbQuery.append(" HOLDER h, ");
    sbQuery.append(" ISSUER issuer, ");
    sbQuery.append(" HOLDER_ACCOUNT_DETAIL had ");
    sbQuery.append(" WHERE co.ID_PROGRAM_AMORTIZATION_FK = pac.ID_PROGRAM_AMORTIZATION_PK ");
    sbQuery.append(" AND cpr.ID_CORPORATIVE_OPERATION_FK = co.ID_CORPORATIVE_OPERATION_PK ");
    sbQuery.append(" AND scb.ID_PARTICIPANT_FK           = p.ID_PARTICIPANT_PK ");
    sbQuery.append(" AND scb.ID_HOLDER_ACCOUNT_FK        = ha.ID_HOLDER_ACCOUNT_PK ");
    sbQuery.append(" AND scb.ID_ISIN_CODE_FK             = sc.ID_ISIN_CODE_PK ");
    sbQuery.append(" AND issuer.ID_ISSUER_PK             = co.ID_ISSUER_FK ");
    sbQuery.append(" AND cpr.ID_HOLDER_ACCOUNT_FK        = hab.ID_HOLDER_ACCOUNT_PK ");
    sbQuery.append(" AND cpr.ID_HOLDER_ACCOUNT_FK        = ha.ID_HOLDER_ACCOUNT_PK ");
    sbQuery.append(" AND cpr.ID_PARTICIPANT_FK           = hab.ID_PARTICIPANT_PK ");
    sbQuery.append(" AND cpr.ID_ISIN_CODE_FK             = hab.ID_ISIN_CODE_PK ");
    sbQuery.append(" AND p.ID_PARTICIPANT_PK             = scb.ID_PARTICIPANT_FK ");
    sbQuery.append(" AND had.ID_HOLDER_ACCOUNT_FK        = ha.ID_HOLDER_ACCOUNT_PK ");
    sbQuery.append(" AND had.ID_HOLDER_FK                = h.ID_HOLDER_PK ");
    if(Validations.validateIsNotNullAndPositive(filter.getId())){
      sbQuery.append(" AND (co.ID_CORPORATIVE_OPERATION_PK = :process_id OR :process_id IS NULL) ");
    }
    sbQuery.append(" AND co.CUTOFF_DATE                  = to_Date(:cutoff_date,'DD/MM/YYYY') ");
    sbQuery.append(" AND co.DELIVERY_DATE               >= to_Date(:delivery_date,'DD/MM/YYYY') ");
    sbQuery.append(" AND co.STATE                        = :state ");
    sbQuery.append(" AND co.CORPORATIVE_EVENT_TYPE       = :event_type ");
    sbQuery.append(" AND (co.id_origin_isin_code_fk      = :isin_code OR :isin_code IS NULL) ");
    sbQuery.append(" AND cpr.IND_ORIGIN_TARGET           = 1 ");
    sbQuery.append(" AND scb.ID_STOCK_CALCULATION_FK     = (SELECT MAX(sp.ID_STOCK_CALCULATION_PK) ");
    sbQuery.append(" FROM STOCK_CALCULATION_PROCESS sp ");
    sbQuery.append(" WHERE sp.stock_type    = 1626 ");
    sbQuery.append(" AND sp.STOCK_STATE     = 1327 ");
    sbQuery.append(" AND sp.STOCK_CLASS     = 1328 ");
    sbQuery.append(" AND sp.CUTOFF_DATE     = to_date(:cutoff_date,'dd/mm/YYYY') ");
    sbQuery.append(" AND sp.ID_ISIN_CODE_FK =:isin_code) ");
    sbQuery.append(" ORDER BY p.mnemonic, ");
    sbQuery.append(" cpr.ID_HOLDER_ACCOUNT_FK, ");
    sbQuery.append(" h.ID_HOLDER_PK ");
  
    Query query = em.createNativeQuery(sbQuery.toString());
    
    if(Validations.validateIsNotNullAndPositive(filter.getId())){
      query.setParameter("process_id", filter.getId());
    }
    query.setParameter("cutoff_date", CommonsUtilities.convertDateToString(filter.getCutOffDate(),"dd/MM/YYYY"));
    query.setParameter("delivery_date",CommonsUtilities.convertDateToString(filter.getDeliveryDate(),"dd/MM/YYYY"));
    query.setParameter("isin_code", filter.getSourceIsinCode().toString());
    query.setParameter("state", filter.getState());
    query.setParameter("event_type", filter.getCorporativeEventType());
    
    return query.getResultList();
  }

  /**
   * Gets the amortization payment bal locked.
   *
   * @param filter the filter
   * @return the amortization payment bal locked
   */
  public List<Object[]> getAmortizationPaymentBalLocked(CorporativeOperationTO filter){
    StringBuilder sbQuery = new StringBuilder();
    sbQuery.append(" SELECT DISTINCT TO_CHAR(CO.DELIVERY_DATE,'dd/mm/YYYY') DELIVERY_DATE, ");
    sbQuery.append(" PTEVENT.PARAMETER_NAME EVEN_TYPE,  ");
    sbQuery.append(" ISS.ID_ISSUER_PK||' - '||ISS.BUSINESS_NAME ISSUER_CODE_DESC, "); 
    sbQuery.append(" SEC.ID_ISSUANCE_CODE_FK ISSUANCE, "); 
    sbQuery.append(" SEC.ID_ISIN_CODE_PK||' - '||SEC.DESCRIPTION SEC_CODE_DESC, "); 
    sbQuery.append(" PAC.COUPON_NUMBER COUPON_NUM, "); 
    sbQuery.append(" PTCURR.PARAMETER_NAME CURRENCY_DESC, "); 
    sbQuery.append(" TO_CHAR(CO.CUTOFF_DATE,'dd/mm/YYYY') CUTTOFF_DATE, "); 
    sbQuery.append(" TO_CHAR(CO.REGISTRY_DATE,'dd/MM/YYYY')  REGISTRY_DATE, "); 
    sbQuery.append(" TO_CHAR(SEC.CURRENT_NOMINAL_VALUE,'9999999999990.99') NOMINAL_VALUE, "); 
    sbQuery.append(" CO.DELIVERY_FACTOR||'%' DELIVERY_FACTOR,  ");
    sbQuery.append(" DECODE(CO.TAX_AMOUNT,NULL,0,CO.TAX_AMOUNT)||'%'  TAX_AMOUNT, "); 
    sbQuery.append(" DECODE(CO.IND_ROUND,0,'NO',1,'YES') IND_ROUND,  ");
    sbQuery.append(" PA.MNEMONIC ||' - '|| PA.ID_PARTICIPANT_PK PART_MNEMO_CODE,  ");
    sbQuery.append(" PA.ID_PARTICIPANT_PK ID_PARTICIPANT_PK,  ");
    sbQuery.append(" HA.ACCOUNT_NUMBER ACCOUNT_NUMBER,  ");
    sbQuery.append(" HO.ID_HOLDER_PK||' - '||DECODE(HO.HOLDER_TYPE,96,DECODE(HO.NAME,NULL,HO.FULL_NAME,DECODE(HO.SECOND_LAST_NAME,NULL,HO.NAME||' '||HO.FIRST_LAST_NAME,HO.NAME||' '||HO.FIRST_LAST_NAME||' '||HO.SECOND_LAST_NAME)),HO.FULL_NAME) RNT_DESC,  ");
    sbQuery.append(" SCB.PAWN_BALANCE PAWN_BALANCE,  ");
    sbQuery.append(" SCB.BAN_BALANCE BAN_BALANCE,  ");
    sbQuery.append(" SCB.OPPOSITION_BALANCE OPPOSITION_BALANCE,  ");
    sbQuery.append(" SCB.RESERVE_BALANCE RESERVE_BALANCE,  ");
    sbQuery.append(" SCB.OTHER_BLOCK_BALANCE OTHER_BLOCK_BALANCE, ");
    sbQuery.append(" CPR.PAWN_BALANCE AMR_PAWN_BALANCE ,  ");
    sbQuery.append(" CPR.BAN_BALANCE AMR_BAN_BALANCE, ");
    sbQuery.append(" CPR.OPPOSITION_BALANCE AMR_OPPO_BALANCE, ");
    sbQuery.append(" CPR.RESERVE_BALANCE AMR_RESERVE_BALANCE, ");
    sbQuery.append(" CPR.OTHER_BLOCK_BALANCE AMR_OTHER_BALANCE, "); 
    sbQuery.append(" CUO.OPERATION_NUMBER DOCUMENT_NUMBER, "); 
    sbQuery.append(" BO.DOCUMENT_NUMBER  DOCUMENT_NUMBER_ACT, "); 
    sbQuery.append(" DECODE(BCKTYPE.PARAMETER_NAME,NULL,',BCKTYPE.PARAMETER_NAME) BLOCK_TYPE_DESC, "); 
    sbQuery.append(" DECODE(BSC.BLOCK_BALANCE,NULL,0,BSC.BLOCK_BALANCE) BLOCK_BALANCE, "); 
    sbQuery.append(" DECODE(bcr.BLOCKED_BALANCE,NULL,0,bcr.BLOCKED_BALANCE) AMORTIZATION, "); 
    sbQuery.append(" DECODE(BE.ID_BLOCK_ENTITY_PK,NULL,',BE.ID_BLOCK_ENTITY_PK)||' - '|| DECODE(BE.person_type,96,DECODE(BE.person_type,NULL,'',BE.name||' '||BE.first_last_name),DECODE(BE.FULL_NAME,NULL,'',BE.FULL_NAME)) BLOCK_ENTITY_DESC, "); 
    sbQuery.append(" (SELECT PARAMETER_NAME FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = CO.STATE) STATE "); 
    sbQuery.append(" FROM "); 
    sbQuery.append(" CORPORATIVE_OPERATION CO, "); 
    sbQuery.append(" SECURITY SEC, ");
    sbQuery.append(" PARAMETER_TABLE PTEVENT, ");
    sbQuery.append(" PARAMETER_TABLE PTCURR, ");
    sbQuery.append(" PARAMETER_TABLE BCKTYPE, ");
    sbQuery.append(" ISSUER ISS, ");
    sbQuery.append(" PROGRAM_AMORTIZATION_COUPON PAC, "); 
    sbQuery.append(" PARTICIPANT PA, "); 
    sbQuery.append(" CORPORATIVE_PROCESS_RESULT CPR, "); 
    sbQuery.append(" HOLDER_ACCOUNT HA, "); 
    sbQuery.append(" HOLDER HO, "); 
    sbQuery.append(" HOLDER_ACCOUNT_DETAIL HAD, "); 
    sbQuery.append(" STOCK_CALCULATION_BALANCE SCB, ");
    sbQuery.append(" STOCK_CALCULATION_PROCESS SCP, ");
    sbQuery.append(" BLOCK_CORPORATIVE_RESULT BCR, "); 
    sbQuery.append(" BLOCK_OPERATION BO, "); 
    sbQuery.append(" CUSTODY_OPERATION CUO, "); 
    sbQuery.append(" BLOCK_STOCK_CALCULATION BSC, "); 
    sbQuery.append(" BLOCK_REQUEST BR, "); 
    sbQuery.append(" BLOCK_ENTITY BE ");
    sbQuery.append(" WHERE ");
    sbQuery.append(" CO.ID_ORIGIN_ISIN_CODE_FK = SEC.ID_ISIN_CODE_PK "); 
    sbQuery.append(" AND "); 
    sbQuery.append(" PTEVENT.PARAMETER_TABLE_PK = CO.CORPORATIVE_EVENT_TYPE "); 
    sbQuery.append(" AND "); 
    sbQuery.append(" PTCURR.PARAMETER_TABLE_PK = SEC.CURRENCY "); 
    sbQuery.append(" AND "); 
    sbQuery.append(" BCKTYPE.PARAMETER_TABLE_PK = BCR.BLOCK_TYPE "); 
    sbQuery.append(" AND "); 
    sbQuery.append(" ISS.ID_ISSUER_PK = CO.ID_ISSUER_FK "); 
    sbQuery.append(" AND "); 
    sbQuery.append(" CPR.ID_CORPORATIVE_OPERATION_FK = CO.ID_CORPORATIVE_OPERATION_PK "); 
    sbQuery.append(" AND "); 
    sbQuery.append(" PA.ID_PARTICIPANT_PK = CPR.ID_PARTICIPANT_FK "); 
    sbQuery.append(" AND "); 
    sbQuery.append(" PAC.ID_PROGRAM_AMORTIZATION_PK = CO.ID_PROGRAM_AMORTIZATION_FK "); 
    sbQuery.append(" AND "); 
    sbQuery.append(" HA.ID_HOLDER_ACCOUNT_PK = CPR.ID_HOLDER_ACCOUNT_FK "); 
    sbQuery.append(" AND "); 
    sbQuery.append(" HAD.ID_HOLDER_FK = HO.ID_HOLDER_PK "); 
    sbQuery.append(" AND "); 
    sbQuery.append(" CPR.ID_HOLDER_ACCOUNT_FK = HAD.ID_HOLDER_ACCOUNT_FK "); 
    sbQuery.append(" AND ");  
    sbQuery.append(" CPR.ID_HOLDER_ACCOUNT_FK = SCB.ID_HOLDER_ACCOUNT_FK "); 
    sbQuery.append(" AND ");  
    sbQuery.append(" SCB.ID_PARTICIPANT_FK  = CPR.ID_PARTICIPANT_FK ");
    sbQuery.append(" AND "); 
    sbQuery.append(" CUO.ID_CUSTODY_OPERATION_PK = BO.ID_BLOCK_OPERATION_PK "); 
    sbQuery.append(" AND "); 
    sbQuery.append(" BCR.ID_CORPORATIVE_OPERATION_FK = CO.ID_CORPORATIVE_OPERATION_PK "); 
    sbQuery.append(" AND "); 
    sbQuery.append(" BSC.ID_BLOCK_OPERATION_FK = BCR.ID_BLOCK_OPERATION_FK ");  
    sbQuery.append(" AND "); 
    sbQuery.append(" BSC.BLOCK_TYPE = BCR.BLOCK_TYPE "); 
    sbQuery.append(" AND ");  
    sbQuery.append(" BO.ID_BLOCK_REQUEST_FK  = BR.ID_BLOCK_REQUEST_PK "); 
    sbQuery.append(" AND ");  
    sbQuery.append(" BR.ID_BLOCK_ENTITY_FK  = BE.ID_BLOCK_ENTITY_PK ");
    sbQuery.append(" AND SCP.ID_ISIN_CODE_FK = SCB.ID_ISIN_CODE_FK ");
    
    if(Validations.validateIsNotNullAndPositive(filter.getId())){
      sbQuery.append(" AND  CO.ID_CORPORATIVE_OPERATION_PK=:process_id "); 
    }
    
    sbQuery.append(" AND ");  
    sbQuery.append(" TRUNC(CO.DELIVERY_DATE)=to_Date(:delivery_date,'dd/MM/YYYY') "); 
    sbQuery.append(" AND "); 
    sbQuery.append(" CO.CORPORATIVE_EVENT_TYPE=:event_type "); 
    sbQuery.append(" AND ");  
    sbQuery.append(" SCP.CUTOFF_DATE=to_date(:cutoff_date,'dd/MM/YYYY') "); 
    sbQuery.append(" AND "); 
    sbQuery.append(" CO.ID_ORIGIN_ISIN_CODE_FK=:code_value ");
    sbQuery.append(" ORDER BY ");
    sbQuery.append(" DELIVERY_DATE, ");
    sbQuery.append(" ISSUER_CODE_DESC, ");
    sbQuery.append(" ISSUANCE, ");
    sbQuery.append(" CUTTOFF_DATE, ");
    sbQuery.append(" ID_PARTICIPANT_PK, ");
    sbQuery.append(" SEC_CODE_DESC, ");
    sbQuery.append(" ACCOUNT_NUMBER, ");
    sbQuery.append(" RNT_DESC, ");
    sbQuery.append(" COUPON_NUM, ");
    sbQuery.append(" CURRENCY_DESC ");
    
    Query query = em.createNativeQuery(sbQuery.toString());
    
    if(Validations.validateIsNotNullAndPositive(filter.getId())){
      query.setParameter("process_id", filter.getId());
    }
    query.setParameter("delivery_date", CommonsUtilities.convertDateToString(filter.getDeliveryDate(),"dd/MM/YYYY"));
    query.setParameter("event_type", filter.getCorporativeEventType());
    query.setParameter("cutoff_date", CommonsUtilities.convertDateToString(filter.getCutOffDate(),"dd/MM/YYYY"));
    query.setParameter("code_value", filter.getSourceIsinCode());
    
    return query.getResultList();
  }
  
  /**
   * Gets the payment information.
   *
   * @param filter the filter
   * @return the payment information
   */
  public List<Object[]> getPaymentInformation(CorporativeOperationTO filter){
    StringBuilder sbQuery = new StringBuilder();
    
    sbQuery.append("    SELECT ");
    sbQuery.append("      (Select Description From Issuer Where Id_Issuer_Pk=Co.Id_Issuer_Fk) Emisor, ");
    sbQuery.append("      Co.Id_Origin_Isin_Code_Fk, ") ; 
    sbQuery.append("      (Select Description From Parameter_Table Where Parameter_Table_Pk=Co.Currency) Moneda,  ");
    sbQuery.append("      Sec.Placed_Amount MONTO_COLOCADO, ") ; 
    sbQuery.append("      (Select Description From Parameter_Table Where Parameter_Table_Pk=Co.Corporative_Event_Type) Concepto_Pago, ") ; 
    sbQuery.append("      Co.Payment_Amount Monto_Pago, ") ; 
    sbQuery.append("      To_Char(Co.Delivery_Date,'dd/mm/yyyy') Fecha_Pago ") ; 
    sbQuery.append("     FROM Corporative_Operation Co ");
    sbQuery.append(" Inner Join Security Sec ");
    sbQuery.append("     On Sec.Id_Isin_Code_Pk=Co.Id_Origin_Isin_Code_Fk ");
    sbQuery.append("    Where Sec.Placed_Amount > 0 ");
    sbQuery.append("      AND Co.Payment_Amount > 0 ");
    sbQuery.append("      AND Co.IND_PAYED = 1 ");
    sbQuery.append("      AND trunc(CO.DELIVERY_DATE)=trunc(:deliveryDate) ");    
    
    Query query = em.createNativeQuery(sbQuery.toString());
    
    query.setParameter("deliveryDate", filter.getDeliveryDate()); 
    return query.getResultList();
  }
  
  /**
   * Gets the evidence of perceived income.
   *
   * @param filter the filter
   * @return the evidence of perceived income
   */
  public List<Object[]> getEvidenceOfPerceivedIncome(EvidenceOfPerceivedIncomeTO filter){
    StringBuilder sbQuery = new StringBuilder();
    
    sbQuery.append("    SELECT (SELECT ALTERNATE_CODE  ");
    sbQuery.append("          FROM HOLDER_ACCOUNT ");
    sbQuery.append("         WHERE ID_HOLDER_ACCOUNT_PK = HAB.ID_HOLDER_ACCOUNT_PK) AS CUENTA_VAL, ") ; // [0]
    sbQuery.append(" 			FN_GET_HOLDER_DESC(HAB.ID_HOLDER_ACCOUNT_PK,',',0) AS TITULAR,  "); // [1]
    sbQuery.append("      (SELECT DESCRIPTION  ") ; 
    sbQuery.append("         FROM ISSUANCE ") ; 
    sbQuery.append("        WHERE ID_ISSUANCE_CODE_PK=SEC.ID_ISSUANCE_CODE_FK) AS EMISION, ") ; // [2] 
    sbQuery.append("      SEC.ID_ISIN_CODE_PK || ' / ' || SEC.CFI_CODE AS VALOR, ") ; // [3]
    sbQuery.append("      TO_CHAR(SEC.EXPIRATION_DATE,'dd/mm/yyyy') AS VENCIMIENTO, ") ; // [4] 
    sbQuery.append("      (FN_GET_NOMINAL_VALUE(SEC.ID_ISIN_CODE_PK, TRUNC(:endDate))) as VALOR_NOMINAL, ") ; // [5]
    sbQuery.append("      ((FN_GET_NOMINAL_VALUE(SEC.ID_ISIN_CODE_PK, TRUNC(:endDate)))  * HAB.TOTAL_BALANCE) AS MONTO_INVERSION, ") ; // [6]
    sbQuery.append("      HAB.TOTAL_BALANCE, ") ; // [7]
    sbQuery.append("      (SELECT DESCRIPTION ") ; 
    sbQuery.append("         FROM PARTICIPANT ") ; 
    sbQuery.append("        WHERE ID_PARTICIPANT_PK=HAB.ID_PARTICIPANT_PK) as PARTICIPANTE, ") ; // [8]
    sbQuery.append("      (SELECT SUM(TOTAL_BALANCE) ") ; 
    sbQuery.append("         FROM CORPORATIVE_PROCESS_RESULT CPR, CORPORATIVE_OPERATION CO ") ; 
    sbQuery.append("        WHERE CPR.ID_HOLDER_ACCOUNT_FK = HAB.ID_HOLDER_ACCOUNT_PK ") ;
    sbQuery.append("          AND CPR.ID_PARTICIPANT_FK = HAB.ID_PARTICIPANT_PK ") ;
    sbQuery.append("          AND CPR.ID_ISIN_CODE_FK = HAB.ID_ISIN_CODE_PK ") ; 
    sbQuery.append("          AND CO.ID_CORPORATIVE_OPERATION_PK=CPR.ID_CORPORATIVE_OPERATION_FK ") ;
    sbQuery.append("          AND CO.IND_PAYED = 1") ;
    sbQuery.append("          AND TRUNC(CO.DELIVERY_DATE)>=TRUNC(:initialDate) ") ; 
    sbQuery.append("          AND TRUNC(CO.DELIVERY_DATE)<=TRUNC(:endDate) ) INTERESES_PERCIBIDOS, ") ; // [9]
    sbQuery.append("          (SELECT PARAMETER_NAME FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK=H.DOCUMENT_TYPE) TIPO_DOCUMENTO, ") ; // [10]
    sbQuery.append("          H.DOCUMENT_NUMBER, ") ; // [11]
    sbQuery.append("          (SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK=SEC.CURRENCY) MONEDA, ") ; // [12]
    sbQuery.append("          (SELECT BUSINESS_NAME FROM ISSUER WHERE ID_ISSUER_PK=SEC.ID_ISSUER_FK ) EMISOR, ") ; // [13]
    sbQuery.append(" 		  SEC.DESCRIPTION") ; // [14]
    sbQuery.append("     FROM SECURITY SEC ");
    sbQuery.append(" INNER JOIN HOLDER_ACCOUNT_BALANCE HAB ");
    sbQuery.append("     ON HAB.ID_ISIN_CODE_PK = SEC.ID_ISIN_CODE_PK ");
    sbQuery.append(" INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ");
    sbQuery.append("       ON HAD.ID_HOLDER_ACCOUNT_FK = HAB.ID_HOLDER_ACCOUNT_PK ");
    sbQuery.append(" INNER JOIN HOLDER H ");
    sbQuery.append("     ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK "); 
    sbQuery.append("      WHERE HAB.TOTAL_BALANCE > 0  ");  
    sbQuery.append("      AND HAD.IND_REPRESENTATIVE=1 ");
    sbQuery.append(" AND EXISTS (SELECT 'X' ");
    sbQuery.append("          FROM CORPORATIVE_OPERATION C");
    sbQuery.append("         WHERE C.ID_ORIGIN_ISIN_CODE_FK = SEC.ID_ISIN_CODE_PK");    
    sbQuery.append("           AND TRUNC(C.DELIVERY_DATE) BETWEEN TRUNC(:initialDate) AND TRUNC(:endDate))");
    
    if(Validations.validateIsNotNull(filter.getIdParticipantPk())){
      sbQuery.append("      AND HAB.ID_PARTICIPANT_PK=:participant");
    }
    if(Validations.validateIsNotNull(filter.getIdHolderAccountPk())){
      sbQuery.append("      AND HAB.ID_HOLDER_ACCOUNT_PK=:holderAccount ");
    }
    if(Validations.validateIsNotNull(filter.getIdHolderPk())){
      sbQuery.append("      AND H.ID_HOLDER_PK = :holder ");
      
    }   
    if(Validations.validateIsNotNull(filter.getIdIssuanceCodePk())){
      sbQuery.append("      AND SEC.ID_ISSUER_FK = :issuer ");
    }
    if(Validations.validateIsNotNull(filter.getIdIsinCodePk())){
      sbQuery.append("      AND SEC.ID_ISIN_CODE_PK = :isin_code ");
    }
    
      
    
    Query query = em.createNativeQuery(sbQuery.toString());
    
    if(Validations.validateIsNotNull(filter.getIdParticipantPk())){
      query.setParameter("participant", filter.getIdParticipantPk()); 
    }
    if(Validations.validateIsNotNull(filter.getIdHolderAccountPk())){
      query.setParameter("holderAccount", filter.getIdHolderAccountPk()); 
    }
    
    if(Validations.validateIsNotNull(filter.getIdHolderPk())){
      query.setParameter("holder", filter.getIdHolderPk());
    }
    if(Validations.validateIsNotNull(filter.getIdIssuanceCodePk())){
      query.setParameter("issuer", filter.getIdIssuanceCodePk());
    }
            
    if(Validations.validateIsNotNull(filter.getIdIsinCodePk())){
      query.setParameter("isin_code", filter.getIdIsinCodePk());
    }
    
    query.setParameter("initialDate", filter.getInitialDate());
    query.setParameter("endDate", filter.getEndDate()); 
    return query.getResultList();
  }
  
  /**
   * Gets the evidence of retentions.
   *
   * @param filter the filter
   * @return the evidence of retentions
   */
  public List<Object[]> getEvidenceOfRetentions(EvidenceOfPerceivedIncomeTO filter){
    StringBuilder sbQuery = new StringBuilder();
    
    sbQuery.append("    SELECT (SELECT ALTERNATE_CODE  ");
    sbQuery.append("          FROM HOLDER_ACCOUNT ");
    sbQuery.append("         WHERE ID_HOLDER_ACCOUNT_PK = HAB.ID_HOLDER_ACCOUNT_PK) AS CUENTA_VAL, ") ; // [0]
    sbQuery.append(" 			FN_GET_HOLDER_DESC(HAB.ID_HOLDER_ACCOUNT_PK,',',0) AS TITULAR,  "); // [1]
    sbQuery.append("      (SELECT DESCRIPTION  ") ; 
    sbQuery.append("         FROM ISSUANCE ") ; 
    sbQuery.append("        WHERE ID_ISSUANCE_CODE_PK=SEC.ID_ISSUANCE_CODE_FK) AS EMISION, ") ; // [2] 
    sbQuery.append("      (SELECT DESCRIPTION "
                  + "FROM PARAMETER_TABLE "
                  + "WHERE PARAMETER_TABLE_PK=SEC.CURRENCY) || '/' || SEC.ID_ISIN_CODE_PK || '/' || SEC.CFI_CODE AS VALOR, ") ; // [3]
    sbQuery.append("      TO_CHAR(SEC.EXPIRATION_DATE,'dd/mm/yyyy') AS VENCIMIENTO, ") ; // [4] 
    sbQuery.append("      SEC.CURRENT_NOMINAL_VALUE as VALOR_NOMINAL, ") ; // [5]
    sbQuery.append("      (SEC.CURRENT_NOMINAL_VALUE * HAB.TOTAL_BALANCE) AS MONTO_INVERSION, ") ; // [6]
    sbQuery.append("      HAB.TOTAL_BALANCE, ") ; // [7]
    sbQuery.append("      (SELECT DESCRIPTION ") ; 
    sbQuery.append("         FROM PARTICIPANT ") ; 
    sbQuery.append("        WHERE ID_PARTICIPANT_PK=HAB.ID_PARTICIPANT_PK) as PARTICIPANTE, ") ; // [8]
    sbQuery.append("      (SELECT SUM(TOTAL_BALANCE) ") ; 
    sbQuery.append("         FROM CORPORATIVE_PROCESS_RESULT CPR, CORPORATIVE_OPERATION CO ") ; 
    sbQuery.append("        WHERE CPR.ID_HOLDER_ACCOUNT_FK = HAB.ID_HOLDER_ACCOUNT_PK ") ;
    sbQuery.append("          AND CPR.ID_PARTICIPANT_FK = HAB.ID_PARTICIPANT_PK ") ;
    sbQuery.append("          AND CPR.ID_ISIN_CODE_FK = HAB.ID_ISIN_CODE_PK ") ; 
    sbQuery.append("          AND CO.ID_CORPORATIVE_OPERATION_PK=CPR.ID_CORPORATIVE_OPERATION_FK ") ;
    sbQuery.append("          AND CO.IND_PAYED = 1") ;
    sbQuery.append("          AND TRUNC(CO.DELIVERY_DATE)>=TRUNC(:initialDate) ") ; 
    sbQuery.append("          AND TRUNC(CO.DELIVERY_DATE)<=TRUNC(:endDate) ) INTERESES_PERCIBIDOS, ") ; // [9]
    sbQuery.append("          (SELECT INDICATOR1 FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK=H.DOCUMENT_TYPE) TIPO_DOCUMENTO, ") ; // [10]
    sbQuery.append("          H.DOCUMENT_NUMBER, ") ; // [11]   
    sbQuery.append(" 			((SEC.CURRENT_NOMINAL_VALUE) * (SELECT SUM(TAX_AMOUNT) ") ; 
	sbQuery.append(" 			   FROM CORPORATIVE_PROCESS_RESULT CPR, CORPORATIVE_OPERATION CO ") ; 
	sbQuery.append(" 			  WHERE CPR.ID_HOLDER_ACCOUNT_FK = HAB.ID_HOLDER_ACCOUNT_PK ") ;
	sbQuery.append(" 			    AND CPR.ID_PARTICIPANT_FK = HAB.ID_PARTICIPANT_PK ") ;
	sbQuery.append(" 			    AND CPR.ID_ISIN_CODE_FK = HAB.ID_ISIN_CODE_PK ") ; 
	sbQuery.append(" 			    AND CO.ID_CORPORATIVE_OPERATION_PK=CPR.ID_CORPORATIVE_OPERATION_FK ") ;
	sbQuery.append(" 			    AND CO.IND_PAYED = 1") ;
	sbQuery.append(" 			    AND TRUNC(CO.DELIVERY_DATE)>=TRUNC(:initialDate) ") ; 
	sbQuery.append(" 			    AND TRUNC(CO.DELIVERY_DATE)<=TRUNC(:endDate) )) BALANCE_BLOQUEADO, ") ; // [12]		 
    sbQuery.append("      (SELECT SUM(CPR.TAX_AMOUNT) ") ; 
    sbQuery.append("         FROM CORPORATIVE_PROCESS_RESULT CPR, CORPORATIVE_OPERATION CO ") ; 
    sbQuery.append("        WHERE CPR.ID_HOLDER_ACCOUNT_FK = HAB.ID_HOLDER_ACCOUNT_PK ") ;
    sbQuery.append("          AND CPR.ID_PARTICIPANT_FK = HAB.ID_PARTICIPANT_PK ") ;
    sbQuery.append("          AND CPR.ID_ISIN_CODE_FK = HAB.ID_ISIN_CODE_PK ") ; 
    sbQuery.append("          AND CO.ID_CORPORATIVE_OPERATION_PK=CPR.ID_CORPORATIVE_OPERATION_FK ") ;
    sbQuery.append("          AND CO.IND_PAYED = 1") ;    
    sbQuery.append("		  AND TRUNC(CO.DELIVERY_DATE)>=TRUNC(:initialDate)");
    sbQuery.append("          AND TRUNC(CO.DELIVERY_DATE)<=TRUNC(:endDate) ) RETENCIONES, ") ; // [13]
    sbQuery.append("          (SELECT BUSINESS_NAME FROM ISSUER WHERE ID_ISSUER_PK=SEC.ID_ISSUER_FK ) EMISOR,") ; // [14]
    sbQuery.append("          (SEC.INTEREST_RATE || '%') AS INTERES,") ; // [15]    
    sbQuery.append("      TO_CHAR(SEC.ISSUANCE_DATE,'dd/mm/yyyy'),") ; // [16]   
    sbQuery.append(" 			    SEC.DESCRIPTION") ; // [17]
    sbQuery.append("     FROM SECURITY SEC ");
    sbQuery.append(" INNER JOIN HOLDER_ACCOUNT_BALANCE HAB ");
    sbQuery.append("     ON HAB.ID_ISIN_CODE_PK = SEC.ID_ISIN_CODE_PK ");
    sbQuery.append(" INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ");
    sbQuery.append("       ON HAD.ID_HOLDER_ACCOUNT_FK = HAB.ID_HOLDER_ACCOUNT_PK ");
    sbQuery.append(" INNER JOIN HOLDER H ");
    sbQuery.append("     ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK "); 
    sbQuery.append("      WHERE HAB.TOTAL_BALANCE > 0  ");  
    sbQuery.append("      AND HAD.IND_REPRESENTATIVE=1 ");
    sbQuery.append(" AND EXISTS (SELECT 'X' ");
    sbQuery.append("          FROM CORPORATIVE_OPERATION C");
    sbQuery.append("         WHERE C.ID_ORIGIN_ISIN_CODE_FK = SEC.ID_ISIN_CODE_PK");
    sbQuery.append("           AND C.IND_PAYED = 1");
    sbQuery.append("           AND TRUNC(C.DELIVERY_DATE) BETWEEN TRUNC(:initialDate) AND TRUNC(:endDate))");
    
    if(Validations.validateIsNotNull(filter.getIdParticipantPk())){
      sbQuery.append("      AND HAB.ID_PARTICIPANT_PK=:participant");
    }
    if(Validations.validateIsNotNull(filter.getIdHolderAccountPk())){
      sbQuery.append("      AND HAB.ID_HOLDER_ACCOUNT_PK=:holderAccount ");
    }
    if(Validations.validateIsNotNull(filter.getIdHolderPk())){
      sbQuery.append("      AND H.ID_HOLDER_PK = :holder ");
      
    }   
    if(Validations.validateIsNotNull(filter.getIdIssuanceCodePk())){
      sbQuery.append("      AND SEC.ID_ISSUER_FK = :issuer ");
    }
    if(Validations.validateIsNotNull(filter.getIdIsinCodePk())){
      sbQuery.append("      AND SEC.ID_ISIN_CODE_PK = :isin_code ");
    }

    Query query = em.createNativeQuery(sbQuery.toString());
    
    if(Validations.validateIsNotNull(filter.getIdParticipantPk())){
      query.setParameter("participant", filter.getIdParticipantPk()); 
    }
    if(Validations.validateIsNotNull(filter.getIdHolderAccountPk())){
      query.setParameter("holderAccount", filter.getIdHolderAccountPk()); 
    }
    
    if(Validations.validateIsNotNull(filter.getIdHolderPk())){
      query.setParameter("holder", filter.getIdHolderPk());
    }
    if(Validations.validateIsNotNull(filter.getIdIssuanceCodePk())){
      query.setParameter("issuer", filter.getIdIssuanceCodePk());
    }
            
    if(Validations.validateIsNotNull(filter.getIdIsinCodePk())){
      query.setParameter("isin_code", filter.getIdIsinCodePk());
    }
    
    query.setParameter("initialDate", filter.getInitialDate());
    query.setParameter("endDate", filter.getEndDate()); 
    return query.getResultList();
  }

  /**
   * Gets the cash dividends paid by bal locked.
   *
   * @param filter the filter
   * @return the cash dividends paid by bal locked
   */
  public List<Object[]> getCashDividendsPaidByBalLocked(CorporativeOperationTO filter){
    StringBuilder sbQuery = new StringBuilder();
    
    sbQuery.append(" SELECT ");
    sbQuery.append(" ptab.parameter_name state, ");
    sbQuery.append(" iss.ID_ISSUER_PK||' - '||iss.business_name iss, ");
    sbQuery.append(" pt.PARAMETER_NAME CURRENCY, ");
    sbQuery.append(" TO_CHAR(co.cutoff_date,'dd/mm/yyyy') cutoff_date, ");
    sbQuery.append(" TO_CHAR(co.registry_date,'dd/mm/yyyy') registry_date, ");
    sbQuery.append(" NVL( ");
    sbQuery.append(" CASE ");
    sbQuery.append(" WHEN co.tax_factor IS NULL ");
    sbQuery.append(" THEN '0.0%' ");
    sbQuery.append(" ELSE TO_CHAR(co.tax_factor,'99999999990.99')||'%' END,'') tax_factor, ");
    sbQuery.append(" DECODE(co.ind_round,0,'NO',1,'YES') ind_round, ");
    sbQuery.append(" sec.id_issuance_code_fk, ");
    sbQuery.append(" TO_CHAR(co.DELIVERY_DATE,'dd/mm/yyyy') payment_date, ");
    sbQuery.append(" ha.account_number, ");
    sbQuery.append(" co.DELIVERY_FACTOR||'%' interest_factor, ");
    sbQuery.append(" sec.id_isin_code_pk||' - '||sec.description security, ");
    sbQuery.append(" TO_CHAR(sec.CURRENT_NOMINAL_VALUE,'99999999990.99') nominal_value, ");
    sbQuery.append(" h.ID_HOLDER_PK|| ' - '|| NVL( ");
    sbQuery.append(" CASE  ");
    sbQuery.append(" WHEN h.HOLDER_TYPE=394 ");
    sbQuery.append(" THEN h.FULL_NAME ");
    sbQuery.append(" ELSE h.Name||' '||h.FIRST_LAST_NAME||' '||h.SECOND_LAST_NAME END,'') holder, ");
    sbQuery.append(" sb.id_isin_code_fk, ");
    sbQuery.append(" sb.PAWN_BALANCE SALDO_PRENDA, ");
    sbQuery.append(" sb.BAN_BALANCE SALDO_EMBARGO, ");
    sbQuery.append(" sb.OPPOSITION_BALANCE SALDO_OPOSICION, ");
    sbQuery.append(" sb.RESERVE_BALANCE SALDO_ENCAJE, ");
    sbQuery.append(" sb.OTHER_BLOCK_BALANCE SALDO_OTROS, ");
    sbQuery.append(" (sb.PAWN_BALANCE+sb.BAN_BALANCE+sb.OPPOSITION_BALANCE+sb.RESERVE_BALANCE+sb.OTHER_BLOCK_BALANCE) TOTAL_BLOCK, ");
    sbQuery.append(" p.mnemonic||' - '|| p.id_participant_pk participant, ");
    sbQuery.append(" p.id_participant_pk participant_id, ");
    sbQuery.append(" cr.pawn_balance INTERES_PRENDA, ");
    sbQuery.append(" cr.ban_balance INTERES_EMBARGO, ");
    sbQuery.append(" cr.opposition_balance INTERES_OPOSICION, ");
    sbQuery.append(" cr.reserve_balance INTERES_ENCAJE, ");
    sbQuery.append(" cr.other_block_balance INTERES_OTROS, ");
    sbQuery.append(" ha.ID_HOLDER_ACCOUNT_PK, ");
    sbQuery.append(" h.ID_HOLDER_PK, ");
    sbQuery.append(" (cr.pawn_balance+cr.ban_balance+cr.opposition_balance+cr.reserve_balance+cr.other_block_balance) TOTAL_INTREST, ");
    sbQuery.append(" cr.id_corporative_process_result cpr_id, ");
    sbQuery.append(" co.id_corporative_operation_pk, ");
    sbQuery.append(" SUBSTR(ha.ALTERNATE_CODE,instr(ha.ALTERNATE_CODE,'('),1+instr(ha.ALTERNATE_CODE,')')-instr(ha.ALTERNATE_CODE,'(')) HOLDER_COUNT ");
    sbQuery.append(" FROM ");
    sbQuery.append(" CORPORATIVE_OPERATION co, ");
    sbQuery.append(" STOCK_CALCULATION_BALANCE sb, ");
    sbQuery.append(" CORPORATIVE_PROCESS_RESULT cr, ");
    sbQuery.append(" SECURITY sec , ");
    sbQuery.append(" HOLDER h, ");
    sbQuery.append(" PARAMETER_TABLE pt, ");
    sbQuery.append(" PARTICIPANT p, ");
    sbQuery.append(" ISSUER iss, ");
    sbQuery.append(" HOLDER_ACCOUNT_DETAIL had, ");
    sbQuery.append(" HOLDER_ACCOUNT ha, ");
    sbQuery.append(" parameter_table ptab, ");
    sbQuery.append(" parameter_table paratab_event_type, ");
    sbQuery.append(" BLOCK_OPERATION bo, ");
    sbQuery.append(" BLOCK_REQUEST br, ");
    sbQuery.append(" BLOCK_ENTITY be, ");
    sbQuery.append(" BLOCK_CORPORATIVE_RESULT bcr, ");
    sbQuery.append(" block_stock_calculation bc ");
    sbQuery.append(" WHERE sec.ID_ISIN_CODE_PK                =co.ID_ORIGIN_ISIN_CODE_FK ");
    sbQuery.append(" AND cr.ID_HOLDER_ACCOUNT_FK              =ha.ID_HOLDER_ACCOUNT_PK ");
    sbQuery.append(" AND had.ID_HOLDER_ACCOUNT_FK             =cr.ID_HOLDER_ACCOUNT_FK ");
    sbQuery.append(" AND had.ID_HOLDER_FK                     =h.ID_HOLDER_PK ");
    sbQuery.append(" AND pt.PARAMETER_TABLE_PK                =sec.CURRENCY ");
    sbQuery.append(" AND iss.ID_ISSUER_PK                     =co.ID_ISSUER_FK ");
    sbQuery.append(" AND p.ID_PARTICIPANT_PK                  =cr.ID_PARTICIPANT_FK ");
    sbQuery.append(" AND co.ID_CORPORATIVE_OPERATION_PK       =cr.ID_CORPORATIVE_OPERATION_FK ");
    sbQuery.append(" AND cr.id_holder_account_fk              =sb.id_holder_account_fk ");
    sbQuery.append(" AND sb.ID_PARTICIPANT_FK                 =cr.ID_PARTICIPANT_FK ");
    sbQuery.append(" AND ptab.parameter_table_pk              =co.state ");
    sbQuery.append(" AND paratab_event_type.parameter_table_pk=co.corporative_event_type ");  
    sbQuery.append(" AND cr.IND_ORIGIN_TARGET=2 "); 
    if(Validations.validateIsNotNullAndNotEmpty(filter.getId())){
      sbQuery.append(" AND (:process_id IS NULL OR co.ID_CORPORATIVE_OPERATION_PK=:process_id) ");  
    }
    if(Validations.validateIsNotNullAndNotEmpty(filter.getSourceIsinCode())){
      sbQuery.append(" AND co.ID_ORIGIN_ISIN_CODE_FK=:code_value ");  
    }
    if(Validations.validateIsNotNullAndNotEmpty(filter.getDeliveryDate())){
      sbQuery.append(" AND TRUNC(co.delivery_date)=to_Date(:delivery_date,'DD/MM/YYYY') "); 
    }
    if(Validations.validateIsNotNullAndNotEmpty(filter.getCutOffDate())){
      sbQuery.append(" AND co.CUTOFF_DATE=to_date(:cutoff_date,'dd/mm/YYYY') ");
    }
    if(Validations.validateIsNotNullAndNotEmpty(filter.getCorporativeEvent())){
      sbQuery.append(" AND co.corporative_event_type= :event_type "); 
    }
    if(Validations.validateIsNotNullAndNotEmpty(filter.getState())){
      sbQuery.append(" AND co.state=:state ");
    }
    sbQuery.append(" AND sb.ID_STOCK_CALCULATION_FK= ");  
    sbQuery.append(" (SELECT MAX(sp.ID_STOCK_CALCULATION_PK) ");  
    sbQuery.append(" FROM ");
    sbQuery.append(" STOCK_CALCULATION_PROCESS sp "); 
    sbQuery.append(" WHERE (sp.stock_type  =205 "); 
    sbQuery.append(" OR sp.stock_type      =1626) "); 
    sbQuery.append(" AND sp.STOCK_STATE    =1327 ");  
    sbQuery.append(" AND sp.STOCK_CLASS    =1328 ");
    
    if(Validations.validateIsNotNullAndNotEmpty(filter.getCutOffDate())){
      sbQuery.append(" AND sp.CUTOFF_DATE    =to_date(:cutoff_date,'dd/mm/YYYY') ");  
    }
    if(Validations.validateIsNotNullAndNotEmpty(filter.getSourceIsinCode())){
      sbQuery.append(" AND sp.ID_ISIN_CODE_FK=:code_value) ");  
    }
    sbQuery.append(" AND bo.ID_PARTICIPANT_FK               =p.id_participant_pk ");  
    sbQuery.append(" AND bo.ID_BLOCK_REQUEST_FK             =br.ID_BLOCK_REQUEST_PK "); 
    sbQuery.append(" AND br.ID_BLOCK_ENTITY_FK              =be.ID_BLOCK_ENTITY_PK ");  
    sbQuery.append(" AND bo.ID_BLOCK_OPERATION_PK           =bcr.ID_BLOCK_OPERATION_FK ");  
    sbQuery.append(" AND bcr.id_corporative_operation_fk    =co.ID_CORPORATIVE_OPERATION_PK "); 
    sbQuery.append(" AND bcr.id_corporative_process_result IN (cr.id_corporative_process_result) ");  
    sbQuery.append(" AND bcr.id_holder_account_fk           =cr.ID_HOLDER_ACCOUNT_FK ");  
    sbQuery.append(" AND bcr.id_participant_fk              =p.id_participant_pk ");  
    sbQuery.append(" AND bc.id_holder_account_fk            =cr.ID_HOLDER_ACCOUNT_FK ");  
    sbQuery.append(" AND bc.id_participant_fk               =p.id_participant_pk ");  
    sbQuery.append(" AND bc.ID_BLOCK_OPERATION_FK           =bcr.ID_BLOCK_OPERATION_FK ");  
    sbQuery.append(" AND bc.BLOCK_TYPE                      =bcr.BLOCK_TYPE "); 
    sbQuery.append(" ORDER BY PARTICIPANT, ");  
    sbQuery.append(" ha.ID_HOLDER_ACCOUNT_PK, "); 
    sbQuery.append(" h.ID_HOLDER_PK ");   
    
    Query query = em.createNativeQuery(sbQuery.toString());
    
    if(Validations.validateIsNotNullAndNotEmpty(filter.getId())){
      query.setParameter("process_id", filter.getId());
    }
    if(Validations.validateIsNotNullAndNotEmpty(filter.getSourceIsinCode())){
      query.setParameter("code_value", filter.getSourceIsinCode());
    }
    if(Validations.validateIsNotNullAndNotEmpty(filter.getDeliveryDate())){
      query.setParameter("delivery_date", CommonsUtilities.convertDateToString(filter.getDeliveryDate(),"dd/MM/YYYY"));
    }
    if(Validations.validateIsNotNullAndNotEmpty(filter.getCutOffDate())){
      query.setParameter("cutoff_date", CommonsUtilities.convertDateToString(filter.getCutOffDate(),"dd/MM/YYYY"));
    }
    if(Validations.validateIsNotNullAndNotEmpty(filter.getCorporativeEvent())){
      query.setParameter("event_type", filter.getCorporativeEvent());
    }
    if(Validations.validateIsNotNullAndNotEmpty(filter.getState())){
      query.setParameter("state", filter.getState());
    }
    
    return query.getResultList();
  }

  /**
   * Gets the cash dividends paid by bal locked sub query.
   *
   * @param filter the filter
   * @return the cash dividends paid by bal locked sub query
   */
  public List<Object[]> getCashDividendsPaidByBalLockedSubQuery(CorporativeOperationSubQueryTO filter){
    StringBuilder sbQuery = new StringBuilder();
    
    sbQuery.append(" SELECT DISTINCT DECODE(bo.DOCUMENT_NUMBER,NULL,'N.A',bo.DOCUMENT_NUMBER) DOCUMENT_NUMBER, ");
    sbQuery.append(" DECODE(br.BLOCK_NUMBER,NULL,'N.A',br.BLOCK_NUMBER) block_number, ");
    sbQuery.append(" DECODE(pr.PARAMETER_NAME,NULL,'N.A',pr.PARAMETER_NAME) block_type, ");
    sbQuery.append(" DECODE(bscal.BLOCK_BALANCE,NULL,'N.A',bscal.BLOCK_BALANCE) block_balance, ");
    sbQuery.append(" DECODE(bcr.BLOCKED_BALANCE,NULL,'N.A',bcr.BLOCKED_BALANCE) BENEFICIO, ");
    sbQuery.append(" DECODE(be.ID_BLOCK_ENTITY_PK,NULL,'N.A',be.ID_BLOCK_ENTITY_PK)||'-'|| DECODE(be.FULL_NAME,NULL,'N.A',be.FULL_NAME) block_entity ");
    sbQuery.append(" FROM "); 
    sbQuery.append(" BLOCK_OPERATION bo , ");
    sbQuery.append(" BLOCK_REQUEST br , ");
    sbQuery.append(" BLOCK_ENTITY be , ");
    sbQuery.append(" PARAMETER_TABLE pr , ");
    sbQuery.append(" BLOCK_CORPORATIVE_RESULT bcr , ");
    sbQuery.append(" block_stock_calculation bscal ");
    sbQuery.append(" WHERE  ");
    sbQuery.append(" bo.ID_PARTICIPANT_FK = DECODE(:participant_id,NULL,-1,:participant_id) ");
    sbQuery.append(" AND bo.ID_BLOCK_REQUEST_FK             = br.ID_BLOCK_REQUEST_PK ");
    sbQuery.append(" AND br.ID_BLOCK_ENTITY_FK              = be.ID_BLOCK_ENTITY_PK ");
    sbQuery.append(" AND pr.PARAMETER_TABLE_PK              = bcr.BLOCK_TYPE ");
    sbQuery.append(" AND bo.ID_BLOCK_OPERATION_PK           = bcr.ID_BLOCK_OPERATION_FK ");
    if(Validations.validateIsNotNullAndNotEmpty(filter.getCorporativeOperationId())){
      sbQuery.append(" AND bcr.id_corporative_operation_fk    = :corp_operation_id ");
    }
    if(Validations.validateIsNotNullAndNotEmpty(filter.getCorporativeProcessResultId())){
      sbQuery.append(" AND bcr.id_corporative_process_result IN (:corp_op_pr_id) ");
    }
    sbQuery.append(" AND bcr.id_holder_account_fk           = :holder_account_id ");
    sbQuery.append(" AND bcr.id_participant_fk              = :participant_id ");
    sbQuery.append(" AND bscal.id_holder_account_fk         = :holder_account_id ");
    sbQuery.append(" AND bscal.id_participant_fk            = :participant_id ");
    sbQuery.append(" AND bscal.ID_BLOCK_OPERATION_FK        = bcr.ID_BLOCK_OPERATION_FK ");
    sbQuery.append(" AND bscal.BLOCK_TYPE                   = bcr.BLOCK_TYPE ");
    
    Query query = em.createNativeQuery(sbQuery.toString());
    
    if(Validations.validateIsNotNullAndNotEmpty(filter.getCorporativeOperationId())){
      query.setParameter("corp_operation_id", filter.getCorporativeOperationId());
    }
    if(Validations.validateIsNotNullAndNotEmpty(filter.getCorporativeProcessResultId())){
      query.setParameter("corp_op_pr_id", filter.getCorporativeProcessResultId());
    }
    if(Validations.validateIsNotNullAndNotEmpty(filter.getParticipantId())){
      query.setParameter("participant_id", filter.getParticipantId());
    }
    if(Validations.validateIsNotNullAndNotEmpty(filter.getHolderAccountId())){
      query.setParameter("holder_account_id", filter.getHolderAccountId());
    }
    
    return query.getResultList();
  }
  
  /**
   * Gets the retirement amortization.
   *
   * @param filter the filter
   * @return the retirement amortization
   */
  @SuppressWarnings("unchecked")
  public List<Object[]> getRetirementAmortization(CorporativeOperationTO filter){
    StringBuilder sbQuery = new StringBuilder();    
    sbQuery.append("SELECT HA.ACCOUNT_NUMBER,");                                  //[0]
    sbQuery.append("    PA.mnemonic||' - '|| PA.id_participant_pk,");                       //[1]
    sbQuery.append("    HO.ID_HOLDER_PK||' - '||NVL(CASE WHEN HO.HOLDER_TYPE=394 " +
                              "THEN HO.FULL_NAME " +
                              "ELSE HO.Name||' '||HO.FIRST_LAST_NAME||' '||HO.SECOND_LAST_NAME " +
                              "END,''),");                        //[2]
    sbQuery.append("    TO_CHAR(CO.REGISTRY_DATE,'dd/MM/YYYY'),");                        //[3]
    sbQuery.append("    NVL(CO.PENALITY_RATE,0)||' %',");                           //[4]
    sbQuery.append("    DECODE(CO.IND_ROUND,0,'NO',1,'YES'),");                         //[5]
    sbQuery.append("    CPR.ID_HOLDER_ACCOUNT_FK,");                              //[6]
    sbQuery.append("    RD.ACCOUNT_BALANCE,");                                  //[7]
    sbQuery.append("    PA.id_participant_pk,");                                //[8]
    sbQuery.append("    CPR.AVAILABLE_BALANCE,");                               //[9]
    sbQuery.append("    NVL(CPR.PENALITY_AMOUNT,0),");                              //[10]
    sbQuery.append("    (CPR.TOTAL_BALANCE + NVL(CPR.PENALITY_AMOUNT,0)),");                  //[11]
    sbQuery.append("    (SELECT PARAMETER_NAME FROM PARAMETER_TABLE " +
                  "WHERE PARAMETER_TABLE_PK = CO.STATE),");                     //[12]
    sbQuery.append("    SUBSTR(HA.ALTERNATE_CODE, instr(HA.ALTERNATE_CODE,'('), " +
                  "1+instr(HA.ALTERNATE_CODE, ')')-instr(HA.ALTERNATE_CODE, '(')) HOLDER_COUNT");   //[13]
    sbQuery.append(" FROM CORPORATIVE_OPERATION CO, RETIREMENT_OPERATION RO,");
    sbQuery.append("    RETIREMENT_DETAIL RD,");
    sbQuery.append("    CORPORATIVE_PROCESS_RESULT CPR, HOLDER_ACCOUNT HA,");
    sbQuery.append("    PARTICIPANT PA, HOLDER HO,");
    sbQuery.append("    HOLDER_ACCOUNT_DETAIL HAD");
    sbQuery.append("  WHERE CPR.ID_CORPORATIVE_OPERATION_FK = CO.ID_CORPORATIVE_OPERATION_PK");
    sbQuery.append("  AND RO.IND_RETIREMENT_EXPIRATION <> 1");//Excluir las Operaciones de Retiro de valores Vencidos
    sbQuery.append("  AND CO.ID_RETIREMENT_OPERATION_FK = RO.ID_RETIREMENT_OPERATION_PK");
    sbQuery.append("  AND RD.ID_RETIREMENT_OPERATION_FK = RO.ID_RETIREMENT_OPERATION_PK");
    sbQuery.append("  AND RD.ID_HOLDER_ACCOUNT_FK = HAD.ID_HOLDER_ACCOUNT_FK");
    sbQuery.append("  AND HAD.ID_HOLDER_FK = HO.ID_HOLDER_PK ");    
    sbQuery.append("  AND RO.ID_PARTICIPANT_FK = PA.ID_PARTICIPANT_PK");
    sbQuery.append("  AND RD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK");
    sbQuery.append("  AND RO.ID_PARTICIPANT_FK = CPR.ID_PARTICIPANT_FK");
    sbQuery.append("  AND RD.ID_HOLDER_ACCOUNT_FK = CPR.ID_HOLDER_ACCOUNT_FK"); 
    sbQuery.append("  AND TRUNC(CO.CUTOFF_DATE) = TRUNC(:cutoff_date)");
    sbQuery.append("  AND TRUNC(CO.DELIVERY_DATE) = TRUNC(:delivery_date)");
    sbQuery.append("  AND CO.STATE = :state");
    sbQuery.append("  AND CO.CORPORATIVE_EVENT_TYPE = :event_type");  
    sbQuery.append("  AND CPR.IND_ORIGIN_TARGET = :indOriginTarget");
    if(Validations.validateIsNotNullAndNotEmpty(filter.getId()))
      sbQuery.append("  AND CO.ID_CORPORATIVE_OPERATION_PK = :process_id");
    if(Validations.validateIsNotNullAndNotEmpty(filter.getSourceIsinCode()))
      sbQuery.append("  AND CO.id_origin_isin_code_fk = :isin_code");   
    sbQuery.append(" ORDER BY PA.mnemonic, CPR.ID_HOLDER_ACCOUNT_FK");
  
    Query query = em.createNativeQuery(sbQuery.toString());
    
    if(Validations.validateIsNotNullAndNotEmpty(filter.getId()))
      query.setParameter("process_id", filter.getId());   
    query.setParameter("cutoff_date", filter.getCutOffDate());
    query.setParameter("delivery_date", filter.getDeliveryDate());
    if(Validations.validateIsNotNullAndNotEmpty(filter.getSourceIsinCode()))
      query.setParameter("isin_code", filter.getSourceIsinCode().toString());
    query.setParameter("state", filter.getState());
    query.setParameter("event_type", filter.getCorporativeEventType());
    query.setParameter("isin_code", filter.getSourceIsinCode());
    query.setParameter("indOriginTarget", new Integer(ReportConstant.CORPORATIVE_BALANCE_DESTINY));
    return query.getResultList();
  }
  
  /**
   * Gets the retirement interest.
   *
   * @param filter the filter
   * @return the retirement interest
   */
  public List<Object[]> getRetirementInterest(CorporativeOperationTO filter){
    StringBuilder sbQuery = new StringBuilder();

    sbQuery.append("SELECT  ha.account_number,"); //[0]
    sbQuery.append("    LISTAGG(h.id_holder_pk||'-'||h.full_name,chr(13)||chr(10)) within group (order by h.full_name) as names,");//[1]
    sbQuery.append("      part.id_participant_pk|| ' - ' || part.mnemonic  participant, "); // [2]
    sbQuery.append("      scb.TOTAL_BALANCE SALDO_CONTABLE, "); //[3]
    sbQuery.append("      (cpr.total_balance - nvl(cpr.TAX_AMOUNT,0) - nvl(cpr.CUSTODY_AMOUNT,0)) INTERES_NETO_CONTABLE,"); // [4]
    sbQuery.append("      (cpr.AVAILABLE_BALANCE - nvl(crd.AVAILABLE_TAX_AMOUNT,0) - nvl(crd.AVAILABLE_CUSTODY_AMOUNT,0)) inerest_neto_disponible,");//[5]
    sbQuery.append("      RD.ACCOUNT_BALANCE saldo_disponible, "); // [6]
    sbQuery.append("      cpr.AVAILABLE_BALANCE interest_bruto,"); // [7]
    sbQuery.append("      NVL(cpr.custody_amount,0) comision_custodia,"); // [8]
    sbQuery.append("      NVL(cpr.tax_amount,0) retencion "); // [9]
    sbQuery.append("    FROM  CORPORATIVE_OPERATION coop,");
    sbQuery.append("      CORPORATIVE_PROCESS_RESULT cpr,");
    sbQuery.append("      CORPORATIVE_RESULT_DETAIL crd,");
    sbQuery.append("      RETIREMENT_OPERATION RO, ");
    sbQuery.append("      RETIREMENT_DETAIL RD, ");
    sbQuery.append("          SECURITY sec,");
    sbQuery.append("          HOLDER h,");
    sbQuery.append("      PARTICIPANT part,");
    sbQuery.append("      ISSUER isr,");
    sbQuery.append("      HOLDER_ACCOUNT_DETAIL had,");
    sbQuery.append("      HOLDER_ACCOUNT ha");
    sbQuery.append("  WHERE cpr.ID_CORPORATIVE_OPERATION_FK =  coop.ID_CORPORATIVE_OPERATION_PK and ");
    sbQuery.append("      cpr.ID_CORPORATIVE_PROCESS_RESULT = crd.ID_CORPORATIVE_PROCESS_RESULT and");
    sbQuery.append("      co.ID_RETIREMENT_OPERATION_FK = RO.ID_RETIREMENT_OPERATION_PK");
    sbQuery.append("      RD.ID_RETIREMENT_OPERATION_FK = RO.ID_RETIREMENT_OPERATION_PK");
    sbQuery.append("            cpr.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK and ");
    sbQuery.append("            cpr.ID_PARTICIPANT_FK = part.ID_PARTICIPANT_PK and");
    sbQuery.append("      RO.ID_PARTICIPANT_FK           = cpr.ID_PARTICIPANT_PK ");
    sbQuery.append("      AND RD.ID_HOLDER_ACCOUNT_FK        = cpr.ID_HOLDER_ACCOUNT_PK ");
    sbQuery.append("      AND RO.ID_ISIN_CODE_FK             = cpr.ID_ISIN_CODE_PK and");   
    sbQuery.append("          isr.ID_ISSUER_PK = coop.ID_ISSUER_FK and");
    sbQuery.append("          had.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK and ");
    sbQuery.append("          had.ID_HOLDER_FK = h.ID_HOLDER_PK and");
    sbQuery.append("          had.ID_HOLDER_ACCOUNT_FK = cpr.ID_HOLDER_ACCOUNT_FK AND ");
    sbQuery.append("          cpr.IND_ORIGIN_TARGET = :indTwo and ");
    sbQuery.append("          cpr.IND_REMANENT = :indZero  and ");
    sbQuery.append("          sec.ID_ISIN_CODE_PK=coop.id_origin_isin_code_fk and");
    sbQuery.append("          cpr.IND_ORIGIN_TARGET = :indTwo and ");
    sbQuery.append("          trunc(coop.delivery_date)=trunc(:delivery_date)  and");
    sbQuery.append("          coop.corporative_event_type = :event_type and ");
    sbQuery.append("          coop.state = :corporate_state");


    Query query = em.createNativeQuery(sbQuery.toString());
    query.setParameter("indZero", BooleanType.NO.getCode());
    query.setParameter("indTwo",2);
    query.setParameter("event_type",filter.getCorporativeEventType());
    query.setParameter("corporate_state",filter.getState());
    query.setParameter("cutoff_date", filter.getCutOffDate());
    query.setParameter("delivery_date",filter.getDeliveryDate());
    query.setParameter("isin_code", filter.getSourceIsinCode());

    return query.getResultList();
  }
  
  /**
   * Gets the stock summary not settelled by value.
   *
   * @param filter the filter
   * @return the stock summary not settelled by value
   */
  public String getStockSummaryNotSettelledByValue(StockCalculationProcessTO filter){
	 StringBuilder sbQuery = new StringBuilder();
  
	 sbQuery.append("SELECT  se.ID_SECURITY_CODE_PK, ");
	    sbQuery.append("    se.DESCRIPTION, ");
	    sbQuery.append("      nme.MECHANISM_NAME, ");
	    sbQuery.append("      nmo.MODALITY_NAME, ");
	    sbQuery.append("      mo.OPERATION_NUMBER, ");
	    sbQuery.append("      to_char(mo.OPERATION_DATE) OPERATION_DATE, ");
	    sbQuery.append("      to_char(mo.CASH_SETTLEMENT_DATE) CASH_SETTLEMENT_DATE, ");
	    sbQuery.append("      to_char(mo.TERM_SETTLEMENT_DATE) TERM_SETTLEMENT_DATE, ");
	    sbQuery.append("      mo.STOCK_QUANTITY, ");
	    sbQuery.append("      pasel.ID_PARTICIPANT_PK PA_SEL, ");
	    sbQuery.append("      pasel.MNEMONIC PA_SEL_DESC, ");
	    sbQuery.append("      pabuy.ID_PARTICIPANT_PK PA_BUY, ");
	    sbQuery.append("      pabuy.MNEMONIC PA_BUY_DESC, ");
	    sbQuery.append("      (SELECT PARAMETER_TABLE.PARAMETER_NAME FROM PARAMETER_TABLE where PARAMETER_TABLE.PARAMETER_TABLE_PK = mo.OPERATION_STATE) ESTADO ");
	    sbQuery.append("    FROM  MECHANISM_OPERATION mo, ");
	    sbQuery.append("      MECHANISM_MODALITY mm, ");
	    sbQuery.append("      NEGOTIATION_MECHANISM nme, ");
	    sbQuery.append("      NEGOTIATION_MODALITY nmo, ");
	    sbQuery.append("      PARTICIPANT pasel, ");
	    sbQuery.append("          PARTICIPANT pabuy, ");
	    sbQuery.append("          SECURITY se ");
	    sbQuery.append("  WHERE mo.ID_NEGOTIATION_MECHANISM_FK = mm.ID_NEGOTIATION_MECHANISM_PK and ");
	    sbQuery.append("      mo.ID_NEGOTIATION_MODALITY_FK = mm.ID_NEGOTIATION_MODALITY_PK and ");
	    sbQuery.append("      mm.ID_NEGOTIATION_MECHANISM_PK = nme.ID_NEGOTIATION_MECHANISM_PK and ");
	    sbQuery.append("      mm.ID_NEGOTIATION_MODALITY_PK = nmo.ID_NEGOTIATION_MODALITY_PK and");
	    sbQuery.append("            mo.ID_SELLER_PARTICIPANT_FK = pasel.ID_PARTICIPANT_PK and ");
	    sbQuery.append("            mo.ID_BUYER_PARTICIPANT_FK = pabuy.ID_PARTICIPANT_PK and");
	    sbQuery.append("      mo.ID_SECURITY_CODE_FK = se.ID_SECURITY_CODE_PK and ");
	    sbQuery.append("      mo.OPERATION_STATE not in (1128,1133,1131,1127,1132,1130,1862,1864,1863,1867,1865,1866) and ");
	    sbQuery.append("      (((CASE WHEN mo.TERM_SETTLEMENT_DATE is null then mo.OPERATION_STATE end)not in (613,615)) ");   
	    sbQuery.append("          or ((CASE WHEN mo.TERM_SETTLEMENT_DATE is not null and mo.OPERATION_STATE=614 then mo.OPERATION_STATE end)not in (613,615)) ");
	    sbQuery.append("          or ((CASE WHEN mo.TERM_SETTLEMENT_DATE is not null and mo.OPERATION_STATE=615 then mo.OPERATION_STATE end)not in (1849,616))) ");
	    sbQuery.append("          and (((CASE WHEN mo.TERM_SETTLEMENT_DATE is null then trunc(mo.CASH_SETTLEMENT_DATE) end)<=TO_DATE('"+filter.getRegistryDate()+"','dd/MM/yyyy')) ");
	    sbQuery.append("          or ((CASE WHEN mo.TERM_SETTLEMENT_DATE is not null and mo.OPERATION_STATE=614 then trunc(mo.CASH_SETTLEMENT_DATE) end)<=TO_DATE('"+filter.getRegistryDate()+"','dd/MM/yyyy')) ");
	    sbQuery.append("          or ((CASE WHEN mo.TERM_SETTLEMENT_DATE is not null and mo.OPERATION_STATE=615 then trunc(mo.TERM_SETTLEMENT_DATE) end)<=TO_DATE('"+filter.getRegistryDate()+"','dd/MM/yyyy'))) ");
	    sbQuery.append("          order by ");
	    sbQuery.append("          se.ID_SECURITY_CODE_PK, ");
	    sbQuery.append("          nme.ID_NEGOTIATION_MECHANISM_PK, ");
	    sbQuery.append("          nmo.ID_NEGOTIATION_MODALITY_PK, ");
	    sbQuery.append("          mo.OPERATION_DATE, ");
	    sbQuery.append("          mo.OPERATION_NUMBER ");
	    sbQuery.append("  ");
	    
	 return sbQuery.toString();
  }
  
  /**
   * Gets the header corporative operation.
   *
   * @param filter the filter
   * @return the header corporative operation
   */
  public Object[] getHeaderCorporativeOperation(CorporativeOperationTO filter){
    StringBuilder sbQuery = new StringBuilder();

    sbQuery.append("select i.id_issuer_pk||'-'||i.business_name as emisor,");
    sbQuery.append("       s.id_isin_code_pk||'-'||s.description as valor,");
    sbQuery.append("       s.current_nominal_value as nominal,");
    sbQuery.append("       nvl(co.tax_factor,0) impuesto,");
    sbQuery.append("       co.delivery_factor interes,");
    sbQuery.append("       decode(ind_round,0,'NO',1,'SI') redondeo,s.id_issuance_code_fk,");
    sbQuery.append("       to_char(co.registry_date,'dd/mm/yyyy'),");
    sbQuery.append("       to_char(co.cutoff_date,'dd/mm/yyyy'),  ");
    sbQuery.append("       to_char(co.delivery_date,'dd/mm/yyyy'), ");
    sbQuery.append("       s.alternative_code,");
    sbQuery.append("       curr.parameter_name");
    sbQuery.append("  from corporative_operation co,");
    sbQuery.append("       security s, ");
    sbQuery.append("       issuer i,");
    sbQuery.append("       parameter_table p,");
    sbQuery.append("       parameter_table curr");
    sbQuery.append("  where co.id_origin_isin_code_fk=s.id_isin_code_pk");
    sbQuery.append("    and s.currency=curr.parameter_table_pk");
    sbQuery.append("    and co.id_issuer_fk=i.id_issuer_pk");
    sbQuery.append("    and co.currency=p.parameter_table_pk");
    sbQuery.append("    and co.cutoff_date=:cutoff_date");
    sbQuery.append("    and co.delivery_date=:delivery_date");
    sbQuery.append("    and co.state=:state");
    sbQuery.append("    and co.corporative_event_type=:event_type");
    sbQuery.append("    and co.id_origin_isin_code_fk=:isin_code");

    Query query = em.createNativeQuery(sbQuery.toString());
    query.setParameter("cutoff_date", filter.getCutOffDate());
    query.setParameter("delivery_date", filter.getDeliveryDate());
    query.setParameter("state", filter.getState());
    query.setParameter("event_type", filter.getCorporativeEventType());
    query.setParameter("isin_code", filter.getSourceIsinCode());

    try{
      Object[] temp = (Object[]) query.getSingleResult();
      return temp;
    }catch(NoResultException ex){
      return null;

    }
  }
  
	/**
	 * Gets the USD exchange rate.
	 *
	 * @return the USD exchange rate
	 * @throws ServiceException the service exception
	 */
	public BigDecimal getUSDExchangeRate()throws ServiceException{
		StringBuffer sbQuery=new StringBuffer();
		sbQuery.append(" SELECT REFERENCE_PRICE");
		sbQuery.append("   FROM DAILY_EXCHANGE_RATES");
		sbQuery.append("  WHERE TRUNC(DATE_RATE)=TRUNC(sysdate)");
		sbQuery.append("    AND ID_CURRENCY=:currency");

		Query query = em.createNativeQuery(sbQuery.toString());

		query.setParameter("currency", CurrencyType.USD.getCode());	

		
		try{
			return (BigDecimal) query.getSingleResult();
		}catch(NoResultException nrException){
			throw new ServiceException(ErrorServiceType.NOT_EXCHANGE_RATE);
 		}
		
 	}
	
	/**
	 * Gets the evidence of earned income.
	 *
	 * @param filter the filter
	 * @return the evidence of earned income
	 */
	public List<Object[]> getEvidenceOfEarnedIncome(EvidenceOfPerceivedIncomeTO filter){
	    StringBuilder sbQuery = new StringBuilder();
	    
	    sbQuery.append("	  SELECT (SELECT ALTERNATE_CODE  ");
		sbQuery.append(" 			    FROM HOLDER_ACCOUNT ");
		sbQuery.append(" 			   WHERE ID_HOLDER_ACCOUNT_PK = HAB.ID_HOLDER_ACCOUNT_PK) AS CUENTA_VAL, ") ; // [0]
		sbQuery.append(" 			FN_GET_HOLDER_DESC(HAB.ID_HOLDER_ACCOUNT_PK,',',0) AS TITULAR,  "); // [1]
		sbQuery.append(" 			(SELECT DESCRIPTION  ") ; 
		sbQuery.append(" 			   FROM ISSUANCE ") ; 
		sbQuery.append(" 			  WHERE ID_ISSUANCE_CODE_PK=SEC.ID_ISSUANCE_CODE_FK) AS EMISION, ") ; // [2] 
		sbQuery.append(" 			(SELECT DESCRIPTION "
									+ "FROM PARAMETER_TABLE "
									+ "WHERE PARAMETER_TABLE_PK=SEC.CURRENCY) || '/' || SEC.ID_ISIN_CODE_PK || '/' || SEC.CFI_CODE AS VALOR, ") ; // [3]
		sbQuery.append(" 			TO_CHAR(SEC.EXPIRATION_DATE,'dd/mm/yyyy') AS VENCIMIENTO, ") ; // [4] 
		sbQuery.append(" 			SEC.CURRENT_NOMINAL_VALUE as VALOR_NOMINAL, ") ; // [5]
		sbQuery.append(" 			(SEC.CURRENT_NOMINAL_VALUE * HAB.TOTAL_BALANCE) AS MONTO_INVERSION, ") ; // [6]
		sbQuery.append(" 			HAB.TOTAL_BALANCE, ") ; // [7]
		sbQuery.append(" 			(SELECT DESCRIPTION ") ; 
		sbQuery.append(" 			   FROM PARTICIPANT ") ; 
		sbQuery.append(" 			  WHERE ID_PARTICIPANT_PK=HAB.ID_PARTICIPANT_PK) as PARTICIPANTE, ") ; // [8]
		sbQuery.append(" 			(SELECT SUM(TOTAL_BALANCE) ") ; 
		sbQuery.append(" 			   FROM CORPORATIVE_PROCESS_RESULT CPR, CORPORATIVE_OPERATION CO ") ; 
		sbQuery.append(" 			  WHERE CPR.ID_HOLDER_ACCOUNT_FK = HAB.ID_HOLDER_ACCOUNT_PK ") ;
		sbQuery.append(" 			    AND CPR.ID_PARTICIPANT_FK = HAB.ID_PARTICIPANT_PK ") ;
		sbQuery.append(" 			    AND CPR.ID_ISIN_CODE_FK = HAB.ID_ISIN_CODE_PK ") ; 
		sbQuery.append(" 			    AND CO.ID_CORPORATIVE_OPERATION_PK=CPR.ID_CORPORATIVE_OPERATION_FK ") ;
		sbQuery.append(" 			    AND CO.IND_PAYED = 1") ;
		sbQuery.append(" 			    AND TRUNC(CO.DELIVERY_DATE)>=TRUNC(:initialDate) ") ; 
		sbQuery.append(" 			    AND TRUNC(CO.DELIVERY_DATE)<=TRUNC(:endDate) ) INTERESES_PERCIBIDOS, ") ; // [9]
		sbQuery.append(" 			    (SELECT PARAMETER_NAME FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK=H.DOCUMENT_TYPE) TIPO_DOCUMENTO, ") ; // [10]
		sbQuery.append(" 			    H.DOCUMENT_NUMBER, ") ; // [11]		
		sbQuery.append(" 			((SEC.CURRENT_NOMINAL_VALUE) * (SELECT SUM(CPR.PAWN_BALANCE + CPR.BAN_BALANCE + CPR.OTHER_BLOCK_BALANCE + CPR.OPPOSITION_BALANCE) ") ; 
		sbQuery.append(" 			   FROM CORPORATIVE_PROCESS_RESULT CPR, CORPORATIVE_OPERATION CO ") ; 
		sbQuery.append(" 			  WHERE CPR.ID_HOLDER_ACCOUNT_FK = HAB.ID_HOLDER_ACCOUNT_PK ") ;
		sbQuery.append(" 			    AND CPR.ID_PARTICIPANT_FK = HAB.ID_PARTICIPANT_PK ") ;
		sbQuery.append(" 			    AND CPR.ID_ISIN_CODE_FK = HAB.ID_ISIN_CODE_PK ") ; 
		sbQuery.append(" 			    AND CO.ID_CORPORATIVE_OPERATION_PK=CPR.ID_CORPORATIVE_OPERATION_FK ") ;
		sbQuery.append(" 			    AND CO.IND_PAYED = 1") ;
		sbQuery.append(" 			    AND TRUNC(CO.DELIVERY_DATE)>=TRUNC(:initialDate) ") ; 
		sbQuery.append(" 			    AND TRUNC(CO.DELIVERY_DATE)<=TRUNC(:endDate) )) BALANCE_BLOQUEADO, ") ; // [12]		 
		sbQuery.append(" 			    (SELECT BUSINESS_NAME FROM ISSUER WHERE ID_ISSUER_PK=SEC.ID_ISSUER_FK ) EMISOR ,") ; // [13]
		sbQuery.append(" 			    (SEC.INTEREST_RATE || '%') AS INTERES,") ; // [14]
		sbQuery.append(" 			    HAB.ID_HOLDER_ACCOUNT_PK,") ; // [15]
		sbQuery.append(" 			    SEC.DESCRIPTION,") ; // [16]
		sbQuery.append(" 			    TO_CHAR(SEC.ISSUANCE_DATE,'dd/mm/yyyy'),") ; // [17]
		sbQuery.append(" 			    SEC.INSTRUMENT_TYPE") ; // [18]
	    sbQuery.append("     FROM SECURITY SEC ");
	    sbQuery.append(" INNER JOIN HOLDER_ACCOUNT_BALANCE HAB ");
	    sbQuery.append("     ON HAB.ID_ISIN_CODE_PK = SEC.ID_ISIN_CODE_PK ");
	    sbQuery.append(" INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ");
	    sbQuery.append("       ON HAD.ID_HOLDER_ACCOUNT_FK = HAB.ID_HOLDER_ACCOUNT_PK ");
	    sbQuery.append(" INNER JOIN HOLDER H ");
	    sbQuery.append("     ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK "); 
	    sbQuery.append("      WHERE HAB.TOTAL_BALANCE > 0  ");  
	    sbQuery.append("      AND HAD.IND_REPRESENTATIVE=1 ");
	    sbQuery.append(" AND EXISTS (SELECT 'X' ");
	    sbQuery.append("          FROM CORPORATIVE_OPERATION C");
	    sbQuery.append("         WHERE C.ID_ORIGIN_ISIN_CODE_FK = SEC.ID_ISIN_CODE_PK");        
	    sbQuery.append("           AND TRUNC(C.DELIVERY_DATE) BETWEEN TRUNC(:initialDate) AND TRUNC(:endDate))");
	    
	    if(Validations.validateIsNotNull(filter.getIdParticipantPk())){
	      sbQuery.append("      AND HAB.ID_PARTICIPANT_PK=:participant");
	    }
	    if(Validations.validateIsNotNull(filter.getIdHolderAccountPk())){
	      sbQuery.append("      AND HAB.ID_HOLDER_ACCOUNT_PK=:holderAccount ");
	    }
	    if(Validations.validateIsNotNull(filter.getIdHolderPk())){
	      sbQuery.append("      AND H.ID_HOLDER_PK = :holder ");
	      
	    }   
	    if(Validations.validateIsNotNull(filter.getIdIssuanceCodePk())){
	      sbQuery.append("      AND SEC.ID_ISSUER_FK = :issuer ");
	    }
	    if(Validations.validateIsNotNull(filter.getIdIsinCodePk())){
	      sbQuery.append("      AND SEC.ID_ISIN_CODE_PK = :isin_code ");
	    }
	    
	      
	    
	    Query query = em.createNativeQuery(sbQuery.toString());
	    
	    if(Validations.validateIsNotNull(filter.getIdParticipantPk())){
	      query.setParameter("participant", filter.getIdParticipantPk()); 
	    }
	    if(Validations.validateIsNotNull(filter.getIdHolderAccountPk())){
	      query.setParameter("holderAccount", filter.getIdHolderAccountPk()); 
	    }
	    
	    if(Validations.validateIsNotNull(filter.getIdHolderPk())){
	      query.setParameter("holder", filter.getIdHolderPk());
	    }
	    if(Validations.validateIsNotNull(filter.getIdIssuanceCodePk())){
	      query.setParameter("issuer", filter.getIdIssuanceCodePk());
	    }
	            
	    if(Validations.validateIsNotNull(filter.getIdIsinCodePk())){
	      query.setParameter("isin_code", filter.getIdIsinCodePk());
	    }
	    
	    query.setParameter("initialDate", filter.getInitialDate());
	    query.setParameter("endDate", filter.getEndDate()); 
	    return query.getResultList();
	  }
	
	/**
	 * Gets the query calculation of stock reported by brokers report.
	 *
	 * @param gfto the gfto
	 * @param idStkCalcProcess the id stk calc process
	 * @return the query calculation of stock reported by brokers report
	 */
	public String getQueryCalculationOfStockReportedByBrokersReport(GenericCorporativesTO gfto, Long idStkCalcProcess){
    	StringBuilder sbQuery= new StringBuilder();
    	
			  sbQuery.append(" select "); 
			  sbQuery.append(" scp.CUTOFF_DATE FECHA_CORTE, ");
			  sbQuery.append(" scp.REGISTRY_DATE FECHA_REGISTRO, ");
			  sbQuery.append(" scb.ID_SECURITY_CODE_FK VALOR, ");
			  sbQuery.append(" scb.ID_PARTICIPANT_FK || ' - ' || p.DESCRIPTION PARTICIPANTE, "); 
			  sbQuery.append(" ha.ACCOUNT_NUMBER CUENTA, ");
			  sbQuery.append(" scb.REPORTED_BALANCE SALDO_REPORTADO, ");
			  sbQuery.append(" HAD.ID_HOLDER_ACCOUNT_DETAIL_PK ");
			  sbQuery.append(" from  ");
			  sbQuery.append(" STOCK_CALCULATION_PROCESS scp, STOCK_CALCULATION_BALANCE scb, HOLDER_ACCOUNT ha, HOLDER_ACCOUNT_DETAIL had,PARTICIPANT p ");
			  sbQuery.append(" where "); 
			  sbQuery.append(" scp.ID_STOCK_CALCULATION_PK = scb.ID_STOCK_CALCULATION_FK ");
			  sbQuery.append(" and scb.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK ");
			  sbQuery.append(" and had.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK ");
			  sbQuery.append(" and scb.ID_PARTICIPANT_FK = p.ID_PARTICIPANT_PK ");
			  sbQuery.append(" and scp.STOCK_STATE = 1327 ");
			  sbQuery.append(" and had.STATE_ACCOUNT_DETAIL = 1174 ");
			  sbQuery.append(" and had.IND_REPRESENTATIVE = 1 ");
			  sbQuery.append(" and scb.REPORTED_BALANCE > 0 ");
			  
			  if(Validations.validateIsNotNull(gfto.getSecurities())){
    			  sbQuery.append(" and scb.ID_SECURITY_CODE_FK ='"+gfto.getSecurities().toString()+"' ");
			  }
			  if(Validations.validateIsNotNull(gfto.getCutDate())){
		  		  sbQuery.append(" and trunc(TO_DATE(scp.CUTOFF_DATE,'dd/MM/yyyy')) = trunc(TO_DATE('"+gfto.getCutDate()+"','dd/MM/yyyy')) ");
			  }
			  if(Validations.validateIsNotNull(gfto.getRegistryDate())){
    			  sbQuery.append(" and trunc(TO_DATE(scp.REGISTRY_DATE,'dd/MM/yyyy')) = trunc(TO_DATE('"+gfto.getRegistryDate()+"','dd/MM/yyyy')) ");
			  }
			  if(Validations.validateIsNotNull(idStkCalcProcess)){
		    		sbQuery.append(" AND  SCB.ID_STOCK_CALCULATION_FK="+idStkCalcProcess+" ");
			  }
//    			  sbQuery.append(" --and scp.STOCK_TYPE = $tipStock ");
	  		  sbQuery.append(" order by scb.ID_PARTICIPANT_FK,HA.ACCOUNT_NUMBER ");
    		  sbQuery.append("  ");
    	
    	return sbQuery.toString();
	}
	
	/**
	 * Gets the query pay of interest on h account to participants report.
	 *
	 * @param gfto the gfto
	 * @return the query pay of interest on h account to participants report
	 */
	public String getQueryPayOfInterestOnHAccountToParticipantsReport(GenericCorporativesTO gfto){
    	StringBuilder sbQuery= new StringBuilder();
    	
		  sbQuery.append(" select "); 
		  sbQuery.append(" iss.MNEMONIC || ' - ' || ISS.BUSINESS_NAME EMISOR, ");
		  sbQuery.append(" s.ID_ISSUANCE_CODE_FK EMISION, ");
		  sbQuery.append(" s.ID_SECURITY_CODE_PK VALOR, ");
		  sbQuery.append(" S.SECURITY_CLASS, ");
		  sbQuery.append(" co.CURRENCY MONEDA, ");
		  sbQuery.append(" co.CUTOFF_DATE FECHA_CORTE, ");
		  sbQuery.append(" co.REGISTRY_DATE FECHA_REGISTRO, ");
		  sbQuery.append(" pic.COUPON_NUMBER NUMERO_CUPON, ");
		  sbQuery.append(" s.CURRENT_NOMINAL_VALUE VALOR_NOMINAL, ");
		  sbQuery.append(" co.DELIVERY_FACTOR FACTOR_INTERES, ");
		  sbQuery.append(" co.TAX_FACTOR IMPUESTO, ");
		  sbQuery.append(" decode(co.IND_ROUND,1,'SI','NO') REDONDEO, ");
		  sbQuery.append(" P.MNEMONIC || ' - ' || p.ID_PARTICIPANT_PK || ' - ' || p.DESCRIPTION  PARTICIPANTE, ");
		  sbQuery.append(" ha.ACCOUNT_NUMBER NUMERO_CUENTA, ");
		  sbQuery.append(" scb.TOTAL_BALANCE SALDO_TOTAL, ");
		  sbQuery.append(" scb.AVAILABLE_BALANCE SALDO_DISPONIBLE, ");
		  sbQuery.append(" cpr.TOTAL_BALANCE INTERES_BRUTO, ");
		  sbQuery.append(" cpr.CUSTODY_AMOUNT MONTO_CUSTODIA, ");
		  sbQuery.append(" cpr.TAX_AMOUNT RETENCION, ");
		  sbQuery.append(" (cpr.TOTAL_BALANCE - cpr.CUSTODY_AMOUNT - cpr.TAX_AMOUNT) INTERES_NETO_CONTABLE, ");
		  sbQuery.append(" (cpr.AVAILABLE_BALANCE - cpr.CUSTODY_AMOUNT - cpr.TAX_AMOUNT) INTERES_NETO_DISPONIBLE, ");
		  sbQuery.append(" HAD.ID_HOLDER_ACCOUNT_DETAIL_PK, ");
		  sbQuery.append(" CO.STATE ");
		  sbQuery.append(" from "); 
		  sbQuery.append(" STOCK_CALCULATION_PROCESS scp, STOCK_CALCULATION_BALANCE scb, HOLDER_ACCOUNT ha, HOLDER_ACCOUNT_DETAIL had,PARTICIPANT p, ");
		  sbQuery.append(" CORPORATIVE_OPERATION co, ISSUER iss, SECURITY s, PROGRAM_INTEREST_COUPON pic, CORPORATIVE_PROCESS_RESULT cpr ");
		  sbQuery.append(" where "); 
		  sbQuery.append(" co.ID_ISSUER_FK = iss.ID_ISSUER_PK ");
		  sbQuery.append(" and co.ID_ORIGIN_SECURITY_CODE_FK = s.ID_SECURITY_CODE_PK ");
		  sbQuery.append(" and scp.ID_STOCK_CALCULATION_PK = scb.ID_STOCK_CALCULATION_FK ");
		  sbQuery.append(" and scb.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK ");
		  sbQuery.append(" and had.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK ");
		  sbQuery.append(" and scb.ID_PARTICIPANT_FK = p.ID_PARTICIPANT_PK ");
		  sbQuery.append(" and co.ID_PROGRAM_INTEREST_FK = pic.ID_PROGRAM_INTEREST_PK ");
		  sbQuery.append(" and co.ID_CORPORATIVE_OPERATION_PK = cpr.ID_CORPORATIVE_OPERATION_FK ");
		  sbQuery.append(" and trunc(co.CUTOFF_DATE) = trunc(scp.CUTOFF_DATE) ");
		  sbQuery.append(" and trunc(co.REGISTRY_DATE) = trunc(scp.REGISTRY_DATE) ");
		  sbQuery.append(" and co.ID_ORIGIN_SECURITY_CODE_FK = scb.ID_SECURITY_CODE_FK ");
		  sbQuery.append(" and scp.STOCK_STATE = 1327 ");
		  sbQuery.append(" and had.STATE_ACCOUNT_DETAIL = 1174 ");
		  sbQuery.append(" and had.IND_REPRESENTATIVE = 1 ");
		  sbQuery.append(" and scp.STOCK_TYPE = 1626 ");
		  sbQuery.append(" and cpr.IND_ORIGIN_TARGET = 2 ");

		  if(Validations.validateIsNotNull(gfto.getProcessId())){
	  		  sbQuery.append(" and CO.ID_CORPORATIVE_OPERATION_PK = "+gfto.getProcessId()+" ");
		  }
		  if(Validations.validateIsNotNull(gfto.getCutDate())){
	  		  sbQuery.append(" and trunc(TO_DATE(scp.CUTOFF_DATE,'dd/MM/yyyy')) = trunc(TO_DATE('"+gfto.getCutDate()+"','dd/MM/yyyy')) ");
		  }
		  if(Validations.validateIsNotNull(gfto.getRegistryDate())){
			  sbQuery.append(" and trunc(TO_DATE(scp.REGISTRY_DATE,'dd/MM/yyyy')) = trunc(TO_DATE('"+gfto.getRegistryDate()+"','dd/MM/yyyy')) ");
		  }
		  if(Validations.validateIsNotNull(gfto.getEventType())){
			  sbQuery.append(" and co.CORPORATIVE_EVENT_TYPE ="+gfto.getEventType()+" ");
		  }
		  if(Validations.validateIsNotNull(gfto.getSecurities())){
			  sbQuery.append(" and scb.ID_SECURITY_CODE_FK ='"+gfto.getSecurities().toString()+"' ");
		  }
		  if(Validations.validateIsNotNull(gfto.getState())){
			  sbQuery.append(" and CO.STATE ="+gfto.getState()+" ");
		  }
		  
//		  sbQuery.append(" --and scp.STOCK_TYPE = $tipStock ");
		  sbQuery.append(" order by HA.ACCOUNT_NUMBER,HAD.ID_HOLDER_FK,scb.ID_PARTICIPANT_FK ");
		  sbQuery.append("  ");

		return sbQuery.toString();
	}
	
	/**
	 * Gets the query pay of amortization on h account to participants report.
	 *
	 * @param gfto the gfto
	 * @return the query pay of amortization on h account to participants report
	 */
	public String getQueryPayOfAmortizationOnHAccountToParticipantsReport(GenericCorporativesTO gfto){
    	StringBuilder sbQuery= new StringBuilder();
    	
		  sbQuery.append(" select "); 
		  sbQuery.append(" iss.MNEMONIC || ' - ' || ISS.BUSINESS_NAME EMISOR, ");
		  sbQuery.append(" s.ID_ISSUANCE_CODE_FK EMISION, ");
		  sbQuery.append(" s.ID_SECURITY_CODE_PK VALOR, ");
		  sbQuery.append(" S.SECURITY_CLASS, ");
		  sbQuery.append(" co.CURRENCY MONEDA, ");
		  sbQuery.append(" co.CUTOFF_DATE FECHA_CORTE, ");
		  sbQuery.append(" co.REGISTRY_DATE FECHA_REGISTRO, ");
		  sbQuery.append(" pac.COUPON_NUMBER NUMERO_CUPON, ");
		  sbQuery.append(" s.CURRENT_NOMINAL_VALUE VALOR_NOMINAL, ");
		  sbQuery.append(" co.DELIVERY_FACTOR FACTOR_INTERES, ");
		  sbQuery.append(" co.TAX_FACTOR IMPUESTO, ");
		  sbQuery.append(" decode(co.IND_ROUND,1,'SI','NO') REDONDEO, ");
		  sbQuery.append(" P.MNEMONIC || ' - ' || p.ID_PARTICIPANT_PK || ' - ' || p.DESCRIPTION  PARTICIPANTE, ");
		  sbQuery.append(" ha.ACCOUNT_NUMBER NUMERO_CUENTA, ");
		  sbQuery.append(" scb.TOTAL_BALANCE SALDO_TOTAL, ");
		  sbQuery.append(" scb.AVAILABLE_BALANCE SALDO_DISPONIBLE, ");
		  sbQuery.append(" cpr.TOTAL_BALANCE INTERES_BRUTO, ");
		  sbQuery.append(" cpr.CUSTODY_AMOUNT MONTO_CUSTODIA, ");
		  sbQuery.append(" cpr.TAX_AMOUNT RETENCION, ");
		  sbQuery.append(" (cpr.TOTAL_BALANCE - cpr.CUSTODY_AMOUNT - cpr.TAX_AMOUNT) INTERES_NETO_CONTABLE, ");
		  sbQuery.append(" (cpr.AVAILABLE_BALANCE - cpr.CUSTODY_AMOUNT - cpr.TAX_AMOUNT) INTERES_NETO_DISPONIBLE, ");
		  sbQuery.append(" HAD.ID_HOLDER_ACCOUNT_DETAIL_PK, ");
		  sbQuery.append(" CO.STATE ");
		  sbQuery.append(" from "); 
		  sbQuery.append(" STOCK_CALCULATION_PROCESS scp, STOCK_CALCULATION_BALANCE scb, HOLDER_ACCOUNT ha, HOLDER_ACCOUNT_DETAIL had,PARTICIPANT p, ");
		  sbQuery.append(" CORPORATIVE_OPERATION co, ISSUER iss, SECURITY s, PROGRAM_AMORTIZATION_COUPON pac, CORPORATIVE_PROCESS_RESULT cpr ");
		  sbQuery.append(" where "); 
		  sbQuery.append(" co.ID_ISSUER_FK = iss.ID_ISSUER_PK ");
		  sbQuery.append(" and co.ID_ORIGIN_SECURITY_CODE_FK = s.ID_SECURITY_CODE_PK ");
		  sbQuery.append(" and scp.ID_STOCK_CALCULATION_PK = scb.ID_STOCK_CALCULATION_FK ");
		  sbQuery.append(" and scb.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK ");
		  sbQuery.append(" and had.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK ");
		  sbQuery.append(" and scb.ID_PARTICIPANT_FK = p.ID_PARTICIPANT_PK ");
		  sbQuery.append(" and co.ID_PROGRAM_AMORTIZATION_FK = pac.ID_PROGRAM_AMORTIZATION_PK ");
		  sbQuery.append(" and co.ID_CORPORATIVE_OPERATION_PK = cpr.ID_CORPORATIVE_OPERATION_FK ");
		  sbQuery.append(" and trunc(co.CUTOFF_DATE) = trunc(scp.CUTOFF_DATE) ");
		  sbQuery.append(" and trunc(co.REGISTRY_DATE) = trunc(scp.REGISTRY_DATE) ");
		  sbQuery.append(" and co.ID_ORIGIN_SECURITY_CODE_FK = scb.ID_SECURITY_CODE_FK ");
		  sbQuery.append(" and scp.STOCK_STATE = 1327 ");
		  sbQuery.append(" and had.STATE_ACCOUNT_DETAIL = 1174 ");
		  sbQuery.append(" and had.IND_REPRESENTATIVE = 1 ");
		  sbQuery.append(" and scp.STOCK_TYPE = 1626 ");
		  sbQuery.append(" and cpr.IND_ORIGIN_TARGET = 2 ");

		  if(Validations.validateIsNotNull(gfto.getProcessId())){
	  		  sbQuery.append(" and CO.ID_CORPORATIVE_OPERATION_PK = "+gfto.getProcessId()+" ");
		  }
		  if(Validations.validateIsNotNull(gfto.getCutDate())){
	  		  sbQuery.append(" and trunc(TO_DATE(scp.CUTOFF_DATE,'dd/MM/yyyy')) = trunc(TO_DATE('"+gfto.getCutDate()+"','dd/MM/yyyy')) ");
		  }
		  if(Validations.validateIsNotNull(gfto.getRegistryDate())){
			  sbQuery.append(" and trunc(TO_DATE(scp.REGISTRY_DATE,'dd/MM/yyyy')) = trunc(TO_DATE('"+gfto.getRegistryDate()+"','dd/MM/yyyy')) ");
		  }
		  if(Validations.validateIsNotNull(gfto.getEventType())){
			  sbQuery.append(" and co.CORPORATIVE_EVENT_TYPE ="+gfto.getEventType()+" ");
		  }
		  if(Validations.validateIsNotNull(gfto.getSecurities())){
			  sbQuery.append(" and scb.ID_SECURITY_CODE_FK ='"+gfto.getSecurities().toString()+"' ");
		  }
		  if(Validations.validateIsNotNull(gfto.getState())){
			  sbQuery.append(" and CO.STATE ="+gfto.getState()+" ");
		  }
		  
//		  sbQuery.append(" --and scp.STOCK_TYPE = $tipStock ");
		  sbQuery.append(" order by HA.ACCOUNT_NUMBER,HAD.ID_HOLDER_FK,scb.ID_PARTICIPANT_FK ");
		  sbQuery.append("  ");

		return sbQuery.toString();
	}
	

	/**
	 * Gets the query pay interest for op reported and warranty margin report.
	 *
	 * @param gfto the gfto
	 * @return the query pay interest for op reported and warranty margin report
	 */
	public String getQueryPayInterestForOpReportedAndWarrantyMarginReport(GenericCorporativesTO gfto){
    	StringBuilder sbQuery= new StringBuilder();
    	
			  sbQuery.append(" select "); 
			  sbQuery.append(" iss.MNEMONIC || ' - ' || ISS.BUSINESS_NAME EMISOR, ");
			  sbQuery.append(" s.ID_ISSUANCE_CODE_FK EMISION, ");
			  sbQuery.append(" s.ID_SECURITY_CODE_PK VALOR, ");
			  sbQuery.append(" S.SECURITY_CLASS, ");
			  sbQuery.append(" co.CURRENCY MONEDA, ");
			  sbQuery.append(" co.CUTOFF_DATE FECHA_CORTE, ");
			  sbQuery.append(" co.DELIVERY_DATE FECHA_PAGO, ");
			  sbQuery.append(" co.REGISTRY_DATE FECHA_REGISTRO, "); 
			  sbQuery.append(" pac.COUPON_NUMBER NUMERO_CUPON, ");
			  sbQuery.append(" s.CURRENT_NOMINAL_VALUE VALOR_NOMINAL, ");
			  sbQuery.append(" co.DELIVERY_FACTOR FACTOR_INTERES, ");
			  sbQuery.append(" co.TAX_FACTOR IMPUESTO, ");
			  sbQuery.append(" decode(co.IND_ROUND,1,'SI','NO') REDONDEO, ");
			  sbQuery.append(" P.MNEMONIC || ' - ' || p.ID_PARTICIPANT_PK || ' - ' || p.DESCRIPTION  PARTICIPANTE, ");
			  sbQuery.append(" ha.ACCOUNT_NUMBER NUMERO_CUENTA, ");
			  sbQuery.append(" scb.TOTAL_BALANCE SALDO_TOTAL, ");
			  sbQuery.append(" (scb.REPORTING_BALANCE+scb.REPORTED_BALANCE) SALDO_REPORTO, ");
			  sbQuery.append(" scb.MARGIN_BALANCE SALDO_GARANTIA, ");
			  sbQuery.append(" (cpr.TOTAL_BALANCE-cpr.TAX_AMOUNT-cpr.CUSTODY_AMOUNT) MONTO_CONTABLE , ");
			  sbQuery.append(" (cpr.REPORTED_BALANCE+cpr.REPORTING_BALANCE) MONTO_REPORTO, ");
			  sbQuery.append(" cpr.MARGIN_BALANCE MONTO_GARANTIA, ");
			  sbQuery.append(" HAD.ID_HOLDER_ACCOUNT_DETAIL_PK ");
			  sbQuery.append(" from "); 
			  sbQuery.append(" STOCK_CALCULATION_PROCESS scp, STOCK_CALCULATION_BALANCE scb, HOLDER_ACCOUNT ha, HOLDER_ACCOUNT_DETAIL had,PARTICIPANT p, ");
			  sbQuery.append(" CORPORATIVE_OPERATION co, ISSUER iss, SECURITY s, PROGRAM_AMORTIZATION_COUPON pac, CORPORATIVE_PROCESS_RESULT cpr ");
			  sbQuery.append(" where "); 
			  sbQuery.append(" co.ID_ISSUER_FK = iss.ID_ISSUER_PK ");
			  sbQuery.append(" and co.ID_ORIGIN_SECURITY_CODE_FK = s.ID_SECURITY_CODE_PK ");
			  sbQuery.append(" and scp.ID_STOCK_CALCULATION_PK = scb.ID_STOCK_CALCULATION_FK ");
			  sbQuery.append(" and scb.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK ");
			  sbQuery.append(" and had.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK ");
			  sbQuery.append(" and scb.ID_PARTICIPANT_FK = p.ID_PARTICIPANT_PK ");
			  sbQuery.append(" and co.ID_PROGRAM_AMORTIZATION_FK = pac.ID_PROGRAM_AMORTIZATION_PK ");
			  sbQuery.append(" and co.ID_CORPORATIVE_OPERATION_PK = cpr.ID_CORPORATIVE_OPERATION_FK ");
			  sbQuery.append(" and trunc(co.CUTOFF_DATE) = trunc(scp.CUTOFF_DATE) ");
			  sbQuery.append(" and trunc(co.REGISTRY_DATE) = trunc(scp.REGISTRY_DATE) ");
			  sbQuery.append(" and co.ID_ORIGIN_SECURITY_CODE_FK = scb.ID_SECURITY_CODE_FK ");
			  sbQuery.append(" and scp.STOCK_STATE = 1327 ");
			  sbQuery.append(" and had.STATE_ACCOUNT_DETAIL = 1174 ");
			  sbQuery.append(" and had.IND_REPRESENTATIVE = 1 ");
			  sbQuery.append(" and scp.STOCK_TYPE = 1626 ");
			  sbQuery.append(" and cpr.IND_ORIGIN_TARGET = 2 ");
			  sbQuery.append(" and (cpr.REPORTED_BALANCE>0 or cpr.REPORTING_BALANCE>0 or cpr.MARGIN_BALANCE>0) ");
    	
		  if(Validations.validateIsNotNull(gfto.getProcessId())){
	  		  sbQuery.append(" and CO.ID_CORPORATIVE_OPERATION_PK = "+gfto.getProcessId()+" ");
		  }
		  if(Validations.validateIsNotNull(gfto.getCutDate())){
	  		  sbQuery.append(" and trunc(TO_DATE(scp.CUTOFF_DATE,'dd/MM/yyyy')) = trunc(TO_DATE('"+gfto.getCutDate()+"','dd/MM/yyyy')) ");
		  }
		  if(Validations.validateIsNotNull(gfto.getRegistryDate())){
			  sbQuery.append(" and trunc(TO_DATE(scp.REGISTRY_DATE,'dd/MM/yyyy')) = trunc(TO_DATE('"+gfto.getRegistryDate()+"','dd/MM/yyyy')) ");
		  }
		  if(Validations.validateIsNotNull(gfto.getEventType())){
			  sbQuery.append(" and co.CORPORATIVE_EVENT_TYPE ="+gfto.getEventType()+" ");
		  }
		  if(Validations.validateIsNotNull(gfto.getSecurities())){
			  sbQuery.append(" and scb.ID_SECURITY_CODE_FK ='"+gfto.getSecurities().toString()+"' ");
		  }
		  if(Validations.validateIsNotNull(gfto.getState())){
			  sbQuery.append(" and CO.STATE ="+gfto.getState()+" ");
		  }
		  
//			  sbQuery.append(" --and scp.STOCK_TYPE = $tipStock ");
			  sbQuery.append(" order by HA.ACCOUNT_NUMBER,HAD.ID_HOLDER_FK,scb.ID_PARTICIPANT_FK ");
			  sbQuery.append("  ");

		return sbQuery.toString();
	}


	/**
	 * Gets the query pay interest on balanece blocked report.
	 *
	 * @param gfto the gfto
	 * @return the query pay interest on balanece blocked report
	 */
	public String getQueryPayInterestOnBalaneceBlockedReport(GenericCorporativesTO gfto){
    	StringBuilder sbQuery= new StringBuilder();
    	
			  sbQuery.append(" select "); 
			  sbQuery.append(" co.STATE ESTADO, ");
			  sbQuery.append(" iss.MNEMONIC || ' - ' || ISS.BUSINESS_NAME EMISOR, ");
			  sbQuery.append(" s.ID_ISSUANCE_CODE_FK EMISION, ");
			  sbQuery.append(" s.ID_SECURITY_CODE_PK VALOR, ");
			  sbQuery.append(" S.SECURITY_CLASS, ");
			  sbQuery.append(" co.CURRENCY MONEDA, ");
			  sbQuery.append(" co.CUTOFF_DATE FECHA_CORTE, ");
			  sbQuery.append(" co.DELIVERY_DATE FECHA_ENTREGA, ");
			  sbQuery.append(" co.REGISTRY_DATE FECHA_REGISTRO, ");
			  sbQuery.append(" pic.COUPON_NUMBER NUMERO_CUPON, ");
			  sbQuery.append(" co.TAX_FACTOR IMPUESTO, ");
			  sbQuery.append(" decode(co.IND_ROUND,1,'SI','NO') REDONDEO, ");
			  sbQuery.append(" s.CURRENT_NOMINAL_VALUE VALOR_NOMINAL, ");
			  sbQuery.append(" co.DELIVERY_FACTOR FACTOR_INTERES, ");
			  sbQuery.append(" P.ID_PARTICIPANT_PK || ' - ' || p.MNEMONIC || ' - ' || p.ID_PARTICIPANT_PK PARTICIPANTE, ");
			  sbQuery.append(" ha.ACCOUNT_NUMBER NUMERO_CUENTA, ");
			  sbQuery.append(" scb.PAWN_BALANCE STOCK_PRENDA, ");
			  sbQuery.append(" scb.BAN_BALANCE STOCK_EMBARGO, ");
			  sbQuery.append(" scb.OTHER_BLOCK_BALANCE STOCK_OTROS, ");
			  sbQuery.append(" SCB.TOTAL_BALANCE STOCK_TOTAL, ");
			  sbQuery.append(" cpr.PAWN_BALANCE INTERES_PRENDA, ");
			  sbQuery.append(" cpr.BAN_BALANCE INTERES_EMBARGO, ");
			  sbQuery.append(" cpr.OTHER_BLOCK_BALANCE INTERES_OTROS, ");
			  sbQuery.append(" CPR.TOTAL_BALANCE INTERES_TOTAL, ");
			  sbQuery.append(" HAD.ID_HOLDER_ACCOUNT_DETAIL_PK, ");
			  sbQuery.append(" CO.ID_CORPORATIVE_OPERATION_PK ");
			  sbQuery.append(" from "); 
			  sbQuery.append(" CORPORATIVE_OPERATION co, CORPORATIVE_PROCESS_RESULT cpr, ISSUER iss,SECURITY s, PROGRAM_INTEREST_COUPON pic, PARTICIPANT p, "); 
			  sbQuery.append(" HOLDER_ACCOUNT ha, HOLDER_ACCOUNT_DETAIL had,STOCK_CALCULATION_PROCESS scp, STOCK_CALCULATION_BALANCE scb ");
			  sbQuery.append(" where "); 
			  sbQuery.append(" co.ID_CORPORATIVE_OPERATION_PK = cpr.ID_CORPORATIVE_OPERATION_FK ");
			  sbQuery.append(" and scp.ID_STOCK_CALCULATION_PK = scb.ID_STOCK_CALCULATION_FK ");
			  sbQuery.append(" and trunc(scp.CUTOFF_DATE) = trunc(co.CUTOFF_DATE) ");
			  sbQuery.append(" and trunc(scp.REGISTRY_DATE) = trunc(co.REGISTRY_DATE) ");
			  sbQuery.append(" and co.ID_ISSUER_FK = iss.ID_ISSUER_PK ");
			  sbQuery.append(" and co.ID_PROGRAM_INTEREST_FK = pic.ID_PROGRAM_INTEREST_PK ");
			  sbQuery.append(" and co.ID_ORIGIN_SECURITY_CODE_FK = s.ID_SECURITY_CODE_PK ");
			  sbQuery.append(" and cpr.ID_PARTICIPANT_FK = p.ID_PARTICIPANT_PK ");
			  sbQuery.append(" and had.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK ");
			  sbQuery.append(" and had.STATE_ACCOUNT_DETAIL = 1174 ");
			  sbQuery.append(" and co.CORPORATIVE_EVENT_TYPE = 14 ");
			  sbQuery.append(" and scp.STOCK_TYPE = 1626 ");
			  sbQuery.append(" and scp.STOCK_STATE = 1327 ");
    	
    	
		  if(Validations.validateIsNotNull(gfto.getProcessId())){
	  		  sbQuery.append(" and CO.ID_CORPORATIVE_OPERATION_PK = "+gfto.getProcessId()+" ");
		  }
		  if(Validations.validateIsNotNull(gfto.getCutDate())){
	  		  sbQuery.append(" and trunc(TO_DATE(scp.CUTOFF_DATE,'dd/MM/yyyy')) = trunc(TO_DATE('"+gfto.getCutDate()+"','dd/MM/yyyy')) ");
		  }
		  if(Validations.validateIsNotNull(gfto.getRegistryDate())){
			  sbQuery.append(" and trunc(TO_DATE(scp.REGISTRY_DATE,'dd/MM/yyyy')) = trunc(TO_DATE('"+gfto.getRegistryDate()+"','dd/MM/yyyy')) ");
		  }
		  if(Validations.validateIsNotNull(gfto.getEventType())){
			  sbQuery.append(" and co.CORPORATIVE_EVENT_TYPE ="+gfto.getEventType()+" ");
		  }
		  if(Validations.validateIsNotNull(gfto.getSecurities())){
			  sbQuery.append(" and s.ID_SECURITY_CODE_PK ='"+gfto.getSecurities().toString()+"' ");
		  }
		  if(Validations.validateIsNotNull(gfto.getState())){
			  sbQuery.append(" and CO.STATE ="+gfto.getState()+" ");
		  }
		  
//		  sbQuery.append(" --and scp.STOCK_TYPE = $tipStock ");
		  sbQuery.append(" order by HA.ACCOUNT_NUMBER,HAD.ID_HOLDER_FK,scb.ID_PARTICIPANT_FK ");
		  sbQuery.append("  ");

	return sbQuery.toString();
}
	
	
}
