package com.pradera.report.generation.executor.settlement.to;

import java.io.Serializable;
import java.util.Date;

public class RegistrationAssignmentHoldersTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6509733311059672538L;
	
	
	private Long idHolderAccountPk;
	private Long idHolderPk;
	private Long participantPk;
	private Long modality;
	private Long mechanism;
	private String securityCode;
	private Integer schema;
	private String strDateInitial;
	private String participantDesc;
	private String securityClass;

	public Long getModality() {
		return modality;
	}
	public void setModality(Long modality) {
		this.modality = modality;
	}
	public Long getParticipantPk() {
		return participantPk;
	}
	public void setParticipantPk(Long participantPk) {
		this.participantPk = participantPk;
	}
	public Integer getSchema() {
		return schema;
	}
	public void setSchema(Integer schema) {
		this.schema = schema;
	}
	public String getParticipantDesc() {
		return participantDesc;
	}
	public void setParticipantDesc(String participantDesc) {
		this.participantDesc = participantDesc;
	}
	public String getSecurityCode() {
		return securityCode;
	}
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
	public String getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(String securityClass) {
		this.securityClass = securityClass;
	}

	public String getStrDateInitial() {
		return strDateInitial;
	}
	public void setStrDateInitial(String strDateInitial) {
		this.strDateInitial = strDateInitial;
	}
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}
	public Long getIdHolderPk() {
		return idHolderPk;
	}
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}
	public Long getMechanism() {
		return mechanism;
	}
	public void setMechanism(Long mechanism) {
		this.mechanism = mechanism;
	}
	

}
