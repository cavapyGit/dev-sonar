package com.pradera.report.generation.executor.corporative;

import java.io.ByteArrayOutputStream;
import java.text.DecimalFormat;
import java.util.List;

import javax.inject.Inject;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.corporateevents.to.CorporativeOperationTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.corporative.service.CorporativeReportServiceBean;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class InterestPaymentBytSecurityIssuer.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , Nov 20, 2013
 */
@ReportProcess(name="RetirementHolderInterestPaymentReport")
public class RetirementHolderInterestPaymentReport extends GenericReport {
	private static int ACCOUNT_NUMBER=0;
 	private static int FULL_NAME=1;
 	private static int PARTICIPANT=2;
	private static int TOTAL_BALANCE=3;
	private static int INTEREST_TOTAL_BALANCE  = 4;
	private static int INTEREST_NET_AVAILABLE  = 5;

	private static int AVAILABLE_BALANCE       = 6;
	private static int BRUT_INTEREST           = 7;
	private static int CUSTODY_AMOUNT          = 8;
	private static int RETENTION               = 9;
	private static int COUPON_BEGIN_DATE=10;
	private static int COUPING_END_DATE=11;

 

	/*Constants for the header corporativeOperation
	 * 
	 */

	private static int ISSUER       = 0;
	private static int ISIN         = 1;
	private static int NOMINAL      = 2;
	private static int TAX          = 3;
	private static int INTEREST     = 4;
	private static int ROUND        = 5;
	private static int ISSUANCE     = 6;
	private static int CUPON_NUMBER = 7;
	private static int REGISTRY_DATE  = 8;
	private static int CUTOFF_DATE    = 9;
	private static int DELIVERY_DATE  = 10;
	private static int ALTERNATIVE_CODE=12;
	private static int CURRENCY=13;

	@Inject
	private CorporativeReportServiceBean corporativeReportServiceBean;



	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {


		CorporativeOperationTO corporativeOperationTO = this.getReportToFromReportLogger(reportLogger);
		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();

		XMLOutputFactory xof = XMLOutputFactory.newInstance();
		try{


			List<Object[]> list = this.corporativeReportServiceBean.searchBalanceForInterestBySecurityIssuer(corporativeOperationTO);
			Object[] tempObjects = this.corporativeReportServiceBean.searchCorporativeOperationByFilter(corporativeOperationTO);

			XMLStreamWriter xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(arrayOutputStream));
			//open document file
			xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");
			//root tag of report
			/**
			 * report
			 **/
			xmlsw.writeStartElement(ReportConstant.REPORT);
			/**
			 * Create to header report
			 * start_hour
			 * report_title
			 * mnemonic_report
			 * clasification_report
			 * generation_date
			 * user_name
			 * mnemonic_entity
			 * **/
			createHeaderReport(xmlsw, reportLogger);
			//Create all the headers of the reports for no result statement
			String tempTitle=reportLogger.getReport().getTitle();			
			String eventType = ImportanceEventType.get(corporativeOperationTO.getCorporativeEventType()).getValue();
			String strTitle = tempTitle.replace("?", eventType);
			createTagString(xmlsw, ReportConstant.REPORT_TITLE, strTitle);

			createTagString(xmlsw, "issuer", tempObjects[ISSUER]);
			createTagString(xmlsw, "isin_code", tempObjects[ISIN]);
			createTagString(xmlsw, "issuance", tempObjects[ISSUANCE]);

			createTagDecimal(xmlsw, "nominal", tempObjects[NOMINAL]);
			DecimalFormat format = new DecimalFormat("0.0000");
			Double interest =Double.parseDouble(tempObjects[INTEREST].toString());
			Double tax =Double.parseDouble(tempObjects[TAX].toString());

			String strInterest = format.format(interest);
			String strTax = format.format(tax);

			createTagString(xmlsw, "tax", strTax+"%");
			if(tax.equals(0.0)){
				createTagString(xmlsw, "tax", "0%");

			}
			else{
				createTagString(xmlsw, "tax", strTax+"%");
 
			}
			createTagString(xmlsw, "interest", strInterest+"%");


			createTagString(xmlsw, "round", tempObjects[ROUND]);
			createTagString(xmlsw, "coupon", tempObjects[CUPON_NUMBER]);
			//createTagString(xmlsw, "interest", tempObjects[INTEREST]);
			createTagString(xmlsw, "cutoff_date", tempObjects[CUTOFF_DATE]);
			createTagString(xmlsw, "delivery_date", tempObjects[DELIVERY_DATE]);
			createTagString(xmlsw, "registry_date", tempObjects[REGISTRY_DATE]);
			String state = CorporateProcessStateType.get(corporativeOperationTO.getState()).getValue();
			createTagString(xmlsw, "corporate_state", state);
			createTagString(xmlsw, "alternate_code", tempObjects[ALTERNATIVE_CODE]);
			createTagString(xmlsw, "currency", tempObjects[CURRENCY]);
			createTagString(xmlsw, "cupon_begin_date", tempObjects[COUPON_BEGIN_DATE]);
			createTagString(xmlsw, "cupon_expiration_date", tempObjects[COUPON_BEGIN_DATE]);


			if(Validations.validateListIsNotNullAndNotEmpty(list)){
				/** Creating the body **/
				createBodyReport(xmlsw, list);
			}
			//END REPORT TAG
			xmlsw.writeEndElement();
			//close document
			xmlsw.writeEndDocument();
			xmlsw.flush();
			xmlsw.close();
		}catch(XMLStreamException e){
			e.printStackTrace();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return arrayOutputStream;

	}

	public void createBodyReport(XMLStreamWriter xmlsw,List<Object[]> listOperations){
		try {
			for(Object[] temp: listOperations){

				xmlsw.writeStartElement("operation");			 
				createTagString(xmlsw, "cuenta",         temp[ACCOUNT_NUMBER]);
				createTagString(xmlsw, "holder",         temp[FULL_NAME].toString());
				//createTagString(xmlsw, "rnt", temp[HOLDER_RNT]);
 				createTagString(xmlsw, ReportConstant.PARTICIPANT_PARAM, temp[PARTICIPANT]);
				//createTagString(xmlsw, "participant_pk",   temp[ID_PARTICIPANT]);
				createTagString(xmlsw, "total_balance",    temp[TOTAL_BALANCE]);
				createTagString(xmlsw, "available_balance",   temp[AVAILABLE_BALANCE]);
 				createTagDecimal(xmlsw, "brut_interes",  temp[BRUT_INTEREST]);
				createTagDecimal(xmlsw, "custody_amount", temp[CUSTODY_AMOUNT]);
				createTagDecimal(xmlsw, "retention", temp[RETENTION]);
				createTagDecimal(xmlsw, "i_total_balance", temp[INTEREST_TOTAL_BALANCE]);
				createTagDecimal(xmlsw, "i_available_balance",temp[INTEREST_NET_AVAILABLE]);
				

				xmlsw.writeEndElement();

			}

		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public CorporativeOperationTO getReportToFromReportLogger(ReportLogger reportLogger){
		CorporativeOperationTO corporativeOperationTO = new CorporativeOperationTO();
		for(ReportLoggerDetail detail:reportLogger.getReportLoggerDetails()){
			if(ReportConstant.CORP_PROCESS_ID.equals(detail.getFilterName())){
				corporativeOperationTO.setId(detail.getFilterValue()!=null?Long.parseLong(detail.getFilterValue()):null);
			}
			else if(ReportConstant.CUTOFF_DATE.equals(detail.getFilterName())){
				corporativeOperationTO.setCutOffDate(CommonsUtilities.convertStringtoDate(detail.getFilterValue()));
			}else if(ReportConstant.DELIVERY_DATE.equals(detail.getFilterName())){
				corporativeOperationTO.setDeliveryDate(CommonsUtilities.convertStringtoDate(detail.getFilterValue()));
			}
			else if(ReportConstant.CORPORATE_EVENT_TYPE.equals(detail.getFilterName())){
				corporativeOperationTO.setCorporativeEventType(Integer.parseInt(detail.getFilterValue()));
			}
			else if(ReportConstant.CODE_ISIN.equals(detail.getFilterName())){
				corporativeOperationTO.setSourceIsinCode(detail.getFilterValue());
			}
			else if(ReportConstant.DEST_CODE_ISIN.equals(detail.getFilterName()))
			{
				if(detail.getFilterValue()!=null){
					corporativeOperationTO.setTargetIsinCode(detail.getFilterValue());
				}
			}
			else if(ReportConstant.STATE_PARAM.equals(detail.getFilterName())){
				corporativeOperationTO.setState(Integer.parseInt(detail.getFilterValue()));
			}
		}

		return corporativeOperationTO;
	}

}
