package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.util.view.UtilReportConstants;


@ReportProcess(name="BlockRequestStatusChangeReport")
public class BlockRequestStatusChangeReport extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@Inject  PraderaLogger log; 
	
	@EJB
	ParticipantServiceBean participantServiceBean;
	
	public BlockRequestStatusChangeReport() {

	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void addParametersQueryReport() {
		List<ReportLoggerDetail> listaLogger = getReportLogger()
				.getReportLoggerDetails();
		if (listaLogger == null) {
			listaLogger = new ArrayList<ReportLoggerDetail>();
		}
		ReportLoggerDetail loggerDetail = new ReportLoggerDetail();
		loggerDetail.setFilterName("p_participante");

		try {
			
			String idParticipant = valueParameter(listaLogger, "participant");
			if (idParticipant!=null){
				Integer participant = new Integer(idParticipant);
			
				Participant filter = new Participant();
				filter.setIdParticipantPk(participant.longValue());
				loggerDetail.setFilterValue((participantServiceBean.findParticipantByFiltersServiceBean(filter)).getDescription());
			}else{
				loggerDetail.setFilterValue("TODOS");
			}
			listaLogger.add(loggerDetail);
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		
		Map<Integer,String> block = new HashMap<Integer, String>();
		Map<Integer,String> state = new HashMap<Integer, String>();
		
		try {

			filter.setMasterTableFk(MasterTableType.TYPE_AFFECTATION.getCode());
			for (ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				block.put(param.getParameterTablePk(),param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.STATE_AFFECTATION_REQUEST.getCode());
			for (ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				state.put(param.getParameterTablePk(),param.getDescription());
			}
						
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		parametersRequired.put("p_block", block);
		parametersRequired.put("p_state", state);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());

		
		// TODO Auto-generated method stub
		return parametersRequired;
		
	}
	
	private String valueParameter(List<ReportLoggerDetail> listaLogger,
			String parameter) {
		String value = null;
		for (int i = 0; i < listaLogger.size(); i++) {
			if (listaLogger.get(i).getFilterName() != null) {
				if (listaLogger.get(i).getFilterName().equals(parameter)) {
					if (listaLogger.get(i).getFilterValue() != null) {
						value = listaLogger.get(i).getFilterValue();
						i = listaLogger.size();
					}
				}
			}
		}
		return value;
	}

}
