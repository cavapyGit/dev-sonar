package com.pradera.report.generation.executor.issuances;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.management.RuntimeErrorException;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.executor.issuances.services.IssuancesReportServiceBean;
import com.pradera.report.generation.executor.issuances.to.GenericIssuanceTO;
import com.pradera.report.generation.poi.ReportPoiGeneratorServiceBean;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class DetailsOfTitlesPendingOfExpirationReport.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01-sep-2015
 */
@ReportProcess(name = "DetailsOfTitlesPendingOfExpirationReportPaymentAgPoi")
public class DetailsOfTitlesPendingOfExpirationReportPaymentAgPoi extends GenericReport{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The issuances report service. */
	@EJB
	private IssuancesReportServiceBean issuancesReportService;
	/** The custody report service bean. */
	@EJB
	CustodyReportServiceBean custodyReportServiceBean;
	
	/** The Report poi generator service bean. */
	@Inject
	private ReportPoiGeneratorServiceBean reportPoiGeneratorServiceBean;
	
	/** The log. */
	@Inject
	private PraderaLogger log;

	/**
	 * Instantiates a new details of titles pending of expiration report.
	 */
	public DetailsOfTitlesPendingOfExpirationReportPaymentAgPoi() {}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {

		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] arrayByteExcel = new byte[16384];
		GenericIssuanceTO gfto = new GenericIssuanceTO();
		List<Object[]> queryMain=new ArrayList<Object[]>();

		for(ReportLoggerDetail param: getReportLogger().getReportLoggerDetails()){
			if(param.getFilterName().equals("securities_situation")){
				if (param.getFilterValue()!=null){
					gfto.setSecuritiesSituation(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("cui_code")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setCui(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("holder_account")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setHolderAccount(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("security_class_key")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setSecurities(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("issuer_code")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setIssuer(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("participant")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setParticipant(param.getFilterValue().toString());
					// capturando su mnemonico en lugar de su id
					try {
						Participant p = issuancesReportService.find(Participant.class, Long.parseLong(gfto.getParticipant()));
						// se obtine nemonico para buscar por agente pagador en el valor
						gfto.setParticipantMnemonic(p.getMnemonic());
					} catch (Exception e) {
						e.printStackTrace();
						System.out.println(":: error : "+e.getMessage());
					}
				}
			}
			if(param.getFilterName().equals("expiration_of")){
				if (param.getFilterValue()!=null){
					gfto.setExpirationOf(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("currency")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setCurrency(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("date_initial")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setInitialDate(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("date_end")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setFinalDate(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("security_class")){ //issue 483
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setSecurityClass(param.getFilterValue().toString());
				}
			}
		}
		
		Date currentDate = CommonsUtilities.currentDate();
		String currDateStr = CommonsUtilities.convertDatetoStringForTemplate(currentDate, CommonsUtilities.DATE_PATTERN);
		
		//VALIDAR TIPO DE CAMBIO A LA FECHA DE CORTE
		BigDecimal exchangeRate = null;
		try{
			exchangeRate = custodyReportServiceBean.getExchangeRate(currDateStr);
		} catch (ServiceException e) {
			if(e.getErrorService().equals(ErrorServiceType.NOT_EXCHANGE_RATE)){
				throw new RuntimeErrorException(null, ReportConstant.ERROR_EXCHANGE_RATE + " " + currDateStr);
			}
			log.error(e.getMessage());
		}
		
		String strQuery = issuancesReportService.getDetailsOfTitlesPendingOfExpirationPoiReportToPaymentAgent(gfto);
		//String strQuery = issuancesReportService.getDetailsOfTitlesPendingOfExpirationReportToPaymentAgent(gfto);
		queryMain=issuancesReportService.getQueryListByClass(strQuery);
		
		Map<String,Object> parametersRequired = new HashMap<>();
		parametersRequired.put("str_Query", queryMain);
		
		/**GENERATE FILE EXCEL ***/
		arrayByteExcel=reportPoiGeneratorServiceBean.writeFileExcelDetailsOfTitlesPendingExpiration(parametersRequired,reportLogger);
		
		/**SETTING ARRAY BYTE TO GENERIC REPORT FOR PERSIST**/
		List<byte[]> bytes = new ArrayList<>();
		bytes.add(arrayByteExcel);
		setListByte(bytes);

		return baos;
	}
}
