package com.pradera.report.generation.executor.custody.to;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


@XmlAccessorType(XmlAccessType.FIELD) 
public class XmlListSeccSirtex implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9153017807891555889L;
	@XmlElement(name = "CUPON")
	private List<XmlCoupon> xmlCoupons;
	
	public XmlListSeccSirtex(){
		
	}

	public List<XmlCoupon> getXmlCoupons() {
		return xmlCoupons;
	}

	public void setXmlCoupons(List<XmlCoupon> xmlCoupons) {
		this.xmlCoupons = xmlCoupons;
	}
}
