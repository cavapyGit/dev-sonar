package com.pradera.report.generation.executor.issuances;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.issuances.services.IssuancesReportServiceBean;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "RelationOfHoldersSecuritiesByIssuerReport")
public class RelationOfHoldersSecuritiesByIssuerReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB private ParameterServiceBean parameterService;
	@EJB private IssuancesReportServiceBean issuancesReportService;
	@Inject private PraderaLogger log;

	public RelationOfHoldersSecuritiesByIssuerReport() {}
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		GenericsFiltersTO gfto = new GenericsFiltersTO();

		for(ReportLoggerDetail param: getReportLogger().getReportLoggerDetails()){
			if(param.getFilterName().equals("issuer_code")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setIdIssuer(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("cui_code")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setCui(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("holder_account")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setIdHolderAccountPk(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("security_class_key")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setIdSecurityCodePk(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("cut_date")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setInitialDt(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("reporting_holders")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setReportingHolders(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("issuance_date")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setIssuanceDate(param.getFilterValue().toString());
					
				}
			}
			if(param.getFilterName().equals("security_class")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setSecurityClass(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("bcb_format")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setBcbFormat (param.getFilterValue().toString());
				}
			}
		}
		
		String strQuery = "";
		strQuery = issuancesReportService.getQueryRelationOfHoldersSecuritiesByIssuer(gfto);
		gfto.setFinalDt(gfto.getInitialDt());
		String strQueryFormatted = issuancesReportService.getQueryFormatForJasper(gfto, strQuery, null);
		
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> secClassDocType 			= new HashMap<Integer, String>();
		Map<Integer,String> secClassSecurity 			= new HashMap<Integer, String>();
		Map<Integer,String> secClassCurrency	 		= new HashMap<Integer, String>();
		try {
			
			filter.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secClassDocType.put(param.getParameterTablePk(), param.getIndicator1());
			}

			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secClassCurrency.put(param.getParameterTablePk(), param.getDescription());
			}
			
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secClassSecurity.put(param.getParameterTablePk(), param.getDescription());
			}
			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		parametersRequired.put("str_Query", strQueryFormatted);
		parametersRequired.put("mDocType", secClassDocType);
		parametersRequired.put("mCurrency", secClassCurrency);
		parametersRequired.put("mDescClaseValor", secClassSecurity);
		parametersRequired.put("initialDt", gfto.getInitialDt());
		parametersRequired.put("bcb_format", gfto.getBcbFormat());
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		
		return parametersRequired;		
		

	}

}
