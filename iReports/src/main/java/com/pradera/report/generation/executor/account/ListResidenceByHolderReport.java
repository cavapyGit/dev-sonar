package com.pradera.report.generation.executor.account;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.service.HolderQueryServiceBean;
import com.pradera.core.component.helperui.service.IssuerQueryServiceBean;
import com.pradera.core.component.helperui.to.IssuerSearcherTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.account.service.AccountReportServiceBean;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "ListResidenceByHolderReport")
public class ListResidenceByHolderReport extends GenericReport {	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	private IssuerQueryServiceBean issuerService;
	
	@EJB
	private HolderQueryServiceBean holderService;
	
	@EJB
	private AccountReportServiceBean accountReportServiceBean;
	
	@Inject
	PraderaLogger log;

	
	public ListResidenceByHolderReport() {}
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParametersQueryReport() {}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		Issuer issuer = null;
		String parameterIssuer = null;
		Holder holder = null;
		String parameterHolder = null;
		String currencyMnemonic = null;
		String date = null;
		
		Map<Integer,String> currency = new HashMap<Integer, String>();
		Map<Integer,String> securityClass = new HashMap<Integer, String>();
		
		// DATOS DE ENTRADA DEL SISTEMA
		for (ReportLoggerDetail r : loggerDetails) {
			
			if (r.getFilterName().equals(ReportConstant.DATE_PARAM)) {
				if(Validations.validateIsNotNull(r.getFilterValue())){
					date = r.getFilterValue();
				}
			}
			
			if (r.getFilterName().equals(ReportConstant.CURRENCY_TYPE)) {
				if(Validations.validateIsNotNull(r.getFilterValue())){
					currencyMnemonic = parameterService.find(new Integer(r.getFilterValue()), ParameterTable.class).getDescription();
				}else{
					currencyMnemonic = "(Todos)";
				}
			}
			
			if (r.getFilterName().equals(ReportConstant.ISSUER_PARAM)) {
				IssuerSearcherTO to = new IssuerSearcherTO();
				if(Validations.validateIsNotNull(r.getFilterValue())){
					to.setIssuerCode(r.getFilterValue());
					try {
						issuer = issuerService.getIssuerByCode(to);
						parameterIssuer = "("+issuer.getMnemonic()+") "+issuer.getBusinessName();
					} catch (ServiceException e) {
						e.printStackTrace();
					}
				}else{
					parameterIssuer = "(Todos)";
				}
			}
			
			if (r.getFilterName().equals(ReportConstant.CUI_CODE_PARAM)) {
				if(Validations.validateIsNotNull(r.getFilterValue())){
					try {
						holder = holderService.findHolderByCode(new Long(r.getFilterValue()));
						parameterHolder = "("+holder.getIdHolderPk()+") "+holder.getDescriptionHolder();
					} catch (ServiceException e) {
						e.printStackTrace();
					}
				}else{
					parameterHolder = "(Todos)";
				}
			}
		}
		
		try {
		
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				currency.put(param.getParameterTablePk(),
						"("+param.getDescription()+") "+param.getParameterName());
			}
			
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				securityClass.put(param.getParameterTablePk(),
						"("+param.getText1()+") "+param.getDescription());
			}
			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		String strQuery = accountReportServiceBean.getListResidenceByHolderReport(date);
		
		parametersRequired.put("str_query", strQuery);
		parametersRequired.put("p_currency", currency);
		parametersRequired.put("p_securityClass", securityClass);
		parametersRequired.put("parameterIssuer", parameterIssuer);
		parametersRequired.put("parameterHolder", parameterHolder);
		parametersRequired.put("currency_mnemonic", currencyMnemonic);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());

		return parametersRequired;
	}
	
	

}
