package com.pradera.report.generation.executor.securities;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.GeographicLocationServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.GeographicLocationStateType;
import com.pradera.model.generalparameter.type.GeographicLocationType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.negotiation.service.NegotiationReportServiceBean;
import com.pradera.report.generation.executor.securities.service.IssuerListReportServiceBean;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "IndividualBusinessReport")
public class IndividualBusinessReport extends GenericReport{
private static final long serialVersionUID = 1L;

@Inject  PraderaLogger log;

@EJB
private ParameterServiceBean parameterService;

@EJB
GeographicLocationServiceBean geographicLocationServiceBean;

@EJB
private IssuerListReportServiceBean issuerServiceBean;

public IndividualBusinessReport() {
}

@Override
public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public void addParametersQueryReport() {
	List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
	if (listaLogger == null) {
		listaLogger = new ArrayList<ReportLoggerDetail>();
	}

}

public List<GeographicLocation> getLstCountry() {

	List<GeographicLocation> lst = null;

	try {

		GeographicLocationTO geographicLocationTO = new GeographicLocationTO();

		geographicLocationTO.setGeographicLocationType(GeographicLocationType.COUNTRY.getCode());
		geographicLocationTO.setState(GeographicLocationStateType.REGISTERED.getCode());

		lst = geographicLocationServiceBean.getListGeographicLocation(geographicLocationTO);

	} catch (Exception ex) {
		ex.printStackTrace();
	}

	return lst;

}

public List<GeographicLocation> getLstReferenceLocation(Integer geographicLocationType) {

	List<GeographicLocation> lst = null;

	try {

		GeographicLocationTO geographicLocationTO = new GeographicLocationTO();

		geographicLocationTO.setGeographicLocationType(geographicLocationType);
		geographicLocationTO.setState(GeographicLocationStateType.REGISTERED.getCode());

		lst = geographicLocationServiceBean.getListGeographicLocation(geographicLocationTO);

	} catch (Exception ex) {
		ex.printStackTrace();
	}

	return lst;

}

@Override
public Map<String, Object> getCustomJasperParameters() {
	Map<String,Object> parametersRequired = new HashMap<>();
	ParameterTableTO  filter = new ParameterTableTO();
	GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
	List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
	String issuerCode = null;
	String issuerDescription = null;
	Map<Integer,String> state_issuer = new HashMap<Integer, String>();
	Map<Integer,String> economic_sector = new HashMap<Integer, String>();
	Map<Integer,String> economic_activity = new HashMap<Integer, String>();
	Map<Integer,String> society_type = new HashMap<Integer, String>();
	Map<Integer,String> person_type = new HashMap<Integer, String>();
	Map<Integer,String> representative_class = new HashMap<Integer, String>();
	Map<Integer,String> document_type = new HashMap<Integer, String>();
	Map<Integer,String> nationality = new HashMap<Integer, String>();
	Map<Integer,String> legal_residence = new HashMap<Integer, String>();
	Map<Integer,String> residence_country = new HashMap<Integer, String>();
	Map<Integer,String> province = new HashMap<Integer, String>();
	Map<Integer,String> district = new HashMap<Integer, String>();
	Map<Integer,String> department = new HashMap<Integer, String>();
	
	for (ReportLoggerDetail r : loggerDetails) {
		if (r.getFilterName().equals(ReportConstant.ISSUER_PARAM) && r.getFilterValue()!=null)
			issuerCode = r.getFilterValue();
	}
	
	try {
		
		if(issuerCode != null){
			Issuer iss = issuerServiceBean.find(Issuer.class, issuerCode);
			issuerDescription = iss.getMnemonic() + " - " + iss.getBusinessName();
		}
		
		filter.setMasterTableFk(MasterTableType.ISSUER_STATE.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
			state_issuer.put(param.getParameterTablePk(), param.getParameterName());
		}
		filter.setMasterTableFk(MasterTableType.ECONOMIC_SECTOR.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
			economic_sector.put(param.getParameterTablePk(), param.getParameterName());
		}
		filter.setMasterTableFk(MasterTableType.ECONOMIC_ACTIVITY.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
			economic_activity.put(param.getParameterTablePk(), param.getParameterName());
		}
		filter.setMasterTableFk(MasterTableType.SOCIETY_TYPE.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
			society_type.put(param.getParameterTablePk(), param.getParameterName());
		}
		filter.setMasterTableFk(MasterTableType.PERSON_TYPE.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
			person_type.put(param.getParameterTablePk(), param.getParameterName());
		}
		filter.setMasterTableFk(MasterTableType.CLASS_REPRENTATIVE.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
			representative_class.put(param.getParameterTablePk(), param.getParameterName());
		}
		filter.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
			document_type.put(param.getParameterTablePk(), param.getIndicator1());
		}
		
		geographicLocationTO.setGeographicLocationType(GeographicLocationType.COUNTRY.getCode());
		geographicLocationTO.setState(GeographicLocationStateType.REGISTERED.getCode());
			
		for(GeographicLocation geo : geographicLocationServiceBean.getListGeographicLocation(geographicLocationTO))		
		{
			nationality.put(geo.getIdGeographicLocationPk(), geo.getName());
			legal_residence.put(geo.getIdGeographicLocationPk(), geo.getName());
			residence_country.put(geo.getIdGeographicLocationPk(), geo.getName());
		}
		
		geographicLocationTO.setGeographicLocationType(GeographicLocationType.DEPARTMENT.getCode());
		for(GeographicLocation geo : getLstReferenceLocation(geographicLocationTO.getGeographicLocationType()))
		{
		   department.put(geo.getIdGeographicLocationPk(), geo.getName());
		}
		
		geographicLocationTO.setGeographicLocationType(GeographicLocationType.PROVINCE.getCode());
		for(GeographicLocation geo : getLstReferenceLocation(geographicLocationTO.getGeographicLocationType()))
		{
			province.put(geo.getIdGeographicLocationPk(), geo.getName());
		}
		
		geographicLocationTO.setGeographicLocationType(GeographicLocationType.DISTRICT.getCode());
		for(GeographicLocation geo : getLstReferenceLocation(geographicLocationTO.getGeographicLocationType()))
		{
			district.put(geo.getIdGeographicLocationPk(), geo.getName());
		}
		
		
		
	}catch(Exception ex){
		log.error(ex.getMessage());
		ex.printStackTrace();
		throw new RuntimeException();
	}
	
	parametersRequired.put("issuerDescription", issuerDescription);
	parametersRequired.put("p_state_issuer", state_issuer);
	parametersRequired.put("p_economic_sector", economic_sector);
	parametersRequired.put("p_economic_activity", economic_activity);
	parametersRequired.put("p_society_type", society_type);
	parametersRequired.put("p_person_type", person_type);
	parametersRequired.put("p_representative_class", representative_class);
	parametersRequired.put("p_document_type", document_type);
	parametersRequired.put("p_nationality", nationality);
	parametersRequired.put("p_legal_residence", legal_residence);
	parametersRequired.put("p_residence_country", residence_country);
	parametersRequired.put("p_province", province);
	parametersRequired.put("p_district", district);
	parametersRequired.put("p_department", department);
	parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
	
	// TODO Auto-generated method stub
	return parametersRequired;
	}
}
