package com.pradera.report.generation.pentaho;

import java.util.Map;

import org.pentaho.reporting.engine.classic.core.DataFactory;
import org.pentaho.reporting.engine.classic.core.modules.misc.datafactory.sql.JndiConnectionProvider;
import org.pentaho.reporting.engine.classic.core.modules.misc.datafactory.sql.SQLReportDataFactory;

import com.pradera.integration.common.validation.Validations;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.model.report.type.ReportType;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class QueryReportGenerator.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/07/2013
 */
@ReportImplementation(type=ReportType.QUERY)
public class QueryReportGenerator extends ReportGenerator {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3415182341733998371L;

	/**
	 * Instantiates a new query report generator.
	 */
	public QueryReportGenerator() {
		super();
	}

	/* (non-Javadoc)
	 * @see com.pradera.report.generation.pentaho.ReportGenerator#getDataFactory()
	 */
	@Override
	public DataFactory getDataFactory() {
		//Query runing inside report
		JndiConnectionProvider connectionProvider = new JndiConnectionProvider();
		connectionProvider.setConnectionPath(jndiIdepositary);
		SQLReportDataFactory dataFactory = new SQLReportDataFactory(connectionProvider);
		dataFactory.setQuery(QUERY_NAME, getReportQuery());
		if(Validations.validateIsNotNullAndNotEmpty(getSubReportQuery())){
			dataFactory.setQuery(SUB_QUERY_NAME, getSubReportQuery());
		}
		return dataFactory;
	}

	/* (non-Javadoc)
	 * @see com.pradera.report.generation.pentaho.ReportGenerator#getReportParameters()
	 */
	@Override
	public Map<String, Object> getReportParameters() {
		Map<String, Object> parameter = super.getReportParameters();
		//Add parameters specific to report
		if (getReportLogger().getReportLoggerDetails()!= null 
				&& !getReportLogger().getReportLoggerDetails().isEmpty()){
			
			for(ReportLoggerDetail loggerDetil : getReportLogger().getReportLoggerDetails() ){
				parameter.put(loggerDetil.getFilterName(), loggerDetil.getFilterValue());
			}
		}
		return parameter;
	}

	@Override
	public void setXmlSubDataSource(String subXmlDataSource) {
		// TODO Auto-generated method stub
		
	}

}
