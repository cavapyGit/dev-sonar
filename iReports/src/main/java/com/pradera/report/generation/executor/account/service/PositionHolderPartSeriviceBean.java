package com.pradera.report.generation.executor.account.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.report.generation.executor.account.to.PositionsHolderParticipantTO;


@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class PositionHolderPartSeriviceBean extends CrudDaoServiceBean {

	public PositionHolderPartSeriviceBean() {
		// TODO Auto-generated constructor stub
	}
	
	public String QueryPositionHolderParticipant(PositionsHolderParticipantTO positionHolderTO, Long idStkCalcProcess){
		StringBuilder sbQuery= new StringBuilder();
		
		if(positionHolderTO.getCourtDate().before(CommonsUtilities.currentDate())){
			sbQuery.append(" Select");
			sbQuery.append(" secu.ID_SECURITY_CODE_PK,");
			sbQuery.append(" ha.ID_HOLDER_ACCOUNT_PK,");
			sbQuery.append(" ha.ACCOUNT_TYPE,");
			sbQuery.append(" p.id_participant_pk,");
			sbQuery.append(" p.mnemonic,");
			sbQuery.append(" p.description as participant,");
			sbQuery.append(" secu.security_class as clase_valor,");
			sbQuery.append(" secu.CURRENCY,");
			sbQuery.append(" ha.account_number as cuenta_titular,");
			sbQuery.append(" hab.total_balance as saldo_contable,");
			sbQuery.append(" hab.available_balance as saldo_disponible,");
			sbQuery.append(" hab.pawn_balance as saldo_prenda,");
			sbQuery.append(" hab.ban_balance as saldo_embargo,");
			sbQuery.append(" hab.other_block_balance as saldo_otros,");
			sbQuery.append(" hab.accreditation_balance as saldo_acreditacion,");
			sbQuery.append(" hab.margin_balance as saldo_margen,");
			sbQuery.append(" hab.reporting_balance as saldo_reportante,");
			sbQuery.append(" hab.reported_balance as saldo_reportado,");
			sbQuery.append(" hab.purchase_balance as saldo_compra,");
			sbQuery.append(" hab.sale_balance as saldo_venta,");
			sbQuery.append(" hab.transit_balance as saldo_transito");
			sbQuery.append(" From");
			sbQuery.append(" holder_account_balance_history hab"); //holder_account_balance_history
			sbQuery.append(" Inner join participant p On hab.id_participant_pk=p.id_participant_pk");
			sbQuery.append(" Inner join security secu On hab.id_security_code_pk=secu.id_security_code_pk");
			sbQuery.append(" Inner join issuer issu On secu.id_issuer_fk=issu.id_issuer_pk");
			sbQuery.append(" Inner join holder_account ha On ha.id_holder_account_pk=hab.id_holder_account_pk");
			sbQuery.append(" where 1=1");
			sbQuery.append(" 	AND TRUNC(HAB.CUT_DATE) = TO_DATE('"+CommonsUtilities.convertDatetoString(positionHolderTO.getCourtDate())+"','DD/MM/YYYY') ");
			sbQuery.append(" 	AND (hab.total_balance + hab.purchase_balance + hab.reported_balance) > 0 ");
			
			if(positionHolderTO.getParticipant()!=null){
				sbQuery.append(" and p.ID_PARTICIPANT_PK=:participant");
			}
			if(positionHolderTO.getSecurityClass() !=null){
				sbQuery.append(" and secu.ID_SECURITY_CODE_PK=:securityClass");
			}
			if(positionHolderTO.getIssuer()  !=null){
				sbQuery.append(" and issu.ID_ISSUER_PK=:issuer");
			}
			if(positionHolderTO.getCuiHolder()  !=null){
				sbQuery.append(" AND (SELECT COUNT(*) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK=HAB.ID_HOLDER_ACCOUNT_PK AND HAD.ID_HOLDER_FK=:cuiHolder)>0");
			}
			
			sbQuery.append(" Group by");
			sbQuery.append(" secu.ID_SECURITY_CODE_PK,");
			sbQuery.append(" ha.ID_HOLDER_ACCOUNT_PK,");
			sbQuery.append(" ha.ACCOUNT_TYPE,");
			sbQuery.append(" p.id_participant_pk,");
			sbQuery.append(" p.mnemonic,");
			sbQuery.append(" p.description,");
			sbQuery.append(" secu.security_class,");
			sbQuery.append(" secu.CURRENCY,");
			sbQuery.append(" ha.account_number,");
			sbQuery.append(" hab.total_balance,");
			sbQuery.append(" hab.available_balance,");
			sbQuery.append(" hab.pawn_balance,");
			sbQuery.append(" hab.ban_balance,");
			sbQuery.append(" hab.other_block_balance,");
			sbQuery.append(" hab.accreditation_balance,");
			sbQuery.append(" hab.margin_balance,");
			sbQuery.append(" hab.reporting_balance,");
			sbQuery.append(" hab.reported_balance,");
			sbQuery.append(" hab.purchase_balance,");
			sbQuery.append(" hab.sale_balance,");
			sbQuery.append(" hab.transit_balance");
			sbQuery.append(" order by p.id_participant_pk,secu.security_class,secu.CURRENCY,ha.ID_HOLDER_ACCOUNT_PK");
		}else {
			sbQuery.append(" Select");
			sbQuery.append(" secu.ID_SECURITY_CODE_PK,");
			sbQuery.append(" ha.ID_HOLDER_ACCOUNT_PK,");
			sbQuery.append(" ha.ACCOUNT_TYPE,");
			sbQuery.append(" p.id_participant_pk,");
			sbQuery.append(" p.mnemonic,");
			sbQuery.append(" p.description as participant,");
			sbQuery.append(" secu.security_class as clase_valor,");
			sbQuery.append(" secu.CURRENCY,");
			sbQuery.append(" ha.account_number as cuenta_titular,");
			sbQuery.append(" hab.total_balance as saldo_contable,");
			sbQuery.append(" hab.available_balance as saldo_disponible,");
			sbQuery.append(" hab.pawn_balance as saldo_prenda,");
			sbQuery.append(" hab.ban_balance as saldo_embargo,");
			sbQuery.append(" hab.other_block_balance as saldo_otros,");
			sbQuery.append(" hab.accreditation_balance as saldo_acreditacion,");
			sbQuery.append(" hab.margin_balance as saldo_margen,");
			sbQuery.append(" hab.reporting_balance as saldo_reportante,");
			sbQuery.append(" hab.reported_balance as saldo_reportado,");
			sbQuery.append(" hab.purchase_balance as saldo_compra,");
			sbQuery.append(" hab.sale_balance as saldo_venta,");
			sbQuery.append(" hab.transit_balance as saldo_transito");
			sbQuery.append(" From");
			sbQuery.append(" holder_account_balance hab"); //account_balance_view
			sbQuery.append(" Inner join participant p On hab.id_participant_pk=p.id_participant_pk");
			sbQuery.append(" Inner join security secu On hab.id_security_code_pk=secu.id_security_code_pk");
			sbQuery.append(" Inner join issuer issu On secu.id_issuer_fk=issu.id_issuer_pk");
			sbQuery.append(" Inner join holder_account ha On ha.id_holder_account_pk=hab.id_holder_account_pk");
			sbQuery.append(" where 1=1");
			//sbQuery.append(" 	AND HAB.CUT_DATE = TO_DATE('"+CommonsUtilities.convertDatetoString(positionHolderTO.getCourtDate())+"','DD/MM/YYYY') ");
			sbQuery.append(" 	AND (hab.total_balance + hab.purchase_balance + hab.reported_balance) > 0 ");
			
			if(positionHolderTO.getParticipant()!=null){
				sbQuery.append(" and p.ID_PARTICIPANT_PK=:participant");
			}
			if(positionHolderTO.getSecurityClass() !=null){
				sbQuery.append(" and secu.ID_SECURITY_CODE_PK=:securityClass");
			}
			if(positionHolderTO.getIssuer()  !=null){
				sbQuery.append(" and issu.ID_ISSUER_PK=:issuer");
			}
			if(positionHolderTO.getCuiHolder()  !=null){
				sbQuery.append(" AND (SELECT COUNT(*) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK=HAB.ID_HOLDER_ACCOUNT_PK AND HAD.ID_HOLDER_FK=:cuiHolder)>0");
			}
			
			sbQuery.append(" Group by");
			sbQuery.append(" secu.ID_SECURITY_CODE_PK,");
			sbQuery.append(" ha.ID_HOLDER_ACCOUNT_PK,");
			sbQuery.append(" ha.ACCOUNT_TYPE,");
			sbQuery.append(" p.id_participant_pk,");
			sbQuery.append(" p.mnemonic,");
			sbQuery.append(" p.description,");
			sbQuery.append(" secu.security_class,");
			sbQuery.append(" secu.CURRENCY,");
			sbQuery.append(" ha.account_number,");
			sbQuery.append(" hab.total_balance,");
			sbQuery.append(" hab.available_balance,");
			sbQuery.append(" hab.pawn_balance,");
			sbQuery.append(" hab.ban_balance,");
			sbQuery.append(" hab.other_block_balance,");
			sbQuery.append(" hab.accreditation_balance,");
			sbQuery.append(" hab.margin_balance,");
			sbQuery.append(" hab.reporting_balance,");
			sbQuery.append(" hab.reported_balance,");
			sbQuery.append(" hab.purchase_balance,");
			sbQuery.append(" hab.sale_balance,");
			sbQuery.append(" hab.transit_balance");
			sbQuery.append(" order by p.id_participant_pk,secu.security_class,secu.CURRENCY,ha.ID_HOLDER_ACCOUNT_PK");
		}
		
		
		return sbQuery.toString();
		
	}
	
	public String QueryPositionHolderParticipantSubSum(PositionsHolderParticipantTO positionHolderTO, Long idStkCalcProcess){
		StringBuilder sbQuery= new StringBuilder();
		
		sbQuery.append(" Select");
		sbQuery.append(" SUM(hab.total_balance) as saldo_contable,");
		sbQuery.append(" SUM(hab.available_balance) as saldo_disponible,");
		sbQuery.append(" SUM(hab.pawn_balance) as saldo_prenda,");
		sbQuery.append(" SUM(hab.ban_balance) as saldo_embargo,");
		sbQuery.append(" SUM(hab.other_block_balance) as saldo_otros,");
		sbQuery.append(" SUM(hab.accreditation_balance) as saldo_acreditacion,");
		sbQuery.append(" SUM(hab.margin_balance) as saldo_margen,");
		sbQuery.append(" SUM(hab.reporting_balance) as saldo_reportante,");
		sbQuery.append(" SUM(hab.reported_balance) as saldo_reportado,");
		sbQuery.append(" SUM(hab.purchase_balance) as saldo_compra,");
		sbQuery.append(" SUM(hab.sale_balance) as saldo_venta,");
		sbQuery.append(" SUM(hab.transit_balance) as saldo_transito");
		sbQuery.append(" From");
		
		if(idStkCalcProcess!=null){
			sbQuery.append(" stock_calculation_balance hab");
			sbQuery.append(" Inner join participant p On hab.id_participant_fk=p.id_participant_pk");
			sbQuery.append(" Inner join security secu On hab.id_security_code_fk=secu.id_security_code_pk");
		}else{
			sbQuery.append(" holder_account_balance hab");
			sbQuery.append(" Inner join participant p On hab.id_participant_pk=p.id_participant_pk");
			sbQuery.append(" Inner join security secu On hab.id_security_code_pk=secu.id_security_code_pk");
		}
		
		sbQuery.append(" Inner join issuer issu On secu.id_issuer_fk=issu.id_issuer_pk");
		
		if(idStkCalcProcess!=null){
			sbQuery.append(" Inner join holder_account ha On ha.id_holder_account_pk=hab.id_holder_account_fk");
		}else{
			sbQuery.append(" Inner join holder_account ha On ha.id_holder_account_pk=hab.id_holder_account_pk");
		}
		sbQuery.append(" Inner join holder_account_detail had On had.id_holder_account_fk=ha.id_holder_account_pk");
		sbQuery.append(" Inner join holder h On had.id_holder_fk=h.id_holder_pk");
		sbQuery.append(" where 1=1");
		
		
		if(positionHolderTO.getParticipant()!=null){
			sbQuery.append(" and p.ID_PARTICIPANT_PK=:participant");
		}
		if(positionHolderTO.getSecurityClass() !=null){
			sbQuery.append(" and secu.ID_SECURITY_CODE_PK=:securityClass");
		}
		if(positionHolderTO.getIssuer()  !=null){
			sbQuery.append(" and issu.ID_ISSUER_PK=:issuer");
		}
		if(positionHolderTO.getCuiHolder()  !=null){
			sbQuery.append(" and h.ID_HOLDER_PK=:cuiHolder");
		}
		if(idStkCalcProcess!=null){
			sbQuery.append(" and hab.id_stock_calculation_fk=:idSCP");
		}
	
		sbQuery.append(" order by p.id_participant_pk,secu.security_class,secu.CURRENCY,ha.ID_HOLDER_ACCOUNT_PK");
		
		return sbQuery.toString();
		
	}
	
	public String QueryFormatForJasper(PositionsHolderParticipantTO positionHolderTO, String sbQuery, Long idStkCalcProcess){
    	String strQueryFormatted = sbQuery.toString();
    	
    	if(positionHolderTO.getParticipant()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":participant", positionHolderTO.getParticipant().toString());
		}
		if(positionHolderTO.getSecurityClass() !=null){
			strQueryFormatted=strQueryFormatted.replace(":securityClass", "'" + positionHolderTO.getSecurityClass() + "'");
		}
		if(positionHolderTO.getIssuer()  !=null){
			strQueryFormatted=strQueryFormatted.replace(":issuer", "'" + positionHolderTO.getIssuer().toString() + "'");
		}
		if(positionHolderTO.getCuiHolder()  !=null){
			strQueryFormatted=strQueryFormatted.replace(":cuiHolder", positionHolderTO.getCuiHolder().toString());
		}
//		if(idStkCalcProcess!=null){
//			strQueryFormatted=strQueryFormatted.replace(":idSCP", idStkCalcProcess.toString());
//		}
    
		return strQueryFormatted;
    }

}
