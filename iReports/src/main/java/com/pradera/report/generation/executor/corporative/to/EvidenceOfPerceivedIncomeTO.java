package com.pradera.report.generation.executor.corporative.to;

import java.util.Date;

public class EvidenceOfPerceivedIncomeTO {

	private Integer signType;
	
	private Long idParticipantPk;
	
	private Long idHolderPk;	
	
	private Long idHolderAccountPk;
	
	private Date initialDate;
	
	private Date endDate;
	
	private String idIsinCodePk;
	
	private String idIssuanceCodePk;
	
	public Integer getSignType() {
		return signType;
	}
	public void setSignType(Integer signType) {
		this.signType = signType;
	}
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	public Long getIdHolderPk() {
		return idHolderPk;
	}
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}
	public Date getInitialDate() {
		return initialDate;
	}
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getIdIsinCodePk() {
		return idIsinCodePk;
	}
	public void setIdIsinCodePk(String idIsinCodePk) {
		this.idIsinCodePk = idIsinCodePk;
	}
	public String getIdIssuanceCodePk() {
		return idIssuanceCodePk;
	}
	public void setIdIssuanceCodePk(String idIssuanceCodePk) {
		this.idIssuanceCodePk = idIssuanceCodePk;
	}
}
