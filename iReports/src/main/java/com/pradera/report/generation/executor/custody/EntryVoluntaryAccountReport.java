package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;

import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.util.view.UtilReportConstants;
//Constancia de Anotacin en Cuenta Voluntaria
@ReportProcess(name = "EntryVoluntaryAccountReport")
public class EntryVoluntaryAccountReport extends GenericReport{
	
	private static final long serialVersionUID = 1L;
	//parametros para el tipo de moneda
	private final String MAP_CURRENCY="map_currency";
	private final String MAP_SECURITY_CLASS="p_securityClass";
	
	private Map<String, Object> map= new HashMap<String, Object>();
	@EJB
	private ParameterServiceBean parameterService;
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		ByteArrayOutputStream arrayOutputStream=new  ByteArrayOutputStream();
		return arrayOutputStream;
	}
	@Override
	public Map<String, Object> getCustomJasperParameters(){
		Map<Integer,String> typesCurrency = new HashMap<Integer, String>();
		ParameterTableTO  parameterFilter = new ParameterTableTO();
	    parameterFilter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
	    
	    Map<Integer,String> typesSecurityClass = new HashMap<Integer, String>();
		ParameterTableTO  parameterFilterSecurityClass = new ParameterTableTO();
		parameterFilterSecurityClass.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(parameterFilter)) 
				typesCurrency.put(param.getParameterTablePk(), param.getDescription());
			map.put(MAP_CURRENCY, typesCurrency);
			
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(parameterFilterSecurityClass)) 
				typesSecurityClass.put(param.getParameterTablePk(), param.getDescription());
			map.put(MAP_SECURITY_CLASS, typesSecurityClass);
//		Iterator it = map.entrySet().iterator();
//		while (it.hasNext()) {
//			Map.Entry e = (Map.Entry)it.next();
//			System.out.println(e.getKey() + ":" + e.getValue());
//		}
		map.put("logo_path", UtilReportConstants.readLogoReport());	
		return map;
	}
}
