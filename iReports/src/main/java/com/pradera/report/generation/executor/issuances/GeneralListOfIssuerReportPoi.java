package com.pradera.report.generation.executor.issuances;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.pradera.integration.common.validation.Validations;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.issuances.services.IssuancesReportServiceBean;
import com.pradera.report.generation.executor.issuances.to.GeneralListOfIssuerReportTO;
import com.pradera.report.generation.poi.ReportPoiGeneratorServiceBean;

@ReportProcess(name = "GeneralListOfIssuerReportPoi")
public class GeneralListOfIssuerReportPoi extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	private IssuancesReportServiceBean issuancesReportServiceBean;
	
	/** The Report poi generator service bean. */
	@Inject
	private ReportPoiGeneratorServiceBean reportPoiGeneratorServiceBean;

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] arrayByteExcel = new byte[16384];
		GeneralListOfIssuerReportTO generalListOfIssuerReportTo =new GeneralListOfIssuerReportTO();
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		Map<String,Object> parameters = new HashMap<>();
		for (int i = 0; i < listaLogger.size(); i++) {

			if (listaLogger.get(i).getFilterName().equals("offer_type"))
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
					generalListOfIssuerReportTo.setOfferType(Integer.parseInt(listaLogger.get(i).getFilterValue()));

			if (listaLogger.get(i).getFilterName().equals("initial_date"))
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
					generalListOfIssuerReportTo.setInitialDate(listaLogger.get(i).getFilterValue());

			if (listaLogger.get(i).getFilterName().equals("final_date"))
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
					generalListOfIssuerReportTo.setFinalDate(listaLogger.get(i).getFilterValue());

			if (listaLogger.get(i).getFilterName().equals("id_issuer"))
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
					generalListOfIssuerReportTo.setIdIssuerPk(listaLogger.get(i).getFilterValue());

			if (listaLogger.get(i).getFilterName().equals("economic_activity"))
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
					generalListOfIssuerReportTo.setEconomicActivity(Integer.parseInt(listaLogger.get(i).getFilterValue()));

			if (listaLogger.get(i).getFilterName().equals("state_holder"))
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
					generalListOfIssuerReportTo.setStateIssuer(Integer.parseInt(listaLogger.get(i).getFilterValue()));

		}
		
		String strQueryFormated = issuancesReportServiceBean.getQueryGeneralListIssuers(generalListOfIssuerReportTo);
		List<Object[]> lstObject = issuancesReportServiceBean.getQueryListByClass(strQueryFormated);
		parameters.put("lstObjects", lstObject);
		
		/**GENERATE FILE EXCEL ***/
		arrayByteExcel = reportPoiGeneratorServiceBean.writeFileExcelGeneralListIssuers(parameters, reportLogger);
		
		/**SETTING ARRAY BYTE TO GENERIC REPORT FOR PERSIST**/
		List<byte[]> bytes = new ArrayList<>();
		bytes.add(arrayByteExcel);
		setListByte(bytes);
		
		return baos;
	}

}
