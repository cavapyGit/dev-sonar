package com.pradera.report.generation.executor.settlement;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.settlement.service.SettlementReportServiceBean;
import com.pradera.report.generation.executor.settlement.to.GenericSettlementOperationTO;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class HoldersProcessedAndUnprocessedReport.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01-sep-2015
 */
@ReportProcess(name = "HoldersProcessedAndUnprocessedReport")
public class HoldersProcessedAndUnprocessedReport extends GenericReport{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The parameter service. */
	@EJB
	private ParameterServiceBean parameterService;
	
	/** The settlement report service bean. */
	@EJB
	private SettlementReportServiceBean settlementReportServiceBean;

	/** The log. */
	@Inject
	private PraderaLogger log;

	/**
	 * Instantiates a new holders processed and unprocessed report.
	 */
	public HoldersProcessedAndUnprocessedReport() {
		// TODO Auto-generated constructor stub
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#generateData(com.pradera.model.report.ReportLogger)
	 */
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#getCustomJasperParameters()
	 */
	@Override
	public Map<String, Object> getCustomJasperParameters() {

		GenericSettlementOperationTO gfto = new GenericSettlementOperationTO();

		for(ReportLoggerDetail param: getReportLogger().getReportLoggerDetails()){
			if(param.getFilterName().equals("liquidation_process")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setLiquidationProcesses(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("settlement_schema")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setSettlementSchema(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("mechanism")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setMechanism(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("date_initial")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setInitialDate(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("date_end")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setFinalDate(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("currency")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setCurrency(param.getFilterValue().toString());
				}
			}
		}
		
		String strQuery = settlementReportServiceBean.getQueryHoldersProcessedAndUnprocessedReport(gfto); 
		 
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> secLiqProc = new HashMap<Integer, String>();
		String currencyDescription=null;
		try {
			filter.setMasterTableFk(MasterTableType.PARAMETER_TABLE_SCHED_SETTLEMENT.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secLiqProc.put(param.getParameterTablePk(), param.getDescription());
			}
			for(NegotiationMechanism param : settlementReportServiceBean.getLstNegotiationMechanismForId(Long.parseLong(gfto.getMechanism()))) {
				gfto.setMechanism(param.getDescription());
			}
			if(gfto.getCurrency()!= null){
				Integer intCurrencyCode = new Integer(gfto.getCurrency());
				ParameterTable param = parameterService.getParameterTableById(intCurrencyCode);
				currencyDescription = param.getDescription();
				}else
					currencyDescription = "Todos";
			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		if(!Validations.validateIsNotNull(gfto.getLiquidationProcesses())){
			gfto.setLiquidationProcesses("Todos") ;
		}else {
			gfto.setLiquidationProcesses(secLiqProc.get(new Integer(gfto.getLiquidationProcesses())));
		}
		if(!Validations.validateIsNotNull(gfto.getSettlementSchema())){
			gfto.setSettlementSchema("Todos");
		}
		parametersRequired.put("currencyDescription", currencyDescription);
		parametersRequired.put("str_Query", strQuery);
		parametersRequired.put("pMechanism", gfto.getMechanism());
		parametersRequired.put("pProcessLiq", gfto.getLiquidationProcesses());
		parametersRequired.put("pSchemaLiq", gfto.getSettlementSchema());
		parametersRequired.put("mLiqProc", secLiqProc);
		parametersRequired.put("initialDt", gfto.getInitialDate());
		parametersRequired.put("finalDt", gfto.getFinalDate());
		
		return parametersRequired;
		
		
	}

}
