package com.pradera.report.generation.executor.billing;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.accounts.Participant;
import com.pradera.model.billing.BillingService;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.billing.service.BillingServiceReportServiceBean;

@ReportProcess(name = "CommissionChangeOwnershipSecuritiesReport")
public class CommissionChangeOwnershipSecuritiesReport extends GenericReport {

	private static final long serialVersionUID = 1L;

	@Inject
	PraderaLogger log;

	@EJB
	private ParameterServiceBean parameterService;

	@Inject
	private BillingServiceReportServiceBean billingServiceReportServiceBean;

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		return null;
	}

	@Override
	public void addParametersQueryReport() {
	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		Map<String, Object> parametersRequired = new HashMap<>();
		ParameterTableTO filter = new ParameterTableTO();
		Map<Integer, String> motive = new HashMap<Integer, String>();
		Map<Integer, String> state = new HashMap<Integer, String>();
		Map<Integer, String> balance_type = new HashMap<Integer, String>();
		Map<Integer, String> affectation_type = new HashMap<Integer, String>();
		Map<Integer, String> level_type = new HashMap<Integer, String>();

		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();

		Long idParticipant = null;
		String serviceCode = null;

		String referenceRateParameter = "";
		String serviceDescriptionParameter = "";
		String clientParticipantParameter = "";

		// obteniendo el tipo de cambio del dolar la fecha
		DailyExchangeRates der = billingServiceReportServiceBean.getExchangeRateForSale(new Date());
		// valor por defecto
		String exchangeRateForSaleParameter = "6.96";
		if (der != null) {
			exchangeRateForSaleParameter = der.getSellPrice().toString();
		}

		for (ReportLoggerDetail r : loggerDetails) {

			if (r.getFilterName().equals("p_service_code")) {
				if (r.getFilterValue() != null) {
					serviceCode = r.getFilterValue();
				} else {
					serviceCode = "0";
				}
				// construir el nombre del servico y tambien encontrando la tarifa correspondiente
				if (!serviceCode.equals("0")) {
					BillingService bs = billingServiceReportServiceBean.getServiceDescription(serviceCode);
					if (bs != null) {
						referenceRateParameter = bs.getReferenceRate();
						serviceDescriptionParameter = "(" + serviceCode + ") " + bs.getServiceName();
					}
				}
			}

			if (r.getFilterName().equals("p_participant_code")) {
				if (r.getFilterValue() != null) {
					Long.parseLong(r.getFilterValue());
					idParticipant = Long.parseLong(r.getFilterValue());
				}

				// construyendo la descripcion del participante origen/cliente
////YA NO ES NECESARIO CALCULAR, XQ YA ESTA EN LA QUERY DEL REPORTE
//				if (idParticipant != null) {
//					Participant participant = billingServiceReportServiceBean.getParticipantToReport(idParticipant);
//					if (participant != null) {
//						clientParticipantParameter = "(" + participant.getMnemonic() + ") " + participant.getDescription();
//					}
//				} else {
//					clientParticipantParameter = "Todos los Participantes";
//				}
			}
		}

		try {

			filter.setMasterTableFk(MasterTableType.MASTER_TABLE_CHANGE_OWNERSHIP_MOTIVE_TYPE.getCode());
			for (ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				motive.put(param.getParameterTablePk(), param.getParameterName());
			}

			filter.setMasterTableFk(MasterTableType.MASTER_TABLE_CHANGE_OWNERSHIP_STATUS_TYPE.getCode());
			for (ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				state.put(param.getParameterTablePk(), param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.MASTER_TABLE_CHANGE_OWNERSHIP_BALANCE_TYPE.getCode());
			for (ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				balance_type.put(param.getParameterTablePk(), param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.TYPE_AFFECTATION.getCode());
			for (ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				affectation_type.put(param.getParameterTablePk(), param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.LEVEL_AFFECTATION.getCode());
			for (ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				level_type.put(param.getParameterTablePk(), param.getDescription());
			}

		} catch (Exception ex) {
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		parametersRequired.put("p_motive", motive);
		parametersRequired.put("p_state", state);
		parametersRequired.put("p_balance_type", balance_type);
		parametersRequired.put("p_affectation_type", affectation_type);
		parametersRequired.put("p_level_type", level_type);

		parametersRequired.put("p_reference_rate", referenceRateParameter);
		parametersRequired.put("p_service_description", serviceDescriptionParameter);
		parametersRequired.put("p_client_participant", clientParticipantParameter);
		parametersRequired.put("p_exchange_rate_sale", exchangeRateForSaleParameter);
		parametersRequired.put("p_service_code", serviceCode);
		parametersRequired.put("sp", idParticipant);
		
		//parametersRequired.put("date_initial", serviceCode);
		//parametersRequired.put("date_end", serviceCode);
		
		return parametersRequired;
	}

}
