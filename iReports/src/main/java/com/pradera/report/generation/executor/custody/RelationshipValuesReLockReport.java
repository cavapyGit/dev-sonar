package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;

import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "RelationshipValuesReLockReport")
public class RelationshipValuesReLockReport extends GenericReport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String MAP_AFFECTATION_TYPE="map_affectation_type";
	private Map<String, Object> map= new HashMap<String, Object>();
	@EJB
	private ParameterServiceBean parameterService;
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
	 return null;	
	}
	@Override
	public Map<String, Object> getCustomJasperParameters(){
		ParameterTableTO  parameterFilter = new ParameterTableTO();
		Map<Integer,String> affectation_type= new HashMap<Integer, String>();
		parameterFilter.setMasterTableFk(MasterTableType.TYPE_AFFECTATION.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(parameterFilter)) 
			affectation_type.put(param.getParameterTablePk(), param.getDescription());
		map.put(MAP_AFFECTATION_TYPE, affectation_type);
		map.put("logo_path", UtilReportConstants.readLogoReport());
		return map;
	}
}
