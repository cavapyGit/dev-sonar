package com.pradera.report.generation.executor.account;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.account.service.AccountReportServiceBean;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "RequestModifyingOfDataCuiReport")
public class RequestModifyingOfDataCuiReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	private AccountReportServiceBean accountReportServiceBean;

	@Inject
	private PraderaLogger log;

	public RequestModifyingOfDataCuiReport() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		GenericsFiltersTO gfto = new GenericsFiltersTO();

		for(ReportLoggerDetail param: getReportLogger().getReportLoggerDetails()){
			if(param.getFilterName().equals("participant")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setParticipant(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("cui_code")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setCui(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("state")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setState(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("date_initial")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setInitialDt(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("date_end")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setFinalDt(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals(ReportConstant.INSTITUTION_TYPE)){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setInstitutionType(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals(ReportConstant.ISSUER_CODE_INSTITUTION_TYPE)){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setIdIssuer(param.getFilterValue().toString());
				}
			}
		}
		String strQuery = accountReportServiceBean.getQueryRequestModifyingOfDataCuiReport(gfto);
		 
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> secHolderType = new HashMap<Integer, String>();
		Map<Integer,String> secHolderState = new HashMap<Integer, String>();
		Map<Integer,String> secRequestState = new HashMap<Integer, String>();
		Map<Integer,String> secRequestMotive = new HashMap<Integer, String>();
		try {
			filter.setMasterTableFk(MasterTableType.PERSON_TYPE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secHolderType.put(param.getParameterTablePk(), param.getParameterName());
			}
			filter.setMasterTableFk(MasterTableType.HOLDER_STATE_TYPE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secHolderState.put(param.getParameterTablePk(), param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.STATE_CREATION_HOLDER_REQUEST.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secRequestState.put(param.getParameterTablePk(), param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_REQUEST_ANNULAR_MOTIVE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secRequestMotive.put(param.getParameterTablePk(), param.getDescription());
			}
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		parametersRequired.put("str_Query", strQuery);
		parametersRequired.put("mHolderType", secHolderType);
		parametersRequired.put("mHolderState", secHolderState);
		parametersRequired.put("mRequestState", secRequestState);
		parametersRequired.put("mRequestMotive", secRequestMotive);
		parametersRequired.put("initialDt", gfto.getInitialDt());
		parametersRequired.put("finalDt", gfto.getFinalDt());
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		
		return parametersRequired;
		
		
	}

}
