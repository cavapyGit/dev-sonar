package com.pradera.report.generation.executor.settlement.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.settlement.SettlementAccountMarketfact;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RegisterChainedOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class RegisterChainedOperationTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The ind automatic. */
	private Integer indAutomatic;
	
	/** The id chained holder operation. */
	private Long idChainedHolderOperation;
	
	/** The id negotiation mechanism pk. */
	private Long idNegotiationMechanismPk;
	
	/** The modality group. */
	private ModalityGroup modalityGroup;
	
	/** The mechanism. */
	private NegotiationMechanism mechanism;
	
	/** The security. */
	private Security security;
	
	/** The participant. */
	private Participant participant;
	
	/** The holder. */
	private Holder holder;
	
	/** The holder account. */
	private HolderAccount holderAccount;
	
	/** The lst holder accounts. */
	private List<HolderAccount> lstHolderAccounts;
	
	/** The settlement date. */
	private Date settlementDate;
	
	/** The currency. */
	private ParameterTable currency;
	
	/** The sale quantity. */
	private BigDecimal saleQuantity = BigDecimal.ZERO;
	
	/** The purchase quantity. */
	private BigDecimal purchaseQuantity = BigDecimal.ZERO;
	
	/** The purchase pn. */
	private BigDecimal purchasePN = BigDecimal.ZERO;
	
	/** The sale pn. */
	private BigDecimal salePN = BigDecimal.ZERO;
	
	/** The market date. */
	private Date marketDate;
	
	/** The market rate. */
	private BigDecimal marketRate;
	
	/** The ind extended. */
	private ParameterTable indExtended;
	
	/** The chain state. */
	private ParameterTable chainState;
	
	/** The instrument type. */
	private Integer instrumentType;
//	private List<SettlementAccountOperation> saleSettlementAccountOperation;
/** The sale settlement account marketfact. */
//	private List<SettlementAccountOperation> purchaseSettlementAccountOperation;
	private List<SettlementAccountMarketfact> saleSettlementAccountMarketfact;
	
	/** The purchase settlement account marketfact. */
	private List<SettlementAccountMarketfact> purchaseSettlementAccountMarketfact;
	
	
	/**
	 * Instantiates a new register chained operation to.
	 */
	public RegisterChainedOperationTO(){
		security = new Security();
		holderAccount = new HolderAccount();
		participant= new Participant();
		modalityGroup= new ModalityGroup();
		currency= new ParameterTable();
		indExtended= new ParameterTable();
		chainState= new ParameterTable();
		NegotiationMechanism objMechainsm= new NegotiationMechanism();
		objMechainsm.setIdNegotiationMechanismPk(NegotiationMechanismType.BOLSA.getCode());
		objMechainsm.setDescription(NegotiationMechanismType.BOLSA.getDescripcion());
		modalityGroup.setNegotiationMechanism(objMechainsm);
//		saleSettlementAccountOperation= new LinkedList<SettlementAccountOperation>();
//		purchaseSettlementAccountOperation= new LinkedList<SettlementAccountOperation>();
		saleSettlementAccountMarketfact= new LinkedList<SettlementAccountMarketfact>();
		purchaseSettlementAccountMarketfact= new LinkedList<SettlementAccountMarketfact>();
		settlementDate= CommonsUtilities.currentDate();
		instrumentType= InstrumentType.FIXED_INCOME.getCode();
	}
	
	/**
	 * Gets the sale quantity.
	 *
	 * @return the sale quantity
	 */
	public BigDecimal getSaleQuantity() {
		return saleQuantity;
	}
	
	/**
	 * Sets the sale quantity.
	 *
	 * @param saleQuantity the new sale quantity
	 */
	public void setSaleQuantity(BigDecimal saleQuantity) {
		this.saleQuantity = saleQuantity;
	}
	
	/**
	 * Gets the purchase quantity.
	 *
	 * @return the purchase quantity
	 */
	public BigDecimal getPurchaseQuantity() {
		return purchaseQuantity;
	}
	
	/**
	 * Sets the purchase quantity.
	 *
	 * @param purchaseQuantity the new purchase quantity
	 */
	public void setPurchaseQuantity(BigDecimal purchaseQuantity) {
		this.purchaseQuantity = purchaseQuantity;
	}
	
	/**
	 * Gets the id negotiation mechanism pk.
	 *
	 * @return the id negotiation mechanism pk
	 */
	public Long getIdNegotiationMechanismPk() {
		return idNegotiationMechanismPk;
	}
	
	/**
	 * Sets the id negotiation mechanism pk.
	 *
	 * @param idNegotiationMechanismPk the new id negotiation mechanism pk
	 */
	public void setIdNegotiationMechanismPk(Long idNegotiationMechanismPk) {
		this.idNegotiationMechanismPk = idNegotiationMechanismPk;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}
	
	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}
	
	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}
	
	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}
	
	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}
	
	/**
	 * Gets the settlement date.
	 *
	 * @return the settlement date
	 */
	public Date getSettlementDate() {
		return settlementDate;
	}
	
	/**
	 * Sets the settlement date.
	 *
	 * @param settlementDate the new settlement date
	 */
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}
	
	/**
	 * Gets the lst holder accounts.
	 *
	 * @return the lst holder accounts
	 */
	public List<HolderAccount> getLstHolderAccounts() {
		return lstHolderAccounts;
	}
	
	/**
	 * Sets the lst holder accounts.
	 *
	 * @param lstHolderAccounts the new lst holder accounts
	 */
	public void setLstHolderAccounts(List<HolderAccount> lstHolderAccounts) {
		this.lstHolderAccounts = lstHolderAccounts;
	}
	
	/**
	 * Gets the purchase pn.
	 *
	 * @return the purchase pn
	 */
	public BigDecimal getPurchasePN() {
		return purchasePN;
	}
	
	/**
	 * Sets the purchase pn.
	 *
	 * @param purchasePN the new purchase pn
	 */
	public void setPurchasePN(BigDecimal purchasePN) {
		this.purchasePN = purchasePN;
	}
	
	/**
	 * Gets the sale pn.
	 *
	 * @return the sale pn
	 */
	public BigDecimal getSalePN() {
		return salePN;
	}
	
	/**
	 * Sets the sale pn.
	 *
	 * @param salePN the new sale pn
	 */
	public void setSalePN(BigDecimal salePN) {
		this.salePN = salePN;
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * Gets the market date.
	 *
	 * @return the market date
	 */
	public Date getMarketDate() {
		return marketDate;
	}

	/**
	 * Sets the market date.
	 *
	 * @param marketDate the new market date
	 */
	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	/**
	 * Gets the market rate.
	 *
	 * @return the market rate
	 */
	public BigDecimal getMarketRate() {
		return marketRate;
	}

	/**
	 * Sets the market rate.
	 *
	 * @param marketRate the new market rate
	 */
	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	/**
	 * Gets the modality group.
	 *
	 * @return the modality group
	 */
	public ModalityGroup getModalityGroup() {
		return modalityGroup;
	}

	/**
	 * Sets the modality group.
	 *
	 * @param modalityGroup the new modality group
	 */
	public void setModalityGroup(ModalityGroup modalityGroup) {
		this.modalityGroup = modalityGroup;
	}

	/**
	 * Gets the id chained holder operation.
	 *
	 * @return the id chained holder operation
	 */
	public Long getIdChainedHolderOperation() {
		return idChainedHolderOperation;
	}

	/**
	 * Sets the id chained holder operation.
	 *
	 * @param idChainedHolderOperation the new id chained holder operation
	 */
	public void setIdChainedHolderOperation(Long idChainedHolderOperation) {
		this.idChainedHolderOperation = idChainedHolderOperation;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public ParameterTable getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(ParameterTable currency) {
		this.currency = currency;
	}

	/**
	 * Gets the ind extended.
	 *
	 * @return the ind extended
	 */
	public ParameterTable getIndExtended() {
		return indExtended;
	}

	/**
	 * Sets the ind extended.
	 *
	 * @param indExtended the new ind extended
	 */
	public void setIndExtended(ParameterTable indExtended) {
		this.indExtended = indExtended;
	}

	/**
	 * Gets the chain state.
	 *
	 * @return the chain state
	 */
	public ParameterTable getChainState() {
		return chainState;
	}

	/**
	 * Sets the chain state.
	 *
	 * @param chainState the new chain state
	 */
	public void setChainState(ParameterTable chainState) {
		this.chainState = chainState;
	}

	/**
	 * Gets the mechanism.
	 *
	 * @return the mechanism
	 */
	public NegotiationMechanism getMechanism() {
		return mechanism;
	}

	/**
	 * Sets the mechanism.
	 *
	 * @param mechanism the new mechanism
	 */
	public void setMechanism(NegotiationMechanism mechanism) {
		this.mechanism = mechanism;
	}

	/**
	 * Gets the ind automatic.
	 *
	 * @return the ind automatic
	 */
	public Integer getIndAutomatic() {
		return indAutomatic;
	}

	/**
	 * Sets the ind automatic.
	 *
	 * @param indAutomatic the new ind automatic
	 */
	public void setIndAutomatic(Integer indAutomatic) {
		this.indAutomatic = indAutomatic;
	}

	/**
	 * Gets the sale settlement account marketfact.
	 *
	 * @return the sale settlement account marketfact
	 */
	public List<SettlementAccountMarketfact> getSaleSettlementAccountMarketfact() {
		return saleSettlementAccountMarketfact;
	}

	/**
	 * Sets the sale settlement account marketfact.
	 *
	 * @param saleSettlementAccountMarketfact the new sale settlement account marketfact
	 */
	public void setSaleSettlementAccountMarketfact(
			List<SettlementAccountMarketfact> saleSettlementAccountMarketfact) {
		this.saleSettlementAccountMarketfact = saleSettlementAccountMarketfact;
	}

	/**
	 * Gets the purchase settlement account marketfact.
	 *
	 * @return the purchase settlement account marketfact
	 */
	public List<SettlementAccountMarketfact> getPurchaseSettlementAccountMarketfact() {
		return purchaseSettlementAccountMarketfact;
	}

	/**
	 * Sets the purchase settlement account marketfact.
	 *
	 * @param purchaseSettlementAccountMarketfact the new purchase settlement account marketfact
	 */
	public void setPurchaseSettlementAccountMarketfact(
			List<SettlementAccountMarketfact> purchaseSettlementAccountMarketfact) {
		this.purchaseSettlementAccountMarketfact = purchaseSettlementAccountMarketfact;
	}

	/**
	 * Gets the instrument type.
	 *
	 * @return the instrument type
	 */
	public Integer getInstrumentType() {
		return instrumentType;
	}

	/**
	 * Sets the instrument type.
	 *
	 * @param instrumentType the new instrument type
	 */
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

}
