package com.pradera.report.generation.executor.custody.to;

import java.io.Serializable;
import java.util.Date;

public class RequestAccreditationCertificationTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4172964944204487184L;
	
	private Long idHolderAccountPk;
	private Long idHolderPk;
	private Long participantPk;
	private String motive;
	private String requestNumber;
	private Integer requestState;
	private String strDateInitial;
	private String strDateEnd;
	private String participantDesc;

	public Long getParticipantPk() {
		return participantPk;
	}
	public void setParticipantPk(Long participantPk) {
		this.participantPk = participantPk;
	}
	public String getParticipantDesc() {
		return participantDesc;
	}
	public void setParticipantDesc(String participantDesc) {
		this.participantDesc = participantDesc;
	}
		public String getStrDateInitial() {
		return strDateInitial;
	}
	public void setStrDateInitial(String strDateInitial) {
		this.strDateInitial = strDateInitial;
	}
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}
	public Long getIdHolderPk() {
		return idHolderPk;
	}
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}
	public Integer getRequestState() {
		return requestState;
	}
	public void setRequestState(Integer requestState) {
		this.requestState = requestState;
	}
	public String getStrDateEnd() {
		return strDateEnd;
	}
	public void setStrDateEnd(String strDateEnd) {
		this.strDateEnd = strDateEnd;
	}
	public String getMotive() {
		return motive;
	}
	public void setMotive(String motive) {
		this.motive = motive;
	}
	public String getRequestNumber() {
		return requestNumber;
	}
	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}
	

}
