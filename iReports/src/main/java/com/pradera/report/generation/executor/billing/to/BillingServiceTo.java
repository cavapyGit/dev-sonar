package com.pradera.report.generation.executor.billing.to;

import java.io.Serializable;
import java.util.Date;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingServiceTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class BillingServiceTo implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id billing service pk. */
	private Long idBillingServicePk;
	
	/** The service type. */
	private Integer serviceType;
	
	/** The entity collection. */
	private Integer entityCollection;
	
	/** The state service. */
	private Integer stateService;
	
	/** The base collection. */
	private Integer baseCollection;
	
	/** The calculation period. */
	private Integer calculationPeriod;
	
	/** The service code. */
	private String serviceCode;
	
	/** The service name. */
	private String serviceName;
	
	/** The end effective date. */
	private Date endEffectiveDate;
	
	/** The initial effective date. */
	private Date initialEffectiveDate;	
	
	/** The description collection entity. */
	private String descriptionCollectionEntity;	
	
	/** The description base collection. */
	private String descriptionBaseCollection;	
	
	/** The description state billing service. */
	private String descriptionStateBillingService;	
	
	/** The description service type. */
	private String descriptionServiceType;	
	
	/** The description collection period. */
	private String descriptionCollectionPeriod;
	
	/** The description client service type. */
	private String descriptionClientServiceType;	
	
	/** The description calculation period. */
	private String descriptionCalculationPeriod;
	
	/** The description name collection entity. */
	private String descriptionNameCollectionEntity;
	
	/** The collection period. */
	private Integer collectionPeriod;
	
	/** The client service type. */
	private Integer clientServiceType; 
	
	/** The currente service. */
	private Integer currenteService;
	
	/** The in integrated bill. */
	private Integer inIntegratedBill;
	
	
	/** The date calculation period. */
	private Date 	dateCalculationPeriod;
	
	/** The calculate daily. */
	private Boolean calculateDaily = Boolean.FALSE;
	
	/** The calculate monthly. */
	private Boolean calculateMonthly= Boolean.FALSE;
	
	/** The collection record to. */
	private CollectionRecordTo collectionRecordTo;

	/**
	 * Gets the id billing service pk.
	 *
	 * @return the id billing service pk
	 */
	public Long getIdBillingServicePk() {
		return idBillingServicePk;
	}

	/**
	 * Sets the id billing service pk.
	 *
	 * @param idBillingServicePk the new id billing service pk
	 */
	public void setIdBillingServicePk(Long idBillingServicePk) {
		this.idBillingServicePk = idBillingServicePk;
	}

	/**
	 * Gets the service type.
	 *
	 * @return the service type
	 */
	public Integer getServiceType() {
		return serviceType;
	}

	/**
	 * Sets the service type.
	 *
	 * @param serviceType the new service type
	 */
	public void setServiceType(Integer serviceType) {
		this.serviceType = serviceType;
	}

	/**
	 * Gets the entity collection.
	 *
	 * @return the entity collection
	 */
	public Integer getEntityCollection() {
		return entityCollection;
	}

	/**
	 * Sets the entity collection.
	 *
	 * @param entityCollection the new entity collection
	 */
	public void setEntityCollection(Integer entityCollection) {
		this.entityCollection = entityCollection;
	}

	/**
	 * Gets the state service.
	 *
	 * @return the state service
	 */
	public Integer getStateService() {
		return stateService;
	}

	/**
	 * Sets the state service.
	 *
	 * @param stateService the new state service
	 */
	public void setStateService(Integer stateService) {
		this.stateService = stateService;
	}

	/**
	 * Gets the base collection.
	 *
	 * @return the base collection
	 */
	public Integer getBaseCollection() {
		return baseCollection;
	}

	/**
	 * Sets the base collection.
	 *
	 * @param baseCollection the new base collection
	 */
	public void setBaseCollection(Integer baseCollection) {
		this.baseCollection = baseCollection;
	}

	/**
	 * Gets the calculation period.
	 *
	 * @return the calculation period
	 */
	public Integer getCalculationPeriod() {
		return calculationPeriod;
	}

	/**
	 * Sets the calculation period.
	 *
	 * @param calculationPeriod the new calculation period
	 */
	public void setCalculationPeriod(Integer calculationPeriod) {
		this.calculationPeriod = calculationPeriod;
	}

	/**
	 * Gets the service code.
	 *
	 * @return the service code
	 */
	public String getServiceCode() {
		return serviceCode;
	}

	/**
	 * Sets the service code.
	 *
	 * @param serviceCode the new service code
	 */
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	/**
	 * Gets the service name.
	 *
	 * @return the service name
	 */
	public String getServiceName() {
		return serviceName;
	}

	/**
	 * Sets the service name.
	 *
	 * @param serviceName the new service name
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	/**
	 * Gets the end effective date.
	 *
	 * @return the end effective date
	 */
	public Date getEndEffectiveDate() {
		return endEffectiveDate;
	}

	/**
	 * Sets the end effective date.
	 *
	 * @param endEffectiveDate the new end effective date
	 */
	public void setEndEffectiveDate(Date endEffectiveDate) {
		this.endEffectiveDate = endEffectiveDate;
	}

	/**
	 * Gets the initial effective date.
	 *
	 * @return the initial effective date
	 */
	public Date getInitialEffectiveDate() {
		return initialEffectiveDate;
	}

	/**
	 * Sets the initial effective date.
	 *
	 * @param initialEffectiveDate the new initial effective date
	 */
	public void setInitialEffectiveDate(Date initialEffectiveDate) {
		this.initialEffectiveDate = initialEffectiveDate;
	}

	/**
	 * Gets the description collection entity.
	 *
	 * @return the description collection entity
	 */
	public String getDescriptionCollectionEntity() {
		return descriptionCollectionEntity;
	}

	/**
	 * Sets the description collection entity.
	 *
	 * @param descriptionCollectionEntity the new description collection entity
	 */
	public void setDescriptionCollectionEntity(String descriptionCollectionEntity) {
		this.descriptionCollectionEntity = descriptionCollectionEntity;
	}

	/**
	 * Gets the description base collection.
	 *
	 * @return the description base collection
	 */
	public String getDescriptionBaseCollection() {
		return descriptionBaseCollection;
	}

	/**
	 * Sets the description base collection.
	 *
	 * @param descriptionBaseCollection the new description base collection
	 */
	public void setDescriptionBaseCollection(String descriptionBaseCollection) {
		this.descriptionBaseCollection = descriptionBaseCollection;
	}

	/**
	 * Gets the description state billing service.
	 *
	 * @return the description state billing service
	 */
	public String getDescriptionStateBillingService() {
		return descriptionStateBillingService;
	}

	/**
	 * Sets the description state billing service.
	 *
	 * @param descriptionStateBillingService the new description state billing service
	 */
	public void setDescriptionStateBillingService(
			String descriptionStateBillingService) {
		this.descriptionStateBillingService = descriptionStateBillingService;
	}

	/**
	 * Gets the description service type.
	 *
	 * @return the description service type
	 */
	public String getDescriptionServiceType() {
		return descriptionServiceType;
	}

	/**
	 * Sets the description service type.
	 *
	 * @param descriptionServiceType the new description service type
	 */
	public void setDescriptionServiceType(String descriptionServiceType) {
		this.descriptionServiceType = descriptionServiceType;
	}

	/**
	 * Gets the description collection period.
	 *
	 * @return the description collection period
	 */
	public String getDescriptionCollectionPeriod() {
		return descriptionCollectionPeriod;
	}

	/**
	 * Sets the description collection period.
	 *
	 * @param descriptionCollectionPeriod the new description collection period
	 */
	public void setDescriptionCollectionPeriod(String descriptionCollectionPeriod) {
		this.descriptionCollectionPeriod = descriptionCollectionPeriod;
	}

	/**
	 * Gets the description client service type.
	 *
	 * @return the description client service type
	 */
	public String getDescriptionClientServiceType() {
		return descriptionClientServiceType;
	}

	/**
	 * Sets the description client service type.
	 *
	 * @param descriptionClientServiceType the new description client service type
	 */
	public void setDescriptionClientServiceType(String descriptionClientServiceType) {
		this.descriptionClientServiceType = descriptionClientServiceType;
	}

	/**
	 * Gets the description calculation period.
	 *
	 * @return the description calculation period
	 */
	public String getDescriptionCalculationPeriod() {
		return descriptionCalculationPeriod;
	}

	/**
	 * Sets the description calculation period.
	 *
	 * @param descriptionCalculationPeriod the new description calculation period
	 */
	public void setDescriptionCalculationPeriod(String descriptionCalculationPeriod) {
		this.descriptionCalculationPeriod = descriptionCalculationPeriod;
	}

	/**
	 * Gets the description name collection entity.
	 *
	 * @return the description name collection entity
	 */
	public String getDescriptionNameCollectionEntity() {
		return descriptionNameCollectionEntity;
	}

	/**
	 * Sets the description name collection entity.
	 *
	 * @param descriptionNameCollectionEntity the new description name collection entity
	 */
	public void setDescriptionNameCollectionEntity(
			String descriptionNameCollectionEntity) {
		this.descriptionNameCollectionEntity = descriptionNameCollectionEntity;
	}

	/**
	 * Gets the collection period.
	 *
	 * @return the collection period
	 */
	public Integer getCollectionPeriod() {
		return collectionPeriod;
	}

	/**
	 * Sets the collection period.
	 *
	 * @param collectionPeriod the new collection period
	 */
	public void setCollectionPeriod(Integer collectionPeriod) {
		this.collectionPeriod = collectionPeriod;
	}

	/**
	 * Gets the client service type.
	 *
	 * @return the client service type
	 */
	public Integer getClientServiceType() {
		return clientServiceType;
	}

	/**
	 * Sets the client service type.
	 *
	 * @param clientServiceType the new client service type
	 */
	public void setClientServiceType(Integer clientServiceType) {
		this.clientServiceType = clientServiceType;
	}

	/**
	 * Gets the currente service.
	 *
	 * @return the currente service
	 */
	public Integer getCurrenteService() {
		return currenteService;
	}

	/**
	 * Sets the currente service.
	 *
	 * @param currenteService the new currente service
	 */
	public void setCurrenteService(Integer currenteService) {
		this.currenteService = currenteService;
	}

	/**
	 * Gets the date calculation period.
	 *
	 * @return the date calculation period
	 */
	public Date getDateCalculationPeriod() {
		return dateCalculationPeriod;
	}

	/**
	 * Sets the date calculation period.
	 *
	 * @param dateCalculationPeriod the new date calculation period
	 */
	public void setDateCalculationPeriod(Date dateCalculationPeriod) {
		this.dateCalculationPeriod = dateCalculationPeriod;
	}

	/**
	 * Gets the calculate daily.
	 *
	 * @return the calculate daily
	 */
	public Boolean getCalculateDaily() {
		return calculateDaily;
	}

	/**
	 * Sets the calculate daily.
	 *
	 * @param calculateDaily the new calculate daily
	 */
	public void setCalculateDaily(Boolean calculateDaily) {
		this.calculateDaily = calculateDaily;
	}

	/**
	 * Gets the calculate monthly.
	 *
	 * @return the calculate monthly
	 */
	public Boolean getCalculateMonthly() {
		return calculateMonthly;
	}

	/**
	 * Sets the calculate monthly.
	 *
	 * @param calculateMonthly the new calculate monthly
	 */
	public void setCalculateMonthly(Boolean calculateMonthly) {
		this.calculateMonthly = calculateMonthly;
	}

	/**
	 * Gets the collection record to.
	 *
	 * @return the collection record to
	 */
	public CollectionRecordTo getCollectionRecordTo() {
		return collectionRecordTo;
	}

	/**
	 * Sets the collection record to.
	 *
	 * @param collectionRecordTo the new collection record to
	 */
	public void setCollectionRecordTo(CollectionRecordTo collectionRecordTo) {
		this.collectionRecordTo = collectionRecordTo;
	}

	/**
	 * Gets the in integrated bill.
	 *
	 * @return the in integrated bill
	 */
	public Integer getInIntegratedBill() {
		return inIntegratedBill;
	}

	/**
	 * Sets the in integrated bill.
	 *
	 * @param inIntegratedBill the new in integrated bill
	 */
	public void setInIntegratedBill(Integer inIntegratedBill) {
		this.inIntegratedBill = inIntegratedBill;
	}
	
	
}
