package com.pradera.report.generation.executor.negotiation;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "TotalAmountTradedReport")
public class TotalAmountTradedReport extends GenericReport{
	
	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;
	@Inject
	PraderaLogger log;
	
	@EJB
	ParticipantServiceBean participantServiceBean;

	public TotalAmountTradedReport() {
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParametersQueryReport() {

	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {

		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> currency = new HashMap<Integer, String>();
		//List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		
		//String mechanismDescription = "TODOS";
		//String modalityDescription = "TODOS";
		
		/*for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.MODALITY_PARAM) && r.getFilterValue()!=null){
				modalityDescription = r.getFilterDescription();
			}
			if (r.getFilterName().equals(ReportConstant.MECHANISM_PARAM) && r.getFilterValue()!=null){
				mechanismDescription = r.getFilterDescription();
			}
		}*/
		
		try {	
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				currency.put(param.getParameterTablePk(), param.getParameterName());
			}
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		//parametersRequired.put("mechanism_desc", mechanismDescription);
		//parametersRequired.put("modality_desc", modalityDescription);
		parametersRequired.put("p_currency", currency);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		// TODO Auto-generated method stub
		return parametersRequired;
	}
}
