package com.pradera.report.generation.executor.security.to;

import java.io.Serializable;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class AuditTrackTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 28/01/2014
 */
public class AuditTrackTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 892251176588411026L;
	
	/** The id system pk. */
	private Integer idSystemPk;
	
	/** The table name. */
	private String tableName;
	
	/** The column name. */
	private String columnName;
	
	/** The user name. */
	private String userName;
	
	/** The event type. */
	private Integer eventType;
	
	/** The start date. */
	private Date startDate;
	
	/** The end date. */
	private Date endDate;

	/**
	 * Instantiates a new audit track to.
	 */
	public AuditTrackTO() {
	
	}

	/**
	 * Gets the id system pk.
	 *
	 * @return the id system pk
	 */
	public Integer getIdSystemPk() {
		return idSystemPk;
	}

	/**
	 * Sets the id system pk.
	 *
	 * @param idSystemPk the new id system pk
	 */
	public void setIdSystemPk(Integer idSystemPk) {
		this.idSystemPk = idSystemPk;
	}

	/**
	 * Gets the table name.
	 *
	 * @return the table name
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * Sets the table name.
	 *
	 * @param tableName the new table name
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	/**
	 * Gets the column name.
	 *
	 * @return the column name
	 */
	public String getColumnName() {
		return columnName;
	}

	/**
	 * Sets the column name.
	 *
	 * @param columnName the new column name
	 */
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the event type.
	 *
	 * @return the event type
	 */
	public Integer getEventType() {
		return eventType;
	}

	/**
	 * Sets the event type.
	 *
	 * @param eventType the new event type
	 */
	public void setEventType(Integer eventType) {
		this.eventType = eventType;
	}

	/**
	 * Gets the start date.
	 *
	 * @return the start date
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Sets the start date.
	 *
	 * @param startDate the new start date
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
		

}
