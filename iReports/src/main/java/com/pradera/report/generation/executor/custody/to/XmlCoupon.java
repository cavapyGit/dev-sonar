package com.pradera.report.generation.executor.custody.to;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD) 
public class XmlCoupon implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7904767439411131844L;

	@XmlElement(name = "NRO_CUPON")
	private int couponNumber;
	
	@XmlElement(name = "PLAZO_CUPON")
	private int couponTerm;
	
	@XmlElement(name = "FECHA_VENC")
	private String expirationDate;
	
	@XmlElement(name = "CANT_OFERTADA")
	private int quantityOffered;
	
	@XmlElement(name = "CANT_ACEPTADA")
	private int acceptedAmount;
	
	@XmlElement(name = "PLAZO_RESTANTE")
	private int term;
	
	@XmlElement(name = "MONTO_CUPON")
	private BigDecimal couponAmount;

	public int getCouponNumber() {
		return couponNumber;
	}

	public void setCouponNumber(int couponNumber) {
		this.couponNumber = couponNumber;
	}

	public int getCouponTerm() {
		return couponTerm;
	}

	public void setCouponTerm(int couponTerm) {
		this.couponTerm = couponTerm;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public int getQuantityOffered() {
		return quantityOffered;
	}

	public void setQuantityOffered(int quantityOffered) {
		this.quantityOffered = quantityOffered;
	}

	public int getAcceptedAmount() {
		return acceptedAmount;
	}

	public void setAcceptedAmount(int acceptedAmount) {
		this.acceptedAmount = acceptedAmount;
	}

	public int getTerm() {
		return term;
	}

	public void setTerm(int term) {
		this.term = term;
	}

	public BigDecimal getCouponAmount() {
		return couponAmount;
	}

	public void setCouponAmount(BigDecimal couponAmount) {
		this.couponAmount = couponAmount;
	}
	 
}
