package com.pradera.report.generation.executor.corporative.to;

import java.io.Serializable;
import java.util.Date;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CorporativeProcesInterestTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 22-abr-2015
 */
public class CorporativeProcesInterestTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1316916691845191629L;
	
	/** The process id. */
	private Long 	processId;
	
	/** The cutoff date. */
	private Date 	cutoffDate;
	
	/** The delivery date. */
	private Date 	deliveryDate;
	
	/** The registry date. */
	private Date 	registryDate;
	
	/** The event type. */
	private Integer eventType;
	
	/** The security code. */
	private String 	securityCode;
	
	/** The target security code. */
	private String 	targetSecurityCode;
	
	/** The code value. */
	private String 	codeValue;
	
	/** The dest code value. */
	private String 	destCodeValue;
	
	/** The report type. */
	private Integer reportType;
	
	/** The state. */
	private Integer state;
	
	/** The stock id. */
	private Long stockId;

	/**
	 * Gets the process id.
	 *
	 * @return the process id
	 */
	public Long getProcessId() {
		return processId;
	}
	
	/**
	 * Sets the process id.
	 *
	 * @param processId the new process id
	 */
	public void setProcessId(Long processId) {
		this.processId = processId;
	}
	
	/**
	 * Gets the cutoff date.
	 *
	 * @return the cutoff date
	 */
	public Date getCutoffDate() {
		return cutoffDate;
	}
	
	/**
	 * Sets the cutoff date.
	 *
	 * @param cutoffDate the new cutoff date
	 */
	public void setCutoffDate(Date cutoffDate) {
		this.cutoffDate = cutoffDate;
	}
	
	/**
	 * Gets the delivery date.
	 *
	 * @return the delivery date
	 */
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	
	/**
	 * Sets the delivery date.
	 *
	 * @param deliveryDate the new delivery date
	 */
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	
	/**
	 * Gets the event type.
	 *
	 * @return the event type
	 */
	public Integer getEventType() {
		return eventType;
	}
	
	/**
	 * Sets the event type.
	 *
	 * @param eventType the new event type
	 */
	public void setEventType(Integer eventType) {
		this.eventType = eventType;
	}
	
	/**
	 * Gets the code value.
	 *
	 * @return the code value
	 */
	public String getCodeValue() {
		return codeValue;
	}
	
	/**
	 * Sets the code value.
	 *
	 * @param codeValue the new code value
	 */
	public void setCodeValue(String codeValue) {
		this.codeValue = codeValue;
	}
	
	/**
	 * Gets the dest code value.
	 *
	 * @return the dest code value
	 */
	public String getDestCodeValue() {
		return destCodeValue;
	}
	
	/**
	 * Sets the dest code value.
	 *
	 * @param destCodeValue the new dest code value
	 */
	public void setDestCodeValue(String destCodeValue) {
		this.destCodeValue = destCodeValue;
	}
	
	/**
	 * Gets the report type.
	 *
	 * @return the report type
	 */
	public Integer getReportType() {
		return reportType;
	}
	
	/**
	 * Sets the report type.
	 *
	 * @param reportType the new report type
	 */
	public void setReportType(Integer reportType) {
		this.reportType = reportType;
	}
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}
	
	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}
	
	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}
	
	/**
	 * Gets the security code.
	 *
	 * @return the security code
	 */
	public String getSecurityCode() {
		return securityCode;
	}
	
	/**
	 * Sets the security code.
	 *
	 * @param securityCode the new security code
	 */
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
	
	/**
	 * Gets the target security code.
	 *
	 * @return the target security code
	 */
	public String getTargetSecurityCode() {
		return targetSecurityCode;
	}
	
	/**
	 * Sets the target security code.
	 *
	 * @param targetSecurityCode the new target security code
	 */
	public void setTargetSecurityCode(String targetSecurityCode) {
		this.targetSecurityCode = targetSecurityCode;
	}

	/**
	 * Gets the stock id.
	 *
	 * @return the stock id
	 */
	public Long getStockId() {
		return stockId;
	}

	/**
	 * Sets the stock id.
	 *
	 * @param stockId the new stock id
	 */
	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

}
