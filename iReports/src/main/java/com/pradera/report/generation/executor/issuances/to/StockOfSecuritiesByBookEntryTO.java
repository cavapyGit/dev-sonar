package com.pradera.report.generation.executor.issuances.to;

import java.io.Serializable;
import java.math.BigDecimal;

public class StockOfSecuritiesByBookEntryTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer currency;
	/*CURRENCY NEMONIC*/
	private String currencyMnemonic;
	/*TC*/
	private BigDecimal exchangeRate;
	/*CLASE DE VALOR*/
	private Integer securtyClass;
	/*CLASE DE VALOR NEMONICO*/
	private String securtyClassMnemonic;
	/*NOMINAL_VALUE*/
	private BigDecimal nominalValue;
	
	private BigDecimal nominalValueUSD;
	
	/*TOTAL_BALANCE*/
	private BigDecimal totalBalance;
	
	public StockOfSecuritiesByBookEntryTO() {
		// TODO Auto-generated constructor stub
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public String getCurrencyMnemonic() {
		return currencyMnemonic;
	}

	public void setCurrencyMnemonic(String currencyMnemonic) {
		this.currencyMnemonic = currencyMnemonic;
	}

	public BigDecimal getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public Integer getSecurtyClass() {
		return securtyClass;
	}

	public void setSecurtyClass(Integer securtyClass) {
		this.securtyClass = securtyClass;
	}

	public String getSecurtyClassMnemonic() {
		return securtyClassMnemonic;
	}

	public void setSecurtyClassMnemonic(String securtyClassMnemonic) {
		this.securtyClassMnemonic = securtyClassMnemonic;
	}

	public BigDecimal getNominalValue() {
		return nominalValue;
	}

	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}

	public BigDecimal getNominalValueUSD() {
		return nominalValueUSD;
	}

	public void setNominalValueUSD(BigDecimal nominalValueUSD) {
		this.nominalValueUSD = nominalValueUSD;
	}

	public BigDecimal getTotalBalance() {
		return totalBalance;
	}

	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}
	
}
