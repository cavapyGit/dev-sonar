package com.pradera.report.generation.executor.custody.to;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;



@XmlAccessorType(XmlAccessType.FIELD) 
public class XmlRegSirtex implements  Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7211492958081380930L;
	@XmlElement(name = "REG_SIRTEX")
	private List<XmlSeccSirtex> seccSirtexs;
	public List<XmlSeccSirtex> getSeccSirtexs() {
		return seccSirtexs;
	}
	public void setSeccSirtexs(List<XmlSeccSirtex> seccSirtexs) {
		this.seccSirtexs = seccSirtexs;
	}
}
