package com.pradera.report.generation.executor.settlement.to;

import java.io.Serializable;
import java.math.BigDecimal;

public class CuadreLogbookTO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String montoTotalCV;
	private String cantOperacionesMTotalCV;
	private String liquidDiferidaCV;
	private String cantOperacionesLDiferidaCV;
	private String etapa1CV;
	private String cantOperacionesEtapa1CV;
	private String etapa2CV;
	private String cantOperacioneEtapa2CV;
	private String melorCV;
	private String cantOperacionesMelorCV;
	private String melidCV;
	private String cantOperacionesMelidCV;
	private String incumplimientosCV;
	private String cantOperacionesIncumplimientosCV;
	
	private String montoTotalREP;
	private String cantOperacionesMTotalREP;
	private String liquidDiferidaREP;
	private String cantOperacionesLDiferidaREP;
	private String etapa1REP;
	private String cantOperacionesEtapa1REP;
	private String etapa2REP;
	private String cantOperacioneEtapa2REP;
	private String melorREP;
	private String cantOperacionesMelorREP;
	private String melidREP;
	private String cantOperacionesMelidREP;
	private String incumplimientosREP;
	private String cantOperacionesIncumplimientosREP;
	
	private String montoTotalCP;
	private String cantOperacionesMTotalCP;
	private String liquidDiferidaCP;
	private String cantOperacionesLDiferidaCP;
	private String etapa1CP;
	private String cantOperacionesEtapa1CP;
	private String etapa2CP;
	private String cantOperacioneEtapa2CP;
	private String melorCP;
	private String cantOperacionesMelorCP;
	private String melidCP;
	private String cantOperacionesMelidCP;
	private String incumplimientosCP;
	private String cantOperacionesIncumplimientosCP;
	
	//total amounts, just for the consolidate cuadre
	private BigDecimal allMontoTotalSum;
	private BigDecimal allLiquidDiferidaSum;
	private BigDecimal allEtapa1Sum;
	private BigDecimal allEtapa2Sum;
	private BigDecimal allMelorSum;
	private BigDecimal allMelidSum;
	private BigDecimal allIncumplimientosSum;
	private BigDecimal allCantOperacionesMTotalSum;
	private BigDecimal allCantOperacionesLDiferidaSum;
	private BigDecimal allCantOperacionesEtapa1Sum;
	private BigDecimal allCantOperacionesEtapa2Sum;
	private BigDecimal allCantOperacionesMelorSum;
	private BigDecimal allCantOperacionesMelidSum;
	private BigDecimal allCantOperacionesIncumplimientosSum;
	private BigDecimal differenceAmount;
	private BigDecimal differenceOperation;
	
	public String getMontoTotalCV() {
		return montoTotalCV;
	}
	public void setMontoTotalCV(String montoTotalCV) {
		this.montoTotalCV = montoTotalCV;
	}
	public String getCantOperacionesMTotalCV() {
		return cantOperacionesMTotalCV;
	}
	public void setCantOperacionesMTotalCV(String cantOperacionesMTotalCV) {
		this.cantOperacionesMTotalCV = cantOperacionesMTotalCV;
	}
	public String getLiquidDiferidaCV() {
		return liquidDiferidaCV;
	}
	public void setLiquidDiferidaCV(String liquidDiferidaCV) {
		this.liquidDiferidaCV = liquidDiferidaCV;
	}
	public String getCantOperacionesLDiferidaCV() {
		return cantOperacionesLDiferidaCV;
	}
	public void setCantOperacionesLDiferidaCV(String cantOperacionesLDiferidaCV) {
		this.cantOperacionesLDiferidaCV = cantOperacionesLDiferidaCV;
	}
	public String getEtapa1CV() {
		return etapa1CV;
	}
	public void setEtapa1CV(String etapa1cv) {
		etapa1CV = etapa1cv;
	}
	public String getCantOperacionesEtapa1CV() {
		return cantOperacionesEtapa1CV;
	}
	public void setCantOperacionesEtapa1CV(String cantOperacionesEtapa1CV) {
		this.cantOperacionesEtapa1CV = cantOperacionesEtapa1CV;
	}
	public String getEtapa2CV() {
		return etapa2CV;
	}
	public void setEtapa2CV(String etapa2cv) {
		etapa2CV = etapa2cv;
	}
	public String getCantOperacioneEtapa2CV() {
		return cantOperacioneEtapa2CV;
	}
	public void setCantOperacioneEtapa2CV(String cantOperacioneEtapa2CV) {
		this.cantOperacioneEtapa2CV = cantOperacioneEtapa2CV;
	}
	public String getMelorCV() {
		return melorCV;
	}
	public void setMelorCV(String melorCV) {
		this.melorCV = melorCV;
	}
	public String getCantOperacionesMelorCV() {
		return cantOperacionesMelorCV;
	}
	public void setCantOperacionesMelorCV(String cantOperacionesMelorCV) {
		this.cantOperacionesMelorCV = cantOperacionesMelorCV;
	}
	public String getMelidCV() {
		return melidCV;
	}
	public void setMelidCV(String melidCV) {
		this.melidCV = melidCV;
	}
	public String getCantOperacionesMelidCV() {
		return cantOperacionesMelidCV;
	}
	public void setCantOperacionesMelidCV(String cantOperacionesMelidCV) {
		this.cantOperacionesMelidCV = cantOperacionesMelidCV;
	}
	public String getIncumplimientosCV() {
		return incumplimientosCV;
	}
	public void setIncumplimientosCV(String incumplimientosCV) {
		this.incumplimientosCV = incumplimientosCV;
	}
	public String getCantOperacionesIncumplimientosCV() {
		return cantOperacionesIncumplimientosCV;
	}
	public void setCantOperacionesIncumplimientosCV(
			String cantOperacionesIncumplimientosCV) {
		this.cantOperacionesIncumplimientosCV = cantOperacionesIncumplimientosCV;
	}
	public String getMontoTotalREP() {
		return montoTotalREP;
	}
	public void setMontoTotalREP(String montoTotalREP) {
		this.montoTotalREP = montoTotalREP;
	}
	public String getCantOperacionesMTotalREP() {
		return cantOperacionesMTotalREP;
	}
	public void setCantOperacionesMTotalREP(String cantOperacionesMTotalREP) {
		this.cantOperacionesMTotalREP = cantOperacionesMTotalREP;
	}
	public String getLiquidDiferidaREP() {
		return liquidDiferidaREP;
	}
	public void setLiquidDiferidaREP(String liquidDiferidaREP) {
		this.liquidDiferidaREP = liquidDiferidaREP;
	}
	public String getCantOperacionesLDiferidaREP() {
		return cantOperacionesLDiferidaREP;
	}
	public void setCantOperacionesLDiferidaREP(String cantOperacionesLDiferidaREP) {
		this.cantOperacionesLDiferidaREP = cantOperacionesLDiferidaREP;
	}
	public String getEtapa1REP() {
		return etapa1REP;
	}
	public void setEtapa1REP(String etapa1rep) {
		etapa1REP = etapa1rep;
	}
	public String getCantOperacionesEtapa1REP() {
		return cantOperacionesEtapa1REP;
	}
	public void setCantOperacionesEtapa1REP(String cantOperacionesEtapa1REP) {
		this.cantOperacionesEtapa1REP = cantOperacionesEtapa1REP;
	}
	public String getEtapa2REP() {
		return etapa2REP;
	}
	public void setEtapa2REP(String etapa2rep) {
		etapa2REP = etapa2rep;
	}
	public String getCantOperacioneEtapa2REP() {
		return cantOperacioneEtapa2REP;
	}
	public void setCantOperacioneEtapa2REP(String cantOperacioneEtapa2REP) {
		this.cantOperacioneEtapa2REP = cantOperacioneEtapa2REP;
	}
	public String getMelorREP() {
		return melorREP;
	}
	public void setMelorREP(String melorREP) {
		this.melorREP = melorREP;
	}
	public String getCantOperacionesMelorREP() {
		return cantOperacionesMelorREP;
	}
	public void setCantOperacionesMelorREP(String cantOperacionesMelorREP) {
		this.cantOperacionesMelorREP = cantOperacionesMelorREP;
	}
	public String getMelidREP() {
		return melidREP;
	}
	public void setMelidREP(String melidREP) {
		this.melidREP = melidREP;
	}
	public String getCantOperacionesMelidREP() {
		return cantOperacionesMelidREP;
	}
	public void setCantOperacionesMelidREP(String cantOperacionesMelidREP) {
		this.cantOperacionesMelidREP = cantOperacionesMelidREP;
	}
	public String getIncumplimientosREP() {
		return incumplimientosREP;
	}
	public void setIncumplimientosREP(String incumplimientosREP) {
		this.incumplimientosREP = incumplimientosREP;
	}
	public String getCantOperacionesIncumplimientosREP() {
		return cantOperacionesIncumplimientosREP;
	}
	public void setCantOperacionesIncumplimientosREP(
			String cantOperacionesIncumplimientosREP) {
		this.cantOperacionesIncumplimientosREP = cantOperacionesIncumplimientosREP;
	}
	public String getMontoTotalCP() {
		return montoTotalCP;
	}
	public String getCantOperacionesMTotalCP() {
		return cantOperacionesMTotalCP;
	}
	public String getLiquidDiferidaCP() {
		return liquidDiferidaCP;
	}
	public String getCantOperacionesLDiferidaCP() {
		return cantOperacionesLDiferidaCP;
	}
	public String getEtapa1CP() {
		return etapa1CP;
	}
	public String getCantOperacionesEtapa1CP() {
		return cantOperacionesEtapa1CP;
	}
	public String getEtapa2CP() {
		return etapa2CP;
	}
	public String getCantOperacioneEtapa2CP() {
		return cantOperacioneEtapa2CP;
	}
	public String getMelorCP() {
		return melorCP;
	}
	public String getCantOperacionesMelorCP() {
		return cantOperacionesMelorCP;
	}
	public String getMelidCP() {
		return melidCP;
	}
	public String getCantOperacionesMelidCP() {
		return cantOperacionesMelidCP;
	}
	public String getIncumplimientosCP() {
		return incumplimientosCP;
	}
	public String getCantOperacionesIncumplimientosCP() {
		return cantOperacionesIncumplimientosCP;
	}
	public BigDecimal getAllMontoTotalSum() {
		return allMontoTotalSum;
	}
	public BigDecimal getAllLiquidDiferidaSum() {
		return allLiquidDiferidaSum;
	}
	public BigDecimal getAllEtapa1Sum() {
		return allEtapa1Sum;
	}
	public BigDecimal getAllEtapa2Sum() {
		return allEtapa2Sum;
	}
	public BigDecimal getAllMelorSum() {
		return allMelorSum;
	}
	public BigDecimal getAllMelidSum() {
		return allMelidSum;
	}
	public BigDecimal getAllIncumplimientosSum() {
		return allIncumplimientosSum;
	}
	public BigDecimal getAllCantOperacionesMTotalSum() {
		return allCantOperacionesMTotalSum;
	}
	public BigDecimal getAllCantOperacionesLDiferidaSum() {
		return allCantOperacionesLDiferidaSum;
	}
	public BigDecimal getAllCantOperacionesEtapa1Sum() {
		return allCantOperacionesEtapa1Sum;
	}
	public BigDecimal getAllCantOperacionesEtapa2Sum() {
		return allCantOperacionesEtapa2Sum;
	}
	public BigDecimal getAllCantOperacionesMelorSum() {
		return allCantOperacionesMelorSum;
	}
	public BigDecimal getAllCantOperacionesMelidSum() {
		return allCantOperacionesMelidSum;
	}
	public BigDecimal getAllCantOperacionesIncumplimientosSum() {
		return allCantOperacionesIncumplimientosSum;
	}
	public BigDecimal getDifferenceAmount() {
		return differenceAmount;
	}
	public BigDecimal getDifferenceOperation() {
		return differenceOperation;
	}
	public void setMontoTotalCP(String montoTotalCP) {
		this.montoTotalCP = montoTotalCP;
	}
	public void setCantOperacionesMTotalCP(String cantOperacionesMTotalCP) {
		this.cantOperacionesMTotalCP = cantOperacionesMTotalCP;
	}
	public void setLiquidDiferidaCP(String liquidDiferidaCP) {
		this.liquidDiferidaCP = liquidDiferidaCP;
	}
	public void setCantOperacionesLDiferidaCP(String cantOperacionesLDiferidaCP) {
		this.cantOperacionesLDiferidaCP = cantOperacionesLDiferidaCP;
	}
	public void setEtapa1CP(String etapa1cp) {
		etapa1CP = etapa1cp;
	}
	public void setCantOperacionesEtapa1CP(String cantOperacionesEtapa1CP) {
		this.cantOperacionesEtapa1CP = cantOperacionesEtapa1CP;
	}
	public void setEtapa2CP(String etapa2cp) {
		etapa2CP = etapa2cp;
	}
	public void setCantOperacioneEtapa2CP(String cantOperacioneEtapa2CP) {
		this.cantOperacioneEtapa2CP = cantOperacioneEtapa2CP;
	}
	public void setMelorCP(String melorCP) {
		this.melorCP = melorCP;
	}
	public void setCantOperacionesMelorCP(String cantOperacionesMelorCP) {
		this.cantOperacionesMelorCP = cantOperacionesMelorCP;
	}
	public void setMelidCP(String melidCP) {
		this.melidCP = melidCP;
	}
	public void setCantOperacionesMelidCP(String cantOperacionesMelidCP) {
		this.cantOperacionesMelidCP = cantOperacionesMelidCP;
	}
	public void setIncumplimientosCP(String incumplimientosCP) {
		this.incumplimientosCP = incumplimientosCP;
	}
	public void setCantOperacionesIncumplimientosCP(
			String cantOperacionesIncumplimientosCP) {
		this.cantOperacionesIncumplimientosCP = cantOperacionesIncumplimientosCP;
	}
	public void setAllMontoTotalSum(BigDecimal allMontoTotalSum) {
		this.allMontoTotalSum = allMontoTotalSum;
	}
	public void setAllLiquidDiferidaSum(BigDecimal allLiquidDiferidaSum) {
		this.allLiquidDiferidaSum = allLiquidDiferidaSum;
	}
	public void setAllEtapa1Sum(BigDecimal allEtapa1Sum) {
		this.allEtapa1Sum = allEtapa1Sum;
	}
	public void setAllEtapa2Sum(BigDecimal allEtapa2Sum) {
		this.allEtapa2Sum = allEtapa2Sum;
	}
	public void setAllMelorSum(BigDecimal allMelorSum) {
		this.allMelorSum = allMelorSum;
	}
	public void setAllMelidSum(BigDecimal allMelidSum) {
		this.allMelidSum = allMelidSum;
	}
	public void setAllIncumplimientosSum(BigDecimal allIncumplimientosSum) {
		this.allIncumplimientosSum = allIncumplimientosSum;
	}
	public void setAllCantOperacionesMTotalSum(
			BigDecimal allCantOperacionesMTotalSum) {
		this.allCantOperacionesMTotalSum = allCantOperacionesMTotalSum;
	}
	public void setAllCantOperacionesLDiferidaSum(
			BigDecimal allCantOperacionesLDiferidaSum) {
		this.allCantOperacionesLDiferidaSum = allCantOperacionesLDiferidaSum;
	}
	public void setAllCantOperacionesEtapa1Sum(
			BigDecimal allCantOperacionesEtapa1Sum) {
		this.allCantOperacionesEtapa1Sum = allCantOperacionesEtapa1Sum;
	}
	public void setAllCantOperacionesEtapa2Sum(
			BigDecimal allCantOperacionesEtapa2Sum) {
		this.allCantOperacionesEtapa2Sum = allCantOperacionesEtapa2Sum;
	}
	public void setAllCantOperacionesMelorSum(BigDecimal allCantOperacionesMelorSum) {
		this.allCantOperacionesMelorSum = allCantOperacionesMelorSum;
	}
	public void setAllCantOperacionesMelidSum(BigDecimal allCantOperacionesMelidSum) {
		this.allCantOperacionesMelidSum = allCantOperacionesMelidSum;
	}
	public void setAllCantOperacionesIncumplimientosSum(
			BigDecimal allCantOperacionesIncumplimientosSum) {
		this.allCantOperacionesIncumplimientosSum = allCantOperacionesIncumplimientosSum;
	}
	public void setDifferenceAmount(BigDecimal differenceAmount) {
		this.differenceAmount = differenceAmount;
	}
	public void setDifferenceOperation(BigDecimal differenceOperation) {
		this.differenceOperation = differenceOperation;
	}

}
