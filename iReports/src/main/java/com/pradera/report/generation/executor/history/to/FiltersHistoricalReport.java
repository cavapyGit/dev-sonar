package com.pradera.report.generation.executor.history.to;

import java.io.Serializable;
import java.util.Date;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.holderaccounts.HolderAccount;

public class FiltersHistoricalReport implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long idReportType; 
	private Long idTypeSettlement;
	private Long idParticipant; 
	private Long idParticipantAux; 
	private Holder holder;
	private HolderAccount holderAccount;
	private String securityClass;
	private String currency;
	private String idSecurityCodePk;
	private String negotiationMode;
	private String classOperation;
	private String transactionType;
	private Date initialDate;
	private Date finalDate;
	

	
	public FiltersHistoricalReport() {
		holder=new Holder();
		holderAccount=new HolderAccount();
		idReportType=0L;
		idParticipant=0L;
		idTypeSettlement=0L;
		initialDate=CommonsUtilities.currentDate();
		finalDate=CommonsUtilities.currentDate();
	}
	public void clearFilters() {
		holder=new Holder();
		holderAccount=new HolderAccount();
		idParticipant=0L;
		idTypeSettlement=0L;
		initialDate=CommonsUtilities.currentDate();
		finalDate=CommonsUtilities.currentDate();
	}
	public String getNegotiationMode() {
		return negotiationMode;
	}


	public void setNegotiationMode(String negotiationMode) {
		this.negotiationMode = negotiationMode;
	}


	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public Long getIdTypeSettlement() {
		return idTypeSettlement;
	}

	public void setIdTypeSettlement(Long idTypeSettlement) {
		this.idTypeSettlement = idTypeSettlement;
	}

	public String getClassOperation() {
		return classOperation;
	}

	public void setClassOperation(String classOperation) {
		this.classOperation = classOperation;
	}

	public Long getIdParticipantAux() {
		return idParticipantAux;
	}


	public void setIdParticipantAux(Long idParticipantAux) {
		this.idParticipantAux = idParticipantAux;
	}


	public HolderAccount getHolderAccount() {
		return holderAccount;
	}


	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}


	public Holder getHolder() {
		return holder;
	}

	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	public String getSecurityClass() {
		return securityClass;
	}

	public void setSecurityClass(String securityClass) {
		this.securityClass = securityClass;
	}


	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	public Long getIdParticipant() {
		return idParticipant;
	}


	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}


	public Long getIdReportType() {
		return idReportType;
	}


	public void setIdReportType(Long idReportType) {
		this.idReportType = idReportType;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
}
