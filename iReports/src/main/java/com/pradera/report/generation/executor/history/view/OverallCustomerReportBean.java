package com.pradera.report.generation.executor.history.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.commons.view.ViewDepositaryBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.report.generation.executor.history.facade.ReportGeneralHistoricFacade;
import com.pradera.report.generation.executor.history.to.FiltersHistoricalReport;
import com.pradera.report.generation.facade.ReportGenerationLocalServiceFacade;
//import com.pradera.report.generation.facade.ReportGenerationLocalServiceFacade;
//import com.pradera.report.util.view.PropertiesConstants;
import com.pradera.report.util.view.PropertiesConstants;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class OverallCustomerReportBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01-sep-2015
 */
@ViewDepositaryBean
@LoggerCreateBean
public class OverallCustomerReportBean extends GenericBaseBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
		
	/** The parameter service. */
	@EJB
	private ParameterServiceBean parameterService;
	
	/** The historic facade. */
	@EJB
	private ReportGeneralHistoricFacade historicFacade;
	
	/** The general parameters facade. */
	@Inject
	private GeneralParametersFacade generalParametersFacade;
	
	/** The report generation facade. */
	@EJB
	private ReportGenerationLocalServiceFacade reportGenerationFacade;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;	
	
	/** The Constant CONFIRM_DIALOG_SUCCESS. */
	private static final String CONFIRM_DIALOG_SUCCESS=":frmReportHistory:reportGenerationOk";
	
	/** The report user. */
	private ReportUser reportUser;
	
	/** The report formats. */
	private List<ParameterTable> reportFormats;
	
	/** The types report historic. */
	private List<ParameterTable> typesReportHistoric;
	
	/** The types class operations. */
	private List<ParameterTable> typesClassOperations;
	
	/** The filters. */
	private FiltersHistoricalReport filters;
	
	//atributos auxiliares para mostrar en la pantalla
    /** The list participant. */
	private List<Participant> listParticipant;
    
    /** The security class list. */
    private List<ParameterTable> securityClassList;
    
    /** The list currency. */
    private List<ParameterTable> listCurrency;
    
    /** The list transaction type. */
    private List<ParameterTable> listTransactionType;
    
    /** The list negotiation mode. */
    private List<ParameterTable> listNegotiationMode;
    
    /** The list settlement mode. */
    private List<ParameterTable> listSettlementMode;
	
	/** The lis account holders. */
	private GenericDataModel<HolderAccountHelperResultTO> lisAccountHolders;
	
	/** The selected account to. */
	private HolderAccountHelperResultTO selectedAccountTO;
	
	/** The holder account selected. */
	private HolderAccount holderAccountSelected;
	//para habilitacion y des
	/** The enable part view. */
	private boolean enablePartView=true;
	
	/** The enable part aux view. */
	private boolean enablePartAuxView=true;
	
	/** The enable cui holder view. */
	private boolean enableCuiHolderView=true;
	
	/** The enable class ope view. */
	private boolean enableClassOpeView=true;
	
	/** The enable currency view. */
	private boolean enableCurrencyView=true;
	
	/** The enable settlement mode. */
	private boolean enableSettlementMode=true;
	
	/** The enable security class view. */
	private boolean enableSecurityClassView=true;
	
	/** The enable security code view. */
	private boolean enableSecurityCodeView=true;
	
	/** The enable transaction type view. */
	private boolean enableTransactionTypeView=true;
	
	/** The enable negotiation mod view. */
	private boolean enableNegotiationModView=true;
	
	/** The enable date initial view. */
	private boolean enableDateInitialView=true;
	
	/** The enable date end view. */
	private boolean enableDateEndView=true;
	
	/** The msg id participant source. */
	private String msgIdParticipantSource=PropertiesUtilities.getMessage("lbl.participant");
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		loadDefaultValues();		
		loadReportFormat();
		loadListTypesReportHistoric();
		loadParticipants();
		loadCurrency();
		loadSecuryClass();
		laodClassOperation();
		loadTransactionTypes();
		loadListNegotiationModeTypes();
		loadListSettlementModeTypes();
		disableAllComponents();
	}
	
	/**
	 * Load default values.
	 */
	public void loadDefaultValues(){
		reportUser= new ReportUser();
		lisAccountHolders = null;
		selectedAccountTO=new HolderAccountHelperResultTO();
		filters=new FiltersHistoricalReport();
		securityClassList = new ArrayList<ParameterTable>();
		listCurrency=new ArrayList<ParameterTable>();
		listParticipant=new ArrayList<Participant>();
		holderAccountSelected=new HolderAccount();
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){
    		filters.setIdParticipant(userInfo.getUserAccountSession().getParticipantCode());
    	}
	}
	
	/**
	 * Laod class operation.
	 */
	public void laodClassOperation(){
		typesClassOperations=historicFacade.getListClassOperation();
	}
	
	/**
	 * Load transaction types.
	 */
	public void loadTransactionTypes(){
		listTransactionType=historicFacade.getListTransactionTypes();
	}
	
	/**
	 * Load report format.
	 */
	public void loadReportFormat(){
		ParameterTableTO parameterFilter = new ParameterTableTO();
		parameterFilter.setState(BooleanType.YES.getCode());
		parameterFilter.setMasterTableFk(MasterTableType.REPORT_FORMAT.getCode());
		parameterFilter.setIndicator4(BooleanType.YES.getCode());
		reportFormats = parameterService.getListParameterTableServiceBean(parameterFilter);
	}
	
	/**
	 * Load participants.
	 */
	public void loadParticipants(){
		Participant participantTO = new Participant();		
		listParticipant=new ArrayList<Participant>(0);
		try {
			listParticipant=historicFacade.getLisParticipantServiceBean(participantTO);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Load secury class.
	 */
	public void loadSecuryClass(){
		ParameterTableTO parameterFilter = new ParameterTableTO();
		parameterFilter = new ParameterTableTO();
		parameterFilter.setMasterTableFk( MasterTableType.SECURITIES_CLASS.getCode() );
		List<ParameterTable> lstSecClasses;
		try {
			lstSecClasses = generalParametersFacade.getListParameterTableServiceBean(parameterFilter);
			for(ParameterTable paramTab:lstSecClasses){
				if(ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState())){
					securityClassList.add(paramTab);
				}
			} 
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Load currency.
	 */
	private void loadCurrency(){
		try{
			listCurrency = new ArrayList<ParameterTable>();
			ParameterTableTO paramTable = new ParameterTableTO();
			paramTable.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			paramTable.setState(GeneralConstants.ONE_VALUE_INTEGER);
			paramTable.setOrderByText1(1);
			for(ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable))
				listCurrency.add(param);
		}
		catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Load list types report historic.
	 */
	public void loadListTypesReportHistoric(){		
		typesReportHistoric=historicFacade.getListTypesReportHistoric();
	}
	
	/**
	 * Load list negotiation mode types.
	 */
	public void loadListNegotiationModeTypes(){
		listNegotiationMode=historicFacade.getListNegotiationMode();
	}
	
	/**
	 * Load list settlement mode types.
	 */
	public void loadListSettlementModeTypes(){
		listSettlementMode=historicFacade.getListTypeSettlement();
	}
	//Enventos on change
	/**
	 * Participant change event.
	 */
	public void participantChangeEvent(){
	}
	
	/**
	 * Participant aux change event.
	 */
	public void participantAuxChangeEvent(){
		
	}
	
	/**
	 * Cui change event.
	 */
	public void cuiChangeEvent(){
		
	}
	
	/**
	 * Securty class change event.
	 */
	public void securtyClassChangeEvent(){
		
	}
	
	/**
	 * Select account change event.
	 */
	public void selectAccountChangeEvent(){
		
	}
	
	/**
	 * Clear all components.
	 */
	public void clearAllComponents(){
		filters.clearFilters();
		msgIdParticipantSource=PropertiesUtilities.getMessage("lbl.participant");
	}
	
	/**
	 * Select report change event.
	 */
	public void selectReportChangeEvent(){
		disableAllComponents();
		clearAllComponents();
		Integer reportSelected=null; 
		if(filters.getIdReportType()!=null)
			reportSelected = (int) (long) filters.getIdReportType();
		else
			return;
		switch (reportSelected) {
			case ReportGeneralHistoricFacade.CARTERA:
				enablePartView=true;
				enableCuiHolderView=true;
				enableCurrencyView=true;
				enableClassOpeView=true;
				enableSecurityClassView=true;
				enableSecurityCodeView=true;
				enableDateEndView=true;
				loadMessagesCartera();
			return;
			case ReportGeneralHistoricFacade.TRANFERENCIAS:
				enablePartView=true;
				enablePartAuxView=true;
				enableSecurityClassView=true;
				enableSecurityCodeView=true;
				enableTransactionTypeView=true;
				enableDateInitialView=true;
				enableDateEndView=true;
				loadMessagesParticipantSource();
			return;
			case ReportGeneralHistoricFacade.COLOCACIONES_VENTA_DIRECTA:
				enablePartView=true;
				enableCuiHolderView=true;
				enableSecurityClassView=true;
				enableSecurityCodeView=true;
				enableDateInitialView=true;
				enableDateEndView=true;
				loadMessagesPropertiesDefault();
			return;
			case ReportGeneralHistoricFacade.COLOCACIONES_TESORO_DIRECTO:
				enablePartView=true;
			    enableSecurityClassView=true;
			    enableSecurityCodeView=true;
			    enableCuiHolderView=true;
			    enableDateInitialView=true;
				enableDateEndView=true;
				loadMessagesPropertiesDefault();
			return;
			case ReportGeneralHistoricFacade.BLOQUEOS:
			return;
			case ReportGeneralHistoricFacade.DESBLOQUEOS:
			return;
			case ReportGeneralHistoricFacade.LIQUIDACIONES:
				enablePartView=true;
				enablePartAuxView=true;
				enableSecurityClassView=true;
				enableSecurityCodeView=true;
				enableNegotiationModView=true;
				enableCurrencyView=true;
				enableSettlementMode=true;
				enableDateInitialView=true;
				enableDateEndView=true;
				loadMessagesParticipantSource();
			return;
			default:
			return;
	      }
	}
	
	/**
	 * Load messages cartera.
	 */
	public void loadMessagesCartera(){
		filters.setInitialDate(null);
	}
	
	/**
	 * Load messages participant source.
	 */
	public void loadMessagesParticipantSource(){
		msgIdParticipantSource=PropertiesUtilities.getMessage("lbl.participant.source");
	}
	
	/**
	 * Load messages properties default.
	 */
	public void loadMessagesPropertiesDefault(){
		msgIdParticipantSource=PropertiesUtilities.getMessage("lbl.participant");
	}
	
	/**
	 * Execute report history.
	 */
	@LoggerAuditWeb
	public void executeReportHistory() {
		reportUser.setUserName(userInfo.getUserAccountSession().getUserName());
		reportUser.setShowNotification(BooleanType.YES.getCode());
		Integer reportSelected=null; 
		if(filters.getIdReportType()!=null)
			reportSelected = (int) (long) filters.getIdReportType();
		else
			return;
		Map<String,String> parameters = new HashMap<String, String>();
		Map<String,String> parameterDetails = new HashMap<String, String>();
		switch (reportSelected) {
			case ReportGeneralHistoricFacade.CARTERA:
				parameters = getParametersGeneralPortfolio();
				try {
					reportGenerationFacade.saveReportExecution(new Long(ReportGeneralHistoricFacade.CARTERA)
				    ,parameters, parameterDetails,reportUser);
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getGenericMessage(GeneralConstants.REPORT_GENERATED_SUCCESSFULLY));
					JSFUtilities.showSimpleValidationDialog();
				} catch (ServiceException e) {
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(PropertiesConstants.ALERT_MESSAGE),
							PropertiesUtilities.getMessage(PropertiesConstants.REPORT_SUCESS_FAIL));
					JSFUtilities.showComponent(CONFIRM_DIALOG_SUCCESS);
					e.printStackTrace();
				}
			break;
			case ReportGeneralHistoricFacade.TRANFERENCIAS:
				parameters = getParametersTransfersReportSaleDirect();
				try {
					reportGenerationFacade.saveReportExecution(new Long(ReportGeneralHistoricFacade.TRANFERENCIAS)
				    ,parameters, parameterDetails, reportUser);
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getGenericMessage(GeneralConstants.REPORT_GENERATED_SUCCESSFULLY));
					JSFUtilities.showSimpleValidationDialog();
				} catch (ServiceException e) {
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(PropertiesConstants.ALERT_MESSAGE),
							PropertiesUtilities.getMessage(PropertiesConstants.REPORT_SUCESS_FAIL));
					JSFUtilities.showComponent(CONFIRM_DIALOG_SUCCESS);
					e.printStackTrace();
				}
			break;
			case ReportGeneralHistoricFacade.COLOCACIONES_VENTA_DIRECTA:
				parameters = getParametersRegisterSaleDirect();
			try {
				reportGenerationFacade.saveReportExecution(new Long(ReportGeneralHistoricFacade.COLOCACIONES_VENTA_DIRECTA)
			    ,parameters, parameterDetails, reportUser);
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						PropertiesUtilities.getGenericMessage(GeneralConstants.REPORT_GENERATED_SUCCESSFULLY));
				JSFUtilities.showSimpleValidationDialog();
			} catch (ServiceException e) {
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(PropertiesConstants.ALERT_MESSAGE),
						PropertiesUtilities.getMessage(PropertiesConstants.REPORT_SUCESS_FAIL));
				JSFUtilities.showComponent(CONFIRM_DIALOG_SUCCESS);
				e.printStackTrace();
			}
			break;
			case ReportGeneralHistoricFacade.COLOCACIONES_TESORO_DIRECTO:
					parameters = getParametersTreasureDirect();
				try {
					reportGenerationFacade.saveReportExecution(new Long(ReportGeneralHistoricFacade.COLOCACIONES_TESORO_DIRECTO)
				    ,parameters, parameterDetails, reportUser);
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getGenericMessage(GeneralConstants.REPORT_GENERATED_SUCCESSFULLY));
					JSFUtilities.showSimpleValidationDialog();
				} catch (ServiceException e) {
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(PropertiesConstants.ALERT_MESSAGE),
							PropertiesUtilities.getMessage(PropertiesConstants.REPORT_SUCESS_FAIL));
					JSFUtilities.showComponent(CONFIRM_DIALOG_SUCCESS);
					e.printStackTrace();
				}
			break;
			case ReportGeneralHistoricFacade.BLOQUEOS:
				try {
					reportGenerationFacade.saveReportExecution(new Long(ReportGeneralHistoricFacade.BLOQUEOS)
				    ,parameters, parameterDetails,reportUser);
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getGenericMessage(GeneralConstants.REPORT_GENERATED_SUCCESSFULLY));
					JSFUtilities.showSimpleValidationDialog();
				} catch (ServiceException e) {
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(PropertiesConstants.ALERT_MESSAGE),
							PropertiesUtilities.getMessage(PropertiesConstants.REPORT_SUCESS_FAIL));
					JSFUtilities.showComponent(CONFIRM_DIALOG_SUCCESS);
					e.printStackTrace();
				}
			break;
			case ReportGeneralHistoricFacade.DESBLOQUEOS:
			break;
			case ReportGeneralHistoricFacade.LIQUIDACIONES:
				parameters = getParametersSettledOperationsHistory();
				try {
					reportGenerationFacade.saveReportExecution(new Long(ReportGeneralHistoricFacade.LIQUIDACIONES)
				    ,parameters, parameterDetails, reportUser);
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getGenericMessage(GeneralConstants.REPORT_GENERATED_SUCCESSFULLY));
					JSFUtilities.showSimpleValidationDialog();
				} catch (ServiceException e) {
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(PropertiesConstants.ALERT_MESSAGE),
							PropertiesUtilities.getMessage(PropertiesConstants.REPORT_SUCESS_FAIL));
					JSFUtilities.showComponent(CONFIRM_DIALOG_SUCCESS);
					e.printStackTrace();
				}
			break;
			default:
			break;
		}
	}
	
	//COLOCACIONES DE VENTA DIRECTA
	/**
	 * Gets the parameters register sale direct.
	 *
	 * @return the parameters register sale direct
	 */
	public Map<String,String> getParametersRegisterSaleDirect(){
		Map<String, String> parameters = new HashMap<String, String>();
		if(Validations.validateIsNotNullAndNotEmpty(filters.getIdParticipant()))
			parameters.put("participant", getMnemonicParticipant(filters.getIdParticipant()));
		if(Validations.validateIsNotNullAndNotEmpty(filters.getSecurityClass()))
			parameters.put("security_class", filters.getSecurityClass());
		if(Validations.validateIsNotNullAndNotEmpty(filters.getIdSecurityCodePk()))
			parameters.put("security_code", getSecurityCode(filters.getIdSecurityCodePk()));
		if(Validations.validateIsNotNullAndNotEmpty(filters.getHolder().getIdHolderPk()))
			parameters.put("cui_cuenta", ""+filters.getHolder().getIdHolderPk());
		parameters.put("date_initial", CommonsUtilities.convertDatetoString(filters.getInitialDate()));
		parameters.put("date_end", CommonsUtilities.convertDatetoString(filters.getFinalDate()));
		return parameters;
	}	
	//TRANFERENCIAS DE VENTA DIRECTA
	/**
	 * Gets the parameters transfers report sale direct.
	 *
	 * @return the parameters transfers report sale direct
	 */
	public Map<String,String> getParametersTransfersReportSaleDirect(){
		Map<String, String> parameters = new HashMap<String, String>();
		if(Validations.validateIsNotNullAndNotEmpty(filters.getIdParticipant()))
			parameters.put("participant_source", getMnemonicParticipant(filters.getIdParticipant()));
		if(Validations.validateIsNotNullAndNotEmpty(filters.getIdParticipantAux()))
			parameters.put("participant_target", getMnemonicParticipant(filters.getIdParticipantAux()));
		if(Validations.validateIsNotNullAndNotEmpty(filters.getSecurityClass()))
			parameters.put("security_class", filters.getSecurityClass());
		if(Validations.validateIsNotNullAndNotEmpty(filters.getIdSecurityCodePk()))
			parameters.put("security_code", getSecurityCode(filters.getIdSecurityCodePk()));
		if(Validations.validateIsNotNullAndNotEmpty(filters.getTransactionType()))
			parameters.put("transaction", ""+filters.getTransactionType());
		parameters.put("date_initial", CommonsUtilities.convertDatetoString(filters.getInitialDate()));
		parameters.put("date_end", CommonsUtilities.convertDatetoString(filters.getFinalDate()));
		return parameters;
	}	
	//TESORO DIRECTO
	/**
	 * Gets the parameters treasure direct.
	 *
	 * @return the parameters treasure direct
	 */
	public Map<String,String> getParametersTreasureDirect(){
		Map<String, String> parameters = new HashMap<String, String>();
		if(Validations.validateIsNotNullAndNotEmpty(filters.getIdParticipant()))
			parameters.put("participant", getMnemonicParticipant(filters.getIdParticipant()));
		if(Validations.validateIsNotNullAndNotEmpty(filters.getSecurityClass()))
			parameters.put("security_class", filters.getSecurityClass());
		if(Validations.validateIsNotNullAndNotEmpty(filters.getIdSecurityCodePk()))
			parameters.put("security_code", getSecurityCode(filters.getIdSecurityCodePk()));
		if(Validations.validateIsNotNullAndNotEmpty(filters.getHolder().getIdHolderPk()))
			parameters.put("holder_account_number", ""+filters.getHolder().getIdHolderPk());
		parameters.put("date_initial", CommonsUtilities.convertDatetoString(filters.getInitialDate()));
		parameters.put("date_end", CommonsUtilities.convertDatetoString(filters.getFinalDate()));
		return parameters;
	}
	//REPORTE DE OPERACIONES LIQUIDADAS
	/**
	 * Gets the parameters settled operations history.
	 *
	 * @return the parameters settled operations history
	 */
	public Map<String,String> getParametersSettledOperationsHistory(){

		Map<String, String> parameters = new HashMap<String, String>();
		if(Validations.validateIsNotNullAndNotEmpty(filters.getIdParticipant()))
			parameters.put("participant_source", getMnemonicParticipant(filters.getIdParticipant()));
		if(Validations.validateIsNotNullAndNotEmpty(filters.getIdParticipantAux()))
			parameters.put("participant_destination", getMnemonicParticipant(filters.getIdParticipantAux()));
		if(Validations.validateIsNotNullAndNotEmpty(filters.getSecurityClass()))
			parameters.put("security_class", filters.getSecurityClass());
		if(Validations.validateIsNotNullAndNotEmpty(filters.getIdSecurityCodePk()))
			parameters.put("security_code", getSecurityCode(filters.getIdSecurityCodePk()));
		if(Validations.validateIsNotNullAndNotEmpty(filters.getNegotiationMode()))
			parameters.put("modality", ""+filters.getNegotiationMode());
		if(Validations.validateIsNotNullAndNotEmpty(filters.getCurrency()))
			parameters.put("currency", ""+filters.getCurrency());
		if(Validations.validateIsNotNullAndNotEmpty(filters.getIdTypeSettlement()))
			parameters.put("schedule_type", ""+filters.getIdTypeSettlement());
		parameters.put("date_initial", CommonsUtilities.convertDatetoString(filters.getInitialDate()));
		parameters.put("date_end", CommonsUtilities.convertDatetoString(filters.getFinalDate()));
		return parameters;
	}
	
	/**
	 * Gets the parameters general portfolio.
	 *
	 * @return the parameters general portfolio
	 */
	public Map<String,String> getParametersGeneralPortfolio(){
		Map<String, String> parameters = new HashMap<String, String>();
		if(Validations.validateIsNotNullAndNotEmpty(filters.getIdParticipant()))
			parameters.put("participant", getMnemonicParticipant(filters.getIdParticipant()));
		
		if(Validations.validateIsNotNullAndNotEmpty(filters.getCurrency()))
			parameters.put("currency", filters.getCurrency());
		
		if(Validations.validateIsNotNullAndNotEmpty(filters.getIdSecurityCodePk()))
			parameters.put("security_code", getSecurityCode(filters.getIdSecurityCodePk()));
		
		if(Validations.validateIsNotNullAndNotEmpty(filters.getSecurityClass()))
			parameters.put("security_class", filters.getSecurityClass());
		
		if(Validations.validateIsNotNullAndNotEmpty(filters.getClassOperation()))
			parameters.put("class_operation", filters.getClassOperation());
		
		if(Validations.validateIsNotNullAndNotEmpty(filters.getHolder().getIdHolderPk()))
			parameters.put("cui_holder", ""+filters.getHolder().getIdHolderPk());
		
		parameters.put("date_end", CommonsUtilities.convertDatetoString(filters.getFinalDate()));
		return parameters;
	}
	
	/**
	 * Gets the mnemonic participant.
	 *
	 * @param idParticipant the id participant
	 * @return the mnemonic participant
	 */
	public String getMnemonicParticipant(Long idParticipant){
	    for (Participant participant : listParticipant) 
			if(idParticipant.equals(participant.getIdParticipantPk()))
				return participant.getMnemonic();
	    return null;
	}
	
	/**
	 * Gets the security code.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @return the security code
	 */
	public String getSecurityCode(String idSecurityCodePk){
		if(idSecurityCodePk!=null)
			return idSecurityCodePk.substring(4,idSecurityCodePk.length());
		return null;
	}
	
	/**
	 * Disable all components.
	 */
	public void disableAllComponents(){
		enablePartView=false;
		enablePartAuxView=false;
		enableCuiHolderView=false;
		enableClassOpeView=false;
		enableCurrencyView=false;
		enableSettlementMode=false;
		enableSecurityClassView=false;
		enableSecurityCodeView=false;
		enableTransactionTypeView=false;
		enableNegotiationModView=false;
		enableDateInitialView=false;
		enableDateEndView=false;
	}
	
	/**
	 * Gets the types class operations.
	 *
	 * @return the types class operations
	 */
	public List<ParameterTable> getTypesClassOperations() {
		return typesClassOperations;
	}
	
	/**
	 * Sets the types class operations.
	 *
	 * @param typesClassOperations the new types class operations
	 */
	public void setTypesClassOperations(List<ParameterTable> typesClassOperations) {
		this.typesClassOperations = typesClassOperations;
	}
	
	/**
	 * Gets the selected account to.
	 *
	 * @return the selected account to
	 */
	public HolderAccountHelperResultTO getSelectedAccountTO() {
		return selectedAccountTO;
	}
	
	/**
	 * Sets the selected account to.
	 *
	 * @param selectedAccountTO the new selected account to
	 */
	public void setSelectedAccountTO(HolderAccountHelperResultTO selectedAccountTO) {
		this.selectedAccountTO = selectedAccountTO;
	}
	
	/**
	 * Gets the lis account holders.
	 *
	 * @return the lis account holders
	 */
	public GenericDataModel<HolderAccountHelperResultTO> getLisAccountHolders() {
		return lisAccountHolders;
	}
	
	/**
	 * Sets the lis account holders.
	 *
	 * @param lisAccountHolders the new lis account holders
	 */
	public void setLisAccountHolders(
			GenericDataModel<HolderAccountHelperResultTO> lisAccountHolders) {
		this.lisAccountHolders = lisAccountHolders;
	}
		
	/**
	 * Gets the list participant.
	 *
	 * @return the list participant
	 */
	public List<Participant> getListParticipant() {
		return listParticipant;
	}
	
	/**
	 * Sets the list participant.
	 *
	 * @param listParticipant the new list participant
	 */
	public void setListParticipant(List<Participant> listParticipant) {
		this.listParticipant = listParticipant;
	}
	
	/**
	 * Gets the security class list.
	 *
	 * @return the security class list
	 */
	public List<ParameterTable> getSecurityClassList() {
		return securityClassList;
	}
	
	/**
	 * Sets the security class list.
	 *
	 * @param securityClassList the new security class list
	 */
	public void setSecurityClassList(List<ParameterTable> securityClassList) {
		this.securityClassList = securityClassList;
	}
	
	/**
	 * Gets the filters.
	 *
	 * @return the filters
	 */
	public FiltersHistoricalReport getFilters() {
		return filters;
	}
	
	/**
	 * Sets the filters.
	 *
	 * @param filters the new filters
	 */
	public void setFilters(FiltersHistoricalReport filters) {
		this.filters = filters;
	}
	
	/**
	 * Gets the types report historic.
	 *
	 * @return the types report historic
	 */
	public List<ParameterTable> getTypesReportHistoric() {
		return typesReportHistoric;
	}
	
	/**
	 * Sets the types report historic.
	 *
	 * @param typesReportHistoric the new types report historic
	 */
	public void setTypesReportHistoric(List<ParameterTable> typesReportHistoric) {
		this.typesReportHistoric = typesReportHistoric;
	}
	
	/**
	 * Gets the report formats.
	 *
	 * @return the report formats
	 */
	public List<ParameterTable> getReportFormats() {
		return reportFormats;
	}
	
	/**
	 * Sets the report formats.
	 *
	 * @param reportFormats the new report formats
	 */
	public void setReportFormats(List<ParameterTable> reportFormats) {
		this.reportFormats = reportFormats;
	}
	
	/**
	 * Gets the report user.
	 *
	 * @return the report user
	 */
	public ReportUser getReportUser() {
		return reportUser;
	}
	
	/**
	 * Sets the report user.
	 *
	 * @param reportUser the new report user
	 */
	public void setReportUser(ReportUser reportUser) {
		this.reportUser = reportUser;
	}
	
	/**
	 * Gets the list currency.
	 *
	 * @return the list currency
	 */
	public List<ParameterTable> getListCurrency() {
		return listCurrency;
	}
	
	/**
	 * Sets the list currency.
	 *
	 * @param listCurrency the new list currency
	 */
	public void setListCurrency(List<ParameterTable> listCurrency) {
		this.listCurrency = listCurrency;
	}
	
	/**
	 * Gets the list transaction type.
	 *
	 * @return the list transaction type
	 */
	public List<ParameterTable> getListTransactionType() {
		return listTransactionType;
	}
	
	/**
	 * Sets the list transaction type.
	 *
	 * @param listTransactionType the new list transaction type
	 */
	public void setListTransactionType(List<ParameterTable> listTransactionType) {
		this.listTransactionType = listTransactionType;
	}
	
	/**
	 * Gets the list negotiation mode.
	 *
	 * @return the list negotiation mode
	 */
	public List<ParameterTable> getListNegotiationMode() {
		return listNegotiationMode;
	}
	
	/**
	 * Sets the list negotiation mode.
	 *
	 * @param listNegotiationMode the new list negotiation mode
	 */
	public void setListNegotiationMode(List<ParameterTable> listNegotiationMode) {
		this.listNegotiationMode = listNegotiationMode;
	}
	
	/**
	 * Gets the list settlement mode.
	 *
	 * @return the list settlement mode
	 */
	public List<ParameterTable> getListSettlementMode() {
		return listSettlementMode;
	}
	
	/**
	 * Sets the list settlement mode.
	 *
	 * @param listSettlementMode the new list settlement mode
	 */
	public void setListSettlementMode(List<ParameterTable> listSettlementMode) {
		this.listSettlementMode = listSettlementMode;
	}
	
	/**
	 * Gets the holder account selected.
	 *
	 * @return the holder account selected
	 */
	public HolderAccount getHolderAccountSelected() {
		return holderAccountSelected;
	}
	
	/**
	 * Sets the holder account selected.
	 *
	 * @param holderAccountSelected the new holder account selected
	 */
	public void setHolderAccountSelected(HolderAccount holderAccountSelected) {
		this.holderAccountSelected = holderAccountSelected;
	}
	//metodos p
	/**
	 * Checks if is enable part view.
	 *
	 * @return true, if is enable part view
	 */
	public boolean isEnablePartView() {
		return enablePartView;
	}
	
	/**
	 * Checks if is enable part aux view.
	 *
	 * @return true, if is enable part aux view
	 */
	public boolean isEnablePartAuxView() {
		return enablePartAuxView;
	}
	
	/**
	 * Checks if is enable cui holder view.
	 *
	 * @return true, if is enable cui holder view
	 */
	public boolean isEnableCuiHolderView() {
		return enableCuiHolderView;
	}
	
	/**
	 * Checks if is enable class ope view.
	 *
	 * @return true, if is enable class ope view
	 */
	public boolean isEnableClassOpeView() {
		return enableClassOpeView;
	}
	
	/**
	 * Checks if is enable currency view.
	 *
	 * @return true, if is enable currency view
	 */
	public boolean isEnableCurrencyView() {
		return enableCurrencyView;
	}
	
	/**
	 * Checks if is enable settlement mode.
	 *
	 * @return true, if is enable settlement mode
	 */
	public boolean isEnableSettlementMode() {
		return enableSettlementMode;
	}
	
	/**
	 * Checks if is enable security class view.
	 *
	 * @return true, if is enable security class view
	 */
	public boolean isEnableSecurityClassView() {
		return enableSecurityClassView;
	}
	
	/**
	 * Checks if is enable security code view.
	 *
	 * @return true, if is enable security code view
	 */
	public boolean isEnableSecurityCodeView() {
		return enableSecurityCodeView;
	}
	
	/**
	 * Checks if is enable transaction type view.
	 *
	 * @return true, if is enable transaction type view
	 */
	public boolean isEnableTransactionTypeView() {
		return enableTransactionTypeView;
	}
	
	/**
	 * Checks if is enable negotiation mod view.
	 *
	 * @return true, if is enable negotiation mod view
	 */
	public boolean isEnableNegotiationModView() {
		return enableNegotiationModView;
	}
	
	/**
	 * Checks if is enable date initial view.
	 *
	 * @return true, if is enable date initial view
	 */
	public boolean isEnableDateInitialView() {
		return enableDateInitialView;
	}
	
	/**
	 * Checks if is enable date end view.
	 *
	 * @return true, if is enable date end view
	 */
	public boolean isEnableDateEndView() {
		return enableDateEndView;
	}
	//set and getter en mensages desde el properties.
	/**
	 * Gets the msg id participant source.
	 *
	 * @return the msg id participant source
	 */
	public String getMsgIdParticipantSource() {
		return msgIdParticipantSource;
	}
	
	/**
	 * Sets the msg id participant source.
	 *
	 * @param msgIdParticipantSource the new msg id participant source
	 */
	public void setMsgIdParticipantSource(String msgIdParticipantSource) {
		this.msgIdParticipantSource = msgIdParticipantSource;
	}
}
