package com.pradera.report.generation.executor.securities;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "ListIssuerRespOfficersReport")
public class ListIssuerRespOfficersReport extends GenericReport{
private static final long serialVersionUID = 1L;

@Inject  PraderaLogger log;

@EJB
private ParameterServiceBean parameterService;

public ListIssuerRespOfficersReport() {
}

@Override
public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public void addParametersQueryReport() {
	List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
	if (listaLogger == null) {
		listaLogger = new ArrayList<ReportLoggerDetail>();
	}

}

@Override
public Map<String, Object> getCustomJasperParameters() {
	Map<String,Object> parametersRequired = new HashMap<>();
	ParameterTableTO  filter = new ParameterTableTO();
	Map<Integer,String> economic_activity = new HashMap<Integer, String>();
	Map<Integer,String> state_issuer = new HashMap<Integer, String>();
	Map<Integer,String> document_type = new HashMap<Integer, String>();
	try {
		filter.setMasterTableFk(MasterTableType.ECONOMIC_ACTIVITY.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
			economic_activity.put(param.getParameterTablePk(), param.getParameterName());
		}
		filter.setMasterTableFk(MasterTableType.ISSUER_STATE.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
			state_issuer.put(param.getParameterTablePk(), param.getParameterName());
		}
		filter.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
			document_type.put(param.getParameterTablePk(), param.getIndicator1());
		}
	}catch(Exception ex){
		log.error(ex.getMessage());
		ex.printStackTrace();
		throw new RuntimeException();
	}
	parametersRequired.put("p_economic_activity", economic_activity);
	parametersRequired.put("p_state_issuer", state_issuer);
	parametersRequired.put("p_document_type", document_type);
	parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
	// TODO Auto-generated method stub
	return parametersRequired;
	}
}
