package com.pradera.report.generation.executor.issuances;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.collectioneconomic.service.ParserUtilServiceReport;
import com.pradera.report.generation.executor.issuances.to.PaymentChronogramTO;
import com.pradera.report.generation.executor.securities.service.PaymentChronogramBySecurity;
import com.pradera.report.generation.service.ReportUtilServiceBean;
import com.pradera.report.util.view.PropertiesConstants;

@ReportProcess(name = "CouponsCharacteristicsReport")
public class CouponsCharacteristicsReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private PaymentChronogramBySecurity securitiesService;
	@Inject
	private PraderaLogger log;
	@EJB
	private ParameterServiceBean parameterService;
	@EJB
	private ReportUtilServiceBean reportUtilServiceBean;
	
	@Inject
	private ParserUtilServiceReport parserUtilServiceReport;
	
	/*
	private static final String[] strDays = new String[]{
		"Domingo",
		"Lunes",
		"Martes",
		"Miercoles",
		"Jueves",
		"Viernes",
		"Sabado"};
	*/
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		PaymentChronogramTO couponsTO = new PaymentChronogramTO();
		boolean automatic = false;
		Long idReportLoggerDetailDate = null;
		boolean logic = false;
		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("date")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					couponsTO.setDate(CommonsUtilities.convertStringtoDate(listaLogger.get(i).getFilterValue()));
					couponsTO.setRegistryDt(CommonsUtilities.convertStringtoDate(listaLogger.get(i).getFilterValue()));
					idReportLoggerDetailDate  = listaLogger.get(i).getIdLoggerDetailPk();
				}
			}
			if(listaLogger.get(i).getFilterName().equals("automatic")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					if(listaLogger.get(i).getFilterValue().equals("true"))
						automatic = true;
				}
			}
			if(listaLogger.get(i).getFilterName().equals("logic")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					if(listaLogger.get(i).getFilterValue().equals("true"))
						logic = true;
					else
						logic = false;
				}
			}
		}
		
		/**Proceso enviado por el batchero, se ejecuta como estaba orgiginalmente*/
		if(automatic){
			/**Proceso normal y automatico*/	
			couponsTO.setDateForName(couponsTO.getDate());
			List<PaymentChronogramTO> lstCoupons = securitiesService.getCouponsCharacteristics(couponsTO);
			buildReportCouponsCharacteristics(lstCoupons,couponsTO.getDateForName());
			return null;
		}
		if(!logic){
			/**Proceso normal y automatico*/	
			couponsTO.setDateForName(couponsTO.getDate());
			List<PaymentChronogramTO> lstCoupons = parserUtilServiceReport.getCouponsCharacteristicsFromFile(couponsTO,false,10L);
			buildReportCouponsCharacteristics(lstCoupons,couponsTO.getDateForName());
			return null;
		}
	
		List<PaymentChronogramTO> lstCoupons = new ArrayList<>();
		
		Calendar now = Calendar.getInstance();
		now.setTime(couponsTO.getRegistryDt());
		//System.out.println("Hoy es : " + strDays[now.get(Calendar.DAY_OF_WEEK) - 1]);
		if(now.get(Calendar.DAY_OF_WEEK)==3||now.get(Calendar.DAY_OF_WEEK)==4||now.get(Calendar.DAY_OF_WEEK)==2){
			if(now.get(Calendar.DAY_OF_WEEK)==3){
				/**Dia martes que debe obetener del dia lunes*/
				List<PaymentChronogramTO> lstMonday = new ArrayList<>();
				Date monday  = CommonsUtilities.addDate(couponsTO.getRegistryDt(),-1);
				couponsTO.setDate(monday);
				couponsTO.setDateForName(monday);
				lstMonday = parserUtilServiceReport.getCouponsCharacteristicsFromFile(couponsTO,false,10L);
				
				List<PaymentChronogramTO>  newLstMonday = new ArrayList<>();
				for (PaymentChronogramTO paymentChronogramTO : lstMonday) 
					if(!paymentChronogramTO.getSecurityClass().equals(SecurityClassType.BTS.getCode()))
						newLstMonday.add(paymentChronogramTO);
				
				/**Dia sabado*/
				List<PaymentChronogramTO> lstSaturday = new ArrayList<>();
				Date saturday  = CommonsUtilities.addDate(couponsTO.getRegistryDt(),-3);
				couponsTO.setDate(saturday);
				lstSaturday = parserUtilServiceReport.getCouponsCharacteristicsFromFile(couponsTO,false,10L);
				lstSaturday.addAll(newLstMonday);
				/**Dias sabado y lunes en el orden respectivo*/
				lstCoupons.addAll(lstSaturday);
			}
			if(now.get(Calendar.DAY_OF_WEEK)==4){
				/**Dia martes que se ejecuta el miercoles*/
				List<PaymentChronogramTO> lstTuesday = new ArrayList<>();
				Date tuesday  = CommonsUtilities.addDate(couponsTO.getRegistryDt(), -1);
				couponsTO.setDate(tuesday);
				couponsTO.setDateForName(tuesday);
				lstTuesday = parserUtilServiceReport.getCouponsCharacteristicsFromFile(couponsTO,false,10L);
				
				List<PaymentChronogramTO> lstMonday = new ArrayList<>();
				Date monday  = CommonsUtilities.addDate(couponsTO.getRegistryDt(), -2);
				couponsTO.setDate(monday);
				lstMonday = parserUtilServiceReport.getCouponsCharacteristicsFromFile(couponsTO,true,10L);
				
				List<PaymentChronogramTO> lstFriday = new ArrayList<>();
				Date friday  = CommonsUtilities.addDate(couponsTO.getRegistryDt(), -5);
				couponsTO.setDate(friday);	
				couponsTO.setDateForName(tuesday);
				lstFriday = parserUtilServiceReport.getCouponsCharacteristicsFromFile(couponsTO,true,10L);	
				
				lstFriday.addAll(lstMonday);
				lstTuesday.addAll(lstFriday);
				lstCoupons.addAll(lstTuesday);
			}
			if(now.get(Calendar.DAY_OF_WEEK)==2){
				/**Dia viernes que se ejecuta el lunes*/
				List<PaymentChronogramTO> lstFriday = new ArrayList<>();
				Date friday  = CommonsUtilities.addDate(couponsTO.getRegistryDt(),-3);
				couponsTO.setDate(friday);
				couponsTO.setDateForName(friday);
				lstFriday = parserUtilServiceReport.getCouponsCharacteristicsFromFile(couponsTO,false,10L);
				/**No tomar en cuenta los BTS*/
				List<PaymentChronogramTO>  newlstFriday = new ArrayList<>();
				for (PaymentChronogramTO paymentChronogramTO : lstFriday) 
					if(!paymentChronogramTO.getSecurityClass().equals(SecurityClassType.BTS.getCode()))
						newlstFriday.add(paymentChronogramTO);
				lstCoupons.addAll(newlstFriday);
			}
		}else{
			couponsTO.setDate(couponsTO.getRegistryDt());
			couponsTO.setDateForName(couponsTO.getRegistryDt());
			lstCoupons= parserUtilServiceReport.getCouponsCharacteristicsFromFile(couponsTO,false,10L);
			couponsTO.setDateForName(couponsTO.getRegistryDt());
		}
		//Solucion, actualizar la fecha cuando se envie con logica
		if(automatic==false&&logic==true){
			buildReportCouponsCharacteristics(lstCoupons,couponsTO.getDateForName());
			ReportLoggerDetail detail = parameterService.find(ReportLoggerDetail.class, idReportLoggerDetailDate);
			detail.setFilterValue(CommonsUtilities.convertDateToString(couponsTO.getDateForName(), "dd/MM/yyyy"));
			try {
				parserUtilServiceReport.updateWithOutFlush(detail);
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	//joining the same coupon
	public List<PaymentChronogramTO> joinCoupons(List<PaymentChronogramTO> lstCoupons){
		List<PaymentChronogramTO> lstSameCoupons= new ArrayList<PaymentChronogramTO>();
		Date lastDate= null;
		String lastSecurity=null;
		PaymentChronogramTO ccTO= null;
		int contador=0;
		int pos=0;
		for (int i = 0; i < lstCoupons.size(); i++) {
			ccTO= new PaymentChronogramTO();
			PaymentChronogramTO coupons= lstCoupons.get(i);
			ccTO.setCustodian(coupons.getCustodian());
			ccTO.setIdSecurityOnly(coupons.getIdSecurityOnly());
			ccTO.setRegistryDt(coupons.getRegistryDt());
			ccTO.setSecurityClass(coupons.getSecurityClass());
			if(lastDate==null && lastSecurity==null){//first time
				lastSecurity = coupons.getIdSecurityOnly();
				if(coupons.getCouponType().equals("INT")){
					lastDate = coupons.getExpirationDtInt();
					ccTO.setExpirationDtInt(coupons.getExpirationDtInt());
					ccTO.setCouponNumberInt(coupons.getCouponNumberInt());
					ccTO.setInterestFactor(coupons.getInterestFactor());
					ccTO.setAmortizationFactor(coupons.getAmortizationFactor());
				}else{
					lastDate = coupons.getExpirationDtAmort();
					ccTO.setExpirationDtAmort(coupons.getExpirationDtAmort());
					ccTO.setCouponNumberAmort(coupons.getCouponNumberAmort());
					ccTO.setAmortizationFactor(coupons.getAmortizationFactor());
					ccTO.setInterestFactor(coupons.getInterestFactor());
				}
				ccTO.setTotalCoupon(ccTO.getTotalCoupon());
				lstSameCoupons.add(ccTO);
			}else{
				if(lastSecurity.equals(coupons.getIdSecurityOnly()) && 
						(lastDate.equals(coupons.getExpirationDtInt()) || lastDate.equals(coupons.getExpirationDtAmort()))){//replace
					for (int j = 0; j < lstSameCoupons.size(); j++) {
						if(lstSameCoupons.get(j).getIdSecurityOnly().equals(coupons.getIdSecurityOnly())) {
							pos=j;
						}
					}
					lstSameCoupons.get(pos).setExpirationDtInt(coupons.getExpirationDtInt());
					lstSameCoupons.get(pos).setCouponNumberInt(coupons.getCouponNumberInt());
					if(lstSameCoupons.get(pos).getInterestFactor().intValue()==0){
						lstSameCoupons.get(pos).setInterestFactor(coupons.getInterestFactor());
					}
					lstSameCoupons.get(pos).setExpirationDtAmort(null);//interest is mandatory
					lstSameCoupons.get(pos).setCouponNumberAmort(null);//interest is mandatory
					if(coupons.getExpirationDtInt() == null){
						lstSameCoupons.get(pos).setExpirationDtInt(coupons.getExpirationDtAmort());
					}
					if(coupons.getCouponNumberInt() == null){
						lstSameCoupons.get(pos).setCouponNumberInt(coupons.getCouponNumberAmort());
					}
					if(lstSameCoupons.get(pos).getAmortizationFactor().intValue()==0){
						lstSameCoupons.get(pos).setAmortizationFactor(coupons.getAmortizationFactor());
					}	
					lstSameCoupons.get(pos).setTotalCoupon(lstSameCoupons.get(pos).getAmortizationFactor().add(lstSameCoupons.get(pos).getInterestFactor()));
//					contador++;
				}else{//add
					lastSecurity = coupons.getIdSecurityOnly();
					if(coupons.getCouponType().equals("INT")){
						lastDate = coupons.getExpirationDtInt();
						ccTO.setExpirationDtInt(coupons.getExpirationDtInt());
						ccTO.setCouponNumberInt(coupons.getCouponNumberInt());
						ccTO.setInterestFactor(coupons.getInterestFactor());
						ccTO.setAmortizationFactor(coupons.getAmortizationFactor());
					}else{
						lastDate = coupons.getExpirationDtAmort();
						ccTO.setExpirationDtAmort(coupons.getExpirationDtAmort());
						ccTO.setCouponNumberAmort(coupons.getCouponNumberAmort());
						ccTO.setAmortizationFactor(coupons.getAmortizationFactor());
						ccTO.setInterestFactor(coupons.getInterestFactor());
					}
					ccTO.setTotalCoupon(ccTO.getTotalCoupon());
					lstSameCoupons.add(ccTO);
				}
			}
		}
		return lstSameCoupons;
	}
	/**Se construye el reprote
	 * */
	private void buildReportCouponsCharacteristics(List<PaymentChronogramTO> lstCoupons,Date dateForName){
		StringBuilder sbTxtFormat = new StringBuilder();
		String comilla= "\"";

		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> secClassMnemonic = new HashMap<Integer, String>();
		try {
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secClassMnemonic.put(param.getParameterTablePk(), param.getText1());
			}
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		for (PaymentChronogramTO cc: lstCoupons) {
			sbTxtFormat.append(comilla+cc.getCustodian()+comilla);
			sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
			if(cc.getDateInformation()==null)
				cc.setDateInformation(dateForName);;
			String dateInformation = CommonsUtilities.convertDateToString(cc.getDateInformation(),"yyyy-MM-dd");
			sbTxtFormat.append(comilla+dateInformation+comilla);
			sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
			sbTxtFormat.append(comilla+secClassMnemonic.get(cc.getSecurityClass())+comilla);
			sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
			sbTxtFormat.append(comilla+cc.getIdSecurityOnly()+comilla);
			sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
			sbTxtFormat.append(comilla+cc.getCouponNumberInt()+comilla);
			sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
			sbTxtFormat.append(comilla+cc.getExpirationDate()+comilla);
			sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
			sbTxtFormat.append(comilla+cc.getAmortizationFactor()+comilla);
			sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
			sbTxtFormat.append(comilla+cc.getInterestFactor()+comilla);
			sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
			sbTxtFormat.append(comilla+cc.getTotalCoupon()+comilla);
			sbTxtFormat.append(GeneralConstants.STR_END_LINE_TXT);
		}
		String physicalName = null;
		if(lstCoupons.isEmpty()){
			//physicalName =getReportLogger().getPhysicalName();
			SimpleDateFormat sdf = new SimpleDateFormat("YYMMdd");  
			String fechaFormat = sdf.format(dateForName);  
			physicalName = "M"+fechaFormat+"E.EDB";
			sbTxtFormat.append(PropertiesConstants.NO_FOUND_DATA_BLANK);
		}else{
			SimpleDateFormat sdf = new SimpleDateFormat("YYMMdd");  
			String fechaFormat = sdf.format(dateForName);  
			physicalName = "M"+fechaFormat+"E.EDB";
		}
		reportUtilServiceBean.saveCustomReportFile(physicalName, ReportFormatType.EDB.getCode(), 
														sbTxtFormat.toString().getBytes(), getReportLogger());
	}
}
