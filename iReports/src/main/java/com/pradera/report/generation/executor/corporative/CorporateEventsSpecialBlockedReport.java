package com.pradera.report.generation.executor.corporative;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.corporatives.stockcalculation.type.StockClassType;
import com.pradera.model.corporatives.stockcalculation.type.StockType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.corporative.service.CorporativeReportServiceBean;
import com.pradera.report.generation.executor.corporative.to.CorporativeProcesInterestTO;
import com.pradera.report.generation.executor.corporative.to.StockCalculationProcessTO;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

@ReportProcess(name="CorporateEventsSpecialBlockedReport")
public class CorporateEventsSpecialBlockedReport extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6505767022470569721L;
	
	@EJB
	ParameterServiceBean parameterService;
	
	@EJB
	CorporativeReportServiceBean corporativeReportService;
	
	@EJB
	ParticipantServiceBean participantService;
	
	private CorporativeProcesInterestTO corporativeTo;
	
	private static Integer PARTICIPANTE_PK = new Integer(0);
	
	private static Integer PARTICIPANTE_MNEMONIC = new Integer(1);
	
	private static Integer PARTICIPANTE_DESC = new Integer(2);
	
	private static Integer ACCOUNT_PK = new Integer(3);
	
	private static Integer ACCOUNT_NUMBER = new Integer(4);
	
	private static Integer HOLDERS_NAME = new Integer(5);
	
	private static Integer STOCK_PAWN = new Integer(6);
	
	private static Integer STOCK_BAN = new Integer(7);
	
	private static Integer STOCK_OTHER = new Integer(8);
	
	private static Integer STOCK_TOTAL = new Integer(9);
	
	private static Integer PROCESS_PAWN = new Integer(10);
	
	private static Integer PROCESS_BAN = new Integer(11);
	
	private static Integer PROCESS_OTHER = new Integer(12);
	
	private static Integer PROCESS_TOTAL = new Integer(13);
	
	private static Integer BLOCK_DOC_NUMBER = new Integer(0);
	
	private static Integer BLOCK_CITE_NUMBER = new Integer(1);
	
	private static Integer BLOCK_TYPE = new Integer(2);
	
	private static Integer BLOCK_BALANCE = new Integer(3);
	
	private static Integer BLOCK_PROCESS = new Integer(4);
	
	private static Integer BLOCK_ENTITY = new Integer(5);
	
	private static Integer BLOCK_ORIGIN_DESTINY = new Integer(6);
	
	private static Integer BLOCK_RESULT_PK = new Integer(7);

	public CorporateEventsSpecialBlockedReport (){
		// TODO Auto-generated constructor stub
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		corporativeTo = getCorporateParameters(reportLogger);
		List<Object[]> processInformation = corporativeReportService.getCorporativeSpecialHeader(corporativeTo);
		try{
			XMLOutputFactory xof = XMLOutputFactory.newInstance();
			XMLStreamWriter xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(baos));
			xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");//<XML>
			xmlsw.writeStartElement(ReportConstant.REPORT);//<report>
			createHeaderReport( xmlsw, reportLogger );//header
			if(!processInformation.isEmpty()){
				Map<Integer,String> processStateMap = new HashMap<>();
				Map<Integer,String> currencyMap = new HashMap<>();
				Map<Integer,String> currencyMapAux = new HashMap<>();
				for (ParameterTable param : parameterService.getListParameterTableServiceBean(MasterTableType.CURRENCY.getCode())){
					currencyMap.put(param.getParameterTablePk(), param.getParameterName());
					currencyMapAux.put(param.getParameterTablePk(), param.getText1());
				}
				for(ParameterTable param : parameterService.getListParameterTableServiceBean(MasterTableType.CORPORATIVE_PROCESS_STATE.getCode())){
					processStateMap.put(param.getParameterTablePk(), param.getParameterName());
				}
				createSubHeaderCorporativeReport(xmlsw,corporativeTo, processInformation.get(0), processStateMap, currencyMap);
				xmlsw.writeStartElement("holders");
				createBodyReport(xmlsw,corporativeTo,new Integer(processInformation.get(0)[GenericReport.CORPORATIVE_TYPE].toString()),currencyMapAux,processInformation.get(0));
				xmlsw.writeEndElement();
			} else {
				xmlsw.writeStartElement("holders");
				xmlsw.writeEndElement();
				
			}
			xmlsw.writeEndElement();//</report>
			xmlsw.writeEndDocument();//</XML>
			xmlsw.flush();
	        xmlsw.close();
		}catch(XMLStreamException exm){
			exm.printStackTrace();
		} catch (ServiceException ex) {
			// TODO: handle exception
		}
		return baos;
	}
	
	public void createBodyReport(XMLStreamWriter xmlsw,CorporativeProcesInterestTO corporativeProcessInformation,Integer corporativeType
			,Map<Integer,String> currencyMap,Object[] corporativeInformation) throws XMLStreamException,ServiceException{
		List<Object[]> processControlInformation = corporativeReportService.getStockSpecialBlockedProcess(corporativeProcessInformation,corporativeInformation);
		if(!processControlInformation.isEmpty()){
			Map<Integer,String> blockedTypeMap = new HashMap<>();
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(MasterTableType.TYPE_AFFECTATION.getCode())){
				blockedTypeMap.put(param.getParameterTablePk(), param.getParameterName());
			}
			Integer processType = new Integer(corporativeInformation[CORPORATIVE_TYPE].toString());
			for(Object[] row : processControlInformation){
				xmlsw.writeStartElement("holder");
					createTagString(xmlsw, "participantCode", row[PARTICIPANTE_PK]);
					createTagString(xmlsw, "participantDesc", row[PARTICIPANTE_MNEMONIC]+" - "+row[PARTICIPANTE_DESC]);
					createTagString(xmlsw, "accountPk", row[ACCOUNT_PK]);
					createTagString(xmlsw, "accountNumber", row[ACCOUNT_NUMBER]);
					createTagCDataString(xmlsw, "holderName", row[HOLDERS_NAME]);
					createTagString(xmlsw, "stockPawn", row[STOCK_PAWN]);
					createTagString(xmlsw, "stockBan", row[STOCK_BAN]);
					createTagString(xmlsw, "stockOther", row[STOCK_OTHER]);
					createTagString(xmlsw, "stockTotal", row[STOCK_TOTAL]);
					createTagInteger(xmlsw, "processPawn", row[PROCESS_PAWN]);
					createTagInteger(xmlsw, "processBan", row[PROCESS_BAN]);
					createTagInteger(xmlsw, "processOther", row[PROCESS_OTHER]);
					createTagInteger(xmlsw, "processTotal", row[PROCESS_TOTAL]);			
					xmlsw.writeStartElement("blockeds");
					List<Object[]> blocks = corporativeReportService.getStockDetailBalanceBlockedProcess(corporativeProcessInformation.getProcessId(),
							new Long(row[ACCOUNT_PK].toString()), corporativeType);
					
						boolean blValidateExchange = validateExchange(blocks);
						
						for(Object[] block : blocks){
							xmlsw.writeStartElement("blocked");
							xmlsw.writeAttribute("id", row[PARTICIPANTE_PK]+" - "+row[ACCOUNT_PK]);
								createTagString(xmlsw, "blockNumber", block[BLOCK_DOC_NUMBER]);
								createTagString(xmlsw, "blockCite", block[BLOCK_CITE_NUMBER]);
								createTagString(xmlsw, "blockType", blockedTypeMap.get(new Integer(block[BLOCK_TYPE].toString())));								
								createTagString(xmlsw, "blockBalance", block[BLOCK_BALANCE]);
								if(blValidateExchange){
									if(new Integer(block[BLOCK_ORIGIN_DESTINY].toString()).equals(GeneralConstants.TWO_VALUE_INTEGER)){
										createTagString(xmlsw, ReportConstant.CURRENCY, CurrencyType.PYG.getCodeIso());
									}else{										
										createTagString(xmlsw, ReportConstant.CURRENCY, currencyMap.get(new Integer(corporativeInformation[CURRENCY_AUX].toString())));
									}
								}else{
									createTagString(xmlsw, ReportConstant.CURRENCY, currencyMap.get(new Integer(corporativeInformation[CURRENCY_AUX].toString())));
								}								
								createTagInteger(xmlsw, "blockAmortization",block[BLOCK_PROCESS]);							
								createTagString(xmlsw, "blockEntity", block[BLOCK_ENTITY]);
								createTagString(xmlsw, "corporativeType", "E");
							xmlsw.writeEndElement();
						}
					xmlsw.writeEndElement();
				xmlsw.writeEndElement();
			}
		}
	}
	
	/**
	 * Validate exchange.
	 *
	 * @param blocks the blocks
	 * @return true, if successful
	 */
	public boolean validateExchange(List<Object[]> blocks){
		boolean validate = false;
		List<Long> idResult = new ArrayList<Long>();
		for(Object[] block : blocks){
			idResult.add(new Long(block[BLOCK_RESULT_PK].toString()));
		}
		validate = corporativeReportService.validateExchange(idResult);
		return validate;
	}
}
