package com.pradera.report.generation.executor.corporative.to;

import java.io.Serializable;
import java.util.Date;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class StockCalculationProcessTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 24-abr-2015
 */
public class StockCalculationProcessTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1675786946774182193L;
	
	/** The participante. */
	private Long participante;
	
	/** The holder. */
	private Long holder;
	
	/** The security. */
	private String security;
	
	/** The stock type. */
	private Integer stockType;

	/** The stock class. */
	private Integer stockClass;
	
	/** The cut off date. */
	private Date cutOffDate;
	
	/** The registry date. */
	private Date registryDate;

	/**
	 * Gets the participante.
	 *
	 * @return the participante
	 */
	public Long getParticipante() {
		return participante;
	}

	/**
	 * Sets the participante.
	 *
	 * @param participante the new participante
	 */
	public void setParticipante(Long participante) {
		this.participante = participante;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Long getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Long holder) {
		this.holder = holder;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public String getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(String security) {
		this.security = security;
	}

	/**
	 * Gets the stock type.
	 *
	 * @return the stock type
	 */
	public Integer getStockType() {
		return stockType;
	}

	/**
	 * Sets the stock type.
	 *
	 * @param stockType the new stock type
	 */
	public void setStockType(Integer stockType) {
		this.stockType = stockType;
	}

	/**
	 * Gets the stock class.
	 *
	 * @return the stock class
	 */
	public Integer getStockClass() {
		return stockClass;
	}

	/**
	 * Sets the stock class.
	 *
	 * @param stockClass the new stock class
	 */
	public void setStockClass(Integer stockClass) {
		this.stockClass = stockClass;
	}

	/**
	 * Gets the cut off date.
	 *
	 * @return the cut off date
	 */
	public Date getCutOffDate() {
		return cutOffDate;
	}

	/**
	 * Sets the cut off date.
	 *
	 * @param cutOffDate the new cut off date
	 */
	public void setCutOffDate(Date cutOffDate) {
		this.cutOffDate = cutOffDate;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}
}
