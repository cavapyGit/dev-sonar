package com.pradera.report.generation.executor.settlement;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;

@ReportProcess(name="HistoryNetPositionsReport")
public class HistoryNetPositionsReport extends GenericReport{
	
	private static final long serialVersionUID = 1L;
	
	@Inject PraderaLogger log;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	ParticipantServiceBean participantServiceBean;
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> settlementCurrency = new HashMap<Integer, String>();
		
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		String participantCode = null;
		String participantMnemonic = null;
		
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.PARTICIPANT_PARAM))
				participantCode = r.getFilterValue();
		}
		
		if(participantCode != null)
			participantMnemonic = participantServiceBean.find(new Long(participantCode),Participant.class).getMnemonic();
		else
			participantMnemonic = "TODOS";
		
		try {
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				settlementCurrency.put(param.getParameterTablePk(), param.getParameterName());
			}
			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		parametersRequired.put("participant_mnemonic", participantMnemonic);
		parametersRequired.put("p_settlement_currency", settlementCurrency);
		
		return parametersRequired;
	}

}
