package com.pradera.report.generation.executor.issuances;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.securities.service.PaymentChronogramBySecurity;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "SecuritiesRegistrationInSystemReport")
public class SecuritiesRegistrationInSystemReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;
	
	@Inject
	private PraderaLogger log;

	public SecuritiesRegistrationInSystemReport() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {

		for(ReportLoggerDetail param: getReportLogger().getReportLoggerDetails()){
			
		}
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> secClassDocType 			= new HashMap<Integer, String>();
		Map<Integer,String> secClassSecurityDesc 		= new HashMap<Integer, String>();
		Map<Integer,String> secClasCurrency 			= new HashMap<Integer, String>();
		Map<Integer,String> secMotiveDesc 				= new HashMap<Integer, String>();

		try {
			filter.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secClassDocType.put(param.getParameterTablePk(), param.getIndicator1());
			}
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secClassSecurityDesc.put(param.getParameterTablePk(), param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secClasCurrency.put(param.getParameterTablePk(), param.getDescription());
			}

			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}

		parametersRequired.put("mDocType", secClassDocType);
		parametersRequired.put("mClassDesc", secClassSecurityDesc);
		parametersRequired.put("mCurrency", secClasCurrency);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());

		
		return parametersRequired;
		
		
	}

}
