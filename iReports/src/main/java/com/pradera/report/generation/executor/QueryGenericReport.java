package com.pradera.report.generation.executor;

import java.io.ByteArrayOutputStream;

import com.pradera.model.report.ReportLogger;
import com.pradera.report.generation.ReportProcess;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class QueryGenericReport.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/07/2013
 */
@ReportProcess(name="QueryGenericReport")
public class QueryGenericReport extends GenericReport {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2175478636055431753L;

	/**
	 * Instantiates a new query generic report.
	 */
	public QueryGenericReport() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#generateData(com.pradera.model.report.ReportLogger)
	 */
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
