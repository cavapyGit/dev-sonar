package com.pradera.report.generation.executor.account;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.GeographicLocationServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.account.service.AccountReportServiceBean;

@ReportProcess(name = "SecuritiesDirectSaleReport")
public class SecuritiesDirectSaleReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	private GeographicLocationServiceBean geographicLocationServiceBean;

	@EJB
	private AccountReportServiceBean accountReportServiceBean;
	
	@Inject
	private PraderaLogger log; 

	public SecuritiesDirectSaleReport() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		String lstDocumentNumber = null;
		Integer stateSecurity = null;
		String initialDate = null;
		String finalDate = null;
		String idSecurityCodePk = null;
		String vecDocNum[]=null;
		for(ReportLoggerDetail param: getReportLogger().getReportLoggerDetails()){
			if(param.getFilterName().equals("lstDocumentNumber")){
				if (Validations.validateIsNotNullAndNotEmpty(param.getFilterValue())){
					lstDocumentNumber = param.getFilterValue().toString().substring(1,param.getFilterValue().toString().length()-1);
					vecDocNum = lstDocumentNumber.split(",");
					if(vecDocNum!=null){
						StringBuilder sb = new StringBuilder("");
						for (int i = 0; i < vecDocNum.length; i++) {
							if(vecDocNum[i]!=null && vecDocNum[i]!="")
							sb.append("'").append(vecDocNum[i].trim()).append("',");
						}
						lstDocumentNumber = sb.toString().substring(0,sb.toString().length()-1);
					}
				}
			}
			if(param.getFilterName().equals("stateSecurity")){
				if (Validations.validateIsNotNullAndNotEmpty(param.getFilterValue())){
					stateSecurity = Integer.parseInt(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("initialDate")){
				if (Validations.validateIsNotNullAndNotEmpty(param.getFilterValue())){
					initialDate = param.getFilterValue().toString();
				}
			}
			if(param.getFilterName().equals("finalDate")){
				if (Validations.validateIsNotNullAndNotEmpty(param.getFilterValue())){
					finalDate = param.getFilterValue().toString();
				}
			}
			if(param.getFilterName().equals("idSecurityCodePk")){
				if (Validations.validateIsNotNullAndNotEmpty(param.getFilterValue())){
					idSecurityCodePk = param.getFilterValue().toString();
				}
			}
		} 
		Map<String,Object> parametersRequired = new HashMap<>();		
		try {
			String strQuery = accountReportServiceBean.getSecuritiesDirectSaleQuery(lstDocumentNumber,initialDate,finalDate,idSecurityCodePk,stateSecurity);
			parametersRequired.put("str_Query", strQuery);
			String stateSecurityDesc=null;
			if(Validations.validateIsNotNullAndPositive(stateSecurity)){
				stateSecurityDesc = parameterService.find(ParameterTable.class, stateSecurity).getDescription();
			}
			parametersRequired.put("state_security", stateSecurityDesc);
			String documentNumber=null;
			if(vecDocNum!=null && vecDocNum.length==1)
				documentNumber = vecDocNum[0];
			parametersRequired.put("documentNumber", documentNumber);
			parametersRequired.put("date_initial", initialDate);
			parametersRequired.put("date_end", finalDate);
			parametersRequired.put("security_class_description", idSecurityCodePk);
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}	
		return parametersRequired;
	}
}
