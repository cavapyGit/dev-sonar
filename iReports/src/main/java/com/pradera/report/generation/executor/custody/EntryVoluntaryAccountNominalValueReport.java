package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;

import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.util.view.UtilReportConstants;
//reporte de anotaciones en cuenta voluntaria a valor nominal
@ReportProcess(name = "EntryVoluntaryAccountNominalValueReport")
public class EntryVoluntaryAccountNominalValueReport extends GenericReport{
	
	private static final long serialVersionUID = 1L;
	private final String MAP_REASON_DESMATERIALIZATION="map_reason_desmaterialization";
	private final String MAP_DEMATERIALIZATION_STATE="map_dematerialization_state";
	
	private Map<String, Object> map= new HashMap<String, Object>();
	@EJB
	private ParameterServiceBean parameterService;
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		ByteArrayOutputStream arrayOutputStream=new  ByteArrayOutputStream();
		return arrayOutputStream;
	}
	@Override
	public Map<String, Object> getCustomJasperParameters(){
		Map<Integer,String> reasonsDematerialisation = new HashMap<Integer, String>();
		Map<Integer,String> statesDematerialization = new HashMap<Integer, String>();
		ParameterTableTO  parameterFilter = new ParameterTableTO();
	    
		parameterFilter.setMasterTableFk(MasterTableType.DEMATERIALIZATION_MOTIVE.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(parameterFilter)) 
			reasonsDematerialisation.put(param.getParameterTablePk(), param.getDescription());
		map.put(MAP_REASON_DESMATERIALIZATION, reasonsDematerialisation);
	    parameterFilter.setMasterTableFk(MasterTableType.DEMATERIALIZATION_STATE.getCode());
	    for(ParameterTable param : parameterService.getListParameterTableServiceBean(parameterFilter)) 
	    	statesDematerialization.put(param.getParameterTablePk(), param.getDescription());
		map.put(MAP_DEMATERIALIZATION_STATE, statesDematerialization);
//		Iterator it = map.entrySet().iterator();
//		while (it.hasNext()) {
//			Map.Entry e = (Map.Entry)it.next();
//			System.out.println(e.getKey() + ":" + e.getValue());
//		}
		map.put("logo_path", UtilReportConstants.readLogoReport());	
		return map;
	}
}
