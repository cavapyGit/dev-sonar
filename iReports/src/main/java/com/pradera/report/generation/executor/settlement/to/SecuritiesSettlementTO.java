package com.pradera.report.generation.executor.settlement.to;

import java.io.Serializable;
import java.math.BigDecimal;

public class SecuritiesSettlementTO  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3859127733928234724L;
	
	private Long idParticipantPk;
	
	private Integer currency;
	
	private String dateStrOpe;
	
	private Long idPartBuyer;
	
	private Long idPartSeller;
	
	private BigDecimal price;
	
	private BigDecimal amount;
	
	private Integer scheduleType;
	
	public SecuritiesSettlementTO() {
	}

	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public String getDateStrOpe() {
		return dateStrOpe;
	}

	public void setDateStrOpe(String dateStrOpe) {
		this.dateStrOpe = dateStrOpe;
	}

	public Long getIdPartBuyer() {
		return idPartBuyer;
	}

	public void setIdPartBuyer(Long idPartBuyer) {
		this.idPartBuyer = idPartBuyer;
	}

	public Long getIdPartSeller() {
		return idPartSeller;
	}

	public void setIdPartSeller(Long idPartSeller) {
		this.idPartSeller = idPartSeller;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Integer getScheduleType() {
		return scheduleType;
	}

	public void setScheduleType(Integer scheduleType) {
		this.scheduleType = scheduleType;
	}
}
