package com.pradera.report.generation.executor.collectioneconomic;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;

@ReportProcess(name = "ObligationPaymentEconomicRightReport")
public class ObligationPaymentEconomicRightReport extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6222061741058613010L;

	@Inject
	PraderaLogger log;

	@EJB
	private ParameterServiceBean parameterService;

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		return null;
	}

	@Override
	public void addParametersQueryReport() {
	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		Map<String, Object> parametersRequired = new HashMap<>();
		ParameterTableTO filter = new ParameterTableTO();
		Map<Integer, String> motive = new HashMap<Integer, String>();
		Map<Integer, String> state = new HashMap<Integer, String>();
		Map<Integer, String> balance_type = new HashMap<Integer, String>();
		Map<Integer, String> affectation_type = new HashMap<Integer, String>();
		Map<Integer, String> level_type = new HashMap<Integer, String>();

		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();

		// variables para setear en los parametros del reporte (300)
		String currency = null;
		String idSecurityCode = null;
		String initialDate = null;
		String finalDate = null;

		for (ReportLoggerDetail r : loggerDetails) {

			if (r.getFilterName().equals("p_currency"))
				if (r.getFilterValue() != null)
					currency = r.getFilterValue();

			if (r.getFilterName().equals("security_code"))
				if (r.getFilterValue() != null)
					idSecurityCode = r.getFilterValue();

			if (r.getFilterName().equals("date_initial"))
				if (r.getFilterValue() != null)
					initialDate = r.getFilterValue();

			if (r.getFilterName().equals("date_end"))
				if (r.getFilterValue() != null)
					finalDate = r.getFilterValue();
		}

		try {

			filter.setMasterTableFk(MasterTableType.MASTER_TABLE_CHANGE_OWNERSHIP_MOTIVE_TYPE.getCode());
			for (ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				motive.put(param.getParameterTablePk(), param.getParameterName());
			}

			filter.setMasterTableFk(MasterTableType.MASTER_TABLE_CHANGE_OWNERSHIP_STATUS_TYPE.getCode());
			for (ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				state.put(param.getParameterTablePk(), param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.MASTER_TABLE_CHANGE_OWNERSHIP_BALANCE_TYPE.getCode());
			for (ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				balance_type.put(param.getParameterTablePk(), param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.TYPE_AFFECTATION.getCode());
			for (ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				affectation_type.put(param.getParameterTablePk(), param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.LEVEL_AFFECTATION.getCode());
			for (ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				level_type.put(param.getParameterTablePk(), param.getDescription());
			}

		} catch (Exception ex) {
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		parametersRequired.put("p_motive", motive);
		parametersRequired.put("p_state", state);
		parametersRequired.put("p_balance_type", balance_type);
		parametersRequired.put("p_affectation_type", affectation_type);
		parametersRequired.put("p_level_type", level_type);

		// Obteniendo los parametros para el reporte 300 de OBLIGACIONES DE PAGO DE DERECHOS ECONOMICOS
		parametersRequired.put("p_currency", currency);
		parametersRequired.put("security_code", idSecurityCode);
		parametersRequired.put("date_initial", initialDate);
		parametersRequired.put("date_end", finalDate);

		return parametersRequired;
	}

}
