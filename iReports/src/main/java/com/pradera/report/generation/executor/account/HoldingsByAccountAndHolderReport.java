package com.pradera.report.generation.executor.account;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.management.RuntimeErrorException;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.account.service.AccountReportServiceBean;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.executor.securities.service.IssuerListReportServiceBean;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "HoldingsByAccountAndHolderReport")
public class HoldingsByAccountAndHolderReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	private AccountReportServiceBean accountReportServiceBean;

	@EJB
	private IssuerListReportServiceBean issuerServiceBean;
	
	@EJB
	private CustodyReportServiceBean custodyReportServiceBean;
	
	@EJB
	private HolderAccountComponentServiceBean accountComponentServiceBean;
	
	@Inject
	private PraderaLogger log;
	
	public static final String MSG_NOT_EXCHANGE_RATE = "No existe tasa de cambio en D\u00F3lares para la fecha ";

	public HoldingsByAccountAndHolderReport() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		GenericsFiltersTO gfto = new GenericsFiltersTO();
		String cuiDescription = GeneralConstants.EMPTY_STRING;
		String accountDescription = GeneralConstants.EMPTY_STRING;
		String securityClassDescription = GeneralConstants.EMPTY_STRING;
		String securityKeyDescription = GeneralConstants.EMPTY_STRING;
		String issuerDescription = GeneralConstants.EMPTY_STRING;
		String currencyDescription = GeneralConstants.EMPTY_STRING;

		for(ReportLoggerDetail param: getReportLogger().getReportLoggerDetails()){
			if(param.getFilterName().equals("participant")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setParticipant(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("cui_code")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setCui(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("holder_account")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setIdHolderAccountPk(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("security_class_key")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setIdSecurityCodePk(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("date_initial")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setInitialDt(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("issuer")){
				if (Validations.validateIsNotNullAndNotEmpty(param.getFilterValue())){
					gfto.setIdIssuer(param.getFilterValue());
				}
			}
			if(param.getFilterName().equals("security_currency")){
				if (Validations.validateIsNotNullAndNotEmpty(param.getFilterValue())){
					gfto.setCurrency(param.getFilterValue());
				}
			}
			if(param.getFilterName().equals("class_security")){
				if (Validations.validateIsNotNullAndNotEmpty(param.getFilterValue())){
					gfto.setSecurityClass(param.getFilterValue());
				}
			}
		}
		
		Long idStkCalcProcess= null;
		Date cutdate = CommonsUtilities.convertStringtoDate(gfto.getInitialDt());
		String strQuery = "";
		String strSubQuery = "";
		BigDecimal exchange_rate_dollar = null;
		
		try{
			DailyExchangeRates currency=new DailyExchangeRates();
			currency.setIdCurrency(CurrencyType.USD.getCode());
			currency.setDateRate(cutdate);
			exchange_rate_dollar = accountReportServiceBean.getExchangeRateByCurrencyType(currency);
		} catch (ServiceException e) {
			if(e.getErrorService().equals(ErrorServiceType.NOT_EXCHANGE_RATE)){
					throw new RuntimeErrorException(null, MSG_NOT_EXCHANGE_RATE + CommonsUtilities.convertDatetoString(cutdate));
			}else{
				e.printStackTrace();
			}
			log.error(e.getMessage());
		}

		strQuery = accountReportServiceBean.getQueryHoldingsByAccountAndHolderReport(gfto, idStkCalcProcess);
		strSubQuery = accountReportServiceBean.getSubTotalQueryHoldingsByAccount(gfto);
		
		try {
		
			//PARA LA CUENTA TITULAR
			if(gfto.getIdHolderAccountPk() != null)
				accountDescription = accountComponentServiceBean.find(new Long(gfto.getIdHolderAccountPk()),HolderAccount.class).getAccountNumber().toString();
			else
				accountDescription = "Todos";
			//PARA EL CUI DEL TITULAR
			if(gfto.getCui() != null)
				cuiDescription = gfto.getCui().toString();
			else
				cuiDescription = "Todos";
			//EMISOR
			if(gfto.getIdIssuer() != null){
				Issuer iss = issuerServiceBean.find(Issuer.class, gfto.getIdIssuer());
				issuerDescription = iss.getMnemonic() + " - " + iss.getBusinessName();
			}else{
				issuerDescription = "Todos";
			}
			//CLASE DE VALOR
			if(gfto.getSecurityClass() != null){
				ParameterTable parameterClass;
				parameterClass = parameterService.getParameterTableById(Integer.parseInt(gfto.getSecurityClass()));
				securityClassDescription=parameterClass.getParameterName();
			}else{
				securityClassDescription = "Todos";
			}
			//clave de valor
			if(gfto.getIdSecurityCodePk() != null)
				securityKeyDescription = gfto.getIdSecurityCodePk().toString();
			else
				securityKeyDescription = "Todos";
			//currency
			if(gfto.getCurrency() != null){
				ParameterTable parameterClass;
				parameterClass = parameterService.getParameterTableById(Integer.parseInt(gfto.getCurrency()));
				currencyDescription=parameterClass.getParameterName();
			}else{
				currencyDescription = "Todos";
			}
		
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> secSecurityClass		= new HashMap<Integer, String>();
		Map<Integer,String> secCurrency		= new HashMap<Integer, String>();
		try {
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secSecurityClass.put(param.getParameterTablePk(), "(" + param.getText1() + ")" + param.getParameterName());
			}
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secCurrency.put(param.getParameterTablePk(), "(" + param.getDescription() + ")" + param.getParameterName());
			}
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		

		parametersRequired.put("str_Query", strQuery);
		parametersRequired.put("str_subTotalQuery", strSubQuery);
		parametersRequired.put("court_date", gfto.getInitialDt());
		parametersRequired.put("mCurrency", secCurrency);
		parametersRequired.put("mSecurityClass", secSecurityClass);
		parametersRequired.put("p_issuer_desc", issuerDescription);
		parametersRequired.put("p_cui_desc", cuiDescription);
		parametersRequired.put("p_account_holder_desc", accountDescription);
		parametersRequired.put("p_currency_desc", currencyDescription);
		parametersRequired.put("p_security_class_key_desc", securityKeyDescription);
		parametersRequired.put("p_security_class_desc", securityClassDescription);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		
		return parametersRequired;
	}
}
