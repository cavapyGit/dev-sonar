package com.pradera.report.generation.executor.securities;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.securities.service.ExpirationSecuritiesReportServiceBean;
import com.pradera.report.generation.executor.securities.to.ExpirationSecuritiesReportTO;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "ExpirationSecuritiesReport")
public class ExpirationSecuritiesReport extends GenericReport{
	
	private static final long serialVersionUID = 1L;

	@EJB private ParameterServiceBean parameterService;
	@Inject PraderaLogger log;
	@EJB ParticipantServiceBean participantServiceBean;
	
	@EJB
	ExpirationSecuritiesReportServiceBean expirationSecuritiesReportServiceBean;

	public ExpirationSecuritiesReport() {}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		return null;
	}

	@Override
	public void addParametersQueryReport() {}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		Map<String, Object> parametersRequired = new HashMap<>();
		String indAutomatic = null;
		boolean excelDelimited = false;
		ExpirationSecuritiesReportTO expirationSecuritiesReportTO = new ExpirationSecuritiesReportTO();
		
		for(ReportLoggerDetail param: getReportLogger().getReportLoggerDetails()){
			/*if(param.getFilterName().equals("ind_automatic")){
				if (param.getFilterValue()!=null){
					indAutomatic = param.getFilterValue().toString();
				}
			}*/
			
			if(param.getFilterName().equals("participant")){
				if (param.getFilterValue()!=null){
					expirationSecuritiesReportTO.setParticipant(new Long(param.getFilterValue()).longValue());
					parametersRequired.put("participant", param.getFilterValue().toString());
				}
			}
			
			if(param.getFilterName().equals("issuer_code")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					expirationSecuritiesReportTO.setIssuer(param.getFilterValue().toString());
					parametersRequired.put("issuer_code", param.getFilterValue());
				}
			}
			
			if(param.getFilterName().equals("security_class")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					expirationSecuritiesReportTO.setSecurityClass(param.getFilterValue().toString());
					parametersRequired.put("security_class", param.getFilterValue());
				}
			}
			
			if(param.getFilterName().equals("security_code_only")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					expirationSecuritiesReportTO.setSecurityClass(param.getFilterValue().toString());
					parametersRequired.put("security_code_only", param.getFilterValue());
				}
			}
			
			if(param.getFilterName().equals("bcb_format")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					expirationSecuritiesReportTO.setBcbFormat(Integer.valueOf(param.getFilterValue()));
					parametersRequired.put("bcb_format", param.getFilterValue());
				}
			}
			
			if(param.getFilterName().equals("without_dpf")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					expirationSecuritiesReportTO.setWithoutDpf(param.getFilterValue());
					parametersRequired.put("without_dpf", param.getFilterValue());
				}
			}
			
			if(param.getFilterName().equals("date_initial")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					expirationSecuritiesReportTO.setInitialDate(CommonsUtilities.convertStringtoDate(param.getFilterValue()));
					parametersRequired.put("date_initial", param.getFilterValue());
				}
			}
			
			if(param.getFilterName().equals("date_end")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					expirationSecuritiesReportTO.setFinalDate(CommonsUtilities.convertStringtoDate(param.getFilterValue()));
					parametersRequired.put("date_end", param.getFilterValue());
				}
			}
			
			if(param.getFilterName().equals("holder_account")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					expirationSecuritiesReportTO.setHolderAccount(Integer.valueOf(param.getFilterValue()));
				}
			}
			
			if(param.getFilterName().equals("cui_holder")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					expirationSecuritiesReportTO.setHolder(param.getFilterValue());
					parametersRequired.put("cui_holder", param.getFilterValue());
				}
			}
			
			if(param.getFilterName().equals("institution_type")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					expirationSecuritiesReportTO.setInstitutionType(param.getFilterValue());
					parametersRequired.put("institution_type", param.getFilterValue());
				}
			}
			
			if(param.getFilterName().equals("participant_investor_institution_type}")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					expirationSecuritiesReportTO.setParticipantInvestorInstitutionType(param.getFilterValue());
					parametersRequired.put("participant_investor_institution_type", param.getFilterValue());
				}
			}
			
			if(param.getFilterName().equals("reportFormats")) {
				if (Validations.validateIsNotNullAndNotEmpty(param.getFilterValue()) &&
						Integer.valueOf(param.getFilterValue()) == 2853) {
					excelDelimited = true;
				}
			}
		}
		
		parametersRequired.put("indAutomatic", indAutomatic);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		
		if(excelDelimited) {
			parametersRequired.put(ReportConstant.REPORT_EXCEL_DELIMITADO, expirationSecuritiesReportServiceBean.generateExcelDelimitated(parametersRequired, expirationSecuritiesReportTO));
		}
		
		return parametersRequired;
	}

}
