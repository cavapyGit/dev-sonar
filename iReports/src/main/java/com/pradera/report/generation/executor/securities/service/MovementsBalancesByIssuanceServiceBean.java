package com.pradera.report.generation.executor.securities.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.report.generation.executor.securities.to.MovementsBalancesByIssuanceTO;

@Stateless
@Performance
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class MovementsBalancesByIssuanceServiceBean extends CrudDaoServiceBean{
	
	public MovementsBalancesByIssuanceServiceBean() {
		// TODO Auto-generated constructor stub
	}
	
	public Issuer getIssuerByPk(MovementsBalancesByIssuanceTO movementsBalancesTO){
		String IssuerPk = movementsBalancesTO.getIdIssuerPk();
		String IssuancePk = movementsBalancesTO.getIdIssuancePk();
		StringBuilder queryIssuer = new StringBuilder();
		queryIssuer.append("SELECT iss FROM Issuer iss INNER JOIN FETCH iss.issuances issuanc WHERE iss.idIssuerPk = :idIssuerParam ");
		if(IssuancePk != null && !IssuancePk.isEmpty()){
			queryIssuer.append(" AND issuanc.idIssuanceCodePk = :idIssuanceParam");
		}
		
		Query query = em.createQuery(queryIssuer.toString());
		query.setParameter("idIssuerParam", IssuerPk);
		
		if(IssuancePk != null && !IssuancePk.isEmpty()){
			query.setParameter("idIssuanceParam", IssuancePk);
		}
		
		Issuer issuerFetch = (Issuer) query.getSingleResult();
		return issuerFetch;
	}
	
	public List<Object[]> getMovementListByParameters(
			MovementsBalancesByIssuanceTO movementsBalancesTO, String IssuancePk) {
				
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" SELECT ");
		sbQuery.append(" 	SM.MOVEMENT_DATE, ");
		sbQuery.append(" 	(SELECT MT.MOVEMENT_NAME FROM MOVEMENT_TYPE MT WHERE MT.ID_MOVEMENT_TYPE_PK = SM.ID_MOVEMENT_TYPE_FK) MOVEMENT_NAME, ");
		sbQuery.append(" 	(SELECT DECODE(MB.ID_BEHAVIOR, 1, 'INGRESO', 'SALIDA') FROM MOVEMENT_BEHAVIOR MB WHERE MB.id_movement_type_fk = SM.id_movement_type_fk) MOVEMENT_TYPE, ");
		sbQuery.append(" 	IO.OPERATION_DATE, ");
		sbQuery.append(" 	IO.ID_ISSUANCE_OPERATION_PK OPERATION_NUMBER, ");
		sbQuery.append(" 	SM.ID_ISIN_CODE_FK, ");
		sbQuery.append(" 	NVL(SM.MOVEMENT_QUANTITY,0) MOVEMENT_QUANTITY, ");
		sbQuery.append(" 	NVL(SM.MOVEMENT_AMOUNT,0) MOVEMENT_AMOUNT ");	
		sbQuery.append(" FROM	");
		sbQuery.append("  	SECURITIES_MOVEMENT SM ");
		sbQuery.append("  	INNER JOIN ISSUANCE_OPERATION IO ON SM.ID_SECURITIES_OPERATION_FK = IO.ID_SECURITIES_OPERATION_FK ");
		sbQuery.append(" WHERE  ");		  
		sbQuery.append("	SM.ID_ISIN_CODE_FK IN (SELECT S.ID_ISIN_CODE_PK FROM SECURITY S WHERE S.ID_ISSUANCE_CODE_FK = '"+IssuancePk+"') ");	
		
		if (Validations.validateIsNotNullAndNotEmpty(movementsBalancesTO
				.getFinalDate())) {
			sbQuery.append(" AND SM.MOVEMENT_DATE<=:finalDate ");
		}
		if (Validations.validateIsNotNullAndNotEmpty(movementsBalancesTO
				.getInitialDate())) {
			sbQuery.append(" AND SM.MOVEMENT_DATE>=:initialDate ");
		}				
		sbQuery.append(" ORDER BY ");
		sbQuery.append("  	1 ASC, ");
		sbQuery.append("  	SM.MOVEMENT_DATE DESC ");

		Query query = em.createNativeQuery(sbQuery.toString());
		
		if (Validations.validateIsNotNullAndNotEmpty(movementsBalancesTO
				.getInitialDate())) {
			query.setParameter("initialDate", movementsBalancesTO.getInitialDate());
		}
		if (Validations.validateIsNotNullAndNotEmpty(movementsBalancesTO
				.getFinalDate())) {
			query.setParameter("finalDate", movementsBalancesTO.getFinalDate());
		}

		return query.getResultList();
	}

}
