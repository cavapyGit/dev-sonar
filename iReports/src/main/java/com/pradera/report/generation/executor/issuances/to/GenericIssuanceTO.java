package com.pradera.report.generation.executor.issuances.to;

public class GenericIssuanceTO {

	private String cui;
	private String holderAccount;
	private String securities;
	private String issuer;
	private String issuance;
	private String participant;
	private String participantMnemonic;
	private String expirationOf;
	private String currency;
	private String initialDate;
	private String finalDate;
	private String securitiesSituation;
	private String securityClass;
	private String indAutomatic;
	
	public String getCui() {
		return cui;
	}
	public void setCui(String cui) {
		this.cui = cui;
	}
	public String getHolderAccount() {
		return holderAccount;
	}
	public void setHolderAccount(String holderAccount) {
		this.holderAccount = holderAccount;
	}
	public String getSecurities() {
		return securities;
	}
	public void setSecurities(String securities) {
		this.securities = securities;
	}
	public String getIssuer() {
		return issuer;
	}
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	public String getIssuance() {
		return issuance;
	}
	public void setIssuance(String issuance) {
		this.issuance = issuance;
	}
	public String getParticipant() {
		return participant;
	}
	public void setParticipant(String participant) {
		this.participant = participant;
	}
	public String getExpirationOf() {
		return expirationOf;
	}
	public void setExpirationOf(String expirationOf) {
		this.expirationOf = expirationOf;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getInitialDate() {
		return initialDate;
	}
	public void setInitialDate(String initialDate) {
		this.initialDate = initialDate;
	}
	public String getFinalDate() {
		return finalDate;
	}
	public void setFinalDate(String finalDate) {
		this.finalDate = finalDate;
	}
	public String getSecuritiesSituation() {
		return securitiesSituation;
	}
	public void setSecuritiesSituation(String securitiesSituation) {
		this.securitiesSituation = securitiesSituation;
	}
	
	public String getParticipantMnemonic() {
		return participantMnemonic;
	}
	public void setParticipantMnemonic(String participantMnemonic) {
		this.participantMnemonic = participantMnemonic;
	}
	/**
	 * @return the securityClass
	 */
	public String getSecurityClass() {
		return securityClass;
	}
	/**
	 * @param securityClass the securityClass to set
	 */
	public void setSecurityClass(String securityClass) {
		this.securityClass = securityClass;
	}
	/**
	 * @return the indAutomatic
	 */
	public String getIndAutomatic() {
		return indAutomatic;
	}
	/**
	 * @param indAutomatic the indAutomatic to set
	 */
	public void setIndAutomatic(String indAutomatic) {
		this.indAutomatic = indAutomatic;
	}
	
}
