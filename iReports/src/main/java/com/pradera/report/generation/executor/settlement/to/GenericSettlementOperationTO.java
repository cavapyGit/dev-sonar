package com.pradera.report.generation.executor.settlement.to;

import java.io.Serializable;

public class GenericSettlementOperationTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String mechanism;
	private String modality;
	private String modalityGroup;
	private String idSecurityCodePk;
	private String settlementSchema;
	private String settlementType;
	private String sellerParticipant;
	private String buyerParticipant;
	private String initialDate;
	private String finalDate;
	private String role;
	private String issuer;
	private String operationState;
	private String operationNumber;
	private String settlementDate;
	private String cui;
	private String holderAccount;
	private String currency;
	private String date;
	private String liquidationProcesses;
	private String ballotNumber;
	private String sequential;
	private String institutionType;
	private String participant;
	private String dateType;
	private String expirationType;
	
	
	public String getMechanism() {
		return mechanism;
	}
	public void setMechanism(String mechanism) {
		this.mechanism = mechanism;
	}
	public String getModality() {
		return modality;
	}
	public void setModality(String modality) {
		this.modality = modality;
	}
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
	public String getSettlementSchema() {
		return settlementSchema;
	}
	public void setSettlementSchema(String settlementSchema) {
		this.settlementSchema = settlementSchema;
	}
	public String getSettlementType() {
		return settlementType;
	}
	public void setSettlementType(String settlementType) {
		this.settlementType = settlementType;
	}
	public String getSellerParticipant() {
		return sellerParticipant;
	}
	public void setSellerParticipant(String sellerParticipant) {
		this.sellerParticipant = sellerParticipant;
	}
	public String getBuyerParticipant() {
		return buyerParticipant;
	}
	public void setBuyerParticipant(String buyerParticipant) {
		this.buyerParticipant = buyerParticipant;
	}
	public String getInitialDate() {
		return initialDate;
	}
	public void setInitialDate(String initialDate) {
		this.initialDate = initialDate;
	}
	public String getFinalDate() {
		return finalDate;
	}
	public void setFinalDate(String finalDate) {
		this.finalDate = finalDate;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getIssuer() {
		return issuer;
	}
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	public String getOperationState() {
		return operationState;
	}
	public void setOperationState(String operationState) {
		this.operationState = operationState;
	}
	public String getOperationNumber() {
		return operationNumber;
	}
	public void setOperationNumber(String operationNumber) {
		this.operationNumber = operationNumber;
	}
	public String getModalityGroup() {
		return modalityGroup;
	}
	public void setModalityGroup(String modalityGroup) {
		this.modalityGroup = modalityGroup;
	}
	public String getSettlementDate() {
		return settlementDate;
	}
	public void setSettlementDate(String settlementDate) {
		this.settlementDate = settlementDate;
	}
	public String getCui() {
		return cui;
	}
	public void setCui(String cui) {
		this.cui = cui;
	}
	public String getHolderAccount() {
		return holderAccount;
	}
	public void setHolderAccount(String holderAccount) {
		this.holderAccount = holderAccount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getLiquidationProcesses() {
		return liquidationProcesses;
	}
	public void setLiquidationProcesses(String liquidationProcesses) {
		this.liquidationProcesses = liquidationProcesses;
	}
	public String getBallotNumber() {
		return ballotNumber;
	}
	public void setBallotNumber(String ballotNumber) {
		this.ballotNumber = ballotNumber;
	}
	public String getSequential() {
		return sequential;
	}
	public void setSequential(String sequential) {
		this.sequential = sequential;
	}
	public String getInstitutionType() {
		return institutionType;
	}
	public void setInstitutionType(String institutionType) {
		this.institutionType = institutionType;
	}
	public String getDateType() {
		return dateType;
	}
	public void setDateType(String dateType) {
		this.dateType = dateType;
	}
	public String getExpirationType() {
		return expirationType;
	}
	public void setExpirationType(String expirationType) {
		this.expirationType = expirationType;
	}
	public String getParticipant() {
		return participant;
	}
	public void setParticipant(String participant) {
		this.participant = participant;
	}
	
	
}
