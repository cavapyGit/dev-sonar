package com.pradera.report.generation.executor.excel.delimited.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ReportPositionsPartAccountTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	public ReportPositionsPartAccountTO() {
		
	}
	
	private String participante;
	
	private String codigoAlterno;
	
	private String claseValor;
	
	private String moneda;
	
	private String codigoValor;
	
	private String contable;
	
	private String disponible;
	
	private String prenda;
	
	private String embargo;
	
	private String fechaVencimiento;
	
	private String saldosAcreditacion;
	
	private String reportante;
	
	private String reportado;
	
	private String reportador;
	
	private String compra;
	
	private String venta;
	
	private String transito;
	
	private String motivoIngreso;
	
	private String valorNominal;
	
	private String claveValor;
	
	private String total;
	
	private String gravamen;
	
	private String bloqueoOtros;
	
	private String totalCuentas;
	
	private String nombreCuenta;
	
	private String referencePartCode;
	
	public String getParticipante() {
		return participante;
	}

	public void setParticipante(String participante) {
		this.participante = participante;
	}

	public String getCodigoAlterno() {
		return codigoAlterno;
	}

	public void setCodigoAlterno(String codigoAlterno) {
		this.codigoAlterno = codigoAlterno;
	}

	public String getClaseValor() {
		return claseValor;
	}

	public void setClaseValor(String claseValor) {
		this.claseValor = claseValor;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getCodigoValor() {
		return codigoValor;
	}

	public void setCodigoValor(String codigoValor) {
		this.codigoValor = codigoValor;
	}

	public String getContable() {
		return contable;
	}

	public void setContable(String contable) {
		this.contable = contable;
	}

	public String getDisponible() {
		return disponible;
	}

	public void setDisponible(String disponible) {
		this.disponible = disponible;
	}

	public String getPrenda() {
		return prenda;
	}

	public void setPrenda(String prenda) {
		this.prenda = prenda;
	}

	public String getEmbargo() {
		return embargo;
	}

	public void setEmbargo(String embargo) {
		this.embargo = embargo;
	}

	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public String getSaldosAcreditacion() {
		return saldosAcreditacion;
	}

	public void setSaldosAcreditacion(String saldosAcreditacion) {
		this.saldosAcreditacion = saldosAcreditacion;
	}

	public String getReportante() {
		return reportante;
	}

	public void setReportante(String reportante) {
		this.reportante = reportante;
	}

	public String getReportado() {
		return reportado;
	}

	public void setReportado(String reportado) {
		this.reportado = reportado;
	}

	public String getCompra() {
		return compra;
	}

	public void setCompra(String compra) {
		this.compra = compra;
	}

	public String getVenta() {
		return venta;
	}

	public void setVenta(String venta) {
		this.venta = venta;
	}

	public String getTransito() {
		return transito;
	}

	public void setTransito(String transito) {
		this.transito = transito;
	}

	public String getValorNominal() {
		return valorNominal;
	}

	public void setValorNominal(String valorNominal) {
		this.valorNominal = valorNominal;
	}

	public String getMotivoIngreso() {
		return motivoIngreso;
	}

	public void setMotivoIngreso(String motivoIngreso) {
		this.motivoIngreso = motivoIngreso;
	}

	public String getClaveValor() {
		return claveValor;
	}

	public void setClaveValor(String claveValor) {
		this.claveValor = claveValor;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getGravamen() {
		return gravamen;
	}

	public void setGravamen(String gravamen) {
		this.gravamen = gravamen;
	}

	public String getBloqueoOtros() {
		return bloqueoOtros;
	}

	public void setBloqueoOtros(String bloqueoOtros) {
		this.bloqueoOtros = bloqueoOtros;
	}

	public String getReportador() {
		return reportador;
	}

	public void setReportador(String reportador) {
		this.reportador = reportador;
	}

	public String getTotalCuentas() {
		return totalCuentas;
	}

	public void setTotalCuentas(String totalCuentas) {
		this.totalCuentas = totalCuentas;
	}

	public String getNombreCuenta() {
		return nombreCuenta;
	}

	public void setNombreCuenta(String nombreCuenta) {
		this.nombreCuenta = nombreCuenta;
	}

	public String getReferencePartCode() {
		return referencePartCode;
	}

	public void setReferencePartCode(String referencePartCode) {
		this.referencePartCode = referencePartCode;
	}

}
