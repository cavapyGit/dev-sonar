package com.pradera.report.generation.executor.settlement.to;

import java.io.Serializable;
	
public class TransferExtrabursatilSirtexTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String idParticipant;
	private String idIssuer;
	private String securityClass;
	private String idSecurityCodePk;
	private String currency;
	private String mecanismoNeg;
	private String modalidadNeg;
	private String formaPago;
	private String idHolder;
	private String valPublicOrPrivate;
	
	private String initialDate;
	private String finalDate;
	public String getIdParticipant() {
		return idParticipant;
	}
	public void setIdParticipant(String idParticipant) {
		this.idParticipant = idParticipant;
	}
	public String getIdIssuer() {
		return idIssuer;
	}
	public void setIdIssuer(String idIssuer) {
		this.idIssuer = idIssuer;
	}
	public String getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(String securityClass) {
		this.securityClass = securityClass;
	}
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getMecanismoNeg() {
		return mecanismoNeg;
	}
	public void setMecanismoNeg(String mecanismoNeg) {
		this.mecanismoNeg = mecanismoNeg;
	}
	public String getModalidadNeg() {
		return modalidadNeg;
	}
	public void setModalidadNeg(String modalidadNeg) {
		this.modalidadNeg = modalidadNeg;
	}
	public String getFormaPago() {
		return formaPago;
	}
	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}
	public String getIdHolder() {
		return idHolder;
	}
	public void setIdHolder(String idHolder) {
		this.idHolder = idHolder;
	}
	public String getValPublicOrPrivate() {
		return valPublicOrPrivate;
	}
	public void setValPublicOrPrivate(String valPublicOrPrivate) {
		this.valPublicOrPrivate = valPublicOrPrivate;
	}
	public String getInitialDate() {
		return initialDate;
	}
	public void setInitialDate(String initialDate) {
		this.initialDate = initialDate;
	}
	public String getFinalDate() {
		return finalDate;
	}
	public void setFinalDate(String finalDate) {
		this.finalDate = finalDate;
	}
}
