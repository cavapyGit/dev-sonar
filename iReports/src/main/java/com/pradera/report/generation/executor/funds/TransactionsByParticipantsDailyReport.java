package com.pradera.report.generation.executor.funds;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "TransactionsByParticipantsDailyReport")
public class TransactionsByParticipantsDailyReport extends GenericReport{
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	PraderaLogger log;
	
	public TransactionsByParticipantsDailyReport() {
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParametersQueryReport() { }
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String, Object> parametersRequired = new HashMap<>();
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		Date date = CommonsUtilities.currentDate();
		String participantDescription = null;

		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.PARTICIPANT_PARAM) && r.getFilterValue()!=null)
				participantDescription = r.getFilterDescription();
			else
				participantDescription = "TODOS";
		}
		parametersRequired.put("participant_desc", participantDescription);
		parametersRequired.put("fecha_actual", CommonsUtilities.convertDatetoString(date));
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		
		return parametersRequired;
	}
	
}