package com.pradera.report.generation.executor.accounting.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.pradera.core.component.helperui.to.AccountingAccountTo;
import com.pradera.model.accounting.AccountingAccount;
import com.pradera.model.accounting.AccountingProcess;
import com.pradera.model.accounting.AccountingReceiptDetail;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingReceiptTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class AccountingReceiptTo implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The list accounting receipt detail to. */
	private List<AccountingReceiptDetailTo> listAccountingReceiptDetailTo;
	
	/** The id accounting receipts pk. */
	private Long idAccountingReceiptsPk;
	
  	/** The accounting process. */
	  private AccountingProcess accountingProcess;
	
  	/** The accounting receipt detail. */
	  private List<AccountingReceiptDetail> accountingReceiptDetail;
  	
	/** The number receipt. */
	private String numberReceipt;
	
	/** The receipt amount. */
	private BigDecimal receiptAmount;
  	
	/** The receipt queantity. */
	private Integer receiptQueantity;
  	
	/** The receipt date. */
	private Date receiptDate;

	/** The status. */
	private Integer status; 
	
	/** The gloss. */
	private String gloss;

	/** The list accounting debit. */
	private List<AccountingAccountTo> listAccountingDebit;
	
	/** The list accounting assets. */
	private List<AccountingAccountTo> listAccountingAssets;
	
	/** The list acc accounting debit. */
	private List<AccountingAccount> listAccAccountingDebit;
	
	/** The list acc accounting assets. */
	private List<AccountingAccount> listAccAccountingAssets;
	
	/**
	 * Instantiates a new accounting receipt to.
	 */
	public AccountingReceiptTo() {
		listAccountingDebit=new ArrayList<AccountingAccountTo>();
		listAccountingAssets=new ArrayList<AccountingAccountTo>();
		
		listAccAccountingDebit=new ArrayList<AccountingAccount>();
		listAccAccountingAssets=new ArrayList<AccountingAccount>();
	}

	/**
	 * Gets the list accounting receipt detail to.
	 *
	 * @return the listAccountingReceiptDetailTo
	 */
	public List<AccountingReceiptDetailTo> getListAccountingReceiptDetailTo() {
		return listAccountingReceiptDetailTo;
	}


	/**
	 * Sets the list accounting receipt detail to.
	 *
	 * @param listAccountingReceiptDetailTo the listAccountingReceiptDetailTo to set
	 */
	public void setListAccountingReceiptDetailTo(
			List<AccountingReceiptDetailTo> listAccountingReceiptDetailTo) {
		this.listAccountingReceiptDetailTo = listAccountingReceiptDetailTo;
	}


	/**
	 * Gets the id accounting receipts pk.
	 *
	 * @return the idAccountingReceiptsPk
	 */
	public Long getIdAccountingReceiptsPk() {
		return idAccountingReceiptsPk;
	}


	/**
	 * Sets the id accounting receipts pk.
	 *
	 * @param idAccountingReceiptsPk the idAccountingReceiptsPk to set
	 */
	public void setIdAccountingReceiptsPk(Long idAccountingReceiptsPk) {
		this.idAccountingReceiptsPk = idAccountingReceiptsPk;
	}


	/**
	 * Gets the accounting process.
	 *
	 * @return the accountingProcess
	 */
	public AccountingProcess getAccountingProcess() {
		return accountingProcess;
	}


	/**
	 * Sets the accounting process.
	 *
	 * @param accountingProcess the accountingProcess to set
	 */
	public void setAccountingProcess(AccountingProcess accountingProcess) {
		this.accountingProcess = accountingProcess;
	}


	/**
	 * Gets the accounting receipt detail.
	 *
	 * @return the accountingReceiptDetail
	 */
	public List<AccountingReceiptDetail> getAccountingReceiptDetail() {
		return accountingReceiptDetail;
	}


	/**
	 * Sets the accounting receipt detail.
	 *
	 * @param accountingReceiptDetail the accountingReceiptDetail to set
	 */
	public void setAccountingReceiptDetail(
			List<AccountingReceiptDetail> accountingReceiptDetail) {
		this.accountingReceiptDetail = accountingReceiptDetail;
	}


	/**
	 * Gets the number receipt.
	 *
	 * @return the numberReceipt
	 */
	public String getNumberReceipt() {
		return numberReceipt;
	}


	/**
	 * Sets the number receipt.
	 *
	 * @param numberReceipt the numberReceipt to set
	 */
	public void setNumberReceipt(String numberReceipt) {
		this.numberReceipt = numberReceipt;
	}


	/**
	 * Gets the receipt amount.
	 *
	 * @return the receiptAmount
	 */
	public BigDecimal getReceiptAmount() {
		return receiptAmount;
	}


	/**
	 * Sets the receipt amount.
	 *
	 * @param receiptAmount the receiptAmount to set
	 */
	public void setReceiptAmount(BigDecimal receiptAmount) {
		this.receiptAmount = receiptAmount;
	}


	/**
	 * Gets the receipt queantity.
	 *
	 * @return the receiptQueantity
	 */
	public Integer getReceiptQueantity() {
		return receiptQueantity;
	}


	/**
	 * Sets the receipt queantity.
	 *
	 * @param receiptQueantity the receiptQueantity to set
	 */
	public void setReceiptQueantity(Integer receiptQueantity) {
		this.receiptQueantity = receiptQueantity;
	}


	/**
	 * Gets the receipt date.
	 *
	 * @return the receiptDate
	 */
	public Date getReceiptDate() {
		return receiptDate;
	}


	/**
	 * Sets the receipt date.
	 *
	 * @param receiptDate the receiptDate to set
	 */
	public void setReceiptDate(Date receiptDate) {
		this.receiptDate = receiptDate;
	}


	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}


	/**
	 * Sets the status.
	 *
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * Gets the list accounting debit.
	 *
	 * @return the listAccountingDebit
	 */
	public List<AccountingAccountTo> getListAccountingDebit() {
		return listAccountingDebit;
	}

	/**
	 * Sets the list accounting debit.
	 *
	 * @param listAccountingDebit the listAccountingDebit to set
	 */
	public void setListAccountingDebit(List<AccountingAccountTo> listAccountingDebit) {
		this.listAccountingDebit = listAccountingDebit;
	}

	/**
	 * Gets the list accounting assets.
	 *
	 * @return the listAccountingAssets
	 */
	public List<AccountingAccountTo> getListAccountingAssets() {
		return listAccountingAssets;
	}

	/**
	 * Sets the list accounting assets.
	 *
	 * @param listAccountingAssets the listAccountingAssets to set
	 */
	public void setListAccountingAssets(
			List<AccountingAccountTo> listAccountingAssets) {
		this.listAccountingAssets = listAccountingAssets;
	}

	/**
	 * Gets the gloss.
	 *
	 * @return the gloss
	 */
	public String getGloss() {
		return gloss;
	}

	/**
	 * Sets the gloss.
	 *
	 * @param gloss the gloss to set
	 */
	public void setGloss(String gloss) {
		this.gloss = gloss;
	}

	/**
	 * Gets the list acc accounting debit.
	 *
	 * @return the listAccAccountingDebit
	 */
	public List<AccountingAccount> getListAccAccountingDebit() {
		return listAccAccountingDebit;
	}

	/**
	 * Sets the list acc accounting debit.
	 *
	 * @param listAccAccountingDebit the listAccAccountingDebit to set
	 */
	public void setListAccAccountingDebit(
			List<AccountingAccount> listAccAccountingDebit) {
		this.listAccAccountingDebit = listAccAccountingDebit;
	}

	/**
	 * Gets the list acc accounting assets.
	 *
	 * @return the listAccAccountingAssets
	 */
	public List<AccountingAccount> getListAccAccountingAssets() {
		return listAccAccountingAssets;
	}

	/**
	 * Sets the list acc accounting assets.
	 *
	 * @param listAccAccountingAssets the listAccAccountingAssets to set
	 */
	public void setListAccAccountingAssets(
			List<AccountingAccount> listAccAccountingAssets) {
		this.listAccAccountingAssets = listAccAccountingAssets;
	}



	
}
