package com.pradera.report.generation.executor.securities.to;

import java.io.Serializable;
import java.util.Date;

public class ReassignmentAccountTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** The id issuer pk. */
	//private String idIssuerPk;
	
	/** The id issuer pk. */
	private Integer idParticipantPk;
	
	/** The initial date. */
	private Date initialDate;

	/** The final date. */
	private Date finalDate;
	
	/** The reason reassignment	 */
	private Integer reason;
	
	

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	public Integer getIdParticipantPk() {
		return idParticipantPk;
	}

	public void setIdParticipantPk(Integer idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	public Integer getReason() {
		return reason;
	}

	public void setReason(Integer reason) {
		this.reason = reason;
	}

	
	
	
}
