package com.pradera.report.generation.executor.corporative;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.model.report.ReportLogger;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "CalcStockByValueCorporativeReport")
public class CalcStockByValueCorporativeReport extends GenericReport{
	/**
	 *  
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;
	@Inject
	PraderaLogger log;
	
	
	public CalcStockByValueCorporativeReport() {
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
 
	@Override
	public void addParametersQueryReport() {
		

	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {

		Map<String, Object> parametersRequired = new HashMap<>();
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());

		// TODO Auto-generated method stub
		return parametersRequired;
	}

}
