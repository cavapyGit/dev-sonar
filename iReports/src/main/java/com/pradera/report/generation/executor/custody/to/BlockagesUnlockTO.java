package com.pradera.report.generation.executor.custody.to;

import java.io.Serializable;
import java.util.Date;

public class BlockagesUnlockTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4172964944204487184L;
	
	private Long idHolderAccountPk;
	private Long idHolderPk;
	private Long participantPk;
	private Integer blockType;
	private String securityCode;
	private String securityClass;
	private Integer requestState;
	private String strDateInitial;
	private String strDateEnd;
	private String participantDesc;

	public Long getParticipantPk() {
		return participantPk;
	}
	public void setParticipantPk(Long participantPk) {
		this.participantPk = participantPk;
	}
	public String getParticipantDesc() {
		return participantDesc;
	}
	public void setParticipantDesc(String participantDesc) {
		this.participantDesc = participantDesc;
	}
	public String getSecurityCode() {
		return securityCode;
	}
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
	public String getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(String securityClass) {
		this.securityClass = securityClass;
	}

	public String getStrDateInitial() {
		return strDateInitial;
	}
	public void setStrDateInitial(String strDateInitial) {
		this.strDateInitial = strDateInitial;
	}
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}
	public Long getIdHolderPk() {
		return idHolderPk;
	}
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}
	public Integer getBlockType() {
		return blockType;
	}
	public void setBlockType(Integer blockType) {
		this.blockType = blockType;
	}
	public Integer getRequestState() {
		return requestState;
	}
	public void setRequestState(Integer requestState) {
		this.requestState = requestState;
	}
	public String getStrDateEnd() {
		return strDateEnd;
	}
	public void setStrDateEnd(String strDateEnd) {
		this.strDateEnd = strDateEnd;
	}
	

}
