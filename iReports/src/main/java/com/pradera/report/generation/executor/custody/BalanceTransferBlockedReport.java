package com.pradera.report.generation.executor.custody;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserFilterTO;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "BalanceTransferBlockedReport")
public class BalanceTransferBlockedReport extends GenericReport{
	
	private static final long serialVersionUID = 1L;
	
	@Inject  PraderaLogger log;
		
		@EJB
		private ParameterServiceBean parameterService;
		
		@EJB
		ParticipantServiceBean participantServiceBean;
		
		@Inject
		ClientRestService clientRestService;

		public BalanceTransferBlockedReport() {
		}

		@Override
		public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
			return null;
		}

		@Override
		public void addParametersQueryReport() {
			List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
			if (listaLogger == null) {
				listaLogger = new ArrayList<ReportLoggerDetail>();
			}
		}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> participantMnemonic = new HashMap<Integer, String>();
		Map<Integer,String> request_status = new HashMap<Integer, String>();
		Map<Integer,String> mnemonic_sc = new HashMap<Integer, String>();
		
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		String partOriginCode = null;
		String partOriginDescription = null;
		
		/*String partDestinationCode = null;
		String partDestinationDescription = null;*/
		String isParticipant = null;
		
		UserFilterTO userFilter=new UserFilterTO();
		userFilter.setUserName(getReportLogger().getRegistryUser());

		List<UserAccountSession> lstUserAccount=clientRestService.getUsersInformation(userFilter);
		if(lstUserAccount.size()==GeneralConstants.ONE_VALUE_INTEGER){
			UserAccountSession objUserAccountSession=lstUserAccount.get(0);
			if(objUserAccountSession.getParticipantCode()!=null){
				isParticipant = objUserAccountSession.getParticipantCode().toString();
			}
		}
		//SI ES NULO LO MUESTRO
		parametersRequired.put("isParticipant", isParticipant);
		
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.PARTICIPANT_ORIGIN) && r.getFilterValue()!=null)
				partOriginCode = r.getFilterValue();
			/*if (r.getFilterName().equals("part_destination") && r.getFilterValue()!=null)
				partDestinationCode = r.getFilterValue();*/
		}
		
		try {
			
			for(Participant participant : participantServiceBean.getLisParticipantServiceBean(new Participant())) {
				participantMnemonic.put(participant.getIdParticipantPk().intValue(), participant.getMnemonic());
			}
			
			if(partOriginCode != null){
				Participant participant = participantServiceBean.find(Participant.class, new Long(partOriginCode));
				partOriginDescription = participant.getMnemonic() + " - " + participant.getIdParticipantPk() + " - " + participant.getDescription();
			}
			else{
				partOriginDescription = "TODOS";
			}
			
			/*if(partDestinationCode != null){
				Participant participant = participantServiceBean.find(Participant.class, new Long(partDestinationCode));
				partDestinationDescription = participant.getIdParticipantPk() + " - " + participant.getMnemonic();
			}
			else{
				partDestinationDescription = "Todos";
			}*/
			
			filter.setMasterTableFk(MasterTableType.SECURITY_TRANSFER_STATE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				request_status.put(param.getParameterTablePk(), param.getParameterName());
			}
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				mnemonic_sc.put(param.getParameterTablePk(), param.getText1());
			}
			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		parametersRequired.put("participant_origin_description", partOriginDescription);
		//parametersRequired.put("participant_destination_description", partDestinationDescription);
		parametersRequired.put("p_mnemonic_sc", mnemonic_sc);
		parametersRequired.put("p_request_status", request_status);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());

		return parametersRequired;
	}
	
}
