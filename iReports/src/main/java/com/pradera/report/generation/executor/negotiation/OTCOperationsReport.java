package com.pradera.report.generation.executor.negotiation;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "OTCOperationsReport")
public class OTCOperationsReport extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;
	@Inject
	PraderaLogger log;
	
	@EJB
	ParticipantServiceBean participantServiceBean;

	public OTCOperationsReport() {
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParametersQueryReport() {

	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {

		Map<String, Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> operationCurrency = new HashMap<Integer, String>();
		Map<Integer,String> participantMnemonic = new HashMap<Integer, String>();
		
		try {
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				operationCurrency.put(param.getParameterTablePk(), param.getParameterName());
			}
			
			for(Participant participant : participantServiceBean.getLisParticipantServiceBean(new Participant())) {
				participantMnemonic.put(participant.getIdParticipantPk().intValue(), participant.getMnemonic());
			}
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		parametersRequired.put("p_operation_currency", operationCurrency);
		parametersRequired.put("p_participant_mnemonic", participantMnemonic);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		// TODO Auto-generated method stub
		return parametersRequired;
	}

}
