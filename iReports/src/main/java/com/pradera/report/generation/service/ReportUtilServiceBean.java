package com.pradera.report.generation.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Random;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.sql.DataSource;
import javax.transaction.TransactionSynchronizationRegistry;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.resourceloader.InjectableResource;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSProcessable;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.pradera.commons.audit.UserTrackProcess;
import com.pradera.commons.audit.service.UserAuditServiceBean;
import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.report.ReportFile;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.type.AESKeyType;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.AESEncryptUtils;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.swift.to.FTPParameters;
import com.pradera.core.framework.notification.service.NotificationServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounting.AccountingProcess;
import com.pradera.model.accounts.type.EconomicActivityType;
import com.pradera.model.billing.BillingCalculationProcess;
import com.pradera.model.custody.accreditationcertificates.DigitalSignature;
import com.pradera.model.custody.type.DigitalSignatureType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceSend;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.report.Report;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.model.report.ReportLoggerFile;
import com.pradera.model.report.ReportLoggerUser;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.model.report.type.ReportLoggerStateType;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.util.view.PrintTextLocations;
import com.pradera.report.util.view.SignPosition;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfDate;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfPKCS7;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignature;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfString;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ReportProcessServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/07/2013
 */
@Stateless
@LocalBean
public class ReportUtilServiceBean extends CrudDaoServiceBean {

	/** The log. */
	@Inject
	PraderaLogger log;

	/** The transaction registry. */
	@Resource
	TransactionSynchronizationRegistry transactionRegistry;

	/** The notification service. */
	@EJB 
	NotificationServiceBean notificationService;
	
	/** The user audit service. */
	@EJB
	UserAuditServiceBean userAuditService;
	
	/** The message configuration. */
	@Inject
	@InjectableResource(location = "GenericConstants.properties")
	private Properties messageConfiguration;
	
	/** The data source. */
	@Resource(lookup="java:jboss/datasources/iDepositaryDS")
	DataSource dataSource;
	@EJB
	CustodyReportServiceBean custodyReportServiceBean;
	private FTPParameters ftpParameters = null;

	@Inject @Configurable
	String reportLogo;

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	public void registerReportProcessTx(ReportLogger reportLogger) {
		this.create(reportLogger);
		//register audit report
		registerAuditReport(reportLogger);
	}

	/**
	 * Register audit report.
	 *
	 * @param reportLogger the report logger
	 */
	public void registerAuditReport(ReportLogger reportLogger){
		LoggerUser loggUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggUser!= null && loggUser.getIdUserSessionPk() !=null){
			//only register when process was initiated by user log in
			UserTrackProcess usertTrackProcess = new UserTrackProcess();
			usertTrackProcess.setBusinessMethod("registerReportProcessTx");
			if(reportLogger.getReport() != null){
			usertTrackProcess.setClassName(reportLogger.getReport().getReportBeanName());
			}
			usertTrackProcess.setIdBusinessProcess(BusinessProcessType.REPORT_MONITORING.getCode());
			usertTrackProcess.setIdUserSessionPk(loggUser.getIdUserSessionPk());
			usertTrackProcess.setIdProcessReport(reportLogger.getIdReportLoggerPk());
			if(reportLogger.getReportLoggerDetails()!= null && !reportLogger.getReportLoggerDetails().isEmpty()){
				usertTrackProcess.setParameters(new HashMap<String,String>());
				for(ReportLoggerDetail logDetail : reportLogger.getReportLoggerDetails()){
					usertTrackProcess.getParameters().put(logDetail.getFilterName(), logDetail.getFilterValue());
				}
			}
			if(loggUser.getIdPrivilegeOfSystem()!=null){
				usertTrackProcess.setIdPrivilegeSystem(loggUser.getIdPrivilegeOfSystem());
				usertTrackProcess.setIndError(BooleanType.NO.getCode());
			} else {
				usertTrackProcess.setIdPrivilegeSystem(BooleanType.NO.getCode());
				usertTrackProcess.setIndError(BooleanType.YES.getCode());
			}
			//register tracking
			userAuditService.registerUserTrackProcess(usertTrackProcess);
		}
	}

	/**
	 * Update state report logger tx in new transaction.
	 *
	 * @param reportLogger the report logger
	 * @param state the state
	 * @param errorDetail the error detail
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	public void updateStateReportLoggerTx(ReportLogger reportLogger,Integer state,String errorDetail) {
		ReportLogger logger = em.find(ReportLogger.class, reportLogger.getIdReportLoggerPk());
		logger.setLastModifyDate(CommonsUtilities.currentDateTime());
		logger.setReportState(state);
		if(state.equals(ReportLoggerStateType.FAILED.getCode())) {
			logger.setIndError(BooleanType.YES.getCode());
			if(StringUtils.isNotBlank(errorDetail) && errorDetail.length()>1000){
				errorDetail=errorDetail.substring(0, 1000);
			}
			logger.setErrorDetail(errorDetail);
		}
		try {
			this.update(logger);
		}catch(ServiceException serv){
			log.error(serv.getMessage());
		}
	}

	/**
	 * Gets one instance of entity report logger from 
	 * parameters of generation of report.
	 *
	 * @param reportId the report id
	 * @param parameters the parameters
	 * @param parameterDetails the parameter details
	 * @return the report logger
	 */
	public ReportLogger getReportLogger(Long reportId,Map<String,String> parameters,Map<String,String> parameterDetails) {
		Report report = find(Report.class, reportId);
		ReportLogger reportLogger = null;
		String idAccreditationOperationPk = null;
		if(report !=null){
			reportLogger = new ReportLogger();
			reportLogger.setReport(report);
			reportLogger.setRegistryDate(CommonsUtilities.currentDateTime());
			LoggerUser loggUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
			reportLogger.setRegistryUser(loggUser.getUserName());
			
			/* Duplicidad en la generacion del CAT en archivo fisico */
			
			 if(reportId.equals(ReportIdType.ACCREDITATION_CERTIFICATE.getCode())){
				 for (Entry keyHash2: parameters.entrySet()) {
					 	idAccreditationOperationPk=keyHash2.getValue().toString();
						}
				 String physicalName = "CAT_" + idAccreditationOperationPk + "_" + CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),"ddMMyyyy");
				 reportLogger.setPhysicalName(physicalName);
			 }else{
				 String physicalName = report.getMnemonico()+"_" + CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),"HHmmssSSSSS");
				 reportLogger.setPhysicalName(physicalName);
			 }
			 
			reportLogger.setIndError(BooleanType.NO.getCode());
			reportLogger.setReportState(ReportLoggerStateType.REGISTERED.getCode());

			//if reportLogger is from externalInterface
			if(Validations.validateIsNotNull(parameters)){
				if(parameters.get(ReportConstant.ID_INTERFACE_SEND_PK)!=null){
					//Create a new reference of externalInterface Send
					ExternalInterfaceSend interfaceSend = new ExternalInterfaceSend();
					interfaceSend.setIdExtInterfaceSendPk(Long.parseLong(parameters.get(ReportConstant.ID_INTERFACE_SEND_PK)));
					//Add reference of externalInterfaceSend on reportLogger
					reportLogger.setExternalInterfaceSend(interfaceSend);
				}
			}
			getReportLoggerDetail(reportLogger, parameters, parameterDetails);
		}

		return reportLogger;
	}

	/**
	 * Gets the report logger detail.
	 *
	 * @param reportLogger the report logger
	 * @param parameters the parameters
	 * @param parameterDetails the parameter details
	 * @return the report logger detail
	 */
	public void getReportLoggerDetail(ReportLogger reportLogger,Map<String,String> parameters,Map<String,String> parameterDetails){
		List<ReportLoggerDetail> reportDetails = new ArrayList<ReportLoggerDetail>();
		if(parameters != null && !parameters.isEmpty()){
			for(String key : parameters.keySet()){
				ReportLoggerDetail loggerDetail = new ReportLoggerDetail();
				loggerDetail.setFilterName(key);
				loggerDetail.setFilterValue(parameters.get(key));
				if (parameterDetails!= null && !parameterDetails.isEmpty()){
					loggerDetail.setFilterDescription(parameterDetails.get(key));
				}
				loggerDetail.setIndViewer(BooleanType.YES.getCode());
				loggerDetail.setReportLogger(reportLogger);
				reportDetails.add(loggerDetail);
			}
		}
		reportLogger.setReportLoggerDetails(reportDetails);
	}

	/**
	 * Save report logger users.
	 *
	 * @param reportLogger the report logger
	 * @param listUsers the list users
	 */
	public void saveReportLoggerUsers(ReportLogger reportLogger,List<String> listUsers){
		List<ReportLoggerUser> loggerUsers = new ArrayList<ReportLoggerUser>();
		for(String userName : listUsers) {
			ReportLoggerUser loggUser = new ReportLoggerUser();
			loggUser.setAccessUser(userName);
			loggUser.setAssignedDate(CommonsUtilities.currentDateTime());
			loggUser.setReportLogger(reportLogger);
			loggerUsers.add(loggUser);
		}
		saveAll(loggerUsers);
	}

	/**
	 * Save report logger users tx in new Transaction.
	 *
	 * @param reportLogger the report logger
	 * @param listUsers the list users
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	public void saveReportLoggerUsersTx(ReportLogger reportLogger,List<String> listUsers){
		this.saveReportLoggerUsers(reportLogger, listUsers);
	}

	/**
	 * Gets the xml data source report.
	 *
	 * @param reportLogger the report logger
	 * @return the xml data source report
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public String getXmlDataSourceReport(ReportLogger reportLogger) throws IOException{
		//TODO old implementation get xml from database
//		Query query = em.createQuery("select r.fileXml from ReportLogger r  where r.idReportLoggerPk = :reportId");
//		query.setParameter("reportId", reportLogger.getIdReportLoggerPk());
//		Object file = query.getSingleResult();
//		String separator = System.getProperty("file.separator");
//		String basePath = System.getProperty("java.io.tmpdir");
//		String uniqueName = basePath+separator+reportLogger.getPhysicalName()
//				+ CommonsUtilities.convertDateToString(CommonsUtilities.getDefaultDateWithCurrentTime(), "mmssSSS")+".xml";
//		Files.write(Paths.get(uniqueName), (byte[])file, StandardOpenOption.CREATE_NEW);
//		//if dont save xml  should catch exception?
		String separator = System.getProperty("file.separator");
		String basePath = System.getProperty("java.io.tmpdir");
		ReportLogger loggerFile = em.find(ReportLogger.class, reportLogger.getIdReportLoggerPk());
		return basePath+separator+loggerFile.getPhysicalName()+".xml";
	}

	/**
	 * Save report files.
	 *
	 * @param files the files
	 * @param reportLogger the report logger
	 */
	
	/**
	 * Save custom file and asociate with reportLogger.
	 * Guarda los algunos reportes, ej. Carteras valoradas FCC, FCI etc
	 * @param fileName the file name
	 * @param fileType the file type
	 * @param file the file
	 * @param reportLogger the report logger
	 */
	public void saveCustomReportFile(String fileName,Integer fileType,byte[] file,ReportLogger reportLogger){
		if(StringUtils.isNotBlank(fileName) && file != null && file.length >0){
			ReportLoggerFile loggerFile = new ReportLoggerFile();
			loggerFile.setNameFile(fileName);
			loggerFile.setFileType(fileType);
			loggerFile.setDataReport(file);
			loggerFile.setReportLogger(reportLogger);
			loggerFile.setLastModifyApp(reportLogger.getLastModifyApp());
			loggerFile.setLastModifyDate(reportLogger.getLastModifyDate());
			loggerFile.setLastModifyIp(reportLogger.getLastModifyIp());
			loggerFile.setLastModifyUser(reportLogger.getLastModifyUser());
//			if(!file.equals(null)){//almacenamiento en disco 
//				loggerFile.setNameTrace(saveReportToDisk(file, reportLogger.getRegistryUser(), fileName, null, null));
//			}
			create(loggerFile);
		}
	}

	/**
	 * Save custom file and asociate with reportLogger.
	 * Guarda los algunos reportes, ej. Carteras valoradas FCC, FCI etc
	 * @param fileName the file name
	 * @param fileType the file type
	 * @param file the file
	 * @param reportLogger the report logger
	 */
	public void saveCustomReportFileAsfi(String fileName,Integer fileType,byte[] file,ReportLogger reportLogger){
		if(StringUtils.isNotBlank(fileName) && file != null && file.length >0){
			ReportLoggerFile loggerFile = new ReportLoggerFile();
			loggerFile.setNameFile(fileName);
			loggerFile.setFileType(fileType);
			loggerFile.setDataReport(file);
			loggerFile.setReportLogger(reportLogger);
			loggerFile.setLastModifyApp(reportLogger.getLastModifyApp());
			loggerFile.setLastModifyDate(reportLogger.getLastModifyDate());
			loggerFile.setLastModifyIp(reportLogger.getLastModifyIp());
			loggerFile.setLastModifyUser(reportLogger.getLastModifyUser());
//			if(!file.equals(null)){//almacenamiento en disco 
//				loggerFile.setNameTrace(saveReportToDisk(file, reportLogger.getRegistryUser(), fileName, null, null));
//			}
			create(loggerFile);
			
			ReportLoggerUser loggerUser = new ReportLoggerUser();
			loggerUser.setAccessUser(reportLogger.getLastModifyUser());
			loggerUser.setAssignedDate(CommonsUtilities.currentDateTime());
			loggerUser.setReportLogger(reportLogger);
			loggerUser.setLastModifyApp(reportLogger.getLastModifyApp());
			loggerUser.setLastModifyDate(reportLogger.getLastModifyDate());
			loggerUser.setLastModifyIp(reportLogger.getLastModifyIp());
			loggerUser.setLastModifyUser(reportLogger.getLastModifyUser());
			
			create(loggerUser);
		}
	}
	
	/**
	 * Save report files.
	 *
	 * @param files the files
	 * @param reportLogger the report logger
	 */
	public void saveReportFiles(List<ReportFile> files,ReportLogger reportLogger) {
		List<ReportLoggerFile> loggerFiles = new ArrayList<ReportLoggerFile>();
		for (ReportFile reportFile : files) {
			ReportLoggerFile loggerFile = new ReportLoggerFile();
			if(reportFile.getPhysicalName()!=null){
				loggerFile.setNameFile(reportFile.getPhysicalName()+"."+ReportFormatType.get(reportFile.getReportType()).getValue());
			}else{
				loggerFile.setNameFile(reportLogger.getPhysicalName()+"."+ReportFormatType.get(reportFile.getReportType()).getValue());
			}
			loggerFile.setFileType(reportFile.getReportType());
			loggerFile.setDataReport(reportFile.getFile());
			loggerFile.setReportLogger(reportLogger);
			loggerFiles.add(loggerFile);
		}
		saveAll(loggerFiles);
	}
	
	/**
	 * Firma digital en CAT
	 * @param personSignature
	 * @param fileContent
	 * @param xOne
	 * @param yOne
	 * @param xTwo
	 * @param yTwo
	 * @param fieldName
	 * @param reason
	 * @param location
	 * @return
	 */
	public byte[] setDigitallySign(Integer personSignature,byte[] fileContent,int xOne, int yOne){
		String passwordPKCS12=null;
			try {
				KeyStore ks = KeyStore.getInstance("pkcs12");
				List<DigitalSignature> lstDigitalSignature = custodyReportServiceBean.getListDigitalSignatureBlob(personSignature);
				byte[] digitalCertificateFirst = null;
				for (DigitalSignature digitalSignature : lstDigitalSignature) {
					digitalCertificateFirst=digitalSignature.getSignature();
					try {
						passwordPKCS12=AESEncryptUtils.decrypt(AESKeyType.PRADERAKEY.getValue(), digitalSignature.getPasswordSignature());
					} catch (Exception e) {
						e.printStackTrace();		
						log.error(e.getMessage());
					}
				}
				if(!Validations.validateIsNotNullAndNotEmpty(digitalCertificateFirst))
					return null;
				
				ByteArrayInputStream arrayInputStream=new ByteArrayInputStream(digitalCertificateFirst);
				ks.load(arrayInputStream, passwordPKCS12.toCharArray());
				String alias = (String)ks.aliases().nextElement();
				PrivateKey key = (PrivateKey)ks.getKey(alias, passwordPKCS12.toCharArray());
				Certificate[] chain = ks.getCertificateChain(alias);
				
				Security.addProvider(new BouncyCastleProvider());
				
				PdfReader pdfReader = new PdfReader(new ByteArrayInputStream(fileContent));
				ByteArrayOutputStream fout=new ByteArrayOutputStream();
				
				com.itextpdf.text.pdf.PdfStamper pdfStamper;
				pdfStamper = PdfStamper.createSignature(pdfReader, fout, '\0',null,true);
				PdfSignatureAppearance sap = pdfStamper.getSignatureAppearance();
				
				
				InputStream inputIO = this.getClass().getResourceAsStream(reportLogo);
				byte[] bytes = IOUtils.toByteArray(inputIO);
                Image image = Image.getInstance(bytes);
		        image.scaleToFit(40,40);
		        image.setAbsolutePosition(xOne-50, yOne+10);
		        PdfContentByte contentByte = pdfStamper.getOverContent(pdfReader.getNumberOfPages());
		        contentByte.addImage(image);
		        BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA_BOLD, BaseFont.WINANSI, BaseFont.EMBEDDED);
		        contentByte.saveState();
		        contentByte.beginText();
		        contentByte.setColorFill(new BaseColor(128,128,128));
		        contentByte.setFontAndSize(bf, 5); 
		        String signName = PdfPKCS7.getSubjectFields((X509Certificate) chain[0]).getField("CN");
				String office = PdfPKCS7.getSubjectFields((X509Certificate) chain[0]).getField("T");
		        String dateString = CommonsUtilities.convertDateToString(CommonsUtilities.currentDate(),CommonsUtilities.DATE_PATTERN);
		        contentByte.showTextAligned(Element.ALIGN_LEFT,signName, xOne, yOne+25, 0f);
		        contentByte.showTextAligned(Element.ALIGN_LEFT,office, xOne, yOne+20, 0f);
		        contentByte.showTextAligned(Element.ALIGN_LEFT,dateString, xOne, yOne+15, 0f);
		        contentByte.showTextAligned(Element.ALIGN_LEFT,"La Paz - Bolivia", xOne, yOne+10, 0f);
		        contentByte.saveState();
		        contentByte.endText();
		        contentByte.restoreState();
				sap.setSignDate(new GregorianCalendar());
				sap.setCrypto(null, chain, null, null);
				sap.setAcro6Layers(true);
				PdfSignature dic = new PdfSignature(PdfName.ADOBE_PPKLITE, PdfName.ADBE_PKCS7_DETACHED);
				dic.setDate(new PdfDate(sap.getSignDate()));
				dic.setName(PdfPKCS7.getSubjectFields((X509Certificate) chain[0]).getField("CN"));
				if (sap.getReason() != null) {
					dic.setReason(sap.getReason());
				}
				if (sap.getLocation() != null) {
					dic.setLocation(sap.getLocation());
				}
				sap.setCryptoDictionary(dic);
				int csize = 4000;
				HashMap exc = new HashMap();
				exc.put(PdfName.CONTENTS, new Integer(csize * 2 + 2));
				sap.preClose(exc);

				CMSSignedDataGenerator generator = new CMSSignedDataGenerator();
				generator.addSigner(key, (X509Certificate) chain[0], CMSSignedDataGenerator.DIGEST_SHA256);

				ArrayList listCert = new ArrayList();
				for (int i = 0; i < chain.length; i++) {
					listCert.add(chain[i]);
				}
				CertStore chainStore;
				try {
					chainStore = CertStore.getInstance("Collection", new CollectionCertStoreParameters(listCert),
							"BC");
					generator.addCertificatesAndCRLs(chainStore);
					CMSProcessable content = new CMSProcessableRange(sap);
					CMSSignedData signedData = generator.generate(content, false, "BC");
					byte[] pk = signedData.getEncoded();

					byte[] outc = new byte[csize];

					PdfDictionary dic2 = new PdfDictionary();

					System.arraycopy(pk, 0, outc, 0, pk.length);
					dic2.put(PdfName.CONTENTS, new PdfString(outc).setHexWriting(true));
					sap.close(dic2);
				} catch (InvalidAlgorithmParameterException | NoSuchProviderException | CertStoreException | CMSException e) {
					e.printStackTrace();
				}
				//pdfStamper.close();
				byte[] data = fout.toByteArray();
				return data;
			} catch (IOException e) {
				e.printStackTrace();
				log.error(e.getMessage());
			} catch (KeyStoreException e) {
				e.printStackTrace();
				log.error(e.getMessage());
			} catch (ServiceException e) {
				e.printStackTrace();
				log.error(e.getMessage());
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
				log.error(e.getMessage());
			} catch (CertificateException e) {
				e.printStackTrace();
				log.error(e.getMessage());
			} catch (UnrecoverableKeyException e) {
				e.printStackTrace();
				log.error(e.getMessage());
			} catch (DocumentException e) {
				e.printStackTrace();
				log.error(e.getMessage());
			}
			return null;
	}
	
	private void singReport() {
		
	}
	/**
	 * Metodo para guardar la mayor parte de los reportes
	 * @param files
	 * @param reportLogger
	 */
	public void saveReportFiles(Map<ReportFormatType,byte[]> files,ReportLogger reportLogger) {
		List<ReportLoggerFile> loggerFiles = new ArrayList<ReportLoggerFile>();
		for(Entry<ReportFormatType,byte[]> fileGenerate :files.entrySet()) {
			ReportLoggerFile loggerFile = new ReportLoggerFile();
			loggerFile.setNameFile(reportLogger.getPhysicalName()+"."+fileGenerate.getKey().getValue());
			loggerFile.setFileType(fileGenerate.getKey().getCode());
			//loggerFile.setDataReport(fileGenerate.getValue());
			loggerFile.setDataReportLength(CommonsUtilities.getByteArgLenghtMB(fileGenerate.getValue()));
			loggerFile.setReportLogger(reportLogger);
			loggerFiles.add(loggerFile);
			if(!fileGenerate.getValue().equals(null)){//almacenamiento en disco
				 loggerFile.setNameTrace(saveReportToDisk(fileGenerate.getValue(),reportLogger.getRegistryUser(),reportLogger.getPhysicalName(),fileGenerate.getKey().getValue(),loggerFile.getReportLogger().getReport().getIdReportPk()));
			}
		}
		saveAll(loggerFiles);
	}

	/**
	 * Method to save report generate to disk
	 * @param User
	 * @param archive
	 * @param nameFile
	 * @param extension
	 * @return
	 */
	public String saveReportToDisk (byte[] file, String user,String nameFile, String extension,Long reportId){
		
		String year=CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),"yyyy");
		String month=CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),"MM");
		String day=CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),"dd");
		String separator=File.separator;
		Integer rutePt = 2351; //id parameter_Table
		
		ParameterTable objParameterTable=new ParameterTable();
		objParameterTable = find(ParameterTable.class, rutePt);
		String rute=objParameterTable.getText1();
		try{
 			 //rute=("D:/reportes/"); //comentar en produccion
			 //creando las carpetas
			 File directory=new File(rute+year+separator+month+separator+day+separator+user);
			 FileUtils.forceMkdir(directory);
			 rute=directory.toString()+separator;
			 //firmar si es CAT //REDMINE 765 se remueve firma digital
//			 if(reportId!=null){
//				 if(reportId.equals(ReportIdType.ACCREDITATION_CERTIFICATE.getCode()) // CAT NORMAL
//						 || reportId.equals(287L) //CAT PGS
//						 || reportId.equals(288L) //CAT B
//						 || reportId.equals(289L) ){ // CAT B PGS
//						SignPosition position = null;
//						try {
//							position= new PrintTextLocations().getPosition(file);
//						} catch (IOException e) {
//							e.printStackTrace();
//							log.error(e.getMessage());
//						} catch (InterruptedException e) {
//							e.printStackTrace();
//							log.error(e.getMessage());
//						}
//					    if (position!=null && position.getX().intValue()!=0){
//					    	int x=position.getX().intValue();
//						    int y=position.getY().intValue();
//						    file=(setDigitallySign(DigitalSignatureType.FIRST_P12_CONTAINER_SIGNATURE.getCode(), file,x+78 ,y+44 ));
//						    file=(setDigitallySign(DigitalSignatureType.SECOND_P12_CONTAINER_SIGNATURE.getCode(), file,x+318, y+44));
//					    }
//					}
//			 }
			 byte[] arrayByte = file;
			   InputStream inputStream = new ByteArrayInputStream(arrayByte);	  
			   //creando el archivo
			   File archive = null;
			   if(extension!=null){
				   archive=new File(rute+nameFile+"."+extension);
			   }else{
				   archive=new File(rute+nameFile);
			   }
			   OutputStream salida=new FileOutputStream(archive);
			   byte[] buf =new byte[1024];
			Integer count;
			   while((count=inputStream.read(buf))>0){
			      salida.write(buf,0,count);
			   }
			   salida.close();
			   inputStream.close();
			   log.info("REPORTES::Almacenado correctamente "+rute+nameFile);
			   log.info("Almacenado correctamente "+rute+nameFile);
			  }catch(IOException e){
			    log.info("Se produjo el error : "+e.toString());
			  }
		 log.info("RUTA EN EL NAMETRACE "+rute);
	
		return rute;
	}
	
	/**
	 * Save reports generates tx.
	 *
	 * @param reportLogger the report logger
	 * @param files the files
	 * @throws FileNotFoundException 
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	public void saveReportsGeneratesTx(ReportLogger reportLogger,Map<ReportFormatType,byte[]> files) throws FileNotFoundException {
		LoggerUser loggUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggUser.setAuditTime(CommonsUtilities.currentDateTime());
		saveReportFiles(files,reportLogger);
	}

	/**
	 * Save result xml.
	 *
	 * @param reportLogger the report logger
	 * @param byteArray the byte array
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void saveResultXml(ReportLogger reportLogger,ByteArrayOutputStream byteArray) throws IOException{
		try{
			//TODO data xml is not save in db
//			ReportLogger loggerFile = em.find(ReportLogger.class, reportLogger.getIdReportLoggerPk());
//			loggerFile.setFileXml(byteArray.toByteArray());
//			loggerFile.setLastModifyDate(CommonsUtilities.currentDateTime());
//			update(loggerFile);
			String separator = System.getProperty("file.separator");
			String basePath = System.getProperty("java.io.tmpdir");
			Random random = new Random();
			String physicalName = reportLogger.getPhysicalName() + (random.nextInt(100)+1)
					+ CommonsUtilities.convertDateToString(CommonsUtilities.getDefaultDateWithCurrentTime(), "mmssSSS");
			String uniqueName = basePath+separator+ physicalName + ".xml";
			Files.write(Paths.get(uniqueName), byteArray.toByteArray(), StandardOpenOption.CREATE_NEW);
			ReportLogger loggerFile = em.find(ReportLogger.class, reportLogger.getIdReportLoggerPk());
			loggerFile.setPhysicalName(physicalName);
			loggerFile.setLastModifyDate(CommonsUtilities.currentDateTime());
			update(loggerFile);
		} catch(ServiceException sexe){
			log.error(sexe.getMessage());
		}
	}

	/**
	 * Send report notification.
	 *
	 * @param reportLogger the report logger
	 * @param loggerUser the logger user
	 */
	public void sendReportNotification(ReportLogger reportLogger,LoggerUser loggerUser){
		ReportLogger reportFinish = em.find(ReportLogger.class, reportLogger.getIdReportLoggerPk());
		if(reportFinish.getIndSendNotification().equals(BooleanType.YES.getCode())
				&&  reportFinish.getReport().getIdepositaryModule() != null){
			Long idBusinesProcess = reportFinish.getReport().getIdepositaryModule().getIdBusinessProcessPk();
			BusinessProcess businessProc = new BusinessProcess();
			businessProc.setIdBusinessProcessPk(idBusinesProcess);
			List<UserAccountSession> userSession = new ArrayList<>();
			for(ReportLoggerUser reportUser : reportFinish.getReportLoggerUsers()){
				UserAccountSession user = new UserAccountSession();
				user.setUserName(reportUser.getAccessUser());
				userSession.add(user);
			}
			String subject = messageConfiguration.getProperty("notification.report.subject");
			String message = messageConfiguration.getProperty("notification.report.message");
			message = MessageFormat.format(message, reportFinish.getReport().getName());
			notificationService.sendNotificationManual(loggerUser, loggerUser.getUserName(), businessProc, userSession
					, subject, message, NotificationType.REPORT);
		}
	}
	
	/**
	 * Gets the connectioni depositary.
	 *
	 * @return the connectioni depositary
	 */
	public Connection getConnectioniDepositary(){
		try{
		return dataSource.getConnection();
		} catch(SQLException sex){
			log.error(sex.getMessage());
			return null;
		}
	}


	/**
	 * Default constructor. 
	 */
	public ReportUtilServiceBean() {
		// TODO Auto-generated constructor stub
	}
	
	/***
	 * Save File XML in BILLING_FILE_EXPORT and 
	 * 	Send File to FTP 
	 * @param byteArray
	 * @throws IOException
	 */
	public void saveFileXMlInterfaceBilling(ByteArrayOutputStream byteArray, Long processBillingCalculationPk, ReportLogger reportLogger) throws IOException{
		try{
			ftpParameters = new FTPParameters();
			ParameterTable parameter=find(ParameterTable.class, 2135);
			String nameFile=ReportConstant.TAG_CXC.concat(CommonsUtilities.convertDatetoStringForTemplate(
								CommonsUtilities.currentDateTime(), CommonsUtilities.DATETIME_HOURS_PATTERN)).concat(GeneralConstants.XML_FORMAT);

			
			BillingCalculationProcess billingCalculationProcess = em.find(BillingCalculationProcess.class, processBillingCalculationPk);
			billingCalculationProcess.setLastModifyDate(CommonsUtilities.currentDateTime());
			billingCalculationProcess.setLastModifyUser(reportLogger.getRegistryUser());
			billingCalculationProcess.setLastModifyApp(reportLogger.getLastModifyApp());
			billingCalculationProcess.setLastModifyIp(reportLogger.getLastModifyIp());
			billingCalculationProcess.setFileXml(byteArray.toByteArray());
			billingCalculationProcess.setNameFile("HC"+nameFile);
			update(billingCalculationProcess);
			
			/**Change Temp Technique To save FTP*/
//			File file =new File(nameFile);
//			FileUtils.writeByteArrayToFile(file, byteArray.toByteArray());
//			ftpParameters.setServer(parameter.getText3());
//			ftpParameters.setUser(parameter.getText1());
//			ftpParameters.setPass(parameter.getText2());
//			ftpParameters.setRemotePath(parameter.getDescription());
//			ftpParameters.setTempInterfaceFile(file);
//			
//			UploadFtp upload= new UploadFtp(ftpParameters);
//			upload.registerAndSendFile(nameFile);

		} catch(ServiceException ex){
			log.error(ex.getMessage());
		}
	}
	
	/***
	 * Save File XML in ACCOUNTING_FILE_EXPORT and 
	 * 	Send File to FTP 
	 * @param byteArray
	 * @throws IOException
	 */
	public void saveFileXMlInterfaceAccounting(ByteArrayOutputStream byteArray, Long processAccountingPk, ReportLogger reportLogger) throws IOException{
		try{
			ftpParameters = new FTPParameters();
			ParameterTable parameter=find(ParameterTable.class, 2135);
			String nameFile=ReportConstant.TAG_ERP.concat(CommonsUtilities.convertDatetoStringForTemplate(
								CommonsUtilities.currentDateTime(), CommonsUtilities.DATETIME_HOURS_PATTERN)).concat(GeneralConstants.XML_FORMAT);

			
			AccountingProcess accountingProcess = em.find(AccountingProcess.class, processAccountingPk);
			accountingProcess.setLastModifyDate(CommonsUtilities.currentDateTime());
			accountingProcess.setLastModifyUser(reportLogger.getRegistryUser());
			accountingProcess.setLastModifyApp(reportLogger.getLastModifyApp());
			accountingProcess.setLastModifyIp(reportLogger.getLastModifyIp());
			accountingProcess.setFileXml(byteArray.toByteArray());
			accountingProcess.setNameFile(nameFile);
			update(accountingProcess);

			/**Change Temp Technique To save FTP*/
//			File file =new File(nameFile);
//			FileUtils.writeByteArrayToFile(file, byteArray.toByteArray());
//			ftpParameters.setServer(parameter.getText3());
//			ftpParameters.setUser(parameter.getText1());
//			ftpParameters.setPass(parameter.getText2());
//			ftpParameters.setRemotePath(parameter.getDescription());
//			ftpParameters.setTempInterfaceFile(file);
//			
//			UploadFtp upload= new UploadFtp(ftpParameters);
//			upload.registerAndSendFile(nameFile);

		} catch(ServiceException ex){
			log.error(ex.getMessage());
		}
	}
	class ObjetDigitalSignature{
		private byte[] digitalCertificateFirst;
		private String password;
		public byte[] getDigitalCertificateFirst() {
			return digitalCertificateFirst;
		}
		public void setDigitalCertificateFirst(byte[] digitalCertificateFirst) {
			this.digitalCertificateFirst = digitalCertificateFirst;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		
	}
	
	/**
	 * Retorna el equivalente de una moneda SAN en su equivalente ASFI
	 * */
	public String getCurrencyAsfi(Integer currency){
		if(currency.equals(CurrencyType.PYG.getCode()))
			return "10";
		if(currency.equals(CurrencyType.DMV.getCode()))
			return "11";
		if(currency.equals(CurrencyType.USD.getCode()))
			return "12";
		if(currency.equals(CurrencyType.UFV.getCode()))
			return "14";
		if(currency.equals(CurrencyType.EU.getCode()))
			return "15";
		return null;
		
	}
	/**
	 * Retorna el nemonico de una actividad economica del SAN en su equivalente ASFI
	 * 
	 * */
	public String getEcomicActivityAsfi(Integer economicActivity){
		if(EconomicActivityType.ADMINISTRACION_CENTRAL.getCode().equals(economicActivity))
			return "AYG";
		if(EconomicActivityType.ADMINISTRADORAS_FONDOS_PENSIONES.getCode().equals(economicActivity))
			return "AFP";
		if(EconomicActivityType.AGENCIAS_BOLSA.getCode().equals(economicActivity))
			return "AGE";	
		if(EconomicActivityType.MUNICIPIOS_GOBIERNOS_LOCALES.getCode().equals(economicActivity))
			return "EGA";
		if(EconomicActivityType.BANCOS.getCode().equals(economicActivity))
			return "BAN";
		if(EconomicActivityType.BANCOS_MULTIPLES.getCode().equals(economicActivity))
			return "MIN";
		if(EconomicActivityType.BANCOS_AHORRO_CREDITOS_FOMENTO.getCode().equals(economicActivity))
			return "MAN";
		if(EconomicActivityType.COMPANIAS_SEGURO.getCode().equals(economicActivity))
			return "SEG";
		if(EconomicActivityType.ASOCIACIONES_AHORROS_PRESTAMOS.getCode().equals(economicActivity))
			return "OSF";
		if(EconomicActivityType.COOPERATIVAS.getCode().equals(economicActivity))
			return "COP";
		if(EconomicActivityType.ENTIDADES_PUBLICAS_INTERMEDIACION_FINANCIERA.getCode().equals(economicActivity))
			return "NGF";
		if(EconomicActivityType.RESTO_SOCIEDADES_DEPOSITOS.getCode().equals(economicActivity))
			return "TYC";
		if(EconomicActivityType.CORPORACIONES_CREDITO_FINANCIERAS.getCode().equals(economicActivity))
			return "HYR";
		if(EconomicActivityType.AFP.getCode().equals(economicActivity))
			return "COM";
		if(EconomicActivityType.FONDOS_INVERSION_ABIERTOS.getCode().equals(economicActivity))
			return "FIA";
		if(EconomicActivityType.FONDOS_INVERSION_CERRADOS.getCode().equals(economicActivity))
			return "FIC";
		if(EconomicActivityType.FONDOS_INVERSION_MIXTOS.getCode().equals(economicActivity))
			return "FIM";
		if(EconomicActivityType.FONDOS_FINANCIEROS_PRIVADOS.getCode().equals(economicActivity))
			return "OSF";
		if(EconomicActivityType.EMPRESAS_PRIVADAS.getCode().equals(economicActivity))
			return "MAN";
		if(EconomicActivityType.HOGARES_MICROEMPRESAS.getCode().equals(economicActivity))
			return "OTR";
		if(EconomicActivityType.MUTUALES.getCode().equals(economicActivity))
			return "OSF";
		if(EconomicActivityType.ORGANISMO_NO_GUBERNAMENTAL_NTERMEDIACION_FINANCIERA.getCode().equals(economicActivity))
			return "NGF";
		if(EconomicActivityType.SECTOR_PUBLICO_NO_FINANCIERO.getCode().equals(economicActivity))
			return "OSF";
		if(EconomicActivityType.SECTOR_FINANCIERO.getCode().equals(economicActivity))
			return "TYC";
		return "OSF";
	}
}
