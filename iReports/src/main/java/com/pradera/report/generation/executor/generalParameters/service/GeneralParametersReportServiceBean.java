package com.pradera.report.generation.executor.generalParameters.service;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.type.DematerializationCertificateMotiveType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.report.generation.executor.issuances.to.GeneralListOfIssuerReportTO;
import com.pradera.report.generation.executor.issuances.to.GenericIssuanceTO;
import com.pradera.report.generation.executor.issuances.to.RelationshipOfBuyersOfBankSharesTO;
import com.pradera.report.generation.executor.issuances.to.SecuritiesDematerializationTO;
import com.pradera.report.generation.executor.issuances.to.StockOfSecuritiesByBookEntryTO;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class AccountReportServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 03/10/2013
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class GeneralParametersReportServiceBean extends CrudDaoServiceBean {

    /**
     * Default constructor. 
     */
    public GeneralParametersReportServiceBean() {
        // TODO Auto-generated constructor stub
    }
    
    public String getLastAnnaCorrelative(){
		StringBuilder sqlQuery = new StringBuilder(); 
		sqlQuery.append("SELECT DISTINCT TRIM(TO_CHAR((NVL(MAX(ID_CORRELATIVE_ANNA_CODE),0) + 1),'9999,000000')) AS CORRELAT	"); 
		sqlQuery.append("FROM SECURITY_INTERFACE_ANNA																			");
		Query query = em.createNativeQuery(sqlQuery.toString());
		return (String) query.getSingleResult();
	}
    
    public List<Object[]> getQueryListByClass(String strQuery) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(strQuery);
		Query query = em.createNativeQuery(sbQuery.toString());
		return query.getResultList();
	}
}
