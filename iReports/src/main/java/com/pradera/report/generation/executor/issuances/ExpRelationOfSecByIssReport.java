package com.pradera.report.generation.executor.issuances;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.securities.service.PaymentChronogramBySecurity;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;

@ReportProcess(name="ExpRelationOfSecByIssReport")
public class ExpRelationOfSecByIssReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private PaymentChronogramBySecurity securityService;
	@Inject
	private PraderaLogger log;
	@EJB
	private ParameterServiceBean parameterService;
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		GenericsFiltersTO secByIssuerTO = new GenericsFiltersTO();

		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("cui_holder")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					secByIssuerTO.setCui(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("holder_account")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					secByIssuerTO.setIdHolderAccountPk(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("issuer_code")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					secByIssuerTO.setIdIssuer(listaLogger.get(i).getFilterValue());
					secByIssuerTO.setIssuerDesc(listaLogger.get(i).getFilterDescription());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("security_code_only")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					secByIssuerTO.setIdSecurityCodePk(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("bcb_format")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					secByIssuerTO.setIndBCB(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("date_initial")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					secByIssuerTO.setInitialDt(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("date_end")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					secByIssuerTO.setFinalDt(listaLogger.get(i).getFilterValue());
				}
			}
		}
		
		String strQueryFormatted=null;
		strQueryFormatted = securityService.getExpRelatOfSecByIssuer(secByIssuerTO);

		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> secClassMnemonic = new HashMap<Integer, String>();
		Map<Integer,String> secClassDesc = new HashMap<Integer, String>();
		Map<Integer,String> secCurrency = new HashMap<Integer, String>();
		Map<Integer,String> secCurrencyDesc = new HashMap<Integer, String>();
		Map<Integer,String> docTypeShort = new HashMap<Integer, String>();
		Map<Integer,String> blockType = new HashMap<Integer, String>();
		try {
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secClassMnemonic.put(param.getParameterTablePk(), param.getText1());
			}
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secClassDesc.put(param.getParameterTablePk(), param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secCurrency.put(param.getParameterTablePk(), param.getDescription());
			}
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secCurrencyDesc.put(param.getParameterTablePk(), param.getParameterName());
			}
			filter.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				docTypeShort.put(param.getParameterTablePk(), param.getIndicator1());
			}
			filter.setMasterTableFk(MasterTableType.TYPE_AFFECTATION.getCode());//block type?
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				blockType.put(param.getParameterTablePk(), param.getDescription());
			}
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		parametersRequired.put("str_query", strQueryFormatted);
		parametersRequired.put("p_secClassMnemonic", secClassMnemonic);
		parametersRequired.put("p_secClassDesc", secClassDesc);
		parametersRequired.put("p_secCurrency", secCurrency);
		parametersRequired.put("p_secCurrencyDesc", secCurrencyDesc);
		parametersRequired.put("p_docTypeShort", docTypeShort);
		parametersRequired.put("p_blockType", blockType);
		parametersRequired.put("p_businessName", secByIssuerTO.getIssuerDesc());
		
		return parametersRequired;
	}
	

}
