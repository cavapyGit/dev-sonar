package com.pradera.report.generation.executor.custody.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class VaultControlReportPositionsTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String ubicacion;
	
	private Date initialDate;
	
	private Date finalDate;
	
	private String emisor;
	
	private Integer moneda;
	
	private String valor;
	
	private Integer participant;
	
	private Integer holder;
	
	private String  cabinet;
	
	private String level;
	
	private String box;
	
	private String folio;
	
	private String boxIndex;
	
	private String securityClass;
	
	private String operationType;
	
	private String entryDate;
	
	private String expirationDate;
	
	private String holderDescription;
	
	private String participantDescription;
	
	private String accountNumber;
	
	private BigDecimal nominalValue;
	
	private String dateFilterType;
	
	private String rut;
	
	private BigDecimal quantity;
	
	private BigDecimal since;
	
	private BigDecimal from;
	
	private Date courtDate;

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	public String getEmisor() {
		return emisor;
	}

	public void setEmisor(String emisor) {
		this.emisor = emisor;
	}

	public Integer getMoneda() {
		return moneda;
	}

	public void setMoneda(Integer moneda) {
		this.moneda = moneda;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public Integer getParticipant() {
		return participant;
	}

	public void setParticipant(Integer participant) {
		this.participant = participant;
	}

	public Integer getHolder() {
		return holder;
	}

	public void setHolder(Integer holder) {
		this.holder = holder;
	}

	public String getCabinet() {
		return cabinet;
	}

	public void setCabinet(String cabinet) {
		this.cabinet = cabinet;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getBox() {
		return box;
	}

	public void setBox(String box) {
		this.box = box;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getBoxIndex() {
		return boxIndex;
	}

	public void setBoxIndex(String boxIndex) {
		this.boxIndex = boxIndex;
	}

	public String getSecurityClass() {
		return securityClass;
	}

	public void setSecurityClass(String securityClass) {
		this.securityClass = securityClass;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public String getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getHolderDescription() {
		return holderDescription;
	}

	public void setHolderDescription(String holderDescription) {
		this.holderDescription = holderDescription;
	}

	public String getParticipantDescription() {
		return participantDescription;
	}

	public void setParticipantDescription(String participantDescription) {
		this.participantDescription = participantDescription;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public BigDecimal getNominalValue() {
		return nominalValue;
	}

	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}

	public String getDateFilterType() {
		return dateFilterType;
	}

	public void setDateFilterType(String dateFilterType) {
		this.dateFilterType = dateFilterType;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getSince() {
		return since;
	}

	public void setSince(BigDecimal since) {
		this.since = since;
	}

	public BigDecimal getFrom() {
		return from;
	}

	public void setFrom(BigDecimal from) {
		this.from = from;
	}

	public Date getCourtDate() {
		return courtDate;
	}

	public void setCourtDate(Date courtDate) {
		this.courtDate = courtDate;
	}

}
