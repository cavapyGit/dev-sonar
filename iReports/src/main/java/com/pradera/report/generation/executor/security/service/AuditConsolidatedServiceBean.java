package com.pradera.report.generation.executor.security.service;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.report.ReportFilter;
import com.pradera.report.generation.executor.security.to.AuditTrackTO;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class AuditConsolidatedServiceBean extends CrudDaoServiceBean{
	
	public AuditConsolidatedServiceBean(){
		//TODO
	}

	public  List<Object[]> getUserAuditForReport(String userCode, String ip, Long processType, 
			Long processState, Date dtIni, Date dtEnd) {
	   	StringBuilder sbQuery = new StringBuilder();
	   	sbQuery.append(" select us.registry_user,  "). //0
	   	append("us.ticket, "). //1
	   	append("to_char(us.initial_date, 'DD/MM/YYYY HH24:MI:SS'), "). //2
	   	append("to_char(us.final_date, 'DD/MM/YYYY HH24:MI:SS'), "). //3
	   	append("us.last_modify_ip, "). //4
	   	append("pa2.name as privilegio, "). //5
	   	append("to_char(uap.registry_date, 'DD/MM/YYYY HH24:MI:SS'), "). //6
	   	append("us.id_user_session_pk, "). //7
	   	append("cl.description, "). //8
	   	append("pa.name as nom_proc, "). //9
	   	append("uap.business_method, "). //10
	   	append("bp.process_name, "). //11
	   	append("bp.process_type, "). //12
	   	append("pl.logger_state, "). //13
	   	append("uad.param_name, "). //14
	   	append("uad.param_value "). //15
	   	append("from user_session us, user_audit_process uap left join process_logger pl ").
	   	append("on uap.id_process_logger_fk=pl.id_process_logger_pk, option_privilege op, catalogue_language cl, ").
	   	append("business_process bp, user_audit_detail uad, parameter pa, parameter pa2 ").	
	   	append("where 1=1 and us.id_user_session_pk=uap.id_user_session_fk and ").
	   	append("uap.last_modify_priv=op.id_option_privilege_pk and op.id_system_option_fk=cl.id_system_option_fk and ").
	   	append("cl.language=1 and bp.id_business_process_pk=uap.id_business_process_fk and ").
	   	append("uad.id_user_audit_process_fk=uap.id_user_audit_process_pk ").
	   	append("and pa.id_parameter_pk=op.id_privilege and pa2.id_parameter_pk=us.logout_motive_fk");
	   	
	   	if (Validations.validateIsNotNullAndNotEmpty(userCode)){   		
	   		sbQuery.append(" And us.registry_user = :user_code");
	   	}
	   	
	   	if (Validations.validateIsNotNullAndNotEmpty(ip)){
	   		sbQuery.append(" And us.last_modify_ip = :ip");
	   	}
	   	
	   	if (Validations.validateIsNotNullAndPositive(processType)){
	   		sbQuery.append(" And bp.process_type = :process_type");
	   	}
	   	
	   	if (Validations.validateIsNotNullAndPositive(processState)){
	   		sbQuery.append(" And pl.logger_state = :process_state");
	   	}
	   	
	   	if(Validations.validateIsNotNullAndNotEmpty(dtIni)){
	   		sbQuery.append(" And trunc(us.initial_date) >= :ini_date");
	   	}
	   	
	   	if(Validations.validateIsNotNullAndNotEmpty(dtEnd)){
	   		sbQuery.append(" And trunc(us.final_date) <= :end_date");
	   	}
	   	sbQuery.append(" order by uap.registry_date ");
	   	
	   	Query query = em.createNativeQuery(sbQuery.toString());
   	
	   	if (Validations.validateIsNotNullAndNotEmpty(userCode)) {   		
	   		query.setParameter("user_code", userCode);
	   	}
	   	
	   	if (Validations.validateIsNotNullAndNotEmpty(ip)) {
	   		query.setParameter("ip", ip);
	   	}
	   	
	   	if (Validations.validateIsNotNullAndPositive(processType)) {
	   		query.setParameter("process_type", processType);
	   	}
	   	
	   	if (Validations.validateIsNotNullAndPositive(processState)) {   		
	   		query.setParameter("process_state", processState);
	   	}
	   	
	   	if (Validations.validateIsNotNullAndNotEmpty(dtIni)) {
	   		query.setParameter("ini_date", dtIni);
	   	}
	   	
	   	if (Validations.validateIsNotNullAndNotEmpty(dtEnd)) {
	   		query.setParameter("end_date", dtEnd);
	   	}
	   	
	   	return query.getResultList();
   }

	public List<Object[]> getAuditedSensitiveFieldList(AuditTrackTO auditTrackTO) 
		throws ServiceException{

		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append("Select distinct ").
		append(" atl.name_table, ").
		append(" atf.name_field, ").
		append(" atl.registry_date, ").		
		append(" decode(atl.event_type,1,'INSERT','UPDATE') event_type, ").
		append(" atl.user_register, ").
		append(" atl.id_audit_track_pk, ").
		append(" atf.old_value, ").
		append(" atf.new_value, ").
		append(" atl.name_trigger trigger_name, ").
		append(" atl.primary_key_row ").
		append(" from ").
		append(" audit_track_log atl ").
		append(" Inner Join audit_track_field atf On atf.id_audit_track_fk = atl.id_audit_track_pk ").
		append(" where ").
		append(" trunc(atl.registry_date) between :startDatePrm and :endDatePrm ").
		append(" And atl.id_system_fk = :idSystemPrm ");
		
		if(Validations.validateIsNotNullAndNotEmpty(auditTrackTO.getTableName())) {
			sbQuery.append(" And atl.name_table = :tablePrm ");
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(auditTrackTO.getColumnName())) {
			sbQuery.append(" And atf.name_field = :columnPrm ");
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(auditTrackTO.getUserName())) {
			sbQuery.append(" And atl.user_register = :userNamePrm ");
		}
		
		if(Validations.validateIsNotNullAndPositive(auditTrackTO.getEventType())) {
			sbQuery.append(" And atl.event_type = :eventTypePrm ");
		}
		
		sbQuery.append(" order by atl.name_table , atf.name_field , atl.registry_date  ");
		
		Query query = em.createNativeQuery(sbQuery.toString());
		
		query.setParameter("startDatePrm", auditTrackTO.getStartDate(), TemporalType.DATE);
		query.setParameter("endDatePrm", auditTrackTO.getEndDate(), TemporalType.DATE);
		query.setParameter("idSystemPrm", auditTrackTO.getIdSystemPk());
		
		if(Validations.validateIsNotNullAndNotEmpty(auditTrackTO.getTableName())) {
			query.setParameter("tablePrm", auditTrackTO.getTableName());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(auditTrackTO.getColumnName())) {
			query.setParameter("columnPrm", auditTrackTO.getColumnName());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(auditTrackTO.getUserName())) {
			query.setParameter("userNamePrm", auditTrackTO.getUserName());
		}
		
		if(Validations.validateIsNotNullAndPositive(auditTrackTO.getEventType())) {
			query.setParameter("eventTypePrm", auditTrackTO.getEventType());
		}
		
		return (List<Object[]>) query.getResultList();
	}

	public String getSystemDescriptionById(Integer idSystemPk) throws ServiceException{
		try {
		StringBuilder sbQuery = new StringBuilder("Select name from system ");
		sbQuery.append(" Where id_system_pk = :idSystemPrm");
		
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("idSystemPrm", idSystemPk);
		
		return query.getSingleResult().toString();
		
		} catch(NonUniqueResultException nuex){
		    return GeneralConstants.EMPTY_STRING;
		} catch(NoResultException nex) {
			return GeneralConstants.EMPTY_STRING;
		}		
	}
	
	public String getSystemSchemaById(Integer idSystemPk) throws ServiceException{
		try {
		StringBuilder sbQuery = new StringBuilder("Select schema_db from system ");
		sbQuery.append(" Where id_system_pk = :idSystemPrm");
		
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("idSystemPrm", idSystemPk);
		
		return query.getSingleResult().toString();
		
		} catch(NonUniqueResultException nuex){
		    return GeneralConstants.EMPTY_STRING;
		} catch(NoResultException nex) {
			return GeneralConstants.EMPTY_STRING;
		}		
	}
	
	public void fillSelectMenuFilterWithTables(String schemaBD, ReportFilter reportFilter)
			throws ServiceException {

		StringBuilder sbQuery = new StringBuilder(
				"select distinct table_name from all_tables where owner = :schemaPrm ");
		sbQuery.append(" Order By table_name");
		
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("schemaPrm", schemaBD);				
		
		List<String> tablesList = (List<String>) query.getResultList();
		
		reportFilter.setSelectMenu(new LinkedHashMap<String,Object>());
		for(String tableName : tablesList){
			reportFilter.getSelectMenu().put(tableName, tableName);
		}
		
	}
	
	public void fillSelectMenuFilterWithColumns(String schemaBD,String tableName,ReportFilter reportFilter)
			throws ServiceException {
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT distinct column_name FROM ALL_TAB_COLUMNS WHERE OWNER=:schemaPrm ");
		sbQuery.append("AND TABLE_NAME=:tableNamePrm");
		sbQuery.append(" Order By column_name");

		
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("schemaPrm", schemaBD);		
		query.setParameter("tableNamePrm", tableName);	
		
		List<String> columnList = (List<String>) query.getResultList();
		
		reportFilter.setSelectMenu(new LinkedHashMap<String,Object>());
		for(String columnName : columnList){
			reportFilter.getSelectMenu().put(columnName, columnName);
		}
				
	}

}
