package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.management.RuntimeErrorException;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.executor.custody.to.ConciliationDailyOperationsTO;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "ConciliationDailyOperationsReport")
public class ConciliationDailyOperationsReport extends GenericReport{
	
	private static final long serialVersionUID = 1L;
	
	@Inject  PraderaLogger log;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	CustodyReportServiceBean custodyReportServiceBean;
	
	@EJB
	HolidayQueryServiceBean holidayQueryServiceBean;
	
	private String query;
	public ConciliationDailyOperationsReport() {}
		
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void addParametersQueryReport() { }
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> currency = new HashMap<Integer, String>();
		Map<Integer,String> currencyMnemonic = new HashMap<Integer, String>();
		
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		String issuerMnemonic = null;
		String holderAccountNumber = null;
		String participantMnemonic = null;
		String date = null;
		
		ConciliationDailyOperationsTO to=new ConciliationDailyOperationsTO();
	
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.PARTICIPANT_PARAM))
				if(r.getFilterValue() != null){
					participantMnemonic = r.getFilterDescription();
					to.setParticipant(r.getFilterValue());
				}else{
					participantMnemonic = "TODOS";
				}
			if (r.getFilterName().equals(ReportConstant.ISSUER_PARAM))
				if(r.getFilterValue() != null){
					issuerMnemonic = r.getFilterDescription();
					to.setIssuer(r.getFilterValue());
				}else{
					issuerMnemonic = "TODOS";
				}
			if (r.getFilterName().equals(ReportConstant.HOLDER_ACCOUNT))
				if(r.getFilterValue() != null){
					holderAccountNumber = r.getFilterDescription();
					to.setHolder_account(r.getFilterValue());
				}else{
					holderAccountNumber = "TODOS";
				}
			if (r.getFilterName().equals(ReportConstant.DATE_PARAM)){
				date = r.getFilterValue();
				to.setDate(r.getFilterValue());
			}
			if (r.getFilterName().equals(ReportConstant.SECURITY_CLASS_PARAM)){
				to.setSecurity_class(r.getFilterValue());
			}
			if (r.getFilterName().equals(ReportConstant.SECURITY_CODE)){
				to.setSecurity_code(r.getFilterValue());
			}
			if (r.getFilterName().equals(ReportConstant.CUI_CODE_PARAM)){
				to.setCui_code(r.getFilterValue());
			}
		}
		query=getQuerySql(to);
		Date cutdate = CommonsUtilities.convertStringtoDate(date);
		Date cutdateYesterday = CommonsUtilities.addDate(cutdate, -1);
		while(!CommonsUtilities.isUtilDate(cutdateYesterday)){
			cutdateYesterday = CommonsUtilities.addDate(cutdateYesterday, -1);
		}
		String strDateYesterday = CommonsUtilities.convertDatetoString(cutdateYesterday);
		
		//VALIDAR TIPO DE CAMBIO A LA FECHA DE CORTE
		try{
			custodyReportServiceBean.getExchangeRate(date);
		} catch (ServiceException e) {
			if(e.getErrorService().equals(ErrorServiceType.NOT_EXCHANGE_RATE)){
				throw new RuntimeErrorException(null, ReportConstant.ERROR_EXCHANGE_RATE + " " + date);
			}
			log.error(e.getMessage());
		}
		
		//VALIDAR TIPO DE CAMBIO A LA FECHA DE CORTE - 1 
		try{
			custodyReportServiceBean.getExchangeRate(strDateYesterday);
		} catch (ServiceException e) {
			if(e.getErrorService().equals(ErrorServiceType.NOT_EXCHANGE_RATE)){
				throw new RuntimeErrorException(null, ReportConstant.ERROR_EXCHANGE_RATE + " " + strDateYesterday);
			}
			log.error(e.getMessage());
		}
		
		try {
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				currency.put(param.getParameterTablePk(), param.getParameterName());
				currencyMnemonic.put(param.getParameterTablePk(), param.getDescription());
			}
			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		String strDateFunc = null;
		
		try {
			Date dateFunc = holidayQueryServiceBean.getCalculateDateServiceBean(cutdate, GeneralConstants.ONE_VALUE_INTEGER, GeneralConstants.ZERO_VALUE_INTEGER, GeneralConstants.ONE_VALUE_INTEGER);
			strDateFunc = CommonsUtilities.convertDatetoString(dateFunc); 
		}catch(Exception ex){
			ex.printStackTrace();
			throw new RuntimeException();
		}
		parametersRequired.put("participant_mnemonic", participantMnemonic);
		parametersRequired.put("issuer_mnemonic", issuerMnemonic);
		parametersRequired.put("holder_account_number", holderAccountNumber);
		parametersRequired.put("p_currency", currency);
		parametersRequired.put("p_currency_mnemonic", currencyMnemonic);
		parametersRequired.put("date_func", strDateFunc);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		parametersRequired.put("query",query);
		return parametersRequired;
	}
	public String getQuerySql(ConciliationDailyOperationsTO to){
		StringBuilder builder=new StringBuilder();
		builder.append(" SELECT ");
		builder.append(" T2.MOVEMENT_CLASS_MNEMONIC, ");
		builder.append(" T2.MOVEMENT_CLASS, ");
		builder.append(" T2.IND_MOVEMENT_CLASS, ");
		builder.append(" T2.CURRENCY, ");
		builder.append(" T2.MOVEMENT_NAME, ");
		builder.append(" T2.PARTICIPANT, ");
		builder.append(" T2.ACCOUNT_NUMBER, ");
		builder.append(" T2.ID_HOLDER_ACCOUNT_PK, ");
		builder.append(" T2.CUI, ");
		builder.append(" T2.MNEMONIC, ");
		builder.append(" T2.SECURITY_CLASS, ");
		builder.append(" SUM(T2.MOVEMENT_QUANTITY) MOVEMENT_QUANTITY, ");
		builder.append(" SUM(T2.VN_USD) VN_USD, ");
		builder.append(" SUM(T2.MP_USD) MP_USD, ");
		builder.append(" T2.MOVEMENT_DATE ");
		builder.append(" FROM ( ");
		builder.append(" SELECT ");
		builder.append(" DECODE(MT.IND_MOVEMENT_CLASS,1,'(I)','(S)') MOVEMENT_CLASS_MNEMONIC, ");
		builder.append(" DECODE(MT.IND_MOVEMENT_CLASS,1,'INGRESOS','EGRESOS') MOVEMENT_CLASS, ");
		builder.append(" MT.IND_MOVEMENT_CLASS IND_MOVEMENT_CLASS, ");
		builder.append(" S.CURRENCY CURRENCY, ");
		builder.append(" (SELECT TEXT1 FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = S.SECURITY_CLASS) SECURITY_CLASS, ");
		builder.append(" MT.MOVEMENT_NAME MOVEMENT_NAME, ");
		builder.append(" P.MNEMONIC PARTICIPANT, ");
		builder.append(" HA.ACCOUNT_NUMBER ACCOUNT_NUMBER, ");
		builder.append(" HA.ID_HOLDER_ACCOUNT_PK ID_HOLDER_ACCOUNT_PK, ");
		builder.append(" (SELECT LISTAGG(HAD.ID_HOLDER_FK, '<BR>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
		builder.append(" FROM HOLDER_ACCOUNT_DETAIL HAD ");
		builder.append(" WHERE HAD.IND_REPRESENTATIVE = 1 ");
		builder.append(" AND HAD.STATE_ACCOUNT_DETAIL = 1174 ");
		builder.append(" AND HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ");
		builder.append(" ) CUI, ");
		builder.append(" ISS.MNEMONIC, ");
		builder.append(" S.ID_SECURITY_CODE_PK, ");
		builder.append(" HAM.MOVEMENT_QUANTITY, ");
		builder.append(" (CASE ");
		builder.append(" WHEN S.CURRENCY = 127 ");
		builder.append(" THEN (HAM.MOVEMENT_QUANTITY * HAM.NOMINAL_VALUE) / ");
		builder.append(" nvl(( SELECT D.BUY_PRICE ");
		builder.append(" FROM DAILY_EXCHANGE_RATES D ");
		builder.append(" WHERE TRUNC(D.DATE_RATE) = to_date('"+to.getDate()+"','dd/mm/yyyy') ");
		builder.append(" AND D.ID_CURRENCY = 430 ");
		builder.append(" AND D.ID_SOURCE_INFORMATION = 212 ");
		builder.append(" ),1) ");
		builder.append(" WHEN S.CURRENCY IN (1734,1304) ");
		builder.append(" THEN (HAM.MOVEMENT_QUANTITY * HAM.NOMINAL_VALUE) * ");
		builder.append(" nvl(( SELECT D.BUY_PRICE ");
		builder.append(" FROM DAILY_EXCHANGE_RATES D ");
		builder.append(" WHERE TRUNC(D.DATE_RATE) = to_date('"+to.getDate()+"','dd/mm/yyyy') ");
		builder.append(" AND D.ID_CURRENCY = S.CURRENCY ");
		builder.append(" AND D.ID_SOURCE_INFORMATION = 212 ");
		builder.append(" ),1) / ");
		builder.append(" nvl(( SELECT D.BUY_PRICE ");
		builder.append(" FROM DAILY_EXCHANGE_RATES D ");
		builder.append(" WHERE TRUNC(D.DATE_RATE) = to_date('"+to.getDate()+"','dd/mm/yyyy') ");
		builder.append(" AND D.ID_CURRENCY = 430 ");
		builder.append(" AND D.ID_SOURCE_INFORMATION = 212 ");
		builder.append(" ),1) ");
		builder.append(" WHEN S.CURRENCY IN (430,1853) ");
		builder.append(" THEN (HAM.MOVEMENT_QUANTITY * HAM.NOMINAL_VALUE) ");
		builder.append(" ELSE (HAM.MOVEMENT_QUANTITY * HAM.NOMINAL_VALUE) ");
		builder.append(" END) VN_USD, ");
		builder.append(" (CASE ");
		builder.append(" WHEN S.CURRENCY = 127 ");
		builder.append(" THEN (HAM.MOVEMENT_QUANTITY * (ROUND(nvl(HAM.NOMINAL_VALUE,0),2))) / ");
		builder.append(" nvl(( SELECT D.BUY_PRICE ");
		builder.append(" FROM DAILY_EXCHANGE_RATES D ");
		builder.append(" WHERE TRUNC(D.DATE_RATE) = to_date('"+to.getDate()+"','dd/mm/yyyy') ");
		builder.append(" AND D.ID_CURRENCY = 430 ");
		builder.append(" AND D.ID_SOURCE_INFORMATION = 212 ");
		builder.append(" ),1) ");
		builder.append(" WHEN S.CURRENCY IN (1734,1304) ");
		builder.append(" THEN (HAM.MOVEMENT_QUANTITY * (ROUND(nvl(HAM.NOMINAL_VALUE,0),2))) * ");
		builder.append(" nvl(( SELECT D.BUY_PRICE ");
		builder.append(" FROM DAILY_EXCHANGE_RATES D ");
		builder.append(" WHERE TRUNC(D.DATE_RATE) = to_date('"+to.getDate()+"','dd/mm/yyyy') ");
		builder.append(" AND D.ID_CURRENCY = S.CURRENCY ");
		builder.append(" AND D.ID_SOURCE_INFORMATION = 212 ");
		builder.append(" ),1) / ");
		builder.append(" nvl(( SELECT D.BUY_PRICE ");
		builder.append(" FROM DAILY_EXCHANGE_RATES D ");
		builder.append(" WHERE TRUNC(D.DATE_RATE) = to_date('"+to.getDate()+"','dd/mm/yyyy') ");
		builder.append(" AND D.ID_CURRENCY = 430 ");
		builder.append(" AND D.ID_SOURCE_INFORMATION = 212 ");
		builder.append(" ),1) ");
		builder.append(" WHEN S.CURRENCY IN (430,1853) ");
		builder.append(" THEN (HAM.MOVEMENT_QUANTITY * (ROUND(nvl(HAM.NOMINAL_VALUE,0),2))) ");
		builder.append(" ELSE (HAM.MOVEMENT_QUANTITY * (ROUND(nvl(HAM.NOMINAL_VALUE,0),2))) ");
		builder.append(" END) MP_USD, ");
		builder.append(" TO_CHAR(HAM.MOVEMENT_DATE,'DD/MM/YYYY') AS MOVEMENT_DATE ");
		builder.append(" FROM MOVEMENT_TYPE MT, HOLDER_ACCOUNT_MOVEMENT HAM, SECURITY S, PARTICIPANT P, ");
//		builder.append(" HOLDER_ACCOUNT HA, ISSUER ISS,MARKET_FACT_VIEW HMM ");
		builder.append(" HOLDER_ACCOUNT HA, ISSUER ISS ");
//		builder.append(" ,HOLDER_MARKETFACT_MOVEMENT HMM ");
		builder.append(" WHERE MT.ID_MOVEMENT_TYPE_PK = HAM.ID_MOVEMENT_TYPE_FK ");
		builder.append(" AND HAM.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK ");
		builder.append(" AND HAM.ID_PARTICIPANT_FK = P.ID_PARTICIPANT_PK ");
		builder.append(" AND HAM.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ");
//		builder.append(" AND HAM.ID_HOLDER_ACCOUNT_MOVEMENT_PK = HMM.ID_HOLDER_ACCOUNT_MOVEMENT_FK ");
//		builder.append(" AND HMM.ID_HOLDER_ACCOUNT_PK = HAM.ID_HOLDER_ACCOUNT_FK ");
//		builder.append(" AND HMM.ID_SECURITY_CODE_PK = HAM.ID_SECURITY_CODE_FK ");
//		builder.append(" AND HMM.ID_PARTICIPANT_PK = HAM.ID_PARTICIPANT_FK     ");
		
		builder.append(" AND S.ID_ISSUER_FK = ISS.ID_ISSUER_PK ");
		builder.append(" AND MT.IND_MOVEMENT_CLASS IN (1,2) ");
		if(Validations.validateIsNotNullAndNotEmpty(to.getParticipant()))
			builder.append(" AND P.ID_PARTICIPANT_PK = "+to.getParticipant());
		if(Validations.validateIsNotNullAndNotEmpty(to.getSecurity_class()))
			builder.append(" AND S.SECURITY_CLASS = "+to.getSecurity_class());
		if(Validations.validateIsNotNullAndNotEmpty(to.getSecurity_code()))
			builder.append(" AND S.ID_SECURITY_CODE_PK = '" + to.getSecurity_code() + "'");
		if(Validations.validateIsNotNullAndNotEmpty(to.getIssuer()))
			builder.append(" AND ISS.ID_ISSUER_PK = '"+to.getIssuer() + "'");
		if(Validations.validateIsNotNullAndNotEmpty(to.getHolder_account()))
			builder.append(" AND HA.ID_HOLDER_ACCOUNT_PK = "+to.getHolder_account());
		if(Validations.validateIsNotNullAndNotEmpty(to.getCui_code())){
			builder.append("  AND (EXISTS ");
			builder.append(" (SELECT had.ID_HOLDER_FK ");
			builder.append(" FROM HOLDER_ACCOUNT_DETAIL had ");
			builder.append(" WHERE had.ID_HOLDER_FK = "+to.getCui_code());
			builder.append(" AND had.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ");
			builder.append(" )) ");
		}
		builder.append(" AND TRUNC(HAM.MOVEMENT_DATE) = to_date('"+to.getDate()+"','dd/mm/yyyy') ");
		builder.append(" ) T2 ");
		builder.append(" GROUP BY ");
		builder.append(" T2.MOVEMENT_DATE, ");
		builder.append(" T2.MOVEMENT_CLASS_MNEMONIC, ");
		builder.append(" T2.MOVEMENT_CLASS, ");
		builder.append(" T2.IND_MOVEMENT_CLASS, ");
		builder.append(" T2.CURRENCY, ");
		builder.append(" T2.MOVEMENT_NAME, ");
		builder.append(" T2.PARTICIPANT, ");
		builder.append(" T2.ACCOUNT_NUMBER, ");
		builder.append(" T2.ID_HOLDER_ACCOUNT_PK, ");
		builder.append(" T2.CUI, ");
		builder.append(" T2.MNEMONIC, ");
		builder.append(" T2.SECURITY_CLASS ");
		builder.append(" ORDER BY ");
		builder.append(" IND_MOVEMENT_CLASS, ");
		builder.append(" CURRENCY, ");
		builder.append(" MOVEMENT_NAME, ");
		builder.append(" PARTICIPANT, ");
		builder.append(" ID_HOLDER_ACCOUNT_PK, ");
		builder.append(" CUI, ");
		builder.append(" MNEMONIC, ");
		builder.append(" SECURITY_CLASS ");
		return builder.toString();
	}
}

