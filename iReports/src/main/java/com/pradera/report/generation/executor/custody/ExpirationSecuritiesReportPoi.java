package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.billing.BillingService;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.executor.custody.to.ExpirationReportTO;
import com.pradera.report.generation.poi.ReportPoiGeneratorServiceBean;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CommissionsAnnotationAccountReport.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@ReportProcess(name = "ExpirationSecuritiesReportPoi")
public class ExpirationSecuritiesReportPoi extends GenericReport {
 
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The parameter service. */
	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	private CustodyReportServiceBean custodyReportServiceBean;
	
	@EJB
	private ParticipantServiceBean participantServiceBean;
	
	/** The Report poi generator service bean. */
	@Inject
	private ReportPoiGeneratorServiceBean reportPoiGeneratorServiceBean;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	
	/**
	 * Instantiates a new commissions annotation account report.
	 */
	public ExpirationSecuritiesReportPoi() {
	}

	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#generateData(com.pradera.model.report.ReportLogger)
	 */
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] arrayByteExcel = new byte[16384];
		List<Object[]> queryMain=new ArrayList<Object[]>();
		
		ExpirationReportTO expirationReportTO = new ExpirationReportTO();
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		Map<String,Object> parametersRequired = new HashMap<>();
		
		for (int i = 0; i < listaLogger.size(); i++){
			if(listaLogger.get(i).getFilterName().equals("date_initial")){
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
			    	expirationReportTO.setInitialDate(listaLogger.get(i).getFilterValue());
			    }
			}else if(listaLogger.get(i).getFilterName().equals("date_end")){
				if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					expirationReportTO.setEndDate(listaLogger.get(i).getFilterValue());
				}
			}else if(listaLogger.get(i).getFilterName().equals("cui_code")){
				if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					expirationReportTO.setCui(new Long(listaLogger.get(i).getFilterValue()));
				}
			}else if(listaLogger.get(i).getFilterName().equals("securityClass")){
				if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					expirationReportTO.setSecurityClass(new Integer(listaLogger.get(i).getFilterValue()));
				}
			}else if(listaLogger.get(i).getFilterName().equals("securityCode")){
				if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					expirationReportTO.setSecurityCode(listaLogger.get(i).getFilterValue());
				}
			}
		}
		
		 
		String strQueryFormated = custodyReportServiceBean.getQueryExpirationSecurities(expirationReportTO);
		List<Object[]> lstObject = custodyReportServiceBean.getQueryListByClass(strQueryFormated);
		parametersRequired.put("lstObjects", lstObject);
		
		/**GENERATE FILE EXCEL ***/
		arrayByteExcel = reportPoiGeneratorServiceBean.writeFileExcelExpirationSecurities(parametersRequired,reportLogger,expirationReportTO);
		
		/**SETTING ARRAY BYTE TO GENERIC REPORT FOR PERSIST**/
		List<byte[]> bytes = new ArrayList<>();
		bytes.add(arrayByteExcel);
		setListByte(bytes);

		return baos;
	}

}
