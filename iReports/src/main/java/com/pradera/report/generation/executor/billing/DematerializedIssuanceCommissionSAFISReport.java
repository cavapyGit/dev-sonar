package com.pradera.report.generation.executor.billing;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounts.Participant;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.billing.service.BillingServiceReportServiceBean;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary. Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class DematerializedIssuanceCommissionReport.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@ReportProcess(name = "DematerializedIssuanceCommissionSAFISReport")
public class DematerializedIssuanceCommissionSAFISReport extends GenericReport {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The parameter service. */
	@EJB
	private ParameterServiceBean parameterService;

	/** The log. */
	@Inject
	PraderaLogger log;

	@EJB
	BillingServiceReportServiceBean serviceReport;

	/**
	 * Instantiates a new dematerialized issuance commission report.
	 */
	public DematerializedIssuanceCommissionSAFISReport() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.pradera.report.generation.executor.GenericReport#generateData(com.pradera
	 * .model.report.ReportLogger)
	 */
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub ok
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.pradera.report.generation.executor.GenericReport#addParametersQueryReport
	 * ()
	 */
	@Override
	public void addParametersQueryReport() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.pradera.report.generation.executor.GenericReport#getCustomJasperParameters
	 * ()
	 */
	@Override
	public Map<String, Object> getCustomJasperParameters() {

		Map<String, Object> parametersRequired = new HashMap<>();
		Long idParticipantPk = 0L;
		String serviceCode = null;
		String dateInitial = null;
		String dateFinal = null;
		String cui = null;
		Participant participant = null;
		List<ReportLoggerDetail> listaLogger = getReportLogger()
				.getReportLoggerDetails();
		for (int i = 0; i < listaLogger.size(); i++) {
			if (listaLogger.get(i).getFilterName().equals("p_participant_code")) {
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i)
						.getFilterValue())) {
					idParticipantPk = Long.parseLong(listaLogger.get(i)
							.getFilterValue());
				}
			}
			if (listaLogger.get(i).getFilterName().equals("p_help_code")) {
				cui = listaLogger.get(i).getFilterValue();
			}
			if (listaLogger.get(i).getFilterName().equals("p_service_code")) {
				serviceCode = listaLogger.get(i).getFilterValue();
			}
			if (listaLogger.get(i).getFilterName().equals("date_initial")) {
				dateInitial = listaLogger.get(i).getFilterValue();
			}
			if (listaLogger.get(i).getFilterName().equals("date_end")) {
				dateFinal = listaLogger.get(i).getFilterValue();
			}
		}
		participant = serviceReport.find(idParticipantPk, Participant.class);
		List<String> list = serviceReport.getIdIssuerPks(participant);
		String listIdIssuerPks = null;

		if (list.size() == 1) {
			listIdIssuerPks = "'" + list.get(0) + "'";
		}
		if (list.size() > 1)
			for (int i = 0; i < list.size(); i++) {
				if (i == 0) {
					listIdIssuerPks = "'" + list.get(i) + "',";
					continue;
				}
				listIdIssuerPks = listIdIssuerPks + "'" + list.get(i) + "'";
				if (i < list.size() - 1)
					listIdIssuerPks = listIdIssuerPks + ",";
			}
		String query = getQuery(serviceCode, idParticipantPk.toString(),
				listIdIssuerPks, dateInitial, dateFinal, cui);
		parametersRequired.put("query", query);
		parametersRequired.put(
				"full_name_participant",
				participant.getIdParticipantPk() + " - "
						+ participant.getDescription());
		return parametersRequired;
	}

	public String getQuery(String serviceCode, String idParticipantPk,
			String listIdIssuerPks, String dateInitial, String dateFinal,
			String cui) {
		StringBuilder query = new StringBuilder();
		query.append("SELECT BS.REFERENCE_RATE, BS.SERVICE_CODE, BS.SERVICE_NAME, CR.ID_ISSUER_FK, CR.CALCULATION_DATE, ");
		query.append(" TO_CHAR(CR.CALCULATION_DATE,'DD/MON/YY','NLS_DATE_LANGUAGE = SPANISH') CALCULATION_DATE_STRING, ");
		query.append(" CRD.SECURITY_CLASS, (SELECT TEXT1 FROM PARAMETER_TABLE ");
		query.append(" WHERE PARAMETER_TABLE_PK=CRD.SECURITY_CLASS) CLASE_VALOR_NEMONIC, CRD.ID_ISSUANCE_CODE_FK, ");
		query.append(" CRD.NEGOTIATED_AMOUNT_ORIGIN,(select DESCRIPTION from PARAMETER_TABLE where PARAMETER_TABLE_PK=CRD.CURRENCY_ORIGIN) AS CURRENCY, CRD.NEGOTIATED_AMOUNT,");
		// CRD.EXCHANGE_RATE_CURRENT
		query.append(" CR.EXCHANGE_RATE_CURRENT, CRD.RATE_AMOUNT, ");
		// CRD.RATE_AMOUNT_BILLED
		query.append(" (CR.EXCHANGE_RATE_CURRENT*CRD.RATE_AMOUNT) as RATE_AMOUNT_BILLED, CR.CURRENCY_BILLING , I.ID_HOLDER_FK, H.FULL_NAME ");
		query.append(" FROM PROCESSED_SERVICE PS INNER JOIN COLLECTION_RECORD CR ON CR.ID_PROCESSED_SERVICE_FK=PS.ID_PROCESSED_SERVICE_PK ");
		query.append(" INNER JOIN COLLECTION_RECORD_DETAIL CRD ON CRD.ID_COLLECTION_RECORD_FK= CR.ID_COLLECTION_RECORD_PK ");
		query.append(" INNER JOIN BILLING_SERVICE BS ON BS.ID_BILLING_SERVICE_PK=PS.ID_BILLING_SERVICE_FK ");
		query.append(" INNER JOIN SERVICE_RATE SR ON SR.ID_BILLING_SERVICE_FK=BS.ID_BILLING_SERVICE_PK ");

		query.append(" INNER JOIN ISSUANCE ISS ON ISS.ID_ISSUANCE_CODE_PK = CRD.ID_ISSUANCE_CODE_FK ");
		query.append(" INNER JOIN ISSUER I ON I.ID_ISSUER_PK = ISS.ID_ISSUER_FK ");
		query.append(" INNER JOIN HOLDER H ON H.ID_HOLDER_PK=I.ID_HOLDER_FK ");
		// RENTA VARIABLE
		query.append(" WHERE  BS.BASE_COLLECTION IN(1806) ");
		if (Validations.validateIsNotNullAndNotEmpty(serviceCode))
			query.append(" AND BS.SERVICE_CODE = " + serviceCode);
		if (Validations.validateIsNotNullAndNotEmpty(cui))
			query.append(" AND I.ID_HOLDER_FK = " + cui);
		if (Validations.validateIsNotNullAndNotEmpty(listIdIssuerPks))
			query.append(" AND CR.ID_ISSUER_FK in ( " + listIdIssuerPks + ") ");
		// if(Validations.validateIsNotNullAndNotEmpty(idParticipantPk))
		// query.append(" AND CR.ID_PARTICIPANT_FK = "+idParticipantPk);

		query.append(" AND trunc(CR.CALCULATION_DATE) BETWEEN to_date('"
				+ dateInitial + "','DD/MM/YYYY') and to_date('" + dateFinal
				+ "','DD/MM/YYYY')");
		query.append(" ORDER BY CR.CALCULATION_DATE, CR.ID_ISSUER_FK, CRD.SECURITY_CLASS");
		return query.toString();
	}

	private String valueParameter(List<ReportLoggerDetail> listaLogger,
			String parameter) {
		String value = null;
		for (int i = 0; i < listaLogger.size(); i++) {
			if (listaLogger.get(i).getFilterName() != null) {
				if (listaLogger.get(i).getFilterName().equals(parameter)) {
					if (listaLogger.get(i).getFilterValue() != null) {
						value = listaLogger.get(i).getFilterValue();
						i = listaLogger.size();
					}
				}
			}
		}
		return value;
	}
}
