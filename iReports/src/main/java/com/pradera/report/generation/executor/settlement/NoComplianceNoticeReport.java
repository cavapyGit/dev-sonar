package com.pradera.report.generation.executor.settlement;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;

@ReportProcess(name="NoComplianceNoticeReport")
public class NoComplianceNoticeReport extends GenericReport{
	
	private static final long serialVersionUID = 1L;
	
	@Inject PraderaLogger log;
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String,Object> parametersRequired = new HashMap<>();
		
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		
		String settlementDate = null;
		String idUnfulfillmentParticipant = null;
		String idAffectedParticipant = null;
		String institutionName = null;
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.SETTLEMENT_DATE_PARAM) && r.getFilterValue()!=null){
				settlementDate = r.getFilterValue();
			}
			if (r.getFilterName().equals(ReportConstant.UNFULFILLMENT_PART_ID) && r.getFilterValue()!=null){
				idUnfulfillmentParticipant = r.getFilterValue();
			}
			if (r.getFilterName().equals(ReportConstant.AFFECTED_PART_ID) && r.getFilterValue()!=null){
				idAffectedParticipant = r.getFilterValue();
			}
			if (r.getFilterName().equals(ReportConstant.INSTITUTION_NAME) && r.getFilterValue()!=null){
				institutionName = r.getFilterValue();
			}
		}
		
		parametersRequired.put(ReportConstant.SETTLEMENT_DATE_PARAM, CommonsUtilities.convertStringtoDate(settlementDate));
		parametersRequired.put(ReportConstant.UNFULFILLMENT_PART_ID, idUnfulfillmentParticipant);
		parametersRequired.put(ReportConstant.AFFECTED_PART_ID, idAffectedParticipant);
		parametersRequired.put(ReportConstant.INSTITUTION_NAME, institutionName);
		
		return parametersRequired;
	}

}
