package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.integration.common.validation.Validations;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.billing.service.BillingServiceReportServiceBean;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.executor.custody.to.RegisterSecuritiesEntSysIsDirectTO;
import com.pradera.report.generation.poi.ReportPoiGeneratorServiceBean;

@ReportProcess(name = "RegisterSecuritiesEntSysIsDirectReportPoi")
public class RegisterSecuritiesEntSysIsDirectReportPoi extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private BillingServiceReportServiceBean billingServiceReportServiceBean;
	
	@EJB
	private CustodyReportServiceBean custodyReportServiceBean;
	
	/** The Report poi generator service bean. */
	@Inject
	private ReportPoiGeneratorServiceBean reportPoiGeneratorServiceBean;
	
	//constructor
	public RegisterSecuritiesEntSysIsDirectReportPoi(){}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] arrayByteExcel = new byte[16384];
		RegisterSecuritiesEntSysIsDirectTO registerSecuritiesEntSysIsDirectTO = new RegisterSecuritiesEntSysIsDirectTO();
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		Map<String,Object> parametersRequired = new HashMap<>();
		
		for (int i = 0; i < listaLogger.size(); i++){
			
			if(listaLogger.get(i).getFilterName().equals("operation")){
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
			    	registerSecuritiesEntSysIsDirectTO.setSourceOperation(Integer.parseInt(listaLogger.get(i).getFilterValue()));
			    }
			}else if(listaLogger.get(i).getFilterName().equals("holder_account")){
				if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					registerSecuritiesEntSysIsDirectTO.setIdHolderAccount(Long.parseLong(listaLogger.get(i).getFilterValue()));
				}
			}else if(listaLogger.get(i).getFilterName().equals("date_initial")){
				if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					registerSecuritiesEntSysIsDirectTO.setInitialDate(listaLogger.get(i).getFilterValue());
				}
			}else if(listaLogger.get(i).getFilterName().equals("date_end")){
				if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					registerSecuritiesEntSysIsDirectTO.setFinalDate(listaLogger.get(i).getFilterValue());
				}
			}else if(listaLogger.get(i).getFilterName().equals("security_code")){
				if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					registerSecuritiesEntSysIsDirectTO.setSecurityCode(listaLogger.get(i).getFilterValue());
				}
			}else if(listaLogger.get(i).getFilterName().equals("cui_code")){
				if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					registerSecuritiesEntSysIsDirectTO.setCuiHolder(Long.parseLong(listaLogger.get(i).getFilterValue()));
				}
			}
		}
		
		String strQueryFormated = custodyReportServiceBean.getQueryRegisterSecuritiesEntSysIsDirect(registerSecuritiesEntSysIsDirectTO);
		List<Object[]> lstObject = custodyReportServiceBean.getQueryListByClass(strQueryFormated);
		parametersRequired.put("lstObjects", lstObject);
		
		/**GENERATE FILE EXCEL ***/
		arrayByteExcel = reportPoiGeneratorServiceBean.writeFileExcelRegisterSecuritiesEntSysDirect(parametersRequired,reportLogger);
		
		/**SETTING ARRAY BYTE TO GENERIC REPORT FOR PERSIST**/
		List<byte[]> bytes = new ArrayList<>();
		bytes.add(arrayByteExcel);
		setListByte(bytes);
		
		return baos;
	}

}
