package com.pradera.report.generation.executor.settlement;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name="SettledOperationsReport")
public class SettledOperationsReport extends GenericReport{
	
	private static final long serialVersionUID = 1L;
	
	@Inject PraderaLogger log;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	ParticipantServiceBean participantServiceBean;
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String,Object> parametersRequired = new HashMap<>();
		Map<Integer,String> currency = new HashMap<Integer, String>();
		Map<Integer,String> participantMnemonic = new HashMap<Integer, String>();
		Map<Integer,String> scheduleType = new HashMap<Integer, String>();
		ParameterTableTO  filter = new ParameterTableTO();
		
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		String participantDescription = null;
		String modalityDescription = null;
		String currencyDescription = null;
		String securityClassDescription = null;
		String schemaDescription = null;
		String scheduleTypeDescription = null;
	
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.PARTICIPANT_PARAM))
				if(r.getFilterValue() != null){
					participantDescription = r.getFilterDescription();
				}else{
					participantDescription = "TODOS";
				}
			if (r.getFilterName().equals(ReportConstant.MODALITY_PARAM))
				if(r.getFilterValue() != null){
					modalityDescription = r.getFilterDescription();
				}else{
					modalityDescription = "TODOS";
				}
			if (r.getFilterName().equals(ReportConstant.CURRENCY))
				if(r.getFilterValue() != null){
					currencyDescription = r.getFilterDescription();
				}else{
					currencyDescription = "TODOS";
				}
			if (r.getFilterName().equals(ReportConstant.SECURITY_CLASS_PARAM))
				if(r.getFilterValue() != null){
					securityClassDescription = r.getFilterDescription();
				}else{
					securityClassDescription = "TODOS";
				}
			if (r.getFilterName().equals(ReportConstant.SETTLEMENT_SCHEMA_PARAM))
				if(r.getFilterValue() != null){
					schemaDescription = r.getFilterDescription();
				}else{
					schemaDescription = "TODOS";
				}
			if (r.getFilterName().equals(ReportConstant.SCHEDULE_TYPE))
				if(r.getFilterValue() != null){
					scheduleTypeDescription = r.getFilterDescription();
				}else{
					scheduleTypeDescription = "TODOS";
				}
		}
		
		try {
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				currency.put(param.getParameterTablePk(), param.getDescription());
			}
			for(Participant participant : participantServiceBean.getLisParticipantServiceBean(new Participant())) {
				participantMnemonic.put(participant.getIdParticipantPk().intValue(), participant.getMnemonic());
			}
			
			filter.setMasterTableFk(MasterTableType.PARAMETER_TABLE_SCHED_SETTLEMENT.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				scheduleType.put(param.getParameterTablePk(), param.getDescription());
			}
			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		parametersRequired.put("participant_description", participantDescription);
		parametersRequired.put("modality_description", modalityDescription);
		parametersRequired.put("currency_description", currencyDescription);
		parametersRequired.put("security_class_description", securityClassDescription);
		parametersRequired.put("schema_description", schemaDescription);
		parametersRequired.put("schedule_type_description", scheduleTypeDescription);
		parametersRequired.put("map_currency", currency);
		parametersRequired.put("p_schedule_type", scheduleType);
		parametersRequired.put("p_participant_mnemonic", participantMnemonic);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		
		return parametersRequired;
	}

}
