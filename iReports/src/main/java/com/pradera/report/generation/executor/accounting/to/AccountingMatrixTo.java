package com.pradera.report.generation.executor.accounting.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.model.accounting.AccountingMatrix;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingMatrixTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class AccountingMatrixTo implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 0xfc167ee37c4c7cb6L;
    
    /** The id accounting matrix pk. */
    private Long idAccountingMatrixPk;
    
    /** The accounting schema fk. */
    private Long accountingSchemaFk;
    
    /** The accounting matrix detail. */
    private List<AccountingMatrixDetailTo> accountingMatrixDetail;
    
    /** The accounting schema. */
    private AccountingSchemaTo		accountingSchema;
    
    /** The status. */
    private Integer status;
    
    /** The auxiliary type. */
    private Integer auxiliaryType;
    
    /** The file source name. */
    private String fileSourceName;
    
    /** The description source. */
    private String descriptionSource;
    
    /** The accounting matrix list. */
    private List<AccountingMatrix> 		accountingMatrixList ;

    
    /**
     * Instantiates a new accounting matrix to.
     */
    public AccountingMatrixTo(){
    
    }

    /**
     * Gets the id accounting matrix pk.
     *
     * @return the id accounting matrix pk
     */
    public Long getIdAccountingMatrixPk(){
    
        return idAccountingMatrixPk;
    }

    /**
     * Sets the id accounting matrix pk.
     *
     * @param idAccountingMatrixPk the new id accounting matrix pk
     */
    public void setIdAccountingMatrixPk(Long idAccountingMatrixPk){
    
        this.idAccountingMatrixPk = idAccountingMatrixPk;
    }

    /**
     * Gets the accounting schema fk.
     *
     * @return the accounting schema fk
     */
    public Long getAccountingSchemaFk(){
    
        return accountingSchemaFk;
    }

    /**
     * Sets the accounting schema fk.
     *
     * @param accountingSchemaFk the new accounting schema fk
     */
    public void setAccountingSchemaFk(Long accountingSchemaFk) {
   
        this.accountingSchemaFk = accountingSchemaFk;
    }

    /**
     * Gets the accounting matrix detail.
     *
     * @return the accounting matrix detail
     */
    public List<AccountingMatrixDetailTo> getAccountingMatrixDetail(){
    
        return accountingMatrixDetail;
    }

    /**
     * Sets the accounting matrix detail.
     *
     * @param accountingMatrixDetail the new accounting matrix detail
     */
    public void setAccountingMatrixDetail(List<AccountingMatrixDetailTo> accountingMatrixDetail){
    
        this.accountingMatrixDetail = accountingMatrixDetail;
    }

    /**
     * Gets the status.
     *
     * @return the status
     */
    public Integer getStatus(){
    
        return status;
    }

    /**
     * Sets the status.
     *
     * @param status the new status
     */
    public void setStatus(Integer status) {
   
        this.status = status;
    }

    /**
     * Gets the auxiliary type.
     *
     * @return the auxiliary type
     */
    public Integer getAuxiliaryType(){
    
        return auxiliaryType;
    }

    /**
     * Sets the auxiliary type.
     *
     * @param auxiliaryType the new auxiliary type
     */
    public void setAuxiliaryType(Integer auxiliaryType){
    
        this.auxiliaryType = auxiliaryType;
    }

    /**
     * Gets the file source name.
     *
     * @return the file source name
     */
    public String getFileSourceName(){
    
        return fileSourceName;
    }

    /**
     * Sets the file source name.
     *
     * @param fileSourceName the new file source name
     */
    public void setFileSourceName(String fileSourceName){
    
        this.fileSourceName = fileSourceName;
    }

    /**
     * Gets the description source.
     *
     * @return the description source
     */
    public String getDescriptionSource(){
    
        return descriptionSource;
    }

    /**
     * Sets the description source.
     *
     * @param descriptionSource the new description source
     */
    public void setDescriptionSource(String descriptionSource){
    
        this.descriptionSource = descriptionSource;
    }

	/**
	 * Gets the accounting matrix list.
	 *
	 * @return the accounting matrix list
	 */
	public List<AccountingMatrix> getAccountingMatrixList() {
		return accountingMatrixList;
	}

	/**
	 * Sets the accounting matrix list.
	 *
	 * @param accountingMatrixList the new accounting matrix list
	 */
	public void setAccountingMatrixList(List<AccountingMatrix> accountingMatrixList) {
		this.accountingMatrixList = accountingMatrixList;
	}

	/**
	 * Gets the accounting schema.
	 *
	 * @return the accountingSchema
	 */
	public AccountingSchemaTo getAccountingSchema() {
		return accountingSchema;
	}

	/**
	 * Sets the accounting schema.
	 *
	 * @param accountingSchema the accountingSchema to set
	 */
	public void setAccountingSchema(AccountingSchemaTo accountingSchema) {
		this.accountingSchema = accountingSchema;
	}
    
    
    
}
