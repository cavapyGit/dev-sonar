package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;

import com.pradera.integration.common.validation.Validations;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;

@ReportProcess(name = "UnificationParticipantPreliminarytInconsReport")
public class UnificationParticipantPreliminarytInconsReport extends GenericReport{
	private static final long serialVersionUID = 1L;
	
	@EJB
	private CustodyReportServiceBean participantUnificationInconsist;

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		GenericsFiltersTO inconsistenciesTO = new GenericsFiltersTO();

		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("idSourceParticipantPk")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					inconsistenciesTO.setParticipant(listaLogger.get(i).getFilterValue());
				}
			}	
		}

		//get all the query in a string format
		String strQuery = participantUnificationInconsist.getQueryParticipantUnificationInconsist(inconsistenciesTO);

		Map<String,Object> parametersRequired = new HashMap<>();
		parametersRequired.put("str_query", strQuery);
		
		return parametersRequired;
	}
}
