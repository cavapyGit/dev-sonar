package com.pradera.report.generation.executor.account;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.management.RuntimeErrorException;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.account.service.PositionHolderPartSeriviceBean;
import com.pradera.report.generation.executor.account.to.PositionsHolderParticipantTO;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name="PositionsHolderParticipantReport")
public class PositionsHolderParticipantReport extends GenericReport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject  PraderaLogger log;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	private PositionHolderPartSeriviceBean positionHolderPartSeriviceBean;
	
	@EJB
	private CustodyReportServiceBean custodyReportServiceBean;

	public PositionsHolderParticipantReport() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void addParametersQueryReport() {

	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> currency = new HashMap<Integer, String>();
		Map<Integer,String> descripction = new HashMap<Integer, String>();
		Map<Integer,String> securityClass = new HashMap<Integer, String>();
		Map<Integer,String> securityClassDescription = new HashMap<Integer, String>();
		String date = null;
		try {
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				currency.put(param.getParameterTablePk(), param.getDescription());
				descripction.put(param.getParameterTablePk(), param.getParameterName());
			}
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				securityClass.put(param.getParameterTablePk(), param.getText1());
				securityClassDescription.put(param.getParameterTablePk(), param.getDescription());
			}
		}catch(Exception ex){
			log.error(ex.getMessage());
			throw new RuntimeException();
		}
		parametersRequired.put("p_currency", currency);
		parametersRequired.put("p_description", descripction);
		parametersRequired.put("p_securityClass", securityClass);
		parametersRequired.put("p_securityClassDescription", securityClassDescription);
		
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		PositionsHolderParticipantTO positionHolderTO = new PositionsHolderParticipantTO();
		
		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("participant")){
				if (listaLogger.get(i).getFilterValue()!=null && !listaLogger.get(i).getFilterValue().equals("0")){
					positionHolderTO.setParticipant(new Long(listaLogger.get(i).getFilterValue()).longValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("issuer")){
				if (listaLogger.get(i).getFilterValue()!=null && !listaLogger.get(i).getFilterValue().equals("0")){
					positionHolderTO.setIssuer(listaLogger.get(i).getFilterValue().toString());
				}
			}
			
			if(listaLogger.get(i).getFilterName().equals("cui_holder")){
				if (listaLogger.get(i).getFilterValue()!=null && !listaLogger.get(i).getFilterValue().equals("0")){
					positionHolderTO.setCuiHolder(new Long(listaLogger.get(i).getFilterValue()));
				}
			}
			
			if(listaLogger.get(i).getFilterName().equals("security_class")){
				if (listaLogger.get(i).getFilterValue()!=null && !listaLogger.get(i).getFilterValue().equals("0")){
					positionHolderTO.setSecurityClass(listaLogger.get(i).getFilterValue());
				}
			}
			
			if(listaLogger.get(i).getFilterName().equals("court_date")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					positionHolderTO.setCourtDate(CommonsUtilities.convertStringtoDate(listaLogger.get(i).getFilterValue()));
					date = listaLogger.get(i).getFilterValue();
				}
			}
		}
		
		//VALIDAR TIPO DE CAMBIO A LA FECHA DE CORTE
		try{
			custodyReportServiceBean.getExchangeRate(date);
		} catch (ServiceException e) {
			if(e.getErrorService().equals(ErrorServiceType.NOT_EXCHANGE_RATE)){
				throw new RuntimeErrorException(null, ReportConstant.ERROR_EXCHANGE_RATE + " " + date);
			}
			log.error(e.getMessage());
		}
		
		Long idStkCalcProcess= null;
		
		String strQuery = positionHolderPartSeriviceBean.QueryPositionHolderParticipant(positionHolderTO, idStkCalcProcess);
		String strQueryFormatted = positionHolderPartSeriviceBean.QueryFormatForJasper(positionHolderTO, strQuery, idStkCalcProcess);
		
		parametersRequired.put("str_query", strQueryFormatted);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		return parametersRequired;
	}

}
