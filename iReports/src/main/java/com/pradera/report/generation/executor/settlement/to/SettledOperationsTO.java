package com.pradera.report.generation.executor.settlement.to;

import java.io.Serializable;
import java.util.Date;

public class SettledOperationsTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6509733311059672538L;
	
	private Long participantPk;
	private Long modality;
	private Integer currency;
	private String securityCode;
	private Integer schema;
	private Date dateInitial;
	private Date dateEnd;
	private Integer scheduleType;
	private String strDateInitial;
	private String strDateEnd;
	private String participantDesc;
	private String securityClass;
	private Integer unfulfillmentMotive;
	private Long participantRegisterReport;
	
	
	public Date getSettlementDate() {
		return settlementDate;
	}
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}
	private Date settlementDate;
	
	public Integer getUnfulfillmentMotive() {
		return unfulfillmentMotive;
	}
	public void setUnfulfillmentMotive(Integer unfulfillmentMotive) {
		this.unfulfillmentMotive = unfulfillmentMotive;
	}
	public Long getModality() {
		return modality;
	}
	public void setModality(Long modality) {
		this.modality = modality;
	}
	public Long getParticipantPk() {
		return participantPk;
	}
	public void setParticipantPk(Long participantPk) {
		this.participantPk = participantPk;
	}
	public Integer getCurrency() {
		return currency;
	}
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	public Date getDateInitial() {
		return dateInitial;
	}
	public void setDateInitial(Date dateInitial) {
		this.dateInitial = dateInitial;
	}
	public Date getDateEnd() {
		return dateEnd;
	}
	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}
	public Integer getSchema() {
		return schema;
	}
	public void setSchema(Integer schema) {
		this.schema = schema;
	}
	public String getParticipantDesc() {
		return participantDesc;
	}
	public void setParticipantDesc(String participantDesc) {
		this.participantDesc = participantDesc;
	}
	public String getSecurityCode() {
		return securityCode;
	}
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
	public String getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(String securityClass) {
		this.securityClass = securityClass;
	}
	public Integer getScheduleType() {
		return scheduleType;
	}
	public void setScheduleType(Integer scheduleType) {
		this.scheduleType = scheduleType;
	}
	public String getStrDateInitial() {
		return strDateInitial;
	}
	public void setStrDateInitial(String strDateInitial) {
		this.strDateInitial = strDateInitial;
	}
	public String getStrDateEnd() {
		return strDateEnd;
	}
	public void setStrDateEnd(String strDateEnd) {
		this.strDateEnd = strDateEnd;
	}
	public Long getParticipantRegisterReport() {
		return participantRegisterReport;
	}
	public void setParticipantRegisterReport(Long participantRegisterReport) {
		this.participantRegisterReport = participantRegisterReport;
	}
	
	
	

}
