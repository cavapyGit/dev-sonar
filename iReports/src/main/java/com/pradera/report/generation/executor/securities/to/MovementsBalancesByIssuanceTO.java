package com.pradera.report.generation.executor.securities.to;

import java.io.Serializable;
import java.util.Date;

public class MovementsBalancesByIssuanceTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The id issuer pk. */
	private String idIssuerPk;
	
	/** The id issuer pk. */
	private String idIssuancePk;
	
	/** The initial date. */
	private Date initialDate;

	/** The final date. */
	private Date finalDate;

	public String getIdIssuerPk() {
		return idIssuerPk;
	}

	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}

	public String getIdIssuancePk() {
		return idIssuancePk;
	}

	public void setIdIssuancePk(String idIssuancePk) {
		this.idIssuancePk = idIssuancePk;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

}
