package com.pradera.report.generation.executor.security;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.security.service.ProcessAndReportsControlServiceBean;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

@ReportProcess(name="ProcessAndReportsControlReport")
public class ProcessAndReportsControlReport extends GenericReport{
	private static final long serialVersionUID = 1L;
	@Inject
	PraderaLogger log;

	@EJB
	private ProcessAndReportsControlServiceBean processAndReportsControlServiceBean;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	//declarando posiciones del query
	private static int INSTITUTION_TYPE=0;
	private static int INSTITUTION_NAME=1;
	private static int USER_ACCOUNT=2;
	private static int LAST_IP=3;
	private static int PROCESS_DT=4;
	private static int MNEMONIC=5;
	private static int PROCESS_NAME=6;
	private static int ORIGIN=7;
	private static int STATE=8;
	private static int PARAMETER_NAME=9;
	private static int PARAMETER_VALUE=10;
	private static int IS_PROCESS_TYPE=11;
	private static int IS_REPORT_TYPE=12;
	
	public ProcessAndReportsControlReport(){
		//TODO
	}
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		boolean existData = false;
		Long user_type = null;
		Long inst_name=null;
		Long id_state=null;
		Long inst_type=null;
		String user_code=null;
		Date initial_date=null;
		Date final_date=null;
		
		//Read parameters from DB, all parameters are string type
		for(ReportLoggerDetail loggerDetail : reportLogger.getReportLoggerDetails()) {
			if(loggerDetail.getFilterName().equals(ReportConstant.USER_TYPE)) {
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					user_type = Long.valueOf(loggerDetail.getFilterValue());
				}				
			}else if(loggerDetail.getFilterName().equals(ReportConstant.INSTITUTION_NAME)) {
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					inst_name = Long.valueOf(loggerDetail.getFilterValue());
				}
			}else if(loggerDetail.getFilterName().equals(ReportConstant.STATE_PARAM)) {
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					id_state = Long.valueOf(loggerDetail.getFilterValue());
				}
			}else if(loggerDetail.getFilterName().equals(ReportConstant.INSTRUMENT_TYPE)) {
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					inst_type = Long.valueOf(loggerDetail.getFilterValue());
				}
			}else if(loggerDetail.getFilterName().equals(ReportConstant.USER_CODE_ID)){
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					user_code = loggerDetail.getFilterValue().toString();
				}
			}else if (loggerDetail.getFilterName().equals(ReportConstant.INITIAL_DATE)){
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					initial_date = CommonsUtilities.convertStringtoDate(loggerDetail.getFilterValue().toString());
				}
			}else if (loggerDetail.getFilterName().equals(ReportConstant.FINAL_DATE)){
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					final_date = CommonsUtilities.convertStringtoDate(loggerDetail.getFilterValue().toString());
				}
			}						
		}
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			//query
			List<Object[]> list = processAndReportsControlServiceBean.getProcessesAndReportsByUser(user_code, user_type, 
															inst_name, id_state, inst_type, initial_date, final_date);
			if(!list.isEmpty()) {
				existData = true;
			}
			
			XMLOutputFactory xof = XMLOutputFactory.newInstance();
			XMLStreamWriter xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(baos));
			//open document file
			xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");
			//root tag of report
			/**
			 * report
			 * */
			xmlsw.writeStartElement(ReportConstant.REPORT);

			createHeaderReport(xmlsw, reportLogger);
			
			if(existData) {
				createTagString(xmlsw,"flag_exist_data","true");
				//method specific to each implementation of reports
				xmlsw.writeStartElement("userByProcOrRep");
				createBodyReport(xmlsw, list);
				xmlsw.writeEndElement();
			} else {
				//when not exist data we need to create empty structure xml
				createTagString(xmlsw,"flag_exist_data","false");			
			}
			String date_initial_format = CommonsUtilities.convertDateToString(initial_date,CommonsUtilities.DATE_PATTERN);
			createTagString(xmlsw,"initial_date",date_initial_format);
			
			String date_end_format = CommonsUtilities.convertDateToString(final_date,CommonsUtilities.DATE_PATTERN);
			createTagString(xmlsw,"final_date",date_end_format);
			
			//close report tag
			xmlsw.writeEndElement();
			//close document
			xmlsw.writeEndDocument();
			xmlsw.flush();
	        xmlsw.close();
		} catch (Exception ex) {
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		return baos;
	}
	
	public void createBodyReport(XMLStreamWriter xmlsw,List<Object[]> list)  throws XMLStreamException{
		ParameterTable objPt=null;
		String procTypeDesc = "---";
		Integer procType=null;
		for(Object[] ob : list) {
			xmlsw.writeStartElement("detailOfUser");
				createTagString(xmlsw, "INSTITUTION_TYPE", ob[INSTITUTION_TYPE]);
				createTagString(xmlsw, "INSTITUTION_NAME", ob[INSTITUTION_NAME]);
				createTagString(xmlsw, "ID_USER_ACCOUNT", ob[USER_ACCOUNT]);
				createTagString(xmlsw, "LAST_MODIFY_IP", ob[LAST_IP]);
				if(Validations.validateIsNotNullAndNotEmpty(ob[IS_REPORT_TYPE]))
					createTagString(xmlsw, "TYPE", "REPORTE");
				else
					createTagString(xmlsw, "TYPE", "PROCESO");
				createTagString(xmlsw, "PROCESS_DATE", ob[PROCESS_DT]);
				createTagString(xmlsw, "MNEMONIC", ob[MNEMONIC]);
				createTagString(xmlsw, "PROCESS_NAME", ob[PROCESS_NAME]);
				if(Validations.validateIsNotNullAndNotEmpty(ob[ORIGIN])){
					procType=Integer.parseInt(ob[ORIGIN].toString());
					objPt = parameterService.getParameterDetail(procType);
					procTypeDesc = objPt.getParameterName();
				}
				createTagString(xmlsw, "ORIGIN", procTypeDesc);
				createTagString(xmlsw, "STATE", ob[STATE]);
				createTagString(xmlsw, "PARAMETER_NAME", ob[PARAMETER_NAME]);
				createTagString(xmlsw, "PARAMETER_VALUE", ob[PARAMETER_VALUE]);
			xmlsw.writeEndElement();
		}
	}

}
