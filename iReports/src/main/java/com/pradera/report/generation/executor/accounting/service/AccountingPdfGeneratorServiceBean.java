package com.pradera.report.generation.executor.accounting.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingPdfGeneratorServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/05/2015
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class AccountingPdfGeneratorServiceBean  {


	/**
	 * Instantiates a new accounting pdf generator service bean.
	 */
	public AccountingPdfGeneratorServiceBean(){
		

	}
	
	/**
	 * Convert Excel To PDF and send To Array Byete.
	 *
	 * @param arrayByteExcel the array byte excel
	 * @return the byte[]
	 */
	public byte[] writeFileExcelAccountingBalance(byte[] arrayByteExcel){
		
		byte[] arrayByte=null;

        
        try {
        	FileOutputStream fileOut = new FileOutputStream("AccountingBalanceReport.pdf");
        	
        	ByteArrayOutputStream baosPDF = new ByteArrayOutputStream();
        	baosPDF.writeTo(fileOut);
		    // Read workBookExcel into HSSFWorkbook
            HSSFWorkbook workBookExcel = new HSSFWorkbook(new ByteArrayInputStream(arrayByteExcel));
            
            // Read workSheet into HSSFSheet
            HSSFSheet workSheet = workBookExcel.getSheetAt(0);
            
            // To iterate over the rows
            Iterator<Row> rowIterator = workSheet.iterator();
            
            //We will create output PDF document objects at this point
            Document docPdf = new Document();
            PdfWriter.getInstance(docPdf, baosPDF);
            docPdf.open();
            
            //we have two columns in the Excel sheet, so we create a PDF table with two columns
            //Note: There are ways to make this dynamic in nature, if you want to.
            PdfPTable myTable = new PdfPTable(9);
            
            //We will use the object below to dynamically add new data to the table
            PdfPCell tableCell;
            
            //Loop through rows.
            while(rowIterator.hasNext()) {
                    Row row = rowIterator.next(); 
                    Iterator<Cell> cellIterator = row.cellIterator();
                            while(cellIterator.hasNext()) {
                                    Cell cell = cellIterator.next(); //Fetch CELL
                                    switch(cell.getCellType()) { //Identify CELL type
                                            //you need to add more code here based on
                                            //your requirement / transformations
                                    case Cell.CELL_TYPE_STRING:
                                            //Push the data from Excel to PDF Cell
                                             tableCell=new PdfPCell(new Phrase(cell.getStringCellValue()));
                                             //feel free to move the code below to suit to your needs
                                             myTable.addCell(tableCell);
                                            break;
                                    }
                                    //next line
                            }
            
            }
            //Finally add the table to PDF document
            docPdf.add(myTable);                       
            docPdf.close();                
            //we created our pdf file
            baosPDF.close();
            arrayByte=baosPDF.toByteArray();
            
            fileOut.close();
		    
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		} catch (DocumentException e) {

			e.printStackTrace();
		}

        
		return arrayByte;
	}
	

	/**
	 * Convert Excel To PDF and send To Array Byete.
	 *
	 * @param arrayByteExcel the array byte excel
	 * @return the byte[]
	 */
	public byte[] writeFileExcelAccountingBalance2(byte[] arrayByteExcel){
		byte[] arrayByte=null;


		return arrayByte;
	}
	
}
