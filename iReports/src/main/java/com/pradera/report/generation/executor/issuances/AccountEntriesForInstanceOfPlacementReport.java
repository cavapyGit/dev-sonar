package com.pradera.report.generation.executor.issuances;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.account.to.PositionsHolderParticipantTO;
import com.pradera.report.generation.executor.issuances.services.IssuancesReportServiceBean;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name="AccountEntriesForInstanceOfPlacementReport")
public class AccountEntriesForInstanceOfPlacementReport extends GenericReport{
	private static final long serialVersionUID = 1L;
	
	@Inject
	private PraderaLogger log;
	@EJB
	private ParameterServiceBean parameterService;
	@EJB
	private IssuancesReportServiceBean issuancesReportService;
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {

		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		GenericsFiltersTO gfto = new GenericsFiltersTO();
		String securitiesExcluded = null;
		
		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("date_initial")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					gfto.setInitialDt(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("date_end")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					gfto.setFinalDt(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("issuer_code")){
				if (listaLogger.get(i).getFilterValue()!=null && !listaLogger.get(i).getFilterValue().equals("0")){
					gfto.setIdIssuer(listaLogger.get(i).getFilterValue().toString());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("participant")){
				if (listaLogger.get(i).getFilterValue()!=null && !listaLogger.get(i).getFilterValue().equals("0")){
					gfto.setParticipant(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("cui_code")){
				if (listaLogger.get(i).getFilterValue()!=null && !listaLogger.get(i).getFilterValue().equals("0")){
					gfto.setCui(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("holder_account")){
				if (listaLogger.get(i).getFilterValue()!=null && !listaLogger.get(i).getFilterValue().equals("0")){
					gfto.setIdHolderAccountPk(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("security_class_key")){
				if (listaLogger.get(i).getFilterValue()!=null && !listaLogger.get(i).getFilterValue().equals("0")){
					gfto.setIdSecurityCodePk(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("securities_key_ex")){
				if (listaLogger.get(i).getFilterValue()!=null && !listaLogger.get(i).getFilterValue().equals("0")){
					securitiesExcluded = listaLogger.get(i).getFilterValue();
				}
			}
		}
		 
		String strQuery = issuancesReportService.getAccountEntriesForInstanceOfPlacement(gfto,securitiesExcluded); 
		
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> secMotive = new HashMap<Integer, String>();
		Map<Integer,String> secCurrency = new HashMap<Integer, String>();
		Map<Integer,String> secCurrencyDesc = new HashMap<Integer, String>();
		Map<Integer,BigDecimal> tipoCambio = new HashMap<Integer,BigDecimal>();
		try {
			filter.setMasterTableFk(MasterTableType.DEMATERIALIZATION_MOTIVE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secMotive.put(param.getParameterTablePk(), param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secCurrency.put(param.getParameterTablePk(), param.getDescription());
			}
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secCurrencyDesc.put(param.getParameterTablePk(), param.getParameterName());
			}
			tipoCambio = parameterService.getDayExchangesRates(CommonsUtilities.currentDate());
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		String strSubTotalQuery = issuancesReportService.getSubTotalAccountEntriesForInstanceOfPlacement(gfto,securitiesExcluded, tipoCambio); 
		
		parametersRequired.put("strQuery", strQuery);
		parametersRequired.put("str_subTotalQuery", strSubTotalQuery);
		parametersRequired.put("pMotivo", secMotive);
		parametersRequired.put("pCurrency", secCurrency);
		parametersRequired.put("pCurrencyDesc", secCurrencyDesc);
		parametersRequired.put("date_initial", gfto.getInitialDt());
		parametersRequired.put("date_end", gfto.getFinalDt());
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		
		return parametersRequired;
	}
	
	

}
