package com.pradera.report.generation.executor.security.to;

import java.io.Serializable;
import java.math.BigDecimal;

public class AcquisitionInPrimaryMarketTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long IdParticipantPk;
	
	private String idSecurityCodePk;
	
	private String idIssuerPk;
	
	private String issuerMnemonic;
	
	private Integer economicActivity;
	
	private Integer securityClass;
	
	private String instrumentKey;
	
	private String issuanceDateSecurity;
	
	private String personType;
	
	private Long idHolderAccountPk;
	
	private Long idParticipantPkBuyer;
	
	private Long idParticipantPkSeller;
	
	private BigDecimal quantity;
	
	private Long idHolderPk;
	 
	private String documentNumber;
	
	private String issuanceDate;
	
	private BigDecimal nominalValue;
	/*ACQUISITION_PRICE*/
	private BigDecimal acquisitionPrice;
	/*CURRENCY*/
	private Integer currency;
	
	public AcquisitionInPrimaryMarketTO() {
		// TODO Auto-generated constructor stub
	}
	

	public Long getIdParticipantPkBuyer() {
		return idParticipantPkBuyer;
	}


	public void setIdParticipantPkBuyer(Long idParticipantPkBuyer) {
		this.idParticipantPkBuyer = idParticipantPkBuyer;
	}


	public Long getIdParticipantPkSeller() {
		return idParticipantPkSeller;
	}


	public void setIdParticipantPkSeller(Long idParticipantPkSeller) {
		this.idParticipantPkSeller = idParticipantPkSeller;
	}


	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}

	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}

	public String getIssuerMnemonic() {
		return issuerMnemonic;
	}
	
	public BigDecimal getQuantity() {
		return quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public void setIssuerMnemonic(String issuerMnemonic) {
		this.issuerMnemonic = issuerMnemonic;
	}

	public Integer getSecurityClass() {
		return securityClass;
	}

	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	public String getInstrumentKey() {
		return instrumentKey;
	}

	public void setInstrumentKey(String instrumentKey) {
		this.instrumentKey = instrumentKey;
	}

	public String getIssuanceDateSecurity() {
		return issuanceDateSecurity;
	}

	public void setIssuanceDateSecurity(String issuanceDateSecurity) {
		this.issuanceDateSecurity = issuanceDateSecurity;
	}

	public String getPersonType() {
		return personType;
	}

	public void setPersonType(String personType) {
		this.personType = personType;
	}

	public Long getIdHolderPk() {
		return idHolderPk;
	}

	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}

	public BigDecimal getNominalValue() {
		return nominalValue;
	}

	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}

	public BigDecimal getAcquisitionPrice() {
		return acquisitionPrice;
	}

	public void setAcquisitionPrice(BigDecimal acquisitionPrice) {
		this.acquisitionPrice = acquisitionPrice;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getIdIssuerPk() {
		return idIssuerPk;
	}

	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}


	public Long getIdParticipantPk() {
		return IdParticipantPk;
	}

	public String getIssuanceDate() {
		return issuanceDate;
	}

	public void setIssuanceDate(String issuanceDate) {
		this.issuanceDate = issuanceDate;
	}


	public void setIdParticipantPk(Long idParticipantPk) {
		IdParticipantPk = idParticipantPk;
	}


	public Integer getEconomicActivity() {
		return economicActivity;
	}


	public void setEconomicActivity(Integer economicActivity) {
		this.economicActivity = economicActivity;
	}
}
