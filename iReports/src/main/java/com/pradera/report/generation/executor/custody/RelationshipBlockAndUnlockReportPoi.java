package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.executor.custody.to.RelationshipBlockAndUnlockReportTO;
import com.pradera.report.generation.poi.ReportPoiGeneratorServiceBean;

/**
 * 
 * @author rlarico
 *
 */
@ReportProcess(name = "RelationshipBlockAndUnlockReportPoi")
public class RelationshipBlockAndUnlockReportPoi extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	PraderaLogger log;
	@EJB
	private CustodyReportServiceBean custodyReportServiceBean;
	@Inject
	private ReportPoiGeneratorServiceBean reportPoiGeneratorServiceBean;
	
	public RelationshipBlockAndUnlockReportPoi(){
		
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] arrayByteExcel = new byte[16384];
		
		RelationshipBlockAndUnlockReportTO relationBlockUnlockReportTO=new RelationshipBlockAndUnlockReportTO();
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		Map<String,Object> parametersRequired = new HashMap<>();
		
		for (int i = 0; i < listaLogger.size(); i++){
			if(listaLogger.get(i).getFilterName().equals("p_participant"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	relationBlockUnlockReportTO.setIdParticipant(listaLogger.get(i).getFilterValue());
			
			if(listaLogger.get(i).getFilterName().equals("p_cui"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	relationBlockUnlockReportTO.setIdHolder(listaLogger.get(i).getFilterValue());
			
			if(listaLogger.get(i).getFilterName().equals("account_holder"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	relationBlockUnlockReportTO.setAccountNumber(listaLogger.get(i).getFilterDescription());
			
			if(listaLogger.get(i).getFilterName().equals("issuer"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	relationBlockUnlockReportTO.setIdIssuer(listaLogger.get(i).getFilterValue());
			
			if(listaLogger.get(i).getFilterName().equals("security_class"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	relationBlockUnlockReportTO.setSecurityClass(listaLogger.get(i).getFilterValue());
			
			if(listaLogger.get(i).getFilterName().equals("security_code"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	relationBlockUnlockReportTO.setIdSecurityCode(listaLogger.get(i).getFilterValue());
			
			if(listaLogger.get(i).getFilterName().equals("block_type"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	relationBlockUnlockReportTO.setLockType(listaLogger.get(i).getFilterValue());
			
			if(listaLogger.get(i).getFilterName().equals("afectation_level"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	relationBlockUnlockReportTO.setLevelAffectation(listaLogger.get(i).getFilterValue());
			
			if(listaLogger.get(i).getFilterName().equals("request_state"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	relationBlockUnlockReportTO.setStateRequest(listaLogger.get(i).getFilterValue());
			
			if(listaLogger.get(i).getFilterName().equals("p_initial_date"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	relationBlockUnlockReportTO.setInitialDate(listaLogger.get(i).getFilterValue());
			
			if(listaLogger.get(i).getFilterName().equals("p_final_date"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	relationBlockUnlockReportTO.setFinalDate(listaLogger.get(i).getFilterValue());
		}
		
		String strQueryFormated = custodyReportServiceBean.getQueryRelationshipBlockAndUnlock(relationBlockUnlockReportTO);
		List<Object[]> lstObject = custodyReportServiceBean.getQueryListByClass(strQueryFormated);
		parametersRequired.put("lstObjects", lstObject);
		
		arrayByteExcel = reportPoiGeneratorServiceBean.writeFileExcelRelationshipBlockAndUnlock(parametersRequired, reportLogger);
		
		List<byte[]> bytes = new ArrayList<>();
		bytes.add(arrayByteExcel);
		setListByte(bytes);
		
		return baos;
	}

}
