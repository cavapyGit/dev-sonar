package com.pradera.report.generation.executor.corporative;

import java.io.ByteArrayOutputStream;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.corporateevents.to.CorporativeOperationTO;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.corporative.service.CorporativeReportServiceBean;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

@ReportProcess(name="PaymentInformationReport")
public class PaymentInformationReport extends GenericReport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Inject
	PraderaLogger log;
	@EJB
	private CorporativeReportServiceBean serviceBean;
	@EJB
	private ParameterServiceBean parameterServiceBean;
	
	private final static int ISSUER_DESCRIPTION		 	= 0;
	private final static int ID_ISIN_CODE_PK			= 1;
	private final static int CURRENCY					= 2;
	private final static int PLACED_AMOUNT				= 3;	
	private final static int EVENT_TYPE					= 4;
	private final static int PAYMENT_AMOUNT				= 5;
	private final static int PAYTMENT_DATE				= 6;

	private String currentDateStr;
	private String deliveryDate;
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		CorporativeOperationTO corporativeOperationTO = reportLoggerCorporativeOperationTO(reportLogger);
		try{
			//GETTING ALL DATA FROM QUERY
			List<Object[]> listObjects = serviceBean.getPaymentInformation(corporativeOperationTO);
			
			XMLOutputFactory xof = XMLOutputFactory.newInstance();
			XMLStreamWriter xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(baos));
			//open document file
			xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");
			//root tag of report
			/**
			 * report
			 **/
			xmlsw.writeStartElement(ReportConstant.REPORT);
			/**
			 * Create to header report
			 * start_hour
			 * report_title
			 * mnemonic_report
			 * clasification_report
			 * generation_date
			 * user_name
			 * mnemonic_entity
			 * **/
			ParameterTableTO parameterTableTO = new ParameterTableTO();			
			parameterTableTO.setParameterTablePk(corporativeOperationTO.getSignType());
			
			List<ParameterTable> parameterTables = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);
			
			//Getting dates
			String deliveryDay   = CommonsUtilities.convertDateToString(corporativeOperationTO.getDeliveryDate(),"dd");
			String deliveryMonth = CommonsUtilities.convertDateToString(corporativeOperationTO.getDeliveryDate(),"MM");
			String deliveryYear  = CommonsUtilities.convertDateToString(corporativeOperationTO.getDeliveryDate(),"yyyy");
			
			//Getting dates
			String currentDay   = CommonsUtilities.convertDateToString(CommonsUtilities.currentDate(),"dd");
			String currentMonth = CommonsUtilities.convertDateToString(CommonsUtilities.currentDate(),"MM");
			String currentYear  = CommonsUtilities.convertDateToString(CommonsUtilities.currentDate(),"yyyy");
			
			
			currentDateStr = currentDay + " de " + getMonth(currentMonth) + " del año " + currentYear;
			deliveryDate = deliveryDay + " de " + getMonth(deliveryMonth) + " del año " + deliveryYear;
			
			createTagString(xmlsw, "NOMBRE_FIRMA", 	parameterTables.get(0).getParameterName());
			createTagString(xmlsw, "CARGO_FIRMA", 	parameterTables.get(0).getObservation());
			
			createHeaderReport(xmlsw, reportLogger);
			
			if(Validations.validateListIsNotNullAndNotEmpty(listObjects)){
				createBodyReport(xmlsw, listObjects);
			}
			//END REPORT TAG
			xmlsw.writeEndElement();
			//close document
			xmlsw.writeEndDocument();
			xmlsw.flush();
	        xmlsw.close();
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		return baos;
	}
	private void createBodyReport(XMLStreamWriter xmlsw,List<Object[]> listObjects) throws XMLStreamException {
		xmlsw.writeStartElement("rows"); //START ROWS TAG
		StringBuilder content = new StringBuilder();
				
		createTagString(xmlsw, "currentDate", currentDateStr);
		
		content.append("En cumplimiento a lo dispuesto en el Art\u00edculo 35 de la Norma que establece Disposiciones Generales sobre la informaci\u00f3n");
		content.append("que deben remitir peri\u00f3dicamente los Emisores Participantes del Mercado, le notificamos formalmente que el d\u00eda ");
		content.append(deliveryDate);
		content.append(" realizamos los siguientes pagos:");
		createTagString(xmlsw, "CONTENT", content);	
		//BY ROW
		for(Object[] object : listObjects){
			xmlsw.writeStartElement("row"); //START ROW TAG
				createTagString(xmlsw, "ISSUER",        	object[ISSUER_DESCRIPTION]);
				createTagString(xmlsw, "ID_ISIN_CODE_PK", 	object[ID_ISIN_CODE_PK]);
				createTagString(xmlsw, "CURRENCY",  		object[CURRENCY]);
				createTagString(xmlsw, "PLACED_AMOUNT", 	object[PLACED_AMOUNT]);
				createTagString(xmlsw, "EVENT_TYPE", 		object[EVENT_TYPE]);
				createTagString(xmlsw, "ID_ISIN_CODE_PK", 	object[ID_ISIN_CODE_PK]);
				createTagString(xmlsw, "PAYMENT_AMOUNT", 	object[PAYMENT_AMOUNT]);
				createTagString(xmlsw, "PAYTMENT_DATE", 	deliveryDate);															
			xmlsw.writeEndElement();
		}
		xmlsw.writeEndElement(); // END ROWS TAG		
	}

	public CorporativeOperationTO reportLoggerCorporativeOperationTO(ReportLogger reportLogger){
		CorporativeOperationTO corporativeOperationTO = new CorporativeOperationTO();
		
		for (ReportLoggerDetail detail : reportLogger.getReportLoggerDetails()) {
			if( detail.getFilterName().equals(ReportConstant.DELIVERY_DATE) ){
				if(Validations.validateIsNotNull(detail.getFilterValue())){
					corporativeOperationTO.setDeliveryDate(CommonsUtilities.convertStringtoDate(detail.getFilterValue().toString()));
				}			
			}else if( detail.getFilterName().equals(ReportConstant.SIGN_TYPE)  ){
				if(Validations.validateIsNotNull(detail.getFilterValue())){
					corporativeOperationTO.setSignType(new Integer(detail.getFilterValue().toString()));
				}
			}
		}
		return corporativeOperationTO;
		
	}
	public String getMonth(String month){
		switch(month){
		case "01": return "enero";
		case "02": return "febrero";
		case "03": return "marzo";
		case "04": return "abril";
		case "05": return "mayo";
		case "06": return "junio";
		case "07": return "julio";
		case "08": return "agosto";
		case "09": return "septiembre";
		case "10": return "octubre";
		case "11": return "noviembre";
		case "12": return "diciembre";
		default: return "";
		}
	}
	
}
