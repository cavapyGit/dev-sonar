package com.pradera.report.generation.executor.generalParameters;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.XMLUtils;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.custody.to.TransferBCBBEMTO;
import com.pradera.integration.component.custody.to.TransferTGNTEMTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.externalinterface.ExternalInterface;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.generalParameters.service.GeneralParametersReportServiceBean;
import com.pradera.report.generation.service.ReportUtilServiceBean;

@ReportProcess(name = "TransferToBcbBemReport")
public class TransferToBcbBemReport extends GenericReport {

	private static final long serialVersionUID = 1L;

	@Inject
	PraderaLogger log;

	@EJB
	private GeneralParametersReportServiceBean generalParametersReportServiceBean;
	@EJB
	private ReportUtilServiceBean reportUtilServiceBean;

	public TransferToBcbBemReport() {
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		return null;
	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		log.info("ENTRANDO AL REPORTE 347");

		Date initialDate = null, finalDate = null;
		Long idInterfacePk = null;

		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		for (ReportLoggerDetail loggerDetail : loggerDetails) {

			if (loggerDetail.getFilterName().equals("interface")) {
				if (Validations.validateIsNotNull(loggerDetail.getFilterValue())) {
					idInterfacePk = Long.parseLong(String.valueOf(loggerDetail.getFilterValue()));
				}
			}
			if (loggerDetail.getFilterName().equals("date_initial")) {
				if (Validations.validateIsNotNull(loggerDetail.getFilterValue())) {
					initialDate = CommonsUtilities.convertStringtoDate(String.valueOf(loggerDetail.getFilterValue()), CommonsUtilities.DATE_PATTERN );
				}
			}
			if (loggerDetail.getFilterName().equals("date_end")) {
				if (Validations.validateIsNotNull(loggerDetail.getFilterValue())) {
					finalDate = CommonsUtilities.convertStringtoDate(String.valueOf(loggerDetail.getFilterValue()), CommonsUtilities.DATE_PATTERN );
				}
			}
		}

		Class entityTOClass = null;
		ExternalInterface ei = null;
		String query = null;
		File txtFile = null;
		byte[] content=null;
		String fileName = CommonsUtilities.convertDateToString(CommonsUtilities.currentDate(), "yyyyMMdd");

		if (Validations.validateIsNotNullAndPositive(idInterfacePk)) {
			ei = generalParametersReportServiceBean.find(ExternalInterface.class, idInterfacePk);
			if (idInterfacePk.equals(1031L)) {
				entityTOClass = TransferBCBBEMTO.class;
				fileName = "BEM" + fileName + ".txt";
			} else if (idInterfacePk.equals(1032L)) {
				entityTOClass = TransferTGNTEMTO.class;
				fileName = "TEM" + fileName + ".txt";
			}
		}
		if (Validations.validateIsNotNull(ei) && Validations.validateIsNotNull(ei.getQuerySql())) {
			try {
				query = ei.getQuerySql();
				Map<String, Object> parameters = new HashMap<String, Object>();
				parameters.put("initialDate", initialDate);
				parameters.put("finalDate", finalDate);
				List<Object> dataList = generalParametersReportServiceBean.findByNativeQuery(query, parameters);

				if (Validations.validateListIsNotNullAndNotEmpty(dataList)) {
					List<String> listOfAlias = null;
					listOfAlias = Arrays.asList("numeroRegistro", "claveValor",
												"cuentaVendedor", "cuentaComprador", "cantidad",
												"fecha", "mechanismOperationPk");
					List<Object> listClass = new ArrayList<>();

					listClass = CommonsUtilities.filledBean(listOfAlias, entityTOClass, dataList);
					try {
						txtFile = generateXmlFile(listClass, fileName,ei.getSchemaDirectory(),
								ComponentConstant.INTERFACE_TAG_ROWSET, ComponentConstant.INTERFACE_TAG_ROW);
						content = XMLUtils.getBytesFromFile(txtFile);
						
						reportUtilServiceBean.saveCustomReportFile(fileName,
								ReportFormatType.TXT.getCode(),
								content, getReportLogger());
					} catch (IOException e) {
						e.printStackTrace();
					}
				}else{
					// si no hay data mandamos un mensaje en el archivo indicando que no hay data
					String msgNoData = "No existen Transferencias para el rango de fechas seleccionado";
					reportUtilServiceBean.saveCustomReportFile(fileName,
							ReportFormatType.TXT.getCode(),
							msgNoData.getBytes(), getReportLogger());
				}
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public File generateXmlFile(List<Object> listObjectValidations,
			String nameFile, String schemeName, String rowset, String row)
			throws IOException {
		return CommonsUtilities.generateXmlFile(listObjectValidations,
				nameFile, schemeName, rowset, row);
	}

}
