package com.pradera.report.generation.executor.accounting;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounting.AccountingAccount;
import com.pradera.model.accounting.AccountingProcess;
import com.pradera.model.accounting.type.AccountingStartType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.accounting.service.AccountingGeneratorReportServiceBean;
import com.pradera.report.generation.executor.accounting.service.AccountingReportQueryServiceBean;
import com.pradera.report.generation.executor.accounting.to.AccountingMatrixDetailTo;
import com.pradera.report.generation.executor.accounting.to.AccountingProcessTo;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class DocumentSecurityReportPdf.
 *  Accounting Accumulated All Process last to Current, History
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@ReportProcess(name="DocumentSecurityReportPdf")
public class DocumentSecurityReportPdf extends GenericReport{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The log. */
	@Inject
	PraderaLogger log;
		
	/** The accounting generator report service bean. */
	@Inject
	private AccountingGeneratorReportServiceBean accountingGeneratorReportServiceBean;

	/** The accounting report query service bean. */
	@EJB
	AccountingReportQueryServiceBean accountingReportQueryServiceBean;
	
	/** The parameter service. */
	@EJB
	private ParameterServiceBean parameterService;
	
	
	/** The tag process. */
	private static String TAG_PROCESS       				= "PROCESS";
	
	/** The tag date process. */
	private static String TAG_DATE_PROCESS       			= "DATE_PROCESS";
	
	/** The tag accounting detail. */
	private static String TAG_ACCOUNTING_DETAIL       		= "accountingDetails";
	
	/** The tag accounts. */
	private static String TAG_ACCOUNTS       				= "accounts";
	
	/** The tag type account. */
	private static String TAG_TYPE_ACCOUNT       			= "TYPE_ACCOUNT";
	
	/** The tag account code father. */
	private static String TAG_ACCOUNT_CODE_FATHER  			= "ACCOUNT_CODE_FATHER";
	
	/** The tag account code. */
	private static String TAG_ACCOUNT_CODE       			= "ACCOUNT_CODE";
	
	/** The tag description. */
	private static String TAG_DESCRIPTION       			= "DESCRIPTION";
	
	/** The tag quantity. */
	private static String TAG_QUANTITY       				= "QUANTITY";
	
	/** The tag amount origin. */
	private static String TAG_AMOUNT_ORIGIN       			= "AMOUNT_ORIGIN_CURRENCY";
	
	/** The tag amount usd. */
	private static String TAG_AMOUNT_USD       				= "AMOUNT_USD";
	
	/** The tag amount bob. */
	private static String TAG_AMOUNT_BOB       				= "AMOUNT_BOB";
	
	/**
	 * Instantiates a new document security report pdf.
	 */
	public DocumentSecurityReportPdf() {
	}
	
	/** The list accounting matrix detail. */
	List<AccountingMatrixDetailTo>	listAccountingMatrixDetail;
	
	/** The list accounting account. */
	List<AccountingAccount>	listAccountingAccount;
	
	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#generateData(com.pradera.model.report.ReportLogger)
	 */
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		
		AccountingProcessTo accountingProcessTo;
		accountingProcessTo=new AccountingProcessTo();
		StringBuilder sbParametersReport = new StringBuilder();
		boolean existData = false;
		for(ReportLoggerDetail loggerDetail : reportLogger.getReportLoggerDetails()) {

	
			if(loggerDetail.getFilterName().equals(ReportConstant.PROCESS_ACCOUNTING_PK)){
				
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					accountingProcessTo.setIdAccountingProcessPk(Long.parseLong(loggerDetail.getFilterValue()));
					sbParametersReport.append(loggerDetail.getFilterValue()).append(GeneralConstants.STR_COMMA);
				}
			}
			else if(loggerDetail.getFilterName().equals(ReportConstant.BRANCH)){
				
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					accountingProcessTo.setProcessType(Integer.parseInt(loggerDetail.getFilterValue()));
					sbParametersReport.append(loggerDetail.getFilterValue()).append(GeneralConstants.STR_COMMA);
				}
			}

			else if(loggerDetail.getFilterName().equals(ReportConstant.DATE_INITIAL_PARAM)){
				
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					accountingProcessTo.setExecutionDate(CommonsUtilities.convertStringtoDate(loggerDetail.getFilterValue()));
					sbParametersReport.append(loggerDetail.getFilterValue()).append(GeneralConstants.STR_COMMA);
				}
			}else if(loggerDetail.getFilterName().equals(GeneralConstants.REPORT_BY_PROCESS)){
				
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					accountingProcessTo.setReportByProcess(Integer.parseInt(loggerDetail.getFilterValue()));
					sbParametersReport.append(loggerDetail.getFilterValue()).append(GeneralConstants.STR_COMMA);
				}
			}
		}

		accountingProcessTo.setStartType(AccountingStartType.CONTINUE.getCode());
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		try {
			
			listAccountingAccount=this.accountingGeneratorReportServiceBean.findAccountBalanceHistory(accountingProcessTo);
			
			try {
				BigDecimal descuadre = null;//
				Integer primaryKey = 2440;
				ParameterTable amountParameter = parameterService.find(ParameterTable.class, primaryKey);
				descuadre = new BigDecimal(amountParameter.getText1());
				
				BigDecimal dif = BigDecimal.ZERO;
				
				for (AccountingAccount acc : listAccountingAccount) {
					if(acc.getAccountCode().equals("705.00")){
						dif = acc.getAmount().subtract(descuadre);
						acc.setAmount(dif);
//						acc.setAmountBOB(dif);
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			XMLOutputFactory xof = XMLOutputFactory.newInstance();
			XMLStreamWriter xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(baos));
			//open document file
			xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");
			//root tag of report
			/**
			 * report
			 **/
			xmlsw.writeStartElement(ReportConstant.REPORT);
			/**
			 * Create to header report
			 * start_hour
			 * report_title
			 * mnemonic_report
			 * clasification_report
			 * generation_date
			 * user_name
			 * mnemonic_entity
			 * **/
			createHeaderReport(xmlsw, reportLogger);
			xmlsw.writeStartElement(TAG_ACCOUNTING_DETAIL);
			

			if(Validations.validateListIsNotNullAndNotEmpty(listAccountingAccount)){
				
				AccountingProcess accountingProcess=accountingReportQueryServiceBean.findAccountingProcessToFilter(accountingProcessTo);
				createTagString(xmlsw, TAG_DATE_PROCESS, CommonsUtilities.convertDatetoString(accountingProcess.getExecutionDate()));
				
				/** Creating the body **/
				createBodyReport(xmlsw, listAccountingAccount);
			}else{
				createEmptyReport(xmlsw);
			}
			//end accreditationOperationDetails
			xmlsw.writeEndElement();
			//END REPORT TAG
			xmlsw.writeEndElement();
			//close document
			xmlsw.writeEndDocument();
			xmlsw.flush();
	        xmlsw.close();
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		return baos;
	}
	
	
	/**
	 * Creates the body report.
	 *
	 * @param xmlsw the xmlsw
	 * @param listAccountingAccount the list accounting account
	 * @throws XMLStreamException the XML stream exception
	 * @throws ServiceException the service exception
	 */
	public void createBodyReport(XMLStreamWriter xmlsw, List<AccountingAccount> listAccountingAccount)  throws XMLStreamException, ServiceException{
		String empty_large="------------------------";
		String empty="---";
		String zero="0";
		//BY ROW
		
		for (AccountingAccount accountingAccount : listAccountingAccount) {
			
			xmlsw.writeStartElement(TAG_ACCOUNTS);			
			if(accountingAccount.getIndAccumulation().equals(GeneralConstants.ONE_VALUE_INTEGER)&&
	        			Validations.validateIsNull(accountingAccount.getAccountingAccount())){
			
				createTagString(xmlsw, TAG_ACCOUNT_CODE_FATHER, accountingAccount.getAccountCode());
				createTagString(xmlsw, TAG_ACCOUNT_CODE, 	 	GeneralConstants.EMPTY_STRING);
			}else{
				createTagString(xmlsw, TAG_ACCOUNT_CODE_FATHER, GeneralConstants.EMPTY_STRING);
				createTagString(xmlsw, TAG_ACCOUNT_CODE, 		accountingAccount.getAccountCode());
				
			}

			createTagString(xmlsw, TAG_DESCRIPTION, 	 accountingAccount.getDescription());
			
			createTagString(xmlsw, TAG_QUANTITY, 	 	CommonsUtilities.getFormatOfAmount(accountingAccount.getQuantity(), 0, 25, GeneralConstants.FORMAT_QUANTITY, true));
			
			createTagString(xmlsw, TAG_AMOUNT_ORIGIN, 	CommonsUtilities.getFormatOfAmount(accountingAccount.getAmount(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
			
			createTagString(xmlsw, TAG_AMOUNT_USD, 	 	CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUSD(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
			
			createTagString(xmlsw, TAG_AMOUNT_BOB, 	 	CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountBOB(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
			
			xmlsw.writeEndElement();
		}
		
		
	}

	

	/**
	 * Creates the empty report.
	 *
	 * @param xmlsw the xmlsw
	 * @throws XMLStreamException the XML stream exception
	 */
	private void createEmptyReport(XMLStreamWriter xmlsw) throws XMLStreamException {
		String empty="---";
		String zero="0";
		
		createTagString(xmlsw, TAG_ACCOUNT_CODE_FATHER,empty);
		createTagString(xmlsw, TAG_ACCOUNT_CODE, 	 empty);
		createTagString(xmlsw, TAG_DESCRIPTION, 	 empty);
		createTagString(xmlsw, TAG_QUANTITY,		 empty);
		createTagString(xmlsw, TAG_AMOUNT_ORIGIN, 	 empty);
		createTagString(xmlsw, TAG_AMOUNT_USD, 	 	 empty);
		createTagString(xmlsw, TAG_AMOUNT_BOB, 	 	 empty);

		
	}
	
	
}
