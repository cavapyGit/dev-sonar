package com.pradera.report.generation.executor.settlement;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.settlement.service.SettlementReportServiceBean;
import com.pradera.report.generation.executor.settlement.to.SettledOperationsTO;
import com.pradera.report.generation.poi.ReportPoiGeneratorServiceBean;
import com.pradera.report.generation.service.ReportUtilServiceBean;

@ReportProcess(name = "SettledOperationsPoiReport")
public class SettledOperationsPoiReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private SettlementReportServiceBean settlementReportServiceBean;
	@EJB
	private ReportUtilServiceBean reportUtilServiceBean;

	/** The Report poi generator service bean. */
	@Inject
	private ReportPoiGeneratorServiceBean reportPoiGeneratorServiceBean;
	@Inject
	private PraderaLogger log;

	private SettledOperationsTO settledOperationsTO;
	
	public SettledOperationsPoiReport() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {


		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] arrayByteExcel = new byte[16384];
		List<Object[]> queryMain=new ArrayList<Object[]>();
		
		String title=null;
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		Map<String,Object> parametersRequired = new HashMap<>();
		settledOperationsTO = new SettledOperationsTO();
		
		
		//Si el usuario es participante extraemos el pk para mostrar o no las cuentas titulares de compra y venta
		Long pkParticipantRegister = settlementReportServiceBean.getParticipantRegister(getReportLogger().getRegistryUser());
		
		if(Validations.validateIsNotNullAndNotEmpty(pkParticipantRegister)){
			settledOperationsTO.setParticipantRegisterReport(pkParticipantRegister);
		}
		
		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("participant")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					settledOperationsTO.setParticipantPk(Long.valueOf(listaLogger.get(i).getFilterValue()));
					settledOperationsTO.setParticipantDesc(listaLogger.get(i).getFilterDescription());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("modality")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					settledOperationsTO.setModality(Long.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("currency")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					settledOperationsTO.setCurrency(Integer.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("security_class")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					settledOperationsTO.setSecurityClass(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("security_code")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					settledOperationsTO.setSecurityCode(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("schema")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					settledOperationsTO.setSchema(Integer.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("schedule_type")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					settledOperationsTO.setScheduleType(Integer.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("date_initial")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					settledOperationsTO.setDateInitial(CommonsUtilities.convertStringtoDate(listaLogger.get(i).getFilterValue()));
					settledOperationsTO.setStrDateInitial(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("date_end")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					settledOperationsTO.setDateEnd(CommonsUtilities.convertStringtoDate(listaLogger.get(i).getFilterValue()));
					settledOperationsTO.setStrDateEnd(listaLogger.get(i).getFilterValue());
				}
			}
		}

			String strQuery = null;
			title = "REPORTE DE OPERACIONES LIQUIDADAS";
			strQuery = settlementReportServiceBean.getQuerySettledOperations(settledOperationsTO);
			queryMain = settlementReportServiceBean.getQueryListByClass(strQuery);
			
//			parametersRequired.put("report_title_customize", title);
			parametersRequired.put("str_query", queryMain);
//			parametersRequired.put("pkParticipantRegister", pkParticipantRegister);
//			parametersRequired.put("p_securityClassDescription", securityClass);
//			parametersRequired.put("p_participant_desc", settledOperationsTO.getParticipantDesc());
//			parametersRequired.put("p_currency", currencyTypeShort);
//			parametersRequired.put("p_description", currencyType);
//			parametersRequired.put("p_currency_desc", currencyType.get(settledOperationsTO.getCurrency()));
//			parametersRequired.put("p_securityClassDesc", securityClassDesc);
//			parametersRequired.put("p_securityTypeDesc", securityType);
			
			/**GENERATE FILE EXCEL ***/
			arrayByteExcel=reportPoiGeneratorServiceBean.writeFileExcelSettledOperations(parametersRequired,reportLogger);
			
			/**SETTING ARRAY BYTE TO GENERIC REPORT FOR PERSIST**/
			List<byte[]> bytes = new ArrayList<>();
			bytes.add(arrayByteExcel);
			setListByte(bytes);

			return baos;
	}

	
	
}