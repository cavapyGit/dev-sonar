package com.pradera.report.generation.executor.securities;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.services.remote.stockcalculation.StockCalculationService;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.service.IssuerQueryServiceBean;
import com.pradera.core.component.helperui.to.IssuerSearcherTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.securities.service.IssuerListReportServiceBean;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "SecuritiesSectorClassReport")
public class SecuritiesSectorClassReport extends GenericReport{
private static final long serialVersionUID = 1L;
	
	@Inject  PraderaLogger log;
	
	@EJB
	private ParameterServiceBean parameterService;

	@EJB
	private IssuerListReportServiceBean issuerServiceBean;
	
	@Inject
	private Instance<StockCalculationService> stockCalculate;
	
	public SecuritiesSectorClassReport() {
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParametersQueryReport() {
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		if (listaLogger == null) {
			listaLogger = new ArrayList<ReportLoggerDetail>();
		}
	}

	
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		String issuerCode = null;
		String issuerMnemonic = null;
		Map<Integer,String> economic_sector = new HashMap<Integer, String>();
		Map<Integer,String> economic_activity = new HashMap<Integer, String>();
		Map<Integer,String> securityClass = new HashMap<Integer, String>();
		Map<Integer,String> mnemonic = new HashMap<Integer, String>();
		Map<Integer,String> mnemonic_ea = new HashMap<Integer, String>();
		String date = null;
		
		for (ReportLoggerDetail r : loggerDetails) {
		if (r.getFilterName().equals(ReportConstant.ISSUER_PARAM) && r.getFilterValue()!=null)
			issuerCode = r.getFilterValue();
		if (r.getFilterName().equals(ReportConstant.DATE_PARAM))
			date = r.getFilterValue();
		}
		
		//StockCalculationService
		Long idStockCalculationMarketFactToday = null;
		Date cutdate = CommonsUtilities.convertStringtoDate(date);
		
		LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());
		
		 try {
			 if(cutdate.before(CommonsUtilities.currentDate()))
			 idStockCalculationMarketFactToday = stockCalculate.get().executeStockCalculationMarketFactByCutDate(cutdate, cutdate, loggerUser);
		 } catch (Exception e) {
			e.printStackTrace();
		}

		try {
			
			if(issuerCode != null){
				Issuer iss = issuerServiceBean.find(Issuer.class, issuerCode);
				issuerMnemonic = iss.getMnemonic();//+ " - " + iss.getBusinessName();
			}
			
			filter.setMasterTableFk(MasterTableType.ECONOMIC_SECTOR.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				economic_sector.put(param.getParameterTablePk(), param.getParameterName());
			}
			filter.setMasterTableFk(MasterTableType.ECONOMIC_ACTIVITY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				economic_activity.put(param.getParameterTablePk(), param.getParameterName());
				mnemonic_ea.put(param.getParameterTablePk(), param.getIndicator1());
			}
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				securityClass.put(param.getParameterTablePk(), param.getParameterName());
				mnemonic.put(param.getParameterTablePk(), param.getText1());
			}
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		parametersRequired.put("issuerMnemonic", issuerMnemonic);
		parametersRequired.put("p_economic_sector", economic_sector);
		parametersRequired.put("p_economic_activity", economic_activity);
		parametersRequired.put("p_securityClass", securityClass);
		parametersRequired.put("p_mnemonic_sc", mnemonic);
		parametersRequired.put("p_mnemonic_ea", mnemonic_ea);
		parametersRequired.put("stock", idStockCalculationMarketFactToday);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		// TODO Auto-generated method stub
		return parametersRequired;
	}
}
