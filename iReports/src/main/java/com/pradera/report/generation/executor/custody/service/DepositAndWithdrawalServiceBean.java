package com.pradera.report.generation.executor.custody.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.report.generation.executor.account.service.AccountReportServiceBean;
import com.pradera.report.generation.executor.custody.to.GenericCustodyOperationTO;

/**
 * <ul>
 * <li>
 * Project iDepositary. Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class AccountReportServiceBean.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 12/07/2013
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class DepositAndWithdrawalServiceBean extends CrudDaoServiceBean {

	@EJB
	private AccountReportServiceBean accountReportService;
	
	/**
	 * Default constructor.
	 */
	public DepositAndWithdrawalServiceBean() {}
	
	public List<Date> getDateForExchangeRateValidation(GenericCustodyOperationTO to) {
		Map<String,Object> mapParam = new HashMap<String,Object>();
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append(" SELECT " );
  		sbQuery.append("  POD.REGISTRY_DATE " );
  
		sbQuery.append(" FROM physical_operation_detail POD " );
  		sbQuery.append("  inner join physical_certificate pc            on pc.id_physical_certificate_pk = pod.id_physical_certificate_fk " );
		sbQuery.append("  inner join physical_certificate_operation pco on pco.ID_PHYSICAL_OPERATION_PK = POD.ID_PHYSICAL_OPERATION_FK " );
		sbQuery.append("  inner join holder_account ha                  on ha.ID_HOLDER_ACCOUNT_PK = pco.ID_HOLDER_ACCOUNT_FK " );
		sbQuery.append("  inner join custody_operation co               on pco.id_physical_operation_pk = co.id_custody_operation_pk " );
		sbQuery.append("  inner join security se                        on pc.id_security_code_fk = se.id_security_code_pk " );
		sbQuery.append("  inner join physical_cert_marketfact pcm       on pcm.id_physical_certificate_fk = pc.id_physical_certificate_pk " );
		sbQuery.append("  inner join physical_balance_detail pbd        on pc.id_physical_certificate_pk = pbd.id_physical_certificate_fk " );
		  
		sbQuery.append(" WHERE pod.state = " );
		sbQuery.append("  CASE pco.physical_operation_type " );
		sbQuery.append("   WHEN 673 THEN 601 " );
		sbQuery.append("   WHEN 674 THEN 602 " );
		sbQuery.append("  END " );
		
		if(Validations.validateIsNotNull(to.getHolder())){
			sbQuery.append("  AND EXISTS (SELECT had.ID_HOLDER_FK " );
			sbQuery.append("   FROM HOLDER_ACCOUNT_DETAIL had " );
			sbQuery.append("   WHERE had.ID_HOLDER_FK = :cui " );
			sbQuery.append(" 	AND had.ID_HOLDER_ACCOUNT_FK = pco.ID_HOLDER_ACCOUNT_FK) " );
			mapParam.put("cui", to.getHolder());
		}
		if(Validations.validateIsNotNull(to.getHolderAccount())){
			sbQuery.append("  AND ha.id_holder_account_pk = :account_holder " );
			mapParam.put("account_holder", to.getHolderAccount());
		}
		if(Validations.validateIsNotNull(to.getSecurities())){
			sbQuery.append("  AND SE.ID_SECURITY_CODE_PK = :key_value " );
			mapParam.put("key_value", to.getSecurities());
		}
		if(Validations.validateIsNotNull(to.getSecurityClass())){
			sbQuery.append("  AND SE.SECURITY_CLASS = :class_security " );
			mapParam.put("class_security", to.getSecurityClass());
		}
		if(Validations.validateIsNotNull(to.getCurrency())){
			sbQuery.append("  AND se.currency = :currency " );
			mapParam.put("currency", to.getCurrency());
		}
		if(Validations.validateIsNotNull(to.getIssuer())){
			sbQuery.append("  AND SE.id_issuer_Fk = :issuer " );
			mapParam.put("issuer", to.getIssuer());
		}
		if(Validations.validateIsNotNull(to.getParticipant())){
			sbQuery.append("  AND ha.id_participant_fk = :participant " );
			mapParam.put("participant", to.getParticipant());
		}
		if(Validations.validateIsNotNull(to.getOperationType())){
			sbQuery.append("  AND pco.PHYSICAL_OPERATION_TYPE = :type_operation " );
			mapParam.put("type_operation", to.getOperationType());
		}
		sbQuery.append("  AND trunc(POD.REGISTRY_DATE) BETWEEN to_date(:date_initial,'dd/MM/yyyy') " );
		mapParam.put("date_initial", to.getInitialDate());
		
		sbQuery.append("   AND to_date(:date_end,'dd/MM/yyyy') " );
		mapParam.put("date_end", to.getFinalDate());
		
		sbQuery.append(" GROUP BY " );
		sbQuery.append("  POD.REGISTRY_DATE " );
		
		Query query = em.createNativeQuery(sbQuery.toString());
		@SuppressWarnings("rawtypes")
		Iterator it= mapParam.entrySet().iterator();
        while(it.hasNext()){
        	@SuppressWarnings("unchecked")
			Entry<String,Object> entry = (Entry<String,Object>)it.next();
        	query.setParameter(entry.getKey(), entry.getValue());
        }
        
		return (List<Date>)query.getResultList();
	}

	public BigDecimal getExchangeRate(Date date) throws ServiceException {
		DailyExchangeRates exchangeRate = new DailyExchangeRates();
		exchangeRate.setIdCurrency(CurrencyType.USD.getCode());
		exchangeRate.setDateRate(date);
		return accountReportService.getExchangeRateByCurrencyType(exchangeRate);
	}
}
