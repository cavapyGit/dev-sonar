package com.pradera.report.generation.executor.securities;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.securities.service.IssuerListReportServiceBean;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "SecuritiesRegistryIssuerReport")
public class SecuritiesRegistryIssuerReport extends GenericReport{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject  PraderaLogger log;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	private IssuerListReportServiceBean issuerServiceBean;

	public SecuritiesRegistryIssuerReport() {
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParametersQueryReport() {
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		if (listaLogger == null) {
			listaLogger = new ArrayList<ReportLoggerDetail>();
		}
	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		String issuerCode = null;
		String motiveCode = null;
		String issuerDescription = null;
		String motiveDescription = null;
		Map<Integer,String> motive = new HashMap<Integer, String>();
		
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.ISSUER_PARAM) && r.getFilterValue()!=null)
				issuerCode = r.getFilterValue();
			if (r.getFilterName().equals(ReportConstant.MOTIVE_PARAM) && r.getFilterValue()!=null)
				motiveCode = r.getFilterValue();
		}
		
		try {
			
			if(issuerCode != null){
				Issuer iss = issuerServiceBean.find(Issuer.class, issuerCode);
				issuerDescription = iss.getMnemonic() + " - " + iss.getBusinessName();
			}else
			{
				issuerDescription = "TODOS";
			}
			
			if(motiveCode != null){
				Integer requestState = new Integer(motiveCode);
				ParameterTable param = parameterService.getParameterTableById(requestState);
				motiveDescription = param.getDescription();
			}else
			{
				motiveDescription = "TODOS";
			}
			
			filter.setMasterTableFk(MasterTableType.DEMATERIALIZATION_MOTIVE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				motive.put(param.getParameterTablePk(), param.getParameterName());
			}
			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		parametersRequired.put("issuerDescription", issuerDescription);
		parametersRequired.put("p_motive", motive);
		parametersRequired.put("p_motiveDescription", motiveDescription);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		// TODO Auto-generated method stub
		return parametersRequired;
	}
}
