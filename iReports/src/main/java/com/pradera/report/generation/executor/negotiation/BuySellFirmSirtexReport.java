package com.pradera.report.generation.executor.negotiation;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;

@ReportProcess(name = "BuySellFirmSirtexReport")
public class BuySellFirmSirtexReport extends GenericReport{

	private static final long serialVersionUID = 1L;
	
	@Inject  PraderaLogger log;
		
	@EJB
	private ParameterServiceBean parameterService;

	public BuySellFirmSirtexReport() {
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParametersQueryReport() {

	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String, Object> parametersRequired = new HashMap<>();
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		String participantDescription = null;
		// TODO Auto-generated method stub
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.PARTICIPANT_PARAM))
				if(r.getFilterValue() != null){
					participantDescription = r.getFilterDescription();
				}else{
					participantDescription = "TODOS";
				}
		}
		
		parametersRequired.put("participant_description", participantDescription);
		
		return parametersRequired;
	}
	
}
