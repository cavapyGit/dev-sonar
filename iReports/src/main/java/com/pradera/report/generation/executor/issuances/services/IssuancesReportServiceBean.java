package com.pradera.report.generation.executor.issuances.services;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.type.DematerializationCertificateMotiveType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.report.generation.executor.issuances.to.GeneralListOfIssuerReportTO;
import com.pradera.report.generation.executor.issuances.to.GenericIssuanceTO;
import com.pradera.report.generation.executor.issuances.to.RelationshipOfBuyersOfBankSharesTO;
import com.pradera.report.generation.executor.issuances.to.SecuritiesDematerializationTO;
import com.pradera.report.generation.executor.issuances.to.StockOfSecuritiesByBookEntryTO;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class AccountReportServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 03/10/2013
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class IssuancesReportServiceBean extends CrudDaoServiceBean {

    /**
     * Default constructor. 
     */
    public IssuancesReportServiceBean() {
        // TODO Auto-generated constructor stub
    }
    
    /**
     * issue 924: query par ael reporte de REPORTE LISTADO GENERAL DE EMISORES POI (313)
     * @param parameters
     * @return
     */
    public String getQueryGeneralListIssuers(GeneralListOfIssuerReportTO parameters){
    	StringBuilder sd=new StringBuilder();
    	
    	sd.append(" SELECT                                                                                                  ");
    	sd.append("	   ID_ISSUER_PK,                                                        ");
    	sd.append("    BUSINESS_NAME,                                                       ");
    	sd.append("    MNEMONIC,                                                            ");
    	sd.append("    (select INDICATOR1 from PARAMETER_TABLE                              ");
    	sd.append("    where PARAMETER_TABLE_PK = i.DOCUMENT_TYPE) as DOCUMENT_TYPE,        ");
    	sd.append("    DOCUMENT_NUMBER,                                                     ");
    	sd.append("    ID_HOLDER_FK,                                                        ");
    	sd.append("    PHONE_NUMBER,                                                        ");
    	sd.append("    (select PARAMETER_NAME from PARAMETER_TABLE                             ");
    	sd.append("    where PARAMETER_TABLE_PK = i.ECONOMIC_ACTIVITY) as ECONOMIC_ACTIVITY,");
    	sd.append("    TO_CHAR(REGISTRY_DATE,'dd/MM/yyyy') fecha_registro,                  ");
    	sd.append("    (select DESCRIPTION from PARAMETER_TABLE                             ");
    	sd.append("    where PARAMETER_TABLE_PK = i.STATE_ISSUER) as STATE_ISSUER  			");
    	sd.append(" FROM  ISSUER I                                                                                          ");
    	sd.append(" WHERE 1 = 1                                                                                             ");
    	
    	if(Validations.validateIsNotNullAndNotEmpty(parameters.getOfferType()))
			sd.append("   AND (I.OFFER_TYPE = :offerType)                                                                       ");
    	
    	if(Validations.validateIsNotNullAndNotEmpty(parameters.getInitialDate())&&Validations.validateIsNotNullAndNotEmpty(parameters.getFinalDate()))
    		sd.append("   AND (I.REGISTRY_DATE BETWEEN to_date(:initialDate,'dd/mm/yyyy') AND to_date(:finalDate,'dd/mm/yyyy')) ");
    	
    	if(Validations.validateIsNotNullAndNotEmpty(parameters.getIdIssuerPk()))
    		sd.append("   AND (I.ID_ISSUER_PK = :idIssuer)                                                                      ");
    	
    	if(Validations.validateIsNotNullAndNotEmpty(parameters.getEconomicActivity()))
    		sd.append("   AND (I.ECONOMIC_ACTIVITY = :economicActivity)                                                         ");
    	
    	if(Validations.validateIsNotNullAndNotEmpty(parameters.getStateIssuer()))
    		sd.append("   AND (I.STATE_ISSUER = :stateHolder)                                                                   ");
    	
    	sd.append(" ORDER BY I.ECONOMIC_ACTIVITY,I.REGISTRY_DATE,I.BUSINESS_NAME											");
    	
    	String strQueryFormatted = sd.toString();
    	if(Validations.validateIsNotNullAndNotEmpty(parameters.getOfferType()))
    		strQueryFormatted = strQueryFormatted.replace(":offerType", parameters.getOfferType().toString());
    	
    	if(Validations.validateIsNotNullAndNotEmpty(parameters.getInitialDate())&&Validations.validateIsNotNullAndNotEmpty(parameters.getFinalDate())){
    		strQueryFormatted = strQueryFormatted.replace(":initialDate", "'"+parameters.getInitialDate()+"'");
    		strQueryFormatted = strQueryFormatted.replace(":finalDate", "'"+parameters.getFinalDate()+"'");
    	}
    	
    	if(Validations.validateIsNotNullAndNotEmpty(parameters.getIdIssuerPk()))
    		strQueryFormatted = strQueryFormatted.replace(":idIssuer", "'"+parameters.getIdIssuerPk()+"'");
    	
    	if(Validations.validateIsNotNullAndNotEmpty(parameters.getEconomicActivity()))
    		strQueryFormatted = strQueryFormatted.replace(":economicActivity", parameters.getEconomicActivity().toString());
    	
    	if(Validations.validateIsNotNullAndNotEmpty(parameters.getStateIssuer()))
    		strQueryFormatted = strQueryFormatted.replace(":stateHolder", parameters.getStateIssuer().toString());
    	
    	return strQueryFormatted;
    	
    }
    
    public String getQueryRelationOfHoldersSecuritiesByIssuer(GenericsFiltersTO gfto){
    	StringBuilder sbQuery= new StringBuilder();
    	
    	sbQuery.append("    SELECT  ");
    	sbQuery.append("     HM.CUT_DATE CUTOFF_DATE, ");
    	sbQuery.append("     HM.CUT_DATE REGISTRY_DATE, ");
    	sbQuery.append("     I.MNEMONIC, ");
    	sbQuery.append("     I.BUSINESS_NAME, ");
    	sbQuery.append("     S.SECURITY_CLASS, ");
    	sbQuery.append("     S.ID_SECURITY_CODE_PK, ");
    	sbQuery.append("     S.ISSUANCE_DATE, ");
    	sbQuery.append("     S.EXPIRATION_DATE, ");
    	sbQuery.append("     S.INTEREST_RATE RATE_VALUE, ");
    	sbQuery.append("     S.CURRENCY, ");
    	
    	sbQuery.append("     (SELECT LISTAGG(HAD.ID_HOLDER_FK,'<br>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
    	sbQuery.append("     FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS ID_HOLDER_PK, ");
    	sbQuery.append("     HA.ACCOUNT_NUMBER, ");
    	
    	sbQuery.append("     (SELECT LISTAGG(H.FULL_NAME,'<br>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
    	sbQuery.append("     FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
    	sbQuery.append("     WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ) AS FULL_NAME, ");
    	
    	sbQuery.append("     (SELECT LISTAGG((SELECT INDICATOR1 FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = H.DOCUMENT_TYPE),'<br>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
    	sbQuery.append("     FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
    	sbQuery.append("     WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ) AS DOCUMENT_TYPE, ");
    	
    	sbQuery.append("     (SELECT LISTAGG(H.DOCUMENT_NUMBER,'<br>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
    	sbQuery.append("     FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
    	sbQuery.append("     WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ) AS DOCUMENT_NUMBER, ");
    	
    	sbQuery.append("     NVL(HM.TOTAL_BALANCE,0) TOTAL_BALANCE, ");
    	sbQuery.append("     NVL(HM.AVAILABLE_BALANCE,0) DISPONIBLE, ");
    	sbQuery.append("     NVL(HM.BAN_BALANCE,0) EMBARGO, ");
    	sbQuery.append("     NVL(HM.PAWN_BALANCE,0) PRENDA, ");
    	sbQuery.append("     NVL(HM.ACCREDITATION_BALANCE,0) CAT, ");
    	sbQuery.append("     NVL(HM.REPORTING_BALANCE,0) REPORTADOR, ");
    	sbQuery.append("     HM.MARKET_RATE MARKET_RATE, ");
    	sbQuery.append("     ROUND(HM.MARKET_PRICE,2) MARKET_PRICE, ");
    	sbQuery.append("     NVL(ROUND(HM.MARKET_PRICE,2) * HM.TOTAL_BALANCE,0) VALOR_MERCADO ");

    	sbQuery.append(" FROM MARKET_FACT_VIEW HM ");
		sbQuery.append("   INNER JOIN HOLDER_ACCOUNT HA  ON HA.ID_HOLDER_ACCOUNT_PK = HM.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append("   INNER JOIN SECURITY S         ON S.ID_SECURITY_CODE_PK = HM.ID_SECURITY_CODE_PK ");
		sbQuery.append("   INNER JOIN ISSUER I           ON I.ID_ISSUER_PK = S.ID_ISSUER_FK ");
    	
    	sbQuery.append("   WHERE 1=1     ");
//    	sbQuery.append("	 AND  S.STATE_SECURITY = 131 "); DEBEN SE TODOS LOS VALORES A UNA FECHA DE CORTE
//    	optimizacion de consulta
//    	sbQuery.append("	 AND  trunc(HM.CUT_DATE) = TO_DATE('"+gfto.getInitialDt()+"','dd/MM/yyyy') ");
    	sbQuery.append("	 AND HM.CUT_DATE >= TRUNC (TO_DATE ('"+gfto.getInitialDt()+"', 'DD/MM/YYYY'), 'DD') ");
    	sbQuery.append("	 AND HM.CUT_DATE < TRUNC (TO_DATE ('"+gfto.getInitialDt()+"', 'DD/MM/YYYY'), 'DD') + 1 ");
    	sbQuery.append("	 AND TRUNC (TO_DATE ('"+gfto.getInitialDt()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+gfto.getInitialDt()+"', 'DD/MM/YYYY') ");
    	
    	sbQuery.append("	 AND  HM.TOTAL_BALANCE > 0 ");
    	if(gfto.getIdIssuer()!=null){
    		sbQuery.append(" AND  I.ID_ISSUER_PK=:issuer_code ");
    	}
    	if(gfto.getIdSecurityCodePk()!=null){
	    	sbQuery.append(" AND  S.ID_SECURITY_CODE_PK=:security_class_key  ");
    	}
		if(gfto.getCui()!=null){
    		sbQuery.append(" AND (EXISTS																																			");
			sbQuery.append(" 	(SELECT had.ID_HOLDER_FK																															");
			sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL had																														");
			sbQuery.append(" 	WHERE had.ID_HOLDER_FK = :cui_code																													");
			sbQuery.append(" 		AND had.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK))																						");
    	}
		if(gfto.getIdHolderAccountPk()!=null){
	    	sbQuery.append(" AND HA.ID_HOLDER_ACCOUNT_PK=:holder_account ");
		}
		if(gfto.getReportingHolders()!=null && gfto.getReportingHolders().equals(GeneralConstants.TWO_VALUE_INTEGER.toString())){
	    	sbQuery.append(" AND HM.REPORTING_BALANCE = 0");
		}
		if(gfto.getIssuanceDate()!=null){
	    	sbQuery.append(" AND trunc(S.ISSUANCE_DATE) = TO_DATE('"+gfto.getIssuanceDate()+"','dd/MM/yyyy') ");
		}

		if(gfto.getSecurityClass()!=null){
	    	sbQuery.append(" AND S.SECURITY_CLASS = "+gfto.getSecurityClass()+" ");
		}

    	sbQuery.append("   ORDER BY I.BUSINESS_NAME,S.SECURITY_CLASS,S.ID_SECURITY_CODE_PK,S.CURRENCY,HA.ACCOUNT_NUMBER ");
    	sbQuery.append("  ");
    	
    	return sbQuery.toString(); 
    }
     
    public String getQuerySecuritiesDematerialization(GenericsFiltersTO gfto, String week, String year){
    	  
    	StringBuilder sbQuery= new StringBuilder();
    	
    	sbQuery.append(" SELECT																												");
		sbQuery.append(" 	TSCB.CURRENCY, 																									");
		sbQuery.append(" 	(SELECT '('||TEXT1||') '||PARAMETER_NAME FROM PARAMETER_TABLE 													");
		sbQuery.append(" 											 WHERE PARAMETER_TABLE_PK = TSCB.CURRENCY) CURRENCY_MNEMONIC,			");
		sbQuery.append(" 	TSCB.SECURITY_CLASS, 																							");
		sbQuery.append(" 	(SELECT TEXT1 FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = TSCB.SECURITY_CLASS) SECURITY_CLASS_MNEMONIC, 	");
		
		sbQuery.append(" 	(SELECT SUM(DSCB.NOMINAL_AMOUNT) FROM DAILY_SECURITY_CLASS_BALANCE DSCB			 								");
		sbQuery.append(" 	WHERE TSCB.SECURITY_CLASS=DSCB.SECURITY_CLASS 																	");
		sbQuery.append(" 	AND TSCB.CURRENCY=DSCB.CURRENCY 																				");
		sbQuery.append(" 	AND DSCB.PROCESS_DATE BETWEEN TO_DATE('" + week + "','DD/MM/YYYY') 												");
		sbQuery.append(" 						  AND TO_DATE(:final_date,'DD/MM/YYYY')) NOMINAL_VALUE_WEEK,								");
		sbQuery.append(" 	(SELECT SUM(DSCB.STOCK_QUANTITY) FROM DAILY_SECURITY_CLASS_BALANCE DSCB			 								");
		sbQuery.append(" 	WHERE TSCB.SECURITY_CLASS=DSCB.SECURITY_CLASS 																	");
		sbQuery.append(" 	AND TSCB.CURRENCY=DSCB.CURRENCY 																				");
		sbQuery.append(" 	AND DSCB.PROCESS_DATE BETWEEN TO_DATE('" + week + "','DD/MM/YYYY') 												");
		sbQuery.append(" 						  AND TO_DATE(:final_date,'DD/MM/YYYY') ) TOTAL_BALANCE_WEEK, 								");
		sbQuery.append(" 	TSCB.YEAR_TOTAL_AMOUNT	 NOMINAL_VALUE_YEAR,																	");
		sbQuery.append(" 	TSCB.YEAR_TOTAL_BALANCE TOTAL_BALANCE_YEAR, 																	");
		sbQuery.append(" 	TSCB.TOTAL_AMOUNT NOMINAL_VALUE_ALL, 																			");
		sbQuery.append(" 	TSCB.TOTAL_BALANCE TOTAL_BALANCE_ALL, 																			");
		sbQuery.append(" 	NVL(CASE 																										");
		sbQuery.append(" 		WHEN TSCB.CURRENCY IN (1734,1304,430) THEN 1 * 																");
		sbQuery.append(" 			nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D 													");
		sbQuery.append(" 				WHERE TRUNC(D.DATE_RATE) = TO_DATE(:final_date,'DD/MM/YYYY') 										");
		sbQuery.append(" 					AND D.ID_CURRENCY = TSCB.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1)							");
		sbQuery.append(" 		WHEN TSCB.CURRENCY IN (127,1853) THEN 1 * 																		");
		sbQuery.append(" 			nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D 													");
		sbQuery.append(" 				WHERE TRUNC(D.DATE_RATE) = TO_DATE(:final_date,'DD/MM/YYYY') 										");
		sbQuery.append(" 					AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) 									");
		sbQuery.append(" 		ELSE 1 																										");
		sbQuery.append(" 	END,0) TC,																										");
		sbQuery.append(" 	NVL(CASE 																										");
		sbQuery.append(" 		WHEN TSCB.CURRENCY = 127 THEN 1 /																				");
		sbQuery.append(" 			nvl((SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D														");
		sbQuery.append(" 				WHERE TRUNC(D.DATE_RATE) = TO_DATE(:final_date,'DD/MM/YYYY') 										");
		sbQuery.append(" 					AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)									");
		sbQuery.append(" 		WHEN TSCB.CURRENCY IN (1734,1304) THEN 1 * 																	");
		sbQuery.append(" 			nvl((SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D 													");
		sbQuery.append(" 				WHERE TRUNC(D.DATE_RATE) = TO_DATE(:final_date,'DD/MM/YYYY')										");
		sbQuery.append(" 					AND D.ID_CURRENCY = TSCB.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) / 							");
		sbQuery.append(" 			nvl((SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D 													");
		sbQuery.append(" 				WHERE TRUNC(D.DATE_RATE) = TO_DATE(:final_date,'DD/MM/YYYY') 										");
		sbQuery.append(" 					AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)									");
		sbQuery.append(" 		WHEN TSCB.CURRENCY IN (430,1853) THEN 1 																		");
		sbQuery.append(" 		ELSE 1 																										");
		sbQuery.append(" 	END,0) FACTOR_TC_USD 																							");
		
		sbQuery.append(" FROM TOTAL_SECURITY_CLASS_BALANCE TSCB 																				");
		sbQuery.append(" where  TRUNC(TSCB.PROCESS_DATE) =TO_DATE(:final_date,'DD/MM/YYYY')													");
		sbQuery.append(" ORDER BY 																											");
		sbQuery.append(" 	CURRENCY_MNEMONIC, 																								");
		sbQuery.append(" 	SECURITY_CLASS_MNEMONIC 																						");
    	
    	String strQueryFormatted = sbQuery.toString();
    	strQueryFormatted = strQueryFormatted.replace(":final_date", "'"+gfto.getFinalDt()+"'");
    	
    	return strQueryFormatted;
    }
        /*Ref idReportPk : 75*/
    public List<SecuritiesDematerializationTO> getSecuritiesDesmaterializations(String strQuery){
		Query query = em.createNativeQuery(strQuery);
		List<Object[]> resultQuery = query.getResultList();
		SecuritiesDematerializationTO dematerializationTO;
		List<SecuritiesDematerializationTO> dematerializationTOs = new ArrayList<>();
		//ParameterTable table;
		for (Object[] objects : resultQuery) {
			dematerializationTO = new SecuritiesDematerializationTO();
			dematerializationTO.setCurrency(new BigDecimal(objects[0].toString()).intValue());
			dematerializationTO.setSecurtyClass(new BigDecimal(objects[2].toString()).intValue());
			dematerializationTO.setSecurtyClassMnemonic(objects[3].toString());
			dematerializationTO.setNominalValueMonthly(objects[4]!=null?new BigDecimal(objects[4].toString()): BigDecimal.ZERO);
			dematerializationTO.setTotalBalanceWeek(objects[5]!=null?new BigDecimal(objects[5].toString()):BigDecimal.ZERO);
			dematerializationTO.setNominalValueYear(objects[6]!=null?new BigDecimal(objects[6].toString()):BigDecimal.ZERO);
			dematerializationTO.setTotalBalanceYear(objects[7]!=null?new BigDecimal(objects[7].toString()):BigDecimal.ZERO);
			dematerializationTO.setNominalValueAll(objects[8]!=null?new BigDecimal(objects[8].toString()):BigDecimal.ZERO);
			dematerializationTO.setTotalBalanceAll(objects[9]!=null?new BigDecimal(objects[9].toString()):BigDecimal.ZERO);
			/*TC*/
			dematerializationTO.setExchangeRate(objects[10]!=null?new BigDecimal(objects[10].toString()):BigDecimal.ZERO);
			dematerializationTO.setExchangeRateDesc(objects[11].toString());
			dematerializationTOs.add(dematerializationTO);
		}
		return dematerializationTOs;
	}
    /*Ref idReportPk : 76*/
    public List<StockOfSecuritiesByBookEntryTO> getQueryStockOfSecuritiesByBookEntryTO(String strQuery){
    	List<StockOfSecuritiesByBookEntryTO> bookEntryTOs = new ArrayList<>();
    	StockOfSecuritiesByBookEntryTO bookEntryTO;
    	Query query = em.createNativeQuery(strQuery);
		List<Object[]> resultQuery = query.getResultList();
		for (Object[] objects : resultQuery) {
			bookEntryTO = new StockOfSecuritiesByBookEntryTO();
			bookEntryTO.setCurrency(new BigDecimal(objects[0].toString()).intValue());
			bookEntryTO.setCurrencyMnemonic(objects[1].toString());
			
			bookEntryTO.setExchangeRate(new BigDecimal(objects[2].toString()));
			bookEntryTO.setSecurtyClass(new BigDecimal(objects[3].toString()).intValue());
			bookEntryTO.setSecurtyClassMnemonic(objects[4].toString());
			
			bookEntryTO.setNominalValue(objects[5]!=null?new BigDecimal(objects[5].toString()): BigDecimal.ZERO);
			bookEntryTO.setNominalValueUSD(objects[6]!=null?new BigDecimal(objects[6].toString()): BigDecimal.ZERO);
			bookEntryTO.setTotalBalance(objects[7]!=null?new BigDecimal(objects[7].toString()): BigDecimal.ZERO);
			
			bookEntryTOs.add(bookEntryTO);
		}
    	return bookEntryTOs;
    }
     
    public String getQueryStockOfSecuritiesByBookEntryReport(GenericsFiltersTO gfto, Long idStkCalcProcess){
    	StringBuilder sbQuery= new StringBuilder();

    	sbQuery.append("	SELECT																			");
    	sbQuery.append("	S.CURRENCY,																		");
    	sbQuery.append("	NVL((SELECT TEXT1 FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK=S.CURRENCY) ,' ') MNEMONIC_CURRENCY, ");
	    sbQuery.append("	NVL((SELECT  D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D							");
	    sbQuery.append("		WHERE  D.ID_CURRENCY =														");
	    sbQuery.append("   			     (CASE WHEN S.CURRENCY IN (127,1853) THEN 430						");
	    sbQuery.append("   				ELSE S.CURRENCY END	)												");
	    sbQuery.append("			AND  D.ID_SOURCE_INFORMATION = 212										");
	    sbQuery.append("			AND TRUNC( D.DATE_RATE) = TO_DATE(:final_date,'dd/MM/yyyy')				");
	    sbQuery.append("	),0) EXCHANGE_RATE,																");
	    sbQuery.append("	S.SECURITY_CLASS,  NVL((SELECT TEXT1 FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK=S.SECURITY_CLASS) ,' ') MNEMONIC_CLASS, ");
	    sbQuery.append("	SUM(S.INITIAL_NOMINAL_VALUE * SCB.TOTAL_BALANCE) NOMINAL_VALUE,					");
	    sbQuery.append("	SUM(S.INITIAL_NOMINAL_VALUE * SCB.TOTAL_BALANCE *								");
	    sbQuery.append("		CASE																		");
	    sbQuery.append("			WHEN S.CURRENCY = 127 THEN 1 /											");
	    sbQuery.append("				nvl((SELECT D.BUY_PRICE												");
	    sbQuery.append("					FROM DAILY_EXCHANGE_RATES D										");
	    sbQuery.append("					WHERE TRUNC(D.DATE_RATE) = TO_DATE(:final_date,'dd/MM/yyyy')	");
	    sbQuery.append("						AND D.ID_CURRENCY = 430										");
	    sbQuery.append("						AND D.ID_SOURCE_INFORMATION = 212 ),1)						");
	    sbQuery.append("			WHEN S.CURRENCY IN (1734,1304) THEN 1 *									");
	    sbQuery.append("				nvl((SELECT D.BUY_PRICE												");
	    sbQuery.append("					FROM DAILY_EXCHANGE_RATES D										");
	    sbQuery.append("					WHERE TRUNC(D.DATE_RATE) = TO_DATE(:final_date,'dd/MM/yyyy')	");
	    sbQuery.append("						AND D.ID_CURRENCY = S.CURRENCY								");
	    sbQuery.append("						AND D.ID_SOURCE_INFORMATION = 212							");
	    sbQuery.append("				),1)/																");
	    sbQuery.append("				nvl((SELECT D.BUY_PRICE												");
	    sbQuery.append("					FROM DAILY_EXCHANGE_RATES D										");
	    sbQuery.append("					WHERE TRUNC(D.DATE_RATE) = TO_DATE(:final_date,'dd/MM/yyyy')	");
	    sbQuery.append("						AND D.ID_CURRENCY = 430										");
	    sbQuery.append("						AND D.ID_SOURCE_INFORMATION = 212							");
	    sbQuery.append("				),1)																");
	    sbQuery.append("			WHEN S.CURRENCY IN (430,1853) THEN 1									");
	    sbQuery.append("			ELSE 0																	");
	    sbQuery.append("		END																			");
	    sbQuery.append("	) NOMINAL_VALUE_USD,															");
	    sbQuery.append("	SUM(SCB.TOTAL_BALANCE) TOTAL_BALANCE											");
        
	    sbQuery.append("   FROM ACCOUNT_BALANCE_VIEW SCB ");
		sbQuery.append("   		INNER JOIN SECURITY S ON S.ID_SECURITY_CODE_PK = SCB.ID_SECURITY_CODE_PK ");
    	sbQuery.append("   WHERE 1=1 ");
    	sbQuery.append("        AND TOTAL_BALANCE > 0 ");
    	sbQuery.append("   		AND trunc(SCB.CUT_DATE) BETWEEN TO_DATE(:initial_date, 'dd/MM/yyyy') AND TO_DATE(:final_date, 'dd/MM/yyyy') ");
    	sbQuery.append("   GROUP BY S.CURRENCY,S.SECURITY_CLASS ");
    	sbQuery.append("   ORDER BY MNEMONIC_CURRENCY,MNEMONIC_CLASS ASC ");
    	sbQuery.append(" ");
    	
    	
    	return sbQuery.toString();
    }
    
    public String getRelationshipOfBuyersOfBankSharesReport(RelationshipOfBuyersOfBankSharesTO gfto){
    	StringBuilder sbQuery= new StringBuilder();
    	
    	sbQuery.append(" SELECT ");
    	sbQuery.append(" CO.OPERATION_DATE, ");
    	sbQuery.append(" CO.OPERATION_NUMBER, ");
    	sbQuery.append(" CO.OPERATION_STATE, ");
    	sbQuery.append(" HA.ID_HOLDER_ACCOUNT_PK, ");
    	sbQuery.append(" HA.ACCOUNT_NUMBER, ");
    	sbQuery.append(" S.ID_SECURITY_CODE_PK, ");
    	sbQuery.append(" AAO.TOTAL_BALANCE, ");
    	sbQuery.append(" AAO.ID_ANNOTATION_OPERATION_PK, ");
    	sbQuery.append(" AAO.STATE_ANNOTATION, ");
    	sbQuery.append(" CO.CONFIRM_DATE, ");
    	sbQuery.append(" AAO.COMMENTS, ");
    	sbQuery.append(" S.ID_SECURITY_CODE_ONLY, ");
    	sbQuery.append(" P.ID_PARTICIPANT_PK, ");
    	sbQuery.append(" P.DESCRIPTION, ");
    	sbQuery.append(" P.MNEMONIC ");
		sbQuery.append(" FROM  ACCOUNT_ANNOTATION_OPERATION AAO ");
    	sbQuery.append("  INNER JOIN SECURITY S ON S.ID_SECURITY_CODE_PK = AAO.ID_SECURITY_CODE_FK ");
	    sbQuery.append("  INNER JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK = AAO.ID_HOLDER_ACCOUNT_FK ");
	    sbQuery.append("  INNER JOIN PARTICIPANT P ON P.ID_PARTICIPANT_PK = HA.ID_PARTICIPANT_FK ");
	    sbQuery.append("  INNER JOIN CUSTODY_OPERATION CO ON CO.ID_CUSTODY_OPERATION_PK = AAO.ID_ANNOTATION_OPERATION_PK ");
	    sbQuery.append("   WHERE 1=1 ");
	    sbQuery.append(" AND AAO.MOTIVE=" +DematerializationCertificateMotiveType.BANK_SHARES_PRIMARY_COLOCATION_PURCHASE.getCode()+"");
	    if(Validations.validateIsNotNull(gfto.getInitialDate()) && Validations.validateIsNotNull(gfto.getFinalDate()) ){
	    	sbQuery.append("  AND trunc(CO.OPERATION_DATE) BETWEEN TO_DATE('" +gfto.getInitialDate()+ "','dd/MM/yyyy') AND TO_DATE('" +gfto.getFinalDate()+ "','dd/MM/yyyy') ");
	    } 
	    if(Validations.validateIsNotNull(gfto.getParticipant())) {
	    	sbQuery.append("  AND P.ID_PARTICIPANT_PK=" +gfto.getParticipant()+ " ");
	    }
	    if(Validations.validateIsNotNull(gfto.getCui())) {
	    	sbQuery.append(" AND (SELECT COUNT(*) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK AND HAD.ID_HOLDER_FK="+ gfto.getCui() + ")>0 ");
	    }
	    if(Validations.validateIsNotNull(gfto.getHolderAccount())) {
	    	sbQuery.append("  AND HA.ID_HOLDER_ACCOUNT_PK=" +gfto.getHolderAccount()+ " ");
	    }else if(Validations.validateListIsNotNullAndNotEmpty(gfto.getLstHolderAccountPk())){
	    	String cadena = GeneralConstants.EMPTY_STRING;
	    	for(int i = 0; i<gfto.getLstHolderAccountPk().size();i++){
	    		Long cad = (Long)gfto.getLstHolderAccountPk().get(i);
	    		if((gfto.getLstHolderAccountPk().size()-1)==i){
	    			cadena = cadena.concat(cad.toString());
	    		}else{
	    			cadena = cadena.concat(cad.toString());
	    			cadena = cadena.concat(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
	    		}
	    	}
	    	sbQuery.append("  AND HA.ID_HOLDER_ACCOUNT_PK in (" +cadena+ ") ");
	    }
	    if(Validations.validateIsNotNull(gfto.getSecurities())) {
	    	sbQuery.append("  AND S.ID_SECURITY_CODE_PK='" +gfto.getSecurities()+ "' ");
	    }
	    if(Validations.validateIsNotNull(gfto.getState())) {
	    	sbQuery.append("  AND AAO.STATE_ANNOTATION=" +gfto.getState()+ " ");
	    }
    	
	    sbQuery.append("  ORDER BY P.MNEMONIC,HA.ACCOUNT_NUMBER,S.ID_SECURITY_CODE_PK,CO.OPERATION_STATE,AAO.STATE_ANNOTATION ");
    	
    	
    	return sbQuery.toString();
    }
	public HolderAccount findHolderAccountByPk(Long idHolderAccountPk) throws ServiceException{
		HolderAccount holderAccount = new HolderAccount();
		try {
			StringBuilder sb = new StringBuilder();
			sb.append(" select ha from HolderAccount ha where ha.idHolderAccountPk="+idHolderAccountPk);
			Query query = em.createQuery(sb.toString());
			holderAccount = (HolderAccount)query.getSingleResult();
		}catch(NoResultException nex) {
			holderAccount = null;
		}
		return holderAccount;
	}
    public String getAccountEntriesForInstanceOfPlacement(GenericsFiltersTO gfto, String securitiesExcluded){
    	StringBuilder sbQuery= new StringBuilder();
    	
    	sbQuery.append(" SELECT ");
    	sbQuery.append(" ISS.ISSUANCE_DATE, ");
		sbQuery.append(" CO.OPERATION_DATE, ");
		sbQuery.append(" P.MNEMONIC AS PARTICIPANT,  ");
		sbQuery.append(" I.ID_HOLDER_FK, ");
		sbQuery.append(" HA.ID_HOLDER_ACCOUNT_PK, ");
		sbQuery.append(" HA.ACCOUNT_NUMBER, ");
		sbQuery.append(" S.ID_SECURITY_CODE_PK, ");
		sbQuery.append(" I.MNEMONIC AS ISSUER,  ");
		sbQuery.append(" S.INITIAL_NOMINAL_VALUE, ");
		sbQuery.append(" S.ID_SECURITY_CODE_PK AS DESCRIPTION, ");
		sbQuery.append(" (SELECT PT.DESCRIPTION FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK = S.SECURITY_CLASS) as SECURITY_CLASS, ");
		sbQuery.append(" AAO.TOTAL_BALANCE CIRCULATION_BALANCE, ");
		sbQuery.append(" (S.INITIAL_NOMINAL_VALUE*AAO.TOTAL_BALANCE) CURRENT_NOMINAL_VALUE, ");
        sbQuery.append(" NVL((SELECT NVL(MFV.MARKET_PRICE,0)*AAO.TOTAL_BALANCE FROM MARKET_FACT_VIEW MFV WHERE S.ID_SECURITY_CODE_PK=MFV.ID_SECURITY_CODE_PK ");
        sbQuery.append(" AND MFV.ID_HOLDER_ACCOUNT_PK = HA.ID_HOLDER_ACCOUNT_PK  AND MFV.ID_PARTICIPANT_PK = P.ID_PARTICIPANT_PK ");
        sbQuery.append(" AND TRUNC(MFV.CUT_DATE) = TO_DATE('"+gfto.getFinalDt()+"','dd/MM/yyyy') ");
		sbQuery.append(" ),0) MARKET_PRICE ,");
        sbQuery.append(" AAO.MOTIVE,");
        sbQuery.append(" S.CURRENCY ");
        sbQuery.append(" FROM HOLDER_ACCOUNT HA ");
        sbQuery.append(" INNER JOIN ACCOUNT_ANNOTATION_OPERATION AAO ON HA.ID_HOLDER_ACCOUNT_PK=AAO.ID_HOLDER_ACCOUNT_FK  ");
		sbQuery.append(" INNER JOIN CUSTODY_OPERATION CO ON AAO.ID_ANNOTATION_OPERATION_PK=CO.ID_CUSTODY_OPERATION_PK ");
		sbQuery.append(" INNER JOIN PARTICIPANT P ON HA.ID_PARTICIPANT_FK=P.ID_PARTICIPANT_PK ");
		sbQuery.append(" INNER JOIN SECURITY S ON AAO.ID_SECURITY_CODE_FK=S.ID_SECURITY_CODE_PK AND S.ID_SECURITY_CODE_PK=AAO.ID_SECURITY_CODE_FK ");
		sbQuery.append(" INNER JOIN ISSUER I ON S.ID_ISSUER_FK=I.ID_ISSUER_PK ");
		sbQuery.append(" INNER JOIN ISSUANCE ISS ON S.ID_ISSUANCE_CODE_FK=ISS.ID_ISSUANCE_CODE_PK ");
		sbQuery.append(" WHERE 1=1 ");
		sbQuery.append(" AND AAO.STATE_ANNOTATION=858 ");
		if(Validations.validateIsNotNull(gfto.getInitialDt())){
			sbQuery.append(" AND CO.CONFIRM_DATE >= TO_DATE('"+gfto.getInitialDt()+"','dd/MM/yyyy') ");
		}
		if(Validations.validateIsNotNull(gfto.getFinalDt()) ){
			sbQuery.append(" AND CO.CONFIRM_DATE <= TO_DATE('"+gfto.getFinalDt()+"','dd/MM/yyyy') ");
		}
		if(Validations.validateIsNotNull(gfto.getIdIssuer())){
			sbQuery.append(" AND I.ID_ISSUER_PK='"+gfto.getIdIssuer()+"' ");
		}
		if(Validations.validateIsNotNull(gfto.getParticipant())) {
			sbQuery.append(" AND P.ID_PARTICIPANT_PK="+gfto.getParticipant()+" ");
		}
		if(Validations.validateIsNotNull(gfto.getCui())) {
			sbQuery.append(" AND I.ID_HOLDER_FK="+gfto.getCui().toString()+" ");
		}
		if(Validations.validateIsNotNull(gfto.getIdHolderAccountPk())) {
			sbQuery.append(" AND HA.ID_HOLDER_ACCOUNT_PK="+gfto.getIdHolderAccountPk().toString()+" ");
		}
		if(Validations.validateIsNotNull(gfto.getIdSecurityCodePk())) {
			sbQuery.append(" AND S.ID_SECURITY_CODE_PK='"+gfto.getIdSecurityCodePk().toString()+"' ");
		}
		if(Validations.validateIsNotNull(securitiesExcluded)) {
			String[] strSecurities = securitiesExcluded.split(";");
			if(strSecurities.length>0){
				for (int i = 0; i < strSecurities.length; i++) {
					sbQuery.append(" AND S.ID_SECURITY_CODE_PK<>'"+strSecurities[i]+"' ");
				}
			}
		}
		sbQuery.append(" ORDER BY AAO.MOTIVE,SECURITY_CLASS,S.CURRENCY,ISS.ISSUANCE_DATE,CO.OPERATION_DATE,P.MNEMONIC,S.ID_SECURITY_CODE_PK,I.MNEMONIC ");
		sbQuery.append("  ");
				
    	return sbQuery.toString();
    }
    
    public String getSubTotalAccountEntriesForInstanceOfPlacement(GenericsFiltersTO gfto, String securitiesExcluded, Map<Integer,BigDecimal> tipoCambio){
    	StringBuilder sbQuery= new StringBuilder();

		sbQuery.append(" SELECT ");        
		sbQuery.append(" TB1.CURRENCY,PT.PARAMETER_NAME,NVL(TB1.INITIAL_VALUE,0) INITIAL_VALUE,NVL(TB1.CANTIDAD,0) CANTIDAD,NVL(TB2.MARKET_PRICE,0) MARKET_PRICE , ");     
		sbQuery.append(" NVL(TB1.nominalBOB,0) NOMINALBOB, NVL(TB1.nominalUSD,0) NOMINALUSD, NVL(TB2.marketBOB,0)*NVL(TB1.CANTIDAD,0) MARKETBOB, NVL(TB2.marketUSD,0)*NVL(TB1.CANTIDAD,0) MARKETUSD ");     
		sbQuery.append(" FROM PARAMETER_TABLE PT, ");      
		sbQuery.append(" (Select ");        
		sbQuery.append(" s.currency, ");       
		sbQuery.append(" sum(s.initial_nominal_value) initial_value, ");       
		sbQuery.append(" sum(AAO.total_balance) cantidad, ");   
		sbQuery.append(" sum(s.initial_nominal_value * AAO.total_balance * ");  
		sbQuery.append(" case WHEN S.CURRENCY = 127 THEN 1 ");
		sbQuery.append(" WHEN S.CURRENCY IN (1734,1304,430,1853) then nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = ");
		sbQuery.append(" TO_DATE('"+gfto.getFinalDt()+"','dd/MM/yyyy') ");
		sbQuery.append(" AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212 ),1) "); 
		sbQuery.append(" end) nominalBOB, ");   
		sbQuery.append(" sum(s.initial_nominal_value * AAO.total_balance * "); 
		sbQuery.append(" case WHEN S.CURRENCY = 127 THEN 1/nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+gfto.getFinalDt()+"','dd/MM/yyyy') AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212 ),1) "); 
		sbQuery.append(" WHEN S.CURRENCY IN (1734,1304) THEN 1 * nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+gfto.getFinalDt()+"','dd/MM/yyyy') AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212 ),1)/ ");
		sbQuery.append(" nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+gfto.getFinalDt()+"','dd/MM/yyyy') AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212 ),1) ");
		sbQuery.append(" WHEN S.CURRENCY IN (430,1853)  THEN 1 ");
		sbQuery.append(" ELSE 1 ");
		sbQuery.append(" END) nominalUSD ");  
		sbQuery.append(" FROM HOLDER_ACCOUNT HA "); 
		sbQuery.append(" INNER JOIN ACCOUNT_ANNOTATION_OPERATION AAO ON HA.ID_HOLDER_ACCOUNT_PK=AAO.ID_HOLDER_ACCOUNT_FK ");  
		sbQuery.append(" INNER JOIN CUSTODY_OPERATION CO ON AAO.ID_ANNOTATION_OPERATION_PK=CO.ID_CUSTODY_OPERATION_PK "); 
		sbQuery.append(" INNER JOIN PARTICIPANT P ON HA.ID_PARTICIPANT_FK=P.ID_PARTICIPANT_PK "); 
		sbQuery.append(" INNER JOIN SECURITY S ON AAO.ID_SECURITY_CODE_FK=S.ID_SECURITY_CODE_PK AND S.ID_SECURITY_CODE_PK=AAO.ID_SECURITY_CODE_FK "); 
		sbQuery.append(" INNER JOIN ISSUER I ON S.ID_ISSUER_FK=I.ID_ISSUER_PK "); 
		sbQuery.append(" INNER JOIN ISSUANCE ISS ON S.ID_ISSUANCE_CODE_FK=ISS.ID_ISSUANCE_CODE_PK "); 
		sbQuery.append(" WHERE 1=1 "); 
		sbQuery.append(" AND AAO.STATE_ANNOTATION=858 ");
		
		if(Validations.validateIsNotNull(gfto.getInitialDt())){
			sbQuery.append(" AND CO.CONFIRM_DATE >= TO_DATE('"+gfto.getInitialDt()+"','dd/MM/yyyy') ");
		}
		if(Validations.validateIsNotNull(gfto.getFinalDt()) ){
			sbQuery.append(" AND CO.CONFIRM_DATE <= TO_DATE('"+gfto.getFinalDt()+"','dd/MM/yyyy') ");
		}
		if(Validations.validateIsNotNull(gfto.getIdIssuer())){
			sbQuery.append(" AND I.ID_ISSUER_PK='"+gfto.getIdIssuer()+"' ");
		}
		if(Validations.validateIsNotNull(gfto.getParticipant())) {
			sbQuery.append(" AND P.ID_PARTICIPANT_PK="+gfto.getParticipant()+" ");
		}
		if(Validations.validateIsNotNull(gfto.getCui())) {
			sbQuery.append(" AND I.ID_HOLDER_FK="+gfto.getCui().toString()+" ");
		}
		if(Validations.validateIsNotNull(gfto.getIdHolderAccountPk())) {
			sbQuery.append(" AND HA.ID_HOLDER_ACCOUNT_PK="+gfto.getIdHolderAccountPk().toString()+" ");
		}
		if(Validations.validateIsNotNull(gfto.getIdSecurityCodePk())) {
			sbQuery.append(" AND S.ID_SECURITY_CODE_PK='"+gfto.getIdSecurityCodePk().toString()+"' ");
		}
		if(Validations.validateIsNotNull(securitiesExcluded)) {
			String[] strSecurities = securitiesExcluded.split(";");
			if(strSecurities.length>0){
				for (int i = 0; i < strSecurities.length; i++) {
					sbQuery.append(" AND S.ID_SECURITY_CODE_PK<>'"+strSecurities[i]+"' ");
				}
			}
		}		 
		sbQuery.append(" GROUP BY s.CURRENCY ) TB1, ");   
		sbQuery.append(" (SELECT ");    
		sbQuery.append(" S.CURRENCY, ");   
		sbQuery.append(" sum(MFV.market_price) market_price, ");     
		sbQuery.append(" sum(MFV.market_price *  AAO.total_balance * "); 
		sbQuery.append(" CASE WHEN S.CURRENCY = 127 THEN 1 ");
		sbQuery.append(" WHEN S.CURRENCY IN (1734,1304,430,1853) then nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+gfto.getFinalDt()+"','dd/MM/yyyy') AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212 ),1) "); 
		sbQuery.append(" end) marketBOB, "); 
		sbQuery.append(" sum(MFV.market_price *  AAO.total_balance * "); 
		sbQuery.append(" case WHEN S.CURRENCY = 127 THEN 1/nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+gfto.getFinalDt()+"','dd/MM/yyyy') AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212 ),1) "); 
		sbQuery.append(" WHEN S.CURRENCY IN (1734,1304) THEN 1 * nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+gfto.getFinalDt()+"','dd/MM/yyyy') AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212 ),1)/ ");
		sbQuery.append(" nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+gfto.getFinalDt()+"','dd/MM/yyyy') AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212 ),1) ");
		sbQuery.append(" WHEN S.CURRENCY IN (430,1853)  THEN 1 ");
		sbQuery.append(" ELSE 1 ");
		sbQuery.append(" end) marketUSD ");   
		sbQuery.append(" FROM HOLDER_ACCOUNT HA "); 
		sbQuery.append(" INNER JOIN ACCOUNT_ANNOTATION_OPERATION AAO ON HA.ID_HOLDER_ACCOUNT_PK=AAO.ID_HOLDER_ACCOUNT_FK ");  
		sbQuery.append(" INNER JOIN CUSTODY_OPERATION CO ON AAO.ID_ANNOTATION_OPERATION_PK=CO.ID_CUSTODY_OPERATION_PK "); 
		sbQuery.append(" INNER JOIN PARTICIPANT P ON HA.ID_PARTICIPANT_FK=P.ID_PARTICIPANT_PK "); 
		sbQuery.append(" INNER JOIN SECURITY S ON AAO.ID_SECURITY_CODE_FK=S.ID_SECURITY_CODE_PK AND S.ID_SECURITY_CODE_PK=AAO.ID_SECURITY_CODE_FK "); 
		sbQuery.append(" INNER JOIN ISSUER I ON S.ID_ISSUER_FK=I.ID_ISSUER_PK "); 
		sbQuery.append(" INNER JOIN ISSUANCE ISS ON S.ID_ISSUANCE_CODE_FK=ISS.ID_ISSUANCE_CODE_PK "); 
		sbQuery.append(" left join MARKET_FACT_VIEW MFV on S.ID_SECURITY_CODE_PK=MFV.ID_SECURITY_CODE_PK AND TRUNC(MFV.CUT_DATE) = TO_DATE('07/05/2015','dd/MM/yyyy') ");
		sbQuery.append(" WHERE 1=1 "); 
		sbQuery.append(" AND AAO.STATE_ANNOTATION=858 ");

		if(Validations.validateIsNotNull(gfto.getInitialDt())){
			sbQuery.append(" AND CO.CONFIRM_DATE >= TO_DATE('"+gfto.getInitialDt()+"','dd/MM/yyyy') ");
		}
		if(Validations.validateIsNotNull(gfto.getFinalDt()) ){
			sbQuery.append(" AND CO.CONFIRM_DATE <= TO_DATE('"+gfto.getFinalDt()+"','dd/MM/yyyy') ");
		}
		if(Validations.validateIsNotNull(gfto.getIdIssuer())){
			sbQuery.append(" AND I.ID_ISSUER_PK='"+gfto.getIdIssuer()+"' ");
		}
		if(Validations.validateIsNotNull(gfto.getParticipant())) {
			sbQuery.append(" AND P.ID_PARTICIPANT_PK="+gfto.getParticipant()+" ");
		}
		if(Validations.validateIsNotNull(gfto.getCui())) {
			sbQuery.append(" AND I.ID_HOLDER_FK="+gfto.getCui().toString()+" ");
		}
		if(Validations.validateIsNotNull(gfto.getIdHolderAccountPk())) {
			sbQuery.append(" AND HA.ID_HOLDER_ACCOUNT_PK="+gfto.getIdHolderAccountPk().toString()+" ");
		}
		if(Validations.validateIsNotNull(gfto.getIdSecurityCodePk())) {
			sbQuery.append(" AND S.ID_SECURITY_CODE_PK='"+gfto.getIdSecurityCodePk().toString()+"' ");
		}
		if(Validations.validateIsNotNull(securitiesExcluded)) {
			String[] strSecurities = securitiesExcluded.split(";");
			if(strSecurities.length>0){
				for (int i = 0; i < strSecurities.length; i++) {
					sbQuery.append(" AND S.ID_SECURITY_CODE_PK<>'"+strSecurities[i]+"' ");
				}
			}
			}    			 
		sbQuery.append(" GROUP BY S.CURRENCY) TB2 ");   
    	sbQuery.append(" WHERE PT.PARAMETER_TABLE_PK=TB1.CURRENCY(+) ");    
		sbQuery.append(" AND TB1.CURRENCY=TB2.CURRENCY    ");
		sbQuery.append(" AND PT.MASTER_TABLE_FK=53 ");  
		sbQuery.append(" ORDER BY PT.PARAMETER_TABLE_CD ");
		sbQuery.append("  ");
				
    	return sbQuery.toString();
    }
    
    public String getModificationCfiReport(GenericsFiltersTO gfto){
    	StringBuilder sbQuery= new StringBuilder();

    	sbQuery.append(" SELECT ");
		sbQuery.append(" CFI.MODIFICATION_DATE, ");
    	sbQuery.append(" S.ID_SECURITY_CODE_PK, ");
    	sbQuery.append(" CFI.PREVIOUS_CFI_CODE, ");
    	sbQuery.append(" CFI.NEW_CFI_CODE, ");
    	sbQuery.append(" CFI.MODIFICATION_USER ");
    	sbQuery.append(" FROM CFI_HISTORY CFI ");
		sbQuery.append(" INNER JOIN SECURITY S ON CFI.ID_SECURITY_CODE_FK=S.ID_SECURITY_CODE_PK ");
		sbQuery.append(" WHERE 1=1 ");
    	sbQuery.append("  AND CFI.MODIFICATION_USER BETWEEN TO_DATE('" +gfto.getInitialDt()+ "','dd/MM/yyyy') AND TO_DATE('" +gfto.getFinalDt()+ "','dd/MM/yyyy') ");
	    if(Validations.validateIsNotNull(gfto.getIdSecurityCodePk())) {
	    	sbQuery.append("  AND S.ID_SECURITY_CODE_PK='" +gfto.getIdSecurityCodePk()+ "' ");
	    }
    	
    	return sbQuery.toString();
    }
    
    /**
     * issue 1341: esta queryesta basada en el metodo: getDetailsOfTitlesPendingOfExpirationReportTo, se realizaron adecuaciones para que sea un reporte para el agente pagador
     * @param gfto
     * @return
     */
    public String getDetailsOfTitlesPendingOfExpirationReportToPaymentAgent(GenericIssuanceTO gfto){
    	StringBuilder sbQuery= new StringBuilder();
		Date currentDate = CommonsUtilities.currentDate();
		String currDateStr = CommonsUtilities.convertDatetoStringForTemplate(currentDate, CommonsUtilities.DATE_PATTERN);
    	
		if(gfto.getSecuritiesSituation().equals(GeneralConstants.ONE_VALUE_STRING)){
			sbQuery.append(" SELECT  ");
			
			sbQuery.append(" CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416)  ");
			sbQuery.append("   THEN S.EXPIRATION_DATE ");
			sbQuery.append("   ELSE PIC.EXPERITATION_DATE  ");
			sbQuery.append(" END AS FECHA_EXP,   ");
		  
			sbQuery.append("  CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) ");
			sbQuery.append("   THEN TO_CHAR(S.EXPIRATION_DATE, 'DD/MM/YYYY') ");
			sbQuery.append("      ELSE TO_CHAR(PIC.EXPERITATION_DATE, 'DD/MM/YYYY') ");
			sbQuery.append("   END AS FECHA_EXP_STR,   ");
			
			sbQuery.append("   (SELECT PAR.PARAMETER_NAME FROM PARAMETER_TABLE PAR WHERE PAR.PARAMETER_TABLE_PK = S.SECURITY_CLASS) AS CLASE_VALOR, ");
			sbQuery.append("   S.SECURITY_CLASS AS CODIGO_CLASE_VALOR, ");
			sbQuery.append("   (SELECT PAR.TEXT1 FROM PARAMETER_TABLE PAR WHERE PAR.PARAMETER_TABLE_PK = S.CURRENCY) AS MONEDA, ");
			sbQuery.append("   S.CURRENCY AS CODIGO_MONEDA, ");
			sbQuery.append("   P.MNEMONIC AS PARTICIPANT, ");
			sbQuery.append("   HA.ACCOUNT_NUMBER AS NRO_CUENTA, ");
			sbQuery.append("   (SELECT LISTAGG(H.ID_HOLDER_PK,'<BR>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
			sbQuery.append("    FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
			sbQuery.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS CUI, ");
			sbQuery.append("   (SELECT LISTAGG(H.FULL_NAME,'<BR>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
			sbQuery.append("    FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
			sbQuery.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS CUI_DESCRIPTION, ");
			sbQuery.append("   S.ID_SECURITY_CODE_PK AS COD_VALOR, ");
			
			sbQuery.append("	CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) ");
			sbQuery.append("	THEN '-' ");
			sbQuery.append("	ELSE (PIC.COUPON_NUMBER || '/' ||    ");
			sbQuery.append("	(SELECT MAX(PIC_SUB.COUPON_NUMBER) FROM INTEREST_PAYMENT_SCHEDULE IPS_SUB, PROGRAM_INTEREST_COUPON PIC_SUB  "); 
			sbQuery.append("	WHERE IPS_SUB.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK AND   ");
			sbQuery.append("	PIC_SUB.ID_INT_PAYMENT_SCHEDULE_fK = IPS_SUB.ID_INT_PAYMENT_SCHEDULE_PK)) ");
			sbQuery.append("	 END  AS CUPON,  ");
			
			sbQuery.append("   HAB.TOTAL_BALANCE AS TOTAL_CONTABLE, ");
			sbQuery.append("   HAB.AVAILABLE_BALANCE AS DISPONIBLE, ");
			sbQuery.append("   HAB.PAWN_BALANCE AS PRENDA, ");
			sbQuery.append("   HAB.BAN_BALANCE AS EMBARGO, ");
			sbQuery.append("   HAB.ACCREDITATION_BALANCE AS ACREDITACION, ");
			sbQuery.append("   HAB.REPORTING_BALANCE AS REPORTADOR, ");
			sbQuery.append("   ROUND(NVL(PIC.COUPON_AMOUNT,0),2) AS MONTO_INTERES, ");
			sbQuery.append("   ROUND(NVL( 			");
			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");
			sbQuery.append("		 ,0),2) AS MONTO_AMORTIZACION, ");
			sbQuery.append("   (ROUND(NVL(PIC.COUPON_AMOUNT,0),2) + ROUND(NVL( ");
			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");
			sbQuery.append("  ,0),2)) AS MONTO_TOTAL, ");
			sbQuery.append("   (ROUND(NVL(PIC.COUPON_AMOUNT,0),2) + ROUND(NVL( ");
			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");	
			sbQuery.append("		,0),2)) * HAB.TOTAL_BALANCE AS TOTAL, ");
			sbQuery.append("    NVL( ");
			sbQuery.append(" 		CASE ");     
			sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1 ");     
			sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304,430) THEN 1 * "); 
			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
			sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) ");
			sbQuery.append(" 			WHEN S.CURRENCY IN (1853) THEN 1 * "); 
			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
			sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			sbQuery.append(" 			ELSE 1 ");   
			sbQuery.append(" 	END,1) MONTO_BOB, ");
			sbQuery.append("    NVL( ");
			sbQuery.append(" 		CASE ");
			sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1 / ");
			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
			sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304) THEN 1 *	");
			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
			sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) / ");
			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
			sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			sbQuery.append(" 			WHEN S.CURRENCY IN (430,1853) THEN 1 ");
			sbQuery.append(" 			ELSE 1 ");
			sbQuery.append(" 	END,0) MONTO_USD ");
			
			sbQuery.append("  ");
			sbQuery.append(" FROM HOLDER_ACCOUNT_BALANCE hab ");
			sbQuery.append("   INNER JOIN SECURITY S                         ON s.ID_SECURITY_CODE_PK=hab.ID_SECURITY_CODE_PK ");
			sbQuery.append("   INNER JOIN HOLDER_ACCOUNT ha                  ON hab.ID_HOLDER_ACCOUNT_PK= ha.ID_HOLDER_ACCOUNT_PK ");
			sbQuery.append("   INNER JOIN ISSUER I                           ON S.ID_ISSUER_FK = I.ID_ISSUER_PK ");
			sbQuery.append("   INNER JOIN PARTICIPANT P                      ON P.ID_PARTICIPANT_PK = HAB.ID_PARTICIPANT_PK ");
			sbQuery.append("   LEFT JOIN interest_payment_schedule ips      ON s.id_security_code_pk=ips.id_security_code_fk ");
			sbQuery.append("   LEFT JOIN program_interest_coupon pic        ON ips.id_int_payment_schedule_pk=pic.id_int_payment_schedule_fk ");
			sbQuery.append("   LEFT JOIN amortization_payment_schedule APS   ON APS.ID_SECURITY_CODE_FK = S.id_security_code_pk  ");
			sbQuery.append("   LEFT JOIN PROGRAM_AMORTIZATION_COUPON PAC     ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK ");
			sbQuery.append("                                                 AND TRUNC(PIC.EXPERITATION_DATE) = TRUNC(PAC.EXPRITATION_DATE) ");
			sbQuery.append(" WHERE 1 = 1 ");
	//		sbQuery.append("   AND trunc(PIC.EXPERITATION_DATE) >= to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') ");
	//		sbQuery.append("   AND trunc(PIC.EXPERITATION_DATE) <= to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy') ");
			
			sbQuery.append("  AND (trunc(PIC.EXPERITATION_DATE) between to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') and to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy')  ");
			sbQuery.append("   OR CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) ");
			sbQuery.append("    THEN trunc(S.EXPIRATION_DATE)  END ");
			sbQuery.append("   between to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') and to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy')) ");
			
			sbQuery.append("   AND HAB.TOTAL_BALANCE > 0 ");
			
			if(Validations.validateIsNotNull(gfto.getCui())){
				sbQuery.append(" AND (SELECT COUNT(*) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK=HAB.ID_HOLDER_ACCOUNT_PK AND HAD.ID_HOLDER_FK="+ gfto.getCui() + ")>0 ");
			}
			if(Validations.validateIsNotNull(gfto.getHolderAccount())){
				sbQuery.append(" AND HAB.ID_HOLDER_ACCOUNT_PK="+gfto.getHolderAccount()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getParticipantMnemonic())){
				//sbQuery.append(" AND HAB.ID_PARTICIPANT_PK="+gfto.getParticipant()+" ");
				// issue 1341: en este reporte se busca por agente pagador del valor
				sbQuery.append(" AND S.PAYMENT_AGENT='"+gfto.getParticipantMnemonic()+"' ");
				// por requerimiento de usuario se omite DPFs
				sbQuery.append(" AND S.SECURITY_CLASS != 420 ");
			}
			if(Validations.validateIsNotNull(gfto.getSecurities())){
				sbQuery.append(" AND S.ID_SECURITY_CODE_PK='"+gfto.getSecurities()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getIssuer())){
				sbQuery.append(" AND S.ID_ISSUER_FK='"+gfto.getIssuer()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getCurrency())){
				sbQuery.append(" AND S.CURRENCY="+gfto.getCurrency()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getSecurityClass())){
				sbQuery.append(" AND S.SECURITY_CLASS="+gfto.getSecurityClass()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getIndAutomatic())){
				sbQuery.append(" AND S.SECURITY_CLASS NOT IN (415,414,420) ");//excluimos BBX,BTX,DPF
			}
			
			sbQuery.append(" UNION ");
			
			// solo para el subproducto 
			sbQuery.append(" SELECT  ");
			
			sbQuery.append(" CASE WHEN S.SECURITY_CLASS in (409)  ");
			sbQuery.append("   THEN S.EXPIRATION_DATE ");
			//sbQuery.append("   ELSE PIC.EXPERITATION_DATE  ");
			sbQuery.append(" END AS FECHA_EXP,   ");
		  
			sbQuery.append("  CASE WHEN S.SECURITY_CLASS in (409) ");
			sbQuery.append("   THEN TO_CHAR(S.EXPIRATION_DATE, 'DD/MM/YYYY') ");
			//sbQuery.append("      ELSE TO_CHAR(PIC.EXPERITATION_DATE, 'DD/MM/YYYY') ");
			sbQuery.append("   END AS FECHA_EXP_STR,   ");
			
			sbQuery.append("   (SELECT PAR.PARAMETER_NAME FROM PARAMETER_TABLE PAR WHERE PAR.PARAMETER_TABLE_PK = S.SECURITY_CLASS) AS CLASE_VALOR, ");
			sbQuery.append("   S.SECURITY_CLASS AS CODIGO_CLASE_VALOR, ");
			sbQuery.append("   (SELECT PAR.TEXT1 FROM PARAMETER_TABLE PAR WHERE PAR.PARAMETER_TABLE_PK = S.CURRENCY) AS MONEDA, ");
			sbQuery.append("   S.CURRENCY AS CODIGO_MONEDA, ");
			sbQuery.append("   P.MNEMONIC AS PARTICIPANT, ");
			sbQuery.append("   HA.ACCOUNT_NUMBER AS NRO_CUENTA, ");
			sbQuery.append("   (SELECT LISTAGG(H.ID_HOLDER_PK,'<BR>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
			sbQuery.append("    FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
			sbQuery.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS CUI, ");
			sbQuery.append("   (SELECT LISTAGG(H.FULL_NAME,'<BR>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
			sbQuery.append("    FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
			sbQuery.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS CUI_DESCRIPTION, ");
			sbQuery.append("   S.ID_SECURITY_CODE_PK AS COD_VALOR, ");
			
			sbQuery.append("	CASE WHEN S.SECURITY_CLASS in (409) ");
			sbQuery.append("	THEN '-' ");
			//sbQuery.append("	ELSE (PIC.COUPON_NUMBER || '/' ||    ");
			//sbQuery.append("	(SELECT MAX(PIC_SUB.COUPON_NUMBER) FROM INTEREST_PAYMENT_SCHEDULE IPS_SUB, PROGRAM_INTEREST_COUPON PIC_SUB  "); 
			//sbQuery.append("	WHERE IPS_SUB.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK AND   ");
			//sbQuery.append("	PIC_SUB.ID_INT_PAYMENT_SCHEDULE_fK = IPS_SUB.ID_INT_PAYMENT_SCHEDULE_PK)) ");
			sbQuery.append("	 END  AS CUPON,  ");
			
			sbQuery.append("   HAB.TOTAL_BALANCE AS TOTAL_CONTABLE, ");
			sbQuery.append("   HAB.AVAILABLE_BALANCE AS DISPONIBLE, ");
			sbQuery.append("   HAB.PAWN_BALANCE AS PRENDA, ");
			sbQuery.append("   HAB.BAN_BALANCE AS EMBARGO, ");
			sbQuery.append("   HAB.ACCREDITATION_BALANCE AS ACREDITACION, ");
			sbQuery.append("   HAB.REPORTING_BALANCE AS REPORTADOR, ");
			//sbQuery.append("   ROUND(NVL(PIC.COUPON_AMOUNT,0),2) AS MONTO_INTERES, ");
			sbQuery.append("   0 AS MONTO_INTERES, ");
			sbQuery.append("   ROUND(NVL( 			");
			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (409) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");
			sbQuery.append("		 ,0),2) AS MONTO_AMORTIZACION, ");
			//sbQuery.append("   (ROUND(NVL(PIC.COUPON_AMOUNT,0),2) + ROUND(NVL( ");
			sbQuery.append("   (0 + ROUND(NVL( ");
			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (409) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");
			sbQuery.append("  ,0),2)) AS MONTO_TOTAL, ");
			//sbQuery.append("   (ROUND(NVL(PIC.COUPON_AMOUNT,0),2) + ROUND(NVL( ");
			sbQuery.append("   (0 + ROUND(NVL( ");
			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (409) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");	
			sbQuery.append("		,0),2)) * HAB.TOTAL_BALANCE AS TOTAL, ");
			sbQuery.append("    NVL( ");
			sbQuery.append(" 		CASE ");     
			sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1 ");     
			sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304,430) THEN 1 * "); 
			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
			sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) ");
			sbQuery.append(" 			WHEN S.CURRENCY IN (1853) THEN 1 * "); 
			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
			sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			sbQuery.append(" 			ELSE 1 ");   
			sbQuery.append(" 	END,1) MONTO_BOB, ");
			sbQuery.append("    NVL( ");
			sbQuery.append(" 		CASE ");
			sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1 / ");
			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
			sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304) THEN 1 *	");
			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
			sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) / ");
			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
			sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			sbQuery.append(" 			WHEN S.CURRENCY IN (430,1853) THEN 1 ");
			sbQuery.append(" 			ELSE 1 ");
			sbQuery.append(" 	END,0) MONTO_USD ");
			
			sbQuery.append("  ");
			sbQuery.append(" FROM HOLDER_ACCOUNT_BALANCE hab ");
			sbQuery.append("   INNER JOIN SECURITY S                         ON s.ID_SECURITY_CODE_PK=hab.ID_SECURITY_CODE_PK ");
			sbQuery.append("   INNER JOIN HOLDER_ACCOUNT ha                  ON hab.ID_HOLDER_ACCOUNT_PK= ha.ID_HOLDER_ACCOUNT_PK ");
			sbQuery.append("   INNER JOIN ISSUER I                           ON S.ID_ISSUER_FK = I.ID_ISSUER_PK ");
			sbQuery.append("   INNER JOIN PARTICIPANT P                      ON P.ID_PARTICIPANT_PK = HAB.ID_PARTICIPANT_PK ");
			//sbQuery.append("   LEFT JOIN interest_payment_schedule ips      ON s.id_security_code_pk=ips.id_security_code_fk ");
			//sbQuery.append("   LEFT JOIN program_interest_coupon pic        ON ips.id_int_payment_schedule_pk=pic.id_int_payment_schedule_fk ");
			sbQuery.append("   LEFT JOIN amortization_payment_schedule APS   ON APS.ID_SECURITY_CODE_FK = S.id_security_code_pk  ");
			sbQuery.append("   LEFT JOIN PROGRAM_AMORTIZATION_COUPON PAC     ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK ");
			//sbQuery.append("                                                 AND TRUNC(PIC.EXPERITATION_DATE) = TRUNC(PAC.EXPRITATION_DATE) ");
			sbQuery.append(" WHERE 1 = 1 ");
			
			//sbQuery.append("  AND (trunc(PIC.EXPERITATION_DATE) between to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') and to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy')  ");
			//sbQuery.append("   OR CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) ");
			//sbQuery.append("    THEN trunc(S.EXPIRATION_DATE)  END ");
			//sbQuery.append("   between to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') and to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy')) ");
			
			sbQuery.append("   AND S.SECURITY_CLASS IN (409) ");
			sbQuery.append("   AND TRUNC(S.EXPIRATION_DATE) BETWEEN to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') AND to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy') ");
			sbQuery.append("   AND s.ID_REF_SECURITY_CODE_FK IS NOT NULL ");
			
			sbQuery.append("   AND HAB.TOTAL_BALANCE > 0 ");
			
			if(Validations.validateIsNotNull(gfto.getCui())){
				sbQuery.append(" AND (SELECT COUNT(*) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK=HAB.ID_HOLDER_ACCOUNT_PK AND HAD.ID_HOLDER_FK="+ gfto.getCui() + ")>0 ");
			}
			if(Validations.validateIsNotNull(gfto.getHolderAccount())){
				sbQuery.append(" AND HAB.ID_HOLDER_ACCOUNT_PK="+gfto.getHolderAccount()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getParticipantMnemonic())){
				// sbQuery.append(" AND HAB.ID_PARTICIPANT_PK="+gfto.getParticipant()+" ");
				// issue 1341: en este reporte se busca por agente pagador del valor
				sbQuery.append(" AND S.PAYMENT_AGENT='"+gfto.getParticipantMnemonic()+"' ");
				// por requerimiento de usuario se omite DPFs
				sbQuery.append(" AND S.SECURITY_CLASS != 420 ");
			}
			if(Validations.validateIsNotNull(gfto.getSecurities())){
				sbQuery.append(" AND S.ID_SECURITY_CODE_PK='"+gfto.getSecurities()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getIssuer())){
				sbQuery.append(" AND S.ID_ISSUER_FK='"+gfto.getIssuer()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getCurrency())){
				sbQuery.append(" AND S.CURRENCY="+gfto.getCurrency()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getSecurityClass())){
				sbQuery.append(" AND S.SECURITY_CLASS="+gfto.getSecurityClass()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getIndAutomatic())){
				sbQuery.append(" AND S.SECURITY_CLASS NOT IN (415,414,420) ");//excluimos BBX,BTX,DPF
			}
			
		}else{
			sbQuery.append(" SELECT      ");
			
			sbQuery.append(" CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416)  ");
			sbQuery.append("   THEN S.EXPIRATION_DATE ");
			sbQuery.append("   ELSE PIC.EXPERITATION_DATE  ");
			sbQuery.append(" END AS FECHA_EXP,   ");
		  
			sbQuery.append("  CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) ");
			sbQuery.append("   THEN TO_CHAR(S.EXPIRATION_DATE, 'DD/MM/YYYY') ");
			sbQuery.append("      ELSE TO_CHAR(PIC.EXPERITATION_DATE, 'DD/MM/YYYY') ");
			sbQuery.append("   END AS FECHA_EXP_STR,   ");
			
			sbQuery.append("         (SELECT PAR.PARAMETER_NAME FROM PARAMETER_TABLE PAR WHERE PAR.PARAMETER_TABLE_PK = S.SECURITY_CLASS) AS CLASE_VALOR,     ");
			sbQuery.append("         S.SECURITY_CLASS AS CODIGO_CLASE_VALOR,     ");
			sbQuery.append("         (SELECT PAR.TEXT1 FROM PARAMETER_TABLE PAR WHERE PAR.PARAMETER_TABLE_PK = S.CURRENCY) AS MONEDA,     ");
			sbQuery.append("         S.CURRENCY AS CODIGO_MONEDA,     ");
			sbQuery.append("         P.MNEMONIC AS PARTICIPANT,     ");
			sbQuery.append("         HA.ACCOUNT_NUMBER AS NRO_CUENTA,     ");
			sbQuery.append("         (SELECT LISTAGG(H.ID_HOLDER_PK,'<BR>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)      ");
			sbQuery.append("             FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK      ");
			sbQuery.append("             WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS CUI,     ");
			sbQuery.append("         (SELECT LISTAGG(H.FULL_NAME,'<BR>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)      ");
			sbQuery.append("             FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK      ");
			sbQuery.append("             WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS CUI_DESCRIPTION,     ");
			sbQuery.append("         S.ID_SECURITY_CODE_PK AS COD_VALOR,     ");
			
			sbQuery.append("	CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) ");
			sbQuery.append("	THEN '-' ");
			sbQuery.append("	ELSE (PIC.COUPON_NUMBER || '/' ||    ");
			sbQuery.append("	(SELECT MAX(PIC_SUB.COUPON_NUMBER) FROM INTEREST_PAYMENT_SCHEDULE IPS_SUB, PROGRAM_INTEREST_COUPON PIC_SUB  "); 
			sbQuery.append("	WHERE IPS_SUB.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK AND   ");
			sbQuery.append("	PIC_SUB.ID_INT_PAYMENT_SCHEDULE_fK = IPS_SUB.ID_INT_PAYMENT_SCHEDULE_PK)) ");
			sbQuery.append("	 END  AS CUPON,  ");

			sbQuery.append("         PBC.CERTIFICATE_QUANTITY AS TOTAL_CONTABLE,     ");
			sbQuery.append("         PBC.CERTIFICATE_QUANTITY AS DISPONIBLE,     ");
			sbQuery.append("         0 AS PRENDA,     ");
			sbQuery.append("         0 AS EMBARGO,     ");
			sbQuery.append("         0 AS ACREDITACION,     ");
			sbQuery.append("         0 AS REPORTADOR,     ");
			sbQuery.append("         ROUND(NVL(PIC.COUPON_AMOUNT,0),2) AS MONTO_INTERES,     ");
			sbQuery.append("         ROUND(NVL( ");
			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");
			sbQuery.append(" 		,0),2) AS MONTO_AMORTIZACION,     ");
			sbQuery.append("         (ROUND(NVL(PIC.COUPON_AMOUNT,0),2) + ROUND(NVL(  ");
			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");		
			sbQuery.append(" 		,0),2)) AS MONTO_TOTAL,     ");
			sbQuery.append("         (ROUND(NVL(PIC.COUPON_AMOUNT,0),2) + ROUND(NVL(  ");
			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");	
			sbQuery.append("  		,0),2)) * PBC.CERTIFICATE_QUANTITY AS TOTAL,      ");
			sbQuery.append("         NVL(  		CASE  			WHEN S.CURRENCY = 127 THEN 1  			WHEN S.CURRENCY IN (1734,1304,430) THEN 1 *  				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1)  			WHEN S.CURRENCY IN (1853) THEN 1 *  				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)  			ELSE 1  	END,1) MONTO_BOB,      ");
			sbQuery.append("         NVL(  		CASE  			WHEN S.CURRENCY = 127 THEN 1 /  				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)  			WHEN S.CURRENCY IN (1734,1304) THEN 1 *	 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /  				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)  			WHEN S.CURRENCY IN (430,1853) THEN 1  			ELSE 1  	END,0) MONTO_USD     ");
			sbQuery.append("     FROM  ");
			sbQuery.append("         PHYSICAL_BALANCE_DETAIL PBC ");
			sbQuery.append("         INNER JOIN PHYSICAL_CERTIFICATE PC            ON PC.ID_PHYSICAL_CERTIFICATE_PK=PBC.ID_PHYSICAL_CERTIFICATE_FK ");
			sbQuery.append("         INNER JOIN SECURITY S                         ON S.ID_SECURITY_CODE_PK=PC.ID_SECURITY_CODE_FK ");
			sbQuery.append("         INNER JOIN HOLDER_ACCOUNT HA                  ON PBC.ID_HOLDER_ACCOUNT_FK= HA.ID_HOLDER_ACCOUNT_PK  ");
			sbQuery.append("         INNER JOIN ISSUER I                           ON S.ID_ISSUER_FK = I.ID_ISSUER_PK  ");
			sbQuery.append("         INNER JOIN PARTICIPANT P                      ON P.ID_PARTICIPANT_PK = PBC.ID_PARTICIPANT_FK  ");
			sbQuery.append("         LEFT JOIN interest_payment_schedule ips      ON S.id_security_code_pk=ips.id_security_code_fk  ");
			sbQuery.append("         LEFT JOIN program_interest_coupon pic        ON ips.id_int_payment_schedule_pk=pic.id_int_payment_schedule_fk  ");
			sbQuery.append("         LEFT JOIN amortization_payment_schedule APS   ON APS.ID_SECURITY_CODE_FK = S.id_security_code_pk   ");
			sbQuery.append("         LEFT JOIN PROGRAM_AMORTIZATION_COUPON PAC     ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK  ");
			sbQuery.append(" 		                                                 AND TRUNC(PIC.EXPERITATION_DATE) = TRUNC(PAC.EXPRITATION_DATE) ");
			sbQuery.append("     WHERE 1 = 1     ");

			sbQuery.append("  AND (trunc(PIC.EXPERITATION_DATE) between to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') and to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy')  ");
			sbQuery.append("   OR CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) ");
			sbQuery.append("    THEN trunc(S.EXPIRATION_DATE)  END ");
			sbQuery.append("   between to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') and to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy')) ");
			
			sbQuery.append("          ");
			
			if(Validations.validateIsNotNull(gfto.getCui())){
				sbQuery.append(" AND (SELECT COUNT(*) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK=HAB.ID_HOLDER_ACCOUNT_PK AND HAD.ID_HOLDER_FK="+ gfto.getCui() + ")>0 ");
			}
			if(Validations.validateIsNotNull(gfto.getHolderAccount())){
				sbQuery.append(" AND PBC.ID_HOLDER_ACCOUNT_FK="+gfto.getHolderAccount()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getParticipantMnemonic())){
				// sbQuery.append(" AND PBC.ID_PARTICIPANT_FK="+gfto.getParticipant()+" ");
				// issue 1341: en este reporte se busca por agente pagador del valor
				sbQuery.append(" AND S.PAYMENT_AGENT='"+gfto.getParticipantMnemonic()+"' ");
				// por requerimiento de usuario se omite DPFs
				sbQuery.append(" AND S.SECURITY_CLASS != 420 ");
			}
			if(Validations.validateIsNotNull(gfto.getSecurities())){
				sbQuery.append(" AND S.ID_SECURITY_CODE_PK='"+gfto.getSecurities()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getIssuer())){
				sbQuery.append(" AND S.ID_ISSUER_FK='"+gfto.getIssuer()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getCurrency())){
				sbQuery.append(" AND S.CURRENCY="+gfto.getCurrency()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getSecurityClass())){
				sbQuery.append(" AND S.SECURITY_CLASS="+gfto.getSecurityClass()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getIndAutomatic())){
				sbQuery.append(" AND S.SECURITY_CLASS NOT IN (415,414,420) ");//excluimos BBX,BTX,DPF
			}
			
			sbQuery.append(" UNION ");
			
			sbQuery.append(" SELECT      ");
			
			sbQuery.append(" CASE WHEN S.SECURITY_CLASS in (409)  ");
			sbQuery.append("   THEN S.EXPIRATION_DATE ");
			//sbQuery.append("   ELSE PIC.EXPERITATION_DATE  ");
			sbQuery.append(" END AS FECHA_EXP,   ");
		  
			sbQuery.append("  CASE WHEN S.SECURITY_CLASS in (409) ");
			sbQuery.append("   THEN TO_CHAR(S.EXPIRATION_DATE, 'DD/MM/YYYY') ");
			//sbQuery.append("      ELSE TO_CHAR(PIC.EXPERITATION_DATE, 'DD/MM/YYYY') ");
			sbQuery.append("   END AS FECHA_EXP_STR,   ");
			
			sbQuery.append("         (SELECT PAR.PARAMETER_NAME FROM PARAMETER_TABLE PAR WHERE PAR.PARAMETER_TABLE_PK = S.SECURITY_CLASS) AS CLASE_VALOR,     ");
			sbQuery.append("         S.SECURITY_CLASS AS CODIGO_CLASE_VALOR,     ");
			sbQuery.append("         (SELECT PAR.TEXT1 FROM PARAMETER_TABLE PAR WHERE PAR.PARAMETER_TABLE_PK = S.CURRENCY) AS MONEDA,     ");
			sbQuery.append("         S.CURRENCY AS CODIGO_MONEDA,     ");
			sbQuery.append("         P.MNEMONIC AS PARTICIPANT,     ");
			sbQuery.append("         HA.ACCOUNT_NUMBER AS NRO_CUENTA,     ");
			sbQuery.append("         (SELECT LISTAGG(H.ID_HOLDER_PK,'<BR>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)      ");
			sbQuery.append("             FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK      ");
			sbQuery.append("             WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS CUI,     ");
			sbQuery.append("         (SELECT LISTAGG(H.FULL_NAME,'<BR>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)      ");
			sbQuery.append("             FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK      ");
			sbQuery.append("             WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS CUI_DESCRIPTION,     ");
			sbQuery.append("         S.ID_SECURITY_CODE_PK AS COD_VALOR,     ");
			
			sbQuery.append("	CASE WHEN S.SECURITY_CLASS in (409) ");
			sbQuery.append("	THEN '-' ");
			//sbQuery.append("	ELSE (PIC.COUPON_NUMBER || '/' ||    ");
			//sbQuery.append("	(SELECT MAX(PIC_SUB.COUPON_NUMBER) FROM INTEREST_PAYMENT_SCHEDULE IPS_SUB, PROGRAM_INTEREST_COUPON PIC_SUB  "); 
			//sbQuery.append("	WHERE IPS_SUB.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK AND   ");
			//sbQuery.append("	PIC_SUB.ID_INT_PAYMENT_SCHEDULE_fK = IPS_SUB.ID_INT_PAYMENT_SCHEDULE_PK)) ");
			sbQuery.append("	 END  AS CUPON,  ");

			sbQuery.append("         PBC.CERTIFICATE_QUANTITY AS TOTAL_CONTABLE,     ");
			sbQuery.append("         PBC.CERTIFICATE_QUANTITY AS DISPONIBLE,     ");
			sbQuery.append("         0 AS PRENDA,     ");
			sbQuery.append("         0 AS EMBARGO,     ");
			sbQuery.append("         0 AS ACREDITACION,     ");
			sbQuery.append("         0 AS REPORTADOR,     ");
			//sbQuery.append("         ROUND(NVL(PIC.COUPON_AMOUNT,0),2) AS MONTO_INTERES,     ");
			sbQuery.append("         0 AS MONTO_INTERES,     ");
			sbQuery.append("         ROUND(NVL( ");
			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (409) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");
			sbQuery.append(" 		,0),2) AS MONTO_AMORTIZACION,     ");
			//sbQuery.append("         (ROUND(NVL(PIC.COUPON_AMOUNT,0),2) + ROUND(NVL(  ");
			sbQuery.append("         (0 + ROUND(NVL(  ");
			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (409) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");		
			sbQuery.append(" 		,0),2)) AS MONTO_TOTAL,     ");
			//sbQuery.append("         (ROUND(NVL(PIC.COUPON_AMOUNT,0),2) + ROUND(NVL(  ");
			sbQuery.append("         (0 + ROUND(NVL(  ");
			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (409) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");	
			sbQuery.append("  		,0),2)) * PBC.CERTIFICATE_QUANTITY AS TOTAL,      ");
			sbQuery.append("         NVL(  		CASE  			WHEN S.CURRENCY = 127 THEN 1  			WHEN S.CURRENCY IN (1734,1304,430) THEN 1 *  				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1)  			WHEN S.CURRENCY IN (1853) THEN 1 *  				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)  			ELSE 1  	END,1) MONTO_BOB,      ");
			sbQuery.append("         NVL(  		CASE  			WHEN S.CURRENCY = 127 THEN 1 /  				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)  			WHEN S.CURRENCY IN (1734,1304) THEN 1 *	 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /  				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)  			WHEN S.CURRENCY IN (430,1853) THEN 1  			ELSE 1  	END,0) MONTO_USD     ");
			sbQuery.append("     FROM  ");
			sbQuery.append("         PHYSICAL_BALANCE_DETAIL PBC ");
			sbQuery.append("         INNER JOIN PHYSICAL_CERTIFICATE PC            ON PC.ID_PHYSICAL_CERTIFICATE_PK=PBC.ID_PHYSICAL_CERTIFICATE_FK ");
			sbQuery.append("         INNER JOIN SECURITY S                         ON S.ID_SECURITY_CODE_PK=PC.ID_SECURITY_CODE_FK ");
			sbQuery.append("         INNER JOIN HOLDER_ACCOUNT HA                  ON PBC.ID_HOLDER_ACCOUNT_FK= HA.ID_HOLDER_ACCOUNT_PK  ");
			sbQuery.append("         INNER JOIN ISSUER I                           ON S.ID_ISSUER_FK = I.ID_ISSUER_PK  ");
			sbQuery.append("         INNER JOIN PARTICIPANT P                      ON P.ID_PARTICIPANT_PK = PBC.ID_PARTICIPANT_FK  ");
			//sbQuery.append("         LEFT JOIN interest_payment_schedule ips      ON S.id_security_code_pk=ips.id_security_code_fk  ");
			//sbQuery.append("         LEFT JOIN program_interest_coupon pic        ON ips.id_int_payment_schedule_pk=pic.id_int_payment_schedule_fk  ");
			sbQuery.append("         LEFT JOIN amortization_payment_schedule APS   ON APS.ID_SECURITY_CODE_FK = S.id_security_code_pk   ");
			sbQuery.append("         LEFT JOIN PROGRAM_AMORTIZATION_COUPON PAC     ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK  ");
			//sbQuery.append(" 		                                                 AND TRUNC(PIC.EXPERITATION_DATE) = TRUNC(PAC.EXPRITATION_DATE) ");
			sbQuery.append("     WHERE 1 = 1     ");

			//sbQuery.append("  AND (trunc(PIC.EXPERITATION_DATE) between to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') and to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy')  ");
			//sbQuery.append("   OR CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) ");
			//sbQuery.append("    THEN trunc(S.EXPIRATION_DATE)  END ");
			//sbQuery.append("   between to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') and to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy')) ");
			
			sbQuery.append("    AND S.SECURITY_CLASS             IN (409)      ");
			sbQuery.append("    AND TRUNC(S.EXPIRATION_DATE) BETWEEN to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') AND to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy')    ");
			sbQuery.append("    AND s.ID_REF_SECURITY_CODE_FK IS NOT NULL      ");
			
			if(Validations.validateIsNotNull(gfto.getCui())){
				sbQuery.append(" AND (SELECT COUNT(*) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK=HAB.ID_HOLDER_ACCOUNT_PK AND HAD.ID_HOLDER_FK="+ gfto.getCui() + ")>0 ");
			}
			if(Validations.validateIsNotNull(gfto.getHolderAccount())){
				sbQuery.append(" AND PBC.ID_HOLDER_ACCOUNT_FK="+gfto.getHolderAccount()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getParticipantMnemonic())){
				// sbQuery.append(" AND PBC.ID_PARTICIPANT_FK="+gfto.getParticipant()+" ");
				// issue 1341: en este reporte se busca por agente pagador del valor
				sbQuery.append(" AND S.PAYMENT_AGENT='"+gfto.getParticipantMnemonic()+"' ");
				// por requerimiento de usuario se omite DPFs
				sbQuery.append(" AND S.SECURITY_CLASS != 420 ");
			}
			if(Validations.validateIsNotNull(gfto.getSecurities())){
				sbQuery.append(" AND S.ID_SECURITY_CODE_PK='"+gfto.getSecurities()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getIssuer())){
				sbQuery.append(" AND S.ID_ISSUER_FK='"+gfto.getIssuer()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getCurrency())){
				sbQuery.append(" AND S.CURRENCY="+gfto.getCurrency()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getSecurityClass())){
				sbQuery.append(" AND S.SECURITY_CLASS="+gfto.getSecurityClass()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getIndAutomatic())){
				sbQuery.append(" AND S.SECURITY_CLASS NOT IN (415,414,420) ");//excluimos BBX,BTX,DPF
			}
		}
		
		sbQuery.append("   ORDER BY FECHA_EXP,CLASE_VALOR,MONEDA,COD_VALOR,NRO_CUENTA ");
		sbQuery.append("  ");

    	return sbQuery.toString();
    }
    
    public String getDetailsOfTitlesPendingOfExpirationReport(GenericIssuanceTO gfto){
    	StringBuilder sbQuery= new StringBuilder();
		Date currentDate = CommonsUtilities.currentDate();
		String currDateStr = CommonsUtilities.convertDatetoStringForTemplate(currentDate, CommonsUtilities.DATE_PATTERN);
    	
			sbQuery.append(" SELECT  ");
			
			sbQuery.append(" CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416)  ");
			sbQuery.append("   THEN S.EXPIRATION_DATE ");
			sbQuery.append("   ELSE PIC.EXPERITATION_DATE  ");
			sbQuery.append(" END AS FECHA_EXP,   ");
		  
			sbQuery.append("  CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) ");
			sbQuery.append("   THEN TO_CHAR(S.EXPIRATION_DATE, 'DD/MM/YYYY') ");
			sbQuery.append("      ELSE TO_CHAR(PIC.EXPERITATION_DATE, 'DD/MM/YYYY') ");
			sbQuery.append("   END AS FECHA_EXP_STR,   ");
			
			sbQuery.append("   (SELECT PAR.PARAMETER_NAME FROM PARAMETER_TABLE PAR WHERE PAR.PARAMETER_TABLE_PK = S.SECURITY_CLASS) AS CLASE_VALOR, ");
			sbQuery.append("   S.SECURITY_CLASS AS CODIGO_CLASE_VALOR, ");
			sbQuery.append("   (SELECT PAR.TEXT1 FROM PARAMETER_TABLE PAR WHERE PAR.PARAMETER_TABLE_PK = S.CURRENCY) AS MONEDA, ");
			sbQuery.append("   S.CURRENCY AS CODIGO_MONEDA, ");
			sbQuery.append("   P.MNEMONIC AS PARTICIPANT, ");
			sbQuery.append("   HA.ACCOUNT_NUMBER AS NRO_CUENTA, ");
			sbQuery.append("   (SELECT LISTAGG(H.ID_HOLDER_PK,'<BR>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
			sbQuery.append("    FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
			sbQuery.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS CUI, ");
			sbQuery.append("   (SELECT LISTAGG(H.FULL_NAME,'<BR>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
			sbQuery.append("    FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
			sbQuery.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS CUI_DESCRIPTION, ");
			sbQuery.append("   S.ID_SECURITY_CODE_PK AS COD_VALOR, ");
			
			sbQuery.append("	CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) ");
			sbQuery.append("	THEN '-' ");
			sbQuery.append("	ELSE (PIC.COUPON_NUMBER || '/' ||    ");
			sbQuery.append("	(SELECT MAX(PIC_SUB.COUPON_NUMBER) FROM INTEREST_PAYMENT_SCHEDULE IPS_SUB, PROGRAM_INTEREST_COUPON PIC_SUB  "); 
			sbQuery.append("	WHERE IPS_SUB.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK AND   ");
			sbQuery.append("	PIC_SUB.ID_INT_PAYMENT_SCHEDULE_fK = IPS_SUB.ID_INT_PAYMENT_SCHEDULE_PK)) ");
			sbQuery.append("	 END  AS CUPON,  ");
			
			sbQuery.append("   HAB.TOTAL_BALANCE AS TOTAL_CONTABLE, ");
			sbQuery.append("   HAB.AVAILABLE_BALANCE AS DISPONIBLE, ");
			sbQuery.append("   HAB.PAWN_BALANCE AS PRENDA, ");
			sbQuery.append("   HAB.BAN_BALANCE AS EMBARGO, ");
			sbQuery.append("   HAB.ACCREDITATION_BALANCE AS ACREDITACION, ");
			sbQuery.append("   HAB.REPORTING_BALANCE AS REPORTADOR, ");
			sbQuery.append("   ROUND(NVL(PIC.COUPON_AMOUNT,0),2) AS MONTO_INTERES, ");
			sbQuery.append("   ROUND(NVL( 			");
			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");
			sbQuery.append("		 ,0),2) AS MONTO_AMORTIZACION, ");
			sbQuery.append("   (ROUND(NVL(PIC.COUPON_AMOUNT,0),2) + ROUND(NVL( ");
			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");
			sbQuery.append("  ,0),2)) AS MONTO_TOTAL, ");
			sbQuery.append("   (ROUND(NVL(PIC.COUPON_AMOUNT,0),2) + ROUND(NVL( ");
			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");	
			sbQuery.append("		,0),2)) * HAB.TOTAL_BALANCE AS TOTAL, ");
			sbQuery.append("    NVL( ");
			sbQuery.append(" 		CASE ");     
			sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1 ");     
			sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304,430) THEN 1 * "); 
			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
			sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) ");
			sbQuery.append(" 			WHEN S.CURRENCY IN (1853) THEN 1 * "); 
			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
			sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			sbQuery.append(" 			ELSE 1 ");   
			sbQuery.append(" 	END,1) MONTO_BOB, ");
			sbQuery.append("    NVL( ");
			sbQuery.append(" 		CASE ");
			sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1 / ");
			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
			sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304) THEN 1 *	");
			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
			sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) / ");
			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
			sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			sbQuery.append(" 			WHEN S.CURRENCY IN (430,1853) THEN 1 ");
			sbQuery.append(" 			ELSE 1 ");
			sbQuery.append(" 	END,0) MONTO_USD ");
			
			sbQuery.append("  ");
			sbQuery.append(" FROM HOLDER_ACCOUNT_BALANCE hab ");
			sbQuery.append("   INNER JOIN SECURITY S                         ON s.ID_SECURITY_CODE_PK=hab.ID_SECURITY_CODE_PK ");
			sbQuery.append("   INNER JOIN HOLDER_ACCOUNT ha                  ON hab.ID_HOLDER_ACCOUNT_PK= ha.ID_HOLDER_ACCOUNT_PK ");
			sbQuery.append("   INNER JOIN ISSUER I                           ON S.ID_ISSUER_FK = I.ID_ISSUER_PK ");
			sbQuery.append("   INNER JOIN PARTICIPANT P                      ON P.ID_PARTICIPANT_PK = HAB.ID_PARTICIPANT_PK ");
			sbQuery.append("   LEFT JOIN interest_payment_schedule ips      ON s.id_security_code_pk=ips.id_security_code_fk ");
			sbQuery.append("   LEFT JOIN program_interest_coupon pic        ON ips.id_int_payment_schedule_pk=pic.id_int_payment_schedule_fk ");
			sbQuery.append("   LEFT JOIN amortization_payment_schedule APS   ON APS.ID_SECURITY_CODE_FK = S.id_security_code_pk  ");
			sbQuery.append("   LEFT JOIN PROGRAM_AMORTIZATION_COUPON PAC     ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK ");
			sbQuery.append("                                                 AND TRUNC(PIC.EXPERITATION_DATE) = TRUNC(PAC.EXPRITATION_DATE) ");
			sbQuery.append(" WHERE 1 = 1 ");
	//		sbQuery.append("   AND trunc(PIC.EXPERITATION_DATE) >= to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') ");
	//		sbQuery.append("   AND trunc(PIC.EXPERITATION_DATE) <= to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy') ");
			
			sbQuery.append("  AND (trunc(PIC.EXPERITATION_DATE) between to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') and to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy')  ");
			sbQuery.append("   OR CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) ");
			sbQuery.append("    THEN trunc(S.EXPIRATION_DATE)  END ");
			sbQuery.append("   between to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') and to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy')) ");
			
			sbQuery.append("   AND HAB.TOTAL_BALANCE > 0 ");
			
			if(gfto.getSecuritiesSituation().equals(GeneralConstants.ONE_VALUE_STRING)) {
				sbQuery.append("	AND S.ISSUANCE_FORM = 132  ");
			} else {
				sbQuery.append("	AND S.ISSUANCE_FORM = 400  ");		
			}
			
			if(Validations.validateIsNotNull(gfto.getCui())){
				sbQuery.append(" AND (SELECT COUNT(*) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK=HAB.ID_HOLDER_ACCOUNT_PK AND HAD.ID_HOLDER_FK="+ gfto.getCui() + ")>0 ");
			}
			if(Validations.validateIsNotNull(gfto.getHolderAccount())){
				sbQuery.append(" AND HAB.ID_HOLDER_ACCOUNT_PK="+gfto.getHolderAccount()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getParticipant())){
				sbQuery.append(" AND HAB.ID_PARTICIPANT_PK="+gfto.getParticipant()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getSecurities())){
				sbQuery.append(" AND S.ID_SECURITY_CODE_PK='"+gfto.getSecurities()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getIssuer())){
				sbQuery.append(" AND S.ID_ISSUER_FK='"+gfto.getIssuer()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getCurrency())){
				sbQuery.append(" AND S.CURRENCY="+gfto.getCurrency()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getSecurityClass())){
				sbQuery.append(" AND S.SECURITY_CLASS="+gfto.getSecurityClass()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getIndAutomatic())){
				sbQuery.append(" AND S.SECURITY_CLASS NOT IN (415,414,420) ");//excluimos BBX,BTX,DPF
			}
			
			sbQuery.append(" UNION ");
			
			// solo para el subproducto 
			sbQuery.append(" SELECT  ");
			
			sbQuery.append(" CASE WHEN S.SECURITY_CLASS in (409)  ");
			sbQuery.append("   THEN S.EXPIRATION_DATE ");
			//sbQuery.append("   ELSE PIC.EXPERITATION_DATE  ");
			sbQuery.append(" END AS FECHA_EXP,   ");
		  
			sbQuery.append("  CASE WHEN S.SECURITY_CLASS in (409) ");
			sbQuery.append("   THEN TO_CHAR(S.EXPIRATION_DATE, 'DD/MM/YYYY') ");
			//sbQuery.append("      ELSE TO_CHAR(PIC.EXPERITATION_DATE, 'DD/MM/YYYY') ");
			sbQuery.append("   END AS FECHA_EXP_STR,   ");
			
			sbQuery.append("   (SELECT PAR.PARAMETER_NAME FROM PARAMETER_TABLE PAR WHERE PAR.PARAMETER_TABLE_PK = S.SECURITY_CLASS) AS CLASE_VALOR, ");
			sbQuery.append("   S.SECURITY_CLASS AS CODIGO_CLASE_VALOR, ");
			sbQuery.append("   (SELECT PAR.TEXT1 FROM PARAMETER_TABLE PAR WHERE PAR.PARAMETER_TABLE_PK = S.CURRENCY) AS MONEDA, ");
			sbQuery.append("   S.CURRENCY AS CODIGO_MONEDA, ");
			sbQuery.append("   P.MNEMONIC AS PARTICIPANT, ");
			sbQuery.append("   HA.ACCOUNT_NUMBER AS NRO_CUENTA, ");
			sbQuery.append("   (SELECT LISTAGG(H.ID_HOLDER_PK,'<BR>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
			sbQuery.append("    FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
			sbQuery.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS CUI, ");
			sbQuery.append("   (SELECT LISTAGG(H.FULL_NAME,'<BR>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
			sbQuery.append("    FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
			sbQuery.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS CUI_DESCRIPTION, ");
			sbQuery.append("   S.ID_SECURITY_CODE_PK AS COD_VALOR, ");
			
			sbQuery.append("	CASE WHEN S.SECURITY_CLASS in (409) ");
			sbQuery.append("	THEN '-' ");
			//sbQuery.append("	ELSE (PIC.COUPON_NUMBER || '/' ||    ");
			//sbQuery.append("	(SELECT MAX(PIC_SUB.COUPON_NUMBER) FROM INTEREST_PAYMENT_SCHEDULE IPS_SUB, PROGRAM_INTEREST_COUPON PIC_SUB  "); 
			//sbQuery.append("	WHERE IPS_SUB.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK AND   ");
			//sbQuery.append("	PIC_SUB.ID_INT_PAYMENT_SCHEDULE_fK = IPS_SUB.ID_INT_PAYMENT_SCHEDULE_PK)) ");
			sbQuery.append("	 END  AS CUPON,  ");
			
			sbQuery.append("   HAB.TOTAL_BALANCE AS TOTAL_CONTABLE, ");
			sbQuery.append("   HAB.AVAILABLE_BALANCE AS DISPONIBLE, ");
			sbQuery.append("   HAB.PAWN_BALANCE AS PRENDA, ");
			sbQuery.append("   HAB.BAN_BALANCE AS EMBARGO, ");
			sbQuery.append("   HAB.ACCREDITATION_BALANCE AS ACREDITACION, ");
			sbQuery.append("   HAB.REPORTING_BALANCE AS REPORTADOR, ");
			//sbQuery.append("   ROUND(NVL(PIC.COUPON_AMOUNT,0),2) AS MONTO_INTERES, ");
			sbQuery.append("   0 AS MONTO_INTERES, ");
			sbQuery.append("   ROUND(NVL( 			");
			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (409) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");
			sbQuery.append("		 ,0),2) AS MONTO_AMORTIZACION, ");
			//sbQuery.append("   (ROUND(NVL(PIC.COUPON_AMOUNT,0),2) + ROUND(NVL( ");
			sbQuery.append("   (0 + ROUND(NVL( ");
			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (409) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");
			sbQuery.append("  ,0),2)) AS MONTO_TOTAL, ");
			//sbQuery.append("   (ROUND(NVL(PIC.COUPON_AMOUNT,0),2) + ROUND(NVL( ");
			sbQuery.append("   (0 + ROUND(NVL( ");
			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (409) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");	
			sbQuery.append("		,0),2)) * HAB.TOTAL_BALANCE AS TOTAL, ");
			sbQuery.append("    NVL( ");
			sbQuery.append(" 		CASE ");     
			sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1 ");     
			sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304,430) THEN 1 * "); 
			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
			sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) ");
			sbQuery.append(" 			WHEN S.CURRENCY IN (1853) THEN 1 * "); 
			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
			sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			sbQuery.append(" 			ELSE 1 ");   
			sbQuery.append(" 	END,1) MONTO_BOB, ");
			sbQuery.append("    NVL( ");
			sbQuery.append(" 		CASE ");
			sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1 / ");
			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
			sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304) THEN 1 *	");
			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
			sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) / ");
			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
			sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			sbQuery.append(" 			WHEN S.CURRENCY IN (430,1853) THEN 1 ");
			sbQuery.append(" 			ELSE 1 ");
			sbQuery.append(" 	END,0) MONTO_USD ");
			
			sbQuery.append("  ");
			sbQuery.append(" FROM HOLDER_ACCOUNT_BALANCE hab ");
			sbQuery.append("   INNER JOIN SECURITY S                         ON s.ID_SECURITY_CODE_PK=hab.ID_SECURITY_CODE_PK ");
			sbQuery.append("   INNER JOIN HOLDER_ACCOUNT ha                  ON hab.ID_HOLDER_ACCOUNT_PK= ha.ID_HOLDER_ACCOUNT_PK ");
			sbQuery.append("   INNER JOIN ISSUER I                           ON S.ID_ISSUER_FK = I.ID_ISSUER_PK ");
			sbQuery.append("   INNER JOIN PARTICIPANT P                      ON P.ID_PARTICIPANT_PK = HAB.ID_PARTICIPANT_PK ");
			//sbQuery.append("   LEFT JOIN interest_payment_schedule ips      ON s.id_security_code_pk=ips.id_security_code_fk ");
			//sbQuery.append("   LEFT JOIN program_interest_coupon pic        ON ips.id_int_payment_schedule_pk=pic.id_int_payment_schedule_fk ");
			sbQuery.append("   LEFT JOIN amortization_payment_schedule APS   ON APS.ID_SECURITY_CODE_FK = S.id_security_code_pk  ");
			sbQuery.append("   LEFT JOIN PROGRAM_AMORTIZATION_COUPON PAC     ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK ");
			//sbQuery.append("                                                 AND TRUNC(PIC.EXPERITATION_DATE) = TRUNC(PAC.EXPRITATION_DATE) ");
			sbQuery.append(" WHERE 1 = 1 ");
			
			//sbQuery.append("  AND (trunc(PIC.EXPERITATION_DATE) between to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') and to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy')  ");
			//sbQuery.append("   OR CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) ");
			//sbQuery.append("    THEN trunc(S.EXPIRATION_DATE)  END ");
			//sbQuery.append("   between to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') and to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy')) ");
			
			sbQuery.append("   AND S.SECURITY_CLASS IN (409) ");
			sbQuery.append("   AND TRUNC(S.EXPIRATION_DATE) BETWEEN to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') AND to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy') ");
			sbQuery.append("   AND s.ID_REF_SECURITY_CODE_FK IS NOT NULL ");
			
			sbQuery.append("   AND HAB.TOTAL_BALANCE > 0 ");
			
			if(gfto.getSecuritiesSituation().equals(GeneralConstants.ONE_VALUE_STRING)) {
				sbQuery.append("	AND S.ISSUANCE_FORM = 132  ");
			} else {
				sbQuery.append("	AND S.ISSUANCE_FORM = 400  ");		
			}
			
			if(Validations.validateIsNotNull(gfto.getCui())){
				sbQuery.append(" AND (SELECT COUNT(*) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK=HAB.ID_HOLDER_ACCOUNT_PK AND HAD.ID_HOLDER_FK="+ gfto.getCui() + ")>0 ");
			}
			if(Validations.validateIsNotNull(gfto.getHolderAccount())){
				sbQuery.append(" AND HAB.ID_HOLDER_ACCOUNT_PK="+gfto.getHolderAccount()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getParticipant())){
				sbQuery.append(" AND HAB.ID_PARTICIPANT_PK="+gfto.getParticipant()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getSecurities())){
				sbQuery.append(" AND S.ID_SECURITY_CODE_PK='"+gfto.getSecurities()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getIssuer())){
				sbQuery.append(" AND S.ID_ISSUER_FK='"+gfto.getIssuer()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getCurrency())){
				sbQuery.append(" AND S.CURRENCY="+gfto.getCurrency()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getSecurityClass())){
				sbQuery.append(" AND S.SECURITY_CLASS="+gfto.getSecurityClass()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getIndAutomatic())){
				sbQuery.append(" AND S.SECURITY_CLASS NOT IN (415,414,420) ");//excluimos BBX,BTX,DPF
			}
			
		
		
		/*else{
			
			sbQuery.append(" SELECT      ");
			
			sbQuery.append(" CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416)  ");
			sbQuery.append("   THEN S.EXPIRATION_DATE ");
			sbQuery.append("   ELSE PIC.EXPERITATION_DATE  ");
			sbQuery.append(" END AS FECHA_EXP,   ");
		  
			sbQuery.append("  CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) ");
			sbQuery.append("   THEN TO_CHAR(S.EXPIRATION_DATE, 'DD/MM/YYYY') ");
			sbQuery.append("      ELSE TO_CHAR(PIC.EXPERITATION_DATE, 'DD/MM/YYYY') ");
			sbQuery.append("   END AS FECHA_EXP_STR,   ");
			
			sbQuery.append("         (SELECT PAR.PARAMETER_NAME FROM PARAMETER_TABLE PAR WHERE PAR.PARAMETER_TABLE_PK = S.SECURITY_CLASS) AS CLASE_VALOR,     ");
			sbQuery.append("         S.SECURITY_CLASS AS CODIGO_CLASE_VALOR,     ");
			sbQuery.append("         (SELECT PAR.TEXT1 FROM PARAMETER_TABLE PAR WHERE PAR.PARAMETER_TABLE_PK = S.CURRENCY) AS MONEDA,     ");
			sbQuery.append("         S.CURRENCY AS CODIGO_MONEDA,     ");
			sbQuery.append("         P.MNEMONIC AS PARTICIPANT,     ");
			sbQuery.append("         HA.ACCOUNT_NUMBER AS NRO_CUENTA,     ");
			sbQuery.append("         (SELECT LISTAGG(H.ID_HOLDER_PK,'<BR>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)      ");
			sbQuery.append("             FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK      ");
			sbQuery.append("             WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS CUI,     ");
			sbQuery.append("         (SELECT LISTAGG(H.FULL_NAME,'<BR>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)      ");
			sbQuery.append("             FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK      ");
			sbQuery.append("             WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS CUI_DESCRIPTION,     ");
			sbQuery.append("         S.ID_SECURITY_CODE_PK AS COD_VALOR,     ");
			
			sbQuery.append("	CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) ");
			sbQuery.append("	THEN '-' ");
			sbQuery.append("	ELSE (PIC.COUPON_NUMBER || '/' ||    ");
			sbQuery.append("	(SELECT MAX(PIC_SUB.COUPON_NUMBER) FROM INTEREST_PAYMENT_SCHEDULE IPS_SUB, PROGRAM_INTEREST_COUPON PIC_SUB  "); 
			sbQuery.append("	WHERE IPS_SUB.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK AND   ");
			sbQuery.append("	PIC_SUB.ID_INT_PAYMENT_SCHEDULE_fK = IPS_SUB.ID_INT_PAYMENT_SCHEDULE_PK)) ");
			sbQuery.append("	 END  AS CUPON,  ");

			sbQuery.append("         PBC.CERTIFICATE_QUANTITY AS TOTAL_CONTABLE,     ");
			sbQuery.append("         PBC.CERTIFICATE_QUANTITY AS DISPONIBLE,     ");
			sbQuery.append("         0 AS PRENDA,     ");
			sbQuery.append("         0 AS EMBARGO,     ");
			sbQuery.append("         0 AS ACREDITACION,     ");
			sbQuery.append("         0 AS REPORTADOR,     ");
			sbQuery.append("         ROUND(NVL(PIC.COUPON_AMOUNT,0),2) AS MONTO_INTERES,     ");
			sbQuery.append("         ROUND(NVL( ");
			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");
			sbQuery.append(" 		,0),2) AS MONTO_AMORTIZACION,     ");
			sbQuery.append("         (ROUND(NVL(PIC.COUPON_AMOUNT,0),2) + ROUND(NVL(  ");
			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");		
			sbQuery.append(" 		,0),2)) AS MONTO_TOTAL,     ");
			sbQuery.append("         (ROUND(NVL(PIC.COUPON_AMOUNT,0),2) + ROUND(NVL(  ");
			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");	
			sbQuery.append("  		,0),2)) * PBC.CERTIFICATE_QUANTITY AS TOTAL,      ");
			sbQuery.append("         NVL(  		CASE  			WHEN S.CURRENCY = 127 THEN 1  			WHEN S.CURRENCY IN (1734,1304,430) THEN 1 *  				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1)  			WHEN S.CURRENCY IN (1853) THEN 1 *  				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)  			ELSE 1  	END,1) MONTO_BOB,      ");
			sbQuery.append("         NVL(  		CASE  			WHEN S.CURRENCY = 127 THEN 1 /  				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)  			WHEN S.CURRENCY IN (1734,1304) THEN 1 *	 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /  				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)  			WHEN S.CURRENCY IN (430,1853) THEN 1  			ELSE 1  	END,0) MONTO_USD     ");
			sbQuery.append("     FROM  ");
			sbQuery.append("         PHYSICAL_BALANCE_DETAIL PBC ");
			sbQuery.append("         INNER JOIN PHYSICAL_CERTIFICATE PC            ON PC.ID_PHYSICAL_CERTIFICATE_PK=PBC.ID_PHYSICAL_CERTIFICATE_FK ");
			sbQuery.append("         INNER JOIN SECURITY S                         ON S.ID_SECURITY_CODE_PK=PC.ID_SECURITY_CODE_FK ");
			sbQuery.append("         INNER JOIN HOLDER_ACCOUNT HA                  ON PBC.ID_HOLDER_ACCOUNT_FK= HA.ID_HOLDER_ACCOUNT_PK  ");
			sbQuery.append("         INNER JOIN ISSUER I                           ON S.ID_ISSUER_FK = I.ID_ISSUER_PK  ");
			sbQuery.append("         INNER JOIN PARTICIPANT P                      ON P.ID_PARTICIPANT_PK = PBC.ID_PARTICIPANT_FK  ");
			sbQuery.append("         LEFT JOIN interest_payment_schedule ips      ON S.id_security_code_pk=ips.id_security_code_fk  ");
			sbQuery.append("         LEFT JOIN program_interest_coupon pic        ON ips.id_int_payment_schedule_pk=pic.id_int_payment_schedule_fk  ");
			sbQuery.append("         LEFT JOIN amortization_payment_schedule APS   ON APS.ID_SECURITY_CODE_FK = S.id_security_code_pk   ");
			sbQuery.append("         LEFT JOIN PROGRAM_AMORTIZATION_COUPON PAC     ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK  ");
			sbQuery.append(" 		                                                 AND TRUNC(PIC.EXPERITATION_DATE) = TRUNC(PAC.EXPRITATION_DATE) ");
			sbQuery.append("     WHERE 1 = 1     ");

			sbQuery.append("  AND (trunc(PIC.EXPERITATION_DATE) between to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') and to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy')  ");
			sbQuery.append("   OR CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) ");
			sbQuery.append("    THEN trunc(S.EXPIRATION_DATE)  END ");
			sbQuery.append("   between to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') and to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy')) ");
			
			sbQuery.append("          ");
			
			if(Validations.validateIsNotNull(gfto.getCui())){
				sbQuery.append(" AND (SELECT COUNT(*) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK=HAB.ID_HOLDER_ACCOUNT_PK AND HAD.ID_HOLDER_FK="+ gfto.getCui() + ")>0 ");
			}
			if(Validations.validateIsNotNull(gfto.getHolderAccount())){
				sbQuery.append(" AND PBC.ID_HOLDER_ACCOUNT_FK="+gfto.getHolderAccount()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getParticipant())){
				sbQuery.append(" AND PBC.ID_PARTICIPANT_FK="+gfto.getParticipant()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getSecurities())){
				sbQuery.append(" AND S.ID_SECURITY_CODE_PK='"+gfto.getSecurities()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getIssuer())){
				sbQuery.append(" AND S.ID_ISSUER_FK='"+gfto.getIssuer()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getCurrency())){
				sbQuery.append(" AND S.CURRENCY="+gfto.getCurrency()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getSecurityClass())){
				sbQuery.append(" AND S.SECURITY_CLASS="+gfto.getSecurityClass()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getIndAutomatic())){
				sbQuery.append(" AND S.SECURITY_CLASS NOT IN (415,414,420) ");//excluimos BBX,BTX,DPF
			}
			
			sbQuery.append(" UNION ");
			
			sbQuery.append(" SELECT      ");
			
			sbQuery.append(" CASE WHEN S.SECURITY_CLASS in (409)  ");
			sbQuery.append("   THEN S.EXPIRATION_DATE ");
			//sbQuery.append("   ELSE PIC.EXPERITATION_DATE  ");
			sbQuery.append(" END AS FECHA_EXP,   ");
		  
			sbQuery.append("  CASE WHEN S.SECURITY_CLASS in (409) ");
			sbQuery.append("   THEN TO_CHAR(S.EXPIRATION_DATE, 'DD/MM/YYYY') ");
			//sbQuery.append("      ELSE TO_CHAR(PIC.EXPERITATION_DATE, 'DD/MM/YYYY') ");
			sbQuery.append("   END AS FECHA_EXP_STR,   ");
			
			sbQuery.append("         (SELECT PAR.PARAMETER_NAME FROM PARAMETER_TABLE PAR WHERE PAR.PARAMETER_TABLE_PK = S.SECURITY_CLASS) AS CLASE_VALOR,     ");
			sbQuery.append("         S.SECURITY_CLASS AS CODIGO_CLASE_VALOR,     ");
			sbQuery.append("         (SELECT PAR.TEXT1 FROM PARAMETER_TABLE PAR WHERE PAR.PARAMETER_TABLE_PK = S.CURRENCY) AS MONEDA,     ");
			sbQuery.append("         S.CURRENCY AS CODIGO_MONEDA,     ");
			sbQuery.append("         P.MNEMONIC AS PARTICIPANT,     ");
			sbQuery.append("         HA.ACCOUNT_NUMBER AS NRO_CUENTA,     ");
			sbQuery.append("         (SELECT LISTAGG(H.ID_HOLDER_PK,'<BR>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)      ");
			sbQuery.append("             FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK      ");
			sbQuery.append("             WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS CUI,     ");
			sbQuery.append("         (SELECT LISTAGG(H.FULL_NAME,'<BR>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)      ");
			sbQuery.append("             FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK      ");
			sbQuery.append("             WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS CUI_DESCRIPTION,     ");
			sbQuery.append("         S.ID_SECURITY_CODE_PK AS COD_VALOR,     ");
			
			sbQuery.append("	CASE WHEN S.SECURITY_CLASS in (409) ");
			sbQuery.append("	THEN '-' ");
			//sbQuery.append("	ELSE (PIC.COUPON_NUMBER || '/' ||    ");
			//sbQuery.append("	(SELECT MAX(PIC_SUB.COUPON_NUMBER) FROM INTEREST_PAYMENT_SCHEDULE IPS_SUB, PROGRAM_INTEREST_COUPON PIC_SUB  "); 
			//sbQuery.append("	WHERE IPS_SUB.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK AND   ");
			//sbQuery.append("	PIC_SUB.ID_INT_PAYMENT_SCHEDULE_fK = IPS_SUB.ID_INT_PAYMENT_SCHEDULE_PK)) ");
			sbQuery.append("	 END  AS CUPON,  ");

			sbQuery.append("         PBC.CERTIFICATE_QUANTITY AS TOTAL_CONTABLE,     ");
			sbQuery.append("         PBC.CERTIFICATE_QUANTITY AS DISPONIBLE,     ");
			sbQuery.append("         0 AS PRENDA,     ");
			sbQuery.append("         0 AS EMBARGO,     ");
			sbQuery.append("         0 AS ACREDITACION,     ");
			sbQuery.append("         0 AS REPORTADOR,     ");
			//sbQuery.append("         ROUND(NVL(PIC.COUPON_AMOUNT,0),2) AS MONTO_INTERES,     ");
			sbQuery.append("         0 AS MONTO_INTERES,     ");
			sbQuery.append("         ROUND(NVL( ");
			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (409) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");
			sbQuery.append(" 		,0),2) AS MONTO_AMORTIZACION,     ");
			//sbQuery.append("         (ROUND(NVL(PIC.COUPON_AMOUNT,0),2) + ROUND(NVL(  ");
			sbQuery.append("         (0 + ROUND(NVL(  ");
			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (409) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");		
			sbQuery.append(" 		,0),2)) AS MONTO_TOTAL,     ");
			//sbQuery.append("         (ROUND(NVL(PIC.COUPON_AMOUNT,0),2) + ROUND(NVL(  ");
			sbQuery.append("         (0 + ROUND(NVL(  ");
			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (409) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");	
			sbQuery.append("  		,0),2)) * PBC.CERTIFICATE_QUANTITY AS TOTAL,      ");
			sbQuery.append("         NVL(  		CASE  			WHEN S.CURRENCY = 127 THEN 1  			WHEN S.CURRENCY IN (1734,1304,430) THEN 1 *  				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1)  			WHEN S.CURRENCY IN (1853) THEN 1 *  				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)  			ELSE 1  	END,1) MONTO_BOB,      ");
			sbQuery.append("         NVL(  		CASE  			WHEN S.CURRENCY = 127 THEN 1 /  				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)  			WHEN S.CURRENCY IN (1734,1304) THEN 1 *	 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /  				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)  			WHEN S.CURRENCY IN (430,1853) THEN 1  			ELSE 1  	END,0) MONTO_USD     ");
			sbQuery.append("     FROM  ");
			sbQuery.append("         PHYSICAL_BALANCE_DETAIL PBC ");
			sbQuery.append("         INNER JOIN PHYSICAL_CERTIFICATE PC            ON PC.ID_PHYSICAL_CERTIFICATE_PK=PBC.ID_PHYSICAL_CERTIFICATE_FK ");
			sbQuery.append("         INNER JOIN SECURITY S                         ON S.ID_SECURITY_CODE_PK=PC.ID_SECURITY_CODE_FK ");
			sbQuery.append("         INNER JOIN HOLDER_ACCOUNT HA                  ON PBC.ID_HOLDER_ACCOUNT_FK= HA.ID_HOLDER_ACCOUNT_PK  ");
			sbQuery.append("         INNER JOIN ISSUER I                           ON S.ID_ISSUER_FK = I.ID_ISSUER_PK  ");
			sbQuery.append("         INNER JOIN PARTICIPANT P                      ON P.ID_PARTICIPANT_PK = PBC.ID_PARTICIPANT_FK  ");
			//sbQuery.append("         LEFT JOIN interest_payment_schedule ips      ON S.id_security_code_pk=ips.id_security_code_fk  ");
			//sbQuery.append("         LEFT JOIN program_interest_coupon pic        ON ips.id_int_payment_schedule_pk=pic.id_int_payment_schedule_fk  ");
			sbQuery.append("         LEFT JOIN amortization_payment_schedule APS   ON APS.ID_SECURITY_CODE_FK = S.id_security_code_pk   ");
			sbQuery.append("         LEFT JOIN PROGRAM_AMORTIZATION_COUPON PAC     ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK  ");
			//sbQuery.append(" 		                                                 AND TRUNC(PIC.EXPERITATION_DATE) = TRUNC(PAC.EXPRITATION_DATE) ");
			sbQuery.append("     WHERE 1 = 1     ");

			//sbQuery.append("  AND (trunc(PIC.EXPERITATION_DATE) between to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') and to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy')  ");
			//sbQuery.append("   OR CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) ");
			//sbQuery.append("    THEN trunc(S.EXPIRATION_DATE)  END ");
			//sbQuery.append("   between to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') and to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy')) ");
			
			sbQuery.append("    AND S.SECURITY_CLASS             IN (409)      ");
			sbQuery.append("    AND TRUNC(S.EXPIRATION_DATE) BETWEEN to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') AND to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy')    ");
			sbQuery.append("    AND s.ID_REF_SECURITY_CODE_FK IS NOT NULL      ");
			
			if(Validations.validateIsNotNull(gfto.getCui())){
				sbQuery.append(" AND (SELECT COUNT(*) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK=HAB.ID_HOLDER_ACCOUNT_PK AND HAD.ID_HOLDER_FK="+ gfto.getCui() + ")>0 ");
			}
			if(Validations.validateIsNotNull(gfto.getHolderAccount())){
				sbQuery.append(" AND PBC.ID_HOLDER_ACCOUNT_FK="+gfto.getHolderAccount()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getParticipant())){
				sbQuery.append(" AND PBC.ID_PARTICIPANT_FK="+gfto.getParticipant()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getSecurities())){
				sbQuery.append(" AND S.ID_SECURITY_CODE_PK='"+gfto.getSecurities()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getIssuer())){
				sbQuery.append(" AND S.ID_ISSUER_FK='"+gfto.getIssuer()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getCurrency())){
				sbQuery.append(" AND S.CURRENCY="+gfto.getCurrency()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getSecurityClass())){
				sbQuery.append(" AND S.SECURITY_CLASS="+gfto.getSecurityClass()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getIndAutomatic())){
				sbQuery.append(" AND S.SECURITY_CLASS NOT IN (415,414,420) ");//excluimos BBX,BTX,DPF
			}
		}*/
		
		sbQuery.append("   ORDER BY FECHA_EXP,CLASE_VALOR,MONEDA,COD_VALOR,NRO_CUENTA ");
		sbQuery.append("  ");

    	return sbQuery.toString();
    }
    
	public Issuer findIssuerByCode(String idIssuerPk) throws ServiceException{
		Issuer issuer = new Issuer();
		try {
			StringBuilder sb = new StringBuilder();
			sb.append("select i from Issuer i where i.idIssuerPk=:issuer");
			TypedQuery<Issuer> query = em.createQuery(sb.toString(), Issuer.class);
			query.setParameter("issuer", idIssuerPk);
			issuer = (Issuer)query.getSingleResult();
		}catch(NoResultException ex) {
			issuer = null;
		}
		return issuer;
	}
    
    public String getQueryFormatForJasper(GenericsFiltersTO gfto, String sbQuery, Long idStkCalcProcess){
    	String strQueryFormatted = null;
    	strQueryFormatted = sbQuery.replace(":final_date", "'"+gfto.getFinalDt()+"'");
    	
    	if(gfto.getInitialDt()!=null){
        	strQueryFormatted = strQueryFormatted.replace(":initial_date", "'"+gfto.getInitialDt()+"'");
    	}
    	if(gfto.getIdIssuer()!=null){
        	strQueryFormatted = strQueryFormatted.replace(":issuer_code", "'"+gfto.getIdIssuer()+"'");
    	}
    	if(gfto.getIdSecurityCodePk()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":security_class_key", "'"+gfto.getIdSecurityCodePk().toString()+"'");
    	}
		if(gfto.getCui()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":cui_code", gfto.getCui().toString());
    	}
		if(gfto.getIdHolderAccountPk()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":holder_account", gfto.getIdHolderAccountPk().toString());
		}
    	
		return strQueryFormatted;
    }
    
    /**
     * issue 1341: query vencimiento de valores par aagente pagador, esta query esta bajasa en la queyr del metodo getDetailsOfTitlesPendingOfExpirationPoiReport
     * @param gfto
     * @return
     */
    public String getDetailsOfTitlesPendingOfExpirationPoiReportToPaymentAgent(GenericIssuanceTO gfto){
    	StringBuilder sbQuery= new StringBuilder();
		Date currentDate = CommonsUtilities.currentDate();
		String currDateStr = CommonsUtilities.convertDatetoStringForTemplate(currentDate, CommonsUtilities.DATE_PATTERN);
    	System.out.println("=======> IF: " + gfto.getSecuritiesSituation());
		
		if(gfto.getSecuritiesSituation().equals(GeneralConstants.ONE_VALUE_STRING)){
			sbQuery.append(" SELECT  ");
			
			sbQuery.append("   (SELECT PAR.TEXT1 FROM PARAMETER_TABLE PAR WHERE PAR.PARAMETER_TABLE_PK = S.SECURITY_CLASS) AS CLASE_VALOR, ");
			//sbQuery.append("   S.SECURITY_CLASS AS CODIGO_CLASE_VALOR, ");
			sbQuery.append("   (SELECT PAR.TEXT1 FROM PARAMETER_TABLE PAR WHERE PAR.PARAMETER_TABLE_PK = S.CURRENCY) AS MONEDA, ");
			
			sbQuery.append(" CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416)  ");
			sbQuery.append("   THEN TO_CHAR(S.EXPIRATION_DATE,'DD/MM/YYYY') ");
			sbQuery.append("   ELSE TO_CHAR(PIC.EXPERITATION_DATE,'DD/MM/YYYY')  ");
			sbQuery.append(" END AS FECHA_EXP_STR,   ");
			
			//sbQuery.append("   S.CURRENCY AS CODIGO_MONEDA, ");
			sbQuery.append("   P.MNEMONIC AS PARTICIPANT, ");
			sbQuery.append("   HA.ACCOUNT_NUMBER AS NRO_CUENTA, ");
			sbQuery.append("   (SELECT LISTAGG(H.ID_HOLDER_PK,', ') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
			sbQuery.append("    FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
			sbQuery.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS CUI, ");
			sbQuery.append("   (SELECT LISTAGG(H.FULL_NAME,', ') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
			sbQuery.append("    FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
			sbQuery.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS CUI_DESCRIPTION, ");
			sbQuery.append("   S.ID_SECURITY_CODE_PK AS COD_VALOR, ");
			sbQuery.append("   (PIC.COUPON_NUMBER || '/' ||  ");
			sbQuery.append("       (SELECT MAX(PIC_SUB.COUPON_NUMBER) FROM INTEREST_PAYMENT_SCHEDULE IPS_SUB, PROGRAM_INTEREST_COUPON PIC_SUB ");
			sbQuery.append("       WHERE IPS_SUB.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK AND ");
			sbQuery.append("       PIC_SUB.ID_INT_PAYMENT_SCHEDULE_fK = IPS_SUB.ID_INT_PAYMENT_SCHEDULE_PK)) AS CUPON, ");
			sbQuery.append("   HAB.TOTAL_BALANCE AS TOTAL_CONTABLE, ");
			sbQuery.append("   HAB.AVAILABLE_BALANCE AS DISPONIBLE, ");
			sbQuery.append("   HAB.PAWN_BALANCE AS PRENDA, ");
			sbQuery.append("   HAB.BAN_BALANCE AS EMBARGO, ");
			sbQuery.append("   HAB.ACCREDITATION_BALANCE AS ACREDITACION, ");
			sbQuery.append("   ROUND(NVL(PIC.COUPON_AMOUNT,0),2) AS MONTO_INTERES, ");
			sbQuery.append("   ROUND(NVL(  ");

			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");	
			sbQuery.append(" 	,0),2) AS MONTO_AMORTIZACION, ");
			
//			sbQuery.append("   (ROUND(NVL(PIC.COUPON_AMOUNT,0),2) + ROUND(NVL(PAC.COUPON_AMOUNT,0),2)) AS MONTO_TOTAL, ");
			sbQuery.append("   (ROUND(NVL(PIC.COUPON_AMOUNT,0),2) + ROUND(NVL( ");
				
			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");			

			sbQuery.append(" 	,0),2)) * HAB.TOTAL_BALANCE AS TOTAL ");
//			sbQuery.append("    ,NVL( ");
//			sbQuery.append(" 		CASE ");     
//			sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1 ");     
//			sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304,430) THEN 1 * "); 
//			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
//			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
//			sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) ");
//			sbQuery.append(" 			WHEN S.CURRENCY IN (1853) THEN 1 * "); 
//			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
//			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
//			sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
//			sbQuery.append(" 			ELSE 1 ");   
//			sbQuery.append(" 	END,1) MONTO_BOB, ");
//			sbQuery.append("    NVL( ");
//			sbQuery.append(" 		CASE ");
//			sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1 / ");
//			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
//			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
//			sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
//			sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304) THEN 1 *	");
//			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
//			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
//			sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) / ");
//			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
//			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
//			sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
//			sbQuery.append(" 			WHEN S.CURRENCY IN (430,1853) THEN 1 ");
//			sbQuery.append(" 			ELSE 1 ");
//			sbQuery.append(" 	END,0) MONTO_USD ");
			
			sbQuery.append("  ");
			sbQuery.append(" FROM HOLDER_ACCOUNT_BALANCE hab ");
			sbQuery.append("   INNER JOIN SECURITY S                         ON s.ID_SECURITY_CODE_PK=hab.ID_SECURITY_CODE_PK ");
			sbQuery.append("   INNER JOIN HOLDER_ACCOUNT ha                  ON hab.ID_HOLDER_ACCOUNT_PK= ha.ID_HOLDER_ACCOUNT_PK ");
			sbQuery.append("   INNER JOIN ISSUER I                           ON S.ID_ISSUER_FK = I.ID_ISSUER_PK ");
			sbQuery.append("   INNER JOIN PARTICIPANT P                      ON P.ID_PARTICIPANT_PK = HAB.ID_PARTICIPANT_PK ");
			sbQuery.append("   LEFT JOIN interest_payment_schedule ips      ON s.id_security_code_pk=ips.id_security_code_fk ");
			sbQuery.append("   LEFT JOIN program_interest_coupon pic        ON ips.id_int_payment_schedule_pk=pic.id_int_payment_schedule_fk ");
			sbQuery.append("   LEFT JOIN amortization_payment_schedule APS   ON APS.ID_SECURITY_CODE_FK = S.id_security_code_pk  ");
			sbQuery.append("   LEFT JOIN PROGRAM_AMORTIZATION_COUPON PAC     ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK ");
			sbQuery.append("                                                 AND TRUNC(PIC.EXPERITATION_DATE) = TRUNC(PAC.EXPRITATION_DATE) ");
			sbQuery.append(" WHERE 1 = 1 ");
	//		sbQuery.append("   AND trunc(PIC.EXPERITATION_DATE) >= to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') ");
	//		sbQuery.append("   AND trunc(PIC.EXPERITATION_DATE) <= to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy') ");
						
			sbQuery.append("  AND (trunc(PIC.EXPERITATION_DATE) between to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') and to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy')  ");
			sbQuery.append("   OR CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) ");
			sbQuery.append("    THEN trunc(S.EXPIRATION_DATE)  END ");
			sbQuery.append("   between to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') and to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy')) ");
			
			sbQuery.append("   AND HAB.TOTAL_BALANCE > 0 ");
			
			if(Validations.validateIsNotNull(gfto.getCui())){
				sbQuery.append(" AND (SELECT COUNT(*) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK=HAB.ID_HOLDER_ACCOUNT_PK AND HAD.ID_HOLDER_FK="+ gfto.getCui() + ")>0 ");
			}
			if(Validations.validateIsNotNull(gfto.getHolderAccount())){
				sbQuery.append(" AND HAB.ID_HOLDER_ACCOUNT_PK="+gfto.getHolderAccount()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getParticipantMnemonic())){
				//sbQuery.append(" AND HAB.ID_PARTICIPANT_PK="+gfto.getParticipant()+" ");
				// issue 1341: en este reporte se busca por agente pagador del valor
				sbQuery.append(" AND S.PAYMENT_AGENT='"+gfto.getParticipantMnemonic()+"' ");
				// por requerimiento de usuario se omite DPFs
				sbQuery.append(" AND S.SECURITY_CLASS != 420 ");
			}
			if(Validations.validateIsNotNull(gfto.getSecurities())){
				sbQuery.append(" AND S.ID_SECURITY_CODE_PK='"+gfto.getSecurities()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getIssuer())){
				sbQuery.append(" AND S.ID_ISSUER_FK='"+gfto.getIssuer()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getSecurityClass())){
				sbQuery.append(" AND S.SECURITY_CLASS="+gfto.getSecurityClass()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getCurrency())){
				sbQuery.append(" AND S.CURRENCY="+gfto.getCurrency()+" ");
			}
			
			sbQuery.append(" UNION  ");
			sbQuery.append(" SELECT  ");
			
			sbQuery.append("   (SELECT PAR.TEXT1 FROM PARAMETER_TABLE PAR WHERE PAR.PARAMETER_TABLE_PK = S.SECURITY_CLASS) AS CLASE_VALOR, ");
			//sbQuery.append("   S.SECURITY_CLASS AS CODIGO_CLASE_VALOR, ");
			sbQuery.append("   (SELECT PAR.TEXT1 FROM PARAMETER_TABLE PAR WHERE PAR.PARAMETER_TABLE_PK = S.CURRENCY) AS MONEDA, ");
			
			sbQuery.append(" CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416)  ");
			sbQuery.append("   THEN TO_CHAR(S.EXPIRATION_DATE,'DD/MM/YYYY') ");
			sbQuery.append("   ELSE TO_CHAR(PAC.EXPRITATION_DATE,'DD/MM/YYYY')  ");
			sbQuery.append(" END AS FECHA_EXP_STR,   ");
			
			//sbQuery.append("   S.CURRENCY AS CODIGO_MONEDA, ");
			sbQuery.append("   P.MNEMONIC AS PARTICIPANT, ");
			sbQuery.append("   HA.ACCOUNT_NUMBER AS NRO_CUENTA, ");
			sbQuery.append("   (SELECT LISTAGG(H.ID_HOLDER_PK,', ') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
			sbQuery.append("    FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
			sbQuery.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS CUI, ");
			sbQuery.append("   (SELECT LISTAGG(H.FULL_NAME,', ') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
			sbQuery.append("    FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
			sbQuery.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS CUI_DESCRIPTION, ");
			sbQuery.append("   S.ID_SECURITY_CODE_PK AS COD_VALOR, ");
			sbQuery.append("   (CASE WHEN S.security_class IN ( 409 ) THEN '-' END ) AS CUPON, ");
			sbQuery.append("   HAB.TOTAL_BALANCE AS TOTAL_CONTABLE, ");
			sbQuery.append("   HAB.AVAILABLE_BALANCE AS DISPONIBLE, ");
			sbQuery.append("   HAB.PAWN_BALANCE AS PRENDA, ");
			sbQuery.append("   HAB.BAN_BALANCE AS EMBARGO, ");
			sbQuery.append("   HAB.ACCREDITATION_BALANCE AS ACREDITACION, ");
			sbQuery.append("   0 AS MONTO_INTERES, ");
			sbQuery.append("   ROUND(NVL(  ");

			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (409) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");	
			sbQuery.append(" 	,0),2) AS MONTO_AMORTIZACION, ");
			
			sbQuery.append("    ( 0 + Round(Nvl( CASE WHEN S.security_class IN (409) THEN S.current_nominal_value ELSE PAC.coupon_amount END, 0), 2) ) * HAB.total_balance AS TOTAL ");
			
			sbQuery.append("  ");
			sbQuery.append(" FROM HOLDER_ACCOUNT_BALANCE hab ");
			sbQuery.append("   INNER JOIN SECURITY S                         ON s.ID_SECURITY_CODE_PK=hab.ID_SECURITY_CODE_PK ");
			sbQuery.append("   INNER JOIN HOLDER_ACCOUNT ha                  ON hab.ID_HOLDER_ACCOUNT_PK= ha.ID_HOLDER_ACCOUNT_PK ");
			sbQuery.append("   INNER JOIN ISSUER I                           ON S.ID_ISSUER_FK = I.ID_ISSUER_PK ");
			sbQuery.append("   INNER JOIN PARTICIPANT P                      ON P.ID_PARTICIPANT_PK = HAB.ID_PARTICIPANT_PK ");
			sbQuery.append("   LEFT JOIN amortization_payment_schedule APS   ON APS.ID_SECURITY_CODE_FK = S.id_security_code_pk  ");
			sbQuery.append("   LEFT JOIN PROGRAM_AMORTIZATION_COUPON PAC     ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK ");
			
			sbQuery.append(" WHERE 1 = 1 ");
			
			sbQuery.append("   AND S.SECURITY_CLASS IN (409) ");
			sbQuery.append("   AND TRUNC(S.EXPIRATION_DATE) BETWEEN to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') AND to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy') ");
			sbQuery.append("   AND s.ID_REF_SECURITY_CODE_FK IS NOT NULL ");
			
			sbQuery.append("   AND HAB.TOTAL_BALANCE > 0 ");
			
			if(Validations.validateIsNotNull(gfto.getCui())){
				sbQuery.append(" AND (SELECT COUNT(*) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK=HAB.ID_HOLDER_ACCOUNT_PK AND HAD.ID_HOLDER_FK="+ gfto.getCui() + ")>0 ");
			}
			if(Validations.validateIsNotNull(gfto.getHolderAccount())){
				sbQuery.append(" AND HAB.ID_HOLDER_ACCOUNT_PK="+gfto.getHolderAccount()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getParticipantMnemonic())){
				//sbQuery.append(" AND HAB.ID_PARTICIPANT_PK="+gfto.getParticipant()+" ");
				// issue 1341: en este reporte se busca por agente pagador del valor
				sbQuery.append(" AND S.PAYMENT_AGENT='"+gfto.getParticipantMnemonic()+"' ");
				// por requerimiento de usuario se omite DPFs
				sbQuery.append(" AND S.SECURITY_CLASS != 420 ");
			}
			if(Validations.validateIsNotNull(gfto.getSecurities())){
				sbQuery.append(" AND S.ID_SECURITY_CODE_PK='"+gfto.getSecurities()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getIssuer())){
				sbQuery.append(" AND S.ID_ISSUER_FK='"+gfto.getIssuer()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getSecurityClass())){
				sbQuery.append(" AND S.SECURITY_CLASS="+gfto.getSecurityClass()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getCurrency())){
				sbQuery.append(" AND S.CURRENCY="+gfto.getCurrency()+" ");
			}
		}else{
			sbQuery.append(" SELECT      ");
			
			sbQuery.append("         (SELECT PAR.PARAMETER_NAME FROM PARAMETER_TABLE PAR WHERE PAR.PARAMETER_TABLE_PK = S.SECURITY_CLASS) AS CLASE_VALOR,     ");
			//sbQuery.append("         S.SECURITY_CLASS AS CODIGO_CLASE_VALOR,     ");
			sbQuery.append("         (SELECT PAR.TEXT1 FROM PARAMETER_TABLE PAR WHERE PAR.PARAMETER_TABLE_PK = S.CURRENCY) AS MONEDA,     ");
			sbQuery.append("         TO_CHAR(PIC.EXPERITATION_DATE, 'DD/MM/YYYY') FECHA_EXP_STR,     ");
			//sbQuery.append("         S.CURRENCY AS CODIGO_MONEDA,     ");
			sbQuery.append("         P.MNEMONIC AS PARTICIPANT,     ");
			sbQuery.append("         HA.ACCOUNT_NUMBER AS NRO_CUENTA,     ");
			sbQuery.append("         (SELECT LISTAGG(H.ID_HOLDER_PK,', ') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)      ");
			sbQuery.append("             FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK      ");
			sbQuery.append("             WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS CUI,     ");
			sbQuery.append("         (SELECT LISTAGG(H.FULL_NAME,', ') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)      ");
			sbQuery.append("             FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK      ");
			sbQuery.append("             WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS CUI_DESCRIPTION,     ");
			sbQuery.append("         S.ID_SECURITY_CODE_PK AS COD_VALOR,     ");
			sbQuery.append("         (PIC.COUPON_NUMBER || '/' ||          ");
			sbQuery.append("             (SELECT MAX(PIC_SUB.COUPON_NUMBER) FROM INTEREST_PAYMENT_SCHEDULE IPS_SUB, PROGRAM_INTEREST_COUPON PIC_SUB         ");
			sbQuery.append("             WHERE IPS_SUB.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK AND         ");
			sbQuery.append("             PIC_SUB.ID_INT_PAYMENT_SCHEDULE_fK = IPS_SUB.ID_INT_PAYMENT_SCHEDULE_PK)) AS CUPON,     ");
			sbQuery.append("         PBC.CERTIFICATE_QUANTITY AS TOTAL_CONTABLE,     ");
			sbQuery.append("         PBC.CERTIFICATE_QUANTITY AS DISPONIBLE,     ");
			sbQuery.append("         0 AS PRENDA,     ");
			sbQuery.append("         0 AS EMBARGO,     ");
			sbQuery.append("         0 AS ACREDITACION,     ");
			sbQuery.append("         ROUND(NVL(PIC.COUPON_AMOUNT,0),2) AS MONTO_INTERES,     ");
			sbQuery.append("         ROUND(NVL(   ");

			sbQuery.append("			CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) 	");
			sbQuery.append("       			THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      			ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  				END		");	
			sbQuery.append(" 		 ,0),2) AS MONTO_AMORTIZACION,     ");
			
//			sbQuery.append("         (ROUND(NVL(PIC.COUPON_AMOUNT,0),2) + ROUND(NVL(PAC.COUPON_AMOUNT,0),2)) AS MONTO_TOTAL,     ");
			sbQuery.append("         (ROUND(NVL(PIC.COUPON_AMOUNT,0),2) + ROUND(NVL(  ");
			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");	
			sbQuery.append("		 ,0),2)) * PBC.CERTIFICATE_QUANTITY AS TOTAL     ");
//			sbQuery.append("         NVL(  		CASE  			WHEN S.CURRENCY = 127 THEN 1  			WHEN S.CURRENCY IN (1734,1304,430) THEN 1 *  				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1)  			WHEN S.CURRENCY IN (1853) THEN 1 *  				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)  			ELSE 1  	END,1) MONTO_BOB,      ");
//			sbQuery.append("         NVL(  		CASE  			WHEN S.CURRENCY = 127 THEN 1 /  				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)  			WHEN S.CURRENCY IN (1734,1304) THEN 1 *	 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /  				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)  			WHEN S.CURRENCY IN (430,1853) THEN 1  			ELSE 1  	END,0) MONTO_USD     ");
			sbQuery.append("     FROM  ");
			sbQuery.append("         PHYSICAL_BALANCE_DETAIL PBC ");
			sbQuery.append("         INNER JOIN PHYSICAL_CERTIFICATE PC            ON PC.ID_PHYSICAL_CERTIFICATE_PK=PBC.ID_PHYSICAL_CERTIFICATE_FK ");
			sbQuery.append("         INNER JOIN SECURITY S                         ON S.ID_SECURITY_CODE_PK=PC.ID_SECURITY_CODE_FK ");
			sbQuery.append("         INNER JOIN HOLDER_ACCOUNT HA                  ON PBC.ID_HOLDER_ACCOUNT_FK= HA.ID_HOLDER_ACCOUNT_PK  ");
			sbQuery.append("         INNER JOIN ISSUER I                           ON S.ID_ISSUER_FK = I.ID_ISSUER_PK  ");
			sbQuery.append("         INNER JOIN PARTICIPANT P                      ON P.ID_PARTICIPANT_PK = PBC.ID_PARTICIPANT_FK  ");
			sbQuery.append("         LEFT JOIN interest_payment_schedule ips      ON S.id_security_code_pk=ips.id_security_code_fk  ");
			sbQuery.append("         LEFT JOIN program_interest_coupon pic        ON ips.id_int_payment_schedule_pk=pic.id_int_payment_schedule_fk  ");
			sbQuery.append("         LEFT JOIN amortization_payment_schedule APS   ON APS.ID_SECURITY_CODE_FK = S.id_security_code_pk   ");
			sbQuery.append("         LEFT JOIN PROGRAM_AMORTIZATION_COUPON PAC     ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK  ");
			sbQuery.append(" 		                                                 AND TRUNC(PIC.EXPERITATION_DATE) = TRUNC(PAC.EXPRITATION_DATE) ");
			sbQuery.append("     WHERE 1 = 1     ");
			
			sbQuery.append("  AND (trunc(PIC.EXPERITATION_DATE) between to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') and to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy')  ");
			sbQuery.append("   OR CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) ");
			sbQuery.append("    THEN trunc(S.EXPIRATION_DATE)  END ");
			sbQuery.append("   between to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') and to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy')) ");
			
			sbQuery.append("          ");
			
			if(Validations.validateIsNotNull(gfto.getCui())){
				sbQuery.append(" AND (SELECT COUNT(*) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK=HAB.ID_HOLDER_ACCOUNT_PK AND HAD.ID_HOLDER_FK="+ gfto.getCui() + ")>0 ");
			}
			if(Validations.validateIsNotNull(gfto.getHolderAccount())){
				sbQuery.append(" AND PBC.ID_HOLDER_ACCOUNT_FK="+gfto.getHolderAccount()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getParticipantMnemonic())){
				//sbQuery.append(" AND HAB.ID_PARTICIPANT_PK="+gfto.getParticipant()+" ");
				// issue 1341: en este reporte se busca por agente pagador del valor
				sbQuery.append(" AND S.PAYMENT_AGENT='"+gfto.getParticipantMnemonic()+"' ");
				// por requerimiento de usuario se omite DPFs
				sbQuery.append(" AND S.SECURITY_CLASS != 420 ");
			}
			if(Validations.validateIsNotNull(gfto.getSecurities())){
				sbQuery.append(" AND S.ID_SECURITY_CODE_PK='"+gfto.getSecurities()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getIssuer())){
				sbQuery.append(" AND S.ID_ISSUER_FK='"+gfto.getIssuer()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getSecurityClass())){
				sbQuery.append(" AND S.SECURITY_CLASS="+gfto.getSecurityClass()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getCurrency())){
				sbQuery.append(" AND S.CURRENCY="+gfto.getCurrency()+" ");
			}
		}
		
		sbQuery.append("   ORDER BY FECHA_EXP_STR,CLASE_VALOR,MONEDA,COD_VALOR,NRO_CUENTA ");
		sbQuery.append("  ");
		System.out.println("Script: " + sbQuery.toString());
    	return sbQuery.toString();
    }
    
    
    public String getDetailsOfTitlesPendingOfExpirationPoiReport(GenericIssuanceTO gfto){
    	StringBuilder sbQuery= new StringBuilder();
		Date currentDate = CommonsUtilities.currentDate();
		String currDateStr = CommonsUtilities.convertDatetoStringForTemplate(currentDate, CommonsUtilities.DATE_PATTERN);
    	System.out.println("=======> IF: " + gfto.getSecuritiesSituation());
		
		if(gfto.getSecuritiesSituation().equals(GeneralConstants.ONE_VALUE_STRING)){
			sbQuery.append(" SELECT  ");
			
			sbQuery.append("   (SELECT PAR.TEXT1 FROM PARAMETER_TABLE PAR WHERE PAR.PARAMETER_TABLE_PK = S.SECURITY_CLASS) AS CLASE_VALOR, ");
			//sbQuery.append("   S.SECURITY_CLASS AS CODIGO_CLASE_VALOR, ");
			sbQuery.append("   (SELECT PAR.TEXT1 FROM PARAMETER_TABLE PAR WHERE PAR.PARAMETER_TABLE_PK = S.CURRENCY) AS MONEDA, ");
			
			sbQuery.append(" CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416)  ");
			sbQuery.append("   THEN TO_CHAR(S.EXPIRATION_DATE,'DD/MM/YYYY') ");
			sbQuery.append("   ELSE TO_CHAR(PIC.EXPERITATION_DATE,'DD/MM/YYYY')  ");
			sbQuery.append(" END AS FECHA_EXP_STR,   ");
			
			//sbQuery.append("   S.CURRENCY AS CODIGO_MONEDA, ");
			sbQuery.append("   P.MNEMONIC AS PARTICIPANT, ");
			sbQuery.append("   HA.ACCOUNT_NUMBER AS NRO_CUENTA, ");
			sbQuery.append("   (SELECT LISTAGG(H.ID_HOLDER_PK,', ') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
			sbQuery.append("    FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
			sbQuery.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS CUI, ");
			sbQuery.append("   (SELECT LISTAGG(H.FULL_NAME,', ') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
			sbQuery.append("    FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
			sbQuery.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS CUI_DESCRIPTION, ");
			sbQuery.append("   S.ID_SECURITY_CODE_PK AS COD_VALOR, ");
			sbQuery.append("   (PIC.COUPON_NUMBER || '/' ||  ");
			sbQuery.append("       (SELECT MAX(PIC_SUB.COUPON_NUMBER) FROM INTEREST_PAYMENT_SCHEDULE IPS_SUB, PROGRAM_INTEREST_COUPON PIC_SUB ");
			sbQuery.append("       WHERE IPS_SUB.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK AND ");
			sbQuery.append("       PIC_SUB.ID_INT_PAYMENT_SCHEDULE_fK = IPS_SUB.ID_INT_PAYMENT_SCHEDULE_PK)) AS CUPON, ");
			sbQuery.append("   HAB.TOTAL_BALANCE AS TOTAL_CONTABLE, ");
			sbQuery.append("   HAB.AVAILABLE_BALANCE AS DISPONIBLE, ");
			sbQuery.append("   HAB.PAWN_BALANCE AS PRENDA, ");
			sbQuery.append("   HAB.BAN_BALANCE AS EMBARGO, ");
			sbQuery.append("   HAB.ACCREDITATION_BALANCE AS ACREDITACION, ");
			sbQuery.append("   ROUND(NVL(PIC.COUPON_AMOUNT,0),2) AS MONTO_INTERES, ");
			sbQuery.append("   ROUND(NVL(  ");

			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");	
			sbQuery.append(" 	,0),2) AS MONTO_AMORTIZACION, ");
			
//			sbQuery.append("   (ROUND(NVL(PIC.COUPON_AMOUNT,0),2) + ROUND(NVL(PAC.COUPON_AMOUNT,0),2)) AS MONTO_TOTAL, ");
			sbQuery.append("   (ROUND(NVL(PIC.COUPON_AMOUNT,0),2) + ROUND(NVL( ");
				
			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");			

			sbQuery.append(" 	,0),2)) * HAB.TOTAL_BALANCE AS TOTAL ");
//			sbQuery.append("    ,NVL( ");
//			sbQuery.append(" 		CASE ");     
//			sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1 ");     
//			sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304,430) THEN 1 * "); 
//			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
//			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
//			sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) ");
//			sbQuery.append(" 			WHEN S.CURRENCY IN (1853) THEN 1 * "); 
//			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
//			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
//			sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
//			sbQuery.append(" 			ELSE 1 ");   
//			sbQuery.append(" 	END,1) MONTO_BOB, ");
//			sbQuery.append("    NVL( ");
//			sbQuery.append(" 		CASE ");
//			sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1 / ");
//			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
//			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
//			sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
//			sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304) THEN 1 *	");
//			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
//			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
//			sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) / ");
//			sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
//			sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY') ");
//			sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
//			sbQuery.append(" 			WHEN S.CURRENCY IN (430,1853) THEN 1 ");
//			sbQuery.append(" 			ELSE 1 ");
//			sbQuery.append(" 	END,0) MONTO_USD ");
			
			sbQuery.append("  ");
			sbQuery.append(" FROM HOLDER_ACCOUNT_BALANCE hab ");
			sbQuery.append("   INNER JOIN SECURITY S                         ON s.ID_SECURITY_CODE_PK=hab.ID_SECURITY_CODE_PK ");
			sbQuery.append("   INNER JOIN HOLDER_ACCOUNT ha                  ON hab.ID_HOLDER_ACCOUNT_PK= ha.ID_HOLDER_ACCOUNT_PK ");
			sbQuery.append("   INNER JOIN ISSUER I                           ON S.ID_ISSUER_FK = I.ID_ISSUER_PK ");
			sbQuery.append("   INNER JOIN PARTICIPANT P                      ON P.ID_PARTICIPANT_PK = HAB.ID_PARTICIPANT_PK ");
			sbQuery.append("   LEFT JOIN interest_payment_schedule ips      ON s.id_security_code_pk=ips.id_security_code_fk ");
			sbQuery.append("   LEFT JOIN program_interest_coupon pic        ON ips.id_int_payment_schedule_pk=pic.id_int_payment_schedule_fk ");
			sbQuery.append("   LEFT JOIN amortization_payment_schedule APS   ON APS.ID_SECURITY_CODE_FK = S.id_security_code_pk  ");
			sbQuery.append("   LEFT JOIN PROGRAM_AMORTIZATION_COUPON PAC     ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK ");
			sbQuery.append("                                                 AND TRUNC(PIC.EXPERITATION_DATE) = TRUNC(PAC.EXPRITATION_DATE) ");
			sbQuery.append(" WHERE 1 = 1 ");
	//		sbQuery.append("   AND trunc(PIC.EXPERITATION_DATE) >= to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') ");
	//		sbQuery.append("   AND trunc(PIC.EXPERITATION_DATE) <= to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy') ");
						
			sbQuery.append("  AND (trunc(PIC.EXPERITATION_DATE) between to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') and to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy')  ");
			sbQuery.append("   OR CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) ");
			sbQuery.append("    THEN trunc(S.EXPIRATION_DATE)  END ");
			sbQuery.append("   between to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') and to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy')) ");
			
			sbQuery.append("   AND HAB.TOTAL_BALANCE > 0 ");
			
			if(Validations.validateIsNotNull(gfto.getCui())){
				sbQuery.append(" AND (SELECT COUNT(*) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK=HAB.ID_HOLDER_ACCOUNT_PK AND HAD.ID_HOLDER_FK="+ gfto.getCui() + ")>0 ");
			}
			if(Validations.validateIsNotNull(gfto.getHolderAccount())){
				sbQuery.append(" AND HAB.ID_HOLDER_ACCOUNT_PK="+gfto.getHolderAccount()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getParticipant())){
				sbQuery.append(" AND HAB.ID_PARTICIPANT_PK="+gfto.getParticipant()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getSecurities())){
				sbQuery.append(" AND S.ID_SECURITY_CODE_PK='"+gfto.getSecurities()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getIssuer())){
				sbQuery.append(" AND S.ID_ISSUER_FK='"+gfto.getIssuer()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getSecurityClass())){
				sbQuery.append(" AND S.SECURITY_CLASS="+gfto.getSecurityClass()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getCurrency())){
				sbQuery.append(" AND S.CURRENCY="+gfto.getCurrency()+" ");
			}
			
			sbQuery.append(" UNION  ");
			sbQuery.append(" SELECT  ");
			
			sbQuery.append("   (SELECT PAR.TEXT1 FROM PARAMETER_TABLE PAR WHERE PAR.PARAMETER_TABLE_PK = S.SECURITY_CLASS) AS CLASE_VALOR, ");
			//sbQuery.append("   S.SECURITY_CLASS AS CODIGO_CLASE_VALOR, ");
			sbQuery.append("   (SELECT PAR.TEXT1 FROM PARAMETER_TABLE PAR WHERE PAR.PARAMETER_TABLE_PK = S.CURRENCY) AS MONEDA, ");
			
			sbQuery.append(" CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416)  ");
			sbQuery.append("   THEN TO_CHAR(S.EXPIRATION_DATE,'DD/MM/YYYY') ");
			sbQuery.append("   ELSE TO_CHAR(PAC.EXPRITATION_DATE,'DD/MM/YYYY')  ");
			sbQuery.append(" END AS FECHA_EXP_STR,   ");
			
			//sbQuery.append("   S.CURRENCY AS CODIGO_MONEDA, ");
			sbQuery.append("   P.MNEMONIC AS PARTICIPANT, ");
			sbQuery.append("   HA.ACCOUNT_NUMBER AS NRO_CUENTA, ");
			sbQuery.append("   (SELECT LISTAGG(H.ID_HOLDER_PK,', ') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
			sbQuery.append("    FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
			sbQuery.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS CUI, ");
			sbQuery.append("   (SELECT LISTAGG(H.FULL_NAME,', ') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
			sbQuery.append("    FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
			sbQuery.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS CUI_DESCRIPTION, ");
			sbQuery.append("   S.ID_SECURITY_CODE_PK AS COD_VALOR, ");
			sbQuery.append("   (CASE WHEN S.security_class IN ( 409 ) THEN '-' END ) AS CUPON, ");
			sbQuery.append("   HAB.TOTAL_BALANCE AS TOTAL_CONTABLE, ");
			sbQuery.append("   HAB.AVAILABLE_BALANCE AS DISPONIBLE, ");
			sbQuery.append("   HAB.PAWN_BALANCE AS PRENDA, ");
			sbQuery.append("   HAB.BAN_BALANCE AS EMBARGO, ");
			sbQuery.append("   HAB.ACCREDITATION_BALANCE AS ACREDITACION, ");
			sbQuery.append("   0 AS MONTO_INTERES, ");
			sbQuery.append("   ROUND(NVL(  ");

			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (409) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");	
			sbQuery.append(" 	,0),2) AS MONTO_AMORTIZACION, ");
			
			sbQuery.append("    ( 0 + Round(Nvl( CASE WHEN S.security_class IN (409) THEN S.current_nominal_value ELSE PAC.coupon_amount END, 0), 2) ) * HAB.total_balance AS TOTAL ");
			
			sbQuery.append("  ");
			sbQuery.append(" FROM HOLDER_ACCOUNT_BALANCE hab ");
			sbQuery.append("   INNER JOIN SECURITY S                         ON s.ID_SECURITY_CODE_PK=hab.ID_SECURITY_CODE_PK ");
			sbQuery.append("   INNER JOIN HOLDER_ACCOUNT ha                  ON hab.ID_HOLDER_ACCOUNT_PK= ha.ID_HOLDER_ACCOUNT_PK ");
			sbQuery.append("   INNER JOIN ISSUER I                           ON S.ID_ISSUER_FK = I.ID_ISSUER_PK ");
			sbQuery.append("   INNER JOIN PARTICIPANT P                      ON P.ID_PARTICIPANT_PK = HAB.ID_PARTICIPANT_PK ");
			sbQuery.append("   LEFT JOIN amortization_payment_schedule APS   ON APS.ID_SECURITY_CODE_FK = S.id_security_code_pk  ");
			sbQuery.append("   LEFT JOIN PROGRAM_AMORTIZATION_COUPON PAC     ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK ");
			
			sbQuery.append(" WHERE 1 = 1 ");
			
			sbQuery.append("   AND S.SECURITY_CLASS IN (409) ");
			sbQuery.append("   AND TRUNC(S.EXPIRATION_DATE) BETWEEN to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') AND to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy') ");
			sbQuery.append("   AND s.ID_REF_SECURITY_CODE_FK IS NOT NULL ");
			
			sbQuery.append("   AND HAB.TOTAL_BALANCE > 0 ");
			
			if(Validations.validateIsNotNull(gfto.getCui())){
				sbQuery.append(" AND (SELECT COUNT(*) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK=HAB.ID_HOLDER_ACCOUNT_PK AND HAD.ID_HOLDER_FK="+ gfto.getCui() + ")>0 ");
			}
			if(Validations.validateIsNotNull(gfto.getHolderAccount())){
				sbQuery.append(" AND HAB.ID_HOLDER_ACCOUNT_PK="+gfto.getHolderAccount()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getParticipant())){
				sbQuery.append(" AND HAB.ID_PARTICIPANT_PK="+gfto.getParticipant()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getSecurities())){
				sbQuery.append(" AND S.ID_SECURITY_CODE_PK='"+gfto.getSecurities()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getIssuer())){
				sbQuery.append(" AND S.ID_ISSUER_FK='"+gfto.getIssuer()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getSecurityClass())){
				sbQuery.append(" AND S.SECURITY_CLASS="+gfto.getSecurityClass()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getCurrency())){
				sbQuery.append(" AND S.CURRENCY="+gfto.getCurrency()+" ");
			}
		}else{
			sbQuery.append(" SELECT      ");
			
			sbQuery.append("         (SELECT PAR.PARAMETER_NAME FROM PARAMETER_TABLE PAR WHERE PAR.PARAMETER_TABLE_PK = S.SECURITY_CLASS) AS CLASE_VALOR,     ");
			//sbQuery.append("         S.SECURITY_CLASS AS CODIGO_CLASE_VALOR,     ");
			sbQuery.append("         (SELECT PAR.TEXT1 FROM PARAMETER_TABLE PAR WHERE PAR.PARAMETER_TABLE_PK = S.CURRENCY) AS MONEDA,     ");
			sbQuery.append("         TO_CHAR(PIC.EXPERITATION_DATE, 'DD/MM/YYYY') FECHA_EXP_STR,     ");
			//sbQuery.append("         S.CURRENCY AS CODIGO_MONEDA,     ");
			sbQuery.append("         P.MNEMONIC AS PARTICIPANT,     ");
			sbQuery.append("         HA.ACCOUNT_NUMBER AS NRO_CUENTA,     ");
			sbQuery.append("         (SELECT LISTAGG(H.ID_HOLDER_PK,', ') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)      ");
			sbQuery.append("             FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK      ");
			sbQuery.append("             WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS CUI,     ");
			sbQuery.append("         (SELECT LISTAGG(H.FULL_NAME,', ') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)      ");
			sbQuery.append("             FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK      ");
			sbQuery.append("             WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS CUI_DESCRIPTION,     ");
			sbQuery.append("         S.ID_SECURITY_CODE_PK AS COD_VALOR,     ");
			sbQuery.append("         (PIC.COUPON_NUMBER || '/' ||          ");
			sbQuery.append("             (SELECT MAX(PIC_SUB.COUPON_NUMBER) FROM INTEREST_PAYMENT_SCHEDULE IPS_SUB, PROGRAM_INTEREST_COUPON PIC_SUB         ");
			sbQuery.append("             WHERE IPS_SUB.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK AND         ");
			sbQuery.append("             PIC_SUB.ID_INT_PAYMENT_SCHEDULE_fK = IPS_SUB.ID_INT_PAYMENT_SCHEDULE_PK)) AS CUPON,     ");
			sbQuery.append("         PBC.CERTIFICATE_QUANTITY AS TOTAL_CONTABLE,     ");
			sbQuery.append("         PBC.CERTIFICATE_QUANTITY AS DISPONIBLE,     ");
			sbQuery.append("         0 AS PRENDA,     ");
			sbQuery.append("         0 AS EMBARGO,     ");
			sbQuery.append("         0 AS ACREDITACION,     ");
			sbQuery.append("         ROUND(NVL(PIC.COUPON_AMOUNT,0),2) AS MONTO_INTERES,     ");
			sbQuery.append("         ROUND(NVL(   ");

			sbQuery.append("			CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) 	");
			sbQuery.append("       			THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      			ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  				END		");	
			sbQuery.append(" 		 ,0),2) AS MONTO_AMORTIZACION,     ");
			
//			sbQuery.append("         (ROUND(NVL(PIC.COUPON_AMOUNT,0),2) + ROUND(NVL(PAC.COUPON_AMOUNT,0),2)) AS MONTO_TOTAL,     ");
			sbQuery.append("         (ROUND(NVL(PIC.COUPON_AMOUNT,0),2) + ROUND(NVL(  ");
			sbQuery.append("		CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) 	");
			sbQuery.append("       		THEN S.CURRENT_NOMINAL_VALUE		");
			sbQuery.append("      		ELSE PAC.COUPON_AMOUNT 		");
			sbQuery.append("  		END		");	
			sbQuery.append("		 ,0),2)) * PBC.CERTIFICATE_QUANTITY AS TOTAL     ");
//			sbQuery.append("         NVL(  		CASE  			WHEN S.CURRENCY = 127 THEN 1  			WHEN S.CURRENCY IN (1734,1304,430) THEN 1 *  				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1)  			WHEN S.CURRENCY IN (1853) THEN 1 *  				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)  			ELSE 1  	END,1) MONTO_BOB,      ");
//			sbQuery.append("         NVL(  		CASE  			WHEN S.CURRENCY = 127 THEN 1 /  				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)  			WHEN S.CURRENCY IN (1734,1304) THEN 1 *	 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /  				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+currDateStr+"','DD/MM/YYYY')  						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)  			WHEN S.CURRENCY IN (430,1853) THEN 1  			ELSE 1  	END,0) MONTO_USD     ");
			sbQuery.append("     FROM  ");
			sbQuery.append("         PHYSICAL_BALANCE_DETAIL PBC ");
			sbQuery.append("         INNER JOIN PHYSICAL_CERTIFICATE PC            ON PC.ID_PHYSICAL_CERTIFICATE_PK=PBC.ID_PHYSICAL_CERTIFICATE_FK ");
			sbQuery.append("         INNER JOIN SECURITY S                         ON S.ID_SECURITY_CODE_PK=PC.ID_SECURITY_CODE_FK ");
			sbQuery.append("         INNER JOIN HOLDER_ACCOUNT HA                  ON PBC.ID_HOLDER_ACCOUNT_FK= HA.ID_HOLDER_ACCOUNT_PK  ");
			sbQuery.append("         INNER JOIN ISSUER I                           ON S.ID_ISSUER_FK = I.ID_ISSUER_PK  ");
			sbQuery.append("         INNER JOIN PARTICIPANT P                      ON P.ID_PARTICIPANT_PK = PBC.ID_PARTICIPANT_FK  ");
			sbQuery.append("         LEFT JOIN interest_payment_schedule ips      ON S.id_security_code_pk=ips.id_security_code_fk  ");
			sbQuery.append("         LEFT JOIN program_interest_coupon pic        ON ips.id_int_payment_schedule_pk=pic.id_int_payment_schedule_fk  ");
			sbQuery.append("         LEFT JOIN amortization_payment_schedule APS   ON APS.ID_SECURITY_CODE_FK = S.id_security_code_pk   ");
			sbQuery.append("         LEFT JOIN PROGRAM_AMORTIZATION_COUPON PAC     ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK  ");
			sbQuery.append(" 		                                                 AND TRUNC(PIC.EXPERITATION_DATE) = TRUNC(PAC.EXPRITATION_DATE) ");
			sbQuery.append("     WHERE 1 = 1     ");
			
			sbQuery.append("  AND (trunc(PIC.EXPERITATION_DATE) between to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') and to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy')  ");
			sbQuery.append("   OR CASE WHEN S.SECURITY_CLASS in (421,1951,1950,1937,2353,418,416) ");
			sbQuery.append("    THEN trunc(S.EXPIRATION_DATE)  END ");
			sbQuery.append("   between to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') and to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy')) ");
			
			sbQuery.append("          ");
			
			if(Validations.validateIsNotNull(gfto.getCui())){
				sbQuery.append(" AND (SELECT COUNT(*) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK=HAB.ID_HOLDER_ACCOUNT_PK AND HAD.ID_HOLDER_FK="+ gfto.getCui() + ")>0 ");
			}
			if(Validations.validateIsNotNull(gfto.getHolderAccount())){
				sbQuery.append(" AND PBC.ID_HOLDER_ACCOUNT_FK="+gfto.getHolderAccount()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getParticipant())){
				sbQuery.append(" AND PBC.ID_PARTICIPANT_FK="+gfto.getParticipant()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getSecurities())){
				sbQuery.append(" AND S.ID_SECURITY_CODE_PK='"+gfto.getSecurities()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getIssuer())){
				sbQuery.append(" AND S.ID_ISSUER_FK='"+gfto.getIssuer()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getSecurityClass())){
				sbQuery.append(" AND S.SECURITY_CLASS="+gfto.getSecurityClass()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getCurrency())){
				sbQuery.append(" AND S.CURRENCY="+gfto.getCurrency()+" ");
			}
		}
		
		sbQuery.append("   ORDER BY FECHA_EXP_STR,CLASE_VALOR,MONEDA,COD_VALOR,NRO_CUENTA ");
		sbQuery.append("  ");
		System.out.println("Script: " + sbQuery.toString());
    	return sbQuery.toString();
    }
    
    
    public List<Object[]> getQueryListByClass(String strQuery) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(strQuery);
		Query query = em.createNativeQuery(sbQuery.toString());
		return query.getResultList();
	}
}
