package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.service.IssuerQueryServiceBean;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.util.view.UtilReportConstants;


@ReportProcess(name="SecuritiesBlockReport")
public class SecuritiesBlockReport extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@Inject  PraderaLogger log; 
	
	@EJB
	ParticipantServiceBean participantServiceBean;

	@EJB
	private IssuerQueryServiceBean issuerQueryServiceBean;

	@EJB
	private HolderAccountComponentServiceBean accountComponentServiceBean;
	
	public SecuritiesBlockReport() {

	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void addParametersQueryReport() {
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		
		Map<Integer,String> block = new HashMap<Integer, String>();
		Map<Integer,String> state = new HashMap<Integer, String>();
		Map<Integer,String> classValue = new HashMap<Integer, String>();
		Map<Integer,String> levelAfectation = new HashMap<Integer, String>();
		Map<Integer,String> typeDocument = new HashMap<Integer, String>();
		
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		String issuerCode = null;
		String issuerMnemonic = null;
		String holderAccountCode = null;
		String holderAccountNumber = null;
		String participantCode = null;
		String participantMnemonic = null;

		// DATOS DE ENTRADA DEL SISTEMA
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.PARTICIPANT_PARAM))
				participantCode = r.getFilterValue();
			if (r.getFilterName().equals(ReportConstant.ISSUER_PARAM))
				issuerCode = r.getFilterValue();
			if (r.getFilterName().equals(ReportConstant.ACCOUNT_HOLDER))
				holderAccountCode = r.getFilterValue();
		}
		if(issuerCode != null)
			issuerMnemonic = issuerQueryServiceBean.find(issuerCode,Issuer.class).getMnemonic();
		else
			issuerMnemonic = "TODOS";
		
		if(holderAccountCode != null)
			holderAccountNumber = accountComponentServiceBean.find(new Long(holderAccountCode),HolderAccount.class).getAccountNumber().toString();
		else
			holderAccountNumber = "TODOS";
		
		if(participantCode != null)
			participantMnemonic = participantServiceBean.find(new Long(participantCode),Participant.class).getMnemonic();
		else
			participantMnemonic = "TODOS";
		
		try {

			filter.setMasterTableFk(MasterTableType.TYPE_AFFECTATION.getCode());
			for (ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				block.put(param.getParameterTablePk(),param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.STATE_AFFECTATION.getCode());
			for (ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				state.put(param.getParameterTablePk(),param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for (ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				classValue.put(param.getParameterTablePk(),param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.LEVEL_AFFECTATION.getCode());
			for (ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				levelAfectation.put(param.getParameterTablePk(),param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
			for (ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				typeDocument.put(param.getParameterTablePk(),param.getIndicator1());
			}
			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		parametersRequired.put("participant_filter", participantMnemonic);
		parametersRequired.put("issuer_filter", issuerMnemonic);
		parametersRequired.put("account_holder_filter", holderAccountNumber);
		parametersRequired.put("p_block", block);
		parametersRequired.put("p_state", state);
		parametersRequired.put("p_classValue", classValue);
		parametersRequired.put("p_levelAfectation", levelAfectation);
		parametersRequired.put("p_typeDocument", typeDocument);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		
		// TODO Auto-generated method stub
		return parametersRequired;
		
	}

}
