package com.pradera.report.generation.executor.negotiation;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.helperui.service.BillingServiceQueryServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounts.Participant;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.negotiation.service.NegotiationReportServiceBean;
import com.pradera.report.generation.executor.negotiation.to.PrimaryAndSecondaryMarketTO;
import com.pradera.report.generation.executor.securities.service.PaymentChronogramBySecurity;
import com.pradera.report.generation.poi.ReportPoiGeneratorServiceBean;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CommissionsAnnotationAccountReport.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@ReportProcess(name = "PrimaryAndSecondoryMarketPoiReport")
public class PrimaryAndSecondoryMarketPoiReport extends GenericReport {
 
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The parameter service. */
	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	private BillingServiceQueryServiceBean billingServiceQueryServiceBean;
	
	@EJB
	private ParticipantServiceBean participantServiceBean;
	
	/** The Report poi generator service bean. */
	@Inject
	private ReportPoiGeneratorServiceBean reportPoiGeneratorServiceBean;
	
	@EJB
	private NegotiationReportServiceBean negotiationReportServiceBean;
	
	
	//TODO varialbes del bean
	private static final Integer PRIMARY_MARKET=1;
	private static final Integer SECONDARY_MARKET=2;
	
	@EJB
	private PaymentChronogramBySecurity paymentChronogramService;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	
	/**
	 * Instantiates a new commissions annotation account report.
	 */
	public PrimaryAndSecondoryMarketPoiReport() {	
	}

	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#generateData(com.pradera.model.report.ReportLogger)
	 */
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] arrayByteExcel = new byte[16384];
		
		PrimaryAndSecondaryMarketTO primaryAndSecondaryMarketTO=new PrimaryAndSecondaryMarketTO();
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		Map<String,Object> parametersRequired = new HashMap<>();
		
		for (int i = 0; i < listaLogger.size(); i++){
			if(listaLogger.get(i).getFilterName().equals("p_type_market")){
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
			    	primaryAndSecondaryMarketTO.setTypeMarket(Integer.parseInt(listaLogger.get(i).getFilterValue()));
			    }
			}else if(listaLogger.get(i).getFilterName().equals("p_participant")){
				if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					Participant particioantBuyer=participantServiceBean.find(Participant.class, Long.parseLong(listaLogger.get(i).getFilterValue()));
					primaryAndSecondaryMarketTO.setMnemonicParticipant(particioantBuyer.getMnemonic());
				}
			}else if(listaLogger.get(i).getFilterName().equals("p_cui")){
				if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					primaryAndSecondaryMarketTO.setIdHolderSelected(Long.parseLong(listaLogger.get(i).getFilterValue()));
				}
			}else if(listaLogger.get(i).getFilterName().equals("p_initial_date")){
				if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					primaryAndSecondaryMarketTO.setInitialDate(listaLogger.get(i).getFilterValue());
				}
			}else if(listaLogger.get(i).getFilterName().equals("p_final_date")){
				if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					primaryAndSecondaryMarketTO.setFinalDate(listaLogger.get(i).getFilterValue());			
				}
			}
		}
		
		String strQueryFormated;
		//verificando el tipo de mercado que se selecciono
		if (primaryAndSecondaryMarketTO.getTypeMarket().equals(PRIMARY_MARKET))
			strQueryFormated = negotiationReportServiceBean.getQueryPrimaryMarket(primaryAndSecondaryMarketTO);
		else
			strQueryFormated = negotiationReportServiceBean.getQuerySecondaryMarket(primaryAndSecondaryMarketTO);
		
		List<Object[]> lstObject = negotiationReportServiceBean.getQueryListByClass(strQueryFormated);
		parametersRequired.put("lstObjects", lstObject);
				
		/**GENERATE FILE EXCEL ***/
		if (primaryAndSecondaryMarketTO.getTypeMarket().equals(PRIMARY_MARKET))
			arrayByteExcel = reportPoiGeneratorServiceBean.writeFileExcelPrimaryMarket(parametersRequired, reportLogger);
		else
			arrayByteExcel = reportPoiGeneratorServiceBean.writeFileExcelSecondaryMarket(parametersRequired, reportLogger);
		
		/**SETTING ARRAY BYTE TO GENERIC REPORT FOR PERSIST**/
		List<byte[]> bytes = new ArrayList<>();
		bytes.add(arrayByteExcel);
		setListByte(bytes);

		return baos;
	}
	
}