package com.pradera.report.generation.executor.settlement.to;

import java.io.Serializable;
import java.util.Date;

public class SettlementOperationTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6509733311059672538L;
	
	
	private Integer settlement_schema;
	private Long participantPk;
	private Integer currency;
	private Date dateInitial;
	private Date dateEnd;
	private Integer mechanism;
	private Long modality;
	private Integer operationPart;
	private String isinCode;
	private Integer unfulfillmentMotive;	
	public Date getSettlementDate() {
		return settlementDate;
	}
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}
	private Date settlementDate;
	
	public Integer getUnfulfillmentMotive() {
		return unfulfillmentMotive;
	}
	public void setUnfulfillmentMotive(Integer unfulfillmentMotive) {
		this.unfulfillmentMotive = unfulfillmentMotive;
	}
	public Integer getMechanism() {
		return mechanism;
	}
	public void setMechanism(Integer mechanism) {
		this.mechanism = mechanism;
	}
	public Long getModality() {
		return modality;
	}
	public void setModality(Long modality) {
		this.modality = modality;
	}
	public Integer getOperationPart() {
		return operationPart;
	}
	public void setOperationPart(Integer operationPart) {
		this.operationPart = operationPart;
	}
	public String getIsinCode() {
		return isinCode;
	}
	public void setIsinCode(String isinCode) {
		this.isinCode = isinCode;
	}
	public Integer getSettlement_schema() {
		return settlement_schema;
	}
	public void setSettlement_schema(Integer settlement_schema) {
		this.settlement_schema = settlement_schema;
	}
	public Long getParticipantPk() {
		return participantPk;
	}
	public void setParticipantPk(Long participantPk) {
		this.participantPk = participantPk;
	}
	public Integer getCurrency() {
		return currency;
	}
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	public Date getDateInitial() {
		return dateInitial;
	}
	public void setDateInitial(Date dateInitial) {
		this.dateInitial = dateInitial;
	}
	public Date getDateEnd() {
		return dateEnd;
	}
	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}

}
