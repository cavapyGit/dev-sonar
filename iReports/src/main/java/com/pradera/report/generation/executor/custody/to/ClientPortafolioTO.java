package com.pradera.report.generation.executor.custody.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ClientPortafolioTO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Integer reportFormat;
	private String reportFormatDesc;
	private String idSecurityCode;
	private Date date;
	private BigDecimal marketRate;
	private BigDecimal marketPrice;
	private Date dateMarketFact;
	private String dateMarketFactString;
	private Long cui;
	private String holderDesc;
	private BigDecimal totalBalance;
	private String securityClass;
	private BigDecimal initialNominalValue;
	private BigDecimal NominalValueOrig;
	private BigDecimal markPriceOrig;
	private BigDecimal totalAmountOrig;
	private Integer issuanceForm;
	private Long holderAccountPk;
	private String idIssuer;
	private String valuatorCode;
	private String custodian;
	private String detached_coupons; //Cupones Desprendidos
	private String coupons_no_detached; //Cupones No Desprendidos
	private Integer showAvailableBalance;
	private Integer idCurrency;
	private Integer portafolioType;
	private Long idParticipant;
	private String participantDesc;
	private String institutionType;
	private boolean isParticipantInvestor;
	private Date registryDate;
	private String dateInformation;
	private String cutDate;
	private String suspendedSecurities;
	
	public String getDateInformation() {
		return dateInformation;
	}
	public void setDateInformation(String dateInformation) {
		this.dateInformation = dateInformation;
	}
	public Date getRegistryDate() {
		return registryDate;
	}
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}
	public String getIdSecurityCode() {
		return idSecurityCode;
	}
	public void setIdSecurityCode(String idSecurityCode) {
		this.idSecurityCode = idSecurityCode;
	}
	public BigDecimal getMarketRate() {
		return marketRate;
	}
	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}
	public BigDecimal getMarketPrice() {
		return marketPrice;
	}
	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}
	public BigDecimal getTotalBalance() {
		return totalBalance;
	}
	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}
	public BigDecimal getTotalAmountOrig() {
		return totalAmountOrig;
	}
	public void setTotalAmountOrig(BigDecimal totalAmountOrig) {
		this.totalAmountOrig = totalAmountOrig;
	}
	public Integer getIssuanceForm() {
		return issuanceForm;
	}
	public void setIssuanceForm(Integer issuanceForm) {
		this.issuanceForm = issuanceForm;
	}
	public Long getCui() {
		return cui;
	}
	public void setCui(Long cui) {
		this.cui = cui;
	}
	public Date getDateMarketFact() {
		return dateMarketFact;
	}
	public void setDateMarketFact(Date dateMarketFact) {
		this.dateMarketFact = dateMarketFact;
	}
	public BigDecimal getInitialNominalValue() {
		return initialNominalValue;
	}
	public void setInitialNominalValue(BigDecimal initialNominalValue) {
		this.initialNominalValue = initialNominalValue;
	}
	public BigDecimal getNominalValueOrig() {
		return NominalValueOrig;
	}
	public void setNominalValueOrig(BigDecimal nominalValueOrig) {
		NominalValueOrig = nominalValueOrig;
	}
	public BigDecimal getMarkPriceOrig() {
		return markPriceOrig;
	}
	public void setMarkPriceOrig(BigDecimal markPriceOrig) {
		this.markPriceOrig = markPriceOrig;
	}
	public String getIdIssuer() {
		return idIssuer;
	}
	public void setIdIssuer(String idIssuer) {
		this.idIssuer = idIssuer;
	}
	public Long getHolderAccountPk() {
		return holderAccountPk;
	}
	public void setHolderAccountPk(Long holderAccountPk) {
		this.holderAccountPk = holderAccountPk;
	}
	public String getHolderDesc() {
		return holderDesc;
	}
	public void setHolderDesc(String holderDesc) {
		this.holderDesc = holderDesc;
	}
	public String getValuatorCode() {
		return valuatorCode;
	}
	public void setValuatorCode(String valuatorCode) {
		this.valuatorCode = valuatorCode;
	}
	public Integer getIdCurrency() {
		return idCurrency;
	}
	public void setIdCurrency(Integer idCurrency) {
		this.idCurrency = idCurrency;
	}
	public Integer getShowAvailableBalance() {
		return showAvailableBalance;
	}
	public Integer getPortafolioType() {
		return portafolioType;
	}
	public void setShowAvailableBalance(Integer showAvailableBalance) {
		this.showAvailableBalance = showAvailableBalance;
	}
	public void setPortafolioType(Integer portafolioType) {
		this.portafolioType = portafolioType;
	}
	public Long getIdParticipant() {
		return idParticipant;
	}
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}
	public String getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(String securityClass) {
		this.securityClass = securityClass;
	}
	public String getParticipantDesc() {
		return participantDesc;
	}
	public void setParticipantDesc(String participantDesc) {
		this.participantDesc = participantDesc;
	}
	public String getDateMarketFactString() {
		return dateMarketFactString;
	}
	public void setDateMarketFactString(String dateMarketFactString) {
		this.dateMarketFactString = dateMarketFactString;
	}
	/**
	 * @return the isParticipantInvestor
	 */
	public boolean getIsParticipantInvestor() {
		return isParticipantInvestor;
	}
	/**
	 * @param isParticipantInvestor the isParticipantInvestor to set
	 */
	public void setIsParticipantInvestor(boolean isParticipantInvestor) {
		this.isParticipantInvestor = isParticipantInvestor;
	}
	/**
	 * @return the institutionType
	 */
	public String getInstitutionType() {
		return institutionType;
	}
	/**
	 * @param institutionType the institutionType to set
	 */
	public void setInstitutionType(String institutionType) {
		this.institutionType = institutionType;
	}
	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	public String getCustodian() {
		return custodian;
	}
	public void setCustodian(String custodian) {
		this.custodian = custodian;
	}
	public String getDetached_coupons() {
		return detached_coupons;
	}
	public void setDetached_coupons(String detached_coupons) {
		this.detached_coupons = detached_coupons;
	}
	public String getCoupons_no_detached() {
		return coupons_no_detached;
	}
	public void setCoupons_no_detached(String coupons_no_detached) {
		this.coupons_no_detached = coupons_no_detached;
	}
	public String getCutDate() {
		return cutDate;
	}
	public void setCutDate(String cutDate) {
		this.cutDate = cutDate;
	}
	public Integer getReportFormat() {
		return reportFormat;
	}
	public void setReportFormat(Integer reportFormat) {
		this.reportFormat = reportFormat;
	}
	public String getReportFormatDesc() {
		return reportFormatDesc;
	}
	public void setReportFormatDesc(String reportFormatDesc) {
		this.reportFormatDesc = reportFormatDesc;
	}
	public String getSuspendedSecurities() {
		return suspendedSecurities;
	}
	public void setSuspendedSecurities(String suspendedSecurities) {
		this.suspendedSecurities = suspendedSecurities;
	}
}
