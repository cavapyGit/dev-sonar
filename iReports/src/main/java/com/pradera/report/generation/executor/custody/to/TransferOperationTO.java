package com.pradera.report.generation.executor.custody.to;

import java.io.Serializable;

public class TransferOperationTO implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public String issuer;
	public Long participant;
	public Integer cuiHolder;
	public Integer holderAccount ;
	public Integer securityClass;
	public String security;
	public Integer currencyOperation;
	public Integer modality;
	public String alternateCode;
	public Integer operation;
	public String strDateInitial;
	public String strDateEnd;
	
	public String getIssuer() {
		return issuer;
	}
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	public Long getParticipant() {
		return participant;
	}
	public void setParticipant(Long participant) {
		this.participant = participant;
	}
	public Integer getCuiHolder() {
		return cuiHolder;
	}
	public void setCuiHolder(Integer cuiHolder) {
		this.cuiHolder = cuiHolder;
	}
	public Integer getHolderAccount() {
		return holderAccount;
	}
	public void setHolderAccount(Integer holderAccount) {
		this.holderAccount = holderAccount;
	}
	public Integer getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}
	public String getSecurity() {
		return security;
	}
	public void setSecurity(String security) {
		this.security = security;
	}
	public Integer getCurrencyOperation() {
		return currencyOperation;
	}
	public void setCurrencyOperation(Integer currencyOperation) {
		this.currencyOperation = currencyOperation;
	}
	public Integer getModality() {
		return modality;
	}
	public void setModality(Integer modality) {
		this.modality = modality;
	}
	public String getAlternateCode() {
		return alternateCode;
	}
	public void setAlternateCode(String alternateCode) {
		this.alternateCode = alternateCode;
	}
	public String getStrDateInitial() {
		return strDateInitial;
	}
	public void setStrDateInitial(String strDateInitial) {
		this.strDateInitial = strDateInitial;
	}
	public String getStrDateEnd() {
		return strDateEnd;
	}
	public void setStrDateEnd(String strDateEnd) {
		this.strDateEnd = strDateEnd;
	}
	/**
	 * @return the operation
	 */
	public Integer getOperation() {
		return operation;
	}
	/**
	 * @param operation the operation to set
	 */
	public void setOperation(Integer operation) {
		this.operation = operation;
	}
	
	
	
	
}
