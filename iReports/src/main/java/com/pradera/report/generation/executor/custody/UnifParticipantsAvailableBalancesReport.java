package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;

import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;
import com.pradera.report.util.view.UtilReportConstants;
@ReportProcess(name = "UnifParticipantsAvailableBalancesReport")
public class UnifParticipantsAvailableBalancesReport extends GenericReport {

	private static final long serialVersionUID = 1L;
	private final String MAP_CURRENCY="map_currency";
    private final String MAP_SECURITIES_STATE="map_securities_state";
    private final String MAP_PARTICIPANT_UNION_STATE="map_participant_union_state";
    private final String MAP_PARTICIPANT_UNIFICATION_MOTIVES="map_participant_unification_motives";
    private final String MAP_PARTICIPANT_ORIG="p_partOrigen";
    private final String MAP_PARTICIPANT_DEST="p_partDestino";
	
	private Map<String, Object> map= new HashMap<String, Object>();
	@EJB
	private ParameterServiceBean parameterService;
	
	@Override
	public Map<String, Object> getCustomJasperParameters(){
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		GenericsFiltersTO unifParticipant= new GenericsFiltersTO();
		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("participant_origin")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					unifParticipant.setParticipant(listaLogger.get(i).getFilterDescription());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("participant_destin")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					unifParticipant.setParticipantDesc(listaLogger.get(i).getFilterDescription());
				}
			}
		}
		
		Map<Integer,String> mapAux;
		ParameterTableTO  parameterFilter = new ParameterTableTO();
		mapAux = new HashMap<Integer, String>();
		parameterFilter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(parameterFilter)) 
			mapAux.put(param.getParameterTablePk(), "("+param.getDescription()+") "+param.getParameterName());
		map.put(MAP_CURRENCY, mapAux);
		
		mapAux= new HashMap<Integer, String>();
		parameterFilter.setMasterTableFk(MasterTableType.SECURITIES_STATE.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(parameterFilter)) 
			mapAux.put(param.getParameterTablePk(), param.getDescription());
		map.put(MAP_SECURITIES_STATE, mapAux);
		
		mapAux = new HashMap<Integer, String>();
		parameterFilter.setMasterTableFk(MasterTableType.PARTICIPANT_UNION_STATE.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(parameterFilter)) 
			mapAux.put(param.getParameterTablePk(), param.getDescription());
		map.put(MAP_PARTICIPANT_UNION_STATE, mapAux);
		
		mapAux = new HashMap<Integer, String>();
		parameterFilter.setMasterTableFk(MasterTableType.PARTICIPANT_UNIFICATION_MOTIVES.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(parameterFilter)) 
			mapAux.put(param.getParameterTablePk(), param.getDescription());
		map.put(MAP_PARTICIPANT_UNIFICATION_MOTIVES, mapAux);
		
		map.put(MAP_PARTICIPANT_ORIG, unifParticipant.getParticipant());
		map.put(MAP_PARTICIPANT_DEST, unifParticipant.getParticipantDesc());
		map.put("logo_path", UtilReportConstants.readLogoReport());
		
		return map;
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		return null;
	}
}
