package com.pradera.report.generation.executor.securities;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "SecuritiesClassListReport")
public class SecuritiesClassListReport extends GenericReport{
private static final long serialVersionUID = 1L;

@Inject  PraderaLogger log;

@EJB
private ParameterServiceBean parameterService;

public SecuritiesClassListReport() {
}

@Override
public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public void addParametersQueryReport() { }

@Override
public Map<String, Object> getCustomJasperParameters() {
	Map<String,Object> parametersRequired = new HashMap<>();
	ParameterTableTO  filter = new ParameterTableTO();
	Map<Integer,String> securities_class_mnemonic = new HashMap<Integer, String>();
	Map<Integer,String> securities_class_description = new HashMap<Integer, String>();
	Map<Integer,String> securities_class_serialled = new HashMap<Integer, String>();
	List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
	try {
		filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
			securities_class_mnemonic.put(param.getParameterTablePk(), param.getText1());
			securities_class_description.put(param.getParameterTablePk(), param.getParameterName());
			securities_class_serialled.put(param.getParameterTablePk(), param.getText2());
		}
	}catch(Exception ex){
		log.error(ex.getMessage());
		ex.printStackTrace();
		throw new RuntimeException();
	}
	
	for(int i=0; i<listaLogger.size(); i++) {
		if(listaLogger.get(i).getFilterName().equals("state_securities")) {
			if(listaLogger.get(i).getFilterValue() != null) {
				parametersRequired.put("state_security", listaLogger.get(i).getFilterValue());
					
			}
		}
		
		if(listaLogger.get(i).getFilterName().equals("economic_activity")) {
			if(listaLogger.get(i).getFilterValue() != null) {
				parametersRequired.put("economic_activity", listaLogger.get(i).getFilterValue());
					
			}
		}
	}
	
	parametersRequired.put("p_securities_class_mnemonic", securities_class_mnemonic);
	parametersRequired.put("p_securities_class_description", securities_class_description);
	parametersRequired.put("p_securities_class_serialled", securities_class_serialled);
	parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());

	// TODO Auto-generated method stub
	return parametersRequired;
	}
}
