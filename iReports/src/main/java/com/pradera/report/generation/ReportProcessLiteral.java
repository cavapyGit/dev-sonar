package com.pradera.report.generation;

import javax.enterprise.util.AnnotationLiteral;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ReportProcessLiteral.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/07/2013
 */
public class ReportProcessLiteral extends AnnotationLiteral<ReportProcess> implements ReportProcess {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2734763763952918109L;
	
	/** The name. */
	private String name;

	/**
	 * Instantiates a new report process literal.
	 */
	public ReportProcessLiteral() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Instantiates a new report process literal.
	 *
	 * @param name the name
	 */
	public ReportProcessLiteral(String name){
		this.name=name;
	}

	/* (non-Javadoc)
	 * @see com.pradera.report.generation.ReportProcess#name()
	 */
	@Override
	public String name() {
		// TODO Auto-generated method stub
		return this.name;
	}

}
