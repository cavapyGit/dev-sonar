package com.pradera.report.generation.executor.negotiation.service;


import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.operation.to.MechanismOperationTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.negotiation.type.InChargeNegotiationType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.NegotiationModalityType;
import com.pradera.model.negotiation.type.SettlementType;
import com.pradera.report.generation.executor.custody.to.TransferOperationTO;
import com.pradera.report.generation.executor.negotiation.to.GenericNegotiationTO;
import com.pradera.report.generation.executor.negotiation.to.PrimaryAndSecondaryMarketGralTO;
import com.pradera.report.generation.executor.negotiation.to.PrimaryAndSecondaryMarketTO;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class AccountReportServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 12/07/2013
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class NegotiationReportServiceBean extends CrudDaoServiceBean {

    /**
     * Default constructor. 
     */
    public NegotiationReportServiceBean() {
        // TODO Auto-generated constructor stub
    }
    
    public List<Object[]> getQueryListByClass(String strQuery) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(strQuery);
		Query query = em.createNativeQuery(sbQuery.toString());
		return query.getResultList();
	}
    
    /**
     * issue 1179: query par ael reporte excel Mercado Primario General
     * @param params
     * @return
     */
    public String getQueryPrimaryMarketGral(PrimaryAndSecondaryMarketGralTO params){
    	
    	StringBuilder sd=new StringBuilder();
    	sd.append(" SELECT                                                                                               ");
    	sd.append(" R.EMISOR,                                                                                            ");
    	sd.append(" R.TIPO_INS,                                                                                          ");
    	sd.append(" R.CLAVE_VALOR,                                                                                       ");
    	sd.append(" R.CUI, FEC_EMISION,                                                                                  ");
    	sd.append(" R.FEC_HR_REGISTRO,                                                                                   ");
    	sd.append(" R.CANTIDAD, FEC_VENC,                                                                                ");
    	sd.append(" R.MON_TOTAL,                                                                                         ");
    	sd.append(" R.TASA_NOMINAL,                                                                                      ");
    	sd.append(" R.MONEDA,                                                                                            ");
    	sd.append(" R.RAZON_SOCIAL,                                                                                      ");
    	sd.append(" R.NIT                                                                                                ");
    	sd.append(" FROM                                                                                                 ");
    	sd.append("   (select                                                                                            ");
    	sd.append("   ISS.MNEMONIC EMISOR,                                                                               ");
    	sd.append("   PT1.TEXT1 TIPO_INS,                                                                                ");
    	sd.append("   SEC.ID_SECURITY_CODE_ONLY CLAVE_VALOR,                                                             ");
    	sd.append("   (SELECT LISTAGG(HAD.ID_HOLDER_FK , ', ' )                                                          ");
    	sd.append("      WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)                                                        ");                                                                     
    	sd.append("      FROM HOLDER_ACCOUNT_DETAIL HAD                                                                  ");                                                                                                                                      
    	sd.append("      WHERE HAD.ID_HOLDER_ACCOUNT_FK = AAO.ID_HOLDER_ACCOUNT_FK ) CUI,                                ");
    	sd.append("                                                                                                      ");
    	sd.append("   TO_CHAR(SEC.ISSUANCE_DATE,'DD/MM/YYYY') FEC_EMISION,                                               ");
    	sd.append("   TO_CHAR(AAO.LAST_MODIFY_DATE,'DD/MM/YYYY hh24:mi:ss') FEC_HR_REGISTRO,                             ");
    	sd.append("   AAO.TOTAL_BALANCE CANTIDAD,                                                                        ");
    	sd.append("   TO_CHAR(SEC.EXPIRATION_DATE,'DD/MM/YYYY') FEC_VENC,                                                ");
    	sd.append("   SEC.INITIAL_NOMINAL_VALUE MON_TOTAL,                                                               ");
    	sd.append("   SEC.INTEREST_RATE TASA_NOMINAL,                                                                    ");
    	sd.append("   PT2.DESCRIPTION MONEDA,                                                                            ");
    	sd.append("   (SELECT LISTAGG((select h.full_name                                                                ");
    	sd.append("                    from holder h                                                                     ");
    	sd.append("                   where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK),', ' )                                      ");
    	sd.append("      WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)                                                        ");
    	sd.append("      FROM HOLDER_ACCOUNT_DETAIL HAD                                                                  ");
    	sd.append("      WHERE HAD.ID_HOLDER_ACCOUNT_FK = AAO.ID_HOLDER_ACCOUNT_FK ) RAZON_SOCIAL,                       ");
    	sd.append("   (SELECT LISTAGG((select h.DOCUMENT_NUMBER                                                          ");
    	sd.append("                    from holder h                                                                     ");
    	sd.append("                   where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK),', ' )                                      ");
    	sd.append("      WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)                                                        ");
    	sd.append("      FROM HOLDER_ACCOUNT_DETAIL HAD                                                                  ");
    	sd.append("      WHERE HAD.ID_HOLDER_ACCOUNT_FK = AAO.ID_HOLDER_ACCOUNT_FK ) NIT,                                ");
    	sd.append("                                                                                                      ");
    	sd.append("   ISS.ID_ISSUER_PK ID_EMISOR,                                                                        ");
    	sd.append("   PT1.PARAMETER_TABLE_PK ID_CLASE_VALOR,                                                             ");
    	sd.append("   SEC.ID_SECURITY_CODE_PK ID_VALOR                                                                   ");
    	sd.append("                                                                                                      ");
    	sd.append("   from ACCOUNT_ANNOTATION_OPERATION AAO                                                              ");
    	sd.append("   INNER JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK = AAO.ID_HOLDER_ACCOUNT_FK                 ");
    	sd.append("   INNER JOIN SECURITY SEC ON SEC.ID_SECURITY_CODE_PK = AAO.ID_SECURITY_CODE_FK                       ");
    	sd.append("   INNER JOIN ISSUER ISS ON SEC.ID_ISSUER_FK = ISS.ID_ISSUER_PK                                       ");
    	sd.append("   INNER JOIN PARAMETER_TABLE PT1 ON SEC.SECURITY_CLASS = PT1.PARAMETER_TABLE_PK                      ");
    	sd.append("   INNER JOIN PARAMETER_TABLE PT2 ON SEC.CURRENCY = PT2.PARAMETER_TABLE_PK                            ");
    	sd.append("   WHERE 1 = 1                                                                                        ");
    	sd.append("   AND TRUNC(AAO.LAST_MODIFY_DATE) BETWEEN  to_date('"+params.getInitialDate()+"','DD/MM/YYYY') AND to_date('"+params.getFinalDate()+"','DD/MM/YYYY') ");
    	sd.append("   AND HA.ACCOUNT_TYPE = 107                                                                          ");
    	sd.append("   AND AAO.STATE_ANNOTATION = 858                                                                     ");
    	sd.append("                                                                                                      ");
    	sd.append("   UNION ALL                                                                                          ");
    	sd.append("                                                                                                      ");
    	sd.append("   SELECT                                                                                             ");
    	sd.append("   ISS.MNEMONIC EMISOR,                                                                               ");
    	sd.append("   PT1.TEXT1 TIPO_INS,                                                                                ");
    	sd.append("   SEC.ID_SECURITY_CODE_ONLY CLAVE_VALOR,                                                             ");
    	sd.append("                                                                                                      ");
    	sd.append("   (SELECT LISTAGG(ID_HOLDER_PK,', ' ||chr(13))  WITHIN GROUP (ORDER BY ID_HOLDER_PK) FROM(           ");
    	sd.append("       SELECT distinct H_SUB.ID_HOLDER_PK                                                             ");
    	sd.append("       from HOLDER_ACCOUNT_OPERATION HAO_SUB,  HOLDER_ACCOUNT_DETAIL HAD_SUB, HOLDER H_SUB            ");
		sd.append("       , MECHANISM_OPERATION MO                                                                       ");    	
    	sd.append("       where 1 = 1                                                                                    ");
    	sd.append("       AND HAD_SUB.ID_HOLDER_ACCOUNT_FK = HAO_SUB.ID_HOLDER_ACCOUNT_FK                                ");
    	sd.append("       AND H_SUB.ID_HOLDER_PK = HAD_SUB.ID_HOLDER_FK                                                  ");
    	sd.append("       AND HAO_SUB.HOLDER_ACCOUNT_STATE = 625                                                         ");
    	sd.append("       AND HAO_SUB.ROLE = 1                                                                           ");
    	sd.append("       AND HAO_SUB.OPERATION_PART = 1                                                                 ");
    	sd.append("       AND HAO_SUB.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK)) CUI,                    ");
    	sd.append("                                                                                                      ");
    	sd.append("   TO_CHAR(SEC.ISSUANCE_DATE,'DD/MM/YYYY') FEC_EMISION,                                               ");
    	sd.append("   TO_CHAR(SP.LAST_MODIFY_DATE,'DD/MM/YYYY hh24:mi:ss') FEC_HR_REGISTRO,                              ");
    	sd.append("   MO.STOCK_QUANTITY CANTIDAD,                                                                        ");
    	sd.append("   TO_CHAR(SEC.EXPIRATION_DATE,'DD/MM/YYYY') FEC_VENC,                                                ");
    	sd.append("   SEC.INITIAL_NOMINAL_VALUE MON_TOTAL,                                                               ");
    	sd.append("   SEC.INTEREST_RATE TASA_NOMINAL,                                                                    ");
    	sd.append("   PT2.DESCRIPTION MONEDA,                                                                            ");
    	sd.append("                                                                                                      ");
    	sd.append("   (SELECT LISTAGG(full_name,', ' ||chr(13))  WITHIN GROUP (ORDER BY full_name) FROM(                 ");
    	sd.append("       SELECT distinct H_SUB.full_name                                                                ");
    	sd.append("       from HOLDER_ACCOUNT_OPERATION HAO_SUB,  HOLDER_ACCOUNT_DETAIL HAD_SUB, HOLDER H_SUB            ");
		sd.append("       , MECHANISM_OPERATION MO                                                                        ");    	
    	sd.append("       where 1 = 1                                                                                    ");
    	sd.append("       AND HAD_SUB.ID_HOLDER_ACCOUNT_FK = HAO_SUB.ID_HOLDER_ACCOUNT_FK                                ");
    	sd.append("       AND H_SUB.ID_HOLDER_PK = HAD_SUB.ID_HOLDER_FK                                                  ");
    	sd.append("       AND HAO_SUB.HOLDER_ACCOUNT_STATE = 625                                                         ");
    	sd.append("       AND HAO_SUB.ROLE = 1                                                                           ");
    	sd.append("       AND HAO_SUB.OPERATION_PART = 1                                                                 ");
    	sd.append("       AND HAO_SUB.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK)) RAZON_SOCIAL,           ");
    	sd.append("                                                                                                      ");
    	sd.append("   (SELECT LISTAGG(DOCUMENT_NUMBER,', ' ||chr(13))  WITHIN GROUP (ORDER BY DOCUMENT_NUMBER) FROM(     ");
    	sd.append("       SELECT distinct H_SUB.DOCUMENT_NUMBER                                                          ");
    	sd.append("       from HOLDER_ACCOUNT_OPERATION HAO_SUB,  HOLDER_ACCOUNT_DETAIL HAD_SUB, HOLDER H_SUB            ");
		sd.append("       , MECHANISM_OPERATION MO                                                                       ");    	    	
    	sd.append("       where 1 = 1                                                                                    ");
    	sd.append("       AND HAD_SUB.ID_HOLDER_ACCOUNT_FK = HAO_SUB.ID_HOLDER_ACCOUNT_FK                                ");
    	sd.append("       AND H_SUB.ID_HOLDER_PK = HAD_SUB.ID_HOLDER_FK                                                  ");
    	sd.append("       AND HAO_SUB.HOLDER_ACCOUNT_STATE = 625                                                         ");
    	sd.append("       AND HAO_SUB.ROLE = 1                                                                           ");
    	sd.append("       AND HAO_SUB.OPERATION_PART = 1                                                                 ");
    	sd.append("       AND HAO_SUB.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK)) NIT,                    ");
    	sd.append("                                                                                                      ");
    	sd.append("   ISS.ID_ISSUER_PK ID_EMISOR,                                                                        ");
    	sd.append("   PT1.PARAMETER_TABLE_PK ID_CLASE_VALOR,                                                             ");
    	sd.append("   SEC.ID_SECURITY_CODE_PK ID_VALOR                                                                   ");
    	sd.append("                                                                                                      ");
    	sd.append("   FROM MECHANISM_OPERATION MO                                                                        ");
    	sd.append("   INNER JOIN SECURITY SEC ON SEC.ID_SECURITY_CODE_PK = MO.ID_SECURITY_CODE_FK                        ");
    	sd.append("   INNER JOIN ISSUER ISS ON SEC.ID_ISSUER_FK = ISS.ID_ISSUER_PK                                       ");
    	sd.append("   INNER JOIN PARAMETER_TABLE PT1 ON SEC.SECURITY_CLASS = PT1.PARAMETER_TABLE_PK                      ");
    	sd.append("   INNER JOIN PARAMETER_TABLE PT2 ON SEC.CURRENCY = PT2.PARAMETER_TABLE_PK                            ");
    	sd.append("   INNER JOIN SETTLEMENT_OPERATION SO ON MO.ID_MECHANISM_OPERATION_PK = SO.ID_MECHANISM_OPERATION_FK  ");
    	sd.append("   INNER JOIN OPERATION_SETTLEMENT OS ON SO.ID_SETTLEMENT_OPERATION_PK = OS.ID_SETTLEMENT_OPERATION_FK");
    	sd.append("   INNER JOIN SETTLEMENT_PROCESS SP ON SP.ID_SETTLEMENT_PROCESS_PK = OS.ID_SETTLMENT_PROCESS_FK       ");
    	sd.append("   WHERE 1 = 1                                                                                        ");
    	sd.append("   AND MO.ID_NEGOTIATION_MODALITY_FK IN (12,13)                                                       ");
    	
    	sd.append("   AND MO.OPERATION_DATE BETWEEN  to_date('"+params.getInitialDate()+"','DD/MM/YYYY') AND to_date('"+params.getFinalDate()+"','DD/MM/YYYY') ");
    	sd.append("   AND SP.PROCESS_STATE = 1284                                                                        ");
    	sd.append("   AND OS.OPERATION_STATE = 1288                                                                      ");
    	sd.append("                                                                                                      ");
    	sd.append("   AND 107 IN ( SELECT HA_SUB.ACCOUNT_TYPE                                                            ");
    	sd.append("       from HOLDER_ACCOUNT_OPERATION HAO_SUB, HOLDER_ACCOUNT HA_SUB                                   ");
    	sd.append("       where 1 = 1                                                                                    ");
    	sd.append("       AND HA_SUB.ID_HOLDER_ACCOUNT_PK = HAO_SUB.ID_HOLDER_ACCOUNT_FK                                 ");
    	sd.append("       AND HAO_SUB.HOLDER_ACCOUNT_STATE = 625                                                         ");
    	sd.append("       AND ROLE = 1                                                                                   ");
    	sd.append("       AND HAO_SUB.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK)                          ");
    	sd.append("                                                                                                      ");
    	sd.append(" ) R                                                                                    ");
    	sd.append(" WHERE 0=0                                                                                            ");
    	if(Validations.validateIsNotNullAndNotEmpty(params.getIdIssuer())){
    		sd.append(" AND R.ID_EMISOR = :idEmisor ");
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(params.getSecurityClass())){
    		sd.append(" AND R.ID_CLASE_VALOR = :securityClass ");
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(params.getIdSecurity())){
    		sd.append(" AND R.ID_VALOR = :idSecurityPk ");
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(params.getCui())){
    		sd.append(" AND R.CUI = :idHolderPk ");
    	}
    	sd.append("   ORDER BY 4,3 ");
    	
    	//reemplazando los parametros en la query
		String query=sd.toString();
		if(Validations.validateIsNotNullAndNotEmpty(params.getIdIssuer())){
    		query=query.replace(":idEmisor", "'"+params.getIdIssuer()+"'");
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(params.getSecurityClass())){
    		query=query.replace(":securityClass", params.getSecurityClass());
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(params.getIdSecurity())){
    		query=query.replace(":idSecurityPk", "'"+params.getIdSecurity()+"'");
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(params.getCui())){
    		query=query.replace(":idHolderPk", params.getCui());
    	}
		
    	return query;
    }
    
    /**
	 * query para el reporte de mercado primario ISSUE 1005
	 * @param primaryAndSecondaryMarketTO : POJO con los parametros de la consulta
	 * @return query del mercado primario
	 */
	public String getQueryPrimaryMarket(PrimaryAndSecondaryMarketTO primaryAndSecondaryMarketTO) {
		StringBuilder sd = new StringBuilder();

		sd.append(" select ");
		sd.append(" FEC_OPERACION FEC_OPERACION, ");
		sd.append(" AGENCIA_COMP AGENCIA_COMP, ");
		sd.append(" CUI_COMP CUI_COMP, ");
		sd.append(" NOMBRE_COMP NOMBRE_COMP, ");
		sd.append(" CLAVE_VALOR CLAVE_VALOR, ");
		sd.append(" EMISOR EMISOR, ");
		sd.append(" EMISOR_RAZ EMISOR_RAZ, ");
		sd.append(" FEC_EMISION_VALOR FEC_EMISION_VALOR, ");
		sd.append(" PLAZO_VALOR PLAZO_VALOR, ");
		sd.append(" FEC_VENC FEC_VENC, ");
		sd.append(" MONEDA MONEDA, ");
		sd.append(" VAL_NOMINAL VAL_NOMINAL, ");
		sd.append(" TASA_INT_NOMINAL TASA_INT_NOMINAL, ");
		sd.append(" TASA_NEGOCIACION TASA_NEGOCIACION, ");
		sd.append(" PREC_NEGOC PREC_NEGOC, ");
		sd.append(" PERI_PAGO_INTE PERI_PAGO_INTE, ");
		sd.append(" PERI_PAGO_AMORT PERI_PAGO_AMORT, ");
		sd.append(" INT_FEC_CUPON_1 INT_FEC_CUPON_1, ");
		sd.append(" AMO_FEC_CUPON_1 AMO_FEC_CUPON_1, ");
		sd.append(" CANTIDAD CANTIDAD, ");
		sd.append(" CP CP  ");
		sd.append(" from ");
		sd.append(" ( ");
		sd.append(" SELECT ");
		sd.append("  ");
		sd.append(" TO_CHAR(AAO.EXPEDITION_DATE,'DD/MM/YYYY') FEC_OPERACION, ");
		sd.append(" (select MNEMONIC from PARTICIPANT where ID_PARTICIPANT_PK =  ");
		sd.append(" (select ID_PARTICIPANT_FK from HOLDER_ACCOUNT where ID_HOLDER_ACCOUNT_PK = AAO.ID_HOLDER_ACCOUNT_FK)) AGENCIA_COMP, ");
		sd.append("  ");
		sd.append(" (SELECT LISTAGG(HAD.ID_HOLDER_FK , ', ' ||chr(13) )  ");
		sd.append("    WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)              ");
		sd.append("    FROM HOLDER_ACCOUNT_DETAIL HAD                            ");
		sd.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK = AAO.ID_HOLDER_ACCOUNT_FK ) CUI_COMP,  ");
		sd.append(" (SELECT LISTAGG((select h.full_name  ");
		sd.append("                  from holder h  ");
		sd.append("                 where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK),', ' ||chr(13))  ");
		sd.append("    WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)   ");
		sd.append("    FROM HOLDER_ACCOUNT_DETAIL HAD             ");
		sd.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK = AAO.ID_HOLDER_ACCOUNT_FK ) NOMBRE_COMP, ");
		sd.append("  ");
		sd.append(" SEC.ID_SECURITY_CODE_PK CLAVE_VALOR, ");
		sd.append(" ISS.MNEMONIC EMISOR, ");
		sd.append(" ISS.BUSINESS_NAME EMISOR_RAZ, ");
		sd.append(" TO_CHAR(AAO.LAST_MODIFY_DATE,'DD/MM/YYYY') FEC_EMISION_VALOR, ");
		sd.append(" SEC.SECURITY_DAYS_TERM PLAZO_VALOR, ");
		sd.append(" TO_CHAR(SEC.EXPIRATION_DATE,'DD/MM/YYYY') FEC_VENC, ");
		sd.append(" PT2.DESCRIPTION MONEDA, ");
		sd.append(" SEC.INITIAL_NOMINAL_VALUE VAL_NOMINAL,  ");
		sd.append(" SEC.INTEREST_RATE TASA_INT_NOMINAL, ");
		sd.append(" SEC.INTEREST_RATE TASA_NEGOCIACION, ");
		sd.append(" SEC.INITIAL_NOMINAL_VALUE PREC_NEGOC,  ");
		sd.append(" PIC.PAYMENT_DAYS PERI_PAGO_INTE, ");
		sd.append(" CASE WHEN SEC.SECURITY_CLASS = 420 THEN SEC.SECURITY_DAYS_TERM");
		sd.append(" ELSE ");
		sd.append(" 	CASE WHEN APS.PERIODICITY_DAYS IS NULL THEN (CASE WHEN APS.PERIODICITY = 540  THEN 360  ");
		sd.append("                                                   WHEN APS.PERIODICITY = 147  THEN 30 ");
		sd.append("                                                   WHEN APS.PERIODICITY = 1747 THEN 30 END) ");
		sd.append("      ELSE APS.PERIODICITY_DAYS  ");
		sd.append("      END");
		sd.append(" END PERI_PAGO_AMORT, ");
		sd.append(" PIC.EXPERITATION_DATE INT_FEC_CUPON_1, ");
		sd.append(" PAC.EXPRITATION_DATE AMO_FEC_CUPON_1, ");
		sd.append(" AAO.TOTAL_BALANCE CANTIDAD , ");
		sd.append(" 'NO' CP ");
		sd.append(" from ACCOUNT_ANNOTATION_OPERATION AAO ");
		sd.append(" INNER JOIN SECURITY SEC ON SEC.ID_SECURITY_CODE_PK = AAO.ID_SECURITY_CODE_FK ");
		sd.append(" INNER JOIN ISSUER ISS ON SEC.ID_ISSUER_FK = ISS.ID_ISSUER_PK ");
		sd.append("  ");
		sd.append(" INNER JOIN PARAMETER_TABLE PT2 ON SEC.CURRENCY = PT2.PARAMETER_TABLE_PK ");
		sd.append(" LEFT JOIN INTEREST_PAYMENT_SCHEDULE IPS ON SEC.ID_SECURITY_CODE_PK = IPS.ID_SECURITY_CODE_FK ");
		sd.append(" LEFT JOIN PROGRAM_INTEREST_COUPON PIC ON IPS.ID_INT_PAYMENT_SCHEDULE_PK = PIC.ID_INT_PAYMENT_SCHEDULE_FK AND PIC.COUPON_NUMBER = 1 ");
		sd.append(" LEFT JOIN AMORTIZATION_PAYMENT_SCHEDULE APS ON SEC.ID_SECURITY_CODE_PK = APS.ID_SECURITY_CODE_FK ");
		sd.append(" LEFT JOIN PROGRAM_AMORTIZATION_COUPON PAC ON APS.ID_AMO_PAYMENT_SCHEDULE_PK = PAC.ID_AMO_PAYMENT_SCHEDULE_FK AND PAC.COUPON_NUMBER = 1 ");
		sd.append(" WHERE 1 = 1 ");
		sd.append("  ");
		// parametros para rango de fechas
		sd.append(" 	AND TRUNC(AAO.EXPEDITION_DATE) BETWEEN to_date('"+primaryAndSecondaryMarketTO.getInitialDate()+"','DD/MM/YYYY') AND to_date('"+primaryAndSecondaryMarketTO.getFinalDate()+"','DD/MM/YYYY') ");
		sd.append("  ");
		sd.append(" AND ((451) IN (SELECT HAD2.ID_HOLDER_FK FROM HOLDER_ACCOUNT_DETAIL HAD2 where HAD2.ID_HOLDER_ACCOUNT_FK = AAO.ID_HOLDER_ACCOUNT_FK ) ");
		sd.append("   OR (452) IN (SELECT HAD2.ID_HOLDER_FK FROM HOLDER_ACCOUNT_DETAIL HAD2 where HAD2.ID_HOLDER_ACCOUNT_FK = AAO.ID_HOLDER_ACCOUNT_FK )) ");
		sd.append("  ");
		sd.append(" UNION ALL ");
		sd.append("  ");
		sd.append(" select  ");
		sd.append(" TO_CHAR(MO.OPERATION_DATE,'DD/MM/YYYY') FEC_OPERACION, ");
		sd.append(" (select LISTAGG(MNEMONIC, ', ' || chr(13)) WITHIN GROUP (ORDER BY MNEMONIC) ");
		sd.append(" from PARTICIPANT where ID_PARTICIPANT_PK IN ");
		sd.append(" (select ID_PARTICIPANT_FK from HOLDER_ACCOUNT where ID_HOLDER_ACCOUNT_PK IN (SELECT HAO.ID_HOLDER_ACCOUNT_FK ");
		sd.append("                                       FROM HOLDER_ACCOUNT_OPERATION HAO  ");
		sd.append("                                       WHERE ROLE = 1  ");
		sd.append("                                       AND HAO.HOLDER_ACCOUNT_STATE = 625 ");
		sd.append("                                       AND HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK))) AGENCIA_COMP, ");
		sd.append(" (SELECT LISTAGG(HAD.ID_HOLDER_FK , ', ' ||chr(13) )  ");
		sd.append("    WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)              ");
		sd.append("    FROM HOLDER_ACCOUNT_DETAIL HAD                            ");
		sd.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK IN ((SELECT HAO.ID_HOLDER_ACCOUNT_FK ");
		sd.append("                                       FROM HOLDER_ACCOUNT_OPERATION HAO  ");
		sd.append("                                       WHERE ROLE = 1  ");
		sd.append("                                       AND HAO.HOLDER_ACCOUNT_STATE = 625 ");
		sd.append("                                       AND HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK)) ) CUI_COMP, ");
		sd.append(" (SELECT LISTAGG((select h.full_name  ");
		sd.append("                  from holder h  ");
		sd.append("                 where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK),', ' ||chr(13))  ");
		sd.append("    WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)   ");
		sd.append("    FROM HOLDER_ACCOUNT_DETAIL HAD             ");
		sd.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK IN (SELECT HAO.ID_HOLDER_ACCOUNT_FK ");
		sd.append("                                       FROM HOLDER_ACCOUNT_OPERATION HAO  ");
		sd.append("                                       WHERE ROLE = 1  ");
		sd.append("                                       AND HAO.HOLDER_ACCOUNT_STATE = 625 ");
		sd.append("                                       AND HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK) ) NOMBRE_COMP, ");
		sd.append("  ");
		sd.append(" SEC.ID_SECURITY_CODE_PK CLAVE_VALOR, ");
		sd.append(" ISS.MNEMONIC EMISOR, ");
		sd.append(" ISS.BUSINESS_NAME EMISOR_RAZ, ");
		sd.append(" TO_CHAR(SEC.ISSUANCE_DATE,'DD/MM/YYYY') FEC_EMISION_VALOR, ");
		sd.append(" SEC.SECURITY_DAYS_TERM PLAZO_VALOR, ");
		sd.append(" TO_CHAR(SEC.EXPIRATION_DATE,'DD/MM/YYYY') FEC_VENC, ");
		sd.append(" PT2.DESCRIPTION MONEDA, ");
		sd.append(" SEC.INITIAL_NOMINAL_VALUE VAL_NOMINAL,  ");
		sd.append(" SEC.INTEREST_RATE TASA_INT_NOMINAL, ");
		sd.append(" MO.AMOUNT_RATE TASA_NEGOCIACION, ");
		sd.append(" MO.CASH_PRICE PREC_NEGOC,  ");
		sd.append(" PIC.PAYMENT_DAYS PERI_PAGO_INTE, ");
		sd.append(" CASE WHEN SEC.SECURITY_CLASS = 420 THEN SEC.SECURITY_DAYS_TERM");
		sd.append(" ELSE ");
		sd.append(" 	CASE WHEN APS.PERIODICITY_DAYS IS NULL THEN (CASE WHEN APS.PERIODICITY = 540  THEN 360  ");
		sd.append("                                                   WHEN APS.PERIODICITY = 147  THEN 30 ");
		sd.append("                                                   WHEN APS.PERIODICITY = 1747 THEN 30 END) ");
		sd.append("      ELSE APS.PERIODICITY_DAYS  ");
		sd.append("      END ");
		sd.append(" END PERI_PAGO_AMORT, ");
		sd.append(" PIC.EXPERITATION_DATE INT_FEC_CUPON_1, ");
		sd.append(" PAC.EXPRITATION_DATE AMO_FEC_CUPON_1, ");
		sd.append(" MO.STOCK_QUANTITY CANTIDAD , ");
		sd.append(" 'SI' CP ");
		sd.append(" from MECHANISM_OPERATION MO  ");
		sd.append(" INNER JOIN SECURITY SEC ON SEC.ID_SECURITY_CODE_PK = MO.ID_SECURITY_CODE_FK ");
		sd.append(" INNER JOIN ISSUER ISS ON SEC.ID_ISSUER_FK = ISS.ID_ISSUER_PK ");
		sd.append("  ");
		sd.append(" INNER JOIN PARAMETER_TABLE PT2 ON SEC.CURRENCY = PT2.PARAMETER_TABLE_PK ");
		sd.append(" LEFT JOIN INTEREST_PAYMENT_SCHEDULE IPS ON SEC.ID_SECURITY_CODE_PK = IPS.ID_SECURITY_CODE_FK ");
		sd.append(
				" LEFT JOIN PROGRAM_INTEREST_COUPON PIC ON IPS.ID_INT_PAYMENT_SCHEDULE_PK = PIC.ID_INT_PAYMENT_SCHEDULE_FK AND PIC.COUPON_NUMBER = 1 ");
		sd.append(" LEFT JOIN AMORTIZATION_PAYMENT_SCHEDULE APS ON SEC.ID_SECURITY_CODE_PK = APS.ID_SECURITY_CODE_FK ");
		sd.append(
				" LEFT JOIN PROGRAM_AMORTIZATION_COUPON PAC ON APS.ID_AMO_PAYMENT_SCHEDULE_PK = PAC.ID_AMO_PAYMENT_SCHEDULE_FK AND PAC.COUPON_NUMBER = 1 ");
		sd.append(" WHERE 1 = 1 ");
		sd.append(" AND MO.ID_NEGOTIATION_MODALITY_FK in (12,13) ");
		sd.append("  ");
		sd.append("  ");
		//parametros para rango de fechas
		sd.append(" 	AND MO.OPERATION_DATE  BETWEEN to_date('"+primaryAndSecondaryMarketTO.getInitialDate()+"','DD/MM/YYYY') AND to_date('"+primaryAndSecondaryMarketTO.getFinalDate()+"','DD/MM/YYYY') ");
		sd.append("  ");
		sd.append(
				" AND ((451) IN (SELECT HAD2.ID_HOLDER_FK FROM HOLDER_ACCOUNT_DETAIL HAD2 where HAD2.ID_HOLDER_ACCOUNT_FK IN (SELECT HAO.ID_HOLDER_ACCOUNT_FK ");
		sd.append("                                       FROM HOLDER_ACCOUNT_OPERATION HAO  ");
		sd.append("                                       WHERE ROLE = 1  ");
		sd.append("                                       AND HAO.HOLDER_ACCOUNT_STATE = 625 ");
		sd.append("                                       AND HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK) ) ");
		sd.append(
				"   OR (452) IN (SELECT HAD2.ID_HOLDER_FK FROM HOLDER_ACCOUNT_DETAIL HAD2 where HAD2.ID_HOLDER_ACCOUNT_FK IN (SELECT HAO.ID_HOLDER_ACCOUNT_FK ");
		sd.append("                                       FROM HOLDER_ACCOUNT_OPERATION HAO  ");
		sd.append("                                       WHERE ROLE = 1  ");
		sd.append("                                       AND HAO.HOLDER_ACCOUNT_STATE = 625 ");
		sd.append("                                       AND HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK) )) ");
		sd.append("    ");
		sd.append("    ");
		sd.append("  ");
		sd.append(" ORDER BY CP, 1, 5 ");
		sd.append(" ) x ");
		sd.append(" where 0=0 ");
		
		//paramentros de filtro en el reporte
		if(Validations.validateIsNotNullAndNotEmpty(primaryAndSecondaryMarketTO.getMnemonicParticipant())){
			sd.append(" and (x.AGENCIA_COMP = ':p_participant' ) ");
    	}
		if(Validations.validateIsNotNullAndNotEmpty(primaryAndSecondaryMarketTO.getIdHolderSelected())){
			sd.append(" and (':p_cui' in (select trim(regexp_substr(replace(x.CUI_COMP,chr(13),''),'[^,]+', 1, level)) from dual ");
			sd.append("connect by regexp_substr(replace(x.CUI_COMP,chr(13),''), '[^,]+', 1, level) is not null))");
    	}
		
		//plezando los parametros en la query
		String query=sd.toString();
		if(Validations.validateIsNotNullAndNotEmpty(primaryAndSecondaryMarketTO.getMnemonicParticipant())){
			query=query.replace(":p_participant", primaryAndSecondaryMarketTO.getMnemonicParticipant());
    	}
		if(Validations.validateIsNotNullAndNotEmpty(primaryAndSecondaryMarketTO.getIdHolderSelected())){
			query=query.replace(":p_cui", primaryAndSecondaryMarketTO.getIdHolderSelected().toString());
    	}
		
		return query;
	}
	
	/**
	 * issue 1179: query para reporte secundario general
	 * @param params
	 * @return
	 */
	public String getQuerySecondayMarketGral(PrimaryAndSecondaryMarketGralTO params){
		StringBuilder sd=new StringBuilder();
		sd.append(" SELECT                                                                                                   ");
		sd.append(" R.ID_MECHANISM_OPERATION_PK,                                                                             ");
		sd.append(" R.NRO_OPERACION,                                                                                         ");
		sd.append(" to_char(R.FECHA_OPS,'DD/MM/YYYY') FECHA_OPS,                                                             ");
		sd.append(" R.PAR_VENTA,                                                                                             ");
		sd.append(" R.MODALIDAD_VENTA,                                                                                       ");
		sd.append(" R.CUI_VENTA,                                                                                             ");
		sd.append(" R.NOMBRE_VENTA,                                                                                          ");
		sd.append(" R.CARNET_VENTA,                                                                                          ");
		sd.append(" R.PAR_COMPRA,                                                                                            ");
		sd.append(" R.MODALIDAD_COMPRA,                                                                                      ");
		sd.append(" R.CUI_COMPRA,                                                                                            ");
		sd.append(" R.NOMBRE_COMPRA,                                                                                         ");
		sd.append(" R.CARNET_COMPRA,                                                                                         ");
		sd.append(" R.MODALIDAD_NEG,                                                                                         ");
		sd.append(" R.MONEDA,                                                                                                ");
		sd.append(" R.CANT_TOTAL_VAL,                                                                                        ");
		sd.append(" R.CLAVE_VALOR,                                                                                           ");
		sd.append(" R.VAL_NOMINAL,                                                                                           ");
		sd.append(" R.PRECIO,                                                                                                ");
		sd.append(" R.TASA,                                                                                                  ");
		sd.append(" R.PAPELETA_SEQ,                                                                                          ");
		sd.append(" R.PRECIO_LIQUIDACION,                                                                                    ");
		
		sd.append(" CASE WHEN R.ID_NEGOTIATION_MODALITY_PK in (3,4) THEN to_char(R.FECHA_OPERA_REPORTO,'DD/MM/YYYY')             ");
		sd.append("         ELSE NULL                                                                                            ");
		sd.append("     END FECHA_OPERA_REPORTO,                                                                                 ");
		
		sd.append("  CASE WHEN R.OPERATION_PART = 1                                                                              ");
		sd.append(" 			  AND R.ID_NEGOTIATION_MODALITY_PK in (3,4)                                                      ");
		//sd.append("				THEN (select (LISTAGG(NVL(TO_CHAR(SO_SUB.REAL_SETTLEMENT_DATE,'DD/MM/YYYY'),TO_CHAR(SO_SUB.SETTLEMENT_DATE,'DD/MM/YYYY')) , ', ' ||chr(13))  ");
		//sd.append("                         WITHIN  GROUP (ORDER BY SO_SUB.REAL_SETTLEMENT_DATE ))                              ");
		
		sd.append("				THEN (select distinct TO_CHAR(MO_SUB.term_settlement_date,'DD/MM/YYYY')        ");
		sd.append(" 				 from MECHANISM_OPERATION MO_SUB                                                            ");
		sd.append(" 				 inner join SETTLEMENT_OPERATION SO_SUB                                                     ");
		sd.append("						   ON SO_SUB.ID_MECHANISM_OPERATION_FK = R.ID_MECHANISM_OPERATION_PK                    ");
		sd.append(" 				 where 1 = 1                                                                                ");
		sd.append(" 				 and SO_SUB.OPERATION_PART = 2                                                              ");
		sd.append(" 				 and MO_SUB.ID_MECHANISM_OPERATION_PK = R.ID_MECHANISM_OPERATION_PK)                        ");
		sd.append("          WHEN R.OPERATION_PART = 1 AND R.ID_NEGOTIATION_MODALITY_PK not in (3,4) THEN NULL                  ");
		sd.append("          WHEN R.OPERATION_PART = 2 THEN TO_CHAR(R.REAL_SETTLEMENT_DATE,'DD/MM/YYYY')                        ");
		sd.append("     END FECHA_LIQ_PLAZO,                                                                                    ");
		
		//sd.append("     R.TERM_SETTLEMENT_DATE FECHA_LIQ_PLAZO,                                                                 "); //issue 1323
		sd.append("                                                                                                             ");
		
//		sd.append(" 	CASE WHEN R.OPERATION_PART = 2                                                                               ");
//		sd.append("           AND R.IND_EXTENDED = 1  THEN ROUND((SELECT DISTINCT VPR_SUB.MARKET_PRICE                               ");
//		sd.append("                                                FROM VALUATOR_PRICE_REPORT VPR_SUB                                ");
//		sd.append("                                                WHERE VPR_SUB.ID_SECURITY_CODE_FK = R.CLAVE_VALOR                 ");
//		sd.append("                                                AND TRUNC(VPR_SUB.PROCESS_DATE) = TRUNC(R.TERM_SETTLEMENT_DATE)   ");
//		sd.append("                                                AND VPR_SUB.IND_ASFI = 1                                          ");
//		sd.append("                                                AND R.OPERATION_PART  = VPR_SUB.operation_part),2)                ");
//		sd.append("      ELSE  ROUND((SELECT DISTINCT VPR_SUB.MARKET_PRICE                                                           ");
//		sd.append("              FROM VALUATOR_PRICE_REPORT VPR_SUB                                                                  ");
//		sd.append("              WHERE VPR_SUB.ID_SECURITY_CODE_FK = R.CLAVE_VALOR                                                   ");
//		sd.append("              AND TRUNC(VPR_SUB.PROCESS_DATE) = TRUNC(R.FECHA_OPS)                                                ");
//		sd.append("              AND VPR_SUB.IND_ASFI = 1                                                                            ");
//		sd.append("              AND R.OPERATION_PART  = VPR_SUB.operation_part),2)                                                  ");
//		sd.append("     END  PRECIO_CURVA_ASFI                                                                                       ");
		
		sd.append("     ROUND((SELECT DISTINCT VPR_SUB.MARKET_PRICE                                                          ");
		sd.append("              FROM VALUATOR_PRICE_REPORT VPR_SUB                                                          ");
		sd.append("              WHERE VPR_SUB.ID_SECURITY_CODE_FK = R.CLAVE_VALOR                                           ");
		sd.append("              AND TRUNC(VPR_SUB.PROCESS_DATE) = TRUNC(R.FECHA_OPS)         								 ");
		sd.append("              AND VPR_SUB.IND_ASFI = 1                                                                    ");
		sd.append("              AND R.OPERATION_PART  = VPR_SUB.operation_part),2)                                          ");
		sd.append("           PRECIO_CURVA_ASFI                                                                              ");
		
		  sd.append("   ,R.ESTADO_OPERACION,             ");
		  sd.append("   R.TASA_VALOR,                   ");
		  sd.append("   R.ACTIVIDAD_ECONOMICA_COMPRA,   ");
		  sd.append("   R.ACTIVIDAD_ECONOMICA_VENTA,     ");
		  sd.append("   R.OBSERVACIONES     ");
		
		sd.append(" FROM                                                                                                     ");
		sd.append("   (SELECT                                                                                                ");
		sd.append("   distinct                                                                                               ");
		sd.append("   MO.ID_MECHANISM_OPERATION_PK,                                                                          ");
		sd.append("   MO.OPERATION_NUMBER NRO_OPERACION,                                                                     ");
		sd.append(" 	CASE WHEN SO.IND_EXTENDED = 1                                                                                               ");
		sd.append(" 	      AND SO.IND_PREPAID = 1                                                                                                ");
		sd.append(" 	      THEN (SELECT DISTINCT TRUNC(SR.AUTHORIZE_DATE)                                                                         ");
		sd.append(" 	            from SETTLEMENT_DATE_OPERATION SDO                                                                              ");
		sd.append(" 	            inner join SETTLEMENT_REQUEST SR on SDO.ID_SETTLEMENT_REQUEST_FK=SR.ID_SETTLEMENT_REQUEST_PK                    ");
		sd.append(" 	            inner join SETTLEMENT_OPERATION SO_SUB on SDO.ID_SETTLEMENT_OPERATION_FK=SO_SUB.ID_SETTLEMENT_OPERATION_PK      ");
		sd.append(" 	            inner join MECHANISM_OPERATION MO_SUB on SO_SUB.ID_MECHANISM_OPERATION_FK=MO_SUB.ID_MECHANISM_OPERATION_PK      ");
		sd.append(" 	            where SR.REQUEST_TYPE=2019                                                                                      ");
		sd.append(" 	            AND MO_SUB.ID_MECHANISM_OPERATION_PK = MO.ID_MECHANISM_OPERATION_PK                                             ");
		sd.append(" 	            AND SR.REQUEST_STATE = 1543 )                                                                                   ");
		sd.append("   	   WHEN HAO.OPERATION_PART = 1 THEN MO.OPERATION_DATE                                                ");
		sd.append("        WHEN HAO.OPERATION_PART = 2 AND SO.IND_EXTENDED = 1 THEN TRUNC(MO.TERM_SETTLEMENT_DATE)           ");
		sd.append("        WHEN HAO.OPERATION_PART = 2 THEN SO.SETTLEMENT_DATE END FECHA_OPS,                                ");
		sd.append("                                                                                                          ");
		sd.append("   CASE WHEN HAO.OPERATION_PART = 1 THEN P1.MNEMONIC                                                      ");
		sd.append("        WHEN HAO.OPERATION_PART = 2 THEN P2.MNEMONIC END PAR_VENTA,                                       ");
		sd.append("                                                                                                          ");
		sd.append("   (CASE WHEN NM.ID_NEGOTIATION_MODALITY_PK in (12,13,1,2) THEN 'VEN'                                     ");
		sd.append("       WHEN NM.ID_NEGOTIATION_MODALITY_PK in (3,4) THEN 'VER'                                             ");
		sd.append("       END) MODALIDAD_VENTA,                                                                              ");
		sd.append("                                                                                                          ");
		sd.append("   (SELECT LISTAGG(ID_HOLDER_PK,', ' ||chr(13))  WITHIN GROUP (ORDER BY ID_HOLDER_PK)                     ");
		sd.append("       FROM(                                                                                              ");
		sd.append("       SELECT distinct H_SUB.ID_HOLDER_PK                                                                 ");
		sd.append("       from HOLDER_ACCOUNT_OPERATION HAO_SUB,  HOLDER_ACCOUNT_DETAIL HAD_SUB, HOLDER H_SUB                ");
		sd.append("       , MECHANISM_OPERATION MO                                                                           ");
		sd.append("       where 1 = 1                                                                                        ");
		sd.append("       AND HAD_SUB.ID_HOLDER_ACCOUNT_FK = HAO_SUB.ID_HOLDER_ACCOUNT_FK                                    ");
		sd.append("       AND H_SUB.ID_HOLDER_PK = HAD_SUB.ID_HOLDER_FK                                                      ");
		sd.append("       AND HAO_SUB.HOLDER_ACCOUNT_STATE = 625                                                             ");
		sd.append("       AND HAO_SUB.ROLE = 2                                                                               ");
		sd.append("       AND HAO_SUB.OPERATION_PART = HAO.OPERATION_PART                                                    ");
		sd.append("       AND HAO_SUB.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK)) CUI_VENTA,                  ");
		sd.append("                                                                                                          ");
		sd.append("   (SELECT LISTAGG(full_name,', ' ||chr(13))  WITHIN GROUP (ORDER BY full_name)                           ");
		sd.append("       FROM(                                                                                              ");
		sd.append("       SELECT distinct H_SUB.full_name                                                                    ");
		sd.append("       from HOLDER_ACCOUNT_OPERATION HAO_SUB,  HOLDER_ACCOUNT_DETAIL HAD_SUB, HOLDER H_SUB                ");
		sd.append("       , MECHANISM_OPERATION MO                                                                           ");
		sd.append("       where 1 = 1                                                                                        ");
		sd.append("       AND HAD_SUB.ID_HOLDER_ACCOUNT_FK = HAO_SUB.ID_HOLDER_ACCOUNT_FK                                    ");
		sd.append("       AND H_SUB.ID_HOLDER_PK = HAD_SUB.ID_HOLDER_FK                                                      ");
		sd.append("       AND HAO_SUB.HOLDER_ACCOUNT_STATE = 625                                                             ");
		sd.append("       AND HAO_SUB.ROLE = 2                                                                               ");
		sd.append("       AND HAO_SUB.OPERATION_PART = HAO.OPERATION_PART                                                    ");
		sd.append("       AND HAO_SUB.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK)) NOMBRE_VENTA,               ");
		sd.append("                                                                                                          ");
		sd.append("   (SELECT LISTAGG(DOCUMENT_NUMBER,', ' ||chr(13))  WITHIN GROUP (ORDER BY DOCUMENT_NUMBER)               ");
		sd.append("       FROM(                                                                                              ");
		sd.append("       SELECT distinct H_SUB.DOCUMENT_NUMBER                                                              ");
		sd.append("       from HOLDER_ACCOUNT_OPERATION HAO_SUB,  HOLDER_ACCOUNT_DETAIL HAD_SUB, HOLDER H_SUB                ");
		sd.append("       , MECHANISM_OPERATION MO                                                                           ");
		sd.append("       where 1 = 1                                                                                        ");
		sd.append("       AND HAD_SUB.ID_HOLDER_ACCOUNT_FK = HAO_SUB.ID_HOLDER_ACCOUNT_FK                                    ");
		sd.append("       AND H_SUB.ID_HOLDER_PK = HAD_SUB.ID_HOLDER_FK                                                      ");
		sd.append("       AND HAO_SUB.HOLDER_ACCOUNT_STATE = 625                                                             ");
		sd.append("       AND HAO_SUB.ROLE = 2                                                                               ");
		sd.append("       AND HAO_SUB.OPERATION_PART = HAO.OPERATION_PART                                                    ");
		sd.append("       AND HAO_SUB.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK)) CARNET_VENTA,               ");
		sd.append("                                                                                                          ");
		sd.append("   CASE WHEN HAO.OPERATION_PART = 1 THEN P2.MNEMONIC                                                      ");
		sd.append("        WHEN HAO.OPERATION_PART = 2 THEN P1.MNEMONIC END PAR_COMPRA,                                      ");
		sd.append("                                                                                                          ");
		sd.append("   ( CASE WHEN NM.ID_NEGOTIATION_MODALITY_PK IN (12,13,1,2) THEN 'COM'                                    ");
		sd.append("           WHEN NM.ID_NEGOTIATION_MODALITY_PK IN (3,4) THEN 'COR'                                         ");
		sd.append("       END) MODALIDAD_COMPRA,                                                                             ");
		sd.append("                                                                                                          ");
		sd.append("   (SELECT LISTAGG(ID_HOLDER_PK,', ' ||chr(13))  WITHIN GROUP (ORDER BY ID_HOLDER_PK)                     ");
		sd.append("       FROM(                                                                                              ");
		sd.append("       SELECT distinct H_SUB.ID_HOLDER_PK                                                                 ");
		sd.append("       from HOLDER_ACCOUNT_OPERATION HAO_SUB,  HOLDER_ACCOUNT_DETAIL HAD_SUB, HOLDER H_SUB                ");
		sd.append("       , MECHANISM_OPERATION MO                                                                           ");
		sd.append("       where 1 = 1                                                                                        ");
		sd.append("       AND HAD_SUB.ID_HOLDER_ACCOUNT_FK = HAO_SUB.ID_HOLDER_ACCOUNT_FK                                    ");
		sd.append("       AND H_SUB.ID_HOLDER_PK = HAD_SUB.ID_HOLDER_FK                                                      ");
		sd.append("       AND HAO_SUB.HOLDER_ACCOUNT_STATE = 625                                                             ");
		sd.append("       AND HAO_SUB.ROLE = 1                                                                               ");
		sd.append("       AND HAO_SUB.OPERATION_PART = HAO.OPERATION_PART                                                    ");
		sd.append("       AND HAO_SUB.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK)) CUI_COMPRA,                 ");
		sd.append("                                                                                                          ");
		sd.append("   (SELECT LISTAGG(full_name,', ' ||chr(13))  WITHIN GROUP (ORDER BY full_name)                           ");
		sd.append("       FROM(                                                                                              ");
		sd.append("       SELECT distinct H_SUB.full_name                                                                    ");
		sd.append("       from HOLDER_ACCOUNT_OPERATION HAO_SUB,  HOLDER_ACCOUNT_DETAIL HAD_SUB, HOLDER H_SUB                ");
		sd.append("       , MECHANISM_OPERATION MO                                                                           ");
		sd.append("       where 1 = 1                                                                                        ");
		sd.append("       AND HAD_SUB.ID_HOLDER_ACCOUNT_FK = HAO_SUB.ID_HOLDER_ACCOUNT_FK                                    ");
		sd.append("       AND H_SUB.ID_HOLDER_PK = HAD_SUB.ID_HOLDER_FK                                                      ");
		sd.append("       AND HAO_SUB.HOLDER_ACCOUNT_STATE = 625                                                             ");
		sd.append("       AND HAO_SUB.ROLE = 1                                                                               ");
		sd.append("       AND HAO_SUB.OPERATION_PART = HAO.OPERATION_PART                                                    ");
		sd.append("       AND HAO_SUB.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK)) NOMBRE_COMPRA,              ");
		sd.append("                                                                                                          ");
		sd.append("   (SELECT LISTAGG(DOCUMENT_NUMBER,', ' ||chr(13))  WITHIN GROUP (ORDER BY DOCUMENT_NUMBER)               ");
		sd.append("       FROM(                                                                                              ");
		sd.append("       SELECT distinct H_SUB.DOCUMENT_NUMBER                                                              ");
		sd.append("       from HOLDER_ACCOUNT_OPERATION HAO_SUB,  HOLDER_ACCOUNT_DETAIL HAD_SUB, HOLDER H_SUB                ");
		sd.append("       , MECHANISM_OPERATION MO                                                                           ");
		sd.append("       where 1 = 1                                                                                        ");
		sd.append("       AND HAD_SUB.ID_HOLDER_ACCOUNT_FK = HAO_SUB.ID_HOLDER_ACCOUNT_FK                                    ");
		sd.append("       AND H_SUB.ID_HOLDER_PK = HAD_SUB.ID_HOLDER_FK                                                      ");
		sd.append("       AND HAO_SUB.HOLDER_ACCOUNT_STATE = 625                                                             ");
		sd.append("       AND HAO_SUB.ROLE = 1                                                                               ");
		sd.append("       AND HAO_SUB.OPERATION_PART = HAO.OPERATION_PART                                                    ");
		sd.append("       AND HAO_SUB.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK)) CARNET_COMPRA,              ");
		sd.append("                                                                                                          ");
		sd.append("   NM.MODALITY_NAME MODALIDAD_NEG,                                                                        ");
		sd.append("   PT1.PARAMETER_NAME MONEDA,                                                                             ");
		sd.append("   SO.STOCK_QUANTITY CANT_TOTAL_VAL,                                                                      ");
		sd.append("   MO.ID_SECURITY_CODE_FK CLAVE_VALOR,                                                                    ");
		sd.append("   SEC.INITIAL_NOMINAL_VALUE VAL_NOMINAL,                                                                 ");
		sd.append("   CASE WHEN HAO.OPERATION_PART = 1 THEN MO.CASH_PRICE                                                    ");
		sd.append("        WHEN HAO.OPERATION_PART = 2 THEN MO.TERM_PRICE END PRECIO,                                        ");
		sd.append("   MO.AMOUNT_RATE TASA,                                                                                   ");
		sd.append("   MO.BALLOT_NUMBER||'/'||MO.SEQUENTIAL PAPELETA_SEQ,                                                     ");
		sd.append("                                                                                                          ");
		sd.append("   CASE WHEN HAO.OPERATION_PART = 1 THEN P1.ID_PARTICIPANT_PK                                             ");
		sd.append("        WHEN HAO.OPERATION_PART = 2 THEN P2.ID_PARTICIPANT_PK END ID_PAR_VENTA,                           ");
		sd.append("   CASE WHEN HAO.OPERATION_PART = 1 THEN P2.ID_PARTICIPANT_PK                                             ");
		sd.append("        WHEN HAO.OPERATION_PART = 2 THEN P1.ID_PARTICIPANT_PK END ID_PAR_COMPRA,                          ");
		sd.append("   PT1.PARAMETER_TABLE_PK ID_MONEDA,                                                                      ");
		sd.append("   NM.ID_NEGOTIATION_MODALITY_PK ID_MODALIDAD_NEG,                                                        ");
		sd.append("   CASE  WHEN MO.IND_TERM_SETTLEMENT = 1 THEN SO.SETTLEMENT_PRICE                                         ");
		sd.append("   ELSE (CASE WHEN HAO.OPERATION_PART = 1 THEN MO.CASH_PRICE                                              ");
		sd.append("              WHEN HAO.OPERATION_PART = 2 THEN MO.TERM_PRICE END)                                         ");
		sd.append("         END PRECIO_LIQUIDACION,                                                                          ");
		sd.append("		HAO.OPERATION_PART ,                        														 "); //issue 1301
		sd.append("     SO.REAL_SETTLEMENT_DATE,                    														 ");
		sd.append("     NM.ID_NEGOTIATION_MODALITY_PK,              														 ");
		sd.append("     MO.OPERATION_DATE FECHA_OPERA_REPORTO,      														 ");
		sd.append("     MO.CASH_SETTLEMENT_DATE,                    														 ");
		sd.append("     MO.TERM_SETTLEMENT_DATE,                    														 ");
		sd.append("     SO.IND_EXTENDED                             														 ");
		sd.append("                                                                                                          ");

		//issue 1364
		sd.append(" ,CASE WHEN MO.ID_NEGOTIATION_MODALITY_FK in (3,4)                                                                           ");
		sd.append("           THEN(CASE WHEN SO.OPERATION_STATE = 615 THEN 'REPORTO'                                                           ");
		sd.append("                     WHEN SO.OPERATION_STATE = 616                                                                          ");
		//sd.append("                       AND TRUNC(MO.TERM_SETTLEMENT_DATE) <> TRUNC(SO.REAL_SETTLEMENT_DATE) THEN 'LIQUIDACION ANTICIPADA'   ");
		
		sd.append("                       and SO.IND_PREPAID = 1 THEN 'LIQUIDACION ANTICIPADA'   ");
		
		sd.append("                     WHEN SO.OPERATION_STATE = 616 THEN 'VENCIMIENTO'    END)                                               ");
		sd.append("  END ESTADO_OPERACION,                                                                                                     ");
		sd.append("  SEC.INTEREST_RATE TASA_VALOR,                                                                                             ");
		sd.append("                                                                                                                            ");
		sd.append(" (SELECT LISTAGG(ACT_ECONOMICA, ', ' ||chr(13))                                                                             ");
		sd.append("                        WITHIN GROUP (ORDER BY ACT_ECONOMICA)                                                               ");
		sd.append("                 FROM ( SELECT distinct CASE WHEN HA_SUB.ACCOUNT_TYPE in ( 106, 108 ) THEN 'PARTICULARES'                   ");
		sd.append("                                    WHEN HA_SUB.ACCOUNT_TYPE = 107 THEN PT_SUB.DESCRIPTION                                  ");
		sd.append("                                    ELSE 'POR COMPLETAR'                                                                    ");
		sd.append("                                    END ACT_ECONOMICA                                                                       ");
		sd.append("                        from  HOLDER_ACCOUNT_OPERATION HAO_SUB,                                                             ");
		sd.append("                              HOLDER_ACCOUNT_DETAIL HAD_SUB,                                                                ");
		sd.append("                              HOLDER H_SUB,                                                                                 ");
		sd.append("                              HOLDER_ACCOUNT HA_SUB,                                                                        ");
		sd.append("                              PARAMETER_TABLE PT_SUB                                                                        ");
		sd.append("                        WHERE 1 = 1                                                                                         ");       
		sd.append("                        AND HAD_SUB.ID_HOLDER_ACCOUNT_FK = HAO_SUB.ID_HOLDER_ACCOUNT_FK                                     ");
		sd.append("                        AND HA_SUB.ID_HOLDER_ACCOUNT_PK = HAO_SUB.ID_HOLDER_ACCOUNT_FK                                      ");
		sd.append("                        AND H_SUB.ID_HOLDER_PK = HAD_SUB.ID_HOLDER_FK                                                       ");
		sd.append("                        AND PT_SUB.PARAMETER_TABLE_PK = H_SUB.ECONOMIC_ACTIVITY                                             ");
		sd.append("                        AND HAO_SUB.HOLDER_ACCOUNT_STATE = 625                                                              ");       
		sd.append("                        AND HAO_SUB.ROLE = 1                                                                                ");       
		sd.append("                        AND HAO_SUB.OPERATION_PART = HAO.OPERATION_PART                                                     ");         
		sd.append("                        AND HAO_SUB.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK)) ACTIVIDAD_ECONOMICA_COMPRA,  ");
		sd.append("                                                                                                                            ");
		sd.append("  (SELECT LISTAGG(ACT_ECONOMICA, ', ' ||chr(13))                                                                            ");
		sd.append("                        WITHIN GROUP (ORDER BY ACT_ECONOMICA)                                                               ");
		sd.append("                 FROM ( SELECT distinct CASE WHEN HA_SUB.ACCOUNT_TYPE in ( 106, 108 ) THEN 'PARTICULARES'                   ");
		sd.append("                                    WHEN HA_SUB.ACCOUNT_TYPE = 107 THEN PT_SUB.DESCRIPTION                                  ");
		sd.append("                                    ELSE 'POR COMPLETAR'                                                                    ");
		sd.append("                                    END ACT_ECONOMICA                                                                       ");
		sd.append("                        from  HOLDER_ACCOUNT_OPERATION HAO_SUB,                                                             ");
		sd.append("                              HOLDER_ACCOUNT_DETAIL HAD_SUB,                                                                ");
		sd.append("                              HOLDER H_SUB,                                                                                 ");
		sd.append("                              HOLDER_ACCOUNT HA_SUB,                                                                        ");
		sd.append("                              PARAMETER_TABLE PT_SUB                                                                        ");
		sd.append("                        WHERE 1 = 1                                                                                         ");       
		sd.append("                        AND HAD_SUB.ID_HOLDER_ACCOUNT_FK = HAO_SUB.ID_HOLDER_ACCOUNT_FK                                     ");
		sd.append("                        AND HA_SUB.ID_HOLDER_ACCOUNT_PK = HAO_SUB.ID_HOLDER_ACCOUNT_FK                                      ");
		sd.append("                        AND H_SUB.ID_HOLDER_PK = HAD_SUB.ID_HOLDER_FK                                                       ");
		sd.append("                        AND PT_SUB.PARAMETER_TABLE_PK = H_SUB.ECONOMIC_ACTIVITY                                             ");
		sd.append("                        AND HAO_SUB.HOLDER_ACCOUNT_STATE = 625                                                              ");       
		sd.append("                        AND HAO_SUB.ROLE = 2                                                                                ");       
		sd.append("                        AND HAO_SUB.OPERATION_PART = HAO.OPERATION_PART                                                     ");         
		sd.append("                        AND HAO_SUB.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK)) ACTIVIDAD_ECONOMICA_VENTA    ");
		
		//issue 1364
		sd.append("  ,CASE WHEN OU.ID_OPERATION_UNFULFILLMENT_PK IS NOT NULL AND OU.UNFULFILLMENT_REASON = 1236 THEN 'INCUMPLIMIENTO DE VALORES'              ");
		sd.append("        WHEN OU.ID_OPERATION_UNFULFILLMENT_PK IS NOT NULL AND OU.UNFULFILLMENT_REASON = 1235 THEN 'INCUMPLIMIENTO DE FONDOS'          	  ");
		sd.append("       WHEN SO.OPERATION_STATE = 613 THEN 'CANCELADA CONTADO'                                     						  				  ");
		sd.append("       WHEN SO.OPERATION_STATE = 1849 THEN 'CANCELADA PLAZO'                                      						  				  ");
		sd.append("       WHEN so.operation_part = 2 AND SR.ID_SETTLEMENT_REQUEST_PK IS NOT NULL THEN 'SELVE'                                                 ");
		sd.append("       WHEN SO.IND_EXTENDED = 1 THEN 'MELID'                                                                              				  ");
		sd.append("       WHEN SO.IND_FORCED_PURCHASE = 1 THEN 'COMPRA FORZOSA'                                                               				  ");
		sd.append("       WHEN SO.IND_EXTENDED = 0                                                                      				  ");
		sd.append("  			THEN (select 'MELOR'                                                                                           				  ");
		sd.append("                    from SETTLEMENT_OPERATION SO_SUB                                                                            			  ");
		sd.append("                    inner join OPERATION_SETTLEMENT OS_SUB on OS_SUB.ID_SETTLEMENT_OPERATION_FK = SO_SUB.ID_SETTLEMENT_OPERATION_PK     	  ");
		sd.append("                    inner join SETTLEMENT_PROCESS SP_SUB ON OS_SUB.ID_SETTLMENT_PROCESS_FK = SP_SUB.ID_SETTLEMENT_PROCESS_PK               ");
		sd.append("                    WHERE 1 = 1                                                                                             				  ");
		sd.append("                    And SO_SUB.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK                                         	  		  ");
		sd.append("                    and SO_SUB.ID_SETTLEMENT_OPERATION_PK = SO.ID_SETTLEMENT_OPERATION_PK 								   				  ");
		sd.append("                    and OS_SUB.OPERATION_STATE = 1288                                                                          			  ");
		sd.append("                    and SP_SUB.PROCESS_STATE = 1284                                                                            			  ");
		sd.append("                    AND SP_SUB.SCHEDULE_TYPE = 1941)                                                                           			  ");
		sd.append("       END OBSERVACIONES                                                                                                    				  ");
		sd.append("   FROM MECHANISM_OPERATION MO                                                                            ");
		sd.append("                                                                                                          ");
		sd.append("   INNER JOIN HOLDER_ACCOUNT_OPERATION HAO ON HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK ");
		sd.append("   INNER JOIN SETTLEMENT_OPERATION SO ON SO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK      ");
		sd.append("         AND SO.OPERATION_PART = HAO.OPERATION_PART                                                       ");
		sd.append("   INNER JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK = HAO.ID_HOLDER_ACCOUNT_FK                     ");
		sd.append("   INNER JOIN SECURITY SEC ON MO.ID_SECURITY_CODE_FK = SEC.ID_SECURITY_CODE_PK                            ");
		sd.append("   INNER JOIN NEGOTIATION_MODALITY NM ON MO.ID_NEGOTIATION_MODALITY_FK = NM.ID_NEGOTIATION_MODALITY_PK    ");
		sd.append("   INNER JOIN NEGOTIATION_MECHANISM MN ON MN.ID_NEGOTIATION_MECHANISM_PK = MO.ID_NEGOTIATION_MECHANISM_FK  ");
		sd.append("   INNER JOIN ISSUER ISS ON ISS.ID_ISSUER_PK = SEC.ID_ISSUER_FK                                           ");
		sd.append("   INNER JOIN PARTICIPANT P1 ON P1.ID_PARTICIPANT_PK = MO.ID_SELLER_PARTICIPANT_FK                        ");
		sd.append("   INNER JOIN PARTICIPANT P2 ON P2.ID_PARTICIPANT_PK = MO.ID_BUYER_PARTICIPANT_FK                         ");
		sd.append("   INNER JOIN PARAMETER_TABLE PT1 ON MO.OPERATION_CURRENCY = PT1.PARAMETER_TABLE_PK                       ");
		sd.append("   INNER JOIN PARTICIPANT P ON P.ID_PARTICIPANT_PK = HA.ID_PARTICIPANT_FK                                 ");
		
		sd.append("   LEFT JOIN OPERATION_UNFULFILLMENT OU ON OU.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK  	");
		sd.append("   left join SETTLEMENT_DATE_OPERATION SDO ON SDO.ID_SETTLEMENT_OPERATION_FK=SO.ID_SETTLEMENT_OPERATION_PK   ");
		sd.append("   left join SETTLEMENT_REQUEST SR on SDO.ID_SETTLEMENT_REQUEST_FK=SR.ID_SETTLEMENT_REQUEST_PK  				");
		sd.append("   							   and SR.REQUEST_TYPE = 2020 and SR.REQUEST_STATE = 1543						");
		
		
		sd.append("   WHERE 1 = 1                                                                                            ");
		sd.append("   AND ROLE = 1                                                                                           ");
		sd.append("  AND MO.OPERATION_STATE <> 613                                                                                                                   ");
		sd.append("   AND HAO.HOLDER_ACCOUNT_STATE = 625                                                                     ");
		sd.append("   AND MO.ID_NEGOTIATION_MODALITY_FK NOT IN (12,13)                                                       ");
		sd.append("   AND MO.ID_NEGOTIATION_MECHANISM_FK = 1                                                                 ");
		sd.append("   AND CASE WHEN SO.IND_EXTENDED = 1 THEN TRUNC(SO.SETTLEMENT_DATE)                                       ");
		sd.append("   		   WHEN HAO.OPERATION_PART = 1 THEN TRUNC(MO.OPERATION_DATE)                                     ");
		sd.append("            WHEN HAO.OPERATION_PART = 2 THEN TRUNC(SO.SETTLEMENT_DATE)                                    ");
		sd.append("       END                                                                                                ");
		sd.append("       BETWEEN to_date('"+params.getInitialDate()+"','DD/MM/YYYY') AND to_date('"+params.getFinalDate()+"','DD/MM/YYYY')                                                              ");
		
		// aplicando filtros de emisor, claseValor y claseClaveValor
		if(Validations.validateIsNotNullAndNotEmpty(params.getIdIssuer())){
			sd.append(" and ISS.ID_ISSUER_PK = :issuer ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(params.getIdSecurity())){
			sd.append(" AND sec.ID_SECURITY_CODE_PK = :claseClaveValor ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(params.getSecurityClass())){
			sd.append(" AND sec.SECURITY_CLASS = :securityClass ");
		}
		
		sd.append("  UNION ALL                                                                                                                                      ");
		sd.append("  SELECT                                                                                                                                         ");
		sd.append("    distinct                                                                                                                                     ");
		sd.append("    MO.ID_MECHANISM_OPERATION_PK,                                                                                                                ");
		sd.append("    MO.OPERATION_NUMBER NRO_OPERACION,                                                                                                           ");
		sd.append("                 CASE WHEN SO.IND_EXTENDED = 1                                                                                                   ");
		sd.append("                       AND SO.IND_PREPAID = 1                                                                                                    ");
		sd.append("                       THEN (SELECT DISTINCT TRUNC(SR.AUTHORIZE_DATE)                                                                            ");
		sd.append("                             from SETTLEMENT_DATE_OPERATION SDO                                                                                  ");
		sd.append("                             inner join SETTLEMENT_REQUEST SR on SDO.ID_SETTLEMENT_REQUEST_FK=SR.ID_SETTLEMENT_REQUEST_PK                        ");
		sd.append("                             inner join SETTLEMENT_OPERATION SO_SUB on SDO.ID_SETTLEMENT_OPERATION_FK=SO_SUB.ID_SETTLEMENT_OPERATION_PK          ");
		sd.append("                             inner join MECHANISM_OPERATION MO_SUB on SO_SUB.ID_MECHANISM_OPERATION_FK=MO_SUB.ID_MECHANISM_OPERATION_PK          ");
		sd.append("                             where SR.REQUEST_TYPE=2019                                                                                          ");
		sd.append("                             AND MO_SUB.ID_MECHANISM_OPERATION_PK = MO.ID_MECHANISM_OPERATION_PK                                                 ");
		sd.append("                             AND SR.REQUEST_STATE = 1543 )                                                                                       ");
		sd.append("                    WHEN SO.OPERATION_PART = 1 THEN MO.OPERATION_DATE                                                                            ");
		sd.append("         WHEN SO.OPERATION_PART = 2 AND SO.IND_EXTENDED = 1 THEN TRUNC(MO.TERM_SETTLEMENT_DATE)                                                  ");
		sd.append("         WHEN SO.OPERATION_PART = 2 THEN SO.SETTLEMENT_DATE END FECHA_OPS,                                                                       ");                                                       
		sd.append("    CASE WHEN SO.OPERATION_PART = 1 THEN P1.MNEMONIC                                                                                             ");
		sd.append("         WHEN SO.OPERATION_PART = 2 THEN P2.MNEMONIC END PAR_VENTA,                                                                              ");                                                    
		sd.append("    (CASE WHEN NM.ID_NEGOTIATION_MODALITY_PK in (12,13,1,2) THEN 'VEN'                                                                           ");
		sd.append("        WHEN NM.ID_NEGOTIATION_MODALITY_PK in (3,4) THEN 'VER'                                                                                   ");
		sd.append("        END) MODALIDAD_VENTA,                                                                                                                    ");                                                               
		sd.append("   '' CUI_VENTA,                                                                                                                                 ");
		sd.append("   '' NOMBRE_VENTA,                                                                                                                              ");
		sd.append("   '' CARNET_VENTA,                                                                                                                              ");
		sd.append("    CASE WHEN SO.OPERATION_PART = 1 THEN P2.MNEMONIC                                                                                             ");
		sd.append("         WHEN SO.OPERATION_PART = 2 THEN P1.MNEMONIC END PAR_COMPRA,                                                                             ");                               
		sd.append("    ( CASE WHEN NM.ID_NEGOTIATION_MODALITY_PK IN (12,13,1,2) THEN 'COM'                                                                          ");
		sd.append("            WHEN NM.ID_NEGOTIATION_MODALITY_PK IN (3,4) THEN 'COR'                                                                               ");
		sd.append("        END) MODALIDAD_COMPRA,                                                                                                                   ");                                                      
		sd.append("    '' CUI_COMPRA,                                                                                                                               ");
		sd.append("    '' NOMBRE_COMPRA,                                                                                                                            ");
		sd.append("    '' CARNET_COMPRA,                                                                                                                            ");
		sd.append("    NM.MODALITY_NAME MODALIDAD_NEG,                                                                                                              ");
		sd.append("    PT1.PARAMETER_NAME MONEDA,                                                                                                                   ");
		sd.append("    SO.STOCK_QUANTITY CANT_TOTAL_VAL,                                                                                                            ");
		sd.append("    MO.ID_SECURITY_CODE_FK CLAVE_VALOR,                                                                                                          ");
		sd.append("    SEC.INITIAL_NOMINAL_VALUE VAL_NOMINAL,                                                                                                       ");
		sd.append("    CASE WHEN SO.OPERATION_PART = 1 THEN MO.CASH_PRICE                                                                                           ");
		sd.append("         WHEN SO.OPERATION_PART = 2 THEN MO.TERM_PRICE END PRECIO,                                                                               ");
		sd.append("    MO.AMOUNT_RATE TASA,                                                                                                                         ");
		sd.append("    MO.BALLOT_NUMBER||'/'||MO.SEQUENTIAL PAPELETA_SEQ,                                                                                           ");                                                          
		sd.append("    CASE WHEN SO.OPERATION_PART = 1 THEN P1.ID_PARTICIPANT_PK                                                                                    ");
		sd.append("         WHEN SO.OPERATION_PART = 2 THEN P2.ID_PARTICIPANT_PK END ID_PAR_VENTA,                                                                  ");
		sd.append("    CASE WHEN SO.OPERATION_PART = 1 THEN P2.ID_PARTICIPANT_PK                                                                                    ");
		sd.append("         WHEN SO.OPERATION_PART = 2 THEN P1.ID_PARTICIPANT_PK END ID_PAR_COMPRA,                                                                 ");
		sd.append("    PT1.PARAMETER_TABLE_PK ID_MONEDA,                                                                                                            ");
		sd.append("    NM.ID_NEGOTIATION_MODALITY_PK ID_MODALIDAD_NEG,                                                                                              ");
		sd.append("    CASE  WHEN MO.IND_TERM_SETTLEMENT = 1 THEN SO.SETTLEMENT_PRICE                                                                               ");
		sd.append("    ELSE (CASE WHEN SO.OPERATION_PART = 1 THEN MO.CASH_PRICE                                                                                     ");
		sd.append("               WHEN SO.OPERATION_PART = 2 THEN MO.TERM_PRICE END)                                                                                ");
		sd.append("          END PRECIO_LIQUIDACION,                                                                                                                ");
		sd.append("      SO.OPERATION_PART ,                                                                                                                        ");                                                                                                 
		sd.append("      SO.REAL_SETTLEMENT_DATE,                                                                                                                   ");                                                                                                                      
		sd.append("      NM.ID_NEGOTIATION_MODALITY_PK,                                                                                                             ");                                                                                                                          
		sd.append("      MO.OPERATION_DATE FECHA_OPERA_REPORTO,                                                                                                     ");                                                                                                                           
		sd.append("      MO.CASH_SETTLEMENT_DATE,                                                                                                                   ");                                                                                                                   
		sd.append("      MO.TERM_SETTLEMENT_DATE,                                                                                                                   ");                                                                                                                   
		sd.append("      SO.IND_EXTENDED                                                                                                                            ");                                                                                                                   
		sd.append("  ,CASE WHEN MO.ID_NEGOTIATION_MODALITY_FK in (3,4)                                                                                              ");
		sd.append("             THEN(CASE WHEN SO.OPERATION_STATE = 615 THEN 'VIGENTE'                                                                              ");
		sd.append("                       WHEN SO.OPERATION_STATE = 616                                                                                             ");
		sd.append("                         AND TRUNC(MO.TERM_SETTLEMENT_DATE) <> TRUNC(SO.REAL_SETTLEMENT_DATE) THEN 'LIQUIDACION ANTICIPADA'                      ");
		sd.append("                       WHEN SO.OPERATION_STATE = 616 THEN 'VENCIMIENTO'    END)                                                                  ");
		sd.append("    END ESTADO_OPERACION,                                                                                                                        ");
		sd.append("    SEC.INTEREST_RATE TASA_VALOR,                                                                                                                ");                                                                                                
		sd.append("   '' ACTIVIDAD_ECONOMICA_COMPRA,                                                                                                                ");          
		sd.append("   '' ACTIVIDAD_ECONOMICA_VENTA                                                                                                                  ");
		sd.append("  ,CASE WHEN SO.OPERATION_PART = 1 AND SO.OPERATION_STATE = 613 THEN 'CANCELADA CONTADO'                                                         ");
		sd.append("  END OBSERVACIONES                                                                                                                              ");
		sd.append("  FROM MECHANISM_OPERATION MO                                                                                                                    ");                                                       
		sd.append("  INNER JOIN SETTLEMENT_OPERATION SO ON SO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK                                              ");
		sd.append("  INNER JOIN SECURITY SEC ON MO.ID_SECURITY_CODE_FK = SEC.ID_SECURITY_CODE_PK                                                                    ");
		sd.append("  INNER JOIN NEGOTIATION_MODALITY NM ON MO.ID_NEGOTIATION_MODALITY_FK = NM.ID_NEGOTIATION_MODALITY_PK                                            ");
		sd.append("  INNER JOIN NEGOTIATION_MECHANISM MN ON MN.ID_NEGOTIATION_MECHANISM_PK = MO.ID_NEGOTIATION_MECHANISM_FK                                         ");
		sd.append("  INNER JOIN ISSUER ISS ON ISS.ID_ISSUER_PK = SEC.ID_ISSUER_FK                                                                                   ");
		sd.append("  INNER JOIN PARTICIPANT P1 ON P1.ID_PARTICIPANT_PK = MO.ID_SELLER_PARTICIPANT_FK                                                                ");
		sd.append("  INNER JOIN PARTICIPANT P2 ON P2.ID_PARTICIPANT_PK = MO.ID_BUYER_PARTICIPANT_FK                                                                 ");
		sd.append("  INNER JOIN PARAMETER_TABLE PT1 ON MO.OPERATION_CURRENCY = PT1.PARAMETER_TABLE_PK                                                               ");
		sd.append("  WHERE 1 = 1                                                                                                                                    ");
		sd.append("  AND MO.ID_NEGOTIATION_MODALITY_FK NOT IN (12,13)                                                                                               ");
		sd.append("  AND MO.OPERATION_STATE = 613                                                                                                                   ");
		sd.append("  AND MO.ID_NEGOTIATION_MECHANISM_FK = 1                                                                                                         ");
		sd.append("  AND TRUNC(MO.OPERATION_DATE)  BETWEEN to_date('"+params.getInitialDate()+"','DD/MM/YYYY') AND to_date('"+params.getFinalDate()+"','DD/MM/YYYY')   ");
		
		// aplicando filtros de emisor, claseValor y claseClaveValor
		if(Validations.validateIsNotNullAndNotEmpty(params.getIdIssuer())){
			sd.append(" and ISS.ID_ISSUER_PK = :issuer ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(params.getIdSecurity())){
			sd.append(" AND sec.ID_SECURITY_CODE_PK = :claseClaveValor ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(params.getSecurityClass())){
			sd.append(" AND sec.SECURITY_CLASS = :securityClass ");
		}
		
		sd.append("   ) R                                                                                                    ");
		sd.append(" WHERE 0=0                                                                                                ");
		if(Validations.validateIsNotNullAndNotEmpty(params.getIdParticipant())){
			sd.append(" and (ID_PAR_VENTA = :idParticipant or ID_PAR_COMPRA = :idParticipant)                                ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(params.getCui())){
			sd.append(" and ( CUI_VENTA like ':cui' or CUI_COMPRA like ':cui')								 ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(params.getCurrency())){
			sd.append(" and ID_MONEDA = :currency                                                                            ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(params.getModality())){
			sd.append(" and :modality = ID_MODALIDAD_NEG                                                                     ");
		}
		sd.append(" ORDER BY R.ID_MECHANISM_OPERATION_PK, R.FECHA_OPS														 ");
		
    	//reemplazando los parametros en la query
		String query=sd.toString();
		
		if(Validations.validateIsNotNullAndNotEmpty(params.getIdIssuer())){
			query=query.replace(":issuer", "'"+params.getIdIssuer()+"'");
		}
		if(Validations.validateIsNotNullAndNotEmpty(params.getIdSecurity())){
			query=query.replace(":claseClaveValor", "'"+params.getIdSecurity()+"'");
		}
		if(Validations.validateIsNotNullAndNotEmpty(params.getSecurityClass())){
			query=query.replace(":securityClass", params.getSecurityClass());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(params.getIdParticipant())){
			query=query.replace(":idParticipant", params.getIdParticipant());
		}
		if(Validations.validateIsNotNullAndNotEmpty(params.getCui())){
			query=query.replace(":cui", params.getCui());
		}
		if(Validations.validateIsNotNullAndNotEmpty(params.getCurrency())){
			query=query.replace(":currency", params.getCurrency());
		}
		if(Validations.validateIsNotNullAndNotEmpty(params.getModality())){
			query=query.replace(":modality", params.getModality());
		}
		
		return query;
	}
	
	/**
	 * query para el reporte de mercado secundario issue 1005
	 * @param primaryAndSecondaryMarketTO
	 * @return
	 */
	public String getQuerySecondaryMarket(PrimaryAndSecondaryMarketTO primaryAndSecondaryMarketTO) {
		StringBuilder sd = new StringBuilder();
		
		sd.append(" SELECT FEC_OPERACION FEC_OPERACION, ");
		sd.append(" AGENCIA_COMP AGENCIA_COMP, ");
		sd.append(" CUI_COMP CUI_COMP, ");
		sd.append(" NOMBRE_COMP NOMBRE_COMP, ");
		sd.append(" AGENCIA_VEN AGENCIA_VEN, ");
		sd.append(" CUI_VEN CUI_VEN, ");
		sd.append(" NOMBRE_VEN NOMBRE_VEN, ");
		sd.append(" CLAVE_VALOR CLAVE_VALOR, ");
		sd.append(" EMISOR EMISOR, ");
		sd.append(" EMISOR_RAZ EMISOR_RAZ, ");
		sd.append(" FEC_EMISION_VALOR FEC_EMISION_VALOR, ");
		sd.append(" PLAZO_VALOR PLAZO_VALOR, ");
		sd.append(" FEC_VENC FEC_VENC, ");
		sd.append(" MONEDA MONEDA, ");
		sd.append(" VAL_NOMINAL VAL_NOMINAL, ");
		sd.append(" TASA_INT_NOMINAL TASA_INT_NOMINAL, ");
		sd.append(" TASA_NEGOCIACION TASA_NEGOCIACION, ");
		sd.append(" PREC_NEGOC PREC_NEGOC, ");
		sd.append(" PERI_PAGO_INTE PERI_PAGO_INTE, ");
		sd.append(" PERI_PAGO_AMORT PERI_PAGO_AMORT, ");
		sd.append(" INT_ULT_FEC_PAGADO INT_ULT_FEC_PAGADO, ");
		sd.append(" AMO_ULT_FEC_PAGADO AMO_ULT_FEC_PAGADO, ");
		sd.append(" INT_SIG_FEC_PAGO INT_SIG_FEC_PAGO, ");
		sd.append(" AMO_SIG_FEC_PAGO AMO_SIG_FEC_PAGO, ");
		sd.append(" CANTIDAD CANTIDAD, ");
		sd.append(" NRO_PAPELETA NRO_PAPELETA,  ");
		sd.append(" PRECIO PRECIO  ");
		sd.append(" FROM ");
		sd.append(" (SELECT  ");
		sd.append(" TO_CHAR(MO.OPERATION_DATE,'DD/MM/YYYY') FEC_OPERACION, ");
		sd.append(" (select MNEMONIC from PARTICIPANT where ID_PARTICIPANT_PK IN  ");
		sd.append(" (select ID_PARTICIPANT_FK from HOLDER_ACCOUNT where ID_HOLDER_ACCOUNT_PK IN (SELECT HAO.ID_HOLDER_ACCOUNT_FK ");
		sd.append("                                       FROM HOLDER_ACCOUNT_OPERATION HAO  ");
		sd.append("                                       WHERE ROLE = 1  ");
		sd.append("                                       AND HAO.HOLDER_ACCOUNT_STATE = 625 ");
		sd.append("                                       AND HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK))) AGENCIA_COMP, ");
		sd.append(" (SELECT LISTAGG(HAD.ID_HOLDER_FK , ', ' ||chr(13) )  ");
		sd.append("    WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)              ");                                                                                                                
		sd.append("    FROM HOLDER_ACCOUNT_DETAIL HAD                            ");                                                                                                                                                                             
		sd.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK IN ((SELECT HAO.ID_HOLDER_ACCOUNT_FK ");
		sd.append("                                       FROM HOLDER_ACCOUNT_OPERATION HAO  ");
		sd.append("                                       WHERE ROLE = 1  ");
		sd.append("                                       AND HAO.HOLDER_ACCOUNT_STATE = 625 ");
		sd.append("                                       AND HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK)) ) CUI_COMP, ");
		sd.append(" (SELECT LISTAGG((select h.full_name  ");
		sd.append("                  from holder h  ");
		sd.append("                 where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK),', ' ||chr(13))  ");
		sd.append("    WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)   ");
		sd.append("    FROM HOLDER_ACCOUNT_DETAIL HAD             ");
		sd.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK IN (SELECT HAO.ID_HOLDER_ACCOUNT_FK ");
		sd.append("                                       FROM HOLDER_ACCOUNT_OPERATION HAO  ");
		sd.append("                                       WHERE ROLE = 1  ");
		sd.append("                                       AND HAO.HOLDER_ACCOUNT_STATE = 625 ");
		sd.append("                                       AND HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK) ) NOMBRE_COMP, ");
		sd.append("                                        ");
		sd.append("   (select MNEMONIC from PARTICIPANT where ID_PARTICIPANT_PK =  ");
		sd.append(" (select ID_PARTICIPANT_FK from HOLDER_ACCOUNT where ID_HOLDER_ACCOUNT_PK IN (SELECT HAO.ID_HOLDER_ACCOUNT_FK ");
		sd.append("                                       FROM HOLDER_ACCOUNT_OPERATION HAO  ");
		sd.append("                                       WHERE ROLE = 2  ");
		sd.append("                                       AND HAO.HOLDER_ACCOUNT_STATE = 625 ");
		sd.append("                                       AND HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK))) AGENCIA_VEN, ");
		sd.append(" (SELECT LISTAGG(HAD.ID_HOLDER_FK , ', ' ||chr(13) )  ");
		sd.append("    WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)              ");                                                                                                                
		sd.append("    FROM HOLDER_ACCOUNT_DETAIL HAD                            ");                                                                                                                                                                             
		sd.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK IN ((SELECT HAO.ID_HOLDER_ACCOUNT_FK ");
		sd.append("                                       FROM HOLDER_ACCOUNT_OPERATION HAO  ");
		sd.append("                                       WHERE ROLE = 2  ");
		sd.append("                                       AND HAO.HOLDER_ACCOUNT_STATE = 625 ");
		sd.append("                                       AND HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK)) ) CUI_VEN, ");
		sd.append(" (SELECT LISTAGG((select h.full_name  ");
		sd.append("                  from holder h  ");
		sd.append("                 where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK),', ' ||chr(13))  ");
		sd.append("    WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)   ");
		sd.append("    FROM HOLDER_ACCOUNT_DETAIL HAD             ");
		sd.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK IN (SELECT HAO.ID_HOLDER_ACCOUNT_FK ");
		sd.append("                                       FROM HOLDER_ACCOUNT_OPERATION HAO  ");
		sd.append("                                       WHERE ROLE = 2 ");
		sd.append("                                       AND HAO.HOLDER_ACCOUNT_STATE = 625 ");
		sd.append("                                       AND HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK) ) NOMBRE_VEN, ");
		sd.append(" SEC.ID_SECURITY_CODE_PK CLAVE_VALOR, ");
		sd.append(" ISS.MNEMONIC EMISOR, ");
		sd.append(" ISS.BUSINESS_NAME EMISOR_RAZ, ");
		sd.append(" TO_CHAR(SEC.ISSUANCE_DATE,'DD/MM/YYYY') FEC_EMISION_VALOR, ");
		sd.append(" SEC.SECURITY_DAYS_TERM PLAZO_VALOR, ");
		sd.append(" TO_CHAR(SEC.EXPIRATION_DATE,'DD/MM/YYYY') FEC_VENC, ");
		sd.append(" PT2.DESCRIPTION MONEDA, ");
		sd.append(" SEC.INITIAL_NOMINAL_VALUE VAL_NOMINAL,  ");
		sd.append(" SEC.INTEREST_RATE TASA_INT_NOMINAL, ");
		sd.append(" MO.AMOUNT_RATE TASA_NEGOCIACION, ");
		sd.append(" MO.CASH_PRICE PREC_NEGOC,  ");
		sd.append(" CASE WHEN (SELECT IPS.PERIODICITY_DAYS ");
		sd.append("   FROM INTEREST_PAYMENT_SCHEDULE IPS ");
		sd.append("   WHERE ID_SECURITY_CODE_FK = MO.ID_SECURITY_CODE_FK) IS NULL THEN (SELECT PIC2.PAYMENT_DAYS ");
		sd.append("           FROM PROGRAM_INTEREST_COUPON PIC2 ");
		sd.append("           INNER JOIN INTEREST_PAYMENT_SCHEDULE IPS2  ");
		sd.append("           ON PIC2.ID_INT_PAYMENT_SCHEDULE_FK = IPS2.ID_INT_PAYMENT_SCHEDULE_PK ");
		sd.append("           WHERE IPS2.ID_SECURITY_CODE_FK = MO.ID_SECURITY_CODE_FK ");
		sd.append("           AND PIC2.COUPON_NUMBER = 1) ");
		sd.append("           ELSE (SELECT IPS.PERIODICITY_DAYS ");
		sd.append("           FROM INTEREST_PAYMENT_SCHEDULE IPS ");
		sd.append("           WHERE ID_SECURITY_CODE_FK = MO.ID_SECURITY_CODE_FK) ");
		sd.append("    END PERI_PAGO_INTE, ");
		sd.append("    CASE WHEN sec.SECURITY_CLASS = 420 THEN SEC.SECURITY_DAYS_TERM  ");
		sd.append("    ELSE ");
		sd.append("       (CASE WHEN (SELECT APS.PERIODICITY_DAYS ");
		sd.append("       FROM AMORTIZATION_PAYMENT_SCHEDULE APS ");
		sd.append("       WHERE ID_SECURITY_CODE_FK = MO.ID_SECURITY_CODE_FK) IS NULL THEN  ");
		sd.append("           (CASE WHEN (SELECT APS.PERIODICITY FROM AMORTIZATION_PAYMENT_SCHEDULE APS WHERE ID_SECURITY_CODE_FK = MO.ID_SECURITY_CODE_FK) = 540  THEN 360  ");
		sd.append("                 WHEN (SELECT APS.PERIODICITY FROM AMORTIZATION_PAYMENT_SCHEDULE APS WHERE ID_SECURITY_CODE_FK = MO.ID_SECURITY_CODE_FK) = 147  THEN 30 ");
		sd.append("                 WHEN (SELECT APS.PERIODICITY FROM AMORTIZATION_PAYMENT_SCHEDULE APS WHERE ID_SECURITY_CODE_FK = MO.ID_SECURITY_CODE_FK) = 1747 THEN 30 END) ");
		sd.append("           ELSE (SELECT APS.PERIODICITY_DAYS ");
		sd.append("           FROM AMORTIZATION_PAYMENT_SCHEDULE APS ");
		sd.append("           WHERE ID_SECURITY_CODE_FK = MO.ID_SECURITY_CODE_FK)       ");
		sd.append("       END) ");
		sd.append("   END PERI_PAGO_AMORT, ");
		sd.append(" (SELECT  MAX (PIC.PAYMENT_DATE) ");
		sd.append("   FROM PROGRAM_INTEREST_COUPON PIC ");
		sd.append("   INNER JOIN INTEREST_PAYMENT_SCHEDULE IPS  ");
		sd.append("   ON PIC.ID_INT_PAYMENT_SCHEDULE_FK = IPS.ID_INT_PAYMENT_SCHEDULE_PK ");
		sd.append("   WHERE IPS.ID_SECURITY_CODE_FK = MO.ID_SECURITY_CODE_FK ");
		sd.append("   AND PIC.SITUATION_PROGRAM_INTEREST = 2259) INT_ULT_FEC_PAGADO, ");
		sd.append(" (SELECT MAX(PAC.PAYMENT_DATE) ");
		sd.append("   FROM PROGRAM_AMORTIZATION_COUPON PAC ");
		sd.append("   INNER JOIN AMORTIZATION_PAYMENT_SCHEDULE APS  ");
		sd.append("   ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK ");
		sd.append("   WHERE APS.ID_SECURITY_CODE_FK = MO.ID_SECURITY_CODE_FK ");
		sd.append("   AND PAC.SITUATION_PROGRAM_AMORTIZATION = 2259) AMO_ULT_FEC_PAGADO, ");
		sd.append(" (SELECT MIN(PIC.PAYMENT_DATE) ");
		sd.append("   FROM PROGRAM_INTEREST_COUPON PIC ");
		sd.append("   INNER JOIN INTEREST_PAYMENT_SCHEDULE IPS  ");
		sd.append("   ON PIC.ID_INT_PAYMENT_SCHEDULE_FK = IPS.ID_INT_PAYMENT_SCHEDULE_PK ");
		sd.append("   WHERE IPS.ID_SECURITY_CODE_FK = MO.ID_SECURITY_CODE_FK ");
		sd.append("   AND PIC.PAYMENT_DATE > to_date('" + primaryAndSecondaryMarketTO.getFinalDate() + "','DD/MM/YYYY') ");
		//sd.append("   AND PIC.PAYMENT_DATE > '30/09/2018' ");
		sd.append("   AND PIC.SITUATION_PROGRAM_INTEREST = 2258) INT_SIG_FEC_PAGO,   ");
		sd.append(" (SELECT MIN(PAC.PAYMENT_DATE) ");
		sd.append("   FROM PROGRAM_AMORTIZATION_COUPON PAC ");
		sd.append("   INNER JOIN AMORTIZATION_PAYMENT_SCHEDULE APS  ");
		sd.append("   ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK ");
		sd.append("   WHERE APS.ID_SECURITY_CODE_FK = MO.ID_SECURITY_CODE_FK ");
		sd.append("   AND PAC.PAYMENT_DATE > to_date('" + primaryAndSecondaryMarketTO.getFinalDate() + "','DD/MM/YYYY') ");
		//sd.append("   AND PAC.PAYMENT_DATE > '30/09/2018' ");
		sd.append("   AND PAC.SITUATION_PROGRAM_AMORTIZATION = 2258) AMO_SIG_FEC_PAGO,   ");
		sd.append(" MO.STOCK_QUANTITY CANTIDAD , ");
		sd.append(" (MO.BALLOT_NUMBER ||'/'|| MO.SEQUENTIAL) NRO_PAPELETA, ");
		sd.append(" MO.CASH_PRICE PRECIO ");
		sd.append(" from MECHANISM_OPERATION MO  ");
		sd.append(" INNER JOIN SECURITY SEC ON SEC.ID_SECURITY_CODE_PK = MO.ID_SECURITY_CODE_FK ");
		sd.append(" INNER JOIN ISSUER ISS ON SEC.ID_ISSUER_FK = ISS.ID_ISSUER_PK ");
		sd.append(" INNER JOIN PARAMETER_TABLE PT2 ON SEC.CURRENCY = PT2.PARAMETER_TABLE_PK ");
		sd.append(" WHERE 1 = 1 ");
		sd.append(" AND MO.ID_NEGOTIATION_MODALITY_FK in (1,2) ");
		sd.append(" AND ((451) IN (SELECT HAD2.ID_HOLDER_FK FROM HOLDER_ACCOUNT_DETAIL HAD2 where HAD2.ID_HOLDER_ACCOUNT_FK IN (SELECT HAO.ID_HOLDER_ACCOUNT_FK ");
		sd.append("                                       FROM HOLDER_ACCOUNT_OPERATION HAO  ");
		sd.append("                                       WHERE ROLE IN (1,2)  ");
		sd.append("                                       AND HAO.HOLDER_ACCOUNT_STATE = 625 ");
		sd.append("                                       AND HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK) ) ");
		sd.append("   OR (452) IN (SELECT HAD2.ID_HOLDER_FK FROM HOLDER_ACCOUNT_DETAIL HAD2 where HAD2.ID_HOLDER_ACCOUNT_FK IN (SELECT HAO.ID_HOLDER_ACCOUNT_FK ");
		sd.append("                                       FROM HOLDER_ACCOUNT_OPERATION HAO  ");
		sd.append("                                       WHERE ROLE IN (1,2)  ");
		sd.append("                                       AND HAO.HOLDER_ACCOUNT_STATE = 625 ");
		sd.append("                                       AND HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK) )) ");
		sd.append("                                        ");
		//parametro para rango de fecha
		sd.append(" 	AND MO.OPERATION_DATE BETWEEN to_date('" + primaryAndSecondaryMarketTO.getInitialDate() + "','DD/MM/YYYY') AND to_date('"+ primaryAndSecondaryMarketTO.getFinalDate() + "','DD/MM/YYYY') ");
		//sd.append("       AND MO.OPERATION_DATE  BETWEEN '25/09/2018' AND '30/09/2018' ");
		sd.append("       ) x ");
		sd.append("       WHERE 0=0 ");

		// paramentros de filtro en el reporte
		if (Validations.validateIsNotNullAndNotEmpty(primaryAndSecondaryMarketTO.getMnemonicParticipant())) {
			sd.append(" and (x.AGENCIA_COMP = ':p_participant' ) ");
		}
		if (Validations.validateIsNotNullAndNotEmpty(primaryAndSecondaryMarketTO.getIdHolderSelected())) {
			sd.append(" and (':p_cui' in (select trim(regexp_substr(replace(x.CUI_COMP,chr(13),''),'[^,]+', 1, level)) from dual ");
			sd.append("connect by regexp_substr(replace(x.CUI_COMP,chr(13),''), '[^,]+', 1, level) is not null))");
		}

		// plezando los parametros en la query
		String query = sd.toString();
		if (Validations.validateIsNotNullAndNotEmpty(primaryAndSecondaryMarketTO.getMnemonicParticipant())) {
			query = query.replace(":p_participant", primaryAndSecondaryMarketTO.getMnemonicParticipant());
		}
		if (Validations.validateIsNotNullAndNotEmpty(primaryAndSecondaryMarketTO.getIdHolderSelected())) {
			query = query.replace(":p_cui", primaryAndSecondaryMarketTO.getIdHolderSelected().toString());
		}

		return query;
	}
	
    public List<Object[]> getTransferPersonalInfo(MechanismOperationTO filter) {
    		
    	StringBuilder sbQuery = new StringBuilder();
    	
    	sbQuery.append(" SELECT ").
    	append(" id_mechanism_operation_pk, ").//0
    	append(" participante_encargado, ").//1
    	append(" operation_number, ").//2
    	append(" mecanimos_negociacion,").//3
    	append(" modalidad_negociacion, ").//4
    	append(" operation_state, ").//5
    	append(" fecha_operacion, ").//6
    	append(" valor, ").//7
    	append(" account_number, ").//8
    	append(" LISTAGG(rnt_code,'<br/><br/><br/>') WITHIN GROUP (ORDER BY rnt_code) AS rnt_code, ").//9
    	append(" LISTAGG(tipo_holder,'<br/><br/><br/>') WITHIN GROUP (ORDER BY rnt_code) AS tipo_holder, ").//10
    	append(" LISTAGG(pais_residencia,'<br/><br/><br/>') WITHIN GROUP (ORDER BY rnt_code) AS pais_residencia, ").//11
    	append(" LISTAGG(provincia,'<br/><br/><br/>') WITHIN GROUP (ORDER BY rnt_code) AS provincia, ").//12
    	append(" LISTAGG(distrito,'<br/><br/><br/>') WITHIN GROUP (ORDER BY rnt_code) AS distrito, ").//13
    	append(" LISTAGG(nacionalidad,'<br/><br/><br/>') WITHIN GROUP (ORDER BY rnt_code) AS nacionalidad, ").//14
    	append(" LISTAGG(holder_description || '<br/>DIRECCIÓN: ' || direccion ,'<br/><br/>') WITHIN GROUP (ORDER BY rnt_code) AS holder_description, ").//15
    	append(" rol, ").//16
    	append(" rol_descripcion, ").//17
    	append(" cantidad ").//18			
    	append(" FROM  ").
    	append(" ( ");
    	
    	sbQuery.append(" Select distinct ").
    	append(" MO.id_mechanism_operation_pk as id_mechanism_operation_pk, ").
    	append(" P.mnemonic  || ' - ' ||  P.id_participant_pk  as participante_encargado, ").
    	append(" MO.operation_number as operation_number, ").
    	append(" (select  mechanism_name  from negotiation_mechanism  where MO.id_negotiation_mechanism_fk = id_negotiation_mechanism_pk) as mecanimos_negociacion, ").
    	append(" (select modality_name  from  negotiation_modality where MO.id_negotiation_modality_fk = id_negotiation_modality_pk) as modalidad_negociacion, ").
    	append(" (select parameter_name from parameter_table where parameter_table_pk=MO.operation_state) as operation_state, ").
    	append(" MO.operation_date fecha_operacion, ").
    	append(" (select (id_isin_code_pk||' - '||description) from security where id_isin_code_pk=mo.id_isin_code_fk) as valor, ").
    	append(" HA.account_number as account_number, ").
    	append(" H.id_holder_pk as rnt_code, ").
    	append(" (select parameter_name from parameter_table where parameter_table_pk=h.holder_type) tipo_holder, ").
    	append(" (select gl.name from geographic_location gl where h.legal_residence_country = gl.id_geographic_location_pk) pais_residencia, ").
    	append(" (select gl.name from geographic_location gl where h.legal_province = gl.id_geographic_location_pk) provincia, ").    	
    	append(" (select gl.name from geographic_location gl where h.legal_district = gl.id_geographic_location_pk) distrito, ").
    	append(" (select gl.name from geographic_location gl where h.nationality = gl.id_geographic_location_pk) nacionalidad, ").
    	append(" h.legal_address direccion, ").
    	append(" case when length(h.full_name)>33 then substr(h.full_name,0,30) || '...' else h.full_name end holder_description, ").
    	append(" HAO.role as rol, ").
    	append(" (case when HAO.role = 1 then 'COMPRA' else 'VENTA' end ) rol_descripcion, ").
    	append(" HAO.stock_quantity as cantidad ").
    	append(" FROM ").
    	append(" MECHANISM_OPERATION MO ").
    	append(" INNER JOIN PARTICIPANT_OPERATION PO ON MO.id_mechanism_operation_pk = PO.id_mechanism_operation_fk and PO.incharge_type=1 and PO.operation_part = 1 ").    	
    	append(" INNER JOIN PARTICIPANT P ON PO.id_participant_fk = p.id_participant_pk ").
    	append(" INNER JOIN HOLDER_ACCOUNT_OPERATION HAO  ON MO.id_mechanism_operation_pk = HAO.id_mechanism_operation_fk  and HAO.operation_part = 1 And HAO.id_incharge_stock_participant=PO.id_participant_fk ").
    	append(" INNER JOIN HOLDER_ACCOUNT  HA ON HA.id_holder_account_pk = HAO.id_holder_account_fk ").
    	append(" INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON had.id_holder_account_fk = HA.id_holder_account_pk ").    	
    	append(" INNER JOIN HOLDER H  ON had.id_holder_fk= h.id_holder_pk ").
    	append(" INNER JOIN SECURITY SE ON SE.id_isin_code_pk = MO.id_isin_code_fk ").
    	append(" WHERE ").
    	append(" MO.OPERATION_STATE = :state_cash_settled ").    	
    	append(" AND MO.id_negotiation_mechanism_fk = :mechanism_type ").
    	append(" AND TRUNC(MO.operation_date) between :init_date And  :end_date ");
    	    	
    	if(Validations.validateIsNotNullAndPositive(filter.getIdNegotiationModalityPk())) {
    		sbQuery.append(" AND MO.id_negotiation_modality_fk = :modality_type ");
    	}
    		
    	
    	if(Validations.validateIsNotNull(filter.getIdSecurityCodePk())) {
    		sbQuery.append(" AND mo.id_isin_code_fk = :isin_code ");
    	}
    		
    	if(Validations.validateIsNotNull(filter.getIdIssuer())) {
    		sbQuery.append(" AND SE.id_issuer_fk = :id_issuer ");
    	}
    	
    	sbQuery.append(" Union All ");
    		
    	
    	sbQuery.append(" Select distinct ").
    	append(" MO.id_mechanism_operation_pk as id_mechanism_operation_pk, ").
    	append(" P.mnemonic  || ' - ' ||  P.id_participant_pk  as participante_encargado, ").
    	append(" MO.operation_number as operation_number, ").
    	append(" (select  mechanism_name  from negotiation_mechanism  where MO.id_negotiation_mechanism_fk = id_negotiation_mechanism_pk) as mecanimos_negociacion, ").
    	append(" (select modality_name  from  negotiation_modality where MO.id_negotiation_modality_fk = id_negotiation_modality_pk) as modalidad_negociacion, ").
    	append(" (select parameter_name from parameter_table where parameter_table_pk=MO.operation_state) as operation_state, ").
    	append(" MO.operation_date fecha_operacion, ").
    	append(" (select (id_isin_code_pk||' - '||description) from security where id_isin_code_pk=mo.id_isin_code_fk) as valor, ").
    	append(" HA.account_number as account_number, ").
    	append(" H.id_holder_pk as rnt_code, ").
    	append(" (select parameter_name from parameter_table where parameter_table_pk=h.holder_type) tipo_holder, ").
    	append(" (select gl.name from geographic_location gl where h.legal_residence_country = gl.id_geographic_location_pk) pais_residencia, ").
    	append(" (select gl.name from geographic_location gl where h.legal_province = gl.id_geographic_location_pk) provincia, ").
    	append(" (select gl.name from geographic_location gl where h.legal_district = gl.id_geographic_location_pk) distrito, ").
    	append(" (select gl.name from geographic_location gl where h.nationality = gl.id_geographic_location_pk) nacionalidad, ").
    	append(" h.legal_address direccion, ").
    	append(" case when length(h.full_name)>33 then substr(h.full_name,0,30) || '...' else h.full_name end holder_description, ").
    	append(" HAO.role as rol, ").
    	append(" (case when HAO.role = 1 then 'COMPRA' else 'VENTA' end ) rol_descripcion, ").
    	append(" HAO.stock_quantity as cantidad ").
    	append(" FROM ").
    	append(" MECHANISM_OPERATION MO ").
    	append(" INNER JOIN PARTICIPANT_OPERATION PO ON MO.id_mechanism_operation_pk = PO.id_mechanism_operation_fk and PO.incharge_type=1 and PO.operation_part = 2 ").
    	append(" INNER JOIN PARTICIPANT P ON PO.id_participant_fk = p.id_participant_pk ").
    	append(" INNER JOIN HOLDER_ACCOUNT_OPERATION HAO ON MO.id_mechanism_operation_pk = HAO.id_mechanism_operation_fk  and HAO.operation_part = 2 And HAO.id_incharge_stock_participant=PO.id_participant_fk ").
    	append(" INNER JOIN HOLDER_ACCOUNT  HA ON HA.id_holder_account_pk = HAO.id_holder_account_fk ").
    	append(" INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON had.id_holder_account_fk = HA.id_holder_account_pk ").
    	append(" INNER JOIN HOLDER H ON had.id_holder_fk= h.id_holder_pk ").
    	append(" INNER JOIN SECURITY SE ON SE.id_isin_code_pk = MO.id_isin_code_fk ").
    	append(" WHERE ").
    	append(" MO.OPERATION_STATE = :state_term_settled ").
    	append(" AND MO.id_negotiation_mechanism_fk = :mechanism_type ").
    	append(" AND TRUNC(MO.operation_date) between :init_date And  :end_date ");
    	    	
    	if(Validations.validateIsNotNullAndPositive(filter.getIdNegotiationModalityPk())) {
    		sbQuery.append(" AND MO.id_negotiation_modality_fk = :modality_type ");
    	}
    		
    	
    	if(Validations.validateIsNotNull(filter.getIdSecurityCodePk())) {
    		sbQuery.append(" AND mo.id_isin_code_fk = :isin_code ");
    	}
    		
    	if(Validations.validateIsNotNull(filter.getIdIssuer())) {
    		sbQuery.append(" AND SE.id_issuer_fk = :id_issuer ");
    	}
    	
    	sbQuery.append(" ) ");
    	
    	sbQuery.append(" Group By ").
    	append(" id_mechanism_operation_pk, ").
    	append(" participante_encargado, ").
    	append(" operation_number, ").
    	append(" mecanimos_negociacion, ").
    	append(" modalidad_negociacion, ").
    	append(" operation_state, ").
    	append(" fecha_operacion, ").
    	append(" valor, ").
    	append(" account_number, ").
    	append(" rol, ").
    	append(" rol_descripcion, ").
    	append(" cantidad ").
    	append(" Order By ").
    	append(" mecanimos_negociacion, modalidad_negociacion,valor, ").
    	append(" fecha_operacion,id_mechanism_operation_pk,rol, ").
    	append(" account_number ");

		
    	Query query = em.createNativeQuery(sbQuery.toString());
    	

    	query.setParameter("state_cash_settled",MechanismOperationStateType.CASH_SETTLED.getCode());
    	query.setParameter("state_term_settled",MechanismOperationStateType.TERM_SETTLED.getCode());
    	query.setParameter("mechanism_type",filter.getIdMechanismOperationPk());
    	query.setParameter("init_date",filter.getInitDate(), TemporalType.DATE);
    	query.setParameter("end_date",filter.getEndDate(), TemporalType.DATE);
    	
    	if(Validations.validateIsNotNullAndPositive(filter.getIdNegotiationModalityPk())) {
    		query.setParameter("modality_type",filter.getIdNegotiationModalityPk());
    	}
    	
    	if(Validations.validateIsNotNull(filter.getIdSecurityCodePk())) {
    		query.setParameter("isin_code",filter.getIdSecurityCodePk());
    	}
    		
    	if(Validations.validateIsNotNull(filter.getIdIssuer())) {
    		query.setParameter("id_issuer",filter.getIdIssuer());
    	}
    		
    
    	return query.getResultList();
    }
    
    public List<Object[]> getHolderAssignmentOperationByParameters(
    		Long participant_incharge,Long mechanism_type,Long modality_type,
    		Integer state_operation, Integer role, Date operation_date){
    	
    	StringBuilder sbQuery = new StringBuilder();
    	
    	sbQuery.append(" SELECT");
    	sbQuery.append(" case when HAO.ind_incharge = 1 and HAO.role = 1 then (P1.mnemonic || ' - ' || P1.id_participant_pk)");
        sbQuery.append(" when HAO.ind_incharge = 1 and HAO.role = 2 then (P2.mnemonic || ' - ' ||  P2.id_participant_pk)");
        sbQuery.append(" when HAO.ind_incharge = 0 then (P.mnemonic || ' - ' || P.id_participant_pk)");
    	sbQuery.append(" end as participant_incharge,");
        sbQuery.append(" MO.operation_number as operation_number,");
        sbQuery.append(" MO.operation_state as operation_state,");
        sbQuery.append(" HA.account_number as account_number,");
        sbQuery.append(" H.id_holder_pk as rnt_code,");
        sbQuery.append(" H.holder_type as holder_type,");
        sbQuery.append(" H.name as name,");
        sbQuery.append(" H.first_last_name as first_last_name,");
        sbQuery.append(" H.second_last_name as second_last_name,");
        sbQuery.append(" H.full_name as full_name,");
        sbQuery.append(" HAO.role as role,");
        sbQuery.append(" HAO.stock_quantity as quantity,");
        sbQuery.append(" P.mnemonic || ' - ' || P.id_participant_pk as participant_negotiator,");
        sbQuery.append(" MO.cash_settlement_date as cash_date,");
        sbQuery.append(" MO.term_settlement_date as term_date");
        sbQuery.append(" FROM MECHANISM_OPERATION MO");
        sbQuery.append(" INNER JOIN PARTICIPANT P1 ON MO.id_buyer_participant_fk = P1.id_participant_pk");
        sbQuery.append(" INNER JOIN PARTICIPANT P2 ON MO.id_seller_participant_fk = P2.id_participant_pk");
        sbQuery.append(" INNER JOIN HOLDER_ACCOUNT_OPERATION HAO ON MO.id_mechanism_operation_pk = HAO.id_mechanism_operation_fk");
        sbQuery.append(" INNER JOIN HOLDER_ACCOUNT  HA ON HA.id_holder_account_pk = HAO.id_holder_account_fk");
        sbQuery.append(" INNER JOIN HOLDER H ON HA.id_participant_fk = H.id_participant_fk");
        sbQuery.append(" INNER JOIN PARTICIPANT P ON HAO.id_incharge_stock_participant = P.id_participant_pk");
        sbQuery.append(" WHERE MO.id_negotiation_mechanism_fk = :mechanism_type");
        sbQuery.append(" AND MO.id_negotiation_modality_fk = :modality_type");
        sbQuery.append(" AND trunc(MO.operation_date) = :operation_date");
        
        if(participant_incharge!=null){
        sbQuery.append(" AND");	
        sbQuery.append(" case when HAO.ind_incharge = 1 and HAO.role = 1 then P1.id_participant_pk");
        sbQuery.append(" when HAO.ind_incharge = 1 and HAO.role = 2 then P2.id_participant_pk");
        sbQuery.append(" when HAO.ind_incharge = 0 then P.id_participant_pk");
        sbQuery.append(" end = :participant_incharge");	
        }
        
        if(state_operation!=null){
        	sbQuery.append(" AND MO.operation_state = :state_operation");
        }
        
        if(role!=null){
        	sbQuery.append(" AND HAO.role = :role");
        }
        
        sbQuery.append(" GROUP BY");
        sbQuery.append(" case when HAO.ind_incharge = 1 and HAO.role = 1 then (P1.mnemonic || ' - ' || P1.id_participant_pk)");
        sbQuery.append(" when HAO.ind_incharge = 1 and HAO.role = 2 then (P2.mnemonic || ' - ' ||  P2.id_participant_pk)");
        sbQuery.append(" when HAO.ind_incharge = 0 then (P.mnemonic || ' - ' || P.id_participant_pk)");
    	sbQuery.append(" end,");
        sbQuery.append(" MO.operation_number,");
        sbQuery.append(" MO.operation_state,");
        sbQuery.append(" HA.account_number,");
        sbQuery.append(" H.id_holder_pk,");
        sbQuery.append(" H.holder_type,");
        sbQuery.append(" H.name,");
        sbQuery.append(" H.first_last_name,");
        sbQuery.append(" H.second_last_name,");
        sbQuery.append(" H.full_name,");
        sbQuery.append(" HAO.role,");
        sbQuery.append(" HAO.stock_quantity,");
        sbQuery.append(" P.mnemonic || ' - ' || P.id_participant_pk,");
        sbQuery.append(" MO.cash_settlement_date,");
        sbQuery.append(" MO.term_settlement_date "); 
        sbQuery.append(" ORDER BY (case when HAO.ind_incharge = 1 and HAO.role = 1 then (P1.mnemonic || ' - ' || P1.id_participant_pk)"); 
        sbQuery.append(" when HAO.ind_incharge = 1 and HAO.role = 2 then (P2.mnemonic || ' - ' ||  P2.id_participant_pk) ");
        sbQuery.append(" when HAO.ind_incharge = 0 then (P.mnemonic || ' - ' || P.id_participant_pk)");
        sbQuery.append(" end),");
        sbQuery.append(" MO.operation_number,");
        sbQuery.append(" HAO.role asc");
        																										
    	 Query query = em.createNativeQuery(sbQuery.toString());
    	
    	 if(mechanism_type!=null){
    	 	query.setParameter("mechanism_type",mechanism_type);
    	 }
    	 if(modality_type!=null){
    		 query.setParameter("modality_type",modality_type);
    	 }
    	 if(operation_date!=null){
    		 query.setParameter("operation_date", operation_date,TemporalType.DATE);
    	 }    	 
    	 if(state_operation!=null){
    		 query.setParameter("state_operation",state_operation);
    	 }
    	 if(role!=null){
    		 query.setParameter("role",role);
    	 }
    	 
    	 return query.getResultList();
    	
    }
    
    
    public List<Object[]> getPendingBlockSellBalanceOperations(Long mechanism_type,Long modality_type,Long participant_type, String isin_code, Date initialDate,
    		Date endDate,Integer operationPart){
    	
    	StringBuilder sbQuery = new StringBuilder();
    	HashMap<String, Object> params = new HashMap<String,Object>();
    			
    	sbQuery.append(" select MO.operation_date, ");
    	sbQuery.append("  MO.operation_number, ");
    	sbQuery.append("  MO.cash_settlement_date, ");
    	sbQuery.append("  MO.term_settlement_date , ");
    	sbQuery.append("  (select mechanism_name from negotiation_mechanism where id_negotiation_mechanism_pk= mo.id_negotiation_mechanism_fk) mecanism,");
    	sbQuery.append("  (select modality_name from negotiation_modality where id_negotiation_modality_pk=mo.id_negotiation_modality_fk) modality,");
    	sbQuery.append("  (select mnemonic||' '||id_participant_pk from participant where id_participant_pk=po.id_participant_fk) seller_part, ");
    	sbQuery.append("  (select mnemonic||' '||id_participant_pk from participant where id_participant_pk=mo.id_buyer_participant_fk) buyer_part, ");
    	sbQuery.append("  (select description from parameter_table where parameter_table_pk=mo.currency) moneda,  ");
    	sbQuery.append("  po.stock_quantity as cantidad_valores,");
    	sbQuery.append("  po.blocked_stock cantidad_bloqueada,");
    	sbQuery.append("  (po.stock_quantity-po.blocked_stock) pendiente_bloquear");
    	sbQuery.append("    from mechanism_operation MO, participant_operation po");
    	sbQuery.append("   where mo.id_mechanism_operation_pk=po.id_mechanism_operation_fk");
    	sbQuery.append(" and po.incharge_type=:indOne ");
    	sbQuery.append(" and po.role=:indeTwo ");
    	sbQuery.append("     and (po.operation_part=:operation_part and mo.cash_settlement_date between trunc(:initial_date) and trunc(:end_date) ");
    	sbQuery.append("   or (po.operation_part=:operation_part and mo.term_settlement_date between trunc(:initial_date) and trunc(:end_date))) ");     

     
        if(mechanism_type!=null){
        	sbQuery.append(" and mo.id_negotiation_mechanism_fk = :mechanism_type ");
        	params.put("mechanism_type", mechanism_type);
        } 
        if(modality_type!=null){
        	sbQuery.append(" and mo.id_negotiation_modality_fk = :modality_type ");
        	params.put("modality_type", modality_type);
        }
        if(participant_type!=null){
        	sbQuery.append(" and po.id_participant_fk=:participant_type ");
        }
         	params.put("initial_date", initialDate);
         	params.put("end_date",CommonsUtilities.currentDate());
         	params.put("indOne",1);        
         	params.put("indeTwo",2);
         	params.put("operation_part", 1);

        
  
        										
        return findByNativeQuery(sbQuery.toString(), params);
    	
    }
    
    
    
    public List<Object[]> getSettlementPositionByParticipant (Integer currencyType, Long mechanismType, Date settlementDate){
		StringBuilder sbQuery = new StringBuilder();
		HashMap<String, Object> params = new HashMap<String,Object>();
		sbQuery.append(" select distinct ");
		sbQuery.append(" po.role, ");
		sbQuery.append(" po.ID_PARTICIPANT_FK as pa_pk, ");
		sbQuery.append(" p.MNEMONIC as pa_nmemonic ,");
		sbQuery.append(" p.DESCRIPTION as pa_desc, ");
		sbQuery.append(" po.ID_MODALITY_GROUP_FK as mod_group, ");
		sbQuery.append(" nme.id_negotiation_mechanism_pk, ");
		sbQuery.append(" nme.mechanism_name as mech_name,");
		sbQuery.append(" nmo.modality_code as mod_code, ");
		sbQuery.append(" nmo.modality_name as mod_name, ");
		sbQuery.append(" mg.GROUP_NAME as group_name, ");
		sbQuery.append(" mo.SETTLEMENT_SCHEMA as schema_type,");
		sbQuery.append(" mo.CURRENCY as currency,");
		sbQuery.append(" (select pt.indicator1 || ' ' || pt.parameter_name from parameter_table pt where pt.parameter_table_pk = mo.currency ) as currency_des,");
		sbQuery.append(" nvl(sum (po.cash_amount),0) as monto ");
		sbQuery.append(" from ");
		sbQuery.append("  PARTICIPANT_OPERATION po, Participant p, MODALITY_GROUP_DETAIL mgd, ");
		sbQuery.append("  MODALITY_GROUP mg, NEGOTIATION_MECHANISM nme, negotiation_modality nmo, MECHANISM_OPERATION mo");
		sbQuery.append(" where");
		sbQuery.append(" po.ID_PARTICIPANT_FK=p.ID_PARTICIPANT_PK ");
		sbQuery.append(" and po.ID_MODALITY_GROUP_FK=mg.ID_MODALITY_GROUP_PK ");
		sbQuery.append(" and mo.ID_NEGOTIATION_MECHANISM_FK=mgd.ID_NEGOTIATION_MECHANISM_PK ");
		sbQuery.append(" and mo.ID_NEGOTIATION_MODALITY_FK=mgd.ID_NEGOTIATION_MODALITY_PK ");
		sbQuery.append(" and mg.ID_MODALITY_GROUP_PK=mg.ID_MODALITY_GROUP_PK ");
		sbQuery.append(" and mgd.ID_NEGOTIATION_MECHANISM_PK=nme.ID_NEGOTIATION_MECHANISM_PK ");
		sbQuery.append(" and mgd.id_negotiation_modality_pk =nmo.id_negotiation_modality_pk ");
		sbQuery.append(" and po.ID_MECHANISM_OPERATION_FK=mo.ID_MECHANISM_OPERATION_PK ");
		sbQuery.append(" and mo.ID_NEGOTIATION_MECHANISM_FK= :mechanismType ");
        sbQuery.append(" and mo.CURRENCY= :currency ");
		sbQuery.append("  ");
		sbQuery.append(" and po.incharge_type= :inchargeType ");
		sbQuery.append(" and mo.SETTLEMENT_TYPE= :settlementType ");
		sbQuery.append(" and (");
		sbQuery.append("     ( mo.OPERATION_STATE in ( :assignedState , :cashSettledState )  ");
		sbQuery.append("     and po.OPERATION_PART= 1 ");
		sbQuery.append("     and ( ");
		sbQuery.append("       (mo.IND_CASH_EXTENDED=0 and mo.IND_CASH_PREPAID=0 and trunc(mo.CASH_SETTLEMENT_DATE)=:settlementDate ) or ");
		sbQuery.append("       (mo.IND_CASH_EXTENDED=1 and trunc(mo.CASH_EXTENDED_DATE)= :settlementDate ) or ");
		sbQuery.append("       (mo.IND_CASH_PREPAID=1 and trunc(mo.CASH_PREPAID_DATE)= :settlementDate ) ");
		sbQuery.append("     ) ");
		sbQuery.append("   ) ");
		sbQuery.append("   or(  mo.OPERATION_STATE in (:cashSettledState , :termSettledState ) ");
		sbQuery.append("     and po.OPERATION_PART= 2  ");
		sbQuery.append("     and (");
		sbQuery.append("       (mo.IND_TERM_EXTENDED=0 and mo.IND_TERM_PREPAID=0 and trunc(mo.TERM_SETTLEMENT_DATE)=:settlementDate) or ");
		sbQuery.append("       (mo.IND_TERM_EXTENDED=1 and trunc(mo.TERM_SETTLEMENT_DATE)=:settlementDate) or ");
		sbQuery.append("       (mo.IND_TERM_PREPAID=1 and trunc(mo.TERM_SETTLEMENT_DATE)=:settlementDate) ");
		sbQuery.append("     )");
		sbQuery.append("   )  ");
		sbQuery.append(" )    ");
		sbQuery.append(" group by");
		sbQuery.append(" po.role, po.ID_PARTICIPANT_FK, p.DESCRIPTION, po.ID_MODALITY_GROUP_FK,nme.id_negotiation_mechanism_pk, nme.mechanism_name,    ");
		sbQuery.append(" nmo.modality_code, nmo.modality_name, mg.GROUP_NAME, mo.SETTLEMENT_SCHEMA, mo.CURRENCY, nme.DESCRIPTION, p.MNEMONIC     ");
		sbQuery.append(" order by   ");
		sbQuery.append(" nme.id_negotiation_mechanism_pk,nmo.modality_code, mo.CURRENCY, po.role asc  ");
        	
        params.put("currency", currencyType);
		params.put("inchargeType", InChargeNegotiationType.INCHARGE_FUNDS.getCode());
		params.put("settlementType", SettlementType.DVP.getCode());
		params.put("assignedState",MechanismOperationStateType.ASSIGNED_STATE.getCode());
		params.put("cashSettledState",MechanismOperationStateType.CASH_SETTLED.getCode());
		params.put("termSettledState",MechanismOperationStateType.TERM_SETTLED.getCode());
		params.put("mechanismType",mechanismType);
		params.put("settlementDate",settlementDate);
		
        return findByNativeQuery(sbQuery.toString(), params);
	}
    		
    
    public Object[] getMechanismDetails(Long idMechanism){
    	StringBuilder sbQuery = new StringBuilder();
    	HashMap<String, Object> params = new HashMap<String,Object>();
    	
    	sbQuery.append(" SELECT");
    	sbQuery.append(" mech.idNegotiationMechanismPk, ");
    	sbQuery.append(" mech.mechanismName ");
    	sbQuery.append(" from NegotiationMechanism mech where mech.idNegotiationMechanismPk = :pk ");
    	
    	params.put("pk", idMechanism);
    	
    	return (Object[]) findObjectByQueryString(sbQuery.toString(), params);
    }
    
    public List<Object[]> getNonfulfillmentOperations(MechanismOperationTO filterTO){
    	StringBuilder sbQuery = new StringBuilder();
    	HashMap<String, Object> parameters = new HashMap<String,Object>();
    	
    	sbQuery.append(" SELECT TO_CHAR(MO.REGISTER_DATE,'dd/mm/yyyy') AS OPERATION_DATE,");					  //[0]
    	sbQuery.append("		NMECH.DESCRIPTION AS MECHANISM, ");												  //[1]
    	sbQuery.append(" 		NMOD.MODALITY_NAME, ");									    					  //[2]
    	sbQuery.append(" 		MO.OPERATION_NUMBER, ");														  //[3]
    	sbQuery.append(" 		ISS.MNEMONIC AS ISSUER, ");														  //[4]
    	sbQuery.append(" 		SEC.ID_ISIN_CODE_PK, ");														  //[5]
    	sbQuery.append(" 		OU.UNFULFILLED_QUANTITY AS CANTIDAD_VALORES, ");								  //[6]
    	sbQuery.append(" 		OU.UNFULFILLED_AMOUNT MONTO_LIQUIDADO, "); 										  //[7]
    	sbQuery.append(" 		PT_MOTIVE.PARAMETER_NAME AS MOTIVO, ");											  //[8]
    	sbQuery.append(" 		PA_SELLER.MNEMONIC AS PART_SELLER, ");											  //[9]
    	sbQuery.append(" 		PA_BUYER.MNEMONIC AS PART_BUYER ");												  //[10]		
    	sbQuery.append("  FROM OPERATION_UNFULFILLMENT OU");
    	sbQuery.append(" INNER JOIN MECHANISM_OPERATION MO");
    	sbQuery.append("	 ON MO.ID_MECHANISM_OPERATION_PK = OU.ID_MECHANISM_OPERATION_FK");
    	sbQuery.append(" INNER JOIN NEGOTIATION_MECHANISM NMECH");
    	sbQuery.append("     ON NMECH.ID_NEGOTIATION_MECHANISM_PK = MO.ID_NEGOTIATION_MECHANISM_FK");
    	sbQuery.append(" INNER JOIN NEGOTIATION_MODALITY NMOD");
    	sbQuery.append("     ON NMOD.ID_NEGOTIATION_MODALITY_PK = MO.ID_NEGOTIATION_MODALITY_FK");
    	sbQuery.append(" INNER JOIN SECURITY SEC");
    	sbQuery.append("     ON SEC.ID_ISIN_CODE_PK = MO.ID_ISIN_CODE_FK");
    	sbQuery.append(" INNER JOIN ISSUER ISS");
    	sbQuery.append("     ON ISS.ID_ISSUER_PK = SEC.ID_ISSUER_FK");
    	sbQuery.append(" INNER JOIN PARAMETER_TABLE PT_MOTIVE");
    	sbQuery.append("     ON PT_MOTIVE.PARAMETER_TABLE_PK = OU.UNFULFILLMENT_REASON");
    	sbQuery.append(" INNER JOIN PARTICIPANT PA_SELLER");
    	sbQuery.append("     ON PA_SELLER.ID_PARTICIPANT_PK = MO.ID_SELLER_PARTICIPANT_FK");
    	sbQuery.append(" INNER JOIN PARTICIPANT PA_BUYER");
    	sbQuery.append("     ON PA_BUYER.ID_PARTICIPANT_PK = MO.ID_BUYER_PARTICIPANT_FK");
    	sbQuery.append("  WHERE OU.SETTLEMENT_DATE = :settlementDate");
    	
    	parameters.put("settlementDate", filterTO.getSettlementDate());
    	
    	if(Validations.validateIsNotNull(filterTO.getIdNegotiationMechanismPk())){
    		sbQuery.append("    AND NMECH.ID_NEGOTIATION_MECHANISM_PK = :negotiationMechanismPk");
    		parameters.put("negotiationMechanismPk", filterTO.getIdNegotiationMechanismPk());
    	}
    	
    	if(Validations.validateIsNotNull(filterTO.getIdNegotiationModalityPk())){
    		sbQuery.append("    AND NMOD.ID_NEGOTIATION_MODALITY_PK = :negotiationModalityPk");
    		parameters.put("negotiationModalityPk", filterTO.getIdNegotiationModalityPk());
    	}
    	if(Validations.validateIsNotNull(filterTO.getCurrency())){    	
    		sbQuery.append("    AND MO.CURRENCY = :currency");
    		parameters.put("currency", filterTO.getCurrency());
    	}
    	sbQuery.append(" ORDER BY NMECH.DESCRIPTION,");
    	sbQuery.append(" NMOD.MODALITY_NAME,");
    	sbQuery.append(" MO.OPERATION_NUMBER,");
    	sbQuery.append(" MO.REGISTER_DATE");
    	
    	return findByNativeQuery(sbQuery.toString(), parameters);
    }

	public List<Object[]> getFinishedNegotiationOperationList(
			MechanismOperationTO mechanismOperationTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
				
		sbQuery.append("Select ").
		append(" mo.id_mechanism_operation_pk, ").
		append(" mo.id_negotiation_mechanism_fk, ").
		append(" negmech.mechanism_name, ").
		append(" mo.id_negotiation_modality_fk, ").
		append(" negmod.modality_name, ").
		append(" mo.operation_number, ").
		append(" mo.operation_date, ").
		append(" mo.real_cash_settlement_date, ").
		append(" mo.real_term_settlement_date, ").
		append(" mo.id_isin_code_fk, ").
		append(" mo.settlement_schema, ").
		append(" mo.settlement_type, ").
		append(" mo.stock_quantity, ").		
		append(" mo.term_amount, ").
		append(" pbuy.mnemonic || ' - ' || pbuy.id_participant_pk as participant_buyer, ").
		append(" psell.mnemonic || ' - ' || psell.id_participant_pk as participant_seller ").
		append(" From ").
		append(" Mechanism_Operation mo ").
		append(" Inner Join Participant pbuy on mo.id_buyer_participant_fk = pbuy.id_participant_pk ").
		append(" Inner Join Participant psell on mo.id_seller_participant_fk = psell.id_participant_pk ").
		append(" Inner Join Negotiation_Mechanism negmech on mo.id_negotiation_mechanism_fk = negmech.id_negotiation_mechanism_pk ").
		append(" Inner Join Negotiation_Modality negmod on mo.id_negotiation_modality_fk = negmod.id_negotiation_modality_pk ").
		append(" Where ").
		append(" mo.operation_state = :operationStatePrm ").
		append(" And mo.id_negotiation_modality_fk in (:reportFixedPrm,:reportEquitiesPrm) ").	
		append(" And Trunc(mo.real_term_settlement_date) between :initialRealTermSettlementDatePrm and :endRealTermSettlementDatePrm ");
		
		if(Validations.validateIsNotNullAndPositive(mechanismOperationTO.getIdNegotiationMechanismPk())) {
			sbQuery.append(" And mo.id_negotiation_mechanism_fk = :negMechanismPrm ");
		}
		
		if(Validations.validateIsNotNullAndPositive(mechanismOperationTO.getIdNegotiationModalityPk())) {
			sbQuery.append(" And mo.id_negotiation_modality_fk = :negModalityPrm ");
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(mechanismOperationTO.getIdSecurityCodePk())) {
			sbQuery.append(" And mo.id_isin_code_fk = :isinCodePrm ");
		}
		
		if(Validations.validateIsNotNullAndPositive(mechanismOperationTO.getSettlementSchema())) {
			sbQuery.append(" And mo.settlement_schema = :settlementSchemaPrm ");
		}
		
		if(Validations.validateIsNotNullAndPositive(mechanismOperationTO.getSettlementType())) {
			sbQuery.append(" And mo.settlement_type = :settlementTypePrm ");
		}
		
		sbQuery.append(" Order by mo.id_negotiation_mechanism_fk, mo.id_negotiation_modality_fk, mo.operation_date  ");
		
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("operationStatePrm", MechanismOperationStateType.TERM_SETTLED.getCode());
		query.setParameter("reportFixedPrm", NegotiationModalityType.REPORT_FIXED_INCOME.getCode());
		query.setParameter("reportEquitiesPrm", NegotiationModalityType.REPORT_EQUITIES.getCode());
		query.setParameter("initialRealTermSettlementDatePrm", mechanismOperationTO.getInitialRealTermSettlementDate(), TemporalType.DATE);
		query.setParameter("endRealTermSettlementDatePrm", mechanismOperationTO.getEndRealTermSettlementDate(), TemporalType.DATE);
		
		if(Validations.validateIsNotNullAndPositive(mechanismOperationTO.getIdNegotiationMechanismPk())) {			
			query.setParameter("negMechanismPrm", mechanismOperationTO.getIdNegotiationMechanismPk());
		}
		
		if(Validations.validateIsNotNullAndPositive(mechanismOperationTO.getIdNegotiationModalityPk())) {			
			query.setParameter("negModalityPrm", mechanismOperationTO.getIdNegotiationModalityPk());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(mechanismOperationTO.getIdSecurityCodePk())) {			
			query.setParameter("isinCodePrm", mechanismOperationTO.getIdSecurityCodePk());
		}
		
		if(Validations.validateIsNotNullAndPositive(mechanismOperationTO.getSettlementSchema())) {			
			query.setParameter("settlementSchemaPrm", mechanismOperationTO.getSettlementSchema());
		}
		
		if(Validations.validateIsNotNullAndPositive(mechanismOperationTO.getSettlementType())) {			
			query.setParameter("settlementTypePrm", mechanismOperationTO.getSettlementType());
		}
				
		return (List<Object[]>)query.getResultList();
	}
	
	public List<Object[]> getTransferBuySaleByParameters(
			Long mechanism_type, Long modality_type,String issuer,String isinCode, Date initialDate,Date finalDate){
    	
    	StringBuilder sbQuery = new StringBuilder();
    	
    	sbQuery.append(" SELECT");
    	sbQuery.append(" MO.operation_number as operation_number,");
        sbQuery.append(" MO.operation_state as operation_state,");
        sbQuery.append(" HA.account_number as account_number,");
        sbQuery.append(" H.id_holder_pk as rnt_code,");
        sbQuery.append(" H.holder_type as holder_type,");
        sbQuery.append(" H.name as name,");
        sbQuery.append(" H.first_last_name as first_last_name,");
        sbQuery.append(" H.second_last_name as second_last_name,");
        sbQuery.append(" H.full_name as full_name,");
        sbQuery.append(" HAO.role as role,");
        sbQuery.append(" HAO.stock_quantity as quantity,");
        sbQuery.append(" P2.mnemonic || ' - ' || P2.id_participant_pk as participant_vendedor,");
        sbQuery.append(" MO.cash_settlement_date as cash_date,");
        sbQuery.append(" MO.term_settlement_date as term_date");
        sbQuery.append(" FROM MECHANISM_OPERATION MO");
        sbQuery.append(" INNER JOIN PARTICIPANT P2 ON MO.id_seller_participant_fk = P2.id_participant_pk");
        sbQuery.append(" INNER JOIN HOLDER_ACCOUNT_OPERATION HAO ON MO.id_mechanism_operation_pk = HAO.id_mechanism_operation_fk");
        sbQuery.append(" INNER JOIN HOLDER_ACCOUNT  HA ON HA.id_holder_account_pk = HAO.id_holder_account_fk");
        sbQuery.append(" INNER JOIN HOLDER H ON HA.id_participant_fk = H.id_participant_fk");
        sbQuery.append(" WHERE 1=1 ");
        if(issuer!=null){
        	sbQuery.append(" INNER JOIN SECURITY SE ON SE.id_isin_code_pk = MO.id_isin_code_fk ");
        }
        if(Validations.validateIsNotNull(mechanism_type)){
        	sbQuery.append(" AND MO.id_negotiation_mechanism_fk = :mechanism_type");
        }
        if(modality_type!=null){
        	sbQuery.append("  AND MO.id_negotiation_modality_fk = :modality_type");
        }
        
        if(issuer!=null){
        	sbQuery.append(" AND SE.id_issuer_fk = :id_issuer ");
        }
        
        if(isinCode!=null){
        	sbQuery.append(" AND MO.id_isin_code_fk = :isinCode");    	
        }
        
        sbQuery.append(" AND trunc(MO.operation_date) between :initial_date And  :final_date");
        
        sbQuery.append(" GROUP BY");
        sbQuery.append(" MO.operation_number,");
        sbQuery.append(" MO.operation_state,");
        sbQuery.append(" HA.account_number,");
        sbQuery.append(" H.id_holder_pk,");
        sbQuery.append(" H.holder_type,");
        sbQuery.append(" H.name,");
        sbQuery.append(" H.first_last_name,");
        sbQuery.append(" H.second_last_name,");
        sbQuery.append(" H.full_name,");
        sbQuery.append(" HAO.role,");
        sbQuery.append(" HAO.stock_quantity,");
        sbQuery.append(" P2.mnemonic || ' - ' || P2.id_participant_pk,");
        sbQuery.append(" MO.cash_settlement_date,");
        sbQuery.append(" MO.term_settlement_date "); 
        sbQuery.append(" ORDER BY ");
        sbQuery.append(" MO.operation_number,");
        sbQuery.append(" HAO.role asc");
        																										
    	 Query query = em.createNativeQuery(sbQuery.toString());
    	 if(modality_type!=null){
    		 query.setParameter("mechanism_type",mechanism_type);
    	 }
    	 if(modality_type!=null){
    		 query.setParameter("modality_type",modality_type);
    	 }
    	 
         if(issuer!=null){
        	 query.setParameter("id_issuer",issuer);
         }
         
         if(isinCode!=null){
        	 query.setParameter("isinCode",isinCode);
         }
    	 
         query.setParameter("initial_date",initialDate);
         
         query.setParameter("final_date",finalDate);
             	 
    	 return query.getResultList();
    	
    }
	
	public String getRegEarlySettlementReportedSirtex(GenericNegotiationTO gfto){
    	StringBuilder sbQuery = new StringBuilder();

    	//COMPRA
		if(gfto.getReportOf().equals("1") || gfto.getReportOf().equals("2")){
	    	sbQuery.append(" SELECT "); 
	    	sbQuery.append("   SOP.ID_SETTLEMENT_OPERATION_PK SOLICITUD, ");
	    	sbQuery.append("   (SELECT P.MNEMONIC || ' - ' || P.ID_PARTICIPANT_PK || ' - ' || P.DESCRIPTION FROM PARTICIPANT P WHERE P.ID_PARTICIPANT_PK=OP.ID_SELLER_PARTICIPANT_FK) PARTICIPANT, ");
	    	if(gfto.getFormatBcb().equals("1")){
	        	sbQuery.append(" SEC.ID_SECURITY_CODE_PK VALOR, ");
	    	}else if(gfto.getFormatBcb().equals("2")){
	    		//issue 672 (si no tuviera codigo bcb entonces se lo genera para el reporte)
	    		sbQuery.append(" CASE WHEN SEC.ID_SECURITY_BCB_CODE IS NOT NULL THEN SEC.ID_SECURITY_BCB_CODE ELSE CASE WHEN SEC.SECURITY_CLASS = 420 THEN SEC.ID_SECURITY_CODE_PK ELSE '('||SUBSTR(SEC.ID_SECURITY_CODE_PK ,0,2)||')'||SUBSTR(SEC.ID_SECURITY_CODE_PK ,5)||'-000' END END VALOR, ");
	        	//sbQuery.append(" SEC.ID_SECURITY_BCB_CODE VALOR, ");
	    	}
	    	sbQuery.append("   CASE SOP.IND_PARTIAL WHEN 0 THEN 'TOTAL' ELSE 'PARCIAL' END AS  TIPO_LIQ, "); 
	    	sbQuery.append("   OP.OPERATION_DATE FECHA_OPERACION, ");
	    	sbQuery.append("   OP.TERM_SETTLEMENT_DAYS AS PLAZO, "); 
	    	sbQuery.append("   OP.STOCK_QUANTITY CANTIDAD, ");
	    	sbQuery.append("   OP.TERM_SETTLEMENT_DATE VTO_TEORICO, ");
	    	sbQuery.append("   SOP.SETTLEMENT_DATE FEC_LIQ_ANTICIPADA, ");
	    	sbQuery.append("   SOP.SETTLED_QUANTITY NRO_TITULOS_ANTICIPADOS, ");
	    	sbQuery.append("   DECODE(SAO.ROLE, 1,'LIQUIDACION DE COMPRA RPT', 2, 'LIQUIDACION DE VENTA RPT') AS ROLE, ");
	    	sbQuery.append("   SEC.SECURITY_CLASS SECURITY_CLASS, ");
	    	sbQuery.append("   SEC.ID_SECURITY_CODE_PK ID_SECURITY_CODE_PK ");
	    	sbQuery.append(" FROM MECHANISM_OPERATION OP "); 
	    	sbQuery.append("   INNER JOIN SETTLEMENT_OPERATION SOP ON SOP.ID_MECHANISM_OPERATION_FK = OP.ID_MECHANISM_OPERATION_PK  ");
			sbQuery.append("   INNER JOIN SETTLEMENT_ACCOUNT_OPERATION SAO ON SAO.ID_SETTLEMENT_OPERATION_FK = SOP.ID_SETTLEMENT_OPERATION_PK ");
			sbQuery.append("   INNER JOIN SECURITY SEC ON SEC.ID_SECURITY_CODE_PK = OP.ID_SECURITY_CODE_FK  ");
			sbQuery.append(" WHERE OP.ID_NEGOTIATION_MECHANISM_FK = 5 ");
			sbQuery.append("   AND OP.IND_REPORTING_BALANCE  = 1 ");
			sbQuery.append("   AND SOP.OPERATION_STATE = 616  ");
//			sbQuery.append("   AND OP.ID_BUYER_PARTICIPANT_FK = 114 ");//issue 672 informacion con participante BCB
			sbQuery.append("   AND SAO.ROLE = 2  ");
			sbQuery.append("   AND SOP.OPERATION_PART = 2 ");
			sbQuery.append("   AND OP.TERM_SETTLEMENT_DATE <> SOP.SETTLEMENT_DATE ");
			sbQuery.append("   AND trunc(SOP.SETTLEMENT_DATE) BETWEEN TO_DATE('"+gfto.getInitialDate()+"','dd/MM/yyyy') AND TO_DATE('"+gfto.getFinalDate()+"','dd/MM/yyyy') ");

			if(gfto.getFormatBcb().equals("2")){
				// issue 672 (se comenta esta condicion para que aparezcan todos los valores tengan o no codigo bcb)
	        	//sbQuery.append(" AND SEC.ID_SECURITY_BCB_CODE IS NOT NULL ");
	    	}
			if(Validations.validateIsNotNull(gfto.getParticipant())){
				sbQuery.append(" AND OP.ID_SELLER_PARTICIPANT_FK = "+gfto.getParticipant()+" ");
			}
		}
		//COMPRA-VENTA
		if(gfto.getReportOf().equals("2")){
			sbQuery.append(" UNION ALL ");
		}
		//VENTA
		if(gfto.getReportOf().equals("0") || gfto.getReportOf().equals("2")){
	    	sbQuery.append(" SELECT "); 
	    	sbQuery.append("   SOP.ID_SETTLEMENT_OPERATION_PK SOLICITUD, ");
	    	sbQuery.append("   (SELECT P.MNEMONIC || ' - ' || P.ID_PARTICIPANT_PK || ' - ' || P.DESCRIPTION FROM PARTICIPANT P WHERE P.ID_PARTICIPANT_PK=OP.ID_BUYER_PARTICIPANT_FK) PARTICIPANT, ");
	    	if(gfto.getFormatBcb().equals("1")){
	        	sbQuery.append(" SEC.ID_SECURITY_CODE_PK VALOR, ");
	    	}else if(gfto.getFormatBcb().equals("2")){
	    		//issue 672 (si no tuviera codigo bcb entonces se lo genera para el reporte)
	    		sbQuery.append(" CASE WHEN SEC.ID_SECURITY_BCB_CODE IS NOT NULL THEN SEC.ID_SECURITY_BCB_CODE ELSE CASE WHEN SEC.SECURITY_CLASS = 420 THEN SEC.ID_SECURITY_CODE_PK ELSE '('||SUBSTR(SEC.ID_SECURITY_CODE_PK ,0,2)||')'||SUBSTR(SEC.ID_SECURITY_CODE_PK ,5)||'-000' END END VALOR, ");
	        	//sbQuery.append(" SEC.ID_SECURITY_BCB_CODE VALOR, ");
	    	}
	    	sbQuery.append("   CASE SOP.IND_PARTIAL WHEN 0 THEN 'TOTAL' ELSE 'PARCIAL' END AS  TIPO_LIQ, "); 
	    	sbQuery.append("   OP.OPERATION_DATE FECHA_OPERACION, ");
	    	sbQuery.append("   OP.TERM_SETTLEMENT_DAYS AS PLAZO, "); 
	    	sbQuery.append("   OP.STOCK_QUANTITY CANTIDAD, ");
	    	sbQuery.append("   OP.TERM_SETTLEMENT_DATE VTO_TEORICO, ");
	    	sbQuery.append("   SOP.SETTLEMENT_DATE FEC_LIQ_ANTICIPADA, ");
	    	sbQuery.append("   SOP.SETTLED_QUANTITY NRO_TITULOS_ANTICIPADOS, ");
	    	sbQuery.append("   DECODE(SAO.ROLE, 1,'LIQUIDACION DE COMPRA RPT', 2, 'LIQUIDACION DE VENTA RPT') AS ROLE, ");
	    	sbQuery.append("   SEC.SECURITY_CLASS SECURITY_CLASS, ");
	    	sbQuery.append("   SEC.ID_SECURITY_CODE_PK ID_SECURITY_CODE_PK ");
	    	sbQuery.append(" FROM MECHANISM_OPERATION OP "); 
	    	sbQuery.append("   INNER JOIN SETTLEMENT_OPERATION SOP ON SOP.ID_MECHANISM_OPERATION_FK = OP.ID_MECHANISM_OPERATION_PK  ");
			sbQuery.append("   INNER JOIN SETTLEMENT_ACCOUNT_OPERATION SAO ON SAO.ID_SETTLEMENT_OPERATION_FK = SOP.ID_SETTLEMENT_OPERATION_PK ");
			sbQuery.append("   INNER JOIN SECURITY SEC ON SEC.ID_SECURITY_CODE_PK = OP.ID_SECURITY_CODE_FK  ");
			sbQuery.append(" WHERE OP.ID_NEGOTIATION_MECHANISM_FK = 5 ");
			sbQuery.append("   AND OP.IND_REPORTING_BALANCE = 1 ");
			sbQuery.append("   AND SOP.OPERATION_STATE = 616  ");
//			sbQuery.append("   AND OP.ID_SELLER_PARTICIPANT_FK = 114 ");//issue 672 informacion con participante BCB
			sbQuery.append("   AND SAO.ROLE = 1  ");
			sbQuery.append("   AND SOP.OPERATION_PART = 2 ");
			sbQuery.append("   AND OP.TERM_SETTLEMENT_DATE <> SOP.SETTLEMENT_DATE ");
			sbQuery.append("   AND trunc(SOP.SETTLEMENT_DATE) BETWEEN TO_DATE('"+gfto.getInitialDate()+"','dd/MM/yyyy') AND TO_DATE('"+gfto.getFinalDate()+"','dd/MM/yyyy') ");

			if(gfto.getFormatBcb().equals("2")){
				// issue 672 (se comenta esta condicion para que aparezcan todos los valores tengan o no codigo bcb)
	        	// sbQuery.append(" AND SEC.ID_SECURITY_BCB_CODE IS NOT NULL ");
	    	}
			if(Validations.validateIsNotNull(gfto.getParticipant())){
				sbQuery.append(" AND OP.ID_BUYER_PARTICIPANT_FK = "+gfto.getParticipant()+" ");
			}
		}
		sbQuery.append(" ORDER BY SECURITY_CLASS,FEC_LIQ_ANTICIPADA,ROLE ");
    	sbQuery.append("  ");
    	
		return sbQuery.toString();
	}
	
	public String getRegShoppingOngoingSalesOpReportedSirtex(GenericNegotiationTO gfto){
    	StringBuilder sbQuery = new StringBuilder();

    	sbQuery.append(" SELECT "); 
    	sbQuery.append(" TO_CHAR(OP.REGISTER_DATE,'dd/MM/yyyy hh:mm:ss') FECHA_HORA, ");
    	sbQuery.append(" REQ.ID_TRADE_REQUEST_PK SOLICITUD, ");
    	sbQuery.append(" (SELECT P.MNEMONIC || ' - ' || P.ID_PARTICIPANT_PK || ' - ' || P.DESCRIPTION FROM PARTICIPANT P WHERE P.ID_PARTICIPANT_PK=PO.ID_PARTICIPANT_FK) PARTICIPANT, ");
    	if(gfto.getFormatBcb().equals("1")){
        	sbQuery.append(" SEC.ID_SECURITY_CODE_PK VALOR, ");
    	}else if(gfto.getFormatBcb().equals("2")){
        	sbQuery.append(" SEC.ID_SECURITY_BCB_CODE VALOR, ");
    	}
    	sbQuery.append(" OP.STOCK_QUANTITY CANTIDAD, ");
    	sbQuery.append(" SO.SETTLEMENT_DATE VTO_TEORICO, ");
    	sbQuery.append(" DECODE(PO.ROLE, 1,'COMPRAS EN REPORTO AL BCB', 2, 'VENTAS EN REPORTO AL BCB') AS ROLE ");
    	sbQuery.append(" FROM MECHANISM_OPERATION OP ");
    	sbQuery.append(" INNER JOIN MECHANISM_MODALITY MMOD ON MMOD.ID_NEGOTIATION_MODALITY_PK = OP.ID_NEGOTIATION_MODALITY_FK ");
    	sbQuery.append(" INNER JOIN SETTLEMENT_OPERATION SO ON SO.ID_MECHANISM_OPERATION_FK = OP.ID_MECHANISM_OPERATION_PK ");
		sbQuery.append(" INNER JOIN PARTICIPANT_SETTLEMENT PO ON SO.ID_SETTLEMENT_OPERATION_PK=PO.ID_SETTLEMENT_OPERATION_FK ");
		sbQuery.append(" INNER JOIN SECURITY SEC ON SEC.ID_SECURITY_CODE_PK=OP.ID_SECURITY_CODE_FK ");
		sbQuery.append(" INNER JOIN TRADE_OPERATION TRAD ON TRAD.ID_TRADE_OPERATION_PK = OP.ID_MECHANISM_OPERATION_PK ");
		sbQuery.append(" INNER JOIN TRADE_REQUEST REQ ON REQ.ID_TRADE_REQUEST_PK = TRAD.ID_TRADE_REQUEST_FK ");
		sbQuery.append(" WHERE MMOD.ID_NEGOTIATION_MECHANISM_PK=5 AND mmod.id_negotiation_modality_pk=3 AND SO.OPERATION_PART=2");

		if(gfto.getFormatBcb().equals("2")){
        	sbQuery.append(" AND SEC.ID_SECURITY_BCB_CODE IS NOT NULL ");
    	}
		if(Validations.validateIsNotNull(gfto.getParticipant())){
			sbQuery.append(" AND PO.ID_PARTICIPANT_FK="+gfto.getParticipant()+" ");
		}
		if(gfto.getReportOf().equals("0")){
			sbQuery.append(" AND PO.ROLE=2 ");
		}
		if(gfto.getReportOf().equals("1")){
			sbQuery.append(" AND PO.ROLE=1 ");
		}
    	sbQuery.append(" ORDER BY PO.ROLE,OP.REGISTER_DATE,SEC.SECURITY_CLASS ");
    	sbQuery.append("  ");
    	
		return sbQuery.toString();
	}
	
	public String getQueryTransferOPeration(TransferOperationTO transferOperationTO){
    	StringBuilder sbQuery = new StringBuilder();

    	sbQuery.append("  select		distinct	MO.ID_MECHANISM_OPERATION_PK,	 ");									
    	sbQuery.append("  CASE WHEN so.OPERATION_PART = 1 THEN 'CONTADO'					 ");									
    	sbQuery.append("       WHEN so.OPERATION_PART = 2 THEN 'PLAZO'					 ");									
    	sbQuery.append("  END PARTE_OP,					 ");									
    	sbQuery.append("  MO.OPERATION_NUMBER NRO_OPERACION,					 ");									
    	sbQuery.append("  to_char(SO.SETTLEMENT_DATE,'DD/MM/YYYY')  FECHA,					 ");									
    	sbQuery.append("  SEC.ID_SECURITY_CODE_PK VALOR,					 ");									
    	sbQuery.append("  SEC.ALTERNATIVE_CODE VALOR_ALT,					 ");									
    	sbQuery.append("  MO.STOCK_QUANTITY CANTIDAD ,					 ");									
    	sbQuery.append("  'VENTA' ROL_ORIGEN,					 ");									
        sbQuery.append(" CASE WHEN MO.ID_NEGOTIATION_MODALITY_FK IN (1,2) THEN SELLER.MNEMONIC 		 ");
        sbQuery.append("     WHEN MO.ID_NEGOTIATION_MODALITY_FK IN (3,4) AND so.OPERATION_PART=1 THEN SELLER.MNEMONIC 		 ");
        sbQuery.append("     WHEN MO.ID_NEGOTIATION_MODALITY_FK IN (3,4) AND so.OPERATION_PART=2 THEN BUYER.MNEMONIC 		 ");
        sbQuery.append(" END  PAR_ORIGEN,				 ");
        
        sbQuery.append("(SELECT LISTAGG(DOCUMENT_NUMBER,'<br>' )  WITHIN GROUP (ORDER BY DOCUMENT_NUMBER)                    ");       
        sbQuery.append("      FROM ( SELECT distinct H_SUB.DOCUMENT_NUMBER                                                   ");                    
        sbQuery.append("             FROM                                                                                    ");
        sbQuery.append("                 HOLDER_ACCOUNT_OPERATION HAO_SUB,                                                   ");
        sbQuery.append("                 HOLDER_ACCOUNT_DETAIL HAD_SUB,                                                      ");
        sbQuery.append("                 HOLDER H_SUB                                                                        ");
        sbQuery.append("                 ,MECHANISM_OPERATION MO, HOLDER_ACCOUNT_OPERATION haos                               ");
        sbQuery.append("             WHERE 1 = 1                                                                             ");                 
        sbQuery.append("                 AND HAD_SUB.ID_HOLDER_ACCOUNT_FK = HAO_SUB.ID_HOLDER_ACCOUNT_FK                     ");                     
        sbQuery.append("                 AND H_SUB.ID_HOLDER_PK = HAD_SUB.ID_HOLDER_FK                                       ");                     
        sbQuery.append("                 AND HAO_SUB.HOLDER_ACCOUNT_STATE = 625                                              ");                     
        sbQuery.append("                 AND HAO_SUB.ROLE = 2                                                                ");                     
        sbQuery.append("                 AND HAO_SUB.OPERATION_PART = haos.OPERATION_PART                                    ");                      
        sbQuery.append("                 AND HAO_SUB.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK)) NIT_ORIGEN,  ");
        sbQuery.append("(SELECT LISTAGG(ID_HOLDER_PK,'<br>' )  WITHIN GROUP (ORDER BY ID_HOLDER_PK)                          "); 
        sbQuery.append("      FROM ( SELECT distinct H_SUB.ID_HOLDER_PK                                                      ");                 
        sbQuery.append("             FROM                                                                                    ");
        sbQuery.append("                 HOLDER_ACCOUNT_OPERATION HAO_SUB,                                                   ");
        sbQuery.append("                 HOLDER_ACCOUNT_DETAIL HAD_SUB,                                                      ");
        sbQuery.append("                 HOLDER H_SUB                                                                        ");
        sbQuery.append("                 ,MECHANISM_OPERATION MO, HOLDER_ACCOUNT_OPERATION haos                               ");
        sbQuery.append("             WHERE 1 = 1                                                                             ");                 
        sbQuery.append("                 AND HAD_SUB.ID_HOLDER_ACCOUNT_FK = HAO_SUB.ID_HOLDER_ACCOUNT_FK                     ");                     
        sbQuery.append("                 AND H_SUB.ID_HOLDER_PK = HAD_SUB.ID_HOLDER_FK                                       ");                     
        sbQuery.append("                 AND HAO_SUB.HOLDER_ACCOUNT_STATE = 625                                              ");                     
        sbQuery.append("                 AND HAO_SUB.ROLE = 2                                                                ");                     
        sbQuery.append("                 AND HAO_SUB.OPERATION_PART = haos.OPERATION_PART                                    ");                      
        sbQuery.append("                 AND HAO_SUB.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK)) CUI_ORIGEN,  ");
        sbQuery.append("(SELECT LISTAGG(FULL_NAME,'<br>' )  WITHIN GROUP (ORDER BY FULL_NAME)                                ");
        sbQuery.append("      FROM ( SELECT distinct H_SUB.FULL_NAME                                                         ");              
        sbQuery.append("             FROM                                                                                    ");
        sbQuery.append("                 HOLDER_ACCOUNT_OPERATION HAO_SUB,                                                   ");
        sbQuery.append("                 HOLDER_ACCOUNT_DETAIL HAD_SUB,                                                      ");
        sbQuery.append("                 HOLDER H_SUB                                                                        ");
        sbQuery.append("                 ,MECHANISM_OPERATION MO, HOLDER_ACCOUNT_OPERATION haos                               ");        
        sbQuery.append("             WHERE 1 = 1                                                                             ");                 
        sbQuery.append("                 AND HAD_SUB.ID_HOLDER_ACCOUNT_FK = HAO_SUB.ID_HOLDER_ACCOUNT_FK                     ");                     
        sbQuery.append("                 AND H_SUB.ID_HOLDER_PK = HAD_SUB.ID_HOLDER_FK                                       ");                     
        sbQuery.append("                 AND HAO_SUB.HOLDER_ACCOUNT_STATE = 625                                              ");                     
        sbQuery.append("                 AND HAO_SUB.ROLE = 2                                                                ");                     
        sbQuery.append("                 AND HAO_SUB.OPERATION_PART = haos.OPERATION_PART                                    ");                      
        sbQuery.append("                 AND HAO_SUB.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK)) NAME_ORIGEN, ");
    	
    	sbQuery.append("  'COMPRA' ROL_DESTINO,										 ");				
        sbQuery.append("  CASE WHEN MO.ID_NEGOTIATION_MODALITY_FK IN (1,2) THEN BUYER.MNEMONIC	 ");
        sbQuery.append("       WHEN MO.ID_NEGOTIATION_MODALITY_FK IN (3,4) AND so.OPERATION_PART=1 THEN BUYER.MNEMONIC	 ");
        sbQuery.append("       WHEN MO.ID_NEGOTIATION_MODALITY_FK IN (3,4) AND so.OPERATION_PART=2 THEN SELLER.MNEMONIC	 ");
        sbQuery.append("  END PAR_DESTINO,	 ");	
        
        sbQuery.append("(SELECT LISTAGG(DOCUMENT_NUMBER,'<br>' )  WITHIN GROUP (ORDER BY DOCUMENT_NUMBER)                  ");          
        sbQuery.append("   FROM ( SELECT distinct H_SUB.DOCUMENT_NUMBER                                                    ");                    
        sbQuery.append("          FROM                                                                                     "); 
        sbQuery.append("              HOLDER_ACCOUNT_OPERATION HAO_SUB,                                                    "); 
        sbQuery.append("              HOLDER_ACCOUNT_DETAIL HAD_SUB,                                                       "); 
        sbQuery.append("              HOLDER H_SUB                                                                         ");
        sbQuery.append("              ,MECHANISM_OPERATION MO, HOLDER_ACCOUNT_OPERATION haos                               ");                
        sbQuery.append("          WHERE 1 = 1                                                                              ");                 
        sbQuery.append("              AND HAD_SUB.ID_HOLDER_ACCOUNT_FK = HAO_SUB.ID_HOLDER_ACCOUNT_FK                      ");                     
        sbQuery.append("              AND H_SUB.ID_HOLDER_PK = HAD_SUB.ID_HOLDER_FK                                        ");                     
        sbQuery.append("              AND HAO_SUB.HOLDER_ACCOUNT_STATE = 625                                               ");                     
        sbQuery.append("              AND HAO_SUB.ROLE = 1                                                                 ");                     
        sbQuery.append("              AND HAO_SUB.OPERATION_PART = haos.OPERATION_PART                                     ");                      
        sbQuery.append("              AND HAO_SUB.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK)) NIT_DESTINO,  "); 
        sbQuery.append(" (SELECT LISTAGG(ID_HOLDER_PK,'<br>' )  WITHIN GROUP (ORDER BY ID_HOLDER_PK)                       ");     
        sbQuery.append("   FROM ( SELECT distinct H_SUB.ID_HOLDER_PK                                                       ");                 
        sbQuery.append("          FROM                                                                                     "); 
        sbQuery.append("              HOLDER_ACCOUNT_OPERATION HAO_SUB,                                                    "); 
        sbQuery.append("              HOLDER_ACCOUNT_DETAIL HAD_SUB,                                                       "); 
        sbQuery.append("              HOLDER H_SUB                                                                         ");
        sbQuery.append("              ,MECHANISM_OPERATION MO, HOLDER_ACCOUNT_OPERATION haos                               ");                
        sbQuery.append("          WHERE 1 = 1                                                                              ");                 
        sbQuery.append("              AND HAD_SUB.ID_HOLDER_ACCOUNT_FK = HAO_SUB.ID_HOLDER_ACCOUNT_FK                      ");                     
        sbQuery.append("              AND H_SUB.ID_HOLDER_PK = HAD_SUB.ID_HOLDER_FK                                        ");                     
        sbQuery.append("              AND HAO_SUB.HOLDER_ACCOUNT_STATE = 625                                               ");                     
        sbQuery.append("              AND HAO_SUB.ROLE = 1                                                                 ");                     
        sbQuery.append("              AND HAO_SUB.OPERATION_PART = haos.OPERATION_PART                                     ");                      
        sbQuery.append("              AND HAO_SUB.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK)) CUI_DESTINO,  "); 
        sbQuery.append("(SELECT LISTAGG(FULL_NAME,'<br>' )  WITHIN GROUP (ORDER BY FULL_NAME)                              "); 
        sbQuery.append("   FROM ( SELECT distinct H_SUB.FULL_NAME                                                          ");              
        sbQuery.append("          FROM                                                                                     "); 
        sbQuery.append("              HOLDER_ACCOUNT_OPERATION HAO_SUB,                                                    "); 
        sbQuery.append("              HOLDER_ACCOUNT_DETAIL HAD_SUB,                                                       "); 
        sbQuery.append("              HOLDER H_SUB                                                                         ");
        sbQuery.append("              ,MECHANISM_OPERATION MO, HOLDER_ACCOUNT_OPERATION haos                               ");                
        sbQuery.append("          WHERE 1 = 1                                                                              ");                 
        sbQuery.append("              AND HAD_SUB.ID_HOLDER_ACCOUNT_FK = HAO_SUB.ID_HOLDER_ACCOUNT_FK                      ");                     
        sbQuery.append("              AND H_SUB.ID_HOLDER_PK = HAD_SUB.ID_HOLDER_FK                                        ");                     
        sbQuery.append("              AND HAO_SUB.HOLDER_ACCOUNT_STATE = 625                                               ");                     
        sbQuery.append("              AND HAO_SUB.ROLE = 1                                                                 ");                     
        sbQuery.append("              AND HAO_SUB.OPERATION_PART = haos.OPERATION_PART                                     ");                      
        sbQuery.append("              AND HAO_SUB.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK)) NAME_DESTINO, "); 
    	
    	sbQuery.append("  MO.ID_NEGOTIATION_MECHANISM_FK MECANISMO,										 ");				
    	sbQuery.append("  MO.ID_NEGOTIATION_MODALITY_FK MODALIDAD,										 ");				
    	sbQuery.append("  nm.MODALITY_NAME MODALIDAD_DESC										 ");
    	sbQuery.append("  ,SO.SETTLEMENT_DATE  FECHA_ORD					 ");
    	sbQuery.append("  from MECHANISM_OPERATION MO										 ");				
    	sbQuery.append("  inner join SETTLEMENT_OPERATION so on so.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK										 ");				
    	sbQuery.append("  inner join SECURITY SEC ON MO.ID_SECURITY_CODE_FK = SEC.ID_SECURITY_CODE_PK										 ");
    	sbQuery.append("  left  join issuer iss on iss.ID_ISSUER_PK = SEC.ID_ISSUER_FK		 ");
    	sbQuery.append("  inner join PARTICIPANT BUYER on BUYER.ID_PARTICIPANT_PK = MO.ID_BUYER_PARTICIPANT_FK										 ");				
    	sbQuery.append("  inner join PARTICIPANT SELLER on SELLER.ID_PARTICIPANT_PK = MO.ID_SELLER_PARTICIPANT_FK										 ");				
    	sbQuery.append("  left  join NEGOTIATION_MODALITY nm on MO.ID_NEGOTIATION_MODALITY_FK = nm.ID_NEGOTIATION_MODALITY_PK										 ");			
    	sbQuery.append("  left join holder_account_operation haos on haos.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK  ");
    	
    	sbQuery.append("  where 1 = 1										 ");
    	
    	sbQuery.append("and so.operation_part = haos.operation_part	   ");
    	sbQuery.append("and haos.holder_account_state = 625            ");
    	sbQuery.append("and haos.role = 1                              ");
    	
    	if(transferOperationTO.getOperation().equals(GeneralConstants.ZERO_VALUE_INTEGER))
    		sbQuery.append("  and MO.ID_NEGOTIATION_MECHANISM_FK in (1,3,5)  ");		
    	if(transferOperationTO.getOperation().equals(GeneralConstants.ONE_VALUE_INTEGER))
    		sbQuery.append("  and MO.ID_NEGOTIATION_MECHANISM_FK in (1)  ");	
    	if(transferOperationTO.getOperation().equals(GeneralConstants.THREE_VALUE_INTEGER))
    		sbQuery.append("  and MO.ID_NEGOTIATION_MECHANISM_FK in (3,5)  ");
    	sbQuery.append("  and SO.OPERATION_STATE =										 ");				
    	sbQuery.append("      CASE WHEN SO.OPERATION_PART = 1 then 615										 ");				
    	sbQuery.append("           WHEN SO.OPERATION_PART = 2 then 616										 ");				
    	sbQuery.append("      END										 ");				
    	sbQuery.append("  	and trunc(SO.SETTLEMENT_DATE)   BETWEEN TO_DATE('"+transferOperationTO.getStrDateInitial()+"','dd/MM/yyyy') AND TO_DATE('"+transferOperationTO.getStrDateEnd()+"','dd/MM/yyyy') ");
    	
    	if(Validations.validateIsNotNull(transferOperationTO.getIssuer())){
    		sbQuery.append("  	and iss.ID_ISSUER_PK =  '"+transferOperationTO.getIssuer()+"' ");
    	}
    	if(Validations.validateIsNotNull(transferOperationTO.getParticipant())){
    		sbQuery.append("  	and (BUYER.ID_PARTICIPANT_PK =  "+transferOperationTO.getParticipant()+" ");
    		sbQuery.append("  		OR SELLER.ID_PARTICIPANT_PK =  "+transferOperationTO.getParticipant()+" ) ");
    	}
    	if(Validations.validateIsNotNull(transferOperationTO.getCuiHolder())){
    		sbQuery.append(" 	AND   " +transferOperationTO.getCuiHolder());
    		sbQuery.append("   in ( select had.ID_HOLDER_FK from HOLDER_ACCOUNT_DETAIL had where had.ID_HOLDER_ACCOUNT_FK in (haos.id_holder_account_fk)) and haos.HOLDER_ACCOUNT_STATE = 625 ");
    	}
    	if(Validations.validateIsNotNull(transferOperationTO.getHolderAccount())){
    		sbQuery.append(" 	AND   " +transferOperationTO.getHolderAccount() + " in haos.id_holder_account_fk  ");
    	}
    	if(Validations.validateIsNotNull(transferOperationTO.getSecurityClass())){
    		sbQuery.append("  	and SEC.SECURITY_CLASS =  "+transferOperationTO.getSecurityClass()+" ");
    	}
    	if(Validations.validateIsNotNull(transferOperationTO.getSecurity())){
    		sbQuery.append("  	and SEC.ID_SECURITY_CODE_PK =  '"+transferOperationTO.getSecurity()+"' ");
    	}
    	if(Validations.validateIsNotNull(transferOperationTO.getCurrencyOperation())){
    		sbQuery.append("  	and MO.OPERATION_CURRENCY =  "+transferOperationTO.getCurrencyOperation()+" ");
    	}
    	if(transferOperationTO.getModality().equals(GeneralConstants.ONE_VALUE_INTEGER)){
    		sbQuery.append("  and MO.ID_NEGOTIATION_MODALITY_FK in (1,2,3,4)  ");		
    	}else 	if (transferOperationTO.getModality().equals(GeneralConstants.TWO_VALUE_INTEGER)){
    				sbQuery.append("  and MO.ID_NEGOTIATION_MODALITY_FK in (1,2)  ");		
    			}else 	if (transferOperationTO.getModality().equals(GeneralConstants.THREE_VALUE_INTEGER)){
    						sbQuery.append("  and MO.ID_NEGOTIATION_MODALITY_FK in (3,4)  ");		
    					}
    	if(Validations.validateIsNotNull(transferOperationTO.getAlternateCode())){
    		sbQuery.append("  	and SEC.ALTERNATIVE_CODE =  '"+transferOperationTO.getAlternateCode()+"' ");
    	}
    	
    	//issue 1379: Adicion de las Transferencias por Intercambio de Valor
    	if(transferOperationTO.getOperation().equals(GeneralConstants.ZERO_VALUE_INTEGER)){
    		// issue 1379: aplicar 'union all' con sus respectivos filtros
    		sbQuery.append(" UNION ");
    		sbQuery.append(getQueryExchangeSecurities(transferOperationTO));
        	return sbQuery.toString();
    		
    	}else if(transferOperationTO.getOperation().equals(GeneralConstants.SIX_VALUE_INTEGER)){ 
    		//issue 1379: aplicar solo la query de transferencia por Intercambio de Valores y absorber a vacio la otra query
    		sbQuery = new StringBuilder();
    		sbQuery.append(getQueryExchangeSecurities(transferOperationTO));
        	return sbQuery.toString();
    	}
 
    	sbQuery.append("    order by MO.ID_NEGOTIATION_MECHANISM_FK,MO.ID_NEGOTIATION_MODALITY_FK, FECHA_ORD,SEC.ID_SECURITY_CODE_PK,MO.OPERATION_NUMBER			 ");	
    	
		return sbQuery.toString();
    	
    	/*
    	sbQuery.append("  UNION ALL										 ");	
    	
    	sbQuery.append("  SELECT 										 ");				
    	sbQuery.append("    'Z' PARTE_OP,										 ");				
    	sbQuery.append("    CUS.OPERATION_NUMBER NRO_OPERACION,										 ");				
    	sbQuery.append("  	TO_CHAR(CUS.OPERATION_DATE,'DD/MM/YYYY') FECHA,									 ");				
    	sbQuery.append("  	SE.ID_SECURITY_CODE_PK VALOR,									 ");				
    	sbQuery.append("  	SE.ALTERNATIVE_CODE VALOR_ALT,									 ");				
    	sbQuery.append("    TO_NUMBER('1') CANTIDAD ,										 ");				
    	sbQuery.append("  	'VENTA' ROL_ORIGEN,									 ");				
    	sbQuery.append("  	(SELECT MNEMONIC FROM PARTICIPANT where ID_PARTICIPANT_PK = COD_ORIGEN.ID_PARTICIPANT_FK) PAR_ORIGEN,			 ");			
    	sbQuery.append("  	(SELECT LISTAGG(H.DOCUMENT_NUMBER,'<br>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)								");				
    	sbQuery.append("  	FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK							");				
    	sbQuery.append("  	WHERE HAD.ID_HOLDER_ACCOUNT_FK =COD_ORIGEN.ID_HOLDER_ACCOUNT_FK) NIT_ORIGEN,									 ");				
    	sbQuery.append("  	(SELECT LISTAGG(HAD.ID_HOLDER_FK,'<br>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)								 ");				
    	sbQuery.append("  	FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK							 ");				
    	sbQuery.append("  	WHERE HAD.ID_HOLDER_ACCOUNT_FK =COD_ORIGEN.ID_HOLDER_ACCOUNT_FK) CUI_ORIGEN,									 ");				
    	sbQuery.append("  	(SELECT LISTAGG(H.FULL_NAME,'<br>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)									 ");				
    	sbQuery.append("  	FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK							 ");				
    	sbQuery.append("  	WHERE HAD.ID_HOLDER_ACCOUNT_FK =COD_ORIGEN.ID_HOLDER_ACCOUNT_FK) NAME_ORIGEN,									 ");				
    	sbQuery.append("  	'COMPRA' ROL_DESTINO,									 ");				
    	sbQuery.append("  	(SELECT MNEMONIC FROM PARTICIPANT where ID_PARTICIPANT_PK = COD_DESTINO.ID_PARTICIPANT_FK) PAR_DESTINO,			");			
    	sbQuery.append("  	--FN_GET_HOLDER_NAME(COD_DESTINO.ID_HOLDER_ACCOUNT_FK) DAT_HOLDER_DES,									 ");				
    	sbQuery.append("  	(SELECT LISTAGG(H.DOCUMENT_NUMBER,'<br>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)									 ");				
    	sbQuery.append("  	FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK									 ");				
    	sbQuery.append("  	WHERE HAD.ID_HOLDER_ACCOUNT_FK =COD_DESTINO.ID_HOLDER_ACCOUNT_FK) NIT_DESTINO,									 ");				
    	sbQuery.append("  	(SELECT LISTAGG(HAD.ID_HOLDER_FK,'<br>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)									 ");				
    	sbQuery.append("  	FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK									 ");				
    	sbQuery.append("  	WHERE HAD.ID_HOLDER_ACCOUNT_FK =COD_DESTINO.ID_HOLDER_ACCOUNT_FK) CUI_DESTINO,									 ");				
    	sbQuery.append("  	(SELECT LISTAGG(H.FULL_NAME,'<br>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)									 ");				
    	sbQuery.append("  	FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK									 ");				
    	sbQuery.append("  	WHERE HAD.ID_HOLDER_ACCOUNT_FK =COD_DESTINO.ID_HOLDER_ACCOUNT_FK) NAME_DESTINO,									 ");				
    	sbQuery.append("    TO_NUMBER('3') MECANISMO,										 ");				
    	sbQuery.append("    CASE WHEN SE.INSTRUMENT_TYPE = 124 THEN TO_NUMBER('1')										 ");				
    	sbQuery.append("         WHEN SE.INSTRUMENT_TYPE = 399 THEN TO_NUMBER('2')										 ");				
    	sbQuery.append("    END MODALIDAD,     										 ");				
    	sbQuery.append("    (select nm.MODALITY_NAME from NEGOTIATION_MODALITY nm where nm.ID_NEGOTIATION_MODALITY_PK = 										 ");				
    	sbQuery.append("      CASE WHEN SE.INSTRUMENT_TYPE = 124 THEN TO_NUMBER('1')										 ");				
    	sbQuery.append("           WHEN SE.INSTRUMENT_TYPE = 399 THEN TO_NUMBER('2')										 ");				
    	sbQuery.append("      END										 ");				
    	sbQuery.append("    ) MODALIDAD_DESC										 ");				
    	sbQuery.append("  FROM SECURITY SE, CHANGE_OWNERSHIP_OPERATION COO, CHANGE_OWNERSHIP_DETAIL COD_ORIGEN, CHANGE_OWNERSHIP_DETAIL COD_DESTINO, CUSTODY_OPERATION CUS		 ");
    	sbQuery.append("  WHERE COO.ID_CHANGE_OWNERSHIP_PK = COD_ORIGEN.ID_CHANGE_OWNERSHIP_FK										 ");				
    	sbQuery.append("  	AND COD_ORIGEN.IND_ORIGIN_TARGET = 1 --ORIGEN									 ");				
    	sbQuery.append("  	AND COO.ID_CHANGE_OWNERSHIP_PK = COD_DESTINO.ID_CHANGE_OWNERSHIP_FK									 ");				
    	sbQuery.append("  	AND COD_DESTINO.IND_ORIGIN_TARGET = 2 --DESTINO									 ");				
    	sbQuery.append("  	AND COD_ORIGEN.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK									 ");				
    	sbQuery.append("    AND COO.ID_CHANGE_OWNERSHIP_PK = CUS.ID_CUSTODY_OPERATION_PK										 ");				
    	sbQuery.append("    AND SE.SECURITY_CLASS IN (420,1976)										 ");				
    	sbQuery.append("  	and TRUNC(CUS.OPERATION_DATE)   BETWEEN TO_DATE('"+transferOperationTO.getStrDateInitial()+"','dd/MM/yyyy') AND TO_DATE('"+transferOperationTO.getStrDateEnd()+"','dd/MM/yyyy') ");

			if(gfto.getFormatBcb().equals("2")){
	        	sbQuery.append(" AND SEC.ID_SECURITY_BCB_CODE IS NOT NULL ");
	    	}
			if(Validations.validateIsNotNull(gfto.getParticipant())){
				sbQuery.append(" AND OP.ID_BUYER_PARTICIPANT_FK="+gfto.getParticipant()+" ");
			}
		}*/
		
	}
	
	/**
	 * Obtiene la query de Intercambio de Valores. Este fragmento de query es utilizado en el metodo: getQueryTransferOPeration
	 * @return
	 */
	private String getQueryExchangeSecurities(TransferOperationTO transferOperationTO){
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append(" SELECT                                                                     ");
		sbQuery.append(" 	NULL ID_MECHANISM_OPERATION_PK,                                         ");
		sbQuery.append(" 	'CONTADO' AS PARTE_OP,                                                  ");
		sbQuery.append(" 	CUS.OPERATION_NUMBER NRO_OPERACION,                                     ");
		sbQuery.append(" 	TO_CHAR ( CUS.OPERATION_DATE,'DD/MM/YYYY' ) FECHA,                      ");
		sbQuery.append(" 	SE.ID_SECURITY_CODE_PK VALOR,                                           ");
		sbQuery.append(" 	SE.ALTERNATIVE_CODE VALOR_ALT,                                          ");
		sbQuery.append(" 	COD_ORIGEN.AVAILABLE_BALANCE CANTIDAD,                                  ");
		sbQuery.append(" 	'VENTA' ROL_ORIGEN,                                                     ");
		sbQuery.append(" 	( SELECT MNEMONIC FROM PARTICIPANT                                      ");
		sbQuery.append("	  WHERE ID_PARTICIPANT_PK = COD_ORIGEN.ID_PARTICIPANT_FK                ");
		sbQuery.append(" 	) PAR_ORIGEN,                                                           ");
		sbQuery.append(" 	(SELECT LISTAGG ( H.DOCUMENT_NUMBER,'<br>' ) WITHIN GROUP (             ");
		sbQuery.append(" 	ORDER BY HAD.ID_HOLDER_FK )                                             ");
		sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD                                          ");
		sbQuery.append(" 	INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK                ");
		sbQuery.append(" 	WHERE HAD.ID_HOLDER_ACCOUNT_FK = COD_ORIGEN.ID_HOLDER_ACCOUNT_FK        ");
		sbQuery.append(" 	) NIT_ORIGEN,                                                           ");
		sbQuery.append(" 	(SELECT LISTAGG ( HAD.ID_HOLDER_FK,'<br>' ) WITHIN GROUP (              ");
		sbQuery.append(" 	ORDER BY HAD.ID_HOLDER_FK )                                             ");
		sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD                                          ");
		sbQuery.append(" 	INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK                ");
		sbQuery.append(" 	WHERE HAD.ID_HOLDER_ACCOUNT_FK = COD_ORIGEN.ID_HOLDER_ACCOUNT_FK        ");
		sbQuery.append(" 	) CUI_ORIGEN,                                                           ");
		sbQuery.append(" 	(SELECT LISTAGG ( H.FULL_NAME,'<br>' ) WITHIN GROUP (                   ");
		sbQuery.append(" 	ORDER BY HAD.ID_HOLDER_FK )                                             ");
		sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD                                          ");
		sbQuery.append(" 	INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK                ");
		sbQuery.append(" 	WHERE HAD.ID_HOLDER_ACCOUNT_FK = COD_ORIGEN.ID_HOLDER_ACCOUNT_FK        ");
		sbQuery.append(" 	) NAME_ORIGEN,                                                          ");
		sbQuery.append(" 	'COMPRA' ROL_DESTINO,                                                   ");
		sbQuery.append(" 	(SELECT MNEMONIC                                                        ");
		sbQuery.append(" 	FROM PARTICIPANT                                                        ");
		sbQuery.append(" 	WHERE ID_PARTICIPANT_PK = COD_DESTINO.ID_PARTICIPANT_FK                 ");
		sbQuery.append(" 	) PAR_DESTINO,                                                          ");
		sbQuery.append(" 	(SELECT LISTAGG ( H.DOCUMENT_NUMBER,'<br>' ) WITHIN GROUP (             ");
		sbQuery.append(" 	ORDER BY HAD.ID_HOLDER_FK )                                             ");
		sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD                                          ");
		sbQuery.append(" 	INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK                ");
		sbQuery.append(" 	WHERE HAD.ID_HOLDER_ACCOUNT_FK = COD_DESTINO.ID_HOLDER_ACCOUNT_FK       ");
		sbQuery.append(" 	) NIT_DESTINO,                                                          ");
		sbQuery.append(" 	(SELECT LISTAGG ( HAD.ID_HOLDER_FK,'<br>' ) WITHIN GROUP (              ");
		sbQuery.append(" 	ORDER BY HAD.ID_HOLDER_FK )                                             ");
		sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD                                          ");
		sbQuery.append(" 	INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK                ");
		sbQuery.append(" 	WHERE HAD.ID_HOLDER_ACCOUNT_FK = COD_DESTINO.ID_HOLDER_ACCOUNT_FK       ");
		sbQuery.append(" 	) CUI_DESTINO,                                                          ");
		sbQuery.append(" 	(SELECT LISTAGG ( H.FULL_NAME,'<br>' ) WITHIN GROUP (                   ");
		sbQuery.append(" 	ORDER BY HAD.ID_HOLDER_FK )                                             ");
		sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD                                          ");
		sbQuery.append(" 	INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK                ");
		sbQuery.append(" 	WHERE HAD.ID_HOLDER_ACCOUNT_FK = COD_DESTINO.ID_HOLDER_ACCOUNT_FK       ");
		sbQuery.append(" 	) NAME_DESTINO,                                                         ");
		sbQuery.append(" 	6 MECANISMO,                                                            ");
		sbQuery.append(" 	11 MODALIDAD,                                                           ");
		sbQuery.append(" 	'COMPRA/VENTA RF' as MODALIDAD_DESC,                                    ");
		sbQuery.append("    CUS.OPERATION_DATE");
		sbQuery.append(" FROM SECURITY SE,                                                          ");
		sbQuery.append(" 	ISSUER I,                                                               ");
		sbQuery.append(" 	CHANGE_OWNERSHIP_OPERATION COO,                                         ");
		sbQuery.append(" 	CHANGE_OWNERSHIP_DETAIL COD_ORIGEN,                                     ");
		sbQuery.append(" 	CHANGE_OWNERSHIP_DETAIL COD_DESTINO,                                    ");
		sbQuery.append(" 	CUSTODY_OPERATION CUS                                                   ");
		sbQuery.append(" WHERE COO.ID_CHANGE_OWNERSHIP_PK = COD_ORIGEN.ID_CHANGE_OWNERSHIP_FK       ");
		sbQuery.append(" AND COD_ORIGEN.IND_ORIGIN_TARGET = 1                                       ");
		sbQuery.append(" AND COO.ID_CHANGE_OWNERSHIP_PK = COD_DESTINO.ID_CHANGE_OWNERSHIP_FK        ");
		sbQuery.append(" AND COD_DESTINO.IND_ORIGIN_TARGET = 2                                      ");
		sbQuery.append(" AND COD_ORIGEN.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK                ");
		sbQuery.append(" AND COO.ID_CHANGE_OWNERSHIP_PK = CUS.ID_CUSTODY_OPERATION_PK               ");
		sbQuery.append(" AND I.ID_ISSUER_PK = SE.ID_ISSUER_FK                                       ");
		sbQuery.append(" AND coo.state = 865                                                        ");
		sbQuery.append(" AND COO.IND_EXCHANGE_SECURITIES = 1										");	
		sbQuery.append(" AND ( TRUNC ( CUS.OPERATION_DATE ) BETWEEN ( TO_DATE ( '"+transferOperationTO.getStrDateInitial()+"','dd/MM/yyyy' ) ) AND ( TO_DATE ( '"+transferOperationTO.getStrDateEnd()+"','dd/MM/yyyy' ) ) )");
		// emisor
		if(Validations.validateIsNotNull(transferOperationTO.getIssuer())){
			sbQuery.append(" and I.ID_ISSUER_PK =  '"+transferOperationTO.getIssuer()+"' ");
		}
		// participante (ambas puntas)
		if(Validations.validateIsNotNull(transferOperationTO.getParticipant())){
    		sbQuery.append("  	and (COD_ORIGEN.ID_PARTICIPANT_FK =  "+transferOperationTO.getParticipant()+" ");
    		sbQuery.append("  		OR COD_DESTINO.ID_PARTICIPANT_FK =  "+transferOperationTO.getParticipant()+" ) ");
    	}
		// cui
		if(Validations.validateIsNotNull(transferOperationTO.getCuiHolder())){
    		sbQuery.append(" 	AND   " +transferOperationTO.getCuiHolder());
    		sbQuery.append("   in ( select had.ID_HOLDER_FK from HOLDER_ACCOUNT_DETAIL had where had.ID_HOLDER_ACCOUNT_FK in (COD.ID_HOLDER_ACCOUNT_FK)) and haos.HOLDER_ACCOUNT_STATE = 625 ");
    	}
		// holder_account (cuenta titular)
		if(Validations.validateIsNotNull(transferOperationTO.getHolderAccount())){
    		sbQuery.append(" 	AND   " +transferOperationTO.getHolderAccount() + " in COD.ID_HOLDER_ACCOUNT_FK  ");
    	}
		// instrumento (clase de valor)
    	if(Validations.validateIsNotNull(transferOperationTO.getSecurityClass())){
    		sbQuery.append("  	and SE.SECURITY_CLASS =  "+transferOperationTO.getSecurityClass()+" ");
    	}
    	// valor
    	if(Validations.validateIsNotNull(transferOperationTO.getSecurity())){
    		sbQuery.append("  	and SE.ID_SECURITY_CODE_PK =  '"+transferOperationTO.getSecurity()+"' ");
    	}
    	// codigo alterno
    	if(Validations.validateIsNotNull(transferOperationTO.getAlternateCode())){
    		sbQuery.append("  	and SE.ALTERNATIVE_CODE =  '"+transferOperationTO.getAlternateCode()+"' ");
    	}
    	
    	// dado que se aplica union el order by es modificado a ordenador por indice de columna
    	sbQuery.append(" ORDER BY 18, 19, 21, 5, 3 ");
    	return sbQuery.toString();
	}
    	
	
}
