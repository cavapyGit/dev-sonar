package com.pradera.report.generation.executor.issuances.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class RelationshipOfBuyersOfBankSharesTO implements Serializable{

	private static final long serialVersionUID = 1L;

	private String initialDate;
	private String finalDate;
	private String participant;
	private String cui;
	private String holderAccount;
	private String securities;
	private String state;
	private String institutionType;
	private List<Long> lstHolderAccountPk;
	
	public String getInitialDate() {
		return initialDate;
	}
	public void setInitialDate(String initialDate) {
		this.initialDate = initialDate;
	}
	public String getFinalDate() {
		return finalDate;
	}
	public void setFinalDate(String finalDate) {
		this.finalDate = finalDate;
	}
	public String getParticipant() {
		return participant;
	}
	public void setParticipant(String participant) {
		this.participant = participant;
	}
	public String getCui() {
		return cui;
	}
	public void setCui(String cui) {
		this.cui = cui;
	}
	public String getHolderAccount() {
		return holderAccount;
	}
	public void setHolderAccount(String holderAccount) {
		this.holderAccount = holderAccount;
	}
	public String getSecurities() {
		return securities;
	}
	public void setSecurities(String securities) {
		this.securities = securities;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public List<Long> getLstHolderAccountPk() {
		return lstHolderAccountPk;
	}
	public void setLstHolderAccountPk(List<Long> lstHolderAccountPk) {
		this.lstHolderAccountPk = lstHolderAccountPk;
	}
	public String getInstitutionType() {
		return institutionType;
	}
	public void setInstitutionType(String institutionType) {
		this.institutionType = institutionType;
	}
	

}
