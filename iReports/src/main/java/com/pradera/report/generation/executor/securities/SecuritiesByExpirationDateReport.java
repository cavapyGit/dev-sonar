package com.pradera.report.generation.executor.securities;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.service.IssuerQueryServiceBean;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "SecuritiesByExpirationDateReport")
public class SecuritiesByExpirationDateReport extends GenericReport{
private static final long serialVersionUID = 1L;

@Inject  PraderaLogger log;

@EJB
private ParameterServiceBean parameterService;

@EJB
private IssuerQueryServiceBean issuerQueryServiceBean;

public SecuritiesByExpirationDateReport() {
}
	
@Override
public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public void addParametersQueryReport() { }

@Override
public Map<String, Object> getCustomJasperParameters() {
	Map<String,Object> parametersRequired = new HashMap<>();
	ParameterTableTO  filter = new ParameterTableTO();
	Map<Integer,String> securities_class = new HashMap<Integer, String>();
	Map<Integer,String> currency = new HashMap<Integer, String>();
	List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
	String issuerCode = "";
	String issuerMnemonic = "";

	// DATOS DE ENTRADA DEL SISTEMA
	for (ReportLoggerDetail r : loggerDetails) {
		if (r.getFilterName().equals(ReportConstant.ISSUER_PARAM))
			issuerCode = r.getFilterValue();
	}
	if(issuerCode != null)
		issuerMnemonic = issuerQueryServiceBean.find(issuerCode,Issuer.class).getMnemonic();
	else
		issuerMnemonic = "TODOS";
	
	try {
		
		filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
			securities_class.put(param.getParameterTablePk(), param.getText1());
		}
		
		filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
			currency.put(param.getParameterTablePk(), param.getDescription());
		}
		
	}catch(Exception ex){
		log.error(ex.getMessage());
		ex.printStackTrace();
		throw new RuntimeException();
	}
	parametersRequired.put("p_securities_class", securities_class);
	parametersRequired.put("p_currency", currency);
	parametersRequired.put("p_issuer_mnemonic", issuerMnemonic);
	parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
	// TODO Auto-generated method stub
	return parametersRequired;
	}

}
