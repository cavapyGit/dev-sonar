package com.pradera.report.generation.executor.issuances.to;

import java.io.Serializable;

/** issue 24: to para capturar los parametros del reporte REPORTE LISTADO GENERAL DE EMISORES
 * 
 * @author rlarico */
public class GeneralListOfIssuerReportTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer offerType;
	private String initialDate;
	private String finalDate;
	private String idIssuerPk;
	private Integer economicActivity;
	private Integer stateIssuer;

	// cosntructor
	public GeneralListOfIssuerReportTO() {
	}

	// TODO getter and setter
	public Integer getOfferType() {
		return offerType;
	}

	public void setOfferType(Integer offerType) {
		this.offerType = offerType;
	}

	public String getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(String initialDate) {
		this.initialDate = initialDate;
	}

	public String getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(String finalDate) {
		this.finalDate = finalDate;
	}

	public String getIdIssuerPk() {
		return idIssuerPk;
	}

	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}

	public Integer getEconomicActivity() {
		return economicActivity;
	}

	public void setEconomicActivity(Integer economicActivity) {
		this.economicActivity = economicActivity;
	}

	public Integer getStateIssuer() {
		return stateIssuer;
	}

	public void setStateIssuer(Integer stateIssuer) {
		this.stateIssuer = stateIssuer;
	}

}
