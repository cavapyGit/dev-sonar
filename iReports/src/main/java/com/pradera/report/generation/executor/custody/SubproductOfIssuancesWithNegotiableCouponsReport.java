package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.account.to.PortafolioDetailCustodianTO;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.executor.custody.to.ClientPortafolioTO;
import com.pradera.report.generation.executor.securities.service.PaymentChronogramBySecurity;
import com.pradera.report.generation.executor.securities.to.SecuritiesByIssuanceAndIssuerTO;
import com.pradera.report.generation.service.ReportUtilServiceBean;
import com.pradera.report.util.view.PropertiesConstants;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "SubproductOfIssuancesWithNegotiableCouponsReport")
public class SubproductOfIssuancesWithNegotiableCouponsReport extends GenericReport {

	private static final long serialVersionUID = 1L;
	
	@EJB
	private CustodyReportServiceBean custodyService;
	@Inject
	private PraderaLogger log;
	@EJB
	private ParameterServiceBean parameterService;
	@EJB
	private ReportUtilServiceBean reportUtilServiceBean;
	
	public SubproductOfIssuancesWithNegotiableCouponsReport() {
	}
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters(){
		
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		Map<Integer,String> securityclass = new HashMap<Integer, String>();	
		ClientPortafolioTO subproductsTO = new ClientPortafolioTO();
		ParameterTableTO  filter = new ParameterTableTO();

		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("registry_date")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					subproductsTO.setRegistryDate(CommonsUtilities.convertStringtoDate(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("security_pk")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					subproductsTO.setIdSecurityCode(listaLogger.get(i).getFilterValue());
				}
			}
		}
		
		Map<String,Object> parametersRequired = new HashMap<>();
		String strQuery = custodyService.getSubProductsOfIssuancesWithNegotiableCuponsJasper(subproductsTO);
		if(ReportFormatType.PDF.getCode().equals(getReportLogger().getReportFormat())){
			String strQueryFormatted = custodyService.getQueryForJasperSubProducts(subproductsTO, strQuery);
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				securityclass.put(param.getParameterTablePk(), param.getText1());
			}
			parametersRequired.put("str_query", strQueryFormatted);
			parametersRequired.put("p_securityclass", securityclass);
			parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		}
		else if(ReportFormatType.TXT.getCode().equals(getReportLogger().getReportFormat())){
			List<ClientPortafolioTO> lstCustody = custodyService.getSubProductsOfIssueancesTxt(subproductsTO, strQuery);
			
			StringBuilder sbTxtFormat = new StringBuilder();
			String comilla= "\"";
			try {
				
				for (ClientPortafolioTO cc: lstCustody) {
					if(cc.getCustodian()!=null){
						sbTxtFormat.append(comilla+cc.getCustodian()+comilla);
					}else{
						sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
					}
					sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
					if(cc.getDateInformation()!=null){
						sbTxtFormat.append(comilla+cc.getDateInformation()+comilla);
					}else{
						sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
					}
					sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
					if(cc.getSecurityClass()!=null){
						sbTxtFormat.append(comilla+cc.getSecurityClass()+comilla);
					}else{
						sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
					}
					sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
					if(cc.getValuatorCode()!=null){
						sbTxtFormat.append(comilla+cc.getValuatorCode()+comilla);
					}else{
						sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
					}
					sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
					if(cc.getDetached_coupons()!=null){
						sbTxtFormat.append(comilla+cc.getDetached_coupons()+comilla);
					}else{
						sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
					}
					sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
					if(cc.getCoupons_no_detached()!=null){
						sbTxtFormat.append(comilla+cc.getCoupons_no_detached()+comilla);
					}else{
						sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
					}
					sbTxtFormat.append(GeneralConstants.STR_END_LINE_TXT);
				}
				
				String physicalName = null;
				if(lstCustody.isEmpty()){
					physicalName =getReportLogger().getPhysicalName();
					sbTxtFormat.append(PropertiesConstants.NO_FOUND_DATA);
				}else{
					physicalName= getNameAccordingFund(lstCustody.get(0));
				}
				reportUtilServiceBean.saveCustomReportFile(physicalName, ReportFormatType.TXT.getCode(), sbTxtFormat.toString().getBytes(), getReportLogger());
				
			}catch(Exception ex){
				log.error(ex.getMessage());
				ex.printStackTrace();
				throw new RuntimeException();
			}
		}
		return parametersRequired;
	}	
	
	public String getNameAccordingFund(ClientPortafolioTO custody){
		String name="";
		Date dateAux = new Date();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
			String custody_code="BBB";
			String firstWord="M";
			String lastWord="AF";
			String strDate= dateFormat.format(dateAux);
			String strDay = strDate.substring(0,2);
			String strMonth = strDate.substring(3,5);
			String strYear = strDate.substring(8,10);

			name=firstWord;
			name+=strYear+strMonth+strDay;
			name+=lastWord;
			name+=GeneralConstants.DOT+custody_code;
		
		return name;
	}
}
	
