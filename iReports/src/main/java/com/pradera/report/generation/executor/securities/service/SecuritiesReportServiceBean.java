package com.pradera.report.generation.executor.securities.service;

import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.report.generation.executor.ReportConstant;
// TODO: Auto-generated Javadoc
/**
 * The Class SecuritiesReportServiceBean.
 *
 * @author edv.
 * @version 1.0 , 11/05/2020
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class SecuritiesReportServiceBean extends CrudDaoServiceBean {

	/**
	 * Instantiates a new billing service report service bean.
	 */
	public SecuritiesReportServiceBean(){
		
	}
	
	/**
	 * Lista de tramos de colocacion Reporte 324
	 * Issue 1028
	 * @param parametersRequired
	 * @return
	 */
	public List<Object[]> getPlacementSegmentListByParameters(Map<String,Object> parameters) {
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append(" SELECT                                                                                                                       ");
		sbQuery.append(" (p.MNEMONIC)as participant_desc                                            												  ");
		sbQuery.append(" ,to_char(ps.REGISTRY_DATE,'dd/mm/yyyy') REGISTRY_DATE                                                                        ");
		sbQuery.append(" ,i.MNEMONIC as EMISOR                                                                                                        ");
		sbQuery.append(" ,isu.ID_ISSUANCE_CODE_PK AS CODIGO_EMISION                                                                                   ");
		sbQuery.append(" ,isu.DESCRIPTION AS DESCRIPCION_EMISION                                                                                      ");
		sbQuery.append(" ,sec.ID_SECURITY_CODE_PK AS CLASE_CLAVE_VALOR                                                                                ");
		sbQuery.append(" ,sec.CURRENT_NOMINAL_VALUE AS VALOR_NOMINAL                                                                                  ");
		sbQuery.append(" ,(select DESCRIPTION from PARAMETER_TABLE where PARAMETER_TABLE_PK = sec.CURRENCY) as MONEDA                                 ");
		sbQuery.append(" ,to_char(ps.BEGINING_DATE,'dd/mm/yyyy') AS INICIO_COLOCACION                                                                 ");
		sbQuery.append(" ,to_char(ps.CLOSING_DATE,'dd/mm/yyyy')  AS FINAL_COLOCACION                                                                  ");
		sbQuery.append(" ,(ps.CLOSING_DATE-ps.BEGINING_DATE)AS PLAZO_COLOCACION                                                                       ");
		sbQuery.append(" ,(CASE WHEN sec.CURRENT_NOMINAL_VALUE <> 0 THEN psd.PLACED_AMOUNT/sec.CURRENT_NOMINAL_VALUE                                  ");
		sbQuery.append("  ELSE psd.PLACED_AMOUNT/sec.INITIAL_NOMINAL_VALUE END) AS CANTIDAD_VALORES                                                   ");
		sbQuery.append(" ,psd.PLACED_AMOUNT AS MONTO_COLOCAR                                                                                          ");
		sbQuery.append(" ,sec.PLACED_AMOUNT AS MONTO_COLOCADO                                                                                         ");
		sbQuery.append(" ,(psd.PLACED_AMOUNT-psd.REQUEST_AMOUNT)AS MONTO_PENDIENTE                                                                    ");
		sbQuery.append(" ,sec.SHARE_BALANCE AS CANTIDAD_VALORES_CONTABLES                                                                             ");
		sbQuery.append(" ,(select DESCRIPTION from PARAMETER_TABLE where PARAMETER_TABLE_PK = ps.STATE_PLACEMENT_SEGMENT) as ESTADO                   ");
		sbQuery.append(" from PLACEMENT_SEGMENT ps                                                                                                    ");
		sbQuery.append(" inner join PLACEMENT_SEGMENT_DETAIL psd on psd.ID_PLACEMENT_SEGMENT_FK = ps.ID_PLACEMENT_SEGMENT_PK                          ");
		sbQuery.append(" inner join ISSUANCE isu on isu.ID_ISSUANCE_CODE_PK = ps.ID_ISSUANCE_CODE_FK                                                  ");
		sbQuery.append(" inner join issuer i on i.ID_ISSUER_PK = isu.ID_ISSUER_FK                                                                     ");
		sbQuery.append(" inner join security sec on sec.ID_SECURITY_CODE_PK = psd.ID_SECURITY_CODE_FK                                                 ");
		sbQuery.append(" inner join PLACEMENT_SEG_PARTICIPA_STRUCT psps on psps.ID_PLACEMENT_SEGMENT_FK = ps.ID_PLACEMENT_SEGMENT_PK                  ");
		sbQuery.append(" inner join PARTICIPANT p on p.ID_PARTICIPANT_PK = psps.ID_PARTICIPANT_FK                                                     ");
		sbQuery.append(" where 1=1                                                                                                                    ");
		if(parameters.containsKey(ReportConstant.PARTICIPANT_PARAM))
		sbQuery.append(" and (psps.ID_PARTICIPANT_FK = :participant)                                                          ");
		if(parameters.containsKey(ReportConstant.ISSUER_PARAM))
		sbQuery.append(" and (i.ID_ISSUER_PK = :issuer)                                                                            ");
		if(parameters.containsKey(ReportConstant.SECURITY_CLASS_PARAM))
		sbQuery.append(" and (isu.SECURITY_CLASS = :security_class)                                                        ");
		if(parameters.containsKey(ReportConstant.SECURITY_CODE))
		sbQuery.append(" and (sec.ID_SECURITY_CODE_PK = :security_code)                                                     ");
		sbQuery.append(" and (trunc(ps.REGISTRY_DATE) between trunc(to_date(:date_initial,'dd/MM/yyyy')) and trunc(to_date(:date_end,'dd/MM/yyyy')))  ");
		sbQuery.append(" UNION                                                                                                                        ");
		sbQuery.append(" select                                                                                                                       ");
		sbQuery.append(" (p.MNEMONIC)                                           																	  ");
		sbQuery.append(" ,to_char(psh.REGISTRY_DATE,'dd/mm/yyyy')                                                                                     ");
		sbQuery.append(" ,ih.MNEMONIC                                                                                                                 ");
		sbQuery.append(" ,isuh.ID_ISSUANCE_CODE_PK                                                                                                    ");
		sbQuery.append(" ,isuh.DESCRIPTION                                                                                                            ");
		sbQuery.append(" ,sech.ID_SECURITY_CODE_PK                                                                                                    ");
		sbQuery.append(" ,sech.CURRENT_NOMINAL_VALUE                                                                                   				  ");
		sbQuery.append(" ,(select DESCRIPTION from PARAMETER_TABLE where PARAMETER_TABLE_PK = sech.CURRENCY) as MONEDA                                ");
		sbQuery.append(" ,to_char(psh.BEGINING_DATE,'dd/mm/yyyy') 		                                                                              ");
		sbQuery.append(" ,to_char(psh.CLOSING_DATE,'dd/mm/yyyy')                                                                                      ");
		sbQuery.append(" ,(psh.CLOSING_DATE-psh.BEGINING_DATE)					                                                                      ");
		sbQuery.append(" ,(psdh.PLACED_AMOUNT/sech.CURRENT_NOMINAL_VALUE)					                                                          ");
		sbQuery.append(" ,psdh.PLACED_AMOUNT                                                                                         				  ");
		sbQuery.append(" ,sech.PLACED_AMOUNT                                                                                          				  ");
		sbQuery.append(" ,(psdh.PLACED_AMOUNT-psdh.REQUEST_AMOUNT)                                                                   				  ");
		sbQuery.append(" ,sech.SHARE_BALANCE                                                                          								  ");
		sbQuery.append(" ,(select DESCRIPTION from PARAMETER_TABLE where PARAMETER_TABLE_PK = psh.STATE_PLACEMENT_SEGMENT) as ESTADO                  ");
		sbQuery.append(" from PLACEMENT_SEGMENT ps                                                                                                    ");
		sbQuery.append(" inner join PLACEMENT_SEGMENT_HYS  psh on psh.ID_PLACEMENT_SEGMENT_FK = ps.ID_PLACEMENT_SEGMENT_PK                            ");
		sbQuery.append(" inner join PLACEMENT_SEGMENT_DETAIL_HYS psdh on psdh.ID_PLACEMENT_SEGMENT_HYS_FK = psh.ID_PLACEMENT_SEGMENT_HYS_PK           ");
		sbQuery.append(" inner join ISSUANCE isuh on isuh.ID_ISSUANCE_CODE_PK = psh.ID_ISSUANCE_CODE_FK                                               ");
		sbQuery.append(" inner join issuer ih on ih.ID_ISSUER_PK = isuh.ID_ISSUER_FK                                                                  ");
		sbQuery.append(" inner join security sech on sech.ID_SECURITY_CODE_PK = psdh.ID_SECURITY_CODE_FK                                              ");
		sbQuery.append(" inner join PLACEMENT_SEG_PARTICIPA_STRUCT psps on psps.ID_PLACEMENT_SEGMENT_FK = ps.ID_PLACEMENT_SEGMENT_PK                  ");
		sbQuery.append(" inner join PARTICIPANT p on p.ID_PARTICIPANT_PK = psps.ID_PARTICIPANT_FK                                                     ");
		sbQuery.append(" where 1=1                                                                                                                    ");
		if(parameters.containsKey(ReportConstant.PARTICIPANT_PARAM))
		sbQuery.append(" and (psps.ID_PARTICIPANT_FK = :participant)                                                          ");
		if(parameters.containsKey(ReportConstant.ISSUER_PARAM))
		sbQuery.append(" and (ih.ID_ISSUER_PK = :issuer)                                                                           ");
		if(parameters.containsKey(ReportConstant.SECURITY_CLASS_PARAM))
		sbQuery.append(" and (isuh.SECURITY_CLASS = :security_class)                                                       ");
		if(parameters.containsKey(ReportConstant.SECURITY_CODE))
		sbQuery.append(" and (sech.ID_SECURITY_CODE_PK = :security_code)                                                    ");
		sbQuery.append(" and (trunc(psh.REGISTRY_DATE) between trunc(to_date(:date_initial,'dd/MM/yyyy')) and trunc(to_date(:date_end,'dd/MM/yyyy'))) ");
		sbQuery.append(" order by 1 asc,6 asc,2 desc                                                                                                  ");
		
	    Query query = em.createNativeQuery(sbQuery.toString());
	    
		if(parameters.containsKey(ReportConstant.PARTICIPANT_PARAM))
			query.setParameter("participant", parameters.get(ReportConstant.PARTICIPANT_PARAM));
		
		if(parameters.containsKey(ReportConstant.ISSUER_PARAM))
			query.setParameter("issuer", parameters.get(ReportConstant.ISSUER_PARAM));	
			
		if(parameters.containsKey(ReportConstant.SECURITY_CLASS_PARAM))
			query.setParameter("security_class", parameters.get(ReportConstant.SECURITY_CLASS_PARAM));	
			
		if(parameters.containsKey(ReportConstant.SECURITY_CODE))
			query.setParameter("security_code", parameters.get(ReportConstant.SECURITY_CODE));	
			
		if(parameters.containsKey(ReportConstant.DATE_INITIAL_PARAM))
			query.setParameter("date_initial", parameters.get(ReportConstant.DATE_INITIAL_PARAM));
		
		if(parameters.containsKey(ReportConstant.DATE_END_PARAM))
			query.setParameter("date_end", parameters.get(ReportConstant.DATE_END_PARAM));
			
		return query.getResultList();
	}
	
}
