package com.pradera.report.generation.executor.account;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "ListHoldersCuiReport")
public class ListHoldersCuiReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;

	@Inject
	private PraderaLogger log;

	public ListHoldersCuiReport() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		GenericsFiltersTO gfto = new GenericsFiltersTO();
		String participantLabel=null;
		for(ReportLoggerDetail param: getReportLogger().getReportLoggerDetails()){
			if(param.getFilterName().equals("doc_type")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setDocType(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("participant")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setParticipant(param.getFilterValue().toString());
					participantLabel=param.getFilterDescription();
				}
			}
			if(param.getFilterName().equals("doc_number")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setDocNumber(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("state_holder")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setStateHolder(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("securities_balance")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setSecuritiesBalance(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("type_person")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setTypePerson(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("economic_activity")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setEconomicActivity(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("date_initial")){				
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setInitialDt(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("date_end")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setFinalDt(param.getFilterValue().toString());
				}
			}
		}
		 
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> secDocType = new HashMap<Integer, String>();
		Map<Integer,String> secHolderState = new HashMap<Integer, String>();
		Map<Integer,String> secTypePerson = new HashMap<Integer, String>();
		Map<Integer,String> secEconomicActivity = new HashMap<Integer, String>();
		try {
			filter.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secDocType.put(param.getParameterTablePk(),param.getParameterName());
			}
			filter.setMasterTableFk(MasterTableType.HOLDER_STATE_TYPE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secHolderState.put(param.getParameterTablePk(), param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.PERSON_TYPE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secTypePerson.put(param.getParameterTablePk(), param.getParameterName());
			}
			filter.setMasterTableFk(MasterTableType.ECONOMIC_ACTIVITY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secEconomicActivity.put(param.getParameterTablePk(), param.getDescription());
			}
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		parametersRequired.put("participant_label", participantLabel);
		parametersRequired.put("participant", gfto.getParticipant());
		parametersRequired.put("mDocType", secDocType);
		parametersRequired.put("mHolderState", secHolderState);
		parametersRequired.put("mTypePerson", secTypePerson);
		parametersRequired.put("mEconomicActivity", secEconomicActivity);
		//Checking if the doc_number field not is empty and null
		if(gfto.getDocNumber()!=null && !gfto.getDocNumber().trim().equals("")){
			gfto.setInitialDt(null);
			gfto.setFinalDt(null);
		}
		parametersRequired.put("initial_date", gfto.getInitialDt());
		parametersRequired.put("final_date", gfto.getFinalDt());
		
		if (gfto.getFinalDt() != null) {
			parametersRequired.put("yearFinalDate", gfto.getFinalDt());
		}else{
			parametersRequired.put("yearFinalDate", CommonsUtilities.convertDatetoString(new Date()));
		}
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());

		return parametersRequired;
		
	}
}
