package com.pradera.report.generation.executor.account;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.account.service.AccountReportServiceBean;
import com.pradera.report.generation.executor.account.to.PortafolioDetailCustodianTO;
import com.pradera.report.generation.executor.collectioneconomic.service.ParserUtilServiceReport;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.service.ReportUtilServiceBean;
import com.pradera.report.util.view.PropertiesConstants;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "CustodyDetailAgbAndSafiAndFicReport")
public class CustodyDetailAgbAndSafiAndFicReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;
	@EJB
	private AccountReportServiceBean accountReportService;
	@EJB
	private ReportUtilServiceBean reportUtilServiceBean;
	
	@Inject
	private ParserUtilServiceReport parserUtilServiceReport;
	
	@EJB
	private CustodyReportServiceBean custodyReportServiceBean;
	
	private Integer reportFormat;
	
	private String dateAux;
	
	public CustodyDetailAgbAndSafiAndFicReport() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		PortafolioDetailCustodianTO pdcTO = new PortafolioDetailCustodianTO();
		pdcTO.setReportPk(getReportLogger().getReport().getIdReportPk());
		boolean automatic = false;
		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("cui_holder")){
				if (Validations.validateIsNotNullAndPositive(Long.valueOf(listaLogger.get(i).getFilterValue()))){
					pdcTO.setCui(new Long(listaLogger.get(i).getFilterValue()).longValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("cui_description")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					pdcTO.setHolderDesc(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("security_pk")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					pdcTO.setIdSecurityCode(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("economic_activity")){
				if (Validations.validateIsNotNullAndPositive(Integer.valueOf(listaLogger.get(i).getFilterValue()))){
					pdcTO.setEconomicActivity(Integer.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("date")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					dateAux = listaLogger.get(i).getFilterValue();
					pdcTO.setDateMarketFact(CommonsUtilities.convertStringtoDate(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("report_format")){
				if (Validations.validateIsNotNullAndPositive(Integer.valueOf(listaLogger.get(i).getFilterValue()))){
					reportFormat= Integer.valueOf(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("automatic")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					if(listaLogger.get(i).getFilterValue().equals("true"))
						automatic = true;
				}
			}
		}
		
		Map<String,Object> parametersRequired = new HashMap<>();
		pdcTO.setReportFormat(reportFormat);
		String strQuery = accountReportService.getQueryCustodyDetailAgbSafiFic(pdcTO, null);
		if(ReportFormatType.PDF.getCode().equals(reportFormat)){
			String strQueryFormatted = accountReportService.getQueryForJasperAgbSafiFic(pdcTO, strQuery, null);
			parametersRequired.put("str_query", strQueryFormatted);
			parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
			parametersRequired.put("holder_description", pdcTO.getHolderDesc());
		}else if(ReportFormatType.TXT.getCode().equals(reportFormat)){
			List<PortafolioDetailCustodianTO> lstPDC = null;
			if(automatic)//indicador que se envia por el batchero
				lstPDC = accountReportService.getCustodyDetailAgbSafiFicTxt(pdcTO, strQuery, null, getReportLogger());
			else{
				try {
					lstPDC = parserUtilServiceReport.getCustodyDetailAgbSafiFicTxtFromFile(pdcTO,strQuery);
				} catch (Exception e) {
					e.printStackTrace();
					return null;
				}
			}
				
			StringBuilder sbTxtFormat = new StringBuilder();
			String comilla= "\"";
			for (PortafolioDetailCustodianTO pdc: lstPDC) {
				sbTxtFormat.append(comilla+pdc.getCustodio()+comilla);
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				sbTxtFormat.append(comilla+pdc.getDateMarketFactString()+comilla);
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				if(Validations.validateIsNotNullAndNotEmpty(pdc.getFundAdmin())){
					sbTxtFormat.append(comilla+pdc.getFundAdmin()+comilla);
				}else{
					String nemmonicFund = parserUtilServiceReport.getFundTypeHolder(pdc.getFundAdmin(),
							pdc.getInstrumentSecClass()+"-"+pdc.getIdSecurityCode(),
							pdc.getDateMarketFactString(),pdc.getValuatorCode(),pdc.getTotalBalance());
					
					sbTxtFormat.append(comilla+nemmonicFund+comilla);
				}
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				/**Campo vacio*/
				if(Validations.validateIsNotNullAndNotEmpty(pdc.getFundType())){
					if(pdc.getFundType().length()>2)
						sbTxtFormat.append(comilla+pdc.getFundType()+comilla);
				}else{
					/*String nemmonicFund = parserUtilServiceReport.getFundTypeHolder(pdc.getFundAdmin(),
							pdc.getInstrumentSecClass()+"-"+pdc.getIdSecurityCode(),
							pdc.getDateMarketFactString(),pdc.getValuatorCode(),pdc.getTotalBalance());
					if(nemmonicFund!=null&&!nemmonicFund.isEmpty())
						sbTxtFormat.append(comilla+nemmonicFund+comilla);
					else*/
						sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
				}
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				sbTxtFormat.append(comilla+pdc.getInstrumentSecClass()+comilla);
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				sbTxtFormat.append(comilla+pdc.getIdSecurityCode()+comilla);
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				if(Validations.validateIsNotNullAndNotEmpty(pdc.getValuatorCode())){
					sbTxtFormat.append(comilla+pdc.getValuatorCode()+comilla);
				}else{
					sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
				}
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				if(pdc.getSecCodeOnCustody().equals("FIS")){
					sbTxtFormat.append(comilla+pdc.getTotalBalance()+comilla);
				}else{
					sbTxtFormat.append(comilla+pdc.getTotalBalance()+comilla);
				}
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				
				if(pdc.getMarketRate()!=null&&pdc.getMarketRate().compareTo(new BigDecimal("-9999"))!=0)
					sbTxtFormat.append(comilla+CommonsUtilities.formatDecimal(pdc.getMarketRate(),ReportConstant.FORMAT_ONLY_FOUR_DECIMALS)+comilla);
				else
					sbTxtFormat.append(comilla+CommonsUtilities.formatDecimal(BigDecimal.ZERO,ReportConstant.FORMAT_ONLY_FOUR_DECIMALS)+comilla);
				
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				sbTxtFormat.append(comilla+CommonsUtilities.formatDecimal(pdc.getMarketPrice(),ReportConstant.FORMAT_ONLY_TWO_DECIMAL)+comilla);
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				sbTxtFormat.append(comilla+pdc.getSecCodeOnCustody()+comilla);
				sbTxtFormat.append(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				if(Validations.validateIsNotNullAndNotEmpty(pdc.getIndReporto())){
					sbTxtFormat.append(comilla+pdc.getIndReporto()+comilla);
				}else{
					sbTxtFormat.append(comilla+GeneralConstants.EMPTY_STRING+comilla);
				}
				sbTxtFormat.append(GeneralConstants.STR_END_LINE_TXT);
			}
			
			if(lstPDC.isEmpty())
				sbTxtFormat.append(PropertiesConstants.NO_FOUND_DATA_BLANK);
			String fileName="";
			String custody_code="EDB";
			String firstWord="M";
			String lastWord="U";
			String strDate= dateAux;
			String strDay = strDate.substring(0,2);
			String strMonth = strDate.substring(3,5);
			String strYear = strDate.substring(8,10);

			fileName=firstWord;
			fileName+=strYear+strMonth+strDay;
			fileName+=lastWord;
			fileName+=GeneralConstants.DOT+custody_code;
			
			reportUtilServiceBean.saveCustomReportFile(fileName, ReportFormatType.TXT.getCode(), sbTxtFormat.toString().getBytes(), getReportLogger());
		}
		parametersRequired.put("date", CommonsUtilities.convertDatetoString(pdcTO.getDateMarketFact()));
		return parametersRequired;
	}
	
	public String getNameAccordingFund(PortafolioDetailCustodianTO custody){
		String name="";
		String custody_code="EDB";
		String firstWord="M";
		String lastWord="U";
		String strDate= dateAux;
		String strDay = strDate.substring(0,2);
		String strMonth = strDate.substring(3,5);
		String strYear = strDate.substring(8,10);

		name=firstWord;
		name+=strYear+strMonth+strDay;
		name+=lastWord;
		name+=GeneralConstants.DOT+custody_code;
		return name;
	}
}
