package com.pradera.report.generation.executor.negotiation.to;

import java.io.Serializable;


public class PrimaryAndSecondaryMarketGralTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String idParticipant;
	private String cui;
	private String currency;
	private String idIssuer;
	private String securityClass;
	private String idSecurity;
	private Integer typeInformation;
	private String modality;
	private String initialDate;
	private String finalDate;
	public String getIdParticipant() {
		return idParticipant;
	}
	public void setIdParticipant(String idParticipant) {
		this.idParticipant = idParticipant;
	}
	public String getCui() {
		return cui;
	}
	public void setCui(String cui) {
		this.cui = cui;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getIdIssuer() {
		return idIssuer;
	}
	public void setIdIssuer(String idIssuer) {
		this.idIssuer = idIssuer;
	}
	public String getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(String securityClass) {
		this.securityClass = securityClass;
	}
	public String getIdSecurity() {
		return idSecurity;
	}
	public void setIdSecurity(String idSecurity) {
		this.idSecurity = idSecurity;
	}
	public Integer getTypeInformation() {
		return typeInformation;
	}
	public void setTypeInformation(Integer typeInformation) {
		this.typeInformation = typeInformation;
	}
	public String getModality() {
		return modality;
	}
	public void setModality(String modality) {
		this.modality = modality;
	}
	public String getInitialDate() {
		return initialDate;
	}
	public void setInitialDate(String initialDate) {
		this.initialDate = initialDate;
	}
	public String getFinalDate() {
		return finalDate;
	}
	public void setFinalDate(String finalDate) {
		this.finalDate = finalDate;
	}

}
