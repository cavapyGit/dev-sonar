package com.pradera.report.generation.executor.corporative;

import java.io.ByteArrayOutputStream;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.corporateevents.to.CorporativeOperationSubQueryTO;
import com.pradera.core.component.corporateevents.to.CorporativeOperationTO;
import com.pradera.core.component.helperui.service.SecuritiesQueryServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.corporative.service.CorporativeReportServiceBean;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;


// TODO: Auto-generated Javadoc
/**
 * The Class DividendsPaidByFrozenBalanceReport.
 */
@ReportProcess(name="DividendsPaidByFrozenBalanceReport")
public class DividendsPaidByFrozenBalanceReport extends GenericReport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static int IMPUESTO 			= 0;
	private static int PARTCIPANT 			= 1;
	private static int ID_PARTICIPANT_PK 	= 2;
	private static int CUENTA 				= 3;
	private static int RNT_DESCRIPCION 		= 4;
	private static int SALDO_PRENDA 		= 5;
	private static int SALDO_EMBARGO 		= 6;
	private static int SALDO_OPOSICION 		= 7;
	private static int SALDO_ENCAJE 		= 8;
	private static int SALDO_OTROS 			= 9;
	private static int SALDO_TOTAL 			= 10;
	private static int BENEFICIO_PRENDA 	= 11;
	private static int BENEFICIO_EMBARGO 	= 12;
	private static int BENEFICIO_OPOSICION 	= 13;
	private static int BENEFICIO_ENCAJE 	= 14;
	private static int BENEFICIO_OTROS 		= 15;
	private static int BENEFICIO_TOTAL 		= 16;
	private static int ID_HOLDER_PK 		= 17;
	private static int STATE 				= 18;
	private static int id_corporative_pk	= 19;
	private static int cpr_id				= 20;
	private static int ID_HOLDER_ACCOUNT_PK	= 21;
	
	private static int DOCUMENT_NUMBER 		= 0;
	private static int block_number 		= 1;
	private static int block_type 			= 2;
	private static int block_balance 		= 3;
	private static int BENEFICIO 			= 4;
	private static int block_entity 		= 5;
	
	@EJB
	CorporativeReportServiceBean corporativeReportServiceBean;
	
	@EJB
	SecuritiesQueryServiceBean securitiesQueryServiceBean;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		boolean existData = false;
		CorporativeOperationTO corporativeOpTO = new CorporativeOperationTO();
		
		
		//Read parameters from DB, all parameters are string type
		for(ReportLoggerDetail loggerDetail : reportLogger.getReportLoggerDetails()) {
			if(loggerDetail.getFilterName().equals(ReportConstant.CORP_PROCESS_ID) &&
				Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	) {
				corporativeOpTO.setId(Long.valueOf(loggerDetail.getFilterValue()));
			}
			
			if(loggerDetail.getFilterName().equals(ReportConstant.CUTOFF_DATE) &&
					Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	){
				corporativeOpTO.setCutOffDate(CommonsUtilities.convertStringtoDate(loggerDetail.getFilterValue()));
				}
			
			if(loggerDetail.getFilterName().equals(ReportConstant.DELIVERY_DATE) &&
					Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	) {
				corporativeOpTO.setDeliveryDate(CommonsUtilities.convertStringtoDate(loggerDetail.getFilterValue()));
				}
			
			if(loggerDetail.getFilterName().equals(ReportConstant.CORPORATE_EVENT_TYPE) &&
					Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	) {
				corporativeOpTO.setCorporativeEventType(Integer.valueOf(loggerDetail.getFilterValue()));
				}
				
			if(loggerDetail.getFilterName().equals(ReportConstant.CODE_ISIN) &&
					Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	){
				corporativeOpTO.setSourceIsinCode(loggerDetail.getFilterValue());
				}
			
			if(loggerDetail.getFilterName().equals(ReportConstant.DEST_CODE_ISIN) &&
					Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	) {
				corporativeOpTO.setTargetIsinCode(loggerDetail.getFilterValue());
				}
			if(loggerDetail.getFilterName().equals(ReportConstant.REPORT_TYPE) &&
					Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	) {
				corporativeOpTO.setReportType(Integer.valueOf(loggerDetail.getFilterValue()));
				}
				
			if(loggerDetail.getFilterName().equals(ReportConstant.STATE_PARAM) &&
					Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	){
				corporativeOpTO.setState(Integer.valueOf(loggerDetail.getFilterValue()));
				}
		}
				
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		try {
		//GETTING ALL DATA FROM QUERY
		List<Object[]> listObjects = corporativeReportServiceBean.getDividensPaidByFrozenBalance(corporativeOpTO);
		
		Security sec = securitiesQueryServiceBean.getSecurityHelpServiceBean(corporativeOpTO.getSourceIsinCode());
		
		CorporativeOperation corporativeOperation = securitiesQueryServiceBean.find(CorporativeOperation.class, corporativeOpTO.getId());
		
		if(Validations.validateIsNotNullAndNotEmpty(listObjects) && listObjects.size()>0){
			existData = true;
		}
		
		XMLOutputFactory xof = XMLOutputFactory.newInstance();
		XMLStreamWriter xmlsw;
		
			xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(baos));
			//open document file
			xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");
			
			//REPORT TAG
			xmlsw.writeStartElement(ReportConstant.REPORT);
			
				//HEADER
				createHeaderReport(xmlsw,reportLogger);	
				
					createTagString(xmlsw, "issuerCode", sec.getIssuer().getIdIssuerPk());
					createTagString(xmlsw, "EMISOR", corporativeOperation.getIssuer().getBusinessName());
					createTagString(xmlsw, "ID_ISSUANCE_CODE_FK", sec.getIssuance().getIdIssuanceCodePk());
					createTagString(xmlsw, "VALOR", sec.getIdSecurityCodePk());
					createTagString(xmlsw, "CURRENCY", CurrencyType.get(sec.getCurrency()).getValue());
					createTagString(xmlsw, "CUTOFF_DATE", corporativeOperation.getCutoffDate().toString());
					createTagString(xmlsw, "DELIVERY_DATE", CommonsUtilities.convertDateToString(corporativeOperation.getDeliveryDate(),GeneralConstants.DATE_FORMAT_PATTERN));
					createTagString(xmlsw, "REGISTRY_DATE", CommonsUtilities.convertDateToString(corporativeOperation.getRegistryDate(),GeneralConstants.DATE_FORMAT_PATTERN));
					createTagString(xmlsw, "VALOR_NOMINAL", sec.getCurrentNominalValue().toString());
					createTagString(xmlsw, "status", CorporateProcessStateType.get(corporativeOperation.getState()).getValue());
					
					if(Validations.validateIsNotNullAndNotEmpty(corporativeOperation.getTaxFactor())){
						createTagString(xmlsw, "rateFactor", corporativeOperation.getTaxFactor().toString());
					}else{
						createTagString(xmlsw, "rateFactor", GeneralConstants.ZERO.toString());
					}
					createTagString(xmlsw, "indRound", corporativeOperation.getIndRound().toString());
			
			//BODY
			if(existData){
				createBodyReport(xmlsw, listObjects);
			}
		
			//END REPORT TAG
			xmlsw.writeEndElement();
			//close document
			xmlsw.writeEndDocument();
			xmlsw.flush();
	        xmlsw.close();
	      
		} catch (XMLStreamException | ServiceException e) {
			log.error(e.getMessage());
			e.printStackTrace();
			throw new RuntimeException();
		} 

		return baos;
	}
	
	public void createBodyReport(XMLStreamWriter xmlsw,List<Object[]> objConsolidateParentAccountList)  throws XMLStreamException{
		CorporativeOperationSubQueryTO copTO = new CorporativeOperationSubQueryTO();
		
		Object[] objtArray = objConsolidateParentAccountList.get(0);
		
		copTO.setCorporativeOperationId(Long.valueOf(objtArray[id_corporative_pk].toString()));
		copTO.setCorporativeProcessResultId(Long.valueOf(objtArray[cpr_id].toString()));
		copTO.setParticipantId(Long.valueOf(objtArray[ID_PARTICIPANT_PK].toString()));
		copTO.setHolderAccountId(Long.valueOf(objtArray[ID_HOLDER_ACCOUNT_PK].toString()));
		
		//GETTING ALL DATA FROM SUBQUERY
		List<Object[]> lstObjectSQ = corporativeReportServiceBean.getDividensPaidByFrozenBalanceSubQuery(copTO);
		
		xmlsw.writeStartElement("participant"); //START PARTICIPANT REPORT ELEMENT	

		for(Object[] obj : objConsolidateParentAccountList){
			xmlsw.writeStartElement("data"); //START DATA ELEMENT	
			createTagString(xmlsw, "participant_id", obj[ID_PARTICIPANT_PK]);
			createTagString(xmlsw, "participant", obj[PARTCIPANT]);
			createTagString(xmlsw, "account_number", obj[CUENTA]);
			createTagString(xmlsw, "holder", obj[RNT_DESCRIPCION]);
			createTagString(xmlsw, "SALDO_PRENDA", obj[SALDO_PRENDA]);
			createTagString(xmlsw, "SALDO_EMBARGO", obj[SALDO_EMBARGO]);
			createTagString(xmlsw, "SALDO_OPOSICION", obj[SALDO_OPOSICION]);
			createTagString(xmlsw, "SALDO_ENCAJE", obj[SALDO_ENCAJE]);
			createTagString(xmlsw, "SALDO_OTROS", obj[SALDO_OTROS]);
			createTagString(xmlsw, "SALDO_TOTAL", obj[SALDO_TOTAL]);
			createTagString(xmlsw, "BENEFICIO_PRENDA", obj[BENEFICIO_PRENDA]);
			createTagString(xmlsw, "BENEFICIO_EMBARGO", obj[BENEFICIO_EMBARGO]);
			createTagString(xmlsw, "BENEFICIO_OPOSICION", obj[BENEFICIO_OPOSICION]);
			createTagString(xmlsw, "BENEFICIO_ENCAJE", obj[BENEFICIO_ENCAJE]);
			createTagString(xmlsw, "BENEFICIO_OTROS", obj[BENEFICIO_OTROS]);
			createTagString(xmlsw, "BENEFICIO_TOTAL", obj[BENEFICIO_TOTAL]);
			xmlsw.writeEndElement(); //END DATA ELEMENT
		}	

		//DETAILS
		for(Object[] obj : lstObjectSQ){
			xmlsw.writeStartElement("row"); //START ROW ELEMENT	
			createTagString(xmlsw, "DOCUMENT_NUMBER", obj[DOCUMENT_NUMBER]);
			createTagString(xmlsw, "block_number", obj[block_number]);
			createTagString(xmlsw, "block_type", obj[block_type]);
			createTagString(xmlsw, "block_balance", obj[block_balance]);
			createTagString(xmlsw, "BENEFICIO", obj[BENEFICIO]);
			createTagString(xmlsw, "block_entity", obj[block_entity]);
			xmlsw.writeEndElement(); //END ROW ELEMENT
		}

		xmlsw.writeEndElement(); //END PARTICIPANT REPORT ELEMENT
	}
}
