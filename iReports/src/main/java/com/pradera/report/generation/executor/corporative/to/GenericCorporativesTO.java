package com.pradera.report.generation.executor.corporative.to;

public class GenericCorporativesTO {

	private String securities;
	private String cutDate;
	private String registryDate;
	private String processId;
	private String eventType;
	private String state;
	
	public String getSecurities() {
		return securities;
	}
	public void setSecurities(String securities) {
		this.securities = securities;
	}
	public String getCutDate() {
		return cutDate;
	}
	public void setCutDate(String cutDate) {
		this.cutDate = cutDate;
	}
	public String getRegistryDate() {
		return registryDate;
	}
	public void setRegistryDate(String registryDate) {
		this.registryDate = registryDate;
	}
	public String getProcessId() {
		return processId;
	}
	public void setProcessId(String processId) {
		this.processId = processId;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	
	
}
