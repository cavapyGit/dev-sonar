package com.pradera.report.generation.executor.settlement;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;

@ReportProcess(name = "ExLackOfSecuritiesOperationFundsOfBagReport")
public class ExLackOfSecuritiesOperationFundsOfBagReport extends GenericReport{

	private static final long serialVersionUID = 1L;
	
	@Inject PraderaLogger log;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	ParticipantServiceBean participantServiceBean;
	
	
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> currency = new HashMap<Integer, String>();
		Map<Integer,String> participantMnemonic = new HashMap<Integer, String>();
		Map<Integer,String> state = new HashMap<Integer, String>();
		
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		String scheduleTypeCode = null;
		String scheduleTypeDescription = null;
		String participantCode = null;
		String participantDescription = null;
		String currencyDescription = null;
		
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.PARTICIPANT_PARAM) && r.getFilterValue()!=null)
				participantCode = r.getFilterValue();
			if (r.getFilterName().equals(ReportConstant.SCHEDULE_TYPE))
				scheduleTypeCode = r.getFilterValue();
			if (r.getFilterName().equals(ReportConstant.CURRENCY))
				if (r.getFilterValue() != null){
					currencyDescription = r.getFilterDescription();
				}
		}
		
		try {
			for(Participant participant : participantServiceBean.getLisParticipantServiceBean(new Participant())) {
				participantMnemonic.put(participant.getIdParticipantPk().intValue(), participant.getMnemonic());
			}
			
			if(participantCode != null){
				Participant participant = participantServiceBean.find(Participant.class, new Long(participantCode));
				participantDescription = participant.getMnemonic() + " - " + participant.getDescription();
			}
			
			if(scheduleTypeCode != null)
				scheduleTypeDescription = parameterService.find(ParameterTable.class, new Integer(scheduleTypeCode)).getDescription();
			else
				scheduleTypeDescription = "TODOS";
			
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				currency.put(param.getParameterTablePk(), param.getParameterName());
			}
			
			filter.setMasterTableFk(MasterTableType.ESTADOS_OPERACION_MCN.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				state.put(param.getParameterTablePk(), param.getDescription());
			}
			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		parametersRequired.put("p_participant_mnemonic", participantMnemonic);
		parametersRequired.put("participant_description", participantDescription);
		parametersRequired.put("schedule_type_description", scheduleTypeDescription);
		parametersRequired.put("currency_description", currencyDescription);
		
		parametersRequired.put("map_currency", currency);
		parametersRequired.put("map_state", state);
		
		return parametersRequired;
	}

}
