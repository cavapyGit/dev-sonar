package com.pradera.report.generation.executor.issuances;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.securities.service.PaymentChronogramBySecurity;
import com.pradera.report.generation.executor.securities.to.SecuritiesByIssuanceAndIssuerTO;
import com.pradera.report.generation.facade.ReportGenerationLocalServiceFacade;
import com.pradera.report.generation.service.ReportUtilServiceBean;

@ReportProcess(name = "AssignmentISINAndCFICodeReport")
public class AssignmentISINAndCFICodeReport extends GenericReport{
	private static final long serialVersionUID = 1L;
	
	private static final String TEMPLATE_NAME_EXCEL = "securities/AssignmentISINAndCFICodeXls.jasper";

	@EJB
	private PaymentChronogramBySecurity securitiesService;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	private ReportGenerationLocalServiceFacade reportGenerationFacade;
	
	@EJB
	ReportUtilServiceBean reportUtilService;
	
	public AssignmentISINAndCFICodeReport() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		SecuritiesByIssuanceAndIssuerTO securitiesTO = new SecuritiesByIssuanceAndIssuerTO();
		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("securityCd")){
				if (listaLogger.get(i).getFilterValue()!=null && !listaLogger.get(i).getFilterValue().equals("0")){
					securitiesTO.setIdSecurityCodePk(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("isinCd")){
				if (listaLogger.get(i).getFilterValue()!=null && !listaLogger.get(i).getFilterValue().equals("0")){
					securitiesTO.setIdIsinCode(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("cfiCd")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					securitiesTO.setCfiCode(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("fsin_code")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					securitiesTO.setFsinCode(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("date_initial")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					securitiesTO.setInitialDate(CommonsUtilities.convertStringtoDate(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("date_end")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					securitiesTO.setFinalDate(CommonsUtilities.convertStringtoDate(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals(ReportConstant.CLASS_SECURITY_PARAM)){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					securitiesTO.setSecurityClass(Integer.parseInt(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals(ReportConstant.STATE_PARAM)){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					securitiesTO.setStateSecurityCode(Integer.parseInt(listaLogger.get(i).getFilterValue()));
				}
			}
		}

		String strQuery;
		if(securitiesTO.getStateSecurityCode()==null) {
			String queryAll = securitiesService.getQueryAssignmentISINAndCFICode(securitiesTO);
			strQuery =  queryAll;
		}else {
			strQuery = securitiesService.getQueryAssignmentISINAndCFICode(securitiesTO);
		}

		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> map_currency = new HashMap<Integer, String>();
		try {
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				map_currency.put(param.getParameterTablePk(), param.getDescription());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		parametersRequired.put("map_currency_mnemonic", map_currency);
		parametersRequired.put("str_query", strQuery);
		if(super.getReportLogger().getReportFormat().equals(ReportFormatType.XLS.getCode()))
			parametersRequired.put(ReportConstant.TEMPLATE_NAME, TEMPLATE_NAME_EXCEL);
		//parametersRequired.put(ReportConstant.CALL_GENERATE_DATA, BooleanType.YES.getBooleanValue());
		return parametersRequired;
	}

}
