package com.pradera.report.generation.executor.securities;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.securities.service.IssuerListReportServiceBean;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "SecuritiesRelationIssuerReport")
public class SecuritiesRelationIssuerReport extends GenericReport{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject  PraderaLogger log;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	private IssuerListReportServiceBean issuerServiceBean;

	public SecuritiesRelationIssuerReport() {
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParametersQueryReport() {
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		if (listaLogger == null) {
			listaLogger = new ArrayList<ReportLoggerDetail>();
		}

		/*ReportLoggerDetail loggerDetail = new ReportLoggerDetail();
		
		loggerDetail.setFilterName("idParicipante");
		if (listaLogger.get(2).getFilterValue()!=null){
			loggerDetail.setFilterValue(listaLogger.get(2).getFilterValue());
		}else{
			loggerDetail.setFilterValue("0");
		}
		listaLogger.add(loggerDetail);*/

	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		Map<String,Object> parametersRequired = new HashMap<>();
		/*ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> instrumentType = new HashMap<Integer, String>();
		Map<Integer,String> securitySource = new HashMap<Integer, String>();
		Map<Integer,String> currency = new HashMap<Integer, String>();
		Map<Integer,String> state = new HashMap<Integer, String>();
		Map<Integer,String> issuanceform = new HashMap<Integer, String>();*/
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		String issuerCode = null;
		String issuerDescription = null;
		//Map<Integer,String> securityClassDescription = new HashMap<Integer, String>();
		
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.ISSUER_PARAM) && r.getFilterValue()!=null)
				issuerCode = r.getFilterValue();
		}
		
		
		try {
			
			if(issuerCode != null){
				Issuer iss = issuerServiceBean.find(Issuer.class, issuerCode);
				issuerDescription = iss.getMnemonic() + " - " + iss.getBusinessName();
			}
			
			/*filter.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				instrumentType.put(param.getParameterTablePk(), param.getParameterName());
			}
			filter.setMasterTableFk(MasterTableType.SECURITY_SOURCE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				securitySource.put(param.getParameterTablePk(), param.getParameterName());
			}
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				currency.put(param.getParameterTablePk(), param.getParameterName());
			}
			filter.setMasterTableFk(MasterTableType.SECURITIES_STATE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				state.put(param.getParameterTablePk(), param.getParameterName());
			}
			filter.setMasterTableFk(MasterTableType.ISSUANCE_TYPE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				issuanceform.put(param.getParameterTablePk(), param.getParameterName());
			}
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				securityClassDescription.put(param.getParameterTablePk(), param.getDescription());
			}*/
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		parametersRequired.put("issuerDescription", issuerDescription);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		/*parametersRequired.put("p_instrumentType", instrumentType);
		parametersRequired.put("p_securitySource", securitySource);
		parametersRequired.put("p_currency", currency);
		parametersRequired.put("p_state", state);
		parametersRequired.put("p_issuanceform", issuanceform);*/
		//parametersRequired.put("p_securityClassDescription", securityClassDescription);
		// TODO Auto-generated method stub
		return parametersRequired;
	}

}
