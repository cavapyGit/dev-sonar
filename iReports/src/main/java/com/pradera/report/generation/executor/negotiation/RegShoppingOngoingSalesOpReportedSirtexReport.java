package com.pradera.report.generation.executor.negotiation;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.negotiation.service.NegotiationReportServiceBean;
import com.pradera.report.generation.executor.negotiation.to.GenericNegotiationTO;

@ReportProcess(name="RegShoppingOngoingSalesOpReportedSirtexReport")
public class RegShoppingOngoingSalesOpReportedSirtexReport extends GenericReport{
	private static final long serialVersionUID = 1L;
	
	@Inject
	private PraderaLogger log;
	@EJB
	private ParameterServiceBean parameterService;
	@EJB
	private NegotiationReportServiceBean negotiationReportServiceBean;
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {

		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		GenericNegotiationTO gfto = new GenericNegotiationTO();
		
		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("participant")){
				if (listaLogger.get(i).getFilterValue()!=null && !listaLogger.get(i).getFilterValue().equals("0")){
					gfto.setParticipant(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("report_of")){
				if (listaLogger.get(i).getFilterValue()!=null){
					gfto.setReportOf(listaLogger.get(i).getFilterValue().toString());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("format_bcb")){
				if (listaLogger.get(i).getFilterValue()!=null && !listaLogger.get(i).getFilterValue().equals("0")){
					gfto.setFormatBcb(listaLogger.get(i).getFilterValue());
				}
			}
		}
		
		String strQuery = negotiationReportServiceBean.getRegShoppingOngoingSalesOpReportedSirtex(gfto); 
		
		Map<String,Object> parametersRequired = new HashMap<>();
		
		parametersRequired.put("str_Query", strQuery);
		parametersRequired.put("initialDt", gfto.getInitialDate());
		parametersRequired.put("finalDt", gfto.getFinalDate());
		
		return parametersRequired;
		
	}
	
}
