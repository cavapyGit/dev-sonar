package com.pradera.report.generation.executor.account.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class PortafolioDetailCustodianTO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String instrumentSecClass;
	private String idSecurityCode;
	private BigDecimal marketRate;
	private BigDecimal marketPrice;
	private Date dateMarketFact;
	private String dateMarketFactString;
	private Long cui;
	private String secCodeOnCustody;
	private Integer economicActivity;
	private String holderDesc;
	private String fundType;
	private BigDecimal totalBalance;
	private Integer documentType;
	private String documentNumber;
	/*AFP AND INSURANCE*/
	private Integer securityClass;
	private BigDecimal initialNominalValue;
	private BigDecimal NominalValueOrig;
	private BigDecimal markPriceOrig;
	private BigDecimal totalAmountOrig;
	private Integer issuanceForm;
	private Long holderAccountPk;
	private String idIssuer;
	private List<String> listFundTypes;
	private String lstFundTypes;
	private Integer transferNumber;
	private String transferNumberString;
	/*AGB, SAFI AND FIC*/
	private Date informationDt;
	private String custodio;
	private String fundAdmin;
	private String indReporto;
	private String valuatorCode;
	private Integer reportFormat;
	private Long reportPk;
	
	public Integer getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}
	public String getIdSecurityCode() {
		return idSecurityCode;
	}
	public void setIdSecurityCode(String idSecurityCode) {
		this.idSecurityCode = idSecurityCode;
	}
	public BigDecimal getMarketRate() {
		return marketRate;
	}
	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}
	public BigDecimal getMarketPrice() {
		return marketPrice;
	}
	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}
	public BigDecimal getTotalBalance() {
		return totalBalance;
	}
	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}
	public BigDecimal getTotalAmountOrig() {
		return totalAmountOrig;
	}
	public void setTotalAmountOrig(BigDecimal totalAmountOrig) {
		this.totalAmountOrig = totalAmountOrig;
	}
	public Integer getIssuanceForm() {
		return issuanceForm;
	}
	public void setIssuanceForm(Integer issuanceForm) {
		this.issuanceForm = issuanceForm;
	}
	public Long getCui() {
		return cui;
	}
	public void setCui(Long cui) {
		this.cui = cui;
	}
	public Date getDateMarketFact() {
		return dateMarketFact;
	}
	public void setDateMarketFact(Date dateMarketFact) {
		this.dateMarketFact = dateMarketFact;
	}
	public String getInstrumentSecClass() {
		return instrumentSecClass;
	}
	public void setInstrumentSecClass(String instrumentSecClass) {
		this.instrumentSecClass = instrumentSecClass;
	}
	public BigDecimal getInitialNominalValue() {
		return initialNominalValue;
	}
	public void setInitialNominalValue(BigDecimal initialNominalValue) {
		this.initialNominalValue = initialNominalValue;
	}
	public BigDecimal getNominalValueOrig() {
		return NominalValueOrig;
	}
	public void setNominalValueOrig(BigDecimal nominalValueOrig) {
		NominalValueOrig = nominalValueOrig;
	}
	public BigDecimal getMarkPriceOrig() {
		return markPriceOrig;
	}
	public void setMarkPriceOrig(BigDecimal markPriceOrig) {
		this.markPriceOrig = markPriceOrig;
	}
	public String getSecCodeOnCustody() {
		return secCodeOnCustody;
	}
	public void setSecCodeOnCustody(String secCodeOnCustody) {
		this.secCodeOnCustody = secCodeOnCustody;
	}
	public String getIdIssuer() {
		return idIssuer;
	}
	public void setIdIssuer(String idIssuer) {
		this.idIssuer = idIssuer;
	}
	public Long getHolderAccountPk() {
		return holderAccountPk;
	}
	public void setHolderAccountPk(Long holderAccountPk) {
		this.holderAccountPk = holderAccountPk;
	}
	public String getFundType() {
		return fundType;
	}
	public void setFundType(String fundType) {
		this.fundType = fundType;
	}
	public Integer getEconomicActivity() {
		return economicActivity;
	}
	public void setEconomicActivity(Integer economicActivity) {
		this.economicActivity = economicActivity;
	}
	public String getLstFundTypes() {
		return lstFundTypes;
	}
	public void setLstFundTypes(String lstFundTypes) {
		this.lstFundTypes = lstFundTypes;
	}
	public String getHolderDesc() {
		return holderDesc;
	}
	public void setHolderDesc(String holderDesc) {
		this.holderDesc = holderDesc;
	}
	public Integer getTransferNumber() {
		return transferNumber;
	}
	public void setTransferNumber(Integer transferNumber) {
		this.transferNumber = transferNumber;
	}
	public List<String> getListFundTypes() {
		return listFundTypes;
	}
	public void setListFundTypes(List<String> listFundTypes) {
		this.listFundTypes = listFundTypes;
	}
	public Date getInformationDt() {
		return informationDt;
	}
	public void setInformationDt(Date informationDt) {
		this.informationDt = informationDt;
	}
	public String getCustodio() {
		return custodio;
	}
	public void setCustodio(String custodio) {
		this.custodio = custodio;
	}
	public String getFundAdmin() {
		return fundAdmin;
	}
	public void setFundAdmin(String fundAdmin) {
		this.fundAdmin = fundAdmin;
	}
	public String getIndReporto() {
		return indReporto;
	}
	public void setIndReporto(String indReporto) {
		this.indReporto = indReporto;
	}
	public String getValuatorCode() {
		return valuatorCode;
	}
	public void setValuatorCode(String valuatorCode) {
		this.valuatorCode = valuatorCode;
	}
	public String getDocumentNumber() {
		return documentNumber;
	}
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	public Integer getDocumentType() {
		return documentType;
	}
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}
	public String getDateMarketFactString() {
		return dateMarketFactString;
	}
	public void setDateMarketFactString(String dateMarketFactString) {
		this.dateMarketFactString = dateMarketFactString;
	}
	/**
	 * @return the reportFormat
	 */
	public Integer getReportFormat() {
		return reportFormat;
	}
	/**
	 * @param reportFormat the reportFormat to set
	 */
	public void setReportFormat(Integer reportFormat) {
		this.reportFormat = reportFormat;
	}
	public Long getReportPk() {
		return reportPk;
	}
	public void setReportPk(Long reportPk) {
		this.reportPk = reportPk;
	}
	public String getTransferNumberString() {
		return transferNumberString;
	}
	public void setTransferNumberString(String transferNumberString) {
		this.transferNumberString = transferNumberString;
	}

}
