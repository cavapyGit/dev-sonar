package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import com.pradera.model.report.ReportLogger;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "EntriesVoluntaryAccountIssuerReport")
public class EntriesVoluntaryAccountIssuerReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {

		Map<String, Object> parametersRequired = new HashMap<>();

		// TODO Auto-generated method stub
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		return parametersRequired;
	}
	
}
