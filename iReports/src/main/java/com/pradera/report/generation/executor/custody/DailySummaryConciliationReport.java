package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.management.RuntimeErrorException;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.executor.custody.to.ConciliationDailyOperationsTO;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "DailySummaryConciliationReport")
public class DailySummaryConciliationReport extends GenericReport {


	/**
	 * 
	 */
	private static final long serialVersionUID = -8541348738631432680L;

	@Inject
	PraderaLogger log;

	@EJB
	private ParameterServiceBean parameterService;

	@EJB
	CustodyReportServiceBean custodyReportServiceBean;

	@EJB
	HolidayQueryServiceBean holidayQueryServiceBean;

	private String query;

	private String queryBalance;

	private String querySubTotal;

	private String queryCutDateTotal;

	public DailySummaryConciliationReport() {
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParametersQueryReport() {
	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		Map<String, Object> parametersRequired = new HashMap<>();
		ParameterTableTO filter = new ParameterTableTO();
		Map<Integer, String> currency = new HashMap<Integer, String>();
		Map<Integer, String> currencyMnemonic = new HashMap<Integer, String>();

		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();

		ConciliationDailyOperationsTO to = new ConciliationDailyOperationsTO();

		for (ReportLoggerDetail r : loggerDetails) {
			//System.out.println("Nombre de filtro:"+r.getFilterName()+"-->"+r.getFilterValue());
			if (r.getFilterName().equals(ReportConstant.PARTICIPANT_PARAM))
				if (r.getFilterValue() != null) {
					to.setParticipant(r.getFilterValue());
					to.setDesParticipant(r.getFilterDescription());
				}
			if (r.getFilterName().equals(ReportConstant.CUI_CODE_PARAM))
				if (r.getFilterValue() != null) {
					to.setCui_code(r.getFilterValue());
					to.setDescCui_code(r.getFilterDescription());
				}
			if (r.getFilterName().equals(ReportConstant.HOLDER_ACCOUNT))
				if (r.getFilterValue() != null) {//es pk de holder_account
					to.setHolder_account(r.getFilterValue());
					to.setDesHolder_account(r.getFilterDescription());
				}
			if (r.getFilterName().equals(ReportConstant.ISSUER_PARAM))
				if (r.getFilterValue() != null) {
					to.setIssuer(r.getFilterValue());
					to.setDescIssuer(r.getFilterDescription());
				}
			if (r.getFilterName().equals(ReportConstant.SECURITY_CLASS_PARAM))
				if (r.getFilterValue() != null) {
					to.setSecurity_class(r.getFilterValue());
					to.setDescSecurity_class(r.getFilterDescription());
				}

			if (r.getFilterName().equals(ReportConstant.SECURITY_CODE))
				if (r.getFilterValue() != null) {
					to.setSecurity_code(r.getFilterValue());
				}
			if (r.getFilterName().equals(ReportConstant.PORTOFOLO_CODE_PARAM))
				if (r.getFilterValue() != null) {
					to.setPortofolio_code(r.getFilterValue());
					to.setDescPortofolio_code(r.getFilterDescription());
				}

			if (r.getFilterName().equals(ReportConstant.CURRENCY_TYPE_PARAM))
				if (r.getFilterValue() != null) {
					to.setCurrency_code(r.getFilterValue());
					to.setDescCurrency_code(r.getFilterDescription());
				}
			if (r.getFilterName().equals(ReportConstant.DATE_PARAM)) {
				to.setDate(r.getFilterValue());
			}
		}
		Date beforeDate = CommonsUtilities.addDate(CommonsUtilities.convertStringtoDate(to.getDate(), "dd/MM/yyyy"), -1);
		String strBeforeDate = CommonsUtilities.convertDatetoString(beforeDate);
		to.setBeforeDate(strBeforeDate);
		if (to.getPortofolio_code().equals("132")) {
			query = getQuerySqlReportConciliation(to); 
			/** Primer sub reporte */
			queryBalance = getQuerySumBalance(to);
			/** Segundo sub reporte --> sub total */
			querySubTotal = getQuerySubTotal(to);
			/** Tercer sub reporte */
			queryCutDateTotal = getQueryCutDate(to);
			parametersRequired.put("pf_portofolio_type", "ANOTACION EN CUENTA");
		} else {
			query = getQuerySqlReportConciliationPhysical(to); 
			queryBalance = getQuerySumBalancePhysical(to);
			//Corregir este script
			querySubTotal = getQuerySubTotalPhysical(to);
			queryCutDateTotal = getQueryCutDatePhysical(to);
			parametersRequired.put("pf_portofolio_type", "FISICO");
		}
		try {
			custodyReportServiceBean.getExchangeRate(to.getDate());
		} catch (ServiceException e) {
			if (e.getErrorService().equals(ErrorServiceType.NOT_EXCHANGE_RATE)) {
				throw new RuntimeErrorException(null, ReportConstant.ERROR_EXCHANGE_RATE + " " + to.getDate());
			}
			log.error(e.getMessage());
		}

		try {
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for (ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				currency.put(param.getParameterTablePk(), param.getParameterName());
				currencyMnemonic.put(param.getParameterTablePk(), param.getDescription());
			}

		} catch (Exception ex) {
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
			currency.put(param.getParameterTablePk(), param.getParameterName());
			currencyMnemonic.put(param.getParameterTablePk(), param.getDescription());
		}
		/*Lista de monedas para categorizar*/
		parametersRequired.put("p_currency_mnemonic", currencyMnemonic);
		parametersRequired.put("p_currency", currency);
		
		parametersRequired.put("participant_mnemonic", to.getDesParticipant());
		if(to.getCui_code()!=null)
			parametersRequired.put("cui_code",to.getCui_code()+" - "+to.getDescCui_code());
		
		if(to.getHolder_account()!=null)
			parametersRequired.put("holder_account_number",to.getHolder_account()+" - "+to.getDesHolder_account());
		
		if(to.getIssuer()!=null)
			parametersRequired.put("issuer_mnemonic",to.getIssuer()+" - "+to.getDescIssuer());
		
		parametersRequired.put("security_class", to.getDescSecurity_class()==null?"TODOS":to.getDescSecurity_class());
		parametersRequired.put("security_code", to.getSecurity_code()==null?"TODOS":to.getSecurity_code());
		parametersRequired.put("currency", to.getDescCurrency_code());
		parametersRequired.put("date", to.getDate());
		parametersRequired.put("query", query);
		/** Primer sub reporte */
		parametersRequired.put("query_total", queryBalance);
		/** Segundo sub reporte */
		parametersRequired.put("query_total1", querySubTotal);
		/** Tercer sub reporte */
		parametersRequired.put("query_total2", queryCutDateTotal);
        
		System.out.println("QUERY PRINCIPAL:"+query);
		System.out.println("QUERY TOTAL BALANCE :"+queryBalance);
		System.out.println("QUERY  SUB TOTAL:" + querySubTotal);
		System.out.println("QUERY  CUT DATE:" + queryCutDateTotal);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		return parametersRequired;
	}
	/**
	 * Reporte de conciliacion por valor
	 * 
	 * @param to
	 * @return
	 */
	private String getQuerySqlReportConciliation(ConciliationDailyOperationsTO to) {
		StringBuilder builder = new StringBuilder();
		builder.append("SELECT ");
		builder.append("  SE.CURRENCY, ");
		builder.append("  MT.MOVEMENT_NAME MOVIMIENTO, ");
		builder.append("  DECODE(MT.IND_MOVEMENT_CLASS,1,SUM(HMM.MOVEMENT_QUANTITY),NULL) CANTIDAD_INGRESO, ");
		builder.append("  DECODE(MT.IND_MOVEMENT_CLASS,2,SUM(HMM.MOVEMENT_QUANTITY),NULL) CANTIDAD_EGRESO, ");
		builder.append("  DECODE(MT.IND_MOVEMENT_CLASS,1,SUM(HMM.MOVEMENT_QUANTITY*HAM.NOMINAL_VALUE),NULL) VALOR_NOMINAL_INGRESO, ");
		builder.append("  DECODE(MT.IND_MOVEMENT_CLASS,2,SUM(HMM.MOVEMENT_QUANTITY*HAM.NOMINAL_VALUE),NULL) VALOR_NOMINAL_EGRESO, ");
		builder.append("  DECODE(MT.IND_MOVEMENT_CLASS,1,SUM(HMM.MOVEMENT_QUANTITY*ROUND(HMM.MARKET_PRICE,2)),NULL) PRECIO_MERCADO_INGRESO, ");
		builder.append("  DECODE(MT.IND_MOVEMENT_CLASS,2,SUM(HMM.MOVEMENT_QUANTITY*ROUND(HMM.MARKET_PRICE,2)),NULL) PRECIO_MERCADO_EGRESO, ");
		builder.append("  (CASE  ");
		builder.append("        WHEN SE.CURRENCY = 127  ");
		builder.append("          THEN (1) /  ");
		builder.append("            nvl(( SELECT D.BUY_PRICE  ");
		builder.append("                  FROM DAILY_EXCHANGE_RATES D  ");
		builder.append("                  WHERE TRUNC(D.DATE_RATE) = to_date('" + to.getDate() + "','dd/mm/yyyy')  ");
		builder.append("                    AND D.ID_CURRENCY = 430  ");
		builder.append("                    AND D.ID_SOURCE_INFORMATION = 212  ");
		builder.append("            ),1)  ");
		builder.append("        WHEN SE.CURRENCY IN (1734,1304)  ");
		builder.append("          THEN (1) *  ");
		builder.append("            nvl(( SELECT D.BUY_PRICE  ");
		builder.append("                  FROM DAILY_EXCHANGE_RATES D  ");
		builder.append("                  WHERE TRUNC(D.DATE_RATE) = to_date('" + to.getDate() + "','dd/mm/yyyy')  ");
		builder.append("                    AND D.ID_CURRENCY = SE.CURRENCY  ");
		builder.append("                    AND D.ID_SOURCE_INFORMATION = 212  ");
		builder.append("            ),1) /  ");
		builder.append("            nvl(( SELECT D.BUY_PRICE  ");
		builder.append("                  FROM DAILY_EXCHANGE_RATES D  ");
		builder.append("                  WHERE TRUNC(D.DATE_RATE) = to_date('" + to.getDate() + "','dd/mm/yyyy')  ");
		builder.append("                    AND D.ID_CURRENCY = 430  ");
		builder.append("                    AND D.ID_SOURCE_INFORMATION = 212  ");
		builder.append("            ),1)  ");
		builder.append("        WHEN SE.CURRENCY IN (430,1853)  ");
		builder.append("          THEN (1)  ");
		builder.append("        ELSE (1)  ");
		builder.append("      END) CONVERTION_FACTOR ");
		builder.append("  ");
		builder.append(" FROM HOLDER_ACCOUNT_MOVEMENT HAM, HOLDER_MARKETFACT_MOVEMENT HMM, MOVEMENT_TYPE MT, SECURITY SE, PARTICIPANT P ");
		builder.append(" WHERE HAM.ID_HOLDER_ACCOUNT_MOVEMENT_PK = HMM.ID_HOLDER_ACCOUNT_MOVEMENT_FK ");
		builder.append("  AND HAM.ID_PARTICIPANT_FK = P.ID_PARTICIPANT_PK ");
		builder.append("  AND HAM.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK ");
		
		builder.append(" AND HMM.ID_HOLDER_ACCOUNT_FK = HAM.ID_HOLDER_ACCOUNT_FK ");
		builder.append(" AND HAM.ID_PARTICIPANT_FK = P.ID_PARTICIPANT_PK ");
		builder.append(" AND HAM.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK ");
		
		builder.append("  AND HAM.ID_MOVEMENT_TYPE_FK = MT.ID_MOVEMENT_TYPE_PK ");
		builder.append("  AND MT.IND_MOVEMENT_CLASS IN (1,2) ");
		/*Filtros de busqueda*/
		if (to.getParticipant() != null)
			builder.append("  AND P.ID_PARTICIPANT_PK = " + to.getParticipant());
		if(to.getHolder_account()!=null)
			builder.append(" AND HAM.ID_HOLDER_ACCOUNT_FK = "+to.getHolder_account() );
		if (to.getIssuer() != null)
			builder.append(" AND SE.ID_ISSUER_FK = '" + to.getIssuer()+"'");
		if (to.getSecurity_class() != null)
			builder.append(" AND SE.SECURITY_CLASS = " + to.getSecurity_class());
		if(to.getSecurity_code()!=null)
			builder.append(" AND SE.ID_SECURITY_CODE_PK = '"+to.getSecurity_code()+"'");
		if (to.getCurrency_code() != null)
			builder.append(" AND SE.CURRENCY = " + to.getCurrency_code());
		

		builder.append(" AND HAM.MOVEMENT_DATE >= TRUNC(TO_DATE('" + to.getDate() + "', 'DD/MM/YYYY'), 'DD') ");
		builder.append(" AND TRUNC(TO_DATE('" + to.getDate() + "', 'DD/MM/YYYY'), 'DD') + 1 > HAM.MOVEMENT_DATE ");
		builder.append(" AND TRUNC(TO_DATE('" + to.getDate() + "', 'DD/MM/YYYY'), 'DD') = TO_DATE('" + to.getDate()	+ "', 'DD/MM/YYYY') ");

		builder.append("  AND MT.ID_MOVEMENT_TYPE_PK NOT IN (300223,300224,300225,300226,300227,300228,300229,300632,300633) ");
		builder.append("  AND ( ");
		builder.append("    (EXISTS ");
		builder.append("      ( SELECT had.ID_HOLDER_FK ");
		builder.append("        FROM HOLDER_ACCOUNT_DETAIL HAD ");
		builder.append("        WHERE 1 = 1 ");
		if(to.getCui_code()!=null)
			builder.append(" AND HAD.ID_HOLDER_FK = "+to.getCui_code());
		builder.append("        AND had.ID_HOLDER_ACCOUNT_FK = HAM.ID_HOLDER_ACCOUNT_FK ");
		builder.append(" ))) ");
		builder.append("GROUP BY ");
		builder.append("  SE.CURRENCY, ");
		builder.append("  MT.MOVEMENT_NAME, ");
		builder.append("  MT.IND_MOVEMENT_CLASS ");
		builder.append("ORDER BY 1  ");
		return builder.toString();
	}
	
	// Subreporte 0
		public String getQuerySumBalance(ConciliationDailyOperationsTO to) {
			StringBuilder builder = new StringBuilder();
			builder.append(" SELECT ");
			builder.append("    sum(MFV.TOTAL_BALANCE) AS TOTAL_BALANCE, ");
			builder.append("    SUM(MFV.AVAILABLE_BALANCE) AS AVAILABLE_BALANCE, ");
			builder.append("    SUM(MFV.TRANSIT_BALANCE) AS TRANSIT_BALANCE, ");
			builder.append("    SUM( MFV.PAWN_BALANCE) AS PAWN_BALANCE, ");
			builder.append("    SUM(MFV.BAN_BALANCE) AS BAN_BALANCE, ");
			builder.append("    SUM(MFV.OTHER_BLOCK_BALANCE)AS OTHER_BLOCK_BALANCE, ");
			builder.append("    SUM(MFV.ACCREDITATION_BALANCE) AS ACCREDITATION_BALANCE, ");
			builder.append("    SUM(MFV.REPORTED_BALANCE) AS REPORTED_BALANCE, ");
			builder.append("    SUM(REPORTING_BALANCE) AS REPORTING_BALANCE, ");

			builder.append("    SUM(MFV.TOTAL_BALANCE) AS CONTABLE,");
			builder.append("    SUM(AVAILABLE_BALANCE+TRANSIT_BALANCE+PAWN_BALANCE+BAN_BALANCE+OTHER_BLOCK_BALANCE+ACCREDITATION_BALANCE+REPORTING_BALANCE) AS SALDO,");

			builder.append("    SUM(MFV.REPORTED_BALANCE) AS REPORTADO, ");
			builder.append("    SUM(MFV.REPORTING_BALANCE) AS REPORTADOR");
			builder.append(" FROM MARKET_FACT_VIEW MFV ");
			
			if (to.getParticipant() != null)
				builder.append("    INNER JOIN  PARTICIPANT P ON P.ID_PARTICIPANT_PK = MFV.ID_PARTICIPANT_PK ");
			builder.append("    INNER JOIN  SECURITY S    ON S.ID_SECURITY_CODE_PK = MFV.ID_SECURITY_CODE_PK ");
			
			builder.append(" WHERE ");
			builder.append("    s.security_type in ");
			builder.append("    ( ");
			builder.append("       1762, ");
			builder.append("       1763  ");
			builder.append("    ) ");
			builder.append("    AND MFV.CUT_DATE >= TRUNC(TO_DATE('" + to.getDate() + "', 'DD/MM/YYYY'), 'DD') ");
			builder.append("    AND MFV.CUT_DATE < TRUNC(TO_DATE('" + to.getDate() + "', 'DD/MM/YYYY'), 'DD') + 1  ");
			builder.append("    AND TRUNC(TO_DATE('" + to.getDate() + "', 'DD/MM/YYYY'), 'DD') = TO_DATE('" + to.getDate()+ "', 'DD/MM/YYYY') ");
			builder.append("    AND ");
			builder.append("    ( ");
			builder.append("       MFV.total_balance > 0  ");
			builder.append("       or MFV.reported_balance > 0  ");
			builder.append("       or MFV.purchase_balance > 0 ");
			builder.append("    ) ");
			builder.append("  AND ( ");
			builder.append("     (EXISTS ");
			builder.append("      ( SELECT had.ID_HOLDER_FK ");
			builder.append("        FROM HOLDER_ACCOUNT_DETAIL had ");
			builder.append("        WHERE 1 = 1 ");
			if(to.getCui_code()!=null)
				builder.append(" AND HAD.ID_HOLDER_FK = "+to.getCui_code());
			builder.append(" AND had.ID_HOLDER_ACCOUNT_FK = MFV.ID_HOLDER_ACCOUNT_PK))) ");
			/*Filtros de busqueda*/
			if (to.getParticipant() != null)
				builder.append(" AND MFV.ID_PARTICIPANT_PK = " + to.getParticipant());
			if(to.getHolder_account()!=null) {
				//builder.append("  INNER JOIN HOLDER_ACCOUNT HA  ON HA.ID_HOLDER_ACCOUNT_PK = MFV.ID_HOLDER_ACCOUNT_PK ");
				builder.append("  AND MFV.ID_HOLDER_ACCOUNT_PK = "+to.getHolder_account() );
			}
			if (to.getIssuer() != null)
				builder.append(" AND S.ID_ISSUER_FK = '" + to.getIssuer()+"'");
			if (to.getSecurity_class() != null)
				builder.append(" AND S.SECURITY_CLASS = " + to.getSecurity_class());
			if(to.getSecurity_code()!=null)
				builder.append(" AND S.ID_SECURITY_CODE_PK = '"+to.getSecurity_code()+"'");
			if (to.getCurrency_code() != null)
				builder.append(" AND S.CURRENCY = " + to.getCurrency_code());

			return builder.toString();
		}
		
		/**
		 * Script del segundo sub reporte.
		 */
		public String getQuerySubTotal(ConciliationDailyOperationsTO to) {
			StringBuilder builder = new StringBuilder();
			builder.append(" SELECT ");
			builder.append("  0 FECHA_CORTE, ");
			builder.append("  'CARTERA DEL DIA ANTERIOR' DESCRIPTION, ");
			builder.append("  NVL(SUM(MFV.TOTAL_BALANCE),0) TOTAL_VALORES, ");
			builder.append("  NVL(SUM( ");
			builder.append("    S.INITIAL_NOMINAL_VALUE * MFV.TOTAL_BALANCE * ");
			builder.append("     CASE ");
			builder.append("        WHEN S.CURRENCY = 127 THEN 1 ");
			builder.append("        WHEN S.CURRENCY IN (1734,1304,430) THEN 1 * ");
			builder.append("          nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("            WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') -1 ");
			builder.append("              AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("        WHEN S.CURRENCY IN (1853) THEN 1 * ");
			builder.append("				  nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("            WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') -1 ");
			builder.append("              AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("        ELSE 1 ");
			builder.append("    END),0) TOTAL_BOB, ");
			builder.append("  NVL(SUM( ");
			builder.append("    S.INITIAL_NOMINAL_VALUE * MFV.TOTAL_BALANCE * ");
			builder.append("      CASE ");
			builder.append("        WHEN S.CURRENCY = 127 THEN 1 / ");
			builder.append("            nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                  WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') -1 ");
			builder.append("                    AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("        WHEN S.CURRENCY IN (1734,1304) THEN 1 * ");
			builder.append("            nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                  WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') -1 ");
			builder.append("                    AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) / ");
			builder.append("            nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                  WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') -1 ");
			builder.append("                    AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("        WHEN S.CURRENCY IN (430,1853) THEN 1 ");
			builder.append("        ELSE 1 ");
			builder.append("  END),0) TOTAL_USD, ");
			builder.append("  NVL(SUM( ");
			builder.append("    ROUND(MFV.MARKET_PRICE,2) * MFV.TOTAL_BALANCE * ");
			builder.append("      CASE ");
			builder.append("        WHEN S.CURRENCY = 127 THEN 1 ");
			builder.append("        WHEN S.CURRENCY IN (1734,1304,430) THEN 1 * ");
			builder.append("          nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("            WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') -1 ");
			builder.append("              AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("        WHEN S.CURRENCY IN (1853) THEN 1 * ");
			builder.append("				  nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("            WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') -1 ");
			builder.append("              AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("        ELSE 1 ");
			builder.append("      END),0) TOTAL_PRECIO_MRKT_BOB, ");
			builder.append("  NVL(SUM( ");
			builder.append("    ROUND(MFV.MARKET_PRICE,2) * MFV.TOTAL_BALANCE * ");
			builder.append("      CASE ");
			builder.append("        WHEN S.CURRENCY = 127 THEN 1 / ");
			builder.append("            nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                  WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') -1 ");
			builder.append("                    AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("        WHEN S.CURRENCY IN (1734,1304) THEN 1 * ");
			builder.append("            nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                  WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') -1 ");
			builder.append("                    AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) / ");
			builder.append("            nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                  WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') -1 ");
			builder.append("                    AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("        WHEN S.CURRENCY IN (430,1853) THEN 1 ");
			builder.append("        ELSE 1 ");
			builder.append("      END),0) TOTAL_PRECIO_MRKT_USD ");
			builder.append("FROM MARKET_FACT_VIEW MFV ");
			builder.append("  INNER JOIN SECURITY S   ON S.ID_SECURITY_CODE_PK = MFV.ID_SECURITY_CODE_PK ");
			builder.append(" WHERE 1 = 1 ");
			//builder.append("  AND TRUNC(MFV.CUT_DATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') -1 ");
			builder.append("  AND (MFV.CUT_DATE >= TRUNC(TO_DATE('" + to.getBeforeDate() + "', 'DD/MM/YYYY'), 'DD') ");
			builder.append("  AND TRUNC(TO_DATE('" + to.getBeforeDate() + "', 'DD/MM/YYYY'), 'DD') + 1 > MFV.CUT_DATE  ");
			builder.append("  AND TRUNC(TO_DATE('" + to.getBeforeDate() + "', 'DD/MM/YYYY'), 'DD') = TO_DATE('"+ to.getBeforeDate() + "', 'DD/MM/YYYY')) ");
			
			/*Condiciones segun filtros*/
			if(to.getParticipant()!=null)
				builder.append(" AND MFV.ID_PARTICIPANT_PK = "+to.getParticipant());
			if(to.getHolder_account()!=null) 
				builder.append(" AND MFV.ID_HOLDER_ACCOUNT_PK = "+to.getHolder_account() );
			if (to.getIssuer() != null)
				builder.append(" AND S.ID_ISSUER_FK = '" + to.getIssuer()+"'");
			if (to.getSecurity_class() != null)
				builder.append(" AND S.SECURITY_CLASS = " + to.getSecurity_class());
			if(to.getSecurity_code()!=null)
				builder.append(" AND S.ID_SECURITY_CODE_PK = '"+to.getSecurity_code()+"'");
			if (to.getCurrency_code() != null)
				builder.append(" AND S.CURRENCY = " + to.getCurrency_code());
			
			
			builder.append("  AND ( ");
			builder.append("  (EXISTS ");
			builder.append("      ( SELECT had.ID_HOLDER_FK ");
			builder.append("        FROM HOLDER_ACCOUNT_DETAIL had ");
			builder.append("        WHERE 1 = 1");
			if(to.getCui_code()!=null)
				builder.append(" AND HAD.ID_HOLDER_FK = "+to.getCui_code());
			builder.append("        AND had.ID_HOLDER_ACCOUNT_FK = MFV.ID_HOLDER_ACCOUNT_PK))) ");
			//**********************************************************************************************************//
			builder.append("UNION ALL ");
			builder.append("SELECT  ");
			builder.append("  1 FECHA_CORTE, ");
			builder.append("  'CARTERA AL INICIO DEL DIA' DESCRIPTION, ");
			builder.append("  NVL(SUM(MFV.TOTAL_BALANCE),0) TOTAL_VALORES, ");
			builder.append("  NVL(SUM( ");
			builder.append("    S.INITIAL_NOMINAL_VALUE * MFV.TOTAL_BALANCE * ");
			builder.append("     CASE ");
			builder.append("        WHEN S.CURRENCY = 127 THEN 1 ");
			builder.append("        WHEN S.CURRENCY IN (1734,1304,430) THEN 1 * ");
			builder.append("          nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("            WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("              AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("        WHEN S.CURRENCY IN (1853) THEN 1 * ");
			builder.append("				  nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("            WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("              AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("        ELSE 1 ");
			builder.append("    END),0) TOTAL_BOB, ");
			builder.append("  NVL(SUM( ");
			builder.append("    S.INITIAL_NOMINAL_VALUE * MFV.TOTAL_BALANCE * ");
			builder.append("      CASE ");
			builder.append("        WHEN S.CURRENCY = 127 THEN 1 / ");
			builder.append("            nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                  WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("                    AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("        WHEN S.CURRENCY IN (1734,1304) THEN 1 * ");
			builder.append("            nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                  WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("                    AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) / ");
			builder.append("            nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                  WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("                    AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("        WHEN S.CURRENCY IN (430,1853) THEN 1 ");
			builder.append("        ELSE 1 ");
			builder.append("  END),0) TOTAL_USD, ");
			builder.append("  NVL(SUM( ");
			builder.append("    ROUND(MFV.MARKET_PRICE,2) * MFV.TOTAL_BALANCE * ");
			builder.append("      CASE ");
			builder.append("        WHEN S.CURRENCY = 127 THEN 1 ");
			builder.append("        WHEN S.CURRENCY IN (1734,1304,430) THEN 1 * ");
			builder.append("          nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("            WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("              AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("        WHEN S.CURRENCY IN (1853) THEN 1 * ");
			builder.append("				  nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("            WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("              AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("        ELSE 1 ");
			builder.append("      END),0) TOTAL_PRECIO_MRKT_BOB, ");
			builder.append("  NVL(SUM( ");
			builder.append("    ROUND(MFV.MARKET_PRICE,2) * MFV.TOTAL_BALANCE * ");
			builder.append("      CASE ");
			builder.append("        WHEN S.CURRENCY = 127 THEN 1 / ");
			builder.append("            nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                  WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("                    AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("        WHEN S.CURRENCY IN (1734,1304) THEN 1 * ");
			builder.append("            nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                  WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("                    AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) / ");
			builder.append("            nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                  WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("                    AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("        WHEN S.CURRENCY IN (430,1853) THEN 1 ");
			builder.append("        ELSE 1 ");
			builder.append("      END),0) TOTAL_PRECIO_MRKT_USD ");
			builder.append("FROM MARKET_FACT_VIEW MFV ");
			builder.append("  INNER JOIN SECURITY S                       ON S.ID_SECURITY_CODE_PK = MFV.ID_SECURITY_CODE_PK ");
			builder.append("WHERE 1 = 1 ");
			//builder.append("  AND TRUNC(MFV.CUT_DATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') -1 ");
			builder.append("  AND (MFV.CUT_DATE >= TRUNC(TO_DATE('" + to.getBeforeDate() + "', 'DD/MM/YYYY'), 'DD') ");
			builder.append("  AND TRUNC(TO_DATE('" + to.getBeforeDate() + "', 'DD/MM/YYYY'), 'DD') + 1 > MFV.CUT_DATE  ");
			builder.append("  AND TRUNC(TO_DATE('" + to.getBeforeDate() + "', 'DD/MM/YYYY'), 'DD') = TO_DATE('"+ to.getBeforeDate() + "', 'DD/MM/YYYY')) ");
			
			/*Condiciones segun filtros*/
			if(to.getParticipant()!=null)
				builder.append(" AND MFV.ID_PARTICIPANT_PK = "+to.getParticipant());
			if(to.getHolder_account()!=null) 
				builder.append(" AND MFV.ID_HOLDER_ACCOUNT_PK = "+to.getHolder_account() );
			if (to.getIssuer() != null)
				builder.append(" AND S.ID_ISSUER_FK = '" + to.getIssuer()+"'");
			if (to.getSecurity_class() != null)
				builder.append(" AND S.SECURITY_CLASS = " + to.getSecurity_class());
			if(to.getSecurity_code()!=null)
				builder.append(" AND S.ID_SECURITY_CODE_PK = '"+to.getSecurity_code()+"'");
			if (to.getCurrency_code() != null)
				builder.append(" AND S.CURRENCY = " + to.getCurrency_code());
			
			builder.append("  AND ( ");
			builder.append("     (EXISTS ");
			builder.append("      ( SELECT had.ID_HOLDER_FK ");
			builder.append("        FROM HOLDER_ACCOUNT_DETAIL had ");
			builder.append("        WHERE 1 = 1");
			if(to.getCui_code()!=null)
				builder.append(" AND HAD.ID_HOLDER_FK = "+to.getCui_code());
			builder.append("       AND  had.ID_HOLDER_ACCOUNT_FK = MFV.ID_HOLDER_ACCOUNT_PK))) ");
			//INGRESO Y EGRESO
			builder.append("UNION ALL ");
			builder.append("SELECT ");
			builder.append("  1 FECHA_CORTE, ");
			builder.append("  '(INGRESO - EGRESO)' DESCRIPTION, ");
			builder.append("  TOTAL_INGRESO_SALIDA_SCB.TOTAL_INGRESO - TOTAL_INGRESO_SALIDA_SCB.TOTAL_SALIDA TOTAL_VALORES, ");
			builder.append("  TOTAL_INGRESO_SALIDA_SCB.VN_BOB_INGRESO - TOTAL_INGRESO_SALIDA_SCB.VN_BOB_SALIDA TOTAL_BOB, ");
			builder.append("  TOTAL_INGRESO_SALIDA_SCB.VN_USD_INGRESO - TOTAL_INGRESO_SALIDA_SCB.VN_USD_SALIDA TOTAL_USD, ");
			builder.append("  TOTAL_INGRESO_SALIDA_SCB.MP_BOB_INGRESO - TOTAL_INGRESO_SALIDA_SCB.MP_BOB_SALIDA TOTAL_PRECIO_MRKT_BOB, ");
			builder.append("  TOTAL_INGRESO_SALIDA_SCB.MP_USD_INGRESO - TOTAL_INGRESO_SALIDA_SCB.MP_USD_SALIDA TOTAL_PRECIO_MRKT_USD ");
			builder.append("FROM ");
			builder.append("(SELECT ");
			builder.append("  NVL(SUM(CASE WHEN MT.IND_MOVEMENT_CLASS = 1 THEN HMM.MOVEMENT_QUANTITY END),0) TOTAL_INGRESO, ");
			builder.append("  NVL(SUM(CASE WHEN MT.IND_MOVEMENT_CLASS = 2 THEN HMM.MOVEMENT_QUANTITY END),0) TOTAL_SALIDA, ");
			builder.append("  NVL(SUM( ");
			builder.append("      CASE ");
			builder.append("        WHEN MT.IND_MOVEMENT_CLASS = 1 THEN ");
			builder.append("          HMM.MOVEMENT_QUANTITY * HAM.NOMINAL_VALUE * ");
			builder.append("            CASE ");
			builder.append("              WHEN S.CURRENCY = 127 THEN 1 ");
			builder.append("              WHEN S.CURRENCY IN (1734,1304,430) THEN 1 * ");
			builder.append("                nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                  WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("                    AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("              WHEN S.CURRENCY IN (1853) THEN 1 * ");
			builder.append("                nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                  WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("                    AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("              ELSE 1 ");
			builder.append("            END ");
			builder.append("        ELSE 0 ");
			builder.append("      END ");
			builder.append("  ),0) VN_BOB_INGRESO, ");
			builder.append("  NVL(SUM( ");
			builder.append("    CASE ");
			builder.append("      WHEN MT.IND_MOVEMENT_CLASS = 1 THEN ");
			builder.append("        HMM.MOVEMENT_QUANTITY * HAM.NOMINAL_VALUE * ");
			builder.append("          CASE ");
			builder.append("            WHEN S.CURRENCY = 127 THEN 1 / ");
			builder.append("                nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                      WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("                        AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("            WHEN S.CURRENCY IN (1734,1304) THEN 1 * ");
			builder.append("                nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                      WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("                        AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) / ");
			builder.append("                nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D  ");
			builder.append("                      WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("                        AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("            WHEN S.CURRENCY IN (430,1853) THEN 1 ");
			builder.append("            ELSE 1 ");
			builder.append("          END ");
			builder.append("      ELSE 0 ");
			builder.append("    END ");
			builder.append("  ),0) VN_USD_INGRESO, ");
			builder.append("  NVL(SUM( ");
			builder.append("    CASE ");
			builder.append("      WHEN MT.IND_MOVEMENT_CLASS = 1 THEN ");
			builder.append("        HMM.MOVEMENT_QUANTITY * (ROUND(NVL(HMM.MARKET_PRICE,0),2)) * ");
			builder.append("          CASE ");
			builder.append("              WHEN S.CURRENCY = 127 THEN 1 ");
			builder.append("              WHEN S.CURRENCY IN (1734,1304,430) THEN 1 * ");
			builder.append("                nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                  WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("                    AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("              WHEN S.CURRENCY IN (1853) THEN 1 * ");
			builder.append("                nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                  WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("                    AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("              ELSE 1 ");
			builder.append("          END ");
			builder.append("      ELSE 0 ");
			builder.append("    END ");
			builder.append("  ),0) MP_BOB_INGRESO, ");
			builder.append("  NVL(SUM( ");
			builder.append("    CASE ");
			builder.append("      WHEN MT.IND_MOVEMENT_CLASS = 1 THEN ");
			builder.append("        HMM.MOVEMENT_QUANTITY * (ROUND(NVL(HMM.MARKET_PRICE,0),2)) * ");
			builder.append("          CASE ");
			builder.append("            WHEN S.CURRENCY = 127 THEN 1 / ");
			builder.append("                nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                      WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("                        AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("            WHEN S.CURRENCY IN (1734,1304) THEN 1 * ");
			builder.append("                nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                      WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("                        AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) / ");
			builder.append("                nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                      WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("                        AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("            WHEN S.CURRENCY IN (430,1853) THEN 1 ");
			builder.append("            ELSE 1 ");
			builder.append("          END ");
			builder.append("      ELSE 0 ");
			builder.append("    END ");
			builder.append("  ),0) MP_USD_INGRESO, ");
			builder.append("  NVL(SUM( ");
			builder.append("    CASE ");
			builder.append("      WHEN MT.IND_MOVEMENT_CLASS = 2 THEN ");
			builder.append("        HMM.MOVEMENT_QUANTITY * HAM.NOMINAL_VALUE * ");
			builder.append("          CASE ");
			builder.append("              WHEN S.CURRENCY = 127 THEN 1 ");
			builder.append("              WHEN S.CURRENCY IN (1734,1304,430) THEN 1 * ");
			builder.append("                nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                  WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("                    AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("              WHEN S.CURRENCY IN (1853) THEN 1 * ");
			builder.append("                nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                  WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("                    AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("              ELSE 1 ");
			builder.append("          END ");
			builder.append("      ELSE 0 ");
			builder.append("    END ");
			builder.append("  ),0) VN_BOB_SALIDA, ");
			builder.append("  NVL(SUM( ");
			builder.append("    CASE ");
			builder.append("      WHEN MT.IND_MOVEMENT_CLASS = 2 THEN ");
			builder.append("        HMM.MOVEMENT_QUANTITY * HAM.NOMINAL_VALUE * ");
			builder.append("          CASE ");
			builder.append("            WHEN S.CURRENCY = 127 THEN 1 / ");
			builder.append("                nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                      WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("                        AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("            WHEN S.CURRENCY IN (1734,1304) THEN 1 * ");
			builder.append("                nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                      WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("                        AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) / ");
			builder.append("                nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                      WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("                        AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("            WHEN S.CURRENCY IN (430,1853) THEN 1 ");
			builder.append("            ELSE 1 ");
			builder.append("          END ");
			builder.append("      ELSE 0 ");
			builder.append("    END ");
			builder.append("  ),0) VN_USD_SALIDA, ");
			builder.append("  NVL(SUM( ");
			builder.append("    CASE ");
			builder.append("      WHEN MT.IND_MOVEMENT_CLASS = 2 THEN ");
			builder.append("        HMM.MOVEMENT_QUANTITY * (ROUND(NVL(HMM.MARKET_PRICE,0),2)) * ");
			builder.append("          CASE ");
			builder.append("              WHEN S.CURRENCY = 127 THEN 1 ");
			builder.append("              WHEN S.CURRENCY IN (1734,1304,430) THEN 1 * ");
			builder.append("                nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                  WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("                    AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("              WHEN S.CURRENCY IN (1853) THEN 1 * ");
			builder.append("                nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                  WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("                    AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("              ELSE 1 ");
			builder.append("          END ");
			builder.append("      ELSE 0 ");
			builder.append("    END ");
			builder.append("  ),0) MP_BOB_SALIDA, ");
			builder.append("  NVL(SUM( ");
			builder.append("    CASE ");
			builder.append("      WHEN MT.IND_MOVEMENT_CLASS = 2 THEN ");
			builder.append("        HMM.MOVEMENT_QUANTITY * (ROUND(NVL(HMM.MARKET_PRICE,0),2)) * ");
			builder.append("          CASE ");
			builder.append("            WHEN S.CURRENCY = 127 THEN 1 / ");
			builder.append("                nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                      WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("                        AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("            WHEN S.CURRENCY IN (1734,1304) THEN 1 * ");
			builder.append("                nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                      WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("                        AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) / ");
			builder.append("                nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
			builder.append("                      WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
			builder.append("                        AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
			builder.append("            WHEN S.CURRENCY IN (430,1853) THEN 1 ");
			builder.append("            ELSE 1 ");
			builder.append("          END ");
			builder.append("      ELSE 0 ");
			builder.append("    END ");
			builder.append("  ),0) MP_USD_SALIDA ");
			builder.append(" FROM MOVEMENT_TYPE MT, HOLDER_ACCOUNT_MOVEMENT HAM, SECURITY S, HOLDER_MARKETFACT_MOVEMENT HMM ");
			builder.append("WHERE MT.ID_MOVEMENT_TYPE_PK = HAM.ID_MOVEMENT_TYPE_FK ");
			builder.append("  AND HAM.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK ");
			builder.append("  AND HAM.ID_HOLDER_ACCOUNT_MOVEMENT_PK = HMM.ID_HOLDER_ACCOUNT_MOVEMENT_FK ");
			builder.append("  AND MT.IND_MOVEMENT_CLASS IN (1,2) ");
			builder.append("  AND MT.ID_MOVEMENT_TYPE_PK NOT IN (300223,300224,300225,300226,300227,300228,300229,300632,300633) ");
			builder.append("  AND (HAM.MOVEMENT_DATE >= TRUNC(TO_DATE('" + to.getDate() + "', 'DD/MM/YYYY'), 'DD') ");
			builder.append("		 AND TRUNC(TO_DATE('" + to.getDate() + "', 'DD/MM/YYYY'), 'DD') + 1 > HAM.MOVEMENT_DATE  ");
			builder.append("		 AND TRUNC(TO_DATE('" + to.getDate() + "', 'DD/MM/YYYY'), 'DD') = TO_DATE('"+ to.getDate() + "', 'DD/MM/YYYY')) ");
			
			/*Condiciones segun filtros*/
			if(to.getParticipant()!=null)
				builder.append(" AND HMM.ID_PARTICIPANT_FK = "+to.getParticipant());
			if(to.getHolder_account()!=null) 
				builder.append(" AND HMM.ID_HOLDER_ACCOUNT_FK = "+to.getHolder_account() );
			if (to.getIssuer() != null)
				builder.append(" AND S.ID_ISSUER_FK = '" + to.getIssuer()+"'");
			if (to.getSecurity_class() != null)
				builder.append(" AND S.SECURITY_CLASS = " + to.getSecurity_class());
			if(to.getSecurity_code()!=null)
				builder.append(" AND S.ID_SECURITY_CODE_PK = '"+to.getSecurity_code()+"'");
			if (to.getCurrency_code() != null)
				builder.append(" AND S.CURRENCY = " + to.getCurrency_code());
			
			builder.append("  AND ( ");
			builder.append("    (EXISTS ");
			builder.append("      ( SELECT had.ID_HOLDER_FK ");
			builder.append("        FROM HOLDER_ACCOUNT_DETAIL had ");
			builder.append("        WHERE  1 = 1 ");
			if(to.getCui_code()!=null)
				builder.append(" AND HAD.ID_HOLDER_FK = "+to.getCui_code());
			builder.append("         AND  had.ID_HOLDER_ACCOUNT_FK = HAM.ID_HOLDER_ACCOUNT_FK))) ");
			builder.append(") TOTAL_INGRESO_SALIDA_SCB ");

			return builder.toString();
		}


	public String getQuerySqlReportConciliationPhysical(ConciliationDailyOperationsTO to) {
		StringBuilder builder = new StringBuilder();
		builder.append("SELECT ");
		builder.append("  SE.CURRENCY AS CURRENCY, ");
		builder.append("  (SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK=PCO.PHYSICAL_OPERATION_TYPE) AS MOVIMIENTO, ");
		builder.append("  DECODE(PCO.PHYSICAL_OPERATION_TYPE,673,PC.CERTIFICATE_QUANTITY,'') CANTIDAD_INGRESO, ");
		builder.append("  DECODE(PCO.PHYSICAL_OPERATION_TYPE,674,PC.CERTIFICATE_QUANTITY,'') CANTIDAD_EGRESO, ");
		builder.append("  DECODE(PCO.PHYSICAL_OPERATION_TYPE,673,SE.INITIAL_NOMINAL_VALUE,'') AS VALOR_NOMINAL_INGRESO, ");
		builder.append("  DECODE(PCO.PHYSICAL_OPERATION_TYPE,674,SE.INITIAL_NOMINAL_VALUE,'') AS VALOR_NOMINAL_EGRESO,  ");
		builder.append("  DECODE(PCO.PHYSICAL_OPERATION_TYPE,673,PCM.MARKET_PRICE,'') AS PRECIO_MERCADO_INGRESO, ");
		builder.append("  DECODE(PCO.PHYSICAL_OPERATION_TYPE,674,PCM.MARKET_PRICE,'') AS PRECIO_MERCADO_EGRESO, ");
		builder.append("  (  ");
		builder.append("      CASE ");
		builder.append("         WHEN ");
		builder.append("            SE.CURRENCY = 127 ");
		builder.append("         THEN ");
		builder.append("(1) / NVL(( ");
		builder.append("            SELECT ");
		builder.append("               D.BUY_PRICE ");
		builder.append("            FROM  ");
		builder.append("               DAILY_EXCHANGE_RATES D ");
		builder.append("            WHERE ");
		builder.append("               TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "', 'dd/mm/yyyy') ");
		builder.append("               AND D.ID_CURRENCY = 430 ");
		builder.append("               AND D.ID_SOURCE_INFORMATION = 212 ), 1) ");
		builder.append("            WHEN ");
		builder.append("               SE.CURRENCY IN ");
		builder.append("               ( ");
		builder.append("                  1734, ");
		builder.append("                  1304 ");
		builder.append("               ) ");
		builder.append("            THEN ");
		builder.append("(1) * nvl(( ");
		builder.append("               SELECT ");
		builder.append("                  D.BUY_PRICE ");
		builder.append("               FROM ");
		builder.append("                  DAILY_EXCHANGE_RATES D ");
		builder.append("               WHERE ");
		builder.append("                  TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "', 'dd/mm/yyyy') ");
		builder.append("                  AND D.ID_CURRENCY = SE.CURRENCY ");
		builder.append("                  AND D.ID_SOURCE_INFORMATION = 212 ), 1) / nvl(( ");
		builder.append("                  SELECT ");
		builder.append("                     D.BUY_PRICE ");
		builder.append("                  FROM ");
		builder.append("                     DAILY_EXCHANGE_RATES D ");
		builder.append("                  WHERE ");
		builder.append("                     TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "', 'dd/mm/yyyy') ");
		builder.append("                     AND D.ID_CURRENCY = 430 ");
		builder.append("                     AND D.ID_SOURCE_INFORMATION = 212 ), 1) ");
		builder.append("                  WHEN ");
		builder.append("                     SE.CURRENCY IN ");
		builder.append("                     ( ");
		builder.append("                        430, ");
		builder.append("                        1853 ");
		builder.append("                     ) ");
		builder.append("                  THEN ");
		builder.append("(1) ");
		builder.append("                  ELSE ");
		builder.append("(1) ");
		builder.append("      END ");
		builder.append("   ) ");
		builder.append("   CONVERTION_FACTOR ");
		builder.append("FROM PHYSICAL_OPERATION_DETAIL POD ");
		builder.append("  INNER JOIN PHYSICAL_CERTIFICATE PC ON PC.ID_PHYSICAL_CERTIFICATE_PK=POD.ID_PHYSICAL_CERTIFICATE_FK ");
		builder.append("  INNER JOIN PHYSICAL_CERTIFICATE_OPERATION PCO ON PCO.ID_PHYSICAL_OPERATION_PK = POD.ID_PHYSICAL_OPERATION_FK ");
		if(to.getParticipant()!=null||to.getHolder_account()!=null||to.getCui_code()!=null)
			builder.append("  INNER JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK = PCO.ID_HOLDER_ACCOUNT_FK ");
		if(to.getCui_code()!=null)
			builder.append("  INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ");
		builder.append("  INNER JOIN CUSTODY_OPERATION CO ON (PCO.ID_PHYSICAL_OPERATION_PK=CO.ID_CUSTODY_OPERATION_PK) ");
		builder.append("  INNER JOIN SECURITY SE ON (PC.ID_SECURITY_CODE_FK=SE.ID_SECURITY_CODE_PK) ");
		builder.append("  INNER JOIN PHYSICAL_CERT_MARKETFACT PCM ON (PCM.ID_PHYSICAL_CERTIFICATE_FK=PC.ID_PHYSICAL_CERTIFICATE_PK) AND PCM.IND_ACTIVE = 1 ");
		builder.append("  INNER JOIN PHYSICAL_BALANCE_DETAIL PBD ON (PC.ID_PHYSICAL_CERTIFICATE_PK=PBD.ID_PHYSICAL_CERTIFICATE_FK) ");
		builder.append(" ");
		builder.append("WHERE ");
		builder.append("POD.STATE = ");
		builder.append("  CASE PCO.PHYSICAL_OPERATION_TYPE ");
		builder.append("    WHEN 673 THEN 601 ");
		builder.append("    WHEN 674 THEN 602 ");
		builder.append("  END ");
		//Ingreso y salida
		builder.append(" AND CO.OPERATION_STATE IN (601,602) "); // confirmada Y AUTORIZADA PARA RETIRO

		if (to.getParticipant() != null)
			builder.append("  AND ha.id_participant_fk = " + to.getParticipant());
		if(to.getCui_code()!=null)
			builder.append(" AND HAD.ID_HOLDER_FK =  " + to.getCui_code());
		if(to.getHolder_account()!=null)
			builder.append(" AND HAD.ID_HOLDER__ACCOUNT_FK =  " + to.getHolder_account());
		if(to.getIssuer()!=null)
			builder.append(" AND SE.ID_ISSUER_PK =  '" + to.getHolder_account()+"'");
		if (to.getSecurity_class() != null)
			builder.append("  AND SE.SECURITY_CLASS = " + to.getSecurity_class());
		if(to.getSecurity_code()!=null)
			builder.append(" AND SE.ID_SECURITY_CODE_PK = "+to.getSecurity_code());
		if (to.getCurrency_code() != null)
			builder.append("  AND SE.CURRENCY = " + to.getCurrency_code());

		builder.append("    AND CO.CONFIRM_DATE >= TRUNC(TO_DATE('" + to.getDate() + "', 'DD/MM/YYYY'), 'DD') ");
		builder.append("    AND CO.CONFIRM_DATE < TRUNC(TO_DATE('" + to.getDate() + "', 'DD/MM/YYYY'), 'DD') + 1  ");
		builder.append("    AND TRUNC(TO_DATE('" + to.getDate() + "', 'DD/MM/YYYY'), 'DD') = TO_DATE('" + to.getDate()+ "', 'DD/MM/YYYY') ");
		
		return builder.toString();
	}
	
	/**
	 * Resumen de fisicos
	 * 
	 */
	public String getQuerySumBalancePhysical(ConciliationDailyOperationsTO to) {
	   StringBuilder builder = new StringBuilder();
	   builder.append(" SELECT   SUM(PSV.TOTAL_BALANCE) TOTAL_BALANCE, ");
	   builder.append(" SUM (PSV.AVAILABLE_BALANCE) AVAILABLE_BALANCE, ");
	   builder.append(" 0 PAWN_BALANCE, ");
	   builder.append(" 0 BAN_BALANCE, ");
	   builder.append(" 0 OTHER_BLOCK_BALANCE, ");
	   builder.append(" 0 ACCREDITATION_BALANCE, ");
	   builder.append(" 0 MARGIN_BALANCE, ");
	   builder.append(" 0 REPORTING_BALANCE, ");
	   builder.append(" 0 REPORTED_BALANCE, ");
	   builder.append(" 0 PURCHASE_BALANCE, ");
	   builder.append(" 0 SALE_BALANCE, ");
	   builder.append(" 0 TRANSIT_BALANCE, ");
	   builder.append(" SUM(PSV.TOTAL_BALANCE) CONTABLE, SUM(PSV.TOTAL_BALANCE) SALDO,");
	   builder.append(" 0 AS REPORTADO, 0 AS REPORTADOR ");
	   builder.append(" FROM BALANCE_PSY_VIEW PSV ");
	   builder.append(" INNER JOIN PHYSICAL_CERT_MARKETFACT HMB ON HMB.ID_PHYSICAL_CERTIFICATE_FK = PSV.ID_PHYSICAL_CERTIFICATE_PK ");
	   if(to.getHolder_account()!=null||to.getCui_code()!=null)
	   	   builder.append(" INNER JOIN HOLDER_ACCOUNT HA ON PSV.ID_HOLDER_ACCOUNT_PK=HA.ID_HOLDER_ACCOUNT_PK ");
	   if (to.getCui_code() != null)
		   builder.append(" INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ");	   
	   //if (to.getParticipant() != null)
       //builder.append(" INNER JOIN PARTICIPANT P ON PSV.ID_PARTICIPANT_PK=P.ID_PARTICIPANT_PK ");
	   builder.append(" INNER JOIN SECURITY S ON PSV.ID_SECURITY_CODE_PK = S.ID_SECURITY_CODE_PK ");
	   //builder.append(" INNER JOIN ISSUER ISS ON SECU.ID_ISSUER_FK=ISS.ID_ISSUER_PK ");
	   builder.append(" LEFT JOIN NOMINAL_VALUE_HISTORY_VIEW NVH ON NVH.CUT_DATE = TO_DATE('"+to.getDate()+"', 'DD/MM/YYYY') ");
	   builder.append(" AND s.ID_SECURITY_CODE_PK = NVH.ID_SECURITY_CODE_FK ");
	   builder.append(" WHERE 1 = 1 ");
	   builder.append(" AND HMB.IND_ACTIVE = 1 ");
	   if (to.getParticipant() != null)
			builder.append("    AND PSV.ID_PARTICIPANT_pK = " + to.getParticipant());
	   
	   if(to.getCui_code()!=null)
		   builder.append(" AND HAD.ID_HOLDER_FK = "+to.getCui_code());
	   
	    if(to.getHolder_account()!=null)	
			builder.append(" AND PSV.ID_HOLDER_ACCOUNT_PK = " + to.getHolder_account());
		
		if (to.getIssuer() != null)
			builder.append(" AND S.ID_ISSUER_FK = '" + to.getIssuer()+"'");

		if (to.getSecurity_class() != null)
			builder.append(" AND S.SECURITY_CLASS = " + to.getSecurity_class());
		
		if (to.getSecurity_code() != null)
			builder.append(" AND S._ID_SECURITY_CODE_PK = '" + to.getSecurity_code()+"'");

		if (to.getCurrency_code() != null)
			builder.append(" AND S.CURRENCY = " + to.getCurrency_code());	
		
	   builder.append(" AND PSV.CUT_DATE >= TRUNC(TO_DATE('"+to.getDate()+"', 'DD/MM/YYYY'), 'DD') ");
	   builder.append(" AND PSV.CUT_DATE < TRUNC(TO_DATE('"+to.getDate()+"', 'DD/MM/YYYY'), 'DD') + 1 ");
	   builder.append(" AND TRUNC(TO_DATE('"+to.getDate()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE('"+to.getDate()+"', 'DD/MM/YYYY') ");
	   builder.append(" AND PSV.TOTAL_BALANCE > 0 ");
       return builder.toString();
	}
	

	/**
	 * 
	 * */
	public String getQueryCutDate(ConciliationDailyOperationsTO to) {
		StringBuilder builder = new StringBuilder();
		builder.append(" SELECT ");
		builder.append("   TOTAL_CURRENCY.CURRENCY, ");
		builder.append("   TOTAL_CURRENCY.TOTAL_VALORES, ");
		builder.append("   TOTAL_CURRENCY.TOTAL_BOB, ");
		builder.append("   TOTAL_CURRENCY.TOTAL_USD, ");
		builder.append("   TOTAL_CURRENCY.TOTAL_BOB_RENTA_FIJA, ");
		builder.append("   TOTAL_CURRENCY.TOTAL_BOB_RENTA_VAR, ");
		builder.append("   TOTAL_CURRENCY.TOTAL_PRECIO_MRKT_BOB, ");
		builder.append("   TOTAL_CURRENCY.TOTAL_PREC_MRKT_BOB_RENTA_FIJA, ");
		builder.append("   TOTAL_CURRENCY.TOTAL_PREC_MRKT_BOB_RENTA_VAR ");
		builder.append(" FROM ");
		builder.append(" (SELECT ");
		builder.append("   (SELECT PARAMETER_NAME FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = S.CURRENCY) CURRENCY, ");
		builder.append("   NVL(SUM(MFV.TOTAL_BALANCE),0) TOTAL_VALORES, ");
		builder.append("   NVL(SUM( ");
		builder.append("     S.INITIAL_NOMINAL_VALUE * MFV.TOTAL_BALANCE * ");
		builder.append("       CASE ");
		builder.append("         WHEN S.CURRENCY = 127 THEN 1 ");
		builder.append("         WHEN S.CURRENCY IN (430,1304,1734) THEN 1 * ");
		builder.append("           nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
		builder.append("             WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
		builder.append("               AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) ");
		builder.append("         WHEN S.CURRENCY IN (1853) THEN 1 * ");
		builder.append(" 				  nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
		builder.append("             WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
		builder.append("               AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
		builder.append("         ELSE 1 ");
		builder.append("       END),0) TOTAL_BOB, ");
		builder.append("    NVL(SUM( ");
		builder.append("     S.INITIAL_NOMINAL_VALUE * MFV.TOTAL_BALANCE * ");
		builder.append("       CASE ");
		builder.append("         WHEN S.CURRENCY = 127 THEN 1 / ");
		builder.append("             nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
		builder.append("                   WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
		builder.append("                     AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
		builder.append("         WHEN S.CURRENCY IN (1734,1304) THEN 1 * ");
		builder.append("             nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
		builder.append("                   WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
		builder.append("                     AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) / ");
		builder.append("             nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
		builder.append("                   WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
		builder.append("                     AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
		builder.append("         WHEN S.CURRENCY IN (430,1853) THEN 1 ");
		builder.append("         ELSE 1 ");
		builder.append("   END),0) TOTAL_USD, ");
		builder.append("   NVL(SUM( ");
		builder.append("     S.INITIAL_NOMINAL_VALUE * MFV.TOTAL_BALANCE * ");
		builder.append("     CASE ");
		builder.append("       WHEN S.INSTRUMENT_TYPE = 124 THEN ");
		builder.append("         CASE ");
		builder.append("           WHEN S.CURRENCY = 127 THEN 1 ");
		builder.append("           WHEN S.CURRENCY IN (1734,1304,430) THEN 1 * ");
		builder.append("             nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
		builder.append("               WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
		builder.append("                 AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) ");
		builder.append("           WHEN S.CURRENCY IN (1853) THEN 1 * ");
		builder.append("             nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
		builder.append("               WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
		builder.append("                 AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
		builder.append("           ELSE 1 ");
		builder.append("         END ");
		builder.append("       ELSE 0 ");
		builder.append("     END ");
		builder.append("   ),0) TOTAL_BOB_RENTA_FIJA, ");
		builder.append("   NVL(SUM( ");
		builder.append("     S.INITIAL_NOMINAL_VALUE * MFV.TOTAL_BALANCE * ");
		builder.append("       CASE ");
		builder.append("         WHEN S.INSTRUMENT_TYPE = 399 THEN ");
		builder.append("           CASE ");
		builder.append("             WHEN S.CURRENCY = 127 THEN 1 ");
		builder.append("             WHEN S.CURRENCY IN (1734,1304,430) THEN 1 * ");
		builder.append("               nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
		builder.append("                 WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
		builder.append("                   AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) ");
		builder.append("             WHEN S.CURRENCY IN (1853) THEN 1 * ");
		builder.append("               nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
		builder.append("                 WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
		builder.append("                   AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
		builder.append("             ELSE 1 ");
		builder.append("           END ");
		builder.append("         ELSE 0 ");
		builder.append("       END ");
		builder.append("   ),0) TOTAL_BOB_RENTA_VAR, ");
		builder.append("   NVL(SUM( ");
		builder.append("     ROUND(MFV.MARKET_PRICE,2) * MFV.TOTAL_BALANCE * ");
		builder.append("       CASE ");
		builder.append("         WHEN S.CURRENCY = 127 THEN 1 ");
		builder.append("         WHEN S.CURRENCY IN (1734,1304,430) THEN 1 * ");
		builder.append("           nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
		builder.append("             WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
		builder.append("               AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) ");
		builder.append("         WHEN S.CURRENCY IN (1853) THEN 1 * ");
		builder.append("           nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
		builder.append("             WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
		builder.append("               AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
		builder.append("         ELSE 1 ");
		builder.append("       END),0) TOTAL_PRECIO_MRKT_BOB, ");
		builder.append("   NVL(SUM( ");
		builder.append("     ROUND(MFV.MARKET_PRICE,2) * MFV.TOTAL_BALANCE * ");
		builder.append("       CASE ");
		builder.append("         WHEN S.INSTRUMENT_TYPE = 124 THEN ");
		builder.append("           CASE ");
		builder.append("             WHEN S.CURRENCY = 127 THEN 1 ");
		builder.append("             WHEN S.CURRENCY IN (1734,1304,430) THEN 1 * ");
		builder.append("               nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
		builder.append("                 WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
		builder.append("                   AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) ");
		builder.append("             WHEN S.CURRENCY IN (1853) THEN 1 * ");
		builder.append("               nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
		builder.append("                 WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
		builder.append("                   AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
		builder.append("             ELSE 1 ");
		builder.append("           END ");
		builder.append("         ELSE 0 ");
		builder.append("       END ");
		builder.append("   ),0) TOTAL_PREC_MRKT_BOB_RENTA_FIJA, ");
		builder.append("   NVL(SUM( ");
		builder.append("     ROUND(MFV.MARKET_PRICE,2) * MFV.TOTAL_BALANCE * ");
		builder.append("       CASE ");
		builder.append("         WHEN S.INSTRUMENT_TYPE = 399 THEN ");
		builder.append("           CASE ");
		builder.append("             WHEN S.CURRENCY = 127 THEN 1 ");
		builder.append("             WHEN S.CURRENCY IN (1734,1304,430) THEN 1 * ");
		builder.append("               nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
		builder.append("                 WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
		builder.append("                   AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) ");
		builder.append("             WHEN S.CURRENCY IN (1853) THEN 1 * ");
		builder.append("               nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D ");
		builder.append("                 WHERE TRUNC(D.DATE_RATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
		builder.append("                   AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1) ");
		builder.append("             ELSE 1 ");
		builder.append("           END ");
		builder.append("         ELSE 0 ");
		builder.append("       END ");
		builder.append("   ),0) TOTAL_PREC_MRKT_BOB_RENTA_VAR ");
		builder.append(" FROM MARKET_FACT_VIEW MFV ");
		builder.append(" INNER JOIN SECURITY S ON S.ID_SECURITY_CODE_PK = MFV.ID_SECURITY_CODE_PK ");
		builder.append(" WHERE 1 = 1 ");
		builder.append(" AND MFV.ID_SECURITY_CODE_PK = S.ID_SECURITY_CODE_PK ");

		/*Filtros*/
		if (to.getParticipant() != null)
			builder.append(" AND MFV.ID_PARTICIPANT_PK = " + to.getParticipant());
		if(to.getHolder_account()!=null) 
			builder.append("  AND MFV.ID_HOLDER_ACCOUNT_PK = "+to.getHolder_account() );
		if(to.getIssuer()!=null)
			builder.append(" AND S.ID_ISSUER_FK = '"+to.getIssuer()+"'");
		if (to.getSecurity_class() != null)
			builder.append(" AND S.SECURITY_CLASS = " + to.getSecurity_class());
		if(to.getSecurity_code()!=null)
			builder.append(" AND S.ID_SECURITY_CODE_PK = '"+to.getSecurity_code()+"'");
		if (to.getCurrency_code() != null)
			builder.append(" AND S.CURRENCY = " + to.getCurrency_code());

		builder.append("   AND ( ");
		builder.append("   (EXISTS ");
		builder.append("       ( SELECT had.ID_HOLDER_FK ");
		builder.append("         FROM HOLDER_ACCOUNT_DETAIL had ");
		builder.append("         WHERE 1 = 1 ");
		if(to.getCui_code()!=null)
			builder.append(" AND HAD.ID_HOLDER_FK = "+to.getCui_code());
		builder.append("          AND had.ID_HOLDER_ACCOUNT_FK = MFV.ID_HOLDER_ACCOUNT_PK))) ");
		builder.append("   AND s.security_type in (1762,1763) ");
		//builder.append("   AND TRUNC(MFV.CUT_DATE) = TO_DATE('" + to.getDate() + "','DD/MM/YYYY') ");
		builder.append("    AND MFV.CUT_DATE >= TRUNC(TO_DATE('" + to.getDate() + "', 'DD/MM/YYYY'), 'DD') ");
		builder.append("    AND MFV.CUT_DATE < TRUNC(TO_DATE('" + to.getDate() + "', 'DD/MM/YYYY'), 'DD') + 1  ");
		builder.append("    AND TRUNC(TO_DATE('" + to.getDate() + "', 'DD/MM/YYYY'), 'DD') = TO_DATE('" + to.getDate()+ "', 'DD/MM/YYYY') ");
		builder.append(" GROUP BY ");
		builder.append("   S.CURRENCY ");
		builder.append(" ORDER BY ");
		builder.append("   S.CURRENCY ");
		builder.append(" ) TOTAL_CURRENCY ");

		return builder.toString();
	}

	

	private String getQuerySubTotalPhysical(ConciliationDailyOperationsTO to) {
		StringBuilder query = new StringBuilder();
		query.append(" SELECT 0 FECHA_CORTE,    ");
		query.append("       'CARTERA DEL DIA ANTERIOR' DESCRIPTION,                                                        ");
		query.append("                                  NVL(SUM(PSV.TOTAL_BALANCE), 0) TOTAL_VALORES,                        ");
		query.append("                                  NVL(SUM(S.INITIAL_NOMINAL_VALUE * PSV.TOTAL_BALANCE * CASE           ");
		query.append("              WHEN S.CURRENCY = 127 THEN 1                             								 ");
		query.append("              WHEN S.CURRENCY IN (1734, 1304, 430) THEN 1 * nvl(       								 ");
		query.append("          (SELECT D.BUY_PRICE                                          								 ");
		query.append("           FROM DAILY_EXCHANGE_RATES D                             								     ");
		query.append("           WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY') -1                   ");
		query.append("             AND D.ID_CURRENCY = S.CURRENCY                          									 ");
		query.append("             AND D.ID_SOURCE_INFORMATION = 212),1)                								     ");
		query.append("              WHEN S.CURRENCY IN (1853) THEN 1 * nvl(                  								 ");
		query.append("        (SELECT D.BUY_PRICE                                                                            ");
		query.append(" FROM DAILY_EXCHANGE_RATES D                                                                           ");
		query.append(" WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY') -1                             ");
		query.append("  AND D.ID_CURRENCY = 430                                                                              ");
		query.append("  AND D.ID_SOURCE_INFORMATION = 212),1)                                                                ");
		query.append("              ELSE 1                                                                                   ");
		query.append("          END), 0) TOTAL_BOB,                                                                          ");
		query.append("                                  NVL(SUM(S.INITIAL_NOMINAL_VALUE * PSV.TOTAL_BALANCE * CASE           ");
		query.append("              WHEN S.CURRENCY = 127 THEN 1 / nvl(                       ");
		query.append("    (SELECT D.BUY_PRICE  ");
		query.append("     FROM DAILY_EXCHANGE_RATES D  ");
		query.append("     WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY') -1           ");
		query.append("       AND D.ID_CURRENCY = 430    ");
		query.append("       AND D.ID_SOURCE_INFORMATION = 212),1)                                     ");
		query.append("              WHEN S.CURRENCY IN (1734, 1304) THEN 1 * nvl(             ");
		query.append("     (SELECT D.BUY_PRICE ");
		query.append("      FROM DAILY_EXCHANGE_RATES D                                       ");
		query.append("      WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY') -1 ");
		query.append("        AND D.ID_CURRENCY = S.CURRENCY                                  ");
		query.append("        AND D.ID_SOURCE_INFORMATION = 212),1) / nvl(                    ");
		query.append("       (SELECT D.BUY_PRICE                              ");
		query.append("        FROM DAILY_EXCHANGE_RATES D                     ");
		query.append("        WHERE TRUNC(D.DATE_RATE) = TO_DATE('"	+ to.getDate() + "', 'DD/MM/YYYY') -1                              ");
		query.append("          AND D.ID_CURRENCY = 430                       ");
		query.append("          AND D.ID_SOURCE_INFORMATION = 212),1)         ");
		query.append("              WHEN S.CURRENCY IN (430, 1853) THEN 1                     ");
		query.append("              ELSE 1     ");
		query.append("          END), 0) TOTAL_USD,                                           ");
		query.append("                                  NVL(SUM(ROUND(PCM.MARKET_PRICE, 2) * PSV.TOTAL_BALANCE * CASE        ");
		query.append("                 WHEN S.CURRENCY = 127 THEN 1                           ");
		query.append("                 WHEN S.CURRENCY IN (1734, 1304, 430) THEN 1 * nvl(     ");
		query.append("             (SELECT D.BUY_PRICE                                        ");
		query.append("              FROM DAILY_EXCHANGE_RATES D                               ");
		query.append("              WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY') -1                        ");
		query.append("                AND D.ID_CURRENCY = S.CURRENCY                          ");
		query.append("                AND D.ID_SOURCE_INFORMATION = 212),1)                   ");
		query.append("                 WHEN S.CURRENCY IN (1853) THEN 1 * nvl(                ");
		query.append("  (SELECT D.BUY_PRICE    ");
		query.append("   FROM DAILY_EXCHANGE_RATES D                                          ");
		query.append("   WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY') -1    ");
		query.append("     AND D.ID_CURRENCY = 430                                            ");
		query.append("     AND D.ID_SOURCE_INFORMATION = 212),1)                              ");
		query.append("                 ELSE 1  ");
		query.append("             END), 0) TOTAL_PRECIO_MRKT_BOB,                            ");
		query.append("                                  NVL(SUM(ROUND(PCM.MARKET_PRICE, 2) * PSV.TOTAL_BALANCE * CASE        ");
		query.append("                 WHEN S.CURRENCY = 127 THEN 1 / nvl(                    ");
		query.append("       (SELECT D.BUY_PRICE        ");
		query.append("        FROM DAILY_EXCHANGE_RATES D                                              ");
		query.append("        WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY') -1        ");
		query.append(" AND D.ID_CURRENCY = 430 ");
		query.append(" AND D.ID_SOURCE_INFORMATION = 212),1)                                  ");
		query.append("                 WHEN S.CURRENCY IN (1734, 1304) THEN 1 * nvl(          ");
		query.append("        (SELECT D.BUY_PRICE                                             ");
		query.append("         FROM DAILY_EXCHANGE_RATES D                                    ");
		query.append("         WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY') -1                             ");
		query.append("           AND D.ID_CURRENCY = S.CURRENCY                               ");
		query.append("           AND D.ID_SOURCE_INFORMATION = 212),1) / nvl(                 ");
		query.append("          (SELECT D.BUY_PRICE                           ");
		query.append("           FROM DAILY_EXCHANGE_RATES D                  ");
		query.append("           WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate() + "', 'DD/MM/YYYY') -1                           ");
		query.append("             AND D.ID_CURRENCY = 430                    ");
		query.append("             AND D.ID_SOURCE_INFORMATION = 212),1)      ");
		query.append("                 WHEN S.CURRENCY IN (430, 1853) THEN 1                  ");
		query.append("                 ELSE 1  ");
		query.append("             END), 0) TOTAL_PRECIO_MRKT_USD                             ");
		query.append(" FROM BALANCE_PSY_VIEW PSV");
		query.append(" INNER JOIN PHYSICAL_CERT_MARKETFACT PCM ON PCM.ID_PHYSICAL_CERTIFICATE_FK = PSV.ID_PHYSICAL_CERTIFICATE_PK                                            ");
		query.append(" INNER JOIN SECURITY S ON S.ID_SECURITY_CODE_PK = PSV.ID_SECURITY_CODE_PK");
		
		//Filtros de busqueda
		query.append(" WHERE 1 = 1   ");
		
		if (to.getParticipant() != null)
			 query.append(" AND PSV.ID_PARTICIPANT_pK = " + to.getParticipant());
		   
	    if(to.getHolder_account()!=null)	
	    	query.append(" AND PSV.ID_HOLDER_ACCOUNT_PK = " + to.getHolder_account());
		
		if (to.getIssuer() != null)
			query.append(" AND S.ID_ISSUER_FK = '" + to.getIssuer()+"'");

		if (to.getSecurity_class() != null)
			query.append(" AND S.SECURITY_CLASS = " + to.getSecurity_class());
		
		if (to.getSecurity_code() != null)
			query.append(" AND S._ID_SECURITY_CODE_PK = '" + to.getSecurity_code()+"'");

		if (to.getCurrency_code() != null)
			query.append(" AND S.CURRENCY = " + to.getCurrency_code());	
		
		//Fecha optimizada
		//query.append("  AND TRUNC(PSV.CUT_DATE) = TO_DATE('" + to.getDate()	+ "', 'DD/MM/YYYY') -1                          ");
		/*Continuar aqui*/
		query.append("     AND (PSV.CUT_DATE >= TRUNC(TO_DATE('" + to.getBeforeDate()+ "', 'DD/MM/YYYY'), 'DD')                        ");
		query.append("          AND TRUNC(TO_DATE('" + to.getBeforeDate()+ "', 'DD/MM/YYYY'), 'DD') + 1 > PSV.CUT_DATE           ");
		query.append("          AND TRUNC(TO_DATE('" + to.getBeforeDate() + "', 'DD/MM/YYYY'), 'DD') = TO_DATE('"+ to.getBeforeDate()+ "', 'DD/MM/YYYY'))  ");
		
		query.append("  AND ((EXISTS       ");
		query.append("          (SELECT had.ID_HOLDER_FK                                       ");
		query.append("           FROM HOLDER_ACCOUNT_DETAIL had                                ");
		query.append("           WHERE 1 = 1 ");
		
		if(to.getCui_code()!=null)
			   query.append(" AND HAD.ID_HOLDER_FK = "+to.getCui_code());
		   
		query.append(" AND had.ID_HOLDER_ACCOUNT_FK = PSV.ID_HOLDER_ACCOUNT_PK))) ");
		//Union
		query.append(" UNION ALL       ");
		query.append("SELECT 1 FECHA_CORTE,    ");
		query.append("       'CARTERA AL INICIO DEL DIA' DESCRIPTION,                         ");
		query.append("                                   NVL(SUM(PSV.TOTAL_BALANCE), 0) TOTAL_VALORES,                       ");
		query.append("                                   NVL(SUM(S.INITIAL_NOMINAL_VALUE * PSV.TOTAL_BALANCE * CASE          ");
		query.append("               WHEN S.CURRENCY = 127 THEN 1                             ");
		query.append("               WHEN S.CURRENCY IN (1734, 1304, 430) THEN 1 * nvl(       ");
		query.append("           (SELECT D.BUY_PRICE                                          ");
		query.append("            FROM DAILY_EXCHANGE_RATES D                                 ");
		query.append("            WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY') ");
		query.append("              AND D.ID_CURRENCY = S.CURRENCY                            ");
		query.append("              AND D.ID_SOURCE_INFORMATION = 212),1)                     ");
		query.append("               WHEN S.CURRENCY IN (1853) THEN 1 * nvl(                  ");
		query.append(" (SELECT D.BUY_PRICE      ");
		query.append(" FROM DAILY_EXCHANGE_RATES D                                            ");
		query.append(" WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY') ");
		query.append("   AND D.ID_CURRENCY = 430                                              ");
		query.append("   AND D.ID_SOURCE_INFORMATION = 212),1)                                ");
		query.append("               ELSE 1                                                   ");
		query.append("           END), 0) TOTAL_BOB,NVL(SUM(S.INITIAL_NOMINAL_VALUE * PSV.TOTAL_BALANCE * CASE          ");
		query.append("               WHEN S.CURRENCY = 127 THEN 1 / nvl(                      ");
		query.append("     (SELECT D.BUY_PRICE ");
		query.append("      FROM DAILY_EXCHANGE_RATES D ");
		query.append("      WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY') ");
		query.append("        AND D.ID_CURRENCY = 430   ");
		query.append("        AND D.ID_SOURCE_INFORMATION = 212),1)                                    ");
		query.append("               WHEN S.CURRENCY IN (1734, 1304) THEN 1 * nvl(            ");
		query.append("      (SELECT D.BUY_PRICE");
		query.append("       FROM DAILY_EXCHANGE_RATES D                                      ");
		query.append("       WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY')   ");
		query.append("         AND D.ID_CURRENCY = S.CURRENCY                                 ");
		query.append("         AND D.ID_SOURCE_INFORMATION = 212),1) / nvl(                   ");
		query.append("        (SELECT D.BUY_PRICE                             ");
		query.append("         FROM DAILY_EXCHANGE_RATES D                    ");
		query.append("         WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate() + "', 'DD/MM/YYYY')       ");
		query.append("           AND D.ID_CURRENCY = 430                      ");
		query.append("           AND D.ID_SOURCE_INFORMATION = 212),1)        ");
		query.append("               WHEN S.CURRENCY IN (430, 1853) THEN 1                    ");
		query.append("               ELSE 1    ");
		query.append("           END), 0) TOTAL_USD,                                          ");
		query.append("                                   NVL(SUM(ROUND(PCM.MARKET_PRICE, 2) * PSV.TOTAL_BALANCE * CASE       ");
		query.append("                  WHEN S.CURRENCY = 127 THEN 1                          ");
		query.append("                  WHEN S.CURRENCY IN (1734, 1304, 430) THEN 1 * nvl(    ");
		query.append("              (SELECT D.BUY_PRICE                                       ");
		query.append("               FROM DAILY_EXCHANGE_RATES D                              ");
		query.append("               WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY')                          ");
		query.append("                 AND D.ID_CURRENCY = S.CURRENCY                         ");
		query.append("                 AND D.ID_SOURCE_INFORMATION = 212),1)                  ");
		query.append("                  WHEN S.CURRENCY IN (1853) THEN 1 * nvl(               ");
		query.append("   (SELECT D.BUY_PRICE   ");
		query.append("    FROM DAILY_EXCHANGE_RATES D                                         ");
		query.append("    WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY')      ");
		query.append("      AND D.ID_CURRENCY = 430                                           ");
		query.append("      AND D.ID_SOURCE_INFORMATION = 212),1)                             ");
		query.append("                  ELSE 1 ");
		query.append("              END), 0) TOTAL_PRECIO_MRKT_BOB,                           ");
		query.append("                                   NVL(SUM(ROUND(PCM.MARKET_PRICE, 2) * PSV.TOTAL_BALANCE * CASE       ");
		query.append("                  WHEN S.CURRENCY = 127 THEN 1 / nvl(                   ");
		query.append("        (SELECT D.BUY_PRICE       ");
		query.append(" FROM DAILY_EXCHANGE_RATES D                                             ");
		query.append(" WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY')          ");
		query.append("  AND D.ID_CURRENCY = 430 ");
		query.append("  AND D.ID_SOURCE_INFORMATION = 212),1)                                 ");
		query.append("                  WHEN S.CURRENCY IN (1734, 1304) THEN 1 * nvl(         ");
		query.append("         (SELECT D.BUY_PRICE                                            ");
		query.append("          FROM DAILY_EXCHANGE_RATES D                                   ");
		query.append("          WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()	+ "', 'DD/MM/YYYY')");
		query.append("            AND D.ID_CURRENCY = S.CURRENCY                              ");
		query.append("            AND D.ID_SOURCE_INFORMATION = 212),1) / nvl(                ");
		query.append("           (SELECT D.BUY_PRICE                          ");
		query.append("            FROM DAILY_EXCHANGE_RATES D                 ");
		query.append("            WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate() + "', 'DD/MM/YYYY')                             ");
		query.append("              AND D.ID_CURRENCY = 430                   ");
		query.append("              AND D.ID_SOURCE_INFORMATION = 212),1)     ");
		query.append("                  WHEN S.CURRENCY IN (430, 1853) THEN 1                 ");
		query.append("                  ELSE 1 ");
		query.append("              END), 0) TOTAL_PRECIO_MRKT_USD                            ");
		query.append(" FROM BALANCE_PSY_VIEW PSV");
		query.append(" INNER JOIN PHYSICAL_CERT_MARKETFACT PCM ON PCM.ID_PHYSICAL_CERTIFICATE_FK = PSV.ID_PHYSICAL_CERTIFICATE_PK                                            ");
		query.append(" INNER JOIN SECURITY S ON S.ID_SECURITY_CODE_PK = PSV.ID_SECURITY_CODE_PK");
		query.append(" WHERE 1 = 1     ");
		//Filtros de busqueda
		 if (to.getParticipant() != null)
			 query.append("    AND PSV.ID_PARTICIPANT_pK = " + to.getParticipant());
		   
	    if(to.getHolder_account()!=null)	
	    	query.append(" AND PSV.ID_HOLDER_ACCOUNT_PK = " + to.getHolder_account());
		
		if (to.getIssuer() != null)
			query.append(" AND S.ID_ISSUER_FK = '" + to.getIssuer()+"'");

		if (to.getSecurity_class() != null)
			query.append(" AND S.SECURITY_CLASS = " + to.getSecurity_class());
		
		if (to.getSecurity_code() != null)
			query.append(" AND S._ID_SECURITY_CODE_PK = '" + to.getSecurity_code()+"'");

		if (to.getCurrency_code() != null)
			query.append(" AND S.CURRENCY = " + to.getCurrency_code());	
		
		query.append("     AND (PSV.CUT_DATE >= TRUNC(TO_DATE('" + to.getDate()+ "', 'DD/MM/YYYY'), 'DD')                        ");
		query.append("          AND TRUNC(TO_DATE('" + to.getDate()+ "', 'DD/MM/YYYY'), 'DD') + 1 > PSV.CUT_DATE           ");
		query.append("          AND TRUNC(TO_DATE('" + to.getDate() + "', 'DD/MM/YYYY'), 'DD') = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY'))  ");
		query.append("  AND ((EXISTS  ");
		query.append("          (SELECT had.ID_HOLDER_FK                                      ");
		query.append("           FROM HOLDER_ACCOUNT_DETAIL had                               ");
		query.append("           WHERE 1 = 1 ");
		if(to.getCui_code()!=null)
			query.append(" AND HAD.ID_HOLDER_FK = "+to.getCui_code());
		query.append(" AND had.ID_HOLDER_ACCOUNT_FK = PSV.ID_HOLDER_ACCOUNT_PK)))   ");
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		query.append(" UNION ALL ");
		query.append(" SELECT 1 FECHA_CORTE, ");
		query.append("        '(INGRESO - EGRESO)' DESCRIPTION,  ");
		query.append("                             TOTAL_INGRESO_SALIDA_SCB.TOTAL_INGRESO - TOTAL_INGRESO_SALIDA_SCB.TOTAL_SALIDA TOTAL_VALORES, ");
		query.append("                             TOTAL_INGRESO_SALIDA_SCB.VN_BOB_INGRESO - TOTAL_INGRESO_SALIDA_SCB.VN_BOB_SALIDA TOTAL_BOB, ");
		query.append("                             TOTAL_INGRESO_SALIDA_SCB.VN_USD_INGRESO - TOTAL_INGRESO_SALIDA_SCB.VN_USD_SALIDA TOTAL_USD, ");
		query.append("                             TOTAL_INGRESO_SALIDA_SCB.MP_BOB_INGRESO - TOTAL_INGRESO_SALIDA_SCB.MP_BOB_SALIDA TOTAL_PRECIO_MRKT_BOB, ");
		query.append("                             TOTAL_INGRESO_SALIDA_SCB.MP_USD_INGRESO - TOTAL_INGRESO_SALIDA_SCB.MP_USD_SALIDA TOTAL_PRECIO_MRKT_USD  ");
		query.append(" FROM ");
		query.append("   (SELECT NVL(SUM(CASE ");
		query.append("                       WHEN PCO.PHYSICAL_OPERATION_TYPE = 673 THEN PC.CERTIFICATE_QUANTITY ");
		query.append("                   END), 0) TOTAL_INGRESO, ");
		query.append("           NVL(SUM(CASE ");
		query.append("                       WHEN PCO.PHYSICAL_OPERATION_TYPE = 674 THEN PC.CERTIFICATE_QUANTITY ");
		query.append("                   END), 0) TOTAL_SALIDA, ");
		query.append("           NVL(SUM(CASE ");
		query.append("                       WHEN PCO.PHYSICAL_OPERATION_TYPE = 673 THEN PC.CERTIFICATE_QUANTITY * S.INITIAL_NOMINAL_VALUE * CASE ");
		query.append(" WHEN S.CURRENCY = 127 THEN 1 ");
		query.append(" WHEN S.CURRENCY IN (1734, 1304, 430) THEN 1 * nvl( ");
		query.append("               (SELECT D.BUY_PRICE ");
		query.append("                FROM DAILY_EXCHANGE_RATES D ");
		query.append("                WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY') "); 
		query.append("                  AND D.ID_CURRENCY = S.CURRENCY ");
		query.append("                  AND D.ID_SOURCE_INFORMATION = 212),1) ");
		query.append(" WHEN S.CURRENCY IN (1853) THEN 1 * nvl(  ");
		query.append("    (SELECT D.BUY_PRICE ");
		query.append("     FROM DAILY_EXCHANGE_RATES D  ");
		query.append("     WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY') ");
		query.append("       AND D.ID_CURRENCY = 430 ");
		query.append("       AND D.ID_SOURCE_INFORMATION = 212),1) ");
		query.append(" ELSE 1 ");
		query.append(" END ");
		query.append(" ELSE 0 ");
		query.append(" END), 0) VN_BOB_INGRESO, ");
		query.append(" NVL(SUM(CASE ");
		query.append(" WHEN PCO.PHYSICAL_OPERATION_TYPE = 673 THEN PC.CERTIFICATE_QUANTITY * S.INITIAL_NOMINAL_VALUE * CASE ");
		query.append(" WHEN S.CURRENCY = 127 THEN 1 / nvl( ");
		query.append(" (SELECT D.BUY_PRICE ");
		query.append(" FROM DAILY_EXCHANGE_RATES D ");
		query.append(" WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY') ");
		query.append("   AND D.ID_CURRENCY = 430 ");
		query.append("   AND D.ID_SOURCE_INFORMATION = 212),1) ");
		query.append(" WHEN S.CURRENCY IN (1734, 1304) THEN 1 * nvl( ");
		query.append("          (SELECT D.BUY_PRICE ");
		query.append("           FROM DAILY_EXCHANGE_RATES D ");
		query.append("           WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY') ");
		query.append("             AND D.ID_CURRENCY = S.CURRENCY ");
		query.append("             AND D.ID_SOURCE_INFORMATION = 212),1) / nvl( ");
		query.append("                                                           (SELECT D.BUY_PRICE ");
		query.append("                                                            FROM DAILY_EXCHANGE_RATES D ");
		query.append("                                                            WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY') ");
		query.append("                                                              AND D.ID_CURRENCY = 430 ");
		query.append("                                                              AND D.ID_SOURCE_INFORMATION = 212),1) ");
		query.append(" WHEN S.CURRENCY IN (430, 1853) THEN 1 ");
		query.append(" ELSE 1 ");
		query.append(" END ");
		query.append(" ELSE 0 ");
		query.append(" END), 0) VN_USD_INGRESO, ");
		query.append(" NVL(SUM(CASE ");
		query.append(" WHEN PCO.PHYSICAL_OPERATION_TYPE = 673 THEN PC.CERTIFICATE_QUANTITY * (ROUND(NVL(PCM.MARKET_PRICE, 0), 2)) * CASE ");
		query.append(" WHEN S.CURRENCY = 127 THEN 1 ");
		query.append(" WHEN S.CURRENCY IN (1734, 1304, 430) THEN 1 * nvl( ");
		query.append("                            (SELECT D.BUY_PRICE ");
		query.append("                             FROM DAILY_EXCHANGE_RATES D ");
		query.append("                             WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY') ");
		query.append("                               AND D.ID_CURRENCY = S.CURRENCY ");
		query.append("                               AND D.ID_SOURCE_INFORMATION = 212),1) ");
		query.append(" WHEN S.CURRENCY IN (1853) THEN 1 * nvl( ");
		query.append("                 (SELECT D.BUY_PRICE ");
		query.append("                  FROM DAILY_EXCHANGE_RATES D ");
		query.append("                  WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY') ");
		query.append("                    AND D.ID_CURRENCY = 430 ");
		query.append("                    AND D.ID_SOURCE_INFORMATION = 212),1) ");
		query.append(" ELSE 1 ");
		query.append(" END ");
		query.append(" ELSE 0 ");
		query.append(" END), 0) MP_BOB_INGRESO, ");
		query.append(" NVL(SUM(CASE ");
		query.append(" WHEN PCO.PHYSICAL_OPERATION_TYPE = 673 THEN PC.CERTIFICATE_QUANTITY * (ROUND(NVL(PCM.MARKET_PRICE, 0), 2)) * CASE ");
		query.append(" WHEN S.CURRENCY = 127 THEN 1 / nvl( ");
		query.append("             (SELECT D.BUY_PRICE ");
		query.append("              FROM DAILY_EXCHANGE_RATES D ");
		query.append("              WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY') ");
		query.append("                AND D.ID_CURRENCY = 430 ");
		query.append("                AND D.ID_SOURCE_INFORMATION = 212),1) ");
		query.append(" WHEN S.CURRENCY IN (1734, 1304) THEN 1 * nvl( ");
		query.append("                       (SELECT D.BUY_PRICE ");
		query.append("                        FROM DAILY_EXCHANGE_RATES D ");
		query.append("                        WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY') ");
		query.append("                          AND D.ID_CURRENCY = S.CURRENCY ");
		query.append("                          AND D.ID_SOURCE_INFORMATION = 212),1) / nvl( ");
		query.append("                                                                        (SELECT D.BUY_PRICE  ");
		query.append("                                                                         FROM DAILY_EXCHANGE_RATES D ");
		query.append("                                                                         WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY') ");
		query.append("                                                                           AND D.ID_CURRENCY = 430 ");
		query.append("                                                                           AND D.ID_SOURCE_INFORMATION = 212),1) ");
		query.append(" WHEN S.CURRENCY IN (430, 1853) THEN 1 ");
		query.append(" ELSE 1 ");
		query.append(" END");
		query.append(" ELSE 0");
		query.append(" END), 0) MP_USD_INGRESO,");
		query.append(" NVL(SUM(CASE");
		query.append(" WHEN PCO.PHYSICAL_OPERATION_TYPE = 674 THEN PC.CERTIFICATE_QUANTITY * S.INITIAL_NOMINAL_VALUE * CASE");
		query.append(" WHEN S.CURRENCY = 127 THEN 1");
		query.append(" WHEN S.CURRENCY IN (1734, 1304, 430) THEN 1 * nvl(");
		query.append("               (SELECT D.BUY_PRICE");
		query.append("                FROM DAILY_EXCHANGE_RATES D");
		query.append("                WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY')");
		query.append("                  AND D.ID_CURRENCY = S.CURRENCY");
		query.append("                  AND D.ID_SOURCE_INFORMATION = 212),1)");
		query.append(" WHEN S.CURRENCY IN (1853) THEN 1 * nvl(");
		query.append("    (SELECT D.BUY_PRICE");
		query.append("     FROM DAILY_EXCHANGE_RATES D");
		query.append("     WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY')");
		query.append("       AND D.ID_CURRENCY = 430");
		query.append("       AND D.ID_SOURCE_INFORMATION = 212),1)");
		query.append(" ELSE 1");
		query.append(" END");
		query.append(" ELSE 0");
		query.append(" END), 0) VN_BOB_SALIDA,");
		query.append(" NVL(SUM(CASE");
		query.append(" WHEN PCO.PHYSICAL_OPERATION_TYPE = 674 THEN PC.CERTIFICATE_QUANTITY * S.INITIAL_NOMINAL_VALUE * CASE");
		query.append(" WHEN S.CURRENCY = 127 THEN 1 / nvl(");
		query.append(" (SELECT D.BUY_PRICE");
		query.append(" FROM DAILY_EXCHANGE_RATES D");
		query.append(" WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY')");
		query.append("   AND D.ID_CURRENCY = 430");
		query.append("   AND D.ID_SOURCE_INFORMATION = 212),1)");
		query.append(" WHEN S.CURRENCY IN (1734, 1304) THEN 1 * nvl(");
		query.append("          (SELECT D.BUY_PRICE");
		query.append("           FROM DAILY_EXCHANGE_RATES D");
		query.append("           WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY')");
		query.append("             AND D.ID_CURRENCY = S.CURRENCY");
		query.append("             AND D.ID_SOURCE_INFORMATION = 212),1) / nvl(");
		query.append("                                                           (SELECT D.BUY_PRICE");
		query.append("                                                            FROM DAILY_EXCHANGE_RATES D");
		query.append("                                                            WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY')");
		query.append("                                                              AND D.ID_CURRENCY = 430");
		query.append("                                                              AND D.ID_SOURCE_INFORMATION = 212),1)");
		query.append(" WHEN S.CURRENCY IN (430, 1853) THEN 1");
		query.append(" ELSE 1");
		query.append(" END");
		query.append(" ELSE 0");
		query.append(" END), 0) VN_USD_SALIDA,");
		query.append(" NVL(SUM(CASE");
		query.append(" WHEN PCO.PHYSICAL_OPERATION_TYPE = 674 THEN PC.CERTIFICATE_QUANTITY * (ROUND(NVL(PCM.MARKET_PRICE, 0), 2)) * CASE");
		query.append(" WHEN S.CURRENCY = 127 THEN 1");
		query.append(" WHEN S.CURRENCY IN (1734, 1304, 430) THEN 1 * nvl(");
		query.append("                            (SELECT D.BUY_PRICE");
		query.append("                             FROM DAILY_EXCHANGE_RATES D");
		query.append("                             WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY')");
		query.append("                               AND D.ID_CURRENCY = S.CURRENCY");
		query.append("                               AND D.ID_SOURCE_INFORMATION = 212),1)");
		query.append(" WHEN S.CURRENCY IN (1853) THEN 1 * nvl(");
		query.append("                 (SELECT D.BUY_PRICE");
		query.append("                  FROM DAILY_EXCHANGE_RATES D");
		query.append("                  WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY')");
		query.append("                    AND D.ID_CURRENCY = 430");
		query.append("                    AND D.ID_SOURCE_INFORMATION = 212),1)");
		query.append(" ELSE 1");
		query.append(" END");
		query.append(" ELSE 0");
		query.append(" END), 0) MP_BOB_SALIDA,");
		query.append(" NVL(SUM(CASE");
		query.append(" WHEN PCO.PHYSICAL_OPERATION_TYPE = 674 THEN PC.CERTIFICATE_QUANTITY * (ROUND(NVL(PCM.MARKET_PRICE, 0), 2)) * CASE");
		query.append(" WHEN S.CURRENCY = 127 THEN 1 / nvl(");
		query.append("             (SELECT D.BUY_PRICE");
		query.append("              FROM DAILY_EXCHANGE_RATES D");
		query.append("              WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY')");
		query.append("                AND D.ID_CURRENCY = 430");
		query.append("                AND D.ID_SOURCE_INFORMATION = 212),1)");
		query.append(" WHEN S.CURRENCY IN (1734, 1304) THEN 1 * nvl(");
		query.append("                       (SELECT D.BUY_PRICE");
		query.append("                        FROM DAILY_EXCHANGE_RATES D");
		query.append("                        WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY')");
		query.append("                          AND D.ID_CURRENCY = S.CURRENCY");
		query.append("                          AND D.ID_SOURCE_INFORMATION = 212),1) / nvl(");
		query.append("                                                                        (SELECT D.BUY_PRICE");
		query.append("                                                                         FROM DAILY_EXCHANGE_RATES D");
		query.append("                                                                         WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY')");
		query.append("                                                                           AND D.ID_CURRENCY = 430 ");
		query.append("                                                                           AND D.ID_SOURCE_INFORMATION = 212),1) ");
		query.append(" WHEN S.CURRENCY IN (430, 1853) THEN 1  ");
		query.append(" ELSE 1 ");
		query.append(" END ");
		query.append(" ELSE 0 ");
		query.append(" END), 0) MP_USD_SALIDA ");
		query.append(" FROM PHYSICAL_OPERATION_DETAIL POD ");
		query.append(" INNER JOIN PHYSICAL_CERTIFICATE PC ON PC.ID_PHYSICAL_CERTIFICATE_PK=POD.ID_PHYSICAL_CERTIFICATE_FK ");
		query.append(" INNER JOIN PHYSICAL_CERTIFICATE_OPERATION PCO ON PCO.ID_PHYSICAL_OPERATION_PK = POD.ID_PHYSICAL_OPERATION_FK ");
		query.append(" INNER JOIN CUSTODY_OPERATION CO ON (PCO.ID_PHYSICAL_OPERATION_PK=CO.ID_CUSTODY_OPERATION_PK) ");
		query.append(" INNER JOIN SECURITY S ON (PC.ID_SECURITY_CODE_FK=S.ID_SECURITY_CODE_PK) ");
		query.append(" INNER JOIN PHYSICAL_CERT_MARKETFACT PCM ON (PCM.ID_PHYSICAL_CERTIFICATE_FK=PC.ID_PHYSICAL_CERTIFICATE_PK) ");
		//--INNER JOIN PHYSICAL_BALANCE_DETAIL PBD ON (PC.ID_PHYSICAL_CERTIFICATE_PK=PBD.ID_PHYSICAL_CERTIFICATE_FK)
		query.append(" WHERE 1 = 1 ");
		query.append(" AND PCM.IND_ACTIVE = 1 ");
	    query.append(" AND POD.STATE = CASE PCO.PHYSICAL_OPERATION_TYPE  WHEN 673 THEN 601 WHEN 674 THEN 602   END ");
	    query.append(" AND CO.OPERATION_STATE IN (601,602) ");
		//Filtros de busqueda
		 if (to.getParticipant() != null)
			 query.append("    AND PCO.ID_PARTICIPANT_FK = " + to.getParticipant());
		   
	    if(to.getHolder_account()!=null)	
	    	query.append(" AND PCO.ID_HOLDER_ACCOUNT_FK = " + to.getHolder_account());
		
		if (to.getIssuer() != null)
			query.append(" AND S.ID_ISSUER_FK = '" + to.getIssuer()+"'");

		if (to.getSecurity_class() != null)
			query.append(" AND S.SECURITY_CLASS = " + to.getSecurity_class());
		
		if (to.getSecurity_code() != null)
			query.append(" AND S._ID_SECURITY_CODE_PK = '" + to.getSecurity_code()+"'");

		if (to.getCurrency_code() != null)
			query.append(" AND S.CURRENCY = " + to.getCurrency_code());	
				
		query.append(" AND CO.CONFIRM_DATE >= TRUNC(TO_DATE('" + to.getDate() + "', 'DD/MM/YYYY'), 'DD') ");
		query.append(" AND CO.CONFIRM_DATE < TRUNC(TO_DATE('" + to.getDate() + "', 'DD/MM/YYYY'), 'DD') + 1  ");
		query.append(" AND TRUNC(TO_DATE('" + to.getDate() + "', 'DD/MM/YYYY'), 'DD') = TO_DATE('" + to.getDate()+ "', 'DD/MM/YYYY') ");
		
		query.append("  AND ((EXISTS  ");
		query.append("          (SELECT had.ID_HOLDER_FK                                      ");
		query.append("           FROM HOLDER_ACCOUNT_DETAIL had                               ");
		query.append("           WHERE 1 = 1 ");
		if(to.getCui_code()!=null)
			query.append(" AND HAD.ID_HOLDER_FK = "+to.getCui_code());
		query.append(" AND had.ID_HOLDER_ACCOUNT_FK = PCO.ID_HOLDER_ACCOUNT_FK)))) TOTAL_INGRESO_SALIDA_SCB  ");


		return query.toString();
	}

	private String getQueryCutDatePhysical(ConciliationDailyOperationsTO to) {

		StringBuilder query = new StringBuilder();
		query.append(" SELECT TOTAL_CURRENCY.CURRENCY,             			 ");
		query.append("       TOTAL_CURRENCY.TOTAL_VALORES,          		 ");
		query.append("       TOTAL_CURRENCY.TOTAL_BOB,             			 ");
		query.append("       TOTAL_CURRENCY.TOTAL_USD,             			 ");
		query.append("       TOTAL_CURRENCY.TOTAL_BOB_RENTA_FIJA,   		 ");
		query.append("       TOTAL_CURRENCY.TOTAL_BOB_RENTA_VAR,     		 ");
		query.append("       TOTAL_CURRENCY.TOTAL_PRECIO_MRKT_BOB,  		 ");
		query.append("       TOTAL_CURRENCY.TOTAL_PREC_MRKT_BOB_RENTA_FIJA,  ");
		query.append("       TOTAL_CURRENCY.TOTAL_PREC_MRKT_BOB_RENTA_VAR    ");
		query.append(" FROM           ");
		query.append("  (SELECT      ");
		query.append("     (SELECT PARAMETER_NAME                   ");
		query.append("      FROM PARAMETER_TABLE                    ");
		query.append("      WHERE PARAMETER_TABLE_PK = S.CURRENCY) CURRENCY,                                       ");
		query.append("          NVL(SUM(PSV.TOTAL_BALANCE), 0) TOTAL_VALORES,                                      ");
		query.append("          NVL(SUM(S.INITIAL_NOMINAL_VALUE * PSV.TOTAL_BALANCE * CASE                         ");
		query.append("                     WHEN S.CURRENCY = 127 THEN 1      ");
		query.append("                     WHEN S.CURRENCY IN (430, 1304, 1734) THEN 1 * nvl(                               ");
		query.append("                                          (SELECT D.BUY_PRICE          ");
		query.append("                                           FROM DAILY_EXCHANGE_RATES D ");
		query.append("                                      WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate() + "', 'DD/MM/YYYY')                                            ");
		query.append("                                             AND D.ID_CURRENCY = S.CURRENCY                           ");
		query.append("                                             AND D.ID_SOURCE_INFORMATION = 212),1)                    ");
		query.append("                     WHEN S.CURRENCY IN (1853) THEN 1 * nvl(                                          ");
		query.append("                               (SELECT D.BUY_PRICE                     ");
		query.append("                                FROM DAILY_EXCHANGE_RATES D            ");
		query.append("                                WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate() + "', 'DD/MM/YYYY')        ");
		query.append("                                  AND D.ID_CURRENCY = 430              ");
		query.append("                                  AND D.ID_SOURCE_INFORMATION = 212),1)");
		query.append("                     ELSE 1                   ");
		query.append("                 END), 0) TOTAL_BOB,          ");
		query.append("          NVL(SUM(S.INITIAL_NOMINAL_VALUE * PSV.TOTAL_BALANCE * CASE                         ");
		query.append("                     WHEN S.CURRENCY = 127 THEN 1 / nvl(                                              ");
		query.append("                           (SELECT D.BUY_PRICE                         ");
		query.append("                            FROM DAILY_EXCHANGE_RATES D                ");
		query.append("                            WHERE TRUNC(D.DATE_RATE) = TO_DATE('"	+ to.getDate() + "', 'DD/MM/YYYY')            ");
		query.append("                              AND D.ID_CURRENCY = 430                  ");
		query.append("                              AND D.ID_SOURCE_INFORMATION = 212),1)    ");
		query.append("                     WHEN S.CURRENCY IN (1734, 1304) THEN 1 * nvl(                                    ");
		query.append("                                     (SELECT D.BUY_PRICE               ");
		query.append("                                      FROM DAILY_EXCHANGE_RATES D      ");
		query.append("                                      WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate() + "', 'DD/MM/YYYY')  ");
		query.append("                                        AND D.ID_CURRENCY = S.CURRENCY ");
		query.append("                                        AND D.ID_SOURCE_INFORMATION = 212),1) / nvl(                  ");
		query.append("                              (SELECT D.BUY_PRICE                                            ");
		query.append("                               FROM DAILY_EXCHANGE_RATES D                                   ");
		query.append("                               WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate() + "', 'DD/MM/YYYY')");
		query.append("                                 AND D.ID_CURRENCY = 430                                     ");
		query.append("                                 AND D.ID_SOURCE_INFORMATION = 212),1)                       ");
		query.append("                     WHEN S.CURRENCY IN (430, 1853) THEN 1                                            ");
		query.append("                     ELSE 1                   ");
		query.append("                 END), 0) TOTAL_USD,          ");
		query.append("          NVL(SUM(S.INITIAL_NOMINAL_VALUE * PSV.TOTAL_BALANCE * CASE                         ");
		query.append("                     WHEN S.INSTRUMENT_TYPE = 124 THEN CASE                                           ");
		query.append("                            WHEN S.CURRENCY = 127 THEN 1               ");
		query.append("                            WHEN S.CURRENCY IN (1734, 1304, 430) THEN 1 * nvl(                        ");
		query.append("                        (SELECT D.BUY_PRICE   ");
		query.append("                         FROM DAILY_EXCHANGE_RATES D                                         ");
		query.append("                         WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate() + "', 'DD/MM/YYYY')      ");
		query.append("                           AND D.ID_CURRENCY = S.CURRENCY                                    ");
		query.append("                           AND D.ID_SOURCE_INFORMATION = 212),1)                             ");
		query.append("                            WHEN S.CURRENCY IN (1853) THEN 1 * nvl(    ");
		query.append("             (SELECT D.BUY_PRICE              ");
		query.append("              FROM DAILY_EXCHANGE_RATES D     ");
		query.append("              WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate() + "', 'DD/MM/YYYY')                 ");
		query.append("                AND D.ID_CURRENCY = 430       ");
		query.append("                AND D.ID_SOURCE_INFORMATION = 212),1)                                        ");
		query.append("                            ELSE 1                                     ");
		query.append("                        END                                            ");
		query.append("                     ELSE 0                   ");
		query.append("                 END), 0) TOTAL_BOB_RENTA_FIJA,        ");
		query.append("          NVL(SUM(S.INITIAL_NOMINAL_VALUE * PSV.TOTAL_BALANCE * CASE                         ");
		query.append("                     WHEN S.INSTRUMENT_TYPE = 399 THEN CASE                                           ");
		query.append("                            WHEN S.CURRENCY = 127 THEN 1               ");
		query.append("                            WHEN S.CURRENCY IN (1734, 1304, 430) THEN 1 * nvl(                        ");
		query.append("                        (SELECT D.BUY_PRICE   ");
		query.append("                         FROM DAILY_EXCHANGE_RATES D                                         ");
		query.append("                         WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate() + "', 'DD/MM/YYYY')      ");
		query.append("                           AND D.ID_CURRENCY = S.CURRENCY                                    ");
		query.append("                           AND D.ID_SOURCE_INFORMATION = 212),1)                             ");
		query.append("                            WHEN S.CURRENCY IN (1853) THEN 1 * nvl(    ");
		query.append("             (SELECT D.BUY_PRICE              ");
		query.append("              FROM DAILY_EXCHANGE_RATES D     ");
		query.append("              WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate() + "', 'DD/MM/YYYY')                 ");
		query.append("                AND D.ID_CURRENCY = 430       ");
		query.append("                AND D.ID_SOURCE_INFORMATION = 212),1)                                        ");
		query.append("                            ELSE 1                                     ");
		query.append("                        END                                            ");
		query.append("                     ELSE 0                   ");
		query.append("                 END), 0) TOTAL_BOB_RENTA_VAR,");
		query.append("          NVL(SUM(ROUND(PCM.MARKET_PRICE, 2) * PSV.TOTAL_BALANCE * CASE                      ");
		query.append("                        WHEN S.CURRENCY = 127 THEN 1   ");
		query.append("                        WHEN S.CURRENCY IN (1734, 1304, 430) THEN 1 * nvl(                            ");
		query.append("                                             (SELECT D.BUY_PRICE       ");
		query.append("                                              FROM DAILY_EXCHANGE_RATES D                             ");
		query.append("                                              WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate() + "', 'DD/MM/YYYY')                                         ");
		query.append(" AND D.ID_CURRENCY = S.CURRENCY                        ");
		query.append(" AND D.ID_SOURCE_INFORMATION = 212),1)                 ");
		query.append("                        WHEN S.CURRENCY IN (1853) THEN 1 * nvl(                                       ");
		query.append("                                  (SELECT D.BUY_PRICE                  ");
		query.append("                                   FROM DAILY_EXCHANGE_RATES D         ");
		query.append("                                   WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate() + "', 'DD/MM/YYYY')     ");
		query.append("                                     AND D.ID_CURRENCY = 430           ");
		query.append("                                     AND D.ID_SOURCE_INFORMATION = 212),1)                            ");
		query.append("                        ELSE 1                ");
		query.append("                    END), 0) TOTAL_PRECIO_MRKT_BOB,    ");
		query.append("          NVL(SUM(ROUND(PCM.MARKET_PRICE, 2) * PSV.TOTAL_BALANCE * CASE                      ");
		query.append("                        WHEN S.INSTRUMENT_TYPE = 124 THEN CASE                                        ");
		query.append("                               WHEN S.CURRENCY = 127 THEN 1            ");
		query.append("                               WHEN S.CURRENCY IN (1734, 1304, 430) THEN 1 * nvl(                     ");
		query.append("                           (SELECT D.BUY_PRICE");
		query.append("                            FROM DAILY_EXCHANGE_RATES D                                      ");
		query.append("                            WHERE TRUNC(D.DATE_RATE) = TO_DATE('"	+ to.getDate() + "', 'DD/MM/YYYY')   ");
		query.append("                              AND D.ID_CURRENCY = S.CURRENCY                                 ");
		query.append("                              AND D.ID_SOURCE_INFORMATION = 212),1)                          ");
		query.append("                               WHEN S.CURRENCY IN (1853) THEN 1 * nvl( ");
		query.append("                (SELECT D.BUY_PRICE           ");
		query.append("                 FROM DAILY_EXCHANGE_RATES D  ");
		query.append("                 WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate() + "', 'DD/MM/YYYY')              ");
		query.append("                   AND D.ID_CURRENCY = 430    ");
		query.append("                   AND D.ID_SOURCE_INFORMATION = 212),1)                                     ");
		query.append("                               ELSE 1                                  ");
		query.append("                           END                                         ");
		query.append("                        ELSE 0                ");
		query.append("                    END), 0) TOTAL_PREC_MRKT_BOB_RENTA_FIJA,                                          ");
		query.append("          NVL(SUM(ROUND(PCM.MARKET_PRICE, 2) * PSV.TOTAL_BALANCE * CASE                      ");
		query.append("                        WHEN S.INSTRUMENT_TYPE = 399 THEN CASE                                        ");
		query.append("                               WHEN S.CURRENCY = 127 THEN 1            ");
		query.append("                               WHEN S.CURRENCY IN (1734, 1304, 430) THEN 1 * nvl(                     ");
		query.append("                           (SELECT D.BUY_PRICE");
		query.append("                            FROM DAILY_EXCHANGE_RATES D                                      ");
		query.append("                            WHERE TRUNC(D.DATE_RATE) = TO_DATE('"	+ to.getDate() + "', 'DD/MM/YYYY')   ");
		query.append("                              AND D.ID_CURRENCY = S.CURRENCY                                  ");
		query.append("                              AND D.ID_SOURCE_INFORMATION = 212),1)                           ");
		query.append("                               WHEN S.CURRENCY IN (1853) THEN 1 * nvl(  ");
		query.append("                (SELECT D.BUY_PRICE            ");
		query.append("                 FROM DAILY_EXCHANGE_RATES D   ");
		query.append("                 WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getDate() + "', 'DD/MM/YYYY')               ");
		query.append("                   AND D.ID_CURRENCY = 430     ");
		query.append("                   AND D.ID_SOURCE_INFORMATION = 212),1)                                      ");
		query.append("                               ELSE 1                                   ");
		query.append("                           END                                          ");
		query.append("                        ELSE 0                 ");
		query.append("                    END), 0) TOTAL_PREC_MRKT_BOB_RENTA_VAR                                             ");
		query.append("   FROM BALANCE_PSY_VIEW PSV                   ");
		query.append("   INNER JOIN PHYSICAL_CERT_MARKETFACT PCM ON PCM.ID_PHYSICAL_CERTIFICATE_FK = PSV.ID_PHYSICAL_CERTIFICATE_PK                                         ");
		query.append("   INNER JOIN SECURITY S ON S.ID_SECURITY_CODE_PK = PSV.ID_SECURITY_CODE_PK                   ");
		query.append("   WHERE 1 = 1  ");
		
		 if (to.getParticipant() != null)
			 query.append("    AND PSV.ID_PARTICIPANT_pK = " + to.getParticipant());
		   
	    if(to.getHolder_account()!=null)	
	    	query.append(" AND PSV.ID_HOLDER_ACCOUNT_PK = " + to.getHolder_account());
		
		if (to.getIssuer() != null)
			query.append(" AND S.ID_ISSUER_FK = '" + to.getIssuer()+"'");

		if (to.getSecurity_class() != null)
			query.append(" AND S.SECURITY_CLASS = " + to.getSecurity_class());
		
		if (to.getSecurity_code() != null)
			query.append(" AND S._ID_SECURITY_CODE_PK = '" + to.getSecurity_code()+"'");

		if (to.getCurrency_code() != null)
			query.append(" AND S.CURRENCY = " + to.getCurrency_code());	
			
		query.append("     AND ((EXISTS                              ");
		query.append("             (SELECT had.ID_HOLDER_FK          ");
		query.append("              FROM HOLDER_ACCOUNT_DETAIL had   ");
		query.append("              WHERE 1 = 1 ");
		
		if(to.getCui_code()!=null)
			   query.append(" AND HAD.ID_HOLDER_FK = "+to.getCui_code());
		
		query.append(" AND had.ID_HOLDER_ACCOUNT_FK = PSV.ID_HOLDER_ACCOUNT_PK)))                    ");
		query.append("     AND s.security_type in (1762, 1763)       ");
		//Optimizar fecha
		//query.append("     AND TRUNC(PSV.CUT_DATE) = TO_DATE('" + to.getDate()+ "', 'DD/MM/YYYY')                   ");
		query.append("   AND (PSV.CUT_DATE >= TRUNC(TO_DATE('" + to.getDate()+ "', 'DD/MM/YYYY'), 'DD')                        ");
		query.append("   AND TRUNC(TO_DATE('" + to.getDate()+ "', 'DD/MM/YYYY'), 'DD') + 1 > PSV.CUT_DATE                 ");
		query.append("   AND TRUNC(TO_DATE('" + to.getDate() + "', 'DD/MM/YYYY'), 'DD') = TO_DATE('"+ to.getDate()+ "', 'DD/MM/YYYY'))  ");		
		query.append("   GROUP BY S.CURRENCY                         ");
		query.append("   ORDER BY S.CURRENCY) TOTAL_CURRENCY");

		return query.toString();

	}
}
