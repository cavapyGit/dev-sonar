package com.pradera.report.generation.executor.security.to;

import java.io.Serializable;
import java.math.BigDecimal;

public class SecuritySettlementTO implements Serializable{
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long idParticipantBuyerPk;
	
	private Long idParticipantSellerPk;
	
	private String idIssuerPk;
	
	private String issuerMnemonic;
	
	private Integer instrumentType;
	
	private Integer securityClass;
	
	private BigDecimal nominalValue;
	
	private BigDecimal acquisitionPrice;
	
	private Integer currency;
	
	private long countSeller;
	
	private long countBuyer;
	
	public SecuritySettlementTO() {
		// TODO Auto-generated constructor stub
	}

	public Long getIdParticipantBuyerPk() {
		return idParticipantBuyerPk;
	}

	public void setIdParticipantBuyerPk(Long idParticipantBuyerPk) {
		this.idParticipantBuyerPk = idParticipantBuyerPk;
	}

	public Long getIdParticipantSellerPk() {
		return idParticipantSellerPk;
	}

	public void setIdParticipantSellerPk(Long idParticipantSellerPk) {
		this.idParticipantSellerPk = idParticipantSellerPk;
	}

	public String getIssuerMnemonic() {
		return issuerMnemonic;
	}

	public void setIssuerMnemonic(String issuerMnemonic) {
		this.issuerMnemonic = issuerMnemonic;
	}

	public Integer getInstrumentType() {
		return instrumentType;
	}

	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	public Integer getSecurityClass() {
		return securityClass;
	}

	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	public BigDecimal getNominalValue() {
		return nominalValue;
	}

	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}

	public BigDecimal getAcquisitionPrice() {
		return acquisitionPrice;
	}

	public void setAcquisitionPrice(BigDecimal acquisitionPrice) {
		this.acquisitionPrice = acquisitionPrice;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public long getCountSeller() {
		return countSeller;
	}

	public void setCountSeller(long countSeller) {
		this.countSeller = countSeller;
	}

	public long getCountBuyer() {
		return countBuyer;
	}

	public void setCountBuyer(long countBuyer) {
		this.countBuyer = countBuyer;
	}

	public String getIdIssuerPk() {
		return idIssuerPk;
	}

	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}
	
}
