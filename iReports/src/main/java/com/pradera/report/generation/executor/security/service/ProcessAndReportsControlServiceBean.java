package com.pradera.report.generation.executor.security.service;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class ProcessAndReportsControlServiceBean extends CrudDaoServiceBean{
	
	public ProcessAndReportsControlServiceBean(){
		//TODO
	}

	public  List<Object[]> getProcessesAndReportsByUser(String userCode, Long userType, Long instName, 
														Long state, Long instType, Date dtIni, Date dtEnd) {
	   	StringBuilder sbQuery = new StringBuilder();
	   	sbQuery.append(" select par.name,  "). //0
	   	append("insec.institution_name, "). //1
	   	append("pl.id_user_account, "). //2
	   	append("pl.last_modify_ip, "). //3
	   	append("to_char(pl.process_date,'dd/MM/yyyy HH24:mi:ss'), "). //4
	   	append("bps.mnemonic, "). //5
	   	append("bps.process_name, "). //6
	   	append("bps.process_type, "). //7
	   	append("pt.parameter_name, "). //8
	   	append("pld.parameter_name as parametro, "). //9
	   	append("pld.parameter_value "). //10
	   	append(", uap.id_process_logger_fk, uap.id_report_logger_fk "). //typeProcess
	   	append("from business_process bps, process_logger_detail pld, process_logger pl left join USER_AUDIT_PROCESS ").
	   	append("uap on uap.id_process_logger_fk=pl.id_process_logger_pk, parameter_table pt,User_Account uac,  ").
	   	append("parameter par, INSTITUTIONS_SECURITY insec ").	
	   	append("where pld.id_process_logger_fk = pl.id_process_logger_pk and ").
	   	append("bps.id_business_process_pk = pl.id_business_process_fk and pt.parameter_table_pk = pl.logger_state ").
	   	append("and uac.login_user = pl.id_user_account and par.id_parameter_pk = uac.institution_type_fk ").
	   	append("and insec.id_institution_security_pk = uac.id_security_institution_fk ").
	   	append("and trunc(pl.process_date) between :ini_date and :end_date ");
	   	
	   	if (Validations.validateIsNotNullAndNotEmpty(userCode)){   		
	   		sbQuery.append(" And uac.login_user = :user_code");
	   	}
	   	
	   	if (Validations.validateIsNotNullAndNotEmpty(userType)){
	   		sbQuery.append(" And uac.type = :user_type");
	   	}
	   	
	   	if (Validations.validateIsNotNullAndPositive(instName)){
	   		sbQuery.append(" And uac.id_security_institution_fk = :inst_name");
	   	}
	   	
	   	if (Validations.validateIsNotNullAndPositive(state)){
	   		sbQuery.append(" And pl.logger_state = :process_state");
	   	}
	   	
	   	if(Validations.validateIsNotNullAndNotEmpty(instType)){
	   		sbQuery.append(" And uac.institution_type_fk = :inst_type");
	   	}
	   	
	   	sbQuery.append(" order by process_date desc ");
	   	
	   	Query query = em.createNativeQuery(sbQuery.toString());
	   	query.setParameter("ini_date", dtIni, TemporalType.DATE);
	   	query.setParameter("end_date", dtEnd, TemporalType.DATE);
	   	
	   	if (Validations.validateIsNotNullAndNotEmpty(userCode)) {   		
	   		query.setParameter("user_code", userCode);
	   	}
	   	
	   	if (Validations.validateIsNotNullAndNotEmpty(userType)) {
	   		query.setParameter("user_type", userType);
	   	}
	   	
	   	if (Validations.validateIsNotNullAndPositive(instName)) {
	   		query.setParameter("user_type", instName);
	   	}
	   	
	   	if (Validations.validateIsNotNullAndPositive(state)) {   		
	   		query.setParameter("process_state", state);
	   	}
	   	
	   	if (Validations.validateIsNotNullAndNotEmpty(instType)) {
	   		query.setParameter("inst_type", instType);
	   	}	
	   	
	   	return query.getResultList();
   }
}
