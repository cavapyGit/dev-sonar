package com.pradera.report.generation.executor.accounting.service;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.to.AccountingParameterTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounting.AccountingAccount;
import com.pradera.model.accounting.AccountingParameter;
import com.pradera.model.accounting.AccountingProcess;
import com.pradera.model.accounting.AccountingReceiptDetail;
import com.pradera.model.accounting.AccountingSourceDetail;
import com.pradera.model.accounting.type.AccountingFieldSourceType;
import com.pradera.model.accounting.type.AccountingReceiptStateType;
import com.pradera.model.accounting.type.AccountingStartType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.custody.type.PhysicalCertificateStateType;
import com.pradera.model.custody.type.SituationType;
import com.pradera.model.custody.type.StateType;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.report.generation.executor.accounting.to.AccountingProcessTo;
import com.pradera.report.generation.executor.accounting.to.OperationBalanceTo;
import com.pradera.report.generation.executor.billing.to.DailyExchangeRateFilter;
// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingServiceReportServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/12/2014
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class AccountingReportQueryServiceBean extends CrudDaoServiceBean  implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The participant service bean. */
	@EJB
	ParticipantServiceBean participantServiceBean;
	
	/**  Begin the constant generic. */
	public static final Integer CURRENCY_TYPE_PK = Integer.valueOf(53);

	/**
	 * Instantiates a new accounting report query service bean.
	 */
	public AccountingReportQueryServiceBean(){

	}
	

	/**
	 * Gets the list parameter table service bean.
	 *
	 * @param filter the filter
	 * @return the list parameter table service bean
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getListParameterTable(ParameterTableTO filter) throws ServiceException	{
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select P.parameterTablePk,P.parameterName, P.indicator4 From ParameterTable P ");
		sbQuery.append(" Where 1 = 1 ");

		if(Validations.validateIsNotNull(filter.getState())){
			sbQuery.append(" And P.parameterState = :statePrm");
		}

		if(Validations.validateIsNotNull(filter.getParameterTablePk())){
			sbQuery.append(" And P.parameterTablePk = :parameterTablePkPrm");
		}

		if(Validations.validateIsNotNull(filter.getMasterTableFk())){
			sbQuery.append(" And P.masterTable.masterTablePk = :masterTableFkPrm");
		}

		if(Validations.validateIsNotNull(filter.getParameterTableCd())){
			sbQuery.append(" And P.parameterTableCd = :parameterTableCdPrm");
		}
				
    	Query query = em.createQuery(sbQuery.toString());  

    	if(Validations.validateIsNotNull(filter.getState())){
        	query.setParameter("statePrm", filter.getState());
		}

		if(Validations.validateIsNotNull(filter.getParameterTablePk())){
	    	query.setParameter("parameterTablePkPrm", filter.getParameterTablePk());
		}

		if(Validations.validateIsNotNull(filter.getMasterTableFk())){
	    	query.setParameter("masterTableFkPrm", filter.getMasterTableFk());
		}

		if(Validations.validateIsNotNull(filter.getParameterTableCd())){
	    	query.setParameter("parameterTableCdPrm", filter.getParameterTableCd());
		}
		List<Object> objectList = query.getResultList();
		List<ParameterTable> parameterTableList = new ArrayList<ParameterTable>();
		for(int i=0;i<objectList.size();++i){			
			//Getting Results from data base
			Object[] sResults = (Object[])objectList.get(i);
			ParameterTable parameterTable = new ParameterTable();
			parameterTable.setParameterTablePk((Integer) sResults[0]);
			parameterTable.setParameterName((String)sResults[1]);
			parameterTable.setIndicator4((Integer) sResults[2]);
			parameterTableList.add(parameterTable);
		}
		return parameterTableList;
	}

	/**
	 * *
	 * Find Accounting Parameter with Filter.
	 *
	 * @param filter the filter
	 * @return the list
	 */
	public List<AccountingParameter> findListParameterAccountingByFilter(AccountingParameterTO filter){
		 		
	 	
		StringBuilder stringBuilderSqlBase = new StringBuilder();		
		stringBuilderSqlBase.append(" SELECT  AC.ID_ACCOUNTING_PARAMETER_PK ");
		stringBuilderSqlBase.append(" ,( select pt.description from parameter_table pt where AC.PARAMETER_TYPE=pt.parameter_table_pk ) as description_parameter_type " );
		stringBuilderSqlBase.append(" ,AC.NR_CORRELATIVE  ");
		stringBuilderSqlBase.append(" ,AC.PARAMETER_NAME  ");
		stringBuilderSqlBase.append(" ,AC.DESCRIPTION     ");		 
		stringBuilderSqlBase.append(" ,( select pt.description from parameter_table pt where AC.STATUS=pt.parameter_table_pk ) as description_accounting_type " );
		
		stringBuilderSqlBase.append(" ,AC.PARAMETER_TYPE  ");
		
		stringBuilderSqlBase.append(" FROM ACCOUNTING_PARAMETER  AC ");

		stringBuilderSqlBase.append(" WHERE 1=1 ");   
		
		if ( Validations.validateIsNotNullAndPositive(filter.getParameterType()) ){
			stringBuilderSqlBase.append(" AND AC.PARAMETER_TYPE = :parameterType    ");
		}
		if ( Validations.validateIsNotNullAndNotEmpty(filter.getParameterName()) ){
			stringBuilderSqlBase.append(" AND AC.PARAMETER_NAME = :parameterName    ");
		}
		if ( Validations.validateIsNotNull(filter.getIdAccountingParameterPk()) ){
			stringBuilderSqlBase.append(" AND AC.ID_ACCOUNTING_PARAMETER_PK =:idAccountingParameterPk   ");
		}
		if ( Validations.validateIsNotNullAndPositive(filter.getStatus()) ){
			stringBuilderSqlBase.append(" AND AC.STATUS = :status    ");
		}
		 
		stringBuilderSqlBase.append(" ORDER BY AC.PARAMETER_TYPE , AC.NR_CORRELATIVE ");
		
		Query query = em.createNativeQuery(stringBuilderSqlBase.toString());	
		
		if ( Validations.validateIsNotNullAndPositive(filter.getParameterType()) ){
			query.setParameter("parameterType", filter.getParameterType() );
		}		
		if ( Validations.validateIsNotNullAndNotEmpty(filter.getParameterName()) ){
			query.setParameter("parameterName", filter.getParameterName() );
		} 
		if ( Validations.validateIsNotNull(filter.getIdAccountingParameterPk()) ){
			query.setParameter("idAccountingParameterPk", filter.getIdAccountingParameterPk() );
		} 
		if ( Validations.validateIsNotNullAndPositive(filter.getStatus()) ){
			query.setParameter("status", filter.getStatus() );
		} 
		
		
		
		List<Object[]>  objectList = query.getResultList();
		
		
		List<AccountingParameter> listAccountingParameters  = new ArrayList<AccountingParameter>();

		
		for(int i=0;i<objectList.size();++i){	
			Object[] sResults = (Object[])objectList.get(i);
			AccountingParameter accountingParameter = new AccountingParameter();
			
			accountingParameter.setIdAccountingParameterPk(Integer.parseInt(sResults[0]==null?"":sResults[0].toString())); 
			accountingParameter.setDescriptionParameterType(sResults[1]==null?"":sResults[1].toString()); 			
			accountingParameter.setNrCorrelative(sResults[2]==null?null:Integer.parseInt(sResults[2].toString()));
			accountingParameter.setParameterName(sResults[3]==null?"":sResults[3].toString());			
			accountingParameter.setDescription(sResults[4]==null?"":sResults[4].toString());
			accountingParameter.setDescriptionParameterStatus(sResults[5]==null?"":sResults[5].toString()); 
			accountingParameter.setParameterType(sResults[6]==null?null:Integer.parseInt(sResults[6].toString()));
			accountingParameter.setNameWithDescription(sResults[3] + " - " +sResults[4].toString());		
			
			listAccountingParameters.add(accountingParameter);
			 
		}
		return listAccountingParameters;
	}
	
	/**
	 * Getting All Participant.
	 *
	 * @return the list participant
	 */
	public Map<Long,Participant> getListParticipant(){
		Map<Long,Participant> mapParticipant=new HashMap<Long,Participant>();
		Participant part=new Participant();
		//part.setState(ParticipantStateType.REGISTERED.getCode());
		List<Participant> listParticipant =participantServiceBean.findParticipantsByFilter(part);
		
		if(Validations.validateListIsNotNullAndNotEmpty(listParticipant)){
			for (Participant participant : listParticipant) {
				mapParticipant.put(participant.getIdParticipantPk(), participant);
			}
		}
		
		return mapParticipant;
	}
	
	
	/**
	 * Find balance for available.
	 *
	 * @return the operation balance to
	 */
	public OperationBalanceTo findBalanceForAvailable(AccountingProcessTo filter){
 		
		StringBuilder stringBuilderSqlBase = new StringBuilder();
		List<Object[]>  objectList=null;
		Query query =null;
		
			
			stringBuilderSqlBase.append(" SELECT  SUM((HAB.AVAILABLE_BALANCE)* ").
								 append(" 	DECODE(se.security_class,426,").
								 
								 append(" 	ROUND(FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt), 4), ").
								 append(" 	ROUND(FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt), 2)) ").
								 /*append(" 	ROUND( 			decode(SE.STATE_SECURITY,131,SE.current_nominal_value, ").
								 append(" 	FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt))		  	,4), ").
								 append(" 	ROUND( 			decode(SE.STATE_SECURITY,131,se.current_nominal_value,").
								 append(" 	FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt)) 			,2)) ").*/
								 append(" ) AS DISPONIBLE, ").
								 append(" SE.CURRENCY   ").
								 append(" FROM MARKET_FACT_VIEW  HAB  ").
								 append(" INNER JOIN SECURITY SE	  ").
								 append(" ON SE.ID_SECURITY_CODE_PK=HAB.ID_SECURITY_CODE_PK   ").
								 append(" WHERE 1 = 1").
								 append(" AND HAB.CUT_DATE >= TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD')").
								 append(" AND HAB.CUT_DATE < TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD') + 1  ").
								 append(" AND TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD') = TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY')").
								 append(" GROUP BY SE.CURRENCY ").
						 		 append("    ");
			
			stringBuilderSqlBase.append(" ORDER BY SE.CURRENCY ");
			
			query = em.createNativeQuery(stringBuilderSqlBase.toString());
			query.setParameter("dateCourt", filter.getExecutionDate());
		
		
		
		
		objectList = query.getResultList();
		
		OperationBalanceTo operationBalanceTo=new OperationBalanceTo();
		
		for(int i=0;i<objectList.size();++i){

			Object[] sResults = (Object[])objectList.get(i);
			BigDecimal amountBalance=new BigDecimal(sResults[0].toString());
			Integer currency =Integer.parseInt(sResults[1].toString());
			
			operationBalanceTo.fillBalanceForCurrency(currency, amountBalance);

		}
		
		
		return operationBalanceTo;
	}
	
	/**
	 * Find balance for reported.
	 *
	 * @return the operation balance to
	 */
	public OperationBalanceTo findBalanceForReported(AccountingProcessTo filter){
 		
		StringBuilder stringBuilderSqlBase = new StringBuilder();
		List<Object[]>  objectList=null;
		Query query =null;
		
			stringBuilderSqlBase.append(" SELECT  SUM(HAB.REPORTED_BALANCE* ").
								 append(" 	DECODE(se.security_class,426, ").
								 append(" 	ROUND(FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt), 4), ").
								 append(" 	ROUND(FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt), 2)) ").
								 /*append(" 	ROUND( 			decode(SE.STATE_SECURITY,131,SE.current_nominal_value, ").
								 append(" 	FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt))		  	,4), ").
								 append(" 	ROUND( 			decode(SE.STATE_SECURITY,131,se.current_nominal_value, ").
								 append(" 	FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt)) 			,2)) ").*/
								 append(" ) AS REPORTADO, ").
								 append(" SE.CURRENCY   ").
								 append(" FROM MARKET_FACT_VIEW  HAB  ").
								 append(" INNER JOIN SECURITY SE	  ").
								 append(" ON SE.ID_SECURITY_CODE_PK=HAB.ID_SECURITY_CODE_PK   ").
								 append(" WHERE 1 = 1 ").
								 append(" AND HAB.CUT_DATE >= TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD')").
								 append(" AND HAB.CUT_DATE < TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD') + 1  ").
								 append(" AND TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD') = TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY')").
								 append(" GROUP BY SE.CURRENCY ").
						 		 append("    ");

			stringBuilderSqlBase.append(" ORDER BY SE.CURRENCY ");
			
			query = em.createNativeQuery(stringBuilderSqlBase.toString());	
			query.setParameter("dateCourt", filter.getExecutionDate());
		
		objectList = query.getResultList();
		OperationBalanceTo operationBalanceTo=new OperationBalanceTo();
		
		for(int i=0;i<objectList.size();++i){

			Object[] sResults = (Object[])objectList.get(i);
			BigDecimal amountBalance=new BigDecimal(sResults[0].toString());
			Integer currency =Integer.parseInt(sResults[1].toString());
			
			operationBalanceTo.fillBalanceForCurrency(currency, amountBalance);

		}
		return operationBalanceTo;
	}

	/**
	 * Find balance for reporting.
	 *
	 * @return the operation balance to
	 */
	public OperationBalanceTo findBalanceForReporting(AccountingProcessTo filter){
 		
		StringBuilder stringBuilderSqlBase = new StringBuilder();
		List<Object[]>  objectList=null;
		Query query =null;
		
			stringBuilderSqlBase.append(" SELECT  SUM(HAB.REPORTING_BALANCE*  ").

								 append(" 	DECODE(se.security_class,426, ").
								 append(" 	ROUND(FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt), 4), ").
								 append(" 	ROUND(FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt), 2)) ").
								 /*append(" 	ROUND( 			decode(SE.STATE_SECURITY,131,SE.current_nominal_value, ").
								 append(" 	FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt))		  	,4), ").
								 append(" 	ROUND( 			decode(SE.STATE_SECURITY,131,se.current_nominal_value, ").
								 append(" 	FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt)) 			,2)) ").*/
								 append(" ) AS REPORTANTE, ").
								 append(" SE.CURRENCY   ").
								 append(" FROM MARKET_FACT_VIEW  HAB  ").
								 append(" INNER JOIN SECURITY SE	  ").
								 append(" ON SE.ID_SECURITY_CODE_PK=HAB.ID_SECURITY_CODE_PK   ").
								 append(" WHERE HAB.REPORTING_BALANCE>0 ").
								 append(" AND HAB.CUT_DATE >= TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD')").
								 append(" AND HAB.CUT_DATE < TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD') + 1  ").
								 append(" AND TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD') = TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY')").
								 append(" GROUP BY SE.CURRENCY ").
						 		 append("    ");

			stringBuilderSqlBase.append(" ORDER BY SE.CURRENCY ");
			
			query = em.createNativeQuery(stringBuilderSqlBase.toString());
			query.setParameter("dateCourt", filter.getExecutionDate());
		
		
		objectList = query.getResultList();
		
		OperationBalanceTo operationBalanceTo=new OperationBalanceTo();
		
		for(int i=0;i<objectList.size();++i){

			Object[] sResults = (Object[])objectList.get(i);
			BigDecimal amountBalance=new BigDecimal(sResults[0].toString());
			Integer currency =Integer.parseInt(sResults[1].toString());
			
			operationBalanceTo.fillBalanceForCurrency(currency, amountBalance);

		}
		return operationBalanceTo;
	}
	

	/**
	 * Find balance for blocking.
	 *
	 * @return the operation balance to
	 */
	public OperationBalanceTo findBalanceForBlocking(AccountingProcessTo filter){
 		
		StringBuilder stringBuilderSqlBase = new StringBuilder();
		List<Object[]>  objectList=null;
		Query query =null;
		
			stringBuilderSqlBase.append(" SELECT  SUM(( HAB.PAWN_BALANCE + HAB.RESERVE_BALANCE + ").
								 append(" HAB.ACCREDITATION_BALANCE)*  ").
								 append(" 	DECODE(se.security_class,426, ").
								 append(" 	ROUND(FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt), 4), ").
								 append(" 	ROUND(FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt), 2)) ").
								 /*append(" 	ROUND( 			decode(SE.STATE_SECURITY,131,SE.current_nominal_value, ").
								 append(" 	FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt))		  	,4), ").
								 append(" 	ROUND( 			decode(SE.STATE_SECURITY,131,se.current_nominal_value, ").
								 append(" 	FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt)) 			,2)) ").*/
								 append(" ) AS BLOQUEOS, ").
								 append(" SE.CURRENCY   ").
								 append(" FROM MARKET_FACT_VIEW  HAB  ").
								 append(" INNER JOIN SECURITY SE	  ").
								 append(" ON SE.ID_SECURITY_CODE_PK=HAB.ID_SECURITY_CODE_PK   ").
								 append(" WHERE  (HAB.PAWN_BALANCE + HAB.RESERVE_BALANCE + HAB.ACCREDITATION_BALANCE) >0 ").
								 append(" AND HAB.CUT_DATE >= TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD')").
								 append(" AND HAB.CUT_DATE < TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD') + 1  ").
								 append(" AND TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD') = TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY')").
								 append(" GROUP BY SE.CURRENCY ").
						 		 append("    ");
					
					stringBuilderSqlBase.append(" ORDER BY SE.CURRENCY ");
					
			query = em.createNativeQuery(stringBuilderSqlBase.toString());	
			query.setParameter("dateCourt", filter.getExecutionDate());

		
		
		objectList = query.getResultList();
		
		OperationBalanceTo operationBalanceTo=new OperationBalanceTo();
		
		for(int i=0;i<objectList.size();++i){

			Object[] sResults = (Object[])objectList.get(i);
			BigDecimal amountBalance=new BigDecimal(sResults[0].toString());
			Integer currency =Integer.parseInt(sResults[1].toString());
			
			operationBalanceTo.fillBalanceForCurrency(currency, amountBalance);

		}
		return operationBalanceTo;
	}
	

	/**
	 * *.
	 *
	 * @return the operation balance to
	 */
	public OperationBalanceTo findBalanceForOtherBlock(AccountingProcessTo filter){
 		
		StringBuilder stringBuilderSqlBase = new StringBuilder();
		List<Object[]>  objectList=null;
		Query query =null;
		
			stringBuilderSqlBase.append(" SELECT  SUM(( HAB.BAN_BALANCE + HAB.OTHER_BLOCK_BALANCE + ").
								 append(" HAB.OPPOSITION_BALANCE)*   ").
								 append(" 	DECODE(se.security_class,426, ").
								 append(" 	ROUND(FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt), 4), ").
								 append(" 	ROUND(FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt), 2)) ").
								/* append(" 	ROUND( 			decode(SE.STATE_SECURITY,131,SE.current_nominal_value, ").
								 append(" 	FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt))		  	,4), ").
								 append(" 	ROUND( 			decode(SE.STATE_SECURITY,131,se.current_nominal_value, ").
								 append(" 	FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt)) 			,2)) ").*/
								 append(" ) AS BLOQUEOS_OTROS, ").
								 append(" SE.CURRENCY   ").
								 append(" FROM MARKET_FACT_VIEW  HAB  ").
								 append(" INNER JOIN SECURITY SE	  ").
								 append(" ON SE.ID_SECURITY_CODE_PK=HAB.ID_SECURITY_CODE_PK   ").
								 append(" WHERE  (HAB.BAN_BALANCE + HAB.OTHER_BLOCK_BALANCE + HAB.OPPOSITION_BALANCE ) >0 ").
								 append(" AND HAB.CUT_DATE >= TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD')").
								 append(" AND HAB.CUT_DATE < TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD') + 1  ").
								 append(" AND TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD') = TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY')").
								 append(" GROUP BY SE.CURRENCY ").
						 		 append("    ");
					
			stringBuilderSqlBase.append(" ORDER BY SE.CURRENCY ");
			
			query = em.createNativeQuery(stringBuilderSqlBase.toString());
			query.setParameter("dateCourt", filter.getExecutionDate());


		
		objectList = query.getResultList();

		OperationBalanceTo operationBalanceTo=new OperationBalanceTo();
		
		for(int i=0;i<objectList.size();++i){

			Object[] sResults = (Object[])objectList.get(i);
			BigDecimal amountBalance=new BigDecimal(sResults[0].toString());
			Integer currency =Integer.parseInt(sResults[1].toString());
			
			operationBalanceTo.fillBalanceForCurrency(currency, amountBalance);

		}
		return operationBalanceTo;
	}
	

	/**
	 * Find balance for physical balance.
	 *
	 * @return the operation balance to
	 */
	public OperationBalanceTo findBalanceForPhysicalBalance(AccountingProcessTo filter){
 		
		StringBuilder stringBuilderSqlBase = new StringBuilder();
		List<Object[]>  objectList=null;
		Query query =null;
		
			Map<String,Object> parameters = new HashMap<String,Object>();	
			/*stringBuilderSqlBase.append(" SELECT  SUM(PCE.CERTIFICATE_QUANTITY*SE.CURRENT_NOMINAL_VALUE) AS FISICO, ").
								 append(" SE.CURRENCY   ").
								 append(" FROM PHYSICAL_CERTIFICATE  PCE  ").
								 append(" INNER JOIN SECURITY SE	  ").
								 append(" ON SE.ID_SECURITY_CODE_PK=PCE.ID_SECURITY_CODE_FK   ").
								 append(" WHERE PCE.STATE IN (:stateList)  ").
								 append("   AND PCE.SITUATION IN (:situationList)").
								 append("   ").
								 append(" GROUP BY SE.CURRENCY ").
						 		 append("    ");
			
			stringBuilderSqlBase.append(" ORDER BY SE.CURRENCY ");*/
			
			
			
			stringBuilderSqlBase.append(" SELECT  SUM(PCE.AVAILABLE_BALANCE*  ").
			 append(" 	DECODE(se.security_class,426, ").
			 append(" 	  ROUND(   NVH.NOMINAL_VALUE                    ,4),").
			 append(" 	  ROUND(    NVH.NOMINAL_VALUE                    ,2)) ").
			 append(" ) AS FISICO, ").
			 append(" SE.CURRENCY   ").
			 append(" FROM HOLDER_ACCOUNT_PSY_HISTORY  PCE  ").
			 append(" INNER JOIN SECURITY SE	  ").
			 append(" ON SE.ID_SECURITY_CODE_PK=PCE.ID_SECURITY_CODE_FK   ").
			 append(" LEFT JOIN NOMINAL_VALUE_HISTORY_VIEW NVH 	  ").
			 append(" ON (NVH.CUT_DATE = TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY')  	  ").
			 append(" AND SE.ID_SECURITY_CODE_PK = NVH.ID_SECURITY_CODE_FK   )  	  ").
			 append(" WHERE 1=1  ").
			 append(" AND PCE.PROCESS_DATE >= TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD')  ").
			 append(" AND PCE.PROCESS_DATE < TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD') + 1   ").
			 append(" AND TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD') = TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY')  ").
			 append(" GROUP BY SE.CURRENCY ").
	 		 append("    ");

			stringBuilderSqlBase.append(" ORDER BY SE.CURRENCY ");
			
			query = em.createNativeQuery(stringBuilderSqlBase.toString());
			query.setParameter("dateCourt", filter.getExecutionDate());
			//--(601,602,605)   
			//query.setParameter("situationList", Arrays.asList(SituationType.CUSTODY_VAULT.getCode(), SituationType.DEPOSITARY_OPERATIONS.getCode(),SituationType.DEPOSITARY_VAULT.getCode()));
			//--(611,608,609) 
			//query.setParameter("stateList", Arrays.asList(PhysicalCertificateStateType.CONFIRMED.getCode(), PhysicalCertificateStateType.AUTHORIZED.getCode(),StateType.DEPOSITED.getCode()));
	
		
		objectList = query.getResultList();
		OperationBalanceTo operationBalanceTo=new OperationBalanceTo();
		
		for(int i=0;i<objectList.size();++i){

			Object[] sResults = (Object[])objectList.get(i);
			BigDecimal amountBalance=new BigDecimal(sResults[0].toString());
			Integer currency =Integer.parseInt(sResults[1].toString());
			
			operationBalanceTo.fillBalanceForCurrency(currency, amountBalance);

		}
		return operationBalanceTo;
	}

	/**
	 * Find balance for not placed.
	 *
	 * @return the operation balance to
	 */
	public OperationBalanceTo findBalanceForNotPlaced(AccountingProcessTo filter){
 		
		StringBuilder stringBuilderSqlBase = new StringBuilder();
		List<Object[]>  objectList=null;
		Query query =null;
		
			stringBuilderSqlBase.append(" SELECT  SUM(T.NOT_PLACED), T.CURRENCY   ").
								 append("   FROM ( ").
								 append("    SELECT ").
								 append("    PSD.PLACED_AMOUNT - NVL(PSD.REQUEST_AMOUNT,0) ").
					//			 append("    	PSD.PLACED_AMOUNT-NVL(PSD.REQUEST_BALANCE,0)* ").
					//			 append(" 			DECODE(se.security_class,426, ").
					//			 append(" 			ROUND( 			decode(SE.STATE_SECURITY,131,SE.current_nominal_value, ").
					//			 append(" 			FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt))		  	,4), ").
					//			 append(" 			ROUND( 			decode(SE.STATE_SECURITY,131,se.current_nominal_value, ").
					//			 append(" 			FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt)) 			,2)) ").
								 append(" 		 AS NOT_PLACED, ").
								 append("    	ISS.ID_ISSUANCE_CODE_PK AS ISSUANCE_CODE, ").
								 append("    	PSS.ID_PARTICIPANT_FK AS PARTICIPANT, ").
								 append("    	PSD.ID_SECURITY_CODE_FK AS SECURITY_CODE, ").
								 append("    	ISS.CURRENCY ").
								 append("    ").
								 append("    FROM ISSUANCE ISS, PLACEMENT_SEGMENT PS, PLACEMENT_SEGMENT_DETAIL PSD, ").
								 append("    	PLACEMENT_SEG_PARTICIPA_STRUCT PSS, SECURITY SE ").
								 append("    WHERE ISS.ID_ISSUANCE_CODE_PK=PS.ID_ISSUANCE_CODE_FK ").
								 append("    	AND PS.ID_PLACEMENT_SEGMENT_PK=PSD.ID_PLACEMENT_SEGMENT_FK ").
								 append("    	AND PS.ID_PLACEMENT_SEGMENT_PK=PSS.ID_PLACEMENT_SEGMENT_FK ").
								 append("    	AND SE.ID_SECURITY_CODE_PK=PSD.ID_SECURITY_CODE_FK ").
								 append("    	AND PSD.PENDING_ANNOTATION=0 ").
								 append("  		AND SE.CURRENT_NOMINAL_VALUE > 0 ").
								 append("    ORDER BY PSS.ID_PARTICIPANT_FK     ").
								 append("    )T GROUP BY T.CURRENCY HAVING SUM(T.NOT_PLACED) > 0 ").
								 append("    " ).
						 		 append("    ");
					
					
					query = em.createNativeQuery(stringBuilderSqlBase.toString());
					//query.setParameter("dateCourt", CommonsUtilities.currentDate());
		


		objectList = query.getResultList();
		
		OperationBalanceTo operationBalanceTo=new OperationBalanceTo();
		
		for(int i=0;i<objectList.size();++i){

			Object[] sResults = (Object[])objectList.get(i);
			BigDecimal amountBalance=new BigDecimal(sResults[0].toString());
			Integer currency =Integer.parseInt(sResults[1].toString());
			
			operationBalanceTo.fillBalanceForCurrency(currency, amountBalance);

		}
		return operationBalanceTo;
	}
	
	

	
	
	/**
	 * Find balance for available.
	 *
	 * @return the operation balance to
	 */
	public OperationBalanceTo findBalanceForAvailableMonthly(AccountingProcessTo filter){
 		
		StringBuilder stringBuilderSqlBase = new StringBuilder();
		List<Object[]>  objectList=null;
		Query query =null;
		
		if(filter.getReportByProcess().equals(BooleanType.YES.getCode())){
			stringBuilderSqlBase.append(" SELECT AVAILABLE_BALANCE, CURRENCY ").
								 append("	FROM VIEW_AVAILABLE_BALANCE ").
								 append("");
			query = em.createNativeQuery(stringBuilderSqlBase.toString());
		}else{
			
			stringBuilderSqlBase.append(" SELECT  SUM((HAB.AVAILABLE_BALANCE)* ").
								 append(" 	DECODE(se.security_class,426,").
								 append(" 	ROUND(FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt), 4), ").
								 append(" 	ROUND(FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt), 2)) ").
								 append(" ) AS DISPONIBLE, ").
								 append(" SE.CURRENCY   ").
								 append(" FROM HOLDER_MARKETFACT_HISTORY  HAB  ").
								 append(" INNER JOIN SECURITY SE	  ").
								 append(" ON SE.ID_SECURITY_CODE_PK=HAB.ID_SECURITY_CODE_FK   ").
								 append(" WHERE HAB.AVAILABLE_BALANCE>0   ").
								 append(" AND HAB.PROCESS_DATE >= TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD')").
								 append(" AND HAB.PROCESS_DATE < TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD') + 1  ").
								 append(" AND TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD') = TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY')").
								 append(" GROUP BY SE.CURRENCY ").
						 		 append("    ");
			
			stringBuilderSqlBase.append(" ORDER BY SE.CURRENCY ");
			
			query = em.createNativeQuery(stringBuilderSqlBase.toString());
			query.setParameter("dateCourt", filter.getExecutionDate());
		}
		
		
		
		
		objectList = query.getResultList();
		
		OperationBalanceTo operationBalanceTo=new OperationBalanceTo();
		
		for(int i=0;i<objectList.size();++i){

			Object[] sResults = (Object[])objectList.get(i);
			BigDecimal amountBalance=new BigDecimal(sResults[0].toString());
			Integer currency =Integer.parseInt(sResults[1].toString());
			
			operationBalanceTo.fillBalanceForCurrency(currency, amountBalance);

		}
		
		
		return operationBalanceTo;
	}
	
	/**
	 * Find balance for reported.
	 *
	 * @return the operation balance to
	 */
	public OperationBalanceTo findBalanceForReportedMonthly(AccountingProcessTo filter){
 		
		StringBuilder stringBuilderSqlBase = new StringBuilder();
		List<Object[]>  objectList=null;
		Query query =null;
		
		if(filter.getReportByProcess().equals(BooleanType.YES.getCode())){
			stringBuilderSqlBase.append(" SELECT REPORTED_BALANCE, CURRENCY ").
								 append("	FROM VIEW_REPORTED_BALANCE ").
								 append("");
			query = em.createNativeQuery(stringBuilderSqlBase.toString());
		}else{
			stringBuilderSqlBase.append(" SELECT  SUM(HAB.REPORTED_BALANCE* ").
								 append(" 	DECODE(se.security_class,426, ").
								 append(" 	ROUND(FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt), 4), ").
								 append(" 	ROUND(FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt), 2)) ").
								 append(" ) AS REPORTADO, ").
								 append(" SE.CURRENCY   ").
								 append(" FROM HOLDER_MARKETFACT_HISTORY  HAB  ").
								 append(" INNER JOIN SECURITY SE	  ").
								 append(" ON SE.ID_SECURITY_CODE_PK=HAB.ID_SECURITY_CODE_FK   ").
								 append(" WHERE HAB.REPORTED_BALANCE>0    ").
								 append(" AND HAB.PROCESS_DATE >= TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD')").
								 append(" AND HAB.PROCESS_DATE < TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD') + 1  ").
								 append(" AND TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD') = TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY')").
								 append(" GROUP BY SE.CURRENCY ").
						 		 append("    ");

			stringBuilderSqlBase.append(" ORDER BY SE.CURRENCY ");
			
			query = em.createNativeQuery(stringBuilderSqlBase.toString());
			query.setParameter("dateCourt", filter.getExecutionDate());
		}
		
		objectList = query.getResultList();
		OperationBalanceTo operationBalanceTo=new OperationBalanceTo();
		
		for(int i=0;i<objectList.size();++i){

			Object[] sResults = (Object[])objectList.get(i);
			BigDecimal amountBalance=new BigDecimal(sResults[0].toString());
			Integer currency =Integer.parseInt(sResults[1].toString());
			
			operationBalanceTo.fillBalanceForCurrency(currency, amountBalance);

		}
		return operationBalanceTo;
	}

	/**
	 * Find balance for reporting.
	 *
	 * @return the operation balance to
	 */
	public OperationBalanceTo findBalanceForReportingMonthly(AccountingProcessTo filter){
 		
		StringBuilder stringBuilderSqlBase = new StringBuilder();
		List<Object[]>  objectList=null;
		Query query =null;
		
		if(filter.getReportByProcess().equals(BooleanType.YES.getCode())){
			stringBuilderSqlBase.append(" SELECT REPORTING_BALANCE, CURRENCY ").
								 append("	FROM VIEW_REPORTING_BALANCE ").
								 append("");
			query = em.createNativeQuery(stringBuilderSqlBase.toString());
		}else{
			stringBuilderSqlBase.append(" SELECT  SUM(HAB.REPORTING_BALANCE*  ").

								 append(" 	DECODE(se.security_class,426, ").
								 append(" 	ROUND(FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt), 4), ").
								 append(" 	ROUND(FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt), 2)) ").
								 append(" ) AS REPORTANTE, ").
								 append(" SE.CURRENCY   ").
								 append(" FROM HOLDER_MARKETFACT_HISTORY  HAB  ").
								 append(" INNER JOIN SECURITY SE	  ").
								 append(" ON SE.ID_SECURITY_CODE_PK=HAB.ID_SECURITY_CODE_FK   ").
								 append(" WHERE HAB.REPORTING_BALANCE>0  ").
								 append(" AND HAB.PROCESS_DATE >= TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD')").
								 append(" AND HAB.PROCESS_DATE < TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD') + 1  ").
								 append(" AND TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD') = TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY')").
								 append(" GROUP BY SE.CURRENCY ").
						 		 append("    ");

			stringBuilderSqlBase.append(" ORDER BY SE.CURRENCY ");
			
			query = em.createNativeQuery(stringBuilderSqlBase.toString());
			query.setParameter("dateCourt", filter.getExecutionDate());
		}
		
		
		objectList = query.getResultList();
		
		OperationBalanceTo operationBalanceTo=new OperationBalanceTo();
		
		for(int i=0;i<objectList.size();++i){

			Object[] sResults = (Object[])objectList.get(i);
			BigDecimal amountBalance=new BigDecimal(sResults[0].toString());
			Integer currency =Integer.parseInt(sResults[1].toString());
			
			operationBalanceTo.fillBalanceForCurrency(currency, amountBalance);

		}
		return operationBalanceTo;
	}
	

	/**
	 * Find balance for blocking.
	 *
	 * @return the operation balance to
	 */
	public OperationBalanceTo findBalanceForBlockingMonthly(AccountingProcessTo filter){
 		
		StringBuilder stringBuilderSqlBase = new StringBuilder();
		List<Object[]>  objectList=null;
		Query query =null;
		
		if(filter.getReportByProcess().equals(BooleanType.YES.getCode())){
			stringBuilderSqlBase.append(" SELECT BLOCK_BALANCE, CURRENCY ").
								 append("	FROM VIEW_BLOCK_BALANCE ").
								 append("");
			query = em.createNativeQuery(stringBuilderSqlBase.toString());
		}else{
			stringBuilderSqlBase.append(" SELECT  SUM(( HAB.PAWN_BALANCE + HAB.RESERVE_BALANCE + ").
								 append(" HAB.ACCREDITATION_BALANCE)*  ").
								 append(" 	DECODE(se.security_class,426, ").
								 append(" 	ROUND(FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt),4), ").
								 append(" 	ROUND(FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt),2)) ").
								 append(" ) AS BLOQUEOS, ").
								 append(" SE.CURRENCY   ").
								 append(" FROM HOLDER_MARKETFACT_HISTORY  HAB  ").
								 append(" INNER JOIN SECURITY SE	  ").
								 append(" ON SE.ID_SECURITY_CODE_PK=HAB.ID_SECURITY_CODE_FK   ").
								 append(" WHERE (HAB.PAWN_BALANCE + HAB.RESERVE_BALANCE + HAB.ACCREDITATION_BALANCE) >0  ").
								 append(" AND HAB.PROCESS_DATE >= TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD')").
								 append(" AND HAB.PROCESS_DATE < TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD') + 1  ").
								 append(" AND TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD') = TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY')").
								 append(" GROUP BY SE.CURRENCY ").
						 		 append("    ");
					
					stringBuilderSqlBase.append(" ORDER BY SE.CURRENCY ");
					
			query = em.createNativeQuery(stringBuilderSqlBase.toString());	
			query.setParameter("dateCourt", filter.getExecutionDate());
		}

		
		
		objectList = query.getResultList();
		
		OperationBalanceTo operationBalanceTo=new OperationBalanceTo();
		
		for(int i=0;i<objectList.size();++i){

			Object[] sResults = (Object[])objectList.get(i);
			BigDecimal amountBalance=new BigDecimal(sResults[0].toString());
			Integer currency =Integer.parseInt(sResults[1].toString());
			
			operationBalanceTo.fillBalanceForCurrency(currency, amountBalance);

		}
		return operationBalanceTo;
	}
	

	/**
	 * *.
	 *
	 * @return the operation balance to
	 */
	public OperationBalanceTo findBalanceForOtherBlockMonthly(AccountingProcessTo filter){
 		
		StringBuilder stringBuilderSqlBase = new StringBuilder();
		List<Object[]>  objectList=null;
		Query query =null;
		
		if(filter.getReportByProcess().equals(BooleanType.YES.getCode())){
			stringBuilderSqlBase.append(" SELECT OTHER_BLOCK_BALANCE, CURRENCY ").
								 append("	FROM VIEW_OTHER_BLOCK_BALANCE ").
								 append("");
			query = em.createNativeQuery(stringBuilderSqlBase.toString());
		}else{
			stringBuilderSqlBase.append(" SELECT  SUM(( HAB.BAN_BALANCE + HAB.OTHER_BLOCK_BALANCE + ").
								 append(" HAB.OPPOSITION_BALANCE)*   ").
								 append(" 	DECODE(se.security_class,426, ").
								 append(" 	ROUND(FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt),4), ").
								 append(" 	ROUND(FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , :dateCourt),2)) ").
								 append(" ) AS BLOQUEOS_OTROS, ").
								 append(" SE.CURRENCY   ").
								 append(" FROM HOLDER_MARKETFACT_HISTORY  HAB  ").
								 append(" INNER JOIN SECURITY SE	  ").
								 append(" ON SE.ID_SECURITY_CODE_PK=HAB.ID_SECURITY_CODE_FK   ").
								 append(" WHERE  (HAB.BAN_BALANCE + HAB.OTHER_BLOCK_BALANCE + HAB.OPPOSITION_BALANCE ) >0 ").
								 append(" AND HAB.PROCESS_DATE >= TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD')").
								 append(" AND HAB.PROCESS_DATE < TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD') + 1  ").
								 append(" AND TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD') = TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY')").
								 append(" GROUP BY SE.CURRENCY ").
						 		 append("    ");
					
			stringBuilderSqlBase.append(" ORDER BY SE.CURRENCY ");
			
			query = em.createNativeQuery(stringBuilderSqlBase.toString());
			query.setParameter("dateCourt", filter.getExecutionDate());

		}

		
		objectList = query.getResultList();

		OperationBalanceTo operationBalanceTo=new OperationBalanceTo();
		
		for(int i=0;i<objectList.size();++i){

			Object[] sResults = (Object[])objectList.get(i);
			BigDecimal amountBalance=new BigDecimal(sResults[0].toString());
			Integer currency =Integer.parseInt(sResults[1].toString());
			
			operationBalanceTo.fillBalanceForCurrency(currency, amountBalance);

		}
		return operationBalanceTo;
	}
	

	/**
	 * Find balance for physical balance.
	 *
	 * @return the operation balance to
	 */
	public OperationBalanceTo findBalanceForPhysicalBalanceMonthly(AccountingProcessTo filter){
 		
		StringBuilder stringBuilderSqlBase = new StringBuilder();
		List<Object[]>  objectList=null;
		Query query =null;
		
		if(filter.getReportByProcess().equals(BooleanType.YES.getCode())){
			stringBuilderSqlBase.append(" SELECT PHYSICAL_BALANCE, CURRENCY ").
								 append("	FROM VIEW_PHYSICAL_BALANCE ").
								 append("");
			query = em.createNativeQuery(stringBuilderSqlBase.toString());
		}else{
			Map<String,Object> parameters = new HashMap<String,Object>();	
			stringBuilderSqlBase.append(" SELECT  SUM(PCE.AVAILABLE_BALANCE*  ").
								 append(" 	DECODE(se.security_class,426, ").
								 append(" 	  ROUND(   NVH.NOMINAL_VALUE                    ,4),").
								 append(" 	  ROUND(    NVH.NOMINAL_VALUE                    ,2)) ").
								 append(" ) AS FISICO, ").
								 append(" SE.CURRENCY   ").
								 append(" FROM HOLDER_ACCOUNT_PSY_HISTORY  PCE  ").
								 append(" INNER JOIN SECURITY SE	  ").
								 append(" ON SE.ID_SECURITY_CODE_PK=PCE.ID_SECURITY_CODE_FK   ").
								 append(" LEFT JOIN NOMINAL_VALUE_HISTORY_VIEW NVH 	  ").
								 append(" ON (NVH.CUT_DATE = TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY')  	  ").
								 append(" AND SE.ID_SECURITY_CODE_PK = NVH.ID_SECURITY_CODE_FK   )  	  ").
								 append(" WHERE 1=1  ").
								 append(" AND PCE.PROCESS_DATE >= TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD')  ").
								 append(" AND PCE.PROCESS_DATE < TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD') + 1   ").
								 append(" AND TRUNC(TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY'), 'DD') = TO_DATE(TO_CHAR(:dateCourt, 'DD/MM/YYYY'), 'DD/MM/YYYY')  ").
								 append(" GROUP BY SE.CURRENCY ").
						 		 append("    ");
			
			stringBuilderSqlBase.append(" ORDER BY SE.CURRENCY ");
			
			query = em.createNativeQuery(stringBuilderSqlBase.toString());
			query.setParameter("dateCourt", filter.getExecutionDate());

	
		}
		
		objectList = query.getResultList();
		OperationBalanceTo operationBalanceTo=new OperationBalanceTo();
		
		for(int i=0;i<objectList.size();++i){

			Object[] sResults = (Object[])objectList.get(i);
			BigDecimal amountBalance=new BigDecimal(sResults[0].toString());
			Integer currency =Integer.parseInt(sResults[1].toString());
			
			operationBalanceTo.fillBalanceForCurrency(currency, amountBalance);

		}
		return operationBalanceTo;
	}

	/**
	 * Find balance for not placed.
	 *
	 * @return the operation balance to
	 */
	public OperationBalanceTo findBalanceForNotPlacedMonthly(AccountingProcessTo filter){
 		
		StringBuilder stringBuilderSqlBase = new StringBuilder();
		List<Object[]>  objectList=null;
		Query query =null;
		
		if(filter.getReportByProcess().equals(BooleanType.YES.getCode())){
			stringBuilderSqlBase.append(" SELECT NOT_PLACED, CURRENCY ").
								 append("	FROM VIEW_NOT_PLACED ").
								 append("");
			query = em.createNativeQuery(stringBuilderSqlBase.toString());
		}else{
			stringBuilderSqlBase.append(" SELECT  SUM(NOMINAL_VALUE*MOVEMENT_QUANTITY) NOT_PLACED, ").
			 					 append(" SE.CURRENCY   ").
								 append("    ").
								 append("    FROM PLACEMENT_SEGMENT_HISTORY PSH, ").
								 append("    	SECURITY SE ").
								 append("    WHERE SE.ID_SECURITY_CODE_PK=PSH.ID_SECURITY_CODE_FK ").
								 append("    	AND TRUNC(PSH.PROCESS_DATE)=:dateCourt ").
								 append("    GROUP BY SE.CURRENCY     ").
								 append("    " ).
						 		 append("    ");
					
					
					query = em.createNativeQuery(stringBuilderSqlBase.toString());
					query.setParameter("dateCourt", filter.getExecutionDate());
		}
		


		objectList = query.getResultList();
		
		OperationBalanceTo operationBalanceTo=new OperationBalanceTo();
		
		for(int i=0;i<objectList.size();++i){

			Object[] sResults = (Object[])objectList.get(i);
			BigDecimal amountBalance=new BigDecimal(sResults[0].toString());
			Integer currency =Integer.parseInt(sResults[1].toString());
			
			operationBalanceTo.fillBalanceForCurrency(currency, amountBalance);

		}
		return operationBalanceTo;
	}
	
	

	/**
	 * Find balance for accounting.
	 *
	 * @param accountingProcess the accounting process
	 * @return the list
	 */
	public List<AccountingAccount> findBalanceForAccounting(AccountingProcess accountingProcess){
 		
	 	
		StringBuilder sBSql = new StringBuilder();
		sBSql.append(" ")
		.append(" WITH INICIO AS(                                                                       ")
		.append(" SELECT                                                                                ")
		.append("         AP.EXECUTION_DATE,                                                            ")
		.append(" 		ACC.ID_PORTFOLIO CONCILIACION,                                                  ")
		.append(" 		ACC.ACCOUNT_CODE AS CUENTA,                                                     ")
		.append(" 		DECODE(ACC.CURRENCY_TYPE, 127, ARD.AMOUNT) AS BOLIVIANOS,                       ")
		.append(" 		DECODE(ACC.CURRENCY_TYPE, 430, ARD.AMOUNT) AS DOLARES,                          ")
		.append(" 		DECODE(ACC.CURRENCY_TYPE, 1304, ARD.AMOUNT) AS EUROS,                           ")
		.append(" 		DECODE(ACC.CURRENCY_TYPE, 1734, ARD.AMOUNT) AS UFV,                             ")
		.append(" 		DECODE(ACC.CURRENCY_TYPE, 1853, ARD.AMOUNT) AS MANT_VALOR,                      ")
		.append(" 		AMD.DYNAMIC_TYPE,                                                               ")
		.append(" 		ACC.INSTRUMENT_TYPE,                                                            ")
		.append(" 		MT.MOVEMENT_NAME                                                                ")
		.append(" 	   FROM ACCOUNTING_PROCESS AP                                                       ")
		.append(" 	   INNER JOIN ACCOUNTING_RECEIPT AR                                                 ")
		.append(" 		ON AP.ID_ACCOUNTING_PROCESS_PK=AR.ID_ACCOUNTING_PROCESS_FK                      ")
		.append(" 	   INNER JOIN ACCOUNTING_RECEIPT_DETAIL ARD                                         ")
		.append(" 		ON AR.ID_ACCOUNTING_RECEIPTS_PK=ARD.ID_ACCOUNTING_RECEIPTS_FK                   ")
		.append(" 	   INNER JOIN ACCOUNTING_SOURCE_LOGGER ASL                                          ")
		.append(" 		ON ARD.ID_ACCOUNTING_RECEIPT_DET_PK=ASL.ID_ACCOUNTING_RECEIPT_DET_FK            ")
		.append(" 	   INNER JOIN  ACCOUNTING_SOURCE_DETAIL ASD                                         ")
		.append(" 		ON ASL.ID_ACCOUNTING_SOURCE_LOGGER_PK=ASD.ID_ACCOUNTING_SOURCE_LOGGER_FK        ")
		.append(" 	   INNER JOIN ACCOUNTING_MATRIX_DETAIL AMD                                          ")
		.append(" 		ON ARD.ID_ACCOUNTING_MATRIX_DETAIL_FK=AMD.ID_ACCOUNTING_MATRIX_DETAIL_PK        ")
		.append(" 	   INNER JOIN ACCOUNTING_ACCOUNT ACC                                                ")
		.append(" 		ON AMD.ID_ACCOUNTING_ACCOUNT_FK=ACC.ID_ACCOUNTING_ACCOUNT_PK                    ")
		.append(" 	   INNER JOIN ACCOUNTING_ACCOUNT ACC2                                               ")
		.append(" 		ON ACC.ID_ACCOUNTING_ACCOUNT_FK=ACC2.ID_ACCOUNTING_ACCOUNT_PK                   ")
		.append(" 	   INNER JOIN MOVEMENT_TYPE MT                                                      ")
		.append(" 		ON MT.ID_MOVEMENT_TYPE_PK=ASD.PARAMETER_VALUE                                   ")
		.append(" 	   INNER JOIN PARAMETER_TABLE PT                                                    ")
		.append(" 		ON PT.PARAMETER_TABLE_PK= MT.ACCOUNTING_SCHEMA                                  ")
		.append(" 	   WHERE ASD.PARAMETER_TYPE=:movementType                                           ")
		.append(" 			AND ACC.IND_REFERENCE=0                                                     ")
		.append(" 	   AND AP.ID_ACCOUNTING_PROCESS_PK=(                                                ")
		.append("         SELECT id_accounting_process_pk FROM (                                        ")
		.append("         SELECT * FROM accounting_process where process_type=:processType              ")
		.append("         and execution_date<=:executionDate                                            ")
		.append("         and START_TYPE=:startType order by execution_date desc                        ")
		.append("         ) WHERE rownum=1)                                                             ")
		.append("      AND AP.START_TYPE=:startType                                                     ")
		.append(" 	   AND   AP.PROCESS_TYPE = :processType                                             ")
		.append(" 	ORDER BY    ACC.INSTRUMENT_TYPE, ACC.CURRENCY_TYPE  )                               ")
		.append("  SELECT                                                                               ")
		.append("   BASE.CONCILIACION,                                                                  ")
		.append("   BASE.CUENTA,                                                                        ")
		.append("   NVL(SUM(BASE.BOLIVIANOS),0) BOLIVIANOS,                                             ")
		.append("   NVL(SUM(BASE.DOLARES),0) DOLARES,                                                   ")
		.append("   NVL(SUM(BASE.EUROS),0) EUROS,                                                       ")
		.append("   NVL(SUM(BASE.UFV),0) UFV,                                                           ")
		.append("   NVL(SUM(BASE.MANT_VALOR),0) MANT_VALOR,                                             ")
		.append("   BASE.DYNAMIC_TYPE,                                                                  ")
		.append("   BASE.INSTRUMENT_TYPE,                                                               ")
		.append("   BASE.MOVEMENT_NAME                                                                  ")
		.append("   FROM                                                                                ")
		.append(" 	(SELECT                                                                             ")
		.append(" 		ACC.ID_PORTFOLIO CONCILIACION,                                                  ")
		.append(" 		ACC.ACCOUNT_CODE AS CUENTA,                                                     ")
		.append(" 		DECODE(ACC.CURRENCY_TYPE, 127, ARD.AMOUNT) AS BOLIVIANOS,                       ")
		.append(" 		DECODE(ACC.CURRENCY_TYPE, 430, ARD.AMOUNT) AS DOLARES,                          ")
		.append(" 		DECODE(ACC.CURRENCY_TYPE, 1304, ARD.AMOUNT) AS EUROS,                           ")
		.append(" 		DECODE(ACC.CURRENCY_TYPE, 1734, ARD.AMOUNT) AS UFV,                             ")
		.append(" 		DECODE(ACC.CURRENCY_TYPE, 1853, ARD.AMOUNT) AS MANT_VALOR,                      ")
		.append(" 		AMD.DYNAMIC_TYPE,                                                               ")
		.append(" 		ACC.INSTRUMENT_TYPE,                                                            ")
		.append(" 		MT.MOVEMENT_NAME                                                                ")
		.append(" 	   FROM ACCOUNTING_PROCESS AP                                                       ")
		.append(" 	   INNER JOIN ACCOUNTING_RECEIPT AR                                                 ")
		.append(" 		ON AP.ID_ACCOUNTING_PROCESS_PK=AR.ID_ACCOUNTING_PROCESS_FK                      ")
		.append(" 	   INNER JOIN ACCOUNTING_RECEIPT_DETAIL ARD                                         ")
		.append(" 		ON AR.ID_ACCOUNTING_RECEIPTS_PK=ARD.ID_ACCOUNTING_RECEIPTS_FK                   ")
		.append(" 	   INNER JOIN ACCOUNTING_SOURCE_LOGGER ASL                                          ")
		.append(" 		ON ARD.ID_ACCOUNTING_RECEIPT_DET_PK=ASL.ID_ACCOUNTING_RECEIPT_DET_FK            ")
		.append(" 	   INNER JOIN  ACCOUNTING_SOURCE_DETAIL ASD                                         ")
		.append(" 		ON ASL.ID_ACCOUNTING_SOURCE_LOGGER_PK=ASD.ID_ACCOUNTING_SOURCE_LOGGER_FK        ")
		.append(" 	   INNER JOIN ACCOUNTING_MATRIX_DETAIL AMD                                          ")
		.append(" 		ON ARD.ID_ACCOUNTING_MATRIX_DETAIL_FK=AMD.ID_ACCOUNTING_MATRIX_DETAIL_PK        ")
		.append(" 	   INNER JOIN ACCOUNTING_ACCOUNT ACC                                                ")
		.append(" 		ON AMD.ID_ACCOUNTING_ACCOUNT_FK=ACC.ID_ACCOUNTING_ACCOUNT_PK                    ")
		.append(" 	   INNER JOIN ACCOUNTING_ACCOUNT ACC2                                               ")
		.append(" 		ON ACC.ID_ACCOUNTING_ACCOUNT_FK=ACC2.ID_ACCOUNTING_ACCOUNT_PK                   ")
		.append(" 	   INNER JOIN MOVEMENT_TYPE MT                                                      ")
		.append(" 		ON MT.ID_MOVEMENT_TYPE_PK=ASD.PARAMETER_VALUE                                   ")
		.append(" 	   INNER JOIN PARAMETER_TABLE PT                                                    ")
		.append(" 		ON PT.PARAMETER_TABLE_PK= MT.ACCOUNTING_SCHEMA                                  ")
		.append(" 	   WHERE ASD.PARAMETER_TYPE=:movementType                                           ")
		.append(" 			AND ACC.IND_REFERENCE=0                                                     ")
		.append("      AND TRUNC(AP.EXECUTION_DATE)>(SELECT DISTINCT EXECUTION_DATE FROM INICIO)        ")
		.append("      AND TRUNC(AP.EXECUTION_DATE)<=:executionDate                                     ")
		.append(" 	 AND AP.START_TYPE = :continueStartType                                             ")
		.append(" 	 AND AP.PROCESS_TYPE = :processType                                                 ")
		.append("     UNION    ALL                                                                          ")
		.append("       SELECT                                                                          ")
		.append("       INICIO.CONCILIACION,                                                            ")
		.append("       INICIO.CUENTA,                                                                  ")
		.append("       BOLIVIANOS BOLIVIANOS,                                                          ")
		.append("       DOLARES DOLARES,                                                                ")
		.append("       EUROS EUROS,                                                                    ")
		.append("       UFV UFV,                                                                        ")
		.append("       MANT_VALOR MANT_VALOR,                                                          ")
		.append("       INICIO.DYNAMIC_TYPE,                                                            ")
		.append("       INICIO.INSTRUMENT_TYPE,                                                         ")
		.append("       INICIO.MOVEMENT_NAME                                                            ")
		.append("     FROM INICIO                                                                       ")
		.append("                                                                                       ")
		.append(" 	) BASE                                                                              ")
		.append("    GROUP BY                                                                           ")
		.append(" 	BASE.CONCILIACION, BASE.CUENTA, BASE.DYNAMIC_TYPE,                                  ")
		.append(" 	BASE.INSTRUMENT_TYPE, BASE.MOVEMENT_NAME                                            ")
		.append("    ORDER BY  1,2                                                                      ");
 
		
		Query query = em.createNativeQuery(sBSql.toString());	

	    query.setParameter("movementType", AccountingFieldSourceType.MOVEMENT_TYPE.getCode());
	    query.setParameter("executionDate", accountingProcess.getExecutionDate());
	    query.setParameter("startType", AccountingStartType.START.getCode());
	    query.setParameter("continueStartType", AccountingStartType.CONTINUE.getCode());
	    query.setParameter("processType", accountingProcess.getProcessType());
		
		List<Object[]>  objectList = query.getResultList();
		List<AccountingAccount> listAccountingAccount=new ArrayList<>();
		AccountingAccount accountingAccount=null;
		
		for(int i=0;i<objectList.size();++i){
			accountingAccount=new AccountingAccount();
			Object[] sResults = (Object[])objectList.get(i);

			accountingAccount.setPortFolio(Integer.parseInt(((BigDecimal)((Object[])sResults)[0]).toString()));
			accountingAccount.setAccountCode(((Object[])sResults)[1].toString());
			accountingAccount.setAmountBOB(((BigDecimal)((Object[])sResults)[2]));
			accountingAccount.setAmountUSD(((BigDecimal)((Object[])sResults)[3]));
			accountingAccount.setAmountEU(((BigDecimal)((Object[])sResults)[4]));
			accountingAccount.setAmountUFV(((BigDecimal)((Object[])sResults)[5]));
			accountingAccount.setAmountMVL(((BigDecimal)((Object[])sResults)[6]));
			accountingAccount.setNatureType(Integer.parseInt(((BigDecimal)((Object[])sResults)[7]).toString()));
			accountingAccount.setInstrumentType(Integer.parseInt(((BigDecimal)((Object[])sResults)[8]).toString()));
			
			listAccountingAccount.add(accountingAccount);

		}
		return listAccountingAccount;
	}
	
	/**
	 * Find balance for accounting.
	 *
	 * @param accountingProcess the accounting process
	 * @return the list
	 */
	public List<AccountingAccount> findBalanceForAccountingStart(AccountingProcess accountingProcess){
 		
	 	
		StringBuilder sBSql = new StringBuilder();
		sBSql.append(" SELECT   ").
		 append("  BASE2.CONCILIACION, ").//0
		 append("  BASE2.CUENTA, ").//1
		 append("  NVL(SUM(BASE2.BOLIVIANOS),0) BOLIVIANOS,  ").//2
		 append("  NVL(SUM(BASE2.DOLARES),0) DOLARES, ").//3
		 append("  NVL(SUM(BASE2.EUROS),0) EUROS, ").//4
		 append("  NVL(SUM(BASE2.UFV),0) UFV, ").//5
		 append("  NVL(SUM(BASE2.MANT_VALOR),0) MANT_VALOR, ").//6
		 append("  BASE2.DYNAMIC_TYPE, ").//7
		 append("  BASE2.INSTRUMENT_TYPE, ").//8/
		 append("  BASE2.MOVEMENT_NAME ").//9//
		 append("  FROM ").
		 append(" (SELECT   ").
				 append("  BASE.CONCILIACION, ").//0
				 append("  BASE.CUENTA, ").//1
				 append("  NVL(SUM(BASE.BOLIVIANOS),0) BOLIVIANOS,  ").//2
				 append("  NVL(SUM(BASE.DOLARES),0) DOLARES, ").//3
				 append("  NVL(SUM(BASE.EUROS),0) EUROS, ").//4
				 append("  NVL(SUM(BASE.UFV),0) UFV, ").//5
				 append("  NVL(SUM(BASE.MANT_VALOR),0) MANT_VALOR, ").//6
				 append("  BASE.DYNAMIC_TYPE, ").//7
				 append("  BASE.INSTRUMENT_TYPE, ").//8/
				 append("  BASE.MOVEMENT_NAME ").//9//
				 append("  FROM ( ").
				 append("  SELECT  ").
				 append("  	ACC.ID_PORTFOLIO CONCILIACION,  ").
				 append("  	ACC.ACCOUNT_CODE AS CUENTA, ").
				 append("  	DECODE(ACC.CURRENCY_TYPE, 127, ARD.AMOUNT) AS BOLIVIANOS, ").
				 append("  	DECODE(ACC.CURRENCY_TYPE, 430, ARD.AMOUNT) AS DOLARES, ").
				 append("  	DECODE(ACC.CURRENCY_TYPE, 1304, ARD.AMOUNT) AS EUROS, ").
				 append("  	DECODE(ACC.CURRENCY_TYPE, 1734, ARD.AMOUNT) AS UFV, ").
				 append("  	DECODE(ACC.CURRENCY_TYPE, 1853, ARD.AMOUNT) AS MANT_VALOR, ").
				 append("  	AMD.DYNAMIC_TYPE, ").
				 append("  	ACC.INSTRUMENT_TYPE, ").
				 append("  	MT.MOVEMENT_NAME ").//
				 append("  FROM ACCOUNTING_PROCESS AP    ").
				 append("  INNER JOIN ACCOUNTING_RECEIPT AR    ").
				 append("  	ON AP.ID_ACCOUNTING_PROCESS_PK=AR.ID_ACCOUNTING_PROCESS_FK    ").
				 append("  INNER JOIN ACCOUNTING_RECEIPT_DETAIL ARD ").
				 append("  	ON AR.ID_ACCOUNTING_RECEIPTS_PK=ARD.ID_ACCOUNTING_RECEIPTS_FK     ").
				 append("  INNER JOIN ACCOUNTING_SOURCE_LOGGER ASL   ").
				 append("  	ON ARD.ID_ACCOUNTING_RECEIPT_DET_PK=ASL.ID_ACCOUNTING_RECEIPT_DET_FK   ").
				 append("  INNER JOIN  ACCOUNTING_SOURCE_DETAIL ASD ").
				 append("  	ON ASL.ID_ACCOUNTING_SOURCE_LOGGER_PK=ASD.ID_ACCOUNTING_SOURCE_LOGGER_FK ").
				 append("  INNER JOIN ACCOUNTING_MATRIX_DETAIL AMD ").
				 append("  	ON ARD.ID_ACCOUNTING_MATRIX_DETAIL_FK=AMD.ID_ACCOUNTING_MATRIX_DETAIL_PK  ").
				 append("  INNER JOIN ACCOUNTING_ACCOUNT ACC ").
				 append("  	ON AMD.ID_ACCOUNTING_ACCOUNT_FK=ACC.ID_ACCOUNTING_ACCOUNT_PK    ").
				 append("  INNER JOIN ACCOUNTING_ACCOUNT ACC2 ").
				 append("  	ON ACC.ID_ACCOUNTING_ACCOUNT_FK=ACC2.ID_ACCOUNTING_ACCOUNT_PK  ").
				 append("  INNER JOIN MOVEMENT_TYPE MT     ").
				 append("  	ON MT.ID_MOVEMENT_TYPE_PK=ASD.PARAMETER_VALUE    ").
				 append("  INNER JOIN PARAMETER_TABLE PT   ").
				 append("  	ON PT.PARAMETER_TABLE_PK= MT.ACCOUNTING_SCHEMA    ").
				 append("  ").
				 append("  WHERE ASD.PARAMETER_TYPE=:movementType").
				 append("  		AND ACC.IND_REFERENCE=").append(GeneralConstants.ZERO_VALUE_INT).
				 append("  AND AP.ID_ACCOUNTING_PROCESS_PK= :idAccountingProcessPk").
				 append("  AND AP.START_TYPE= :startType").
				 append("  AND   AP.PROCESS_TYPE = :processType ").
				 append("  ").
				 append("  	ORDER BY    ACC.INSTRUMENT_TYPE, ACC.CURRENCY_TYPE  ").
				 append("  	 ) BASE ").
				 append("  GROUP BY ").
				 append("  	BASE.CONCILIACION, BASE.CUENTA,BASE.DYNAMIC_TYPE,").
				 append("  	BASE.INSTRUMENT_TYPE, BASE.MOVEMENT_NAME").//
				
				 append("  ) BASE2").
				 append("    ").
				 append("  GROUP BY ").
				 append("  	BASE2.CONCILIACION, BASE2.CUENTA, BASE2.DYNAMIC_TYPE,").
				 append("  	BASE2.INSTRUMENT_TYPE, BASE2.MOVEMENT_NAME").//
				 append("  ORDER BY  1,2").
				 append("    ");
 
		
		Query query = em.createNativeQuery(sBSql.toString());	

	    query.setParameter("movementType", AccountingFieldSourceType.MOVEMENT_TYPE.getCode());
	    query.setParameter("idAccountingProcessPk", accountingProcess.getIdAccountingProcessPk());
	    query.setParameter("startType", AccountingStartType.START.getCode());
	    query.setParameter("processType", accountingProcess.getProcessType());
	    
		List<Object[]>  objectList = query.getResultList();
		List<AccountingAccount> listAccountingAccount=new ArrayList<>();
		AccountingAccount accountingAccount=null;
		
		for(int i=0;i<objectList.size();++i){
			accountingAccount=new AccountingAccount();
			Object[] sResults = (Object[])objectList.get(i);

			accountingAccount.setPortFolio(Integer.parseInt(((BigDecimal)((Object[])sResults)[0]).toString()));
			accountingAccount.setAccountCode(((Object[])sResults)[1].toString());
			accountingAccount.setAmountBOB(((BigDecimal)((Object[])sResults)[2]));
			accountingAccount.setAmountUSD(((BigDecimal)((Object[])sResults)[3]));
			accountingAccount.setAmountEU(((BigDecimal)((Object[])sResults)[4]));
			accountingAccount.setAmountUFV(((BigDecimal)((Object[])sResults)[5]));
			accountingAccount.setAmountMVL(((BigDecimal)((Object[])sResults)[6]));
			accountingAccount.setNatureType(Integer.parseInt(((BigDecimal)((Object[])sResults)[7]).toString()));
			accountingAccount.setInstrumentType(Integer.parseInt(((BigDecimal)((Object[])sResults)[8]).toString()));
			
			listAccountingAccount.add(accountingAccount);

		}
		return listAccountingAccount;
	}
	
	/**
	 * Find balance for accounting.
	 *
	 * @param accountingProcess the accounting process
	 * @return the list
	 */
	public List<AccountingAccount> findBalanceForAccountingMonthly(AccountingProcess accountingProcess){
 		
	 	
		StringBuilder sBSql = new StringBuilder();
		sBSql.append(" SELECT   ").
		 append("  BASE2.CONCILIACION, ").//0
		 append("  BASE2.CUENTA, ").//1
		 append("  NVL(SUM(BASE2.BOLIVIANOS),0) BOLIVIANOS,  ").//2
		 append("  NVL(SUM(BASE2.DOLARES),0) DOLARES, ").//3
		 append("  NVL(SUM(BASE2.EUROS),0) EUROS, ").//4
		 append("  NVL(SUM(BASE2.UFV),0) UFV, ").//5
		 append("  NVL(SUM(BASE2.MANT_VALOR),0) MANT_VALOR, ").//6
		 append("  BASE2.DYNAMIC_TYPE, ").//7
		 append("  BASE2.INSTRUMENT_TYPE, ").//8/
		 append("  BASE2.MOVEMENT_NAME ").//9//
		 append("  FROM ").
		 append(" (SELECT   ").
				 append("  BASE.CONCILIACION, ").//0
				 append("  BASE.CUENTA, ").//1
				 append("  NVL(SUM(BASE.BOLIVIANOS),0) BOLIVIANOS,  ").//2
				 append("  NVL(SUM(BASE.DOLARES),0) DOLARES, ").//3
				 append("  NVL(SUM(BASE.EUROS),0) EUROS, ").//4
				 append("  NVL(SUM(BASE.UFV),0) UFV, ").//5
				 append("  NVL(SUM(BASE.MANT_VALOR),0) MANT_VALOR, ").//6
				 append("  BASE.DYNAMIC_TYPE, ").//7
				 append("  BASE.INSTRUMENT_TYPE, ").//8/
				 append("  BASE.MOVEMENT_NAME ").//9//
				 append("  FROM ( ").
				 append("  SELECT  ").
				 append("  	ACC.ID_PORTFOLIO CONCILIACION,  ").
				 append("  	ACC.ACCOUNT_CODE AS CUENTA, ").
				 append("  	DECODE(ACC.CURRENCY_TYPE, 127, AMR.AMOUNT) AS BOLIVIANOS, ").
				 append("  	DECODE(ACC.CURRENCY_TYPE, 430, AMR.AMOUNT) AS DOLARES, ").
				 append("  	DECODE(ACC.CURRENCY_TYPE, 1304, AMR.AMOUNT) AS EUROS, ").
				 append("  	DECODE(ACC.CURRENCY_TYPE, 1734, AMR.AMOUNT) AS UFV, ").
				 append("  	DECODE(ACC.CURRENCY_TYPE, 1853, AMR.AMOUNT) AS MANT_VALOR, ").
				 append("  	AMD.DYNAMIC_TYPE, ").
				 append("  	ACC.INSTRUMENT_TYPE, ").
				 append("  	MT.MOVEMENT_NAME ").//
				 append("  FROM ACCOUNTING_PROCESS AP    ").
				 append("  INNER JOIN ACCOUNTING_RECEIPT AR    ").
				 append("  	ON AP.ID_ACCOUNTING_PROCESS_PK=AR.ID_ACCOUNTING_PROCESS_FK    ").
				 append("  INNER JOIN ACCOUNTING_MONTH_RESULT AMR ").
				 append("  	ON AMR.ID_ACCOUNTING_RECEIPT_FK=AR.ID_ACCOUNTING_RECEIPTS_PK          ").
				 append("  INNER JOIN ACCOUNTING_MATRIX_DETAIL AMD ").
				 append("  	ON AMR.ID_ACCOUNTING_MATRIX_DETAIL_FK=AMD.ID_ACCOUNTING_MATRIX_DETAIL_PK  ").
				 append("  INNER JOIN ACCOUNTING_ACCOUNT ACC ").
				 append("  	ON AMD.ID_ACCOUNTING_ACCOUNT_FK=ACC.ID_ACCOUNTING_ACCOUNT_PK    ").
				 append("  INNER JOIN ACCOUNTING_ACCOUNT ACC2 ").
				 append("  	ON ACC.ID_ACCOUNTING_ACCOUNT_FK=ACC2.ID_ACCOUNTING_ACCOUNT_PK  ").
				 append("  INNER JOIN MOVEMENT_TYPE MT     ").
				 append("  	ON MT.ID_MOVEMENT_TYPE_PK=AMR.MOVEMENT_TYPE    ").
				 append("  INNER JOIN PARAMETER_TABLE PT   ").
				 append("  	ON PT.PARAMETER_TABLE_PK= MT.ACCOUNTING_SCHEMA    ").
				 append("  ").
				 append("  WHERE ACC.IND_REFERENCE=").append(GeneralConstants.ZERO_VALUE_INT).
				 append("  AND AP.ID_ACCOUNTING_PROCESS_PK= :idAccountingProcessPk").
				 append("  AND AP.START_TYPE= :startType").
				 append("  AND   AP.PROCESS_TYPE = :processType ").
				 append("  ").
				 append("  	ORDER BY    ACC.INSTRUMENT_TYPE, ACC.CURRENCY_TYPE  ").
				 append("  	 ) BASE ").
				 append("  GROUP BY ").
				 append("  	BASE.CONCILIACION, BASE.CUENTA,BASE.DYNAMIC_TYPE,").
				 append("  	BASE.INSTRUMENT_TYPE, BASE.MOVEMENT_NAME").//
				
				 append("  ) BASE2").
				 append("    ").
				 append("  GROUP BY ").
				 append("  	BASE2.CONCILIACION, BASE2.CUENTA, BASE2.DYNAMIC_TYPE,").
				 append("  	BASE2.INSTRUMENT_TYPE, BASE2.MOVEMENT_NAME").//
				 append("  ORDER BY  1,2").
				 append("    ");
 
		
		Query query = em.createNativeQuery(sBSql.toString());	

	    query.setParameter("idAccountingProcessPk", accountingProcess.getIdAccountingProcessPk());
	    query.setParameter("startType", AccountingStartType.MONTHLY.getCode());
	    query.setParameter("processType", accountingProcess.getProcessType());
	    
		List<Object[]>  objectList = query.getResultList();
		List<AccountingAccount> listAccountingAccount=new ArrayList<>();
		AccountingAccount accountingAccount=null;
		
		for(int i=0;i<objectList.size();++i){
			accountingAccount=new AccountingAccount();
			Object[] sResults = (Object[])objectList.get(i);

			accountingAccount.setPortFolio(Integer.parseInt(((BigDecimal)((Object[])sResults)[0]).toString()));
			accountingAccount.setAccountCode(((Object[])sResults)[1].toString());
			accountingAccount.setAmountBOB(((BigDecimal)((Object[])sResults)[2]));
			accountingAccount.setAmountUSD(((BigDecimal)((Object[])sResults)[3]));
			accountingAccount.setAmountEU(((BigDecimal)((Object[])sResults)[4]));
			accountingAccount.setAmountUFV(((BigDecimal)((Object[])sResults)[5]));
			accountingAccount.setAmountMVL(((BigDecimal)((Object[])sResults)[6]));
			accountingAccount.setNatureType(Integer.parseInt(((BigDecimal)((Object[])sResults)[7]).toString()));
			accountingAccount.setInstrumentType(Integer.parseInt(((BigDecimal)((Object[])sResults)[8]).toString()));
			
			listAccountingAccount.add(accountingAccount);

		}
		return listAccountingAccount;
	}
	
	/**
	 * List of the Process with Receipt.
	 *
	 * @param filter the filter
	 * @return the list
	 */
	public List<AccountingProcess> findAccountingProcess(AccountingProcessTo filter){

		
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" select distinct acp from AccountingProcess acp ").
				append("   left join fetch   acp.accountingReceipt  acr ").
				append("  ");

		sbQuery.append("  WHERE   ");
		sbQuery.append("   acr.status!= :statusProcess");
		if(Validations.validateIsNotNullAndPositive(filter.getProcessType())){
			
			 sbQuery.append(" AND acp.processType= :processTypePrm ");
		}
		
		if(Validations.validateIsNotNull(filter.getInitialDate())&&Validations.validateIsNotNull(filter.getFinalDate())){
		    sbQuery.append(" and TRUNC(acp.executionDate, 'DD') between :initialDatePrm and :finalDatePrm ");
	    }
		
		if(Validations.validateIsNotNull(filter.getExecutionDate())){
			sbQuery.append(" and TRUNC(acp.executionDate) = TRUNC(:executionDatePrm)  ");
		}
    	Query query = em.createQuery(sbQuery.toString());
    	
    	query.setParameter("statusProcess", AccountingReceiptStateType.ERRADO.getCode());
    	
    	if(Validations.validateIsNotNullAndPositive(filter.getProcessType())){
		    query.setParameter("processTypePrm", filter.getProcessType());
		}
    	if(Validations.validateIsNotNull(filter.getInitialDate())&&Validations.validateIsNotNull(filter.getFinalDate())){
		    query.setParameter("initialDatePrm", filter.getInitialDate());
		    query.setParameter("finalDatePrm", filter.getFinalDate());
	    }
    	if(Validations.validateIsNotNull(filter.getExecutionDate())){
    		query.setParameter("executionDatePrm", filter.getExecutionDate());
    	}
		
		
    	return  ((List<AccountingProcess>)query.getResultList());
    	
	}
	
	/**
	 * Find acc receip detail by pk.
	 *
	 * @param listReceiptPk the list receipt pk
	 * @return the list
	 */
	public List<AccountingReceiptDetail> findAccReceipDetailByPk(List<Long> listReceiptPk){

		
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" select distinct acrd from AccountingReceiptDetail acrd ").
				append("  left join  fetch acrd.accountingSourceLogger  asl ").
				append("  left join  fetch acrd.accountingMatrixDetail  amd ").
				append("  left outer  join amd.accountingMatrix  acm ").
				append("  left join  fetch amd.accountingAccount  aca ").
				append("  left outer  join acm.accountingSchema  acsh ").
				append("  ");

		sbQuery.append("  WHERE   ");
		
		if(Validations.validateListIsNotNullAndNotEmpty(listReceiptPk)){
			
			 sbQuery.append("  acrd.accountingReceipt.idAccountingReceiptsPk in(:listReceiptPk)  ");
		}
		
    	Query query = em.createQuery(sbQuery.toString());
    	
    	if(Validations.validateListIsNotNullAndNotEmpty(listReceiptPk)){

		    query.setParameter("listReceiptPk", listReceiptPk);
		}
		
		
    	return  ((List<AccountingReceiptDetail>)query.getResultList());
    	
	}
	
	/**
	 * Find acc source detail.
	 *
	 * @param listLoggerPk the list logger pk
	 * @return the list
	 */
	public List<AccountingSourceDetail> findAccSourceDetail(List<Long> listLoggerPk){

		
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" select  new AccountingSourceDetail( asd.idAccountingSourceDetailPk, ").
				append("  asd.accountingSourceLogger.idAccountingSourceLoggerPk, ").
				append("  asd.parameterName, ").
				append("  asd.parameterValue, ").
				append("  asd.parameterType )").
				append("  From AccountingSourceDetail asd   ").
				append(" left outer join asd.accountingSourceLogger asl");

		sbQuery.append("  WHERE   1=1 ");
		
		if(Validations.validateListIsNotNullAndNotEmpty(listLoggerPk)){
			
			 sbQuery.append(" and asd.accountingSourceLogger.idAccountingSourceLoggerPk in(:listLoggerPk)  ");
		}
		//sbQuery.append(" and asd.parameterType="+AccountingFieldSourceType.PARTICIPANT.getCode());
    	Query query = em.createQuery(sbQuery.toString());
    	
    	if(Validations.validateListIsNotNullAndNotEmpty(listLoggerPk)){

		    query.setParameter("listLoggerPk", listLoggerPk);
		}
		
		
    	return  ((List<AccountingSourceDetail>)query.getResultList());
    	
	}

	/**
	 * Find acc source detail by pk.
	 *
	 * @param listLoggerPk the list logger pk
	 * @return the list
	 */
	public List<AccountingSourceDetail> findAccSourceDetailByPk(List<Long> listLoggerPk){

		
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" select distinct asd from AccountingSourceDetail asd ").
				append("  ");

		sbQuery.append("  WHERE 1=1  ");
		
		if(Validations.validateListIsNotNullAndNotEmpty(listLoggerPk)){
			
			 sbQuery.append(" and asd.accountingSourceLogger.idAccountingSourceLoggerPk in(:listLoggerPk)  ");
		}
		
		//sbQuery.append(" and asd.parameterType="+AccountingFieldSourceType.PARTICIPANT.getCode());
		
    	Query query = em.createQuery(sbQuery.toString());
    	
    	if(Validations.validateListIsNotNullAndNotEmpty(listLoggerPk)){

		    query.setParameter("listLoggerPk", listLoggerPk);
		}
		
		
    	return  ((List<AccountingSourceDetail>)query.getResultList());
    	
	}
	
	/**
	 * To search ChangeTypeSession in DailyExchangerates .
	 *
	 * @param dailyExchangerate the daily exchangerate
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<DailyExchangeRates> searchExchangeMoneyTypeRandomListService(DailyExchangeRateFilter dailyExchangerate)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		List<DailyExchangeRates> dailyExchangeList = null;
		sbQuery.append(" select d From DailyExchangeRates d  Where");
		sbQuery.append(" d.idSourceinformation = :source ");
		if (Validations.validateIsNotNullAndNotEmpty(dailyExchangerate.getIdCurrency())
				&& dailyExchangerate.getIdCurrency() > 0) {
		sbQuery.append("and d.idCurrency = :currencyType");
		}
		if (Validations.validateIsNotNullAndNotEmpty(dailyExchangerate.getDateRate())) {
		sbQuery.append(" and d.dateRate = :creationDate");
		}
		if(Validations.validateIsNotNullAndNotEmpty(dailyExchangerate.getInitialDate())){
			sbQuery.append(" And d.dateRate >= :initialDate");
		}
		if(Validations.validateIsNotNullAndNotEmpty(dailyExchangerate.getFinalDate())){
			sbQuery.append(" And d.dateRate <= :finalDate");
		}
		sbQuery.append(" Order By d.iddailyExchangepk");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("source",dailyExchangerate.getIdSourceinformation());	
		if (Validations.validateIsNotNullAndNotEmpty(dailyExchangerate.getIdCurrency())
				&& dailyExchangerate.getIdCurrency() > 0) {
		query.setParameter("currencyType",dailyExchangerate.getIdCurrency());
		}
		if (Validations.validateIsNotNullAndNotEmpty(dailyExchangerate.getDateRate())) {
		query.setParameter("creationDate",dailyExchangerate.getDateRate());
		}
		if(Validations.validateIsNotNullAndNotEmpty(dailyExchangerate.getInitialDate())){
			query.setParameter("initialDate", dailyExchangerate.getInitialDate());
		}
		if(Validations.validateIsNotNullAndNotEmpty(dailyExchangerate.getFinalDate())){
			query.setParameter("finalDate", dailyExchangerate.getFinalDate());
		}
		
		dailyExchangeList = query.getResultList();
		
		return dailyExchangeList;
	}
	
	
	/**
	 * Find accounting vouchers.
	 *
	 * @param accountingProcessTo the accounting process to
	 * @return the list
	 */
	public List<Object[]> findAccountingVouchers(AccountingProcessTo accountingProcessTo){

		
		StringBuilder sbQuery=new StringBuilder();
		 /**PK the entity*/
		sbQuery.append(" SELECT ").
		        append(" FILEX.TIPO, ").								//0
		        append(" DECODE(FILEX.IND_REFERENCE,0,FILEX.CUENTA, ").	
		        append(" SUBSTR(FILEX.CUENTA,0,14)) AS CUENTA, ").		//1
		        append(" FILEX.MONTO, ").								//2
		        append(" FILEX.OPERACION, ").							//3
		        append(" FILEX.INSTRUMENT_TYPE, ").						//4	
		        append(" FILEX.CURRENCY_TYPE, ").						//5
		        append(" FILEX.EXECUTION_DATE, ").						//6
		        append(" FILEX.BUY_PRICE, ").							//7
		        append(" FILEX.MOVEMENT_NAME, ").						//8
		        append(" FILEX.PARAMETER_NAME ").						//9
		        append(" FROM ( ");
		
		sbQuery.append(" SELECT ").
				append(" 	BASE.TIPO AS TIPO, ").				//0
				append(" 	BASE.CUENTA, ").			//1
				append(" 	SUM(BASE.MONTO) MONTO,  ").		//2
				append(" 	BASE.OPERACION, ").			//3
				append(" 	BASE.INSTRUMENT_TYPE, ").	//4
				append(" 	BASE.CURRENCY_TYPE, ").		//5
				append(" 	BASE.EXECUTION_DATE, ").	//6
				append(" 	BASE.BUY_PRICE, ").			//7
				append(" 	BASE.MOVEMENT_NAME, ").		//8
				append(" 	BASE.PARAMETER_NAME, ").		//9
				append("    BASE.IND_REFERENCE ").
				append(" 	").
				append(" FROM ( ").
				
				append(" 	  SELECT").
				append("  		ASD.PARAMETER_VALUE AS TIPO,  ").
				append("        ACC.IND_REFERENCE,  ").
				//append(" 		DECODE (ACC.IND_REFERENCE,0,").
				//append(" 		ACC.ACCOUNT_CODE||'.'||PA.MNEMONIC, 1, ACC.ACCOUNT_CODE) AS CUENTA, ").
				append(" 		ACC.ACCOUNT_CODE||'.'||PA.MNEMONIC AS CUENTA, ").
//				append("		ACC.ACCOUNT_CODE AS CUENTA, ").
				append("		TRUNC(ARD.AMOUNT,2) AS MONTO , ").
				append("		(SELECT ACP.PARAMETER_NAME FROM ACCOUNTING_PARAMETER ACP ").
				append("		WHERE ACP.ID_ACCOUNTING_PARAMETER_PK=AMD.DYNAMIC_TYPE ) AS OPERACION, ").
				append("		ACC.INSTRUMENT_TYPE, ").
				append("		ACC.CURRENCY_TYPE, ").
				append("		TO_CHAR(AP.EXECUTION_DATE,'YYYYMMDD') AS EXECUTION_DATE , ").
				append("		DER.BUY_PRICE, ").
				append("		MT.MOVEMENT_NAME, ").
				append(" 		PT.PARAMETER_NAME").
				append(" 		").
				append("	  FROM ACCOUNTING_PROCESS AP ").
				append("	  INNER JOIN ACCOUNTING_RECEIPT AR ").
				append(" 	  	ON AP.ID_ACCOUNTING_PROCESS_PK=AR.ID_ACCOUNTING_PROCESS_FK ").
				append("	  INNER JOIN ACCOUNTING_RECEIPT_DETAIL ARD ").
				append(" 	  	ON AR.ID_ACCOUNTING_RECEIPTS_PK=ARD.ID_ACCOUNTING_RECEIPTS_FK ").
				append("	  INNER JOIN ACCOUNTING_SOURCE_LOGGER ASL ").
				append(" 	  	ON ARD.ID_ACCOUNTING_RECEIPT_DET_PK=ASL.ID_ACCOUNTING_RECEIPT_DET_FK ").
				append("	  INNER JOIN  ACCOUNTING_SOURCE_DETAIL ASD ").
				append(" 	  	ON ASL.ID_ACCOUNTING_SOURCE_LOGGER_PK=ASD.ID_ACCOUNTING_SOURCE_LOGGER_FK ").
				append("	  INNER JOIN ACCOUNTING_MATRIX_DETAIL AMD ").
				append(" 	  	ON ARD.ID_ACCOUNTING_MATRIX_DETAIL_FK=AMD.ID_ACCOUNTING_MATRIX_DETAIL_PK ").
				append("	  INNER JOIN ACCOUNTING_ACCOUNT ACC ").
				append(" 	  	ON AMD.ID_ACCOUNTING_ACCOUNT_FK=ACC.ID_ACCOUNTING_ACCOUNT_PK").
				append(" 	  INNER JOIN DAILY_EXCHANGE_RATES DER").
				append(" 		ON (DER.DATE_RATE=AP.EXECUTION_DATE AND DER.ID_CURRENCY=430) ").
				append(" 	  INNER JOIN MOVEMENT_TYPE MT ").
				append(" 		ON MT.ID_MOVEMENT_TYPE_PK=ASD.PARAMETER_VALUE ").
				append(" 	  INNER JOIN PARAMETER_TABLE PT ").
				append(" 		ON PT.PARAMETER_TABLE_PK= MT.ACCOUNTING_SCHEMA ").
				append("	  INNER JOIN  ACCOUNTING_SOURCE_DETAIL ASD2").
				append(" 		ON ASD2.ID_ACCOUNTING_SOURCE_LOGGER_FK=ASD.ID_ACCOUNTING_SOURCE_LOGGER_FK").
				append(" 	  INNER JOIN PARTICIPANT PA	").
				append(" 		ON PA.ID_PARTICIPANT_PK=ASD2.PARAMETER_VALUE ").
				append("	    ").
				append(" 		").
				append(" 		").
				append(" 		").
				append("	  ").
				append("	  WHERE ASD.PARAMETER_TYPE=:movementType ").
				append(" 		AND ASD2.PARAMETER_TYPE=:participantType ").
				append(" 		").
				append("");
	
	    
		
		
		
	    if(Validations.validateIsNotNullAndPositive(accountingProcessTo.getIdAccountingProcessPk())){
	    	sbQuery.append(" AND AP.ID_ACCOUNTING_PROCESS_PK=:processAccountingPk  ");	
	    }
	    if(Validations.validateIsNotNull(accountingProcessTo.getProcessType())){
	        
            sbQuery.append(" AND   AP.PROCESS_TYPE = :processType ");
        }
        
        if(Validations.validateIsNotNull(accountingProcessTo.getExecutionDate() )){
            
            sbQuery.append(" AND  trunc(AP.EXECUTION_DATE) = :executionDate ");
        }
	    sbQuery.append(" 	  ORDER BY  TIPO, ACC.INSTRUMENT_TYPE, ACC.CURRENCY_TYPE,OPERACION  ").
	    	    append(" )  BASE   ").
	    	    append(" GROUP BY  BASE.TIPO, BASE.CUENTA, BASE.OPERACION,   ").
	    	    append(" 	BASE.INSTRUMENT_TYPE, BASE.CURRENCY_TYPE,  ").
	    	    append(" 	BASE.EXECUTION_DATE, BASE.BUY_PRICE,").
				append(" 	BASE.MOVEMENT_NAME, BASE.PARAMETER_NAME, BASE.IND_REFERENCE ").
				append(" 	").
				append("	").
	    	    append(" ORDER BY BASE.EXECUTION_DATE, BASE.TIPO, BASE.INSTRUMENT_TYPE,   ").
	    	    append(" 	BASE.CURRENCY_TYPE,BASE.OPERACION   ").
	    	    append("    ");
	    
	    sbQuery.append(" ) FILEX ").
	            append(" ORDER BY FILEX.EXECUTION_DATE, FILEX.TIPO, FILEX.INSTRUMENT_TYPE, ").
	            append(" FILEX.CURRENCY_TYPE,FILEX.OPERACION ");
	    
	    Query query = em.createNativeQuery(sbQuery.toString());
	    
	    query.setParameter("movementType", AccountingFieldSourceType.MOVEMENT_TYPE.getCode());
	    query.setParameter("participantType", AccountingFieldSourceType.PARTICIPANT.getCode());
	    
	    
	    if(Validations.validateIsNotNullAndPositive(accountingProcessTo.getIdAccountingProcessPk())){
	    	query.setParameter("processAccountingPk", accountingProcessTo.getIdAccountingProcessPk());
	    }
	    
	    if(Validations.validateIsNotNull(accountingProcessTo.getProcessType())){
	    	query.setParameter("processType", accountingProcessTo.getProcessType());
        }
        
        if(Validations.validateIsNotNull(accountingProcessTo.getExecutionDate() )){
        	query.setParameter("executionDate", accountingProcessTo.getExecutionDate() );
        }

	    return query.getResultList();
	
	}
	
	/**
	 * Find accounting vouchers.
	 *
	 * @param accountingProcessTo the accounting process to
	 * @return the list
	 */
	public List<Object[]> findAccountingVouchers__(AccountingProcessTo accountingProcessTo){

		
		StringBuilder sbQuery=new StringBuilder();
		 /**PK the entity*/
		sbQuery.append(" SELECT").
				append(" 	BASE.TIPO, ").				//0
				append(" 	BASE.CUENTA, ").			//1
				append(" 	SUM(BASE.MONTO),  ").		//2
				append(" 	BASE.OPERACION, ").			//3
				append(" 	BASE.INSTRUMENT_TYPE, ").	//4
				append(" 	BASE.CURRENCY_TYPE, ").		//5
				append(" 	BASE.EXECUTION_DATE, ").	//6
				append(" 	BASE.BUY_PRICE, ").			//7
				append(" 	BASE.MOVEMENT_NAME, ").		//8
				append(" 	BASE.PARAMETER_NAME ").		//9
				append(" 	").
				append(" FROM ( ").
				
				append(" 	  SELECT").
				append("  		ASD.PARAMETER_VALUE AS TIPO,  ").
				//append(" 		DECODE (ACC.IND_REFERENCE,0,").
				//append(" 		ACC.ACCOUNT_CODE||'.'||PA.MNEMONIC, 1, ACC.ACCOUNT_CODE) AS CUENTA, ").
				append(" 		ACC.ACCOUNT_CODE||'.'||PA.MNEMONIC AS CUENTA, ").
//				append("		ACC.ACCOUNT_CODE AS CUENTA, ").
				append("		TRUNC(ARD.AMOUNT,2) AS MONTO , ").
				append("		(SELECT ACP.PARAMETER_NAME FROM ACCOUNTING_PARAMETER ACP ").
				append("		WHERE ACP.ID_ACCOUNTING_PARAMETER_PK=AMD.DYNAMIC_TYPE ) AS OPERACION, ").
				append("		ACC.INSTRUMENT_TYPE, ").
				append("		ACC.CURRENCY_TYPE, ").
				append("		TO_CHAR(AP.EXECUTION_DATE,'YYYYMMDD') AS EXECUTION_DATE , ").
				append("		DER.BUY_PRICE, ").
				append("		MT.MOVEMENT_NAME, ").
				append(" 		PT.PARAMETER_NAME").
				append(" 		").
				append("	  FROM ACCOUNTING_PROCESS AP ").
				append("	  INNER JOIN ACCOUNTING_RECEIPT AR ").
				append(" 	  	ON AP.ID_ACCOUNTING_PROCESS_PK=AR.ID_ACCOUNTING_PROCESS_FK ").
				append("	  INNER JOIN ACCOUNTING_RECEIPT_DETAIL ARD ").
				append(" 	  	ON AR.ID_ACCOUNTING_RECEIPTS_PK=ARD.ID_ACCOUNTING_RECEIPTS_FK ").
				append("	  INNER JOIN ACCOUNTING_SOURCE_LOGGER ASL ").
				append(" 	  	ON ARD.ID_ACCOUNTING_RECEIPT_DET_PK=ASL.ID_ACCOUNTING_RECEIPT_DET_FK ").
				append("	  INNER JOIN  ACCOUNTING_SOURCE_DETAIL ASD ").
				append(" 	  	ON ASL.ID_ACCOUNTING_SOURCE_LOGGER_PK=ASD.ID_ACCOUNTING_SOURCE_LOGGER_FK ").
				append("	  INNER JOIN ACCOUNTING_MATRIX_DETAIL AMD ").
				append(" 	  	ON ARD.ID_ACCOUNTING_MATRIX_DETAIL_FK=AMD.ID_ACCOUNTING_MATRIX_DETAIL_PK ").
				append("	  INNER JOIN ACCOUNTING_ACCOUNT ACC ").
				append(" 	  	ON AMD.ID_ACCOUNTING_ACCOUNT_FK=ACC.ID_ACCOUNTING_ACCOUNT_PK").
				append(" 	  INNER JOIN DAILY_EXCHANGE_RATES DER").
				append(" 		ON (DER.DATE_RATE=AP.EXECUTION_DATE AND DER.ID_CURRENCY=430) ").
				append(" 	  INNER JOIN MOVEMENT_TYPE MT ").
				append(" 		ON MT.ID_MOVEMENT_TYPE_PK=ASD.PARAMETER_VALUE ").
				append(" 	  INNER JOIN PARAMETER_TABLE PT ").
				append(" 		ON PT.PARAMETER_TABLE_PK= MT.ACCOUNTING_SCHEMA ").
				append("	  INNER JOIN  ACCOUNTING_SOURCE_DETAIL ASD2").
				append(" 		ON ASD2.ID_ACCOUNTING_SOURCE_LOGGER_FK=ASD.ID_ACCOUNTING_SOURCE_LOGGER_FK").
				append(" 	  INNER JOIN PARTICIPANT PA	").
				append(" 		ON PA.ID_PARTICIPANT_PK=ASD2.PARAMETER_VALUE ").
				append("	    ").
				append(" 		").
				append(" 		").
				append(" 		").
				append("	  ").
				append("	  WHERE ASD.PARAMETER_TYPE=:movementType ").
				append(" 		AND ASD2.PARAMETER_TYPE=:participantType ").
				append(" 		").
				append("");
	
	    
		
		
		
	    if(Validations.validateIsNotNullAndPositive(accountingProcessTo.getIdAccountingProcessPk())){
	    	sbQuery.append(" AND AP.ID_ACCOUNTING_PROCESS_PK=:processAccountingPk  ");	
	    }
	    if(Validations.validateIsNotNull(accountingProcessTo.getProcessType())){
	        
            sbQuery.append(" AND   AP.PROCESS_TYPE = :processType ");
        }
        
        if(Validations.validateIsNotNull(accountingProcessTo.getExecutionDate() )){
            
            sbQuery.append(" AND  trunc(AP.EXECUTION_DATE) = :executionDate ");
        }
	    sbQuery.append(" 	  ORDER BY  TIPO, ACC.INSTRUMENT_TYPE, ACC.CURRENCY_TYPE,OPERACION  ").
	    	    append(" )  BASE   ").
	    	    append(" GROUP BY  BASE.TIPO, BASE.CUENTA, BASE.OPERACION,   ").
	    	    append(" 	BASE.INSTRUMENT_TYPE, BASE.CURRENCY_TYPE,  ").
	    	    append(" 	BASE.EXECUTION_DATE, BASE.BUY_PRICE,").
				append(" 	BASE.MOVEMENT_NAME, BASE.PARAMETER_NAME ").
				append(" 	").
				append("	").
	    	    append(" ORDER BY BASE.EXECUTION_DATE, BASE.TIPO, BASE.INSTRUMENT_TYPE,   ").
	    	    append(" 	BASE.CURRENCY_TYPE,BASE.OPERACION   ").
	    	    append("    ");
	    
	    Query query = em.createNativeQuery(sbQuery.toString());
	    
	    query.setParameter("movementType", AccountingFieldSourceType.MOVEMENT_TYPE.getCode());
	    query.setParameter("participantType", AccountingFieldSourceType.PARTICIPANT.getCode());
	    
	    
	    if(Validations.validateIsNotNullAndPositive(accountingProcessTo.getIdAccountingProcessPk())){
	    	query.setParameter("processAccountingPk", accountingProcessTo.getIdAccountingProcessPk());
	    }
	    
	    if(Validations.validateIsNotNull(accountingProcessTo.getProcessType())){
	    	query.setParameter("processType", accountingProcessTo.getProcessType());
        }
        
        if(Validations.validateIsNotNull(accountingProcessTo.getExecutionDate() )){
        	query.setParameter("executionDate", accountingProcessTo.getExecutionDate() );
        }

	    return query.getResultList();
	
	}

	
	/**
	 * *.
	 *
	 * @param accountingProcess the accounting process
	 * @return the ting accounting account by balance process
	 */
	public List<AccountingAccount> gettingAccountingAccountByBalanceProcess(AccountingProcess accountingProcess){
		List<AccountingAccount> listAccountingAccount=new ArrayList<>();
		AccountingAccount accountinAccount=null;

		StringBuilder sbQuery=new StringBuilder();
		 /**PK the entity*/
		sbQuery.append(" SELECT").
				append(" 	BASE.ACCOUNT_CODE  ,   ").		//0
				append(" 	SUM(BASE.AMOUNT),  ").			//1
				append(" 	BASE.DYNAMIC_TYPE,  ").			//2
				append(" 	SUM(BASE.MOVEMENT_QUANTITY)  ").//3
				append(" FROM (  ").
				append("	SELECT ").
				append(" 	DECODE (ACC.IND_REFERENCE,0,  ").
				append(" 		ACC.ACCOUNT_CODE||'.'||PA.MNEMONIC, 1, ACC.ACCOUNT_CODE) AS ACCOUNT_CODE, ").
				append(" 	ASD2.PARAMETER_VALUE AS MOVEMENT_QUANTITY, ").
				append(" 	ARD.AMOUNT, ").
				append(" 	AMD.DYNAMIC_TYPE ").
				append(" 	").
				append("	  FROM ACCOUNTING_PROCESS AP ").
				append("	  INNER JOIN ACCOUNTING_RECEIPT AR ").
				append(" 	  	ON AP.ID_ACCOUNTING_PROCESS_PK=AR.ID_ACCOUNTING_PROCESS_FK ").
				append("	  INNER JOIN ACCOUNTING_RECEIPT_DETAIL ARD ").
				append(" 	  	ON AR.ID_ACCOUNTING_RECEIPTS_PK=ARD.ID_ACCOUNTING_RECEIPTS_FK ").
				append("	  INNER JOIN ACCOUNTING_SOURCE_LOGGER ASL ").
				append(" 	  	ON ARD.ID_ACCOUNTING_RECEIPT_DET_PK=ASL.ID_ACCOUNTING_RECEIPT_DET_FK ").
				append("	  INNER JOIN  ACCOUNTING_SOURCE_DETAIL ASD ").
				append(" 	  	ON ASL.ID_ACCOUNTING_SOURCE_LOGGER_PK=ASD.ID_ACCOUNTING_SOURCE_LOGGER_FK ").
				append("	  INNER JOIN  ACCOUNTING_SOURCE_DETAIL ASD2").
				append("	  	ON ASD2.ID_ACCOUNTING_SOURCE_LOGGER_FK=ASD.ID_ACCOUNTING_SOURCE_LOGGER_FK ").
				append("	  INNER JOIN ACCOUNTING_MATRIX_DETAIL AMD ").
				append(" 	  	ON ARD.ID_ACCOUNTING_MATRIX_DETAIL_FK=AMD.ID_ACCOUNTING_MATRIX_DETAIL_PK ").
				append("	  INNER JOIN ACCOUNTING_ACCOUNT ACC ").
				append(" 	  	ON AMD.ID_ACCOUNTING_ACCOUNT_FK=ACC.ID_ACCOUNTING_ACCOUNT_PK").
				append(" 	  INNER JOIN PARTICIPANT PA").
				append(" 		ON PA.ID_PARTICIPANT_PK=ASD.PARAMETER_VALUE ").
				append("	  ").
				append("	  ").
				append("	  WHERE ASD.PARAMETER_TYPE=:participantType ").
				append("");

		
	    if(Validations.validateIsNotNullAndPositive(accountingProcess.getIdAccountingProcessPk())){
	    	sbQuery.append(" AND AP.ID_ACCOUNTING_PROCESS_PK=:processAccountingPk  ");	
	    }


	    if(Validations.validateIsNotNull(accountingProcess.getProcessType())){
	        
            sbQuery.append(" AND   AP.PROCESS_TYPE = :processType ");
        }
        
        if(Validations.validateIsNotNull(accountingProcess.getExecutionDate() )){
            
            sbQuery.append(" AND  trunc(AP.EXECUTION_DATE) = :executionDate ");
        }
        sbQuery.append(" AND ASD2.PARAMETER_NAME='MOVEMENT_QUANTITY' ");
        
	    sbQuery.append(" )  BASE   ").
	    	    append(" GROUP BY  BASE.ACCOUNT_CODE, BASE.DYNAMIC_TYPE  ").
				append(" 	").
				append("	").
	    	    append(" ORDER BY BASE.ACCOUNT_CODE  ").
	    	    append("    ");
	    
	    Query query = em.createNativeQuery(sbQuery.toString());
	    
	    query.setParameter("participantType", AccountingFieldSourceType.PARTICIPANT.getCode());
	    
	    if(Validations.validateIsNotNullAndPositive(accountingProcess.getIdAccountingProcessPk())){
	    	
	    	query.setParameter("processAccountingPk", accountingProcess.getIdAccountingProcessPk());
	    }
	    
	    if(Validations.validateIsNotNull(accountingProcess.getProcessType())){
	    	
	    	query.setParameter("processType", accountingProcess.getProcessType());
        }
        
        if(Validations.validateIsNotNull(accountingProcess.getExecutionDate() )){
        	
        	query.setParameter("executionDate", accountingProcess.getExecutionDate() );
        }

	    List<Object[]>  objectList = query.getResultList();

	    for(int i=0;i<objectList.size();++i){
	    	Object[] sResults = (Object[])objectList.get(i);
	    	accountinAccount=  new AccountingAccount();
	    
	    	accountinAccount.setAccountCode(sResults[0]==null?"":sResults[0].toString());
	    	accountinAccount.setAmount(((BigDecimal)((Object[])sResults)[1]));
	    	accountinAccount.setNatureType(sResults[1]==null?0:Integer.parseInt(sResults[2].toString()));
	    	accountinAccount.setQuantity(((BigDecimal)((Object[])sResults)[3]));
	    	listAccountingAccount.add(accountinAccount);
	    }

		
		
		return listAccountingAccount;
	}
	
	/**
	 * *.
	 *
	 * @param accountingProcess the accounting process
	 * @return the ting accounting account by balance process history
	 */
	public List<AccountingAccount> gettingAccountingAccountByBalanceProcessHistory(AccountingProcess accountingProcess){
		List<AccountingAccount> listAccountingAccount=new ArrayList<>();
		AccountingAccount accountinAccount=null;

		StringBuilder sbQuery=new StringBuilder();
		 /**PK the entity*/
		sbQuery.append(" SELECT").
				append(" 	BASE2.ACCOUNT_CODE  ,   ").		//0
				append(" 	SUM(BASE2.AMOUNT),  ").			//1
				append(" 	BASE2.DYNAMIC_TYPE,  ").			//2
				append(" 	SUM(BASE2.MOVEMENT_QUANTITY)  ").//3
				append(" FROM (  ").
				append(" SELECT").
				append(" 	BASE.ACCOUNT_CODE  ,   ").		//0
				append(" 	SUM(BASE.AMOUNT) as AMOUNT,  ").			//1
				append(" 	BASE.DYNAMIC_TYPE,  ").			//2
				append(" 	SUM(BASE.MOVEMENT_QUANTITY) as MOVEMENT_QUANTITY ").//3
				append(" FROM (  ").
				append("	SELECT ").
				append(" 	DECODE (ACC.IND_REFERENCE,0,  ").
				append(" 		ACC.ACCOUNT_CODE||'.'||PA.MNEMONIC, 1, ACC.ACCOUNT_CODE) AS ACCOUNT_CODE, ").
				append(" 	ASD2.PARAMETER_VALUE AS MOVEMENT_QUANTITY, ").
				append(" 	ARD.AMOUNT, ").
				append(" 	AMD.DYNAMIC_TYPE ").
				append(" 	").
				append("	  FROM ACCOUNTING_PROCESS AP ").
				append("	  INNER JOIN ACCOUNTING_RECEIPT AR ").
				append(" 	  	ON AP.ID_ACCOUNTING_PROCESS_PK=AR.ID_ACCOUNTING_PROCESS_FK ").
				append("	  INNER JOIN ACCOUNTING_RECEIPT_DETAIL ARD ").
				append(" 	  	ON AR.ID_ACCOUNTING_RECEIPTS_PK=ARD.ID_ACCOUNTING_RECEIPTS_FK ").
				append("	  INNER JOIN ACCOUNTING_SOURCE_LOGGER ASL ").
				append(" 	  	ON ARD.ID_ACCOUNTING_RECEIPT_DET_PK=ASL.ID_ACCOUNTING_RECEIPT_DET_FK ").
				append("	  INNER JOIN  ACCOUNTING_SOURCE_DETAIL ASD ").
				append(" 	  	ON ASL.ID_ACCOUNTING_SOURCE_LOGGER_PK=ASD.ID_ACCOUNTING_SOURCE_LOGGER_FK ").
				append("	  INNER JOIN  ACCOUNTING_SOURCE_DETAIL ASD2").
				append("	  	ON ASD2.ID_ACCOUNTING_SOURCE_LOGGER_FK=ASD.ID_ACCOUNTING_SOURCE_LOGGER_FK ").
				append("	  INNER JOIN ACCOUNTING_MATRIX_DETAIL AMD ").
				append(" 	  	ON ARD.ID_ACCOUNTING_MATRIX_DETAIL_FK=AMD.ID_ACCOUNTING_MATRIX_DETAIL_PK ").
				append("	  INNER JOIN ACCOUNTING_ACCOUNT ACC ").
				append(" 	  	ON AMD.ID_ACCOUNTING_ACCOUNT_FK=ACC.ID_ACCOUNTING_ACCOUNT_PK").
				append(" 	  INNER JOIN PARTICIPANT PA").
				append(" 		ON PA.ID_PARTICIPANT_PK=ASD.PARAMETER_VALUE ").
				append("	  ").
				append("	  ").
				append("	  WHERE ASD.PARAMETER_TYPE=:participantType ").
				append("");

		
//	    if(Validations.validateIsNotNullAndPositive(accountingProcess.getIdAccountingProcessPk())){
//	    	sbQuery.append(" AND AP.ID_ACCOUNTING_PROCESS_PK<=:processAccountingPk  ");
//	    }
		sbQuery.append(" AND AP.EXECUTION_DATE > trunc(to_date('31/12/2018','DD/MM/YYYY'),'DD') ");//fecha de corte 31/12/2018
	    
		if(Validations.validateIsNotNull(accountingProcess.getExecutionDate())){
	    	sbQuery.append(" AND AP.EXECUTION_DATE <=:executionDate  ");
	    }
	    
	    if(Validations.validateIsNotNull(accountingProcess.getProcessType())){
	        
            sbQuery.append(" AND   AP.PROCESS_TYPE = :processType ");
        }
        
        sbQuery.append(" AND ASD2.PARAMETER_NAME='MOVEMENT_QUANTITY' ");
        
	    sbQuery.append(" )  BASE   ").
	    	    append(" GROUP BY  BASE.ACCOUNT_CODE, BASE.DYNAMIC_TYPE  ").
				append(" union all	").
				append("	select ACCOUNT_CODE").
				append("		,AMOUNT_BOB as AMOUNT").
				append("		,DYNAMIC_TYPE").
				append("		,MOVEMENT_QUANTITY ").
				append("	from ACCOUNTING_CUT_REPORT").
				append("	where IND_REPORT_TYPE = 2) BASE2").
				append(" GROUP BY  BASE2.ACCOUNT_CODE, BASE2.DYNAMIC_TYPE  ").
				append("	").
	    	    append(" ORDER BY 1  ").
	    	    append("    ");
	    
	    Query query = em.createNativeQuery(sbQuery.toString());
	    
	    query.setParameter("participantType", AccountingFieldSourceType.PARTICIPANT.getCode());
	    
//	    if(Validations.validateIsNotNullAndPositive(accountingProcess.getIdAccountingProcessPk())){
//	    	
//	    	query.setParameter("processAccountingPk", accountingProcess.getIdAccountingProcessPk());
//	    }
	    
	    if(Validations.validateIsNotNull(accountingProcess.getProcessType())){
	    	
	    	query.setParameter("processType", accountingProcess.getProcessType());
        }
	    
	    if(Validations.validateIsNotNull(accountingProcess.getExecutionDate())){
	    	query.setParameter("executionDate", accountingProcess.getExecutionDate());
	    }
        


	    List<Object[]>  objectList = query.getResultList();

	    for(int i=0;i<objectList.size();++i){
	    	Object[] sResults = (Object[])objectList.get(i);
	    	accountinAccount=  new AccountingAccount();
	    
	    	accountinAccount.setAccountCode(sResults[0]==null?"":sResults[0].toString());
	    	accountinAccount.setAmount(((BigDecimal)((Object[])sResults)[1]));
	    	accountinAccount.setNatureType(sResults[1]==null?0:Integer.parseInt(sResults[2].toString()));
	    	accountinAccount.setQuantity(((BigDecimal)((Object[])sResults)[3]));
	    	listAccountingAccount.add(accountinAccount);
	    }

		
		
		return listAccountingAccount;
	}

	/**
	 * Find accounting process to filter.
	 *
	 * @param accountingProcessTo the accounting process to
	 * @return the accounting process
	 */
	public AccountingProcess findAccountingProcessToFilter(AccountingProcessTo accountingProcessTo){

		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" select distinct acp from AccountingProcess acp ").
				append(" WHERE 1=1 ");

		if(Validations.validateIsNotNullAndPositive(accountingProcessTo.getIdAccountingProcessPk())){
			sbQuery.append(" AND acp.idAccountingProcessPk= :idAccountingProcessPk ");
		}
		if(Validations.validateIsNotNullAndPositive(accountingProcessTo.getProcessType())){
			
			 sbQuery.append(" AND acp.processType= :processTypePrm ");
		}
		
		if(Validations.validateIsNotNull(accountingProcessTo.getInitialDate())
				&&Validations.validateIsNotNull(accountingProcessTo.getFinalDate())){
		    sbQuery.append(" and TRUNC(acp.executionDate, 'DD') between :initialDatePrm and :finalDatePrm ");
	    }
		
		if(Validations.validateIsNotNull(accountingProcessTo.getExecutionDate())){
			sbQuery.append(" and TRUNC(acp.executionDate) = TRUNC(:executionDatePrm)  ");
		}
		if(Validations.validateIsNotNullAndPositive(accountingProcessTo.getStartType())){
			sbQuery.append(" AND acp.startType= :startType ");
		}
		sbQuery.append(" ORDER BY acp.idAccountingProcessPk DESC");
    	Query query = em.createQuery(sbQuery.toString());
    	
    	if(Validations.validateIsNotNullAndPositive(accountingProcessTo.getIdAccountingProcessPk())){
    		query.setParameter("idAccountingProcessPk", accountingProcessTo.getIdAccountingProcessPk());
    	}
    	
    	if(Validations.validateIsNotNullAndPositive(accountingProcessTo.getProcessType())){

		    query.setParameter("processTypePrm", accountingProcessTo.getProcessType());
		}
    	if(Validations.validateIsNotNull(accountingProcessTo.getInitialDate())&&
    			Validations.validateIsNotNull(accountingProcessTo.getFinalDate())){
		    query.setParameter("initialDatePrm", accountingProcessTo.getInitialDate());
		    query.setParameter("finalDatePrm", accountingProcessTo.getFinalDate());
	    }
    	if(Validations.validateIsNotNull(accountingProcessTo.getExecutionDate())){
    		query.setParameter("executionDatePrm", accountingProcessTo.getExecutionDate());
    	}
		if(Validations.validateIsNotNullAndPositive(accountingProcessTo.getStartType())){
			query.setParameter("startType", accountingProcessTo.getStartType());
		}
		
    	AccountingProcess accountingProcess=null;

        accountingProcess=(AccountingProcess) query.getResultList().get(0);
        	
    	return  accountingProcess;
    	
	}
	



}