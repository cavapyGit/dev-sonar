package com.pradera.report.generation.executor.collectioneconomic;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.collectioneconomic.service.CollectionReportServiceBean;
import com.pradera.report.generation.executor.collectioneconomic.to.RequestCollectionTO;
import com.pradera.report.generation.poi.ReportPoiGeneratorServiceBean;

@ReportProcess(name = "RequestCollectionEconomicRightReportPoi")
public class RequestCollectionEconomicRightReportPoi extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6175221615431420887L;
	
	@EJB
	private CollectionReportServiceBean  collectionReportServiceBean;
	
	@Inject
	private ReportPoiGeneratorServiceBean reportPoiGeneratorServiceBean;
	
	private RequestCollectionTO collectionTO;

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		
		collectionTO = new RequestCollectionTO();
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals("reportFormats")&&r.getFilterValue()!=null){
				collectionTO.setReportType(Integer.parseInt(r.getFilterValue()));
			}
			
			if (r.getFilterName().equals("sp")&&r.getFilterValue()!=null){
				collectionTO.setIdParticipantPk(Long.parseLong(r.getFilterValue()));
			}
			if (r.getFilterName().equals("param_cui")&&r.getFilterValue() != null){
				collectionTO.setIdHoderPk(Long.parseLong(r.getFilterValue()));
			}
			if (r.getFilterName().equals("security_code")&&r.getFilterValue() != null){
				collectionTO.setIdSecurityCodePk(r.getFilterValue());
			}
			if (r.getFilterName().equals("param_duedate")&&r.getFilterValue() != null){
				collectionTO.setDueDate(r.getFilterValue());
			}
			if (r.getFilterName().equals("param_paymentdate")&& r.getFilterValue() != null){
				collectionTO.setPaymentDate(r.getFilterValue());
			}
			if (r.getFilterName().equals("request_number")&&r.getFilterValue() != null){
				collectionTO.setIdReqCollectionEconomicPk(Long.parseLong(r.getFilterValue()));
			}					
			if (r.getFilterName().equals("date_initial")&&r.getFilterValue() != null){
				collectionTO.setInitialDate(r.getFilterValue());
			}
			if (r.getFilterName().equals("date_end")&&r.getFilterValue() != null){
				collectionTO.setFinalDate(r.getFilterValue());
			}
		}
		//--------------
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] arrayByteExcel = new byte[16384];
		
		List<Object[]> listResult = collectionReportServiceBean.searchEconomicCollectionRequest(collectionTO);
		
		/**GENERATE FILE EXCEL ***/
		try {
			arrayByteExcel=reportPoiGeneratorServiceBean.writeFileExcelRequestCollection(listResult);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/**SETTING ARRAY BYTE TO GENERIC REPORT FOR PERSIST**/
		List<byte[]> bytes = new ArrayList<>();
		bytes.add(arrayByteExcel);
		setListByte(bytes);
		return baos;
	}
}
