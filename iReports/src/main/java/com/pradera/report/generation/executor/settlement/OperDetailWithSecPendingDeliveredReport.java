package com.pradera.report.generation.executor.settlement;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.settlement.to.GenericSettlementOperationTO;

@ReportProcess(name = "OperDetailWithSecPendingDeliveredReport")
public class OperDetailWithSecPendingDeliveredReport extends GenericReport{
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@Inject
	private PraderaLogger log;

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		GenericSettlementOperationTO securitiesPending = new GenericSettlementOperationTO();

		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("currency_type")){
				if (listaLogger.get(i).getFilterValue()!=null && !listaLogger.get(i).getFilterValue().equals("0")){
					securitiesPending.setCurrency(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("date")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					securitiesPending.setDate(listaLogger.get(i).getFilterValue());
				}
			}	
		}

		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> currencyDesc = new HashMap<Integer, String>();
		Map<Integer,String> processTypeDesc = new HashMap<Integer, String>();
		try {
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				currencyDesc.put(param.getParameterTablePk(), param.getParameterName());
			}
			filter.setMasterTableFk(MasterTableType.PARAMETER_TABLE_SCHED_SETTLEMENT.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				processTypeDesc.put(param.getParameterTablePk(), param.getParameterName());
			}
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		if(securitiesPending.getCurrency()!=null){
			parametersRequired.put("currency_type_desc", currencyDesc.get(Integer.valueOf(securitiesPending.getCurrency())));
		}else{
			parametersRequired.put("currency_type_desc", "TODOS");
		}
		parametersRequired.put("map_currency_desc", currencyDesc);
		parametersRequired.put("map_process_type_desc", processTypeDesc);
		
		return parametersRequired;
	}
}
