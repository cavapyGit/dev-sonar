package com.pradera.report.generation.executor.securities;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.GeographicLocationServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.GeographicLocationStateType;
import com.pradera.model.generalparameter.type.GeographicLocationType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;

@ReportProcess(name = "ExpirationAnticipatedSecurityByIssuerReport")
public class ExpirationAnticipatedSecurityByIssuerReport extends GenericReport{
private static final long serialVersionUID = 1L;

@Inject  PraderaLogger log;

@EJB
private ParameterServiceBean parameterService;

@EJB
GeographicLocationServiceBean geographicLocationServiceBean;

public ExpirationAnticipatedSecurityByIssuerReport() {
}
	
@Override
public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public void addParametersQueryReport() {
	List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
	if (listaLogger == null) {
		listaLogger = new ArrayList<ReportLoggerDetail>();
	}

}

public List<GeographicLocation> getLstCountry() {

	List<GeographicLocation> lst = null;

	try {

		GeographicLocationTO geographicLocationTO = new GeographicLocationTO();

		geographicLocationTO.setGeographicLocationType(GeographicLocationType.COUNTRY.getCode());
		geographicLocationTO.setState(GeographicLocationStateType.REGISTERED.getCode());

		lst = geographicLocationServiceBean.getListGeographicLocation(geographicLocationTO);

	} catch (Exception ex) {
		ex.printStackTrace();
	}

	return lst;

}

@Override
public Map<String, Object> getCustomJasperParameters() {
	Map<String,Object> parametersRequired = new HashMap<>();
	ParameterTableTO  filter = new ParameterTableTO();
	GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
	Map<Integer,String> document_type = new HashMap<Integer, String>();
	Map<Integer,String> economic_sector = new HashMap<Integer, String>();
	Map<Integer,String> economic_activity = new HashMap<Integer, String>();
	Map<Integer,String> person_type = new HashMap<Integer, String>();
	Map<Integer,String> representative_class = new HashMap<Integer, String>();
	Map<Integer,String> nationality = new HashMap<Integer, String>();
	Map<Integer,String> legal_residence = new HashMap<Integer, String>();
	try {
		filter.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
			document_type.put(param.getParameterTablePk(), param.getIndicator1());
		}
		filter.setMasterTableFk(MasterTableType.ECONOMIC_SECTOR.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
			economic_sector.put(param.getParameterTablePk(), param.getParameterName());
		}
		filter.setMasterTableFk(MasterTableType.ECONOMIC_ACTIVITY.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
			economic_activity.put(param.getParameterTablePk(), param.getParameterName());
		}
		filter.setMasterTableFk(MasterTableType.PERSON_TYPE.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
			person_type.put(param.getParameterTablePk(), param.getParameterName());
		}
		filter.setMasterTableFk(MasterTableType.CLASS_REPRENTATIVE.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
			representative_class.put(param.getParameterTablePk(), param.getParameterName());
		}
		
		geographicLocationTO.setGeographicLocationType(GeographicLocationType.COUNTRY.getCode());
		
		for(GeographicLocation geo : geographicLocationServiceBean.getListGeographicLocation(geographicLocationTO))		
		{
			nationality.put(geo.getIdGeographicLocationPk(), geo.getName());
			legal_residence.put(geo.getIdGeographicLocationPk(), geo.getName());
		}
		
	}catch(Exception ex){
		log.error(ex.getMessage());
		ex.printStackTrace();
		throw new RuntimeException();
	}
	parametersRequired.put("p_document_type", document_type);
	parametersRequired.put("p_economic_sector", economic_sector);
	parametersRequired.put("p_economic_activity", economic_activity);
	parametersRequired.put("p_person_type", person_type);
	parametersRequired.put("p_representative_class", representative_class);
	parametersRequired.put("p_nationality", nationality);
	parametersRequired.put("p_legal_residence", legal_residence);
	// TODO Auto-generated method stub
	return parametersRequired;
	}

}
