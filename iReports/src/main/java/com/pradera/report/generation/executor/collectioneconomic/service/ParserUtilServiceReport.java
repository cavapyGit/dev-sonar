package com.pradera.report.generation.executor.collectioneconomic.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.report.generation.executor.account.service.AccountReportServiceBean;
import com.pradera.report.generation.executor.account.to.PortafolioDetailCustodianTO;
import com.pradera.report.generation.executor.issuances.to.PaymentChronogramTO;
import com.pradera.report.generation.executor.securities.service.PaymentChronogramBySecurity;
import com.pradera.report.generation.executor.securities.to.SecuritiesByIssuanceAndIssuerTO;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;
import com.pradera.report.management.facade.ReportMgmtServiceFacade;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class ParserUtilServiceReport extends CrudDaoServiceBean {

	@EJB
	private ReportMgmtServiceFacade reportMgmtServiceFacade;

	@EJB
	private PaymentChronogramBySecurity securitiesService;
	
	@EJB
	private AccountReportServiceBean accountReportService;

	@EJB
	private ParameterServiceBean parameterService;
	
	public List<SecuritiesByIssuanceAndIssuerTO> getSecurityCharacteristicsFromFile(
			SecuritiesByIssuanceAndIssuerTO charactsecTO, Long idReportPk) {

		List<SecuritiesByIssuanceAndIssuerTO> listResult = new ArrayList<>();
		try {
			/** Buscar los registros en los reportes ya generados */
			List<String> list = getDataListReportFile(idReportPk,
					charactsecTO.getDate());
			/**
			 * Puede que no haya ningun reporte generado, entonces se ejecuta el
			 * reporte
			 */
			if (list == null) {
				List<SecuritiesByIssuanceAndIssuerTO> lstAux = securitiesService
						.getSecurityCharacteristics(charactsecTO);
				/** Filtramos solo clase de valor buscados. */
				listResult = filterListBySecurityClass(lstAux,
						charactsecTO.getLstSecurityClass());
				return listResult;
			}
			SecuritiesByIssuanceAndIssuerTO cc = null;
			for (String rowValue : list) {

				String[] column = rowValue.split(",", -1);
				cc = new SecuritiesByIssuanceAndIssuerTO();

				cc.setSecurityClass(getIdParameterTablePk(column[0]));
				if (!charactsecTO.getLstSecurityClass().contains(
						cc.getSecurityClass()))
					continue;

				cc.setIdSecurityOnly(column[1]);
				cc.setMnemonic(column[2]);
				cc.setCustodian(column[3]);

				// Date dateInformation =
				// CommonsUtilities.convertStringtoDate(column[4],"yyyy-MM-dd");
				Date dateInformation = charactsecTO.getDateForName();
				cc.setDateInformation(dateInformation);

				Date registreDate = CommonsUtilities.convertStringtoDate(column[5], "yyyy-MM-dd");
				cc.setRegistryDt(registreDate);

				cc.setParticipantPlacement(column[6]);

				Integer currencyAsfi = Integer.parseInt(column[7]);
				//if (currencyAsfi.equals(CurrencyType.PYG.getCodeAsfi()))
				if (currencyAsfi.equals(10))
					cc.setCurrency(CurrencyType.PYG.getCode());

				//if (currencyAsfi.equals(CurrencyType.USD.getCodeAsfi()))
				if (currencyAsfi.equals(12))
					cc.setCurrency(CurrencyType.USD.getCode());

				//if (currencyAsfi.equals(CurrencyType.UFV.getCodeAsfi()))
				if (currencyAsfi.equals(14))
					cc.setCurrency(CurrencyType.UFV.getCode());

				//if (currencyAsfi.equals(CurrencyType.EU.getCodeAsfi()))
				if (currencyAsfi.equals(15))
					cc.setCurrency(CurrencyType.EU.getCode());

				//if (currencyAsfi.equals(CurrencyType.DMV.getCodeAsfi()))
				if (currencyAsfi.equals(11))
					cc.setCurrency(CurrencyType.DMV.getCode());

				BigDecimal ininomval = new BigDecimal(column[8]);
				cc.setIninomval(ininomval);

				Date issuancedate = CommonsUtilities.convertStringtoDate(column[9], "yyyy-MM-dd");
				cc.setIssuancedate(issuancedate);

				Date expirationDtInt = CommonsUtilities.convertStringtoDate(column[10], "yyyy-MM-dd");
				cc.setExpirationDtInt(expirationDtInt);

				if (column[11].length() > 0)
					cc.setIssuanceAmount(new BigDecimal(column[11]));

				if (column[12].length() > 0)
					cc.setMaximumInvestment(new BigDecimal(column[12]));

				Integer shareBalance = Integer.parseInt(column[13]);
				cc.setShareBalance(shareBalance);

				BigDecimal marketRate = new BigDecimal(column[14]);
				cc.setMarketrate(marketRate);

				BigDecimal discountRate = new BigDecimal(column[15]);
				cc.setDiscountrate(discountRate);

				Integer couponsAmount = Integer.parseInt(column[16]);
				cc.setCouponNumberInt(couponsAmount);

				Integer amortizationType = Integer.parseInt(column[17]);
				cc.setAmortizationType(amortizationType);

				if (column[18].length() > 0)
					cc.setPeriodicity(column[18]);

				if (column[19].length() > 0)
					cc.setNegotiatingmanner(column[19]);

				if (column[20].length() > 0)
					cc.setMarketmoney(column[20]);

				/*
				 * if(column[21].length()>0)
				 * cc.setParticipantPlacement(column[21]);
				 */
				listResult.add(cc);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return listResult;
	}

	public List<PaymentChronogramTO> getCouponsCharacteristicsFromFile(
			PaymentChronogramTO couponsTO, boolean onlyBts, Long idReportPk) {
		List<PaymentChronogramTO> listResult = new ArrayList<>();
		/** Buscar los registros en los reportes ya generados */
		List<String> list;
		try {
			list = getDataListReportFile(idReportPk, couponsTO.getDate());
			/**
			 * Puede que no haya ningun reporte generado, entonces se ejecuta el
			 * reporte
			 */
			if (list == null) {
				List<PaymentChronogramTO> lstAux = securitiesService
						.getCouponsCharacteristics(couponsTO);
				return lstAux;
			}
			/** Parser del reporte */
			PaymentChronogramTO cc = null;
			for (String rowValue : list) {
				String[] column = rowValue.split(",", -1);
				cc = new PaymentChronogramTO();
				cc.setCustodian(column[0]);
				cc.setRegistryDate(column[1]);
				cc.setDateForName(couponsTO.getDateForName());
				cc.setDateInformation(couponsTO.getDateForName());
				cc.setSecurityClass(getIdParameterTablePk(column[2]));
				cc.setIdSecurityOnly(column[3]);
				cc.setCouponNumberInt(getIntegerValue(column[4]));
				cc.setExpirationDate(column[5]);
				cc.setAmortizationFactor(getBigdecimalValue(column[6]));
				cc.setInterestFactor(getBigdecimalValue(column[7]));
				cc.setTotalCoupon(getBigdecimalValue(column[8]));
				if (onlyBts)
					if (cc.getSecurityClass().equals(
							SecurityClassType.BTS.getCode()))
						listResult.add(cc);
					else
						continue;
				else
					listResult.add(cc);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return listResult;
	}
	public List<PaymentChronogramTO> getCouponsCharacteristicsFromFileExcludeBts(
			PaymentChronogramTO couponsTO, Long idReportPk) {
		List<PaymentChronogramTO> listResult = new ArrayList<>();
		/** Buscar los registros en los reportes ya generados */
		List<String> list;
		try {
			list = getDataListReportFile(idReportPk, couponsTO.getDate());
			/**
			 * Puede que no haya ningun reporte generado, entonces se ejecuta el
			 * reporte
			 */
			if (list == null) {
				List<PaymentChronogramTO> lstAux = securitiesService
						.getCouponsCharacteristics(couponsTO);
				return lstAux;
			}
			/** Parser del reporte */
			PaymentChronogramTO cc = null;
			for (String rowValue : list) {
				String[] column = rowValue.split(",", -1);
				cc = new PaymentChronogramTO();
				cc.setCustodian(column[0]);
				cc.setRegistryDate(column[1]);
				cc.setDateForName(couponsTO.getDateForName());
				cc.setDateInformation(couponsTO.getDateForName());
				cc.setSecurityClass(getIdParameterTablePk(column[2]));
				cc.setIdSecurityOnly(column[3]);
				cc.setCouponNumberInt(getIntegerValue(column[4]));
				cc.setExpirationDate(column[5]);
				cc.setAmortizationFactor(getBigdecimalValue(column[6]));
				cc.setInterestFactor(getBigdecimalValue(column[7]));
				cc.setTotalCoupon(getBigdecimalValue(column[8]));
				if (cc.getSecurityClass().equals(SecurityClassType.BTS.getCode()))
					continue;
				else
					listResult.add(cc);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return listResult;
	}

	private Integer getIntegerValue(String value) {
		try {
			Integer integer = Integer.parseInt(value);
			return integer;
		} catch (Exception e) {
			return 0;
		}
	}

	private BigDecimal getBigdecimalValue(String value) {
		try {
			BigDecimal bigDecimal = new BigDecimal(value);
			return bigDecimal;
		} catch (Exception e) {
			return BigDecimal.ZERO;
		}
	}

	/** Filtramos solo las clases de valor requeridas. */
	public List<SecuritiesByIssuanceAndIssuerTO> filterListBySecurityClass(
			List<SecuritiesByIssuanceAndIssuerTO> list,
			List<Integer> lstSecurityClass) {
		List<SecuritiesByIssuanceAndIssuerTO> listResult = new ArrayList<>();
		for (SecuritiesByIssuanceAndIssuerTO cc : list) {
			if (!lstSecurityClass.contains(cc.getSecurityClass()))
				continue;
			listResult.add(cc);
		}
		return listResult;
	}

	public Integer getIdParameterTablePk(String parameterValue) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder
				.append(" select PARAMETER_TABLE_PK from PARAMETER_TABLE ");
		stringBuilder.append(" where 1 = 1 ");
		stringBuilder.append(" and TEXT1 = '" + parameterValue + "'");

		Query query = em.createNativeQuery(stringBuilder.toString());
		BigDecimal idParameterTablePk = (BigDecimal) query.getSingleResult();
		return idParameterTablePk.intValue();
	}

	public List<String> getDataListReportFile(Long idReportPk, Date cutDate)
			throws IOException {
		/**
		 * Verificar si existe reporte con ese filtro de automatic = true Solo
		 * el batchero pone ese filtro
		 */
		BigDecimal idReportLoggerFilePk = null;
		/** descendente para obtener la ultima pre carga de reportes */
		StringBuilder firstBuilder = new StringBuilder();
		firstBuilder
				.append(" SELECT MAX(RLF.ID_LOGGER_FILE_PK) FROM REPORT_LOGGER_FILE RLF ");
		firstBuilder
				.append(" INNER JOIN REPORT_LOGGER RL  ON RLF.ID_REPORT_LOGGER_FK = RL.ID_REPORT_LOGGER_PK ");
		firstBuilder
				.append(" INNER JOIN    REPORT_LOGGER_DETAIL RLD1    ON RL.ID_REPORT_LOGGER_PK = RLD1.ID_REPORT_LOGGER_FK ");
		firstBuilder
				.append(" INNER JOIN    REPORT_LOGGER_DETAIL RLD2    ON RL.ID_REPORT_LOGGER_PK = RLD2.ID_REPORT_LOGGER_FK ");
		firstBuilder.append(" where 1 = 1 ");
		firstBuilder.append(" AND RL.REPORT_STATE = 1387 ");
		firstBuilder.append(" AND RL.REPORT_FORMAT = 1391 ");
		firstBuilder.append(" AND RL.IND_ERROR = 0 ");
		firstBuilder.append(" AND RLD1.FILTER_NAME = 'date' ");
		firstBuilder.append(" AND RLD1.FILTER_VALUE = '"
				+ CommonsUtilities.convertDatetoString(cutDate) + "'");
		firstBuilder.append(" AND RLD2.FILTER_NAME = 'automatic' ");//enviado por batchero
		firstBuilder.append(" AND RLD2.FILTER_VALUE = 'true' ");
		firstBuilder.append(" AND RL.ID_REPORT_FK = " + idReportPk);
		firstBuilder.append(" AND RLF.DATA_REPORT IS NOT NULL ");
		firstBuilder.append(" ORDER BY RLF.ID_LOGGER_FILE_PK DESC ");
		Query firstQuery;
		try {
			firstQuery = em.createNativeQuery(firstBuilder.toString());
			idReportLoggerFilePk = (BigDecimal) firstQuery.getSingleResult();
		} catch (Exception e) {
			idReportLoggerFilePk = null;
		}
		/** Se considera el reporte creado por el usuario KEGUIVAR */
		if (idReportLoggerFilePk == null) {
			StringBuilder secondBuilder = new StringBuilder();
			secondBuilder
					.append(" SELECT MIN(RLF.ID_LOGGER_FILE_PK) FROM REPORT_LOGGER_FILE RLF ");
			secondBuilder
					.append(" INNER JOIN REPORT_LOGGER RL  ON RLF.ID_REPORT_LOGGER_FK = RL.ID_REPORT_LOGGER_PK ");
			secondBuilder
					.append(" INNER JOIN REPORT_LOGGER_DETAIL RLD ON RL.ID_REPORT_LOGGER_PK = RLD.ID_REPORT_LOGGER_FK ");
			secondBuilder.append(" where 1 = 1 ");
			secondBuilder.append(" AND RL.REPORT_STATE = 1387 ");
			secondBuilder.append(" AND RL.REPORT_FORMAT = 1391 ");
			secondBuilder.append(" AND RL.IND_ERROR = 0 ");
			secondBuilder.append(" AND RLD.FILTER_NAME = 'date' ");
			secondBuilder.append(" AND RLD.FILTER_VALUE = '"+ CommonsUtilities.convertDatetoString(cutDate) + "'");
			secondBuilder.append(" AND RL.ID_REPORT_FK = " + idReportPk);
			secondBuilder.append(" AND RL.REGISTRY_USER =  'KEGUIVAR' ");
			secondBuilder.append(" AND RLF.DATA_REPORT IS NOT NULL ");
			secondBuilder.append(" ORDER BY RLF.ID_LOGGER_FILE_PK ASC ");
			Query secondQuery;
			try {
				secondQuery = em.createNativeQuery(secondBuilder.toString());
				idReportLoggerFilePk = (BigDecimal) secondQuery
						.getSingleResult();
			} catch (Exception e) {
				idReportLoggerFilePk = null;
			}
		}
		/*
		 * En caso de que no se corrio el batchero, pero se ejecuto de manera
		 * manual y sin la logica de fechas
		 */
		if (idReportLoggerFilePk == null) {
			StringBuilder zeroBuilder = new StringBuilder();
			zeroBuilder
					.append(" SELECT MIN(RLF.ID_LOGGER_FILE_PK) FROM REPORT_LOGGER_FILE RLF ");
			zeroBuilder
					.append(" INNER JOIN REPORT_LOGGER RL  ON RLF.ID_REPORT_LOGGER_FK = RL.ID_REPORT_LOGGER_PK ");
			zeroBuilder
					.append(" INNER JOIN    REPORT_LOGGER_DETAIL RLD1    ON RL.ID_REPORT_LOGGER_PK = RLD1.ID_REPORT_LOGGER_FK ");
			zeroBuilder
					.append(" INNER JOIN    REPORT_LOGGER_DETAIL RLD2    ON RL.ID_REPORT_LOGGER_PK = RLD2.ID_REPORT_LOGGER_FK ");
			zeroBuilder.append(" where 1 = 1 ");
			zeroBuilder.append(" AND RL.REPORT_STATE = 1387 ");
			zeroBuilder.append(" AND RL.REPORT_FORMAT = 1391 ");
			zeroBuilder.append(" AND RL.IND_ERROR = 0 ");
			zeroBuilder.append(" AND RLD1.FILTER_NAME = 'date' ");
			zeroBuilder.append(" AND RLD1.FILTER_VALUE = '"
					+ CommonsUtilities.convertDatetoString(cutDate) + "'");
			zeroBuilder.append(" AND RLD2.FILTER_NAME = 'logic' ");
			zeroBuilder.append(" AND RLD2.FILTER_VALUE = 'false' ");
			zeroBuilder.append(" AND RL.ID_REPORT_FK = " + idReportPk);
			zeroBuilder.append(" AND RLF.DATA_REPORT IS NOT NULL ");
			zeroBuilder.append(" ORDER BY RLF.ID_LOGGER_FILE_PK ASC ");
			Query zeroQuery;
			try {
				zeroQuery = em.createNativeQuery(zeroBuilder.toString());
				idReportLoggerFilePk = (BigDecimal) zeroQuery.getSingleResult();
			} catch (Exception e) {
				idReportLoggerFilePk = null;
			}
		}
		/**
		 * Consultamos en caso de que el reporte no haya si creado con el
		 * batchero(reportes pasados)
		 */
		if (idReportLoggerFilePk == null) {
			StringBuilder threedBuilder = new StringBuilder();
			threedBuilder.append(" SELECT MIN(RLF.ID_LOGGER_FILE_PK) FROM REPORT_LOGGER_FILE RLF ");
			threedBuilder.append(" INNER JOIN REPORT_LOGGER RL  ON RLF.ID_REPORT_LOGGER_FK = RL.ID_REPORT_LOGGER_PK ");
			threedBuilder.append(" INNER JOIN REPORT_LOGGER_DETAIL RLD ON RL.ID_REPORT_LOGGER_PK = RLD.ID_REPORT_LOGGER_FK ");
			threedBuilder.append(" where 1 = 1 ");
			threedBuilder.append(" AND RL.REPORT_STATE = 1387 ");
			threedBuilder.append(" AND RL.REPORT_FORMAT = 1391 ");
			threedBuilder.append(" AND RL.IND_ERROR = 0 ");
			threedBuilder.append(" AND RLD.FILTER_NAME = 'date' ");
			threedBuilder.append(" AND RLD.FILTER_VALUE = '"+ CommonsUtilities.convertDatetoString(cutDate) + "'");
			threedBuilder.append(" AND RL.ID_REPORT_FK = " + idReportPk);
			threedBuilder.append(" AND RLF.DATA_REPORT IS NOT NULL ");
			threedBuilder.append(" ORDER BY RLF.ID_LOGGER_FILE_PK ASC ");
			Query threeQuery;
			try {
				threeQuery = em.createNativeQuery(threedBuilder.toString());
				idReportLoggerFilePk = (BigDecimal) threeQuery
						.getSingleResult();
			} catch (Exception e) {
				idReportLoggerFilePk = null;
			}
		}
		/** Solo valido para el id 70 reporte */
		if (idReportLoggerFilePk == null && idReportPk == 70L) {
			StringBuilder fourBuilder = new StringBuilder();
			fourBuilder
					.append(" SELECT MIN(RLF.ID_LOGGER_FILE_PK) FROM REPORT_LOGGER_FILE RLF ");
			fourBuilder
					.append(" INNER JOIN REPORT_LOGGER RL  ON RLF.ID_REPORT_LOGGER_FK = RL.ID_REPORT_LOGGER_PK ");
			fourBuilder
					.append(" INNER JOIN REPORT_LOGGER_DETAIL RLD ON RL.ID_REPORT_LOGGER_PK = RLD.ID_REPORT_LOGGER_FK ");
			fourBuilder.append(" where 1 = 1 ");
			fourBuilder.append(" AND RL.REPORT_STATE = 1387 ");
			fourBuilder.append(" AND RL.REPORT_FORMAT = 1391 ");
			fourBuilder.append(" AND RL.IND_ERROR = 0 ");
			fourBuilder.append(" AND RLD.FILTER_NAME = 'registry_date' ");
			fourBuilder.append(" AND RLD.FILTER_VALUE = '"+ CommonsUtilities.convertDatetoString(cutDate) + "'");
			fourBuilder.append(" AND RL.ID_REPORT_FK = " + idReportPk);
			fourBuilder.append(" AND RLF.DATA_REPORT IS NOT NULL ");
			fourBuilder.append(" ORDER BY RLF.ID_LOGGER_FILE_PK ASC ");
			Query fourQuery;
			try {
				fourQuery = em.createNativeQuery(fourBuilder.toString());
				idReportLoggerFilePk = (BigDecimal) fourQuery.getSingleResult();
			} catch (Exception e) {
				return null;
			}
		}
		if (idReportLoggerFilePk == null)
			return null;
		/** Recuperando datos del reporte */
		try {
			byte[] fileContent = reportMgmtServiceFacade
					.getReportLoggerFileContent(idReportLoggerFilePk
							.longValue());
			String dataReport = new String(fileContent);
			Reader reader = new StringReader(dataReport);
			BufferedReader buf = new BufferedReader(reader);
			List<String> list = new ArrayList<>();
			String lineReport = buf.readLine();
			/**
			 * Reportes pasados tienen un espacio y la cadena no se encontro
			 * data
			 */
			if (lineReport.length() > 1
					&& !lineReport.equals("No se encontro data disponible"))
				while (lineReport != null) {
					lineReport = lineReport.replace("\"", "");
					list.add(lineReport);
					lineReport = buf.readLine();
				}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	/** Reporte 8 **/
	@TransactionTimeout(unit=TimeUnit.HOURS,value=2)
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<PortafolioDetailCustodianTO> getCustodyDetailAgbSafiFicTxtFromFile(PortafolioDetailCustodianTO pdcTO,String strQuery) {
		List<String> list;
		List<PortafolioDetailCustodianTO> listResult = new ArrayList<>();
		try {
			list = getDataListReportFile(8L, pdcTO.getDateMarketFact());
			if (list == null){
				listResult = accountReportService.getCustodyDetailAgbSafiFicTxt(pdcTO, strQuery, null);
				return listResult;
			}
			PortafolioDetailCustodianTO pdc = null;
			for (String rowValue : list) {
				String[] column = rowValue.split(",", -1);
				pdc = new PortafolioDetailCustodianTO();
				pdc.setCustodio(column[0]); // CUSTODIA
				pdc.setDateMarketFactString(column[1]);// INFO DATE
				pdc.setFundAdmin(column[2]);// MNEMONICO SAFI
				pdc.setFundType(column[3]);// MNEMONICO FONDOS
				pdc.setInstrumentSecClass(column[4]);// CLASE
				pdc.setIdSecurityCode(column[5]);// CLAVE
				pdc.setValuatorCode(column[6].toString());
				pdc.setTotalBalance(getBigdecimalValue(column[7]));// CANTIDAD
				pdc.setMarketRate(getBigdecimalValue(column[8]));// TASA
				pdc.setMarketPrice(getBigdecimalValue(column[9]));// PRECIO
				pdc.setSecCodeOnCustody(column[10]); // DES o FIS
				pdc.setIndReporto(column[11]);
				listResult.add(pdc);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return listResult;
	}

	public List<PaymentChronogramTO> getDetachedCouponsCharactFromFile(
			GenericsFiltersTO filtersTO) {
		List<String> list;
		List<PaymentChronogramTO> listResult = new ArrayList<>();
		Date date = CommonsUtilities.convertStringtoDate(filtersTO.getDate(),"dd/MM/yyyy");
		try {
			list = getDataListReportFile(70L, date);
			if(list== null){
				listResult = securitiesService.getDetachedCouponsCharact(filtersTO);
				return listResult;
			}
				
			PaymentChronogramTO cc;
			for (String rowValue : list) {
				String[] column = rowValue.split(",", -1);
				cc = new PaymentChronogramTO();
				cc.setCustodian(column[0]);
				Date registryDt = CommonsUtilities.convertStringtoDate(column[1], "yyyy-MMdd");
				cc.setRegistryDt(registryDt);
				cc.setSecurityClass(getIntegerValue(column[2]));
				cc.setIdSecurityOnly(column[3]);
				cc.setStrCouponNumbers(column[4]);
				cc.setValidCoupon(column[5]);
				listResult.add(cc);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return listResult;
	}
	public String searchParticipant(String idSecurityCodePk){
		StringBuilder builder = new StringBuilder();
		builder.append(" select P_COLOC.MNEMONIC ");
		builder.append(" from MECHANISM_OPERATION MO ");
		builder.append(" LEFT JOIN PARTICIPANT P_COLOC ON P_COLOC.ID_PARTICIPANT_PK = MO.ID_SELLER_PARTICIPANT_FK  ");
		builder.append(" where 1 = 1 ");
		builder.append(" and MO.ID_SECURITY_CODE_FK = '"+idSecurityCodePk+"'");
		builder.append(" AND MO.ID_NEGOTIATION_MODALITY_FK = 12  ");
		builder.append(" AND MO.ID_NEGOTIATION_MECHANISM_FK = 1  ");
		builder.append(" AND MO.OPERATION_STATE = 615 ");
		Query query;
		try {
			query = em.createNativeQuery(builder.toString());
			String part =  (String) query.getSingleResult();
			return part;
		} catch (Exception e) {
			return null;
		}
	}
	public String dematerializedSecurities(String idSecurityCodePk){
		StringBuilder builder = new StringBuilder();
		builder.append(" SELECT DESMATERIALIZED_BALANCE FROM SECURITY ");
		builder.append(" WHERE 1 = 1 ");
		builder.append(" AND ID_SECURITY_CODE_PK = '"+idSecurityCodePk+"'");
		builder.append(" AND STATE_SECURITY = 131 ");
		Query query;
		try {
			query = em.createNativeQuery(builder.toString());
			BigDecimal res=   (BigDecimal) query.getSingleResult();
			return res.toString();
		} catch (Exception e) {
			return null;
		}
	}
	public String getIssuerMnemonic(String idSecurityCodePk){
		StringBuilder builder = new StringBuilder();
		builder.append(" select P_COLOC.MNEMONIC ");
		builder.append(" from MECHANISM_OPERATION MO ");
		builder.append(" LEFT JOIN PARTICIPANT P_COLOC ON P_COLOC.ID_PARTICIPANT_PK = MO.ID_SELLER_PARTICIPANT_FK  ");
		builder.append(" where 1 = 1 ");
		builder.append(" and MO.ID_SECURITY_CODE_FK = '"+idSecurityCodePk+"'");
		builder.append(" AND MO.ID_NEGOTIATION_MODALITY_FK = 12  ");
		builder.append(" AND MO.ID_NEGOTIATION_MECHANISM_FK = 1  ");
		builder.append(" AND MO.OPERATION_STATE = 615 ");
		Query query;
		try {
			query = em.createNativeQuery(builder.toString());
			String part =  (String) query.getSingleResult();
			return part;
		} catch (Exception e) {
			return null;
		}
	}
	public String getFundTypeHolder(String mNemonicPart,String idSecurityCodePk,String cutDate,String valuatorCode, BigDecimal totalBalance){
		StringBuilder builder = new StringBuilder();
		builder.append(" select HO.FUND_ADMINISTRATOR from MARKET_FACT_VIEW MFV ");
		builder.append(" inner join HOLDER_ACCOUNT_DETAIL HAD  ON MFV.ID_HOLDER_ACCOUNT_PK = HAD.ID_HOLDER_ACCOUNT_FK ");
		builder.append(" INNER JOIN HOLDER HO ON HO.ID_HOLDER_PK =  HAD.ID_HOLDER_FK ");
		builder.append(" where 1 = 1 ");
		//builder.append(" and MFV.ID_PARTICIPANT_PK = (select ID_PARTICIPANT_PK from PARTICIPANT where 1 = 1 and MNEMONIC = '"+mNemonicPart+"') ");
		builder.append(" and MFV.ID_SECURITY_CODE_PK =  '"+idSecurityCodePk+"' ");
		builder.append(" and MFV.CUT_DATE = TO_DATE('"+cutDate+"','yyyy-MM-dd') ");
		if(!valuatorCode.isEmpty())
			builder.append(" and MFV.VALUATOR_CODE = '"+valuatorCode+"' ");
		builder.append(" and MFV.TOTAL_BALANCE = "+totalBalance);
		Query query;
		try {
			query = em.createNativeQuery(builder.toString());
			String part =  (String) query.getSingleResult();
			return part;
		} catch (Exception e) {
			return "";
		}
	}
	public boolean verifyReportsAsfi(Date cutDate,Long idReportPk){
		BigDecimal idReportLoggerFilePk = null;
		/** descendente para obtener la ultima pre carga de reportes */
		StringBuilder firstBuilder = new StringBuilder();
		firstBuilder
				.append(" SELECT MAX(RLF.ID_LOGGER_FILE_PK) FROM REPORT_LOGGER_FILE RLF ");
		firstBuilder
				.append(" INNER JOIN REPORT_LOGGER RL  ON RLF.ID_REPORT_LOGGER_FK = RL.ID_REPORT_LOGGER_PK ");
		firstBuilder
				.append(" INNER JOIN    REPORT_LOGGER_DETAIL RLD1    ON RL.ID_REPORT_LOGGER_PK = RLD1.ID_REPORT_LOGGER_FK ");
		firstBuilder
				.append(" INNER JOIN    REPORT_LOGGER_DETAIL RLD2    ON RL.ID_REPORT_LOGGER_PK = RLD2.ID_REPORT_LOGGER_FK ");
		firstBuilder.append(" where 1 = 1 ");
		firstBuilder.append(" AND RL.REPORT_STATE = 1387 ");
		firstBuilder.append(" AND RL.REPORT_FORMAT = 1391 ");
		firstBuilder.append(" AND RL.IND_ERROR = 0 ");
		firstBuilder.append(" AND RLD1.FILTER_NAME = 'date' ");
		firstBuilder.append(" AND RLD1.FILTER_VALUE = '"
				+ CommonsUtilities.convertDatetoString(cutDate) + "'");
		firstBuilder.append(" AND RLD2.FILTER_NAME = 'automatic' ");//enviado por batchero
		firstBuilder.append(" AND RLD2.FILTER_VALUE = 'true' ");
		firstBuilder.append(" AND RL.ID_REPORT_FK = " + idReportPk);
		firstBuilder.append(" AND RLF.DATA_REPORT IS NOT NULL ");
		firstBuilder.append(" ORDER BY RLF.ID_LOGGER_FILE_PK DESC ");
		Query firstQuery;
		try {
			firstQuery = em.createNativeQuery(firstBuilder.toString());
			idReportLoggerFilePk = (BigDecimal) firstQuery.getSingleResult();
			if(idReportLoggerFilePk==null)
				return false;
			else
				return true;
		} catch (Exception e) {
			return false;
		}
	}
}
