package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.account.service.AccountReportServiceBean;
import com.pradera.report.generation.executor.account.to.NoteToExternalTO;

@ReportProcess(name = "NoteGenerationNewProductReport")
public class NoteGenerationNewProductReport extends GenericReport{

	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;

	@EJB
	private AccountReportServiceBean reportServiceBean;

	@Inject
	PraderaLogger log;
	
	public NoteGenerationNewProductReport() {}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParametersQueryReport() {

	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {

		Map<String, Object> parametersRequired = new HashMap<>();
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		NoteToExternalTO noteExternalTO = new NoteToExternalTO();
		
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		DateFormat dateFormatAnio = new SimpleDateFormat("yyyy");
		
		DateFormat formateador = new SimpleDateFormat("dd 'de' MMMM 'de' yyyy", new Locale("ES"));
		Date fechaDate = new Date();
		
		// DATOS DE ENTRADA DEL SISTEMA
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.QUOTE_NOTE_EXTERNAL)) {
				noteExternalTO.setQuote(r.getFilterValue());
			}
			
		}
		
		parametersRequired.put("quote", noteExternalTO.getQuote());
		parametersRequired.put("fecha_actual", dateFormat.format(date));
		parametersRequired.put("anio", dateFormatAnio.format(date));
		parametersRequired.put("fecha_hoy", formateador.format(fechaDate));

		return parametersRequired;
	}
}
