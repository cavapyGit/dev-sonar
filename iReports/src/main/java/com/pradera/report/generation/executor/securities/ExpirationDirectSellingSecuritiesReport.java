package com.pradera.report.generation.executor.securities;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.GeographicLocationServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.GeographicLocationType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;

@ReportProcess(name = "ExpirationDirectSellingSecuritiesReport")
public class ExpirationDirectSellingSecuritiesReport extends GenericReport{
	private static final long serialVersionUID = 1L;
	
	@Inject  PraderaLogger log;

	@Inject 
	@Configurable 
	String idIssuerBC;
	
	@EJB
	private ParameterServiceBean parameterService;

	@EJB
	GeographicLocationServiceBean geographicLocationServiceBean;

	public ExpirationDirectSellingSecuritiesReport() {
	}
		
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParametersQueryReport() { }

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
		Map<Integer,String> securities_class_mnemonic = new HashMap<Integer, String>();
		Map<Integer,String> securities_class_description = new HashMap<Integer, String>();
		Map<Integer,String> currency = new HashMap<Integer, String>();
		Map<Integer,String> departament = new HashMap<Integer, String>();
		Map<Integer,String> municipality = new HashMap<Integer, String>();
		try {
			
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				securities_class_mnemonic.put(param.getParameterTablePk(), param.getText1());
				securities_class_description.put(param.getParameterTablePk(), param.getParameterName());
			}
			
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				currency.put(param.getParameterTablePk(), param.getText1());
			}
			
			geographicLocationTO.setGeographicLocationType(GeographicLocationType.DEPARTMENT.getCode());
			for(GeographicLocation geo : geographicLocationServiceBean.getListGeographicLocation(geographicLocationTO)) {
				departament.put(geo.getIdGeographicLocationPk(), geo.getName());
			}
			
			geographicLocationTO.setGeographicLocationType(GeographicLocationType.DISTRICT.getCode());
			for(GeographicLocation geo : geographicLocationServiceBean.getListGeographicLocation(geographicLocationTO)) {
				municipality.put(geo.getIdGeographicLocationPk(), geo.getName());
			}
			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		parametersRequired.put("idIssuerBC", idIssuerBC);
		parametersRequired.put("p_securities_class_mnemonic", securities_class_mnemonic);
		parametersRequired.put("p_securities_class_description", securities_class_description);
		parametersRequired.put("p_currency", currency);
		parametersRequired.put("p_departament", departament);
		parametersRequired.put("p_municipality", municipality);
		// TODO Auto-generated method stub
		return parametersRequired;
		}

}
