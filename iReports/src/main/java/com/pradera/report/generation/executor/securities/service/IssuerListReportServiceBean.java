package com.pradera.report.generation.executor.securities.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.report.generation.executor.securities.to.IssuerListTO;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class IssuerListReportServiceBean extends CrudDaoServiceBean {

	public IssuerListReportServiceBean() {
		// TODO Auto-generated constructor stub
	}

	public List<Object[]> getIssuerListByParameters(IssuerListTO issuerListTO) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" SELECT ");
		sbQuery.append(" 	I.ID_ISSUER_PK, ");
		sbQuery.append(" 	I.BUSINESS_NAME, ");
		sbQuery.append(" 	I.MNEMONIC, ");
		sbQuery.append(" 	I.ID_HOLDER_FK, ");
		sbQuery.append(" 	((SELECT PT.INDICATOR1 FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK = I.DOCUMENT_TYPE)||' - '||I.DOCUMENT_NUMBER) AS DOCUMENT, ");
		sbQuery.append(" 	(SELECT PT.TEXT1 FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK = I.ECONOMIC_SECTOR) ECONOMIC_SECTOR, ");
		sbQuery.append(" 	NVL((SELECT SUM(S.SHARE_CAPITAL) FROM SECURITY S WHERE S.SHARE_CAPITAL IS NOT NULL AND S.ID_ISSUER_FK = I.ID_ISSUER_PK GROUP BY S.ID_ISSUER_FK),0) SHARE_CAPITAL, ");
		sbQuery.append(" 	I.REGISTRY_DATE, ");
		sbQuery.append(" 	(SELECT PT.DESCRIPTION FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK = I.STATE_ISSUER) STATE_ISSUER");
		sbQuery.append(" FROM	");
		sbQuery.append("  	ISSUER I ");
		sbQuery.append(" WHERE  ");
		  
		if (Validations.validateIsNotNullAndNotEmpty(issuerListTO
				.getFinalDate())) {
			sbQuery.append(" I.REGISTRY_DATE<=:finalDate ");
		}
		if (Validations.validateIsNotNullAndNotEmpty(issuerListTO
				.getInitialDate())) {
			sbQuery.append(" AND I.REGISTRY_DATE>=:initialDate ");
		}
		if (Validations.validateIsNotNullAndNotEmpty(issuerListTO
				.getIdIssuerPk())) {
			sbQuery.append(" AND I.ID_ISSUER_PK=:idIssuerPk ");
		}
		if (Validations
				.validateIsNotNullAndNotEmpty(issuerListTO.getMnemonic())) {
			sbQuery.append(" AND I.MNEMONIC=:mnemonico ");
		}
		if (Validations.validateIsNotNullAndPositive(issuerListTO
				.getEconomicSector())) {
			sbQuery.append(" AND I.ECONOMIC_SECTOR=:economicSector ");
		}

		sbQuery.append(" ORDER BY ");
		sbQuery.append("  	I.ID_ISSUER_PK	");

		Query query = em.createNativeQuery(sbQuery.toString());

		if (Validations.validateIsNotNullAndNotEmpty(issuerListTO
				.getIdIssuerPk())) {
			query.setParameter("idIssuerPk", issuerListTO.getIdIssuerPk());
		}
		if (Validations
				.validateIsNotNullAndNotEmpty(issuerListTO.getMnemonic())) {
			query.setParameter("mnemonico", issuerListTO.getMnemonic());
		}
		if (Validations.validateIsNotNullAndPositive(issuerListTO
				.getEconomicSector())) {
			query.setParameter("economicSector",
					issuerListTO.getEconomicSector());
		}
		if (Validations.validateIsNotNullAndNotEmpty(issuerListTO
				.getInitialDate())) {
			query.setParameter("initialDate", issuerListTO.getInitialDate());
		}
		if (Validations.validateIsNotNullAndNotEmpty(issuerListTO
				.getFinalDate())) {
			query.setParameter("finalDate", issuerListTO.getFinalDate());
		}

		return query.getResultList();
	}
}
