package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.management.RuntimeErrorException;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.custody.service.DepositAndWithdrawalServiceBean;
import com.pradera.report.generation.executor.custody.to.GenericCustodyOperationTO;
import com.pradera.report.generation.executor.securities.service.IssuerListReportServiceBean;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "DepositAndWithdrawalPhysicalCertiReport")
public class DepositAndWithdrawalPhysicalCertiReport extends GenericReport{

	private static final long serialVersionUID = 1L;
	
	@Inject PraderaLogger log;
	@EJB private ParameterServiceBean parameterService;
	@EJB private ParticipantServiceBean participantServiceBean;
	@EJB private IssuerListReportServiceBean issuerServiceBean;
	@EJB private DepositAndWithdrawalServiceBean depositAndWithdrawalServiceBean;
	
	public DepositAndWithdrawalPhysicalCertiReport() { }
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> currency = new HashMap<Integer, String>();
		Map<Integer,String> securityClass = new HashMap<Integer, String>();
		Map<Integer,String> participantMnemonic = new HashMap<Integer, String>();
		Map<Integer,String> mnemonic_c = new HashMap<Integer, String>();
		
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		String holderAccountNumber = null;
		String securityClassDescription = null;
		String currencyDescription = null;
		String participantDescription = null;
		String issuerMnemonic = null;
		String operationTypeDescription = null;
		Date currentDate = new Date();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		GenericCustodyOperationTO to = new GenericCustodyOperationTO();
		
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.CUI_PARAM)){
				if(r.getFilterValue() != null){
					to.setHolder(r.getFilterValue());
				}
			}
			if (r.getFilterName().equals(ReportConstant.ACCOUNT_HOLDER)){
				if(r.getFilterValue() != null){
					to.setHolderAccount(r.getFilterValue());
					holderAccountNumber = r.getFilterDescription();
				}else{
					holderAccountNumber = "TODAS";
				}
			}
			if (r.getFilterName().equals(ReportConstant.KEY_VALUE)){
				if(r.getFilterValue() != null){
					to.setSecurities(r.getFilterValue());
				}
			}
			if (r.getFilterName().equals(ReportConstant.CLASS_SECURITY_PARAM)){
				if(r.getFilterValue() != null){
					to.setSecurityClass(r.getFilterValue());
					securityClassDescription = r.getFilterDescription();
				}else{
					securityClassDescription = "TODAS";
				}
			}
			if (r.getFilterName().equals(ReportConstant.CURRENCY)){
				if(r.getFilterValue() != null){
					to.setCurrency(r.getFilterValue());
					currencyDescription = r.getFilterDescription();
				}else{
					currencyDescription = "TODOS";
				}
			}
			if (r.getFilterName().equals(ReportConstant.PARTICIPANT_PARAM)){
				if(r.getFilterValue() != null){
					to.setParticipant(r.getFilterValue());
					participantDescription = r.getFilterDescription();
				}else{
					participantDescription = "TODOS";
				}
			}
			if (r.getFilterName().equals(ReportConstant.ISSUER_PARAM)){
				if(r.getFilterValue() != null){
					to.setIssuer(r.getFilterValue());
					issuerMnemonic = r.getFilterDescription();
				}else{
					issuerMnemonic = "TODOS";
				}
			}
			if (r.getFilterName().equals(ReportConstant.TYPE_OPERATION_PARAM)){
				if(r.getFilterValue() != null){
					to.setOperationType(r.getFilterValue());
					operationTypeDescription = r.getFilterDescription();
				}else{
					operationTypeDescription = "TODOS";
				}
			}
			if (r.getFilterName().equals(ReportConstant.DATE_INITIAL_PARAM)){
				if(r.getFilterValue() != null){
					to.setInitialDate(r.getFilterValue());
				}
			}
			if (r.getFilterName().equals(ReportConstant.DATE_END_PARAM)){
				if(r.getFilterValue() != null){
					to.setFinalDate(r.getFilterValue());
				}
			}
		}
		
		//VALIDAR TIPO DE CAMBIO A LA FECHA DE REGISTRO DE CADA OPERACION DE TITULOS FISICOS
		String dateForMessage = GeneralConstants.EMPTY_STRING;
		try{
			List<Date> listDate = depositAndWithdrawalServiceBean.getDateForExchangeRateValidation(to);
			for (Date date : listDate) {
				dateForMessage = CommonsUtilities.convertDatetoString(date);
				depositAndWithdrawalServiceBean.getExchangeRate(date);
			}
		} catch (ServiceException e) {
			if(e.getErrorService().equals(ErrorServiceType.NOT_EXCHANGE_RATE)){
				throw new RuntimeErrorException(null, ReportConstant.ERROR_EXCHANGE_RATE + " " + dateForMessage);
			}
			log.error(e.getMessage());
		}
		
		try {
			for(Participant participant : participantServiceBean.getLisParticipantServiceBean(new Participant())) {
				participantMnemonic.put(participant.getIdParticipantPk().intValue(), participant.getMnemonic());
			}

			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				currency.put(param.getParameterTablePk(), param.getParameterName());
				mnemonic_c.put(param.getParameterTablePk(), param.getText1());
			}
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				securityClass.put(param.getParameterTablePk(), param.getText1());
			}
			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		parametersRequired.put("holder_account_number", holderAccountNumber);
		parametersRequired.put("security_class_description", securityClassDescription);
		parametersRequired.put("currency_description", currencyDescription);
		parametersRequired.put("participant_description", participantDescription);
		parametersRequired.put("issuer_mnemonic", issuerMnemonic);
		parametersRequired.put("operation_type_description", operationTypeDescription);
		parametersRequired.put("p_currency", currency);
		parametersRequired.put("map_currency", mnemonic_c);
		parametersRequired.put("p_securityClass", securityClass);
		parametersRequired.put("fecha_actual", dateFormat.format(currentDate));
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		// TODO Auto-generated method stub
		return parametersRequired;
	}
}
