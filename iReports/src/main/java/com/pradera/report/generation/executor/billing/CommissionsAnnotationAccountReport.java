package com.pradera.report.generation.executor.billing;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.helperui.service.BillingServiceQueryServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.type.EntityCollectionType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.util.view.UtilReportConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CommissionsAnnotationAccountReport.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@ReportProcess(name = "CommissionsAnnotationAccountReport")
public class CommissionsAnnotationAccountReport extends GenericReport {
 
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The parameter service. */
	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	private BillingServiceQueryServiceBean billingServiceQueryServiceBean;
	
	@EJB
	private ParticipantServiceBean participantServiceBean;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	
	/**
	 * Instantiates a new commissions annotation account report.
	 */
	public CommissionsAnnotationAccountReport() {	
	}

	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#generateData(com.pradera.model.report.ReportLogger)
	 */
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
 
	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#addParametersQueryReport()
	 */
	@Override
	public void addParametersQueryReport() {

		
	}

	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#getCustomJasperParameters()
	 */
	@Override
	public Map<String, Object> getCustomJasperParameters() {

		Map<String, Object> parametersRequired = new HashMap<>();
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		String serviceCode=null;
		Long participantCode=null;
		Long cuiCode=null;
		Integer institutionType=null;
		BillingService billingService=null;
		String dateInitial=null;
		String dateEnd=null;
		for (int i = 0; i < listaLogger.size(); i++){
			if(listaLogger.get(i).getFilterName().equals("p_service_code")){
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
			    	serviceCode=listaLogger.get(i).getFilterValue();
			    }
			}else if(listaLogger.get(i).getFilterName().equals("p_participant_code")){
				if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					participantCode=Long.parseLong(listaLogger.get(i).getFilterValue());
				}
			}else if(listaLogger.get(i).getFilterName().equals("cui_code")){
				if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					cuiCode=Long.parseLong(listaLogger.get(i).getFilterValue());
				}
			}else if(listaLogger.get(i).getFilterName().equals("institution_type")){
				if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					institutionType=Integer.parseInt(listaLogger.get(i).getFilterValue());
				}
			}else if(listaLogger.get(i).getFilterName().equals("date_initial")){
				if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					dateInitial=listaLogger.get(i).getFilterValue().toString();
				}
			}else if(listaLogger.get(i).getFilterName().equals("date_end")){
				if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					dateEnd=listaLogger.get(i).getFilterValue().toString();
				}
			}
			
			
		}
		
		
		parametersRequired.put("date_initial", dateInitial);
		parametersRequired.put("date_end", dateEnd);
		
		/**
		 * Validar: Si es un participante Inversionista, y la entidad de cobro es emisor, buscar su 
		 * el emisor del participante y setearlo como entity Collection
		 */
		/**
		 * Hacer busqueda del servicio y traer sus caracteristicas(Entidad de cobro)
		 */
		String p_entity_code="";
		if(Validations.validateIsNotNullAndNotEmpty(serviceCode)){
			billingService=billingServiceQueryServiceBean.getBillingServiceByCode(serviceCode);
			if(EntityCollectionType.PARTICIPANTS.getCode().equals(billingService.getEntityCollection())){
				parametersRequired.put("p_entity_code", participantCode);
			}else if(EntityCollectionType.ISSUERS.getCode().equals(billingService.getEntityCollection())){
				if(InstitutionType.PARTICIPANT_INVESTOR.getCode().equals(institutionType)){
					p_entity_code=participantServiceBean.getIssuerIsParticipant(participantCode);
				}
				parametersRequired.put("p_entity_code", p_entity_code);
			}else if(EntityCollectionType.HOLDERS.getCode().equals(billingService.getEntityCollection())){
				parametersRequired.put("p_entity_code", cuiCode);
			}else if(EntityCollectionType.OTHERS.getCode().equals(billingService.getEntityCollection())){
				parametersRequired.put("p_entity_code", "");
			}
		}
		
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());

		return parametersRequired;
	}

}
