package com.pradera.report.generation.executor.custody.to;

import java.io.Serializable;

public class RegisterSecuritiesEntSysIsDirectTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// estos son los parametros del reporte
	private Long idHolderAccount;
	private Integer sourceOperation;
	private String initialDate;
	private String finalDate;
	private String securityCode;
	private Long cuiHolder;

	// constructor
	public RegisterSecuritiesEntSysIsDirectTO() {
	}

	// TODO methods getter and setter
	public Integer getSourceOperation() {
		return sourceOperation;
	}

	public Long getIdHolderAccount() {
		return idHolderAccount;
	}

	public void setIdHolderAccount(Long idHolderAccount) {
		this.idHolderAccount = idHolderAccount;
	}

	public void setSourceOperation(Integer sourceOperation) {
		this.sourceOperation = sourceOperation;
	}

	public String getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(String initialDate) {
		this.initialDate = initialDate;
	}

	public String getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(String finalDate) {
		this.finalDate = finalDate;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	public Long getCuiHolder() {
		return cuiHolder;
	}

	public void setCuiHolder(Long cuiHolder) {
		this.cuiHolder = cuiHolder;
	}
}