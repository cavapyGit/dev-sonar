package com.pradera.report.generation.executor.billing.to;

import java.io.Serializable;

/**
 * @author rlarico
 *
 */
public class ConsolidatedCollectionByEntityTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//estos son los parametrso del reporte 312 (POI)
	private String entityCollection;
	private String collectionState;
	private String idIssuer;
	private String idParticipant;
	private String idHolder;
	private String otherNumber;
	private String otherDescription;
	private String afp;
	private String initialDate;
	private String finalDate;
	private String serviceCode;
	
	// constructor
	public ConsolidatedCollectionByEntityTO (){}

	
	//TODO getter and setter
	public String getEntityCollection() {
		return entityCollection;
	}

	public void setEntityCollection(String entityCollection) {
		this.entityCollection = entityCollection;
	}

	public String getCollectionState() {
		return collectionState;
	}

	public void setCollectionState(String collectionState) {
		this.collectionState = collectionState;
	}

	public String getServiceCode() {
		return serviceCode;
	}


	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}


	public String getIdIssuer() {
		return idIssuer;
	}

	public String getOtherDescription() {
		return otherDescription;
	}


	public void setOtherDescription(String otherDescription) {
		this.otherDescription = otherDescription;
	}


	public void setIdIssuer(String idIssuer) {
		this.idIssuer = idIssuer;
	}

	public String getIdParticipant() {
		return idParticipant;
	}

	public void setIdParticipant(String idParticipant) {
		this.idParticipant = idParticipant;
	}

	public String getIdHolder() {
		return idHolder;
	}

	public void setIdHolder(String idHolder) {
		this.idHolder = idHolder;
	}

	public String getOtherNumber() {
		return otherNumber;
	}

	public void setOtherNumber(String otherNumber) {
		this.otherNumber = otherNumber;
	}

	public String getAfp() {
		return afp;
	}

	public void setAfp(String afp) {
		this.afp = afp;
	}

	public String getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(String initialDate) {
		this.initialDate = initialDate;
	}

	public String getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(String finalDate) {
		this.finalDate = finalDate;
	}
	
}
