package com.pradera.report.generation.executor.custody.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.corporatives.stockcalculation.type.StockType;
import com.pradera.model.custody.accreditationcertificates.AccreditationOperation;
import com.pradera.model.custody.accreditationcertificates.BlockRequest;
import com.pradera.model.custody.accreditationcertificates.DigitalSignature;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.custody.type.BlockEntityStateType;
import com.pradera.model.custody.type.ChangeOwnershipBalanceTypeType;
import com.pradera.model.custody.type.PhysicalCertificateOperationType;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.negotiation.type.SirtexOperationStateType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.account.service.AccountReportServiceBean;
import com.pradera.report.generation.executor.custody.to.AccreditationOperationTO;
import com.pradera.report.generation.executor.custody.to.AffectationTO;
import com.pradera.report.generation.executor.custody.to.AnnotationOperationTO;
import com.pradera.report.generation.executor.custody.to.BalanceTransferAvailableTO;
import com.pradera.report.generation.executor.custody.to.BlockagesUnlockTO;
import com.pradera.report.generation.executor.custody.to.ChangeOwnershipReportTO;
import com.pradera.report.generation.executor.custody.to.ClientPortafolioTO;
import com.pradera.report.generation.executor.custody.to.EntriesVoluntaryAccountIssuerTO;
import com.pradera.report.generation.executor.custody.to.ExpirationReportTO;
import com.pradera.report.generation.executor.custody.to.GenericCustodyOperationTO;
import com.pradera.report.generation.executor.custody.to.PhysicalCertificateTO;
import com.pradera.report.generation.executor.custody.to.RegisterSecuritiesEntSysIsDirectTO;
import com.pradera.report.generation.executor.custody.to.RelationshipBlockAndUnlockReportTO;
import com.pradera.report.generation.executor.custody.to.RequestAccreditationCertificationTO;
import com.pradera.report.generation.executor.custody.to.RetirementOperationTO;
import com.pradera.report.generation.executor.custody.to.SecurityTransferOperationTO;
import com.pradera.report.generation.executor.custody.to.SirtexOperationResultTO;
import com.pradera.report.generation.executor.custody.to.XmlCoupon;
import com.pradera.report.generation.executor.custody.to.XmlListSeccSirtex;
import com.pradera.report.generation.executor.custody.to.XmlOperationsSirtex;
import com.pradera.report.generation.executor.custody.to.XmlRegSirtex;
import com.pradera.report.generation.executor.custody.to.XmlSeccSirtex;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;
import com.pradera.report.generation.service.ReportManageServiceBean;

/**
 * <ul>
 * <li>
 * Project iDepositary. Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class AccountReportServiceBean.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 12/07/2013
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class CustodyReportServiceBean extends CrudDaoServiceBean {

	@Inject
	ReportManageServiceBean reportManageServiceBean;

	@EJB
	private AccountReportServiceBean accountReportService;
	
	private final String REPORTO_RF = "REPORTO RF";
	
	private final String COMPRA_VENTA_RF = "COMPRA/VENTA RF";
	
	private final String REPORTO_REVERSO_RF = "REPORTO REVERSO RF";
	
	/**
	 * Default constructor.
	 */
	public CustodyReportServiceBean() {
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getQueryListByClass(String strQuery) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(strQuery);
		Query query = em.createNativeQuery(sbQuery.toString());
		return query.getResultList();
	}
	
	/**
	 * issue 1189: query para el REPORTE REPORTE DE RELACION DE BLOQUEOS Y DESBLOQUEOS
	 * @param filter
	 * @return
	 */
	public String getQueryRelationshipBlockAndUnlock(RelationshipBlockAndUnlockReportTO filter){
		StringBuilder sd=new StringBuilder();
		
		sd.append(" select                                                                                                                                ");
		sd.append(" BR.REGISTRY_DATE FEC_REG,                                                                                 ");
//		sd.append(" CASE WHEN BO.BLOCK_LEVEL = 1119 THEN BR.REGISTRY_DATE                                                                                 ");
//		sd.append("      WHEN BO.BLOCK_LEVEL = 1120 THEN  (select UR_SUB.CONFIRMATION_DATE                                                                ");
//		sd.append("                                       from UNBLOCK_OPERATION UO_SUB                                                                   ");
//		sd.append("                                       inner join UNBLOCK_REQUEST UR_SUB ON UR_SUB.ID_UNBLOCK_REQUEST_PK = UO_SUB.ID_UNBLOCK_REQUEST_FK");
//		sd.append("                                       WHERE 1 = 1                                                                                     ");
//		sd.append("                                       AND UR_SUB.UNBLOCK_REQUEST_STATE = 1208                                                         ");
//		sd.append("                                       AND UO_SUB.ID_BLOCK_OPERATION_FK = BO.ID_BLOCK_OPERATION_PK                                     ");
//		sd.append("                                       )                                                                                               ");
//		sd.append("     END FEC_REG,                                                                                                                      ");
		
		sd.append(" PT_CLASS.TEXT1 CLAS_VAL,                                                                                                              ");
		sd.append(" SEC.ID_SECURITY_CODE_PK CLAVE_VAL,                                                                                                    ");
		sd.append(" P.MNEMONIC PART,                                                                                                                      ");

		sd.append(" (SELECT LISTAGG(HAD.ID_HOLDER_FK , ',')                                                                                               ");
		sd.append("     WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)  																			              ");
		sd.append("     FROM HOLDER_ACCOUNT_DETAIL HAD  																								  ");
		sd.append("     WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK  																		  ");			 
		sd.append(" ) CUI,                                                                                                                                ");

		sd.append(" HA.ACCOUNT_NUMBER CTA_TITULAR,                                                                                                        ");

		sd.append(" (SELECT LISTAGG((SELECT PT_SUB_H.INDICATOR1                                                                                           ");
		sd.append("                  from HOLDER H_SUB                                                                                                    ");
		sd.append("                  inner join PARAMETER_TABLE PT_SUB_H ON PT_SUB_H.PARAMETER_TABLE_PK = H_SUB.DOCUMENT_TYPE                             ");
		sd.append("                  WHERE H_SUB.ID_HOLDER_PK = HAD.ID_HOLDER_FK) , ',' )                                                                 ");
		sd.append(" WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)  																			                  ");
		sd.append(" FROM HOLDER_ACCOUNT_DETAIL HAD  																									  ");
		sd.append(" WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK  																			  ");
		sd.append(" ) TIP_DOC_CUI,                                                                                                                        ");

		sd.append(" (SELECT LISTAGG((SELECT H_SUB.DOCUMENT_NUMBER                                                                                         ");
		sd.append("                  from HOLDER H_SUB                                                                                                    ");
		sd.append("                  WHERE H_SUB.ID_HOLDER_PK = HAD.ID_HOLDER_FK) , ',' )                                                                 ");
		sd.append(" WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)  																			                  ");
		sd.append(" FROM HOLDER_ACCOUNT_DETAIL HAD  																									  ");
		sd.append(" WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK  																			  ");
		sd.append(" ) NUM_DOC_CUI,                                                                                                                        ");

		sd.append(" BE.FULL_NAME ENT_BLQ,                                                                                                                 ");
		sd.append(" PT_DOCT_BE.INDICATOR1 TIP_DOC_ENT_BLQ,                                                                                                ");
		sd.append(" BE.DOCUMENT_NUMBER DOC_NUM_ENT_BLQ,                                                                                                   ");
		sd.append(" PT_BTYP.PARAMETER_NAME TIP_BLQ,                                                                                                       ");
		sd.append(" BO.ORIGINAL_BLOCK_BALANCE CANT_BLQ_RBLQ,                                                                                              ");
		sd.append(" BO.ACTUAL_BLOCK_BALANCE SALD_CANT_BLQ_ACT,                                                                                            ");
		sd.append(" PT_LVL.PARAMETER_NAME BLQ_RBLQ,                                                                                                       ");
		sd.append(" PT_STATE.PARAMETER_NAME ESTADO,                                                                                                       ");
		sd.append(" SEC.ISSUANCE_DATE FEC_EMIS,                                                                                                           ");
		sd.append(" SEC.EXPIRATION_DATE FEC_VENC,                                                                                                         ");
		sd.append(" ISS.MNEMONIC EMISOR,                                                                                                                  ");
		sd.append(" BR.CONFIRMATION_USER USUARIO,                                                                                                         ");
		sd.append(" ubr.registry_date as FECHA_HORA_DESBLOQUEO ");

		sd.append(" From unblock_operation ubo ");
		sd.append(" right join block_operation bo On ubo.id_block_operation_fk=bo.id_block_operation_pk and ubo.unblock_state=1208                        ");
		sd.append(" inner join PARAMETER_TABLE PT_LVL       ON PT_LVL.PARAMETER_TABLE_PK = BO.BLOCK_LEVEL                                                 ");
		sd.append(" inner join PARAMETER_TABLE PT_STATE     ON PT_STATE.PARAMETER_TABLE_PK = BO.BLOCK_STATE                                               ");
		sd.append(" inner join BLOCK_REQUEST BR             ON BR.ID_BLOCK_REQUEST_PK = BO.ID_BLOCK_REQUEST_FK                                            ");
		sd.append(" inner join PARAMETER_TABLE PT_BTYP      ON PT_BTYP.PARAMETER_TABLE_PK = BR.BLOCK_TYPE                                                 ");
		sd.append(" inner join BLOCK_ENTITY BE              ON BE.ID_BLOCK_ENTITY_PK = BR.ID_BLOCK_ENTITY_FK                                              ");
		sd.append(" inner join PARAMETER_TABLE PT_DOCT_BE   ON PT_DOCT_BE.PARAMETER_TABLE_PK = BE.DOCUMENT_TYPE                                           ");
		sd.append(" inner join CUSTODY_OPERATION CO         ON CO.ID_CUSTODY_OPERATION_PK = BO.ID_BLOCK_OPERATION_PK                                      ");
		sd.append(" left join unblock_request ubr           On ubo.id_unblock_request_fk=ubr.id_unblock_request_pk ");
		sd.append(" inner join SECURITY SEC                 ON BO.ID_SECURITY_CODE_FK = SEC.ID_SECURITY_CODE_PK                                           ");
		sd.append(" inner join ISSUER ISS                   ON ISS.ID_ISSUER_PK = SEC.ID_ISSUER_FK                                                        ");
		sd.append(" inner join PARAMETER_TABLE PT_CLASS     ON PT_CLASS.PARAMETER_TABLE_PK = SEC.SECURITY_CLASS                                           ");
		sd.append(" inner join PARTICIPANT P                ON P.ID_PARTICIPANT_PK = BO.ID_PARTICIPANT_FK                                                 ");
		sd.append(" inner join HOLDER_ACCOUNT HA            ON HA.ID_HOLDER_ACCOUNT_PK = BO.ID_HOLDER_ACCOUNT_FK                                          ");
		sd.append(" where 1 = 1                                                                                                                           ");
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdParticipant())){
			sd.append(" and P.ID_PARTICIPANT_PK = :idParticipant");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdHolder())){
			sd.append(" AND (SELECT LISTAGG(HAD.ID_HOLDER_FK , ',')                 ");
			sd.append("     WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)  				");															 
			sd.append("     FROM HOLDER_ACCOUNT_DETAIL HAD  						");																										 
			sd.append("     WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK");																									 
			sd.append(" ) = :cui															");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getAccountNumber())){
			sd.append(" and HA.ACCOUNT_NUMBER = :accountNumber ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdIssuer())){
			sd.append(" and ISS.ID_ISSUER_PK = :idIssuer ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdSecurityCode())){
			sd.append(" and SEC.ID_SECURITY_CODE_PK = :idSecurityCode");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getSecurityClass())){
			sd.append(" and PT_CLASS.PARAMETER_TABLE_PK = :securityClass");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getLockType())){
			sd.append(" and PT_BTYP.PARAMETER_TABLE_PK = :lockType");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getLevelAffectation())){
			sd.append(" and PT_LVL.PARAMETER_TABLE_PK = :lvlAfectation");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getStateRequest())){
			sd.append(" and br.block_request_state = :state");
		}
		
		sd.append(" AND (TRUNC(BR.REGISTRY_DATE) BETWEEN to_date('"+filter.getInitialDate()+"','DD/MM/YYYY') AND to_date('"+filter.getFinalDate()+"','DD/MM/YYYY') ");
		sd.append(" or trunc(ubr.registry_date) between (TO_DATE('"+filter.getInitialDate()+"','dd/MM/yyyy')) and (TO_DATE('"+filter.getFinalDate()+"','dd/MM/yyyy')))");
		sd.append(" ORDER BY 1,2,3																														  ");
		
		
    	//reemplazando los parametros en la query
		String query=sd.toString();
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdParticipant())){
			query=query.replace(":idParticipant", filter.getIdParticipant());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdHolder())){
			query=query.replace(":cui", "'"+filter.getIdHolder()+"'");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getAccountNumber())){
			query=query.replace(":accountNumber", filter.getAccountNumber());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdIssuer())){
			query=query.replace(":idIssuer", "'"+filter.getIdIssuer()+"'");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdSecurityCode())){
			query=query.replace(":idSecurityCode", "'"+filter.getIdSecurityCode()+"'");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getSecurityClass())){
			query=query.replace(":securityClass", filter.getSecurityClass());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getLockType())){
			query=query.replace(":lockType", filter.getLockType());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getLevelAffectation())){
			query=query.replace(":lvlAfectation", filter.getLevelAffectation());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getStateRequest())){
			query=query.replace(":state", filter.getStateRequest());
		}
		
		return query;
	}
	
	/**
	 * issue 1053
	 * query para construir el reporte ACV Venta directa en formato EXCEL con apache POI
	 * @param registerSecuritiesEntSysIsDirectTO
	 * @return
	 */
	public String getQueryRegisterSecuritiesEntSysIsDirect(RegisterSecuritiesEntSysIsDirectTO registerSecuritiesEntSysIsDirectTO){
    	StringBuilder sbQuery= new StringBuilder();
    	
    	sbQuery.append(" SELECT                                                                     ");
    	sbQuery.append("   S.ID_SECURITY_CODE_PK,                                                   ");
    	sbQuery.append("   (SELECT PT.DESCRIPTION                                                   ");
    	sbQuery.append("   FROM PARAMETER_TABLE PT                                                  ");
    	sbQuery.append("   WHERE PT.PARAMETER_TABLE_PK = S.CURRENCY                                 ");
    	sbQuery.append("   ) CURRENCY,                                                              ");
    	sbQuery.append("   S.CURRENT_NOMINAL_VALUE,                                                 ");
    	sbQuery.append("   TO_CHAR(S.ISSUANCE_DATE,'DD/MM/YYYY') ISSUANCE_DATE,                     ");
    	sbQuery.append("   TO_CHAR(S.EXPIRATION_DATE,'DD/MM/YYYY') EXPIRATION_DATE,                 ");
    	sbQuery.append("   S.INTEREST_RATE,                                                         ");
    	sbQuery.append("   I.MNEMONIC,                                                              ");
    	sbQuery.append("   (SELECT GL.NAME                                                          ");
    	sbQuery.append("   FROM GEOGRAPHIC_LOCATION GL                                              ");
    	sbQuery.append("   WHERE GL.ID_GEOGRAPHIC_LOCATION_PK = AAO.DEPARTMENT                      ");
    	sbQuery.append("   ) AS DEPARTAMENT,                                                        ");
    	sbQuery.append("   (SELECT GL.NAME                                                          ");
    	sbQuery.append("   FROM GEOGRAPHIC_LOCATION GL                                              ");
    	sbQuery.append("   WHERE GL.ID_GEOGRAPHIC_LOCATION_PK = AAO.MUNICIPALITY                    ");
    	sbQuery.append("   ) AS LOCALITY,                                                           ");
    	sbQuery.append("   HA.ACCOUNT_NUMBER,                                                       ");
    	//aca se aplica la funcion LISTAGG debido a que hay cuentas mancomunadas
    	sbQuery.append(" (SELECT LISTAGG((select h.ID_HOLDER_PK from holder h where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK), ', ') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)                                                                                    ");
    	sbQuery.append("  FROM HOLDER_ACCOUNT_DETAIL HAD                                                                                                                                                                                                ");
    	sbQuery.append("  WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ) CUI,                                                                                                                                                               ");
    	sbQuery.append(" (SELECT LISTAGG((select h.full_name from holder h where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK), ', ') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)                                                                                       ");
    	sbQuery.append("  FROM HOLDER_ACCOUNT_DETAIL HAD                                                                                                                                                                                                ");
    	sbQuery.append("  WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ) DESCRIPTION,                                                                                                                                                       ");
    	sbQuery.append(" (SELECT LISTAGG((select CASE WHEN H.ind_residence = 1 THEN 'RE' ELSE 'NR' END from holder h where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK), ', ') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)                                             ");
    	sbQuery.append("  FROM HOLDER_ACCOUNT_DETAIL HAD                                                                                                                                                                                                ");
    	sbQuery.append("  WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ) IS_RESIDENT,                                                                                                                                                       ");
    	sbQuery.append(" (SELECT LISTAGG((select (SELECT PT.indicator1 FROM   parameter_table PT WHERE  PT.parameter_table_pk = H.document_type) from holder h where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK), ', ') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)   ");
    	sbQuery.append("  FROM HOLDER_ACCOUNT_DETAIL HAD                                                                                                                                                                                                ");
    	sbQuery.append("  WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ) DOCUMENT_TYPE,                                                                                                                                                     ");
    	sbQuery.append(" (SELECT LISTAGG((select H.document_number from holder h where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK), ', ') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)                                                                                 ");
    	sbQuery.append("  FROM HOLDER_ACCOUNT_DETAIL HAD                                                                                                                                                                                                ");
    	sbQuery.append("  WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ) NUM_DOCUMENT,																																						");
    	
    	sbQuery.append("   AAO.TOTAL_BALANCE,                                                       ");
    	sbQuery.append("   AAO.TOTAL_BALANCE * S.CURRENT_NOMINAL_VALUE NOMINAL_TOTAL                ");
    	sbQuery.append(" FROM ACCOUNT_ANNOTATION_OPERATION AAO                                      ");
    	sbQuery.append(" INNER JOIN CUSTODY_OPERATION CO                                            ");
    	sbQuery.append(" ON CO.ID_CUSTODY_OPERATION_PK = AAO.ID_ANNOTATION_OPERATION_PK             ");
    	sbQuery.append(" INNER JOIN SECURITY S                                                      ");
    	sbQuery.append(" ON AAO.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK                         ");
    	sbQuery.append(" INNER JOIN ISSUER I                                                        ");
    	sbQuery.append(" ON I.ID_ISSUER_PK = S.ID_ISSUER_FK                                         ");
    	sbQuery.append(" INNER JOIN HOLDER_ACCOUNT HA                                               ");
    	sbQuery.append(" ON HA.ID_HOLDER_ACCOUNT_PK = AAO.ID_HOLDER_ACCOUNT_FK                      ");
    	
    	sbQuery.append(" WHERE AAO.STATE_ANNOTATION = 858                                                       ");
    	sbQuery.append("   AND S.SECURITY_CLASS = CASE                                                          ");
    	sbQuery.append("       WHEN "+registerSecuritiesEntSysIsDirectTO.getSourceOperation()+" = 2 THEN 414                                                  ");
    	sbQuery.append("       ELSE 415                                                                         ");
    	sbQuery.append("     END                                                                                ");
    	sbQuery.append(" AND TRUNC(CO.OPERATION_DATE) BETWEEN to_date('"+registerSecuritiesEntSysIsDirectTO.getInitialDate()+"','dd/mm/yyyy')    ");
    	sbQuery.append("	AND to_date('"+registerSecuritiesEntSysIsDirectTO.getFinalDate()+"','dd/mm/yyyy')                                  ");
    	sbQuery.append("   AND AAO.ID_ANNOTATION_OPERATION_PK not between 328294 and 329043                     ");
    	
    	if(Validations.validateIsNotNullAndNotEmpty(registerSecuritiesEntSysIsDirectTO.getSecurityCode()))
    		sbQuery.append("   AND (S.ID_SECURITY_CODE_PK = :security_code )         ");
    	
    	if(Validations.validateIsNotNullAndNotEmpty(registerSecuritiesEntSysIsDirectTO.getCuiHolder())){
    		sbQuery.append("   AND (                                                         ");
        	sbQuery.append("       (EXISTS                                                                          ");
        	sbQuery.append("         (SELECT had.ID_HOLDER_FK                                                       ");
        	sbQuery.append("          FROM HOLDER_ACCOUNT_DETAIL had                                                ");
        	sbQuery.append("          WHERE had.ID_HOLDER_FK = :cui_code                                         ");
        	sbQuery.append("            AND had.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK                      ");
        	sbQuery.append("   )))                                                                                  ");
    	}
    	
    	
    	if(Validations.validateIsNotNullAndNotEmpty(registerSecuritiesEntSysIsDirectTO.getIdHolderAccount()))
    		sbQuery.append("   AND (HA.ID_HOLDER_ACCOUNT_PK = :holder_account )     ");
    	
    	sbQuery.append(" ORDER BY                                                                               ");
    	sbQuery.append("   S.ID_SECURITY_CODE_PK,                                                               ");
    	sbQuery.append("   AAO.DEPARTMENT,                                                                      ");
    	sbQuery.append("   AAO.MUNICIPALITY,                                                                    ");
    	sbQuery.append("   HA.ID_HOLDER_ACCOUNT_PK																");
		
		String strQueryFormatted = sbQuery.toString();
		
		if(Validations.validateIsNotNullAndNotEmpty(registerSecuritiesEntSysIsDirectTO.getSecurityCode())){
			strQueryFormatted = strQueryFormatted.replace(":security_code", "'"+registerSecuritiesEntSysIsDirectTO.getSecurityCode()+"'");
		}
		if(Validations.validateIsNotNullAndNotEmpty(registerSecuritiesEntSysIsDirectTO.getCuiHolder())){
			strQueryFormatted = strQueryFormatted.replace(":cui_code", registerSecuritiesEntSysIsDirectTO.getCuiHolder().toString());
		}
		if(Validations.validateIsNotNullAndNotEmpty(registerSecuritiesEntSysIsDirectTO.getIdHolderAccount())){
			strQueryFormatted = strQueryFormatted.replace(":holder_account", registerSecuritiesEntSysIsDirectTO.getIdHolderAccount().toString());
		}
		
    	return strQueryFormatted;
	}

	/**
	 * Return Transfer Availables Operation's
	 * ---------------------------------------- IN SecurityTransferOperationTO
	 * OUT Lis<Object[]>
	 * */

	public List<Object[]> getTransferAvailable(SecurityTransferOperationTO secTo) {

		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append("  select "
				+ "	    co.operation_number operation_number, "
				+ "	    co.operation_date, "
				+ "	    sto.transfer_type, "
				+ "	    (select description from participant  where id_participant_pk = sto.id_source_participant_fk) filter_participant_desc, "
				+ "	    (select HA.account_number  from holder_account ha  where HA.id_holder_account_pk = sto.id_source_holder_account_fk) source_account, "
				+ "	    (select (mnemonic||'-'||id_participant_pk)  from participant   where id_participant_pk = sto.id_target_participant_fk) counterpart_participant_desc, "
				+ "	    (select HA.account_number from holder_account ha where HA.id_holder_account_pk = decode(sto.transfer_type,196,sto.id_target_holder_account_fk,0)) target_account, "
				+ "	    sto.id_isin_code_fk, "
				+ "	    sto.quantity_operation, "
				+ "	    sto.state, "
				+ "	    FN_GET_HOLDER_NAME(sto.id_source_holder_account_fk) source_account_description, "
				+ "	    FN_GET_HOLDER_NAME(sto.id_target_holder_account_fk) target_account_description, "
				+ "	    sto.last_modify_date ");
		sbQuery.append("	from "
				+ "	    security_transfer_operation sto "
				+ "	inner join "
				+ "	    custody_operation co "
				+ "	        on sto.ID_TRANSFER_OPERATION_PK = co.ID_CUSTODY_OPERATION_PK "
				+ "	where "
				+ "	    sto.IND_BLOCK_TRANSFER = 0 "
				+ "	    and not(sto.transfer_type = 196 and sto.state in(166,747))  "); // --Destino
																						// origen

		/* where's */
		if (Validations
				.validateIsNotNullAndPositive(secTo.getOperationNumber()))
			sbQuery.append(" and co.operation_number = :operationNumber ");

		if (Validations.validateIsNotNull(secTo.getOperationInitDate()))
			sbQuery.append(" and co.operation_date >= :initDate ");

		if (Validations.validateIsNotNull(secTo.getOperationEndDate()))
			sbQuery.append(" and co.operation_date <= :endDate ");

		if (Validations.validateIsNotNullAndPositive(secTo.getTransferType()))
			sbQuery.append(" and sto.transfer_type = :transferType ");

		if (Validations.validateIsNotNull(secTo.getIdIsinCodePk()))
			sbQuery.append(" and sto.id_isin_code_fk = :isinCode ");

		if (Validations.validateIsNotNullAndPositive(secTo.getState()))
			sbQuery.append(" and sto.state = :state ");

		if (Validations.validateIsNotNullAndPositive(secTo
				.getIdHolderAccountPk()))
			sbQuery.append(" and sto.id_source_holder_account_fk = :accountPk ");

		if (Validations
				.validateIsNotNullAndPositive(secTo.getIdParticipantPk()))
			sbQuery.append(" and sto.id_source_participant_fk = :participantPk ");
		// sbQuery.append(" ) ");

		sbQuery.append(" union all ");

		sbQuery.append("  select "
				+ "	    co.operation_number operation_number, "
				+ "	    co.operation_date, "
				+ "	    sto.transfer_type, "
				+ "	    (select description  from participant   where id_participant_pk = sto.id_target_participant_fk) filter_participant_desc, "
				+ "	    (select HA.account_number from holder_account ha where HA.id_holder_account_pk = decode(sto.transfer_type,196,sto.id_source_holder_account_fk,0)) source_account, "
				+ "	    (select (mnemonic||'-'||id_participant_pk) from participant  where id_participant_pk = sto.id_source_participant_fk) counterpart_participant_desc, "
				+ "	    (select HA.account_number from holder_account ha  where HA.id_holder_account_pk = sto.id_target_holder_account_fk) target_account, "
				+ "	    sto.id_isin_code_fk, "
				+ "	    sto.quantity_operation, "
				+ "	    sto.state, "
				+ "	    FN_GET_HOLDER_NAME(sto.id_source_holder_account_fk) source_account_description, "
				+ "	    FN_GET_HOLDER_NAME(sto.id_target_holder_account_fk) target_account_description, "
				+ "	    sto.last_modify_date ");
		sbQuery.append("	from "
				+ "	    security_transfer_operation sto "
				+ "	inner join "
				+ "	    custody_operation co "
				+ "	        on sto.ID_TRANSFER_OPERATION_PK = co.ID_CUSTODY_OPERATION_PK "
				+ "	where "
				+ "	    sto.IND_BLOCK_TRANSFER = 0 "
				+ "	    and not(sto.transfer_type = 525 and sto.state in(166,747))  ");
		/* where's */
		if (Validations
				.validateIsNotNullAndPositive(secTo.getOperationNumber()))
			sbQuery.append(" and co.operation_number = :operationNumber ");

		if (Validations.validateIsNotNull(secTo.getOperationInitDate()))
			sbQuery.append(" and co.operation_date >= :initDate ");

		if (Validations.validateIsNotNull(secTo.getOperationEndDate()))
			sbQuery.append(" and co.operation_date <= :endDate ");

		if (Validations.validateIsNotNullAndPositive(secTo.getTransferType()))
			sbQuery.append(" and sto.transfer_type = :transferType ");

		if (Validations.validateIsNotNull(secTo.getIdIsinCodePk()))
			sbQuery.append(" and sto.id_isin_code_fk = :isinCode ");

		if (Validations.validateIsNotNullAndPositive(secTo.getState()))
			sbQuery.append(" and sto.state = :state ");

		if (Validations.validateIsNotNullAndPositive(secTo
				.getIdHolderAccountPk()))
			sbQuery.append(" and sto.id_target_holder_account_fk = :accountPk ");

		if (Validations
				.validateIsNotNullAndPositive(secTo.getIdParticipantPk()))
			sbQuery.append(" and sto.id_target_participant_fk = :participantPk ");
		// sbQuery.append(" ) ");

		/* End where's */
		sbQuery.append("	Order By filter_participant_desc, operation_number ");

		Query query = em.createNativeQuery(sbQuery.toString());

		/* Parameters */
		if (Validations
				.validateIsNotNullAndPositive(secTo.getOperationNumber()))
			query.setParameter("operationNumber", secTo.getOperationNumber());

		if (Validations
				.validateIsNotNullAndPositive(secTo.getIdParticipantPk()))
			query.setParameter("participantPk", secTo.getIdParticipantPk());

		if (Validations.validateIsNotNullAndPositive(secTo
				.getIdHolderAccountPk()))
			query.setParameter("accountPk", secTo.getIdHolderAccountPk());

		if (Validations.validateIsNotNull(secTo.getOperationInitDate()))
			query.setParameter("initDate", secTo.getOperationInitDate());

		if (Validations.validateIsNotNull(secTo.getOperationEndDate()))
			query.setParameter("endDate", secTo.getOperationEndDate());

		if (Validations.validateIsNotNull(secTo.getTransferType()))
			query.setParameter("transferType", secTo.getTransferType());

		if (Validations.validateIsNotNull(secTo.getIdIsinCodePk()))
			query.setParameter("isinCode", secTo.getIdIsinCodePk());

		if (Validations.validateIsNotNull(secTo.getState()))
			query.setParameter("state", secTo.getState());

		/* End parameters */
		return query.getResultList();
	}

	/**
    * 
    * */
	public List<Object[]> getPhysicalCertificates(
			PhysicalCertificateTO physicalCertTo) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append("	select "
				+ "	distinct iss.MNEMONIC || ' - ' || iss.ID_ISSUER_PK AS EMISOR, "
				+ "	part.mnemonic || ' - ' || part.ID_PARTICIPANT_PK AS PARTICIPANTE, "
				+ "	HA.ACCOUNT_NUMBER AS CUENTA, "
				+ "	(case when hh.HOLDER_TYPE = 394  "
				+ "   then hh.id_holder_pk || ' - ' || hh.FULL_NAME "
				+ "   else hh.id_holder_pk || ' - ' ||hh.Name||' '||hh.FIRST_LAST_NAME||' '||hh.SECOND_LAST_NAME end) AS TITULAR, "
				+ "	pc.CERTIFICATE_NUMBER AS NO_TITULO, "
				+ "	decode(insType.PARAMETER_NAME,null,'N.A',insType.PARAMETER_NAME) AS INSTRUMENTO, "
				+ "	to_char(pbd.DEPOSIT_DATE,'dd/mm/yyyy') AS FECHA_DEPOSITO, "
				+ "	to_char(pbd.RETIREMENT_DATE,'dd/mm/yyyy') AS FECHA_RETIRO, "
				+ "	decode(curr.DESCRIPTION,null,'N.A',curr.DESCRIPTION) AS MONEDA, "
				+ "	pc.NOMINAL_VALUE AS VALOR_NOMINAL, "
				+ "	decode(situ.PARAMETER_NAME,null,'N.A',situ.PARAMETER_NAME) SITUACION, "
				+ "	decode(stat.PARAMETER_NAME,null,'N.A',stat.PARAMETER_NAME) ESTADO, "
				+ "	iss.ID_ISSUER_PK,"
				+ "	pbd.DEPOSIT_DATE,"
				+ "	part.ID_PARTICIPANT_PK, "
				+ "	HA.ACCOUNT_NUMBER  ,"
				+ "   (SELECT PARAMETER_NAME FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK=pc.SECURITY_CLASS) SECURITY_CLASS, "
				+ "    hh.id_holder_pk");

		sbQuery.append(" from  " + "	PHYSICAL_BALANCE_DETAIL pbd,  "
				+ "	PHYSICAL_CERTIFICATE pc,  "
				+ "	PHYSICAL_OPERATION_DETAIL pod, "
				+ "	PHYSICAL_CERTIFICATE_OPERATION pco, " + "	ISSUER iss, "
				+ "	PARTICIPANT part, " + "	HOLDER_ACCOUNT_DETAIL had, "
				+ "	HOLDER_ACCOUNT ha, " + "	HOLDER hh, "
				+ "	PARAMETER_TABLE insType, " + "	PARAMETER_TABLE curr,  "
				+ "	PARAMETER_TABLE situ, " + "	PARAMETER_TABLE stat");
		sbQuery.append(" where "
				+ "	pbd.ID_PHYSICAL_CERTIFICATE_FK = pc.ID_PHYSICAL_CERTIFICATE_PK and "
				+ "	not exists(select 1 from PHYSICAL_OPERATION_DETAIL pp where pp.ID_PHYSICAL_OPERATION_FK = pc.ID_PHYSICAL_CERTIFICATE_PK) and "
				+ "	pbd.ID_PARTICIPANT_FK = part.ID_PARTICIPANT_PK and "
				+ "	pod.ID_PHYSICAL_CERTIFICATE_FK = pc.ID_PHYSICAL_CERTIFICATE_PK and "
				+ "	pod.ID_PHYSICAL_OPERATION_FK = pco.ID_PHYSICAL_OPERATION_PK  and "
				+ "	pbd.ID_HOLDER_ACCOUNT_FK = had.ID_HOLDER_ACCOUNT_FK and "
				+ "	had.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK and "
				+ "	hh.ID_HOLDER_PK = had.ID_HOLDER_FK and "
				+ "	pc.ID_ISSUER_FK = iss.ID_ISSUER_PK and "
				+ "	pc.CURRENCY = curr.PARAMETER_TABLE_PK and "
				+ "	pc.SITUATION = situ.PARAMETER_TABLE_PK and "
				+ "	pbd.STATE = stat.PARAMETER_TABLE_PK and "
				+ "	insType.PARAMETER_TABLE_PK = pc.INSTRUMENT_TYPE ");

		if (Validations.validateIsNotNull(physicalCertTo.getIdIssuerPk()))
			sbQuery.append("	and pc.ID_ISSUER_FK = :issuerPk ");

		if (Validations.validateIsNotNullAndPositive(physicalCertTo
				.getIdParticipantPk()))
			sbQuery.append(" and pbd.ID_PARTICIPANT_FK = :participantPk ");

		if (Validations.validateIsNotNullAndPositive(physicalCertTo
				.getInstrumentType()))
			sbQuery.append(" and pc.INSTRUMENT_TYPE = :instrumentType ");

		if (Validations.validateIsNotNullAndPositive(physicalCertTo.getState()))
			sbQuery.append(" and pc.STATE = :state ");

		if (Validations.validateIsNotNull(physicalCertTo.getInitDate()))
			sbQuery.append(" and TRUNC(pbd.DEPOSIT_DATE) >= TRUNC(:initDate) ");

		if (Validations.validateIsNotNull(physicalCertTo.getEndDate()))
			sbQuery.append(" and TRUNC(pbd.DEPOSIT_DATE) <= TRUNC(:endDate) ");

		sbQuery.append("	order by iss.ID_ISSUER_PK,pbd.DEPOSIT_DATE,part.ID_PARTICIPANT_PK,HA.ACCOUNT_NUMBER");

		Query query = em.createNativeQuery(sbQuery.toString());

		/* Parameters */

		if (Validations.validateIsNotNull(physicalCertTo.getIdIssuerPk()))
			query.setParameter("issuerPk", physicalCertTo.getIdIssuerPk());

		if (Validations.validateIsNotNullAndPositive(physicalCertTo
				.getIdParticipantPk()))
			query.setParameter("participantPk",
					physicalCertTo.getIdParticipantPk());

		if (Validations.validateIsNotNullAndPositive(physicalCertTo
				.getInstrumentType()))
			query.setParameter("instrumentType",
					physicalCertTo.getInstrumentType());

		if (Validations.validateIsNotNullAndPositive(physicalCertTo.getState()))
			query.setParameter("state", physicalCertTo.getState());

		if (Validations.validateIsNotNull(physicalCertTo.getInitDate()))
			query.setParameter("initDate", physicalCertTo.getInitDate());

		if (Validations.validateIsNotNull(physicalCertTo.getEndDate()))
			query.setParameter("endDate", physicalCertTo.getEndDate());

		if (Validations.validateIsNotNullAndPositive(physicalCertTo.getState()))
			query.setParameter("state", physicalCertTo.getState());

		return query.getResultList();
	}

	/**
	 * Return Transfer Block Operation's --------------------------------- IN
	 * SecurityTransferOperationTO OUT Lis<Object[]>
	 * */
	public List<Object[]> getTransferBlock(SecurityTransferOperationTO secTo) {
		StringBuilder sbQuery = new StringBuilder();

		/* Select's */
		sbQuery.append("select "
				+ " co.OPERATION_NUMBER,"
				+ // 0
				" co.OPERATION_DATE,"
				+ // 1
				" sto.transfer_type,"
				+ // 2
				" br.BLOCK_TYPE,"
				+ // 3
				" (select pa.MNEMONIC from Participant pa where pa.ID_PARTICIPANT_PK=sto.ID_SOURCE_PARTICIPANT_FK) as part_source_description,"
				+ // 4
				" (select HA.account_number from holder_account ha where HA.id_holder_account_pk=sto.ID_SOURCE_HOLDER_ACCOUNT_FK) as source_account_number,"
				+ // 5
				" (select pa.MNEMONIC from Participant pa where pa.ID_PARTICIPANT_PK=sto.ID_TARGET_PARTICIPANT_FK) as part_target_description,"
				+ // 6
				" (select HA.account_number from holder_account ha where HA.id_holder_account_pk=sto.ID_TARGET_HOLDER_ACCOUNT_FK) as target_account_number,"
				+ // 7
				" sto.ID_ISIN_CODE_FK,"
				+ // 8
				" sto.QUANTITY_OPERATION,"
				+ // 9
				" sto.STATE, "
				+ // 10
				" FN_GET_HOLDER_NAME(sto.ID_SOURCE_HOLDER_ACCOUNT_FK) source_account_description, "
				+ // 11
				" FN_GET_HOLDER_NAME(sto.ID_TARGET_HOLDER_ACCOUNT_FK) target_account_description, "
				+ // 12
				" bo.document_number, " + // 13
				" bo.block_level, " + // 14
				" bot.original_block_balance, " + // 15
				" sto.last_modify_date"); // 16

		/* From, joins */
		sbQuery.append(" from "
				+ "       SECURITY_TRANSFER_OPERATION sto"
				+ "   inner join"
				+ "       CUSTODY_OPERATION co"
				+ "           on sto.ID_TRANSFER_OPERATION_PK=co.ID_CUSTODY_OPERATION_PK"
				+ "   inner join"
				+ "       BLOCK_OPERATION_TRANSFER bot"
				+ "           on bot.ID_TRANSFER_OPERATION_FK=sto.ID_TRANSFER_OPERATION_PK"
				+ "   inner join"
				+ "       BLOCK_OPERATION bo"
				+ "           on bot.ID_BLOCK_OPERATION_FK=bo.ID_BLOCK_OPERATION_PK"
				+ "   inner join" + "       BLOCK_REQUEST br"
				+ "           on bo.ID_BLOCK_REQUEST_FK=br.ID_BLOCK_REQUEST_PK");
		/* where's */
		sbQuery.append(" WHERE 1=1 ");
		if (Validations
				.validateIsNotNullAndPositive(secTo.getOperationNumber())) {
			sbQuery.append(" and co.operation_number = :operationNumber ");
		}

		if (Validations.validateIsNotNull(secTo.getOperationInitDate())) {
			sbQuery.append(" and co.operation_date >= :initDate ");
		}

		if (Validations.validateIsNotNull(secTo.getOperationEndDate())) {
			sbQuery.append(" and co.operation_date <= :endDate ");
		}

		if (Validations.validateIsNotNullAndPositive(secTo.getBlockType())) {
			sbQuery.append(" and br.block_Type = :blockTypePrm ");
		}

		if (Validations.validateIsNotNullAndPositive(secTo.getTransferType())) {
			sbQuery.append(" and sto.transfer_type = :transferType ");
		}

		if (Validations.validateIsNotNull(secTo.getIdIsinCodePk())) {
			sbQuery.append(" and sto.id_isin_code_fk = :isinCode ");
		}

		if (Validations.validateIsNotNullAndPositive(secTo.getState())) {
			sbQuery.append(" and sto.state = :state ");
		}

		if (Validations.validateIsNotNullAndPositive(secTo
				.getIdHolderAccountPk())) {
			sbQuery.append(" and ( "
					+ "				   sto.id_source_holder_account_fk = :accountPk "
					+ "				   or sto.id_target_holder_account_fk = :accountPk "
					+ "			 )");
		}

		if (Validations
				.validateIsNotNullAndPositive(secTo.getIdParticipantPk())) {
			sbQuery.append(" and ( "
					+ "          sto.id_target_participant_fk = :participantPk "
					+ "          or sto.id_source_participant_fk = :participantPk "
					+ "      )");
		}
		/* End where's */

		sbQuery.append("  Order By "
				+ "      part_source_description, co.operation_number ");

		Query query = em.createNativeQuery(sbQuery.toString());

		/* Parameters */
		if (Validations
				.validateIsNotNullAndPositive(secTo.getOperationNumber())) {
			query.setParameter("operationNumber", secTo.getOperationNumber());
		}

		if (Validations
				.validateIsNotNullAndPositive(secTo.getIdParticipantPk())) {
			query.setParameter("participantPk", secTo.getIdParticipantPk());
		}

		if (Validations.validateIsNotNullAndPositive(secTo
				.getIdHolderAccountPk())) {
			query.setParameter("accountPk", secTo.getIdHolderAccountPk());
		}

		if (Validations.validateIsNotNull(secTo.getOperationInitDate())) {
			query.setParameter("initDate", secTo.getOperationInitDate());
		}

		if (Validations.validateIsNotNull(secTo.getOperationEndDate())) {
			query.setParameter("endDate", secTo.getOperationEndDate());
		}

		if (Validations.validateIsNotNull(secTo.getTransferType())) {
			query.setParameter("transferType", secTo.getTransferType());
		}

		if (Validations.validateIsNotNullAndPositive(secTo.getBlockType())) {
			query.setParameter("blockTypePrm", secTo.getBlockType());
		}

		if (Validations.validateIsNotNull(secTo.getIdIsinCodePk())) {
			query.setParameter("isinCode", secTo.getIdIsinCodePk());
		}

		if (Validations.validateIsNotNull(secTo.getState())) {
			query.setParameter("state", secTo.getState());
		}
		/* End parameters */

		return query.getResultList();
	}

	public List<Object[]> getListRequestAffectation(
			AffectationTO searchRequestAffectationTO) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> mapParam = new HashMap<String, Object>();
		sbQuery.append("	SELECT BR.idBlockRequestPk, "
				+ // 0 nro solicitud de bloqueo
				"	BR.registryDate, "
				+ // 1 fecha
				"	BR.holder.fullName, "
				+ // 2 holder name
				"	BE.fullName, "
				+ // 3 nombre entidad de bloqueo
				"	BR.blockType,"
				+ // 4 tipo de bloqueo
				"	BR.blockRequestState,"
				+ // 5 estado
				"	BR.blockNumber, "
				+ // 6 nro de acto
				"	BR.blockNumberDate, "
				+ // 7 fecha de Acto
				"	BR.lastModifyDate, "
				+ // 8 fecha ultima modificacion
				"	BO.documentNumber, "
				+ // 9 documento de bloqueo
				"	BO.blockLevel, "
				+ // 10 bloqueo/rebloqueo
				"	BO.securities.idIsinCodePk, "
				+ // 11 isin
				"	(BO.participant.idParticipantPk||' - '||BO.participant.mnemonic), "
				+ // 12 participante
				"	BO.originalBlockBalance,	"
				+ // 13 monto
				"	BO.holderAccount.accountNumber, "
				+ // 14 account number
				"   BR.holder.idHolderPk "
				+ // 15 RNT
				"	FROM BlockOperation BO," + "		 BlockRequest BR, "
				+ "		 BlockEntity BE "
				+ "	WHERE BO.blockRequest = BR.idBlockRequestPk "
				+ "	AND BE.idBlockEntityPk = BR.blockEntity.idBlockEntityPk "
				+ "  	AND BE.stateBlockEntity = "
				+ BlockEntityStateType.ACTIVATED.getCode()
				+ "	AND BR.holder.stateHolder IN ("
				+ HolderStateType.REGISTERED.getCode() + ","
				+ HolderStateType.BLOCKED.getCode() + ")");

		if (Validations.validateIsNotNullAndPositive(searchRequestAffectationTO
				.getBlockRequestState()))
			sbQuery.append("	AND BR.blockRequestState = :state");

		if (Validations.validateIsNotNullAndPositive(searchRequestAffectationTO
				.getBlockType()))
			sbQuery.append("	AND BR.blockType = :blockType");

		if (Validations.validateIsNotNullAndPositive(searchRequestAffectationTO
				.getIdHolderPk()))
			sbQuery.append("	AND BR.holder.idHolderPk = :idHolderPk");

		if (Validations.validateIsNotNullAndPositive(searchRequestAffectationTO
				.getIdBlockEntityPk()))
			sbQuery.append("	AND BR.blockEntity.idBlockEntityPk = :idBlockEntityPk");

		if (Validations.validateIsNotNull(searchRequestAffectationTO
				.getBlockNumber()))
			sbQuery.append("	AND BR.blockNumber = :numberRecord ");

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getIdBlockRequestPk()))
			sbQuery.append("	AND BR.idBlockRequestPk = :idBlockRequestPk ");

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getInitialDate()))
			sbQuery.append("	AND BR.registryDate >= :initDate ");

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getEndDate()))
			sbQuery.append("	AND BR.registryDate <= :endDate ");

		sbQuery.append("	ORDER BY BR.idBlockRequestPk DESC");
		/*****************************************************************************************************************/

		Query query = em.createQuery(sbQuery.toString());

		if (Validations.validateIsNotNullAndPositive(searchRequestAffectationTO
				.getBlockRequestState()))
			query.setParameter("state",
					searchRequestAffectationTO.getBlockRequestState());

		if (Validations.validateIsNotNullAndPositive(searchRequestAffectationTO
				.getBlockType()))
			query.setParameter("blockType",
					searchRequestAffectationTO.getBlockType());

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getIdHolderPk()))
			query.setParameter("idHolderPk",
					searchRequestAffectationTO.getIdHolderPk());

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getIdBlockEntityPk()))
			query.setParameter("idBlockEntityPk",
					searchRequestAffectationTO.getIdBlockEntityPk());

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getBlockNumber()))
			query.setParameter("numberRecord",
					searchRequestAffectationTO.getBlockNumber());

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getIdBlockRequestPk()))
			query.setParameter("idBlockRequestPk",
					searchRequestAffectationTO.getIdBlockRequestPk());

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getInitialDate()))
			query.setParameter("initDate",
					searchRequestAffectationTO.getInitialDate());

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getEndDate()))
			query.setParameter("endDate",
					searchRequestAffectationTO.getEndDate());

		List<Object[]> lstResult = query.getResultList();
		return lstResult;
	}

	public List<Object[]> getListPhysicalDeposit(
			PhysicalCertificateTO physicalCertTo) {
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> mapParam = new HashMap<String, Object>();

		sbQuery.append("select "
				+ "pc.ID_PHYSICAL_CERTIFICATE_PK, "
				+ // 0- PK
				"(select description from participant where id_participant_pk=pco.ID_PARTICIPANT_FK) , "
				+ // 1- AFP
				"pc.CERTIFICATE_NUMBER , "
				+ // 2- TITULO
				"pc.CURRENCY ,  "
				+ // 3- MONEDA
				"to_char(pc.EXPIRATION_DATE, 'dd/mm/yyyy'), "
				+ // 4- F VENCIMIENTO
				"to_char(pc.ISSUE_DATE, 'dd/mm/yyyy'),"
				+ // 5- F EMISION
				"(select business_name from issuer where id_issuer_pk=pc.ID_ISSUER_FK) , "
				+ // 6-EMISOR
				"TO_CHAR(pc.NOMINAL_VALUE,'999,999,999,999,999,999,999,999.00') , "
				+ // 7-VALOR NOMINAL
				"pc.SECURITY_CLASS, "
				+ // 8-clase titulo
				"(select parameter_name from parameter_table where parameter_table_pk=pc.SECURITY_CLASS) as clase_param ,"
				+ // 9-CLASE TITULO parametro
				"pc.INTEREST_RATE, "
				+ // 10-tasa
				"(select parameter_name from parameter_table where parameter_table_pk=PAYMENT_PERIODICITY) as periodicidad ,"
				+ // 11-periodicidad
				"(select description from parameter_table where parameter_table_pk=pc.CURRENCY) as moneda ");// 12-moneda
																												// parameter

		sbQuery.append("from " + "PHYSICAL_CERTIFICATE pc, "
				+ "PHYSICAL_OPERATION_DETAIL pod, "
				+ "PHYSICAL_CERTIFICATE_OPERATION pco ");
		sbQuery.append("where"
				+ " pc.ID_PHYSICAL_CERTIFICATE_PK = pod.ID_PHYSICAL_CERTIFICATE_FK "
				+ " and pod.ID_PHYSICAL_OPERATION_FK = pco.ID_PHYSICAL_OPERATION_PK "
				+ " and pco.PHYSICAL_OPERATION_TYPE = :depositOperationType " + // Deposito
				" and pod.STATE IN (:lstStates) ");// estado (confirmado,
													// authorizado, y aplicado)
		// " and pod.ind_applied = 1 "); //solo cuando esta aplicado podra
		// mostrarse

		if (Validations.validateIsNotNullAndNotEmpty(physicalCertTo
				.getInitDate()))
			sbQuery.append("	AND TRUNC(pod.registry_date) >= TRUNC(:initDate) ");

		if (Validations.validateIsNotNullAndNotEmpty(physicalCertTo
				.getEndDate()))
			sbQuery.append("	AND TRUNC(pod.registry_date) <= TRUNC(:endDate) ");

		if (Validations.validateIsNotNullAndNotEmpty(physicalCertTo
				.getIdParticipantPk()))
			sbQuery.append("	AND pco.ID_PARTICIPANT_FK = :participant ");

		sbQuery.append("	ORDER BY pc.CERTIFICATE_NUMBER DESC ");

		Query query = em.createNativeQuery(sbQuery.toString());

		query.setParameter("depositOperationType",
				PhysicalCertificateOperationType.DEPOSIT.getCode());
		query.setParameter("lstStates", physicalCertTo.getLstStates());

		if (Validations.validateIsNotNull(physicalCertTo.getEndDate()))
			query.setParameter("endDate", physicalCertTo.getEndDate());

		if (Validations.validateIsNotNull(physicalCertTo.getInitDate()))
			query.setParameter("initDate", physicalCertTo.getInitDate());

		if (Validations.validateIsNotNullAndPositive(physicalCertTo
				.getIdParticipantPk()))
			query.setParameter("participant",
					physicalCertTo.getIdParticipantPk());

		List<Object[]> lstResult = query.getResultList();
		return lstResult;

	}

	public List<Object[]> getListPhysicalRetirement(
			PhysicalCertificateTO physicalCertTo) {
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> mapParam = new HashMap<String, Object>();

		sbQuery.append("select "
				+ "pc.ID_PHYSICAL_CERTIFICATE_PK, "
				+ // 0- PK
				"(select description from participant where id_participant_pk=pco.ID_PARTICIPANT_FK) , "
				+ // 1- AFP
				"pc.CERTIFICATE_NUMBER , "
				+ // 2- TITULO
				"pc.CURRENCY ,  "
				+ // 3- MONEDA
				"TO_CHAR(pc.EXPIRATION_DATE,'dd/MM/yyyy') , "
				+ // 4- F VENCIMIENTO
				"TO_CHAR(pc.ISSUE_DATE,'dd/MM/yyyy') , "
				+ // 5- F EMISION
				"(select business_name from issuer where id_issuer_pk=pc.ID_ISSUER_FK) , "
				+ // 6-EMISOR
				"TO_CHAR(pc.NOMINAL_VALUE,'999,999,999,999,999,999,999,999.00') , "
				+ // 7-VALOR NOMINAL
				"pc.SECURITY_CLASS, "
				+ // 8-CLASE TITULO
				"(select parameter_name from parameter_table where parameter_table_pk=pc.SECURITY_CLASS) as clase_param ,"
				+ // 9-CLASE TITULO parametro
				"pc.INTEREST_RATE, "
				+ // 10-tasa
				"(select parameter_name from parameter_table where parameter_table_pk=PAYMENT_PERIODICITY) as periodicidad ,"
				+ // 11-periodicidad
				"(select description from parameter_table where parameter_table_pk=pc.CURRENCY) as moneda ");// 12-moneda
																												// parameter

		sbQuery.append("from " + "PHYSICAL_CERTIFICATE pc, "
				+ "PHYSICAL_OPERATION_DETAIL pod, "
				+ "PHYSICAL_CERTIFICATE_OPERATION pco ");
		sbQuery.append("where"
				+ " pc.ID_PHYSICAL_CERTIFICATE_PK = pod.ID_PHYSICAL_CERTIFICATE_FK "
				+ " and pod.ID_PHYSICAL_OPERATION_FK = pco.ID_PHYSICAL_OPERATION_PK "
				+ " and pco.PHYSICAL_OPERATION_TYPE = :retirementOperationType "
				+ // Retiro
				" and pod.STATE IN (:lstStates) ");// estado (confirmado,
													// authorizado, y aplicado)
		// " and pod.ind_applied = 1 "); //solo cuando esta aplicado podra
		// mostrarse

		if (Validations.validateIsNotNullAndNotEmpty(physicalCertTo
				.getInitDate()))
			sbQuery.append("	AND TRUNC(pod.registry_date) >= TRUNC(:initDate) ");

		if (Validations.validateIsNotNullAndNotEmpty(physicalCertTo
				.getEndDate()))
			sbQuery.append("	AND TRUNC(pod.registry_date) <= TRUNC(:endDate) ");

		if (Validations.validateIsNotNullAndNotEmpty(physicalCertTo
				.getIdParticipantPk()))
			sbQuery.append("	AND pco.ID_PARTICIPANT_FK = :participant ");

		sbQuery.append("	ORDER BY pc.CERTIFICATE_NUMBER DESC ");

		Query query = em.createNativeQuery(sbQuery.toString());

		query.setParameter("retirementOperationType",
				PhysicalCertificateOperationType.RETIREMENT.getCode());
		query.setParameter("lstStates", physicalCertTo.getLstStates());

		if (Validations.validateIsNotNull(physicalCertTo.getEndDate()))
			query.setParameter("endDate", physicalCertTo.getEndDate());

		if (Validations.validateIsNotNull(physicalCertTo.getInitDate()))
			query.setParameter("initDate", physicalCertTo.getInitDate());

		if (Validations.validateIsNotNullAndPositive(physicalCertTo
				.getIdParticipantPk()))
			query.setParameter("participant",
					physicalCertTo.getIdParticipantPk());

		List<Object[]> lstResult = query.getResultList();
		return lstResult;

	}

	/**/
	public List<Object[]> getListRequestAffectationWithBlockEntity(
			AffectationTO searchRequestAffectationTO) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> mapParam = new HashMap<String, Object>();
		sbQuery.append(" SELECT "
				+ " ( DECODE( PERSON_TYPE,96, BE.ID_BLOCK_ENTITY_PK||' '||BE.NAME||' '||BE.first_last_name||' '||BE.second_last_name, BE.ID_BLOCK_ENTITY_PK||' '||BE.FULL_NAME) ), "
				+ // 0 nombre entidad de bloqueo
				// " ( BE.ID_BLOCK_ENTITY_PK||' '||BE.NAME ), "+ //0 nombre
				// entidad de bloqueo
				" ( SELECT (ID_ISIN_CODE_PK||' '||DESCRIPTION) FROM SECURITY WHERE ID_ISIN_CODE_PK = BO.ID_ISIN_CODE_FK ), "
				+ // 1 isin
				" BO.DOCUMENT_NUMBER, "
				+ // 2 documento de bloqueo
				" HA.ACCOUNT_NUMBER, "
				+ // 3 account number
				" (BO.ID_PARTICIPANT_FK||' - '||PA.MNEMONIC) as participant, "
				+ // 4 participante
				" FN_GET_HOLDER_NAME(HA.id_Holder_Account_Pk) as rnt_description, "
				+ // 5 RNT - holder name
				" BR.BLOCK_TYPE, "
				+ // 6 tipo de bloqueo
				" BO.BLOCK_LEVEL, "
				+ // 7 bloqueo/rebloqueo
				" BR.REGISTRY_DATE, "
				+ // 8 fecha
				" BO.IND_INTEREST, "
				+ // 9 indice interes
				" BO.IND_AMORTIZATION, "
				+ // 10 indice amortizacion
				" BO.IND_RESCUE, "
				+ // 11 indice rescate
				" BO.ORIGINAL_BLOCK_BALANCE, "
				+ // 12 monto original
				" BO.ACTUAL_BLOCK_BALANCE, "
				+ // 13 monto actual
				" BO.BLOCK_STATE "
				+ // 14 estado
				" FROM " + " BLOCK_OPERATION BO, " + " HOLDER_ACCOUNT HA, "
				+ " Participant PA, " + " BLOCK_REQUEST BR, "
				+ " BLOCK_ENTITY BE " + " WHERE "
				+ " BO.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK "
				+ " and BO.ID_PARTICIPANT_FK=PA.ID_PARTICIPANT_PK "
				+ " and BO.ID_BLOCK_REQUEST_FK=BR.ID_BLOCK_REQUEST_PK "
				+ " and BE.ID_BLOCK_ENTITY_PK=BR.ID_BLOCK_ENTITY_FK ");

		if (Validations.validateIsNotNullAndPositive(searchRequestAffectationTO
				.getCustodyOperationtState()))
			sbQuery.append("	AND BO.BLOCK_STATE = :state");

		if (Validations.validateIsNotNullAndPositive(searchRequestAffectationTO
				.getBlockType()))
			sbQuery.append("	AND BR.block_Type = :blockType");

		if (Validations.validateIsNotNullAndPositive(searchRequestAffectationTO
				.getIdHolderPk()))
			sbQuery.append("	AND BR.id_holder_fk = :idHolderPk");

		if (Validations.validateIsNotNullAndPositive(searchRequestAffectationTO
				.getIdBlockEntityPk()))
			sbQuery.append("	AND BE.id_Block_Entity_Pk = :idBlockEntityPk");

		if (Validations.validateIsNotNull(searchRequestAffectationTO
				.getDocumentNumber()))
			sbQuery.append("	AND BO.document_Number = :documentNumber");

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getInitialDate()))
			sbQuery.append("	AND TRUNC(BR.registry_Date) >= TRUNC(:initDate)");

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getEndDate()))
			sbQuery.append("	AND TRUNC(BR.registry_Date) <= TRUNC(:endDate) ");

		sbQuery.append("	ORDER BY BE.name DESC ");
		/*****************************************************************************************************************/

		Query query = em.createNativeQuery(sbQuery.toString());
		if (Validations.validateIsNotNullAndPositive(searchRequestAffectationTO
				.getCustodyOperationtState()))
			query.setParameter("state",
					searchRequestAffectationTO.getCustodyOperationtState());

		if (Validations.validateIsNotNullAndPositive(searchRequestAffectationTO
				.getBlockType()))
			query.setParameter("blockType",
					searchRequestAffectationTO.getBlockType());

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getIdHolderPk()))
			query.setParameter("idHolderPk",
					searchRequestAffectationTO.getIdHolderPk());

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getIdBlockEntityPk()))
			query.setParameter("idBlockEntityPk",
					searchRequestAffectationTO.getIdBlockEntityPk());

		if (Validations.validateIsNotNull(searchRequestAffectationTO
				.getDocumentNumber()))
			query.setParameter("documentNumber",
					searchRequestAffectationTO.getDocumentNumber());

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getInitialDate()))
			query.setParameter("initDate",
					searchRequestAffectationTO.getInitialDate());

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getEndDate()))
			query.setParameter("endDate",
					searchRequestAffectationTO.getEndDate());

		List<Object[]> lstResult = query.getResultList();
		return lstResult;
	}

	public List<Object[]> getListAffectationWithParticipant(
			AffectationTO searchRequestAffectationTO) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> mapParam = new HashMap<String, Object>();

		sbQuery.append(" SELECT "
				+ " BE.FULL_NAME, "
				+ // 0 nombre entidad de bloqueo
				" BO.ID_ISIN_CODE_FK, "
				+ // 1 isin
				" BO.DOCUMENT_NUMBER, "
				+ // 2 documento de bloqueo
				" HA.ACCOUNT_NUMBER, "
				+ // 3 account number
				" (BO.ID_PARTICIPANT_FK||' - '||PA.MNEMONIC) as participant, "
				+ // 4 participante
				" FN_GET_HOLDER_NAME(HA.id_Holder_Account_Pk) as rnt_description, "
				+ // 5 RNT - holder name
				" BR.BLOCK_TYPE, "
				+ // 6 tipo de bloqueo
				" BO.BLOCK_LEVEL, "
				+ // 7 bloqueo/rebloqueo
				" BR.REGISTRY_DATE, "
				+ // 8 fecha
				" BO.ORIGINAL_BLOCK_BALANCE, "
				+ // 9 monto original
				" BO.ACTUAL_BLOCK_BALANCE, "
				+ // 10 monto actual
				" BO.BLOCK_STATE "
				+ // 11 estado
				" FROM " + " BLOCK_OPERATION BO, " + " HOLDER_ACCOUNT HA, "
				+ " PARTICIPANT PA, " + " BLOCK_REQUEST BR, "
				+ " BLOCK_ENTITY BE " + " WHERE "
				+ " BO.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK "
				+ " and BO.ID_PARTICIPANT_FK=PA.ID_PARTICIPANT_PK "
				+ " and BO.ID_BLOCK_REQUEST_FK=BR.ID_BLOCK_REQUEST_PK "
				+ " and BE.ID_BLOCK_ENTITY_PK=BR.ID_BLOCK_ENTITY_FK ");
		/*
		 * sbQuery.append("	SELECT BO.documentNumber, " + //0 nro documento de
		 * bloqueo "	CO.registryDate, " + //1 fecha
		 * "	(BR.holder.idHolderPk||' - '||BR.holder.fullName) as rnt_description, "
		 * + //2 RNT - holder name "	BE.name, " + //3 nombre entidad de bloqueo
		 * "	BR.blockType," + //4 tipo de bloqueo "	BO.blockState," + //5 estado
		 * documento de bloqueo "	BR.blockNumber, " + //6 nro de acto
		 * "	BR.blockNumberDate, " + //7 fecha de Acto
		 * "	BO.actualBlockBalance "+ //8 saldo "	FROM BlockRequest BR, " +
		 * "		 BlockEntity BE," + "		 BlockOperation BO," +
		 * "		 CustodyOperation CO "+
		 * "	WHERE BE.idBlockEntityPk = BR.blockEntity.idBlockEntityPk " +
		 * "	AND BO.blockRequest.idBlockRequestPk = BR.idBlockRequestPk " +
		 * "	AND CO.idCustodyOperationPk = BO.idBlockOperationPk " +
		 * "	AND BO.blockState = "+AffectationStateType.BLOCKED.getCode());
		 */
		if (Validations.validateIsNotNullAndPositive(searchRequestAffectationTO
				.getCustodyOperationtState()))
			sbQuery.append("	AND BO.block_State = :state");

		if (Validations.validateIsNotNullAndPositive(searchRequestAffectationTO
				.getBlockType()))
			sbQuery.append("	AND BR.block_Type = :blockType");

		if (Validations.validateIsNotNullAndPositive(searchRequestAffectationTO
				.getIdHolderPk()))
			sbQuery.append("	AND BR.id_holder_fk = :idHolderPk");

		if (Validations.validateIsNotNullAndPositive(searchRequestAffectationTO
				.getIdBlockEntityPk()))
			sbQuery.append("	AND BE.id_Block_Entity_Pk = :idBlockEntityPk");

		if (Validations.validateIsNotNull(searchRequestAffectationTO
				.getIdParticipant()))
			sbQuery.append("	AND BO.ID_PARTICIPANT_FK = :idParticipant");

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getInitialDate()))
			sbQuery.append("	AND TRUNC(BR.REGISTRY_DATE) >= TRUNC(:initDate)");

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getEndDate()))
			sbQuery.append("	AND TRUNC(BR.REGISTRY_DATE) <= TRUNC(:endDate)");

		sbQuery.append("	ORDER BY participant DESC ");

		Query query = em.createNativeQuery(sbQuery.toString());

		if (Validations.validateIsNotNullAndPositive(searchRequestAffectationTO
				.getCustodyOperationtState()))
			query.setParameter("state",
					searchRequestAffectationTO.getCustodyOperationtState());

		if (Validations.validateIsNotNullAndPositive(searchRequestAffectationTO
				.getBlockType()))
			query.setParameter("blockType",
					searchRequestAffectationTO.getBlockType());

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getIdHolderPk()))
			query.setParameter("idHolderPk",
					searchRequestAffectationTO.getIdHolderPk());

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getIdBlockEntityPk()))
			query.setParameter("idBlockEntityPk",
					searchRequestAffectationTO.getIdBlockEntityPk());

		if (Validations.validateIsNotNull(searchRequestAffectationTO
				.getIdParticipant()))
			query.setParameter("idParticipant",
					searchRequestAffectationTO.getIdParticipant());

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getInitialDate()))
			query.setParameter("initDate",
					searchRequestAffectationTO.getInitialDate());

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getEndDate()))
			query.setParameter("endDate",
					searchRequestAffectationTO.getEndDate());

		List<Object[]> lstResult = query.getResultList();
		return lstResult;
	}

	public List<Object[]> getListUnaffectationWithType(
			AffectationTO searchRequestAffectationTO) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> mapParam = new HashMap<String, Object>();
		sbQuery.append("	SELECT UR.idUnblockRequestPk, "
				+ // 0 nro solicitud de desbloqueo
				"	UR.registryDate, "
				+ // 1 fecha
				"	(UR.holder.idHolderPk||' - '||UR.holder.fullName) as rnt_description, "
				+ // 2 RNT - holder name
				"	BE.fullName, "
				+ // 3 nombre entidad de bloqueo
				"	BR.blockType,"
				+ // 4 tipo de bloqueo
				"	UO.unblockState,"
				+ // 5 estado
				"	UR.motive, "
				+ // 6 motivo de desbloqueo
				"	UO.unblockQuantity, "
				+ // 7 monto de desbloqueo
				"	UR.lastModifyDate, "
				+ // 8 fecha ultima modificacion
				"	UO.documentNumber, "
				+ // 9 documento de bloqueo
				"	BO.blockLevel, "
				+ // 10 bloqueo/rebloqueo
				"	BO.securities.idIsinCodePk, "
				+ // 11 isin
				"	(BO.participant.idParticipantPk||' - '||BO.participant.mnemonic), "
				+ // 12 participante
				"	BO.originalBlockBalance,	"
				+ // 13 monto
				"	BO.holderAccount.accountNumber, "
				+ // 14 account number
				" 	BO.actualBlockBalance, "
				+ // 15 monto actual
				"	BO.documentNumber "
				+ // 16 documento de bloqueo
				"	FROM BlockOperation BO," + "		 BlockRequest BR, "
				+ "		 BlockEntity BE, "
				+ "		 UnblockOperation UO, "
				+ "		 UnblockRequest UR "
				+ "	WHERE BO.blockRequest = BR.idBlockRequestPk "
				+ "	AND BE.idBlockEntityPk = BR.blockEntity.idBlockEntityPk "
				+ "	AND UO.blockOperation = BO.idBlockOperationPk "
				+ "	AND UO.unblockRequest = UR.idUnblockRequestPk "
				+
				// "  	AND BE.stateBlockEntity = " +
				// BlockEntityStateType.ACTIVATED.getCode() +
				"	AND BR.holder.stateHolder IN ("
				+ HolderStateType.REGISTERED.getCode() + ","
				+ HolderStateType.BLOCKED.getCode() + ")");

		if (Validations.validateIsNotNullAndPositive(searchRequestAffectationTO
				.getState()))
			sbQuery.append("	AND UO.unblockState = :state");

		if (Validations.validateIsNotNullAndPositive(searchRequestAffectationTO
				.getBlockType()))
			sbQuery.append("	AND BR.blockType = :blockType");

		if (Validations.validateIsNotNullAndPositive(searchRequestAffectationTO
				.getIdHolderPk()))
			sbQuery.append("	AND BR.holder.idHolderPk = :idHolderPk");

		if (Validations.validateIsNotNullAndPositive(searchRequestAffectationTO
				.getIdBlockEntityPk()))
			sbQuery.append("	AND BR.blockEntity.idBlockEntityPk = :idBlockEntityPk");

		if (Validations.validateIsNotNull(searchRequestAffectationTO
				.getBlockNumber()))
			sbQuery.append("	AND UO.documentNumber = :blockNumber ");

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getIdUnblockRequestPk()))
			sbQuery.append("	AND UR.idUnblockRequestPk = :idUnblockRequestPk ");

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getInitialDate()))
			sbQuery.append("	AND TRUNC(BR.registryDate) >= TRUNC(:initDate) ");

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getEndDate()))
			sbQuery.append("	AND TRUNC(BR.registryDate) <= TRUNC(:endDate) ");

		sbQuery.append("	ORDER BY UR.idUnblockRequestPk DESC");
		/*****************************************************************************************************************/

		Query query = em.createQuery(sbQuery.toString());

		if (Validations.validateIsNotNullAndPositive(searchRequestAffectationTO
				.getState()))
			query.setParameter("state", searchRequestAffectationTO.getState());

		if (Validations.validateIsNotNullAndPositive(searchRequestAffectationTO
				.getBlockType()))
			query.setParameter("blockType",
					searchRequestAffectationTO.getBlockType());

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getIdHolderPk()))
			query.setParameter("idHolderPk",
					searchRequestAffectationTO.getIdHolderPk());

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getIdBlockEntityPk()))
			query.setParameter("idBlockEntityPk",
					searchRequestAffectationTO.getIdBlockEntityPk());

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getBlockNumber()))
			query.setParameter("blockNumber",
					searchRequestAffectationTO.getBlockNumber());

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getIdUnblockRequestPk()))
			query.setParameter("idUnblockRequestPk",
					searchRequestAffectationTO.getIdUnblockRequestPk());

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getInitialDate()))
			query.setParameter("initDate",
					searchRequestAffectationTO.getInitialDate());

		if (Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO
				.getEndDate()))
			query.setParameter("endDate",
					searchRequestAffectationTO.getEndDate());

		List<Object[]> lstResult = query.getResultList();
		return lstResult;
	}

	public List<Object[]> getChangeOwnershipListAvailableSecurities(
			ChangeOwnershipReportTO changeOwnershipReportTO)
			throws ServiceException {

		List<Long> lstChangeOwnershipPk = getListChangeOwnershipAvailablePk(changeOwnershipReportTO);

		if (Validations.validateListIsNullOrEmpty(lstChangeOwnershipPk)) {
			return new ArrayList<Object[]>();
		} else {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" select ")
					.append(" coo.id_change_ownership_pk, ")
					. // 0
					append(" p_source.description || ' - ' || p_source.id_participant_pk as participant_source, ")
					. // 1
					append(" co.registry_date, ")
					. // 2
					append(" coo.motive, ")
					. // 3
					append(" coo.state, ")
					. // 4
					append(" ha_source.account_number as account_number_source, ")
					. // 5
					append(" LISTAGG(h_source.id_holder_pk,'<br/>') WITHIN GROUP (ORDER BY h_source.id_holder_pk) AS id_holder_source, ")
					. // 6
					append(" LISTAGG(case when length(h_source.full_name)>50 then substr(h_source.full_name,0,47)||'...' else h_source.full_name end,'<br/>') WITHIN GROUP (ORDER BY h_source.id_holder_pk) AS holder_description_source, ")
					. // 7
					append(" p_target.mnemonic || ' - ' || p_target.id_participant_pk as participant_target ")
					. // 8
					append(" From custody_operation co ")
					.append(" inner join change_ownership_operation coo on co.id_custody_operation_pk = coo.id_change_ownership_pk ")
					.append(" inner join participant p_source on coo.id_source_participant_fk=p_source.id_participant_pk ")
					.append(" inner join holder_account ha_source on ha_source.id_holder_account_pk=coo.id_source_holder_account_fk ")
					.append(" inner join holder_account_detail had_source on ha_source.id_holder_account_pk=had_source.id_holder_account_fk ")
					.append(" inner join holder h_source on had_source.id_holder_fk=h_source.id_holder_pk ")
					.append(" inner join participant p_target on coo.id_target_participant_fk=p_target.id_participant_pk ")
					.append(" Where coo.id_change_ownership_pk in (:lstIdChangeOwnershipPrm)")
					.append(" Group By coo.id_change_ownership_pk,p_source.description || ' - ' || p_source.id_participant_pk,co.registry_date,coo.motive,coo.state,ha_source.account_number, p_target.mnemonic || ' - ' || p_target.id_participant_pk ")
					.append(" Order By p_source.description || ' - ' || p_source.id_participant_pk,coo.id_change_ownership_pk ");

			Query query = em.createNativeQuery(sbQuery.toString());

			query.setParameter("lstIdChangeOwnershipPrm", lstChangeOwnershipPk);

			List<Object[]> lstChangeOwnershipHeader = (List<Object[]>) query
					.getResultList();

			List<Object[]> lstChangeOwnershipToReturn = new ArrayList<Object[]>();

			for (Object[] changeOwnershipHeader : lstChangeOwnershipHeader) {
				lstChangeOwnershipToReturn
						.addAll(getChangeOwnershipDetailListAvailable(changeOwnershipHeader));
			}

			return lstChangeOwnershipToReturn;
		}

	}

	public List<Long> getListChangeOwnershipAvailablePk(
			ChangeOwnershipReportTO changeOwnershipReportTO) {

		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select distinct")
				.append(" coo.id_change_ownership_pk ")
				.append(" From custody_operation co ")
				.append(" inner join change_ownership_operation coo on co.id_custody_operation_pk = coo.id_change_ownership_pk ");

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getIdTargetHolderPk())) {
			sbQuery.append(
					" inner join change_ownership_detail cod on coo.id_change_ownership_pk = cod.id_change_ownership_fk And cod.ind_origin_target = 2 ")
					.append(" inner join holder_account ha_target on ha_target.id_holder_account_pk=cod.id_holder_account_fk ")
					.append(" inner join holder_account_detail had_target on ha_target.id_holder_account_pk=had_target.id_holder_account_fk ")
					.append(" inner join holder h_target on had_target.id_holder_fk=h_target.id_holder_pk ");
		}

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getIdSourceHolderPk())) {
			sbQuery.append(
					" inner join holder_account ha_source on ha_source.id_holder_account_pk=coo.id_source_holder_account_fk ")
					.append(" inner join holder_account_detail had_source on ha_source.id_holder_account_pk=had_source.id_holder_account_fk ")
					.append(" inner join holder h_source on had_source.id_holder_fk=h_source.id_holder_pk ");
		}

		sbQuery.append(" Where coo.balance_type = :balanceTypePrm ")
				.append(" And co.registry_date >= :startDatePrm and co.registry_date < :endDatePrm ");

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getIdChangeOwnershipPk())) {
			sbQuery.append(" And coo.id_change_ownership_pk = :idChangeOwnershipPkPrm ");
		}

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getIdTargetHolderPk())) {
			sbQuery.append(" And h_target.id_holder_pk = :idTargetHolderPrm ");
		}

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getIdSourceHolderPk())) {
			sbQuery.append(" And h_source.id_holder_pk = :idSourceHolderPrm ");
		}

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getIdSourceParticipantPk())) {
			sbQuery.append(" And coo.id_source_participant_fk = :idSourceParticipantPrm ");
		}

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getIdTargetParticipantPk())) {
			sbQuery.append(" And coo.id_target_participant_fk = :idTargetParticipantPrm ");
		}

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getMotive())) {
			sbQuery.append(" And coo.motive = :motivePrm ");
		}

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getState())) {
			sbQuery.append(" And coo.state = :statePrm ");
		}

		Query query = em.createNativeQuery(sbQuery.toString());

		query.setParameter("balanceTypePrm",
				ChangeOwnershipBalanceTypeType.AVAILABLE_BALANCE.getCode());
		query.setParameter("startDatePrm",
				changeOwnershipReportTO.getStartDate(), TemporalType.DATE);
		query.setParameter(
				"endDatePrm",
				CommonsUtilities.addDaysToDate(
						changeOwnershipReportTO.getEndDate(), 1),
				TemporalType.DATE);

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getIdChangeOwnershipPk())) {
			query.setParameter("idChangeOwnershipPkPrm",
					changeOwnershipReportTO.getIdChangeOwnershipPk());
		}

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getIdTargetHolderPk())) {
			query.setParameter("idTargetHolderPrm",
					changeOwnershipReportTO.getIdTargetHolderPk());
		}

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getIdSourceHolderPk())) {
			query.setParameter("idSourceHolderPrm",
					changeOwnershipReportTO.getIdSourceHolderPk());
		}

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getIdSourceParticipantPk())) {
			query.setParameter("idSourceParticipantPrm",
					changeOwnershipReportTO.getIdSourceParticipantPk());
		}

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getIdTargetParticipantPk())) {
			query.setParameter("idTargetParticipantPrm",
					changeOwnershipReportTO.getIdTargetParticipantPk());
		}

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getMotive())) {
			query.setParameter("motivePrm", changeOwnershipReportTO.getMotive());
		}

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getState())) {
			query.setParameter("statePrm", changeOwnershipReportTO.getState());
		}

		return (List<Long>) query.getResultList();

	}

	public List<Long> getListChangeOwnershipBlockedPk(
			ChangeOwnershipReportTO changeOwnershipReportTO) {

		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select distinct")
				.append(" coo.id_change_ownership_pk ")
				.append(" From custody_operation co ")
				.append(" inner join change_ownership_operation coo on co.id_custody_operation_pk = coo.id_change_ownership_pk ")
				.append(" inner join change_ownership_detail cod on coo.id_change_ownership_pk = cod.id_change_ownership_fk ")
				.append(" inner join block_change_ownership bco on cod.id_change_ownership_detail_pk = bco.id_change_ownership_detail_fk ");

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getIdTargetHolderPk())) {
			sbQuery.append(
					" inner join holder_account ha_target on ha_target.id_holder_account_pk=bco.id_holder_account_fk ")
					.append(" inner join holder_account_detail had_target on ha_target.id_holder_account_pk=had_target.id_holder_account_fk ")
					.append(" inner join holder h_target on had_target.id_holder_fk=h_target.id_holder_pk ");
		}

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getBlockType())) {
			sbQuery.append(
					" inner join block_operation blo on bco.id_block_operation_fk = blo.id_block_operation_pk ")
					.append(" inner join block_request blr on blo.id_block_request_fk = blr.id_block_request_pk ");
		}

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getIdSourceHolderPk())) {
			sbQuery.append(
					" inner join holder_account ha_source on ha_source.id_holder_account_pk=coo.id_source_holder_account_fk ")
					.append(" inner join holder_account_detail had_source on ha_source.id_holder_account_pk=had_source.id_holder_account_fk ")
					.append(" inner join holder h_source on had_source.id_holder_fk=h_source.id_holder_pk ");
		}

		sbQuery.append(" Where coo.balance_type = :balanceTypePrm ")
				.append(" And co.registry_date >= :startDatePrm and co.registry_date < :endDatePrm ");

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getIdChangeOwnershipPk())) {
			sbQuery.append(" And coo.id_change_ownership_pk = :idChangeOwnershipPkPrm ");
		}

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getIdTargetHolderPk())) {
			sbQuery.append(" And h_target.id_holder_pk = :idTargetHolderPrm ");
		}

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getIdSourceHolderPk())) {
			sbQuery.append(" And h_source.id_holder_pk = :idSourceHolderPrm ");
		}

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getIdSourceParticipantPk())) {
			sbQuery.append(" And coo.id_source_participant_fk = :idSourceParticipantPrm ");
		}

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getIdTargetParticipantPk())) {
			sbQuery.append(" And coo.id_target_participant_fk = :idTargetParticipantPrm ");
		}

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getBlockType())) {
			sbQuery.append(" And blr.blockType = :blockTypePrm ");
		}

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getMotive())) {
			sbQuery.append(" And coo.motive = :motivePrm ");
		}

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getState())) {
			sbQuery.append(" And coo.state = :statePrm ");
		}

		Query query = em.createNativeQuery(sbQuery.toString());

		query.setParameter("balanceTypePrm",
				ChangeOwnershipBalanceTypeType.BLOCKED_BALANCE.getCode());
		query.setParameter("startDatePrm",
				changeOwnershipReportTO.getStartDate(), TemporalType.DATE);
		query.setParameter(
				"endDatePrm",
				CommonsUtilities.addDaysToDate(
						changeOwnershipReportTO.getEndDate(), 1),
				TemporalType.DATE);

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getIdChangeOwnershipPk())) {
			query.setParameter("idChangeOwnershipPkPrm",
					changeOwnershipReportTO.getIdChangeOwnershipPk());
		}

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getIdTargetHolderPk())) {
			query.setParameter("idTargetHolderPrm",
					changeOwnershipReportTO.getIdTargetHolderPk());
		}

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getIdSourceHolderPk())) {
			query.setParameter("idSourceHolderPrm",
					changeOwnershipReportTO.getIdSourceHolderPk());
		}

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getIdSourceParticipantPk())) {
			query.setParameter("idSourceParticipantPrm",
					changeOwnershipReportTO.getIdSourceParticipantPk());
		}

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getIdTargetParticipantPk())) {
			query.setParameter("idTargetParticipantPrm",
					changeOwnershipReportTO.getIdTargetParticipantPk());
		}

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getBlockType())) {
			query.setParameter("blockTypePrm",
					changeOwnershipReportTO.getBlockType());
		}

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getMotive())) {
			query.setParameter("motivePrm", changeOwnershipReportTO.getMotive());
		}

		if (Validations.validateIsNotNullAndPositive(changeOwnershipReportTO
				.getState())) {
			query.setParameter("statePrm", changeOwnershipReportTO.getState());
		}

		return (List<Long>) query.getResultList();

	}

	public List<Object[]> getChangeOwnershipListBlockedSecurities(
			ChangeOwnershipReportTO changeOwnershipReportTO)
			throws ServiceException {

		List<Long> lstChangeOwnershipPk = getListChangeOwnershipBlockedPk(changeOwnershipReportTO);

		if (Validations.validateListIsNullOrEmpty(lstChangeOwnershipPk)) {
			return new ArrayList<Object[]>();
		} else {

			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" select ")
					.append(" coo.id_change_ownership_pk, ")
					. // 0
					append(" p_source.description || ' - ' || p_source.id_participant_pk as participant_source, ")
					. // 1
					append(" co.registry_date, ")
					. // 2
					append(" coo.motive, ")
					. // 3
					append(" coo.state, ")
					. // 4
					append(" ha_source.account_number as account_number_source, ")
					. // 5
					append(" LISTAGG(h_source.id_holder_pk,'<br/>') WITHIN GROUP (ORDER BY h_source.id_holder_pk) AS id_holder_source, ")
					. // 6
					append(" LISTAGG(case when length(h_source.full_name)>50 then substr(h_source.full_name,0,47)||'...' else h_source.full_name end,'<br/>') WITHIN GROUP (ORDER BY h_source.id_holder_pk) AS holder_description_source, ")
					. // 7
					append(" p_target.mnemonic || ' - ' || p_target.id_participant_pk as participant_target ")
					. // 8
					append(" From custody_operation co ")
					.append(" inner join change_ownership_operation coo on co.id_custody_operation_pk = coo.id_change_ownership_pk ")
					.append(" inner join participant p_source on coo.id_source_participant_fk=p_source.id_participant_pk ")
					.append(" inner join holder_account ha_source on ha_source.id_holder_account_pk=coo.id_source_holder_account_fk ")
					.append(" inner join holder_account_detail had_source on ha_source.id_holder_account_pk=had_source.id_holder_account_fk ")
					.append(" inner join holder h_source on had_source.id_holder_fk=h_source.id_holder_pk ")
					.append(" inner join participant p_target on coo.id_target_participant_fk=p_target.id_participant_pk ")
					.append(" Where coo.id_change_ownership_pk in (:lstIdChangeOwnershipPrm)")
					.append(" Group By coo.id_change_ownership_pk,p_source.description || ' - ' || p_source.id_participant_pk,co.registry_date,coo.motive,coo.state,ha_source.account_number, p_target.mnemonic || ' - ' || p_target.id_participant_pk ")
					.append(" Order By p_source.description || ' - ' || p_source.id_participant_pk,coo.id_change_ownership_pk ");

			Query query = em.createNativeQuery(sbQuery.toString());

			query.setParameter("lstIdChangeOwnershipPrm", lstChangeOwnershipPk);

			List<Object[]> lstChangeOwnershipHeader = (List<Object[]>) query
					.getResultList();

			List<Object[]> lstChangeOwnershipToReturn = new ArrayList<Object[]>();

			for (Object[] changeOwnershipHeader : lstChangeOwnershipHeader) {
				lstChangeOwnershipToReturn
						.addAll(getChangeOwnershipDetailListBlocked(changeOwnershipHeader));
			}

			return lstChangeOwnershipToReturn;
		}

	}

	public List<Object[]> getChangeOwnershipDetailListAvailable(
			Object[] changeOwnershipHeader) throws ServiceException {

		Long idChangeOwnershipPk = Long.valueOf(changeOwnershipHeader[0]
				.toString());

		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select ")
				.append(" cod.id_isin_code_fk, ")
				. // 0
				append(" ha_target.account_number as account_number_target, ")
				. // 1
				append(" LISTAGG(h_target.id_holder_pk,'<br/>') WITHIN GROUP (ORDER BY h_target.id_holder_pk) AS id_holder_target, ")
				. // 2
				append(" LISTAGG(case when length(h_target.full_name)>50 then substr(h_target.full_name,0,47)||'...' else h_target.full_name end,'<br/>') WITHIN GROUP (ORDER BY h_target.id_holder_pk) AS holder_description_target, ")
				. // 3
				append(" cod.available_balance ")
				. // 4
				append(" From change_ownership_detail cod ")
				.append(" inner join holder_account ha_target on ha_target.id_holder_account_pk=cod.id_holder_account_fk And cod.ind_origin_target = 2 ")
				.append(" inner join holder_account_detail had_target on ha_target.id_holder_account_pk=had_target.id_holder_account_fk ")
				.append(" inner join holder h_target on had_target.id_holder_fk=h_target.id_holder_pk ")
				.append(" Where cod.id_change_ownership_fk = :idChangeOwnershipPkPrm ")
				.append(" Group By cod.id_isin_code_fk,ha_target.account_number,cod.available_balance ")
				.append(" Order By cod.id_isin_code_fk,ha_target.account_number ");

		Query query = em.createNativeQuery(sbQuery.toString());

		query.setParameter("idChangeOwnershipPkPrm", idChangeOwnershipPk);

		List<Object[]> lstChangeOwnershipDetail = (List<Object[]>) query
				.getResultList();

		List<Object[]> lstChangeOwnershipToReturn = new ArrayList<Object[]>();
		Object[] arrObjChangeOwnership = null;

		for (Object[] changeOwnershipDetail : lstChangeOwnershipDetail) {
			arrObjChangeOwnership = new Object[14];

			// Add attributs of header
			arrObjChangeOwnership[0] = changeOwnershipHeader[0];
			arrObjChangeOwnership[1] = changeOwnershipHeader[1];
			arrObjChangeOwnership[2] = changeOwnershipHeader[2];
			arrObjChangeOwnership[3] = changeOwnershipHeader[3];
			arrObjChangeOwnership[4] = changeOwnershipHeader[4];
			arrObjChangeOwnership[5] = changeOwnershipHeader[5];
			arrObjChangeOwnership[6] = changeOwnershipHeader[6];
			arrObjChangeOwnership[7] = changeOwnershipHeader[7];
			arrObjChangeOwnership[8] = changeOwnershipHeader[8];

			// Add attributs of detail
			arrObjChangeOwnership[9] = changeOwnershipDetail[0];
			arrObjChangeOwnership[10] = changeOwnershipDetail[1];
			arrObjChangeOwnership[11] = changeOwnershipDetail[2];
			arrObjChangeOwnership[12] = changeOwnershipDetail[3];
			arrObjChangeOwnership[13] = changeOwnershipDetail[4];

			lstChangeOwnershipToReturn.add(arrObjChangeOwnership);
		}

		return lstChangeOwnershipToReturn;

	}

	public List<Object[]> getChangeOwnershipDetailListBlocked(
			Object[] changeOwnershipHeader) throws ServiceException {

		Long idChangeOwnershipPk = Long.valueOf(changeOwnershipHeader[0]
				.toString());

		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select ")
				.append(" cod.id_isin_code_fk, ")
				. // 0
				append(" ha_target.account_number as account_number_target, ")
				. // 1
				append(" LISTAGG(h_target.id_holder_pk,'<br/>') WITHIN GROUP (ORDER BY h_target.id_holder_pk) AS id_holder_target, ")
				. // 2
				append(" LISTAGG(case when length(h_target.full_name)>30 then substr(h_target.full_name,0,27)||'...' else h_target.full_name end,'<br/>') WITHIN GROUP (ORDER BY h_target.id_holder_pk) AS holder_description_target, ")
				. // 3
				append(" blo.document_number, ")
				. // 4
				append(" bco.block_quantity, ")
				. // 5
				append(" blr.block_type, ")
				. // 6
				append(" blo.block_level ")
				. // 7
				append(" From change_ownership_detail cod ")
				.append(" inner join block_change_ownership bco on cod.id_change_ownership_detail_pk = bco.id_change_ownership_detail_fk ")
				.append(" inner join block_operation blo on bco.id_block_operation_fk = blo.id_block_operation_pk ")
				.append(" inner join block_request blr on blo.id_block_request_fk = blr.id_block_request_pk ")
				.append(" inner join holder_account ha_target on ha_target.id_holder_account_pk=bco.id_holder_account_fk ")
				.append(" inner join holder_account_detail had_target on ha_target.id_holder_account_pk=had_target.id_holder_account_fk ")
				.append(" inner join holder h_target on had_target.id_holder_fk=h_target.id_holder_pk ")
				.append(" Where cod.id_change_ownership_fk = :idChangeOwnershipPkPrm ")
				.append(" Group By cod.id_isin_code_fk,ha_target.account_number,blo.document_number,bco.block_quantity,blr.block_type,blo.block_level ")
				.append(" Order By cod.id_isin_code_fk,ha_target.account_number ");

		Query query = em.createNativeQuery(sbQuery.toString());

		query.setParameter("idChangeOwnershipPkPrm", idChangeOwnershipPk);

		List<Object[]> lstChangeOwnershipDetail = (List<Object[]>) query
				.getResultList();

		List<Object[]> lstChangeOwnershipToReturn = new ArrayList<Object[]>();
		Object[] arrObjChangeOwnership = null;

		for (Object[] changeOwnershipDetail : lstChangeOwnershipDetail) {
			arrObjChangeOwnership = new Object[17];

			// Add attributs of header
			arrObjChangeOwnership[0] = changeOwnershipHeader[0];
			arrObjChangeOwnership[1] = changeOwnershipHeader[1];
			arrObjChangeOwnership[2] = changeOwnershipHeader[2];
			arrObjChangeOwnership[3] = changeOwnershipHeader[3];
			arrObjChangeOwnership[4] = changeOwnershipHeader[4];
			arrObjChangeOwnership[5] = changeOwnershipHeader[5];
			arrObjChangeOwnership[6] = changeOwnershipHeader[6];
			arrObjChangeOwnership[7] = changeOwnershipHeader[7];
			arrObjChangeOwnership[8] = changeOwnershipHeader[8];

			// Add attributs of detail
			arrObjChangeOwnership[9] = changeOwnershipDetail[0];
			arrObjChangeOwnership[10] = changeOwnershipDetail[1];
			arrObjChangeOwnership[11] = changeOwnershipDetail[2];
			arrObjChangeOwnership[12] = changeOwnershipDetail[3];
			arrObjChangeOwnership[13] = changeOwnershipDetail[4];
			arrObjChangeOwnership[14] = changeOwnershipDetail[5];
			arrObjChangeOwnership[15] = changeOwnershipDetail[6];
			arrObjChangeOwnership[16] = changeOwnershipDetail[7];

			lstChangeOwnershipToReturn.add(arrObjChangeOwnership);
		}

		return lstChangeOwnershipToReturn;
	}

	//aqui trae el cat desde el repositorio de archivos
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updateAccreditationOperationCAT(Long accreditationId, ReportLogger reportLogger) throws ServiceException, IOException {
		
		Object[]  reportDisk= getReportLoggerFile(reportLogger.getIdReportLoggerPk());
		String reportDiskRute=(reportDisk[0].toString() +reportDisk[1].toString());
		Path path = Paths.get(reportDiskRute);
		byte[] reportData = Files.readAllBytes(path);
		
		AccreditationOperation accreditationOperation = find(AccreditationOperation.class, accreditationId);
		accreditationOperation.setCertificationFile(reportData);
		accreditationOperation.setIndGenerated(BooleanType.YES.getCode());
		accreditationOperation.setGeneratedDate(CommonsUtilities.currentDate());
		update(accreditationOperation);
		
	}
	
	public Object[]  getReportLoggerFile(Long idReportLoggerPk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("		SELECT  ");
		sbQuery.append("		rlf.NAME_TRACE, ");
		sbQuery.append("		rlf.NAME_FILE ");
		sbQuery.append("		from REPORT_LOGGER_FILE  rlf ");
		sbQuery.append("		where ID_REPORT_LOGGER_FK=:idReportLoggerPk  ");
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("idReportLoggerPk", idReportLoggerPk);
		return  (Object[]) query.getSingleResult();
	}

	public List<Object[]> getAccreditationCertification(
			AccreditationOperationTO accreditationOperationTO)
			throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		String currentDateString = CommonsUtilities.convertDateToString(
				CommonsUtilities.currentDate(), CommonsUtilities.DATE_PATTERN);

		sbQuery.append("		SELECT ");
		sbQuery.append("			   ( SELECT LISTAGG(TRIM(H_.FULL_NAME)||', '||(SELECT INDICATOR1 FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK=H_.DOCUMENT_TYPE)||' '||H_.DOCUMENT_NUMBER,', ')");
		sbQuery.append("			   WITHIN GROUP (ORDER BY HAD_.ID_HOLDER_FK) 																													");
		sbQuery.append("			   FROM HOLDER_ACCOUNT_DETAIL HAD_																																");
		sbQuery.append("			   INNER JOIN HOLDER H_ ON H_.ID_HOLDER_PK = HAD_.ID_HOLDER_FK																									");
		sbQuery.append("			   WHERE HAD_.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK																									");// 0
		sbQuery.append("			   ) AS HOLDER_DESCRIPTION,																																		");

		sbQuery.append("			   (SELECT LISTAGG(HAD_.ID_HOLDER_FK,', ') WITHIN GROUP (ORDER BY HAD_.ID_HOLDER_FK)																			");
		sbQuery.append("			   FROM HOLDER_ACCOUNT_DETAIL HAD_																																");
		sbQuery.append("			   WHERE HAD_.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK																									");
		sbQuery.append("			   ) AS CUI,																																					");// 1

		sbQuery.append("			   P.DESCRIPTION PART_DESC,");// 2
		sbQuery.append("			   S.DESCRIPTION SECURITY_DESC,");// 3
		sbQuery.append("			   (SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = AO.MOTIVE) MOTIVE_DESC,");// 4
		sbQuery.append("			   S.ID_SECURITY_CODE_PK,");// 5
		sbQuery.append("			   AO.EXPIRATION_DATE,");// 6
		sbQuery.append("			   CASE AO.VALIDITY_DAYS ");// 7
		sbQuery.append("			   	WHEN 0 THEN 1 ");// 7
		sbQuery.append("			   	ELSE AO.VALIDITY_DAYS ");// 7
		sbQuery.append("			   	END VALIDITY_DAYS, ");// 7
		sbQuery.append("			   (SELECT TEXT1 FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = S.SECURITY_CLASS) SECURITY_CLASS_DESC,");// 8
		sbQuery.append("			   (SELECT TEXT1 || ' - ' || PARAMETER_NAME FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = S.CURRENCY) MONEDA,");// 9
		sbQuery.append("			   S.CURRENT_NOMINAL_VALUE,");// 10
		sbQuery.append("			   TO_CHAR(S.ISSUANCE_DATE, 'DD/MM/YYYY') FECHA_EMISION,");// 11
		
		//SI ES CAT B
		if(Validations.validateIsNotNullAndNotEmpty(accreditationOperationTO.getIdReportPk())){
			if(accreditationOperationTO.getIdReportPk().equals(289L)
					|| accreditationOperationTO.getIdReportPk().equals(288L)){
				sbQuery.append("			   TRUNC(S.EXPIRATION_DATE) - TRUNC(S.ISSUANCE_DATE) PLAZO_VIDA,");// 12
			}else{
				sbQuery.append("			   REPLACE(TO_CHAR((S.EXPIRATION_DATE - TO_DATE('"+ currentDateString + "','DD/MM/YYYY')),'999,999,999'),',','.') PLAZO_VIDA,");// 12
			}
		}else{
			sbQuery.append("			   REPLACE(TO_CHAR((S.EXPIRATION_DATE - TO_DATE('"+ currentDateString + "','DD/MM/YYYY')),'999,999,999'),',','.') PLAZO_VIDA,");// 12
		}
		
		sbQuery.append("			   S.INTEREST_RATE,");// 13
		sbQuery.append("			   (SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = S.PERIODICITY) TASA_EMISION,");// 14
		sbQuery.append("			   PIC.COUPON_NUMBER COUPON_INTEREST,");// 15
		sbQuery.append("			   TO_CHAR(PIC.EXPERITATION_DATE, 'DD/MM/YYYY') EXPIRATION_DATE_INT,");// 16

		
		if(Validations.validateIsNotNullAndNotEmpty(accreditationOperationTO.getIdReportPk())){
			if(accreditationOperationTO.getIdReportPk().equals(289L)
					|| accreditationOperationTO.getIdReportPk().equals(288L)){
				sbQuery.append("	TRUNC(PIC.EXPERITATION_DATE) - TRUNC(PIC.BEGINING_DATE)  PLAZO_VIDA_CUPON_INT,	");
			}else{
				sbQuery.append("			   CASE																	");
				sbQuery.append("			   		WHEN PIC.STATE_PROGRAM_INTEREST = 1351							");
				sbQuery.append("			   			THEN 0														");
				sbQuery.append("			   		WHEN TRUNC(CURRENT_DATE) > TRUNC(PIC.BEGINING_DATE)				");
				sbQuery.append("			   			THEN TRUNC(PIC.EXPERITATION_DATE) - TRUNC(CURRENT_DATE)		");
				sbQuery.append("			   		ELSE TRUNC(PIC.EXPERITATION_DATE) - TRUNC(PIC.BEGINING_DATE)	");
				sbQuery.append("			   END PLAZO_VIDA_CUPON_INT,											");// 17
			}
		}else{
			sbQuery.append("			   CASE																	");
			sbQuery.append("			   		WHEN PIC.STATE_PROGRAM_INTEREST = 1351							");
			sbQuery.append("			   			THEN 0														");
			sbQuery.append("			   		WHEN TRUNC(CURRENT_DATE) > TRUNC(PIC.BEGINING_DATE)				");
			sbQuery.append("			   			THEN TRUNC(PIC.EXPERITATION_DATE) - TRUNC(CURRENT_DATE)		");
			sbQuery.append("			   		ELSE TRUNC(PIC.EXPERITATION_DATE) - TRUNC(PIC.BEGINING_DATE)	");
			sbQuery.append("			   END PLAZO_VIDA_CUPON_INT,											");// 17
		}
		
		sbQuery.append("            S.CALENDAR_DAYS,");// 18
		// 20 monto total al vencimiento si y solo si el estado es pendiente del
		// cupon
		// sbQuery.append("            (S.INTEREST_RATE*(S.CURRENT_NOMINAL_VALUE/s.calendar_days))*((pic.experitation_date-pic.begining_date)+1)*s.desmaterialized_balance,");
		// 21 monto total al vencimiento

		sbQuery.append("			   ROUND(PIC.PAYED_AMOUNT,2) PAYED_AMOUNT_INTEREST,");// 19
		sbQuery.append("			   (SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = PIC.STATE_PROGRAM_INTEREST) ESTADO_INT,");// 20
																																			// estado
																																			// del
																																			// cupon
		sbQuery.append("			   PIC.COUPON_NUMBER COUPON_AMORTIZATION,");// 21
		sbQuery.append("			   TO_CHAR(DECODE(PAC.EXPRITATION_DATE,NULL,PIC.EXPERITATION_DATE,PAC.EXPRITATION_DATE), 'DD/MM/YYYY') EXPIRATION_DATE_AMORT,");// 22
		sbQuery.append("			   DECODE(PAC.EXPRITATION_DATE,NULL,0,(PAC.EXPRITATION_DATE-PAC.BEGINING_DATE)+1) PLAZO_VIDA_CUPON_AMO,");// 23
		// 23 amortizacion de capital si y solo si el estado es pendiente del
		// cupon
		
		sbQuery.append(" CASE	");								 								 
		sbQuery.append(" 	WHEN S.DESCRIPTION like '%Q'	");					 
		sbQuery.append(" 		THEN S.CURRENT_NOMINAL_VALUE	");												 
		sbQuery.append(" 	ELSE  DECODE(PAC.COUPON_AMOUNT,NULL,0,PAC.COUPON_AMOUNT) "); 
		sbQuery.append(" END AMORT_CAPITAL,	"); //24
		
		//sbQuery.append("			   DECODE(PAC.COUPON_AMOUNT,NULL,0,PAC.COUPON_AMOUNT) AMORT_CAPITAL,");// 24
																									// --amortizacio
																									// de
																									// capital
		// 24 amortizacion de capital
		sbQuery.append("			   DECODE(PAC.PAYED_AMOUNT,NULL,0,PAC.PAYED_AMOUNT) PAYED_AMOUNT_AMORTIZATION,");// 25
		sbQuery.append("			   (SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = PAC.STATE_PROGRAM_AMORTIZATON) ESTADO_AMORT,");// 26
		sbQuery.append("			   REPLACE(TO_CHAR(AD.TOTAL_BALANCE,'999,999,999'),',','.')  ,");// 27
		sbQuery.append("			   REPLACE(TO_CHAR(AD.REPORTING_BALANCE,'999,999,999'),',','.'),");// 28
		sbQuery.append("			   REPLACE(TO_CHAR(AD.BAN_BALANCE,'999,999,999'),',','.'),");// 29
		sbQuery.append("			   REPLACE(TO_CHAR(AD.PAWN_BALANCE,'999,999,999'),',','.'),");// 30
		sbQuery.append("			   REPLACE(TO_CHAR(AD.AVAILABLE_BALANCE,'999,999,999'),',','.'),");// 31
		sbQuery.append("			   S.INSTRUMENT_TYPE,");// 32
		sbQuery.append("            PIC.BEGINING_DATE,");// 33
		sbQuery.append("            S.DESMATERIALIZED_BALANCE,");// 34
		sbQuery.append("			   PIC.EXPERITATION_DATE EXPIRATION_DATE_STDR,");// 35
		sbQuery.append("			   AO.REGISTER_DATE,");// 36
		sbQuery.append("			   HA.ACCOUNT_NUMBER,");// 37
		sbQuery.append("			   S.ID_SECURITY_CODE_ONLY,");// 38
		sbQuery.append("			   CASE");// 39 BUSCAR RELACION ENTRE ACCOUNT
										// ANNOTATION OPERATION Y
										// ACCREDITATION_OPERATION
		sbQuery.append("			   	WHEN S.SECURITY_CLASS IN (420,1976)");
		sbQuery.append("			   		THEN S.ALTERNATIVE_CODE ");
		sbQuery.append("			   	END ID_ISSUER_FK, ");
		sbQuery.append("			   S.ID_ISIN_CODE,");// 40
		sbQuery.append("			   S.CFI_CODE,");// 41
		sbQuery.append("			   ROUND(PIC.COUPON_AMOUNT,2),");// 42 --intereses
		sbQuery.append("			   AO.CERTIFICATE_NUMBER,");// 43
		sbQuery.append("			   CO.OPERATION_NUMBER,");// 44
		

		sbQuery.append("			   (NVL((SELECT COUNT(*) ");
		sbQuery.append("			   FROM PROGRAM_INTEREST_COUPON");
		sbQuery.append("			   WHERE ID_INT_PAYMENT_SCHEDULE_FK = IPS.ID_INT_PAYMENT_SCHEDULE_PK),0) + ");
		sbQuery.append("			   NVL((SELECT COUNT(*) ");
		sbQuery.append("			   FROM PROGRAM_AMORTIZATION_COUPON");
		sbQuery.append("			   WHERE ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK),0) + ");
		sbQuery.append("			   NVL(S.NUMBER_COUPONS,0)) NUMBER_COUPONS, ");// 45

		sbQuery.append("			   NVL((SELECT COUNT(*) ");
		sbQuery.append("			   FROM PROGRAM_INTEREST_COUPON");
		sbQuery.append("			   WHERE ID_INT_PAYMENT_SCHEDULE_FK = IPS.ID_INT_PAYMENT_SCHEDULE_PK),0) NUMBER_COUPONS_INT, ");// 46
		sbQuery.append("			   AO.SECURITY_CODE_CAT, "); // 47 CODIGO DE BARRAS
		sbQuery.append("			   S.INITIAL_NOMINAL_VALUE,  ");// 48 VALOR NOMINAL INICIAL
		sbQuery.append("			   I.BUSINESS_NAME,  ");// 49 DESCRIPCION ISSUER
		sbQuery.append("			   (SELECT PTS.PARAMETER_NAME FROM PARAMETER_TABLE PTS WHERE PTS.PARAMETER_TABLE_PK = S.SECURITY_CLASS),  ");// 50 DESCRIPCION COMPLETA CLASE DE VALOR
		sbQuery.append("			   ISS.ENDORSEMENT,  ");// 51 AVAL
		sbQuery.append("			   (select NAME from GEOGRAPHIC_LOCATION where ID_GEOGRAPHIC_LOCATION_PK   = I.DEPARTMENT) || ' - ' || TO_CHAR(I.CONSTITUTION_DATE,'DD/MM/YYYY'),  ");// 52 LUGAR_CONSTITUCION
		sbQuery.append("			   (select NAME from GEOGRAPHIC_LOCATION where ID_GEOGRAPHIC_LOCATION_PK   = S.ID_GEOGRAPHIC_LOCATION_FK),  ");// 53 LUGAR_EMISION_VALOR
		sbQuery.append("			   I.RMV_REGISTER,  ");// 54 INSCRIPCION_RMV
		sbQuery.append("			   I.LEGAL_ADDRESS,  ");// 55 DOMICILIO_EMISOR

		//ISSUE 940: si la clase de valor es PGS(1954): colocar valorNominalInicial, caso contrario Monto de Emision
		sbQuery.append("			   CASE WHEN S.SECURITY_CLASS = 1954 ");
		sbQuery.append("			   	THEN S.INITIAL_NOMINAL_VALUE  ");
		sbQuery.append("			   	ELSE ISS.ISSUANCE_AMOUNT  ");
		sbQuery.append("			   END,  "); // 56 MONTO_EMISION
		
		sbQuery.append("			   S.PLACE_PAYMENT,  ");// 57 LUGAR_PAGO
		sbQuery.append("			   CASE WHEN S.IND_ISSUANCE_PROTEST = 1 THEN 'SI' WHEN S.IND_ISSUANCE_PROTEST = 0 THEN 'NO' END,  ");// 58 EMISION_PROTESTO
		sbQuery.append("			   AO.CAT_TYPE,  ");// 59 TIPO_DECAT
		sbQuery.append("			   REPLACE(TO_CHAR(AD.OTHER_BALANCE,'999,999,999'),',','.')  ");// 60 OTRO SALDO BLOQUEADO
		sbQuery.append("		  , S.EXPIRATION_DATE SEC_EXPIRATION_DATE ");// 61 FECHA VENCIMIENTO
		sbQuery.append("			  , ISS.TESTIMONY_NUMBER,");// 62
		sbQuery.append("			   ISS.DESCRIPTION,");// 63

		//issue 940: el monto Capital que figura en el CAT es el Tramo de colocacion
		sbQuery.append("			   nvl((SELECT sum(DTC.PLACED_AMOUNT) FROM PLACEMENT_SEGMENT_DETAIL DTC");
		sbQuery.append("			   INNER JOIN PLACEMENT_SEGMENT TC ON TC.ID_PLACEMENT_SEGMENT_PK = DTC.ID_PLACEMENT_SEGMENT_FK");
		sbQuery.append("			   WHERE 0=0");
		sbQuery.append("			   and tc.STATE_PLACEMENT_SEGMENT not in ( 868, 580 )");
		sbQuery.append("			   AND DTC.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK");
		sbQuery.append("			   AND TC.ID_ISSUANCE_CODE_FK = S.ID_ISSUANCE_CODE_FK),0) AS TRAMO_COLOCACION,");// 64
		
		sbQuery.append("			   (S.SECURITY_CLASS)");// 65
		sbQuery.append("			   ,AD.ROLE  ");// 66 ROLE
		sbQuery.append("		  , S.EXPIRATION_FONDO_DATE SEC_EXPIRATION_FONDO_DATE ");// 67 FECHA EXPIRACION DE FONDO SOLO PARA CFC
		
		sbQuery.append("		  FROM ACCREDITATION_DETAIL AD																														");
		sbQuery.append("	INNER JOIN ACCREDITATION_OPERATION AO       ON AO.ID_ACCREDITATION_OPERATION_PK = AD.ID_ACCREDITATION_OPERATION_FK										");
		sbQuery.append("	INNER JOIN CUSTODY_OPERATION CO             ON AO.ID_ACCREDITATION_OPERATION_PK = CO.ID_CUSTODY_OPERATION_PK 											");
		sbQuery.append("	INNER JOIN HOLDER_ACCOUNT HA                ON AD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK 														");
		sbQuery.append("	LEFT JOIN PARTICIPANT P                    	ON P.ID_PARTICIPANT_PK = HA.ID_PARTICIPANT_FK																");
		sbQuery.append("	INNER JOIN SECURITY S                       ON S.ID_SECURITY_CODE_PK = AD.ID_SECURITY_CODE_FK 															");
		sbQuery.append("	INNER JOIN ISSUANCE ISS                     ON S.ID_ISSUANCE_CODE_FK = ISS.ID_ISSUANCE_CODE_PK 															");
		sbQuery.append("	INNER JOIN ISSUER I                       	ON I.ID_ISSUER_PK = S.ID_ISSUER_FK 																			");
		sbQuery.append("	LEFT JOIN INTEREST_PAYMENT_SCHEDULE IPS     ON IPS.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK 															");
		sbQuery.append("	LEFT JOIN PROGRAM_INTEREST_COUPON PIC      	ON PIC.ID_INT_PAYMENT_SCHEDULE_FK = IPS.ID_INT_PAYMENT_SCHEDULE_PK 											");
		sbQuery.append("	LEFT JOIN AMORTIZATION_PAYMENT_SCHEDULE APS ON APS.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK 															");
		sbQuery.append("	LEFT JOIN PROGRAM_AMORTIZATION_COUPON PAC  	ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK											");

		sbQuery.append("													AND ((PIC.EXPERITATION_DATE = PAC.EXPRITATION_DATE ");
		sbQuery.append("														AND (SELECT COUNT(*) ");
		sbQuery.append("															FROM PROGRAM_INTEREST_COUPON ");
		sbQuery.append("															WHERE ID_INT_PAYMENT_SCHEDULE_FK = IPS.ID_INT_PAYMENT_SCHEDULE_PK) > 0) ");
		sbQuery.append("														OR (SELECT COUNT(*) ");
		sbQuery.append("															FROM PROGRAM_INTEREST_COUPON ");
		sbQuery.append("															WHERE ID_INT_PAYMENT_SCHEDULE_FK = IPS.ID_INT_PAYMENT_SCHEDULE_PK) = 0 ");
		sbQuery.append("														) ");

		sbQuery.append(" WHERE  AO.ID_ACCREDITATION_OPERATION_PK= :idAccreditation																								");

		sbQuery.append(" ORDER BY PIC.COUPON_NUMBER																																");

		Query query = em.createNativeQuery(sbQuery.toString());

		query.setParameter("idAccreditation",
				accreditationOperationTO.getIdAccreditationOperationPk());

		return query.getResultList();
	}

	public List<RetirementOperationTO> searchRetirementOperations(RetirementOperationTO retirementOperationTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append("	SELECT distinct ro.idRetirementOperationPk, " );
		sbQuery.append("		ro.custodyOperation.registryDate, " );
		sbQuery.append("		ro.security.idSecurityCodePk, " );
		sbQuery.append("		ro.totalBalance, " );
		sbQuery.append("		ro.state, " );
		sbQuery.append("		( select pt.parameterName from ParameterTable pt where pt.parameterTablePk = ro.state ), " );
		sbQuery.append("		( select pt.parameterName from ParameterTable pt where pt.parameterTablePk = ro.motive ), " );
		sbQuery.append("		ro.custodyOperation.operationNumber, " );
		sbQuery.append("		ro.security.idSecurityBcbCode, " );
		sbQuery.append("		ro.security.securityClass " );
		sbQuery.append("	FROM RetirementDetail rd inner join rd.retirementOperation ro" );
		sbQuery.append("	WHERE ro.idRetirementOperationPk != null");
//		sbQuery.append("	WHERE TRUNC(ro.custodyOperation.registryDate) between :initialDate AND :endDate");
				
		/*
		if(Validations.validateIsNotNullAndPositive(retirementOperationTO.getIndWithdrawalBCB())){
			sbQuery.append("	 AND ro.indWithdrawalBCB = :indWithdrawalBCB ");
			parameters.put("indWithdrawalBCB", retirementOperationTO.getIndWithdrawalBCB());
		}*/
		
		if(Validations.validateIsNotNull(retirementOperationTO.getInitialDate()) &&
				Validations.validateIsNotNull(retirementOperationTO.getFinalDate())){
			sbQuery.append("	AND TRUNC(ro.custodyOperation.registryDate) between :initialDate AND :endDate");
			parameters.put("initialDate", retirementOperationTO.getInitialDate());
			parameters.put("endDate", retirementOperationTO.getFinalDate());
		}
		
		if(Validations.validateIsNotNullAndPositive(retirementOperationTO.getSecurityClass())){
			sbQuery.append("	AND ro.security.securityClass = :securityClass");
			parameters.put("securityClass", retirementOperationTO.getSecurityClass());
		}
		if(Validations.validateIsNotNullAndNotEmpty(retirementOperationTO.getIdParticipantPk())){
			sbQuery.append("	AND rd.participant.idParticipantPk = :idParticipantPk");
			parameters.put("idParticipantPk", retirementOperationTO.getIdParticipantPk());
		}
		
		if(Validations.validateIsNotNull(retirementOperationTO.getIdHolderPk())){
			sbQuery.append("	exists ( ");
			sbQuery.append("			select had.holder.idHolderPk from HolderAccount ha inner join HA.holderAccountDetails ");
			sbQuery.append("			where had.holder.idHolderPk = :idHolder ");
			sbQuery.append("			and had.holderAccount.idHolderAccountPk = rd.holderAccount.idHolderAccountPk ) ");
			parameters.put("idHolder", retirementOperationTO.getIdHolderPk());
		}
			
		if(Validations.validateIsNotNullAndNotEmpty(retirementOperationTO.getState())){
			sbQuery.append("	AND ro.state = :state");
			parameters.put("state", retirementOperationTO.getState());
		}
			
		if(Validations.validateIsNotNullAndNotEmpty(retirementOperationTO.getOperationNumber())){
			sbQuery.append("	AND ro.custodyOperation.operationNumber = :operationNumber");
			parameters.put("operationNumber", retirementOperationTO.getOperationNumber());
		}
			
		if(Validations.validateIsNotNull(retirementOperationTO.getSecurity()) && Validations.validateIsNotNull(retirementOperationTO.getSecurity().getIdSecurityCodePk())){
			sbQuery.append("	AND ( ro.security.idSecurityCodePk = :idSecurityCodePrm or ro.targetSecurity.idSecurityCodePk = :idSecurityCodePrm ) ");
			parameters.put("idSecurityCodePrm", retirementOperationTO.getSecurity().getIdSecurityCodePk());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(retirementOperationTO.getMotive())){
			sbQuery.append("	AND ro.motive = :motive");
			parameters.put("motive", retirementOperationTO.getMotive());
		}
		
		if (Validations.validateIsNotNull(retirementOperationTO.getIndWithdrawalBCB())) {
			sbQuery.append("	AND ro.indWithdrawalBCB = :indWithdrawalBCB");
			parameters.put("indWithdrawalBCB", retirementOperationTO.getIndWithdrawalBCB());
		}
		
		sbQuery.append("	ORDER BY ro.idRetirementOperationPk DESC");
		
		List<Object[]> lstResult = findListByQueryString(sbQuery.toString(), parameters);
		List<RetirementOperationTO> lstRetireOperation = new ArrayList<RetirementOperationTO>();
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){			
			for(Object[] arrObjects : lstResult){
				RetirementOperationTO retirementOperation = new RetirementOperationTO();
				retirementOperation.setIdRetirementOperationPk((Long)arrObjects[0]);
				retirementOperation.setInitialDate((Date)arrObjects[1]);
				retirementOperation.setIdSecurityCodePk((String)arrObjects[2]);
				retirementOperation.setTotalBalance((BigDecimal)arrObjects[3]);
				retirementOperation.setState((Integer)arrObjects[4]);
				retirementOperation.setStateDescription((String)arrObjects[5]);
				retirementOperation.setMotiveDescription((String)arrObjects[6]);
				retirementOperation.setOperationNumber((Long)arrObjects[7]);
				if (Validations.validateIsNotNull(arrObjects[8])) {
					retirementOperation.setIdSecurityBCBcode(arrObjects[8].toString());
				}
				retirementOperation.setSecurityClass(new Integer(arrObjects[9].toString()));
				lstRetireOperation.add(retirementOperation);
			}
		}		
		return lstRetireOperation;				
	}

	/**
	 * Get Digital Signature by management service bean.
	 * 
	 * @param typeSignature
	 *            the Type Digital Signature
	 * @return the list Digital Signature
	 * @throws ServiceException
	 *             the service exception
	 */

	@SuppressWarnings("unchecked")
	public List<DigitalSignature> getListDigitalSignatureBlob(
			Integer typeSignature) throws ServiceException {
		List<DigitalSignature> lstDigitalSignature = new ArrayList<DigitalSignature>();
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" Select ds from DigitalSignature ds ");
			sbQuery.append(" where ds.typeSignature = :typeSignature and ds.activeSignature = 1");
			sbQuery.append(" order by ds.idDigitalSignaturePk desc ");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("typeSignature", typeSignature);
			lstDigitalSignature = (List<DigitalSignature>) query
					.getResultList();
		} catch (NoResultException e) {
			return null;
		}
		return lstDigitalSignature;
	}

	/**
	 * Search holders.
	 * 
	 * @param idHolderAccountPk
	 *            the id holder account pk
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
	public List<Holder> searchHolders(Long idHolderAccountPk)
			throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> mapParam = new HashMap<String, Object>();
		sbQuery.append("  SELECT ");
		sbQuery.append("         HAD.holder.idHolderPk,");
		sbQuery.append("         HAD.holder.fullName,");
		sbQuery.append("         HAD.holder.documentType,");
		sbQuery.append("         HAD.holder.documentNumber");
		sbQuery.append("   FROM  HolderAccountDetail HAD");
		sbQuery.append("  WHERE  HAD.holderAccount.idHolderAccountPk=:idHolderAccountPk");

		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idHolderAccountPk", idHolderAccountPk);

		List<Object> objects = (List<Object>) query.getResultList();
		List<Holder> holders = new ArrayList<Holder>();
		for (int i = 0; i < objects.size(); ++i) {
			Object[] object = (Object[]) objects.get(i);
			Holder holder = new Holder();

			holder.setIdHolderPk((Long) object[0]);
			holder.setFullName((String) object[1]);
			holder.setDocumentType((Integer) object[2]);
			holder.setDocumentNumber((String) object[3]);
			holders.add(holder);
		}
		return holders;
	}

	public List<Object[]> getAccreditationOperations(
			AccreditationOperationTO accreditationOperationTO)
			throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> mapParam = new HashMap<String, Object>();
		sbQuery.append("    SELECT ");
		sbQuery.append("           AO.idAccreditationOperationPk,");
		sbQuery.append("           AO.petitionerType,");
		sbQuery.append("           AO.holder.idHolderPk,");
		sbQuery.append("           AO.holder.fullName,");
		sbQuery.append("           AO.certificationType,");
		sbQuery.append("           AO.registerDate,");
		sbQuery.append("           AO.expirationDate,");
		sbQuery.append("           AO.accreditationState,");
		sbQuery.append("           AD.availableBalance,");
		sbQuery.append("           AD.banBalance,");
		sbQuery.append("           AD.pawnBalance,");
		sbQuery.append("           AD.idParticipantFk.idParticipantPk,");
		sbQuery.append("           AD.idParticipantFk.mnemonic,");
		sbQuery.append("           AD.totalBalance,");
		sbQuery.append("           AD.idIsinCodeFk,");
		sbQuery.append("           AD.holderAccount.accountNumber,");
		sbQuery.append("           AD.otherBalance");
		sbQuery.append("   	  FROM AccreditationDetail AD");
		sbQuery.append("      JOIN AD.accreditationOperation AO");
		sbQuery.append("     WHERE 1=1");

		if (Validations.validateIsNotNull(accreditationOperationTO.getHolder())) {
			sbQuery.append(" AND AO.holder.idHolderPk =:idHolderPk");
			mapParam.put("idHolderPk", accreditationOperationTO.getHolder());
		}

		if (Validations.validateIsNotNull(accreditationOperationTO
				.getParticipant())) {
			sbQuery.append(" AND AD.idParticipantFk.idParticipantPk =:idParticipantPk");
			mapParam.put("idParticipantPk",
					accreditationOperationTO.getParticipant());
		}

		if (Validations.validateIsNotNull(accreditationOperationTO
				.getPetitionerType())) {
			sbQuery.append(" AND AO.petitionerType =:petitionerType");
			mapParam.put("petitionerType",
					accreditationOperationTO.getPetitionerType());
		}

		if (Validations.validateIsNotNull(accreditationOperationTO
				.getSecurityCode())) {
			sbQuery.append(" AND AD.idIsinCodeFk =:isinCode");
			mapParam.put("isinCode", accreditationOperationTO.getSecurityCode());
		}

		if (Validations.validateIsNotNull(accreditationOperationTO
				.getCertificationType())) {
			sbQuery.append(" AND AO.certificationType =:certificationType");
			mapParam.put("certificationType",
					accreditationOperationTO.getCertificationType());
		}

		if (Validations.validateIsNotNull(accreditationOperationTO
				.getAccreditationState())) {
			sbQuery.append(" AND AO.accreditationState =:state");
			mapParam.put("state",
					accreditationOperationTO.getAccreditationState());
		}

		if (Validations.validateIsNotNull(accreditationOperationTO
				.getInitialDate())
				&& Validations.validateIsNotNull(accreditationOperationTO
						.getFinalDate())) {
			sbQuery.append("     AND AO.registerDate >=:initialDate");
			sbQuery.append("     AND AO.registerDate <=:endDate");
			mapParam.put("initialDate",
					accreditationOperationTO.getInitialDate());
			mapParam.put("endDate", accreditationOperationTO.getFinalDate());
		}
		// CREATING QUERY
		Query query = em.createQuery(sbQuery.toString());
		// Building Itarator
		@SuppressWarnings("rawtypes")
		Iterator it = mapParam.entrySet().iterator();
		// Iterating
		while (it.hasNext()) {
			// Building the entry
			Entry<String, Object> entry = (Entry<String, Object>) it.next();
			// Setting Param
			query.setParameter(entry.getKey(), entry.getValue());
		}
		return query.getResultList();

	}

	public BlockRequest searchBlockRequestFromAccreditation(
			Long idAccreditationOperationPk) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		// Creating query
		sbQuery.append("Select BOC.blockOperation.blockRequest.blockEntity.idBlockEntityPk, ");
		sbQuery.append("       BOC.blockOperation.blockRequest.blockEntity.fullName,");
		sbQuery.append("       BOC.blockOperation.blockRequest.blockNumber,");
		sbQuery.append("       BOC.blockOperation.blockRequest.blockNumberDate,");
		sbQuery.append("       BOC.blockOperation.blockRequest.blockType");
		sbQuery.append("  FROM BlockOperationCertification BOC ");
		sbQuery.append(" where BOC.accreditationDetail.accreditationOperation.idAccreditationOperationPk =:idAccreditationPk");
		Query query = em.createQuery(sbQuery.toString());
		// Setting parameter
		query.setParameter("idAccreditationPk", idAccreditationOperationPk);
		// getting result list
		List<Object> objectList = query.getResultList();
		BlockRequest blockRequest = new BlockRequest();
		for (int i = 0; i < objectList.size(); ++i) {
			BlockEntity blockEntityTmp = new BlockEntity();
			Object[] sResults = (Object[]) objectList.get(i);
			blockEntityTmp.setIdBlockEntityPk((Long) sResults[0]);
			blockEntityTmp.setFullName((String) sResults[1]);

			blockRequest.setBlockEntity(blockEntityTmp);
			blockRequest.setBlockNumber((String) sResults[2]);
			blockRequest.setBlockNumberDate((Date) sResults[3]);
			blockRequest.setBlockType((Integer) sResults[4]);

			break;
		}
		return blockRequest;
	}

	public List<Object[]> retirementSecuritiesOperations(
			RetirementOperationTO retirementOperationTO)
			throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> mapParam = new HashMap<String, Object>();

		sbQuery.append("    SELECT ");
		sbQuery.append("           DISTINCT RD.idRetirementDetailPk,");
		sbQuery.append("           RO.idRetirementOperationPk,");
		sbQuery.append("           P.idParticipantPk,");
		sbQuery.append("           P.description,");
		sbQuery.append("           SE.idIsinCodePk,");
		sbQuery.append("           HA.idHolderAccountPk,");
		sbQuery.append("           HA.accountNumber,");
		sbQuery.append("           RO.motive,");
		sbQuery.append("           custody.registryDate,");
		sbQuery.append("           RD.accountBalance,");
		sbQuery.append("           RO.state,");
		sbQuery.append("           CO.idCorporativeOperationPk,");
		sbQuery.append("           HA.accountType,");
		sbQuery.append("           P.mnemonic");
		sbQuery.append("      FROM RetirementDetail RD");
		sbQuery.append("      JOIN RD.retirementOperation RO");
		sbQuery.append("      JOIN RD.holderAccount HA");
		sbQuery.append("   	  JOIN HA.holderAccountDetails HAD");
		sbQuery.append(" LEFT JOIN RO.corporativeOperation CO");
		sbQuery.append("   	  JOIN RO.idParticipantFk P");
		sbQuery.append("   	  JOIN RO.custodyOperation custody");
		sbQuery.append("   	  JOIN RO.idIsinCodeFk SE");
		sbQuery.append("     WHERE 1=1");

		if (Validations
				.validateIsNotNull(retirementOperationTO.getIdHolderPk())) {
			sbQuery.append(" AND HAD.holder.idHolderPk =:idHolderPk");
			mapParam.put("idHolderPk", retirementOperationTO.getIdHolderPk());
		}

		if (Validations.validateIsNotNull(retirementOperationTO
				.getIdRetirementOperationPk())) {
			sbQuery.append(" AND RO.idRetirementOperationPk =:idRetirementOperationPk");
			mapParam.put("idRetirementOperationPk",
					retirementOperationTO.getIdRetirementOperationPk());
		}

		if (Validations.validateIsNotNull(retirementOperationTO
				.getAccountNumber())) {
			sbQuery.append(" AND RD.holderAccount.accountNumber =:accountNumber");
			mapParam.put("accountNumber",
					retirementOperationTO.getAccountNumber());
		}
		if (Validations.validateIsNotNull(retirementOperationTO
				.getIdIsinCodeFk())) {
			sbQuery.append(" AND SE.idIsinCodePk =:idIsinCodeFk");
			mapParam.put("idIsinCodeFk",
					retirementOperationTO.getIdIsinCodeFk());
		}
		if (Validations.validateIsNotNull(retirementOperationTO.getState())) {
			sbQuery.append(" AND RO.state =:state");
			mapParam.put("state", retirementOperationTO.getState());
		}
		if (Validations.validateIsNotNull(retirementOperationTO.getMotive())) {
			sbQuery.append(" AND RO.motive =:motive");
			mapParam.put("motive", retirementOperationTO.getMotive());
		}
		if (Validations.validateIsNotNull(retirementOperationTO
				.getInitialDate())
				&& Validations.validateIsNotNull(retirementOperationTO
						.getFinalDate())) {
			sbQuery.append("     AND RO.custodyOperation.registryDate >=:initialDate");
			sbQuery.append("     AND RO.custodyOperation.registryDate <=:endDate");
			mapParam.put("initialDate", retirementOperationTO.getInitialDate());
			mapParam.put("endDate", retirementOperationTO.getFinalDate());
		}
		// CREATING QUERY
		Query query = em.createQuery(sbQuery.toString());
		// Building Itarator
		@SuppressWarnings("rawtypes")
		Iterator it = mapParam.entrySet().iterator();
		// Iterating
		while (it.hasNext()) {
			// Building the entry
			Entry<String, Object> entry = (Entry<String, Object>) it.next();
			// Setting Param
			query.setParameter(entry.getKey(), entry.getValue());
		}
		return query.getResultList();
	}

	public List<Object[]> getAccountAnnotationOperationByFilter(
			AnnotationOperationTO filter) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT DISTINCT");
		sbQuery.append("       P.DESCRIPTION || ' - '||P.ID_PARTICIPANT_PK as participant,"); // 0
		sbQuery.append("       AAO.ID_ANNOTATION_OPERATION_PK as operation,");// 1
		sbQuery.append("       AAO.ID_ISIN_CODE_FK,");// 2
		sbQuery.append("       HA.ACCOUNT_NUMBER,");// 3
		sbQuery.append("       HA.ID_HOLDER_ACCOUNT_PK, ");// 4
		sbQuery.append("       AAO.CERTIFICATE_NUMBER,");// 5
		sbQuery.append("       AAO.SERIAL_NUMBER,");// 6
		sbQuery.append("       AAO.MOTIVE,"); // 7
		sbQuery.append("       CO.REGISTRY_DATE,");// 8;
		sbQuery.append("       AAO.TOTAL_BALANCE,");// 9
		sbQuery.append("       AAO.STATE_ANNOTATION, ");// 10
		sbQuery.append("       P.ID_PARTICIPANT_PK, ");// 11
		sbQuery.append("       HA.DESCRIPTION"); // 12
		sbQuery.append("       FROM ACCOUNT_ANNOTATION_OPERATION AAO ");
		sbQuery.append("INNER JOIN HOLDER_ACCOUNT HA ");
		sbQuery.append("          ON AAO.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append("INNER JOIN PARTICIPANT P ");
		sbQuery.append("          ON HA.ID_PARTICIPANT_FK=P.ID_PARTICIPANT_PK ");
		sbQuery.append("INNER JOIN CUSTODY_OPERATION CO ");
		sbQuery.append("          ON AAO.ID_ANNOTATION_OPERATION_PK=CO.ID_CUSTODY_OPERATION_PK ");
		sbQuery.append("INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ");
		sbQuery.append("          ON HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK ");
		// sbQuery.append(" WHERE p.ID_PARTICIPANT_PK=55 ");
		sbQuery.append("where 1=1 ");

		if (Validations.validateIsNotNull(filter.getIdParticipantPk())) {
			sbQuery.append(" and P.ID_PARTICIPANT_PK=:idParticipant");
		}

		if (Validations.validateIsNotNull(filter.getRnt())) {
			sbQuery.append(" and HAD.ID_HOLDER_FK=:rnt");
		}

		if (Validations.validateIsNotNull(filter.getAccountNumber())) {
			sbQuery.append(" and HA.ACCOUNT_NUMBER=:accountNumber");
		}

		if (Validations.validateIsNotNull(filter.getIdIsinCode())) {
			sbQuery.append(" and AAO.ID_ISIN_CODE_FK=:isinCode");
		}

		if (Validations.validateIsNotNull(filter.getState())) {
			sbQuery.append(" and AAO.STATE_ANNOTATION=:state");
		}

		if (Validations.validateIsNotNull(filter.getInitialDate())
				&& Validations.validateIsNotNull(filter.getFinalDate())) {
			sbQuery.append(" and CO.REGISTRY_DATE between :initalDate and :finalDate ");
		}

		sbQuery.append("ORDER BY P.ID_PARTICIPANT_PK ");

		Query query = em.createNativeQuery(sbQuery.toString());

		if (Validations.validateIsNotNull(filter.getRnt())) {
			query.setParameter("rnt", filter.getRnt());
		}

		if (Validations.validateIsNotNull(filter.getIdParticipantPk())) {
			query.setParameter("idParticipant", filter.getIdParticipantPk());
		}

		if (Validations.validateIsNotNull(filter.getAccountNumber())) {
			query.setParameter("accountNumber", filter.getAccountNumber());
		}

		if (Validations.validateIsNotNull(filter.getIdIsinCode())) {
			query.setParameter("isinCode", filter.getIdIsinCode());
		}

		if (Validations.validateIsNotNull(filter.getState())) {
			query.setParameter("state", filter.getState());
			// sbQuery.append(" and AAO.STATE_ANNOTATION=:state");
		}

		try {
			if (Validations.validateIsNotNull(filter.getInitialDate())
					&& Validations.validateIsNotNull(filter.getFinalDate())) {
				query.setParameter("initalDate", filter.getInitialDate());
				query.setParameter("finalDate", filter.getFinalDate());
			}
		} catch (Exception es) {

			es.printStackTrace();
		}
		// return query.getResultList();

		List<Object[]> list = query.getResultList();

		return list;
	}

	/* it only returns the query to send to the jasper as PARAMETER */
	public String getQueryDirectTreasureOperation(String operation,
			String operationType) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" SELECT ");

		sbQuery.append(" 	(SELECT HA.ACCOUNT_NUMBER FROM HOLDER_ACCOUNT HA WHERE HA.ID_HOLDER_ACCOUNT_PK=AAO.ID_HOLDER_ACCOUNT_FK) CUENTA_TITULAR, ");
		
		sbQuery.append(" 	(SELECT LISTAGG(HAD.ID_HOLDER_FK,'<BR>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
		sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK = AAO.ID_HOLDER_ACCOUNT_FK) AS ID_HOLDER_PK, ");

		sbQuery.append(" 	(SELECT LISTAGG((SELECT INDICATOR1 FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = H.DOCUMENT_TYPE),'<BR>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
		sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
		sbQuery.append(" 	WHERE HAD.ID_HOLDER_ACCOUNT_FK = AAO.ID_HOLDER_ACCOUNT_FK ) AS DOCUMENT_TYPE, ");

		sbQuery.append(" 	(SELECT LISTAGG(H.DOCUMENT_NUMBER,'<BR>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
		sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
		sbQuery.append(" 	WHERE HAD.ID_HOLDER_ACCOUNT_FK = AAO.ID_HOLDER_ACCOUNT_FK ) AS DOCUMENT_NUMBER, ");

		sbQuery.append(" 	(SELECT LISTAGG(H.FULL_NAME,'<BR>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
		sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
		sbQuery.append(" 	WHERE HAD.ID_HOLDER_ACCOUNT_FK = AAO.ID_HOLDER_ACCOUNT_FK ) AS FULL_NAME, ");

		sbQuery.append(" 	(SELECT LISTAGG((SELECT PARAMETER_NAME FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = H.HOLDER_TYPE),'<BR>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
		sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
		sbQuery.append(" 	WHERE HAD.ID_HOLDER_ACCOUNT_FK = AAO.ID_HOLDER_ACCOUNT_FK ) AS HOLDER_TYPE, ");

		sbQuery.append(" 	'C' OPERATION, ");
		sbQuery.append(" 	S.ID_SECURITY_CODE_PK, ");
		sbQuery.append(" 	S.CURRENT_NOMINAL_VALUE, ");
		sbQuery.append(" 	AAO.TOTAL_BALANCE, ");

		sbQuery.append(" 	(SELECT GL.CODE_GEOGRAPHIC_LOCATION FROM GEOGRAPHIC_LOCATION GL WHERE GL.ID_GEOGRAPHIC_LOCATION_PK = AAO.DEPARTMENT) DEPARTMENT, ");
		sbQuery.append(" 	(SELECT GL.NAME FROM GEOGRAPHIC_LOCATION GL WHERE GL.ID_GEOGRAPHIC_LOCATION_PK = AAO.MUNICIPALITY) MUNICIPALITY, ");

		sbQuery.append(" 	(SELECT PT.DESCRIPTION FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK=S.CURRENCY) MONEDA ");

		sbQuery.append(" FROM ACCOUNT_ANNOTATION_OPERATION AAO ");
		sbQuery.append(" 	INNER JOIN CUSTODY_OPERATION CO ON CO.ID_CUSTODY_OPERATION_PK = AAO.ID_ANNOTATION_OPERATION_PK ");
		sbQuery.append(" 	INNER JOIN SECURITY S ON AAO.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK ");

		sbQuery.append(" WHERE AAO.STATE_ANNOTATION = 858 ");
		sbQuery.append(" 	AND ($P{security_code} IS NULL OR S.ID_SECURITY_CODE_PK = $P{security_code}) ");
		sbQuery.append(" 	AND S.SECURITY_CLASS = ");
		sbQuery.append(" 		CASE " + operation + " ");
		sbQuery.append(" 			WHEN 2 THEN 414 ");
		sbQuery.append(" 			ELSE 415 ");
		sbQuery.append(" 		END ");
		sbQuery.append(" 	AND TRUNC(CO.OPERATION_DATE) = TO_DATE($P{date},'DD/MM/YYYY') ");
		sbQuery.append(" 	AND " + operationType + " = 1 ");

		sbQuery.append(" UNION ALL");

		sbQuery.append(" SELECT ");

		sbQuery.append(" 	(SELECT HA.ACCOUNT_NUMBER FROM HOLDER_ACCOUNT HA WHERE HA.ID_HOLDER_ACCOUNT_PK=RD.ID_HOLDER_ACCOUNT_FK) CUENTA_TITULAR, ");
		
		sbQuery.append(" 	(SELECT LISTAGG(HAD.ID_HOLDER_FK,'<BR>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
		sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK = RD.ID_HOLDER_ACCOUNT_FK) AS ID_HOLDER_PK, ");

		sbQuery.append(" 	(SELECT LISTAGG((SELECT INDICATOR1 FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = H.DOCUMENT_TYPE),'<BR>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
		sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
		sbQuery.append(" 	WHERE HAD.ID_HOLDER_ACCOUNT_FK = RD.ID_HOLDER_ACCOUNT_FK ) AS DOCUMENT_TYPE, ");

		sbQuery.append(" 	(SELECT LISTAGG(H.DOCUMENT_NUMBER,'<BR>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
		sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
		sbQuery.append(" 	WHERE HAD.ID_HOLDER_ACCOUNT_FK = RD.ID_HOLDER_ACCOUNT_FK ) AS DOCUMENT_NUMBER, ");

		sbQuery.append(" 	(SELECT LISTAGG(H.FULL_NAME,'<BR>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
		sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
		sbQuery.append(" 	WHERE HAD.ID_HOLDER_ACCOUNT_FK = RD.ID_HOLDER_ACCOUNT_FK ) AS FULL_NAME, ");

		sbQuery.append(" 	(SELECT LISTAGG((SELECT PARAMETER_NAME FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = H.HOLDER_TYPE),'<BR>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
		sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
		sbQuery.append(" 	WHERE HAD.ID_HOLDER_ACCOUNT_FK = RD.ID_HOLDER_ACCOUNT_FK ) AS HOLDER_TYPE, ");

		sbQuery.append(" 	'A' OPERATION, ");
		sbQuery.append(" 	S.ID_SECURITY_CODE_PK, ");
		sbQuery.append(" 	S.CURRENT_NOMINAL_VALUE, ");
		sbQuery.append(" 	RO.TOTAL_BALANCE, ");

		sbQuery.append(" 	'' DEPARTMENT, ");
		sbQuery.append(" 	'' MUNICIPALITY, ");

		sbQuery.append(" 	(SELECT PT.DESCRIPTION FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK=S.CURRENCY) MONEDA ");

		sbQuery.append(" FROM RETIREMENT_OPERATION RO ");
		sbQuery.append(" 	INNER JOIN RETIREMENT_DETAIL RD ON RD.ID_RETIREMENT_OPERATION_FK = RO.ID_RETIREMENT_OPERATION_PK ");
		sbQuery.append(" 	INNER JOIN CUSTODY_OPERATION CO ON CO.ID_CUSTODY_OPERATION_PK = RO.ID_RETIREMENT_OPERATION_PK ");
		sbQuery.append(" 	INNER JOIN SECURITY S ON RO.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK ");

		sbQuery.append(" 	WHERE CO.OPERATION_STATE = 1144 ");
		sbQuery.append(" 		AND RO.IND_RETIREMENT_EXPIRATION <> 1");//Excluir las Operaciones de Retiro de valores Vencidos
		sbQuery.append(" 		AND ($P{security_code} IS NULL OR S.ID_SECURITY_CODE_PK = $P{security_code}) ");
		sbQuery.append(" 		AND S.SECURITY_CLASS = ");
		sbQuery.append(" 			CASE " + operation + " ");
		sbQuery.append(" 				WHEN 2 THEN 414 ");
		sbQuery.append(" 				ELSE 415 ");
		sbQuery.append(" 			END ");
		sbQuery.append(" 		AND TRUNC(CO.OPERATION_DATE) = TO_DATE($P{date},'DD/MM/YYYY') ");
		sbQuery.append(" 		AND " + operationType + " = 2 ");

		sbQuery.append(" order by 13,8,4 ");
		sbQuery.append("  ");

		return sbQuery.toString();
	}

	public String getRelationshipOfRelockOfValuesReport(
			GenericCustodyOperationTO gfto) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" select  ");
		sbQuery.append(" p.ID_PARTICIPANT_PK, ");
		sbQuery.append(" '(' ||p.mnemonic||') ' || p.description, ");
		sbQuery.append(" se.id_security_code_pk, ");
		sbQuery.append(" se.security_class, ");
		sbQuery.append(" Se.Alternative_Code, ");
		sbQuery.append(" bo.id_block_operation_pk, ");
		sbQuery.append(" br.block_number, ");
		sbQuery.append(" ho.id_holder_pk as cui_debtor, ");
		sbQuery.append(" ho.full_name as name_debtor, ");
		sbQuery.append(" be.id_holder_fk as cui_creditor, ");
		sbQuery.append(" be.full_name as name_creditor, ");
		sbQuery.append(" br.block_type, ");
		sbQuery.append(" Bo.Block_State, ");
		sbQuery.append(" bo.original_block_balance, ");
		sbQuery.append(" bo.actual_block_balance, ");
		sbQuery.append(" co.registry_date ");
		sbQuery.append(" from block_operation bo, block_request br , security se , holder ho , block_entity be , participant p , custody_operation co ");
		sbQuery.append(" where bo.id_block_request_fk = br.id_block_request_pk ");
		sbQuery.append(" and bo.id_security_code_fk = se.id_security_code_pk ");
		sbQuery.append(" and br.id_holder_fk = ho.id_holder_pk ");
		sbQuery.append(" and br.id_block_entity_fk = be.id_block_entity_pk ");
		sbQuery.append(" and bo.id_participant_fk = p.id_participant_pk ");
		sbQuery.append(" and bo.id_block_operation_pk = co.id_custody_operation_pk ");
		sbQuery.append(" and bo.block_level = 1120 ");
		sbQuery.append(" and Bo.Block_State = 1141 ");
		sbQuery.append(" and co.registry_date between to_date('"
				+ gfto.getInitialDate() + "','dd/MM/yyyy') and to_date('"
				+ gfto.getFinalDate() + "','dd/MM/yyyy')");

		if (Validations.validateIsNotNull(gfto.getParticipant())) {
			sbQuery.append(" and p.id_participant_pk=" + gfto.getParticipant()
					+ " ");
		}
		if (Validations.validateIsNotNull(gfto.getHolder())) {
			sbQuery.append(" and ho.id_holder_pk=" + gfto.getHolder() + " ");
		}
		if (Validations.validateIsNotNull(gfto.getIssuer())) {
			sbQuery.append(" and se.id_issuer_fk='"
					+ gfto.getIssuer().toString() + "' ");
		}
		if (Validations.validateIsNotNull(gfto.getBlockType())) {
			sbQuery.append(" and br.block_type=" + gfto.getBlockType() + " ");
		}
		sbQuery.append(" order by  p.description,se.id_security_code_pk ");
		sbQuery.append("  ");

		return sbQuery.toString();
	}

	public Holder searchHolder(Long idHolderPk) {
		StringBuilder stringBuilderSql = new StringBuilder();
		stringBuilderSql
				.append("SELECT h FROM Holder h WHERE h.idHolderPk = :idHolderPk");

		Query query = em.createQuery(stringBuilderSql.toString());
		query.setParameter("idHolderPk", idHolderPk);

		return (Holder) query.getSingleResult();
	}

	public String getEarlyRedemptionsOfDirectSaleBCBReport(GenericCustodyOperationTO gfto,Integer institutionType){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT "); 
		sbQuery.append(" (select pt.description from Parameter_Table pt where pt.parameter_table_pk = RO.STATE) as STATE, ");
		sbQuery.append(" (select pa.description from participant pa where pa.id_participant_pk = hab.id_participant_pk) as DESC_PARTICIPANT, ");
		sbQuery.append(" S.ID_SECURITY_CODE_PK CLASE_CLAVE_VALOR, ");
		sbQuery.append(" S.SECURITY_CLASS, ");
		sbQuery.append(" S.CURRENCY AS MONEDA, ");
		sbQuery.append(" S.ISSUANCE_DATE FECHA_EMI, ");
		sbQuery.append(" S.EXPIRATION_DATE FECHA_EXP, ");
		sbQuery.append(" S.RATE_VALUE TASA, ");
		sbQuery.append(" HA.ACCOUNT_NUMBER CUENTA, ");
		sbQuery.append(" DECODE(NVL(HAB.ACCREDITATION_BALANCE,0)+NVL(HAB.BAN_BALANCE,0)+NVL(HAB.OTHER_BLOCK_BALANCE,0)+NVL(HAB.PAWN_BALANCE,0),0,'NO','SI') BLOQUEO, ");
		sbQuery.append(" (select RD.AVAILABLE_BALANCE from RETIREMENT_DETAIL RD ");
		sbQuery.append(" where RD.ID_RETIREMENT_OPERATION_FK = RO.ID_RETIREMENT_OPERATION_PK ");
		sbQuery.append(" and RD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK) as SALDO_TOTAL ,");
		//sbQuery.append(" RO.TOTAL_BALANCE SALDO_TOTAL, ");
		//sbQuery.append(" S.CURRENT_NOMINAL_VALUE VALOR_UNIT_FINAL, ");
		sbQuery.append(" S.Initial_Nominal_Value VALOR_UNIT_FINAL, ");
		//sbQuery.append(" (S.CURRENT_NOMINAL_VALUE*RO.TOTAL_BALANCE) MONTO_FINAL, ");
		sbQuery.append(" (S.Initial_Nominal_Value*(select RD.AVAILABLE_BALANCE from RETIREMENT_DETAIL RD where RD.ID_RETIREMENT_OPERATION_FK = RO.ID_RETIREMENT_OPERATION_PK and RD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK)) MONTO_FINAL, ");
		//sbQuery.append(" (S.CURRENT_NOMINAL_VALUE*(select RD.AVAILABLE_BALANCE from RETIREMENT_DETAIL RD where RD.ID_RETIREMENT_OPERATION_FK = RO.ID_RETIREMENT_OPERATION_PK and RD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK)) MONTO_FINAL, ");
		sbQuery.append(" CO.OPERATION_DATE FECHA_COBRO, ");
		sbQuery.append(" (SELECT '('|| I.MNEMONIC ||')'|| I.BUSINESS_NAME FROM ISSUER I WHERE I.ID_ISSUER_PK=S.ID_ISSUER_FK) AS BANCO, ");
		sbQuery.append(" HA.ID_HOLDER_ACCOUNT_PK, ");
		sbQuery.append(" S.REGISTRY_DATE FECHA_REG ");
		sbQuery.append(" FROM RETIREMENT_OPERATION RO, SECURITY S, RETIREMENT_DETAIL ROD, HOLDER_ACCOUNT HA, ");
		sbQuery.append(" HOLDER_ACCOUNT_BALANCE HAB, CUSTODY_OPERATION CO "); 
		sbQuery.append(" WHERE ");
		sbQuery.append(" RO.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK ");
		sbQuery.append(" AND RO.IND_RETIREMENT_EXPIRATION <> 1 "); //Excluir las Operaciones de Retiro de valores Vencidos
		sbQuery.append(" AND RO.ID_RETIREMENT_OPERATION_PK = ROD.ID_RETIREMENT_OPERATION_FK ");
		sbQuery.append(" AND ROD.ID_HOLDER_ACCOUNT_FK = HAB.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append(" AND HAB.ID_HOLDER_ACCOUNT_PK = HA.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append(" AND HAB.ID_SECURITY_CODE_PK = S.ID_SECURITY_CODE_PK  ");
		sbQuery.append(" AND S.SECURITY_CLASS = 415 ");
		if(institutionType.equals(InstitutionType.PARTICIPANT.getCode())||institutionType.equals(InstitutionType.PARTICIPANT_INVESTOR.getCode()))
			sbQuery.append(" AND RO.STATE = 1144"); //1144 --confirmada
		else
			if(institutionType.equals(InstitutionType.DEPOSITARY.getCode()))
				if(Validations.validateIsNotNull(gfto.getState()))
					sbQuery.append(" AND RO.STATE = "+gfto.getState()); 
		sbQuery.append(" AND CO.ID_CUSTODY_OPERATION_PK = RO.ID_RETIREMENT_OPERATION_PK ");
		sbQuery.append(" AND TRUNC(CO.REGISTRY_DATE) between to_date('"+gfto.getInitialDate()+"','dd/MM/yyyy') and to_date('"+gfto.getFinalDate()+"','dd/MM/yyyy')");
		
		if(Validations.validateIsNotNull(gfto.getHolder())){
			sbQuery.append(" AND (SELECT COUNT(*) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK AND HAD.ID_HOLDER_FK="+ gfto.getHolder() + ")>0 ");
		}
		if(Validations.validateIsNotNull(gfto.getHolderAccount())){
			sbQuery.append(" AND HA.ID_HOLDER_ACCOUNT_PK="+gfto.getHolderAccount()+" ");
		}
		if(Validations.validateIsNotNull(gfto.getParticipant())){
			sbQuery.append(" AND HA.ID_PARTICIPANT_FK="+gfto.getParticipant()+" ");
		}
		if(Validations.validateIsNotNull(gfto.getSecurities())){
			sbQuery.append(" AND S.ID_SECURITY_CODE_PK='"+gfto.getSecurities().toString()+"' ");
		}
		sbQuery.append(" ORDER BY S.ID_SECURITY_CODE_PK");
		sbQuery.append("  ");
		return sbQuery.toString();
	}

	public String getRequestsForDetachmentOfCouponsReport(
			GenericCustodyOperationTO gfto, String requestNumber) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" SELECT ");
		sbQuery.append(" CO.ID_CUSTODY_OPERATION_PK OPERATION_NUMBER, ");
		sbQuery.append(" SCO.REGISTRY_DATE, ");
		sbQuery.append(" P.MNEMONIC, ");
		sbQuery.append(" HA.ACCOUNT_NUMBER, ");
		sbQuery.append(" HA.ID_HOLDER_ACCOUNT_PK, ");
		sbQuery.append(" SEC.ID_SECURITY_CODE_PK, ");
		sbQuery.append(" SCO.ID_SUBPRODUCT_CODE_FK as SubProduct, ");
		sbQuery.append(" SCM.OPERATION_QUANTITY, ");
		sbQuery.append(" SCO.REQUEST_STATE ");
		sbQuery.append(" FROM SPLIT_COUPON_MARKETFACT SCM ");
		sbQuery.append(" INNER JOIN SPLIT_COUPON_OPERATION SCO ON SCM.ID_SPLIT_OPERATION_FK=SCO.ID_SPLIT_OPERATION_PK ");
		sbQuery.append(" INNER JOIN CUSTODY_OPERATION CO ON SCO.ID_SPLIT_OPERATION_PK=CO.ID_CUSTODY_OPERATION_PK ");
		sbQuery.append(" INNER JOIN PARTICIPANT P ON SCO.ID_PARTICIPANT_FK=P.ID_PARTICIPANT_PK ");
		sbQuery.append(" INNER JOIN SECURITY SEC ON SEC.ID_SECURITY_CODE_PK=SCO.ID_SECURITY_CODE_FK ");
		sbQuery.append(" INNER JOIN HOLDER_ACCOUNT HA ON SCO.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append(" WHERE 1=1 ");
		sbQuery.append(" AND trunc(SCO.REGISTRY_DATE) between to_date('"
				+ gfto.getInitialDate() + "','dd/MM/yyyy') and to_date('"
				+ gfto.getFinalDate() + "','dd/MM/yyyy') ");

		if (Validations.validateIsNotNull(requestNumber)) {
			sbQuery.append(" AND CO.ID_CUSTODY_OPERATION_PK=" + requestNumber
					+ " ");
		}
		if (Validations.validateIsNotNull(gfto.getState())) {
			sbQuery.append(" AND SCO.REQUEST_STATE=" + gfto.getState() + " ");
		}
		if (Validations.validateIsNotNull(gfto.getParticipant())) {
			sbQuery.append(" AND P.ID_PARTICIPANT_PK=" + gfto.getParticipant()
					+ " ");
		}
		if (Validations.validateIsNotNull(gfto.getSecurities())) {
			sbQuery.append(" AND SEC.ID_SECURITY_CODE_PK='"
					+ gfto.getSecurities().toString() + "' ");
		}

		sbQuery.append(" ORDER BY CO.OPERATION_NUMBER,SCO.REGISTRY_DATE ");
		sbQuery.append("  ");

		return sbQuery.toString();
	}

	/* it only returns the query to send to the jasper as PARAMETER */
	public String getQueryParticipantUnificationInconsist(
			GenericsFiltersTO inconsistenciesTO) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" select");
		sbQuery.append(" ID_SECURITY_CODE_PK VALOR,");// 0
		sbQuery.append(" (SELECT S.DESCRIPTION FROM SECURITY S WHERE S.ID_SECURITY_CODE_PK=HAB.ID_SECURITY_CODE_PK) DESCRIPCION_VALOR,");// 1
		sbQuery.append(" (SELECT (P.ID_PARTICIPANT_PK||' '||P.DESCRIPTION)  FROM PARTICIPANT P WHERE P.ID_PARTICIPANT_PK=HAB.ID_PARTICIPANT_PK) PARTICIPANT,");// 2
		sbQuery.append(" (SELECT HA.ACCOUNT_NUMBER FROM HOLDER_ACCOUNT HA WHERE HAB.ID_HOLDER_ACCOUNT_PK = HA.ID_HOLDER_ACCOUNT_PK) CUENTA_TITULAR,");// 3
		sbQuery.append(" HAB.TOTAL_BALANCE,");// 4
		sbQuery.append(" HAB.AVAILABLE_BALANCE,");// 5
		sbQuery.append(" HAB.TRANSIT_BALANCE,");// 6
		sbQuery.append(" HAB.REPORTED_BALANCE,");// 7,8
		sbQuery.append(" HAB.REPORTING_BALANCE,");
		sbQuery.append(" HAB.PAWN_BALANCE,");// 7
		sbQuery.append(" HAB.BAN_BALANCE,");// 8
		sbQuery.append(" HAB.OTHER_BLOCK_BALANCE,");// 9
		sbQuery.append(" HAB.ACCREDITATION_BALANCE,");// 10
		sbQuery.append(" HAB.PURCHASE_BALANCE,");// 11
		sbQuery.append(" HAB.LENDER_BALANCE,");// 12
		sbQuery.append(" HAB.SALE_BALANCE,");// 13
		sbQuery.append(" FN_GET_HOLDER_NAME(HAB.ID_HOLDER_ACCOUNT_PK) DESCRIPTION_HOLDER");// 14
		sbQuery.append(" FROM HOLDER_ACCOUNT_BALANCE HAB");
		sbQuery.append(" WHERE HAB.ID_PARTICIPANT_PK=:idParticipantPk");
		sbQuery.append(" AND (HAB.TRANSIT_BALANCE>0 OR HAB.PURCHASE_BALANCE>0 OR HAB.REPORTED_BALANCE>0 OR HAB.REPORTING_BALANCE>0 OR HAB.SALE_BALANCE>0 OR");
		sbQuery.append(" 	 ((SELECT COUNT(*) FROM SETTLEMENT_OPERATION SAO, HOLDER_ACCOUNT_OPERATION HAO, MECHANISM_OPERATION MO ");
		sbQuery.append("	 WHERE HAO.ID_MECHANISM_OPERATION_FK=MO.ID_MECHANISM_OPERATION_PK AND   SAO.ID_MECHANISM_OPERATION_FK = HAO.ID_MECHANISM_OPERATION_FK");
		sbQuery.append(" 	 AND HAO.ROLE=:idRole");
		sbQuery.append(" 	 AND HAO.OPERATION_PART=:idOperation");
		sbQuery.append(" 	 AND SAO.STOCK_REFERENCE IS NULL");
		sbQuery.append(" 	 AND MO.OPERATION_STATE IN (612,614)");
		sbQuery.append(" 	 AND MO.ID_SECURITY_CODE_FK=HAB.ID_SECURITY_CODE_PK AND MO.ID_BUYER_PARTICIPANT_FK=HAB.ID_PARTICIPANT_PK)>0))");
		sbQuery.append(" 	 ORDER BY VALOR, PARTICIPANT, HAB.ID_HOLDER_ACCOUNT_PK");

		String strQueryFormatted = sbQuery.toString();

		strQueryFormatted = strQueryFormatted.replace(":idParticipantPk",
				inconsistenciesTO.getParticipant());
		strQueryFormatted = strQueryFormatted.replace(":idRole",
				ParticipantRoleType.SELL.getCode().toString());
		strQueryFormatted = strQueryFormatted.replace(":idOperation",
				BooleanType.YES.getCode().toString());

		return strQueryFormatted;
	}

	//DEMATERIALIZED
	public String getTotalOfPortfolioByTypeCustomerDemater(ClientPortafolioTO to) {
		
		StringBuilder sbQuery = new StringBuilder();
		
		//QUERY PARA EL VALOR NOMINAL
		StringBuilder sbSubQuery = new StringBuilder();
		sbSubQuery.append(" 		(DECODE(S.SECURITY_CLASS,426,ROUND( 																					");
//		if(CommonsUtilities.convertStringtoDate(to.getCutDate()).before(CommonsUtilities.currentDate())){
//			sbSubQuery.append("			(SELECT distinct NVH.NOMINAL_VALUE FROM NOMINAL_VALUE_HISTORY NVH WHERE TRUNC(NVH.CUT_DATE) = TRUNC(MFV.CUT_DATE) 			");
//			sbSubQuery.append("			AND S.ID_SECURITY_CODE_PK = NVH.ID_SECURITY_CODE_FK) 																");
//		}else{
			sbSubQuery.append("			S.current_nominal_value									 															");
//		}
		sbSubQuery.append("		,4),ROUND( 																													");
//		if(CommonsUtilities.convertStringtoDate(to.getCutDate()).before(CommonsUtilities.currentDate())){
//			sbSubQuery.append("			(SELECT distinct NVH.NOMINAL_VALUE FROM NOMINAL_VALUE_HISTORY NVH WHERE TRUNC(NVH.CUT_DATE) = TRUNC(MFV.CUT_DATE) 			");
//			sbSubQuery.append("			AND S.ID_SECURITY_CODE_PK = NVH.ID_SECURITY_CODE_FK) 																");
//		}else{
			sbSubQuery.append("			S.current_nominal_value									 															");
//		}
		sbSubQuery.append("		,2)) * MFV.TOTAL_BALANCE) AS VALOR_NOMINAL,																					");
		
		//QUERY PARA TIPO DE CAMBIO
		StringBuilder sbCurrencyQuery = new StringBuilder();
		sbCurrencyQuery.append(" 		CASE																												");
		sbCurrencyQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1 /																					");
		sbCurrencyQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D														");
		sbCurrencyQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getCutDate() +"','DD/MM/YYYY') 								");
		sbCurrencyQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)										");
		sbCurrencyQuery.append(" 			WHEN S.CURRENCY IN (1734,1304) THEN 1 *																			");
		sbCurrencyQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D														");
		sbCurrencyQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getCutDate() +"','DD/MM/YYYY') 								");
		sbCurrencyQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /								");
		sbCurrencyQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D														");
		sbCurrencyQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getCutDate() +"','DD/MM/YYYY') 								");
		sbCurrencyQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)										");
		sbCurrencyQuery.append(" 			WHEN S.CURRENCY IN (430,1853) THEN 1																			");
		sbCurrencyQuery.append(" 			ELSE 1																											");
		sbCurrencyQuery.append(" 	END TC_USD ");

		//MONTO TOTAL CARTERA PROPIA SAB
		sbQuery.append(" SELECT ");
		sbQuery.append("   '1' TIPO_REPORTE, ");
		sbQuery.append("   'Monto Total de Cartera Propia de Casas de Bolsa' DESC_REPORTE, ");
		sbQuery.append("   TA.INSTRUMENT_TYPE, ");
		sbQuery.append("   PT.PARAMETER_NAME, ");
		sbQuery.append("   SUM(TA.VALOR_NOMINAL * TC_USD) VALOR_NOMINAL, ");
		sbQuery.append("   SUM(TA.VALOR_MARKET * TC_USD)  VALOR_MARKET ");
		  
		sbQuery.append(" FROM PARAMETER_TABLE PT ");
		sbQuery.append("   LEFT JOIN ( ");
		sbQuery.append("     SELECT ");
		sbQuery.append("       S.INSTRUMENT_TYPE, ");
		sbQuery.append(sbSubQuery.toString());
		sbQuery.append("       MFV.TOTAL_BALANCE * ROUND(MFV.MARKET_PRICE,2) VALOR_MARKET, ");
		sbQuery.append(sbCurrencyQuery.toString());

		sbQuery.append("     FROM MARKET_FACT_VIEW MFV ");
		sbQuery.append("       INNER JOIN SECURITY S ON S.ID_SECURITY_CODE_PK = MFV.ID_SECURITY_CODE_PK ");

		sbQuery.append("     WHERE TRUNC(MFV.CUT_DATE) = to_date('"+ to.getCutDate() +"','dd/mm/yyyy') ");
		sbQuery.append("       AND (MFV.TOTAL_BALANCE > 0 or MFV.REPORTED_BALANCE > 0) ");
		sbQuery.append("       AND EXISTS ");
		sbQuery.append("         (SELECT 1 ");
		sbQuery.append("       	 FROM HOLDER_ACCOUNT_DETAIL HAD ");
        sbQuery.append("           INNER JOIN HOLDER H       ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
        sbQuery.append("           INNER JOIN PARTICIPANT P  ON P.ID_HOLDER_FK = H.ID_HOLDER_PK ");
        sbQuery.append("           							   AND H.ECONOMIC_ACTIVITY IN (43) ");
        sbQuery.append("         WHERE HAD.ID_HOLDER_ACCOUNT_FK = MFV.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append("         ) ");

		sbQuery.append("   ) TA ON TA.INSTRUMENT_TYPE = PT.PARAMETER_TABLE_PK ");

		sbQuery.append(" WHERE PT.MASTER_TABLE_FK = 50 ");
		sbQuery.append(" GROUP BY TA.INSTRUMENT_TYPE, PT.PARAMETER_NAME ");
		
		sbQuery.append(" UNION ALL ");

		//MONTO TOTAL CARTERA NO PROPIA SAB
		sbQuery.append(" SELECT ");
		sbQuery.append("   '2' TIPO_REPORTE, ");
		sbQuery.append("   'Monto Total de Cartera de Clientes de Casas de Bolsa' DESC_REPORTE, ");
		sbQuery.append("   TA.INSTRUMENT_TYPE, ");
		sbQuery.append("   PT.PARAMETER_NAME, ");
		sbQuery.append("   SUM(TA.VALOR_NOMINAL * TC_USD) VALOR_NOMINAL, ");
		sbQuery.append("   SUM(TA.VALOR_MARKET * TC_USD)  VALOR_MARKET ");
		sbQuery.append(" ");   
		sbQuery.append(" FROM PARAMETER_TABLE PT ");
		sbQuery.append("   LEFT JOIN ( ");
		sbQuery.append("     SELECT ");
		sbQuery.append("       S.INSTRUMENT_TYPE, ");
		sbQuery.append(sbSubQuery.toString());
		sbQuery.append("       MFV.TOTAL_BALANCE * ROUND(MFV.MARKET_PRICE,2) VALOR_MARKET, ");
		sbQuery.append(sbCurrencyQuery.toString());

		sbQuery.append("     FROM MARKET_FACT_VIEW MFV ");
		sbQuery.append("		INNER JOIN SECURITY S ON S.ID_SECURITY_CODE_PK = MFV.ID_SECURITY_CODE_PK ");
		sbQuery.append("		INNER JOIN PARTICIPANT PA ON PA.ID_PARTICIPANT_PK = MFV.ID_PARTICIPANT_PK ");
		sbQuery.append("     WHERE TRUNC(MFV.CUT_DATE) = to_date('"+ to.getCutDate() +"','dd/mm/yyyy') ");
		sbQuery.append("		AND PA.ACCOUNT_CLASS IN (28,2795) "); 
		sbQuery.append("       	AND (MFV.TOTAL_BALANCE > 0 or MFV.REPORTED_BALANCE > 0) ");
		sbQuery.append("       	AND EXISTS ");
		sbQuery.append("         (SELECT 1 ");
		sbQuery.append("       	 FROM HOLDER_ACCOUNT_DETAIL HAD ");
		sbQuery.append("       	 INNER JOIN HOLDER H  ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK  ");
        sbQuery.append("         WHERE HAD.ID_HOLDER_ACCOUNT_FK = MFV.ID_HOLDER_ACCOUNT_PK ");
        sbQuery.append("			AND PA.ID_HOLDER_FK <> HAD.ID_HOLDER_FK) ");//se excluye agencias de bolsa
        sbQuery.append("       AND NOT EXISTS ");
		sbQuery.append("         (SELECT 1 ");
		sbQuery.append("       	 FROM HOLDER_ACCOUNT_DETAIL HAD2 ");
        sbQuery.append("           INNER JOIN HOLDER H  ON H.ID_HOLDER_PK = HAD2.ID_HOLDER_FK ");
        sbQuery.append("           							AND H.ECONOMIC_ACTIVITY IN (1879,55,56,57,48,43) ");//se excluye a las safis y fondos de inversion y las companias de seguro
        sbQuery.append("         WHERE HAD2.ID_HOLDER_ACCOUNT_FK = MFV.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append("         ) ");

		sbQuery.append("   ) TA ON TA.INSTRUMENT_TYPE = PT.PARAMETER_TABLE_PK ");

		sbQuery.append(" WHERE PT.MASTER_TABLE_FK = 50 ");
		sbQuery.append(" GROUP BY TA.INSTRUMENT_TYPE, PT.PARAMETER_NAME ");

		sbQuery.append(" UNION ALL ");

		//MONTO TOTAL SAFI Y FONDOS DE INVERSION
		sbQuery.append(" SELECT ");
		sbQuery.append("   '3' TIPO_REPORTE, ");
		sbQuery.append("   'Monto Total de Cartera de SAFI s y Fondos de Inversion' DESC_REPORTE, ");
		sbQuery.append("   TA.INSTRUMENT_TYPE, ");
		sbQuery.append("   PT.PARAMETER_NAME, ");
		sbQuery.append("   SUM(TA.VALOR_NOMINAL * TC_USD) VALOR_NOMINAL, ");
		sbQuery.append("   SUM(TA.VALOR_MARKET * TC_USD)  VALOR_MARKET ");

		sbQuery.append(" FROM PARAMETER_TABLE PT ");
		sbQuery.append("   LEFT JOIN ( ");
		sbQuery.append("     SELECT ");
		sbQuery.append("       S.INSTRUMENT_TYPE, ");
		sbQuery.append(sbSubQuery.toString());
		sbQuery.append("       MFV.TOTAL_BALANCE * ROUND(MFV.MARKET_PRICE,2) VALOR_MARKET, ");
		sbQuery.append(sbCurrencyQuery.toString());

		sbQuery.append("     FROM MARKET_FACT_VIEW MFV ");
		sbQuery.append("       INNER JOIN SECURITY S ON S.ID_SECURITY_CODE_PK = MFV.ID_SECURITY_CODE_PK ");

		sbQuery.append("     WHERE TRUNC(MFV.CUT_DATE) = to_date('"+ to.getCutDate() +"','dd/mm/yyyy') ");
		sbQuery.append("       AND (MFV.TOTAL_BALANCE > 0 or MFV.REPORTED_BALANCE > 0) ");
		sbQuery.append("       AND EXISTS ");
		sbQuery.append("         (SELECT 1 ");
		sbQuery.append("       	 FROM HOLDER_ACCOUNT_DETAIL HAD ");
        sbQuery.append("           INNER JOIN HOLDER H  ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
        sbQuery.append("           							AND H.ECONOMIC_ACTIVITY IN (1879,55,56,57) ");
        sbQuery.append("         WHERE HAD.ID_HOLDER_ACCOUNT_FK = MFV.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append("         ) ");
		
		sbQuery.append("   ) TA ON TA.INSTRUMENT_TYPE = PT.PARAMETER_TABLE_PK ");

		sbQuery.append(" WHERE PT.MASTER_TABLE_FK = 50 ");
		sbQuery.append(" GROUP BY TA.INSTRUMENT_TYPE, PT.PARAMETER_NAME ");

		sbQuery.append(" UNION ALL ");

		//MONTO TOTAL CARTERA COMPANIAS DE SEGUROS
		sbQuery.append(" SELECT ");
		sbQuery.append("   '4' TIPO_REPORTE, ");
		sbQuery.append("   'Monto Total de Cartera de Companias de Seguro' DESC_REPORTE, ");
		sbQuery.append("   TA.INSTRUMENT_TYPE, ");
		sbQuery.append("   PT.PARAMETER_NAME, ");
		sbQuery.append("   SUM(TA.VALOR_NOMINAL * TC_USD) VALOR_NOMINAL, ");
		sbQuery.append("   SUM(TA.VALOR_MARKET * TC_USD)  VALOR_MARKET ");

		sbQuery.append(" FROM PARAMETER_TABLE PT ");
		sbQuery.append("   LEFT JOIN ( ");
		sbQuery.append("     SELECT ");
		sbQuery.append("       S.INSTRUMENT_TYPE, ");
		sbQuery.append(sbSubQuery.toString());
		sbQuery.append("       MFV.TOTAL_BALANCE * ROUND(MFV.MARKET_PRICE,2) VALOR_MARKET, ");
		sbQuery.append(sbCurrencyQuery.toString());

		sbQuery.append("     FROM MARKET_FACT_VIEW MFV ");
		sbQuery.append("       INNER JOIN SECURITY S ON S.ID_SECURITY_CODE_PK = MFV.ID_SECURITY_CODE_PK ");

		sbQuery.append("     WHERE TRUNC(MFV.CUT_DATE) = to_date('"+ to.getCutDate() +"','dd/mm/yyyy') ");
		sbQuery.append("       AND (MFV.TOTAL_BALANCE > 0 or MFV.REPORTED_BALANCE > 0) ");
		sbQuery.append("       AND EXISTS ");
		sbQuery.append("         (SELECT 1 ");
		sbQuery.append("       	 FROM HOLDER_ACCOUNT_DETAIL HAD ");
        sbQuery.append("           INNER JOIN HOLDER H  ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
        sbQuery.append("           							AND H.ECONOMIC_ACTIVITY IN (48) ");
        sbQuery.append("         WHERE HAD.ID_HOLDER_ACCOUNT_FK = MFV.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append("         ) ");

		sbQuery.append("   ) TA ON TA.INSTRUMENT_TYPE = PT.PARAMETER_TABLE_PK ");

		sbQuery.append(" WHERE PT.MASTER_TABLE_FK = 50 ");
		sbQuery.append(" GROUP BY TA.INSTRUMENT_TYPE, PT.PARAMETER_NAME ");

		sbQuery.append(" UNION ALL ");

		//MONTO TOTAL CARTERA AFP-FCC
		sbQuery.append(" SELECT ");
		sbQuery.append("   '5' TIPO_REPORTE, ");
		sbQuery.append("   'Monto Total de Cartera de AFP - FCC' DESC_REPORTE, ");
		sbQuery.append("   TA.INSTRUMENT_TYPE, ");
		sbQuery.append("   PT.PARAMETER_NAME, ");
		sbQuery.append("   SUM(TA.VALOR_NOMINAL * TC_USD) VALOR_NOMINAL, ");
		sbQuery.append("   SUM(TA.VALOR_MARKET * TC_USD)  VALOR_MARKET ");

		sbQuery.append(" FROM PARAMETER_TABLE PT ");
		sbQuery.append("   LEFT JOIN ( ");
		sbQuery.append("     SELECT ");
		sbQuery.append("       S.INSTRUMENT_TYPE, ");
		sbQuery.append(sbSubQuery.toString());
		sbQuery.append("       MFV.TOTAL_BALANCE * ROUND(MFV.MARKET_PRICE,2) VALOR_MARKET, ");
		sbQuery.append(sbCurrencyQuery.toString());

		sbQuery.append("     FROM MARKET_FACT_VIEW MFV ");
		sbQuery.append("       INNER JOIN SECURITY S ON S.ID_SECURITY_CODE_PK = MFV.ID_SECURITY_CODE_PK ");

		sbQuery.append("     WHERE TRUNC(MFV.CUT_DATE) = to_date('"+ to.getCutDate() +"','dd/mm/yyyy') ");
		sbQuery.append("       AND (MFV.TOTAL_BALANCE > 0 or MFV.REPORTED_BALANCE > 0) ");
		sbQuery.append("       AND EXISTS ");
		sbQuery.append("         (SELECT 1 ");
		sbQuery.append("       	 FROM HOLDER_ACCOUNT_DETAIL HAD ");
        sbQuery.append("           INNER JOIN HOLDER H  ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
        sbQuery.append("                              		AND H.ECONOMIC_ACTIVITY IN (42) ");
		sbQuery.append("                                    AND H.FUND_TYPE='FCC' ");
        sbQuery.append("         WHERE HAD.ID_HOLDER_ACCOUNT_FK = MFV.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append("         ) ");

		sbQuery.append("   ) TA ON TA.INSTRUMENT_TYPE = PT.PARAMETER_TABLE_PK ");

		sbQuery.append(" WHERE PT.MASTER_TABLE_FK = 50 ");
		sbQuery.append(" GROUP BY TA.INSTRUMENT_TYPE, PT.PARAMETER_NAME ");

		sbQuery.append(" UNION ALL ");

		//MONTO TOTAL CARTERA AFP-FCI
		sbQuery.append(" SELECT ");
		sbQuery.append("   '6' TIPO_REPORTE, ");
		sbQuery.append("   'Monto Total de Cartera de AFP - FCI' DESC_REPORTE, ");
		sbQuery.append("   TA.INSTRUMENT_TYPE, ");
		sbQuery.append("   PT.PARAMETER_NAME, ");
		sbQuery.append("   SUM(TA.VALOR_NOMINAL * TC_USD) VALOR_NOMINAL, ");
		sbQuery.append("   SUM(TA.VALOR_MARKET * TC_USD)  VALOR_MARKET ");

		sbQuery.append(" FROM PARAMETER_TABLE PT ");
		sbQuery.append("   LEFT JOIN ( ");
		sbQuery.append("     SELECT ");
		sbQuery.append("       S.INSTRUMENT_TYPE, ");
		sbQuery.append(sbSubQuery.toString());
		sbQuery.append("       MFV.TOTAL_BALANCE * ROUND(MFV.MARKET_PRICE,2) VALOR_MARKET, ");
		sbQuery.append(sbCurrencyQuery.toString());

		sbQuery.append("     FROM MARKET_FACT_VIEW MFV ");
		sbQuery.append("       INNER JOIN SECURITY S ON S.ID_SECURITY_CODE_PK = MFV.ID_SECURITY_CODE_PK ");

		sbQuery.append("     WHERE TRUNC(MFV.CUT_DATE) = to_date('"+ to.getCutDate() +"','dd/mm/yyyy') ");
		sbQuery.append("       AND (MFV.TOTAL_BALANCE > 0 or MFV.REPORTED_BALANCE > 0) ");
		sbQuery.append("       AND EXISTS ");
		sbQuery.append("         (SELECT 1 ");
		sbQuery.append("       	 FROM HOLDER_ACCOUNT_DETAIL HAD ");
        sbQuery.append("           INNER JOIN HOLDER H  ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
        sbQuery.append("                              		AND H.ECONOMIC_ACTIVITY IN (42) ");
		sbQuery.append("                                    AND (H.FUND_TYPE='FCI') ");
        sbQuery.append("         WHERE HAD.ID_HOLDER_ACCOUNT_FK = MFV.ID_HOLDER_ACCOUNT_PK ");
        
        sbQuery.append("         UNION ALL ");
        sbQuery.append("         SELECT 1 ");
		sbQuery.append("       	 FROM HOLDER_ACCOUNT_DETAIL HAD ");
        sbQuery.append("           INNER JOIN HOLDER H  ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK AND H.ID_HOLDER_PK = 455 ");
        sbQuery.append("         WHERE HAD.ID_HOLDER_ACCOUNT_FK = MFV.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append("          ");
        
		sbQuery.append("         ) ");

		sbQuery.append("   ) TA ON TA.INSTRUMENT_TYPE = PT.PARAMETER_TABLE_PK ");

		sbQuery.append(" WHERE PT.MASTER_TABLE_FK = 50 ");
		sbQuery.append(" GROUP BY TA.INSTRUMENT_TYPE, PT.PARAMETER_NAME ");

		sbQuery.append(" UNION ALL ");

		//MONTO TOTAL DE CARTERA DE ENTIDADES FINANCIERAS (BANCO, MUTUAL, COOPERATIVA, FONDO FINANCIERO)
		sbQuery.append(" SELECT ");
		sbQuery.append("   '7' TIPO_REPORTE, ");
		sbQuery.append("   'Monto Total de Cartera de Entidades Financieras (Banco,Mutual,Cooperativa,Fondo Financiero)' DESC_REPORTE, ");
		sbQuery.append("   TA.INSTRUMENT_TYPE, ");
		sbQuery.append("   PT.PARAMETER_NAME, ");
		sbQuery.append("   SUM(TA.VALOR_NOMINAL * TC_USD) VALOR_NOMINAL, ");
		sbQuery.append("   SUM(TA.VALOR_MARKET * TC_USD)  VALOR_MARKET ");

		sbQuery.append(" FROM PARAMETER_TABLE PT ");
		sbQuery.append("   LEFT JOIN ( ");
		sbQuery.append("     SELECT ");
		sbQuery.append("       S.INSTRUMENT_TYPE, ");
		sbQuery.append(sbSubQuery.toString());
		sbQuery.append("       MFV.TOTAL_BALANCE * ROUND(MFV.MARKET_PRICE,2) VALOR_MARKET, ");
		sbQuery.append(sbCurrencyQuery.toString());

		sbQuery.append("     FROM MARKET_FACT_VIEW MFV ");
		sbQuery.append("       	INNER JOIN SECURITY S ON S.ID_SECURITY_CODE_PK = MFV.ID_SECURITY_CODE_PK ");
		sbQuery.append("		INNER JOIN PARTICIPANT PA ON PA.ID_PARTICIPANT_PK = MFV.ID_PARTICIPANT_PK ");
		sbQuery.append("     WHERE TRUNC(MFV.CUT_DATE) = to_date('"+ to.getCutDate() +"','dd/mm/yyyy') ");
		sbQuery.append("		AND PA.ACCOUNT_CLASS IN (32,33,34,35,30,1961) ");
		sbQuery.append("       	AND (MFV.TOTAL_BALANCE > 0 or MFV.REPORTED_BALANCE > 0) ");
		sbQuery.append("       AND NOT EXISTS ");
		sbQuery.append("         (SELECT 1 ");
		sbQuery.append("       	 FROM HOLDER_ACCOUNT_DETAIL HAD ");
        sbQuery.append("           INNER JOIN HOLDER H  ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
        sbQuery.append("           							AND H.ECONOMIC_ACTIVITY IN (1879,55,56,57,48) ");//se excluye a las safis y fondos de inversion y las companias de seguro
        sbQuery.append("         WHERE HAD.ID_HOLDER_ACCOUNT_FK = MFV.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append("         ) ");
	
		sbQuery.append("       AND NOT EXISTS ");
		sbQuery.append("         (SELECT 1 ");
		sbQuery.append("       	 FROM HOLDER_ACCOUNT_DETAIL HAD2 ");
	    sbQuery.append("           INNER JOIN HOLDER H  ON H.ID_HOLDER_PK = HAD2.ID_HOLDER_FK ");
	    sbQuery.append("           AND H.ID_HOLDER_PK IN (451,452,453,454,455,456) ");//se excluye a las safis y fondos de inversion y las companias de seguro
	    sbQuery.append("         WHERE HAD2.ID_HOLDER_ACCOUNT_FK = MFV.ID_HOLDER_ACCOUNT_PK ");
	    sbQuery.append("         ) ");
		
		sbQuery.append("   ) TA ON TA.INSTRUMENT_TYPE = PT.PARAMETER_TABLE_PK ");

		sbQuery.append(" WHERE PT.MASTER_TABLE_FK = 50 ");
		sbQuery.append(" GROUP BY TA.INSTRUMENT_TYPE, PT.PARAMETER_NAME ");
		
		sbQuery.append(" UNION ALL ");

		//MONTO TOTAL CARTERA EMPRESAS EMISORAS ISSUE 720
		sbQuery.append(" SELECT ");
		sbQuery.append("   '8' TIPO_REPORTE, ");
		sbQuery.append("   'Monto Total de Cartera de Empresas Emisoras' DESC_REPORTE, ");
		sbQuery.append("   TA.INSTRUMENT_TYPE, ");
		sbQuery.append("   PT.PARAMETER_NAME, ");
		sbQuery.append("   SUM(TA.VALOR_NOMINAL * TC_USD) VALOR_NOMINAL, ");
		sbQuery.append("   SUM(TA.VALOR_MARKET * TC_USD)  VALOR_MARKET ");
		sbQuery.append(" ");   
		sbQuery.append(" FROM PARAMETER_TABLE PT ");
		sbQuery.append("   LEFT JOIN ( ");
		sbQuery.append("     SELECT ");
		sbQuery.append("       S.INSTRUMENT_TYPE, ");
		sbQuery.append(sbSubQuery.toString());
		sbQuery.append("       MFV.TOTAL_BALANCE * ROUND(MFV.MARKET_PRICE,2) VALOR_MARKET, ");
		sbQuery.append(sbCurrencyQuery.toString());

		sbQuery.append("     FROM MARKET_FACT_VIEW MFV ");
		sbQuery.append("		INNER JOIN SECURITY S ON S.ID_SECURITY_CODE_PK = MFV.ID_SECURITY_CODE_PK ");
		sbQuery.append("		INNER JOIN PARTICIPANT PA ON PA.ID_PARTICIPANT_PK = MFV.ID_PARTICIPANT_PK ");
		sbQuery.append("     WHERE TRUNC(MFV.CUT_DATE) = to_date('"+ to.getCutDate() +"','dd/mm/yyyy') ");
		sbQuery.append("		AND PA.ACCOUNT_CLASS in(2360,31,263,2359)  "); 
		sbQuery.append("       	AND (MFV.TOTAL_BALANCE > 0 or MFV.REPORTED_BALANCE > 0) ");
		
		sbQuery.append("       AND NOT EXISTS ");
		sbQuery.append("         (SELECT 1 ");
		sbQuery.append("       	 FROM HOLDER_ACCOUNT_DETAIL HAD ");
        sbQuery.append("           INNER JOIN HOLDER H       ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
        sbQuery.append("           INNER JOIN PARTICIPANT P  ON P.ID_HOLDER_FK = H.ID_HOLDER_PK ");
        sbQuery.append("           							   AND H.ECONOMIC_ACTIVITY IN (43) ");
        sbQuery.append("         WHERE HAD.ID_HOLDER_ACCOUNT_FK = MFV.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append("         ) ");
		
        sbQuery.append("       AND NOT EXISTS ");
		sbQuery.append("         (SELECT 1 ");
		sbQuery.append("       	 FROM HOLDER_ACCOUNT_DETAIL HAD2 ");
        sbQuery.append("           INNER JOIN HOLDER H  ON H.ID_HOLDER_PK = HAD2.ID_HOLDER_FK ");
        sbQuery.append("           							AND H.ID_HOLDER_PK NOT IN (451,452,453,454,455,456) ");//se excluye a las safis y fondos de inversion y las companias de seguro
        sbQuery.append("           							AND H.ECONOMIC_ACTIVITY IN (1879,55,56,57,48,43) ");
        sbQuery.append("         WHERE HAD2.ID_HOLDER_ACCOUNT_FK = MFV.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append("         ) ");

		sbQuery.append("   ) TA ON TA.INSTRUMENT_TYPE = PT.PARAMETER_TABLE_PK ");

		sbQuery.append(" WHERE PT.MASTER_TABLE_FK = 50 ");
		sbQuery.append(" GROUP BY TA.INSTRUMENT_TYPE, PT.PARAMETER_NAME ");

		sbQuery.append(" ORDER BY 1,3 ");
		
		return sbQuery.toString();
	}
	
	//PHYSICAL
	/**
	 * Metodo Query para report_pk 188
	 * mod ISSUE 910
	 */
	public String getTotalOfPortfolioByTypeCustomerPhysical(ClientPortafolioTO to) {
			
		StringBuilder sbQuery = new StringBuilder();

		//QUERY PARA EL VALOR NOMINAL
		StringBuilder sbSubQuery = new StringBuilder();
		sbSubQuery.append(" 		(DECODE(S.SECURITY_CLASS,426,ROUND( 																					");
//		if(CommonsUtilities.convertStringtoDate(to.getCutDate()).before(CommonsUtilities.currentDate())){
//			sbSubQuery.append("			(SELECT distinct NVH.NOMINAL_VALUE FROM NOMINAL_VALUE_HISTORY NVH WHERE (NVH.CUT_DATE) = (PSV.CUT_DATE) 			");
//			sbSubQuery.append("			AND S.ID_SECURITY_CODE_PK = NVH.ID_SECURITY_CODE_FK) 																");
//		}else{
			sbSubQuery.append("			S.current_nominal_value									 															");
//		}
		sbSubQuery.append("		,4),ROUND( 																													");
//		if(CommonsUtilities.convertStringtoDate(to.getCutDate()).before(CommonsUtilities.currentDate())){
//			sbSubQuery.append("			(SELECT distinct NVH.NOMINAL_VALUE FROM NOMINAL_VALUE_HISTORY NVH WHERE (NVH.CUT_DATE) = (PSV.CUT_DATE) 			");
//			sbSubQuery.append("			AND S.ID_SECURITY_CODE_PK = NVH.ID_SECURITY_CODE_FK) 																");
//		}else{
			sbSubQuery.append("			S.current_nominal_value									 															");
//		}
		sbSubQuery.append("		,2)) * PSV.TOTAL_BALANCE) AS VALOR_NOMINAL,																			");
		
		//QUERY PARA TIPO DE CAMBIO
		StringBuilder sbCurrencyQuery = new StringBuilder();
		sbCurrencyQuery.append(" 		CASE																												");
		sbCurrencyQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1 /																					");
		sbCurrencyQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D														");
		sbCurrencyQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getCutDate() +"','DD/MM/YYYY') 								");
		sbCurrencyQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)										");
		sbCurrencyQuery.append(" 			WHEN S.CURRENCY IN (1734,1304) THEN 1 *																			");
		sbCurrencyQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D														");
		sbCurrencyQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getCutDate() +"','DD/MM/YYYY') 								");
		sbCurrencyQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /								");
		sbCurrencyQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D														");
		sbCurrencyQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+ to.getCutDate() +"','DD/MM/YYYY') 								");
		sbCurrencyQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)										");
		sbCurrencyQuery.append(" 			WHEN S.CURRENCY IN (430,1853) THEN 1																			");
		sbCurrencyQuery.append(" 			ELSE 1																											");
		sbCurrencyQuery.append(" 	END TC_USD ");
		
		//MONTO TOTAL CARTERA PROPIA SAB
		sbQuery.append(" SELECT ");
		sbQuery.append("   '1' TIPO_REPORTE, ");
		sbQuery.append("   'Monto Total de Cartera Propia de Casas de Bolsa' DESC_REPORTE, ");
		sbQuery.append("   TA.INSTRUMENT_TYPE, ");
		sbQuery.append("   PT.PARAMETER_NAME, ");
		sbQuery.append("   SUM(TA.VALOR_NOMINAL * TC_USD) VALOR_NOMINAL, ");
		sbQuery.append("   SUM(TA.VALOR_MARKET * TC_USD)  VALOR_MARKET ");
		  
		sbQuery.append(" FROM PARAMETER_TABLE PT ");
		sbQuery.append("   LEFT JOIN ( ");
		sbQuery.append("     SELECT ");
		sbQuery.append("       S.INSTRUMENT_TYPE, ");
		
		sbQuery.append(sbSubQuery.toString());
		sbQuery.append("      1 * PSV.TOTAL_BALANCE VALOR_MARKET, ");
		sbQuery.append(sbCurrencyQuery.toString());
		sbQuery.append("     from BALANCE_PSY_VIEW PSV ");
//		sbQuery.append("  	INNER JOIN PHYSICAL_CERT_MARKETFACT HMB ON HMB.ID_PHYSICAL_CERTIFICATE_FK = PSV.ID_PHYSICAL_CERTIFICATE_PK       ");
		sbQuery.append("  	inner join holder_Account ha   		 on PSV.id_holder_Account_pk=HA.id_holder_Account_pk                         ");
		sbQuery.append("  	Inner join participant pa 				 On PSV.id_participant_pk=pa.id_participant_pk                           ");
		sbQuery.append("  	Inner join security S 				 On PSV.id_security_code_pk = S.id_security_code_pk                          ");
		sbQuery.append("     WHERE 1 = 1 ");
		sbQuery.append("     AND trunc(PSV.CUT_DATE ) = TO_DATE('"+ to.getCutDate() +"','DD/MM/YYYY')  ");
		sbQuery.append("       AND EXISTS ");
		sbQuery.append("         (SELECT 1 ");
		sbQuery.append("       	 FROM HOLDER_ACCOUNT_DETAIL HAD ");
        sbQuery.append("           INNER JOIN HOLDER H       ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
        sbQuery.append("           INNER JOIN PARTICIPANT P  ON P.ID_HOLDER_FK = H.ID_HOLDER_PK ");
        sbQuery.append("           							   AND H.ECONOMIC_ACTIVITY IN (43) ");
        sbQuery.append("         WHERE HAD.ID_HOLDER_ACCOUNT_FK = PSV.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append("         ) ");
		
		sbQuery.append("   ) TA ON TA.INSTRUMENT_TYPE = PT.PARAMETER_TABLE_PK ");

		sbQuery.append(" WHERE PT.MASTER_TABLE_FK = 50 ");
		sbQuery.append(" GROUP BY TA.INSTRUMENT_TYPE, PT.PARAMETER_NAME ");
		
		sbQuery.append(" UNION ALL ");

		//MONTO TOTAL CARTERA NO PROPIA SAB
		sbQuery.append(" SELECT ");
		sbQuery.append("   '2' TIPO_REPORTE, ");
		sbQuery.append("   'Monto Total de Cartera de Clientes de Casas de Bolsa' DESC_REPORTE, ");
		sbQuery.append("   TA.INSTRUMENT_TYPE, ");
		sbQuery.append("   PT.PARAMETER_NAME, ");
		sbQuery.append("   SUM(TA.VALOR_NOMINAL * TC_USD) VALOR_NOMINAL, ");
		sbQuery.append("   SUM(TA.VALOR_MARKET * TC_USD)  VALOR_MARKET ");
		sbQuery.append(" ");   
		sbQuery.append(" FROM PARAMETER_TABLE PT ");
		sbQuery.append("   LEFT JOIN ( ");
		sbQuery.append("     SELECT ");
		sbQuery.append("       S.INSTRUMENT_TYPE, ");
		sbQuery.append(sbSubQuery.toString());
		sbQuery.append("      1 * PSV.TOTAL_BALANCE VALOR_MARKET, ");
		sbQuery.append(sbCurrencyQuery.toString());
		sbQuery.append("     from BALANCE_PSY_VIEW PSV ");
//		sbQuery.append("  	INNER JOIN PHYSICAL_CERT_MARKETFACT HMB ON HMB.ID_PHYSICAL_CERTIFICATE_FK = PSV.ID_PHYSICAL_CERTIFICATE_PK       ");
		sbQuery.append("  	inner join holder_Account ha   		 on PSV.id_holder_Account_pk=HA.id_holder_Account_pk                         ");
		sbQuery.append("  	Inner join participant pa 				 On PSV.id_participant_pk=pa.id_participant_pk                           ");
		sbQuery.append("  	Inner join security S 				 On PSV.id_security_code_pk = S.id_security_code_pk                          ");
		sbQuery.append("     WHERE 1 = 1 ");
		sbQuery.append("     AND trunc(PSV.CUT_DATE ) = TO_DATE('"+ to.getCutDate() +"','DD/MM/YYYY')  ");
		sbQuery.append("		AND PA.ACCOUNT_CLASS IN (28,2795) "); //AGENCIAS DE BOLSA y CASAS DE BOLSA
		sbQuery.append("       	AND EXISTS ");
		sbQuery.append("         (SELECT 1 ");
		sbQuery.append("       	 FROM HOLDER_ACCOUNT_DETAIL HAD ");
        sbQuery.append("         WHERE HAD.ID_HOLDER_ACCOUNT_FK = PSV.ID_HOLDER_ACCOUNT_PK ");
        sbQuery.append("			AND PA.ID_HOLDER_FK <> HAD.ID_HOLDER_FK ) ");
        sbQuery.append("     AND NOT EXISTS                                                    ");
        sbQuery.append("       (SELECT 1                                                       ");
        sbQuery.append("       FROM HOLDER_ACCOUNT_DETAIL HAD                                  ");
        sbQuery.append("       INNER JOIN HOLDER H  ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK       ");      		
        sbQuery.append("       AND H.ECONOMIC_ACTIVITY IN (48)                                 ");
        sbQuery.append("       WHERE HAD.ID_HOLDER_ACCOUNT_FK = PSV.ID_HOLDER_ACCOUNT_PK       ");
        sbQuery.append("       )                                                               ");
        
        
		sbQuery.append("   ) TA ON TA.INSTRUMENT_TYPE = PT.PARAMETER_TABLE_PK ");

		sbQuery.append(" WHERE PT.MASTER_TABLE_FK = 50 ");
		sbQuery.append(" GROUP BY TA.INSTRUMENT_TYPE, PT.PARAMETER_NAME ");

		sbQuery.append(" UNION ALL ");

		//MONTO TOTAL SAFI Y FONDOS DE INVERSION
		sbQuery.append(" SELECT ");
		sbQuery.append("   '3' TIPO_REPORTE, ");
		sbQuery.append("   'Monto Total de Cartera de SAFIs y Fondos de Inversion' DESC_REPORTE, ");
		sbQuery.append("   TA.INSTRUMENT_TYPE, ");
		sbQuery.append("   PT.PARAMETER_NAME, ");
		sbQuery.append("   SUM(TA.VALOR_NOMINAL * TC_USD) VALOR_NOMINAL, ");
		sbQuery.append("   SUM(TA.VALOR_MARKET * TC_USD)  VALOR_MARKET ");

		sbQuery.append(" FROM PARAMETER_TABLE PT ");
		sbQuery.append("   LEFT JOIN ( ");
		sbQuery.append("     SELECT ");
		sbQuery.append("       S.INSTRUMENT_TYPE, ");
		sbQuery.append(sbSubQuery.toString());
		sbQuery.append("     1 * PSV.TOTAL_BALANCE VALOR_MARKET, ");
		sbQuery.append(sbCurrencyQuery.toString());
		sbQuery.append("     from BALANCE_PSY_VIEW PSV ");
//		sbQuery.append("  	INNER JOIN PHYSICAL_CERT_MARKETFACT HMB ON HMB.ID_PHYSICAL_CERTIFICATE_FK = PSV.ID_PHYSICAL_CERTIFICATE_PK       ");
		sbQuery.append("  	inner join holder_Account ha   		 on PSV.id_holder_Account_pk=HA.id_holder_Account_pk                         ");
		sbQuery.append("  	Inner join participant pa 				 On PSV.id_participant_pk=pa.id_participant_pk                           ");
		sbQuery.append("  	Inner join security S 				 On PSV.id_security_code_pk = S.id_security_code_pk                          ");
		sbQuery.append("     WHERE 1 = 1 ");
		sbQuery.append("     AND PA.ACCOUNT_CLASS NOT IN (28,2795)  ");
		sbQuery.append("     AND trunc(PSV.CUT_DATE ) = TO_DATE('"+ to.getCutDate() +"','DD/MM/YYYY')  ");
		sbQuery.append("       AND EXISTS ");
		sbQuery.append("         (SELECT 1 ");
		sbQuery.append("       	 FROM HOLDER_ACCOUNT_DETAIL HAD ");
        sbQuery.append("           INNER JOIN HOLDER H  ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
        sbQuery.append("           							AND H.ECONOMIC_ACTIVITY IN (1879,55,56,57) ");
        sbQuery.append("         WHERE HAD.ID_HOLDER_ACCOUNT_FK = PSV.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append("         ) ");

		sbQuery.append("   ) TA ON TA.INSTRUMENT_TYPE = PT.PARAMETER_TABLE_PK ");

		sbQuery.append(" WHERE PT.MASTER_TABLE_FK = 50 ");
		sbQuery.append(" GROUP BY TA.INSTRUMENT_TYPE, PT.PARAMETER_NAME ");

		sbQuery.append(" UNION ALL ");

		//MONTO TOTAL CARTERA COMPANIAS DE SEGUROS
		sbQuery.append(" SELECT ");
		sbQuery.append("   '4' TIPO_REPORTE, ");
		sbQuery.append("   'Monto Total de Cartera de Companias de Seguro' DESC_REPORTE, ");
		sbQuery.append("   TA.INSTRUMENT_TYPE, ");
		sbQuery.append("   PT.PARAMETER_NAME, ");
		sbQuery.append("   SUM(TA.VALOR_NOMINAL * TC_USD) VALOR_NOMINAL, ");
		sbQuery.append("   SUM(TA.VALOR_MARKET * TC_USD)  VALOR_MARKET ");

		sbQuery.append(" FROM PARAMETER_TABLE PT ");
		sbQuery.append("   LEFT JOIN ( ");
		sbQuery.append("     SELECT ");
		sbQuery.append("       S.INSTRUMENT_TYPE, ");
		sbQuery.append(sbSubQuery.toString());
		sbQuery.append("     1 * PSV.TOTAL_BALANCE VALOR_MARKET, ");
		sbQuery.append(sbCurrencyQuery.toString());
		sbQuery.append("     from BALANCE_PSY_VIEW PSV ");
//		sbQuery.append("  	INNER JOIN PHYSICAL_CERT_MARKETFACT HMB ON HMB.ID_PHYSICAL_CERTIFICATE_FK = PSV.ID_PHYSICAL_CERTIFICATE_PK       ");
		sbQuery.append("  	inner join holder_Account ha   		 on PSV.id_holder_Account_pk=HA.id_holder_Account_pk                         ");
		sbQuery.append("  	Inner join participant pa 				 On PSV.id_participant_pk=pa.id_participant_pk                           ");
		sbQuery.append("  	Inner join security S 				 On PSV.id_security_code_pk = S.id_security_code_pk                          ");
		sbQuery.append("     WHERE 1 = 1 ");
		sbQuery.append("     AND trunc(PSV.CUT_DATE ) = TO_DATE('"+ to.getCutDate() +"','DD/MM/YYYY')  ");
		sbQuery.append("       AND EXISTS ");
		sbQuery.append("         (SELECT 1 ");
		sbQuery.append("       	 FROM HOLDER_ACCOUNT_DETAIL HAD ");
        sbQuery.append("           INNER JOIN HOLDER H  ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
        sbQuery.append("           							AND H.ECONOMIC_ACTIVITY IN (48) ");
        sbQuery.append("         WHERE HAD.ID_HOLDER_ACCOUNT_FK = PSV.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append("         ) ");

		sbQuery.append("   ) TA ON TA.INSTRUMENT_TYPE = PT.PARAMETER_TABLE_PK ");

		sbQuery.append(" WHERE PT.MASTER_TABLE_FK = 50 ");
		sbQuery.append(" GROUP BY TA.INSTRUMENT_TYPE, PT.PARAMETER_NAME ");

		sbQuery.append(" UNION ALL ");

		//MONTO TOTAL CARTERA AFP-FCC
		sbQuery.append(" SELECT ");
		sbQuery.append("   '5' TIPO_REPORTE, ");
		sbQuery.append("   'Monto Total de Cartera de AFP - FCC' DESC_REPORTE, ");
		sbQuery.append("   TA.INSTRUMENT_TYPE, ");
		sbQuery.append("   PT.PARAMETER_NAME, ");
		sbQuery.append("   SUM(TA.VALOR_NOMINAL * TC_USD) VALOR_NOMINAL, ");
		sbQuery.append("   SUM(TA.VALOR_MARKET * TC_USD)  VALOR_MARKET ");

		sbQuery.append(" FROM PARAMETER_TABLE PT ");
		sbQuery.append("   LEFT JOIN ( ");
		sbQuery.append("     SELECT ");
		sbQuery.append("       S.INSTRUMENT_TYPE, ");
		sbQuery.append(sbSubQuery.toString());
		sbQuery.append("     1* PSV.TOTAL_BALANCE VALOR_MARKET, ");
		sbQuery.append(sbCurrencyQuery.toString());
		sbQuery.append("     from BALANCE_PSY_VIEW PSV ");
//		sbQuery.append("  	INNER JOIN PHYSICAL_CERT_MARKETFACT HMB ON HMB.ID_PHYSICAL_CERTIFICATE_FK = PSV.ID_PHYSICAL_CERTIFICATE_PK       ");
		sbQuery.append("  	inner join holder_Account ha   		 on PSV.id_holder_Account_pk=HA.id_holder_Account_pk                         ");
		sbQuery.append("  	Inner join participant pa 				 On PSV.id_participant_pk=pa.id_participant_pk                           ");
		sbQuery.append("  	Inner join security S 				 On PSV.id_security_code_pk = S.id_security_code_pk                          ");
		sbQuery.append("     WHERE 1 = 1 ");
		sbQuery.append("     AND trunc(PSV.CUT_DATE ) = TO_DATE('"+ to.getCutDate() +"','DD/MM/YYYY')  ");
		sbQuery.append("       AND EXISTS ");
		sbQuery.append("         (SELECT 1 ");
		sbQuery.append("       	 FROM HOLDER_ACCOUNT_DETAIL HAD ");
        sbQuery.append("           INNER JOIN HOLDER H  ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
        sbQuery.append("                              		AND H.ECONOMIC_ACTIVITY IN (42) ");
		sbQuery.append("                                    AND H.FUND_TYPE='FCC' ");
        sbQuery.append("         WHERE HAD.ID_HOLDER_ACCOUNT_FK = PSV.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append("         ) ");

		sbQuery.append("   ) TA ON TA.INSTRUMENT_TYPE = PT.PARAMETER_TABLE_PK ");

		sbQuery.append(" WHERE PT.MASTER_TABLE_FK = 50 ");
		sbQuery.append(" GROUP BY TA.INSTRUMENT_TYPE, PT.PARAMETER_NAME ");

		sbQuery.append(" UNION ALL ");

		//MONTO TOTAL CARTERA AFP-FCI
		sbQuery.append(" SELECT ");
		sbQuery.append("   '6' TIPO_REPORTE, ");
		sbQuery.append("   'Monto Total de Cartera de AFP - FCI' DESC_REPORTE, ");
		sbQuery.append("   TA.INSTRUMENT_TYPE, ");
		sbQuery.append("   PT.PARAMETER_NAME, ");
		sbQuery.append("   SUM(TA.VALOR_NOMINAL * TC_USD) VALOR_NOMINAL, ");
		sbQuery.append("   SUM(TA.VALOR_MARKET * TC_USD)  VALOR_MARKET ");

		sbQuery.append(" FROM PARAMETER_TABLE PT ");
		sbQuery.append("   LEFT JOIN ( ");
		sbQuery.append("     SELECT ");
		sbQuery.append("       S.INSTRUMENT_TYPE, ");
		sbQuery.append(sbSubQuery.toString());
		sbQuery.append("      1 * PSV.TOTAL_BALANCE VALOR_MARKET, ");
		sbQuery.append(sbCurrencyQuery.toString());
		sbQuery.append("     from BALANCE_PSY_VIEW PSV ");
//		sbQuery.append("  	INNER JOIN PHYSICAL_CERT_MARKETFACT HMB ON HMB.ID_PHYSICAL_CERTIFICATE_FK = PSV.ID_PHYSICAL_CERTIFICATE_PK       ");
		sbQuery.append("  	inner join holder_Account ha   		 on PSV.id_holder_Account_pk=HA.id_holder_Account_pk                         ");
		sbQuery.append("  	Inner join participant pa 				 On PSV.id_participant_pk=pa.id_participant_pk                           ");
		sbQuery.append("  	Inner join security S 				 On PSV.id_security_code_pk = S.id_security_code_pk                          ");
		sbQuery.append("     WHERE 1 = 1 ");
		sbQuery.append("     AND trunc(PSV.CUT_DATE ) = TO_DATE('"+ to.getCutDate() +"','DD/MM/YYYY')  ");
		sbQuery.append("       AND EXISTS ");
		sbQuery.append("         (SELECT 1 ");
		sbQuery.append("       	 FROM HOLDER_ACCOUNT_DETAIL HAD ");
        sbQuery.append("           INNER JOIN HOLDER H  ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
        sbQuery.append("                              		AND H.ECONOMIC_ACTIVITY IN (42) ");
		sbQuery.append("                                    AND H.FUND_TYPE='FCI' ");
        sbQuery.append("         WHERE HAD.ID_HOLDER_ACCOUNT_FK = PSV.ID_HOLDER_ACCOUNT_PK ");
        sbQuery.append("         UNION ALL ");
        sbQuery.append("         SELECT 1 ");
		sbQuery.append("       	 FROM HOLDER_ACCOUNT_DETAIL HAD ");
        sbQuery.append("           INNER JOIN HOLDER H  ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK AND H.ID_HOLDER_PK = 455 ");
        sbQuery.append("         WHERE HAD.ID_HOLDER_ACCOUNT_FK = PSV.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append("         ) ");

		sbQuery.append("   ) TA ON TA.INSTRUMENT_TYPE = PT.PARAMETER_TABLE_PK ");

		sbQuery.append(" WHERE PT.MASTER_TABLE_FK = 50 ");
		sbQuery.append(" GROUP BY TA.INSTRUMENT_TYPE, PT.PARAMETER_NAME ");

		sbQuery.append(" UNION ALL ");

		//MONTO TOTAL DE CARTERA DE ENTIDADES FINANCIERAS (BANCO, MUTUAL, COOPERATIVA, FONDO FINANCIERO)
		sbQuery.append(" SELECT ");
		sbQuery.append("   '7' TIPO_REPORTE, ");
		sbQuery.append("   'Monto Total de Cartera de Entidades Financieras (Banco,Mutual,Cooperativa,Fondo Financiero)' DESC_REPORTE, ");
		sbQuery.append("   TA.INSTRUMENT_TYPE, ");
		sbQuery.append("   PT.PARAMETER_NAME, ");
		sbQuery.append("   SUM(TA.VALOR_NOMINAL * TC_USD) VALOR_NOMINAL, ");
		sbQuery.append("   SUM(TA.VALOR_MARKET * TC_USD)  VALOR_MARKET ");

		sbQuery.append(" FROM PARAMETER_TABLE PT ");
		sbQuery.append("   LEFT JOIN ( ");
		sbQuery.append("     SELECT ");
		sbQuery.append("       S.INSTRUMENT_TYPE, ");
		sbQuery.append(sbSubQuery.toString());
		sbQuery.append("      1 * PSV.TOTAL_BALANCE VALOR_MARKET, ");
		sbQuery.append(sbCurrencyQuery.toString());
		sbQuery.append("     from BALANCE_PSY_VIEW PSV ");
//		sbQuery.append("  	INNER JOIN PHYSICAL_CERT_MARKETFACT HMB ON HMB.ID_PHYSICAL_CERTIFICATE_FK = PSV.ID_PHYSICAL_CERTIFICATE_PK       ");
		sbQuery.append("  	inner join holder_Account ha   		 on PSV.id_holder_Account_pk=HA.id_holder_Account_pk                         ");
		sbQuery.append("  	Inner join participant pa 				 On PSV.id_participant_pk=pa.id_participant_pk                           ");
		sbQuery.append("  	Inner join security S 				 On PSV.id_security_code_pk = S.id_security_code_pk                          ");
		sbQuery.append("     WHERE 1 = 1 ");
		sbQuery.append("     AND trunc(PSV.CUT_DATE ) = TO_DATE('"+ to.getCutDate() +"','DD/MM/YYYY')  ");
		sbQuery.append("		AND PA.ACCOUNT_CLASS IN (32,33,34,35,30,1961) ");
		sbQuery.append("       AND NOT EXISTS ");
		sbQuery.append("         (SELECT 1 ");
		sbQuery.append("       	 FROM HOLDER_ACCOUNT_DETAIL HAD ");
        sbQuery.append("           INNER JOIN HOLDER H  ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
        sbQuery.append("           							AND H.ECONOMIC_ACTIVITY IN (1879,55,56,57,48) ");//se excluye a las safis y fondos de inversion y las companias de seguro
        sbQuery.append("         WHERE HAD.ID_HOLDER_ACCOUNT_FK = PSV.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append("         ) ");
		sbQuery.append("   ) TA ON TA.INSTRUMENT_TYPE = PT.PARAMETER_TABLE_PK ");

		sbQuery.append(" WHERE PT.MASTER_TABLE_FK = 50 ");
		sbQuery.append(" GROUP BY TA.INSTRUMENT_TYPE, PT.PARAMETER_NAME ");
		sbQuery.append(" ORDER BY 1,3 ");
		
		return sbQuery.toString();
	}

	//DEMATERIALIZED
    public String getQueryClientPortafolio(ClientPortafolioTO clientPortafolioTO, Long idStkCalcProcess){
    	StringBuilder sbQuery= new StringBuilder();
    	
		sbQuery.append(" select	");
  		sbQuery.append(" 	TO_NUMBER(ID_PARTICIPANT_PK) ID_PARTICIPANT_PK, ");
		sbQuery.append(" 	DESCRIPTION, ");
		sbQuery.append(" 	MNEMONIC, ");
		sbQuery.append(" 	TO_NUMBER(CURRENCY) CURRENCY, ");
		sbQuery.append(" 	CURRENCY_MNEMONIC, ");
		sbQuery.append(" 	MNEMONIC_ISSUER MNEMONIC_ISSUER, ");
		sbQuery.append(" 	BUSINESS_NAME BUSINESS_NAME, ");
		sbQuery.append(" 	TO_NUMBER(ID_HOLDER_ACCOUNT_PK) ID_HOLDER_ACCOUNT_PK, ");
		sbQuery.append(" 	HOLDERS HOLDERS, ");
		sbQuery.append(" 	HOLDERSDESC HOLDERSDESC, ");
		sbQuery.append(" 	TO_NUMBER(ACCOUNT_NUMBER) ACCOUNT_NUMBER, ");
		sbQuery.append(" 	TO_NUMBER(SECURITY_CLASS) SECURITY_CLASS, ");
		sbQuery.append(" 	SECURITY_CLASS_MNEMONIC SECURITY_CLASS_MNEMONIC, ");
		sbQuery.append(" 	ID_SECURITY_CODE_PK ID_SECURITY_CODE_PK, ");
		sbQuery.append(" 	FECHAEMISION FECHAEMISION, ");
		sbQuery.append(" 	TO_NUMBER(SECURITY_DAYS_TERM) SECURITY_DAYS_TERM, ");
		sbQuery.append(" 	TO_NUMBER(INITIAL_NOMINAL_VALUE) INITIAL_NOMINAL_VALUE, ");
		sbQuery.append(" 	TO_NUMBER(INTEREST_RATE) INTEREST_RATE, ");
		sbQuery.append(" 	TO_NUMBER(SECURITY_TYPE) SECURITY_TYPE, ");
		sbQuery.append(" 	TO_NUMBER(VALORFINAL) VALORFINAL, ");
		sbQuery.append(" 	TO_NUMBER(DIASVIGENTES) DIASVIGENTES, ");
		sbQuery.append(" 	TO_NUMBER(VALOR_PRESENTE) VALOR_PRESENTE, ");
		sbQuery.append(" 	TOTAL_BALANCE, ");
		sbQuery.append(" 	AVAILABLE_BALANCE, ");
		sbQuery.append(" 	PAWN_BALANCE, ");
		sbQuery.append(" 	BAN_BALANCE, ");
		sbQuery.append(" 	OTHER_BLOCK_BALANCE, ");
		sbQuery.append(" 	ACCREDITATION_BALANCE, ");
		sbQuery.append(" 	MARGIN_BALANCE, ");
		sbQuery.append(" 	REPORTING_BALANCE, ");
		sbQuery.append(" 	REPORTED_BALANCE, ");
		sbQuery.append(" 	PURCHASE_BALANCE, ");
		sbQuery.append(" 	SALE_BALANCE, ");
		sbQuery.append(" 	TRANSIT_BALANCE, ");
		sbQuery.append(" 	TASA_MERCADO, ");
		sbQuery.append(" 	PRECIO_MERCADO, ");
		sbQuery.append(" 	TOTAL_AMOUNT_MF, ");
		sbQuery.append(" 	THEORIC_RATE, ");
		sbQuery.append(" 	THEORIC_PRICE, ");
		sbQuery.append(" 	TOTAL_AMOUNT, ");
		sbQuery.append(" 	CURRENT_NOMINAL_VALUE, ");
		sbQuery.append(" 	CURRENT_TOTAL_AMOUNT, ");
		sbQuery.append(" 	alternative_code, ");
		sbQuery.append(" 	to_char(expiration_date,'dd/mm/yyyy'), ");
		sbQuery.append(" 	holder_type, ");
		sbQuery.append(" 	document_number, ");
		sbQuery.append(" 	serie, ");
		
		sbQuery.append(" 	economicActivity, ");
		sbQuery.append(" 	initialNValueUSD, ");
		sbQuery.append(" 	currentNValueUSD, ");
		sbQuery.append(" 	marketPriceUSD ");
		
		sbQuery.append(" 	,CUT_DATE ");
		sbQuery.append(" 	,INVESTOR_TYPE ");
		
		sbQuery.append(" from ");
		sbQuery.append(" 	(select /*+ USE_NL(ISS,SECU,P,MFV) */ ");
		sbQuery.append(" 	p.id_participant_pk||HA.id_holder_account_pk||secu.id_security_code_pk secuencia, ");
		sbQuery.append(" 	p.id_participant_pk, ");
		sbQuery.append(" 	p.description, ");
		sbQuery.append(" 	p.mnemonic, ");																										 
		sbQuery.append(" 	secu.currency, ");																																				 
		sbQuery.append(" 	(select pt.text1 from parameter_table pt where pt.parameter_table_pk=secu.currency) currency_mnemonic, ");														 
		sbQuery.append(" 	iss.mnemonic as MNEMONIC_ISSUER, ");																															 
		sbQuery.append(" 	iss.business_name, ");																																			 
		sbQuery.append(" 	HA.id_holder_account_pk, ");																																	 
		sbQuery.append(" 	(SELECT LISTAGG(HAD.ID_HOLDER_FK , ',' ||chr(13) ) WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");																			 
		sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD ");																																 
		sbQuery.append(" 	WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ");																									 
		sbQuery.append(" 	) Holders, ");
		sbQuery.append("    (SELECT LISTAGG((select h.full_name from holder h where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK) ||chr(13)) WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
		sbQuery.append("    FROM HOLDER_ACCOUNT_DETAIL HAD	");
		sbQuery.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK " );
		sbQuery.append(" 	) HoldersDesc, ");
		sbQuery.append(" 	HA.account_number, ");
		//
		sbQuery.append(" 	secu.ALTERNATIVE_CODE, ");
		sbQuery.append(" 	CASE WHEN secu.security_class != "+SecurityClassType.CFC.getCode()+" THEN secu.expiration_date ELSE secu.expiration_fondo_date END expiration_date, ");
		//sbQuery.append(" 	secu.expiration_date, ");
		sbQuery.append(" 	(select parameter_name from parameter_table where parameter_table_pk=(SELECT distinct (select h.holder_type from holder h where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK)) as holder_type, ");
		sbQuery.append(" 	(SELECT LISTAGG((select h.document_number from holder h where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK) ||chr(13)) WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)     FROM HOLDER_ACCOUNT_DETAIL HAD	    WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK  	) document_number, ");
		sbQuery.append(" 	secu.id_security_code_only as serie, ");
		
		sbQuery.append(" 	secu.security_class, ");																																	 
		sbQuery.append(" 	(select pt.text1 from parameter_table pt where pt.parameter_table_pk=secu.security_class) security_class_mnemonic, ");											 
		sbQuery.append(" 	secu.id_security_code_pk, ");																																	 
		sbQuery.append(" 	(to_char(secu.issuance_date,'dd/MM/YYYY')) fechaEmision, ");																									 
		sbQuery.append(" 	secu.security_days_term, ");																																	 
		sbQuery.append(" 	secu.initial_nominal_value, ");
		sbQuery.append(" 	secu.interest_rate, ");
		sbQuery.append(" 	secu.security_type, ");

		sbQuery.append("	( (secu.INITIAL_NOMINAL_VALUE) * (  1+(secu.INTEREST_RATE/100)*  ( secu.security_days_term )/ 360 )) as ValorFinal,  ");//as ValorFinal,
		sbQuery.append(" 	GREATEST(0,nvl(trunc(CASE WHEN secu.security_class != "+SecurityClassType.CFC.getCode()+" THEN secu.expiration_date ELSE secu.expiration_fondo_date END) - TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY'),0)) DiasVigentes, ");
		//sbQuery.append(" 	GREATEST(0,nvl(trunc(secu.expiration_date) - TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY'),0)) DiasVigentes, ");
		sbQuery.append("	ROUND( ( (secu.INITIAL_NOMINAL_VALUE) * (  1+(secu.INTEREST_RATE/100)*  ( secu.security_days_term )/ 360 )) / (1+(secu.INTEREST_RATE/100) * GREATEST(0,nvl(trunc(secu.expiration_date) -  TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY'),0)) /360 ) ,  2)  VALOR_PRESENTE   ,  ");//as ValorFinal,
		sbQuery.append(" 	MFV.total_balance, "); 
		sbQuery.append(" 	MFV.available_balance, ");
		sbQuery.append(" 	MFV.pawn_balance, "); 
		sbQuery.append(" 	MFV.ban_balance, ");
		sbQuery.append(" 	MFV.other_block_balance, ");
		sbQuery.append(" 	MFV.accreditation_balance, ");
		sbQuery.append(" 	MFV.margin_balance, ");
		sbQuery.append(" 	MFV.reporting_balance, ");
		sbQuery.append(" 	MFV.reported_balance, ");
		sbQuery.append(" 	MFV.purchase_balance, ");
		sbQuery.append(" 	MFV.sale_balance, ");
		sbQuery.append(" 	MFV.transit_balance, ");
		sbQuery.append(" 	NVL(MFV.market_rate,0) TASA_MERCADO, ");
		sbQuery.append(" 	NVL(MFV.market_price,0) PRECIO_MERCADO, ");
		sbQuery.append(" 	NVL(ROUND(MFV.market_price,2),0) * MFV.total_balance as TOTAL_AMOUNT_MF, ");
		sbQuery.append(" 	NVL(MFV.THEORIC_RATE,0) THEORIC_RATE, ");
		sbQuery.append(" 	NVL(MFV.THEORIC_PRICE,0) THEORIC_PRICE, ");
		sbQuery.append(" 	NVL(MFV.THEORIC_PRICE,0) * MFV.total_balance total_amount, ");
		
		
		sbQuery.append(" NVL(CASE WHEN HA.ACCOUNT_TYPE in ( 106, 108 ) THEN 'PARTICULARES'                                                                              ");
		sbQuery.append("         WHEN HA.ACCOUNT_TYPE = 107 THEN ( select PT_SUB.DESCRIPTION                                                                            ");
		sbQuery.append("                                            from holder H_SUB                                                                                   ");
		sbQuery.append("                                            inner join HOLDER_ACCOUNT_DETAIL HAD_SUB on HAD_SUB.ID_HOLDER_FK = H_SUB.ID_HOLDER_PK               ");
		sbQuery.append("                                            inner join HOLDER_ACCOUNT HA_SUB ON HAD_SUB.ID_HOLDER_ACCOUNT_FK = HA_SUB.ID_HOLDER_ACCOUNT_PK      ");
		sbQuery.append("                                            inner join PARAMETER_TABLE PT_SUB on PT_SUB.PARAMETER_TABLE_PK = H_SUB.ECONOMIC_ACTIVITY            ");
		sbQuery.append("                                            where 1 = 1                                                                                         ");
		sbQuery.append("                                            and HA_SUB.ID_HOLDER_ACCOUNT_PK = HA.ID_HOLDER_ACCOUNT_PK)                                          ");
		sbQuery.append("        END,'POR COMPLETAR') economicActivity,                                                                                                   ");
		
		sbQuery.append("	round(((MFV.total_balance * secu.initial_nominal_value) *    ");  
		sbQuery.append("	CASE WHEN secu.CURRENCY = 127 THEN 1 /                                                                                          ");  
		sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
		sbQuery.append("						 AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)                                              ");  
		sbQuery.append("		 WHEN secu.CURRENCY IN (1734,1304) THEN 1 *                                                                                 ");  
		sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
		sbQuery.append("						AND D.ID_CURRENCY = secu.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /                                   ");  
		sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
		sbQuery.append("						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)                                               ");  
		sbQuery.append("		 WHEN secu.CURRENCY IN (430,1853) THEN 1 ELSE 1 END ),2) initialNValueUSD,                                                  ");  
		
//		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
//			sbQuery.append("	round(((MFV.total_balance * ");  
//			sbQuery.append("			(DECODE(SECU.SECURITY_CLASS,426,ROUND( NVH.NOMINAL_VALUE ,4),        ROUND( NVH.NOMINAL_VALUE ,2))) 			");
//			sbQuery.append("	) *       ");                                                                                     
//		
//		}else{
			sbQuery.append("	round(((MFV.total_balance *  DECODE(SECU.SECURITY_CLASS,426,ROUND( secu.CURRENT_NOMINAL_VALUE ,4),        ROUND( secu.CURRENT_NOMINAL_VALUE ,2))) *                                                                   ");  
//		}
		sbQuery.append("	CASE WHEN secu.CURRENCY = 127 THEN 1 /                                                                                          ");  
		sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
		sbQuery.append("						 AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)                                              ");  
		sbQuery.append("		 WHEN secu.CURRENCY IN (1734,1304) THEN 1 *                                                                                 ");  
		sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
		sbQuery.append("						AND D.ID_CURRENCY = secu.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /                                   ");  
		sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
		sbQuery.append("						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)                                               ");  
		sbQuery.append("		 WHEN secu.CURRENCY IN (430,1853) THEN 1 ELSE 1 END ),2) currentNValueUSD,                                                  ");  
		
		sbQuery.append("	round(((NVL(MFV.total_balance * ROUND(NVL(MFV.market_price,0),2),0)) *                                                                                              ");  
		sbQuery.append("	CASE WHEN secu.CURRENCY = 127 THEN 1 /                                                                                          ");  
		sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
		sbQuery.append("						 AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)                                              ");  
		sbQuery.append("		 WHEN secu.CURRENCY IN (1734,1304) THEN 1 *                                                                                 ");  
		sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
		sbQuery.append("						AND D.ID_CURRENCY = secu.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /                                   ");  
		sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
		sbQuery.append("						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)                                               ");  
		sbQuery.append("		 WHEN secu.CURRENCY IN (430,1853) THEN 1 ELSE 1 END ),2) marketPriceUSD,                                                    ");  
		
		sbQuery.append("    DECODE(secu.security_class,426,ROUND(");
//		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
//			sbQuery.append("			NVH.NOMINAL_VALUE 			");
//		}else{
			sbQuery.append("			secu.current_nominal_value ");
//		}
		sbQuery.append("		  	,4),ROUND( ");
//		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
//			sbQuery.append("			NVH.NOMINAL_VALUE			");
//		}else{
			sbQuery.append("			secu.current_nominal_value ");
//		}
		sbQuery.append(" 			,2)) ");
		sbQuery.append(" 	as current_nominal_value, ");
		sbQuery.append("    DECODE(secu.security_class,426,ROUND( ");
//		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
//			sbQuery.append("			NVH.NOMINAL_VALUE			");
//		}else{
			sbQuery.append("			secu.current_nominal_value ");
//		}
		sbQuery.append("		  	,4),ROUND( ");
//		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
//			sbQuery.append("			NVH.NOMINAL_VALUE			");
//		}else{
			sbQuery.append("			secu.current_nominal_value ");
//		}
		sbQuery.append(" 			,2)) * MFV.total_balance ");
		sbQuery.append(" 	as current_total_amount ");
		
		sbQuery.append(" 	, '"+clientPortafolioTO.getDateMarketFactString()+"' as CUT_DATE ");
		sbQuery.append(" 	,FN_GET_HOLDER_TYPE_DESC(HA.ID_HOLDER_ACCOUNT_PK) as INVESTOR_TYPE");
		sbQuery.append(" From holder_account ha ");
		sbQuery.append(" inner join market_fact_view mfv on mfv.id_holder_account_pk = HA.id_holder_account_pk							");
		sbQuery.append(" Inner join participant p	ON mfv.id_participant_pk = p.id_participant_pk									  	");
		sbQuery.append(" Inner join security secu ON mfv.id_security_code_pk	= secu.id_security_code_pk  							");
		sbQuery.append(" Inner join issuer iss 	ON secu.id_issuer_fk				= iss.id_issuer_pk 									");
		
//		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
//			sbQuery.append("			INNER JOIN NOMINAL_VALUE_HISTORY_VIEW NVH ON NVH.CUT_DATE = MFV.CUT_DATE 		");
//			sbQuery.append("			AND secu.ID_SECURITY_CODE_PK = NVH.ID_SECURITY_CODE_FK							");
//		}

		sbQuery.append(" Where secu.security_type in (1762,1763)																													");
		
		// Optimizacion Query
		sbQuery.append(" 	AND trunc(MFV.CUT_DATE)= TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
//		sbQuery.append(" AND MFV.CUT_DATE >= TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') ");
//		sbQuery.append(" AND MFV.CUT_DATE < TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 ");
//		sbQuery.append(" AND TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY') ");
		
		sbQuery.append(" 	AND (MFV.total_balance > 0 or MFV.reported_balance > 0 or MFV.purchase_balance > 0)																									");
		//validacion de los participantes inversionistas
		if(clientPortafolioTO.getIsParticipantInvestor()){ 
			sbQuery.append(" 	AND (EXISTS																						    												");
			sbQuery.append(" 		(SELECT 1																																		");
			sbQuery.append(" 		FROM HOLDER_ACCOUNT_DETAIL HAD																													");
			sbQuery.append(" 		INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK																						");
			sbQuery.append(" 		WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.id_holder_account_pk																						");
			sbQuery.append(" 			AND H.DOCUMENT_TYPE = p.DOCUMENT_TYPE																										");
			sbQuery.append(" 			AND H.DOCUMENT_NUMBER = p.DOCUMENT_NUMBER																									");
			sbQuery.append(" 	))																																					");
		}
		
		if(Validations.validateIsNotNull(clientPortafolioTO.getIdParticipant())){
			sbQuery.append(" and p.id_participant_pk= :participantPk																												");
		}
		if(Validations.validateIsNotNull(clientPortafolioTO.getHolderAccountPk())){
    		sbQuery.append(" and HA.id_holder_account_pk=:holder_account																											");
    	}
		if(Validations.validateIsNotNull(clientPortafolioTO.getIdSecurityCode())){
    		sbQuery.append(" and secu.id_security_code_pk= :securityPk																												");
    	}
		
		if(clientPortafolioTO.getShowAvailableBalance().equals(Integer.valueOf("1"))){
			sbQuery.append(" and MFV.reported_balance > 0																															");
		}
		
		if(Validations.validateIsNotNull(clientPortafolioTO.getCui())){
			sbQuery.append(" AND (EXISTS																																			");
			sbQuery.append(" 	(SELECT had.ID_HOLDER_FK																															");
			sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL had																														");
			sbQuery.append(" 	WHERE had.ID_HOLDER_FK = :cui_code																													");
			sbQuery.append(" 		AND had.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK))																						");
	    }
		if(clientPortafolioTO.getInstitutionType().equals("186") && clientPortafolioTO.getSecurityClass() == null){
			sbQuery.append(" and secu.SECURITY_CLASS in (420,1976)													 																");
	  	}		
		if(Validations.validateIsNotNull(clientPortafolioTO.getSecurityClass())){
    		sbQuery.append(" and secu.security_class in (:security_class)");
    	}
		if(Validations.validateIsNotNull(clientPortafolioTO.getIdIssuer())){
    		sbQuery.append(" and iss.id_issuer_pk=:issuerPk");
    	}
		if(Validations.validateIsNotNull(clientPortafolioTO.getIdCurrency())){
    		sbQuery.append(" and secu.currency=:currency");
    	}
		//VALORES SUSPENDIDOS Issue 1244
		if(Validations.validateIsNotNull(clientPortafolioTO.getSuspendedSecurities())){
			//excluyendo valores suspendidos
			if(clientPortafolioTO.getSuspendedSecurities().equalsIgnoreCase("1"))
				sbQuery.append(" AND nvl(secU.expiration_date, MFV.CUT_DATE + 1) > TO_DATE('" + clientPortafolioTO.getDateMarketFactString() + "','DD/MM/YYYY') ");
			//mostrando solo valores suspendidos
			if(clientPortafolioTO.getSuspendedSecurities().equalsIgnoreCase("2"))
				sbQuery.append(" AND secu.expiration_date <= TO_DATE('" + clientPortafolioTO.getDateMarketFactString() + "','DD/MM/YYYY') AND (MFV.PAWN_BALANCE>0 or MFV.BAN_BALANCE>0 or MFV.OTHER_BLOCK_BALANCE>0) ");
		}
		
		sbQuery.append(" )T  Order by mnemonic, security_type, currency_mnemonic, security_class_mnemonic ,id_security_code_pk, Holders ");

		String strQueryFormatted = sbQuery.toString();
		
		if(clientPortafolioTO.getIdParticipant()!=null){
    		strQueryFormatted= strQueryFormatted.replace(":participantPk", clientPortafolioTO.getIdParticipant().toString());
    	}
    	if(clientPortafolioTO.getHolderAccountPk()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":holder_account", clientPortafolioTO.getHolderAccountPk().toString());
    	}
    	if(clientPortafolioTO.getIdSecurityCode()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":securityPk", "'"+clientPortafolioTO.getIdSecurityCode()+"'");
    	}
    	if(Validations.validateIsNotNullAndPositive(clientPortafolioTO.getCui())){
    		strQueryFormatted=strQueryFormatted.replace(":cui_code", clientPortafolioTO.getCui().toString());
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(clientPortafolioTO.getSecurityClass())){
    		strQueryFormatted=strQueryFormatted.replace(":security_class", clientPortafolioTO.getSecurityClass());
    	}
    	if(clientPortafolioTO.getIdIssuer()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":issuerPk", "'"+clientPortafolioTO.getIdIssuer()+"'");
    	}
    	if(clientPortafolioTO.getIdCurrency()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":currency", clientPortafolioTO.getIdCurrency().toString());
    	}
    	if(clientPortafolioTO.getSuspendedSecurities() != null){
    		strQueryFormatted=strQueryFormatted.replace(":suspended_securities", clientPortafolioTO.getSuspendedSecurities());
    	}
    	
    	System.out.println(strQueryFormatted);
    	return strQueryFormatted;
    }

    /**
     * 
     * @param clientPortafolioTO
     * @param idStkCalcProcess
     * @return
     */
    /*cartera desmaterializada*/
    public String getSubTotalQueryClientPortafolio(ClientPortafolioTO clientPortafolioTO, Long idStkCalcProcess){
    	StringBuilder sbQuery= new StringBuilder();

		sbQuery.append(" SELECT 																															");
		sbQuery.append(" 	(SELECT PARAMETER_NAME FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = S.CURRENCY) CURRENCY,									");   
		sbQuery.append(" 	NVL(SUM(MFV.TOTAL_BALANCE),0) TOTAL_VALORES,																					");   
		sbQuery.append(" 	NVL(SUM(																														");    
		sbQuery.append(" 		S.INITIAL_NOMINAL_VALUE * MFV.TOTAL_BALANCE *																				");      
		sbQuery.append(" 		CASE																														");     
		sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1																							");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304,430) THEN 1 *																				"); 
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		// OPTIMIZACION 1
		//sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1)										");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1853) THEN 1 *																						"); 
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		// OPTIMIZACION 2
		//sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)												");
		sbQuery.append(" 			ELSE 1																													");   
		sbQuery.append(" 	END),0) TOTAL_BOB,																												");    
		sbQuery.append("	NVL(SUM(																														"); 
		sbQuery.append(" 		S.INITIAL_NOMINAL_VALUE * MFV.TOTAL_BALANCE *																				");
		sbQuery.append(" 		CASE																														");
		sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1 /																							");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		// OPTIMIZACION 3
		//sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)												");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304) THEN 1 *																					");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		// OPTIMIZACION 4
		//sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /										");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		// OPTIMIZACION 5
		//sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)												");
		sbQuery.append(" 			WHEN S.CURRENCY IN (430,1853) THEN 1																					");
		sbQuery.append(" 			ELSE 1																													");
		sbQuery.append(" 	END),0) TOTAL_USD,																												");
		
		sbQuery.append(" 	NVL(SUM(																														");
		sbQuery.append(" 		ROUND(NVL(MFV.market_price,0),2) * MFV.total_balance *																		");
		sbQuery.append(" 		CASE																														");
		sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1																							");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304,430) THEN 1 *																				");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		// OPTIMIZACION 6
		//sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1)										");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1853) THEN 1 *																						");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		// OPTIMIZACION 7
		//sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)												");
		sbQuery.append(" 			ELSE 1																													");
		sbQuery.append(" 		END),0) TOTAL_PRECIO_MRKT_BOB,																								");
		sbQuery.append(" 	NVL(SUM(																														");
		sbQuery.append(" 		ROUND(NVL(MFV.market_price,0),2) * MFV.total_balance *																		");
		sbQuery.append(" 		CASE																														");
		sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1 /																							");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		// OPTIMIZACION 8
		//sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)												");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304) THEN 1 *																					");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		// OPTIMIZACION 9
		//sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /										");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		// OPTIMIZACION 10
		//sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 					AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)													");
		sbQuery.append(" 			WHEN S.CURRENCY IN (430,1853) THEN 1																					");
		sbQuery.append(" 			ELSE 1																													");
		sbQuery.append(" 		END),0) TOTAL_PRECIO_MRKT_USD,																								");
		
		sbQuery.append(" 	NVL(SUM(																														");    
		sbQuery.append(" 		DECODE(S.SECURITY_CLASS,426,ROUND( 																							");
//		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
//		
//			sbQuery.append("			NVH.NOMINAL_VALUE			");
//		}else{
			sbQuery.append("			S.current_nominal_value									 															");
//		}
		sbQuery.append("		,4),ROUND( 																													");
//		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
//			// OPTIMIZACION 12
//			sbQuery.append("			NVH.NOMINAL_VALUE 			");
//		}else{
			sbQuery.append("			S.current_nominal_value									 															");
//		}
		sbQuery.append("		,2)) * MFV.TOTAL_BALANCE *																									");      
		sbQuery.append(" 		CASE																														");     
		sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1																							");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304,430) THEN 1 *																				"); 
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		// OPTIMIZACION 13
		//sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1)										");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1853) THEN 1 *																						"); 
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		// OPTIMIZACION 14
		//sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)												");
		sbQuery.append(" 			ELSE 1																													");   
		sbQuery.append(" 	END),0) TOTAL_CURRENT_NOMINAL_BOB,																								");    
		sbQuery.append(" 	NVL(SUM(																														");    
		sbQuery.append(" 		DECODE(S.SECURITY_CLASS,426,ROUND( 																							");
//		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
//			// OPTIMIZACION 15
//			sbQuery.append("			NVH.NOMINAL_VALUE     			");
//		}else{
			sbQuery.append("			S.current_nominal_value									 															");
//		}
		sbQuery.append("		,4),ROUND( 																													");
//		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
//			// OPTIMIZACION 16 
//			sbQuery.append("			NVH.NOMINAL_VALUE     			");
//		}else{
			sbQuery.append("			S.current_nominal_value									 															");
//		}
		sbQuery.append("		,2)) * MFV.TOTAL_BALANCE *																									");
		sbQuery.append(" 		CASE																														");
		sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1 /																							");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		// OPTIMIZACION 17
		//sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)												");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304) THEN 1 *																					");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		// OPTIMIZACION 18
		//sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");		
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /										");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		// OPTIMIZACION 19
		//sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)												");
		sbQuery.append(" 			WHEN S.CURRENCY IN (430,1853) THEN 1																					");
		sbQuery.append(" 			ELSE 1																													");
		sbQuery.append(" 	END),0) TOTAL_CURRENT_NOMINAL_USD																								");
		
		sbQuery.append(" FROM MARKET_FACT_VIEW MFV																											");
		sbQuery.append(" INNER JOIN PARTICIPANT P                    ON P.ID_PARTICIPANT_PK = MFV.ID_PARTICIPANT_PK											");
		sbQuery.append(" INNER JOIN SECURITY S                       ON S.ID_SECURITY_CODE_PK = MFV.ID_SECURITY_CODE_PK										");							
		sbQuery.append(" INNER JOIN HOLDER_ACCOUNT HA                ON HA.ID_HOLDER_ACCOUNT_PK = MFV.ID_HOLDER_ACCOUNT_PK									");
		sbQuery.append(" INNER JOIN ISSUER ISU                       ON ISU.ID_ISSUER_PK = S.ID_ISSUER_FK													");							
		
//		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
//			sbQuery.append("			INNER JOIN NOMINAL_VALUE_HISTORY_VIEW NVH ON NVH.CUT_DATE = MFV.CUT_DATE 		");
//			sbQuery.append("			AND S.ID_SECURITY_CODE_PK = NVH.ID_SECURITY_CODE_FK							");
//		}
		
		sbQuery.append(" 	WHERE s.security_type in (1762,1763) AND S.ISSUANCE_FORM = 132																						");

		// Optimizacion Query
		//sbQuery.append("	AND TRUNC(MFV.CUT_DATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 								");
		sbQuery.append(" AND MFV.CUT_DATE >= TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') ");
		sbQuery.append(" AND MFV.CUT_DATE < TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 ");
		sbQuery.append(" AND TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY') ");
		
		sbQuery.append(" 	AND (MFV.total_balance > 0 or MFV.reported_balance > 0 or MFV.purchase_balance > 0)																			");
		//validacion de los participantes inversionistas
		if(clientPortafolioTO.getIsParticipantInvestor()){
			sbQuery.append(" 	AND (EXISTS																						    						");
			sbQuery.append(" 		(SELECT 1																												");
			sbQuery.append(" 		FROM HOLDER_ACCOUNT_DETAIL HAD																							");
			sbQuery.append(" 		INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK																");
			sbQuery.append(" 		WHERE HAD.ID_HOLDER_ACCOUNT_FK = MFV.ID_HOLDER_ACCOUNT_PK																");
			sbQuery.append(" 			AND H.DOCUMENT_TYPE = P.DOCUMENT_TYPE																				");
			sbQuery.append(" 			AND H.DOCUMENT_NUMBER = P.DOCUMENT_NUMBER																			");
			sbQuery.append(" 	))																															");
		}
		if(Validations.validateIsNotNull(clientPortafolioTO.getIdParticipant())){
			sbQuery.append("	and MFV.ID_PARTICIPANT_PK = :participantPk																					");
		}
		if(Validations.validateIsNotNull(clientPortafolioTO.getHolderAccountPk())){
    		sbQuery.append(" 	and HA.ID_HOLDER_ACCOUNT_PK =:holder_account																				");
    	}
		if(Validations.validateIsNotNull(clientPortafolioTO.getIdSecurityCode())){
    		sbQuery.append(" 	and S.ID_SECURITY_CODE_PK = :securityPk																						");
    	}
		if(clientPortafolioTO.getShowAvailableBalance().equals(Integer.valueOf("1"))){
			sbQuery.append(" 	and MFV.reported_balance > 0																								");
		}
		if(Validations.validateIsNotNull(clientPortafolioTO.getCui())){
			sbQuery.append(" 	AND (EXISTS																													");
			sbQuery.append(" 		(SELECT had.ID_HOLDER_FK																								");
			sbQuery.append(" 		FROM HOLDER_ACCOUNT_DETAIL had																							");
			sbQuery.append(" 		WHERE had.ID_HOLDER_FK = :cui_code																						");
			sbQuery.append(" 			AND had.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK))															");
	    }
		if(Validations.validateIsNotNull(clientPortafolioTO.getSecurityClass())){
    		sbQuery.append(" 	and S.SECURITY_CLASS in (:security_class)																					");
    	}
		if(clientPortafolioTO.getInstitutionType().equals("186") && clientPortafolioTO.getSecurityClass() == null){
			sbQuery.append(" 	and S.SECURITY_CLASS in (420,1976)							 																");
	  	}
		if(Validations.validateIsNotNull(clientPortafolioTO.getIdIssuer())){
    		sbQuery.append(" 	and ISU.ID_ISSUER_PK =:issuerPk																								");
    	}
		if(Validations.validateIsNotNull(clientPortafolioTO.getIdCurrency())){
    		sbQuery.append(" 	and S.CURRENCY =:currency																									");
    	}
		//VALORES SUSPENDIDOS Issue 1244
		if(Validations.validateIsNotNull(clientPortafolioTO.getSuspendedSecurities())){
			//excluyendo valores suspendidos
			if(clientPortafolioTO.getSuspendedSecurities().equalsIgnoreCase("1"))
				sbQuery.append(" AND nvl(s.expiration_date, MFV.CUT_DATE + 1) > TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY') ");
			//mostrando solo valores suspendidos
			if(clientPortafolioTO.getSuspendedSecurities().equalsIgnoreCase("2"))
				sbQuery.append(" AND s.expiration_date <= TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY') AND (MFV.PAWN_BALANCE>0 or MFV.BAN_BALANCE>0 or MFV.OTHER_BLOCK_BALANCE>0) ");
		}
		
		sbQuery.append(" GROUP BY S.CURRENCY																												");
		sbQuery.append(" ORDER BY S.CURRENCY																												");
		
		String strQueryFormatted = sbQuery.toString();
    	
		if(clientPortafolioTO.getIdParticipant()!=null){
    		strQueryFormatted= strQueryFormatted.replace(":participantPk", clientPortafolioTO.getIdParticipant().toString());
    	}
		if(clientPortafolioTO.getHolderAccountPk()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":holder_account", clientPortafolioTO.getHolderAccountPk().toString());
    	}
    	if(clientPortafolioTO.getIdSecurityCode()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":securityPk", "'"+clientPortafolioTO.getIdSecurityCode()+"'");
    	}
    	
    	if(Validations.validateIsNotNullAndPositive(clientPortafolioTO.getCui())){
    		strQueryFormatted=strQueryFormatted.replace(":cui_code", clientPortafolioTO.getCui().toString());
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(clientPortafolioTO.getSecurityClass())){
    		strQueryFormatted=strQueryFormatted.replace(":security_class", clientPortafolioTO.getSecurityClass());
    	}
    	if(clientPortafolioTO.getIdIssuer()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":issuerPk", "'"+clientPortafolioTO.getIdIssuer()+"'");
    	}
    	if(clientPortafolioTO.getIdCurrency()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":currency", clientPortafolioTO.getIdCurrency().toString());
    	}
    	if(clientPortafolioTO.getSuspendedSecurities() != null){
    		strQueryFormatted=strQueryFormatted.replace(":suspended_securities", clientPortafolioTO.getSuspendedSecurities());
    	}
    	
    	System.out.println(strQueryFormatted);
    	return strQueryFormatted;
    }

    //PHYSICAL
    public String getQueryClientPortafolio(ClientPortafolioTO clientPortafolioTO){
    	StringBuilder sbQuery= new StringBuilder();
    	
    	sbQuery.append(" select	");
  		sbQuery.append(" 	TO_NUMBER(ID_PARTICIPANT_PK) ID_PARTICIPANT_PK, ");
		sbQuery.append(" 	DESCRIPTION, ");
		sbQuery.append(" 	MNEMONIC, ");
		sbQuery.append(" 	TO_NUMBER(CURRENCY) CURRENCY, ");
		sbQuery.append(" 	CURRENCY_MNEMONIC, ");
		sbQuery.append(" 	MNEMONIC_ISSUER, ");
		sbQuery.append(" 	BUSINESS_NAME, ");
		sbQuery.append(" 	TO_NUMBER(ID_HOLDER_ACCOUNT_PK) ID_HOLDER_ACCOUNT_PK, ");
		sbQuery.append(" 	HOLDERS, ");  
		sbQuery.append(" 	HOLDERSDESC, ");  
		sbQuery.append(" 	TO_NUMBER(ACCOUNT_NUMBER) ACCOUNT_NUMBER, ");
		sbQuery.append(" 	TO_NUMBER(SECURITY_CLASS) SECURITY_CLASS, ");
		sbQuery.append(" 	SECURITY_CLASS_MNEMONIC, ");
		sbQuery.append(" 	ID_SECURITY_CODE_PK, ");
		sbQuery.append(" 	FECHAEMISION, ");
		sbQuery.append(" 	TO_NUMBER(SECURITY_DAYS_TERM) SECURITY_DAYS_TERM, ");
		sbQuery.append(" 	TO_NUMBER(INITIAL_NOMINAL_VALUE) INITIAL_NOMINAL_VALUE, ");
		sbQuery.append(" 	TO_NUMBER(INTEREST_RATE) INTEREST_RATE, ");
		sbQuery.append(" 	TO_NUMBER(SECURITY_TYPE) SECURITY_TYPE, ");
		sbQuery.append(" 	TO_NUMBER(VALORFINAL) VALORFINAL, ");
		sbQuery.append(" 	TO_NUMBER(DIASVIGENTES) DIASVIGENTES, ");
		sbQuery.append(" 	TO_NUMBER(VALOR_PRESENTE) VALOR_PRESENTE, ");
		sbQuery.append(" 	TOTAL_BALANCE, ");
		sbQuery.append(" 	AVAILABLE_BALANCE, ");
		sbQuery.append(" 	PAWN_BALANCE, ");
		sbQuery.append(" 	BAN_BALANCE, ");
		sbQuery.append(" 	OTHER_BLOCK_BALANCE, ");
		sbQuery.append(" 	ACCREDITATION_BALANCE, ");
		sbQuery.append(" 	MARGIN_BALANCE, ");
		sbQuery.append(" 	REPORTING_BALANCE, ");
		sbQuery.append(" 	REPORTED_BALANCE, ");
		sbQuery.append(" 	PURCHASE_BALANCE, ");
		sbQuery.append(" 	SALE_BALANCE, ");
		sbQuery.append(" 	TRANSIT_BALANCE, ");
		sbQuery.append(" 	TASA_MERCADO, ");
		sbQuery.append(" 	PRECIO_MERCADO, ");
		sbQuery.append(" 	TOTAL_AMOUNT_MF, ");
		sbQuery.append(" 	THEORIC_RATE, ");
		sbQuery.append(" 	THEORIC_PRICE, ");
		sbQuery.append(" 	TOTAL_AMOUNT, ");
		sbQuery.append(" 	CURRENT_NOMINAL_VALUE, ");
		sbQuery.append(" 	CURRENT_TOTAL_AMOUNT ");
		sbQuery.append(" 	, NULL as A ");
		sbQuery.append("	,EXPIRATION_DATE   ");
		sbQuery.append("	, NULL as B ");
		sbQuery.append("	, NULL as C  ");
		sbQuery.append("	, NULL as D  ");
		
		sbQuery.append(" 	,economicActivity, ");
		sbQuery.append(" 	initialNValueUSD, ");
		sbQuery.append(" 	currentNValueUSD, ");
		sbQuery.append(" 	marketPriceUSD ");
		
		sbQuery.append(" 	,CUT_DATE ");
		sbQuery.append(" 	,INVESTOR_TYPE ");
		
		sbQuery.append(" from ");
		sbQuery.append(" 	(select ");
		sbQuery.append(" 	p.id_participant_pk||HA.id_holder_account_pk||secu.id_security_code_pk secuencia, ");
		sbQuery.append(" 	p.id_participant_pk, ");
		sbQuery.append(" 	p.description, ");
		sbQuery.append(" 	p.mnemonic, ");																										 
		sbQuery.append(" 	secu.currency, ");																																				 
		sbQuery.append(" 	(select pt.text1 from parameter_table pt where pt.parameter_table_pk=secu.currency) currency_mnemonic, ");														 
		sbQuery.append(" 	iss.mnemonic as MNEMONIC_ISSUER, ");																															 
		sbQuery.append(" 	iss.business_name, ");																																			 
		sbQuery.append(" 	HA.id_holder_account_pk, ");																																	 
		sbQuery.append(" 	(SELECT LISTAGG(HAD.ID_HOLDER_FK , ',' ||chr(13)) WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");																			 
		sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD ");																																 
		sbQuery.append(" 	WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ");																									 
		sbQuery.append(" 	) Holders, ");
		sbQuery.append("    (SELECT LISTAGG((select h.full_name from holder h where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK) ||chr(13)) WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)");
		sbQuery.append("    FROM HOLDER_ACCOUNT_DETAIL HAD	");
		sbQuery.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK " );
		sbQuery.append(" 	) HoldersDesc, ");
		sbQuery.append(" 	HA.account_number, ");
		sbQuery.append(" 	secu.security_class, ");																																	 
		sbQuery.append(" 	(select pt.text1 from parameter_table pt where pt.parameter_table_pk=secu.security_class) security_class_mnemonic, ");											 
		sbQuery.append(" 	secu.id_security_code_pk, ");																																	 
		sbQuery.append(" 	(to_char(secu.issuance_date,'dd/MM/YYYY')) fechaEmision, ");																									 
		sbQuery.append(" 	secu.security_days_term, ");																																	 
		sbQuery.append(" 	secu.initial_nominal_value, ");
		sbQuery.append(" 	secu.interest_rate, ");
		sbQuery.append(" 	secu.security_type, ");
		sbQuery.append(" 	secu.initial_nominal_value+ ");
		sbQuery.append(" 	secu.initial_nominal_value*nvl(secu.interest_rate,0)/100 ValorFinal, ");											
		sbQuery.append(" 	GREATEST(0,nvl(trunc(CASE WHEN secu.security_class != "+SecurityClassType.CFC.getCode()+" THEN secu.expiration_date ELSE secu.expiration_fondo_date END) - trunc(sysdate),0)) DiasVigentes, ");
		//sbQuery.append(" 	GREATEST(0,nvl(trunc(secu.expiration_date) - trunc(sysdate),0)) DiasVigentes, ");
		sbQuery.append(" 	ROUND((secu.current_nominal_value/TO_NUMBER(POWER(1+(secu.INTEREST_RATE/100),(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')-secu.ISSUANCE_DATE)/360)) + secu.current_nominal_value),2) VALOR_PRESENTE, ");
		sbQuery.append(" 	PSV.TOTAL_BALANCE total_balance, "); 
		sbQuery.append(" 	PSV.AVAILABLE_BALANCE available_balance, ");
		sbQuery.append(" 	0 pawn_balance, "); 
		sbQuery.append(" 	0 ban_balance, ");
		sbQuery.append(" 	0 other_block_balance, ");
		sbQuery.append(" 	0 accreditation_balance, ");
		sbQuery.append(" 	0 margin_balance, ");
		sbQuery.append(" 	0 reporting_balance, ");
		sbQuery.append(" 	0 reported_balance, ");
		sbQuery.append(" 	0 purchase_balance, ");
		sbQuery.append(" 	0 sale_balance, ");
		sbQuery.append(" 	PSV.TRANSIT_BALANCE transit_balance, ");
		sbQuery.append(" 	DECODE(secu.INSTRUMENT_TYPE,124,1,null) TASA_MERCADO, ");
		sbQuery.append(" 	1 PRECIO_MERCADO, ");
		sbQuery.append(" 	1 * PSV.TOTAL_BALANCE as TOTAL_AMOUNT_MF, ");
		sbQuery.append(" 	1 THEORIC_RATE, ");
		sbQuery.append(" 	1 THEORIC_PRICE, ");
		sbQuery.append(" 	1 * PSV.TOTAL_BALANCE total_amount, ");
//		sbQuery.append("  DECODE(secu.security_class,426,ROUND(secu.current_nominal_value,4),ROUND(secu.current_nominal_value,2)) current_nominal_value, ");
//		sbQuery.append("  DECODE(secu.security_class,426,ROUND(secu.current_nominal_value,4),ROUND(secu.current_nominal_value,2)) * PBD.CERTIFICATE_QUANTITY current_total_amount ");
		
		sbQuery.append("    DECODE(secu.security_class,426,ROUND(");
			//if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
				//sbQuery.append("			NVH.NOMINAL_VALUE															");
			//}else{
				sbQuery.append("			secu.current_nominal_value ");
			//}
			sbQuery.append("		  	,4),ROUND( ");
			//if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
				//sbQuery.append("			NVH.NOMINAL_VALUE 															");
			//}else{
				sbQuery.append("			secu.current_nominal_value ");
			//}
			sbQuery.append(" 			,2)) ");
			sbQuery.append(" 	as current_nominal_value, ");
			sbQuery.append("    DECODE(secu.security_class,426,ROUND( ");
			//if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
				//sbQuery.append("			NVH.NOMINAL_VALUE														");
			//}else{
				sbQuery.append("			secu.current_nominal_value ");
			//}
			sbQuery.append("		  	,4),ROUND( ");
			//if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
				//sbQuery.append("			NVH.NOMINAL_VALUE 			");
			//}else{
				sbQuery.append("			secu.current_nominal_value ");
			//}
			sbQuery.append(" 			,2)) * PSV.TOTAL_BALANCE ");
			sbQuery.append(" 	as current_total_amount ");
			
			//sbQuery.append("			,   secu.EXPIRATION_DATE    ");
			sbQuery.append(" ,CASE WHEN secu.security_class != "+SecurityClassType.CFC.getCode()+" THEN secu.expiration_date ELSE secu.expiration_fondo_date END EXPIRATION_DATE    ");
			
			sbQuery.append(" ,NVL(CASE WHEN HA.ACCOUNT_TYPE in ( 106, 108 ) THEN 'PARTICULARES'                                                                              ");
			sbQuery.append("         WHEN HA.ACCOUNT_TYPE = 107 THEN ( select PT_SUB.DESCRIPTION                                                                            ");
			sbQuery.append("                                            from holder H_SUB                                                                                   ");
			sbQuery.append("                                            inner join HOLDER_ACCOUNT_DETAIL HAD_SUB on HAD_SUB.ID_HOLDER_FK = H_SUB.ID_HOLDER_PK               ");
			sbQuery.append("                                            inner join HOLDER_ACCOUNT HA_SUB ON HAD_SUB.ID_HOLDER_ACCOUNT_FK = HA_SUB.ID_HOLDER_ACCOUNT_PK      ");
			sbQuery.append("                                            inner join PARAMETER_TABLE PT_SUB on PT_SUB.PARAMETER_TABLE_PK = H_SUB.ECONOMIC_ACTIVITY            ");
			sbQuery.append("                                            where 1 = 1                                                                                         ");
			sbQuery.append("                                            and HA_SUB.ID_HOLDER_ACCOUNT_PK = HA.ID_HOLDER_ACCOUNT_PK)                                          ");
			sbQuery.append("        END,'POR COMPLETAR') economicActivity,                                                                                                   ");
			
			sbQuery.append("	round(((PSV.total_balance * secu.initial_nominal_value) *    ");  
			sbQuery.append("	CASE WHEN secu.CURRENCY = 127 THEN 1 /                                                                                          ");  
			sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
			sbQuery.append("						 AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)                                              ");  
			sbQuery.append("		 WHEN secu.CURRENCY IN (1734,1304) THEN 1 *                                                                                 ");  
			sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
			sbQuery.append("						AND D.ID_CURRENCY = secu.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /                                   ");  
			sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
			sbQuery.append("						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)                                               ");  
			sbQuery.append("		 WHEN secu.CURRENCY IN (430,1853) THEN 1 ELSE 1 END ),2) initialNValueUSD,                                                  ");  
			
			//if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
				//sbQuery.append("	round(((PSV.total_balance * ");  
				//sbQuery.append("			(DECODE(SECU.SECURITY_CLASS,426,ROUND( NVH.NOMINAL_VALUE ,4),        ROUND( NVH.NOMINAL_VALUE ,2))) 			");
				//sbQuery.append("	) *       ");                                                                                     
			
			//}else{
				sbQuery.append("	round(((PSV.total_balance *  DECODE(SECU.SECURITY_CLASS,426,ROUND( secu.CURRENT_NOMINAL_VALUE ,4),        ROUND( secu.CURRENT_NOMINAL_VALUE ,2))) *                                                                   ");  
			//}
			sbQuery.append("	CASE WHEN secu.CURRENCY = 127 THEN 1 /                                                                                          ");  
			sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
			sbQuery.append("						 AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)                                              ");  
			sbQuery.append("		 WHEN secu.CURRENCY IN (1734,1304) THEN 1 *                                                                                 ");  
			sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
			sbQuery.append("						AND D.ID_CURRENCY = secu.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /                                   ");  
			sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
			sbQuery.append("						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)                                               ");  
			sbQuery.append("		 WHEN secu.CURRENCY IN (430,1853) THEN 1 ELSE 1 END ),2) currentNValueUSD,                                                  ");  
			
			sbQuery.append("	round(((NVL(PSV.total_balance * 1,0)) *                                                                                              ");  
			sbQuery.append("	CASE WHEN secu.CURRENCY = 127 THEN 1 /                                                                                          ");  
			sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
			sbQuery.append("						 AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)                                              ");  
			sbQuery.append("		 WHEN secu.CURRENCY IN (1734,1304) THEN 1 *                                                                                 ");  
			sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
			sbQuery.append("						AND D.ID_CURRENCY = secu.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /                                   ");  
			sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
			sbQuery.append("						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)                                               ");  
			sbQuery.append("		 WHEN secu.CURRENCY IN (430,1853) THEN 1 ELSE 1 END ),2) marketPriceUSD                                                    ");  
			
			sbQuery.append(" 	, '"+clientPortafolioTO.getDateMarketFactString()+"' as CUT_DATE ");
			sbQuery.append(" 	,FN_GET_HOLDER_TYPE_DESC(HA.ID_HOLDER_ACCOUNT_PK) as INVESTOR_TYPE");
			
		sbQuery.append(" From BALANCE_PSY_VIEW PSV	 ");
		
		sbQuery.append(" inner join holder_Account ha   		 on PSV.id_holder_Account_pk=HA.id_holder_Account_pk			");
		sbQuery.append(" Inner join participant p 				 On PSV.id_participant_pk=p.id_participant_pk			");
		sbQuery.append(" Inner join security secu 				 On PSV.id_security_code_pk = secu.id_security_code_pk																");
		sbQuery.append(" Inner join issuer iss 					 On secu.id_issuer_fk=iss.id_issuer_pk																				");
		
//		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
//			sbQuery.append("			LEFT JOIN NOMINAL_VALUE_HISTORY_VIEW NVH ON NVH.CUT_DATE = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 		");
//			sbQuery.append("			AND secu.ID_SECURITY_CODE_PK = NVH.ID_SECURITY_CODE_FK							");
//		}
		
		sbQuery.append(" Where 1=1  																													");
		
		sbQuery.append(" AND TRUNC(PSV.CUT_DATE) = TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') ");
//		sbQuery.append(" AND PSV.CUT_DATE >= TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') ");
//		sbQuery.append(" AND PSV.CUT_DATE < TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 ");
//		sbQuery.append(" AND TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY') ");
		
		//validacion de los participantes inversionistas
		if(clientPortafolioTO.getIsParticipantInvestor()){
			sbQuery.append(" 	AND (EXISTS																						    												");
			sbQuery.append(" 		(SELECT 1																																		");
			sbQuery.append(" 		FROM HOLDER_ACCOUNT_DETAIL HAD																													");
			sbQuery.append(" 		INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK																						");
			sbQuery.append(" 		WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK																						");
			sbQuery.append(" 			AND H.DOCUMENT_TYPE = p.DOCUMENT_TYPE																										");
			sbQuery.append(" 			AND H.DOCUMENT_NUMBER = p.DOCUMENT_NUMBER																									");
			sbQuery.append(" 	))																																					");
		}
		if(Validations.validateIsNotNull(clientPortafolioTO.getIdParticipant())){
			sbQuery.append(" and PSV.id_participant_pk = :participantPk																												");
		}
		if(Validations.validateIsNotNull(clientPortafolioTO.getHolderAccountPk())){
    		sbQuery.append(" and PSV.id_holder_account_pk =:holder_account																											");
    	}
		if(Validations.validateIsNotNull(clientPortafolioTO.getIdSecurityCode())){
    		sbQuery.append(" and PSV.id_security_code_pk = :securityPk																												");
    	}
		
		if(clientPortafolioTO.getShowAvailableBalance().equals(Integer.valueOf("0"))){			
			sbQuery.append(" and PSV.TOTAL_BALANCE > 0																														");
		}
		if(Validations.validateIsNotNull(clientPortafolioTO.getCui())){
			sbQuery.append(" AND (EXISTS																																			");
			sbQuery.append(" 	(SELECT had.ID_HOLDER_FK																															");
			sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL had																														");
			sbQuery.append(" 	WHERE had.ID_HOLDER_FK = :cui_code																													");
			sbQuery.append(" 		AND had.ID_HOLDER_ACCOUNT_FK = PSV.id_holder_account_pk))																						");
	    }
		if(Validations.validateIsNotNull(clientPortafolioTO.getSecurityClass())){
    		sbQuery.append(" and secu.security_class in (:security_class)");
    	}
		if(Validations.validateIsNotNull(clientPortafolioTO.getIdIssuer())){
    		sbQuery.append(" and iss.id_issuer_pk=:issuerPk");
    	}
		if(Validations.validateIsNotNull(clientPortafolioTO.getIdCurrency())){
    		sbQuery.append(" and secu.currency=:currency");
    	}
		sbQuery.append(" ) T Order by mnemonic, security_type, currency_mnemonic, security_class_mnemonic ,id_security_code_pk, Holders  ");

		String strQueryFormatted = sbQuery.toString();
		
		if(clientPortafolioTO.getIdParticipant()!=null){
    		strQueryFormatted= strQueryFormatted.replace(":participantPk", clientPortafolioTO.getIdParticipant().toString());
    	}
    	if(clientPortafolioTO.getHolderAccountPk()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":holder_account", clientPortafolioTO.getHolderAccountPk().toString());
    	}
    	if(clientPortafolioTO.getIdSecurityCode()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":securityPk", "'"+clientPortafolioTO.getIdSecurityCode()+"'");
    	}
    	if(Validations.validateIsNotNullAndPositive(clientPortafolioTO.getCui())){
    		strQueryFormatted=strQueryFormatted.replace(":cui_code", clientPortafolioTO.getCui().toString());
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(clientPortafolioTO.getSecurityClass())){
    		strQueryFormatted=strQueryFormatted.replace(":security_class", clientPortafolioTO.getSecurityClass());
    	}
    	if(clientPortafolioTO.getIdIssuer()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":issuerPk", "'"+clientPortafolioTO.getIdIssuer()+"'");
    	}
    	if(clientPortafolioTO.getIdCurrency()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":currency", clientPortafolioTO.getIdCurrency().toString());
    	}
    	return strQueryFormatted;
    }
    
    /* cartera fisica*/
    public String getSubTotalQueryClientPortafolio(ClientPortafolioTO clientPortafolioTO){
    	StringBuilder sbQuery= new StringBuilder();

		sbQuery.append(" SELECT 																															");
		sbQuery.append(" 	(SELECT PARAMETER_NAME FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = S.CURRENCY) CURRENCY,									");   
		sbQuery.append(" 	NVL(SUM(PSV.TOTAL_BALANCE),0) TOTAL_VALORES,																							");   
		sbQuery.append(" 	NVL(SUM(																														");    
		sbQuery.append(" 		S.INITIAL_NOMINAL_VALUE * PSV.TOTAL_BALANCE *																		");      
		sbQuery.append(" 		CASE																														");     
		sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1																							");     
		sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304,430) THEN 1 *																				"); 
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1)										");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1853) THEN 1 *																						"); 
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)												");
		sbQuery.append(" 			ELSE 1																													");   
		sbQuery.append(" 	END),0) TOTAL_BOB,																												");    
		sbQuery.append("	NVL(SUM(																														"); 
		sbQuery.append(" 		S.INITIAL_NOMINAL_VALUE * PSV.TOTAL_BALANCE *																		");
		sbQuery.append(" 		CASE																														");
		sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1 /																							");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)												");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304) THEN 1 *																					");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /										");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)												");
		sbQuery.append(" 			WHEN S.CURRENCY IN (430,1853) THEN 1																					");
		sbQuery.append(" 			ELSE 1																													");
		sbQuery.append(" 	END),0) TOTAL_USD,																												");
		
		sbQuery.append(" 	NVL(SUM(	S.CURRENT_NOMINAL_VALUE * PSV.TOTAL_BALANCE *																		");
		sbQuery.append(" 		CASE																														");
		sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1																							");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304,430) THEN 1 *																				");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1)										");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1853) THEN 1 *																						");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)												");
		sbQuery.append(" 			ELSE 1																													");
		sbQuery.append(" 		END),0) TOTAL_PRECIO_MRKT_BOB,																								");
		sbQuery.append(" 	NVL(SUM(	S.CURRENT_NOMINAL_VALUE * PSV.TOTAL_BALANCE *																		");
		sbQuery.append(" 		CASE																														");
		sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1 /																							");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)												");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304) THEN 1 *																					");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /										");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 					AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)													");
		sbQuery.append(" 			WHEN S.CURRENCY IN (430,1853) THEN 1																					");
		sbQuery.append(" 			ELSE 1																													");
		sbQuery.append(" 		END),0) TOTAL_PRECIO_MRKT_USD,																								");
		
		sbQuery.append(" 	NVL(SUM(																														");
		sbQuery.append(" 		DECODE(s.security_class,426,ROUND(s.current_nominal_value,4),ROUND(s.current_nominal_value,2)) * PSV.TOTAL_BALANCE *	");
		sbQuery.append(" 		CASE																														");
		sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1																							");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304,430) THEN 1 *																				");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1)										");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1853) THEN 1 *																						");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)												");
		sbQuery.append(" 			ELSE 1																													");
		sbQuery.append(" 		END),0) TOTAL_CURRENT_NOMINAL_BOB,																							");
		sbQuery.append(" 	NVL(SUM(																														");
		sbQuery.append(" 		DECODE(s.security_class,426,ROUND(s.current_nominal_value,4),ROUND(s.current_nominal_value,2)) * PSV.TOTAL_BALANCE *	");
		sbQuery.append(" 		CASE																														");
		sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1 /																							");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)												");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304) THEN 1 *																					");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /										");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 					AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)													");
		sbQuery.append(" 			WHEN S.CURRENCY IN (430,1853) THEN 1																					");
		sbQuery.append(" 			ELSE 1																													");
		sbQuery.append(" 		END),0) TOTAL_CURRENT_NOMINAL_USD																							");
		
		sbQuery.append(" FROM BALANCE_PSY_VIEW	PSV																								");
		sbQuery.append(" INNER JOIN SECURITY S                   ON S.ID_SECURITY_CODE_PK = PSV.ID_SECURITY_CODE_PK											");
		sbQuery.append(" INNER JOIN PARTICIPANT P                ON P.ID_PARTICIPANT_PK = PSV.ID_PARTICIPANT_PK												");
		sbQuery.append(" WHERE S.ISSUANCE_FORM = 400 ");

		sbQuery.append(" AND trunc(PSV.CUT_DATE ) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')	");
		
//		String currentDateString = CommonsUtilities.convertDateToString(CommonsUtilities.currentDate(), CommonsUtilities.DATE_PATTERN);
//		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
//			sbQuery.append(" 	AND PBD.DEPOSIT_DATE <= TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')							");
//			sbQuery.append(" 	AND TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') < 												");
//			sbQuery.append(" 		DECODE(PBD.RETIREMENT_DATE,NULL,TO_DATE('"+currentDateString+"','DD/MM/YYYY'),PBD.RETIREMENT_DATE)						");
//		}else{
//			//certificado
//			sbQuery.append(" 	AND PBD.STATE = 605																											");
//		}
		//validacion de los participantes inversionistas
		if(clientPortafolioTO.getIsParticipantInvestor()){
			sbQuery.append(" 	AND (EXISTS																						    						");
			sbQuery.append(" 		(SELECT 1																												");
			sbQuery.append(" 		FROM HOLDER_ACCOUNT_DETAIL HAD																							");
			sbQuery.append(" 		INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK																");
			sbQuery.append(" 		WHERE HAD.ID_HOLDER_ACCOUNT_FK = PSV.ID_HOLDER_ACCOUNT_FPK																");
			sbQuery.append(" 			AND H.DOCUMENT_TYPE = P.DOCUMENT_TYPE																				");
			sbQuery.append(" 			AND H.DOCUMENT_NUMBER = P.DOCUMENT_NUMBER																			");
			sbQuery.append(" 	))																															");
		}

		if(Validations.validateIsNotNull(clientPortafolioTO.getIdParticipant())){
			sbQuery.append(" 	AND PSV.ID_PARTICIPANT_pK =:participantPk																					");
		}
		if(Validations.validateIsNotNull(clientPortafolioTO.getHolderAccountPk())){
    		sbQuery.append(" 	AND PSV.ID_HOLDER_ACCOUNT_PK =:holder_account																				");
    	}
		if(Validations.validateIsNotNull(clientPortafolioTO.getIdSecurityCode())){
    		sbQuery.append(" 	AND PSV.ID_SECURITY_CODE_PK =:securityPk																						");
    	}
		
		if(clientPortafolioTO.getShowAvailableBalance().equals(Integer.valueOf("0"))){
			sbQuery.append(" 	AND PSV.TOTAL_BALANCE > 0																							");
		}
		if(Validations.validateIsNotNull(clientPortafolioTO.getCui())){
			sbQuery.append(" 	AND (EXISTS																													");
			sbQuery.append(" 		(SELECT had.ID_HOLDER_FK																								");
			sbQuery.append(" 		FROM HOLDER_ACCOUNT_DETAIL had																							");
			sbQuery.append(" 		WHERE had.ID_HOLDER_FK =:cui_code																						");
			sbQuery.append(" 			AND had.ID_HOLDER_ACCOUNT_FK = PSV.ID_HOLDER_ACCOUNT_PK))															");
	    }
		if(Validations.validateIsNotNull(clientPortafolioTO.getSecurityClass())){
    		sbQuery.append(" 	AND S.SECURITY_CLASS in (:security_class)																					");
    	}
		if(Validations.validateIsNotNull(clientPortafolioTO.getIdIssuer())){
    		sbQuery.append(" 	AND S.ID_ISSUER_FK =:issuerPk																								");
    	}
		if(Validations.validateIsNotNull(clientPortafolioTO.getIdCurrency())){
    		sbQuery.append(" 	AND S.CURRENCY =:currency																									");
    	}
		
		sbQuery.append(" GROUP BY S.CURRENCY																												");
		sbQuery.append(" ORDER BY S.CURRENCY																												");
		
		String strQueryFormatted = sbQuery.toString();
    	
    	if(clientPortafolioTO.getIdParticipant()!=null){
    		strQueryFormatted= strQueryFormatted.replace(":participantPk", clientPortafolioTO.getIdParticipant().toString());
    	}
    	if(clientPortafolioTO.getHolderAccountPk()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":holder_account", clientPortafolioTO.getHolderAccountPk().toString());
    	}
    	if(clientPortafolioTO.getIdSecurityCode()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":securityPk", "'"+clientPortafolioTO.getIdSecurityCode()+"'");
    	}
    	
    	if(Validations.validateIsNotNullAndPositive(clientPortafolioTO.getCui())){
    		strQueryFormatted=strQueryFormatted.replace(":cui_code", clientPortafolioTO.getCui().toString());
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(clientPortafolioTO.getSecurityClass())){
    		strQueryFormatted=strQueryFormatted.replace(":security_class", clientPortafolioTO.getSecurityClass());
    	}
    	if(clientPortafolioTO.getIdIssuer()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":issuerPk", "'"+clientPortafolioTO.getIdIssuer()+"'");
    	}
    	if(clientPortafolioTO.getIdCurrency()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":currency", clientPortafolioTO.getIdCurrency().toString());
    	}
    	
    	return strQueryFormatted;
    }
    
	public Long searchIdStockCalculationProcess(Date cutDate)
			throws ServiceException {
		StringBuilder stringBuilderSql = new StringBuilder();
		Integer stockType = StockType.MONTHLY.getCode();
		stringBuilderSql.append("	SELECT SCP.ID_STOCK_CALCULATION_PK																				");
		stringBuilderSql.append("	FROM STOCK_CALCULATION_PROCESS SCP																				");
		stringBuilderSql.append("	WHERE SCP.STOCK_TYPE = '" + stockType + "'																		");
		stringBuilderSql.append("		AND TRUNC(SCP.CUTOFF_DATE) = :cutDate																		");
		stringBuilderSql.append("		AND (																										");
		stringBuilderSql.append("					SELECT count(*) 																				");
		stringBuilderSql.append("				FROM STOCK_CALCULATION_MARKETFACT SCM  	");
		stringBuilderSql.append("					WHERE SCM.ID_STOCK_CALCULATION_FK = SCP.ID_STOCK_CALCULATION_PK )	 > 0						");

		Query query = em.createNativeQuery(stringBuilderSql.toString());
		query.setParameter("cutDate", cutDate);

		List<Object> list = query.getResultList();
		Long idStockCalculationProcess = null;

		if (list != null && !list.isEmpty()) {
			Object obj = (Object) list.get(0);
			idStockCalculationProcess = new Long(obj.toString());
		} else {
			throw new ServiceException(
					ErrorServiceType.STOCK_CALCULATION_PROCESS_NOT_EXIST);
		}
		return idStockCalculationProcess;
	}

	/*
	 * public List<ClientPortafolioTO>
	 * getSubProductsOfIssuancesWithNegotiableCupons(ClientPortafolioTO
	 * subproductsTO, String strQuery){ StringBuilder sbQuery= new
	 * StringBuilder();
	 * 
	 * sbQuery.append("	SELECT ");
	 * sbQuery.append("         'EDB' AS CUSTODIO, "); sbQuery.append(
	 * "         trunc(to_date(:date,'dd/MM/yyyy')) as FECHA_INFORMACION, ");
	 * sbQuery.append("         S.ID_SECURITY_CODE_PK, "); sbQuery.append(
	 * "         (SELECT LISTAGG(PIC.COUPON_NUMBER, ';') WITHIN GROUP (ORDER BY PIC.COUPON_NUMBER) "
	 * ); sbQuery.append("         FROM PROGRAM_INTEREST_COUPON PIC ");
	 * sbQuery.append(
	 * "         INNER JOIN INTEREST_PAYMENT_SCHEDULE IPS  ON IPS.ID_INT_PAYMENT_SCHEDULE_PK = PIC.ID_INT_PAYMENT_SCHEDULE_FK "
	 * ); sbQuery.append(
	 * "         INNER JOIN SECURITY SR                    ON SR.ID_SECURITY_CODE_PK = IPS.ID_SECURITY_CODE_FK "
	 * ); sbQuery.append("         WHERE pic.STATE_PROGRAM_INTEREST = 1350 ");
	 * sbQuery
	 * .append("         AND SR.ID_SECURITY_CODE_PK = S.ID_REF_SECURITY_CODE_FK "
	 * ); sbQuery.append("         AND PIC.COUPON_NUMBER NOT IN( ");
	 * sbQuery.append("         SELECT PIC.COUPON_NUMBER ");
	 * sbQuery.append("         FROM PROGRAM_INTEREST_COUPON PIC ");
	 * sbQuery.append(
	 * "         INNER JOIN INTEREST_PAYMENT_SCHEDULE IPS  ON IPS.ID_INT_PAYMENT_SCHEDULE_PK = PIC.ID_INT_PAYMENT_SCHEDULE_FK "
	 * ); sbQuery.append(
	 * "             INNER JOIN SECURITY SR                    ON SR.ID_SECURITY_CODE_PK = IPS.ID_SECURITY_CODE_FK "
	 * );
	 * sbQuery.append("             WHERE pic.STATE_PROGRAM_INTEREST = 1350 ");
	 * sbQuery
	 * .append("         AND SR.ID_SECURITY_CODE_PK = S.ID_SECURITY_CODE_PK ");
	 * sbQuery.append("         )) CUPONES_DESPRENDIDOS, "); sbQuery.append(
	 * "             (SELECT LISTAGG(PIC.COUPON_NUMBER, ';') WITHIN GROUP (ORDER BY PIC.COUPON_NUMBER) FROM PROGRAM_INTEREST_COUPON PIC "
	 * ); sbQuery.append(
	 * "             INNER JOIN INTEREST_PAYMENT_SCHEDULE IPS ON IPS.ID_INT_PAYMENT_SCHEDULE_PK = PIC.ID_INT_PAYMENT_SCHEDULE_FK "
	 * );
	 * sbQuery.append("             WHERE PIC.STATE_PROGRAM_INTEREST = 1350 ");
	 * sbQuery
	 * .append("         AND IPS.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK ");
	 * sbQuery.append("         ) CUPONES_NO_DESPRENDIDOS ");
	 * sbQuery.append("         FROM SECURITY S ");
	 * sbQuery.append("             WHERE 1= 1 AND s.IND_SPLIT_COUPON = 1 ");
	 * sbQuery.append("             AND S.IND_IS_DETACHED = 1 ");
	 * if(Validations.validateIsNotNull(subproductsTO.getIdSecurityCode())){
	 * sbQuery.append(" and s.ID_SECURITY_CODE_PK = (:security_pk) "); }
	 * sbQuery.
	 * append(" and (trunc(s.registry_date)=to_date(:registry_date, 'dd/MM/yyyy') "
	 * ); sbQuery.append("     ORDER BY ");
	 * sbQuery.append("         s.ID_SECURITY_CODE_PK ");
	 * 
	 * Query query = em.createNativeQuery(sbQuery.toString());
	 * 
	 * query.setParameter("date",
	 * CommonsUtilities.convertDatetoString(subproductsTO.getDate()));
	 * query.setParameter("registry_date",
	 * CommonsUtilities.convertDatetoString(subproductsTO.getRegistryDate()));
	 * query.setParameter("security_pk", subproductsTO.getIdSecurityCode());
	 * 
	 * List<Object[]> lstObjects = query.getResultList();
	 * 
	 * ClientPortafolioTO cc= null; List<ClientPortafolioTO> lstCC = new
	 * ArrayList<ClientPortafolioTO>(); for (int i = 0; i < lstObjects.size();
	 * i++) { cc= new ClientPortafolioTO(); Object[] objList = (Object[])
	 * lstObjects.get(i); if(Validations.validateIsNotNull(objList[0])){
	 * cc.setCustodian((objList[0]).toString()); }
	 * if(Validations.validateIsNotNull(objList[1])){
	 * cc.setDateinformation((Date)objList[1]); }
	 * if(Validations.validateIsNotNull(objList[2])){
	 * cc.setIdSecurityCode(objList[2].toString()); }
	 * if(Validations.validateIsNotNull(objList[3])){
	 * cc.setDetached_coupons(objList[3].toString()); }
	 * if(Validations.validateIsNotNull(objList[4])){
	 * cc.setCoupons_no_detached(objList[4].toString()); } lstCC.add(cc); }
	 * return lstCC;
	 * 
	 * }
	 */

	public List<ClientPortafolioTO> getSubProductsOfIssueancesTxt(
			ClientPortafolioTO subproductsTO, String sbQuery) {

		Query query = em.createNativeQuery(sbQuery.toString());

		query.setParameter("registry_date", CommonsUtilities
				.convertDatetoString(subproductsTO.getRegistryDate()));

		if (subproductsTO.getIdSecurityCode() != null) {
			query.setParameter("security_pk", subproductsTO.getIdSecurityCode());
		}

		List<Object[]> lstObjects = query.getResultList();

		ClientPortafolioTO cc = null;
		List<ClientPortafolioTO> lstCC = new ArrayList<ClientPortafolioTO>();
		for (int i = 0; i < lstObjects.size(); i++) {
			cc = new ClientPortafolioTO();
			Object[] objList = (Object[]) lstObjects.get(i);
			if (Validations.validateIsNotNull(objList[0])) {
				cc.setCustodian((objList[0]).toString());
			}
			if (Validations.validateIsNotNull(objList[1])) {
				cc.setDateInformation(objList[1].toString());
			}
			if (Validations.validateIsNotNull(objList[2])) {
				cc.setSecurityClass(objList[2].toString());
			}
			if (Validations.validateIsNotNull(objList[3])) {
				cc.setValuatorCode(objList[3].toString());
			}
			if (Validations.validateIsNotNull(objList[4])) {
				cc.setDetached_coupons(objList[4].toString());
			}
			if (Validations.validateIsNotNull(objList[5])) {
				cc.setCoupons_no_detached(objList[5].toString());
			}
			lstCC.add(cc);
		}
		return lstCC;

	}

	public String getSubProductsOfIssuancesWithNegotiableCuponsJasper(
			ClientPortafolioTO subproductsTO) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append("	SELECT ");
		sbQuery.append("         'EDB' AS CUSTODIO, ");
		sbQuery.append("         to_char(CURRENT_DATE,'yyyy-mm-dd') as FECHA_INFORMACION, ");
		sbQuery.append("         (SELECT PT.TEXT1 FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK=S.SECURITY_CLASS) CLASE_VALOR, ");
		sbQuery.append("         S.ID_SECURITY_CODE_ONLY, ");
		sbQuery.append("         (SELECT LISTAGG(PIC.COUPON_NUMBER, ';') WITHIN GROUP (ORDER BY PIC.COUPON_NUMBER) ");
		sbQuery.append("         FROM PROGRAM_INTEREST_COUPON PIC ");
		sbQuery.append("         INNER JOIN INTEREST_PAYMENT_SCHEDULE IPS  ON IPS.ID_INT_PAYMENT_SCHEDULE_PK = PIC.ID_INT_PAYMENT_SCHEDULE_FK ");
		sbQuery.append("         INNER JOIN SECURITY SR                    ON SR.ID_SECURITY_CODE_PK = IPS.ID_SECURITY_CODE_FK ");
		sbQuery.append("         WHERE pic.STATE_PROGRAM_INTEREST = 1350 ");
		sbQuery.append("         AND SR.ID_SECURITY_CODE_PK = S.ID_REF_SECURITY_CODE_FK ");
		sbQuery.append("         AND PIC.COUPON_NUMBER NOT IN( ");
		sbQuery.append("         SELECT PIC.COUPON_NUMBER ");
		sbQuery.append("         FROM PROGRAM_INTEREST_COUPON PIC ");
		sbQuery.append("         INNER JOIN INTEREST_PAYMENT_SCHEDULE IPS  ON IPS.ID_INT_PAYMENT_SCHEDULE_PK = PIC.ID_INT_PAYMENT_SCHEDULE_FK ");
		sbQuery.append("             INNER JOIN SECURITY SR                    ON SR.ID_SECURITY_CODE_PK = IPS.ID_SECURITY_CODE_FK ");
		sbQuery.append("             WHERE pic.STATE_PROGRAM_INTEREST = 1350 ");
		sbQuery.append("         AND SR.ID_SECURITY_CODE_PK = S.ID_SECURITY_CODE_PK ");
		sbQuery.append("         )) CUPONES_DESPRENDIDOS, ");
		sbQuery.append("             (SELECT LISTAGG(PIC.COUPON_NUMBER, ';') WITHIN GROUP (ORDER BY PIC.COUPON_NUMBER) FROM PROGRAM_INTEREST_COUPON PIC ");
		sbQuery.append("             INNER JOIN INTEREST_PAYMENT_SCHEDULE IPS ON IPS.ID_INT_PAYMENT_SCHEDULE_PK = PIC.ID_INT_PAYMENT_SCHEDULE_FK ");
		sbQuery.append("             WHERE PIC.STATE_PROGRAM_INTEREST = 1350 ");
		sbQuery.append("         AND IPS.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK ");
		sbQuery.append("         ) CUPONES_NO_DESPRENDIDOS ");
		sbQuery.append("         FROM SECURITY S ");
		sbQuery.append("             WHERE 1= 1 AND s.IND_SPLIT_COUPON = 1 ");
		sbQuery.append("             AND S.IND_IS_DETACHED = 1 ");
		if (Validations.validateIsNotNull(subproductsTO.getIdSecurityCode())) {
			sbQuery.append(" and s.ID_SECURITY_CODE_PK = (:security_pk) ");
		}
		sbQuery.append(" and (trunc(s.registry_date)=to_date(:registry_date, 'dd/MM/yyyy')) ");
		sbQuery.append("     ORDER BY ");
		sbQuery.append("         s.ID_SECURITY_CODE_PK ");
		sbQuery.append("  ");

		return sbQuery.toString();
	}

	public String getQueryForJasperSubProducts(
			ClientPortafolioTO subproductsTO, String sbQuery) {
		String strQueryFormatted = sbQuery.toString();

		if (subproductsTO.getIdSecurityCode() != null) {
			strQueryFormatted = strQueryFormatted.replace(":security_pk", "'"+ subproductsTO.getIdSecurityCode() + "'");
		}
		strQueryFormatted = strQueryFormatted.replace(":registry_date",	"'"	+ CommonsUtilities.convertDateToString(subproductsTO.getRegistryDate(), "dd/MM/yyyy")+ "'");
		return strQueryFormatted;
	}

	public BigDecimal getExchangeRate(String date) throws ServiceException {
		DailyExchangeRates exchangeRate = new DailyExchangeRates();
		exchangeRate.setIdCurrency(CurrencyType.USD.getCode());
		exchangeRate.setDateRate(CommonsUtilities.convertStringtoDate(date));
		return accountReportService.getExchangeRateByCurrencyType(exchangeRate);
	}
	
	public String getOverdueTitles(GenericCustodyOperationTO filter) {

		StringBuilder sbQuery = new StringBuilder();

		//CORTE PARA LOS CUPONES
//		sbQuery.append(" Select " ); 
//  		sbQuery.append("   'CUPONES' TITULO, " );
//		sbQuery.append("   p.id_participant_pk, " );
//  		sbQuery.append("   p.mnemonic, " );
//  		sbQuery.append("   p.description, " );
//  		sbQuery.append("   HA.account_number, " );
//  		sbQuery.append("   s.CURRENCY, " );
//  		sbQuery.append("   to_char(s.ISSUANCE_DATE,'dd/mm/yyyy') ISSUANCE_DATE, " );
//  		sbQuery.append("   to_char(pic.EXPERITATION_DATE, 'dd/mm/yyyy') EXPIRATION_DATE, " );
//  		sbQuery.append("   ips.ID_SECURITY_CODE_FK, " );
//  		sbQuery.append("   (pic.COUPON_NUMBER) " );
//  		sbQuery.append("   || '/' || " );
//  		sbQuery.append("   nvl( " );
//  		sbQuery.append("     (SELECT MAX(I_CPU.COUPON_NUMBER) MAX " );
//  		sbQuery.append("     FROM INTEREST_PAYMENT_SCHEDULE SCHE_I " );
//  		sbQuery.append("         LEFT JOIN PROGRAM_INTEREST_COUPON I_CPU       ON I_CPU.ID_INT_PAYMENT_SCHEDULE_FK = sche_I.ID_INT_PAYMENT_SCHEDULE_PK " );
//  		sbQuery.append("     WHERE SCHE_I.id_security_code_fk = S.ID_SECURITY_CODE_PK " );
//  		sbQuery.append("     ) " );
//  		sbQuery.append("   ,0) AS TOTAL_CUP, " );
//  		sbQuery.append("   (CASE " ); 
//  		sbQuery.append("       WHEN TRUNC(pic.experitation_date) < SYSDATE " );
//  		sbQuery.append("           THEN (SELECT SUM(MFV.TOTAL_BALANCE) " ); 
//  		sbQuery.append("                 FROM MARKET_FACT_VIEW MFV " );
//  		sbQuery.append("                 WHERE 1 = 1 " );
//  		sbQuery.append("                       AND TRUNC(MFV.CUT_DATE) = TRUNC(pic.experitation_date) - 1 " ); 
//  		sbQuery.append("                       AND MFV.ID_PARTICIPANT_PK = HA.ID_PARTICIPANT_FK " );
//  		sbQuery.append("                       AND MFV.ID_HOLDER_ACCOUNT_PK = HA.ID_HOLDER_ACCOUNT_PK " );
//  		sbQuery.append("                       AND MFV.ID_SECURITY_CODE_PK = s.id_security_code_pk " );
//  		sbQuery.append("                 GROUP BY MFV.ID_SECURITY_CODE_PK,MFV.ID_PARTICIPANT_PK,MFV.ID_HOLDER_ACCOUNT_PK) " );
//  		sbQuery.append("       ELSE " );
//  		sbQuery.append("           hab.TOTAL_BALANCE " );
//  		sbQuery.append("   END) TOTAL_BALANCE, " );
//  		sbQuery.append("   ROUND(NVL(pic.COUPON_AMOUNT,0),2) COUPON_AMOUNT_INT, " );
//  		sbQuery.append("   0 COUPON_AMOUNT_AMO, " );
//  		sbQuery.append("   ROUND(NVL(pic.COUPON_AMOUNT,0),2) + " );
//  		sbQuery.append("   0 TOTAL_GENERAL, " );
//  		sbQuery.append("   (select pt.text1 from parameter_table pt where pt.parameter_table_pk=s.currency) currency_mnemonic, " );
//  		sbQuery.append("   (select pt.parameter_name from parameter_table pt where pt.parameter_table_pk=s.currency) currency_name, " );
//  		sbQuery.append("   HA.ID_HOLDER_ACCOUNT_PK, " );
//  		sbQuery.append("   (SELECT LISTAGG(HAD.ID_HOLDER_FK,'<br>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) " );
//  		sbQuery.append("       FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) CUI, " );
//  		sbQuery.append("   (SELECT LISTAGG(H.FULL_NAME,'<br>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) " );
//  		sbQuery.append("       FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK " );
//  		sbQuery.append("       WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) CUI_DESCRIPTION " );
//  		sbQuery.append(" FROM SECURITY s " );
//  		sbQuery.append("   INNER JOIN HOLDER_ACCOUNT_BALANCE hab         ON hab.ID_SECURITY_CODE_PK = s.ID_SECURITY_CODE_PK " );
//  		sbQuery.append("   INNER JOIN HOLDER_ACCOUNT ha                  ON hab.ID_HOLDER_ACCOUNT_PK = HA.ID_HOLDER_ACCOUNT_PK " );
//  		sbQuery.append("   INNER JOIN PARTICIPANT p                      ON HAB.id_participant_Pk = p.id_participant_pk " );
//  		sbQuery.append("   INNER JOIN INTEREST_PAYMENT_SCHEDULE IPS      ON IPS.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK " );
//  		sbQuery.append("   INNER JOIN PROGRAM_INTEREST_COUPON PIC      	 ON PIC.ID_INT_PAYMENT_SCHEDULE_FK = IPS.ID_INT_PAYMENT_SCHEDULE_PK " );
//  		sbQuery.append("   LEFT JOIN AMORTIZATION_PAYMENT_SCHEDULE APS   ON APS.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK " );
//  		sbQuery.append("   LEFT JOIN PROGRAM_AMORTIZATION_COUPON PAC     ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK " );
//  		sbQuery.append(" WHERE  1 = 1 AND" );
//		sbQuery.append("   ( " );
//		sbQuery.append("              (SELECT " );
//		sbQuery.append("                  (select COUNT(1) " );
//		sbQuery.append("                  FROM INTEREST_PAYMENT_SCHEDULE SCHE_I " );
//		sbQuery.append("                  left JOIN PROGRAM_INTEREST_COUPON I_CPU ON I_CPU.ID_INT_PAYMENT_SCHEDULE_FK = SCHE_I.ID_INT_PAYMENT_SCHEDULE_PK " );
//		sbQuery.append("              WHERE SCHE_I.id_security_code_fk = SEC.ID_SECURITY_CODE_PK) INT " );
//		sbQuery.append("              FROM SECURITY SEC " );
//		sbQuery.append("              WHERE SEC.INSTRUMENT_TYPE = 124 " );
//		sbQuery.append("              AND SEC.ID_SECURITY_CODE_PK = S.ID_SECURITY_CODE_PK " );
//		sbQuery.append("              ) > 0 " );
//		sbQuery.append("              AND " );
//		sbQuery.append("              (((SELECT I_CPU.COUPON_NUMBER CUP_MAX " );
//		sbQuery.append("              FROM INTEREST_PAYMENT_SCHEDULE SCHE_I " );
//		sbQuery.append("              LEFT JOIN PROGRAM_INTEREST_COUPON I_CPU       ON I_CPU.ID_INT_PAYMENT_SCHEDULE_FK = sche_I.ID_INT_PAYMENT_SCHEDULE_PK " );
//		sbQuery.append("              WHERE SCHE_I.id_security_code_fk = S.ID_SECURITY_CODE_PK " );
//		sbQuery.append("              AND TRUNC(I_CPU.EXPERITATION_DATE) = trunc(PIC.EXPERITATION_DATE) " );
//		sbQuery.append("              ) " );
//		sbQuery.append("              < " );
//		sbQuery.append("              (SELECT " );
//		sbQuery.append("                  (SELECT MAX(I_CPU.COUPON_NUMBER) " );
//		sbQuery.append("                      FROM INTEREST_PAYMENT_SCHEDULE SCHE_I " );
//		sbQuery.append("                      LEFT JOIN PROGRAM_INTEREST_COUPON I_CPU   ON I_CPU.ID_INT_PAYMENT_SCHEDULE_FK = sche_I.ID_INT_PAYMENT_SCHEDULE_PK " );
//		sbQuery.append("                      WHERE SCHE_I.id_security_code_fk = SEC.ID_SECURITY_CODE_PK " );
//		sbQuery.append("                  ) TOT_INT " );
//		sbQuery.append("                  FROM SECURITY SEC " );
//		sbQuery.append("                  WHERE SEC.INSTRUMENT_TYPE = 124 " );
//		sbQuery.append("                  AND SEC.ID_SECURITY_CODE_PK = S.ID_SECURITY_CODE_PK)) OR (S.IND_IS_DETACHED = 1 AND S.EXPIRATION_DATE <> PIC.EXPERITATION_DATE)) " );
//		sbQuery.append("          ) " );
//  		sbQuery.append("   AND pic.COUPON_AMOUNT > 0 " );
//  		sbQuery.append("   AND trunc(pic.EXPERITATION_DATE) >= to_date('"+ filter.getInitialDate() +"','DD/MM/YYYY') " );
//  		sbQuery.append("   AND trunc(pic.EXPERITATION_DATE) <= to_date('"+ filter.getFinalDate() +"','dd/MM/YYYY') " );
		
		sbQuery.append(" Select   ");
		sbQuery.append("    'CUPONES' TITULO,  ");
		sbQuery.append("    p.id_participant_pk,  ");
		sbQuery.append("    p.mnemonic,  ");
		sbQuery.append("    p.description,  ");
		sbQuery.append("    HA.account_number,  ");
		sbQuery.append("    s.CURRENCY,  ");
		sbQuery.append("    to_char(s.ISSUANCE_DATE,'dd/mm/yyyy') ISSUANCE_DATE,  ");
		sbQuery.append("    to_char(pic.EXPERITATION_DATE, 'dd/mm/yyyy') EXPIRATION_DATE,  ");
		sbQuery.append("    ips.ID_SECURITY_CODE_FK,  ");
		sbQuery.append("    (pic.COUPON_NUMBER)  ");
		sbQuery.append("    || '/' ||  ");
		sbQuery.append("    nvl(  ");
		sbQuery.append("      (SELECT MAX(I_CPU.COUPON_NUMBER) MAX  ");
		sbQuery.append("      FROM INTEREST_PAYMENT_SCHEDULE SCHE_I  ");
		sbQuery.append("          LEFT JOIN PROGRAM_INTEREST_COUPON I_CPU       ON I_CPU.ID_INT_PAYMENT_SCHEDULE_FK = sche_I.ID_INT_PAYMENT_SCHEDULE_PK  ");
		sbQuery.append("      WHERE SCHE_I.id_security_code_fk = S.ID_SECURITY_CODE_PK  ");
		sbQuery.append("      )  ");
		sbQuery.append("    ,0) AS TOTAL_CUP,  ");
		sbQuery.append("    HAB.TOTAL_BALANCE TOTAL_BALANCE,  ");
		sbQuery.append("    HAB.AVAILABLE_BALANCE AVAILABLE_BALANCE,  ");
		sbQuery.append("    HAB.PAWN_BALANCE PAWN_BALANCE,  ");
		sbQuery.append("    HAB.BAN_BALANCE BAN_BALANCE,  ");
		sbQuery.append("    HAB.OTHER_BLOCK_BALANCE OTHER_BLOCK_BALANCE,  ");
		sbQuery.append("    ROUND(NVL(pic.COUPON_AMOUNT,0),2) COUPON_AMOUNT,  ");
		sbQuery.append("    ROUND(NVL(pic.COUPON_AMOUNT,0),2) COUPON_AMOUNT_INT,  ");
		sbQuery.append("    0 COUPON_AMOUNT_AMO,  ");
		sbQuery.append("    ROUND(NVL(pic.COUPON_AMOUNT,0),2) +  ");
		sbQuery.append("    0 TOTAL_GENERAL,  ");
		sbQuery.append("    (select pt.text1 from parameter_table pt where pt.parameter_table_pk=s.currency) currency_mnemonic,  ");
		sbQuery.append("    (select pt.parameter_name from parameter_table pt where pt.parameter_table_pk=s.currency) currency_name,  ");
		sbQuery.append("    HA.ID_HOLDER_ACCOUNT_PK,  ");
		sbQuery.append("    (SELECT LISTAGG(HAD.ID_HOLDER_FK,'<br>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)  ");
		sbQuery.append("        FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) CUI,  ");
		sbQuery.append("    (SELECT LISTAGG(H.FULL_NAME,'<br>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)  ");
		sbQuery.append("        FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK  ");
		sbQuery.append("        WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) CUI_DESCRIPTION  ");
		sbQuery.append("  FROM ACCOUNT_BALANCE_VIEW hab ");
		sbQuery.append("    INNER JOIN SECURITY s                         ON s.ID_SECURITY_CODE_PK  = hab.ID_SECURITY_CODE_PK ");
		sbQuery.append("    INNER JOIN HOLDER_ACCOUNT ha                  ON hab.ID_HOLDER_ACCOUNT_PK = HA.ID_HOLDER_ACCOUNT_PK  ");
		sbQuery.append("    INNER JOIN PARTICIPANT p                      ON HAB.id_participant_Pk = p.id_participant_pk  ");
		sbQuery.append("    INNER JOIN INTEREST_PAYMENT_SCHEDULE IPS      ON IPS.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK  ");
		sbQuery.append("    INNER JOIN PROGRAM_INTEREST_COUPON PIC        ON PIC.ID_INT_PAYMENT_SCHEDULE_FK = IPS.ID_INT_PAYMENT_SCHEDULE_PK  ");
		sbQuery.append("  WHERE  1 = 1 ");
  		sbQuery.append("   AND trunc(pic.EXPERITATION_DATE) >= to_date('"+ filter.getInitialDate() +"','DD/MM/YYYY') " );
  		sbQuery.append("   AND trunc(pic.EXPERITATION_DATE) <= to_date('"+ filter.getFinalDate() +"','dd/MM/YYYY') " );
  		sbQuery.append("   AND HAB.CUT_DATE = trunc(pic.EXPERITATION_DATE) - 1 " );
  		sbQuery.append("   AND HAB.TOTAL_BALANCE > 0 " );
		
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getParticipant())){
  			sbQuery.append("   AND HA.ID_PARTICIPANT_FK = :participant " );
  		}
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getHolder())){
  			sbQuery.append("   AND " );
  			sbQuery.append("     (EXISTS " );
  			sbQuery.append("       (SELECT had.ID_HOLDER_FK " );
  			sbQuery.append("        FROM HOLDER_ACCOUNT_DETAIL had " );
  			sbQuery.append("                WHERE had.ID_HOLDER_FK = :cui_code " );
  			sbQuery.append("         AND had.ID_HOLDER_ACCOUNT_FK = hab.id_holder_account_pk " );
  			sbQuery.append("   )) " );
  		}
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getHolderAccount())){
  			sbQuery.append("   AND HA.id_holder_account_pk = :holder_account " );
  		}
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getCurrency())){
  			sbQuery.append("   AND s.currency = :currency " );
  		}
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getSecurityClass())){
  			sbQuery.append("   AND s.security_class = :class_value " );
  		}
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getSecurities())){
  			sbQuery.append("   AND s.id_security_code_pk = :value_series " );
  		}
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getState())){
  			sbQuery.append("   AND s.STATE_SECURITY = :state " );
  		}

  		//CORTE PARA LOS VALORES QUE VENCEN DE FORMA NORMAL
  		if(Validations.validateIsNullOrEmpty(filter.getState()) || filter.getState().equals(SecurityStateType.EXPIRED.getCode().toString())){
  			sbQuery.append(" UNION " );
  	  		
//  	  		sbQuery.append(" select " ); 
//  	  		sbQuery.append("     'VALORES' TITULO, " ); 
//  	  		sbQuery.append("     p.id_participant_pk, " ); 
//  	  		sbQuery.append("     p.mnemonic, " ); 
//  	  		sbQuery.append("     p.description, " ); 
//  	  		sbQuery.append("     HA.account_number, " ); 
//  	  		sbQuery.append("     s.CURRENCY, " ); 
//  	  		sbQuery.append("     to_char(s.ISSUANCE_DATE,'dd/mm/yyyy') ISSUANCE_DATE, " ); 
//  	  		sbQuery.append("     to_char(s.EXPIRATION_DATE, 'dd/mm/yyyy') EXPIRATION_DATE, " );  
//  	  		sbQuery.append("     S.ID_SECURITY_CODE_PK, " ); 
//  	  		sbQuery.append("     ((select max(pic_sub.coupon_number) from interest_payment_schedule ips_sub,program_interest_coupon pic_sub " );
//  	  		sbQuery.append("             where ips_sub.id_int_payment_schedule_pk = pic_sub.id_int_payment_schedule_fk and " );
//  	  		sbQuery.append("             ips_sub.id_security_code_fk = s.id_security_code_pk) " ); 
//  	  		sbQuery.append("     ||'/'|| " ); 
//  	  		sbQuery.append("     (select count(*) from interest_payment_schedule ips_sub2,program_interest_coupon pic_sub2 " );
//  	  		sbQuery.append("             where ips_sub2.id_int_payment_schedule_pk = pic_sub2.id_int_payment_schedule_fk and " );
//  	  		sbQuery.append("             ips_sub2.id_security_code_fk = s.id_security_code_pk)) TOTAL_CUP, " );
//	  	  	sbQuery.append("   (CASE " ); 
//	  		sbQuery.append("       WHEN TRUNC(s.EXPIRATION_DATE) < SYSDATE " );
//	  		sbQuery.append("           THEN (SELECT SUM(MFV.TOTAL_BALANCE) " ); 
//	  		sbQuery.append("                 FROM MARKET_FACT_VIEW MFV " );
//	  		sbQuery.append("                 WHERE 1 = 1 " );
//	  		sbQuery.append("                       AND TRUNC(MFV.CUT_DATE) = TRUNC(s.EXPIRATION_DATE) - 1 " ); 
//	  		sbQuery.append("                       AND MFV.ID_PARTICIPANT_PK = HA.ID_PARTICIPANT_FK " );
//	  		sbQuery.append("                       AND MFV.ID_HOLDER_ACCOUNT_PK = HA.ID_HOLDER_ACCOUNT_PK " );
//	  		sbQuery.append("                       AND MFV.ID_SECURITY_CODE_PK = s.id_security_code_pk " );
//	  		sbQuery.append("                 GROUP BY MFV.ID_SECURITY_CODE_PK,MFV.ID_PARTICIPANT_PK,MFV.ID_HOLDER_ACCOUNT_PK) " );
//	  		sbQuery.append("       ELSE " );
//	  		sbQuery.append("           hab.TOTAL_BALANCE " );
//	  		sbQuery.append("   END) TOTAL_BALANCE, " );
//  	  		sbQuery.append("     NVL(ROUND((SELECT SUM(PIC.COUPON_AMOUNT) " ); 
//	  		sbQuery.append("     FROM INTEREST_PAYMENT_SCHEDULE ips " ); 
//	  		sbQuery.append("     inner join PROGRAM_INTEREST_COUPON pic on pic.ID_INT_PAYMENT_SCHEDULE_FK=ips.ID_INT_PAYMENT_SCHEDULE_PK " ); 
//	  		sbQuery.append("     where ips.id_SECURITY_CODE_FK= S.ID_SECURITY_CODE_PK " );
//	  		sbQuery.append("     AND TRUNC(PIC.EXPERITATION_DATE) = TRUNC(s.expiration_date)),2),0) AS COUPON_AMOUNT_INT,");
//	  		sbQuery.append("     NVL(ROUND((SELECT SUM(pac.COUPON_AMOUNT) " ); 
//  	  		sbQuery.append("      FROM PROGRAM_AMORTIZATION_COUPON pac,AMORTIZATION_PAYMENT_SCHEDULE APS " ); 
//  	  		sbQuery.append("      WHERE APS.ID_AMO_PAYMENT_SCHEDULE_PK=pac.ID_AMO_PAYMENT_SCHEDULE_FK " ); 
//  	  		sbQuery.append("        AND APS.ID_SECURITY_CODE_FK=S.ID_SECURITY_CODE_PK " );
//  	  		sbQuery.append("        AND TRUNC(pac.EXPRITATION_DATE) = TRUNC(s.expiration_date) " );
//  	  		sbQuery.append("     ),2),0) AS COUPON_AMOUNT_AMO,");
//  	  		sbQuery.append(" (ROUND(NVL((SELECT SUM(PIC.COUPON_AMOUNT)    ");
//  	  		sbQuery.append("      FROM INTEREST_PAYMENT_SCHEDULE ips    ");
//  	  		sbQuery.append("      inner join PROGRAM_INTEREST_COUPON pic on pic.ID_INT_PAYMENT_SCHEDULE_FK=ips.ID_INT_PAYMENT_SCHEDULE_PK    ");
//  	  		sbQuery.append("      where ips.id_SECURITY_CODE_FK= S.ID_SECURITY_CODE_PK   ");
//	  		sbQuery.append("      AND TRUNC(PIC.EXPERITATION_DATE) = TRUNC(s.expiration_date)),0),2) +    ");
//	  		sbQuery.append("      ROUND(NVL((SELECT SUM(pac.COUPON_AMOUNT)    ");
//	  		sbQuery.append("       FROM PROGRAM_AMORTIZATION_COUPON pac,AMORTIZATION_PAYMENT_SCHEDULE APS    ");
//  	  		sbQuery.append("       WHERE APS.ID_AMO_PAYMENT_SCHEDULE_PK=pac.ID_AMO_PAYMENT_SCHEDULE_FK    ");
//  	  		sbQuery.append("         AND APS.ID_SECURITY_CODE_FK=S.ID_SECURITY_CODE_PK   ");
//  	  		sbQuery.append("         AND TRUNC(pac.EXPRITATION_DATE) = TRUNC(s.expiration_date)   ");
//	  		sbQuery.append("      ),0),2)) TOTAL_GENERAL,  ");
//  	  		sbQuery.append("     (select pt.text1 from parameter_table pt where pt.parameter_table_pk=s.currency) currency_mnemonic, " ); 
//  	  		sbQuery.append("     (select pt.parameter_name from parameter_table pt where pt.parameter_table_pk=s.currency) currency_name, " ); 
//  	  		sbQuery.append("     HA.ID_HOLDER_ACCOUNT_PK, " ); 
//  	  		sbQuery.append("     (SELECT LISTAGG(HAD.ID_HOLDER_FK,'<br>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) " ); 
//  	  		sbQuery.append("     FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) CUI, " ); 
//  	  		sbQuery.append("     (SELECT LISTAGG(H.FULL_NAME,'<br>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) " ); 
//  	  		sbQuery.append("     FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK " ); 
//  	  		sbQuery.append("     WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) CUI_DESCRIPTION " ); 
//  	  		sbQuery.append(" from " );
//  	  		sbQuery.append("     security s,holder_account_balance hab,participant p, holder_account ha " );
//  	  		sbQuery.append(" where " );
//  	  		sbQuery.append("     1 = 1 " );
//  	  		sbQuery.append("     and hab.id_participant_pk = p.id_participant_pk " );
//  	  		sbQuery.append("     and hab.id_holder_account_pk = HA.id_holder_account_pk " );
//  	  		sbQuery.append("     and hab.id_security_code_pk = s.id_security_code_pk " );
//  	  		sbQuery.append("     and trunc(s.expiration_date) <= NVL( (select pic_sub.EXPERITATION_DATE from interest_payment_schedule ips_sub,program_interest_coupon pic_sub " );
//  	  		sbQuery.append("         where ips_sub.id_int_payment_schedule_pk = pic_sub.id_int_payment_schedule_fk and " );
//  	  		sbQuery.append("         ips_sub.id_security_code_fk = s.id_security_code_pk and " );
//  	  		sbQuery.append("         pic_sub.COUPON_NUMBER = ( " );
//  	  		sbQuery.append("             select max(pic_sub2.coupon_number) from interest_payment_schedule ips_sub2,program_interest_coupon pic_sub2 " );
//  	  		sbQuery.append("             where ips_sub2.id_int_payment_schedule_pk = pic_sub2.id_int_payment_schedule_fk and " );
//  	  		sbQuery.append("             ips_sub2.id_security_code_fk = s.id_security_code_pk " );
//  	  		sbQuery.append("         )), to_date('"+ filter.getFinalDate() +"','dd/MM/YYYY') ) " );
//  	  		sbQuery.append("     AND trunc(s.expiration_date) >= to_date('"+ filter.getInitialDate() +"','DD/MM/YYYY') " );
//  	  		sbQuery.append("     AND trunc(s.expiration_date) <= to_date('"+ filter.getFinalDate() +"','dd/MM/YYYY') " );
  			
  			sbQuery.append(" select   ");
  			sbQuery.append("    'VALORES' TITULO,   ");
  			sbQuery.append("    p.id_participant_pk,   ");
  			sbQuery.append("    p.mnemonic,   ");
  			sbQuery.append("    p.description,   ");
  			sbQuery.append("    HA.account_number,   ");
  			sbQuery.append("    s.CURRENCY,   ");
  			sbQuery.append("    to_char(s.ISSUANCE_DATE,'dd/mm/yyyy') ISSUANCE_DATE,   ");
  			sbQuery.append("    to_char(s.EXPIRATION_DATE, 'dd/mm/yyyy') EXPIRATION_DATE,    ");
  			sbQuery.append("    S.ID_SECURITY_CODE_PK,   ");
  			sbQuery.append("    ((select max(pic_sub.coupon_number) from interest_payment_schedule ips_sub,program_interest_coupon pic_sub  ");
  			sbQuery.append("            where ips_sub.id_int_payment_schedule_pk = pic_sub.id_int_payment_schedule_fk and  ");
  			sbQuery.append("            ips_sub.id_security_code_fk = s.id_security_code_pk)   ");
  			sbQuery.append("    ||'/'||   ");
  			sbQuery.append("    (select count(*) from interest_payment_schedule ips_sub2,program_interest_coupon pic_sub2  ");
  			sbQuery.append("            where ips_sub2.id_int_payment_schedule_pk = pic_sub2.id_int_payment_schedule_fk and  ");
  			sbQuery.append("            ips_sub2.id_security_code_fk = s.id_security_code_pk)) TOTAL_CUP,  ");
  			sbQuery.append("    HAB.TOTAL_BALANCE TOTAL_BALANCE,  ");
  			sbQuery.append("    HAB.AVAILABLE_BALANCE AVAILABLE_BALANCE,  ");
  			sbQuery.append("    HAB.PAWN_BALANCE PAWN_BALANCE,  ");
  			sbQuery.append("    HAB.BAN_BALANCE BAN_BALANCE,  ");
  			sbQuery.append("    HAB.OTHER_BLOCK_BALANCE OTHER_BLOCK_BALANCE,  ");
  			sbQuery.append("    NVL(ROUND(pac.COUPON_AMOUNT,2),0) AS COUPON_AMOUNT, ");
  			sbQuery.append("    0 AS COUPON_AMOUNT_INT, ");
  			sbQuery.append("    NVL(ROUND(pac.COUPON_AMOUNT,2),0) AS COUPON_AMOUNT_AMO, ");
  			sbQuery.append("    (0 + NVL(ROUND(pac.COUPON_AMOUNT,2),0)) TOTAL_GENERAL,   ");
  			sbQuery.append("    (select pt.text1 from parameter_table pt where pt.parameter_table_pk=s.currency) currency_mnemonic,   ");
  			sbQuery.append("    (select pt.parameter_name from parameter_table pt where pt.parameter_table_pk=s.currency) currency_name,   ");
  			sbQuery.append("    HA.ID_HOLDER_ACCOUNT_PK,   ");
  			sbQuery.append("    (SELECT LISTAGG(HAD.ID_HOLDER_FK,'<br>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)   ");
  			sbQuery.append("    FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) CUI,   ");
  			sbQuery.append("    (SELECT LISTAGG(H.FULL_NAME,'<br>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)   ");
  			sbQuery.append("    FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK   ");
  			sbQuery.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) CUI_DESCRIPTION   ");
  			sbQuery.append("    FROM ACCOUNT_BALANCE_VIEW hab ");
  			sbQuery.append("    INNER JOIN SECURITY s                         ON s.ID_SECURITY_CODE_PK  = hab.ID_SECURITY_CODE_PK ");
  			sbQuery.append("    INNER JOIN HOLDER_ACCOUNT ha                  ON hab.ID_HOLDER_ACCOUNT_PK = HA.ID_HOLDER_ACCOUNT_PK  ");
  			sbQuery.append("    INNER JOIN PARTICIPANT p                      ON HAB.id_participant_Pk = p.id_participant_pk  ");
  			sbQuery.append("    INNER JOIN AMORTIZATION_PAYMENT_SCHEDULE APS  ON APS.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK  ");
  			sbQuery.append("    INNER JOIN PROGRAM_AMORTIZATION_COUPON PAC    ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK  ");
  			sbQuery.append(" where  ");
  			sbQuery.append("    1 = 1  ");
  			sbQuery.append("    AND trunc(PAC.expritation_date) >= to_date('"+ filter.getInitialDate() +"','DD/MM/YYYY')  ");
  			sbQuery.append("    AND trunc(PAC.expritation_date) <= to_date('"+ filter.getFinalDate() +"','dd/MM/YYYY') ");
  			sbQuery.append("    AND HAB.CUT_DATE = trunc(PAC.expritation_date) - 1 ");
  			sbQuery.append("    AND hab.TOTAL_BALANCE > 0 ");
  	  		
  	  		if(Validations.validateIsNotNullAndNotEmpty(filter.getParticipant())){
  	  			sbQuery.append("   AND HA.ID_PARTICIPANT_FK = :participant " );
  	  		}
  	  		if(Validations.validateIsNotNullAndNotEmpty(filter.getHolder())){
  	  			sbQuery.append("   AND " );
  	  			sbQuery.append("     (EXISTS " );
  	  			sbQuery.append("       (SELECT had.ID_HOLDER_FK " );
  	  			sbQuery.append("        FROM HOLDER_ACCOUNT_DETAIL had " );
  	  			sbQuery.append("                WHERE had.ID_HOLDER_FK = :cui_code " );
  	  			sbQuery.append("         AND had.ID_HOLDER_ACCOUNT_FK = hab.id_holder_account_pk " );
  	  			sbQuery.append("   )) " );
  	  		}
  	  		if(Validations.validateIsNotNullAndNotEmpty(filter.getHolderAccount())){
  	  			sbQuery.append("   AND HA.id_holder_account_pk = :holder_account " );
  	  		}
  	  		if(Validations.validateIsNotNullAndNotEmpty(filter.getCurrency())){
  	  			sbQuery.append("   AND s.currency = :currency " );
  	  		}
  	  		if(Validations.validateIsNotNullAndNotEmpty(filter.getSecurityClass())){
  	  			sbQuery.append("   AND s.security_class = :class_value " );
  	  		}
  	  		if(Validations.validateIsNotNullAndNotEmpty(filter.getSecurities())){
  	  			sbQuery.append("   AND s.id_security_code_pk = :value_series " );
  	  		}
  	  		if(Validations.validateIsNotNullAndNotEmpty(filter.getState())){
  	  			sbQuery.append("   AND s.STATE_SECURITY = :state " );
  	  		}
  		}
  		
  		//CORTE PARA LOS VALORES QUE SON REDIMIDOS
  		if(Validations.validateIsNullOrEmpty(filter.getState()) || filter.getState().equals(SecurityStateType.REDEEMED.getCode().toString())){
  			sbQuery.append(" UNION " );
  			
  			sbQuery.append(" SELECT " ); 
  			sbQuery.append("     'VALORES' TITULO, " ); 
  			sbQuery.append("     p.id_participant_pk, " ); 
  			sbQuery.append("     p.mnemonic, " ); 
  			sbQuery.append("     p.description, " ); 
  			sbQuery.append("     HA.account_number, " ); 
  			sbQuery.append("     s.CURRENCY, " ); 
  			sbQuery.append("     to_char(s.ISSUANCE_DATE,'dd/mm/yyyy') ISSUANCE_DATE, " ); 
  			sbQuery.append("     to_char(co.confirm_date, 'dd/mm/yyyy') EXPIRATION_DATE, " );  
  			sbQuery.append("     S.ID_SECURITY_CODE_PK, " );
  			sbQuery.append("     ' ' TOTAL_CUP, " );
  			sbQuery.append("     rd.available_balance AS TOTAL_BALANCE, " );
  			sbQuery.append("     rd.available_balance as AVAILABLE_BALANCE,  ");
  			sbQuery.append("     rd.PAWN_BALANCE as PAWN_BALANCE,  ");
  			sbQuery.append("     rd.BAN_BALANCE as BAN_BALANCE,  ");
  			sbQuery.append("     rd.OTHER_BLOCK_BALANCE as OTHER_BLOCK_BALANCE,  ");
  			//SUMA DE INTERES Y AMORTIZACION
  			sbQuery.append("     NVL(ROUND((SELECT SUM(PIC.COUPON_AMOUNT) " ); 
  			sbQuery.append("     FROM INTEREST_PAYMENT_SCHEDULE ips " ); 
  			sbQuery.append("     inner join PROGRAM_INTEREST_COUPON pic on pic.ID_INT_PAYMENT_SCHEDULE_FK=ips.ID_INT_PAYMENT_SCHEDULE_PK " ); 
  			sbQuery.append("     where ips.id_SECURITY_CODE_FK= S.ID_SECURITY_CODE_PK " );
  			sbQuery.append("     AND TRUNC(PIC.EXPERITATION_DATE) = TRUNC(s.expiration_date)),2) + " ); 
  			sbQuery.append("     ROUND((SELECT SUM(pac.COUPON_AMOUNT) " ); 
  			sbQuery.append("      FROM PROGRAM_AMORTIZATION_COUPON pac,AMORTIZATION_PAYMENT_SCHEDULE APS " ); 
  			sbQuery.append("      WHERE APS.ID_AMO_PAYMENT_SCHEDULE_PK=pac.ID_AMO_PAYMENT_SCHEDULE_FK " ); 
  			sbQuery.append("        AND APS.ID_SECURITY_CODE_FK=S.ID_SECURITY_CODE_PK " );
  			sbQuery.append("        AND TRUNC(pac.EXPRITATION_DATE) = TRUNC(s.expiration_date) " );
  			sbQuery.append("     ),2),0) COUPON_AMOUNT, " );
  			//END
  			sbQuery.append("     nvl(ROUND((SELECT SUM(PIC.COUPON_AMOUNT)   " );
  			sbQuery.append("     		 FROM INTEREST_PAYMENT_SCHEDULE ips   " );
  			sbQuery.append("     		 inner join PROGRAM_INTEREST_COUPON pic on pic.ID_INT_PAYMENT_SCHEDULE_FK=ips.ID_INT_PAYMENT_SCHEDULE_PK   " );
  			sbQuery.append("     		 where ips.id_SECURITY_CODE_FK= S.ID_SECURITY_CODE_PK  " );
  			sbQuery.append("     		 AND TRUNC(PIC.EXPERITATION_DATE) = TRUNC(s.expiration_date)),2),0) AS COUPON_AMOUNT_INT,  " );
  			sbQuery.append("     nvl(ROUND((SELECT SUM(pac.COUPON_AMOUNT)   " );
  			sbQuery.append("     		  FROM PROGRAM_AMORTIZATION_COUPON pac,AMORTIZATION_PAYMENT_SCHEDULE APS   " );
  			sbQuery.append("     		  WHERE APS.ID_AMO_PAYMENT_SCHEDULE_PK=pac.ID_AMO_PAYMENT_SCHEDULE_FK   " );
  			sbQuery.append("     			AND APS.ID_SECURITY_CODE_FK=S.ID_SECURITY_CODE_PK  " );
  			sbQuery.append("     			AND TRUNC(pac.EXPRITATION_DATE) = TRUNC(s.expiration_date)),2),0) AS COUPON_AMOUNT_AMO,  " );
  			sbQuery.append("     NVL(ROUND((SELECT SUM(PIC.COUPON_AMOUNT) " ); 
  			sbQuery.append("     FROM INTEREST_PAYMENT_SCHEDULE ips " ); 
  			sbQuery.append("     inner join PROGRAM_INTEREST_COUPON pic on pic.ID_INT_PAYMENT_SCHEDULE_FK=ips.ID_INT_PAYMENT_SCHEDULE_PK " ); 
  			sbQuery.append("     where ips.id_SECURITY_CODE_FK= S.ID_SECURITY_CODE_PK " );
  			sbQuery.append("     AND TRUNC(PIC.EXPERITATION_DATE) = TRUNC(s.expiration_date)),2) + " ); 
  			sbQuery.append("     ROUND((SELECT SUM(pac.COUPON_AMOUNT) " ); 
  			sbQuery.append("      FROM PROGRAM_AMORTIZATION_COUPON pac,AMORTIZATION_PAYMENT_SCHEDULE APS " ); 
  			sbQuery.append("      WHERE APS.ID_AMO_PAYMENT_SCHEDULE_PK=pac.ID_AMO_PAYMENT_SCHEDULE_FK " ); 
  			sbQuery.append("        AND APS.ID_SECURITY_CODE_FK=S.ID_SECURITY_CODE_PK " );
  			sbQuery.append("        AND TRUNC(pac.EXPRITATION_DATE) = TRUNC(s.expiration_date) " );
  			sbQuery.append("     ),2),0) TOTAL_GENERAL, " );
  			sbQuery.append("     (select pt.text1 from parameter_table pt where pt.parameter_table_pk=s.currency) currency_mnemonic, " ); 
  			sbQuery.append("     (select pt.parameter_name from parameter_table pt where pt.parameter_table_pk=s.currency) currency_name, " ); 
  			sbQuery.append("     HA.ID_HOLDER_ACCOUNT_PK, " ); 
  			sbQuery.append("     (SELECT LISTAGG(HAD.ID_HOLDER_FK,'<br>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) " ); 
  			sbQuery.append("     FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) CUI, " ); 
  			sbQuery.append("     (SELECT LISTAGG(H.FULL_NAME,'<br>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) " ); 
  			sbQuery.append("     FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK " ); 
  			sbQuery.append("     WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) CUI_DESCRIPTION " ); 
  			sbQuery.append(" FROM " ); 
  			sbQuery.append("     retirement_operation ro,custody_operation co,account_balance_view hab, " );
  			sbQuery.append("     security s,participant p,holder_account ha, retirement_detail rd " );
  			sbQuery.append(" where 1 = 1 " ); 
  			sbQuery.append("     and hab.id_security_code_pk = ro.id_security_code_fk " );
  			sbQuery.append(" 	 AND RO.IND_RETIREMENT_EXPIRATION <> 1");//Excluir las Operaciones de Retiro de valores Vencidos
  			sbQuery.append("     and hab.id_participant_pk = p.id_participant_pk " );
  			sbQuery.append("     and hab.id_holder_account_pk = HA.id_holder_account_pk " );
  			sbQuery.append("     and ro.id_security_code_fk = s.id_security_code_pk " );
  			sbQuery.append("     and ro.id_retirement_operation_pk = rd.id_retirement_operation_fk " );
  			sbQuery.append("     and hab.id_holder_account_pk = rd.id_holder_account_fk " );
  			sbQuery.append("     and hab.id_participant_pk = rd.id_participant_fk " );
  			sbQuery.append("     and ro.id_retirement_operation_pk = co.id_custody_operation_pk " );
  			sbQuery.append("     and trunc(s.expiration_date) <> TRUNC(co.confirm_date) " );
  			sbQuery.append("     and ro.motive in (667,668) " );
  			sbQuery.append("     and ro.state = 1144 " );
  			sbQuery.append("     and ro.retirement_type in (2174,2175) " );
  			sbQuery.append("     and hab.cut_date = trunc(co.confirm_date) - 1" );
  			sbQuery.append("     AND trunc(co.confirm_date) >= to_date('"+ filter.getInitialDate() +"','DD/MM/YYYY') " );
  			sbQuery.append("     AND trunc(co.confirm_date) <= to_date('"+ filter.getFinalDate() +"','dd/MM/YYYY') " );
  			
  			if(Validations.validateIsNotNullAndNotEmpty(filter.getParticipant())){
  				sbQuery.append("   AND HA.ID_PARTICIPANT_FK = :participant " );
  			}
  			if(Validations.validateIsNotNullAndNotEmpty(filter.getHolder())){
  				sbQuery.append("   AND " );
  				sbQuery.append("     (EXISTS " );
  				sbQuery.append("       (SELECT had.ID_HOLDER_FK " );
  				sbQuery.append("        FROM HOLDER_ACCOUNT_DETAIL had " );
  				sbQuery.append("                WHERE had.ID_HOLDER_FK = :cui_code " );
  				sbQuery.append("         AND had.ID_HOLDER_ACCOUNT_FK = hab.id_holder_account_pk " );
  				sbQuery.append("   )) " );
  			}
  			if(Validations.validateIsNotNullAndNotEmpty(filter.getHolderAccount())){
  				sbQuery.append("   AND HA.id_holder_account_pk = :holder_account " );
  			}
  			if(Validations.validateIsNotNullAndNotEmpty(filter.getCurrency())){
  				sbQuery.append("   AND s.currency = :currency " );
  			}
  			if(Validations.validateIsNotNullAndNotEmpty(filter.getSecurityClass())){
  				sbQuery.append("   AND s.security_class = :class_value " );
  			}
  			if(Validations.validateIsNotNullAndNotEmpty(filter.getSecurities())){
  				sbQuery.append("   AND s.id_security_code_pk = :value_series " );
  			}
  			if(Validations.validateIsNotNullAndNotEmpty(filter.getState())){
  				sbQuery.append("   AND s.STATE_SECURITY = :state " );
  			}
  		}

  		sbQuery.append(" ORDER BY " );
  		sbQuery.append("   1 DESC,3,6,5 " );
  		
  		String strQueryFormatted = sbQuery.toString();
		
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getParticipant())){
  			strQueryFormatted= strQueryFormatted.replace(":participant", filter.getParticipant().toString());
  		}
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getHolder())){
  			strQueryFormatted=strQueryFormatted.replace(":cui_code", filter.getHolder().toString());
  		}
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getHolderAccount())){
  			strQueryFormatted=strQueryFormatted.replace(":holder_account", filter.getHolderAccount().toString());
  		}
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getCurrency())){
  			strQueryFormatted=strQueryFormatted.replace(":currency", filter.getCurrency().toString());
  		}
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getSecurityClass())){
  			strQueryFormatted=strQueryFormatted.replace(":class_value", filter.getSecurityClass().toString());
  		}
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getSecurities())){
  			strQueryFormatted=strQueryFormatted.replace(":value_series", "'" + filter.getSecurities().toString() + "'");
  		}
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getState())){
  			strQueryFormatted=strQueryFormatted.replace(":state", filter.getState().toString());
  		}
  		
		return strQueryFormatted;
	}
	
	public String getRelationCurrentBlock(GenericCustodyOperationTO filter,Long idStockCalculation) {

		StringBuilder sbQuery = new StringBuilder();

  		sbQuery.append(" SELECT " );
  		sbQuery.append(" 	BO.ID_PARTICIPANT_FK, " );
  		sbQuery.append("    '(' || PAR.MNEMONIC || ') ' || PAR.DESCRIPTION as PARTICIPANT, " );
  		sbQuery.append(" 	BO.ID_SECURITY_CODE_FK, " );
  		sbQuery.append("    (SELECT TEXT1 FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = SE.SECURITY_CLASS) as SECURITY_CLASS_MNEMONIC, " );
  		sbQuery.append(" 	(SELECT parameter_name FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK=SE.SECURITY_CLASS) SECURITY_DESCRIPTION, " );
  		sbQuery.append(" 	SE.ALTERNATIVE_CODE, " );
  		sbQuery.append(" 	(SELECT PM.DESCRIPTION FROM PARAMETER_TABLE PM WHERE PM.PARAMETER_TABLE_PK = BR.BLOCK_TYPE) BLOCK_TYPE, " );
  		sbQuery.append(" 	BR.BLOCK_NUMBER NRO_CITE, " );
  		sbQuery.append(" 	BR.ID_BLOCK_REQUEST_PK BLOCK_REQUEST, " );
  		sbQuery.append("    HA.ID_HOLDER_ACCOUNT_PK, " );
  		sbQuery.append(" 	HA.ACCOUNT_NUMBER CUENTA_TITULAR_DEUDOR, " );
  		//sbQuery.append("   FN_GET_HOLDER_NAME(HA.ID_HOLDER_ACCOUNT_PK) CUI_TITULAR_DEUDOR, " );
  		sbQuery.append("    (SELECT LISTAGG((select h.id_holder_pk from holder h where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK),'<br/>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ) CUI_TITULAR, " );
        sbQuery.append("    (SELECT LISTAGG((select h.full_name from holder h where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK),'<br/>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ) NOMBRE_TITULAR, " );
        sbQuery.append("    (SELECT LISTAGG((select h.DOCUMENT_NUMBER from holder h where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK),'<br/>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ) CI_TITULAR, " );
  		sbQuery.append(" 	BE.FULL_NAME ACREEDOR, " );
  		sbQuery.append(" 	TRUNC(BR.CONFIRMATION_DATE) FECHA_INICIAL, " );
  		sbQuery.append(" 	BO.ACTUAL_BLOCK_BALANCE CANTIDAD_BLOQUEO, " );
  		sbQuery.append(" 	DECODE(BO.BLOCK_LEVEL,1119,'NO','SI') REBLOQUEO, " );
//  		sbQuery.append(" 	BR.COMMENTS " ); //issue 554 mostrar observaciones  
  		sbQuery.append(" 	(case when BR.COMMENTS is null then 'SIN OBSERVACIONES' else BR.COMMENTS end) AS COMMENTS " );
  		sbQuery.append("  " );
  		sbQuery.append(" FROM BLOCK_OPERATION BO, BLOCK_REQUEST BR, HOLDER_ACCOUNT HA,BLOCK_ENTITY BE,  " );
  		sbQuery.append("   SECURITY SE, PARTICIPANT PAR " );
  		if(Validations.validateIsNotNullAndNotEmpty(idStockCalculation)){
  			sbQuery.append(" ,STOCK_CALCULATION_PROCESS SCP,BLOCK_STOCK_CALCULATION BSC " );	
  		}
  		sbQuery.append(" WHERE 1 = 1  " );
  		sbQuery.append("   AND BR.ID_BLOCK_REQUEST_PK = BO.ID_BLOCK_REQUEST_FK " );
  		sbQuery.append("   AND BO.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK " );
  		sbQuery.append("   AND BR.ID_BLOCK_ENTITY_FK = BE.ID_BLOCK_ENTITY_PK " );
  		sbQuery.append("   AND BO.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK " );
  		sbQuery.append("   AND PAR.ID_PARTICIPANT_PK = BO.ID_PARTICIPANT_FK " );
  		if(Validations.validateIsNotNullAndNotEmpty(idStockCalculation)){
  			sbQuery.append("   AND SCP.ID_STOCK_CALCULATION_PK = BSC.ID_STOCK_CALCULATION_FK " );
  	  		sbQuery.append("   AND BSC.ID_BLOCK_OPERATION_FK = BO.ID_BLOCK_OPERATION_PK " );
  	  		sbQuery.append("   AND SCP.STOCK_TYPE = 1324 " );
  			sbQuery.append("   AND TRUNC(SCP.CUTOFF_DATE) = to_date('"+ filter.getInitialDate() +"','DD/MM/YYYY') " );
  		}
  		
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getParticipant())){
  			sbQuery.append("   AND HA.ID_PARTICIPANT_FK = :participant " );
  		}
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getHolder())){
  			sbQuery.append("   AND " );
  			sbQuery.append("     (EXISTS " );
  			sbQuery.append("       (SELECT had.ID_HOLDER_FK " );
  			sbQuery.append("        FROM HOLDER_ACCOUNT_DETAIL had " );
  			sbQuery.append("                WHERE had.ID_HOLDER_FK = :cui_code " );
  			sbQuery.append("         AND had.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK " );
  			sbQuery.append("   )) " );
  		}
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getBlockType())){
  			sbQuery.append("   AND BR.BLOCK_TYPE = :blockType " );
  		}
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getIssuer())){
  			sbQuery.append("   AND SE.ID_ISSUER_FK = :idIssuerPk " );
  		}
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getCurrency())){
  			sbQuery.append("   AND SE.CURRENCY = :currency " );
  		}
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getSecurities())){
  			sbQuery.append("   AND SE.ID_SECURITY_CODE_PK = :idSecurityCodePk " );
  		}
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getSecurityClass())){
  			sbQuery.append("   AND SE.SECURITY_CLASS = :securityClass " );
  		}
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getHolderAccount())){
  			sbQuery.append("   AND HA.ID_HOLDER_ACCOUNT_PK = :idHolderAccountPk " );
  		}
  		
  		sbQuery.append("   AND BO.ACTUAL_BLOCK_BALANCE > 0 " );
  		sbQuery.append("   AND BO.BLOCK_STATE IN (1141,1142) " ); //issue 481 boqueos vigentes 
  		
  		sbQuery.append("  " );
  		sbQuery.append(" ORDER BY " );
  		if(Validations.validateIsNullOrEmpty(filter.getInstitutionType()) || (!filter.getInstitutionType().equals(InstitutionType.ISSUER.getCode()) &&
  				!filter.getInstitutionType().equals(InstitutionType.ISSUER_DPF.getCode()))){
  			sbQuery.append("   PAR.MNEMONIC, " );
  		}
  		sbQuery.append("   SE.ID_SECURITY_CODE_PK, " );
  		sbQuery.append("   BR.ID_BLOCK_REQUEST_PK, " );
  		sbQuery.append("   HA.ID_HOLDER_ACCOUNT_PK " );

  		String strQueryFormatted = sbQuery.toString();
		
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getParticipant())){
  			strQueryFormatted= strQueryFormatted.replace(":participant", filter.getParticipant().toString());
  		}
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getHolder())){
  			strQueryFormatted=strQueryFormatted.replace(":cui_code", filter.getHolder().toString());
  		}
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getBlockType())){
  			strQueryFormatted=strQueryFormatted.replace(":blockType", filter.getBlockType().toString());
  		}
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getIssuer())){
  			strQueryFormatted=strQueryFormatted.replace(":idIssuerPk", "'" + filter.getIssuer().toString() + "'");
  		}
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getCurrency())){
  			strQueryFormatted=strQueryFormatted.replace(":currency", filter.getCurrency().toString());
  		}
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getSecurities())){
  			strQueryFormatted=strQueryFormatted.replace(":idSecurityCodePk", "'" + filter.getSecurities().toString() + "'");
  		}
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getSecurityClass())){
  			strQueryFormatted=strQueryFormatted.replace(":securityClass",filter.getSecurityClass().toString());
  		}
  		if(Validations.validateIsNotNullAndNotEmpty(filter.getHolderAccount())){
  			strQueryFormatted=strQueryFormatted.replace(":idHolderAccountPk", filter.getHolderAccount().toString());
  		}
  		

  		
		return strQueryFormatted; 
	}
	
	 /**
     * Query del reporte Bloqueos y desbloqueos
     * @param registrationAssignmentHoldersTO
     * @return
     */
    
    public String getQueryBlockagesUnlock(BlockagesUnlockTO blockagesUnlockTO){
    	StringBuilder sbQuery= new StringBuilder();
    	
    	sbQuery.append(" 	SELECT	 ");
    	sbQuery.append(" 	co.operation_number as NRO_OPE_DE_BLOQUEO,	 ");
    	sbQuery.append(" 	br.id_block_request_pk as NRO_SOL_DE_BLOQUEO,	 ");
    	sbQuery.append(" 	ubo.document_number as NRO_OPER_DESBLOQUEO,	 ");
    	sbQuery.append(" 	CASE WHEN ubo.id_unblock_operation_pk is null THEN 0 ELSE 1 END as DESBLOQUEO,	 ");
    	sbQuery.append(" 	ubo.ID_UNBLOCK_REQUEST_FK as NRO_SOL_DE_DESBLOQUEO,	 ");
    	sbQuery.append(" 	TO_CHAR(br.registry_date,'dd/MM/yyyy - hh:Mi') as FECHA_HORA_BLOQUEO,	 ");
    	sbQuery.append(" 	TO_CHAR(ubr.registry_date,'dd/MM/yyyy - hh:Mi') as FECHA_HORA_DESBLOQUEO,	 ");
    	sbQuery.append(" 	(select pt.description from parameter_table pt where pt.parameter_table_pk=br.block_type) TIPO_BLOQUEO,	 ");
    	sbQuery.append(" 	secu.ID_SECURITY_CODE_PK as CLAVE_VALOR,	 ");
    	sbQuery.append(" 	secu.security_class as CLASE_VALOR,	 ");
    	sbQuery.append(" 	decode(ubo.id_unblock_operation_pk,NULL,bo.ORIGINAL_BLOCK_BALANCE,ubo.UNBLOCK_QUANTITY) as CANTIDAD,	 ");
    	sbQuery.append(" 	br.registry_user as USUARIO,	 ");
    	sbQuery.append(" 	(select pt.description from parameter_table pt where pt.parameter_table_pk=br.block_request_state) ESTADO_BLOQUEO,	 ");
    	sbQuery.append(" 	(select par.MNEMONIC from participant par where par.ID_PARTICIPANT_PK=HA.ID_PARTICIPANT_FK) as participant,	 ");
    	sbQuery.append(" 	HA.ACCOUNT_NUMBER,	 ");
    	sbQuery.append(" 	(SELECT LISTAGG(HAD.ID_HOLDER_FK,',' || chr(13)) WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)	 ");
    	sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK =HA.ID_HOLDER_ACCOUNT_PK	 ");
    	sbQuery.append(" 	) AS CUI	 ");
    	sbQuery.append(" 	From unblock_operation ubo	 ");
    	sbQuery.append(" 	right join block_operation bo On ubo.id_block_operation_fk=bo.id_block_operation_pk and ubo.unblock_state=1208	 ");
    	sbQuery.append(" 	left join block_request br On br.id_block_request_pk=bo.id_block_request_fk	 ");
    	sbQuery.append(" 	left join custody_operation co On bo.id_block_operation_pk=co.id_custody_operation_pk	 ");
    	sbQuery.append(" 	left join unblock_request ubr On ubo.id_unblock_request_fk=ubr.id_unblock_request_pk	 ");
    	sbQuery.append(" 	left join security secu On bo.id_security_code_fk=secu.id_security_code_pk	 ");
    	sbQuery.append(" 	left join holder_account ha On bo.id_holder_account_fk=HA.id_holder_account_pk	 ");
    	sbQuery.append(" 	where	 ");
    	
    	sbQuery.append(" 	(trunc(br.registry_date) between (TO_DATE(':date_initial','dd/MM/yyyy')) and (TO_DATE(':date_end','dd/MM/yyyy'))	 ");
    	sbQuery.append(" 	or trunc(ubr.registry_date) between (TO_DATE(':date_initial','dd/MM/yyyy')) and (TO_DATE(':date_end','dd/MM/yyyy')) )	 ");
    	
		if(Validations.validateIsNotNull(blockagesUnlockTO.getParticipantPk())){
			sbQuery.append(" 	and(:participant = bo.ID_PARTICIPANT_fK)	 ");
		}
		if(Validations.validateIsNotNull(blockagesUnlockTO.getIdHolderAccountPk())){
			sbQuery.append(" 	and (:holder_account = HA.id_holder_account_pk)	 ");
    	}
		if(Validations.validateIsNotNull(blockagesUnlockTO.getIdHolderPk())){
			sbQuery.append(" 	and (:cui_holder = br.ID_HOLDER_FK)	 ");
    	}
		if(Validations.validateIsNotNull(blockagesUnlockTO.getBlockType())){
			sbQuery.append(" 	and (:block_type = br.block_type)	 ");
    	}
		if(Validations.validateIsNotNull(blockagesUnlockTO.getSecurityClass())){
			sbQuery.append(" 	and (:value_class = secu.security_class)	 ");
		}
		if(Validations.validateIsNotNull(blockagesUnlockTO.getSecurityCode())){
			sbQuery.append(" 	and (':security_code' = secu.id_security_code_pk)	 ");
		}
		if(Validations.validateIsNotNull(blockagesUnlockTO.getRequestState())){
			sbQuery.append(" 	and (:request_state = br.block_request_state)	 ");
		}
    	sbQuery.append(" 	order by br.registry_date desc	 ");

		String strQueryFormatted = sbQuery.toString();
		
		if(blockagesUnlockTO.getParticipantPk()!=null){
    		strQueryFormatted= strQueryFormatted.replace(":participant", blockagesUnlockTO.getParticipantPk().toString());
    	}
    	if(blockagesUnlockTO.getBlockType()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":block_type", blockagesUnlockTO.getBlockType().toString());
    	}
    	if(blockagesUnlockTO.getRequestState()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":request_state", blockagesUnlockTO.getRequestState().toString());
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(blockagesUnlockTO.getSecurityCode())){
    		strQueryFormatted=strQueryFormatted.replace(":security_code", blockagesUnlockTO.getSecurityCode().toString());
    	}
    	if(blockagesUnlockTO.getSecurityClass()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":security_class", blockagesUnlockTO.getSecurityClass().toString());
    	}
    	if(blockagesUnlockTO.getIdHolderAccountPk()!=null){ 
    		strQueryFormatted= strQueryFormatted.replace(":holder_account", blockagesUnlockTO.getIdHolderAccountPk().toString());
    	}
    	if(blockagesUnlockTO.getIdHolderPk()!=null){
    		strQueryFormatted= strQueryFormatted.replace(":cui_code", blockagesUnlockTO.getIdHolderPk().toString());
    	}
    	
    	strQueryFormatted=strQueryFormatted.replace(":date_initial", blockagesUnlockTO.getStrDateInitial());
    	strQueryFormatted=strQueryFormatted.replace(":date_end", blockagesUnlockTO.getStrDateEnd());
    	
    	return strQueryFormatted;
    }
    
    /**
     * Query del reporte Solicitudes de CAT a valor nominal
     * @param registrationAssignmentHoldersTO
     * @return
     */
    
    public String getQueryRequestAccreditation(RequestAccreditationCertificationTO requestAccreditationCertificationTO){
    	StringBuilder sbQuery= new StringBuilder();
    	
    	sbQuery.append("  	SELECT	 ");	
    	sbQuery.append("  	  TO_CHAR(AO.REGISTER_DATE,'dd/mm/yyyy') REGISTER_DATE,	 ");	//0
    	sbQuery.append("  	  (PA.MNEMONIC || ' - ' || PA.ID_PARTICIPANT_PK) as PARTICIPANT,	 ");	//1
    	sbQuery.append("  	  (select pt.description from parameter_table pt where pt.parameter_table_pk=AO.MOTIVE) as MOTIVO,	 ");	//2
    	sbQuery.append("  	  CO.OPERATION_NUMBER,	 ");	//3
    	sbQuery.append("  	  TO_CHAR(AO.ISSUANCE_DATE,'dd/mm/yyyy') ISSUANCE_DATE,	 ");	//4
    	sbQuery.append("  	  SE.ID_SECURITY_CODE_PK,	 ");	//5
    	sbQuery.append("  	    (select pt.parameter_name from parameter_table pt where pt.parameter_table_pk=AO.CERTIFICATION_TYPE) CERTTYPE,	 ");	//6
    	sbQuery.append("  	    (select pt.description from parameter_table pt where pt.parameter_table_pk=BOC.BLOCK_TYPE) BLOCKTYPE,	 ");	//7
    	sbQuery.append("  	  (SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = SE.CURRENCY) CURRENCY,	 ");	//8
    	sbQuery.append("  	  AD.TOTAL_BALANCE,	 ");	//9
    	sbQuery.append("  	  SE.CURRENT_NOMINAL_VALUE UNIT_NOMINAL_VALUE,	 ");	//10
    	sbQuery.append("  	  (SE.CURRENT_NOMINAL_VALUE * AD.TOTAL_BALANCE) TOTAL_NOMINAL_VALUE,	 ");	//11
    	sbQuery.append("  	  CASE	 ");	
    	sbQuery.append("  	    WHEN SE.CURRENCY = 127 THEN (SE.CURRENT_NOMINAL_VALUE * AD.TOTAL_BALANCE) /	 ");	
    	sbQuery.append("  	        nvl(( SELECT D.BUY_PRICE	 ");	
    	sbQuery.append("  	              FROM DAILY_EXCHANGE_RATES D	 ");	
    	sbQuery.append("  	              WHERE D.DATE_RATE = AO.REGISTER_DATE	 ");	
    	sbQuery.append("  	                AND D.ID_CURRENCY = 430	 ");	
    	sbQuery.append("  	                AND D.ID_SOURCE_INFORMATION = 212	 ");	
    	sbQuery.append("  	        ),1)	 ");	
    	sbQuery.append("  	    WHEN SE.CURRENCY IN (1734,1304)	 ");	
    	sbQuery.append("  	      THEN (SE.CURRENT_NOMINAL_VALUE * AD.TOTAL_BALANCE) *	 ");	
    	sbQuery.append("  	        nvl(( SELECT D.BUY_PRICE	 ");	
    	sbQuery.append("  	              FROM DAILY_EXCHANGE_RATES D	 ");	
    	sbQuery.append("  	              WHERE D.DATE_RATE = AO.REGISTER_DATE	 ");	
    	sbQuery.append("  	                AND D.ID_CURRENCY = 127	 ");	
    	sbQuery.append("  	                AND D.ID_SOURCE_INFORMATION = 212	 ");	
    	sbQuery.append("  	        ),1) /	 ");	
    	sbQuery.append("  	        nvl(( SELECT D.BUY_PRICE	 ");	
    	sbQuery.append("  	              FROM DAILY_EXCHANGE_RATES D	 ");	
    	sbQuery.append("  	              WHERE D.DATE_RATE = AO.REGISTER_DATE	 ");	
    	sbQuery.append("  	                AND D.ID_CURRENCY = 430	 ");	
    	sbQuery.append("  	                AND D.ID_SOURCE_INFORMATION = 212	 ");	
    	sbQuery.append("  	        ),1)	 ");	
    	sbQuery.append("  	    WHEN SE.CURRENCY IN (430,1853)  THEN (SE.CURRENT_NOMINAL_VALUE * AD.TOTAL_BALANCE)	 ");	
    	sbQuery.append("  	    ELSE (SE.CURRENT_NOMINAL_VALUE * AD.TOTAL_BALANCE)	 ");	
    	sbQuery.append("  	  END TOTAL_NOMINAL_VALUE_D,	 ");	//12
    	sbQuery.append("  	    (select pt.description from parameter_table pt where pt.parameter_table_pk=AO.ACCREDITATION_STATE) as STATE	 ");	//13
    	sbQuery.append("  	FROM ACCREDITATION_OPERATION AO	 ");	
    	sbQuery.append("  	INNER JOIN CUSTODY_OPERATION CO             ON AO.ID_ACCREDITATION_OPERATION_PK = CO.ID_CUSTODY_OPERATION_PK	 ");	
    	sbQuery.append("  	INNER JOIN ACCREDITATION_DETAIL AD          ON AD.ID_ACCREDITATION_OPERATION_FK = AO.ID_ACCREDITATION_OPERATION_PK	 ");	
    	sbQuery.append("  	LEFT JOIN BLOCK_OPERATION_CERTIFICATION BOC ON BOC.ID_ACCREDITATION_DETAIL_FK = AD.ID_ACCREDITATION_DETAIL_PK	 ");	
    	sbQuery.append("  	INNER JOIN PARTICIPANT PA                   ON PA.ID_PARTICIPANT_PK = AD.ID_PARTICIPANT_FK	 ");	
    	sbQuery.append("  	INNER JOIN SECURITY SE                      ON SE.ID_SECURITY_CODE_PK = AD.ID_SECURITY_CODE_FK	 ");	

    	sbQuery.append("  	WHERE trunc(AO.REGISTER_DATE) BETWEEN to_date(':date_initial','dd/mm/yyyy') AND to_date(':date_end','dd/mm/yyyy') ");	
    	
		if(Validations.validateIsNotNull(requestAccreditationCertificationTO.getParticipantPk())){
	    	sbQuery.append("  	  AND (PA.ID_PARTICIPANT_PK = :participant)	 ");
		}
		if(Validations.validateIsNotNull(requestAccreditationCertificationTO.getIdHolderAccountPk())){
			sbQuery.append("  	  AND (AD.ID_HOLDER_ACCOUNT_FK = :holder_account)	 ");
    	}
		if(Validations.validateIsNotNull(requestAccreditationCertificationTO.getIdHolderPk())){
			sbQuery.append("  	  AND (AD.ID_HOLDER_FK = :cui_code)	 ");	
    	}
		if(Validations.validateIsNotNull(requestAccreditationCertificationTO.getRequestNumber())){
			sbQuery.append("  	  AND (CO.OPERATION_NUMBER = :operation_number)	 ");
		}
		if(Validations.validateIsNotNull(requestAccreditationCertificationTO.getMotive())){
			sbQuery.append("  	  AND (AO.MOTIVE = :motive)	 ");
		}
		if(Validations.validateIsNotNull(requestAccreditationCertificationTO.getRequestState())){
			sbQuery.append("  	  AND (AO.ACCREDITATION_STATE = :state)	 ");
		}
		sbQuery.append("  	ORDER BY AO.REGISTER_DATE,2,3,4	 ");	

		String strQueryFormatted = sbQuery.toString();
		
		if(requestAccreditationCertificationTO.getParticipantPk()!=null){
    		strQueryFormatted= strQueryFormatted.replace(":participant", requestAccreditationCertificationTO.getParticipantPk().toString());
    	}
		if(Validations.validateIsNotNullAndNotEmpty(requestAccreditationCertificationTO.getRequestState())){
    		strQueryFormatted=strQueryFormatted.replace(":request_state", requestAccreditationCertificationTO.getRequestState().toString());
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(requestAccreditationCertificationTO.getMotive())){
    		strQueryFormatted=strQueryFormatted.replace(":motive", requestAccreditationCertificationTO.getMotive().toString());
    	}
    	if(requestAccreditationCertificationTO.getRequestNumber()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":operation_number", requestAccreditationCertificationTO.getRequestNumber().toString());
    	}
    	if(requestAccreditationCertificationTO.getIdHolderAccountPk()!=null){ 
    		strQueryFormatted= strQueryFormatted.replace(":holder_account", requestAccreditationCertificationTO.getIdHolderAccountPk().toString());
    	}
    	if(requestAccreditationCertificationTO.getIdHolderPk()!=null){
    		strQueryFormatted= strQueryFormatted.replace(":cui_code", requestAccreditationCertificationTO.getIdHolderPk().toString());
    	}
    	
    	strQueryFormatted=strQueryFormatted.replace(":date_initial", requestAccreditationCertificationTO.getStrDateInitial());
    	strQueryFormatted=strQueryFormatted.replace(":date_end", requestAccreditationCertificationTO.getStrDateEnd());
    	
    	return strQueryFormatted;
    }
    
    public String getQueryExpirationSecurities(ExpirationReportTO expirationReportTO){
    	StringBuilder sbQuery= new StringBuilder();
    	
    	sbQuery.append(" SELECT ");
    	sbQuery.append("   (SELECT PT.TEXT1 FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK = S.SECURITY_CLASS) AS CLASE_VALOR, ");
    	sbQuery.append("   S.ID_SECURITY_CODE_ONLY AS ONLY_CODE, ");
    	sbQuery.append("   PIC.COUPON_NUMBER, ");
    	sbQuery.append("   TO_CHAR(PIC.EXPERITATION_DATE, 'DD/MM/YYYY') EXPIRATION_DATE, ");
    	sbQuery.append("   ROUND(PIC.COUPON_AMOUNT,2) + ROUND(NVL((SELECT PAC.COUPON_AMOUNT FROM PROGRAM_AMORTIZATION_COUPON PAC,amortization_payment_schedule APS ");
    	sbQuery.append("                                 WHERE PAC.ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK AND APS.ID_SECURITY_CODE_FK =  ");
    	sbQuery.append("                                 S.ID_SECURITY_CODE_PK AND TRUNC(PAC.EXPRITATION_DATE) = TRUNC(PIC.EXPERITATION_DATE)),0),2) INITIAL_NOMINAL_VALUE, ");
    	sbQuery.append("   HAB.TOTAL_BALANCE CANTIDAD, ");
    	sbQuery.append("   (SELECT LISTAGG((CASE H.DOCUMENT_TYPE WHEN 86 THEN 'DCI' WHEN 386 THEN 'NIT' WHEN 389 THEN 'PAS' ELSE 'RUN' END),',') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
    	sbQuery.append("     FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
    	sbQuery.append("     WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS DOCUMENT_TYPE, ");
    	sbQuery.append("   (SELECT LISTAGG(H.DOCUMENT_NUMBER,',') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
    	sbQuery.append("     FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
    	sbQuery.append("     WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS DOCUMENT_NUMBER, ");
    	sbQuery.append("   (SELECT LISTAGG(H.NAME,',') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
    	sbQuery.append("     FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
    	sbQuery.append("     WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS NAME, ");
    	sbQuery.append("   (SELECT LISTAGG(H.FIRST_LAST_NAME,',') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
    	sbQuery.append("     FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
    	sbQuery.append("     WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS FIRST_LAST_NAME, ");
    	sbQuery.append("   (SELECT LISTAGG(H.SECOND_LAST_NAME,',') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
    	sbQuery.append("     FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
    	sbQuery.append("     WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS SECOND_LAST_NAME, ");
    	sbQuery.append("   (SELECT LISTAGG((CASE H.HOLDER_TYPE WHEN 96 THEN 'N' ELSE 'J' END),',') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
    	sbQuery.append("     FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
    	sbQuery.append("     WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS HOLDER_TYPE, ");
    	sbQuery.append("   (SELECT LISTAGG(TO_CHAR(H.BIRTH_DATE, 'DD/MM/YYYY'),',') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
    	sbQuery.append("     FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
    	sbQuery.append("     WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS FECH_NAC, ");
    	sbQuery.append("   (SELECT LISTAGG((CASE H.STATE_HOLDER WHEN 496 THEN 'H' ELSE 'D' END),',') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
    	sbQuery.append("     FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
    	sbQuery.append("     WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS ESTADO, ");
    	sbQuery.append("   (nvl(hab.accreditation_balance,0)+nvl(hab.BAN_BALANCE,0)+nvl(hab.OTHER_BLOCK_BALANCE,0)+nvl(hab.PAWN_BALANCE,0)) BLOQUEO ");
    	sbQuery.append("  ");
    	sbQuery.append(" FROM HOLDER_ACCOUNT_BALANCE hab ");
    	sbQuery.append("   INNER JOIN SECURITY S                         ON s.ID_SECURITY_CODE_PK=hab.ID_SECURITY_CODE_PK ");
    	sbQuery.append("   INNER JOIN HOLDER_ACCOUNT ha                  ON hab.ID_HOLDER_ACCOUNT_PK= HA.ID_HOLDER_ACCOUNT_PK ");
    	sbQuery.append("   INNER JOIN interest_payment_schedule ips      ON s.id_security_code_pk=ips.id_security_code_fk ");
    	sbQuery.append("   INNER JOIN program_interest_coupon pic        ON ips.id_int_payment_schedule_pk=pic.id_int_payment_schedule_fk ");
    	sbQuery.append("  ");
    	sbQuery.append(" WHERE 1 = 1 ");
    	sbQuery.append("   AND trunc(PIC.EXPERITATION_DATE) >= to_date('"+expirationReportTO.getInitialDate()+"','dd/MM/yyyy') ");
    	sbQuery.append("   AND trunc(PIC.EXPERITATION_DATE) <= to_date('"+expirationReportTO.getEndDate()+"','dd/MM/yyyy') ");
    	sbQuery.append("   AND HAB.TOTAL_BALANCE > 0 ");
    	sbQuery.append("   AND HAB.ID_PARTICIPANT_PK = 103 ");
    	
//    	sbQuery.append("   AND S.SECURITY_CLASS = 414 ");
    	if(Validations.validateIsNotNull(expirationReportTO.getCui())){
			sbQuery.append(" AND (EXISTS																																			");
			sbQuery.append(" 	(SELECT had.ID_HOLDER_FK																															");
			sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL had																														");
			sbQuery.append(" 	WHERE had.ID_HOLDER_FK = :cui_code																													");
			sbQuery.append(" 		AND had.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK))																						");
	    }
    	if(Validations.validateIsNotNull(expirationReportTO.getSecurityClass())){
    		sbQuery.append(" and s.security_class = :security_class");
    	}else{
    		sbQuery.append(" and s.security_class = 414");
    	}
    	if(Validations.validateIsNotNull(expirationReportTO.getSecurityCode())){
    		sbQuery.append(" and s.id_security_code_pk = ':securityPk'																											");
    	}
    	sbQuery.append("    ");
    	sbQuery.append(" ORDER BY  ");
    	sbQuery.append("   1,4,2 ");
		
		String strQueryFormatted = sbQuery.toString();
		
    	if(Validations.validateIsNotNullAndNotEmpty(expirationReportTO.getCui())){
    		strQueryFormatted = strQueryFormatted.replace(":cui_code", expirationReportTO.getCui().toString());
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(expirationReportTO.getSecurityClass())){
    		strQueryFormatted = strQueryFormatted.replace(":security_class", expirationReportTO.getSecurityClass().toString());
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(expirationReportTO.getSecurityCode())){
    		strQueryFormatted = strQueryFormatted.replace(":securityPk", expirationReportTO.getSecurityCode().toString());
    	}
    	return strQueryFormatted;
	}
    
    /**
     * Query del reporte Solicitudes de CAT a valor nominal
     * @param registrationAssignmentHoldersTO
     * @return
     */
    
    public String getQueryBalanceTransferAvailable(BalanceTransferAvailableTO balanceTransferAvailableTO){
    	StringBuilder sbQuery= new StringBuilder();
    	
    	sbQuery.append("  	SELECT DISTINCT	 ");
    	sbQuery.append("  	CO.OPERATION_NUMBER,	 ");//0
    	sbQuery.append("  	(SELECT LISTAGG(HAD.ID_HOLDER_FK || ' - ' || h.full_name, ', ') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)	 ");
    	sbQuery.append("  	    FROM HOLDER_ACCOUNT_DETAIL HAD	 ");
    	sbQuery.append("  	    INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK	 ");
    	sbQuery.append("  	    WHERE HAD.ID_HOLDER_ACCOUNT_FK = STO.ID_SOURCE_HOLDER_ACCOUNT_FK	 ");
    	sbQuery.append("  	  ) CUI_TITULAR,	 ");//1
    	sbQuery.append("  	STO.ID_SOURCE_HOLDER_ACCOUNT_FK,	 ");//2
    	sbQuery.append("  	STO.TRANSFER_TYPE    	,	 ");//3
    	sbQuery.append("  	(select pt.description from PARAMETER_TABLE pt where pt.PARAMETER_TABLE_PK=STO.TRANSFER_TYPE) as TRANSFERTYPE,	 ");//4
    	sbQuery.append("  	CO.REGISTRY_DATE,	 ");//5
    	//sbQuery.append("  	CO.LAST_MODIFY_DATE,	 ");
    	sbQuery.append("  	STO.ID_SOURCE_PARTICIPANT_FK || ' - ' || P.MNEMONIC  ID_PARTICIPANT_PK_P,	 ");//6
    	//sbQuery.append("  	P.MNEMONIC MNEMONIC_P,	 ");//7
    	sbQuery.append("  	(select HA.ACCOUNT_NUMBER	 ");
    	sbQuery.append("  	from holder_account ha WHERE HA.ID_HOLDER_ACCOUNT_PK=STO.ID_SOURCE_HOLDER_ACCOUNT_FK)ACCOUNT_NUMBER_P,	 "); //8
    	sbQuery.append("  	STO.ID_TARGET_PARTICIPANT_FK || ' - ' || P2.MNEMONIC ID_PARTICIPANT_PK_P2,	 ");//9
    	//sbQuery.append("  	P2.MNEMONIC MNEMONIC_P2,	 ");
    	sbQuery.append("  	(select HA.ACCOUNT_NUMBER	 ");
    	sbQuery.append("  	from holder_account ha WHERE HA.ID_HOLDER_ACCOUNT_PK=STO.ID_TARGET_HOLDER_ACCOUNT_FK)ACCOUNT_NUMBER_P2,	 ");//10
    	sbQuery.append("  	STO.ID_TRANSFER_OPERATION_PK,	 ");//11
    	
    	sbQuery.append("  	STM.MARKET_DATE MARDATE,	 ");//12
    	sbQuery.append("  	STM.MARKET_RATE MARKRATE,	 ");//13
    	sbQuery.append("  	STM.MARKET_PRICE MARKPRICE,	 ");//14
    	//sbQuery.append("  	(SELECT to_char(MARKET_DATE, 'dd/mm/yyyy') FROM SECURITY_TRANSFER_MARKETFACT WHERE ID_TRANSFER_OPERATION_FK = STO.ID_TRANSFER_OPERATION_PK) as MARDATE, 	 ");//12
    	//sbQuery.append("  	(SELECT nvl(MARKET_RATE,0) FROM SECURITY_TRANSFER_MARKETFACT WHERE ID_TRANSFER_OPERATION_FK = STO.ID_TRANSFER_OPERATION_PK) as MARKRATE, 	 ");//13
    	//sbQuery.append("  	(SELECT MARKET_PRICE FROM SECURITY_TRANSFER_MARKETFACT WHERE ID_TRANSFER_OPERATION_FK = STO.ID_TRANSFER_OPERATION_PK) as MARKPRICE, 	 ");//14
    	//sbQuery.append("  	SEC.SECURITY_CLASS,	 ");
    	sbQuery.append("  	SEC.ID_SECURITY_CODE_PK,	 ");//15
    	//sbQuery.append("  	SEC.ALTERNATIVE_CODE,	 ");
    	sbQuery.append("  	STM.QUANTITY_OPERATION,	 ");//16
    	//sbQuery.append("  	STO.STATE,	 ");
    	sbQuery.append("  	(select pt.parameter_name from parameter_table pt where pt.parameter_table_pk= STO.STATE) STATE,	 ");//17
    	sbQuery.append("  	STO.LAST_MODIFY_USER,	 ");//18
    	sbQuery.append("  	SEC.SECURITY_DAYS_TERM	 ");//19
    	sbQuery.append("  	FROM SECURITY_TRANSFER_OPERATION STO	 ");
    	sbQuery.append("  	INNER JOIN CUSTODY_OPERATION CO             ON STO.ID_TRANSFER_OPERATION_PK=CO.ID_CUSTODY_OPERATION_PK	 ");
    	sbQuery.append("  	INNER JOIN SECURITY SEC	 ");
    	sbQuery.append("  	      ON STO.ID_SECURITY_CODE_FK=SEC.ID_SECURITY_CODE_PK	 ");
    	sbQuery.append("  	LEFT JOIN PARTICIPANT P	 ");
    	sbQuery.append("  	      ON STO.ID_SOURCE_PARTICIPANT_FK=P.ID_PARTICIPANT_PK	 ");
    	sbQuery.append("  	LEFT JOIN PARTICIPANT P2	 ");
    	sbQuery.append("  	      ON STO.ID_TARGET_PARTICIPANT_FK=P2.ID_PARTICIPANT_PK	 ");
    	sbQuery.append("    LEFT JOIN SECURITY_TRANSFER_MARKETFACT STM ON STM.ID_TRANSFER_OPERATION_FK = STO.ID_TRANSFER_OPERATION_PK	 ");
    	sbQuery.append("  	WHERE ");
    	sbQuery.append("  	trunc(CO.REGISTRY_DATE) >= to_date(':date_initial','dd/mm/yyyy')	 ");
    	sbQuery.append("  	AND trunc(CO.REGISTRY_DATE) <= to_date(':date_end','dd/mm/yyyy')	 ");
    	sbQuery.append("  	AND STO.IND_BLOCK_TRANSFER = 0 	 "); //DISPONIBLES
    	
		if(Validations.validateIsNotNull(balanceTransferAvailableTO.getParticipantPk())){
			sbQuery.append("  	AND ((STO.ID_SOURCE_PARTICIPANT_FK=:participant_origin)	 ");
	    	sbQuery.append("  	OR (STO.ID_TARGET_PARTICIPANT_FK=:participant_origin))	 ");
		}
		if(Validations.validateIsNotNull(balanceTransferAvailableTO.getIdHolderAccountPk())){
	    	sbQuery.append("  	AND   (EXISTS	 ");
	    	sbQuery.append("  	        (SELECT had.ID_HOLDER_FK	 ");
	    	sbQuery.append("  	         FROM HOLDER_ACCOUNT_DETAIL had	 ");
	    	sbQuery.append("  	         WHERE had.ID_HOLDER_FK = :cui_code	 ");
	    	sbQuery.append("  	           AND had.ID_HOLDER_ACCOUNT_FK = STO.ID_SOURCE_HOLDER_ACCOUNT_FK	 ");
	    	sbQuery.append("  	    ))	 ");
    	}
		if(Validations.validateIsNotNull(balanceTransferAvailableTO.getIssuer())){
			sbQuery.append("  	AND (SEC.ID_ISSUER_FK= ':issuer')	 ");
    	}
		if(Validations.validateIsNotNull(balanceTransferAvailableTO.getCurrency())){
			sbQuery.append("  	AND (SEC.CURRENCY = :currency)	 ");
		}
		if(Validations.validateIsNotNull(balanceTransferAvailableTO.getSecurityClass())){
			sbQuery.append("  	AND (SEC.SECURITY_CLASS = :security_class)	 ");
		}
		if(Validations.validateIsNotNull(balanceTransferAvailableTO.getTransferType())){
			sbQuery.append("  	AND (STO.TRANSFER_TYPE = :transfer_type)	 ");
		}
		if(Validations.validateIsNotNull(balanceTransferAvailableTO.getIdHolderAccountPk())){
	    	sbQuery.append("  	AND (STO.ID_SOURCE_HOLDER_ACCOUNT_FK = holder_account_o)	 ");
		}
		if(Validations.validateIsNotNull(balanceTransferAvailableTO.getSecurity())){
			sbQuery.append("  	AND (SEC.ID_SECURITY_CODE_PK= ':value_series')	 ");
		}
		if(Validations.validateIsNotNull(balanceTransferAvailableTO.getRequestState())){
			sbQuery.append("  	AND (STO.STATE = :request_status)	 ");
		}
		sbQuery.append("  	ORDER BY  STO.TRANSFER_TYPE,CO.OPERATION_NUMBER ");

		String strQueryFormatted = sbQuery.toString();
		
		if(balanceTransferAvailableTO.getParticipantPk()!=null){
    		strQueryFormatted= strQueryFormatted.replace(":participant_origin", balanceTransferAvailableTO.getParticipantPk().toString());
    	}
		if(balanceTransferAvailableTO.getRequestState()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":request_status", balanceTransferAvailableTO.getRequestState().toString());
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(balanceTransferAvailableTO.getIssuer())){
    		strQueryFormatted=strQueryFormatted.replace(":issuer", balanceTransferAvailableTO.getIssuer());
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(balanceTransferAvailableTO.getSecurity())){
    		strQueryFormatted=strQueryFormatted.replace(":value_series", balanceTransferAvailableTO.getSecurity());
    	}
    	if(balanceTransferAvailableTO.getTransferType()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":transfer_type", balanceTransferAvailableTO.getTransferType().toString());
    	}
    	if(balanceTransferAvailableTO.getSecurityClass()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":security_class", balanceTransferAvailableTO.getSecurityClass().toString());
    	}
    	if(balanceTransferAvailableTO.getCurrency()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":currency", balanceTransferAvailableTO.getCurrency().toString());
    	}
    	if(balanceTransferAvailableTO.getIdHolderAccountPk()!=null){ 
    		strQueryFormatted= strQueryFormatted.replace(":holder_account_o", balanceTransferAvailableTO.getIdHolderAccountPk().toString());
    	}
    	if(balanceTransferAvailableTO.getIdHolderPk()!=null){
    		strQueryFormatted= strQueryFormatted.replace(":cui_code", balanceTransferAvailableTO.getIdHolderPk().toString());
    	}
    	
    	strQueryFormatted=strQueryFormatted.replace(":date_initial", balanceTransferAvailableTO.getStrDateInitial());
    	strQueryFormatted=strQueryFormatted.replace(":date_end", balanceTransferAvailableTO.getStrDateEnd());
    	
    	return strQueryFormatted;
    }
    
    /**
     * Query del reporte ACV por emisor
     * @param registrationAssignmentHoldersTO
     * @return
     */
    
    public String getQueryAccountVoluntaryEntryByIssuer(EntriesVoluntaryAccountIssuerTO entriesVoluntaryAccountIssuerTO){
    	StringBuilder sbQuery= new StringBuilder();
    	
    	sbQuery.append("  	SELECT	 ");
    	sbQuery.append("  	    PA.ID_PARTICIPANT_PK || ' - ' || PA.MNEMONIC  PARTICIPANTE,	 ");//0
    	//sbQuery.append("  	    PA.ID_PARTICIPANT_PK,	 ");
    	sbQuery.append("  	    CO.OPERATION_NUMBER NRO_SOLICITUD,	 ");//1
    	sbQuery.append("  	    to_char(CO.REGISTRY_DATE,'dd/mm/yyyy') FECHA_SOLICITUD,	 ");//2
    	sbQuery.append("  	    HA.ID_HOLDER_ACCOUNT_PK,	 ");//3
    	sbQuery.append("  	    HA.ACCOUNT_NUMBER NRO_CUENTA,	 ");//4
    	sbQuery.append("  (SELECT LISTAGG(HAD.ID_HOLDER_FK || ' - ' || h.full_name, ', ') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) 	 ");//5
    	sbQuery.append("     FROM HOLDER_ACCOUNT_DETAIL HAD 	 ");
    	sbQuery.append("     INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK 	 ");
    	sbQuery.append("     WHERE HAD.ID_HOLDER_ACCOUNT_FK = AAO.ID_HOLDER_ACCOUNT_FK) CUI_TITULAR, 	 ");
    	sbQuery.append("  	    (SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK=AAO.MOTIVE) MOTIVO,	 ");//6
    	//sbQuery.append("  	    SE.MNEMONIC CLASE_VALOR,	 ");
    	sbQuery.append("  	    SE.ID_SECURITY_CODE_PK CLAVE_VALOR,	 ");//7
    	sbQuery.append("  	    AAO.CERTIFICATE_NUMBER NRO_TITULO,	 ");//8
    	sbQuery.append("  	    AAO.TOTAL_BALANCE CANTIDAD,	 ");//9
    	sbQuery.append("  	    to_char(CO.LAST_MODIFY_DATE,'dd/mm/yyyy') FECHA,	 ");//10
    	sbQuery.append("  	    (SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK=CO.OPERATION_STATE) ESTADO,	 ");//11
    	sbQuery.append("  	    DECODE(CO.REJECT_MOTIVE_OTHER,NULL,(SELECT PM.DESCRIPTION FROM PARAMETER_TABLE PM WHERE PM.PARAMETER_TABLE_PK = CO.REJECT_MOTIVE),CO.REJECT_MOTIVE_OTHER) OBSERVACIONES	 ");//12

    	sbQuery.append("  	  FROM ACCOUNT_ANNOTATION_OPERATION AAO	 ");
    	sbQuery.append("  	  INNER JOIN CUSTODY_OPERATION CO ON CO.ID_CUSTODY_OPERATION_PK = AAO.ID_ANNOTATION_OPERATION_PK	 ");
    	sbQuery.append("  	  INNER JOIN HOLDER_ACCOUNT HA    ON HA.ID_HOLDER_ACCOUNT_PK = AAO.ID_HOLDER_ACCOUNT_FK	 ");
    	sbQuery.append("  	  INNER JOIN SECURITY SE          ON SE.ID_SECURITY_CODE_PK = AAO.ID_SECURITY_CODE_FK	 ");
    	sbQuery.append("  	  INNER JOIN PARTICIPANT PA       ON PA.ID_PARTICIPANT_PK = HA.ID_PARTICIPANT_FK	 ");
    	
    	sbQuery.append("  	WHERE 1=1	 ");
    	sbQuery.append("  	    AND TRUNC(CO.REGISTRY_DATE) between TO_DATE(':date_initial','dd/mm/yyyy')	 ");
    	sbQuery.append("  	    AND TO_DATE(':date_end','dd/mm/yyyy')	 ");

    	if(Validations.validateIsNotNull(entriesVoluntaryAccountIssuerTO.getIssuer())){
			sbQuery.append("        AND (':issuer' = PA.ID_ISSUER_FK)	 ");
		}
		if(Validations.validateIsNotNull(entriesVoluntaryAccountIssuerTO.getParticipantPk())){
			sbQuery.append("  	    AND (:participant = PA.ID_PARTICIPANT_PK)	 ");
		}
		if(Validations.validateIsNotNull(entriesVoluntaryAccountIssuerTO.getRequestNumber())){
			sbQuery.append("  	    AND (:request_number = CO.OPERATION_NUMBER)	 ");
		}
		if(Validations.validateIsNotNull(entriesVoluntaryAccountIssuerTO.getOperationState())){
			sbQuery.append("  	    AND (:operation_state = CO.OPERATION_STATE)	 ");
		}
		if(Validations.validateIsNotNull(entriesVoluntaryAccountIssuerTO.getIdHolderAccountPk())){
			sbQuery.append("  	    AND (:account_holder = HA.ID_HOLDER_ACCOUNT_PK)	 ");
		}
		if(Validations.validateIsNotNull(entriesVoluntaryAccountIssuerTO.getIdHolderPk())){
			sbQuery.append("  	    AND ((EXISTS (SELECT 1	 ");
	    	sbQuery.append("  	      FROM HOLDER_ACCOUNT_DETAIL had	 ");
	    	sbQuery.append("  	      WHERE had.ID_HOLDER_FK = :cui_holder	 ");
	    	sbQuery.append("  	      AND had.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK	)))	  ");
		}
		if(Validations.validateIsNotNull(entriesVoluntaryAccountIssuerTO.getSecurity())){
			sbQuery.append("  	    AND (:key_value = SE.ID_SECURITY_CODE_PK)	 ");
		}
		if(Validations.validateIsNotNull(entriesVoluntaryAccountIssuerTO.getMotive())){
			sbQuery.append("  	    AND (:reason = AAO.MOTIVE)	 ");
		}
        if(entriesVoluntaryAccountIssuerTO.getInstitutionType().equals(InstitutionType.ISSUER_DPF.getCode())){
        	if(Validations.validateIsNotNull(entriesVoluntaryAccountIssuerTO.getSecurityClass())){
        		sbQuery.append("  	      AND   (:class_security = SE.SECURITY_CLASS)	 ");
        	 }else{
        		 sbQuery.append("  	      AND (SE.SECURITY_CLASS IN (1976,420))	 ");
        	 }
        }else{
        	if(Validations.validateIsNotNull(entriesVoluntaryAccountIssuerTO.getSecurityClass())){
        		 sbQuery.append("              AND (:class_security = SE.SECURITY_CLASS)))	 ");
        	}
        }
		sbQuery.append("  	ORDER BY	 ");
    	sbQuery.append("  	    CASE WHEN :institution_type NOT IN (22,186) THEN PA.MNEMONIC END,	 ");
    	sbQuery.append("  	    CO.OPERATION_NUMBER,	 ");
    	sbQuery.append("  	    CO.REGISTRY_DATE,	 ");
    	sbQuery.append("  	    HA.ID_HOLDER_ACCOUNT_PK	 ");

		String strQueryFormatted = sbQuery.toString();
		
		if(entriesVoluntaryAccountIssuerTO.getIssuer()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":issuer", entriesVoluntaryAccountIssuerTO.getIssuer());
    	}
		if(entriesVoluntaryAccountIssuerTO.getParticipantPk()!=null){
    		strQueryFormatted= strQueryFormatted.replace(":participant", entriesVoluntaryAccountIssuerTO.getParticipantPk().toString());
    	}
		if(entriesVoluntaryAccountIssuerTO.getRequestNumber()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":request_number", entriesVoluntaryAccountIssuerTO.getRequestNumber().toString());
    	}
		if(entriesVoluntaryAccountIssuerTO.getOperationState()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":operation_state", entriesVoluntaryAccountIssuerTO.getOperationState().toString());
    	}
		if(entriesVoluntaryAccountIssuerTO.getIdHolderAccountPk()!=null){
    		strQueryFormatted= strQueryFormatted.replace(":account_holder", entriesVoluntaryAccountIssuerTO.getIdHolderAccountPk().toString());
    	}
		if(entriesVoluntaryAccountIssuerTO.getIdHolderPk()!=null){
    		strQueryFormatted= strQueryFormatted.replace(":cui_holder", entriesVoluntaryAccountIssuerTO.getIdHolderPk().toString());
    	}
		if(Validations.validateIsNotNullAndNotEmpty(entriesVoluntaryAccountIssuerTO.getSecurity())){
    		strQueryFormatted=strQueryFormatted.replace(":key_value", entriesVoluntaryAccountIssuerTO.getSecurity());
    	}
		if(entriesVoluntaryAccountIssuerTO.getMotive()!=null){
    		strQueryFormatted= strQueryFormatted.replace(":reason", entriesVoluntaryAccountIssuerTO.getMotive().toString());
    	}
		if(entriesVoluntaryAccountIssuerTO.getSecurityClass()!=null){
    		strQueryFormatted= strQueryFormatted.replace(":class_security", entriesVoluntaryAccountIssuerTO.getSecurityClass().toString());
    	}
		strQueryFormatted=strQueryFormatted.replace(":institution_type", entriesVoluntaryAccountIssuerTO.getInstitutionType().toString());
    	strQueryFormatted=strQueryFormatted.replace(":date_initial", entriesVoluntaryAccountIssuerTO.getStrDateInitial());
    	strQueryFormatted=strQueryFormatted.replace(":date_end", entriesVoluntaryAccountIssuerTO.getStrDateEnd());
    	
    	return strQueryFormatted;
    }
    
    
    /**
     * DESCRIPCION COMPLETA DEL EMISOR
     * @param issuerPk
     * @return
     */
    public String getIssuerDetails(String issuerPk){
    	
    	StringBuilder querySql = new StringBuilder();
		
		querySql.append("  SELECT ISS.MNEMONIC || ' - ' || ISS.ID_ISSUER_PK || ' - ' || ISS.BUSINESS_NAME	      ");
		querySql.append("  FROM ISSUER ISS                                      							 	  ");
		querySql.append("  WHERE  ISS.ID_ISSUER_PK = :issuerPk                   								  "); //REGISTRADO
		
		Query query = em.createNativeQuery(querySql.toString());
		query.setParameter("issuerPk", issuerPk);
		String mnemonic = (String)query.getSingleResult();
		return mnemonic;
    }
    public Integer searchInstitutionType(LoggerUser loggerUser){
    	StringBuilder querySql = new StringBuilder();
		
		querySql.append(" select INSTITUTION_TYPE_FK from PRADERADEP_SECURITY.user_account	");
		querySql.append(" where 1 = 1 ");
		querySql.append(" and LOGIN_USER = :userName " ); 
		
		Query query = em.createNativeQuery(querySql.toString());
		query.setParameter("userName", loggerUser.getUserName());
		BigDecimal institutionType = (BigDecimal)query.getSingleResult();
		Integer valInt = institutionType.intValue();
		return valInt;
    }
    public String getQueryClientPortafolioDpfSirtex(ClientPortafolioTO clientPortafolioTO, Long idStkCalcProcess){
    	StringBuilder sbQuery= new StringBuilder();
    	
		sbQuery.append(" select	");
  		sbQuery.append(" 	TO_NUMBER(ID_PARTICIPANT_PK) ID_PARTICIPANT_PK, ");
		sbQuery.append(" 	DESCRIPTION, ");
		sbQuery.append(" 	MNEMONIC, ");
		sbQuery.append(" 	TO_NUMBER(CURRENCY) CURRENCY, ");
		sbQuery.append(" 	CURRENCY_MNEMONIC, ");
		sbQuery.append(" 	MNEMONIC_ISSUER MNEMONIC_ISSUER, ");
		sbQuery.append(" 	BUSINESS_NAME BUSINESS_NAME, ");
		sbQuery.append(" 	TO_NUMBER(ID_HOLDER_ACCOUNT_PK) ID_HOLDER_ACCOUNT_PK, ");
		sbQuery.append(" 	HOLDERS HOLDERS, ");
		sbQuery.append(" 	HOLDERSDESC HOLDERSDESC, ");
		sbQuery.append(" 	TO_NUMBER(ACCOUNT_NUMBER) ACCOUNT_NUMBER, ");
		sbQuery.append(" 	TO_NUMBER(SECURITY_CLASS) SECURITY_CLASS, ");
		sbQuery.append(" 	SECURITY_CLASS_MNEMONIC SECURITY_CLASS_MNEMONIC, ");
		sbQuery.append(" 	ID_SECURITY_CODE_PK ID_SECURITY_CODE_PK, ");
		sbQuery.append(" 	FECHAEMISION FECHAEMISION, ");
		sbQuery.append(" 	TO_NUMBER(SECURITY_DAYS_TERM) SECURITY_DAYS_TERM, ");
		sbQuery.append(" 	TO_NUMBER(INITIAL_NOMINAL_VALUE) INITIAL_NOMINAL_VALUE, ");
		sbQuery.append(" 	TO_NUMBER(INTEREST_RATE) INTEREST_RATE, ");
		sbQuery.append(" 	TO_NUMBER(SECURITY_TYPE) SECURITY_TYPE, ");
		sbQuery.append(" 	TO_NUMBER(VALORFINAL) VALORFINAL, ");
		sbQuery.append(" 	TO_NUMBER(DIASVIGENTES) DIASVIGENTES, ");
		sbQuery.append(" 	TO_NUMBER(VALOR_PRESENTE) VALOR_PRESENTE, ");
		//sbQuery.append("    SUM(TOTAL_BALANCE) as TOTAL_BALANCE, ");
		sbQuery.append("    TOTAL_BALANCE, ");
		sbQuery.append(" 	AVAILABLE_BALANCE, ");
		sbQuery.append(" 	PAWN_BALANCE, ");
		sbQuery.append(" 	BAN_BALANCE, ");
		sbQuery.append(" 	OTHER_BLOCK_BALANCE, ");
		sbQuery.append(" 	ACCREDITATION_BALANCE, ");
		sbQuery.append(" 	MARGIN_BALANCE, ");
		sbQuery.append(" 	REPORTING_BALANCE, ");
		sbQuery.append(" 	REPORTED_BALANCE, ");
		sbQuery.append(" 	PURCHASE_BALANCE, ");
		sbQuery.append(" 	SALE_BALANCE, ");
		sbQuery.append(" 	TRANSIT_BALANCE, ");
		sbQuery.append(" 	TASA_MERCADO, ");
		sbQuery.append(" 	PRECIO_MERCADO, ");
		//sbQuery.append(" 	SUM(TOTAL_AMOUNT_MF) as TOTAL_AMOUNT_MF, ");
		sbQuery.append(" 	TOTAL_AMOUNT_MF, ");
		sbQuery.append(" 	THEORIC_RATE, ");
		sbQuery.append(" 	THEORIC_PRICE, ");
		sbQuery.append(" 	TOTAL_AMOUNT, ");
		sbQuery.append(" 	CURRENT_NOMINAL_VALUE, ");
		sbQuery.append(" 	CURRENT_TOTAL_AMOUNT, ");
		
		sbQuery.append(" 	alternative_code, ");
		sbQuery.append(" 	to_char(expiration_date,'dd/mm/yyyy'), ");
		sbQuery.append(" 	holder_type, ");
		sbQuery.append(" 	document_number, ");
		sbQuery.append(" 	serie, ");
		sbQuery.append(" 	FIRST_HOLDER, ");
		sbQuery.append(" 	WITH_COUPONS ");
		
		sbQuery.append(" from ");
		sbQuery.append(" 	(select /*+ USE_NL(ISS,SECU,P,MFV) */ ");
		sbQuery.append(" 	p.id_participant_pk||HA.id_holder_account_pk||secu.id_security_code_pk secuencia, ");
		sbQuery.append(" 	p.id_participant_pk, ");
		sbQuery.append(" 	p.description, ");
		sbQuery.append(" 	p.mnemonic, ");																										 
		sbQuery.append(" 	secu.currency, ");																																				 
		sbQuery.append(" 	(select pt.text1 from parameter_table pt where pt.parameter_table_pk=secu.currency) currency_mnemonic, ");														 
		sbQuery.append(" 	iss.mnemonic as MNEMONIC_ISSUER, ");																															 
		sbQuery.append(" 	iss.business_name, ");																																			 
		sbQuery.append(" 	HA.id_holder_account_pk, ");																																	 
		sbQuery.append(" 	(SELECT LISTAGG(HAD.ID_HOLDER_FK , ',' ||chr(13) ) WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");																			 
		sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD ");																																 
		sbQuery.append(" 	WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ");																									 
		sbQuery.append(" 	) Holders, ");
		sbQuery.append("    (SELECT LISTAGG((select h.full_name from holder h where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK) ||chr(13)) WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
		sbQuery.append("    FROM HOLDER_ACCOUNT_DETAIL HAD	");
		sbQuery.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK " );
		sbQuery.append(" 	) HoldersDesc, ");
		sbQuery.append(" 	HA.account_number, ");
		//
		sbQuery.append(" 	secu.ALTERNATIVE_CODE, ");
		sbQuery.append(" 	secu.expiration_date, ");
		sbQuery.append(" 	(select parameter_name from parameter_table where parameter_table_pk=(SELECT distinct (select h.holder_type from holder h where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK)) as holder_type, ");
		sbQuery.append(" 	(SELECT LISTAGG((select h.document_number from holder h where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK) ||chr(13)) WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)     FROM HOLDER_ACCOUNT_DETAIL HAD	    WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK  	) document_number, ");
		sbQuery.append(" 	secu.id_security_code_only as serie, ");
		
		sbQuery.append(" 	secu.security_class, ");																																	 
		sbQuery.append(" 	(select pt.text1 from parameter_table pt where pt.parameter_table_pk=secu.security_class) security_class_mnemonic, ");											 
		sbQuery.append(" 	secu.id_security_code_pk, ");																																	 
		sbQuery.append(" 	(to_char(secu.issuance_date,'dd/MM/YYYY')) fechaEmision, ");																									 
		sbQuery.append(" 	secu.security_days_term, ");																																	 
		sbQuery.append(" 	secu.initial_nominal_value, ");
		sbQuery.append(" 	secu.interest_rate, ");
		sbQuery.append(" 	secu.security_type, ");

		sbQuery.append("	( (secu.INITIAL_NOMINAL_VALUE) * (  1+(secu.INTEREST_RATE/100)*  ( secu.security_days_term )/ 360 )) as ValorFinal,  ");//as ValorFinal,
		sbQuery.append(" 	GREATEST(0,nvl(trunc(secu.expiration_date) - TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY'),0)) DiasVigentes, ");
		sbQuery.append("	ROUND( ( (secu.INITIAL_NOMINAL_VALUE) * (  1+(secu.INTEREST_RATE/100)*  ( secu.security_days_term )/ 360 )) / (1+(secu.INTEREST_RATE/100) * GREATEST(0,nvl(trunc(secu.expiration_date) -  TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY'),0)) /360 ) ,  2)  VALOR_PRESENTE   ,  ");//as ValorFinal,
		sbQuery.append(" 	MFV.total_balance, "); 
		sbQuery.append(" 	MFV.available_balance, ");
		sbQuery.append(" 	MFV.pawn_balance, "); 
		sbQuery.append(" 	MFV.ban_balance, ");
		sbQuery.append(" 	MFV.other_block_balance, ");
		sbQuery.append(" 	MFV.accreditation_balance, ");
		sbQuery.append(" 	MFV.margin_balance, ");
		sbQuery.append(" 	MFV.reporting_balance, ");
		sbQuery.append(" 	MFV.reported_balance, ");
		sbQuery.append(" 	MFV.purchase_balance, ");
		sbQuery.append(" 	MFV.sale_balance, ");
		sbQuery.append(" 	MFV.transit_balance, ");
		sbQuery.append(" 	NVL(MFV.market_rate,0) TASA_MERCADO, ");
		sbQuery.append(" 	NVL(MFV.market_price,0) PRECIO_MERCADO, ");
		sbQuery.append(" 	NVL(ROUND(MFV.market_price,2),0) * MFV.total_balance as TOTAL_AMOUNT_MF, ");
		sbQuery.append(" 	NVL(MFV.THEORIC_RATE,0) THEORIC_RATE, ");
		sbQuery.append(" 	NVL(MFV.THEORIC_PRICE,0) THEORIC_PRICE, ");
		sbQuery.append(" 	NVL(MFV.THEORIC_PRICE,0) * MFV.total_balance total_amount, ");
		sbQuery.append("    DECODE(secu.security_class,426,ROUND(");
		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
			sbQuery.append("			(SELECT distinct NVH.NOMINAL_VALUE FROM NOMINAL_VALUE_HISTORY NVH WHERE TRUNC(NVH.CUT_DATE) = TRUNC(MFV.CUT_DATE) 			");
			sbQuery.append("			AND secu.ID_SECURITY_CODE_PK = NVH.ID_SECURITY_CODE_FK) 															");
		}else{
			sbQuery.append("			secu.current_nominal_value ");
		}
		sbQuery.append("		  	,4),ROUND( ");
		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
			sbQuery.append("			(SELECT distinct NVH.NOMINAL_VALUE FROM NOMINAL_VALUE_HISTORY NVH WHERE TRUNC(NVH.CUT_DATE) = TRUNC(MFV.CUT_DATE) 			");
			sbQuery.append("			AND secu.ID_SECURITY_CODE_PK = NVH.ID_SECURITY_CODE_FK) 															");
		}else{
			sbQuery.append("			secu.current_nominal_value ");
		}
		sbQuery.append(" 			,2)) ");
		sbQuery.append(" 	as current_nominal_value, ");
		sbQuery.append("    DECODE(secu.security_class,426,ROUND( ");
		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
			sbQuery.append("			(SELECT distinct NVH.NOMINAL_VALUE FROM NOMINAL_VALUE_HISTORY NVH WHERE TRUNC(NVH.CUT_DATE) = TRUNC(MFV.CUT_DATE) 			");
			sbQuery.append("			AND secu.ID_SECURITY_CODE_PK = NVH.ID_SECURITY_CODE_FK) 															");
		}else{
			sbQuery.append("			secu.current_nominal_value ");
		}
		sbQuery.append("		  	,4),ROUND( ");
		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
			sbQuery.append("			(SELECT distinct NVH.NOMINAL_VALUE FROM NOMINAL_VALUE_HISTORY NVH WHERE TRUNC(NVH.CUT_DATE) = TRUNC(MFV.CUT_DATE) 			");
			sbQuery.append("			AND secu.ID_SECURITY_CODE_PK = NVH.ID_SECURITY_CODE_FK) 															");
		}else{
			sbQuery.append("			secu.current_nominal_value ");
		}
		sbQuery.append(" 			,2)) * MFV.total_balance ");
		sbQuery.append(" 	as current_total_amount, ");
		/*Nuevas columnas issue:1164*/
		//106 Y 107 NATURAL Y SOLO 108 JURIDICO
        //sbQuery.append(" (CASE WHEN (select ACCOUNT_TYPE from HOLDER_ACCOUNT WHERE ID_HOLDER_ACCOUNT_FK = AAO.ID_HOLDER_ACCOUNT_FK AND ROWNUM <= 1 ) = 107  THEN 'JURIDICO' ELSE 'NATURAL'  END ) AS FIRST_HOLDER, ");
        sbQuery.append(" (SELECT PARAMETER_NAME FROM PARAMETER_TABLE WHERE ");
		sbQuery.append("  PARAMETER_TABLE_PK= (SELECT HHH.HOLDER_TYPE FROM HOLDER HHH WHERE HHH.ID_HOLDER_PK");
		sbQuery.append("  IN (SELECT ID_HOLDER_FK FROM HOLDER_ACCOUNT_DETAIL WHERE ");
		sbQuery.append("  ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK  AND ROWNUM <= 1 )))AS FIRST_HOLDER, ");
		
		sbQuery.append("  (CASE WHEN secu.NUMBER_COUPONS = 1 THEN 'NO' ELSE 'SI' END) as WITH_COUPONS ");
		
		sbQuery.append(" From holder_account ha ");
		//sbQuery.append(" INNER JOIN HOLDER_ACCOUNT_OPERATION HAO ON HAO.ID_HOLDER_ACCOUNT_FK = HA.id_holder_account_pk 					");
		sbQuery.append(" inner join market_fact_view mfv on mfv.id_holder_account_pk = HA.id_holder_account_pk							");
		sbQuery.append(" Inner join participant p	ON mfv.id_participant_pk = p.id_participant_pk									  	");
		sbQuery.append(" Inner join security secu ON mfv.id_security_code_pk	= secu.id_security_code_pk  							");
		sbQuery.append(" Inner join issuer iss 	ON secu.id_issuer_fk				= iss.id_issuer_pk 									");
		//sbQuery.append(" INNER JOIN holder_marketfact_balance HMB ON secu.id_security_code_pk = HMB.id_security_code_fk ");
		sbQuery.append(" INNER JOIN security_negotiation_mechanism SNM ON SECU.id_security_code_pk = SNM.id_security_code_fk ");
		//sbQuery.append(" inner join ACCOUNT_ANNOTATION_OPERATION AAO ON AAO.ID_SECURITY_CODE_FK = secu.ID_SECURITY_CODE_PK ");
		
		sbQuery.append(" WHERE 1 = 1	");
		//sbQuery.append(" AND HMB.IND_ACTIVE = 1 ");
		sbQuery.append(" AND SNM.SECURITY_NEGOTATION_STATE =  1 ");
		//sbQuery.append(" AND MFV.total_balance > 0 ");
		sbQuery.append(" AND (MFV.total_balance > 0 or MFV.reported_balance > 0) ");
		
		//Mecanismo 5 no existe datos solo con 1 y 3
//		sbQuery.append(" AND SNM.ID_NEGOTIATION_MECHANISM_FK = 5 ");
		
		sbQuery.append(" AND SECU.SECURITY_CLASS = 420 ");
		sbQuery.append(" AND SNM.ID_NEGOTIATION_MODALITY_FK IN (1,3,22) ");
		sbQuery.append(" AND SECU.SECURITY_TYPE IN (1762,1763) ");
				
		sbQuery.append(" AND MFV.CUT_DATE >= TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') ");
		sbQuery.append(" AND MFV.CUT_DATE < TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 ");
		sbQuery.append(" AND TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY') ");
		
		if(clientPortafolioTO.getIsParticipantInvestor()){ 
			sbQuery.append(" 	AND (EXISTS																						    												");
			sbQuery.append(" 		(SELECT 1																																		");
			sbQuery.append(" 		FROM HOLDER_ACCOUNT_DETAIL HAD																													");
			sbQuery.append(" 		INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK																						");
			sbQuery.append(" 		WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.id_holder_account_pk																						");
			sbQuery.append(" 			AND H.DOCUMENT_TYPE = p.DOCUMENT_TYPE																										");
			sbQuery.append(" 			AND H.DOCUMENT_NUMBER = p.DOCUMENT_NUMBER																									");
			sbQuery.append(" 	))																																					");
		}
			
		
		if(Validations.validateIsNotNull(clientPortafolioTO.getIdParticipant())){
			sbQuery.append(" and p.id_participant_pk= :participantPk																												");
		}
		if(Validations.validateIsNotNull(clientPortafolioTO.getHolderAccountPk())){
    		sbQuery.append(" and HA.id_holder_account_pk=:holder_account																											");
    	}
		if(Validations.validateIsNotNull(clientPortafolioTO.getIdSecurityCode())){
    		sbQuery.append(" and secu.id_security_code_pk= :securityPk																												");
    	}
		
		if(Validations.validateIsNotNull(clientPortafolioTO.getCui())){
			sbQuery.append(" AND (EXISTS																																			");
			sbQuery.append(" 	(SELECT had.ID_HOLDER_FK																															");
			sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL had																														");
			sbQuery.append(" 	WHERE had.ID_HOLDER_FK = :cui_code																													");
			sbQuery.append(" 		AND had.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK))																						");
	    }
		if(Validations.validateIsNotNull(clientPortafolioTO.getIdIssuer())){
    		sbQuery.append(" and iss.id_issuer_pk=:issuerPk");
    	}
		if(Validations.validateIsNotNull(clientPortafolioTO.getIdCurrency())){
    		sbQuery.append(" and secu.currency=:currency");
    	}
		sbQuery.append(" )T ");
		
		sbQuery.append(" GROUP BY  ");
		sbQuery.append(" ID_PARTICIPANT_PK,DESCRIPTION, mnemonic, CURRENCY, CURRENCY_MNEMONIC, MNEMONIC_ISSUER, ");
		sbQuery.append(" BUSINESS_NAME, ID_HOLDER_ACCOUNT_PK, HOLDERS, HOLDERSDESC, ACCOUNT_NUMBER, SECURITY_CLASS, SECURITY_CLASS_MNEMONIC, ");
		sbQuery.append(" ID_SECURITY_CODE_PK, FECHAEMISION, SECURITY_DAYS_TERM,INITIAL_NOMINAL_VALUE, INTEREST_RATE,SECURITY_TYPE,VALORFINAL,  ");
		sbQuery.append(" DIASVIGENTES, VALOR_PRESENTE,TOTAL_BALANCE, AVAILABLE_BALANCE, PAWN_BALANCE,BAN_BALANCE,OTHER_BLOCK_BALANCE, ");
		sbQuery.append(" ACCREDITATION_BALANCE,MARGIN_BALANCE,REPORTING_BALANCE,REPORTED_BALANCE,PURCHASE_BALANCE,SALE_BALANCE,TRANSIT_BALANCE, ");
		sbQuery.append(" TASA_MERCADO,PRECIO_MERCADO,TOTAL_AMOUNT_MF,THEORIC_RATE,THEORIC_PRICE,TOTAL_AMOUNT,CURRENT_NOMINAL_VALUE,CURRENT_TOTAL_AMOUNT, ");
		sbQuery.append(" ALTERNATIVE_CODE,expiration_date,HOLDER_TYPE,DOCUMENT_NUMBER,SERIE,FIRST_HOLDER,WITH_COUPONS ");
		
		sbQuery.append(" order by ");
		
		sbQuery.append(" ID_PARTICIPANT_PK,DESCRIPTION, mnemonic, CURRENCY, CURRENCY_MNEMONIC, MNEMONIC_ISSUER, ");
		sbQuery.append(" BUSINESS_NAME, ID_HOLDER_ACCOUNT_PK, HOLDERS, HOLDERSDESC, ACCOUNT_NUMBER, SECURITY_CLASS, SECURITY_CLASS_MNEMONIC, ");
		sbQuery.append(" ID_SECURITY_CODE_PK, FECHAEMISION, SECURITY_DAYS_TERM,INITIAL_NOMINAL_VALUE, INTEREST_RATE,SECURITY_TYPE,VALORFINAL,  ");
		sbQuery.append(" DIASVIGENTES, VALOR_PRESENTE,TOTAL_BALANCE, AVAILABLE_BALANCE, PAWN_BALANCE,BAN_BALANCE,OTHER_BLOCK_BALANCE, ");
		sbQuery.append(" ACCREDITATION_BALANCE,MARGIN_BALANCE,REPORTING_BALANCE,REPORTED_BALANCE,PURCHASE_BALANCE,SALE_BALANCE,TRANSIT_BALANCE, ");
		sbQuery.append(" TASA_MERCADO,PRECIO_MERCADO,TOTAL_AMOUNT_MF,THEORIC_RATE,THEORIC_PRICE,TOTAL_AMOUNT,CURRENT_NOMINAL_VALUE,CURRENT_TOTAL_AMOUNT, ");
		sbQuery.append(" ALTERNATIVE_CODE,expiration_date,HOLDER_TYPE,DOCUMENT_NUMBER,SERIE,FIRST_HOLDER,WITH_COUPONS ");
		
		String strQueryFormatted = sbQuery.toString();
		
		if(clientPortafolioTO.getIdParticipant()!=null){
    		strQueryFormatted= strQueryFormatted.replace(":participantPk", clientPortafolioTO.getIdParticipant().toString());
    	}
    	if(clientPortafolioTO.getHolderAccountPk()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":holder_account", clientPortafolioTO.getHolderAccountPk().toString());
    	}
    	if(clientPortafolioTO.getIdSecurityCode()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":securityPk", "'"+clientPortafolioTO.getIdSecurityCode()+"'");
    	}
    	
    	if(Validations.validateIsNotNullAndPositive(clientPortafolioTO.getCui())){
    		strQueryFormatted=strQueryFormatted.replace(":cui_code", clientPortafolioTO.getCui().toString());
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(clientPortafolioTO.getSecurityClass())){
    		strQueryFormatted=strQueryFormatted.replace(":security_class", clientPortafolioTO.getSecurityClass());
    	}
    	if(clientPortafolioTO.getIdIssuer()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":issuerPk", "'"+clientPortafolioTO.getIdIssuer()+"'");
    	}
    	if(clientPortafolioTO.getIdCurrency()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":currency", clientPortafolioTO.getIdCurrency().toString());
    	}
    	return strQueryFormatted;
    }
    
    /**
     * 
     * @param clientPortafolioTO
     * @param idStkCalcProcess
     * @return
     */
    /*cartera desmaterializada*/
    public String getSubTotalQueryClientPortafolioDpfSirtex(ClientPortafolioTO clientPortafolioTO, Long idStkCalcProcess){
    	StringBuilder sbQuery= new StringBuilder();

		sbQuery.append(" SELECT 																															");
		sbQuery.append(" 	(SELECT PARAMETER_NAME FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = S.CURRENCY) CURRENCY,									");   
		sbQuery.append(" 	NVL(SUM(MFV.TOTAL_BALANCE),0) TOTAL_VALORES,																					");   
		sbQuery.append(" 	NVL(SUM(																														");    
		sbQuery.append(" 		S.INITIAL_NOMINAL_VALUE * MFV.TOTAL_BALANCE *																				");      
		sbQuery.append(" 		CASE																														");     
		sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1																							");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304,430) THEN 1 *																				"); 
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1)										");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1853) THEN 1 *																						"); 
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)												");
		sbQuery.append(" 			ELSE 1																													");   
		sbQuery.append(" 	END),0) TOTAL_BOB,																												");    
		sbQuery.append("	NVL(SUM(																														"); 
		sbQuery.append(" 		S.INITIAL_NOMINAL_VALUE * MFV.TOTAL_BALANCE *																				");
		sbQuery.append(" 		CASE																														");
		sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1 /																							");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)												");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304) THEN 1 *																					");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /										");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)												");
		sbQuery.append(" 			WHEN S.CURRENCY IN (430,1853) THEN 1																					");
		sbQuery.append(" 			ELSE 1																													");
		sbQuery.append(" 	END),0) TOTAL_USD,																												");
		
		sbQuery.append(" 	NVL(SUM(																														");
		sbQuery.append(" 		ROUND(NVL(MFV.market_price,0),2) * MFV.total_balance *																		");
		sbQuery.append(" 		CASE																														");
		sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1																							");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304,430) THEN 1 *																				");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1)										");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1853) THEN 1 *																						");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)												");
		sbQuery.append(" 			ELSE 1																													");
		sbQuery.append(" 		END),0) TOTAL_PRECIO_MRKT_BOB,																								");
		sbQuery.append(" 	NVL(SUM(																														");
		sbQuery.append(" 		ROUND(NVL(MFV.market_price,0),2) * MFV.total_balance *																		");
		sbQuery.append(" 		CASE																														");
		sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1 /																							");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)												");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304) THEN 1 *																					");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /										");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 					AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)													");
		sbQuery.append(" 			WHEN S.CURRENCY IN (430,1853) THEN 1																					");
		sbQuery.append(" 			ELSE 1																													");
		sbQuery.append(" 		END),0) TOTAL_PRECIO_MRKT_USD,																								");
		
		sbQuery.append(" 	NVL(SUM(																														");    
		sbQuery.append(" 		DECODE(S.SECURITY_CLASS,426,ROUND( 																							");
		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
			sbQuery.append("			(SELECT distinct NVH.NOMINAL_VALUE FROM NOMINAL_VALUE_HISTORY NVH WHERE NVH.CUT_DATE = MFV.CUT_DATE     			");
			
			sbQuery.append("			AND S.ID_SECURITY_CODE_PK = NVH.ID_SECURITY_CODE_FK) 																");
		}else{
			sbQuery.append("			S.current_nominal_value									 															");
		}
		sbQuery.append("		,4),ROUND( 																													");
		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
			sbQuery.append("			(SELECT distinct NVH.NOMINAL_VALUE FROM NOMINAL_VALUE_HISTORY NVH WHERE NVH.CUT_DATE = MFV.CUT_DATE 			");
			
			sbQuery.append("			AND S.ID_SECURITY_CODE_PK = NVH.ID_SECURITY_CODE_FK) 																");
		}else{
			sbQuery.append("			S.current_nominal_value									 															");
		}
		sbQuery.append("		,2)) * MFV.TOTAL_BALANCE *																									");      
		sbQuery.append(" 		CASE																														");     
		sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1																							");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304,430) THEN 1 *																				"); 
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1)										");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1853) THEN 1 *																						"); 
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)												");
		sbQuery.append(" 			ELSE 1																													");   
		sbQuery.append(" 	END),0) TOTAL_CURRENT_NOMINAL_BOB,																								");    
		sbQuery.append(" 	NVL(SUM(																														");    
		sbQuery.append(" 		DECODE(S.SECURITY_CLASS,426,ROUND( 																							");
		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
			sbQuery.append("			(SELECT distinct NVH.NOMINAL_VALUE FROM NOMINAL_VALUE_HISTORY NVH WHERE NVH.CUT_DATE = MFV.CUT_DATE     			");
			sbQuery.append("			AND S.ID_SECURITY_CODE_PK = NVH.ID_SECURITY_CODE_FK) 																");
		}else{
			sbQuery.append("			S.current_nominal_value									 															");
		}
		sbQuery.append("		,4),ROUND( 																													");
		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
			sbQuery.append("			(SELECT distinct NVH.NOMINAL_VALUE FROM NOMINAL_VALUE_HISTORY NVH WHERE NVH.CUT_DATE = MFV.CUT_DATE     			");
			
			sbQuery.append("			AND S.ID_SECURITY_CODE_PK = NVH.ID_SECURITY_CODE_FK) 																");
		}else{
			sbQuery.append("			S.current_nominal_value									 															");
		}
		sbQuery.append("		,2)) * MFV.TOTAL_BALANCE *																									");
		sbQuery.append(" 		CASE																														");
		sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1 /																							");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)												");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304) THEN 1 *																					");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");		
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /										");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE                                                                                               			");
		sbQuery.append("                    D.DATE_RATE >= TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') AND     ");
		sbQuery.append("                    D.DATE_RATE < TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 AND  ");
		sbQuery.append("                    TRUNC (TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE ('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY')");
		
		sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)												");
		sbQuery.append(" 			WHEN S.CURRENCY IN (430,1853) THEN 1																					");
		sbQuery.append(" 			ELSE 1																													");
		sbQuery.append(" 	END),0) TOTAL_CURRENT_NOMINAL_USD																								");
		
		sbQuery.append(" FROM MARKET_FACT_VIEW MFV ");
		sbQuery.append(" INNER JOIN PARTICIPANT P ON P.ID_PARTICIPANT_PK = MFV.ID_PARTICIPANT_PK ");
		sbQuery.append(" INNER JOIN SECURITY S ON S.ID_SECURITY_CODE_PK = MFV.ID_SECURITY_CODE_PK ");
		sbQuery.append(" INNER JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK = MFV.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append(" INNER JOIN ISSUER ISU ON ISU.ID_ISSUER_PK = S.ID_ISSUER_FK ");
		sbQuery.append(" WHERE 1 = 1 ");
		sbQuery.append(" AND S.SECURITY_TYPE IN (1762,1763) ");
		sbQuery.append(" AND MFV.CUT_DATE >= TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') ");
		sbQuery.append(" AND MFV.CUT_DATE < TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 ");
		sbQuery.append(" AND TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY') ");
		sbQuery.append(" AND (MFV.TOTAL_BALANCE > 0 OR MFV.REPORTED_BALANCE > 0) ");
		/**Filtros*/
		if(Validations.validateIsNotNull(clientPortafolioTO.getIdParticipant()))
			sbQuery.append("	and MFV.ID_PARTICIPANT_PK = :participantPk																					");
		
		if(Validations.validateIsNotNull(clientPortafolioTO.getHolderAccountPk()))
    		sbQuery.append(" 	and HA.ID_HOLDER_ACCOUNT_PK =:holder_account																				");
		
		if(Validations.validateIsNotNull(clientPortafolioTO.getIdSecurityCode()))
    		sbQuery.append(" 	and S.ID_SECURITY_CODE_PK = :securityPk																						");
		
		if(Validations.validateIsNotNull(clientPortafolioTO.getCui())){
			sbQuery.append(" 	AND (EXISTS																													");
			sbQuery.append(" 		(SELECT had.ID_HOLDER_FK																								");
			sbQuery.append(" 		FROM HOLDER_ACCOUNT_DETAIL had																							");
			sbQuery.append(" 		WHERE had.ID_HOLDER_FK = :cui_code																						");
			sbQuery.append(" 			AND had.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK))															");
	    }
		
		if(Validations.validateIsNotNull(clientPortafolioTO.getIdIssuer()))
    		sbQuery.append(" 	and ISU.ID_ISSUER_PK =:issuerPk																							");
		
		if(Validations.validateIsNotNull(clientPortafolioTO.getIdCurrency()))
    		sbQuery.append(" 	and S.CURRENCY =:currency																							");
		
		if(Validations.validateIsNotNull(clientPortafolioTO.getIdIssuer())){
    		sbQuery.append(" 	and ISU.ID_ISSUER_PK =:issuerPk																								");
    	}
		/**Fin filtros*/
	
		//sbQuery.append(" ) ");

		sbQuery.append(" AND S.ID_SECURITY_CODE_PK IN ( ");
		
		sbQuery.append(" SELECT distinct(SNM.ID_SECURITY_CODE_FK) ");
		sbQuery.append(" FROM SECURITY_NEGOTIATION_MECHANISM SNM ");
		sbQuery.append(" INNER JOIN MARKET_FACT_VIEW MFV ON MFV.ID_SECURITY_CODE_PK = SNM.ID_SECURITY_CODE_FK ");
		sbQuery.append(" INNER JOIN SECURITY s ON s.ID_SECURITY_CODE_PK = SNM.ID_SECURITY_CODE_FK ");
		sbQuery.append(" INNER JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK = MFV.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append(" INNER JOIN ISSUER ISU ON ISU.ID_ISSUER_PK = S.ID_ISSUER_FK ");
		sbQuery.append(" WHERE 1 = 1 ");
		sbQuery.append(" AND S.SECURITY_TYPE IN (1762,1763) ");
		sbQuery.append(" AND MFV.CUT_DATE >= TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') ");
		sbQuery.append(" AND MFV.CUT_DATE < TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 ");
		sbQuery.append(" AND TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY') ");
		sbQuery.append(" AND (MFV.TOTAL_BALANCE > 0) ");
		sbQuery.append(" AND SNM.ID_NEGOTIATION_MECHANISM_FK = 5 ");
		sbQuery.append(" AND SNM.SECURITY_NEGOTATION_STATE = 1 ");
		sbQuery.append(" AND SNM.ID_NEGOTIATION_MODALITY_FK IN (1,3,22) ");
		sbQuery.append(" AND S.SECURITY_CLASS = 420 ");
		
		sbQuery.append(" ) ");
		
		sbQuery.append(" GROUP BY S.CURRENCY ");
		sbQuery.append(" ORDER BY S.CURRENCY ");
		
		String strQueryFormatted = sbQuery.toString();
    	
		if(clientPortafolioTO.getIdParticipant()!=null)
    		strQueryFormatted= strQueryFormatted.replace(":participantPk", clientPortafolioTO.getIdParticipant().toString());
		
		if(clientPortafolioTO.getHolderAccountPk()!=null)
    		strQueryFormatted=strQueryFormatted.replace(":holder_account", clientPortafolioTO.getHolderAccountPk().toString());
    	
		if(clientPortafolioTO.getIdSecurityCode()!=null)
    		strQueryFormatted=strQueryFormatted.replace(":securityPk", "'"+clientPortafolioTO.getIdSecurityCode()+"'");
    	
		if(Validations.validateIsNotNullAndPositive(clientPortafolioTO.getCui()))
    		strQueryFormatted=strQueryFormatted.replace(":cui_code", clientPortafolioTO.getCui().toString());
    	
		if(clientPortafolioTO.getIdIssuer()!=null)
    		strQueryFormatted=strQueryFormatted.replace(":issuerPk", "'"+clientPortafolioTO.getIdIssuer()+"'");
    	
		if(clientPortafolioTO.getIdCurrency()!=null)
    		strQueryFormatted=strQueryFormatted.replace(":currency", clientPortafolioTO.getIdCurrency().toString());

    	return strQueryFormatted;
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void deleteRegistreReport(Long idReportLoggerPk){
		StringBuilder querySql = new StringBuilder();
		querySql.append("  delete REPORT_LOGGER_USER where ID_REPORT_LOGGER_FK = "+idReportLoggerPk);
		Query query = em.createNativeQuery(querySql.toString());
		query.executeUpdate();
	}
   
    

	public List<XmlOperationsSirtex> getQueryClientOperationsDpfSirtexByParticipant(
			ClientPortafolioTO clientPortafolioTO, boolean indExcel) {
		XmlOperationsSirtex operationsSirtex = null;
		List<XmlOperationsSirtex> listReturn = new ArrayList<>();
		try {
			List<SirtexOperationResultTO> list = getSirtexOperationsServiceBean(
					clientPortafolioTO, indExcel);
			operationsSirtex = new XmlOperationsSirtex();
			Long idConsult = getCorrelativeConsultOpSirtex();
			operationsSirtex.setConsultNumber(idConsult.toString());
			operationsSirtex.setOperation("CSX");
			// (CommonsUtilities.convertDateToString(clientPortafolioTO.getDateMarketFact(),CommonsUtilities.DATE_PATTERN));
			operationsSirtex.setOperationDate(CommonsUtilities
					.convertDateToString(CommonsUtilities.currentDate(),
							CommonsUtilities.DATE_PATTERN));
			List<XmlSeccSirtex> sirtexs = new ArrayList<>();
			for (SirtexOperationResultTO resultTO : list) {
				XmlSeccSirtex seccSirtex = new XmlSeccSirtex();
				seccSirtex.setNumberOperationSirtex(resultTO
						.getIdMechanismOperationRequestPk().toString());
				// seccSirtex.setDateOperationSirtex(CommonsUtilities.convertDateToString(resultTO.getSettlementTermDate(),CommonsUtilities.DATE_PATTERN));
				seccSirtex.setDateOperationSirtex(CommonsUtilities
						.convertDateToString(
								clientPortafolioTO.getDateMarketFact(),
								CommonsUtilities.DATE_PATTERN));
				seccSirtex.setSellerParticipantCode(resultTO
						.getMnemoPartSeller());
				seccSirtex.setSecurityKey(resultTO.getSecurityCode());
				seccSirtex.setIssuanceDate(CommonsUtilities
						.convertDateToString(resultTO.getIssuanceDate(),
								CommonsUtilities.DATE_PATTERN));
				seccSirtex.setExpirationDate(CommonsUtilities
						.convertDateToString(resultTO.getExperitationDate(),
								CommonsUtilities.DATE_PATTERN));
				if (indExcel) {
					seccSirtex.setExchangeAgency(resultTO.getExchangeAgency());
					seccSirtex.setClient(resultTO.getClient());
				}
				if (resultTO.getModalityName().equals(COMPRA_VENTA_RF))
					seccSirtex.setModality("1");

				if (resultTO.getModalityName().equals(REPORTO_RF))
					seccSirtex.setModality("3");

				if (resultTO.getModalityName().equals(REPORTO_REVERSO_RF))
					seccSirtex.setModality("22");

				seccSirtex.setRate(resultTO.getRateExchange()
						.setScale(2, BigDecimal.ROUND_FLOOR).toString());
				/** Plazo */
				seccSirtex.setTerm(String.valueOf(resultTO
						.getTermSettlementDays()));
				seccSirtex.setQuantityOffere(String.valueOf(resultTO
						.getQuantityOffere().toString()));
				// if(resultTO.getAcceptedAmount()!=null){
				// Integer acept = resultTO.getAcceptedAmount().intValue();
				// if(acept>0)
				seccSirtex.setAcceptedAmount("0");
				// }

				if (indExcel)
					seccSirtex.setStockQuantity(resultTO.getStockQuantity());

				/** Si es DPF, solo adicionamos estos valores */
				// if(indExcel)
				if (resultTO.getSecurityClass().equals(
						SecurityClassType.DPF.getCode()))
					seccSirtex.setSerieF(resultTO.getSecurityCode());

				seccSirtex.setInitialNominalValue(String.valueOf(resultTO
						.getNominalValue().toString()));
				seccSirtex.setFinalNominalValue(String.valueOf(resultTO
						.getFinalValue().toString()));
				// seccSirtex.setSerieF(resultTO.getIdSecurityCodePkBcb());
				/** Plazo restante */
				seccSirtex.setRemainingTerm(String.valueOf(resultTO
						.getRemainingTerm().intValue()));
				if (!resultTO.getSecurityClass().equals(
						SecurityClassType.DPF.getCode())) {
					seccSirtex.setSerieF(resultTO.getIdSecurityCodePkBcb());
				} else
					seccSirtex.setSerieF(resultTO.getSecurityCode());
				if (indExcel)
					seccSirtex.setFinalNominalValue(String.valueOf(resultTO
							.getFinalValue().toString()));

				if (!resultTO.getSecurityClass().equals(
						SecurityClassType.DPF.getCode())) {
					List<XmlCoupon> listCoupons = seachXmlCoupunList(
							resultTO.getIdMechanismOperationRequestPk(),
							resultTO.getSecurityCode(),
							CommonsUtilities.currentDate(),
							seccSirtex.getQuantityOffere());
					XmlListSeccSirtex listCoponsSecc = new XmlListSeccSirtex();
					listCoponsSecc.setXmlCoupons(listCoupons);
					seccSirtex.setListCoupons(listCoponsSecc);
				} else {

					com.pradera.model.issuancesecuritie.Security security = super
							.find(com.pradera.model.issuancesecuritie.Security.class,
									resultTO.getSecurityCode());
					if (security.getNumberCoupons() == 1) {
						seccSirtex.setSerieF(resultTO.getSecurityCode());
						List<XmlCoupon> listCoupons = new ArrayList<>();
						XmlListSeccSirtex listCoponsSecc = new XmlListSeccSirtex();
						listCoponsSecc.setXmlCoupons(listCoupons);
						seccSirtex.setListCoupons(listCoponsSecc);
					} else {
						List<XmlCoupon> listCoupons = seachXmlCoupunList(resultTO.getIdMechanismOperationRequestPk(),resultTO.getSecurityCode(),CommonsUtilities.currentDate(),seccSirtex.getQuantityOffere());
						XmlListSeccSirtex listCoponsSecc = new XmlListSeccSirtex();
						listCoponsSecc.setXmlCoupons(listCoupons);
						seccSirtex.setListCoupons(listCoponsSecc);
					}

				}
				sirtexs.add(seccSirtex);
			}
			XmlRegSirtex regSirtex = new XmlRegSirtex();
			regSirtex.setSeccSirtexs(sirtexs);
			List<XmlRegSirtex> listSirtexs = new ArrayList<>();
			listSirtexs.add(regSirtex);
			operationsSirtex.setRegSirtexs(listSirtexs);
			listReturn.add(operationsSirtex);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listReturn;
	}
    
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Long getCorrelativeConsultOpSirtex(){
		final String SEQUENCE = "SQ_CORRELATIVE_SIRTEX_CONSULT";
		return super.getNextCorrelativeOperations(SEQUENCE);
	}
    /**
     * Busqueda de la operacion SIRTEX
     *  */
	@SuppressWarnings("unchecked")
	public List<SirtexOperationResultTO> getSirtexOperationsServiceBean(ClientPortafolioTO clientPortafolioTO, boolean indExcel) throws ServiceException {

		StringBuilder querySql = new StringBuilder();
		
		querySql.append("     SELECT                                                                                               ");	
		querySql.append("      DISTINCT TREQ.ID_TRADE_REQUEST_PK,                                                                  ");	
		querySql.append("      MOPE.ID_NEGOTIATION_MODALITY_FK,                                                                    ");
		querySql.append("      (select MODALITY_NAME from NEGOTIATION_MODALITY                                                     ");
		querySql.append("      where ID_NEGOTIATION_MODALITY_PK = MOPE.ID_NEGOTIATION_MODALITY_FK ) MODALITY_NAME,                 ");
		querySql.append("      TREQ.OPERATION_DATE,                                                                                ");	
		querySql.append("      TREQ.ID_SELLER_PARTICIPANT_FK ,                                                                     ");	
		querySql.append("      PAR1.MNEMONIC ,                                                                                     ");	
		querySql.append("      TREQ.ID_BUYER_PARTICIPANT_FK,                                                                       ");	
		querySql.append("      PAR2.MNEMONIC PAR2MNEMONIC,                                                                         ");	
		querySql.append("      TREQ.CASH_SETTLEMENT_DATE ,                                                                         ");	
		querySql.append("      TREQ.TERM_SETTLEMENT_DATE ,                                                                         ");	
		querySql.append("      TREQ.ID_SECURITY_CODE_FK ,                                                                          ");	
		querySql.append("      TREQ.OPERATION_STATE ,                                                                              ");	
		querySql.append("      TREQ.REGISTRY_USER ,                                                                                ");	
		//querySql.append("      TREQ.STOCK_QUANTITY ,                                                                               ");	
		querySql.append("      ATR.STOCK_QUANTITY ,                                                                                ");	
		//querySql.append("      MOPE.TERM_SETTLEMENT_DAYS ,                                                                         ");
		//querySql.append("      trunc(SEC.EXPIRATION_DATE -SEC.ISSUANCE_DATE) as PLAZO, ");
		querySql.append("      SEC.SECURITY_DAYS_TERM as PLAZO, "); 
		querySql.append("      MOPE.TERM_SETTLEMENT_DATE MOPEDATE_TERM,                                                             ");	
		querySql.append("      SEC.ISSUANCE_DATE ISSUANCE_DATE, ");
		querySql.append("      SEC.EXPIRATION_DATE EXPIRATION_DATE, ");
		querySql.append("      SEC.INTEREST_RATE INTEREST_RATE, ");
		querySql.append("      (select SUPPLIED_QUANTITY from COUPON_TRADE_REQUEST where COUPON_NUMBER = 0 AND IND_IS_CAPITAL = 0 AND ID_TRADE_REQUEST_FK = TREQ.ID_TRADE_REQUEST_PK ) CANT_OFERTADA, ");
		querySql.append("      (select ACCEPTED_QUANTITY from COUPON_TRADE_REQUEST where COUPON_NUMBER = 0 AND IND_IS_CAPITAL = 0 AND ID_TRADE_REQUEST_FK = TREQ.ID_TRADE_REQUEST_PK ) CANT_ACEPTADA, ");
		querySql.append("      decode(0, 1, SEC.ID_SECURITY_CODE_PK,NVL(SEC.ID_SECURITY_BCB_CODE, '('||SUBSTR(SEC.ID_SECURITY_CODE_PK, 0, 2)||')'||SUBSTR(SEC.ID_SECURITY_CODE_PK, 5)||'-000')) BCB_CODE, ");
		querySql.append("      MOPE.NOMINAL_VALUE NOMINAL_VALUE, ");
		//querySql.append("      SEC.CURRENT_NOMINAL_VALUE+SEC.INTEREST_RATE as VALOR_FINAL, ");
		querySql.append("      ((SEC.INITIAL_NOMINAL_VALUE) * (1+(SEC.INTEREST_RATE/100)* (SEC.security_days_term)/ 360)) AS VALOR_FINAL, ");
		//querySql.append("      trunc(SEC.EXPIRATION_DATE-(SELECT SYSDATE FROM DUAL)) as PLAZO_RESTANTE, ");
		querySql.append("      GREATEST(0, nvl(trunc(sec.expiration_date) - TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 0)) as PLAZO_RESTANTE, ");
		querySql.append("      SEC.SECURITY_CLASS SECURITY_CLASS ");
		//PARTICIPANT_SELL COLUMNA AGENCIA_BOLSA
		querySql.append("      ,  PAR1.MNEMONIC PARTICIPANT_SELL ,");
		querySql.append("       (SELECT LISTAGG((select h.full_name from holder h where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK) ||chr(13)) WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)	");
		querySql.append("       FROM HOLDER_ACCOUNT_DETAIL HAD		");
		querySql.append("       WHERE HAD.ID_HOLDER_ACCOUNT_FK = HAO.ID_HOLDER_ACCOUNT_FK	");
		querySql.append("      ) HOLDERSDESC	");
		//HOLDER_DESC COLUMNA CLIENTE
		
		querySql.append("  FROM   MECHANISM_OPERATION MOPE                                                                         ");
		
		querySql.append("  INNER JOIN HOLDER_ACCOUNT_OPERATION HAO ON HAO.ID_MECHANISM_OPERATION_FK = MOPE.ID_MECHANISM_OPERATION_PK         ");
	
		//querySql.append("  INNER JOIN SETTLEMENT_OPERATION SO ON SO.ID_MECHANISM_OPERATION_FK = MOPE.ID_MECHANISM_OPERATION_PK     ");
		//querySql.append("  INNER JOIN COUPON_TRADE_REQUEST CTR ON CTR.ID_MECHANISM_OPERATION_FK = MOPE.ID_MECHANISM_OPERATION_PK   ");
		querySql.append("  INNER JOIN HOLDER_MARKETFACT_BALANCE HMB ON HMB.ID_SECURITY_CODE_FK = MOPE.ID_SECURITY_CODE_FK          ");
		querySql.append("  INNER JOIN SECURITY SEC ON MOPE.ID_SECURITY_CODE_FK = SEC.ID_SECURITY_CODE_PK                		   ");
		querySql.append("  INNER JOIN TRADE_OPERATION TOPE ON MOPE.ID_MECHANISM_OPERATION_PK=TOPE.ID_TRADE_OPERATION_PK  		   ");         
		querySql.append("  INNER JOIN TRADE_REQUEST TREQ ON TOPE.ID_TRADE_REQUEST_FK=TREQ.ID_TRADE_REQUEST_PK             		   ");
		querySql.append("  LEFT OUTER JOIN ACCOUNT_TRADE_REQUEST ATR on TREQ.ID_TRADE_REQUEST_PK=ATR.ID_TRADE_REQUEST_FK   		   ");   
		querySql.append("  INNER JOIN PARTICIPANT PAR1 ON TREQ.ID_SELLER_PARTICIPANT_FK=PAR1.ID_PARTICIPANT_PK             		   ");
		querySql.append("  INNER JOIN PARTICIPANT PAR2 ON TREQ.ID_BUYER_PARTICIPANT_FK=PAR2.ID_PARTICIPANT_PK               	   ");
		
		querySql.append("  WHERE                                                                                                   ");	
		querySql.append("      TREQ.ID_NEGOTIATION_MECHANISM_FK=MOPE.ID_NEGOTIATION_MECHANISM_FK                                   ");	
		querySql.append("      AND TREQ.ID_NEGOTIATION_MECHANISM_FK=5                                                              ");	
		
		
		if (clientPortafolioTO.getIdParticipant() != null) {
			querySql.append("And	(TREQ.ID_BUYER_PARTICIPANT_FK = :participCode 		    ");
			querySql.append("		 OR TREQ.ID_SELLER_PARTICIPANT_FK = :participCode )		");
		}
		
		querySql.append(" AND (EXISTS ");
		querySql.append(" ( ");
		querySql.append(" SELECT had.ID_HOLDER_FK ");
		querySql.append(" FROM HOLDER_ACCOUNT_DETAIL had ");
		querySql.append(" WHERE 1 = 1 ");
		querySql.append(" AND had.ID_HOLDER_ACCOUNT_FK = HMB.ID_HOLDER_ACCOUNT_FK ");
		
    	if(Validations.validateIsNotNullAndNotEmpty(clientPortafolioTO.getCui()))
    		querySql.append(" AND had.ID_HOLDER_FK = :idHolderPk ");
    	
    	if(Validations.validateIsNotNullAndNotEmpty(clientPortafolioTO.getHolderAccountPk()))
    		querySql.append(" AND HMB.ID_HOLDER_ACCOUNT_FK = :holderAccount ");
    	
    	querySql.append(" )) ");
    	
    	/**Estados Aprobado*/
		if(clientPortafolioTO.getReportFormat().equals(ReportFormatType.TXT.getCode()))
			querySql.append(" AND TREQ.OPERATION_STATE IN (616,1863,1865,1866,1867) ");
    	/*Reporte EXCEL*/
		/*Estado revisado*/
		if(clientPortafolioTO.getReportFormat().equals(ReportFormatType.POI.getCode()))
			querySql.append(" AND TREQ.OPERATION_STATE IN (616,1863,1865,1866,1867) ");
		
    	if(Validations.validateIsNotNullAndNotEmpty(clientPortafolioTO.getIdCurrency()))
    		querySql.append(" AND SEC.CURRENCY =  :currency ");
    	
    	if(Validations.validateIsNotNullAndNotEmpty(clientPortafolioTO.getIdIssuer()))
    		querySql.append(" AND SEC.ID_ISSUER_FK =  :idIssuerPk ");
    	
    	if(Validations.validateIsNotNullAndNotEmpty(clientPortafolioTO.getIdSecurityCode()))
    		querySql.append(" AND SEC.ID_SECURITY_CODE_PK = :idSecurityCodePk ");
    	
    	if(Validations.validateIsNotNullAndNotEmpty(clientPortafolioTO.getSecurityClass()))
    		querySql.append(" AND SEC.SECURITY_CLASS = :securityClass ");
    	
    	querySql.append(" AND SEC.STATE_SECURITY = 131 ");
    	
    	if(indExcel)
    		querySql.append(" AND MOPE.ID_NEGOTIATION_MODALITY_FK in (1,3,22) ");
    	else
    		querySql.append(" AND MOPE.ID_NEGOTIATION_MODALITY_FK in (3,22) ");
		
		querySql.append(" AND TRUNC(TREQ.CASH_SETTLEMENT_DATE)	BETWEEN :initialDate And :finalDate ");
		
		querySql.append(" AND HAO.role = 2               ");
		querySql.append(" AND HAO.OPERATION_PART = 1     ");
		
		querySql.append("  ORDER BY  TREQ.ID_TRADE_REQUEST_PK DESC        	     					");	

		Query query = em.createNativeQuery(querySql.toString());
		
		if (clientPortafolioTO.getIdParticipant() != null) {
			query.setParameter("participCode",clientPortafolioTO.getIdParticipant());
		}
		
		if (clientPortafolioTO.getIdIssuer() != null) 
			query.setParameter("idIssuerPk",clientPortafolioTO.getIdIssuer());
		
		if (clientPortafolioTO.getCui() != null) {
			query.setParameter("idHolderPk",clientPortafolioTO.getCui());
		}
		
		if (clientPortafolioTO.getHolderAccountPk() != null) {
			query.setParameter("holderAccount",clientPortafolioTO.getHolderAccountPk());
		}
		
		if (clientPortafolioTO.getIdCurrency() != null) {
			query.setParameter("currency",clientPortafolioTO.getIdCurrency());
		}
		
		if (clientPortafolioTO.getIdSecurityCode() != null) {
			query.setParameter("idSecurityCodePk",clientPortafolioTO.getIdSecurityCode());
		}
		
		if (clientPortafolioTO.getSecurityClass() != null) {
			query.setParameter("securityClass",clientPortafolioTO.getSecurityClass());
		}
		
		if (clientPortafolioTO.getDateMarketFact() != null) {
			query.setParameter("initialDate", clientPortafolioTO.getDateMarketFact());
			query.setParameter("finalDate", clientPortafolioTO.getDateMarketFact());
		}

		List<Object[]> result = query.getResultList();
		
		List<SirtexOperationResultTO> sirtexResultList = new ArrayList<SirtexOperationResultTO>();
		
		try {
			for(Object[] objectData: result){

				if(Integer.valueOf(objectData[11].toString()).equals(SirtexOperationStateType.REVIEWED.getCode()) ||
						Integer.valueOf(objectData[11].toString()).equals(SirtexOperationStateType.REVIEWED.getCode())){
					
					StringBuilder subQuerySql = new StringBuilder();
					
					subQuerySql.append("     SELECT CTR.* FROM                                                                                       ");	
					subQuerySql.append("     COUPON_TRADE_REQUEST CTR						                                                        ");	
					subQuerySql.append(" 	 WHERE CTR.ID_TRADE_REQUEST_FK = :idTrade        	     												");	

					Query subQuery = em.createNativeQuery(subQuerySql.toString());
					
					subQuery.setParameter("idTrade",Integer.valueOf(objectData[0].toString()));
					
					List<Object[]> result2 = subQuery.getResultList();
					
					BigDecimal quantity = new BigDecimal(0);
					
					for(Object[] results : result2){
						//Si de algun cupon la cantidad no es aceptada, entonces no se suma
						if(Validations.validateIsNotNullAndNotEmpty(results[8])){
							if((Integer.valueOf(results[9].toString())) == GeneralConstants.ZERO_VALUE_INTEGER){
								quantity = quantity.add(new BigDecimal(Integer.valueOf(results[8].toString())));
								break;
							}
						}
					}
					objectData[13] = quantity;
				}
				sirtexResultList.add(populateSearchSirtexTO(objectData));
			}
			return sirtexResultList;
		} catch (NoResultException ex) {
			return null;
		}
	}
	private SirtexOperationResultTO populateSearchSirtexTO(Object[] data){
		SirtexOperationResultTO sirtexResultTO = new SirtexOperationResultTO();
		sirtexResultTO.setIdMechanismOperationRequestPk(Long.valueOf(data[0].toString()));
		sirtexResultTO.setIdNegotiationModalityPk(Long.valueOf(data[1].toString()));
		sirtexResultTO.setModalityName((String) data[2]);
		sirtexResultTO.setOperationDate((Date) data[3]);
		sirtexResultTO.setIdPartSeller(Long.valueOf(data[4].toString()));
		sirtexResultTO.setMnemoPartSeller((String) data[5]);
		sirtexResultTO.setIdPartBuyer(Long.valueOf(data[6].toString()));
		sirtexResultTO.setMnemoPartBuyer((String) data[7]);
		sirtexResultTO.setSettlementCashDate((Date) data[8]);
		//sirtexResultTO.setSettlementTermDate((Date) data[9]);
		sirtexResultTO.setSecurityCode((String) data[10]);
		sirtexResultTO.setOperationState(Integer.valueOf(data[11].toString()));
		sirtexResultTO.setRegistryUser((String)data[12]);
		sirtexResultTO.setStockQuantity(new BigDecimal(data[13].toString()));
		sirtexResultTO.setTermSettlementDays(data[14]!=null ? (Long.valueOf(data[14].toString())) : null );
		sirtexResultTO.setSettlementTermDate(data[15]!=null ? (Date)data[15] : null );
		sirtexResultTO.setIssuanceDate(data[16]!=null ? (Date)data[16] : null);
		sirtexResultTO.setExperitationDate(data[17]!=null ? (Date)data[17] : null);
		sirtexResultTO.setRateExchange(data[18]!=null?new BigDecimal(data[18].toString()):BigDecimal.ZERO);
		sirtexResultTO.setQuantityOffere(data[19]!=null?new BigDecimal(data[19].toString()):BigDecimal.ZERO);
		/*CANTIDAD ACEPTADA*/
		sirtexResultTO.setAcceptedAmount(data[20]!=null?new BigDecimal(data[20].toString()):BigDecimal.ZERO);
		sirtexResultTO.setIdSecurityCodePkBcb(data[21]!=null?data[21].toString():sirtexResultTO.getSecurityCode());
		sirtexResultTO.setNominalValue(data[22]!=null?new BigDecimal(data[22].toString()):BigDecimal.ZERO);
		BigDecimal bigDecimal = data[23]!=null?new BigDecimal(data[23].toString()):BigDecimal.ZERO;
		String result = String.format("%.2f", bigDecimal.doubleValue());
		sirtexResultTO.setFinalValue(new BigDecimal(result));
		//plazo restante
		sirtexResultTO.setRemainingTerm(data[24]!=null?Integer.parseInt(data[24].toString()):0);
		sirtexResultTO.setSecurityClass(data[25]!=null?Integer.parseInt(data[25].toString()):0);
		//sirtexResultTO.setTermSettlementDaysReal(data[16]!=null ? (Long.valueOf(data[16].toString())) : null );
		//sirtexResultTO.setCoupon((Integer.valueOf(data[18].toString()))== GeneralConstants.ONE_VALUE_INTEGER ? (BooleanType.YES.getValue()) : null );
		sirtexResultTO.setExchangeAgency(data[26]!=null?data[26].toString():"");
		sirtexResultTO.setClient(data[27]!=null?data[27].toString():"");
		return sirtexResultTO;
	}
	
	@SuppressWarnings("unchecked")
	private List<XmlCoupon> seachXmlCoupunList(Long idTradeRequestPk,String idSecurityCodePk,Date operationDate,String quantityOffered){
		StringBuilder sbQuery= new StringBuilder();
    	
		sbQuery.append(" SELECT PI.COUPON_NUMBER AS COUPON_NUMBER, ");
		sbQuery.append(" PI.PAYMENT_DAYS AS DAYS_OF_LIFE, ");
		sbQuery.append(" pi.EXPERITATION_DATE as EXPIRED_DATE, ");
		//sbQuery.append(" CT.SUPPLIED_QUANTITY as CANT_OFERTADA, ");
		//sbQuery.append(" CT.ACCEPTED_QUANTITY as CANT_ACEPTADA ");
		sbQuery.append(" 0 as CANT_OFERTADA, ");
		sbQuery.append(" 0 as CANT_ACEPTADA, ");
		/**5*/
		sbQuery.append(" trunc(PI.EXPERITATION_DATE-TR.REGISTRY_DATE) as PLAZO_RESTANTE,");
		/**6*/
		sbQuery.append(" ROUND(PI.COUPON_AMOUNT,2) ");
		
		sbQuery.append(" from PROGRAM_INTEREST_COUPON pi ");
		sbQuery.append(" INNER JOIN CORPORATIVE_OPERATION CO ON PI.ID_PROGRAM_INTEREST_PK = CO.ID_PROGRAM_INTEREST_FK ");
		sbQuery.append(" INNER JOIN TRADE_REQUEST TR ON TR.ID_TRADE_REQUEST_PK = :idTradeRequestPk ");
		sbQuery.append(" INNER JOIN COUPON_TRADE_REQUEST CT ON CT.ID_TRADE_REQUEST_FK =  TR.ID_TRADE_REQUEST_PK ");
		sbQuery.append(" WHERE 1 = 1 ");
		sbQuery.append(" AND CO.ID_ORIGIN_SECURITY_CODE_FK = :idSecurityCodePk ");
		sbQuery.append(" AND TR.ID_TRADE_REQUEST_PK =  :idTradeRequestPk ");
		sbQuery.append(" AND TRUNC(PI.EXPERITATION_DATE)> = :experitationDate ");
		sbQuery.append(" ORDER BY PI.COUPON_NUMBER ASC ");
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("idTradeRequestPk", idTradeRequestPk);
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		query.setParameter("experitationDate", operationDate);
		
		List<Object[]> result = (List<Object[]>) query.getResultList();
		
		List<XmlCoupon> listCoupons = new ArrayList<>();
		XmlCoupon coupon = null;
		for (Object[] objects : result) {
			coupon = new XmlCoupon();
			coupon.setCouponNumber(Integer.parseInt(objects[0].toString()));
			coupon.setCouponTerm(Integer.parseInt(objects[1].toString()));
			coupon.setExpirationDate(CommonsUtilities.convertDateToString((Date) objects[2],CommonsUtilities.DATE_PATTERN));
			coupon.setQuantityOffered(Integer.parseInt(quantityOffered));
			coupon.setTerm(Integer.parseInt(objects[5].toString()));
			coupon.setAcceptedAmount(0);	
			coupon.setCouponAmount(new BigDecimal(objects[6].toString()));
			listCoupons.add(coupon);
		}
		return listCoupons;
	}
	
	//DEMATERIALIZED PDF - SIRTEX POR PARTICIPANTE
    public String getQueryClientOperationsDpfSirtexByParticipant(ClientPortafolioTO clientPortafolioTO, Long idStkCalcProcess){
    	StringBuilder sbQuery= new StringBuilder();
    	
		sbQuery.append(" select	");
  		sbQuery.append(" 	TO_NUMBER(ID_PARTICIPANT_PK) ID_PARTICIPANT_PK, ");
		sbQuery.append(" 	DESCRIPTION, ");
		sbQuery.append(" 	MNEMONIC, ");
		sbQuery.append(" 	TO_NUMBER(CURRENCY) CURRENCY, ");
		sbQuery.append(" 	CURRENCY_MNEMONIC, ");
		sbQuery.append(" 	MNEMONIC_ISSUER MNEMONIC_ISSUER, ");
		sbQuery.append(" 	BUSINESS_NAME BUSINESS_NAME, ");
		sbQuery.append(" 	TO_NUMBER(ID_HOLDER_ACCOUNT_PK) ID_HOLDER_ACCOUNT_PK, ");
		sbQuery.append(" 	HOLDERS HOLDERS, ");
		sbQuery.append(" 	HOLDERSDESC HOLDERSDESC, ");
		sbQuery.append(" 	TO_NUMBER(ACCOUNT_NUMBER) ACCOUNT_NUMBER, ");
		sbQuery.append(" 	TO_NUMBER(SECURITY_CLASS) SECURITY_CLASS, ");
		sbQuery.append(" 	SECURITY_CLASS_MNEMONIC SECURITY_CLASS_MNEMONIC, ");
		sbQuery.append(" 	ID_SECURITY_CODE_PK ID_SECURITY_CODE_PK, ");
		sbQuery.append(" 	FECHAEMISION FECHAEMISION, ");
		sbQuery.append(" 	TO_NUMBER(SECURITY_DAYS_TERM) SECURITY_DAYS_TERM, ");
		sbQuery.append(" 	TO_NUMBER(INITIAL_NOMINAL_VALUE) INITIAL_NOMINAL_VALUE, ");
		sbQuery.append(" 	TO_NUMBER(INTEREST_RATE) INTEREST_RATE, ");
		sbQuery.append(" 	TO_NUMBER(SECURITY_TYPE) SECURITY_TYPE, ");
		sbQuery.append(" 	TO_NUMBER(VALORFINAL) VALORFINAL, ");
		sbQuery.append(" 	TO_NUMBER(DIASVIGENTES) DIASVIGENTES, ");
		sbQuery.append(" 	TO_NUMBER(VALOR_PRESENTE) VALOR_PRESENTE, ");
		sbQuery.append(" 	TOTAL_BALANCE, ");
		sbQuery.append(" 	AVAILABLE_BALANCE, ");
		sbQuery.append(" 	PAWN_BALANCE, ");
		sbQuery.append(" 	BAN_BALANCE, ");
		sbQuery.append(" 	OTHER_BLOCK_BALANCE, ");
		sbQuery.append(" 	ACCREDITATION_BALANCE, ");
		sbQuery.append(" 	MARGIN_BALANCE, ");
		sbQuery.append(" 	REPORTING_BALANCE, ");
		sbQuery.append(" 	REPORTED_BALANCE, ");
		sbQuery.append(" 	PURCHASE_BALANCE, ");
		sbQuery.append(" 	SALE_BALANCE, ");
		sbQuery.append(" 	TRANSIT_BALANCE, ");
		sbQuery.append(" 	TASA_MERCADO, ");
		sbQuery.append(" 	PRECIO_MERCADO, ");
		sbQuery.append(" 	TOTAL_AMOUNT_MF, ");
		sbQuery.append(" 	THEORIC_RATE, ");
		sbQuery.append(" 	THEORIC_PRICE, ");
		sbQuery.append(" 	TOTAL_AMOUNT, ");
		sbQuery.append(" 	CURRENT_NOMINAL_VALUE, ");
		sbQuery.append(" 	CURRENT_TOTAL_AMOUNT, ");
		
		sbQuery.append(" 	alternative_code, ");
		sbQuery.append(" 	to_char(expiration_date,'dd/mm/yyyy'), ");
		sbQuery.append(" 	holder_type, ");
		sbQuery.append(" 	document_number, ");
		sbQuery.append(" 	serie, ");
		sbQuery.append("  	FECHA_OPERACION, ");
		sbQuery.append("    CANTIDAD_CUPONES, ");
		sbQuery.append("    CANTIDAD_VALORES, ");
		sbQuery.append("    NUM_SOLICITUD_EDV ");

		sbQuery.append(" from ");
		sbQuery.append(" 	(select /*+ USE_NL(ISS,SECU,P,MFV) */ ");
		sbQuery.append(" 	p.id_participant_pk||HA.id_holder_account_pk||secu.id_security_code_pk secuencia, ");
		sbQuery.append(" 	p.id_participant_pk, ");
		sbQuery.append(" 	p.description, ");
		sbQuery.append(" 	p.mnemonic, ");																										 
		sbQuery.append(" 	secu.currency, ");																																				 
		sbQuery.append(" 	(select pt.text1 from parameter_table pt where pt.parameter_table_pk=secu.currency) currency_mnemonic, ");														 
		sbQuery.append(" 	iss.mnemonic as MNEMONIC_ISSUER, ");																															 
		sbQuery.append(" 	iss.business_name, ");																																			 
		sbQuery.append(" 	HA.id_holder_account_pk, ");																																	 
		sbQuery.append(" 	(SELECT LISTAGG(HAD.ID_HOLDER_FK , ',' ||chr(13) ) WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");																			 
		sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD ");																																 
		sbQuery.append(" 	WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ");																									 
		sbQuery.append(" 	) Holders, ");
		sbQuery.append("    (SELECT LISTAGG((select h.full_name from holder h where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK) ||chr(13)) WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
		sbQuery.append("    FROM HOLDER_ACCOUNT_DETAIL HAD	");
		sbQuery.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK " );
		sbQuery.append(" 	) HoldersDesc, ");
		sbQuery.append(" 	HA.account_number, ");
		//
		sbQuery.append(" 	secu.ALTERNATIVE_CODE, ");
		sbQuery.append(" 	secu.expiration_date, ");
		sbQuery.append(" 	(select parameter_name from parameter_table where parameter_table_pk=(SELECT distinct (select h.holder_type from holder h where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK)) as holder_type, ");
		sbQuery.append(" 	(SELECT LISTAGG((select h.document_number from holder h where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK) ||chr(13)) WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)     FROM HOLDER_ACCOUNT_DETAIL HAD	    WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK  	) document_number, ");
		sbQuery.append(" 	secu.id_security_code_only as serie, ");
		
		sbQuery.append(" 	secu.security_class, ");																																	 
		sbQuery.append(" 	(select pt.text1 from parameter_table pt where pt.parameter_table_pk=secu.security_class) security_class_mnemonic, ");											 
		sbQuery.append(" 	secu.id_security_code_pk, ");																																	 
		sbQuery.append(" 	(to_char(secu.issuance_date,'dd/MM/YYYY')) fechaEmision, ");																									 
		sbQuery.append(" 	secu.security_days_term, ");																																	 
		sbQuery.append(" 	secu.initial_nominal_value, ");
		sbQuery.append(" 	secu.interest_rate, ");
		sbQuery.append(" 	secu.security_type, ");

		sbQuery.append("	( (secu.INITIAL_NOMINAL_VALUE) * (  1+(secu.INTEREST_RATE/100)*  ( secu.security_days_term )/ 360 )) as ValorFinal,  ");//as ValorFinal,
		sbQuery.append(" 	GREATEST(0,nvl(trunc(secu.expiration_date) - TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY'),0)) DiasVigentes, ");
		sbQuery.append("	ROUND( ( (secu.INITIAL_NOMINAL_VALUE) * (  1+(secu.INTEREST_RATE/100)*  ( secu.security_days_term )/ 360 )) / (1+(secu.INTEREST_RATE/100) * GREATEST(0,nvl(trunc(secu.expiration_date) -  TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY'),0)) /360 ) ,  2)  VALOR_PRESENTE   ,  ");//as ValorFinal,
		sbQuery.append(" 	MFV.total_balance, "); 
		sbQuery.append(" 	MFV.available_balance, ");
		sbQuery.append(" 	MFV.pawn_balance, "); 
		sbQuery.append(" 	MFV.ban_balance, ");
		sbQuery.append(" 	MFV.other_block_balance, ");
		sbQuery.append(" 	MFV.accreditation_balance, ");
		sbQuery.append(" 	MFV.margin_balance, ");
		sbQuery.append(" 	MFV.reporting_balance, ");
		sbQuery.append(" 	MFV.reported_balance, ");
		sbQuery.append(" 	MFV.purchase_balance, ");
		sbQuery.append(" 	MFV.sale_balance, ");
		sbQuery.append(" 	MFV.transit_balance, ");
		sbQuery.append(" 	NVL(MFV.market_rate,0) TASA_MERCADO, ");
		sbQuery.append(" 	NVL(MFV.market_price,0) PRECIO_MERCADO, ");
		sbQuery.append(" 	NVL(ROUND(MFV.market_price,2),0) * MFV.total_balance as TOTAL_AMOUNT_MF, ");
		sbQuery.append(" 	NVL(MFV.THEORIC_RATE,0) THEORIC_RATE, ");
		sbQuery.append(" 	NVL(MFV.THEORIC_PRICE,0) THEORIC_PRICE, ");
		sbQuery.append(" 	NVL(MFV.THEORIC_PRICE,0) * MFV.total_balance total_amount, ");
		sbQuery.append("    DECODE(secu.security_class,426,ROUND(");
		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
			sbQuery.append("			(SELECT distinct NVH.NOMINAL_VALUE FROM NOMINAL_VALUE_HISTORY NVH WHERE TRUNC(NVH.CUT_DATE) = TRUNC(MFV.CUT_DATE) 			");
			sbQuery.append("			AND secu.ID_SECURITY_CODE_PK = NVH.ID_SECURITY_CODE_FK) 															");
		}else{
			sbQuery.append("			secu.current_nominal_value ");
		}
		sbQuery.append("		  	,4),ROUND( ");
		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
			sbQuery.append("			(SELECT distinct NVH.NOMINAL_VALUE FROM NOMINAL_VALUE_HISTORY NVH WHERE TRUNC(NVH.CUT_DATE) = TRUNC(MFV.CUT_DATE) 			");
			sbQuery.append("			AND secu.ID_SECURITY_CODE_PK = NVH.ID_SECURITY_CODE_FK) 															");
		}else{
			sbQuery.append("			secu.current_nominal_value ");
		}
		sbQuery.append(" 			,2)) ");
		sbQuery.append(" 	as current_nominal_value, ");
		sbQuery.append("    DECODE(secu.security_class,426,ROUND( ");
		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
			sbQuery.append("			(SELECT distinct NVH.NOMINAL_VALUE FROM NOMINAL_VALUE_HISTORY NVH WHERE TRUNC(NVH.CUT_DATE) = TRUNC(MFV.CUT_DATE) 			");
			sbQuery.append("			AND secu.ID_SECURITY_CODE_PK = NVH.ID_SECURITY_CODE_FK) 															");
		}else{
			sbQuery.append("			secu.current_nominal_value ");
		}
		sbQuery.append("		  	,4),ROUND( ");
		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
			sbQuery.append("			(SELECT distinct NVH.NOMINAL_VALUE FROM NOMINAL_VALUE_HISTORY NVH WHERE TRUNC(NVH.CUT_DATE) = TRUNC(MFV.CUT_DATE) 			");
			sbQuery.append("			AND secu.ID_SECURITY_CODE_PK = NVH.ID_SECURITY_CODE_FK) 															");
		}else{
			sbQuery.append("			secu.current_nominal_value ");
		}
		sbQuery.append(" 			,2)) * MFV.total_balance ");
		sbQuery.append(" 	as current_total_amount, ");
		
		/**Para cantidad de cupones*/
		sbQuery.append(" MO.OPERATION_DATE FECHA_OPERACION, ");
		sbQuery.append(" ( ");
		sbQuery.append(" SELECT COUNT(PIC.COUPON_NUMBER) FROM SECURITY S ");
		sbQuery.append(" INNER JOIN INTEREST_PAYMENT_SCHEDULE IPS ON S.ID_SECURITY_CODE_PK=IPS.ID_SECURITY_CODE_FK ");
		sbQuery.append(" INNER JOIN PROGRAM_INTEREST_COUPON PIC   ON IPS.ID_INT_PAYMENT_SCHEDULE_PK=PIC.ID_INT_PAYMENT_SCHEDULE_FK ");
		sbQuery.append(" WHERE 1 = 1 ");
		sbQuery.append(" AND PIC.PAYMENT_DATE >= TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') ");
		sbQuery.append(" AND TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY') ");
		sbQuery.append(" AND S.ID_SECURITY_CODE_PK = SECU.ID_SECURITY_CODE_PK ");
		sbQuery.append(" ) CANTIDAD_CUPONES, ");
		sbQuery.append(" MO.STOCK_QUANTITY CANTIDAD_VALORES, ");
		sbQuery.append(" MO.OPERATION_NUMBER NUM_SOLICITUD_EDV ");
		/**Issue: 1164*/
	    sbQuery.append(" (SELECT PARAMETER_NAME FROM PARAMETER_TABLE WHERE ");
		sbQuery.append("  PARAMETER_TABLE_PK= (SELECT HHH.HOLDER_TYPE FROM HOLDER HHH WHERE HHH.ID_HOLDER_PK");
		sbQuery.append("  IN (SELECT ID_HOLDER_FK FROM HOLDER_ACCOUNT_DETAIL WHERE ");
		sbQuery.append("  ID_HOLDER_ACCOUNT_FK = AAO.ID_HOLDER_ACCOUNT_FK  AND ROWNUM <= 1 )))AS FIRST_HOLDER, ");
		sbQuery.append(" (select CASE WHEN WITH_COUPONS_SIRTEX_NEGOTIATE = 1 THEN 'SI' ELSE 'NO' END FROM ISSUER WHERE 1 = 1 AND ISS.ID_ISSUER_PK = ID_ISSUER_PK) as WITH_COUPONS ");
		
		
		sbQuery.append(" From holder_account ha ");
		sbQuery.append(" inner join market_fact_view mfv on mfv.id_holder_account_pk = HA.id_holder_account_pk							");
		sbQuery.append(" Inner join participant p	ON mfv.id_participant_pk = p.id_participant_pk									  	");
		sbQuery.append(" Inner join security secu ON mfv.id_security_code_pk	= secu.id_security_code_pk  							");
		sbQuery.append(" Inner join issuer iss 	ON secu.id_issuer_fk				= iss.id_issuer_pk 									");
		
		sbQuery.append(" INNER JOIN holder_marketfact_balance HMB ON secu.id_security_code_pk = HMB.id_security_code_fk ");
		sbQuery.append(" INNER JOIN security_negotiation_mechanism SNM ON SECU.id_security_code_pk = SNM.id_security_code_fk ");
		sbQuery.append(" INNER JOIN MECHANISM_OPERATION MO ON MO.ID_SECURITY_CODE_FK = SECU.ID_SECURITY_CODE_PK ");
		/**1164*/
		sbQuery.append(" inner join ACCOUNT_ANNOTATION_OPERATION AAO ON AAO.ID_SECURITY_CODE_FK = secu.ID_SECURITY_CODE_PK ");

		
		sbQuery.append(" WHERE 1 = 1	");
		sbQuery.append(" AND HMB.IND_ACTIVE = 1 ");
		//sbQuery.append(" AND HMB.AVAILABLE_BALANCE > 0 ");
		sbQuery.append(" AND MO.ID_NEGOTIATION_MECHANISM_FK = 5  ");
		sbQuery.append(" AND SNM.ID_NEGOTIATION_MECHANISM_FK = 5 ");
		//sbQuery.append(" AND SECU.SECURITY_CLASS = 420 ");
		sbQuery.append(" AND SNM.ID_NEGOTIATION_MODALITY_FK IN (1,3) ");
		sbQuery.append(" AND SECU.SECURITY_TYPE IN (1762,1763) ");
		
		
		// Optimizacion Query
		//sbQuery.append(" 	AND trunc(MFV.CUT_DATE)= TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 			");
		sbQuery.append(" AND MFV.CUT_DATE >= TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') ");
		sbQuery.append(" AND MFV.CUT_DATE < TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 ");
		sbQuery.append(" AND TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY') ");
		
		//sbQuery.append(" 	AND (MFV.total_balance > 0 or MFV.reported_balance > 0)																									");
		sbQuery.append(" 	AND MFV.total_balance > 0 	");
		//validacion de los participantes inversionistas
		if(clientPortafolioTO.getIsParticipantInvestor()){ 
			sbQuery.append(" 	AND (EXISTS																						    												");
			sbQuery.append(" 		(SELECT 1																																		");
			sbQuery.append(" 		FROM HOLDER_ACCOUNT_DETAIL HAD																													");
			sbQuery.append(" 		INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK																						");
			sbQuery.append(" 		WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.id_holder_account_pk																						");
			sbQuery.append(" 			AND H.DOCUMENT_TYPE = p.DOCUMENT_TYPE																										");
			sbQuery.append(" 			AND H.DOCUMENT_NUMBER = p.DOCUMENT_NUMBER																									");
			sbQuery.append(" 	))																																					");
		}
			
		
		if(Validations.validateIsNotNull(clientPortafolioTO.getIdParticipant())){
			sbQuery.append(" and p.id_participant_pk= :participantPk																												");
		}
		if(Validations.validateIsNotNull(clientPortafolioTO.getHolderAccountPk())){
    		sbQuery.append(" and HA.id_holder_account_pk=:holder_account																											");
    	}
		if(Validations.validateIsNotNull(clientPortafolioTO.getIdSecurityCode())){
    		sbQuery.append(" and secu.id_security_code_pk= :securityPk																												");
    	}
		
		/*if(clientPortafolioTO.getShowAvailableBalance().equals(Integer.valueOf("1"))){
			sbQuery.append(" and MFV.reported_balance > 0																															");
		}*/
		
		if(Validations.validateIsNotNull(clientPortafolioTO.getCui())){
			sbQuery.append(" AND (EXISTS																																			");
			sbQuery.append(" 	(SELECT had.ID_HOLDER_FK																															");
			sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL had																														");
			sbQuery.append(" 	WHERE had.ID_HOLDER_FK = :cui_code																													");
			sbQuery.append(" 		AND had.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK))																						");
	    }
		if(clientPortafolioTO.getInstitutionType().equals("186") && clientPortafolioTO.getSecurityClass() == null){
			sbQuery.append(" and secu.SECURITY_CLASS in (420)		//Solo DPF											 																");
	  	}		
		if(Validations.validateIsNotNull(clientPortafolioTO.getSecurityClass())){
    		sbQuery.append(" and secu.security_class in (:security_class)");
    	}
		if(Validations.validateIsNotNull(clientPortafolioTO.getIdIssuer())){
    		sbQuery.append(" and iss.id_issuer_pk=:issuerPk");
    	}
		if(Validations.validateIsNotNull(clientPortafolioTO.getIdCurrency())){
    		sbQuery.append(" and secu.currency=:currency");
    	}
		sbQuery.append(" )T ");
		
		//sbQuery.append(" )T  Order by mnemonic, security_type, currency_mnemonic, security_class_mnemonic ,id_security_code_pk, Holders ");
		sbQuery.append(" GROUP BY  ID_PARTICIPANT_PK,DESCRIPTION, mnemonic, CURRENCY, CURRENCY_MNEMONIC, MNEMONIC_ISSUER, ");
		sbQuery.append(" BUSINESS_NAME, ID_HOLDER_ACCOUNT_PK, HOLDERS, HOLDERSDESC, ACCOUNT_NUMBER, SECURITY_CLASS, SECURITY_CLASS_MNEMONIC, ");
		sbQuery.append(" ID_SECURITY_CODE_PK, FECHAEMISION, SECURITY_DAYS_TERM,INITIAL_NOMINAL_VALUE, INTEREST_RATE,SECURITY_TYPE,VALORFINAL,  ");
		sbQuery.append(" DIASVIGENTES, VALOR_PRESENTE,TOTAL_BALANCE, AVAILABLE_BALANCE, PAWN_BALANCE,BAN_BALANCE,OTHER_BLOCK_BALANCE, ");
		sbQuery.append(" ACCREDITATION_BALANCE,MARGIN_BALANCE,REPORTING_BALANCE,REPORTED_BALANCE,PURCHASE_BALANCE,SALE_BALANCE,TRANSIT_BALANCE, ");
		sbQuery.append(" TASA_MERCADO,PRECIO_MERCADO,TOTAL_AMOUNT_MF,THEORIC_RATE,THEORIC_PRICE,TOTAL_AMOUNT,CURRENT_NOMINAL_VALUE,CURRENT_TOTAL_AMOUNT, ");
		sbQuery.append(" ALTERNATIVE_CODE,expiration_date,HOLDER_TYPE,DOCUMENT_NUMBER,SERIE, ");
		sbQuery.append(" FECHA_OPERACION, CANTIDAD_CUPONES, CANTIDAD_VALORES, NUM_SOLICITUD_EDV ");
		sbQuery.append(" ORDER BY MNEMONIC,SECURITY_TYPE,CURRENCY_MNEMONIC,SECURITY_CLASS_MNEMONIC,ID_SECURITY_CODE_PK,HOLDERS ");

		String strQueryFormatted = sbQuery.toString();
		
		if(clientPortafolioTO.getIdParticipant()!=null){
    		strQueryFormatted= strQueryFormatted.replace(":participantPk", clientPortafolioTO.getIdParticipant().toString());
    	}
    	if(clientPortafolioTO.getHolderAccountPk()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":holder_account", clientPortafolioTO.getHolderAccountPk().toString());
    	}
    	if(clientPortafolioTO.getIdSecurityCode()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":securityPk", "'"+clientPortafolioTO.getIdSecurityCode()+"'");
    	}
    	
    	if(Validations.validateIsNotNullAndPositive(clientPortafolioTO.getCui())){
    		strQueryFormatted=strQueryFormatted.replace(":cui_code", clientPortafolioTO.getCui().toString());
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(clientPortafolioTO.getSecurityClass())){
    		strQueryFormatted=strQueryFormatted.replace(":security_class", clientPortafolioTO.getSecurityClass());
    	}
    	if(clientPortafolioTO.getIdIssuer()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":issuerPk", "'"+clientPortafolioTO.getIdIssuer()+"'");
    	}
    	if(clientPortafolioTO.getIdCurrency()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":currency", clientPortafolioTO.getIdCurrency().toString());
    	}
    	System.out.println("QUERY:"+strQueryFormatted);
    	return strQueryFormatted;
    }
    
    /**
	 * Lista de Anotaciones en cuenta 328
	 * Issue 1228
	 * @param parametersRequired
	 * @return
	 */
	public List<Object[]> getVoluntaryAccountEntriesListByParameters(Map<String,Object> parameters) {
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append(" SELECT                                                                                                                             ");
		sbQuery.append(" 	to_char(CO.LAST_MODIFY_DATE,'dd/mm/yyyy')  FECHA_ULTIMO_ESTADO                                                                  ");
		sbQuery.append(" 	,p.MNEMONIC  as participant                                                                                                     ");
		sbQuery.append(" 	,HA.ACCOUNT_NUMBER                                                                                                              ");
		sbQuery.append(" 	,(SELECT LISTAGG(HAD.ID_HOLDER_FK,', ') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)  	                                            ");
		sbQuery.append(" 	  FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK = AAO.ID_HOLDER_ACCOUNT_FK) AS ID_HOLDER_PK                     ");
		sbQuery.append(" 	,(CASE(HA.ACCOUNT_TYPE) WHEN 106 then 'N' WHEN 107 then 'J' WHEN 108 then 'N' ELSE '' END) AS ACCOUNT_TYPE                      ");
		sbQuery.append(" 	,(CASE                                                                                                         ");
		sbQuery.append(" 		WHEN HA.ACCOUNT_TYPE IN( 108,106) THEN (SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK=1877)                                       ");
		sbQuery.append(" 		ELSE (SELECT DISTINCT description FROM HOLDER_ACCOUNT_DETAIL HAD                                                                     ");
		sbQuery.append(" 			INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK                                                                ");
		sbQuery.append(" 			INNER JOIN PARAMETER_TABLE PT ON PT.parameter_table_pk = H.ECONOMIC_ACTIVITY                                            ");
		sbQuery.append(" 			WHERE HAD.ID_HOLDER_ACCOUNT_FK = AAO.ID_HOLDER_ACCOUNT_FK) END) AS ECONOMIC_ACTIVITY                                    ");
		sbQuery.append(" 	,(SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK=AAO.MOTIVE) MOTIVO                                           ");
		sbQuery.append(" 	,(SELECT TEXT1 FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK=s.SECURITY_CLASS) SECURITY_CLASS                                   ");
		sbQuery.append(" 	,i.MNEMONIC as emisor                                                                                                           ");
		sbQuery.append(" 	,s.ID_SECURITY_CODE_PK                                                                                                          ");
		sbQuery.append(" 	,(SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK=s.CURRENCY) MONEDA                                           ");
		sbQuery.append(" 	,to_char(s.issuance_date,'dd/mm/yyyy') ISSUANCE_DATE                                                                            ");
		sbQuery.append(" 	,(s.EXPIRATION_DATE - s.ISSUANCE_DATE) as PLAZO                                                                                 ");
		sbQuery.append(" 	,s.EXPIRATION_DATE                                                                                                              ");
		sbQuery.append(" ,NVL ( (select max(HAM.NOMINAL_VALUE) from HOLDER_ACCOUNT_MOVEMENT ham where HAM.ID_CUSTODY_OPERATION_FK = CO.ID_CUSTODY_OPERATION_PK) ");
		sbQuery.append(" ,s.current_nominal_value ) AS INITIAL_NOMINAL_VALUE	");
		sbQuery.append(" 	,s.INTEREST_RATE                                                                                                                ");
		sbQuery.append(" 	,aam.MARKET_RATE                                                                                                                ");
		sbQuery.append(" 	,aam.MARKET_PRICE                                                                                                               ");
		sbQuery.append(" 	,aao.TOTAL_BALANCE                                                                                                              ");
		sbQuery.append(" 	,idepositary.TIPOCAMBIO(NVL ( (select max(HAM.NOMINAL_VALUE) from HOLDER_ACCOUNT_MOVEMENT ham where HAM.ID_CUSTODY_OPERATION_FK = CO.ID_CUSTODY_OPERATION_PK) ,s.current_nominal_value ), CO.LAST_MODIFY_DATE,                                                           ");
		sbQuery.append(" 		(SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK=s.CURRENCY), 'USD') VALOR_NOMINAL_USD                     ");
		sbQuery.append(" 	,NVL ( (select max(HAM.NOMINAL_VALUE) from HOLDER_ACCOUNT_MOVEMENT ham where HAM.ID_CUSTODY_OPERATION_FK = CO.ID_CUSTODY_OPERATION_PK) ,s.current_nominal_value ) * aao.TOTAL_BALANCE as MONTO_TOTAL                                                                     ");
		sbQuery.append(" 	,idepositary.TIPOCAMBIO((NVL ( (select max(HAM.NOMINAL_VALUE) from HOLDER_ACCOUNT_MOVEMENT ham where HAM.ID_CUSTODY_OPERATION_FK = CO.ID_CUSTODY_OPERATION_PK) ,s.current_nominal_value )*aao.TOTAL_BALANCE),CO.LAST_MODIFY_DATE,                                        ");
		sbQuery.append(" 		(SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK=s.CURRENCY), 'USD') MONTO_TOTAL_USD                       ");
		sbQuery.append(" FROM ACCOUNT_ANNOTATION_OPERATION aao                                                                                              ");
		sbQuery.append(" 	INNER JOIN CUSTODY_OPERATION co on co.ID_CUSTODY_OPERATION_PK = AAO.ID_ANNOTATION_OPERATION_PK                                  ");
		sbQuery.append(" 	INNER JOIN PARTICIPANT p on p.ID_PARTICIPANT_PK = aao.ID_PLACEMENT_PARTICIPANT_FK                                               ");
		sbQuery.append(" 	INNER JOIN HOLDER_ACCOUNT ha on HA.ID_HOLDER_ACCOUNT_PK = aao.ID_HOLDER_ACCOUNT_FK                                              ");
		sbQuery.append(" 	INNER JOIN SECURITY s on s.ID_SECURITY_CODE_PK = aao.ID_SECURITY_CODE_FK                                                        ");
		sbQuery.append(" 	INNER JOIN ISSUER i on i.ID_ISSUER_PK = s.ID_ISSUER_FK                                                                          ");
		sbQuery.append(" 	INNER JOIN ACCOUNT_ANNOTATION_MARKETFACT aam on aam.ID_ANNOTATION_OPERATION_FK = aao.ID_ANNOTATION_OPERATION_PK                 ");
		sbQuery.append(" WHERE 1 = 1                                                                                                                        ");
		sbQuery.append(" AND CO.OPERATION_STATE = 858                                                                                                       ");
		
		if(parameters.containsKey(ReportConstant.PARTICIPANT_PARAM))
		sbQuery.append(" 	AND (p.ID_PARTICIPANT_PK = :participant)                                                                                        ");
		if(parameters.containsKey(ReportConstant.HOLDER_PARAM)){
		sbQuery.append(" 	AND (EXISTS(SELECT 1                                                                                                            ");
		sbQuery.append(" 		FROM HOLDER_ACCOUNT_DETAIL had                                                                                              ");
		sbQuery.append(" 		WHERE had.ID_HOLDER_FK = :holder                                                                                            ");
		sbQuery.append(" 		AND had.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK)                                                                     ");
		sbQuery.append(" 		)                                                                                                                           ");
		}
		if(parameters.containsKey(ReportConstant.SECURITY_CLASS_PARAM))
		sbQuery.append(" 	and (S.SECURITY_CLASS = :security_class)                                                                                        ");
		if(parameters.containsKey(ReportConstant.SECURITY_CODE))
		sbQuery.append(" 	and (S.ID_SECURITY_CODE_PK = :security_code)                                                                                    ");
		if(parameters.containsKey(ReportConstant.ISSUER_PARAM))
		sbQuery.append(" 	and (I.ID_ISSUER_PK = :issuer)                                                                                                  ");
		if(parameters.containsKey(ReportConstant.CURRENCY_TYPE))
		sbQuery.append(" 	and (s.CURRENCY = :currency)                                                                                                    ");
		if(parameters.containsKey(ReportConstant.MOTIVE_PARAM))
		sbQuery.append(" 	and (AAO.MOTIVE = :motive)                                                                                                      ");
		sbQuery.append(" 	and (trunc(CO.LAST_MODIFY_DATE) between trunc(to_date(:date_initial,'dd/MM/yyyy')) and trunc(to_date(:date_end,'dd/MM/yyyy')))  ");
		sbQuery.append(" order by CO.LAST_MODIFY_DATE asc, S.ID_SECURITY_CODE_PK asc																		");
		
	    Query query = em.createNativeQuery(sbQuery.toString());
	    
	    if(parameters.containsKey(ReportConstant.PARTICIPANT_PARAM))
	    	query.setParameter("participant", parameters.get(ReportConstant.PARTICIPANT_PARAM));
		if(parameters.containsKey(ReportConstant.HOLDER_PARAM))
			query.setParameter("holder", parameters.get(ReportConstant.HOLDER_PARAM));
		if(parameters.containsKey(ReportConstant.SECURITY_CLASS_PARAM))
			query.setParameter("security_class", parameters.get(ReportConstant.SECURITY_CLASS_PARAM));
		if(parameters.containsKey(ReportConstant.SECURITY_CODE))
			query.setParameter("security_code", parameters.get(ReportConstant.SECURITY_CODE));
		if(parameters.containsKey(ReportConstant.ISSUER_PARAM))
			query.setParameter("issuer", parameters.get(ReportConstant.ISSUER_PARAM));
		if(parameters.containsKey(ReportConstant.CURRENCY_TYPE))
			query.setParameter("currency", parameters.get(ReportConstant.CURRENCY_TYPE));
		if(parameters.containsKey(ReportConstant.MOTIVE_PARAM))
			query.setParameter("motive", parameters.get(ReportConstant.MOTIVE_PARAM));
		
		query.setParameter("date_initial", parameters.get(ReportConstant.DATE_INITIAL_PARAM));
		query.setParameter("date_end", parameters.get(ReportConstant.DATE_END_PARAM));
		
		return query.getResultList();
	}
	
	/**
	 * Lista de acciones
	 * Issue 1285
	 * @param parametersRequired
	 * @return
	 */
	public List<Object[]> getActionsListByParameters(Map<String,Object> parameters) {
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append(" select distinct                                                                                                   ");
		sbQuery.append(" HA.account_number as HOLDER_ACCOUNT    ");
		sbQuery.append(" ,(SELECT LISTAGG(HAD.ID_HOLDER_FK,', ') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)                                  ");
		sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS CUI                ");
		sbQuery.append(" ,(SELECT LISTAGG(H.FULL_NAME,', ') WITHIN GROUP (ORDER BY H.FULL_NAME)                                            ");
		sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK                        ");
		sbQuery.append("   WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK) AS DESCRIPCION_TITULAR                                ");
		sbQuery.append(" ,(SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK =                                              ");
		sbQuery.append("  (select account_type from HOLDER_ACCOUNT HA where MFV.ID_HOLDER_ACCOUNT_PK=HA.ID_HOLDER_ACCOUNT_PK)) TIPO_CUENTA ");
		sbQuery.append(" ,SEC.ID_SECURITY_CODE_PK AS CLASE_CLAVE                                                                           ");
		sbQuery.append(" ,SEC.CURRENT_NOMINAL_VALUE AS VALOR_NOMINAL                                                                       ");
		sbQuery.append(" ,ACCREDITATION_BALANCE as cat                                                                                     ");
		sbQuery.append(" ,BAN_BALANCE as req_judicial                                                                                      ");
		sbQuery.append(" ,PAWN_BALANCE as prenda                                                                                           ");
		sbQuery.append(" ,AVAILABLE_BALANCE as cantidad_disponible                                                                         ");
		sbQuery.append(" ,MFV.total_balance as cantidad_total                                                                              ");
		sbQuery.append(" ,(select HAD.ID_HOLDER_FK FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK and rownum =1) AS CUI_2 ");
		sbQuery.append(" from MARKET_FACT_VIEW MFV                                                                                         ");
		sbQuery.append(" INNER JOIN PARTICIPANT PA ON PA.ID_PARTICIPANT_PK = MFV.ID_PARTICIPANT_PK                                         ");
		sbQuery.append(" INNER JOIN SECURITY SEC ON SEC.ID_SECURITY_CODE_PK = MFV.ID_SECURITY_CODE_PK                                      ");
		sbQuery.append(" INNER JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK = MFV.ID_HOLDER_ACCOUNT_PK                                ");
		sbQuery.append(" WHERE 1 = 1                                                                                                       ");
		sbQuery.append(" AND MFV.total_balance > 0                                                                 "); 
		sbQuery.append(" AND SEC.security_class in(126,406,1962,1945,2246,2855,2856)                                                                 "); 
		
		if(parameters.containsKey(ReportConstant.ISSUER_PARAM))
		sbQuery.append(" 	AND (SEC.ID_ISSUER_FK = :issuer) ");
		if(parameters.containsKey(ReportConstant.SECURITY_CLASS_PARAM))
		sbQuery.append(" 	AND (SEC.SECURITY_CLASS = :security_class) ");
		if(parameters.containsKey(ReportConstant.SECURITY_CODE))
		sbQuery.append(" 	and (SEC.ID_SECURITY_CODE_PK = :security_code) ");
		sbQuery.append(" 	and (TRUNC(MFV.CUT_DATE) <= TO_DATE(:cut_date, 'dd/mm/yyyy')) ");
		sbQuery.append(" order by SEC.ID_SECURITY_CODE_PK asc,12 asc,HA.account_number asc ");
		
	    Query query = em.createNativeQuery(sbQuery.toString());
	    
	    if(parameters.containsKey(ReportConstant.ISSUER_PARAM))
	    	query.setParameter(ReportConstant.ISSUER_PARAM, parameters.get(ReportConstant.ISSUER_PARAM));
		if(parameters.containsKey(ReportConstant.SECURITY_CLASS_PARAM))
			query.setParameter(ReportConstant.SECURITY_CLASS_PARAM, parameters.get(ReportConstant.SECURITY_CLASS_PARAM));
		if(parameters.containsKey(ReportConstant.SECURITY_CODE))
			query.setParameter(ReportConstant.SECURITY_CODE, parameters.get(ReportConstant.SECURITY_CODE));
		
			query.setParameter("cut_date", parameters.get("cut_date"));
	    
		return query.getResultList();
	}
}
