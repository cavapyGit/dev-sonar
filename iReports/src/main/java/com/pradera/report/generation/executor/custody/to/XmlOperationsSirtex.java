package com.pradera.report.generation.executor.custody.to;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;



@XmlAccessorType(XmlAccessType.FIELD) 
public class XmlOperationsSirtex implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4403666294445149228L;
	
	@XmlElement(name = "NRO_CONSULTA")
	private String consultNumber;
	
	@XmlElement(name = "OPERACION")
	private String operation;
	
	@XmlElement(name = "FECHA_OPERACION")
	private String operationDate;
	
	@XmlElement(name = "SECC_REG_SIRTEX")
	private List<XmlRegSirtex> regSirtexs;

	public String getConsultNumber() {
		return consultNumber;
	}

	public void setConsultNumber(String consultNumber) {
		this.consultNumber = consultNumber;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getOperationDate() {
		return operationDate;
	}

	public void setOperationDate(String operationDate) {
		this.operationDate = operationDate;
	}

	public List<XmlRegSirtex> getRegSirtexs() {
		return regSirtexs;
	}

	public void setRegSirtexs(List<XmlRegSirtex> regSirtexs) {
		this.regSirtexs = regSirtexs;
	}
	
}
