package com.pradera.report.generation.executor.securities;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "SecuritiesRegistrationReport")
public class SecuritiesRegistrationReport extends GenericReport{

private static final long serialVersionUID = 1L;
	
	@Inject  PraderaLogger log;
	
	@EJB
	private ParameterServiceBean parameterService;

	public SecuritiesRegistrationReport() {
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParametersQueryReport() {
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		if (listaLogger == null) {
			listaLogger = new ArrayList<ReportLoggerDetail>();
		}

	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		Map<String,Object> parametersRequired = new HashMap<>();
		
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		String currencyDescription = null;
		String securityclassDescription = null;
		
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.CURRENCY)){
				if(r.getFilterValue() != null){
					currencyDescription = r.getFilterDescription();
				}else{
					currencyDescription = "TODOS";
				}
			}
			if (r.getFilterName().equals(ReportConstant.SECURITY_CLASS_PARAM)){
				if(r.getFilterValue() != null){
					securityclassDescription = r.getFilterDescription();
				}else{
					securityclassDescription = "TODOS";
				}
			}
		}
		
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		parametersRequired.put("pDescriptionC", currencyDescription);
		parametersRequired.put("pDescriptionSC", securityclassDescription);
		parametersRequired.put("fecha_actual", dateFormat.format(date));
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		// TODO Auto-generated method stub
		return parametersRequired;
	}
}
