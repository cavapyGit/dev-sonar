package com.pradera.report.generation.executor.accounting;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounting.AccountingProcess;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.accounting.service.AccountingReportQueryServiceBean;
import com.pradera.report.generation.executor.accounting.to.AccountingProcessTo;
import com.pradera.report.generation.executor.accounting.to.AccountingVoucherRegistryTo;
import com.pradera.report.generation.executor.accounting.to.AccountingVoucherTo;
import com.pradera.report.generation.service.ReportUtilServiceBean;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingVouchersReport.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@ReportProcess(name="AccountingVouchersReport")
public class AccountingVouchersReport extends GenericReport{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The report util service. */
	@EJB
	ReportUtilServiceBean reportUtilService;

	
	/** The accounting report query service bean. */
	@EJB
	AccountingReportQueryServiceBean accountingReportQueryServiceBean;
	
	/** Parameter outPut Voucher Accounting. */
	public static int INDEX_NUMCBTE=0;
	
	/** The index glosa. */
	public static int INDEX_GLOSA=1;
	
	/** The index tipocambiocab. */
	public static int INDEX_TIPOCAMBIOCAB=7;//
	
	/** The index fecha. */
	public static int INDEX_FECHA=6;//
	
	/** The index tipo. */
	public static int INDEX_TIPO=4;
	
	/** The index numreg. */
	public static int INDEX_NUMREG=5;
	
	/** The index cuenta. */
	public static int INDEX_CUENTA=1;//
	
	/** The index monto. */
	public static int INDEX_MONTO=2;//
	
	/** The index operacion. */
	public static int INDEX_OPERACION=3;//
	
	/** The index glosaparticular. */
	public static int INDEX_GLOSAPARTICULAR=8;//

	/**
	 * Instantiates a new accounting vouchers report.
	 */
	public AccountingVouchersReport(){
		
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#generateData(com.pradera.model.report.ReportLogger)
	 */
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {

		ByteArrayOutputStream byteArrayFile =null;
		Long processAccountingPk=null;
		AccountingProcessTo accountingProcessTo=new AccountingProcessTo();
		
		try {
			for(ReportLoggerDetail loggerDetail : getReportLogger().getReportLoggerDetails()) {
				
				if(loggerDetail.getFilterName().equals(ReportConstant.PROCESS_ACCOUNTING_PK)){
					//SET THE PARAMETER [PROCESS ACCOUNTING PK]
					if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
						
						processAccountingPk=Long.parseLong(loggerDetail.getFilterValue());
						accountingProcessTo.setIdAccountingProcessPk(processAccountingPk);
					}
				}
				else if(loggerDetail.getFilterName().equals(ReportConstant.BRANCH)){
					
					if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
						accountingProcessTo.setBranch(Integer.parseInt(loggerDetail.getFilterValue()));
						accountingProcessTo.setProcessType(Integer.parseInt(loggerDetail.getFilterValue()));
					}
				}
				else if(loggerDetail.getFilterName().equals(ReportConstant.DATE_INITIAL_PARAM)){
					
					if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
						accountingProcessTo.setInitialDate(CommonsUtilities.convertStringtoDate(loggerDetail.getFilterValue()));
						accountingProcessTo.setExecutionDate(CommonsUtilities.convertStringtoDate(loggerDetail.getFilterValue()));
					}
				} 
				else if(loggerDetail.getFilterName().equals(ReportConstant.DATE_END_PARAM)){
					
					if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
						accountingProcessTo.setFinalDate(CommonsUtilities.convertStringtoDate(loggerDetail.getFilterValue()));
					}
				}else if(loggerDetail.getFilterName().equals(GeneralConstants.REPORT_BY_PROCESS)){
					
					if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
						accountingProcessTo.setReportByProcess(Integer.parseInt(loggerDetail.getFilterValue()));
					}
				}

			}

			//Generacion del Archivo XML para el Envio de la interfaz 
			byteArrayFile = this.generateFileXml(accountingProcessTo);
			//Si el PK es null, xq vino el reporte por pantalla 
			if(processAccountingPk==null){
				AccountingProcess accountingProcess=accountingReportQueryServiceBean.findAccountingProcessToFilter(accountingProcessTo);
				if(accountingProcess!=null){
					processAccountingPk=accountingProcess.getIdAccountingProcessPk();
				}
			}
			
			reportUtilService.saveFileXMlInterfaceAccounting(byteArrayFile, processAccountingPk, getReportLogger());
			setListByte(Arrays.asList(byteArrayFile.toByteArray()));
		
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		return byteArrayFile;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#getCustomJasperParameters()
	 */
	@Override
	public Map<String, Object> getCustomJasperParameters() {

		Map<String,Object> parametersRequired = new HashMap<>();

		return parametersRequired;
	}

	
	/**
	 * Generate file xml.
	 *
	 * @param accountingProcessTo the accounting process to
	 * @return the byte array output stream
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public ByteArrayOutputStream generateFileXml(AccountingProcessTo accountingProcessTo) throws IOException{

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ByteArrayOutputStream baosReturn = new ByteArrayOutputStream();
		List<AccountingVoucherTo> listAccountingVoucherTo=new ArrayList<AccountingVoucherTo>();
		List<Object[]> listObject=null;
		try {
			
			listObject=this.accountingReportQueryServiceBean.findAccountingVouchers(accountingProcessTo);//processAccountingPk
			
			listAccountingVoucherTo=fillVoucherByObject(listObject);
			
			XMLOutputFactory xof = XMLOutputFactory.newInstance();
			XMLStreamWriter xmlsw;
			
			xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(baos));
			xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");
			//root tag of report
			/**
			 * SERVICES
			 * */
			xmlsw.writeStartElement(ReportConstant.TAG_TRANSACCIONES);

		
		if(!listObject.isEmpty()) {
			//method specific to each implementation of reports
			createBodyReport(xmlsw,  listAccountingVoucherTo);
		} else {
			//when not exist data we need to create empty structure xml
			xmlsw.writeStartElement(ReportConstant.TAG_COMPROBANTE);
				createTagString(xmlsw, ReportConstant.TAG_NUMCBTE,  GeneralConstants.EMPTY_STRING );
				createTagString(xmlsw, ReportConstant.TAG_GLOSA,  GeneralConstants.EMPTY_STRING );
				createTagString(xmlsw, ReportConstant.TAG_TIPOCAMBIOCAB,  GeneralConstants.EMPTY_STRING );
				createTagString(xmlsw, ReportConstant.TAG_FECHA,  GeneralConstants.EMPTY_STRING );
				createTagString(xmlsw, ReportConstant.TAG_TIPO,  GeneralConstants.EMPTY_STRING );
				
				xmlsw.writeStartElement(ReportConstant.TAG_DETALLE);
					xmlsw.writeStartElement(ReportConstant.TAG_REGISTRO);
						createTagString(xmlsw, ReportConstant.TAG_NUMREG,  GeneralConstants.EMPTY_STRING );
						createTagString(xmlsw, ReportConstant.TAG_CUENTA,  GeneralConstants.EMPTY_STRING );
						createTagString(xmlsw, ReportConstant.TAG_MONTO,  GeneralConstants.EMPTY_STRING );
						createTagString(xmlsw, ReportConstant.TAG_OPERACION,  GeneralConstants.EMPTY_STRING );
						createTagString(xmlsw, ReportConstant.TAG_GLOSAPARTICULAR,  GeneralConstants.EMPTY_STRING );
					xmlsw.writeEndElement();//TAG_REGISTRO
				xmlsw.writeEndElement();//TAG_DETALLE
			
			xmlsw.writeEndElement();//TAG_COMPROBANTE
		}

		//close report tag SERVICES
		xmlsw.writeEndElement();
		//close document
		xmlsw.writeEndDocument();
		xmlsw.flush();
        xmlsw.close();
        
        String replaceQuote=baos.toString();
        replaceQuote=replaceQuote.replace("'", "\"");
        baosReturn.write(replaceQuote.getBytes());
        
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return baosReturn;
	}
	
	/**
	 * Creates the body report.
	 * good practice factoring
	 *
	 * @param xmlsw the XMLStreamWriter
	 * @param accountingVoucher the accounting voucher
	 * @throws XMLStreamException the xML stream exception
	 */
	public void createBodyReport(XMLStreamWriter xmlsw,	List<AccountingVoucherTo> accountingVoucher)  throws XMLStreamException{
		int i=1;
		
		for (AccountingVoucherTo accountingVoucherTo : accountingVoucher) {
			
			xmlsw.writeStartElement(ReportConstant.TAG_COMPROBANTE);
			createTagString(xmlsw, ReportConstant.TAG_NUMCBTE,  accountingVoucherTo.getVoucherNumber());
			createTagString(xmlsw, ReportConstant.TAG_GLOSA,  accountingVoucherTo.getGloss() );
			createTagString(xmlsw, ReportConstant.TAG_TIPOCAMBIOCAB,  accountingVoucherTo.getDailyExchange() );
			createTagString(xmlsw, ReportConstant.TAG_FECHA,  accountingVoucherTo.getDate() );
			createTagString(xmlsw, ReportConstant.TAG_TIPO,  accountingVoucherTo.getType() );
			
			
			List<AccountingVoucherRegistryTo> listAccVoucherRegistry=accountingVoucherTo.getListAccountingVoucherRegistryTo();
			xmlsw.writeStartElement(ReportConstant.TAG_DETALLE);
			if(Validations.validateListIsNotNullAndNotEmpty(listAccVoucherRegistry)){
				int j=0;
				for (AccountingVoucherRegistryTo accountingVoucherRegistryTo : listAccVoucherRegistry) {
					xmlsw.writeStartElement(ReportConstant.TAG_REGISTRO);
						createTagString(xmlsw, ReportConstant.TAG_NUMREG,  accountingVoucherRegistryTo.getNumRegister() );
						createTagString(xmlsw, ReportConstant.TAG_CUENTA,  accountingVoucherRegistryTo.getAccountingAccount() );
						createTagString(xmlsw, ReportConstant.TAG_MONTO,  accountingVoucherRegistryTo.getAmount() );
						createTagString(xmlsw, ReportConstant.TAG_OPERACION,  accountingVoucherRegistryTo.getOperation() );
						createTagString(xmlsw, ReportConstant.TAG_GLOSAPARTICULAR, accountingVoucherRegistryTo.getGloss() );
					xmlsw.writeEndElement();//TAG_REGISTRO
					j++;
				}
			}else{
				xmlsw.writeStartElement(ReportConstant.TAG_REGISTRO);
					createTagString(xmlsw, ReportConstant.TAG_NUMREG,  GeneralConstants.EMPTY_STRING );
					createTagString(xmlsw, ReportConstant.TAG_CUENTA,  GeneralConstants.EMPTY_STRING );
					createTagString(xmlsw, ReportConstant.TAG_MONTO,  GeneralConstants.EMPTY_STRING );
					createTagString(xmlsw, ReportConstant.TAG_OPERACION,  GeneralConstants.EMPTY_STRING );
					createTagString(xmlsw, ReportConstant.TAG_GLOSAPARTICULAR,  GeneralConstants.EMPTY_STRING );
				xmlsw.writeEndElement();//TAG_REGISTRO
			}
			
				
			xmlsw.writeEndElement();//TAG_DETALLE
			
			xmlsw.writeEndElement();//TAG_COMPROBANTE
			i++;
		}

		
	}

	/**
	 * Fill voucher by object.
	 *
	 * @param listObject the list object
	 * @return the list
	 */
	public List<AccountingVoucherTo> fillVoucherByObject(List<Object[]> listObject){
		
		List<AccountingVoucherTo> listAccountingVoucherTo=new ArrayList<AccountingVoucherTo>();
		AccountingVoucherTo accountingVoucherTo=new AccountingVoucherTo();
		List<AccountingVoucherRegistryTo> listAccountingVoucherRegistryTo=new ArrayList<>();
		AccountingVoucherRegistryTo accountingVoucherRegistryTo=null;
		String court=null;
		String courtLast=null;
		int i=1,j=0;
		for (Object[] objeto : listObject) {
			court=objeto[0].toString().concat(objeto[4].toString()).concat(objeto[5].toString());

			
			if(!court.equals(courtLast)){//El primero o Cuando cambia de Registro
				accountingVoucherTo=new AccountingVoucherTo();
				j=1;
				accountingVoucherTo.setDailyExchange(new BigDecimal(objeto[INDEX_TIPOCAMBIOCAB].toString()));
				accountingVoucherTo.setDate(objeto[INDEX_FECHA].toString());
				accountingVoucherTo.setGloss(objeto[8].toString());
				accountingVoucherTo.setType(objeto[9].toString());
				accountingVoucherTo.setVoucherNumber(new BigDecimal(i));

				listAccountingVoucherRegistryTo=new ArrayList<>();
				accountingVoucherTo.setListAccountingVoucherRegistryTo(listAccountingVoucherRegistryTo);
				listAccountingVoucherTo.add(accountingVoucherTo);
				i++;
			}
			/**
			    BASE.TIPO, [0] BASE.CUENTA,[1] SUM(BASE.MONTO),[2] 
				BASE.OPERACION,[3] BASE.INSTRUMENT_TYPE,[4] 
				BASE.CURRENCY_TYPE [5], BASE.EXECUTION_DATE [6]
			 */
			accountingVoucherRegistryTo=new AccountingVoucherRegistryTo();
			accountingVoucherRegistryTo.setAccountingAccount(objeto[INDEX_CUENTA].toString());
			accountingVoucherRegistryTo.setAmount(new BigDecimal(objeto[INDEX_MONTO].toString()));
			accountingVoucherRegistryTo.setGloss(objeto[INDEX_GLOSAPARTICULAR].toString());
			accountingVoucherRegistryTo.setOperation(objeto[INDEX_OPERACION].toString());
			accountingVoucherRegistryTo.setNumRegister(new BigDecimal(j));
			j++;
			courtLast=court;
			listAccountingVoucherRegistryTo.add(accountingVoucherRegistryTo);
		}
		
		
		return listAccountingVoucherTo;
		
	}
	
	

}
