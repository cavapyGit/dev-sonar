package com.pradera.report.generation.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.report.ReportFile;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.security.model.type.UserAccountStateType;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceSend;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.type.ReportLoggerStateType;
import com.pradera.report.generation.jms.JMSSenderReportOne;
import com.pradera.report.generation.jms.JMSSenderReportThree;
import com.pradera.report.generation.jms.JMSSenderReportTwo;
import com.pradera.report.generation.type.QueueType;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ReportManageServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/07/2013
 */
@ApplicationScoped
public class ReportManageServiceBean extends CrudDaoServiceBean {
	
	/** The report util service. */
	@EJB
	ReportUtilServiceBean reportUtilService;
	
	/** The jms report one. */
	@EJB
	JMSSenderReportOne jmsReportOne;
	
	/** The jms report two. */
	@EJB
	JMSSenderReportTwo jmsReportTwo;
	
	/** The jms report three. */
	@EJB
	JMSSenderReportThree jmsReportThree;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The client rest service. */
	@Inject
	ClientRestService clientRestService;

	
	/**
	 * Save report execution and
	 * send report instance to jms queue.
	 *
	 * @param reportLogger the report logger
	 * @param reportUser the report user who have grant to see report
	 * @return the report logger
	 * @throws ServiceException the service exception
	 */
	public ReportLogger saveReportExecution(ReportLogger reportLogger,ReportUser reportUser) throws ServiceException {
		try {
			//save report logger
			reportUtilService.registerReportProcessTx(reportLogger);
			//save user who see the report
			List<String> userList = new ArrayList<String>();
			//if ReportUser is null only see report user in audit field
			if(reportUser == null){
				userList.add(reportLogger.getLastModifyUser());
			} else {
				if(StringUtils.isNotBlank(reportUser.getUserName())){
					userList.add(reportUser.getUserName());
				}
				if (reportUser.getPartcipantCode()!=null){
					//service get user of type participant
					List<UserAccountSession> users =  clientRestService.getUsersInformation(UserAccountStateType.CONFIRMED.getCode(), InstitutionType.PARTICIPANT,
							reportUser.getPartcipantCode(), null, null, null);
					for(UserAccountSession account : users){
						userList.add(account.getUserName());
					}
				}
				else if(StringUtils.isNotBlank(reportUser.getIssuerCode())){
					//service get user of type issuer
					List<UserAccountSession> users =  clientRestService.getUsersInformation(UserAccountStateType.CONFIRMED.getCode(), InstitutionType.ISSUER,
							reportUser.getIssuerCode(), null, null, null);
					for(UserAccountSession account : users){
						userList.add(account.getUserName());
					}
				}else if(Validations.validateIsNotNullAndNotEmpty(reportUser.getInstitutionType()) && reportUser.getInstitutionType().equals(InstitutionType.BOLSA.getCode())){
					List<UserAccountSession> users1 =  clientRestService.getUsersInformation(UserAccountStateType.CONFIRMED.getCode(), InstitutionType.BOLSA,
							null, null, null, null);
					for(UserAccountSession account : users1){
						userList.add(account.getUserName());
					}
				}
			}
			//Save users in new trasaction
			reportUtilService.saveReportLoggerUsersTx(reportLogger, userList);
			//send report to queue
			QueueType queue = QueueType.get(reportLogger.getReport().getIndQueue());
			if (queue != null) {
				switch (queue) {
				case ONE:
					jmsReportOne.sendMessage(reportLogger);
					break;
					
				case TWO:
					jmsReportTwo.sendMessage(reportLogger);
					break;
					
				case THREE:
					jmsReportThree.sendMessage(reportLogger);
					break;
				}
			} else{
				log.error("Error wrong queue not exist: "+reportLogger.getReport().getIndQueue());
			}
			return reportLogger;
		} catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			if (reportLogger != null && reportLogger.getIdReportLoggerPk()!=null) {
				reportUtilService.updateStateReportLoggerTx(reportLogger,
						ReportLoggerStateType.FAILED.getCode(), ex.getMessage());
			} 
			throw new ServiceException(ErrorServiceType.REPORT_NOT_EXECUTED, ex.getMessage());
		}
	}
	
	/**
	 * Default constructor. 
	 */
	public ReportManageServiceBean() {
	    // TODO Auto-generated constructor stub
	}
	
	/**
	 * Gets the data report.
	 *
	 * @param reportLoggerId the report logger id
	 * @return the data report
	 * @throws ServiceException the service exception
	 */
	public byte[] getDataReport(Long reportLoggerId) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT RLF.dataReport");
		sbQuery.append("   FROM ReportLoggerFile RLF");
		sbQuery.append("   JOIN RLF.reportLogger RL");
		sbQuery.append("  WHERE RL.idReportLoggerPk =:reportLoggerId");		
		
		Query query = em.createQuery(sbQuery.toString()); 
		query.setParameter("reportLoggerId",reportLoggerId);
		byte[] dataReport = (byte[]) query.getSingleResult();
		return dataReport;
	}

	/**
	 * Save report file.
	 *
	 * @param reportLogger the report logger
	 * @param files the files
	 * @param reportUser the report user
	 * @return the report logger
	 * @throws ServiceException the service exception
	 */
	public ReportLogger saveReportFile(ReportLogger reportLogger,
			List<ReportFile> files, ReportUser reportUser) throws ServiceException {
		
		try{
			reportUtilService.create(reportLogger);
			//save user who see the report
			List<String> userList = new ArrayList<String>();
			
			//if report to save it's a externalInterface File
			if(reportUser.getIndExtInterface()!=null && reportUser.getIndExtInterface().equals(BooleanType.YES.getCode())){
				InstitutionType institutionType = InstitutionType.get(reportUser.getInstitutionType());
				Object institutionCode = null;
				ExternalInterfaceSend externalIntSend = new ExternalInterfaceSend();
				switch(institutionType){
				case PARTICIPANT: case AFP:
					institutionCode = reportUser.getPartcipantCode();break;
				case ISSUER:
					institutionCode = reportUser.getIssuerCode();break;
				}
				//validacion interface 1034
				if(Validations.validateIsNotNullAndNotEmpty(reportLogger.getExternalInterfaceSend().getIdExtInterfaceSendPk())){
					 externalIntSend=find(ExternalInterfaceSend.class, reportLogger.getExternalInterfaceSend().getIdExtInterfaceSendPk());
				}
				if(Validations.validateIsNotNullAndNotEmpty(externalIntSend) &&
						(externalIntSend.getExternalInterface().getIdExtInterfacePk().equals(1034L)||
						 // issue 1340: corrigiendo el acceso a la bitacora de los usuarios que generan reporte BEM, TEM (BCB)
						 externalIntSend.getExternalInterface().getIdExtInterfacePk().equals(1031L)||
						 externalIntSend.getExternalInterface().getIdExtInterfacePk().equals(1032L)||
								externalIntSend.getExternalInterface().getIdExtInterfacePk().equals(GeneralConstants.EXTERNAL_INTERFACE_TR_CR)
								||externalIntSend.getExternalInterface().getIdExtInterfacePk().equals(GeneralConstants.EXTERNAL_INTERFACE_CFI_FSIN))){
					userList.add(reportLogger.getRegistryUser());
				}else{
					List<UserAccountSession> userAccountList = 	clientRestService.getUsersInformation(UserAccountStateType.CONFIRMED.getCode(), 
							institutionType,institutionCode, null, null, BooleanType.YES.getCode());
					
					if(userAccountList!=null){
						for(UserAccountSession userAccount: userAccountList){
							userList.add(userAccount.getUserName());
						}
					}
				}
			}else{
				userList.add(reportLogger.getLastModifyUser());
			}
	    	//Save users in new trasaction
			reportUtilService.saveReportLoggerUsers(reportLogger, userList);
		
			reportUtilService.saveReportFiles(files,reportLogger);
		
			reportLogger.setReportState( ReportLoggerStateType.FINISHED.getCode());
			return reportUtilService.update(reportLogger);
			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			if (reportLogger != null && reportLogger.getIdReportLoggerPk()!=null) {
				reportUtilService.updateStateReportLoggerTx(reportLogger,
						ReportLoggerStateType.FAILED.getCode(), ex.getMessage());
			} 
			throw new ServiceException(ErrorServiceType.REPORT_NOT_EXECUTED, ex.getMessage());
		}
	}
	
	/**
	 * Gets the data report object.
	 *
	 * @param reportLoggerId the report logger id
	 * @return the data report object
	 * @throws ServiceException the service exception
	 */
	public Object[] getDataReportObject(Long reportLoggerId) throws ServiceException {
		Object [] objects = null;
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();		
		//sbQuery.append(" SELECT RLF.nameFile, RLF.nameTrace "); //reptodisk
		sbQuery.append(" SELECT RLF.nameFile, RLF.dataReport ");
		sbQuery.append(" FROM ReportLoggerFile RLF ");  
		sbQuery.append(" JOIN RLF.reportLogger RL ");
		sbQuery.append(" WHERE RL.idReportLoggerPk =:reportLoggerId "); 		
		parameters.put("reportLoggerId", reportLoggerId);
		objects = (Object[]) findObjectByQueryString(sbQuery.toString(),parameters);		
		return objects;
	}

}
