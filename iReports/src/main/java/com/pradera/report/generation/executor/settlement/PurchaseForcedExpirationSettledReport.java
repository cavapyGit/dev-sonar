package com.pradera.report.generation.executor.settlement;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserFilterTO;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;

@ReportProcess(name = "PurchaseForcedExpirationSettledReport")
public class PurchaseForcedExpirationSettledReport extends GenericReport{

	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;
	@Inject
	PraderaLogger log;
	
	@EJB
	ParticipantServiceBean participantServiceBean;
	
	@Inject
	ClientRestService clientRestService;

	public PurchaseForcedExpirationSettledReport() {
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParametersQueryReport() {
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		if (listaLogger == null) {
			listaLogger = new ArrayList<ReportLoggerDetail>();
		}
	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		
		String isParticipant = null;
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		Map<String, Object> parametersRequired = new HashMap<>();
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.PARTICIPANT_PARAM))
				isParticipant = r.getFilterValue();
		}
		UserFilterTO userFilter=new UserFilterTO();
		userFilter.setUserName(getReportLogger().getRegistryUser());

		//SI ES NULO LO MUESTRO
		parametersRequired.put("isParticipant", isParticipant);
		
		// TODO Auto-generated method stub
		return parametersRequired;
	}
}
