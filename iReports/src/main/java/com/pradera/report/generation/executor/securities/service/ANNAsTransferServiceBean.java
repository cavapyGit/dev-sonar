package com.pradera.report.generation.executor.securities.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class ANNAsTransferServiceBean extends CrudDaoServiceBean{
	
	/*it only returns the query to send to the jasper as PARAMETER*/
    public String getQueryAnnasTransfer(GenericsFiltersTO genericsTO){
    	StringBuilder sbQuery= new StringBuilder();
    	
    	
    	sbQuery.append("	SELECT ");
		sbQuery.append("		S.ID_SECURITY_CODE_PK, ");
		sbQuery.append("		S.ID_ISIN_CODE, ");
		sbQuery.append("		S.CFI_CODE, ");
		sbQuery.append("		TO_CHAR(S.REGISTRY_DATE,'DD/MM/YYYY') REGISTRY_DATE, ");
		sbQuery.append("		S.REGISTRY_USER USER_REGISTER, ");
		sbQuery.append("		TO_CHAR(SIA.GENERATION_DATE,'DD/MM/YYYY hh:mi:ss') GENERATION_DATE, ");
		sbQuery.append("		SIA.NAME_FILE_INTERFACE ");
		  
		sbQuery.append("	FROM SECURITY_INTERFACE_ANNA SIA ");
		sbQuery.append("		INNER JOIN SECURITY S ON S.ID_SECURITY_CODE_PK = SIA.ID_SECURITY_CODE_FK ");
    	
    	
		if(genericsTO.getInitialDt()!=null && genericsTO.getFinalDt()!=null){
    		sbQuery.append(" and TRUNC(SIA.GENERATION_DATE) between TO_DATE(:iniDt,'dd/MM/yyyy') and TO_DATE(:endDt,'dd/MM/yyyy')");
    	}
		if(genericsTO.getFileCode()!=null){
			sbQuery.append(" and SIA.NAME_FILE_INTERFACE like :file_code");
		}
		sbQuery.append(" ORDER BY S.ID_SECURITY_CODE_PK ");
		
		String strQueryFormatted = sbQuery.toString();

		if(genericsTO.getInitialDt()!=null && genericsTO.getFinalDt()!=null){
			strQueryFormatted= strQueryFormatted.replace(":iniDt", "'"+genericsTO.getInitialDt()+"'");
	    	strQueryFormatted= strQueryFormatted.replace(":endDt", "'"+genericsTO.getFinalDt()+"'");
    	}
		if(genericsTO.getIsinCode()!=null){
    		strQueryFormatted= strQueryFormatted.replace(":file_code", "'%"+genericsTO.getFileCode()+"%'");
		}
    	
    	return strQueryFormatted;
    }

}
