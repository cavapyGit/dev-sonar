package com.pradera.report.generation.jasper;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.inject.Alternative;
import javax.inject.Inject;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.fill.JRAbstractLRUVirtualizer;
import net.sf.jasperreports.engine.fill.JRGzipVirtualizer;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.Exporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.configuration.StageDependent;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.monitoring.MonitoringType;
import com.pradera.core.framework.monitoring.PerformanceAuditor;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.model.report.type.ReportType;
import com.pradera.report.generation.api.ReportGenerationFramework;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.service.ReportUtilServiceBean;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class ReportGeneratorJasper.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/10/2014
 */
@Alternative
public abstract class ReportGeneratorJasper implements ReportGenerationFramework, Serializable {
	
	

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3483475556299766799L;
	
	/** The report util service. */
	@EJB
	ReportUtilServiceBean reportUtilService;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The jndi idepositary. */
	@Inject @Configurable
	protected String jndiIdepositary;
	
	/** The folder pentaho. */
	@Inject @StageDependent
	protected String folderJasper;
	
	/** The template report url. */
	private String templateReportUrl;
	
	/** The xml data source. */
	private String xmlDataSource;
	
	/** The report query. */
	private String reportQuery;
	
	/** The sub report query. */
	private String subReportQuery;
	
	/** The report logger. */
	private ReportLogger reportLogger;
	
	/** The sub xml data source. */
	private String subXmlDataSource;
	
	/** The custom jasper parameters. */
	private Map<String,Object> customJasperParameters;
	
	/** The report type. */
	private ReportType reportType;
	
	/** The query name. */
	protected  String QUERY_NAME = "QueryReport";
	
	/** The query name. */
	protected  String SUB_QUERY_NAME = "SubQueryReport";
	
	//parameters requireds in report
	
	/** The report title. */
	private String REPORT_TITLE="report_title";
	
	/** The user name. */
	private String USER_NAME="user_name";
	
	/** The mnemonic entity. */
	private String MNEMONIC_ENTITY="mnemonic_entity";
	
	/** The mnemonic report. */
	private String MNEMONIC_REPORT="mnemonic_report";
	
	/** The clasification report. */
	private String CLASIFICATION_REPORT="clasification_report";
	
	/** The generation date. */
	private String GENERATION_DATE="generation_date";
	
	/** The generation date. */
	private String LOGO_PATH="logo_path";
	
	/** The subreport path. */
	private String SUBREPORT_PATH="subreport_path";
	

	/* (non-Javadoc)
	 * @see com.pradera.report.generation.api.ReportGenerationFramework#setReportLogger(com.pradera.model.report.ReportLogger)
	 */
	@Override
	public void setReportLogger(ReportLogger reportLogger) {
		this.reportLogger=reportLogger;
	}

	/* (non-Javadoc)
	 * @see com.pradera.report.generation.api.ReportGenerationFramework#setReportQuery(java.lang.String)
	 */
	@Override
	public void setReportQuery(String reportQuery) {
		this.reportQuery=reportQuery;
	}

	/* (non-Javadoc)
	 * @see com.pradera.report.generation.api.ReportGenerationFramework#setSubReportQuery(java.lang.String)
	 */
	@Override
	public void setSubReportQuery(String subReportQuery) {
		this.subReportQuery=subReportQuery;
	}

	/* (non-Javadoc)
	 * @see com.pradera.report.generation.api.ReportGenerationFramework#setTemplateReportUrl(java.lang.String)
	 */
	@Override
	public void setTemplateReportUrl(String templateReportUrl) {
		this.templateReportUrl=templateReportUrl;
	}

	/* (non-Javadoc)
	 * @see com.pradera.report.generation.api.ReportGenerationFramework#setXmlDataSource(java.lang.String)
	 */
	@Override
	public void setXmlDataSource(String xmlDataSource) {
		this.xmlDataSource=xmlDataSource;
	}

	/* (non-Javadoc)
	 * @see com.pradera.report.generation.api.ReportGenerationFramework#setXmlSubDataSource(java.lang.String)
	 */
	@Override
	public void setXmlSubDataSource(String subXmlDataSource) {
		this.subXmlDataSource=subXmlDataSource;
	}
	
	/**
	 * Gets the report logger.
	 *
	 * @return the report logger
	 */
	public ReportLogger getReportLogger() {
		return reportLogger;
	}
	
	/**
	 * Gets the report definition.
	 *
	 * @return the report definition
	 */
	public JasperReport getReportDefinition(){
		try{
			InputStream inputStream= JRLoader.getLocationInputStream(folderJasper+"/"+templateReportUrl);
			if(inputStream!= null){
				return (JasperReport)JRLoader.loadObject(inputStream);
			} else {
				throw new JRException("Template not loading");
			}
		}catch(JRException ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Gets the data source.
	 *
	 * @return the data source
	 */
	public abstract JRDataSource getDataSource();
	
	/**
	 * Release resources.
	 */
	public abstract void releaseResources();
	
	/**
	 * Gets the report parameters.
	 *
	 * @return the report parameters
	 */
	public Map<String, Object> getReportParameters(){
		Map<String, Object> parametersRequired = null;
		if(reportLogger != null) {
			parametersRequired = new HashMap<String, Object>();
			parametersRequired.put(REPORT_TITLE, reportLogger.getReport().getTitle());		
			parametersRequired.put(MNEMONIC_REPORT, reportLogger.getReport().getMnemonico());
			parametersRequired.put(CLASIFICATION_REPORT, reportLogger.getReport().getReportClasification());
			parametersRequired.put(GENERATION_DATE,
					CommonsUtilities.convertDateToString(reportLogger.getLastModifyDate(), "dd/MM/yyyy hh:mm a"));
			parametersRequired.put(USER_NAME, reportLogger.getRegistryUser());
			parametersRequired.put(MNEMONIC_ENTITY,"");
			String separator = System.getProperty("file.separator");
			String basePath = System.getProperty("java.io.tmpdir");
//			parametersRequired.put(LOGO_PATH,basePath+separator+ ReportConstant.LOGO_RESOURCE_PATH);
			if(StringUtils.isNotBlank(reportLogger.getReport().getSubQueryReport())){
				parametersRequired.put(SUBREPORT_PATH, folderJasper+GeneralConstants.SLASH+reportLogger.getReport().getSubQueryReport()+GeneralConstants.SLASH);
			}
			//jasper
			//Set the default language to English to set the correct pattern to numeric fields ("." for decimal separator and "," for Grouping separator)
			parametersRequired.put(JRParameter.REPORT_LOCALE, Locale.ENGLISH);
		}
		return parametersRequired;
	}

	/* (non-Javadoc)
	 * @see com.pradera.report.generation.api.ReportGenerationFramework#generateReports()
	 */
	@Override
	@PerformanceAuditor(MonitoringType.REPORT)
	@Performance
	public void generateReports(ReportLogger reportLog) {
		//generation of report
		JasperReport jrTemplate = null;
		JasperPrint jasperPrint = null;
		JRAbstractLRUVirtualizer virtualizer = null;
		try{
			jrTemplate = this.getReportDefinition();
			//Virtualization for performance
			virtualizer = new JRGzipVirtualizer(1000);//Testing for better value or change implementation
			Map<String, Object> reportParameters = this.getReportParameters();
			reportParameters.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
			//check if exist custom jasper parameters
			if(getCustomJasperParameters()!= null){
				reportParameters.putAll(getCustomJasperParameters());
			}
			//generating report in jasper format
			switch (getReportType()) {
			case QUERY:
				jasperPrint = JasperFillManager.fillReport(jrTemplate, reportParameters);
				break;
			case BEAN:
				jasperPrint = JasperFillManager.fillReport(jrTemplate, reportParameters,this.getDataSource());
				break;
			}
			
			//performance
			virtualizer.setReadOnly(true);
			//export in format wanted
			Map<ReportFormatType,byte[]> files = new HashMap<ReportFormatType, byte[]>();
			//according changes only generate one format on time
			ReportFormatType reportFormat = ReportFormatType.get(reportLogger.getReportFormat());
			switch (reportFormat) {
			case PDF:
				files.put(ReportFormatType.PDF, JasperExportManager.exportReportToPdf(jasperPrint));
				break;
			case XLS:
				ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
				Exporter exporter = new JRXlsxExporter();
				exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
				exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));
				SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
				configuration.setOnePagePerSheet(false);
				//configuration.setDetectCellType(true);
				//configuration.setCollapseRowSpan(false);				
				configuration.setMaxRowsPerSheet(ReportConstant.INTEGER_MAX_REPORTS);
				configuration.setWhitePageBackground(false);
				configuration.setRemoveEmptySpaceBetweenRows(true);
				exporter.setConfiguration(configuration);
				exporter.exportReport();
				files.put(ReportFormatType.XLSX, outputStream.toByteArray());
				break;
			case EXCEL_DELIMITADO:
				Object obj = reportParameters.get(ReportConstant.REPORT_EXCEL_DELIMITADO);
				byte[] xlsx = null;
				if(obj!=null) {
					xlsx = (byte[])obj;
				}
				files.put(ReportFormatType.XLSX, xlsx);
				break;				
			default:
				//if format is txt is already generate from bean associate to report
				break;
			}
			//save files only have data
			if(!files.isEmpty()){
				reportUtilService.saveReportsGeneratesTx(getReportLogger(),files);
		    }
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException(ex);
		} finally {
			if(virtualizer != null){
				virtualizer.cleanup();
			}
			//release
			releaseResources();
		}
	}

	/**
	 * Gets the sub xml data source.
	 *
	 * @return the sub xml data source
	 */
	public String getSubXmlDataSource() {
		return subXmlDataSource;
	}

	/**
	 * Sets the sub xml data source.
	 *
	 * @param subXmlDataSource the new sub xml data source
	 */
	public void setSubXmlDataSource(String subXmlDataSource) {
		this.subXmlDataSource = subXmlDataSource;
	}

	/**
	 * Gets the xml data source.
	 *
	 * @return the xml data source
	 */
	public String getXmlDataSource() {
		return xmlDataSource;
	}

	/**
	 * Gets the report query.
	 *
	 * @return the report query
	 */
	public String getReportQuery() {
		return reportQuery;
	}

	/**
	 * Gets the sub report query.
	 *
	 * @return the sub report query
	 */
	public String getSubReportQuery() {
		return subReportQuery;
	}

	/**
	 * Gets the custom jasper parameters.
	 *
	 * @return the custom jasper parameters
	 */
	public Map<String,Object> getCustomJasperParameters() {
		return customJasperParameters;
	}

	/* (non-Javadoc)
	 * @see com.pradera.report.generation.api.ReportGenerationFramework#setCustomJasperParameters(java.util.Map)
	 */
	@Override
	public void setCustomJasperParameters(Map<String,Object> customJasperParameters) {
		this.customJasperParameters = customJasperParameters;
	}

	/**
	 * Gets the report type.
	 *
	 * @return the report type
	 */
	public ReportType getReportType() {
		return reportType;
	}

	/**
	 * Sets the report type.
	 *
	 * @param reportType the new report type
	 */
	public void setReportType(ReportType reportType) {
		this.reportType = reportType;
	}

}
