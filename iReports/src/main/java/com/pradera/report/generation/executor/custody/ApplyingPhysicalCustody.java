package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.service.HolderQueryServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;

@ReportProcess(name = "ApplyingPhysicalCustody")
public class ApplyingPhysicalCustody extends GenericReport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ParameterServiceBean parameterService;

	@EJB
	private HolderQueryServiceBean holderService;
	
	@Inject
	PraderaLogger log;
	
	public ApplyingPhysicalCustody() {
	}
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParametersQueryReport() {

	}
	@Override
	public Map<String, Object> getCustomJasperParameters(){
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		GenericsFiltersTO applyingPhysicalCustody = new GenericsFiltersTO();
		String stateValue=null;
		
		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("participant")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					applyingPhysicalCustody.setParticipantDesc(listaLogger.get(i).getFilterDescription());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("holder")){
				if (listaLogger.get(i).getFilterValue()!=null && !listaLogger.get(i).getFilterValue().equals("")){
					applyingPhysicalCustody.setCui(listaLogger.get(i).getFilterDescription());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("security_currency")){
				if (listaLogger.get(i).getFilterValue()!=null && !listaLogger.get(i).getFilterValue().equals("")){
					applyingPhysicalCustody.setCurrency(listaLogger.get(i).getFilterDescription());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("account_number")){
				if (listaLogger.get(i).getFilterValue()!=null && !listaLogger.get(i).getFilterValue().equals("")){
						HolderAccount holderAccount = holderService.find(HolderAccount.class,Long.parseLong(listaLogger.get(i).getFilterValue()));
						applyingPhysicalCustody.setAccountNumber(holderAccount.getAccountNumber().toString());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("state")){
				if (listaLogger.get(i).getFilterValue()!=null && !listaLogger.get(i).getFilterValue().equals("")){
					applyingPhysicalCustody.setState(listaLogger.get(i).getFilterValue());
					stateValue=listaLogger.get(i).getFilterDescription();
				}
			}
		}
		
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filtro = new ParameterTableTO();
		Map<Integer,String> securityclass = new HashMap<Integer, String>();
		try {
			filtro.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filtro)) {
				securityclass.put(param.getParameterTablePk(), param.getText1());
			}
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		parametersRequired.put("p_securityclass", securityclass);
		parametersRequired.put("p_accountNumber", applyingPhysicalCustody.getAccountNumber());
		if(applyingPhysicalCustody.getCurrency()!=null){
			parametersRequired.put("p_securityCurrency", applyingPhysicalCustody.getCurrency());
		}
		if(applyingPhysicalCustody.getParticipantDesc()!=null){
			parametersRequired.put("participant_desc", applyingPhysicalCustody.getParticipantDesc());
		}
		if(applyingPhysicalCustody.getCui()!=null){
			parametersRequired.put("holder_desc", applyingPhysicalCustody.getCui());
		}
		if(applyingPhysicalCustody.getState()!=null){
			parametersRequired.put("state", Long.parseLong(applyingPhysicalCustody.getState()));
			parametersRequired.put("stateValue", stateValue);
		}

		return parametersRequired;
	}	
}
	
