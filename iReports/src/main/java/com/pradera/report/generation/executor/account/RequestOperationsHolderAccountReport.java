package com.pradera.report.generation.executor.account;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "RequestOperationsHolderAccountReport")
public class RequestOperationsHolderAccountReport extends GenericReport {
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@Inject
	PraderaLogger log;

	
	public RequestOperationsHolderAccountReport() {
	}
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParametersQueryReport() {

	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		
		Map<Integer,String> typeDocument = new HashMap<Integer, String>();
		Map<Integer,String> requestType = new HashMap<Integer, String>();
		Map<Integer,String> motive = new HashMap<Integer, String>();
		Map<Integer,String> requestState = new HashMap<Integer, String>();
		
		try {
		
			filter.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				typeDocument.put(param.getParameterTablePk(), param.getIndicator1());
			}
			
			filter.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_TYPE_ACTION.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				requestType.put(param.getParameterTablePk(), param.getParameterName());
			}
			//MASTER_TABLE_FK in (224,225,245);
			//224
			filter.setMasterTableFk(MasterTableType.HOLDER_REQUEST_REJECT_MOTIVE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				motive.put(param.getParameterTablePk(), param.getDescription());
			}//225
			filter.setMasterTableFk(MasterTableType.HOLDER_REQUEST_ANNULAR_MOTIVE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				motive.put(param.getParameterTablePk(), param.getDescription());
			}//245
			filter.setMasterTableFk(MasterTableType.MOTIVE_BLOCK_HOLDERACCOUNT.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				motive.put(param.getParameterTablePk(), param.getDescription());
			}
			
			filter.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_REQUEST_STATUS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				requestState.put(param.getParameterTablePk(), param.getDescription());
			}
		
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		parametersRequired.put("p_typeDocument", typeDocument);
		parametersRequired.put("p_requestType", requestType);
		parametersRequired.put("p_motive", motive);
		parametersRequired.put("p_requestState", requestState);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		
		return parametersRequired;
	}
	
	

}
