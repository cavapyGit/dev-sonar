package com.pradera.report.generation.executor.account.service;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.integration.common.validation.Validations;
import com.pradera.report.generation.executor.custody.to.ClientPortafolioTO;
import com.pradera.report.generation.service.ReportManageServiceBean;
/**
* 
* <ul><li>Copyright EDV 2020.</li></ul> 
* @author RCHIARA.
* 
*
*/
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class ApplicantReportServiceBean {
	@Inject
	ReportManageServiceBean reportManageServiceBean;

	public ApplicantReportServiceBean() {
		
	}
	
	/**
	 * 
	 * 
	 * 
	 * */
    public String getQueryClientPortafolio(ClientPortafolioTO clientPortafolioTO, Long idStkCalcProcess){
    	StringBuilder sbQuery= new StringBuilder();
    	
		sbQuery.append(" SELECT	");
  		sbQuery.append(" 	TO_NUMBER(ID_PARTICIPANT_PK) ID_PARTICIPANT_PK, ");
		sbQuery.append(" 	DESCRIPTION, ");
		sbQuery.append(" 	MNEMONIC, ");
		sbQuery.append(" 	TO_NUMBER(CURRENCY) CURRENCY, ");
		sbQuery.append(" 	CURRENCY_MNEMONIC, ");
		sbQuery.append(" 	MNEMONIC_ISSUER MNEMONIC_ISSUER, ");
		sbQuery.append(" 	BUSINESS_NAME BUSINESS_NAME, ");
		sbQuery.append(" 	TO_NUMBER(ID_HOLDER_ACCOUNT_PK) ID_HOLDER_ACCOUNT_PK, ");
		sbQuery.append(" 	HOLDERS HOLDERS, ");
		sbQuery.append(" 	HOLDERSDESC HOLDERSDESC, ");
		sbQuery.append(" 	TO_NUMBER(ACCOUNT_NUMBER) ACCOUNT_NUMBER, ");
		sbQuery.append(" 	TO_NUMBER(SECURITY_CLASS) SECURITY_CLASS, ");
		sbQuery.append(" 	SECURITY_CLASS_MNEMONIC SECURITY_CLASS_MNEMONIC, ");
		sbQuery.append(" 	ID_SECURITY_CODE_PK ID_SECURITY_CODE_PK, ");
		sbQuery.append(" 	FECHAEMISION FECHAEMISION, ");
		sbQuery.append(" 	TO_NUMBER(SECURITY_DAYS_TERM) SECURITY_DAYS_TERM, ");
		sbQuery.append(" 	TO_NUMBER(INITIAL_NOMINAL_VALUE) INITIAL_NOMINAL_VALUE, ");
		sbQuery.append(" 	TO_NUMBER(INTEREST_RATE) INTEREST_RATE, ");
		sbQuery.append(" 	TO_NUMBER(SECURITY_TYPE) SECURITY_TYPE, ");
		sbQuery.append(" 	TO_NUMBER(VALORFINAL) VALORFINAL, ");
		sbQuery.append(" 	TO_NUMBER(DIASVIGENTES) DIASVIGENTES, ");
		sbQuery.append(" 	TO_NUMBER(VALOR_PRESENTE) VALOR_PRESENTE, ");
		sbQuery.append(" 	TOTAL_BALANCE, ");
		sbQuery.append(" 	AVAILABLE_BALANCE, ");
		sbQuery.append(" 	PAWN_BALANCE, ");
		sbQuery.append(" 	BAN_BALANCE, ");
		sbQuery.append(" 	OTHER_BLOCK_BALANCE, ");
		sbQuery.append(" 	ACCREDITATION_BALANCE, ");
		sbQuery.append(" 	MARGIN_BALANCE, ");
		sbQuery.append(" 	REPORTING_BALANCE, ");
		sbQuery.append(" 	REPORTED_BALANCE, ");
		sbQuery.append(" 	PURCHASE_BALANCE, ");
		sbQuery.append(" 	SALE_BALANCE, ");
		sbQuery.append(" 	TRANSIT_BALANCE, ");
		sbQuery.append(" 	TASA_MERCADO, ");
		sbQuery.append(" 	PRECIO_MERCADO, ");
		sbQuery.append(" 	TOTAL_AMOUNT_MF, ");
		sbQuery.append(" 	THEORIC_RATE, ");
		sbQuery.append(" 	THEORIC_PRICE, ");
		sbQuery.append(" 	TOTAL_AMOUNT, ");
		sbQuery.append(" 	CURRENT_NOMINAL_VALUE, ");
		sbQuery.append(" 	CURRENT_TOTAL_AMOUNT, ");
		sbQuery.append(" 	alternative_code, ");
		sbQuery.append(" 	to_char(expiration_date,'dd/mm/yyyy'), ");
		sbQuery.append(" 	holder_type, ");
		sbQuery.append(" 	document_number, ");
		sbQuery.append(" 	serie, ");		
		sbQuery.append(" 	economicActivity, ");
		sbQuery.append(" 	initialNValueUSD, ");
		sbQuery.append(" 	currentNValueUSD, ");
		sbQuery.append(" 	marketPriceUSD ");
		
		sbQuery.append(" FROM ");
		sbQuery.append(" 	(select /*+ USE_NL(ISS,SECU,P,MFV) */ ");
		sbQuery.append(" 	p.id_participant_pk||HA.id_holder_account_pk||secu.id_security_code_pk secuencia, ");
		sbQuery.append(" 	p.id_participant_pk, ");
		sbQuery.append(" 	p.description, ");
		sbQuery.append(" 	p.mnemonic, ");																										 
		sbQuery.append(" 	secu.currency, ");																																				 
		sbQuery.append(" 	(select pt.text1 from parameter_table pt where pt.parameter_table_pk=secu.currency) currency_mnemonic, ");														 
		sbQuery.append(" 	iss.mnemonic as MNEMONIC_ISSUER, ");																															 
		sbQuery.append(" 	iss.business_name, ");																																			 
		sbQuery.append(" 	HA.id_holder_account_pk, ");																																	 
		sbQuery.append(" 	(SELECT LISTAGG(HAD.ID_HOLDER_FK , ',' ||chr(13) ) WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");																			 
		sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD ");																																 
		sbQuery.append(" 	WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ");																									 
		sbQuery.append(" 	) Holders, ");
		sbQuery.append("    (SELECT LISTAGG((select h.full_name from holder h where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK) ||chr(13)) WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
		sbQuery.append("    FROM HOLDER_ACCOUNT_DETAIL HAD	");
		sbQuery.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK " );
		sbQuery.append(" 	) HoldersDesc, ");
		sbQuery.append(" 	HA.account_number, ");
		sbQuery.append(" 	secu.ALTERNATIVE_CODE, ");
		sbQuery.append(" 	secu.expiration_date, ");
		sbQuery.append(" 	(select parameter_name from parameter_table where parameter_table_pk=(SELECT distinct (select h.holder_type from holder h where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK)) as holder_type, ");
		sbQuery.append(" 	(SELECT LISTAGG((select h.document_number from holder h where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK) ||chr(13)) WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)     FROM HOLDER_ACCOUNT_DETAIL HAD	    WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK  	) document_number, ");
		sbQuery.append(" 	secu.id_security_code_only as serie, ");
		sbQuery.append(" 	secu.security_class, ");																																	 
		sbQuery.append(" 	(select pt.text1 from parameter_table pt where pt.parameter_table_pk=secu.security_class) security_class_mnemonic, ");											 
		sbQuery.append(" 	secu.id_security_code_pk, ");																																	 
		sbQuery.append(" 	(to_char(secu.issuance_date,'dd/MM/YYYY')) fechaEmision, ");																									 
		sbQuery.append(" 	secu.security_days_term, ");																																	 
		sbQuery.append(" 	secu.initial_nominal_value, ");
		sbQuery.append(" 	secu.interest_rate, ");
		sbQuery.append(" 	secu.security_type, ");
		sbQuery.append("	( (secu.INITIAL_NOMINAL_VALUE) * (  1+(secu.INTEREST_RATE/100)*  ( secu.security_days_term )/ 360 )) as ValorFinal,  ");
		sbQuery.append(" 	GREATEST(0,nvl(trunc(secu.expiration_date) - TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY'),0)) DiasVigentes, ");
		sbQuery.append("	ROUND( ( (secu.INITIAL_NOMINAL_VALUE) * (  1+(secu.INTEREST_RATE/100)*  ( secu.security_days_term )/ 360 )) / (1+(secu.INTEREST_RATE/100) * GREATEST(0,nvl(trunc(secu.expiration_date) -  TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY'),0)) /360 ) ,  2)  VALOR_PRESENTE   ,  ");
		sbQuery.append(" 	MFV.total_balance, "); 
		sbQuery.append(" 	MFV.available_balance, ");
		sbQuery.append(" 	MFV.pawn_balance, "); 
		sbQuery.append(" 	MFV.ban_balance, ");
		sbQuery.append(" 	MFV.other_block_balance, ");
		sbQuery.append(" 	MFV.accreditation_balance, ");
		sbQuery.append(" 	MFV.margin_balance, ");
		sbQuery.append(" 	MFV.reporting_balance, ");
		sbQuery.append(" 	MFV.reported_balance, ");
		sbQuery.append(" 	MFV.purchase_balance, ");
		sbQuery.append(" 	MFV.sale_balance, ");
		sbQuery.append(" 	MFV.transit_balance, ");
		sbQuery.append(" 	NVL(MFV.market_rate,0) TASA_MERCADO, ");
		sbQuery.append(" 	NVL(MFV.market_price,0) PRECIO_MERCADO, ");
		sbQuery.append(" 	NVL(ROUND(MFV.market_price,2),0) * MFV.total_balance as TOTAL_AMOUNT_MF, ");
		sbQuery.append(" 	NVL(MFV.THEORIC_RATE,0) THEORIC_RATE, ");
		sbQuery.append(" 	NVL(MFV.THEORIC_PRICE,0) THEORIC_PRICE, ");
		sbQuery.append(" 	NVL(MFV.THEORIC_PRICE,0) * MFV.total_balance total_amount, ");
		sbQuery.append(" 	NVL(CASE WHEN HA.ACCOUNT_TYPE in ( 106, 108 ) THEN 'PARTICULARES'                                                                              ");
		sbQuery.append("         WHEN HA.ACCOUNT_TYPE = 107 THEN ( select PT_SUB.DESCRIPTION                                                                            ");
		sbQuery.append("                                            from holder H_SUB                                                                                   ");
		sbQuery.append("                                            inner join HOLDER_ACCOUNT_DETAIL HAD_SUB on HAD_SUB.ID_HOLDER_FK = H_SUB.ID_HOLDER_PK               ");
		sbQuery.append("                                            inner join HOLDER_ACCOUNT HA_SUB ON HAD_SUB.ID_HOLDER_ACCOUNT_FK = HA_SUB.ID_HOLDER_ACCOUNT_PK      ");
		sbQuery.append("                                            inner join PARAMETER_TABLE PT_SUB on PT_SUB.PARAMETER_TABLE_PK = H_SUB.ECONOMIC_ACTIVITY            ");
		sbQuery.append("                                            where 1 = 1                                                                                         ");
		sbQuery.append("                                            and HA_SUB.ID_HOLDER_ACCOUNT_PK = HA.ID_HOLDER_ACCOUNT_PK)                                          ");
		sbQuery.append("        END,'POR COMPLETAR') economicActivity,                                                                                                 ");
		sbQuery.append("	round(((MFV.total_balance * secu.initial_nominal_value) *    ");  
		sbQuery.append("	CASE WHEN secu.CURRENCY = 127 THEN 1 /                                                                                          ");  
		sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
		sbQuery.append("						 AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)                                              ");  
		sbQuery.append("		 WHEN secu.CURRENCY IN (1734,1304) THEN 1 *                                                                                 ");  
		sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
		sbQuery.append("						AND D.ID_CURRENCY = secu.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /                                   ");  
		sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
		sbQuery.append("						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)                                               ");  
		sbQuery.append("		 WHEN secu.CURRENCY IN (430,1853) THEN 1 ELSE 1 END ),2) initialNValueUSD,                                                  ");  
		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
			sbQuery.append("	round(((MFV.total_balance * ");  
			sbQuery.append("			(DECODE(SECU.SECURITY_CLASS,426,ROUND( NVH.NOMINAL_VALUE ,4),        ROUND( NVH.NOMINAL_VALUE ,2))) 			");
			sbQuery.append("	) *       ");                                                                                     
		
		}else{
			sbQuery.append("	round(((MFV.total_balance *  DECODE(SECU.SECURITY_CLASS,426,ROUND( secu.CURRENT_NOMINAL_VALUE ,4),        ROUND( secu.CURRENT_NOMINAL_VALUE ,2))) *                                                                   ");  
		}
		sbQuery.append("	CASE WHEN secu.CURRENCY = 127 THEN 1 /                                                                                          ");  
		sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
		sbQuery.append("						 AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)                                              ");  
		sbQuery.append("		 WHEN secu.CURRENCY IN (1734,1304) THEN 1 *                                                                                 ");  
		sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
		sbQuery.append("						AND D.ID_CURRENCY = secu.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /                                   ");  
		sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
		sbQuery.append("						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)                                               ");  
		sbQuery.append("		 WHEN secu.CURRENCY IN (430,1853) THEN 1 ELSE 1 END ),2) currentNValueUSD,                                                  ");  
		sbQuery.append("	round(((NVL(MFV.total_balance * ROUND(NVL(MFV.market_price,0),2),0)) *                                                                                              ");  
		sbQuery.append("	CASE WHEN secu.CURRENCY = 127 THEN 1 /                                                                                          ");  
		sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
		sbQuery.append("						 AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)                                              ");  
		sbQuery.append("		 WHEN secu.CURRENCY IN (1734,1304) THEN 1 *                                                                                 ");  
		sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
		sbQuery.append("						AND D.ID_CURRENCY = secu.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /                                   ");  
		sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
		sbQuery.append("						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)                                               ");  
		sbQuery.append("		 WHEN secu.CURRENCY IN (430,1853) THEN 1 ELSE 1 END ),2) marketPriceUSD,                                                    ");  
		sbQuery.append("    DECODE(secu.security_class,426,ROUND(");
		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
			sbQuery.append("			NVH.NOMINAL_VALUE 			");
		}else{
			sbQuery.append("			secu.current_nominal_value ");
		}
		sbQuery.append("		  	,4),ROUND( ");
		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
			sbQuery.append("			NVH.NOMINAL_VALUE			");
		}else{
			sbQuery.append("			secu.current_nominal_value ");
		}
		sbQuery.append(" 			,2)) ");
		sbQuery.append(" 	as current_nominal_value, ");
		sbQuery.append("    DECODE(secu.security_class,426,ROUND( ");
		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
			sbQuery.append("			NVH.NOMINAL_VALUE			");
		}else{
			sbQuery.append("			secu.current_nominal_value ");
		}
		sbQuery.append("		  	,4),ROUND( ");
		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
			sbQuery.append("			NVH.NOMINAL_VALUE			");
		}else{
			sbQuery.append("			secu.current_nominal_value ");
		}
		sbQuery.append(" 			,2)) * MFV.total_balance ");
		sbQuery.append(" 	as current_total_amount ");
		
		sbQuery.append(" FROM holder_account ha ");
		sbQuery.append(" 	inner join market_fact_view mfv on mfv.id_holder_account_pk = HA.id_holder_account_pk		");
		sbQuery.append(" 	Inner join participant p ON mfv.id_participant_pk = p.id_participant_pk						");
		sbQuery.append(" 	Inner join security secu ON mfv.id_security_code_pk	= secu.id_security_code_pk  			");
		sbQuery.append(" 	Inner join issuer iss ON secu.id_issuer_fk	= iss.id_issuer_pk 								");
		
		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
			sbQuery.append("			INNER JOIN NOMINAL_VALUE_HISTORY_VIEW NVH ON NVH.CUT_DATE = MFV.CUT_DATE 		");
			sbQuery.append("			AND secu.ID_SECURITY_CODE_PK = NVH.ID_SECURITY_CODE_FK							");
		}

		sbQuery.append(" WHERE secu.security_type in (1762,1763)																													");
		sbQuery.append(" 	AND MFV.CUT_DATE >= TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') ");
		sbQuery.append(" 	AND MFV.CUT_DATE < TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 ");
		sbQuery.append(" 	AND TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY') ");
		sbQuery.append(" 	AND (MFV.total_balance > 0 or MFV.reported_balance > 0)																									");
		
		if(Validations.validateIsNotNull(clientPortafolioTO.getCui())){
			sbQuery.append(" AND (EXISTS																																			");
			sbQuery.append(" 	(SELECT had.ID_HOLDER_FK																															");
			sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL had																														");
			sbQuery.append(" 	WHERE had.ID_HOLDER_FK = :cui_code																													");
			sbQuery.append(" 		AND had.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK))																						");
	    }
		sbQuery.append(" )T  Order by mnemonic, security_type, currency_mnemonic, security_class_mnemonic ,id_security_code_pk, Holders ");

		String strQueryFormatted = sbQuery.toString();
		
		if(Validations.validateIsNotNullAndPositive(clientPortafolioTO.getCui())){
    		strQueryFormatted=strQueryFormatted.replace(":cui_code", clientPortafolioTO.getCui().toString());
    	}
    	
		return strQueryFormatted;
    }
    
    /**
     * 
     * 
     * 
     * */
    public String getQueryClientPortafolio(ClientPortafolioTO clientPortafolioTO){
    	StringBuilder sbQuery= new StringBuilder();
    	
    	sbQuery.append(" SELECT	");
  		sbQuery.append(" 	TO_NUMBER(ID_PARTICIPANT_PK) ID_PARTICIPANT_PK, ");
		sbQuery.append(" 	DESCRIPTION, ");
		sbQuery.append(" 	MNEMONIC, ");
		sbQuery.append(" 	TO_NUMBER(CURRENCY) CURRENCY, ");
		sbQuery.append(" 	CURRENCY_MNEMONIC, ");
		sbQuery.append(" 	MNEMONIC_ISSUER, ");
		sbQuery.append(" 	BUSINESS_NAME, ");
		sbQuery.append(" 	TO_NUMBER(ID_HOLDER_ACCOUNT_PK) ID_HOLDER_ACCOUNT_PK, ");
		sbQuery.append(" 	HOLDERS, ");  
		sbQuery.append(" 	HOLDERSDESC, ");  
		sbQuery.append(" 	TO_NUMBER(ACCOUNT_NUMBER) ACCOUNT_NUMBER, ");
		sbQuery.append(" 	TO_NUMBER(SECURITY_CLASS) SECURITY_CLASS, ");
		sbQuery.append(" 	SECURITY_CLASS_MNEMONIC, ");
		sbQuery.append(" 	ID_SECURITY_CODE_PK, ");
		sbQuery.append(" 	FECHAEMISION, ");
		sbQuery.append(" 	TO_NUMBER(SECURITY_DAYS_TERM) SECURITY_DAYS_TERM, ");
		sbQuery.append(" 	TO_NUMBER(INITIAL_NOMINAL_VALUE) INITIAL_NOMINAL_VALUE, ");
		sbQuery.append(" 	TO_NUMBER(INTEREST_RATE) INTEREST_RATE, ");
		sbQuery.append(" 	TO_NUMBER(SECURITY_TYPE) SECURITY_TYPE, ");
		sbQuery.append(" 	TO_NUMBER(VALORFINAL) VALORFINAL, ");
		sbQuery.append(" 	TO_NUMBER(DIASVIGENTES) DIASVIGENTES, ");
		sbQuery.append(" 	TO_NUMBER(VALOR_PRESENTE) VALOR_PRESENTE, ");
		sbQuery.append(" 	TOTAL_BALANCE, ");
		sbQuery.append(" 	AVAILABLE_BALANCE, ");
		sbQuery.append(" 	PAWN_BALANCE, ");
		sbQuery.append(" 	BAN_BALANCE, ");
		sbQuery.append(" 	OTHER_BLOCK_BALANCE, ");
		sbQuery.append(" 	ACCREDITATION_BALANCE, ");
		sbQuery.append(" 	MARGIN_BALANCE, ");
		sbQuery.append(" 	REPORTING_BALANCE, ");
		sbQuery.append(" 	REPORTED_BALANCE, ");
		sbQuery.append(" 	PURCHASE_BALANCE, ");
		sbQuery.append(" 	SALE_BALANCE, ");
		sbQuery.append(" 	TRANSIT_BALANCE, ");
		sbQuery.append(" 	TASA_MERCADO, ");
		sbQuery.append(" 	PRECIO_MERCADO, ");
		sbQuery.append(" 	TOTAL_AMOUNT_MF, ");
		sbQuery.append(" 	THEORIC_RATE, ");
		sbQuery.append(" 	THEORIC_PRICE, ");
		sbQuery.append(" 	TOTAL_AMOUNT, ");
		sbQuery.append(" 	CURRENT_NOMINAL_VALUE, ");
		sbQuery.append(" 	CURRENT_TOTAL_AMOUNT ");
		sbQuery.append(" 	, NULL as A ");
		sbQuery.append("	,EXPIRATION_DATE   ");
		sbQuery.append("	, NULL as B ");
		sbQuery.append("	, NULL as C  ");
		sbQuery.append("	, NULL as D  ");
		sbQuery.append(" 	,economicActivity, ");
		sbQuery.append(" 	initialNValueUSD, ");
		sbQuery.append(" 	currentNValueUSD, ");
		sbQuery.append(" 	marketPriceUSD ");

		sbQuery.append(" FROM ");
		sbQuery.append(" 	(select ");
		sbQuery.append(" 	p.id_participant_pk||HA.id_holder_account_pk||secu.id_security_code_pk secuencia, ");
		sbQuery.append(" 	p.id_participant_pk, ");
		sbQuery.append(" 	p.description, ");
		sbQuery.append(" 	p.mnemonic, ");																										 
		sbQuery.append(" 	secu.currency, ");																																				 
		sbQuery.append(" 	(select pt.text1 from parameter_table pt where pt.parameter_table_pk=secu.currency) currency_mnemonic, ");														 
		sbQuery.append(" 	iss.mnemonic as MNEMONIC_ISSUER, ");																															 
		sbQuery.append(" 	iss.business_name, ");																																			 
		sbQuery.append(" 	HA.id_holder_account_pk, ");																																	 
		sbQuery.append(" 	(SELECT LISTAGG(HAD.ID_HOLDER_FK , ',' ||chr(13)) WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");																			 
		sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD ");																																 
		sbQuery.append(" 	WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ");																									 
		sbQuery.append(" 	) Holders, ");
		sbQuery.append("    (SELECT LISTAGG((select h.full_name from holder h where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK) ||chr(13)) WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)");
		sbQuery.append("    FROM HOLDER_ACCOUNT_DETAIL HAD	");
		sbQuery.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK " );
		sbQuery.append(" 	) HoldersDesc, ");
		sbQuery.append(" 	HA.account_number, ");
		sbQuery.append(" 	secu.security_class, ");																																	 
		sbQuery.append(" 	(select pt.text1 from parameter_table pt where pt.parameter_table_pk=secu.security_class) security_class_mnemonic, ");											 
		sbQuery.append(" 	secu.id_security_code_pk, ");																																	 
		sbQuery.append(" 	(to_char(secu.issuance_date,'dd/MM/YYYY')) fechaEmision, ");																									 
		sbQuery.append(" 	secu.security_days_term, ");																																	 
		sbQuery.append(" 	secu.initial_nominal_value, ");
		sbQuery.append(" 	secu.interest_rate, ");
		sbQuery.append(" 	secu.security_type, ");
		sbQuery.append(" 	secu.initial_nominal_value+ ");
		sbQuery.append(" 	secu.initial_nominal_value*nvl(secu.interest_rate,0)/100 ValorFinal, ");											
		sbQuery.append(" 	GREATEST(0,nvl(trunc(secu.expiration_date) - trunc(sysdate),0)) DiasVigentes, ");
		sbQuery.append(" 	ROUND((secu.current_nominal_value/TO_NUMBER(POWER(1+(secu.INTEREST_RATE/100),(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')-secu.ISSUANCE_DATE)/360)) + secu.current_nominal_value),2) VALOR_PRESENTE, ");
		sbQuery.append(" 	PSV.TOTAL_BALANCE total_balance, "); 
		sbQuery.append(" 	PSV.AVAILABLE_BALANCE available_balance, ");
		sbQuery.append(" 	0 pawn_balance, "); 
		sbQuery.append(" 	0 ban_balance, ");
		sbQuery.append(" 	0 other_block_balance, ");
		sbQuery.append(" 	0 accreditation_balance, ");
		sbQuery.append(" 	0 margin_balance, ");
		sbQuery.append(" 	0 reporting_balance, ");
		sbQuery.append(" 	0 reported_balance, ");
		sbQuery.append(" 	0 purchase_balance, ");
		sbQuery.append(" 	0 sale_balance, ");
		sbQuery.append(" 	0 transit_balance, ");
		sbQuery.append(" 	DECODE(secu.INSTRUMENT_TYPE,124,FN_PHYSICAL_MARKET_PRICE_RATE(PSV.id_security_code_pk,TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY'),2),null) TASA_MERCADO, ");
		sbQuery.append(" 	NVL(FN_PHYSICAL_MARKET_PRICE_RATE(PSV.id_security_code_pk,TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY'),1),0) PRECIO_MERCADO, ");
		sbQuery.append(" 	NVL(ROUND(FN_PHYSICAL_MARKET_PRICE_RATE(PSV.id_security_code_pk,TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY'),1),2),0) * PSV.TOTAL_BALANCE as TOTAL_AMOUNT_MF, ");
		sbQuery.append(" 	NVL(FN_PHYSICAL_MARKET_PRICE_RATE(PSV.id_security_code_pk,TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY'),2),0) THEORIC_RATE, ");
		sbQuery.append(" 	NVL(FN_PHYSICAL_MARKET_PRICE_RATE(PSV.id_security_code_pk,TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY'),1),0) THEORIC_PRICE, ");
		sbQuery.append(" 	NVL(ROUND(FN_PHYSICAL_MARKET_PRICE_RATE(PSV.id_security_code_pk,TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY'),1),2),0) * PSV.TOTAL_BALANCE total_amount, ");
		sbQuery.append("    DECODE(secu.security_class,426,ROUND(");
		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
			sbQuery.append("			NVH.NOMINAL_VALUE															");
		}else{
			sbQuery.append("			secu.current_nominal_value ");
		}
		sbQuery.append("		  	,4),ROUND( ");
		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
			sbQuery.append("			NVH.NOMINAL_VALUE 															");
		}else{
			sbQuery.append("			secu.current_nominal_value ");
		}
		sbQuery.append(" 			,2)) ");
		sbQuery.append(" 	as current_nominal_value, ");
		sbQuery.append("    DECODE(secu.security_class,426,ROUND( ");
		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
			sbQuery.append("			NVH.NOMINAL_VALUE														");
		}else{
			sbQuery.append("			secu.current_nominal_value ");
		}
		sbQuery.append("		  	,4),ROUND( ");
		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
			sbQuery.append("			NVH.NOMINAL_VALUE 			");
		}else{
			sbQuery.append("			secu.current_nominal_value ");
		}
		sbQuery.append(" 			,2)) * PSV.TOTAL_BALANCE ");
		sbQuery.append(" 	as current_total_amount ");		
		sbQuery.append("			,   secu.EXPIRATION_DATE    ");		
		sbQuery.append(" 	,NVL(CASE WHEN HA.ACCOUNT_TYPE in ( 106, 108 ) THEN 'PARTICULARES'                                                                          ");
		sbQuery.append("         WHEN HA.ACCOUNT_TYPE = 107 THEN ( select PT_SUB.DESCRIPTION                                                                            ");
		sbQuery.append("                                            from holder H_SUB                                                                                   ");
		sbQuery.append("                                            inner join HOLDER_ACCOUNT_DETAIL HAD_SUB on HAD_SUB.ID_HOLDER_FK = H_SUB.ID_HOLDER_PK               ");
		sbQuery.append("                                            inner join HOLDER_ACCOUNT HA_SUB ON HAD_SUB.ID_HOLDER_ACCOUNT_FK = HA_SUB.ID_HOLDER_ACCOUNT_PK      ");
		sbQuery.append("                                            inner join PARAMETER_TABLE PT_SUB on PT_SUB.PARAMETER_TABLE_PK = H_SUB.ECONOMIC_ACTIVITY            ");
		sbQuery.append("                                            where 1 = 1                                                                                         ");
		sbQuery.append("                                            and HA_SUB.ID_HOLDER_ACCOUNT_PK = HA.ID_HOLDER_ACCOUNT_PK)                                          ");
		sbQuery.append("        END,'POR COMPLETAR') economicActivity,                                                                                                  ");		
		sbQuery.append("	round(((PSV.total_balance * secu.initial_nominal_value) *    ");  
		sbQuery.append("	CASE WHEN secu.CURRENCY = 127 THEN 1 /                                                                                          ");  
		sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
		sbQuery.append("						 AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)                                              ");  
		sbQuery.append("		 WHEN secu.CURRENCY IN (1734,1304) THEN 1 *                                                                                 ");  
		sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
		sbQuery.append("						AND D.ID_CURRENCY = secu.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /                                   ");  
		sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
		sbQuery.append("						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)                                               ");  
		sbQuery.append("		 WHEN secu.CURRENCY IN (430,1853) THEN 1 ELSE 1 END ),2) initialNValueUSD,                                                  ");  		
		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
			sbQuery.append("	round(((PSV.total_balance * ");  
			sbQuery.append("			(DECODE(SECU.SECURITY_CLASS,426,ROUND( NVH.NOMINAL_VALUE ,4),        ROUND( NVH.NOMINAL_VALUE ,2))) 			");
			sbQuery.append("	) *       ");   		
		}else{
			sbQuery.append("	round(((PSV.total_balance *  DECODE(SECU.SECURITY_CLASS,426,ROUND( secu.CURRENT_NOMINAL_VALUE ,4),        ROUND( secu.CURRENT_NOMINAL_VALUE ,2))) *                                                                   ");  
		}
		sbQuery.append("	CASE WHEN secu.CURRENCY = 127 THEN 1 /                                                                                          ");  
		sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
		sbQuery.append("						 AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)                                              ");  
		sbQuery.append("		 WHEN secu.CURRENCY IN (1734,1304) THEN 1 *                                                                                 ");  
		sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
		sbQuery.append("						AND D.ID_CURRENCY = secu.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /                                   ");  
		sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
		sbQuery.append("						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)                                               ");  
		sbQuery.append("		 WHEN secu.CURRENCY IN (430,1853) THEN 1 ELSE 1 END ),2) currentNValueUSD,                                                  "); 		
		sbQuery.append("	round(((NVL(PSV.total_balance * ROUND(NVL(  NVL(FN_PHYSICAL_MARKET_PRICE_RATE(PSV.id_security_code_pk,TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY'),1),0)  ,0),2),0)) *                                                                                              ");  
		sbQuery.append("	CASE WHEN secu.CURRENCY = 127 THEN 1 /                                                                                          ");  
		sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
		sbQuery.append("						 AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)                                              ");  
		sbQuery.append("		 WHEN secu.CURRENCY IN (1734,1304) THEN 1 *                                                                                 ");  
		sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
		sbQuery.append("						AND D.ID_CURRENCY = secu.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /                                   ");  
		sbQuery.append("		 nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY')         ");  
		sbQuery.append("						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)                                               ");  
		sbQuery.append("		 WHEN secu.CURRENCY IN (430,1853) THEN 1 ELSE 1 END ),2) marketPriceUSD                                                    ");  
		
		sbQuery.append(" FROM BALANCE_PSY_VIEW PSV	 ");		
		sbQuery.append(" 	INNER JOIN PHYSICAL_CERT_MARKETFACT HMB ON HMB.ID_PHYSICAL_CERTIFICATE_FK = PSV.ID_PHYSICAL_CERTIFICATE_PK		");
		sbQuery.append(" 	inner join holder_Account ha on PSV.id_holder_Account_pk=HA.id_holder_Account_pk			");
		sbQuery.append(" 	Inner join participant p On PSV.id_participant_pk=p.id_participant_pk			");
		sbQuery.append(" 	Inner join security secu On PSV.id_security_code_pk = secu.id_security_code_pk			");
		sbQuery.append(" 	Inner join issuer iss  On secu.id_issuer_fk=iss.id_issuer_pk		");		
		if(CommonsUtilities.convertStringtoDate(clientPortafolioTO.getDateMarketFactString()).before(CommonsUtilities.currentDate())){
			sbQuery.append("	LEFT JOIN NOMINAL_VALUE_HISTORY_VIEW NVH ON NVH.CUT_DATE = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"','DD/MM/YYYY') 		");
			sbQuery.append("		AND secu.ID_SECURITY_CODE_PK = NVH.ID_SECURITY_CODE_FK			");
		}
		
		sbQuery.append(" WHERE 1=1  																										");
		sbQuery.append(" 	AND HMB.IND_ACTIVE = 1																							");		
		sbQuery.append(" 	AND PSV.CUT_DATE >= TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD')   	");
		sbQuery.append(" 	AND PSV.CUT_DATE < TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') + 1 	");
		sbQuery.append(" 	AND TRUNC(TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY'), 'DD') = TO_DATE('"+clientPortafolioTO.getDateMarketFactString()+"', 'DD/MM/YYYY') ");
		sbQuery.append(" 	and PSV.TOTAL_BALANCE > 0																						");
		if(Validations.validateIsNotNull(clientPortafolioTO.getCui())){
			sbQuery.append(" AND (EXISTS																									");
			sbQuery.append(" 	(SELECT had.ID_HOLDER_FK																					");
			sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL had																				");
			sbQuery.append(" 	WHERE had.ID_HOLDER_FK = :cui_code																			");
			sbQuery.append(" 		AND had.ID_HOLDER_ACCOUNT_FK = PSV.id_holder_account_pk))												");
	    }
		sbQuery.append(" ) T Order by mnemonic, security_type, currency_mnemonic, security_class_mnemonic ,id_security_code_pk, Holders  ");

		String strQueryFormatted = sbQuery.toString();
		
		if(Validations.validateIsNotNullAndPositive(clientPortafolioTO.getCui())){
    		strQueryFormatted=strQueryFormatted.replace(":cui_code", clientPortafolioTO.getCui().toString());
    	}
    	
		return strQueryFormatted;
    }
    /**
     * 
     * QUERY REPORTE DE BUSQUEDAS
     * 
     * */
    public String getQueryPersonSearchLog (Date date_initial, Date date_end, String doc_number) {
    	
    	StringBuilder sbQuery= new StringBuilder();
    	
    	sbQuery.append("  select a.APPLICANT_DESC ");
		sbQuery.append("        ,a.ID_APPLICANT_PK ");
		sbQuery.append("        ,a.REQUEST_DATE ");
		sbQuery.append("        ,a.APPLICANT_NUM ");
		sbQuery.append("        ,TO_CHAR(a.PROCESS_DATE,'DD/MM/YYYY HH24:MM') as PROCESS_DATE ");
		sbQuery.append("        ,a.USER_REG ");
		sbQuery.append("        ,(case ac.DOC_SEARCH when 'NOMBRE' then ac.DOC_SEARCH else pt.INDICATOR1 end) as DOC_SEARCH ");
		sbQuery.append("        ,acr.DOC_NUM ");
		sbQuery.append("        ,acr.ISSUED ");
		sbQuery.append("        ,ac.NAME ||' '|| ac.LAST_NAME_1 ||' '|| ac.LAST_NAME_2 as DESCRIPTION ");
		sbQuery.append("        ,acr.IND_CUI_EXIST ");
		sbQuery.append("		,acr.ID_HOLDER_FK ");
		sbQuery.append("        ,acr.IND_SEC_VALID ");
		sbQuery.append("  from APPLICANT a ");
		sbQuery.append("    join APPLICANT_CONSULT ac on ac.ID_APPLICANT_FK = a.ID_APPLICANT_PK  ");
		sbQuery.append("    join APPLICANT_CONSULT_REQUEST acr on acr.ID_APPLICANT_CONS_FK = ac.ID_APPLICANT_CONS_PK  ");
		sbQuery.append("    left join PARAMETER_TABLE pt on ac.DOC_TYPE = pt.PARAMETER_TABLE_PK  ");
		sbQuery.append("  where 1=1 ");
		sbQuery.append("    and a.PROCESS_QUANTITY is not null ");
					
		if(Validations.validateIsNotNullAndNotEmpty(doc_number)) {
			sbQuery.append("    and ac.DOC_NUM = :docNum ");
		}	
		
		sbQuery.append("    and trunc(a.REQUEST_DATE) BETWEEN to_date(:date_initial, 'DD/MM/YYYY') and to_date(:date_end, 'DD/MM/YYYY') ");	
		sbQuery.append("  order by a.REQUEST_DATE,a.ID_APPLICANT_PK ");
		
		String strQueryFormatted = sbQuery.toString();
		
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		if(Validations.validateIsNotNullAndNotEmpty(date_initial)) {
			String date_initial_formated = formato.format(date_initial);
			strQueryFormatted = strQueryFormatted.replace(":date_initial", "'"+date_initial_formated+"'");
		}
		if(Validations.validateIsNotNullAndNotEmpty(date_end)) {
			String date_end_formated = formato.format(date_end);
			strQueryFormatted = strQueryFormatted.replace(":date_end", "'"+date_end_formated+"'");
		}
		if(Validations.validateIsNotNullAndNotEmpty(doc_number)) {
			strQueryFormatted = strQueryFormatted.replace(":docNum","'"+doc_number+"'");
		}
    	return strQueryFormatted;
    	
    }
}
