package com.pradera.report.generation.executor.custody.to;

import java.io.Serializable;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * The Class ChangeOwnershipReportTO.
 */
public class ChangeOwnershipReportTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6955794213491411507L;

	/** The operation number. */
	private Long idChangeOwnershipPk;
	
	/** The id target holder pk. */
	private Long idTargetHolderPk;
	
	/** The id source holder pk. */
	private Long idSourceHolderPk;
	
	/** The id source participant pk. */
	private Long idSourceParticipantPk;
	
	/** The id target participant pk. */
	private Long idTargetParticipantPk;
	
	/** The motive. */
	private Integer motive;
	
	/** The state. */
	private Integer state;
	
	/** The start date. */
	private Date startDate;
	
	/** The end date. */
	private Date endDate;
	
	/** The block type. */
	private Integer blockType;
		
	/**
	 * Instantiates a new change ownership report to.
	 */
	public ChangeOwnershipReportTO() {
 
	}



	public Long getIdChangeOwnershipPk() {
		return idChangeOwnershipPk;
	}



	public void setIdChangeOwnershipPk(Long idChangeOwnershipPk) {
		this.idChangeOwnershipPk = idChangeOwnershipPk;
	}



	/**
	 * Gets the id target holder pk.
	 *
	 * @return the id target holder pk
	 */
	public Long getIdTargetHolderPk() {
		return idTargetHolderPk;
	}

	/**
	 * Sets the id target holder pk.
	 *
	 * @param idTargetHolderPk the new id target holder pk
	 */
	public void setIdTargetHolderPk(Long idTargetHolderPk) {
		this.idTargetHolderPk = idTargetHolderPk;
	}

	/**
	 * Gets the id source holder pk.
	 *
	 * @return the id source holder pk
	 */
	public Long getIdSourceHolderPk() {
		return idSourceHolderPk;
	}

	/**
	 * Sets the id source holder pk.
	 *
	 * @param idSourceHolderPk the new id source holder pk
	 */
	public void setIdSourceHolderPk(Long idSourceHolderPk) {
		this.idSourceHolderPk = idSourceHolderPk;
	}

	/**
	 * Gets the id source participant pk.
	 *
	 * @return the id source participant pk
	 */
	public Long getIdSourceParticipantPk() {
		return idSourceParticipantPk;
	}

	/**
	 * Sets the id source participant pk.
	 *
	 * @param idSourceParticipantPk the new id source participant pk
	 */
	public void setIdSourceParticipantPk(Long idSourceParticipantPk) {
		this.idSourceParticipantPk = idSourceParticipantPk;
	}

	/**
	 * Gets the id target participant pk.
	 *
	 * @return the id target participant pk
	 */
	public Long getIdTargetParticipantPk() {
		return idTargetParticipantPk;
	}

	/**
	 * Sets the id target participant pk.
	 *
	 * @param idTargetParticipantPk the new id target participant pk
	 */
	public void setIdTargetParticipantPk(Long idTargetParticipantPk) {
		this.idTargetParticipantPk = idTargetParticipantPk;
	}

	/**
	 * Gets the motive.
	 *
	 * @return the motive
	 */
	public Integer getMotive() {
		return motive;
	}

	/**
	 * Sets the motive.
	 *
	 * @param motive the new motive
	 */
	public void setMotive(Integer motive) {
		this.motive = motive;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the start date.
	 *
	 * @return the start date
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Sets the start date.
	 *
	 * @param startDate the new start date
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * Gets the block type.
	 *
	 * @return the block type
	 */
	public Integer getBlockType() {
		return blockType;
	}

	/**
	 * Sets the block type.
	 *
	 * @param blockType the new block type
	 */
	public void setBlockType(Integer blockType) {
		this.blockType = blockType;
	}

	
}
