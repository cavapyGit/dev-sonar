package com.pradera.report.generation.executor.issuances;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;

import com.pradera.integration.common.validation.Validations;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.securities.service.ANNAsTransferServiceBean;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;

@ReportProcess(name = "ANNAsTransferReport")
public class ANNAsTransferReport extends GenericReport{
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ANNAsTransferServiceBean annasTransferService;

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		GenericsFiltersTO annasTransferTO = new GenericsFiltersTO();

		for (ReportLoggerDetail r : listaLogger) {
			if(r.getFilterName().equals(ReportConstant.FILE_CODE)){
				if (r.getFilterValue()!=null && !r.getFilterValue().equals("")){
					annasTransferTO.setFileCode(r.getFilterValue());
				}
			}
			if(r.getFilterName().equals(ReportConstant.DATE_INITIAL_PARAM)){
				if (Validations.validateIsNotNullAndNotEmpty(r.getFilterValue())){
					annasTransferTO.setInitialDt(r.getFilterValue());
				}
			}
			if(r.getFilterName().equals(ReportConstant.DATE_END_PARAM)){
				if (Validations.validateIsNotNullAndNotEmpty(r.getFilterValue())){
					annasTransferTO.setFinalDt(r.getFilterValue());
				}
			}
		}

		String strQuery = annasTransferService.getQueryAnnasTransfer(annasTransferTO);

		Map<String,Object> parametersRequired = new HashMap<>();
		parametersRequired.put("str_query", strQuery);
		
		return parametersRequired;
	}
}
