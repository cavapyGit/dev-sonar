package com.pradera.report.generation.executor.settlement;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.settlement.service.SettlementReportServiceBean;
import com.pradera.report.generation.executor.settlement.to.RegistrationAssignmentHoldersTO;
import com.pradera.report.generation.poi.ReportPoiGeneratorServiceBean;
import com.pradera.report.generation.service.ReportUtilServiceBean;

@ReportProcess(name = "RegistrationAssignmentHoldersPoiReport")
public class RegistrationAssignmentHoldersPoiReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private SettlementReportServiceBean settlementReportServiceBean;
	@EJB
	private ReportUtilServiceBean reportUtilServiceBean;

	/** The Report poi generator service bean. */
	@Inject
	private ReportPoiGeneratorServiceBean reportPoiGeneratorServiceBean;
	@Inject
	private PraderaLogger log;

	private RegistrationAssignmentHoldersTO registrationAssignmentHoldersTO;
	
	public RegistrationAssignmentHoldersPoiReport() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {


		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] arrayByteExcel = new byte[16384];
		List<Object[]> queryMain=new ArrayList<Object[]>();
		
		String title=null;
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		Map<String,Object> parametersRequired = new HashMap<>();
		registrationAssignmentHoldersTO = new RegistrationAssignmentHoldersTO();

		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("date")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
//					registrationAssignmentHoldersTO.setDateInitial(CommonsUtilities.convertStringtoDate(listaLogger.get(i).getFilterValue()));
					registrationAssignmentHoldersTO.setStrDateInitial(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("holder_account")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					registrationAssignmentHoldersTO.setIdHolderAccountPk(Long.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("cui_code")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					registrationAssignmentHoldersTO.setIdHolderPk(Long.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("value_series")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					registrationAssignmentHoldersTO.setSecurityCode(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("modality")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					registrationAssignmentHoldersTO.setModality(Long.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("mechanism")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					registrationAssignmentHoldersTO.setMechanism(Long.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("schema")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					registrationAssignmentHoldersTO.setSchema(Integer.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("participant")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					registrationAssignmentHoldersTO.setParticipantPk(Long.valueOf(listaLogger.get(i).getFilterValue()));
					registrationAssignmentHoldersTO.setParticipantDesc(listaLogger.get(i).getFilterDescription());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("security_class")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					registrationAssignmentHoldersTO.setSecurityClass(listaLogger.get(i).getFilterValue());
				}
			}
		}
		
			String strQuery = null;
				title="REPORTE DE VENCIMIENTO DE VALORES POR EMISOR";
			strQuery = settlementReportServiceBean.getQueryRegistrationAssignment(registrationAssignmentHoldersTO);
			queryMain=settlementReportServiceBean.getQueryListByClass(strQuery);
			
			parametersRequired.put("str_query", queryMain);
			
			/**GENERATE FILE EXCEL ***/
			arrayByteExcel=reportPoiGeneratorServiceBean.writeFileExcelRegistrationAssignment(parametersRequired,reportLogger);
			
			/**SETTING ARRAY BYTE TO GENERIC REPORT FOR PERSIST**/
			List<byte[]> bytes = new ArrayList<>();
			bytes.add(arrayByteExcel);
			setListByte(bytes);

			return baos;
	}
}