package com.pradera.report.generation.executor.custody.to;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "CON_SIRTEX")
@XmlAccessorType(XmlAccessType.FIELD) 
public class XmlReportSirtexByParticipant implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 255653136781479711L;
	public XmlReportSirtexByParticipant() {
		// TODO Auto-generated constructor stub
	}
	@XmlElement(name = "OPERACIONES")
	private XmlSecOperations secOperations;
	public XmlSecOperations getSecOperations() {
		return secOperations;
	}
	public void setSecOperations(XmlSecOperations secOperations) {
		this.secOperations = secOperations;
	}
}
