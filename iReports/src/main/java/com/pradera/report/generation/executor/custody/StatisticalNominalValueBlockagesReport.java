package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.util.view.UtilReportConstants;


@ReportProcess(name="StatisticalNominalValueBlockagesReport")
public class StatisticalNominalValueBlockagesReport extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@Inject  PraderaLogger log; 
	
	public StatisticalNominalValueBlockagesReport() {

	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void addParametersQueryReport() {
		
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		
		Map<Integer, String> blockType = new HashMap<Integer, String>();
		Map<Integer, String> block = new HashMap<Integer, String>();
		Map<Integer, String> classValue = new HashMap<Integer, String>();

		try {
			filter.setMasterTableFk(MasterTableType.LEVEL_AFFECTATION.getCode());
			for (ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				blockType.put(param.getParameterTablePk(),param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for (ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				classValue.put(param.getParameterTablePk(),param.getText1());
			}
			filter.setMasterTableFk(MasterTableType.TYPE_AFFECTATION.getCode());
			for (ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				block.put(param.getParameterTablePk(),param.getDescription());
			}
			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		parametersRequired.put("p_blockType", blockType);
		parametersRequired.put("p_classValue", classValue);
		parametersRequired.put("p_block", block);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());


		// TODO Auto-generated method stub
		return parametersRequired;
	}

}
