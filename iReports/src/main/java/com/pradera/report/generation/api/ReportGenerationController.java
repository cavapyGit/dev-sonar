package com.pradera.report.generation.api;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.report.remote.ReportRegisterTO;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.report.generation.facade.ReportGenerationLocalServiceFacade;

@Path("/generate")
@Stateless
public class ReportGenerationController {

	@EJB
	ReportGenerationLocalServiceFacade reportGenerationLocalServiceFacade;

	@POST
	@Path("/modify")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes(MediaType.APPLICATION_JSON)
	public Response searchNotifications(Object dataObject) throws ServiceException {
		Gson gson = new Gson();
		try {
			ReportRegisterTO req = gson.fromJson(gson.toJson(dataObject), ReportRegisterTO.class);

			reportGenerationLocalServiceFacade.sendModifyingOfData(req.getReportId(), req.getRelatedId(),
					req.getLoggerUser().getUserName(), req.getLoggerUser(), req.getRelatedParam());

			return Response.status(Status.OK.getStatusCode()).entity("Success").build();
		} catch (JsonSyntaxException e) {
			return Response.status(Status.BAD_REQUEST.getStatusCode()).entity("Invalid request body format").build();
		}
	}

	@POST
	@Path("/billing/preliminary")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes(MediaType.APPLICATION_JSON)
	public Response generatePreliminaryBilling(Object dataObject) throws ServiceException {
		Gson gson = new Gson();
		try {
			ReportRegisterTO req = gson.fromJson(gson.toJson(dataObject), ReportRegisterTO.class);

			Map<String, String> parameters = new HashMap<String, String>();
			parameters.put("process_id", req.getRelatedId().toString());
			/** ParameterDetails */
			Map<String, String> parameterDetails = new HashMap<String, String>();

			ReportUser reportUser = new ReportUser();

			reportUser.setUserName(req.getLoggerUser().getUserName());
			reportUser.setReportFormat(ReportFormatType.PDF.getCode());
			// how report is send for screen so for default send notification
			reportUser.setShowNotification(BooleanType.YES.getCode());

			ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), req.getLoggerUser());

			reportGenerationLocalServiceFacade.saveReportExecution(req.getReportId(), parameters, parameterDetails,
					reportUser);

			return Response.status(Status.OK.getStatusCode()).entity("Success").build();
		} catch (JsonSyntaxException e) {
			return Response.status(Status.BAD_REQUEST.getStatusCode()).entity("Invalid request body format").build();
		}
	}

	@POST
	@Path("/negotiation/unfullfillment")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes(MediaType.APPLICATION_JSON)
	public Response sendUnfullfillmentReports(Object dataObject) throws ServiceException {
		Gson gson = new Gson();
		try {
			ReportRegisterTO req = gson.fromJson(gson.toJson(dataObject), ReportRegisterTO.class);

			Map<String, String> parameters = new HashMap<String, String>();
			parameters.put("settlement_date", req.getStringInitDate());
			parameters.put("mechanism_type", req.getRelatedId().toString());
			parameters.put("institution", req.getInstitutionName());
			/** ParameterDetails */
			Map<String, String> parameterDetails = new HashMap<String, String>();

			ReportUser reportUser = new ReportUser();

			reportUser.setReportFormat(ReportFormatType.PDF.getCode());
			// how report is send for screen so for default send notification
			reportUser.setShowNotification(BooleanType.YES.getCode());
			if (Validations.validateIsNotNull(req.getParticipantCode())) {
				reportUser.setPartcipantCode(req.getParticipantCode());
				parameters.put("unfulfillment_part_id", req.getParticipantCode().toString());
			} else {
				reportUser.setUserName(req.getLoggerUser().getUserName());
			}

			if (Validations.validateIsNotNull(req.getInstitutionType())) {
				reportUser.setInstitutionType(req.getInstitutionType());
			}

			ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), req.getLoggerUser());

			reportGenerationLocalServiceFacade.saveReportExecution(req.getReportId(), parameters, parameterDetails,
					reportUser);

			return Response.status(Status.OK.getStatusCode()).entity("Success").build();
		} catch (JsonSyntaxException e) {
			return Response.status(Status.BAD_REQUEST.getStatusCode()).entity("Invalid request body format").build();
		}
	}
	
	@POST
	@Path("/negotiation/affected")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes(MediaType.APPLICATION_JSON)
	public Response sendAffectedReports(Object dataObject) throws ServiceException {
		Gson gson = new Gson();
		try {
			ReportRegisterTO req = gson.fromJson(gson.toJson(dataObject), ReportRegisterTO.class);

			Map<String, String> parameters = new HashMap<String, String>();
			parameters.put("settlement_date", req.getStringInitDate());
			parameters.put("mechanism_type", req.getRelatedId().toString());
			parameters.put("institution", req.getInstitutionName());
			/** ParameterDetails */
			Map<String, String> parameterDetails = new HashMap<String, String>();

			ReportUser reportUser = new ReportUser();

			reportUser.setReportFormat(ReportFormatType.PDF.getCode());
			// how report is send for screen so for default send notification
			reportUser.setShowNotification(BooleanType.YES.getCode());
			if (Validations.validateIsNotNull(req.getParticipantCode())) {
				reportUser.setPartcipantCode(req.getParticipantCode());
				parameters.put("affected_part_id", req.getParticipantCode().toString());
			} else {
				reportUser.setUserName(req.getLoggerUser().getUserName());
			}

			if (Validations.validateIsNotNull(req.getInstitutionType())) {
				reportUser.setInstitutionType(req.getInstitutionType());
			}

			ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), req.getLoggerUser());

			reportGenerationLocalServiceFacade.saveReportExecution(req.getReportId(), parameters, parameterDetails,
					reportUser);

			return Response.status(Status.OK.getStatusCode()).entity("Success").build();
		} catch (JsonSyntaxException e) {
			return Response.status(Status.BAD_REQUEST.getStatusCode()).entity("Invalid request body format").build();
		}
	}
}
