package com.pradera.report.generation.executor.account;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.util.view.UtilReportConstants;


@ReportProcess(name="QueryParticipantReport")
public class QueryParticipantReport extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@Inject  PraderaLogger log; 
	
	public QueryParticipantReport() {

	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void addParametersQueryReport() {
		
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String,Object> parametersRequired = new HashMap<>();
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		
		for (ReportLoggerDetail  loggerDetail: loggerDetails) {
			
			if (loggerDetail.getFilterName().equals(ReportConstant.ECONOMIC_ACTIVITY_PARAM)){
				if(loggerDetail.getFilterValue() != null){
//					parametersRequired.put(ReportConstant.ACTIVITY_ECONOMIC_PARAM,loggerDetail.getFilterValue().toString());
					parametersRequired.put("economic_activity_desc",loggerDetail.getFilterDescription());
				}else{
					parametersRequired.put("economic_activity_desc","TODAS");
				}
			}
		}
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		return parametersRequired;
	}

}
