package com.pradera.report.generation.executor;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ReportConstant.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 11/07/2013
 */
public class ReportConstant {

	/**
	 * Instantiates a new report constant.
	 */
	public ReportConstant() {
		// TODO Auto-generated constructor stub
	}
	
	/** The Constant ENCODING_XML. */
	public static final String ENCODING_XML="UTF-8";
	
	/** The operation number param. */
	public static String OPERATION_NUMBER_PARAM = "operation_number";

	
	/** The Constant REPORT. */
	public static final String REPORT="report";
	
	/** The Constant BCB_FORMAT. */
	public static final String BCB_FORMAT="bcb_format";
	
	/** The Constant REPORT_ID. */
	public static final String REPORT_ID="report_id";
	
	/** The report title. */
	public static final String REPORT_TITLE="report_title";
	
	/** The user name. */
	public static final String USER_NAME="user_name";
	
	/** The mnemonic entity. */
	public static final String MNEMONIC_ENTITY="mnemonic_entity";
	
	/** The mnemonic report. */
	public static final String MNEMONIC_REPORT="mnemonic_report";
	
	/** The clasification report. */
	public static final String CLASIFICATION_REPORT="clasification_report";
	
	/** The generation date. */
	public static final String GENERATION_DATE="generation_date";
	
	/** The Constant START_HOUR. */
	public static final String START_HOUR = "start_hour";
	
	/** The Constant END_HOUR. */
	public static final String END_HOUR = "end_hour";
	
	/** The Constant ISSUER. */
	public static final String ISSUER= "issuer";
	
	/** The Constant ISSUANCE. */
	public static final String ISSUANCE= "issuance";
	
	/** The Constant SECURITY_KEY. */
	public static final String SECURITY_KEY= "securityCode";
	
	/** The Constant SECURITY_CODE. */
	public static final String SECURITY_CODE= "security_code";
	
	public static final String PORTOFOLO_CODE_PARAM = "portofolio_code";
	
	/** The Constant SECURITY_CODE_DESC. */
	public static final String SECURITY_CODE_DESC= "security_code_desc";
	
	/** The Constant SECURITY_KEY_AUX. */
	public static final String SECURITY_KEY_AUX= "securityCodeAux";
	
	/** The Constant KEY_VALUE. */
	public static final String KEY_VALUE= "key_value";
	
	/** The Constant VALUE_SERIES. */
	public static final String VALUE_SERIES= "value_series";
	
	/** The Constant CURRENCY. */
	public static final String CURRENCY= "currency";
	
	/** The Constant CUTTOFDATE. */
	public static final String CUTTOFDATE= "cutOffDate";
	
	/** The Constant REGISTRYDATE. */
	public static final String REGISTRYDATE= "registryDate";
	
	/** The Constant DELIVERYDATE. */
	public static final String DELIVERYDATE= "deliveryDate";
	
	/** The Constant NOMINALVALUE. */
	public static final String NOMINALVALUE= "nominalValue";
	
	/** The Constant DELIVERYFACTOR. */
	public static final String DELIVERYFACTOR= "deliveryFactor";
	
	/** The Constant CORPORATIVE_TYPE. */
	public static final String CORPORATIVE_TYPE= "corporativeType";
	
	/** The Constant TAX. */
	public static final String TAX= "tax";
	
	/** The Constant ROUND. */
	public static final String ROUND= "isRound";
	
	/** The Constant PROCESSSTATE. */
	public static final String PROCESSSTATE= "processState";
	
	/** The Constant COUPON. */
	public static final String COUPON= "coupon";
	
	/** The Constant FILE_CODE. */
	public static final String FILE_CODE = "file_code";
	
	/** The Constant PARAMETERS_TAG_REPORT. */
	public static final String PARAMETERS_TAG_REPORT="parameters";
	
	/** The report name. */
	public static final String REPORT_NAME="report_name";
	
	/** The Constant DATE_FORMAT_TIME_SECOND. */
	public static final String DATE_FORMAT_TIME_SECOND = "hh:mm:ss";
	
	/** The Constant DATE_FORMAT_TIME_SECOND. */
	public static final String DATE_FORMAT_TIME_SECOND_WITH_AM_OR_PM = "hh:mm:ss a";
	
	/** The Constant DATE_FORMAT_HEADER. */
	public static final String DATE_FORMAT_HEADER = "dd/MM/yyyy hh:mm a";
	
	/** The Constant DATE_FORMAT_STANDAR. */
	public static final String DATE_FORMAT_STANDAR = "dd/MM/yyyy";
	
	/** The Constant DATE_FORMAT_DOCUMENT. */
	public static final String DATE_FORMAT_DOCUMENT = "dd ' de ' MMMM ' de ' yyyy";
	
	/** The Constant QUANTITY_FORMAT_DECIMAL. */
	public static final String QUANTITY_FORMAT_DECIMAL = "##,###,###,###,##0.00";
	
	/** The Constant INTEGER_FORMAT_NUMBER. */
	public static final String INTEGER_FORMAT_NUMBER = "##,###,###,###,##0";
	
	/** The Constant PERCENT_FORMAT_DECIMAL. */
	public static final String PERCENT_FORMAT_DECIMAL = "##,###,###,###,##0.0000";
	
	/** The Constant PERCENT_FORMAT_EIGTH_DECIMAL. */
	public static final String PERCENT_FORMAT_EIGTH_DECIMAL = "##,###,###,###,##0.00000000";
	
	/** The Constant DATA_FOUND. */
	public static final String DATA_FOUND = "data_found";
	
	/** ****************** PARAMETERS CONSTANTS REPORTS  ***************************. */
	
	public static String PARTICIPANT_TYPE_PARAM = "participant_type";
	
	/** The participant class param. */
	public static String PARTICIPANT_CLASS_PARAM = "participant_class";
	
	/** The participant state param. */
	public static String PARTICIPANT_STATE_PARAM = "participant_state";
	
	/** The participant source. */
	public static String PARTICIPANT_SOURCE = "part_source";
	
	/** The participant target. */
	public static String PARTICIPANT_TARGET = "part_target";
	
	/** The participant origin. */
	public static String PARTICIPANT_ORIGIN = "participant_origin";
	
	/** The participant destination. */
	public static String PARTICIPANT_DESTINATION = "participant_destination";
	
	/** The rnt code param. */
	public static String RNT_CODE_PARAM = "rnt_code";
	
	/** The requester type param. */
	public static String REQUESTER_TYPE_PARAM = "requester_type";
	
	/** The requester param. */
	public static String REQUESTER_PARAM = "requester";
	
	/** The participant param. */
	public static String PARTICIPANT_PARAM = "participant";
	
	/** The participant pk param. */
	public static String PARTICIPANT_PK_PARAM = "participant_pk";
	
	/** The participant param. */
	public static String PARTICIPANT_DESC = "participant_desc";
	
	/** The state rnt param. */
	public static String STATE_RNT_PARAM = "state_rnt";
	
	/** The date initial param. */
	public static String DATE_INITIAL_PARAM = "date_initial";
	
	/** The initial date param. */
	public static String INITIAL_DATE_PARAM="initial_date";

	/** The end date param. */
	public static String END_DATE_PARAM="end_date";
	
	/** The date end param. */
	public static String DATE_END_PARAM = "date_end";
	
	/** The court date param*/
	public static String COURT_DATE_PARAM = "court_date";
	
	/** The date param. */
	public static String DATE_PARAM = "date";

	/** The to date. */
	public static String TO_DATE = "to_date";
	
	/** The to day. */
	public static String TO_DAY = "to_day";
	
	/** The to month. */
	public static String TO_MONTH = "to_month";
	
	/** The to year. */
	public static String TO_YEAR = "to_year";
	
	/** The balance indicator param. */
	public static String BALANCE_INDICATOR_PARAM = "balance_ind";
	
	/** The person type param. */
	public static String PERSON_TYPE_PARAM = "type_person";
	
	/** The state account param. */
	public static String STATE_ACCOUNT_PARAM = "state_account";
	
	/** The account type param. */
	public static String ACCOUNT_TYPE_PARAM = "type_account";
	
	/** The holder account. */
	public static String HOLDER_ACCOUNT = "holder_account";
	
	/** The account holder. */
	public static String ACCOUNT_HOLDER = "account_holder";
	
	/** The issuer param. */
	public static String ISSUER_PARAM = "issuer";
	
	/** The issuer param. */
	public static String ISSUER_DESC = "issuer_desc";
	
	/** The issuer code param. */
	public static String ISSUER_CODE_PARAM = "issuer_code";
	
	/** The class value. */
	public static String CLASS_VALUE = "class_value";
	
	/** The cui code param. */
	public static String CUI_CODE_PARAM = "cui_code";
	
	/** The cui holder param. */
	public static String CUI_HOLDER_PARAM = "cui_holder";
	
	/** The cui param. */
	public static String CUI_PARAM = "cui";
	
	/** The security param. */
	public static String SECURITY_PARAM = "security";
	
	/** The security class param. */
	public static String SECURITY_CLASS_PARAM = "security_class";
	
	/** The security class param. */
	public static String SECURITY_CLASS_DESC = "security_class_desc";
	
	/** The date type param. */
	public static String DATE_TYPE_PARAM = "date_type";
	
	/** The role param. */
	public static String ROLE_PARAM = "role";
	
	/** The expiration type param. */
	public static String EXPIRATION_TYPE_PARAM = "expiration_type";
	
	/** The class security param. */
	public static String CLASS_SECURITY_PARAM = "class_security";
	
	/** The type operation param. */
	public static String TYPE_OPERATION_PARAM = "type_operation";
	
	/** The transfer type param. */
	public static String TRANSFER_TYPE_PARAM = "transfer_type";
	
	/** The account number param. */
	public static String ACCOUNT_NUMBER_PARAM = "account_number";
	
	/** The accounts param. */
	public static String ACCOUNTS_PARAM = "accounts";

	/** The isin code param. */
	public static String ISIN_CODE_PARAM = "isin_code";
	
	/** The corporative isin code param. */
	public static String CORPORATIVE_ISIN_CODE_PARAM = "lbl_isin_code";

	/** The state operation type param. */
	public static String OPERATION_PARAM = "operation";
	
	/** The state operation param. */
	public static String STATE_OPERATION_PARAM = "state_operation";
	
	/** The state document param. */
	public static String STATE_DOCUMENT_PARAM = "state_document";
	
	/** The block entity param. */
	public static String BLOCK_ENTITY_PARAM = "block_entity";
	
	/** The affectation type param. */
	public static String AFFECTATION_TYPE_PARAM = "affectation_type";
	
	/** The block number param. */
	public static String BLOCK_NUMBER_PARAM = "block_number"; //numero de acto
	
	/** The filter dates param. */
	public static String FILTER_DATES_PARAM = "filter_dates";
	
	/** The request number param. */
	public static String REQUEST_NUMBER_PARAM = "request_number";
	
	/** The participant source param. */
	public static String PARTICIPANT_SOURCE_PARAM = "participant_source";
	
	/** The participant target param. */
	public static String PARTICIPANT_TARGET_PARAM = "participant_target";
	
	/** The holder source param. */
	public static String HOLDER_SOURCE_PARAM = "holder_source";
	
	/** The holder target param. */
	public static String HOLDER_TARGET_PARAM = "holder_target";
	
	/** The state param. */
	public static String STATE_PARAM = "state";
	
	/** The request state param. */
	public static String REQUEST_STATE_PARAM = "request_state";
	
	/** The block type param. */
	public static String BLOCK_TYPE_PARAM = "block_type";
	
	/** The value class param. */
	public static String VALUE_CLASS_PARAM = "value_class";
		
	/** The motive param. */
	public static String MOTIVE_PARAM = "motive";
	
	/** The motive param. */
	public static String MOTIVE_PARAM_DESC = "motive_desc";
	
	/** The balance type param. */
	public static String BALANCE_TYPE_PARAM = "balance_type";
	
	/** The accreditation operation id param. */
	public static String ACCREDITATION_OPERATION_ID_PARAM="accreditation_operation_id";
	
	/** The petitioner type param. */
	public static String PETITIONER_TYPE_PARAM="petitioner_type";
	
	/** The mnemonic param. */
	public static String MNEMONIC_PARAM="mnemonic";
	
	/** The economic sector param. */
	public static String ECONOMIC_SECTOR_PARAM="economic_sector";
	
	/** The activity economic param. */
	public static String ECONOMIC_ACTIVITY_PARAM="economic_activity";
	
	/** The benefit type param. */
	public static String BENEFIT_TYPE_PARAM="benefit_type";
	
	/** The issuance code param. */
	public static String ISSUANCE_CODE_PARAM="issuance_code";
	
	/** The participant negotiation param. */
	public static String PARTICIPANT_NEGOTIATION_PARAM = "participant_negot";
	
	/** The mechanism type param. */
	public static String MECHANISM_TYPE_PARAM="mechanism_type";
	
	/** The mechanism param. */
	public static String MECHANISM_PARAM="mechanism";
	
	/** The modality type param. */
	public static String MODALITY_TYPE_PARAM="modality_type";
	
	/** The modality param. */
	public static String MODALITY_PARAM="modality";
	
	/** The operation date param. */
	public static String OPERATION_DATE_PARAM="operation_date";

	/** The settlement date param. */
	public static String SETTLEMENT_DATE_PARAM="settlement_date";
	
	/** The currency type param. */
	public static String CURRENCY_TYPE_PARAM="currency_type";

	/** The valuation type. */
	public static String VALUATION_TYPE="valuation_type";
	
	/** The instrument type. */
	public static String INSTRUMENT_TYPE="instrument_type";
	
	/** The stock type. */
	public static String STOCK_TYPE="stock_type";
	
	/** The register date. */
	public static String REGISTER_DATE="register_date";
	
	/** The corporative register date. */
	public static String CORPORATIVE_REGISTER_DATE="lbl_register_date";
	
	/** The level sanctions. */
	public static String LEVEL_SANCTIONS="levelSanctions";
	
	/** The str level sanctions. */
	public static String STR_LEVEL_SANCTIONS="strLevelSanctions";
	
	/** The motive sanctions. */
	public static String MOTIVE_SANCTIONS="motiveSanctions";
	
	/** The amount sanctions. */
	public static String AMOUNT_SANCTIONS="amountSanctions";
	
	/** The part sanctions. */
	public static String PART_SANCTIONS="partSanctions";
	
	/** The str penalty. */
	public static String STR_PENALTY="strPenalty";
	
	/** The str penalty ope. */
	public static String STR_PENALTY_OPE="strPenaltyOpe";
	
	/** The str resolution number. */
	public static String STR_RESOLUTION_NUMBER="strResolutionNumber";
	
	/** The str date time. */
	public static String STR_DATE_TIME="strDateTime";
	
	/** The str iteration motive. */
	public static String STR_ITERATION_MOTIVE="iterationMotive";
	
	/** The str penalty level. */
	public static String STR_PENALTY_LEVEL="penaltyLevel";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_START="SOAP-ENV:Envelope";
	public static String TAG_SOAP_START_CONTENT=" xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ied='http://schemas.datacontract.org/2004/07/IEDVServicios' xmlns:tem='http://tempuri.org/'";
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_HEADER="SOAP-ENV:Header";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_BODY="SOAP-ENV:Body";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_ServFactura="tem:ServFactura";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_facturaC="tem:facturaC";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_IED_FACTURAC="ied:FacturaC";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_CLIENTE="ied:CLIENTE";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_CLIENTE_FACTURAR="ied:CLIENTE_FACTURAR";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_CUENTA="ied:CUENTA";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_DESCRIPCION="ied:DESCRIPCION";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_Detalle="ied:Detalle";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_FacturaD="ied:FacturaD";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_CANTIDAD="ied:CANTIDAD";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_ITEM="ied:ITEM";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_PRECIO="ied:PRECIO";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_FECOBRO="ied:FECOBRO";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_IMPUESTO="ied:IMPUESTO";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_MONEDA="ied:MONEDA";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_NUMCXC="ied:NUMCXC";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_NUMFACT="ied:NUMFACT";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_REF_AUX="ied:REF_AUX";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_TIPOSERVICIO="ied:TIPOSERVICIO";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_TOKKEN="ied:TOKKEN";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_VENTAS="VENTAS";
	
	/** The tag cxc. */
	public static String TAG_CXC="CXC";
	
	/** The tag numcxc. */
	public static String TAG_NUMCXC="NUMCXC";
	
	/** The tag account. */
	public static String TAG_ACCOUNT="CUENTA";
	
	/** The tag client. */
	public static String TAG_CLIENT="CLIENTE";
	
	/** The tag client. */
	public static String TAG_CLIENTE_FACTURAR="CLIENTE_FACTURAR";
	
	/** The tag description. */
	public static String TAG_DESCRIPTION="DESCRIPCION";
	
	/** The tag impuesto. */
	public static String TAG_IMPUESTO="IMPUESTO";
	
	/** The tag moneda. */
	public static String TAG_MONEDA="MONEDA";
	
	/** The tag cantidad. */
	public static String TAG_CANTIDAD="CANTIDAD";
	
	/** The tag precio. */
	public static String TAG_PRECIO="PRECIO";
	
	/** The tag fecobro. */
	public static String TAG_FECOBRO="FECOBRO";
	
	/** The tag tiposervicio. */
	public static String TAG_TIPOSERVICIO="TIPOSERVICIO";
	
	/** The tag ref aux. */
	public static String TAG_REF_AUX="REF_AUX";
	
	/** Billing Service Filters. */
	public static String FILTER_SEARCH_PARAM="filter_search";
	
	/** The billing service count param. */
	public static String BILLING_SERVICE_COUNT_PARAM="filter_billing";
	
	/** The billing service param. */
	public static String BILLING_SERVICE_PARAM="billing_service";
	
	/** The entity collection param. */
	public static String ENTITY_COLLECTION_PARAM="entity_collection";
	
	/** The collection period. */
	public static String COLLECTION_PERIOD="collection_period";
	
	/** The collection state param. */
	public static String COLLECTION_STATE_PARAM="collection_state";
	
	/** The holder param. */
	public static String HOLDER_PARAM="holder";

	/** The holder param. */
	public static String HOLDER_PARAM_DESC="holder_desc";
	
	/** The afp. */
	public static String AFP="afp";
	
	/** The other type document. */
	public static String OTHER_TYPE_DOCUMENT="other_type_document";
	
	/** The other entity document param. */
	public static String OTHER_ENTITY_DOCUMENT_PARAM="other_number";
	
	/** The other entity description param. */
	public static String OTHER_ENTITY_DESCRIPTION_PARAM="other_description";
	
	/** The process billing calculation pk. */
	public static String PROCESS_BILLING_CALCULATION_PK ="process_id";
	
	/** *** ACCOUNTING. */
	public static String DAILY_AUXILIARY_TYPE="daily_aux";
	
	/** The branch. */
	public static String BRANCH="branch";
	
	/** The report by. */
	public static String REPORT_BY="report_by";
	
	/** The process accounting pk. */
	public static String PROCESS_ACCOUNTING_PK ="process_id";
	
	
	/** Accountin Voucher Tag. */
	public static String TAG_ERP="ERP";
	
	/** The tag transacciones. */
	public static String TAG_TRANSACCIONES="TRANSACCIONES";
	
	/** The tag comprobante. */
	public static String TAG_COMPROBANTE="COMPROBANTE";
	
	/** The tag numcbte. */
	public static String TAG_NUMCBTE="NUMCBTE";
	
	/** The tag glosa. */
	public static String TAG_GLOSA="GLOSA";
	
	/** The tag tipocambiocab. */
	public static String TAG_TIPOCAMBIOCAB="TIPOCAMBIOCAB";
	
	/** The tag fecha. */
	public static String TAG_FECHA="FECHA";
	
	/** The tag tipo. */
	public static String TAG_TIPO="TIPO";
	
	/** The tag detalle. */
	public static String TAG_DETALLE="DETALLE";
	
	/** The tag registro. */
	public static String TAG_REGISTRO="REGISTRO";
	
	/** The tag numreg. */
	public static String TAG_NUMREG="NUMREG";
	
	/** The tag cuenta. */
	public static String TAG_CUENTA="CUENTA";
	
	/** The tag monto. */
	public static String TAG_MONTO="MONTO";
	
	/** The tag operacion. */
	public static String TAG_OPERACION="OPERACION";
	
	/** The tag glosaparticular. */
	public static String TAG_GLOSAPARTICULAR="GLOSAPARTICULAR";
	
	/** Accounting Poi Label. */

	public static String LABEL_FECHA="FECHA: ";
	
	/** The label hora. */
	public static String LABEL_HORA="HORA: ";
	
	/** The label clasificacion. */
	public static String LABEL_CLASIFICACION="CLASIFICACION: ";
	
	/** The label usuario. */
	public static String LABEL_USUARIO="USUARIO: ";
	
	/** The label fecha proceso. */
	public static String LABEL_FECHA_PROCESO="Fecha del proceso: ";
	
	/** The label period. */
	public static String LABEL_PERIOD="Periodo del ";
	
	/** The label cuenta. */
	public static String LABEL_CUENTA="CUENTA";
	
	/** The label descripcion. */
	public static String LABEL_DESCRIPCION="DESCRIPCION";

	/** The label descripcion titular. */
	public static String LABEL_DESCRIPCION_TITULAR="DESCRIPCION TITULAR";
	
	/** The label cantidad. */
	public static String LABEL_CANTIDAD="CANTIDAD";
	
	public static String LABEL_ID_COLUMN="ID";
	
	/** The label moneda origen. */
	public static String LABEL_MONEDA_ORIGEN="MONEDA ORIGEN";
	
	/** The label dolares. */
	public static String LABEL_DOLARES="DOLARES";
	
	/** The label bolivianos. */
	public static String LABEL_BOLIVIANOS="BOLIVIANOS";
	
	/** The label debe. */
	public static String LABEL_DEBE="DEBE";
	
	/** The label haber. */
	public static String LABEL_HABER="HABER";
	
	/** The label saldo. */
	public static String LABEL_SALDO="SALDO";
	
	/** The label total. */
	public static String LABEL_TOTAL="TOTAL";
	
	/** The label glosa. */
	public static String LABEL_GLOSA="GLOSA";
	
	/** The label fecha inicio. */
	public static String LABEL_FECHA_INICIO="FECHA INICIO";

	/** The label fecha fin. */
	public static String LABEL_FECHA_FIN="FECHA FIN";

	/** The label genero. */
	public static String LABEL_GENERO="GENERO";

	/** The label edad. */
	public static String LABEL_EDAD="EDAD";

	/** The label tipo titular. */
	public static String LABEL_TIPO_TITULAR="TIPO TITULAR";

	/** The label mantiene valores. */
	public static String LABEL_SECURITY_BALANCE="MANTIENE VALORES";

	/** The label cuentas titulares. */
	public static String LABEL_HOLDER_ACCOUNTS="CUENTAS TITULAR";

	/** The label estado titular. */
	public static String LABEL_FACT_DATE="DATOS A LA FECHA";
	
	
	public final static String LABEL_DEPARTAMENT="DEPARTAMENTO";
	
	public final static String LABEL_LOCALITY="LOCALIDAD";
	
	public final static String LABEL_PLACING_ENTITY="ENTIDAD COLOCADORA";
	
	public final static String LABEL_LOCK_ENTITY="ENTIDAD DE BLOQUEO";
	
	public final static String LABEL_LOCK_ENTITY_TYPE_DOC="TIPO DE DOCUMENTO DE ENTIDAD DE BLOQUEO";
	
	public final static String LABEL_LOCK_ENTITY_NUM_DOC="NUM DE DOCUMENTO DE ENTIDAD DE BLOQUEO";
	
	public final static String LABEL_IS_RESIDENT="RESIDENTE/NO RESIDENTE";
	
	public final static String LABEL_DOCUMENT_TYPE="TIPO DE DOCUMENTO";
	
	public final static String LABEL_LOCK_TYPE="TIPO DE BLOQUEO";
	
	public final static String LABEL_DOCUMENT_NUM="NUMERO DE DOCUMENTO";
	
	public final static String LABEL_TOTAL_NV="TOTAL NOMINAL";
	
	public final static String LABEL_CODE_ISSUER = "CODIGO";
	
	public final static String LABEL_MNEMONIC_ISSUER = "NEMONICO";
	
	public final static String LABEL_BUSSINESS_NAME = "RAZON SOCIAL";
	
	public final static String LABEL_PHONE_NUMBER = "TELEFONO";
	
	public final static String LABEL_ECONOMIC_ACTIVITY = "ACTIVIDAD ECONOMICA";
	
	public final static String LABEL_CLIENT_SELL = "CLIENTE VENDEDOR";
	
	public final static String LABEL_CLIENT_BUY = "CLIENTE COMPRADOR";
	
	/*Codigo alterno del reporte de cliente desmateriliza*/
	public final static String LABEL_ALTERNATE_CODE="CODIGO_ALTERNO";
	
	/*nombre del reporte de cliente desmateriliza*/
	public final static String LABEL_NAME="NOMBRE";
	
	/*Fecha de vencimiento del reporte de cliente desmateriliza*/
	public final static String LABEL_DUE_DATE="FECHA DE VENCIMIENTO";
	
	/*Tipo de persona del reporte de cliente desmateriliza*/
	public final static String LABEL_TYPE_OF_PERSON="TIPO PERSONA";
	
	public final static String LABEL_TYPE_TERM="TIPO PLAZO";
	
	/*CI NIT del reporte de cliente desmateriliza*/
	public final static String LABEL_CI_NIT="CI RUT";
	
	public final static String LABEL_NIT="NIT";
	
	/*SERIE del reporte de cliente desmateriliza*/
	public final static String LABEL_INITIAL_NVALUE_USD="MONTO A VN INICIAL EN USD";
	
	/*SERIE del reporte de cliente desmateriliza*/
	public final static String LABEL_CURRENT_NVALUE_USD="MONTO A VN ACTUAL EN USD";
	
	/*SERIE del reporte de cliente desmateriliza*/
	public final static String LABEL_MARKET_PRICE_USD="MONTO A PRECIO DE MERCADO EN USD";

	/*FECHA DE CORTE del reporte de cliente desmateriliza*/
	public final static String LABEL_CUT_DATE="FECHA DE CORTE";
	
	/*TIPO DE INVERSIONISTA del reporte de cliente desmateriliza*/
	public final static String LABEL_INVESTOR_TYPE="TIPO DE INVERSIONISTA";
	
	/*SERIE del reporte de cliente desmateriliza*/
	public final static String LABEL_SERIE="SERIE";
	
	public final static String LABEL_FIRST_HOLDER = "PRIMER TITULAR";
	
	public final static String WITH_COUPONS = "CON CUPONES";
	
	/*Primario o Secundario del reporte de cliente desmateriliza*/
	public final static String LABEL_PRIMARY_SECONDARY="PRIMARIO/SECUNDARIO";

	/** The label accounting squaring. */
	public static String LABEL_ACCOUNTING_SQUARING="CUADRE CONTABLE";
	
	/** The label portfolio balance. */
	public static String LABEL_PORTFOLIO_BALANCE="Saldos en Cartera";
	
	/** The label accounting balance. */
	public static String LABEL_ACCOUNTING_BALANCE="Saldos Contables";
	
	/** The label report operation squaring. */
	public static String LABEL_REPORT_OPERATION_SQUARING="REPORTE DE CUADRE DE OPERACIONES";
	
	/** The label system date. */
	public static String LABEL_SYSTEM_DATE="Fecha Sistema";
	
	/** The label tc usd. */
	public static String LABEL_TC_USD="TC USD ";
	
	/** The label tc ufv. */
	public static String LABEL_TC_UFV="TC UFV ";
	
	/** The label tc ecu.  */
	public static String LABEL_TC_ECU="TC ECU";
	
	/** The label date time close. */
	public static String LABEL_DATE_TIME_CLOSE="Fecha y Hora de Cierre";
	
	/** The label description. */
	public static String LABEL_DESCRIPTION="Descripci\u00f3n";
	
	/** The label currency bob. */
	public static String LABEL_CURRENCY_BOB="Bolivianos";
	
	/** The label currency usd. */
	public static String LABEL_CURRENCY_USD="D\u00F3lares";
	
	/** The label currency mv. */
	public static String LABEL_CURRENCY_MV="M/V";
	
	/** The label currency ufv. */
	public static String LABEL_CURRENCY_UFV="UFV's";
	
	/** The label currency euro. */
	public static String LABEL_CURRENCY_EURO="Euros ";
	
	/** The label total currency bob. */
	public static String LABEL_TOTAL_CURRENCY_BOB="Total en Bs";
	
	/** The label account. */
	public static String LABEL_ACCOUNT="Cuenta";
	
	/** The label diference. */
	public static String LABEL_DIFERENCE="Diferencias";
	
	/** The label custody balance security available. */
	public static String LABEL_CUSTODY_BALANCE_SECURITY_AVAILABLE="Custodia de Valores Anotados en Cuenta";
	
	/** The label balance security repoted. */
	public static String LABEL_BALANCE_SECURITY_REPOTED="Valores Desmaterializados Dados en Reporto";
	
	/** The label balance security lock. */
	public static String LABEL_BALANCE_SECURITY_LOCK="Valores Desmaterializados Restringidos";
	
	/** The label balance security other lock. */
	public static String LABEL_BALANCE_SECURITY_OTHER_LOCK="Valores Desmaterializados con Otras Restringiones	";
	
	/** The label balance security physical. */
	public static String LABEL_BALANCE_SECURITY_PHYSICAL="Custodia de Valores Fisicos";
	
	/** The label security pending. */
	public static String LABEL_SECURITY_PENDING="Valores Pendientes de Colocacion";

	/** The label account if available. */
	public static String LABEL_ACCOUNT_IF_AVAILABLE="605.01.01.m.01";
	
	/** The label account iv available. */
	public static String LABEL_ACCOUNT_IV_AVAILABLE="605.01.03.m.01";
	
	/** The label account im available. */
	public static String LABEL_ACCOUNT_IM_AVAILABLE="605.01.04.m.01";
	
	/** The label account if repoted. */
	public static String LABEL_ACCOUNT_IF_REPOTED="605.01.02.m.01";
	
	/** The label account iv repoted. */
	public static String LABEL_ACCOUNT_IV_REPOTED="605.01.02.m.02";
	
	/** The label account im repoted. */
	public static String LABEL_ACCOUNT_IM_REPOTED="605.01.02.m.03";
	
	/** The label account if lock. */
	public static String LABEL_ACCOUNT_IF_LOCK="605.02.01.m.01";
	
	/** The label account iv lock. */
	public static String LABEL_ACCOUNT_IV_LOCK="605.02.01.m.02";
	
	/** The label account im lock. */
	public static String LABEL_ACCOUNT_IM_LOCK="605.02.01.m.03";
	
	/** The label account if other lock. */
	public static String LABEL_ACCOUNT_IF_OTHER_LOCK="605.02.02.m.01";
	
	/** The label account iv other lock. */
	public static String LABEL_ACCOUNT_IV_OTHER_LOCK="605.02.02.m.02";
	
	/** The label account im other lock. */
	public static String LABEL_ACCOUNT_IM_OTHER_LOCK="605.02.02.m.03";
	
	/** The label account if physical. */
	public static String LABEL_ACCOUNT_IF_PHYSICAL="607.01.01.m.01";
	
	/** The label account iv physical. */
	public static String LABEL_ACCOUNT_IV_PHYSICAL="607.01.02.m.01";
	
	/** The label account im physical. */
	public static String LABEL_ACCOUNT_IM_PHYSICAL="607.01.03.m.01";
	
	/** The label account if not placed. */
	public static String LABEL_ACCOUNT_IF_NOT_PLACED="603.01.01.m.01";
	
	/** The label account iv not placed. */
	public static String LABEL_ACCOUNT_IV_NOT_PLACED="603.01.02.m.01";
	
	/** The label account im not placed. */
	public static String LABEL_ACCOUNT_IM_NOT_PLACED="603.01.03.m.01";
	
	
	
	/** The label date format. */
	public static String LABEL_DATE_FORMAT="d/m/yy h:mm";
	
	/** The label name file excel balance. */
	public static String LABEL_NAME_FILE_EXCEL_BALANCE="AccountingBalanceReport.xls";
	
	/** The label name file excel security. */
	public static String LABEL_NAME_FILE_EXCEL_SECURITY="DocumentSecurityReport.xls";
	
	/** The label name file excel squaring. */
	public static String LABEL_NAME_FILE_EXCEL_SQUARING="AccountingSquaringReport.xls";
	
	/** The label name file excel balance history. */
	public static String LABEL_NAME_FILE_EXCEL_BALANCE_HISTORY="AccountingBalanceHistoryReport.xls";
	
	/** The label name file excel client portfolio. */
	public static String LABEL_NAME_FILE_EXCEL_CLIENT_PORTFOLIO="ClientPortFolio.xls";
	public static String LABEL_NAME_FILE_EXCEL_COMMISSIONS_ISSUANCE_DPF="CommissionsIssuanceDPF.xls";
	public static String LABEL_NAME_FILE_EXCEL_COMMISSIONS_PHYSICAL_CUSTODY="CommissionsPhysicalCustody.xls";
	public static String LABEL_NAME_FILE_EXCEL_COMMISSIONS_SUMMARY_PORTFOLIO="CommissionsSummaryPortfolio.xls";
	
	/** The label name file excel settled operations. */
	public static String LABEL_NAME_FILE_EXCEL_SETTLED_OPERATIONS="SettledOperations.xls";
	
	/** The label name file excel registration assignment. */
	public static String LABEL_NAME_FILE_EXCEL_REGISTRATION_ASSIGNMENT="RegistrationAssingmentHolder.xls";
	
	/** The label name file excel blockages unlock. */
	public static String LABEL_NAME_FILE_EXCEL_BLOCKAGES_UNLOCK="BlockagesUnlock.xls";
	
	/** The label name file excel request accreditation. */
	public static String LABEL_NAME_FILE_EXCEL_REQUEST_ACCREDITATION="RequestAccreditation.xls";
	
	/** The label name file excel balance transfer availaible. */
	public static String LABEL_NAME_FILE_EXCEL_BALANCE_TRANSFER_AVAILABLE="BalanceTransferAvailable.xls";
	
	/** The label name file excel Entries Voluntary Account Issuer. */
	public static String LABEL_NAME_FILE_EXCEL_ENTRIES_VOLUNTARY_ACCOUNT_ISSUER="EntriesVoluntaryAccountIssuer.xls";

	/** The label name file excel trading record bbv. */
	public static String LABEL_NAME_FILE_EXCEL_TRADING_RECORD_BBV="TradingRecordBBV.xls";
	
	/** The label name file excel expiration securities by issuer. */
	public static String LABEL_NAME_FILE_EXCEL_EXPIRATION_SECURITIES_BY_ISSUER="ExpirationSecuritiesByIssuer.xls";
	
	/** The label name file excel expiration securities by issuer. */
	public static String LABEL_NAME_FILE_EXCEL_DETAILS_TITLES_PENDING_EXP="DetailsTitlesPendingExp.xls";
	
	/** The label name file excel placement segment by participant. */
	public static String LABEL_NAME_FILE_EXCEL_PLACEMENT_SEGMENT="PlacementSegment.xls";
	
	/** The label name file excel expiration securities by issuer. */
	public static String LABEL_NAME_FILE_EXCEL_VOLUNTARY_ACCOUNT_ENTRIES="VoluntaryAccountEntriesReport.xls";
	
	/** The label name file excel expiration securities by issuer. */
	public static String LABEL_NAME_FILE_EXCEL_ACTIONS="DetailsActionsSeriesReport.xls";
	
	/** The label name file transfer operation. */
	public static String LABEL_NAME_FILE_EXCEL_TRANSFER_OPERATION="transferOperation.xls";
	
	/** The label excel format. */
	public static String LABEL_EXCEL_FORMAT="EXCEL";
	
	/** The label pdf format. */
	public static String LABEL_PDF_FORMAT="PDF";
	
	/** The label participant. */
	public static String LABEL_PARTICIPANT="DEPOSITANTE";
	
	/** The label holder account. */
	public static String LABEL_HOLDER_ACCOUNT="CUENTA TITULAR";
	
	/** The label cui. */
	public static String LABEL_CUI="RUT";
	
	/** The label security class. */
	public static String LABEL_SECURITY_CLASS="CLASE VALOR";
	
	/** The label security code. */
	public static String LABEL_SECURITY_CODE="CÓDIGO DE VALOR";
	
	/** The label security class code. */
	public static String LABEL_SECURITY_CLASS_CODE ="CLASE CÓDIGO VALOR";
	
	/** The label issuer. */
	public static String LABEL_ISSUER="EMISOR";
	
	/** The label currency mnemonic. */
	public static String LABEL_CURRENCY_MNEMONIC="MONEDA";
	
	/** The label issuance date. */
	public static String LABEL_ISSUANCE_DATE="FECHA EMISION";
	
	/** The label plazo. */
	public static String LABEL_PLAZO="PLAZO";
	
	/** The label vn. */
	public static String LABEL_VN="VALOR NOMINAL";
	
	public static String LABEL_SECURITY_PUBLIC_PRIVATE="VALOR PUBLICO - PRIVADO";
	
	public static String LABEL_VN_INITIAL="VALOR NOMINAL INICIAL";
	
	public static String LABEL_VN_CURRENT="VALOR NOMINAL ACTUAL";
	
	public static String LABEL_VN_INITIAL_USD="MONTO A VALOR NOMINAL INICIAL USD";
	
	public static String LABEL_VN_CURRENT_USD="MONTO A VALOR NOMINAL ACTUAL USD";
	
	/** The label nominal rate. */
	public static String LABEL_NOMINAL_RATE="TASA NOMINAL";
	
	/** The label final rate. */
	public static String LABEL_FINAL_RATE="VALOR FINAL";
	
	/** The label total balance. */
	public static String LABEL_TOTAL_BALANCE="SALDO CONTABLE";
	
	/** The label available balance. */
	public static String LABEL_AVAILABLE_BALANCE="SALDO DISPONIBLE";
	
	/** The label reported balance. */
	public static String LABEL_REPORTED_BALANCE="SALDO REPORTADO";
	
	/** The label reported balance. */
	public static String LABEL_REPORTING_BALANCE="SALDO REPORTADOR";
	
	/** The label reported balance. */
	public static String LABEL_PAWN_BALANCE="SALDO GRAVAMEN";
	
	/** The label reported balance. */
	public static String LABEL_BAN_BALANCE="SALDO EMBARGO";
	
	/** The label reported balance. */
	public static String LABEL_OTHER_BLOCK_BALANCE="SALDO OTROS BLOQUEOS";
	
	/** The label reported balance. */
	public static String LABEL_ACCREDITATION_BALANCE="ACREDITACION";
	
	/** The label markfact. */
	public static String LABEL_MARKFACT="PRECIO DE MERCADO";
	
	/** The label rate percent. */
	public static String LABEL_RATE_PERCENT="TASA %";
	
	public static String LABEL_RATE="TASA";
	
	/** The label total amount. */
	public static String LABEL_TOTAL_AMOUNT="MONTO TOTAL";
	
	/** The label current nv. */
	public static String LABEL_CURRENT_NV="ACTUAL";
	
	/** The label total current nv. */
	public static String LABEL_TOTAL_CURRENT_NV="TOTAL";
	
	/** The label issuance. */
	public static String LABEL_ISSUANCE="EMISION";
	
	/** The label balance. */
	public static String LABEL_BALANCE="SALDOS";
	
	/** The label mark. */
	public static String LABEL_MARK="A MERCADO";
	
	/** The label nominal value. */
	public static String LABEL_NOMINAL_VALUE="VALOR NOMINAL";
	
	/** SETTLED OPERATIONS - OPERACIONES LIQUIDADAS. */
	public static String LABEL_OPERATION_DATE="FECHA DE OPERACION";
	
	public static String LABEL_REGISTER_DATE="FECHA DE REGISTRO";
	
	public static String LABEL_LOCK_DATE="FECHA BLOQUEO";
	
	public static String LABEL_UNLOCK_DATE="FECHA DESBLOQUEO";
	
	public static String LABEL_SETTLEMENT_COUNT="FECHA LIQUIDACION CONTADO";
	
	public static String LABEL_SETTLEMENT_TERM="FECHA LIQUIDACION PLAZO";
	
	public static String LABEL_NRO_OPERATION="NRO OPERACION";
	
	/** The label modality. */
	public static String LABEL_MODALITY="MODALIDAD";
	
	public static String LABEL_INSTRUMENT_TYPE="INSTRUMENTO";
	
	/** The label ballot number. */
	public static String LABEL_BALLOT_NUMBER="PAPELETA";

	public static String LABEL_OPERATION_PRICE_SETTLEMENT="PRECIO LIQUIDACION";
	
	public static String LABEL_OPERATION_PRICE_CURVE="PRECIO CURVA";
	
	public static String LABEL_OPERATION_SECURITY_RATE="TASA EMISION";
	public static String LABEL_OPERATION_STATE="ESTADO OPERACION REPORTO";
	public static String LABEL_OPERATION_ECONOMIC_BUY="ACTIVIDAD ECONOMICA COMPRA";
	public static String LABEL_OPERATION_ECONOMIC_SELL="ACTIVIDAD ECONOMICA VENTA";
	public static String LABEL_OPERATION_OBSERVATIONS="OBSERVACIONES";
	
	
	public static String LABEL_OPERATION_FECHA_OPERA_REPORTO="FECHA OP. REPORTO";
	
	public static String LABEL_OPERATION_FECHA_LIQ_PLAZO="FECHA LIQ. PLAZO";
	
	/** The label sequential. */
	public static String LABEL_SEQUENTIAL="SECUENCIAL";
	
	/** The label stock quantity. */
	public static String LABEL_STOCK_QUANTITY="CANTIDAD";
	
	/** The label stock quantity. */
	public static String LABEL_SECURITY_TERM="PLAZO EN DIAS";
	
	public static String LABEL_TERM_REMAINING="PLAZO RESTANTE";
	
	public static String LABEL_TERM_REPORTO="PLAZO REPORTO";
	
	/** The label operation price. */
	public static String LABEL_OPERATION_PRICE="PRECIO";
	
	/** The label operation part. */
	public static String LABEL_OPERATION_PART="PARTE OPERACION";
	
	/** The label buy participant. */
	public static String LABEL_BUY_PARTICIPANT="AGENCIA COMPRADORA";
	
	public static String LABEL_BAG_AGENCY="AGENCIA DE BOLSA";
	
	/** The label buy holder account. */
	public static String LABEL_BUY_HOLDER_ACCOUNT="CUENTA COMPRA";
	
	/** The label buy holder account. */
	public static String LABEL_BUY_CUI_NAME="CUI NOMBRE COMPRA";
	
	public static String LABEL_DATETIME_REGISTER="FECHA Y HORA DE REGISTRO";
	
	public static String LABEL_DATE_OPERATION="FECHA OPERACION";
	
	/** The label buy holder account. */
	public static String LABEL_BUY_CUI_ACT_ECO="CUI ACT ECO COMPRA";
	
	/** The label buy holder account. */
	public static String LABEL_BUY_CUI_TYPE="TIPO CUI COMPRA";
	
	public static String LABEL_BUY_CUI="CUI COMPRA";
			
	/** The label sell participant. */
	public static String LABEL_SELL_PARTICIPANT="PARTICIPANTE VENTA";
	
	public static String LABEL_BUYER_PARTICIPANT="PARTICIPANTE COMPRA";
	
	/** The label sell holder account. */
	public static String LABEL_SELL_HOLDER_ACCOUNT="CUENTA VENTA";
	
	/** The label sell holder account. */
	public static String LABEL_SELL_CUI_NAME="CUI NOMBRE VENTA";
	
	public static String LABEL_CI="CI";
	
	/** The label buy holder account. */
	public static String LABEL_SELL_CUI_ACT_ECO="CUI ACT ECO VENTA";
	
	/** The label sell holder account. */
	public static String LABEL_SELL_CUI_TYPE="TIPO CUI VENTA";
	
	public static String LABEL_SELL_CUI="CUI VENTA";
	
	/** The label total operation amount. */
	public static String LABEL_TOTAL_OPERATION_AMOUNT="MONTO ORIGINAL";
	
	/** The label total operation amount usd. */
	public static String LABEL_TOTAL_OPERATION_AMOUNT_USD="MONTO DOLARES";
	
	/** The label settlement type. */
	public static String LABEL_SETTLEMENT_TYPE="LIQ TIPO";
	
	/** The label instancia. */
	public static String LABEL_INSTANCIA="LIQ PROCESO";
	
	/** COMISIONES POR MANTENIMIENTO EN CUENTA. */
	public static String LABEL_CURRENCY_ORIGINAL="TOTAL MONEDA ORIGINAL";
	
	/** The label total usd. */
	public static String LABEL_TOTAL_USD="TOTAL EN USD";
	
	/** The label codigo tarifa. */
	public static String LABEL_CODIGO_TARIFA="CODIGO TARIFA";
	
	public static String LABEL_CARTERA_USD="CARTERA EN USD";
	
	public static String LABEL_CODIGO_SERVICIO="CODIGO DE SERVICIO";
	
	/** The label tarifa. */
	public static String LABEL_TARIFA="TARIFA";
	
	/** The label comission usd. */
	public static String LABEL_COMISSION_USD="COMISION EN USD";
	
	/** The label exchange rate. */
	public static String LABEL_EXCHANGE_RATE="TIPO DE CAMBIO";
	
	/** The label comission bob. */
	public static String LABEL_COMISSION_BOB="COMISION EN PYG";
	
	public static String LABEL_CONTRIBUTION_FUND="APORTE FONDO DE GARANTIA BS.";
	
	/** The label date. */
	public static String LABEL_DATE="FECHA";
	
	public static String LABEL_RATE_ISSUANCE="TASA DE EMISION";
	public static String LABEL_RATE_MARKETFACT="TASA DE HECHO DE MERCADO";
	public static String LABEL_BUSINESS_NAME="RAZON SOCIAL";
	public static String LABEL_NEGOTIATION_RATE="TASA DE NEGOCIACION";
	public static String LABEL_SELAR="SELAR"; 
	public static String LABEL_RATE_NOMINAL="TASA DE INTERES NOMINAL";
	public static String LABEL_PERIOD_PAYMENT_INTERES="PERIDO DE PAGO INTERES";
	public static String LABEL_PERIOD_PAYMENT_AMORTIZATION="PERIODO DE PAGO AMORTIZACION";
	public static String LABEL_DATE_PAYMENT_INTEREST="FECHA DE PAGO CUPON DE INTERES";
	public static String LABEL_DATE_PAYMENT_AMORTIZATION="FECHA DE PAGO AMORTIZACION";
	public static String LABEL_CP="COLOCACION PRIMARIA";
	
	public static String LABEL_LAST_DATE_PAYMENT_INT="FECHA DE ULTIMO INTERES PAGADO";
	public static String LABEL_LAST_DATE_PAYMENT_AMR="FECHA DE ULTIMA AMORTIZACION PAGADA";
	public static String LABEL_NEXT_DATE_PAYMENT_INT="SIGUIENTE FECHA DE PAGO DE INTERES";
	public static String LABEL_NEXT_DATE_PAYMENT_AMR="SIGUIENTE FECHA DE PAGO DE AMORTIZACION";
	
	/** The label service. */
	public static String LABEL_SERVICE="SERVICIO";
	public static String LABEL_TARIFARIO="TARIFARIO";
	public static String LABEL_CODE_SERVICE="CODIGO DE SERVICIO";
	public static String LABEL_MONTO_MONEDA_ORIGINAL="MONTO MONEDA ORIGINAL";
	public static String LABEL_MONTO_MONEDA_USD="MONTO MONEDA EN USD";
	public static String LABEL_VALOR_REFERENCIA_USD="VALOR REFERENCIA USD";
	
	/** The label comission ufv. */
	public static String LABEL_COMISSION_UFV="COMISION EN UFV";

	/** REGISTER ASSIGNMENT. */
	
	public static String LABEL_OPERATION="OPERACION";
	
	/** The label cui description. */
	public static String LABEL_CUI_DESCRIPTION="RUT - DESCRIPCION";
	
	/** The label encharge. */
	public static String LABEL_ENCHARGE="ENCARGO";
	
	/** The label rate percent marketfact. */
	public static String LABEL_RATE_PERCENT_MARKETFACT="TASA DE MERCADO";
	
	/** The label marketfact date. */
	public static String LABEL_MARKETFACT_DATE="FECHA DE MERCADO";
	
	/** The label state. */
	public static String LABEL_STATE="ESTADO";
	public static String LABEL_HOLDER="TITULAR";
	
	/** Blockages Unlock */
	
	public static String LABEL_BLOCK_OPERATION="OPERACION BLOQUEO";
	public static String LABEL_REQ_BLOCK_NUMBER="SOL BLOQUEO";
	public static String LABEL_UNLOCK_OPERATION="OPERACION DESBLOQUEO";
	public static String LABEL_REQ_UNLOCK_NUMBER="SOL DESBLOQUEO";
	public static String LABEL_DATE_BLOCK="FECHA Y HORA BLOQUEO";
	public static String LABEL_DATE_UNLOCK="FECHA Y HORA DESBLOQUEO";
	public static String LABEL_BLOCK_TYPE="TIPO BLOQUEO";
	
	/** Request Accreditation to Nominal Value */
	public static String LABEL_REQUEST_OPERATION="OPERACION BLOQUEO";
	public static String LABEL_MOTIVE="MOTIVO"; 
	public static String LABEL_DATE_REGISTER="FECHA REGISTRO"; 
	public static String LABEL_DATE_ISSUANCE="FECHA EMISION";
	public static String LABEL_CURRENCY="MONEDA";
	public static String LABEL_CERTIFICATION_TYPE="TIPO SALDO ACREDITAR";
	public static String LABEL_TOTAL_BALANCE_CERT="TOTAL SALDO ACREDITADO";
	public static String LABEL_INIT_NOMINAL_VALUE="VALOR NOMINAL UNITARIO";
	public static String LABEL_TOTAL_NOMINAL_VALUE="TOTAL VALOR NOMINAL USD";
	public static String LABEL_TOTAL_QUANTITY="CANTIDAD TOTAL";
	
	public static String LABEL_QUANTITY_LOCK_SECURITIES="CANTIDAD DE VALORES BLOQUEADOS";
	public static String LABEL_AVAILABLE_LOCK_SECURITIES="SALDO DE VALORES BLOQUEADOS";
	public static String LABEL_LEVEL_AFECTATION="NIVEL DE AFECTACION";
			
	/** Balance Transfer Available*/
	public static String LABEL_REQUEST_NUMBER="No SOLICITUD";
	public static String LABEL_TRANSFER_TYPE="TIPO TRASPASO";
	public static String LABEL_PARTICIPANT_ORIGIN="PARTICIPANTE ORIGEN";
	public static String LABEL_ACCOUNT_ORIGIN="CUENTA ORIGEN";
	public static String LABEL_PARTICIPANT_DESTINATION="PARTICIPANTE DESTINO";
	public static String LABEL_ACCOUNT_DESTINATION="CUENTA DESTINO";
	public static String LABEL_USER="USUARIO";
	
	/** Balance Transfer Available*/
	public static String LABEL_REQUEST_DATE="FECHA SOLICITUD";
	public static String LABEL_TITLE_NUMBER="No TITULO";
	public static String LABEL_OBSERVATIONS="OBSERVACIONES";
	
	/** Details of titles pending of expiration	 */
	public static String LABEL_COUPON="CUPON";
	public static String LABEL_AVAILABLE="DISPONIBLE";
	public static String LABEL_PAWN="PRENDA";
	public static String LABEL_BAN="EMBARGO";
	public static String LABEL_CAT="ACREDITACION";
	public static String LABEL_INTEREST_AMOUNT="MONTO INTERES";
	public static String LABEL_AMORTIZATION_AMOUNT="MONTO AMORTIZACION";
	
	public static String LABEL_WAY_TO_PAY="FORMA DE PAGO";
	public static String LABEL_MECHANISM_NEG="MECANISMO";
	public static String LABEL_MODALITY_NEG="MODALIDAD";
	
	public static String LABEL_ALTERNATE_SERIES = "SERIE ALTERNA";
	public static String LABEL_PART = "PARTE";
	public static String LABEL_ROL = "ROL";
	public static String LABEL_ORIGIN_HOLDER = "TITULAR ORIGEN";
	public static String LABEL_ORIGIN_DESTINATION = "TITULAR DESTINO";
	public static String LABEL_OPERATION_TYPE = "TIPO DE OPERACION";
	public static String LABEL_MODALIDAD = "MODALIDAD";
	
	/** The settlement schema. */
	
	public static String SETTLEMENT_SCHEMA="settlement_schema";
	
	/** The settlement schema param. */
	public static String SETTLEMENT_SCHEMA_PARAM="schema";
	
	/** The settlement type. */
	public static String SETTLEMENT_TYPE="settlement_type";
	
	/** The unfulfillment type. */
	public static String UNFULFILLMENT_TYPE="unfulfillment_type";
	
	/** The unfulfillment part id. */
	public static String UNFULFILLMENT_PART_ID="unfulfillment_part_id";
	
	/** The affected part id. */
	public static String AFFECTED_PART_ID="affected_part_id";
 	
	 /** The currency type. */
	 public static String CURRENCY_TYPE="currency";
	 
	 /** The currency desc type. */
	 public static String CURRENCY_TYPE_DESC="currency_desc";
	
	/** The operation part. */
	public static String OPERATION_PART="operation_part";
	
	/** The unfulfillment motive. */
	public static String UNFULFILLMENT_MOTIVE="unfulfillment_motive";
	
	/** The schedule type. */
	public static String SCHEDULE_TYPE="schedule_type";
	
	/** The stage. */
	public static String STAGE="stage";
	
	/** The logo path label. */
	public static String LOGO_PATH_LABEL = "logo_path";
	
	/** The logo resource path. */
	public static String LOGO_RESOURCE_PATH = "idepositaryreport.jpg";
	
	/** The corp process id. */
	public static String CORP_PROCESS_ID="process_id";
	
	/** The cutoff date. */
	public static String CUTOFF_DATE="cutoff_date";
	
	/** The corporative cutoff date. */
	public static String CORPORATIVE_CUTOFF_DATE="lbl_cutoff_date";
	
	/** The delivery date. */
	public static String DELIVERY_DATE="delivery_date";
	
	/** The corporate event type. */
	public static String CORPORATE_EVENT_TYPE="event_type";
	
	/** The code isin. */
	public static String CODE_ISIN="code_value";
	
	/** The dest code isin. */
	public static String DEST_CODE_ISIN="dest_code_value";
	
	/** The report type. */
	public static String REPORT_TYPE="report_type";
	
	/** The period type. */
	public static String PERIOD_TYPE="period_type";
	
	/** The year combo. */
	public static String YEAR_COMBO="year";
	
	// SECURITY REPORT CONSTANTS
	
	/** The system. */
	public static String SYSTEM = "system";
	
	/** The mnemonic. */
	public static String MNEMONIC = "mnemonic";
	   
	/** The sign type. */
	public static String SIGN_TYPE="sign_type";		
	
	/** The id interface send pk. */
	public static String ID_INTERFACE_SEND_PK = "id_interface_send_pk";
	
	/** The id interface pk. */
	public static String ID_INTERFACE_PK = "id_interface_pk";
	
	/** The table. */
	public static String TABLE = "table";
	
	/** The column. */
	public static String COLUMN = "column";
	
	/** The user. */
	public static String USER = "user";
	
	/** The dml action type. */
	public static String DML_ACTION_TYPE = "type";
	
	/** The name report deafult. */
	public static String NAME_REPORT_DEAFULT = "EDVRPT";
	
	/** The user code. */
	//AUDIT CONSOLIDATED REPORT
	public static String USER_CODE = "userCode";
	
	/** The ip. */
	public static String IP = "ip";
	
	/** The process type. */
	public static String PROCESS_TYPE = "processType";
	
	/** The process state. */
	public static String PROCESS_STATE = "processState";
	
	/** The start date. */
	public static String START_DATE = "start_date";
	
	/** The end date. */
	public static String END_DATE = "end_date";
	
	/** The event type. */
	public static String EVENT_TYPE = "eventType";

	/** The user code id. */
	//REPORTS AND PROCESS CONTROL
	public static String USER_CODE_ID = "user_code";
	
	/** The user type. */
	public static String USER_TYPE = "user_type";
	
	/** The institution type. */
	public static String INSTITUTION_TYPE = "institution_type";
	
	/** The issuer code institution type. */
	public static String ISSUER_CODE_INSTITUTION_TYPE = "issuer_code_institution_type";
	
	/** The participant investor institution type. */
	public static String PARTICIPANT_INVESTOR_INSTITUTION_TYPE = "participant_investor_institution_type";
	
	/** The institution name. */
	public static String INSTITUTION_NAME = "institution";
	
	/** The initial date. */
	public static String INITIAL_DATE = "initial_date";
	
	/** The final date. */
	public static String FINAL_DATE = "final_date";
	
	/** The system name. */
	public static String SYSTEM_NAME = "system_name";
	
	/** The corporative balance origin. */
	public static String CORPORATIVE_BALANCE_ORIGIN="1";
	
	/** The corporative balance destiny. */
	public static String CORPORATIVE_BALANCE_DESTINY="2";
	
	/** The participant. */
	public static String PARTICIPANT = "participant_param";

	/** The format only two decimal. */
	public static String FORMAT_ONLY_TWO_DECIMAL= "##0.00";
	
	/** The format only four decimals. */
	public static String FORMAT_ONLY_FOUR_DECIMALS= "##0.0000";
	
	/** The format only eigth decimals. */
	public static String FORMAT_ONLY_EIGTH_DECIMALS= "##0.00000000";
	
	/** The format only ten decimal. */
	public static String FORMAT_ONLY_TEN_DECIMAL= "###.##########";
	
	/** The format two decimal default zero. */
	public static String FORMAT_TWO_DECIMAL_DEFAULT_ZERO= "##0.00";
	
	
	/** The reference note external. */
	//CONSTANTS FOR NOTE TO EXTERNAL REPORTS
	public static String REFERENCE_NOTE_EXTERNAL = "reference";
	
	/** The quote note external. */
	public static String QUOTE_NOTE_EXTERNAL = "quote";
	
	/** The attached sheets. */
	public static String ATTACHED_SHEETS = "attached_sheets";
	
	/** The balance origin. */
	public static Integer BALANCE_ORIGIN = Integer.valueOf(1);
	
	/** The balance destiny. */
	public static Integer BALANCE_DESTINY = Integer.valueOf(2);
		
	/** The error exchange rate by date. */
	public static String ERROR_EXCHANGE_RATE = "No existe tasa de cambio en D\u00F3lares para la fecha";
	
	/** The error exchange rate by date. */
	public static String ERROR_STOCK_CALCULATION_PROCESS_NOT_EXIST = "No existe un c\u00E1lculo de stock mensual para la fecha";
	
	/** The Constant INTEGER_MAX_REPORTS. */
	public static final Integer INTEGER_MAX_REPORTS = new Integer(20000);
	
	/** The extends pdf. */
	public static String EXTENDS_PDF = ".pdf";
	
	/** The Constant PARAMTER_FTP_SETTLEMENTS. */
	public static final Integer PARAMTER_FTP_SETTLEMENTS = new Integer(2339);
	
	/**Parametro para deshabilitar fecha */
	public static String DATE_DISABLED =  "date_disabled";
	
	/**/
	public static String SUSPENDED_SECURITIES = "suspended_securities";
	
	public static String TEMPLATE_NAME = "template_name";
	
	public static String CALL_GENERATE_DATA="call_generate_data";
	
	public static final String REPORT_EXCEL_DELIMITADO="REPORT_EXCEL_DELIMITADO";
}
