package com.pradera.report.generation.executor.collectioneconomic.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.Query;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.report.generation.executor.collectioneconomic.to.RequestCollectionTO;
import com.pradera.report.generation.service.ReportManageServiceBean;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class CollectionReportServiceBean extends CrudDaoServiceBean  {
	
	@Inject
	ReportManageServiceBean reportManageServiceBean;
	
	/**Buscamos las solicitude de registro, se usa la misma query a la que se usa en 
	 * el reporte PDF
	 * */
	public List<Object[]> searchEconomicCollectionRequest(RequestCollectionTO requestCollectionTO){
		StringBuilder stringBuilder = new StringBuilder();
		
		stringBuilder.append(" SELECT ");
		stringBuilder.append(" CER.ID_REQ_COLLECTION_ECONOMIC_FK AS REQ_NUMBER ");
		stringBuilder.append(" ,RCE.ID_PARTICIPANT_FK AS ID_PARTICIPANT_PK ");
		stringBuilder.append(" ,(SELECT PARTICIPANT.DESCRIPTION FROM PARTICIPANT WHERE PARTICIPANT.ID_PARTICIPANT_PK = RCE.ID_PARTICIPANT_FK) AS DESC_PARTICIPANT ");
		stringBuilder.append(" ,TO_CHAR(RCE.PROCESS_DATE,'dd/MM/yyyy') AS REQ_DATE ");
		
		stringBuilder.append(" ,CASE ");
		stringBuilder.append(" WHEN CHECK_OTHER = 1 ");
		stringBuilder.append(" THEN CER.ANOTHER_CURENCY ");
		stringBuilder.append(" ELSE CER.CURRENCY ");
		stringBuilder.append(" END AS CURRENCY ");
		
		stringBuilder.append(" ,(SELECT TEXT1 FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = CER.CURRENCY) AS DESC_CURRENCY_ORIGEN ");
		
		stringBuilder.append(" ,(SELECT TEXT1 FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = ( ");
		stringBuilder.append(" CASE ");
		stringBuilder.append(" WHEN CHECK_OTHER = 1 ");
		stringBuilder.append(" THEN CER.ANOTHER_CURENCY ");
		stringBuilder.append(" ELSE CER.CURRENCY ");
		stringBuilder.append(" END)) AS CURRENCY_DESC ");
		
		stringBuilder.append(" ,CER.ID_HOLDER_PK AS CUI ");
		stringBuilder.append(" ,(select HOLDER.FULL_NAME from HOLDER WHERE HOLDER.ID_HOLDER_PK = CER.ID_HOLDER_PK) AS DESC_HOLDER ");
		stringBuilder.append(" ,CER.COUPON_NUMBER AS COUPON_NUMBER ");
		stringBuilder.append(" ,CER.ID_SECURITY_CODE_FK AS ID_SECURITY_CODE_PK ");
		stringBuilder.append(" ,CER.AMOUNT_SECURITIES AS AMOUNT_SECURITIES ");
		stringBuilder.append(" ,RCE.STATE AS REQ_STATE ");
		
		stringBuilder.append(" ,CASE ");
		stringBuilder.append(" WHEN CHECK_OTHER = 1 ");
		stringBuilder.append(" THEN CER.ANOTHER_AMOUNT_RECEIVABLE ");
		stringBuilder.append(" ELSE CER.AMOUNT_RECEIVABLE ");
		stringBuilder.append(" END  AS PAYMENT_AMOUNT ");
		
		stringBuilder.append(" ,CER.AMOUNT_RECEIVABLE AS AMOUNT_EXPIRATION ");
		stringBuilder.append(" ,CER.AMORTIZATION AS AMORTIZATION_AMOUNT ");
		stringBuilder.append(" ,CER.INTEREST AS INTEREST_AMOUNT ");
		stringBuilder.append(" ,TO_CHAR(CER.PAYMENT_DATE,'dd/MM/yyyy') AS PAYMENT_DATE ");
		stringBuilder.append(" ,TO_CHAR(CER.DUE_DATE,'dd/MM/yyyy') as EXPIRATION_DATE ");
		
		stringBuilder.append(" ,CER.ID_BANK_FK ");
		stringBuilder.append(" ,(select BANK.MNEMONIC from BANK WHERE BANK.ID_BANK_PK = CER.ID_BANK_FK) AS DESC_BANK ");
		stringBuilder.append(" ,CER.DEP_ACCOUNT_NUMBER AS DEP_ACCOUNT_NUMBER ");
		stringBuilder.append(" ,RCE.STATE ");
		stringBuilder.append(" ,(SELECT PARAMETER_TABLE.DESCRIPTION FROM PARAMETER_TABLE WHERE RCE.STATE = parameter_table.PARAMETER_TABLE_PK) AS DESC_STATE ");
		stringBuilder.append(" ,SE.INSTRUMENT_TYPE ");
		stringBuilder.append(" from COLLECTION_ECONOMIC_RIGHT CER ");
		stringBuilder.append(" RIGHT JOIN REQ_COLLECTION_ECONOMIC_RIGHT RCE ON CER.ID_REQ_COLLECTION_ECONOMIC_FK = RCE.ID_REQ_COLLECTION_ECONOMIC_PK ");
		stringBuilder.append(" INNER JOIN SECURITY SE ON SE.ID_SECURITY_CODE_PK = CER.ID_SECURITY_CODE_FK ");
		stringBuilder.append(" WHERE 1 = 1 ");
		
		stringBuilder.append(" AND RCE.STATE in (2385,2387,2388) ");
		
		if(requestCollectionTO.getIdParticipantPk() != null)
		stringBuilder.append(" AND RCE.ID_PARTICIPANT_FK = :idParticipantPk ");
		
		if(requestCollectionTO.getIdHoderPk()!=null)
			stringBuilder.append(" AND CER.ID_HOLDER_PK = :idHolderPk");
		
		if(requestCollectionTO.getIdSecurityCodePk()!=null)
			stringBuilder.append(" AND CER.ID_SECURITY_CODE_FK =  :idSecurityCodePk ");
		
		if(requestCollectionTO.getDueDate()!=null)
			stringBuilder.append(" AND CER.DUE_DATE = TO_DATE( :dueDate,'dd/MM/yyyy')");
		
		if(requestCollectionTO.getPaymentDate()!=null)
			stringBuilder.append(" AND CER.PAYMENT_DATE = TO_DATE( :paymentDate,'dd/mm/yyyy') ");
		/**Numero de solicitud*/
		if(requestCollectionTO.getIdReqCollectionEconomicPk()!=null)
			stringBuilder.append(" AND CER.ID_REQ_COLLECTION_ECONOMIC_FK = :idReqCollectionEconomicPK ");
		
		if(requestCollectionTO.getInitialDate()!=null&&requestCollectionTO.getFinalDate()!=null)
			stringBuilder.append(" AND TRUNC(CER.PAYMENT_DATE) BETWEEN TO_DATE( :dateInitial ,'dd/MM/yyyy') AND TO_DATE( :finalDate ,'dd/MM/yyyy') ");
		else
			return null;
		
		stringBuilder.append(" ORDER BY CER.PAYMENT_DATE ASC,CER.DUE_DATE ASC,SE.ID_ISSUER_FK,SE.ID_SECURITY_CODE_PK ");
	
		Query query = em.createNativeQuery(stringBuilder.toString());
		
		if(requestCollectionTO.getIdParticipantPk() != null)
			query.setParameter("idParticipantPk",requestCollectionTO.getIdParticipantPk());
			
		if(requestCollectionTO.getIdHoderPk()!=null)
			query.setParameter("idHolderPk",requestCollectionTO.getIdHoderPk());
		
		if(requestCollectionTO.getIdSecurityCodePk()!=null)
			query.setParameter("idSecurityCodePk",requestCollectionTO.getIdSecurityCodePk());
		
		if(requestCollectionTO.getDueDate()!=null)
			query.setParameter("dueDate",requestCollectionTO.getDueDate());
		
		if(requestCollectionTO.getPaymentDate()!=null)
			query.setParameter("paymentDate",requestCollectionTO.getPaymentDate());
		
		/**Numero de solicitud*/
		if(requestCollectionTO.getIdReqCollectionEconomicPk()!=null)
			query.setParameter("idReqCollectionEconomicPK",requestCollectionTO.getIdReqCollectionEconomicPk());
		
		if(requestCollectionTO.getInitialDate()!=null&&requestCollectionTO.getFinalDate()!=null){
			query.setParameter("dateInitial",requestCollectionTO.getInitialDate());
			query.setParameter("finalDate",requestCollectionTO.getFinalDate());
		}
			
		@SuppressWarnings("unchecked")
		List<Object[]> listResult = query.getResultList();
		return listResult;
	}
}