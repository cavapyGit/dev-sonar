package com.pradera.report.generation.jms;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.model.report.ReportLogger;
import com.pradera.report.ReportContext;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class JMSSenderReportThree.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 11/07/2013
 */
@Stateless
public class JMSSenderReportThree extends JMSQueueBean {
	
	/** serial version id. */
	private static final long serialVersionUID = 1L;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The report queue. */
	@Resource(lookup="java:/queue/ReportProcessReceiveQueueThree")
	private Queue reportQueue;
	
	/** The connection factory. */
	@Resource(lookup="java:/IdepositaryQueueConnectionFactory")
    private ConnectionFactory connectionFactory;
	
	/** The session. */
	private Session session;
	
    /** The connection. */
    private Connection connection;
    
    /**
     * Inits the.
     */
    @PostConstruct
    public void init() {
        try {
        	 connection = connectionFactory.createConnection();
             session=connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
             jmsMessageProducer=session.createProducer(reportQueue);
             connection.start();
        } catch (JMSException ex) {
        	log.error(ex.getMessage());
            throw new RuntimeException(ex);
        }
    }
    
    /**
     * Destroy.
     */
    @PreDestroy
    public void destroy() {
    	try{
			if (jmsMessageProducer != null) {
				jmsMessageProducer.close();
			}
			if(session != null) {
				session.close();
			}
			if(connection!= null) {
				connection.close();
			}

		} catch(Exception ex){
			log.error(ex.getMessage());
		}
  
    }


	/**
	 * Send message.
	 *
	 * @param reportLogger the report logger
	 */
	public void sendMessage(ReportLogger reportLogger){
		try{
			ObjectMessage  objectMessage = session.createObjectMessage();
			ReportContext reportContext = createContext(reportLogger);
			objectMessage.setObject(reportContext);
			jmsMessageProducer.send(objectMessage);
		}catch(JMSException jex){
			log.error(jex.getMessage());
			jex.printStackTrace();
		} catch(Exception ex) {
			log.error(ex.getMessage());
		}
		 
		 
	}
}
