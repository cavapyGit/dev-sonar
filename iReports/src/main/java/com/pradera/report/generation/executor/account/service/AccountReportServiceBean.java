package com.pradera.report.generation.executor.account.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.Query;

import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.corporateevents.to.CorporativeOperationTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.helperui.to.HolderHelperOutputTO;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.CrossInformationResult;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.HolderHistory;
import com.pradera.model.accounts.HolderReqFileHistory;
import com.pradera.model.accounts.LegalRepresentativeHistory;
import com.pradera.model.accounts.RepresentativeFileHistory;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.holderaccounts.HolderAccountBankRequest;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetRequest;
import com.pradera.model.accounts.holderaccounts.HolderAccountFileRequest;
import com.pradera.model.custody.type.StateType;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.type.InformationSourceType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.report.Report;
import com.pradera.model.report.ReportLogger;
import com.pradera.report.generation.executor.account.to.ConsolidateParentAccountTO;
import com.pradera.report.generation.executor.account.to.NoteRegistrationDGravamenTO;
import com.pradera.report.generation.executor.account.to.PortafolioDetailCustodianTO;
import com.pradera.report.generation.executor.issuances.to.ListHoldersCuiReportTO;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class AccountReportServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 12/07/2013
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class AccountReportServiceBean extends CrudDaoServiceBean {
	
	@Inject
	private NotificationServiceFacade notificationServiceFacade;
	
    /**
     * Default constructor. 
     */
    public AccountReportServiceBean() {
        // TODO Auto-generated constructor stub
    }
    
    
   /**
    * Gets the participant list by class.
    *
    * @param participantClass the participant class
    * @return the participant lis by class
    */
   public  List<Object[]> getParticipantListByClass(Integer participantClass) {
    	StringBuilder sbQuery = new StringBuilder(100);
    	sbQuery.append(" select p.id_participant_pk, ").
    	append("p.description,").
    	append("p.mnemonic,").
    	append("p.document_type,").
    	append("p.document_number,").
    	append("p.phone_number,").
    	append("p.state,").
    	append("p.registry_date,").
    	//8
    	append("p.account_class,").
    	append("decode(p.state,6,'S','N') as vigent").    	
    	append(" from participant p ");
    	if (participantClass != null) {
    		sbQuery.append(" where p.account_class = :class");
    	}
    	sbQuery.append(" order by p.account_class,p.id_participant_pk ");
    	Query query = em.createNativeQuery(sbQuery.toString());
    	if (participantClass != null) {
    		query.setParameter("class", participantClass);
    	}
    	return query.getResultList();
    }
   
   public  List<Object[]> getParticipantListForReport(Integer participantType, Integer participantClass, Integer participantState) {
	   	StringBuilder sbQuery = new StringBuilder();
	   	sbQuery.append(" select p.id_participant_pk as code, "). //0
	   	append("p.description,"). //1
	   	append("p.mnemonic,"). //2
	   	append("p.document_type,"). //3
	   	append("p.document_number,"). //4
	   	append("p.phone_number,"). //5
	   	append("p.account_Type,"). //6
	   	append("p.state,"). //7
	   	append("p.registry_date,"). //8
	   	append("p.account_class,"). //9
	   	append(" case p.state when 520 then "). //10
	   	append(" (SELECT TO_CHAR(pa3.id_participant_pk) FROM participant pa3 where pa3.id_origin_participant_fk=p.id_participant_pk) ").
	   	append(" else '---' end as participant_vigent ").  	
	   	append(" from participant p ").
	   	append(" Where 1=1 ");
	   	
	   	if (Validations.validateIsNotNullAndPositive(participantType)) {   		
	   		sbQuery.append(" And p.account_type = :participant_type");
	   	}
	   	
	   	if (Validations.validateIsNotNullAndPositive(participantClass)) {
	   		sbQuery.append(" And p.account_class = :participant_class");
	   	}
	   	
	   	if (Validations.validateIsNotNullAndPositive(participantState)) {
	   		sbQuery.append(" And p.state = :participant_state");
	   	}
	   	
	   	sbQuery.append(" order by p.account_class,p.id_participant_pk ");
	   	Query query = em.createNativeQuery(sbQuery.toString());

	   	
	   	if (Validations.validateIsNotNullAndPositive(participantType)) {   		
	   		query.setParameter("participant_type", participantType);
	   	}
	   	
	   	if (Validations.validateIsNotNullAndPositive(participantClass)) {
	   		query.setParameter("participant_class", participantClass);
	   	}
	   	
	   	if (Validations.validateIsNotNullAndPositive(participantState)) {
	   		query.setParameter("participant_state", participantState);
	   	}
	   	
	   	return query.getResultList();
   }
   
   public Holder getHolderByRntCode(Long rnt_code){
	   Holder holder=null;
	   try
	   {
	   StringBuilder sbQuery = new StringBuilder();
	   sbQuery.append(" Select h Holder h Where h.id_holder_pk = :rnt_code");
	   Query query = em.createNativeQuery(sbQuery.toString());
	   
	   query.setParameter("rnt_code", rnt_code);
	   
	   holder =  (Holder)query.getSingleResult();
	   }
	   catch(Exception e){
		   holder = null;
	   }
	   
	   return holder;
	   
   }
   
   public List<Object[]> getHistoricalRNTByParameters(Long rnt_code, Integer requester_type,Long participant,Integer state, Date date_initial, Date date_end){
	   StringBuilder sbQuery = new StringBuilder();
	   sbQuery.append(" Select"); 
	   sbQuery.append(" H.id_holder_pk as rnt_code,"); 
	   sbQuery.append(" H.full_name as full_name,");
	   sbQuery.append(" PT2.indicator1 || ' - ' || H.document_number as type_nro_document,");
	   sbQuery.append(" PT.parameter_name as state,");
	   sbQuery.append(" PT3.parameter_name as motive,");
	   sbQuery.append(" to_char(HS.update_state_date,'dd/mm/yyyy') as date_update_state,");
	   sbQuery.append(" PT4.parameter_name as requester,");
	   sbQuery.append(" HS.registry_user as user_register");
	   sbQuery.append(" From ");
	   sbQuery.append(" HOLDER_HISTORY_STATE HS ");
	   sbQuery.append(" inner join HOLDER_REQUEST HR on HS.id_holder_request_fk=HR.id_holder_request_pk ");
	   sbQuery.append(" inner join HOLDER H on HS.id_holder_fk = H.id_holder_pk");
	   sbQuery.append(" inner join PARAMETER_TABLE PT on HS.new_state = PT.parameter_table_pk");
	   sbQuery.append(" inner join PARAMETER_TABLE PT2 on H.document_type = PT2.parameter_table_pk");
	   sbQuery.append(" left join PARAMETER_TABLE PT3 on HS.motive = PT3.parameter_table_pk"); 
	   sbQuery.append(" left join PARAMETER_TABLE PT4 on HR.requester_type = PT4.parameter_table_pk");
	   
	   sbQuery.append(" Where 1=1 ");
	   
	       if(requester_type!=null){
			   sbQuery.append(" And HR.requester_type = :requesterType");
		   }
		   
		   if(participant!=null){
			   sbQuery.append(" And HR.id_participant_fk = :participant");
		   }
		   
		   if(rnt_code!=null){
			   sbQuery.append(" And HS.id_holder_fk = :rntCode");
		   }
		   
		   if(state!=null){
			   sbQuery.append(" And H.state_holder = :state");
		   }
		   
		   if(date_initial!=null && date_end!=null){
			   sbQuery.append(" And HS.update_state_date Between :dateInitial and :dateEnd");
		   }
		   
		   sbQuery.append(" Order By HS.id_hol_history_state_pk");
		  
	   
	   
	   Query query = em.createNativeQuery(sbQuery.toString());
	   
	   		   		   
	   	   if(requester_type!=null){
			   query.setParameter("requesterType",requester_type);
		   }
		   
		   if(participant!=null){
			   query.setParameter("participant", participant);
		   }
		   
		   if(rnt_code!=null){
			   query.setParameter("rntCode",rnt_code);
		   }
		   
		   if(state!=null){
			   query.setParameter("state", state);
		   }
		   
		   if(date_initial!=null && date_end!=null){
			   query.setParameter("dateInitial", date_initial);
			   query.setParameter("dateEnd",date_end);
		   }	   
		   
	     
	   return query.getResultList();
   }
   
   public List<Object[]> getHolderRelatedParticipantByParameters(Long participant,
		   Integer balance_indicator,
		   Integer state_holder,
		   Long rnt_code,
		   Integer person_type,
		   Integer state_account,
		   Integer account_type,
		   Date date_initial,
		   Date date_end){
	   
	   StringBuilder sbQuery = new StringBuilder();
	   
	   sbQuery.append(" Select"); 
	   sbQuery.append(" P.id_participant_pk as participant,"); 
	   sbQuery.append(" P.description as participant_description,");
	   sbQuery.append(" H.id_holder_pk as rnt_code,");
	   sbQuery.append(" H.holder_type as person_type,");
	   sbQuery.append(" FN_GET_HOLDER_DESC(HA.id_holder_account_pk,chr(13),0) as full_name,");
	   sbQuery.append(" H.document_type || '-' || H.document_number,");
	   sbQuery.append(" H.legal_Address as legal_address,");
	   sbQuery.append(" H.legal_residence_country as residence,");
	   sbQuery.append(" H.home_phone_number as phone_number,");
	   sbQuery.append(" H.state_holder as state,");
	   sbQuery.append(" H.economic_activity as economic_activity,");
	   sbQuery.append(" to_char(H.registry_date,'dd/mm/yyyy') as date_registry,");
	   sbQuery.append(" HA.id_holder_account_pk as cuenta_pk,");
	   sbQuery.append(" HA.account_number as nro_account,");
	   sbQuery.append(" (select (CASE WHEN sum(total_balance) > 0 THEN '1' ELSE '0' END)");
	   sbQuery.append(" from holder_account_balance");
	   sbQuery.append(" where id_holder_account_pk=HA.id_holder_account_pk) as ind_balance,");
	   sbQuery.append(" B.id_bank_pk as banco_pk,");
	   sbQuery.append(" B.mnemonic as bank_description,");
	   sbQuery.append(" HABK.bank_account_number as bank_account_nro,");
	   sbQuery.append(" HA.account_type as account_type,");      
	   sbQuery.append(" HABK.currency as currency,"); 
	   sbQuery.append(" HA.state_account as state_account");
	   sbQuery.append(" From"); 
	   sbQuery.append(" Participant P");
	   sbQuery.append(" Inner join"); 
	   sbQuery.append(" Holder H on P.id_participant_pk = H.id_participant_fk");
	   sbQuery.append(" Inner join");
	   sbQuery.append(" HOLDER_ACCOUNT HA on HA.id_participant_fk = P.id_participant_pk");
	   sbQuery.append(" Inner join");
	   sbQuery.append(" HOLDER_ACCOUNT_DETAIL HAD");
	   sbQuery.append(" on HAD.id_holder_account_fk = HA.id_holder_account_pk and"); 
	   sbQuery.append(" HAD.id_holder_fk = H.id_holder_pk");
	   sbQuery.append(" Inner join");
	   sbQuery.append(" HOLDER_ACCOUNT_BANK HABK");
	   sbQuery.append(" on HABK.id_holder_account_fk = HA.id_holder_account_pk");
	   sbQuery.append(" Inner join BANK B on HABK.id_bank_fk=B.id_bank_pk");
	   
	   sbQuery.append(" Where 1=1 ");
	   
	   if(participant!=null){
		   sbQuery.append(" And P.id_participant_pk = :idParticipantPk");
	   }
	   
	   if(state_holder!=null)
	   {
		   sbQuery.append(" And H.state_holder = :stateHolder");
	   }
   
	   if(rnt_code!=null){
		   sbQuery.append(" And H.id_holder_pk = :idHolderPk");
	   }
	   if(person_type!=null){		 
		sbQuery.append(" And H.holder_type = :holderType");
	   }
	   if(state_account!=null){
		   sbQuery.append(" And HA.state_account = :stateAccount");
	   }
	   if(account_type!=null){
		   sbQuery.append(" And HA.account_type = :accountType");
	   }
	   if(date_initial!=null && date_end!=null){
		   sbQuery.append(" And trunc(H.registry_date) Between trunc(:dateInitial) and trunc(:dateEnd)");
	   }
	   
	   
	   sbQuery.append(" Group by P.id_participant_pk,");
	   sbQuery.append(" P.description,");
	   sbQuery.append(" H.id_holder_pk, H.holder_type,"); 
	   sbQuery.append(" H.full_name,");
	   sbQuery.append(" H.document_type || '-' || H.document_number,");
	   sbQuery.append(" H.legal_Address,");
	   sbQuery.append(" H.legal_residence_country,");
	   sbQuery.append(" H.home_phone_number,");
	   sbQuery.append(" H.state_holder,");      
	   sbQuery.append(" H.economic_activity,");
	   sbQuery.append(" H.registry_date,");         
	   sbQuery.append(" HA.id_holder_account_pk,"); 
	   sbQuery.append(" HA.account_number,");
	   sbQuery.append(" B.id_bank_pk,"); 
	   sbQuery.append(" B.mnemonic,");
	   sbQuery.append(" HABK.bank_account_number,");
	   sbQuery.append(" HA.account_type,"); 
	   sbQuery.append(" HABK.currency,");
	   sbQuery.append(" HA.state_account");
	   
	if(balance_indicator!=null){
	   sbQuery.append(" Having  (select (CASE WHEN sum(total_balance) > 0 THEN '1' ELSE '0' END)");
	   sbQuery.append(" from holder_account_balance where id_holder_account_pk=HA.id_holder_account_pk) = :ind_balance");
	}
	   
	   sbQuery.append(" Order by P.id_participant_pk asc");
	   
	   Query query = em.createNativeQuery(sbQuery.toString());
	   
	   if(participant!=null){
		   query.setParameter("idParticipantPk",participant);		   
	   }
	   
	   if(state_holder!=null){
		   query.setParameter("stateHolder",state_holder);
	   }
	   if(rnt_code!=null){
		   query.setParameter("idHolderPk",rnt_code);
	   }
	   if(person_type!=null){
		   query.setParameter("holderType",person_type);
	   }
	   if(state_account!=null){
		   query.setParameter("stateAccount",state_account);
	   }
	   if(account_type!=null){
		   query.setParameter("accountType",account_type);
	   }
	   if(date_initial!=null && date_end!=null){
		   query.setParameter("dateInitial",date_initial);
		   query.setParameter("dateEnd",date_end);
	   }
	   
	   if(balance_indicator!=null){
		   query.setParameter("ind_balance",balance_indicator);
	   }
	   
	   
	   
	   return query.getResultList();
   }

   	public List<Object[]> consolidateParentAccount(ConsolidateParentAccountTO consolidateParentAccountTO){
   		StringBuilder sbQuery = new StringBuilder();
   		
   		sbQuery.append(" SELECT ");
   		sbQuery.append(" 	HAB.ID_PARTICIPANT_PK, ");
   		sbQuery.append(" 	PA.DESCRIPTION AS PART_DESC, ");
   		sbQuery.append(" 	HAB.ID_ISIN_CODE_PK, ");
   		sbQuery.append(" 	SEC.DESCRIPTION AS SEC_DESC, ");
   		sbQuery.append(" 	HO.ID_HOLDER_PK, ");
   		sbQuery.append(" 	HO.HOLDER_TYPE, ");
   		sbQuery.append(" 	HO.NAME, ");
   		sbQuery.append(" 	HO.FIRST_LAST_NAME, ");
   		sbQuery.append(" 	HO.SECOND_LAST_NAME, ");
   		sbQuery.append(" 	HO.FULL_NAME, ");
   		sbQuery.append(" 	HA.ACCOUNT_NUMBER, ");
   		sbQuery.append(" 	HAB.TOTAL_BALANCE, ");
   		sbQuery.append(" 	HAB.AVAILABLE_BALANCE, ");
   		sbQuery.append(" 	HAB.PAWN_BALANCE, ");
   		sbQuery.append(" 	HAB.BAN_BALANCE, ");
   		sbQuery.append(" 	HAB.OTHER_BLOCK_BALANCE, ");
   		sbQuery.append(" 	HAB.OPPOSITION_BALANCE, ");
   		sbQuery.append(" 	HAB.RESERVE_BALANCE, ");
   		sbQuery.append(" 	HAB.ACCREDITATION_BALANCE, ");
   		sbQuery.append(" 	HAB.MARGIN_BALANCE, ");
   		sbQuery.append(" 	HAB.REPORTING_BALANCE, ");
   		sbQuery.append(" 	HAB.REPORTED_BALANCE, ");
   		sbQuery.append(" 	HAB.BUY_BALANCE, ");
   		sbQuery.append(" 	HAB.SELL_BALANCE, ");
   		sbQuery.append(" 	HAB.TRANSIT_BALANCE ");
   		sbQuery.append(" FROM  ");
   		sbQuery.append(" 	HOLDER_ACCOUNT_BALANCE HAB ");
   		sbQuery.append(" JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK = HAB.ID_HOLDER_ACCOUNT_PK ");
   		sbQuery.append(" JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ");
   		sbQuery.append(" JOIN HOLDER HO ON HO.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
   		sbQuery.append(" JOIN SECURITY SEC ON SEC.ID_ISIN_CODE_PK= HAB.ID_ISIN_CODE_PK ");
   		sbQuery.append(" JOIN PARTICIPANT PA ON PA.ID_PARTICIPANT_PK = HAB.ID_PARTICIPANT_PK ");
   		sbQuery.append(" WHERE 1=1 ");
   		
   	
   		 if(Validations.validateIsNotNullAndPositive(consolidateParentAccountTO.getIdParticipantPk())){
   			sbQuery.append(" AND HAB.ID_PARTICIPANT_PK = :participantCode ");
		 }
   		 
   		if(Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getIdIssuerPk())){
   			sbQuery.append(" AND SEC.ID_ISSUER_FK = :issuerCode ");
		 }
   		
   		if(Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getIdIsinCodePk())){
   			sbQuery.append(" AND HAB.ID_ISIN_CODE_PK= :securityCode ");
		 }

   		sbQuery.append(" ORDER BY ");
   		sbQuery.append(" 	HAB.ID_PARTICIPANT_PK, ");
   		sbQuery.append(" 	HAB.ID_ISIN_CODE_PK, ");
   		sbQuery.append(" 	HO.ID_HOLDER_PK, ");
   		sbQuery.append(" 	HA.ACCOUNT_NUMBER ");
   		
   		Query query = em.createNativeQuery(sbQuery.toString());
   		
   		if(Validations.validateIsNotNullAndPositive(consolidateParentAccountTO.getIdParticipantPk())){
   			query.setParameter("participantCode", consolidateParentAccountTO.getIdParticipantPk());
		 }
   		 
   		if(Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getIdIssuerPk())){
   			query.setParameter("issuerCode", consolidateParentAccountTO.getIdIssuerPk());
		 }
   		
   		if(Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getIdIsinCodePk())){
   			query.setParameter("securityCode", consolidateParentAccountTO.getIdIsinCodePk());
		 }
   		
   		return query.getResultList();
   	}

   	public List<Object[]> getRntBlockUnblockRequestByParameters(Long rnt_code, Integer requester_type,Long participant,Integer state, Date date_initial, Date date_end,Integer blockRequest,Integer unBlockRequest){
 	   StringBuilder sbQuery = new StringBuilder();
 	  sbQuery.append("Select "); 
 	  sbQuery.append(" HR.id_holder_request_pk as request_number,"); 
 	  sbQuery.append(" H.id_holder_pk as rnt_code,");  
 	  sbQuery.append(" H.full_name as full_name,"); 
 	  sbQuery.append(" H.document_type || '-' || H.document_number as type_nro_document,"); 
 	  sbQuery.append(" HR.request_type as request_type,"); 
 	  sbQuery.append(" HS.motive as motive,"); 
 	  sbQuery.append(" to_char(HR.registry_date,'dd/mm/yyyy') as registry_date,"); 
 	  sbQuery.append(" HR.requester_type as requester,"); 
 	  sbQuery.append(" P.mnemonic as participant_mnemonic,"); 
 	  sbQuery.append(" HR.state_holder_request as state_request");
 	  sbQuery.append(" From");  
 	  sbQuery.append(" HOLDER_REQUEST HR");  
 	  sbQuery.append(" inner join HOLDER_HISTORY_STATE HS on HR.id_holder_request_pk=HS.id_holder_request_fk");  
 	  sbQuery.append(" inner join HOLDER H on HS.id_holder_fk = H.id_holder_pk");
 	  sbQuery.append(" left join PARTICIPANT P on HR.id_participant_fk = P.id_participant_pk");
 	   
 	   sbQuery.append(" Where (HR.request_type = :blockRequest or HR.request_type = :unBlockRequest)");
 	   
 	       if(requester_type!=null){
 			   sbQuery.append(" And HR.requester_type = :requesterType");
 		   }
 		   
 		   if(participant!=null){
 			   sbQuery.append(" And HR.id_participant_fk = :participant");
 		   }
 		   
 		   if(rnt_code!=null){
 			   sbQuery.append(" And HS.id_holder_fk = :rntCode");
 		   }
 		   
 		   if(state!=null){
 			   sbQuery.append(" And HR.state_holder_request = :state");
 		   }
 		   
 		   if(date_initial!=null && date_end!=null){
 			   sbQuery.append(" And HS.update_state_date Between :dateInitial and :dateEnd");
 		   }
 		   
 		   sbQuery.append(" Order By HR.id_holder_request_pk");
 		  
 	   
 	   
 	   Query query = em.createNativeQuery(sbQuery.toString());
 	   
 	   		   		   
 	   	   if(requester_type!=null){
 			   query.setParameter("requesterType",requester_type);
 		   }
 		   
 		   if(participant!=null){
 			   query.setParameter("participant", participant);
 		   }
 		   
 		   if(rnt_code!=null){
 			   query.setParameter("rntCode",rnt_code);
 		   }
 		   
 		   if(state!=null){
 			   query.setParameter("state", state);
 		   }
 		   
 		   if(date_initial!=null && date_end!=null){
 			   query.setParameter("dateInitial", date_initial);
 			   query.setParameter("dateEnd",date_end);
 		   }
 		   
 		   query.setParameter("blockRequest", blockRequest);
 		   query.setParameter("unBlockRequest", unBlockRequest);
 		   
 	     
 	   return query.getResultList();
    }

   	public List<Object[]> summaryParticipantAccount(ConsolidateParentAccountTO consolidateParentAccountTO){
   		StringBuilder sbQuery = new StringBuilder();
   		
   		sbQuery.append(" SELECT ");
   		sbQuery.append(" DISTINCT ");
   		sbQuery.append(" 	HAB.ID_PARTICIPANT_PK AS PARTICIPANT, ");
   		sbQuery.append(" 	PA.DESCRIPTION AS PART_NAME, ");
   		sbQuery.append(" 	HAB.ID_ISIN_CODE_PK AS SECURITY, ");
   		sbQuery.append(" 	SEC.DESCRIPTION AS SEC_DESC, ");
   		sbQuery.append(" 	(SELECT SUM(HAB2.TOTAL_BALANCE) FROM HOLDER_ACCOUNT_BALANCE HAB2 WHERE  HAB2.ID_PARTICIPANT_PK = HAB.ID_PARTICIPANT_PK AND HAB2.ID_ISIN_CODE_PK = HAB.ID_ISIN_CODE_PK) AS SUM_TOTAL_BAL, ");
   		sbQuery.append(" 	(SELECT SUM(HAB2.AVAILABLE_BALANCE) FROM HOLDER_ACCOUNT_BALANCE HAB2 WHERE HAB2.ID_PARTICIPANT_PK = HAB.ID_PARTICIPANT_PK AND HAB2.ID_ISIN_CODE_PK = HAB.ID_ISIN_CODE_PK) AS SUM_AVAILABLE_BALANCE, ");
   		sbQuery.append(" 	(SELECT SUM(HAB2.PAWN_BALANCE) FROM HOLDER_ACCOUNT_BALANCE HAB2 WHERE HAB2.ID_PARTICIPANT_PK = HAB.ID_PARTICIPANT_PK AND HAB2.ID_ISIN_CODE_PK = HAB.ID_ISIN_CODE_PK) AS SUM_PAWN_BALANCE, ");
   		sbQuery.append(" 	(SELECT SUM(HAB2.BAN_BALANCE) FROM HOLDER_ACCOUNT_BALANCE HAB2 WHERE HAB2.ID_PARTICIPANT_PK = HAB.ID_PARTICIPANT_PK AND HAB2.ID_ISIN_CODE_PK = HAB.ID_ISIN_CODE_PK) AS SUM_BAN_BALANCE, ");
   		sbQuery.append(" 	(SELECT SUM(HAB2.OTHER_BLOCK_BALANCE) FROM HOLDER_ACCOUNT_BALANCE HAB2 WHERE HAB2.ID_PARTICIPANT_PK = HAB.ID_PARTICIPANT_PK AND HAB2.ID_ISIN_CODE_PK = HAB.ID_ISIN_CODE_PK) AS SUM_OTHER_BLOCK_BALANCE, ");
   		sbQuery.append(" 	(SELECT SUM(HAB2.OPPOSITION_BALANCE) FROM HOLDER_ACCOUNT_BALANCE HAB2 WHERE HAB2.ID_PARTICIPANT_PK = HAB.ID_PARTICIPANT_PK AND HAB2.ID_ISIN_CODE_PK = HAB.ID_ISIN_CODE_PK) AS SUM_OPPOSITION_BALANCE, ");
   		sbQuery.append(" 	(SELECT SUM(HAB2.RESERVE_BALANCE) FROM HOLDER_ACCOUNT_BALANCE HAB2 WHERE HAB2.ID_PARTICIPANT_PK = HAB.ID_PARTICIPANT_PK AND HAB2.ID_ISIN_CODE_PK = HAB.ID_ISIN_CODE_PK) AS SUM_RESERVE_BALANCE, ");
   		sbQuery.append(" 	(SELECT SUM(HAB2.ACCREDITATION_BALANCE) FROM HOLDER_ACCOUNT_BALANCE HAB2 WHERE HAB2.ID_PARTICIPANT_PK = HAB.ID_PARTICIPANT_PK AND HAB2.ID_ISIN_CODE_PK = HAB.ID_ISIN_CODE_PK) AS SUM_ACCREDITATION_BALANCE, ");
   		sbQuery.append(" 	(SELECT SUM(HAB2.MARGIN_BALANCE) FROM HOLDER_ACCOUNT_BALANCE HAB2 WHERE HAB2.ID_PARTICIPANT_PK = HAB.ID_PARTICIPANT_PK AND HAB2.ID_ISIN_CODE_PK = HAB.ID_ISIN_CODE_PK) AS SUM_MARGIN_BALANCE, ");
   		sbQuery.append(" 	(SELECT SUM(HAB2.REPORTING_BALANCE) FROM HOLDER_ACCOUNT_BALANCE HAB2 WHERE HAB2.ID_PARTICIPANT_PK = HAB.ID_PARTICIPANT_PK AND HAB2.ID_ISIN_CODE_PK = HAB.ID_ISIN_CODE_PK) AS SUM_REPORTING_BALANCE, ");
   		sbQuery.append(" 	(SELECT SUM(HAB2.REPORTED_BALANCE) FROM HOLDER_ACCOUNT_BALANCE HAB2 WHERE HAB2.ID_PARTICIPANT_PK = HAB.ID_PARTICIPANT_PK AND HAB2.ID_ISIN_CODE_PK = HAB.ID_ISIN_CODE_PK) AS SUM_REPORTED_BALANCE, ");
   		sbQuery.append(" 	(SELECT SUM(HAB2.BUY_BALANCE) FROM HOLDER_ACCOUNT_BALANCE HAB2 WHERE HAB2.ID_PARTICIPANT_PK = HAB.ID_PARTICIPANT_PK AND HAB2.ID_ISIN_CODE_PK = HAB.ID_ISIN_CODE_PK) AS SUM_BUY_BALANCE, ");
   		sbQuery.append(" 	(SELECT SUM(HAB2.SELL_BALANCE) FROM HOLDER_ACCOUNT_BALANCE HAB2 WHERE HAB2.ID_PARTICIPANT_PK = HAB.ID_PARTICIPANT_PK AND HAB2.ID_ISIN_CODE_PK = HAB.ID_ISIN_CODE_PK) AS SUM_SELL_BALANCE, ");
   		sbQuery.append(" 	(SELECT SUM(HAB2.TRANSIT_BALANCE) FROM HOLDER_ACCOUNT_BALANCE HAB2 WHERE HAB2.ID_PARTICIPANT_PK = HAB.ID_PARTICIPANT_PK AND HAB2.ID_ISIN_CODE_PK = HAB.ID_ISIN_CODE_PK) AS SUM_TRANSIT_BALANCE, ");
   		sbQuery.append(" 	(SELECT   ");
   		sbQuery.append(" 			COUNT(*)  	");
   		sbQuery.append(" 	 FROM 				");
   		sbQuery.append(" 	 		HOLDER_ACCOUNT_BALANCE HAB1 ");
   		sbQuery.append(" 	 JOIN ");
   		sbQuery.append(" 		HOLDER_ACCOUNT HA1 ON HA1.ID_HOLDER_ACCOUNT_PK = HAB1.ID_HOLDER_ACCOUNT_PK ");
   		sbQuery.append(" 	WHERE ");
   		sbQuery.append(" 		HAB1.ID_PARTICIPANT_PK = HAB.ID_PARTICIPANT_PK ");
   		sbQuery.append(" 	AND ");
   		sbQuery.append(" 		HAB1.ID_ISIN_CODE_PK = HAB.ID_ISIN_CODE_PK ");
   		sbQuery.append(" 	) AS ACCS_BY_SEC ");
   		sbQuery.append(" FROM  ");
   		sbQuery.append(" 	HOLDER_ACCOUNT_BALANCE HAB ");
   		sbQuery.append(" JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK = HAB.ID_HOLDER_ACCOUNT_PK ");
   		sbQuery.append(" JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ");
   		sbQuery.append(" JOIN SECURITY SEC ON SEC.ID_ISIN_CODE_PK= HAB.ID_ISIN_CODE_PK ");
   		sbQuery.append(" JOIN PARTICIPANT PA ON PA.ID_PARTICIPANT_PK = HAB.ID_PARTICIPANT_PK ");
   		sbQuery.append(" WHERE 1=1 ");
   		
   	
   		 if(Validations.validateIsNotNullAndPositive(consolidateParentAccountTO.getIdParticipantPk())){
   			sbQuery.append(" AND HAB.ID_PARTICIPANT_PK = :participantCode ");
		 }
   		 
   		if(Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getIdIssuerPk())){
   			sbQuery.append(" AND SEC.ID_ISSUER_FK = :issuerCode ");
		 }
   		
   		if(Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getIdIsinCodePk())){
   			sbQuery.append(" AND HAB.ID_ISIN_CODE_PK= :securityCode ");
		 }

   		sbQuery.append(" ORDER BY ");
   		sbQuery.append(" 	HAB.ID_PARTICIPANT_PK, ");
   		sbQuery.append(" 	HAB.ID_ISIN_CODE_PK ");
   		
   		Query query = em.createNativeQuery(sbQuery.toString());
   		
   		if(Validations.validateIsNotNullAndPositive(consolidateParentAccountTO.getIdParticipantPk())){
   			query.setParameter("participantCode", consolidateParentAccountTO.getIdParticipantPk());
		 }
   		 
   		if(Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getIdIssuerPk())){
   			query.setParameter("issuerCode", consolidateParentAccountTO.getIdIssuerPk());
		 }
   		
   		if(Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getIdIsinCodePk())){
   			query.setParameter("securityCode", consolidateParentAccountTO.getIdIsinCodePk());
		 }
   		
   		return query.getResultList();
   	}

   	public List<Object[]> accountTransactionsHistory(ConsolidateParentAccountTO consolidateParentAccountTO){
   		StringBuilder sbQuery = new StringBuilder();
   		
   		sbQuery.append(" SELECT DISTINCT ");
   		sbQuery.append(" 	HAM.ID_PARTICIPANT_FK AS PARTICIPANT, ");
   		sbQuery.append(" 	PA.DESCRIPTION AS PARTICIPANT_NAME, ");
   		sbQuery.append("	HO.ID_HOLDER_PK AS RNT, ");
   		sbQuery.append(" 	HO.HOLDER_TYPE AS HOLDER_TYPE, ");
   		sbQuery.append(" 	(SELECT PARAMETER_NAME FROM PARAMETER_TABLE PT WHERE PT.parameter_table_pk = HO.HOLDER_TYPE ) AS HOLDER_TYPE_DESC, ");
   		sbQuery.append(" 	HO.NAME AS HOLDER_NAME, ");
   		sbQuery.append(" 	HO.FIRST_LAST_NAME AS APELLIDO_PAT, ");
   		sbQuery.append(" 	HO.SECOND_LAST_NAME AS APELLIDO_MAT, ");
   		sbQuery.append(" 	HO.FULL_NAME AS FULL_NAME, ");
   		sbQuery.append(" 	HA.ACCOUNT_NUMBER AS ACCOUNT_NUMBER, ");
   		sbQuery.append(" 	HA.ALTERNATE_CODE AS ALTERNATE_CODE, ");
   		sbQuery.append(" 	HAM.ID_ISIN_CODE_FK AS ISIN_CODE, ");
   		sbQuery.append(" 	SEC.DESCRIPTION AS VALOR_DESC, ");
   		sbQuery.append(" 	DECODE(MT.IND_BLOCK_MOVEMENT,0,DECODE(MB.ID_BEHAVIOR,1,1,2),3) AS MOV_TYPE, ");
   		sbQuery.append(" 	DECODE(MT.IND_BLOCK_MOVEMENT,0,DECODE(MB.ID_BEHAVIOR,1,'INGRESO','SALIDA'),'BLOQUEO') AS MOV_TYPE_DESC, ");
   		sbQuery.append(" 	HAM.OPERATION_DATE, ");
   		sbQuery.append(" 	HAM.OPERATION_NUMBER, ");
   		sbQuery.append("  	MT.MOVEMENT_NAME AS MOVEMENT,");
   		sbQuery.append("  	(SELECT PA1.DESCRIPTION FROM PARTICIPANT PA1 WHERE PA1.id_participant_pk = HAM.ID_SOURCE_PARTICIPANT) AS SOURCE_PART,");
   		sbQuery.append("  	(SELECT PA2.DESCRIPTION FROM PARTICIPANT PA2 WHERE PA2.id_participant_pk = HAM.ID_TARGET_PARTICIPANT) AS TARGET_PART,");
   		sbQuery.append("  	HAM.OPERATION_PRICE AS PRICE,");
   		sbQuery.append("  	HAM.MOVEMENT_QUANTITY AS QUANTITY");
   		sbQuery.append(" FROM	");
   		sbQuery.append("  	MOVEMENT_TYPE MT ");
   		sbQuery.append(" JOIN MOVEMENT_BEHAVIOR MB ON MB.ID_MOVEMENT_TYPE_FK = MT.ID_MOVEMENT_TYPE_PK	");
   		sbQuery.append(" JOIN BALANCE_TYPE BT ON BT.ID_BALANCE_TYPE_PK = MB.ID_BALANCE_TYPE_FK 	");
   		sbQuery.append(" JOIN HOLDER_ACCOUNT_MOVEMENT HAM ON HAM.ID_MOVEMENT_TYPE_FK = MT.ID_MOVEMENT_TYPE_PK ");
   		sbQuery.append(" JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK = HAM.ID_HOLDER_ACCOUNT_FK  ");
   		sbQuery.append(" JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = HAM.ID_HOLDER_ACCOUNT_FK	 ");
   		sbQuery.append(" JOIN HOLDER HO ON HO.ID_HOLDER_PK = HAD.ID_HOLDER_FK  ");
   		sbQuery.append(" JOIN PARTICIPANT PA ON PA.ID_PARTICIPANT_PK = HAM.ID_PARTICIPANT_FK	 ");
   		sbQuery.append(" JOIN SECURITY SEC ON SEC.ID_ISIN_CODE_PK = HAM.ID_ISIN_CODE_FK  ");
   		sbQuery.append(" WHERE  ");
   		sbQuery.append(" 	BT.ID_BALANCE_TYPE_PK IN (3001,3002,3004,3005,3006) ");
  
   		if(Validations.validateIsNotNullAndPositive(consolidateParentAccountTO.getIdParticipantPk())){
   			sbQuery.append(" AND HAM.ID_PARTICIPANT_FK=:idParticipantPk ");
   		}
		if(Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getIdIsinCodePk())){
			sbQuery.append(" AND HAM.ID_ISIN_CODE_FK=:idIsinCodePk ");		
	    }
		if(Validations.validateIsNotNullAndPositive(consolidateParentAccountTO.getIdHolderPk())){
			sbQuery.append(" AND HO.ID_HOLDER_PK=:idHolderPk ");		
		}
		if(Validations.validateIsNotNullAndPositive(consolidateParentAccountTO.getAccountNumber())){
			sbQuery.append(" AND HA.ACCOUNT_NUMBER=:accountNumber ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getInitialDate())){
			sbQuery.append(" AND HAM.OPERATION_DATE>=:initialDate ");	
		}
		if(Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getFinalDate())){
			sbQuery.append(" AND HAM.OPERATION_DATE<=:finalDate ");	
		}
   		
   		sbQuery.append(" ORDER BY ");
   		sbQuery.append("  	HAM.ID_PARTICIPANT_FK,	");
   		sbQuery.append(" 	HO.ID_HOLDER_PK,	 ");
   		sbQuery.append(" 	HA.ACCOUNT_NUMBER, ");
   		sbQuery.append(" 	HAM.ID_ISIN_CODE_FK, ");
   		sbQuery.append(" 	HAM.OPERATION_DATE, ");
   		sbQuery.append(" 	MOV_TYPE ");
   		
   		Query query = em.createNativeQuery(sbQuery.toString());
   		
   		if(Validations.validateIsNotNullAndPositive(consolidateParentAccountTO.getIdParticipantPk())){
   			query.setParameter("idParticipantPk", consolidateParentAccountTO.getIdParticipantPk());
   		}
		if(Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getIdIsinCodePk())){
			query.setParameter("idIsinCodePk", consolidateParentAccountTO.getIdIsinCodePk());	
	    }
		if(Validations.validateIsNotNullAndPositive(consolidateParentAccountTO.getIdHolderPk())){
			query.setParameter("idHolderPk", consolidateParentAccountTO.getIdHolderPk());		
		}
		if(Validations.validateIsNotNullAndPositive(consolidateParentAccountTO.getAccountNumber())){
			query.setParameter("accountNumber", consolidateParentAccountTO.getAccountNumber());
		}
		if(Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getInitialDate())){
			query.setParameter("initialDate", consolidateParentAccountTO.getInitialDate());	
		}
		if(Validations.validateIsNotNullAndNotEmpty(consolidateParentAccountTO.getFinalDate())){
			query.setParameter("finalDate", consolidateParentAccountTO.getFinalDate());
		}
   		
		return query.getResultList();
   	}
   	
   	public List<CrossInformationResult> getCrossInformationResultList(){
   		StringBuilder sbQuery = new StringBuilder();
   		sbQuery.append("select cir from CrossInformationResult cir left join fetch cir.pnaPersonFk pna"
   						+ " left join fetch cir.pepPersonFk pep left join fetch cir.idHolderFk "
   						+ " left join fetch cir.idLegalRepresentativeFk left join fetch cir.idParticipantFk"
   						+ " left join fetch cir.idBlockEntityFk left join fetch pna.country cou "
   						+ " left join fetch pep.geographicLocation geo left join fetch cir.ofacPersonFk");
   		Query query = em.createQuery(sbQuery.toString());
   		List<CrossInformationResult> list = (List<CrossInformationResult>) query.getResultList();
   		
   		return list;
   	}
   	
    /**
     * Gets the participant list by class.
     *
     * @param participantClass the participant class
     * @return the participant lis by class
     */
    public  List<Object[]> getParticipantDescriptionByParticipantId(Long idParticipantPk) {
     	StringBuilder sbQuery = new StringBuilder(100);
     	sbQuery.append(" select p.id_participant_pk, ").
     	append(" p.description, 	").
     	append(" p.mnemonic 		").
      	append(" from Participant p ");
     	if (idParticipantPk != null) {
     		sbQuery.append(" where p.id_participant_pk = :idPk");
     	}
     	sbQuery.append(" order by p.id_participant_pk ");
     	Query query = em.createNativeQuery(sbQuery.toString());
     	if (idParticipantPk != null) {
     		query.setParameter("idPk", idParticipantPk);
     	}
     	return query.getResultList();
     }
    
    public List<Object[]> getModificationRequestRNTByParameters(Long participant, Long rnt_code, Integer state_rnt, Date date_initial, Date date_end){
    	StringBuilder sbQuery = new StringBuilder();
    	
    	sbQuery.append(" SELECT ");
    	sbQuery.append(" P.id_participant_pk as participant,");
    	sbQuery.append(" P.mnemonic as mnemonic,");
    	sbQuery.append(" to_char(HR.registry_date,'dd/mm/yyyy') as registry_date,");
    	sbQuery.append(" H.id_holder_pk as rnt_code,");
    	sbQuery.append(" H.holder_type as holder_type,");
    	sbQuery.append(" H.state_holder as state_holder,");
    	sbQuery.append(" HR.id_holder_request_pk as holder_request_pk,");
    	sbQuery.append(" HR.state_holder_request as state_holder_request");
    	sbQuery.append(" FROM ");
    	sbQuery.append(" HOLDER_REQUEST HR");
    	sbQuery.append(" INNER JOIN");
    	sbQuery.append(" PARTICIPANT P ON HR.id_participant_fk = P.id_participant_pk");
    	sbQuery.append(" INNER JOIN");
    	sbQuery.append(" HOLDER H ON H.id_holder_pk = HR.id_holder_fk");
    	sbQuery.append(" WHERE");
    	sbQuery.append(" HR.request_type = 518");
    	
    	if(participant!=null){
    		sbQuery.append(" and P.id_participant_pk = :participant");
    	}
    	if(rnt_code!=null){
    		sbQuery.append(" and H.id_holder_pk = :rnt_code");
    	}
    	if(state_rnt!=null){
    		sbQuery.append(" and H.state_holder = :state_rnt");
    	}
    	if(date_initial!=null && date_end!=null){
    		sbQuery.append(" and HR.registry_date between :date_initial and :date_end");
    	}
    	
    	sbQuery.append(" GROUP BY");
    	sbQuery.append(" P.id_participant_pk,");
    	sbQuery.append(" P.mnemonic,");
    	sbQuery.append(" HR.registry_date,");
    	sbQuery.append(" H.id_holder_pk,");
    	sbQuery.append(" H.holder_type,");
    	sbQuery.append(" H.state_holder,");
    	sbQuery.append(" HR.id_holder_request_pk,");
    	sbQuery.append(" HR.state_holder_request");
    	sbQuery.append(" ORDER BY");
    	sbQuery.append(" P.id_participant_pk,");
    	sbQuery.append(" P.mnemonic,");
    	sbQuery.append(" HR.registry_date,");
    	sbQuery.append(" H.id_holder_pk,");
    	sbQuery.append(" H.holder_type,");
    	sbQuery.append(" H.state_holder,");
    	sbQuery.append(" HR.id_holder_request_pk,");
    	sbQuery.append(" HR.state_holder_request ASC");
    	
    	Query query = em.createNativeQuery(sbQuery.toString());
    	
    	if(participant!=null){
    		query.setParameter("participant",participant);
    	}
    	if(rnt_code!=null){
    		query.setParameter("rnt_code",rnt_code);
    	}
    	if(state_rnt!=null){
    		query.setParameter("state_rnt",state_rnt);
    	}
    	if(date_initial!=null && date_end!=null){
    		query.setParameter("date_initial",date_initial);
    		query.setParameter("date_end",date_end);
    	}
    	
    	
    	return query.getResultList();
    	
    }
    
    public List<HolderHistory> getHolderHistoryByHolderRequest(Long idHolderRequest){
    	StringBuilder sbQuery = new StringBuilder();
    	
    	sbQuery.append(" Select h");
    	sbQuery.append(" From HolderHistory h Where h.holderRequest.idHolderRequestPk =:holderRequest");
    	Query query = em.createQuery(sbQuery.toString());
    	query.setParameter("holderRequest", idHolderRequest);
    	
    	return (List<HolderHistory>)query.getResultList();
    	
    }
    
    public List<LegalRepresentativeHistory> getLegalRepresentativeHistoryByHolderRequest(Long idHolderRequest){
    	StringBuilder sbQuery = new StringBuilder();
    	
    	sbQuery.append(" Select h");
    	sbQuery.append(" From LegalRepresentativeHistory h Where h.holderRequest.idHolderRequestPk =:holderRequest");
    	Query query = em.createQuery(sbQuery.toString());
    	query.setParameter("holderRequest", idHolderRequest);
    	
    	return (List<LegalRepresentativeHistory>)query.getResultList();
    	
    }
    
    public String getParticipantDescription(Long idParticipantPk){
    	StringBuilder sbQuery = new StringBuilder();
    	
     	sbQuery.append(" select mnemonic ||' - '|| id_participant_pk from participant where id_participant_pk=:participantPk ");
    	Query query = em.createNativeQuery(sbQuery.toString());
    	query.setParameter("participantPk", idParticipantPk);
    	
    	return (String) query.getSingleResult();
    	
    }
    
    public List<Object[]> getSummaryPossessionsHolderAccount(){
		
	    StringBuilder sbQuery = new StringBuilder();
	    
	    sbQuery.append("SELECT DISTINCT   ");
	    sbQuery.append("SEC.ID_ISIN_CODE_PK, SEC.DESCRIPTION, ");
	    sbQuery.append("TO_CHAR(DECODE(SEC.CURRENT_NOMINAL_VALUE,NULL,0,SEC.CURRENT_NOMINAL_VALUE),'9999999999990.99') NOMINAL_VALUE, ");
		sbQuery.append("DECODE(COP.DELIVERY_FACTOR,NULL,0,COP.DELIVERY_FACTOR) || '%' FACTOR_INT, ");
		sbQuery.append("TO_CHAR(DECODE(COP.TAX_FACTOR,NULL,0,COP.TAX_FACTOR),'9999999999990.9') || '%' TAX, ");
		sbQuery.append("TO_CHAR(COP.CUTOFF_DATE,'dd/MM/YYYY') FECHA_CORTE, ");
		sbQuery.append("PART.ID_PARTICIPANT_PK PARTCIPANT, PART.DESCRIPTION, ");
		sbQuery.append("SUM(SCB.TOTAL_BALANCE) SALDO_STOCK_FECHA_DE_CORTE, ");
		sbQuery.append("SUM(CPR.TOTAL_BALANCE) SALDO_BRUTO_INTERES, ");
		sbQuery.append("HA.ACCOUNT_NUMBER, ");
		sbQuery.append("HO.ID_HOLDER_PK, ");
		sbQuery.append("HO.FULL_NAME ");
		sbQuery.append("FROM  "); 
		sbQuery.append("CORPORATIVE_OPERATION COP, ");
		sbQuery.append("CORPORATIVE_PROCESS_RESULT CPR, ");
		sbQuery.append("STOCK_CALCULATION_BALANCE SCB, "); 
		sbQuery.append("PARTICIPANT PART, ");
		sbQuery.append("HOLDER_ACCOUNT HA, ");
		sbQuery.append("HOLDER_ACCOUNT_DETAIL HAD, ");
		sbQuery.append("HOLDER HO, ");
		sbQuery.append("SECURITY SEC, ");
		sbQuery.append("PROGRAM_INTEREST_COUPON PRO, ");
		sbQuery.append("PARAMETER_TABLE CURR, ");
		sbQuery.append("PARAMETER_TABLE PT, ");
		sbQuery.append("ISSUER ISS ");
		sbQuery.append("WHERE  CPR.ID_CORPORATIVE_OPERATION_FK = COP.ID_CORPORATIVE_OPERATION_PK ");
		sbQuery.append("AND SCB.ID_PARTICIPANT_FK = CPR.ID_PARTICIPANT_FK ");
		sbQuery.append("AND SCB.ID_HOLDER_ACCOUNT_FK = CPR.ID_HOLDER_ACCOUNT_FK ");
		sbQuery.append("AND CPR.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append("AND CPR.ID_PARTICIPANT_FK = PART.ID_PARTICIPANT_PK ");
		sbQuery.append("AND CPR.IND_ORIGIN_TARGET = 2 ");
		sbQuery.append("AND CPR.IND_REMANENT = 0 ");
		sbQuery.append("AND SEC.ID_ISIN._CODE_PK=COP.ID_ORIGIN_ISIN_CODE_FK ");
		sbQuery.append("AND PRO.ID_PROGRAM_INTEREST_PK = COP.ID_PROGRAM_INTEREST_FK ");
		sbQuery.append("AND CURR.PARAMETER_TABLE_PK = SEC.CURRENCY ");
		sbQuery.append("AND PT.PARAMETER_TABLE_PK = COP.STATE ");
		sbQuery.append("AND ISS.ID_ISSUER_PK = COP.ID_ISSUER_FK ");
		sbQuery.append("AND HA.ID_HOLDER_ACCOUNT_PK = HAD.ID_HOLDER_ACCOUNT_FK ");
		sbQuery.append("AND HAD.ID_HOLDER_FK = HO.ID_HOLDER_PK ");
		sbQuery.append("AND SCB.ID_STOCK_CALCULATION_FK = ( ");
		sbQuery.append("SELECT MAX(SP.ID_STOCK_CALCULATION_PK) ");
		sbQuery.append("FROM STOCK_CALCULATION_PROCESS SP  ");
		sbQuery.append("WHERE SP.STOCK_TYPE = 205 ");
		sbQuery.append("AND SP.STOCK_STATE = 1327 ");
		sbQuery.append("AND SP.STOCK_CLASS = 1328 ");
		sbQuery.append("AND SP.REGISTRY_DATE = COP.REGISTRY_DATE) ");         
		sbQuery.append("GROUP BY ");
		sbQuery.append("SEC.ID_ISIN_CODE_PK, SEC.DESCRIPTION, ");
		sbQuery.append("SEC.CURRENT_NOMINAL_VALUE, PART.DESCRIPTION, ");
		sbQuery.append("PART.ID_PARTICIPANT_PK, COP.DELIVERY_FACTOR, COP.TAX_FACTOR, ");
		sbQuery.append("HA.ACCOUNT_NUMBER, COP.CUTOFF_DATE, ");
		sbQuery.append("HO.ID_HOLDER_PK, HO.FULL_NAME ");
		sbQuery.append("ORDER BY  ");
		sbQuery.append("SEC.ID_ISIN_CODE_PK, ");
		sbQuery.append("PART.ID_PARTICIPANT_PK, ");
		sbQuery.append("HA.ACCOUNT_NUMBER, ");
		sbQuery.append("HO.ID_HOLDER_PK, ");
		sbQuery.append("HO.FULL_NAME"); 
	  
	    Query query = em.createNativeQuery(sbQuery.toString());

	    return query.getResultList();
	}
    
    public List<Object[]> getAllBalanceHolderAccount(CorporativeOperationTO filter){
    	StringBuilder sbQuery = new StringBuilder();
    	sbQuery.append("   select DISTINCT P.ID_PARTICIPANT_PK || ' - '|| P.DESCRIPTION || ' - '||P.MNEMONIC PARTICIPANT,");
    	sbQuery.append("   S.ID_ISIN_CODE_PK ||'/'||s.cfi_code||' '||S.DESCRIPTION VALOR,");
    	sbQuery.append("   S.CFI_CODE,");
    	sbQuery.append("   (SELECT PARAMETER_NAME FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK=S.PERIODICITY) PERIODICITY,");
    	sbQuery.append("   fn_get_holder_desc(HAB.ID_HOLDER_ACCOUNT_PK,CHR(13),1) RNT_NAM,");
    	sbQuery.append("   HA.ACCOUNT_NUMBER ACCOUNT,");
    	sbQuery.append("   hab.available_balance,");
     	sbQuery.append("   hab.total_balance,");
    	sbQuery.append("   hab.transit_balance,");
    	sbQuery.append("   hab.pawn_balance,");
    	sbQuery.append("   hab.ban_balance,");
    	sbQuery.append("   hab.other_block_balance,");
    	sbQuery.append("   hab.reserve_balance,");
    	sbQuery.append("   hab.accreditation_balance,");
    	sbQuery.append("   hab.buy_balance,");
    	sbQuery.append("   hab.sell_balance,");
    	sbQuery.append("   hab.reporting_balance,");
    	sbQuery.append("   hab.reported_balance,");
    	sbQuery.append("   hab.margin_balance,");
    	sbQuery.append("   hab.opposition_balance,");
    	sbQuery.append("   ha.id_holder_account_pk,");
    	sbQuery.append("   p.id_participant_pk,");
    	sbQuery.append("   s.id_isin_code_pk,");
    	sbQuery.append("   ha.alternate_code,");
    	sbQuery.append("   (SELECT PARAMETER_NAME FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK=HA.ACCOUNT_TYPE) A_TYPE,");
    	sbQuery.append("   (SELECT PARAMETER_NAME FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK=HA.STATE_ACCOUNT) ASTATE,");
    	sbQuery.append("   s.CURRENT_NOMINAL_VALUE,");
    	sbQuery.append("  ( SELECT average_price from quotation  where trunc(quotation_date) = (select trunc(max(QUOTATION_DATE)) from quotation where id_isin_code_fk=s.id_isin_code_pk)),");
    	sbQuery.append("   s.interest_rate,");
    	sbQuery.append("  (select description from parameter_Table where parameter_table_pk=s.currency) curr,");
    	sbQuery.append("   s.instrument_type,");
    	sbQuery.append("   s.currency,");
    	sbQuery.append("   hab.grant_balance,");
    	sbQuery.append("   hab.granted_balance");  	

    	sbQuery.append("  FROM holder_account_balance HAB,");
    	sbQuery.append("       PARTICIPANT P,");
    	sbQuery.append("       HOLDER_ACCOUNT HA,");
    	sbQuery.append("       SECURITY S,");
    	sbQuery.append("       HOLDER_ACCOUNT_DETAIL HAD ");
    	sbQuery.append("       WHERE HAB.ID_PARTICIPANT_PK=P.ID_PARTICIPANT_PK");
    	sbQuery.append("       AND   HAB.ID_HOLDER_ACCOUNT_PK=HA.ID_HOLDER_ACCOUNT_PK");
    	sbQuery.append("       AND   HAB.ID_ISIN_CODE_PK=S.ID_ISIN_CODE_PK");
    	sbQuery.append("       AND   HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK ");
    	sbQuery.append("       AND   hab.total_balance>0");

    	if(filter.getRnt()!=null){
    		sbQuery.append("  and had.id_holder_fk=:rnt");
    	}
    	if(filter.getIdParticipantPk()!=null){
    		sbQuery.append("  and p.id_participant_pk=:participantPk");
    	}
    	
    	if(filter.getHolderAccount()!=null){
    		sbQuery.append("  and ha.id_holder_account_pk=:holderaccount");
    	}
    	
    	if(filter.getSourceIsinCode()!=null){
    	    sbQuery.append(" and s.id_isin_code_pk=:isin");
    	}
    	
    	sbQuery.append(" order by PARTICIPANT,ACCOUNT,VALOR,curr ");
    	
    	Query query = em.createNativeQuery(sbQuery.toString());
    	if(filter.getRnt()!=null){
    		query.setParameter("rnt", filter.getRnt());
     	}
    	if(filter.getIdParticipantPk()!=null){
    		query.setParameter("participantPk", filter.getIdParticipantPk());
     	}
    	if(filter.getHolderAccount()!=null){
    		query.setParameter("holderaccount", filter.getHolderAccount());
     	}
    	
    	if(filter.getSourceIsinCode()!=null){
    		query.setParameter("isin", filter.getSourceIsinCode());
     	}
    	
    	
    	return (List<Object[]>)query.getResultList();
    	
    	}
    
    public List<Object[]> getAllBalanceHolderAccountFromStock(CorporativeOperationTO filter){
    	StringBuilder sbQuery = new StringBuilder();
    	sbQuery.append("   select DISTINCT P.ID_PARTICIPANT_PK || ' - '|| P.DESCRIPTION || ' - '||P.MNEMONIC PARTICIPANT,");
    	sbQuery.append("   S.ID_ISIN_CODE_PK ||' '||S.DESCRIPTION VALOR,");
    	sbQuery.append("   S.CFI_CODE,");
    	sbQuery.append("   (SELECT PARAMETER_NAME FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK=S.PERIODICITY) PERIODICITY,");
    	sbQuery.append("   fn_get_holder_desc(HAB.ID_HOLDER_ACCOUNT_fK,CHR(13),1) RNT_NAM,");
    	sbQuery.append("   HA.ACCOUNT_NUMBER ACCOUNT,");
    	sbQuery.append("   hab.available_balance,");
     	sbQuery.append("   hab.total_balance,");
    	sbQuery.append("   hab.transit_balance,");
    	sbQuery.append("   hab.pawn_balance,");
    	sbQuery.append("   hab.ban_balance,");
    	sbQuery.append("   hab.other_block_balance,");
    	sbQuery.append("   hab.reserve_balance,");
    	sbQuery.append("   hab.accreditation_balance,");
    	sbQuery.append("   hab.buy_balance,");
    	sbQuery.append("   hab.sell_balance,");
    	sbQuery.append("   hab.reporting_balance,");
    	sbQuery.append("   hab.reported_balance,");
    	sbQuery.append("   hab.margin_balance,");
    	sbQuery.append("   hab.opposition_balance,");
    	sbQuery.append("   ha.id_holder_account_pk,");
    	sbQuery.append("   p.id_participant_pk,");
    	sbQuery.append("   s.id_isin_code_pk,");
    	sbQuery.append("   ha.alternate_code,");
    	sbQuery.append("   (SELECT PARAMETER_NAME FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK=HA.ACCOUNT_TYPE) A_TYPE,");
    	sbQuery.append("   (SELECT PARAMETER_NAME FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK=HA.STATE_ACCOUNT) ASTATE,");
    	sbQuery.append("   s.CURRENT_NOMINAL_VALUE,");
    	sbQuery.append("  ( SELECT average_price from quotation  where trunc(quotation_date) = (select trunc(max(QUOTATION_DATE)) from quotation where id_isin_code_fk=s.id_isin_code_pk)),");
    	sbQuery.append("   s.interest_rate,");
    	sbQuery.append("  (select description from parameter_Table where parameter_table_pk=s.currency) curr,");
    	sbQuery.append("  s.instrument_type,");
    	sbQuery.append("  s.currency");
    	sbQuery.append("  FROM stock_calculation_balance HAB,");
    	sbQuery.append("       PARTICIPANT P,");
    	sbQuery.append("       HOLDER_ACCOUNT HA,");
    	sbQuery.append("       SECURITY S,");
    	sbQuery.append("       HOLDER_ACCOUNT_DETAIL HAD ");
    	sbQuery.append("       WHERE HAB.ID_PARTICIPANT_fK=P.ID_PARTICIPANT_PK");
    	sbQuery.append("       AND   HAB.ID_HOLDER_ACCOUNT_fK=HA.ID_HOLDER_ACCOUNT_PK");
    	sbQuery.append("       AND   HAB.ID_ISIN_CODE_fK=S.ID_ISIN_CODE_PK");
    	sbQuery.append("       AND   HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK");
    	sbQuery.append("       AND hab.id_stock_calculation_fk=:stock");
    	sbQuery.append(" and    hab.total_balance>0");
    	if(filter.getRnt()!=null){
    		sbQuery.append("  and had.id_holder_fk=:rnt");
    	}
    	if(filter.getIdParticipantPk()!=null){
    		sbQuery.append("  and p.id_participant_pk=:participantPk");
    	}
    	
    	if(filter.getHolderAccount()!=null){
    		sbQuery.append("  and ha.id_holder_account_pk=:holderaccount");
    	}
    	
    	if(filter.getSourceIsinCode()!=null){
    		sbQuery.append(" and s.id_isin_code_pk=:isin");
    	}
    	
    	sbQuery.append(" order by PARTICIPANT,ACCOUNT,VALOR,curr ");

    	
    	Query query = em.createNativeQuery(sbQuery.toString());
    	
    	query.setParameter("stock",filter.getStockProcess());
    	if(filter.getRnt()!=null){
    		query.setParameter("rnt", filter.getRnt());
     	}
    	if(filter.getIdParticipantPk()!=null){
    		query.setParameter("participantPk", filter.getIdParticipantPk());
     	}
    	if(filter.getHolderAccount()!=null){
    		query.setParameter("holderaccount", filter.getHolderAccount());
     	}
    	
    	if(filter.getSourceIsinCode()!=null){
    		query.setParameter("isin", filter.getSourceIsinCode());
     	}
    	
    	
    	return (List<Object[]>)query.getResultList();
    	
    	}
    
    /*it only returns the query to send to the jasper as PARAMETER*/
    public String getQueryPortafolioDetailCustodian(PortafolioDetailCustodianTO pdcTO){
    	StringBuilder sbQuery= new StringBuilder();
    	// issue 1195: el NO_USE_NL es un optimizador de query no tocarlo
    	// se quita el distinct para que cuadre con el reporte de cartera desmaterializada
    	sbQuery.append(" 	select /*+ NO_USE_NL(s) */ ");
		sbQuery.append(" 	s.ID_SECURITY_CODE_PK,");//0
//		sbQuery.append(" 	NVL(mfv.EQUIVALENT_RATE,0) as MARKET_RATE,");//1
		sbQuery.append(" 	(case when mfv.reported_balance>0 or mfv.reporting_balance>0 then NVL(mfv.MARKET_RATE,0) else NVL(mfv.EQUIVALENT_RATE,0) end) as MARKET_RATE,");//1
		sbQuery.append(" 	NVL(s.initial_nominal_value,0) as INITIAL_NOMINAL_VALUE,");//2
		sbQuery.append(" 	NVL(DECODE(s.CURRENCY,127, S.INITIAL_NOMINAL_VALUE,NVL(round(s.INITIAL_NOMINAL_VALUE*(select D.buy_price FROM DAILY_EXCHANGE_RATES D ");
		sbQuery.append(" 			WHERE D.DATE_RATE=to_date(:date, 'dd/MM/yyyy') AND D.ID_CURRENCY=s.CURRENCY AND D.ID_SOURCE_INFORMATION=212),2),0)), 0) as NOMINAL_VALUE_BOL,");//3		
		sbQuery.append(" 	NVL(mfv.MARKET_PRICE, 0) as MARKET_PRICE,");//4
		sbQuery.append(" 	NVL(DECODE(s.CURRENCY,127,mfv.MARKET_PRICE,NVL(round(round(mfv.MARKET_PRICE,2)*(select D.buy_price FROM DAILY_EXCHANGE_RATES D");
		sbQuery.append(" 		WHERE D.DATE_RATE=to_date(:date, 'dd/MM/yyyy') AND D.ID_CURRENCY=S.CURRENCY AND D.ID_SOURCE_INFORMATION=212),2),0)), 0) as MARKET_PRICE_BOL,");//5
		sbQuery.append(" 	NVL(mfv.total_balance,0) as SECURITIES_AMOUNT,");//6
		sbQuery.append(" 	NVL(TRUNC(MARKET_PRICE,2)*mfv.total_balance, 0) as TOTAL_AMOUNT_ORIG,");//7
		sbQuery.append(" 		'D' as SECURITY_CODE,");//8
		sbQuery.append(" 		p.DESCRIPTION,");//9
		sbQuery.append(" 		H.ID_HOLDER_PK,");//10
		sbQuery.append(" 		H.DOCUMENT_TYPE,");//11
		sbQuery.append(" 		H.DOCUMENT_NUMBER,");//12
		sbQuery.append(" 		H.FUND_TYPE,");//13
		sbQuery.append(" 		H.ECONOMIC_ACTIVITY,");//14
		sbQuery.append(" 		H.FULL_NAME,");//15
		sbQuery.append(" 		H.TRANSFER_NUMBER,");//16
		sbQuery.append(" 	(case when mfv.reported_balance>0 or mfv.reporting_balance>0 then 'CR' else null end) IND_REPORTO");//17
		sbQuery.append("  	from MARKET_FACT_VIEW mfv     "); 
		sbQuery.append(" 	inner join security s on mfv.id_security_code_pk=s.id_security_code_pk");
		sbQuery.append(" 	inner join holder_account_detail had on mfv.id_holder_account_pk=had.id_holder_account_fk");
		sbQuery.append(" 	inner join holder h on had.id_holder_fk=h.id_holder_pk");
		sbQuery.append(" 	inner join participant P on P.ID_PARTICIPANT_PK= MFV.ID_PARTICIPANT_PK");
		sbQuery.append(" 	INNER JOIN ISSUANCE ISS ON ISS.ID_ISSUANCE_CODE_PK=S.ID_ISSUANCE_CODE_FK");
		sbQuery.append(" 	where had.ind_representative= :indRepresentative and s.id_issuer_fk not in (:issuer_code)");
		sbQuery.append(" AND ISS.IND_REGULATOR_REPORT= :indRegulatorReport  ");
//		sbQuery.append(" and trunc(mfv.market_date)<=to_date(:date, 'dd/MM/yyyy')");
//		sbQuery.append(" and trunc(mfv.cut_date)=to_date(:date, 'dd/MM/yyyy')");
		
		// issue 1195: este es un artificio para que la busqueda por fecha de corte sea mas veloz
		sbQuery.append(" AND MFV.CUT_DATE >= TRUNC (TO_DATE (:date, 'DD/MM/YYYY'), 'DD')	    ");
		sbQuery.append(" AND MFV.CUT_DATE < TRUNC (TO_DATE (:date, 'DD/MM/YYYY'), 'DD') + 1  ");
		sbQuery.append(" and TRUNC(MFV.CUT_DATE)=to_date(:date, 'dd/MM/yyyy') 				");
		
		// comentado Issu 477
		//sbQuery.append(" and (s.expiration_date is null or trunc(s.expiration_date)>to_date(:date, 'dd/MM/yyyy'))");
		// issue 1195: se comenta esta linea porque la data de esta query tambien considera historico
		//sbQuery.append(" and s.STATE_SECURITY in ( ").append(SecurityStateType.REGISTERED.getCode()).append(",").append(SecurityStateType.SUSPENDED.getCode()).append(" )");
		
		sbQuery.append(" and (mfv.TOTAL_BALANCE + mfv.reported_balance + mfv.reporting_balance) > 0");
		if(pdcTO.getEconomicActivity()!=null){
			sbQuery.append(" and h.ECONOMIC_ACTIVITY= :economicAct");
		}
		if(pdcTO.getFundType()!=null){
			sbQuery.append(" and h.FUND_TYPE= :fundType");
		}else if(Validations.validateIsNotNullAndNotEmpty(pdcTO.getLstFundTypes())){
			sbQuery.append(" and h.FUND_TYPE in (:allFundTypes)");
		}
		if(Validations.validateIsNotNullAndPositive(pdcTO.getCui())){
    		sbQuery.append(" and h.id_holder_pk=:cui_code");
    	}
		if(pdcTO.getIdSecurityCode()!=null){
    		sbQuery.append(" and s.id_security_code_pk= :securityPk");
    	}
		//sbQuery.append(" AND ROWNUM <= 1 ");
		sbQuery.append(" UNION ALL ");
		// issue 1195: se quita el distinct para que cuadre con el reporte de cartera fisica
		sbQuery.append(" select 																																	");
		sbQuery.append(" s.ID_SECURITY_CODE_PK,																																");//0
		sbQuery.append(" NVL(PCM.MARKET_RATE,0) as MARKET_RATE,																												");//1
		sbQuery.append(" NVL(s.initial_nominal_value,0) as INITIAL_NOMINAL_VALUE,																							");//2
		sbQuery.append(" NVL(DECODE(s.CURRENCY,127, S.INITIAL_NOMINAL_VALUE,NVL(round(s.INITIAL_NOMINAL_VALUE*(select D.buy_price FROM DAILY_EXCHANGE_RATES D 				");
		sbQuery.append(" 	WHERE D.DATE_RATE=to_date(:date, 'dd/MM/yyyy') AND D.ID_CURRENCY=s.CURRENCY AND D.ID_SOURCE_INFORMATION=212),2),0)), 0) as NOMINAL_VALUE_BOL,	");//3
		sbQuery.append(" NVL(PCM.market_price,0) as MARKET_PRICE,																											");//4
		sbQuery.append(" NVL(DECODE(s.CURRENCY,127,PCM.MARKET_PRICE,NVL(round(PCM.MARKET_PRICE*(select D.buy_price FROM DAILY_EXCHANGE_RATES D 								");
		sbQuery.append(" 	WHERE D.DATE_RATE=to_date(:date, 'dd/MM/yyyy') AND D.ID_CURRENCY=S.CURRENCY AND D.ID_SOURCE_INFORMATION=212),2),0)), 0) as MARKET_PRICE_BOL,	");//5
		sbQuery.append(" NVL(PCM.QUANTITY,0) AS SECURITIES_AMOUNT,																											");//6
		sbQuery.append(" NVL(TRUNC(MARKET_PRICE,2)*PCM.QUANTITY, 0) as TOTAL_AMOUNT_ORIG,																					");//7(NO VA)
		sbQuery.append(" 'F' as SECURITY_CODE,																																");//8
		sbQuery.append(" p.DESCRIPTION,																																		");//9
		sbQuery.append(" H.ID_HOLDER_PK,																																	");//10
		sbQuery.append(" H.DOCUMENT_TYPE,																																	");//11
		sbQuery.append(" H.DOCUMENT_NUMBER,																																	");//12
		sbQuery.append(" H.FUND_TYPE,																																		");//13
		sbQuery.append(" H.ECONOMIC_ACTIVITY,																																");//14
		sbQuery.append(" H.FULL_NAME,																																		");//15
		sbQuery.append(" H.TRANSFER_NUMBER,																																	");//16
		sbQuery.append(" '' as IND_REPORTO																																	");//17
		
		// issue 1195: este from se quita porque tiene q igual con la query de cartera fisica
//		sbQuery.append(" from PHYSICAL_CERT_MARKETFACT PCM																													");
//		sbQuery.append(" Inner join PHYSICAL_CERTIFICATE PC On PC.ID_PHYSICAL_CERTIFICATE_PK=PCM.ID_PHYSICAL_CERTIFICATE_FK													");
//		sbQuery.append(" Inner join security s On PC.id_security_code_fk=S.id_security_code_pk																				");
//		sbQuery.append(" Inner join PHYSICAL_BALANCE_DETAIL PBD On PBD.ID_PHYSICAL_CERTIFICATE_FK=PC.ID_PHYSICAL_CERTIFICATE_PK												");
//		sbQuery.append(" inner join holder_account_detail had on PBD.id_holder_account_Fk=had.id_holder_account_fk															");
//		sbQuery.append(" inner join holder_account ha on had.id_holder_account_fk= ha.id_holder_account_pk																	");
//		sbQuery.append(" inner join holder h on had.id_holder_fk=h.id_holder_pk																								");
//		sbQuery.append(" inner join participant p on PBD.id_participant_fk= p.id_participant_pk																				");
//		sbQuery.append(" INNER JOIN ISSUANCE ISS ON ISS.ID_ISSUANCE_CODE_PK=S.ID_ISSUANCE_CODE_FK																			");
		
		// issue 1195: este from es el que cuadra con el reporte de cartera fisica
		sbQuery.append(" FROM   balance_psy_view PSV 																					");
		sbQuery.append(" inner join physical_cert_marketfact PCM ON PCM.id_physical_certificate_fk = PSV.id_physical_certificate_pk     ");
		sbQuery.append(" inner join security s ON PSV.id_security_code_pk = S.id_security_code_pk                                       ");
		sbQuery.append(" inner join holder_account ha ON PSV.id_holder_account_pk = ha.id_holder_account_pk                             ");
		sbQuery.append(" inner join holder_account_detail had ON ha.id_holder_account_pk  = had.id_holder_account_fk                    ");
		sbQuery.append(" inner join holder h ON had.id_holder_fk = h.id_holder_pk                                                       ");
		sbQuery.append(" inner join participant p ON PSV.id_participant_pk = p.id_participant_pk                                        ");
		sbQuery.append(" inner join issuance ISS ON ISS.id_issuance_code_pk = S.id_issuance_code_fk                                     ");
		
		sbQuery.append(" where had.ind_representative= :indRepresentative and PSV.id_security_code_pk IS NOT NULL and s.id_issuer_fk not in (:issuer_code)					");
//		sbQuery.append(" AND trunc(PCM.market_date)<=to_date(:date, 'dd/MM/yyyy') 									");
		
		// comentando por el Issue: 477
		//sbQuery.append(" AND trunc(PBD.DEPOSIT_DATE)<=to_date(:date, 'dd/MM/yyyy')									");
		// agregando criterio por Issue: 477
		sbQuery.append(" and s.STATE_SECURITY in ( ").append(SecurityStateType.REGISTERED.getCode()).append(",").append(SecurityStateType.SUSPENDED.getCode()).append(" )");
				
		sbQuery.append(" AND ISS.IND_REGULATOR_REPORT= :indRegulatorReport 																									");
		
		// issue 1195: estos filtros son los mismos que tiene el reporte de cartera fisica
		sbQuery.append(" AND PSV.total_balance > 0 ");
		sbQuery.append(" AND PCM.ind_active = 1  ");
		
		// issue 1195: es un artificio para que la busqueda por fecha de corte sea mas veloz
		sbQuery.append(" AND PSV.CUT_DATE                                    >= TRUNC(TO_DATE(:date, 'DD/MM/YYYY'), 'DD')	");
		sbQuery.append(" AND PSV.CUT_DATE                                     < TRUNC(TO_DATE(:date, 'DD/MM/YYYY'), 'DD') + 1 ");
		sbQuery.append(" AND TRUNC(TO_DATE(:date, 'DD/MM/YYYY'), 'DD') = TO_DATE(:date, 'DD/MM/YYYY') ");
		
		// issue 1195: se omite este filtro debido a que pernececia al otro 'from incorrecto'
		//sbQuery.append(" AND PBD.STATE=:state 																																");
		if(pdcTO.getEconomicActivity()!=null){
			sbQuery.append(" and h.ECONOMIC_ACTIVITY= :economicAct");
		}
		if(pdcTO.getFundType()!=null){
			sbQuery.append(" and h.FUND_TYPE= :fundType");
		}else if(Validations.validateIsNotNullAndNotEmpty(pdcTO.getLstFundTypes())){
			sbQuery.append(" and h.FUND_TYPE in (:allFundTypes)");
		}
		if(Validations.validateIsNotNullAndPositive(pdcTO.getCui())){
    		sbQuery.append(" and h.id_holder_pk=:cui_code");
    	}
		if(pdcTO.getIdSecurityCode()!=null){
    		sbQuery.append(" and s.id_security_code_pk= :securityPk");
    	}
		/*Agregar temporalmente*/
		//sbQuery.append(" AND ROWNUM <= 1 ");
		sbQuery.append(" order by SECURITY_CODE,ID_SECURITY_CODE_PK, ID_HOLDER_PK");
		
    	return sbQuery.toString();
    }
     
    public String getQueryFormatForJasper(PortafolioDetailCustodianTO pdcTO, String sbQuery){
    	String strQueryFormatted = sbQuery.toString();
    	strQueryFormatted= strQueryFormatted.replace(":indRepresentative", BooleanType.YES.getCode().toString());
    	strQueryFormatted= strQueryFormatted.replace(":indRegulatorReport", BooleanType.YES.getCode().toString());
    	if(Validations.validateIsNotNullAndNotEmpty(pdcTO.getFundType())){
    		strQueryFormatted= strQueryFormatted.replace(":fundType", "'"+pdcTO.getFundType()+"'");
    	}else if(Validations.validateIsNotNullAndNotEmpty(pdcTO.getLstFundTypes())){
    		strQueryFormatted= strQueryFormatted.replace(":allFundTypes", pdcTO.getLstFundTypes());
    	}
    	strQueryFormatted= strQueryFormatted.replace(":date", "'"+CommonsUtilities.convertDateToString(pdcTO.getDateMarketFact(),"dd/MM/yyyy")+"'");
    	strQueryFormatted=strQueryFormatted.replace(":state", StateType.DEPOSITED.getCode().toString());
    	strQueryFormatted=strQueryFormatted.replace(":issuer_code", "'"+pdcTO.getIdIssuer()+"'");
    	if(Validations.validateIsNotNullAndPositive(pdcTO.getCui())){
    		strQueryFormatted=strQueryFormatted.replace(":cui_code", pdcTO.getCui().toString());
    	}
    	if(pdcTO.getIdSecurityCode()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":securityPk", "'"+pdcTO.getIdSecurityCode()+"'");
    	}
    	if(pdcTO.getEconomicActivity()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":economicAct", pdcTO.getEconomicActivity().toString());
    	}
    	
		return strQueryFormatted;
    }
    
    public List<PortafolioDetailCustodianTO> getPortafolioDetailCustodianTxt(PortafolioDetailCustodianTO pdcTO, String sbQuery){
		Query query = em.createNativeQuery(sbQuery.toString());
		PortafolioDetailCustodianTO pdc= null;
		List<PortafolioDetailCustodianTO> lstPDC = new ArrayList<PortafolioDetailCustodianTO>();

		query.setParameter("indRepresentative", BooleanType.YES.getCode());
		query.setParameter("indRegulatorReport", BooleanType.YES.getCode());
		if(Validations.validateIsNotNullAndNotEmpty(pdcTO.getFundType())){
			query.setParameter("fundType", pdcTO.getFundType());
    	}else if(Validations.validateIsNotNullAndNotEmpty(pdcTO.getListFundTypes())){
    		query.setParameter("allFundTypes", pdcTO.getListFundTypes());
    	}
		query.setParameter("date", CommonsUtilities.convertDatetoString(pdcTO.getDateMarketFact()));
		//query.setParameter("state", StateType.DEPOSITED.getCode().toString());
		query.setParameter("issuer_code",pdcTO.getIdIssuer());
		if(Validations.validateIsNotNullAndPositive(pdcTO.getCui())){
    		query.setParameter("cui_code",pdcTO.getCui());
    	}
		if(pdcTO.getEconomicActivity()!=null){
			query.setParameter("economicAct", pdcTO.getEconomicActivity());
    	}
    	if(pdcTO.getIdSecurityCode()!=null){
    		query.setParameter("securityPk",pdcTO.getIdSecurityCode());
    	}
		
		for(Object[] obj : (List<Object[]>)query.getResultList()){
			pdc= new PortafolioDetailCustodianTO();
			if(obj[0].toString().length()>4){//only securities with correct format 
				pdc.setInstrumentSecClass(obj[0].toString().substring(0, 3));
				pdc.setIdSecurityCode(obj[0].toString().substring(4, obj[0].toString().length()));
			}
			pdc.setMarketRate((BigDecimal)obj[1]);
			pdc.setInitialNominalValue((BigDecimal)obj[2]);
			pdc.setNominalValueOrig((BigDecimal)obj[3]);
			pdc.setMarketPrice((BigDecimal)obj[4]);
			pdc.setMarkPriceOrig((BigDecimal)obj[5]);
			pdc.setTotalBalance((BigDecimal)obj[6]);
			pdc.setTotalAmountOrig((BigDecimal)obj[7]);
			pdc.setSecCodeOnCustody(obj[8].toString());
			//for txt name
			if(obj[13]!=null){
				pdc.setFundType(obj[13].toString());
			}
			if(obj[14]!=null){
				pdc.setEconomicActivity(new BigDecimal(obj[14].toString()).intValue());
			}
			if(obj[15]!=null){
				pdc.setHolderDesc(obj[15].toString());
			}
			if(obj[16]!=null){
				pdc.setTransferNumber(new BigDecimal(obj[16].toString()).intValue());
				if(obj[16].toString().contains(GeneralConstants.ZERO_VALUE_INT+""))
					pdc.setTransferNumberString(obj[16].toString());
				else
					pdc.setTransferNumberString(GeneralConstants.ZERO_VALUE_INT+obj[16].toString());
			}
			if(obj[17]!=null){
				pdc.setIndReporto(obj[17].toString());
			}else{
				pdc.setIndReporto(GeneralConstants.EMPTY_STRING);
			}
			
			lstPDC.add(pdc);
		}
    	return lstPDC;
    }
    
    public Date getDateLastCodeGeneration(){
    	StringBuilder sbQuery = new StringBuilder();
    	
    	sbQuery.append(" select MAX(LAST_MODIFY_DATE) ");
    	sbQuery.append(" from security ");
    	Query query = em.createNativeQuery(sbQuery.toString());
    	return (Date)query.getSingleResult();
    }
    
    public String getQueryHoldingsByAccountAndHolderReport(GenericsFiltersTO gfto, Long idStkCalcProcess){
        StringBuilder sbQuery = new StringBuilder();
        
        sbQuery.append(" SELECT ");
        sbQuery.append("     PARTICIPANT, ");
        sbQuery.append("     ID_PARTICIPANT_FK, ");
        sbQuery.append("     ID_SECURITY_CODE_PK, ");
        sbQuery.append("     SECURITY_CLASS, ");
        sbQuery.append("     ID_HOLDER_ACCOUNT_PK, ");
        sbQuery.append("     ACCOUNT_NUMBER, ");
        sbQuery.append("     TOTAL_BALANCE, ");
        sbQuery.append("     AVAILABLE_BALANCE, ");
        sbQuery.append("     PAWN_BALANCE, ");
        sbQuery.append("     BAN_BALANCE, ");
        sbQuery.append("     ACCREDITATION_BALANCE, ");
        sbQuery.append("     REPORTED_BALANCE, ");
        sbQuery.append("     REPORTING_BALANCE, ");
        sbQuery.append("     CURRENT_NOMINAL_VALUE, ");
        sbQuery.append("     CURRENCY, ");
        sbQuery.append("     MARKET_PRICE, ");
        sbQuery.append("     TOTAL_BALANCE * ROUND(MARKET_PRICE,2) * ");
        sbQuery.append("     CASE CURRENCY ");
        sbQuery.append("     WHEN 127 THEN 1 ");
        sbQuery.append("     WHEN 430 THEN 1 * BUY_PRICE_DOL ");
        sbQuery.append("     END MARKET_PRICE_BOB, ");
        sbQuery.append("     TOTAL_BALANCE * ROUND(MARKET_PRICE,2) * ");
        sbQuery.append("     CASE CURRENCY ");
        sbQuery.append("     WHEN 127 THEN 1 / BUY_PRICE_DOL ");
        sbQuery.append("     WHEN 430 THEN 1 ");
        sbQuery.append("     END MARKET_PRICE_USD, ");
        sbQuery.append("     TOTAL_BALANCE * CURRENT_NOMINAL_VALUE  * ");
        sbQuery.append("     CASE CURRENCY ");
        sbQuery.append("     WHEN 127 THEN 1 ");
        sbQuery.append("     WHEN 430 THEN 1 * BUY_PRICE_DOL ");
        sbQuery.append("     END NOMINAL_VALUE_BOB, ");
        sbQuery.append("     TOTAL_BALANCE * CURRENT_NOMINAL_VALUE *  ");
        sbQuery.append("     CASE CURRENCY ");
        sbQuery.append("     WHEN 127 THEN 1 / BUY_PRICE_DOL ");
        sbQuery.append("     WHEN 430 THEN 1 ");
        sbQuery.append("     END NOMINAL_VALUE_USD, ");
        sbQuery.append("     BUY_PRICE_DOL, ");
        sbQuery.append("     FN_GET_HOLDER_NAME(ID_HOLDER_ACCOUNT_PK) AS CUI_DESCRIPTION,  ");
        sbQuery.append("     CORTE  ");
        sbQuery.append(" FROM( ");
        sbQuery.append("     SELECT ");
        sbQuery.append("     (P.ID_PARTICIPANT_PK || ' - ' || P.MNEMONIC || ' - ' || P.DESCRIPTION) PARTICIPANT, ");
        sbQuery.append("     HA.ID_PARTICIPANT_FK, ");
        sbQuery.append("     S.ID_SECURITY_CODE_PK, ");
        sbQuery.append("     S.SECURITY_CLASS, ");
        sbQuery.append("     HA.ID_HOLDER_ACCOUNT_PK, ");
        sbQuery.append("     HA.ACCOUNT_NUMBER, ");
        sbQuery.append("     HAB.TOTAL_BALANCE, ");
        sbQuery.append("     HAB.AVAILABLE_BALANCE, ");
        sbQuery.append("     HAB.PAWN_BALANCE, ");
        sbQuery.append("     HAB.BAN_BALANCE, ");
        sbQuery.append("     HAB.ACCREDITATION_BALANCE, ");
        sbQuery.append("     HAB.REPORTED_BALANCE, ");
        sbQuery.append("     HAB.REPORTING_BALANCE, ");
        if(CommonsUtilities.convertStringtoDate(gfto.getInitialDt()).before(CommonsUtilities.currentDate())){
        	sbQuery.append("	 FN_NOMINAL_PORTFOLIO(S.id_security_code_pk ,HAB.CUT_DATE) AS CURRENT_NOMINAL_VALUE, ");
        }else{
        	sbQuery.append("     S.CURRENT_NOMINAL_VALUE, ");
        }
        sbQuery.append("     S.CURRENCY, ");
        sbQuery.append("     NVL(HAB.MARKET_PRICE,0) MARKET_PRICE, ");
        sbQuery.append("     NVL((SELECT DER.BUY_PRICE FROM DAILY_EXCHANGE_RATES DER  ");
        sbQuery.append("     WHERE DER.ID_CURRENCY = 430 ");
        sbQuery.append("     AND trunc(DER.DATE_RATE) = TO_DATE('"+ gfto.getInitialDt() +"','DD/MM/YYYY')  ");
        sbQuery.append("     AND DER.ID_SOURCE_INFORMATION=212),1   ");
        sbQuery.append("     ) BUY_PRICE_DOL, ");
        sbQuery.append("     'COMPRAS DEFINITIVAS' AS CORTE ");
        sbQuery.append("     FROM MARKET_FACT_VIEW HAB ");
        sbQuery.append("     INNER JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK = HAB.ID_HOLDER_ACCOUNT_PK ");
        sbQuery.append("     INNER JOIN PARTICIPANT P ON P.ID_PARTICIPANT_PK  = HAB.ID_PARTICIPANT_PK ");
        sbQuery.append("     INNER JOIN SECURITY S ON S.ID_SECURITY_CODE_PK=HAB.ID_SECURITY_CODE_PK  ");
        sbQuery.append("     WHERE 1=1 ");
        sbQuery.append("     AND trunc(HAB.CUT_DATE) = TO_DATE('"+ gfto.getInitialDt() +"','DD/MM/YYYY') ");
        sbQuery.append("     AND S.CURRENCY IN (127,430)   ");
        sbQuery.append("     AND HAB.REPORTING_BALANCE = 0   ");
        sbQuery.append("     AND HAB.TOTAL_BALANCE > 0   ");
		if(Validations.validateIsNotNull(gfto.getParticipant())) {
			sbQuery.append(" AND HA.ID_PARTICIPANT_FK="+gfto.getParticipant()+" ");
		}
		if(Validations.validateIsNotNull(gfto.getCui())) {
			sbQuery.append(" AND (SELECT COUNT(*) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK AND HAD.ID_HOLDER_FK="+ gfto.getCui() + ")>0 ");
		}
		if(Validations.validateIsNotNull(gfto.getIdHolderAccountPk())) {
			sbQuery.append(" AND HA.ID_HOLDER_ACCOUNT_PK="+gfto.getIdHolderAccountPk()+" ");
		}
		if(Validations.validateIsNotNull(gfto.getIdSecurityCodePk())) {
			sbQuery.append(" AND S.ID_SECURITY_CODE_PK='"+gfto.getIdSecurityCodePk().toString()+"' ");
		}
		if(Validations.validateIsNotNull(gfto.getSecurityClass())){
    		sbQuery.append(" and S.security_class = "+gfto.getSecurityClass()+" ");
    	}
		if(Validations.validateIsNotNull(gfto.getIdIssuer())){
    		sbQuery.append(" and S.id_issuer_fk='"+gfto.getIdIssuer()+"' ");
    	}
		if(Validations.validateIsNotNull(gfto.getCurrency())){
    		sbQuery.append(" and S.currency="+gfto.getCurrency()+" ");
    	}
		
		sbQuery.append("     UNION   ");
		
		sbQuery.append("     SELECT ");
        sbQuery.append("     (P.ID_PARTICIPANT_PK || ' - ' || P.MNEMONIC || ' - ' || P.DESCRIPTION) PARTICIPANT, ");
        sbQuery.append("     HA.ID_PARTICIPANT_FK, ");
        sbQuery.append("     S.ID_SECURITY_CODE_PK, ");
        sbQuery.append("     S.SECURITY_CLASS, ");
        sbQuery.append("     HA.ID_HOLDER_ACCOUNT_PK, ");
        sbQuery.append("     HA.ACCOUNT_NUMBER, ");
        sbQuery.append("     HAB.TOTAL_BALANCE, ");
        sbQuery.append("     HAB.AVAILABLE_BALANCE, ");
        sbQuery.append("     HAB.PAWN_BALANCE, ");
        sbQuery.append("     HAB.BAN_BALANCE, ");
        sbQuery.append("     HAB.ACCREDITATION_BALANCE, ");
        sbQuery.append("     HAB.REPORTED_BALANCE, ");
        sbQuery.append("     HAB.REPORTING_BALANCE, ");
        if(CommonsUtilities.convertStringtoDate(gfto.getInitialDt()).before(CommonsUtilities.currentDate())){
        	sbQuery.append("	 FN_NOMINAL_PORTFOLIO(S.id_security_code_pk ,HAB.CUT_DATE) AS CURRENT_NOMINAL_VALUE, ");
        }else{
        	sbQuery.append("     S.CURRENT_NOMINAL_VALUE, ");
        }
        sbQuery.append("     S.CURRENCY, ");
        sbQuery.append("     NVL(HAB.MARKET_PRICE,0) MARKET_PRICE, ");
        sbQuery.append("     NVL((SELECT DER.BUY_PRICE FROM DAILY_EXCHANGE_RATES DER  ");
        sbQuery.append("     WHERE DER.ID_CURRENCY = 430 ");
        sbQuery.append("     AND trunc(DER.DATE_RATE) = TO_DATE('"+ gfto.getInitialDt() +"','DD/MM/YYYY')  ");
        sbQuery.append("     AND DER.ID_SOURCE_INFORMATION=212),1   ");
        sbQuery.append("     ) BUY_PRICE_DOL, ");
        sbQuery.append("     'REPORTOS ADQUIRIDOS' AS CORTE ");
        sbQuery.append("     FROM MARKET_FACT_VIEW HAB ");
        sbQuery.append("     INNER JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK = HAB.ID_HOLDER_ACCOUNT_PK ");
        sbQuery.append("     INNER JOIN PARTICIPANT P ON P.ID_PARTICIPANT_PK  = HAB.ID_PARTICIPANT_PK ");
        sbQuery.append("     INNER JOIN SECURITY S ON S.ID_SECURITY_CODE_PK=HAB.ID_SECURITY_CODE_PK  ");
        sbQuery.append("     WHERE 1=1 ");
        sbQuery.append("     AND trunc(HAB.CUT_DATE) = TO_DATE('"+ gfto.getInitialDt() +"','DD/MM/YYYY') ");
        sbQuery.append("     AND S.CURRENCY IN (127,430)   ");
        sbQuery.append("     AND HAB.REPORTING_BALANCE > 0   ");
        sbQuery.append("     AND HAB.TOTAL_BALANCE > 0   ");
        if(Validations.validateIsNotNull(gfto.getParticipant())) {
			sbQuery.append(" AND HA.ID_PARTICIPANT_FK="+gfto.getParticipant()+" ");
		}
		if(Validations.validateIsNotNull(gfto.getCui())) {
			sbQuery.append(" AND (SELECT COUNT(*) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK AND HAD.ID_HOLDER_FK="+ gfto.getCui() + ")>0 ");
		}
		if(Validations.validateIsNotNull(gfto.getIdHolderAccountPk())) {
			sbQuery.append(" AND HA.ID_HOLDER_ACCOUNT_PK="+gfto.getIdHolderAccountPk()+" ");
		}
		if(Validations.validateIsNotNull(gfto.getIdSecurityCodePk())) {
			sbQuery.append(" AND S.ID_SECURITY_CODE_PK='"+gfto.getIdSecurityCodePk().toString()+"' ");
		}
		if(Validations.validateIsNotNull(gfto.getSecurityClass())){
    		sbQuery.append(" and S.security_class = "+gfto.getSecurityClass()+" ");
    	}
		if(Validations.validateIsNotNull(gfto.getIdIssuer())){
    		sbQuery.append(" and S.id_issuer_fk='"+gfto.getIdIssuer()+"' ");
    	}
		if(Validations.validateIsNotNull(gfto.getCurrency())){
    		sbQuery.append(" and S.currency="+gfto.getCurrency()+" ");
    	}
		
		sbQuery.append(") T ");
		sbQuery.append("ORDER BY CORTE,ID_PARTICIPANT_FK,SECURITY_CLASS,CURRENCY,ACCOUNT_NUMBER,ID_SECURITY_CODE_PK");
        
		return sbQuery.toString();
    }
    
    public String getSubTotalQueryHoldingsByAccount(GenericsFiltersTO gfto){
    	StringBuilder sbQuery= new StringBuilder();

		sbQuery.append(" SELECT 																															");
		sbQuery.append(" 	(SELECT PARAMETER_NAME FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = S.CURRENCY) CURRENCY,									");   
		sbQuery.append(" 	NVL(SUM(MFV.TOTAL_BALANCE),0) TOTAL_VALORES,																					");   
		sbQuery.append(" 	NVL(SUM(																														");
		sbQuery.append(" 		ROUND(NVL(MFV.market_price,0),2) * MFV.total_balance *																		");
		sbQuery.append(" 		CASE																														");
		sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1																							");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304,430) THEN 1 *																				");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+gfto.getInitialDt()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1)										");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1853) THEN 1 *																						");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+gfto.getInitialDt()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)												");
		sbQuery.append(" 			ELSE 1																													");
		sbQuery.append(" 		END),0) TOTAL_PRECIO_MRKT_BOB,																								");
		sbQuery.append(" 	NVL(SUM(																														");
		sbQuery.append(" 		ROUND(NVL(MFV.market_price,0),2) * MFV.total_balance *																		");
		sbQuery.append(" 		CASE																														");
		sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1 /																							");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+gfto.getInitialDt()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)												");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304) THEN 1 *																					");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+gfto.getInitialDt()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /										");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+gfto.getInitialDt()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 					AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)													");
		sbQuery.append(" 			WHEN S.CURRENCY IN (430,1853) THEN 1																					");
		sbQuery.append(" 			ELSE 1																													");
		sbQuery.append(" 		END),0) TOTAL_PRECIO_MRKT_USD,																								");
		
		sbQuery.append(" 	NVL(SUM(																														");    
		if(CommonsUtilities.convertStringtoDate(gfto.getInitialDt()).before(CommonsUtilities.currentDate())){
        	sbQuery.append("	 FN_NOMINAL_PORTFOLIO(S.id_security_code_pk ,MFV.CUT_DATE) ");
        }else{
        	sbQuery.append("     S.CURRENT_NOMINAL_VALUE ");
        }
		sbQuery.append("		* MFV.TOTAL_BALANCE *																									");      
		sbQuery.append(" 		CASE																														");     
		sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1																							");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304,430) THEN 1 *																				"); 
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+gfto.getInitialDt()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1)										");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1853) THEN 1 *																						"); 
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+gfto.getInitialDt()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)												");
		sbQuery.append(" 			ELSE 1																													");   
		sbQuery.append(" 	END),0) TOTAL_CURRENT_NOMINAL_BOB,																								");    
		sbQuery.append(" 	NVL(SUM(																														");    
		if(CommonsUtilities.convertStringtoDate(gfto.getInitialDt()).before(CommonsUtilities.currentDate())){
        	sbQuery.append("	 FN_NOMINAL_PORTFOLIO(S.id_security_code_pk ,MFV.CUT_DATE) ");
        }else{
        	sbQuery.append("     S.CURRENT_NOMINAL_VALUE ");
        }
		sbQuery.append("		* MFV.TOTAL_BALANCE *																									");
		sbQuery.append(" 		CASE																														");
		sbQuery.append(" 			WHEN S.CURRENCY = 127 THEN 1 /																							");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+gfto.getInitialDt()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)												");
		sbQuery.append(" 			WHEN S.CURRENCY IN (1734,1304) THEN 1 *																					");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+gfto.getInitialDt()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 						AND D.ID_CURRENCY = S.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /										");
		sbQuery.append(" 				nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D																");
		sbQuery.append(" 					WHERE TRUNC(D.DATE_RATE) = TO_DATE('"+gfto.getInitialDt()+"','DD/MM/YYYY') 			");
		sbQuery.append(" 						AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)												");
		sbQuery.append(" 			WHEN S.CURRENCY IN (430,1853) THEN 1																					");
		sbQuery.append(" 			ELSE 1																													");
		sbQuery.append(" 	END),0) TOTAL_CURRENT_NOMINAL_USD																								");
		
		sbQuery.append(" FROM MARKET_FACT_VIEW MFV																											");
		sbQuery.append(" INNER JOIN PARTICIPANT P                    ON P.ID_PARTICIPANT_PK = MFV.ID_PARTICIPANT_PK											");
		sbQuery.append(" INNER JOIN SECURITY S                       ON S.ID_SECURITY_CODE_PK = MFV.ID_SECURITY_CODE_PK										");							
		sbQuery.append(" INNER JOIN HOLDER_ACCOUNT HA                ON HA.ID_HOLDER_ACCOUNT_PK = MFV.ID_HOLDER_ACCOUNT_PK									");
		sbQuery.append(" INNER JOIN ISSUER ISU                       ON ISU.ID_ISSUER_PK = S.ID_ISSUER_FK													");
		sbQuery.append(" 	WHERE 1 = 1																							");
		sbQuery.append("	 AND TRUNC(MFV.CUT_DATE) = TO_DATE('"+gfto.getInitialDt()+"','DD/MM/YYYY') 								");
		sbQuery.append("     AND S.CURRENCY IN (127,430)   ");
        sbQuery.append("     AND MFV.REPORTING_BALANCE >= 0   ");
        sbQuery.append("     AND MFV.TOTAL_BALANCE > 0   ");
		
		if(Validations.validateIsNotNull(gfto.getParticipant())) {
			sbQuery.append(" AND HA.ID_PARTICIPANT_FK="+gfto.getParticipant()+" ");
		}
		if(Validations.validateIsNotNull(gfto.getCui())) {
			sbQuery.append(" AND (SELECT COUNT(*) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK AND HAD.ID_HOLDER_FK="+ gfto.getCui() + ")>0 ");
		}
		if(Validations.validateIsNotNull(gfto.getIdHolderAccountPk())) {
			sbQuery.append(" AND HA.ID_HOLDER_ACCOUNT_PK="+gfto.getIdHolderAccountPk()+" ");
		}
		if(Validations.validateIsNotNull(gfto.getIdSecurityCodePk())) {
			sbQuery.append(" AND S.ID_SECURITY_CODE_PK='"+gfto.getIdSecurityCodePk().toString()+"' ");
		}
		if(Validations.validateIsNotNull(gfto.getSecurityClass())){
    		sbQuery.append(" and S.security_class = "+gfto.getSecurityClass()+" ");
    	}
		if(Validations.validateIsNotNull(gfto.getIdIssuer())){
    		sbQuery.append(" and S.id_issuer_fk='"+gfto.getIdIssuer()+"' ");
    	}
		if(Validations.validateIsNotNull(gfto.getCurrency())){
    		sbQuery.append(" and S.currency="+gfto.getCurrency()+" ");
    	}
		
		sbQuery.append(" GROUP BY S.CURRENCY ");
		sbQuery.append(" ORDER BY S.CURRENCY ");
		
		String strQueryFormatted = sbQuery.toString();
    	return strQueryFormatted;
    }
    
    public NoteRegistrationDGravamenTO getRegistrationDGravamen(String blockType){
    	StringBuilder sbQuery= new StringBuilder();
    	
    	sbQuery.append(" Select new com.pradera.report.generation.executor.account.to.NoteRegistrationDGravamenTO( ");
    	sbQuery.append(" br.confirmationDate, be.fullName, br.idBlockRequestPk, br.blockNumber, br.blockNumberDate, ");
    	sbQuery.append(" br.registryUser, h.idHolderPk, s.securityClass, i.businessName, s.description, ");
    	sbQuery.append(" bmo.actualBlockBalance ) ");
    	
		sbQuery.append(" from BlockMarketFactOperation bmo ");
		sbQuery.append(" INNER JOIN bmo.blockOperation bo ");
		sbQuery.append(" INNER JOIN bo.blockRequest br ");
		sbQuery.append(" INNER JOIN bo.securities s ");
		sbQuery.append(" INNER JOIN br.blockEntity be ");
		sbQuery.append(" INNER JOIN br.holder h ");
		sbQuery.append(" INNER JOIN s.issuer i ");
		sbQuery.append(" where");
		if(Validations.validateIsNotNull(blockType)) {
			sbQuery.append(" AND br.blockType= "+blockType/*.toString()*/+" ");
		}
		/*if(dgravamenTO.getBlockType()!=null){
			sbQuery.append(" br.block_type= :blockType");
		}*/
		//sbQuery.append(" order by br.confirmation_date, br.id_block_request_pk");
		Query query = em.createQuery(sbQuery.toString());
		
		if(query.getResultList().size()>0){
    	return (NoteRegistrationDGravamenTO)query.getResultList().get(0);
		}
		
    	return new NoteRegistrationDGravamenTO();
    }
    
    public BigDecimal getExchangeRateByCurrencyType(DailyExchangeRates exchangeRate) throws ServiceException{	
    	StringBuilder sbQuery = new StringBuilder();
    	
    	sbQuery.append(" SELECT BUY_PRICE ");
    	sbQuery.append(" FROM DAILY_EXCHANGE_RATES DER ");
    	sbQuery.append(" WHERE DER.ID_SOURCE_INFORMATION=:infSource AND DER.ID_CURRENCY=:currencyType ");
    	if(exchangeRate.getDateRate()!=null){
    		sbQuery.append(" AND trunc(DER.DATE_RATE)=trunc(:dateRate) ");
    	}else{
    		sbQuery.append(" AND DER.DATE_RATE=TO_DATE(SYSDATE) ");
    	}
    	
    	Query query = em.createNativeQuery(sbQuery.toString());
    	
    	query.setParameter("infSource", InformationSourceType.BCRD.getCode());
    	query.setParameter("currencyType", exchangeRate.getIdCurrency());
    	
    	if(exchangeRate.getDateRate()!=null){
    		query.setParameter("dateRate", exchangeRate.getDateRate());
    	}
    	
    	BigDecimal bdBuyPrice=null;
    	if(!query.getResultList().isEmpty()){
    		bdBuyPrice=(BigDecimal) query.getResultList().get(0);
    	}else{
    		throw new ServiceException(ErrorServiceType.NOT_EXCHANGE_RATE);
    	}
    	return bdBuyPrice;
    }
    /*it only returns the query to send to the jasper as PARAMETER*/
    public String getQueryCustodyDetailAgbSafiFic(PortafolioDetailCustodianTO pdcTO, Long idStkCalcProcess){
    	StringBuilder sbQuery= new StringBuilder();
//    	Query query = em.createNativeQuery(((Report)find(Report.class, pdcTO.getReportPk())).getQueryReport());
//    	sbQuery.append(((Report)find(Report.class, pdcTO.getReportPk())).getQueryReport());
//    	return sbQuery.toString(); 
    	
    	sbQuery.append("SELECT																										 	");
		sbQuery.append("	'CVP' as CUSTODIO,																						 	");// CUSTODIO
		sbQuery.append("    to_char(TO_DATE(:date,'dd/MM/yyyy'),'YYYY-MM-DD') INFO_DATE,											 	");//FECHA DE INFORMACION
		sbQuery.append("	DECODE(H.ECONOMIC_ACTIVITY,43,H.MNEMONIC,NVL(H.FUND_ADMINISTRATOR,' ')) AS NEMONIC_SAFI, 				 	");//SAFI
		sbQuery.append("	DECODE(H.ECONOMIC_ACTIVITY,43,' ', NVL(H.MNEMONIC,' ')) AS NEMONIC_FUND,					 			 	");//FONDO
		sbQuery.append("    (SELECT TEXT1 FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = SEC.SECURITY_CLASS) AS SECURITY_CLASS,	 	");//CLASE
		sbQuery.append("	SEC.ID_SECURITY_CODE_ONLY AS SECURITY_CODE, 															 	");//VALOR
		sbQuery.append("	case                                                                                                	 	");
		sbQuery.append("        when SUM(MFV.reporting_balance) > 0                                                                  	");
		sbQuery.append("            then null                                                                                        	");
		sbQuery.append("        when SEC.SECURITY_CLASS = 1855 OR SEC.SECURITY_CLASS = 1854 							             	");
		sbQuery.append("            then null                                                                                        	");
		sbQuery.append("        else MFV.VALUATOR_CODE 																				 	");
		sbQuery.append("                end VALUATOR_CODE,                                                                           	");//COD. VALORACION
		sbQuery.append("    NVL(SUM(MFV.TOTAL_BALANCE),0) as SECURITIES_AMOUNT,                                                         ");//CANTIDAD	tamo
		sbQuery.append("    MFV.TOTAL_BALANCE AS TOTAL_BALANCE,                                                                         ");//SALDO TOTAL
		sbQuery.append("    MFV.AVAILABLE_BALANCE AS AVAILABLE_BALANCE,                                                                 ");//SALDO DISPONIBLE
		sbQuery.append("    MFV.BAN_BALANCE AS BAN_BALANCE,																				");//SALDO BLOQUEADO
		sbQuery.append("    'DES' AS CUSTODIA,                                                                                       	");//CUSTODIA
		sbQuery.append("	case 																									 	");
		sbQuery.append("        when SUM(MFV.reporting_balance) > 0                                                                  	");
		sbQuery.append("        then 'REP' else null                                                                                 	");
		sbQuery.append("    end IND_REPORTO,         	                                                                             	");//IND. REPORTO
		sbQuery.append("    H.ID_HOLDER_PK,             	                                                                         	");//PK DE HOLDER PARA PDF
		sbQuery.append("    H.FULL_NAME	                    		                                                                 	");//FULL NAME PARA PDF
		sbQuery.append("FROM                                                                                                         	");
		sbQuery.append("		MARKET_FACT_VIEW MFV                                                                                 	");
		sbQuery.append("		inner join HOLDER_ACCOUNT HA           ON HA.ID_HOLDER_ACCOUNT_PK = MFV.ID_HOLDER_ACCOUNT_PK 		 	");
		sbQuery.append("		inner join SECURITY SEC                ON SEC.ID_SECURITY_CODE_PK = MFV.ID_SECURITY_CODE_PK			 	");		
		sbQuery.append("		inner join HOLDER_ACCOUNT_DETAIL HAD   ON HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK  	     	");
		sbQuery.append("		inner join HOLDER H                    ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK                          	");
		sbQuery.append("		inner join ISSUANCE ISSU 			   ON ISSU.ID_ISSUANCE_CODE_PK = SEC.ID_ISSUANCE_CODE_FK 		 	");		
		sbQuery.append("WHERE ISSU.IND_REGULATOR_REPORT = 1                                                                          	");
		sbQuery.append("    AND TRUNC(MFV.CUT_DATE) = to_date(:date, 'dd/MM/yyyy')													 	");
		sbQuery.append("    AND H.HOLDER_TYPE = 394                                                                                  	");
		sbQuery.append("    AND (MFV.TOTAL_BALANCE > 0)    									                                         	");
//		sbQuery.append("    AND H.ECONOMIC_ACTIVITY in (43,55,56,57)                                                        		 	");
		sbQuery.append("GROUP BY H.ECONOMIC_ACTIVITY,H.MNEMONIC,H.FUND_ADMINISTRATOR,												 	");
		sbQuery.append("SEC.SECURITY_CLASS,SEC.ID_SECURITY_CODE_ONLY,MFV.VALUATOR_CODE,												 	");
		sbQuery.append("MFV.EQUIVALENT_RATE, MFV.TOTAL_BALANCE, MFV.AVAILABLE_BALANCE,MFV.BAN_BALANCE, 									");
		sbQuery.append("H.ID_HOLDER_PK, H.FULL_NAME    																				 	");
		
//		sbQuery.append(" union all 																									 	");                                                                                              
//                                                                                                                                    
//		sbQuery.append("SELECT 																										 	");                                                                                                  
//		sbQuery.append("	'EDB' as CUSTODIO, 																						 	");
//		sbQuery.append("	to_char(TO_DATE(:date,'dd/MM/yyyy'),'YYYY-MM-DD') INFO_DATE,											 	");
//		sbQuery.append("	NVL(H.FUND_ADMINISTRATOR,' ') AS NEMONIC_SAFI, 															 	");
//		sbQuery.append("	NVL(H.MNEMONIC,' ') AS NEMONIC_FUND, 																	 	");
//		sbQuery.append("	(SELECT TEXT1 FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = PC.SECURITY_CLASS) AS SECURITY_CLASS,		 	");
//		sbQuery.append("	SEC.ID_SECURITY_CODE_ONLY SECURITY_CODE,							 									 	");
//		sbQuery.append("	NULL AS VALUATOR_CODE,																					 	");
//		sbQuery.append("	PC.CERTIFICATE_QUANTITY AS SECURITIES_AMOUNT,															 	");
//		sbQuery.append("	PCM.MARKET_RATE AS RATE, 																					");
//		sbQuery.append("	PCM.MARKET_PRICE AS PRICE,																					");
//		sbQuery.append("	'FIS' AS CUSTODIA, 																						 	");
//		sbQuery.append("	NULL AS IND_REPORTO, 																					 	");
//		sbQuery.append("    H.ID_HOLDER_PK,             	                                                                         	");//PK DE HOLDER PARA PDF
//		sbQuery.append("    H.FULL_NAME	                    		                                                                 	");//FULL NAME PARA PDF
//		sbQuery.append("FROM PHYSICAL_CERTIFICATE PC 																					");
//		sbQuery.append("	LEFT OUTER JOIN VALUATOR_CODE_VIEW VCV    ON VCV.ID_SECURITY_CODE_PK = pc.id_security_code_fk 			 	");
//		sbQuery.append("	inner join SECURITY SEC                	  ON SEC.ID_SECURITY_CODE_PK = pc.ID_SECURITY_CODE_FK			 	");
//		sbQuery.append("	inner join PHYSICAL_CERT_MARKETFACT pcm   ON pc.ID_PHYSICAL_CERTIFICATE_PK =pcm.ID_PHYSICAL_CERTIFICATE_FK 	");
//		sbQuery.append("	inner join PHYSICAL_BALANCE_DETAIL pbd    ON pc.ID_PHYSICAL_CERTIFICATE_PK=pbd.ID_PHYSICAL_CERTIFICATE_FK 	");
//		sbQuery.append("	inner join HOLDER_ACCOUNT ha              ON pbd.ID_HOLDER_ACCOUNT_FK=ha.ID_HOLDER_ACCOUNT_PK 				");
//		sbQuery.append("	inner join HOLDER_ACCOUNT_DETAIL had      ON ha.ID_HOLDER_ACCOUNT_PK = had.ID_HOLDER_ACCOUNT_FK 			");
//		sbQuery.append("	inner join HOLDER h                       ON had.ID_HOLDER_FK=h.ID_HOLDER_PK 								");
//		sbQuery.append("where PC.ID_SECURITY_CODE_FK IS NOT NULL 																		");
//		sbQuery.append("AND TRUNC(pbd.DEPOSIT_DATE) <= TO_DATE(:date,'dd/MM/yyyy')														");
//		sbQuery.append("AND (pbd.RETIREMENT_DATE IS NULL OR TRUNC(pbd.RETIREMENT_DATE) > TO_DATE(:date,'dd/MM/yyyy'))					");
////		sbQuery.append("    AND PC.state in (601,602,605) 																				");//CONFIRMADO,AUTORIZADO,DEPOSITADO
////		sbQuery.append("    AND PC.situation in (611,608,609) 																			");//ESTADO Q CONSIDERA EL VALORADOR
//		//sbQuery.append("    AND TRUNC(PCM.REGISTRY_DATE) < to_date(:date, 'dd/MM/yyyy') 												");
//		sbQuery.append("    AND pcm.IND_ACTIVE = 1									 													");
////		sbQuery.append("    AND pbd.STATE=:state 																						");
//		sbQuery.append("	AND h.HOLDER_TYPE = 394 																					");
//		sbQuery.append("    AND H.ECONOMIC_ACTIVITY in (43,55,56,57)                                                        			");
//		sbQuery.append("ORDER BY	11,5,6,13																							");
		
//		if(pdcTO.getReportFormat().equals(ReportFormatType.TXT.getCode())){
//			sbQuery.append(" order by 11,5,6,13																							");
//		}else{
//			sbQuery.append(" order by 11,5,6,13																							");
//		}
    	
    	return sbQuery.toString();
    }
    
    public String getQueryForJasperAgbSafiFic(PortafolioDetailCustodianTO pdcTO, String sbQuery, Long idStkCalcProcess){
    	String strQueryFormatted = sbQuery.toString();
    	strQueryFormatted= strQueryFormatted.replace(":date", "'"+CommonsUtilities.convertDateToString(pdcTO.getDateMarketFact(),"dd/MM/yyyy")+"'");
//    	strQueryFormatted=strQueryFormatted.replace(":state", StateType.DEPOSITED.getCode().toString());
    	if(Validations.validateIsNotNullAndPositive(pdcTO.getCui())){
    		strQueryFormatted=strQueryFormatted.replace(":cui_code", pdcTO.getCui().toString());
    	}
    	if(pdcTO.getIdSecurityCode()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":securityPk", "'"+pdcTO.getIdSecurityCode()+"'");
    	}
    	if(pdcTO.getEconomicActivity()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":economicAct", pdcTO.getEconomicActivity().toString());
    	}
    	if(idStkCalcProcess!=null){
    		strQueryFormatted=strQueryFormatted.replace(":idSCP", idStkCalcProcess.toString());
    	}
    	
		return strQueryFormatted;
    }
    
    @SuppressWarnings("unused")
	public List<PortafolioDetailCustodianTO> getCustodyDetailAgbSafiFicTxt(PortafolioDetailCustodianTO pdcTO, String sbQuery, Long idStkCalcProcess, ReportLogger reportLogger){
    	Query query = em.createNativeQuery(sbQuery.toString());
		PortafolioDetailCustodianTO pdc= null;
		List<PortafolioDetailCustodianTO> lstPDC = new ArrayList<PortafolioDetailCustodianTO>();

		query.setParameter("date", CommonsUtilities.convertDatetoString(pdcTO.getDateMarketFact()));
//		query.setParameter("state", StateType.DEPOSITED.getCode().toString());
		if(Validations.validateIsNotNullAndPositive(pdcTO.getCui())){
    		query.setParameter("cui_code",pdcTO.getCui());
    	}
		if(pdcTO.getEconomicActivity()!=null){
			query.setParameter("economicAct", pdcTO.getEconomicActivity());
    	}
    	if(pdcTO.getIdSecurityCode()!=null){
    		query.setParameter("securityPk",pdcTO.getIdSecurityCode());
    	}
		if(idStkCalcProcess!=null){
			query.setParameter("idSCP", idStkCalcProcess);
		}
		
		for(Object[] obj : (List<Object[]>)query.getResultList()){
			pdc= new PortafolioDetailCustodianTO();
			
			pdc.setCustodio(obj[0].toString()); //CUSTODIA
			pdc.setDateMarketFactString(obj[1].toString());//INFO DATE
			try{
				pdc.setFundAdmin(obj[2].toString());//MNEMONICO SAFI
				if(obj[2]==null){
					new NullPointerException();
				}
			}catch(Exception np){
				
				long idBusinessProcess = 40741;
		    	BusinessProcess businessProcess = find(BusinessProcess.class, idBusinessProcess);
		    	
		        String errorDetail = "No existe MNEMONICO ASFI para el CUI: " + obj[12].toString();
				Object[] parametersNotif = {errorDetail};
		        
				notificationServiceFacade.sendNotification(reportLogger.getLastModifyUser(),businessProcess,null, parametersNotif,  null);
		    	
				throw new NullPointerException("No existe MNEMONICO ASFI para el CUI: " + obj[12].toString());
			}
			
			try{
				pdc.setFundType(obj[3].toString());//MNEMONICO FONDOS
				if(obj[3]==null){
					new NullPointerException();
				}
			}catch(Exception np){
				
				long idBusinessProcess = 40741;
		    	BusinessProcess businessProcess = find(BusinessProcess.class, idBusinessProcess);
		    	
		        String errorDetail = "No existe MNEMONICO FONDO para el CUI: " + obj[12].toString();
				Object[] parametersNotif = {errorDetail};
		        
				notificationServiceFacade.sendNotification(reportLogger.getLastModifyUser(),businessProcess,null, parametersNotif,  null);
				
				throw new NullPointerException("No existe MNEMONICO FONDO para el CUI: " + obj[12].toString());
			}
			
			pdc.setInstrumentSecClass(obj[4].toString());//CLASE
			pdc.setIdSecurityCode(obj[5].toString());//CLAVE
			if(Validations.validateIsNotNullAndNotEmpty(obj[6])){
				pdc.setValuatorCode(obj[6].toString());
			}else{
				pdc.setValuatorCode(GeneralConstants.EMPTY_STRING);
			} //COD DE VALORACION
			pdc.setTotalBalance((BigDecimal)obj[7]);//CANTIDAD
			pdc.setMarketRate((BigDecimal)obj[8]);//TASA
			pdc.setMarketPrice((BigDecimal)obj[9]);//PRECIO
			pdc.setSecCodeOnCustody(obj[10].toString()); //DES o FIS
			if(obj[11]!=null){
				pdc.setIndReporto(obj[11].toString());
			}else{
				pdc.setIndReporto(GeneralConstants.EMPTY_STRING);
			}//IND REPORTO
			
			lstPDC.add(pdc);
		}
    	return lstPDC;
    }
    
    public List<PortafolioDetailCustodianTO> getCustodyDetailAgbSafiFicTxt(PortafolioDetailCustodianTO pdcTO, String sbQuery, Long idStkCalcProcess){
		Query query = em.createNativeQuery(sbQuery.toString());
		PortafolioDetailCustodianTO pdc= null;
		List<PortafolioDetailCustodianTO> lstPDC = new ArrayList<PortafolioDetailCustodianTO>();

		query.setParameter("date", CommonsUtilities.convertDatetoString(pdcTO.getDateMarketFact()));
//		query.setParameter("state", StateType.DEPOSITED.getCode().toString());
		if(Validations.validateIsNotNullAndPositive(pdcTO.getCui())){
    		query.setParameter("cui_code",pdcTO.getCui());
    	}
		if(pdcTO.getEconomicActivity()!=null){
			query.setParameter("economicAct", pdcTO.getEconomicActivity());
    	}
    	if(pdcTO.getIdSecurityCode()!=null){
    		query.setParameter("securityPk",pdcTO.getIdSecurityCode());
    	}
		if(idStkCalcProcess!=null){
			query.setParameter("idSCP", idStkCalcProcess);
		}
		
		for(Object[] obj : (List<Object[]>)query.getResultList()){
			pdc= new PortafolioDetailCustodianTO();
			
			pdc.setCustodio(obj[0].toString()); //CUSTODIA
			pdc.setDateMarketFactString(obj[1].toString());//INFO DATE
			pdc.setFundAdmin(obj[2].toString());//MNEMONICO SAFI
			pdc.setFundType(obj[3].toString());//MNEMONICO FONDOS
			pdc.setInstrumentSecClass(obj[4].toString());//CLASE
			pdc.setIdSecurityCode(obj[5].toString());//CLAVE
			if(Validations.validateIsNotNullAndNotEmpty(obj[6])){
				pdc.setValuatorCode(obj[6].toString());
			}else{
				pdc.setValuatorCode(GeneralConstants.EMPTY_STRING);
			} //COD DE VALORACION
			pdc.setTotalBalance((BigDecimal)obj[7]);//CANTIDAD
			pdc.setMarketRate((BigDecimal)obj[8]);//TASA
			pdc.setMarketPrice((BigDecimal)obj[9]);//PRECIO
			pdc.setSecCodeOnCustody(obj[10].toString()); //DES o FIS
			if(obj[11]!=null){
				pdc.setIndReporto(obj[11].toString());
			}else{
				pdc.setIndReporto(GeneralConstants.EMPTY_STRING);
			}//IND REPORTO
			
			lstPDC.add(pdc);
		}
    	return lstPDC;
    }
     
     public List<HolderReqFileHistory> getHolderReqFileHistoryByHolderRequest(Long idHolderRequest){
      	StringBuilder sbQuery = new StringBuilder();
      	
      	sbQuery.append(" Select h");
      	sbQuery.append(" From HolderReqFileHistory h Where h.holderRequest.idHolderRequestPk =:holderRequest");
      	Query query = em.createQuery(sbQuery.toString());
      	query.setParameter("holderRequest", idHolderRequest);
      	
      	return (List<HolderReqFileHistory>)query.getResultList();
      	
      }
     
     public List<RepresentativeFileHistory> getRepresentativeFileHistoryByRepresentativeHistory(Long idRespresentativeHistory, Long idRespresentativeHistoryCopy){
       	StringBuilder sbQuery = new StringBuilder();
       	
       	if(Validations.validateIsNull(idRespresentativeHistory) && Validations.validateIsNull(idRespresentativeHistoryCopy)){
       		return new ArrayList<RepresentativeFileHistory>();
       	}else{
       		sbQuery.append(" Select h");
       		if(Validations.validateIsNotNull(idRespresentativeHistory) && Validations.validateIsNotNull(idRespresentativeHistoryCopy)){
       			sbQuery.append(" From RepresentativeFileHistory h Where h.legalRepresentativeHistory.idRepresentativeHistoryPk in (:respresentativeHistory,:respresentativeHistoryCopy)");
       		} else if(Validations.validateIsNotNull(idRespresentativeHistory)){
       			sbQuery.append(" From RepresentativeFileHistory h Where h.legalRepresentativeHistory.idRepresentativeHistoryPk in (:respresentativeHistory)");
       		} else if(Validations.validateIsNotNull(idRespresentativeHistoryCopy)){
       			sbQuery.append(" From RepresentativeFileHistory h Where h.legalRepresentativeHistory.idRepresentativeHistoryPk in (:respresentativeHistoryCopy)");
       		}
       		Query query = em.createQuery(sbQuery.toString());
       		if(Validations.validateIsNotNull(idRespresentativeHistory)){
       			query.setParameter("respresentativeHistory", idRespresentativeHistory);
       		}
       		if(Validations.validateIsNotNull(idRespresentativeHistoryCopy)){
       			query.setParameter("respresentativeHistoryCopy", idRespresentativeHistoryCopy);
       		}
       		return (List<RepresentativeFileHistory>)query.getResultList();
       	}
       	
       	
       }
     
     public String getModificationHolderRequest(Long idHolderRequest){
     	StringBuilder sbQuery = new StringBuilder();
     	
     	sbQuery.append(" SELECT  ");
     	sbQuery.append("   HR.ID_HOLDER_REQUEST_PK,  ");
     	sbQuery.append("   (SELECT (P.MNEMONIC ||' - '|| P.ID_PARTICIPANT_PK || ' - ' || P.DESCRIPTION)  FROM PARTICIPANT P WHERE P.ID_PARTICIPANT_PK=H.ID_PARTICIPANT_FK) PARTICIPANT, ");
     	sbQuery.append("   H.ID_HOLDER_PK, ");
     	sbQuery.append("   H.FULL_NAME, ");
     	sbQuery.append("   HR.REGISTRY_DATE, ");
     	sbQuery.append("   HR.STATE_HOLDER_REQUEST, ");
     	sbQuery.append("   HR.REQUEST_NUMBER ");
     	sbQuery.append(" FROM HOLDER H  ");
     	sbQuery.append(" INNER JOIN HOLDER_REQUEST HR ON H.ID_HOLDER_PK=HR.ID_HOLDER_FK ");
     	sbQuery.append(" WHERE ");
     	sbQuery.append(" HR.REQUEST_TYPE = 518 ");
 		sbQuery.append(" and HR.ID_HOLDER_REQUEST_PK = "+idHolderRequest+" ");
     	
     	
     	return sbQuery.toString();
     	
     }
     
     public String getModificationHolderAccountRequest(Long idHolderRequest){
     	StringBuilder sbQuery = new StringBuilder();
     	
     	sbQuery.append(" SELECT  ");
     	sbQuery.append("   HR.ID_HOLDER_ACCOUNT_REQ_PK,  ");
     	sbQuery.append("   (SELECT (P.MNEMONIC||' - '|| P.ID_PARTICIPANT_PK || ' - ' || P.DESCRIPTION)  FROM PARTICIPANT P WHERE P.ID_PARTICIPANT_PK=HA.ID_PARTICIPANT_FK) PARTICIPANT, ");
     	sbQuery.append("   HA.ID_HOLDER_ACCOUNT_PK, ");
     	sbQuery.append("   HA.ACCOUNT_NUMBER, ");
     	sbQuery.append("   HR.REGISTRY_DATE, ");
     	sbQuery.append("   HR.REQUEST_STATE   ");
     	sbQuery.append(" FROM HOLDER_ACCOUNT HA  ");
     	sbQuery.append(" INNER JOIN HOLDER_ACCOUNT_REQUEST HR ON HA.ID_HOLDER_ACCOUNT_PK=HR.ID_HOLDER_ACCOUNT_FK ");
     	sbQuery.append(" WHERE ");
     	sbQuery.append(" HR.REQUEST_TYPE = 473 ");
 		sbQuery.append(" and HR.ID_HOLDER_ACCOUNT_REQ_PK = "+idHolderRequest+" ");
     	
     	
     	return sbQuery.toString();
     	
     }
     
     public List<Holder> getHolderByHolderRequest(Long idHolderRequest){
     	StringBuilder sbQuery = new StringBuilder();

     	sbQuery.append(" Select h.holder ");
     	sbQuery.append(" From HolderAccountDetRequest h Where h.holderAccountRequest.idHolderAccountReqPk =:holderRequest");
     	Query query = em.createQuery(sbQuery.toString());
     	query.setParameter("holderRequest", idHolderRequest);
     	
     	return  (List<Holder>)query.getResultList();
     }
     
     public List<HolderAccountDetRequest> getHolderAccountDetByHolderRequest(Long idHolderRequest){
        	StringBuilder sbQuery = new StringBuilder();

        	sbQuery.append(" Select h ");
        	sbQuery.append(" From HolderAccountDetRequest h Where h.holderAccountRequest.idHolderAccountReqPk =:holderRequest");
        	Query query = em.createQuery(sbQuery.toString());
        	query.setParameter("holderRequest", idHolderRequest);
        	
        	return  (List<HolderAccountDetRequest>)query.getResultList();
     }
     
     public List<HolderAccountBankRequest> getHolderAccountBankByHolderRequest(Long idHolderRequest){
       	StringBuilder sbQuery = new StringBuilder();

       	sbQuery.append(" Select h ");
       	sbQuery.append(" From HolderAccountBankRequest h Where h.holderAccountRequest.idHolderAccountReqPk =:holderRequest");
       	Query query = em.createQuery(sbQuery.toString());
       	query.setParameter("holderRequest", idHolderRequest);
       	
       	return  (List<HolderAccountBankRequest>)query.getResultList();
     }
     public List<Bank> getBankByHolderRequest(Long idHolderRequest){
        	StringBuilder sbQuery = new StringBuilder();

        	sbQuery.append(" Select h.bank");
        	sbQuery.append(" From HolderAccountBankRequest h Where h.holderAccountRequest.idHolderAccountReqPk =:holderRequest");
        	Query query = em.createQuery(sbQuery.toString());
        	query.setParameter("holderRequest", idHolderRequest);
        	
        	return  (List<Bank>)query.getResultList();
      }
     
     public List<HolderAccountFileRequest> getHolderAccountFileByHolderRequest(Long idHolderRequest){
       	StringBuilder sbQuery = new StringBuilder();
       	
       	sbQuery.append(" Select h");
       	sbQuery.append(" From HolderAccountFileRequest h Where h.holderAccountRequest.idHolderAccountReqPk =:holderRequest");
       	Query query = em.createQuery(sbQuery.toString());
       	query.setParameter("holderRequest", idHolderRequest);
       	
       	return (List<HolderAccountFileRequest>)query.getResultList();
       	
     }
     
     public String getQueryRequestModifyingOfDataCuiReport(GenericsFiltersTO gfto){
        	StringBuilder sbQuery = new StringBuilder();
        	
        	sbQuery.append(" SELECT ");
        	sbQuery.append(" 	(SELECT (P.MNEMONIC||' - '|| P.ID_PARTICIPANT_PK || ' - ' || P.DESCRIPTION)  FROM PARTICIPANT P WHERE P.ID_PARTICIPANT_PK=H.ID_PARTICIPANT_FK) PARTICIPANT, ");
        	sbQuery.append(" 	H.ID_HOLDER_PK CUI, ");
        	sbQuery.append(" 	H.FULL_NAME NOMBRE_CUI, ");
        	sbQuery.append(" 	HR.REGISTRY_DATE FECHA_REGISTRO, ");
        	sbQuery.append(" 	H.HOLDER_TYPE TIPO_HOLDER, ");
        	sbQuery.append(" 	H.STATE_HOLDER ESTADO_HOLDER, ");
        	sbQuery.append(" 	HR.ID_HOLDER_REQUEST_PK SOLICITUD, ");
        	sbQuery.append(" 	HR.STATE_HOLDER_REQUEST ESTADO_SOLICITUD, ");
        	sbQuery.append(" 	HR.REQUEST_NUMBER  ");
        	
        	sbQuery.append(" FROM HOLDER_REQUEST HR ");
        	sbQuery.append(" 	INNER JOIN HOLDER H ON HR.ID_HOLDER_FK=H.ID_HOLDER_PK ");
        	
        	sbQuery.append(" WHERE 1=1 ");
        	sbQuery.append(" 	AND HR.REQUEST_TYPE=518 ");
         	sbQuery.append(" 	AND trunc(HR.REGISTRY_DATE) BETWEEN TO_DATE('"+ gfto.getInitialDt() +"','dd/MM/yyyy') AND TO_DATE('"+ gfto.getFinalDt() +"','dd/MM/yyyy') ");
         	
         	if(Validations.validateIsNotNull(gfto.getInstitutionType())
         			&& gfto.getInstitutionType().equals(InstitutionType.PARTICIPANT_INVESTOR.getCode().toString())){
         		//PARTICIPANT_INVESTOR
         		sbQuery.append(" AND (EXISTS(SELECT 1 ");
         		sbQuery.append("	FROM PARTICIPANT PA ");
         		sbQuery.append("	WHERE PA.DOCUMENT_TYPE = H.DOCUMENT_TYPE ");
         		sbQuery.append("		AND PA.DOCUMENT_NUMBER = H.DOCUMENT_NUMBER ");
         		sbQuery.append("	)) ");
         	}
         	
         	if(Validations.validateIsNotNull(gfto.getInstitutionType())
         			&& gfto.getInstitutionType().equals(InstitutionType.ISSUER_DPF.getCode().toString())){
         		//ISSUER_DPF
         		sbQuery.append(" AND (EXISTS(SELECT 1 ");
         		sbQuery.append("	FROM ISSUER I ");
         		
         		sbQuery.append("	WHERE I.ID_ISSUER_PK = " + gfto.getIdIssuer() + " ");
         		sbQuery.append("		AND I.DOCUMENT_TYPE = H.DOCUMENT_TYPE ");
         		sbQuery.append("		AND I.DOCUMENT_NUMBER = H.DOCUMENT_NUMBER ");
         		sbQuery.append("	)) ");
         	}
         	
         	if(Validations.validateIsNotNull(gfto.getParticipant())){
         		sbQuery.append(" AND H.ID_PARTICIPANT_FK = "+ gfto.getParticipant() + " ");
         	}
         	if(Validations.validateIsNotNull(gfto.getCui())){
         		sbQuery.append(" AND H.ID_HOLDER_PK = "+ gfto.getCui() + " ");
         	}
         	if(Validations.validateIsNotNull(gfto.getState())){
         		sbQuery.append(" AND HR.STATE_HOLDER_REQUEST = "+ gfto.getState() + " ");
         	}
         	sbQuery.append(" ORDER BY ");
         	if(Validations.validateIsNotNull(gfto.getInstitutionType())
         			&& !gfto.getInstitutionType().equals(InstitutionType.ISSUER.getCode().toString())
         			&& !gfto.getInstitutionType().equals(InstitutionType.ISSUER_DPF.getCode().toString())){
         		sbQuery.append(" 	H.ID_PARTICIPANT_FK, ");         		
         	}
         	sbQuery.append(" 	HR.REGISTRY_DATE,H.ID_HOLDER_PK ");
         	sbQuery.append("  ");
         	
         	return sbQuery.toString();
     }
     
     public String getQueryRequestModifyingOfDataAccountReport(GenericsFiltersTO gfto){
     	StringBuilder sbQuery = new StringBuilder();
     	
     	sbQuery.append(" SELECT "); 
     	sbQuery.append(" 	(SELECT (P.MNEMONIC||' - '|| P.ID_PARTICIPANT_PK || ' - ' || P.DESCRIPTION)  FROM PARTICIPANT P WHERE P.ID_PARTICIPANT_PK=HA.ID_PARTICIPANT_FK) PARTICIPANT, ");
     	sbQuery.append(" 	HA.ID_HOLDER_ACCOUNT_PK, ");
     	sbQuery.append(" 	HA.ACCOUNT_NUMBER CUENTA, ");
     	sbQuery.append(" 	HAR.REGISTRY_DATE FECHA_REGISTRO, ");
     	sbQuery.append(" 	HA.ACCOUNT_TYPE TIPO_CUENTA, ");
     	sbQuery.append(" 	HA.STATE_ACCOUNT ESTADO_CUENTA, ");
     	sbQuery.append(" 	HAR.ID_HOLDER_ACCOUNT_REQ_PK SOLICITUD, ");
     	sbQuery.append(" 	HAR.REQUEST_STATE ESTADO_SOLICITUD ");
     	
     	sbQuery.append(" FROM HOLDER_ACCOUNT_REQUEST HAR ");
     	sbQuery.append(" 	INNER JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK=HAR.ID_HOLDER_ACCOUNT_FK ");
     	
     	sbQuery.append(" WHERE 1=1 ");
    	sbQuery.append(" AND HAR.REQUEST_TYPE=473 ");
     	sbQuery.append(" AND trunc(HAR.REGISTRY_DATE) BETWEEN TO_DATE('"+ gfto.getInitialDt() +"','dd/MM/yyyy') AND TO_DATE('"+ gfto.getFinalDt() +"','dd/MM/yyyy') ");
      	
     	if(Validations.validateIsNotNull(gfto.getInstitutionType())
     			&& gfto.getInstitutionType().equals(InstitutionType.PARTICIPANT_INVESTOR.getCode().toString())){
     		//PARTICIPANT_INVESTOR
     		sbQuery.append(" AND (EXISTS(SELECT 1 ");
     		sbQuery.append("	FROM PARTICIPANT PA ");
     		sbQuery.append(" 		INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = HAR.ID_HOLDER_ACCOUNT_FK ");
     		sbQuery.append(" 		INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK");
     		sbQuery.append("	WHERE PA.DOCUMENT_TYPE = H.DOCUMENT_TYPE ");
     		sbQuery.append("		AND PA.DOCUMENT_NUMBER = H.DOCUMENT_NUMBER ");
     		sbQuery.append("		AND PA.ID_PARTICIPANT_PK = HAR.ID_PARTICIPANT_FK ");
     		sbQuery.append("	)) ");
     	}
     	
     	if(Validations.validateIsNotNull(gfto.getInstitutionType())
     			&& gfto.getInstitutionType().equals(InstitutionType.ISSUER_DPF.getCode().toString())){
     		//ISSUER_DPF
     		sbQuery.append(" AND (EXISTS(SELECT 1 ");
     		sbQuery.append("	FROM ISSUER I ");
     		sbQuery.append(" 		INNER JOIN PARTICIPANT PA ON PA.DOCUMENT_TYPE = I.DOCUMENT_TYPE ");
     		sbQuery.append(" 								    AND PA.DOCUMENT_NUMBER = I.DOCUMENT_NUMBER ");
     		sbQuery.append(" 		INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ");
     		sbQuery.append(" 		INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
     		sbQuery.append("	WHERE I.ID_ISSUER_PK = " + gfto.getIdIssuer() + " ");
     		sbQuery.append("		AND I.DOCUMENT_TYPE = H.DOCUMENT_TYPE ");
     		sbQuery.append("		AND I.DOCUMENT_NUMBER = H.DOCUMENT_NUMBER ");
     		sbQuery.append("	)) ");
     	}
     	
      	if(Validations.validateIsNotNull(gfto.getParticipant())){
      		sbQuery.append(" AND HA.ID_PARTICIPANT_FK = "+ gfto.getParticipant() + " ");
      	}
      	if(Validations.validateIsNotNull(gfto.getCui())){
      		sbQuery.append(" AND (SELECT COUNT(*) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK AND HAD.ID_HOLDER_FK="+ gfto.getCui() + ")>0 ");
      	}
      	if(Validations.validateIsNotNull(gfto.getIdHolderAccountPk())){
      		sbQuery.append(" AND HA.ID_HOLDER_ACCOUNT_PK = "+ gfto.getIdHolderAccountPk() + " ");
      	}
      	if(Validations.validateIsNotNull(gfto.getState())){
      		sbQuery.append(" AND HAR.REQUEST_STATE = "+ gfto.getState() + " ");
      	}
      	sbQuery.append(" ORDER BY ");
     	if(Validations.validateIsNotNull(gfto.getInstitutionType())
     			&& !gfto.getInstitutionType().equals(InstitutionType.ISSUER.getCode().toString())
     			&& !gfto.getInstitutionType().equals(InstitutionType.ISSUER_DPF.getCode().toString())){
     		sbQuery.append(" 	HA.ID_PARTICIPANT_FK, ");         		
     	}
     	sbQuery.append(" 	HAR.REGISTRY_DATE ");
      	sbQuery.append("  ");
      	
      	return sbQuery.toString();
  }
     
     public List<Object[]> getHoldersByFilter(HolderHelperOutputTO filterTO)throws ServiceException{
 		StringBuilder sbQuery = new StringBuilder();
 		sbQuery.append("select distinct h.fund_type");//0
 		sbQuery.append(",  h.full_name");//1
 		sbQuery.append(",  h.id_holder_pk");//2
 		sbQuery.append(",  h.economic_activity ");//3
 		sbQuery.append(",  h.document_type ");//4
 		sbQuery.append(",  h.document_number ");//5
 		sbQuery.append(",  h.transfer_number ");//6
 		sbQuery.append(" from holder h ");
 		sbQuery.append(" where 1=1");
 		sbQuery.append(" and h.state_Holder = 496 ");//solo en tiulares en estado registrado
 		if(Validations.validateIsNotNullAndPositive(filterTO.getHolderId())){
 			sbQuery.append(" and h.id_holder_pk= :holderPk");			
 		}else if(filterTO.getFundCUI()!=null){
 			sbQuery.append(" and h.id_holder_pk= :fundCUI");
 		}
 		if(Validations.validateIsNotNull(filterTO.getLstFundType()) && filterTO.getLstFundType().size()>0){
 			sbQuery.append(" and h.fund_type in :lisFoundType");			
 		}else if(Validations.validateIsNotNullAndNotEmpty(filterTO.getStrFundType())){
 			sbQuery.append(" and h.fund_type= :foundType");			
 		}
 		if(Validations.validateIsNotNullAndPositive(filterTO.getEconomicActivity())){
 			sbQuery.append(" and h.economic_activity= :economicAct");			
 		}else if(Validations.validateListIsNotNullAndNotEmpty(filterTO.getLstEconomicActivity())){
 			sbQuery.append(" and h.economic_activity in (:lstEconomicAct)");			
 		}
 		sbQuery.append(" order by h.id_holder_pk ");
 		
 		Query query = em.createNativeQuery(sbQuery.toString());
 		
 		if(Validations.validateIsNotNull(filterTO.getHolderId())){
 			query.setParameter("holderPk",filterTO.getHolderId());
 		}else if(Validations.validateIsNotNull(filterTO.getFundCUI())){
 			query.setParameter("fundCUI",filterTO.getFundCUI());
 		}
 		if(Validations.validateIsNotNull(filterTO.getLstFundType()) && filterTO.getLstFundType().size()>0){
 			query.setParameter("lisFoundType",filterTO.getLstFundType());			
 		}else if(Validations.validateIsNotNullAndNotEmpty(filterTO.getStrFundType())){
 			query.setParameter("foundType",filterTO.getStrFundType());			
 		}
 		if(Validations.validateIsNotNullAndPositive(filterTO.getEconomicActivity())){
 			query.setParameter("economicAct",filterTO.getEconomicActivity());				
 		}else if(Validations.validateListIsNotNullAndNotEmpty(filterTO.getLstEconomicActivity())){
 			query.setParameter("lstEconomicAct",filterTO.getLstEconomicActivity());			
 		}

 	   return query.getResultList();

 	}

     public String getListResidenceByHolderReport(String date){
     	StringBuilder sbQuery= new StringBuilder();

     	
 		sbQuery.append(" 	SELECT ");
 		sbQuery.append(" 		SECU.CURRENCY, ");   
 		sbQuery.append(" 		SECU.SECURITY_CLASS, ");   
 		sbQuery.append(" 		NVL((SELECT TEXT1 FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK=SECU.SECURITY_CLASS), ' ') MNEMONIC_CLASS, ");    
 		sbQuery.append(" 		NVL(SUM(case H.IND_RESIDENCE when 1 then SECU.CURRENT_NOMINAL_VALUE*HAB.total_balance end ),0) AS NOMINAL_VALUE_R, ");      
 		sbQuery.append(" 		NVL(SUM(case H.IND_RESIDENCE when 1 then HAB.total_balance end ),0) AS NUMBER_VALUES_R, ");
 		sbQuery.append(" 		NVL(COUNT(distinct case H.IND_RESIDENCE when 1 then H.ID_HOLDER_PK end ),0) AS NUMBER_HOLDERS_R, ");
 		sbQuery.append(" 		NVL(SUM(case H.IND_RESIDENCE when 0 then SECU.CURRENT_NOMINAL_VALUE*HAB.total_balance end ),0) AS NOMINAL_VALUE_N, ");
 		sbQuery.append(" 		NVL(SUM(case H.IND_RESIDENCE when 0 then HAB.total_balance end ),0) AS NUMBER_VALUES_N, ");
 		sbQuery.append(" 		NVL(COUNT(distinct case H.IND_RESIDENCE when 0 then H.ID_HOLDER_PK end ),0) AS NUMBER_HOLDERS_N ");
 		 
 		if(CommonsUtilities.convertStringtoDate(date).before(CommonsUtilities.currentDate())){
 			sbQuery.append(" FROM HOLDER_ACCOUNT HA ");
 	 		sbQuery.append(" 	INNER JOIN HOLDER_ACCOUNT_DETAIL HAD  		ON HA.ID_HOLDER_ACCOUNT_PK 		= HAD.ID_HOLDER_ACCOUNT_FK ");
 	 		sbQuery.append(" 	INNER JOIN HOLDER H                   		ON HAD.ID_HOLDER_FK 			= H.ID_HOLDER_PK ");
 	 		sbQuery.append(" 	INNER JOIN STOCK_CALCULATION_BALANCE HAB  	ON HAB.ID_HOLDER_ACCOUNT_FK		= HA.ID_HOLDER_ACCOUNT_PK ");
 	 		sbQuery.append(" 	INNER JOIN STOCK_CALCULATION_PROCESS SCP  	ON SCP.ID_STOCK_CALCULATION_PK	= HAB.ID_STOCK_CALCULATION_FK ");
 	 		sbQuery.append(" 	INNER JOIN SECURITY SECU              		ON HAB.ID_SECURITY_CODE_FK 		= SECU.ID_SECURITY_CODE_PK");
 	 		sbQuery.append(" 	INNER JOIN ISSUER ISSU                		ON SECU.ID_ISSUER_FK 			= ISSU.ID_ISSUER_PK ");
 	 		
 	 		sbQuery.append(" 	WHERE TRUNC(SCP.CUTOFF_DATE) <= TO_DATE($P{date},'dd/mm/yyyy') ");
 		}else{
 			sbQuery.append(" FROM HOLDER_ACCOUNT HA ");
 	 		sbQuery.append(" 	INNER JOIN HOLDER_ACCOUNT_DETAIL HAD  		ON HA.ID_HOLDER_ACCOUNT_PK 	= HAD.ID_HOLDER_ACCOUNT_FK ");
 	 		sbQuery.append(" 	INNER JOIN HOLDER H                   		ON HAD.ID_HOLDER_FK 			= H.ID_HOLDER_PK ");
 	 		sbQuery.append(" 	INNER JOIN HOLDER_ACCOUNT_BALANCE HAB 		ON HAB.ID_HOLDER_ACCOUNT_PK 	= HA.ID_HOLDER_ACCOUNT_PK ");
 	 		sbQuery.append(" 	INNER JOIN SECURITY SECU              		ON HAB.ID_SECURITY_CODE_PK 	= SECU.ID_SECURITY_CODE_PK");
 	 		sbQuery.append(" 	INNER JOIN ISSUER ISSU                		ON SECU.ID_ISSUER_FK 			= ISSU.ID_ISSUER_PK ");
 	 		
 	 		sbQuery.append(" 	WHERE TRUNC(H.REGISTRY_DATE) <= TO_DATE($P{date},'dd/mm/yyyy') ");
 		}
 		sbQuery.append(" 		AND issu.id_issuer_pk = $P{issuer} ");
 		sbQuery.append(" 		AND ($P{security_code} IS NULL or secu.ID_SECURITY_CODE_PK = $P{security_code}) ");
 		sbQuery.append(" 		AND hab.total_balance>0 ");
 		
 		sbQuery.append(" 	GROUP BY ");
 		sbQuery.append(" 		SECU.CURRENCY, ");
 		sbQuery.append(" 		SECU.SECURITY_CLASS ");
 		
 		sbQuery.append(" 	ORDER BY ");
 		sbQuery.append(" 		SECU.CURRENCY, ");
 		sbQuery.append(" 		MNEMONIC_CLASS ");
     	
     	return sbQuery.toString();
     }
     
     
     /**
      * issue 1200: query para el reporte de REPORTE DE CODIGO UNICO DE IDENTIFICACION POI (325)
      * @param listHoldersCuiReportTo
      * @return
      */
	public String getQueryListHoldersCui(
			ListHoldersCuiReportTO holdersCuiReportTo) {
		StringBuilder sb = new StringBuilder();

		sb.append(" SELECT HOL.* ");
		sb.append(" FROM ( ");
		sb.append(" SELECT ");
		sb.append(" TO_CHAR(H.REGISTRY_DATE,'dd/MM/yyyy') fecha_registro ");
		sb.append(" ,(SELECT MNEMONIC ");
		sb.append(" FROM PARTICIPANT ");
		sb.append(" WHERE ID_PARTICIPANT_PK = H.ID_PARTICIPANT_FK) participante ");
		sb.append(" ,H.ID_HOLDER_PK cui ");
		sb.append(" ,(SELECT INDICATOR1 ");
		sb.append(" FROM PARAMETER_TABLE ");
		sb.append(" WHERE PARAMETER_TABLE_PK = H.DOCUMENT_TYPE) tipo_documento ");
		sb.append(" ,H.DOCUMENT_NUMBER numero_documento ");
		sb.append(" ,H.FULL_NAME descripcion_titular ");
		sb.append(" ,(SELECT PARAMETER_NAME ");
		sb.append(" FROM PARAMETER_TABLE ");
		sb.append(" WHERE PARAMETER_TABLE_PK = H.SEX) genero ");
		sb.append(" ,(extract(year from to_date(:dateEnd, 'dd/mm/yyyy') ) - extract(year from H.BIRTH_DATE)) edad ");
		sb.append(" ,(SELECT PARAMETER_NAME ");
		sb.append(" FROM PARAMETER_TABLE ");
		sb.append(" WHERE PARAMETER_TABLE_PK=H.HOLDER_TYPE) tipo_titular ");
		sb.append(" ,(SELECT PARAMETER_NAME ");
		sb.append(" FROM PARAMETER_TABLE ");
		sb.append(" WHERE PARAMETER_TABLE_PK =H.ECONOMIC_ACTIVITY) actividad_economica ");
		sb.append(" ,(SELECT PARAMETER_NAME ");
		sb.append(" FROM PARAMETER_TABLE ");
		sb.append(" WHERE PARAMETER_TABLE_PK = H.STATE_HOLDER) estado ");
		sb.append(" ,(case (select count(*) ");
		sb.append(" from HOLDER_ACCOUNT_BALANCE HAB ");
		sb.append(" where ID_HOLDER_ACCOUNT_PK in (select ha.ID_HOLDER_ACCOUNT_PK ");
		sb.append(" from HOLDER_ACCOUNT ha join HOLDER_ACCOUNT_DETAIL had ");
		sb.append(" on ha.ID_HOLDER_ACCOUNT_PK = had.ID_HOLDER_ACCOUNT_FK and had.ID_HOLDER_FK = H.ID_HOLDER_PK) ");
		sb.append(" and (HAB.TOTAL_BALANCE>0 or HAB.REPORTED_BALANCE>0)) when 0 then 'NO' else 'SI' end) mantiene_valores_cartera ");
		sb.append(" ,(SELECT COUNT(*)  FROM HOLDER_ACCOUNT_DETAIL HAD WHERE  HAD.ID_HOLDER_FK = H.ID_HOLDER_PK) cuentas_titular ");
		sb.append(" FROM HOLDER H ");
		sb.append(" WHERE 1=1 ");

		if (Validations.validateIsNotNullAndNotEmpty(holdersCuiReportTo
				.getDocType()))
			sb.append(" AND H.DOCUMENT_TYPE = :docType ");

		if (Validations.validateIsNotNullAndNotEmpty(holdersCuiReportTo
				.getParticipant()))
			sb.append(" AND H.ID_PARTICIPANT_FK = :participant ");

		if (Validations.validateIsNotNullAndNotEmpty(holdersCuiReportTo
				.getDocNumber()))
			sb.append(" AND H.DOCUMENT_NUMBER = :docNumber ");

		if (Validations.validateIsNotNullAndNotEmpty(holdersCuiReportTo
				.getStateHolder()))
			sb.append(" AND H.STATE_HOLDER = :stateHolder ");

		if (Validations.validateIsNotNullAndNotEmpty(holdersCuiReportTo
				.getTypePerson()))
			sb.append(" AND H.HOLDER_TYPE = :typePerson ");

		if (Validations.validateIsNotNullAndNotEmpty(holdersCuiReportTo
				.getEconomicActivity()))
			sb.append(" AND H.ECONOMIC_ACTIVITY = :economicActivity ");

		if (Validations.validateIsNotNullAndNotEmpty(holdersCuiReportTo
				.getDateInitial())
				&& Validations.validateIsNotNullAndNotEmpty(holdersCuiReportTo
						.getDateEnd()))
			sb.append(" AND trunc(H.REGISTRY_DATE) between to_date(:dateInitial, 'dd/mm/yyyy') and to_date(:dateEnd, 'dd/mm/yyyy') ");

		sb.append(" ) HOL ");
		sb.append(" WHERE 1=1 ");

		if (Validations.validateIsNotNullAndNotEmpty(holdersCuiReportTo
				.getSecuritiesBalance()))
			sb.append(" AND HOL.mantiene_valores_cartera = :securitiesBalance ");

		sb.append(" ORDER BY HOL.fecha_registro,HOL.cui ASC ");

		String strQueryFormatted = sb.toString();
		if (Validations.validateIsNotNullAndNotEmpty(holdersCuiReportTo
				.getDocType()))
			strQueryFormatted = strQueryFormatted.replace(":docType",
					holdersCuiReportTo.getDocType().toString());

		if (Validations.validateIsNotNullAndNotEmpty(holdersCuiReportTo
				.getParticipant()))
			strQueryFormatted = strQueryFormatted.replace(":participant",
					holdersCuiReportTo.getParticipant().toString());

		if (Validations.validateIsNotNullAndNotEmpty(holdersCuiReportTo
				.getDocNumber()))
			strQueryFormatted = strQueryFormatted.replace(":docNumber",
					"'"+holdersCuiReportTo.getDocNumber().toString()+"'");

		if (Validations.validateIsNotNullAndNotEmpty(holdersCuiReportTo
				.getStateHolder()))
			strQueryFormatted = strQueryFormatted.replace(":stateHolder",
					holdersCuiReportTo.getStateHolder().toString());

		if (Validations.validateIsNotNullAndNotEmpty(holdersCuiReportTo
				.getSecuritiesBalance()))
			strQueryFormatted = strQueryFormatted.replace(":securitiesBalance",
					"'"+holdersCuiReportTo.getSecuritiesBalance().toString()+"'");

		if (Validations.validateIsNotNullAndNotEmpty(holdersCuiReportTo
				.getTypePerson()))
			strQueryFormatted = strQueryFormatted.replace(":typePerson",
					holdersCuiReportTo.getTypePerson().toString());

		if (Validations.validateIsNotNullAndNotEmpty(holdersCuiReportTo
				.getEconomicActivity()))
			strQueryFormatted = strQueryFormatted.replace(":economicActivity",
					holdersCuiReportTo.getEconomicActivity().toString());

		if (Validations.validateIsNotNullAndNotEmpty(holdersCuiReportTo
				.getDateInitial())
				&& Validations.validateIsNotNullAndNotEmpty(holdersCuiReportTo
						.getDateEnd())) {
			strQueryFormatted = strQueryFormatted.replace(":dateInitial", "'"
					+ holdersCuiReportTo.getDateInitial() + "'");
			strQueryFormatted = strQueryFormatted.replace(":dateEnd", "'"
					+ holdersCuiReportTo.getDateEnd() + "'");
		}else{
			strQueryFormatted = strQueryFormatted.replace(":dateEnd", "'"
					+ CommonsUtilities.convertDatetoString(new Date()) + "'");
		}

		return strQueryFormatted;

	}
	
	public List<Object[]> getQueryListByClass(String strQuery) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(strQuery);
		Query query = em.createNativeQuery(sbQuery.toString());
		return query.getResultList();
	}
	
	public String getSecuritiesDirectSaleQuery(String lstDocNumber,String initialDate,String finalDate,String idSecurityCodePk,Integer stateSecurity){
     	StringBuilder sbQuery = new StringBuilder(); 		
 		sbQuery.append(" SELECT DISTINCT CO.REGISTRY_DATE, AAO.ID_SECURITY_CODE_FK,HOL.FULL_NAME,HOL.ID_HOLDER_PK                          ");
 		sbQuery.append(" ,(SELECT ACCOUNT_NUMBER FROM HOLDER_ACCOUNT WHERE ID_HOLDER_ACCOUNT_PK = HAD.ID_HOLDER_ACCOUNT_FK) AS ID_HOLDER_ACCOUNT_FK ");
 		sbQuery.append(" ,HOL.DOCUMENT_NUMBER,AAO.TOTAL_BALANCE,SEC.INITIAL_NOMINAL_VALUE                            ");
 		sbQuery.append(" ,(SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = SEC.STATE_SECURITY) STATE                       ");
 		sbQuery.append(" ,SEC.STATE_SECURITY	");
 		sbQuery.append(" FROM ACCOUNT_ANNOTATION_OPERATION AAO                                                                                ");
 		sbQuery.append(" INNER JOIN HOLDER_ACCOUNT_DETAIL HAD on AAO.ID_HOLDER_ACCOUNT_FK = HAD.ID_HOLDER_ACCOUNT_FK                          ");
 		sbQuery.append(" INNER JOIN HOLDER HOL on HAD.ID_HOLDER_FK = HOL.ID_HOLDER_PK                                                         ");
 		sbQuery.append(" INNER JOIN SECURITY SEC on AAO.ID_SECURITY_CODE_FK = SEC.ID_SECURITY_CODE_PK                                         ");
 		sbQuery.append(" INNER JOIN CUSTODY_OPERATION CO on AAO.ID_ANNOTATION_OPERATION_PK = CO.ID_CUSTODY_OPERATION_PK                       "); 		
 		sbQuery.append(" WHERE  1=1                                                                                                           ");
 		sbQuery.append(" AND SEC.SECURITY_CLASS = 415                                                                                    	  ");
 		sbQuery.append(" AND AAO.STATE_ANNOTATION = 858                                                                                  	  ");
 		sbQuery.append(" AND HOL.DOCUMENT_NUMBER IN ("+lstDocNumber+")			                                                         	  ");
 		if(Validations.validateIsNotNullAndNotEmpty(idSecurityCodePk))
 			sbQuery.append(" AND SEC.ID_SECURITY_CODE_PK = '"+idSecurityCodePk+"' ");
 		if(Validations.validateIsNotNullAndPositive(stateSecurity))
 			sbQuery.append(" AND SEC.STATE_SECURITY = "+stateSecurity+" ");
 		sbQuery.append(" AND TRUNC(CO.REGISTRY_DATE) BETWEEN TO_DATE('"+initialDate+"','DD/MM/YYYY') and TO_DATE('"+finalDate+"','DD/MM/YYYY') ");
 		//sbQuery.append(" AND TRUNC(AAO.EXPEDITION_DATE) BETWEEN TO_DATE('"+initialDate+"','DD/MM/YYYY') and TO_DATE('"+finalDate+"','DD/MM/YYYY') ");
 		sbQuery.append(" ORDER BY CO.REGISTRY_DATE                                                                                               ");
     	return sbQuery.toString();
     	
     }
     
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                