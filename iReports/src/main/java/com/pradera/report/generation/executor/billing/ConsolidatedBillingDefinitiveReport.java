package com.pradera.report.generation.executor.billing;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.model.billing.BillingEconomicActivity;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.type.BaseCollectionType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.billing.service.BillingServiceReportServiceBean;
import com.pradera.report.generation.service.ReportUtilServiceBean;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ConsolidatedBillingDefinitiveReport.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@ReportProcess(name="ConsolidatedBillingDefinitiveReport")
public class ConsolidatedBillingDefinitiveReport extends GenericReport{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	/** The report util service. */
	@EJB
	ReportUtilServiceBean reportUtilService;
	
	/** The billin report service. */
	@Inject
	private BillingServiceReportServiceBean billinReportService;
	
	/** The parameter service. */
	@EJB
	private ParameterServiceBean parameterService;
	
	/** Parameter outPut Billing. */
	private static int INDEX_NUMCXC= 0;
	
	/** The index account. */
	private static int INDEX_ACCOUNT=1;
	
	/** The index client. */
	private static int INDEX_CLIENT=2;
	
	/** The index description. */
	private static int INDEX_DESCRIPTION=3;
	
	/** The index impuesto. */
	private static int INDEX_IMPUESTO=4;
	
	/** The index moneda. */
	private static int INDEX_MONEDA=5;
	
	/** The index cantidad. */
	private static int INDEX_CANTIDAD=6;
	
	/** The index precio. */
	private static int INDEX_PRECIO=7;
	
	/** The index fecobro. */
	private static int INDEX_FECOBRO=8;
	
	/** The index tiposervicio. */
	private static int INDEX_TIPOSERVICIO=9;
	
	/** The index ref aux. */
	private static int INDEX_REF_AUX=10;

	/** The index ref aux. */
	private static int INDEX_BASE_COLLECTION=11;
	
	/** The index ref aux. */
	private static int INDEX_ENTITY_DESCRIPTION=12;
	
	/** The  Index Mnemonic Holder*/
	private static int INDEX_MNEMONIC_HOLDER=13;
	
	/** The  Index Mnemonic Holder*/
	private static int INDEX_ID_BILLING_SERVICE_PK=14;
	
	/** The  Index Mnemonic Holder*/
	private static int INDEX_SERVICE_CODE=15;
	
	/**  The description AFP Tarifa 1*/
	private static int INDEX_DESCRIPTION_TARIFA_ONE_AFP=16;
	
	/**  The description AFP Tarifa 1*/
	private static int INDEX_HOLDER_TARIFA_ONE_AFP=17;
	
	/**  The description AFP Tarifa 1*/
	private static String MNEMONIC_GPS="GPS";
	
	/**  The Constant DATE_PATTERN_BILLING *. */
	public static final String DATE_PATTERN_BILLING = "yyyyMMdd";
	
	/**
	 * Instantiates a new consolidated billing definitive report.
	 */
	public ConsolidatedBillingDefinitiveReport(){
		
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#getCustomJasperParameters()
	 */
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Long processBillingCalculationPk=null;
		
		Map<String,Object> parametersRequired = new HashMap<>();


		try {
			for(ReportLoggerDetail loggerDetail : getReportLogger().getReportLoggerDetails()) {
				
				if(loggerDetail.getFilterName().equals(ReportConstant.PROCESS_BILLING_CALCULATION_PK)){
					//SET THE PARAMETER [PROCESS BILLING PK]
					if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
						
						processBillingCalculationPk=Long.parseLong(loggerDetail.getFilterValue());
						
					}
				}

			}
			ByteArrayOutputStream byteArrayFile = this.generateFileXml(processBillingCalculationPk);
			reportUtilService.saveFileXMlInterfaceBilling(byteArrayFile,processBillingCalculationPk, getReportLogger());

		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		parametersRequired.put(ReportConstant.PROCESS_BILLING_CALCULATION_PK, processBillingCalculationPk);
		
		
		return parametersRequired;
	}
	
	/**
	 * Generate file xml.
	 *
	 * @param processBillingCalculationPk the process billing calculation pk
	 * @return the byte array output stream
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public ByteArrayOutputStream generateFileXml(Long processBillingCalculationPk) throws IOException{

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ByteArrayOutputStream baosReturn = new ByteArrayOutputStream();
		try {
			List<Object[]> billing=this.billinReportService.getAccountReceivable(processBillingCalculationPk);
			XMLOutputFactory xof = XMLOutputFactory.newInstance();
			XMLStreamWriter xmlsw;

			xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(baos));

			//open document file

//			xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");
			

			//root tag of report
			/**
			 * SERVICES
			 * */
			xmlsw.writeStartElement(ReportConstant.TAG_SOAP_START);
			createTagString(xmlsw,ReportConstant.TAG_SOAP_HEADER, GeneralConstants.EMPTY_STRING);
			xmlsw.writeStartElement(ReportConstant.TAG_SOAP_BODY);
			xmlsw.writeStartElement(ReportConstant.TAG_SOAP_ServFactura);
			xmlsw.writeStartElement(ReportConstant.TAG_SOAP_facturaC);


		if(!billing.isEmpty()) {
			//Method create body of File XML
			createBodyReport(xmlsw,  billing);

		} else {
			//when not exist data we need to create empty structure xml
//			xmlsw.writeStartElement(ReportConstant.TAG_CXC);
			createTagString(xmlsw, ReportConstant.TAG_SOAP_CLIENTE,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, ReportConstant.TAG_SOAP_CLIENTE_FACTURAR,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, ReportConstant.TAG_SOAP_CUENTA,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, ReportConstant.TAG_SOAP_DESCRIPCION, GeneralConstants.EMPTY_STRING  );
			xmlsw.writeStartElement(ReportConstant.TAG_SOAP_Detalle);
			xmlsw.writeStartElement(ReportConstant.TAG_SOAP_FacturaD);
			createTagString(xmlsw, ReportConstant.TAG_SOAP_CANTIDAD,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, ReportConstant.TAG_SOAP_ITEM,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, ReportConstant.TAG_SOAP_PRECIO,  GeneralConstants.EMPTY_STRING );
			xmlsw.writeEndElement();
			xmlsw.writeEndElement();
			createTagString(xmlsw, ReportConstant.TAG_SOAP_FECOBRO,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, ReportConstant.TAG_SOAP_IMPUESTO,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, ReportConstant.TAG_SOAP_MONEDA,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, ReportConstant.TAG_SOAP_NUMCXC,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, ReportConstant.TAG_SOAP_NUMFACT,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, ReportConstant.TAG_SOAP_REF_AUX,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, ReportConstant.TAG_SOAP_TIPOSERVICIO,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, ReportConstant.TAG_SOAP_TOKKEN,  GeneralConstants.EMPTY_STRING );
//			xmlsw.writeEndElement();
		}

		//close report tag SERVICES
		xmlsw.writeEndElement();
		xmlsw.writeEndElement();
		xmlsw.writeEndElement();
		xmlsw.writeEndElement();
		//close document
//		xmlsw.writeEndDocument();
		xmlsw.flush();
        xmlsw.close();
        

        String replaceQuote=baos.toString();
        replaceQuote=replaceQuote.replace("<"+ReportConstant.TAG_SOAP_START,"<"+ReportConstant.TAG_SOAP_START.concat(ReportConstant.TAG_SOAP_START_CONTENT));
        replaceQuote=replaceQuote.replace("></SOAP-ENV:Header>", "/>");
        replaceQuote=replaceQuote.replace("'", "\"");
        replaceQuote=replaceQuote.replace(" - AFP, CARTERA CLIENTES", " - AFP");
        replaceQuote=replaceQuote.replace(" - AFP, CARTERA PROPIA", " - AFP");
        baosReturn.write(replaceQuote.getBytes());
		
		} catch (XMLStreamException ex) {
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		return baosReturn;
	}

	
	
	/**
	 * Creates the body report.
	 * good practice factoring
	 *
	 * @param xmlsw the xmlsw
	 * @param billings the billings
	 * @throws XMLStreamException the xML stream exception
	 */
	public void createBodyReport(XMLStreamWriter xmlsw,	List<Object[]> billings)  throws XMLStreamException{
		int i=1;
		ParameterTable tokken = billinReportService.find(ParameterTable.class,2538 );
		for(Object[] billing : billings) {
			Long idBillignServicePk=billing[INDEX_ID_BILLING_SERVICE_PK]!=null?new Long(billing[INDEX_ID_BILLING_SERVICE_PK].toString()):0l;
			BillingService billingService = billinReportService.find(BillingService.class,idBillignServicePk );
			List<BillingEconomicActivity> listEconActBd=billinReportService.getBillingEconomicActivityByBillingServicePk(billingService.getIdBillingServicePk());
			List<Integer> listEconomicAct=billinReportService.getBillingEconomicActivityListInteger(listEconActBd);
			Integer portFolio=billingService.getPortfolio();
			String mnemonicHolder=null;
			
			//Validar si el cliente a Facturar es GPS no facturar al TGN
			if(BillingServiceReportServiceBean.isAfpPortfolioClient(listEconomicAct, portFolio)&&
					!(MNEMONIC_GPS.equals(billing[INDEX_MNEMONIC_HOLDER].toString().trim())) && 
					(billingService.getServiceCode().equals("71")||(billingService.getServiceCode().equals("72")))) {
				mnemonicHolder="TGN";
			}else {
				mnemonicHolder=billing[INDEX_MNEMONIC_HOLDER].toString().trim();
			}
			Integer baseCollection=Integer.parseInt(billing[INDEX_BASE_COLLECTION].toString()) ;
//			xmlsw.writeStartElement(ReportConstant.TAG_CXC);

			xmlsw.writeStartElement(ReportConstant.TAG_SOAP_IED_FACTURAC);
			//NEW dETAIL REPORT
			createTagString(xmlsw, ReportConstant.TAG_SOAP_CLIENTE,  billing[INDEX_CLIENT].toString().trim() );
			createTagString(xmlsw, ReportConstant.TAG_SOAP_CLIENTE_FACTURAR,  mnemonicHolder );
			createTagString(xmlsw, ReportConstant.TAG_SOAP_CUENTA,  billing[INDEX_ACCOUNT].toString().trim().concat(GeneralConstants.DOT.concat(billing[INDEX_CLIENT].toString().trim())) );
			/**
			 * Issuer 23 Mantis EDV, Incluir en el Tag de Descripcion del archivo 
			 * de Servicios, el Nemonico del Participante, CUI y Descripcion del Titular.
			 * Este cambio para los Servicios 3 y 4 para AFPs.
			 */
			if(BaseCollectionType.RECORD_BOOK_ENTRY_SECURITIES_FIXED_INCOME_AFP.getCode().equals(baseCollection)||
					BaseCollectionType.RECORD_BOOK_ENTRY_SECURITIES_VARIABLE_INCOME_AFP.getCode().equals(baseCollection)){
				createTagString(xmlsw, ReportConstant.TAG_SOAP_DESCRIPCION,  billing[INDEX_DESCRIPTION].toString().concat(GeneralConstants.TWO_POINTS).concat(billing[INDEX_CLIENT].toString().trim()).concat(GeneralConstants.DASH).concat(billing[INDEX_ENTITY_DESCRIPTION].toString()) );
			}else{
				StringBuilder sb=new StringBuilder();
				sb.append(billing[INDEX_DESCRIPTION]);
				Object descriptionTarifa1=billing[INDEX_DESCRIPTION_TARIFA_ONE_AFP];
				if(BillingServiceReportServiceBean.isBaseCollectionOneAfp(listEconomicAct, baseCollection) &&
						(descriptionTarifa1!=null && descriptionTarifa1.toString().trim().length()>20)) {
					sb.append(GeneralConstants.TWO_POINTS);
					if(billingService.getServiceCode().equalsIgnoreCase("69") || billingService.getServiceCode().equalsIgnoreCase("70")){
						sb.append(billing[INDEX_CLIENT]);
						sb.append(" - ");
						sb.append(billing[INDEX_HOLDER_TARIFA_ONE_AFP]);
					} else sb.append(billing[INDEX_DESCRIPTION_TARIFA_ONE_AFP]);
				}
				createTagString(xmlsw, ReportConstant.TAG_SOAP_DESCRIPCION,  sb.toString() );
			}
			xmlsw.writeStartElement(ReportConstant.TAG_SOAP_Detalle);
			xmlsw.writeStartElement(ReportConstant.TAG_SOAP_FacturaD);
			createTagString(xmlsw, ReportConstant.TAG_SOAP_CANTIDAD,  billing[INDEX_CANTIDAD] );
			createTagString(xmlsw, ReportConstant.TAG_SOAP_ITEM,  billing[INDEX_SERVICE_CODE] );//Agregar T00XX
			createTagString(xmlsw, ReportConstant.TAG_SOAP_PRECIO,  billing[INDEX_PRECIO] );
			xmlsw.writeEndElement();
			xmlsw.writeEndElement();
			createTagString(xmlsw, ReportConstant.TAG_SOAP_FECOBRO,  billing[INDEX_FECOBRO] );
			createTagString(xmlsw, ReportConstant.TAG_SOAP_IMPUESTO,  billing[INDEX_IMPUESTO] );
			createTagString(xmlsw, ReportConstant.TAG_SOAP_MONEDA,  billing[INDEX_MONEDA] );
			createTagString(xmlsw, ReportConstant.TAG_SOAP_NUMCXC,  i );
			createTagString(xmlsw, ReportConstant.TAG_SOAP_NUMFACT, billing[INDEX_NUMCXC] );
			createTagString(xmlsw, ReportConstant.TAG_SOAP_REF_AUX,  billing[INDEX_REF_AUX] );
			createTagString(xmlsw, ReportConstant.TAG_SOAP_TIPOSERVICIO,  billing[INDEX_TIPOSERVICIO] );
			createTagString(xmlsw, ReportConstant.TAG_SOAP_TOKKEN,  tokken.getDescription() );
			
			i++;
			xmlsw.writeEndElement();
		}
		
	}


	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#generateData(com.pradera.model.report.ReportLogger)
	 */
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

}
