package com.pradera.report.generation.executor.account;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.account.service.PositionsPartAccountServiceBean;
import com.pradera.report.generation.executor.account.to.PositionsParticipantAccountTO;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name="PositionsParticipantAccountReport")
public class PositionsParticipantAccountReport extends GenericReport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject  PraderaLogger log;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	private PositionsPartAccountServiceBean positionsPartAccountServiceBean;
	
	@EJB
	CustodyReportServiceBean custodyReportServiceBean;

	public PositionsParticipantAccountReport() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void addParametersQueryReport() {
	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		PositionsParticipantAccountTO positionParticipantTO = new PositionsParticipantAccountTO();
		boolean excelDelimited = false;
		
		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("participant")){
				if (listaLogger.get(i).getFilterValue()!=null && !listaLogger.get(i).getFilterValue().equals("0")){
					positionParticipantTO.setParticipant(new Long(listaLogger.get(i).getFilterValue()).longValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("issuer")){
				if (listaLogger.get(i).getFilterValue()!=null && !listaLogger.get(i).getFilterValue().equals("0")){
					positionParticipantTO.setIssuer(listaLogger.get(i).getFilterValue().toString());
				}
			}
			
			if(listaLogger.get(i).getFilterName().equals("security_class")){
				if (listaLogger.get(i).getFilterValue()!=null && !listaLogger.get(i).getFilterValue().equals("0")){
					positionParticipantTO.setSecurityClass(listaLogger.get(i).getFilterValue().toString());
				}
			}
			
			if(listaLogger.get(i).getFilterName().equals("court_date")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					positionParticipantTO.setCourtDate(CommonsUtilities.convertStringtoDate(listaLogger.get(i).getFilterValue()));
				}
			}
			
			if(listaLogger.get(i).getFilterName().equals("reportFormats")) {
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()) &&
						Integer.valueOf(listaLogger.get(i).getFilterValue()) == 2853) {
					excelDelimited = true;
				}
			}
		}
		
		Long idStkCalcProcess= null;
		
		String strQuery = positionsPartAccountServiceBean.QueryPositionParticipantAccount(positionParticipantTO, idStkCalcProcess);

		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> currency = new HashMap<Integer, String>();
		Map<Integer,String> descripction = new HashMap<Integer, String>();
		Map<Integer,String> securityClass = new HashMap<Integer, String>();
		Map<Integer,String> securityClassDescription = new HashMap<Integer, String>();
		Map<Integer,String> dematerializationMotive = new HashMap<Integer, String>(); 
		try {
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				currency.put(param.getParameterTablePk(), "(" + param.getDescription() + ")" + param.getParameterName());
			}
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				securityClass.put(param.getParameterTablePk(), "(" + param.getText1() + ")" + param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.DEMATERIALIZATION_MOTIVE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				dematerializationMotive.put(param.getParameterTablePk(), param.getDescription());
			}
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		parametersRequired.put("str_query", strQuery);
		parametersRequired.put("p_currency", currency);
		parametersRequired.put("p_securityClass", securityClass);
		parametersRequired.put("p_dematerializationMotive", dematerializationMotive);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());

		if(excelDelimited) {
			parametersRequired.put(ReportConstant.REPORT_EXCEL_DELIMITADO, positionsPartAccountServiceBean.generateExcelDelimitated(parametersRequired, positionParticipantTO, idStkCalcProcess));
		}
		
		return parametersRequired;
	}

}
