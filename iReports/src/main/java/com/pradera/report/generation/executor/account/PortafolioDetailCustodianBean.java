package com.pradera.report.generation.executor.account;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.commons.view.ViewDepositaryBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.HolderHelperOutputTO;
import com.pradera.core.component.helperui.to.SecuritiesSearcherTO;
import com.pradera.core.component.helperui.to.SecuritiesTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.type.EconomicActivityType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.report.generation.executor.account.service.AccountReportServiceBean;
import com.pradera.report.generation.executor.account.to.PortafolioDetailCustodianTO;
import com.pradera.report.generation.facade.ReportGenerationLocalServiceFacade;
import com.pradera.report.util.view.PropertiesConstants;

@ViewDepositaryBean
@LoggerCreateBean
public class PortafolioDetailCustodianBean extends GenericBaseBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ParameterServiceBean parameterService;
	@EJB
	private AccountReportServiceBean accountReportService;
	@EJB
	private HelperComponentFacade helperComponentFacade;
	@EJB
	private ReportGenerationLocalServiceFacade reportGenerationFacade;
	@Inject
	private UserInfo userInfo;	
	@Inject
	Instance<PortafolioDetailCustodianReport>  pdcReport;
	
	private static final String CONFIRM_DIALOG_SUCCESS=":frmCustodyDetail:reportGenerationOk";
	
	private ReportUser reportUser;
	private List<ParameterTable> reportFormats;
	private List<ParameterTable> lstEconomicActivity;
	private List<HolderHelperOutputTO> lstAllFundType;
	private List<HolderHelperOutputTO> lstFoundCUI;
	private HolderHelperOutputTO holderHelper;
	private String FCC="FCC";
	private String FCI="FCI";
	private String FRD="FRD";
	private boolean blAFPDisabled;
	private boolean blInsuranceDisabled;
	private Long reportId=new Long(4);
	
	public PortafolioDetailCustodianBean() {
		blAFPDisabled=true;
		blInsuranceDisabled=true;
		SecuritiesTO security= new SecuritiesTO();
		holderHelper = new HolderHelperOutputTO();
		holderHelper.setDate(new Date());
		holderHelper.setSecurityTO(security);
	}
	
	@PostConstruct
	public void init(){
		loadActivityEconomic();
		loadReportFormat();
		holderHelper.setLstFundType(fillFunds());
		getFundCUI();
		reportUser= new ReportUser();
	}

	public void loadReportFormat(){
		ParameterTableTO parameterFilter = new ParameterTableTO();
		parameterFilter.setState(BooleanType.YES.getCode());
		List<Integer> lstParamPk = new ArrayList<Integer>();
		lstParamPk.add(ReportFormatType.TXT.getCode());
		lstParamPk.add(ReportFormatType.PDF.getCode());
		parameterFilter.setLstParameterTablePk(lstParamPk);
		reportFormats = parameterService.getListParameterTableServiceBean(parameterFilter);
	}
	
	public void loadActivityEconomic(){
		List<Integer> lstPks= fillEconomicActivity();
		ParameterTableTO parameterFilter = new ParameterTableTO();
		parameterFilter.setState(BooleanType.YES.getCode());
		parameterFilter.setLstParameterTablePk(lstPks);
		lstEconomicActivity = parameterService.getListParameterTableServiceBean(parameterFilter);
		holderHelper.setLstEconomicActivity(lstPks);
	}
	
	public void getFunds(){
		lstAllFundType = new ArrayList<HolderHelperOutputTO>();
		List<HolderHelperOutputTO> listFundTypes = new ArrayList<HolderHelperOutputTO>();
		HolderHelperOutputTO fundType= new HolderHelperOutputTO();
		if(EconomicActivityType.ADMINISTRADORAS_FONDOS_PENSIONES.getCode().equals(this.holderHelper.getEconomicActivity())){
			fundType.setLstFundType(fillFunds());
			blAFPDisabled=false;
			blInsuranceDisabled=false;
			//fill static comboBox
			for(String ft: fundType.getLstFundType()){
				fundType= new HolderHelperOutputTO();
				fundType.setEconomicActivity(EconomicActivityType.ADMINISTRADORAS_FONDOS_PENSIONES.getCode());
				fundType.setStrFundType(ft);
				listFundTypes.add(fundType);
			}
			lstAllFundType.addAll(listFundTypes);
			holderHelper.setLstEconomicActivity(null);
			holderHelper.setLstFundType(null);
			getFundCUI();
		}else if(EconomicActivityType.COMPANIAS_SEGURO.getCode().equals(this.holderHelper.getEconomicActivity())){
			holderHelper.setEconomicActivity(EconomicActivityType.COMPANIAS_SEGURO.getCode());
			holderHelper.setLstEconomicActivity(null);
			holderHelper.setLstFundType(null);
			holderHelper.setStrFundType(null);
			blAFPDisabled=false;
			blInsuranceDisabled=true;
			getFundCUI();
		}else{
			blAFPDisabled=true;
			blInsuranceDisabled=true;
			holderHelper.setLstEconomicActivity(fillEconomicActivity());
		}
	}
	
	public List<Integer> fillEconomicActivity(){
		List<Integer> lstPks= new ArrayList<Integer>();
		lstPks.add(EconomicActivityType.ADMINISTRADORAS_FONDOS_PENSIONES.getCode());
		lstPks.add(EconomicActivityType.COMPANIAS_SEGURO.getCode());
		
		return lstPks;
	}
	
	private List<String> fillFunds(){
		List<String> lstFunds= new ArrayList<String>();
		
		lstFunds.add(FCC);
		lstFunds.add(FCI);
		lstFunds.add(FRD);

		return lstFunds;
	}

	public void getFundCUI(){
		lstFoundCUI = new ArrayList<HolderHelperOutputTO>();
		holderHelper.setFundCUI(null);
		try {
			List<Object[]> list = accountReportService.getHoldersByFilter(holderHelper);
			HolderHelperOutputTO hhoTO= null;
			for(Object[] temp: list){
				hhoTO = new HolderHelperOutputTO(); 
				if(temp[0]!=null){
					hhoTO.setStrFundType(temp[0].toString());
				}
				if(temp[1]!=null){
					hhoTO.setFullName(temp[1].toString());
				}
				hhoTO.setFundCUI(Long.valueOf(temp[2].toString()));
				
				lstFoundCUI.add(hhoTO);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	public void searchSecurityByCode() throws ServiceException{
		String idSecurity = holderHelper.getSecurityTO().getSecuritiesCode();
		if(StringUtils.isNotBlank(idSecurity)){
			SecuritiesSearcherTO  searcher = new SecuritiesSearcherTO();
			searcher.setIsinCode(idSecurity);
			List<SecuritiesTO>  securityList = helperComponentFacade.findSecuritiesFromHelperNativeQuery(searcher);
			if(!securityList.isEmpty()){
				holderHelper.setSecurityTO(securityList.get(0));
			} else {
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
									PropertiesUtilities.getGenericMessage(GeneralConstants.HELPER_SECURITY_NOT_FOUND));
				JSFUtilities.showSimpleValidationDialog();
				holderHelper.getSecurityTO().setSecuritiesCode(null);
				holderHelper.getSecurityTO().setDescription(null);
			}
		} else {
			holderHelper.getSecurityTO().setSecuritiesCode(null);
			holderHelper.getSecurityTO().setDescription(null);
		}
	}
	
	@LoggerAuditWeb
	public String submitForm() {
		Map<String,String> parameters = new HashMap<String, String>();
		Map<String,String> parameterDetails = new HashMap<String, String>();
		
		reportUser.setUserName(userInfo.getUserAccountSession().getUserName());
		//how report is send for screen so for default send notification
		reportUser.setShowNotification(BooleanType.YES.getCode());
		try {
			if(Validations.validateIsNotNullAndPositive(reportId)){
				if(Validations.validateIsNullOrEmpty(holderHelper.getStrFundType()) && EconomicActivityType.AFP.getCode().equals(holderHelper.getEconomicActivity())){
					holderHelper.setLstFundType(fillFunds());
				}else if(Validations.validateIsNullOrNotPositive(holderHelper.getEconomicActivity())){
					holderHelper.setLstEconomicActivity(fillEconomicActivity());
				}
				List<Object[]> list = accountReportService.getHoldersByFilter(holderHelper);
				HolderHelperOutputTO hhoTO= null;
				for(Object[] temp: list){
					hhoTO = new HolderHelperOutputTO(); 
					if(temp[0]!=null){
						hhoTO.setStrFundType(temp[0].toString());
					}
					if(temp[1]!=null){
						hhoTO.setFullName(temp[1].toString());
					}
					hhoTO.setFundCUI(Long.valueOf(temp[2].toString()));
					if(temp[3]!=null){
						hhoTO.setEconomicActivity(new BigDecimal(temp[3].toString()).intValue());
					}
					if(temp[4]!=null){
						hhoTO.setDocumentType(((BigDecimal)temp[4]).intValue());
					}
					if(temp[5]!=null){
						hhoTO.setDocumentNumber(temp[5].toString());
					}
					if(temp[6]!=null){
						hhoTO.setTransferNumber(temp[6].toString());
					}
					hhoTO.setSecurityTO(new SecuritiesTO());
					hhoTO.getSecurityTO().setSecuritiesCode(holderHelper.getSecurityTO().getSecuritiesCode());
					hhoTO.setDate(holderHelper.getDate());
					parameters = getCustomJasperParameters(hhoTO);
					reportGenerationFacade.saveReportExecution(reportId, parameters, parameterDetails, reportUser);
				}
				if(list.isEmpty()){//send an empty map parameters
					parameters = getCustomJasperParameters(holderHelper);
					reportGenerationFacade.saveReportExecution(reportId, parameters, parameterDetails, reportUser);
				}
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
												PropertiesUtilities.getGenericMessage(GeneralConstants.REPORT_GENERATED_SUCCESSFULLY));
				JSFUtilities.showSimpleValidationDialog();
//				clean();
			}else{
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(PropertiesConstants.ALERT_MESSAGE),
						PropertiesUtilities.getMessage(PropertiesConstants.REPORT_SUCESS_FAIL));
				JSFUtilities.showComponent(CONFIRM_DIALOG_SUCCESS);
			}
		} catch(Exception ex){
			ex.printStackTrace();;
		}
		return null;
	}
	
	public void clean(){
		holderHelper = new HolderHelperOutputTO();
		holderHelper.setSecurityTO(new SecuritiesTO());
		holderHelper.setDate(new Date());
		lstAllFundType = null;
		blInsuranceDisabled=true;
		init();
	}
	
	public Map<String, String> getCustomJasperParameters(HolderHelperOutputTO hhoTO) {
		PortafolioDetailCustodianTO pdcTO = new PortafolioDetailCustodianTO();
		Map<String, String> parametersRequired = new HashMap<String, String>();
		
		if(Validations.validateIsNotNullAndNotEmpty(hhoTO.getStrFundType())){
			pdcTO.setFundType(hhoTO.getStrFundType());
		}else{
			String fundTypes="";
			List<String> lstFunds=fillFunds();
			int i=1;
			for(String strFundType: lstFunds){
				if(i<lstFunds.size()){
					fundTypes+="'"+strFundType+"',";
				}else{
					fundTypes+="'"+strFundType+"'";
				}
				i++;
			}
			pdcTO.setLstFundTypes(fundTypes);//jasper
			pdcTO.setListFundTypes(fillFunds());//txt
		}
		if(hhoTO.getHolderId()!=null){
			pdcTO.setCui(hhoTO.getHolderId());
		}else if(hhoTO.getFundCUI()!=null){
			pdcTO.setCui(hhoTO.getFundCUI());
		}else{
			pdcTO.setCui(new Long(0));
		}

		pdcTO.setHolderDesc(hhoTO.getFullName());
		pdcTO.setDocumentType(hhoTO.getDocumentType());
		pdcTO.setDocumentNumber(hhoTO.getDocumentNumber());

		if(hhoTO.getEconomicActivity()!=null){
			pdcTO.setEconomicActivity(hhoTO.getEconomicActivity());
		}else{
			pdcTO.setEconomicActivity(0);
		}
		if(hhoTO.getSecurityTO().getSecuritiesCode()!=null){
			pdcTO.setIdSecurityCode(hhoTO.getSecurityTO().getSecuritiesCode());
		}
		if(hhoTO.getTransferNumber()!=null){
			//pdcTO.setTransferNumber(Integer.parseInt(hhoTO.getTransferNumber()));
			pdcTO.setTransferNumberString(hhoTO.getTransferNumber());
		}
		pdcTO.setDateMarketFact(hhoTO.getDate());
		
		//SEND PARAMETERS(REPORT FILTERS)
		if(pdcTO.getFundType()!=null){
			parametersRequired.put("fund_type", pdcTO.getFundType());
		}else {
			if(!pdcTO.getEconomicActivity().equals(EconomicActivityType.COMPANIAS_SEGURO.getCode())){
				parametersRequired.put("all_fund_types_jasper", pdcTO.getLstFundTypes().toString());
				parametersRequired.put("all_fund_types_txt", pdcTO.getListFundTypes().toString());
			}
		}
		parametersRequired.put("cui_holder", pdcTO.getCui().toString());
		parametersRequired.put("cui_description", pdcTO.getHolderDesc());
		parametersRequired.put("document_type", pdcTO.getDocumentType().toString());
		parametersRequired.put("document_number", pdcTO.getDocumentNumber());
		parametersRequired.put("security_pk", pdcTO.getIdSecurityCode());
		parametersRequired.put("economic_activity", pdcTO.getEconomicActivity().toString());
		parametersRequired.put("date", CommonsUtilities.convertDatetoString(pdcTO.getDateMarketFact()));
		parametersRequired.put("report_format", reportUser.getReportFormat().toString());
		parametersRequired.put("transfer_number", pdcTO.getTransferNumberString());
		return parametersRequired;
	}

	public List<ParameterTable> getLstEconomicActivity() {
		return lstEconomicActivity;
	}

	public void setLstEconomicActivity(List<ParameterTable> lstEconomicActivity) {
		this.lstEconomicActivity = lstEconomicActivity;
	}

	public List<HolderHelperOutputTO> getLstAllFundType() {
		return lstAllFundType;
	}

	public void setLstAllFundType(List<HolderHelperOutputTO> lstAllFundType) {
		this.lstAllFundType = lstAllFundType;
	}

	public List<HolderHelperOutputTO> getLstFoundCUI() {
		return lstFoundCUI;
	}

	public void setLstFoundCUI(List<HolderHelperOutputTO> lstFoundCUI) {
		this.lstFoundCUI = lstFoundCUI;
	}

	public HolderHelperOutputTO getHolderHelper() {
		return holderHelper;
	}

	public void setHolderHelper(HolderHelperOutputTO holderHelper) {
		this.holderHelper = holderHelper;
	}

	public boolean isBlAFPDisabled() {
		return blAFPDisabled;
	}

	public void setBlAFPDisabled(boolean blAFPDisabled) {
		this.blAFPDisabled = blAFPDisabled;
	}

	public ReportUser getReportUser() {
		return reportUser;
	}

	public void setReportUser(ReportUser reportUser) {
		this.reportUser = reportUser;
	}

	public List<ParameterTable> getReportFormats() {
		return reportFormats;
	}

	public void setReportFormats(List<ParameterTable> reportFormats) {
		this.reportFormats = reportFormats;
	}

	public boolean isBlInsuranceDisabled() {
		return blInsuranceDisabled;
	}

	public void setBlInsuranceDisabled(boolean blInsuranceDisabled) {
		this.blInsuranceDisabled = blInsuranceDisabled;
	}

}
