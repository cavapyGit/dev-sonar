package com.pradera.report.generation.facade;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.report.ReportFile;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.FileData;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.ftp.UploadFtp;
import com.pradera.core.component.swift.to.FTPParameters;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.core.framework.batchprocess.service.BatchServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerUser;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.model.report.type.ReportLoggerStateType;
import com.pradera.report.generation.service.ReportManageServiceBean;
import com.pradera.report.generation.service.ReportUtilServiceBean;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ReportGenerationLocalServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 09/09/2015
 */
@Stateless
@Interceptors(ContextHolderInterceptor.class)
public class ReportGenerationLocalServiceFacade implements Serializable{

	

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6495143512962244545L;

	/** The crud dato service bean. */
	@EJB
	CrudDaoServiceBean crudDatoServiceBean;
	
    /** The log. */
    @Inject
    PraderaLogger log;
    
    /** The transaction registry. */
    @Resource
    TransactionSynchronizationRegistry transactionRegistry;
    
    /** The report manage service. */
    @Inject
    ReportManageServiceBean reportManageService;
    
    /** The report util service. */
    @EJB
    ReportUtilServiceBean reportUtilService;
    
    /** The batch process service facade. */
    @EJB
	BatchProcessServiceFacade batchProcessServiceFacade;
    
    /** The batch service bean. */
    @EJB
    BatchServiceBean batchServiceBean;
    /**
     * Save report execution.
     *
     * @param reportId the report id
     * @param parameters the parameters
     * @param parameterDetails the parameter details
     * @param reportUser the report user who have grant to see report
     * @return the report logger
     * @throws ServiceException the service exception
     */
    public ReportLogger saveReportExecution(Long reportId,Map<String,String> parameters,Map<String,String> parameterDetails,ReportUser reportUser) throws ServiceException{
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	//register report
    	if(loggerUser.getIdPrivilegeOfSystem()==null){
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
    	}
    	ReportLogger reportLogger = reportUtilService.getReportLogger(reportId, parameters, parameterDetails);
    	//save indicator about notification
    	if(reportUser != null){
    		//report format to generate
    		if(Validations.validateIsNotNull(reportUser.getReportFormat())){
    			reportLogger.setReportFormat(reportUser.getReportFormat());
    		}else{
    			reportLogger.setReportFormat(Integer.parseInt(parameters.get("reportFormats")));
    		}
    		reportLogger.setIndSendNotification(reportUser.getShowNotification());
    	} else {
    		//if no setting format for default is pdf
    		reportLogger.setReportFormat(ReportFormatType.PDF.getCode());
    		reportLogger.setIndSendNotification(BooleanType.NO.getCode());
    	}
    	return reportManageService.saveReportExecution(reportLogger, reportUser);
    }
    
//    public Report getReport(Long idReportPk){
//    	return crudDatoServiceBean.find(Report.class, idReportPk);
//    }
    
    /**
 * Gets the data report.
 *
 * @param reportId the report id
 * @return the data report
 * @throws ServiceException the service exception
 */
public byte[] getDataReport(Long reportId) throws ServiceException {		
		return reportManageService.getDataReport(reportId);
	}
    
    /**
     * Method that is fired from AccreditationCertificatesServiceFacade in PraderaCustody
     * It used to get the Accreditation Certificate File and update the AccreditationOperation Table with that file
     * 
     *  *.
     *
     * @param reportId the report id
     * @param accreditationId the accreditation id
     * @param username the username
     * @param loggerUser the logger user
     * @throws ServiceException the service exception
     */
    public void setAccreditationCertificate(Long reportId, Long accreditationId, String username, LoggerUser loggerUser)throws ServiceException{    	
    	transactionRegistry.putResource(RegistryContextHolderType.LOGGER_USER, loggerUser);
    	ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(),loggerUser);
    	BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.ACCREDITATION_CERTIFICATE_GENERATION.getCode());
	    Map<String, Object> param = new HashMap<String, Object>();
	    //Getting the user logged
	    String userNameLogged = username;
	    
	    param.put("idAccreditationOperationPk",accreditationId);	
	    param.put("idReportPk",reportId);	
	    param.put("userNameLogged", userNameLogged);
	    //Firing the batch
	    this.batchServiceBean.registerBatchTx(userNameLogged, businessProcess, param);
    }

	/**
	 * Save report file.
	 *
	 * @param reportId the report id
	 * @param parameters the parameters
	 * @param parameterDetails the parameter details
	 * @param files the files
	 * @param reportUser the report user
	 * @return the report logger
	 * @throws ServiceException the service exception
	 */
	public ReportLogger saveReportFile(Long reportId,Map<String,String> parameters,Map<String,String> parameterDetails,
			List<ReportFile> files, ReportUser reportUser) throws ServiceException {
		
		ReportLogger reportLogger = null;
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser.getIdPrivilegeOfSystem() ==null){
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
    	}
    	
    	reportLogger = getReportLogger(reportId, parameters, parameterDetails);
    	//save indicator about notification
    	if(reportUser != null){
    		reportLogger.setIndSendNotification(reportUser.getShowNotification());
    	} else {
    		reportLogger.setIndSendNotification(BooleanType.NO.getCode());
    	}
    	return reportManageService.saveReportFile(reportLogger,files,reportUser);
	}

	/**
	 * Gets the report logger.
	 *
	 * @param reportId the report id
	 * @param parameters the parameters
	 * @param parameterDetails the parameter details
	 * @return the report logger
	 */
	public ReportLogger getReportLogger(Long reportId,Map<String,String> parameters,Map<String,String> parameterDetails){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser.getIdPrivilegeOfSystem() ==null){
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
    	}
		ReportLogger reportLogger = reportUtilService.getReportLogger(reportId, parameters, parameterDetails);
		reportLogger.setIndSendNotification(BooleanType.YES.getCode());
		//default pdf 
		reportLogger.setReportFormat(ReportFormatType.PDF.getCode());
		return reportUtilService.create(reportLogger);
	}
	
	/**
	 * Gets the report logger.
	 *
	 * @param reportId the report id
	 * @param parameters the parameters
	 * @param parameterDetails the parameter details
	 * @return the report logger
	 */
	public ReportLogger getReportLoggerAsfi(Long reportId,Map<String,String> parameters,Map<String,String> parameterDetails){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser.getIdPrivilegeOfSystem() ==null){
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
    	}
		ReportLogger reportLogger = reportUtilService.getReportLogger(reportId, parameters, parameterDetails);
		reportLogger.setIndSendNotification(BooleanType.YES.getCode());
		reportLogger.setReportState(ReportLoggerStateType.FINISHED.getCode());
		reportLogger.setReportFormat(ReportFormatType.TXT.getCode());
		
		ReportLoggerUser reportLoggerUser = new ReportLoggerUser();
		reportLoggerUser.setAccessUser(reportLogger.getRegistryUser());
		reportLoggerUser.setAssignedDate(CommonsUtilities.currentDate());
		reportLoggerUser.setAudit(loggerUser);
		reportLoggerUser.setLastModifyApp(reportLogger.getLastModifyApp());
		reportLoggerUser.setLastModifyDate(reportLogger.getLastModifyDate());
		reportLoggerUser.setLastModifyIp(reportLogger.getLastModifyIp());
		reportLoggerUser.setLastModifyUser(reportLogger.getLastModifyUser());
		reportLoggerUser.setReportLogger(reportLogger);
		
		List <ReportLoggerUser> reportLoggerUsers = new ArrayList<>();
		reportLoggerUsers.add(reportLoggerUser);
		reportLogger.setReportLoggerUsers(reportLoggerUsers);
	     
		return reportUtilService.create(reportLogger);
	}
	
    /**
     * Send modifying of data.
     *
     * @param reportId the report id
     * @param holderRequestId the holder request id
     * @param username the username
     * @param loggerUser the logger user
     * @param nameParam the name param
     * @throws ServiceException the service exception
     */
    public void sendModifyingOfData(Long reportId, Long holderRequestId, String username, LoggerUser loggerUser, String nameParam)throws ServiceException{    	
    	transactionRegistry.putResource(RegistryContextHolderType.LOGGER_USER, loggerUser);
    	BusinessProcess businessProcess = new BusinessProcess();
    	if(nameParam.equalsIgnoreCase("Holder")){
    		businessProcess.setIdBusinessProcessPk(BusinessProcessType.MODIFYING_CUI_REPORT.getCode());
    	}else {
    		businessProcess.setIdBusinessProcessPk(BusinessProcessType.MODIFYING_ACCOUNT_REPORT.getCode());
    	}
	    Map<String, Object> param = new HashMap<String, Object>();
	    //Getting the user logged
	    String userNameLogged = username;
	    
	    param.put("idHolderRequestPk",holderRequestId);	
	    param.put("idReportPk",reportId);	
	    param.put("userNameLogged", userNameLogged);
	    //Firing the batch
	    this.batchProcessServiceFacade.registerBatch(userNameLogged, businessProcess, param);
    }
    
    /**
     * Gets the data report object.
     *
     * @param reportId the report id
     * @return the data report object
     * @throws ServiceException the service exception
     */
    public Object [] getDataReportObject(Long reportId) throws ServiceException {		
		return reportManageService.getDataReportObject(reportId);
	}
    
	/**
	 * Send FTP upload.
	 *
	 * @param listFileData the list file data
	 * @param parameterTablePk the parameter table PK
	 * @return true, if successful
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public boolean sendFtpUpload(List<FileData> listFileData, Integer parameterTablePk) throws IOException {
		ParameterTable objParameterTable = reportUtilService.find(ParameterTable.class, parameterTablePk);
		FTPParameters ftpParameters = new FTPParameters();		
		ftpParameters.setServer(objParameterTable.getText3().trim());
		ftpParameters.setUser(objParameterTable.getText1().trim());
		ftpParameters.setPass(objParameterTable.getText2().trim());
		ftpParameters.setRemotePath(objParameterTable.getDescription());
		ftpParameters.setListFileData(listFileData);							
		UploadFtp upload= new UploadFtp(ftpParameters);
		return upload.registerAndSendFileMassive();		
	}
    
	/**
	 * ENVIA REPORTE CONSULTA PERSONAS
	 * 
	 * @param reportId report id
	 * @param idHolderPk holder id
	 * @param idApplicantConsultRequest applicant consult id
	 * @param holderDesc holder description
	 * @param username user execute name
	 * @param logguerUser logger user
	 * @throws servide exception
	 * */
	public void sendApplicantConsult(Long reportId, Long idHolderPk, Long idApplicantConsultRequest, String holderDesc, String username, LoggerUser loggerUser) throws ServiceException{    	
		transactionRegistry.putResource(RegistryContextHolderType.LOGGER_USER, loggerUser);
    	BusinessProcess businessProcess = new BusinessProcess();    	
    	businessProcess.setIdBusinessProcessPk(BusinessProcessType.PORTFOLIO_REPORT.getCode());
    	
	    Map<String, Object> param = new HashMap<String, Object>();	    
	    String userNameLogged = username;	    
	    param.put("idHolderPk",idHolderPk);
	    param.put("idApplicantConsultRequest", idApplicantConsultRequest);
	    param.put("holderDesc",holderDesc);
	    param.put("idReportPk",reportId);	
	    param.put("userNameLogged", userNameLogged);
	    
	    this.batchProcessServiceFacade.registerBatch(userNameLogged, businessProcess, param);	    
    }
}
