package com.pradera.report.generation.executor.accounting.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.pradera.core.component.helperui.to.AccountingAccountTo;
import com.pradera.model.accounting.AccountingReceipt;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingReceiptDetailTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class AccountingReceiptDetailTo implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7496136437839429199L;
	
	/** The id accounting receipts detail pk. */
	private Long idAccountingReceiptsDetailPk;

  	/** The accounting receipt. */
	  private AccountingReceipt accountingReceipt;
  	  	
  	/** The accounting matrix detail to. */
	  private AccountingMatrixDetailTo accountingMatrixDetailTo;

	/** The number receipt detail. */
	private String numberReceiptDetail;
	
	/** The associate code. */
	private Integer associateCode;
  	
	/** The annex ref. */
	private Integer annexRef;
  	
	/** The amount. */
	private BigDecimal amount;
  	
	/** The quantity. */
	private Integer quantity;
  	
	/** The gloss. */
	private String gloss;
  	
	/** The document date. */
	private Date documentDate;
	
	/** The list accounting debit. */
	private List<AccountingAccountTo> listAccountingDebit;
	
	/** The list accounting assets. */
	private List<AccountingAccountTo> listAccountingAssets;

	/** The accounting source logger to. */
	private AccountingSourceLoggerTo	accountingSourceLoggerTo;
	
	/**
	 * Instantiates a new accounting receipt detail to.
	 */
	public AccountingReceiptDetailTo() {
		listAccountingDebit=new ArrayList<AccountingAccountTo>();
		listAccountingAssets=new ArrayList<AccountingAccountTo>();
	}




	/**
	 * Gets the id accounting receipts detail pk.
	 *
	 * @return the idAccountingReceiptsDetailPk
	 */
	public Long getIdAccountingReceiptsDetailPk() {
		return idAccountingReceiptsDetailPk;
	}




	/**
	 * Sets the id accounting receipts detail pk.
	 *
	 * @param idAccountingReceiptsDetailPk the idAccountingReceiptsDetailPk to set
	 */
	public void setIdAccountingReceiptsDetailPk(Long idAccountingReceiptsDetailPk) {
		this.idAccountingReceiptsDetailPk = idAccountingReceiptsDetailPk;
	}




	/**
	 * Gets the accounting receipt.
	 *
	 * @return the accountingReceipt
	 */
	public AccountingReceipt getAccountingReceipt() {
		return accountingReceipt;
	}




	/**
	 * Sets the accounting receipt.
	 *
	 * @param accountingReceipt the accountingReceipt to set
	 */
	public void setAccountingReceipt(AccountingReceipt accountingReceipt) {
		this.accountingReceipt = accountingReceipt;
	}




	/**
	 * Gets the accounting matrix detail to.
	 *
	 * @return the accountingMatrixDetailTo
	 */
	public AccountingMatrixDetailTo getAccountingMatrixDetailTo() {
		return accountingMatrixDetailTo;
	}




	/**
	 * Sets the accounting matrix detail to.
	 *
	 * @param accountingMatrixDetailTo the accountingMatrixDetailTo to set
	 */
	public void setAccountingMatrixDetailTo(
			AccountingMatrixDetailTo accountingMatrixDetailTo) {
		this.accountingMatrixDetailTo = accountingMatrixDetailTo;
	}




	/**
	 * Gets the number receipt detail.
	 *
	 * @return the numberReceiptDetail
	 */
	public String getNumberReceiptDetail() {
		return numberReceiptDetail;
	}




	/**
	 * Sets the number receipt detail.
	 *
	 * @param numberReceiptDetail the numberReceiptDetail to set
	 */
	public void setNumberReceiptDetail(String numberReceiptDetail) {
		this.numberReceiptDetail = numberReceiptDetail;
	}




	/**
	 * Gets the associate code.
	 *
	 * @return the associateCode
	 */
	public Integer getAssociateCode() {
		return associateCode;
	}




	/**
	 * Sets the associate code.
	 *
	 * @param associateCode the associateCode to set
	 */
	public void setAssociateCode(Integer associateCode) {
		this.associateCode = associateCode;
	}




	/**
	 * Gets the annex ref.
	 *
	 * @return the annexRef
	 */
	public Integer getAnnexRef() {
		return annexRef;
	}




	/**
	 * Sets the annex ref.
	 *
	 * @param annexRef the annexRef to set
	 */
	public void setAnnexRef(Integer annexRef) {
		this.annexRef = annexRef;
	}




	/**
	 * Gets the amount.
	 *
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}




	/**
	 * Sets the amount.
	 *
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}




	/**
	 * Gets the quantity.
	 *
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}




	/**
	 * Sets the quantity.
	 *
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}




	/**
	 * Gets the gloss.
	 *
	 * @return the gloss
	 */
	public String getGloss() {
		return gloss;
	}




	/**
	 * Sets the gloss.
	 *
	 * @param gloss the gloss to set
	 */
	public void setGloss(String gloss) {
		this.gloss = gloss;
	}




	/**
	 * Gets the document date.
	 *
	 * @return the documentDate
	 */
	public Date getDocumentDate() {
		return documentDate;
	}




	/**
	 * Sets the document date.
	 *
	 * @param documentDate the documentDate to set
	 */
	public void setDocumentDate(Date documentDate) {
		this.documentDate = documentDate;
	}




	/**
	 * Gets the list accounting debit.
	 *
	 * @return the listAccountingDebit
	 */
	public List<AccountingAccountTo> getListAccountingDebit() {
		return listAccountingDebit;
	}




	/**
	 * Sets the list accounting debit.
	 *
	 * @param listAccountingDebit the listAccountingDebit to set
	 */
	public void setListAccountingDebit(List<AccountingAccountTo> listAccountingDebit) {
		this.listAccountingDebit = listAccountingDebit;
	}




	/**
	 * Gets the list accounting assets.
	 *
	 * @return the listAccountingAssets
	 */
	public List<AccountingAccountTo> getListAccountingAssets() {
		return listAccountingAssets;
	}




	/**
	 * Sets the list accounting assets.
	 *
	 * @param listAccountingAssets the listAccountingAssets to set
	 */
	public void setListAccountingAssets(
			List<AccountingAccountTo> listAccountingAssets) {
		this.listAccountingAssets = listAccountingAssets;
	}




	/**
	 * Gets the accounting source logger to.
	 *
	 * @return the accountingSourceLoggerTo
	 */
	public AccountingSourceLoggerTo getAccountingSourceLoggerTo() {
		return accountingSourceLoggerTo;
	}




	/**
	 * Sets the accounting source logger to.
	 *
	 * @param accountingSourceLoggerTo the accountingSourceLoggerTo to set
	 */
	public void setAccountingSourceLoggerTo(
			AccountingSourceLoggerTo accountingSourceLoggerTo) {
		this.accountingSourceLoggerTo = accountingSourceLoggerTo;
	}


}
