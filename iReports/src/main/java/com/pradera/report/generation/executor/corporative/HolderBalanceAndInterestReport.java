package com.pradera.report.generation.executor.corporative;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.utils.CommonsUtilities;
//import com.pradera.core.component.corporateevents.service.StockCalculationServiceBean;
import com.pradera.core.component.corporateevents.to.CorporativeOperationTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.type.EconomicActivityType;
import com.pradera.model.corporatives.stockcalculation.StockCalculationProcess;
import com.pradera.model.corporatives.stockcalculation.type.StockClassType;
import com.pradera.model.corporatives.stockcalculation.type.StockStateType;
import com.pradera.model.corporatives.stockcalculation.type.StockType;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.PeriodType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.corporative.service.CorporativeReportServiceBean;
import com.pradera.report.generation.executor.corporative.to.HolderBalanceAndInterestTO;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;


@ReportProcess(name="HolderBalanceAndInterestReport")
public class HolderBalanceAndInterestReport extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final  Logger logger = LoggerFactory.getLogger(HolderBalanceAndInterestReport.class);


	private static int COUNTRY=0;
	private static int ECONOMINC_ACTIVITY=1;
	private static int CURRENCY=2;
	private static int IND_RESIDENCE=3;
	private static int BALANCE=4;
	private static int COUNTRY_STR=5;

//	@Inject
//	StockCalculationServiceBean stockCalculationServiceBean;

	@Inject
	private CorporativeReportServiceBean corporativeReportServiceBean;

	private Integer periodType;
	private Integer year = 2013;
	private List<HolderBalanceAndInterestTO> holderBalanceList;
	private List<HolderBalanceAndInterestTO> holderInterestList;



	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {


		logger.info("BILY");

		CorporativeOperationTO corporativeOperationTO = this.getReportToFromReportLogger(reportLogger);
		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
		holderBalanceList = new ArrayList<HolderBalanceAndInterestTO>();
		holderInterestList = new ArrayList<HolderBalanceAndInterestTO>();

		XMLOutputFactory xof = XMLOutputFactory.newInstance();
		try{
			logger.info("");

			this.generateHolderBalanceData(); 
			holderBalanceList.addAll(holderInterestList);
			Collections.sort(holderBalanceList,new HolderBalanceAndInterestTO());

			//this.printData();
			XMLStreamWriter xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(arrayOutputStream));
			//open document file
			xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");
			//root tag of report
			/**
			 * report
			 **/
			xmlsw.writeStartElement(ReportConstant.REPORT);
			/**
			 * Create to header report
			 * start_hour
			 * report_title
			 * mnemonic_report
			 * clasification_report
			 * generation_date
			 * user_name
			 * mnemonic_entity
			 * **/
			createHeaderReport(xmlsw, reportLogger);
			//Create all the headers of the reports for no result statement


			Integer period = this.getPeriodType();
			
			if(PeriodType.MONTHLY.getCode().equals(period)){
				createTagString(xmlsw, "ind_month", 1);
			}	
			if(PeriodType.BIMONTHLY.getCode().equals(period)){
				createTagString(xmlsw, "ind_month", 2);
			}			
			if(PeriodType.QUARTERLY.getCode().equals(period)){
				createTagString(xmlsw, "ind_month", 3);
			}
			if(PeriodType.FOURMONTHLY.getCode().equals(period)){
				createTagString(xmlsw, "ind_month", 4);
			}
			
			if(PeriodType.BIANNUAL.getCode().equals(period)){
				createTagString(xmlsw, "ind_month", 6);
			}
			
			if(PeriodType.ANNUAL.getCode().equals(period)){
				createTagString(xmlsw, "ind_month", 11);
			}
			

			if(Validations.validateListIsNotNullAndNotEmpty(holderBalanceList)){
				/** Creating the body **/
				createBodyReport(xmlsw,holderBalanceList);
			}
			//END REPORT TAG
			xmlsw.writeEndElement();
			//close document
			xmlsw.writeEndDocument();
			xmlsw.flush();
			xmlsw.close();
		}catch(XMLStreamException e){
			e.printStackTrace();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return arrayOutputStream;		

	}

	public void printData(){
		Collections.sort(holderBalanceList,new HolderBalanceAndInterestTO());
		Collections.sort(holderInterestList,new HolderBalanceAndInterestTO());
		for(int i =0; i<holderBalanceList.size(); i++){
			HolderBalanceAndInterestTO temp=holderInterestList.get(i);
			System.out.println("Type: "+temp.getGroupList()+"    ********* Register  Currency: " +temp.getCurrency() + " Country "+temp.getCountry() + " AE " + temp.getEconomicActivity() + " RES: " +temp.getIndResidence() );

			for(Map.Entry<Integer, BigDecimal> balanceEntry: temp.getMapBalances().entrySet()){
				System.out.println("MONTH: " + balanceEntry.getKey() + "Balance " + balanceEntry.getValue());
			}
		}
	}

	public List<Integer> getAllMonthsValues(){
		Calendar calendar = Calendar.getInstance();
		Map<String, Integer> mapMonths = calendar.getDisplayNames(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH); 	

		List<Integer> arrayList = new ArrayList<Integer>();
		for(Map.Entry<String, Integer> entryMonths: mapMonths.entrySet()){
			arrayList.add(entryMonths.getValue());

		}
		Collections.sort(arrayList);
		return arrayList;
	}

	public void generateBalance (Long processPks, Integer month, Date date){
		//Date date = CommonsUtilities.getMaxDateByMonthAndYear(month,this.getYear());
		Long processPk =this.executeStockCalculation(date);
		List<HolderBalanceAndInterestTO> balanceHolderList = this.getListHolderBalanceAndInterest(processPk, month,null);
		List<HolderBalanceAndInterestTO> interestHolderList = this.getListHolderBalanceAndInterest(null, month,date);
		//Fill for balances
		if(Validations.validateListIsNullOrEmpty(holderBalanceList)){
			for(HolderBalanceAndInterestTO temp: balanceHolderList){
				holderBalanceList.add(temp);
			}
		}else{
			this.compareHolderBalancesLists(balanceHolderList,month.intValue(),holderBalanceList);
		}
		//Fill for Interest
		if(Validations.validateListIsNullOrEmpty(holderInterestList)){
			for(HolderBalanceAndInterestTO temp: interestHolderList){
				holderInterestList.add(temp);
			}
		}else{
			this.compareHolderBalancesLists(interestHolderList, month,holderInterestList );
		}
	}


	public void generateHolderBalanceData(){
		Calendar calendar = Calendar.getInstance();
		List<Integer> monthsList = this.getAllMonthsValues();


		if(PeriodType.MONTHLY.getCode().equals(this.getPeriodType())){
			Map<String, Integer> mapMonths = calendar.getDisplayNames(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH); 	
 
			//for(Map.Entry<String, Integer> entryMonths: mapMonths.entrySet()){
			for(Integer month: monthsList){
				Date date = CommonsUtilities.getMaxDateByMonthAndYear(month,this.getYear());
				Long processPk =this.executeStockCalculation(date);
				List<HolderBalanceAndInterestTO> balanceHolderList = this.getListHolderBalanceAndInterest(processPk, month,null);
				List<HolderBalanceAndInterestTO> interestHolderList = this.getListHolderBalanceAndInterest(null, month,date);
				//Fill for balances
				if(Validations.validateListIsNullOrEmpty(holderBalanceList)){
					for(HolderBalanceAndInterestTO temp: balanceHolderList){
						holderBalanceList.add(temp);
					}
				}else{
					this.compareHolderBalancesLists(balanceHolderList,month.intValue(),holderBalanceList);
				}
				//Fill for Interest
				if(Validations.validateListIsNullOrEmpty(holderInterestList)){
					for(HolderBalanceAndInterestTO temp: interestHolderList){
						holderInterestList.add(temp);
					}
				}else{
					this.compareHolderBalancesLists(interestHolderList, month,holderInterestList );
				}
			}
		} else if(PeriodType.BIMONTHLY.getCode().equals(this.getPeriodType())){
 			for(int i =1; i<monthsList.size();i=i+2){
				Integer month = monthsList.get(i);
				Date date = CommonsUtilities.getMaxDateByMonthAndYear(month,this.getYear());
				this.generateBalance(null, month, date);
			}		

		}
		else if(PeriodType.QUARTERLY.getCode().equals(this.getPeriodType())){
			for(int i =2; i<monthsList.size();i=i+3){
				Integer month = monthsList.get(i);
				Date date = CommonsUtilities.getMaxDateByMonthAndYear(month,this.getYear());
				this.generateBalance(null, month, date);
			}
 		}
		
		else if(PeriodType.FOURMONTHLY.getCode().equals(this.getPeriodType())){
			for(int i =3; i<monthsList.size();i=i+4){
				Integer month = monthsList.get(i);
				Date date = CommonsUtilities.getMaxDateByMonthAndYear(month, this.getYear());
				this.generateBalance(null, month, date);
			}
		}
		else if(PeriodType.BIANNUAL.getCode().equals(this.getPeriodType())){
			for(int i =5; i<monthsList.size();i=i+6){
				Integer month = monthsList.get(i);
				Date date = CommonsUtilities.getMaxDateByMonthAndYear(month, this.getYear());
				this.generateBalance(null, month, date);
			}
		}
		else if(PeriodType.ANNUAL.getCode().equals(this.getPeriodType())){
			for(int i =11; i<monthsList.size();i=i+11){
				Integer month = monthsList.get(i);
				Date date = CommonsUtilities.getMaxDateByMonthAndYear(month, this.getYear());
				this.generateBalance(null, month, date);
			}
		}
	}

	public List<HolderBalanceAndInterestTO> cloneList(List<HolderBalanceAndInterestTO> tempList){
		List<HolderBalanceAndInterestTO> cloneList = new ArrayList<HolderBalanceAndInterestTO>();
		for(HolderBalanceAndInterestTO temp: tempList){
			cloneList.add(temp);
		}
		return cloneList;
	}

	public void compareHolderBalancesLists(List<HolderBalanceAndInterestTO> inputHolderBalanceList,int month, List<HolderBalanceAndInterestTO> actualList){
		List<HolderBalanceAndInterestTO> tmpHolderBalanceList = this.cloneList(actualList);
		boolean flag = false;
		for(HolderBalanceAndInterestTO inputHolderBalanceTo: inputHolderBalanceList){
			//for(HolderBalanceAndInterestTO holderBalanceTo: holderBalanceAndInterestList){
			for(int i =0; i<tmpHolderBalanceList.size();i++){
				HolderBalanceAndInterestTO holderBalanceTo = tmpHolderBalanceList.get(i);					
				if(inputHolderBalanceTo.getCountry().equals(holderBalanceTo.getCountry())  &&
						inputHolderBalanceTo.getCurrency().equals(holderBalanceTo.getCurrency())&&
						inputHolderBalanceTo.getIndResidence().equals(holderBalanceTo.getIndResidence())&&
						inputHolderBalanceTo.getEconomicActivity().equals(holderBalanceTo.getEconomicActivity())){
					System.out.println("Found Coincidence");
					System.out.println("country: "+inputHolderBalanceTo.getCountry()+
							"currency " + inputHolderBalanceTo.getCurrency()+
							"economic_activity" + inputHolderBalanceTo.getEconomicActivity()+
							"ind_res" + inputHolderBalanceTo.getIndResidence());
					System.out.println("With");
					System.out.println("country: "+holderBalanceTo.getCountry()+
							"currency " + holderBalanceTo.getCurrency()+
							"economic_activity" + holderBalanceTo.getEconomicActivity()+
							"ind_res" + holderBalanceTo.getIndResidence());
					//holderBalanceTo.getMapBalances().put(month, inputHolderBalanceTo.getMapBalances().get(month));
					actualList.get(i).getMapBalances().put(month,inputHolderBalanceTo.getMapBalances().get(month));

					break;		

				}
				else{
					flag = Boolean.TRUE;
				}
			}
			if(flag){
				actualList.add(inputHolderBalanceTo);
				flag = Boolean.FALSE;
			}


		}
	}



	public List<HolderBalanceAndInterestTO> getListHolderBalanceAndInterest(Long stockProcess, int month, Date date){
		List<Object[]> list = null;
		if(stockProcess!=null){
			list = this.corporativeReportServiceBean.searchStockCalculationBalanceByStockProcess(stockProcess);
		}
		if(date!=null){
			list = this.corporativeReportServiceBean.getCorporateProcessResultByDeliveryDate(date);

		}
		List<HolderBalanceAndInterestTO> listHolderBalance = new ArrayList<HolderBalanceAndInterestTO>();
		HolderBalanceAndInterestTO holderBalanceTo;
		for(Object[] temp: list){
			holderBalanceTo = new HolderBalanceAndInterestTO();
			holderBalanceTo.setCurrency(Integer.parseInt(temp[CURRENCY].toString()));
			holderBalanceTo.setCountry(Integer.parseInt(temp[COUNTRY].toString()));
			holderBalanceTo.setEconomicActivity(Integer.parseInt(temp[ECONOMINC_ACTIVITY].toString()));
			holderBalanceTo.setIndResidence(Integer.parseInt(temp[IND_RESIDENCE].toString()));
			Integer residence = Integer.parseInt(temp[IND_RESIDENCE].toString());
			if(residence.equals(new Integer(1))){
				if(stockProcess!=null){
					holderBalanceTo.setGroupList(1);
				}
				else{
					holderBalanceTo.setGroupList(3);
				}
			}
			else if(residence.equals(new Integer(0))){
				if(stockProcess!=null){
					holderBalanceTo.setGroupList(2);
				}else{
					holderBalanceTo.setGroupList(4);
				}
			}
			holderBalanceTo.setCountryStr(temp[COUNTRY_STR].toString());
			holderBalanceTo.getMapBalances().put(month, new BigDecimal(temp[BALANCE].toString()));
			listHolderBalance.add(holderBalanceTo);
		}

		return listHolderBalance;


	}

	public Long executeStockCalculation(Date date){
		StockCalculationProcess stockCalculationProcess = new StockCalculationProcess();
		//Date date = CommonsUtilities.getMaxDateByMonthAndYear(month,year);

		stockCalculationProcess.setIdStockCalculationPk(null);
		stockCalculationProcess.setStockState(StockStateType.REGISTERED.getCode());
		stockCalculationProcess.setCreationDate(CommonsUtilities.currentDateTime());
		stockCalculationProcess.setRegistryUser("test");
		stockCalculationProcess.setStockClass(StockClassType.NORMAL.getCode());
		stockCalculationProcess.setStockType(StockType.MONTHLY.getCode());
		stockCalculationProcess.setRegistryDate(date);
		stockCalculationProcess.setCutoffDate(date);
		stockCalculationProcess.setIndAutomactic(BooleanType.YES.getCode());
		stockCalculationProcess.setLastModifyApp(1);
		stockCalculationProcess.setLastModifyDate(CommonsUtilities.currentDate());
		stockCalculationProcess.setLastModifyIp("ip");
		stockCalculationProcess.setLastModifyUser("JFABIAn");
//		try {
//			stockCalculationServiceBean.createStockCalculation(stockCalculationProcess);
//			if(Validations.validateIsNotNull(stockCalculationProcess.getIdStockCalculationPk())){
//				stockCalculationServiceBean.executeBatchStockCalculation(stockCalculationProcess.getIdStockCalculationPk());
//			}
//		} catch (ServiceException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return stockCalculationProcess.getIdStockCalculationPk();


	}

	public void createBodyReport(XMLStreamWriter xmlsw,List<HolderBalanceAndInterestTO> listOperations){

		try {
			for(HolderBalanceAndInterestTO temp: listOperations){
				xmlsw.writeStartElement("operation");
				createTagString(xmlsw, "country", temp.getCountryStr());

				String economicActivityStr = EconomicActivityType.get(temp.getEconomicActivity()).getValue();
				String currencyStr = CurrencyType.get(temp.getCurrency()).getCodeIso();
				createTagString(xmlsw, "econominc_activity", economicActivityStr);
				createTagString(xmlsw, "currency", currencyStr);
				createTagString(xmlsw, "ind_residence", temp.getIndResidence());
				if(temp.getGroupList().equals(new Integer(1))){
					createTagString(xmlsw, "type", "SALDO TENEDORES RESIDENTES");
				}

				if(temp.getGroupList().equals(new Integer(2))){
					createTagString(xmlsw, "type", "SALDO TENEDORES NO RESIDENTES");
				}
				if(temp.getGroupList().equals(new Integer(3))){
					createTagString(xmlsw, "type", "PAGO DE INTERESES TENEDORES RESIDENTES");
				}
				if(temp.getGroupList().equals(new Integer(4))){
					createTagString(xmlsw, "type", "PAGO DE INTERESES TENEDORES NO RESIDENTES");
				}
				
				
				

				Calendar calendar = Calendar.getInstance();

				Map<String, Integer> mapMonths = calendar.getDisplayNames(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH); 		
				for(Map.Entry<String, Integer> entryMonths: mapMonths.entrySet()){
					createTagString(xmlsw, entryMonths.getKey(), temp.getMapBalances().get(entryMonths.getValue()));

				}

				xmlsw.writeEndElement();
			}

		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public CorporativeOperationTO getReportToFromReportLogger(ReportLogger reportLogger){
		CorporativeOperationTO corporativeOperationTO = new CorporativeOperationTO();
		for(ReportLoggerDetail detail:reportLogger.getReportLoggerDetails()){
			if(ReportConstant.PERIOD_TYPE.equals(detail.getFilterName())){
				this.setPeriodType(Integer.parseInt(detail.getFilterValue()));
			}
			if(ReportConstant.YEAR_COMBO.equals(detail.getFilterName())){
				this.setYear(Integer.parseInt(detail.getFilterValue()));
			}

		}

		return corporativeOperationTO;
	}


	public Integer getYear() {
		return year;
	}


	public void setYear(Integer year) {
		this.year = year;
	}


	public Integer getPeriodType() {
		return periodType;
	}


	public void setPeriodType(Integer periodType) {
		this.periodType = periodType;
	}









}
