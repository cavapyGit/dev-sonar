package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.account.to.NoteToExternalTO;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;

@ReportProcess(name = "DocNoteRegistrationDGravamenReport")
public class DocNoteRegistrationDGravamenReport extends GenericReport {

	private static final long serialVersionUID = 1L;

	@Inject
	PraderaLogger log;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	private CustodyReportServiceBean reportServiceBean;

	public static final String ARCHETYPE_MRS = "a ";

	public static final String EXECUTIVE_DIRECTOR_ARCH_MRS = "DIRECTORA EJECUTIVA a.i.";

	public static final String ASFI = "AUTORIDAD DE SUPERVISI\u00d3N DEL SISTEMA FINANCIERO";
	
	public DocNoteRegistrationDGravamenReport() {	
	}
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		return null;
	}
	
	@Override
	public void addParametersQueryReport() {
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		if (listaLogger == null) {
			listaLogger = new ArrayList<ReportLoggerDetail>();
		}
	}
	
	// METODO PARA OBTENER EL DESTINATARIO DEL DOCUMENTO
		public String getRecipientNoteToExternal(Integer parameterTablePk) {
					String recipient = "";
					try {
						recipient = parameterService.findParameterTableDescription(parameterTablePk);
					} catch (Exception ex) {
						log.error(ex.getMessage());
						ex.printStackTrace();
						throw new RuntimeException();
					}
					return recipient;
		}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {

		Map<String, Object> parametersRequired = new HashMap<>();
		//ParameterTableTO  filter = new ParameterTableTO();
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		NoteToExternalTO noteExternalTO = new NoteToExternalTO();

		// DATOS DE ENTRADA DEL SISTEMA
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.REFERENCE_NOTE_EXTERNAL)) {
				noteExternalTO.setIdReferencePk(Integer.parseInt(r.getFilterValue()));
			}
			if (r.getFilterName().equals(ReportConstant.QUOTE_NOTE_EXTERNAL)) {
				noteExternalTO.setQuote(r.getFilterValue());
			}
			if (r.getFilterName().equals(ReportConstant.ATTACHED_SHEETS)) {
				noteExternalTO.setAttachedSheets(r.getFilterValue());
			}
		}
		
		//Map<Integer,String> block_type = new HashMap<Integer, String>();
		
		// DESCRIPCION DE LA REFERENCIA SELECCIONADA
		try {
			Integer idReference = noteExternalTO.getIdReferencePk();
			String desReference = parameterService.findParameterTableDescription(idReference);
			noteExternalTO.setDesReference(desReference);
			
			/*filter.setMasterTableFk(MasterTableType.AFFECTATION_TYPE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				block_type.put(param.getParameterTablePk(), param.getParameterName());
			}*/
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}

		// DATOS SELECCIONADOS SEGUN PK DE LA REFERENCIA
		switch (noteExternalTO.getIdReferencePk()) {
			//GRAVAMEN
			case 2167:
				noteExternalTO.setArchetype(ARCHETYPE_MRS);
				noteExternalTO.setRecipient(getRecipientNoteToExternal(2252));
				noteExternalTO.setOccupation(EXECUTIVE_DIRECTOR_ARCH_MRS);
				noteExternalTO.setDepartment(ASFI);
				noteExternalTO.setBlockType("1115");
				break;
			//DEGRAVAMEN
			case 2168:
				noteExternalTO.setArchetype(ARCHETYPE_MRS);
				noteExternalTO.setRecipient(getRecipientNoteToExternal(2252));
				noteExternalTO.setOccupation(EXECUTIVE_DIRECTOR_ARCH_MRS);
				noteExternalTO.setDepartment(ASFI);
				noteExternalTO.setBlockType("1117");
				break;
		}

		//parametersRequired.put("quote", noteExternalTO.getQuote());
		parametersRequired.put("archetype", noteExternalTO.getArchetype());
		parametersRequired.put("recipient", noteExternalTO.getRecipient());
		parametersRequired.put("ocupation", noteExternalTO.getOccupation());
		parametersRequired.put("department", noteExternalTO.getDepartment());
		parametersRequired.put("desReference", noteExternalTO.getDesReference());
		//parametersRequired.put("attached_sheets", noteExternalTO.getAttachedSheets());
		parametersRequired.put("block_type", noteExternalTO.getBlockType());
		parametersRequired.put("referencepk", noteExternalTO.getIdReferencePk());

		return parametersRequired;
	}
	
	/*public String getBody(Integer parameterTablePk, String blockType){
		NoteRegistrationDGravamenTO registration = new NoteRegistrationDGravamenTO();
		reportServiceBean.getRegistrationDGravamen(blockType);
		String body="";
		switch (parameterTablePk) {
		case 20000:
				body = BODY_DOC_1 + registration.getIdBlockRequestPk() + BODY_DOC_1_1 + registration.getFullName() 
				+ BODY_DOC_1_2 + registration.getBlockNumberDate() + BODY_DOC_1_3 + registration.getRegistryUser()
				+ BODY_DOC_1_4 + " " + BODY_DOC_1_5 + BODY_DOC_FAREWELL_1;
				
			break;
		case 21000:
				body = BODY_DOC_2 + registration.getBlockNumber() + BODY_DOC_2_1 + registration.getRegistryUser()
				+ ":" + " " + BODY_DOC_2_2 + BODY_DOC_FAREWELL_2;
			break;
		}
		return body;		
	}*/
}