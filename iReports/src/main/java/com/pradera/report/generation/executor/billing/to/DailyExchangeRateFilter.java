package com.pradera.report.generation.executor.billing.to;


import java.io.Serializable;
import java.util.Date;

import com.pradera.commons.utils.CommonsUtilities;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class DailyExchangeRateFilter.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class DailyExchangeRateFilter implements  Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The date rate. */
	private Date dateRate;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/** The id currency. */
	private Integer idCurrency;
	
	/** The id sourceinformation. */
	private Integer idSourceinformation;

	/**
	 * Gets the date rate.
	 *
	 * @return the dateRate
	 */
	public Date getDateRate() {
		return dateRate;
	}

	/**
	 * Sets the date rate.
	 *
	 * @param dateRate the dateRate to set
	 */
	public void setDateRate(Date dateRate) {
		this.dateRate = dateRate;
	}

	/**
	 * Gets the id currency.
	 *
	 * @return the idCurrency
	 */
	public Integer getIdCurrency() {
		return idCurrency;
	}

	/**
	 * Sets the id currency.
	 *
	 * @param idCurrency the idCurrency to set
	 */
	public void setIdCurrency(Integer idCurrency) {
		this.idCurrency = idCurrency;
	}

	/**
	 * Gets the id sourceinformation.
	 *
	 * @return the idSourceinformation
	 */
	public Integer getIdSourceinformation() {
		return idSourceinformation;
	}

	/**
	 * Sets the id sourceinformation.
	 *
	 * @param idSourceinformation the idSourceinformation to set
	 */
	public void setIdSourceinformation(Integer idSourceinformation) {
		this.idSourceinformation = idSourceinformation;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initialDate
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the initialDate to set
	 */
	public void setInitialDate(Date initialDate) {
		Date today = CommonsUtilities.currentDate();
		if (initialDate != null && today.compareTo(initialDate) >= 0) {
			this.initialDate = initialDate;
			if(finalDate != null && initialDate.after(finalDate)){
				finalDate = null;
			}
		} else {
			this.initialDate = null;
		}
	}

	/**
	 * Gets the final date.
	 *
	 * @return the finalDate
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the finalDate to set
	 */
	public void setFinalDate(Date finalDate) {
		Date today = CommonsUtilities.currentDate();
		if (finalDate != null
				&& today.compareTo(finalDate) >= 0
				&& (initialDate == null || initialDate.compareTo(finalDate) <= 0)) {
			this.finalDate = finalDate;
		} else {
			this.finalDate = null;
		}
	}
}
