package com.pradera.report.generation.executor.issuances;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.management.RuntimeErrorException;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.service.HolderQueryServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.executor.issuances.services.IssuancesReportServiceBean;
import com.pradera.report.generation.executor.issuances.to.GenericIssuanceTO;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class DetailsOfTitlesPendingOfExpirationReport.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01-sep-2015
 */
@ReportProcess(name = "DetailsOfTitlesPendingOfExpirationReport")
public class DetailsOfTitlesPendingOfExpirationReport extends GenericReport{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The parameter service. */
	@EJB
	private ParameterServiceBean parameterService;
	
	/** The participant service bean. */
	@EJB
	private ParticipantServiceBean participantServiceBean;

	/** The holder query service bean. */
	@EJB
	private HolderQueryServiceBean holderQueryServiceBean;
	
	/** The issuances report service. */
	@EJB
	private IssuancesReportServiceBean issuancesReportService;
	
	/** The custody report service bean. */
	@EJB
	CustodyReportServiceBean custodyReportServiceBean;
	
	/** The log. */
	@Inject
	private PraderaLogger log;

	/**
	 * Instantiates a new details of titles pending of expiration report.
	 */
	public DetailsOfTitlesPendingOfExpirationReport() {}
	
	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#generateData(com.pradera.model.report.ReportLogger)
	 */
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		return null;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#getCustomJasperParameters()
	 */
	@Override
	public Map<String, Object> getCustomJasperParameters() {

		GenericIssuanceTO gfto = new GenericIssuanceTO();
		String descSituation = null;
		String descExpiration = null;

		for(ReportLoggerDetail param: getReportLogger().getReportLoggerDetails()){
			if(param.getFilterName().equals("securities_situation")){
				if (param.getFilterValue()!=null){
					gfto.setSecuritiesSituation(param.getFilterValue().toString());
					descSituation = param.getFilterDescription().toString();
				}
			}
			if(param.getFilterName().equals("cui_code")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setCui(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("holder_account")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setHolderAccount(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("security_class_key")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setSecurities(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("issuer_code")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setIssuer(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("participant")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setParticipant(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("expiration_of")){
				if (param.getFilterValue()!=null){
					gfto.setExpirationOf(param.getFilterValue().toString());
					descExpiration = param.getFilterDescription().toString();
				}
			}
			if(param.getFilterName().equals("currency")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setCurrency(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("date_initial")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setInitialDate(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("date_end")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setFinalDate(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("security_class")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setSecurityClass(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("ind_automatic")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setIndAutomatic(param.getFilterValue().toString());
				}
			}
		}
		
		Date currentDate = CommonsUtilities.currentDate();
		String currDateStr = CommonsUtilities.convertDatetoStringForTemplate(currentDate, CommonsUtilities.DATE_PATTERN);
		
		//VALIDAR TIPO DE CAMBIO A LA FECHA DE CORTE
		BigDecimal exchangeRate = null;
		try{
			exchangeRate = custodyReportServiceBean.getExchangeRate(currDateStr);
		} catch (ServiceException e) {
			if(e.getErrorService().equals(ErrorServiceType.NOT_EXCHANGE_RATE)){
				throw new RuntimeErrorException(null, ReportConstant.ERROR_EXCHANGE_RATE + " " + currDateStr);
			}
			log.error(e.getMessage());
		}
		
		String strQuery = issuancesReportService.getDetailsOfTitlesPendingOfExpirationReport(gfto); 
		
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> secSecClass 			= new HashMap<Integer, String>();
		Map<Integer,String> secCurrency		 		= new HashMap<Integer, String>();
		Participant participant = new Participant();
		Holder holder = new Holder();
		HolderAccount holderAccount = new HolderAccount();
		Issuer issuer = new Issuer();
		String part=null;
		String iss=null;
		
		try {
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secSecClass.put(param.getParameterTablePk(), param.getText1());
			}
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secCurrency.put(param.getParameterTablePk(), param.getParameterName());
			}
			if(Validations.validateIsNotNull(gfto.getParticipant())){
				participant.setIdParticipantPk(Long.valueOf(gfto.getParticipant()));
				participant = participantServiceBean.findParticipantByFiltersServiceBean(participant);
				part = participant.getMnemonic() +" - "+participant.getDescription();
			}
			if(Validations.validateIsNotNull(gfto.getCui())){
				holder = holderQueryServiceBean.findHolderByCode(Long.valueOf(gfto.getCui()));
			}
			if(Validations.validateIsNotNull(gfto.getHolderAccount())){
				holderAccount = issuancesReportService.findHolderAccountByPk(Long.valueOf(gfto.getHolderAccount()));
				parametersRequired.put("pAccount", holderAccount.getAccountNumber().toString());
			}
			if(Validations.validateIsNotNull(gfto.getIssuer())){
				issuer = issuancesReportService.findIssuerByCode(gfto.getIssuer());
				iss = issuer.getMnemonic()+" - "+issuer.getBusinessName();
			}
			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		parametersRequired.put("str_Query", strQuery);
		parametersRequired.put("initialDt", gfto.getInitialDate());
		parametersRequired.put("finalDt", gfto.getFinalDate());
		parametersRequired.put("mSecurityClass", secSecClass);
		parametersRequired.put("mCurrency", secCurrency);
		parametersRequired.put("pSecSituation", descSituation);
		parametersRequired.put("pHolder", holder.getFullName());
		parametersRequired.put("pCurrency", gfto.getCurrency());
		parametersRequired.put("pValor", gfto.getSecurities());
		parametersRequired.put("pIssuer", iss != null ? iss : null);
		parametersRequired.put("pParticipant", part != null ? part : null);
		parametersRequired.put("pExpiration", descExpiration);
		if(exchangeRate!=null){
			parametersRequired.put("exchange_rate", exchangeRate.toString());
		}
		return parametersRequired;
		
	}

}
