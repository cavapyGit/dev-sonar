package com.pradera.report.generation.executor.negotiation.to;

import java.io.Serializable;

public class PrimaryAndSecondaryMarketTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//TODO parameters input to report
	private Integer typeMarket;
	private String mnemonicParticipant;
	private Long idHolderSelected;
	private String initialDate;
	private String finalDate;
	
	//constructor 1
	public PrimaryAndSecondaryMarketTO() {}

	//TODO methods getter and setter
	public Integer getTypeMarket() {
		return typeMarket;
	}

	public void setTypeMarket(Integer typeMarket) {
		this.typeMarket = typeMarket;
	}

	public Long getIdHolderSelected() {
		return idHolderSelected;
	}

	public String getMnemonicParticipant() {
		return mnemonicParticipant;
	}

	public void setMnemonicParticipant(String mnemonicParticipant) {
		this.mnemonicParticipant = mnemonicParticipant;
	}

	public void setIdHolderSelected(Long idHolderSelected) {
		this.idHolderSelected = idHolderSelected;
	}

	public String getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(String initialDate) {
		this.initialDate = initialDate;
	}

	public String getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(String finalDate) {
		this.finalDate = finalDate;
	}
}
