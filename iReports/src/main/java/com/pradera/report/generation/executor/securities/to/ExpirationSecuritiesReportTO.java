package com.pradera.report.generation.executor.securities.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ExpirationSecuritiesReportTO  implements Serializable {


	private static final long serialVersionUID = 1L;
	
	//report configuration
	private String securityClass;
	
	private String holder;
	
	private Integer holderAccount;
	
	private String issuer;
	
	private String securityCode;
	
	private Date initialDate;
	
	private Integer bcbFormat;
	
	private Date finalDate;
	
	private String withoutDpf;
	
	private Long participant;
	
	private String institutionType;
	
	private String participantInvestorInstitutionType;
	
	//report fields
	private String titulo;
	
	private String businessName;
	
	private String descriptionCu;
	
	private String mnemonicCu;
	
	private String expirationDate;
	
	private String serial;
	
	private String idSecurityCodePk;
	
	private String description;
	
	private String issuanceDate;
	
	private BigDecimal interestRate;
	
	private String couponNumber;
	
	private BigDecimal accountNumber;
	
	private String snBloqueo;
	
	private BigDecimal totalBalance;
	
	private BigDecimal availableBalance;
	
	private BigDecimal pawnBalance;
	
	private BigDecimal BanBalance;
	
	private BigDecimal acreditationBalance;
	
	private BigDecimal initialNominalValue;
	
	private BigDecimal monto;
	
	private BigDecimal currency;
	
	private String idIssuerPk;
	
	private Integer idHolderAccountPk;
	
	private BigDecimal reportingBalance;
	
	private String participantDescription;
	
	private String rut;
	
	private String holderDescription;
	
	private String motive;
	
	private String electronicCoupon;
	
	public Long getParticipant() {
		return participant;
	}

	public void setParticipant(Long participant) {
		this.participant = participant;
	}

	public String getIssuer() {
		return issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	public String getSecurityClass() {
		return securityClass;
	}

	public void setSecurityClass(String securityClass) {
		this.securityClass = securityClass;
	}

	public String getHolder() {
		return holder;
	}

	public void setHolder(String holder) {
		this.holder = holder;
	}

	public Integer getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(Integer holderAccount) {
		this.holderAccount = holderAccount;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Integer getBcbFormat() {
		return bcbFormat;
	}

	public void setBcbFormat(Integer bcbFormat) {
		this.bcbFormat = bcbFormat;
	}

	public Date getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	public String getWithoutDpf() {
		return withoutDpf;
	}

	public void setWithoutDpf(String withoutDpf) {
		this.withoutDpf = withoutDpf;
	}

	public String getInstitutionType() {
		return institutionType;
	}

	public void setInstitutionType(String institutionType) {
		this.institutionType = institutionType;
	}

	public String getParticipantInvestorInstitutionType() {
		return participantInvestorInstitutionType;
	}

	public void setParticipantInvestorInstitutionType(String participantInvestorInstitutionType) {
		this.participantInvestorInstitutionType = participantInvestorInstitutionType;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getDescriptionCu() {
		return descriptionCu;
	}

	public void setDescriptionCu(String descriptionCu) {
		this.descriptionCu = descriptionCu;
	}

	public String getMnemonicCu() {
		return mnemonicCu;
	}

	public void setMnemonicCu(String mnemonicCu) {
		this.mnemonicCu = mnemonicCu;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIssuanceDate() {
		return issuanceDate;
	}

	public void setIssuanceDate(String issuanceDate) {
		this.issuanceDate = issuanceDate;
	}

	public BigDecimal getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}

	public String getCouponNumber() {
		return couponNumber;
	}

	public void setCouponNumber(String couponNumber) {
		this.couponNumber = couponNumber;
	}

	public BigDecimal getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(BigDecimal accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getSnBloqueo() {
		return snBloqueo;
	}

	public void setSnBloqueo(String snBloqueo) {
		this.snBloqueo = snBloqueo;
	}

	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	public BigDecimal getPawnBalance() {
		return pawnBalance;
	}

	public void setPawnBalance(BigDecimal pawnBalance) {
		this.pawnBalance = pawnBalance;
	}

	public BigDecimal getBanBalance() {
		return BanBalance;
	}

	public void setBanBalance(BigDecimal banBalance) {
		BanBalance = banBalance;
	}

	public BigDecimal getAcreditationBalance() {
		return acreditationBalance;
	}

	public void setAcreditationBalance(BigDecimal acreditationBalance) {
		this.acreditationBalance = acreditationBalance;
	}

	public BigDecimal getInitialNominalValue() {
		return initialNominalValue;
	}

	public void setInitialNominalValue(BigDecimal initialNominalValue) {
		this.initialNominalValue = initialNominalValue;
	}

	public BigDecimal getMonto() {
		return monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

	public BigDecimal getCurrency() {
		return currency;
	}

	public void setCurrency(BigDecimal currency) {
		this.currency = currency;
	}

	public String getIdIssuerPk() {
		return idIssuerPk;
	}

	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}

	public Integer getIdHolderAccountPk() {
		return idHolderAccountPk;
	}

	public void setIdHolderAccountPk(Integer idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}

	public BigDecimal getReportingBalance() {
		return reportingBalance;
	}

	public void setReportingBalance(BigDecimal reportingBalance) {
		this.reportingBalance = reportingBalance;
	}

	public BigDecimal getTotalBalance() {
		return totalBalance;
	}

	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	public String getParticipantDescription() {
		return participantDescription;
	}

	public void setParticipantDescription(String participantDescription) {
		this.participantDescription = participantDescription;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getHolderDescription() {
		return holderDescription;
	}

	public void setHolderDescription(String holderDescription) {
		this.holderDescription = holderDescription;
	}	
	
	public String getMotive() {
		return motive;
	}

	public void setMotive(String motive) {
		this.motive = motive;
	}

	public String getElectronicCoupon() {
		return electronicCoupon;
	}

	public void setElectronicCoupon(String electronicCoupon) {
		this.electronicCoupon = electronicCoupon;
	}
	
}