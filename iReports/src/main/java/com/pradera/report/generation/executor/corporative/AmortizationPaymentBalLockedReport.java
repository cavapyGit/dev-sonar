package com.pradera.report.generation.executor.corporative;

import java.io.ByteArrayOutputStream;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.corporateevents.to.CorporativeOperationTO;
import com.pradera.core.component.helperui.service.SecuritiesQueryServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.corporative.service.CorporativeReportServiceBean;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

@ReportProcess(name="AmortizationPaymentBalLockedReport")
public class AmortizationPaymentBalLockedReport extends GenericReport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The corporative report service bean. */
	@EJB
	CorporativeReportServiceBean corporativeReportServiceBean;
	
	/** The securities query service bean. */
	@EJB
	SecuritiesQueryServiceBean securitiesQueryServiceBean;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	private static int DELIVERY_DATE 		= 0;
	private static int EVEN_TYPE 			= 1;
	private static int ISSUER_CODE_DESC 	= 2;
	private static int ISSUANCE 			= 3;
	private static int SEC_CODE_DESC 		= 4;
	private static int COUPON_NUM 			= 5;
	private static int CURRENCY_DESC 		= 6;
	private static int CUTOFF_DATE 			= 7;
	private static int REGISTRY_DATE 		= 8;
	private static int NOMINAL_VALUE 		= 9;
	private static int DELIVERY_FACTOR 		= 10;
	private static int TAX_AMOUNT 			= 11;
	private static int IND_ROUND 			= 12;
	private static int PART_MNEMO_CODE 		= 13;
	private static int ID_PARTICIPANT_PK 	= 14;
	private static int ACCOUNT_NUMBER 		= 15;
	private static int RNT_DESC 			= 16;
	private static int PAWN_BALANCE 		= 17;
	private static int BAN_BALANCE 			= 18;
	private static int OPPOSITION_BALANCE 	= 19;
	private static int RESERVE_BALANCE 		= 20;
	private static int OTHER_BLOCK_BALANCE 	= 21;
	private static int AMR_PAWN_BALANCE 	= 22;
	private static int AMR_BAN_BALANCE		= 23;
	private static int AMR_OPPO_BALANCE 	= 24;
	private static int AMR_RESERVE_BALANCE 	= 25;
	private static int AMR_OTHER_BALANCE 	= 26;
	private static int DOCUMENT_NUMBER 		= 27;
	private static int DOCUMENT_NUMBER_ACT	= 28;
	private static int BLOCK_ENTITY_DESC 	= 29;
	private static int STATE 				= 30;
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		boolean existData = false;
		CorporativeOperationTO corporativeOpTO = new CorporativeOperationTO();

		//Read parameters from DB, all parameters are string type
		for(ReportLoggerDetail loggerDetail : reportLogger.getReportLoggerDetails()) {	
			
			if(loggerDetail.getFilterName().equals(ReportConstant.CORP_PROCESS_ID) &&
					Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	){
				corporativeOpTO.setId(Long.valueOf(loggerDetail.getFilterValue()));
				}
			
			if(loggerDetail.getFilterName().equals(ReportConstant.CORPORATE_EVENT_TYPE) &&
					Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	) {
				corporativeOpTO.setCorporativeEventType(Integer.valueOf(loggerDetail.getFilterValue()));
				}
				
			if(loggerDetail.getFilterName().equals(ReportConstant.REPORT_TYPE) &&
					Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	){
				corporativeOpTO.setReportType(Integer.valueOf(loggerDetail.getFilterValue()));
				}
			if(loggerDetail.getFilterName().equals(ReportConstant.STATE_PARAM) &&
					Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	) {
				corporativeOpTO.setState(Integer.valueOf(loggerDetail.getFilterValue()));
				}
				
			if(loggerDetail.getFilterName().equals(ReportConstant.DEST_CODE_ISIN) &&
					Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	){
				corporativeOpTO.setTargetIsinCode(loggerDetail.getFilterValue());
				}
					
			if(loggerDetail.getFilterName().equals(ReportConstant.CUTOFF_DATE) &&
					Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	){
				corporativeOpTO.setCutOffDate(CommonsUtilities.convertStringtoDate(loggerDetail.getFilterValue()));
				}
			
			if(loggerDetail.getFilterName().equals(ReportConstant.DELIVERY_DATE) &&
					Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	) {
				corporativeOpTO.setDeliveryDate(CommonsUtilities.convertStringtoDate(loggerDetail.getFilterValue()));
				}
				
			if(loggerDetail.getFilterName().equals(ReportConstant.CODE_ISIN) &&
					Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	){
				corporativeOpTO.setSourceIsinCode(loggerDetail.getFilterValue());
				}
		}
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		try {
			//GETTING ALL DATA FROM QUERY
			List<Object[]> listObjects = corporativeReportServiceBean.getAmortizationPaymentBalLocked(corporativeOpTO);

			Security sec = securitiesQueryServiceBean.getSecurityHelpServiceBean(corporativeOpTO.getSourceIsinCode());
			
			if(Validations.validateIsNotNullAndNotEmpty(listObjects) && listObjects.size()>0){
				existData = true;
			}
			
			XMLOutputFactory xof = XMLOutputFactory.newInstance();
			XMLStreamWriter xmlsw;
			
			
				xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(baos));
				//open document file
				xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");
				
				//REPORT TAG
				xmlsw.writeStartElement(ReportConstant.REPORT);
				
				
					//HEADER
					createHeaderReport(xmlsw,reportLogger);	
					
					xmlsw.writeStartElement("headerReport"); //START HEADER REPORT ELEMENT	
						
						createTagString(xmlsw, "SEC_CODE_DESC", sec.getIdSecurityCodePk()+" - "+sec.getDescription());
						createTagString(xmlsw, "ISSUER_CODE_DESC", sec.getIssuer().getIdIssuerPk()+" - "+sec.getIssuer().getBusinessName());
						createTagString(xmlsw, "CUTOFF_DATE", CommonsUtilities.convertDateToString(corporativeOpTO.getCutOffDate(),"dd/MM/YYYY"));
						createTagString(xmlsw, "NOMINAL_VALUE", sec.getCurrentNominalValue());
						createTagString(xmlsw, "ISSUANCE", sec.getIssuance().getIdIssuanceCodePk());
						createTagString(xmlsw, "CURRENCY_DESC", CurrencyType.get(sec.getCurrency()).getValue());
						createTagString(xmlsw, "DELIVERY_DATE", CommonsUtilities.convertDateToString(corporativeOpTO.getDeliveryDate(),"dd/MM/YYYY"));
						
						
						if(existData){
							
						createTagString(xmlsw, "DELIVERY_FACTOR", listObjects.get(0)[DELIVERY_FACTOR].toString());
						createTagString(xmlsw, "REGISTRY_DATE", listObjects.get(0)[REGISTRY_DATE].toString());
						createTagString(xmlsw, "TAX_AMOUNT", listObjects.get(0)[TAX_AMOUNT].toString());
						createTagString(xmlsw, "COUPON_NUM", listObjects.get(0)[COUPON_NUM].toString());
						createTagString(xmlsw, "IND_ROUND", listObjects.get(0)[IND_ROUND].toString());
						
						//BODY
							createBodyReport(xmlsw, listObjects);
						}
						
					xmlsw.writeEndElement(); //END HEADER REPORT ELEMENT					

			
				//END REPORT TAG
				xmlsw.writeEndElement();
				//close document
				xmlsw.writeEndDocument();
				xmlsw.flush();
		        xmlsw.close();
					
		} catch (XMLStreamException | ServiceException e) {
			log.error(e.getMessage());
			e.printStackTrace();
			throw new RuntimeException();
		} 
		return baos;
	}

	public void createBodyReport(XMLStreamWriter xmlsw,List<Object[]> listObjects)  throws XMLStreamException{
		xmlsw.writeStartElement("rows"); //START ROWS TAG	

		//BY ROW
		for(Object[] obj : listObjects){
			xmlsw.writeStartElement("row"); //START ROW TAG
			
			createTagString(xmlsw, "ID_PARTICIPANT_PK", obj[ID_PARTICIPANT_PK]);
			createTagString(xmlsw, "ACCOUNT_NUMBER", obj[ACCOUNT_NUMBER]);
			createTagString(xmlsw, "RNT_DESC", obj[RNT_DESC]);
			createTagString(xmlsw, "PAWN_BALANCE", obj[PAWN_BALANCE]);
			createTagString(xmlsw, "BAN_BALANCE", obj[BAN_BALANCE]);
			createTagString(xmlsw, "OPPOSITION_BALANCE", obj[OPPOSITION_BALANCE]);
			createTagString(xmlsw, "OTHER_BLOCK_BALANCE", obj[OTHER_BLOCK_BALANCE]);
			createTagString(xmlsw, "AMR_PAWN_BALANCE", obj[AMR_PAWN_BALANCE]);
			createTagString(xmlsw, "AMR_BAN_BALANCE", obj[AMR_BAN_BALANCE]);
			createTagString(xmlsw, "AMR_OPPO_BALANCE", obj[AMR_OPPO_BALANCE]);
			createTagString(xmlsw, "AMR_RESERVE_BALANCE", obj[AMR_RESERVE_BALANCE]);
			createTagString(xmlsw, "AMR_OTHER_BALANCE", obj[AMR_OTHER_BALANCE]);
			createTagString(xmlsw, "DOCUMENT_NUMBER", obj[DOCUMENT_NUMBER]);
			createTagString(xmlsw, "DOCUMENT_NUMBER_ACT", obj[DOCUMENT_NUMBER_ACT]);
			createTagString(xmlsw, "BLOCK_ENTITY_DESC", obj[BLOCK_ENTITY_DESC]);
			createTagString(xmlsw, "STATE", obj[STATE]);
			
			xmlsw.writeEndElement();	//END ROWS TAG
		}
	}
	

}
