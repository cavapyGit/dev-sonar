package com.pradera.report.generation.executor.corporative;

import java.io.ByteArrayOutputStream;
import java.math.RoundingMode;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.corporateevents.facade.CoporateEventComponentFacade;
import com.pradera.core.component.corporateevents.to.CorporativeOperationSubQueryTO;
import com.pradera.core.component.corporateevents.to.CorporativeOperationTO;
import com.pradera.core.component.helperui.service.SecuritiesQueryServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.corporative.service.CorporativeReportServiceBean;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

@ReportProcess(name="CashDividendsPaidByBalLockedReport")
public class CashDividendsPaidByBalLockedReport extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The corporative report service bean. */
	@EJB
	CorporativeReportServiceBean corporativeReportServiceBean;
	
	/** The securities query service bean. */
	@EJB
	SecuritiesQueryServiceBean securitiesQueryServiceBean;
	
	@EJB
	CoporateEventComponentFacade corporateServiceFacade;
	
	//QUERY COLUMN NAMES
	private static int state 					   = 0;
	private static int iss 						   = 1;
	private static int CURRENCY 				   = 2;
	private static int cutoff_date 				   = 3;
	private static int registry_date 			   = 4;
	private static int VALOR 					   = 5;
	private static int tax_factor 				   = 6;
	private static int ind_round 				   = 7;
	private static int id_issuance_code_fk 		   = 8;
	private static int payment_date 			   = 9;
	private static int account_number 			   = 10;
	private static int interest_factor 			   = 11;
	private static int security 				   = 12;
	private static int nominal_value 			   = 13;
	private static int holder 					   = 14;
	private static int id_isin_code_fk 			   = 15;
	private static int SALDO_PRENDA 			   = 16;
	private static int SALDO_EMBARGO 			   = 17;
	private static int SALDO_OPOSICION 			   = 18;
	private static int SALDO_ENCAJE 			   = 19;
	private static int SALDO_OTROS 				   = 20;
	private static int TOTAL_BLOCK 				   = 21;
	private static int participant 				   = 22;
	private static int participant_id			   = 23;
	private static int INTERES_PRENDA 			   = 24;
	private static int INTERES_EMBARGO 			   = 25;
	private static int INTERES_OPOSICION 		   = 26;
	private static int INTERES_ENCAJE 			   = 27;
	private static int INTERES_OTROS 			   = 22;
	private static int ID_HOLDER_ACCOUNT_PK		   = 23;
	private static int ID_HOLDER_PK 			   = 24;
	private static int TOTAL_INTREST 			   = 25;
	private static int cpr_id 					   = 26;
	private static int id_corporative_operation_pk = 27;
	private static int HOLDER_COUNT 			   = 28;
	
	private static int DOCUMENT_NUMBER 			   = 0;
	private static int block_number 			   = 1;
	private static int block_type 				   = 2;
	private static int block_balance 			   = 3;
	private static int BENEFICIO 			       = 4;
	private static int block_entity 			   = 5;
	
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		boolean existData = false;
		CorporativeOperationTO corporativeOpTO = new CorporativeOperationTO();
		
		//Read parameters from DB, all parameters are string type
		for(ReportLoggerDetail loggerDetail : reportLogger.getReportLoggerDetails()) {	
			
			if(loggerDetail.getFilterName().equals(ReportConstant.CORP_PROCESS_ID) &&
					Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	){
				corporativeOpTO.setId(Long.valueOf(loggerDetail.getFilterValue()));
				}
			
			if(loggerDetail.getFilterName().equals(ReportConstant.CORPORATE_EVENT_TYPE) &&
					Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	) {
				corporativeOpTO.setCorporativeEventType(Integer.valueOf(loggerDetail.getFilterValue()));
				}
				
			if(loggerDetail.getFilterName().equals(ReportConstant.REPORT_TYPE) &&
					Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	){
				corporativeOpTO.setReportType(Integer.valueOf(loggerDetail.getFilterValue()));
				}
			if(loggerDetail.getFilterName().equals(ReportConstant.STATE_PARAM) &&
					Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	) {
				corporativeOpTO.setState(Integer.valueOf(loggerDetail.getFilterValue()));
				}
				
			if(loggerDetail.getFilterName().equals(ReportConstant.DEST_CODE_ISIN) &&
					Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	){
				corporativeOpTO.setTargetIsinCode(loggerDetail.getFilterValue());
				}
					
			if(loggerDetail.getFilterName().equals(ReportConstant.CUTOFF_DATE) &&
					Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	){
				corporativeOpTO.setCutOffDate(CommonsUtilities.convertStringtoDate(loggerDetail.getFilterValue()));
				}
			
			if(loggerDetail.getFilterName().equals(ReportConstant.DELIVERY_DATE) &&
					Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	) {
				corporativeOpTO.setDeliveryDate(CommonsUtilities.convertStringtoDate(loggerDetail.getFilterValue()));
				}
				
			if(loggerDetail.getFilterName().equals(ReportConstant.CODE_ISIN) &&
					Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	){
				corporativeOpTO.setSourceIsinCode(loggerDetail.getFilterValue());
				}
		}
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
				
		try {
			//GETTING ALL DATA FROM QUERY
			List<Object[]> listObjects = corporativeReportServiceBean.getCashDividendsPaidByBalLocked(corporativeOpTO);

			Security sec = securitiesQueryServiceBean.getSecurityHelpServiceBean(corporativeOpTO.getSourceIsinCode());
			
			CorporativeOperation corporativeOperation = corporateServiceFacade.findCorporativeOperationByFilter(corporativeOpTO);
			
			if(Validations.validateIsNotNullAndNotEmpty(listObjects) && listObjects.size()>0){
				existData = true;
			}
			
			XMLOutputFactory xof = XMLOutputFactory.newInstance();
			XMLStreamWriter xmlsw;
			
			
				xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(baos));
				//open document file
				xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");
				
				//REPORT TAG
				xmlsw.writeStartElement(ReportConstant.REPORT);
				
				//HEADER
				createHeaderReport(xmlsw,reportLogger);	
					
				createTagString(xmlsw, "VALOR", sec.getIdSecurityCodePk()+" - "+sec.getDescription());
				createTagString(xmlsw, "EMISOR", sec.getIssuer().getIdIssuerPk()+" - "+sec.getIssuer().getBusinessName());
				createTagString(xmlsw, "CUTOFF_DATE", CommonsUtilities.convertDateToString(corporativeOpTO.getCutOffDate(),"dd/MM/YYYY"));
				createTagString(xmlsw, "VALOR_NOMINAL", sec.getCurrentNominalValue().setScale(2, RoundingMode.HALF_DOWN).toString());
				createTagString(xmlsw, "ID_ISSUANCE_CODE_FK", sec.getIssuance().getIdIssuanceCodePk());
				createTagString(xmlsw, "CURRENCY", CurrencyType.get(sec.getCurrency()).getValue());
				createTagString(xmlsw, "DELIVERY_DATE", CommonsUtilities.convertDateToString(corporativeOpTO.getDeliveryDate(),"dd/MM/YYYY"));
				createTagString(xmlsw, "FACTOR_INT", corporativeOperation.getIndTax());
				createTagString(xmlsw, "REGISTRY_DATE",CommonsUtilities.convertDateToString(corporativeOperation.getRegistryDate(),"dd/MM/YYYY"));
				createTagString(xmlsw, "IMPUESTO", corporativeOperation.getTaxAmount());
				createTagString(xmlsw, "IND_ROUND", corporativeOperation.getIndRound());
				createTagString(xmlsw, "state", CorporateProcessStateType.get(corporativeOperation.getState()).getValue());
	
				//BODY
				if(existData){		
					createBodyReport(xmlsw, listObjects);					
				}
				
				//END REPORT TAG
				xmlsw.writeEndElement();
				//close document
				xmlsw.writeEndDocument();
				xmlsw.flush();
		        xmlsw.close();
					
		} catch (XMLStreamException | ServiceException e) {
			log.error(e.getMessage());
			e.printStackTrace();
			throw new RuntimeException();
		} 
		
		return baos;
	}
			
	public void createBodyReport(XMLStreamWriter xmlsw,List<Object[]> listObjects)  throws XMLStreamException{
		CorporativeOperationSubQueryTO copTO = new CorporativeOperationSubQueryTO();
		
		Object[] objtArray = listObjects.get(0);
		
		copTO.setCorporativeOperationId(Long.valueOf(objtArray[id_corporative_operation_pk].toString()));
		copTO.setCorporativeProcessResultId(Long.valueOf(objtArray[cpr_id].toString()));
		copTO.setParticipantId(Long.valueOf(objtArray[participant_id].toString()));
		copTO.setHolderAccountId(Long.valueOf(objtArray[ID_HOLDER_ACCOUNT_PK].toString()));
		
		//GETTING ALL DATA FROM SUBQUERY
		List<Object[]> lstObjectSQ = corporativeReportServiceBean.getCashDividendsPaidByBalLockedSubQuery(copTO);
		
		
		xmlsw.writeStartElement("participant"); //START PARTICIPANT REPORT ELEMENT	
		
		for(Object[] obj : listObjects){
			xmlsw.writeStartElement("data"); //START DATA ELEMENT	
			createTagString(xmlsw, "participant_id", obj[participant_id]);
			createTagString(xmlsw, "participant", obj[participant_id]+" - "+obj[participant]);
			createTagString(xmlsw, "account_number", obj[account_number]);
			createTagString(xmlsw, "holder", obj[holder]);
			createTagString(xmlsw, "SALDO_PRENDA", obj[SALDO_PRENDA]);
			createTagString(xmlsw, "SALDO_EMBARGO", obj[SALDO_EMBARGO]);
			createTagString(xmlsw, "SALDO_OPOSICIÓN", obj[SALDO_OPOSICION]);
			createTagString(xmlsw, "SALDO_ENCAJE", obj[SALDO_ENCAJE]);
			createTagString(xmlsw, "SALDO_OTROS", obj[SALDO_OTROS]);
			createTagString(xmlsw, "TOTAL_BLOCK", obj[TOTAL_BLOCK]);
			createTagString(xmlsw, "INTERES_PRENDA", obj[INTERES_PRENDA]);
			createTagString(xmlsw, "INTERES_EMBARGO", obj[INTERES_EMBARGO]);
			createTagString(xmlsw, "INTERES_OPOSICIÓN", obj[INTERES_OPOSICION]);
			createTagString(xmlsw, "INTERES_ENCAJE", obj[INTERES_ENCAJE]);
			createTagString(xmlsw, "INTERES_OTROS", obj[INTERES_OTROS]);
			createTagString(xmlsw, "TOTAL_INTREST", obj[TOTAL_INTREST]);
			createTagString(xmlsw, "HOLDER_COUNT", obj[HOLDER_COUNT]);
			xmlsw.writeEndElement(); //END DATA ELEMENT
		}	

		//DETAILS
		for(Object[] obj : lstObjectSQ){
			xmlsw.writeStartElement("row"); //START ROW ELEMENT	
			createTagString(xmlsw, "DOCUMENT_NUMBER", obj[DOCUMENT_NUMBER]);
			createTagString(xmlsw, "block_number", obj[block_number]);
			createTagString(xmlsw, "block_type", obj[block_type]);
			createTagString(xmlsw, "block_balance", obj[block_balance]);
			createTagString(xmlsw, "BENEFICIO", obj[BENEFICIO]);
			createTagString(xmlsw, "block_entity", obj[block_entity]);
			xmlsw.writeEndElement(); //END ROW ELEMENT
		}

		xmlsw.writeEndElement(); //END PARTICIPANT REPORT ELEMENT
	}

			
}
