package com.pradera.report.generation.executor.securities.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class PortfolioVauatorDailyReportServiceBean extends CrudDaoServiceBean {

	public PortfolioVauatorDailyReportServiceBean() {
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("unchecked")
	public List<Long> getAccountsID(Long idHolderPk, Long idParticipantPk) throws ServiceException{
		StringBuilder idAccountSql = new StringBuilder();
		Map<String, Object> parametersIdAccount = new HashMap<String, Object>();
		idAccountSql.append("Select ha.idHolderAccountPk From HolderAccount	ha				");
		idAccountSql.append("Inner Join	ha.holderAccountDetails det							");
		idAccountSql.append("Where 1 = 1 													");
		if (idParticipantPk != null) {
			idAccountSql.append("And ha.participant.idParticipantPk = :idParticipantPk ");
			parametersIdAccount.put("idParticipantPk", idParticipantPk);
		}
		if (idHolderPk != null) {
			idAccountSql.append("And det.holder.idHolderPk = :idHolderPk ");
			parametersIdAccount.put("idHolderPk", idHolderPk);
		}
		idAccountSql.append(" And ha.stateAccount in (:accountStateActive,:accountStateBlock)");
		parametersIdAccount.put("accountStateActive", HolderAccountStatusType.ACTIVE.getCode());
		parametersIdAccount.put("accountStateBlock", HolderAccountStatusType.BLOCK.getCode());
		return findListByQueryString(idAccountSql.toString(),parametersIdAccount);
	}
}
