package com.pradera.report.generation.executor.settlement;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.settlement.service.SettlementReportServiceBean;
import com.pradera.report.generation.executor.settlement.to.GenericSettlementOperationTO;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "TransferBuySalesReport")
public class TransferBuySalesReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	private SettlementReportServiceBean settlementReportServiceBean;

	@Inject
	private PraderaLogger log;

	public TransferBuySalesReport() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {

		GenericSettlementOperationTO gfto = new GenericSettlementOperationTO();

		for(ReportLoggerDetail param: getReportLogger().getReportLoggerDetails()){
			if(param.getFilterName().equals("mechanism")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setMechanism(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("modality")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setModality(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("role")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setRole(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("issuer_code")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setIssuer(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("security_class_key")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setIdSecurityCodePk(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("operation_state")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setOperationState(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("operation_number")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setOperationNumber(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("seller_participant")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setSellerParticipant(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("buyer_participant")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setBuyerParticipant(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("ballot_number")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setBallotNumber(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("sequential")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setSequential(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("date_initial")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setInitialDate(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("date_end")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setFinalDate(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals(ReportConstant.INSTITUTION_TYPE)){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setInstitutionType(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals(ReportConstant.ISSUER_CODE_INSTITUTION_TYPE)){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setIssuer(param.getFilterValue().toString());
				}
			}
		}
		String strQuery = settlementReportServiceBean.getQueryTransferBuySalesReport(gfto); 
		 
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> secState = new HashMap<Integer, String>();
		try {
			filter.setMasterTableFk(MasterTableType.ESTADOS_OPERACION_MCN.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secState.put(param.getParameterTablePk(), param.getDescription());
			}
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		parametersRequired.put("str_Query", strQuery);
		parametersRequired.put("mStateOperation", secState);
		parametersRequired.put("initialDt", gfto.getInitialDate());
		parametersRequired.put("finalDt", gfto.getFinalDate());
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		
		return parametersRequired;
	}

}
