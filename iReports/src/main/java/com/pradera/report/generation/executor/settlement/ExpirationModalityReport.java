package com.pradera.report.generation.executor.settlement;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.settlement.service.SettlementReportServiceBean;
import com.pradera.report.generation.executor.settlement.to.GenericSettlementOperationTO;

@ReportProcess(name="ExpirationModalityReport")
public class ExpirationModalityReport extends GenericReport{
	
	private static final long serialVersionUID = 1L;
	
	@Inject PraderaLogger log;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	ParticipantServiceBean participantServiceBean;
	
	@EJB
	private SettlementReportServiceBean settlementReportServiceBean;
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		GenericSettlementOperationTO genericSettlementOperationTO = new GenericSettlementOperationTO();
		Map<String,Object> parametersRequired = new HashMap<>();

		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		String participantDescription = null;
		String modalityDescription = null;
		String currencyDescription = null;
		String dateTypeDescription = null;
		String expirationTypeDescription = null;
		String holderDescription = null;
		String roleDescription = null;
		String securityClassDescription = null;
	
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.PARTICIPANT_PARAM))
				if(r.getFilterValue() != null){
					participantDescription = r.getFilterDescription();
					genericSettlementOperationTO.setParticipant(r.getFilterValue());
					genericSettlementOperationTO.setIssuer(getReportLogger().getRegistryUser());
				}else{
					participantDescription = "TODOS";
				}
			if (r.getFilterName().equals(ReportConstant.MODALITY_PARAM))
				if(r.getFilterValue() != null){
					modalityDescription = r.getFilterDescription();
					genericSettlementOperationTO.setModalityGroup(r.getFilterValue());
				}else{
					modalityDescription = "TODOS";
				}
			if (r.getFilterName().equals(ReportConstant.CURRENCY))
				if(r.getFilterValue() != null){
					currencyDescription = r.getFilterDescription();
					genericSettlementOperationTO.setCurrency(r.getFilterValue());
				}else{
					currencyDescription = "TODOS";
				}
			if (r.getFilterName().equals(ReportConstant.DATE_TYPE_PARAM))
				if(r.getFilterValue() != null){
					dateTypeDescription = r.getFilterDescription();
					genericSettlementOperationTO.setDateType(r.getFilterValue());
				}else{
					dateTypeDescription = "TODOS";
				}
			if (r.getFilterName().equals(ReportConstant.EXPIRATION_TYPE_PARAM))
				if(r.getFilterValue() != null){
					expirationTypeDescription = r.getFilterDescription();
					genericSettlementOperationTO.setExpirationType(r.getFilterValue());
				}else{
					expirationTypeDescription = "TODOS";
				}
			if (r.getFilterName().equals(ReportConstant.CUI_HOLDER_PARAM))
				if(r.getFilterValue() != null){
					holderDescription = r.getFilterValue();
					genericSettlementOperationTO.setCui(r.getFilterValue());
				}else{
					holderDescription = "TODOS";
				}
			if (r.getFilterName().equals(ReportConstant.ROLE_PARAM))
				if(r.getFilterValue() != null){
					roleDescription = r.getFilterDescription();
					genericSettlementOperationTO.setRole(r.getFilterValue());
				}else{
					roleDescription = "TODOS";
				}
			if (r.getFilterName().equals(ReportConstant.SECURITY_PARAM))
				if(r.getFilterValue() != null){
					securityClassDescription = r.getFilterDescription();
					genericSettlementOperationTO.setIdSecurityCodePk(r.getFilterValue());
				}else{
					securityClassDescription = "TODOS";
				}
			if (r.getFilterName().equals(ReportConstant.DATE_INITIAL_PARAM))
				genericSettlementOperationTO.setInitialDate(r.getFilterValue());
			if (r.getFilterName().equals(ReportConstant.DATE_END_PARAM))
				genericSettlementOperationTO.setFinalDate(r.getFilterValue());
		}

		
		//VALIDAMOS SI ES TIPO PARTICIPANTE INVERSIONISTA
		/*clientPortafolioTO.setIsParticipantInvestor(false);
		if(clientPortafolioTO.getInstitutionType()!=null && Integer.valueOf(clientPortafolioTO.getInstitutionType()).equals(InstitutionType.PARTICIPANT_INVESTOR.getCode())){
			clientPortafolioTO.setIsParticipantInvestor(true);
		}*/
		
		String strQuery = settlementReportServiceBean.getExpirationModalityReport(genericSettlementOperationTO); 
		
		parametersRequired.put("str_query", strQuery);
		System.out.println(strQuery);
		parametersRequired.put("participant_description", participantDescription);
		parametersRequired.put("holder_description", holderDescription);
		parametersRequired.put("role_description", roleDescription);
		parametersRequired.put("security_class_description", securityClassDescription);
		parametersRequired.put("currency_description", currencyDescription);
		parametersRequired.put("expiration_type_description", expirationTypeDescription);
		parametersRequired.put("date_type_description", dateTypeDescription);
		parametersRequired.put("modality_description", modalityDescription);
		
		return parametersRequired;
	}

}
