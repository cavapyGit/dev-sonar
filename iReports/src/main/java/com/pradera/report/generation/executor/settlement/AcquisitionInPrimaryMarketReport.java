package com.pradera.report.generation.executor.settlement;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;

import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.settlement.service.SettlementReportServiceBean;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;
import com.pradera.report.generation.service.ReportUtilServiceBean;

@ReportProcess(name = "AcquisitionInPrimaryMarketReport")
public class AcquisitionInPrimaryMarketReport extends GenericReport{

	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	private ParticipantServiceBean participantServiceBean;
	
	@EJB
	private ReportUtilServiceBean reportUtilServiceBean;
	
	@EJB
	private SettlementReportServiceBean settlementReportServiceBean;

	public AcquisitionInPrimaryMarketReport() {
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParametersQueryReport() {

	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		//Reporte solo en formato TXT
		GenericsFiltersTO gfto = new GenericsFiltersTO();
		for(ReportLoggerDetail param: getReportLogger().getReportLoggerDetails()){
			if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
				
				//Filtros de Fecha
				if(param.getFilterName().equals("date_init"))
						gfto.setInitialDt(param.getFilterValue().toString());
				if(param.getFilterName().equals("date_end"))
						gfto.setFinalDt(param.getFilterValue().toString());
			}
		}
		//Metodo para generar el archivo
		generateReportTXT(gfto);
		Map<String, Object>  map = new HashMap<String, Object>();
    	return map;
	}
	
	/**
	 * Metodo para la generacion en TXT de los datos del reporte 3 ASFI
	 * @param gfto
	 * @throws ServiceException
	 */
	private void generateReportTXT(GenericsFiltersTO gfto){
		
		//Declaramos variables
		StringBuilder sbTxtFormat = new StringBuilder();
		//Generamos el nombre del archiv
		String fileName = getNameAccordingFund(gfto)+"TXT";
		//Query con la informacion que saldra en el reporte
		List <Object[]> listToRep = settlementReportServiceBean.getAcquisitionInPrimaryMarketBBVobj(gfto);
		
		//Si el reporte tiene datos 
		if(listToRep.size() > GeneralConstants.ZERO_VALUE_INTEGER){
			
			boolean firstLoop = true;
			
			for (Object [] unitString : listToRep){
				
				if(firstLoop){
					firstLoop = false;
				}else{
					//Salto de linea
					sbTxtFormat.append(GeneralConstants.STR_END_LINE_TXT);
				}
				
				//Concatenamos todo en una sola cadena
				sbTxtFormat.append(GeneralConstants.STR_COMILLA);
				sbTxtFormat.append(unitString[0] != null ? unitString[0].toString() : "");
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				sbTxtFormat.append(unitString[1] != null ? unitString[1].toString() : "");
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				sbTxtFormat.append(unitString[2] != null ? unitString[2].toString() : "");
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				sbTxtFormat.append(unitString[3] != null ? unitString[3].toString() : "");
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				sbTxtFormat.append(unitString[4] != null ? unitString[4].toString() : "");
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				sbTxtFormat.append(unitString[5] != null ? unitString[5].toString() : "");
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				sbTxtFormat.append(unitString[6] != null ? unitString[6].toString() : "");
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				sbTxtFormat.append(unitString[7] != null ? unitString[7].toString() : "");
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				sbTxtFormat.append(unitString[8] != null ? unitString[8].toString() : "");
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				sbTxtFormat.append(unitString[9] != null ? unitString[9].toString() : "0");
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				sbTxtFormat.append(unitString[10] != null ? unitString[10].toString() : "0");
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				sbTxtFormat.append(unitString[11] != null ? unitString[11].toString() : "0");
				sbTxtFormat.append(GeneralConstants.STR_COMILLA + GeneralConstants.STR_COMMA_WITHOUT_SPACE + GeneralConstants.STR_COMILLA);
				sbTxtFormat.append(unitString[12] != null ? unitString[12].toString() : "");
				sbTxtFormat.append(GeneralConstants.STR_COMILLA);
				
			}
			//Generamos el archivo 
			reportUtilServiceBean.saveCustomReportFile(fileName, ReportFormatType.TXT.getCode(), sbTxtFormat.toString().getBytes(), getReportLogger());
		}else{
			//Si el reporte no tiene datos
			sbTxtFormat.append(GeneralConstants.STR_COMILLA + "SIN DATOS" + GeneralConstants.STR_COMILLA);
			reportUtilServiceBean.saveCustomReportFile(fileName, ReportFormatType.TXT.getCode(), sbTxtFormat.toString().getBytes(), getReportLogger());
		}
	}
	
	/**
	 * Metodo para generar el nombre del archivo
	 * @param gfto
	 * @return
	 */
	private String getNameAccordingFund(GenericsFiltersTO gfto){
		String name="";
		String firstWord="M";
		String strDate= gfto.getFinalDt();
		String strDay = strDate.substring(0,2);
		String strMonth = strDate.substring(3,5);
		String strYear = strDate.substring(8,10);
		String lastWord="AS";
		String reportCode="";

		name=firstWord;
		name+=strYear+strMonth+strDay;
		name+=lastWord;
		name+=GeneralConstants.DOT+reportCode;
		return name;
	}
}
