package com.pradera.report.generation.executor.billing;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.model.billing.BillingEconomicActivity;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.type.BaseCollectionType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.billing.service.BillingServiceReportServiceBean;
import com.pradera.report.generation.service.ReportUtilServiceBean;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ConsolidatedBillingPreliminaryReport.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@ReportProcess(name="ConsolidatedBillingPreliminaryReport")
public class ConsolidatedBillingPreliminaryReport extends GenericReport{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The report util service. */
	@EJB
	ReportUtilServiceBean reportUtilService;
	
	/** The billin report service. */
	@Inject
	private BillingServiceReportServiceBean billinReportService;
	
	/** The parameter service. */
	@EJB
	private ParameterServiceBean parameterService;
	
	
	/** Parameter outPut Billing. */
	private static int INDEX_NUMCXC= 0;
	
	/** The index account. */
	private static int INDEX_ACCOUNT=1;
	
	/** The index client. */
	private static int INDEX_CLIENT=2;
	
	/** The index description. */
	private static int INDEX_DESCRIPTION=3;
	
	/** The index impuesto. */
	private static int INDEX_IMPUESTO=4;
	
	/** The index moneda. */
	private static int INDEX_MONEDA=5;
	
	/** The index cantidad. */
	private static int INDEX_CANTIDAD=6;
	
	/** The index precio. */
	private static int INDEX_PRECIO=7;
	
	/** The index fecobro. */
	private static int INDEX_FECOBRO=8;
	
	/** The index tiposervicio. */
	private static int INDEX_TIPOSERVICIO=9;
	
	/** The index ref aux. */
	private static int INDEX_REF_AUX=10;
	
	/** The index ref aux. */
	private static int INDEX_BASE_COLLECTION=11;
	
	/** The index ref aux. */
	private static int INDEX_ENTITY_DESCRIPTION=12;
	
	/** The  Index Mnemonic Holder*/
	private static int INDEX_MNEMONIC_HOLDER=13;

	/**
	 * Instantiates a new consolidated billing preliminary report.
	 */
	public ConsolidatedBillingPreliminaryReport(){
		
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#getCustomJasperParameters()
	 */
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Long processBillingCalculationPk=null;
		
		Map<String,Object> parametersRequired = new HashMap<>();


		try {
			for(ReportLoggerDetail loggerDetail : getReportLogger().getReportLoggerDetails()) {
				
				if(loggerDetail.getFilterName().equals(ReportConstant.PROCESS_BILLING_CALCULATION_PK)){
					//SET THE PARAMETER [PROCESS BILLING PK]
					if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
						
						processBillingCalculationPk=Long.parseLong(loggerDetail.getFilterValue());
						
					}
				}

				ByteArrayOutputStream byteArrayFile = this.generateFileXml(processBillingCalculationPk);
				
//				reportUtilService.saveFileXMlInterfaceBilling(byteArrayFile,processBillingCalculationPk, getReportLogger());
			}


		
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		parametersRequired.put(ReportConstant.PROCESS_BILLING_CALCULATION_PK, processBillingCalculationPk);
		
		
		return parametersRequired;
	}

	
	/**
	 * Generate file xml.
	 *
	 * @param processBillingCalculationPk the process billing calculation pk
	 * @return the byte array output stream
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public ByteArrayOutputStream generateFileXml(Long processBillingCalculationPk) throws IOException{

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ByteArrayOutputStream baosReturn = new ByteArrayOutputStream();
		try {
			List<Object[]> billing=this.billinReportService.getAccountReceivable(processBillingCalculationPk);//processBillingCalculationPk
			XMLOutputFactory xof = XMLOutputFactory.newInstance();
			XMLStreamWriter xmlsw;
			
			xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(baos));
			xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");
			//root tag of report
			/**
			 * SERVICES
			 * */
			xmlsw.writeStartElement(ReportConstant.TAG_VENTAS);

		
		if(!billing.isEmpty()) {
			//method specific to each implementation of reports
			createBodyReport(xmlsw,  billing);
		} else {
			//when not exist data we need to create empty structure xml
			xmlsw.writeStartElement(ReportConstant.TAG_CXC);
			createTagString(xmlsw, ReportConstant.TAG_NUMCXC,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, ReportConstant.TAG_ACCOUNT,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, ReportConstant.TAG_CLIENT,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, ReportConstant.TAG_CLIENTE_FACTURAR, GeneralConstants.EMPTY_STRING  );
			createTagString(xmlsw, ReportConstant.TAG_DESCRIPTION,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, ReportConstant.TAG_IMPUESTO,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, ReportConstant.TAG_MONEDA,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, ReportConstant.TAG_CANTIDAD,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, ReportConstant.TAG_PRECIO,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, ReportConstant.TAG_FECOBRO,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, ReportConstant.TAG_TIPOSERVICIO,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, ReportConstant.TAG_REF_AUX,  GeneralConstants.EMPTY_STRING );
			xmlsw.writeEndElement();
		}

		//close report tag SERVICES
		xmlsw.writeEndElement();
		//close document
		xmlsw.writeEndDocument();
		xmlsw.flush();
        xmlsw.close();
        
        String replaceQuote=baos.toString();
        replaceQuote=replaceQuote.replace("'", "\"");
        baosReturn.write(replaceQuote.getBytes());
        
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return baosReturn;
	}
	
	/**
	 * Creates the body report.
	 * good practice factoring
	 *
	 * @param xmlsw the xmlsw
	 * @param billings the billings
	 * @throws XMLStreamException the xML stream exception
	 */
	public void createBodyReport(XMLStreamWriter xmlsw,	List<Object[]> billings)  throws XMLStreamException{
		int i=1;
		for(Object[] billing : billings) {
			//billing[7]=CommonsUtilities.currentDate();
			Integer baseCollection=Integer.parseInt(billing[INDEX_BASE_COLLECTION].toString());
			Long idBillignServicePk=billing[14]!=null?new Long(billing[14].toString()):0l;
			BillingService billingService = billinReportService.find(BillingService.class,idBillignServicePk );
			List<BillingEconomicActivity> listEconActBd=billinReportService.getBillingEconomicActivityByBillingServicePk(billingService.getIdBillingServicePk());
			List<Integer> listEconomicAct=billinReportService.getBillingEconomicActivityListInteger(listEconActBd);
			Integer portFolio=billingService.getPortfolio();
			String mnemonicHolder=null;
			if(billinReportService.isAfpPortfolioClient(listEconomicAct, portFolio)) {
				mnemonicHolder="TGN";
			}else {
				mnemonicHolder=billing[INDEX_MNEMONIC_HOLDER].toString().trim();
			}
			
			
			xmlsw.writeStartElement(ReportConstant.TAG_CXC);
			//detail report
			createTagString(xmlsw, ReportConstant.TAG_NUMCXC,  i );
			createTagString(xmlsw, ReportConstant.TAG_ACCOUNT,  billing[INDEX_ACCOUNT].toString().trim().concat(billing[INDEX_CLIENT].toString().trim()) );
			createTagString(xmlsw, ReportConstant.TAG_CLIENT,  billing[INDEX_CLIENT].toString().trim() );
			createTagString(xmlsw, ReportConstant.TAG_CLIENTE_FACTURAR,  mnemonicHolder );
			/**
			 * Issuer 23 Mantis EDV, Incluir en el Tag de Descripcion del archivo 
			 * de Servicios, el Nemonico del Participante, CUI y Descripcion del Titular.
			 * Este cambio para los Servicios 3 y 4 para AFPs.
			 */
			if(BaseCollectionType.RECORD_BOOK_ENTRY_SECURITIES_FIXED_INCOME_AFP.getCode().equals(baseCollection)||
					BaseCollectionType.RECORD_BOOK_ENTRY_SECURITIES_VARIABLE_INCOME_AFP.getCode().equals(baseCollection)){
				createTagString(xmlsw, ReportConstant.TAG_DESCRIPTION,  billing[INDEX_DESCRIPTION].toString().concat(GeneralConstants.TWO_POINTS).concat(billing[INDEX_CLIENT].toString().trim()).concat(GeneralConstants.DASH).concat(billing[INDEX_ENTITY_DESCRIPTION].toString()) );
			}else{
				createTagString(xmlsw, ReportConstant.TAG_DESCRIPTION,  billing[INDEX_DESCRIPTION] );
			}
			
			createTagString(xmlsw, ReportConstant.TAG_IMPUESTO,  billing[INDEX_IMPUESTO] );
			createTagString(xmlsw, ReportConstant.TAG_MONEDA,  billing[INDEX_MONEDA] );
			createTagString(xmlsw, ReportConstant.TAG_CANTIDAD,  billing[INDEX_CANTIDAD] );
			createTagString(xmlsw, ReportConstant.TAG_PRECIO,  billing[INDEX_PRECIO] );
			createTagString(xmlsw, ReportConstant.TAG_FECOBRO,  billing[INDEX_FECOBRO] );
			createTagString(xmlsw, ReportConstant.TAG_TIPOSERVICIO,  billing[INDEX_TIPOSERVICIO] );
			createTagString(xmlsw, ReportConstant.TAG_REF_AUX,  billing[INDEX_REF_AUX] );
			i++;
			xmlsw.writeEndElement();
		}
		
	}

	
	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#generateData(com.pradera.model.report.ReportLogger)
	 */
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	

}
