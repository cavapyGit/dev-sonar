package com.pradera.report.generation.executor.security.to;

import java.io.Serializable;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class SecurityTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/07/2013
 */
public class SecurityTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer system;
	
	private String mnemonic;
	
	private Integer state;
	
	private Integer reportType;

	/**
	 * @return the system
	 */
	public Integer getSystem() {
		return system;
	}

	/**
	 * @param system the system to set
	 */
	public void setSystem(Integer system) {
		this.system = system;
	}

	/**
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}

	/**
	 * @param mnemonic the mnemonic to set
	 */
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	/**
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * @return the reportType
	 */
	public Integer getReportType() {
		return reportType;
	}

	/**
	 * @param reportType the reportType to set
	 */
	public void setReportType(Integer reportType) {
		this.reportType = reportType;
	}
	
	

}
