package com.pradera.report.generation.executor.corporative;

import java.io.ByteArrayOutputStream;
import java.text.DecimalFormat;
import java.util.List;

import javax.inject.Inject;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.corporateevents.to.CorporativeOperationTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.corporative.service.CorporativeReportServiceBean;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;


@ReportProcess(name="CorporativeProcessBlockedBalanceReport")
public class CorporativeProcessBlockedBalanceReport extends GenericReport {

	private static int ACCOUNT_NUMBER=0;
	private static int HOLDER_TYPE=1;
	private static int HOLDER_RNT=2;
	private static int FULL_NAME=3;
	private static int ALTERNATE_CODE=4;
	private static int PARTICIPANT=5;
	private static int ID_PARTICIPANT=6;
	private static int TOTAL_BALANCE=7;
	private static int REPORTING_BALANCE=8;
	private static int MARGIN_BALANCE=9;
	private static int INTEREST_TOTAL_BALANCE  = 10;
	private static int INTEREST_REPORT_BALANCE = 11;
	private static int INTEREST_MARGIN_BALANCE = 12;
	private static int INTEREST_NET_AVAILABLE  = 13;
	private static int AVAILABLE_BALANCE       = 14;
	private static int BRUT_INTEREST           = 15;
	private static int CUSTODY_AMOUNT          = 16;
	private static int RETENTION               = 17;
	private static int pawn_balance            = 18;
	private static int BAN_BALANCE             = 19;
	private static int OPO_BALANCE             = 20;
	private static int RES_BALANCE             = 21;
	private static int OTHER_B_BALANCE         = 22;
	private static int I_PAWN_BALANCE          = 23;
	private static int I_BAN_BALANCE           = 24;
	private static int I_OPO_BALANCE           = 25;
	private static int I_RES_BALANCE           = 26;
	private static int I_OHERS_B_BALANCE       = 27;
	private static int HOLDER_ACCOUNT_Pk       = 28;
	private static int TOTAL_B_BALANCE         = 29;
	private static int I_TOTAL_B_BALANEC       = 30;


	/*Constants for the header corporativeOperation
	 * 
	 */

	private static int ISSUER       = 0;
	private static int ISIN         = 1;
	private static int NOMINAL      = 2;
	private static int TAX          = 3;
	private static int INTEREST     = 4;
	private static int ROUND        = 5;
	private static int ISSUANCE     = 6;
	private static int CUPON_NUMBER = 7;
	private static int REGISTRY_DATE  = 8;
	private static int CUTOFF_DATE    = 9;
	private static int DELIVERY_DATE  = 10;
	private static int ALTERNATIVE_CODE=11;
	private static int CURRENCY=12;
	private static int COUPON_BEGIN_DATE=13;
	private static int COUPING_END_DATE=14;
	
	private Boolean indData=Boolean.FALSE;


	@Inject
	private CorporativeReportServiceBean corporativeReportServiceBean;

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {



		CorporativeOperationTO corporativeOperationTO = this.getReportToFromReportLogger(reportLogger);
		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();

		XMLOutputFactory xof = XMLOutputFactory.newInstance();
		try{


			List<Object[]> list = this.corporativeReportServiceBean.searchBalanceByCorporativeForReportAndMargin(corporativeOperationTO);
			Object[] tempObjects = this.corporativeReportServiceBean.searchCorporativeOperationByFilter(corporativeOperationTO);

			XMLStreamWriter xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(arrayOutputStream));
			//open document file
			xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");
			//root tag of report
			/**
			 * report
			 **/
			xmlsw.writeStartElement(ReportConstant.REPORT);
			/**
			 * Create to header report
			 * start_hour
			 * report_title
			 * mnemonic_report
			 * clasification_report
			 * generation_date
			 * user_name
			 * mnemonic_entity
			 * **/
			createHeaderReport(xmlsw, reportLogger);
			//Create all the headers of the reports for no result statement
			createTagString(xmlsw, "issuer", tempObjects[ISSUER]);
			createTagString(xmlsw, "isin_code", tempObjects[ISIN]);
			createTagString(xmlsw, "issuance", tempObjects[ISSUANCE]);
			String reportTitle = reportLogger.getReport().getTitle();
			String procesName = ImportanceEventType.get(corporativeOperationTO.getCorporativeEventType()).getValue();
			reportTitle = reportTitle.replace("?", procesName.toUpperCase());
			createTagString(xmlsw, ReportConstant.REPORT_TITLE, reportTitle);

			createTagDecimal(xmlsw, "nominal", tempObjects[NOMINAL]);
			DecimalFormat format = new DecimalFormat("###.0000");
			Double interest =Double.parseDouble(tempObjects[INTEREST].toString());
			Double tax =Double.parseDouble(tempObjects[TAX].toString());
			tax = tax*100;

			String strInterest = format.format(interest);
			String strTax = format.format(tax);

			createTagString(xmlsw, "tax", strTax+"%");
			createTagString(xmlsw, "interest", strInterest+"%");


			createTagString(xmlsw, "round", tempObjects[ROUND]);
			createTagString(xmlsw, "coupon", tempObjects[CUPON_NUMBER]);
			createTagString(xmlsw, "interest", tempObjects[INTEREST]);
			createTagString(xmlsw, "cutoff_date", tempObjects[CUTOFF_DATE]);
			createTagString(xmlsw, "delivery_date", tempObjects[DELIVERY_DATE]);
			createTagString(xmlsw, "registry_date", tempObjects[REGISTRY_DATE]);
			String state = CorporateProcessStateType.get(corporativeOperationTO.getState()).getValue();
			createTagString(xmlsw, "corporate_state", state);
			createTagString(xmlsw, "alternate_code", tempObjects[ALTERNATIVE_CODE]);
			createTagString(xmlsw, "currency", tempObjects[CURRENCY]);
			createTagString(xmlsw, "cupon_begin_date", tempObjects[COUPON_BEGIN_DATE].toString());
			createTagString(xmlsw, "cupon_expiration_date", tempObjects[COUPING_END_DATE].toString());
			//		String procesName = ImportanceEventType.get(corporativeOperationTO.getCorporativeEventType()).getValue();
			createTagString(xmlsw, "process_name", procesName);
			createTagString(xmlsw, "ind_date", "0");



			if(Validations.validateListIsNotNullAndNotEmpty(list)){
				/** Creating the body **/
				createBodyReport(xmlsw, list);
			}
			
			if(indData){
				createTagString(xmlsw, "ind_data", "1");

			}
			else{
				createTagString(xmlsw, "ind_data", "0");

			}
			//END REPORT TAG
			xmlsw.writeEndElement();
			//close document
			xmlsw.writeEndDocument();
			xmlsw.flush();
			xmlsw.close();
		}catch(XMLStreamException e){
			e.printStackTrace();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return arrayOutputStream;


	}

	public CorporativeOperationTO getReportToFromReportLogger(ReportLogger reportLogger){
		CorporativeOperationTO corporativeOperationTO = new CorporativeOperationTO();
		for(ReportLoggerDetail detail:reportLogger.getReportLoggerDetails()){
			if(ReportConstant.CORP_PROCESS_ID.equals(detail.getFilterName())){
				corporativeOperationTO.setId(detail.getFilterValue()!=null?Long.parseLong(detail.getFilterValue()):null);
			}
			else if(ReportConstant.CUTOFF_DATE.equals(detail.getFilterName())){
				corporativeOperationTO.setCutOffDate(CommonsUtilities.convertStringtoDate(detail.getFilterValue()));
			}else if(ReportConstant.DELIVERY_DATE.equals(detail.getFilterName())){
				corporativeOperationTO.setDeliveryDate(CommonsUtilities.convertStringtoDate(detail.getFilterValue()));
			}
			else if(ReportConstant.CORPORATE_EVENT_TYPE.equals(detail.getFilterName())){
				corporativeOperationTO.setCorporativeEventType(Integer.parseInt(detail.getFilterValue()));
			}
			else if(ReportConstant.CODE_ISIN.equals(detail.getFilterName())){
				corporativeOperationTO.setSourceIsinCode(detail.getFilterValue());
			}
			else if(ReportConstant.DEST_CODE_ISIN.equals(detail.getFilterName()))
			{
				if(detail.getFilterValue()!=null){
					corporativeOperationTO.setTargetIsinCode(detail.getFilterValue());
				}
			}
			else if(ReportConstant.STATE_PARAM.equals(detail.getFilterName())){
				corporativeOperationTO.setState(Integer.parseInt(detail.getFilterValue()));
			}
		}

		return corporativeOperationTO;
	}
	
	 


	public void createBodyReport(XMLStreamWriter xmlsw,List<Object[]> listOperations){
		try {
			for(Object[] temp: listOperations){
				if(!temp[BAN_BALANCE].toString().equals("0")||
				   !temp[pawn_balance].toString().equals("0")||
				   !temp[OPO_BALANCE].toString().equals("0")||
				   !temp[OTHER_B_BALANCE].toString().equals("0")){

					xmlsw.writeStartElement("operation");	
					indData=Boolean.TRUE;
					createTagString(xmlsw, "cuenta",         temp[ACCOUNT_NUMBER]);
					createTagString(xmlsw, "holder",         temp[HOLDER_RNT].toString()+"-"+temp[FULL_NAME].toString());
					createTagString(xmlsw, "rnt", temp[HOLDER_RNT]);
					createTagString(xmlsw, ReportConstant.PARTICIPANT_PARAM, temp[PARTICIPANT]);
					createTagString(xmlsw, "participant_pk",   temp[ID_PARTICIPANT]);
					createTagString(xmlsw, "pawn_balance",    temp[pawn_balance]);
					createTagString(xmlsw, "ban_balance",   temp[BAN_BALANCE]);
					createTagString(xmlsw, "opo_balance",  temp[OPO_BALANCE]);
					createTagString(xmlsw, "reser_balance", temp[RES_BALANCE]);	
					createTagString(xmlsw, "other_b_balance", temp[OTHER_B_BALANCE]);				
					createTagDecimal(xmlsw, "i_pawn_balance", temp[I_PAWN_BALANCE]);
					createTagDecimal(xmlsw, "i_ban_balance",temp[I_BAN_BALANCE]);
					createTagDecimal(xmlsw, "i_opo_balance", temp[I_OPO_BALANCE]);
					createTagDecimal(xmlsw, "i_res_balance",temp[I_RES_BALANCE]);
					createTagDecimal(xmlsw, "i_other_balance", temp[I_OHERS_B_BALANCE]);
					createTagString(xmlsw, "holder_account_pk", temp[HOLDER_ACCOUNT_Pk]);
					createTagString(xmlsw, "total_b_balance", temp[TOTAL_B_BALANCE]);
					createTagString(xmlsw, "i_total_b_balance",temp[I_TOTAL_B_BALANEC]);

					xmlsw.writeEndElement();
				}

			}

		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}
