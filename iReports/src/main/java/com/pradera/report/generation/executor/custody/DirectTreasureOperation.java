package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;

@ReportProcess(name = "DirectTreasureOperation")
public class DirectTreasureOperation extends GenericReport{
	private static final long serialVersionUID = 1L;
	
	@EJB
	private CustodyReportServiceBean custodyReportrService;
	@EJB
	private ParameterServiceBean parameterService;
	@Inject
	private PraderaLogger log;

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		String operationDescription = null;
		String operationTypeDescription = null;
		String operation = null;
		String operationType = null;

		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("operation")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					operation = listaLogger.get(i).getFilterValue();
					operationDescription = listaLogger.get(i).getFilterDescription();
				}
			}
			if(listaLogger.get(i).getFilterName().equals("operation_type")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					operationType = listaLogger.get(i).getFilterValue();
					operationTypeDescription = listaLogger.get(i).getFilterDescription();
				}
			}	
		}

		String strQuery = custodyReportrService.getQueryDirectTreasureOperation(operation, operationType);

		Map<String,Object> parametersRequired = new HashMap<>();

		parametersRequired.put("operation_description", operationDescription);
		parametersRequired.put("operation_type_description", operationTypeDescription);
		parametersRequired.put("str_query", strQuery);
		
		return parametersRequired;
	}
}
