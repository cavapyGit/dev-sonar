package com.pradera.report.generation.executor.funds;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.funds.service.FundsReportServiceBean;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name="ParameterOfAccountCentralizingReport")
public class ParameterOfAccountCentralizingReport extends GenericReport{
	private static final long serialVersionUID = 1L;
	
	@Inject
	private PraderaLogger log;
	@EJB
	private ParameterServiceBean parameterService;
	@EJB
	private FundsReportServiceBean fundsReportServiceBean;
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {

		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		GenericsFiltersTO gfto = new GenericsFiltersTO();
		
		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("currency")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					gfto.setCurrency(listaLogger.get(i).getFilterValue());
				}
			}
		}
		
		String strQuery = fundsReportServiceBean.getParameterOfAccountCentralizing(gfto); 
		
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> secAccountType = new HashMap<Integer, String>();
		Map<Integer,String> secCurrency = new HashMap<Integer, String>();
		Map<Integer,String> secState = new HashMap<Integer, String>();
		try {
			filter.setMasterTableFk(MasterTableType.MASTER_TABLE_BANK_ACCOUNT_TYPE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secAccountType.put(param.getParameterTablePk(), param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secCurrency.put(param.getParameterTablePk(), param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.MASTER_TABLE_CASH_ACCOUNT_STATE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secState.put(param.getParameterTablePk(), param.getDescription());
			}
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		parametersRequired.put("str_Query", strQuery);
		parametersRequired.put("mAccountType", secAccountType);
		parametersRequired.put("mCurrency", secCurrency);
		parametersRequired.put("mState", secState);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		
		
		return parametersRequired;
	}
	
	

}
