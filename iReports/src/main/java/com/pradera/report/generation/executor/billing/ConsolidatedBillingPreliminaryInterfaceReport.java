package com.pradera.report.generation.executor.billing;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.swing.text.MaskFormatter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.billing.service.BillingServiceReportServiceBean;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ConsolidatedBillingPreliminaryInterfaceReport.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@ReportProcess(name="ConsolidatedBillingPreliminaryInterfaceReport")
public class ConsolidatedBillingPreliminaryInterfaceReport extends GenericReport{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The billin report service. */
	@Inject
	private BillingServiceReportServiceBean billinReportService;
	
	/** The parameter service. */
	@EJB
	private ParameterServiceBean parameterService;
	
	/** Parameter outPut Billing. */
	private static int OPERATION_DATE= 0;
	
	/** The billing period. */
	private static int BILLING_PERIOD=1;
	
	/** The billing date. */
	private static int BILLING_DATE=2;
	
	/** The billing number. */
	private static int BILLING_NUMBER=3;
	
	/** The processed state. */
	private static int PROCESSED_STATE=4;
	
	/** The entity type. */
	private static int ENTITY_TYPE=5;
	
	/** The grouss amount. */
	private static int GROUSS_AMOUNT=6;
	
	/** The collection amount. */
	private static int COLLECTION_AMOUNT=7;
	
	/** The tax applied. */
	private static int TAX_APPLIED=8;
	
	/** The grouss amount detail. */
	private static int GROUSS_AMOUNT_DETAIL=9;
	
	/** The collection amount detail. */
	private static int COLLECTION_AMOUNT_DETAIL=10;
	
	/** The tax applied detail. */
	private static int TAX_APPLIED_DETAIL=11;
	
	/** The code entity collection. */
	private static int CODE_ENTITY_COLLECTION=12;
	
	/** The name entity collection. */
	private static int NAME_ENTITY_COLLECTION=13;
	
	/** The address entity collection. */
	private static int ADDRESS_ENTITY_COLLECTION=14;
	
	/** The doc number entity collection. */
	private static int DOC_NUMBER_ENTITY_COLLECTION=15;
	
	/** The country entity collection. */
	private static int COUNTRY_ENTITY_COLLECTION=16;
	
	/** The province entity collection. */
	private static int PROVINCE_ENTITY_COLLECTION=17;
	
	/** The district entity collection. */
	private static int DISTRICT_ENTITY_COLLECTION=18;
	
	/** The currency type. */
	private static int CURRENCY_TYPE=19;
	
	/** The service code. */
	private static int SERVICE_CODE=20;
	
	/** The service name. */
	private static int SERVICE_NAME=21;
	
	/** The billing pk. */
	private static int BILLING_PK=22;
	
	/** The billing date start. */
	private static int BILLING_DATE_START=23;
	
	/** The billing court date. */
	private static int BILLING_COURT_DATE=24;
	
	/** The billing ncf. */
	private static int BILLING_NCF=25;
	
	/** The document type. */
	private static int DOCUMENT_TYPE=26;
	

	
	/**
	 * Instantiates a new consolidated billing preliminary interface report.
	 */
	public ConsolidatedBillingPreliminaryInterfaceReport(){
		
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#generateData(com.pradera.model.report.ReportLogger)
	 */
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		Long processBillingCalculationPk=null;
//		Object[] dataCevaldom=null;
		boolean existData = false;
		Map<Integer,String> entityCollectionDescription = null;
		Map<Integer,String> currencyTypeDescription = null;
		Map<Integer,String> billingPeriodDescription = null;
		Map<Integer,String> processedStateDescription = null;
		Map<Integer,String> documentTypeDescription = null;
		StringBuilder sbParametersReport = new StringBuilder();
		//Read parameters from DB, all parameters are string type
				for(ReportLoggerDetail loggerDetail : reportLogger.getReportLoggerDetails()) {
					
					if(loggerDetail.getFilterName().equals(ReportConstant.PROCESS_BILLING_CALCULATION_PK)){
						//SET THE FOURTH COMBO [ONE ENTITY OR ALL ENTITY]
						if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
							processBillingCalculationPk=Long.parseLong(loggerDetail.getFilterValue());
							sbParametersReport.append(loggerDetail.getFilterValue()).append(GeneralConstants.STR_COMMA);
						}
					} 
					
				}
		//checking parameter
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
		List<Object[]> billing=this.billinReportService.getConsolidatedBillingPreliminaryReport(processBillingCalculationPk);
		
		
		
		/**
		 * Dos partes:
		 * 
		 * El archivo XML
		 * 
		 * El reporte
		 */
		if(!billing.isEmpty()){
//			 dataCevaldom= this.billinReportService.getInformationParticipant(133L);
			 existData = true;
			//load parameters
			 entityCollectionDescription = new HashMap<Integer, String>();
			 billingPeriodDescription = new HashMap<Integer, String>();
			 currencyTypeDescription= new HashMap<Integer, String>();
			 processedStateDescription=new HashMap<Integer, String>();
			 documentTypeDescription=new HashMap<Integer, String>();
			ParameterTableTO  filter = new ParameterTableTO();
			
			filter.setMasterTableFk(MasterTableType.MASTER_TABLE_ENTITY_COLLECTION.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				entityCollectionDescription.put(param.getParameterTablePk(), param.getParameterName());
			}
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				currencyTypeDescription.put(param.getParameterTablePk(), param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.MASTER_TABLE_COLLECTION_PERIOD.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				billingPeriodDescription.put(param.getParameterTablePk(), param.getParameterName());
			}
			filter.setMasterTableFk(MasterTableType.BILLING_PROCESSED_STATUS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				processedStateDescription.put(param.getParameterTablePk(), param.getParameterName());
			}
			filter.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				documentTypeDescription.put(param.getParameterTablePk(), param.getParameterName());
			}
						
		}
			XMLOutputFactory xof = XMLOutputFactory.newInstance();
			XMLStreamWriter xmlsw;
			
			xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(baos));
			
			//open document file
			xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");
			//root tag of report
			/**
			 * report
			 * */
			xmlsw.writeStartElement(ReportConstant.TAG_VENTAS);
			
			xmlsw.writeStartElement(ReportConstant.TAG_CXC);
			if(existData) {
				//method specific to each implementation of reports
				createBodyReport(xmlsw, entityCollectionDescription,currencyTypeDescription,billingPeriodDescription,processedStateDescription,documentTypeDescription, billing);
			} else {
				//when not exist data we need to create empty structure xml
				
				xmlsw.writeStartElement(ReportConstant.TAG_CXC);
					createTagString(xmlsw, ReportConstant.TAG_NUMCXC,  GeneralConstants.EMPTY_STRING );
					createTagString(xmlsw, ReportConstant.TAG_CLIENT,  GeneralConstants.EMPTY_STRING );
					createTagString(xmlsw, ReportConstant.TAG_DESCRIPTION,  GeneralConstants.EMPTY_STRING );
					createTagString(xmlsw, ReportConstant.TAG_IMPUESTO,  GeneralConstants.EMPTY_STRING );
					createTagString(xmlsw, ReportConstant.TAG_MONEDA,  GeneralConstants.EMPTY_STRING );
					createTagString(xmlsw, ReportConstant.TAG_CANTIDAD,  GeneralConstants.EMPTY_STRING );
					createTagString(xmlsw, ReportConstant.TAG_PRECIO,  GeneralConstants.EMPTY_STRING );
					createTagString(xmlsw, ReportConstant.TAG_FECOBRO,  GeneralConstants.EMPTY_STRING );
					createTagString(xmlsw, ReportConstant.TAG_TIPOSERVICIO,  GeneralConstants.EMPTY_STRING );
					createTagString(xmlsw, ReportConstant.TAG_REF_AUX, GeneralConstants.EMPTY_STRING  );
					
				xmlsw.writeEndElement();
			}
			
			
			//Report Id
			createTagString(xmlsw,ReportConstant.REPORT_ID,reportLogger.getReport().getIdReportPk());
			
		
		//time finish and parameters
				String hourEnd = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),
						ReportConstant.DATE_FORMAT_TIME_SECOND);
				createTagString(xmlsw, ReportConstant.END_HOUR, hourEnd);
				//parameters
				String parametersFilter = Validations.validateIsNotNullAndNotEmpty(sbParametersReport) ? sbParametersReport.substring(0, sbParametersReport.toString().length() -2) : GeneralConstants.EMPTY_STRING;
				createTagString(xmlsw, ReportConstant.PARAMETERS_TAG_REPORT, parametersFilter);
				//close report tag
				xmlsw.writeEndElement();
				//close document
				xmlsw.writeEndDocument();
				xmlsw.flush();
		        xmlsw.close();
		
		
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		return baos;
	}
	
	
	/**
	 * Creates the body report.
	 * good practice factoring
	 *
	 * @param xmlsw the xmlsw
	 * @param entityCollectionDescription the entity Collection Description
	 * @param currencyTypeDescription the currency type description
	 * @param billingPeriodDescription the billing period description
	 * @param processedStateDescription the processed state description
	 * @param documentTypeDescription the document type description
	 * @param billings the billings
	 * @throws XMLStreamException the xML stream exception
	 */
	public void createBodyReport(XMLStreamWriter xmlsw,Map<Integer,String> entityCollectionDescription	,
			Map<Integer,String>  currencyTypeDescription,Map<Integer,String> billingPeriodDescription,
			Map<Integer,String> processedStateDescription,Map<Integer,String> documentTypeDescription,
			List<Object[]> billings)  throws XMLStreamException{
			
		for(Object[] billing : billings) {
						
			xmlsw.writeStartElement(ReportConstant.TAG_CXC);
			//detail report
			createTagString(xmlsw, ReportConstant.TAG_NUMCXC,  billing[BILLING_NUMBER] );//3
			createTagString(xmlsw, ReportConstant.TAG_CLIENT,  billing[CODE_ENTITY_COLLECTION] );//12
			createTagString(xmlsw, ReportConstant.TAG_DESCRIPTION, billing[BILLING_NUMBER] );//21
			createTagString(xmlsw, ReportConstant.TAG_IMPUESTO, billing[BILLING_NUMBER] );//n/a
			createTagString(xmlsw, ReportConstant.TAG_MONEDA, currencyTypeDescription.get(Integer.valueOf(billing[CURRENCY_TYPE] .toString())) );//19
			createTagString(xmlsw, ReportConstant.TAG_CANTIDAD,  billing[BILLING_NUMBER] );
			createTagString(xmlsw, ReportConstant.TAG_PRECIO,  billing[GROUSS_AMOUNT] );//6
			createTagString(xmlsw, ReportConstant.TAG_FECOBRO,  billing[BILLING_DATE] );//2
			createTagString(xmlsw, ReportConstant.TAG_TIPOSERVICIO, GeneralConstants.EMPTY_STRING  );
			createTagString(xmlsw, ReportConstant.TAG_REF_AUX, GeneralConstants.EMPTY_STRING  );
			

			xmlsw.writeEndElement();
		}
		
	}
	
	/**
	 * Mask formatter number.
	 *
	 * @param numero the numero
	 * @param mask the mask
	 * @return the string
	 * @throws ParseException the parse exception
	 */
	public static String maskFormatterNumber(String numero,String mask) throws ParseException{
   	 String cadena="";
			MaskFormatter formatter = new MaskFormatter(mask);
			formatter.setValueContainsLiteralCharacters(false);
			cadena=formatter.valueToString(numero);
	return cadena;
	}

}
