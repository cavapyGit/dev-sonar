package com.pradera.report.generation.executor.accounting.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.pradera.model.generalparameter.type.CurrencyType;
 


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class OperationBalanceTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class OperationBalanceTo implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3632201120647874452L;
	
	
	
	/** The date operation. */
	private Date 		dateOperation;
	
	/** The balance. */
	private BigDecimal 	balance;
	
	/** The currency. */
	private Integer 	currency;
	
	/** The description currency. */
	private String 		descriptionCurrency;
	
	/** The prefix account. */
	private String 		prefixAccount; 

	/** The balance bob. */
	private BigDecimal 	balanceBOB;
	
	/** The balance usd. */
	private BigDecimal 	balanceUSD;
	
	/** The balance mvl. */
	private BigDecimal 	balanceMVL;
	
	/** The balance ufv. */
	private BigDecimal 	balanceUFV;
	
	/** The balance ecu. */
	private BigDecimal 	balanceECU;

	/** Sum To All Balances To Daily Exchange To Bob. */
	private BigDecimal 	amountToBOB;
	
	/**
	 * Instantiates a new operation balance to.
	 */
	public OperationBalanceTo(){
		
	}
	
	/**
	 * Gets the date operation.
	 *
	 * @return the dateOperation
	 */
	public Date getDateOperation() {
		return dateOperation;
	}
	
	/**
	 * Sets the date operation.
	 *
	 * @param dateOperation the dateOperation to set
	 */
	public void setDateOperation(Date dateOperation) {
		this.dateOperation = dateOperation;
	}
	
	/**
	 * Gets the balance.
	 *
	 * @return the balance
	 */
	public BigDecimal getBalance() {
		return balance;
	}
	
	/**
	 * Sets the balance.
	 *
	 * @param balance the balance to set
	 */
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	
	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}
	
	/**
	 * Sets the currency.
	 *
	 * @param currency the currency to set
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	
	/**
	 * Gets the description currency.
	 *
	 * @return the descriptionCurrency
	 */
	public String getDescriptionCurrency() {
		return descriptionCurrency;
	}
	
	/**
	 * Sets the description currency.
	 *
	 * @param descriptionCurrency the descriptionCurrency to set
	 */
	public void setDescriptionCurrency(String descriptionCurrency) {
		this.descriptionCurrency = descriptionCurrency;
	}
	
	/**
	 * Gets the balance bob.
	 *
	 * @return the balanceBOB
	 */
	public BigDecimal getBalanceBOB() {
		return balanceBOB;
	}
	
	/**
	 * Sets the balance bob.
	 *
	 * @param balanceBOB the balanceBOB to set
	 */
	public void setBalanceBOB(BigDecimal balanceBOB) {
		this.balanceBOB = balanceBOB;
	}
	
	/**
	 * Gets the balance usd.
	 *
	 * @return the balanceUSD
	 */
	public BigDecimal getBalanceUSD() {
		return balanceUSD;
	}
	
	/**
	 * Sets the balance usd.
	 *
	 * @param balanceUSD the balanceUSD to set
	 */
	public void setBalanceUSD(BigDecimal balanceUSD) {
		this.balanceUSD = balanceUSD;
	}
	
	/**
	 * Gets the balance mvl.
	 *
	 * @return the balanceMVL
	 */
	public BigDecimal getBalanceMVL() {
		return balanceMVL;
	}
	
	/**
	 * Sets the balance mvl.
	 *
	 * @param balanceMVL the balanceMVL to set
	 */
	public void setBalanceMVL(BigDecimal balanceMVL) {
		this.balanceMVL = balanceMVL;
	}
	
	/**
	 * Gets the balance ufv.
	 *
	 * @return the balanceUFV
	 */
	public BigDecimal getBalanceUFV() {
		return balanceUFV;
	}
	
	/**
	 * Sets the balance ufv.
	 *
	 * @param balanceUFV the balanceUFV to set
	 */
	public void setBalanceUFV(BigDecimal balanceUFV) {
		this.balanceUFV = balanceUFV;
	}
	
	/**
	 * Gets the balance ecu.
	 *
	 * @return the balanceECU
	 */
	public BigDecimal getBalanceECU() {
		return balanceECU;
	}
	
	/**
	 * Sets the balance ecu.
	 *
	 * @param balanceECU the balanceECU to set
	 */
	public void setBalanceECU(BigDecimal balanceECU) {
		this.balanceECU = balanceECU;
	}
	
	/**
	 * Gets the prefix account.
	 *
	 * @return the prefixAccount
	 */
	public String getPrefixAccount() {
		return prefixAccount;
	}
	
	/**
	 * Sets the prefix account.
	 *
	 * @param prefixAccount the prefixAccount to set
	 */
	public void setPrefixAccount(String prefixAccount) {
		this.prefixAccount = prefixAccount;
	}
	
	
	
	/**
	 * Gets the amount to bob.
	 *
	 * @return the amountToBOB
	 */
	public BigDecimal getAmountToBOB() {
		return amountToBOB;
	}
	
	/**
	 * Sets the amount to bob.
	 *
	 * @param amountToBOB the amountToBOB to set
	 */
	public void setAmountToBOB(BigDecimal amountToBOB) {
		this.amountToBOB = amountToBOB;
	}
	
	/**
	 * Fill balance for currency.
	 *
	 * @param currency the currency
	 * @param amountBalance the amount balance
	 */
	public void fillBalanceForCurrency(Integer currency,BigDecimal amountBalance){
		
		if(CurrencyType.PYG.getCode().equals(currency)){
			balanceBOB=amountBalance;
		}else if(CurrencyType.USD.getCode().equals(currency)){
			balanceUSD=amountBalance;
		}else if(CurrencyType.UFV.getCode().equals(currency)){
			balanceUFV=amountBalance;
		}else if(CurrencyType.EU.getCode().equals(currency)){
			balanceECU=amountBalance;
		}else if(CurrencyType.DMV.getCode().equals(currency)){
			balanceMVL=amountBalance;
		}

	}



}
