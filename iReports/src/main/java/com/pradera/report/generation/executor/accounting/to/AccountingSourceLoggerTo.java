package com.pradera.report.generation.executor.accounting.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingSourceLoggerTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class AccountingSourceLoggerTo implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id accounting source logger pk. */
	private Long 							idAccountingSourceLoggerPk;
	
	/** The result partial. */
	private BigDecimal 						resultPartial;
	
	/** The formulate input. */
	private String 							formulateInput;
	
	/** The id accounting receipt detail fk. */
	private Long 							idAccountingReceiptDetailFk;
	
	/** The list accounting source detail. */
	private List<AccountingSourceDetailTo> 	listAccountingSourceDetail;
	
	/**
	 * Gets the id accounting source logger pk.
	 *
	 * @return the idAccountingSourceLoggerPk
	 */
	public Long getIdAccountingSourceLoggerPk() {
		return idAccountingSourceLoggerPk;
	}
	
	/**
	 * Sets the id accounting source logger pk.
	 *
	 * @param idAccountingSourceLoggerPk the idAccountingSourceLoggerPk to set
	 */
	public void setIdAccountingSourceLoggerPk(Long idAccountingSourceLoggerPk) {
		this.idAccountingSourceLoggerPk = idAccountingSourceLoggerPk;
	}
	
	/**
	 * Gets the result partial.
	 *
	 * @return the resultPartial
	 */
	public BigDecimal getResultPartial() {
		return resultPartial;
	}
	
	/**
	 * Sets the result partial.
	 *
	 * @param resultPartial the resultPartial to set
	 */
	public void setResultPartial(BigDecimal resultPartial) {
		this.resultPartial = resultPartial;
	}
	
	/**
	 * Gets the formulate input.
	 *
	 * @return the formulateInput
	 */
	public String getFormulateInput() {
		return formulateInput;
	}
	
	/**
	 * Sets the formulate input.
	 *
	 * @param formulateInput the formulateInput to set
	 */
	public void setFormulateInput(String formulateInput) {
		this.formulateInput = formulateInput;
	}
	
	/**
	 * Gets the id accounting receipt detail fk.
	 *
	 * @return the idAccountingReceiptDetailFk
	 */
	public Long getIdAccountingReceiptDetailFk() {
		return idAccountingReceiptDetailFk;
	}
	
	/**
	 * Sets the id accounting receipt detail fk.
	 *
	 * @param idAccountingReceiptDetailFk the idAccountingReceiptDetailFk to set
	 */
	public void setIdAccountingReceiptDetailFk(Long idAccountingReceiptDetailFk) {
		this.idAccountingReceiptDetailFk = idAccountingReceiptDetailFk;
	}
	
	/**
	 * Gets the list accounting source detail.
	 *
	 * @return the listAccountingSourceDetail
	 */
	public List<AccountingSourceDetailTo> getListAccountingSourceDetail() {
		return listAccountingSourceDetail;
	}
	
	/**
	 * Sets the list accounting source detail.
	 *
	 * @param listAccountingSourceDetail the listAccountingSourceDetail to set
	 */
	public void setListAccountingSourceDetail(
			List<AccountingSourceDetailTo> listAccountingSourceDetail) {
		this.listAccountingSourceDetail = listAccountingSourceDetail;
	}

	
}
