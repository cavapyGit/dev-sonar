package com.pradera.report.generation.executor.account.to;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class NoteToExternalTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , Nov 5, 2013
 */
public class NoteToExternalTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id reference pk. */
	private Integer	idReferencePk;
	
	/** The date Header of Document. */
	private String	dateHeader;
	
	/** The quote of Document. */
	private String	quote;
	
	/** The recipient archetype. */
	private String archetype;
	
	/** The recipient of Document. */
	private String 	recipient;
	
	/** The ocupation of Recipient. */
	private String occupation;
	
	/** The department of Recipient. */
	private String department;
	
	/** The atn of Document. */
	private String atn;
	
	/** The description of Reference. */
	private String 	desReference;
	
	/** The body of Document. */
	private String bodyDoc;
	
	/** The attached sheets. */
	private String attachedSheets;
	
	private String blockType;
	
	public String getBlockType() {
		return blockType;
	}

	public void setBlockType(String blockType) {
		this.blockType = blockType;
	}

	//GETTERS AND SETTERS
	/**
	 * Gets the id reference pk.
	 *
	 * @return the id reference pk
	 */
	public Integer getIdReferencePk() {
		return idReferencePk;
	}
	
	/**
	 * Sets the id reference pk.
	 *
	 * @param idReferencePk the new id reference pk
	 */
	public void setIdReferencePk(Integer idReferencePk) {
		this.idReferencePk = idReferencePk;
	}
	
	/**
	 * Gets the date Header of Reference.
	 *
	 * @return the date Header of Reference
	 */
	public String getDateHeader() {
		return dateHeader;
	}
	
	/**
	 * Sets the date Header of Reference.
	 *
	 * @param dateHeader the new date Header of Reference
	 */
	public void setDateHeader(String dateHeader) {
		this.dateHeader = dateHeader;
	}
	
	/**
	 * Gets the quote of Reference.
	 *
	 * @return the quote of Reference
	 */
	public String getQuote() {
		return quote;
	}
	
	/**
	 * Sets the quote of Reference.
	 *
	 * @param quote the new quote of Reference
	 */
	public void setQuote(String quote) {
		this.quote = quote;
	}

	/**
	 * @return the archetype
	 */
	public String getArchetype() {
		return archetype;
	}

	/**
	 * @param archetype the archetype to set
	 */
	public void setArchetype(String archetype) {
		this.archetype = archetype;
	}

	/**
	 * @return the recipient
	 */
	public String getRecipient() {
		return recipient;
	}

	/**
	 * @param recipient the recipient to set
	 */
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	/**
	 * @return the ocupation
	 */
	public String getOccupation() {
		return occupation;
	}

	/**
	 * @param ocupation the ocupation to set
	 */
	public void setOccupation(String ocupation) {
		this.occupation = ocupation;
	}

	/**
	 * @return the department
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * @param department the department to set
	 */
	public void setDepartment(String department) {
		this.department = department;
	}

	/**
	 * @return the atn
	 */
	public String getAtn() {
		return atn;
	}

	/**
	 * @param atn the atn to set
	 */
	public void setAtn(String atn) {
		this.atn = atn;
	}

	/**
	 * @return the desReference
	 */
	public String getDesReference() {
		return desReference;
	}

	/**
	 * @param desReference the desReference to set
	 */
	public void setDesReference(String desReference) {
		this.desReference = desReference;
	}

	/**
	 * @return the bodyDoc
	 */
	public String getBodyDoc() {
		return bodyDoc;
	}

	/**
	 * @param bodyDoc the bodyDoc to set
	 */
	public void setBodyDoc(String bodyDoc) {
		this.bodyDoc = bodyDoc;
	}

	/**
	 * @return the attached sheets
	 */
	public String getAttachedSheets() {
		return attachedSheets;
	}

	/**
	 * @param attachedSheets the attached sheets to set
	 */
	public void setAttachedSheets(String attachedSheets) {
		this.attachedSheets = attachedSheets;
	}
}
