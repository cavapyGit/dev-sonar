package com.pradera.report.generation.executor;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.model.report.type.ReportLoggerStateType;
import com.pradera.model.report.type.ReportType;
import com.pradera.report.ReportContext;
import com.pradera.report.generation.api.ReportGenerationFramework;
import com.pradera.report.generation.executor.corporative.to.CorporativeProcesInterestTO;
import com.pradera.report.generation.jasper.ReportGeneratorJasper;
import com.pradera.report.generation.pentaho.ReportImplementationLiteral;
import com.pradera.report.generation.service.ReportUtilServiceBean;



/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class GenericReport.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/07/2013
 */
public abstract class GenericReport implements Serializable, Runnable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The report util service. */
	@EJB
	ReportUtilServiceBean reportUtilService;
	
	/** The log. */
	@Inject transient PraderaLogger log;

	/** The template report url. */
	private String templateReportUrl;
	
	/** The xml data source. */
	private String xmlDataSource;
	
	/** The priority. */
	private Integer priority;
	
	/** The report query. */
	private String reportQuery;
	
	/** The sub report query. */
	private String subReportQuery;
	
	/** The report logger. */
	private ReportLogger reportLogger;
	
	/** The report type. */
	private Integer reportType;
	
	/** The report generator. */
	@Inject @Any
	Instance<ReportGenerationFramework> reportGenerator;
	

	/**  The ArrayByte. */
	private byte[] arrayByte;
	
	/** The List To Byte */
	private List<byte[]> listByte;
	
	/** The issuer mnemonic. */
	public static Integer ISSUER_MNEMONIC = new Integer(0);
	
	/** The issuer pk. */
	public static Integer ISSUER_PK = new Integer(1);
	
	/** The issuer name. */
	public static Integer ISSUER_NAME = new Integer(2);
	
	/** The issuance pk. */
	public static Integer ISSUANCE_PK = new Integer(3);
	
	/** The security code. */
	public static Integer SECURITY_CODE = new Integer(4);
	
	/** The nominal value. */
	public static Integer NOMINAL_VALUE = new Integer(5);
	
	/** The corporative type. */
	public static Integer CORPORATIVE_TYPE = new Integer(6);
	
	/** The currency. */
	public static Integer CURRENCY = new Integer(7);
	
	/** The cuttof date. */
	public static Integer CUTTOF_DATE = new Integer(8);
	
	/** The registry date. */
	public static Integer REGISTRY_DATE = new Integer(9);
	
	/** The delivery date. */
	public static Integer DELIVERY_DATE = new Integer(10);
	
	/** The round. */
	public static Integer ROUND = new Integer(11);
	
	/** The delivery factor. */
	public static Integer DELIVERY_FACTOR = new Integer(12);
	
	/** The tax factor. */
	public static Integer TAX_FACTOR = new Integer(13);
	
	/** The interest coupon. */
	public static Integer INTEREST_COUPON = new Integer(14);
	
	/** The amortization coupon. */
	public static Integer AMORTIZATION_COUPON = new Integer(15);
	
	/** The corporative state. */
	public static Integer CORPORATIVE_STATE = new Integer(16);
	
	/** The corporative operation. */
	public static Integer CORPORATIVE_OPERATION = new Integer(17);
	
	/** The currency aux. */
	public static Integer CURRENCY_AUX = new Integer(18);
	
	/** The sum tax amount. */
	public static Integer SUM_TAX_AMOUNT = new Integer(19);
	
	/** The event type description. */
	public static Integer EVENT_TYPE_DESCRIPTION =  new Integer(20);
	
	/**
	 * Sets the report context.
	 *
	 * @param reportContext the new report context
	 */
	public void setReportContext(ReportContext reportContext){
		templateReportUrl=reportContext.getTemplateReportUrl();
		xmlDataSource=reportContext.getXmlDataSource();
		priority=reportContext.getPriority();
		reportQuery=reportContext.getReportQuery();
		subReportQuery=reportContext.getSubReportQuery();
		reportType=reportContext.getReportType();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		runOutsideThread();
	}
	
	/**
	 * Run outside thread.
	 */
	public void runOutsideThread(){
		//Install audit fields
				//Set the default language to English to set the correct pattern to numeric fields ("." for decimal separator and "," for Grouping separator)
				Locale.setDefault(Locale.ENGLISH);
				ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(),getLoggerUser());
				reportUtilService.updateStateReportLoggerTx(reportLogger, ReportLoggerStateType.PROCESSING.getCode(), null);
				try {
					if(this.reportType.equals(ReportType.QUERY.getCode())) {
						ReportGenerationFramework generatorQueryReport= getGenerator(reportType);
						generatorQueryReport.setReportLogger(reportLogger);
						generatorQueryReport.setReportQuery(reportQuery);
						generatorQueryReport.setSubReportQuery(subReportQuery);
						generatorQueryReport.setTemplateReportUrl(templateReportUrl);
						//only delegate to generator
						//add parameters in subclasess
						addParametersQueryReport();
						//add jasper paremeters
						generatorQueryReport.setCustomJasperParameters(getCustomJasperParameters());
						//only export to jasper if format is PDF, XLS
						if(checkIndicatorTemplate(reportLogger, ReportFormatType.get(reportLogger.getReportFormat()))){
							/**Modificado por Henry Coarite*/
							/*ReportGeneratorJasper generatorJasperXls = (ReportGeneratorJasper)generatorQueryReport;
							String templateName = null;
							try {
								templateName = generatorJasperXls.getCustomJasperParameters().get(ReportConstant.TEMPLATE_NAME).toString();
							} catch (Exception e) {
								// TODO: handle exception
							}
							if(reportLogger.getReportFormat().equals(ReportFormatType.XLS.getCode())&&Validations.validateIsNotNullAndNotEmpty(templateName)) {
								generatorQueryReport.setTemplateReportUrl(templateName);
								reportLogger.getReport().setTemplateName(templateName);
							}*/
							generatorQueryReport.generateReports(reportLogger);
						}
					} else {
						//Process xml data source file
						saveXmlData();
						//if have at least one indicator will generate file
						if(checkIndicatorTemplate(reportLogger, ReportFormatType.get(reportLogger.getReportFormat()))){
							//instance generator report
							ReportGenerationFramework generatorBeanReport= getGenerator(reportType);
							generatorBeanReport.setReportLogger(reportLogger);
							generatorBeanReport.setTemplateReportUrl(templateReportUrl);
							generatorBeanReport.setXmlDataSource(xmlDataSource);
							generatorBeanReport.generateReports(reportLogger);
						}
						else{
							//instance generator report
							ReportGenerationFramework generatorBeanReport= getGenerator(reportType);
							generatorBeanReport.setReportLogger(reportLogger);
							generatorBeanReport.setTemplateReportUrl(templateReportUrl);
							generatorBeanReport.setXmlDataSource(xmlDataSource);
							generatorBeanReport.generateReportsByListFile(listByte);
						}
					}
					reportUtilService.updateStateReportLoggerTx(reportLogger, ReportLoggerStateType.FINISHED.getCode(), null);
					//if report finish in state regular check notification
					reportUtilService.sendReportNotification(reportLogger, getLoggerUser());
				} catch(Exception ex){
					//error change status
					//log.error(ex.getMessage());
					ex.printStackTrace();
					reportUtilService.updateStateReportLoggerTx(reportLogger, ReportLoggerStateType.FAILED.getCode(), ex.getMessage());
				}
	}
	
	/**
	 * Check indicator template.
	 *
	 * @param reportLogger the report logger
	 * @param reportFormat the report format
	 * @return true, if successful
	 */
	public boolean checkIndicatorTemplate(ReportLogger reportLogger,ReportFormatType reportFormat){
		boolean executeJasper=false;
		switch (reportFormat) {
		case PDF:
			if(reportLogger.getReport().getIndPdfFormat().equals(BooleanType.YES.getCode())){
				executeJasper=true;
			}
			break;

		case XLS:
			if(reportLogger.getReport().getIndXlsFormat().equals(BooleanType.YES.getCode())){
				executeJasper=true;
			}
			break;
			
		case EXCEL_DELIMITADO:
			if(reportLogger.getReport().getIndExcelDelimitedFormat().equals(BooleanType.YES.getCode())){
				executeJasper=true;
			}
			break;
			
		case POI:
			if(reportLogger.getReport().getIndPoiFormat().equals(BooleanType.YES.getCode())){
				executeJasper=false;
			}
			break;
		default:
			//nothing for now
			break;
		}
		return executeJasper;
	}
	
	/**
	 * Gets the logger user.
	 *
	 * @return the logger user
	 */
	public LoggerUser getLoggerUser(){
		LoggerUser loggerUser = new LoggerUser();
		loggerUser.setUserName(reportLogger.getLastModifyUser());
		loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
		loggerUser.setIdPrivilegeOfSystem(reportLogger.getLastModifyApp());
		loggerUser.setIpAddress(reportLogger.getLastModifyIp());
		return loggerUser;
	}
	
	
	/**
	 * Adds the parameters query report.
	 * override in subclasess
	 */
	public void addParametersQueryReport(){
		
	}
	
	
	/**
	 * Gets the customize jasper parameters.
	 *
	 * @return the customize jasper parameters
	 */
	public Map<String,Object> getCustomJasperParameters(){
		return null;
	}
	
	/**
	 * Save xml data.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void saveXmlData() throws IOException{
		ByteArrayOutputStream byteArrayFile = this.generateData(reportLogger);
		reportUtilService.saveResultXml(reportLogger, byteArrayFile);
	}
	
	/**
	 * Gets the generator.
	 *
	 * @param reportType the report type
	 * @return the generator
	 */
	private ReportGenerationFramework getGenerator(Integer reportType){
		ReportType report = ReportType.get(reportType);
		return reportGenerator.select(new ReportImplementationLiteral(report)).get();
	}
	
	/**
	 * Creates the header report.
	 *
	 * @param xmlsw the xmlsw
	 * @param reportLogger the report logger
	 * @throws XMLStreamException the XML stream exception
	 */
	public void createHeaderReport(XMLStreamWriter xmlsw,ReportLogger reportLogger) throws XMLStreamException{
		String strStartHour = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),ReportConstant.DATE_FORMAT_TIME_SECOND);
		createTagString(xmlsw, ReportConstant.START_HOUR, strStartHour);
		//title
		createTagString(xmlsw, ReportConstant.REPORT_TITLE, reportLogger.getReport().getTitle());
		//mnemonic report
		createTagString(xmlsw, ReportConstant.MNEMONIC_REPORT, reportLogger.getReport().getMnemonico());
		//classification
		createTagString(xmlsw, ReportConstant.CLASIFICATION_REPORT, reportLogger.getReport().getReportClasification());
		//generation date
		String generationDate = CommonsUtilities.convertDateToString(reportLogger.getRegistryDate(), ReportConstant.DATE_FORMAT_STANDAR);
		createTagString(xmlsw, ReportConstant.GENERATION_DATE, generationDate);
		//user
		createTagString(xmlsw, ReportConstant.USER_NAME, reportLogger.getRegistryUser());
		//mnemonic entity
		createTagString(xmlsw, ReportConstant.MNEMONIC_ENTITY, GeneralConstants.EMPTY_STRING);
		//Logo Path
		String separator = System.getProperty("file.separator");
		String basePath = System.getProperty("java.io.tmpdir");
		createTagString(xmlsw, ReportConstant.LOGO_PATH_LABEL,basePath+separator+ ReportConstant.LOGO_RESOURCE_PATH);
		//report name
		createTagString(xmlsw, ReportConstant.REPORT_NAME, reportLogger.getReport().getName());
		
	}
	
	/**
	 * Creates the sub header corporative report.
	 *
	 * @param xmlsw the xmlsw
	 * @param corporativeInformation the corporative information
	 * @param processStateMap the process state map
	 * @param currencyMap the currency map
	 * @throws XMLStreamException the XML stream exception
	 */
	public void createSubHeaderCorporativeReport(XMLStreamWriter xmlsw,CorporativeProcesInterestTO corporativeProcessInformation, Object[] corporativeInformation,Map<Integer,String> processStateMap,Map<Integer,String> currencyMap) throws XMLStreamException{
		Integer processType = new Integer(corporativeInformation[CORPORATIVE_TYPE].toString());
		if(processType.equals(ImportanceEventType.SECURITIES_EXCLUSION.getCode())
				||processType.equals(ImportanceEventType.CONVERTIBILY_BONE_TO_ACTION.getCode())
				||processType.equals(ImportanceEventType.CHANGE_NOMINAL_VALUE_NO_CAP_VAR.getCode())
				||processType.equals(ImportanceEventType.CHANGE_NOMAL_VALUE_CAP_VAR.getCode())
				||processType.equals(ImportanceEventType.SECURITIES_FUSION.getCode())
				||processType.equals(ImportanceEventType.SECURITIES_UNIFICATION.getCode())
				||processType.equals(ImportanceEventType.SECURITIES_EXCISION.getCode())
				||processType.equals(ImportanceEventType.CAPITAL_REDUCTION.getCode())){
			createTagString(xmlsw, ReportConstant.EVENT_TYPE, corporativeInformation[EVENT_TYPE_DESCRIPTION].toString());
		}		
		createTagString(xmlsw, ReportConstant.PROCESS_STATE, processStateMap.get(new Integer(corporativeInformation[CORPORATIVE_STATE].toString())));
		createTagString(xmlsw, ReportConstant.ISSUER, corporativeInformation[ISSUER_MNEMONIC]+ " - " +corporativeInformation[ISSUER_NAME]);
		createTagString(xmlsw,ReportConstant.ISSUANCE,corporativeInformation[ISSUANCE_PK]);
		createTagString(xmlsw,ReportConstant.SECURITY_KEY,corporativeInformation[SECURITY_CODE]);
		if(processType.equals(ImportanceEventType.SECURITIES_EXCLUSION.getCode())
				||processType.equals(ImportanceEventType.CONVERTIBILY_BONE_TO_ACTION.getCode())
				||processType.equals(ImportanceEventType.CHANGE_NOMINAL_VALUE_NO_CAP_VAR.getCode())
				||processType.equals(ImportanceEventType.CHANGE_NOMAL_VALUE_CAP_VAR.getCode())
				||processType.equals(ImportanceEventType.SECURITIES_FUSION.getCode())
				||processType.equals(ImportanceEventType.SECURITIES_UNIFICATION.getCode())
				||processType.equals(ImportanceEventType.SECURITIES_EXCISION.getCode())
				||processType.equals(ImportanceEventType.CAPITAL_REDUCTION.getCode())){
			createTagString(xmlsw,ReportConstant.SECURITY_KEY_AUX,corporativeProcessInformation.getSecurityCode());
		}		
		if(processType.equals(ImportanceEventType.ACTION_DIVIDENDS.getCode()) || 
				processType.equals(ImportanceEventType.SECURITIES_EXCLUSION.getCode())
				||processType.equals(ImportanceEventType.CONVERTIBILY_BONE_TO_ACTION.getCode())
				||processType.equals(ImportanceEventType.CHANGE_NOMINAL_VALUE_NO_CAP_VAR.getCode())
				||processType.equals(ImportanceEventType.CHANGE_NOMAL_VALUE_CAP_VAR.getCode())
				||processType.equals(ImportanceEventType.SECURITIES_FUSION.getCode())
				||processType.equals(ImportanceEventType.SECURITIES_UNIFICATION.getCode())
				||processType.equals(ImportanceEventType.SECURITIES_EXCISION.getCode())
				||processType.equals(ImportanceEventType.CAPITAL_REDUCTION.getCode())
				||processType.equals(ImportanceEventType.PREFERRED_SUSCRIPTION.getCode())){
			createTagString(xmlsw, ReportConstant.CURRENCY, currencyMap.get(new Integer(corporativeInformation[CURRENCY_AUX].toString())));
		}else{
			createTagString(xmlsw, ReportConstant.CURRENCY, currencyMap.get(new Integer(corporativeInformation[CURRENCY].toString())));
		}		
		createTagString(xmlsw, ReportConstant.CUTTOFDATE, CommonsUtilities.convertDatetoString((Date)corporativeInformation[CUTTOF_DATE]));
		createTagString(xmlsw, ReportConstant.DELIVERYDATE, CommonsUtilities.convertDatetoString((Date)corporativeInformation[DELIVERY_DATE]));
		createTagString(xmlsw, ReportConstant.REGISTRYDATE, CommonsUtilities.convertDatetoString((Date)corporativeInformation[REGISTRY_DATE]));
		createTagDecimal(xmlsw, ReportConstant.NOMINALVALUE, corporativeInformation[NOMINAL_VALUE]);
		createTagString(xmlsw, ReportConstant.ROUND, BooleanType.YES.getCode().equals(new Integer(corporativeInformation[ROUND].toString()))?BooleanType.YES.getValue():BooleanType.NO.getValue());
		BigDecimal factor = new BigDecimal(0);
		if(corporativeInformation[DELIVERY_FACTOR]!=null){
			factor = new BigDecimal(corporativeInformation[DELIVERY_FACTOR].toString());
		}
		createTagString(xmlsw, ReportConstant.DELIVERYFACTOR, formatDecimalEigth(factor)+"%");
		if(new BigDecimal(corporativeInformation[SUM_TAX_AMOUNT].toString()).compareTo(BigDecimal.ZERO)==1){
			createTagString(xmlsw, ReportConstant.TAX, BooleanType.YES.getValue());
		}else{
			createTagString(xmlsw, ReportConstant.TAX, BooleanType.NO.getValue());
		}		
		
		if(processType.equals(ImportanceEventType.INTEREST_PAYMENT.getCode())){
			createTagString(xmlsw, ReportConstant.COUPON, corporativeInformation[INTEREST_COUPON]);
			createTagString(xmlsw, ReportConstant.CORPORATIVE_TYPE, "I");
		} else if(processType.equals(ImportanceEventType.CAPITAL_AMORTIZATION.getCode())){
			createTagString(xmlsw, ReportConstant.COUPON, corporativeInformation[AMORTIZATION_COUPON]);
			createTagString(xmlsw, ReportConstant.CORPORATIVE_TYPE, "A");
		}else if(processType.equals(ImportanceEventType.CASH_DIVIDENDS.getCode())){
			createTagString(xmlsw, ReportConstant.CORPORATIVE_TYPE, "CD");
		}else if(processType.equals(ImportanceEventType.ACTION_DIVIDENDS.getCode())){
			createTagString(xmlsw, ReportConstant.CORPORATIVE_TYPE, "AD");
		}else if(processType.equals(ImportanceEventType.PREFERRED_SUSCRIPTION.getCode())){
			createTagString(xmlsw, ReportConstant.CORPORATIVE_TYPE, "PS");
		}else{
			createTagString(xmlsw, ReportConstant.CORPORATIVE_TYPE, "E");
		}
	}
	
	/**
	 * Gets the corporate parameters.
	 *
	 * @param reportLogger the report logger
	 * @return the corporate parameters
	 */
	public CorporativeProcesInterestTO getCorporateParameters(ReportLogger reportLogger){
		CorporativeProcesInterestTO corporative = new CorporativeProcesInterestTO();
		for (ReportLoggerDetail detail : reportLogger.getReportLoggerDetails()) {
			if(detail.getFilterName().equals(ReportConstant.CORP_PROCESS_ID) ){
				if(StringUtils.isNotBlank(detail.getFilterValue())){
					corporative.setProcessId(new Long(detail.getFilterValue()));
				}
			}else if(detail.getFilterName().equals(ReportConstant.DEST_CODE_ISIN) ){
				if(StringUtils.isNotBlank(detail.getFilterValue())){
					corporative.setSecurityCode(detail.getFilterValue().toString());
				}
			}else if(detail.getFilterName().equals(ReportConstant.CORPORATE_EVENT_TYPE) ){
				if(StringUtils.isNotBlank(detail.getFilterValue())){
					corporative.setEventType(new Integer(detail.getFilterValue()));
				}
			}else if(detail.getFilterName().equals(ReportConstant.STATE_PARAM) ){
				if(StringUtils.isNotBlank(detail.getFilterValue())){
					corporative.setState(new Integer(detail.getFilterValue()));
				}
			}
		}
		return corporative;
	}
	
	/**
	 * Creates the tag string.
	 *
	 * @param xmlsw the xmlsw
	 * @param nameTag the name tag
	 * @param data the data
	 * @throws XMLStreamException the xML stream exception
	 */
	public void createTagString(XMLStreamWriter xmlsw,String nameTag,Object data) throws XMLStreamException{
		xmlsw.writeStartElement(nameTag);
		xmlsw.writeCharacters(formatString(data));
		xmlsw.writeEndElement();
	}
	
	/**
	 * Creates the tag decimal.
	 * with format number 
	 * @param xmlsw the xmlsw
	 * @param nameTag the name tag
	 * @param data the data
	 * @throws XMLStreamException the xML stream exception
	 */
	public void createTagDecimal(XMLStreamWriter xmlsw,String nameTag,Object data) throws XMLStreamException{
		xmlsw.writeStartElement(nameTag);
		xmlsw.writeCharacters(formatDecimal(data));
		xmlsw.writeEndElement();
	}
	
	/**
	 * Creates the tag cdata.
	 * with format String 
	 * @param xmlsw the xmlsw
	 * @param nameTag the name tag
	 * @param data the data
	 * @throws XMLStreamException the xML stream exception
	 */
	public void createTagCDataString(XMLStreamWriter xmlsw,String nameTag,Object data) throws XMLStreamException{
		xmlsw.writeStartElement(nameTag);
		xmlsw.writeCData(formatString(data));
		xmlsw.writeEndElement();
	}
	
	/**
	 * Creates the tag integer.
	 *
	 * @param xmlsw the xmlsw
	 * @param nameTag the name tag
	 * @param data the data
	 * @throws XMLStreamException the XML stream exception
	 */
	public void createTagInteger(XMLStreamWriter xmlsw,String nameTag,Object data) throws XMLStreamException{
		xmlsw.writeStartElement(nameTag);
		xmlsw.writeCharacters(formatInteger(data));
		xmlsw.writeEndElement();
	}
	
	/**
	 * Format string.
	 *
	 * @param data the data
	 * @return the string
	 */
	public String formatString(Object data){
		String value = GeneralConstants.EMPTY_STRING;
		if(data!=null) {
			value = data.toString();
		}
		return value;
	}
	
	/**
	 * Format decimal.
	 *
	 * @param data the data
	 * @return the string
	 */
	public String formatDecimal(Object data){
		String value = GeneralConstants.ZERO_DECIMAL_STRING;
		if(data!=null) {
			DecimalFormat objDf = new DecimalFormat(ReportConstant.QUANTITY_FORMAT_DECIMAL);
			value = objDf.format(data);
		}
		return value;
	}
	
	/**
	 * Format decimal eigth.
	 *
	 * @param data the data
	 * @return the string
	 */
	public String formatDecimalEigth(Object data){
		String value = GeneralConstants.ZERO_DECIMAL_STRING;
		if(data!=null) {
			DecimalFormat objDf = new DecimalFormat(ReportConstant.PERCENT_FORMAT_EIGTH_DECIMAL);
			value = objDf.format(data);
		}
		return value;
	}
	
	/**
	 * Format integer.
	 *
	 * @param data the data
	 * @return the string
	 */
	public String formatInteger(Object data){
		String value = GeneralConstants.ZERO_VALUE_STRING;
		if(data!=null) {
			DecimalFormat objDf = new DecimalFormat(ReportConstant.INTEGER_FORMAT_NUMBER);
			value = objDf.format(data);
		}
		return value;
	}
	
	public Long getIntegerNumber(Object data){
		if(data != null){
			return new Long(data.toString());
		} else {
			return new Long(0);
		}
	}
	
	public BigDecimal getBigDecimalNumber(Object data){
		if(data != null){
			return new BigDecimal(data.toString());
		} else {
			return new BigDecimal(0);
		}
	}
	
	/**
	 * Generate data.
	 *
	 * @param reportLogger the report logger
	 * @return the byte array output stream
	 */
	public abstract ByteArrayOutputStream generateData(ReportLogger reportLogger);

	/**
	 * Gets the priority.
	 *
	 * @return the priority
	 */
	public Integer getPriority() {
		return priority;
	}

	/**
	 * Sets the priority.
	 *
	 * @param priority the new priority
	 */
	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	/**
	 * Gets the template report url.
	 *
	 * @return the template report url
	 */
	public String getTemplateReportUrl() {
		return templateReportUrl;
	}

	/**
	 * Sets the template report url.
	 *
	 * @param templateReportUrl the new template report url
	 */
	public void setTemplateReportUrl(String templateReportUrl) {
		this.templateReportUrl = templateReportUrl;
	}

	/**
	 * Gets the xml data source.
	 *
	 * @return the xml data source
	 */
	public String getXmlDataSource() {
		return xmlDataSource;
	}

	/**
	 * Sets the xml data source.
	 *
	 * @param xmlDataSource the new xml data source
	 */
	public void setXmlDataSource(String xmlDataSource) {
		this.xmlDataSource = xmlDataSource;
	}

	/**
	 * Gets the report query.
	 *
	 * @return the report query
	 */
	public String getReportQuery() {
		return reportQuery;
	}

	/**
	 * Sets the report query.
	 *
	 * @param reportQuery the new report query
	 */
	public void setReportQuery(String reportQuery) {
		this.reportQuery = reportQuery;
	}

	/**
	 * Gets the sub report query.
	 *
	 * @return the subReportQuery
	 */
	public String getSubReportQuery() {
		return subReportQuery;
	}

	/**
	 * Sets the sub report query.
	 *
	 * @param subReportQuery the subReportQuery to set
	 */
	public void setSubReportQuery(String subReportQuery) {
		this.subReportQuery = subReportQuery;
	}

	/**
	 * Gets the report logger.
	 *
	 * @return the report logger
	 */
	public ReportLogger getReportLogger() {
		return reportLogger;
	}

	/**
	 * Sets the report logger.
	 *
	 * @param reportLogger the new report logger
	 */
	public void setReportLogger(ReportLogger reportLogger) {
		this.reportLogger = reportLogger;
	}

	/**
	 * Gets the report type.
	 *
	 * @return the report type
	 */
	public Integer getReportType() {
		return reportType;
	}

	/**
	 * Sets the report type.
	 *
	 * @param reportType the new report type
	 */
	public void setReportType(Integer reportType) {
		this.reportType = reportType;
	}

	/**
	 * Instantiates a new generic report.
	 */
	public GenericReport() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the array byte.
	 *
	 * @return the arrayByte
	 */
	public byte[] getArrayByte() {
		return arrayByte;
	}

	/**
	 * Sets the array byte.
	 *
	 * @param arrayByte the arrayByte to set
	 */
	public void setArrayByte(byte[] arrayByte) {
		this.arrayByte = arrayByte;
	}

	/**
	 * @return the listByte
	 */
	public List<byte[]> getListByte() {
		return listByte;
	}

	/**
	 * @param listByte the listByte to set
	 */
	public void setListByte(List<byte[]> listByte) {
		this.listByte = listByte;
	}
	
	

}
