package com.pradera.report.generation.executor.accounting.to;

import java.io.Serializable;
import java.math.BigDecimal;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingVoucherRegistryTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class AccountingVoucherRegistryTo implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The num register. */
	private BigDecimal 	numRegister;//NUMREG
	
	/** The accounting account. */
	private String 		accountingAccount;//CUENTA
	
	/** The amount. */
	private BigDecimal 	amount;//MONTO
	
	/** The operation. */
	private String 		operation;//OPERACION
	
	/** The gloss. */
	private String 		gloss;//GLOSAPARTICULAR
	
	
	
	
	/**
	 * Instantiates a new accounting voucher registry to.
	 */
	public AccountingVoucherRegistryTo() {
		
	}

	/**
	 * Instantiates a new accounting voucher registry to.
	 *
	 * @param numRegister the num register
	 * @param accountingAccount the accounting account
	 * @param amount the amount
	 * @param operation the operation
	 * @param gloss the gloss
	 */
	public AccountingVoucherRegistryTo(BigDecimal numRegister,
			String accountingAccount, BigDecimal amount, String operation,
			String gloss) {
		this.numRegister = numRegister;
		this.accountingAccount = accountingAccount;
		this.amount = amount;
		this.operation = operation;
		this.gloss = gloss;
	}
	
	/**
	 * Gets the num register.
	 *
	 * @return the numRegister
	 */
	public BigDecimal getNumRegister() {
		return numRegister;
	}
	
	/**
	 * Sets the num register.
	 *
	 * @param numRegister the numRegister to set
	 */
	public void setNumRegister(BigDecimal numRegister) {
		this.numRegister = numRegister;
	}
	
	/**
	 * Gets the accounting account.
	 *
	 * @return the accountingAccount
	 */
	public String getAccountingAccount() {
		return accountingAccount;
	}
	
	/**
	 * Sets the accounting account.
	 *
	 * @param accountingAccount the accountingAccount to set
	 */
	public void setAccountingAccount(String accountingAccount) {
		this.accountingAccount = accountingAccount;
	}
	
	/**
	 * Gets the amount.
	 *
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	
	/**
	 * Sets the amount.
	 *
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	/**
	 * Gets the operation.
	 *
	 * @return the operation
	 */
	public String getOperation() {
		return operation;
	}
	
	/**
	 * Sets the operation.
	 *
	 * @param operation the operation to set
	 */
	public void setOperation(String operation) {
		this.operation = operation;
	}
	
	/**
	 * Gets the gloss.
	 *
	 * @return the gloss
	 */
	public String getGloss() {
		return gloss;
	}
	
	/**
	 * Sets the gloss.
	 *
	 * @param gloss the gloss to set
	 */
	public void setGloss(String gloss) {
		this.gloss = gloss;
	}

	
}
