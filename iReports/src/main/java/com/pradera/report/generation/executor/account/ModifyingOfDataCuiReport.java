package com.pradera.report.generation.executor.account;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.RegistryType;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.GeographicLocationServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounts.HolderHistory;
import com.pradera.model.accounts.HolderReqFileHistory;
import com.pradera.model.accounts.LegalRepresentativeHistory;
import com.pradera.model.accounts.RepresentativeFileHistory;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.GeographicLocationType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.account.service.AccountReportServiceBean;
import com.pradera.report.generation.executor.securities.service.PaymentChronogramBySecurity;

@ReportProcess(name = "ModifyingOfDataCuiReport")
public class ModifyingOfDataCuiReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	private GeographicLocationServiceBean geographicLocationServiceBean;

	@EJB
	private AccountReportServiceBean accountReportServiceBean;
	
	@Inject
	private PraderaLogger log; 

	public ModifyingOfDataCuiReport() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {

		Long idHolderRequestPk = null;
		
		for(ReportLoggerDetail param: getReportLogger().getReportLoggerDetails()){
			if(param.getFilterName().equals("idHolderRequestPk")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					idHolderRequestPk = Long.parseLong(param.getFilterValue().toString());
				}
			}
		} 
		
		/** HashMap  * BEGIN **/
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();

		Map<Integer,String> secState		 			= new HashMap<Integer, String>();
		Map<Integer,String> secPersonType	 			= new HashMap<Integer, String>();
		Map<Integer,String> secDocumentType	 			= new HashMap<Integer, String>();
		Map<Integer,String> secBoolean		 			= new HashMap<Integer, String>();
		Map<Integer,String> secPais	 					= new HashMap<Integer, String>();
		Map<Integer,String> secDepartamento	 			= new HashMap<Integer, String>();
		Map<Integer,String> secProvincia	 			= new HashMap<Integer, String>();
		Map<Integer,String> secMunicipio	 			= new HashMap<Integer, String>();
		Map<Integer,String> secSecEcominico	 			= new HashMap<Integer, String>();
		Map<Integer,String> secActEconimica	 			= new HashMap<Integer, String>();
		GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
		
		try {

			filter.setMasterTableFk(MasterTableType.STATE_CREATION_HOLDER_REQUEST.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secState.put(param.getParameterTablePk(), param.getDescription());
			}	

			filter.setMasterTableFk(MasterTableType.PERSON_TYPE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secPersonType.put(param.getParameterTablePk(), param.getParameterName());
			}	
			
			filter.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secDocumentType.put(param.getParameterTablePk(), param.getIndicator1());
			}	
			
			geographicLocationTO.setGeographicLocationType(GeographicLocationType.COUNTRY.getCode());
			for(GeographicLocation geographic : geographicLocationServiceBean.getListGeographicLocation(geographicLocationTO)){
				secPais.put(geographic.getIdGeographicLocationPk(),geographic.getName());
			}
			
			geographicLocationTO.setGeographicLocationType(GeographicLocationType.DEPARTMENT.getCode());
			for(GeographicLocation geographic : geographicLocationServiceBean.getListGeographicLocation(geographicLocationTO)){
				secDepartamento.put(geographic.getIdGeographicLocationPk(),geographic.getName());
			}
			
			geographicLocationTO.setGeographicLocationType(GeographicLocationType.PROVINCE.getCode());
			for(GeographicLocation geographic : geographicLocationServiceBean.getListGeographicLocation(geographicLocationTO)){
				secProvincia.put(geographic.getIdGeographicLocationPk(),geographic.getName());
			}
			
			geographicLocationTO.setGeographicLocationType(GeographicLocationType.DISTRICT.getCode());
			for(GeographicLocation geographic : geographicLocationServiceBean.getListGeographicLocation(geographicLocationTO)){
				secMunicipio.put(geographic.getIdGeographicLocationPk(),geographic.getName());
			}
			
			filter.setMasterTableFk(MasterTableType.ECONOMIC_SECTOR.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secSecEcominico.put(param.getParameterTablePk(), param.getDescription());
			}	
			
			filter.setMasterTableFk(MasterTableType.ECONOMIC_ACTIVITY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secActEconimica.put(param.getParameterTablePk(), param.getDescription());
			}	
			
			for(BooleanType param : BooleanType.list) {
				secBoolean.put(param.getCode(), param.getValue());
			}	
		
		/** HashMap  * END **/

		

			String strQuery = accountReportServiceBean.getModificationHolderRequest(idHolderRequestPk);

			List<String> lstData = new ArrayList<String>();
			List<String> lstOldData = new ArrayList<String>();
			List<String> lstNewData	= new ArrayList<String>();
			
			HolderHistory holderHistory = new HolderHistory();
			HolderHistory holderHistoryCopy = new HolderHistory();
			
			List<LegalRepresentativeHistory> legalRepresentativeHistory = new ArrayList<LegalRepresentativeHistory>();
			List<LegalRepresentativeHistory> legalRepresentativeHistoryCopy = new ArrayList<LegalRepresentativeHistory>();
			
			List<HolderReqFileHistory> holderReqFileHistory = new ArrayList<HolderReqFileHistory>();
			List<HolderReqFileHistory> holderReqFileHistoryCopy = new ArrayList<HolderReqFileHistory>();
			
			List<HolderHistory> modificationHistoryList = accountReportServiceBean.getHolderHistoryByHolderRequest(idHolderRequestPk);

			if(modificationHistoryList.get(0).getRegistryType().equals(RegistryType.HISTORICAL_COPY.getCode())){
				holderHistoryCopy = (HolderHistory) modificationHistoryList.get(0);
			}else if(modificationHistoryList.get(0).getRegistryType().equals(RegistryType.ORIGIN_OF_THE_REQUEST.getCode())){
				holderHistory = (HolderHistory) modificationHistoryList.get(0);
			}
			
			if(modificationHistoryList.get(1).getRegistryType().equals(RegistryType.HISTORICAL_COPY.getCode())){
				holderHistoryCopy = (HolderHistory) modificationHistoryList.get(1);
			}else if(modificationHistoryList.get(1).getRegistryType().equals(RegistryType.ORIGIN_OF_THE_REQUEST.getCode())){
			    holderHistory = (HolderHistory) modificationHistoryList.get(1);
			}

			lstData.add("Datos de Registro");
			lstOldData.add("");
			lstNewData.add("");
			
			/** Datos de Registro  * BEGIN **/
//			if(Validations.validateIsNotNull(holderHistory.getIdHolderHistoryPk())){
//				if(!holderHistory.getIdHolderHistoryPk().equals(holderHistoryCopy.getIdHolderHistoryPk())){
//					lstData.add("RUT (Registro Unico del Titular)	:");
//					lstOldData.add(holderHistoryCopy.getIdHolderHistoryPk().toString());
//					lstNewData.add(holderHistory.getIdHolderHistoryPk().toString());
//				}
//			} else if(Validations.validateIsNotNull(holderHistoryCopy.getIdHolderHistoryPk())){
//				lstData.add("RUT (Registro Unico del Titular)	:");
//				lstOldData.add(holderHistoryCopy.getIdHolderHistoryPk().toString());
//				lstNewData.add(holderHistory.getIdHolderHistoryPk().toString());
//			}
			
			//verificando si existen cambios en el tipo de persona
			if(Validations.validateIsNotNull(holderHistory.getHolderType())){
				if(!holderHistory.getHolderType().equals(holderHistoryCopy.getHolderType())){
					lstData.add("Tipo Persona	:");
					lstOldData.add(secPersonType.get(holderHistoryCopy.getHolderType()));
					lstNewData.add(secPersonType.get(holderHistory.getHolderType()));
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getHolderType())){
				lstData.add("Tipo Persona	:		");
				lstOldData.add(secPersonType.get(holderHistoryCopy.getHolderType()));
				lstNewData.add(secPersonType.get(holderHistory.getHolderType()));
			}
			
			//verificando si existen cambios en los datos de envio ASFI
			//mnemonico fondo
			
			if(Validations.validateIsNotNull(holderHistory.getMnemonic())){
				if(!holderHistory.getMnemonic().equals(holderHistoryCopy.getMnemonic())){
					lstData.add("Mnemonico Fondo	:");
					lstOldData.add(holderHistoryCopy.getMnemonic());
					lstNewData.add(holderHistory.getMnemonic());
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getMnemonic())){
				lstData.add("Mnemonico Fondo	:		");
				lstOldData.add(holderHistoryCopy.getMnemonic());
				lstNewData.add(holderHistory.getMnemonic());
			}
			
			
			//mnemonico asfi
			
			if(Validations.validateIsNotNull(holderHistory.getFundAdministrator())){
				if(!holderHistory.getFundAdministrator().equals(holderHistoryCopy.getFundAdministrator())){
					lstData.add("Mnemonico ASFI	:");
					lstOldData.add(holderHistoryCopy.getFundAdministrator());
					lstNewData.add(holderHistory.getFundAdministrator());
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getFundAdministrator())){
				lstData.add("Mnemonico ASFI	:		");
				lstOldData.add(holderHistoryCopy.getFundAdministrator());
				lstNewData.add(holderHistory.getFundAdministrator());
			}
			
			
			//numero de transferencia
			
			if(Validations.validateIsNotNull(holderHistory.getTransferNumber())){
				if(!holderHistory.getTransferNumber().equals(holderHistoryCopy.getTransferNumber())){
					lstData.add("No de Transferencia	:");
					lstOldData.add(holderHistoryCopy.getTransferNumber());
					lstNewData.add(holderHistory.getTransferNumber());
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getTransferNumber())){
				lstData.add("No de Transferencia	:		");
				lstOldData.add(holderHistoryCopy.getTransferNumber());
				lstNewData.add(holderHistory.getTransferNumber());
			}
			//Tipo de fondo
			
			if(Validations.validateIsNotNull(holderHistory.getFundType())){
				if(!holderHistory.getFundType().equals(holderHistoryCopy.getFundType())){
					lstData.add("Tipo de fondo	:");
					lstOldData.add(holderHistoryCopy.getFundType());
					lstNewData.add(holderHistory.getFundType());
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getFundType())){
				lstData.add("Tipo de fondo	:		");
				lstOldData.add(holderHistoryCopy.getFundType());
				lstNewData.add(holderHistory.getFundType());
			}
			
			// Transferencia Extrabursatil
			if(Validations.validateIsNotNull(holderHistory.getIndCanNegotiate())){
				if(!holderHistory.getIndCanNegotiate().equals(holderHistoryCopy.getIndCanNegotiate())){
					lstData.add("Transferencia Extraburs\u00e1til:		");
					lstOldData.add(secBoolean.get(holderHistoryCopy.getIndCanNegotiate()));
					lstNewData.add(secBoolean.get(holderHistory.getIndCanNegotiate()));
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getIndCanNegotiate())){
				lstData.add("Transferencia Extraburs\u00e1til:		");
				lstOldData.add(secBoolean.get(holderHistoryCopy.getIndCanNegotiate()));
				lstNewData.add(secBoolean.get(holderHistory.getIndCanNegotiate()));
			}
			
			//negociacion SIRTEX
			if(Validations.validateIsNotNull(holderHistory.getIndSirtexNeg())){
				if(!holderHistory.getIndSirtexNeg().equals(holderHistoryCopy.getIndSirtexNeg())){
					lstData.add("Negocia SIRTEX:		");
					lstOldData.add(secBoolean.get(holderHistoryCopy.getIndSirtexNeg()));
					lstNewData.add(secBoolean.get(holderHistory.getIndSirtexNeg()));
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getIndSirtexNeg())){
				lstData.add("Negocia SIRTEX:		");
				lstOldData.add(secBoolean.get(holderHistoryCopy.getIndSirtexNeg()));
				lstNewData.add(secBoolean.get(holderHistory.getIndSirtexNeg()));
			}
			
			/** Datos de Registro  * END **/
			
			/** Datos de Personales  * BEGIN **/
			if(Validations.validateIsNotNull(holderHistory.getDocumentType())){
				if(!holderHistory.getDocumentType().equals(holderHistoryCopy.getDocumentType())){
					lstData.add("Tipo Documento	:");
					lstOldData.add(secDocumentType.get(holderHistoryCopy.getDocumentType()));
					lstNewData.add(secDocumentType.get(holderHistory.getDocumentType()));
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getDocumentType())){
				lstData.add("Tipo Documento	:");
				lstOldData.add(secDocumentType.get(holderHistoryCopy.getDocumentType()));
				lstNewData.add(secDocumentType.get(holderHistory.getDocumentType()));
			}
			if(Validations.validateIsNotNull(holderHistory.getDocumentNumber())){
				if(!holderHistory.getDocumentNumber().equals(holderHistoryCopy.getDocumentNumber())){
					lstData.add("N\u00famero Documento	:		");
					lstOldData.add(holderHistoryCopy.getDocumentNumber());
					lstNewData.add(holderHistory.getDocumentNumber());
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getDocumentNumber())){
				lstData.add("N\u00famero Documento	:		");
				lstOldData.add(holderHistoryCopy.getDocumentNumber());
				lstNewData.add(holderHistory.getDocumentNumber());
			}
			if(Validations.validateIsNotNull(holderHistory.getFullName())){
				if(!holderHistory.getFullName().equals(holderHistoryCopy.getFullName())){
					lstData.add("Nombre Completo	:		");
					lstOldData.add(holderHistoryCopy.getFullName());
					lstNewData.add(holderHistory.getFullName());
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getFullName())){
				lstData.add("Nombre Completo	:		");
				lstOldData.add(holderHistoryCopy.getFullName());
				lstNewData.add(holderHistory.getFullName());
			}
			if(Validations.validateIsNotNull(holderHistory.getFirstLastName())){
				if(!holderHistory.getFirstLastName().equals(holderHistoryCopy.getFirstLastName())){
					lstData.add("Primer Apellido	:		");
					lstOldData.add(holderHistoryCopy.getFirstLastName());
					lstNewData.add(holderHistory.getFirstLastName());
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getFirstLastName())){
				lstData.add("Primer Apellido	:		");
				lstOldData.add(holderHistoryCopy.getFirstLastName());
				lstNewData.add(holderHistory.getFirstLastName());
			}
			if(Validations.validateIsNotNull(holderHistory.getSecondLastName())){
				if(!holderHistory.getSecondLastName().equals(holderHistoryCopy.getSecondLastName())){
					lstData.add("Segundo Apellido	:		");
					lstOldData.add(holderHistoryCopy.getSecondLastName());
					lstNewData.add(holderHistory.getSecondLastName());
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getSecondLastName())){
				lstData.add("Segundo Apellido	:		");
				lstOldData.add(holderHistoryCopy.getSecondLastName());
				lstNewData.add(holderHistory.getSecondLastName());
			}
			/** Datos de Personales  * END **/
			
			/** Datos de Naturales  * BEGIN **/	
			if(Validations.validateIsNotNull(holderHistory.getIndDisabled())){
				if(!holderHistory.getIndDisabled().equals(holderHistoryCopy.getIndDisabled())){
					lstData.add("Discapacidad	:		");
					lstOldData.add(secBoolean.get(holderHistoryCopy.getIndDisabled()));
					lstNewData.add(secBoolean.get(holderHistory.getIndDisabled()));
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getIndDisabled())){
				lstData.add("Discapacidad	:		");
				lstOldData.add(secBoolean.get(holderHistoryCopy.getIndDisabled()));
				lstNewData.add(secBoolean.get(holderHistory.getIndDisabled()));
			}
			if(Validations.validateIsNotNull(holderHistory.getNationality())){
				if(!holderHistory.getNationality().equals(holderHistoryCopy.getNationality())){
					lstData.add("Pa\u00eds de Nacionalidad	:		");
					lstOldData.add(secPais.get(holderHistoryCopy.getNationality()));
					lstNewData.add(secPais.get(holderHistory.getNationality()));
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getNationality())){
				lstData.add("Pa\u00eds de Nacionalidad	:		");
				lstOldData.add(secPais.get(holderHistoryCopy.getNationality()));
				lstNewData.add(secPais.get(holderHistory.getNationality()));
			}
			if(Validations.validateIsNotNull(holderHistory.getIndResidence())){
				if(!holderHistory.getIndResidence().equals(holderHistoryCopy.getIndResidence())){
					lstData.add("Residente	:		");
					lstOldData.add(secBoolean.get(holderHistoryCopy.getIndResidence()));
					lstNewData.add(secBoolean.get(holderHistory.getIndResidence()));
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getIndResidence())){
				lstData.add("Residente	:		");
				lstOldData.add(secBoolean.get(holderHistoryCopy.getIndResidence()));
				lstNewData.add(secBoolean.get(holderHistory.getIndResidence()));
			}
//			Pais de residencia
//			if(!holderHistory.getIndResidence().equals(holderHistoryCopy.getIndResidence())){
//				lstData.add("Pais de Residencia	:		");
//				lstOldData.add(holderHistoryCopy.getIndResidence());
//				lstNewData.add(holderHistory.getIndResidence());
//			}
			if(Validations.validateIsNotNull(holderHistory.getSecondNationality())){
				if(!holderHistory.getSecondNationality().equals(holderHistoryCopy.getSecondNationality())){
					lstData.add("Pa\u00eds Segunda Nacionalidad	:		");
					lstOldData.add(secPais.get(holderHistoryCopy.getSecondNationality()));
					lstNewData.add(secPais.get(holderHistory.getSecondNationality()));
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getSecondNationality())){
				lstData.add("Pa\u00eds Segunda Nacionalidad	:		");
				lstOldData.add(secPais.get(holderHistoryCopy.getSecondNationality()));
				lstNewData.add(secPais.get(holderHistory.getSecondNationality()));
			}
			if(Validations.validateIsNotNull(holderHistory.getSecondDocumentType())){
				if(!holderHistory.getSecondDocumentType().equals(holderHistoryCopy.getSecondDocumentType())){
					lstData.add("Tipo Documento Segunda Nacionalidad	:		");
					lstOldData.add(secDocumentType.get(holderHistoryCopy.getSecondDocumentType()));
					lstNewData.add(secDocumentType.get(holderHistory.getSecondDocumentType()));
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getSecondDocumentType())){
				lstData.add("Tipo Documento Segunda Nacionalidad	:		");
				lstOldData.add(secDocumentType.get(holderHistoryCopy.getSecondDocumentType()));
				lstNewData.add(secDocumentType.get(holderHistory.getSecondDocumentType()));
			}
			if(Validations.validateIsNotNull(holderHistory.getSecondDocumentNumber())){
				if(!holderHistory.getSecondDocumentNumber().equals(holderHistoryCopy.getSecondDocumentNumber())){
					lstData.add("N\u00famero Documento Segunda Nacionalidad	:		");
					lstOldData.add(holderHistoryCopy.getSecondDocumentNumber());
					lstNewData.add(holderHistory.getSecondDocumentNumber());
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getSecondDocumentNumber())){
				lstData.add("N\u00famero Documento Segunda Nacionalidad	:		");
				lstOldData.add(holderHistoryCopy.getSecondDocumentNumber());
				lstNewData.add(holderHistory.getSecondDocumentNumber());
			}
			/** Datos de Naturales  * END **/		
			
			/** PEP  * BEGIN **/	
			if(Validations.validateIsNotNull(holderHistory.getIndPEP())){
				if(!holderHistory.getIndPEP().equals(holderHistoryCopy.getIndPEP())){
					lstData.add("PEP	:		");
					lstOldData.add(secBoolean.get(holderHistoryCopy.getIndPEP()));
					lstNewData.add(secBoolean.get(holderHistory.getIndPEP()));
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getIndPEP())){
				lstData.add("PEP	:		");
				lstOldData.add(secBoolean.get(holderHistoryCopy.getIndPEP()));
				lstNewData.add(secBoolean.get(holderHistory.getIndPEP()));
			}
			/** PEP  * END **/	
			
			/** Dirección Legal  * BEGIN **/	
			if(Validations.validateIsNotNull(holderHistory.getLegalResidenceCountry())){
				if(!holderHistory.getLegalResidenceCountry().equals(holderHistoryCopy.getLegalResidenceCountry())){
					lstData.add("Pa\u00eds Legal	:		");
					lstOldData.add(secPais.get(holderHistoryCopy.getLegalResidenceCountry()));
					lstNewData.add(secPais.get(holderHistory.getLegalResidenceCountry()));
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getLegalResidenceCountry())){
				lstData.add("Pa\u00eds Legal	:		");
				lstOldData.add(secPais.get(holderHistoryCopy.getLegalResidenceCountry()));
				lstNewData.add(secPais.get(holderHistory.getLegalResidenceCountry()));
			}
			if(Validations.validateIsNotNull(holderHistory.getLegalDepartment())){
				if(!holderHistory.getLegalDepartment().equals(holderHistoryCopy.getLegalDepartment())){
					lstData.add("Departamento Legal	:		");
					lstOldData.add(secDepartamento.get(holderHistoryCopy.getLegalDepartment()));
					lstNewData.add(secDepartamento.get(holderHistory.getLegalDepartment()));
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getLegalDepartment())){
				lstData.add("Departamento Legal	:		");
				lstOldData.add(secDepartamento.get(holderHistoryCopy.getLegalDepartment()));
				lstNewData.add(secDepartamento.get(holderHistory.getLegalDepartment()));
			}
			if(Validations.validateIsNotNull(holderHistory.getLegalProvince())){
				if(!holderHistory.getLegalProvince().equals(holderHistoryCopy.getLegalProvince())){
					lstData.add("PCiudadLegal	:		");
					lstOldData.add(secProvincia.get(holderHistoryCopy.getLegalProvince()));
					lstNewData.add(secProvincia.get(holderHistory.getLegalProvince()));
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getLegalProvince())){
				lstData.add("PCiudadLegal	:		");
				lstOldData.add(secProvincia.get(holderHistoryCopy.getLegalProvince()));
				lstNewData.add(secProvincia.get(holderHistory.getLegalProvince()));
			}
			if(Validations.validateIsNotNull(holderHistory.getLegalDistrict())){
				if(!holderHistory.getLegalDistrict().equals(holderHistoryCopy.getLegalDistrict())){
					lstData.add("Barrio Legal	:		");
					lstOldData.add(secMunicipio.get(holderHistoryCopy.getLegalDistrict()));
					lstNewData.add(secMunicipio.get(holderHistory.getLegalDistrict()));
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getLegalDistrict())){
				lstData.add("Barrio Legal	:		");
				lstOldData.add(secMunicipio.get(holderHistoryCopy.getLegalDistrict()));
				lstNewData.add(secMunicipio.get(holderHistory.getLegalDistrict()));
			}
			if(Validations.validateIsNotNull(holderHistory.getLegalAddress())){
				if(!holderHistory.getLegalAddress().equals(holderHistoryCopy.getLegalAddress())){
					lstData.add("Direcci\u00f3n Legal	:		");
					lstOldData.add(holderHistoryCopy.getLegalAddress());
					lstNewData.add(holderHistory.getLegalAddress());
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getLegalAddress())){
				lstData.add("Direcci\u00f3n Legal	:		");
				lstOldData.add(holderHistoryCopy.getLegalAddress());
				lstNewData.add(holderHistory.getLegalAddress());
			}
			/** Dirección Legal  * END **/	
			
			/** Dirección Postal  * BEGIN **/
			if(Validations.validateIsNotNull(holderHistory.getPostalResidenceCountry())){	
				if(!holderHistory.getPostalResidenceCountry().equals(holderHistoryCopy.getPostalResidenceCountry())){
					lstData.add("Pa\u00eds Postal	:		");
					lstOldData.add(secPais.get(holderHistoryCopy.getPostalResidenceCountry()));
					lstNewData.add(secPais.get(holderHistory.getPostalResidenceCountry()));
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getPostalResidenceCountry())){
				lstData.add("Pa\u00eds Postal	:		");
				lstOldData.add(secPais.get(holderHistoryCopy.getPostalResidenceCountry()));
				lstNewData.add(secPais.get(holderHistory.getPostalResidenceCountry()));
			}
			if(Validations.validateIsNotNull(holderHistory.getPostalDepartment())){
				if(!holderHistory.getPostalDepartment().equals(holderHistoryCopy.getPostalDepartment())){
					lstData.add("Departamento Postal	:		");
					lstOldData.add(secDepartamento.get(holderHistoryCopy.getPostalDepartment()));
					lstNewData.add(secDepartamento.get(holderHistory.getPostalDepartment()));
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getPostalDepartment())){
				lstData.add("Departamento Postal	:		");
				lstOldData.add(secDepartamento.get(holderHistoryCopy.getPostalDepartment()));
				lstNewData.add(secDepartamento.get(holderHistory.getPostalDepartment()));
			}
			if(Validations.validateIsNotNull(holderHistory.getPostalProvince())){
				if(!holderHistory.getPostalProvince().equals(holderHistoryCopy.getPostalProvince())){
					lstData.add("ProCiudadstal	:		");
					lstOldData.add(secProvincia.get(holderHistoryCopy.getPostalProvince()));
					lstNewData.add(secProvincia.get(holderHistory.getPostalProvince()));
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getPostalProvince())){
				lstData.add("ProCiudadstal	:		");
				lstOldData.add(secProvincia.get(holderHistoryCopy.getPostalProvince()));
				lstNewData.add(secProvincia.get(holderHistory.getPostalProvince()));
			}
			if(Validations.validateIsNotNull(holderHistory.getPostalDistrict())){
				if(!holderHistory.getPostalDistrict().equals(holderHistoryCopy.getPostalDistrict())){
					lstData.add("Barrio Postal	:		");
					lstOldData.add(secMunicipio.get(holderHistoryCopy.getPostalDistrict()));
					lstNewData.add(secMunicipio.get(holderHistory.getPostalDistrict()));
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getPostalDistrict())){
				lstData.add("Barrio Postal	:		");
				lstOldData.add(secMunicipio.get(holderHistoryCopy.getPostalDistrict()));
				lstNewData.add(secMunicipio.get(holderHistory.getPostalDistrict()));
			}
			if(Validations.validateIsNotNull(holderHistory.getPostalAddress())){
				if(!holderHistory.getPostalAddress().equals(holderHistoryCopy.getPostalAddress())){
					lstData.add("Direcci\u00f3n Postal	:		");
					lstOldData.add(holderHistoryCopy.getPostalAddress());
					lstNewData.add(holderHistory.getPostalAddress());
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getPostalAddress())){
				lstData.add("Direcci\u00f3n Postal	:		");
				lstOldData.add(holderHistoryCopy.getPostalAddress());
				lstNewData.add(holderHistory.getPostalAddress());
			}
			/** Dirección Postal  * END **/	
			
			/** Datos de Contacto  * BEGIN **/	
			if(Validations.validateIsNotNull(holderHistory.getHomePhoneNumber())){
				if(!holderHistory.getHomePhoneNumber().equals(holderHistoryCopy.getHomePhoneNumber())){
					lstData.add("Tel\u00e9fono Casa	:		");
					lstOldData.add(holderHistoryCopy.getHomePhoneNumber());
					lstNewData.add(holderHistory.getHomePhoneNumber());
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getHomePhoneNumber())){
				lstData.add("Tel\u00e9fono Casa	:		");
				lstOldData.add(holderHistoryCopy.getHomePhoneNumber());
				lstNewData.add(holderHistory.getHomePhoneNumber());
			}
			if(Validations.validateIsNotNull(holderHistory.getMobileNumber())){
				if(!holderHistory.getMobileNumber().equals(holderHistoryCopy.getMobileNumber())){
					lstData.add("Tel\u00e9fono Celular	:		");
					lstOldData.add(holderHistoryCopy.getMobileNumber());
					lstNewData.add(holderHistory.getMobileNumber());
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getMobileNumber())){
				lstData.add("Tel\u00e9fono Celular	:		");
				lstOldData.add(holderHistoryCopy.getMobileNumber());
				lstNewData.add(holderHistory.getMobileNumber());
			}
			if(Validations.validateIsNotNull(holderHistory.getOfficePhoneNumber())){
				if(!holderHistory.getOfficePhoneNumber().equals(holderHistoryCopy.getOfficePhoneNumber())){
					lstData.add("Tel\u00e9fono Oficina	:		");
					lstOldData.add(holderHistoryCopy.getOfficePhoneNumber());
					lstNewData.add(holderHistory.getOfficePhoneNumber());
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getOfficePhoneNumber())){
				lstData.add("Tel\u00e9fono Oficina	:		");
				lstOldData.add(holderHistoryCopy.getOfficePhoneNumber());
				lstNewData.add(holderHistory.getOfficePhoneNumber());
			}
			if(Validations.validateIsNotNull(holderHistory.getFaxNumber())){
				if(!holderHistory.getFaxNumber().equals(holderHistoryCopy.getFaxNumber())){
					lstData.add("Fax	:		");
					lstOldData.add(holderHistoryCopy.getFaxNumber());
					lstNewData.add(holderHistory.getFaxNumber());
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getFaxNumber())){
				lstData.add("Fax	:		");
				lstOldData.add(holderHistoryCopy.getFaxNumber());
				lstNewData.add(holderHistory.getFaxNumber());
			}
			if(Validations.validateIsNotNull(holderHistory.getEmail())){
				if(!holderHistory.getEmail().equals(holderHistoryCopy.getEmail())){
					lstData.add("Correo Electr\u00f3nico	:		");
					lstOldData.add(holderHistoryCopy.getEmail());
					lstNewData.add(holderHistory.getEmail());
				}
			} else if(Validations.validateIsNotNull(holderHistoryCopy.getEmail())){
				lstData.add("Correo Electr\u00f3nico	:		");
				lstOldData.add(holderHistoryCopy.getEmail());
				lstNewData.add(holderHistory.getEmail());
			}
			/** Datos de Contacto  * END **/	

			int contAux = 0;
			int contador = 0;
			List<HolderReqFileHistory> holderReqFileHistoryList = accountReportServiceBean.getHolderReqFileHistoryByHolderRequest(idHolderRequestPk);

			/** Archivos Adjuntos  * BEGIN **/
			for (int i = 0; i < holderReqFileHistoryList.size(); i++) {
				if(holderReqFileHistoryList.get(i).getRegistryType().equals(RegistryType.HISTORICAL_COPY.getCode())){
					holderReqFileHistoryCopy.add(holderReqFileHistoryList.get(i));
				}else if(holderReqFileHistoryList.get(i).getRegistryType().equals(RegistryType.ORIGIN_OF_THE_REQUEST.getCode())){
					holderReqFileHistory.add(holderReqFileHistoryList.get(i));
				}
			}
			for (int i = 0; i < holderReqFileHistory.size(); i++) {
				contAux = 0;
				for (int j = 0; j < holderReqFileHistoryCopy.size(); j++) {
					if(Validations.validateIsNotNull(holderReqFileHistory.get(i).getDocumentType())){
						if(holderReqFileHistory.get(i).getDocumentType().equals(holderReqFileHistoryCopy.get(j).getDocumentType())){
							if(!holderReqFileHistory.get(i).getFilename().equals(holderReqFileHistoryCopy.get(j).getFilename())){
									if(contador == 0){lstData.add("Archivos Adjuntos 	:		");}
									else {lstData.add("   ");}
									lstNewData.add(holderReqFileHistory.get(i).getDescription());
									contador++;
							}
							contAux++;
						} 
					} else {
						if(contador == 0){lstData.add("Archivos Adjuntos 	:		");}
						else {lstData.add("   ");}
						lstNewData.add(" ");
						contador++;
					}
				}
				if(contAux == 0) {
					if(contador == 0){lstData.add("Archivos Adjuntos 	:		");}
					else {lstData.add("   ");}
					lstNewData.add(holderReqFileHistory.get(i).getDescription());
					contador++;
				}
			}
			for (int i = 0; i < holderReqFileHistoryCopy.size(); i++) {
				contAux = 0;
				for (int j = 0; j < holderReqFileHistory.size(); j++) {
					if(Validations.validateIsNotNull(holderReqFileHistory.get(i).getDocumentType())){
						if(holderReqFileHistoryCopy.get(i).getDocumentType().equals(holderReqFileHistory.get(j).getDocumentType())){
							if(!holderReqFileHistoryCopy.get(i).getFilename().equals(holderReqFileHistory.get(j).getFilename())){
									lstOldData.add(holderReqFileHistoryCopy.get(i).getDescription());
							}
							contAux++;
						} 
					} else {
						lstOldData.add(" ");
					}
				}
				if(contAux == 0) {
					lstOldData.add(holderReqFileHistoryCopy.get(i).getDescription());
				}
			}

			if(lstNewData.size()>lstOldData.size()){
				for (int i = lstOldData.size(); i < lstData.size(); i++) {
					lstOldData.add("");
				}
			}
			if(lstOldData.size()>lstNewData.size()){
				for (int i = lstNewData.size(); i < lstData.size(); i++) {
					lstNewData.add("");
				}
			}
			/** Archivos Adjuntos  * END **/
			
			int contLegal = 0;
			List<LegalRepresentativeHistory> modificationLegalRepresentativeHistoryList = accountReportServiceBean.getLegalRepresentativeHistoryByHolderRequest(idHolderRequestPk);

			for (int i = 0; i < modificationLegalRepresentativeHistoryList.size(); i++) {
				
				if(modificationLegalRepresentativeHistoryList.get(i).getRegistryType().equals(RegistryType.HISTORICAL_COPY.getCode())){
					legalRepresentativeHistoryCopy.add(modificationLegalRepresentativeHistoryList.get(i));
				}else if(modificationLegalRepresentativeHistoryList.get(i).getRegistryType().equals(RegistryType.ORIGIN_OF_THE_REQUEST.getCode())){
					legalRepresentativeHistory.add(modificationLegalRepresentativeHistoryList.get(i));
				}
				
			}	
			
			if(legalRepresentativeHistory.size() == 0){
				legalRepresentativeHistory.add(new LegalRepresentativeHistory());
			}
			if(legalRepresentativeHistory.size()>=legalRepresentativeHistoryCopy.size()){
				contLegal = legalRepresentativeHistory.size();
			} else {
				contLegal = legalRepresentativeHistoryCopy.size();
			}
			int contRepresentative = 0;
			for (int i = 0; i < contLegal; i++) {
					if(i==0){
						lstData.add("");
						lstOldData.add("");
						lstNewData.add("");
						lstData.add("Datos del Representante Legal");
						lstOldData.add("");
						lstNewData.add("");
					}
					if(contRepresentative<lstData.size()){
						lstData.add("");
						lstOldData.add("");
						lstNewData.add("");
					}
					contRepresentative = lstData.size();
					Long pkHistory =  null;
					Long pkHistoryCopy =  null;
					try {
						pkHistory = legalRepresentativeHistory.get(i).getIdRepresentativeHistoryPk();
					} catch (IndexOutOfBoundsException e) {
						legalRepresentativeHistory.add(new LegalRepresentativeHistory());
					}
					try {
						pkHistoryCopy = legalRepresentativeHistoryCopy.get(i).getIdRepresentativeHistoryPk();
					} catch (IndexOutOfBoundsException e) {
						legalRepresentativeHistoryCopy.add(new LegalRepresentativeHistory());
					}
					
					/** Datos Representante Legal  * BEGIN **/
					if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistory.get(i).getPersonType())){
						if(!legalRepresentativeHistory.get(i).getPersonType().equals(legalRepresentativeHistoryCopy.get(i).getPersonType())){
							lstData.add("Tipo Representante	:		");
							lstOldData.add(legalRepresentativeHistoryCopy.get(i).getPersonTypeDescription());
							lstNewData.add(legalRepresentativeHistory.get(i).getPersonTypeDescription());
						}
					} else if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistoryCopy.get(i).getPersonType())){
						lstData.add("Tipo Representante	:		");
						lstOldData.add(legalRepresentativeHistoryCopy.get(i).getPersonTypeDescription());
						lstNewData.add(legalRepresentativeHistory.get(i).getPersonTypeDescription());
					}
		
					if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistory.get(i).getNationality())){
						if(!legalRepresentativeHistory.get(i).getNationality().equals(legalRepresentativeHistoryCopy.get(i).getNationality())){
							lstData.add("Pa\u00eds de Nacionalidad	:		");
							lstOldData.add(secPais.get(legalRepresentativeHistoryCopy.get(i).getNationality()));
							lstNewData.add(secPais.get(legalRepresentativeHistory.get(i).getNationality()));
						}
					} else if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistoryCopy.get(i).getNationality())){
						lstData.add("Pa\u00eds de Nacionalidad	:		");
						lstOldData.add(secPais.get(legalRepresentativeHistoryCopy.get(i).getNationality()));
						lstNewData.add(secPais.get(legalRepresentativeHistory.get(i).getNationality()));
					}
					
					if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistory.get(i).getIndResidence())){
						if(!legalRepresentativeHistory.get(i).getIndResidence().equals(legalRepresentativeHistoryCopy.get(i).getIndResidence())){
							lstData.add("Residente	:		");
							lstOldData.add(secBoolean.get(legalRepresentativeHistoryCopy.get(i).getIndResidence()));
							lstNewData.add(secBoolean.get(legalRepresentativeHistory.get(i).getIndResidence()));
						}
					} else if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistoryCopy.get(i).getIndResidence())){
						lstData.add("Residente	:		");
						lstOldData.add(secBoolean.get(legalRepresentativeHistoryCopy.get(i).getIndResidence()));
						lstNewData.add(secBoolean.get(legalRepresentativeHistory.get(i).getIndResidence()));
					}
		//			Pais de residencia
		//			if(!legalRepresentativeHistory.get(i).getLegalResidenceCountry().equals(legalRepresentativeHistoryCopy.get(i).getLegalResidenceCountry())){
		//				lstData.add("País	:		");
		//				lstOldData.add(legalRepresentativeHistoryCopy.get(i).getLegalResidenceCountry());
		//				lstNewData.add(legalRepresentativeHistory.get(i).getLegalResidenceCountry());
		//			}
		
					if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistory.get(i).getDocumentType())){
						if(!legalRepresentativeHistory.get(i).getDocumentType().equals(legalRepresentativeHistoryCopy.get(i).getDocumentType())){
							lstData.add("Tipo Documento	:		");
							lstOldData.add(secDocumentType.get(legalRepresentativeHistoryCopy.get(i).getDocumentType()));
							lstNewData.add(secDocumentType.get(legalRepresentativeHistory.get(i).getDocumentType()));
						}
					} else if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistoryCopy.get(i).getDocumentType())){
						lstData.add("Tipo Documento	:		");
						lstOldData.add(secDocumentType.get(legalRepresentativeHistoryCopy.get(i).getDocumentType()));
						lstNewData.add(secDocumentType.get(legalRepresentativeHistory.get(i).getDocumentType()));
					}
					
					if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistory.get(i).getDocumentNumber())){
						if(!legalRepresentativeHistory.get(i).getDocumentNumber().equals(legalRepresentativeHistoryCopy.get(i).getDocumentNumber())){
							lstData.add("N\u00famero Documento	:		");
							lstOldData.add(legalRepresentativeHistoryCopy.get(i).getDocumentNumber());
							lstNewData.add(legalRepresentativeHistory.get(i).getDocumentNumber());
						}
					} else if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistoryCopy.get(i).getDocumentNumber())){
						lstData.add("N\u00famero Documento	:		");
						lstOldData.add(legalRepresentativeHistoryCopy.get(i).getDocumentNumber());
						lstNewData.add(legalRepresentativeHistory.get(i).getDocumentNumber());
					}
					
					if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistory.get(i).getFullName())){
						if(!legalRepresentativeHistory.get(i).getFullName().equals(legalRepresentativeHistoryCopy.get(i).getFullName())){
							lstData.add("Nombre Completo	:		");
							lstOldData.add(legalRepresentativeHistoryCopy.get(i).getFullName());
							lstNewData.add(legalRepresentativeHistory.get(i).getFullName());
						}
					} else if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistoryCopy.get(i).getFullName())){
						lstData.add("Nombre Completo	:		");
						lstOldData.add(legalRepresentativeHistoryCopy.get(i).getFullName());
						lstNewData.add(legalRepresentativeHistory.get(i).getFullName());
					}
					
					if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistory.get(i).getEconomicSector())){
						if(!legalRepresentativeHistory.get(i).getEconomicSector().equals(legalRepresentativeHistoryCopy.get(i).getEconomicSector())){
							lstData.add("Sector Econ\u00f3mico	:		");
							lstOldData.add(secSecEcominico.get(legalRepresentativeHistoryCopy.get(i).getEconomicSector()));
							lstNewData.add(secSecEcominico.get(legalRepresentativeHistory.get(i).getEconomicSector()));
						}
					} else if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistoryCopy.get(i).getEconomicSector())){
						lstData.add("Sector Econ\u00f3mico	:		");
						lstOldData.add(secSecEcominico.get(legalRepresentativeHistoryCopy.get(i).getEconomicSector()));
						lstNewData.add(secSecEcominico.get(legalRepresentativeHistory.get(i).getEconomicSector()));
					}
					
					if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistory.get(i).getEconomicActivity())){
						if(!legalRepresentativeHistory.get(i).getEconomicActivity().equals(legalRepresentativeHistoryCopy.get(i).getEconomicActivity())){
							lstData.add("Actividad Econ\u00f3mica	:		");
							lstOldData.add(secActEconimica.get(legalRepresentativeHistoryCopy.get(i).getEconomicActivity()));
							lstNewData.add(secActEconimica.get(legalRepresentativeHistory.get(i).getEconomicActivity()));
						}
					} else if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistoryCopy.get(i).getEconomicActivity())){
						lstData.add("Actividad Econ\u00f3mica	:		");
						lstOldData.add(secActEconimica.get(legalRepresentativeHistoryCopy.get(i).getEconomicActivity()));
						lstNewData.add(secActEconimica.get(legalRepresentativeHistory.get(i).getEconomicActivity()));
					} 
					
					if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistory.get(i).getIndPEP())){
						if(!legalRepresentativeHistory.get(i).getIndPEP().equals(legalRepresentativeHistoryCopy.get(i).getIndPEP())){
							lstData.add("PEP	:		");
							lstOldData.add(secBoolean.get(legalRepresentativeHistoryCopy.get(i).getIndPEP()));
							lstNewData.add(secBoolean.get(legalRepresentativeHistory.get(i).getIndPEP()));
						}
					} else if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistoryCopy.get(i).getIndPEP())){
						lstData.add("PEP	:		");
						lstOldData.add(secBoolean.get(legalRepresentativeHistoryCopy.get(i).getIndPEP()));
						lstNewData.add(secBoolean.get(legalRepresentativeHistory.get(i).getIndPEP()));
					}
					
					if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistory.get(i).getLegalResidenceCountry())){
						if(!legalRepresentativeHistory.get(i).getLegalResidenceCountry().equals(legalRepresentativeHistoryCopy.get(i).getLegalResidenceCountry())){
							lstData.add("Pa\u00eds Legal	:		");
							lstOldData.add(secPais.get(legalRepresentativeHistoryCopy.get(i).getLegalResidenceCountry()));
							lstNewData.add(secPais.get(legalRepresentativeHistory.get(i).getLegalResidenceCountry()));
						}
					} else if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistoryCopy.get(i).getLegalResidenceCountry())){
						lstData.add("Pa\u00eds Legal	:		");
						lstOldData.add(secPais.get(legalRepresentativeHistoryCopy.get(i).getLegalResidenceCountry()));
						lstNewData.add(secPais.get(legalRepresentativeHistory.get(i).getLegalResidenceCountry()));
					}
					
					if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistory.get(i).getLegalDepartment())){
						if(!legalRepresentativeHistory.get(i).getLegalDepartment().equals(legalRepresentativeHistoryCopy.get(i).getLegalDepartment())){
							lstData.add("Departamento Legal	:		");
							lstOldData.add(secDepartamento.get(legalRepresentativeHistoryCopy.get(i).getLegalDepartment()));
							lstNewData.add(secDepartamento.get(legalRepresentativeHistory.get(i).getLegalDepartment()));
						}
					} else if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistoryCopy.get(i).getLegalDepartment())){
						lstData.add("Departamento Legal	:		");
						lstOldData.add(secDepartamento.get(legalRepresentativeHistoryCopy.get(i).getLegalDepartment()));
						lstNewData.add(secDepartamento.get(legalRepresentativeHistory.get(i).getLegalDepartment()));
					}
					
					if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistory.get(i).getLegalProvince())){
						if(!legalRepresentativeHistory.get(i).getLegalProvince().equals(legalRepresentativeHistoryCopy.get(i).getLegalProvince())){
							lstData.add("ProviCiudadl	:		");
							lstOldData.add(secProvincia.get(legalRepresentativeHistoryCopy.get(i).getLegalProvince()));
							lstNewData.add(secProvincia.get(legalRepresentativeHistory.get(i).getLegalProvince()));
						}
					} else if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistoryCopy.get(i).getLegalProvince())){
						lstData.add("ProviCiudadl	:		");
						lstOldData.add(secProvincia.get(legalRepresentativeHistoryCopy.get(i).getLegalProvince()));
						lstNewData.add(secProvincia.get(legalRepresentativeHistory.get(i).getLegalProvince()));
					}
		
					if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistory.get(i).getLegalDistrict())){
						if(!legalRepresentativeHistory.get(i).getLegalDistrict().equals(legalRepresentativeHistoryCopy.get(i).getLegalDistrict())){
							lstData.add("Barrio Legal	:		");
							lstOldData.add(secMunicipio.get(legalRepresentativeHistoryCopy.get(i).getLegalDistrict()));
							lstNewData.add(secMunicipio.get(legalRepresentativeHistory.get(i).getLegalDistrict()));
						}
					} else if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistoryCopy.get(i).getLegalDistrict())){
						lstData.add("Barrio Legal	:		");
						lstOldData.add(secMunicipio.get(legalRepresentativeHistoryCopy.get(i).getLegalDistrict()));
						lstNewData.add(secMunicipio.get(legalRepresentativeHistory.get(i).getLegalDistrict()));
					}
					
					if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistory.get(i).getPostalResidenceCountry())){
						if(!legalRepresentativeHistory.get(i).getPostalResidenceCountry().equals(legalRepresentativeHistoryCopy.get(i).getPostalResidenceCountry())){
							lstData.add("Pa\u00eds Postal	:		");
							lstOldData.add(secPais.get(legalRepresentativeHistoryCopy.get(i).getPostalResidenceCountry()));
							lstNewData.add(secPais.get(legalRepresentativeHistory.get(i).getPostalResidenceCountry()));
						}
					} else if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistoryCopy.get(i).getPostalResidenceCountry())){
						lstData.add("Pa\u00eds Postal	:		");
						lstOldData.add(secPais.get(legalRepresentativeHistoryCopy.get(i).getPostalResidenceCountry()));
						lstNewData.add(secPais.get(legalRepresentativeHistory.get(i).getPostalResidenceCountry()));
					}
					
					if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistory.get(i).getPostalDepartment())){
						if(!legalRepresentativeHistory.get(i).getPostalDepartment().equals(legalRepresentativeHistoryCopy.get(i).getPostalDepartment())){
							lstData.add("Departamento Postal	:		");
							lstOldData.add(secDepartamento.get(legalRepresentativeHistoryCopy.get(i).getPostalDepartment()));
							lstNewData.add(secDepartamento.get(legalRepresentativeHistory.get(i).getPostalDepartment()));
						}
					} else if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistoryCopy.get(i).getPostalDepartment())){
						lstData.add("Departamento Postal	:		");
						lstOldData.add(secDepartamento.get(legalRepresentativeHistoryCopy.get(i).getPostalDepartment()));
						lstNewData.add(secDepartamento.get(legalRepresentativeHistory.get(i).getPostalDepartment()));
					}
					
					if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistory.get(i).getPostalProvince())){
						if(!legalRepresentativeHistory.get(i).getPostalProvince().equals(legalRepresentativeHistoryCopy.get(i).getPostalProvince())){
							lstData.add("ProviCiudadal	:		");
							lstOldData.add(secProvincia.get(legalRepresentativeHistoryCopy.get(i).getPostalProvince()));
							lstNewData.add(secProvincia.get(legalRepresentativeHistory.get(i).getPostalProvince()));
						}
					} else if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistoryCopy.get(i).getPostalProvince())){
						lstData.add("ProviCiudadal	:		");
						lstOldData.add(secProvincia.get(legalRepresentativeHistoryCopy.get(i).getPostalProvince()));
						lstNewData.add(secProvincia.get(legalRepresentativeHistory.get(i).getPostalProvince()));
					}
					
					if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistory.get(i).getPostalDistrict())){
						if(!legalRepresentativeHistory.get(i).getPostalDistrict().equals(legalRepresentativeHistoryCopy.get(i).getPostalDistrict())){
							lstData.add("Barrio Postal	:		");
							lstOldData.add(secMunicipio.get(legalRepresentativeHistoryCopy.get(i).getPostalDistrict()));
							lstNewData.add(secMunicipio.get(legalRepresentativeHistory.get(i).getPostalDistrict()));
						}
					} else if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistoryCopy.get(i).getPostalDistrict())){
						lstData.add("Barrio Postal	:		");
						lstOldData.add(secMunicipio.get(legalRepresentativeHistoryCopy.get(i).getPostalDistrict()));
						lstNewData.add(secMunicipio.get(legalRepresentativeHistory.get(i).getPostalDistrict()));
					}
					
					if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistory.get(i).getPostalAddress())){
						if(!legalRepresentativeHistory.get(i).getPostalAddress().equals(legalRepresentativeHistoryCopy.get(i).getPostalAddress())){
							lstData.add("Direcci\u00f3n Postal	:		");
							lstOldData.add(legalRepresentativeHistoryCopy.get(i).getPostalAddress());
							lstNewData.add(legalRepresentativeHistory.get(i).getPostalAddress());
						}
					} else if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistoryCopy.get(i).getPostalAddress())){
						lstData.add("Direcci\u00f3n Postal	:		");
						lstOldData.add(legalRepresentativeHistoryCopy.get(i).getPostalAddress());
						lstNewData.add(legalRepresentativeHistory.get(i).getPostalAddress());
					}
					
					if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistory.get(i).getHomePhoneNumber())){
						if(!legalRepresentativeHistory.get(i).getHomePhoneNumber().equals(legalRepresentativeHistoryCopy.get(i).getHomePhoneNumber())){
							lstData.add("Tel\u00e9fono Casa	:		");
							lstOldData.add(legalRepresentativeHistoryCopy.get(i).getHomePhoneNumber());
							lstNewData.add(legalRepresentativeHistory.get(i).getHomePhoneNumber());
						}
					} else if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistoryCopy.get(i).getHomePhoneNumber())){
						lstData.add("Tel\u00e9fono Casa	:		");
						lstOldData.add(legalRepresentativeHistoryCopy.get(i).getHomePhoneNumber());
						lstNewData.add(legalRepresentativeHistory.get(i).getHomePhoneNumber());
					}
					
					if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistory.get(i).getOfficePhoneNumber())){
						if(!legalRepresentativeHistory.get(i).getOfficePhoneNumber().equals(legalRepresentativeHistoryCopy.get(i).getOfficePhoneNumber())){
							lstData.add("Tel\u00e9fono Oficina	:		");
							lstOldData.add(legalRepresentativeHistoryCopy.get(i).getOfficePhoneNumber());
							lstNewData.add(legalRepresentativeHistory.get(i).getOfficePhoneNumber());
						}
					} else if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistoryCopy.get(i).getOfficePhoneNumber())){
						lstData.add("Tel\u00e9fono Oficina	:		");
						lstOldData.add(legalRepresentativeHistoryCopy.get(i).getOfficePhoneNumber());
						lstNewData.add(legalRepresentativeHistory.get(i).getOfficePhoneNumber());
					}
					
					if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistory.get(i).getMobileNumber())){
						if(!legalRepresentativeHistory.get(i).getMobileNumber().equals(legalRepresentativeHistoryCopy.get(i).getMobileNumber())){
							lstData.add("Tel\u00e9fono Celular	:		");
							lstOldData.add(legalRepresentativeHistoryCopy.get(i).getMobileNumber());
							lstNewData.add(legalRepresentativeHistory.get(i).getMobileNumber());
						}
					} else if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistoryCopy.get(i).getMobileNumber())){
						lstData.add("Tel\u00e9fono Celular	:		");
						lstOldData.add(legalRepresentativeHistoryCopy.get(i).getMobileNumber());
						lstNewData.add(legalRepresentativeHistory.get(i).getMobileNumber());
					}
					
					if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistory.get(i).getFaxNumber())){
						if(!legalRepresentativeHistory.get(i).getFaxNumber().equals(legalRepresentativeHistoryCopy.get(i).getFaxNumber())){
							lstData.add("Fax	:		");
							lstOldData.add(legalRepresentativeHistoryCopy.get(i).getFaxNumber());
							lstNewData.add(legalRepresentativeHistory.get(i).getFaxNumber());
						}
					} else if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistoryCopy.get(i).getFaxNumber())){
						lstData.add("Fax	:		");
						lstOldData.add(legalRepresentativeHistoryCopy.get(i).getFaxNumber());
						lstNewData.add(legalRepresentativeHistory.get(i).getFaxNumber());
					}
					
					if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistory.get(i).getEmail())){
						if(!legalRepresentativeHistory.get(i).getEmail().equals(legalRepresentativeHistoryCopy.get(i).getEmail())){
							lstData.add("Correo Electr\u00f3nico	:		");
							lstOldData.add(legalRepresentativeHistoryCopy.get(i).getEmail());
							lstNewData.add(legalRepresentativeHistory.get(i).getEmail());
						}
					} else if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistoryCopy.get(i).getEmail())){
						lstData.add("Correo Electr\u00f3nico	:		");
						lstOldData.add(legalRepresentativeHistoryCopy.get(i).getEmail());
						lstNewData.add(legalRepresentativeHistory.get(i).getEmail());
					}
					/** Datos Representante Legal  * END **/
					
		
					contador = 0;
					contAux = 0;
					List<RepresentativeFileHistory> representativeFileHistory = new ArrayList<RepresentativeFileHistory>();
					List<RepresentativeFileHistory> representativeFileHistoryCopy = new ArrayList<RepresentativeFileHistory>();
					List<RepresentativeFileHistory> representativeFileHistoryList = accountReportServiceBean.getRepresentativeFileHistoryByRepresentativeHistory(pkHistory,pkHistoryCopy);
					
					/** Datos Representante Legal Archivos Adjuntos * BEGIN **/
					for (int k = 0; k < representativeFileHistoryList.size(); k++) {
						if(representativeFileHistoryList.get(k).getRegistryType().equals(RegistryType.HISTORICAL_COPY.getCode())){
							representativeFileHistoryCopy.add(representativeFileHistoryList.get(k));
						}else if(representativeFileHistoryList.get(k).getRegistryType().equals(RegistryType.ORIGIN_OF_THE_REQUEST.getCode())){
							representativeFileHistory.add(representativeFileHistoryList.get(k));
						}
					}
					for (int k = 0; k < representativeFileHistory.size(); k++) {
						contAux = 0;
						for (int l = 0; l < representativeFileHistoryCopy.size(); l++) {
							if(Validations.validateIsNotNullAndNotEmpty(representativeFileHistory.get(k).getDocumentType())){
								if(representativeFileHistory.get(k).getDocumentType().equals(representativeFileHistoryCopy.get(l).getDocumentType())){
									if(!representativeFileHistory.get(k).getFilename().equals(representativeFileHistoryCopy.get(l).getFilename())){
											if(contador == 0){lstData.add("Archivos Adjuntos RL	:		");}
											else {lstData.add("   ");}
											lstNewData.add(representativeFileHistory.get(k).getDescription());
											contador++;
									}
									contAux++;
								} 
							} else {
								if(contador == 0){lstData.add("Archivos Adjuntos RL	:		");}
								else {lstData.add("   ");}
								lstNewData.add("");
								contador++;
							}
						}
						if(contAux == 0) {
							if(contador == 0){lstData.add("Archivos Adjuntos RL	:		");}
							else {lstData.add("   ");}
							lstNewData.add(representativeFileHistory.get(k).getDescription());
							contador++;
						}
					}
					for (int k = 0; k < representativeFileHistoryCopy.size(); k++) {
						contAux = 0;
						for (int l = 0; l < representativeFileHistory.size(); l++) {
							if(Validations.validateIsNotNullAndNotEmpty(representativeFileHistoryCopy.get(k).getDocumentType())){
								if(representativeFileHistoryCopy.get(k).getDocumentType().equals(representativeFileHistory.get(l).getDocumentType())){
									if(!representativeFileHistoryCopy.get(k).getFilename().equals(representativeFileHistory.get(l).getFilename())){
											lstOldData.add(representativeFileHistoryCopy.get(k).getDescription());
									}
									contAux++;
								} 
							} else {
								lstOldData.add(" ");
							}
						}
						if(contAux == 0) {
							lstOldData.add(representativeFileHistoryCopy.get(k).getDescription());
						}
					}
					/** Datos Representante Legal Archivos Adjuntos * END **/
				

					if(lstNewData.size()>lstOldData.size()){
						for (int k = lstOldData.size(); k < lstData.size(); k++) {
							lstOldData.add("");
						}
					}
					if(lstOldData.size()>lstNewData.size()){
						for (int k = lstNewData.size(); k < lstData.size(); k++) {
							lstNewData.add("");
						}
					}
			} 
				

			for (int i = 0; i < lstOldData.size(); i++) {
				if(!Validations.validateIsNotNull(lstOldData.get(i))){
					lstOldData.set(i, " ");
				}
			}
			for (int i = 0; i < lstNewData.size(); i++) {
				if(!Validations.validateIsNotNull(lstNewData.get(i))){
					lstNewData.set(i, " ");
				}
			}
			
			parametersRequired.put("str_Query", strQuery);
			parametersRequired.put("lstData", lstData);
			parametersRequired.put("lstOldData", lstOldData);
			parametersRequired.put("lstNewData", lstNewData);
			parametersRequired.put("pState", secState);
			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}

		
		
		return parametersRequired;
		
		
	}

}
