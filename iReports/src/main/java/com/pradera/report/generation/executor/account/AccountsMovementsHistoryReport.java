package com.pradera.report.generation.executor.account;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "AccountsMovementsHistoryReport")
public class AccountsMovementsHistoryReport extends GenericReport {
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@Inject
	PraderaLogger log;

	
	public AccountsMovementsHistoryReport() {
	}
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParametersQueryReport() {

	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		
		Map<Integer,String> securityClass = new HashMap<Integer, String>();
		Map<Integer,String> accountType = new HashMap<Integer, String>();
		
		try {
		
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				securityClass.put(param.getParameterTablePk(), param.getDescription());
			}
			
			filter.setMasterTableFk(MasterTableType.ACCOUNTS_TYPE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				accountType.put(param.getParameterTablePk(), param.getDescription());
			}
		
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		parametersRequired.put("p_securityClass", securityClass);
		parametersRequired.put("p_accountType", accountType);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		
		return parametersRequired;
	}
	
	

}
