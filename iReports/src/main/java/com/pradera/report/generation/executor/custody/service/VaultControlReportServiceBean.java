package com.pradera.report.generation.executor.custody.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.to.VaultControlTO;
import com.pradera.report.generation.executor.custody.DateFilterType;
import com.pradera.report.generation.executor.custody.to.VaultControlReportPositionsTO;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class VaultControlReportServiceBean extends CrudDaoServiceBean {
	
	public VaultControlReportServiceBean(){
		
	}
	
	public String queryVaultControlReport(VaultControlTO filters) {
		StringBuilder sbQuery= new StringBuilder();
		
		sbQuery.append(" SELECT COUNT(cb.ID_SECURITY_CODE_FK) AS CANTIDAD_ACTUAL, ");
		sbQuery.append("(SELECT COUNT(*) FROM CONTROL_BOVEDA cb2 WHERE cb2.ARMARIO = cb.ARMARIO AND cb2.NIVEL = cb.NIVEL AND cb2.CAJA = cb.CAJA) ");
		sbQuery.append("- COUNT(cb.ID_SECURITY_CODE_FK) AS SALDO_DISPONIBLE, ");
		sbQuery.append("(SELECT COUNT(*) FROM CONTROL_BOVEDA cb2 WHERE cb2.ARMARIO = cb.ARMARIO AND cb2.NIVEL = cb.NIVEL AND cb2.CAJA = cb.CAJA) AS TOTAL, ");
		sbQuery.append(" cb.BOVEDA, cb.ARMARIO, cb.NIVEL, cb.CAJA ");
		sbQuery.append(" FROM CONTROL_BOVEDA cb WHERE 1=1 ");
		/*if(filters.getFechaVencimientoInicial()!=null) {
			sbQuery.append("AND SE:EXPIRATION_DATE >= TO_DATE('" + CommonsUtilities.convertDatetoString(filters.getFechaVencimientoInicial()) + "','DD/MM/YYYY') ");
		}
		if(filters.getFechaVencimientoFinal()!=null) {
			sbQuery.append("AND (SE:EXPIRATION_DATE <= TO_DATE('" + CommonsUtilities.convertDatetoString(filters.getFechaVencimientoInicial()) + "','DD/MM/YYYY') ");
		}		
		if(filters.getFechaRetiroInicial()!=null) {
			sbQuery.append(" AND (FECHA_RETIRO >= TIMESTAMP':pFechaRetiroInicial OR FECHA_RETIRO IS NULL)");
		}
		if(filters.getFechaRetiroFinal()!=null) {
			sbQuery.append(" AND (FECHA_RETIRO <= TIMESTAMP':pFechaRetiroFinal OR FECHA_RETIRO IS NULL)");
		}*/
		if(filters.getNivel()!=null) {
			sbQuery.append(" AND NIVEL = :pNivel");
		}
		if(filters.getArmario()!=null) {
			sbQuery.append(" AND ARMARIO = :pArmario");
		}
		if(filters.getCaja()!=null) {
			sbQuery.append(" AND CAJA = :pCaja");
		}
		sbQuery.append(" GROUP BY cb.BOVEDA, CB.NIVEL ,cb.ARMARIO, cb.CAJA ");
		sbQuery.append(" ORDER BY cb.ARMARIO ASC, cb.NIVEL ASC, cb.CAJA ASC ");
		
		return sbQuery.toString();
	}

	public String queryFormatForJasper(VaultControlTO vaultControlTO, String sbQuery){
    	String strQueryFormatted = sbQuery.toString();
    	
    	/*if(vaultControlTO.getFechaVencimientoInicial()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":pFechaVencimientoInicial", vaultControlTO.getFechaVencimientoInicial().concat("'"));
		}
    	if(vaultControlTO.getFechaVencimientoFinal()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":pFechaVencimientoFinal", vaultControlTO.getFechaVencimientoFinal().concat("'"));
		}
		if(vaultControlTO.getFechaRetiroInicial() !=null){
			strQueryFormatted=strQueryFormatted.replace(":pFechaRetiroInicial", vaultControlTO.getFechaRetiroInicial().concat("'"));
		}
		if(vaultControlTO.getFechaRetiroFinal() !=null){
			strQueryFormatted=strQueryFormatted.replace(":pFechaRetiroFinal", vaultControlTO.getFechaRetiroFinal().concat("'"));
		}*/
		if(vaultControlTO.getArmario()!=null) {
			strQueryFormatted=strQueryFormatted.replace(":pArmario", vaultControlTO.getArmario());
		}
		if(vaultControlTO.getNivel()!=null) {
			strQueryFormatted=strQueryFormatted.replace(":pNivel", vaultControlTO.getNivel().toString());
		}
		if(vaultControlTO.getCaja()!=null) {
			strQueryFormatted=strQueryFormatted.replace(":pCaja", vaultControlTO.getCaja().toString());
		}
		
		return strQueryFormatted;
    }
	
	public String queryVaultControlReportPositions(VaultControlReportPositionsTO filters) {
		
		StringBuilder sbQuery= new StringBuilder();
		/*
		sbQuery.append("SELECT CB.BOVEDA || '-' || 'A' || CB.ARMARIO || '-' || 'N' ||CB.NIVEL || '-' || 'C' || CB.CAJA || '-' ||'I' || CB.INDICE_CAJA AS UBICACION, ");
		sbQuery.append("ID_SECURITY_CODE_FK AS CODIGO_VALOR, SE.CURRENCY AS MONEDA, SE.CURRENT_NOMINAL_VALUE AS VALOR_NOMINAL, ");
		sbQuery.append("TO_CHAR(SE.EXPIRATION_DATE, 'DD/MM/YYYY') AS FECHA_VENCIMIENTO, ISSU.BUSINESS_NAME AS EMISOR, HA.RELATED_NAME AS NOMBRE ");
		sbQuery.append("FROM CONTROL_BOVEDA CB ,SECURITY SE, ISSUER ISSU, HOLDER_ACCOUNT_BALANCE HAB, HOLDER_ACCOUNT HA ");
		sbQuery.append("WHERE SE.ID_SECURITY_CODE_PK = CB.ID_SECURITY_CODE_FK AND ISSU.ID_ISSUER_PK = SE.ID_ISSUER_FK ");
		sbQuery.append("AND HAB.ID_SECURITY_CODE_PK = SE.ID_SECURITY_CODE_PK AND HAB.ID_HOLDER_ACCOUNT_PK = HA.ID_HOLDER_ACCOUNT_PK AND HAB.TOTAL_BALANCE > 0 ");*/
		
		sbQuery.append(" SELECT DISTINCT CB.BOVEDA, CB.ARMARIO, CB.NIVEL, CB.CAJA, CB.FOLIO, CB.INDICE_CAJA, ");
		sbQuery.append(" CASE WHEN CB.ID_PENDING_SECURITY_CODE_PK IS NULL ");
		sbQuery.append(" THEN CB.ID_SECURITY_CODE_FK ELSE CB.ID_PENDING_SECURITY_CODE_PK END AS CODIGO_VALOR, ");
		sbQuery.append(" PC.NOMINAL_VALUE AS VALOR_NOMINAL, I.BUSINESS_NAME AS EMISOR, PC.EXPIRATION_DATE AS FECHA_VENCIMIENTO, PC.CURRENCY AS MONEDA, FN_GET_HOLDER_DESC(HA.ID_HOLDER_ACCOUNT_PK, '-',0), ");
		sbQuery.append(" CB.REGISTRY_DATE AS FECHA_INGRESO ");
		sbQuery.append(" , CB.ID_SECURITY_CODE_FK ");
		sbQuery.append(" , NVL(cb.COUPON_NUMBER, 0) ");
		sbQuery.append(" FROM VAULT_CONTROL_VIEW CB INNER JOIN PHYSICAL_CERTIFICATE PC ");
		sbQuery.append(" ON PC.ID_PHYSICAL_CERTIFICATE_PK = CB.ID_PHYSICAL_CERTIFICATE_FK INNER JOIN ISSUER I ON I.ID_ISSUER_PK = PC.ID_ISSUER_FK ");
		sbQuery.append(" INNER JOIN ACCOUNT_ANNOTATION_OPERATION AAO ");
		sbQuery.append(" ON (PC.ID_PHYSICAL_CERTIFICATE_PK = AAO.ID_PHYSICAL_CERTIFICATE_FK ");
		sbQuery.append(" OR PC.PHYSICAL_CERTIFICATE_REFERENCE = AAO.ID_PHYSICAL_CERTIFICATE_FK) ");
		sbQuery.append(" INNER JOIN HOLDER_ACCOUNT HA ON");
		sbQuery.append(" HA.ID_HOLDER_ACCOUNT_PK = AAO.ID_HOLDER_ACCOUNT_FK ");
		sbQuery.append(" INNER JOIN PARTICIPANT P ON ");
		sbQuery.append(" HA.ID_PARTICIPANT_FK = P.ID_PARTICIPANT_PK ");
		sbQuery.append(" INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ");
		sbQuery.append(" ON HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append(" INNER JOIN HOLDER H ");
		sbQuery.append(" ON HAD.ID_HOLDER_FK  = H.ID_HOLDER_PK	");
		sbQuery.append(" WHERE CB.ID_SECURITY_CODE_FK IS NOT NULL ");
		
		if(filters.getValor() != null) {
			sbQuery.append(" AND CB.ID_SECURITY_CODE_FK = :pValor");
		}
		
		sbQuery.append(" AND TRUNC(CB.CUT_DATE) = TO_DATE('" + CommonsUtilities.convertDatetoString(filters.getCourtDate()) + "','DD/MM/YYYY') ");
		
		if(filters.getDateFilterType() != null) {
			if(filters.getInitialDate() != null && filters.getFinalDate() != null) {
				if(filters.getDateFilterType().equals(DateFilterType.ADMISSION_DATE.getCode().toString())) {
					sbQuery.append(" AND TRUNC(CB.REGISTRY_DATE) >= TO_DATE('"+CommonsUtilities.convertDatetoString(filters.getInitialDate())+"','DD/MM/YYYY') ");
					sbQuery.append(" AND TRUNC(CB.REGISTRY_DATE) <= TO_DATE('"+CommonsUtilities.convertDatetoString(filters.getFinalDate())+"','DD/MM/YYYY') ");
				}
				
				if(filters.getDateFilterType().equals(DateFilterType.EXPIRATION_DATE.getCode().toString())) {
					sbQuery.append(" AND TRUNC(PC.EXPIRATION_DATE) >= TO_DATE('"+CommonsUtilities.convertDatetoString(filters.getInitialDate())+"','DD/MM/YYYY') ");
					sbQuery.append(" AND TRUNC(PC.EXPIRATION_DATE) <= TO_DATE('"+CommonsUtilities.convertDatetoString(filters.getFinalDate())+"','DD/MM/YYYY') ");
				}
				
				if(filters.getDateFilterType().equals(DateFilterType.CUT_DATE.getCode().toString())) {
					sbQuery.append(" AND TRUNC(CB.REGISTRY_DATE) <= TO_DATE('"+CommonsUtilities.convertDatetoString(filters.getFinalDate())+"','DD/MM/YYYY') ");
				}
 			}
		}
		
		if(filters.getEmisor() != null) {
			sbQuery.append(" AND I.ID_ISSUER_PK = :emisor");
		}
		
		if(filters.getHolder() != null) {
			sbQuery.append(" AND H.ID_HOLDER_PK = :holder");
		}
		
		if(filters.getMoneda() != null) {
			sbQuery.append(" AND PC.CURRENCY = :currency");
		}
		
		if(filters.getParticipant() != null) {
			sbQuery.append(" AND P.ID_PARTICIPANT_PK = :participant");
		}
		
		/*if(filters.getFechaVencimientoInicial()!=null) {
			sbQuery.append("AND SE:EXPIRATION_DATE >= TO_DATE('" + CommonsUtilities.convertDatetoString(filters.getFechaVencimientoInicial()) + "','DD/MM/YYYY') ");
		}
		if(filters.getFechaVencimientoFinal()!=null) {
			sbQuery.append("AND (SE:EXPIRATION_DATE <= TO_DATE('" + CommonsUtilities.convertDatetoString(filters.getFechaVencimientoInicial()) + "','DD/MM/YYYY') ");
		}*/
		
		//sbQuery.append(" ORDER BY  CB.BOVEDA ASC, CB.ARMARIO ASC, CB.NIVEL ASC, CB.CAJA ASC, CB.FOLIO ASC, CB.INDICE_CAJA ASC ");
		sbQuery.append(" ORDER BY cb.ID_SECURITY_CODE_FK ASC, NVL(cb.COUPON_NUMBER, 0) ASC ");
		
		return sbQuery.toString();
	}
	
	public String queryFormatForPositionsJasper(VaultControlReportPositionsTO vaultControlReportPositionsTO, String sbQuery){
    	String strQueryFormatted = sbQuery.toString();

		if(vaultControlReportPositionsTO.getEmisor() != null) {
			strQueryFormatted=strQueryFormatted.replace(":emisor", "'" + vaultControlReportPositionsTO.getEmisor() + "'");
		}
		
		if(vaultControlReportPositionsTO.getValor() != null) {
			strQueryFormatted=strQueryFormatted.replace(":pValor", "'" + vaultControlReportPositionsTO.getValor() + "'");
		}
		
		if(vaultControlReportPositionsTO.getHolder() != null) {
			strQueryFormatted=strQueryFormatted.replace(":holder", vaultControlReportPositionsTO.getHolder().toString());
		}
		
		if(vaultControlReportPositionsTO.getMoneda() != null) {
			strQueryFormatted=strQueryFormatted.replace(":currency", vaultControlReportPositionsTO.getMoneda().toString());
		}
		
		if(vaultControlReportPositionsTO.getParticipant() != null) {
			strQueryFormatted=strQueryFormatted.replace(":participant", vaultControlReportPositionsTO.getParticipant().toString());
		}
		
		return strQueryFormatted;
    }
	
	public String queryVaultControlReportPositionsExcelDelimited(VaultControlReportPositionsTO filters) {
		
		StringBuilder sbQuery= new StringBuilder();
		
		sbQuery.append(" SELECT DISTINCT CB.ARMARIO, "); //0
		sbQuery.append(" CB.NIVEL, "); //1
		sbQuery.append(" CB.CAJA, "); //2
		sbQuery.append(" CB.FOLIO, "); //3
		sbQuery.append(" CB.INDICE_CAJA, "); //4
		sbQuery.append(" PC.CURRENCY AS MONEDA, "); //5
		sbQuery.append(" CASE WHEN CB.ID_PENDING_SECURITY_CODE_PK IS NULL THEN CB.ID_SECURITY_CODE_FK");
		sbQuery.append(" ELSE CB.ID_PENDING_SECURITY_CODE_PK ");
		sbQuery.append(" END AS SERIE, "); //6
		sbQuery.append(" PC.SECURITY_CLASS AS TIPO_VALOR, "); //7
		//sbQuery.append(" (SELECT MOTIVE FROM ACCOUNT_ANNOTATION_OPERATION AAO WHERE ");
		//sbQuery.append(" AAO.ID_SECURITY_CODE_FK = CB.ID_SECURITY_CODE_FK) AS TIPO_OPERACION, "); //8
		sbQuery.append(" AAO.MOTIVE AS TIPO_OPERACION, "); //8
		sbQuery.append(" I.BUSINESS_NAME AS EMISOR, "); //9
		sbQuery.append(" FN_GET_HOLDER_DESC(HA.ID_HOLDER_ACCOUNT_PK, '-',1)  AS TITULAR, "); //10
		sbQuery.append(" CB.REGISTRY_DATE AS FECHA_INGRESO, "); //11
		sbQuery.append(" PC.EXPIRATION_DATE AS FECHA_VENCIMIENTO, "); //12
		/*sbQuery.append(" (SELECT P.DESCRIPTION FROM ACCOUNT_ANNOTATION_OPERATION AAO ");
		sbQuery.append(" INNER JOIN PHYSICAL_CERTIFICATE PC ON ");
		sbQuery.append(" PC.ID_PHYSICAL_CERTIFICATE_PK = AAO.ID_PHYSICAL_CERTIFICATE_FK ");
		sbQuery.append(" INNER JOIN HOLDER_ACCOUNT HA ON ");
		sbQuery.append(" HA.ID_HOLDER_ACCOUNT_PK = AAO.ID_HOLDER_ACCOUNT_FK ");
		sbQuery.append(" INNER JOIN PARTICIPANT P ON HA.ID_PARTICIPANT_FK = P.ID_PARTICIPANT_PK ");
		sbQuery.append(" WHERE PC.ID_SECURITY_CODE_FK = CB.ID_SECURITY_CODE_FK) AS DEPOSITANTE ");*/ //12
		sbQuery.append(" P.DESCRIPTION  AS DEPOSITANTE, "); //13
		sbQuery.append(" HA.ACCOUNT_NUMBER AS NUMERO_CUENTA, "); //14
		sbQuery.append(" PC.NOMINAL_VALUE AS VALOR_NOMINAL, "); //15
		sbQuery.append(" FN_GET_HOLDER_DESC(HA.ID_HOLDER_ACCOUNT_PK, '-',2) AS RUT, "); //16
		sbQuery.append(" CASE WHEN SE.INSTRUMENT_TYPE <> 399 THEN 1 "); 
		sbQuery.append(" ELSE (AAO.CERTIFICATE_TO - AAO.CERTIFICATE_FROM + 1) END AS CANTIDAD, "); //17
		sbQuery.append(" AAO.CERTIFICATE_FROM AS DESDE, "); //18
		sbQuery.append(" AAO.CERTIFICATE_TO AS HASTA"); //19
		sbQuery.append(" FROM VAULT_CONTROL_VIEW CB ");
		sbQuery.append(" INNER JOIN PHYSICAL_CERTIFICATE PC ON ");
		sbQuery.append(" PC.ID_PHYSICAL_CERTIFICATE_PK = CB.ID_PHYSICAL_CERTIFICATE_FK ");
		sbQuery.append(" INNER JOIN ISSUER I ON ");
		sbQuery.append(" I.ID_ISSUER_PK = PC.ID_ISSUER_FK ");
		sbQuery.append(" INNER JOIN ACCOUNT_ANNOTATION_OPERATION AAO ");
		sbQuery.append(" ON (PC.ID_PHYSICAL_CERTIFICATE_PK = AAO.ID_PHYSICAL_CERTIFICATE_FK ");
		sbQuery.append(" OR PC.PHYSICAL_CERTIFICATE_REFERENCE = AAO.ID_PHYSICAL_CERTIFICATE_FK) ");
		sbQuery.append(" INNER JOIN HOLDER_ACCOUNT HA ON ");
		sbQuery.append(" HA.ID_HOLDER_ACCOUNT_PK = AAO.ID_HOLDER_ACCOUNT_FK ");
		sbQuery.append(" INNER JOIN PARTICIPANT P ON ");
		sbQuery.append(" HA.ID_PARTICIPANT_FK = P.ID_PARTICIPANT_PK ");
		sbQuery.append(" INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ");
		sbQuery.append(" ON HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append(" INNER JOIN HOLDER H ");
		sbQuery.append(" ON HAD.ID_HOLDER_FK  = H.ID_HOLDER_PK ");
		sbQuery.append(" INNER JOIN SECURITY SE ");
		sbQuery.append(" ON SE.ID_SECURITY_CODE_PK = CB.ID_SECURITY_CODE_FK ");
		sbQuery.append(" WHERE CB.ID_SECURITY_CODE_FK IS NOT NULL ");
			
		if(filters.getValor() != null) {
			sbQuery.append(" AND CB.ID_SECURITY_CODE_FK = " + "'" + filters.getValor() + "'");
		}
		
		sbQuery.append(" AND TRUNC(CB.CUT_DATE) = TO_DATE('" + CommonsUtilities.convertDatetoString(filters.getCourtDate()) + "','DD/MM/YYYY') ");
		
		if(filters.getDateFilterType() != null) {
			if(filters.getInitialDate() != null && filters.getFinalDate() != null) {
				if(filters.getDateFilterType().equals(DateFilterType.ADMISSION_DATE.getCode().toString())) {
					sbQuery.append(" AND TRUNC(CB.REGISTRY_DATE) >= TO_DATE('"+CommonsUtilities.convertDatetoString(filters.getInitialDate())+"','DD/MM/YYYY') ");
					sbQuery.append(" AND TRUNC(CB.REGISTRY_DATE) <= TO_DATE('"+CommonsUtilities.convertDatetoString(filters.getFinalDate())+"','DD/MM/YYYY') ");
				}
				
				if(filters.getDateFilterType().equals(DateFilterType.EXPIRATION_DATE.getCode().toString())) {
					sbQuery.append(" AND TRUNC(PC.EXPIRATION_DATE) >= TO_DATE('"+CommonsUtilities.convertDatetoString(filters.getInitialDate())+"','DD/MM/YYYY') ");
					sbQuery.append(" AND TRUNC(PC.EXPIRATION_DATE) <= TO_DATE('"+CommonsUtilities.convertDatetoString(filters.getFinalDate())+"','DD/MM/YYYY') ");
				}
				
				if(filters.getDateFilterType().equals(DateFilterType.CUT_DATE.getCode().toString())) {
					sbQuery.append(" AND TRUNC(CB.REGISTRY_DATE) <= TO_DATE('"+CommonsUtilities.convertDatetoString(filters.getFinalDate())+"','DD/MM/YYYY') ");
				}
 			}
		}
		
		if(filters.getEmisor() != null) {
			sbQuery.append(" AND I.ID_ISSUER_PK = " + "'" + filters.getEmisor() + "'");
		}
		
		if(filters.getHolder() != null) {
			sbQuery.append(" AND H.ID_HOLDER_PK = " + filters.getHolder().toString());
		}
		
		if(filters.getMoneda() != null) {
			sbQuery.append(" AND PC.CURRENCY = " + filters.getMoneda().toString());
		}
		
		if(filters.getParticipant() != null) {
			sbQuery.append(" AND P.ID_PARTICIPANT_PK = " + filters.getParticipant());
			
		}
		
		//sbQuery.append(" ORDER BY CB.ARMARIO ASC, CB.NIVEL ASC, ");
		//sbQuery.append(" CB.CAJA ASC, CB.FOLIO ASC, CB.INDICE_CAJA ASC ");
		sbQuery.append(" ORDER BY SERIE ASC ");
		
		return sbQuery.toString();
		
	}
	
	public byte[] generateExcelDelimited(Map<String,Object> parametersRequired, VaultControlReportPositionsTO filters) {
		
		String query = this.queryVaultControlReportPositionsExcelDelimited(filters);
		
		List<VaultControlReportPositionsTO> listResult = this.getReportDataExcelDelimited(query);

		byte[] bytes = null;
		
		List<String> lstHeadsSheet = new ArrayList<String>();
		
		StringBuilder headers = new StringBuilder();
				
		headers.append("ARMARIO;");
		headers.append("NIVEL;");
		headers.append("CAJA;");
		headers.append("FOLIO;");
		headers.append("INDICE CAJA;");
		headers.append("DEPOSITANTE;");
		headers.append("TIPO DE OPERACION;");
		headers.append("RUT;");
		headers.append("CUENTA TITULAR;");
		headers.append("TITULAR;");
		headers.append("TIPO DE VALOR;");
		headers.append("EMISOR;");
		headers.append("SERIE;");
		headers.append("MONEDA;");
		headers.append("VALOR NOMINAL;");
		headers.append("CANTIDAD;");
		headers.append("DESDE;");
		headers.append("HASTA;");
		headers.append("FECHA DE INGRESO;");
		headers.append("FECHA DE VENCIMIENTO;");
		
		
		lstHeadsSheet.add(headers.toString());
		
		String [] headerSheet = new String[lstHeadsSheet.size()];
		int m = 0;
		for (String title : lstHeadsSheet) {
			headerSheet[m] = title;
			m = ++m;
		}
		XSSFWorkbook libro= new XSSFWorkbook();
		String hoja = "Hoja 1";
		XSSFSheet hojas = libro.createSheet(hoja);
		CellStyle styleTitle = libro.createCellStyle();
	    Font fontTitle = libro.createFont();
	    fontTitle.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    fontTitle.setFontHeightInPoints((short)14); 
	    styleTitle.setFont(fontTitle);
	    
	    CellStyle styleSubTitle = libro.createCellStyle();
	    Font fontSubTitle = libro.createFont();
	    fontSubTitle.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    fontSubTitle.setFontHeightInPoints((short)10); 
	    styleSubTitle.setFont(fontSubTitle);
	    
		CellStyle style = libro.createCellStyle();
	    Font font = libro.createFont();
	    font.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    font.setFontHeightInPoints((short)10); 
	    style.setFont(font);
	    
	    XSSFRow row=hojas.createRow(0); 
		XSSFCell cellTitle= row.createCell(0); // 3 columna en ubicarse el titulo
		cellTitle.setCellStyle(styleTitle);  
		cellTitle.setCellValue("REPORTE DE CONTROL DE BOVEDA");
		
		if (Validations.validateIsNotNull(listResult) && !listResult.isEmpty()) {
			
			int i = 0;
			
			row = hojas.createRow(++i); 
    		for (int k = 0; k <headerSheet.length; k++) {
    			XSSFCell cell= row.createCell(k); 
    			cell.setCellStyle(style); 
    			cell.setCellValue(headerSheet[k]); 
    		}
    		
    		for(VaultControlReportPositionsTO vaultControlReportPositionsTO : listResult) {
    			
				row = hojas.createRow(++i); 
				int j = 0;
				StringBuilder result = new StringBuilder();
				result.append(vaultControlReportPositionsTO.getCabinet());
				result.append(";");
				
				result.append(vaultControlReportPositionsTO.getLevel());
				result.append(";");
				
				result.append(vaultControlReportPositionsTO.getBox());
				result.append(";");
				
				result.append(vaultControlReportPositionsTO.getFolio());
				result.append(";");
				
				result.append(vaultControlReportPositionsTO.getBoxIndex());
				result.append(";");
				
				result.append(vaultControlReportPositionsTO.getParticipantDescription());
				result.append(";");
				
				Map<Integer, String> map_operationType = (Map<Integer,String>) parametersRequired.get("p_dematerializationMotive");
				map_operationType.forEach((key, value) -> {
					if(key.toString().equals(vaultControlReportPositionsTO.getOperationType())) {
						result.append(value);
						result.append(";");
					}
				});
				
				result.append(vaultControlReportPositionsTO.getRut());
				result.append(";");
				
				result.append(vaultControlReportPositionsTO.getAccountNumber());
				result.append(";");
				
				result.append(vaultControlReportPositionsTO.getHolderDescription());
				result.append(";");
				
				Map<Integer, String> map_securityClass = (Map<Integer,String>) parametersRequired.get("p_securityClass");
				map_securityClass.forEach((key, value) -> {
					if(key.toString().equals(vaultControlReportPositionsTO.getSecurityClass())) {
						result.append(value);
						result.append(";");
					}
				});
				
				result.append(vaultControlReportPositionsTO.getEmisor());
				result.append(";");
				
				result.append(vaultControlReportPositionsTO.getValor());
				result.append(";");
				
				Map<Integer,String> map_currency = (Map<Integer,String>) parametersRequired.get("p_currency");
				map_currency.forEach((key, value) -> {
					if(key.toString().equals(vaultControlReportPositionsTO.getMoneda().toString())) {
						result.append(value);
						result.append(";");
					}
				});
				
				DecimalFormat formatter = new DecimalFormat("###,###.00");
				
				result.append(formatter.format(vaultControlReportPositionsTO.getNominalValue()));
				result.append(";");
				
				result.append(vaultControlReportPositionsTO.getQuantity());
				result.append(";");
				
				result.append(vaultControlReportPositionsTO.getSince() != null ? vaultControlReportPositionsTO.getSince() : "");
				result.append(";");
				
				result.append(vaultControlReportPositionsTO.getFrom() != null ? vaultControlReportPositionsTO.getFrom() : "");
				result.append(";");
				
				result.append(vaultControlReportPositionsTO.getEntryDate());
				result.append(";");
				
				result.append(vaultControlReportPositionsTO.getExpirationDate());
				result.append(";");
				
				XSSFCell
				cell = row.createCell(j);   cell.setCellValue(result.toString());
    		}
			
		}

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		
		try {
			try {
				libro.write(bos);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} finally {
		    try {
				bos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		bytes = bos.toByteArray();
		return bytes;
		
	}
	
	private List<VaultControlReportPositionsTO> getReportDataExcelDelimited(String query) {
		
		Query queryExecution= em.createNativeQuery(query);
		List<Object[]> listResult = queryExecution.getResultList();
		Locale esLocale = new Locale("es");
	    SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy", esLocale);

		List<VaultControlReportPositionsTO> listReportData = new ArrayList<VaultControlReportPositionsTO>();
		
		if(listResult != null && listResult.size() > 0) {
			for(Object[] iterator : listResult) {
				
				VaultControlReportPositionsTO vaultControlReportPositionsTO = new VaultControlReportPositionsTO();
				vaultControlReportPositionsTO.setCabinet(iterator[0].toString());
				vaultControlReportPositionsTO.setLevel(iterator[1].toString());
				vaultControlReportPositionsTO.setBox(iterator[2].toString());
				vaultControlReportPositionsTO.setFolio(iterator[3].toString());
				vaultControlReportPositionsTO.setBoxIndex(iterator[4].toString());
				vaultControlReportPositionsTO.setMoneda(Integer.valueOf(iterator[5].toString()));
				vaultControlReportPositionsTO.setValor(iterator[6].toString());
				vaultControlReportPositionsTO.setSecurityClass(iterator[7].toString());
				vaultControlReportPositionsTO.setOperationType(iterator[8].toString());
				vaultControlReportPositionsTO.setEmisor(iterator[9].toString());
				vaultControlReportPositionsTO.setHolderDescription(iterator[10].toString());
				vaultControlReportPositionsTO.setEntryDate(formatter.format((Date)iterator[11]));
			    vaultControlReportPositionsTO.setExpirationDate(iterator[12] != null ? formatter.format((Date)iterator[12]) : "");			
				vaultControlReportPositionsTO.setParticipantDescription(iterator[13].toString());
				vaultControlReportPositionsTO.setAccountNumber(iterator[14].toString());
				vaultControlReportPositionsTO.setNominalValue(new BigDecimal(iterator[15].toString()));
				vaultControlReportPositionsTO.setRut(iterator[16].toString());
				vaultControlReportPositionsTO.setQuantity(new BigDecimal(iterator[17].toString()));
				vaultControlReportPositionsTO.setSince(iterator[18] != null ? new BigDecimal(iterator[18].toString()) : null);
				vaultControlReportPositionsTO.setFrom(iterator[19] != null ? new BigDecimal(iterator[19].toString()) : null);
				
				
				listReportData.add(vaultControlReportPositionsTO);
			}
		}
	
		return listReportData;
	}

}
