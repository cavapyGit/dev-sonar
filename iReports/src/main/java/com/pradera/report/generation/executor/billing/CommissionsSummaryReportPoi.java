package com.pradera.report.generation.executor.billing;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.InstitutionType;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.helperui.service.BillingServiceQueryServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.type.EntityCollectionType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.billing.service.BillingServiceReportServiceBean;
import com.pradera.report.generation.executor.billing.to.CommissionsAnnotationAccountTo;
import com.pradera.report.generation.poi.ReportPoiGeneratorServiceBean;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CommissionsAnnotationAccountReport.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@ReportProcess(name = "CommissionsSummaryReportPoi")
public class CommissionsSummaryReportPoi extends GenericReport {
 
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The parameter service. */
	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	private BillingServiceQueryServiceBean billingServiceQueryServiceBean;
	
	@EJB
	private ParticipantServiceBean participantServiceBean;
	
	/** The Report poi generator service bean. */
	@Inject
	private ReportPoiGeneratorServiceBean reportPoiGeneratorServiceBean;
	
	@EJB
	private BillingServiceReportServiceBean billingServiceReportServiceBean;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	
	/**
	 * Instantiates a new commissions annotation account report.
	 */
	public CommissionsSummaryReportPoi() {
	}

	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#generateData(com.pradera.model.report.ReportLogger)
	 */
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] arrayByteExcel = new byte[16384];
		List<Object[]> queryMain=new ArrayList<Object[]>();
		
		CommissionsAnnotationAccountTo commissionsAnnotationAccountTo = new CommissionsAnnotationAccountTo();
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		Map<String,Object> parametersRequired = new HashMap<>();
		String serviceCode=null;
		Long participantCode=null;
		Long cuiCode=null;
		String tarifaCode=null;
		BillingService billingService=null;
		
		for (int i = 0; i < listaLogger.size(); i++){
			if(listaLogger.get(i).getFilterName().equals("p_service_code")){
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
			    	serviceCode=listaLogger.get(i).getFilterValue();
			    	commissionsAnnotationAccountTo.setServiceCode(serviceCode);
			    }
			}else if(listaLogger.get(i).getFilterName().equals("p_participant_code")){
				if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					participantCode=Long.parseLong(listaLogger.get(i).getFilterValue());
					commissionsAnnotationAccountTo.setIdParticipantPk(participantCode);
				}
			}else if(listaLogger.get(i).getFilterName().equals("cui_code")){
				if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					cuiCode=Long.parseLong(listaLogger.get(i).getFilterValue());
					commissionsAnnotationAccountTo.setCui(cuiCode);
				}
			}else if(listaLogger.get(i).getFilterName().equals("p_tariff_code")){
				if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					tarifaCode=listaLogger.get(i).getFilterValue();
					commissionsAnnotationAccountTo.setTarifaCode(tarifaCode);
				}
			}else if(listaLogger.get(i).getFilterName().equals("date_initial")){
				if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					commissionsAnnotationAccountTo.setDateInitial(listaLogger.get(i).getFilterValue());
				}
			}else if(listaLogger.get(i).getFilterName().equals("date_end")){
				if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					commissionsAnnotationAccountTo.setDateEnd(listaLogger.get(i).getFilterValue());
				}
			}
		}
		
		String strQueryFormated = billingServiceReportServiceBean.getQueryCommissionsSummary(commissionsAnnotationAccountTo);
		List<Object[]> lstObject = billingServiceReportServiceBean.getQueryListByClass(strQueryFormated);
		parametersRequired.put("lstObjects", lstObject);
		
		/**GENERATE FILE EXCEL ***/
		arrayByteExcel = reportPoiGeneratorServiceBean.writeFileExcelCommissionsSummary(parametersRequired,reportLogger);
		
		/**SETTING ARRAY BYTE TO GENERIC REPORT FOR PERSIST**/
		List<byte[]> bytes = new ArrayList<>();
		bytes.add(arrayByteExcel);
		setListByte(bytes);

		return baos;
	}

}
