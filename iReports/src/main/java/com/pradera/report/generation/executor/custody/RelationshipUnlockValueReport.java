package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.service.IssuerQueryServiceBean;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "RelationshipUnlockValueReport")
public class RelationshipUnlockValueReport extends GenericReport{

	/**
	 * 
	 */
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@Inject  PraderaLogger log; 
	
	@EJB
	ParticipantServiceBean participantServiceBean;
	
	@EJB
	IssuerQueryServiceBean issuerServiceBean;
	
	@EJB
	HolderAccountComponentServiceBean holderAccountServiceBean;
	
	public RelationshipUnlockValueReport() {

	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void addParametersQueryReport() {}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		
		Map<Integer, String> blockType = new HashMap<Integer, String>();
		Map<Integer, String> mStateUnBlock = new HashMap<Integer, String>();
		
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		String issuerCode = null;
		String issuerDescription = null;
		String participantCode = null;
		String participantDescription = null;
		String holderAccountCode = null;
		String holderAccountDescription = null;
		
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.PARTICIPANT_PARAM) && r.getFilterValue()!=null)
				participantCode = r.getFilterValue();
			if (r.getFilterName().equals(ReportConstant.ISSUER_PARAM))
				issuerCode = r.getFilterValue();
			if (r.getFilterName().equals(ReportConstant.ACCOUNT_HOLDER))
				holderAccountCode = r.getFilterValue();
		}

		try {
			
			if(participantCode != null){
				Participant participant = participantServiceBean.find(Participant.class, new Long(participantCode));
				participantDescription = participant.getMnemonic() + " - " + participant.getDescription();
			}else{
				participantDescription = "TODOS";
			}
			
			if(issuerCode != null){
				Issuer issuer = issuerServiceBean.find(Issuer.class, issuerCode);
				issuerDescription = issuer.getMnemonic();
			}else{
				issuerDescription = "TODOS";
			}
			
			if(holderAccountCode != null){
				HolderAccount account = holderAccountServiceBean.find(HolderAccount.class, new Long(holderAccountCode));
				holderAccountDescription = account.getAccountNumber().toString();
			}else{
				holderAccountDescription = "TODOS";
			}
			
			filter.setMasterTableFk(MasterTableType.TYPE_AFFECTATION.getCode());
			for (ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				blockType.put(param.getParameterTablePk(), param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.STATE_REVERSALS_REQUEST.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				mStateUnBlock.put(param.getParameterTablePk(), param.getDescription());
			}

		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		parametersRequired.put("p_participant", participantDescription);
		parametersRequired.put("p_account_holder", holderAccountDescription);
		parametersRequired.put("p_issuer", issuerDescription);
		parametersRequired.put("p_blockType", blockType);
		parametersRequired.put("mStateUnBlock", mStateUnBlock);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());

		// TODO Auto-generated method stub
		return parametersRequired;
	}
}
