package com.pradera.report.generation.executor.custody.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class PhysicalCertificateTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//form
	private String idIssuerPk;
	private Long idParticipantPk;	
	private Integer instrumentType;	
	private Integer state;							
	private Date initDate;	
	private Date endDate;
	private List<Integer> lstStates;
	
	
	public String getIdIssuerPk() {
		return idIssuerPk;
	}
	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	public Integer getInstrumentType() {
		return instrumentType;
	}
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public Date getInitDate() {
		return initDate;
	}
	public void setInitDate(Date initDate) {
		this.initDate = initDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public List<Integer> getLstStates() {
		return lstStates;
	}
	public void setLstStates(List<Integer> lstStates) {
		this.lstStates = lstStates;
	}			
	
}
