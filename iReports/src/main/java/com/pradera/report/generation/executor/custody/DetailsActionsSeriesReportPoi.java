package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.poi.ReportPoiGeneratorServiceBean;

@ReportProcess(name = "DetailsActionsSeriesReportPoi")
public class DetailsActionsSeriesReportPoi extends GenericReport{

	private static final long serialVersionUID = 1L;

	@EJB
	private CustodyReportServiceBean custodyReportServiceBean;
	@Inject
	PraderaLogger log;
	@Inject
	private ReportPoiGeneratorServiceBean reportPoiGeneratorServiceBean;

	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		Map<String,Object> parametersRequired = new HashMap<>();
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		
		for (ReportLoggerDetail  loggerDetail: loggerDetails) {
			if(Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue()))
			parametersRequired.put(loggerDetail.getFilterName(),loggerDetail.getFilterValue());
		}
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] arrayByteExcel = new byte[16384];
		
		List<Object[]> lstObject = custodyReportServiceBean.getActionsListByParameters(parametersRequired);
		parametersRequired.put("lstObjects", lstObject);
		
		/**GENERATE FILE EXCEL ***/
		arrayByteExcel = reportPoiGeneratorServiceBean.writeFileExcelActionsReportPoi(parametersRequired,reportLogger);
		/**SETTING ARRAY BYTE TO GENERIC REPORT FOR PERSIST**/
		List<byte[]> bytes = new ArrayList<>();
		bytes.add(arrayByteExcel);
		setListByte(bytes);
		
		return baos;
	}
	
}
