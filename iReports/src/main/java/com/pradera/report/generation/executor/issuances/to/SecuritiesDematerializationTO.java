package com.pradera.report.generation.executor.issuances.to;

import java.io.Serializable;
import java.math.BigDecimal;

public class SecuritiesDematerializationTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/*CURENCY*/
	private Integer currency;
	/*CURRENCY NEMONIC*/
	private String currencyMnemonic;
	/*CLASE DE VALOR*/
	private Integer securtyClass;
	/*CLASE DE VALOR NEMONICO*/
	private String securtyClassMnemonic;
	/*NOMINAL_VALUE_WEEK*/
	private BigDecimal nominalValueWeek;
	/*NOMINAL_VALUE_MONTHLY*/
	private BigDecimal nominalValueMonthly;
	/*TOTAL_BALANCE_WEEK*/
	private BigDecimal totalBalanceWeek;
	/*NOMINAL_VALUE_YEAR*/
	private BigDecimal nominalValueYear;
	/*TOTAL_BALANCE_YEAR*/
	private BigDecimal totalBalanceYear;
	/*NOMINAL_VALUE_ALL*/
	private BigDecimal nominalValueAll;
	/*TOTAL_BALANCE_ALL*/
	private BigDecimal totalBalanceAll;
	/*TC*/
	private BigDecimal exchangeRate;
	/*FACTOR_TC_USD*/
	private String exchangeRateDesc;
	
	public SecuritiesDematerializationTO() {
		// TODO Auto-generated constructor stub
	}
	public Integer getCurrency() {
		return currency;
	}
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	public String getCurrencyMnemonic() {
		return currencyMnemonic;
	}
	public void setCurrencyMnemonic(String currencyMnemonic) {
		this.currencyMnemonic = currencyMnemonic;
	}
	public Integer getSecurtyClass() {
		return securtyClass;
	}
	public void setSecurtyClass(Integer securtyClass) {
		this.securtyClass = securtyClass;
	}
	public String getSecurtyClassMnemonic() {
		return securtyClassMnemonic;
	}
	public void setSecurtyClassMnemonic(String securtyClassMnemonic) {
		this.securtyClassMnemonic = securtyClassMnemonic;
	}
	public BigDecimal getNominalValueWeek() {
		return nominalValueWeek;
	}
	public void setNominalValueWeek(BigDecimal nominalValueWeek) {
		this.nominalValueWeek = nominalValueWeek;
	}
	public BigDecimal getTotalBalanceWeek() {
		return totalBalanceWeek;
	}
	public void setTotalBalanceWeek(BigDecimal totalBalanceWeek) {
		this.totalBalanceWeek = totalBalanceWeek;
	}
	
	public BigDecimal getNominalValueMonthly() {
		return nominalValueMonthly;
	}
	public void setNominalValueMonthly(BigDecimal nominalValueMonthly) {
		this.nominalValueMonthly = nominalValueMonthly;
	}
	public BigDecimal getNominalValueYear() {
		return nominalValueYear;
	}
	public void setNominalValueYear(BigDecimal nominalValueYear) {
		this.nominalValueYear = nominalValueYear;
	}
	public BigDecimal getTotalBalanceYear() {
		return totalBalanceYear;
	}
	public void setTotalBalanceYear(BigDecimal totalBalanceYear) {
		this.totalBalanceYear = totalBalanceYear;
	}
	public BigDecimal getNominalValueAll() {
		return nominalValueAll;
	}
	public void setNominalValueAll(BigDecimal nominalValueAll) {
		this.nominalValueAll = nominalValueAll;
	}
	public BigDecimal getTotalBalanceAll() {
		return totalBalanceAll;
	}
	public void setTotalBalanceAll(BigDecimal totalBalanceAll) {
		this.totalBalanceAll = totalBalanceAll;
	}
	public BigDecimal getExchangeRate() {
		return exchangeRate;
	}
	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
	public String getExchangeRateDesc() {
		return exchangeRateDesc;
	}
	public void setExchangeRateDesc(String exchangeRateDesc) {
		this.exchangeRateDesc = exchangeRateDesc;
	}
}
