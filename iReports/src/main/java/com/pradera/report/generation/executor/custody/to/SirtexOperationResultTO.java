package com.pradera.report.generation.executor.custody.to;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.pradera.model.negotiation.type.MechanismOperationStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class OtcOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/04/2013
 */
public class SirtexOperationResultTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id mechanism operation request pk. */
	private Long idMechanismOperationRequestPk;
	
	/** The id negotiation modality pk. */
	private Long idNegotiationModalityPk;
	
	/** The modality name. */
	private String modalityName;
	
	/** The operation date. */
	private Date operationDate;
	
	/** The operation number. */
	private Long operationNumber;
	
	/** The id part seller. */
	private Long idPartSeller;
	
	/** The mnemo part seller. */
	private String mnemoPartSeller;
	
	/** The id part buyer. */
	private Long idPartBuyer;
	
	/** The mnemo part buyer. */
	private String mnemoPartBuyer;
	
	/** The id part in charge. */
	private Long idPartInCharge;
	
	/** The mnemo part in charge. */
	private String mnemoPartInCharge;
	
	/** The settlement cash date. */
	private Date settlementCashDate;
	
	/** The settlement term date. */
	private Date settlementTermDate;
	
	/** The isin code. */
	private String isinCode;
	
	/** The isin description. */
	private String isinDescription;
	
	/** The operation state. */
	private Integer operationState;
	
	/** The operation state desc. */
	private String operationStateDesc;
	
	/** The mcn operation state. */
	private Long mcnOperationState;
	
	/** The security code. */
	private String securityCode;
	
	/** The reference number. */
	private String referenceNumber;
	
	/** The modality code. */
	private String modalityCode;
	
	/** The reference date. */
	private Date referenceDate;
	
	/** The stock quantity. */
	private BigDecimal stockQuantity;
	
	/** The cash price. */
	private BigDecimal cashPrice;
	
	/** The cash amount. */
	private BigDecimal cashAmount;
	
	/** The term settlement days. */
	private Long termSettlementDays;
	
	/** The term price. */
	private BigDecimal termPrice;
	
	/** The term amount. */
	private BigDecimal termAmount;
	
	/** The id mcn process file fk. */
	private Long idMcnProcessFileFk;
	
	/** The term settlement days. */
	private Long termSettlementDaysReal;
	
	/** The registry user. */
	private String registryUser;
	
	/** The registry user. */
	private String coupon;
	
	/**Fecha de emision*/
	private Date issuanceDate;
	
	/**Fecha de expiracion*/
	private Date experitationDate;
	
	/**Tasa*/
	private BigDecimal rateExchange;
	
	/**Catidad ofertada*/
	private BigDecimal quantityOffere;
	
	/**Cantidad aceptada*/
	private BigDecimal acceptedAmount;
	
	/**Calse-calve de valor en formato BCB*/
	private String idSecurityCodePkBcb;
	
	/**Valor nominal*/
	private BigDecimal nominalValue;
	
	/***/
	private BigDecimal finalValue;
	
	/**Plazo restante**/
	private Integer remainingTerm;
	
	/**Agencia de bolsa*/
	private String exchangeAgency;
	
	/**Cliente*/
	private String client;
	
	private Integer securityClass;
	
	/**
	 * Gets the id mechanism operation request pk.
	 *
	 * @return the id mechanism operation request pk
	 */
	public Long getIdMechanismOperationRequestPk() {
		return idMechanismOperationRequestPk;
	}

	/**
	 * Sets the id mechanism operation request pk.
	 *
	 * @param idMechanismOperationRequestPk the new id mechanism operation request pk
	 */
	public void setIdMechanismOperationRequestPk(Long idMechanismOperationRequestPk) {
		this.idMechanismOperationRequestPk = idMechanismOperationRequestPk;
	}

	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return operationDate;
	}

	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}

	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}

	/**
	 * Gets the id part seller.
	 *
	 * @return the id part seller
	 */
	public Long getIdPartSeller() {
		return idPartSeller;
	}

	/**
	 * Sets the id part seller.
	 *
	 * @param idPartSeller the new id part seller
	 */
	public void setIdPartSeller(Long idPartSeller) {
		this.idPartSeller = idPartSeller;
	}

	/**
	 * Gets the mnemo part seller.
	 *
	 * @return the mnemo part seller
	 */
	public String getMnemoPartSeller() {
		return mnemoPartSeller;
	}

	/**
	 * Sets the mnemo part seller.
	 *
	 * @param mnemoPartSeller the new mnemo part seller
	 */
	public void setMnemoPartSeller(String mnemoPartSeller) {
		this.mnemoPartSeller = mnemoPartSeller;
	}

	/**
	 * Gets the id part buyer.
	 *
	 * @return the id part buyer
	 */
	public Long getIdPartBuyer() {
		return idPartBuyer;
	}

	/**
	 * Sets the id part buyer.
	 *
	 * @param idPartBuyer the new id part buyer
	 */
	public void setIdPartBuyer(Long idPartBuyer) {
		this.idPartBuyer = idPartBuyer;
	}

	/**
	 * Gets the mnemo part buyer.
	 *
	 * @return the mnemo part buyer
	 */
	public String getMnemoPartBuyer() {
		return mnemoPartBuyer;
	}

	/**
	 * Sets the mnemo part buyer.
	 *
	 * @param mnemoPartBuyer the new mnemo part buyer
	 */
	public void setMnemoPartBuyer(String mnemoPartBuyer) {
		this.mnemoPartBuyer = mnemoPartBuyer;
	}

	/**
	 * Gets the id part in charge.
	 *
	 * @return the id part in charge
	 */
	public Long getIdPartInCharge() {
		return idPartInCharge;
	}

	/**
	 * Sets the id part in charge.
	 *
	 * @param idPartInCharge the new id part in charge
	 */
	public void setIdPartInCharge(Long idPartInCharge) {
		this.idPartInCharge = idPartInCharge;
	}

	/**
	 * Gets the mnemo part in charge.
	 *
	 * @return the mnemo part in charge
	 */
	public String getMnemoPartInCharge() {
		return mnemoPartInCharge;
	}

	/**
	 * Sets the mnemo part in charge.
	 *
	 * @param mnemoPartInCharge the new mnemo part in charge
	 */
	public void setMnemoPartInCharge(String mnemoPartInCharge) {
		this.mnemoPartInCharge = mnemoPartInCharge;
	}

	/**
	 * Gets the settlement cash date.
	 *
	 * @return the settlement cash date
	 */
	public Date getSettlementCashDate() {
		return settlementCashDate;
	}

	/**
	 * Sets the settlement cash date.
	 *
	 * @param settlementCashDate the new settlement cash date
	 */
	public void setSettlementCashDate(Date settlementCashDate) {
		this.settlementCashDate = settlementCashDate;
	}

	/**
	 * Gets the settlement term date.
	 *
	 * @return the settlement term date
	 */
	public Date getSettlementTermDate() {
		return settlementTermDate;
	}

	/**
	 * Sets the settlement term date.
	 *
	 * @param settlementTermDate the new settlement term date
	 */
	public void setSettlementTermDate(Date settlementTermDate) {
		this.settlementTermDate = settlementTermDate;
	}

	/**
	 * Gets the isin code.
	 *
	 * @return the isin code
	 */
	public String getIsinCode() {
		return isinCode;
	}

	/**
	 * Sets the isin code.
	 *
	 * @param isinCode the new isin code
	 */
	public void setIsinCode(String isinCode) {
		this.isinCode = isinCode;
	}

	/**
	 * Gets the isin description.
	 *
	 * @return the isin description
	 */
	public String getIsinDescription() {
		return isinDescription;
	}

	/**
	 * Sets the isin description.
	 *
	 * @param isinDescription the new isin description
	 */
	public void setIsinDescription(String isinDescription) {
		this.isinDescription = isinDescription;
	}

	/**
	 * Gets the operation state.
	 *
	 * @return the operation state
	 */
	public Integer getOperationState() {
		return operationState;
	}
	
	/**
	 * Gets the operation state.
	 *
	 * @param operationState the new operation state
	 * @return the operation state
	 */
	/**
	 * Sets the operation state.
	 *
	 * @param operationState the new operation state
	 */
	public void setOperationState(Integer operationState) {
		this.operationState = operationState;
	}

	/**
	 * Gets the modality name.
	 *
	 * @return the modality name
	 */
	public String getModalityName() {
		return modalityName;
	}

	/**
	 * Sets the modality name.
	 *
	 * @param modalityName the new modality name
	 */
	public void setModalityName(String modalityName) {
		this.modalityName = modalityName;
	}

	/**
	 * Instantiates a new otc operation result to.
	 *
	 * @param idMechanismOperationRequestPk the id mechanism operation request pk
	 */
	public SirtexOperationResultTO(Long idMechanismOperationRequestPk) {
		super();
		this.idMechanismOperationRequestPk = idMechanismOperationRequestPk;
	}

	/**
	 * Instantiates a new otc operation result to.
	 *
	 * @param idMechanismOperationRequestPk the id mechanism operation request pk
	 * @param modalityName the modality name
	 */
	public SirtexOperationResultTO(Long idMechanismOperationRequestPk,String modalityName) {
		super();
		this.idMechanismOperationRequestPk = idMechanismOperationRequestPk;
		this.modalityName = modalityName;
	}

	/**
	 * Instantiates a new sirtex operation result to.
	 *
	 * @param idMechanismOperationRequestPk the id mechanism operation request pk
	 * @param modalityName the modality name
	 * @param operationDate the operation date
	 * @param operationNumber the operation number
	 * @param idPartSeller the id part seller
	 * @param mnemoPartSeller the mnemo part seller
	 * @param idPartBuyer the id part buyer
	 * @param mnemoPartBuyer the mnemo part buyer
	 */
	public SirtexOperationResultTO(Long idMechanismOperationRequestPk,
			String modalityName, Date operationDate, Long operationNumber,
			Long idPartSeller, String mnemoPartSeller, Long idPartBuyer,
			String mnemoPartBuyer) {
		super();
		this.idMechanismOperationRequestPk = idMechanismOperationRequestPk;
		this.modalityName = modalityName;
		this.operationDate = operationDate;
		this.operationNumber = operationNumber;
		this.idPartSeller = idPartSeller;
		this.mnemoPartSeller = mnemoPartSeller;
		this.idPartBuyer = idPartBuyer;
		this.mnemoPartBuyer = mnemoPartBuyer;
	}
	
	/**
	 * Instantiates a new sirtex operation result to.
	 *
	 * @param idMechanismOperationRequestPk the id mechanism operation request pk
	 * @param idNegotiationModalityPk the id negotiation modality pk
	 * @param modalityName the modality name
	 * @param operationDate the operation date
	 * @param idPartSeller the id part seller
	 * @param mnemoPartSeller the mnemo part seller
	 * @param idPartBuyer the id part buyer
	 * @param mnemoPartBuyer the mnemo part buyer
	 * @param settlementCashDate the settlement cash date
	 * @param settlementTermDate the settlement term date
	 * @param securityCode the security code
	 * @param state the state
	 */
	public SirtexOperationResultTO(Long idMechanismOperationRequestPk, Long idNegotiationModalityPk,
			String modalityName, Date operationDate, 
			Long idPartSeller, String mnemoPartSeller, Long idPartBuyer,
			String mnemoPartBuyer, Date settlementCashDate,
			Date settlementTermDate, String securityCode, Integer state) {
		super();
		this.idMechanismOperationRequestPk = idMechanismOperationRequestPk;
		this.idNegotiationModalityPk = idNegotiationModalityPk;
		this.modalityName = modalityName;
		this.operationDate = operationDate;
		this.idPartSeller = idPartSeller;
		this.mnemoPartSeller = mnemoPartSeller;
		this.idPartBuyer = idPartBuyer;
		this.mnemoPartBuyer = mnemoPartBuyer;
		this.settlementCashDate = settlementCashDate;
		this.settlementTermDate = settlementTermDate;
		this.securityCode = securityCode;
		this.operationState = state;
	}
	
	

	/**
	 * Instantiates a new sirtex operation result to.
	 *
	 * @param idMechanismOperationRequestPk the id mechanism operation request pk
	 * @param idNegotiationModalityPk the id negotiation modality pk
	 * @param modalityName the modality name
	 * @param operationDate the operation date
	 * @param operationNumber the operation number
	 * @param referenceNumber the reference number
	 * @param idPartSeller the id part seller
	 * @param mnemoPartSeller the mnemo part seller
	 * @param idPartBuyer the id part buyer
	 * @param mnemoPartBuyer the mnemo part buyer
	 * @param settlementCashDate the settlement cash date
	 * @param settlementTermDate the settlement term date
	 * @param isinCode the isin code
	 * @param operationState the operation state
	 * @param modalityCode the modality code
	 * @param referenceDate the reference date
	 * @param stockQuantity the stock quantity
	 * @param cashPrice the cash price
	 * @param cashAmount the cash amount
	 * @param termSettlementDays the term settlement days
	 * @param termPrice the term price
	 * @param termAmount the term amount
	 */
	public SirtexOperationResultTO(Long idMechanismOperationRequestPk,
			Long idNegotiationModalityPk, String modalityName,
			Date operationDate, Long operationNumber, String referenceNumber, Long idPartSeller,
			String mnemoPartSeller, Long idPartBuyer, String mnemoPartBuyer,
			Date settlementCashDate, Date settlementTermDate, String isinCode,
			Integer operationState, String modalityCode, Date referenceDate,
			BigDecimal stockQuantity, BigDecimal cashPrice, BigDecimal cashAmount,
			Long termSettlementDays, BigDecimal termPrice, BigDecimal termAmount) {
		super();
		this.idMechanismOperationRequestPk = idMechanismOperationRequestPk;
		this.idNegotiationModalityPk = idNegotiationModalityPk;
		this.modalityName = modalityName;
		this.operationDate = operationDate;
		this.operationNumber = operationNumber;
		this.referenceNumber = referenceNumber;
		this.idPartSeller = idPartSeller;
		this.mnemoPartSeller = mnemoPartSeller;
		this.idPartBuyer = idPartBuyer;
		this.mnemoPartBuyer = mnemoPartBuyer;
		this.settlementCashDate = settlementCashDate;
		this.settlementTermDate = settlementTermDate;
		this.isinCode = isinCode;
		this.operationState = operationState;
		this.modalityCode = modalityCode;
		this.referenceDate = referenceDate;
		this.stockQuantity = stockQuantity;
		this.cashPrice = cashPrice;
		this.cashAmount = cashAmount;
		this.termSettlementDays = termSettlementDays;
		this.termPrice = termPrice;
		this.termAmount = termAmount;
	}

	/**
	 * Instantiates a new sirtex operation result to.
	 */
	public SirtexOperationResultTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the id negotiation modality pk.
	 *
	 * @return the id negotiation modality pk
	 */
	public Long getIdNegotiationModalityPk() {
		return idNegotiationModalityPk;
	}

	/**
	 * Sets the id negotiation modality pk.
	 *
	 * @param idNegotiationModalityPk the new id negotiation modality pk
	 */
	public void setIdNegotiationModalityPk(Long idNegotiationModalityPk) {
		this.idNegotiationModalityPk = idNegotiationModalityPk;
	}

	/**
	 * Gets the modality code.
	 *
	 * @return the modalityCode
	 */
	public String getModalityCode() {
		return modalityCode;
	}

	/**
	 * Sets the modality code.
	 *
	 * @param modalityCode the modalityCode to set
	 */
	public void setModalityCode(String modalityCode) {
		this.modalityCode = modalityCode;
	}

	/**
	 * Gets the reference date.
	 *
	 * @return the referenceDate
	 */
	public Date getReferenceDate() {
		return referenceDate;
	}

	/**
	 * Sets the reference date.
	 *
	 * @param referenceDate the referenceDate to set
	 */
	public void setReferenceDate(Date referenceDate) {
		this.referenceDate = referenceDate;
	}

	/**
	 * Gets the stock quantity.
	 *
	 * @return the stockQuantity
	 */
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}

	/**
	 * Sets the stock quantity.
	 *
	 * @param stockQuantity the stockQuantity to set
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}


	/**
	 * Gets the term settlement days.
	 *
	 * @return the termSettlementDays
	 */
	public Long getTermSettlementDays() {
		return termSettlementDays;
	}

	/**
	 * Sets the term settlement days.
	 *
	 * @param termSettlementDays the termSettlementDays to set
	 */
	public void setTermSettlementDays(Long termSettlementDays) {
		this.termSettlementDays = termSettlementDays;
	}

	/**
	 * Gets the term price.
	 *
	 * @return the termPrice
	 */
	public BigDecimal getTermPrice() {
		return termPrice;
	}

	/**
	 * Sets the term price.
	 *
	 * @param termPrice the termPrice to set
	 */
	public void setTermPrice(BigDecimal termPrice) {
		this.termPrice = termPrice;
	}

	/**
	 * Gets the term amount.
	 *
	 * @return the termAmount
	 */
	public BigDecimal getTermAmount() {
		return termAmount;
	}

	/**
	 * Sets the term amount.
	 *
	 * @param termAmount the termAmount to set
	 */
	public void setTermAmount(BigDecimal termAmount) {
		this.termAmount = termAmount;
	}

	/**
	 * Gets the reference number.
	 *
	 * @return the referenceNumber
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}

	/**
	 * Sets the reference number.
	 *
	 * @param referenceNumber the referenceNumber to set
	 */
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	/**
	 * Gets the cash price.
	 *
	 * @return the cashPrice
	 */
	public BigDecimal getCashPrice() {
		return cashPrice;
	}

	/**
	 * Sets the cash price.
	 *
	 * @param cashPrice the cashPrice to set
	 */
	public void setCashPrice(BigDecimal cashPrice) {
		this.cashPrice = cashPrice;
	}

	/**
	 * Gets the cash amount.
	 *
	 * @return the cashAmount
	 */
	public BigDecimal getCashAmount() {
		return cashAmount;
	}

	/**
	 * Sets the cash amount.
	 *
	 * @param cashAmount the cashAmount to set
	 */
	public void setCashAmount(BigDecimal cashAmount) {
		this.cashAmount = cashAmount;
	}

	/**
	 * Gets the id mcn process file fk.
	 *
	 * @return the idMcnProcessFileFk
	 */
	public Long getIdMcnProcessFileFk() {
		return idMcnProcessFileFk;
	}

	/**
	 * Sets the id mcn process file fk.
	 *
	 * @param idMcnProcessFileFk the idMcnProcessFileFk to set
	 */
	public void setIdMcnProcessFileFk(Long idMcnProcessFileFk) {
		this.idMcnProcessFileFk = idMcnProcessFileFk;
	}

	/**
	 * Gets the mcn operation state.
	 *
	 * @return the mcn operation state
	 */
	public Long getMcnOperationState() {
		return mcnOperationState;
	}

	/**
	 * Sets the mcn operation state.
	 *
	 * @param mcnOperationState the new mcn operation state
	 */
	public void setMcnOperationState(Long mcnOperationState) {
		this.mcnOperationState = mcnOperationState;
	}
	
	/**
	 * Gets the mcn operation state desc.
	 *
	 * @return the mcn operation state desc
	 */
	public String getMcnOperationStateDesc() {
		MechanismOperationStateType negOperat = MechanismOperationStateType.lookup.get(operationState);
		if(negOperat!=null){
			return negOperat.getValue();
		}
		return "";
	}

	/**
	 * Gets the security code.
	 *
	 * @return the security code
	 */
	public String getSecurityCode() {
		return securityCode;
	}

	/**
	 * Sets the security code.
	 *
	 * @param securityCode the new security code
	 */
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	/**
	 * Gets the operation state desc.
	 *
	 * @return the operation state desc
	 */
	public String getOperationStateDesc() {
		return operationStateDesc;
	}

	/**
	 * Sets the operation state desc.
	 *
	 * @param operationStateDesc the new operation state desc
	 */
	public void setOperationStateDesc(String operationStateDesc) {
		this.operationStateDesc = operationStateDesc;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registryUser
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the registryUser to set
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public Long getTermSettlementDaysReal() {
		return termSettlementDaysReal;
	}

	public void setTermSettlementDaysReal(Long termSettlementDaysReal) {
		this.termSettlementDaysReal = termSettlementDaysReal;
	}

	public String getCoupon() {
		return coupon;
	}

	public void setCoupon(String coupon) {
		this.coupon = coupon;
	}

	public Date getIssuanceDate() {
		return issuanceDate;
	}

	public void setIssuanceDate(Date issuanceDate) {
		this.issuanceDate = issuanceDate;
	}

	public Date getExperitationDate() {
		return experitationDate;
	}

	public void setExperitationDate(Date experitationDate) {
		this.experitationDate = experitationDate;
	}

	public BigDecimal getRateExchange() {
		return rateExchange;
	}

	public void setRateExchange(BigDecimal rateExchange) {
		this.rateExchange = rateExchange;
	}

	public BigDecimal getQuantityOffere() {
		return quantityOffere;
	}

	public void setQuantityOffere(BigDecimal quantityOffere) {
		this.quantityOffere = quantityOffere;
	}

	public BigDecimal getAcceptedAmount() {
		return acceptedAmount;
	}

	public void setAcceptedAmount(BigDecimal acceptedAmount) {
		this.acceptedAmount = acceptedAmount;
	}

	public String getIdSecurityCodePkBcb() {
		return idSecurityCodePkBcb;
	}

	public void setIdSecurityCodePkBcb(String idSecurityCodePkBcb) {
		this.idSecurityCodePkBcb = idSecurityCodePkBcb;
	}

	public BigDecimal getNominalValue() {
		return nominalValue;
	}

	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}

	public BigDecimal getFinalValue() {
		return finalValue;
	}

	public void setFinalValue(BigDecimal finalValue) {
		this.finalValue = finalValue;
	}

	public Integer getRemainingTerm() {
		return remainingTerm;
	}

	public void setRemainingTerm(Integer remainingTerm) {
		this.remainingTerm = remainingTerm;
	}

	public Integer getSecurityClass() {
		return securityClass;
	}

	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	public String getExchangeAgency() {
		return exchangeAgency;
	}

	public void setExchangeAgency(String exchangeAgency) {
		this.exchangeAgency = exchangeAgency;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}
}
