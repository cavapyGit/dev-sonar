package com.pradera.report.generation.executor.billing;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.billing.service.BillingServiceReportServiceBean;
import com.pradera.report.generation.executor.billing.to.ConsolidatedCollectionByEntityTO;
import com.pradera.report.generation.poi.ReportPoiGeneratorServiceBean;

@ReportProcess(name = "ConsolidatedCollectionByEntityReportPoi")
public class ConsolidatedCollectionByEntityReportPoi  extends GenericReport{

	private static final long serialVersionUID = 1L;
	
	@EJB
	private BillingServiceReportServiceBean billingServiceReportServiceBean;
	@Inject
	private ReportPoiGeneratorServiceBean reportPoiGeneratorServiceBean;

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] arrayByteExcel = new byte[16384];
		ConsolidatedCollectionByEntityTO consCollectionByEntityTO = new ConsolidatedCollectionByEntityTO();
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		Map<String, Object> parametersRequired = new HashMap<>();
		
		for (ReportLoggerDetail r : listaLogger) {
			if (r.getFilterName().equals("entity_collection"))
				if (r.getFilterValue() != null)
					consCollectionByEntityTO.setEntityCollection(r.getFilterValue());
			
			if (r.getFilterName().equals("collection_state"))
				if (r.getFilterValue() != null)
					consCollectionByEntityTO.setCollectionState(r.getFilterValue());
			
			if (r.getFilterName().equals("issuer"))
				if (r.getFilterValue() != null)
					consCollectionByEntityTO.setIdIssuer(r.getFilterValue());
			
			if (r.getFilterName().equals("participant"))
				if (r.getFilterValue() != null)
					consCollectionByEntityTO.setIdParticipant(r.getFilterValue());
			
			if (r.getFilterName().equals("holder"))
				if (r.getFilterValue() != null)
					consCollectionByEntityTO.setIdHolder(r.getFilterValue());
			
			if (r.getFilterName().equals("other_number"))
				if (r.getFilterValue() != null)
					consCollectionByEntityTO.setOtherNumber(r.getFilterValue());
			
			if (r.getFilterName().equals("other_description"))
				if (r.getFilterValue() != null)
					consCollectionByEntityTO.setOtherDescription(r.getFilterValue());
			
			if (r.getFilterName().equals("afp"))
				if (r.getFilterValue() != null)
					consCollectionByEntityTO.setAfp(r.getFilterValue());
			
			if (r.getFilterName().equals("date_initial"))
				if (r.getFilterValue() != null)
					consCollectionByEntityTO.setInitialDate(r.getFilterValue());
			
			if (r.getFilterName().equals("date_end"))
				if (r.getFilterValue() != null)
					consCollectionByEntityTO.setFinalDate(r.getFilterValue());
			
			if (r.getFilterName().equals("p_service_code"))
				if (r.getFilterValue() != null)
					consCollectionByEntityTO.setServiceCode(r.getFilterValue());
		}
		
		String strQueryFormated = billingServiceReportServiceBean.getQueryConsCollectionByEntity(consCollectionByEntityTO);
		List<Object[]> lstObject = billingServiceReportServiceBean.getQueryListByClass(strQueryFormated);
		parametersRequired.put("lstObjects", lstObject);
		
		/**GENERATE FILE EXCEL ***/
		arrayByteExcel = reportPoiGeneratorServiceBean.writeFileExcelConsCollectionByEntityPoi(parametersRequired,reportLogger);
		/**SETTING ARRAY BYTE TO GENERIC REPORT FOR PERSIST**/
		List<byte[]> bytes = new ArrayList<>();
		bytes.add(arrayByteExcel);
		setListByte(bytes);
		
		return baos;
	}

}
