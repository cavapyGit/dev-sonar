package com.pradera.report.generation.executor.negotiation;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.model.report.ReportLogger;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;

@ReportProcess(name = "AnticipatedSettlementReportosSirtexReport")
public class AnticipatedSettlementReportosSirtexReport extends GenericReport{

	private static final long serialVersionUID = 1L;
	
	@Inject  PraderaLogger log;
		
	@EJB
	private ParameterServiceBean parameterService;

	public AnticipatedSettlementReportosSirtexReport() {
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParametersQueryReport() {

	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String, Object> parametersRequired = new HashMap<>();
		
		// TODO Auto-generated method stub
		return parametersRequired;
	}
	
}
