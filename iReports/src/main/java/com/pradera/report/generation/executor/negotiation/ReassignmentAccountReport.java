package com.pradera.report.generation.executor.negotiation;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.securities.service.PaymentChronogramBySecurity;
import com.pradera.report.generation.executor.securities.to.ReassignmentAccountTO;
import com.pradera.report.generation.executor.securities.to.SecuritiesByIssuanceAndIssuerTO;
import com.pradera.report.generation.executor.settlement.service.SettlementReportServiceBean;

@ReportProcess(name = "ReassignmentAccountReport")
public class ReassignmentAccountReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private SettlementReportServiceBean settlementService;
	@EJB
	private ParameterServiceBean parameterService;
	
	public ReassignmentAccountReport() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	String partic = null;
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		ReassignmentAccountTO reAccountTO = new ReassignmentAccountTO();

		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("participant")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					reAccountTO.setIdParticipantPk(Integer.valueOf(listaLogger.get(i).getFilterValue().toString()));
					partic = settlementService.getParticipantDetail(reAccountTO.getIdParticipantPk());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("reason")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					reAccountTO.setReason(Integer.valueOf(listaLogger.get(i).getFilterValue().toString()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("date_initial")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					reAccountTO.setInitialDate(CommonsUtilities.convertStringtoDate(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("date_end")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					reAccountTO.setFinalDate(CommonsUtilities.convertStringtoDate(listaLogger.get(i).getFilterValue()));
				}
			}	
		}

		// Query listo para ejecutar
		String strQuery = settlementService.reassignmentAccountReport(reAccountTO);
		System.out.println("::: Quey:\n"+strQuery);
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> map_reason = new HashMap<Integer, String>();
		try {
			//filter.setMasterTableFk(MasterTableType.REQUEST_REASON_REASSIGNMENT.getCode());
			filter.setMasterTableFk(589);
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				map_reason.put(param.getParameterTablePk(), param.getDescription());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		parametersRequired.put("map_reason", map_reason);
		parametersRequired.put("p_reason", reAccountTO.getReason()!=null?String.valueOf(reAccountTO.getReason()):null);
		parametersRequired.put("p_participant", reAccountTO.getIdParticipantPk()!=null?String.valueOf(partic):null);
		parametersRequired.put("str_query", strQuery);
		
		return parametersRequired;
	}

}
