package com.pradera.report.generation.executor.settlement;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.account.service.AccountReportServiceBean;
import com.pradera.report.generation.executor.account.to.NoteRegistrationDGravamenTO;
import com.pradera.report.generation.executor.account.to.NoteToExternalTO;

@ReportProcess(name = "CommunicationDefaultReport")
public class CommunicationDefaultReport extends GenericReport {

	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;

	@EJB
	private AccountReportServiceBean reportServiceBean;

	@Inject
	PraderaLogger log;
	
	public static final String ARCHETYPE_MRS = "a ";

	public static final String EXECUTIVE_DIRECTOR_ARCH_MRS = "DIRECTORA EJECUTIVA a.i.";

	public static final String ASFI = "AUTORIDAD DE SUPERVISI\u00d3N DEL SISTEMA FINANCIERO";

	//public static final String BODY_DOC_1 = "De nuestra consideraci\u00f3n<br>";
			//+ " Comunicamos a usted que en respuesta a la solicitud electr\u00f3nica N° ";

	//public static final String BODY_DOC_1_1 = "de ";
	
	//public static final String BODY_DOC_1_2 = "de fecha ";
	
	//public static final String BODY_DOC_1_3 = ", hemos inscrito el gravamen, en el sistema de"
	//		+ " Registro de Anotaciones en Cuenta a cargo de nuestra Entidad, en favor de la ";
	
	//public static final String BODY_DOC_1_4 = "con los siguientes valores: ";
	
	//public static final String BODY_DOC_1_5 = "Asimismo y en base a la nota, el levantamiento de dicho gravamen"
		//	+ " solamente podrá ser efectuado con la autorizaci\u00f3n de la AUTORIDAD DE SUPERVISION DEL SISTEMA"
			//+ " FINANCIERO.";
	
	//public static final String BODY_DOC_2 = "De nuestra consideraci\u00f3n:<br><br><br><br><br>"
		//	+ " En atenci\u00f3n a su nota ";
	
	//public static final String BODY_DOC_2_1 = ", comunicamos a usted que en la fecha nuestra Entidad procedi\u00f3"
		//	+ " a desbloquear los siguientes valores pignorados a favor de la ";
	
	/*public static final String BODY_DOC_2_2 = ".<br><br>Asimismo, para los fines que corresponda, adjunto a la presente enviamos"
			+ " a usted el reporte de desbloqueos emitido por nuestra Entidad.";

	public static final String BODY_DOC_FAREWELL_1 = ".<br><br>Sin otro particular, nos despedimos de usted con las"
			+ " consideraciones más distinguidas."
			+ "<br>Atentamente, ";
	public static final String BODY_DOC_FAREWELL_2 = "<br><br>Agradeceremos tomar nota de la presente, sin otro particular,"
			+ " nos despedimos de usted con las consideraciones más distinguidas."
			+ "<br><br>Atentamente, ";*/
	
	public CommunicationDefaultReport() {}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParametersQueryReport() {

	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {

		Map<String, Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		NoteToExternalTO noteExternalTO = new NoteToExternalTO();
		//NoteRegistrationDGravamenTO noteRegistrationDGravamenTO = new NoteRegistrationDGravamenTO();

		// DATOS DE ENTRADA DEL SISTEMA
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.REFERENCE_NOTE_EXTERNAL)) {
				noteExternalTO.setIdReferencePk(Integer.parseInt(r.getFilterValue()));
			}
			/*if (r.getFilterName().equals(ReportConstant.QUOTE_NOTE_EXTERNAL)) {
				noteExternalTO.setQuote(r.getFilterValue());
			}
			if (r.getFilterName().equals(ReportConstant.ATTACHED_SHEETS)) {
				noteExternalTO.setAttachedSheets(r.getFilterValue());
			}*/
		}
		
		/*Map<Integer,String> securityClass = new HashMap<Integer, String>();
		Map<Integer,String> block_type = new HashMap<Integer, String>();*/
		//Map<Integer,String> mnemonic = new HashMap<Integer, String>();
		
		/*try {
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				securityClass.put(param.getParameterTablePk(), param.getParameterName());
				//mnemonic.put(param.getParameterTablePk(), param.getText1());
			}
			filter.setMasterTableFk(MasterTableType.BLOCK_TYPE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				block_type.put(param.getParameterTablePk(), param.getParameterName());
			}
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}*/

		// DESCRIPCION DE LA REFERENCIA SELECCIONADA
		try {
			Integer idReference = noteExternalTO.getIdReferencePk();
			String desReference = parameterService.findParameterTableDescription(idReference);
			noteExternalTO.setDesReference(desReference);
		} catch (Exception ex) {
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}

		// DATOS SELECCIONADOS SEGUN PK DE LA REFERENCIA
		switch (noteExternalTO.getIdReferencePk()) {
			//GRAVAMEN
			case 2167:
				noteExternalTO.setArchetype(ARCHETYPE_MRS);
				noteExternalTO.setRecipient(getRecipientNoteToExternal(2013));
				noteExternalTO.setOccupation(EXECUTIVE_DIRECTOR_ARCH_MRS);
				noteExternalTO.setDepartment(ASFI);
				//noteExternalTO.setBodyDoc(BODY_DOC_1_5 + BODY_DOC_FAREWELL_1);
				noteExternalTO.setBlockType("1115");
				break;
			//DEGRAVAMEN
			case 2168:
				noteExternalTO.setArchetype(ARCHETYPE_MRS);
				noteExternalTO.setRecipient(getRecipientNoteToExternal(2013));
				noteExternalTO.setOccupation(EXECUTIVE_DIRECTOR_ARCH_MRS);
				noteExternalTO.setDepartment(ASFI);
				//noteExternalTO.setBodyDoc(BODY_DOC_2_2 + BODY_DOC_FAREWELL_2);
				noteExternalTO.setBlockType("1117");
				break;
		}

		//parametersRequired.put("dateHeader", noteExternalTO.getDateHeader());
		parametersRequired.put("quote", noteExternalTO.getQuote());
		parametersRequired.put("archetype", noteExternalTO.getArchetype());
		parametersRequired.put("recipient", noteExternalTO.getRecipient());
		parametersRequired.put("ocupation", noteExternalTO.getOccupation());
		parametersRequired.put("department", noteExternalTO.getDepartment());
		//parametersRequired.put("atn", noteExternalTO.getAtn());
		parametersRequired.put("desReference", noteExternalTO.getDesReference());
		//parametersRequired.put("bodyDoc", noteExternalTO.getBodyDoc());
		parametersRequired.put("attached_sheets", noteExternalTO.getAttachedSheets());
		parametersRequired.put("block_type", noteExternalTO.getBlockType());
		//parametersRequired.put("p_securityClass", securityClass);
		//parametersRequired.put("p_mnemonic_sc", mnemonic);

		return parametersRequired;
	}

	// METODO PARA OBTENER LA ULTIMA FECHA DE GENERACION DE CODIGOS ISIN Y CFI
	/*public String getDateLastCodeGeneration() {
		return CommonsUtilities.convertDateToStringLocaleES(
				reportServiceBean.getDateLastCodeGeneration(),
				ReportConstant.DATE_FORMAT_DOCUMENT);
	}*/

	// METODO PARA OBTENER EL DESTINATARIO DEL DOCUMENTO
	public String getRecipientNoteToExternal(Integer parameterTablePk) {
		String recipient = "";
		try {
			recipient = parameterService.findParameterTableDescription(parameterTablePk);
		} catch (Exception ex) {
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		return recipient;
	}
	
	/*public String getBody(Integer parameterTablePk, String blockType){
		NoteRegistrationDGravamenTO registration = new NoteRegistrationDGravamenTO();
		reportServiceBean.getRegistrationDGravamen(blockType);
		String body="";
		switch (parameterTablePk) {
		case 20000:
				body = BODY_DOC_1 + registration.getIdBlockRequestPk() + BODY_DOC_1_1 + registration.getFullName() 
				+ BODY_DOC_1_2 + registration.getBlockNumberDate() + BODY_DOC_1_3 + registration.getRegistryUser()
				+ BODY_DOC_1_4 + " " + BODY_DOC_1_5 + BODY_DOC_FAREWELL_1;
				
			break;
		case 21000:
				body = BODY_DOC_2 + registration.getBlockNumber() + BODY_DOC_2_1 + registration.getRegistryUser()
				+ ":" + " " + BODY_DOC_2_2 + BODY_DOC_FAREWELL_2;
			break;
		}
		return body;		
	}*/
}
