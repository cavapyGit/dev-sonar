package com.pradera.report.generation.jasper;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.inject.Alternative;

import net.sf.jasperreports.engine.DefaultJasperReportsContext;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRPropertiesUtil;
import net.sf.jasperreports.engine.JRRewindableDataSource;
import net.sf.jasperreports.engine.data.JRXmlDataSource;
import net.sf.jasperreports.engine.query.JRXPathQueryExecuterFactory;

import org.pentaho.reporting.engine.classic.core.DataFactory;
import org.pentaho.reporting.engine.classic.core.MasterReport;
import org.pentaho.reporting.engine.classic.extensions.datasources.xpath.XPathDataFactory;

import com.pradera.integration.common.validation.Validations;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.model.report.type.ReportType;
import com.pradera.report.generation.pentaho.ReportImplementation;
import com.pradera.report.generation.service.ReportUtilServiceBean;

@Alternative
@ReportImplementation(type=ReportType.BEAN)
public class BeanReportGeneratorJasper extends ReportGeneratorJasper {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8398931380116074852L;
	
	@EJB
	ReportUtilServiceBean reportUtilService;

	public BeanReportGeneratorJasper() {
		setReportType(ReportType.BEAN);
	}

	@Override
	public JRDataSource getDataSource() {
		JRRewindableDataSource jasperDS = null;
		 try {
		String xmlTemp = reportUtilService.getXmlDataSourceReport(getReportLogger());
		jasperDS = new JRXmlDataSource(xmlTemp,getXmlDataSource());
		//for subreports
		jasperDS.moveFirst();
		 } catch(IOException iox) {
			 iox.printStackTrace();
			 throw new RuntimeException(iox);
		 } catch (JRException jex) {
			 jex.printStackTrace();
			 throw new RuntimeException(jex);
		}
		return jasperDS;
	}
	
	@Override
	public Map<String, Object> getReportParameters() {
		Map<String, Object> parameter = super.getReportParameters();
		//parameters for generation from xml source
//		parameter.put(JRXPathQueryExecuterFactory.XML_LOCALE, Locale.getDefault());
		parameter.put(JRXPathQueryExecuterFactory.XML_DATE_PATTERN, "dd/MM/yyyy");
		parameter.put(JRXPathQueryExecuterFactory.XML_NUMBER_PATTERN, "#,##0.##");
		parameter.put(JRXPathQueryExecuterFactory.XML_LOCALE, Locale.ENGLISH);     
		parameter.put(JRParameter.REPORT_DATA_SOURCE, getDataSource());
		//using jaxen for performance parsing xml
		DefaultJasperReportsContext context = DefaultJasperReportsContext.getInstance();
		JRPropertiesUtil.getInstance(context).setProperty("net.sf.jasperreports.xpath.executer.factory",
		    "net.sf.jasperreports.engine.util.xml.JaxenXPathExecuterFactory");
		
		return parameter;
	}

	@Override
	public void releaseResources() {
			String tempXml = null;
			try{
			tempXml = reportUtilService.getXmlDataSourceReport(getReportLogger());
				if(tempXml != null) {
					Files.deleteIfExists(Paths.get(tempXml));
				}
			} catch(IOException iox){
				log.error("can't delete file "+tempXml);
				log.error(iox.getMessage());
			}
	}

	@Override
	public void generateReportsByFile(byte[] arrayByte) {

		MasterReport report=null;
		DataFactory dataFactory=null;
		try{

		    
		    /**REPORT ACCOUNTING**/
		    Map<ReportFormatType,byte[]> files = new HashMap<ReportFormatType, byte[]>();
		    files.put(ReportFormatType.XLS, arrayByte);


		    
		    //save files
		    reportUtilService.saveReportsGeneratesTx(getReportLogger(),files);
		}catch(Exception ex){
			log.error(ex.getMessage());
			throw new RuntimeException(ex);
			//change status to failed
//			reportUtilService.updateStateReportLoggerTx(getReportLogger(), ReportLoggerStateType.FAILED.getCode(), ex.getMessage());
		} finally {
			if (dataFactory != null && dataFactory instanceof XPathDataFactory){
				XPathDataFactory xpathFactory =(XPathDataFactory) dataFactory;
				String tempXml = xpathFactory.getXqueryDataFile();
				if(tempXml != null) {
					try {
					Files.deleteIfExists(Paths.get(tempXml));
					} catch(IOException iox) {
						log.error("can't delete file "+tempXml);
						log.error(iox.getMessage());
 					}
				}
			}
		}
	
		
	}
	
	@Override
	public void generateReportsByListFile(List<byte[]> listArrayByte) {
		
		MasterReport report=null;
		DataFactory dataFactory=null;
		try{
			if(Validations.validateListIsNotNullAndNotEmpty(listArrayByte)){

				 /**REPORT ACCOUNTING**/
			    for (byte[] arrayByte : listArrayByte) {
			    	Map<ReportFormatType,byte[]> files = new HashMap<ReportFormatType, byte[]>();
				    files.put(ReportFormatType.XLS, arrayByte);


				    
				    //save files
				    reportUtilService.saveReportsGeneratesTx(getReportLogger(),files);
				}
			}
			
		   
		    
		}catch(Exception ex){
			log.error(ex.getMessage());
			throw new RuntimeException(ex);
			//change status to failed
//			reportUtilService.updateStateReportLoggerTx(getReportLogger(), ReportLoggerStateType.FAILED.getCode(), ex.getMessage());
		} finally {
			if (dataFactory != null && dataFactory instanceof XPathDataFactory){
				XPathDataFactory xpathFactory =(XPathDataFactory) dataFactory;
				String tempXml = xpathFactory.getXqueryDataFile();
				if(tempXml != null) {
					try {
					Files.deleteIfExists(Paths.get(tempXml));
					} catch(IOException iox) {
						log.error("can't delete file "+tempXml);
						log.error(iox.getMessage());
 					}
				}
			}
		}
	
		
	}

}
