package com.pradera.report.generation.executor.billing.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.EconomicActivityType;
import com.pradera.model.billing.BillingEconomicActivity;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.type.BaseCollectionType;
import com.pradera.model.billing.type.EntityCollectionType;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.report.generation.executor.billing.to.CollectionRecordTo;
import com.pradera.report.generation.executor.billing.to.CommissionsAnnotationAccountTo;
import com.pradera.report.generation.executor.billing.to.ConsolidatedCollectionByEntityTO;
// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class BillingServiceReportServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/12/2013
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class BillingServiceReportServiceBean extends CrudDaoServiceBean {

	/**
	 * Instantiates a new billing service report service bean.
	 */
	public BillingServiceReportServiceBean(){
		
	}
	
	/**
	 * issue 1097
	 * query para construir el reporte de CONSOLIDADO COBROS POR ENTIDAD en formato Excel con apache POI
	 * @param consCollectionByEntityTO
	 * @return
	 */
	public String getQueryConsCollectionByEntity(ConsolidatedCollectionByEntityTO consCollectionByEntityTO){
		StringBuilder sd=new StringBuilder();
		
		sd.append(" SELECT                                                                                                                      ");
		sd.append("        BS.ENTITY_COLLECTION ,                                                                                               ");
		sd.append(" 	     CASE                                                                                                               ");
		sd.append(" 	       WHEN BS.ENTITY_COLLECTION=1406 THEN NVL(CR.ID_ISSUER_FK ,' ')                                                    ");
		sd.append(" 	       WHEN BS.ENTITY_COLLECTION=1407 THEN CR.ID_PARTICIPANT_FK || ' '                                                  ");
		sd.append(" 	       WHEN BS.ENTITY_COLLECTION=1408 THEN CR.ID_HOLDER_FK || ' '                                                       ");
		sd.append(" 	       WHEN BS.ENTITY_COLLECTION=1530 THEN nvl(CR.OTHER_ENTITY_DOCUMENT,' ')                                            ");
		sd.append("                                                                                                                             ");
		sd.append("                                                                                                                             ");
		sd.append(" 	       END AS CODE_ENTITY_COLLECTION,                                                                                   ");
		sd.append(" 	CASE                                                                                                                    ");
		sd.append("       WHEN BS.ENTITY_COLLECTION=1406		THEN  NVL(ISS.MNEMONIC,' ')                                                     ");
		sd.append("       WHEN BS.ENTITY_COLLECTION=1407 	THEN NVL(PAR.MNEMONIC ,' ')                                                         ");
		sd.append("     	WHEN BS.ENTITY_COLLECTION=1408		 THEN  NVL(HOL.MNEMONIC ,' ')                                                   ");
		sd.append("       WHEN BS.ENTITY_COLLECTION=1530		 THEN NVL(CR.OTHER_ENTITY_DOCUMENT, ' ')                                        ");
		sd.append("       END AS   MNEMONIC ,                                                                                                   ");
		sd.append("       CASE                                                                                                                  ");
		sd.append(" 	      WHEN bs.ENTITY_COLLECTION=1406 THEN ISS.BUSINESS_NAME                                                             ");
		sd.append(" 	     WHEN bs.ENTITY_COLLECTION=1407 THEN  PAR.DESCRIPTION                                                               ");
		sd.append(" 	     WHEN bs.ENTITY_COLLECTION=1408 THEN HOL.full_name                                                                  ");
		sd.append(" 	     WHEN BS.ENTITY_COLLECTION=1530 THEN nvl(CR.OTHER_ENTITY_DESCRIPTION,' ')                                           ");
		sd.append("                                                                                                                             ");
		sd.append(" 	      END AS NAME_ENTITY_COLLECTION,                                                                                    ");
		sd.append("        trunc(CR.CALCULATION_DATE) CALCULATION_DATE ,                                                                        ");
		sd.append("        TO_CHAR(CR.CALCULATION_DATE,'DD/MON/YY','NLS_DATE_LANGUAGE = SPANISH') CALCULATION_DATE_STRING,                      ");
		sd.append("        nvl(CR.RECORD_STATE,1518) RECORD_STATE,                                                                              ");
		sd.append("                                                                                                                             ");
		sd.append("        (select NVL(PT.PARAMETER_NAME,'ERROR') from PARAMETER_TABLE PT                                                       ");
		sd.append(" where PT.MASTER_TABLE_FK=402 AND PT.PARAMETER_TABLE_PK=CR.RECORD_STATE) RECORD_STATE_NAME,                                  ");
		sd.append("        BS.REFERENCE_RATE,                                                                                                   ");
		sd.append("        TO_NUMBER(BS.SERVICE_CODE,9999) SERVICE_CODE,                                                                        ");
		sd.append(" 	     NVL(CR.TAX_APPLIED,0) TAX_APPLIED,                                                                                 ");
		sd.append(" 	     NVL(CR.GROSS_AMOUNT,0) GROSS_AMOUNT,                                                                               ");
		sd.append(" 	     NVL(CR.COLLECTION_AMOUNT,0) COLLECTION_AMOUNT,                                                                     ");
		sd.append("        NVL(CR.COLLECTION_AMOUNT_BILLED,0) COLLECTION_AMOUNT_BILLED,                                                         ");
		sd.append("        CR.EXCHANGE_RATE_CURRENT,                                                                                            ");
		sd.append(" 	     BS.SERVICE_TYPE,                                                                                                   ");
		sd.append(" 	     BS.COLLECTION_PERIOD,                                                                                              ");
		sd.append(" 	  BS.CURRENCY_CALCULATION,                                                                                              ");
		sd.append(" 	(SELECT PT.DESCRIPTION FROM PARAMETER_TABLE PT                                                                          ");
		sd.append(" 	WHERE PT.PARAMETER_TABLE_PK=BS.CURRENCY_CALCULATION) AS CURRENCY_DESCRIPTION                                            ");
		// esta es la columna requerida en el issue 1097
		sd.append("		,(SELECT SUM(CRD.NEGOTIATED_AMOUNT)  from collection_record CRSQ                                        ");
		sd.append("		 INNER JOIN COLLECTION_RECORD_DETAIL CRD ON CRD.ID_COLLECTION_RECORD_FK= CRSQ.ID_COLLECTION_RECORD_PK   ");
		sd.append("		 WHERE 0=0                                                                                              ");
		sd.append("		 AND trunc(CRSQ.CALCULATION_DATE)                                                                       ");
		sd.append("			BETWEEN to_date( '"+consCollectionByEntityTO.getInitialDate()+"' ,'DD/MM/YYYY') and to_date( '"+consCollectionByEntityTO.getFinalDate()+"' ,'DD/MM/YYYY')                 ");
		sd.append("			AND CRD.ID_COLLECTION_RECORD_FK = CR.ID_COLLECTION_RECORD_PK                                        ");
		sd.append("			) NEGOTIATED_AMOUNT																					");
		
		sd.append(" 	     from collection_record CR                                                                                          ");
		sd.append(" 	     inner join processed_service ps  ON (PS.ID_PROCESSED_SERVICE_PK= CR.ID_PROCESSED_SERVICE_FK)                       ");
		sd.append(" 	     inner join billing_service bs on  ( ps.ID_BILLING_SERVICE_FK = bs.ID_BILLING_SERVICE_PK)                           ");
		sd.append(" 	     inner join parameter_table PT  ON (BS.ENTITY_COLLECTION= PT.parameter_table_pk)                                    ");
		sd.append(" 	     left JOIN ISSUER ISS ON(ISS.ID_ISSUER_PK=cr.ID_ISSUER_FK )                                                         ");
		sd.append(" 	     left JOIN PARTICIPANT PAR ON (PAR.ID_PARTICIPANT_PK=CR.ID_PARTICIPANT_FK)                                          ");
		sd.append(" 	     left JOIN HOLDER HOL ON (HOL.ID_HOLDER_PK=CR.ID_HOLDER_FK)                                                         ");
		sd.append(" 	where  0=0                                                                                                               ");
		
		if(Validations.validateIsNotNullAndNotEmpty(consCollectionByEntityTO.getEntityCollection()))
			sd.append(" 	    ( and BS.ENTITY_COLLECTION= :entity_collection )                                          ");
		
		if(Validations.validateIsNotNullAndNotEmpty(consCollectionByEntityTO.getCollectionState()))
			sd.append("        AND ( CR.RECORD_STATE= :collection_state)                                               ");
		
		if(Validations.validateIsNotNullAndNotEmpty(consCollectionByEntityTO.getIdIssuer()))
			sd.append("                         and (BS.ENTITY_COLLECTION=1406 AND CR.ID_ISSUER_FK= :issuer )                                        ");
		
		if(Validations.validateIsNotNullAndNotEmpty(consCollectionByEntityTO.getIdParticipant()))
			sd.append("                         and (BS.ENTITY_COLLECTION=1407 AND CR.ID_PARTICIPANT_FK= :participant )                              ");
		
		if(Validations.validateIsNotNullAndNotEmpty(consCollectionByEntityTO.getIdHolder()))
			sd.append("                        and (BS.ENTITY_COLLECTION=1408 AND CR.ID_HOLDER_FK= :holder )                                        ");

		if(Validations.validateIsNotNullAndNotEmpty(consCollectionByEntityTO.getOtherNumber())){
			sd.append("                         and (BS.ENTITY_COLLECTION=1530 AND (CR.OTHER_ENTITY_DOCUMENT= :other_number                       ");
			
			if(Validations.validateIsNotNullAndNotEmpty(consCollectionByEntityTO.getOtherDescription()))
				sd.append("                                                           OR (CR.OTHER_ENTITY_DESCRIPTION= :other_description ) ");
			
			sd.append("                                                           ))               ");
		}
			
		if(Validations.validateIsNotNullAndNotEmpty(consCollectionByEntityTO.getAfp()))
			sd.append("                         OR (BS.ENTITY_COLLECTION=1726 AND BS.ENTITY_COLLECTION= :afp )                                      ");
		
		sd.append("        AND trunc(CR.CALCULATION_DATE) BETWEEN to_date( '"+consCollectionByEntityTO.getInitialDate()+"' ,'DD/MM/YYYY') and to_date( '"+consCollectionByEntityTO.getFinalDate()+"' ,'DD/MM/YYYY')   ");
		
		if(Validations.validateIsNotNullAndNotEmpty(consCollectionByEntityTO.getServiceCode()))
			sd.append(" AND ( bs.SERVICE_CODE = :p_service_code )                                                        ");
		
		sd.append("        order by                                                                                                             ");
		sd.append(" 	     bs.ENTITY_COLLECTION,                                                                                              ");
		sd.append(" 	     CASE                                                                                                               ");
		sd.append(" 	      WHEN bs.ENTITY_COLLECTION=1406 THEN NVL(cr.ID_ISSUER_FK , ' ')                                                    ");
		sd.append(" 	      WHEN bs.ENTITY_COLLECTION=1407 THEN  CR.ID_PARTICIPANT_FK || ' '                                                  ");
		sd.append(" 	      WHEN bs.ENTITY_COLLECTION=1408 THEN CR.ID_HOLDER_FK || ' '                                                        ");
		sd.append(" 	      WHEN BS.ENTITY_COLLECTION=1530 THEN nvl(CR.OTHER_ENTITY_DOCUMENT,' ')                                             ");
		sd.append("                                                                                                                             ");
		sd.append("                                                                                                                             ");
		sd.append(" 	     END,                                                                                                               ");
		sd.append("        trunc(CR.CALCULATION_DATE),                                                                                          ");
		sd.append("        CR.RECORD_STATE,                                                                                                     ");
		sd.append("        BS.REFERENCE_RATE,                                                                                                   ");
		sd.append("        TO_NUMBER(BS.SERVICE_CODE,9999)																						");
		
		String strQueryFormatted = sd.toString();
		
		// reemplazando los parametros
		if(Validations.validateIsNotNullAndNotEmpty(consCollectionByEntityTO.getEntityCollection()))
			strQueryFormatted = strQueryFormatted.replace(":entity_collection", consCollectionByEntityTO.getEntityCollection());
		
		if(Validations.validateIsNotNullAndNotEmpty(consCollectionByEntityTO.getCollectionState()))
			strQueryFormatted = strQueryFormatted.replace(":collection_state", consCollectionByEntityTO.getCollectionState());
		
		if(Validations.validateIsNotNullAndNotEmpty(consCollectionByEntityTO.getIdIssuer()))
			strQueryFormatted = strQueryFormatted.replace(":issuer", "'"+consCollectionByEntityTO.getIdIssuer()+"'");
		
		if(Validations.validateIsNotNullAndNotEmpty(consCollectionByEntityTO.getIdParticipant()))
			strQueryFormatted = strQueryFormatted.replace(":participant", consCollectionByEntityTO.getIdParticipant());
		
		if(Validations.validateIsNotNullAndNotEmpty(consCollectionByEntityTO.getIdHolder()))
			strQueryFormatted = strQueryFormatted.replace(":holder", consCollectionByEntityTO.getIdHolder());
		
		if(Validations.validateIsNotNullAndNotEmpty(consCollectionByEntityTO.getOtherNumber())){
			strQueryFormatted = strQueryFormatted.replace(":other_number", "'"+consCollectionByEntityTO.getOtherNumber()+"'");
			if(Validations.validateIsNotNullAndNotEmpty(consCollectionByEntityTO.getOtherDescription()))
				strQueryFormatted = strQueryFormatted.replace(":other_description", "'"+consCollectionByEntityTO.getOtherDescription()+"'");
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(consCollectionByEntityTO.getAfp()))
			strQueryFormatted = strQueryFormatted.replace(":afp", consCollectionByEntityTO.getAfp());
		
		if(Validations.validateIsNotNullAndNotEmpty(consCollectionByEntityTO.getServiceCode()))
			strQueryFormatted = strQueryFormatted.replace(":p_service_code", consCollectionByEntityTO.getServiceCode());
			
    	return strQueryFormatted;
	}
	
	/** metodo para obtener el tipo de cambio del dolar a la venta
	 * 
	 * @param dateParameter
	 * @return null si no se encunetran resultados */
	public DailyExchangeRates getExchangeRateForSale(Date dateParameter) {
		DailyExchangeRates dailyExchangeRates = null;
		List<DailyExchangeRates> lst = new ArrayList<>();
		StringBuilder sd = new StringBuilder();
		sd.append(" select der ");
		sd.append(" from DailyExchangeRates der ");
		sd.append(" where 0 = 0 ");
		sd.append(" and der.dateRate = :dateParameter ");
		// moneda USD
		sd.append(" and der.idCurrency = 430 ");
		// sujeto al BCB
		sd.append(" and der.idSourceinformation = 212 ");
		sd.append(" order by der.iddailyExchangepk desc ");
		Query query = em.createQuery(sd.toString());
		query.setParameter("dateParameter", dateParameter);
		lst = query.getResultList();
		if (!lst.isEmpty()) {
			dailyExchangeRates = lst.get(0);
		}
		return dailyExchangeRates;
	}

	/** metodo para obtener un participante especifico
	 * 
	 * @param idParticipant
	 * @return null si no se encuntra al participante */
	public Participant getParticipantToReport(Long idParticipant) {
		Participant participant = null;
		participant = em.find(Participant.class, idParticipant);
		return participant;
	}

	/** metodo para obtener la descripcion de un servico
	 * 
	 * @param serviceCode
	 * @return null si no se encuentran resultados */
	public BillingService getServiceDescription(String serviceCode) {
		BillingService bs = null;
		List<BillingService> lst = new ArrayList<>();
		StringBuilder sd = new StringBuilder();
		// select bs.REFERENCE_RATE, bs.SERVICE_NAME from BILLING_SERVICE bs where 0=0
		sd.append(" select bs ");
		sd.append(" from BillingService bs ");
		sd.append(" where 0 = 0 ");
		sd.append(" and bs.serviceCode = :serviceCode ");
		Query query = em.createQuery(sd.toString());
		query.setParameter("serviceCode", serviceCode);
		lst = query.getResultList();
		if (!lst.isEmpty()) {
			bs = lst.get(0);
		}
		return bs;
	}
	
	public List<Object[]> getQueryListByClass(String strQuery) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(strQuery);
		Query query = em.createNativeQuery(sbQuery.toString());
		return query.getResultList();
	}
	
	/**
	 * Gets the processed service to consolidate.
	 *
	 * @param filterSearch the filter search
	 * @param numBillingService the num billing service
	 * @param filter the filter
	 * @return the processed service to consolidate
	 */
	public List<Object[]> getProcessedServiceToConsolidate(Integer filterSearch,Integer numBillingService,CollectionRecordTo filter){
		StringBuilder sbQuery = new StringBuilder();
	    sbQuery.append("  select");
	    
	    sbQuery.append("  (CASE WHEN ps.CALCULATION_DATE IS NOT NULL THEN to_char(ps.CALCULATION_DATE,'dd/mm/yyyy')  "); 
	    sbQuery.append("   WHEN ps.CALCULATION_DATE IS  NULL THEN ' ' END ) AS OPERATION_DATE, "); 
	    sbQuery.append(" count(*), ");
	    sbQuery.append(" NVL(CR.TAX_APPLIED,0), ");
	    sbQuery.append(" NVL(CR.GROSS_AMOUNT,0), ");
	    sbQuery.append(" NVL(CR.COLLECTION_AMOUNT,0),  ");
	    sbQuery.append(" BS.ENTITY_COLLECTION AS ENTITY_TYPE, ");
	    sbQuery.append(" CASE   ");
	    sbQuery.append("  WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN CR.ID_ISSUER_FK || ' '  ");
	    sbQuery.append("   WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN  CR.ID_PARTICIPANT_FK || ' ' ");
	    sbQuery.append("  WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN CR.ID_HOLDER_FK || ' '  ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN NVL(CR.OTHER_ENTITY_DOCUMENT,' ')  ");
	    
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.STOCK_EXCHANGE.getCode()+" THEN NVL(to_char(BS.ENTITY_COLLECTION), 'S/N') ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.CENTRAL_BANK.getCode()+" THEN NVL(to_char(BS.ENTITY_COLLECTION), 'S/N') ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.HACIENDA.getCode()+" THEN NVL(to_char(BS.ENTITY_COLLECTION), 'S/N') ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.AFP.getCode()+" THEN NVL(to_char(BS.ENTITY_COLLECTION), 'S/N') ");//afp
	
	    sbQuery.append("   END AS CODE_ENTITY_COLLECTION, ");
	    sbQuery.append(" CASE   ");
	    sbQuery.append("  WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN  NVL(ISS.BUSINESS_NAME,' ') ");
	    sbQuery.append("  WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN  NVL(PAR.DESCRIPTION,' ')  ");
	    sbQuery.append("  WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN  NVL(HOL.full_name,' ')  ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN  NVL(CR.OTHER_ENTITY_DESCRIPTION,' ')       ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.STOCK_EXCHANGE.getCode()+" THEN NVL(   to_char(pt.DESCRIPTION ),  'S/N') ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.CENTRAL_BANK.getCode()+" THEN NVL(   to_char(pt.DESCRIPTION ),  'S/N') ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.HACIENDA.getCode()+" THEN NVL(   to_char(pt.DESCRIPTION ),  'S/N')   ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.AFP.getCode()+" THEN NVL(   to_char(pt.DESCRIPTION ),  'S/N')  ");
	    
	    sbQuery.append("  END AS NAME_ENTITY_COLLECTION, ");
	    sbQuery.append(" BS.SERVICE_CODE,BS.SERVICE_NAME, ");
	    sbQuery.append(" BS.SERVICE_TYPE, ");
	    sbQuery.append(" BS.CALCULATION_PERIOD ");
	    
	    sbQuery.append(" from processed_service ps  ");
	    sbQuery.append(" inner join billing_service bs on  ( ps.ID_BILLING_SERVICE_FK = bs.ID_BILLING_SERVICE_PK) ");
	    sbQuery.append(" inner  JOIN COLLECTION_RECORD CR ON (PS.ID_PROCESSED_SERVICE_PK= CR.ID_PROCESSED_SERVICE_FK) ");
	    sbQuery.append(" inner join parameter_table PT  ON (BS.ENTITY_COLLECTION= PT.parameter_table_pk) ");
	    sbQuery.append(" left JOIN ISSUER ISS ON(ISS.ID_ISSUER_PK=cr.ID_ISSUER_FK ) "); 
	    sbQuery.append(" left JOIN PARTICIPANT PAR ON (PAR.ID_PARTICIPANT_PK=CR.ID_PARTICIPANT_FK) ");
	    sbQuery.append(" left JOIN HOLDER HOL ON (HOL.ID_HOLDER_PK=CR.ID_HOLDER_FK) ");
	    
	    sbQuery.append(" where 1=1  ");
	    if(Validations.validateIsNotNullAndPositive(filterSearch)){
	    	if(filterSearch.compareTo(1)==0){
	    		if(Validations.validateIsNotNull(numBillingService)){
	    			if(numBillingService.compareTo(1)==0){
	    				if(Validations.validateIsNotNullAndNotEmpty(filter.getProcessedServiceTo().getBillingServiceTo())){
	    	    	    	if(Validations.validateIsNotNullAndNotEmpty(filter.getProcessedServiceTo().getBillingServiceTo().getServiceCode())){
	    	    	    		sbQuery.append(" and bs.service_code=:billingServiceCodePrm ");
	    	    	    	}
	    	    	    }
	    			}
	    		}
	    		
	    	}
	    	else if(filterSearch.compareTo(2)==0){
	    		if(Validations.validateIsNotNullAndNotEmpty(filter.getProcessedServiceTo())){
	    		if(Validations.validateIsNotNullAndNotEmpty(filter.getProcessedServiceTo().getBillingServiceTo())){
	    	    	if(Validations.validateIsNotNull(filter.getProcessedServiceTo().getBillingServiceTo().getEntityCollection())){
	    	    		sbQuery.append(" and bs.ENTITY_COLLECTION=:entityCollectionPrm ");
	    	    	}
	    	    }
	    		}
	    	}
	    }
	    if(Validations.validateIsNotNull(filter.getInitialDate())&&Validations.validateIsNotNull(filter.getFinalDate())){
		    sbQuery.append(" and ps.CALCULATION_DATE between :initialDatePrm and :finalDatePrm ");
	    }
	    sbQuery.append(" group by  ");
	    sbQuery.append("  (CASE WHEN ps.CALCULATION_DATE IS NOT NULL THEN to_char(ps.CALCULATION_DATE,'dd/mm/yyyy')  "); 
	    sbQuery.append("   WHEN ps.CALCULATION_DATE IS  NULL THEN ' ' END ), "); 
	    sbQuery.append("  NVL(CR.TAX_APPLIED,0), ");
	    sbQuery.append("  NVL(CR.GROSS_AMOUNT,0),  ");
	    sbQuery.append(" NVL(CR.COLLECTION_AMOUNT,0),  ");
	    sbQuery.append(" BS.ENTITY_COLLECTION, ");
	    sbQuery.append("CASE   ");
	    sbQuery.append("   WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN CR.ID_ISSUER_FK || ' ' ");
	    sbQuery.append("   WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN  CR.ID_PARTICIPANT_FK || ' ' ");
	    sbQuery.append("   WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN CR.ID_HOLDER_FK || ' ' ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN nvl(CR.OTHER_ENTITY_DOCUMENT,' ')   ");	    
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.STOCK_EXCHANGE.getCode()+" THEN NVL(to_char(BS.ENTITY_COLLECTION), 'S/N') ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.CENTRAL_BANK.getCode()+" THEN NVL(to_char(BS.ENTITY_COLLECTION), 'S/N') ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.HACIENDA.getCode()+" THEN NVL(to_char(BS.ENTITY_COLLECTION), 'S/N') ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.AFP.getCode()+" THEN NVL(to_char(BS.ENTITY_COLLECTION), 'S/N') ");
	    sbQuery.append("   END, ");
	    sbQuery.append(" CASE   ");
	    sbQuery.append("  WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN  NVL(ISS.BUSINESS_NAME,' ') ");
	    sbQuery.append("  WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN  NVL(PAR.DESCRIPTION,' ')  ");
	    sbQuery.append("  WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN  NVL(HOL.full_name,' ')  ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN nvl(CR.OTHER_ENTITY_DESCRIPTION,' ')  ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.STOCK_EXCHANGE.getCode()+" THEN NVL( to_char(pt.DESCRIPTION ),  'S/N') ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.CENTRAL_BANK.getCode()+" THEN NVL( to_char(pt.DESCRIPTION ),  'S/N') ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.HACIENDA.getCode()+" THEN NVL( to_char(pt.DESCRIPTION ),  'S/N') ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.AFP.getCode()+" THEN NVL( to_char(pt.DESCRIPTION ),  'S/N') ");//afp
	    
	    sbQuery.append("   END, ");
	    sbQuery.append(" BS.SERVICE_CODE, ");
	    sbQuery.append(" BS.SERVICE_NAME, ");
	    sbQuery.append(" BS.SERVICE_TYPE,  ");
	    sbQuery.append(" BS.CALCULATION_PERIOD ");
	    
	    sbQuery.append(" order by ");
	    sbQuery.append("  (CASE WHEN ps.CALCULATION_DATE IS NOT NULL THEN to_char(ps.CALCULATION_DATE,'dd/mm/yyyy')  "); 
	    sbQuery.append("   WHEN ps.CALCULATION_DATE IS  NULL THEN ' ' END ), "); 
	    sbQuery.append(" TO_NUMBER(BS.SERVICE_CODE,9999) ,  ");
	    sbQuery.append(" BS.ENTITY_COLLECTION, ");
	    sbQuery.append(" CASE ");
	    sbQuery.append("  WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN cr.ID_ISSUER_FK || ' '  ");
	    sbQuery.append("  WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN  CR.ID_PARTICIPANT_FK || ' '  ");
	    sbQuery.append("  WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN CR.ID_HOLDER_FK || ' '  ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN nvl(CR.OTHER_ENTITY_DOCUMENT,' ')    ");	    
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.STOCK_EXCHANGE.getCode()+" THEN NVL(to_char(BS.ENTITY_COLLECTION), 'S/N') ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.CENTRAL_BANK.getCode()+" THEN NVL(to_char(BS.ENTITY_COLLECTION), 'S/N') ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.HACIENDA.getCode()+" THEN NVL(to_char(BS.ENTITY_COLLECTION), 'S/N') ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.AFP.getCode()+" THEN NVL(to_char(BS.ENTITY_COLLECTION), 'S/N') ");//afp
	    sbQuery.append(" END ");
	    
	    Query query = em.createNativeQuery(sbQuery.toString());
	    	    
	    if(Validations.validateIsNotNullAndPositive(filterSearch)){
	    	if(filterSearch.compareTo(1)==0){
	    		if(Validations.validateIsNotNull(numBillingService)){
	    			if(numBillingService.compareTo(1)==0){
	    				if(Validations.validateIsNotNullAndNotEmpty(filter.getProcessedServiceTo().getBillingServiceTo())){
	    	    	    	if(Validations.validateIsNotNullAndNotEmpty(filter.getProcessedServiceTo().getBillingServiceTo().getServiceCode())){
	    	    	    		query.setParameter("billingServiceCodePrm", filter.getProcessedServiceTo().getBillingServiceTo().getServiceCode());
	    	    	    	}
	    	    	    }
	    			}
	    		}
	    		
	    	}
	    	else if(filterSearch.compareTo(2)==0){
	    		if(Validations.validateIsNotNullAndNotEmpty(filter.getProcessedServiceTo())){
	    		if(Validations.validateIsNotNullAndNotEmpty(filter.getProcessedServiceTo().getBillingServiceTo())){
	    	    	if(Validations.validateIsNotNull(filter.getProcessedServiceTo().getBillingServiceTo().getEntityCollection())){
	    	    		query.setParameter("entityCollectionPrm", filter.getProcessedServiceTo().getBillingServiceTo().getEntityCollection());
	    	    	}
	    	    }
	    		}
	    	}
	    }
	    
	    
	    if(Validations.validateIsNotNull(filter.getInitialDate())&&Validations.validateIsNotNull(filter.getFinalDate())){
		    query.setParameter("initialDatePrm", filter.getInitialDate());
		    query.setParameter("finalDatePrm", filter.getFinalDate());
		    }
	    
	    return query.getResultList();
	}
	
	
	
	
	/**
	 *  Return List Object, get Collection By Entity.
	 *
	 * @param filter the filter
	 * @return the consolidated by entity
	 */
	public List<Object[]> getConsolidatedByEntity(CollectionRecordTo filter){
		StringBuilder sbQuery = new StringBuilder();
	    sbQuery.append("  select");
	    sbQuery.append("  (CASE WHEN CR.CALCULATION_DATE IS NOT NULL THEN to_char(CR.CALCULATION_DATE,'dd/mm/yyyy')  "); 
	    sbQuery.append("   WHEN CR.CALCULATION_DATE IS  NULL THEN ' ' END ) AS CALCULATION_DATE, ");
	    sbQuery.append(" NVL(CR.TAX_APPLIED,0), ");
	    sbQuery.append(" NVL(CR.GROSS_AMOUNT,0), ");
	    sbQuery.append(" NVL(CR.COLLECTION_AMOUNT,0),  ");
	    sbQuery.append(" BS.ENTITY_COLLECTION AS ENTITY_TYPE, ");
	    sbQuery.append(" CASE   ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN NVL(CR.ID_ISSUER_FK ,' ') ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN CR.ID_PARTICIPANT_FK || ' ' ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN CR.ID_HOLDER_FK || ' ' ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN nvl(CR.OTHER_ENTITY_DOCUMENT,' ')   ");
	    
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.STOCK_EXCHANGE.getCode()+" THEN NVL(to_char(BS.ENTITY_COLLECTION), ' ') ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.CENTRAL_BANK.getCode()+" THEN NVL(to_char(BS.ENTITY_COLLECTION), ' ') ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.HACIENDA.getCode()+" THEN NVL(to_char(BS.ENTITY_COLLECTION), ' ') ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.AFP.getCode()+" THEN NVL(to_char(BS.ENTITY_COLLECTION), ' ') ");//afp
	    sbQuery.append("   END AS CODE_ENTITY_COLLECTION, ");
	    sbQuery.append(" CASE   ");
	    sbQuery.append("  WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN ISS.BUSINESS_NAME ");
	    sbQuery.append(" WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN  PAR.DESCRIPTION  ");
	    sbQuery.append(" WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN HOL.full_name  ");
	    sbQuery.append(" WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN nvl(CR.OTHER_ENTITY_DESCRIPTION,' ')       ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.STOCK_EXCHANGE.getCode()+" THEN NVL(   to_char(pt.DESCRIPTION ),  'S/N') ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.CENTRAL_BANK.getCode()+" THEN NVL(   to_char(pt.DESCRIPTION ),  'S/N') ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.HACIENDA.getCode()+" THEN NVL(   to_char(pt.DESCRIPTION ),  'S/N')   ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.AFP.getCode()+" THEN NVL(   to_char(pt.DESCRIPTION ),  'S/N')  ");//afp
	    sbQuery.append("  END AS NAME_ENTITY_COLLECTION, ");
	    
	    sbQuery.append(" BS.SERVICE_TYPE, ");
	    sbQuery.append(" BS.COLLECTION_PERIOD, ");
	    sbQuery.append(" nvl(CR.RECORD_STATE,1518)");
	    sbQuery.append(" from collection_record CR  ");
	    
	    sbQuery.append(" inner join processed_service ps  ON (PS.ID_PROCESSED_SERVICE_PK= CR.ID_PROCESSED_SERVICE_FK) ");
	    sbQuery.append(" inner join billing_service bs on  ( ps.ID_BILLING_SERVICE_FK = bs.ID_BILLING_SERVICE_PK) ");
	    sbQuery.append(" inner join parameter_table PT  ON (BS.ENTITY_COLLECTION= PT.parameter_table_pk) ");
	    sbQuery.append(" left JOIN ISSUER ISS ON(ISS.ID_ISSUER_PK=cr.ID_ISSUER_FK ) "); 
	    sbQuery.append(" left JOIN PARTICIPANT PAR ON (PAR.ID_PARTICIPANT_PK=CR.ID_PARTICIPANT_FK) ");
	    sbQuery.append(" left JOIN HOLDER HOL ON (HOL.ID_HOLDER_PK=CR.ID_HOLDER_FK) ");
	    
	    sbQuery.append(" where 1=1  ");
	    if(Validations.validateIsNotNullAndNotEmpty(filter.getRecordState())){
	    	sbQuery.append(" and CR.RECORD_STATE=:recordStatePrm  ");	    	
	    }
	    
	   
	    if(Validations.validateIsNotNullAndNotEmpty(filter.getProcessedServiceTo())){
	    	if(Validations.validateIsNotNullAndNotEmpty(filter.getProcessedServiceTo().getBillingServiceTo())){
	    	    if(Validations.validateIsNotNull(filter.getProcessedServiceTo().getBillingServiceTo().getEntityCollection())){	    	    		
	    	    		Integer entityCollection=filter.getProcessedServiceTo().getBillingServiceTo().getEntityCollection();
	    	    		sbQuery.append("  and BS.ENTITY_COLLECTION= :billingEntityCollectionPrm ");
	    	    		
	    	    		if(entityCollection.compareTo(EntityCollectionType.HOLDERS.getCode())==0){
	    	    			if(Validations.validateIsNotNull(filter.getHolderCode()))
	    	    			sbQuery.append(" and CR.ID_HOLDER_FK= :entityHolderPrm ");
	    	    		}
	    	    		else if(entityCollection.compareTo(EntityCollectionType.ISSUERS.getCode())==0){
	    	    			if(Validations.validateIsNotNull(filter.getIssuerCode()))
	    	    			sbQuery.append(" and cr.ID_ISSUER_FK= :entityIssuerPrm ");
	    	    		}
	    	    		else if(entityCollection.compareTo(EntityCollectionType.PARTICIPANTS.getCode())==0){
	    	    			if(Validations.validateIsNotNull(filter.getParticipantCode()))
	    	    			sbQuery.append(" and CR.ID_PARTICIPANT_FK= :entityParticipantPrm ");
	    	    		}
	    	    		else if(entityCollection.compareTo(EntityCollectionType.OTHERS.getCode())==0){
	    	    			if(Validations.validateIsNotNullAndNotEmpty(filter.getOtherEntityDocument()))
	    	    				sbQuery.append(" and CR.OTHER_ENTITY_DOCUMENT= :entityOtherDocumentPrm ");
	    	    			if(Validations.validateIsNotNullAndNotEmpty(filter.getOtherEntityDescription()))
	    	    				sbQuery.append(" and CR.OTHER_ENTITY_DESCRIPTION= :entityOtherNamePrm ");
	    	    		}
	    	    		
	    	    	}
	    	}
	    }
	    if(Validations.validateIsNotNull(filter.getInitialDate())&&Validations.validateIsNotNull(filter.getFinalDate())){
		    sbQuery.append(" and ps.CALCULATION_DATE between :initialDatePrm and :finalDatePrm ");
	    }
	    sbQuery.append(" group by ");
	    sbQuery.append("  (CASE WHEN CR.CALCULATION_DATE IS NOT NULL THEN to_char(CR.CALCULATION_DATE,'dd/mm/yyyy')  "); 
	    sbQuery.append("   WHEN CR.CALCULATION_DATE IS  NULL THEN ' ' END ), ");
	    sbQuery.append(" NVL(CR.TAX_APPLIED,0), ");
	    sbQuery.append(" NVL(CR.GROSS_AMOUNT,0), ");
	    sbQuery.append(" NVL(CR.COLLECTION_AMOUNT,0),  ");
	    sbQuery.append(" 	BS.ENTITY_COLLECTION, ");
	    sbQuery.append(" CASE   ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN NVL(CR.ID_ISSUER_FK ,' ') ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN CR.ID_PARTICIPANT_FK || ' ' ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN CR.ID_HOLDER_FK || ' ' ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN nvl(CR.OTHER_ENTITY_DOCUMENT,' ')   ");
	    
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.STOCK_EXCHANGE.getCode()+" THEN NVL(to_char(BS.ENTITY_COLLECTION), ' ') ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.CENTRAL_BANK.getCode()+" THEN NVL(to_char(BS.ENTITY_COLLECTION), ' ') ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.HACIENDA.getCode()+" THEN NVL(to_char(BS.ENTITY_COLLECTION), ' ') ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.AFP.getCode()+" THEN NVL(to_char(BS.ENTITY_COLLECTION), ' ') ");//afp
//	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.SUPERINTENDENCE.getCode()+" THEN NVL(to_char(BS.ENTITY_COLLECTION), ' ') ");
	    
	    sbQuery.append("   END, ");
	    sbQuery.append(" CASE   ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN ISS.BUSINESS_NAME ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN  PAR.DESCRIPTION ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN HOL.full_name ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN nvl(CR.OTHER_ENTITY_DESCRIPTION,' ')  ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.STOCK_EXCHANGE.getCode()+" THEN NVL(   to_char(pt.DESCRIPTION ),  'S/N') ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.CENTRAL_BANK.getCode()+" THEN NVL(   to_char(pt.DESCRIPTION ),  'S/N') ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.HACIENDA.getCode()+" THEN NVL(   to_char(pt.DESCRIPTION ),  'S/N')   ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.AFP.getCode()+" THEN NVL(   to_char(pt.DESCRIPTION ),  'S/N')  ");//afp
//	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.SUPERINTENDENCE.getCode()+" THEN NVL(   to_char(pt.DESCRIPTION ),  'S/N')   ");
	    sbQuery.append("   END, ");
	    sbQuery.append(" BS.SERVICE_TYPE,  ");
	    sbQuery.append(" BS.COLLECTION_PERIOD, ");
	    sbQuery.append(" CR.RECORD_STATE  ");
	    
	    sbQuery.append(" order by  ");
	    sbQuery.append(" bs.ENTITY_COLLECTION,");
	    sbQuery.append(" CASE ");
	    sbQuery.append("  WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN NVL(cr.ID_ISSUER_FK , ' ')  ");
	    sbQuery.append("  WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN  CR.ID_PARTICIPANT_FK || ' '  ");
	    sbQuery.append("  WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN CR.ID_HOLDER_FK || ' '  ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN nvl(CR.OTHER_ENTITY_DOCUMENT,' ')    ");	    
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.STOCK_EXCHANGE.getCode()+" THEN NVL(to_char(BS.ENTITY_COLLECTION), ' ') ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.CENTRAL_BANK.getCode()+" THEN NVL(to_char(BS.ENTITY_COLLECTION), ' ') ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.HACIENDA.getCode()+" THEN NVL(to_char(BS.ENTITY_COLLECTION), ' ') ");
	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.AFP.getCode()+" THEN NVL(to_char(BS.ENTITY_COLLECTION), ' ') ");//afp
//	    sbQuery.append("   WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.SUPERINTENDENCE.getCode()+" THEN NVL(to_char(BS.ENTITY_COLLECTION), ' ') ");
	    
	    sbQuery.append(" END, ");
	    sbQuery.append("  (CASE WHEN CR.CALCULATION_DATE IS NOT NULL THEN to_char(CR.CALCULATION_DATE,'dd/mm/yyyy')  "); 
	    sbQuery.append("   WHEN CR.CALCULATION_DATE IS  NULL THEN ' ' END ), ");
	    sbQuery.append(" CR.RECORD_STATE ");
	    
	    Query query = em.createNativeQuery(sbQuery.toString());
	    
	    if(Validations.validateIsNotNullAndNotEmpty(filter.getRecordState())){
	    	query.setParameter("recordStatePrm", filter.getRecordState());	    	
	    }
	    if(Validations.validateIsNotNullAndNotEmpty(filter.getProcessedServiceTo())){
	    	if(Validations.validateIsNotNullAndNotEmpty(filter.getProcessedServiceTo().getBillingServiceTo())){
	    	    if(Validations.validateIsNotNull(filter.getProcessedServiceTo().getBillingServiceTo().getEntityCollection())){
	    	    	Integer entityCollection=filter.getProcessedServiceTo().getBillingServiceTo().getEntityCollection();
		    		query.setParameter("billingEntityCollectionPrm", entityCollection); 
		    		if(entityCollection.compareTo(EntityCollectionType.HOLDERS.getCode())==0){
		    			if(Validations.validateIsNotNullAndPositive(filter.getHolderCode()))
		    			query.setParameter("entityHolderPrm", filter.getHolderCode());
		    		}
		    		else if(entityCollection.compareTo(EntityCollectionType.ISSUERS.getCode())==0){
		    			if(Validations.validateIsNotNullAndNotEmpty(filter.getIssuerCode()))
		    			query.setParameter("entityIssuerPrm", filter.getIssuerCode());
		    		}
		    		else if(entityCollection.compareTo(EntityCollectionType.PARTICIPANTS.getCode())==0){
		    			if(Validations.validateIsNotNullAndPositive(filter.getParticipantCode()))
		    			query.setParameter("entityParticipantPrm", filter.getParticipantCode());
		    		}
		    		else if(entityCollection.compareTo(EntityCollectionType.OTHERS.getCode())==0){
		    			if(Validations.validateIsNotNullAndNotEmpty(filter.getOtherEntityDocument()))
			    			query.setParameter("entityOtherDocumentPrm", filter.getOtherEntityDocument());
	    	    		if(Validations.validateIsNotNullAndNotEmpty(filter.getOtherEntityDescription()))
	    	    			query.setParameter("entityOtherNamePrm", filter.getOtherEntityDescription());
		    		}
	    	    	}
	    	}
	    }
	    if(Validations.validateIsNotNull(filter.getInitialDate())&&Validations.validateIsNotNull(filter.getFinalDate())){
		    query.setParameter("initialDatePrm", filter.getInitialDate());
		    query.setParameter("finalDatePrm", filter.getFinalDate());
		    }
	    
	    return query.getResultList();
	}
	
	/**
	 *  Return List Object, get Report Billing Definitive.
	 *
	 * @param processBillingCalculationPk the process billing calculation pk
	 * @return the consolidated billing definitive report
	 */
	public List<Object[]> getConsolidatedBillingDefinitiveReport(Long processBillingCalculationPk){
		StringBuilder sbQuery = new StringBuilder();
	    sbQuery.append("  SELECT  ");
	    sbQuery.append("  (CASE WHEN BCP.OPERATION_DATE IS NOT NULL THEN to_char(BCP.OPERATION_DATE,'dd/mm/yyyy')  "); 
	    sbQuery.append("   WHEN BCP.OPERATION_DATE IS  NULL THEN ' ' END ) AS OPERATION_DATE, "); //0
	    sbQuery.append("   BCP.BILLING_PERIOD, "); //1
	    sbQuery.append("  (CASE WHEN BC.BILLING_DATE IS NOT NULL THEN to_char(BC.BILLING_DATE,'dd/mm/yyyy')  "); 
	    sbQuery.append("   WHEN BC.BILLING_DATE IS  NULL THEN ' ' END ) AS BILLING_DATE, "); //2 Fecha cuando se confirmo el definitivo
	    
	    sbQuery.append("  (CASE WHEN bc.BILLING_NUMBER IS NOT NULL THEN bc.BILLING_NUMBER ||'' "); 
	    sbQuery.append("  WHEN bc.BILLING_NUMBER IS NULL THEN ' ' END) AS BILLING_NUMBER ,  ");//3 
	    
	    sbQuery.append("  BCP.PROCESSED_STATE, ");	//4 
	    sbQuery.append("  NVL(BCP.ENTITY_COLLECTION,0) as ENTITY_COLLECTION, ");	//5
	   
	    sbQuery.append("  NVL(BC.GROSS_AMOUNT,0) AS GROSS_AMOUNT_CAB,  ");			//6
	    sbQuery.append("  NVL(BC.NET_AMOUNT,0) AS NET_AMOUNT_CAB, ");			//7
	    sbQuery.append("  NVL(BC.NET_TAX,0) AS NET_TAX_CAB, ");				//8
	    
	    sbQuery.append("  NVL(bcd.GROSS_AMOUNT,0), ");			//9
	    sbQuery.append("  NVL(bcd.COLLECTION_AMOUNT,0), ");	//10
	    sbQuery.append("  NVL(bcd.TAX_APPLIED,0), ");			//11
	    
	    /**PK the entity*/
	    sbQuery.append("  CASE   ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN NVL(BC.ID_ISSUER_FK , ' ' )"); 
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN  BC.ID_PARTICIPANT_FK || ' '  ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN BC.ID_HOLDER_FK || ' '      ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ') ");
	    
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.STOCK_EXCHANGE.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ') ");//bolsa
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.CENTRAL_BANK.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ') ");//central bank
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HACIENDA.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ') ");//hacienda
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.AFP.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ') ");//afp
//	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.SUPERINTENDENCE.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ') ");//superintendencia
	    sbQuery.append("  END AS CODE_ENTITY_COLLECTION,  ");	//12
	    /**Name the entity*/
	    sbQuery.append("  CASE  ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN NVL(ISS.BUSINESS_NAME, ' ')  ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN NVL(PAR.DESCRIPTION, ' ') ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN NVL(HOL.full_name,' ')   ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN NVL(BC.ENTITY_DESCRIPTION, ' ') ");
	    
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.STOCK_EXCHANGE.getCode()+" THEN NVL(BC.ENTITY_DESCRIPTION, ' ') ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.CENTRAL_BANK.getCode()+" THEN NVL(BC.ENTITY_DESCRIPTION, ' ') ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HACIENDA.getCode()+" THEN NVL(BC.ENTITY_DESCRIPTION, ' ') ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.AFP.getCode()+" THEN NVL(BC.ENTITY_DESCRIPTION, ' ') ");//afp
//	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.SUPERINTENDENCE.getCode()+" THEN NVL(BC.ENTITY_DESCRIPTION, ' ') ");
	    sbQuery.append("  END AS NAME_ENTITY_COLLECTION, ");	//13
	    /**Address the entity*/
	    sbQuery.append("  CASE  ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN NVL(BC.OTHER_ENTITY_ADDRESS  ,'S/N ') ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN NVL(BC.OTHER_ENTITY_ADDRESS  ,'S/N ') ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN NVL(BC.OTHER_ENTITY_ADDRESS  ,'S/N ')  ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN NVL(BC.OTHER_ENTITY_ADDRESS  ,'S/N') ");
	    
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.STOCK_EXCHANGE.getCode()+" THEN NVL(BC.OTHER_ENTITY_ADDRESS, 'S/N') ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.CENTRAL_BANK.getCode()+" THEN NVL(BC.OTHER_ENTITY_ADDRESS, 'S/N') ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HACIENDA.getCode()+" THEN NVL(BC.OTHER_ENTITY_ADDRESS, 'S/N') ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.AFP.getCode()+" THEN NVL(BC.OTHER_ENTITY_ADDRESS, 'S/N') ");//afp
//	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.SUPERINTENDENCE.getCode()+" THEN NVL(BC.OTHER_ENTITY_ADDRESS, 'S/N') ");
	    sbQuery.append("  END AS ADDRESS_ENTITY_COLLECTION, ");//14
	    /**Document Number the entity*/
	    sbQuery.append("  CASE  ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN NVL(ISS.DOCUMENT_NUMBER ,' ')  ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN NVL( PAR.DOCUMENT_NUMBER ,' ') ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN NVL(HOL.DOCUMENT_NUMBER  ,' ')  ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ')  ");
	    
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.STOCK_EXCHANGE.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ') ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.CENTRAL_BANK.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ') ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HACIENDA.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ') ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.AFP.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ') ");//afp
//	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.SUPERINTENDENCE.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ') ");
	    sbQuery.append("  END AS DOC_NUMBER_ENTITY_COLLECTION, ");//15
	    
	    /**Country the entity*/
	    sbQuery.append("  CASE  ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN NVL( (select name from geographic_location where id_geographic_location_pk=BC.OTHER_ENTITY_COUNTRY),'S/N')     ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN NVL( (select name from geographic_location where id_geographic_location_pk=BC.OTHER_ENTITY_COUNTRY) ,'S/N')  ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN NVL( (select name from geographic_location where id_geographic_location_pk=BC.OTHER_ENTITY_COUNTRY)  ,'S/N') ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN NVL( (select name from geographic_location where id_geographic_location_pk=BC.OTHER_ENTITY_COUNTRY),'S/N')  ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.STOCK_EXCHANGE.getCode()+" THEN NVL( (select name from geographic_location where id_geographic_location_pk=BC.OTHER_ENTITY_COUNTRY),'S/N')     ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.CENTRAL_BANK.getCode()+" THEN NVL( (select name from geographic_location where id_geographic_location_pk=BC.OTHER_ENTITY_COUNTRY) ,'S/N')  ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HACIENDA.getCode()+" THEN NVL( (select name from geographic_location where id_geographic_location_pk=BC.OTHER_ENTITY_COUNTRY)  ,'S/N') ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.AFP.getCode()+" THEN NVL( (select name from geographic_location where id_geographic_location_pk=BC.OTHER_ENTITY_COUNTRY),'S/N')  ");
//	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.SUPERINTENDENCE.getCode()+" THEN NVL( (select name from geographic_location where id_geographic_location_pk=BC.OTHER_ENTITY_COUNTRY) ,'S/N')  ");
	    sbQuery.append("   END AS COUNTRY_ENTITY_COLLECTION, ");//16
	    
	    /**PROVINCE the entity*/
	    sbQuery.append("  CASE  ");
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append(" 	 where id_geographic_location_pk=BC.OTHER_ENTITY_PROVINCE AND TYPE_GEOGRAPHIC_LOCATION=3),'S/N')      ");
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append("  	 where id_geographic_location_pk=BC.OTHER_ENTITY_PROVINCE AND TYPE_GEOGRAPHIC_LOCATION=3) ,'S/N')  ");
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append(" 	 where id_geographic_location_pk=BC.OTHER_ENTITY_PROVINCE AND TYPE_GEOGRAPHIC_LOCATION=3)  ,'S/N') ");
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append(" 	 where id_geographic_location_pk=BC.OTHER_ENTITY_PROVINCE AND TYPE_GEOGRAPHIC_LOCATION=3)  ,'S/N') ");
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.STOCK_EXCHANGE.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append(" 	 where id_geographic_location_pk=BC.OTHER_ENTITY_PROVINCE AND TYPE_GEOGRAPHIC_LOCATION=3),'S/N')      ");
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.CENTRAL_BANK.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append("  	 where id_geographic_location_pk=BC.OTHER_ENTITY_PROVINCE AND TYPE_GEOGRAPHIC_LOCATION=3) ,'S/N')  ");
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HACIENDA.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append(" 	 where id_geographic_location_pk=BC.OTHER_ENTITY_PROVINCE AND TYPE_GEOGRAPHIC_LOCATION=3)  ,'S/N') ");
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.AFP.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append(" 	 where id_geographic_location_pk=BC.OTHER_ENTITY_PROVINCE AND TYPE_GEOGRAPHIC_LOCATION=3)  ,'S/N') ");
//	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.SUPERINTENDENCE.getCode()+" THEN NVL( (select name from geographic_location ");
//	    sbQuery.append(" 	 where id_geographic_location_pk=BC.OTHER_ENTITY_PROVINCE AND TYPE_GEOGRAPHIC_LOCATION=3)  ,'S/N') ");
	    sbQuery.append("   END AS PROVINCE_ENTITY_COLLECTION, ");//16
	    
	    /**DISTRICT the entity*/
	    sbQuery.append(" CASE  ");
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append(" 	  where id_geographic_location_pk=BC.OTHER_ENTITY_DISTRICT AND TYPE_GEOGRAPHIC_LOCATION=4),'S/N')  ");
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append("  	  where id_geographic_location_pk=BC.OTHER_ENTITY_DISTRICT AND TYPE_GEOGRAPHIC_LOCATION=4) ,'S/N ') ");
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append("  	 where id_geographic_location_pk=BC.OTHER_ENTITY_DISTRICT AND TYPE_GEOGRAPHIC_LOCATION=4)  ,'S/N ')  ");
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append("  	 where id_geographic_location_pk=BC.OTHER_ENTITY_DISTRICT AND TYPE_GEOGRAPHIC_LOCATION=4)  ,'S/N ')  ");
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.STOCK_EXCHANGE.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append(" 	  where id_geographic_location_pk=BC.OTHER_ENTITY_DISTRICT AND TYPE_GEOGRAPHIC_LOCATION=4),'S/N')  ");
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.CENTRAL_BANK.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append("  	  where id_geographic_location_pk=BC.OTHER_ENTITY_DISTRICT AND TYPE_GEOGRAPHIC_LOCATION=4) ,'S/N ') ");
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HACIENDA.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append("  	 where id_geographic_location_pk=BC.OTHER_ENTITY_DISTRICT AND TYPE_GEOGRAPHIC_LOCATION=4)  ,'S/N ')  ");
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.AFP.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append("  	 where id_geographic_location_pk=BC.OTHER_ENTITY_DISTRICT AND TYPE_GEOGRAPHIC_LOCATION=4)  ,'S/N ')  ");//18
//	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.SUPERINTENDENCE.getCode()+" THEN NVL( (select name from geographic_location ");
//	    sbQuery.append("  	 where id_geographic_location_pk=BC.OTHER_ENTITY_DISTRICT AND TYPE_GEOGRAPHIC_LOCATION=4)  ,'S/N ')  ");
	    sbQuery.append("   END AS DISTRICT_ENTITY_COLLECTION, ");//18
	    
	    sbQuery.append("  BC.CURRENCY_TYPE, ");			//19
	    sbQuery.append("  BS.SERVICE_CODE, ");			//20
	    sbQuery.append("  BS.SERVICE_NAME, ");			//21
	    sbQuery.append("  BC.ID_BILLING_CALCULATION_PK, ");//22
	    sbQuery.append("  (CASE WHEN BC.BILLING_DATE_START IS NOT NULL THEN to_char(BC.BILLING_DATE_START,'dd/mm/yyyy')      ");			
	    sbQuery.append("  WHEN BC.BILLING_DATE_START IS  NULL THEN ' ' END ) AS BILLING_DATE_START, ");			//23
	    sbQuery.append("  (CASE WHEN bc.BILLING_COURT_DATE IS NOT NULL THEN to_char(BC.BILLING_COURT_DATE,'dd/mm/yyyy')  ");		
	    sbQuery.append("  WHEN BC.BILLING_COURT_DATE IS  NULL THEN ' ' END ) AS BILLING_COURT_DATE, ");			//24
	    sbQuery.append("  bc.BILLING_NCF AS BILLING_NCF, ");		//25
	    sbQuery.append("  NVL(BC.INDENTIFY_TYPE,0 ) AS  INDENTIFY_TYPE ");			//26
	    sbQuery.append("   ");		
	    
	    sbQuery.append(" FROM  ");
	    sbQuery.append(" BILLING_CALCULATION_PROCESS BCP  ");
	    sbQuery.append(" inner join  BILLING_CALCULATION BC  on BC.ID_BILLING_CALC_PROCESS_FK=BCP.ID_BILLING_CALC_PROCESS_PK ");
	    sbQuery.append(" inner join BILLING_CALCULATION_DETAIL BCD on BCD.ID_BILLING_CALCULATION_FK=BC.ID_BILLING_CALCULATION_PK  ");
	    sbQuery.append(" inner join BILLING_SERVICE BS ON BS.ID_BILLING_SERVICE_PK=BCD.ID_BILLING_SERVICE_FK  "); 
	    sbQuery.append(" left JOIN ISSUER ISS ON(ISS.ID_ISSUER_PK=BC.ID_ISSUER_FK )   ");
	    sbQuery.append(" left JOIN PARTICIPANT PAR ON (PAR.ID_PARTICIPANT_PK=BC.ID_PARTICIPANT_FK)   ");
	    sbQuery.append(" left JOIN HOLDER HOL ON (HOL.ID_HOLDER_PK=BC.ID_HOLDER_FK)    ");
	    sbQuery.append(" where 1=1  ");
	    if(Validations.validateIsNotNullAndPositive(processBillingCalculationPk)){
	    	sbQuery.append(" and BCP.ID_BILLING_CALC_PROCESS_PK=:billingCalcProcessPk  ");
	    	
	    }

	    sbQuery.append(" order by  ");
	    sbQuery.append("  BCP.OPERATION_DATE,   ");
	    sbQuery.append("  BCP.ENTITY_COLLECTION, ");
	    sbQuery.append("  CASE   ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN BC.ID_ISSUER_FK || '' "); 
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN  BC.ID_PARTICIPANT_FK || ''  ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN BC.ID_HOLDER_FK || ''    ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ') ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.STOCK_EXCHANGE.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ') ");//bolsa
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.CENTRAL_BANK.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ') ");//central bank
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HACIENDA.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ') ");//hacienda
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.AFP.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ') ");//afp
//	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.SUPERINTENDENCE.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ') ");//hacienda
	    sbQuery.append("  END   ");
	    Query query = em.createNativeQuery(sbQuery.toString());
	    
	    if(Validations.validateIsNotNullAndPositive(processBillingCalculationPk)){
	    	query.setParameter("billingCalcProcessPk", processBillingCalculationPk);	    	
	    }
	    
	    
	    return query.getResultList();
	}
	
	

	/**
	 *  Return List Object, get Collection By Entity.
	 *
	 * @param participantPk the participant pk
	 * @return the information participant
	 */
	public Object[] getInformationParticipant(Long participantPk){
		StringBuilder sbQuery = new StringBuilder();
	    sbQuery.append("  SELECT  ");
	    sbQuery.append("   NVL(pa.DESCRIPTION,' ') AS DESCRIPTION,  ");
	    sbQuery.append("   NVL(pa.ADDRESS,' ') AS  ADDRESS ,");
	    sbQuery.append("   NVL( (select name from geographic_location where id_geographic_location_pk=pa.RESIDENCE_COUNTRY),'S/N') AS COUNTRY,  ");
	    sbQuery.append("   NVL(pa.PHONE_NUMBER,'S/N') AS PHONE_NUMBER,   ");
	    sbQuery.append("   NVL(pa.FAX_NUMBER, 'S/N') AS FAX_NUMBER ,  ");
	    sbQuery.append("   NVL(pa.COMMERCIAL_REGISTER,' ') AS COMMERCIAL_REGISTER  ");
	    sbQuery.append("  FROM   ");
	    sbQuery.append("  PARTICIPANT pa  ");
	    sbQuery.append("  WHERE 1=1   ");
	    
	    if(Validations.validateIsNotNullAndPositive(participantPk)){
	    	sbQuery.append(" and id_participant_pk=:participantPk  ");
	    }
	    Query query = em.createNativeQuery(sbQuery.toString());
	    
	    if(Validations.validateIsNotNullAndPositive(participantPk)){
	    	query.setParameter("participantPk", participantPk);	    	
	    }
	    
	    
	    return (Object[]) query.getSingleResult();
	}
	
	/**
	 * Gets the file structure billing report.
	 *
	 * @param processBillingCalculationPk the process billing calculation pk
	 * @return the file structure billing report
	 */
	public List<Object[]> getFileStructureBillingReport(Long processBillingCalculationPk){
		StringBuilder sbQuery = new StringBuilder();
	    
	    
	    if(Validations.validateIsNotNullAndPositive(processBillingCalculationPk)){
	    	sbQuery.append("   ");	
	    }
	    
	    Query query = em.createNativeQuery(sbQuery.toString());
	    
	    if(Validations.validateIsNotNullAndPositive(processBillingCalculationPk)){
	    	query.setParameter("billingCalcProcessPk", processBillingCalculationPk);	    	
	    }
	    return query.getResultList();
	}
	
	/**
	 *  Return List Object, get Report Billing Preliminary.
	 *
	 * @param processBillingCalculationPk the process billing calculation pk
	 * @return the consolidated billing preliminary report
	 */
	public List<Object[]> getConsolidatedBillingPreliminaryReport(Long processBillingCalculationPk){
		StringBuilder sbQuery = new StringBuilder();
	    sbQuery.append("  SELECT  ");
	    sbQuery.append("  (CASE WHEN BCP.OPERATION_DATE IS NOT NULL THEN to_char(BCP.OPERATION_DATE,'dd/mm/yyyy')  "); 
	    sbQuery.append("   WHEN BCP.OPERATION_DATE IS  NULL THEN ' ' END ) AS OPERATION_DATE, "); //0
	    sbQuery.append("   BCP.BILLING_PERIOD, "); //1
	    sbQuery.append("  (CASE WHEN BC.BILLING_DATE IS NOT NULL THEN to_char(BC.BILLING_DATE,'dd/mm/yyyy')  "); 
	    sbQuery.append("   WHEN BC.BILLING_DATE IS  NULL THEN ' ' END ) AS BILLING_DATE, "); //2 Fecha cuando se confirmo el definitivo
	    
	    sbQuery.append("  (CASE WHEN bc.BILLING_NUMBER IS NOT NULL THEN bc.BILLING_NUMBER ||'' "); 
	    sbQuery.append("  WHEN bc.BILLING_NUMBER IS NULL THEN ' ' END) AS BILLING_NUMBER ,  ");//3 
	    
	    sbQuery.append("  BCP.PROCESSED_STATE, ");	//4 
	    sbQuery.append("  NVL(BCP.ENTITY_COLLECTION,0) as ENTITY_COLLECTION, ");	//5
	   
	    sbQuery.append("  NVL(BC.GROSS_AMOUNT,0) AS GROSS_AMOUNT_CAB,  ");			//6
	    sbQuery.append("  NVL(BC.NET_AMOUNT,0) AS NET_AMOUNT_CAB, ");			//7
	    sbQuery.append("  NVL(BC.NET_TAX,0) AS NET_TAX_CAB, ");				//8
	    
	    sbQuery.append("  NVL(bcd.GROSS_AMOUNT,0), ");			//9
	    sbQuery.append("  NVL(bcd.COLLECTION_AMOUNT,0), ");	//10
	    sbQuery.append("  nvl(bcd.TAX_APPLIED,0), ");			//11
	    
	    /**PK the entity*/
	    sbQuery.append("  CASE   ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN NVL(bc.ID_ISSUER_FK , ' ' )"); 
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN  BC.ID_PARTICIPANT_FK || ' '  ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN BC.ID_HOLDER_FK || ' '      ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ') ");
	    
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.STOCK_EXCHANGE.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ') ");//bolsa
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.CENTRAL_BANK.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ') ");//central bank
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HACIENDA.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ') ");//hacienda
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.AFP.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ') ");//afp
//	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.SUPERINTENDENCE.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ') ");//hacienda
	    sbQuery.append("  END AS CODE_ENTITY_COLLECTION,  ");	//12
	    /**Name the entity*/
	    sbQuery.append("  CASE  ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN NVL(ISS.BUSINESS_NAME, ' ')  ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN NVL(PAR.DESCRIPTION, ' ')");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN NVL(HOL.full_name,' ')   ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN NVL(BC.ENTITY_DESCRIPTION, ' ') ");

	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.STOCK_EXCHANGE.getCode()+" THEN NVL(BC.ENTITY_DESCRIPTION, ' ') ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.CENTRAL_BANK.getCode()+" THEN NVL(BC.ENTITY_DESCRIPTION, ' ') ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HACIENDA.getCode()+" THEN NVL(BC.ENTITY_DESCRIPTION, ' ') ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.AFP.getCode()+" THEN NVL(BC.ENTITY_DESCRIPTION, ' ') ");//afp
//	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.SUPERINTENDENCE.getCode()+" THEN NVL(BC.ENTITY_DESCRIPTION, ' ') ");
	    sbQuery.append("  END AS NAME_ENTITY_COLLECTION, ");	//13
	    /**Address the entity*/
	    sbQuery.append("  CASE  ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN NVL(ISS.LEGAL_ADDRESS  ,'S/N ') ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN NVL( PAR.ADDRESS  ,'S/N ') ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN NVL(HOL.LEGAL_ADDRESS  ,'S/N ')  ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN NVL(BC.OTHER_ENTITY_ADDRESS,'S/N') ");
	    
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.STOCK_EXCHANGE.getCode()+" THEN NVL(   to_char((select pt.COMMENTS from parameter_table pt where parameter_table_pk=BCP.ENTITY_COLLECTION)),  'S/N') ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.CENTRAL_BANK.getCode()+" THEN NVL(   to_char((select pt.COMMENTS from parameter_table pt where parameter_table_pk=BCP.ENTITY_COLLECTION)),  'S/N')   ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HACIENDA.getCode()+" THEN NVL(   to_char((select pt.COMMENTS from parameter_table pt where parameter_table_pk=BCP.ENTITY_COLLECTION)),  'S/N') ");
//	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.SUPERINTENDENCE.getCode()+" THEN NVL(   to_char((select pt.COMMENTS from parameter_table pt where parameter_table_pk=BCP.ENTITY_COLLECTION)),  'S/N') ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.AFP.getCode()+" THEN NVL(   PAR.ADDRESS,  'S/N') ");//afp
	    sbQuery.append("  END AS ADDRESS_ENTITY_COLLECTION, ");//14
	    /**Document Number the entity*/
	    sbQuery.append("  CASE  ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN NVL(ISS.DOCUMENT_NUMBER ,' ')  ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN NVL( PAR.DOCUMENT_NUMBER ,' ') ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN NVL(HOL.DOCUMENT_NUMBER  ,' ')  ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ')  ");
	    
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.STOCK_EXCHANGE.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ')  ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.CENTRAL_BANK.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ')  ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HACIENDA.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ')  ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.AFP.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ')  ");
//	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.SUPERINTENDENCE.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ')  ");
	    
	    sbQuery.append("  END AS DOC_NUMBER_ENTITY_COLLECTION, ");//15
	    
	    /**Country the entity*/
	    sbQuery.append("  CASE  ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN NVL( (select name from geographic_location where id_geographic_location_pk=ISS.RESIDENCE_COUNTRY),'S/N')     ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN NVL( (select name from geographic_location where id_geographic_location_pk=par.RESIDENCE_COUNTRY) ,'S/N')  ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN NVL( (select name from geographic_location where id_geographic_location_pk=hol.LEGAL_RESIDENCE_COUNTRY)  ,'S/N') ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN NVL( (select name from geographic_location where id_geographic_location_pk=BC.OTHER_ENTITY_COUNTRY AND TYPE_GEOGRAPHIC_LOCATION=1),'S/N')  ");
	    
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.STOCK_EXCHANGE.getCode()+" THEN NVL( (select name from geographic_location where ");
	    sbQuery.append("   id_geographic_location_pk=(select pt.INDICATOR4 from parameter_table pt where parameter_table_pk=BCP.ENTITY_COLLECTION)  ");
	    sbQuery.append("   AND TYPE_GEOGRAPHIC_LOCATION=1)  ,'S/N') ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.CENTRAL_BANK.getCode()+" THEN NVL( (select name from geographic_location where ");
	    sbQuery.append("   id_geographic_location_pk=(select pt.INDICATOR4 from parameter_table pt where parameter_table_pk=BCP.ENTITY_COLLECTION)  ");
	    sbQuery.append("   AND TYPE_GEOGRAPHIC_LOCATION=1)  ,'S/N') ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HACIENDA.getCode()+" THEN NVL( (select name from geographic_location where ");
	    sbQuery.append("   id_geographic_location_pk=(select pt.INDICATOR4 from parameter_table pt where parameter_table_pk=BCP.ENTITY_COLLECTION)  ");
	    sbQuery.append("   AND TYPE_GEOGRAPHIC_LOCATION=1)  ,'S/N') ");
//	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.SUPERINTENDENCE.getCode()+" THEN NVL( (select name from geographic_location where ");
//	    sbQuery.append("   id_geographic_location_pk=(select pt.INDICATOR4 from parameter_table pt where parameter_table_pk=BCP.ENTITY_COLLECTION)  ");
//	    sbQuery.append("   AND TYPE_GEOGRAPHIC_LOCATION=1)  ,'S/N') ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.AFP.getCode()+" THEN NVL( (select name from geographic_location where id_geographic_location_pk=par.RESIDENCE_COUNTRY) ,'S/N') ");//afp
	    sbQuery.append("   END AS COUNTRY_ENTITY_COLLECTION, ");//16
	    
	    /**PROVINCE the entity*/
	    sbQuery.append(" CASE  ");
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append(" 	 where id_geographic_location_pk=ISS.PROVINCE AND TYPE_GEOGRAPHIC_LOCATION=3),'S/N')      ");
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append("  	 where id_geographic_location_pk=par.PROVINCE AND TYPE_GEOGRAPHIC_LOCATION=3) ,'S/N')  ");
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append(" 	 where id_geographic_location_pk=hol.LEGAL_PROVINCE AND TYPE_GEOGRAPHIC_LOCATION=3)  ,'S/N') ");
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append(" 	 where id_geographic_location_pk=BC.OTHER_ENTITY_PROVINCE AND TYPE_GEOGRAPHIC_LOCATION=3)  ,'S/N') ");
	    
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.STOCK_EXCHANGE.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append(" 	 where id_geographic_location_pk=(select pt.INDICATOR5 from parameter_table pt where parameter_table_pk=BCP.ENTITY_COLLECTION) AND TYPE_GEOGRAPHIC_LOCATION=3)  ,'S/N') ");
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.CENTRAL_BANK.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append(" 	 where id_geographic_location_pk=(select pt.INDICATOR5 from parameter_table pt where parameter_table_pk=BCP.ENTITY_COLLECTION) AND TYPE_GEOGRAPHIC_LOCATION=3)  ,'S/N') ");
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HACIENDA.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append(" 	 where id_geographic_location_pk=(select pt.INDICATOR5 from parameter_table pt where parameter_table_pk=BCP.ENTITY_COLLECTION) AND TYPE_GEOGRAPHIC_LOCATION=3)  ,'S/N') ");
//	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.SUPERINTENDENCE.getCode()+" THEN NVL( (select name from geographic_location ");
//	    sbQuery.append(" 	 where id_geographic_location_pk=(select pt.INDICATOR5 from parameter_table pt where parameter_table_pk=BCP.ENTITY_COLLECTION) AND TYPE_GEOGRAPHIC_LOCATION=3)  ,'S/N') ");
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.AFP.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append("  	 where id_geographic_location_pk=par.PROVINCE AND TYPE_GEOGRAPHIC_LOCATION=3) ,'S/N')  ");//afp
	    
	    sbQuery.append("  END AS PROVINCE_ENTITY_COLLECTION, ");//17 
	    
	    /**DISTRICT the entity*/
	    sbQuery.append(" CASE  ");
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append(" 	  where id_geographic_location_pk=ISS.DISTRICT AND TYPE_GEOGRAPHIC_LOCATION=4),'S/N')  ");
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append("  	  where id_geographic_location_pk=par.DISTRICT AND TYPE_GEOGRAPHIC_LOCATION=4) ,'S/N ') ");
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append("  	 where id_geographic_location_pk=hol.LEGAL_DISTRICT AND TYPE_GEOGRAPHIC_LOCATION=4)  ,'S/N ')  ");
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append("  	 where id_geographic_location_pk=BC.OTHER_ENTITY_DISTRICT AND TYPE_GEOGRAPHIC_LOCATION=4)  ,'S/N ')  ");
	    
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.STOCK_EXCHANGE.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append(" 	  where id_geographic_location_pk=(select pt.INDICATOR6 from parameter_table pt where parameter_table_pk=BCP.ENTITY_COLLECTION) AND TYPE_GEOGRAPHIC_LOCATION=4),'S/N')  ");
	    
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.CENTRAL_BANK.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append(" 	  where id_geographic_location_pk=(select pt.INDICATOR6 from parameter_table pt where parameter_table_pk=BCP.ENTITY_COLLECTION) AND TYPE_GEOGRAPHIC_LOCATION=4),'S/N')  ");
	    
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HACIENDA.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append(" 	  where id_geographic_location_pk=(select pt.INDICATOR6 from parameter_table pt where parameter_table_pk=BCP.ENTITY_COLLECTION) AND TYPE_GEOGRAPHIC_LOCATION=4),'S/N')  ");
	    
//	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.SUPERINTENDENCE.getCode()+" THEN NVL( (select name from geographic_location ");
//	    sbQuery.append(" 	  where id_geographic_location_pk=(select pt.INDICATOR6 from parameter_table pt where parameter_table_pk=BCP.ENTITY_COLLECTION) AND TYPE_GEOGRAPHIC_LOCATION=4),'S/N')  ");
	    
	    sbQuery.append("  WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.AFP.getCode()+" THEN NVL( (select name from geographic_location ");
	    sbQuery.append("  	  where id_geographic_location_pk=par.DISTRICT AND TYPE_GEOGRAPHIC_LOCATION=4) ,'S/N ') ");
	    sbQuery.append("  END AS DISTRICT_ENTITY_COLLECTION, ");//
	    
	    
	    sbQuery.append("  BC.CURRENCY_TYPE, ");			//19
	    sbQuery.append("  NVL(BS.SERVICE_CODE,' ') as SERVICE_CODE, ");			//20
	    sbQuery.append("  NVL(BS.SERVICE_NAME, ' ') as SERVICE_NAME, ");			//21
	    sbQuery.append("  BC.ID_BILLING_CALCULATION_PK, ");//22
	    sbQuery.append("  (CASE WHEN BC.BILLING_DATE_START IS NOT NULL THEN to_char(BC.BILLING_DATE_START,'dd/mm/yyyy')      ");			
	    sbQuery.append("  WHEN BC.BILLING_DATE_START IS  NULL THEN ' ' END ) AS BILLING_DATE_START, ");			//23
	    sbQuery.append("  (CASE WHEN bc.BILLING_COURT_DATE IS NOT NULL THEN to_char(BC.BILLING_COURT_DATE,'dd/mm/yyyy')  ");		
	    sbQuery.append("  WHEN BC.BILLING_COURT_DATE IS  NULL THEN ' ' END ) AS BILLING_COURT_DATE, ");			//24
	    sbQuery.append("  bc.BILLING_NCF AS BILLING_NCF, ");		//25
	    sbQuery.append("  NVL(BC.INDENTIFY_TYPE,0 ) AS  INDENTIFY_TYPE ");			//26
	    sbQuery.append("   ");		
	    
	    sbQuery.append(" FROM  ");
	    sbQuery.append(" BILLING_CALCULATION_PROCESS BCP  ");
	    sbQuery.append(" inner join  BILLING_CALCULATION BC  on BC.ID_BILLING_CALC_PROCESS_FK=BCP.ID_BILLING_CALC_PROCESS_PK ");
	    sbQuery.append(" inner join BILLING_CALCULATION_DETAIL BCD on BCD.ID_BILLING_CALCULATION_FK=BC.ID_BILLING_CALCULATION_PK  ");
	    sbQuery.append(" inner join BILLING_SERVICE BS ON BS.ID_BILLING_SERVICE_PK=BCD.ID_BILLING_SERVICE_FK  "); 
	    sbQuery.append(" left JOIN ISSUER ISS ON(ISS.ID_ISSUER_PK=BC.ID_ISSUER_FK )   ");
	    sbQuery.append(" left JOIN PARTICIPANT PAR ON (PAR.ID_PARTICIPANT_PK=BC.ID_PARTICIPANT_FK)   ");
	    sbQuery.append(" left JOIN HOLDER HOL ON (HOL.ID_HOLDER_PK=BC.ID_HOLDER_FK)    ");
	    sbQuery.append(" where 1=1  ");
	    if(Validations.validateIsNotNullAndPositive(processBillingCalculationPk)){
	    	sbQuery.append(" and BCP.ID_BILLING_CALC_PROCESS_PK=:billingCalcProcessPk  ");	
	    }

	    sbQuery.append(" order by  ");
	    sbQuery.append("  BCP.OPERATION_DATE,   ");
	    sbQuery.append("  BCP.ENTITY_COLLECTION, ");
	    sbQuery.append("  CASE   ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN BC.ID_ISSUER_FK || '' "); 
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN  BC.ID_PARTICIPANT_FK || ''  ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN BC.ID_HOLDER_FK || ''    ");
	    sbQuery.append("   WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ') ");
	    sbQuery.append("  END   ");
	    
	    Query query = em.createNativeQuery(sbQuery.toString());
	    
	    if(Validations.validateIsNotNullAndPositive(processBillingCalculationPk)){
	    	query.setParameter("billingCalcProcessPk", processBillingCalculationPk);	    	
	    }
	    
	    
	    return query.getResultList();
	}


	/**
	 * Gets the account receivable.
	 *
	 * @param processBillingCalculationPk the process billing calculation pk
	 * @return the account receivable
	 */
	public List<Object[]> getAccountReceivable(Long processBillingCalculationPk){
		
		StringBuilder sbQuery=new StringBuilder();
		 /**PK the entity*/
		sbQuery.append(" SELECT ");
		sbQuery.append("	BC.BILLING_NUMBER AS NUMCXC, ");
		sbQuery.append("	BS.ACCOUNTING_CODE AS CUENTA, ");
	    sbQuery.append("  	CASE   ");
	    sbQuery.append("   		WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN NVL(ISS.MNEMONIC , ' ' )    "); 
	    sbQuery.append("   		WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN  PAR.MNEMONIC || ' '   ");
	    sbQuery.append("   		WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN PART.MNEMONIC || ' '        ");
	    sbQuery.append("   		WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ')     ");
	    sbQuery.append("	END AS CLIENTE,  ");
	    sbQuery.append("	BS.SERVICE_NAME||' '||TO_CHAR(BC.BILLING_DATE_START,'DD-MM-YYYY') ");
	    sbQuery.append("	||' AL '||TO_CHAR(BC.BILLING_COURT_DATE,'DD-MM-YYYY') AS DESCRIPCION, ");
	    sbQuery.append("	'VFP' AS IMPUESTO, ");
	    sbQuery.append("	(SELECT PT.DESCRIPTION FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK=BC.CURRENCY_TYPE) AS MONEDA, ");
//	    sbQuery.append("	DECODE(BC.COUNT_REQUEST, 0 , 1 , BC.COUNT_REQUEST ) AS CANTIDAD, ");
	    sbQuery.append("	'1' AS CANTIDAD, ");
	    sbQuery.append("	ROUND(BC.GROSS_AMOUNT , 2) AS PRECIO, ");
	    sbQuery.append("	TO_CHAR(BC.BILLING_DATE,'YYYYMMDD') AS FECOBRO, ");
	    sbQuery.append("	BS.REFERENCE_RATE AS TIPOSERVICIO, ");
	    sbQuery.append("	BS.REFERENCE_RATE||'-1000' AS REF_AUX, ");//10
	    sbQuery.append("	BS.BASE_COLLECTION,  ");//11
	    sbQuery.append("	BC.ENTITY_CODE||' '||BC.ENTITY_DESCRIPTION,  ");//12
	    
	    sbQuery.append("  	CASE   ");
	    sbQuery.append("		WHEN BC.MNEMONIC_CONCATENATED IS NOT NULL and BC.MNEMONIC_CONCATENATED!='null' THEN BC.MNEMONIC_CONCATENATED  ");//13
	    sbQuery.append("   		WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN NVL(ISS.MNEMONIC , ' ' )    "); 
	    sbQuery.append("   		WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN  PAR.MNEMONIC || ' '   ");
	    sbQuery.append("   		WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN PART.MNEMONIC || ' '        ");
	    sbQuery.append("   		WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ')     ");
	    sbQuery.append("	END AS MNEMONIC_CONCATENATED,  ");//13
	    sbQuery.append("  	BCD.ID_BILLING_SERVICE_FK,   ");//14
	    sbQuery.append("  	'T'||LPAD(BS.SERVICE_CODE,4,'0'),   ");//15
	  //sbQuery.append("  	NVL(PART.MNEMONIC ,' ')||' - '||NVL(HOL.ID_HOLDER_PK,'')||' - '|| NVL(HOL.FULL_NAME,' ')||' - '|| NVL(HOL.FUND_TYPE,' ')   ");//16
	    sbQuery.append("  	NVL(PART.MNEMONIC ,' ')||' - '||NVL(HOL.ID_HOLDER_PK,'')||' '|| NVL(HOL.FULL_NAME,' '),");//16
	    sbQuery.append("  	NVL(HOL.ID_HOLDER_PK,'')");//17

	    sbQuery.append(" FROM  ");
	    sbQuery.append(" 	BILLING_CALCULATION_PROCESS BCP  ");
	    sbQuery.append(" 	INNER JOIN BILLING_CALCULATION BC  on BC.ID_BILLING_CALC_PROCESS_FK=BCP.ID_BILLING_CALC_PROCESS_PK  ");
	    sbQuery.append(" 	INNER JOIN BILLING_CALCULATION_DETAIL BCD on BCD.ID_BILLING_CALCULATION_FK=BC.ID_BILLING_CALCULATION_PK  ");
	    sbQuery.append(" 	INNER JOIN BILLING_SERVICE BS ON BS.ID_BILLING_SERVICE_PK=BCD.ID_BILLING_SERVICE_FK  "); 
	    sbQuery.append(" 	LEFT JOIN ISSUER ISS ON(ISS.ID_ISSUER_PK=BC.ID_ISSUER_FK )   ");
	    sbQuery.append(" 	LEFT JOIN PARTICIPANT PAR ON (PAR.ID_PARTICIPANT_PK=BC.ID_PARTICIPANT_FK)   ");
	    sbQuery.append(" 	LEFT JOIN HOLDER HOL ON (HOL.ID_HOLDER_PK=BC.ID_HOLDER_FK)    ");
	    sbQuery.append(" 	LEFT JOIN PARTICIPANT PART ON (PART.DOCUMENT_NUMBER=HOL.DOCUMENT_NUMBER)    ");
	    
	    sbQuery.append(" WHERE 1=1  ");
	    
	    if(Validations.validateIsNotNullAndPositive(processBillingCalculationPk)){
	    	sbQuery.append(" AND BCP.ID_BILLING_CALC_PROCESS_PK=:billingCalcProcessPk  ");	
	    }
	    
	    Query query = em.createNativeQuery(sbQuery.toString());
	    
	    if(Validations.validateIsNotNullAndPositive(processBillingCalculationPk)){
	    	query.setParameter("billingCalcProcessPk", processBillingCalculationPk);	    	
	    }

	    return query.getResultList();
	}
	
	/**
	 * issue 1157: es el mismo reporte ACV del metodo  getQueryCommissionsAnnotationAccount pero con diferente agrupacion
	 * @param commissionsAnnotationAccountTo
	 * @return
	 */
	public String getQueryCommissionsAnnotationAccountNonBCB(CommissionsAnnotationAccountTo commissionsAnnotationAccountTo){
    	StringBuilder sbQuery= new StringBuilder();
    	
    	sbQuery.append(" select ");
    	sbQuery.append("     X.REFERENCE_RATE, ");//0
    	sbQuery.append("     X.SERVICE_CODE, ");//1
    	sbQuery.append("     X.SERVICE_NAME, ");//2
    	sbQuery.append("     X.ENTITY_COLLECTION, ");//3
    	sbQuery.append("     X.MNEMONIC, ");//4
    	sbQuery.append("     X.CODE_ENTITY_COLLECTION, ");//5
    	sbQuery.append("     X.NAME_ENTITY_COLLECTION, ");//6
    	sbQuery.append("     X.CALCULATION_DATE, ");//7
    	sbQuery.append("     X.CALCULATION_DATE_STRING, ");//8
    	sbQuery.append("     X.ID_HOLDER_FK, ");//9
    	sbQuery.append("     X.FULL_NAME, ");//10
    	sbQuery.append("     X.SECURITY_CLASS, ");//11
    	sbQuery.append("     X.CLASE_VALOR_NEMONIC, ");//12
    	sbQuery.append("     X.CURRENCY_ORIGIN, ");//13
    	sbQuery.append("     X.CURRENCY_DESCRIPTION, ");//14
    	sbQuery.append("     X.RATE_PERCENT, ");//15
    	sbQuery.append("     X.EXCHANGE_RATE_CURRENT, ");//16
    	sbQuery.append("     SUM(X.RATE_AMOUNT) COLLECTION_AMOUNT , ");//17
    	sbQuery.append("     SUM(X.RATE_AMOUNT_BILLED) COLLECTION_AMOUNT_BILLED, ");//18
    	sbQuery.append("     SUM(X.NEGOTIATED_AMOUNT_ORIGIN) NEGOTIATED_AMOUNT_ORIGIN, ");//19
    	sbQuery.append("     SUM(X.NEGOTIATED_AMOUNT) NEGOTIATED_AMOUNT ");//20
    	sbQuery.append("     from (select ");
    	sbQuery.append("     BS.REFERENCE_RATE, ");
    	sbQuery.append("     TO_NUMBER(BS.SERVICE_CODE,9999) SERVICE_CODE, ");
    	sbQuery.append("     BS.SERVICE_NAME, ");
    	sbQuery.append("     BS.ENTITY_COLLECTION, ");
    	sbQuery.append("     CASE ");
    	sbQuery.append("         WHEN BS.ENTITY_COLLECTION=1406      THEN (SELECT ISS.MNEMONIC FROM ISSUER ISS WHERE ISS.ID_ISSUER_PK =CR.ID_ISSUER_FK ) ");
    	sbQuery.append("         WHEN BS.ENTITY_COLLECTION=1407  THEN (SELECT PAR.MNEMONIC FROM PARTICIPANT PAR WHERE PAR.ID_PARTICIPANT_PK= CR.ID_PARTICIPANT_FK ) ");
    	sbQuery.append("         WHEN BS.ENTITY_COLLECTION=1408       THEN (SELECT HOL.MNEMONIC FROM HOLDER HOL WHERE HOL.ID_HOLDER_PK=CR.ID_HOLDER_FK) ");
    	sbQuery.append("         WHEN BS.ENTITY_COLLECTION=1530       THEN NVL(CR.OTHER_ENTITY_DOCUMENT, ' ') ");
    	sbQuery.append("     END AS   MNEMONIC , ");
    	sbQuery.append("     CASE ");
    	sbQuery.append("         WHEN BS.ENTITY_COLLECTION=1406       THEN NVL(CR.ID_ISSUER_FK , ' ' ) ");
    	sbQuery.append("         WHEN BS.ENTITY_COLLECTION=1407   THEN  CR.ID_PARTICIPANT_FK || ' ' ");
    	sbQuery.append("         WHEN BS.ENTITY_COLLECTION=1408       THEN CR.ID_HOLDER_FK || ' ' ");
    	sbQuery.append("         WHEN BS.ENTITY_COLLECTION=1530       THEN NVL(CR.OTHER_ENTITY_DOCUMENT, ' ') ");
    	sbQuery.append("     END AS   CODE_ENTITY_COLLECTION , ");
    	sbQuery.append("     CASE ");
    	sbQuery.append("         WHEN BS.ENTITY_COLLECTION=1406      THEN (SELECT ISS.BUSINESS_NAME FROM ISSUER ISS WHERE ISS.ID_ISSUER_PK =CR.ID_ISSUER_FK ) ");
    	sbQuery.append("         WHEN BS.ENTITY_COLLECTION=1407  	 THEN (SELECT PAR.DESCRIPTION FROM PARTICIPANT PAR WHERE PAR.ID_PARTICIPANT_PK= CR.ID_PARTICIPANT_FK ) ");
    	sbQuery.append("         WHEN BS.ENTITY_COLLECTION=1408      THEN (SELECT HOL.full_name FROM HOLDER HOL WHERE HOL.ID_HOLDER_PK=CR.ID_HOLDER_FK) ");
    	sbQuery.append("         WHEN BS.ENTITY_COLLECTION=1530      THEN NVL(CR.OTHER_ENTITY_DESCRIPTION, ' ') ");
    	sbQuery.append("     END AS  NAME_ENTITY_COLLECTION , ");
    	sbQuery.append("     trunc(CR.CALCULATION_DATE) CALCULATION_DATE, ");
    	sbQuery.append("     TO_CHAR(CR.CALCULATION_DATE,'DD/MON/YY','NLS_DATE_LANGUAGE = SPANISH') CALCULATION_DATE_STRING, ");
    	sbQuery.append("     decode (BS.REFERENCE_RATE,'T02',HOL2.ID_HOLDER_PK,HA.RELATED_ACCOUNT) ID_HOLDER_FK, ");
    	sbQuery.append("     decode (BS.REFERENCE_RATE,'T02',HOL2.full_name,decode (BS.REFERENCE_RATE,'T02',HOL2.full_name, ");
		sbQuery.append("      	(SELECT LISTAGG(H.FULL_NAME,',') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
		sbQuery.append(" 	  	FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
		sbQuery.append("		WHERE HAD.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK))) FULL_NAME, "); 
    	sbQuery.append("     CRD.SECURITY_CLASS,");
    	sbQuery.append("     (SELECT TEXT1 FROM PARAMETER_TABLE ");
    	sbQuery.append("     WHERE PARAMETER_TABLE_PK=CRD.SECURITY_CLASS) CLASE_VALOR_NEMONIC, ");
    	sbQuery.append("     CRD.CURRENCY_ORIGIN, ");
    	sbQuery.append("     (SELECT PT.DESCRIPTION FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK=CRD.CURRENCY_ORIGIN) AS CURRENCY_DESCRIPTION, ");
    	sbQuery.append("     (SR.RATE_PERCENT*36500) RATE_PERCENT, ");
    	sbQuery.append("     CRD.RATE_AMOUNT, ");
    	sbQuery.append("     CRD.RATE_AMOUNT_BILLED , ");
    	sbQuery.append("     CRD.EXCHANGE_RATE_CURRENT, ");
    	sbQuery.append("     CRD.NEGOTIATED_AMOUNT_ORIGIN, ");
    	sbQuery.append("     CRD.NEGOTIATED_AMOUNT ");
    	sbQuery.append("  ");
    	sbQuery.append(" FROM PROCESSED_SERVICE PS ");
    	sbQuery.append("     INNER JOIN COLLECTION_RECORD CR ON CR.ID_PROCESSED_SERVICE_FK=PS.ID_PROCESSED_SERVICE_PK ");
    	sbQuery.append("     INNER JOIN COLLECTION_RECORD_DETAIL CRD ON CRD.ID_COLLECTION_RECORD_FK= CR.ID_COLLECTION_RECORD_PK ");
    	sbQuery.append("     INNER JOIN BILLING_SERVICE BS ON BS.ID_BILLING_SERVICE_PK=PS.ID_BILLING_SERVICE_FK ");
    	sbQuery.append("     INNER JOIN SERVICE_RATE SR ON SR.ID_BILLING_SERVICE_FK=BS.ID_BILLING_SERVICE_PK ");
    	sbQuery.append("     LEFT JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK=CRD.ID_HOLDER_ACCOUNT_FK ");
    	sbQuery.append("     LEFT JOIN ISSUER ISS2 ON (ISS2.ID_ISSUER_PK=CRD.ID_ISSUER_FK) ");
    	sbQuery.append("     LEFT JOIN HOLDER HOL2 ON (HOL2.ID_HOLDER_PK=ISS2.ID_HOLDER_FK) ");
    	sbQuery.append("  ");
    	sbQuery.append(" WHERE BS.BASE_COLLECTION IN (253,1412,1800,1801,2136,2137) ");
    	sbQuery.append(" AND trunc(CR.CALCULATION_DATE) BETWEEN to_date('"+commissionsAnnotationAccountTo.getDateInitial()+"','DD/MM/YYYY') AND to_date('"+commissionsAnnotationAccountTo.getDateEnd()+"','DD/MM/YYYY') ");
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getServiceCode())){
    		sbQuery.append(" AND (BS.SERVICE_CODE= :p_service_code ) ");
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getEntityCode())){
    		sbQuery.append(" AND ( ");
    		sbQuery.append(" 	(BS.ENTITY_COLLECTION=1406 AND CR.ID_ISSUER_FK= ':p_entity_code' ) ");
    		sbQuery.append("    OR (BS.ENTITY_COLLECTION=1407 AND CR.ID_PARTICIPANT_FK= :p_entity_code ) ");
    		sbQuery.append("    OR (BS.ENTITY_COLLECTION=1408 AND CR.ID_HOLDER_FK= :p_entity_code ) ");
    		sbQuery.append("    OR (BS.ENTITY_COLLECTION=1409 AND CR.OTHER_ENTITY_DOCUMENT= :p_entity_code )) ");
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getCui())){
    		sbQuery.append(" AND (CRD.ID_HOLDER_FK= :cui_code ) ");
    	}
    	sbQuery.append(" ) X ");
    	sbQuery.append(" GROUP BY ");
    	sbQuery.append("     X.REFERENCE_RATE, ");
    	sbQuery.append("     X.SERVICE_CODE, ");
    	sbQuery.append("     X.SERVICE_NAME, ");
    	sbQuery.append("     X.ENTITY_COLLECTION, ");
    	sbQuery.append("     X.MNEMONIC, ");
    	sbQuery.append("     X.CODE_ENTITY_COLLECTION, ");
    	sbQuery.append("     X.NAME_ENTITY_COLLECTION, ");
    	sbQuery.append("     X.ID_HOLDER_FK, ");
    	sbQuery.append("     X.FULL_NAME, ");
    	sbQuery.append("     X.CALCULATION_DATE, ");
    	sbQuery.append("     X.CALCULATION_DATE_STRING, ");
    	sbQuery.append("     X.SECURITY_CLASS, ");
    	sbQuery.append("     X.CLASE_VALOR_NEMONIC, ");
    	sbQuery.append("     X.CURRENCY_ORIGIN, ");
    	sbQuery.append("     X.CURRENCY_DESCRIPTION, ");
    	sbQuery.append("     X.RATE_PERCENT, ");
    	sbQuery.append("     X.EXCHANGE_RATE_CURRENT ");
    	sbQuery.append(" ORDER BY ");
    	sbQuery.append("     X.REFERENCE_RATE, ");
    	sbQuery.append("     X.SERVICE_CODE, ");
    	sbQuery.append("     X.CODE_ENTITY_COLLECTION, ");
    	sbQuery.append("     X.ID_HOLDER_FK, ");
    	sbQuery.append("     X.CALCULATION_DATE, ");
    	sbQuery.append("     X.SECURITY_CLASS, ");
    	sbQuery.append("     X.CURRENCY_ORIGIN ");

		String strQueryFormatted = sbQuery.toString();
		
		if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getServiceCode())){
			strQueryFormatted = strQueryFormatted.replace(":p_service_code", commissionsAnnotationAccountTo.getServiceCode().toString());
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getEntityCode())){
    		strQueryFormatted = strQueryFormatted.replace(":p_entity_code", commissionsAnnotationAccountTo.getEntityCode().toString());
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getCui())){
    		strQueryFormatted = strQueryFormatted.replace(":cui_code", commissionsAnnotationAccountTo.getCui().toString());
    	}
    	
    	return strQueryFormatted;
    }
	
	public String getQueryCommissionsAnnotationAccount(CommissionsAnnotationAccountTo commissionsAnnotationAccountTo){
    	StringBuilder sbQuery= new StringBuilder();
    	
    	sbQuery.append(" select                                                                                                                                      ");
    	sbQuery.append(" '(' || X.SERVICE_CODE || ')' ||                                                                                                             ");
    	sbQuery.append(" X.SERVICE_NAME SERVICIOS,                                                                                                                   ");
    	sbQuery.append(" X.CALCULATION_DATE_STRING FECHA,                                                                                                            ");
    	sbQuery.append(" X.ID_HOLDER_FK CUI,                                                                                                                         ");
    	sbQuery.append(" X.FULL_NAME CUI_DESCRIPCION,                                                                                                                ");
    	sbQuery.append(" X.CLASE_VALOR_NEMONIC CLASE_VALOR,                                                                                                          ");
    	sbQuery.append(" X.CURRENCY_DESCRIPTION MONEDA,                                                                                                              ");
    	sbQuery.append(" SUM(X.NEGOTIATED_AMOUNT_ORIGIN) TOTAL_MONEDA_ORIGEN,                                                                                        ");
    	sbQuery.append(" SUM(X.NEGOTIATED_AMOUNT) TOTAL_EN_USD,                                                                                                      ");
    	sbQuery.append(" X.REFERENCE_RATE CODIGO_TARIFA,                                                                                                             ");
    	sbQuery.append(" X.RATE_PERCENT TARIFA,                                                                                                                      ");
    	sbQuery.append(" SUM(X.RATE_AMOUNT) COMISION_USD,                                                                                                            ");
    	sbQuery.append(" X.EXCHANGE_RATE_CURRENT TIPO_CAMBIO,                                                                                                        ");
    	sbQuery.append(" SUM(X.RATE_AMOUNT_BILLED) COMISION_BS                                                                                                       ");

    	sbQuery.append(" from (select                                                                                                                                ");
    	sbQuery.append(" CRD.ID_COLLECTION_RECORD_DETAIL_PK,                                                                                                         ");
    	sbQuery.append(" CRD.ID_SECURITY_CODE_FK,                                                                                                                    ");
    	sbQuery.append(" BS.REFERENCE_RATE,                                                                                                                          ");
    	sbQuery.append(" TO_NUMBER(BS.SERVICE_CODE,9999) SERVICE_CODE,                                                                                               ");
    	sbQuery.append(" BS.SERVICE_NAME,                                                                                                                            ");
    	sbQuery.append(" BS.ENTITY_COLLECTION,                                                                                                                       ");
    	sbQuery.append(" CASE                                                                                                                                        ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1406      THEN (SELECT ISS.MNEMONIC FROM ISSUER ISS WHERE ISS.ID_ISSUER_PK =CR.ID_ISSUER_FK )                 ");
    	sbQuery.append("                                                                                                                                             ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1407  THEN (SELECT PAR.MNEMONIC FROM PARTICIPANT PAR WHERE PAR.ID_PARTICIPANT_PK= CR.ID_PARTICIPANT_FK )      ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1408       THEN (SELECT HOL.MNEMONIC FROM HOLDER HOL WHERE HOL.ID_HOLDER_PK=CR.ID_HOLDER_FK)                  ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1530       THEN NVL(CR.OTHER_ENTITY_DOCUMENT, ' ')                                                            ");
    	sbQuery.append(" END AS   MNEMONIC ,                                                                                                                         ");
    	sbQuery.append(" CASE                                                                                                                                        ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1406       THEN NVL(CR.ID_ISSUER_FK , ' ' )                                                                   ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1407   THEN  CR.ID_PARTICIPANT_FK || ' '                                                                      ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1408       THEN CR.ID_HOLDER_FK || ' '                                                                        ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1530       THEN NVL(CR.OTHER_ENTITY_DOCUMENT, ' ')                                                            ");
    	sbQuery.append(" END AS   CODE_ENTITY_COLLECTION ,                                                                                                           ");

    	sbQuery.append(" CASE                                                                                                                                        ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1406      THEN (SELECT ISS.BUSINESS_NAME FROM ISSUER ISS WHERE ISS.ID_ISSUER_PK =CR.ID_ISSUER_FK )            ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1407  THEN (SELECT PAR.DESCRIPTION FROM PARTICIPANT PAR WHERE PAR.ID_PARTICIPANT_PK= CR.ID_PARTICIPANT_FK )   ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1408       THEN (SELECT HOL.full_name FROM HOLDER HOL WHERE HOL.ID_HOLDER_PK=CR.ID_HOLDER_FK)                 ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1530       THEN NVL(CR.OTHER_ENTITY_DESCRIPTION, ' ')                                                         ");
    	sbQuery.append(" END AS  NAME_ENTITY_COLLECTION ,                                                                                                            ");
    	sbQuery.append(" trunc(CR.CALCULATION_DATE) CALCULATION_DATE,                                                                                                ");
    	sbQuery.append(" CR.CALCULATION_DATE CALCULATION_DATE_STRING,                                             ");
    	
    	sbQuery.append(" decode (BS.REFERENCE_RATE,'T02',HOL2.ID_HOLDER_PK,HA.RELATED_ACCOUNT) ID_HOLDER_FK,                                                         ");
    	sbQuery.append(" decode (BS.REFERENCE_RATE,'T02',HOL2.full_name,FN_GET_HOLDER_NAME(CRD.ID_HOLDER_ACCOUNT_FK)) FULL_NAME,                                     ");
    	
    	sbQuery.append(" CRD.SECURITY_CLASS,                                                                                                                         ");
    	
    	sbQuery.append(" (SELECT TEXT1 FROM PARAMETER_TABLE                                                                                                          ");
    	sbQuery.append(" WHERE PARAMETER_TABLE_PK=CRD.SECURITY_CLASS) CLASE_VALOR_NEMONIC,                                                                           ");
    	
    	sbQuery.append(" CRD.CURRENCY_ORIGIN,                                                                                                                        ");
    	
    	sbQuery.append(" (SELECT PT.DESCRIPTION FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK=CRD.CURRENCY_ORIGIN) AS CURRENCY_DESCRIPTION,                    ");
    	
    	sbQuery.append(" (SR.RATE_PERCENT*36500) RATE_PERCENT,                                                                                                       ");
    	
    	sbQuery.append(" CRD.RATE_AMOUNT ,                                                                                                                           ");
    	sbQuery.append(" CRD.RATE_AMOUNT_BILLED ,                                                                                                                    ");
    	
    	sbQuery.append(" CRD.EXCHANGE_RATE_CURRENT,                                                                                                                  ");
    	sbQuery.append(" CRD.NEGOTIATED_AMOUNT_ORIGIN,                                                                                                               ");
    	sbQuery.append(" CRD.NEGOTIATED_AMOUNT                                                                                                                       ");
    	
    	sbQuery.append(" FROM PROCESSED_SERVICE PS ");
    	sbQuery.append("     INNER JOIN COLLECTION_RECORD CR ON CR.ID_PROCESSED_SERVICE_FK=PS.ID_PROCESSED_SERVICE_PK ");
    	sbQuery.append("     INNER JOIN COLLECTION_RECORD_DETAIL CRD ON CRD.ID_COLLECTION_RECORD_FK= CR.ID_COLLECTION_RECORD_PK ");
    	sbQuery.append("     INNER JOIN BILLING_SERVICE BS ON BS.ID_BILLING_SERVICE_PK=PS.ID_BILLING_SERVICE_FK ");
    	sbQuery.append("     INNER JOIN SERVICE_RATE SR ON SR.ID_BILLING_SERVICE_FK=BS.ID_BILLING_SERVICE_PK ");
    	sbQuery.append("     LEFT JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK=CRD.ID_HOLDER_ACCOUNT_FK ");
    	sbQuery.append("     LEFT JOIN ISSUER ISS2 ON (ISS2.ID_ISSUER_PK=CRD.ID_ISSUER_FK) ");
    	sbQuery.append("     LEFT JOIN HOLDER HOL2 ON (HOL2.ID_HOLDER_PK=ISS2.ID_HOLDER_FK) ");
    	sbQuery.append("  ");
    	sbQuery.append(" WHERE BS.BASE_COLLECTION IN (253,1412,1800,1801,2136,2137) ");
    	sbQuery.append(" AND trunc(CR.CALCULATION_DATE) BETWEEN to_date('"+commissionsAnnotationAccountTo.getDateInitial()+"','DD/MM/YYYY') AND to_date('"+commissionsAnnotationAccountTo.getDateEnd()+"','DD/MM/YYYY') ");
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getServiceCode())){
    		sbQuery.append(" AND (BS.SERVICE_CODE= :p_service_code ) ");
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getEntityCode())){
    		sbQuery.append(" AND ( ");
    		sbQuery.append(" 	(BS.ENTITY_COLLECTION=1406 AND CR.ID_ISSUER_FK= ':p_entity_code' ) ");
    		sbQuery.append("    OR (BS.ENTITY_COLLECTION=1407 AND CR.ID_PARTICIPANT_FK= :p_entity_code ) ");
    		sbQuery.append("    OR (BS.ENTITY_COLLECTION=1408 AND CR.ID_HOLDER_FK= :p_entity_code ) ");
    		sbQuery.append("    OR (BS.ENTITY_COLLECTION=1409 AND CR.OTHER_ENTITY_DOCUMENT= :p_entity_code )) ");
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getCui())){
    		sbQuery.append(" AND (CRD.ID_HOLDER_FK= :cui_code ) ");
    	}
    	sbQuery.append(" ) X ");
    	sbQuery.append(" GROUP BY                                                                                                                                    ");
    	sbQuery.append(" X.ID_COLLECTION_RECORD_DETAIL_PK,                                                                                                           ");
    	sbQuery.append(" x.ID_SECURITY_CODE_FK,                                                                                                                      ");
    	sbQuery.append(" X.REFERENCE_RATE,                                                                                                                           ");
    	sbQuery.append(" X.SERVICE_CODE,                                                                                                                             ");
    	sbQuery.append(" X.SERVICE_NAME,                                                                                                                             ");
    	sbQuery.append(" X.ENTITY_COLLECTION,                                                                                                                        ");
    	sbQuery.append(" X.MNEMONIC,                                                                                                                                 ");
    	sbQuery.append(" X.CODE_ENTITY_COLLECTION,                                                                                                                   ");
    	sbQuery.append(" X.NAME_ENTITY_COLLECTION,                                                                                                                   ");
    	sbQuery.append("                                                                                                                                             ");
    	sbQuery.append(" X.ID_HOLDER_FK,                                                                                                                             ");
    	sbQuery.append(" X.FULL_NAME, ");
    	sbQuery.append(" X.CALCULATION_DATE, ");
    	sbQuery.append(" X.CALCULATION_DATE_STRING, ");
    	sbQuery.append(" X.SECURITY_CLASS, ");
    	sbQuery.append(" X.CLASE_VALOR_NEMONIC, ");
    	sbQuery.append(" X.CURRENCY_ORIGIN, ");
    	sbQuery.append(" X.CURRENCY_DESCRIPTION, ");
    	sbQuery.append(" X.RATE_PERCENT, ");
    	sbQuery.append(" X.EXCHANGE_RATE_CURRENT ");
    	sbQuery.append(" ORDER BY ");
    	sbQuery.append("     X.REFERENCE_RATE, ");
    	sbQuery.append("     X.SERVICE_CODE, ");
    	sbQuery.append("     X.CODE_ENTITY_COLLECTION, ");
    	sbQuery.append("     X.ID_HOLDER_FK, ");
    	sbQuery.append("     X.CALCULATION_DATE, ");
    	sbQuery.append("     X.SECURITY_CLASS, ");
    	sbQuery.append("     X.CURRENCY_ORIGIN ");

		String strQueryFormatted = sbQuery.toString();
		
		if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getServiceCode())){
			strQueryFormatted = strQueryFormatted.replace(":p_service_code", commissionsAnnotationAccountTo.getServiceCode().toString());
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getEntityCode())){
    		strQueryFormatted = strQueryFormatted.replace(":p_entity_code", commissionsAnnotationAccountTo.getEntityCode().toString());
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getCui())){
    		strQueryFormatted = strQueryFormatted.replace(":cui_code", commissionsAnnotationAccountTo.getCui().toString());
    	}
    	
    	return strQueryFormatted;
    }
	
	public String getQueryCommissionsSummary(CommissionsAnnotationAccountTo commissionsAnnotationAccountTo){
    	StringBuilder sbQuery= new StringBuilder();
    	
    	sbQuery.append(" select ");
    	sbQuery.append(" 	X.ENTITY_COLLECTION, ");
    	sbQuery.append(" 	X.CODE_ENTITY_COLLECTION, ");
    	sbQuery.append(" 	X.MNEMONIC , ");
    	sbQuery.append(" 	X.NAME_ENTITY_COLLECTION, ");
    	sbQuery.append(" 	X.ID_HOLDER_FK, ");
    	sbQuery.append(" 	nvl(X.FULL_NAME,' ') FULL_NAME, ");
    	sbQuery.append(" 	X.REFERENCE_RATE, ");
    	sbQuery.append(" 	X.SERVICE_CODE , ");
    	sbQuery.append(" 	X.SERVICE_NAME, ");
    	sbQuery.append(" 	X.CURRENCY_ORIGIN , ");
    	sbQuery.append(" 	X.CURRENCY_DESCRIPTION, ");
    	sbQuery.append(" 	SUM(X.RATE_AMOUNT_UFV) RATE_AMOUNT_UFV, ");
    	sbQuery.append(" 	SUM(X.RATE_AMOUNT) RATE_AMOUNT, ");
    	sbQuery.append(" 	SUM(X.RATE_AMOUNT_BILLED) RATE_AMOUNT_BILLED ");
    	sbQuery.append(" 	FROM ( ");
    	sbQuery.append(" 	SELECT ");
    	sbQuery.append(" 	BS.ENTITY_COLLECTION , ");
    	sbQuery.append(" 	CASE ");
    	sbQuery.append(" 	WHEN bs.ENTITY_COLLECTION=1406 THEN CR.ID_ISSUER_FK || ' ' ");
    	sbQuery.append(" 	WHEN bs.ENTITY_COLLECTION=1407 THEN  CR.ID_PARTICIPANT_FK || ' ' ");
    	sbQuery.append(" 	WHEN bs.ENTITY_COLLECTION=1408 THEN CR.ID_HOLDER_FK || ' ' ");
    	sbQuery.append(" 	WHEN BS.ENTITY_COLLECTION=1530 THEN NVL(CR.OTHER_ENTITY_DOCUMENT,' ') ");
    	sbQuery.append(" 	ELSE ' ' ");
    	sbQuery.append(" 	END AS CODE_ENTITY_COLLECTION, ");
    	sbQuery.append(" 	CASE ");
    	sbQuery.append(" 	WHEN BS.ENTITY_COLLECTION=1406 	THEN ISS.MNEMONIC ");
    	sbQuery.append(" 	WHEN BS.ENTITY_COLLECTION=1407 	THEN PAR.MNEMONIC ");
    	sbQuery.append(" 	WHEN BS.ENTITY_COLLECTION=1408		 THEN HOL.MNEMONIC ");
    	sbQuery.append(" 	WHEN BS.ENTITY_COLLECTION=1530		 THEN NVL(CR.OTHER_ENTITY_DOCUMENT, ' ') ");
    	sbQuery.append(" 	ELSE ' ' ");
    	sbQuery.append(" 	END AS   MNEMONIC , ");
    	sbQuery.append(" 	CASE ");
    	sbQuery.append(" 	WHEN bs.ENTITY_COLLECTION=1406 THEN  NVL(ISS.BUSINESS_NAME,' ') ");
    	sbQuery.append(" 	WHEN bs.ENTITY_COLLECTION=1407 THEN  NVL(PAR.DESCRIPTION,' ') ");
    	sbQuery.append(" 	WHEN bs.ENTITY_COLLECTION=1408 THEN  NVL(HOL.full_name,' ') ");
    	sbQuery.append(" 	WHEN BS.ENTITY_COLLECTION=1530 THEN  NVL(CR.OTHER_ENTITY_DESCRIPTION,' ') ");
    	sbQuery.append(" 	ELSE ' ' ");
    	sbQuery.append(" 	END AS NAME_ENTITY_COLLECTION, ");
    	sbQuery.append(" 	CASE BS.REFERENCE_RATE ");
    	sbQuery.append(" 	WHEN 'T02' THEN ");
    	sbQuery.append(" 	(SELECT HO1.ID_HOLDER_PK FROM HOLDER HO1 WHERE HO1.ID_HOLDER_PK = ISS2.ID_HOLDER_FK) ");
    	sbQuery.append(" 	WHEN 'T08' THEN ");
    	sbQuery.append(" 	(SELECT HO2.ID_HOLDER_PK FROM HOLDER HO2 WHERE HO2.ID_HOLDER_PK = PAR.ID_HOLDER_FK) ");
    	sbQuery.append(" 	ELSE   HA.RELATED_ACCOUNT ");
    	sbQuery.append(" 	END ID_HOLDER_FK, ");
    	sbQuery.append(" 	CASE BS.REFERENCE_RATE ");
    	sbQuery.append(" 	WHEN 'T02' THEN ");
    	sbQuery.append(" 	(SELECT nvl(HO1.full_name,' ') FROM HOLDER HO1 WHERE HO1.ID_HOLDER_PK = ISS2.ID_HOLDER_FK) ");
    	sbQuery.append(" 	WHEN 'T08' THEN ");
    	sbQuery.append(" 	(SELECT nvl(HO2.full_name,' ') FROM HOLDER HO2 WHERE HO2.ID_HOLDER_PK = PAR.ID_HOLDER_FK) ");
    	sbQuery.append(" 	ELSE    (SELECT LISTAGG(H.ID_HOLDER_PK || ' - '  || H.FULL_NAME,',') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
    	sbQuery.append(" 	 		FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
    	sbQuery.append(" 	 		WHERE HAD.ID_HOLDER_ACCOUNT_FK = CRD.ID_HOLDER_ACCOUNT_FK)");
    	sbQuery.append(" 	END FULL_NAME, ");
    	sbQuery.append(" 	BS.REFERENCE_RATE, ");
    	sbQuery.append(" 	TO_NUMBER(BS.SERVICE_CODE,9999) SERVICE_CODE, ");
    	sbQuery.append(" 	BS.SERVICE_NAME, ");
    	sbQuery.append(" 	CRD.CURRENCY_ORIGIN, ");
    	sbQuery.append(" 	(SELECT PT.DESCRIPTION FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK=CRD.CURRENCY_ORIGIN) AS CURRENCY_DESCRIPTION, ");
    	sbQuery.append(" 	CASE ");
    	sbQuery.append(" 	WHEN BS.CURRENCY_CALCULATION=1734 THEN NVL(CRD.RATE_PERCENT,0) ");
    	sbQuery.append(" 	ELSE 0 ");
    	sbQuery.append(" 	end RATE_AMOUNT_UFV , ");
    	sbQuery.append(" 	CASE ");
    	sbQuery.append(" 	WHEN BS.CURRENCY_CALCULATION=430 THEN  NVL(CRD.RATE_PERCENT,0) ");
    	sbQuery.append(" 	ELSE 0 ");
    	sbQuery.append(" 	END RATE_AMOUNT, ");
    	sbQuery.append(" 	NVL(CRD.RATE_AMOUNT_BILLED,0) RATE_AMOUNT_BILLED ");
    	sbQuery.append(" FROM processed_service ps ");
    	sbQuery.append(" 	INNER JOIN billing_service bs on  ( ps.ID_BILLING_SERVICE_FK = bs.ID_BILLING_SERVICE_PK) ");
    	sbQuery.append(" 	INNER JOIN COLLECTION_RECORD CR ON (PS.ID_PROCESSED_SERVICE_PK= CR.ID_PROCESSED_SERVICE_FK) ");
    	sbQuery.append(" 	INNER JOIN COLLECTION_RECORD_DETAIL CRD ON (CRD.ID_COLLECTION_RECORD_FK= CR.ID_COLLECTION_RECORD_PK) ");
    	sbQuery.append(" 	INNER JOIN parameter_table PT  ON (BS.ENTITY_COLLECTION= PT.parameter_table_pk) ");
    	sbQuery.append(" 	INNER JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK=CRD.ID_HOLDER_ACCOUNT_FK ");
    	sbQuery.append(" 	LEFT JOIN ISSUER ISS ON(ISS.ID_ISSUER_PK=cr.ID_ISSUER_FK ) ");
    	sbQuery.append(" 	LEFT JOIN PARTICIPANT PAR ON (PAR.ID_PARTICIPANT_PK=CR.ID_PARTICIPANT_FK) ");
    	sbQuery.append(" 	LEFT JOIN HOLDER HOL ON (HOL.ID_HOLDER_PK=CR.ID_HOLDER_FK) ");
    	sbQuery.append(" 	LEFT JOIN ISSUER ISS2 ON (ISS2.ID_ISSUER_PK=CRD.ID_ISSUER_FK) ");
    	sbQuery.append(" WHERE 1 = 1 ");
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getTarifaCode())){
    		sbQuery.append(" 	AND (BS.REFERENCE_RATE=:p_tariff_code) ");
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getServiceCode())){
    		sbQuery.append(" 	AND (bs.service_code=:p_service_code) ");
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getIdParticipantPk())){
    		sbQuery.append(" 	AND (bs.ENTITY_COLLECTION=1407 AND CR.ID_PARTICIPANT_FK=:p_participant_code) ");
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getCui())){
    		sbQuery.append(" 	AND (CRD.ID_HOLDER_FK=:cui_code) ");
    	}
    	sbQuery.append(" 	AND  trunc(CR.CALCULATION_DATE) BETWEEN to_date('"+commissionsAnnotationAccountTo.getDateInitial()+"','DD/MM/YYYY') and to_date('"+commissionsAnnotationAccountTo.getDateEnd()+"','DD/MM/YYYY') ");
    	sbQuery.append(" ) X ");
    	sbQuery.append(" GROUP BY ");
    	sbQuery.append(" 	X.ENTITY_COLLECTION, ");
    	sbQuery.append(" 	X.CODE_ENTITY_COLLECTION, ");
    	sbQuery.append(" 	X.MNEMONIC , ");
    	sbQuery.append(" 	X.NAME_ENTITY_COLLECTION, ");
    	sbQuery.append(" 	X.ID_HOLDER_FK, ");
    	sbQuery.append(" 	X.FULL_NAME, ");
    	sbQuery.append(" 	X.REFERENCE_RATE, ");
    	sbQuery.append(" 	X.SERVICE_CODE , ");
    	sbQuery.append(" 	X.SERVICE_NAME, ");
    	sbQuery.append(" 	X.CURRENCY_ORIGIN , ");
    	sbQuery.append(" 	X.CURRENCY_DESCRIPTION ");
    	sbQuery.append(" ORDER BY ");
    	sbQuery.append(" 	X.ENTITY_COLLECTION, ");
    	sbQuery.append(" 	X.MNEMONIC , ");
    	sbQuery.append(" 	X.ID_HOLDER_FK, ");
    	sbQuery.append(" 	X.REFERENCE_RATE, ");
    	sbQuery.append(" 	X.SERVICE_CODE , ");
        sbQuery.append(" 	X.CURRENCY_ORIGIN ");
		
		String strQueryFormatted = sbQuery.toString();
		
		if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getTarifaCode())){
    		strQueryFormatted = strQueryFormatted.replace(":p_tariff_code", commissionsAnnotationAccountTo.getTarifaCode().toString());
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getServiceCode())){
    		strQueryFormatted = strQueryFormatted.replace(":p_service_code", commissionsAnnotationAccountTo.getServiceCode().toString());
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getIdParticipantPk())){
    		strQueryFormatted = strQueryFormatted.replace(":p_participant_code", commissionsAnnotationAccountTo.getIdParticipantPk().toString());
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getCui())){
    		strQueryFormatted = strQueryFormatted.replace(":cui_code", commissionsAnnotationAccountTo.getCui().toString());
    	}
		
    	return strQueryFormatted;
    }
	
	public String getQueryCommissionsIssuanceDPF(CommissionsAnnotationAccountTo commissionsAnnotationAccountTo){
    	StringBuilder sbQuery= new StringBuilder();
    	
    	sbQuery.append(" select ");
    	sbQuery.append(" Z.sell_price, ");
    	sbQuery.append(" BS.REFERENCE_RATE, ");
    	sbQuery.append(" BS.SERVICE_NAME, ");
    	sbQuery.append(" TO_NUMBER(BS.SERVICE_CODE,9999) SERVICE_CODE, ");
    	sbQuery.append(" BS.ENTITY_COLLECTION, ");
    	sbQuery.append(" CASE ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1406      THEN (SELECT ISS.MNEMONIC FROM ISSUER ISS WHERE ISS.ID_ISSUER_PK =CR.ID_ISSUER_FK ) ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1407  THEN (SELECT PAR.MNEMONIC FROM PARTICIPANT PAR WHERE PAR.ID_PARTICIPANT_PK= CR.ID_PARTICIPANT_FK ) ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1408       THEN (SELECT HOL.MNEMONIC FROM HOLDER HOL WHERE HOL.ID_HOLDER_PK=CR.ID_HOLDER_FK) ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1530       THEN NVL(CR.OTHER_ENTITY_DOCUMENT, ' ') ");
    	sbQuery.append(" END AS   MNEMONIC , ");
    	sbQuery.append(" CASE ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1406       THEN NVL(CR.ID_ISSUER_FK , ' ' ) ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1407   THEN  CR.ID_PARTICIPANT_FK || ' ' ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1408       THEN CR.ID_HOLDER_FK || ' ' ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1530       THEN NVL(CR.OTHER_ENTITY_DOCUMENT, ' ') ");
    	sbQuery.append(" END AS   CODE_ENTITY_COLLECTION , ");
    	sbQuery.append(" CASE ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1406      THEN (SELECT ISS.BUSINESS_NAME FROM ISSUER ISS WHERE ISS.ID_ISSUER_PK =CR.ID_ISSUER_FK ) ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1407  THEN (SELECT PAR.DESCRIPTION FROM PARTICIPANT PAR WHERE PAR.ID_PARTICIPANT_PK= CR.ID_PARTICIPANT_FK ) ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1408       THEN (SELECT HOL.full_name FROM HOLDER HOL WHERE HOL.ID_HOLDER_PK=CR.ID_HOLDER_FK) ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1530       THEN NVL(CR.OTHER_ENTITY_DESCRIPTION, ' ') ");
    	sbQuery.append(" END AS  NAME_ENTITY_COLLECTION , ");
    	sbQuery.append(" CRD.SECURITY_CLASS, ");
    	sbQuery.append(" (SELECT TEXT1 FROM PARAMETER_TABLE ");
    	sbQuery.append(" WHERE PARAMETER_TABLE_PK=CRD.SECURITY_CLASS) CLASE_VALOR_NEMONIC, ");
    	sbQuery.append(" (SELECT PT.DESCRIPTION FROM PARAMETER_TABLE PT ");
    	sbQuery.append(" WHERE PT.PARAMETER_TABLE_PK=CRD.CURRENCY_ORIGIN) AS CURRENCY_DESCRIPTION, ");
    	sbQuery.append("  ");
    	sbQuery.append(" CRD.ID_SECURITY_CODE_FK, ");
    	sbQuery.append(" CRD.CURRENCY_ORIGIN, ");
    	sbQuery.append(" CR.CALCULATION_DATE, ");
    	sbQuery.append(" TO_CHAR(CR.CALCULATION_DATE,'DD/MON/YY','NLS_DATE_LANGUAGE = SPANISH') CALCULATION_DATE_STRING, ");
    	sbQuery.append(" nvl(CRD.NEGOTIATED_AMOUNT_ORIGIN,0) NEGOTIATED_AMOUNT_ORIGIN, ");
    	sbQuery.append(" nvl(CRD.NEGOTIATED_AMOUNT,0) NEGOTIATED_AMOUNT, ");
    	sbQuery.append("  ");
    	sbQuery.append("  ");
    	sbQuery.append(" nvl(CR.COLLECTION_AMOUNT,0) COLLECTION_AMOUNT, ");
    	sbQuery.append(" nvl(CRD.EXCHANGE_RATE_CURRENT,0) EXCHANGE_RATE_CURRENT ");
    	sbQuery.append("  ");
    	sbQuery.append(" FROM PROCESSED_SERVICE PS ");
    	sbQuery.append(" INNER JOIN COLLECTION_RECORD CR ON CR.ID_PROCESSED_SERVICE_FK=PS.ID_PROCESSED_SERVICE_PK ");
    	sbQuery.append(" INNER JOIN COLLECTION_RECORD_DETAIL CRD ON CRD.ID_COLLECTION_RECORD_FK= CR.ID_COLLECTION_RECORD_PK ");
    	sbQuery.append(" INNER JOIN BILLING_SERVICE BS ON BS.ID_BILLING_SERVICE_PK=PS.ID_BILLING_SERVICE_FK ");
    	sbQuery.append(" INNER JOIN SERVICE_RATE SR ON SR.ID_BILLING_SERVICE_FK=BS.ID_BILLING_SERVICE_PK ");
    	sbQuery.append(" LEFT JOIN (select X.sell_price,1 UNO from (select DISTINCT sell_price, ID_DAILY_EXCHANGE_PK from DAILY_EXCHANGE_RATES ");
    	sbQuery.append(" where trunc(DATE_RATE)=TRUNC(to_date('"+commissionsAnnotationAccountTo.getDateEnd()+"','DD/MM/YYYY')) ");
    	sbQuery.append(" and ID_CURRENCY=430 AND ID_SOURCE_INFORMATION = 212 ");
    	sbQuery.append(" order by ID_DAILY_EXCHANGE_PK desc) X where rownum<2) Z ON Z.UNO=1 ");
    	sbQuery.append(" WHERE BS.BASE_COLLECTION IN(2201) ");
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getServiceCode())){
    		sbQuery.append(" 	AND (bs.service_code=:p_service_code) ");
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getIssuerCode())){
    		sbQuery.append(" 	AND (bs.ENTITY_COLLECTION=1406 AND CR.ID_ISSUER_FK=:p_issuer_code) ");
    	}
    	sbQuery.append(" AND trunc(CR.CALCULATION_DATE) BETWEEN to_date('"+commissionsAnnotationAccountTo.getDateInitial()+"','DD/MM/YYYY') and to_date('"+commissionsAnnotationAccountTo.getDateEnd()+"','DD/MM/YYYY') ");
    	sbQuery.append(" ORDER BY BS.REFERENCE_RATE, ");
    	sbQuery.append(" TO_NUMBER(BS.SERVICE_CODE,9999) , ");
    	sbQuery.append(" BS.ENTITY_COLLECTION, ");
    	sbQuery.append(" CASE ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1406       THEN NVL(CR.ID_ISSUER_FK , ' ' ) ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1407   	  THEN  CR.ID_PARTICIPANT_FK || ' ' ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1408       THEN CR.ID_HOLDER_FK || ' ' ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1530       THEN NVL(CR.OTHER_ENTITY_DOCUMENT, ' ') ");
    	sbQuery.append(" END, ");
    	sbQuery.append(" CR.CALCULATION_DATE, ");
    	sbQuery.append(" CRD.SECURITY_CLASS ");
		
		String strQueryFormatted = sbQuery.toString();
		
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getServiceCode())){
    		strQueryFormatted = strQueryFormatted.replace(":p_service_code", commissionsAnnotationAccountTo.getServiceCode().toString());
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getIssuerCode())){
    		strQueryFormatted = strQueryFormatted.replace(":p_issuer_code", "'"+commissionsAnnotationAccountTo.getIssuerCode().toString()+"'");
    	}
		
    	return strQueryFormatted;
    }
	
	public String getQueryCommissionsPhysicalCustody(CommissionsAnnotationAccountTo commissionsAnnotationAccountTo){
    	StringBuilder sbQuery= new StringBuilder();
    	
    	sbQuery.append(" select ");
    	sbQuery.append(" X.REFERENCE_RATE, ");
    	sbQuery.append(" X.SERVICE_CODE, ");
    	sbQuery.append(" X.SERVICE_NAME, ");
    	sbQuery.append(" X.ENTITY_COLLECTION, ");
    	sbQuery.append(" X.MNEMONIC, ");
    	sbQuery.append(" X.CODE_ENTITY_COLLECTION, ");
    	sbQuery.append(" X.NAME_ENTITY_COLLECTION, ");
    	sbQuery.append(" X.CALCULATION_DATE, ");
    	sbQuery.append(" X.CALCULATION_DATE_STRING, ");
    	sbQuery.append(" X.ID_HOLDER_FK, ");
    	sbQuery.append(" X.FULL_NAME, ");
    	sbQuery.append(" X.SECURITY_CLASS, ");
    	sbQuery.append(" X.CLASE_VALOR_NEMONIC, ");
    	sbQuery.append(" X.CURRENCY_ORIGIN, ");
    	sbQuery.append(" X.CURRENCY_DESCRIPTION, ");
    	sbQuery.append(" X.RATE_PERCENT, ");
    	sbQuery.append(" X.EXCHANGE_RATE_CURRENT, ");
    	sbQuery.append(" SUM(X.RATE_AMOUNT) RATE_AMOUNT , ");
    	sbQuery.append(" SUM(X.RATE_AMOUNT_BILLED) RATE_AMOUNT_BILLED, ");
    	sbQuery.append(" SUM(X.NEGOTIATED_AMOUNT_ORIGIN) NEGOTIATED_AMOUNT_ORIGIN, ");
    	sbQuery.append(" SUM(X.NEGOTIATED_AMOUNT) NEGOTIATED_AMOUNT ");
    	sbQuery.append(" from (select ");
    	sbQuery.append(" BS.REFERENCE_RATE, ");
    	sbQuery.append(" TO_NUMBER(BS.SERVICE_CODE,9999) SERVICE_CODE, ");
    	sbQuery.append(" BS.SERVICE_NAME, ");
    	sbQuery.append(" BS.ENTITY_COLLECTION, ");
    	sbQuery.append(" CASE ");
    	sbQuery.append(" 	WHEN BS.ENTITY_COLLECTION=1406		THEN (SELECT ISS.MNEMONIC FROM ISSUER ISS WHERE ISS.ID_ISSUER_PK =CR.ID_ISSUER_FK ) ");
    	sbQuery.append(" 	WHEN BS.ENTITY_COLLECTION=1407 	THEN (SELECT PAR.MNEMONIC FROM PARTICIPANT PAR WHERE PAR.ID_PARTICIPANT_PK= CR.ID_PARTICIPANT_FK ) ");
    	sbQuery.append(" 	WHEN BS.ENTITY_COLLECTION=1408		 THEN (SELECT HOL.MNEMONIC FROM HOLDER HOL WHERE HOL.ID_HOLDER_PK=CR.ID_HOLDER_FK) ");
    	sbQuery.append(" 	WHEN BS.ENTITY_COLLECTION=1530		 THEN NVL(CR.OTHER_ENTITY_DOCUMENT, ' ') ");
    	sbQuery.append(" END AS   MNEMONIC , ");
    	sbQuery.append(" CASE ");
    	sbQuery.append(" 	WHEN BS.ENTITY_COLLECTION=1406		 THEN NVL(CR.ID_ISSUER_FK , ' ' ) ");
    	sbQuery.append(" 	WHEN BS.ENTITY_COLLECTION=1407 	 THEN  CR.ID_PARTICIPANT_FK || ' ' ");
    	sbQuery.append(" 	WHEN BS.ENTITY_COLLECTION=1408		 THEN CR.ID_HOLDER_FK || ' ' ");
    	sbQuery.append(" 	WHEN BS.ENTITY_COLLECTION=1530		 THEN NVL(CR.OTHER_ENTITY_DOCUMENT, ' ') ");
    	sbQuery.append(" END AS   CODE_ENTITY_COLLECTION , ");
    	sbQuery.append(" CASE ");
    	sbQuery.append(" 	WHEN BS.ENTITY_COLLECTION=1406		THEN (SELECT ISS.BUSINESS_NAME FROM ISSUER ISS WHERE ISS.ID_ISSUER_PK =CR.ID_ISSUER_FK ) ");
    	sbQuery.append(" 	WHEN BS.ENTITY_COLLECTION=1407 	THEN (SELECT PAR.DESCRIPTION FROM PARTICIPANT PAR WHERE PAR.ID_PARTICIPANT_PK= CR.ID_PARTICIPANT_FK ) ");
    	sbQuery.append(" 	WHEN BS.ENTITY_COLLECTION=1408		 THEN (SELECT HOL.full_name FROM HOLDER HOL WHERE HOL.ID_HOLDER_PK=CR.ID_HOLDER_FK) ");
    	sbQuery.append(" 	WHEN BS.ENTITY_COLLECTION=1530		 THEN NVL(CR.OTHER_ENTITY_DESCRIPTION, ' ') ");
    	sbQuery.append(" END AS  NAME_ENTITY_COLLECTION , ");
    	sbQuery.append(" CR.CALCULATION_DATE, ");
    	sbQuery.append(" TO_CHAR(CR.CALCULATION_DATE,'DD/MON/YY','NLS_DATE_LANGUAGE = SPANISH') CALCULATION_DATE_STRING, ");
    	sbQuery.append(" CRD.SECURITY_CLASS, ");
    	sbQuery.append(" (SELECT TEXT1 FROM PARAMETER_TABLE ");
    	sbQuery.append(" WHERE PARAMETER_TABLE_PK=CRD.SECURITY_CLASS) CLASE_VALOR_NEMONIC, ");
    	sbQuery.append(" CRD.ID_SECURITY_CODE_FK, ");
    	sbQuery.append(" CRD.CURRENCY_ORIGIN, ");
    	sbQuery.append(" (SELECT PT.DESCRIPTION FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK=CRD.CURRENCY_ORIGIN) AS CURRENCY_DESCRIPTION, ");
    	sbQuery.append(" (SR.RATE_PERCENT*36500) RATE_PERCENT,");
    	sbQuery.append(" CRD.RATE_AMOUNT , ");
    	sbQuery.append(" CRD.RATE_AMOUNT_BILLED , ");
    	sbQuery.append(" CRD.EXCHANGE_RATE_CURRENT, ");
    	sbQuery.append(" CRD.NEGOTIATED_AMOUNT_ORIGIN, ");
    	sbQuery.append(" CRD.NEGOTIATED_AMOUNT, ");
    	sbQuery.append(" CRD.ID_HOLDER_FK, ");
    	sbQuery.append(" H.FULL_NAME ");
    	sbQuery.append(" FROM PROCESSED_SERVICE PS ");
    	sbQuery.append(" INNER JOIN COLLECTION_RECORD CR ON CR.ID_PROCESSED_SERVICE_FK=PS.ID_PROCESSED_SERVICE_PK ");
    	sbQuery.append(" INNER JOIN COLLECTION_RECORD_DETAIL CRD ON CRD.ID_COLLECTION_RECORD_FK= CR.ID_COLLECTION_RECORD_PK ");
    	sbQuery.append(" INNER JOIN BILLING_SERVICE BS ON BS.ID_BILLING_SERVICE_PK=PS.ID_BILLING_SERVICE_FK ");
    	sbQuery.append(" INNER JOIN SERVICE_RATE SR ON SR.ID_BILLING_SERVICE_FK=BS.ID_BILLING_SERVICE_PK ");
    	sbQuery.append(" LEFT JOIN HOLDER H ON H.ID_HOLDER_PK=CRD.ID_HOLDER_FK ");
    	sbQuery.append(" WHERE BS.BASE_COLLECTION IN( 1413,1729,1802,1809) ");
    	sbQuery.append("  ");
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getServiceCode())){
    		sbQuery.append(" 	AND (bs.service_code=:p_service_code) ");
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getEntityCode())){
    		sbQuery.append(" AND (");
        	sbQuery.append("    (BS.ENTITY_COLLECTION=1406 AND CR.ID_ISSUER_FK=':p_entity_code') ");
        	sbQuery.append("    OR (BS.ENTITY_COLLECTION=1407 AND CR.ID_PARTICIPANT_FK=:p_entity_code) ");
        	sbQuery.append("    OR (BS.ENTITY_COLLECTION=1408 AND CR.ID_HOLDER_FK=:p_entity_code) ");
        	sbQuery.append("    OR (BS.ENTITY_COLLECTION=1409 AND CR.OTHER_ENTITY_DOCUMENT=:p_entity_code)) ");
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getCui())){
    		sbQuery.append(" 	AND (CRD.ID_HOLDER_FK=:cui_code) ");
    	}
    	sbQuery.append(" AND trunc(CR.CALCULATION_DATE) BETWEEN to_date('"+commissionsAnnotationAccountTo.getDateInitial()+"','DD/MM/YYYY') and to_date('"+commissionsAnnotationAccountTo.getDateEnd()+"','DD/MM/YYYY') ");
    	sbQuery.append(" ) X ");
    	sbQuery.append(" GROUP BY ");
    	sbQuery.append(" X.REFERENCE_RATE, ");
    	sbQuery.append(" X.SERVICE_CODE, ");
    	sbQuery.append(" X.SERVICE_NAME, ");
    	sbQuery.append(" X.ENTITY_COLLECTION, ");
    	sbQuery.append(" X.MNEMONIC, ");
    	sbQuery.append(" X.CODE_ENTITY_COLLECTION, ");
    	sbQuery.append(" X.NAME_ENTITY_COLLECTION, ");
    	sbQuery.append(" X.CALCULATION_DATE, ");
    	sbQuery.append(" X.CALCULATION_DATE_STRING, ");
    	sbQuery.append(" X.ID_HOLDER_FK, ");
    	sbQuery.append(" X.FULL_NAME, ");
    	sbQuery.append(" X.SECURITY_CLASS, ");
    	sbQuery.append(" X.CLASE_VALOR_NEMONIC, ");
    	sbQuery.append(" X.CURRENCY_ORIGIN, ");
    	sbQuery.append(" X.CURRENCY_DESCRIPTION, ");
    	sbQuery.append(" X.RATE_PERCENT, ");
    	sbQuery.append(" X.EXCHANGE_RATE_CURRENT ");
    	sbQuery.append(" ORDER BY ");
    	sbQuery.append(" X.REFERENCE_RATE, ");
    	sbQuery.append(" X.SERVICE_CODE, ");
    	sbQuery.append(" X.CODE_ENTITY_COLLECTION, ");
    	sbQuery.append(" X.CALCULATION_DATE, ");
    	sbQuery.append(" X.ID_HOLDER_FK, ");
    	sbQuery.append(" X.SECURITY_CLASS, ");
    	sbQuery.append(" X.CURRENCY_ORIGIN ");
		
		String strQueryFormatted = sbQuery.toString();
		
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getServiceCode())){
    		strQueryFormatted = strQueryFormatted.replace(":p_service_code", commissionsAnnotationAccountTo.getServiceCode().toString());
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getEntityCode())){
    		strQueryFormatted = strQueryFormatted.replace(":p_entity_code", commissionsAnnotationAccountTo.getEntityCode().toString());
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getCui())){
    		strQueryFormatted = strQueryFormatted.replace(":cui_code", commissionsAnnotationAccountTo.getCui().toString());
    	}
    	return strQueryFormatted;
    	
	}
	
	public String getQueryCommissionsSummaryPortfolio(CommissionsAnnotationAccountTo commissionsAnnotationAccountTo){
    	StringBuilder sbQuery= new StringBuilder();
    	
    	sbQuery.append(" select ");
    	sbQuery.append(" X.ENTITY_COLLECTION, ");
    	sbQuery.append(" X.MNEMONIC, ");
    	sbQuery.append(" X.CODE_ENTITY_COLLECTION, ");
    	sbQuery.append(" X.NAME_ENTITY_COLLECTION, ");
    	sbQuery.append(" X.ID_HOLDER_FK, ");
    	sbQuery.append(" X.FULL_NAME, ");
    	sbQuery.append(" X.SERVICE_CODE, ");
    	sbQuery.append(" X.SERVICE_NAME, ");
    	sbQuery.append(" SUM(X.NEGOTIATED_AMOUNT) NEGOTIATED_AMOUNT , ");
    	sbQuery.append(" SUM(X.RATE_AMOUNT) RATE_AMOUNT, ");
    	sbQuery.append(" SUM(X.RATE_AMOUNT_BILLED) RATE_AMOUNT_BILLED ");
    	sbQuery.append(" from ( ");
    	sbQuery.append(" SELECT ");
    	sbQuery.append(" BS.ENTITY_COLLECTION, ");
    	sbQuery.append(" CASE ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1406 	THEN NVL(ISS.MNEMONIC , ' ' ) ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1407 	THEN NVL(PAR.MNEMONIC , ' ' ) ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1408	THEN NVL(HOL.MNEMONIC , ' ' ) ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1530	THEN NVL(CR.OTHER_ENTITY_DOCUMENT, ' ') ");
    	sbQuery.append("     else ' ' ");
    	sbQuery.append(" END AS   MNEMONIC , ");
    	sbQuery.append(" CASE ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1406       THEN NVL(CR.ID_ISSUER_FK , ' ' ) ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1407   THEN  CR.ID_PARTICIPANT_FK  || ' ' ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1408       THEN CR.ID_HOLDER_FK  || ' ' ");
    	sbQuery.append("     WHEN BS.ENTITY_COLLECTION=1530       THEN NVL(CR.OTHER_ENTITY_DOCUMENT, ' ') ");
    	sbQuery.append(" END AS   CODE_ENTITY_COLLECTION , ");
    	sbQuery.append(" CASE ");
    	sbQuery.append("         WHEN bs.ENTITY_COLLECTION=1406 THEN  NVL(ISS.BUSINESS_NAME,' ') ");
    	sbQuery.append(" 	      WHEN bs.ENTITY_COLLECTION=1407 THEN  NVL(PAR.DESCRIPTION,' ') ");
    	sbQuery.append(" 	      WHEN bs.ENTITY_COLLECTION=1408 THEN  NVL(HOL.full_name,' ') ");
    	sbQuery.append(" 	      WHEN BS.ENTITY_COLLECTION=1530 THEN  NVL(CR.OTHER_ENTITY_DESCRIPTION,' ') ");
    	sbQuery.append("         else ' ' ");
    	sbQuery.append(" END  AS  NAME_ENTITY_COLLECTION , ");
    	sbQuery.append(" decode (BS.REFERENCE_RATE,'T02',ISS.ID_HOLDER_FK,HA.RELATED_ACCOUNT) ID_HOLDER_FK, ");
    	sbQuery.append(" decode (BS.REFERENCE_RATE,'T02',(select full_name from holder where id_holder_pk=ISS.ID_HOLDER_FK and rownum<2),(SELECT LISTAGG(H.ID_HOLDER_PK || ' - '  || H.FULL_NAME,',') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK WHERE HAD.ID_HOLDER_ACCOUNT_FK = CRD.ID_HOLDER_ACCOUNT_FK)) FULL_NAME, ");
    	sbQuery.append(" TO_NUMBER(BS.SERVICE_CODE,9999) SERVICE_CODE, ");
    	sbQuery.append(" BS.SERVICE_NAME, ");
    	sbQuery.append(" CRD.NEGOTIATED_AMOUNT, ");
    	sbQuery.append(" CRD.RATE_AMOUNT, ");
    	sbQuery.append(" CRD.RATE_AMOUNT_BILLED ");
    	sbQuery.append(" FROM PROCESSED_SERVICE PS ");
    	sbQuery.append(" INNER JOIN COLLECTION_RECORD CR ON CR.ID_PROCESSED_SERVICE_FK=PS.ID_PROCESSED_SERVICE_PK ");
    	sbQuery.append(" INNER JOIN COLLECTION_RECORD_DETAIL CRD ON CRD.ID_COLLECTION_RECORD_FK= CR.ID_COLLECTION_RECORD_PK ");
    	sbQuery.append(" INNER JOIN BILLING_SERVICE BS ON BS.ID_BILLING_SERVICE_PK=PS.ID_BILLING_SERVICE_FK ");
    	sbQuery.append(" INNER JOIN SERVICE_RATE SR ON SR.ID_BILLING_SERVICE_FK=BS.ID_BILLING_SERVICE_PK ");
    	sbQuery.append(" LEFT JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK=CRD.ID_HOLDER_ACCOUNT_FK ");
    	sbQuery.append(" LEFT JOIN ISSUER ISS ON(ISS.ID_ISSUER_PK=CR.ID_ISSUER_FK ) ");
    	sbQuery.append(" LEFT JOIN PARTICIPANT PAR ON (PAR.ID_PARTICIPANT_PK=CR.ID_PARTICIPANT_FK) ");
    	sbQuery.append(" LEFT JOIN HOLDER HOL ON (HOL.ID_HOLDER_PK=CR.ID_HOLDER_FK) ");
    	sbQuery.append(" WHERE BS.BASE_COLLECTION IN(2136,2137,1413,253,1800,1412,1801,1413,1809,1729) ");
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getServiceCode())){
    		sbQuery.append(" 	AND (bs.service_code=:p_service_code) ");
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getIdParticipantPk())){
    		sbQuery.append(" 	AND (bs.ENTITY_COLLECTION=1407 AND CR.ID_PARTICIPANT_FK=:p_participant_code) ");
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getCui())){
    		sbQuery.append(" 	AND (CRD.ID_HOLDER_FK=:cui_code) ");
    	}
    	sbQuery.append(" 	 AND trunc(CR.CALCULATION_DATE) BETWEEN to_date('"+commissionsAnnotationAccountTo.getDateInitial()+"','DD/MM/YYYY') and to_date('"+commissionsAnnotationAccountTo.getDateEnd()+"','DD/MM/YYYY') ");
    	sbQuery.append("  ");
    	sbQuery.append(" )X ");
    	sbQuery.append(" GROUP BY ");
    	sbQuery.append(" X.ENTITY_COLLECTION, ");
    	sbQuery.append(" X.MNEMONIC, ");
    	sbQuery.append(" X.CODE_ENTITY_COLLECTION, ");
    	sbQuery.append(" X.NAME_ENTITY_COLLECTION, ");
    	sbQuery.append(" X.ID_HOLDER_FK, ");
    	sbQuery.append(" X.FULL_NAME, ");
    	sbQuery.append(" X.SERVICE_CODE, ");
    	sbQuery.append(" X.SERVICE_NAME ");
    	sbQuery.append(" ORDER BY ");
    	sbQuery.append(" X.ENTITY_COLLECTION, ");
    	sbQuery.append(" X.MNEMONIC, ");
    	sbQuery.append(" X.ID_HOLDER_FK, ");
    	sbQuery.append(" X.SERVICE_CODE , ");
    	sbQuery.append(" X.SERVICE_NAME ");
		
		String strQueryFormatted = sbQuery.toString();
		
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getServiceCode())){
    		strQueryFormatted = strQueryFormatted.replace(":p_service_code", commissionsAnnotationAccountTo.getServiceCode().toString());
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getIdParticipantPk())){
    		strQueryFormatted = strQueryFormatted.replace(":p_participant_code", commissionsAnnotationAccountTo.getIdParticipantPk().toString());
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(commissionsAnnotationAccountTo.getCui())){
    		strQueryFormatted = strQueryFormatted.replace(":cui_code", commissionsAnnotationAccountTo.getCui().toString());
    	}
    	return strQueryFormatted;
	}
	@SuppressWarnings("unchecked")
	public List<String> getIdIssuerPks(Participant participant){
		//select ID_ISSUER_PK, MNEMONIC from issuer where DOCUMENT_NUMBER=(select DOCUMENT_NUMBER from participant where 1=1 and id_participant_pk = 109) and MNEMONIC != 'SMV';
		StringBuilder sbQuery= new StringBuilder();    	
    	sbQuery.append("select ID_ISSUER_PK from issuer ");
    	sbQuery.append( " where DOCUMENT_NUMBER=(select DOCUMENT_NUMBER from participant");
    	sbQuery.append( "  where 1=1 ");
    	sbQuery.append( " and ID_PARTICIPANT_PK = :idParticipantPk)");
    	sbQuery.append( " and MNEMONIC != ':mnemonic'");
    	
    	String strQueryFormatted = sbQuery.toString();
    	if(Validations.validateIsNotNullAndNotEmpty(participant.getIdParticipantPk())){
    		strQueryFormatted = strQueryFormatted.replace(":idParticipantPk",participant.getIdParticipantPk().toString());
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(participant.getIdParticipantPk())){
    		strQueryFormatted = strQueryFormatted.replace(":mnemonic",participant.getMnemonic());
    	}
    	try {
    		Query query = em.createNativeQuery(strQueryFormatted);
    		return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	

	/**
	 * Gets the billing service by service code.		 
	 */
	public List<BillingEconomicActivity> getBillingEconomicActivityByBillingServicePk(Long idBillingServicePk) {
		List<BillingEconomicActivity> listBillingEconomicActivity=null; 
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select distinct bea From BillingEconomicActivity bea ");
		sbQuery.append(" join bea.billingService bs ");
		sbQuery.append(" Where bs.idBillingServicePk = :idBillingServicePk ");		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idBillingServicePk", idBillingServicePk);		
		
		try{
			listBillingEconomicActivity=(List<BillingEconomicActivity>)query.getResultList();
		} catch(NoResultException  nrex){
			nrex.printStackTrace();
		}
		return listBillingEconomicActivity;
	}

	public static Boolean isAfpPortfolioClient(List<Integer> economicActivityList, Integer portfolio ){
		
		if(portfolio!=null && GeneralConstants.ZERO_VALUE_INTEGER.equals(portfolio)) {
			if(economicActivityList.contains(EconomicActivityType.ADMINISTRADORAS_FONDOS_PENSIONES.getCode())) {
				return Boolean.TRUE;
			}
		}
		
		return Boolean.FALSE;
	}

	public static Boolean isBaseCollectionOneAfp(List<Integer> economicActivityList, Integer baseCollection ){
		
		if(baseCollection!=null && (BaseCollectionType.CUSTODY_SECURITIES_PHYSICAL.getCode().equals(baseCollection)||
				BaseCollectionType.RECORD_BOOK_ENTRY_SECURITIES_VARIABLE_INCOME.getCode().equals(baseCollection))) {
			if(economicActivityList.contains(EconomicActivityType.ADMINISTRADORAS_FONDOS_PENSIONES.getCode())) {
				return Boolean.TRUE;
			}
		}
		
		return Boolean.FALSE;
	}
	
	public static List<Integer> getBillingEconomicActivityListInteger(List<BillingEconomicActivity> listBillingEconomicActivityBD){
		List<Integer> listEconomic=new ArrayList<>();
		listEconomic.add(0);//parar evitar que envie lista vacia
		if(Validations.validateListIsNotNullAndNotEmpty(listBillingEconomicActivityBD)) {
			for(BillingEconomicActivity select:listBillingEconomicActivityBD) {
				listEconomic.add(select.getEconomicActivity()!=null?select.getEconomicActivity():null);
				
			}
		}
		
		return listEconomic;
	}
	
	public List<String> getServiceCodeFromCollectionRecord(Map<String,String> parameters){
		List<BillingEconomicActivity> listBillingEconomicActivity=null; 
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append(" Select distinct bs.referenceRate,bs.serviceCode ");
		sbQuery.append(" From CollectionRecord cr ");
		sbQuery.append(" Join cr.processedService ps ");
		sbQuery.append(" Join ps.billingService bs ");
		sbQuery.append(" Where 1=1 ");
		sbQuery.append(" and cr.calculationDate between :date_initial and :date_end ");
		
		if(parameters.containsKey("entity_collection")&& parameters.get("entity_collection")!=null){
			sbQuery.append(" and bs.entityCollection = :entity_collection ");
		}
		if(parameters.containsKey("collection_state")&& parameters.get("collection_state")!=null){
			sbQuery.append(" and cr.recordState = :collection_state ");
		}
		if(parameters.containsKey("issuer")&& parameters.get("issuer")!=null){
			sbQuery.append(" and bs.entityCollection = 1406 and cr.issuer.idIssuerPk = :issuer ");
		}
		if(parameters.containsKey("participant")&& parameters.get("participant")!=null){
			sbQuery.append(" and bs.entityCollection = 1407 and cr.participant.idParticipantPk = :participant ");
		}
		if(parameters.containsKey("holder")&& parameters.get("holder")!=null){
			sbQuery.append(" and bs.entityCollection = 1408 and cr.holder.idHolderPk = :holder ");
		}
		if(parameters.containsKey("other_number")&& parameters.get("other_number")!=null){
			sbQuery.append(" and bs.entityCollection = 1530 and (cr.otherEntityDocument = :other_number or (:other_description is null or cr.otherEntityDescription = :other_description )) ");
		}
		if(parameters.containsKey("afp")&& parameters.get("afp")!=null){
			sbQuery.append(" and bs.entityCollection = 1726 and bs.entityCollection = :afp ");
		}
		sbQuery.append(" order by bs.referenceRate,bs.serviceCode ");
		
		Query query = em.createQuery(sbQuery.toString());
		if(parameters.containsKey("entity_collection")&& parameters.get("entity_collection")!=null)
			query.setParameter("entity_collection", parameters.get("entity_collection"));
		if(parameters.containsKey("collection_state")&& parameters.get("collection_state")!=null)
			query.setParameter("collection_state", parameters.get("collection_state"));
		if(parameters.containsKey("issuer")&& parameters.get("issuer")!=null)
			query.setParameter("issuer", parameters.get("issuer"));
		if(parameters.containsKey("participant")&& parameters.get("participant")!=null)
			query.setParameter("participant", parameters.get("participant"));
		if(parameters.containsKey("holder")&& parameters.get("holder")!=null)
			query.setParameter("holder", parameters.get("holder"));
		if(parameters.containsKey("other_number")&& parameters.get("other_number")!=null){
			query.setParameter("other_number", parameters.get("other_number"));
			query.setParameter("other_description", parameters.get("other_description"));
		}
		if(parameters.containsKey("afp")&& parameters.get("afp")!=null)
			query.setParameter("afp", parameters.get("afp"));
		if(parameters.containsKey("date_initial")&& parameters.get("date_initial")!=null)
			query.setParameter("date_initial",CommonsUtilities.convertStringtoDate(parameters.get("date_initial"),"dd/MM/yyyy"));
		if(parameters.containsKey("date_end")&& parameters.get("date_end")!=null)
			query.setParameter("date_end",CommonsUtilities.convertStringtoDate(parameters.get("date_end"),"dd/MM/yyyy"));
		
		List<Object[]> lstObj = query.getResultList();
		List<String> lstServiceCode = new ArrayList<String>();
		
		for(Object[] obj: lstObj){
			lstServiceCode.add((String)obj[1]);
		}
		return lstServiceCode;
	}
	
}
