package com.pradera.report.generation.executor.settlement;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.model.settlement.SettlementAccountMarketfact;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.settlement.service.SettlementReportServiceBean;
import com.pradera.report.generation.executor.settlement.to.ChainedOperationPendingTO;
import com.pradera.report.generation.executor.settlement.to.RegisterChainedOperationTO;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ChainedOperationPendingReport.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/09/2015
 */
@ReportProcess(name="ChainedOperationPendingReport")
public class ChainedOperationPendingReport extends GenericReport{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The settlement report service bean. */
	@EJB
	private SettlementReportServiceBean settlementReportServiceBean;
	
	/** The code participant. */
	private static int CODE_PARTICIPANT 	= 0;
	
	/** The description participant. */
	private static int DESC_PARTICIPANT 	= 1;
	
	/** The date registry. */
	private static int DATE_REGISTRY	 	= 2;
	
	/** The hour registry. */
	private static int HOUR_REGISTRY 		= 3;
	
	/** The quantity chained. */
	private static int QUANTITY_CHAINED 	= 4;
	
	/** The quantity registry. */
	private static int QUANTITY_REG 		= 5;
	
	/** The quantity confirm. */
	private static int QUANTITY_CONF 		= 6;
	
	/** The quantity cancel. */
	private static int QUANTITY_CANC 		= 7;
	
	/** The type execution. */
	private static int TYPE_EXEC 			= 8;	
	
	/** The not generate. */
	private static String NOT_GENERATE 	= "NO GENERADO";
	
	private List<Long> lstParticipant;
	
	/** The Constant DATE_PATTERN. */
	public static final String DATE_PATTERN = "dd-MM-yyyy";
	
	boolean blOnlyGenerate;
	
	/**
	 * Instantiates a new chained operation pending report.
	 */
	public ChainedOperationPendingReport() {
		
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#generateData(com.pradera.model.report.ReportLogger)
	 */
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub		
		ChainedOperationPendingTO objChainedOperationPendingTO = new ChainedOperationPendingTO();
		lstParticipant = new ArrayList<Long>();
		//Read parameters from DB, all parameters are string type
		for(ReportLoggerDetail loggerDetail : reportLogger.getReportLoggerDetails()) {
			if(loggerDetail.getFilterName().equals(ReportConstant.DATE_INITIAL_PARAM) &&
				Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	) {
				objChainedOperationPendingTO.setStrDateInitial(loggerDetail.getFilterValue());
			}	
			if(loggerDetail.getFilterName().equals(ReportConstant.DATE_END_PARAM) &&
					Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	) {
				objChainedOperationPendingTO.setStrDateEnd(loggerDetail.getFilterValue());
			}
			if(loggerDetail.getFilterName().equals(ReportConstant.PARTICIPANT_PARAM) &&
					Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	) {
				objChainedOperationPendingTO.setParticipant(loggerDetail.getFilterValue());
			}
		}
									
		ByteArrayOutputStream objByteArrayOutputStream = new ByteArrayOutputStream();		
		try {			
			List<Object[]> listChainedOperationPending = settlementReportServiceBean.getListChainedOperationPending(objChainedOperationPendingTO);			
			XMLOutputFactory xof = XMLOutputFactory.newInstance();
			XMLStreamWriter xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(objByteArrayOutputStream));
			xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");
			xmlsw.writeStartElement(ReportConstant.REPORT);			
			createHeaderReport(xmlsw, reportLogger);
			xmlsw.writeStartElement("chained");	
			createTagString(xmlsw, "DATE_INITIAL", objChainedOperationPendingTO.getStrDateInitial());
			createTagString(xmlsw, "DATE_END", objChainedOperationPendingTO.getStrDateEnd());
			if(Validations.validateListIsNotNullAndNotEmpty(listChainedOperationPending)){
				createBodyReport(xmlsw, listChainedOperationPending);
			} else {
				validateChainedHolderOperations(xmlsw, CommonsUtilities.currentDate());
				if(!blOnlyGenerate){
					createEmptyReport(xmlsw);
				}				
			}									
			xmlsw.writeEndElement();
			xmlsw.writeEndElement();
			xmlsw.writeEndDocument();
			xmlsw.flush();
	        xmlsw.close();			
		} catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}		
		return objByteArrayOutputStream;
	}
	
	/**
	 * Creates the body report.
	 *
	 * @param xmlsw the xmlsw
	 * @param listChainedOperationPending the list chained operation pending
	 * @throws XMLStreamException the XML stream exception
	 * @throws ServiceException the service exception
	 */
	public void createBodyReport(XMLStreamWriter xmlsw, List<Object[]> listChainedOperationPending)  throws XMLStreamException, ServiceException {
		String strEmpty = "---";
		String strZero = "0";
		for(Object[] chainedOperationPending : listChainedOperationPending){
			
			if(Validations.validateIsNotNullAndNotEmpty(chainedOperationPending[CODE_PARTICIPANT])) {
				validateChainedHolderOperations(xmlsw, new Long(chainedOperationPending[CODE_PARTICIPANT].toString()),
						chainedOperationPending[DESC_PARTICIPANT].toString(),  CommonsUtilities.currentDate());
			}
						
			xmlsw.writeStartElement("lstchained");
			if(Validations.validateIsNotNullAndNotEmpty(chainedOperationPending[CODE_PARTICIPANT])) {
				createTagString(xmlsw, "CODE_PARTICIPANT",  chainedOperationPending[CODE_PARTICIPANT]);
			} else {
				createTagString(xmlsw, "CODE_PARTICIPANT",  strEmpty);
			}			
			if(Validations.validateIsNotNullAndNotEmpty(chainedOperationPending[DESC_PARTICIPANT])) {
				createTagString(xmlsw, "DESC_PARTICIPANT",  chainedOperationPending[DESC_PARTICIPANT]);
			} else {
				createTagString(xmlsw, "DESC_PARTICIPANT",  strEmpty);
			}			
			if(Validations.validateIsNotNullAndNotEmpty(chainedOperationPending[DATE_REGISTRY])) {
				createTagString(xmlsw, "DATE_REGISTRY", chainedOperationPending[DATE_REGISTRY]);
			} else {
				createTagString(xmlsw, "DATE_REGISTRY",  strEmpty);
			}			
			if(Validations.validateIsNotNullAndNotEmpty(chainedOperationPending[HOUR_REGISTRY])) {
				createTagString(xmlsw, "HOUR_REGISTRY",  chainedOperationPending[HOUR_REGISTRY]);
			} else {
				createTagString(xmlsw, "HOUR_REGISTRY",  strEmpty);
			}			
			if(Validations.validateIsNotNullAndNotEmpty(chainedOperationPending[QUANTITY_CHAINED])) {
				createTagString(xmlsw, "QUANTITY_CHAINED",  chainedOperationPending[QUANTITY_CHAINED]);
			} else {
				createTagString(xmlsw, "QUANTITY_CHAINED",  strZero);
			}			
			if(Validations.validateIsNotNullAndNotEmpty(chainedOperationPending[QUANTITY_REG])) {
				createTagString(xmlsw, "QUANTITY_REG",  chainedOperationPending[QUANTITY_REG]);
			} else {
				createTagString(xmlsw, "QUANTITY_REG",  strZero);
			}			
			if(Validations.validateIsNotNullAndNotEmpty(chainedOperationPending[QUANTITY_CONF])) {
				createTagString(xmlsw, "QUANTITY_CONF",  chainedOperationPending[QUANTITY_CONF]);
			} else {
				createTagString(xmlsw, "QUANTITY_CONF",  strZero);
			}			
			if(Validations.validateIsNotNullAndNotEmpty(chainedOperationPending[QUANTITY_CANC])) {
				createTagString(xmlsw, "QUANTITY_CANC",  chainedOperationPending[QUANTITY_CANC]);
			} else {
				createTagString(xmlsw, "QUANTITY_CANC",  strZero);
			}			
			if(Validations.validateIsNotNullAndNotEmpty(chainedOperationPending[TYPE_EXEC])) {
				createTagString(xmlsw, "TYPE_EXEC",  chainedOperationPending[TYPE_EXEC]);
			} else {
				createTagString(xmlsw, "TYPE_EXEC",  strEmpty);
			}
			xmlsw.writeEndElement();						
			
		}		
		
		validateChainedHolderOperations(xmlsw, CommonsUtilities.currentDate());
		
	}
		
	/**
	 * Creates the empty report.
	 *
	 * @param xmlsw the xmlsw
	 * @throws XMLStreamException the XML stream exception
	 */
	private void createEmptyReport(XMLStreamWriter xmlsw) throws XMLStreamException {		
		xmlsw.writeStartElement("lstchained");
		createTagString(xmlsw, "CODE_PARTICIPANT",  null);
		createTagString(xmlsw, "DESC_PARTICIPANT",  null);
		createTagString(xmlsw, "DATE_REGISTRY",  null);
		createTagString(xmlsw, "HOUR_REGISTRY",  null);
		createTagString(xmlsw, "QUANTITY_CHAINED",  null);
		createTagString(xmlsw, "QUANTITY_REG",  null);
		createTagString(xmlsw, "QUANTITY_CONF",  null);
		createTagString(xmlsw, "QUANTITY_CANC",  null);
		createTagString(xmlsw, "TYPE_EXEC",  null);	
		xmlsw.writeEndElement();
	}
	
	/**
	 * Validate chained holder operations.
	 *
	 * @param xmlsw the xmlsw
	 * @param settlementDate the settlement date
	 * @return the list
	 * @throws XMLStreamException the XML stream exception
	 * @throws ServiceException the service exception
	 */
	public void validateChainedHolderOperations(XMLStreamWriter xmlsw, Date settlementDate) throws XMLStreamException, ServiceException {
		String strEmpty = "---";
		String strZero = "0";
		Integer instrumentType= InstrumentType.FIXED_INCOME.getCode();
		List<Integer> lstIndExtended = new ArrayList<Integer>();
		lstIndExtended.add(BooleanType.YES.getCode());
		lstIndExtended.add(BooleanType.NO.getCode());
		List<Participant> lstParticipants;					
		try {
			lstParticipants = settlementReportServiceBean.getListParticipants(NegotiationMechanismType.BOLSA.getCode(), null, null, null);						
			if(Validations.validateListIsNotNullAndNotEmpty(lstParticipants)){
				for(Participant objParticipant : lstParticipants){
					
					if(Validations.validateListIsNotNullAndNotEmpty(lstParticipant) && 
							lstParticipant.contains(objParticipant.getIdParticipantPk())){
						return;
					}
					
					List<RegisterChainedOperationTO> lstRegisterChainedOperationTO = validateChainedHolderOperations(settlementDate, 
							objParticipant.getIdParticipantPk(), lstIndExtended, instrumentType);
					
					if (Validations.validateListIsNotNullAndNotEmpty(lstRegisterChainedOperationTO)) {	
						blOnlyGenerate = true;
						xmlsw.writeStartElement("lstchained");
						if(Validations.validateIsNotNullAndNotEmpty(objParticipant.getIdParticipantPk())) {
							createTagString(xmlsw, "CODE_PARTICIPANT",  objParticipant.getIdParticipantPk());
						} else {
							createTagString(xmlsw, "CODE_PARTICIPANT",  strEmpty);
						}			
						if(Validations.validateIsNotNullAndNotEmpty(objParticipant.getDescription())) {
							createTagString(xmlsw, "DESC_PARTICIPANT",  objParticipant.getDescription());
						} else {
							createTagString(xmlsw, "DESC_PARTICIPANT",  strEmpty);
						}			
						createTagString(xmlsw, "DATE_REGISTRY", CommonsUtilities.convertDatetoString(settlementDate));			
						createTagString(xmlsw, "HOUR_REGISTRY",  strEmpty);			
						createTagString(xmlsw, "QUANTITY_CHAINED",  lstRegisterChainedOperationTO.size());			
						createTagString(xmlsw, "QUANTITY_REG",  strZero);			
						createTagString(xmlsw, "QUANTITY_CONF",  strZero);			
						createTagString(xmlsw, "QUANTITY_CANC",  strZero);			
						createTagString(xmlsw, "TYPE_EXEC",  NOT_GENERATE);
						xmlsw.writeEndElement();						
					}
				}
			}			
		} catch (ServiceException ex) {
			log.error(ex.getMessage());
			ex.printStackTrace();			
		}			
		
	}
	
	
	/**
	 * Validate chained holder operations.
	 *
	 * @param xmlsw the xmlsw
	 * @param settlementDate the settlement date
	 * @return the list
	 * @throws XMLStreamException the XML stream exception
	 * @throws ServiceException the service exception
	 */
	public void validateChainedHolderOperations(XMLStreamWriter xmlsw, Long lngIdParticipantPk, String descParticipant,  
			Date settlementDate) throws XMLStreamException, ServiceException {		
		String strEmpty = "---";
		String strZero = "0";
		Integer instrumentType= InstrumentType.FIXED_INCOME.getCode();
		List<Integer> lstIndExtended = new ArrayList<Integer>();
		lstIndExtended.add(BooleanType.YES.getCode());
		lstIndExtended.add(BooleanType.NO.getCode());				
		try {
			
			if(lstParticipant.contains(lngIdParticipantPk)){
				return;
			}
			lstParticipant.add(lngIdParticipantPk);
			
			List<RegisterChainedOperationTO> lstRegisterChainedOperationTO = validateChainedHolderOperations(settlementDate, 
					lngIdParticipantPk, lstIndExtended, instrumentType);
			
			if (Validations.validateListIsNotNullAndNotEmpty(lstRegisterChainedOperationTO)) {				
				xmlsw.writeStartElement("lstchained");
				if(Validations.validateIsNotNullAndNotEmpty(lngIdParticipantPk)) {
					createTagString(xmlsw, "CODE_PARTICIPANT",  lngIdParticipantPk);
				} else {
					createTagString(xmlsw, "CODE_PARTICIPANT",  strEmpty);
				}			
				if(Validations.validateIsNotNullAndNotEmpty(descParticipant)) {
					createTagString(xmlsw, "DESC_PARTICIPANT",  descParticipant);
				} else {
					createTagString(xmlsw, "DESC_PARTICIPANT",  strEmpty);
				}			
				createTagString(xmlsw, "DATE_REGISTRY",  CommonsUtilities.convertDatetoString(settlementDate));	
				createTagString(xmlsw, "HOUR_REGISTRY",  strEmpty);			
				createTagString(xmlsw, "QUANTITY_CHAINED",  lstRegisterChainedOperationTO.size());			
				createTagString(xmlsw, "QUANTITY_REG",  strZero);			
				createTagString(xmlsw, "QUANTITY_CONF",  strZero);			
				createTagString(xmlsw, "QUANTITY_CANC",  strZero);			
				createTagString(xmlsw, "TYPE_EXEC",  NOT_GENERATE);
				xmlsw.writeEndElement();						
			}			
		} catch (ServiceException ex) {
			log.error(ex.getMessage());
			ex.printStackTrace();			
		}			
		
	}
	
	/**
	 * Validate the chained holder operations automatic.
	 *
	 * @param settlementDate the settlement date
	 * @param idParticipant the id participant
	 * @param lstIndExtended the lst ind extended
	 * @param instrumentType the instrument type
	 * @return the Map Register Chained Operation TO
	 * @throws ServiceException the service exception
	 */
	public List<RegisterChainedOperationTO> validateChainedHolderOperations(Date settlementDate, Long idParticipant, List<Integer> lstIndExtended, 
														Integer instrumentType) throws ServiceException {
		//we keep in mind the settlementAccountOperation for all participants, holder accounts and securities 
		Map<String,RegisterChainedOperationTO> mpChainedHolderOperation = null;
		List<RegisterChainedOperationTO> lstRegisterChainedOperationTO = null;
		Long idModalityGroup = null, idHolderAccount = null;
		String idSecurityCode = null;
		Integer currency = null;
		String mnemonicPart = null;		
		if(Validations.validateListIsNotNullAndNotEmpty(lstIndExtended)){
			mpChainedHolderOperation = new LinkedHashMap<String,RegisterChainedOperationTO>();
			for(Integer indExtended : lstIndExtended) {
				
				List<Object[]> lstSettlementAccountOperation = settlementReportServiceBean.getLstSettlementAccountOperationByValidateChained(settlementDate, idParticipant, 
						idSecurityCode, idHolderAccount, idModalityGroup, currency, indExtended, instrumentType);
				
				if (Validations.validateListIsNotNullAndNotEmpty(lstSettlementAccountOperation)) {					
					for (Object[] objChainedSettlementOperation : lstSettlementAccountOperation) {
						idSecurityCode = objChainedSettlementOperation[0].toString();
						idParticipant = new Long(objChainedSettlementOperation[1].toString());	
						mnemonicPart = objChainedSettlementOperation[24].toString();
						idHolderAccount = new Long(objChainedSettlementOperation[2].toString());
						idModalityGroup = new Long(objChainedSettlementOperation[10].toString());
						Date marketDate = (Date) objChainedSettlementOperation[3];
						BigDecimal marketRate = new BigDecimal(objChainedSettlementOperation[4].toString());
						currency = new Integer(objChainedSettlementOperation[11].toString());
						BigDecimal stockQuantity = new BigDecimal(objChainedSettlementOperation[5].toString());
						Integer role = new Integer(objChainedSettlementOperation[7].toString());				
						Long idSettlementAccountMarketfact = new Long(objChainedSettlementOperation[9].toString());
						SettlementAccountMarketfact settlementAccountMarketfact= new SettlementAccountMarketfact();
						settlementAccountMarketfact.setIdSettAccountMarketfactPk(idSettlementAccountMarketfact);
						settlementAccountMarketfact.setMarketDate(marketDate);
						settlementAccountMarketfact.setMarketQuantity(stockQuantity);
						settlementAccountMarketfact.setMarketRate(marketRate);
						settlementAccountMarketfact.setChainedQuantity(stockQuantity);				
						String strChainedHolderOperation= idParticipant+"*"+idHolderAccount+"*"+idSecurityCode+"*"+idModalityGroup+"*"+marketDate+"*"+marketRate+"*"+currency;
						//we verify if exists the same combination to be chain holder operation
						if (mpChainedHolderOperation.containsKey(strChainedHolderOperation)) {
							RegisterChainedOperationTO objRegisterChainedOperationTO= mpChainedHolderOperation.get(strChainedHolderOperation);
							if (ComponentConstant.SALE_ROLE.equals(role)) {
								objRegisterChainedOperationTO.setSaleQuantity(objRegisterChainedOperationTO.getSaleQuantity().add(stockQuantity));
								objRegisterChainedOperationTO.getSaleSettlementAccountMarketfact().add(settlementAccountMarketfact);
							} else if (ComponentConstant.PURCHARSE_ROLE.equals(role)) {
								objRegisterChainedOperationTO.setPurchaseQuantity(objRegisterChainedOperationTO.getPurchaseQuantity().add(stockQuantity));
								objRegisterChainedOperationTO.getPurchaseSettlementAccountMarketfact().add(settlementAccountMarketfact);
							}
						} else { 
							//we must create a new object to register
							RegisterChainedOperationTO objRegisterChainedOperationTO= new RegisterChainedOperationTO();
							objRegisterChainedOperationTO.setIndAutomatic(BooleanType.YES.getCode());
							objRegisterChainedOperationTO.getParticipant().setIdParticipantPk(idParticipant);							
							objRegisterChainedOperationTO.getParticipant().setMnemonic(mnemonicPart);							
							objRegisterChainedOperationTO.getHolderAccount().setIdHolderAccountPk(idHolderAccount);
							objRegisterChainedOperationTO.getSecurity().setIdSecurityCodePk(idSecurityCode);
							objRegisterChainedOperationTO.getModalityGroup().setIdModalityGroupPk(idModalityGroup);
							objRegisterChainedOperationTO.setMarketDate(marketDate);
							objRegisterChainedOperationTO.setMarketRate(marketRate);
							objRegisterChainedOperationTO.setSettlementDate(settlementDate);
							objRegisterChainedOperationTO.getCurrency().setParameterTablePk(currency);
							objRegisterChainedOperationTO.getIndExtended().setParameterTablePk(indExtended);
							if (ComponentConstant.SALE_ROLE.equals(role)) {
								objRegisterChainedOperationTO.setSaleQuantity(stockQuantity);
								objRegisterChainedOperationTO.getSaleSettlementAccountMarketfact().add(settlementAccountMarketfact);
							} else if (ComponentConstant.PURCHARSE_ROLE.equals(role)) {
								objRegisterChainedOperationTO.setPurchaseQuantity(stockQuantity);
								objRegisterChainedOperationTO.getPurchaseSettlementAccountMarketfact().add(settlementAccountMarketfact);
							}
							mpChainedHolderOperation.put(strChainedHolderOperation, objRegisterChainedOperationTO);
						}
					}		
					
					if (!mpChainedHolderOperation.isEmpty()) {
						lstRegisterChainedOperationTO = new ArrayList<RegisterChainedOperationTO>();
						List<String> lstKeyChainedHolderOperation= new ArrayList<String>(mpChainedHolderOperation.keySet());
						for (String keyChainedHolderOperation: lstKeyChainedHolderOperation) {
							RegisterChainedOperationTO objRegisterChainedOperationTO = mpChainedHolderOperation.get(keyChainedHolderOperation);
							configureChainedHolderOperation(objRegisterChainedOperationTO);							
							if (objRegisterChainedOperationTO.getPurchaseQuantity().compareTo(BigDecimal.ZERO) > 0 &&
								objRegisterChainedOperationTO.getSaleQuantity().compareTo(BigDecimal.ZERO) > 0) {
								//at this time, we got the correct chain settlement account operations in order to save them
								lstRegisterChainedOperationTO.add(objRegisterChainedOperationTO);
							}
						}
					}
				}				
			}
		}		
		return lstRegisterChainedOperationTO;
	}	
	
	/**
	 * Configure chained holder operation.
	 *
	 * @param objRegisterChainedOperationTO the obj register chained operation to
	 */
	private void configureChainedHolderOperation(RegisterChainedOperationTO objRegisterChainedOperationTO)
	{
		BigDecimal saleQuantity= objRegisterChainedOperationTO.getSaleQuantity();
		BigDecimal purchasQuantity= objRegisterChainedOperationTO.getPurchaseQuantity();
		BigDecimal deltaQuantity= saleQuantity.subtract(purchasQuantity);
		if (deltaQuantity.compareTo(BigDecimal.ZERO) > 0 ) {
			//we must verify the the sale role operations. The list of SettlementAccountOperation is order by stock quantity
			//SettlementAccountOperation objSettlementAccountOperation= objRegisterChainedOperationTO.getSaleSettlementAccountOperation().get(0);
			SettlementAccountMarketfact settlementAccountMarketfact= objRegisterChainedOperationTO.getSaleSettlementAccountMarketfact().get(0);
			BigDecimal stockQuantity= settlementAccountMarketfact.getMarketQuantity();
			if (deltaQuantity.compareTo(stockQuantity) >= 0) {
				//we cannot have settlementAccountOperation with stock quantity greater than deltaQuantity
				objRegisterChainedOperationTO.setSaleQuantity(objRegisterChainedOperationTO.getSaleQuantity().subtract(stockQuantity));
				//objRegisterChainedOperationTO.getSaleSettlementAccountOperation().remove(objSettlementAccountOperation);
				objRegisterChainedOperationTO.getSaleSettlementAccountMarketfact().remove(settlementAccountMarketfact);
				configureChainedHolderOperation(objRegisterChainedOperationTO);
			} else {
				//we'll chain only the rest of stock quantity
				//objSettlementAccountOperation.setChainedQuantity(stockQuantity.subtract(deltaQuantity));
				settlementAccountMarketfact.setChainedQuantity(stockQuantity.subtract(deltaQuantity));
			}
		} else if (deltaQuantity.compareTo(BigDecimal.ZERO) < 0) {
			//we must verify the the purchase role operations. The list of SettlementAccountOperation is order by stock quantity
			deltaQuantity= deltaQuantity.abs();
			//SettlementAccountOperation objSettlementAccountOperation= objRegisterChainedOperationTO.getPurchaseSettlementAccountOperation().get(0);
			SettlementAccountMarketfact settlementAccountMarketfact= objRegisterChainedOperationTO.getPurchaseSettlementAccountMarketfact().get(0);
			BigDecimal stockQuantity= settlementAccountMarketfact.getMarketQuantity();
			if (deltaQuantity.compareTo(stockQuantity) >= 0) {
				//we cannot have settlementAccountOperation with stock quantity greater than deltaQuantity
				objRegisterChainedOperationTO.setPurchaseQuantity(objRegisterChainedOperationTO.getPurchaseQuantity().subtract(stockQuantity));
				//objRegisterChainedOperationTO.getPurchaseSettlementAccountOperation().remove(objSettlementAccountOperation);
				objRegisterChainedOperationTO.getPurchaseSettlementAccountMarketfact().remove(settlementAccountMarketfact);
				configureChainedHolderOperation(objRegisterChainedOperationTO);
			} else {
				//we'll chain only the rest of stock quantity
				//objSettlementAccountOperation.setChainedQuantity(stockQuantity.subtract(deltaQuantity));
				settlementAccountMarketfact.setChainedQuantity(stockQuantity.subtract(deltaQuantity));
			}
		}
	}

}
