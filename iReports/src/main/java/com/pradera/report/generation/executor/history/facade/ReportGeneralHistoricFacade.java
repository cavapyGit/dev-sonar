package com.pradera.report.generation.executor.history.facade;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;


@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ReportGeneralHistoricFacade {
	@EJB
	private ParticipantServiceBean participantServiceBean;
	public static final int CARTERA=217;//Alex
	public static final int TRANFERENCIAS=219;//Javier
	public static final int COLOCACIONES_VENTA_DIRECTA=218;//Javier
	public static final int COLOCACIONES_TESORO_DIRECTO=220;//Oscar
	public static final int BLOQUEOS=5;
	public static final int DESBLOQUEOS=6;
	public static final int LIQUIDACIONES=221;//Oscar
	
	public List<ParameterTable> getListTypesReportHistoric(){
		List<ParameterTable> list= new ArrayList<ParameterTable>();
		ParameterTable parameterTable=new ParameterTable();
		
		parameterTable.setParameterTablePk(CARTERA);
		parameterTable.setDescription("Historicos de Cartera");
		parameterTable.setParameterName("REPORTE DE CARTERA");
		list.add(parameterTable);
		
		parameterTable=new ParameterTable();
		parameterTable.setParameterTablePk(TRANFERENCIAS);
		parameterTable.setDescription("Historicos de Tranferencias");
		parameterTable.setParameterName("REPORTE DE TRANFERENCIAS");
		list.add(parameterTable);
		
		parameterTable=new ParameterTable();
		parameterTable.setParameterTablePk(COLOCACIONES_VENTA_DIRECTA);
		parameterTable.setDescription("Colocaciones de Venta Directa");
		parameterTable.setParameterName("REPORTE DE COLOCACIONES VENTA DIRECTA");
		list.add(parameterTable);
		
		parameterTable=new ParameterTable();
		parameterTable.setParameterTablePk(COLOCACIONES_TESORO_DIRECTO);
		parameterTable.setDescription("Colocaciones Tesoro Directo");
		parameterTable.setParameterName("REPORTE DE COLOCACIONES TESORO DIRECTO");
		list.add(parameterTable);
		
//		parameterTable=new ParameterTable();
//		parameterTable.setParameterTablePk(BLOQUEOS);
//		parameterTable.setDescription("Bloqueos");
//		parameterTable.setParameterName("REPORTE DE BLOQUEOS");
//		list.add(parameterTable);
		
//		parameterTable=new ParameterTable();
//		parameterTable.setParameterTablePk(DESBLOQUEOS);
//		parameterTable.setDescription("Desbloqueos");
//		parameterTable.setParameterName("REPORTE DE DESBLOQUEOS");
//		list.add(parameterTable);
		
		parameterTable=new ParameterTable();
		parameterTable.setParameterTablePk(LIQUIDACIONES);
		parameterTable.setDescription("Liquidaciones");
		parameterTable.setParameterName("REPORTE DE LIQUIDACIONES");
		list.add(parameterTable);
		
		return list;
	}
	/*
	 *when 'C' then 'CAMBIO DE TITULARIDAD'      when 'B' then 'BURSATIL'
		when 'E' then 'EXTRA BURSATIL' 
	 * */
	public List<ParameterTable> getListTransactionTypes(){
		List<ParameterTable> list= new ArrayList<ParameterTable>();
		ParameterTable parameterTable=new ParameterTable();
		
		parameterTable.setParameterTablePk(1);
		parameterTable.setDescription("C");
		parameterTable.setParameterName("CAMBIO DE TITULARIDAD");
		list.add(parameterTable);
		
		parameterTable=new ParameterTable();
		parameterTable.setParameterTablePk(2);
		parameterTable.setDescription("B");
		parameterTable.setParameterName("BURSATIL");
		list.add(parameterTable);
		
		parameterTable=new ParameterTable();
		parameterTable.setParameterTablePk(3);
		parameterTable.setDescription("E");
		parameterTable.setParameterName("EXTRA BURSATIL");
		list.add(parameterTable);
		return list;
	}
	/*cvt compra venta
		rpt reporto
		lqr liquidacion anticipada de reporto
		vcr vencimiento de reporto*/
	public List<ParameterTable> getListNegotiationMode(){
		List<ParameterTable> list= new ArrayList<ParameterTable>();
		ParameterTable parameterTable=new ParameterTable();
		
		parameterTable.setParameterTablePk(1);
		parameterTable.setDescription("CVT");
		parameterTable.setParameterName("COMPRA VENTA");
		list.add(parameterTable);
		
		parameterTable=new ParameterTable();
		parameterTable.setParameterTablePk(2);
		parameterTable.setDescription("RPT");
		parameterTable.setParameterName("REPORTO");
		list.add(parameterTable);
		
		parameterTable=new ParameterTable();
		parameterTable.setParameterTablePk(3);
		parameterTable.setDescription("LQR");
		parameterTable.setParameterName("LIQUIDACION ANTICIPADA DE REPORTO");
		list.add(parameterTable);
		
		parameterTable=new ParameterTable();
		parameterTable.setParameterTablePk(4);
		parameterTable.setDescription("VCR");
		parameterTable.setParameterName("VENCIMIENTO DE REPORTO");
		list.add(parameterTable);
		
		return list;
	}
	public List<ParameterTable> getListClassOperation(){
		List<ParameterTable> list= new ArrayList<ParameterTable>();
		ParameterTable parameterTable=new ParameterTable();
		
		parameterTable.setParameterTablePk(1);
		parameterTable.setDescription("CVT");
		parameterTable.setParameterName("COMPRA VENTA");
		list.add(parameterTable);
		
		parameterTable=new ParameterTable();
		parameterTable.setParameterTablePk(2);
		parameterTable.setDescription("RPT");
		parameterTable.setParameterName("REPORTEO");
		list.add(parameterTable);
				
		return list;
	}
	/*
	 * tipo de liquidacion
		[10:12:31] Oscar: normal
		[10:12:33] Oscar: melor
		[10:12:34] Oscar: melid
		[10:12:39] Oscar: Individual
		[10:12:40] Oscar: Todas
		[10:12:44] Oscar: 1,2,3,4,5*/
	public List<ParameterTable> getListTypeSettlement(){
		List<ParameterTable> list= new ArrayList<ParameterTable>();
		ParameterTable parameterTable=new ParameterTable();
		
		parameterTable.setParameterTablePk(1);
		parameterTable.setParameterName("NORMAL");
		list.add(parameterTable);
		
		parameterTable=new ParameterTable();
		parameterTable.setParameterTablePk(2);
		parameterTable.setParameterName("MELOR");
		list.add(parameterTable);
		
		parameterTable=new ParameterTable();
		parameterTable.setParameterTablePk(2);
		parameterTable.setParameterName("MELID");
		list.add(parameterTable);
		
		parameterTable=new ParameterTable();
		parameterTable.setParameterTablePk(2);
		parameterTable.setParameterName("INDIVIDUAL");
		list.add(parameterTable);
		
		return list;
	}
	public List<Participant> getLisParticipantServiceBean(Participant filter)
			throws ServiceException {
		return participantServiceBean.getLisParticipantServiceBean(filter);
	}
}
