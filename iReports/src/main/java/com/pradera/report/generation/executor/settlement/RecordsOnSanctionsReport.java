package com.pradera.report.generation.executor.settlement;

import java.io.ByteArrayOutputStream;
import java.text.MessageFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounts.Participant;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.settlement.service.SettlementReportServiceBean;

@ReportProcess(name="RecordsOnSanctionsReport")
public class RecordsOnSanctionsReport extends GenericReport{
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private ParticipantServiceBean participantServiceBean;
	
	@EJB
	private SettlementReportServiceBean settlementReportServiceBean;
	
	/** The message configuration. */
	@Inject
	@InjectableResource(location = "GenericConstants.properties")
	private Properties messageConfiguration;
	
	@Inject PraderaLogger log;
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String,Object> parametersRequired = new HashMap<>();
		
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		
		String messagePenaltyOperation = messageConfiguration.getProperty("message.sanction.penalty.operation");
		String messageResolutionsLast = messageConfiguration.getProperty("message.sanction.resolution.last");
		String messagePenalty = messageConfiguration.getProperty("message.sanction.penalty");
		
		String idParticipantPenalty=null;
		String idParticipant=null;
		String strParticipantDesc=null;
		String strSettDate=null;
		String penaltyAmount = null;
		String penaltyMotive = null;
		String penaltyMotiveOther = null;
		String penaltyLevel = null;
		String ballotPenalty = null;
		String amountLetters=null;
		String levelLetters=null;
		String romanLevelLetters=null;
		String strPenaltyMotive=null;
		String strPenalty=null;
		String strPenaltyOperation=null;
		String strResolutionNumber=null;
		String strSettlementDate=null;
		String strIssuanceDate=null;
		
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals("idParticipantPenalty") && r.getFilterValue()!=null){
				idParticipantPenalty = r.getFilterValue();
			}	
		}
		
		List<Object[]>  lstResult=settlementReportServiceBean.getBallotPartPenalty(idParticipantPenalty);
		for(Object[] obj:lstResult){
			idParticipantPenalty=obj[0].toString();
			strSettDate=CommonsUtilities.convertDateToString((Date)obj[1], "DD/MMM/YYYY");
			penaltyAmount=obj[2].toString();
			penaltyMotive=obj[3].toString();
			if(Validations.validateIsNotNullAndNotEmpty(obj[4])){
				penaltyMotiveOther=obj[4].toString();
			}
			penaltyLevel=obj[5].toString();
			idParticipant=obj[6].toString();
			ballotPenalty=obj[7].toString();
			strResolutionNumber=obj[8].toString();
			strSettlementDate=obj[9].toString();
			strIssuanceDate=obj[10].toString();
		}
		
		switch (penaltyLevel){
		case "1":
			levelLetters ="Primer Nivel:";
			romanLevelLetters="I";
			break;
		case "2":
			levelLetters ="Segundo Nivel:";
			romanLevelLetters="II";
			break;
		case "3":
			levelLetters ="Tercer Nivel:";
			romanLevelLetters="III";
			break;
		case "4":
			levelLetters ="Cuarto Nivel:";
			romanLevelLetters="IV";
			break;
		case "5":
			levelLetters ="Quinto Nivel:";
			romanLevelLetters="V";
			break;
		default:
			levelLetters ="";
			romanLevelLetters="";
			break;
		}
		
		switch (penaltyAmount){
		case "0":
			amountLetters ="Cero";
			levelLetters ="";
			romanLevelLetters="";
			break;
		case "50":
			amountLetters ="Cincuenta";
			break;
		case "100":
			amountLetters ="Cien";
			break;
		case "150":
			amountLetters ="Ciento Cincuenta";
			break;
		case "200":
			amountLetters ="Doscientos";
			break;
		case "250":
			amountLetters ="Doscientos Ciuncuenta";
			break;
		default:
			amountLetters ="";
		}


		
		switch (penaltyMotive){
		case "1824"://melor x fondos
			strPenaltyMotive ="Mecanismo de Liquidaci\u00F3n de Operaciones Retrasadas - MELOR, debido a que fueron retiradas de la Segunda Etapa de Liquidaci\u00F3n por falta de fondos.";
			break;
		case "1825"://melor x valores
			strPenaltyMotive ="Mecanismo de Liquidaci\u00F3n de Operaciones Retrasadas - MELOR, debido a que fueron retiradas de la Segunda Etapa de Liquidaci\u00F3n por falta de Valores.";
			break;
		case "1826"://Melid x fondos
			strPenaltyMotive ="";
			break;
		case "1827"://Melid x valores
			strPenaltyMotive ="Tercer Nivel";
			break;
		case "1828"://INCUMPLIMIENTO POR FONDOS
			strPenaltyMotive ="Cuarto Nivel";
			break;
		case "1829"://INCUMPLIMIENTO POR VALORES
			strPenaltyMotive ="Quinto Nivel";
			break;
		case "1834"://OTROS
			strPenaltyMotive ="Segundo Nivel";
			break;
		}
		
		Participant partSanctions= participantServiceBean.find(Participant.class, new Long(idParticipant));		
		strParticipantDesc=partSanctions.getDescription();

		//Resuelve		
		strPenaltyOperation=MessageFormat.format(messagePenaltyOperation,strIssuanceDate,strParticipantDesc,ballotPenalty);
		
		StringBuilder iterationMotive=new StringBuilder();
		lstResult=settlementReportServiceBean.getHistorySanctionsByParticipant(new Long(idParticipant),idParticipantPenalty);
		//Iterar		
		for(Object[] obj:lstResult){
			String messageTemp="";
			messageTemp=MessageFormat.format(messageResolutionsLast,strParticipantDesc,obj[0],obj[1],obj[2]);
			iterationMotive.append(messageTemp).append("\n");
		}
		
		strPenalty=MessageFormat.format(messagePenalty ,penaltyAmount,amountLetters,strParticipantDesc,levelLetters);  
		
		parametersRequired.put(ReportConstant.LEVEL_SANCTIONS, levelLetters);
		parametersRequired.put(ReportConstant.STR_LEVEL_SANCTIONS, romanLevelLetters.concat(GeneralConstants.DOT).concat(levelLetters));
		parametersRequired.put(ReportConstant.MOTIVE_SANCTIONS, strPenaltyMotive);
		parametersRequired.put(ReportConstant.AMOUNT_SANCTIONS, "$us. "+ penaltyAmount);
		parametersRequired.put(ReportConstant.PART_SANCTIONS, strParticipantDesc);
		parametersRequired.put(ReportConstant.STR_PENALTY_OPE, strPenaltyOperation);
		parametersRequired.put(ReportConstant.STR_PENALTY, strPenalty);
		parametersRequired.put(ReportConstant.STR_RESOLUTION_NUMBER, strResolutionNumber);
		parametersRequired.put(ReportConstant.STR_DATE_TIME, strSettlementDate);
		parametersRequired.put(ReportConstant.STR_ITERATION_MOTIVE, iterationMotive.toString());
		parametersRequired.put(ReportConstant.STR_PENALTY_LEVEL, penaltyLevel);
		
		
		return parametersRequired;
	}
}
