package com.pradera.report.generation.executor.custody.to;

import java.io.Serializable;

public class GenericCustodyOperationTO implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String participant;
	private String holder;
	private String holderAccount;
	private String issuer;
	private String securities;
	private String securityClass;
	private String currency;
	private String blockType;
	private String operationType;
	private String state;
	private String initialDate;
	private String finalDate;
	private String institutionType;
	
	public String getParticipant() {
		return participant;
	}
	public void setParticipant(String participant) {
		this.participant = participant;
	}
	public String getHolder() {
		return holder;
	}
	public void setHolder(String holder) {
		this.holder = holder;
	}
	public String getHolderAccount() {
		return holderAccount;
	}
	public void setHolderAccount(String holderAccount) {
		this.holderAccount = holderAccount;
	}
	public String getIssuer() {
		return issuer;
	}
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	public String getSecurities() {
		return securities;
	}
	public void setSecurities(String securities) {
		this.securities = securities;
	}
	public String getBlockType() {
		return blockType;
	}
	public void setBlockType(String blockType) {
		this.blockType = blockType;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getInitialDate() {
		return initialDate;
	}
	public void setInitialDate(String initialDate) {
		this.initialDate = initialDate;
	}
	public String getFinalDate() {
		return finalDate;
	}
	public void setFinalDate(String finalDate) {
		this.finalDate = finalDate;
	}
	public String getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(String securityClass) {
		this.securityClass = securityClass;
	}
	public String getOperationType() {
		return operationType;
	}
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	/**
	 * @return the institutionType
	 */
	public String getInstitutionType() {
		return institutionType;
	}
	/**
	 * @param institutionType the institutionType to set
	 */
	public void setInstitutionType(String institutionType) {
		this.institutionType = institutionType;
	}
	
}
