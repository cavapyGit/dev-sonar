package com.pradera.report.generation.executor.billing;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.model.report.ReportLogger;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;

/**
 * 
 * @author rlarico
 *
 */
@ReportProcess(name = "ConsolidatedCollectionByEntityBcbReport")
public class ConsolidatedCollectionByEntityBcbReport extends GenericReport {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The parameter service. */
	@EJB
	private ParameterServiceBean parameterService;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	
	/**
	 * Instantiates a new consolidated collection by entity report.
	 */
	public ConsolidatedCollectionByEntityBcbReport() {
	}

	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#generateData(com.pradera.model.report.ReportLogger)
	 */
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
 
	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#addParametersQueryReport()
	 */
	@Override
	public void addParametersQueryReport() {
		

	}

	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#getCustomJasperParameters()
	 */
	@Override
	public Map<String, Object> getCustomJasperParameters() {

		Map<String, Object> parametersRequired = new HashMap<>();

		// TODO Auto-generated method stub
		return parametersRequired;
	}
}
