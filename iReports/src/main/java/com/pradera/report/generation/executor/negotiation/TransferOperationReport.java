package com.pradera.report.generation.executor.negotiation;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.custody.to.TransferOperationTO;
import com.pradera.report.generation.executor.negotiation.service.NegotiationReportServiceBean;


@ReportProcess(name="TransferOperationReport")
public class TransferOperationReport extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private NegotiationReportServiceBean negotiationReportServiceBean;
	
	@Inject  PraderaLogger log; 
	
	@EJB
	private ParameterServiceBean parameterService;
	
//	@EJB
//	ParticipantServiceBean participantServiceBean;

//	@EJB
//	private IssuerQueryServiceBean issuerQueryServiceBean;

	@EJB
	private HolderAccountComponentServiceBean accountComponentServiceBean;
	
	public TransferOperationReport() {
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		return null;
	}
	@Override
	public void addParametersQueryReport() {
	}
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String,Object> parametersRequired = new HashMap<>();
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		
		TransferOperationTO transferOperationTO = new TransferOperationTO();
		
		String	issuers              = null;
		String  alternateCodes 		= null;
		String	participant         = null;
		String	cuiHolder          = null;
		String	holderAccount      = null;
		String	securityClass      = null;
		String	security            = null;
		String	currencyOperation  = null;
		String  modality		   = null;
		String	dateInitial        = null;
		String	dateEnd            = null;
		String operation = null;
		
		
		// DATOS DE ENTRADA DEL SISTEMA PARA EL QUERY Y PARA LOS FILTROS
		for (ReportLoggerDetail r : loggerDetails) {
			
			if(Validations.validateIsNotNull(r.getFilterValue())){
				if (r.getFilterName().equals("issuer")){
					issuers = r.getFilterValue();
					transferOperationTO.setIssuer(issuers); 
				}
					
				if (r.getFilterName().equals("participant")){
					participant = r.getFilterValue();
					transferOperationTO.setParticipant(Long.valueOf(participant));
				}
					
				if (r.getFilterName().equals("cui_code")){
					cuiHolder = r.getFilterValue();
					transferOperationTO.setCuiHolder(Integer.valueOf(cuiHolder));
				}
					
				if (r.getFilterName().equals("holder_account")){
					holderAccount = r.getFilterValue();
					transferOperationTO.setHolderAccount(Integer.valueOf(holderAccount));
				}
					
				if (r.getFilterName().equals("security_class")){
					securityClass = r.getFilterValue();
					transferOperationTO.setSecurityClass(Integer.valueOf(securityClass));
				}
					
				if (r.getFilterName().equals("security")){
					security = r.getFilterValue();
					transferOperationTO.setSecurity(security);
				}
					
				if (r.getFilterName().equals("currency_operation")){
					currencyOperation = r.getFilterValue();
					transferOperationTO.setCurrencyOperation(Integer.valueOf(currencyOperation));
				}
					
				if (r.getFilterName().equals("modality")){
					modality = r.getFilterValue();
					transferOperationTO.setModality(Integer.valueOf(modality));	
				}
					
				if (r.getFilterName().equals("alternate_code")){
					alternateCodes = r.getFilterValue();
					transferOperationTO.setAlternateCode(alternateCodes);	
				}
				
				if (r.getFilterName().equals("operation")){
					operation = r.getFilterValue();
					transferOperationTO.setOperation(Integer.valueOf(operation));	
				}
					
				if (r.getFilterName().equals("date_initial")){
					dateInitial = r.getFilterValue();
					transferOperationTO.setStrDateInitial(dateInitial);	
				}
					
				if (r.getFilterName().equals("date_end")){
					dateEnd = r.getFilterValue();
					transferOperationTO.setStrDateEnd(dateEnd);		
				}
			}
		}
		
		/* PARA ENVIAR Al REPORTE COMO FILTROS*/
		
		try {

			if(issuers != null){ 
				issuers = accountComponentServiceBean.find(new String(issuers),Issuer.class).getMnemonic().toString() + 
							" - " + accountComponentServiceBean.find(new String(issuers),Issuer.class).getBusinessName().toString();
			} else{ issuers = "TODOS"; }
			
			if(participant != null){
				participant = accountComponentServiceBean.find(new Long(participant),Participant.class).getMnemonic().toString() + 
					     		" - " + accountComponentServiceBean.find(new Long(participant),Participant.class).getIdParticipantPk().toString() +
					     			" - " + accountComponentServiceBean.find(new Long(participant),Participant.class).getDescription().toString();
			} else{ participant = "TODOS"; }
			
			if(cuiHolder != null){
				cuiHolder = accountComponentServiceBean.find(new Long(cuiHolder),Holder.class).getIdHolderPk().toString() +
							" - " + accountComponentServiceBean.find(new Long(cuiHolder),Holder.class).getFullName().toString();
			} else{ cuiHolder = "TODOS"; }
			
			if(holderAccount != null){
				holderAccount = accountComponentServiceBean.find(new Long(holderAccount),HolderAccount.class).getAccountNumber().toString();
			} else{ holderAccount = "TODOS"; }
			
			if(securityClass != null){
				ParameterTable parameterClass = parameterService.getParameterTableById(Integer.parseInt(securityClass));
				securityClass = parameterClass.getText1() + " - " + parameterClass.getParameterName();
			} else{  securityClass = "TODOS"; }
				
			if(security == null){ security = "TODOS"; }
			
			if(currencyOperation != null){
				ParameterTable parameterClass = parameterService.getParameterTableById(Integer.parseInt(currencyOperation));
				currencyOperation = parameterClass.getText1() + " - " + parameterClass.getParameterName();
			} else{ currencyOperation = "TODOS"; }
			
			if(operation!=null){
				Integer operationType = Integer.valueOf(operation);
				if(operationType.equals(GeneralConstants.ZERO_VALUE_INTEGER))
					operation = "TODOS";
				if(operationType.equals(GeneralConstants.ONE_VALUE_INTEGER))
					operation = "BURSATILES";
				if(operationType.equals(GeneralConstants.THREE_VALUE_INTEGER))
					operation = "EXTRABURSATILES";
			}
			else{
				operation = "TODOS";
			}
			
		} catch (Exception e) {
			
		}
		
		String strQuery = null;
		
		strQuery = negotiationReportServiceBean.getQueryTransferOPeration(transferOperationTO);
		
		parametersRequired.put("str_query", strQuery);
		
		parametersRequired.put("cui_holder", cuiHolder);
		parametersRequired.put("holder_account", holderAccount);
		parametersRequired.put("issuer", issuers);
		parametersRequired.put("participant", participant);
		parametersRequired.put("security_class", securityClass);
		parametersRequired.put("security", security);
		parametersRequired.put("currency_operation", currencyOperation);
		parametersRequired.put("operation_type", operation);
		parametersRequired.put("date_initial", dateInitial);
		parametersRequired.put("date_end", dateEnd);

		return parametersRequired;
	}
}
