package com.pradera.report.generation.executor.custody.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.pradera.model.issuancesecuritie.Security;

public class RetirementOperationTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long idRetirementOperationPk; 

	private String idIsinCodeFk;
	
	private Long idParticipantFk;

	private Integer motive;

	private Integer state;
	
	private Integer accountNumber;
	
	private Date initialDate;

    private Date finalDate;
    
    private Long idHolderPk;
    
    /** The security. */
	private Security security;
    
    /** The security class. */
	private Integer securityClass;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The operation number. */
	private Long operationNumber;
	
	/** The ind withdrawal bcb. */
	private Integer indWithdrawalBCB;
	
	/** The isin code pk. */
	private String idSecurityCodePk;
	
	/** The total balance. */
	private BigDecimal totalBalance;
	
	/** The state type description. */
	private String stateDescription;
	
	/** The motive type description. */
	private String motiveDescription;
	
	/** The id security bc bcode. */
	private String idSecurityBCBcode;
	
	public Long getIdRetirementOperationPk() {
		return idRetirementOperationPk;
	}

	public void setIdRetirementOperationPk(Long idRetirementOperationPk) {
		this.idRetirementOperationPk = idRetirementOperationPk;
	}

	public String getIdIsinCodeFk() {
		return idIsinCodeFk;
	}

	public void setIdIsinCodeFk(String idIsinCodeFk) {
		this.idIsinCodeFk = idIsinCodeFk;
	}

	public Long getIdParticipantFk() {
		return idParticipantFk;
	}

	public void setIdParticipantFk(Long idParticipantFk) {
		this.idParticipantFk = idParticipantFk;
	}

	public Integer getMotive() {
		return motive;
	}

	public void setMotive(Integer motive) {
		this.motive = motive;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	public Long getIdHolderPk() {
		return idHolderPk;
	}

	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}

	public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public Integer getSecurityClass() {
		return securityClass;
	}

	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	public Long getOperationNumber() {
		return operationNumber;
	}

	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}

	public Integer getIndWithdrawalBCB() {
		return indWithdrawalBCB;
	}

	public void setIndWithdrawalBCB(Integer indWithdrawalBCB) {
		this.indWithdrawalBCB = indWithdrawalBCB;
	}

	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	public BigDecimal getTotalBalance() {
		return totalBalance;
	}

	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	public String getStateDescription() {
		return stateDescription;
	}

	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}

	public String getMotiveDescription() {
		return motiveDescription;
	}

	public void setMotiveDescription(String motiveDescription) {
		this.motiveDescription = motiveDescription;
	}

	public String getIdSecurityBCBcode() {
		return idSecurityBCBcode;
	}

	public void setIdSecurityBCBcode(String idSecurityBCBcode) {
		this.idSecurityBCBcode = idSecurityBCBcode;
	}
	
}
