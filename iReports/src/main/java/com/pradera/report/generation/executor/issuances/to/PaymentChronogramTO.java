package com.pradera.report.generation.executor.issuances.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PaymentChronogramTO implements Serializable{
	private static final long serialVersionUID = 1L;
	 
	private Date date;
	private Date dateForName;
	private String custodian;
	private Date registryDt;
	private Date dateInformation;
	private Integer securityClass;
	private String idSecurityOnly;
	private Integer couponNumberInt;
	private Date expirationDtInt;
	private BigDecimal interestFactor;
	private Integer couponNumberAmort;
	private Date expirationDtAmort;
	private BigDecimal amortizationFactor;
	private BigDecimal totalCoupon;
	private String couponType;
	private String validCoupon;
	private String strCouponNumbers;
	private String registryDate;
	private String expirationDate;
	
	//For chronogram of coupons
	private String strState;
	private String strEconomicActivity;
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getCustodian() {
		return custodian;
	}
	public void setCustodian(String custodian) {
		this.custodian = custodian;
	}
	public Date getRegistryDt() {
		return registryDt;
	}
	public void setRegistryDt(Date registryDt) {
		this.registryDt = registryDt;
	}
	public Integer getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}
	public String getIdSecurityOnly() {
		return idSecurityOnly;
	}
	public void setIdSecurityOnly(String idSecurityOnly) {
		this.idSecurityOnly = idSecurityOnly;
	}
	public Integer getCouponNumberInt() {
		return couponNumberInt;
	}
	public void setCouponNumberInt(Integer couponNumberInt) {
		this.couponNumberInt = couponNumberInt;
	}
	public Date getExpirationDtInt() {
		return expirationDtInt;
	}
	public void setExpirationDtInt(Date expirationDtInt) {
		this.expirationDtInt = expirationDtInt;
	}
	public BigDecimal getInterestFactor() {
		return interestFactor;
	}
	public void setInterestFactor(BigDecimal interestFactor) {
		this.interestFactor = interestFactor;
	}
	public Integer getCouponNumberAmort() {
		return couponNumberAmort;
	}
	public void setCouponNumberAmort(Integer couponNumberAmort) {
		this.couponNumberAmort = couponNumberAmort;
	}
	public Date getExpirationDtAmort() {
		return expirationDtAmort;
	}
	public void setExpirationDtAmort(Date expirationDtAmort) {
		this.expirationDtAmort = expirationDtAmort;
	}
	public BigDecimal getAmortizationFactor() {
		return amortizationFactor;
	}
	public void setAmortizationFactor(BigDecimal amortizationFactor) {
		this.amortizationFactor = amortizationFactor;
	}
	public BigDecimal getTotalCoupon() {
		return totalCoupon;
	}
	public void setTotalCoupon(BigDecimal totalCoupon) {
		this.totalCoupon = totalCoupon;
	}
	public String getCouponType() {
		return couponType;
	}
	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}
	public String getValidCoupon() {
		return validCoupon;
	}
	public void setValidCoupon(String validCoupon) {
		this.validCoupon = validCoupon;
	}
	public String getStrCouponNumbers() {
		return strCouponNumbers;
	}
	public void setStrCouponNumbers(String strCouponNumbers) {
		this.strCouponNumbers = strCouponNumbers;
	}
	public String getStrState() {
		return strState;
	}
	public void setStrState(String strState) {
		this.strState = strState;
	}
	public String getStrEconomicActivity() {
		return strEconomicActivity;
	}
	public void setStrEconomicActivity(String strEconomicActivity) {
		this.strEconomicActivity = strEconomicActivity;
	}
	public String getRegistryDate() {
		return registryDate;
	}
	public void setRegistryDate(String registryDate) {
		this.registryDate = registryDate;
	}
	public String getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
	public Date getDateForName() {
		return dateForName;
	}
	public void setDateForName(Date dateForName) {
		this.dateForName = dateForName;
	}
	public Date getDateInformation() {
		return dateInformation;
	}
	public void setDateInformation(Date dateInformation) {
		this.dateInformation = dateInformation;
	}
}
