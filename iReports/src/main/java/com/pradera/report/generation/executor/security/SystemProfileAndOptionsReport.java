package com.pradera.report.generation.executor.security;

import java.io.ByteArrayOutputStream;
import java.util.List;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.pradera.commons.services.remote.security.SystemSecurityMgmtService;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.security.to.SecurityTO;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class SystemProfileAndOptionsReport.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/07/2013
 */
@ReportProcess(name="SystemProfileAndOptionsReport")
public class SystemProfileAndOptionsReport extends GenericReport{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7316500141655854627L;
	
	
	@Inject 
	Instance<SystemSecurityMgmtService>   reportRemote;
	
	/**
	 * Instantiates a new SystemProfileAndOptionsReport
	 */
	public SystemProfileAndOptionsReport() {
		// TODO Auto-generated constructor stub
	}

	
	
	
	 @Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		
			boolean existData = false;
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			try {
				
				SecurityTO securityTO = new SecurityTO();
				securityTO  = reportLoggerToSecurityTo(reportLogger);
			    List<Object[]> profilesAndOptions = reportRemote.get().getSystemProfileAndOptions(securityTO.getSystem(),
			    									securityTO.getMnemonic(), securityTO.getState());
			
			if(!profilesAndOptions.isEmpty()) {
				existData = true;
				
			}
			XMLOutputFactory xof = XMLOutputFactory.newInstance();
			XMLStreamWriter xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(baos));
			//open document file
			xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");
			xmlsw.writeStartElement(ReportConstant.REPORT);
			createBodyReport(xmlsw, profilesAndOptions);
			xmlsw.writeEndElement();
			//close document
			xmlsw.writeEndDocument();
			xmlsw.flush();
	        xmlsw.close();
			}catch(Exception ex){
				//log.error(ex.getMessage());
				ex.printStackTrace();
				throw new RuntimeException();
			}

		return baos;
	}

	 /**
		 * Creates the body report.
		 * good practice factoring
		 * @param xmlsw the xmlsw
		 * @param stateDescription the state description
		 * @param documentTypeDescription the document type description
		 * @param classDescription the class description
		 * @param participants the participants
		 * @throws XMLStreamException the xML stream exception
		 */

		public void createBodyReport(XMLStreamWriter xmlsw,List<Object[]> profiles)  throws XMLStreamException{
			
			for(Object[] profile : profiles) {
				xmlsw.writeStartElement("report-data");
				createTagString(xmlsw, "system-name", profile[0].toString().toUpperCase());
				createTagString(xmlsw, "profile", profile[1].toString().toUpperCase());
		        createTagString(xmlsw, "register-date", profile[2].toString());
				createTagString(xmlsw, "module", profile[3].toString().toUpperCase());
				createTagString(xmlsw, "sub-module", profile[4].toString().toUpperCase());
				createTagString(xmlsw, "optionId", profile[5].toString());
				createTagString(xmlsw, "option", profile[6].toString().toUpperCase());
				createTagString(xmlsw, "privilege", profile[7].toString().toUpperCase());
			    xmlsw.writeEndElement();
			}
		}
	 
	 
	 
	 private SecurityTO reportLoggerToSecurityTo(ReportLogger reportLogger){
			
	     SecurityTO objecto = new SecurityTO();
		
		for (ReportLoggerDetail detail : reportLogger.getReportLoggerDetails()) {

			if( detail.getFilterName().equals(ReportConstant.SYSTEM) ){
				if(Validations.validateIsNotNull(detail.getFilterValue())){
					objecto.setSystem(new Integer(detail.getFilterValue().toString()));
				}
			}else if( detail.getFilterName().equals(ReportConstant.MNEMONIC) ){
				if(Validations.validateIsNotNull(detail.getFilterValue())){
					objecto.setMnemonic(detail.getFilterValue().toString());
				}
			}else if( detail.getFilterName().equals(ReportConstant.STATE_PARAM) ){
				if(Validations.validateIsNotNull(detail.getFilterValue())){
					objecto.setState(new Integer(detail.getFilterValue().toString()));
				}
			}

		}
		
		return objecto;
	}
}
