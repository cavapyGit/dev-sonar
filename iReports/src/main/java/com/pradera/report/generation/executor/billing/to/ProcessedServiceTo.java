package com.pradera.report.generation.executor.billing.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ProcessedServiceTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class ProcessedServiceTo implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id processed service pk. */
	private Long idProcessedServicePk;

	/** The calculation date. */
	private Date calculationDate;

	/** The operation date. */
	private Date operationDate;

	/** The id daily exchange fk. */
	private Integer idDailyExchangeFk;
	
	/** The month process. */
	private Integer monthProcess;

	/** The processed state. */
	private Integer processedState;

	/** The year process. */
	private Integer yearProcess;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/** The billing service to. */
	private BillingServiceTo billingServiceTo;

	/** The collection records to. */
	private List<CollectionRecordTo> collectionRecordsTo;
	
	/** The collection record to search. */
	private CollectionRecordTo collectionRecordToSearch;
	
	/** The description processed state. */
	private String descriptionProcessedState;
	
	/** The number of collections. */
	private Integer numberOfCollections;

	/**
	 * Gets the billing service to.
	 *
	 * @return the billing service to
	 */
	public BillingServiceTo getBillingServiceTo() {
		return billingServiceTo;
	}

	/**
	 * Sets the billing service to.
	 *
	 * @param billingServiceTo the new billing service to
	 */
	public void setBillingServiceTo(BillingServiceTo billingServiceTo) {
		this.billingServiceTo = billingServiceTo;
	}

	/**
	 * Gets the id processed service pk.
	 *
	 * @return the id processed service pk
	 */
	public Long getIdProcessedServicePk() {
		return idProcessedServicePk;
	}

	/**
	 * Sets the id processed service pk.
	 *
	 * @param idProcessedServicePk the new id processed service pk
	 */
	public void setIdProcessedServicePk(Long idProcessedServicePk) {
		this.idProcessedServicePk = idProcessedServicePk;
	}

	/**
	 * Gets the calculation date.
	 *
	 * @return the calculation date
	 */
	public Date getCalculationDate() {
		return calculationDate;
	}

	/**
	 * Sets the calculation date.
	 *
	 * @param calculationDate the new calculation date
	 */
	public void setCalculationDate(Date calculationDate) {
		this.calculationDate = calculationDate;
	}

	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return operationDate;
	}

	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	/**
	 * Gets the id daily exchange fk.
	 *
	 * @return the id daily exchange fk
	 */
	public Integer getIdDailyExchangeFk() {
		return idDailyExchangeFk;
	}

	/**
	 * Sets the id daily exchange fk.
	 *
	 * @param idDailyExchangeFk the new id daily exchange fk
	 */
	public void setIdDailyExchangeFk(Integer idDailyExchangeFk) {
		this.idDailyExchangeFk = idDailyExchangeFk;
	}

	/**
	 * Gets the month process.
	 *
	 * @return the month process
	 */
	public Integer getMonthProcess() {
		return monthProcess;
	}

	/**
	 * Sets the month process.
	 *
	 * @param monthProcess the new month process
	 */
	public void setMonthProcess(Integer monthProcess) {
		this.monthProcess = monthProcess;
	}

	/**
	 * Gets the processed state.
	 *
	 * @return the processed state
	 */
	public Integer getProcessedState() {
		return processedState;
	}

	/**
	 * Sets the processed state.
	 *
	 * @param processedState the new processed state
	 */
	public void setProcessedState(Integer processedState) {
		this.processedState = processedState;
	}

	/**
	 * Gets the year process.
	 *
	 * @return the year process
	 */
	public Integer getYearProcess() {
		return yearProcess;
	}

	/**
	 * Sets the year process.
	 *
	 * @param yearProcess the new year process
	 */
	public void setYearProcess(Integer yearProcess) {
		this.yearProcess = yearProcess;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	/**
	 * Gets the collection records to.
	 *
	 * @return the collection records to
	 */
	public List<CollectionRecordTo> getCollectionRecordsTo() {
		return collectionRecordsTo;
	}

	/**
	 * Sets the collection records to.
	 *
	 * @param collectionRecordsTo the new collection records to
	 */
	public void setCollectionRecordsTo(List<CollectionRecordTo> collectionRecordsTo) {
		this.collectionRecordsTo = collectionRecordsTo;
	}

	/**
	 * Gets the collection record to search.
	 *
	 * @return the collection record to search
	 */
	public CollectionRecordTo getCollectionRecordToSearch() {
		return collectionRecordToSearch;
	}

	/**
	 * Sets the collection record to search.
	 *
	 * @param collectionRecordToSearch the new collection record to search
	 */
	public void setCollectionRecordToSearch(
			CollectionRecordTo collectionRecordToSearch) {
		this.collectionRecordToSearch = collectionRecordToSearch;
	}

	/**
	 * Gets the description processed state.
	 *
	 * @return the description processed state
	 */
	public String getDescriptionProcessedState() {
		return descriptionProcessedState;
	}

	/**
	 * Sets the description processed state.
	 *
	 * @param descriptionProcessedState the new description processed state
	 */
	public void setDescriptionProcessedState(String descriptionProcessedState) {
		this.descriptionProcessedState = descriptionProcessedState;
	}

	/**
	 * Gets the number of collections.
	 *
	 * @return the number of collections
	 */
	public Integer getNumberOfCollections() {
		return numberOfCollections;
	}

	/**
	 * Sets the number of collections.
	 *
	 * @param numberOfCollections the new number of collections
	 */
	public void setNumberOfCollections(Integer numberOfCollections) {
		this.numberOfCollections = numberOfCollections;
	}
	
	

}
