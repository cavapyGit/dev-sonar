package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.to.VaultControlTO;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.executor.custody.service.VaultControlReportServiceBean;
import com.pradera.report.generation.executor.custody.to.VaultControlReportPositionsTO;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name="VaultControlReportPositions")
public class VaultControlReportPositions extends GenericReport{
	
	private static final long serialVersionUID = 1L;
	
	@Inject  PraderaLogger log;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	private VaultControlReportServiceBean vaultControlReportServiceBean;

	public VaultControlReportPositions() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void addParametersQueryReport() {

	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		VaultControlReportPositionsTO vaultControlReportPositionsTO = new VaultControlReportPositionsTO();
		Map<Integer,String> p_currency = new HashMap<Integer,String>();
		Map<Integer,String> dematerializationMotive = new HashMap<Integer, String>();
		Map<Integer,String> securityClass = new HashMap<Integer, String>();
		Boolean excelDelimited  = false;
		
		for(int i=0; i<listaLogger.size(); i++) {
			
			if(listaLogger.get(i).getFilterName().equals("date_initial")) {
				if(listaLogger.get(i).getFilterValue() != null) {
					vaultControlReportPositionsTO.setInitialDate(CommonsUtilities.convertStringtoDate(listaLogger.get(i).getFilterValue()));
					parametersRequired.put("date_initial", listaLogger.get(i).getFilterValue());
						
				}
			}
			
			if(listaLogger.get(i).getFilterName().equals("date_end")) {
				if(listaLogger.get(i).getFilterValue() != null) {
					vaultControlReportPositionsTO.setFinalDate(CommonsUtilities.convertStringtoDate(listaLogger.get(i).getFilterValue()));
					parametersRequired.put("date_end", listaLogger.get(i).getFilterValue());	
				}
			}
			
			if(listaLogger.get(i).getFilterName().equals("issuer")) {
				if(listaLogger.get(i).getFilterValue() != null) {
					vaultControlReportPositionsTO.setEmisor(listaLogger.get(i).getFilterValue());
					parametersRequired.put("p_emisor", listaLogger.get(i).getFilterValue());
				}
			}
			
			if(listaLogger.get(i).getFilterName().equals("securityCode")) {
				if(listaLogger.get(i).getFilterValue() != null) {
					vaultControlReportPositionsTO.setValor(listaLogger.get(i).getFilterValue());
					parametersRequired.put("p_valor", listaLogger.get(i).getFilterValue());
				}
			}
			
			if(listaLogger.get(i).getFilterName().equals("MONEDA")) {
				if(listaLogger.get(i).getFilterValue() != null) {
					vaultControlReportPositionsTO.setMoneda(Integer.valueOf(listaLogger.get(i).getFilterValue()));
					parametersRequired.put("p_moneda", listaLogger.get(i).getFilterDescription());
				}
			}
			
			if(listaLogger.get(i).getFilterName().equals("participant")) {
				if(listaLogger.get(i).getFilterValue() != null) {
					vaultControlReportPositionsTO.setParticipant(Integer.valueOf(listaLogger.get(i).getFilterValue()));
					parametersRequired.put("p_participant", listaLogger.get(i).getFilterDescription());
				}
			}
			
			if(listaLogger.get(i).getFilterName().equals("cui_code")) {
				if(listaLogger.get(i).getFilterValue() != null) {
					vaultControlReportPositionsTO.setHolder(Integer.valueOf(listaLogger.get(i).getFilterValue()));
					parametersRequired.put("p_holder", listaLogger.get(i).getFilterDescription());
				}
			}
			
			if(listaLogger.get(i).getFilterName().equals("reportFormats")) {
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()) &&
						Integer.valueOf(listaLogger.get(i).getFilterValue()) == 2853) {
					excelDelimited = true;
				}
			}
			
			if(listaLogger.get(i).getFilterName().equals("dateToFilter")) {
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())) {
					vaultControlReportPositionsTO.setDateFilterType(listaLogger.get(i).getFilterValue());
				}
			}
			
			if(listaLogger.get(i).getFilterName().equals("court_date")) {
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())) {
					vaultControlReportPositionsTO.setCourtDate(CommonsUtilities.convertStringtoDate(listaLogger.get(i).getFilterValue()));
					parametersRequired.put("court_date", listaLogger.get(i).getFilterValue());	
				}
			}
			
		}
		
		try {
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				p_currency.put(param.getParameterTablePk(), "(" + param.getDescription() + ")" + param.getParameterName());
			}
			filter.setMasterTableFk(MasterTableType.DEMATERIALIZATION_MOTIVE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				dematerializationMotive.put(param.getParameterTablePk(), param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				securityClass.put(param.getParameterTablePk(), "(" + param.getText1() + ")" + param.getDescription());
			}
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		
		String strQuery = vaultControlReportServiceBean.queryVaultControlReportPositions(vaultControlReportPositionsTO);
		String strQueryFormatted = vaultControlReportServiceBean.queryFormatForPositionsJasper(vaultControlReportPositionsTO, strQuery);
		
		parametersRequired.put("str_query", strQueryFormatted);
		parametersRequired.put("p_currency", p_currency);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		parametersRequired.put("p_securityClass", securityClass);
		parametersRequired.put("p_dematerializationMotive", dematerializationMotive);
		
		if(excelDelimited) {
			parametersRequired.put(ReportConstant.REPORT_EXCEL_DELIMITADO, vaultControlReportServiceBean.generateExcelDelimited(parametersRequired, vaultControlReportPositionsTO));
		}
		
		return parametersRequired;
		
	}

}
