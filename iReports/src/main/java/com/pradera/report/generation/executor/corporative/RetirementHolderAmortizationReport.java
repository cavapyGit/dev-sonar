package com.pradera.report.generation.executor.corporative;

import java.io.ByteArrayOutputStream;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.corporateevents.to.CorporativeOperationTO;
import com.pradera.core.component.helperui.service.SecuritiesQueryServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.corporative.service.CorporativeReportServiceBean;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

@ReportProcess(name="RetirementHolderAmortizationReport")
public class RetirementHolderAmortizationReport extends GenericReport{

	private static final long serialVersionUID = 1L;	
	private static int ACCOUNT_NUMBER = 0;
	private static int PARTICIPANT = 1;
	private static int HOLDER = 2;
	private static int REGISTRY_DATE = 3;
	private static int PENALITY_RATE = 4;
	private static int ROUND = 5;
	private static int ID_HOLDER_ACCOUNT_PK = 6;
	private static int TOTAL_BALANCE = 7;
	private static int ID_PARTICIPANT_PK = 8;
	private static int GROSS_AMORTIZATION = 9;
	private static int PENALITY_AMOUNT = 10;
	private static int TOTAL_AMOUNT = 11;	
	private static int STATE = 12;
	private static int HOLDER_COUNT = 13;	
	
	/** The corporative report service bean. */
	@EJB
	CorporativeReportServiceBean corporativeReportServiceBean;
	
	/** The securities query service bean. */
	@EJB
	SecuritiesQueryServiceBean securitiesQueryServiceBean;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		boolean existData = false;
		CorporativeOperationTO corporativeOpTO = new CorporativeOperationTO();
		
		//Read parameters from DB, all parameters are string type
		for(ReportLoggerDetail loggerDetail : reportLogger.getReportLoggerDetails()) {			
			if(Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())
					&& ReportConstant.CORP_PROCESS_ID.equals(loggerDetail.getFilterName())){
				corporativeOpTO.setId(Long.valueOf(loggerDetail.getFilterValue()));
			}else if(Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())
						&& loggerDetail.getFilterName().equals(ReportConstant.CORPORATE_EVENT_TYPE)){
				corporativeOpTO.setCorporativeEventType(Integer.valueOf(loggerDetail.getFilterValue()));
			}else if(Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())
						&& loggerDetail.getFilterName().equals(ReportConstant.REPORT_TYPE)){
				corporativeOpTO.setReportType(Integer.valueOf(loggerDetail.getFilterValue()));
			}else if(Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())	
						&& loggerDetail.getFilterName().equals(ReportConstant.STATE_PARAM)){
				corporativeOpTO.setState(Integer.valueOf(loggerDetail.getFilterValue()));
			}else if(Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())
						&& loggerDetail.getFilterName().equals(ReportConstant.DEST_CODE_ISIN)){
				corporativeOpTO.setTargetIsinCode(loggerDetail.getFilterValue());
			}else if(Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())
						&& loggerDetail.getFilterName().equals(ReportConstant.CUTOFF_DATE)){
				corporativeOpTO.setCutOffDate(CommonsUtilities.convertStringtoDate(loggerDetail.getFilterValue()));
			}else if(Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())
						&& loggerDetail.getFilterName().equals(ReportConstant.DELIVERY_DATE)){
				corporativeOpTO.setDeliveryDate(CommonsUtilities.convertStringtoDate(loggerDetail.getFilterValue()));
			}else if(Validations.validateIsNotNullAndNotEmpty(loggerDetail.getFilterValue())
						&& loggerDetail.getFilterName().equals(ReportConstant.CODE_ISIN)){
				corporativeOpTO.setSourceIsinCode(loggerDetail.getFilterValue());
			}
		}
				
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
				
		try {
			//GETTING ALL DATA FROM QUERY
			List<Object[]> listObjects = corporativeReportServiceBean.getRetirementAmortization(corporativeOpTO);
			Security sec = securitiesQueryServiceBean.getSecurityHelpServiceBean(corporativeOpTO.getSourceIsinCode());
			if(Validations.validateListIsNotNullAndNotEmpty(listObjects))
				existData = true;
			
			XMLOutputFactory xof = XMLOutputFactory.newInstance();
			XMLStreamWriter xmlsw;
			xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(baos));
			//open document file
			xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");
			//REPORT TAG
			xmlsw.writeStartElement(ReportConstant.REPORT);
					
			//HEADER
			createHeaderReport(xmlsw,reportLogger);	
			createTagString(xmlsw, "VALOR", sec.getIdSecurityCodePk() + GeneralConstants.DASH + sec.getDescription());
			createTagString(xmlsw, "EMISOR", sec.getIssuer().getIdIssuerPk() + GeneralConstants.DASH + sec.getIssuer().getBusinessName());
			createTagString(xmlsw, "CUTOFF_DATE", CommonsUtilities.convertDateToString(corporativeOpTO.getCutOffDate(), GeneralConstants.DATE_FORMAT_PATTERN));
			createTagString(xmlsw, "VALOR_NOMINAL", sec.getCurrentNominalValue());
			createTagString(xmlsw, "ID_ISSUANCE_CODE_FK", sec.getIssuance().getIdIssuanceCodePk());
			createTagString(xmlsw, "CURRENCY", CurrencyType.get(sec.getCurrency()).getValue());
			createTagString(xmlsw, "DELIVERY_DATE", CommonsUtilities.convertDateToString(corporativeOpTO.getDeliveryDate(), GeneralConstants.DATE_FORMAT_PATTERN));
			//BODY
			if(existData){
				xmlsw.writeStartElement("participant"); //START HEADER REPORT ELEMENT	
				createTagString(xmlsw, "PENALIDAD", listObjects.get(0)[PENALITY_RATE].toString());
				createTagString(xmlsw, "REGISTRY_DATE", listObjects.get(0)[REGISTRY_DATE].toString());							
				createTagString(xmlsw, "IND_ROUND", listObjects.get(0)[ROUND].toString());
				createBodyReport(xmlsw, listObjects);
				xmlsw.writeEndElement(); //END HEADER REPORT ELEMENT					
			}
			//END REPORT TAG
			xmlsw.writeEndElement();
			//close document
			xmlsw.writeEndDocument();
			xmlsw.flush();
			xmlsw.close();
		} catch (XMLStreamException | ServiceException e) {
			log.error(e.getMessage());
			e.printStackTrace();
			throw new RuntimeException();
		}					
	 return baos;
	}
	
	public void createBodyReport(XMLStreamWriter xmlsw,List<Object[]> listObjects)  throws XMLStreamException{
		xmlsw.writeStartElement("rows"); //START ROWS TAG		
		//BY ROW
		for(Object[] obj : listObjects){
			xmlsw.writeStartElement("row"); //START ROW TAG			
			createTagString(xmlsw, "CUENTA", obj[ACCOUNT_NUMBER]);
			createTagString(xmlsw, "ID_PARTICIPANT_FK", obj[PARTICIPANT]);
			createTagString(xmlsw, "RNT_DESCRIPCIÓN", obj[HOLDER]);
			createTagString(xmlsw, "ID_HOLDER_ACCOUNT_FK", obj[ID_HOLDER_ACCOUNT_PK]);
			createTagString(xmlsw, "SALDO_CONTABLE", obj[TOTAL_BALANCE]);
			createTagString(xmlsw, "SALDO_DISPONIBLE", obj[TOTAL_BALANCE]);
			createTagString(xmlsw, "participant_id", obj[ID_PARTICIPANT_PK]);
			createTagString(xmlsw, "AMORTIZACIÓN_BRUTA", obj[GROSS_AMORTIZATION]);
			createTagString(xmlsw, "PENALITY_AMOUNT", obj[PENALITY_AMOUNT]);
			createTagString(xmlsw, "AMORTIZACION_NETA_CONTABLE", obj[TOTAL_AMOUNT]);
			createTagString(xmlsw, "AMORTIZACION_NETA_DISPONIBLE", obj[TOTAL_AMOUNT]);
			createTagString(xmlsw, "STATE", obj[STATE]);			
			createTagString(xmlsw, "HOLDER_COUNT", obj[HOLDER_COUNT]);			
			xmlsw.writeEndElement();//END ROW TAG
		}		
		xmlsw.writeEndElement();//END ROWS TAG
	}

}
