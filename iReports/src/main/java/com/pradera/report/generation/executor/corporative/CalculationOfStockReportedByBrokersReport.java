package com.pradera.report.generation.executor.corporative;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.management.RuntimeErrorException;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.corporative.service.CorporativeReportServiceBean;
import com.pradera.report.generation.executor.corporative.to.GenericCorporativesTO;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;

@ReportProcess(name = "CalculationOfStockReportedByBrokersReport")
public class CalculationOfStockReportedByBrokersReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	private CorporativeReportServiceBean corporativeReportServiceBean;

	@Inject
	private PraderaLogger log;
	
	@EJB
	private CustodyReportServiceBean custodyReportServiceBean;

	public CalculationOfStockReportedByBrokersReport() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {

		GenericCorporativesTO gfto = new GenericCorporativesTO();
		Long idStkCalcProcess= null;
		Date cutdate = new Date();
		Date regdate = new Date();

		for(ReportLoggerDetail param: getReportLogger().getReportLoggerDetails()){
			if(param.getFilterName().equals("security_class_key")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setSecurities(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("date_initial")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					cutdate = CommonsUtilities.convertStringtoDate(param.getFilterValue().toString());
					gfto.setCutDate(CommonsUtilities.convertDateToString(cutdate, "dd/MM/yy"));
				}
			}
			if(param.getFilterName().equals("date_end")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					regdate = CommonsUtilities.convertStringtoDate(param.getFilterValue().toString());
					gfto.setRegistryDate(CommonsUtilities.convertDateToString(regdate, "dd/MM/yy"));
				}
			}
		}
		
		String date = CommonsUtilities.convertDatetoString(cutdate);
		try {
			if(cutdate.before(CommonsUtilities.currentDate())){
				idStkCalcProcess = custodyReportServiceBean.searchIdStockCalculationProcess(cutdate);
			}
		} catch (ServiceException e) {
			if(e.getErrorService().equals(ErrorServiceType.STOCK_CALCULATION_PROCESS_NOT_EXIST)){
				throw new RuntimeErrorException(null, ReportConstant.ERROR_STOCK_CALCULATION_PROCESS_NOT_EXIST + " " + date);
			}
			log.error(e.getMessage());
		}
		
		String strQuery = corporativeReportServiceBean.getQueryCalculationOfStockReportedByBrokersReport(gfto, idStkCalcProcess); 
		 
		Map<String,Object> parametersRequired = new HashMap<>();

		parametersRequired.put("str_Query", strQuery);
		parametersRequired.put("initialDt", gfto.getCutDate());
		parametersRequired.put("finalDt", gfto.getRegistryDate());
		
		return parametersRequired;
	}
}
