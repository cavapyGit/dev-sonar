package com.pradera.report.generation.pentaho;

import javax.enterprise.util.AnnotationLiteral;

import com.pradera.model.report.type.ReportType;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ReportImplementationLiteral.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/07/2013
 */
public class ReportImplementationLiteral extends AnnotationLiteral<ReportImplementation> implements ReportImplementation{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8393949635454213245L;
	
	/** The type. */
	private ReportType type;
	
	/**
	 * Instantiates a new report implementation literal.
	 */
	public ReportImplementationLiteral(){
		
	}
	
	/**
	 * Instantiates a new report implementation literal.
	 *
	 * @param type the type
	 */
	public ReportImplementationLiteral(ReportType type){
		this.type=type;
	}

	/* (non-Javadoc)
	 * @see com.pradera.report.generation.pentaho.ReportImplementation#type()
	 */
	@Override
	public ReportType type() {
		// TODO Auto-generated method stub
		return type;
	}

}
