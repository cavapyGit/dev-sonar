package com.pradera.report.generation.executor.account;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.naming.directory.ModificationItem;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.RegistryType;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.GeographicLocationServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.HolderHistory;
import com.pradera.model.accounts.HolderReqFileHistory;
import com.pradera.model.accounts.LegalRepresentativeHistory;
import com.pradera.model.accounts.RepresentativeFileHistory;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.holderaccounts.HolderAccountBankRequest;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetRequest;
import com.pradera.model.accounts.holderaccounts.HolderAccountFileRequest;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.GeographicLocationType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.account.service.AccountReportServiceBean;
import com.pradera.report.generation.executor.securities.service.PaymentChronogramBySecurity;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "ModifyingOfDataAccountReport")
public class ModifyingOfDataAccountReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	private GeographicLocationServiceBean geographicLocationServiceBean;

	@EJB
	private AccountReportServiceBean accountReportServiceBean;
	
	@Inject
	private PraderaLogger log; 

	public ModifyingOfDataAccountReport() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {

		Long idHolderRequestPk = null;
		
		for(ReportLoggerDetail param: getReportLogger().getReportLoggerDetails()){
			if(param.getFilterName().equals("idHolderRequestPk")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					idHolderRequestPk = Long.parseLong(param.getFilterValue().toString());
				}
			}
		} 

		
		/** HashMap  * BEGIN **/
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();

		Map<Integer,String> secState		 			= new HashMap<Integer, String>();
		Map<Integer,String> secBoolean		 			= new HashMap<Integer, String>();
		
		try {

			filter.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_REQUEST_STATUS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secState.put(param.getParameterTablePk(), param.getDescription());
			}	
			
			for(BooleanType param : BooleanType.list) {
				secBoolean.put(param.getCode(), param.getValue());
			}
			
		/** HashMap  * END **/

			String strQuery = accountReportServiceBean.getModificationHolderAccountRequest(idHolderRequestPk);

			int contAux = 0;
			int contador = 0;
			
			List<String> lstData = new ArrayList<String>();
			List<String> lstDataN = new ArrayList<String>();
			List<String> lstOldData = new ArrayList<String>();
			List<String> lstNewData	= new ArrayList<String>();

			List<HolderAccountDetRequest> holderAccountDetRequest = new ArrayList<HolderAccountDetRequest>();
			List<HolderAccountDetRequest> holderAccountDetRequestCopy = new ArrayList<HolderAccountDetRequest>();
			
			List<HolderAccountDetRequest> holderAccountDetRequestList = accountReportServiceBean.getHolderAccountDetByHolderRequest(idHolderRequestPk);
			List<Holder> holderList = accountReportServiceBean.getHolderByHolderRequest(idHolderRequestPk);
			
			/** Datos del titular  * BEGIN **/
			for (int i = 0; i < holderAccountDetRequestList.size(); i++) {
				if((holderAccountDetRequestList.get(i).getIndOldNew().equals(BooleanType.NO.getCode()))){	
					holderAccountDetRequestCopy.add(holderAccountDetRequestList.get(i));
				}else if((holderAccountDetRequestList.get(i).getIndOldNew().equals(BooleanType.YES.getCode()))){
					holderAccountDetRequest.add(holderAccountDetRequestList.get(i));
				}
			}
			
			for (int i = 0; i < holderAccountDetRequest.size(); i++) {
				if(!holderAccountDetRequest.get(i).getIndRepresentative().equals(holderAccountDetRequestCopy.get(i).getIndRepresentative())){
					if(contador == 0){
						lstDataN.add("Titulares Asociados	:		");
						lstData.add("Titulares Asociados	:		");
						lstOldData.add("");
						lstNewData.add("");
						contador++;
					}
					else {
						lstDataN.add("   ");
						lstData.add("   ");
						lstOldData.add("");
						lstNewData.add("");
					}
					lstData.add("CUI Principal "+holderList.get(i).getIdHolderPk()+"	:		");
					lstDataN.add("CUI Principal "+holderList.get(i).getIdHolderPk()+"	:		");
					lstOldData.add(secBoolean.get(holderAccountDetRequestCopy.get(i).getIndRepresentative()));
					lstNewData.add(secBoolean.get(holderAccountDetRequest.get(i).getIndRepresentative()));
				}
			}
			
			
			/** Datos del titular  * END **/
			
			List<HolderAccountBankRequest> holderAccountBankRequest = new ArrayList<HolderAccountBankRequest>();
			List<HolderAccountBankRequest> holderAccountBankRequestCopy = new ArrayList<HolderAccountBankRequest>();
			
			List<HolderAccountFileRequest> holderAccountFileRequest = new ArrayList<HolderAccountFileRequest>();
			List<HolderAccountFileRequest> holderAccountFileRequestCopy = new ArrayList<HolderAccountFileRequest>();
			
			List<HolderAccountBankRequest> holderAccountRequestBankList = accountReportServiceBean.getHolderAccountBankByHolderRequest(idHolderRequestPk);
			List<Bank> bankList = accountReportServiceBean.getBankByHolderRequest(idHolderRequestPk);

			/** Cuentas Bancarias  * BEGIN **/
			for (int i = 0; i < holderAccountRequestBankList.size(); i++) {
				if((holderAccountRequestBankList.get(i).getIndOldNew().equals(BooleanType.NO.getCode()))){	
					holderAccountBankRequestCopy.add(holderAccountRequestBankList.get(i));
				}else if((holderAccountRequestBankList.get(i).getIndOldNew().equals(BooleanType.YES.getCode()))){
					holderAccountBankRequest.add(holderAccountRequestBankList.get(i));
				}
			}
			for (int i = 0; i < holderAccountBankRequestCopy.size(); i++) {
				lstData.add("Banco			:		");
				lstOldData.add(bankList.get(i).getDescription());
				lstData.add("	Tipo Cuenta	:		");
				lstOldData.add(holderAccountBankRequestCopy.get(i).getBankAccountTypeDescription());
				lstData.add("	Moneda		:		");
				lstOldData.add(holderAccountBankRequestCopy.get(i).getCurrencyDescription());
				lstData.add("	N° Cuenta	:		");
				lstOldData.add(holderAccountBankRequestCopy.get(i).getAccountNumber());
				lstData.add("	Estado		:		");
				lstOldData.add(holderAccountBankRequestCopy.get(i).getStateDescription());
			}
			for (int i = 0; i < holderAccountBankRequest.size(); i++) {
				lstDataN.add("Banco			:		");
				lstNewData.add(bankList.get(i).getDescription());
				lstDataN.add("	Tipo Cuenta	:		");
				lstNewData.add(holderAccountBankRequest.get(i).getBankAccountTypeDescription());
				lstDataN.add("	Moneda		:		");
				lstNewData.add(holderAccountBankRequest.get(i).getCurrencyDescription());
				lstDataN.add("	N° Cuenta	:		");
				lstNewData.add(holderAccountBankRequest.get(i).getAccountNumber());
				lstDataN.add("	Estado		:		");
				lstNewData.add(holderAccountBankRequest.get(i).getStateDescription());
			}
			
			if(lstNewData.size()>lstOldData.size()){
				for (int i = lstOldData.size(); i < lstDataN.size(); i++) {
					lstOldData.add("");
					lstData.add("");
				}
			}
			if(lstOldData.size()>lstNewData.size()){
				for (int i = lstNewData.size(); i < lstData.size(); i++) {
					lstNewData.add("");
					lstDataN.add("");
				}
			} 
			/** Cuentas Bancarias  * END **/
			
			contador = 0;
			contAux = 0;
			List<HolderAccountFileRequest> holderAccountRequestFileList = accountReportServiceBean.getHolderAccountFileByHolderRequest(idHolderRequestPk);

			/** Archivos Adjuntos * BEGIN **/
			for (int i = 0; i < holderAccountRequestFileList.size(); i++) {
				if(holderAccountRequestFileList.get(i).getIndOldNew().equals(BooleanType.NO.getCode())){
					holderAccountFileRequestCopy.add(holderAccountRequestFileList.get(i));
				}else if(holderAccountRequestFileList.get(i).getIndOldNew().equals(BooleanType.YES.getCode())){
					holderAccountFileRequest.add(holderAccountRequestFileList.get(i));
				}
			}
			for (int i = 0; i < holderAccountFileRequest.size(); i++) {
				contAux = 0;
				for (int j = 0; j < holderAccountFileRequestCopy.size(); j++) {
					if(holderAccountFileRequest.get(i).getDocumentType().equals(holderAccountFileRequestCopy.get(j).getDocumentType())){
						if(!holderAccountFileRequest.get(i).getFilename().equals(holderAccountFileRequestCopy.get(j).getFilename())){
								if(contador == 0){lstDataN.add("Archivos Adjuntos	:		");}
								else {lstDataN.add("   ");}
								lstNewData.add(holderAccountFileRequest.get(i).getFilename());
								contador++;
						}
						contAux++;
					} 
				}
				if(contAux == 0) {
					if(contador == 0){lstDataN.add("Archivos Adjuntos	:		");}
					else {lstDataN.add("   ");}
					lstNewData.add(holderAccountFileRequest.get(i).getFilename());
					contador++;
				}
			}
			contador = 0;
			for (int i = 0; i < holderAccountFileRequestCopy.size(); i++) {
				contAux = 0;
				for (int j = 0; j < holderAccountFileRequest.size(); j++) {
					if(holderAccountFileRequestCopy.get(i).getDocumentType().equals(holderAccountFileRequest.get(j).getDocumentType())){
						if(!holderAccountFileRequestCopy.get(i).getFilename().equals(holderAccountFileRequest.get(j).getFilename())){
							if(contador == 0){lstData.add("Archivos Adjuntos	:		");}
							else {lstData.add("   ");}
							lstOldData.add(holderAccountFileRequestCopy.get(i).getFilename());
							contador++;
						}
						contAux++;
					} 
				}
				if(contAux == 0) {
					if(contador == 0){lstData.add("Archivos Adjuntos	:		");}
					else {lstData.add("   ");}
					lstOldData.add(holderAccountFileRequestCopy.get(i).getFilename());
					contador++;
				}
			}
			/** Archivos Adjuntos * END **/
		 
			for (int i = 0; i < lstOldData.size(); i++) {
				if(!Validations.validateIsNotNull(lstOldData.get(i))){
					lstOldData.set(i, " ");
				}
			}
			for (int i = 0; i < lstNewData.size(); i++) {
				if(!Validations.validateIsNotNull(lstNewData.get(i))){
					lstNewData.set(i, " ");
				}
			}
			
			parametersRequired.put("str_Query", strQuery);
			parametersRequired.put("lstData", lstData);
			parametersRequired.put("lstDataN", lstDataN);
			parametersRequired.put("lstOldData", lstOldData);
			parametersRequired.put("lstNewData", lstNewData);
			parametersRequired.put("pState", secState);
			parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}

		
		
		return parametersRequired;
		
		
	}

}
