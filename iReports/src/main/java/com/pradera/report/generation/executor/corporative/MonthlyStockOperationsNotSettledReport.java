package com.pradera.report.generation.executor.corporative;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.corporative.service.CorporativeReportServiceBean;
import com.pradera.report.generation.executor.corporative.to.StockCalculationProcessTO;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name="MonthlyStockOperationsNotSettledReport")
public class MonthlyStockOperationsNotSettledReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;
	@Inject
	PraderaLogger log;
	
	@EJB
	ParticipantServiceBean participantServiceBean;

	public MonthlyStockOperationsNotSettledReport() {
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParametersQueryReport() {
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		if (listaLogger == null) {
			listaLogger = new ArrayList<ReportLoggerDetail>();
		}
	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {

		Map<String, Object> parametersRequired = new HashMap<>();

		// TODO Auto-generated method stub
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		return parametersRequired;
	}
}
