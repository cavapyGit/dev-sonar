package com.pradera.report.generation.executor.account.to;

import java.io.Serializable;

public class NoteRegistrationDGravamenTO implements Serializable{

	private String confirmationDate;
	
	private String fullName;
	
	private String idBlockRequestPk;
	
	private String blockNumber;
	
	private String blockNumberDate;
	
	private String registryUser;
	
	private String idHolderFk;
	
	private String securityClass;
	
	private String businessName;
	
	private String description;
	
	private String actualBlockBalance;
	
	private String blockType;
	
	public NoteRegistrationDGravamenTO() {}
	

	public NoteRegistrationDGravamenTO(String confirmationDate,
			String fullName, String idBlockRequestPk, String blockNumber,
			String blockNumberDate, String registryUser, String idHolderFk,
			String securityClass, String businessName, String description,
			String actualBlockBalance, String blockType) {
		super();
		this.confirmationDate = confirmationDate;
		this.fullName = fullName;
		this.idBlockRequestPk = idBlockRequestPk;
		this.blockNumber = blockNumber;
		this.blockNumberDate = blockNumberDate;
		this.registryUser = registryUser;
		this.idHolderFk = idHolderFk;
		this.securityClass = securityClass;
		this.businessName = businessName;
		this.description = description;
		this.actualBlockBalance = actualBlockBalance;
		this.blockType = blockType;
	}

	public String getConfirmationDate() {
		return confirmationDate;
	}

	public String getBlockType() {
		return blockType;
	}

	public void setBlockType(String blockType) {
		this.blockType = blockType;
	}

	public void setConfirmationDate(String confirmationDate) {
		this.confirmationDate = confirmationDate;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getIdBlockRequestPk() {
		return idBlockRequestPk;
	}

	public void setIdBlockRequestPk(String idBlockRequestPk) {
		this.idBlockRequestPk = idBlockRequestPk;
	}

	public String getBlockNumber() {
		return blockNumber;
	}

	public void setBlockNumber(String blockNumber) {
		this.blockNumber = blockNumber;
	}

	public String getBlockNumberDate() {
		return blockNumberDate;
	}

	public void setBlockNumberDate(String blockNumberDate) {
		this.blockNumberDate = blockNumberDate;
	}

	public String getRegistryUser() {
		return registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public String getIdHolderFk() {
		return idHolderFk;
	}

	public void setIdHolderFk(String idHolderFk) {
		this.idHolderFk = idHolderFk;
	}

	public String getSecurityClass() {
		return securityClass;
	}

	public void setSecurityClass(String securityClass) {
		this.securityClass = securityClass;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getActualBlockBalance() {
		return actualBlockBalance;
	}

	public void setActualBlockBalance(String actualBlockBalance) {
		this.actualBlockBalance = actualBlockBalance;
	}
	
}
