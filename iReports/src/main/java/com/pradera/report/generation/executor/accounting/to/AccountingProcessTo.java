package com.pradera.report.generation.executor.accounting.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingProcessTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class AccountingProcessTo implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id accounting process pk. */
	private Long idAccountingProcessPk;
	
	/** The process type. */
	private Integer processType;

	/** The branch. */
	private Integer branch;
	
	/** The status. */
	private Integer status;
		 
	/** The execution date. */
	private Date executionDate;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/** The list accounting receipt to. */
	private List<AccountingReceiptTo> listAccountingReceiptTo;
	
	/**Indicator Report executed By Process*/
	private Integer reportByProcess;
	
	/** The start type. */
	private Integer startType;

	
	/**
	 * Gets the id accounting process pk.
	 *
	 * @return the id accounting process pk
	 */
	public Long getIdAccountingProcessPk() {
		return idAccountingProcessPk;
	}

	/**
	 * Sets the id accounting process pk.
	 *
	 * @param idAccountingProcessPk the new id accounting process pk
	 */
	public void setIdAccountingProcessPk(Long idAccountingProcessPk) {
		this.idAccountingProcessPk = idAccountingProcessPk;
	}

	/**
	 * Gets the process type.
	 *
	 * @return the process type
	 */
	public Integer getProcessType() {
		return processType;
	}

	/**
	 * Sets the process type.
	 *
	 * @param processType the new process type
	 */
	public void setProcessType(Integer processType) {
		this.processType = processType;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * Gets the execution date.
	 *
	 * @return the execution date
	 */
	public Date getExecutionDate() {
		return executionDate;
	}

	/**
	 * Sets the execution date.
	 *
	 * @param executionDate the new execution date
	 */
	public void setExecutionDate(Date executionDate) {
		this.executionDate = executionDate;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initialDate
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the initialDate to set
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the finalDate
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the finalDate to set
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	/**
	 * Gets the list accounting receipt to.
	 *
	 * @return the listAccountingReceiptTo
	 */
	public List<AccountingReceiptTo> getListAccountingReceiptTo() {
		return listAccountingReceiptTo;
	}

	/**
	 * Sets the list accounting receipt to.
	 *
	 * @param listAccountingReceiptTo the listAccountingReceiptTo to set
	 */
	public void setListAccountingReceiptTo(
			List<AccountingReceiptTo> listAccountingReceiptTo) {
		this.listAccountingReceiptTo = listAccountingReceiptTo;
	}

	/**
	 * Gets the branch.
	 *
	 * @return the branch
	 */
	public Integer getBranch() {
		return branch;
	}

	/**
	 * Sets the branch.
	 *
	 * @param branch the branch to set
	 */
	public void setBranch(Integer branch) {
		this.branch = branch;
	}

	/**
	 * @return the reportByProcess
	 */
	public Integer getReportByProcess() {
		return reportByProcess;
	}

	/**
	 * @param reportByProcess the reportByProcess to set
	 */
	public void setReportByProcess(Integer reportByProcess) {
		this.reportByProcess = reportByProcess;
	}

	/**
	 * @return the startType
	 */
	public Integer getStartType() {
		return startType;
	}

	/**
	 * @param startType the startType to set
	 */
	public void setStartType(Integer startType) {
		this.startType = startType;
	}
	
	

}
