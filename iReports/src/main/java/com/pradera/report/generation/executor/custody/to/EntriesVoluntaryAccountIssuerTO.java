package com.pradera.report.generation.executor.custody.to;

import java.io.Serializable;

public class EntriesVoluntaryAccountIssuerTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6820106702558102822L;
	
	private Integer operationState;
	private Integer motive;
	private Long idHolderAccountPk;
	private Integer securityClass;
	private Integer currency;
	private String issuer;
	private Long idHolderPk;
	private Long participantPk;
	private String strDateInitial;
	private String strDateEnd;
	private String security;
	private Integer requestNumber;
	private Integer institutionType;

	public Long getParticipantPk() {
		return participantPk;
	}
	public void setParticipantPk(Long participantPk) {
		this.participantPk = participantPk;
	}
	public String getStrDateInitial() {
		return strDateInitial;
	}
	public void setStrDateInitial(String strDateInitial) {
		this.strDateInitial = strDateInitial;
	}
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}
	public Long getIdHolderPk() {
		return idHolderPk;
	}
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}
	public String getStrDateEnd() {
		return strDateEnd;
	}
	public void setStrDateEnd(String strDateEnd) {
		this.strDateEnd = strDateEnd;
	}
	public Integer getMotive() {
		return motive;
	}
	public void setMotive(Integer motive) {
		this.motive = motive;
	}
	public Integer getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}
	public Integer getCurrency() {
		return currency;
	}
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	public String getIssuer() {
		return issuer;
	}
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	public String getSecurity() {
		return security;
	}
	public void setSecurity(String security) {
		this.security = security;
	}
	public Integer getOperationState() {
		return operationState;
	}
	public void setOperationState(Integer operationState) {
		this.operationState = operationState;
	}
	public Integer getRequestNumber() {
		return requestNumber;
	}
	public void setRequestNumber(Integer requestNumber) {
		this.requestNumber = requestNumber;
	}
	public Integer getInstitutionType() {
		return institutionType;
	}
	public void setInstitutionType(Integer institutionType) {
		this.institutionType = institutionType;
	}


}
