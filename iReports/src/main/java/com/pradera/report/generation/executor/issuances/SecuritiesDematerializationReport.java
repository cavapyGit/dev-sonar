package com.pradera.report.generation.executor.issuances;

import java.io.ByteArrayOutputStream;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.issuances.services.IssuancesReportServiceBean;
import com.pradera.report.generation.executor.issuances.to.SecuritiesDematerializationTO;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;
import com.pradera.report.generation.service.ReportUtilServiceBean;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "SecuritiesDematerializationReport")
public class SecuritiesDematerializationReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	private IssuancesReportServiceBean issuancesReportService;
	
	@EJB
	private ReportUtilServiceBean reportUtilServiceBean;
	
	private final String NEW_LINE = GeneralConstants.STR_NEW_LINEA;
	private final String COMM = GeneralConstants.STR_COMMA_WITHOUT_SPACE;
	private final String COMM_COM =  GeneralConstants.STR_COMMA_WITHOUT_SPACE+GeneralConstants.STR_COMILLA;
	private final String COM_COMM_COM = GeneralConstants.STR_COMILLA+GeneralConstants.STR_COMMA_WITHOUT_SPACE+GeneralConstants.STR_COMILLA;
	private final String COM_COMM = GeneralConstants.STR_COMILLA+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
	private final String COM =  GeneralConstants.STR_COMILLA;
	
	public SecuritiesDematerializationReport() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		GenericsFiltersTO gfto = new GenericsFiltersTO();
		for(ReportLoggerDetail param: getReportLogger().getReportLoggerDetails()){
			if(param.getFilterName().equals("final_date")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setFinalDt(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("reportFormats")){
				if(param.getFilterDescription().equalsIgnoreCase(ReportFormatType.TXT.getValue()))
				if (Validations.validateIsNotNullAndPositive(Integer.valueOf(param.getFilterValue()))){
					gfto.setReportFormat(Integer.valueOf(param.getFilterValue()));
				}
			}
		}
		
		Date fecha = CommonsUtilities.convertStringtoDate(gfto.getFinalDt());
		Calendar calendario = Calendar.getInstance();
		calendario.setTime(fecha);
		calendario.add(Calendar.DATE, -6);
		
		String week = CommonsUtilities.convertDateToString(calendario.getTime(),"dd/MM/yyyy");
		String year = CommonsUtilities.convertDateToString(fecha,"yyyy");
		
		String strQuery;
		Map<String,Object> parametersRequired = new HashMap<>();
		
		if(ReportFormatType.TXT.getCode().equals(gfto.getReportFormat())){
	    	String month = CommonsUtilities.convertDateToString(fecha,"MM/yyyy");
	    	String firstDate = "01/"+month;
	    	strQuery = issuancesReportService.getQuerySecuritiesDematerialization(gfto, firstDate, null);
			generateReportTXT(gfto,strQuery);
			return null;
		}else{
			strQuery = issuancesReportService.getQuerySecuritiesDematerialization(gfto, week, year);
			parametersRequired.put("str_Query", strQuery);
			parametersRequired.put("initialDt", week);
			parametersRequired.put("finalDt", gfto.getFinalDt());
			parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		}
		return parametersRequired;
	}
	/**
	 * Metodo que genera el reporte en formato TXT para la ASFI ID_REPORT_PK : 75
	 * @author hcoarite
	 * @param GenericsFiltersTO gfto,String strQuery
	 * @see <a href = "http://192.168.100.83/mantisbt/view.php?id=638" />
	 * */
	private void generateReportTXT(GenericsFiltersTO gfto,String strQuery){
		StringBuilder sbTxtFormat = new StringBuilder();
		String fileName = getNameAccordingFund(gfto)+"EDB";
		List<SecuritiesDematerializationTO> dematerializationTOs = issuancesReportService.getSecuritiesDesmaterializations(strQuery);
		if(Validations.validateListIsNullOrEmpty(dematerializationTOs)){
			sbTxtFormat.append(COM+"SIN DATOS"+COM);
			reportUtilServiceBean.saveCustomReportFile(fileName, ReportFormatType.TXT.getCode(), sbTxtFormat.toString().getBytes(), getReportLogger());
			return;
		}
		sbTxtFormat.append(getSecuritiesDematerializationTO(dematerializationTOs, CurrencyType.PYG.getCode(),gfto));
//		sbTxtFormat.append(GeneralConstants.STR_NEW_LINEA);
		sbTxtFormat.append(getSecuritiesDematerializationTO(dematerializationTOs, CurrencyType.UFV.getCode(),gfto));
//		sbTxtFormat.append(GeneralConstants.STR_NEW_LINEA);
		sbTxtFormat.append(getSecuritiesDematerializationTO(dematerializationTOs, CurrencyType.DMV.getCode(),gfto));
//		sbTxtFormat.append(GeneralConstants.STR_NEW_LINEA);
		sbTxtFormat.append(getSecuritiesDematerializationTO(dematerializationTOs, CurrencyType.EU.getCode(),gfto));
//		sbTxtFormat.append(GeneralConstants.STR_NEW_LINEA);
		sbTxtFormat.append(getSecuritiesDematerializationTO(dematerializationTOs, CurrencyType.USD.getCode(),gfto));
		
		reportUtilServiceBean.saveCustomReportFile(fileName, ReportFormatType.TXT.getCode(), sbTxtFormat.toString().getBytes(), getReportLogger());
	}
	/**
	 * Retorna cantidad de valores representados en ACV
	 * */
	
	
	/**
	 * Devuelve el nombre para el reporte
	 * @author hcoarite
	 * @param GenericsFiltersTO gfto,String strQuery
	 * @see <a href = "http://192.168.100.83/mantisbt/view.php?id=638" />
	 * */
	private String getNameAccordingFund(GenericsFiltersTO gfto){
		String name="";
		String firstWord="M";
		String strDate= gfto.getFinalDt();
		String strDay = strDate.substring(0,2);
		String strMonth = strDate.substring(3,5);
		String strYear = strDate.substring(8,10);
		String lastWord="AQ";
		String reportCode="";

		name=firstWord;
		name+=strYear+strMonth+strDay;
		name+=lastWord;
		name+=GeneralConstants.DOT+reportCode;
		return name;
	}
	/**
	 * Metodo que genera el reporte en formato TXT para la ASFI
	 * @author hcoarite
	 * @param List<SecuritiesDematerializationTO> dematerializationTOs, Integer currency, GenericsFiltersTO gfto
	 * @see <a href = "http://192.168.100.83/mantisbt/view.php?id=638" />
	 * */
	private String getSecuritiesDematerializationTO(List<SecuritiesDematerializationTO> dematerializationTOs, Integer currency, GenericsFiltersTO gfto){
		StringBuilder txtFormat = new StringBuilder();
		boolean firstLoop = true;
		for (SecuritiesDematerializationTO to : dematerializationTOs) {
			if(!to.getCurrency().equals(currency))
				continue;
			
			if(to.getCurrency().equals(CurrencyType.PYG.getCode())
					&& firstLoop){
				firstLoop = false;
			}else{
				txtFormat.append(NEW_LINE);
			}
			
			/*1,2,3*/
			txtFormat.append(COM+"EDB"+COM_COMM_COM+getDateString(gfto.getFinalDt())+
			COM_COMM_COM+reportUtilServiceBean.getCurrencyAsfi(to.getCurrency())+COM_COMM);
			/*4,5*/
			txtFormat.append(COM+to.getSecurtyClassMnemonic()+
			COM_COMM_COM+to.getNominalValueMonthly().setScale(2, RoundingMode.HALF_UP)+COM_COMM);
			/*6 Cantidad de valores desmaterializados mes*/
			txtFormat.append(COM+to.getTotalBalanceWeek()+
		    /*7 Valor nominal acmulado ANIO*/
			COM_COMM_COM+to.getNominalValueYear().setScale(2, RoundingMode.HALF_UP)+COM_COMM);
			/*8 valores des desmaterializados ano */
			txtFormat.append(COM+to.getTotalBalanceYear()+
			COM_COMM_COM+to.getNominalValueAll()+COM_COMM);
			/*9 Valores desmaterializados desde inicios de funcionamiento*/
			txtFormat.append(COM+to.getTotalBalanceAll()+
			COM);
			
		}
		return txtFormat.toString();
	}
	/**
	 * Devuelve el nombre para el reporte
	 * @author hcoarite
	 * @param String templateDate
	 * @see <a href = "http://192.168.100.83/mantisbt/view.php?id=638" />
	 * */
	private String getDateString(String templateDate){
		Date date = CommonsUtilities.convertStringtoDate(templateDate, GeneralConstants.DATE_FORMAT_PATTERN);
		return CommonsUtilities.convertDateToString(date,GeneralConstants.DATE_FORMAT_PATTERN_TXT);
	}
}
