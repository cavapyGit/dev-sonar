package com.pradera.report.generation.executor.to;

import java.io.Serializable;

//use it when the report is only pdf format
public class GenericsFiltersTO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String idSecurityCodePk;
	private String finalDt;
	private String initialDt;
	private String idIssuer;
	private String issuerDesc;
	private String cui;
	private String idHolderAccountPk;
	private String indBCB;
	private String date;
	private String participant;
	private String participantDesc;
	private String isinCode;
	private String cfiCode;
	private String lstIntegerForJasper;
	private String motive;
	private String idInteger;
	private String state;
	private String userCode;
	private String correlative;
	private String requestNumber;
	private String currency;
	private String accountNumber;
	private String reportingHolders;
	private String fileCode;
	private String institutionType;
	private String issuanceDate;
	private String securityClass;
	private String docType;
	private String docNumber;
	private String stateHolder;
	private String securitiesBalance;
	private String typePerson;
	private String economicActivity;
	private Integer reportFormat;
	private String bcbFormat;
	
	public String getBcbFormat() {
		return bcbFormat;
	}
	public void setBcbFormat(String bcbFormat) {
		this.bcbFormat = bcbFormat;
	}	
	public String getFinalDt() {
		return finalDt;
	}
	public void setFinalDt(String finalDt) {
		this.finalDt = finalDt;
	}
	public String getInitialDt() {
		return initialDt;
	}
	public void setInitialDt(String initialDt) {
		this.initialDt = initialDt;
	}
	public String getIdIssuer() {
		return idIssuer;
	}
	public void setIdIssuer(String idIssuer) {
		this.idIssuer = idIssuer;
	}
	public String getCui() {
		return cui;
	}
	public void setCui(String cui) {
		this.cui = cui;
	}
	public String getIndBCB() {
		return indBCB;
	}
	public void setIndBCB(String indBCB) {
		this.indBCB = indBCB;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
	public String getIdHolderAccountPk() {
		return idHolderAccountPk;
	}
	public void setIdHolderAccountPk(String idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}
	public String getParticipant() {
		return participant;
	}
	public void setParticipant(String participant) {
		this.participant = participant;
	}
	public String getIsinCode() {
		return isinCode;
	}
	public void setIsinCode(String isinCode) {
		this.isinCode = isinCode;
	}
	public String getCfiCode() {
		return cfiCode;
	}
	public void setCfiCode(String cfiCode) {
		this.cfiCode = cfiCode;
	}
	public String getLstIntegerForJasper() {
		return lstIntegerForJasper;
	}
	public void setLstIntegerForJasper(String lstIntegerForJasper) {
		this.lstIntegerForJasper = lstIntegerForJasper;
	}
	public String getMotive() {
		return motive;
	}
	public void setMotive(String motive) {
		this.motive = motive;
	}
	public String getParticipantDesc() {
		return participantDesc;
	}
	public void setParticipantDesc(String participantDesc) {
		this.participantDesc = participantDesc;
	}
	public String getIssuerDesc() {
		return issuerDesc;
	}
	public void setIssuerDesc(String issuerDesc) {
		this.issuerDesc = issuerDesc;
	}
	public String getIdInteger() {
		return idInteger;
	}
	public void setIdInteger(String idInteger) {
		this.idInteger = idInteger;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getUserCode() {
		return userCode;
	}
	public String getCorrelative() {
		return correlative;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public void setCorrelative(String correlative) {
		this.correlative = correlative;
	}
	public String getRequestNumber() {
		return requestNumber;
	}
	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getReportingHolders() {
		return reportingHolders;
	}
	public void setReportingHolders(String reportingHolders) {
		this.reportingHolders = reportingHolders;
	}
	public String getFileCode() {
		return fileCode;
	}
	public void setFileCode(String fileCode) {
		this.fileCode = fileCode;
	}
	public String getInstitutionType() {
		return institutionType;
	}
	public void setInstitutionType(String institutionType) {
		this.institutionType = institutionType;
	}
	public String getIssuanceDate() {
		return issuanceDate;
	}
	public void setIssuanceDate(String issuanceDate) {
		this.issuanceDate = issuanceDate;
	}
	public String getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(String securityClass) {
		this.securityClass = securityClass;
	}
	public String getDocType() {
		return docType;
	}
	public void setDocType(String docType) {
		this.docType = docType;
	}
	public String getDocNumber() {
		return docNumber;
	}
	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}
	public String getStateHolder() {
		return stateHolder;
	}
	public void setStateHolder(String stateHolder) {
		this.stateHolder = stateHolder;
	}
	public String getSecuritiesBalance() {
		return securitiesBalance;
	}
	public void setSecuritiesBalance(String securitiesBalance) {
		this.securitiesBalance = securitiesBalance;
	}
	public String getTypePerson() {
		return typePerson;
	}
	public void setTypePerson(String typePerson) {
		this.typePerson = typePerson;
	}
	public String getEconomicActivity() {
		return economicActivity;
	}
	public void setEconomicActivity(String economicActivity) {
		this.economicActivity = economicActivity;
	}
	public Integer getReportFormat() {
		return reportFormat;
	}
	public void setReportFormat(Integer reportFormat) {
		this.reportFormat = reportFormat;
	}
}
