package com.pradera.report.generation.executor.corporative;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.corporatives.stockcalculation.type.StockClassType;
import com.pradera.model.corporatives.stockcalculation.type.StockType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.corporative.service.CorporativeReportServiceBean;
import com.pradera.report.generation.executor.corporative.to.CorporativeProcesInterestTO;
import com.pradera.report.generation.executor.corporative.to.StockCalculationProcessTO;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

@ReportProcess(name="CorporateEventsSpecialControlReport")
public class CorporateEventsSpecialControlReport extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3670029005786056499L;
	
	@EJB
	ParameterServiceBean parameterService;
	
	@EJB
	CorporativeReportServiceBean corporativeReportService;
	
	@EJB
	ParticipantServiceBean participantService;
	
	private CorporativeProcesInterestTO corporativeTo; 
	
	private static Integer PARTICIPANTE_PK = new Integer(0);
	
	private static Integer AVAILABLE_STOCK = new Integer(1);
	
	private static Integer AVAILABLE_PROCESS = new Integer(2);
	
	private static Integer HOLDERS_STOCK = new Integer(3);
	
	private static Integer BLOCKED_STOCK = new Integer(4);
	
	private static Integer BLOCKED_PROCESS = new Integer(5);
	
	private static Integer HOLDERS_BLOCKED = new Integer(6);
	
	private static Integer TOTAL_STOCK = new Integer(7);
	
	private static Integer TOTAL_PROCESS = new Integer(8);

	public CorporateEventsSpecialControlReport() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		corporativeTo = getCorporateParameters(reportLogger);
		List<Object[]> processInformation = corporativeReportService.getCorporativeSpecialHeader(corporativeTo);
		try{
			XMLOutputFactory xof = XMLOutputFactory.newInstance();
			XMLStreamWriter xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(baos));
			xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");//<XML>
			xmlsw.writeStartElement(ReportConstant.REPORT);//<report>
			createHeaderReport( xmlsw, reportLogger );//header
			if(!processInformation.isEmpty()){
				Map<Integer,String> processStateMap = new HashMap<>();
				Map<Integer,String> currencyMap = new HashMap<>();
				for (ParameterTable param : parameterService.getListParameterTableServiceBean(MasterTableType.CURRENCY.getCode())){
					currencyMap.put(param.getParameterTablePk(), param.getParameterName());
				}
				for(ParameterTable param : parameterService.getListParameterTableServiceBean(MasterTableType.CORPORATIVE_PROCESS_STATE.getCode())){
					processStateMap.put(param.getParameterTablePk(), param.getParameterName());
				}
				createSubHeaderCorporativeReport(xmlsw,corporativeTo, processInformation.get(0), processStateMap, currencyMap);
				xmlsw.writeStartElement("participants");
				createBodyReport(xmlsw,corporativeTo,processInformation.get(0));
				xmlsw.writeEndElement();
			} else {
				xmlsw.writeStartElement("participants");
				createTagInteger(xmlsw, "participant","0");
				xmlsw.writeEndElement();
				
			}
			xmlsw.writeEndElement();//</report>
			xmlsw.writeEndDocument();//</XML>
			xmlsw.flush();
	        xmlsw.close();
		}catch(XMLStreamException exm){
			exm.printStackTrace();
		} catch (ServiceException ex) {
			// TODO: handle exception
		}
		return baos;
	}
	
	public void createBodyReport(XMLStreamWriter xmlsw,CorporativeProcesInterestTO corporativeProcessInformation,Object[] corporativeInformation) throws XMLStreamException{
		List<Object[]> processControlInformation = corporativeReportService.getStockSpecialControlProcess(corporativeProcessInformation,corporativeInformation);
		double stockAvailable=0,processAvailable=0,stockBlocked=0,processBlocked=0,totalStock=0,totalProcess=0;
		int holderAvailableCount=0,holderBlockedCount=0,totalHolders=0;
		Integer processType = new Integer(corporativeInformation[CORPORATIVE_TYPE].toString());
		if(!processControlInformation.isEmpty()){
			Map<Long,String> participantMap = new HashMap<>();
			for(Participant part : participantService.getComboParticipantsByMapFilter(new HashMap<String,Object>())){
				participantMap.put(part.getIdParticipantPk(), part.getMnemonic()+" - "+part.getDescription());
			}			
			for(Object[] row : processControlInformation){
				xmlsw.writeStartElement("participant");
					createTagString(xmlsw, "code", row[PARTICIPANTE_PK].toString());
					createTagString(xmlsw, "partDescription", participantMap.get(new Long(row[PARTICIPANTE_PK].toString())));
					createTagInteger(xmlsw, "stockAvailable", row[AVAILABLE_STOCK]);
					createTagInteger(xmlsw, "processAvailable", row[AVAILABLE_PROCESS]);			
					createTagInteger(xmlsw, "holderAvailableCount", row[HOLDERS_STOCK]);
					createTagInteger(xmlsw, "stockBlocked", row[BLOCKED_STOCK]);
					createTagInteger(xmlsw, "processBlocked", row[BLOCKED_PROCESS]);					
					createTagInteger(xmlsw, "holderBlockedCount", row[HOLDERS_BLOCKED]);
					createTagInteger(xmlsw, "totalStock", row[TOTAL_STOCK]);
					createTagInteger(xmlsw, "totalProcess", row[TOTAL_PROCESS]);				
					createTagInteger(xmlsw, "totalHolders", getIntegerNumber(row[HOLDERS_STOCK])+getIntegerNumber(row[HOLDERS_BLOCKED]));
				xmlsw.writeEndElement();
				stockAvailable+=getBigDecimalNumber(row[AVAILABLE_STOCK]).doubleValue();
				processAvailable+=getBigDecimalNumber(row[AVAILABLE_PROCESS]).doubleValue();
				holderAvailableCount+=getIntegerNumber(row[HOLDERS_STOCK]);
				stockBlocked+=getBigDecimalNumber(row[BLOCKED_STOCK]).doubleValue();
				processBlocked+=getBigDecimalNumber(row[BLOCKED_PROCESS]).doubleValue();
				holderBlockedCount+=getIntegerNumber(row[HOLDERS_BLOCKED]);
				totalStock+=getBigDecimalNumber(row[TOTAL_STOCK]).doubleValue();
				totalProcess+=getBigDecimalNumber(row[TOTAL_PROCESS]).doubleValue();
				totalHolders+=getIntegerNumber(row[HOLDERS_STOCK])+getIntegerNumber(row[HOLDERS_BLOCKED]);
			}
		} else {
			xmlsw.writeStartElement("participant");
			createTagString(xmlsw, "code",0);
			xmlsw.writeEndElement();
		}
		//total participants
		createTagInteger(xmlsw, "sumStockAvailable", stockAvailable);
		createTagInteger(xmlsw, "sumProcessAvailable", processAvailable);	
		createTagInteger(xmlsw, "sumHolderAvailableCount", holderAvailableCount);
		createTagInteger(xmlsw, "sumStockBlocked", stockBlocked);
		createTagInteger(xmlsw, "sumProcessBlocked", processBlocked);		
		createTagInteger(xmlsw, "sumHolderBlockedCount", holderBlockedCount);
		createTagInteger(xmlsw, "sumTotalStock", totalStock);
		createTagInteger(xmlsw, "sumTotalProcess", totalProcess);	
		createTagInteger(xmlsw, "sumTotalHolders",totalHolders);
	}
}
