package com.pradera.report.generation.executor.securities.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class SecuritiesByIssuanceAndIssuerTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** The id issuer pk. */
	//private String idIssuerPk;
	
	/** The id issuer pk. */
	private String idIssuancePk;
	
	/** The initial date. */
	private Date initialDate;

	/** The final date. */
	private Date finalDate;
	private String cfiCode;
	private String idIsinCode;
	private String fsinCode;
	private String IdSecurityCodePk;
	
	private Date dateForName;
	private Date date;
	private Integer securityClass;
	private String idSecurityOnly;
	private Integer stateSecurityCode;
	private String idIssuerPk;
	private Integer shareBalance;
	private String custodian;
	private Date registryDt;
	private String mnemonic;
	private Integer currency;
	private String bussinesname;
	private BigDecimal ininomval;
	private Date issuancedate;
	private Date expirationDtInt;
	private Integer securitiesnumber; // Cantidad de Valores
	private BigDecimal marketrate; // Tasa de Rendimiento
	private BigDecimal discountrate;// Tasa de descuento
	private Integer couponNumberInt;
	private Date dateinformation;
	private Integer amortizationType;
	private String periodicity;
	private String negotiatingmanner; //Modalidad de Negociacion
	private String marketmoney; // Mesa de Dinero
	
	private Date expirationDtAmort;
	private BigDecimal amortizationFactor;
    
	private List<Integer> lstSecurityClass;
	private Integer motive;
	private Integer stateAnnotation;
	
	private String participantPlacement;
	private Long idReport;
	
	//issue 578 reporte caracteristicas de valor B
	private BigDecimal issuanceAmount;
	private BigDecimal maximumInvestment;
	
	public BigDecimal getIssuanceAmount() {
		return issuanceAmount;
	}
	public void setIssuanceAmount(BigDecimal issuanceAmount) {
		this.issuanceAmount = issuanceAmount;
	}
		
	//--
	
	public BigDecimal getMaximumInvestment() {
		return maximumInvestment;
	}
	public void setMaximumInvestment(BigDecimal maximumInvestment) {
		this.maximumInvestment = maximumInvestment;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	public Date getDateInformation() {
		return dateinformation;
	}

	public void setDateInformation(Date dateinformation) {
		this.dateinformation = dateinformation;
	}
	
	public Integer getSecuritiesNumber(){
		return securitiesnumber;
	}
	public void setSecuritiesNumber(Integer SecuritiesNumber) {
		this.securitiesnumber = SecuritiesNumber;
	}
	
	public Integer getShareBalance(){
		return shareBalance;
	}
	public void setShareBalance(Integer ShareBalance) {
		this.shareBalance = ShareBalance;
	}
	
	public BigDecimal getMarketrate(){
		return marketrate;
	}
	public void setMarketrate(BigDecimal Marketrate) {
		this.marketrate = Marketrate; 
	}
	
	public BigDecimal getDiscountrate(){
		return discountrate;
	}
	public void setDiscountrate(BigDecimal Discountrate) {
		this.discountrate = Discountrate;
	}
	
	public String getNegotiatingmanner(){
		return negotiatingmanner;
	}
	public void setNegotiatingmanner(String Negotiatingmanner) {
		this.negotiatingmanner = Negotiatingmanner;
	}
	
	public String getMarketmoney(){
		return marketmoney;
	}
	public void setMarketmoney(String Marketmoney) {
		this.marketmoney = Marketmoney;
	}
	
	public String getBussinesName() {
		return bussinesname;
	}

	public void setBussinesName(String bussinesname) {
		this.bussinesname = bussinesname;
	}
	
	public String getIdIssuerPk() {
		return idIssuerPk;
	}

	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}

	public String getIdIssuancePk() {
		return idIssuancePk;
	}

	public void setIdIssuancePk(String idIssuancePk) {
		this.idIssuancePk = idIssuancePk;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	public String getIdSecurityOnly() {
		return idSecurityOnly;
	}

	public void setIdSecurityOnly(String IdSecurityOnly) {
		this.idSecurityOnly = IdSecurityOnly;
	}

	public String getIdIsinCode() {
		return idIsinCode;
	}

	public void setIdIsinCode(String idIsinCode) {
		this.idIsinCode = idIsinCode;
	}

	public String getCfiCode() {
		return cfiCode;
	}

	public void setCfiCode(String cfiCode) {
		this.cfiCode = cfiCode;
	}
	
	public Date getRegistryDt() {
		return registryDt;
	}
	public void setRegistryDt(Date registryDt) {
		this.registryDt = registryDt;
	}
	
	public Integer getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	public String getMnemonic() {
		return mnemonic;
	}
	public void setMnemonic(String Mnemonic) {
		this.mnemonic = Mnemonic;
	}
	
	public Integer getCurrency() {
		return currency;
	}
	public void setCurrency(Integer Currency) {
		this.currency = Currency;
	}
	
	public String getCustodian() {
		return custodian;
	}
	public void setCustodian(String Custodian) {
		this.custodian = Custodian;
	}
	
	public BigDecimal getIninomval() {
		return ininomval;
	}
	public void setIninomval(BigDecimal Ininomval) {
		this.ininomval = Ininomval;
	}
	
	public Date getExpirationDtInt() {
		return expirationDtInt;
	}
	public void setExpirationDtInt(Date ExpirationDtInt) {
		this.expirationDtInt = ExpirationDtInt;
	}
	
	public Date getIssuancedate() {
		return issuancedate;
	}
	public void setIssuancedate(Date Issuancedate) {
		this.issuancedate = Issuancedate;
	}
	
	public Integer getCouponNumberInt() {
		return couponNumberInt;
	}
	public void setCouponNumberInt(Integer CouponNumberInt) {
		this.couponNumberInt = CouponNumberInt;
	}
	
	public Integer getAmortizationType() {
		return amortizationType;
	}
	public void setAmortizationType(Integer AmortizationType) {
		this.amortizationType = AmortizationType;
	}
	
	public String getPeriodicity() {
		return periodicity;
	}
	public void setPeriodicity(String Periodicity) {
		this.periodicity = Periodicity;
	}
	
	public String getIdSecurityCodePk() {
		return IdSecurityCodePk;
	}

	public void setIdSecurityCodePk(String IdSecurityCodePk) {
		this.IdSecurityCodePk = IdSecurityCodePk;
	}
	
	public Date getExpirationDtAmort() {
		return expirationDtAmort;
	}
	public void setExpirationDtAmort(Date expirationDtAmort) {
		this.expirationDtAmort = expirationDtAmort;
	}
	public BigDecimal getAmortizationFactor() {
		return amortizationFactor;
	}
	public void setAmortizationFactor(BigDecimal amortizationFactor) {
		this.amortizationFactor = amortizationFactor;
	}
	public List<Integer> getLstSecurityClass() {
		return lstSecurityClass;
	}
	public void setLstSecurityClass(List<Integer> lstSecurityClass) {
		this.lstSecurityClass = lstSecurityClass;
	}
	public Integer getMotive() {
		return motive;
	}
	public void setMotive(Integer motive) {
		this.motive = motive;
	}
	public Integer getStateAnnotation() {
		return stateAnnotation;
	}
	public void setStateAnnotation(Integer stateAnnotation) {
		this.stateAnnotation = stateAnnotation;
	}
	public String getParticipantPlacement() {
		return participantPlacement;
	}
	public void setParticipantPlacement(String participantPlacement) {
		this.participantPlacement = participantPlacement;
	}
	public Long getIdReport() {
		return idReport;
	}
	public void setIdReport(Long idReport) {
		this.idReport = idReport;
	}
	public String getFsinCode() {
		return fsinCode;
	}
	public void setFsinCode(String fsinCode) {
		this.fsinCode = fsinCode;
	}
	public Date getDateForName() {
		return dateForName;
	}
	public void setDateForName(Date dateForName) {
		this.dateForName = dateForName;
	}
	public Integer getStateSecurityCode() {
		return stateSecurityCode;
	}
	public void setStateSecurityCode(Integer stateSecurityCode) {
		this.stateSecurityCode = stateSecurityCode;
	}
}
