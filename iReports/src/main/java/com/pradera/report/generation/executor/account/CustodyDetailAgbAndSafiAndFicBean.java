package com.pradera.report.generation.executor.account;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.commons.view.ViewDepositaryBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.HolderHelperOutputTO;
import com.pradera.core.component.helperui.to.SecuritiesSearcherTO;
import com.pradera.core.component.helperui.to.SecuritiesTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.type.EconomicActivityType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.report.generation.executor.account.service.AccountReportServiceBean;
import com.pradera.report.generation.executor.account.to.PortafolioDetailCustodianTO;
import com.pradera.report.generation.facade.ReportGenerationLocalServiceFacade;
import com.pradera.report.util.view.PropertiesConstants;

@ViewDepositaryBean
@LoggerCreateBean
public class CustodyDetailAgbAndSafiAndFicBean extends GenericBaseBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ParameterServiceBean parameterService;
	@EJB
	private AccountReportServiceBean accountReportService;
	@EJB
	private HelperComponentFacade helperComponentFacade;
	@EJB
	private ReportGenerationLocalServiceFacade reportGenerationFacade;
	@Inject
	private UserInfo userInfo;	
	@Inject
	Instance<PortafolioDetailCustodianReport>  pdcReport;
	
	private static final String CONFIRM_DIALOG_SUCCESS=":frmCustodyDetail:reportGenerationOk";
	
	private ReportUser reportUser;
	private List<ParameterTable> reportFormats;
	private List<ParameterTable> lstEconomicActivity;
	private List<HolderHelperOutputTO> lstFoundCUI;
	private HolderHelperOutputTO holderHelper;
	private boolean blDisabled;
	private Long reportId=new Long(8);
	
	public CustodyDetailAgbAndSafiAndFicBean() {
		blDisabled=true;
		SecuritiesTO security= new SecuritiesTO();
		holderHelper = new HolderHelperOutputTO();
		holderHelper.setDate(new Date());
		holderHelper.setSecurityTO(security);
	}
	
	@PostConstruct
	public void init(){
		loadActivityEconomic();
		loadReportFormat();
		reportUser= new ReportUser();
	}

	public void loadReportFormat(){
		ParameterTableTO parameterFilter = new ParameterTableTO();
		parameterFilter.setState(BooleanType.YES.getCode());
		List<Integer> lstParamPk = new ArrayList<Integer>();
		lstParamPk.add(ReportFormatType.TXT.getCode());
		lstParamPk.add(ReportFormatType.PDF.getCode());
		parameterFilter.setLstParameterTablePk(lstParamPk);
		reportFormats = parameterService.getListParameterTableServiceBean(parameterFilter);
	}
	
	public List<Integer> fillEconomicActivity(){
		List<Integer> lstPks= new ArrayList<Integer>();
		lstPks.add(EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode());
		lstPks.add(EconomicActivityType.FONDOS_INVERSION_CERRADOS.getCode());
		lstPks.add(EconomicActivityType.AGENCIAS_BOLSA.getCode());
		
		return lstPks;
	}
	
	public void loadActivityEconomic(){
		List<Integer> lstPks= fillEconomicActivity();
		ParameterTableTO parameterFilter = new ParameterTableTO();
		parameterFilter.setState(BooleanType.YES.getCode());
		parameterFilter.setLstParameterTablePk(lstPks);
			lstEconomicActivity = parameterService.getListParameterTableServiceBean(parameterFilter);
	}
	
	public void getFunds(){
		if(Validations.validateIsNotNullAndPositive(holderHelper.getEconomicActivity())){
			getFundCUI();
			blDisabled=false;
		}else{
			blDisabled=true;
			holderHelper.setLstEconomicActivity(fillEconomicActivity());
		}
	}

	public void getFundCUI(){
		lstFoundCUI = new ArrayList<HolderHelperOutputTO>();
		holderHelper.setHolderId(null);
		try {
			List<Object[]> list = accountReportService.getHoldersByFilter(holderHelper);
			HolderHelperOutputTO hhoTO= null;
			for(Object[] temp: list){
				hhoTO = new HolderHelperOutputTO(); 
				if(temp[0]!=null){
					hhoTO.setStrFundType(temp[0].toString());
				}
				if(temp[1]!=null){
					hhoTO.setFullName(temp[1].toString());
				}
				hhoTO.setFundCUI(Long.valueOf(temp[2].toString()));
				
				lstFoundCUI.add(hhoTO);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	public void searchSecurityByCode() throws ServiceException{
		String idSecurity = holderHelper.getSecurityTO().getSecuritiesCode();
		if(StringUtils.isNotBlank(idSecurity)){
			SecuritiesSearcherTO  searcher = new SecuritiesSearcherTO();
			searcher.setIsinCode(idSecurity);
			List<SecuritiesTO>  securityList = helperComponentFacade.findSecuritiesFromHelperNativeQuery(searcher);
			if(!securityList.isEmpty()){
				holderHelper.setSecurityTO(securityList.get(0));
			} else {
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
									PropertiesUtilities.getGenericMessage(GeneralConstants.HELPER_SECURITY_NOT_FOUND));
				JSFUtilities.showSimpleValidationDialog();
				holderHelper.getSecurityTO().setSecuritiesCode(null);
				holderHelper.getSecurityTO().setDescription(null);
			}
		} else {
			holderHelper.getSecurityTO().setSecuritiesCode(null);
			holderHelper.getSecurityTO().setDescription(null);
		}
	}
	
	@LoggerAuditWeb
	public String submitForm() {
		Map<String,String> parameters = new HashMap<String, String>();
		Map<String,String> parameterDetails = new HashMap<String, String>();
		
		reportUser.setUserName(userInfo.getUserAccountSession().getUserName());
		//how report is send for screen so for default send notification
		reportUser.setShowNotification(BooleanType.YES.getCode());
		try {
			if(Validations.validateIsNotNullAndPositive(reportId)){
				if(holderHelper.getEconomicActivity()==null){
					holderHelper.setLstEconomicActivity(fillEconomicActivity());
				}
				List<Object[]> list = accountReportService.getHoldersByFilter(holderHelper);
				HolderHelperOutputTO hhoTO= null;
				if(holderHelper.getFundCUI()!=null){
					Object[] temp = list.get(0);
					hhoTO = new HolderHelperOutputTO(); 
					if(temp[0]!=null){
						hhoTO.setStrFundType(temp[0].toString());
					}
					if(temp[1]!=null){
						hhoTO.setFullName(temp[1].toString());
					}
					hhoTO.setHolderId(Long.valueOf(temp[2].toString()));
					if(temp[3]!=null){
						hhoTO.setEconomicActivity(new BigDecimal(temp[3].toString()).intValue());
					}
					hhoTO.setSecurityTO(new SecuritiesTO());
					hhoTO.getSecurityTO().setSecuritiesCode(holderHelper.getSecurityTO().getSecuritiesCode());
					hhoTO.setDate(holderHelper.getDate());
					holderHelper = hhoTO;
				}

				parameters = getCustomJasperParameters(holderHelper);
				reportGenerationFacade.saveReportExecution(reportId, parameters, parameterDetails, reportUser);
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
												PropertiesUtilities.getGenericMessage(GeneralConstants.REPORT_GENERATED_SUCCESSFULLY));
				JSFUtilities.showSimpleValidationDialog();
//				clean();
			}else{
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(PropertiesConstants.ALERT_MESSAGE),
						PropertiesUtilities.getMessage(PropertiesConstants.REPORT_SUCESS_FAIL));
				JSFUtilities.showComponent(CONFIRM_DIALOG_SUCCESS);
			}
		} catch(Exception ex){
			ex.getStackTrace();
		}
		return null;
	}
	
	public void clean(){
		holderHelper = new HolderHelperOutputTO();
		holderHelper.setSecurityTO(new SecuritiesTO());
		holderHelper.setDate(new Date());
		blDisabled=true;
	}
	
	public Map<String, String> getCustomJasperParameters(HolderHelperOutputTO hhoTO) {
		PortafolioDetailCustodianTO pdcTO = new PortafolioDetailCustodianTO();
		Map<String, String> parametersRequired = new HashMap<String, String>();

		pdcTO.setCui(hhoTO.getHolderId());
		if(hhoTO.getFullName()!=null){
			pdcTO.setHolderDesc(hhoTO.getFullName());
		}
		if(hhoTO.getEconomicActivity()!=null){
			pdcTO.setEconomicActivity(hhoTO.getEconomicActivity());
		}else{
			pdcTO.setEconomicActivity(0);
		}
		if(hhoTO.getSecurityTO().getSecuritiesCode()!=null){
			pdcTO.setIdSecurityCode(hhoTO.getSecurityTO().getSecuritiesCode());
		}
		pdcTO.setDateMarketFact(hhoTO.getDate());
		
		//SEND PARAMETERS(REPORT FILTERS)
		if(pdcTO.getCui()!=null){
			parametersRequired.put("cui_holder", pdcTO.getCui().toString());
			parametersRequired.put("cui_description", pdcTO.getHolderDesc());
		}
		parametersRequired.put("security_pk", pdcTO.getIdSecurityCode());
		parametersRequired.put("economic_activity", pdcTO.getEconomicActivity().toString());
		parametersRequired.put("date", CommonsUtilities.convertDatetoString(pdcTO.getDateMarketFact()));
		parametersRequired.put("report_format", reportUser.getReportFormat().toString());
		
		return parametersRequired;
	}

	public List<ParameterTable> getLstEconomicActivity() {
		return lstEconomicActivity;
	}

	public void setLstEconomicActivity(List<ParameterTable> lstEconomicActivity) {
		this.lstEconomicActivity = lstEconomicActivity;
	}

	public List<HolderHelperOutputTO> getLstFoundCUI() {
		return lstFoundCUI;
	}

	public void setLstFoundCUI(List<HolderHelperOutputTO> lstFoundCUI) {
		this.lstFoundCUI = lstFoundCUI;
	}

	public HolderHelperOutputTO getHolderHelper() {
		return holderHelper;
	}

	public void setHolderHelper(HolderHelperOutputTO holderHelper) {
		this.holderHelper = holderHelper;
	}

	public boolean isBlDisabled() {
		return blDisabled;
	}

	public void setBlDisabled(boolean blDisabled) {
		this.blDisabled = blDisabled;
	}

	public ReportUser getReportUser() {
		return reportUser;
	}

	public void setReportUser(ReportUser reportUser) {
		this.reportUser = reportUser;
	}

	public List<ParameterTable> getReportFormats() {
		return reportFormats;
	}

	public void setReportFormats(List<ParameterTable> reportFormats) {
		this.reportFormats = reportFormats;
	}

}
