package com.pradera.report.generation.executor.account.to;

import java.io.Serializable;
import java.util.Date;

public class PositionsHolderParticipantTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	private Long participant;
	private String issuer;
	private Long cuiHolder;
	private String securityClass;
	private Date courtDate;
	private String courtDateReport;
	
	private String accountHolder;
	
	
	public Long getParticipant() {
		return participant;
	}
	public void setParticipant(Long participant) {
		this.participant = participant;
	}
	public String getIssuer() {
		return issuer;
	}
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	public Long getCuiHolder() {
		return cuiHolder;
	}
	public void setCuiHolder(Long cuiHolder) {
		this.cuiHolder = cuiHolder;
	}
	public String getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(String securityClass) {
		this.securityClass = securityClass;
	}
	public Date getCourtDate() {
		return courtDate;
	}
	public void setCourtDate(Date courtDate) {
		this.courtDate = courtDate;
	}
	public String getAccountHolder() {
		return accountHolder;
	}
	public void setAccountHolder(String accountHolder) {
		this.accountHolder = accountHolder;
	}
	public String getCourtDateReport() {
		return courtDateReport;
	}
	public void setCourtDateReport(String courtDateReport) {
		this.courtDateReport = courtDateReport;
	}
}
