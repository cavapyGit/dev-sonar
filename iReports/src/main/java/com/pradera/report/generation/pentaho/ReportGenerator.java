package com.pradera.report.generation.pentaho;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import org.pentaho.reporting.engine.classic.core.ClassicEngineBoot;
import org.pentaho.reporting.engine.classic.core.DataFactory;
import org.pentaho.reporting.engine.classic.core.MasterReport;
import org.pentaho.reporting.engine.classic.core.modules.output.pageable.pdf.PdfReportUtil;
import org.pentaho.reporting.engine.classic.core.modules.output.table.csv.CSVReportUtil;
import org.pentaho.reporting.engine.classic.core.modules.output.table.xls.ExcelReportUtil;
import org.pentaho.reporting.engine.classic.extensions.datasources.xpath.XPathDataFactory;
import org.pentaho.reporting.libraries.resourceloader.Resource;
import org.pentaho.reporting.libraries.resourceloader.ResourceException;
import org.pentaho.reporting.libraries.resourceloader.ResourceManager;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.report.generation.api.ReportGenerationFramework;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.service.ReportUtilServiceBean;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ReportGenerator.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/07/2013
 */
public abstract class ReportGenerator implements ReportGenerationFramework,Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The report util service. */
	@EJB
	ReportUtilServiceBean reportUtilService;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The template report url. */
	private String templateReportUrl;
	
	/** The xml data source. */
	private String xmlDataSource;
	
	/** The report query. */
	private String reportQuery;
	
	/** The sub report query. */
	private String subReportQuery;
	
	/** The report logger. */
	private ReportLogger reportLogger;
	
	/** The query name. */
	protected  String QUERY_NAME = "QueryReport";
	
	/** The query name. */
	protected  String SUB_QUERY_NAME = "SubQueryReport";
	
	/** The jndi idepositary. */
	@Inject @Configurable
	protected String jndiIdepositary;
	
	/** The folder pentaho. */
	@Inject @Configurable
	protected String folderPentaho;
	
	//parameters requireds in report
	
	/** The report title. */
	private String REPORT_TITLE="report_title";
	
	/** The user name. */
	private String USER_NAME="user_name";
	
	/** The mnemonic entity. */
	private String MNEMONIC_ENTITY="mnemonic_entity";
	
	/** The mnemonic report. */
	private String MNEMONIC_REPORT="mnemonic_report";
	
	/** The clasification report. */
	private String CLASIFICATION_REPORT="clasification_report";
	
	/** The generation date. */
	private String GENERATION_DATE="generation_date";
	
	/** The generation date. */
	private String LOGO_PATH="logo_path";

	/**
	 * Instantiates a new report generator.
	 */
	public ReportGenerator() {
		ClassicEngineBoot.getInstance().start();
	}
	
	/**
	 * Gets the report definition.
	 *
	 * @return the report definition
	 */
	public MasterReport getReportDefinition(){
		try
	    {
			//load template
			URL templateFile = this.getClass().getResource("/"+folderPentaho+"/"+templateReportUrl);
			// Parse the report file
		    final ResourceManager resourceManager = new ResourceManager();
		    resourceManager.registerDefaults();
		    final Resource directly = resourceManager.createDirectly(templateFile, MasterReport.class);
		    return (MasterReport) directly.getResource();
	    }catch(ResourceException rex){
	    	rex.printStackTrace();
	    } catch (Exception ex) {
			log.error(ex.getMessage());
		}
		return null;
	}
	
	/**
	 * Gets the data factory.
	 *
	 * @return the data factory
	 */
	public abstract DataFactory getDataFactory();
	
	/**
	 * Gets the report parameters.
	 *
	 * @return the report parameters
	 */
	public Map<String, Object> getReportParameters(){
		Map<String, Object> parametersRequired = null;
		if(reportLogger != null) {
			parametersRequired = new HashMap<String, Object>();
			parametersRequired.put(REPORT_TITLE, reportLogger.getReport().getTitle());		
			parametersRequired.put(MNEMONIC_REPORT, reportLogger.getReport().getMnemonico());
			parametersRequired.put(CLASIFICATION_REPORT, reportLogger.getReport().getReportClasification());
			parametersRequired.put(GENERATION_DATE,
					CommonsUtilities.convertDateToString(reportLogger.getLastModifyDate(), "dd/MM/yyyy hh:mm a"));
			parametersRequired.put(USER_NAME, reportLogger.getRegistryUser());
			parametersRequired.put(MNEMONIC_ENTITY,"");
			parametersRequired.put(LOGO_PATH,this.getClass().getResource(ReportConstant.LOGO_RESOURCE_PATH).getPath());
		}
		return parametersRequired;
	}
	
	/**
	 * Generate reports.
	 */
	public void generateReports(ReportLogger reportLog){
		MasterReport report=null;
		DataFactory dataFactory=null;
		try{
			report = this.getReportDefinition();
		    dataFactory = this.getDataFactory();
		    // Set the data factory for the report
		    if (dataFactory != null)
		    {
		      report.setDataFactory(dataFactory);
		    }
		 // Add any parameters to the report
		    final Map<String, Object> reportParameters = this.getReportParameters();
		    if (null != reportParameters){
		      for (String key : reportParameters.keySet()){
		        report.getParameterValues().put(key, reportParameters.get(key));
		      }
		    }

		    ByteArrayOutputStream  byteArray = null;
		    Map<ReportFormatType,byte[]> files = new HashMap<ReportFormatType, byte[]>();
		    if(reportLogger.getReport().getIndPdfFormat().equals(BooleanType.YES.getCode())) {
		    	byteArray = new ByteArrayOutputStream();
		    	PdfReportUtil.createPDF(report, byteArray);
		    	files.put(ReportFormatType.PDF, byteArray.toByteArray());
		    }
		    if(reportLogger.getReport().getIndXlsFormat().equals(BooleanType.YES.getCode())) {
		    	byteArray = new ByteArrayOutputStream();
		    	ExcelReportUtil.createXLS(report, byteArray);
		    	files.put(ReportFormatType.XLS, byteArray.toByteArray());
		    }
		    if(reportLogger.getReport().getIndTxtFormat().equals(BooleanType.YES.getCode())) {
		    	byteArray = new ByteArrayOutputStream();
		    	CSVReportUtil.createCSV(report, byteArray,"UTF-8");
		    	files.put(ReportFormatType.TXT, byteArray.toByteArray());
		    }
		    //save files
		    reportUtilService.saveReportsGeneratesTx(getReportLogger(),files);
		}catch(Exception ex){
			log.error(ex.getMessage());
			throw new RuntimeException(ex);
			//change status to failed
//			reportUtilService.updateStateReportLoggerTx(getReportLogger(), ReportLoggerStateType.FAILED.getCode(), ex.getMessage());
		} finally {
			if (dataFactory != null && dataFactory instanceof XPathDataFactory){
				XPathDataFactory xpathFactory =(XPathDataFactory) dataFactory;
				String tempXml = xpathFactory.getXqueryDataFile();
				if(tempXml != null) {
					try {
					Files.deleteIfExists(Paths.get(tempXml));
					} catch(IOException iox) {
						log.error("can't delete file "+tempXml);
						log.error(iox.getMessage());
					}
				}
			}
		}
	}
	
	@Override
	public void setCustomJasperParameters(Map<String,Object> customJasperParameters) {
		//empty method only using in jasper report
	}
	

	/**
	 * Gets the template report url.
	 *
	 * @return the template report url
	 */
	public String getTemplateReportUrl() {
		return templateReportUrl;
	}

	/**
	 * Sets the template report url.
	 *
	 * @param templateReportUrl the new template report url
	 */
	public void setTemplateReportUrl(String templateReportUrl) {
		this.templateReportUrl = templateReportUrl;
	}

	/**
	 * Gets the xml data source.
	 *
	 * @return the xml data source
	 */
	public String getXmlDataSource() {
		return xmlDataSource;
	}

	/**
	 * Sets the xml data source.
	 *
	 * @param xmlDataSource the new xml data source
	 */
	public void setXmlDataSource(String xmlDataSource) {
		this.xmlDataSource = xmlDataSource;
	}

	/**
	 * Gets the report query.
	 *
	 * @return the report query
	 */
	public String getReportQuery() {
		return reportQuery;
	}

	/**
	 * Sets the report query.
	 *
	 * @param reportQuery the new report query
	 */
	public void setReportQuery(String reportQuery) {
		this.reportQuery = reportQuery;
	}

	/**
	 * @return the subReportQuery
	 */
	public String getSubReportQuery() {
		return subReportQuery;
	}

	/**
	 * @param subReportQuery the subReportQuery to set
	 */
	public void setSubReportQuery(String subReportQuery) {
		this.subReportQuery = subReportQuery;
	}

	/**
	 * Gets the report logger.
	 *
	 * @return the report logger
	 */
	public ReportLogger getReportLogger() {
		return reportLogger;
	}

	/**
	 * Sets the report logger.
	 *
	 * @param reportLogger the new report logger
	 */
	public void setReportLogger(ReportLogger reportLogger) {
		this.reportLogger = reportLogger;
	}
	
	/**
	 * Generate reports. to accounting
	 */
	public void generateReportsByFile(byte[] arrayByte){
		MasterReport report=null;
		DataFactory dataFactory=null;
		try{
			report = this.getReportDefinition();
		    dataFactory = this.getDataFactory();
		    // Set the data factory for the report
		    if (dataFactory != null)
		    {
		      report.setDataFactory(dataFactory);
		    }
		 // Add any parameters to the report
		    final Map<String, Object> reportParameters = this.getReportParameters();
		    if (null != reportParameters)
		    {
		      for (String key : reportParameters.keySet())
		      {
		        report.getParameterValues().put(key, reportParameters.get(key));
		      }
		    }
		    
		    /**REPORT ACCOUNTING**/
		    Map<ReportFormatType,byte[]> files = new HashMap<ReportFormatType, byte[]>();
		    files.put(ReportFormatType.XLS, arrayByte);


		    
		    //save files
		    reportUtilService.saveReportsGeneratesTx(getReportLogger(),files);
		}catch(Exception ex){
			log.error(ex.getMessage());
			throw new RuntimeException(ex);
			//change status to failed
//			reportUtilService.updateStateReportLoggerTx(getReportLogger(), ReportLoggerStateType.FAILED.getCode(), ex.getMessage());
		} finally {
			if (dataFactory != null && dataFactory instanceof XPathDataFactory){
				XPathDataFactory xpathFactory =(XPathDataFactory) dataFactory;
				String tempXml = xpathFactory.getXqueryDataFile();
				if(tempXml != null) {
					try {
					Files.deleteIfExists(Paths.get(tempXml));
					} catch(IOException iox) {
						log.error("can't delete file "+tempXml);
						log.error(iox.getMessage());
 					}
				}
			}
		}
	}
	
	public void generateReportsByListFile(List<byte[]> listArrayByte) {
		
	}

}
