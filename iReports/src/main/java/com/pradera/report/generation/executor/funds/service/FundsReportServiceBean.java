package com.pradera.report.generation.executor.funds.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class FundsReportServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/03/2015
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class FundsReportServiceBean extends CrudDaoServiceBean {

    /**
     * Default constructor. 
     */
    public FundsReportServiceBean() {
        // TODO Auto-generated constructor stub
    }
    
	public String getParameterOfAccountCentralizing(GenericsFiltersTO gfto){
    	StringBuilder sbQuery = new StringBuilder();

    	sbQuery.append(" SELECT "); 
    	sbQuery.append(" (SELECT PARAMETER_NAME FROM PARAMETER_TABLE pt WHERE PARAMETER_TABLE_PK = ICA.ACCOUNT_TYPE) AS CUENTA_CENTRALIZADORA, "); 
    	sbQuery.append(" IBA.BANK_ACOUNT_TYPE TIPO_CUENTA, "); 
    	sbQuery.append(" IBA.CURRENCY TIPO_MONEDA, "); 
    	sbQuery.append(" IBA.ACCOUNT_NUMBER CUENTA, "); 
    	sbQuery.append(" ICA.ACCOUNT_STATE ESTADO "); 
    	sbQuery.append(" FROM INSTITUTION_CASH_ACCOUNT ICA,INSTITUTION_BANK_ACCOUNT IBA,CASH_ACCOUNT_DETAIL CAD ");  
    	sbQuery.append(" WHERE CAD.ID_INSTITUTION_BANK_ACCOUNT_FK=IBA.ID_INSTITUTION_BANK_ACCOUNT_PK "); 
		sbQuery.append(" AND CAD.ID_INSTITUTION_CASH_ACCOUNT_FK=ICA.ID_INSTITUTION_CASH_ACCOUNT_PK "); 
		sbQuery.append(" AND ICA.ACCOUNT_TYPE IN (2900,2901,2902,2903,2904,2905,2915) "); 
			if(Validations.validateIsNotNull(gfto.getCurrency())){
				sbQuery.append(" AND IBA.CURRENCY="+gfto.getCurrency()+" ");
			}
		sbQuery.append(" ORDER BY IBA.CURRENCY "); 
    	sbQuery.append("  ");
    	
		return sbQuery.toString();
	}
    	
	
}
