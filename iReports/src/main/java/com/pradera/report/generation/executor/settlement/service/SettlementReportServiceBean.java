package com.pradera.report.generation.executor.settlement.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.NegotiationMechanismStateType;
import com.pradera.model.accounts.type.ParticipantMechanismStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.type.AccountOperationReferenceType;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.InternationalOperationStateType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.negotiation.type.SettlementType;
import com.pradera.model.settlement.type.ChainedOperationStateType;
import com.pradera.model.settlement.type.SanctionStateType;
import com.pradera.model.settlement.type.SettlementProcessStateType;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.security.to.AcquisitionInPrimaryMarketTO;
import com.pradera.report.generation.executor.security.to.SecuritySettlementTO;
import com.pradera.report.generation.executor.securities.to.ReassignmentAccountTO;
import com.pradera.report.generation.executor.settlement.to.ChainedOperationPendingTO;
import com.pradera.report.generation.executor.settlement.to.EDVComissionSettlementOperationTo;
import com.pradera.report.generation.executor.settlement.to.GenericSettlementOperationTO;
import com.pradera.report.generation.executor.settlement.to.RegistrationAssignmentHoldersTO;
import com.pradera.report.generation.executor.settlement.to.SecuritiesSettlementTO;
import com.pradera.report.generation.executor.settlement.to.SettledOperationsTO;
import com.pradera.report.generation.executor.settlement.to.SettlementOperationTO;
import com.pradera.report.generation.executor.settlement.to.TradingReportBBVTO;
import com.pradera.report.generation.executor.settlement.to.TransferExtrabursatilSirtexTO;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlementReportServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/09/2015
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class SettlementReportServiceBean extends CrudDaoServiceBean{
	
	/** The compra venta. */
	private String COMPRA_VENTA="CVT";
	
	/** The reporto. */
	private String REPORTO="RPT";
	
	/** The colocacion primaria. */
	private String COLOCACION_PRIMARIA="CP";

	/**
	 * Instantiates a new settlement report service bean.
	 */
	public SettlementReportServiceBean(){

	}
	
	private String getQueryExchangeSecurities(TransferExtrabursatilSirtexTO filter){
		StringBuilder sd=new StringBuilder();
		sd.append(" SELECT CUS.OPERATION_DATE AS FECHA,                                                                             ");
		sd.append(" 	( SELECT MNEMONIC FROM PARTICIPANT WHERE ID_PARTICIPANT_PK = COD_DESTINO.ID_PARTICIPANT_FK                  ");
		sd.append(" 	) AS PAR_COMP,                                                                                              ");
		sd.append(" 	( SELECT MNEMONIC FROM PARTICIPANT WHERE ID_PARTICIPANT_PK = COD_ORIGEN.ID_PARTICIPANT_FK                   ");
		sd.append(" 	) PAR_VEND,                                                                                                 ");
		sd.append(" 	(SELECT LISTAGG ( H.FULL_NAME, '<br>' ) WITHIN GROUP (                                                      ");
		sd.append(" 	ORDER BY HAD.ID_HOLDER_FK )                                                                                 ");
		sd.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD                                                                              ");
		sd.append(" 	INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK                                                    ");
		sd.append(" 	WHERE HAD.ID_HOLDER_ACCOUNT_FK = COD_DESTINO.ID_HOLDER_ACCOUNT_FK                                           ");
		sd.append(" 	) CLIENTE_COMP,                                                                                             ");
		sd.append(" 	(SELECT LISTAGG ( H.FULL_NAME, '<br>' ) WITHIN GROUP (                                                      ");
		sd.append(" 	ORDER BY HAD.ID_HOLDER_FK )                                                                                 ");
		sd.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD                                                                              ");
		sd.append(" 	INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK                                                    ");
		sd.append(" 	WHERE HAD.ID_HOLDER_ACCOUNT_FK = COD_ORIGEN.ID_HOLDER_ACCOUNT_FK                                            ");
		sd.append(" 	) CLIENTE_VEND,                                                                                             ");
		sd.append(" 	(SELECT LISTAGG ( HAD.ID_HOLDER_FK, '<br>' ) WITHIN GROUP (                                                 ");
		sd.append(" 	ORDER BY HAD.ID_HOLDER_FK )                                                                                 ");
		sd.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD                                                                              ");
		sd.append(" 	INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK                                                    ");
		sd.append(" 	WHERE HAD.ID_HOLDER_ACCOUNT_FK = COD_DESTINO.ID_HOLDER_ACCOUNT_FK                                           ");
		sd.append(" 	) CUI_COMP,                                                                                                 ");
		sd.append(" 	(SELECT LISTAGG (                                                                                           ");
		sd.append(" 		( SELECT SQPT.DESCRIPTION FROM PARAMETER_TABLE SQPT WHERE SQPT.PARAMETER_TABLE_PK = H.ECONOMIC_ACTIVITY ");
		sd.append(" 		) , '<br>' ) WITHIN GROUP (                                                                             ");
		sd.append(" 	ORDER BY HAD.ID_HOLDER_FK )                                                                                 ");
		sd.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD                                                                              ");
		sd.append(" 	INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK                                                    ");
		sd.append(" 	WHERE HAD.ID_HOLDER_ACCOUNT_FK = COD_DESTINO.ID_HOLDER_ACCOUNT_FK                                           ");
		sd.append(" 	) ACT_ECONOMIC_CUI_COMP,                                                                                    ");
		sd.append(" 	(SELECT LISTAGG ( HAD.ID_HOLDER_FK, '<br>' ) WITHIN GROUP (                                                 ");
		sd.append(" 	ORDER BY HAD.ID_HOLDER_FK )                                                                                 ");
		sd.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD                                                                              ");
		sd.append(" 	INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK                                                    ");
		sd.append(" 	WHERE HAD.ID_HOLDER_ACCOUNT_FK = COD_ORIGEN.ID_HOLDER_ACCOUNT_FK                                            ");
		sd.append(" 	) CUI_VEND,                                                                                                 ");
		sd.append(" 	(SELECT LISTAGG (                                                                                           ");
		sd.append(" 		( SELECT SQPT.DESCRIPTION FROM PARAMETER_TABLE SQPT WHERE SQPT.PARAMETER_TABLE_PK = H.ECONOMIC_ACTIVITY ");
		sd.append(" 		) , '<br>' ) WITHIN GROUP (                                                                             ");
		sd.append(" 	ORDER BY HAD.ID_HOLDER_FK )                                                                                 ");
		sd.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD                                                                              ");
		sd.append(" 	INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK                                                    ");
		sd.append(" 	WHERE HAD.ID_HOLDER_ACCOUNT_FK = COD_ORIGEN.ID_HOLDER_ACCOUNT_FK                                            ");
		sd.append(" 	) ACT_ECONOMIC_CUI_VEND,                                                                                    ");
		sd.append(" 	SUBSTR ( SE.ID_SECURITY_CODE_PK,1,3 ) AS INSTRUMENTO,                                                       ");
		sd.append(" 	se.ID_SECURITY_CODE_PK SERIE_F_EDV,                                                                         ");
		sd.append(" 	i.MNEMONIC EMISOR,                                                                                          ");
		sd.append(" 	(SELECT SQPT.INDICATOR3                                                                                     ");
		sd.append(" 	FROM PARAMETER_TABLE SQPT                                                                                   ");
		sd.append(" 	WHERE SQPT.PARAMETER_TABLE_PK = Se.CURRENCY                                                                 ");
		sd.append(" 	) AS moneda,                                                                                                ");
		sd.append(" 	se.ISSUANCE_DATE F_EMISION,                                                                                 ");
		sd.append(" 	se.EXPIRATION_DATE F_VENC,                                                                                  ");
		sd.append(" 	CASE WHEN se.EXPIRATION_DATE - TRUNC ( sysdate ) < 0                                                        ");
		sd.append(" 		THEN 0                                                                                                  ");
		sd.append(" 		ELSE se.EXPIRATION_DATE - TRUNC ( sysdate )                                                             ");
		sd.append(" 	END PLAZO_RESTANTE,                                                                                         ");
		sd.append(" 	NVL (                                                                                                       ");
		sd.append(" 	(SELECT SQPT.INDICATOR3                                                                                     ");
		sd.append(" 	FROM PARAMETER_TABLE SQPT                                                                                   ");
		sd.append(" 	WHERE SQPT.PARAMETER_TABLE_PK = se.INTEREST_PAYMENT_MODALITY                                                ");
		sd.append(" 	) , 'AL VENCIMIENTO' ) FORMA_PAGO,                                                                          ");
		sd.append(" 	se.INITIAL_NOMINAL_VALUE VN_INICIAL,                                                                        ");
		sd.append(" 	se.CURRENT_NOMINAL_VALUE VN_ACTUAL,                                                                         ");
		sd.append(" 	COD_ORIGEN.AVAILABLE_BALANCE CANT_OPERACION,                                                                ");
		sd.append(" 	se.INTEREST_RATE TASA_EMISION,                                                                              ");
		sd.append(" 	NULL                     AS PLAZO_REPORTO,                                                                  ");
		sd.append(" 	NULL                     AS TIPO_PLAZO,                                                                     ");
		sd.append(" 	'INTERCAMBIO DE VALORES' AS MECANISMO,                                                                      ");
		sd.append(" 	'COMPRA/VENTA RF' MODALIDAD,                                                                                ");
		sd.append(" 	CASE WHEN i.ID_ISSUER_PK IN ( 'BO017','BO016' )                                                             ");
		sd.append(" 		THEN 'PUBLICO'                                                                                          ");
		sd.append(" 		ELSE 'PRIVADO'                                                                                          ");
		sd.append(" 	END VAL_PUB_PRIV,                                                                                           ");
		sd.append(" 	ROUND ( TIPOCAMBIO ( ( SE.INITIAL_NOMINAL_VALUE * COD_ORIGEN.AVAILABLE_BALANCE ) , CUS.OPERATION_DATE,      ");
		sd.append(" 	(SELECT SQPT.TEXT1                                                                                          ");
		sd.append(" 	FROM PARAMETER_TABLE SQPT                                                                                   ");
		sd.append(" 	WHERE SQPT.PARAMETER_TABLE_PK = Se.CURRENCY                                                                 ");
		sd.append(" 	) , 'USD' ) , 2 ) AS VNI_USD,                                                                               ");
		sd.append(" 	ROUND ( TIPOCAMBIO ( ( NVL (                                                                                ");
		sd.append(" 	( SELECT DISTINCT nvh.NOMINAL_VALUE                                                                         ");
		sd.append(" 	FROM NOMINAL_VALUE_HISTORY_OLD nvh                                                                              ");
		sd.append(" 	WHERE 0 = 0                                                                                                 ");
		sd.append(" 	AND nvh.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK                                                        ");
		sd.append(" 	AND nvh.CUT_DATE = TRUNC ( CUS.OPERATION_DATE )                                                             ");
		sd.append(" 	) ,SE.CURRENT_NOMINAL_VALUE ) * COD_ORIGEN.AVAILABLE_BALANCE ) , CUS.OPERATION_DATE,                        ");
		sd.append(" 	( SELECT SQPT.TEXT1 FROM PARAMETER_TABLE SQPT WHERE SQPT.PARAMETER_TABLE_PK = Se.CURRENCY                   ");
		sd.append(" 	) , 'USD' ) , 2 ) AS VNA_USD,                                                                               ");
		sd.append(" 	NULL              AS FECHA_LIQ_CONT,                                                                        ");
		sd.append(" 	NULL              AS FECHA_LIQ_PLA                                                                          ");
		sd.append(" FROM SECURITY SE,                                                                                               ");
		sd.append(" 	ISSUER I,                                                                                                   ");
		sd.append(" 	CHANGE_OWNERSHIP_OPERATION COO,                                                                             ");
		sd.append(" 	CHANGE_OWNERSHIP_DETAIL COD_ORIGEN,                                                                         ");
		sd.append(" 	CHANGE_OWNERSHIP_DETAIL COD_DESTINO,                                                                        ");
		sd.append(" 	CUSTODY_OPERATION CUS                                                                                       ");
		sd.append(" WHERE COO.ID_CHANGE_OWNERSHIP_PK = COD_ORIGEN.ID_CHANGE_OWNERSHIP_FK                                            ");
		sd.append(" AND COD_ORIGEN.IND_ORIGIN_TARGET = 1                                                                            ");
		sd.append(" AND COO.ID_CHANGE_OWNERSHIP_PK = COD_DESTINO.ID_CHANGE_OWNERSHIP_FK                                             ");
		sd.append(" AND COD_DESTINO.IND_ORIGIN_TARGET = 2                                                                           ");
		sd.append(" AND COD_ORIGEN.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK                                                     ");
		sd.append(" AND COO.ID_CHANGE_OWNERSHIP_PK = CUS.ID_CUSTODY_OPERATION_PK                                                    ");
		sd.append(" AND I.ID_ISSUER_PK = SE.ID_ISSUER_FK                                                                            ");
		sd.append(" AND coo.state = 865                                                                                             ");
		sd.append(" AND COO.IND_EXCHANGE_SECURITIES = 1																				");
		sd.append(" AND ( TRUNC ( CUS.OPERATION_DATE ) BETWEEN ( TO_DATE ('"+filter.getInitialDate()+"','dd/MM/yyyy' ) ) AND ( TO_DATE ('"+filter.getFinalDate()+"','dd/MM/yyyy' ) ) ) ");
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdParticipant())){
			sd.append(" and (COD_DESTINO.ID_PARTICIPANT_FK = :idParticipant or COD_ORIGEN.ID_PARTICIPANT_FK  = :idParticipant) ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdIssuer())){
			sd.append(" AND i.ID_ISSUER_PK = :idIssuer ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getSecurityClass())){
			sd.append(" AND Se.SECURITY_CLASS = :securityClass ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdSecurityCodePk())){
			sd.append(" and se.ID_SECURITY_CODE_PK = :idSecurityCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getCurrency())){
			sd.append(" AND SE.CURRENCY = :currency ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getMecanismoNeg())){
			try {
				Integer mecanismo = Integer.parseInt(String.valueOf(filter.getMecanismoNeg()));
				if(!mecanismo.equals(GeneralConstants.SIX_VALUE_INTEGER)){
					// si el mecanismo es distinto de 6 (mecanismo de ITERCAMBIO DE VALORES (mecanismo artificial)) entonces no se mostrarA los datos de la query de intercambio de valores
					// para ello, colocamos una condicion contradictoria para no mostrar datos
					sd.append(" AND 1=0 "); // aplicamos esto porque la query q lo llama antepone un 'union'
				}
			} catch (Exception e) {
				System.out.println("::: Error parseo de mecanismo (INTERCAMBIO DE VALORES) "+ e.getMessage());
			}
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getModalidadNeg())){
			try {
				Integer mecanismo = Integer.parseInt(String.valueOf(filter.getModalidadNeg()));
				// verificando si es distinto de COMPRA/VENTA
				if(!mecanismo.equals(Integer.parseInt("1")) && !mecanismo.equals(Integer.parseInt("2"))){
					// si la modalidad es distinto de 1 y 2 entonces no se mostrarA los datos de la query de intercambio de valores
					// para ello, colocamos una condicion contradictoria para no mostrar datos
					sd.append(" AND 1=0 "); // aplicamos esto porque la query q lo llama antepone un 'union'
				}
			} catch (Exception e) {
				System.out.println("::: Error parseo de mecanismo (INTERCAMBIO DE VALORES) "+ e.getMessage());
			}
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getFormaPago())){
			sd.append(" AND sec.INTEREST_PAYMENT_MODALITY = :formaPago ");
		}
		
		return sd.toString();
	}
	
	public String getQueryTransferExtrabursatilSirtex(TransferExtrabursatilSirtexTO filter){
		StringBuilder sd=new StringBuilder();
		
		sd.append(" select                                                                                                                       ");
		sd.append("   FECHA, PAR_COMP, PAR_VEND, CLIENTE_COMP,                                                                                   ");
		sd.append("   CLIENTE_VEND, CUI_COMP, ACT_ECONOMIC_CUI_COMP,                                                                             ");
		sd.append("   CUI_VEND, ACT_ECONOMIC_CUI_VEND,                                                                                           ");
		sd.append("   INSTRUMENTO, SERIE_F_EDV, EMISOR, MONEDA, F_EMISION, F_VENC,                                                               ");
		sd.append("   PLAZO_RESTANTE, FORMA_PAGO, VN_INICIAL, VN_ACTUAL,                                                                         ");
		sd.append("   CANT_OPERACION, TASA_EMISION, PLAZO_REPORTO, TIPO_PLAZO,                                                                   ");
		sd.append("   MECANISMO, MODALIDAD, VAL_PUB_PRIV, VNI_USD, VNA_USD,                                                             		 ");
		sd.append("   FECHA_LIQ_CONT, FECHA_LIQ_PLA                                                                                              ");
		sd.append(" from                                                                                                                         ");
		sd.append(" (select                                                                                                                      ");
		sd.append(" MO.REGISTER_DATE FECHA,                                                                                                      ");
		sd.append(" pbuyer.MNEMONIC PAR_COMP,                                                                                                    ");
		sd.append(" pseller.MNEMONIC PAR_VEND,                                                                                                   ");

		sd.append(" (select LISTAGG (h_sub.FULL_NAME, ', ' )                                                                                     ");
		sd.append("     WITHIN GROUP (ORDER BY had_sub.ID_HOLDER_FK)                                                                             ");
		sd.append("      from HOLDER_ACCOUNT_OPERATION hao_sub                                                                                   ");
		sd.append("      inner join HOLDER_ACCOUNT_DETAIL had_sub on had_sub.ID_HOLDER_ACCOUNT_FK = hao_sub.ID_HOLDER_ACCOUNT_FK                 ");
		sd.append("      inner join HOLDER h_sub on h_sub.ID_HOLDER_PK = had_sub.ID_HOLDER_FK                                                    ");
		sd.append("      where hao_sub.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK                                                  ");
		sd.append("      and hao_sub.ROLE = 1                                                                                                    ");
		sd.append("      and hao_sub.OPERATION_PART = 1                                                                                          ");
		sd.append("     ) CLIENTE_COMP,                                                                                                          ");

		sd.append(" (select LISTAGG (h_sub.FULL_NAME, ', ' )                                                                                     ");
		sd.append("     WITHIN GROUP (ORDER BY had_sub.ID_HOLDER_FK)                                                                             ");
		sd.append("      from HOLDER_ACCOUNT_OPERATION hao_sub                                                                                   ");
		sd.append("      inner join HOLDER_ACCOUNT_DETAIL had_sub on had_sub.ID_HOLDER_ACCOUNT_FK = hao_sub.ID_HOLDER_ACCOUNT_FK                 ");
		sd.append("      inner join HOLDER h_sub on h_sub.ID_HOLDER_PK = had_sub.ID_HOLDER_FK                                                    ");
		sd.append("      where hao_sub.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK                                                  ");
		sd.append("      and hao_sub.ROLE = 2                                                                                                    ");
		sd.append("      and hao_sub.OPERATION_PART = 1                                                                                          ");
		sd.append("     ) CLIENTE_VEND,                                                                                                          ");

		sd.append(" (select LISTAGG (had_sub.ID_HOLDER_FK, ', ' )                                                                                ");
		sd.append("     WITHIN GROUP (ORDER BY had_sub.ID_HOLDER_FK)                                                                             ");
		sd.append("      from HOLDER_ACCOUNT_OPERATION hao_sub                                                                                   ");
		sd.append("      inner join HOLDER_ACCOUNT_DETAIL had_sub on had_sub.ID_HOLDER_ACCOUNT_FK = hao_sub.ID_HOLDER_ACCOUNT_FK                 ");
		sd.append("      where hao_sub.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK                                                  ");
		sd.append("      and hao_sub.ROLE = 1                                                                                                    ");
		sd.append("      and hao_sub.OPERATION_PART = 1                                                                                          ");
		sd.append("     ) CUI_COMP,                                                                                                              ");

		sd.append(" (select LISTAGG ((select PARAMETER_NAME from PARAMETER_TABLE where PARAMETER_TABLE_PK = h_sub.ECONOMIC_ACTIVITY), ', ' )     ");
		sd.append("     WITHIN GROUP (ORDER BY had_sub.ID_HOLDER_FK)                                                                             ");
		sd.append("      from HOLDER_ACCOUNT_OPERATION hao_sub                                                                                   ");
		sd.append("      inner join HOLDER_ACCOUNT_DETAIL had_sub on had_sub.ID_HOLDER_ACCOUNT_FK = hao_sub.ID_HOLDER_ACCOUNT_FK                 ");
		sd.append("      inner join HOLDER h_sub on h_sub.ID_HOLDER_PK = had_sub.ID_HOLDER_FK                                                    ");
		sd.append("      where hao_sub.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK                                                  ");
		sd.append("      and hao_sub.ROLE = 1                                                                                                    ");
		sd.append("      and hao_sub.OPERATION_PART = 1                                                                                          ");
		sd.append("     ) ACT_ECONOMIC_CUI_COMP,                                                                                                 ");

		sd.append(" (select LISTAGG (had_sub.ID_HOLDER_FK, ', ' )                                                                                ");
		sd.append("      WITHIN GROUP (ORDER BY had_sub.ID_HOLDER_FK)                                                                            ");
		sd.append("      from HOLDER_ACCOUNT_OPERATION hao_sub                                                                                   ");
		sd.append("      inner join HOLDER_ACCOUNT_DETAIL had_sub on had_sub.ID_HOLDER_ACCOUNT_FK = hao_sub.ID_HOLDER_ACCOUNT_FK                 ");
		sd.append("      where hao_sub.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK                                                  ");
		sd.append("      and hao_sub.ROLE = 2                                                                                                    ");
		sd.append("      and hao_sub.OPERATION_PART = 1                                                                                          ");
		sd.append("     ) CUI_VEND,                                                                                                              ");

		sd.append(" (select LISTAGG ((select PARAMETER_NAME from PARAMETER_TABLE where PARAMETER_TABLE_PK = h_sub.ECONOMIC_ACTIVITY), ', ' )     ");
		sd.append("     WITHIN GROUP (ORDER BY had_sub.ID_HOLDER_FK)                                                                             ");
		sd.append("      from HOLDER_ACCOUNT_OPERATION hao_sub                                                                                   ");
		sd.append("      inner join HOLDER_ACCOUNT_DETAIL had_sub on had_sub.ID_HOLDER_ACCOUNT_FK = hao_sub.ID_HOLDER_ACCOUNT_FK                 ");
		sd.append("      inner join HOLDER h_sub on h_sub.ID_HOLDER_PK = had_sub.ID_HOLDER_FK                                                    ");
		sd.append("      where hao_sub.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK                                                  ");
		sd.append("      and hao_sub.ROLE = 2                                                                                                    ");
		sd.append("      and hao_sub.OPERATION_PART = 1                                                                                          ");
		sd.append("     ) ACT_ECONOMIC_CUI_VEND,                                                                                                 ");

		sd.append(" ptsec.TEXT1 INSTRUMENTO,                                                                                                     ");
		sd.append(" sec.ID_SECURITY_CODE_PK SERIE_F_EDV,                                                                                         ");
		sd.append(" iss.MNEMONIC EMISOR,                                                                                                         ");
		sd.append(" ptcurr.INDICATOR3 MONEDA,                                                                                                    ");
		sd.append(" sec.ISSUANCE_DATE F_EMISION,                                                                                                 ");
		sd.append(" sec.EXPIRATION_DATE F_VENC,                                                                                                  ");
		sd.append(" CASE WHEN sec.EXPIRATION_DATE - trunc(sysdate) < 0 THEN 0                                                                    ");
		sd.append("     ELSE sec.EXPIRATION_DATE - trunc(sysdate) END PLAZO_RESTANTE,                                                            ");
		sd.append(" nvl(ptint.DESCRIPTION,'AL VENCIMIENTO') FORMA_PAGO,                                                                          ");
		sd.append(" sec.INITIAL_NOMINAL_VALUE VN_INICIAL,                                                                                        ");
		sd.append(" sec.CURRENT_NOMINAL_VALUE VN_ACTUAL,                                                                                         ");
		sd.append("                                                                                                                              ");
		sd.append(" MO.STOCK_QUANTITY CANT_OPERACION,                                                                                            ");
		sd.append(" sec.INTEREST_RATE TASA_EMISION,                                                                                              ");
		sd.append(" MO.TERM_SETTLEMENT_DAYS PLAZO_REPORTO,                                                                                       ");
		sd.append(" CASE WHEN MO.TERM_SETTLEMENT_DAYS BETWEEN 1 AND 10 THEN 'CP'                                                                 ");
		sd.append("      WHEN MO.TERM_SETTLEMENT_DAYS BETWEEN 11 AND 30 THEN 'MP'                                                                ");
		sd.append("      WHEN MO.TERM_SETTLEMENT_DAYS BETWEEN 31 AND 45 THEN 'LP'                                                                ");
		sd.append("      WHEN MO.TERM_SETTLEMENT_DAYS IS NULL THEN NULL END                                                                      ");
		sd.append(" TIPO_PLAZO,                                                                                                                  ");
		
		sd.append(" NM.DESCRIPTION MECANISMO,                                                                                                    ");
		sd.append(" NMM.MODALITY_NAME MODALIDAD,                                                                                                 ");
		sd.append("                                                                                                                              ");
		sd.append(" CASE WHEN iss.ID_ISSUER_PK in ('BO017','BO016') THEN 'PUBLICO'                                                               ");
		sd.append("      ELSE 'PRIVADO' END                                                                                                      ");
		sd.append(" VAL_PUB_PRIV,                                                                                                                ");

		sd.append(" round(((SEC.INITIAL_NOMINAL_VALUE * MO.STOCK_QUANTITY) *                                                                     ");
		sd.append(" CASE WHEN sec.CURRENCY = 127 THEN 1 /                                                                                        ");
		sd.append("       nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = trunc(MO.REGISTER_DATE)                ");
		sd.append("                       AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)                                          ");
		sd.append("       WHEN sec.CURRENCY IN (1734,1304) THEN 1 *                                                                              ");
		sd.append("       nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = trunc(MO.REGISTER_DATE)                ");
		sd.append("                       AND D.ID_CURRENCY = sec.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /                               ");
		sd.append("       nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = trunc(MO.REGISTER_DATE)                ");
		sd.append("                       AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)                                          ");
		sd.append("       WHEN sec.CURRENCY IN (430,1853) THEN 1 ELSE 1 END),2) VNI_USD,                                                         ");
		
		sd.append(" round((nvl((select distinct nvh.NOMINAL_VALUE from NOMINAL_VALUE_HISTORY_OLD nvh                                                 ");
		sd.append(" where 0=0                                                                                                                    ");
		sd.append(" and nvh.ID_SECURITY_CODE_FK = SEC.ID_SECURITY_CODE_PK                                                                        ");
		sd.append(" and nvh.CUT_DATE = trunc(MO.REGISTER_DATE)                                                                                   ");
		sd.append(" ),SEC.CURRENT_NOMINAL_VALUE)* MO.STOCK_QUANTITY) *                                                                           ");
		sd.append(" CASE WHEN sec.CURRENCY = 127 THEN 1 /                                                                                        ");
		sd.append("       nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = trunc(MO.REGISTER_DATE)                ");
		sd.append("                       AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)                                          ");
		sd.append("       WHEN sec.CURRENCY IN (1734,1304) THEN 1 *                                                                              ");
		sd.append("       nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = trunc(MO.REGISTER_DATE)                ");
		sd.append("                       AND D.ID_CURRENCY = sec.CURRENCY AND D.ID_SOURCE_INFORMATION = 212),1) /                               ");
		sd.append("       nvl(( SELECT D.BUY_PRICE FROM DAILY_EXCHANGE_RATES D WHERE TRUNC(D.DATE_RATE) = trunc(MO.REGISTER_DATE)                ");
		sd.append("                       AND D.ID_CURRENCY = 430 AND D.ID_SOURCE_INFORMATION = 212),1)                                          ");
		sd.append("       WHEN sec.CURRENCY IN (430,1853) THEN 1 ELSE 1 END, 2) VNA_USD,                                                         ");

		sd.append(" MO.REGISTER_DATE FECHA_LIQ_CONT,                                                                                             ");
		sd.append(" MO.TERM_SETTLEMENT_DATE FECHA_LIQ_PLA                                                                                        ");
		sd.append(" from MECHANISM_OPERATION MO                                                                                                  ");
		
		sd.append(" left JOIN TRADE_OPERATION TOPE ON MO.ID_MECHANISM_OPERATION_PK=TOPE.ID_TRADE_OPERATION_PK  		   ");         
		sd.append(" left JOIN TRADE_REQUEST TREQ ON TOPE.ID_TRADE_REQUEST_FK=TREQ.ID_TRADE_REQUEST_PK             		   ");
		
		sd.append(" inner join participant pbuyer on pbuyer.ID_PARTICIPANT_PK = MO.ID_BUYER_PARTICIPANT_FK                                       ");
		sd.append(" inner join participant pseller on pseller.ID_PARTICIPANT_PK = MO.ID_SELLER_PARTICIPANT_FK                                    ");
		sd.append(" inner join security sec on sec.ID_SECURITY_CODE_PK = MO.ID_SECURITY_CODE_FK                                                  ");
		sd.append(" inner join parameter_table ptsec on ptsec.PARAMETER_TABLE_PK = sec.SECURITY_CLASS                                            ");
		sd.append(" inner join issuer iss on iss.ID_ISSUER_PK = sec.ID_ISSUER_FK                                                                 ");
		sd.append(" inner join parameter_table ptcurr on ptcurr.PARAMETER_TABLE_PK = sec.CURRENCY                                                ");
		sd.append(" left join parameter_table ptint on ptint.PARAMETER_TABLE_PK = sec.INTEREST_PAYMENT_MODALITY                                 ");
		sd.append(" inner join NEGOTIATION_MECHANISM NM on MO.ID_NEGOTIATION_MECHANISM_FK = NM.ID_NEGOTIATION_MECHANISM_PK                       ");
		sd.append(" inner join NEGOTIATION_MODALITY NMM on MO.ID_NEGOTIATION_MODALITY_FK = NMM.ID_NEGOTIATION_MODALITY_PK                        ");
		sd.append(" where ");
		sd.append(" MO.ID_NEGOTIATION_MECHANISM_FK in (3,5)                                                                                      ");
		
		sd.append(" And (TREQ.OPERATION_STATE in (616,1866) or TOPE.OPERATION_STATE = 1131) "); // TREQ.confirmado , TREQ.liquidada plazo o TOPE.confirmada(para OTC)
		
		//NO TE OLVIDES COLOCAR TRUNC
		sd.append(" and TRUNC(MO.REGISTER_DATE) BETWEEN  to_date('"+filter.getInitialDate()+"','DD/MM/YYYY') AND to_date('"+filter.getFinalDate()+"','DD/MM/YYYY') ");

		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdParticipant())){
			sd.append(" and (pbuyer.ID_PARTICIPANT_PK = :idParticipant or pseller.ID_PARTICIPANT_PK = :idParticipant) ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdIssuer())){
			sd.append(" AND iss.ID_ISSUER_PK = :idIssuer ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getSecurityClass())){
			sd.append(" AND ptsec.PARAMETER_TABLE_PK = :securityClass ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdSecurityCodePk())){
			sd.append(" and sec.ID_SECURITY_CODE_PK = :idSecurityCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getCurrency())){
			sd.append(" AND ptcurr.PARAMETER_TABLE_PK = :currency ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getMecanismoNeg())){
			sd.append(" AND NM.ID_NEGOTIATION_MECHANISM_PK = :mecanismoNeg ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getModalidadNeg())){
			sd.append(" AND NMM.ID_NEGOTIATION_MODALITY_PK = :modalidadNeg ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getFormaPago())){
			sd.append(" AND sec.INTEREST_PAYMENT_MODALITY = :formaPago ");
		}
		
		//issue 1379: se adicona la query de INTERCAMBIO DE VALORES
		sd.append(" union ");
		sd.append(getQueryExchangeSecurities(filter));
		
		sd.append(" ) sq                                                                                                                         ");
		sd.append(" where 0=0 ");
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdHolder())){
			sd.append(" and (sq.CUI_COMP = :idHolder or CUI_VEND = :idHolder) ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getValPublicOrPrivate())){
			sd.append(" AND VAL_PUB_PRIV = :valPuPriv ");
		}
		
		sd.append(" order by FECHA,PAR_COMP,SERIE_F_EDV asc																						 ");
		
		// reemplazando los parametros 
		String query=sd.toString();
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdParticipant())){
			query=query.replace(":idParticipant", filter.getIdParticipant());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdIssuer())){
			query=query.replace(":idIssuer", "'"+filter.getIdIssuer()+"'");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getSecurityClass())){
			query=query.replace(":securityClass", filter.getSecurityClass());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdSecurityCodePk())){
			query=query.replace(":idSecurityCodePk", "'"+filter.getIdSecurityCodePk()+"'");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getCurrency())){
			query=query.replace(":currency", filter.getCurrency());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getMecanismoNeg())){
			query=query.replace(":mecanismoNeg", filter.getMecanismoNeg());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getModalidadNeg())){
			query=query.replace(":modalidadNeg", filter.getModalidadNeg());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getFormaPago())){
			query=query.replace(":formaPago", filter.getFormaPago());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdHolder())){
			query=query.replace(":idHolder", "'"+filter.getIdHolder()+"'");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getValPublicOrPrivate())){
			query=query.replace(":valPuPriv", "'"+filter.getValPublicOrPrivate()+"'");
		}
		
		return query;
	}
	
	/**
	 * issue 1166 query que tiene el resumen de comisiones y aportes de las agencias de bolsa
	 * @param parameterTo
	 * @return
	 */
	public String getEDVCommisionSettlementOperationReport(EDVComissionSettlementOperationTo parameterTo){
		StringBuilder sd=new StringBuilder();
		sd.append(" SELECT                                                                                                             ");
		sd.append("   RM.MNEMONIC_ENTITY_COLLECTION AS MNEMONICO_RESUME,                                                               ");
		sd.append("   RM.NAME_ENTITY_COLLECTION AS NAME_RESUME,                                                                        ");
		sd.append("   ROUND(SUM(RM.RATE_PERCENT), 2) AS COMISION_RESUME,                                                               ");
		sd.append("   ROUND(SUM(RM.APORTE*RM.DAILY_EXCHANGE_RATES), 2) AS APORTE_RESUME                                                ");
		sd.append(" FROM                                                                                                               ");
		sd.append(" (SELECT                                                                                                            ");
		sd.append("   BS.ENTITY_COLLECTION,                                                                                            ");

		sd.append("   CASE BS.ENTITY_COLLECTION                                                                                        ");
		sd.append("     WHEN 1406 THEN (SELECT ISS.MNEMONIC FROM ISSUER ISS WHERE ISS.ID_ISSUER_PK =CR.ID_ISSUER_FK )                  ");
		sd.append("     WHEN 1407 THEN (SELECT PAR.MNEMONIC FROM PARTICIPANT PAR WHERE PAR.ID_PARTICIPANT_PK= CR.ID_PARTICIPANT_FK )   ");
		sd.append("     WHEN 1408	THEN (SELECT HOL.MNEMONIC FROM HOLDER HOL WHERE HOL.ID_HOLDER_PK=CR.ID_HOLDER_FK)                  ");
		sd.append("     WHEN 1530	THEN NVL(CR.OTHER_ENTITY_DOCUMENT, ' ')                                                            ");
		sd.append("   END MNEMONIC_ENTITY_COLLECTION,                                                                                  ");

		sd.append("   CASE BS.ENTITY_COLLECTION                                                                                        ");
		sd.append("     WHEN 1406 THEN NVL(CR.ID_ISSUER_FK , ' ')                                                                      ");
		sd.append("     WHEN 1407 THEN CR.ID_PARTICIPANT_FK || ' '                                                                     ");
		sd.append("     WHEN 1408	THEN CR.ID_HOLDER_FK || ' '                                                                        ");
		sd.append("     WHEN 1530	THEN NVL(CR.OTHER_ENTITY_DOCUMENT, ' ')                                                            ");
		sd.append("   END CODE_ENTITY_COLLECTION,                                                                                      ");

		sd.append("   CASE BS.ENTITY_COLLECTION                                                                                        ");
		sd.append("     WHEN 1406 THEN (SELECT ISS.BUSINESS_NAME FROM ISSUER ISS WHERE ISS.ID_ISSUER_PK =CR.ID_ISSUER_FK )             ");
		sd.append("     WHEN 1407 THEN (SELECT PAR.DESCRIPTION FROM PARTICIPANT PAR WHERE PAR.ID_PARTICIPANT_PK= CR.ID_PARTICIPANT_FK )");
		sd.append("     WHEN 1408	THEN (SELECT HOL.FULL_NAME FROM HOLDER HOL WHERE HOL.ID_HOLDER_PK=CR.ID_HOLDER_FK)                 ");
		sd.append("     WHEN 1530	THEN NVL(CR.OTHER_ENTITY_DESCRIPTION, ' ')                                                         ");
		sd.append("   END NAME_ENTITY_COLLECTION,                                                                                      ");

		sd.append("   NM.MODALITY_CODE,                                                                                                ");
		sd.append("   NM.MODALITY_NAME,                                                                                                ");
		sd.append("   CRD.ID_HOLDER_ACCOUNT_FK,                                                                                        ");
		sd.append("   HA.ACCOUNT_NUMBER,                                                                                               ");
		sd.append("   HA.DESCRIPTION,                                                                                                  ");
		sd.append("   FN_GET_HOLDER_NAME(HA.ID_HOLDER_ACCOUNT_PK) CUI_TITULAR,                                                         ");
		sd.append("   SE.SECURITY_CLASS,                                                                                               ");
		sd.append("   CRD.CURRENCY_RATE,                                                                                               ");
		sd.append("   TO_CHAR(CRD.OPERATION_DATE,'DD/MON/YY') OPERATION_DATE,                                                          ");
		sd.append("   CRD.BALLOT_SEQ,                                                                                                  ");
		sd.append("   SE.ID_SECURITY_CODE_ONLY,                                                                                        ");
		sd.append("   CRD.OPERATION_PRICE,                                                                                             ");
		sd.append("   CRD.MOVEMENT_QUANTITY CANTIDAD,                                                                                  ");
		sd.append("   CRD.NEGOTIATED_AMOUNT AS MONTO_NEG_DOLARES,                                                                      ");
		sd.append("   CRD.NEGOTIATED_AMOUNT_ORIGIN MONTO_NEG_ORIGINAL,                                                                 ");
		sd.append("   DECODE(CRD.ROLE,1,'COMPRA',2,'VENTA') ROLE,                                                                      ");
		sd.append("   CRD.RATE_PERCENT,                                                                                                ");
		sd.append("   CRD.RATE_AMOUNT_BILLED ,                                                                                         ");
		sd.append("   (CRD.RATE_PERCENT * 0.10) APORTE,                                                                                ");
		sd.append("   NVL((SELECT DER.SELL_PRICE                                                                                       ");
		sd.append("      FROM DAILY_EXCHANGE_RATES DER                                                                                 ");
		sd.append("      WHERE TRUNC(DER.DATE_RATE) = to_date('"+parameterTo.getFinalDate()+"','dd/mm/yyyy')                                               ");
		sd.append("        AND DER.ID_CURRENCY = 430                                                                                   ");
		sd.append("        AND ID_SOURCE_INFORMATION = 212),1) DAILY_EXCHANGE_RATES,                                                   ");
		sd.append(" 'COMISION (USD)' AS CROSSTAB_COMMISION,                                                                            ");
		sd.append("   'APORTE FONDO DE GARANTIA (Bs.)' AS CROSSTAB_CONTRIBUTION_FUND                                                   ");

		sd.append(" FROM COLLECTION_RECORD CR, COLLECTION_RECORD_DETAIL CRD, PROCESSED_SERVICE PS,                                     ");
		sd.append("   BILLING_SERVICE BS, SERVICE_RATE SR, HOLDER_ACCOUNT HA, NEGOTIATION_MODALITY NM,                                 ");
		sd.append("   SECURITY SE                                                                                                      ");

		sd.append(" WHERE BS.ID_BILLING_SERVICE_PK = PS.ID_BILLING_SERVICE_FK                                                          ");
		sd.append("   AND PS.ID_PROCESSED_SERVICE_PK = CR.ID_PROCESSED_SERVICE_FK                                                      ");
		sd.append("   AND CR.ID_COLLECTION_RECORD_PK = CRD.ID_COLLECTION_RECORD_FK                                                     ");
		sd.append("   AND SR.ID_BILLING_SERVICE_FK = BS.ID_BILLING_SERVICE_PK                                                          ");
		sd.append("   AND HA.ID_HOLDER_ACCOUNT_PK = CRD.ID_HOLDER_ACCOUNT_FK                                                           ");
		sd.append("   AND BS.BASE_COLLECTION IN(1414,1803)                                                                             ");
		sd.append("   AND NM.ID_NEGOTIATION_MODALITY_PK = CRD.MODALITY                                                                 ");
		sd.append("   AND CRD.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK                                                             ");
		sd.append("   AND SR.RATE_TYPE=1524                                                                                            ");
		
		if(Validations.validateIsNotNullAndNotEmpty(parameterTo.getModality()))
			sd.append("   AND CRD.MODALITY = :modality                                                        ");
		
		if(Validations.validateIsNotNullAndNotEmpty(parameterTo.getParticipant()))
			sd.append("   AND CR.ID_PARTICIPANT_FK = :participant                                          ");
		
		if(Validations.validateIsNotNullAndNotEmpty(parameterTo.getCuiCode())){
			sd.append("   AND (                                                                                     ");
			sd.append("     (EXISTS                                                                                                        ");
			sd.append("       (SELECT had.ID_HOLDER_FK                                                                                     ");
			sd.append("       FROM HOLDER_ACCOUNT_DETAIL had                                                                               ");
			sd.append("       WHERE had.ID_HOLDER_FK = :cui_code                                                                        ");
			sd.append("         AND had.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK                                                     ");
			sd.append("     )))                                                                                                            ");
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(parameterTo.getHolderAccount()))
			sd.append("   AND HA.ID_HOLDER_ACCOUNT_PK = :holder_account                                 ");
		
		if(Validations.validateIsNotNullAndNotEmpty(parameterTo.getSecurityClass()))
			sd.append("   AND SE.SECURITY_CLASS = :security_class                                       ");
		
		sd.append("   AND trunc(CR.CALCULATION_DATE) between to_date('"+parameterTo.getInitialDate()+"','dd/mm/yyyy')                                        ");
		sd.append("   AND to_date('"+parameterTo.getFinalDate()+"','dd/mm/yyyy')                                                                           ");
		sd.append("                                                                                                                    ");
		sd.append(" ORDER BY                                                                                                           ");
		sd.append("   BS.ENTITY_COLLECTION,                                                                                            ");
		sd.append("   MNEMONIC_ENTITY_COLLECTION,                                                                                      ");
		sd.append("   NM.MODALITY_NAME,                                                                                                ");
		sd.append("   crd.ID_HOLDER_ACCOUNT_FK,                                                                                        ");
		sd.append("   CRD.SECURITY_CLASS,                                                                                              ");
		sd.append("   CRD.CURRENCY_RATE,                                                                                               ");
		sd.append("   CR.CALCULATION_DATE) RM                                                                                          ");
		sd.append(" GROUP BY RM.MNEMONIC_ENTITY_COLLECTION, RM.NAME_ENTITY_COLLECTION                                                  ");
		sd.append(" ORDER BY RM.MNEMONIC_ENTITY_COLLECTION, RM.NAME_ENTITY_COLLECTION												   ");
		
		String strQueryFormatted = sd.toString();
		
		if(Validations.validateIsNotNullAndNotEmpty(parameterTo.getModality())){
			strQueryFormatted = strQueryFormatted.replace(":modality", parameterTo.getModality());
		}
		if(Validations.validateIsNotNullAndNotEmpty(parameterTo.getParticipant())){
			strQueryFormatted = strQueryFormatted.replace(":participant", parameterTo.getParticipant());
		}
		if(Validations.validateIsNotNullAndNotEmpty(parameterTo.getCuiCode())){
			strQueryFormatted = strQueryFormatted.replace(":cui_code", parameterTo.getCuiCode());
		}
		if(Validations.validateIsNotNullAndNotEmpty(parameterTo.getHolderAccount())){
			strQueryFormatted = strQueryFormatted.replace(":holder_account", parameterTo.getHolderAccount());
		}
		if(Validations.validateIsNotNullAndNotEmpty(parameterTo.getSecurityClass())){
			strQueryFormatted = strQueryFormatted.replace(":security_class", parameterTo.getSecurityClass());
		}
		
		return strQueryFormatted;
	}

	/**
	 * Gets the sent and reception international securities.
	 *
	 * @return the sent and reception international securities
	 */
	public List<Object[]> getSentAndReceptionInternationalSecurities(){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("     SELECT ");
		sbQuery.append("   		    IO.ID_LOCAL_PARTICIPANT_FK,");
		sbQuery.append("   		    S.ID_ISIN_CODE_PK,");
		sbQuery.append("   		    OT.ID_OPERATION_TYPE_PK,");
		sbQuery.append("            SUM(DECODE(io.operation_state,:registeredState,1,0))  AS REGISTRADA,");
		sbQuery.append("		    SUM(DECODE(io.operation_state,:liquidationState,1,0)) AS LIQUIDADA,");
		sbQuery.append("            SUM(DECODE(io.operation_state,:anullatedState,1,0))   AS ANULADA,");
		sbQuery.append("            SUM(DECODE(io.operation_state,:cancelledState,1,0))   AS CANCELADA,");
		sbQuery.append("		    SUM(DECODE(io.operation_state,:approvedState,1,0)) 	  AS APROBADA,");		
		sbQuery.append("            SUM(DECODE(io.operation_state,:confirmedState,1,0))   AS CONFIRMADA,");
		sbQuery.append("            SUM(DECODE(io.operation_state,:rejectedState,1,0))    AS RECHAZADA,");
		sbQuery.append("			P.MNEMONIC 	  || ' - ' || P.ID_PARTICIPANT_PK AS PART_MNEMONIC,");
		sbQuery.append("			P.DESCRIPTION || ' - ' || P.ID_PARTICIPANT_PK AS PART_DESC,");
		sbQuery.append("			S.DESCRIPTION || ' - ' || S.ID_ISIN_CODE_PK AS SEC_DESC");
		sbQuery.append("	   FROM INTERNATIONAL_OPERATION IO");
		sbQuery.append(" INNER JOIN PARAMETER_TABLE PT");
		sbQuery.append("	   	 ON PT.PARAMETER_TABLE_PK=IO.OPERATION_STATE");
		sbQuery.append(" INNER JOIN OPERATION_TYPE OT");
		sbQuery.append(" 		 ON OT.ID_OPERATION_TYPE_PK=IO.TRANSFER_TYPE");
		sbQuery.append(" INNER JOIN PARTICIPANT P");
		sbQuery.append(" 		 ON P.ID_PARTICIPANT_PK = IO.ID_LOCAL_PARTICIPANT_FK");
		sbQuery.append(" INNER JOIN SECURITY S");
		sbQuery.append(" 		 ON S.ID_ISIN_CODE_PK = IO.ID_ISIN_CODE_FK");
		sbQuery.append("   GROUP BY IO.ID_LOCAL_PARTICIPANT_FK, S.ID_ISIN_CODE_PK, OT.ID_OPERATION_TYPE_PK,");
		sbQuery.append(" 			P.MNEMONIC 	  || ' - ' || P.ID_PARTICIPANT_PK, ");
		sbQuery.append(" 			P.DESCRIPTION || ' - ' || P.ID_PARTICIPANT_PK, ");
		sbQuery.append("			S.DESCRIPTION || ' - ' || S.ID_ISIN_CODE_PK");

		Query query = em.createNativeQuery(sbQuery.toString());

		query.setParameter("anullatedState",   InternationalOperationStateType.ANULADA.getCode());
		query.setParameter("approvedState",    InternationalOperationStateType.APROBADA.getCode());
		query.setParameter("cancelledState",   InternationalOperationStateType.CANCELADA.getCode());
		query.setParameter("liquidationState", InternationalOperationStateType.LIQUIDADA.getCode());
		query.setParameter("rejectedState",    InternationalOperationStateType.RECHAZADA.getCode());
		query.setParameter("registeredState",  InternationalOperationStateType.REGISTRADA.getCode());		
		query.setParameter("confirmedState",   InternationalOperationStateType.CONFIRMADA.getCode());		

		return query.getResultList();
	}
	
	/**
	 * Metodo para extraer el pk de la entidad relacionada con el usuario
	 * @param loginUser
	 * @return
	 */
	public Long getParticipantRegister (String loginUser){
		
		Long pkParticpant =  null;
		
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append("     SELECT UA.ID_PARTICIPANT_FK FROM SECURITY.USER_ACCOUNT UA ");
		sbQuery.append("   	 WHERE UA.LOGIN_USER =:loginUser ");
		
		Query query = em.createNativeQuery(sbQuery.toString());

		query.setParameter("loginUser",   loginUser);
		
		try {
			pkParticpant = Long.valueOf(query.getSingleResult().toString());
		} catch (Exception e) {
			pkParticpant = null;
		}
		
		return pkParticpant;
	}


	/**
	 * Gets the settlement transfer.
	 *
	 * @param filterto the filterto
	 * @return the settlement transfer
	 */
	public List<Object[]> getSettlementTransfer(SettlementOperationTO filterto) {

		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select distinct MO.id_mechanism_operation_pk,");
		sbQuery.append("       MO.operation_number,");
		sbQuery.append("       MO.settlement_schema,");
		sbQuery.append("       MO.id_negotiation_mechanism_fk,");
		sbQuery.append("       MO.id_negotiation_modality_fk,");
		sbQuery.append("       trunc(MO.operation_date),");
		sbQuery.append("       FO.operation_part,");
		sbQuery.append("       TO_CHAR(MO.real_cash_settlement_date,'dd/MM/yyyy'),");
		sbQuery.append("       TO_CHAR(MO.real_term_settlement_date,'dd/MM/yyyy'),");
		sbQuery.append("       P.bic_code,");
		sbQuery.append("       FO.operation_amount,");
		sbQuery.append("       PARAM1.PARAMETER_NAME as currency,");
		sbQuery.append("       TO_CHAR(MO.real_term_settlement_date,'hh:mm'), ");
		sbQuery.append("       TO_CHAR(MO.real_cash_settlement_date,'hh:mm'),");
		sbQuery.append("       PARAM2.PARAMETER_NAME as state, ");
		sbQuery.append("       mo.operation_state ");
		sbQuery.append("     FROM	FUNDS_OPERATION FO ");
		sbQuery.append("INNER JOIN MECHANISM_OPERATION MO ");
		sbQuery.append("      ON FO.id_mechanism_operation_fk=MO.id_mechanism_operation_pk ");
		sbQuery.append("INNER JOIN PARTICIPANT P ");
		sbQuery.append("      ON FO.id_participant_fk=P.id_participant_pk ");
		sbQuery.append("INNER JOIN PARAMETER_TABLE PARAM1");
		sbQuery.append("      ON FO.currency=PARAM1.PARAMETER_TABLE_PK ");
		sbQuery.append("INNER JOIN PARAMETER_TABLE PARAM2");
		sbQuery.append("      ON MO.operation_state=PARAM2.PARAMETER_TABLE_PK ");
		sbQuery.append(" where 1=1 ");
		sbQuery.append(" and FO.funds_operation_class=:indTwo");
		sbQuery.append(" and MO.operation_state in(615,616)");


		if(Validations.validateIsNotNull(filterto.getParticipantPk())){
			sbQuery.append(" and P.id_participant_pk=:participantPk");
		}

		if(Validations.validateIsNotNull(filterto.getDateInitial()) && Validations.validateIsNotNull(filterto.getDateEnd())){
			sbQuery.append(" and FO.registry_date between :dateInitial and :dateEnd ");
		}

		if(Validations.validateIsNotNull(filterto.getCurrency())){
			sbQuery.append(" and FO.currency=:currency ");
		}

		if(Validations.validateIsNotNull(filterto.getSettlement_schema())){
			sbQuery.append(" and MO.settlement_schema=:schema");
		}

		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("indTwo", 2);

		if(Validations.validateIsNotNull(filterto.getCurrency())){
			query.setParameter("currency", filterto.getCurrency());
		}

		if(Validations.validateIsNotNull(filterto.getParticipantPk())){
			query.setParameter("participantPk", filterto.getParticipantPk());
		}

		if(Validations.validateIsNotNull(filterto.getDateInitial()) && Validations.validateIsNotNull(filterto.getDateEnd())){
			query.setParameter("dateInitial", filterto.getDateInitial());
			query.setParameter("dateEnd", filterto.getDateEnd());
		}

		if(Validations.validateIsNotNull(filterto.getSettlement_schema())){
			query.setParameter("schema", filterto.getSettlement_schema());
		}
		List<Object[]> listObjects = query.getResultList();

		return listObjects;
	}
 


	/**
	 * Gets the unfulfillment list by filter.
	 *
	 * @param filter the filter
	 * @return the unfulfillment list by filter
	 */
	public List<Object[]> getUnfulfillmentListByFilter(SettlementOperationTO filter){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT TO_CHAR(MO.OPERATION_DATE,'dd/MM/yyyy')                                 AS FECHA_OPERATION,");
		sbQuery.append("        MO.OPERATION_NUMBER                               AS NUMERO_OPERATION,");
		sbQuery.append("        TO_CHAR(OU.SETTLEMENT_DATE,'dd/MM/yyyy')                                AS FECHA_LIQUIDACION,"); 
		sbQuery.append("        MO.ID_ISIN_CODE_FK                                AS VALOR,");
		sbQuery.append("        DECODE(MO.SETTLEMENT_TYPE,1,'FOP',");
		sbQuery.append("                                  2,'DVP',");
		sbQuery.append("                                  3,'DVD') AS TIPO_LIQ,");
		sbQuery.append("       MO.STOCK_QUANTITY                                  AS CANTIDAD_OPERATION,"); 
		sbQuery.append("       OU.UNFULFILLED_QUANTITY                            AS CANTIDAD_PENDIENTE,");
		sbQuery.append("       OU.UNFULFILLED_AMOUNT                              as IMPORTE_PENDIENTE,");
		sbQuery.append("       P.ID_PARTICIPANT_PK                                AS ID_PARTICIPANT,");		
		sbQuery.append("       P.MNEMONIC                                      AS PARTICIPANTE,");
		sbQuery.append("       MOTIVE.PARAMETER_NAME                              AS  MOTIVO,");
		sbQuery.append("       UNTYPE.PARAMETER_NAME                              AS TIPO_INCUMPLIMIENTO,");
		sbQuery.append("       DECODE(OU.OPERATION_PART,1,'C',");
		sbQuery.append("                                2,'P')                 AS CONTADO_O_PLAZO,");
		sbQuery.append("     CURRENCY.DESCRIPTION                            as MONEDA,");
		sbQuery.append("      P2.ID_PARTICIPANT_PK                                as ID_PARTICIPANT_INFRACTOR,");
		sbQuery.append("      P2.MNEMONIC                                      as PARTI_INFRACTOR,");
		sbQuery.append("      NM.MODALITY_NAME                                   AS MODALIDAD,");
		sbQuery.append("        NME.DESCRIPTION                               as MECANISMO, ");
		sbQuery.append("        CURRENCY.PARAMETER_TABLE_PK                   AS CURRENCY_TYPE");
		sbQuery.append("        FROM OPERATION_UNFULFILLMENT OU ");
		sbQuery.append("        JOIN MECHANISM_OPERATION MO");
		sbQuery.append("            ON OU.ID_MECHANISM_OPERATION_FK =MO.ID_MECHANISM_OPERATION_PK");
		sbQuery.append("       JOIN PARAMETER_TABLE MOTIVE");
		sbQuery.append("            ON OU.UNFULFILLMENT_REASON=MOTIVE.PARAMETER_TABLE_PK");
		sbQuery.append("       JOIN PARTICIPANT P");
		sbQuery.append("            ON OU.ID_AFFECTED_PARTICIPANT_FK=P.ID_PARTICIPANT_PK");
		sbQuery.append("       JOIN PARAMETER_TABLE UNTYPE");
		sbQuery.append("            ON OU.UNFULFILLMENT_TYPE=UNTYPE.PARAMETER_TABLE_PK");
		sbQuery.append("       JOIN PARTICIPANT P2");
		sbQuery.append("            ON OU.ID_UNFULFILLED_PARTICIPANT_FK  =P2.ID_PARTICIPANT_PK");
		sbQuery.append("       JOIN PARAMETER_TABLE CURRENCY ");
		sbQuery.append("            ON MO.CURRENCY=CURRENCY.PARAMETER_TABLE_PK ");
		sbQuery.append("       JOIN NEGOTIATION_MODALITY NM");
		sbQuery.append("            ON MO.ID_NEGOTIATION_MODALITY_FK=NM.ID_NEGOTIATION_MODALITY_PK");
		sbQuery.append("       JOIN NEGOTIATION_MECHANISM NME");
		sbQuery.append("            ON MO.ID_NEGOTIATION_MECHANISM_FK=NME.ID_NEGOTIATION_MECHANISM_PK");

		sbQuery.append(" WHERE 1 = 1 ");

		if(Validations.validateIsNotNull(filter.getDateInitial()) && Validations.validateIsNotNull(filter.getDateEnd())){
			sbQuery.append(" and trunc(OU.UNFULFILLMENT_DATE) between :initalDate and :endDate ");
		} 		

		if(Validations.validateIsNotNull(filter.getMechanism())){
			sbQuery.append(" and MO.ID_NEGOTIATION_MECHANISM_FK=:mechanism");
		} 			
		if(Validations.validateIsNotNull(filter.getModality())){
			sbQuery.append(" and MO.ID_NEGOTIATION_MODALITY_FK=:modality");
		} 	
		if(Validations.validateIsNotNull(filter.getCurrency())){
			sbQuery.append(" and MO.CURRENCY=:currency");
		} 
		
		if(Validations.validateIsNotNull(filter.getParticipantPk())){
			sbQuery.append(" and OU.ID_AFFECTED_PARTICIPANT_FK=:participant");
		}
		
		if(Validations.validateIsNotNull(filter.getIsinCode())){
			sbQuery.append(" and MO.ID_ISIN_CODE_FK=:isin");
		}
		
		if(Validations.validateIsNotNull(filter.getOperationPart())){
			sbQuery.append(" and OU.OPERATION_PART=:operationPart");			
		}
		
		if(Validations.validateIsNotNull(filter.getUnfulfillmentMotive())){
			sbQuery.append(" and OU.OU.UNFULFILLMENT_REASON=:motive");		
		}
		
		
		sbQuery.append(" ORDER BY NME.DESCRIPTION,NM.MODALITY_NAME,OU.SETTLEMENT_DATE ");


		Query query = em.createNativeQuery(sbQuery.toString());

		if(Validations.validateIsNotNull(filter.getDateInitial()) && Validations.validateIsNotNull(filter.getDateEnd())){
			query.setParameter("initalDate", filter.getDateInitial());
			query.setParameter("endDate", filter.getDateEnd());
		} 		

		if(Validations.validateIsNotNull(filter.getMechanism())){
			query.setParameter("mechanism", filter.getMechanism());
		} 		

		if(Validations.validateIsNotNull(filter.getModality())){
			query.setParameter("modality", filter.getModality());
		} 	
		
		if(Validations.validateIsNotNull(filter.getCurrency())){
			query.setParameter("currency", filter.getCurrency());
 		} 
		
		if(Validations.validateIsNotNull(filter.getParticipantPk())){
			query.setParameter("participant", filter.getParticipantPk());
 		}
		
		if(Validations.validateIsNotNull(filter.getIsinCode())){
			query.setParameter("isin", filter.getIsinCode());
 		}
		
		if(Validations.validateIsNotNull(filter.getOperationPart())){
 			query.setParameter("operationPart", filter.getOperationPart());
		}
		
		if(Validations.validateIsNotNull(filter.getUnfulfillmentMotive())){
 			query.setParameter("motive", filter.getUnfulfillmentMotive());
 		}

		List<Object[]> listObjects =(List<Object[]>) query.getResultList();

		return listObjects;
	}
	
	
	/**
	 * Gets the negotiation filter.
	 *
	 * @param filter the filter
	 * @return the negotiation filter
	 */
	public Object[] getNegotiationFilter(SettlementOperationTO filter){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select (select parameter_name from parameter_table where parameter_table_pk=:currency_type),");
		sbQuery.append("       (select participant.mnemonic from participant where participant.id_participant_pk=:participant_pk),");
		sbQuery.append("       (select modality_name from negotiation_modality where negotiation_modality.id_negotiation_modality_pk=:modality),");
		sbQuery.append("       (select mechanism_name from negotiation_mechanism where negotiation_mechanism.id_negotiation_mechanism_pk=:mechanism),");
		sbQuery.append("       decode(:operatio_part,1,'CONTADO',2,'PLAZO')  ");
		sbQuery.append("  from dual");
		
		Query query = em.createNativeQuery(sbQuery.toString());
		
		query.setParameter("currency_type", filter.getCurrency()==null?"0":filter.getCurrency());
		query.setParameter("participant_pk", filter.getParticipantPk()==null?"0":filter.getParticipantPk());
		query.setParameter("modality", filter.getModality()==null?"0":filter.getModality());		
		query.setParameter("mechanism", filter.getMechanism()==null?"0":filter.getMechanism());
		query.setParameter("operatio_part", filter.getOperationPart()==null?"0":filter.getOperationPart());

 		
		try{
			Object[] temp = (Object[]) query.getSingleResult();
		  return temp;
		}catch(NoResultException ex)
		{
 		}
		return null;
		
		
 	}
	
	
	/**
	 * Gets the prepaid settlement by filter.
	 *
	 * @param filter the filter
	 * @return the prepaid settlement by filter
	 */
	public List<Object[]> getPrepaidSettlementByFilter(SettlementOperationTO filter){
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append("SELECT MO.ID_MECHANISM_OPERATION_PK, ");
 		sbQuery.append("       TO_CHAR(MO.OPERATION_DATE,'dd/MM/yyyy') as FECHA_OPERACION,");
		sbQuery.append("       MO.OPERATION_NUMBER as NUMERO_OPERACION,");
		sbQuery.append("       TO_CHAR(MO.CASH_PREPAID_DATE,'dd/MM/yyyy'), ");
		sbQuery.append("       TO_CHAR(MO.TERM_PREPAID_DATE,'dd/MM/yyyy'),");
		sbQuery.append("       MO.IND_CASH_PREPAID, ");
		sbQuery.append("       MO.IND_TERM_PREPAID, ");
		sbQuery.append("       TO_CHAR(MO.CASH_SETTLEMENT_DATE,'dd/MM/yyyy'),  ");
		sbQuery.append("       TO_CHAR(MO.TERM_SETTLEMENT_DATE,'dd/MM/YYYY'),  ");
		sbQuery.append("       MO.ID_ISIN_CODE_FK,");
		sbQuery.append("       MO.CASH_AMOUNT,");
		sbQuery.append("       LISTAGG(HAO.ID_HOLDER_ACCOUNT_FK||'   '||DECODE(HAO.role,1,'comprador',2,'vendedor'),' \n'  ) within group (order by HAO.ID_HOLDER_ACCOUNT_FK) as CUENTAS,");
	//	sbQuery.append("       HAO.ID_HOLDER_ACCOUNT_FK,");
		//sbQuery.append("       DECODE(HAO.role,1,'COMPRADOR',");
		//sbQuery.append("                       2,'VENDERDOR') as role,");		
		sbQuery.append("       DECODE(HAO.OPERATION_PART,1,'CONTADO',");
		sbQuery.append("                                 2,'PLAZO') as part,");
 		sbQuery.append("       MO.SETTLEMENT_TYPE,");
		sbQuery.append("       SO.PARAMETER_NAME AS ESTADO_OPERACION,");
		sbQuery.append("       SL.PARAMETER_NAME as ESTADO_LIQUIDACION,");
		sbQuery.append("       CURRENCY.PARAMETER_NAME as MONEDA,");
		sbQuery.append("       PB.ID_PARTICIPANT_PK as PARTICIPANTE_COMPRADOR_PK,");
		sbQuery.append("       PB.DESCRIPTION as PARTICIPANTE_COMPRADOR,");
 		sbQuery.append("       NMO.MODALITY_NAME,");
		sbQuery.append("       NME.DESCRIPTION ");
		
		sbQuery.append("      FROM MECHANISM_OPERATION  MO   ");
		sbQuery.append("    INNER JOIN HOLDER_ACCOUNT_OPERATION HAO");
		sbQuery.append("         ON HAO.ID_MECHANISM_OPERATION_FK=MO.ID_MECHANISM_OPERATION_PK  ");
		sbQuery.append("    INNER JOIN  SETTLEMENT_DATE_REQUEST SDR");
 		sbQuery.append("         ON SDR.ID_MECHANISM_OPERATION_FK=MO.ID_MECHANISM_OPERATION_PK  ");
		sbQuery.append("    INNER JOIN PARAMETER_TABLE SO");
		sbQuery.append("         ON SDR.REQUEST_STATE=SO.PARAMETER_TABLE_PK");
		sbQuery.append("    INNER JOIN  PARAMETER_TABLE SL");
		sbQuery.append("         ON MO.OPERATION_STATE=SL.PARAMETER_TABLE_PK");
		sbQuery.append("    INNER JOIN  PARAMETER_TABLE CURRENCY");
 		sbQuery.append("         ON MO.CURRENCY=CURRENCY.PARAMETER_TABLE_PK");
		sbQuery.append("    INNER JOIN  PARTICIPANT PB");
		sbQuery.append("         ON MO.ID_BUYER_PARTICIPANT_FK=PB.ID_PARTICIPANT_PK");
		sbQuery.append("    INNER JOIN  PARTICIPANT PS");
		sbQuery.append("         ON MO.ID_SELLER_PARTICIPANT_FK=PS.ID_PARTICIPANT_PK");
		sbQuery.append("    INNER JOIN  NEGOTIATION_MODALITY NMO");
 		sbQuery.append("         ON MO.ID_NEGOTIATION_MODALITY_FK=NMO.ID_NEGOTIATION_MODALITY_PK");
		sbQuery.append("    INNER JOIN  NEGOTIATION_MECHANISM NME");
		sbQuery.append("         ON MO.ID_NEGOTIATION_MECHANISM_FK=NME.ID_NEGOTIATION_MECHANISM_PK");
		
		sbQuery.append("     WHERE MO.IND_CASH_PREPAID =1 or MO.IND_TERM_PREPAID=1");
		sbQuery.append("  AND SDR.request_state in (1229,1474,1543)  ");
		
		if(Validations.validateIsNotNull(filter.getDateInitial()) && Validations.validateIsNotNull(filter.getDateEnd())){
			sbQuery.append(" and MO.OPERATION_DATE between :dateInitial and :dateEnd");
		}
		
		if(Validations.validateIsNotNull(filter.getModality())){			
			sbQuery.append(" and MO.ID_NEGOTIATION_MODALITY_FK=:modality");
		}
		
		if(Validations.validateIsNotNull(filter.getMechanism())){
			sbQuery.append(" and MO.ID_NEGOTIATION_MECHANISM_FK=:mechanism");
		}
		
		if(Validations.validateIsNotNull(filter.getCurrency())){
			sbQuery.append(" and MO.CURRENCY=:currency");
		}
		
		if(Validations.validateIsNotNull(filter.getOperationPart())){
			sbQuery.append(" and HAO.OPERATION_PART=:operationPart");
		}
		
		if(Validations.validateIsNotNull(filter.getParticipantPk())){
			sbQuery.append(" and PB.ID_PARTICIPANT_PK=:participant");

		}
		
		
		sbQuery.append(" GROUP BY MO.ID_MECHANISM_OPERATION_PK,");
		sbQuery.append("          MO.OPERATION_DATE,");
		sbQuery.append("          MO.OPERATION_NUMBER,");
		sbQuery.append("          TO_CHAR(MO.CASH_PREPAID_DATE,'dd/MM/yyyy'),");
		sbQuery.append("          TO_CHAR(MO.TERM_PREPAID_DATE,'dd/MM/yyyy'),");
		sbQuery.append("          MO.IND_CASH_PREPAID, ");
		sbQuery.append("          MO.IND_TERM_PREPAID,");
		sbQuery.append("          TO_CHAR(MO.CASH_SETTLEMENT_DATE,'dd/MM/yyyy'),");
		sbQuery.append("          TO_CHAR(MO.TERM_SETTLEMENT_DATE,'dd/MM/yyyy'),");
		sbQuery.append("          MO.ID_ISIN_CODE_FK,");
		sbQuery.append("          MO.CASH_AMOUNT,");
		sbQuery.append("          HAO.OPERATION_PART,");
		sbQuery.append("          MO.SETTLEMENT_TYPE,");
		sbQuery.append("          SO.PARAMETER_NAME,");
		sbQuery.append("          SL.PARAMETER_NAME,");
		sbQuery.append("          CURRENCY.PARAMETER_NAME,");
		sbQuery.append("          PB.ID_PARTICIPANT_PK,");
		sbQuery.append("          PB.DESCRIPTION,");
		sbQuery.append("          NMO.MODALITY_NAME,");
		sbQuery.append("          NME.DESCRIPTION,");
		sbQuery.append("          TO_CHAR(MO.TERM_SETTLEMENT_DATE,'dd/MM/YYYY')");
		
		Query query =em.createNativeQuery(sbQuery.toString());
		
		if(Validations.validateIsNotNull(filter.getDateInitial()) && Validations.validateIsNotNull(filter.getDateEnd())){
			query.setParameter("dateInitial", filter.getDateInitial());
			query.setParameter("dateEnd", filter.getDateEnd());
 		}
		
		if(Validations.validateIsNotNull(filter.getModality())){	
			query.setParameter("modality", filter.getModality());
 		}
		
		if(Validations.validateIsNotNull(filter.getMechanism())){
			query.setParameter("mechanism", filter.getMechanism());
 		}
		
		if(Validations.validateIsNotNull(filter.getCurrency())){
			query.setParameter("currency", filter.getCurrency());
 		}
		
		if(Validations.validateIsNotNull(filter.getOperationPart())){
			query.setParameter("operationPart", filter.getOperationPart());
 		}
		
		if(Validations.validateIsNotNull(filter.getParticipantPk())){
 			query.setParameter("participant", filter.getParticipantPk());
		}
		
		
		
		List<Object[]> listObjects = (List<Object[]>) query.getResultList();
		
		return listObjects;
	}
	
	
	
	
	/**
	 * Gets the maket compensation by participant.
	 *
	 * @param fitler the fitler
	 * @return the maket compensation by participant
	 */
	public List<Object[]> getMaketCompensationByParticipant(SettlementOperationTO fitler){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT P.BIC_CODE,");
		sbQuery.append("       P.ID_PARTICIPANT_PK,");
        sbQuery.append("       P.DESCRIPTION parti_descripcion,");
		sbQuery.append("       PARA.DESCRIPTION CURRENCY,");
		sbQuery.append("       NM.MODALITY_NAME modality,");
		sbQuery.append("       NME.DESCRIPTION mechanism,");
		//********* SUM BALANCE TO BUY
		//NVL use because depending on the operation state sum cash_amount or term_amount
		sbQuery.append("       NVL( SUM((select DECODE(MO.OPERATION_STATE,:cash_state,CASH_AMOUNT");
		sbQuery.append("                                                 ,:term_state,TERM_AMOUNT)");
		sbQuery.append("       from PARTICIPANT_OPERATION");
		sbQuery.append("       where ID_PARTICIPANT_OPERATION_PK=PO.ID_PARTICIPANT_OPERATION_PK and role=:indTwo and PO.INCHARGE_TYPE=:indTwo)),0) as A_COBRAR,");		
		//********* SUM BALANCE TO PAY
		//NVL use because depending on the operation state sum cash_amount or term_amount
		sbQuery.append("       NVL( SUM((select DECODE(MO.OPERATION_STATE,:cash_state,CASH_AMOUNT");
		sbQuery.append("                                                 ,:term_state,TERM_AMOUNT)");
		sbQuery.append("   from PARTICIPANT_OPERATION");
		sbQuery.append("   WHERE ID_PARTICIPANT_OPERATION_PK=PO.ID_PARTICIPANT_OPERATION_PK and role=:indOne and PO.INCHARGE_TYPE=:indTwo )),0) as A_PAGAR");
		
		sbQuery.append("   FROM PARTICIPANT_OPERATION PO");
		sbQuery.append("   JOIN PARTICIPANT P");
		sbQuery.append("        ON  PO.ID_PARTICIPANT_FK=P.ID_PARTICIPANT_PK");
		sbQuery.append("    JOIN MECHANISM_OPERATION MO");
		sbQuery.append("        ON PO.ID_MECHANISM_OPERATION_FK=MO.ID_MECHANISM_OPERATION_PK");
		sbQuery.append("    JOIN PARAMETER_TABLE PARA");
		sbQuery.append("        ON PO.CURRENCY=PARA.PARAMETER_TABLE_PK");
		sbQuery.append("    JOIN NEGOTIATION_MECHANISM NME");
		sbQuery.append("        ON PO.ID_NEGOTIATION_MECHANISM_FK=NME.ID_NEGOTIATION_MECHANISM_PK");
		sbQuery.append("    JOIN NEGOTIATION_MODALITY NM");
		
		sbQuery.append("        ON PO.ID_NEGOTIATION_MODALITY_FK=NM.ID_NEGOTIATION_MODALITY_PK");
		//** Compare the settlement date depending on the operation state. 
		sbQuery.append("            where( (MO.OPERATION_STATE=:cash_state and trunc(MO.REAL_CASH_SETTLEMENT_DATE)=:settlement_date ) or");
		sbQuery.append("                   (MO.OPERATION_STATE=:term_state and trunc(MO.REAL_TERM_SETTLEMENT_DATE)=:settlement_date ))");
		
		sbQuery.append("    AND PO.ID_NEGOTIATION_MECHANISM_FK=:mechanism_type");
		sbQuery.append("    AND PO.ID_NEGOTIATION_MODALITY_FK=:modality_type");
		sbQuery.append("    AND (PO.CURRENCY=:currency_type)");
		sbQuery.append("    AND MO.Settlement_type=:indTwo");
		sbQuery.append("    GROUP BY P.BIC_CODE,");
		sbQuery.append("          P.ID_PARTICIPANT_PK,");
		sbQuery.append("          P.DESCRIPTION,");
		sbQuery.append("          PARA.DESCRIPTION,");
		sbQuery.append("          NM.MODALITY_NAME,");
		sbQuery.append("          NME.DESCRIPTION");
		
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter(ReportConstant.MECHANISM_TYPE_PARAM, fitler.getMechanism());
		query.setParameter(ReportConstant.MODALITY_TYPE_PARAM, fitler.getModality());
		query.setParameter(ReportConstant.CURRENCY_TYPE_PARAM, fitler.getCurrency());
		query.setParameter("indTwo", 2);
		query.setParameter("indOne", BooleanType.YES.getCode());
		query.setParameter("cash_state", MechanismOperationStateType.CASH_SETTLED.getCode());
		query.setParameter("term_state", MechanismOperationStateType.TERM_SETTLED.getCode());
		query.setParameter(ReportConstant.SETTLEMENT_DATE_PARAM, fitler.getSettlementDate());
		
		return query.getResultList();
 
		
	}
	
	/**
	 * Gets the operations culminated reported.
	 *
	 * @param gfto the gfto
	 * @return the operations culminated reported
	 */
	public String getOperationsCulminatedReported(GenericSettlementOperationTO gfto){
		StringBuilder sbQuery = new StringBuilder();
		
			sbQuery.append(" SELECT  ");
			sbQuery.append(" 	(SELECT NM.DESCRIPTION FROM NEGOTIATION_MECHANISM NM WHERE NM.ID_NEGOTIATION_MECHANISM_PK=MO.ID_NEGOTIATION_MECHANISM_FK) AS MECHANISM, ");
			sbQuery.append("    (SELECT NM.MODALITY_NAME FROM NEGOTIATION_MODALITY NM WHERE NM.ID_NEGOTIATION_MODALITY_PK=MO.ID_NEGOTIATION_MODALITY_FK) AS MODALITY, ");
	        sbQuery.append("    MO.BALLOT_NUMBER ||'-'|| MO.SEQUENTIAL PAPELETA_SECUENCIAL, ");
	        sbQuery.append("    MO.OPERATION_NUMBER, ");
	        sbQuery.append("    MO.OPERATION_DATE, ");
	        sbQuery.append("    SO_CASH.SETTLEMENT_DATE AS DATE_COUNTED, ");
	        sbQuery.append("    SO_TERM.SETTLEMENT_DATE AS DATE_TERM, ");
	        sbQuery.append("    MO.ID_SECURITY_CODE_FK, ");
	        sbQuery.append("    SO_TERM.SETTLEMENT_SCHEMA, ");
	        sbQuery.append("    SO_TERM.SETTLEMENT_TYPE, ");
	        sbQuery.append("    SO_CASH.SETTLEMENT_AMOUNT AS AMOUNT_COUNTED, ");
	        sbQuery.append("    SO_TERM.SETTLEMENT_AMOUNT AS AMOUNT_TERM, ");
	        sbQuery.append("    SO_TERM.SETTLEMENT_PRICE AS AMOUNT_PRICE, ");
	        sbQuery.append("    (SELECT P.MNEMONIC ||'-'|| P.ID_PARTICIPANT_PK FROM PARTICIPANT P WHERE P.ID_PARTICIPANT_PK=MO.ID_SELLER_PARTICIPANT_FK) SELLET_PARTICIPANT, ");
	        sbQuery.append("    (SELECT P.MNEMONIC ||'-'|| P.ID_PARTICIPANT_PK FROM PARTICIPANT P WHERE P.ID_PARTICIPANT_PK=MO.ID_BUYER_PARTICIPANT_FK) BUYER_PARTICIPANT, ");
	        sbQuery.append("    (select parameter_name from parameter_table where master_table_fk = 158 and element_subclasification1 = SO_TERM.SETTLEMENT_SCHEMA) AS SETTLEMENT_SCHEMA_DESCRIPTION, ");
	        sbQuery.append("    (select parameter_name from parameter_table where master_table_fk = 12 and element_subclasification1 = SO_TERM.SETTLEMENT_TYPE) AS SETTLEMENT_TYPE_DESCRIPTION, ");
	        sbQuery.append("    SO_TERM.STOCK_QUANTITY AS STOCK_QUANTITY, ");
	        sbQuery.append("    DECODE(SO_TERM.IND_PREPAID,1,'SI','') AS ANTICIPO ");
	        sbQuery.append(" FROM MECHANISM_OPERATION MO "); 
    		sbQuery.append(" INNER JOIN SETTLEMENT_OPERATION SO_TERM ON MO.ID_MECHANISM_OPERATION_PK=SO_TERM.ID_MECHANISM_OPERATION_FK ");
			sbQuery.append(" INNER JOIN SETTLEMENT_OPERATION SO_CASH on MO.ID_MECHANISM_OPERATION_PK=SO_CASH.ID_MECHANISM_OPERATION_FK ");
			sbQuery.append(" WHERE SO_TERM.OPERATION_PART=2 AND SO_CASH.OPERATION_PART=1 AND SO_TERM.IND_PARTIAL=0 AND SO_CASH.IND_PARTIAL=0 AND SO_TERM.OPERATION_STATE=616 AND SO_CASH.ID_MECHANISM_OPERATION_FK = SO_TERM.ID_MECHANISM_OPERATION_FK ");
			sbQuery.append(" AND TRUNC(SO_TERM.SETTLEMENT_DATE) BETWEEN TO_DATE('"+gfto.getInitialDate()+"','dd/MM/yyyy') AND TO_DATE('"+gfto.getFinalDate()+"','dd/MM/yyyy') ");
			
			if(Validations.validateIsNotNull(gfto.getMechanism())){
				sbQuery.append(" AND MO.ID_NEGOTIATION_MECHANISM_FK=" + gfto.getMechanism() + " ");
			}
			if(Validations.validateIsNotNull(gfto.getModality())){
				sbQuery.append(" AND MO.ID_NEGOTIATION_MODALITY_FK=" + gfto.getModality() + " ");
			}
			if(Validations.validateIsNotNull(gfto.getIdSecurityCodePk())){
				sbQuery.append(" AND MO.ID_SECURITY_CODE_FK='" +gfto.getIdSecurityCodePk()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getSettlementSchema())){
				sbQuery.append(" AND SO_TERM.SETTLEMENT_SCHEMA=" + gfto.getSettlementSchema() + " ");
			}
			if(Validations.validateIsNotNull(gfto.getSettlementType())){
				sbQuery.append(" AND SO_TERM.SETTLEMENT_TYPE=" + gfto.getSettlementType() + " ");
			}
			if(Validations.validateIsNotNull(gfto.getSellerParticipant())){
				sbQuery.append(" AND MO.ID_SELLER_PARTICIPANT_FK=" + gfto.getSellerParticipant() + " ");
			}
			if(Validations.validateIsNotNull(gfto.getBuyerParticipant())){
				sbQuery.append(" AND MO.ID_BUYER_PARTICIPANT_FK=" + gfto.getBuyerParticipant() + " ");
			}
			sbQuery.append(" ORDER BY MECHANISM,MODALITY,MO.OPERATION_DATE,MO.BALLOT_NUMBER,MO.SEQUENTIAL,MO.OPERATION_NUMBER ");
		
		
		return sbQuery.toString();
	}
	
	/**
	 * Gets the net settlement positions query.
	 *
	 * @param gfto the gfto
	 * @return the net settlement positions query
	 */
	public String getNetSettlementPositionsQuery(GenericSettlementOperationTO gfto){
		StringBuilder sbQuery = new StringBuilder();
	
		sbQuery.append(" select P.Mnemonic, P.Description, ");
		sbQuery.append(" (select pt.parameter_name from Parameter_table pt where pt.parameter_table_pk = ps.settlement_currency ) as currency, ");
		sbQuery.append(" so.settlement_schema as id_schema, ");
		sbQuery.append(" sum( ");
		sbQuery.append("   (case when ps.role = 1 then -1 ");
		sbQuery.append("         when ps.role = 2 then 1 ");
		sbQuery.append("         else null  end )*ps.settlement_amount ");
		sbQuery.append(" ) as net_position ");
		sbQuery.append(" from mechanism_operation mo , settlement_operation so, participant_settlement ps , participant p ");
		sbQuery.append(" where Ps.Id_Settlement_Operation_Fk = So.Id_Settlement_Operation_Pk ");
		sbQuery.append(" and So.Id_Mechanism_Operation_Fk = Mo.Id_Mechanism_Operation_Pk ");
		sbQuery.append(" and P.Id_Participant_Pk = Ps.Id_Participant_Fk ");
		
		if(gfto.getBuyerParticipant() !=null){
			sbQuery.append(" and P.Id_Participant_Pk = " + gfto.getBuyerParticipant());
		}
		
		if(gfto.getSettlementDate()!=null){
			sbQuery.append(" and So.settlement_date = TO_DATE('"+gfto.getSettlementDate()+"','dd/MM/yyyy') ");
		}
		sbQuery.append(" and so.settlement_type =  " + SettlementType.DVP.getCode());
		
		if(gfto.getSettlementSchema() !=null){
			sbQuery.append(" and so.settlement_schema = " + gfto.getSettlementSchema());
		}
		
		if(gfto.getMechanism() !=null){
			sbQuery.append(" and mo.Id_Negotiation_Mechanism_Fk = " + gfto.getMechanism());
		}
		
		if(gfto.getModalityGroup() !=null){
			sbQuery.append(" and exists ( ");
			sbQuery.append("   select distinct mg.id_modality_group_pk ");
			sbQuery.append("   from Modality_Group mg , Modality_Group_Detail mgd , Mechanism_Modality mm ");
			sbQuery.append("   where mg.id_modality_group_pk = mgd.id_modality_group_fk ");
			sbQuery.append("     and Mgd.Id_Negotiation_Mechanism_Fk = Mm.Id_Negotiation_Mechanism_Pk ");
			sbQuery.append("     and Mgd.Id_Negotiation_Modality_Fk = Mm.Id_Negotiation_Modality_Pk ");
			sbQuery.append("     and Mgd.Id_Negotiation_Mechanism_Fk = mo.Id_Negotiation_Mechanism_Fk ");
			sbQuery.append("     and Mgd.Id_Negotiation_Modality_Fk = mo.Id_Negotiation_Modality_Fk ");
			sbQuery.append("     and Mg.Settlement_Schema = Mm.Settlement_Schema ");
			sbQuery.append("     and Mg.Settlement_Schema = So.Settlement_Schema ");
			sbQuery.append("     and Mg.Id_Modality_Group_Pk = " + gfto.getModalityGroup());
			sbQuery.append("   ) ");
		}
		
		sbQuery.append(" and ( (so.operation_part = 1 and  SO.operation_state in (612,614)) or ");
		sbQuery.append("       (so.operation_part = 2 and  SO.operation_state in (615)) ");
		sbQuery.append("     ) ");
		sbQuery.append(" group by P.Mnemonic, P.Description, ps.settlement_currency, so.settlement_schema ");
		sbQuery.append(" order by ps.settlement_currency ");
		
		return sbQuery.toString();
	}
	
	/**
	 * Gets the query transfer buy sales report.
	 *
	 * @param gfto the gfto
	 * @return the query transfer buy sales report
	 */
	public String getQueryTransferBuySalesReport(GenericSettlementOperationTO gfto){
		StringBuilder sbQuery = new StringBuilder();
		
			sbQuery.append(" SELECT   ");
			sbQuery.append(" 	(SELECT NM.DESCRIPTION FROM NEGOTIATION_MECHANISM NM WHERE NM.ID_NEGOTIATION_MECHANISM_PK=MO.ID_NEGOTIATION_MECHANISM_FK) AS MECHANISM, ");
			sbQuery.append(" 	(SELECT NM.MODALITY_NAME FROM NEGOTIATION_MODALITY NM WHERE NM.ID_NEGOTIATION_MODALITY_PK=MO.ID_NEGOTIATION_MODALITY_FK) AS MODALITY, ");
			sbQuery.append(" 	MO.BALLOT_NUMBER ||'-'|| MO.SEQUENTIAL PAPELETA_SECUENCIAL, ");
			sbQuery.append(" 	MO.ID_SECURITY_CODE_FK, ");
			sbQuery.append(" 	MO.OPERATION_DATE, ");
			sbQuery.append(" 	MO.OPERATION_NUMBER, ");
			sbQuery.append(" 	MO.OPERATION_STATE, ");
			sbQuery.append(" 	(SELECT P.MNEMONIC ||'-'|| P.ID_PARTICIPANT_PK FROM PARTICIPANT P WHERE P.ID_PARTICIPANT_PK=MO.ID_SELLER_PARTICIPANT_FK) SELLET_PARTICIPANT, ");
			sbQuery.append(" 	(SELECT P.MNEMONIC ||'-'|| P.ID_PARTICIPANT_PK FROM PARTICIPANT P WHERE P.ID_PARTICIPANT_PK=MO.ID_BUYER_PARTICIPANT_FK) BUYER_PARTICIPANT, ");
			sbQuery.append(" 	HA.ID_HOLDER_ACCOUNT_PK, ");
			sbQuery.append(" 	HA.ACCOUNT_NUMBER, ");
	        sbQuery.append(" 	DECODE(HAO.ROLE, 1,'COMPRA', 2, 'VENTA') AS ROLE, ");
			sbQuery.append(" 	HAO.STOCK_QUANTITY, ");
			sbQuery.append(" 	(SELECT P.MNEMONIC ||'-'|| P.ID_PARTICIPANT_PK FROM PARTICIPANT P WHERE P.ID_PARTICIPANT_PK=HAO.ID_INCHARGE_STOCK_PARTICIPANT) ID_INCHARGE_STOCK_PARTICIPANT,  ");
			sbQuery.append(" 	(SELECT P.MNEMONIC ||'-'|| P.ID_PARTICIPANT_PK FROM PARTICIPANT P WHERE P.ID_PARTICIPANT_PK=HAO.ID_INCHARGE_FUNDS_PARTICIPANT) ID_INCHARGE_FUNDS_PARTICIPANT,  ");
			sbQuery.append(" 	SO_CASH.SETTLEMENT_DATE AS DATE_COUNTED, ");
			sbQuery.append(" 	SO_TERM.SETTLEMENT_DATE AS DATE_TERM ");
			sbQuery.append(" FROM MECHANISM_OPERATION MO "); 
			sbQuery.append(" INNER JOIN SETTLEMENT_OPERATION SO_CASH ON (MO.ID_MECHANISM_OPERATION_PK=SO_CASH.ID_MECHANISM_OPERATION_FK AND SO_CASH.OPERATION_PART = 1 ) ");
			sbQuery.append(" INNER JOIN HOLDER_ACCOUNT_OPERATION HAO ON HAO.ID_MECHANISM_OPERATION_FK=MO.ID_MECHANISM_OPERATION_PK ");
			sbQuery.append(" INNER JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK=HAO.ID_HOLDER_ACCOUNT_FK ");
			sbQuery.append(" INNER JOIN SECURITY S ON MO.ID_SECURITY_CODE_FK=S.ID_SECURITY_CODE_PK ");
			sbQuery.append(" LEFT JOIN SETTLEMENT_OPERATION SO_TERM on ( ");
			sbQuery.append(" MO.ID_MECHANISM_OPERATION_PK=SO_TERM.ID_MECHANISM_OPERATION_FK  ");
			sbQuery.append(" AND SO_CASH.ID_MECHANISM_OPERATION_FK = SO_TERM.ID_MECHANISM_OPERATION_FK ");
			sbQuery.append(" AND SO_TERM.OPERATION_PART=2 ) ");	
			sbQuery.append(" WHERE ");
			sbQuery.append(" 	( ");
			sbQuery.append(" 	( mo.ind_term_settlement = 0 and hao.operation_part = 1 and mo.operation_state in (612,614,615) ) ");
			sbQuery.append(" 	or ");
			sbQuery.append(" 	( mo.ind_term_settlement = 1 and ( ( hao.operation_part = 1 and mo.operation_state in (612,614) ) or ( hao.operation_part = 2 and mo.operation_state in (615,616) ))) ");
			sbQuery.append(" 	) ");
			sbQuery.append(" 	AND trunc(MO.OPERATION_DATE) BETWEEN TO_DATE('"+gfto.getInitialDate()+"','dd/MM/yyyy') AND TO_DATE('"+gfto.getFinalDate()+"','dd/MM/yyyy') ");
			sbQuery.append(" and HAO.HOLDER_ACCOUNT_STATE = 625 ");
			
			if(Validations.validateIsNotNull(gfto.getMechanism())){
				sbQuery.append(" AND MO.ID_NEGOTIATION_MECHANISM_FK=" + gfto.getMechanism() + " ");
			}
			if(Validations.validateIsNotNull(gfto.getModality())){
				sbQuery.append(" AND MO.ID_NEGOTIATION_MODALITY_FK=" + gfto.getModality() + " ");
			}
			if(Validations.validateIsNotNull(gfto.getRole())){
				sbQuery.append(" AND HAO.ROLE=" +gfto.getRole()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getIssuer())){
				sbQuery.append(" AND S.ID_ISSUER_FK='" +gfto.getIssuer()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getIdSecurityCodePk())){
				sbQuery.append(" AND MO.ID_SECURITY_CODE_FK='" +gfto.getIdSecurityCodePk()+"' ");
			}
			
			if(Validations.validateIsNotNull(gfto.getInstitutionType())
	     			&& gfto.getInstitutionType().equals(InstitutionType.ISSUER_DPF.getCode().toString())){
	     		//ISSUER_DPF
	     		sbQuery.append(" AND secu.SECURITY_CLASS IN (1976,420) ");
	     	}
			
			if(Validations.validateIsNotNull(gfto.getOperationState())){
				sbQuery.append(" AND MO.OPERATION_STATE=" +gfto.getOperationState()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getOperationNumber())){
				sbQuery.append(" AND MO.OPERATION_NUMBER=" +gfto.getOperationNumber()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getBallotNumber())){
				sbQuery.append(" AND MO.BALLOT_NUMBER=" +gfto.getBallotNumber()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getSequential())){
				sbQuery.append(" AND MO.SEQUENTIAL=" +gfto.getSequential()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getSellerParticipant())){
				sbQuery.append(" AND MO.ID_SELLER_PARTICIPANT_FK='" +gfto.getSellerParticipant()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getBuyerParticipant())){
				sbQuery.append(" AND MO.ID_BUYER_PARTICIPANT_FK='" +gfto.getBuyerParticipant()+"' ");
			}
					
			sbQuery.append(" ORDER BY MECHANISM,MODALITY,MO.OPERATION_DATE,MO.OPERATION_NUMBER ");
		
		return sbQuery.toString();
	}
	
	/**
	 * Gets the query transfer personal data report.
	 *
	 * @param gfto the gfto
	 * @return the query transfer personal data report
	 */
	public String getQueryTransferPersonalDataReport(GenericSettlementOperationTO gfto){
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append(" SELECT  ");
		sbQuery.append(" (SELECT NM.DESCRIPTION FROM NEGOTIATION_MECHANISM NM WHERE NM.ID_NEGOTIATION_MECHANISM_PK=MO.ID_NEGOTIATION_MECHANISM_FK) AS MECHANISM, ");
        sbQuery.append(" (SELECT NM.MODALITY_NAME FROM NEGOTIATION_MODALITY NM WHERE NM.ID_NEGOTIATION_MODALITY_PK=MO.ID_NEGOTIATION_MODALITY_FK) AS MODALITY, ");
        sbQuery.append(" MO.ID_SECURITY_CODE_FK, ");
        sbQuery.append(" MO.OPERATION_DATE, ");
        sbQuery.append(" MO.OPERATION_NUMBER, ");
        sbQuery.append(" MO.OPERATION_STATE, ");
        sbQuery.append(" DECODE(HAO.ROLE, 1,'COMPRA', 2, 'VENTA') AS ROLE, ");
        sbQuery.append(" (SELECT P.MNEMONIC ||'-'|| P.ID_PARTICIPANT_PK FROM PARTICIPANT P WHERE P.ID_PARTICIPANT_PK=HAO.ID_INCHARGE_STOCK_PARTICIPANT) AS PARTICIPANT_DESCRIPTION, ");
        sbQuery.append(" HA.ACCOUNT_NUMBER, ");
        sbQuery.append(" HA.ID_HOLDER_ACCOUNT_PK , ");
        sbQuery.append(" MO.STOCK_QUANTITY AS TOTAL_QUANTITY, ");
        sbQuery.append(" HAO.STOCK_QUANTITY AS ACCOUNT_QUANTITY, ");
        sbQuery.append(" MO.CASH_SETTLEMENT_DATE FECHA_CONTADO, ");
        sbQuery.append(" MO.TERM_SETTLEMENT_DATE FECHA_PLAZO, ");
        sbQuery.append(" MO.BALLOT_NUMBER NRO_PAPELETA, ");
        sbQuery.append(" MO.SEQUENTIAL SECUENCIAL ");
        
        sbQuery.append(" FROM MECHANISM_OPERATION MO "); 
		sbQuery.append(" INNER JOIN HOLDER_ACCOUNT_OPERATION HAO ON HAO.ID_MECHANISM_OPERATION_FK=MO.ID_MECHANISM_OPERATION_PK ");
		sbQuery.append(" INNER JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK=HAO.ID_HOLDER_ACCOUNT_FK ");
		sbQuery.append(" WHERE HAO.OPERATION_PART = 1 AND HAO.HOLDER_ACCOUNT_STATE = 625 ");
		sbQuery.append(" AND MO.OPERATION_DATE BETWEEN TO_DATE('"+gfto.getInitialDate()+"','dd/MM/yyyy') AND TO_DATE('"+gfto.getFinalDate()+"','dd/MM/yyyy') ");
		
	
			if(Validations.validateIsNotNull(gfto.getMechanism())){
				sbQuery.append(" AND MO.ID_NEGOTIATION_MECHANISM_FK=" + gfto.getMechanism() + " ");
			}
			if(Validations.validateIsNotNull(gfto.getModality())){
				sbQuery.append(" AND MO.ID_NEGOTIATION_MODALITY_FK=" + gfto.getModality() + " ");
			}
			if(Validations.validateIsNotNull(gfto.getRole())){
				sbQuery.append(" AND HAO.ROLE=" +gfto.getRole()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getSellerParticipant())){
				sbQuery.append(" AND HAO.ID_INCHARGE_STOCK_PARTICIPANT='" +gfto.getSellerParticipant()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getIssuer())){
				sbQuery.append(" AND S.ID_ISSUER_FK='" +gfto.getIssuer()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getHolderAccount())){
				sbQuery.append(" AND HA.ID_HOLDER_ACCOUNT_PK=" +gfto.getHolderAccount()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getCui())){
				sbQuery.append(" AND (SELECT COUNT(*) FROM HOLDER_ACCOUNT_DETAIL HAD WHERE HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK AND HAD.ID_HOLDER_FK="+ gfto.getCui() + ")>0 ");
			}
			if(Validations.validateIsNotNull(gfto.getOperationState())){
				sbQuery.append(" AND MO.OPERATION_STATE=" +gfto.getOperationState()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getOperationNumber())){
				sbQuery.append(" AND MO.OPERATION_NUMBER=" +gfto.getOperationNumber()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getIdSecurityCodePk())){
				sbQuery.append(" AND MO.ID_SECURITY_CODE_FK='" +gfto.getIdSecurityCodePk()+"' ");
			}
					
			sbQuery.append(" ORDER BY MECHANISM,MODALITY,MO.OPERATION_DATE,MO.OPERATION_NUMBER ");
		
		return sbQuery.toString();
	}
	
	/**
	 * Gets the query daily list of international movement control report.
	 *
	 * @param gfto the gfto
	 * @param movementType the movement type
	 * @return the query daily list of international movement control report
	 */
	public String getQueryDailyListOfInternationalMovementControlReport(GenericSettlementOperationTO gfto, String movementType){
		StringBuilder sbQuery = new StringBuilder();
		
			sbQuery.append(" select ");
			sbQuery.append(" HAM.ID_SECURITY_CODE_FK, ");
			sbQuery.append(" p.MNEMONIC || ' - ' || p.DESCRIPTION PARTICIPANTE, ");
			sbQuery.append(" ha.ACCOUNT_NUMBER CUENTA, ");
			sbQuery.append(" ham.OPERATION_NUMBER NUMERO_OPERACION, ");
			sbQuery.append(" ham.OPERATION_DATE FECHA_OPERACION, ");
			sbQuery.append(" mt.MOVEMENT_NAME MOVIMIENTO, ");
			sbQuery.append(" decode(mt.IND_MOVEMENT_CLASS,1,'DEPOSITO','RETIRO') TIPO, ");
			sbQuery.append(" ham.MOVEMENT_QUANTITY CANTIDAD, ");
			sbQuery.append(" ham.CURRENCY AS MONEDA, ");
			sbQuery.append(" (ham.MOVEMENT_QUANTITY*ham.OPERATION_PRICE) MONTO ");
			sbQuery.append(" from HOLDER_ACCOUNT_MOVEMENT ham, INTERNATIONAL_OPERATION IO,MOVEMENT_TYPE mt, PARTICIPANT p, ");
			sbQuery.append(" HOLDER_ACCOUNT ha ");
			sbQuery.append(" where ");
			sbQuery.append(" ham.ID_TRADE_OPERATION_FK = IO.ID_INTERNATIONAL_OPERATION_PK ");
			sbQuery.append(" and ham.ID_MOVEMENT_TYPE_FK = mt.ID_MOVEMENT_TYPE_PK ");
			sbQuery.append(" and ham.ID_PARTICIPANT_FK = p.ID_PARTICIPANT_PK ");
			sbQuery.append(" and ham.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK ");
			sbQuery.append(" AND ham.OPERATION_DATE BETWEEN TO_DATE('"+gfto.getInitialDate()+"','dd/MM/yyyy') AND TO_DATE('"+gfto.getFinalDate()+"','dd/MM/yyyy') ");

			if(Validations.validateIsNotNull(gfto.getIdSecurityCodePk())){
				sbQuery.append(" AND HAM.ID_SECURITY_CODE_FK='" +gfto.getIdSecurityCodePk().toString()+"' ");
			}
			if(Validations.validateIsNotNull(movementType)){
				sbQuery.append(" AND mt.IND_MOVEMENT_CLASS=" +movementType+" ");
			}
			if(Validations.validateIsNotNull(gfto.getSellerParticipant())){
				sbQuery.append(" AND P.ID_PARTICIPANT_PK='" +gfto.getSellerParticipant()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getCurrency())){
				sbQuery.append(" AND ham.CURRENCY=" +gfto.getCurrency()+" ");
			}
			
			
			sbQuery.append("  ");

		return sbQuery.toString();
	}
	
	/**
	 * Gets the query holders assigned in operations buy sell report.
	 *
	 * @param gfto the gfto
	 * @return the query holders assigned in operations buy sell report
	 */
	public String getQueryHoldersAssignedInOperationsBuySellReport(GenericSettlementOperationTO gfto){
		StringBuilder sbQuery = new StringBuilder();
		
			sbQuery.append(" SELECT  ");
			sbQuery.append(" nm.DESCRIPTION MECANISMO, ");
			sbQuery.append(" nmo.MODALITY_NAME MODALIDAD, ");
			sbQuery.append(" mo.OPERATION_DATE FECHA_OPERACION, ");
			sbQuery.append(" (SELECT P.MNEMONIC ||' - '|| P.ID_PARTICIPANT_PK || ' - ' || P.DESCRIPTION FROM PARTICIPANT P WHERE P.ID_PARTICIPANT_PK=mo.ID_BUYER_PARTICIPANT_FK) PARTICIPANTE, ");
			sbQuery.append(" mo.OPERATION_NUMBER NUMERO_OPERACION, ");
			sbQuery.append(" mo.OPERATION_STATE ESTADO, ");
			sbQuery.append(" ha.ACCOUNT_NUMBER N_CUENTA, ");
			sbQuery.append(" FN_GET_HOLDER_NAME(ha.ID_HOLDER_ACCOUNT_PK) TITULAR, ");
			sbQuery.append(" mo.ID_SECURITY_CODE_FK CLAVE_VALOR, ");
			sbQuery.append(" decode(hao.ROLE,1,'COMPRA','VENTA') ROL, ");
			sbQuery.append(" hao.STOCK_QUANTITY CANTIDAD, ");
			sbQuery.append(" p.MNEMONIC NEMONICO, ");
			sbQuery.append(" mo.CASH_SETTLEMENT_DATE FECHA_LIQ, ");
			sbQuery.append(" ha.ID_HOLDER_ACCOUNT_PK ");
			sbQuery.append(" FROM ");
			sbQuery.append(" MECHANISM_OPERATION mo, HOLDER_ACCOUNT_OPERATION hao, NEGOTIATION_MECHANISM nm, NEGOTIATION_MODALITY nmo, ");
			sbQuery.append(" HOLDER_ACCOUNT ha, PARTICIPANT p ");
			sbQuery.append(" WHERE ");
			sbQuery.append(" mo.ID_MECHANISM_OPERATION_PK = hao.ID_MECHANISM_OPERATION_FK ");
			sbQuery.append(" and hao.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK ");
			sbQuery.append(" and ha.ID_PARTICIPANT_FK = p.ID_PARTICIPANT_PK ");
			sbQuery.append(" and mo.ID_NEGOTIATION_MECHANISM_FK = nm.ID_NEGOTIATION_MECHANISM_PK ");
			sbQuery.append(" and mo.ID_NEGOTIATION_MODALITY_FK = nmo.ID_NEGOTIATION_MODALITY_PK ");
			sbQuery.append(" AND MO.OPERATION_STATE IN (612,614) ");
			//sbQuery.append(" AND NMO.MODALITY_CODE='CVT' ");
			sbQuery.append(" AND MO.OPERATION_DATE = TO_DATE('"+gfto.getInitialDate()+"','dd/MM/yyyy')");

			if(Validations.validateIsNotNull(gfto.getBuyerParticipant())){
				sbQuery.append(" AND MO.ID_BUYER_PARTICIPANT_FK='" +gfto.getBuyerParticipant()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getHolderAccount())){
				sbQuery.append(" AND HA.ID_HOLDER_ACCOUNT_PK=" +gfto.getHolderAccount()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getMechanism())){
				sbQuery.append(" AND NM.ID_NEGOTIATION_MECHANISM_PK=" + gfto.getMechanism() + " ");
			}
			if(Validations.validateIsNotNull(gfto.getModality())){
				sbQuery.append(" AND NMO.ID_NEGOTIATION_MODALITY_PK=" + gfto.getModality() + " ");
			}
			if(Validations.validateIsNotNull(gfto.getIdSecurityCodePk())){
				sbQuery.append(" AND MO.ID_SECURITY_CODE_FK='" +gfto.getIdSecurityCodePk()+"' ");
			}
			if(Validations.validateIsNotNull(gfto.getOperationState())){
				sbQuery.append(" AND MO.OPERATION_STATE=" +gfto.getOperationState()+" ");
			}
			if(Validations.validateIsNotNull(gfto.getRole())){
				sbQuery.append(" AND HAO.ROLE=" +gfto.getRole()+" ");
			}
			
			sbQuery.append(" order by ");
			sbQuery.append("   4, mo.ID_SECURITY_CODE_FK, mo.OPERATION_NUMBER,ha.ACCOUNT_NUMBER,hao.ROLE,mo.OPERATION_DATE ");
			sbQuery.append("  ");

	return sbQuery.toString();
	}
	
	/**
	 * Metodo para extraer el usuario que ejecuta el reporte
	 * @param processDate
	 * @return
	 */
	public Integer getIdParticipantPk(String idParticipantPK){

		StringBuilder querySql = new StringBuilder();
		
		querySql.append("  SELECT ID_PARTICIPANT_FK                                ");
		querySql.append("  FROM SECURITY.USER_ACCOUNT                              ");
		querySql.append("  WHERE  1 = 1                          				   "); 
		querySql.append("  AND LOGIN_USER ='" + idParticipantPK +"' ");
		
		Query query = em.createNativeQuery(querySql.toString());
		
		BigDecimal idParticipant = (BigDecimal) query.getSingleResult();
		
		Integer idParticipantInt = null;
		
		if(Validations.validateIsNotNullAndNotEmpty(idParticipant)){
			idParticipantInt = idParticipant.intValue();
		}
		
		return idParticipantInt;
	}
		
		
	/**
	 * QUERY VENCIMIENTOS DE REPORTO
	 * ISSUE 815
	 * @param gfto the gfto
	 * @return query
	 */
	public String getExpirationModalityReport(GenericSettlementOperationTO gfto){
		StringBuilder sbQuery = new StringBuilder();
		Integer idParticipantPk = null;
		
		//Si el filtro participante esta lleno
		if(Validations.validateIsNotNull(gfto.getIssuer())){
			idParticipantPk = getIdParticipantPk(gfto.getIssuer());
		}
		
		//REPORTOS SIN ANTICIPACION
		sbQuery.append("	select                                                                                                ");
		sbQuery.append("	MO.ID_MECHANISM_OPERATION_PK,                                                                         ");
		sbQuery.append("	MO.OPERATION_DATE FEC_OPERACION,                                                                      ");
		sbQuery.append("	MO.TERM_SETTLEMENT_DATE FEC_VENCIMIENTO,                                                              ");
		sbQuery.append("	MO.TERM_SETTLEMENT_DATE FEC_VENC_PLAZO,                                                               ");
		sbQuery.append("	MO.BALLOT_NUMBER || '/' || MO.SEQUENTIAL PAPELETA_SEQ,                                                ");
		sbQuery.append("	NM.MODALITY_NAME MODALIDAD,                                                                           ");
		sbQuery.append("	P_SELLER.MNEMONIC P_REPORTADO,                                                                         ");
		if(Validations.validateIsNotNull(idParticipantPk)){
			sbQuery.append(" CASE WHEN " + idParticipantPk +"  = P_SELLER.ID_PARTICIPANT_PK THEN   ");
		}
		if(Validations.validateIsNotNull(gfto.getCui()) && !Validations.validateIsNotNull(idParticipantPk)){
			sbQuery.append(" CASE WHEN " + gfto.getCui() +" IN  (   ");
			sbQuery.append(" 	SELECT H.ID_HOLDER_PK                                                                                          ");
			sbQuery.append(" 	FROM MECHANISM_OPERATION MO_SUBQ                                                                               ");
			sbQuery.append(" 	INNER JOIN HOLDER_ACCOUNT_OPERATION hao on MO_SUBQ.ID_MECHANISM_OPERATION_PK = hao.ID_MECHANISM_OPERATION_FK   ");
			sbQuery.append(" 	INNER JOIN HOLDER_ACCOUNT_DETAIL had on hao.ID_HOLDER_ACCOUNT_FK = had.ID_HOLDER_ACCOUNT_FK                    ");
			sbQuery.append(" 	INNER JOIN HOLDER H ON H.ID_HOLDER_PK = had.ID_HOLDER_FK                                                       ");
			sbQuery.append(" 	WHERE hao.HOLDER_ACCOUNT_STATE = 625                                                                           ");
			sbQuery.append(" 	AND MO_SUBQ.ID_MECHANISM_OPERATION_PK = Mo.Id_Mechanism_Operation_Pk          )    THEN                            ");
		}
		sbQuery.append("	(SELECT LISTAGG(HA.ACCOUNT_NUMBER , chr(13) )                                                                   ");
		sbQuery.append("	WITHIN GROUP (ORDER BY HA.ACCOUNT_NUMBER)  																		"); 
		sbQuery.append("	FROM MECHANISM_OPERATION MO_SUBQ                                                                                ");
		sbQuery.append("	INNER JOIN HOLDER_ACCOUNT_OPERATION hao on MO_SUBQ.ID_MECHANISM_OPERATION_PK = hao.ID_MECHANISM_OPERATION_FK    ");
		sbQuery.append("	INNER JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK = hao.ID_HOLDER_ACCOUNT_FK                              ");
		sbQuery.append("	INNER JOIN HOLDER_ACCOUNT_DETAIL had on hao.ID_HOLDER_ACCOUNT_FK = had.ID_HOLDER_ACCOUNT_FK                     ");
		sbQuery.append("	INNER JOIN HOLDER H ON H.ID_HOLDER_PK = had.ID_HOLDER_FK                                                        ");
		sbQuery.append("	WHERE hao.HOLDER_ACCOUNT_STATE = 625                                                                            ");
		sbQuery.append("	AND hao.OPERATION_PART = 1                                                                                      ");
		sbQuery.append("	AND hao.ROLE = 2                                                                                                ");
		sbQuery.append("	AND MO_SUBQ.ID_MECHANISM_OPERATION_PK = Mo.Id_Mechanism_Operation_Pk	)									    ");	
		if(Validations.validateIsNotNull(idParticipantPk) || Validations.validateIsNotNull(gfto.getCui())){
			sbQuery.append(" ELSE NULL END   ");
		}
		sbQuery.append("	C_COMPRA,                                                                                                    ");
		sbQuery.append("	P_BUYER.MNEMONIC P_REPORTADOR,                                                                      			");
		if(Validations.validateIsNotNull(idParticipantPk)){
			sbQuery.append(" CASE WHEN " + idParticipantPk +"  = P_BUYER.ID_PARTICIPANT_PK THEN   ");
		}
		if(Validations.validateIsNotNull(gfto.getCui()) && !Validations.validateIsNotNull(idParticipantPk)){
			sbQuery.append(" CASE WHEN " + gfto.getCui() +" IN  (   ");
			sbQuery.append(" 	SELECT H.ID_HOLDER_PK                                                                                          ");
			sbQuery.append(" 	FROM MECHANISM_OPERATION MO_SUBQ                                                                               ");
			sbQuery.append(" 	INNER JOIN HOLDER_ACCOUNT_OPERATION hao on MO_SUBQ.ID_MECHANISM_OPERATION_PK = hao.ID_MECHANISM_OPERATION_FK   ");
			sbQuery.append(" 	INNER JOIN HOLDER_ACCOUNT_DETAIL had on hao.ID_HOLDER_ACCOUNT_FK = had.ID_HOLDER_ACCOUNT_FK                    ");
			sbQuery.append(" 	INNER JOIN HOLDER H ON H.ID_HOLDER_PK = had.ID_HOLDER_FK                                                       ");
			sbQuery.append(" 	WHERE hao.HOLDER_ACCOUNT_STATE = 625                                                                           ");
			sbQuery.append(" 	AND MO_SUBQ.ID_MECHANISM_OPERATION_PK = Mo.Id_Mechanism_Operation_Pk          )    THEN                            ");
		}
		sbQuery.append("	(SELECT LISTAGG(HA.ACCOUNT_NUMBER , chr(13) )                                                                   ");
		sbQuery.append("	WITHIN GROUP (ORDER BY HA.ACCOUNT_NUMBER)  																		"); 
		sbQuery.append("	FROM MECHANISM_OPERATION MO_SUBQ                                                                                ");
		sbQuery.append("	INNER JOIN HOLDER_ACCOUNT_OPERATION hao on MO_SUBQ.ID_MECHANISM_OPERATION_PK = hao.ID_MECHANISM_OPERATION_FK    ");
		sbQuery.append("	INNER JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK = hao.ID_HOLDER_ACCOUNT_FK                              ");
		sbQuery.append("	INNER JOIN HOLDER_ACCOUNT_DETAIL had on hao.ID_HOLDER_ACCOUNT_FK = had.ID_HOLDER_ACCOUNT_FK                     ");
		sbQuery.append("	INNER JOIN HOLDER H ON H.ID_HOLDER_PK = had.ID_HOLDER_FK                                                        ");
		sbQuery.append("	WHERE hao.HOLDER_ACCOUNT_STATE = 625                                                                            ");
		sbQuery.append("	AND hao.OPERATION_PART = 1                                                                                      ");
		sbQuery.append("	AND hao.ROLE = 1                                                                                                ");
		sbQuery.append("	AND MO_SUBQ.ID_MECHANISM_OPERATION_PK = Mo.Id_Mechanism_Operation_Pk	)										");	
		if(Validations.validateIsNotNull(idParticipantPk) || Validations.validateIsNotNull(gfto.getCui())){
			sbQuery.append(" ELSE NULL END   ");
		}
		sbQuery.append("	C_VENTA,                                                                                                     ");
		sbQuery.append("	MO.ID_SECURITY_CODE_FK CLAVE_VALOR,                                                                   ");
		sbQuery.append("	MO.STOCK_QUANTITY CANTIDAD,                                                                           ");
		sbQuery.append("	MO.AMOUNT_RATE TASA,                                                                                  ");
		sbQuery.append("	MO.TERM_SETTLEMENT_DAYS PLAZO_ORIGINAL,                                                               ");
		sbQuery.append("	CASE WHEN MO.OPERATION_STATE = 615 THEN MO.TERM_SETTLEMENT_DATE - TRUNC(SYSDATE) ELSE 0 END DIAS_VIDA, ");
		sbQuery.append("	'NO' IND_ANTICIPADA,                                                                                  ");
		sbQuery.append("	0 CANT_ANTICIPADA,                                                                                    ");
		sbQuery.append("	MO.CASH_PRICE PREC_INICIAL,                                                                           ");
		sbQuery.append("	MO.CASH_AMOUNT MONTO_INICIAL,                                                                         ");
		sbQuery.append("	MO.TERM_PRICE PREC_FINAL,                                                                             ");
		sbQuery.append("	MO.TERM_AMOUNT MONTO_FINAL,                                                                           ");
		sbQuery.append("	PT_CURRENCY.PARAMETER_NAME MONEDA                                                                     ");
		sbQuery.append("	from MECHANISM_OPERATION MO                                                                           ");
		sbQuery.append("	INNER JOIN NEGOTIATION_MODALITY NM ON NM.ID_NEGOTIATION_MODALITY_PK = MO.ID_NEGOTIATION_MODALITY_FK   ");
		sbQuery.append("	INNER JOIN PARTICIPANT P_BUYER ON MO.ID_BUYER_PARTICIPANT_FK = P_BUYER.ID_PARTICIPANT_PK              ");
		sbQuery.append("	INNER JOIN PARTICIPANT P_SELLER ON MO.ID_SELLER_PARTICIPANT_FK = P_SELLER.ID_PARTICIPANT_PK           ");
		sbQuery.append("	INNER JOIN PARAMETER_TABLE PT_CURRENCY ON PT_CURRENCY.PARAMETER_TABLE_PK = MO.OPERATION_CURRENCY      ");
		sbQuery.append("	WHERE 1 = 1                                                                                           ");
		sbQuery.append("	AND MO.ID_NEGOTIATION_MECHANISM_FK = 1                                                                ");
		sbQuery.append("	AND MO.ID_MECHANISM_OPERATION_PK NOT IN (                                                                                ");
		sbQuery.append("	     SELECT SUB_MO.ID_MECHANISM_OPERATION_PK FROM                                                                        ");
		sbQuery.append("	     settlement_request SUB_SR                                                                                           ");
		sbQuery.append("	     INNER JOIN settlement_date_operation SUB_SDO on SUB_Sr.Id_Settlement_Request_Pk = SUB_Sdo.Id_Settlement_Request_Fk  ");
		sbQuery.append("	     INNER JOIN settlement_operation SUB_SO on SUB_Sdo.Id_Settlement_Operation_Fk = SUB_So.Id_Settlement_Operation_Pk    ");
		sbQuery.append("	     INNER JOIN mechanism_operation SUB_MO on SUB_So.Id_Mechanism_Operation_Fk = SUB_Mo.Id_Mechanism_Operation_Pk        ");
		sbQuery.append("	     AND SUB_sr.request_type = 2019                                                                                      ");
		sbQuery.append("	     AND SUB_mo.ID_NEGOTIATION_MECHANISM_FK=1                                                                            ");
		sbQuery.append("	     AND SUB_SR.REQUEST_STATE = 1543                                                                                     ");
		sbQuery.append("	     WHERE 1 = 1                                                                                                         ");
		sbQuery.append("	     AND SUB_Mo.Id_Mechanism_Operation_Pk = Mo.Id_Mechanism_Operation_Pk                                                 ");
		sbQuery.append("	 )                                                                                                                       ");
		
		if(Validations.validateIsNotNull(gfto.getParticipant())){
			sbQuery.append(" AND (MO.ID_BUYER_PARTICIPANT_FK='" +gfto.getParticipant()+"' ");
			sbQuery.append(" OR MO.ID_SELLER_PARTICIPANT_FK='" +gfto.getParticipant()+"') ");
		
			//El filtro ROL solo funciona si el filtro participante no es nulo.
			if(Validations.validateIsNotNull(gfto.getRole())){
				if(gfto.getRole().equals(GeneralConstants.ONE_VALUE_STRING)){
					sbQuery.append(" AND MO.ID_BUYER_PARTICIPANT_FK='" +gfto.getParticipant()+"' ");
				}else{
					sbQuery.append(" AND MO.ID_SELLER_PARTICIPANT_FK='" +gfto.getParticipant()+"' ");
				}
			}
		}
		
		if(Validations.validateIsNotNull(gfto.getModalityGroup())){
			sbQuery.append(" AND MO.ID_NEGOTIATION_MODALITY_FK = '" +gfto.getModalityGroup()+"' ");
		}else{
			sbQuery.append(" AND MO.ID_NEGOTIATION_MODALITY_FK IN (3,4) ");
		}
		if(Validations.validateIsNotNull(gfto.getIdSecurityCodePk())){
			sbQuery.append(" AND MO.ID_SECURITY_CODE_FK='" +gfto.getIdSecurityCodePk()+"' ");
		}
		if(Validations.validateIsNotNull(gfto.getCurrency())){
			sbQuery.append(" AND MO.OPERATION_CURRENCY='" +gfto.getCurrency()+"' ");
		}
		if(Validations.validateIsNotNull(gfto.getExpirationType())){
			if(gfto.getExpirationType().equals(GeneralConstants.ONE_VALUE_STRING)){
				//VENCIDO
				sbQuery.append(" AND MO.OPERATION_STATE = 616 ");
			}else{
				//VIGENTE
				sbQuery.append(" AND MO.OPERATION_STATE = 615 ");
			}
		}
		if(Validations.validateIsNotNull(gfto.getDateType())){
			if(gfto.getDateType().equals(GeneralConstants.ONE_VALUE_STRING)){
				//OPERACION
				sbQuery.append(" AND MO.OPERATION_DATE BETWEEN TO_DATE('" +gfto.getInitialDate()+"','DD/MM/YYYY') ");
				sbQuery.append(" AND TO_DATE('" +gfto.getFinalDate()+"','DD/MM/YYYY') ");
			}else{
				//VENCIMIENTO
				sbQuery.append(" AND MO.TERM_SETTLEMENT_DATE BETWEEN TO_DATE('" +gfto.getInitialDate()+"','DD/MM/YYYY') ");
				sbQuery.append(" AND TO_DATE('" +gfto.getFinalDate()+"','DD/MM/YYYY') ");
			}
		}
		
		if(Validations.validateIsNotNull(gfto.getCui())){
			sbQuery.append(" AND '" +gfto.getCui()+"' IN  (");
			sbQuery.append(" 	SELECT H.ID_HOLDER_PK                                                                                          ");
			sbQuery.append(" 	FROM MECHANISM_OPERATION MO_SUBQ                                                                               ");
			sbQuery.append(" 	INNER JOIN HOLDER_ACCOUNT_OPERATION hao on MO_SUBQ.ID_MECHANISM_OPERATION_PK = hao.ID_MECHANISM_OPERATION_FK   ");
			sbQuery.append(" 	INNER JOIN HOLDER_ACCOUNT_DETAIL had on hao.ID_HOLDER_ACCOUNT_FK = had.ID_HOLDER_ACCOUNT_FK                    ");
			sbQuery.append(" 	INNER JOIN HOLDER H ON H.ID_HOLDER_PK = had.ID_HOLDER_FK                                                       ");
			sbQuery.append(" 	WHERE hao.HOLDER_ACCOUNT_STATE = 625                                                                           ");
			sbQuery.append(" 	AND MO_SUBQ.ID_MECHANISM_OPERATION_PK = Mo.Id_Mechanism_Operation_Pk          )                                ");
		}
		
		//REPORTOS ANTICIPADOS
		sbQuery.append("	UNION ALL                                                                                                    ");
		sbQuery.append("	                                                                                                             ");
		sbQuery.append("	SELECT                                                                                                       ");
		sbQuery.append("	MO.ID_MECHANISM_OPERATION_PK,                                                                                ");
		sbQuery.append("	MO.OPERATION_DATE FEC_OPERACION,                                                                             ");
		sbQuery.append("	MO.TERM_SETTLEMENT_DATE FEC_VENCIMIENTO,                                                                     ");
		sbQuery.append("	TO_DATE(sdo.settlement_date,'dd/mm/yyyy') FEC_VENC_PLAZO,                                                    ");
		sbQuery.append("	MO.BALLOT_NUMBER || '/' || MO.SEQUENTIAL PAPELETA_SEQ,                                                       ");
		sbQuery.append("	NM.MODALITY_NAME MODALIDAD,                                                                                  ");
		sbQuery.append("	DECODE(SO.OPERATION_PART,1,PA_BUYER.MNEMONIC, PA_SELLER.MNEMONIC) P_REPORTADO,                               ");
		if(Validations.validateIsNotNull(idParticipantPk)){
			sbQuery.append(" CASE WHEN " + idParticipantPk +"  = DECODE(SO.OPERATION_PART,1,PA_BUYER.ID_PARTICIPANT_PK, PA_SELLER.ID_PARTICIPANT_PK)  THEN   ");
		}
		if(Validations.validateIsNotNull(gfto.getCui()) && !Validations.validateIsNotNull(idParticipantPk)){
			sbQuery.append(" CASE WHEN " + gfto.getCui() +" IN  (   ");
			sbQuery.append(" 	SELECT H.ID_HOLDER_PK                                                                                          ");
			sbQuery.append(" 	FROM MECHANISM_OPERATION MO_SUBQ                                                                               ");
			sbQuery.append(" 	INNER JOIN HOLDER_ACCOUNT_OPERATION hao on MO_SUBQ.ID_MECHANISM_OPERATION_PK = hao.ID_MECHANISM_OPERATION_FK   ");
			sbQuery.append(" 	INNER JOIN HOLDER_ACCOUNT_DETAIL had on hao.ID_HOLDER_ACCOUNT_FK = had.ID_HOLDER_ACCOUNT_FK                    ");
			sbQuery.append(" 	INNER JOIN HOLDER H ON H.ID_HOLDER_PK = had.ID_HOLDER_FK                                                       ");
			sbQuery.append(" 	WHERE hao.HOLDER_ACCOUNT_STATE = 625                                                                           ");
			sbQuery.append(" 	AND MO_SUBQ.ID_MECHANISM_OPERATION_PK = Mo.Id_Mechanism_Operation_Pk          )    THEN                            ");
		}
		sbQuery.append("	(SELECT LISTAGG(HA.ACCOUNT_NUMBER , chr(13) )                                                                   ");
		sbQuery.append("	WITHIN GROUP (ORDER BY HA.ACCOUNT_NUMBER)  																		"); 
		sbQuery.append("	FROM MECHANISM_OPERATION MO_SUBQ                                                                                ");
		sbQuery.append("	INNER JOIN HOLDER_ACCOUNT_OPERATION hao on MO_SUBQ.ID_MECHANISM_OPERATION_PK = hao.ID_MECHANISM_OPERATION_FK    ");
		sbQuery.append("	INNER JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK = hao.ID_HOLDER_ACCOUNT_FK                              ");
		sbQuery.append("	INNER JOIN HOLDER_ACCOUNT_DETAIL had on hao.ID_HOLDER_ACCOUNT_FK = had.ID_HOLDER_ACCOUNT_FK                     ");
		sbQuery.append("	INNER JOIN HOLDER H ON H.ID_HOLDER_PK = had.ID_HOLDER_FK                                                        ");
		sbQuery.append("	WHERE hao.HOLDER_ACCOUNT_STATE = 625                                                                            ");
		sbQuery.append("	AND hao.OPERATION_PART = 1                                                                                      ");
		sbQuery.append("	AND hao.ROLE = 2                                                                                                ");
		sbQuery.append("	AND MO_SUBQ.ID_MECHANISM_OPERATION_PK = Mo.Id_Mechanism_Operation_Pk)											");	
		if(Validations.validateIsNotNull(idParticipantPk) || Validations.validateIsNotNull(gfto.getCui())){
			sbQuery.append(" ELSE NULL END   ");
		}
		sbQuery.append("	C_COMPRA,                                                                                                     ");
		sbQuery.append("	DECODE(SO.OPERATION_PART,1,PA_SELLER.MNEMONIC, PA_BUYER.MNEMONIC) P_REPORTADOR,                                 ");
		if(Validations.validateIsNotNull(idParticipantPk)){
			sbQuery.append(" CASE WHEN " + idParticipantPk +"  = DECODE(SO.OPERATION_PART,1,PA_SELLER.ID_PARTICIPANT_PK, PA_BUYER.ID_PARTICIPANT_PK)  THEN   ");
		}
		if(Validations.validateIsNotNull(gfto.getCui()) && !Validations.validateIsNotNull(idParticipantPk)){
			sbQuery.append(" CASE WHEN " + gfto.getCui() +" IN  (   ");
			sbQuery.append(" 	SELECT H.ID_HOLDER_PK                                                                                          ");
			sbQuery.append(" 	FROM MECHANISM_OPERATION MO_SUBQ                                                                               ");
			sbQuery.append(" 	INNER JOIN HOLDER_ACCOUNT_OPERATION hao on MO_SUBQ.ID_MECHANISM_OPERATION_PK = hao.ID_MECHANISM_OPERATION_FK   ");
			sbQuery.append(" 	INNER JOIN HOLDER_ACCOUNT_DETAIL had on hao.ID_HOLDER_ACCOUNT_FK = had.ID_HOLDER_ACCOUNT_FK                    ");
			sbQuery.append(" 	INNER JOIN HOLDER H ON H.ID_HOLDER_PK = had.ID_HOLDER_FK                                                       ");
			sbQuery.append(" 	WHERE hao.HOLDER_ACCOUNT_STATE = 625                                                                           ");
			sbQuery.append(" 	AND MO_SUBQ.ID_MECHANISM_OPERATION_PK = Mo.Id_Mechanism_Operation_Pk          )    THEN                            ");
		}
		sbQuery.append("	(SELECT LISTAGG(HA.ACCOUNT_NUMBER , chr(13) )                                                                   ");
		sbQuery.append("	WITHIN GROUP (ORDER BY HA.ACCOUNT_NUMBER)  																		"); 
		sbQuery.append("	FROM MECHANISM_OPERATION MO_SUBQ                                                                                ");
		sbQuery.append("	INNER JOIN HOLDER_ACCOUNT_OPERATION hao on MO_SUBQ.ID_MECHANISM_OPERATION_PK = hao.ID_MECHANISM_OPERATION_FK    ");
		sbQuery.append("	INNER JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK = hao.ID_HOLDER_ACCOUNT_FK                              ");
		sbQuery.append("	INNER JOIN HOLDER_ACCOUNT_DETAIL had on hao.ID_HOLDER_ACCOUNT_FK = had.ID_HOLDER_ACCOUNT_FK                     ");
		sbQuery.append("	INNER JOIN HOLDER H ON H.ID_HOLDER_PK = had.ID_HOLDER_FK                                                        ");
		sbQuery.append("	WHERE hao.HOLDER_ACCOUNT_STATE = 625                                                                            ");
		sbQuery.append("	AND hao.OPERATION_PART = 1                                                                                      ");
		sbQuery.append("	AND hao.ROLE = 1                                                                                                ");
		sbQuery.append("	AND MO_SUBQ.ID_MECHANISM_OPERATION_PK = Mo.Id_Mechanism_Operation_Pk	)										");	
		if(Validations.validateIsNotNull(idParticipantPk) || Validations.validateIsNotNull(gfto.getCui())){
			sbQuery.append(" ELSE NULL END   ");
		}
		sbQuery.append("	C_VENTA,                                                                                                      ");
		sbQuery.append("	MO.ID_SECURITY_CODE_FK CLAVE_VALOR,                                                                          ");
		sbQuery.append("	MO.STOCK_QUANTITY CANTIDAD,                                                                                  ");
		sbQuery.append("	MO.AMOUNT_RATE TASA,                                                                                         ");
		sbQuery.append("	MO.TERM_SETTLEMENT_DAYS PLAZO_ORIGINAL,                                                                      ");
		sbQuery.append("	CASE WHEN MO.OPERATION_STATE = 615 THEN MO.TERM_SETTLEMENT_DATE - TRUNC(SYSDATE) ELSE 0 END DIAS_VIDA,       ");
		sbQuery.append("	'SI' IND_ANTICIPADA,                                                                                         ");
		sbQuery.append("	sdo.stock_quantity CANT_ANTICIPADA,                                                                          ");
		sbQuery.append("	MO.CASH_PRICE PREC_INICIAL,                                                                                  ");
		sbQuery.append("	MO.CASH_AMOUNT MONTO_INICIAL,                                                                                ");
		sbQuery.append("	sdo.settlement_price PREC_FINAL,                                                                             ");
		sbQuery.append("	SDO.SETTLEMENT_AMOUNT MONTO_FINAL,                                                                           ");
		sbQuery.append("	PT_CURRENCY.PARAMETER_NAME MONEDA                                                                            ");
		sbQuery.append("	                                                                                                             ");
		sbQuery.append("	from                                                                                                         ");
		sbQuery.append("	settlement_request sr                                                                                        ");
		sbQuery.append("	INNER JOIN settlement_date_operation sdo on Sr.Id_Settlement_Request_Pk = Sdo.Id_Settlement_Request_Fk       ");
		sbQuery.append("	INNER JOIN settlement_operation so on Sdo.Id_Settlement_Operation_Fk = So.Id_Settlement_Operation_Pk         ");
		sbQuery.append("	INNER JOIN mechanism_operation MO on So.Id_Mechanism_Operation_Fk = Mo.Id_Mechanism_Operation_Pk             ");
		sbQuery.append("	INNER JOIN PARAMETER_TABLE PT_CURRENCY ON PT_CURRENCY.PARAMETER_TABLE_PK = MO.OPERATION_CURRENCY             ");
		sbQuery.append("	INNER JOIN NEGOTIATION_MODALITY NM ON NM.ID_NEGOTIATION_MODALITY_PK = MO.ID_NEGOTIATION_MODALITY_FK          ");
		sbQuery.append("	INNER JOIN participant PA_SELLER ON PA_SELLER.ID_PARTICIPANT_PK = MO.ID_SELLER_PARTICIPANT_FK                ");
		sbQuery.append("	INNER JOIN participant PA_BUYER ON PA_BUYER.ID_PARTICIPANT_PK = MO.ID_BUYER_PARTICIPANT_FK                   ");
		sbQuery.append("	where 1 = 1                                                                                                  ");
		sbQuery.append("	AND sr.request_type = 2019                                                                                   ");
		sbQuery.append("	AND mo.ID_NEGOTIATION_MECHANISM_FK=1                                                                         ");
		sbQuery.append("	AND SR.REQUEST_STATE = 1543                                                                                  ");

		if(Validations.validateIsNotNull(gfto.getParticipant())){
			sbQuery.append(" AND (MO.ID_BUYER_PARTICIPANT_FK='" +gfto.getParticipant()+"' ");
			sbQuery.append(" OR MO.ID_SELLER_PARTICIPANT_FK='" +gfto.getParticipant()+"') ");
		
			//El filtro ROL solo funciona si el filtro participante no es nulo.
			if(Validations.validateIsNotNull(gfto.getRole())){
				if(gfto.getRole().equals(GeneralConstants.ONE_VALUE_STRING)){
					sbQuery.append(" AND MO.ID_BUYER_PARTICIPANT_FK='" +gfto.getParticipant()+"' ");
				}else{
					sbQuery.append(" AND MO.ID_SELLER_PARTICIPANT_FK='" +gfto.getParticipant()+"' ");
				}
			}
		}
		
		if(Validations.validateIsNotNull(gfto.getModalityGroup())){
			sbQuery.append(" AND MO.ID_NEGOTIATION_MODALITY_FK = '" +gfto.getModalityGroup()+"' ");
		}else{
			sbQuery.append(" AND MO.ID_NEGOTIATION_MODALITY_FK IN (3,4) ");
		}
		if(Validations.validateIsNotNull(gfto.getIdSecurityCodePk())){
			sbQuery.append(" AND MO.ID_SECURITY_CODE_FK='" +gfto.getIdSecurityCodePk()+"' ");
		}
		if(Validations.validateIsNotNull(gfto.getCurrency())){
			sbQuery.append(" AND MO.OPERATION_CURRENCY='" +gfto.getCurrency()+"' ");
		}

		if(Validations.validateIsNotNull(gfto.getDateType())){
			if(gfto.getDateType().equals(GeneralConstants.ONE_VALUE_STRING)){
				//OPERACION
				sbQuery.append(" AND MO.OPERATION_DATE BETWEEN  TO_DATE('" +gfto.getInitialDate()+"','DD/MM/YYYY') ");
				sbQuery.append(" AND  TO_DATE('" +gfto.getFinalDate()+"','DD/MM/YYYY') ");
			}else{
				//VENCIMIENTO
				sbQuery.append(" AND SDO.SETTLEMENT_DATE BETWEEN  TO_DATE('" +gfto.getInitialDate()+"','DD/MM/YYYY') ");
				sbQuery.append(" AND  TO_DATE('" +gfto.getFinalDate()+"','DD/MM/YYYY') ");
			}
		}
		
		if(Validations.validateIsNotNull(gfto.getCui())){
			sbQuery.append(" AND '" +gfto.getCui()+"' IN  (");
			sbQuery.append(" 	SELECT H.ID_HOLDER_PK                                                                                          ");
			sbQuery.append(" 	FROM MECHANISM_OPERATION MO_SUBQ                                                                               ");
			sbQuery.append(" 	INNER JOIN HOLDER_ACCOUNT_OPERATION hao on MO_SUBQ.ID_MECHANISM_OPERATION_PK = hao.ID_MECHANISM_OPERATION_FK   ");
			sbQuery.append(" 	INNER JOIN HOLDER_ACCOUNT_DETAIL had on hao.ID_HOLDER_ACCOUNT_FK = had.ID_HOLDER_ACCOUNT_FK                    ");
			sbQuery.append(" 	INNER JOIN HOLDER H ON H.ID_HOLDER_PK = had.ID_HOLDER_FK                                                       ");
			sbQuery.append(" 	WHERE hao.HOLDER_ACCOUNT_STATE = 625                                                                           ");
			sbQuery.append(" 	AND MO_SUBQ.ID_MECHANISM_OPERATION_PK = Mo.Id_Mechanism_Operation_Pk          )                                ");
		}
		
		sbQuery.append("	ORDER BY                                                                                                     ");
		sbQuery.append("	  20,1,9                                                                                                     ");
		
	return sbQuery.toString();
}
	
	/**
	 * Gets the query holders processed and unprocessed report.
	 *
	 * @param gfto the gfto
	 * @return the query holders processed and unprocessed report
	 */
	public String getQueryHoldersProcessedAndUnprocessedReport(GenericSettlementOperationTO gfto){
		StringBuilder sbQuery = new StringBuilder();
		
			sbQuery.append(" SELECT 																									 		");
			sbQuery.append(" (SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = SP.SCHEDULE_TYPE) SCHEDULE_TYPE, 				");//0 ESQUEMA
			sbQuery.append(" (SELECT TEXT1 FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = SP.CURRENCY) MONEDA, 								");//1 MONEDA
			sbQuery.append(" NM.MODALITY_NAME MODALIDAD, 																						");//2 MODALIDAD
			sbQuery.append(" sum(( select count(DISTINCT SO.ID_SETTLEMENT_OPERATION_PK) 														");
			sbQuery.append(" 	FROM SETTLEMENT_OPERATION SO1 																					");
			sbQuery.append(" 	WHERE SO.ID_SETTLEMENT_OPERATION_PK = SO1.ID_SETTLEMENT_OPERATION_PK AND OS.OPERATION_STATE = 1288)) TOTAL_OPERACIONES, ");//3  TOTAL DE OPERACIONES
			sbQuery.append(" sum((select COUNT(DISTINCT SAO.ID_SETTLEMENT_ACCOUNT_PK) 															");
			sbQuery.append(" 	FROM  SETTLEMENT_OPERATION SO1,SETTLEMENT_ACCOUNT_OPERATION SAO, SECURITY SE 									");
			sbQuery.append(" 	WHERE SO.ID_SETTLEMENT_OPERATION_PK = SO1.ID_SETTLEMENT_OPERATION_PK 											");
			sbQuery.append(" 	and SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK 												");
			sbQuery.append(" 	AND MO.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK																");
			sbQuery.append(" 	AND SAO.OPERATION_STATE = 625																					");
			sbQuery.append(" 	AND OS.OPERATION_STATE = 1288																					");
//			sbQuery.append(" 	AND (SE.SECURITY_CLASS <> 406 OR 																				");
//			sbQuery.append(" 		(SE.SECURITY_CLASS = 406 AND SAO.ROLE=2 AND NM.ID_NEGOTIATION_MODALITY_PK = 13 ))							");
			sbQuery.append(" 	AND SO.IND_DEMATERIALIZATION <> 1																				");
			sbQuery.append(" 	AND (SO.OPERATION_STATE =615 AND SO.OPERATION_PART=1 OR SO.OPERATION_STATE=616 AND SO.OPERATION_PART=2) 		");
			sbQuery.append(" )) CUENTAS_CONFIRMADAS,																							");//CUENTAS CONFIRMADAS
			sbQuery.append(" sum((select COUNT(DISTINCT SAO.ID_SETTLEMENT_ACCOUNT_PK) 															");
			sbQuery.append("  	FROM  SETTLEMENT_OPERATION SO1,SETTLEMENT_ACCOUNT_OPERATION SAO, SECURITY SE  									");
			sbQuery.append(" 	WHERE SO.ID_SETTLEMENT_OPERATION_PK = SO1.ID_SETTLEMENT_OPERATION_PK  											");
			sbQuery.append(" 	AND SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK 												");
			sbQuery.append(" 	AND MO.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK																");
			sbQuery.append(" 	AND SAO.OPERATION_STATE = 625																					");
//			sbQuery.append(" 	AND OS.OPERATION_STATE != 1288																					");
			sbQuery.append(" 	AND SO.IND_DEMATERIALIZATION = 1																				"); //ACCIONES BANCARIAS
//			sbQuery.append(" 	AND (SE.SECURITY_CLASS <> 406 OR 																				");
//			sbQuery.append(" 		(SE.SECURITY_CLASS = 406 AND SAO.ROLE=1 AND NM.ID_NEGOTIATION_MODALITY_PK = 13))							");
			sbQuery.append(" ))CUENTAS_PENDIENTES																								");// CUENTAS PENDIENTES
			sbQuery.append(" FROM SETTLEMENT_OPERATION SO, MECHANISM_OPERATION MO, 																");
			sbQuery.append(" NEGOTIATION_MODALITY NM, SETTLEMENT_PROCESS SP, OPERATION_SETTLEMENT OS											");
			sbQuery.append(" WHERE SO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK 													");
			sbQuery.append(" AND MO.ID_NEGOTIATION_MODALITY_FK = NM.ID_NEGOTIATION_MODALITY_PK 													");
			sbQuery.append(" AND SP.ID_SETTLEMENT_PROCESS_PK = OS.ID_SETTLMENT_PROCESS_FK														");
			sbQuery.append(" AND OS.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK 													");
			sbQuery.append(" AND so.SETTLEMENT_DATE BETWEEN TO_DATE('"+gfto.getInitialDate()+"','dd/MM/yyyy') AND TO_DATE('"+gfto.getFinalDate()+"','dd/MM/yyyy') ");
			sbQuery.append(" AND sp.ID_NEGOTIATION_MECHANISM_FK = "+gfto.getMechanism()+" ");
			sbQuery.append(" AND sp.settlement_schema = 2 ");
			
			if(Validations.validateIsNotNull(gfto.getCurrency())){
			sbQuery.append(" AND sp.CURRENCY = "+gfto.getCurrency()																				);
			}
			if(Validations.validateIsNotNull(gfto.getSettlementSchema())){		
				sbQuery.append(" AND so.SETTLEMENT_SCHEMA=" +gfto.getSettlementSchema()+"														");
			}
			if(Validations.validateIsNotNull(gfto.getLiquidationProcesses())){
				sbQuery.append(" AND sp.SCHEDULE_TYPE=" +gfto.getLiquidationProcesses()+"														");
			}
			sbQuery.append(" group by ");
			sbQuery.append(" SP.SCHEDULE_TYPE, SP.CURRENCY, NM.MODALITY_NAME 																	");
			sbQuery.append(" order by ");
			sbQuery.append(" SP.SCHEDULE_TYPE, SP.CURRENCY, NM.MODALITY_NAME 																	");

		return sbQuery.toString();
	}
	
	/**
	 * Gets the lst negotiation mechanism.
	 *
	 * @param indMcnProcess the ind mcn process
	 * @return the lst negotiation mechanism
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationMechanism> getLstNegotiationMechanism(Integer indMcnProcess) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("	SELECT nm FROM NegotiationMechanism nm");
		sbQuery.append("	WHERE nm.stateMechanism = :state");
		if(indMcnProcess!=null){
			sbQuery.append("	AND nm.indMcnProcess = :indMcn");
			parameters.put("indMcn", indMcnProcess); 
		}
		sbQuery.append("	ORDER BY nm.mechanismName ASC");
		parameters.put("state", NegotiationMechanismStateType.ACTIVE.getCode());
		return findListByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Gets the lst negotiation mechanism for id.
	 *
	 * @param idNegMcnProcess the id neg mcn process
	 * @return the lst negotiation mechanism for id
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationMechanism> getLstNegotiationMechanismForId(Long idNegMcnProcess) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("	SELECT nm FROM NegotiationMechanism nm");
		sbQuery.append("	WHERE 1 = 1");
		if(idNegMcnProcess!=null){
			sbQuery.append("	AND nm.idNegotiationMechanismPk = :idNegProcess");
			parameters.put("idNegProcess", idNegMcnProcess); 
		}
		sbQuery.append("	ORDER BY nm.mechanismName ASC");
		return findListByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Gets the query net positions in operations in bag report.
	 *
	 * @param gfto the gfto
	 * @return the query net positions in operations in bag report
	 */
	public String getQueryNetPositionsInOperationsInBagReport(GenericSettlementOperationTO gfto){
		StringBuilder sbQuery = new StringBuilder();

			sbQuery.append(" SELECT DISTINCT ");
			sbQuery.append(" SP.SCHEDULE_TYPE AS PROCESO, ");
			sbQuery.append(" (SELECT PA.MNEMONIC ||'-'|| PA.DESCRIPTION FROM PARTICIPANT PA WHERE PA.ID_PARTICIPANT_PK = HAO.ID_INCHARGE_STOCK_PARTICIPANT) PARTICIPANTE, ");
			sbQuery.append(" DECODE(HAO.ROLE,1,'COMPRA','VENTA') ROL, ");
			sbQuery.append(" MO.OPERATION_CURRENCY AS MONEDA_OPERACION, ");
			sbQuery.append(" (SELECT NM.MODALITY_NAME FROM NEGOTIATION_MODALITY NM WHERE NM.ID_NEGOTIATION_MODALITY_PK = MO.ID_NEGOTIATION_MODALITY_FK) MODALIDAD, ");
			sbQuery.append(" MO.BALLOT_NUMBER ||'-'|| MO.SEQUENTIAL PAPELETA_SECUENCIAL, ");
			sbQuery.append(" (SELECT PA.MNEMONIC FROM PARTICIPANT PA WHERE PA.ID_PARTICIPANT_PK = MO.ID_BUYER_PARTICIPANT_FK) PARTICIPANTE_COMPRADOR, ");
			sbQuery.append(" (SELECT PA.MNEMONIC FROM PARTICIPANT PA WHERE PA.ID_PARTICIPANT_PK = MO.ID_SELLER_PARTICIPANT_FK) PARTICIPANTE_VENDEDOR, ");
			sbQuery.append(" MO.ID_SECURITY_CODE_FK CLASE_CLAVE_VALOR, ");
			sbQuery.append(" SO.STOCK_QUANTITY CANTIDAD, ");
			sbQuery.append(" SO.SETTLEMENT_PRICE PRECIO_UNITARIO, ");
			sbQuery.append(" SO.AMOUNT_RATE TASA, ");
			sbQuery.append(" SO.SETTLEMENT_DAYS, ");
			sbQuery.append(" NVL(SO.SETTLEMENT_DAYS,0) PLAZO, ");
			sbQuery.append(" SO.SETTLEMENT_AMOUNT TOTAL, ");
			sbQuery.append(" DECODE(SO.IND_PARTIAL,1,'SI','NO') ANTICIPADA, ");
			sbQuery.append(" SO.OPERATION_STATE AS ESTADO_OPERACION ");
			sbQuery.append(" FROM OPERATION_SETTLEMENT OS, SETTLEMENT_PROCESS SP, HOLDER_ACCOUNT_OPERATION HAO, MECHANISM_OPERATION MO, SETTLEMENT_OPERATION SO ");
			sbQuery.append(" WHERE SP.ID_SETTLEMENT_PROCESS_PK = OS.ID_SETTLMENT_PROCESS_FK ");
			sbQuery.append(" AND OS.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK ");
			sbQuery.append(" AND SO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK ");
			sbQuery.append(" AND MO.ID_MECHANISM_OPERATION_PK = HAO.ID_MECHANISM_OPERATION_FK ");
			sbQuery.append(" AND SP.SETTLEMENT_SCHEMA=2 ");
			sbQuery.append(" AND HAO.HOLDER_ACCOUNT_STATE= 625  ");
			sbQuery.append(" AND SP.SETTLEMENT_DATE = TO_DATE('"+gfto.getInitialDate()+"','dd/MM/yyyy')");
			sbQuery.append(" AND SP.SCHEDULE_TYPE=" +gfto.getLiquidationProcesses()+" ");
			sbQuery.append(" AND HAO.ID_INCHARGE_STOCK_PARTICIPANT=" +gfto.getBuyerParticipant()+" ");
			sbQuery.append(" ORDER BY PAPELETA_SECUENCIAL ");
			sbQuery.append("  ");
	
		return sbQuery.toString();
	}

	/**
	 * Gets the query ex lack of securities operation funds of bag report.
	 *
	 * @param gfto the gfto
	 * @return the query ex lack of securities operation funds of bag report
	 */
	public String getQueryExLackOfSecuritiesOperationFundsOfBagReport(GenericSettlementOperationTO gfto){
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" SELECT ");
		sbQuery.append(" SP.SCHEDULE_TYPE PROCESO, ");
		sbQuery.append(" (SELECT PA.MNEMONIC || ' - ' || PA.ID_PARTICIPANT_PK || ' - ' || PA.DESCRIPTION FROM PARTICIPANT PA WHERE PA.ID_PARTICIPANT_PK=HAO.ID_INCHARGE_STOCK_PARTICIPANT) PARTICIPANTE, ");
		sbQuery.append(" OS.OPERATION_STATE TIPO_EXCLUSION, ");
		sbQuery.append(" DECODE(HAO.ROLE,1,'COMPRA','VENTA') ROL, ");
		sbQuery.append(" MO.OPERATION_CURRENCY MONEDA_OPERACION, ");
		sbQuery.append(" (SELECT NM.MODALITY_NAME FROM NEGOTIATION_MODALITY NM WHERE NM.ID_NEGOTIATION_MODALITY_PK = MO.ID_NEGOTIATION_MODALITY_FK) MODALIDAD, ");
		sbQuery.append(" MO.BALLOT_NUMBER ||'-'|| MO.SEQUENTIAL PAPELETA_SECUENCIAL, ");
		sbQuery.append(" (SELECT PA.MNEMONIC FROM PARTICIPANT PA WHERE PA.ID_PARTICIPANT_PK = MO.ID_BUYER_PARTICIPANT_FK) PARTICIPANTE_COMPRADOR, ");
		sbQuery.append(" (SELECT PA.MNEMONIC FROM PARTICIPANT PA WHERE PA.ID_PARTICIPANT_PK = MO.ID_SELLER_PARTICIPANT_FK) PARTICIPANTE_VENDEDOR, ");
		sbQuery.append(" MO.ID_SECURITY_CODE_FK CLASE_CLAVE_VALOR, ");
		sbQuery.append(" SUM(SAO.STOCK_QUANTITY) CANTIDAD, ");
		sbQuery.append(" SO.SETTLEMENT_PRICE PRECIO_UNITARIO, ");
		sbQuery.append(" SO.AMOUNT_RATE TASA, ");
		sbQuery.append(" SO.SETTLEMENT_DAYS, ");
		sbQuery.append(" NVL(SO.SETTLEMENT_DAYS,0) PLAZO, ");
		sbQuery.append(" SUM(SAO.SETTLEMENT_AMOUNT) TOTAL, ");
		sbQuery.append(" DECODE(SO.IND_PARTIAL,1,'SI','NO') ANTICIPADA, ");
		sbQuery.append(" SO.OPERATION_STATE ESTADO_OPERACION ");
		sbQuery.append(" FROM OPERATION_SETTLEMENT OS, SETTLEMENT_PROCESS SP, HOLDER_ACCOUNT_OPERATION HAO, MECHANISM_OPERATION MO, SETTLEMENT_OPERATION SO, SETTLEMENT_ACCOUNT_OPERATION SAO ");
		sbQuery.append(" WHERE SP.ID_SETTLEMENT_PROCESS_PK = OS.ID_SETTLMENT_PROCESS_FK ");
		sbQuery.append(" AND OS.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK ");
		sbQuery.append(" AND SO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK ");
		sbQuery.append(" AND MO.ID_MECHANISM_OPERATION_PK = HAO.ID_MECHANISM_OPERATION_FK ");
		sbQuery.append(" AND HAO.ID_HOLDER_ACCOUNT_OPERATION_PK = SAO.ID_HOLDER_ACCOUNT_OPERATION_FK ");
		sbQuery.append(" AND SP.SETTLEMENT_SCHEMA=2 ");
		sbQuery.append(" AND OS.OPERATION_STATE IN (1286,1287,1638)");
		sbQuery.append(" AND HAO.HOLDER_ACCOUNT_STATE= 625");
		sbQuery.append(" AND SP.SETTLEMENT_DATE = TO_DATE('"+gfto.getInitialDate()+"','dd/MM/yyyy')");
		sbQuery.append(" AND SP.SCHEDULE_TYPE=" +gfto.getLiquidationProcesses()+" ");
		sbQuery.append(" AND HAO.ID_INCHARGE_STOCK_PARTICIPANT=" +gfto.getBuyerParticipant()+" ");
		sbQuery.append(" GROUP BY "); 
		sbQuery.append(" SP.SCHEDULE_TYPE, ");
		sbQuery.append(" HAO.ID_INCHARGE_STOCK_PARTICIPANT, ");
		sbQuery.append(" OS.OPERATION_STATE, ");
		sbQuery.append(" HAO.ROLE, ");
		sbQuery.append(" MO.OPERATION_CURRENCY, ");
		sbQuery.append(" MO.ID_NEGOTIATION_MODALITY_FK, ");
		sbQuery.append(" MO.BALLOT_NUMBER ||'-'|| MO.SEQUENTIAL, ");
		sbQuery.append(" MO.ID_BUYER_PARTICIPANT_FK, ");
		sbQuery.append(" MO.ID_SELLER_PARTICIPANT_FK, ");
		sbQuery.append(" MO.ID_SECURITY_CODE_FK, ");
		sbQuery.append(" SO.SETTLEMENT_PRICE, ");
		sbQuery.append(" SO.AMOUNT_RATE, ");
		sbQuery.append(" SO.SETTLEMENT_DAYS, ");
		sbQuery.append(" SO.SETTLEMENT_DAYS, ");
		sbQuery.append(" SO.IND_PARTIAL, ");
		sbQuery.append(" SO.OPERATION_STATE ");		
		sbQuery.append(" ORDER BY PAPELETA_SECUENCIAL ");
		sbQuery.append("  ");

	return sbQuery.toString();
}
	
	/**
	 * Gets the cuadre logbook.
	 *
	 * @param cuadreLogbook the cuadre logbook
	 * @return the cuadre logbook
	 */
	/*it only returns the query to send to the jasper as PARAMETER*/
    public String getCuadreLogbook(GenericSettlementOperationTO cuadreLogbook){
    	StringBuilder sbQuery= new StringBuilder();
    	
    	sbQuery.append(" select");
		sbQuery.append(" 'Compra/venta' OPERACION,");//0
		sbQuery.append(" NVL(sum(mo.CASH_SETTLEMENT_AMOUNT),0) MONTO_TOTAL,");//1
		sbQuery.append(" count(1) CANTIDAD_TOTAL,");//2
		sbQuery.append(" NVL(sum(decode(so.IND_EXTENDED,1,mo.CASH_SETTLEMENT_AMOUNT,0)),0) LIQ_DIFERIDA_MONTO,");//3
		sbQuery.append(" NVL(sum(decode(so.IND_EXTENDED,1,1,0)),0) LIQ_DIFERIDA_CANT");//4
//		if(cuadreLogbook.getSettlementSchema().equals(SettlementSchemaType.NET.getCode().toString())){
			sbQuery.append(", NVL(sum(decode(sp.SCHEDULE_TYPE,1939,decode(mo.OPERATION_STATE,615,mo.CASH_SETTLEMENT_AMOUNT,0),0)),0) MONTO_1_ETAPA_DOLARES,");//5
			sbQuery.append(" NVL(sum(decode(sp.SCHEDULE_TYPE,1939,decode(mo.OPERATION_STATE,615,1,0),0)),0) N_OPE_1_ETAPA_DOLARES,");//6
			sbQuery.append(" NVL(sum(decode(sp.SCHEDULE_TYPE,1940,decode(mo.OPERATION_STATE,615,mo.CASH_SETTLEMENT_AMOUNT,0),0)),0) MONTO_2_ETAPA_DOLARES,");//7
			sbQuery.append(" NVL(sum(decode(sp.SCHEDULE_TYPE,1940,decode(mo.OPERATION_STATE,615,1,0),0)),0) N_OPE_2_ETAPA_DOLARES,");//8
			sbQuery.append(" NVL(sum(decode(sp.SCHEDULE_TYPE,1941,decode(mo.OPERATION_STATE,615,mo.CASH_SETTLEMENT_AMOUNT,0),0)),0) MONTO_MELOR_DOLARES,");//9
			sbQuery.append(" NVL(sum(decode(sp.SCHEDULE_TYPE,1941,decode(mo.OPERATION_STATE,615,1,0),0)),0) N_OPE_MELOR_DOLARES");//10
//			if(cuadreLogbook.getDate().equals(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()))){
				sbQuery.append(", NVL(sum(decode(sp.SCHEDULE_TYPE,1942,decode(mo.OPERATION_STATE,615,mo.CASH_SETTLEMENT_AMOUNT,0),0)),0) MONTO_MELID_DOLARES,");//11
				sbQuery.append(" NVL(sum(decode(sp.SCHEDULE_TYPE,1942,decode(mo.OPERATION_STATE,615,1,0),0)),0) N_OPE_MELID_DOLARES,");//12
				sbQuery.append(" NVL(sum(decode(so.IND_UNFULFILLED,1,mo.CASH_SETTLEMENT_AMOUNT,0)),0) LIQ_INCUMPLIDA_MONTO,");//13
				sbQuery.append(" NVL(sum(decode(so.IND_UNFULFILLED,1,1,0)),0) CANT_INCUMPLIDA");//14
//			}	
//		}
		sbQuery.append(" from");
		sbQuery.append(" SETTLEMENT_OPERATION so, SETTLEMENT_PROCESS sp,OPERATION_SETTLEMENT os,MECHANISM_OPERATION mo,NEGOTIATION_MODALITY nmo");
		sbQuery.append(" where");
		sbQuery.append(" os.ID_SETTLEMENT_OPERATION_FK = so.ID_SETTLEMENT_OPERATION_PK");
		sbQuery.append(" and os.ID_SETTLMENT_PROCESS_FK = sp.ID_SETTLEMENT_PROCESS_PK");
		sbQuery.append(" and mo.ID_NEGOTIATION_MODALITY_FK = nmo.ID_NEGOTIATION_MODALITY_PK");
		sbQuery.append(" and so.ID_MECHANISM_OPERATION_FK = mo.ID_MECHANISM_OPERATION_PK");
		sbQuery.append(" and mo.OPERATION_CURRENCY = :idCurrency");
		sbQuery.append(" and nmo.MODALITY_CODE = :modalityCodeCV");
		sbQuery.append(" and so.OPERATION_PART = :idOperation");
		sbQuery.append(" and so.SETTLEMENT_SCHEMA = :idSchema");
		sbQuery.append(" and (trunc(mo.CASH_SETTLEMENT_DATE) = TO_DATE(:date,'dd/MM/yyyy') and mo.OPERATION_STATE = :mechanismOpeState)");
		if(cuadreLogbook.getBuyerParticipant()!=null){
			sbQuery.append(" and mo.ID_BUYER_PARTICIPANT_FK= :idParticipant");
		}
		sbQuery.append(" UNION ALL");//Reporto
		sbQuery.append(" select");
		sbQuery.append(" 'Reporto' OPERACION,");//0
		sbQuery.append(" NVL(sum(mo.CASH_SETTLEMENT_AMOUNT),0) MONTO_TOTAL,");//1
		sbQuery.append(" count(1) CANTIDAD_TOTAL,");//2
		sbQuery.append(" NVL(sum(decode(so.IND_EXTENDED,1,decode(mo.OPERATION_STATE,615,mo.CASH_SETTLEMENT_AMOUNT,mo.TERM_SETTLEMENT_AMOUNT),0)),0) LIQ_DIFERIDA_MONTO,");//3
		sbQuery.append(" NVL(sum(decode(so.IND_EXTENDED,1,1,0)),0) LIQ_DIFERIDA_CANT");//4
//		if(cuadreLogbook.getSettlementSchema().equals(SettlementSchemaType.NET.getCode().toString())){
			sbQuery.append(", NVL(sum(decode(sp.SCHEDULE_TYPE,1939,decode(mo.OPERATION_STATE,615,mo.CASH_SETTLEMENT_AMOUNT,0),0)),0) MONTO_1_ETAPA_DOLARES,");//5
			sbQuery.append(" NVL(sum(decode(sp.SCHEDULE_TYPE,1939,decode(mo.OPERATION_STATE,615,1,0),0)),0) N_OPE_1_ETAPA_DOLARES,");//6
			sbQuery.append(" NVL(sum(decode(sp.SCHEDULE_TYPE,1940,decode(mo.OPERATION_STATE,615,mo.CASH_SETTLEMENT_AMOUNT,0),0)),0) MONTO_2_ETAPA_DOLARES,");//7
			sbQuery.append(" NVL(sum(decode(sp.SCHEDULE_TYPE,1940,decode(mo.OPERATION_STATE,615,1,0),0)),0) N_OPE_2_ETAPA_DOLARES,");//8
			sbQuery.append(" NVL(sum(decode(sp.SCHEDULE_TYPE,1941,decode(mo.OPERATION_STATE,615,decode(mo.OPERATION_STATE,615,mo.CASH_SETTLEMENT_AMOUNT,mo.TERM_SETTLEMENT_AMOUNT),0),0)),0) MONTO_MELOR_DOLARES,");//9
			sbQuery.append(" NVL(sum(decode(sp.SCHEDULE_TYPE,1941,decode(mo.OPERATION_STATE,615,1,0),0)),0) N_OPE_MELOR_DOLARES");//10
//			if(cuadreLogbook.getDate().equals(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()))){
				sbQuery.append(", NVL(sum(decode(sp.SCHEDULE_TYPE,1942,decode(mo.OPERATION_STATE,615,decode(mo.OPERATION_STATE,615,mo.CASH_SETTLEMENT_AMOUNT,mo.TERM_SETTLEMENT_AMOUNT),0),0)),0) MONTO_MELID_DOLARES,");//1
				sbQuery.append(" NVL(sum(decode(sp.SCHEDULE_TYPE,1942,decode(mo.OPERATION_STATE,615,1,0),0)),0) N_OPE_MELID_DOLARES,");//12
				sbQuery.append(" NVL(sum(decode(so.IND_UNFULFILLED,1,decode(mo.OPERATION_STATE,615,mo.CASH_SETTLEMENT_AMOUNT,mo.TERM_SETTLEMENT_AMOUNT),0)),0) LIQ_INCUMPLIDA_MONTO,");//13
				sbQuery.append(" NVL(sum(decode(so.IND_UNFULFILLED,1,1,0)),0) CANT_INCUMPLIDA");//14
//			}
//		}
		sbQuery.append(" from");
		sbQuery.append(" SETTLEMENT_OPERATION so, SETTLEMENT_PROCESS sp,OPERATION_SETTLEMENT os,MECHANISM_OPERATION mo,NEGOTIATION_MODALITY nmo");
		sbQuery.append(" where");
		sbQuery.append(" os.ID_SETTLEMENT_OPERATION_FK = so.ID_SETTLEMENT_OPERATION_PK");
		sbQuery.append(" and os.ID_SETTLMENT_PROCESS_FK = sp.ID_SETTLEMENT_PROCESS_PK");
		sbQuery.append(" and mo.ID_NEGOTIATION_MODALITY_FK = nmo.ID_NEGOTIATION_MODALITY_PK");
		sbQuery.append(" and so.ID_MECHANISM_OPERATION_FK = mo.ID_MECHANISM_OPERATION_PK");
		sbQuery.append(" and mo.OPERATION_CURRENCY = :idCurrency");
		sbQuery.append(" and nmo.MODALITY_CODE = :modalityCodeRep");
		sbQuery.append(" and so.OPERATION_PART = :idOperation");
		sbQuery.append(" and so.SETTLEMENT_SCHEMA = :idSchema");
		sbQuery.append(" and ((trunc(mo.CASH_SETTLEMENT_DATE) = TO_DATE(:date,'dd/MM/yyyy') and mo.OPERATION_STATE = :mechanismOpeState)");
		sbQuery.append(" 	 OR (trunc(mo.TERM_SETTLEMENT_DATE) = TO_DATE(:date,'dd/MM/yyyy') and mo.OPERATION_STATE = :mechanismOpeState2))");
		if(cuadreLogbook.getBuyerParticipant()!=null){
			sbQuery.append(" and mo.ID_BUYER_PARTICIPANT_FK= :idParticipant");
		}
		sbQuery.append(" UNION ALL");//Colocacion primaria
		sbQuery.append(" select");
		sbQuery.append(" 'Colocacion primaria' OPERACION,");//0
		sbQuery.append(" NVL(sum(mo.CASH_SETTLEMENT_AMOUNT),0) MONTO_TOTAL,");//1
		sbQuery.append(" count(1) CANTIDAD_TOTAL,");//2
		sbQuery.append(" NVL(sum(decode(so.IND_EXTENDED,1,mo.CASH_SETTLEMENT_AMOUNT,0)),0) LIQ_DIFERIDA_MONTO,");//3
		sbQuery.append(" NVL(sum(decode(so.IND_EXTENDED,1,1,0)),0) LIQ_DIFERIDA_CANT");//4
//		if(cuadreLogbook.getSettlementSchema().equals(SettlementSchemaType.NET.getCode().toString())){
			sbQuery.append(", NVL(sum(decode(sp.SCHEDULE_TYPE,1939,mo.CASH_SETTLEMENT_AMOUNT,0)),0) MONTO_1_ETAPA_DOLARES,");//5
			sbQuery.append(" NVL(sum(decode(sp.SCHEDULE_TYPE,1939,decode(mo.OPERATION_STATE,615,1,0),0)),0) N_OPE_1_ETAPA_DOLARES,");//6
			sbQuery.append(" NVL(sum(decode(sp.SCHEDULE_TYPE,1940,mo.CASH_SETTLEMENT_AMOUNT,0)),0) MONTO_2_ETAPA_DOLARES,");//7
			sbQuery.append(" NVL(sum(decode(sp.SCHEDULE_TYPE,1940,decode(mo.OPERATION_STATE,615,1,0),0)),0) N_OPE_2_ETAPA_DOLARES,");//8
			sbQuery.append(" NVL(sum(decode(sp.SCHEDULE_TYPE,1941,decode(mo.OPERATION_STATE,615,mo.CASH_SETTLEMENT_AMOUNT,0),0)),0) MONTO_MELOR_DOLARES,");//9
			sbQuery.append(" NVL(sum(decode(sp.SCHEDULE_TYPE,1941,decode(mo.OPERATION_STATE,615,1,0),0)),0) N_OPE_MELOR_DOLARES");//10
//			if(cuadreLogbook.getDate().equals(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()))){
				sbQuery.append(", NVL(sum(decode(sp.SCHEDULE_TYPE,1942,decode(mo.OPERATION_STATE,615,mo.CASH_SETTLEMENT_AMOUNT,0),0)),0) MONTO_MELID_DOLARES,");//11
				sbQuery.append(" NVL(sum(decode(sp.SCHEDULE_TYPE,1942,decode(mo.OPERATION_STATE,615,1,0),0)),0) N_OPE_MELID_DOLARES,");//12
				sbQuery.append(" NVL(sum(decode(so.IND_UNFULFILLED,1,mo.CASH_SETTLEMENT_AMOUNT,0)),0) LIQ_INCUMPLIDA_MONTO,");//13
				sbQuery.append(" NVL(sum(decode(so.IND_UNFULFILLED,1,1,0)),0) CANT_INCUMPLIDA");//14
//			}
//		}
		sbQuery.append(" from");
		sbQuery.append(" SETTLEMENT_OPERATION so, SETTLEMENT_PROCESS sp,OPERATION_SETTLEMENT os,MECHANISM_OPERATION mo,NEGOTIATION_MODALITY nmo");
		sbQuery.append(" where");
		sbQuery.append(" os.ID_SETTLEMENT_OPERATION_FK = so.ID_SETTLEMENT_OPERATION_PK");
		sbQuery.append(" and os.ID_SETTLMENT_PROCESS_FK = sp.ID_SETTLEMENT_PROCESS_PK");
		sbQuery.append(" and mo.ID_NEGOTIATION_MODALITY_FK = nmo.ID_NEGOTIATION_MODALITY_PK");
		sbQuery.append(" and so.ID_MECHANISM_OPERATION_FK = mo.ID_MECHANISM_OPERATION_PK");
		sbQuery.append(" and mo.OPERATION_CURRENCY = :idCurrency");
		sbQuery.append(" and nmo.MODALITY_CODE = :modalityCodeCP");
		sbQuery.append(" and so.OPERATION_PART = :idOperation");
		sbQuery.append(" and so.SETTLEMENT_SCHEMA = :idSchema");
		sbQuery.append(" and ((trunc(mo.CASH_SETTLEMENT_DATE) = TO_DATE(:date,'dd/MM/yyyy') and mo.OPERATION_STATE = :mechanismOpeState)");
		sbQuery.append(" 	 OR (trunc(mo.TERM_SETTLEMENT_DATE) = TO_DATE(:date,'dd/MM/yyyy') and mo.OPERATION_STATE = :mechanismOpeState2))");
		if(cuadreLogbook.getBuyerParticipant()!=null){
			sbQuery.append(" and mo.ID_BUYER_PARTICIPANT_FK= :idParticipant");
		}
		
//		sbQuery.append(" order by s.ID_SECURITY_CODE_PK, h.id_participant_fk");
		
    	return sbQuery.toString();
    }
    
    /**
     * Gets the cuadre logbook for jasper.
     *
     * @param cuadreLogbook the cuadre logbook
     * @param strQuery the str query
     * @return the cuadre logbook for jasper
     */
    public String getCuadreLogbookForJasper(GenericSettlementOperationTO cuadreLogbook, String strQuery){
    	String strQueryFormatted = strQuery;

		strQueryFormatted= strQueryFormatted.replace(":idCurrency", cuadreLogbook.getCurrency());
		strQueryFormatted= strQueryFormatted.replace(":modalityCodeCV", "'"+COMPRA_VENTA+"'");
		strQueryFormatted= strQueryFormatted.replace(":modalityCodeRep", "'"+REPORTO+"'");
		strQueryFormatted= strQueryFormatted.replace(":modalityCodeCP", "'"+COLOCACION_PRIMARIA+"'");
		strQueryFormatted= strQueryFormatted.replace(":idOperation", BooleanType.YES.getCode().toString());
		strQueryFormatted= strQueryFormatted.replace(":idSchema", cuadreLogbook.getSettlementSchema());
		strQueryFormatted= strQueryFormatted.replace(":date", "'"+cuadreLogbook.getDate()+"'");
    	strQueryFormatted= strQueryFormatted.replace(":mechanismOpeState", MechanismOperationStateType.CASH_SETTLED.getCode().toString());
    	strQueryFormatted= strQueryFormatted.replace(":mechanismOpeState2", MechanismOperationStateType.TERM_SETTLED.getCode().toString());
		if(cuadreLogbook.getBuyerParticipant()!=null){
    		strQueryFormatted= strQueryFormatted.replace(":idParticipant", cuadreLogbook.getBuyerParticipant());
		}
		
		return strQueryFormatted;	
    }
    
    /**
     * Lst cuadre logbook by currency.
     *
     * @param cuadreLogbook the cuadre logbook
     * @param strQuery the str query
     * @return the list
     */
    public List<Object[]> lstCuadreLogbookByCurrency(GenericSettlementOperationTO cuadreLogbook, String strQuery){
    	Query query = em.createNativeQuery(strQuery);

    	query.setParameter("idCurrency", cuadreLogbook.getCurrency());
    	query.setParameter("modalityCodeCV", COMPRA_VENTA);
    	query.setParameter("modalityCodeRep", REPORTO);
    	query.setParameter("modalityCodeCP", COLOCACION_PRIMARIA);
    	query.setParameter("idOperation", BooleanType.YES.getCode());
    	query.setParameter("idSchema", cuadreLogbook.getSettlementSchema());
    	query.setParameter("date", cuadreLogbook.getDate());
    	query.setParameter("mechanismOpeState", MechanismOperationStateType.CASH_SETTLED.getCode().toString());
    	query.setParameter("mechanismOpeState2", MechanismOperationStateType.TERM_SETTLED.getCode().toString());
    	if(cuadreLogbook.getBuyerParticipant()!=null){
    		query.setParameter("idParticipant", cuadreLogbook.getBuyerParticipant());
    	}
    	
		return query.getResultList();
    }
    
    /**
     * Gets the ballot part penalty.
     *
     * @param idParticipantPenalty the id participant penalty
     * @return the ballot part penalty
     */
    public List<Object[]> getBallotPartPenalty(String idParticipantPenalty){
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append("  SELECT																					");//
		sbQuery.append("  PP.ID_PARTICIPANT_PENALTY_PK,																");//0
		sbQuery.append("  PP.SETTLEMENT_DATE,																		");//1 Settlement Date
		sbQuery.append("  PP.PENALTY_AMOUNT,																		");//2 Penalty Amount
		sbQuery.append("  PP.PENALTY_MOTIVE,																		");//3 Penalty Motive
		sbQuery.append("  PP.PENALTY_MOTIVE_OTHER,																	");//4 Penalty Other Motive
		sbQuery.append("  PP.PENALTY_LEVEL,																			");//5 Penalty Level
		sbQuery.append("  PP.ID_PARTICIPANT_FK,																		");//6 Participant
		sbQuery.append("  DECODE (       PP.PENALTY_MOTIVE,1834,PP.PENALTY_MOTIVE_OTHER,  							");//7 Ballot/Sequential
		sbQuery.append("  (SELECT LISTAGG(MO.BALLOT_NUMBER || '/' || MO.SEQUENTIAL ,', ') 							");
		sbQuery.append("  WITHIN GROUP (ORDER BY MO.ID_MECHANISM_OPERATION_PK)										");
		sbQuery.append("  FROM SETTLEMENT_OPERATION SO, MECHANISM_OPERATION MO,PARTICIPANT_PENALTY_OPERATION PPO	");
		sbQuery.append("  WHERE PPO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK						");
		sbQuery.append("  AND PPO.ID_PARTICIPANT_PENALTY_FK=PP.ID_PARTICIPANT_PENALTY_PK							");
		sbQuery.append("  AND SO.ID_MECHANISM_OPERATION_FK=MO.ID_MECHANISM_OPERATION_PK)) AS BALLOT,				");
		sbQuery.append("  PP.RESOLUTION_NUMBER||'/'||TO_CHAR(PP.SETTLEMENT_DATE,'YYYY'),							");//8 Resolution Number
		sbQuery.append("  TO_CHAR(PP.SETTLEMENT_DATE,'DAY DD  MONTH YYYY','NLS_DATE_LANGUAGE = SPANISH'), 			");//9 Date time description Settlement
		sbQuery.append("  TO_CHAR(PP.PENALTY_EMITION_DATE,'DAY DD  MONTH YYYY','NLS_DATE_LANGUAGE = SPANISH') 		");//10 Date time description Emision
		sbQuery.append("  FROM PARTICIPANT_PENALTY PP																");
		sbQuery.append("  WHERE PP.ID_PARTICIPANT_PENALTY_PK=:idParticipantPenalty									");
		sbQuery.append("  AND PP.PENALTY_STATE=:penaltyState														");
		
		sbQuery.append("");
		sbQuery.append("");
		
		Query query = em.createNativeQuery(sbQuery.toString());

		query.setParameter("idParticipantPenalty", new Long( idParticipantPenalty));
		query.setParameter("penaltyState", SanctionStateType.CONFIRMED.getCode());
		
		return query.getResultList();
    }

    /**
     * 		//Numero de resolucion , motivo, fecha.
     *
     * @param idParticipantPk the id participant pk
     * @param idParticipantPenalty the id participant penalty
     * @return the history sanctions by participant
     */
    public List<Object[]> getHistorySanctionsByParticipant(Long idParticipantPk,String idParticipantPenalty){
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append("  SELECT																					");//
		sbQuery.append("  PP.RESOLUTION_NUMBER||'/'||TO_CHAR(PP.SETTLEMENT_DATE,'YYYY'),							");//0 Resolution Number
		sbQuery.append("  TO_CHAR(PP.SETTLEMENT_DATE,'DAY DD  MONTH YYYY','NLS_DATE_LANGUAGE = SPANISH'), 			");//1 Date time description
		sbQuery.append("  (SELECT P.DESCRIPTION FROM PARAMETER_TABLE P WHERE PP.PENALTY_MOTIVE=P.PARAMETER_TABLE_PK)");//2 Motive Description

		sbQuery.append("  FROM PARTICIPANT_PENALTY PP																");
		sbQuery.append("  WHERE PP.ID_PARTICIPANT_PENALTY_PK != :idParticipantPenalty								");
		sbQuery.append("  AND PP.PENALTY_STATE=:penaltyState														");
		sbQuery.append("  AND PP.ID_PARTICIPANT_FK=:idParticipantPk													");
		
		sbQuery.append("");
		sbQuery.append("");
		
		Query query = em.createNativeQuery(sbQuery.toString());

		query.setParameter("idParticipantPenalty", new Long( idParticipantPenalty));
		query.setParameter("penaltyState", SanctionStateType.CONFIRMED.getCode());
		query.setParameter("idParticipantPk", idParticipantPk);
		
		return query.getResultList();
    }

    /**
     * Query del reporte Operaciones Liquidadas
     * @param settledOperationsTO
     * @return
     */
    
    public String getQuerySettledOperations(SettledOperationsTO settledOperationsTO){
    	StringBuilder sbQuery= new StringBuilder();
    	
    	sbQuery.append(" 	SELECT DISTINCT	 ");
    	sbQuery.append(" 	    SO.SETTLEMENT_DATE,	 ");
    	sbQuery.append(" 	    NMO.MODALITY_CODE,	 ");
    	sbQuery.append(" 	    MO.BALLOT_NUMBER,	 ");
    	sbQuery.append(" 	    MO.SEQUENTIAL,	 ");
    	sbQuery.append(" 	    DECODE(SO.OPERATION_PART , 1 , 'CONTADO' , 2, 'PLAZO' ) AS OP_PART,	 ");
    	sbQuery.append(" 	    MO.ID_BUYER_PARTICIPANT_FK BUYER_PARTICIPANT,	 ");
    	
    	if(settledOperationsTO.getParticipantRegisterReport()!=null){
    		sbQuery.append(" 	    CASE WHEN :participantRegisterReport = MO.ID_BUYER_PARTICIPANT_FK THEN (	 ");
    		sbQuery.append(" 	    CONCAT((SELECT LISTAGG(PURCHASE_HA.ACCOUNT_NUMBER, ', ') WITHIN GROUP (ORDER BY PURCHASE_HA.ALTERNATE_CODE)	 ");
        	sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION PURCHASE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT PURCHASE_HA ON PURCHASE_HA.ID_HOLDER_ACCOUNT_PK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION PURCHASE_SAO ON PURCHASE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        WHERE PURCHASE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ROLE = 1	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	        ),'')	 ");
        	sbQuery.append(" 	        ) ELSE NULL END    ");
        	sbQuery.append(" 	    BUYER_PARTICIPANT_ACCOUNT,	 ");
        	
    		sbQuery.append(" 	    CASE WHEN :participantRegisterReport = MO.ID_BUYER_PARTICIPANT_FK THEN (	 ");
        	sbQuery.append(" 	    CONCAT((SELECT LISTAGG(H.ID_HOLDER_PK||','||H.FULL_NAME, ', ') WITHIN GROUP (ORDER BY H.ID_HOLDER_PK)	 ");
        	sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION PURCHASE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT PURCHASE_HA ON PURCHASE_HA.ID_HOLDER_ACCOUNT_PK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = PURCHASE_HA.ID_HOLDER_ACCOUNT_PK		");
        	sbQuery.append(" 	        INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK	");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION PURCHASE_SAO ON PURCHASE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        WHERE PURCHASE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ROLE = 1	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	        ),'')	 ");
        	sbQuery.append(" 	        ) ELSE NULL END    ");
        	sbQuery.append(" 	    BUYER_CUI_NAME,	 ");
        	
        	sbQuery.append(" 	    CASE WHEN :participantRegisterReport = MO.ID_BUYER_PARTICIPANT_FK THEN (	 ");
        	sbQuery.append(" 	    CONCAT((SELECT LISTAGG(PT.INDICATOR1,', ') WITHIN GROUP (ORDER BY H.ID_HOLDER_PK)	 ");
        	sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION PURCHASE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT PURCHASE_HA ON PURCHASE_HA.ID_HOLDER_ACCOUNT_PK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = PURCHASE_HA.ID_HOLDER_ACCOUNT_PK		");
        	sbQuery.append(" 	        INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK	");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION PURCHASE_SAO ON PURCHASE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        INNER JOIN PARAMETER_TABLE PT ON PT.PARAMETER_TABLE_PK = H.ECONOMIC_ACTIVITY ");
        	sbQuery.append(" 	        WHERE PURCHASE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ROLE = 1	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	        ),'')	 ");
        	sbQuery.append(" 	        ) ELSE NULL END    ");
        	sbQuery.append(" 	    BUYER_ACT_ECONOMIC,	 ");
        	
    		sbQuery.append(" 	    CASE WHEN :participantRegisterReport = MO.ID_BUYER_PARTICIPANT_FK THEN (	 ");
        	sbQuery.append(" 	    CONCAT((SELECT LISTAGG((select PARAMETER_NAME from PARAMETER_TABLE where PARAMETER_TABLE_PK = H.HOLDER_TYPE ), ', ') WITHIN GROUP (ORDER BY H.HOLDER_TYPE)	 ");
        	sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION PURCHASE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT PURCHASE_HA ON PURCHASE_HA.ID_HOLDER_ACCOUNT_PK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = PURCHASE_HA.ID_HOLDER_ACCOUNT_PK		");
        	sbQuery.append(" 	        INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK	");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION PURCHASE_SAO ON PURCHASE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        WHERE PURCHASE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ROLE = 1	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	        ),'')	 ");
        	sbQuery.append(" 	        ) ELSE NULL END    ");
        	sbQuery.append(" 	    BUYER_CUI_TYPE,	 ");
    	}else{
    		sbQuery.append(" 	    CONCAT((SELECT LISTAGG(PURCHASE_HA.ACCOUNT_NUMBER, ', ') WITHIN GROUP (ORDER BY PURCHASE_HA.ALTERNATE_CODE)	 ");
        	sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION PURCHASE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT PURCHASE_HA ON PURCHASE_HA.ID_HOLDER_ACCOUNT_PK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION PURCHASE_SAO ON PURCHASE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        WHERE PURCHASE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ROLE = 1	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	        ),'')	 ");
        	sbQuery.append(" 	    BUYER_PARTICIPANT_ACCOUNT,	 ");
        	
        	sbQuery.append(" 	    CONCAT((SELECT LISTAGG(H.ID_HOLDER_PK||','||H.FULL_NAME, ', ') WITHIN GROUP (ORDER BY H.ID_HOLDER_PK)	 ");
        	sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION PURCHASE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT PURCHASE_HA ON PURCHASE_HA.ID_HOLDER_ACCOUNT_PK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = PURCHASE_HA.ID_HOLDER_ACCOUNT_PK		");
        	sbQuery.append(" 	        INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK	");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION PURCHASE_SAO ON PURCHASE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        WHERE PURCHASE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ROLE = 1	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	        ),'')	 ");
        	sbQuery.append(" 	    BUYER_CUI_NAME,	 ");
        	
        	sbQuery.append(" 	    CONCAT((SELECT LISTAGG(PT.INDICATOR1,', ') WITHIN GROUP (ORDER BY H.ID_HOLDER_PK)	 ");
        	sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION PURCHASE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT PURCHASE_HA ON PURCHASE_HA.ID_HOLDER_ACCOUNT_PK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = PURCHASE_HA.ID_HOLDER_ACCOUNT_PK		");
        	sbQuery.append(" 	        INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK	");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION PURCHASE_SAO ON PURCHASE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        INNER JOIN PARAMETER_TABLE PT ON PT.PARAMETER_TABLE_PK = H.ECONOMIC_ACTIVITY ");
        	sbQuery.append(" 	        WHERE PURCHASE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ROLE = 1	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	        ),'')	 ");
        	sbQuery.append(" 	    BUYER_ACT_ECONOMIC,	 ");
        	
        	sbQuery.append(" 	    CONCAT((SELECT LISTAGG((select PARAMETER_NAME from PARAMETER_TABLE where PARAMETER_TABLE_PK = H.HOLDER_TYPE), ', ') WITHIN GROUP (ORDER BY H.HOLDER_TYPE)	 ");
        	sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION PURCHASE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT PURCHASE_HA ON PURCHASE_HA.ID_HOLDER_ACCOUNT_PK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = PURCHASE_HA.ID_HOLDER_ACCOUNT_PK		");
        	sbQuery.append(" 	        INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK	");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION PURCHASE_SAO ON PURCHASE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        WHERE PURCHASE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ROLE = 1	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	        ),'')	 ");
        	sbQuery.append(" 	    BUYER_CUI_TYPE,	 ");
    	}
    	
    	sbQuery.append(" 	    MO.ID_SELLER_PARTICIPANT_FK SELLER_PARTICIPANT,	 ");
    	
    	if(settledOperationsTO.getParticipantRegisterReport()!=null){
    		sbQuery.append(" 	    CASE WHEN :participantRegisterReport = MO.ID_SELLER_PARTICIPANT_FK THEN (	 ");
    		sbQuery.append(" 	    CONCAT((SELECT LISTAGG(SALE_HA.ACCOUNT_NUMBER, ', ') WITHIN GROUP (ORDER BY SALE_HA.ALTERNATE_CODE)	 ");
        	sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION SALE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT SALE_HA ON SALE_HA.ID_HOLDER_ACCOUNT_PK = SALE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION SALE_SAO ON SALE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = SALE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        WHERE SALE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ROLE = 2	 ");
        	sbQuery.append(" 	        AND SALE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	    ),'')	 ");
        	sbQuery.append(" 	        ) ELSE NULL END    ");
        	sbQuery.append(" 	    SELLER_PARTICIPANT_ACCOUNT,	 ");
        	
    		sbQuery.append(" 	    CASE WHEN :participantRegisterReport = MO.ID_SELLER_PARTICIPANT_FK THEN (	 ");
            sbQuery.append(" 	    CONCAT((SELECT LISTAGG(H.ID_HOLDER_PK||','||H.FULL_NAME, ', ') WITHIN GROUP (ORDER BY H.ID_HOLDER_PK)	 ");
        	sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION SALE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT SALE_HA ON SALE_HA.ID_HOLDER_ACCOUNT_PK = SALE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
            sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = SALE_HA.ID_HOLDER_ACCOUNT_PK		");
            sbQuery.append(" 	        INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK	");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION SALE_SAO ON SALE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = SALE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        WHERE SALE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ROLE = 2	 ");
        	sbQuery.append(" 	        AND SALE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	    ),'')	 ");
        	sbQuery.append(" 	        ) ELSE NULL END    ");
        	sbQuery.append(" 	    SELLER_CUI_NAME,	 ");
        	
        	sbQuery.append(" 	    CASE WHEN :participantRegisterReport = MO.ID_SELLER_PARTICIPANT_FK THEN (	 ");
        	sbQuery.append(" 	    CONCAT((SELECT LISTAGG(PT.INDICATOR1,', ') WITHIN GROUP (ORDER BY H.ID_HOLDER_PK)	 ");
        	sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION SALE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT SALE_HA ON SALE_HA.ID_HOLDER_ACCOUNT_PK = SALE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
            sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = SALE_HA.ID_HOLDER_ACCOUNT_PK		");
            sbQuery.append(" 	        INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK	");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION SALE_SAO ON SALE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = SALE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        INNER JOIN PARAMETER_TABLE PT ON PT.PARAMETER_TABLE_PK = H.ECONOMIC_ACTIVITY ");
        	sbQuery.append(" 	        WHERE SALE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ROLE = 2	 ");
        	sbQuery.append(" 	        AND SALE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	    ),'')	 ");
        	sbQuery.append(" 	        ) ELSE NULL END    ");
        	sbQuery.append(" 	    SELLER_ACT_ECONOMIC,	 ");
            	
    		sbQuery.append(" 	    CASE WHEN :participantRegisterReport = MO.ID_SELLER_PARTICIPANT_FK THEN (	 ");
        	sbQuery.append(" 	    CONCAT((SELECT LISTAGG((select PARAMETER_NAME from PARAMETER_TABLE where PARAMETER_TABLE_PK = H.HOLDER_TYPE ), ', ') WITHIN GROUP (ORDER BY H.HOLDER_TYPE)	 ");
        	sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION SALE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT SALE_HA ON SALE_HA.ID_HOLDER_ACCOUNT_PK = SALE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = SALE_HA.ID_HOLDER_ACCOUNT_PK		");
        	sbQuery.append(" 	        INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK	");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION SALE_SAO ON SALE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = SALE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        WHERE SALE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ROLE = 2	 ");
        	sbQuery.append(" 	        AND SALE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	    ),'')	 ");
        	sbQuery.append(" 	        ) ELSE NULL END    ");
        	sbQuery.append(" 	    SELLER_CUI_TYPE,	 ");
    	}else{
    		sbQuery.append(" 	    CONCAT((SELECT LISTAGG(SALE_HA.ACCOUNT_NUMBER, ', ') WITHIN GROUP (ORDER BY SALE_HA.ALTERNATE_CODE)	 ");
    		sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION SALE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT SALE_HA ON SALE_HA.ID_HOLDER_ACCOUNT_PK = SALE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION SALE_SAO ON SALE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = SALE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        WHERE SALE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ROLE = 2	 ");
        	sbQuery.append(" 	        AND SALE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	    ),'')	 ");
        	sbQuery.append(" 	    SELLER_PARTICIPANT_ACCOUNT,	 ");
        	
            sbQuery.append(" 	    CONCAT((SELECT LISTAGG(H.ID_HOLDER_PK||','||H.FULL_NAME, ', ') WITHIN GROUP (ORDER BY H.ID_HOLDER_PK)	 ");
    		sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION SALE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT SALE_HA ON SALE_HA.ID_HOLDER_ACCOUNT_PK = SALE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = SALE_HA.ID_HOLDER_ACCOUNT_PK		");
        	sbQuery.append(" 	        INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK	");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION SALE_SAO ON SALE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = SALE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        WHERE SALE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ROLE = 2	 ");
        	sbQuery.append(" 	        AND SALE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	    ),'')	 ");
        	sbQuery.append(" 	    SELLER_CUI_NAME,	 ");
        	
        	sbQuery.append(" 	    CONCAT((SELECT LISTAGG(PT.INDICATOR1,', ') WITHIN GROUP (ORDER BY H.ID_HOLDER_PK)	 ");
    		sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION SALE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT SALE_HA ON SALE_HA.ID_HOLDER_ACCOUNT_PK = SALE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = SALE_HA.ID_HOLDER_ACCOUNT_PK		");
        	sbQuery.append(" 	        INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK	");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION SALE_SAO ON SALE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = SALE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        INNER JOIN PARAMETER_TABLE PT ON PT.PARAMETER_TABLE_PK = H.ECONOMIC_ACTIVITY ");
        	sbQuery.append(" 	        WHERE SALE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ROLE = 2	 ");
        	sbQuery.append(" 	        AND SALE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	    ),'')	 ");
        	sbQuery.append(" 	    SELLER_ACT_ECONOMIC,	 ");
        	
        	sbQuery.append(" 	    CONCAT((SELECT LISTAGG((select PARAMETER_NAME from PARAMETER_TABLE where PARAMETER_TABLE_PK = H.HOLDER_TYPE ), ', ') WITHIN GROUP (ORDER BY H.HOLDER_TYPE)	 ");
    		sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION SALE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT SALE_HA ON SALE_HA.ID_HOLDER_ACCOUNT_PK = SALE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = SALE_HA.ID_HOLDER_ACCOUNT_PK		");
        	sbQuery.append(" 	        INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK	");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION SALE_SAO ON SALE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = SALE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        WHERE SALE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ROLE = 2	 ");
        	sbQuery.append(" 	        AND SALE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	    ),'')	 ");
        	sbQuery.append(" 	    SELLER_CUI_TYPE,	 ");
    	}

    	sbQuery.append(" 	    MO.ID_SECURITY_CODE_FK,	 ");
    	sbQuery.append(" 	    (SELECT PT.TEXT1 FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK  = MO.OPERATION_CURRENCY) AS OPERATION_CURRENCY,	 ");
    	sbQuery.append(" 	    SO.STOCK_QUANTITY,	 ");
    	sbQuery.append(" 	    so.settlement_price,	 ");
    	//sbQuery.append(" 	    DECODE(SO.OPERATION_PART , 1 , MO.CASH_PRICE, 2, MO.TERM_PRICE) AS OPER_PRICE,	 ");
    	sbQuery.append(" 	    SO.AMOUNT_RATE,	 ");
    	sbQuery.append(" 	    SO.SETTLEMENT_DAYS,	 ");
    	sbQuery.append("  		SO.SETTLEMENT_AMOUNT, ");
    	//sbQuery.append(" 	    DECODE(SO.OPERATION_PART , 1 , MO.CASH_PRICE*SO.STOCK_QUANTITY, 2, MO.TERM_PRICE*SO.STOCK_QUANTITY) AS OPER_AMOUNT,	 ");
    	sbQuery.append(" 	    (CASE SO.SETTLEMENT_CURRENCY	 ");
    	sbQuery.append(" 	        WHEN 127 THEN	 ");
    	sbQuery.append(" 	            SO.SETTLEMENT_AMOUNT / NVL((SELECT DER.BUY_PRICE FROM DAILY_EXCHANGE_RATES DER WHERE DER.DATE_RATE = SO.SETTLEMENT_DATE	 ");
    	sbQuery.append(" 	            AND DER.ID_CURRENCY = 430 AND ID_SOURCE_INFORMATION = 212),1)	 ");
    	sbQuery.append(" 	        WHEN 430 THEN	 ");
    	sbQuery.append(" 	            SO.SETTLEMENT_AMOUNT	 ");
    	sbQuery.append(" 	        END	 ");
    	sbQuery.append(" 	    ) AS OPER_AMOUNT_USD,	 ");
    	sbQuery.append(" 	    DECODE(SO.SETTLEMENT_TYPE, 1, 'FOP', 2, 'DVP' ) SETTLEMENT_TYPE,	 ");
    	sbQuery.append(" 	    (SELECT PT.PARAMETER_NAME FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK  = SP.SCHEDULE_TYPE) AS INSTANCIA,	 ");
    	sbQuery.append(" 	    BUYER_PA.MNEMONIC MNEMONIC_BUYER_PART,	 ");
    	sbQuery.append(" 	    SELLER_PA.MNEMONIC MNEMONIC_SELLER_PART,	 ");
    	
    	//ISSUE 1295: A requerimeinto del usuario se determina si la operacion es o no SELAR
    	sbQuery.append("		decode((select count(*)                                                                                     ");
        sbQuery.append("		from SETTLEMENT_DATE_OPERATION sdo                                                                          ");
        sbQuery.append("		inner join SETTLEMENT_REQUEST sr on SR.ID_SETTLEMENT_REQUEST_PK = SDO.ID_SETTLEMENT_REQUEST_FK              ");
        sbQuery.append("		inner join SETTLEMENT_OPERATION so_sq on SO_SQ.ID_SETTLEMENT_OPERATION_PK = SDO.ID_SETTLEMENT_OPERATION_FK  ");
        sbQuery.append("		inner join MECHANISM_OPERATION mo_sq on MO_SQ.ID_MECHANISM_OPERATION_PK = SO_SQ.ID_MECHANISM_OPERATION_FK   ");
        sbQuery.append("		where 0=0                                                                                                   ");
        sbQuery.append("		AND MO_SQ.BALLOT_NUMBER = MO.BALLOT_NUMBER                                                                  ");
        sbQuery.append("		AND MO_SQ.SEQUENTIAL = MO.SEQUENTIAL                                                                        ");
        sbQuery.append("		and (trunc(sr.REGISTER_DATE) = TRUNC(SO.SETTLEMENT_DATE))                                                   ");
        // indicador de SELAR = 2019
        sbQuery.append("		and sr.REQUEST_TYPE=2019 ),                                                                                 ");
        sbQuery.append("		0,'NO','SI') AS SELAR,																						");
		sbQuery.append("		mo.AMOUNT_RATE as tasa_negociacion																			");
    	
    	sbQuery.append(" 	FROM	 ");
    	sbQuery.append(" 	    MECHANISM_OPERATION MO,  SETTLEMENT_OPERATION SO, PARTICIPANT BUYER_PA , PARTICIPANT SELLER_PA	 ");
    	sbQuery.append(" 	    ,MODALITY_GROUP MG , MODALITY_GROUP_DETAIL MGD , MECHANISM_MODALITY MM , NEGOTIATION_MODALITY  NMO	 ");
    	sbQuery.append(" 	    ,SETTLEMENT_PROCESS SP , OPERATION_SETTLEMENT OS, PARTICIPANT_SETTLEMENT PS, security secu	 ");
    	sbQuery.append(" 	WHERE	 ");
    	sbQuery.append(" 	    MO.ID_MECHANISM_OPERATION_PK = SO.ID_MECHANISM_OPERATION_FK	 ");
    	sbQuery.append(" 	    AND MG.ID_MODALITY_GROUP_PK = MGD.ID_MODALITY_GROUP_FK	 ");
    	sbQuery.append(" 	    AND MGD.ID_NEGOTIATION_MECHANISM_FK = MM.ID_NEGOTIATION_MECHANISM_PK	 ");
    	sbQuery.append(" 	    AND MGD.ID_NEGOTIATION_MODALITY_FK = MM.ID_NEGOTIATION_MODALITY_PK	 ");
    	sbQuery.append(" 	    AND MM.ID_NEGOTIATION_MECHANISM_PK = MO.ID_NEGOTIATION_MECHANISM_FK	 ");
    	sbQuery.append(" 	    AND MM.ID_NEGOTIATION_MODALITY_PK = MO.ID_NEGOTIATION_MODALITY_FK	 ");
    	sbQuery.append(" 	    AND MM.ID_NEGOTIATION_MODALITY_PK = NMO.ID_NEGOTIATION_MODALITY_PK	 ");
    	sbQuery.append(" 	    AND MG.SETTLEMENT_SCHEMA = SO.SETTLEMENT_SCHEMA	 ");
    	sbQuery.append(" 	    AND SP.ID_SETTLEMENT_PROCESS_PK = OS.ID_SETTLMENT_PROCESS_FK	 ");
    	sbQuery.append(" 	    AND OS.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
//    	sbQuery.append(" 	    AND SP.PROCESS_STATE = 1284  ");
    	sbQuery.append("        AND SO.OPERATION_STATE in(615,616) ");
    	sbQuery.append(" 	    AND SP.SETTLEMENT_DATE = SO.SETTLEMENT_DATE	 ");
    	sbQuery.append(" 	    AND SP.SETTLEMENT_SCHEMA = SO.SETTLEMENT_SCHEMA	 ");
    	sbQuery.append(" 	    AND SP.CURRENCY = SO.SETTLEMENT_CURRENCY	 ");
    	sbQuery.append(" 	    AND SP.ID_NEGOTIATION_MECHANISM_FK = MO.ID_NEGOTIATION_MECHANISM_FK	 ");
    	sbQuery.append(" 	    AND SP.ID_MODALITY_GROUP_FK = MG.ID_MODALITY_GROUP_PK	 ");
    	sbQuery.append("        AND SO.OPERATION_STATE in(615,616) ");
//    	sbQuery.append(" 	    AND ( (SO.OPERATION_PART = 1 AND  SO.OPERATION_STATE IN (615))	 ");
//    	sbQuery.append(" 	         OR (SO.OPERATION_PART = 2 AND  SO.OPERATION_STATE IN (616)))	 ");
    	sbQuery.append(" 	    AND MO.ID_BUYER_PARTICIPANT_FK = BUYER_PA.ID_PARTICIPANT_PK	 ");
    	sbQuery.append(" 	    AND MO.ID_SELLER_PARTICIPANT_FK = SELLER_PA.ID_PARTICIPANT_PK	 ");
    	sbQuery.append(" 	    AND OS.OPERATION_STATE=1288 	 ");
    	sbQuery.append("        AND MM.ID_NEGOTIATION_MECHANISM_PK=1 ");
    	sbQuery.append(" 	    and mo.ID_SECURITY_CODE_FK=secu.ID_SECURITY_CODE_PK	 ");
    	sbQuery.append(" 	    AND PS.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
    	sbQuery.append(" 	    AND (TRUNC(SO.SETTLEMENT_DATE) BETWEEN TO_DATE(':date_initial','DD/MM/YYYY') AND TO_DATE(':date_end','DD/MM/YYYY'))	 ");

		if(Validations.validateIsNotNull(settledOperationsTO.getParticipantPk())){
			sbQuery.append(" AND (PS.ID_PARTICIPANT_FK = :participantPk)	 ");	
		}
		if(Validations.validateIsNotNull(settledOperationsTO.getModality())){
			sbQuery.append(" AND (NMO.ID_NEGOTIATION_MODALITY_PK = :modality) ");
    	}
		if(Validations.validateIsNotNull(settledOperationsTO.getCurrency())){
			sbQuery.append(" AND (MO.OPERATION_CURRENCY = :currency)	 ");
    	}
		
		if(Validations.validateIsNotNull(settledOperationsTO.getSecurityCode())){
			sbQuery.append(" AND (MO.ID_SECURITY_CODE_FK = :security_code)	 ");
		}
		if(Validations.validateIsNotNull(settledOperationsTO.getSecurityClass())){
			sbQuery.append(" AND (SECU.SECURITY_CLASS = :security_class)	 ");
		}
		
		if(Validations.validateIsNotNull(settledOperationsTO.getSchema())){
			sbQuery.append(" AND (SO.SETTLEMENT_SCHEMA = :schema)	 ");
    	}
		if(Validations.validateIsNotNull(settledOperationsTO.getScheduleType())){
			sbQuery.append(" AND (SP.SCHEDULE_TYPE = :schedule_type)	 ");
    	}
		
		sbQuery.append("	AND SO.OPERATION_PART = 1	 ");
		
		sbQuery.append("	UNION	 ");
		
		sbQuery.append(" 	SELECT DISTINCT	 ");
    	sbQuery.append(" 	    SO.SETTLEMENT_DATE,	 ");
    	sbQuery.append(" 	    NMO.MODALITY_CODE,	 ");
    	sbQuery.append(" 	    MO.BALLOT_NUMBER,	 ");
    	sbQuery.append(" 	    MO.SEQUENTIAL,	 ");
    	sbQuery.append(" 	    DECODE(SO.OPERATION_PART , 1 , 'CONTADO' , 2, 'PLAZO' ) AS OP_PART,	 ");
    	//
//    	sbQuery.append(" 	    MO.ID_BUYER_PARTICIPANT_FK BUYER_PARTICIPANT,	 ");
    	sbQuery.append(" 	    MO.ID_SELLER_PARTICIPANT_FK BUYER_PARTICIPANT,	 ");
    	
    	if(settledOperationsTO.getParticipantRegisterReport()!=null){
    		sbQuery.append(" 	    CASE WHEN :participantRegisterReport = MO.ID_SELLER_PARTICIPANT_FK THEN (	 ");
    		sbQuery.append(" 	    CONCAT((SELECT LISTAGG(PURCHASE_HA.ACCOUNT_NUMBER, ', ') WITHIN GROUP (ORDER BY PURCHASE_HA.ALTERNATE_CODE)	 ");
        	sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION PURCHASE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT PURCHASE_HA ON PURCHASE_HA.ID_HOLDER_ACCOUNT_PK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION PURCHASE_SAO ON PURCHASE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        WHERE PURCHASE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ROLE = 1	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	        ),'')	 ");
        	sbQuery.append(" 	        ) ELSE NULL END    ");
        	sbQuery.append(" 	    BUYER_PARTICIPANT_ACCOUNT,	 ");
        	
    		sbQuery.append(" 	    CASE WHEN :participantRegisterReport = MO.ID_SELLER_PARTICIPANT_FK THEN (	 ");
        	sbQuery.append(" 	    CONCAT((SELECT LISTAGG(H.ID_HOLDER_PK||','||H.FULL_NAME, ', ') WITHIN GROUP (ORDER BY H.ID_HOLDER_PK)	 ");
        	sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION PURCHASE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT PURCHASE_HA ON PURCHASE_HA.ID_HOLDER_ACCOUNT_PK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = PURCHASE_HA.ID_HOLDER_ACCOUNT_PK		");
        	sbQuery.append(" 	        INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK	");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION PURCHASE_SAO ON PURCHASE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        WHERE PURCHASE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ROLE = 1	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	        ),'')	 ");
        	sbQuery.append(" 	        ) ELSE NULL END    ");
        	sbQuery.append(" 	    BUYER_CUI_NAME,	 ");
        	
        	sbQuery.append(" 	    CASE WHEN :participantRegisterReport = MO.ID_SELLER_PARTICIPANT_FK THEN (	 ");
        	sbQuery.append(" 	    CONCAT((SELECT LISTAGG(PT.INDICATOR1,', ') WITHIN GROUP (ORDER BY H.ID_HOLDER_PK)	 ");
        	sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION PURCHASE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT PURCHASE_HA ON PURCHASE_HA.ID_HOLDER_ACCOUNT_PK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = PURCHASE_HA.ID_HOLDER_ACCOUNT_PK		");
        	sbQuery.append(" 	        INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK	");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION PURCHASE_SAO ON PURCHASE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        INNER JOIN PARAMETER_TABLE PT ON PT.PARAMETER_TABLE_PK = H.ECONOMIC_ACTIVITY ");
        	sbQuery.append(" 	        WHERE PURCHASE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ROLE = 1	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	        ),'')	 ");
        	sbQuery.append(" 	        ) ELSE NULL END    ");
        	sbQuery.append(" 	    BUYER_ACT_ECONOMIC,	 ");
        	
    		sbQuery.append(" 	    CASE WHEN :participantRegisterReport = MO.ID_SELLER_PARTICIPANT_FK THEN (	 ");
        	sbQuery.append(" 	    CONCAT((SELECT LISTAGG((select PARAMETER_NAME from PARAMETER_TABLE where PARAMETER_TABLE_PK = H.HOLDER_TYPE ), ', ') WITHIN GROUP (ORDER BY H.HOLDER_TYPE)	 ");
        	sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION PURCHASE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT PURCHASE_HA ON PURCHASE_HA.ID_HOLDER_ACCOUNT_PK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = PURCHASE_HA.ID_HOLDER_ACCOUNT_PK		");
        	sbQuery.append(" 	        INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK	");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION PURCHASE_SAO ON PURCHASE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        WHERE PURCHASE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ROLE = 1	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	        ),'')	 ");
        	sbQuery.append(" 	        ) ELSE NULL END    ");
        	sbQuery.append(" 	    BUYER_CUI_TYPE,	 ");
    	}else{
    		sbQuery.append(" 	    CONCAT((SELECT LISTAGG(PURCHASE_HA.ACCOUNT_NUMBER, ', ') WITHIN GROUP (ORDER BY PURCHASE_HA.ALTERNATE_CODE)	 ");
        	sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION PURCHASE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT PURCHASE_HA ON PURCHASE_HA.ID_HOLDER_ACCOUNT_PK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION PURCHASE_SAO ON PURCHASE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        WHERE PURCHASE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ROLE = 1	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	        ),'')	 ");
        	sbQuery.append(" 	    BUYER_PARTICIPANT_ACCOUNT,	 ");
        	
        	sbQuery.append(" 	    CONCAT((SELECT LISTAGG(H.ID_HOLDER_PK||','||H.FULL_NAME, ', ') WITHIN GROUP (ORDER BY H.ID_HOLDER_PK)	 ");
        	sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION PURCHASE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT PURCHASE_HA ON PURCHASE_HA.ID_HOLDER_ACCOUNT_PK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = PURCHASE_HA.ID_HOLDER_ACCOUNT_PK		");
        	sbQuery.append(" 	        INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK	");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION PURCHASE_SAO ON PURCHASE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        WHERE PURCHASE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ROLE = 1	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	        ),'')	 ");
        	sbQuery.append(" 	    BUYER_CUI_NAME,	 ");
        	
        	sbQuery.append(" 	    CONCAT((SELECT LISTAGG(PT.INDICATOR1,', ') WITHIN GROUP (ORDER BY H.ID_HOLDER_PK)	 ");
        	sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION PURCHASE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT PURCHASE_HA ON PURCHASE_HA.ID_HOLDER_ACCOUNT_PK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = PURCHASE_HA.ID_HOLDER_ACCOUNT_PK		");
        	sbQuery.append(" 	        INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK	");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION PURCHASE_SAO ON PURCHASE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        INNER JOIN PARAMETER_TABLE PT ON PT.PARAMETER_TABLE_PK = H.ECONOMIC_ACTIVITY ");
        	sbQuery.append(" 	        WHERE PURCHASE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ROLE = 1	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	        ),'')	 ");
        	sbQuery.append(" 	    BUYER_ACT_ECONOMIC,	 ");
        	
        	sbQuery.append(" 	    CONCAT((SELECT LISTAGG((select PARAMETER_NAME from PARAMETER_TABLE where PARAMETER_TABLE_PK = H.HOLDER_TYPE), ', ') WITHIN GROUP (ORDER BY H.HOLDER_TYPE)	 ");
        	sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION PURCHASE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT PURCHASE_HA ON PURCHASE_HA.ID_HOLDER_ACCOUNT_PK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = PURCHASE_HA.ID_HOLDER_ACCOUNT_PK		");
        	sbQuery.append(" 	        INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK	");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION PURCHASE_SAO ON PURCHASE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = PURCHASE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        WHERE PURCHASE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.ROLE = 1	 ");
        	sbQuery.append(" 	        AND PURCHASE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	        ),'')	 ");
        	sbQuery.append(" 	    BUYER_CUI_TYPE,	 ");
    	}
    	
//    	sbQuery.append(" 	    MO.ID_SELLER_PARTICIPANT_FK SELLER_PARTICIPANT,	 ");
    	sbQuery.append(" 	    MO.ID_BUYER_PARTICIPANT_FK SELLER_PARTICIPANT,	 ");
    	
    	if(settledOperationsTO.getParticipantRegisterReport()!=null){
    		sbQuery.append(" 	    CASE WHEN :participantRegisterReport = MO.ID_BUYER_PARTICIPANT_FK THEN (	 ");
    		sbQuery.append(" 	    CONCAT((SELECT LISTAGG(SALE_HA.ACCOUNT_NUMBER, ', ') WITHIN GROUP (ORDER BY SALE_HA.ALTERNATE_CODE)	 ");
        	sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION SALE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT SALE_HA ON SALE_HA.ID_HOLDER_ACCOUNT_PK = SALE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION SALE_SAO ON SALE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = SALE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        WHERE SALE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ROLE = 2	 ");
        	sbQuery.append(" 	        AND SALE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	    ),'')	 ");
        	sbQuery.append(" 	        ) ELSE NULL END    ");
        	sbQuery.append(" 	    SELLER_PARTICIPANT_ACCOUNT,	 ");
        	
    		sbQuery.append(" 	    CASE WHEN :participantRegisterReport = MO.ID_BUYER_PARTICIPANT_FK THEN (	 ");
            sbQuery.append(" 	    CONCAT((SELECT LISTAGG(H.ID_HOLDER_PK||','||H.FULL_NAME, ', ') WITHIN GROUP (ORDER BY H.ID_HOLDER_PK)	 ");
        	sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION SALE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT SALE_HA ON SALE_HA.ID_HOLDER_ACCOUNT_PK = SALE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
            sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = SALE_HA.ID_HOLDER_ACCOUNT_PK		");
            sbQuery.append(" 	        INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK	");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION SALE_SAO ON SALE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = SALE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        WHERE SALE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ROLE = 2	 ");
        	sbQuery.append(" 	        AND SALE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	    ),'')	 ");
        	sbQuery.append(" 	        ) ELSE NULL END    ");
        	sbQuery.append(" 	    SELLER_CUI_NAME,	 ");
        	
        	sbQuery.append(" 	    CASE WHEN :participantRegisterReport = MO.ID_BUYER_PARTICIPANT_FK THEN (	 ");
        	sbQuery.append(" 	    CONCAT((SELECT LISTAGG(PT.INDICATOR1,', ') WITHIN GROUP (ORDER BY H.ID_HOLDER_PK)	 ");
        	sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION SALE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT SALE_HA ON SALE_HA.ID_HOLDER_ACCOUNT_PK = SALE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
            sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = SALE_HA.ID_HOLDER_ACCOUNT_PK		");
            sbQuery.append(" 	        INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK	");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION SALE_SAO ON SALE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = SALE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        INNER JOIN PARAMETER_TABLE PT ON PT.PARAMETER_TABLE_PK = H.ECONOMIC_ACTIVITY ");
        	sbQuery.append(" 	        WHERE SALE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ROLE = 2	 ");
        	sbQuery.append(" 	        AND SALE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	    ),'')	 ");
        	sbQuery.append(" 	        ) ELSE NULL END    ");
        	sbQuery.append(" 	    SELLER_ACT_ECONOMIC,	 ");
            	
    		sbQuery.append(" 	    CASE WHEN :participantRegisterReport = MO.ID_BUYER_PARTICIPANT_FK THEN (	 ");
        	sbQuery.append(" 	    CONCAT((SELECT LISTAGG((select PARAMETER_NAME from PARAMETER_TABLE where PARAMETER_TABLE_PK = H.HOLDER_TYPE ), ', ') WITHIN GROUP (ORDER BY H.HOLDER_TYPE)	 ");
        	sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION SALE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT SALE_HA ON SALE_HA.ID_HOLDER_ACCOUNT_PK = SALE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = SALE_HA.ID_HOLDER_ACCOUNT_PK		");
        	sbQuery.append(" 	        INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK	");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION SALE_SAO ON SALE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = SALE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        WHERE SALE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ROLE = 2	 ");
        	sbQuery.append(" 	        AND SALE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	    ),'')	 ");
        	sbQuery.append(" 	        ) ELSE NULL END    ");
        	sbQuery.append(" 	    SELLER_CUI_TYPE,	 ");
    	}else{
    		sbQuery.append(" 	    CONCAT((SELECT LISTAGG(SALE_HA.ACCOUNT_NUMBER, ', ') WITHIN GROUP (ORDER BY SALE_HA.ALTERNATE_CODE)	 ");
    		sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION SALE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT SALE_HA ON SALE_HA.ID_HOLDER_ACCOUNT_PK = SALE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION SALE_SAO ON SALE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = SALE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        WHERE SALE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ROLE = 2	 ");
        	sbQuery.append(" 	        AND SALE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	    ),'')	 ");
        	sbQuery.append(" 	    SELLER_PARTICIPANT_ACCOUNT,	 ");
        	
            sbQuery.append(" 	    CONCAT((SELECT LISTAGG(H.ID_HOLDER_PK||','||H.FULL_NAME, ', ') WITHIN GROUP (ORDER BY H.ID_HOLDER_PK)	 ");
    		sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION SALE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT SALE_HA ON SALE_HA.ID_HOLDER_ACCOUNT_PK = SALE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = SALE_HA.ID_HOLDER_ACCOUNT_PK		");
        	sbQuery.append(" 	        INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK	");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION SALE_SAO ON SALE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = SALE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        WHERE SALE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ROLE = 2	 ");
        	sbQuery.append(" 	        AND SALE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	    ),'')	 ");
        	sbQuery.append(" 	    SELLER_CUI_NAME,	 ");
        	
        	sbQuery.append(" 	    CONCAT((SELECT LISTAGG(PT.INDICATOR1,', ') WITHIN GROUP (ORDER BY H.ID_HOLDER_PK)	 ");
    		sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION SALE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT SALE_HA ON SALE_HA.ID_HOLDER_ACCOUNT_PK = SALE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = SALE_HA.ID_HOLDER_ACCOUNT_PK		");
        	sbQuery.append(" 	        INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK	");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION SALE_SAO ON SALE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = SALE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        INNER JOIN PARAMETER_TABLE PT ON PT.PARAMETER_TABLE_PK = H.ECONOMIC_ACTIVITY ");
        	sbQuery.append(" 	        WHERE SALE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ROLE = 2	 ");
        	sbQuery.append(" 	        AND SALE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	    ),'')	 ");
        	sbQuery.append(" 	    SELLER_ACT_ECONOMIC,	 ");
        	
        	sbQuery.append(" 	    CONCAT((SELECT LISTAGG((select PARAMETER_NAME from PARAMETER_TABLE where PARAMETER_TABLE_PK = H.HOLDER_TYPE ), ', ') WITHIN GROUP (ORDER BY H.HOLDER_TYPE)	 ");
    		sbQuery.append(" 	        FROM HOLDER_ACCOUNT_OPERATION SALE_HAO	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT SALE_HA ON SALE_HA.ID_HOLDER_ACCOUNT_PK = SALE_HAO.ID_HOLDER_ACCOUNT_FK	 ");
        	sbQuery.append(" 	        INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = SALE_HA.ID_HOLDER_ACCOUNT_PK		");
        	sbQuery.append(" 	        INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK	");
        	sbQuery.append(" 	        INNER JOIN SETTLEMENT_ACCOUNT_OPERATION SALE_SAO ON SALE_SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = SALE_HAO.ID_HOLDER_ACCOUNT_OPERATION_PK	 ");
        	sbQuery.append(" 	        WHERE SALE_HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
        	sbQuery.append(" 	        AND SALE_SAO.ROLE = 2	 ");
        	sbQuery.append(" 	        AND SALE_SAO.OPERATION_STATE = 625	 ");
        	sbQuery.append(" 	    ),'')	 ");
        	sbQuery.append(" 	    SELLER_CUI_TYPE,	 ");
    	}

    	sbQuery.append(" 	    MO.ID_SECURITY_CODE_FK,	 ");
    	sbQuery.append(" 	    (SELECT PT.TEXT1 FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK  = MO.OPERATION_CURRENCY) AS OPERATION_CURRENCY,	 ");
    	sbQuery.append(" 	    SO.STOCK_QUANTITY,	 ");
    	sbQuery.append(" 	    so.settlement_price,	 ");
    	//sbQuery.append(" 	    DECODE(SO.OPERATION_PART , 1 , MO.CASH_PRICE, 2, MO.TERM_PRICE) AS OPER_PRICE,	 ");
    	sbQuery.append(" 	    SO.AMOUNT_RATE,	 ");
    	sbQuery.append(" 	    SO.SETTLEMENT_DAYS,	 ");
    	sbQuery.append("  		SO.SETTLEMENT_AMOUNT, ");
    	//sbQuery.append(" 	    DECODE(SO.OPERATION_PART , 1 , MO.CASH_PRICE*SO.STOCK_QUANTITY, 2, MO.TERM_PRICE*SO.STOCK_QUANTITY) AS OPER_AMOUNT,	 ");
    	sbQuery.append(" 	    (CASE SO.SETTLEMENT_CURRENCY	 ");
    	sbQuery.append(" 	        WHEN 127 THEN	 ");
    	sbQuery.append(" 	            SO.SETTLEMENT_AMOUNT / NVL((SELECT DER.BUY_PRICE FROM DAILY_EXCHANGE_RATES DER WHERE DER.DATE_RATE = SO.SETTLEMENT_DATE	 ");
    	sbQuery.append(" 	            AND DER.ID_CURRENCY = 430 AND ID_SOURCE_INFORMATION = 212),1)	 ");
    	sbQuery.append(" 	        WHEN 430 THEN	 ");
    	sbQuery.append(" 	            SO.SETTLEMENT_AMOUNT	 ");
    	sbQuery.append(" 	        END	 ");
    	sbQuery.append(" 	    ) AS OPER_AMOUNT_USD,	 ");
    	sbQuery.append(" 	    DECODE(SO.SETTLEMENT_TYPE, 1, 'FOP', 2, 'DVP' ) SETTLEMENT_TYPE,	 ");
    	sbQuery.append(" 	    (SELECT PT.PARAMETER_NAME FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK  = SP.SCHEDULE_TYPE) AS INSTANCIA,	 ");
    	sbQuery.append(" 	    SELLER_PA.MNEMONIC MNEMONIC_BUYER_PART,	 ");
    	sbQuery.append(" 	    BUYER_PA.MNEMONIC MNEMONIC_SELLER_PART,	 ");
    	
    	//ISSUE 1295: A requerimeinto del usuario se determina si la operacion es o no SELAR
    	sbQuery.append("		decode((select count(*)                                                                                     ");
        sbQuery.append("		from SETTLEMENT_DATE_OPERATION sdo                                                                          ");
        sbQuery.append("		inner join SETTLEMENT_REQUEST sr on SR.ID_SETTLEMENT_REQUEST_PK = SDO.ID_SETTLEMENT_REQUEST_FK              ");
        sbQuery.append("		inner join SETTLEMENT_OPERATION so_sq on SO_SQ.ID_SETTLEMENT_OPERATION_PK = SDO.ID_SETTLEMENT_OPERATION_FK  ");
        sbQuery.append("		inner join MECHANISM_OPERATION mo_sq on MO_SQ.ID_MECHANISM_OPERATION_PK = SO_SQ.ID_MECHANISM_OPERATION_FK   ");
        sbQuery.append("		where 0=0                                                                                                   ");
        sbQuery.append("		AND MO_SQ.BALLOT_NUMBER = MO.BALLOT_NUMBER                                                                  ");
        sbQuery.append("		AND MO_SQ.SEQUENTIAL = MO.SEQUENTIAL                                                                        ");
        sbQuery.append("		and (trunc(sr.REGISTER_DATE) = TRUNC(SO.SETTLEMENT_DATE))                                                   ");
        // indicador de SELAR = 2019
        sbQuery.append("		and sr.REQUEST_TYPE=2019 ),                                                                                 ");
        sbQuery.append("		0,'NO','SI') AS SELAR,																						");
		sbQuery.append("		mo.AMOUNT_RATE as tasa_negociacion																			");
    	
    	sbQuery.append(" 	FROM	 ");
    	sbQuery.append(" 	    MECHANISM_OPERATION MO,  SETTLEMENT_OPERATION SO, PARTICIPANT BUYER_PA , PARTICIPANT SELLER_PA	 ");
    	sbQuery.append(" 	    ,MODALITY_GROUP MG , MODALITY_GROUP_DETAIL MGD , MECHANISM_MODALITY MM , NEGOTIATION_MODALITY  NMO	 ");
    	sbQuery.append(" 	    ,SETTLEMENT_PROCESS SP , OPERATION_SETTLEMENT OS, PARTICIPANT_SETTLEMENT PS, security secu	 ");
    	sbQuery.append(" 	WHERE	 ");
    	sbQuery.append(" 	    MO.ID_MECHANISM_OPERATION_PK = SO.ID_MECHANISM_OPERATION_FK	 ");
    	sbQuery.append(" 	    AND MG.ID_MODALITY_GROUP_PK = MGD.ID_MODALITY_GROUP_FK	 ");
    	sbQuery.append(" 	    AND MGD.ID_NEGOTIATION_MECHANISM_FK = MM.ID_NEGOTIATION_MECHANISM_PK	 ");
    	sbQuery.append(" 	    AND MGD.ID_NEGOTIATION_MODALITY_FK = MM.ID_NEGOTIATION_MODALITY_PK	 ");
    	sbQuery.append(" 	    AND MM.ID_NEGOTIATION_MECHANISM_PK = MO.ID_NEGOTIATION_MECHANISM_FK	 ");
    	sbQuery.append(" 	    AND MM.ID_NEGOTIATION_MODALITY_PK = MO.ID_NEGOTIATION_MODALITY_FK	 ");
    	sbQuery.append(" 	    AND MM.ID_NEGOTIATION_MODALITY_PK = NMO.ID_NEGOTIATION_MODALITY_PK	 ");
    	sbQuery.append(" 	    AND MG.SETTLEMENT_SCHEMA = SO.SETTLEMENT_SCHEMA	 ");
    	sbQuery.append(" 	    AND SP.ID_SETTLEMENT_PROCESS_PK = OS.ID_SETTLMENT_PROCESS_FK	 ");
    	sbQuery.append(" 	    AND OS.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
//    	sbQuery.append(" 	    AND SP.PROCESS_STATE = 1284  ");
    	sbQuery.append("        AND SO.OPERATION_STATE in(615,616) ");
    	sbQuery.append(" 	    AND SP.SETTLEMENT_DATE = SO.SETTLEMENT_DATE	 ");
    	sbQuery.append(" 	    AND SP.SETTLEMENT_SCHEMA = SO.SETTLEMENT_SCHEMA	 ");
    	sbQuery.append(" 	    AND SP.CURRENCY = SO.SETTLEMENT_CURRENCY	 ");
    	sbQuery.append(" 	    AND SP.ID_NEGOTIATION_MECHANISM_FK = MO.ID_NEGOTIATION_MECHANISM_FK	 ");
    	sbQuery.append(" 	    AND SP.ID_MODALITY_GROUP_FK = MG.ID_MODALITY_GROUP_PK	 ");
    	sbQuery.append("        AND SO.OPERATION_STATE in(615,616) ");
//    	sbQuery.append(" 	    AND ( (SO.OPERATION_PART = 1 AND  SO.OPERATION_STATE IN (615))	 ");
//    	sbQuery.append(" 	         OR (SO.OPERATION_PART = 2 AND  SO.OPERATION_STATE IN (616)))	 ");
    	sbQuery.append(" 	    AND MO.ID_BUYER_PARTICIPANT_FK = BUYER_PA.ID_PARTICIPANT_PK	 ");
    	sbQuery.append(" 	    AND MO.ID_SELLER_PARTICIPANT_FK = SELLER_PA.ID_PARTICIPANT_PK	 ");
    	sbQuery.append(" 	    AND OS.OPERATION_STATE=1288 	 ");
    	sbQuery.append("        AND MM.ID_NEGOTIATION_MECHANISM_PK=1 ");
    	sbQuery.append(" 	    and mo.ID_SECURITY_CODE_FK=secu.ID_SECURITY_CODE_PK	 ");
    	sbQuery.append(" 	    AND PS.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK	 ");
    	sbQuery.append(" 	    AND (TRUNC(SO.SETTLEMENT_DATE) BETWEEN TO_DATE(':date_initial','DD/MM/YYYY') AND TO_DATE(':date_end','DD/MM/YYYY'))	 ");

		if(Validations.validateIsNotNull(settledOperationsTO.getParticipantPk())){
			sbQuery.append(" AND (PS.ID_PARTICIPANT_FK = :participantPk)	 ");	
		}
		if(Validations.validateIsNotNull(settledOperationsTO.getModality())){
			sbQuery.append(" AND (NMO.ID_NEGOTIATION_MODALITY_PK = :modality) ");
    	}
		if(Validations.validateIsNotNull(settledOperationsTO.getCurrency())){
			sbQuery.append(" AND (MO.OPERATION_CURRENCY = :currency)	 ");
    	}
		
		if(Validations.validateIsNotNull(settledOperationsTO.getSecurityCode())){
			sbQuery.append(" AND (MO.ID_SECURITY_CODE_FK = :security_code)	 ");
		}
		if(Validations.validateIsNotNull(settledOperationsTO.getSecurityClass())){
			sbQuery.append(" AND (SECU.SECURITY_CLASS = :security_class)	 ");
		}
		
		if(Validations.validateIsNotNull(settledOperationsTO.getSchema())){
			sbQuery.append(" AND (SO.SETTLEMENT_SCHEMA = :schema)	 ");
    	}
		if(Validations.validateIsNotNull(settledOperationsTO.getScheduleType())){
			sbQuery.append(" AND (SP.SCHEDULE_TYPE = :schedule_type)	 ");
    	}
		
		sbQuery.append("	AND NMO.MODALITY_CODE = 'RPT'	 ");
		
		//Issue 811 
		//sbQuery.append("	AND MO.OPERATION_STATE = 616	 ");
		
		sbQuery.append("	AND SO.OPERATION_PART = 2			");
		
		sbQuery.append(" 	ORDER BY SETTLEMENT_DATE	 ");

		String strQueryFormatted = sbQuery.toString();
		
		if(settledOperationsTO.getParticipantPk()!=null){
    		strQueryFormatted= strQueryFormatted.replace(":participantPk", settledOperationsTO.getParticipantPk().toString());
    	}
    	if(settledOperationsTO.getModality()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":modality", settledOperationsTO.getModality().toString());
    	}
    	if(settledOperationsTO.getCurrency()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":currency", "'"+settledOperationsTO.getCurrency()+"'");
    	}
    	if(settledOperationsTO.getSecurityClass()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":security_class", "'"+settledOperationsTO.getSecurityClass()+"'");
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(settledOperationsTO.getSecurityCode())){
    		strQueryFormatted=strQueryFormatted.replace(":security_code", settledOperationsTO.getSecurityCode().toString());
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(settledOperationsTO.getSchema())){
    		strQueryFormatted=strQueryFormatted.replace(":schema", settledOperationsTO.getSchema().toString());
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(settledOperationsTO.getScheduleType())){
    		strQueryFormatted=strQueryFormatted.replace(":schedule_type", settledOperationsTO.getScheduleType().toString());
    	}
    	
    	//ISSUE 775
    	if(settledOperationsTO.getParticipantRegisterReport()!=null){
    		strQueryFormatted= strQueryFormatted.replace(":participantRegisterReport", settledOperationsTO.getParticipantRegisterReport().toString());
    	}
    	
    	strQueryFormatted=strQueryFormatted.replace(":date_initial", settledOperationsTO.getStrDateInitial());
    	strQueryFormatted=strQueryFormatted.replace(":date_end", settledOperationsTO.getStrDateEnd());
    	
    	return strQueryFormatted;
    }
    /*Rep : 285*/
    public List<SecuritiesSettlementTO> getListSecuritiesSettlements(GenericsFiltersTO gfto){
    	StringBuilder builder= new StringBuilder();
    	builder.append(" SELECT DISTINCT "); 
    	builder.append(" PS.ID_PARTICIPANT_FK AS ID_PARTICIPANT_PK, ");
    	builder.append(" MO.OPERATION_CURRENCY, ");
    	builder.append(" MO.OPERATION_DATE, ");
        builder.append(" MO.ID_BUYER_PARTICIPANT_FK BUYER_PARTICIPANT, ");
        builder.append(" MO.ID_SELLER_PARTICIPANT_FK SELLER_PARTICIPANT, ");
        builder.append(" so.settlement_price AS OPER_PRICE, ");
        builder.append(" SO.SETTLEMENT_AMOUNT AS OPER_AMOUNT, ");
        builder.append(" SP.SCHEDULE_TYPE ");
        builder.append(" FROM ");
        builder.append(" MECHANISM_OPERATION MO,  SETTLEMENT_OPERATION SO, PARTICIPANT BUYER_PA , PARTICIPANT SELLER_PA ");
        builder.append(" ,MODALITY_GROUP MG , MODALITY_GROUP_DETAIL MGD , MECHANISM_MODALITY MM , NEGOTIATION_MODALITY  NMO ");
        builder.append(" ,SETTLEMENT_PROCESS SP , OPERATION_SETTLEMENT OS, PARTICIPANT_SETTLEMENT PS, security secu ");
        builder.append(" WHERE 1 = 1 ");
        builder.append(" AND MO.ID_MECHANISM_OPERATION_PK = SO.ID_MECHANISM_OPERATION_FK ");
        builder.append(" AND MG.ID_MODALITY_GROUP_PK = MGD.ID_MODALITY_GROUP_FK ");
        builder.append(" AND MGD.ID_NEGOTIATION_MECHANISM_FK = MM.ID_NEGOTIATION_MECHANISM_PK ");
        builder.append(" AND MGD.ID_NEGOTIATION_MODALITY_FK = MM.ID_NEGOTIATION_MODALITY_PK ");
        builder.append(" AND MM.ID_NEGOTIATION_MECHANISM_PK = MO.ID_NEGOTIATION_MECHANISM_FK ");
        builder.append(" AND MM.ID_NEGOTIATION_MODALITY_PK = MO.ID_NEGOTIATION_MODALITY_FK ");
        builder.append(" AND MM.ID_NEGOTIATION_MODALITY_PK = NMO.ID_NEGOTIATION_MODALITY_PK ");
        builder.append(" AND MG.SETTLEMENT_SCHEMA = SO.SETTLEMENT_SCHEMA ");
        builder.append(" AND SP.ID_SETTLEMENT_PROCESS_PK = OS.ID_SETTLMENT_PROCESS_FK ");
        builder.append(" AND OS.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK ");
        builder.append(" and SO.OPERATION_STATE in(615,616) ");
        builder.append(" AND SP.SETTLEMENT_DATE = SO.SETTLEMENT_DATE ");
        builder.append(" AND SP.SETTLEMENT_SCHEMA = SO.SETTLEMENT_SCHEMA ");
        builder.append(" AND SP.CURRENCY = SO.SETTLEMENT_CURRENCY ");
        builder.append(" AND SP.ID_NEGOTIATION_MECHANISM_FK = MO.ID_NEGOTIATION_MECHANISM_FK ");
        builder.append(" AND SP.ID_MODALITY_GROUP_FK = MG.ID_MODALITY_GROUP_PK ");
        builder.append(" AND MO.ID_BUYER_PARTICIPANT_FK = BUYER_PA.ID_PARTICIPANT_PK ");
        builder.append(" AND MO.ID_SELLER_PARTICIPANT_FK = SELLER_PA.ID_PARTICIPANT_PK ");
        builder.append(" AND OS.OPERATION_STATE=1288 "); //Liquidada
        builder.append(" and mo.ID_SECURITY_CODE_FK=secu.ID_SECURITY_CODE_PK ");
        builder.append(" AND PS.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK ");
        builder.append(" AND MM.ID_NEGOTIATION_MECHANISM_PK=1 ");
        builder.append(" AND PS.ID_PARTICIPANT_FK = "+gfto.getParticipant());
        builder.append(" AND MO.OPERATION_CURRENCY = "+gfto.getCurrency());
        builder.append(" AND TRUNC(SO.SETTLEMENT_DATE) = TO_DATE('"+gfto.getFinalDt()+"','dd/MM/yyyy') ");
        //builder.append(" AND TRUNC(SO.SETTLEMENT_DATE) BETWEEN TO_DATE('22/06/2017','dd/MM/yyyy') AND TO_DATE('22/06/2017','dd/MM/yyyy') ");
        builder.append(" ORDER BY MO.OPERATION_DATE ");
        
        Query query = em.createNativeQuery(builder.toString());
        
        List<Object[]> resultList = query.getResultList();
        List<SecuritiesSettlementTO> securitiesSettlementTOs = new ArrayList<>();
        SecuritiesSettlementTO settlementTO;
        for (Object[] objects : resultList) {
        	settlementTO = new SecuritiesSettlementTO();
        	settlementTO.setIdParticipantPk(Long.parseLong(objects[0].toString()));
        	settlementTO.setCurrency(Integer.parseInt(objects[1].toString()));
        	settlementTO.setIdPartBuyer(Long.parseLong(objects[3].toString()));
        	settlementTO.setIdPartSeller(Long.parseLong(objects[4].toString()));
        	settlementTO.setPrice(new BigDecimal(objects[5].toString()));
        	settlementTO.setAmount(new BigDecimal(objects[6].toString()));
        	settlementTO.setScheduleType(Integer.parseInt(objects[7].toString()));
        	
        	securitiesSettlementTOs.add(settlementTO);
		}
    	return securitiesSettlementTOs;
    }
    
    /*Ref 285 report*/
    public String getMnemonnicParticipant(Long idParticipantPk){
    	Participant participant = em.find(Participant.class, idParticipantPk);
    	return participant.getMnemonic();
    }
    /*Rep 284*/
    public List<AcquisitionInPrimaryMarketTO>getAcquisitionInPrimaryMarketBBV(GenericsFiltersTO gfto){
    	StringBuilder builder= new StringBuilder();
    	builder.append(" SELECT MO.ID_SELLER_PARTICIPANT_FK, ");//0
    	builder.append(" SE.ID_SECURITY_CODE_PK, ");//1
    	builder.append(" SE.ID_ISSUER_FK, ");//2
    	builder.append(" (select ISS.MNEMONIC from ISSUER ISS where ISS.ID_ISSUER_PK=SE.ID_ISSUER_FK) AS ISSUER_MNEMONIC, ");    //3
    	builder.append(" SE.SECURITY_CLASS as SECURITY_CLASS, "); //4
    	builder.append(" SE.DESCRIPTION AS INSTRUMENT_KEY, "); //5
    	builder.append(" to_char(ISS.ISSUANCE_DATE,'dd/mm/yyyy') AS ISSUANCE_DATE, ");//6
    	builder.append(" (select DECODE(HO.Holder_Type,96,'N',394,'J','') FROM HOLDER HO WHERE HO.ID_HOLDER_PK = (select ISS.ID_HOLDER_FK from ISSUER ISS where ISS.ID_ISSUER_PK=SE.ID_ISSUER_FK)) AS PERSON_TYPE, ");//7
    	builder.append(" (select HO.DOCUMENT_NUMBER from holder HO WHERE HO.ID_HOLDER_PK = (select ISS.ID_HOLDER_FK from ISSUER ISS where ISS.ID_ISSUER_PK=SE.ID_ISSUER_FK)) AS DOCUMENT_NUMBER, "); //8
    	builder.append(" (select ISS.ID_HOLDER_FK from ISSUER ISS where ISS.ID_ISSUER_PK=SE.ID_ISSUER_FK) AS ID_HOLDER_PK, ");//9
    	builder.append(" (SELECT PA.ECONOMIC_ACTIVITY FROM PARTICIPANT PA WHERE PA.ID_PARTICIPANT_PK = MO.ID_SELLER_PARTICIPANT_FK) AS ECONOMIC_ACTIVITY, ");//10
    	builder.append(" SE.CURRENT_NOMINAL_VALUE AS NOMINAL_VALUE, ");//11
    	builder.append(" MO.CASH_PRICE AS ACQUISITION_PRICE, ");//12
    	builder.append(" SE.CURRENCY AS CURRENCY ");        //13
    	builder.append(" FROM MECHANISM_OPERATION MO, ");   
    	builder.append(" MECHANISM_OPERATION MO_REF, ");    
    	builder.append(" SECURITY SE, ISSUANCE ISS ");      
    	builder.append(" WHERE MO_REF.ID_REFERENCE_OPERATION_FK (+) = MO.ID_MECHANISM_OPERATION_PK ");
    	builder.append(" AND MO.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK ");
    	builder.append(" AND MO.ID_NEGOTIATION_MECHANISM_FK = 1 ");
    	builder.append(" AND ISS.ID_ISSUANCE_CODE_PK = SE.ID_ISSUANCE_CODE_FK ");
    	builder.append(" AND SE.SECURITY_CLASS ! = 420 ");
    	if(Validations.validateIsNotNullAndNotEmpty(gfto.getIdSecurityCodePk()))
    		builder.append(" AND SE.ID_SECURITY_CODE_PK = '"+gfto.getIdSecurityCodePk()+"'" );
    	builder.append(" AND trunc(mo.OPERATION_DATE) BETWEEN to_date('"+gfto.getInitialDt()+"','dd/MM/yyyy') AND to_date('"+gfto.getFinalDt()+"','dd/MM/yyyy') ");
    	builder.append(" ORDER BY SE.ID_SECURITY_CODE_PK ");
    	
    	Query query = em.createNativeQuery(builder.toString());
    	List<Object[]> list = query.getResultList();
    	AcquisitionInPrimaryMarketTO acquisitionInPrimaryMarketTO;
    	List<AcquisitionInPrimaryMarketTO> listTOs = new ArrayList<>();
    	for (Object[] objects : list) {
    		acquisitionInPrimaryMarketTO = new AcquisitionInPrimaryMarketTO();
    		acquisitionInPrimaryMarketTO.setIdParticipantPk(new BigDecimal(objects[0].toString()).longValue());
    		acquisitionInPrimaryMarketTO.setIdSecurityCodePk(objects[1].toString());
    		acquisitionInPrimaryMarketTO.setIdIssuerPk(objects[2].toString());
    		acquisitionInPrimaryMarketTO.setIssuerMnemonic(objects[3].toString());
    		acquisitionInPrimaryMarketTO.setSecurityClass(new BigDecimal(objects[4].toString()).intValue());
    		acquisitionInPrimaryMarketTO.setInstrumentKey(objects[5]!=null?objects[5].toString():null);
    		acquisitionInPrimaryMarketTO.setIssuanceDate(objects[6].toString());
    		acquisitionInPrimaryMarketTO.setPersonType(objects[7]!=null?objects[7].toString():"");
    		acquisitionInPrimaryMarketTO.setDocumentNumber(objects[8]!=null?objects[8].toString():"");
    		acquisitionInPrimaryMarketTO.setIdHolderPk(objects[9]!=null?Long.parseLong(objects[9].toString()):null);
    		acquisitionInPrimaryMarketTO.setEconomicActivity(objects[10]!=null?Integer.parseInt(objects[10].toString()):null);
    		acquisitionInPrimaryMarketTO.setNominalValue(objects[11]!=null?new BigDecimal(objects[11].toString()):null);
    		acquisitionInPrimaryMarketTO.setAcquisitionPrice(objects[12]!=null?new BigDecimal(objects[12].toString()):null);
    		acquisitionInPrimaryMarketTO.setCurrency(Integer.parseInt(objects[13].toString()));
    		listTOs.add(acquisitionInPrimaryMarketTO);
		}
		return listTOs;
    }
    public String getSecurityClassDesc(Integer securityClass){
		ParameterTable parameterTable = em.find(ParameterTable.class, securityClass);
		return parameterTable.getText1();
	}
   
    public BigDecimal getCountACV(GenericsFiltersTO gfto,AcquisitionInPrimaryMarketTO to){
	    StringBuilder builder = new StringBuilder();
	    builder.append(" SELECT count(PA.Id_Participant_Pk) ");
	    builder.append(" FROM ACCOUNT_ANNOTATION_OPERATION AAO, CUSTODY_OPERATION CO, HOLDER_ACCOUNT HA, ");
	    builder.append(" SECURITY SE, ACCOUNT_ANNOTATION_MARKETFACT AAM, PARTICIPANT PA ");
	    builder.append(" WHERE AAO.ID_ANNOTATION_OPERATION_PK = CO.ID_CUSTODY_OPERATION_PK ");
	    builder.append(" AND AAO.ID_ANNOTATION_OPERATION_PK = AAM.ID_ANNOTATION_OPERATION_FK ");
	    builder.append(" AND AAO.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK ");
	    builder.append(" AND AAO.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ");
	    builder.append(" AND HA.ID_PARTICIPANT_FK = PA.ID_PARTICIPANT_PK ");
	    
	    //builder.append(" AND PA.Id_Participant_Pk =  "+to.getIdParticipantPk());
	    builder.append(" AND PA.ID_ISSUER_FK = '"+to.getIdIssuerPk()+"' ");
	    //builder.append(" AND SE.ID_SECURITY_CODE_PK   = '"+to.getIdSecurityCodePk()+"' ");
	    //builder.append(" AND SE.SECURITY_CLASS = "+to.getId);
	    builder.append(" AND CO.OPERATION_STATE = 858 ");
	    builder.append(" AND TRUNC(CO.REGISTRY_DATE) between TO_DATE('"+gfto.getInitialDt()+"','dd/mm/yyyy') ");
	    builder.append(" AND TO_DATE('"+gfto.getFinalDt()+"','dd/mm/yyyy') ");
	    builder.append(" ORDER BY ");
	    builder.append(" PA.MNEMONIC, ");
	    builder.append(" HA.ID_HOLDER_ACCOUNT_PK, ");
	    builder.append(" SE.ID_SECURITY_CODE_PK, ");
	    builder.append(" CO.OPERATION_NUMBER, ");
	    builder.append(" se.issuance_date ");
	    Query  query = em.createNativeQuery(builder.toString());
	   
	    BigDecimal count = (BigDecimal) query.getSingleResult();
	    return count;
   }
    
    /**
     * QUERY PARA GENERAR EL REPORTE 4 DE ASFI
     * Solo necesitamos la fecha para generar el reporte
     * @param gfto
     * @return
     */
    public List<Object[]> getListSecuritiesSettlementss(GenericsFiltersTO gfto){
    	StringBuilder builder= new StringBuilder();
    	
    	builder.append(" select CPY CPY,                                             ");
    	builder.append(" FECHA FECHA,                                                ");
    	builder.append(" ENTIDAD ENTIDAD,                                            ");
    	builder.append(" MONEDAOP MONEDAOP,                                          ");
    	builder.append(" MONEDA MONEDA,                                              ");
    	builder.append(" COMPRA COMPRA,                                              ");
    	builder.append(" VENTA VENTA,                                                ");
    	builder.append(" POSNETADEUDA POSNETADEUDA,                                  ");
    	builder.append(" POSNETAACRE POSNETAACRE,                                    ");
    	// este cAlculo de MontoLiquidado es a requerimiento de usuario
    	builder.append(" (COMPRA + VENTA - OPERACIONES_CRUZADAS) MON_LIQ,");
    	builder.append(" MON_LIQ_MELOR MON_LIQ_MELOR,                                ");
    	builder.append(" MON_LIQ_MELID MON_LIQ_MELID,                                ");
    	builder.append(" OPERACIONES_CRUZADAS OPERACIONES_CRUZADAS					 ");
    	builder.append(" FROM (					 ");
        
    	builder.append(" 	select                                                                                                                    "); 
    	builder.append(" 	'CPY' CPY,                                                                                                                    "); 
    	builder.append(" 	TO_CHAR(A.SETTLEMENT_DATE,'YYYY-MM-DD') FECHA,                                                                            "); 
    	builder.append(" 	(SELECT MNEMONIC FROM PARTICIPANT WHERE ID_PARTICIPANT_PK = A.ID_INCHARGE_STOCK_PARTICIPANT) ENTIDAD,                     "); 
    	builder.append(" 	CASE  WHEN A.OPERATION_CURRENCY = 127  THEN '10'                                                                       "); 
    	builder.append(" 		WHEN A.OPERATION_CURRENCY = 430  THEN '12'                                                                         "); 
    	builder.append(" 		WHEN A.OPERATION_CURRENCY = 1853 THEN '11'                                                                            "); 
    	builder.append(" 		WHEN A.OPERATION_CURRENCY = 1734 THEN '14'                                                                          "); 
    	builder.append(" 		WHEN A.OPERATION_CURRENCY = 1304 THEN '15'                                                                            "); 
    	builder.append(" 		END MONEDAOP,                                                                                                         "); 
    	builder.append(" 	A.OPERATION_CURRENCY MONEDA,                                                                                              "); 
    	builder.append(" 	NVL(sum(A.TOTAL1),0) COMPRA,                                                                                              "); 
    	builder.append(" 	NVL(sum(A.TOTAL2),0) VENTA ,                                                                                              "); 
    	builder.append(" 	CASE WHEN NVL(sum(A.TOTAL2),0)-NVL(sum(A.TOTAL1),0) < 0                                                                   "); 
    	builder.append(" 		THEN TO_NUMBER(REPLACE(TO_CHAR(ROUND(NVL(sum(A.TOTAL2),0)-NVL(sum(A.TOTAL1),0),2)),'-','')) ELSE 0                    "); 
    	builder.append(" 		END  POSNETADEUDA,                                                                                                    "); 
    	builder.append(" 	CASE WHEN NVL(sum(A.TOTAL2),0)-NVL(sum(A.TOTAL1),0) >= 0                                                                  "); 
    	builder.append(" 		THEN ROUND(NVL(sum(A.TOTAL2),0)-NVL(sum(A.TOTAL1),0),2) ELSE 0                                                        "); 
    	builder.append(" 		END  POSNETAACRE,                                                                                                     "); 
    	builder.append(" 	(SELECT NVL(ROUND(sum(SETTLEMENT_AMOUNT),2),0)                                                                            "); 
    	builder.append(" 	FROM (                                                                                                                    "); 
    	builder.append(" 			SELECT DISTINCT                                                                                                   "); 
    	builder.append(" 				PS_SUB1.ID_PARTICIPANT_FK,                                                                                    "); 
    	builder.append(" 				MO_SUB1.OPERATION_CURRENCY,                                                                                   "); 
    	builder.append(" 				MO_SUB1.ID_MECHANISM_OPERATION_PK,                                                                            "); 
    	builder.append(" 				SO_SUB1.SETTLEMENT_AMOUNT,                                                                                    "); 
    	builder.append(" 				SO_SUB1.SETTLEMENT_DATE                                                                                       "); 
    	builder.append(" 			FROM                                                                                                              "); 
    	builder.append(" 				MECHANISM_OPERATION MO_SUB1,  SETTLEMENT_OPERATION SO_SUB1                                                    "); 
    	builder.append(" 				,SETTLEMENT_PROCESS SP_SUB1 , OPERATION_SETTLEMENT OS_SUB1, PARTICIPANT_SETTLEMENT PS_SUB1                    "); 
    	builder.append(" 			WHERE                                                                                                             "); 
    	builder.append(" 				MO_SUB1.ID_MECHANISM_OPERATION_PK = SO_SUB1.ID_MECHANISM_OPERATION_FK                                         "); 
    	builder.append(" 				AND SP_SUB1.ID_SETTLEMENT_PROCESS_PK = OS_SUB1.ID_SETTLMENT_PROCESS_FK                                        "); 
    	builder.append(" 				AND OS_SUB1.ID_SETTLEMENT_OPERATION_FK = SO_SUB1.ID_SETTLEMENT_OPERATION_PK                                   "); 
    	builder.append(" 				and SO_SUB1.OPERATION_STATE in(615,616)                                                                       "); 
    	builder.append(" 				AND SP_SUB1.SETTLEMENT_SCHEMA = SO_SUB1.SETTLEMENT_SCHEMA                                                     "); 
    	builder.append(" 				AND OS_SUB1.OPERATION_STATE=1288                                                                "); 
    	builder.append(" 				AND PS_SUB1.ID_SETTLEMENT_OPERATION_FK = SO_SUB1.ID_SETTLEMENT_OPERATION_PK                                   "); 
    	builder.append(" 				AND SO_SUB1.SETTLEMENT_SCHEMA = 2                                                                             "); 
    	builder.append(" 				AND MO_SUB1.ID_NEGOTIATION_MECHANISM_FK = 1                                                                   "); 
    	builder.append(" 				AND SP_SUB1.SCHEDULE_TYPE in (1939,1940)                                                                      "); 
    	//builder.append(" 				order by 1,2                                                                                                  "); 
    	builder.append(" 		) X                                                                                                                   "); 
    	builder.append(" 	WHERE X.OPERATION_CURRENCY =  A.OPERATION_CURRENCY                                                                        "); 
    	builder.append(" 	AND X.ID_PARTICIPANT_FK = A.ID_INCHARGE_STOCK_PARTICIPANT                                                                 "); 
    	builder.append(" 	AND TRUNC(X.SETTLEMENT_DATE) = A.SETTLEMENT_DATE                                                                          "); 
    	builder.append(" 	) MON_LIQ,                                                                                                                "); 
    	builder.append(" 	(SELECT NVL(ROUND(sum(SETTLEMENT_AMOUNT),2),0)                                                                            "); 
    	builder.append(" 	FROM (                                                                                                                    "); 
    	builder.append(" 			SELECT DISTINCT                                                                                                   "); 
    	builder.append(" 				PS_SUB1.ID_PARTICIPANT_FK,                                                                                    "); 
    	builder.append(" 				MO_SUB1.OPERATION_CURRENCY,                                                                                   "); 
    	builder.append(" 				MO_SUB1.ID_MECHANISM_OPERATION_PK,                                                                            "); 
    	builder.append(" 				SO_SUB1.SETTLEMENT_AMOUNT,                                                                                    "); 
    	builder.append(" 				SO_SUB1.SETTLEMENT_DATE                                                                                       "); 
    	builder.append(" 			FROM                                                                                                              "); 
    	builder.append(" 				MECHANISM_OPERATION MO_SUB1,  SETTLEMENT_OPERATION SO_SUB1                                                    "); 
    	builder.append(" 				,SETTLEMENT_PROCESS SP_SUB1 , OPERATION_SETTLEMENT OS_SUB1, PARTICIPANT_SETTLEMENT PS_SUB1                    "); 
    	builder.append(" 			WHERE                                                                                                             "); 
    	builder.append(" 				MO_SUB1.ID_MECHANISM_OPERATION_PK = SO_SUB1.ID_MECHANISM_OPERATION_FK                                         "); 
    	builder.append(" 				AND SP_SUB1.ID_SETTLEMENT_PROCESS_PK = OS_SUB1.ID_SETTLMENT_PROCESS_FK                                        "); 
    	builder.append(" 				AND OS_SUB1.ID_SETTLEMENT_OPERATION_FK = SO_SUB1.ID_SETTLEMENT_OPERATION_PK                                   "); 
    	builder.append(" 				and SO_SUB1.OPERATION_STATE in(615,616)                                                                       "); 
    	builder.append(" 				AND SP_SUB1.SETTLEMENT_SCHEMA = SO_SUB1.SETTLEMENT_SCHEMA                                                     "); 
    	builder.append(" 				AND OS_SUB1.OPERATION_STATE=1288 			                                                                  "); 
    	builder.append(" 				AND PS_SUB1.ID_SETTLEMENT_OPERATION_FK = SO_SUB1.ID_SETTLEMENT_OPERATION_PK                                   "); 
    	builder.append(" 				AND SO_SUB1.SETTLEMENT_SCHEMA = 2                                                                             "); 
    	builder.append(" 				AND MO_SUB1.ID_NEGOTIATION_MECHANISM_FK = 1                                                                   "); 
    	builder.append(" 				AND SP_SUB1.SCHEDULE_TYPE in (1941)                                                                           "); 
    	//builder.append(" 				order by 1,2                                                                                                  "); 
    	builder.append(" 		) X                                                                                                                   "); 
    	builder.append(" 	WHERE x.OPERATION_CURRENCY =  A.OPERATION_CURRENCY                                                                        "); 
    	builder.append(" 	AND x.ID_PARTICIPANT_FK = A.ID_INCHARGE_STOCK_PARTICIPANT                                                                 "); 
    	builder.append(" 	AND TRUNC(X.SETTLEMENT_DATE) = A.SETTLEMENT_DATE                                                                          "); 
    	builder.append(" 	) MON_LIQ_MELOR,                                                                                                          "); 
    	builder.append(" 	(SELECT NVL(ROUND(sum(SETTLEMENT_AMOUNT),2),0)                                                                            "); 
    	builder.append(" 	FROM (                                                                                                                    "); 
    	builder.append(" 			SELECT DISTINCT                                                                                                   "); 
    	builder.append(" 				PS_SUB1.ID_PARTICIPANT_FK,                                                                                    "); 
    	builder.append(" 				MO_SUB1.OPERATION_CURRENCY,                                                                                   "); 
    	builder.append(" 				MO_SUB1.ID_MECHANISM_OPERATION_PK,                                                                            "); 
    	builder.append(" 				SO_SUB1.SETTLEMENT_AMOUNT,                                                                                    "); 
    	builder.append(" 				SO_SUB1.SETTLEMENT_DATE                                                                                       "); 
    	builder.append(" 			FROM                                                                                                              "); 
    	builder.append(" 				MECHANISM_OPERATION MO_SUB1,  SETTLEMENT_OPERATION SO_SUB1                                                    "); 
    	builder.append(" 				,SETTLEMENT_PROCESS SP_SUB1 , OPERATION_SETTLEMENT OS_SUB1, PARTICIPANT_SETTLEMENT PS_SUB1                    "); 
    	builder.append(" 			WHERE                                                                                                             "); 
    	builder.append(" 				MO_SUB1.ID_MECHANISM_OPERATION_PK = SO_SUB1.ID_MECHANISM_OPERATION_FK                                         "); 
    	builder.append(" 				AND SP_SUB1.ID_SETTLEMENT_PROCESS_PK = OS_SUB1.ID_SETTLMENT_PROCESS_FK                                        "); 
    	builder.append(" 				AND OS_SUB1.ID_SETTLEMENT_OPERATION_FK = SO_SUB1.ID_SETTLEMENT_OPERATION_PK                                   "); 
    	builder.append(" 				and SO_SUB1.OPERATION_STATE in(615,616)                                                                       "); 
    	builder.append(" 				AND SP_SUB1.SETTLEMENT_SCHEMA = SO_SUB1.SETTLEMENT_SCHEMA                                                     "); 
    	builder.append(" 				AND OS_SUB1.OPERATION_STATE=1288			                                                                  "); 
    	builder.append(" 				AND PS_SUB1.ID_SETTLEMENT_OPERATION_FK = SO_SUB1.ID_SETTLEMENT_OPERATION_PK                                   "); 
    	builder.append(" 				AND SO_SUB1.SETTLEMENT_SCHEMA = 2                                                                             "); 
    	builder.append(" 				AND MO_SUB1.ID_NEGOTIATION_MECHANISM_FK = 1                                                                   "); 
    	builder.append(" 				AND SP_SUB1.SCHEDULE_TYPE in (1938)                                                                           "); 
    	//builder.append(" 				order by 1,2                                                                                                  "); 
    	builder.append(" 		) X                                                                                                                   "); 
    	builder.append(" 	WHERE x.OPERATION_CURRENCY =  A.OPERATION_CURRENCY                                                                        "); 
    	builder.append(" 	AND x.ID_PARTICIPANT_FK = A.ID_INCHARGE_STOCK_PARTICIPANT                                                                 "); 
    	builder.append(" 	AND TRUNC(X.SETTLEMENT_DATE) = A.SETTLEMENT_DATE                                                                          "); 
    	builder.append(" 	) MON_LIQ_MELID,                                                                                                           "); 
    	builder.append(" 	                                                                                                                          "); 
    	
    	builder.append(" (SELECT NVL(ROUND(sum(SETTLEMENT_AMOUNT),2),0)                                                                   ");         
    	builder.append(" 	FROM (                                                                                                        ");            
    	builder.append(" 			SELECT DISTINCT                                                                                       ");            
    	builder.append(" 				PS_SUB1.ID_PARTICIPANT_FK,                                                                        ");            
    	builder.append(" 				MO_SUB1.OPERATION_CURRENCY,                                                                       ");            
    	builder.append(" 				MO_SUB1.ID_MECHANISM_OPERATION_PK,                                                                ");            
    	builder.append(" 				SO_SUB1.SETTLEMENT_AMOUNT,                                                                        ");            
    	builder.append(" 				SO_SUB1.SETTLEMENT_DATE                                                                           ");            
    	builder.append(" 			FROM                                                                                                  ");            
    	builder.append(" 				MECHANISM_OPERATION MO_SUB1,  SETTLEMENT_OPERATION SO_SUB1                                        ");            
    	builder.append(" 				,SETTLEMENT_PROCESS SP_SUB1 , OPERATION_SETTLEMENT OS_SUB1, PARTICIPANT_SETTLEMENT PS_SUB1        ");            
    	builder.append(" 			WHERE                                                                                                 ");            
    	builder.append(" 				MO_SUB1.ID_MECHANISM_OPERATION_PK = SO_SUB1.ID_MECHANISM_OPERATION_FK                             ");            
    	builder.append(" 				AND SP_SUB1.ID_SETTLEMENT_PROCESS_PK = OS_SUB1.ID_SETTLMENT_PROCESS_FK                            ");            
    	builder.append(" 				AND OS_SUB1.ID_SETTLEMENT_OPERATION_FK = SO_SUB1.ID_SETTLEMENT_OPERATION_PK                       ");            
    	builder.append(" 				and SO_SUB1.OPERATION_STATE in(615,616)                                                           ");            
    	builder.append(" 				AND SP_SUB1.SETTLEMENT_SCHEMA = SO_SUB1.SETTLEMENT_SCHEMA                                         ");            
    	builder.append(" 				AND OS_SUB1.OPERATION_STATE=1288                                                                  ");
    	builder.append(" 				AND PS_SUB1.ID_SETTLEMENT_OPERATION_FK = SO_SUB1.ID_SETTLEMENT_OPERATION_PK                       ");            
    	builder.append(" 				AND SO_SUB1.SETTLEMENT_SCHEMA = 2                                                                 ");            
    	builder.append(" 				AND MO_SUB1.ID_NEGOTIATION_MECHANISM_FK = 1                                                       ");            
    	builder.append(" 				AND SP_SUB1.SCHEDULE_TYPE in (1939,1940,1941,1942)                                                          ");
    	builder.append(" 		AND MO_SUB1.ID_SELLER_PARTICIPANT_FK  = MO_SUB1.ID_BUYER_PARTICIPANT_FK                                   ");
    	builder.append(" 		) X                                                                                                       ");            
    	builder.append(" 	WHERE X.OPERATION_CURRENCY =  A.OPERATION_CURRENCY                                                            ");            
    	builder.append(" 	AND X.ID_PARTICIPANT_FK = A.ID_INCHARGE_STOCK_PARTICIPANT                                                     ");            
    	builder.append(" 	AND TRUNC(X.SETTLEMENT_DATE) = A.SETTLEMENT_DATE                                                              ");            
    	builder.append(" 	) OPERACIONES_CRUZADAS                                                                                       ");
    	
    	builder.append(" 	FROM (                                                                                                                    "); 
    	builder.append(" 			SELECT                                                                                                            "); 
    	builder.append(" 			distinct                                                                                                          "); 
    	builder.append(" 			HAO.ID_INCHARGE_STOCK_PARTICIPANT,                                                                                "); 
    	builder.append(" 			SP.SETTLEMENT_DATE,                                                                                               "); 
    	builder.append(" 			MO.OPERATION_CURRENCY,                                                                                            "); 
    	builder.append(" 			NM.MODALITY_CODE,                                                                                                 "); 
    	builder.append(" 			NM.MODALITY_NAME,                                                                                                 "); 
    	builder.append(" 			NM.ID_NEGOTIATION_MODALITY_PK,                                                                                    "); 
    	builder.append(" 			HAO.ROLE,                                                                                                         "); 
    	builder.append(" 			MO.BALLOT_NUMBER ||'/'|| MO.SEQUENTIAL PAPELETA_SECUENCIAL,                                                       "); 
    	builder.append(" 			DECODE(SO.OPERATION_PART,1,MO.ID_BUYER_PARTICIPANT_FK,2,MO.ID_SELLER_PARTICIPANT_FK) ID_BUYER_PARTICIPANT_FK,     "); 
    	builder.append(" 			DECODE(SO.OPERATION_PART,1,MO.ID_SELLER_PARTICIPANT_FK,2,MO.ID_BUYER_PARTICIPANT_FK) ID_SELLER_PARTICIPANT_FK,    "); 
    	builder.append(" 			MO.ID_SECURITY_CODE_FK,                                                                                           "); 
    	builder.append(" 			SO.SETTLEMENT_PRICE PRECIO_UNITARIO,                                                                              "); 
    	builder.append(" 			SO.AMOUNT_RATE,                                                                                                   "); 
    	builder.append(" 			SO.STOCK_QUANTITY CANTIDAD,                                                                                       "); 
    	builder.append(" 			case                                                                                                              "); 
    	builder.append(" 			WHEN HAO.ROLE=1 THEN SO.SETTLEMENT_AMOUNT                                                                         "); 
    	builder.append(" 			END TOTAL1,                                                                                                       "); 
    	builder.append(" 			case                                                                                                              "); 
    	builder.append(" 			WHEN HAO.ROLE=2 THEN SO.SETTLEMENT_AMOUNT                                                                         "); 
    	builder.append(" 			END TOTAL2,                                                                                                       "); 
    	builder.append(" 			DECODE(NVL(SO.SETTLEMENT_DAYS,0),0,NVL(MO.TERM_SETTLEMENT_DAYS,0),SO.SETTLEMENT_DAYS) PLAZO,                      "); 
    	builder.append(" 			DECODE(SO.OPERATION_PART,1,'CONTADO',2,'PLAZO') PARTE,                                                            "); 
    	builder.append(" 			DECODE(SO.IND_PREPAID,1,'ANTICIPADA',' ') || decode (SO.IND_EXTENDED,1,                                           "); 
    	builder.append("  			DECODE(trunc(SO.SETTLEMENT_DATE),trunc(SP.SETTLEMENT_DATE),'AMPLIADA', ' '),' ')OBSERVACIONES                     "); 
    	builder.append(" 			FROM OPERATION_SETTLEMENT OS, SETTLEMENT_PROCESS SP, HOLDER_ACCOUNT_OPERATION HAO, MECHANISM_OPERATION MO,        "); 
    	builder.append(" 				SETTLEMENT_OPERATION SO, SETTLEMENT_ACCOUNT_OPERATION SAO, HOLDER_ACCOUNT HA, NEGOTIATION_MODALITY NM         "); 
    	builder.append(" 	                                                                                                                          "); 
    	builder.append(" 			WHERE SP.ID_SETTLEMENT_PROCESS_PK = OS.ID_SETTLMENT_PROCESS_FK                                                    "); 
    	builder.append(" 			AND OS.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK                                                 "); 
    	builder.append(" 			AND SO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK                                                   "); 
    	builder.append(" 			AND MO.ID_MECHANISM_OPERATION_PK = HAO.ID_MECHANISM_OPERATION_FK                                                  "); 
    	builder.append(" 			AND HAO.ID_HOLDER_ACCOUNT_OPERATION_PK = SAO.ID_HOLDER_ACCOUNT_OPERATION_FK                                       "); 
    	builder.append(" 			AND HAO.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK                                                            "); 
    	builder.append(" 			AND NM.ID_NEGOTIATION_MODALITY_PK = MO.ID_NEGOTIATION_MODALITY_FK                                                 "); 
    	builder.append(" 			AND HAO.OPERATION_PART = SO.OPERATION_PART                                                                        "); 
    	builder.append(" 			AND OS.OPERATION_STATE=1288                                                                                       "); 
    	builder.append(" 			AND SP.SETTLEMENT_SCHEMA=2                                                                                        "); 
    	builder.append(" 			AND HAO.HOLDER_ACCOUNT_STATE= 625                                                                                 "); 
    	builder.append(" 			AND HAO.ROLE = 1                                                                                                  "); 
    	builder.append(" 			AND (TRUNC(SP.SETTLEMENT_DATE) = TO_DATE('"+gfto.getFinalDt()+"','DD/MM/YYYY'))                                   "); 
    	//builder.append(" 			--AND (101 IS NULL OR HAO.ID_INCHARGE_STOCK_PARTICIPANT = 101)                                                    "); 
    	builder.append(" 			AND OPERATION_CURRENCY in (430,127,1734,1853)                                                                     "); 
    	builder.append(" 	                                                                                                                          "); 
    	builder.append(" 			UNION ALL                                                                                                         "); 
    	builder.append(" 	                                                                                                                          "); 
    	builder.append(" 			SELECT                                                                                                            "); 
    	builder.append(" 			distinct                                                                                                          "); 
    	builder.append(" 			HAO.ID_INCHARGE_STOCK_PARTICIPANT,                                                                                "); 
    	builder.append(" 			SP.SETTLEMENT_DATE,                                                                                               "); 
    	builder.append(" 			MO.OPERATION_CURRENCY,                                                                                            "); 
    	builder.append(" 			NM.MODALITY_CODE,                                                                                                 "); 
    	builder.append(" 			NM.MODALITY_NAME,                                                                                                 "); 
    	builder.append(" 			NM.ID_NEGOTIATION_MODALITY_PK,                                                                                    "); 
    	builder.append(" 			HAO.ROLE,                                                                                                         "); 
    	builder.append(" 			MO.BALLOT_NUMBER ||'/'|| MO.SEQUENTIAL PAPELETA_SECUENCIAL,                                                       "); 
    	builder.append(" 			DECODE(SO.OPERATION_PART,1,MO.ID_BUYER_PARTICIPANT_FK,2,MO.ID_SELLER_PARTICIPANT_FK) ID_BUYER_PARTICIPANT_FK,     "); 
    	builder.append(" 			DECODE(SO.OPERATION_PART,1,MO.ID_SELLER_PARTICIPANT_FK,2,MO.ID_BUYER_PARTICIPANT_FK) ID_SELLER_PARTICIPANT_FK,    "); 
    	builder.append(" 			MO.ID_SECURITY_CODE_FK,                                                                                           "); 
    	builder.append(" 			SO.SETTLEMENT_PRICE PRECIO_UNITARIO,                                                                              "); 
    	builder.append(" 			SO.AMOUNT_RATE,                                                                                                   "); 
    	builder.append(" 			SO.STOCK_QUANTITY CANTIDAD,                                                                                       "); 
    	builder.append(" 			case                                                                                                              "); 
    	builder.append(" 			WHEN HAO.ROLE=1 THEN SO.SETTLEMENT_AMOUNT                                                                         "); 
    	builder.append(" 			END TOTAL1,                                                                                                       "); 
    	builder.append(" 			case                                                                                                              "); 
    	builder.append(" 			WHEN HAO.ROLE=2 THEN SO.SETTLEMENT_AMOUNT                                                                         "); 
    	builder.append(" 			END TOTAL2,                                                                                                       "); 
    	builder.append(" 			DECODE(NVL(SO.SETTLEMENT_DAYS,0),0,NVL(MO.TERM_SETTLEMENT_DAYS,0),SO.SETTLEMENT_DAYS) PLAZO,                      "); 
    	builder.append(" 			DECODE(SO.OPERATION_PART,1,'CONTADO',2,'PLAZO') PARTE,                                                            "); 
    	builder.append(" 			DECODE(SO.IND_PREPAID,1,'ANTICIPADA',' ') || decode (SO.IND_EXTENDED,1,                                           "); 
    	builder.append(" 			DECODE(trunc(SO.SETTLEMENT_DATE),trunc(SP.SETTLEMENT_DATE),'AMPLIADA', ' '),' ')OBSERVACIONES                     "); 
    	builder.append(" 	                                                                                                                          "); 
    	builder.append(" 			FROM OPERATION_SETTLEMENT OS, SETTLEMENT_PROCESS SP, HOLDER_ACCOUNT_OPERATION HAO, MECHANISM_OPERATION MO,        "); 
    	builder.append(" 			SETTLEMENT_OPERATION SO, SETTLEMENT_ACCOUNT_OPERATION SAO, HOLDER_ACCOUNT HA, NEGOTIATION_MODALITY NM             "); 
    	builder.append(" 	                                                                                                                          "); 
    	builder.append(" 			WHERE SP.ID_SETTLEMENT_PROCESS_PK = OS.ID_SETTLMENT_PROCESS_FK                                                    "); 
    	builder.append(" 			AND OS.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK                                                 "); 
    	builder.append(" 			AND SO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK                                                   "); 
    	builder.append(" 			AND MO.ID_MECHANISM_OPERATION_PK = HAO.ID_MECHANISM_OPERATION_FK                                                  "); 
    	builder.append(" 			AND HAO.ID_HOLDER_ACCOUNT_OPERATION_PK = SAO.ID_HOLDER_ACCOUNT_OPERATION_FK                                       "); 
    	builder.append(" 			AND HAO.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK                                                            "); 
    	builder.append(" 			AND NM.ID_NEGOTIATION_MODALITY_PK = MO.ID_NEGOTIATION_MODALITY_FK                                                 "); 
    	builder.append(" 			AND HAO.OPERATION_PART = SO.OPERATION_PART                                                                        "); 
    	builder.append(" 			AND OS.OPERATION_STATE=1288                                                                                       "); 
    	builder.append(" 			AND SP.SETTLEMENT_SCHEMA=2                                                                                        "); 
    	builder.append(" 			AND HAO.HOLDER_ACCOUNT_STATE= 625 			                                                                      "); 
    	builder.append(" 			AND HAO.ROLE = 2                                                                                                  "); 
    	builder.append(" 			AND (TRUNC(SP.SETTLEMENT_DATE) = TO_DATE('"+gfto.getFinalDt()+"','DD/MM/YYYY'))                                   "); 
    	//builder.append(" 			-- AND (101 IS NULL OR HAO.ID_INCHARGE_STOCK_PARTICIPANT = 101)                                                   "); 
    	builder.append(" 			AND OPERATION_CURRENCY in (430,127,1734,1853)                                                                     "); 
    	builder.append(" 	                                                                                                                          "); 
    	builder.append(" 		)A                                                                                                                    "); 
    	builder.append(" 		group by A.ID_INCHARGE_STOCK_PARTICIPANT,A.OPERATION_CURRENCY,A.SETTLEMENT_DATE                                       "); 
    	builder.append(" 		)r ");
    	builder.append(" 		order by MONEDA, 3                                                          "); 
    	
        Query query = em.createNativeQuery(builder.toString());
        
        List<Object[]> resultList = query.getResultList();
    	return resultList;
    }
    
    /**
     * QUERY PARA GENERAR EL REPORTE 5 DE ASFI
     * Solo necesitamos fechaInicio y fechaFin para generar el reporte
     * @param gfto
     * @return
     */
    public List<Object[]> getListCompensationSettlements(GenericsFiltersTO gfto){
    	StringBuilder builder= new StringBuilder();
        
    	builder.append(" 	select                                                                                                                       "); 
    	builder.append(" 		'EDB',                                                                                                                   "); 
    	builder.append(" 		TO_CHAR(A.SETTLEMENT_DATE,'YYYY-MM-DD') FECHA,                                                                           "); 
    	builder.append(" 		(SELECT MNEMONIC FROM PARTICIPANT WHERE ID_PARTICIPANT_PK = A.ID_INCHARGE_STOCK_PARTICIPANT) ENTIDAD,                    "); 
    	builder.append(" 	( SELECT TEXT1 FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = A.SECURITY_CLASS ) SECURITY_CLASS,                            "); 
    	builder.append(" 		CASE  WHEN A.CURRENCY = 127  THEN '10'                                                                                "); 
    	builder.append(" 			WHEN A.CURRENCY = 430  THEN '12'                                                                                  "); 
    	builder.append(" 			WHEN A.CURRENCY = 1853 THEN '11'                                                                                     "); 
    	builder.append(" 			WHEN A.CURRENCY = 1734 THEN '14'                                                                                   "); 
    	builder.append(" 			WHEN A.CURRENCY = 1304 THEN '15'                                                                                     "); 
    	builder.append(" 			END MONEDAOP,                                                                                                        "); 
    	builder.append(" 		A.CURRENCY MONEDA,                                                                                                       "); 
    	builder.append(" 		NVL(sum(A.TOTAL1),0) COMPRA,                                                                                             "); 
    	builder.append(" 		NVL(sum(A.TOTAL2),0) VENTA,                                                                                              "); 
    	builder.append(" 	                                                                                                                             "); 
    	builder.append(" 	(SELECT NVL(SUM (MFV.TOTAL_BALANCE),0)                                                                                           "); 
    	builder.append(" 		FROM MARKET_FACT_VIEW MFV                                                                                                "); 
    	builder.append(" 		INNER JOIN SECURITY SEC ON MFV.ID_SECURITY_CODE_PK = SEC.ID_SECURITY_CODE_PK                                             "); 
    	builder.append(" 		WHERE MFV.TOTAL_BALANCE > 0                                                                                              "); 
    	builder.append(" 		AND MFV.ID_PARTICIPANT_PK = A.ID_INCHARGE_STOCK_PARTICIPANT                                                              "); 
    	builder.append(" 		AND SEC.CURRENCY = A.CURRENCY								                                                              ");
    	builder.append(" 		AND MFV.CUT_DATE = TO_DATE('"+gfto.getFinalDt()+"','DD/MM/YYYY')                                                         "); 
    	builder.append(" 		AND SEC.SECURITY_CLASS = A.SECURITY_CLASS) FINALDIA,                                                                     "); 
    	builder.append(" 		                                                                                                                         "); 
    	builder.append(" 		(SELECT NVL(SUM (MFV.TOTAL_BALANCE),0)                                                                                          "); 
    	builder.append(" 		FROM MARKET_FACT_VIEW MFV                                                                                                "); 
    	builder.append(" 		INNER JOIN SECURITY SEC ON MFV.ID_SECURITY_CODE_PK = SEC.ID_SECURITY_CODE_PK                                             "); 
    	builder.append(" 		WHERE MFV.TOTAL_BALANCE > 0                                                                                              "); 
    	builder.append(" 		AND MFV.ID_PARTICIPANT_PK = A.ID_INCHARGE_STOCK_PARTICIPANT                                                              ");
    	builder.append(" 		AND SEC.CURRENCY = A.CURRENCY								                                                              ");
    	builder.append(" 		AND MFV.CUT_DATE = (TO_DATE('"+gfto.getInitialDt()+"','DD/MM/YYYY'))                                                       "); 
    	builder.append(" 		AND SEC.SECURITY_CLASS = A.SECURITY_CLASS) INICIODIA                                                                     "); 
    	builder.append(" 	                                                                                                                             "); 
    	builder.append(" 	FROM (                                                                                                                       "); 
    	builder.append(" 	                                                                                                                             "); 
    	builder.append(" 	SELECT                                                                                                                       "); 
    	builder.append(" 				distinct                                                                                                         "); 
    	builder.append(" 		SEC.SECURITY_CLASS,                                                                                                      "); 
    	builder.append(" 				HAO.ID_INCHARGE_STOCK_PARTICIPANT,                                                                               "); 
    	builder.append(" 				SP.SETTLEMENT_DATE,                                                                                              "); 
    	builder.append(" 				SEC.CURRENCY,                                                                                                    "); 
    	builder.append(" 				                                                                                                                 "); 
    	builder.append(" 		NM.MODALITY_CODE,                                                                                                        "); 
    	builder.append(" 				NM.MODALITY_NAME,                                                                                                "); 
    	builder.append(" 				NM.ID_NEGOTIATION_MODALITY_PK,                                                                                   "); 
    	builder.append(" 				HAO.ROLE,                                                                                                        "); 
    	builder.append(" 				MO.BALLOT_NUMBER ||'/'|| MO.SEQUENTIAL PAPELETA_SECUENCIAL,                                                      "); 
    	builder.append(" 		case                                                                                                                     "); 
    	builder.append(" 				WHEN HAO.ROLE=1 THEN SO.STOCK_QUANTITY                                                                           "); 
    	builder.append(" 				END TOTAL1,                                                                                                      "); 
    	builder.append(" 				case                                                                                                             "); 
    	builder.append(" 				WHEN HAO.ROLE=2 THEN SO.STOCK_QUANTITY                                                                           "); 
    	builder.append(" 				END TOTAL2,                                                                                                      "); 
    	builder.append(" 				DECODE(NVL(SO.SETTLEMENT_DAYS,0),0,NVL(MO.TERM_SETTLEMENT_DAYS,0),SO.SETTLEMENT_DAYS) PLAZO,                     "); 
    	builder.append(" 				DECODE(SO.OPERATION_PART,1,'CONTADO',2,'PLAZO') PARTE,                                                           "); 
    	builder.append(" 				DECODE(SO.IND_PREPAID,1,'ANTICIPADA',' ') || decode (SO.IND_EXTENDED,1,                                          "); 
    	builder.append(" 				DECODE(trunc(SO.SETTLEMENT_DATE),trunc(SP.SETTLEMENT_DATE),'AMPLIADA', ' '),' ')OBSERVACIONES                    "); 
    	builder.append(" 				FROM OPERATION_SETTLEMENT OS, SETTLEMENT_PROCESS SP, HOLDER_ACCOUNT_OPERATION HAO, MECHANISM_OPERATION MO,       "); 
    	builder.append(" 					SETTLEMENT_OPERATION SO, SETTLEMENT_ACCOUNT_OPERATION SAO, HOLDER_ACCOUNT HA, NEGOTIATION_MODALITY NM ,      "); 
    	builder.append(" 			SECURITY SEC                                                                                                         "); 
    	builder.append(" 																																 "); 
    	builder.append(" 				WHERE SP.ID_SETTLEMENT_PROCESS_PK = OS.ID_SETTLMENT_PROCESS_FK                                                   "); 
    	builder.append(" 				AND OS.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK                                                "); 
    	builder.append(" 				AND SO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK                                                  "); 
    	builder.append(" 				AND MO.ID_MECHANISM_OPERATION_PK = HAO.ID_MECHANISM_OPERATION_FK                                                 "); 
    	builder.append(" 				AND HAO.ID_HOLDER_ACCOUNT_OPERATION_PK = SAO.ID_HOLDER_ACCOUNT_OPERATION_FK                                      "); 
    	builder.append(" 				AND HAO.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK                                                           "); 
    	builder.append(" 				AND NM.ID_NEGOTIATION_MODALITY_PK = MO.ID_NEGOTIATION_MODALITY_FK                                                "); 
    	builder.append(" 		AND MO.ID_SECURITY_CODE_FK = SEC.ID_SECURITY_CODE_PK                                                                     "); 
    	builder.append(" 				AND HAO.OPERATION_PART = SO.OPERATION_PART                                                                       "); 
    	builder.append(" 				AND OS.OPERATION_STATE=1288                                                                                      "); 
    	builder.append(" 				AND SP.SETTLEMENT_SCHEMA=2                                                                                       "); 
    	builder.append(" 				AND HAO.HOLDER_ACCOUNT_STATE= 625 				                                                                 "); 
    	builder.append(" 				AND HAO.ROLE = 1                                                                                                 "); 
    	builder.append(" 				AND (TRUNC(SP.SETTLEMENT_DATE) = TO_DATE('"+gfto.getDate()+"','DD/MM/YYYY'))                                  "); 
    	//builder.append(" 				AND HAO.ID_INCHARGE_STOCK_PARTICIPANT = 101                                                                      "); 
    	builder.append(" 				AND OPERATION_CURRENCY in (430,127,1734,1853)                                                                    "); 
    	builder.append(" 																																 "); 
    	builder.append(" 				UNION ALL                                                                                                        "); 
    	builder.append(" 																																 "); 
    	builder.append(" 				SELECT                                                                                                           "); 
    	builder.append(" 				distinct                                                                                                         "); 
    	builder.append(" 		SEC.SECURITY_CLASS ,                                                                                                     "); 
    	builder.append(" 				HAO.ID_INCHARGE_STOCK_PARTICIPANT,                                                                               "); 
    	builder.append(" 				SP.SETTLEMENT_DATE,                                                                                              "); 
    	builder.append(" 				SEC.CURRENCY,                                                                                                    "); 
    	builder.append(" 				NM.MODALITY_CODE,                                                                                                "); 
    	builder.append(" 				NM.MODALITY_NAME,                                                                                                "); 
    	builder.append(" 				NM.ID_NEGOTIATION_MODALITY_PK,                                                                                   "); 
    	builder.append(" 				HAO.ROLE,                                                                                                        "); 
    	builder.append(" 				MO.BALLOT_NUMBER ||'/'|| MO.SEQUENTIAL PAPELETA_SECUENCIAL,                                                      "); 
    	builder.append(" 		case                                                                                                                     "); 
    	builder.append(" 				WHEN HAO.ROLE=1 THEN SO.STOCK_QUANTITY                                                                           "); 
    	builder.append(" 				END TOTAL1,                                                                                                      "); 
    	builder.append(" 				case                                                                                                             "); 
    	builder.append(" 				WHEN HAO.ROLE=2 THEN SO.STOCK_QUANTITY                                                                           "); 
    	builder.append(" 				END TOTAL2,                                                                                                      "); 
    	builder.append(" 				DECODE(NVL(SO.SETTLEMENT_DAYS,0),0,NVL(MO.TERM_SETTLEMENT_DAYS,0),SO.SETTLEMENT_DAYS) PLAZO,                     "); 
    	builder.append(" 				DECODE(SO.OPERATION_PART,1,'CONTADO',2,'PLAZO') PARTE,                                                           "); 
    	builder.append(" 				DECODE(SO.IND_PREPAID,1,'ANTICIPADA',' ') || decode (SO.IND_EXTENDED,1,                                          "); 
    	builder.append(" 				DECODE(trunc(SO.SETTLEMENT_DATE),trunc(SP.SETTLEMENT_DATE),'AMPLIADA', ' '),' ')OBSERVACIONES                    "); 
    	builder.append(" 																																 "); 
    	builder.append(" 				FROM OPERATION_SETTLEMENT OS, SETTLEMENT_PROCESS SP, HOLDER_ACCOUNT_OPERATION HAO, MECHANISM_OPERATION MO,       "); 
    	builder.append(" 				SETTLEMENT_OPERATION SO, SETTLEMENT_ACCOUNT_OPERATION SAO, HOLDER_ACCOUNT HA, NEGOTIATION_MODALITY NM,           ");  
    	builder.append(" 		SECURITY SEC                                                                                                             "); 
    	builder.append(" 				WHERE SP.ID_SETTLEMENT_PROCESS_PK = OS.ID_SETTLMENT_PROCESS_FK                                                   "); 
    	builder.append(" 				AND OS.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK                                                "); 
    	builder.append(" 				AND SO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK                                                  "); 
    	builder.append(" 				AND MO.ID_MECHANISM_OPERATION_PK = HAO.ID_MECHANISM_OPERATION_FK                                                 "); 
    	builder.append(" 				AND HAO.ID_HOLDER_ACCOUNT_OPERATION_PK = SAO.ID_HOLDER_ACCOUNT_OPERATION_FK                                      "); 
    	builder.append(" 				AND HAO.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK                                                           "); 
    	builder.append(" 				AND NM.ID_NEGOTIATION_MODALITY_PK = MO.ID_NEGOTIATION_MODALITY_FK                                                "); 
    	builder.append(" 		AND MO.ID_SECURITY_CODE_FK = SEC.ID_SECURITY_CODE_PK                                                                     "); 
    	builder.append(" 				AND HAO.OPERATION_PART = SO.OPERATION_PART                                                                       "); 
    	builder.append(" 				AND OS.OPERATION_STATE=1288                                                                                      "); 
    	builder.append(" 				AND SP.SETTLEMENT_SCHEMA=2                                                                                       "); 
    	builder.append(" 				AND HAO.HOLDER_ACCOUNT_STATE= 625 				                                                                 "); 
    	builder.append(" 				AND HAO.ROLE = 2                                                                                                 "); 
    	builder.append(" 				AND (TRUNC(SP.SETTLEMENT_DATE) = TO_DATE('"+gfto.getDate()+"','DD/MM/YYYY'))                                     "); 
    	//builder.append(" 				AND HAO.ID_INCHARGE_STOCK_PARTICIPANT = 101                                                                      "); 
    	builder.append(" 				AND OPERATION_CURRENCY in (430,127,1734,1853)                                                                    "); 
    	builder.append(" 	)A                                                                                                                           "); 
    	builder.append(" 	group by A.ID_INCHARGE_STOCK_PARTICIPANT,A.CURRENCY,A.SECURITY_CLASS,A.SETTLEMENT_DATE                                       "); 
    	builder.append(" 	order by A.CURRENCY,3,4                                                                          "); 
    	
        Query query = em.createNativeQuery(builder.toString());
        
        List<Object[]> resultList = query.getResultList();
    	return resultList;
    }
    
    /**
     * QUERY PARA GENERAR EL REPORTE 3 DE ASFI
     * Solo necesitamos las fechas de inicio y fin para generar el reporte
     * @param gfto
     * @return
     */
    public List<Object[]> getAcquisitionInPrimaryMarketBBVobj(GenericsFiltersTO gfto){
    	StringBuilder builder= new StringBuilder();
    	
    	Date fechaFinal=CommonsUtilities.convertStringtoDate(gfto.getFinalDt());
    	String fechaFinalFormatoAsfi=CommonsUtilities.convertDateToString(fechaFinal, "yyyy-MM-dd");
    	
		builder.append("	SELECT");
        builder.append("	CPY, FECHA, EMISOR, CLASE, CLAVE, EMISION, TIPO_PERSONA, DOCUMENTO, ACTIVIDAD_EC, CANTIDAD, VAL_NOMINAL, PRECIO, MONEDAOP, NRO_OPERACION FROM"); 
		builder.append("	(");
        builder.append("	select 'CPY' as CPY");                                                                                                                                               
		builder.append("		,TO_CHAR('"+fechaFinalFormatoAsfi+"') FECHA");                                                                                                                                   
		builder.append("		,ISS.MNEMONIC EMISOR");                                                                                                                                      
		builder.append("		,(SELECT TEXT1 FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = SECU.SECURITY_CLASS )CLASE");                                                             
		builder.append("		,secu.ID_SECURITY_CODE_ONLY CLAVE");                                                                                                                        
		builder.append("		,TO_CHAR(TO_DATE(SECU.ISSUANCE_DATE),'YYYY-MM-DD') EMISION");
		builder.append("		,(select pt.TEXT2 from PARAMETER_TABLE pt where pt.PARAMETER_TABLE_PK = ha.ACCOUNT_TYPE) TIPO_PERSONA");
		builder.append("		,(select h1.DOCUMENT_NUMBER from HOLDER h1 where h1.ID_HOLDER_PK = had.ID_HOLDER_FK) DOCUMENTO");
		builder.append("		,(select pt.INDICATOR2 from PARAMETER_TABLE pt where pt.PARAMETER_TABLE_PK = h.ECONOMIC_ACTIVITY) ACTIVIDAD_EC");
		builder.append("		,hao.STOCK_QUANTITY CANTIDAD");
		builder.append("		,(CASE WHEN secu.SECURITY_CLASS = 1945");                           
		builder.append("			   THEN (SELECT DISTINCT NVH.NOMINAL_VALUE");                         
		builder.append("					 FROM NOMINAL_VALUE_HISTORY_OLD NVH");                             
		builder.append("					 WHERE TRUNC(SO.SETTLEMENT_DATE) = NVH.CUT_DATE");             
		builder.append("					 AND NVH.ID_SECURITY_CODE_FK = secu.ID_SECURITY_CODE_PK)");  
		builder.append("			   ELSE secu.INITIAL_NOMINAL_VALUE END) VAL_NOMINAL");
		builder.append("		,TO_NUMBER (SO.SETTLEMENT_PRICE) AS PRECIO");                                                                                                              
		builder.append("		,CASE WHEN mo.OPERATION_CURRENCY = 127  THEN '10'");                                                                                                    
		builder.append("			  WHEN mo.OPERATION_CURRENCY = 430  THEN '12'");                                                                                                    
		builder.append("			  WHEN mo.OPERATION_CURRENCY = 1853 THEN '11'");                                                                                                       
		builder.append("			  WHEN mo.OPERATION_CURRENCY = 1734 THEN '14'");                                                                                                     
		builder.append("			  WHEN mo.OPERATION_CURRENCY = 1304 THEN '15'");                                                                                                       
		builder.append("		 END MONEDAOP");
		builder.append("		,mo.ID_MECHANISM_OPERATION_PK NRO_OPERACION");
		builder.append("	from HOLDER_ACCOUNT_OPERATION hao");
		builder.append("		join MECHANISM_OPERATION mo on hao.ID_MECHANISM_OPERATION_FK = mo.ID_MECHANISM_OPERATION_PK");
		builder.append("		join HOLDER_ACCOUNT ha on hao.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK");
		builder.append("		join HOLDER_ACCOUNT_DETAIL had on ha.ID_HOLDER_ACCOUNT_PK = had.ID_HOLDER_ACCOUNT_FK");
		builder.append("		join HOLDER h on had.ID_HOLDER_FK = h.ID_HOLDER_PK");
		builder.append("		,SETTLEMENT_OPERATION SO");
		builder.append("		,SETTLEMENT_PROCESS SP");
		builder.append("		,OPERATION_SETTLEMENT OS");
		builder.append("		,PARTICIPANT_SETTLEMENT PS");
		builder.append("		,SECURITY secu");
		builder.append("		,ISSUER iss");
		builder.append("	where 1=1");
		builder.append("		and PS.ROLE = 1");
		builder.append("		and mo.ID_MECHANISM_OPERATION_PK = SO.ID_MECHANISM_OPERATION_FK");                                                                                                 
		builder.append("		and SP.ID_SETTLEMENT_PROCESS_PK = OS.ID_SETTLMENT_PROCESS_FK");                                                                                                
		builder.append("		and OS.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK");                                                                                           
		builder.append("		and SO.OPERATION_STATE in(615,616)");                                                                                                                          
		builder.append("		and SP.SETTLEMENT_DATE = SO.SETTLEMENT_DATE");                                                                                                                 
		builder.append("		and SP.SETTLEMENT_SCHEMA = SO.SETTLEMENT_SCHEMA");                                                                                                             
		builder.append("		and SP.CURRENCY = SO.SETTLEMENT_CURRENCY");                                                                                                                    
		builder.append("		and SP.ID_NEGOTIATION_MECHANISM_FK = mo.ID_NEGOTIATION_MECHANISM_FK");                                                                                         
		builder.append("		and OS.OPERATION_STATE = 1288");                                                                                                                                 
		builder.append("		and mo.ID_SECURITY_CODE_FK = secu.ID_SECURITY_CODE_PK");                                                                                                         
		builder.append("		and PS.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK");                                                                                           
		builder.append("		and SECU.ID_ISSUER_FK = ISS.ID_ISSUER_PK");                                                                                                                    
		builder.append("		and SO.SETTLEMENT_SCHEMA = 2");                                                                                                                                
		builder.append("		and mo.ID_NEGOTIATION_MECHANISM_FK = 1");                                                                                                                        
		builder.append("		and mo.ID_NEGOTIATION_MODALITY_FK in (12,13)");          
		builder.append("		and TRUNC(SO.SETTLEMENT_DATE) BETWEEN TO_DATE('"+gfto.getInitialDt()+"','DD/MM/YYYY') AND TO_DATE('"+gfto.getFinalDt()+"','DD/MM/YYYY')"); 
		builder.append("	union all");
		builder.append("	select 'CPY' as CPY");                                                                                                                                               
		builder.append("		,TO_CHAR('"+fechaFinalFormatoAsfi+"') FECHA");                                                                                                                                      
		builder.append("		,I.MNEMONIC EMISOR");                                                                                                                               
		builder.append("		,(SELECT TEXT1 FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = S.SECURITY_CLASS )CLASE");                                                                       
		builder.append("		,S.ID_SECURITY_CODE_ONLY CLAVE");                                                                                                                                     
		builder.append("		,TO_CHAR(TO_DATE(S.ISSUANCE_DATE),'YYYY-MM-DD') EMISION");                                                                                                                
		builder.append("		,(SELECT TEXT2 FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = HA.ACCOUNT_TYPE) TIPO_PERSONA");                                                            
		builder.append("		,(SELECT LISTAGG((select h.DOCUMENT_NUMBER from holder h where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK), ', ') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)");                
		builder.append("		  FROM HOLDER_ACCOUNT_DETAIL HAD");                                                                                                                          
		builder.append("		  WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK"); 				                                                                                
		builder.append("		 ) DOCUMENTO");                                                                                                                                                 
		builder.append("		,(SELECT (SELECT INDICATOR2 FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = H.ECONOMIC_ACTIVITY)");                                                               
		builder.append("			  FROM HOLDER_ACCOUNT_DETAIL HAD");                                                                                                                          
		builder.append("			  INNER JOIN HOLDER H ON HAD.ID_HOLDER_FK = H.ID_HOLDER_PK");                                                                                                
		builder.append("			  WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK"); 			                                                                                    
		builder.append("			  AND HA.ACCOUNT_TYPE = 107");                                                                                                                               
		builder.append("		 )  ACTIVIDAD_EC");                                                                                                                                             
		builder.append("		,AAO.TOTAL_BALANCE CANTIDAD");                                                                                                                                  
		builder.append("		,(CASE WHEN S.SECURITY_CLASS = 1945");                          
		builder.append("		  THEN");                                                       
		builder.append("			( SELECT DISTINCT NVH.NOMINAL_VALUE");                      
		builder.append("			  FROM NOMINAL_VALUE_HISTORY_OLD NVH");                         
		builder.append("			  WHERE TRUNC(CO.OPERATION_DATE) = NVH.CUT_DATE");          
		builder.append("			  AND NVH.ID_SECURITY_CODE_FK    = S.ID_SECURITY_CODE_PK)");
		builder.append("		  ELSE S.INITIAL_NOMINAL_VALUE");                                  
		builder.append("		  END) VAL_NOMINAL");												
		builder.append("		,(CASE WHEN S.SECURITY_CLASS = 1945 or (SELECT DISTINCT ROUND(NVL(AAM.MARKET_PRICE,0),2)");                             
		builder.append("												FROM ACCOUNT_ANNOTATION_MARKETFACT AAM");                     
		builder.append("												WHERE AAM.ID_ANNOTATION_OPERATION_FK = AAO.ID_ANNOTATION_OPERATION_PK) = 0");                                     
		builder.append("		  THEN");                                                                   
		builder.append("			( SELECT DISTINCT ROUND(NVH.NOMINAL_VALUE,2)");                                  
		builder.append("			  FROM NOMINAL_VALUE_HISTORY_OLD NVH");                                     
		builder.append("			  WHERE TRUNC(CO.OPERATION_DATE) = NVH.CUT_DATE");                      
		builder.append("			  AND NVH.ID_SECURITY_CODE_FK    = S.ID_SECURITY_CODE_PK)");            
		builder.append("		  ELSE");                                                                   
		builder.append("			( SELECT DISTINCT ROUND(NVL(AAM.MARKET_PRICE,0),2)");                             
		builder.append("			  FROM ACCOUNT_ANNOTATION_MARKETFACT AAM");                               
		builder.append("			  WHERE AAM.ID_ANNOTATION_OPERATION_FK = AAO.ID_ANNOTATION_OPERATION_PK )");
		builder.append("		  END) PRECIO");
		builder.append("		,CASE WHEN S.CURRENCY = 127  THEN '10'");                                                                                                                     
		builder.append("			  WHEN S.CURRENCY = 430  THEN '12'");                                                                                                                     
		builder.append("			  WHEN S.CURRENCY = 1853 THEN '11'");                                                                                                                        
		builder.append("			  WHEN S.CURRENCY = 1734 THEN '14'");                                                                                                                      
		builder.append("			  WHEN S.CURRENCY = 1304 THEN '15'");                                                                                                                        
		builder.append("		 END MONEDAOP");                                                                                                                                                  
		builder.append("		,ROWNUM NRO_OPERACION");    
		builder.append("  	from ACCOUNT_ANNOTATION_OPERATION AAO");                                                                                                                           
		builder.append("	  	join CUSTODY_OPERATION CO ON CO.ID_CUSTODY_OPERATION_PK = AAO.ID_ANNOTATION_OPERATION_PK");                                                                
		builder.append("	  	join SECURITY S ON AAO.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK");                                                                                      
		builder.append("	  	join ISSUER I ON I.ID_ISSUER_PK = S.ID_ISSUER_FK");                                                                                                        
		builder.append("	  	join HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK = AAO.ID_HOLDER_ACCOUNT_FK");                                                                         
		builder.append("  	where 1=1");
		builder.append("	  	and AAO.STATE_ANNOTATION = 858");                                                                                                                             
		builder.append("	  	and AAO.ID_ANNOTATION_OPERATION_PK not between 328294 and 329043");                                                                                          
		builder.append("	  	and TRUNC(CO.OPERATION_DATE) between TO_DATE('"+gfto.getInitialDt()+"','DD/MM/YYYY') AND TO_DATE('"+gfto.getFinalDt()+"','DD/MM/YYYY')");
		builder.append("	order by 5,14,10");
		builder.append(")	");	    	
    	
        Query query = em.createNativeQuery(builder.toString());
        
        List<Object[]> resultList = query.getResultList();
    	return resultList;
    }
    
    /*Rep 286*/
    public List<SecuritySettlementTO>getSecuritieSettlementBBV(GenericsFiltersTO gfto){
    	StringBuilder builder= new StringBuilder();
    	builder.append(" SELECT MO.ID_SELLER_PARTICIPANT_FK, "); //0
    	builder.append(" Mo.Id_Buyer_Participant_Fk,");          //1
    	builder.append(" SE.ID_ISSUER_FK, ");//2
    	builder.append(" (select ISS.MNEMONIC from ISSUER ISS where ISS.ID_ISSUER_PK=SE.ID_ISSUER_FK) AS ISSUER_MNEMONIC, ");//2
    	builder.append(" SE.SECURITY_CLASS as SECURITY_CLASS, ");//3
    	builder.append(" Se.Instrument_Type AS INSTRUMENT_TYPE,  ");//4
        builder.append(" SE.CURRENT_NOMINAL_VALUE AS NOMINAL_VALUE, ");//5
        builder.append(" MO.CASH_PRICE AS ACQUISITION_PRICE, ");//6
        builder.append(" SE.CURRENCY AS CURRENCY ");//7
        builder.append(" FROM MECHANISM_OPERATION MO, ");
        builder.append(" MECHANISM_OPERATION MO_REF, ");
        builder.append(" SECURITY SE, ISSUANCE ISS ");
        builder.append(" WHERE MO_REF.ID_REFERENCE_OPERATION_FK (+) = MO.ID_MECHANISM_OPERATION_PK ");
        builder.append(" AND MO.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK ");
        builder.append(" AND MO.ID_NEGOTIATION_MECHANISM_FK = 1 ");
        builder.append(" AND ISS.ID_ISSUANCE_CODE_PK = SE.ID_ISSUANCE_CODE_FK ");
        builder.append(" AND SE.SECURITY_CLASS ! = 420 ");
        builder.append(" AND trunc(mo.OPERATION_DATE) = to_date('"+gfto.getFinalDt()+"','dd/MM/yyyy') ");
        builder.append(" ORDER BY SE.ID_SECURITY_CODE_PK ");
    	
    	Query query = em.createNativeQuery(builder.toString());
    	List<Object[]> list = query.getResultList();
    	SecuritySettlementTO securitySettlementTO;
    	List<SecuritySettlementTO> listTOs = new ArrayList<>();
    	for (Object[] object : list) {
    		securitySettlementTO = new SecuritySettlementTO();
    		securitySettlementTO.setIdParticipantSellerPk(Long.parseLong(object[0].toString()));
    		securitySettlementTO.setIdParticipantBuyerPk(Long.parseLong(object[1].toString()));
    		securitySettlementTO.setIdIssuerPk(object[2].toString());
    		securitySettlementTO.setIssuerMnemonic(object[3].toString());
    		securitySettlementTO.setSecurityClass(Integer.parseInt(object[4].toString()));
    		securitySettlementTO.setInstrumentType(Integer.parseInt(object[5].toString()));
    		securitySettlementTO.setNominalValue(object[6]!=null?new BigDecimal(object[6].toString()):null);
    		securitySettlementTO.setAcquisitionPrice(object[7]!=null?new BigDecimal(object[7].toString()):null);
    		securitySettlementTO.setCurrency(Integer.parseInt(object[8].toString()));
    		listTOs.add(securitySettlementTO);
		}
		return listTOs;
    }
    
    public String getText1ParameterTable(Integer idParameterTablePk){
		ParameterTable parameterTable = em.find(ParameterTable.class, idParameterTablePk);
		return parameterTable.getText1();
	}
   

    /**
     * Query del reporte Registro de asignaciones de cuentas
     * @param registrationAssignmentHoldersTO
     * @return
     */
    
    public String getQueryRegistrationAssignment(RegistrationAssignmentHoldersTO registrationAssignmentHoldersTO){
    	StringBuilder sbQuery= new StringBuilder();
    	
    	sbQuery.append(" 	select	 ");
    	sbQuery.append(" 	mo.operation_number,	 ");
    	sbQuery.append(" 	mo.operation_date,	 ");
    	sbQuery.append(" 	hao.ID_INCHARGE_FUNDS_PARTICIPANT,	 ");
    	sbQuery.append(" 	p.MNEMONIC PARTICIPANTE_FUN,	 ");
    	sbQuery.append(" 	hao.ID_INCHARGE_STOCK_PARTICIPANT,	 ");
    	sbQuery.append(" 	p2.MNEMONIC PARTICIPANTE_STO,	 ");
    	sbQuery.append(" 	mo.ballot_number || '/' ||mo.sequential papeleta_secuencial,	 ");
    	sbQuery.append(" 	mo.ballot_number papeleta,	 ");
    	sbQuery.append(" 	mo.sequential secuencial,	 ");
    	sbQuery.append(" 	nmo.modality_name modalidad,	 ");
    	sbQuery.append(" 	HA.ACCOUNT_NUMBER,	 ");
    	sbQuery.append(" 	(SELECT NM.MODALITY_CODE FROM NEGOTIATION_MODALITY NM WHERE NM.ID_NEGOTIATION_MODALITY_PK = MO.ID_NEGOTIATION_MODALITY_FK) modalidad_nemonico,	 ");
    	sbQuery.append(" 	Nme.Mechanism_Name,	 ");
    	sbQuery.append(" 	decode(hao.role, 1 , 'COM' , 2 , 'VEN') operacion,	 ");
    	//sbQuery.append(" 	FN_GET_HOLDER_NAME(hao.ID_HOLDER_ACCOUNT_FK) CUI,	 ");
    	
    	sbQuery.append(" 	(SELECT LISTAGG(HAD.ID_HOLDER_FK , ',' ||chr(13) ) WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");																			 
		sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL HAD ");																																 
		sbQuery.append(" 	WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ");																									 
		sbQuery.append(" 	) Holders, ");
		
    	
    	sbQuery.append(" 	mo.id_security_code_fk CLASE_CLAVE_VALOR,	 ");
    	sbQuery.append(" 	hao.stock_quantity cantidad,	 ");
    	sbQuery.append(" 	aom.market_date fecha_mercado,	 ");
    	sbQuery.append(" 	decode(hao.role, 1 , aom.market_rate , 2 ,nvl(aom.market_rate,0)) as tasa_mercado,	 ");
    	sbQuery.append(" 	decode(hao.role, 1 , aom.market_price , 2 ,nvl(aom.market_price,0)) as precio_mercado,	 ");
    	sbQuery.append(" 	(select pt.parameter_name from parameter_table pt where pt.parameter_table_pk = mo.OPERATION_STATE) as estado,	 ");
    	sbQuery.append(" 	s.security_class,	 ");
    	sbQuery.append(" 	(select pt.text1 from parameter_table pt where pt.PARAMETER_TABLE_PK=s.security_class) as clase,	 ");
    	sbQuery.append(" 	(SELECT COUNT(DISTINCT AR.ID_ASSIGNMENT_REQUEST_PK)	 ");
    	sbQuery.append(" 	FROM ASSIGNMENT_REQUEST AR, PARTICIPANT_ASSIGNMENT PAS	 ");
    	sbQuery.append(" 	WHERE AR.ID_PARTICIPANT_ASSIGNMENT_FK = PAS.ID_PARTICIPANT_ASSIGNMENT_PK	 ");
    	sbQuery.append(" 	AND PAS.ID_ASSIGNMENT_PROCESS_FK = MO.ID_ASSIGNMENT_PROCESS_FK	 ");
    	sbQuery.append(" 	AND PAS.ID_PARTICIPANT_FK = :participant ) cantidad_1,	 ");
    	
    	sbQuery.append("    (SELECT LISTAGG((select h.full_name from holder h where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK) ||chr(13)) WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) ");
		sbQuery.append("    FROM HOLDER_ACCOUNT_DETAIL HAD	");
		sbQuery.append("    WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK " );
		sbQuery.append(" 	) HoldersDesc ");
    	
    	sbQuery.append(" 	from mechanism_operation mo	 ");
    	sbQuery.append(" 	inner join negotiation_modality nmo on mo.id_negotiation_modality_fk = nmo.id_negotiation_modality_pk	 ");
    	sbQuery.append(" 	inner join negotiation_mechanism nme on mo.id_negotiation_mechanism_fk = nme.id_negotiation_mechanism_pk	 ");
    	sbQuery.append(" 	inner join holder_account_operation hao on hao.id_mechanism_operation_fk = mo.id_mechanism_operation_pk	 ");
    	sbQuery.append(" 	inner join holder_account ha on ha.id_holder_account_pk = hao.id_holder_account_fk	 ");
    	sbQuery.append(" 	left join account_operation_marketfact aom on hao.id_holder_Account_operation_pk = aom.id_holder_Account_operation_fk	 ");
    	sbQuery.append(" 	INNER JOIN PARTICIPANT P ON P.ID_PARTICIPANT_PK = hao.ID_INCHARGE_FUNDS_PARTICIPANT	 ");
    	sbQuery.append(" 	INNER JOIN PARTICIPANT P2 ON P2.ID_PARTICIPANT_PK = hao.ID_INCHARGE_STOCK_PARTICIPANT	 ");
    	sbQuery.append(" 	INNER JOIN SECURITY S ON mo.id_security_code_fk = s.id_security_code_pk	 ");
    	sbQuery.append(" 	where	 ");
    	sbQuery.append(" 	hao.holder_account_state in (623,625)	 ");
    	sbQuery.append(" 	and hao.operation_part = 1	 ");
    	sbQuery.append(" 	and hao.IND_INCHARGE IN (0,1)	 ");
    	sbQuery.append(" 	and trunc(mo.OPERATION_DATE) = to_date( ':date' ,'dd/MM/yyyy')	 ");
		
		if(Validations.validateIsNotNull(registrationAssignmentHoldersTO.getParticipantPk())){
	    	sbQuery.append(" 	and (Hao.Id_Incharge_Stock_Participant = :participant )	 ");
		}
		if(Validations.validateIsNotNull(registrationAssignmentHoldersTO.getSchema())){
			sbQuery.append(" 	and ( mo.settlement_schema = :schema )	 ");
    	}
		if(Validations.validateIsNotNull(registrationAssignmentHoldersTO.getMechanism())){
			sbQuery.append(" 	and ( nme.id_negotiation_mechanism_pk = :mechanism )	 ");
    	}
		if(Validations.validateIsNotNull(registrationAssignmentHoldersTO.getModality())){
			sbQuery.append(" 	and ( nmo.id_negotiation_modality_pk = ( :modality ))	 ");
    	}
		if(Validations.validateIsNotNull(registrationAssignmentHoldersTO.getSecurityCode())){
			sbQuery.append(" 	and ( mo.id_security_code_fk = ( :value_series ))	 ");
		}
		if(Validations.validateIsNotNull(registrationAssignmentHoldersTO.getSecurityClass())){
			sbQuery.append(" 	and ( s.security_class = ( :security_class ))	 ");
		}
		if(Validations.validateIsNotNull(registrationAssignmentHoldersTO.getIdHolderAccountPk())){
			sbQuery.append(" 	and ( ha.ACCOUNT_NUMBER = ( :holder_account ))	 ");
		}
		if(Validations.validateIsNotNull(registrationAssignmentHoldersTO.getIdHolderPk())){
	    	sbQuery.append(" 	and (EXISTS	 ");
	    	sbQuery.append(" 	(SELECT had.ID_HOLDER_FK	 ");
	    	sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL had	 ");
	    	sbQuery.append(" 	WHERE had.ID_HOLDER_FK = :cui_code	 ");
	    	sbQuery.append(" 	AND had.ID_HOLDER_ACCOUNT_FK = ha.id_holder_account_pk	 ");
	    	sbQuery.append(" 	))	 ");
		}

	  	sbQuery.append(" 	order by modalidad,papeleta,secuencial		 ");

		String strQueryFormatted = sbQuery.toString();
		
		if(registrationAssignmentHoldersTO.getParticipantPk()!=null){
    		strQueryFormatted= strQueryFormatted.replace(":participant", registrationAssignmentHoldersTO.getParticipantPk().toString());
    	}
    	if(registrationAssignmentHoldersTO.getSchema()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":schema", registrationAssignmentHoldersTO.getSchema().toString());
    	}
    	if(registrationAssignmentHoldersTO.getMechanism()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":mechanism", registrationAssignmentHoldersTO.getMechanism().toString());
    	}
    	if(registrationAssignmentHoldersTO.getModality()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":modality", registrationAssignmentHoldersTO.getModality().toString());
    	}
    	if(Validations.validateIsNotNullAndNotEmpty(registrationAssignmentHoldersTO.getSecurityCode())){
    		strQueryFormatted=strQueryFormatted.replace(":security_code", registrationAssignmentHoldersTO.getSecurityCode().toString());
    	}
    	if(registrationAssignmentHoldersTO.getSecurityClass()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":security_class", registrationAssignmentHoldersTO.getSecurityClass().toString());
    	}

    	if(registrationAssignmentHoldersTO.getIdHolderAccountPk()!=null){ //Numero de cuenta account_number
    		strQueryFormatted= strQueryFormatted.replace(":holder_account", registrationAssignmentHoldersTO.getIdHolderAccountPk().toString());
    	}
    	if(registrationAssignmentHoldersTO.getIdHolderPk()!=null){
    		strQueryFormatted= strQueryFormatted.replace(":cui_code", registrationAssignmentHoldersTO.getIdHolderPk().toString());
    	}
    	
    	strQueryFormatted=strQueryFormatted.replace(":date", registrationAssignmentHoldersTO.getStrDateInitial());
    	
    	return strQueryFormatted;
    }
    
	@SuppressWarnings("unchecked")
	public List<Object[]> getQueryListByClass(String strQuery) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(strQuery);
		Query query = em.createNativeQuery(sbQuery.toString());
		return query.getResultList();
	}

    
    /**
     * Gets the list chained operation pending.
     *
     * @param objChainedOperationPendingTO the object chained operation pending to
     * @return the list chained operation pending
     */
    @SuppressWarnings("unchecked")
	public List<Object[]> getListChainedOperationPending(ChainedOperationPendingTO objChainedOperationPendingTO){
		StringBuilder sbQuery = new StringBuilder();						
		try {
			sbQuery.append("  select CHO.ID_PARTICIPANT_FK, PART.DESCRIPTION, to_char(CHO.REGISTER_DATE, 'dd/mm/yyyy'),										"); //0, 1, 2
			sbQuery.append("  to_char(CHO.REGISTER_DATE, 'hh24:mi:ss'), COUNT(*),																			"); //3, 4
			sbQuery.append("  (SELECT COUNT(*) FROM CHAINED_HOLDER_OPERATION CHO1 WHERE CHO1.ID_PARTICIPANT_FK = CHO.ID_PARTICIPANT_FK 						");
			sbQuery.append("  AND CHO1.CHAIN_STATE = 1957 AND CHO1.IND_AUTOMATIC = CHO.IND_AUTOMATIC AND CHO1.REGISTER_DATE = CHO.REGISTER_DATE), 			");//5
			sbQuery.append("  (SELECT COUNT(*) FROM CHAINED_HOLDER_OPERATION CHO1 WHERE CHO1.ID_PARTICIPANT_FK = CHO.ID_PARTICIPANT_FK						");
			sbQuery.append("  AND CHO1.CHAIN_STATE = 1959 AND CHO1.IND_AUTOMATIC = CHO.IND_AUTOMATIC AND CHO1.REGISTER_DATE = CHO.REGISTER_DATE),			");//6
			sbQuery.append("  (SELECT COUNT(*) FROM CHAINED_HOLDER_OPERATION CHO1 WHERE CHO1.ID_PARTICIPANT_FK = CHO.ID_PARTICIPANT_FK 						");
			sbQuery.append("  AND CHO1.CHAIN_STATE IN (1958, 1960) AND CHO1.IND_AUTOMATIC = CHO.IND_AUTOMATIC AND CHO1.REGISTER_DATE = CHO.REGISTER_DATE), 	");//7
			sbQuery.append("  DECODE(CHO.IND_AUTOMATIC,1,'AUTOMATICO', 'MANUAL') 																			");//8
			sbQuery.append("  FROM CHAINED_HOLDER_OPERATION CHO, PARTICIPANT PART																			");
			sbQuery.append("  WHERE 1=1 AND CHO.ID_PARTICIPANT_FK = PART.ID_PARTICIPANT_PK																	");	
			sbQuery.append("  AND (trunc(CHO.REGISTER_DATE) >= to_date(:parDateInitial, 'dd/MM/yyyy')														");	
			sbQuery.append("  AND trunc(CHO.REGISTER_DATE) <= to_date(:parDateEnd,'dd/MM/yyyy') )															");
			if(Validations.validateIsNotNullAndNotEmpty(objChainedOperationPendingTO.getParticipant())){
				sbQuery.append("  AND CHO.ID_PARTICIPANT_FK = :participant																					");
			}
			sbQuery.append("  group by CHO.ID_PARTICIPANT_FK, PART.DESCRIPTION, CHO.REGISTER_DATE,															");
			sbQuery.append("  CHO.IND_AUTOMATIC, DECODE(CHO.IND_AUTOMATIC,1,'AUTOMATICO', 'MANUAL') 														");
			sbQuery.append("  ORDER BY PART.DESCRIPTION ASC, CHO.REGISTER_DATE DESC																			");		
			Query query = em.createNativeQuery(sbQuery.toString());
			query.setParameter("parDateInitial", objChainedOperationPendingTO.getStrDateInitial());
			query.setParameter("parDateEnd", objChainedOperationPendingTO.getStrDateEnd());
			if(Validations.validateIsNotNullAndNotEmpty(objChainedOperationPendingTO.getParticipant())){
				query.setParameter("participant", new Long(objChainedOperationPendingTO.getParticipant().toString()));
			}
			return query.getResultList();
		} catch(NoResultException ex) {
			return null;
		}				
    }
	
	/**
	 * Gets the list participants for mechanism modality.
	 *
	 * @param mechanismId the mechanism id
	 * @param modalityId the modality id
	 * @param accountType the account type
	 * @param indIncharge the ind incharge
	 * @return the list participants for mechanism modality
	 */
	@SuppressWarnings("unchecked")
	public List<Participant> getListParticipants(Long mechanismId, Long modalityId, Integer accountType, Integer indIncharge) {
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append("	SELECT pa FROM Participant pa");
		sbQuery.append("	WHERE pa.idParticipantPk IN (");
		sbQuery.append("	SELECT distinct pm.participant.idParticipantPk");
		sbQuery.append("	FROM ParticipantMechanism pm");
		sbQuery.append("	WHERE pm.stateParticipantMechanism = :stateParticipantMechanism ");		
		sbQuery.append("	AND pm.mechanismModality.id.idNegotiationMechanismPk = :idNegotiationMechanismPk");
		if(modalityId!=null){
			sbQuery.append("	AND pm.mechanismModality.id.idNegotiationModalityPk = :idNegotiationModalityPk ");
			parameters.put("idNegotiationModalityPk", modalityId);
		}
		sbQuery.append("	)");
		if(accountType!=null){
			sbQuery.append("	AND pa.accountType = :accountType ");
			parameters.put("accountType", accountType);
		}
		if(indIncharge!=null){
			sbQuery.append("	AND pa.indSettlementIncharge = :indIncharge ");
			parameters.put("indIncharge", indIncharge);
		}
		sbQuery.append("	AND pa.state = :state ");
		sbQuery.append("	ORDER BY pa.mnemonic ASC");
		
		parameters.put("stateParticipantMechanism", ParticipantMechanismStateType.REGISTERED.getCode());
		parameters.put("state", ParticipantStateType.REGISTERED.getCode());
		parameters.put("idNegotiationMechanismPk", mechanismId);
			
		return (List<Participant>)findListByQueryString(sbQuery.toString(), parameters);
	}
	
	/*Busqueda de titulares*/
	public String searchDocumentNumbersHolders(Long idHolderAccountPk){
		StringBuilder builder = new StringBuilder();
		builder = new StringBuilder();
		builder.append(" select had.holder.documentNumber from HolderAccountDetail had ");
		builder.append(" where 1 = 1 ");
		builder.append(" and had.holderAccount.idHolderAccountPk = :idHolderAccountPk ");
		Query query = em.createQuery(builder.toString());
		query.setParameter("idHolderAccountPk", idHolderAccountPk);
		
		List<String> documents = new ArrayList<>();
		documents = query.getResultList();
		builder = new StringBuilder();
		for (String string : documents) 
			builder.append(string+GeneralConstants.STR_COMMA);
		String documentsNumber = builder.toString();
		return documentsNumber.substring(0,documentsNumber.length()-2);
	}
	
	/**
	 * Gets the lst settlement account operation by validate chained.
	 *
	 * @param settlementDate the settlement date
	 * @param idParticipant the id participant
	 * @param idSecurityCode the id security code
	 * @param idHolderAccount the id holder account
	 * @param idModalityGroup the id modality group
	 * @param currency the currency
	 * @param indExtended the ind extended
	 * @param instrumentType the instrument type
	 * @return the lst settlement account operation
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getLstSettlementAccountOperationByValidateChained(Date settlementDate, Long idParticipant, String idSecurityCode, Long idHolderAccount, 
												Long idModalityGroup, Integer currency, Integer indExtended, Integer instrumentType)
	{
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT MO.securities.idSecurityCodePk, "); 								//0
		stringBuffer.append(" SAO.holderAccountOperation.inchargeStockParticipant.idParticipantPk, ");	//1
		stringBuffer.append(" SAO.holderAccountOperation.holderAccount.idHolderAccountPk, ");			//2
		stringBuffer.append(" SAM.marketDate, ");														//3
		stringBuffer.append(" SAM.marketRate, ");														//4
		stringBuffer.append(" SAM.marketQuantity, ");													//5
		stringBuffer.append(" SAO.stockQuantity, ");													//6
		stringBuffer.append(" SAO.role, ");																//7
		stringBuffer.append(" SAO.idSettlementAccountPk, ");											//8
		stringBuffer.append(" SAM.idSettAccountMarketfactPk, ");										//9
		stringBuffer.append(" MGD.modalityGroup.idModalityGroupPk, ");									//10
		stringBuffer.append(" SAO.settlementOperation.settlementCurrency, ");							//11
		stringBuffer.append(" MO.mechanisnModality.negotiationModality.idNegotiationModalityPk, ");		//12
		stringBuffer.append(" MO.mechanisnModality.negotiationModality.modalityName, ");				//13
		stringBuffer.append(" MO.ballotNumber, ");														//14
		stringBuffer.append(" MO.sequential, ");														//15
		stringBuffer.append(" MO.operationNumber, ");													//16
		stringBuffer.append(" MO.operationDate, ");														//17
		stringBuffer.append(" SAO.settlementOperation.operationState, ");								//18
		stringBuffer.append(" SAO.settlementOperation.operationPart, ");								//19
		stringBuffer.append(" SAO.settlementOperation.idSettlementOperationPk, ");						//20
		stringBuffer.append(" MO.idMechanismOperationPk, ");											//21
		stringBuffer.append(" MO.buyerParticipant.mnemonic, ");											//22
		stringBuffer.append(" MO.sellerParticipant.mnemonic, ");										//23
		stringBuffer.append(" SAO.holderAccountOperation.inchargeStockParticipant.mnemonic ");			//24
		stringBuffer.append(" FROM SettlementAccountOperation SAO, ModalityGroupDetail MGD, MechanismOperation MO, SettlementAccountMarketfact SAM ");
		stringBuffer.append(" WHERE SAO.settlementOperation.mechanismOperation.idMechanismOperationPk = MO.idMechanismOperationPk ");
		stringBuffer.append(" and MO.mechanisnModality.negotiationMechanism.idNegotiationMechanismPk = MGD.negotiationMechanism.idNegotiationMechanismPk ");
		stringBuffer.append(" and MO.mechanisnModality.negotiationModality.idNegotiationModalityPk = MGD.negotiationModality.idNegotiationModalityPk ");
		stringBuffer.append(" and SAM.settlementAccountOperation.idSettlementAccountPk = SAO.idSettlementAccountPk ");
		stringBuffer.append(" and SAO.settlementOperation.settlementDate = :settlementDate ");
		stringBuffer.append(" and MO.securities.instrumentType = :instrumentType ");
		stringBuffer.append(" and ( (SAO.stockReference is null or SAO.stockReference = :referentialStock) and SAO.settlementOperation.operationState in (:lstCashOperationState) "
								+ " or (SAO.stockReference in (:lstTermStockReference) and SAO.settlementOperation.operationState = :cashSettledState) ) ");
		//we exclude the operations inside chains
		stringBuffer.append(" and not exists (SELECT HCD FROM HolderChainDetail HCD "
											+ " WHERE HCD.settlementAccountMarketfact.settlementAccountOperation.idSettlementAccountPk = SAO.idSettlementAccountPk "
											+ " and HCD.chainedHolderOperation.chaintState in (:lstChainState) ) ");
		stringBuffer.append(" and SAO.operationState in (:lstSettlementAccountState) ");
		stringBuffer.append(" and SAO.settlementOperation.settlementSchema = :NETSchema ");
		stringBuffer.append(" and MGD.modalityGroup.settlementSchema = SAO.settlementOperation.settlementSchema ");
		stringBuffer.append(" and SAM.indActive = :constantOne ");
		//we exclude the operation inside alive settlement processes
		stringBuffer.append(" and not exists (SELECT OS FROM OperationSettlement OS "
											+ " WHERE OS.settlementOperation.idSettlementOperationPk = SAO.settlementOperation.idSettlementOperationPk "
											+ " and OS.settlementProcess.settlementDate = :settlementDate "
											+ " and OS.settlementProcess.processState in (:lstProcessState) ) ");
		//we exclude the report operations in cash part as buyers
		stringBuffer.append(" and not exists (SELECT SAO_REPO FROM SettlementAccountOperation SAO_REPO "
											+ " WHERE SAO_REPO.settlementOperation.mechanismOperation.indReportingBalance = :constantOne "
											+ " and SAO_REPO.settlementOperation.operationPart = :constantOne "
											+ " and SAO_REPO.role = :constantOne "
											+ " and SAO_REPO.operationState in (:lstSettlementAccountState) "
											+ " and SAO_REPO.idSettlementAccountPk = SAO.idSettlementAccountPk) ");
		if (Validations.validateIsNotNull(idModalityGroup)) {
			stringBuffer.append(" and MGD.modalityGroup.idModalityGroupPk = :idModalityGroup ");
		}
		if(Validations.validateIsNotNull(indExtended)) {
			stringBuffer.append(" and SAO.settlementOperation.indExtended = :indExtended ");
		}
		if (Validations.validateIsNotNull(currency)) {
			stringBuffer.append(" and SAO.settlementOperation.settlementCurrency = :currency ");
		}
		if (Validations.validateIsNotNull(idParticipant)) {
			stringBuffer.append(" and SAO.holderAccountOperation.inchargeStockParticipant.idParticipantPk = :idParticipant ");
		}
		if (Validations.validateIsNotNull(idSecurityCode)) {
			stringBuffer.append(" and MO.securities.idSecurityCodePk = :idSecurityCode ");
		}
		if (Validations.validateIsNotNull(idHolderAccount)) {
			stringBuffer.append(" and SAO.holderAccountOperation.holderAccount.idHolderAccountPk = :idHolderAccount ");
		}
		stringBuffer.append(" ORDER BY MO.securities.idSecurityCodePk, SAO.holderAccountOperation.holderAccount.idHolderAccountPk, "
									+ " SAM.marketDate, SAM.marketRate, SAM.marketQuantity, SAO.role ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("settlementDate", settlementDate);
		query.setParameter("instrumentType", instrumentType);
		query.setParameter("referentialStock", AccountOperationReferenceType.REFERENTIAL_SECURITIES.getCode());
		List<Integer> lstCashOperationState= new ArrayList<Integer>();
		lstCashOperationState.add(MechanismOperationStateType.REGISTERED_STATE.getCode());
		lstCashOperationState.add(MechanismOperationStateType.ASSIGNED_STATE.getCode());
		query.setParameter("lstCashOperationState", lstCashOperationState);
		List<Long> lstTermStockReference= new ArrayList<Long>();
		lstTermStockReference.add(AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode());
		lstTermStockReference.add(AccountOperationReferenceType.REFERENTIAL_SECURITIES.getCode());
		query.setParameter("lstTermStockReference", lstTermStockReference);
		query.setParameter("cashSettledState", MechanismOperationStateType.CASH_SETTLED.getCode());
		List<Integer> lstChainState= new ArrayList<Integer>();
		lstChainState.add(ChainedOperationStateType.REGISTERED.getCode());
		lstChainState.add(ChainedOperationStateType.CONFIRMED.getCode());
		query.setParameter("lstChainState", lstChainState);
		List<Integer> lstSettlementAccountState= new ArrayList<Integer>();
		lstSettlementAccountState.add(HolderAccountOperationStateType.REGISTERED.getCode());
		lstSettlementAccountState.add(HolderAccountOperationStateType.CONFIRMED.getCode());
		query.setParameter("lstSettlementAccountState", lstSettlementAccountState);
		query.setParameter("NETSchema", SettlementSchemaType.NET.getCode());
		query.setParameter("constantOne", BooleanType.YES.getCode());
		List<Integer> lstProcessState= new ArrayList<Integer>();
		lstProcessState.add(SettlementProcessStateType.IN_PROCESS.getCode());
		lstProcessState.add(SettlementProcessStateType.STARTED.getCode());
		lstProcessState.add(SettlementProcessStateType.WAITING.getCode());
		lstProcessState.add(SettlementProcessStateType.ERROR.getCode());
		query.setParameter("lstProcessState", lstProcessState);
		
		if (Validations.validateIsNotNull(idModalityGroup)) {
			query.setParameter("idModalityGroup", idModalityGroup);
		}
		if(Validations.validateIsNotNull(indExtended)) {
			query.setParameter("indExtended", indExtended);
		}
		if (Validations.validateIsNotNull(currency)) {
			query.setParameter("currency", currency);
		}
		if (Validations.validateIsNotNull(idParticipant)) {
			query.setParameter("idParticipant", idParticipant);
		}
		if (Validations.validateIsNotNull(idSecurityCode)) {
			query.setParameter("idSecurityCode", idSecurityCode);
		}
		if (Validations.validateIsNotNull(idHolderAccount)) {
			query.setParameter("idHolderAccount", idHolderAccount);
		}
		
		return query.getResultList();
	}

	/**
     * Query report TradingReportBBV
     * @param tradingReportBBVTO
     * @return
     */
    
    public String getQueryTradingReportBBV(TradingReportBBVTO tradingReportBBVTO){
    	StringBuilder sbQuery= new StringBuilder();
    	
    	sbQuery.append(" 		SELECT	  ");
    	sbQuery.append(" 		MO.ID_NEGOTIATION_MODALITY_FK,	  ");
    	sbQuery.append(" 		(SELECT NM.MODALITY_CODE FROM NEGOTIATION_MODALITY NM WHERE NM.ID_NEGOTIATION_MODALITY_PK = MO.ID_NEGOTIATION_MODALITY_FK) MODALIDAD,	  ");
    	sbQuery.append(" 		MO.BALLOT_NUMBER ||'/'|| MO.SEQUENTIAL PAPELETA_SECUENCIAL,	  ");
    	sbQuery.append(" 		(SELECT PA.ID_PARTICIPANT_PK || ' - ' ||PA.MNEMONIC FROM PARTICIPANT PA WHERE PA.ID_PARTICIPANT_PK = MO.ID_BUYER_PARTICIPANT_FK) COMPRADOR,	  ");
    	sbQuery.append(" 		(SELECT PA.ID_PARTICIPANT_PK || ' - ' ||PA.MNEMONIC FROM PARTICIPANT PA WHERE PA.ID_PARTICIPANT_PK = MO.ID_SELLER_PARTICIPANT_FK) VENDEDOR,	  ");
    	sbQuery.append(" 		MO.ID_SECURITY_CODE_FK CLASE_CLAVE_VALOR,	  ");
    	sbQuery.append(" 		NVL(MO.TERM_SETTLEMENT_DAYS,0) PLAZO,	  ");
    	//sbQuery.append(" 		--TO_CHAR(MO.AMOUNT_RATE, '99,999.999') TASA,	  ");
    	sbQuery.append(" 		MO.AMOUNT_RATE TASA,	  ");
    	sbQuery.append(" 		MO.STOCK_QUANTITY CANTIDAD,	  ");
    	sbQuery.append(" 		MO.CASH_PRICE PRECIO,	  ");
    	sbQuery.append(" 		(SELECT PT.DESCRIPTION FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK = MO.OPERATION_CURRENCY) MONEDA,	  ");
    	sbQuery.append(" 		MO.CASH_AMOUNT TOTAL,	  ");
    	sbQuery.append(" 		(SELECT PT.DESCRIPTION FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK = MO.OPERATION_STATE) ESTADO,	  ");
    	//sbQuery.append(" 		--DATOS ORIGEN DE TRANSFERENCIA DE REPORTO (REPORTO ORIGINAL ENLAZADA AL REPO SECUNDARIO)	  ");
    	sbQuery.append(" 		MO_REF.BALLOT_NUMBER ||'/'|| MO_REF.SEQUENTIAL PAPELETA_SECUENCIAL_R,	  ");
    	sbQuery.append(" 		(SELECT PA.MNEMONIC FROM PARTICIPANT PA WHERE PA.ID_PARTICIPANT_PK = MO_REF.ID_SELLER_PARTICIPANT_FK) VENDEDOR_R,	  ");
    	sbQuery.append(" 		MO_REF.TERM_SETTLEMENT_DAYS PLAZO_R,	  ");
    	sbQuery.append(" 		MO_REF.AMOUNT_RATE TASA_R,	  ");
    	sbQuery.append(" 		MO_REF.CASH_PRICE PRECIO_R,	  ");
    	sbQuery.append(" 		TRUNC(MO_REF.OPERATION_DATE) FECHA_OPERACION_R	  ");
    	sbQuery.append(" 		FROM MECHANISM_OPERATION MO,	  ");
    	sbQuery.append(" 		MECHANISM_OPERATION MO_REF, 	  "); //CasoReportoSecundario
    	sbQuery.append(" 		SECURITY SE	  ");
    	sbQuery.append(" 		WHERE MO_REF.ID_REFERENCE_OPERATION_FK (+) = MO.ID_MECHANISM_OPERATION_PK	  ");
    	sbQuery.append(" 		AND MO.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK	  ");
    	sbQuery.append(" 		AND MO.ID_NEGOTIATION_MECHANISM_FK = 1 	  "); //MecanismoBolsa
    	//sbQuery.append(" 		--AND HAO.OPERATION_PART=1	  ");
    	sbQuery.append(" 		  AND trunc(mo.OPERATION_DATE) = to_date(':settlement_date','dd/MM/yyyy')	  ");
		
		if(Validations.validateIsNotNull(tradingReportBBVTO.getParticipantPk())){
			sbQuery.append(" 		  AND (MO.ID_SELLER_PARTICIPANT_FK = :participant	  ");
	    	sbQuery.append(" 		  OR MO.ID_BUYER_PARTICIPANT_FK = :participant)	  ");
		}
		if(Validations.validateIsNotNull(tradingReportBBVTO.getModality())){
			sbQuery.append(" 		  AND (mo.ID_NEGOTIATION_MODALITY_FK = (:modality))	  ");
    	}
		if(Validations.validateIsNotNull(tradingReportBBVTO.getSecurityClass())){
			sbQuery.append(" 		  AND (SE.SECURITY_CLASS = (:security_class))	  ");
		}
	
    	sbQuery.append(" 		ORDER BY  MODALIDAD,PAPELETA_SECUENCIAL	  ");

		String strQueryFormatted = sbQuery.toString();
		
		if(tradingReportBBVTO.getParticipantPk()!=null){
    		strQueryFormatted= strQueryFormatted.replace(":participant", tradingReportBBVTO.getParticipantPk().toString());
    	}
    	if(tradingReportBBVTO.getModality()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":modality", tradingReportBBVTO.getModality().toString());
    	}
    	if(tradingReportBBVTO.getSecurityClass()!=null){
    		strQueryFormatted=strQueryFormatted.replace(":security_class", tradingReportBBVTO.getSecurityClass().toString());
    	}
    	strQueryFormatted=strQueryFormatted.replace(":settlement_date", tradingReportBBVTO.getStrSettlementDate());
    	
    	return strQueryFormatted;
    }
    
    /**
     * QUERY REPORT 281 Reasignacion de cuentas
     * @param reAccountTO
     * @return
     */
    public String reassignmentAccountReport(ReassignmentAccountTO reAccountTO){
    	
    	StringBuilder sbQuery= new StringBuilder();
    	
    	sbQuery.append("    SELECT                                                                                                                                  ");
    	sbQuery.append("    Z.SOLICITUD,                                                                                                                            ");
    	sbQuery.append("    Z.PARTICIPANTE,                                                                                                                         ");
    	sbQuery.append("    Z.FECHA,                                                                                                                                ");
    	sbQuery.append("    Z.PAP_SEQ,                                                                                                                              ");
    	sbQuery.append("    Z.MOTIVO,                                                                                                                               ");
    	sbQuery.append("    Z.CUEN_OLD,                                                                                                                             ");
    	sbQuery.append("    Z.FE_OLD,                                                                                                                               ");
    	sbQuery.append("    Z.TAS_OLD,                                                                                                                              ");
    	sbQuery.append("    Z.PRE_OLD,                                                                                                                              ");
    	sbQuery.append("    Z.CANT_OLD,                                                                                                                             ");
    	sbQuery.append("    Z.CUEN_NEW,                                                                                                                             ");
    	sbQuery.append("    Z.FE_NEW,                                                                                                                               ");
    	sbQuery.append("    Z.TAS_NEW,                                                                                                                              ");
    	sbQuery.append("    Z.PRE_NEW,                                                                                                                              ");
    	sbQuery.append("    Z.CANT_NEW,                                                                                                                             ");
    	sbQuery.append("    Z.ROL,                                                                                                                                  ");
    	sbQuery.append("    Z.ESTADO, z.CLASE_CLAVE_VALOR FROM(   																														");
    	
    	sbQuery.append("    SELECT                                                                                                                                  ");
    	sbQuery.append("    T.SOLICITUD,                                                                                                                            ");
    	sbQuery.append("    T.PARTICIPANTE,                                                                                                                         ");
    	sbQuery.append("    T.FECHA,                                                                                                                                ");
    	sbQuery.append("    T.PAP_SEQ,                                                                                                                              ");
    	sbQuery.append("    T.MOTIVO,                                                                                                                               ");
    	sbQuery.append("    T.CUEN_OLD,                                                                                                                             ");
    	sbQuery.append("    T.FE_OLD,                                                                                                                               ");
    	sbQuery.append("    T.TAS_OLD,                                                                                                                              ");
    	sbQuery.append("    T.PRE_OLD,                                                                                                                              ");
    	sbQuery.append("    T.CANT_OLD,                                                                                                                             ");
    	sbQuery.append("    T.CUEN_NEW,                                                                                                                             ");
    	sbQuery.append("    T.FE_NEW,                                                                                                                               ");
    	sbQuery.append("    T.TAS_NEW,                                                                                                                              ");
    	sbQuery.append("    T.PRE_NEW,                                                                                                                              ");
    	sbQuery.append("    T.CANT_NEW,                                                                                                                             ");
    	sbQuery.append("    T.ROL,                                                                                                                                  ");
    	sbQuery.append("    T.ESTADO, t.CLASE_CLAVE_VALOR FROM(                                                                                                                          ");
    	sbQuery.append("    SELECT                                                                                                                                  ");
    	sbQuery.append("    RR.ID_REASSIGNMENT_REQUEST_PK SOLICITUD,                                                                                                ");
    	sbQuery.append("    RR.ID_PARTICIPANT_FK || ' - ' || PAR.MNEMONIC || ' - ' || PAR.DESCRIPTION PARTICIPANTE,                                                 ");
    	sbQuery.append("    TO_CHAR(RR.REGISTER_DATE, 'DD/MM/YYYY') FECHA,                                                                                          ");
    	sbQuery.append("    MO.BALLOT_NUMBER || ' / ' || MO.SEQUENTIAL PAP_SEQ,                                                                                     ");
    	sbQuery.append("    (select PT_SQ.DESCRIPTION from PARAMETER_TABLE pt_sq where PT_SQ.PARAMETER_TABLE_PK = rr.REQUEST_REASON) as MOTIVO,                                                                                                             ");
    	sbQuery.append("    CASE WHEN RRD.HOLDER_ACCOUNT_STATE = 624 THEN TO_CHAR(HA.ALTERNATE_CODE)                                                                ");
    	sbQuery.append("          ELSE ' ' END CUEN_OLD,                                                                                                            ");
    	sbQuery.append("    CASE WHEN RRD.HOLDER_ACCOUNT_STATE = 624 THEN TO_CHAR(AOM.MARKET_DATE,'DD/MM/YYYY')                                                                  ");
    	sbQuery.append("          ELSE ' ' END FE_OLD,                                                                                                              ");
    	sbQuery.append("    CASE WHEN RRD.HOLDER_ACCOUNT_STATE = 624 THEN TO_CHAR(AOM.MARKET_RATE,'999,999,999,999.0000')                                                                  ");
    	sbQuery.append("          ELSE ' ' END TAS_OLD,                                                                                                             ");
    	sbQuery.append("    CASE WHEN RRD.HOLDER_ACCOUNT_STATE = 624 THEN TO_CHAR(AOM.MARKET_PRICE, '999,999,999,999.00')                                                                 ");
    	sbQuery.append("          ELSE ' ' END PRE_OLD,                                                                                                             ");
    	sbQuery.append("    CASE WHEN RRD.HOLDER_ACCOUNT_STATE = 624 THEN TO_CHAR(AOM.OPERATION_QUANTITY)                                                           ");
    	sbQuery.append("          ELSE ' ' END CANT_OLD,                                                                                                            ");
    	sbQuery.append("    CASE WHEN RRD.HOLDER_ACCOUNT_STATE = 625 THEN TO_CHAR(HA.ALTERNATE_CODE)                                                                ");
    	sbQuery.append("          ELSE ' ' END CUEN_NEW,                                                                                                            ");
    	sbQuery.append("    CASE WHEN RRD.HOLDER_ACCOUNT_STATE = 625 THEN TO_CHAR(AOM.MARKET_DATE,'DD/MM/YYYY')                                                                  ");
    	sbQuery.append("          ELSE ' ' END FE_NEW,                                                                                                              ");
    	sbQuery.append("    CASE WHEN RRD.HOLDER_ACCOUNT_STATE = 625 THEN TO_CHAR(AOM.MARKET_RATE,'999,999,999,999.0000')                                                                  ");
    	sbQuery.append("          ELSE ' ' END TAS_NEW,                                                                                                             ");
    	sbQuery.append("    CASE WHEN RRD.HOLDER_ACCOUNT_STATE = 625 THEN TO_CHAR(AOM.MARKET_PRICE,'999,999,999,999.00')                                                                 ");
    	sbQuery.append("          ELSE ' ' END PRE_NEW,                                                                                                             ");
    	sbQuery.append("    CASE WHEN RRD.HOLDER_ACCOUNT_STATE = 625 THEN TO_CHAR(AOM.OPERATION_QUANTITY)                                                           ");
    	sbQuery.append("          ELSE ' ' END CANT_NEW,                                                                                                            ");
    	sbQuery.append("                                                                                                                                            ");
    	sbQuery.append("    CASE WHEN HAO.ROLE = 2 THEN 'VENTA'                                                                                                     ");
    	sbQuery.append("          ELSE 'COMPRA' END ROL,                                                                                                            ");
    	//sbQuery.append("    CASE WHEN RRD.HOLDER_ACCOUNT_STATE = 625 THEN 'CONFIRMADA'                                                                              ");
    	//sbQuery.append("                 ELSE 'ANULADA'                                                                                                             ");
    	sbQuery.append("    case when  CASE WHEN RRD.HOLDER_ACCOUNT_STATE = 624 THEN TO_CHAR(HA.ALTERNATE_CODE) ELSE ' ' END = ' ' then (SELECT pt_sq.description FROM parameter_table pt_sq WHERE pt_sq.parameter_table_pk = rr.REQUEST_STATE ) else (CASE WHEN RRD.HOLDER_ACCOUNT_STATE = 625 THEN 'CONFIRMADA' ELSE 'ANULADA' END) end as ESTADO, MO.ID_SECURITY_CODE_FK as CLASE_CLAVE_VALOR                                                                                                                              ");
    	sbQuery.append("    FROM REASSIGNMENT_REQUEST_DETAIL RRD                                                                                                    ");
    	sbQuery.append("    INNER JOIN REASSIGNMENT_REQUEST RR ON RRD.ID_REASSIGNMENT_REQUEST_FK = RR.ID_REASSIGNMENT_REQUEST_PK                                    ");
    	sbQuery.append("    INNER JOIN HOLDER_ACCOUNT_OPERATION HAO ON RRD.ID_HOLDER_ACCOUNT_OPERATION_FK = HAO.ID_HOLDER_ACCOUNT_OPERATION_PK                      ");
    	sbQuery.append("    INNER JOIN HOLDER_ACCOUNT HA ON HAO.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK                                                      ");
    	// usamos 'left' porque para el comprador no se realiza asignacion de HM en el proceso de asignacion
    	sbQuery.append("    LEFT JOIN ACCOUNT_OPERATION_MARKETFACT AOM ON hao.ID_HOLDER_ACCOUNT_OPERATION_PK = AOM.ID_HOLDER_ACCOUNT_OPERATION_FK                  	");
    	sbQuery.append("    INNER JOIN MECHANISM_OPERATION MO ON RR.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK                                        ");
    	sbQuery.append("    INNER JOIN PARTICIPANT PAR ON RR.ID_PARTICIPANT_FK = PAR.ID_PARTICIPANT_PK                                                              ");
    	sbQuery.append("    WHERE 1 = 1                                                                                                                             ");
    	// motivo MODIFICACION DE CUENTAS TITULAR
    	sbQuery.append("    AND RR.REQUEST_REASON    in (2336)                                                                                                 ");
    	//sbQuery.append("    AND RR.REQUEST_STATE IN ( 2365 ) 				                                                                                        ");
    	
    	if(Validations.validateIsNotNullAndNotEmpty(reAccountTO.getIdParticipantPk())){
    		sbQuery.append("    AND RR.ID_PARTICIPANT_FK = :participant                                                                                             ");
    	}
    	
    	if(Validations.validateIsNotNullAndNotEmpty(reAccountTO.getReason())){
    		sbQuery.append("    and RR.REQUEST_REASON = :reason                                                                                                     ");
    	}
    	
    	sbQuery.append("    AND TRUNC(RR.AUTHORIZE_DATE) BETWEEN TO_DATE(:iniDt,'dd/MM/yyyy') AND TO_DATE(:endDt,'dd/MM/yyyy')                                      ");
    	sbQuery.append("    ORDER BY ESTADO,CANT_NEW DESC                                                                                    ");
    	sbQuery.append("    ) T                                                                                                                                     ");
    	sbQuery.append("                                                                                                                                            ");
    	sbQuery.append("    UNION ALL                                                                                                                               ");
    	sbQuery.append("                                                                                                                                            ");
    	sbQuery.append("    SELECT                                                                                                                                  ");
    	sbQuery.append("    T.SOLICITUD,                                                                                                                            ");
    	sbQuery.append("    T.PARTICIPANTE,                                                                                                                         ");
    	sbQuery.append("    T.FECHA,                                                                                                                                ");
    	sbQuery.append("    T.PAP_SEQ,                                                                                                                              ");
    	sbQuery.append("    T.MOTIVO,                                                                                                                               ");
    	sbQuery.append("    T.CUEN_OLD,                                                                                                                             ");
    	sbQuery.append("    T.FE_OLD,                                                                                                                               ");
    	sbQuery.append("    T.TAS_OLD,                                                                                                                              ");
    	sbQuery.append("    T.PRE_OLD,                                                                                                                              ");
    	sbQuery.append("    T.CANT_OLD,                                                                                                                             ");
    	sbQuery.append("    T.CUEN_NEW,                                                                                                                             ");
    	sbQuery.append("    T.FE_NEW,                                                                                                                               ");
    	sbQuery.append("    T.TAS_NEW,                                                                                                                              ");
    	sbQuery.append("    T.PRE_NEW,                                                                                                                              ");
    	sbQuery.append("    T.CANT_NEW,                                                                                                                             ");
    	sbQuery.append("    T.ROL,                                                                                                                                  ");
    	sbQuery.append("    T.ESTADO, t.CLASE_CLAVE_VALOR FROM(                                                                                                                          ");
    	sbQuery.append("    select                                                                                                                                  ");
    	sbQuery.append("    RR.ID_REASSIGNMENT_REQUEST_PK SOLICITUD,      			                                                                                ");
    	sbQuery.append("    RR.ID_PARTICIPANT_FK || ' - ' || PAR.MNEMONIC || ' - ' || PAR.DESCRIPTION PARTICIPANTE,                                                 ");
    	sbQuery.append("    TO_CHAR(RR.REGISTER_DATE, 'DD/MM/YYYY') FECHA,                                                                                          ");
    	sbQuery.append("    MO.BALLOT_NUMBER || ' / ' || MO.SEQUENTIAL PAP_SEQ,                                                                                     ");
    	sbQuery.append("    (select PT_SQ.DESCRIPTION from PARAMETER_TABLE pt_sq where PT_SQ.PARAMETER_TABLE_PK = rr.REQUEST_REASON) as MOTIVO,                                                                                                                      ");
    	sbQuery.append("    TO_CHAR(HA_NEW.ALTERNATE_CODE) CUEN_OLD,                                                                                                ");
    	sbQuery.append("    TO_CHAR(RRD.MARKET_DATE,'DD/MM/YYYY') FE_OLD,                                                                                                        ");
    	sbQuery.append("    TO_CHAR(RRD.MARKET_RATE,'999,999,999,999.0000') TAS_OLD,                                                                                                       ");
    	sbQuery.append("    TO_CHAR(RRD.MARKET_PRICE,'999,999,999,999.00') PRE_OLD,                                                                                                      ");
    	sbQuery.append("    TO_CHAR(RRD.MARKET_QUANTITY) CANT_OLD,                                                                                                  ");
    	sbQuery.append("    TO_CHAR(HA_NEW.ALTERNATE_CODE) CUEN_NEW,                                                                                                ");
    	sbQuery.append("    TO_CHAR(SAM.MARKET_DATE,'DD/MM/YYYY') FE_NEW,                                                                                                        ");
    	sbQuery.append("    TO_CHAR(SAM.MARKET_RATE,'999,999,999,999.0000') TAS_NEW,                                                                                                       ");
    	sbQuery.append("    TO_CHAR(SAM.MARKET_PRICE,'999,999,999,999.00') PRE_NEW,                                                                                                      ");
    	sbQuery.append("    TO_CHAR(SAM.MARKET_QUANTITY) CANT_NEW,                                                                                                  ");
    	sbQuery.append("    CASE WHEN HAO_NEW.ROLE = 2 THEN 'VENTA'                                                                                                 ");
    	sbQuery.append("          ELSE 'COMPRA' END ROL,                                                                                                            ");
    	sbQuery.append("    (select pt_sq.description from parameter_table pt_sq where pt_sq.parameter_table_pk = rr.REQUEST_STATE) ESTADO, MO.ID_SECURITY_CODE_FK as CLASE_CLAVE_VALOR                                                                                                                              ");
    	sbQuery.append("    from REASSIGNMENT_REQUEST_DETAIL RRD                                                                                                    ");
    	sbQuery.append("    INNER JOIN REASSIGNMENT_REQUEST RR ON RR.ID_REASSIGNMENT_REQUEST_PK = RRD.ID_REASSIGNMENT_REQUEST_FK and RRD.IND_OLD = 0                ");
    	sbQuery.append("    INNER JOIN SETTLEMENT_ACCOUNT_MARKETFACT SAM ON RRD.ID_ACCOUNT_MARKETFACT_FK = SAM.ID_ACCOUNT_MARKETFACT_PK                             ");
    	sbQuery.append("    INNER JOIN SETTLEMENT_ACCOUNT_OPERATION SAO ON SAM.ID_SETTLEMENT_ACCOUNT_FK = SAO.ID_SETTLEMENT_ACCOUNT_PK                              ");
    	sbQuery.append("    INNER JOIN HOLDER_ACCOUNT_OPERATION HAO_NEW ON SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = HAO_NEW.ID_HOLDER_ACCOUNT_OPERATION_PK              ");
    	sbQuery.append("    INNER JOIN HOLDER_ACCOUNT HA_NEW ON HAO_NEW.ID_HOLDER_ACCOUNT_FK = HA_NEW.ID_HOLDER_ACCOUNT_PK                                          ");
    	sbQuery.append("    INNER JOIN HOLDER_ACCOUNT_OPERATION HAO_OLD ON RRD.ID_HOLDER_ACCOUNT_OPERATION_FK = HAO_OLD.ID_HOLDER_ACCOUNT_OPERATION_PK              ");
    	sbQuery.append("    INNER JOIN HOLDER_ACCOUNT HA_OLD ON HAO_OLD.ID_HOLDER_ACCOUNT_FK = HA_OLD.ID_HOLDER_ACCOUNT_PK                                          ");
    	sbQuery.append("    INNER JOIN MECHANISM_OPERATION MO ON RR.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK                                        ");
    	sbQuery.append("    INNER JOIN PARTICIPANT PAR ON RR.ID_PARTICIPANT_FK = PAR.ID_PARTICIPANT_PK                                                              ");
    	sbQuery.append("                                                                                                                                            ");
    	sbQuery.append("    WHERE 1 = 1                                                                                                                             ");
    	// motivo MODIFICACION HECHOS DE MERCADO
    	sbQuery.append("    AND RR.REQUEST_REASON    in (2337)                                                                                                 ");
    	//sbQuery.append("    AND RR.REQUEST_STATE IN ( 2365 ) 				                                                                                        ");
    	
    	if(Validations.validateIsNotNullAndNotEmpty(reAccountTO.getReason())){
    		sbQuery.append("    and RR.REQUEST_REASON = :reason                                                                                                     ");
    	}
    	
    	if(Validations.validateIsNotNullAndNotEmpty(reAccountTO.getIdParticipantPk())){
    		sbQuery.append("    AND RR.ID_PARTICIPANT_FK = :participant                                                                                             ");
    	}
    	
    	sbQuery.append("    AND TRUNC(RR.AUTHORIZE_DATE) BETWEEN TO_DATE(:iniDt,'dd/MM/yyyy') AND TO_DATE(:endDt,'dd/MM/yyyy')                                      ");
    	sbQuery.append("    ) T                                                                                                     ");
    	
    	sbQuery.append("    ) Z  ORDER BY Z.PARTICIPANTE,Z.MOTIVO,Z.SOLICITUD, z.CUEN_OLD desc, z.ROL desc, Z.ESTADO                                                                                                ");
    	
		String strQueryFormatted = null;
		
		strQueryFormatted= sbQuery.toString();
		
		if(Validations.validateIsNotNullAndNotEmpty(reAccountTO.getReason())){
			strQueryFormatted= strQueryFormatted.replace(":reason", "'"+reAccountTO.getReason().toString()+"'");
		}
		if(Validations.validateIsNotNullAndNotEmpty(reAccountTO.getIdParticipantPk())){
			strQueryFormatted= strQueryFormatted.replace(":participant", "'"+reAccountTO.getIdParticipantPk().toString()+"'");
		}
		
    	strQueryFormatted= strQueryFormatted.replace(":iniDt", "'"+CommonsUtilities.convertDateToString(reAccountTO.getInitialDate(),"dd/MM/yyyy")+"'");
    	strQueryFormatted= strQueryFormatted.replace(":endDt", "'"+CommonsUtilities.convertDateToString(reAccountTO.getFinalDate(),"dd/MM/yyyy")+"'");
		
		return strQueryFormatted;
    }
    
    
    /**
     * Metodo para rellenar el filtro de participante issue 573
     * @param participant
     * @return
     */
    public String getParticipantDetail(Integer participant){

		StringBuilder querySql = new StringBuilder();
		
		querySql.append("   SELECT 										 ");
		querySql.append("   PAR.ID_PARTICIPANT_PK || ' - ' ||  		 	 ");
		querySql.append("   PAR.MNEMONIC || ' - ' ||           			 ");
		querySql.append("   PAR.DESCRIPTION                   			 ");
		querySql.append("   FROM PARTICIPANT PAR               			 ");
		querySql.append("   WHERE PAR.ID_PARTICIPANT_PK =:participant  	 ");
		
		Query query = em.createNativeQuery(querySql.toString());
		query.setParameter("participant", participant);
		String part = (String)query.getSingleResult();
		return part;
	}
        
}
    
    