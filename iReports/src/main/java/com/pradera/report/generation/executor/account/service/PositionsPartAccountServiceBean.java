package com.pradera.report.generation.executor.account.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.report.generation.executor.account.to.PositionsParticipantAccountTO;
import com.pradera.report.generation.executor.excel.delimited.to.ReportPositionsPartAccountTO;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class PositionsPartAccountServiceBean extends CrudDaoServiceBean {

	public PositionsPartAccountServiceBean() {
		// TODO Auto-generated constructor stub
	}
	
	private static int START_TITLE = 0;
	
	public String QueryPositionParticipantAccount(PositionsParticipantAccountTO ppato, Long idStkCalcProcess){
		StringBuilder sbQuery= new StringBuilder();
		StringBuilder sbSubQuery= new StringBuilder();
		
		//sbSubQuery.append(" FROM ACCOUNT_BALANCE_VIEW HAB, PARTICIPANT PA, HOLDER_ACCOUNT HA, SECURITY SE ");
		
//		sbSubQuery.append(" FROM HOLDER_ACCOUNT_BALANCE HAB, PARTICIPANT PA, HOLDER_ACCOUNT HA, SECURITY SE ");
//		sbSubQuery.append(" WHERE HAB.ID_PARTICIPANT_PK = PA.ID_PARTICIPANT_PK ");
//		sbSubQuery.append(" AND HAB.ID_HOLDER_ACCOUNT_PK = HA.ID_HOLDER_ACCOUNT_PK ");
//		sbSubQuery.append(" AND HAB.ID_SECURITY_CODE_PK = SE.ID_SECURITY_CODE_PK ");
		
//		sbSubQuery.append(" AND SE.ID_SECURITY_CODE_PK = AAO.ID_SECURITY_CODE_FK ");
//		sbSubQuery.append(" AND HAB.ID_HOLDER_ACCOUNT_PK = AAO.ID_HOLDER_ACCOUNT_FK ");
//		sbSubQuery.append(" AND AAO.MOTIVE = PT.PARAMETER_TABLE_PK ");
		//sbSubQuery.append(" AND HAB.CUT_DATE = TO_DATE('"+CommonsUtilities.convertDatetoString(ppato.getCourtDate())+"','DD/MM/YYYY') ");
		
		if(ppato.getCourtDate().equals(CommonsUtilities.currentDate())) {
			
			StringBuilder sbSubQuery2 = new StringBuilder();
			
			sbSubQuery2.append(" FROM SECURITY SE  "); 
			sbSubQuery2.append(" INNER JOIN HOLDER_ACCOUNT_BALANCE HAB ON HAB.ID_SECURITY_CODE_PK = SE.ID_SECURITY_CODE_PK  ");
			sbSubQuery2.append(" INNER JOIN PARTICIPANT PA ON PA.ID_PARTICIPANT_PK = hab.ID_PARTICIPANT_PK   ");
			sbSubQuery2.append(" INNER JOIN HOLDER_ACCOUNT HA ON hab.ID_HOLDER_ACCOUNT_PK = HA.ID_HOLDER_ACCOUNT_PK ");
			sbSubQuery2.append(" INNER JOIN ISSUER i ON i.ID_ISSUER_PK = SE.ID_ISSUER_FK   ");
			sbSubQuery2.append(" LEFT JOIN (SELECT DISTINCT t1.ID_SECURITY_CODE_FK, t1.MOTIVE, t1.IND_CUPON,aao.ID_HOLDER_ACCOUNT_FK FROM PHYSICAL_CERTIFICATE t1   ");
			sbSubQuery2.append(" INNER JOIN ACCOUNT_ANNOTATION_OPERATION aao ON t1.ID_PHYSICAL_CERTIFICATE_PK = aao.ID_PHYSICAL_CERTIFICATE_FK ");
			sbSubQuery2.append(" 	WHERE t1.STATE IN (601)   "); 
			sbSubQuery2.append(" ) pc ON  pc.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK AND pc.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK   "); 
			sbSubQuery2.append(" LEFT JOIN PARAMETER_TABLE pt ON pt.PARAMETER_TABLE_PK = pc.MOTIVE   ");
			sbSubQuery2.append(" WHERE SE.STATE_SECURITY NOT IN (2033,2848) AND hab.TOTAL_BALANCE > 0  ");
			sbSubQuery2.append(" AND (pc.IND_CUPON = 0 OR pc.IND_CUPON IS NULL)  ");
			
			if(!ppato.getCourtDate().before(CommonsUtilities.currentDate())){
				sbSubQuery2.append(" AND HA.STATE_ACCOUNT <> 479 ");
			}
			
			if(Validations.validateIsNotNull(idStkCalcProcess)){
				sbSubQuery2.append(" AND HAB.ID_STOCK_CALCULATION_FK="+idStkCalcProcess+" ");
			}
			if(Validations.validateIsNotNull(ppato.getParticipant())){
				sbSubQuery2.append(" AND HAB.ID_PARTICIPANT_PK="+ppato.getParticipant()+" ");
			}
			if(Validations.validateIsNotNull(ppato.getIssuer())){
				sbSubQuery2.append(" AND SE.ID_ISSUER_FK='"+ppato.getIssuer()+"' ");
			}
			if(Validations.validateIsNotNull(ppato.getSecurityClass())){
				sbSubQuery2.append(" AND SE.ID_SECURITY_CODE_PK='"+ppato.getSecurityClass()+"' ");
			}
//			if(Validations.validateIsNotNull(ppato.getCourtDate())){
//				sbSubQuery2.append(" AND TRUNC(SE.REGISTRY_DATE) = TO_DATE('"+CommonsUtilities.convertDatetoString(ppato.getCourtDate())+"','DD/MM/YYYY') ");
//			}
			
//			sbQuery.append(" UNION ");
			sbQuery.append(" SELECT * FROM ( ");
			sbQuery.append(" SELECT "); 
			sbQuery.append(" PA.MNEMONIC ||'-'|| PA.ID_PARTICIPANT_PK ||'-'|| PA.DESCRIPTION AS PARTICIPANTE, ");
			sbQuery.append(" HA.ALTERNATE_CODE AS COD_ALTERNO, ");
			sbQuery.append(" SE.SECURITY_CLASS AS CLASE_VALOR, ");
			sbQuery.append(" SE.CURRENCY AS MONEDA, ");
			sbQuery.append(" SE.ID_SECURITY_CODE_PK AS CLAVE_VALOR, ");
			sbQuery.append(" SE.CURRENT_NOMINAL_VALUE AS VALOR_NOMINAL, ");
			sbQuery.append(" TO_CHAR(SE.EXPIRATION_DATE, 'DD/MM/YYYY') AS FECHA_VENCIMIENTO, ");
			sbQuery.append(" ( CASE WHEN (");
			sbQuery.append(" 	SELECT count (1) FROM  PHYSICAL_CERTIFICATE t2 WHERE t2.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK   AND t2.STATE IN (601)");
			sbQuery.append(" ) = 0 THEN 'ELECTRONICO'");
			sbQuery.append(" else pt.PARAMETER_NAME END ) AS MOTIVO,");
//			sbQuery.append(" PT.DESCRIPTION AS MOTIVO, ");
			sbQuery.append(" SUM(HAB.TOTAL_BALANCE) AS TOTAL, ");
			sbQuery.append(" SUM(HAB.AVAILABLE_BALANCE) AS DISPONIBLE, ");
			sbQuery.append(" SUM(HAB.TRANSIT_BALANCE) AS TRANSITO, ");
			sbQuery.append(" SUM(HAB.PAWN_BALANCE) AS GRAVAMEN, ");
			sbQuery.append(" SUM(HAB.BAN_BALANCE) AS EMBARGO, ");
			sbQuery.append(" SUM(HAB.OTHER_BLOCK_BALANCE) AS BLOQUEO_OTROS, ");
			sbQuery.append(" SUM(HAB.ACCREDITATION_BALANCE) AS ACREDITACION, ");
			sbQuery.append(" SUM(HAB.REPORTING_BALANCE) AS REPORTADOR, ");
			sbQuery.append(" SUM(HAB.REPORTED_BALANCE) AS REPORTADO, ");
			sbQuery.append(" SUM(HAB.SALE_BALANCE) AS VENTA, ");
			sbQuery.append(" SUM(HAB.PURCHASE_BALANCE) AS COMPRA, ");
			sbQuery.append(" COUNT(1) AS TOTAL_CUENTAS, ");
			//sbQuery.append(" HA.RELATED_NAME AS NOMBRE_CUENTA, ");
			sbQuery.append("(SELECT PRADERA_DEPOSITARY.FN_GET_HOLDER_DESC(HAB.ID_HOLDER_ACCOUNT_PK , '-', 1) FROM DUAL) AS NOMBRE_CUENTA, ");
			sbQuery.append(" SE.REFERENCE_PART_CODE ");
			/*sbQuery.append(" CAST(COUNT(1)*100/(SELECT SUM(1) "); 
			sbQuery.append(" " + sbSubQuery.toString() + " ");
			sbQuery.append(" )AS NUMBER(8,4)) DIV_CUENTAS ");*/
			sbQuery.append(" " + sbSubQuery2.toString() + " ");
			sbQuery.append(" GROUP BY  ");
			sbQuery.append(" PA.MNEMONIC ||'-'|| PA.ID_PARTICIPANT_PK ||'-'|| PA.DESCRIPTION, ");
			sbQuery.append(" HA.ALTERNATE_CODE, ");
			sbQuery.append(" SE.ID_SECURITY_CODE_ONLY, ");
			sbQuery.append(" SE.CURRENCY, ");
			sbQuery.append(" SE.SECURITY_CLASS, ");
			sbQuery.append(" SE.CURRENT_NOMINAL_VALUE, SE.EXPIRATION_DATE, ");
			sbQuery.append(" HAB.ID_HOLDER_ACCOUNT_PK, ");
			sbQuery.append(" pt.PARAMETER_NAME, ");
			sbQuery.append(" SE.REFERENCE_PART_CODE, ");
			sbQuery.append(" SE.ID_SECURITY_CODE_PK ");
			sbQuery.append(" order by PARTICIPANTE,SE.SECURITY_CLASS,SE.CURRENCY asc) ");			
			
		}else {
			sbSubQuery.append(" FROM SECURITY SE  "); 
			sbSubQuery.append(" INNER JOIN HOLDER_ACCOUNT_BALANCE_HISTORY HAB ON HAB.ID_SECURITY_CODE_PK = SE.ID_SECURITY_CODE_PK  ");
			sbSubQuery.append(" INNER JOIN PARTICIPANT PA ON PA.ID_PARTICIPANT_PK = hab.ID_PARTICIPANT_PK   ");
			sbSubQuery.append(" INNER JOIN HOLDER_ACCOUNT HA ON hab.ID_HOLDER_ACCOUNT_PK = HA.ID_HOLDER_ACCOUNT_PK ");
			sbSubQuery.append(" INNER JOIN ISSUER i ON i.ID_ISSUER_PK = SE.ID_ISSUER_FK   ");
			sbSubQuery.append(" LEFT JOIN (SELECT DISTINCT t1.ID_SECURITY_CODE_FK, t1.MOTIVE, t1.IND_CUPON,aao.ID_HOLDER_ACCOUNT_FK FROM PHYSICAL_CERTIFICATE t1   "); 
			sbSubQuery.append(" INNER JOIN ACCOUNT_ANNOTATION_OPERATION aao ON t1.ID_PHYSICAL_CERTIFICATE_PK = aao.ID_PHYSICAL_CERTIFICATE_FK ");
			sbSubQuery.append(" 	WHERE t1.STATE IN (601)   "); 
			sbSubQuery.append(" ) pc ON  pc.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK AND pc.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK  "); 
			sbSubQuery.append(" LEFT JOIN PARAMETER_TABLE pt ON pt.PARAMETER_TABLE_PK = pc.MOTIVE   ");
			sbSubQuery.append(" WHERE SE.STATE_SECURITY NOT IN (2033,2848) AND hab.TOTAL_BALANCE > 0  ");
			sbSubQuery.append(" AND (pc.IND_CUPON = 0 OR pc.IND_CUPON IS NULL)  ");
			
			if(!ppato.getCourtDate().before(CommonsUtilities.currentDate())){
				sbSubQuery.append(" AND HA.STATE_ACCOUNT <> 479 ");
			}
			
			if(Validations.validateIsNotNull(idStkCalcProcess)){
				sbSubQuery.append(" AND HAB.ID_STOCK_CALCULATION_FK="+idStkCalcProcess+" ");
			}
			if(Validations.validateIsNotNull(ppato.getParticipant())){
				sbSubQuery.append(" AND HAB.ID_PARTICIPANT_PK="+ppato.getParticipant()+" ");
			}
			if(Validations.validateIsNotNull(ppato.getIssuer())){
				sbSubQuery.append(" AND SE.ID_ISSUER_FK='"+ppato.getIssuer()+"' ");
			}
			if(Validations.validateIsNotNull(ppato.getSecurityClass())){
				sbSubQuery.append(" AND SE.ID_SECURITY_CODE_PK='"+ppato.getSecurityClass()+"' ");
			}
			if(Validations.validateIsNotNull(ppato.getCourtDate())){
				sbSubQuery.append(" AND TRUNC(HAB.CUT_DATE) = TO_DATE('"+CommonsUtilities.convertDatetoString(ppato.getCourtDate())+"','DD/MM/YYYY') ");
			}
			sbQuery.append(" SELECT * FROM ("); 
			sbQuery.append(" SELECT "); 
			sbQuery.append(" PA.MNEMONIC ||'-'|| PA.ID_PARTICIPANT_PK ||'-'|| PA.DESCRIPTION AS PARTICIPANTE, ");
			sbQuery.append(" HA.ALTERNATE_CODE AS COD_ALTERNO, ");
			sbQuery.append(" SE.SECURITY_CLASS AS CLASE_VALOR, ");
			sbQuery.append(" SE.CURRENCY AS MONEDA, ");
			sbQuery.append(" SE.ID_SECURITY_CODE_PK AS CLAVE_VALOR, ");
			sbQuery.append(" SE.CURRENT_NOMINAL_VALUE AS VALOR_NOMINAL, ");
			sbQuery.append(" TO_CHAR(SE.EXPIRATION_DATE, 'DD/MM/YYYY') AS FECHA_VENCIMIENTO, ");
			sbQuery.append(" ( CASE WHEN (");
			sbQuery.append(" 	SELECT count (1) FROM  PHYSICAL_CERTIFICATE t2 WHERE t2.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK   AND t2.STATE IN (601)");
			sbQuery.append(" ) = 0 THEN 'ELECTRONICO'");
			sbQuery.append(" else pt.PARAMETER_NAME END ) AS MOTIVO,");
//			sbQuery.append(" PT.DESCRIPTION AS MOTIVO, ");
			sbQuery.append(" SUM(HAB.TOTAL_BALANCE) AS TOTAL, ");
			sbQuery.append(" SUM(HAB.AVAILABLE_BALANCE) AS DISPONIBLE, ");
			sbQuery.append(" SUM(HAB.TRANSIT_BALANCE) AS TRANSITO, ");
			sbQuery.append(" SUM(HAB.PAWN_BALANCE) AS GRAVAMEN, ");
			sbQuery.append(" SUM(HAB.BAN_BALANCE) AS EMBARGO, ");
			sbQuery.append(" SUM(HAB.OTHER_BLOCK_BALANCE) AS BLOQUEO_OTROS, ");
			sbQuery.append(" SUM(HAB.ACCREDITATION_BALANCE) AS ACREDITACION, ");
			sbQuery.append(" SUM(HAB.REPORTING_BALANCE) AS REPORTADOR, ");
			sbQuery.append(" SUM(HAB.REPORTED_BALANCE) AS REPORTADO, ");
			sbQuery.append(" SUM(HAB.SALE_BALANCE) AS VENTA, ");
			sbQuery.append(" SUM(HAB.PURCHASE_BALANCE) AS COMPRA, ");
			sbQuery.append(" COUNT(1) AS TOTAL_CUENTAS, ");
			//sbQuery.append(" HA.RELATED_NAME AS NOMBRE_CUENTA, ");
			sbQuery.append("(SELECT PRADERA_DEPOSITARY.FN_GET_HOLDER_DESC(HAB.ID_HOLDER_ACCOUNT_PK , '-', 1) FROM DUAL) AS NOMBRE_CUENTA, ");
			sbQuery.append(" SE.REFERENCE_PART_CODE ");
			/*sbQuery.append(" CAST(COUNT(1)*100/(SELECT SUM(1) "); 
			sbQuery.append(" " + sbSubQuery.toString() + " ");
			sbQuery.append(" )AS NUMBER(8,4)) DIV_CUENTAS ");*/
			sbQuery.append(" " + sbSubQuery.toString() + " ");
			sbQuery.append(" GROUP BY  ");
			sbQuery.append(" PA.MNEMONIC ||'-'|| PA.ID_PARTICIPANT_PK ||'-'|| PA.DESCRIPTION, ");
			sbQuery.append(" HA.ALTERNATE_CODE, ");
			sbQuery.append(" SE.ID_SECURITY_CODE_ONLY, ");
			sbQuery.append(" SE.CURRENCY, ");
			sbQuery.append(" SE.SECURITY_CLASS, ");
			sbQuery.append(" SE.CURRENT_NOMINAL_VALUE, SE.EXPIRATION_DATE, ");
			sbQuery.append(" HAB.ID_HOLDER_ACCOUNT_PK, ");
			sbQuery.append(" pt.PARAMETER_NAME, ");
			sbQuery.append(" SE.REFERENCE_PART_CODE, ");
			sbQuery.append(" SE.ID_SECURITY_CODE_PK ");
			sbQuery.append(" order by PARTICIPANTE,SE.SECURITY_CLASS,SE.CURRENCY asc) ");
		}
		
		return sbQuery.toString();
	}
	
	public String QueryFormatForJasper(PositionsParticipantAccountTO positionsParticipantAccountTO, String sbQuery, Long idStkCalcProcess){
    	String strQueryFormatted = null;
    	
    	if(positionsParticipantAccountTO.getParticipant()!=null){
    		strQueryFormatted=sbQuery.replace(":participant", positionsParticipantAccountTO.getParticipant().toString());
		}
		if(positionsParticipantAccountTO.getIssuer() !=null){
			strQueryFormatted=strQueryFormatted.replace(":issuer", positionsParticipantAccountTO.getIssuer().toString());
		}
		if(positionsParticipantAccountTO.getSecurityClass() !=null){
			strQueryFormatted=strQueryFormatted.replace(":securityClass", positionsParticipantAccountTO.getSecurityClass());
		}
		if(idStkCalcProcess!=null){
			strQueryFormatted=strQueryFormatted.replace(":idSCP", idStkCalcProcess.toString());
		}
    
    	
    	return strQueryFormatted;
    }
	
	public byte[] generateExcelDelimitated(Map<String,Object> parametersRequired, PositionsParticipantAccountTO ppato, Long idStkCalcProcess) {

		String query = this.QueryPositionParticipantAccount(ppato, idStkCalcProcess);
		
		//Query queryExecution= em.createNativeQuery(query);
	
		//List<Object> listResult = queryExecution.getResultList();
		
		List<ReportPositionsPartAccountTO> listResult = this.getReportData(query);
		
		byte[] bytes = null;
		
		List<String> lstHeadsSheet = new ArrayList<String>();
		lstHeadsSheet.add("DEPOSITANTE;CLASE VALOR;CÓDIGO ALTERNO;NOMBRE CUENTA;CÓDIGO DE VALOR;CÓDIGO REFERENCIA EN DEPOSITANTE;MONEDA;VALOR NOMINAL;FECHA VENCIMIENTO;MOTIVO DE INGRESO;CONTABLE;DISPONIBLE;"
				+ "PRENDA;EMBARGO;ACREDITACIÓN;REPORTANTE;REPORTADO;COMPRA;VENTA;TRANSITO;");
		/*lstHeadsSheet.add("CÓDIGO DE VALOR;");
		lstHeadsSheet.add("CONTABLE;");
		lstHeadsSheet.add("DISPONIBLE;");
		lstHeadsSheet.add("PRENDA;");
		lstHeadsSheet.add("EMBARGO");
		lstHeadsSheet.add("FECHA VENCIMIENTO;");
		lstHeadsSheet.add("SALDOS ACREDITACIÓN;");
		lstHeadsSheet.add("REPORTANTE;");
		lstHeadsSheet.add("REPORTADO;");
		lstHeadsSheet.add("COMPRA;");
		lstHeadsSheet.add("VENTA;");
		lstHeadsSheet.add("TRANSITO");
		lstHeadsSheet.add("MOTIVO DE INGRESO;");
		lstHeadsSheet.add("VALOR NOMINAL;");*/
		
        String [] headerSheet = new String[lstHeadsSheet.size()];
		int m = 0;
		for (String title : lstHeadsSheet) {
			headerSheet[m] = title;
			m = ++m;
		}
		XSSFWorkbook libro= new XSSFWorkbook();
		String hoja = "Hoja 1";
		XSSFSheet hojas = libro.createSheet(hoja);
		CellStyle styleTitle = libro.createCellStyle();
	    Font fontTitle = libro.createFont();
	    fontTitle.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    fontTitle.setFontHeightInPoints((short)14); 
	    styleTitle.setFont(fontTitle);
	    
	    CellStyle styleSubTitle = libro.createCellStyle();
	    Font fontSubTitle = libro.createFont();
	    fontSubTitle.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    fontSubTitle.setFontHeightInPoints((short)10); 
	    styleSubTitle.setFont(fontSubTitle);
	    
		CellStyle style = libro.createCellStyle();
	    Font font = libro.createFont();
	    font.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    font.setFontHeightInPoints((short)10); 
	    style.setFont(font);
	    
	    XSSFRow row=hojas.createRow(START_TITLE); 
		XSSFCell cellTitle= row.createCell(0); // 3 columna en ubicarse el titulo
		cellTitle.setCellStyle(styleTitle);  
		cellTitle.setCellValue("REPORTE DE CONSOLIDADO DE POSICIONES DEPOSITANTE CUENTA");
		
		if(listResult != null && listResult.size() > 0) {
			
			int i = 0;
			
			row = hojas.createRow(++i); 
    		for (int k = 0; k <headerSheet.length; k++) {
    			XSSFCell cell= row.createCell(k); 
    			cell.setCellStyle(style); 
    			cell.setCellValue(headerSheet[k]); 
    		} 
    		
    		for(ReportPositionsPartAccountTO reportPositionsPartAccountTO : listResult) {
				row = hojas.createRow(++i); 
				int j = 0;
				StringBuilder result = new StringBuilder();
				result.append(reportPositionsPartAccountTO.getParticipante());
				result.append(";");
				
				Map<Integer,String> map_security = (Map<Integer, String>) parametersRequired.get("p_securityClass");
				map_security.forEach((key, value) -> {
					if(key.toString().equals(reportPositionsPartAccountTO.getClaseValor())) {
						result.append(value);
						result.append(";");
					}
				});

				result.append(reportPositionsPartAccountTO.getCodigoAlterno());
				result.append(";");
				result.append(reportPositionsPartAccountTO.getNombreCuenta());
				result.append(";");
				result.append(reportPositionsPartAccountTO.getCodigoValor());
				result.append(";");
				result.append(reportPositionsPartAccountTO.getReferencePartCode());
				result.append(";");				
			
				Map<Integer,String> map_currency = (Map<Integer,String>) parametersRequired.get("p_currency");
				map_currency.forEach((key, value) -> {
					if(key.toString().equals(reportPositionsPartAccountTO.getMoneda())) {
						result.append(value);
						result.append(";");
					}
				});
				
				
				result.append(reportPositionsPartAccountTO.getValorNominal());
				result.append(";");
				
				result.append(reportPositionsPartAccountTO.getFechaVencimiento().toString());
				result.append(";");
				
				result.append(reportPositionsPartAccountTO.getMotivoIngreso());
				result.append(";");
				
				result.append(reportPositionsPartAccountTO.getTotal());
				result.append(";");
				result.append(reportPositionsPartAccountTO.getDisponible());
				result.append(";");
				result.append(reportPositionsPartAccountTO.getGravamen());
				result.append(";");
				result.append(reportPositionsPartAccountTO.getEmbargo());
				result.append(";");

				result.append(reportPositionsPartAccountTO.getSaldosAcreditacion());
				result.append(";");
				result.append(reportPositionsPartAccountTO.getReportador());
				result.append(";");
				result.append(reportPositionsPartAccountTO.getReportado());
				result.append(";");
				result.append(reportPositionsPartAccountTO.getCompra());
				result.append(";");
				result.append(reportPositionsPartAccountTO.getVenta());
				result.append(";");
				result.append(reportPositionsPartAccountTO.getTransito());
				result.append(";");


				//String result = reportPositionsPartAccountTO.getParticipante().concat(reportPositionsPartAccountTO.getParticipante()).concat(reportPositionsPartAccountTO.getParticipante());
				XSSFCell
				cell = row.createCell(j);   cell.setCellValue(result.toString());
				/*cell = row.createCell(++j); cell.setCellValue("CLIENTE;");
				cell = row.createCell(++j); cell.setCellValue("CLIENTE;");
				cell = row.createCell(++j); cell.setCellValue("CLIENTE;");
				cell = row.createCell(++j); cell.setCellValue("CLIENTE;");
				cell = row.createCell(++j); cell.setCellValue("CLIENTE;");
				cell = row.createCell(++j); cell.setCellValue("CLIENTE;");
				cell = row.createCell(++j); cell.setCellValue("CLIENTE;");
				cell = row.createCell(++j); cell.setCellValue("CLIENTE;");
				cell = row.createCell(++j); cell.setCellValue("CLIENTE;");
				cell = row.createCell(++j); cell.setCellValue("CLIENTE;");
				cell = row.createCell(++j); cell.setCellValue("CLIENTE;");
				cell = row.createCell(++j); cell.setCellValue("CLIENTE;");
				cell = row.createCell(++j); cell.setCellValue("CLIENTE;");
				cell = row.createCell(++j); cell.setCellValue("CLIENTE;");*/
				
    		}
    		
		}
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		//OutputStream ous = new OutputStream
		
		try {
			try {
				libro.write(bos);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} finally {
		    try {
				bos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		bytes = bos.toByteArray();
		return bytes;
		
	}
	
	public List<ReportPositionsPartAccountTO> getReportData(String query) {
		
		Query queryExecution= em.createNativeQuery(query);
		List<Object[]> listResult = queryExecution.getResultList();
		
		List<ReportPositionsPartAccountTO> listReportData = new ArrayList<ReportPositionsPartAccountTO>();
		if(listResult != null && listResult.size() > 0) {
			for(Object[] iterator : listResult) {
				ReportPositionsPartAccountTO reportPositionsPartAccountTO = new ReportPositionsPartAccountTO();
				reportPositionsPartAccountTO.setParticipante(iterator[0] != null ? iterator[0].toString() : GeneralConstants.EMPTY_STRING);
				reportPositionsPartAccountTO.setCodigoAlterno(iterator[1] != null ? iterator[1].toString() : GeneralConstants.EMPTY_STRING);
				reportPositionsPartAccountTO.setClaseValor(iterator[2] != null ? iterator[2].toString() : GeneralConstants.EMPTY_STRING);
				reportPositionsPartAccountTO.setMoneda(iterator[3] != null ? iterator[3].toString() : GeneralConstants.EMPTY_STRING);
				reportPositionsPartAccountTO.setCodigoValor(iterator[4] != null ? iterator[4].toString() : GeneralConstants.EMPTY_STRING);
				reportPositionsPartAccountTO.setValorNominal(iterator[5] != null ? iterator[5].toString() : GeneralConstants.EMPTY_STRING);
				reportPositionsPartAccountTO.setFechaVencimiento(iterator[6] != null ? iterator[6].toString() : GeneralConstants.EMPTY_STRING);
				reportPositionsPartAccountTO.setMotivoIngreso(iterator[7] != null ? iterator[7].toString() : GeneralConstants.EMPTY_STRING);
				reportPositionsPartAccountTO.setTotal(iterator[8] != null ? iterator[8].toString() : GeneralConstants.EMPTY_STRING);
				reportPositionsPartAccountTO.setDisponible(iterator[9] != null ? iterator[9].toString() : GeneralConstants.EMPTY_STRING);
				reportPositionsPartAccountTO.setTransito(iterator[10] != null ? iterator[10].toString() : GeneralConstants.EMPTY_STRING);
				reportPositionsPartAccountTO.setGravamen(iterator[11] != null ? iterator[11].toString() : GeneralConstants.EMPTY_STRING);
				reportPositionsPartAccountTO.setEmbargo(iterator[12] != null ? iterator[12].toString() : GeneralConstants.EMPTY_STRING);
				reportPositionsPartAccountTO.setBloqueoOtros(iterator[13] != null ? iterator[13].toString() : GeneralConstants.EMPTY_STRING);
				reportPositionsPartAccountTO.setSaldosAcreditacion(iterator[14] != null ? iterator[14].toString() : GeneralConstants.EMPTY_STRING);
				reportPositionsPartAccountTO.setReportador(iterator[15] != null ? iterator[15].toString() : GeneralConstants.EMPTY_STRING);
				reportPositionsPartAccountTO.setReportado(iterator[16] != null ? iterator[16].toString() : GeneralConstants.EMPTY_STRING);
				reportPositionsPartAccountTO.setVenta(iterator[17] != null ? iterator[17].toString() : GeneralConstants.EMPTY_STRING);
				reportPositionsPartAccountTO.setCompra(iterator[18] != null ? iterator[18].toString() : GeneralConstants.EMPTY_STRING);
				reportPositionsPartAccountTO.setTotalCuentas(iterator[19] != null ? iterator[19].toString() : GeneralConstants.EMPTY_STRING);
				reportPositionsPartAccountTO.setNombreCuenta(iterator[20] != null ? iterator[20].toString() : GeneralConstants.EMPTY_STRING);
				reportPositionsPartAccountTO.setReferencePartCode(iterator[21] != null ? iterator[21].toString() : GeneralConstants.EMPTY_STRING);
				
				listReportData.add(reportPositionsPartAccountTO);
			}
		}
		
		return listReportData;
		
	}
	
}
