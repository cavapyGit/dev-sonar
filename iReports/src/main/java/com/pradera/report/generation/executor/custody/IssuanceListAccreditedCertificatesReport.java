package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;

import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.util.view.UtilReportConstants;
@ReportProcess(name = "IssuanceListAccreditedCertificatesReport")
public class IssuanceListAccreditedCertificatesReport extends GenericReport {

	private static final long serialVersionUID = 1L;
    private final String MAP_STATE="map_state";
    
	private Map<String, Object> map= new HashMap<String, Object>();
	@EJB
	private ParameterServiceBean parameterService;
	
	@Override
	public Map<String, Object> getCustomJasperParameters(){
		Map<Integer,String> mapAux;
		ParameterTableTO  parameterFilter = new ParameterTableTO();

		mapAux= new HashMap<Integer, String>();
		parameterFilter.setMasterTableFk(MasterTableType.ACCREDITATION_CERTIFICATE_STATE.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(parameterFilter)) 
			mapAux.put(param.getParameterTablePk(), param.getDescription());
		map.put(MAP_STATE, mapAux);
		
		map.put("logo_path", UtilReportConstants.readLogoReport());
			
		return map;
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		return null;
	}
}
