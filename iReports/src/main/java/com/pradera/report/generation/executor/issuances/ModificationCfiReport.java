package com.pradera.report.generation.executor.issuances;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.issuances.services.IssuancesReportServiceBean;
import com.pradera.report.generation.executor.securities.service.PaymentChronogramBySecurity;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;

@ReportProcess(name = "ModificationCfiReport")
public class ModificationCfiReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;

	@EJB
	private IssuancesReportServiceBean issuancesReportService;
	
	@Inject
	private PraderaLogger log;

	public ModificationCfiReport() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {

		GenericsFiltersTO gfto = new GenericsFiltersTO();
		
		for(ReportLoggerDetail param: getReportLogger().getReportLoggerDetails()){
			if(param.getFilterName().equals("security_class_key")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setIdSecurityCodePk(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("date_initial")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setInitialDt(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("date_end")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setFinalDt(param.getFilterValue().toString());
				}
			}
		}
		
		String strQuery = issuancesReportService.getModificationCfiReport(gfto); 
		
		
		Map<String,Object> parametersRequired = new HashMap<>();

		parametersRequired.put("strQuery", strQuery);
		parametersRequired.put("initialDt", gfto.getInitialDt());
		parametersRequired.put("finalDt", gfto.getFinalDt());
		
		return parametersRequired;
		
		
	}

}
