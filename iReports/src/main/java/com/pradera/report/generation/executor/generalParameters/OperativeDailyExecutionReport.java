package com.pradera.report.generation.executor.generalParameters;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;

@ReportProcess(name = "OperativeDailyExecutionReport")
public class OperativeDailyExecutionReport extends GenericReport{

	private static final long serialVersionUID = 1L;
	
	@Inject PraderaLogger log;
	@EJB private ParameterServiceBean parameterService;
	
	public OperativeDailyExecutionReport() { }
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		return null;
	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		Map<String,Object> parametersRequired = new HashMap<>();
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		
		for (ReportLoggerDetail  loggerDetail: loggerDetails) {
			
			if (loggerDetail.getFilterName().equals("proceso_cierre")){
				if(loggerDetail.getFilterValue() != null){
					parametersRequired.put("proceso_cierre",loggerDetail.getFilterValue().toString());
					parametersRequired.put("proceso_cierre_desc",loggerDetail.getFilterDescription());
				}else{
					parametersRequired.put("proceso_cierre_desc","TODOS");
				}
			}
			if (loggerDetail.getFilterName().equals(ReportConstant.DATE_INITIAL_PARAM)){
				if(loggerDetail.getFilterValue() != null)
					parametersRequired.put(ReportConstant.DATE_INITIAL_PARAM,loggerDetail.getFilterValue().toString());
			}
			if (loggerDetail.getFilterName().equals(ReportConstant.DATE_END_PARAM)){
				if(loggerDetail.getFilterValue() != null)
					parametersRequired.put(ReportConstant.DATE_END_PARAM,loggerDetail.getFilterValue().toString());
			}
			if (loggerDetail.getFilterName().equalsIgnoreCase("admin")){
				if(loggerDetail.getFilterValue() != null){
					if(loggerDetail.getFilterValue().equalsIgnoreCase("NO")){
						parametersRequired.put("admin",loggerDetail.getFilterValue().toString());
						parametersRequired.put("admin_desc",loggerDetail.getFilterValue());
					}
					else{
						loggerDetail.setFilterValue(null);
						parametersRequired.put("admin",loggerDetail.getFilterValue());
						parametersRequired.put("admin_desc","SI");
					}
				}
				else parametersRequired.put("admin_desc","TODOS");
				
			}
		}
			
		
		return parametersRequired;
	}
}
