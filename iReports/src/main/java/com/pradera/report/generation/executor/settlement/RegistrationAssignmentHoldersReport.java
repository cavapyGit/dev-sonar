package com.pradera.report.generation.executor.settlement;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "RegistrationAssignmentHoldersReport")
public class RegistrationAssignmentHoldersReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;
	@Inject
	PraderaLogger log;
	
	@EJB
	ParticipantServiceBean participantServiceBean;

	public RegistrationAssignmentHoldersReport() {
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addParametersQueryReport() {

	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		Map<String,Object> parametersRequired = new HashMap<>();
		Map<Integer,String> participantMnemonic = new HashMap<Integer, String>();
		
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		String participantCode = null;
		String participantDescription = null;
		
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals(ReportConstant.PARTICIPANT_PARAM) && r.getFilterValue()!=null)
				participantCode = r.getFilterValue();
		}
		
		try {
			
			for(Participant participant : participantServiceBean.getLisParticipantServiceBean(new Participant())) {
				participantMnemonic.put(participant.getIdParticipantPk().intValue(), participant.getMnemonic());
			}
			
			if(participantCode != null){
				Participant participant = participantServiceBean.find(Participant.class, new Long(participantCode));
				participantDescription = participant.getMnemonic() + " - " + participant.getDescription();
			}
			else{
				participantDescription = "TODOS";
			}
			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		parametersRequired.put("participant_description", participantDescription);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		// TODO Auto-generated method stub
		return parametersRequired;
	}
	
}
