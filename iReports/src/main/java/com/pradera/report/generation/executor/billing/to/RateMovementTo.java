package com.pradera.report.generation.executor.billing.to;

import java.io.Serializable;
import java.math.BigDecimal;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RateMovementTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class RateMovementTo implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The rate amount. */
	private BigDecimal rateAmount;
	
	/** The rate percent. */
	private BigDecimal ratePercent;
	
	/** The description movement type. */
	private String descriptionMovementType;
	
	/**
	 * Gets the rate amount.
	 *
	 * @return the rate amount
	 */
	public BigDecimal getRateAmount() {
		return rateAmount;
	}
	
	/**
	 * Sets the rate amount.
	 *
	 * @param rateAmount the new rate amount
	 */
	public void setRateAmount(BigDecimal rateAmount) {
		this.rateAmount = rateAmount;
	}
	
	/**
	 * Gets the rate percent.
	 *
	 * @return the rate percent
	 */
	public BigDecimal getRatePercent() {
		return ratePercent;
	}
	
	/**
	 * Sets the rate percent.
	 *
	 * @param ratePercent the new rate percent
	 */
	public void setRatePercent(BigDecimal ratePercent) {
		this.ratePercent = ratePercent;
	}
	
	/**
	 * Gets the description movement type.
	 *
	 * @return the description movement type
	 */
	public String getDescriptionMovementType() {
		return descriptionMovementType;
	}
	
	/**
	 * Sets the description movement type.
	 *
	 * @param descriptionMovementType the new description movement type
	 */
	public void setDescriptionMovementType(String descriptionMovementType) {
		this.descriptionMovementType = descriptionMovementType;
	}
	
	

}
