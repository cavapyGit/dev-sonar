package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "VoluntaryAccountEntriesReport")
public class VoluntaryAccountEntriesReport extends GenericReport{

	private static final long serialVersionUID = 1L;
	
	@Inject PraderaLogger log;
	
	public VoluntaryAccountEntriesReport() { }
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		return null;
	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		Map<String,Object> parametersRequired = new HashMap<>();
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();
		
		for (ReportLoggerDetail  loggerDetail: loggerDetails) {
			
			if (loggerDetail.getFilterName().equals(ReportConstant.PARTICIPANT_PARAM)){
				if(loggerDetail.getFilterValue() != null){
					parametersRequired.put(ReportConstant.PARTICIPANT_PARAM,loggerDetail.getFilterValue().toString());
					parametersRequired.put(ReportConstant.PARTICIPANT_DESC,loggerDetail.getFilterDescription());
				}else{
					parametersRequired.put(ReportConstant.PARTICIPANT_DESC,"TODOS");
				}
			}
			if (loggerDetail.getFilterName().equals(ReportConstant.HOLDER_PARAM)){
				if(loggerDetail.getFilterValue() != null){
					parametersRequired.put(ReportConstant.HOLDER_PARAM,loggerDetail.getFilterValue().toString());
					parametersRequired.put(ReportConstant.HOLDER_PARAM_DESC,loggerDetail.getFilterDescription());
				}else{
					parametersRequired.put(ReportConstant.HOLDER_PARAM_DESC,"TODOS");
				}
			}
			if (loggerDetail.getFilterName().equals(ReportConstant.SECURITY_CLASS_PARAM)){
				if(loggerDetail.getFilterValue() != null){
					parametersRequired.put(ReportConstant.SECURITY_CLASS_PARAM,loggerDetail.getFilterValue().toString());
					parametersRequired.put(ReportConstant.SECURITY_CLASS_DESC,loggerDetail.getFilterDescription());
				}else{
					parametersRequired.put(ReportConstant.SECURITY_CLASS_DESC,"TODAS");
				}
			}
			if (loggerDetail.getFilterName().equals(ReportConstant.SECURITY_CODE)){
				if(loggerDetail.getFilterValue() != null){
					parametersRequired.put(ReportConstant.SECURITY_CODE,loggerDetail.getFilterValue().toString());
					parametersRequired.put(ReportConstant.SECURITY_CODE_DESC,loggerDetail.getFilterDescription());
				}else{
					parametersRequired.put(ReportConstant.SECURITY_CODE_DESC,"TODOS");
				}
			}
			if (loggerDetail.getFilterName().equals(ReportConstant.ISSUER_PARAM)){
				if(loggerDetail.getFilterValue() != null){
					parametersRequired.put(ReportConstant.ISSUER_PARAM,loggerDetail.getFilterValue().toString());
					parametersRequired.put(ReportConstant.ISSUER_DESC,loggerDetail.getFilterDescription());
				}else{
					parametersRequired.put(ReportConstant.ISSUER_DESC,"TODOS");
				}
			}
			if (loggerDetail.getFilterName().equals(ReportConstant.CURRENCY_TYPE)){
				if(loggerDetail.getFilterValue() != null){
					parametersRequired.put(ReportConstant.CURRENCY_TYPE,loggerDetail.getFilterValue().toString());
					parametersRequired.put(ReportConstant.CURRENCY_TYPE_DESC,loggerDetail.getFilterDescription());
				}else{
					parametersRequired.put(ReportConstant.CURRENCY_TYPE_DESC,"TODAS");
				}
			}
			if (loggerDetail.getFilterName().equals(ReportConstant.MOTIVE_PARAM)){
				if(loggerDetail.getFilterValue() != null){
					parametersRequired.put(ReportConstant.MOTIVE_PARAM,loggerDetail.getFilterValue().toString());
					parametersRequired.put(ReportConstant.MOTIVE_PARAM_DESC,loggerDetail.getFilterDescription());
				}else{
					parametersRequired.put(ReportConstant.MOTIVE_PARAM_DESC,"TODOS");
				}
			}
			if (loggerDetail.getFilterName().equals(ReportConstant.DATE_END_PARAM)){
				if(loggerDetail.getFilterValue() != null)
					parametersRequired.put(ReportConstant.DATE_END_PARAM,loggerDetail.getFilterValue().toString());
			}
			if (loggerDetail.getFilterName().equals(ReportConstant.DATE_INITIAL_PARAM)){
				if(loggerDetail.getFilterValue() != null)
					parametersRequired.put(ReportConstant.DATE_INITIAL_PARAM,loggerDetail.getFilterValue().toString());
			}
		}
		
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		return parametersRequired;
	}
}
